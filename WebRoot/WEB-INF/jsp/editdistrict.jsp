<!-- @Author: Gagan 
 * @Discription: view of edit district Page.
-->
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>

<script type="text/javascript" src="dwr/interface/DistrictDomainAjax.js?ver=${resourceMap['DistrictDomainAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/DistrictAjax.js?ver=${resourceMap['DistrictAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/I4QuestionSetAjax.js?ver=${resourceMap['I4QuestionSetAjax']}"></script>
<script type="text/javascript" src="dwr/interface/DistrictAssessmentAjax.js?ver=${resourceMap['DistrictAssessmentAjax']}"></script>
<script type="text/javascript" src="dwr/interface/UserAjax.js?ver=${resourceMap['UserAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<!-- TPL-4823--- Add configuration option HireVue Integration for District start-->	
<script type='text/javascript' src='js/editdistrict.js'></script>
<!-- TPL-4823--- Add configuration option HireVue Integration for District end-->	
<script type='text/javascript' src="js/editdistrict.js?ver=${resourceMap['js/editdistrict.js']}"></script>
  <!-- Optionally enable responsive features in IE8 -->
    <script type='text/javascript' src="js/bootstrap-slider.js"></script>
    <link rel="stylesheet" type="text/css" href="css/slider.css" />  
<script src="calender/js/jscal2.js"></script>
<script src="calender/js/lang/en.js"></script>
<link rel="stylesheet" type="text/css" href="calender/css/jscal2.css" />
<link rel="stylesheet" type="text/css" href="calender/css/border-radius.css" />
<link rel="stylesheet" type="text/css" href="calender/css/gold/gold.css" />
<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" />  
<style>
.table th, .table td {
    padding: 8px;
    line-height: 20px;
    text-align: left;
    vertical-align: top;  
}
.divwidth
{
float: left;
width: 40px;
}
.input-group-addon {
    background-color: #fff;
	border: none;
}
</style>
<script type="text/javascript">
	$( document ).ready(function() {	

		
		/******TPL-4823--- Add configuration option HireVue Integration for District start*******/
		if("${districtMaster.isHireVueIntegration}" == "true" && "${districtMaster.isHireVueIntegration}" != ""){
			$("#hireVueAPIKeydiv").show(); 
		}
		/******TPL-4823--- Add configuration option HireVue Integration for District end*******/
		$('#addkey').click(function() {
				if ($('#availableContactTypeAdduser option:selected').val() != null) {
					 $('#availableContactTypeAdduser option:selected').remove().appendTo('#attachContactTypeAdduser');
					 $("#availableContactTypeAdduser").attr('selectedIndex','-1').find("option:selected").removeAttr("selected");
			         $("#attachContactTypeAdduser").attr('selectedIndex', '-1').find("option:selected").removeAttr("selected");      
					 } 
			});
		$('#removeKey').click(function() {
			       if ($('#attachContactTypeAdduser option:selected').val() != null) {
			             $('#attachContactTypeAdduser option:selected').remove().appendTo('#availableContactTypeAdduser');
			            $("#attachContactTypeAdduser").attr('selectedIndex',  '-1').find("option:selected").removeAttr("selected");
			          $("#availableContactTypeAdduser").attr('selectedIndex', '-1').find("option:selected").removeAttr("selected");         
						}
			});
			});
	</script>	
<script type="text/javascript">
var $j=jQuery.noConflict();
        $j(document).ready(function() {
        });
        
function applyScrollOnTbl()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#districtSchoolsTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 280,
        width: 722,
        minWidth: null,
        minWidthAuto: false,
        colratio:[288,182,144,110], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}

function applyScrollOnTblDomain()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#domainTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 280,
        width: 600,
        minWidth: null,
        minWidthAuto: false,
        colratio:[500,100], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}

function applyScrollOnTblNotes()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#notesTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 280,
        width: 722,
        minWidth: null,
        minWidthAuto: false,
        colratio:[288,182,144,110], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}



 </script>  
<script type="text/javascript">

</script>
<style>
	.hide
	{
		display: none;
	}
	.inline
	{
		display: inline;
	}
	.nowrap
	{
		white-space: nowrap
	}
	.setSlider
	{
		margin-left: -20px; 
		margin-top: -8px;
	}
	
	.tooltip {
  position: absolute;
  z-index: 1030;
  display: block;
  font-size: 12px;
  line-height: 1.4;
  opacity: 0;
  filter: alpha(opacity=0);
  visibility: visible;
}

.tooltip.in {
  opacity: 0.9;
  filter: alpha(opacity=90);
}

.tooltip.top {
  padding: 5px 0;
  margin-top: -3px;
}

.tooltip.right {
  padding: 0 5px;
  margin-left: 3px;
}

.tooltip.bottom {
  padding: 5px 0;
  margin-top: 3px;
}

.tooltip.left {
  padding: 0 5px;
  margin-left: -3px;
}

.tooltip-inner {
  max-width: 200px;
  padding: 3px 8px;
  color: #ffffff;
  text-align: center;
  text-decoration: none;
  background-color: #000000;
  border-radius: 4px;
}

.tooltip-arrow {
  position: absolute;
  width: 0;
  height: 0;
  border-color: transparent;
  border-style: solid;
}

.tooltip.top .tooltip-arrow {
  bottom: 0;
  left: 50%;
  margin-left: -5px;
  border-top-color: #000000;
  border-width: 5px 5px 0;
}

.tooltip.top-left .tooltip-arrow {
  bottom: 0;
  left: 5px;
  border-top-color: #000000;
  border-width: 5px 5px 0;
}

.tooltip.top-right .tooltip-arrow {
  right: 5px;
  bottom: 0;
  border-top-color: #000000;
  border-width: 5px 5px 0;
}

.tooltip.right .tooltip-arrow {
  top: 50%;
  left: 0;
  margin-top: -5px;
  border-right-color: #000000;
  border-width: 5px 5px 5px 0;
}

.tooltip.left .tooltip-arrow {
  top: 50%;
  right: 0;
  margin-top: -5px;
  border-left-color: #000000;
  border-width: 5px 0 5px 5px;
}

.tooltip.bottom .tooltip-arrow {
  top: 0;
  left: 50%;
  margin-left: -5px;
  border-bottom-color: #000000;
  border-width: 0 5px 5px;
}

.tooltip.bottom-left .tooltip-arrow {
  top: 0;
  left: 5px;
  border-bottom-color: #000000;
  border-width: 0 5px 5px;
}

.tooltip.bottom-right .tooltip-arrow {
  top: 0;
  right: 5px;
  border-bottom-color: #000000;
  border-width: 0 5px 5px;
}
</style>
	<!--<h1>Welcome Gagan Pavan </h1><br/>
	<h3>Manage District</h3>-->
<form:form commandName="districtMaster"  method="post" onsubmit="return validateEditDistrict();"  enctype="multipart/form-data">
<div class="row" style="margin-left: 0px;margin-right: 0px;margin-top:">
         <div style="float: left;">
         	<img src="images/add-district.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;"><spring:message code="msgEditDistrict" /></div>	
         </div>
		
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
</div>

    <input type="hidden" name="headQuarterId" id="headQuarterId" value="${headQuarterId}"/>
    <input type="hidden" name="branchId" id="branchId" value="${branchId}"/>
	<div>
		<div class="mt10">
      		<div class="panel-group" id="accordion">
        		<div class="panel panel-default">
          		<div class="accordion-heading"> <a href="#collapseOne" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle minus"> <spring:message code="lnkGenInfo" /> </a> </div>
          		<div class="accordion-body in" id="collapseOne" style="height:auto;">
            	<div class="accordion-inner">
              	<div class="offset1">
              	<div class="row">
					<div class="col-sm-12 col-md-12">			                         
					 	<div class='required' id='errorlogoddiv'></div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6 col-md-6">
						<label><spring:message code="lblDistrictName" /></label>
						<form:input path="districtName" cssClass="form-control" maxlength="50" tabindex="-1" placeholder="" readonly="true" />
	                   </div>
                 </div>
				<div class="row">
					<div class="col-sm-6 col-md-6">
                   		<label><spring:message code="lblAdd" /></label>
						<form:input path="address" cssClass="form-control" maxlength="50" tabindex="-1" placeholder="" readonly="true"/>
                    </div>
				</div>
                  
				<div class="row">
                  	<div class="col-sm-2 col-md-2">
                 		<label><spring:message code="lblNCESDistID" /></label>
						<form:input path="districtId" cssClass="form-control" maxlength="50" tabindex="-1" placeholder="" readonly="true"/>
                  	</div>
                  	<div class="col-sm-2 col-md-2">
                    	<label><spring:message code="lblNCESStID" /></label>
						<form:input path="ncesStateId" cssClass="form-control" maxlength="50" tabindex="-1" placeholder="" readonly="true"/>
                  	</div>
                  	<div class="col-sm-2 col-md-2">
                    	<label><spring:message code="lblZepCode" /></label>
						<form:input path="zipCode" cssClass="form-control" maxlength="50" tabindex="-1" placeholder="" readonly="true"/>
                  	</div>
				</div>
				
                <div  class="row">
                	<div class="col-sm-3 col-md-3">
                    	<label><spring:message code="lblSt" /></label>
                    	<input type="text" class="form-control" value="${districtMaster.stateId.stateName }" tabindex="-1" readonly="true"/>
                  </div>
                  <div class="col-sm-3 col-md-3">
                    	<label><spring:message code="lblCity" /></label>
                    	<form:input path="cityName" cssClass="form-control" maxlength="50" tabindex="-1" placeholder="" readonly="true"/>
                  </div>
                
                </div>
                  
                  <div class="row">
                    <div class="col-sm-2 col-md-2">
                      	<label><spring:message code="lblPhone" /></label>
						<form:input path="phoneNumber" cssClass="form-control" maxlength="20" tabindex="-1" placeholder="" readonly="true"/>
                    </div>
                    <div class="col-sm-2 col-md-2">
                      	<label><spring:message code="lblAltPh" /></label>
						<form:input path="alternatePhone" cssClass="form-control" maxlength="20" placeholder=""/>
                    </div>
                    <div class="col-sm-2 col-md-2">
                      	<label><spring:message code="lblFax" /></label>
						<form:input path="faxNumber" cssClass="form-control" maxlength="20" placeholder=""/>
                    </div>
                  </div>
                  
                  <div class="row">
                  	<div class="col-sm-6 col-md-6">
                      	<label><spring:message code="lblWebUrl" /></label>
						<form:input path="website" cssClass="form-control" maxlength="100" placeholder=""/>
                    </div>
                  </div>
                 
                  
                  <div class="row">
                   
                    <div class="col-sm-2 col-md-2">
                      <label><spring:message code="msgTtlSchls" /> <a href="#"  id="iconpophover6" rel="tooltip"   data-original-title='<spring:message code="msgeditdistrict1" />'><img src="images/qua-icon.png" width="15" height="15" alt=""></a></label>
						<form:input path="totalNoOfSchools" cssClass="form-control" maxlength="50" tabindex="-1" placeholder="" readonly="true"/>
                    </div>
                   
                    <div class="col-sm-2 col-md-2">
                      <label><spring:message code="lblToOfTech" /> <a href="#" id="iconpophover7" rel="tooltip" data-original-title='<spring:message code="msgeditdistrict2" />'><img src="images/qua-icon.png" width="15" height="15" alt=""></a></label>
						<form:input path="totalNoOfTeachers" cssClass="form-control" maxlength="50" tabindex="-1" placeholder="" readonly="true"/>
                    </div>
                    <div class="col-sm-2 col-md-2">
                      <label><spring:message code="lblToOfStu" /> <a href="#" id="iconpophover10" rel="tooltip" data-original-title="<spring:message code="msgeditdistrict3" />"><img src="images/qua-icon.png" width="15" height="15" alt=""></a></label>
						<form:input path="totalNoOfStudents" cssClass="form-control" maxlength="50" tabindex="-1" placeholder="" readonly="true"/>
                    </div>
                  </div>
                  
                  <div class="row">
                    <div class="col-sm-6 col-md-6">
                     	 <label><spring:message code="lblDplyN" />&nbsp;<a href="#" id="iconpophover1" rel="tooltip" data-original-title="<spring:message code="msgeditdistrict4" />"><img src="images/qua-icon.png" width="15" height="15" alt=""></a></label>
						<form:input path="displayName" cssClass="form-control" maxlength="50" placeholder=""/>
                    </div>                  
                  </div>
                  
                  <div class="row">
                  	 <div class="col-sm-6 col-md-6">
                      	<label><spring:message code="lblChar" /> <a href="#" id="iconpophover3" rel="tooltip" data-original-title="<spring:message code="msgeditdistrict5" />"><img src="images/qua-icon.png" width="15" height="15" alt=""></a></label>
                    <br/><spring:message code="lblRegion" /> : ${districtMaster.stateId.regionId.regionName}
                    </div>                    
                  </div>
             
                 <div  class="row">
					<div class="col-sm-2 col-md-2">
						<label><spring:message code="lblLg" /> <a href="#" id="iconpophover2" rel="tooltip" data-original-title="<spring:message code="msgLogoForDist" />"><img src="images/qua-icon.png" width="15" height="15" alt=""></a></label>    
						 <c:if test="${fn:indexOf(roleAccessGI,'|9|')!=-1}">
						<form:input path="logoPathFile" type="file"/>
						<!-- <a href="#" onclick="return clearFile(1)">Clear</a> -->
						</c:if>
				 	</div>
                   	<div class="col-sm-4 col-md-4">
                   		<label>&nbsp;</label><span id="showLogo">&nbsp;</span>
                   	</div>
                </div>
                  
					<div class="row">
                    	<div class="col-sm-9 col-md-9" id="dDescription">
                      	<label><spring:message code="lblDescription1" /></label>
                      	<label class="redtextmsg"> <spring:message code="msgHowToCopyPastCutDouc" /> </label>
                      	<form:textarea path="description" rows="3" maxlength="1500" onkeyup="return chkLenOfTextArea(this,1500);" onblur="return chkLenOfTextArea(this,1500);" class="form-control"/>
                    	</div>
                  	</div>
                  	<div class="row top5">
                    	<div class="col-sm-9 col-md-9">
                      	<p> <spring:message code="msgeditdistrict6" /> <a href="#" id="iconpophover9" rel="tooltip" data-original-title="<spring:message code='msgeditdistrict7' />"><img src="images/qua-icon.png" width="15" height="15" alt=""></a></p>
                    	</div>
                  </div>
				<div class="row">
					<div class="col-sm-1 col-md-1">
						<label class="radio">
                        
                        <%-- <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked="">--%>
                        <form:radiobutton path="canTMApproach" value="1" />
                        <spring:message code="optY" />
                        </label>
                     </div>
                     <div class="col-sm-1 col-md-1 left20-sm2">
                      	<label class="radio">
                        <form:radiobutton path="canTMApproach" value="0" />
                        <%-- <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked="">--%>
                        <spring:message code="optN" /> </label>
                    </div>
                  </div>
              </div>
              <div class="clearfix"></div>
            </div>
          </div>
        </div>
		<div class="panel panel-default">
			<div class="accordion-heading"> <a href="#collapseTwo" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle plus"> <spring:message code="headContInfo" /> </a> </div>
          	<div class="accordion-body collapse" id="collapseTwo" style="height: 0px;">
           
           	<div class="accordion-inner">
				<div class="span14 offset1">
                <div class="row">
<!--                  <form>-->
						<div class="col-sm-12 col-md-12">			                         
						 	<div class='required' id='errorcontactinfodiv'></div>
						 	<div class='required' id='errorpwddiv'></div>
						</div>
						<div class="col-sm-12 col-md-12">			                         
						 	<div class='required' id='errorkeydiv'></div>
						</div>
						<c:set var="disableDmFields" value=""></c:set>					
						<c:if test="${districtMaster.dmName ne '' && districtMaster.dmPassword ne ''}">
				         	<c:set var="disableDmFields" value="true"></c:set>	 
				         	<c:set var="dmPwdValue" value=""></c:set>	         			
						</c:if>
					<div class="col-sm-4 col-md-4">
						<label><spring:message code="lblFnlDecMkrN" /><span class="required">*</span> <a href="#" id="iconpophover4" data-toggle="tooltip"  data-placement="right" data-original-title="<spring:message code="msgeditdistrict8" />"><img src="images/qua-icon.png" width="15" height="15" alt=""></a></label>
                      	<%-- <input type="text" placeholder="" class=" span5">--%>
                      	<form:input path="dmName" cssClass="form-control" maxlength="50" readonly="${disableDmFields}" placeholder=""/>
                    </div>
                    <div class="col-sm-3 col-md-3">
                      	<label><spring:message code="lblEmail" /><span class="required">*</span></label>
                      	<%-- <input type="text" placeholder="" class=" span5">--%>
                      	<form:input path="dmEmailAddress" cssClass="form-control" maxlength="100" readonly="${disableDmFields}" placeholder=""/>
                    </div>
                    <div class="col-sm-2 col-md-2">
                      	<label><spring:message code="headPhone" /></label>
                      	<%-- <input type="text" placeholder="" class=" span5">--%>
                      	<form:input path="dmPhoneNumber" cssClass="form-control" maxlength="20" readonly="${disableDmFields}" placeholder=""/>
                    </div>
                    <c:if test="${districtMaster.dmName ne '' && districtMaster.dmPassword ne ''}">
					<c:set var="hideDmPwd" value="hide"></c:set>
					</c:if>
	                    <div id="dmPwdDiv" class="col-sm-2 col-md-2 ${hideDmPwd} }">
	                      	<label><spring:message code="lblPass" /><span class="required">*</span></label>
	                      	<%-- <input type="text" placeholder="" class=" span5">--%>
	                      	<form:input path="dmPassword" type="password" cssClass="form-control" maxlength="20" placeholder=""/>
	                      	<%--<form:password path="dmPassword" cssClass="span2" maxlength="20" showPassword="false" />--%>
	                    </div>
					<c:if test="${districtMaster.dmName ne '' && districtMaster.dmPassword ne ''}">
	                 <div id="cnfmPwdDiv" class="col-sm-2 col-md-2">
                      	<label><spring:message code="lblPass" /></label>
                      	 <input type="password" id="cnfmPassword" name="cnfmPassword" class="form-control" maxlength="20" placeholder=""/>
<!--                      	 <button type="button" id="btnDm" name="btnDm" class="btn btn-large btn-primary" onclick="editDmFields()"><strong>Go<i class="icon"></i></strong></button>-->
                    </div>
                     <div class="col-sm-1 col-md-1" style="margin-left:-10px;">
	                  	 <label>&nbsp;</label>
	                  	  <c:if test="${fn:indexOf(roleAccessCI,'|13|')!=-1}">
	                      	 <button type="button" id="btnDm" name="btnDm" class="btn btn-primary" onclick="editDmFields()"><strong>Go</strong></button>
	                      </c:if>
	                    </div>
					</c:if>
                   <input type="hidden" id="entityTypeOfSession" name="entityTypeOfSession" class="span2" maxlength="20" value="${userSession.entityType}" placeholder=""/>
                </div>
                
				<div class="row">
					<div class="col-sm-4 col-md-4">
                    	<label><spring:message code="lblStuAssCoord" /><!--  <span class="required">*</span> --> &nbsp;  <a href="#" id="iconpophover5" rel="tooltip" data-original-title="<spring:message code="msgeditdistrict9" />"><img src="images/qua-icon.png" width="15" height="15" alt=""></a></label>
						<form:input path="acName" cssClass="form-control" maxlength="50"/>
                  	</div>
                  	<div class="col-sm-3 col-md-3">
                    	<label><spring:message code="lblEmail" /></label>
						<form:input path="acEmailAddress" cssClass="form-control" maxlength="100" placeholder=""/>
                  	</div>
                  	<div class="col-sm-2 col-md-2">
                    	<label><spring:message code="headPhone" /></label>
						<form:input path="acPhoneNumber" cssClass="form-control" maxlength="20" placeholder=""/>
                  	</div>                  	
                </div>
               
                <div class="row">
                		<%-- ========= This Div Will Readonly Mode Excepting Entity Type 1====== And In Edit Mode For TeacherMatch Admin  --%>
                		<c:set var="disableAccountManagerFields" value=""></c:set>					
						<c:if test="${userSession.entityType ne 1}">
				         	<c:set var="disableAccountManagerFields" value="true"></c:set>	 
						</c:if>
					<div class="col-sm-4 col-md-4">
                    	<label><spring:message code="lblAccMngFrTm" /><span class="required">*</span> <a href="#" id="iconpophover11" rel="tooltip" data-original-title="<spring:message code="msgTheindividualfromTeacherMatch"/>"><img src="images/qua-icon.png" width="15" height="15" alt=""></a></label>
						<form:input path="amName" cssClass="form-control" maxlength="50" readonly="${disableAccountManagerFields}" placeholder=""/>
                  	</div>
                  	<div class="col-sm-3 col-md-3">
                    	<label><spring:message code="lblEmail1" /></label>
						<form:input path="amEmailAddress" cssClass="form-control" maxlength="100" readonly="${disableAccountManagerFields}" placeholder=""/>
                  	</div>
                  	<div class="col-sm-2 col-md-2">
                    	<label><spring:message code="headPhone" /></label>
						<form:input path="amPhoneNumber" cssClass="form-control" maxlength="20" readonly="${disableAccountManagerFields}" placeholder=""/>
                  	</div>                  	
                </div>
                 <c:if test="${userSession!=null && userSession.entityType eq 1 || (userSession.entityType eq 2 && userSession.roleId.roleId eq 7)}"> 
		             <div class="row">                               
	                      <div class="col-sm-11 col-md-11"><spring:message code="lblOthrKeCont" />
	                      <div class="pull-right">
	                       <c:if test="${fn:indexOf(roleAccessCI,'|1|')!=-1}">
	                      <a href="javascript:void(0);"  onclick="showKeyContact();"><spring:message code="lnkAddKyContact" /></a>
	                      </c:if>
	                      </div>
	                    </div>                 
	                </div>
               </c:if>
                
                <div class="row">
                  <div  class="col-sm-11 col-md-11 mt08">
                    <table id="keyContactTable"  width="100%" border="0" class="table table-bordered">
                      <thead class="bg">
                        <tr>
                          <th width="20%"><spring:message code="lblContTy" /></th>
                          <th width="20%"><spring:message code="lblContN" /></th>
                          <th width="15%"><spring:message code="lblEmail1" /></th>
                          <th width="15%"><spring:message code="headPhone" /></th>
                          <th width="15%"><spring:message code="lblTitle" /></th>
                          <th width="15%"><spring:message code="lblAct" /></th>
                        </tr>
                      </thead>
                    </table>
                  </div>
                </div>
                
                <div id="addKeyContactDiv"  class="hide">
                <div class="row">
                    <div class="col-sm-4 col-md-4">
                      <label><spring:message code="lblContTy" /> </label>
                      <select class="form-control" id="dropContactType" name="dropContactType">
                        <c:forEach items="${contactTypeMaster}" var="ctm">
							<c:set var="selected" value=""></c:set>					
			        		<c:if test="${st.stateId eq ctm.contactTypeId}">	        			
			         			<c:set var="selected" value="selected"></c:set>	         			
			        		</c:if>	
							<option id="${ctm.contactTypeId}" value="${ctm.contactTypeId}" $selected >${ctm.contactType}</option>
						</c:forEach>
                      </select>
                      <input type="hidden" id="keyContactId" name="keyContactId" placeholder="" class=" span3">
                    </div>
                    <div class="col-sm-4 col-md-4">
                    <label><spring:message code="lblContFName" /><span class="required">*</span> </label>
                    <input type="text" id="keyContactFirstName" name="keyContactFirstName" maxlength="50" placeholder="" class="form-control">
                  </div>
                   <div class="col-sm-3 col-md-3">
                    <label><spring:message code="lblContLName" /><span class="required">*</span> </label>
                    <input type="text" id="keyContactLastName" name="keyContactLastName" maxlength="50" placeholder="" class="form-control">
                  </div>
                  </div>
                  
                  <div class="row">
                  <div class="col-sm-4 col-md-4">
                    <label><spring:message code="lblContEmail" /><span class="required">*</span> </label>
                    <input type="text" id="keyContactEmailAddress" name="keyContactEmailAddress"  maxlength="100" placeholder="" class="form-control">
                  </div>
                  <div class="col-sm-4 col-md-4">
                    <label><spring:message code="headPhone" /></label>
                    <input type="text" id="keyContactPhoneNumber" name="keyContactPhoneNumber"   maxlength="20" placeholder="" class="form-control">
                  </div>
                    <div class="col-sm-3 col-md-3">
                      <label><spring:message code="lblTitle" /></label>
                      <input type="text"  id="keyContactTitle" name="keyContactTitle"  maxlength="25" class="form-control">
                    </div>
                </div>
                
                <div class="row">
                  <div class="col-sm-5 col-md-5 idone mt10"><a href="javascript:void(0);" onclick="return addKeyContact();"><spring:message code="lnkImD" /></a>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onclick="clearKeyContact();"><spring:message code="lnkCancel" /></a></div>
                </div>
                
                 </div>
              </div>
              <div class="clearfix">&nbsp;</div>
            </div>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="accordion-heading"> <a href="#collapseThree" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle plus"> <spring:message code="lnkUsr" /> </a> </div>
          <div class="accordion-body collapse" id="collapseThree" style="height: 0px;">
            <div class="accordion-inner">
              <div class="offset1">
               <div class="row">
	                <div class="col-sm-9 col-md-9">
	              		<div id="errordistrictdiv" class="required"></div>
	                </div>
                </div>
                <div class="row">
                  <div class="col-sm-10 col-md-10"><label><spring:message code="lblAdmin" /></label>
	                  <div class="pull-right">
	                   <c:if test="${fn:indexOf(roleAccessU,'|1|')!=-1}">
	                   <a href="javascript:void(0);" onclick="addAdministrator(2);"><spring:message code="lblAddAmin" /></a>
	                   </c:if>
	                   </div>
                   </div>
                </div>
                
                 <div class="row col-sm-10 col-md-10">            
                    <div id="districtAdministratorDiv" class="row">
                      <div class="col-sm-3 col-md-3"><spring:message code="distAdmin1" /> <a href="#"><img width="15" height="15" class="can" src="images/can-icon.png" alt=""></a></div>
                      <div class="col-sm-3 col-md-3"><spring:message code="distAdmin2" /> <a href="#"><img width="15" height="15" class="can" src="images/can-icon.png" alt=""></a></div>
                      <div class="col-sm-3 col-md-3"><spring:message code="distAdmin3" /> <a href="#"><img width="15" height="15" class="can" src="images/can-icon.png" alt=""></a></div>
                      <div class="col-sm-3 col-md-3"><spring:message code="distAdmin1" /> <a href="#"><img width="15" height="15" class="can" src="images/can-icon.png" alt=""></a></div>                  
                      </div>
                 </div>
                
                <div class="row">
                  <div class="col-sm-10 col-md-10 top20"><label><spring:message code="lblAlys" /></label>
	                  <div class="pull-right"> 
	                   <c:if test="${fn:indexOf(roleAccessU,'|1|')!=-1}">
	                  <a href="javascript:void(0);" onclick="addAdministrator(5);"><spring:message code="lnkAddAlys" /></a>
	                  </c:if>
	                  </div>
                  </div>
                </div>
                
                <div class="row col-sm-10 col-md-10">                
                    <div id="districtAnalystDiv"  class="row">
                      <div class="col-sm-3 col-md-3"><spring:message code="distAdmin1" /> <a href="#"><img width="15" height="15" class="can" src="images/can-icon.png" alt=""></a></div>
                      <div class="col-sm-3 col-md-3"><spring:message code="distAdmin2" /> <a href="#"><img width="15" height="15" class="can" src="images/can-icon.png" alt=""></a></div>
                      <div class="col-sm-3 col-md-3"><spring:message code="distAdmin3" /> <a href="#"><img width="15" height="15" class="can" src="images/can-icon.png" alt=""></a></div>
                      <div class="col-sm-3 col-md-3"><spring:message code="distAdmin1" /> <a href="#"><img width="15" height="15" class="can" src="images/can-icon.png" alt=""></a></div>                  
                  </div>
                </div>

                
                <!-- Add Administrator Elements :  -->
                <div id="addAdministratorDiv" class="hide" style="padding-top: 8px;">
                <div class="row col-sm-10 col-md-10">
                <div class="row">
					<div class="col-sm-3 col-md-3">
						<label><strong><spring:message code="lblSalutation" /></strong></label>
						<select class="form-control" id="salutation" name="salutation">
							<option value="1"><spring:message code="optMr" /></option>
							<option value="2"><spring:message code="optMs" /></option>
							<option value="3"><spring:message code="optMrs" /></option>
							<option value="4"><spring:message code="optDr" /></option>
						</select>
					</div>
					<div class="col-sm-3 col-md-3">
                    	<label><strong><spring:message code="lblFname" /></strong><span class="required">*</span></label>
                    	 <input class="form-control" type="hidden" id="roleId" name="roleId" value="" placeholder="">
                    	 <input class="form-control" type="text" id="firstName" name="firstName"  maxlength="50"placeholder="">
                    	<%-- <form:input type="hidden" path="userId" id="userId" />
                    	<form:input path="firstName" cssClass="span3" maxlength="50" placeholder=""/>--%>
					</div>
					<div class="col-sm-3 col-md-3">
                    	<label><strong><spring:message code="lblLname" /></strong><span class="required">*</span></label>
                    	 <input class="form-control" type="text" id="lastName" name="lastName" maxlength="50" placeholder="">
					</div>
					</div>
				</div>
				
                <div class="row col-sm-10 col-md-10">
                   <div class="row">
					<div class="col-sm-9 col-md-9">
                    	<label><strong><spring:message code="lblTitle" /></strong></label>
                    	<input class="form-control" type="text" id="title" name="title" maxlength="200" placeholder="">
                  </div>
                  </div>
                </div>
<!--hafeez-->
<input type="hidden" id="roleIdUserLogin" name="roleIdUserLogin" value="${ userSession.roleId.roleId}">						
		 <c:if test="${ userSession.roleId.roleName eq 'TM Admin' || ( userSession.roleId.roleName eq 'TM Analyst')|| ( userSession.roleId.roleName eq 'Operations Administrator')}">
                   
                <div class="row col-sm-10 col-md-10">
                   <div class="row">
                   <div class="col-sm-9 col-md-9">
                     <label><spring:message code="keyContactType" /> </label>
                   </div>
                    </div>
                     <div class="row">
                      <div class="col-sm-4 col-md-4 ">
                    	<label><strong><spring:message code="availablekeyContact"/></strong></label>
                      <select multiple class="form-control" id="availableContactTypeAdduser" name="availableContactTypeAdduser" style="height: 150px;">
                        <c:forEach items="${contactTypeMaster}" var="ctm">
							<option id="${ctm.contactTypeId}" value="${ctm.contactTypeId}" >${ctm.contactType}</option>
						</c:forEach>
                      </select>
                  	</div>
                  	<div class="col-sm-1 col-md-1 left20-sm"> 
				<div class="span2"> <span id="addkey" style="cursor:pointer;"><img alt="" src="images/rightarrow.jpg" width="30px"> </span></div>
				<div class="span2"> <span id="removeKey" style="cursor:pointer;"><img alt="" src="images/leftarrow.jpg" width="30px"> </span></div>
			</div>
			<div class="col-sm-4 col-md-4 ">
                    	<label><strong><spring:message code="attachedkeyContact"/></strong></label>
                      <select multiple class="form-control" id="attachContactTypeAdduser" name="attachContactTypeAdduser" style="height: 150px;">

<!--                        <c:forEach items="${attachContactType}" var="ctm">				-->
<!--							<option id="${ctm.contactTypeId}" value="${ctm.contactTypeId}" >${ctm.contactType}</option>-->
<!--						</c:forEach>-->
                      </select>
                  	</div>
                     </div>
<!--					<div class="col-sm-9 col-md-9">-->
<!--					 <label>Key Contact Type</label>-->
<!--                      <select class="form-control" id="dropContactTypeAsAdminitratorOrAnalyst" name="dropContactTypeAsAdminitratorOrAnalyst">-->
<!--                      <option id="0" value="0" $selected >Select Contact Type</option>-->
<!--                        <c:forEach items="${contactTypeMaster}" var="ctm">-->
<!--							<c:set var="selected" value=""></c:set>					-->
<!--			        		<c:if test="${st.stateId eq ctm.contactTypeId}">	        			-->
<!--			         			<c:set var="selected" value="selected"></c:set>	         			-->
<!--			        		</c:if>	-->
<!--							<option id="${ctm.contactTypeId}" value="${ctm.contactTypeId}" $selected >${ctm.contactType}</option>-->
<!--						</c:forEach>-->
<!--                      </select>-->
<!--                      -->
<!--                  </div>-->
               
                </div>
                </c:if>
                
    <!--                hafeez-->  
             
                <div class="row col-sm-10 col-md-10">
                   <div class="row">
					<div class="col-sm-9 col-md-9">
                    	<label><strong><spring:message code="lblEmail1" /></strong><span class="required">*</span></label>
                    	 <input class="form-control" type="text" id="emailAddress" name="emailAddress" maxlength="75" placeholder="">
                  </div>
                </div>
                </div>
                
                <div class="row col-sm-10 col-md-10">
                   <div class="row">
					<div class="col-sm-5 col-md-5">
                    	<label><strong><spring:message code="headPhone" /></strong></label>
                    	<input class="form-control" type="text" id="phone" name="phone" maxlength="20" placeholder="">
                  	</div>
                  	<div class="col-sm-4 col-md-4">
                    	<label><strong><spring:message code="lblMobile" /></strong></label>
                    	<input class="form-control" type="text" id="mobileNumber" name="mobileNumber" maxlength="20" placeholder="">
                  	</div>
                </div>
                </div>
                
                <div class="row">
                  <div class="col-sm-3 col-md-3 idone top10"><a href="javascript:void(0);" onclick="validateDistrictAdministratorOrAnalyst();"> <spring:message code="lnkImD" /> </a>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onclick="clearUser();"><spring:message code="lnkCancel" /></a></div>
                </div>
                
            </div><%-- End of addAdministratorDiv --%>
                
                
                
              </div>
              <div class="clearfix">&nbsp;</div>
            </div>
          </div>
        </div>
        
        <%-- ================ Gagan : Adding accordion Div for Social ======================  --%>
	   <div class="panel panel-default">
          <div class="accordion-heading"> <a href="#collapseFive" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle plus"> <spring:message code="msgSocial1" /> </a> </div>
          <div class="accordion-body collapse" id="collapseFive" style="height: 0px;">
            <div class="accordion-inner">
              <div class="offset1">
<%----------------- Gagan : Social offset Div -----------------------------  --%>
              <div class="row col-sm-12 col-md-12">
               <div id="errordistrictdiv" class="required"></div>
              </div>
                             
                   <div class="row mt10">
                  		<div class="col-sm-12 col-md-12">
	                       <spring:message code="lblSolNetPost" />
					  	</div>
                  	</div>
                  
                  <div class="row">
	                  <div class="col-sm-12 col-md-12">
	                        <label class="checkbox" style="margin-bottom: 0px;">
							 <form:checkbox path="postingOnDistrictWall" id="postingOnDistrictWall"  value="1"/>
			                <spring:message code="msgSocial2" /> <a href="#" id="iconpophover12" rel="tooltip" data-original-title="<spring:message code="msgTheindividualfromTeacherMatch1"/>"><img src="images/qua-icon.png" width="15" height="15" alt=""></a> 
							</label>
					  </div>
                  </div>
                  
                  <div class="row">
	                  <div class="col-sm-12 col-md-12">
	                        <label class="checkbox">
	                        <form:checkbox path="postingOnTMWall" id="postingOnTMWall"  disabled="${disableForTm}" value="1"/>
							<spring:message code="msgSocial3" />

							</label>
					   </div>
                   </div>
                
                
              </div>
              <div class="clearfix">&nbsp;</div>
            </div>
          </div>
        </div>    
        
         <%-- ================ Gagan : Adding accordion Div for Social ======================  --%>
	   <div class="panel panel-default">
          <div class="accordion-heading"> <a href="#collapseSix" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle plus"><spring:message code="lblMos" /> </a> </div>
          <div class="accordion-body collapse" id="collapseSix" style="height: 0px;">
            <div class="accordion-inner">
              <div class="offset1">
<%----------------- Gagan : Mosaic offset Div collapseSix -----------------------------  --%>
              <div class="row col-sm-12 col-md-12">
	              <div id="errordistrictdiv" class="required"></div>
	               <div class='required' id='errormosaicinfodiv'></div>
			   </div> 		
                  	<div class="row mt10">
	                  	<div class="col-sm-4 col-md-4">
							<label><spring:message code="lblDistributionEmail" /></label>
	                      	<form:input path="distributionEmail" cssClass="form-control" maxlength="100" />
	                    </div>
                  	</div>
					
					<div class="row mt10">
                  			<div class="col-sm-4 col-md-4">
                    		<spring:message code="lblCanDFeeCret" /> <a href="#" id="iconpophover13" rel="tooltip" data-original-title="<spring:message code="lblSetCriteriatoView" />" data-placement="right"><img src="images/qua-icon.png" width="15" height="15" alt=""></a>
                  		</div>
                  	</div>

                  <div class="row mt10"> 
                  <fmt:bundle basename="resource"><fmt:message key="sliderMaxLength" var="sliderMaxLength" /></fmt:bundle>
                  <fmt:bundle basename="resource"><fmt:message key="sliderRationMaxLength" var="sliderRationMaxLength" /></fmt:bundle>
                  
             <%--------------------------- Slider Candidate Feed Functionality Check Start here ------------------------------------ --%>
                 <c:choose>
				    <c:when test="${empty districtMaster.candidateFeedNormScore}">
				       <fmt:bundle basename="resource"><fmt:message key="defaultcandidateFeedNormScore" var="candidateFeedNormScoreValue" /></fmt:bundle>
				    </c:when>
				    <c:otherwise>
				        <c:set var="candidateFeedNormScoreValue" value="${districtMaster.candidateFeedNormScore}"></c:set>
				    </c:otherwise>
				</c:choose>
				
				<c:choose>
				    <c:when test="${empty districtMaster.candidateFeedDaysOfNoActivity}">
				       <fmt:bundle basename="resource"><fmt:message key="defaultcandidateFeedDaysOfNoActivity" var="candidateFeedDaysOfNoActivityValue" /></fmt:bundle>
				    </c:when>
				    <c:otherwise>
				        <c:set var="candidateFeedDaysOfNoActivityValue" value="${districtMaster.candidateFeedDaysOfNoActivity}"></c:set>
				    </c:otherwise>
				</c:choose>
				
				
			 <%--------------------------- Slider Functionality Check ENDDDDDDDDDDD here ------------------------------------ --%>
				
          <%--         GAgan     <c:out value="${candidateFeedNormScoreValue } "></c:out> --%> 
                  
                  
                  			<div class="col-sm-12 col-md-12">
								<label> <spring:message code="msgPostCandActInMos" /></label>
							</div>
                  
							<div class="col-sm-4 col-md-4" style="max-width: 250px;">
								<label>  <spring:message code="lblCondNoScrHi" />   </label>
							</div>
							<div class="col-sm-8 col-md-8" style=" margin-top: -8px;"> 
								<label> 
									<iframe id="ifrm1"  src="slideract.do?name=slider1&tickInterval=10&max=${sliderMaxLength }&swidth=200&svalue=${candidateFeedNormScoreValue}" scrolling="no" frameBorder="0" style="border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:280px;"></iframe>
									<form:hidden cssClass="span2" path="candidateFeedNormScore" value="${candidateFeedNormScoreValue }"/>
								</label>
							</div>
							<div class="clearfix"></div>
							<div class="col-sm-12 col-md-12">
								<label>  <spring:message code="lbl&" />  </label>
							</div>
							
							<div class="col-sm-6 col-md-6" style="max-width: 390px;">
								<label class="radio">
								
								<input type="hidden" id="mosaicRadiosFlag" name="mosaicRadiosFlag" value="1">
								<input type="radio"  name="mosaicRadios" id="mosaicRadios1" value="1" <c:if test="${districtMaster.candidateFeedDaysOfNoActivity ge 0}">checked </c:if> onclick="disableSlider()">
									<%--<form:radiobutton path="noSchoolUnderContract"  value="1" />--%>
			                        <spring:message code="msgCommWithCand" /> 
		                        </label>
							</div>
							
							<div class="col-sm-3 col-md-3" style="min-width: 240px;">
								<label> 
									<c:set var="dsliderValueWhendbValue0" value="1"></c:set>
											<c:if test="${candidateFeedDaysOfNoActivityValue eq 0}">
												<c:set var="dsliderValueWhendbValue0" value="0"></c:set>
											</c:if>
								<iframe id="ifrm2"  src="slideract.do?name=slider2&tickInterval=10&max=${sliderMaxLength }&swidth=200&svalue=${candidateFeedDaysOfNoActivityValue}&dslider=${dsliderValueWhendbValue0}" scrolling="no" frameBorder="0" style="border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:280px;"></iframe>
									<form:hidden cssClass="span2" path="candidateFeedDaysOfNoActivity" value="${candidateFeedDaysOfNoActivityValue }"/>
								</label>
							
						    </div>
							<div class="col-sm-2 col-md-2" style="min-width: 140px;">
								<label>&nbsp;&nbsp; <spring:message code="lblDyApp" /></label>
							</div>
							<div style="clear: both;"></div>
							<div class="col-sm-12 col-md-12" style="margin-top: -10px;">
								<label class="radio">
								<input type="radio"  name="mosaicRadios" id="mosaicRadios2" value="0" <c:if test="${districtMaster.candidateFeedDaysOfNoActivity eq 0}">checked </c:if> onclick="disableSlider()" >
									<%--<form:radiobutton path="noSchoolUnderContract"  value="1" />--%>
			                        <spring:message code="lblTheyHvApp" />
		                        </label>
							</div>
							
							<div class="col-sm-12 col-md-12">
					 			<label class="checkbox">
								<%-- <form:checkbox path="postingOnDistrictWall" id="postingOnDistrictWall"  value="1"/> --%>
			                 	<form:checkbox path="canSchoolOverrideCandidateFeed"/>	<spring:message code="lblSchoolOverrideSet" />
 
								</label>
							</div>				   
                   </div>
                  
                  <div class="row">
                  		<div class="col-sm-12 col-md-12">
                    		<spring:message code="lblJoFeeCrit" /> <a href="#" id="iconpophover14" rel="tooltip" data-original-title="<spring:message code="lblSetCriteriatoViewJobs" />"><img src="images/qua-icon.png" width="15" height="15" alt=""></a>
                  		</div>
                  </div>
                  
                  
                  <div class="row">
                  
                    <%--------------------------- Slider Jobbbbbbb Feed Functionality Check Start here ------------------------------------ --%>
                 <c:choose>
				    <c:when test="${empty districtMaster.jobFeedCriticalJobActiveDays}">
				       <fmt:bundle basename="resource"><fmt:message key="defaultjobFeedCriticalJobActiveDays" var="jobFeedCriticalJobActiveDaysValue" /></fmt:bundle>
				    </c:when>
				    <c:otherwise>
				        <c:set var="jobFeedCriticalJobActiveDaysValue" value="${districtMaster.jobFeedCriticalJobActiveDays}"></c:set>
				    </c:otherwise>
				</c:choose>
				
				<c:choose>
				    <c:when test="${empty districtMaster.jobFeedCriticalCandidateRatio}">
				       <fmt:bundle basename="resource"><fmt:message key="defaultjobFeedCriticalCandidateRatio" var="jobFeedCriticalCandidateRatioValue" /></fmt:bundle>
				    </c:when>
				    <c:otherwise>
				        <c:set var="jobFeedCriticalCandidateRatioValue" value="${districtMaster.jobFeedCriticalCandidateRatio}"></c:set>
				    </c:otherwise>
				</c:choose>
				
				<c:choose>
				    <c:when test="${empty districtMaster.jobFeedCriticalNormScore}">
				       <fmt:bundle basename="resource"><fmt:message key="defaultjobFeedCriticalNormScore" var="jobFeedCriticalNormScoreValue" /></fmt:bundle>
				    </c:when>
				    <c:otherwise>
				        <c:set var="jobFeedCriticalNormScoreValue" value="${districtMaster.jobFeedCriticalNormScore}"></c:set>
				    </c:otherwise>
				</c:choose>
				
				<c:choose>
				    <c:when test="${empty districtMaster.lblCriticialJobName}">
				       <c:set var="lblCriticialJobNameValue" value="Critical"></c:set>
				    </c:when>
				    <c:otherwise>
				        <c:set var="lblCriticialJobNameValue" value="${districtMaster.lblCriticialJobName}"></c:set>
				    </c:otherwise>
				</c:choose>
				
				 <c:choose>
				    <c:when test="${empty districtMaster.jobFeedAttentionJobActiveDays}">
				       <fmt:bundle basename="resource"><fmt:message key="defaultjobFeedAttentionJobActiveDays" var="jobFeedAttentionJobActiveDaysValue" /></fmt:bundle>
				    </c:when>
				    <c:otherwise>
				        <c:set var="jobFeedAttentionJobActiveDaysValue" value="${districtMaster.jobFeedAttentionJobActiveDays}"></c:set>
				    </c:otherwise>
				</c:choose>
				
				<c:choose>
				    <c:when test="${empty districtMaster.jobFeedAttentionNormScore}">
				       <fmt:bundle basename="resource"><fmt:message key="defaultjobFeedAttentionNormScore" var="jobFeedAttentionNormScoreValue" /></fmt:bundle>
				    </c:when>
				    <c:otherwise>
				        <c:set var="jobFeedAttentionNormScoreValue" value="${districtMaster.jobFeedAttentionNormScore}"></c:set>
				    </c:otherwise>
				</c:choose>
				
				<c:choose>
				    <c:when test="${empty districtMaster.lblAttentionJobName}">
				        <c:set var="lblAttentionJobNameValue" value="Attention"></c:set>
				    </c:when>
				    <c:otherwise>
				        <c:set var="lblAttentionJobNameValue" value="${districtMaster.lblAttentionJobName}"></c:set>
				    </c:otherwise>
				</c:choose>
							
			 <%--------------------------- Slider Functionality Check ENDDDDDDDDDDD here ------------------------------------ --%>
							<div class="col-sm-4 col-md-4 top15">							
								<label><spring:message code="lblPostJoActInMosaic" /></label>
							</div>	
													
							<div class="col-sm-3 col-md-3 top10">		
								<form:input path="lblCriticialJobName" cssClass="form-control" value="${lblCriticialJobNameValue}" maxlength="50"/> 
							</div>
							<div class="col-sm-4 col-md-4 top15" style="margin-left: -20px;">
							 ,<spring:message code="lblif"/>
							</div>                 			
							
							<div class="clearfix"></div>
							<div class="col-sm-3 col-md-3" style="max-width: 170px;">
								<label> <spring:message code="lbljobActivefor" /> </label>
							</div>
							
							<div class="col-sm-3 col-md-3" style="min-width: 240px;">
								<label> 
								<iframe id="ifrm3"  src="slideract.do?name=slider3&tickInterval=10&max=${sliderMaxLength }&swidth=200&svalue=${jobFeedCriticalJobActiveDaysValue}" scrolling="no" frameBorder="0" style="border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:280px;"></iframe>
									<form:hidden path="jobFeedCriticalJobActiveDays" cssClass="form-control" value="${jobFeedCriticalJobActiveDaysValue}"/>
								</label>
							</div>
							<div class="col-sm-1 col-md-1" style="min-width: 60px;">
								<label>&nbsp; <spring:message code="lblDays" /> </label>
							</div>
								<div class="clearfix"></div>
							<div class="col-sm-12 col-md-12">
								<label>  <spring:message code="lbl&" />  </label>
							</div>
							
							<div class="col-sm-6 col-md-6" style="max-width: 400px;">
								<label>  <spring:message code="msgCondRatio" />  </label>
							</div>
							
							<div class="col-sm-3 col-md-3">
								<label> 
								<iframe id="ifrm4"  src="slideract.do?name=slider4&tickInterval=1&max=${sliderRationMaxLength }&swidth=200&svalue=${jobFeedCriticalCandidateRatioValue}" scrolling="no" frameBorder="0" style="border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:280px;"></iframe>
									<form:hidden path="jobFeedCriticalCandidateRatio" cssClass="form-control" value="${jobFeedCriticalCandidateRatioValue}"/>
								</label>
							</div>
							<div class="clearfix"></div>
							<div class="col-sm-12 col-md-12">
								<label>  <spring:message code="lblWhere" />  </label>
							</div>
							
							<div class="col-sm-5 col-md-5" style="max-width: 290px;">
								<label> <spring:message code="msgEachAppNormScore" />  </label>
							</div>
							<div class="col-sm-7 col-md-7">
								<label> 
								<iframe id="ifrm5"  src="slideract.do?name=slider5&tickInterval=10&max=${sliderMaxLength }&swidth=200&svalue=${jobFeedCriticalNormScoreValue}" scrolling="no" frameBorder="0" style="border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:280px;"></iframe>
									<form:hidden path="jobFeedCriticalNormScore" cssClass="span2" value="${jobFeedCriticalNormScoreValue}"/>
								</label>
							</div>
							
							<div class="col-sm-4 col-md-4 top15">							
								<label><spring:message code="lblPostJoActInMosaic" /></label>
							</div>	
													
							<div class="col-sm-3 col-md-3 top10">		
								<form:input path="lblAttentionJobName" cssClass="form-control" value="${lblAttentionJobNameValue}" maxlength="50"/>
							</div>
							<div class="col-sm-4 col-md-4 top15" style="margin-left: -20px;">
							  ,&nbsp<spring:message code="lblif"/>
							</div>
							<div class="clearfix"></div>
							<div class="col-sm-3 col-md-3" style="max-width: 170px;">
								<label>  <spring:message code="lbljobActivefor" />   </label>
							</div>
							
							<div class="col-sm-3 col-md-3" style="min-width: 240px">
								<label> 
								<iframe id="ifrm6"  src="slideract.do?name=slider6&tickInterval=10&max=${sliderMaxLength }&swidth=200&svalue=${jobFeedAttentionJobActiveDaysValue}" scrolling="no" frameBorder="0" style="border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:280px;"></iframe>
									<form:hidden path="jobFeedAttentionJobActiveDays" cssClass="span2" value="${jobFeedAttentionJobActiveDaysValue}"/>
								</label>
							</div>
							<div class="col-sm-1 col-md-1" style="min-width: 60px">
								<label>&nbsp; <spring:message code="lblDays" /> </label>
							</div>
								<div class="clearfix"></div>
							<div class="col-sm-12 col-md-12">
								<label>  <spring:message code="lbl&" />  </label>
							</div>
							
							<div class="col-sm-12 col-md-12">
								<label class="checkbox">  
									<form:checkbox path="jobFeedAttentionJobNotFilled" value="1"/> <spring:message code="msgFillingCompletly" />
								</label>
							</div>
							
							<div class="col-sm-12 col-md-12">
								<label>  <spring:message code="lbl&" />  </label>
							</div>
							
							<div class="col-sm-4 col-md-4" style="max-width: 280px;">
								<label>  <spring:message code="lblAppScoreGtrThan" />  </label>
							</div>
							
							<div class="col-sm-4 col-md-4">
								<label> 
								<iframe id="ifrm7"  src="slideract.do?name=slider7&tickInterval=10&max=${sliderMaxLength }&swidth=200&svalue=${jobFeedAttentionNormScoreValue}" scrolling="no" frameBorder="0" style="border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:280px;"></iframe>
									<form:hidden path="jobFeedAttentionNormScore" cssClass="span2" value="${jobFeedAttentionNormScoreValue}"/>
								</label>
							</div>
							
							<div class="col-sm-12 col-md-12">
								<label>    <spring:message code="msgNotCommWithMsgPhone" /></label>
							</div>
							
							
							<%--
							<div class="span11">
								<label> Label for Critical Jobs &nbsp;&nbsp;&nbsp;<form:input path="lblCriticialJobName" cssClass="span3" value="${lblCriticialJobNameValue}" maxlength="50"/> </label>
							</div>
							<div class="span11">
								<label> Label for Logging Jobs &nbsp;&nbsp;<form:input path="lblAttentionJobName" cssClass="span3" value="${lblAttentionJobNameValue}" maxlength="50"/></label>
							</div>
							 --%>
							 
							<div class="col-sm-6 col-md-6">
					 			<label class="checkbox">
								<%-- <form:checkbox path="postingOnDistrictWall" id="postingOnDistrictWall"  value="1"/> --%>
			                 	<form:checkbox path="canSchoolOverrideJobFeed"  value="1"/>
			                 		<spring:message code="msgSchoolCanOverRide" /> 
								</label>
							</div>				   
                   </div>               
              </div>
              <div class="clearfix">&nbsp;</div>
            </div>
          </div>
        </div>    
        <%----------------- Sekhar : Message And Notes Div -----------------------------  --%>
        <div class="panel panel-default">
          <div class="accordion-heading"> <a href="#collapseSeven" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle plus"> <spring:message code="msgMessageNotes" /> </a> </div>
          <div class="accordion-body collapse" id="collapseSeven" style="height: 0px;">
            <div class="accordion-inner">
              <div class="offset1">
                  <div class="row">
                    <div class="span11">
                      <label class="radio">
						<form:radiobutton path="communicationsAccess" value="2" />
                        <spring:message code="msgScAdminSeeMsg" /></label>
                    </div>
                    <div class="span11" style="padding-right: 15px;">
                      <label class="radio">
						<form:radiobutton path="communicationsAccess" value="1" />
                        <spring:message code="msgScAdminSeeMsg2" /> </label>
                    </div>
                    <div class="span11">
                      <label class="radio">
						<form:radiobutton path="communicationsAccess" value="0" />
                        <spring:message code="msgScAdminSeeMsg3" /> </label>
                    </div>
                  </div>
                
              </div>
            </div>
          </div>
        </div>  
        <%-----------------End Sekhar : Message And Notes Div -----------------------------  --%>  
         <%----------------- Sekhar : CG Grid -----------------------------  --%>
        <div class="panel panel-default">
          <div class="accordion-heading"> <a href="#collapseEight" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle plus"> <spring:message code="lblCandidateGrid" /> </a> </div>
          <div class="accordion-body collapse" id="collapseEight" style="height: 0px;">
            <div class="accordion-inner">
              <div class="offset1">
              	 <div class="row mt10">
                  		<div class="span11">
                    		<spring:message code="lblDisplaySelected" /> <a href="#" id="iconpophovercg" rel="tooltip" data-original-title="<spring:message code="msgCheckFieldas" />" data-placement="right"><img src="images/qua-icon.png" width="15" height="15" alt=""></a>
                  		</div>
                  </div>
                  <div class="row mt10">
                  <div class="row">
                  <div class="col-sm-2 col-md-2">
                      <label class="checkbox">
						 <form:checkbox path="displayCGPA" />
                        <spring:message code="lblCGPA" /></label>
                    </div>
                    <div class="col-sm-2 col-md-2">
                      <label class="checkbox">
						 <form:checkbox path="displayAchievementScore" />
                        <spring:message code="msgAchievementScore" /></label>
                    </div>
                    <div class="col-sm-2 col-md-2">
                      <label class="checkbox">
						 <form:checkbox path="displayFitScore"/>
                        <spring:message code="lblFitScore" /> </label>
                    </div>
                    <div class="col-sm-1 col-md-1">
                      <label class="checkbox">
						 <form:checkbox path="displayTFA" />
                        <spring:message code="lblTFA" /> </label>
                    </div>
                    <div class="col-sm-2 col-md-2">
                      <label class="checkbox">
						 <form:checkbox path="displaySeniorityNumber" />
                        <spring:message code="msgSeniorityNumber" /> </label>
                    </div>
                    
                     <div class="col-sm-2 col-md-2">
                      <label class="checkbox">
						 <form:checkbox path="displayPhoneInterview" />
                        <spring:message code="lblPhoneInterview" /></label>
                    </div>
                    
                    </div>
                    <div class="row">
                    <div class="col-sm-2 col-md-2">
                      <label class="checkbox" style="margin-top: 0px;">
						 <form:checkbox path="displayDemoClass" />
                        <spring:message code="msgDemoLesson" /></label>
                    </div>
                    <div class="col-sm-2 col-md-2">
                      <label class="checkbox" style="margin-top: 0px;">
						 <form:checkbox path="displayYearsTeaching" />
                        <spring:message code="lblTeachingYears" />  </label>
                    </div>
                    <div class="col-sm-2 col-md-2">
                      <label class="checkbox" style="margin-top: 0px;">
						 <form:checkbox path="displayExpectedSalary"/>
                        <spring:message code="lblExpectedSalary" /> </label>
                    </div>
                    <div class="col-sm-1 col-md-1">
                      <label class="checkbox" style="margin-top: 0px;">
						 <form:checkbox path="displayJSI" />
                        <spring:message code="toolJSI" /></label>
                    </div>
                    <div class="col-sm-2 col-md-2">
                      <label class="checkbox" style="margin-top: 0px;">
						<form:checkbox path="jobAppliedDate" id="jobAppliedDate" value="1" />
			    		<span style="display: inline-block;"><spring:message code="lblJobAppliedDate" /></span></label>
                    </div>
                     <div class="col-sm-3 col-md-3">
                      <label class="checkbox" style="margin-top: 0px;">
						<form:checkbox path="jobCompletionNeeded" id="jobCompletionNeeded" value="1" />
			    		<span style="display: inline-block;"><spring:message code="lbljobCompletionNeeded" /></span></label>
                    </div>
                    </div>
                  </div>
                  <div class="row mt10">
                  		<div class="span11">
                  		<div class='required' id='errordisplaycommunication'></div>
                    		<spring:message code="msgDisplayCommunication" />
                  		</div>
                </div>
                <div class="row  mt8" >
                	 <div class="span12 left20">
                	 	<label class="radio inline" style="padding-left: 0px;">
                		<c:choose>
                				<c:when test="${displayCommunication eq true}">
                					<input type="radio"  name="displayCommunication" id="specificCommunication" checked="checked" value="0"/>
					         	  	<spring:message code="msgJobSpecificCommunications" /> 
                				</c:when>
                				<c:otherwise>
                					<input type="radio"  name="displayCommunication" id="specificCommunication" value="0" checked="checked"/>
					         	  	<spring:message code="msgJobSpecificCommunications" /> 
                				</c:otherwise>
                		</c:choose>
                		</label>
                		<br>
                		<label class="radio inline" style="padding-left: 0px;">
                		<c:choose>
                				<c:when test="${displayCommunication eq false}">
                					 <input type="radio" name="displayCommunication" id="allCommunication" checked="checked" value="1"/>
					           		 <spring:message code="msgAllCommunications" />
                				</c:when>
                				<c:otherwise>
                					 <input type="radio" name="displayCommunication" id="allCommunication" value="1"/>
					            	 <spring:message code="msgAllCommunications" />
                				</c:otherwise>
                		</c:choose>
                		</label>
                	</div>	
                </div>
                  
                  
                  
              </div>
            </div>
          </div>
        </div>  
        <%-----------------End Sekhar : CG Grid -----------------------------  --%>  
         <%-- ================ Gagan : Adding accordion Div for Score Status ======================  --%>
	   <div class="panel panel-default">
          <div class="accordion-heading"> <a href="#collapseNine" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle plus"> <spring:message code="msgScoreonStatus" /> </a> </div>
          <div class="accordion-body collapse" id="collapseNine" style="height: 0px;">
            <div class="accordion-inner">
              <div class="row">
                    <div class="col-sm-12 col-md-12">
              <div id="errordistrictdiv" class="required"></div>
              </div>
              </div>
    	          <div  style="margin-left:25px;min-width: 850px;" class="offset1">
    	          	<c:set var="arrStatusId" value=""></c:set>    
    	          	
		         		<c:if test="${displayRecordFlagFromDistrictMaxFitScoreTable eq 0}">
		         		<c:set var="counter" value="0"></c:set>
                    	 <c:forEach items="${lstStatusMaster}" var="lst">
                     		<c:set var="arrStatusId" value="${arrStatusId}${lst.statusId}," ></c:set>
                     		<c:set var="counter" value="${counter+1}"></c:set> 
		                  		<c:if test="${(counter%2==0) || (counter==0)}">
                    				<div class="row mt10">
                    		    </c:if>	
		                  		<div class="col-sm-2 col-md-2">
									<label>${lst.status}  </label>
								</div>
								<c:set var="svalue" value="${defaultFitScore}"></c:set>
								<c:set var = "shortName" value="scomp"/>
								<c:if test="${lst.statusShortName eq shortName}">
									<c:set var="svalue" value="${scompFitScore}"></c:set>
								</c:if>
								<c:set var = "shortName" value="ecomp"/>
								<c:if test="${lst.statusShortName eq shortName}">
									<c:set var="svalue" value="${ecompFitScore}"></c:set>
								</c:if>
								<c:set var = "shortName" value="vcomp"/>
								<c:if test="${lst.statusShortName eq shortName}">
									<c:set var="svalue" value="${vcompFitScore}"></c:set>
								</c:if>
								
								<div class="col-sm-4 col-md-4 setSlider"> 
									<label> <iframe id="ifrm_${lst.statusId}"  src="slideract.do?name=slider_${lst.statusId}&tickInterval=20&max=100&swidth=250&svalue=${svalue}&step=1" scrolling="no" frameBorder="0" style="border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:420px;"></iframe> </label>
								</div>
								<c:if test="${(counter%2==0) || (counter==0)}">
                    				</div>
                    		    </c:if>						
						</c:forEach>
						
						<c:forEach items="${lstsecondaryStatus}" var="lstSec">
							<c:set var="arrSecStatusId" value="${arrSecStatusId}${lstSec.secondaryStatusId}," ></c:set>
							<c:set var="counter" value="${counter+1}"></c:set>
		                  		<c:if test="${(counter%2==0) || (counter==0)}">
                    				<div class="row mt10">
                    		    </c:if>	
			                  		<div class="col-sm-2 col-md-2">
										<label>${lstSec.secondaryStatusName}</label>
									</div>
									<div class="col-sm-4 col-md-4 setSlider"> 
										<label> <iframe id="ifrm_${lstSec.secondaryStatusId}"  src="slideract.do?name=slider_${lstSec.secondaryStatusId}&tickInterval=20&max=100&swidth=250&svalue=100&step=1" scrolling="no" frameBorder="0" style="border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:420px;"></iframe> </label>
									</div>
								<c:if test="${(counter%2==0) || (counter==0)}">
                    				</div>
                    		    </c:if>	                  	
						</c:forEach>
					</c:if>
					
					<c:if test="${displayRecordFlagFromDistrictMaxFitScoreTable eq 1}">
						<c:set var="counter" value="0"></c:set>
                    	 <c:forEach items="${lstStatusMaster}" var="lst">
                     		<c:set var="arrStatusId" value="${arrStatusId}${lst.statusId}," ></c:set>
                     			<c:set var="counter" value="${counter+1}"></c:set> 
									<c:if test="${(counter%2==0) || (counter==0)}">
                    				<div class="row mt10">
                    		    </c:if>	
		                  		     <div class="col-sm-2 col-md-2">
										<label>${lst.status}</label>
									</div>
									<div class="col-sm-4 col-md-4 setSlider"> 
										<label><iframe id="ifrm_${lst.statusId}"  src="slideract.do?name=slider_${lst.statusId}&tickInterval=20&max=100&swidth=250&svalue=${lst.maxFitScore}&step=1" scrolling="no" frameBorder="0" style="border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:420px;"></iframe> </label>
									</div>
									<c:if test="${(counter%2==0) || (counter==0)}">
                    				</div>
                    		    </c:if>                  			
						</c:forEach>
							
						<c:forEach items="${lstsecondaryStatus}" var="lstSec">
							<c:set var="arrSecStatusId" value="${arrSecStatusId}${lstSec.secondaryStatusId}," ></c:set>
							<c:set var="counter" value="${counter+1}"></c:set>
			                  		<c:if test="${(counter%2==0) || (counter==0)}">
                    				<div class="row mt10">
                    		    </c:if>	
		                  		     <div class="col-sm-2 col-md-2">
										<label>${lstSec.secondaryStatusName}</label>
									</div>
									<div class="col-sm-4 col-md-4 setSlider"> 
										<label> <iframe id="ifrm_${lstSec.secondaryStatusId}"  src="slideract.do?name=slider_${lstSec.secondaryStatusId}&tickInterval=20&max=100&swidth=250&svalue=${lstSec.maxFitScore}&step=1" scrolling="no" frameBorder="0" style="border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:420px;"></iframe> </label>
									</div>
									<c:if test="${(counter%2==0) || (counter==0)}">
                    				</div>
                    		        </c:if>								
						</c:forEach>
					</c:if>
					<c:if test="${displayRecordNewSecStatusRecords eq 1}">
						<c:set var="counter" value="0"></c:set>
						<c:forEach items="${lstsecondaryStatusNew}" var="lstSecNew">
							<c:set var="arrSecStatusId" value="${arrSecStatusId}${lstSecNew.secondaryStatusId}," ></c:set>
							<c:set var="counter" value="${counter+1}"></c:set>
		                  		<c:if test="${(counter%2==0) || (counter==0)}">
                    				<div class="row mt10">
                    		    </c:if>	
		                  		 <div class="col-sm-2 col-md-2">
									<label>${lstSecNew.secondaryStatusName}</label>
								</div>
								<div class="col-sm-4 col-md-4 setSlider"> 
									<label> <iframe id="ifrm_${lstSecNew.secondaryStatusId}"  src="slideract.do?name=slider_${lstSecNew.secondaryStatusId}&tickInterval=20&max=100&swidth=250&svalue=100&step=1" scrolling="no" frameBorder="0" style="border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:420px;"></iframe> </label>
								</div>
								<c:if test="${(counter%2==0) || (counter==0)}">
                    				</div>
                    		    </c:if>	                  	
						</c:forEach>
					</c:if>
					
					
                <%-- <c:out value="${arrStatusId}"></c:out>--%>	
                	<input type="hidden" id="arrStatusId" name="arrStatusId" value="${arrStatusId}">
                	<input type="hidden" id="arrSecStatusId" name="arrSecStatusId" value="${arrSecStatusId}">
                </div>
              <div class="clearfix">&nbsp;</div>
            </div>
          </div>
        </div>    
        
         <%-- ================ Gagan : End of accordion Div for Score Status ======================  --%>
         
         <%-- ================ rajendra : Start of accordion Div for Privilege For District =======  --%>
         <div class="panel panel-default">
          <div class="accordion-heading"> <a href="#collapseTen" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle plus"><spring:message code="msgPRivilageforDist" /></a></div>
          <div class="accordion-body collapse" id="collapseTen" style="height: 0px;">
            <div class="accordion-inner">
    	      <div class="offset1"><!--
    	      
    	       <div class="row mt10">
    	      	<label><spring:message code="lblUpdateEmailCredentials" /></label>
              </div>
    	   
    	        <div class="row">
	                  <div class="col-sm-12 col-md-12">
		                    <div id="privilegeForEmail" style="margin-left: -15px;">
							<label>	<input type="checkbox" name="
entials" id="emailCredentials" />&nbsp;&nbsp;<spring:message code="msgEmailCredentials" /></label>
		                    </div>
	                  </div>
                  </div>  
    	      	<div class="row mt10 left5">
		                  <div class="col-sm-6 col-md-6">
		                        <label class="checkbox inline" style="padding-left: 0px; margin-left: -30px;"></label>
		                        <form:checkbox path="emailCredentials" id="emailCredentials"   value="1"/>
								<label style="margin-left: 2px"> <spring:message code="msgEmailCredentials" /></label>
								
						   </div>
	                   </div> 
    	      <div class="row mt10" style="margin-left: -10px">
    	      	<div id="privilegeForDistrictErrorDiv"> </div>
              </div>
              --><!-- Demo shrirma-->
              
               <div class="row mt10" style="margin-left: -10px">
    	      	<div id="privilegeForDistrictErrorDiv"> </div>
              </div>
              <!-- shriram -->
    	       <div class="row mt10">
    	      	<div class="span11"><spring:message code="lblTrackEmailCommunications" /></div>
              </div>
             
    	   <div class="row">
	                  <div class="col-sm-12 col-md-12">
		                    <div id="emailcrid11" >
		                   <label class="checkbox" style="padding-left:5px;"> 
		                         <form:checkbox path="emailCredentials" id="emailCredentials"   value="1"/>
								 <spring:message code="lblTrackEmailCredentialsOfAllAdmins"/></label>
								 </div>
								 </div>
		 </div>
		 <c:if test="${headQuarterId==2}" >
		  <div class="row">
	                  <div class="col-sm-12 col-md-12">
		                    <div id="jobCompletionEmailDiv" >
		                   <label class="checkbox" style="padding-left:5px;"> 
		                         <form:checkbox path="jobCompletionEmail" id="jobCompletionEmail"   value="1"/>
								 <spring:message code="lblJobCompletionEmail"/></label>
								 </div>
								 </div>
		 </div>
		 </c:if>
		        <div class="row mt10">
    	      	<div class="span11"><spring:message code="lblTrackEmailFlag" /></div>
              </div>
             
    	   <div class="row">
	                  <div class="col-sm-12 col-md-12">
		                    <div id="emailcrid11" >
		                   <label class="checkbox" style="padding-left:5px;"> 
		                         <form:checkbox path="districtEmailFlag" id="districtEmailFlag"   value="1"/>
								 <spring:message code="lblTrackEmailFlagOfAllAdmins"/></label>
								 </div>
								 </div>
		 </div>
		 
		               
              <!-- Demo shrirma -->
              
               <div class="row mt10">
                  		<div class="span11">
                    		<spring:message code="lblEeocSupport" />
                  		</div>
                </div>
               <div class="row">
	                  <div class="col-sm-12 col-md-12">
		                    <div id="eeoc_support" >
		                      <label class="checkbox" style="padding-left:5px;"> 
								<form:checkbox path="eeocRequired" id="eeocRequired" value="1"/>
								 <spring:message code="lblEeocSupportNeeded"/>
								 </label>
		                    </div>
	                  </div>
                  </div>
                
    	      <div class="row mt10">
                  		<div class="span11">
                    		<spring:message code="msgJobApproval" />
                  		</div>
                </div>
                <div class="row">
                  	<div class="col-sm-5 col-md-5" >
                  	<c:set var="disablenoOfApprovalNeeded" value=""></c:set>					
						<c:if test="${districtMaster.approvalBeforeGoLive ne true}">
				         	<c:set var="disablenoOfApprovalNeeded" value="disabled"></c:set>	 
						</c:if>
                      <label class="checkbox" style="padding-left:5px;">                      
						 <form:checkbox path="approvalBeforeGoLive"  id="approvalBeforeGoLive1" onclick="return jobApprovalFunc();"/>
                        <spring:message code="msgApprovalprocess" /></label>
                    </div>
                    <div class="col-sm-3 col-md-3" style="margin: 10px -57px 0px 0px;">                    
                        <spring:message code="msgApprovalsNeeded" />

                     </div>
                     <div class="col-sm-2 col-md-2" >    
                     <c:set var="selValue" value="${districtMaster.noOfApprovalNeeded}"></c:set>	                
                        <select class="form-control" id="noOfApprovalNeeded" ${disablenoOfApprovalNeeded} name="noOfApprovalNeeded" style="width:60px;padding-left:5px;">
                        <option value="0">0</option>
							<c:forEach begin="6" end="15" varStatus="loop">
								<c:choose>
				                    <c:when test="${districtMaster.noOfApprovalNeeded == loop.count}">
				                         <option value="${loop.count}" selected="selected">${loop.count}</option>
				                    </c:when>
				                    <c:otherwise>
				                         <option value="${loop.count}">${loop.count}</option>
				                    </c:otherwise>
	              			 </c:choose>
							</c:forEach>
						</select>
                     </div>                    
                </div>
                
                
                <!-- Start of Grouping Functionality -->
                
                <div class="row mt10" style='margin-top: 0px;' id="buildApprovalGroupsDivId">
                	<div class="span11">
                		<label class="checkbox">
                			<form:checkbox path="buildApprovalGroup" id="buildApprovalGroups" value="1" />                
                			Build Approval Groups &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                			<a href="javascript:void(0);" onclick="showAddGroup()" style="margin-left: 65%;"> +Add Member </a>
                		</label>
                	</div>
                </div>                
                  
                
                <div id="groupMembersList" class="hideGroup" style="display: block;margin-left: 21px;margin-top: -11px;"> </div>
                
              		 <div class="row mt10">
							<div class="span11">
							  <spring:message code="JobOrderViewPermissions" />	
							</div>							
						</div>
					<div class="row" style="display: block; margin-left: 21px; ">
						<div id="showApprovalGropus">
							<div class="col-sm-12 col-md-12">
								<label class="radio inline" style="padding-left: 5px;">
									<form:radiobutton path="showApprovalGropus"
										id="showApprovalGropus" value="1" />
									<spring:message code="JOVPType1" />
								</label>
							</div>
							<div class="col-sm-12 col-md-12">
								<label class="radio inline" style="padding-left: 5px;">
									<form:radiobutton path="showApprovalGropus"
										id="showApprovalGropus" value="2" />
									<spring:message code="JOVPType2" />
								</label>
							</div>
							<div class="col-sm-12 col-md-12">
								<label class="radio inline" style="padding-left: 5px;">
									<form:radiobutton path="showApprovalGropus"
										id="showApprovalGropus" value="3" />
									<spring:message code="JOVPType3" />
								</label>
							</div>
							<div class="col-sm-12 col-md-12">
								<label class="radio inline" style="padding-left: 5px;">
									<form:radiobutton path="showApprovalGropus"
										id="showApprovalGropus" value="4" />
									<spring:message code="JOVPType4" />
								</label>
							</div>
						</div>
					</div>

					<div class="bs-example bs-example-form" id="addApprovalGroupAndMemberDiv" class="hideGroup">
                	<div class="row" style="margin-left: -20px;margin-top: 15px;">
                		<input type="hidden" value="1" id="noOfApproval" name="noOfApproval" class="form-control"  maxlength="4">
                		<div class="col-lg-3">
                			<label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Enter Group Name <font color="red">*</font> </label>
                			<div class="input-group">
                				<span class="input-group-addon">
                					<input type="radio" name="groupRadioButtonId" id="groupRadioButtonId1" value="radioButton1" checked="checked" onclick="groupRadioButton()">
                				</span>
                				<input type="text" class="form-control" name="addGroupName" id="addGroupName">
                			</div>
                			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                			<label style="margin-top: 11px; margin-left: 5px;" class="idone">
                				<a href='javascript:void(0);' onclick="addGroupORMember()"> <spring:message code="lnkImD"/> </a> &nbsp;&nbsp; 
                				<a href='javascript:void(0);' onclick="hideAddApprovalGroupAndMemberDiv()"> Cancel </a>
                			</label>
                		</div>
                		
                		<div class="col-lg-3">
                			<label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Select Group Name <font color="red">*</font> </label>
                			<div class="input-group">
                				<span class="input-group-addon">
                					<input type="radio" name="groupRadioButtonId" id="groupRadioButtonId2" value="radioButton2" onclick="groupRadioButton()">
                				</span>
               					<select class="form-control" id="groupName" name="groupName">
               					</select>
               				</div>
               			</div>
               			
               			<div class="col-lg-3">
               				<label> Select Members <font color="red">*</font> </label>
               				<div class="input-group">
               					<select multiple="" class="form-control" id="groupMembers" name="groupMembers" style="height: 70px; min-width: 260px;">
               					</select>
               				</div>
               			</div>
               			
               		</div>
               	</div>
                
                <c:if test="${districtMaster.districtId eq 804800}">
					<div class="row">
						<div class="span11">
					      <label class="checkbox">
									 <form:checkbox path="approvalByPredefinedGroups" />
									Approval By Provided Groups</label>
					    </div>
					</div>
				</c:if>
                
				<!-- End of Grouping Functionality -->
                
                
    	      	<div class="row mt10">
                  		<div class="span11">
                    		<spring:message code="msgCandidatePool" />
                  		</div>
                </div>
    	      	<div class="row">
                  	<div class="span11">
                      <label class="checkbox">
						 <form:checkbox path="isTeacherPoolOnLoad" />
                        <spring:message code="msgShowpageload" /></label>
                    </div>
                </div>

				<!-- TPL-4833  Configuration and Logic for Candidates Not Reviewed starts-->				
                <form:hidden path="candidatesNotReviewed" />  
    	      	<div class="row">
					<div class="span11">
					<spring:message code="lblCandidateNotReviewed"/>
					<a href="#" id="iconpophover25" rel="tooltip" data-original-title="Displays orange flame icon against users until one of the following system events has occurred."><img src="images/qua-icon.png" width="15" height="15" alt=""></a>
					</div>
                </div>	
				<div class="row left25">			
					<div class="row">
						<div class="span11 top5">
							<spring:message code="lblViewPermission"/>
						</div>
					</div>	
					<div class="row">
						<div class="col-sm-2 col-md-2">
							<label class="checkbox inline"  style="padding:0px;margin:0px">
							 <input type="checkbox" name="CandidateNotReviewed" value="DAVP"/>
							<spring:message code="lblDAView"/> 
							</label>
						</div>
						<div class="col-sm-2 col-md-2">
							<label class="checkbox inline"  style="padding:0px;margin:0px">
							 <input type="checkbox" name="CandidateNotReviewed" value="SAVP"/>
							<spring:message code="lblSAView"/> 
							</label>
						</div>
					</div>	
					<div class="row">
						<div class="span11 top5">
							<spring:message code="lblActionPermission"/>
						</div>
					</div>		
					<div class="row">
						<div class="col-sm-2 col-md-2">
						<label class="checkbox inline"  style="padding:0px;margin:0px">
							 <input type="checkbox"  name="CandidateNotReviewed" value="DAACT"/>
							<spring:message code="lblDAAction"/> 
						</label>
						</div>
						<div class="col-sm-2 col-md-2">
						<label class="checkbox inline"  style="padding:0px;margin:0px">
							 <input type="checkbox"  name="CandidateNotReviewed"  value="SAACT"/>
							<spring:message code="lblSAAction"/> 
						</label>
						</div>
					</div>
					<div class="row">
						<div class="span11 top5">
							<spring:message code="lblCandidateReviewedActivities"/>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6 col-md-6">
							<label class="checkbox inline"  style="padding:0px;margin:0px">
								 <input type="checkbox"  name="CandidateNotReviewed" value="CNR_CRA"/>
								<spring:message code="lblClickOnResumeActivity"/> 
							</label>
						</div>					
					</div>
					<div class="row">
						<div class="col-sm-6 col-md-6">
							<label class="checkbox inline"  style="padding:0px;margin:0px">
								 <input type="checkbox" name="CandidateNotReviewed" value="CNR_AGTA"/>
								<spring:message code="lblApplyGlobalTagActivity"/> 
							</label>
						</div>				
					</div>
					<div class="row">
						<div class="col-sm-6 col-md-6">
							<label class="checkbox inline"  style="padding:0px;margin:0px">
								 <input type="checkbox"  name="CandidateNotReviewed" value="CNR_AJSTA"/>
								<spring:message code="lblApplyJobSpecTagActivity"/> 
							</label>
						</div>				
					</div>
					<div class="row">
						<div class="col-sm-6 col-md-6">
							<label class="checkbox inline"  style="padding:0px;margin:0px">
								 <input type="checkbox" name="CandidateNotReviewed" value="CNR_AMNPA"/>
								<spring:message code="lblAddmsgNotePhoneActivity"/> 
							</label>
						</div>				
					</div>
					<div class="row">
						<div class="col-sm-6 col-md-6">
							<label class="checkbox inline"  style="padding:0px;margin:0px">
								 <input type="checkbox"  name="CandidateNotReviewed" value="CNR_SLCCA"/>
								<spring:message code="lblSLCChangeActivity"/> 
							</label>
						</div>				
					</div>
					<div class="row">
						<div class="col-sm-6 col-md-6">
							<label class="checkbox inline"  style="padding:0px;margin:0px">
								 <input type="checkbox" name="CandidateNotReviewed" value="CNR_IOEA"/>
								<spring:message code="lblInviteOnEventActivity"/> 
							</label>
						</div>				
					</div>
					<div class="row">
						<div class="col-sm-6 col-md-6">
							<label class="checkbox inline"  style="padding:0px;margin:0px">
								 <input type="checkbox"  name="CandidateNotReviewed" value="CNR_PAA"/>
								<spring:message code="lblPanelAppliedActivity"/> 
							</label>
						</div>
					</div>
				</div>
				<!-- TPL-4833  Configuration and Logic for Candidates Not Reviewed ends-->    	      	
    	      	<div class="row mt10">
                  		<div class="span11">
                    		<spring:message code="headStatusNotes" />
                  		</div>
                </div>
    	      	 
    	      	<div class="row">
                  	<div class="span11">
                      <label class="checkbox">
						 <form:checkbox path="statusNotes" />
                        <spring:message code="msgalltheStatuses" /></label>
                    </div>
                </div>
    	      	 
    	      	<div class="row">
                  		<div class="span11">
                    		<spring:message code="lblRequisitionNumber" />
                  		</div>
                </div>
    	      	 
    	      	<div class="row">
                  	<div class="span11">
                      <label class="checkbox">
						 <form:checkbox path="isReqNoRequired" />
                        <spring:message code="msgRequisitionnumberReq" /></label>
                    </div>
                </div>
                
                <div class="row">
                  	<div class="span11">
                      <label class="checkbox">
						 <form:checkbox path="isReqNoForHiring" />
                        <spring:message code="msgRequisitionnumberReq4Hire" /></label>
                    </div>
                </div>
                
                <div class="row">
                  		<div class="span11">
                    		<spring:message code="lblZone" />
                  		</div>
                </div>
                
                <div class="row">
                  	<div class="span11">
                      <label class="checkbox">	
                       <form:checkbox path="isZoneRequired" value="1" disabled = "disabled"/>				
                        <spring:message code="msgZoneReq" /></label>
                    </div>
                </div>
				
     
      
                <div class="row">
                  	<div class="span11">
                      <label class="checkbox">
						 <form:checkbox path="autoNotifyCandidateOnAttachingWithJob" />
                        <spring:message code="msgAutoNotify" /></label>
                    </div>
                </div>
                
                 <div class="row">
                  	<div class="span11">
                      <label class="checkbox">
						 <form:checkbox path="noEPI" />
                          <spring:message code="msgEPIScore1" /></label>
                    </div>
                </div>
              
              
              
                  <div class="row">   
                    <div class="span12">
                    
			            <form:checkbox path="autoEmailRequired" cssClass="checkbox inline" style="vertical-align: bottom" id="autoEmailRequired" value="1" onclick="disableStatusMasterEmail()" />
			            <span style="display: inline-block;"><spring:message code="msgAutonotify1" /></span>
					</div>
					<div class="left15">
					     <c:forEach items="${lstStatusMaster_PFC}" var="lstsm">
	                  		<div class="col-sm-3 col-md-3 top5" style="padding-left: 20px;">
	                  		
	                  		       <label class="checkbox inline" style="padding-left: 0px;">
										<form:checkbox path="chkStatusMasterEml" id="chkStatusMasterEml" value="${lstsm.statusId}"/>
							            ${lstsm.status}
									</label>
							</div>
						 </c:forEach>
						 
						
						
						 <c:forEach items="${lstsecondaryStatus_PFC}" var="lstSecSM">
							<div class="col-sm-3 col-md-3 top5" style="padding-left: 20px;">
								<label class="checkbox inline" style="padding-left: 0px;">
									<form:checkbox path="chkSecondaryStatusNameEml" id="chkSecondaryStatusNameEml" value="${lstSecSM.secondaryStatusId}" />
						            ${lstSecSM.secondaryStatusName}
								</label>
							</div>
						 </c:forEach>
				</div>		
                </div>
                				
				<!-- TPL-4992 --- Enable district to set district-wide job closing time at manage district start  -->
				<input type="hidden" id="hidPostingStartTime" value=""/>
				<input type="hidden" id="hidPostingEndTime" value=""/>
				<div class="row" style="padding-top:15px;">
					<spring:message code="lbltimehdr" />
				</div>
                <div class="row">
					<div class="col-sm-4 col-md-4">
						<table cellspacing=0 cellpadding=0>
						<tr><td>
								<span class=""><label><spring:message code="lblPostingStartTime" /></label></span>
								<select id="postingStartTime" name="postingStartTime" class="help-inline form-control fontsize11" style="width:92px;-webkit-appearance: none;" onfocus="getpostingtimeoption(this);" onblur="saveposttimeonblur(this);">	
								</select>
						</td><td style="padding-left:20px;">
								<span class=""><label><spring:message code="lblPostingEndTime" /></label></span>
								<select id="postingEndTime" name="postingEndTime" class="help-inline form-control fontsize11" style="width:92px;-webkit-appearance: none;" onfocus="getpostingtimeoption(this);" onblur="saveposttimeonblur(this);">	
								</select>
						</td></tr>
						</table>
					</div>
				</div>
				<!-- TPL-4992 --- Enable district to set district-wide job closing time at manage district end  -->
                
                <div class="row mt10">
                  		<div class="span11">
                  		<div class='required' id='errorpdistictdiv'></div>
                    		<spring:message code="lblJobCat" />
                  		</div>
                </div>
                <div class="row  mt8" >
                	 <div class="span12 left20">
                	 	<label class="radio inline" style="padding-left: 0px;">
                		<c:choose>
                				<c:when test="${displayTMDefaultJobCategory eq true}">
                					<input type="radio"  name="displayTMDefaultJobCategory" id="tmdefault" checked="checked" value="1"/>
					         	  	<spring:message code="msgDisplayTeacherMatch1" />
                				</c:when>
                				<c:otherwise>
                					<input type="radio"  name="displayTMDefaultJobCategory" id="tmdefault" value="1" checked="checked"/>
					         	  	<spring:message code="msgDisplayTeacherMatch1" />
                				</c:otherwise>
                		</c:choose>
                		</label>
                		<br>
                		<label class="radio inline" style="padding-left: 0px;">
                		<c:choose>
                				<c:when test="${displayTMDefaultJobCategory eq false}">
                					 <input type="radio" name="displayTMDefaultJobCategory" id="districtdefault" checked="checked" value="0"/>
					           		 <spring:message code="msgDisplayonlyDistricts" />
                				</c:when>
                				<c:otherwise>
                					 <input type="radio" name="displayTMDefaultJobCategory" id="districtdefault" value="0"/>
					            	 <spring:message code="msgDisplayonlyDistricts" />
                				</c:otherwise>
                		</c:choose>
                		</label>
                	</div>	
                </div>
                 
                <div class="row mt10">
                  		<div class="span11">
                    		<spring:message code="msgStatusWorkFlow" />
                  		</div>
                </div>
                
                <div class="row  mt8" >
                	 <div class="span12 left20">
                	 	<label class="radio inline" style="padding-left: 0px;">
	                	<c:choose>
	               				<c:when test="${setAssociatedStatusToSetDPoints eq false}">	               					
	               					<input type="radio" name="setAssociatedStatusToSetDPoints" id="beforeDpoint" checked="checked" onclick="chkDStatus(false)" value="0" />
				            		<spring:message code="msgAdmincansetDpoint" />
	               				</c:when>
	               				<c:otherwise>
	               					<input type="radio" name="setAssociatedStatusToSetDPoints" id="beforeDpoint"  onclick="chkDStatus(false)"  value="0" />
				            		<spring:message code="msgAdmincansetDpoint" />
	               				</c:otherwise>
	                	</c:choose>
	                	</label>
	                	<br>
	                	<label class="radio inline" style="padding-left: 0px;">
	                	<c:choose>
	             				<c:when test="${setAssociatedStatusToSetDPoints eq true}">
	             					<input type="radio" name="setAssociatedStatusToSetDPoints" id="afterDpoint"  onclick="chkDStatus(true)" checked="checked" value="1"/>
			            			<spring:message code="msgAdmincansetDpoint1" />
	             				</c:when>
	             				<c:otherwise>
	             					<input type="radio" name="setAssociatedStatusToSetDPoints" id="afterDpoint"  onclick="chkDStatus(true)" value="1"/>
			            			<spring:message code="msgAdmincansetDpoint1" />
	             				</c:otherwise>
	             				
	                	</c:choose>
	                	</label> 
	                </div>	
	                	<br>
	                	<div class="col-sm-12 col-md-12 left25" style="margin-top:-15px;">
	                	   <div  id="dPointsSceStatus">
						   <c:forEach items="${lstStatusMasterDPoints}" var="lstdp">
								<label class="checkbox inline col-sm-2 col-md-2" style="padding-left: 0px;margin-top: 0px;">
								<c:set var="accessDPointsVal" value="||${lstdp.statusId}||" />
								<c:choose>
									<c:when test="${fn:contains(districtMaster.accessDPoints,accessDPointsVal)}">
										<form:checkbox path="chkDStatusMaster" id="chkDStatusMaster" value="${lstdp.statusId}" checked="checked" />
									</c:when>	
									<c:otherwise>
										<form:checkbox path="chkDStatusMaster" id="chkDStatusMaster" value="${lstdp.statusId}" />
									</c:otherwise>
								</c:choose>
						            ${lstdp.status} 
								</label>
							</c:forEach>
						  </div>
						  </div>	                	
	            </div>
                
                <div class="row">   
                    <div class="span12">
                        <form:checkbox path="offerVirtualVideoInterview" cssClass="checkbox inline" style="vertical-align: bottom" id="offerVirtualVideoInterview" value="1" onclick="showOfferVVI();" />
			            <span style="display: inline-block;"><spring:message code="lblOfrVVINtr" /></span>
					</div>
					
					<div class="left15 hide" id="offerVVIDiv">
							   <div class="row">
							     <div class="col-sm-12 col-md-12">
							        	<div class="divErrorMsg" id="errorofferVVIDiv"></div>         
						           </div>
								</div>
								<div class="left15">
							<div class="row">
							     <div class="col-sm-12 col-md-12 top25-sm23">
							             		<div  class="col-sm-6 col-md-6" style="margin:12px;">
							             			<label id="captionDistrictOrSchool"><spring:message code="lblPlzSeltQuesSet" /></label>
							             			<input type="hidden" id="quesId" name="quesId" value="${quesSetIDValue}"> 
								             		<input type="text" id="quesName" name="quesName"  class="form-control"
								             		onfocus="getQuestionSetAutoComp(this, event, 'divTxtShowData', 'quesName','quesId','');"
													onkeyup="getQuestionSetAutoComp(this, event, 'divTxtShowData', 'quesName','quesId','');"
													onblur="hideDistrictMasterDiv(this,'quesId','divTxtShowData');"	
													value="${quesSetNameValue}"/>
													<div id='divTxtShowData' style=' display:none;position:absolute;z-index:5000;' onmouseover="mouseOverChk('divTxtShowData','quesName')" class='result' ></div>	
								             	</div>		             
						           </div>
								</div>
						
										<div class="row top5">
													<form:checkbox path="" cssClass="checkbox inline" style="vertical-align: bottom" id="wantScore" value="1" onclick="showMarksDiv();" />
													<span style="display: inline-block;"><spring:message code="lblWantSco" /></span>
										</div>	
											<div class="row hide" id="marksDiv" >
								`				<div class="col-sm-3 col-md-3 top10">
													<spring:message code="lblMMarks" />		
													<form:input path="maxScoreForVVI" cssClass="form-control" value="${districtMaster.maxScoreForVVI}" id="maxScoreForVVI" maxlength="3" onkeypress="return checkForInt(event)" /> 
												</div>
											</div>
										
										<div class="row">
			                    	            <form:checkbox path="sendAutoVVILink" cssClass="checkbox inline" style="vertical-align: bottom" id="sendAutoVVILink" value="1" onclick="showLinkDiv();" />
									            <span style="display: inline-block;"><spring:message code="lblWntToSendInrLnkAuto" /></span>
										</div>
										<div class="hide" id="autolinkDiv">
										<div class="row">
											<div class="mt10" style="padding-left: 15px;">
												<div class="col-sm-4 col-md-4 top5" style="max-width:430px;">
							                 		<label class="radio inline" style="padding-left: 0px;">
													       <input type="radio"  name="jobCompletedVVILink" id="jobCompletedVVILink1" value="0" <c:if test="${districtMaster.jobCompletedVVILink ne null && districtMaster.jobCompletedVVILink eq false}">checked </c:if> onclick="showHideVVILink(0)"/>
													</label>
										            <spring:message code="lblLnkWiBeSendAudoOnceAdminSet" /> 
										        </div>
										        
						                      	<div class="col-sm-3 col-md-3 top25-sm23">
											            <select class="form-control" id="statusListBoxForVVI" name="statusListBoxForVVI">
											            <option value=""></option>
								                        <c:forEach items="${lstStatusMaster_PFC}" var="lstsmForVVI">
															<c:set var="selected" value=""></c:set>						
											        		<c:if test="${lstsmForVVI.statusId eq statusListBoxValueForVVI}">	        			
											         			<c:set var="selected" value="selected"></c:set>	         			
											        		</c:if>	
															<option value="${lstsmForVVI.statusId}" ${selected} >${lstsmForVVI.status}</option>
														</c:forEach>
														
														<c:set var="lstvalueForVVI" value=""></c:set>
														<c:forEach items="${lstsecondaryStatus_PFC}" var="lstSecSMForVVI">
															<c:set var="selected" value=""></c:set>
															<c:set var="lstvalueForVVI" value="SSIDForVVI_${lstSecSMForVVI.secondaryStatusId}"></c:set>
															<c:if test="${lstvalueForVVI eq statusListBoxValueForVVI}">	        			
											         			<c:set var="selected" value="selected"></c:set>	         			
											        		</c:if>
															<option value="SSIDForVVI_${lstSecSMForVVI.secondaryStatusId}" ${selected} >${lstSecSMForVVI.secondaryStatusName}</option>
														</c:forEach>
								                      </select>
						                      	</div>
						                      	                      
										       	<div class="col-sm-3 col-md-3 top5 top25-sm23">
										           &nbsp;<spring:message code="lblStAgApp" />
												</div>
											</div>	
						                </div>
						                <div class="row">
											<div class="mt10" style="padding-left: 15px;">
												<div class="col-sm-5 col-md-5 top5" style="max-width:430px;">
							                 		<label class="radio inline" style="padding-left: 0px;">
							                 			   <input type="radio"  name="jobCompletedVVILink" id="jobCompletedVVILink2" value="1" <c:if test="${districtMaster.jobCompletedVVILink ne null &&  districtMaster.jobCompletedVVILink eq true}">checked </c:if> onclick="showHideVVILink(1)"/>
													</label>
										            <spring:message code="lbljobCompletedVVILink" /> 
										        </div>
										    </div>
										</div>
										</div>
										        
						                
						                <div class="row">
						                	<div class="mt10">
							                	<div  class="col-sm-4 col-md-4 top5">
													<label><spring:message code="msgTimeAllowedPerQuestion" /></label>
													<form:input path="timeAllowedPerQuestion" id="timeAllowedPerQuestion" cssClass="form-control" maxlength="4" tabindex="-1" placeholder="" onkeypress="return checkForInt(event)" />
							                    </div>
							                    <div  class="col-sm-4 col-md-4 top5">	
							                    	<label><spring:message code="lblVVItrExInDays" /></label>
													<form:input path="VVIExpiresInDays" cssClass="form-control" style="width:200px;" maxlength="2" tabindex="-1" placeholder="" onkeypress="return checkForInt(event)" />
							            		</div>
						            		</div>
						            	</div>
						            	</div>
					</div>
			    </div>
			    
			    <div class="row">
			    	<div class="span12">
                        <input type="hidden" id="distASMTIDSList" name="distASMTIDSList" value="" />
                        <form:checkbox path="offerAssessmentInviteOnly" cssClass="checkbox inline" style="vertical-align: bottom" id="offerAssessmentInviteOnly" value="1" onclick="showOfferAMT();" />
			            <span style="display: inline-block;"><spring:message code="lblOffAssToCndOnInvite" /></span>
					</div>
					
					<div class="left15 hide" id="offerAMTInvite">
							   <div class="row">
							     <div class="col-sm-12 col-md-12">
							        	<div class="divErrorMsg" id="errorofferDASMT"></div>         
						           </div>
								</div>
								
								<div class="" id="multiDisAdminDiv">
									<div class="row left5">
										<div class="col-sm-5 col-md-5" style="border: 0px solid red;">
											<label><spring:message code="lblSltDistAss" /></label>
										</div>
									</div>
									<div class="row left5">
										 <div class="col-sm-5 col-md-5" style="border: 0px solid red;">
											<div id="1stSchoolDiv">
												<select multiple id="lstDistrictAdmins" name="lstDistrictAdmins" class="form-control" style="height: 150px;" > 
												</select>
											</div>
										</div>		
										<div class="col-sm-1 col-md-1 left20-sm"> 
											<div class="span2"> <span id="addPop" style="cursor:pointer;"><img alt="" src="images/rightarrow.jpg" width="30px"> </span></div>
											<div class="span2"> <span id="removePop" style="cursor:pointer;"><img alt="" src="images/leftarrow.jpg" width="30px"> </span></div>
										</div>
										<div class="col-md-5 col-md-5" style="border: 0px solid green;">
											<div id="2ndSchoolDiv">
												<select multiple class="form-control" id="attachedDAList" name="attachedDAList" style="height: 150px;">
												</select>
											</div>
										</div>	
									</div>
								</div>
								
								
								<%-- 
								<div class="row">
							     	<div class="col-sm-12 col-md-12 top25-sm23">
							             		<div  class="col-sm-6 col-md-6" style="margin:12px;">
							             			<label id="captionDistrictOrSchool">Please select a assessment</label>
							             			<input type="hidden" id="assessmentId" name="assessmentId" value="${distASMTId}"> 
								             		<input type="text" id="assessmentName" name="assessmentName"  class="form-control"
								             		onfocus="getDistrictAssessmentAutoComp(this, event, 'divTxtShowData1', 'assessmentName','assessmentId','');"
													onkeyup="getDistrictAssessmentAutoComp(this, event, 'divTxtShowData1', 'assessmentName','assessmentId','');"
													onblur="hideDistrictMasterDiv(this,'assessmentId','divTxtShowData1');"	
													value="${distASMTVlaue}"/>
													<div id='divTxtShowData1' style=' display:none;position:absolute;z-index:5000;' onmouseover="mouseOverChk('divTxtShowData1','assessmentName')" class='result' ></div>	
								             	</div>		             
						           </div>
								</div>
								--%>
					</div>
			    </div>
			    
			      <div class="row ">
					<div id="qqSetDistrict">	
						<div class="row left15">
						 <div class="col-sm-6 col-md-6 " style="border: 0px solid red; margin-left: -25px;margin-top: 10px">
								<label><spring:message code="msgSetQualificationQn" /></label>
								<select id="qqAvlDistList" name="qqAvlDistList" class="form-control" onchange="saveQQInManageDistrict()"> 
								   
							    </select>
						</div>
						</div>
							
						<div class="row left15">
						<div class="col-sm-6 col-md-6 " style="border: 0px solid red; margin-left: -25px;margin-top: 10px">
						<label><spring:message code="lblSetQualificationQuestion"/></label>
						<select id="qqAvlDistListForOnboard" name="qqAvlDistListForOnboard" class="form-control" onchange="saveQQInManageDistrict()"> 
						</select>
						</div>
						</div>	
					
					</div>
				  </div>
			    
			    <div class="row top10" >
			    	<div class="span12 top5">
                       <span style="display: inline-block;"><spring:message code="lnkSchoolSelection" /></span>
					</div>
                 	<div class="col-sm-7 col-md-7 top2" style="">
			            <spring:message code="msgAskCadToChooseSchl" />
			        </div>    
		            <div class="col-sm-4 col-md-4 top25-sm23">
		            <select class="form-control" id="statusListBoxForCC" name="statusListBoxForCC">
		            <option value=""></option>
		            <c:if test="${lstStatusMaster_PFC ne null}">
                       <c:forEach items="${lstStatusMaster_PFC}" var="lstsmForCC">
						<c:set var="selected" value=""></c:set>						
		        		<c:if test="${lstsmForCC.statusId eq statusListBoxValueForCC}">	        			
		         			<c:set var="selected" value="selected"></c:set>	         			
		        		</c:if>	
						<option value="${lstsmForCC.statusId}" ${selected} >${lstsmForCC.status}</option>
						</c:forEach>
					</c:if>
				 	<c:set var="lstvalueForCC" value=""></c:set>
				 	<c:if test="${lstsecondaryStatus_PFC ne null}">
						<c:forEach items="${lstsecondaryStatus_PFC}" var="lstSecSMForCC">
							<c:set var="selected" value=""></c:set>
							<c:if test="${lstSecSMForCC.secondaryStatusId eq statusListBoxValueForCC}">	        			
			         			<c:set var="selected" value="selected"></c:set>	         			
			        		</c:if>
							<option value="SSIDForCC_${lstSecSMForCC.secondaryStatusId}" ${selected} >${lstSecSMForCC.secondaryStatusName}</option>
						</c:forEach>
					</c:if>
                     </select>
                     </div>                      
			    </div>
			    <div class="row">
			    	<div class="col-sm-7 col-md-7">
			            <spring:message code="lblStaAgThTech" />
			        </div> 
			    </div>

			      <div class="row">
		           <div class="span12 top5">
						<form:checkbox path="sendReminderToIcompCandiates" cssClass="checkbox inline" style="vertical-align: bottom" id="sendReminderToIcompCandiates" onclick="showReminderDiv();"/>
						<span style="display: inline-block;"><spring:message code="msgSendReminder1" /></span>
					</div>
				</div>
				
			 <div class="left15 hide" id="sendFrequencyReminder">
			 	<div class="row">
					<div class="col-sm-12 col-md-12">
						<div class="divErrorMsg" id="erroroReminderSet"></div>
				   </div>
				</div>
				
				<div class="row">
			        <div class="col-sm-6 col-md-6 top5">
			        	<spring:message code="msgTotalReminderssent" />
			        </div>
			        <div class="col-sm-3 col-md-3 top5">
			        	<form:input path="noOfReminder" id="noOfReminder" cssClass="form-control" maxlength="3" placeholder="" onkeypress="return checkForInt(event);"/>
			         </div>
				</div>
			 
			 	 <div class="row">
			 		<div class="col-sm-6 col-md-6 top5">
			        	<spring:message code="msgNoOfDaysRemain" /> 
			        </div>
			        <div class="col-sm-3 col-md-3 top5">
			        	<form:input path="reminderFrequencyInDays" id="reminderFrequencyInDays" cssClass="form-control" maxlength="3" placeholder="" onkeypress="return checkForInt(event);"/>
			        </div>
			        <div class="col-sm-2 col-md-2 top25-sm23" style="padding-top: 10px;"><spring:message code="lblDays" /></div>
			 	 </div>
			 	 
			 	 <div class="row">
			     	<div class="col-sm-6 col-md-6 top5">
						<spring:message code="msgDaysBtwReminder" />
					</div>
					
					<div class="col-sm-3 col-md-3 top5">
						<form:input path="reminderOfFirstFrequencyInDays" id="reminderOfFirstFrequencyInDays" cssClass="form-control" maxlength="3" placeholder="" onkeypress="return checkForInt(event);"/>
					</div>
					<div class="col-sm-2 col-md-2 top25-sm23" style="padding-top: 10px;"><spring:message code="lblDays" /></div>
			     </div>
				
			   <div class="row"> 
			        <div class="col-sm-6 col-md-6 top5">
			        	<spring:message code="msgActionPerformed1" />
			        </div>
			        <div class="col-sm-3 col-md-3 top5">
			        	<select class="form-control" id="statusIdReminderExpireAction" name="statusIdReminderExpireAction">
				            <option value=""><spring:message code="msgNoactiontaken" /> </option>
		                    <c:forEach items="${lstStatusMasterRem_PFC}" var="lstsmForReminder">
								<c:set var="selected" value=""></c:set>						
				        		<c:if test="${lstsmForReminder.statusId eq statusIdReminderExpireAction}">
				         			<c:set var="selected" value="selected"></c:set>
				        		</c:if>
								<option value="${lstsmForReminder.statusId}" ${selected} >${lstsmForReminder.status}</option>
							</c:forEach>
		              </select>
			        </div>
			    </div>
			 </div>
			 
			 <!-- Send Reminder Email for Portfolio -->
			 <div class="row">
			   <div class="span12 top5">
			        <form:checkbox path="sendReminderForPortfolio" cssClass="checkbox inline" style="vertical-align: bottom" id="sendReminderForPortfolio" onclick="showPortfolioReminderDiv();"/>
			        <span style="display: inline-block;">Send Reminder(s) for Portfolio applicant(s)</span>
			    </div>
			</div>
			
			<div class="left15 hide" id="sendFrequencyReminderForPortfolio">
			 	<div class="row">
					<div class="col-sm-12 col-md-12">
						<div class="divErrorMsg" id="erroroReminderSetPortfolio"></div>
				   </div>
				</div>
				
				<div class="row">
			        <div class="col-sm-6 col-md-6 top5">
			        	Total number of Portfolio Reminders to be sent
			        </div>
			        <div class="col-sm-3 col-md-3 top5">
			        	<form:input path="noOfReminderPortfolio" id="noOfReminderPortfolio" cssClass="form-control" maxlength="3" placeholder="" onkeypress="return checkForInt(event);"/>
			         </div>
				</div>
			 
			 	 <div class="row">
			 		<div class="col-sm-6 col-md-6 top5">
			        	<spring:message code="msgNoOfDaysRemain" /> 
			        </div>
			        <div class="col-sm-3 col-md-3 top5">
			        	<form:input path="reminderFrequencyInDaysPortfolio" id="reminderFrequencyInDaysPortfolio" cssClass="form-control" maxlength="3" placeholder="" onkeypress="return checkForInt(event);"/>
			        </div>
			        <div class="col-sm-2 col-md-2 top25-sm23" style="padding-top: 10px;"><spring:message code="lblDays" /></div>
			 	 </div>
			 	 
			 	 <div class="row">
			     	<div class="col-sm-6 col-md-6 top5">
						<spring:message code="msgDaysBtwReminder" />
					</div>
					
					<div class="col-sm-3 col-md-3 top5">
						<form:input path="reminderOfFirstFrequencyInDaysPortfolio" id="reminderOfFirstFrequencyInDaysPortfolio" cssClass="form-control" maxlength="3" placeholder="" onkeypress="return checkForInt(event);"/>
					</div>
					<div class="col-sm-2 col-md-2 top25-sm23" style="padding-top: 10px;"><spring:message code="lblDays" /></div>
			     </div>
				
			   <div class="row"> 
			        <div class="col-sm-6 col-md-6 top5">
			        	Action to be performed if applicant does not respond after Portfolio Reminder(s)
			        </div>
			        <div class="col-sm-3 col-md-3 top5">
			        <select class="form-control" id="statusIdReminderExpireActionPortfolio" name="statusIdReminderExpireActionPortfolio">
				            <option value=""><spring:message code="msgNoactiontaken" /> </option>
		                    <c:forEach items="${lstStatusMaster_PFC}" var="lstsmForReminder">
								<c:set var="selected" value=""></c:set>	
								<c:if test="${statusIdReminderExpireActionPortfolio ne null and statusIdReminderExpireActionPortfolio ne ''}">					
					        		<c:if test="${lstsmForReminder.statusId eq statusIdReminderExpireActionPortfolio}">
					         			<c:set var="selected" value="selected"></c:set>
					        		</c:if>
				        		</c:if>
								<option value="${lstsmForReminder.statusId}" ${selected} >${lstsmForReminder.status}</option>
							</c:forEach>
		              </select>
			        </div>
			    </div>
			    
			    <div class="row"> 
			        <div class="col-sm-6 col-md-6 top5">
			        	Send mail to applicant on following status
			        </div>
			        <div class="col-sm-3 col-md-3 top5">
			        	<select class="form-control" id="statusSendMailPortfolio" name="statusSendMailPortfolio" multiple>
				            <option value=""><spring:message code="msgNoactiontaken" /> </option>
		                    <c:forEach items="${lstStatusMaster_PFC}" var="lstsmForReminder">
								<c:set var="selected" value=""></c:set>
								<c:if test="${statusListPortfolio ne null}">						
				        			<c:forEach items="${statusListPortfolio}" var="statusListPortfolio1">
				        				<c:if test="${lstsmForReminder.statusId eq statusListPortfolio1}">
						         			<c:set var="selected" value="selected"></c:set>
						        		</c:if>
				        			</c:forEach>
				        			</c:if>
								<option value="${lstsmForReminder.statusId}" ${selected} >${lstsmForReminder.status}</option>
							</c:forEach>
							
							<c:forEach items="${lstsecondaryStatus_PFC}" var="lstSecSMForReminder">
								<c:set var="selected" value=""></c:set>
								
								<c:if test="${secondaryStatusListPortfolio ne null}">						
				        			<c:forEach items="${secondaryStatusListPortfolio}" var="secondaryStatusListPortfolio1">
				        				<c:if test="${lstSecSMForReminder.secondaryStatusId eq secondaryStatusListPortfolio1}">
						         			<c:set var="selected" value="selected"></c:set>
						        		</c:if>
				        			</c:forEach>
				        			</c:if>
								<option value="SSIDForReminder_${lstSecSMForReminder.secondaryStatusId}" ${selected} >${lstSecSMForReminder.secondaryStatusName}</option>
							</c:forEach>
							
		              </select>
			        </div>
			    </div>
			 </div>
			 <!-- End Section -->
			 
			 <div class="row">
		           <div class="span12 top5">
		                <c:if test="${headQuarterId==2}">	 
		                   <form:checkbox path="sendReferenceOnJobComplete" cssClass="checkbox inline" style="vertical-align: bottom" disabled = "disabled" id="sendReferenceOnJobCompleteId" value="1"  onclick="SendReferenceFinalizeStatusHide(this);"/>
						</c:if>
						<c:if test="${headQuarterId !=2}">	 
		                   <form:checkbox path="sendReferenceOnJobComplete" cssClass="checkbox inline" style="vertical-align: bottom" disabled = "disabled" id="sendReferenceOnJobCompleteId" value="1" />
						</c:if>
						
						<span style="display: inline-block;"><spring:message code="msgSendeReference" /></span>
					</div>
			</div>
				
				<!--  Send --Reference on Finalize status -->
				<div class="row top10" id="SendReferenceFinalizeStatusId">
				  
				  <c:if test="${headQuarterId !=2}">	
					<div class="col-sm-6 col-md-6 top2" style="margin-left: -12px;">
						<spring:message code="msgSendeReference1" />
					</div>
				  </c:if>	
					
					<c:if test="${headQuarterId ==2}">
					  <div class="col-sm-6 col-md-6 top2" style="margin-left: -15px;">
					   <form:checkbox path="" cssClass="checkbox inline" style="vertical-align: bottom" id="sendEReferenceId" value="1"  onclick="sendEReference(this);"/>
						<spring:message code="msgSendeReference1" />
					</div>
					</c:if>
					
					<div class="col-sm-4 col-md-4 top25-sm23" style="margin-left: 20px;">
					<select class="form-control" id="statusListBoxForReminder" name="statusListBoxForReminder">
		            <option value=""></option>
                       <c:forEach items="${lstStatusMaster_PFC}" var="lstsmForReminder">
						<c:set var="selected" value=""></c:set>						
		        		<c:if test="${lstsmForReminder.statusId eq statusListBoxValueForReminder}">	        			
		         			<c:set var="selected" value="selected"></c:set>	         			
		        		</c:if>	
						<option value="${lstsmForReminder.statusId}" ${selected} >${lstsmForReminder.status}</option>
					</c:forEach>
					
				 	<c:set var="lstvalueForReminder" value=""></c:set>
					<c:forEach items="${lstsecondaryStatus_PFC}" var="lstSecSMForReminder">
						<c:set var="selected" value=""></c:set>
						<c:if test="${lstSecSMForReminder.secondaryStatusId eq statusListBoxValueForReminder}">	        			
		         			<c:set var="selected" value="selected"></c:set>	         			
		        		</c:if>
						<option value="SSIDForReminder_${lstSecSMForReminder.secondaryStatusId}" ${selected} >${lstSecSMForReminder.secondaryStatusName}</option>
					</c:forEach>
				
                     </select>
					 </div>                      
				</div>
				
			<!-- End Finalize status -->
						
			<!-- Start of Negative QQ -->
			<div class="row top10" >
				<div class="span12 top5">
					<form:checkbox path="sendNotificationOnNegativeQQ" cssClass="checkbox inline" style="vertical-align: bottom" disabled = "disabled" id="sendNotificationOnNegativeQQId" value="" onclick="displayAllActiveDistrictUser()"/>
					<span style="display: inline-block;"><spring:message code="msgSendNotfGiveNegQlf" /> </span>
			</div>
			
			<!-- flag Start of Automatic deactive job when expected hire full -->
			<div class="row top10" >
				<div class="span12 top5">
					<form:checkbox path="autoInactiveOrNot" cssClass="checkbox inline" style="vertical-align: bottom"  id="autoInactiveOrNotId" value="" />
					<span style="display: inline-block;"><spring:message code="lblAutoInactiveJobMSG" /> </span>
			</div>
				
				<div class="col-sm-12 col-md-12 top25-sm23" style="margin-left: -10px;" id="DistrictAdminListDiv" name="DistrictAdminListDiv">
					<div class="row left5">
						<div style="  margin-left: 16px; margin-top: 5px; margin-bottom: 5px;"> <spring:message code="msgSendnotificationtoAll" /> </div>
						<div class="col-sm-5 col-md-5">
							<select multiple id="1stDistrictAdminList" name="1stDistrictAdminList" class="form-control" style="height: 150px;" > </select>
						</div>
						<div class="col-sm-1 col-md-1 left20-sm">
							<div class="span2"> <span id="addUser" style="cursor:pointer;"><img alt="" src="images/rightarrow.jpg" width="30px" onclick="shiftElementsFromOneDivToSecondDiv('1stDistrictAdminList','2ndDistrictAdminList')"> </span></div>
							<div class="span2"> <span id="removeUser" style="cursor:pointer;"><img alt="" src="images/leftarrow.jpg" width="30px" onclick="shiftElementsFromOneDivToSecondDiv('2ndDistrictAdminList','1stDistrictAdminList')"> </span></div>
						</div>
						
						<div class="col-md-5 col-md-5" style="border: 0px solid green;">
							<select multiple class="form-control" id="2ndDistrictAdminList" name="2ndDistrictAdminList" style="height: 150px;"> </select>
						</div>
					</div>
				</div>
			</div>
			<!-- End of Negative QQ -->
			
			<!-- Start Mosaic Setting -->
			<div class="row mt10">
              		<div class="span11">
              		<div class='required' id='errorpdistictdiv'></div>
                		<spring:message code="lblDisplayCandidateTOMosaic" />
              		</div>
            </div>
			<div class="row  mt8" >
               	 <div class="span12 left20">
               	 	<label class="radio inline" style="padding-left: 0px;">
               		<c:choose>
               				<c:when test="${displayCandidateTOMosaic eq true}">
               					<input type="radio"  name="displayCandidateTOMosaic" id="allCandidate" checked="checked" value="1"/>
				         	  	<spring:message code="lblAllCandidates" />
               				</c:when>
               				<c:otherwise>
               					<input type="radio"  name="displayCandidateTOMosaic" id="allCandidate" value="1" checked="checked"/>
				         	  	<spring:message code="lblAllCandidates" />
               				</c:otherwise>
               		</c:choose>
               		</label>
               		<br>
               		<label class="radio inline" style="padding-left: 0px;">
               		<c:choose>
               				<c:when test="${displayCandidateTOMosaic eq false}">
               					 <input type="radio" name="displayCandidateTOMosaic" id="selectedCandidate" checked="checked" value="0"/>
				           		 <spring:message code="lblSelectedCandidates" />
               				</c:when>
               				<c:otherwise>
               					 <input type="radio" name="displayCandidateTOMosaic" id="selectedCandidate" value="0"/>
				            	 <spring:message code="lblSelectedCandidates" />
               				</c:otherwise>
               		</c:choose>
               		</label>
               	</div>	
                </div>
			<!-- Ends -->
			<!-- submenu visible as a parent -->
				<div class="row top10">
					<div class="checkbox span12 top5">
						<form:checkbox path="displaySubmenuAsaParentMenu" />
                		<span>If only one sub menu exists under parent menu then display sub menu as a parent menu.</span>
              		</div>
              	</div>
			<!-- end -->
			<!-- Start Configurable column -->
			<div class="row top10">
              		<div class="span12">
              		<div class='required' id='districtJobOrderColumnCnfg'></div>
                		Display selected columns in District Job Order	
                		<form:hidden path="displayCNFGColumn" />  
              		</div>
					<div class="left15">
					<div class="col-sm-3 col-md-3 top5" style="padding-left: 20px;">
                      <label class="checkbox inline" style="padding-left: 0px;">
						 <input type="checkbox" name="displayCNFGCol" value="JID" />
                        <spring:message code="lblJobId" />
                       </label>
                    </div>
                    <div class="col-sm-3 col-md-3 top5" style="padding-left: 20px;">
                      <label class="checkbox inline" style="padding-left: 0px;">
						 <input type="checkbox" name="displayCNFGCol" value="TIT" />
                        <spring:message code="lblTitle" />
                       </label>
                    </div>
              		<div class="col-sm-3 col-md-3 top5" style="padding-left: 20px;">
                      <label class="checkbox inline" style="padding-left: 0px;">
						 <input type="checkbox" name="displayCNFGCol" value="SCH" />
                        <spring:message code="lblSchool" />
                       </label>
                    </div>
                    <c:if test="${empty userSession.headQuarterMaster || (not empty userSession.headQuarterMaster  && userSession.headQuarterMaster.headQuarterId ne 2)}">
                    <div class="col-sm-3 col-md-3 top5" style="padding-left: 20px;">
                      <label class="checkbox inline" style="padding-left: 0px;">
						 <input type="checkbox" name="displayCNFGCol" value="ZON" />
                        <spring:message code="lblZone" />
                       </label>
                    </div>
                    <div class="col-sm-3 col-md-3 top5" style="padding-left: 20px;">
                      <label class="checkbox inline" style="padding-left: 0px;">
						 <input type="checkbox" name="displayCNFGCol" value="SUB" />
                        <spring:message code="lblSub" />
                       </label>
                    </div>
                    </c:if>
                    <div class="col-sm-3 col-md-3 top5" style="padding-left: 20px;">
                      <label class="checkbox inline" style="padding-left: 0px;">
						 <input type="checkbox" name="displayCNFGCol" value="STA" />
                        <spring:message code="optStatus" />
                       </label>
                    </div>
                    <div class="col-sm-3 col-md-3 top5" style="padding-left: 20px;">
                      <label class="checkbox inline" style="padding-left: 0px;">
						 <input type="checkbox" name="displayCNFGCol" value="POSU" />
						 <spring:message code="lblPostedUntil" />
                       </label>
                    </div>
                    <div class="col-sm-3 col-md-3 top5" style="padding-left: 20px;">
                      <label class="checkbox inline" style="padding-left: 0px;">
						 <input type="checkbox" name="displayCNFGCol" value="POS" />
                      	<spring:message code="msgPositions" />
                       </label>
                    </div>
                    <div class="col-sm-3 col-md-3 top5" style="padding-left: 20px;">
                      <label class="checkbox inline" style="padding-left: 0px;">
						 <input type="checkbox" name="displayCNFGCol" value="APP" />
                        <spring:message code="lblApplicants" />
                       </label>
                    </div>
                    <div class="col-sm-3 col-md-3 top5" style="padding-left: 20px;">
                      <label class="checkbox inline" style="padding-left: 0px;">
						 <input type="checkbox" name="displayCNFGCol" value="HPER" />
                        <spring:message code="lblHires" />
                       </label>
                    </div>
                    <div class="col-sm-3 col-md-3 top5" style="padding-left: 20px;">
                      <label class="checkbox inline" style="padding-left: 0px;">
						 <input type="checkbox" name="displayCNFGCol" value="CANG" />
                        <spring:message code="lblCandidateGrid" />
                       </label>
                    </div>
                    <!--
                    <div class="col-sm-3 col-md-3 top5" style="padding-left: 20px;">
                      <label class="checkbox inline" style="padding-left: 0px;">
						 <input type="checkbox" name="displayCNFGCol" value="REQ" />Position/Requisition Number
                        <spring:message code="lblCGPA" />
                       </label>
                    </div>
                   -->
                   <div class="col-sm-3 col-md-3 top5" style="padding-left: 20px;">
                      <label class="checkbox inline" style="padding-left: 0px;">
						 <input type="checkbox" name="displayCNFGCol" value="ATT" />
                        <spring:message code="lblDistrictAttachment" />
                       </label>
                    </div>
                    <div class="col-sm-3 col-md-3 top5" style="padding-left: 20px;">
                      <label class="checkbox inline" style="padding-left: 0px;">
						 <input type="checkbox" name="displayCNFGCol" value="ACT" />
                        <spring:message code="lblAct" />
                       </label>
                    </div>
                    <div class="col-sm-3 col-md-3 top5" style="padding-left: 20px;">
                      <label class="checkbox inline" style="padding-left: 0px;">
						 <input type="checkbox" name="displayCNFGCol" value="SADAT" />
                        <spring:message code="lblActivationDate" />
                       </label>
                    </div>
					</div>
            </div>
			<!-- END Configurable column -->
			<!-- Start Assessment Notifications To Candidates Setting -->
            <div class="row mt10">
            	<div class="span11">
              		<div class='required' id='errorpdistictdiv'></div>
               		<spring:message code="lblAssessmentNotifications" />
				</div>
			</div>
			<div class="row mt8" >
            	<div class="span12 left20">
            		<c:choose>
               			<c:when test="${epiSetting eq 'null'}">
               				<label class="checkbox" style="margin-bottom: 0px;">
            					<input type="checkbox" name="epiSettingChk" id="epiSettingChk" value="" onclick="showAssessmentNotifications(1)"/>
            					<span style="display: inline-block;"><spring:message code="epiSetting" /> </span>
            				</label>
               				<div class="left15 hide" id="epiSettingDiv">
		 						<div class="row">
									<div class="col-sm-12 col-md-12">
										<label class="radio-inline" style="margin-top: 0px;">
											<form:radiobutton path="epiSetting" id="epiSetting1" value="0" /><spring:message code="lblNone" />
										</label>
										<label class="radio-inline" style="margin-top: 0px;">
											<form:radiobutton path="epiSetting" id="epiSetting2" value="1" /><spring:message code="lblCompleted" />
										</label>
										<label class="radio-inline" style="margin-top: 0px;">
											<form:radiobutton path="epiSetting" id="epiSetting3" value="2" /><spring:message code="optAll" />
										</label>
					   				</div>
								</div>
							</div>
               			</c:when>
               			<c:otherwise>
               				<label class="checkbox" style="margin-bottom: 0px;">
            					<input type="checkbox" name="epiSettingChk" id="epiSettingChk" checked="checked" value="" onclick="showAssessmentNotifications(1)"/>
            					<span style="display: inline-block;"><spring:message code="epiSetting" /> </span>
            				</label>
               				<div class="left15" id="epiSettingDiv">
		 						<div class="row">
									<div class="col-sm-12 col-md-12">
										<label class="radio-inline" style="margin-top: 0px;">
											<form:radiobutton path="epiSetting" id="epiSetting1" value="0" /><spring:message code="lblNone" />
										</label>
										<label class="radio-inline" style="margin-top: 0px;">
											<form:radiobutton path="epiSetting" id="epiSetting2" value="1" /><spring:message code="lblCompleted" />
										</label>
										<label class="radio-inline" style="margin-top: 0px;">
											<form:radiobutton path="epiSetting" id="epiSetting3" value="2" /><spring:message code="optAll" />
										</label>
					   				</div>
								</div>
							</div>
               			</c:otherwise>
               		</c:choose>
					<c:choose>
						<c:when test="${jsiSetting eq 'null'}">
							<label class="checkbox" style="margin-bottom: 0px;">
								<input type="checkbox" name="jsiSettingChk" id="jsiSettingChk" value="" onclick="showAssessmentNotifications(2)"/>
								<span style="display: inline-block;"><spring:message code="jsiSetting" /> </span>
							</label>
               				<div class="left15 hide" id="jsiSettingDiv">
				 				<div class="row">
									<div class="col-sm-12 col-md-12">
										<label class="radio-inline" style="margin-top: 0px;">
											<form:radiobutton path="jsiSetting" id="jsiSetting1" value="0" /><spring:message code="lblNone" />
										</label>
										<label class="radio-inline" style="margin-top: 0px;">
											<form:radiobutton path="jsiSetting" id="jsiSetting2" value="1" /><spring:message code="lblCompleted" />
										</label>
										<label class="radio-inline" style="margin-top: 0px;">
											<form:radiobutton path="jsiSetting" id="jsiSetting3" value="2" /><spring:message code="optAll" />
										</label>
					   				</div>
								</div>
							</div>
               			</c:when>
               			<c:otherwise>
               				<label class="checkbox" style="margin-bottom: 0px;">
								<input type="checkbox" name="jsiSettingChk" id="jsiSettingChk" checked="checked" value="" onclick="showAssessmentNotifications(2)"/>
								<span style="display: inline-block;"><spring:message code="jsiSetting" /> </span>
							</label>
               				<div class="left15" id="jsiSettingDiv">
				 				<div class="row">
									<div class="col-sm-12 col-md-12">
										<label class="radio-inline" style="margin-top: 0px;">
											<form:radiobutton path="jsiSetting" id="jsiSetting1" value="0" /><spring:message code="lblNone" />
										</label>
										<label class="radio-inline" style="margin-top: 0px;">
											<form:radiobutton path="jsiSetting" id="jsiSetting2" value="1" /><spring:message code="lblCompleted" />
										</label>
										<label class="radio-inline" style="margin-top: 0px;">
											<form:radiobutton path="jsiSetting" id="jsiSetting3" value="2" /><spring:message code="optAll" />
										</label>
					   				</div>
								</div>
							</div>
               			</c:otherwise>
               		</c:choose>
               	</div>
			</div>
			<!-- End Assessment Notifications To Candidates Setting -->
			
			<!-- start Default PDP  Setting -->
			<div class="row mt10">
							<div class="span11">
							  <spring:message code="msgdefaultpdp" />	
							</div>							
		   </div>
					<div class="row" style="display: block; margin-left: 10px; ">
						<div id="defaultPDPsetting">
							<div class="col-sm-12 col-md-12">
								<label class="radio inline" style="padding-left: 5px;">
									<form:radiobutton path="defaultPDP"
										id="defaultPDP" value="1" />
									<spring:message code="msgdefaultpdpEPI" />
								</label>
							</div>
							<div class="col-sm-12 col-md-12">
								<label class="radio inline" style="padding-left: 5px;">
									<form:radiobutton path="defaultPDP"
										id="defaultPDP" value="2" />
									<spring:message code="msgdefaultpdpIPI" />
								</label>
							</div>
							<div class="col-sm-12 col-md-12">
								<label class="radio inline" style="padding-left: 5px;">
									<form:radiobutton path="defaultPDP"
										id="defaultPDP" value="3" disabled="true"/>
									<spring:message code="msgdefaultpdpSP" />
								</label>
							</div>
						</div>
					</div>
			<!-- End Default PDP  Setting -->
			<!-- Start Self Service Portfolio Apply on Job -->
			<c:if test="${userSession.entityType eq 1}">
				<div class="row mt8" >
	            	<div class="span12">
	            		<label class="checkbox" style="margin-bottom: 0px;">
	            		<form:checkbox path="selfServicePortfolioStatus" id="selfServicePortfolioStatus" value="1"/>
	            		<span><spring:message code="DSPQ_lblDistSelfServiceMsg" /></span>
	            		</label>
	            	</div>
            	</div>
			</c:if>
			
			<!-- End Self Service Portfolio Apply on Job -->
			
              </div>
              <div class="clearfix">&nbsp;</div>
            </div>
          </div>
        </div> 
         <%-- ================ rajendra : End of accordion Div for Privilege For District ======================  --%>
        
        <!--------- Start ... Adding accordion Div for Status ----------------->
	    <div class="panel panel-default">
          <div class="accordion-heading"> <a href="#collapseEle" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle plus"><spring:message code="msgPrivilegeForSchool" /></a></div>
          <div class="accordion-body collapse" id="collapseEle" style="height: 0px;">
            <div class="accordion-inner">
    	      <div class="offset1">
    	      	
    	      	<div class="row mt10">
                  		<div class="span11">
                    		<spring:message code="msgRWPrivilege" /> <!-- <a href="#" id="iconpophovercg" rel="tooltip" data-original-title="Check the fields you want to view in Candidate Grid" data-placement="right"><img src="images/qua-icon.png" width="15" height="15" alt=""></a> -->
                  		</div>
                </div>
                
                <div class="row  mt10">                    
                    <div class="span12 left20">
				 	<label class="checkbox inline" style="padding-left: 0px;">
			            <form:checkbox path="writePrivilegeToSchool" value="1"/>
			            <spring:message code="msgDistAnAttchSchR/WPriv" />
					</label>
					</div>                    
                </div>
                
             <!-- @Start
                  @Ashish Kumar
                  @Description :: Attach  Read/write privilege for qualification issues -->
	                <div class="row  mt10">
	                    <div class="span12 left20">
					 	<label class="checkbox inline" style="padding-left: 0px;">
				            <form:checkbox path="resetQualificationIssuesPrivilegeToSchool" id='qqThumbAndContent'/>
				            <spring:message code="msgAttachedschoolqualification" />
						</label>
						</div>
	                </div>
             <!-- @End
                  @Ashish Kumar
                  @Description :: Attach  Read/write privilege for qualification issues -->  
                  
                  <div class="row mt10">
	                   <div class="span12 left20">
		                   <label class="checkbox inline" style="padding-left: 0px;">
				               	<form:checkbox path="qqThumbShowOrNot"  id="qqThumbShowOrNot" value="1" />
					             <spring:message code="msgTmInveLstepJoAppAftrCompl44"/> <!-- SAs can view QQ thumb on candidates -->
				            </label>
			            </div>
				 </div>
				 
				 <div class="row mt10">
	                   <div class="span12 left20">
		                   <label class="checkbox inline" style="padding-left: 0px;">
				               	<form:checkbox path="sACreateDistJOb"  id="sACreateDistJOb" name="sACreateDistJOb" value="1" />
					             <spring:message code="msgTmInveLstepJoAppAftrCompl45"/>
				            </label>
			            </div>
				 </div>
				<!-- New checkbox ******-->
                 <div class="row mt10">
	                   <div class="span12 left20">
		                   <label class="checkbox inline" style="padding-left: 0px;">
				               	<form:checkbox path="sAEditUpdateJob"  id="sAEditUpdateJob" name="sAEditUpdateJob"/>
					             <spring:message code="lblSchoolAEditUpdateJO" />
				            </label>
			            </div>
				 </div>
				 <!-- END****-->
                 <div class="row mt10">
	                   <div class="span12 left20">
		                   <label class="checkbox inline" style="padding-left: 0px;">
		                   		<form:checkbox path="sACreateEvent"  id="sACreateEvent" name="sACreateEvent" value="1" />
					            <spring:message code="msgTmInveLstepJoAppAftrCompl46"/>
					             <a href="#" id="iconpophover23" rel="tooltip" data-original-title='<spring:message code="listPleaseSelectQQSetForonboarding77"/>'>
					             	<img src="images/qua-icon.png" width="15" height="15" alt="">
					             </a> 
				            </label>
			            </div>
				 </div>
				  
                 <div class="row mt10">   
                    <div class="col-sm-12 col-md-12" style="margin-left: -15px;">
			            <form:checkbox path="statusPrivilegeForSchools" cssClass="checkbox inline" style="vertical-align: bottom" id="statusPrivilegeForSchools" value="1" onclick="disableStatusMaster()" />
			        <span style="display: inline-block;"><spring:message code="msgSchoolcanSetStatus" /></span>
			            
					</div>
				
					<!-- ******************************** -->
					
		         	<%-- <c:if test="${displayRecordFlagFromDistrictMaxFitScoreTable eq 0}"> --%>
		         	<div class="left15">
                    	 <c:forEach items="${lstStatusMaster_PFC}" var="lstsm">
	                  		<div class="col-sm-3 col-md-3 top5" style="padding-left: 20px;">
								<label class="checkbox inline" style="padding-left: 0px;">
									<form:checkbox path="chkStatusMaster" id="chkStatusMaster" value="${lstsm.statusId}" />
						            <%--  <input type="checkbox" name="chkStatusMaster" value="${lstsm.statusId}" />--%>
						            ${lstsm.status}
								</label>
							</div>
						 </c:forEach>
						
						 <c:forEach items="${lstsecondaryStatus_PFC}" var="lstSecSM">
							<div class="col-sm-3 col-md-3 top5" style="padding-left: 20px;">
								<label class="checkbox inline" style="padding-left: 0px;">
									<form:checkbox path="chkSecondaryStatusName" id="chkSecondaryStatusName" value="${lstSecSM.secondaryStatusId}" />
						           <%--  <input type="checkbox" name="chkSecondaryStatusName" value="${lstSecSM.secondaryStatusId}" />--%>
						            ${lstSecSM.secondaryStatusName}
								</label>
							</div>
						 </c:forEach>
						 </div>
					<%--  </c:if> --%>
					
					<%-- 
					<c:if test="${displayRecordFlagFromDistrictMaxFitScoreTable eq 1}">
	                   	 <c:forEach items="${lstStatusMaster}" var="lst">
							<div class="span3" style="width:150px;">
								<label>${lst.statusMaster.status} -3 sm</label>
							</div>
						 </c:forEach>
								
						 <c:forEach items="${lstsecondaryStatus}" var="lstSec">
					  		<div class="span3" style="width:150px;">
								<label>${lstSec.secondaryStatus.secondaryStatusName} -4 ssn</label>
							</div>
						 </c:forEach>
					</c:if>
					--%>
					<!-- ******************************** -->
					
					
					
					
                </div>
                  
    	      	<div class="row mt10">
                 	<div class="col-sm-6 col-md-6 top5" style="max-width:430px;">
			            <spring:message code="msgSchoolCanViewTeacher" /> 
			        </div>    
			            <div class="col-sm-3 col-md-3 top25-sm23">
			            <select class="form-control" id="statusListBox" name="statusListBox">
			            <option value=""></option>
                        <c:forEach items="${lstStatusMaster_PFC}" var="lstsm">
							<c:set var="selected" value=""></c:set>						
			        		<c:if test="${lstsm.statusId eq statusListBoxValue}">	        			
			         			<c:set var="selected" value="selected"></c:set>	         			
			        		</c:if>	
							<option value="${lstsm.statusId}" ${selected} >${lstsm.status}</option>
						</c:forEach>
						
						<c:set var="lstvalue" value=""></c:set>
						<c:forEach items="${lstsecondaryStatus_PFC}" var="lstSecSM">
							<c:set var="selected" value=""></c:set>
							<c:set var="lstvalue" value="SSID_${lstSecSM.secondaryStatusId}"></c:set>
							<c:if test="${lstvalue eq statusListBoxValue}">	        			
			         			<c:set var="selected" value="selected"></c:set>	         			
			        		</c:if>
							<option value="SSID_${lstSecSM.secondaryStatusId}" ${selected} >${lstSecSM.secondaryStatusName}</option>
						</c:forEach>
                      </select>
                      </div>                      
			       <div class="col-sm-3 col-md-3 top5 top25-sm23">
			           &nbsp;<spring:message code="lblStaAgThTech" />
					</div>
                </div>
						      
						
                <div class="row mt10">
                	Tag Management
                	<div class="col-sm-12 col-md-12">
						<div class="span11">
						     <label class="checkbox"> <form:checkbox path="isCreateTags" />Create tags</label>
						</div>
				   </div>
				   <div class="col-sm-12 col-md-12">
						<div class="span11">
						     <label class="checkbox"> <form:checkbox path="isEditTags" />Edit tags</label>
						</div>
				   </div>
				   <div class="col-sm-12 col-md-12">
						<div class="span11">
						     <label class="checkbox"><form:checkbox path="isRemoveTags" />Activate/Deactivate</label>
						</div>
				   </div>
				   <div class="col-sm-12 col-md-12">
						<div class="span11">
						     <label class="checkbox"><form:checkbox path="isDistrictWideTags" />Apply/Remove a district-wide tag to/from an applicant</label>
						</div>
				   </div>
				   <div class="col-sm-12 col-md-12">
						<div class="span11">
						     <label class="checkbox"> <form:checkbox path="isJobSpecificTags" /> Apply/Remove a job specific tag to/from an applicant</label>
						</div>
				   </div>
                </div>
            
			       <div class="row" >
			     	    E-Reference icon to SAs
				        <div class="col-sm-12 col-md-12 top6">
				            <label class="radio" style="margin-top: 0px;">
						    <form:radiobutton path="showReferenceToSA" value="2" />
				                 Show e-Reference icon only
				            </label>
				        </div>
				                    
				        <div class="col-sm-12 col-md-12">
				            <label class="radio"  style="margin-top: -8px;">
							<form:radiobutton path="showReferenceToSA"  value="1"/>
				                 Show e-Reference icon and allow actions
				            </label>
				        </div>
			 	    </div>
		 	    
    	      </div>
              <div class="clearfix">&nbsp;</div>
            </div>
          </div>
        </div>    
        
         <!--------- End ... Adding accordion Div for Status -----------------> 
         
         <%-- ================ rajendra : Start of accordion Div for Privilege For Interrnal Candidates =======  --%>
         <div class="panel panel-default">
          <div class="accordion-heading"> <a href="#collapseFifteen" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle plus"><spring:message code="msgPrivforIntTransf" /></a></div>
          <div class="accordion-body collapse" id="collapseFifteen" style="height: 0px;">
            <div class="accordion-inner">
    	      <div class="offset1">
	    	      	<div class="row mt10">
	                  		<div class="span11">
	                    		 <spring:message code="msgWhenInternlCand" />
	                  		</div>
	                </div>
	                <div class="row  mt8" >
	                	 <div class="span12 left20">
	                	 	<label class="checkbox inline" style="padding-left: 0px;">
	                	 		<form:checkbox path="offerPortfolioNeeded"/><spring:message code="lblOfrPortf" /></br>
	                	 		<form:checkbox path="offerDistrictSpecificItems"/><spring:message code="lblOfDistSpeMandItm" /></br>
	                	 		<form:checkbox path="offerQualificationItems"/><spring:message code="lblOfQualiItm" /></br>
	                	 		<form:checkbox path="offerEPI"/><spring:message code="msgOfferEPI" /></br>
	                	 		<form:checkbox path="offerJSI"/><spring:message code="msgOfferEPI" /></br>
	       					</label>
	                	</div>	
	               </div>
	             </div>
              <div class="clearfix">&nbsp;</div>
            </div>
          </div>
        </div>
         
        <%-- ================ rajendra : End of accordion Div for Privilege For Interrnal Candidates =======  --%>
         
        <%-- ================ rajendra : End of accordion Div for Privilege For Domain =======  --%>
        <div class="panel panel-default">
          <div class="accordion-heading"> <a href="#collapseSixteen" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle plus"><spring:message code="msgDistrictDomain1" /></a></div>
          <div class="accordion-body collapse" id="collapseSixteen" style="height: 0px;">
            <div class="accordion-inner">
    	      <div class="offset1">
    	      
    	         <div class="row">
                  <div class="col-sm-8 col-md-8">                  
                      <div class="pull-right">
                      <a href="javascript:void(0);"  onclick="addDistrictDomain();"><spring:message code="lnAddDomain" /></a>
                      </div>                  
                   </div>
                  </div>
                
                <div class="row  mt5" >
                	<div class="col-sm-8 col-md-8">			                         
 						<div id="domainGrid"  onclick="getSortDomainGrid()"></div>
	  				</div>
                </div>
                
                <div class="row  mt15" id="domainDiv" >
                	<div class="col-sm-12 col-md-12">			                         
 						<div class='divErrorMsg' id='domainErrorDiv' ></div>
	  				</div>
                	 <div class="col-sm-4 col-md-4">
                	 	<label><spring:message code="lblDomainName" /><span class="required">*</span>
                	 		 <a href="#" id="iconpophover22" rel="tooltip"
                	 		  data-original-title='<spring:message code="msgEditDomain2" />'>
                	 		  <img src="images/qua-icon.png" width="15" height="15" alt=""></a>
                	 	</label>
                	 	<input type="hidden" id="districtDomainId"/>
                	 	<input type="text" class="form-control" id="domainName" maxlength="100" onkeypress="return chkForEnterSaveDistrictDomain(event);"/>
                	 </div>	
                	 <div class="clearfix"></div>
                	  <div class="col-sm-12 col-md-12 top5 idone" >
                  		<a href="javascript:void(0);" onclick="saveDistrictDomain();"><spring:message code="lnkImD" /></a>&nbsp;&nbsp;&nbsp;
                  		<a href="javascript:void(0);" onclick="clearDomain()"><spring:message code="lnkCancel" /></a>
                  	  </div>
    	      	  </div>	
    	      	  	 
                </div>
              <div class="clearfix">&nbsp;</div>
            </div>
          </div>
        </div> 
        <%-- ================ rajendra : End of accordion Div for Privilege For Domain =======  --%>
         
         <%-- ================ rajendra : End of accordion Div for Account =======  --%>
        <div class="panel panel-default">
          <div class="accordion-heading"> <a href="#collapseFour" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle plus"> <spring:message code="lblAccInfo" /> </a> </div>
          <div class="accordion-body collapse" id="collapseFour" style="height: 0px;">
            <div class="accordion-inner">
               <div class="offset1">
	                <div class="row">
		                <div class="">
								 	<div class='required' id='erroraccountinfodiv'></div>
								 	<div class='required' id='errordistrictschoolediv'></div>
								 	<div class='required' id='errornotediv'></div>
								 	<div class='required' id='errordatediv'></div>
						</div>	
					</div>	
					<div class="row">
		                <div class="col-sm-12 col-md-12" style="margin-left:-15px;"> 	
		                  <p> <spring:message code="msgInitialContractingProcess" /> <a href="mailto:clientservices@teachermatch.net">clientservices@teachermatch.net</a></p>
		                </div>
	                </div>
              </div>
              
                <c:if test="${userSession.entityType ne 1}">	
        			<c:set var="disable" value="true"></c:set>
         		</c:if>
              <div class="row">
                <div class="offset1">
                  <div class="row">
                        <div class="col-sm-3 col-md-3">
                        <label><spring:message code="lblDateInit" /></label>
						<form:input path="initiatedOnDate" disabled="${disable}" cssClass="form-control"  value="${initiatedOnDate}"/>
                        </div>
                        <div class="col-sm-3 col-md-3">
                        <label><spring:message code="lblcontStDate" /></label>
						<form:input path="contractStartDate" disabled="${disable}" cssClass="form-control"   value="${contractStartDate}"/>
                        </div>
                      	<div class="col-sm-3 col-md-3">
                        <label><spring:message code="lblContEdDate" /></label>
						<form:input path="contractEndDate" disabled="${disable}" cssClass="form-control"   value="${contractEndDate}"/>
                 		</div>
                 	</div>	
                  <div class="row">
                    <div class="col-sm-3 col-md-3">
                      <label><spring:message code="msgSchoolsUnderContract" /></label>
					  <form:input path="schoolsUnderContract" disabled="${disable}" cssClass="form-control" maxlength="9" placeholder=""/>
                    </div>
                    <div class="col-sm-3 col-md-3">
                      <label><spring:message code="lblTchUdrCont" /></label>
						<form:input path="teachersUnderContract" disabled="${disable}" cssClass="form-control" maxlength="9" placeholder=""/>
                    </div>
                    <div class="col-sm-3 col-md-3">
                      <label><spring:message code="lblStudUdrCont" /></label>
						<form:input path="studentsUnderContract"   disabled="${disable}" cssClass="form-control" maxlength="9" placeholder=""/>
                    </div>
                  </div>               
               
			<script>
				var $poststarttime = document.getElementById("postingStartTime");
				var $postendtime = document.getElementById("postingEndTime");		
				function saveposttimeonblur(element){
					if(element.id == "postingStartTime")
						document.getElementById("hidPostingStartTime").value = element.value;
					else if(element.id == "postingEndTime")
						document.getElementById("hidPostingEndTime").value = element.value;
				}
				if("${districtMaster.postingEndTime}" != ""){
					var def_opt = document.createElement("option");
					def_opt.selected = "selected";
					def_opt.text = "${districtMaster.postingEndTime}";
					def_opt.value="${districtMaster.postingEndTime}";
					$postendtime.appendChild(def_opt);
				}else{
					var def_opt = document.createElement("option");
					def_opt.selected = "selected";
					def_opt.text = "11:59 PM";
					def_opt.value="11:59 PM";
					$postendtime.appendChild(def_opt);
				}
				if("${districtMaster.postingStartTime}" != ""){
					var def_opt = document.createElement("option");
					def_opt.selected = "selected";
					def_opt.text = "${districtMaster.postingStartTime}";
					def_opt.value="${districtMaster.postingStartTime}";
					$poststarttime.appendChild(def_opt);
				}else{
					var def_opt = document.createElement("option");
					def_opt.selected = "selected";
					def_opt.text = "12:01 AM";
					def_opt.value="12:01 AM";
					$poststarttime.appendChild(def_opt);
				}
				function getpostingtimeoption(element){
					var opt_val = ["12:01 AM","12:30 AM","01:00 AM","01:30 AM","02:00 AM","02:30 AM","03:00 AM","03:30 AM","04:30 AM","05:00 AM","05:00 AM","05:30 AM","06:00 AM","06:30 AM","07:00 AM","07:30 AM","08:00 AM","08:30 AM","09:00 AM","09:30 AM","10:00 AM","10:30 AM","11:00 AM","11:30 AM","12:00 PM","12:30 PM","01:00 PM","01:30 PM","02:00 PM","02:30 PM","03:00 PM","03:30 PM","04:00 PM","04:30 PM","05:00 PM","05:30 PM","06:00 PM","06:30 PM","07:30 PM","08:00 PM","08:30 PM","09:00 PM","09:30 PM","10:00 PM","10:30 PM","11:00 PM","11:30 PM","11:59 PM"];
					$(element).find('option').remove().end();
					for (var i = 0; i < opt_val.length; i++) {
						var option = document.createElement("option");
						var temp_count = 0;
						if("${districtMaster.postingStartTime}" != "" && "${districtMaster.postingStartTime}" == opt_val[i] && element.id == "postingStartTime"){
							option.selected = "selected";
						}else if(opt_val[i] == "12:01 AM" && element.id == "postingStartTime" ){
							option.selected = "selected";
						}				
						if("${districtMaster.postingEndTime}" != "" && "${districtMaster.postingEndTime}" == opt_val[i] && element.id == "postingEndTime"){
							option.selected = "selected";
							temp_count++;
						}else if(opt_val[i] == "11:59 PM" && element.id == "postingEndTime"){
							if(temp_count > 0 || "${districtMaster.postingEndTime}" == "")
								option.selected = "selected";
						}				
						option.value = opt_val[i];
						option.text = opt_val[i];
						element.appendChild(option);
						if(document.getElementById("hidPostingStartTime").value != "" && element.id == "postingStartTime"){
							element.value =  document.getElementById("hidPostingStartTime").value;
						}else if(document.getElementById("hidPostingEndTime").value != "" && element.id == "postingEndTime"){
							element.value =  document.getElementById("hidPostingEndTime").value;
						}
					}
				}
			</script>     
				<!-- TPL-4992 --- Enable district to set district-wide job closing time at manage district end  -->         
               
                  <div class="row">
                    <div class="col-sm-3 col-md-3">
                      <label><spring:message code="msgAnnualSubscriptionAmount" /></label>
						<form:input path="annualSubsciptionAmount" disabled="${disable}" cssClass="form-control" maxlength="50" placeholder=""/>
                    </div>
                     <c:if test="${userSession.entityType eq 1}">	
                      	<div class="col-sm-3 col-md-3">
                      	<label><spring:message code="lblAuthKy" /></label>
							<input type="text" name="authKeyVal" readonly="readonly"  class="form-control"  value="${authKey}"  />
                    	</div>
                     </c:if>
                  </div>
                  
			<!-- timezone field in edit district account information page start -->
			<div class="row">
				<div class="col-sm-3 col-md-3">
					<input type="hidden" id="hidtimezone" value="${districtMaster.timezone}"/>
					<span class=""><label class="">Default Time Zone</label></span>
					<select id="timezone" name="timezone" class="help-inline form-control fontsize11" >	
						<c:forEach items="${timezone}" var="timezonevar"> 
							<c:choose>
								<c:when test="${empty districtMaster.timezone && timezonevar.timeZoneId eq 4}">
									<option value="${timezonevar.timeZoneId}" selected="selected">${timezonevar.timeZoneShortName}&nbsp;(${timezonevar.timeZoneName})</option>
								</c:when>
								<c:when test="${not empty districtMaster.timezone && districtMaster.timezone eq timezonevar.timeZoneId}">
									<option value="${timezonevar.timeZoneId}" selected="selected">${timezonevar.timeZoneShortName}&nbsp;(${timezonevar.timeZoneName})</option>
								</c:when>
								<c:otherwise>
									<option value="${timezonevar.timeZoneId}">${timezonevar.timeZoneShortName}&nbsp;(${timezonevar.timeZoneName})</option>
								</c:otherwise>
							</c:choose>									
						</c:forEach>					
					</select>
				</div>
			</div>
            <!-- timezone field in edit district account information page end -->  
                  
                  <div class="row">
                    <div class="col-sm-6 col-md-6 mt10">
                      <p><spring:message code="msgSchoolUnderContract" /></p>
                    </div>
                  </div>
                  
                  <div class="row">
                    <div class="col-sm-3 col-md-3">
                    <c:if test="${districtMaster.noSchoolUnderContract eq 2 && districtMaster.allSchoolsUnderContract eq 2 && districtMaster.allGradeUnderContract eq 2 && districtMaster.selectedSchoolsUnderContract eq 2}">	
	        			<c:set var="checked" value=""></c:set>
	         			<c:set var="checked" value="checked"></c:set>        			
	        		</c:if>
                      <label class="radio" style="margin-top: 0px;">
						<form:radiobutton path="noSchoolUnderContract" disabled="${disable}" value="1" onclick="uncheckedOtherRadio(1)" checked="${checked}" />
                        <spring:message code="msgNoSchoolAttached" /></label>
                    </div>
                  </div>  
                                  
                  <div class="row">
                    <div class="col-sm-3 col-md-3">
                      <label class="radio" style="margin-top: 0px;">
						<form:radiobutton path="allSchoolsUnderContract" disabled="${disable}" value="1" onclick="uncheckedOtherRadio(2)"  />
                        <spring:message code="msgAllSchoolinDistrict" /> </label>
                    </div>
                  </div>
                  
                    <div class="row">
                    <div class="col-sm-3 col-md-3 left20">
                        <div>
                        <label class="radio inline" style="padding-left: 0px;">
						<form:radiobutton path="allGradeUnderContract" disabled="${disable}" id="allGradeUnderContract" value="1" onclick="uncheckedOtherRadio(3)"  />
                        <spring:message code="lblGrd" /> </label>
                       </div>
                      		<c:set var="showHide" value="hide"></c:set>					
			        		<c:if test="${districtMaster.allGradeUnderContract eq 1}">	        			
			         			<c:set var="showHide" value=""></c:set>	         			
			        		</c:if>	
			        </div>		     
			        </div>		
			         <div class="row col-sm-12 col-md-12 left5" id="gradesDiv" class="${showHide}" >                     
                      	<div class="divwidth">
							 <label class="checkbox inline" style="padding-left: 0px;">
		                        <form:checkbox path="pkOffered" id="pkOffered" disabled="${disable}" value="1"/>
		                     <spring:message code="lblPK"/>
		                      </label>
						</div>
						<div class="divwidth">
		                       <label class="checkbox inline" style="padding-left: 0px;">
								<form:checkbox path="kgOffered" id="kgOffered" disabled="${disable}" value="1"/>
		                        <spring:message code="lblKG"/>
		                      </label>
	                    </div>
						<div class="divwidth">  
		                       <label class="checkbox inline" style="padding-left: 0px;">
								<form:checkbox path="g01Offered" id="g01Offered" disabled="${disable}" value="1"/>
		                        1 
		                      </label>
		                </div>
						<div class="divwidth">      
		                       <label class="checkbox inline" style="padding-left: 0px;">
								<form:checkbox path="g02Offered" id="g02Offered" disabled="${disable}" value="1"/>
		                        2 
		                      </label>
		                </div>
						<div class="divwidth">						      
		                       <label class="checkbox inline" style="padding-left: 0px;">
								<form:checkbox path="g03Offered"   disabled="${disable}" id="g03Offered" value="1"/>
		                        3 
		                      </label>
		                </div>
						<div class="divwidth">      
		                       <label class="checkbox inline" style="padding-left: 0px;">
								<form:checkbox path="g04Offered"   disabled="${disable}" id="g04Offered" value="1"/>
		                        4 
		                      </label>
		                </div>
						<div class="divwidth">     
		                       <label class="checkbox inline" style="padding-left: 0px;">
								<form:checkbox path="g05Offered"   disabled="${disable}" id="g05Offered" value="1"/>
		                        5 
		                      </label>
		                </div>
						<div class="divwidth">      
		                       <label class="checkbox inline" style="padding-left: 0px;">
								<form:checkbox path="g06Offered"  disabled="${disable}"  id="g06Offered" value="1"/>
		                        6 
		                      </label>
		                </div>
						<div class="divwidth">      
		                       <label class="checkbox inline" style="padding-left: 0px;">
								<form:checkbox path="g07Offered"   disabled="${disable}" id="g07Offered" value="1"/>
		                        7 
		                      </label>
		                </div>
						<div class="divwidth">      
		                       <label class="checkbox inline" style="padding-left: 0px;">
								<form:checkbox path="g08Offered"  disabled="${disable}"  id="g08Offered" value="1"/>
		                        8 
		                      </label>
		                </div>
						<div class="divwidth">      
		                       <label class="checkbox inline" style="padding-left: 0px;">
								<form:checkbox path="g09Offered"  disabled="${disable}"  id="g09Offered" value="1"/>
		                        9 
		                      </label>
		                </div>
						<div class="divwidth">      
		                       <label class="checkbox inline" style="padding-left: 0px;">
								<form:checkbox path="g10Offered"  disabled="${disable}"  id="g10Offered" value="1"/>
		                        10 
		                      </label>
		               </div>
						<div class="divwidth">       
		                       <label class="checkbox inline" style="padding-left: 0px;">
								<form:checkbox path="g11Offered"  disabled="${disable}"  id="g11Offered" value="1"/>
		                        11 
		                      </label>
		               </div>
					   <div class="divwidth">       
	                       <label class="checkbox inline" style="padding-left: 0px;">
							<form:checkbox path="g12Offered"  disabled="${disable}"  id="g12Offered" value="1"/>
	                        12 
	                      </label>
                       </div>                      
                    </div>
                    
                  
                  <div class="row">
                     <div class="col-sm-3 col-md-3 mt10 left20">                      
	                        <div>
	                          <label class="radio inline" style="padding-left: 0px;">
								<form:radiobutton path="selectedSchoolsUnderContract"  disabled="${disable}" value="1" onclick="uncheckedOtherRadio(4)"  />
	                            <spring:message code="lblSltSch" /> </label>
	                        </div>
	                 </div>    
                            <c:set var="showHideAddSchoolLink" value="hide"></c:set>					
			        		<c:if test="${districtMaster.selectedSchoolsUnderContract eq 1}">	        			
			         			<c:set var="showHideAddSchoolLink" value=""></c:set>	         			
			        		</c:if>	
			        		<div class="col-sm-6 col-md-6 left18 mt10">
				        		<div class="pull-right ${showHideAddSchoolLink}" id="addSchoolLink">
			                         <c:if test="${fn:indexOf(roleAccessAI,'|1|')!=-1}">
			                        <a href="javascript:void(0);" onclick="addSchool();"><spring:message code="lnkAddSchl" /></a>
			                        </c:if>
	                             </div> 
			        		</div>
                        
                  </div>
                  <input type="hidden" id="gridNo" name="gridNo" value="">
                 <%-- ======== District School Grid ========= --%>
                 <div class="row col-sm-9 col-md-9">
                 	 <div  id="districtSchoolsGrid" class="${showHideAddSchoolLink}" style="padding-bottom: 10px;" onclick="getSortSchoolGrid()">
                     </div>
                 </div>                   
                  <div id="addSchoolDiv" class="hide">
	                  <div class="row col-sm-10 col-md-10" style="max-width:755px;">
	                    <div>
	                      <label><spring:message code="lblSchoolName" /></label>
							<input type="text" id="districtORSchoolName" name="districtORSchoolName" placeholder="" maxlength="100" class="form-control"
			             		onfocus="getDistrictMasterAutoComp(this, event, 'divTxtShowData', 'districtORSchoolName','districtOrSchoolId','');"
								onkeyup="getDistrictMasterAutoComp(this, event, 'divTxtShowData', 'districtORSchoolName','districtOrSchoolId','');"
								onblur="hideDistrictMasterDiv(this,'districtOrSchoolId','divTxtShowData');"	/>
								<input type="hidden" id="districtOrSchooHiddenlId" />
								<div id='divTxtShowData'
								onmouseover="mouseOverChk('divTxtShowData','districtORSchoolName')" 
								style=' display:none;position:absolute;z-index: 5000' class='result' ></div>
	                    </div>
	                  </div>
	                  <div class="row col-sm-5 col-md-5 idone">
                 		 <div class="span2 idone"><a href="javascript:void(0);" onclick="return saveDistrictSchools();"><spring:message code="lnkImD" /></a>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onclick="clearSchool()"><spring:message code="lnkCancel" /></a></div>
                	  </div>
                  </div>
                  
                  <%-- Add Notes Field ! It will display only for Tm --%>
                  <c:if test="${userSession.entityType eq 1}">	 
	                  <div class="row">
	                    <div class="col-sm-10 col-md-10">
	                        <spring:message code="headNot" />
	                   
	                    
	                    <div class="pull-right" style="margin-right: 40px">
	                     <c:if test="${fn:indexOf(roleAccessAI,'|1|')!=-1}">
	                    <a href="javascript:void(0);" onclick="addNotes();"><spring:message code="lnkAddNote" /></a>
	                     </c:if>
	                    </div>
	                     </div>
	                  </div>
	                  
	                   <div class="row">
	                    <div class="col-sm-10 col-md-10">
	                    <div id="notesGrid" onclick="getSortNotesGrid()">
		                  <table id="notesTable" width="100%" border="0" class="table table-bordered mt10">
		                  </table>
	                    </div>
	                   </div>
	                  </div>
	                  
	                  <div id="addNotesDiv"  class="hide">
		                  <div class="row">
		                    <div class="col-sm-9 col-md-9" id="dNote">
		                      <label><spring:message code="headNot" /></label>
		                      <label class="redtextmsg"> <spring:message code="msgHowToCopyPastCutDouc" /></label>
		                       <textarea class="span12" rows="2" id="note" name="note" maxlength="500" onkeyup="return chkLenOfTextArea(this,500);" onblur="return chkLenOfTextArea(this,500);"></textarea>
		                    </div>
		                  </div>
		                  <div class="row col-sm-4 col-md-4 idone">
	                 		 <div class="span2 idone"><a href="javascript:void(0);" onclick="return saveNotes();"><spring:message code="lnkImD"/></a>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onclick="clearNotes();"><spring:message code="lnkCancel" /></a></div>
	                	  </div>
	                  </div>
        		 </c:if>	<%-- Add Notes check end here  --%>	
        		 <div class="clearfix"></div>
        		 <div class="row col-sm-12 col-md-12 left5 top10">
	                  <label class="checkbox inline" style="padding-left: 0px;">
	                  <input type="hidden" name="aJobRelation" value="${aJobRelation}"/>
	                   <input type="checkbox" ${aJobRelation}  id="cbxUploadAssessment" name="cbxUploadAssessment" onclick="uploadAssessment(1)" ${(districtMaster.assessmentUploadURL != null ) ? 'checked' : ''}>
	                   <spring:message code="lblAttachDefJoSpeciInventory" /> <!-- <a href="#" id="iconpophover8" rel="tooltip" data-original-title="Lorem ipsum dolor sit&#013;&#10;amet, consectetur&#013;&#10;adipiscing elit. Morbi&#013;&#10;non quam nunc"><img src="images/qua-icon.png" width="15" height="15" alt=""></a> -->
	                  </label>
                  </div>
                  
                  <div  id="uploadAssessmentDiv" class="row hide top10">				 
						 <c:if test="${fn:indexOf(roleAccessAI,'|9|')!=-1}">
						 <div class="col-sm-12 col-md-12">
						 <spring:message code="msgUnloadJoSpeciInventory" />
						</div>
						<div class="col-sm-4 col-md-4">
							<form:input path="assessmentUploadURLFile" type="file"/>
						</div>
												
						</c:if> 
						<div class="col-sm-4 col-md-4">
						<a data-original-title='District' rel='tooltip' id='JSIDistrict' href='javascript:void(0)'  onclick="showFile(2,'district','${districtMaster.districtId}','${districtMaster.assessmentUploadURL}','JSIDistrict');${windowFunc}"><c:out value="${(districtMaster.assessmentUploadURL==''||districtMaster.assessmentUploadURL==null)?'':'view'}"/></a>
						</div>					
                  </div>
                  
                  <input type="hidden" id="assessmentUploadURLVal" name="assessmentUploadURLVal" value="${(districtMaster.assessmentUploadURL != null ) ? '1' : '0'}" />
                  
                <div class="row top15">
                    <div class="col-sm-12 col-md-12">
                      <p><spring:message code="msgHiringDecesion" /></p>
                    </div>
                 </div>
                 
				<div class="row" >
                    <div class="col-sm-12 col-md-12">
	        			<c:set var="checked" value=""></c:set>
	                    <c:if test="${districtMaster.hiringAuthority eq ''}">	
		         			<c:set var="checked" value="checked"></c:set>        			
		        		</c:if>
	        		
                      <label class="radio" style="margin-top: 0px;">
						<form:radiobutton path="hiringAuthority" value="D" />
                        <spring:message code="msgDistrictAdministrator" />
                      </label>
                    </div>
                    
                    <div class="col-sm-12 col-md-12">
                      <label class="radio"  style="margin-top: 0px;">
						<form:radiobutton path="hiringAuthority"  value="S" checked="${checked}" />
                        <spring:message code="msgSchoolAdministrator" />
                      </label>
                    </div>
               </div>
                  
                   <div class="row  hide">
	                    <div class="col-sm-12 col-md-12">
	                      <p><spring:message code="msgSchCanCreatOwn" /></p>
	                    </div>
                   </div>
                   
				<div class="row hide">
                    <div class="col-sm-12 col-md-12">
                      <label class="radio">
						<form:radiobutton path="districtApproval" value="1"  checked="checked" />
                        <spring:message code="lblYes" /></label>
                    </div>
                    <div class="col-sm-12 col-md-12">
                      <label class="radio">
						<form:radiobutton path="districtApproval"  value="0" />
                        <spring:message code="lblNo" /> </label>
                    </div>
                </div>
                  
                  
                   <div class="row mt10">
	                  <div class="col-sm-12 col-md-12">
	                    <spring:message code="lblJoBrdURL" />
	                    <div class="">
							<a target="blank" href="${JobBoardURL}">${JobBoardURL}</a>
	                    </div>
	                  </div>
                  </div>
                  
                  <!-- shadab   -->
                  <div class="row mt10">
	                  <div class="col-sm-12 col-md-12">
	                    <div class="">
							<form:checkbox path="includeState" id="includeState" value="1"/> &nbsp;&nbsp;<spring:message code="lblIncludeState"></spring:message><br/>
							<form:checkbox path="includeZipCode" id="includeZipCode" value="1"/>&nbsp;&nbsp; <spring:message code="lblIncludeZipCode"></spring:message><br/>
							<form:checkbox path="includeZone" id="includeZone" value="1"/>&nbsp;&nbsp; <spring:message code="lblIncludeZone"></spring:message>
	                    </div>
	                  </div>
                  </div>
                  <!-- end -->
                  
                  <div class="row mt10">
	                  <div class="col-sm-12 col-md-12">
	                    <spring:message code="msgInternalTransferURL" />
	                    <div class="">
							<a target="blank" href="${InternalTransferURL}">${InternalTransferURL}</a>
	                    </div>
	                  </div>
                  </div>
                  
                   <c:if test="${userSession.entityType ne 1}">	
        			<c:set var="disableForTm" value="true"></c:set>
         		   </c:if>
                  
                    
                   <input type="hidden" id="distId" name="distId" value="${districtMaster.districtId}">  
                   <c:if test="${userSession.entityType eq 1}">	                 
					   <div class="row top10 left5">
			                        <div class="col-sm-4 col-md-4">
										<label class="checkbox inline" style="padding-left: 0px;">
				                        	<form:checkbox path="isPortfolioNeeded" id="isPortfolioNeeded"  value="1"/>
											<spring:message code="lblIsPortNed" /> <a href="#" id="iconpophover15" rel="tooltip" data-original-title="<spring:message code="msgSelectThisCheckBox" />"><img src="images/qua-icon.png" width="15" height="15" alt=""></a>
										</label>
								  	</div>
			                        <div class="col-sm-4 col-md-4">
				                      <a href="javascript:void(0);" onclick="setEmailFormat()"> <spring:message code="msgSetEmailFormat" /></a>
			                        </div>
                 			
	                   </div>
	                   
	                   <div class="row mt10 left5">
		                  <div class="col-sm-12 col-md-12">
		                        <label class="checkbox inline" style="padding-left: 0px;">
		                        <form:checkbox path="isWeeklyCgReport" id="isWeeklyCgReport"  value="1"/>
								<spring:message code="lblWekCGRept" /> <a href="#" id="iconpophover16" rel="tooltip" data-original-title="<spring:message code="msgEditDistrict3" />"><img src="images/qua-icon.png" width="15" height="15" alt=""></a>
								</label>
						   </div>
	                   </div>
                   </c:if>
                   
                   <div class="row mt10 left5">
	                  <div class="col-sm-6 col-md-6">
	                        <label class="checkbox inline" style="padding-left: 0px;">
	                        <form:checkbox path="allowMessageTeacher" id="allowMessageTeacher"  onclick="emailForTeacherDivCheck();"  value="1"/>
							<spring:message code="lblAllMsgfrTch" />
							</label>
					   </div>
                   </div>  

                    <c:if test="${userSession.entityType eq 1}">
                         <div id=""  class="row mt10">			
	                  	  	<div class="col-sm-6 col-md-6">
		                      <label ><spring:message code="msgExclusivePeriod" /></label>
		                    </div>
		                   </div> 
		                   
		                    <div class="row">  
		                    <div class="col-sm-6 col-md-6">
								<form:input path="exclusivePeriod" cssClass="form-control" maxlength="3"/>
							 </div>
							  </div>						
                   </c:if>
                   
				  
        		 <div class="clearfix"></div>                  
                   
                   <div id="emailForTeacherDiv"  class="hide row mt10">
                  	 <div class="col-sm-6 col-md-6">
                      <label ><spring:message code="msgRecMsgFrTch" /></label>
						<form:input path="emailForTeacher" cssClass="form-control" maxlength="100"/>
					 </div>
	                </div>
	                
	                
	                <c:if test="${headQuarterId eq 2}">
	                    <div class="row mt10 left5">
		                  <div class="col-sm-6 col-md-6">
		                        <label class="checkbox inline" style="padding-left: 0px;">
		                        <form:checkbox path="hrIntegrated" id="hrIntegratedId"   value="1"/>
								<spring:message code="lblHRIntegrated" />
								</label>
						   </div>
	                   </div>                   
                    </c:if> 
                    
                    <c:if test="${districtMaster.flagForURL ne 1 && districtMaster.flagForMessage ne 1}">	
        			<c:set var="appchecked" value=""></c:set>
         			<c:set var="appchecked" value="checked"></c:set>        			
        		    </c:if>
        		          
        		    <c:if test="${userSession.entityType eq 1}">	             
                    <div class="row mt10 left5">
	                  <div class="col-sm-6 col-md-6">
	                     <label class="checkbox inline" style="padding-left: 0px;">
	                        <form:checkbox path="isApplitrackIntegration" id="isApplitrackIntegration"  onclick="applitrackIntegratoinCheck();"  value="1"/>
							<spring:message code="appliTrackIntegration" />
						</label>
					   </div>
                   </div>     
					
				<!-- TPL-4823--- Add configuration option HireVue Integration for District start-->		
				 <div id="errorhirevueAPIkeydiv" class="required" style="display:none;"><spring:message code="errmsghirevueAPIkey" /></div>
                 <div class="row">
					<div class="col-sm-12 col-md-12">
					<label class="checkbox" style="margin-top: 0px;">
						<form:checkbox path="isHireVueIntegration" id="isHireVueIntegration" value="" onclick="create_hirevue_key(this);" />
			    		<spring:message code="lblhireVueIntegration" />
					</label>
                    </div>
				 </div>
				 <div class="row" id="hireVueAPIKeydiv" style="display:none;" >
					<div class="col-sm-6 col-md-6" >
						<form:input path="hireVueAPIKey" id="hireVueAPIKey"  maxlength="100" cssClass="form-control"/>
                    </div>
				 </div>
				<!-- TPL-4823--- Add configuration option HireVue Integration for District end-->	

                   <div class="clearfix"></div>                  
                   
                   <div id="appliTrackIntegrationDiv"  class="hide row mt10">
                   <div class="col-sm-12 col-md-12">			                         
						 	<div class='required' id='errorapplitrackdiv'></div>
						</div>
                  	 <div class="col-sm-3 col-md-3">
                      <label>Client code</label><span class="required">*</span>
						<input type="text" name="clientCode" id="clientCode" class="form-control" maxlength="20" value="${applitrackDistricts.clientcode}" />
					 </div>
					 <div class="col-sm-3 col-md-3">
                      <label>API Username</label><span class="required">*</span>
						<input type="text" name="apiUserName" id="apiUserName" class="form-control" maxlength="20" value="${applitrackDistricts.userName}" />
					 </div>
					 <div class="col-sm-3 col-md-3">
                      <label>API Password</label><span class="required">*</span>
						<input type="text" name="apiPassword" id="apiPassword" class="form-control" maxlength="20" value="${applitrackDistricts.password}" />
					 </div>
	                </div>
	               </c:if>
	                
                  <c:if test="${districtMaster.flagForURL ne 1 && districtMaster.flagForMessage ne 1}">	
        			<c:set var="checked" value=""></c:set>
         			<c:set var="checked" value="checked"></c:set>        			
        		  </c:if>
        		  <c:if test="${(districtMaster.flagForMessage ne 1 && districtMaster.flagForURL ne 1 && userSession.entityType eq 2)||(districtMaster.flagForURL eq 1 && userSession.entityType eq 2)|| userSession.entityType eq 1}">
                   <div class="row">
                    <div class="col-sm-12 col-md-12">
                      <label class="radio">

                        <form:radiobutton path="flagForURL" value="1" onclick="uncheckedMessageRadio()" checked="${checked}" />
                        <spring:message code="msgCandHasFinishedInvent" /> <a href="#" id="iconpophover17" rel="tooltip" data-original-title="<spring:message code="editDistrictBord" />"><img src="images/qua-icon.png" width="15" height="15" alt=""></a> </label>
                    </div>
                  </div>
                  
                  <div class="row">
                  <div class="col-sm-10 col-md-10" style="max-width: 750px;">
                  <form:input path="exitURL" class="form-control" maxlength="250"/>
                  </div>
                  </div>                  
                  </c:if>
                  
                  <c:if test="${(districtMaster.flagForMessage eq 1 && userSession.entityType eq 2)|| userSession.entityType eq 1}">
                  <div class="row mt10">
                    <div class="col-sm-12 col-md-12">
                      <label class="radio">
                        <form:radiobutton path="flagForMessage"  value="1" onclick="uncheckedUrlRadio()"  />
                        <spring:message code="msgCandWillFinishTmInventAftrComplDistApp" /> 
                        <a href="#" id="iconpophover19" rel="tooltip" data-original-title="<spring:message code="iconMessage" />"><img src="images/qua-icon.png" width="15" height="15" alt=""></a> 
                        </label>
                    </div>
                  </div>

                  <div class="row">
                  <div class="col-sm-12 col-md-12">
                    <p><spring:message code="lblComMsg" /> </p>
                    <div style="width:710px;"   id="eMessage">
                    <label class="redtextmsg"><spring:message code="msgHowToCopyPastCutDouc" /></label>
					<form:textarea path="exitMessage" class="form-control" rows="3" maxlength="1000" onkeyup="return chkLenOfTextArea(this,1000);" onblur="return chkLenOfTextArea(this,1000);"/>
                    </div>
                  </div>
                  </div>
                  </c:if>
                  <div class="clearfix"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
       <!-- End Account Section -->
       
       <div class="modal hide"  id="myModalEmail" >
			<div class="modal-dialog-for-cgmessage">
		    <div class="modal-content">
			<div class="modal-header">
		  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
				<h3 id="myModalLabel">Teachermatch</h3>
			</div>
		
			<div  class="modal-body">
				<div class="control-group">
					<div class='divErrorMsg' id='errordivEmail'></div>
				</div>
				<div id='support'>
					<div class="control-group">
						<div class="">
					    	<label><strong><spring:message code="lblFrm" /></strong><span class="required">*</span></label>
				        	<input type="text" id="fromAddress" name="fromAddress"   class="form-control" maxlength="250" />
						</div>
					</div>
					
					<div class="control-group">
						<div class="">
					    	<label><strong><spring:message code="lblSub" /></strong><span class="required">*</span></label>
				        	<input  type="text"  id="subjectLine" name="subjectLine"class="form-control" maxlength="250" />
						</div>
					</div>
				            
			        <div class="control-group">
						<div class="" id="mailBody">
					    	<label><strong><spring:message code="lblMsg" /></strong><span class="required">*</span></label>
					    	<label class="redtextmsg"><spring:message code="msgHowToCopyPastCutDouc" /></label>
				        	<textarea rows="5" class="form-control" cols="" id="" name=""></textarea>
						</div>
					</div>
		 		</div>
		 	</div>
		 	
		 	<div class="modal-footer">
		 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="lnkCancel" /></button>
		 		<button class="btn btn-primary"  onclick='return saveEmailFormat();' ><spring:message code="btnSave" /></button>
		 	</div>
	</div>
	 	</div>
	</div>
       
       <div class="mt30">
        <c:if test="${fn:indexOf(roleAccess,'|8|')!=-1}">
        <button type="submit" class="btn btn-large btn-primary"><strong> <spring:message code="msgSaveDistrict" /> <i class="icon"></i></strong></button>
        </c:if>
          &nbsp;&nbsp;<a href="javascript:void(0);" onclick="cancelDistrict();"><spring:message code="lnkCancel" /></a><br>
          <br>
        </div>
      </div>
    </div>
  </div>
  
  <iframe src="" id="iframeJSI" width="100%" height="480px" style="display: none;">
  </iframe> 
   
  <div  class="modal hide"  id="myModalMsg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"   style="z-index: 5000;"  data-backdrop="static">
		<div class="modal-dialog">
		<div class="modal-content">
		<div class="modal-header">
	  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick="return aKeyContactDiv(0);">x</button>
			<h3 id="myModalLabel"><spring:message code="headKyCont" /></h3>
		</div>
		<div class="modal-body">		
			<div class="control-group">
				<div class="">
			    	<span id="Msg"></span>	        	
				</div>
			</div>
	 	</div>
	 	<div class="modal-footer">
	 		<button class="btn" data-dismiss="modal" aria-hidden="true"  onclick="return aKeyContactDiv(1);"><spring:message code="btnOk" /></button>
	 		<button class="btn" data-dismiss="modal" aria-hidden="true"  onclick="return aKeyContactDiv(0);"><spring:message code="btnClose" /></button> 		
 		</div>
	</div>
	</div> 
	</div>

	<div style="display:none;" id="loadingDiv">
		<table  align="center" >
			 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
		</table>
	</div>
</form:form>

<script> 
	loadStatusMaster();
	document.getElementById("alternatePhone").focus();
	displayKeyContact();
	displayNotes();
	
	displayDistrictAdministrator(2);
	displayDistrictAnalyst(5);
	displayDistrictSchools();
	uploadAssessment(${(districtMaster.assessmentUploadURL != null ) ? '1' : '0'});
	showFile(1,'district','${districtMaster.districtId}','${districtMaster.logoPath}',null);
	
</script>


<script type="text/javascript">

var selectIds = $('#collapseOne,#collapseTwo,#collapseThree,#collapseFour,#collapseFive,#collapseSix,#collapseSeven,#collapseEight,#collapseNine,#collapseTen,#collapseEle,#collapseFifteen,#collapseSixteen');
$(function ($) {
    selectIds.on('show.bs.collapse hidden.bs.collapse', function () {
        $(this).prev().find('.accordion-toggle').toggleClass('plus minus');
    })
});


$('.accordion').collapse();
$('.accordion').on('show', function (e) {
$(e.target).prev('.accordion-heading').find('.accordion-toggle').addClass('minus').removeClass('plus');
});

$('.accordion').on('hide', function (e) {
$(this).find('.accordion-toggle').not($(e.target)).removeClass('minus').addClass('plus');
});


function emailForTeacherDivCheck(){
	var allowMessageTeacher=document.getElementById("allowMessageTeacher").checked;
	if(allowMessageTeacher==1){
		$("#emailForTeacherDiv").fadeIn();
		$("#emailForTeacher").focus();
	}else{
		$("#emailForTeacherDiv").hide();
		$("#allowMessageTeacher").focus();
		document.getElementById("emailForTeacher").value="";
	}
}
emailForTeacherDivCheck();

function applitrackIntegratoinCheck(){
	var isApplitrackIntegration=document.getElementById("isApplitrackIntegration").checked;
	if(isApplitrackIntegration==1){
		$("#appliTrackIntegrationDiv").fadeIn();
		//$("#emailForTeacher").focus();
	}else{
		$("#appliTrackIntegrationDiv").hide();
		//$("#allowMessageTeacher").focus();
		//document.getElementById("emailForTeacher").value="";
	}
}
applitrackIntegratoinCheck();

//var isApplitrackIntegration=document.getElementById("isApplitrackIntegration").checked;
	//if(isApplitrackIntegration!=1){
	//document.getElementById("isApplitrackIntegration").disabled = true;
//	}
if(${applitrackDistricts.appDistrictId}==0)
document.getElementById("isApplitrackIntegration").disabled = true;
</script>

<script type="text/javascript">
$('#myModal').modal('hide');
$('#iconpophover1').tooltip();
$('#iconpophover2').tooltip();
$('#iconpophover3').tooltip();
$('#iconpophover4').tooltip();
$('#iconpophover5').tooltip();
$('#iconpophover6').tooltip();
$('#iconpophover7').tooltip();
//$('#iconpophover8').tooltip();
$('#iconpophover9').tooltip();
$('#iconpophover19').tooltip();
$('#iconpophover10').tooltip();
$('#iconpophover11').tooltip();
$('#iconpophover12').tooltip();
$('#iconpophover13').tooltip();
$('#iconpophover14').tooltip();
$('#iconpophover15').tooltip();
$('#iconpophover16').tooltip();
$('#iconpophovercg').tooltip();
$('#iconpophover17').tooltip();
$('#iconpophover22').tooltip();
$('#iconpophover23').tooltip();
$('#iconpophover25').tooltip();

$(document).ready(function(){
  		$('#eMessage').find(".jqte").width(724);
  		$('#dNote').find(".jqte").width(724);
  		//$('#dDescription').find(".jqte").width(535);
}) 

</script>
<script><!--
	function disableSlider()
	{
		if($('#mosaicRadios1').is(':checked')) 
		{ 
	   		$("#mosaicRadiosFlag").val("1");
	   		$("#candidateFeedDaysOfNoActivity").val("0");
	   			try
	    		{
	    			document.getElementById("ifrm2").src="slideract.do?name=slider2&tickInterval=10&max=${sliderMaxLength }&swidth=200&svalue=${candidateFeedDaysOfNoActivityValue}&dslider=1";
    			}
	    		catch(e)
	    		{} 
	    }
	    else
	    {
	    	if($('#mosaicRadios2').is(':checked')) 
			{ 
	    	
	    		$("#mosaicRadiosFlag").val("0");
	    		$("#candidateFeedDaysOfNoActivity").val("0");
	    		try
	    		{
	    			document.getElementById("ifrm2").src="slideract.do?name=slider2&tickInterval=10&max=${sliderMaxLength }&swidth=200&svalue=0&dslider=0";
    			}
	    		catch(e)
	    		{} 

	    //		alert(" mosaicRadios2 is checked ");
		    }
	    }
	}
getDistrictDomains();
chkDStatus(${setAssociatedStatusToSetDPoints});
showOfferVVI();
showMarksDiv();
showLinkDiv();
showReminderDiv();
showOfferAMT();
chkTimeAndDayForVVI();
showPortfolioReminderDiv();

</script>


<script type="text/javascript">//<![CDATA[
    var cal = Calendar.setup({
        onSelect: function(cal) { cal.hide() },
        showTime: true
    });
  	 cal.manageFields("initiatedOnDate", "initiatedOnDate", "%m-%d-%Y");
     cal.manageFields("contractStartDate", "contractStartDate", "%m-%d-%Y");
     cal.manageFields("contractEndDate", "contractEndDate", "%m-%d-%Y");
    //]]></script>
    
    
    <script type="text/javascript">
	$('#addPop').click(function() {
		if ($('#lstDistrictAdmins option:selected').val() != null) {
			 $('#lstDistrictAdmins option:selected').remove().appendTo('#attachedDAList');
			 $("#attachedDAList").attr('selectedIndex','-1').find("option:selected").removeAttr("selected");
	         $("#attachedDAList").attr('selectedIndex', '-1').find("option:selected").removeAttr("selected");
	 }
	});
	
	$('#removePop').click(function() {
	       if ($('#attachedDAList option:selected').val() != null) {
	             $('#attachedDAList option:selected').remove().appendTo('#lstDistrictAdmins');
	           //  sortSelectList();
	             $("#attachedDAList").attr('selectedIndex',  '-1').find("option:selected").removeAttr("selected");
	             $("#lstDistrictAdmins").attr('selectedIndex', '-1').find("option:selected").removeAttr("selected");
	             $("#lstDistrictAdmins").attr('selectedIndex', '-1').addAttr("selected");
	             
	}
	});
	displayAllDistrictAssessment();
	$("#DistrictAdminListDiv").hide();
</script>
<!-- @Author: Gagan 
 * @Discription: view of edit district Page.
 -->
 
 <script>
 var sendNotificationOnNegativeQQ322 = '${sendNotificationOnNegativeQQValue}';
$(document).ready(function(){
	
	if(sendNotificationOnNegativeQQ322!=null)
		if(sendNotificationOnNegativeQQ322!="")
			$("#sendNotificationOnNegativeQQId").attr("checked","checked");
	
	buildApprovalGroup322();
	displayAllActiveDistrictUser();
});
function buildApprovalGroup322()
{
	var isCheckedBuildApprovalGroups = $("#buildApprovalGroups").attr("checked");
 	var isCheckedApprovalBeforeGoLive = $("#approvalBeforeGoLive1").attr("checked");
	
	groupRadioButton();
	$(".hideGroup").hide();
	if(isCheckedApprovalBeforeGoLive!=null)
	{
		if(isCheckedApprovalBeforeGoLive=="checked")
		{
			$("#buildApprovalGroupsDivId").show();
			$("#buildApprovalGroups").show();
			$("#addGroupHyperLink").show();
			$("#groupMembersList").hide();
			$("#addApprovalGroupAndMemberDiv").hide();
			
			buildApprovalGroup();
			$("#buildApprovalGroupsDivId").show();
			$("#buildApprovalGroups").show();
			$("#addGroupHyperLink").show();
			$("#addApprovalGroupAndMemberDiv").hide();
		}
	}
	else
	{
		$("#buildApprovalGroupsDivId").hide();
		$("#buildApprovalGroups").hide();
		$("#addGroupHyperLink").hide();
		$("#groupMembersList").hide();
		$("#addApprovalGroupAndMemberDiv").hide();
	}
}

 
function fileContainsVirusDiv(msg)
{
	$('#loadingDiv').hide();
	$('#virusDivId .modal-body').html(msg);
	$('#virusDivId').modal('show');
}
</script>

<div class="modal hide" id="virusDivId" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog-for-cgcoverletter" style='width: 35%;'>
		<div class="modal-content">
			<div class="modal-header">
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body" style="max-height: 400px;overflow-y:auto;">
				<div class="control-group">
				</div>
			</div>
			<div class="modal-footer">
				<span id=""><button class="btn btn-primary" data-dismiss="modal" onclick="showDSPQDiv();"><spring:message code="btnOk" /> <i class="icon"></i></button></span>&nbsp;&nbsp;
			</div>
		</div>
	</div>
</div>

<div class="modal hide" id="removeMemberConfirmation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false" removeMemberGroupId="" removeMemberId="">
	<div class="modal-dialog-for-cgcoverletter" style='width: 35%;'>
		<div class="modal-content">
			<div class="modal-header">
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body" style="max-height: 400px;overflow-y:auto;">
				<div class="control-group">
					<spring:message code="msgRemoveThisMembr" />
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-primary" data-dismiss="modal" onclick="removeMember()"><spring:message code="btnOk" /> <i class="icon"></i></button>&nbsp;
				<button class="btn" data-dismiss="modal"><spring:message code="lnkCancel" /></button> &nbsp;&nbsp;
			</div>
		</div>
	</div>
</div>

<div class="modal hide" id="removeGroupConfirmation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false" removeGroupId="">
	<div class="modal-dialog-for-cgcoverletter" style='width: 35%;'>
		<div class="modal-content">
			<div class="modal-header">
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body" style="max-height: 400px;overflow-y:auto;">
				<div class="control-group">
					<spring:message code="msgRemoveThisGrp" />
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-primary" data-dismiss="modal" onclick="removeGroup()">  <spring:message code="btnOk" />  <i class="icon"></i></button>&nbsp;
				<button class="btn" data-dismiss="modal"><spring:message code="lnkCancel" /></button> &nbsp;&nbsp;
			</div>
		</div>
	</div>
</div>



<c:if test="${sendReferenceOnJobCompleteNC==1}">
	<script>
	document.getElementById("SendReferenceFinalizeStatusId").style.display="none";
	</script>
</c:if>


<c:if test="${headQuarterId==2}">
    <c:if test="${empty statusListBoxValueForReminder}">
		<script>
		document.getElementById("statusListBoxForReminder").disabled=true;
		</script>
	</c:if>
	
	<c:if test="${not empty statusListBoxValueForReminder}">
		<script>
		document.getElementById("sendEReferenceId").checked=true;
		</script>
	</c:if>
</c:if>


<script>


	function SendReferenceFinalizeStatusHide(element){
		if(element.checked){
			document.getElementById("SendReferenceFinalizeStatusId").style.display="none";
			document.getElementById("statusListBoxForReminder").value="";
			document.getElementById("sendEReferenceId").checked=false;
			document.getElementById("statusListBoxForReminder").disabled=true;
			
		}else{
		    document.getElementById("SendReferenceFinalizeStatusId").style.display="block";
		}
	}
	
	function sendEReference(element){
	  if(element.checked){
	  document.getElementById("statusListBoxForReminder").disabled=false;
	  }else{
	  document.getElementById("statusListBoxForReminder").disabled=true;
	  }
	}
	
	$(function(){
		$('[name="displayCNFGCol"]').click(function(){
            	var sThisVal ="";
            	var i=0;
	            $('[name="displayCNFGCol"]').each(function () {
	            	if(this.checked)
	            		if(i==0)
	      		 		sThisVal += $(this).val();
	      		 		else
	      		 		sThisVal += "##"+$(this).val();
	      		 		i++;
	  			});
            	$('[name="displayCNFGColumn"]').val(sThisVal);
               // alert("Checkbox is checked========"+$('[name="displayCNFGCol"]').val()+"  ==   "+$('[name="displayCNFGColumn"]').val());
        });
		$('[name="CandidateNotReviewed"]').click(function(){
            	var sThisVal ="";
            	var i=0;
	            $('[name="CandidateNotReviewed"]').each(function () {
	            	if(this.checked)
	            		if(i==0)
	      		 		sThisVal += $(this).val();
	      		 		else
	      		 		sThisVal += "##"+$(this).val();
	      		 		i++;
	  			});
            	$('[name="candidatesNotReviewed"]').val(sThisVal);
                //alert("Checkbox is checked========"+$('[name="CandidateNotReviewed"]').val()+"  ==   "+$('[name="candidatesNotReviewed"]').val());
        });
	});
	$(document).ready(function(){
		var selectCNFGCol=$('[name="displayCNFGColumn"]').val().split("##");
			$.each(selectCNFGCol,function(i,val){
				$('input[type="checkbox"][name="displayCNFGCol"][value="' + val + '"]').prop('checked', true);
			});
			
		var selectCNRCols=$('[name="candidatesNotReviewed"]').val().split("##");
			$.each(selectCNRCols,function(i,val){
				$('input[type="checkbox"][name="CandidateNotReviewed"][value="' + val + '"]').prop('checked', true);
			});
	});
</script>
