<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<script type="text/javascript" src="dwr/interface/DashboardAjax.js?ver=${resouceMap['DashboardAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src="dwr/util.js"></script>

<script type="text/javascript" src="dwr/interface/DSPQServiceAjax.js?ver=${resourceMap['DSPQServiceAjax.ajax']}"></script>
<script type="text/javascript" src="js/jobapplicationflow/jafstart.js?ver=${resouceMap['js/jobapplicationflow/jafstart.js']}"></script>

<link href="css/quest.css?ver=${resourceMap['css/quest.css']}" rel="stylesheet" type="text/css"/>
<style type="text/css">

.greydiv
{
font-size:15px;
background-color: #474747;
text-align: left;
width:100%;
padding-left: 30px;
height: 22px;
color:white;
}

.maroondiv
{
font-size:15px;
background-color: maroon;
text-align: left;
width:100%;
padding-left: 30px;
height: 22px;
color:white;
}

.upperleftdiv
{
width:250px;
background-color:#f5f5f5;
float: left;
height: 130px;
box-shadow: 0 0 7px black;
padding:5px;
color:black;
}

.upperleftdiv .upperleftarrowdiv
{
	width: 20px;
    height: 20px;
    border: none;
    transform: rotate(45deg);
    position:absolute;
    top: 48px;
    left:245px;
    background-color: #f5f5f5;
    box-shadow:  0 9px 0 0px #f5f5f5,  -9px 0 0 0px #f5f5f5, 1px -1px 1px #818181;
    /*box-shadow:  0 9px 0px 0px #f5f5f5,  -9px 0 0px 0px #f5f5f5, 1px -1px 7px black;*/
}

.lowerleftdiv
{
width:250px;
background-color: #f5f5f5;
float: left;
height: 130px;
margin-top: 5%;
box-shadow: 0 0 7px black;
padding:5px;
color:black;
}

.lowerleftdiv .lowerleftarrowdiv
{
    width: 20px;
    height: 20px;
    border: 0px;
    transform: rotate(45deg);
    position:absolute;
    top: 205px;
    left:245px;
    background-color: #f5f5f5;
    box-shadow:  0 9px 0px 0px #f5f5f5,  -9px 0 0px 0px #f5f5f5, 1px -1px 1px #818181;
}

.upperrightdiv
{
width:250px;
background-color: #f5f5f5;
float: left;
height: 130px;
margin-left: 5%;
box-shadow: 0 0 7px black;
padding:5px;
color:black;
margin-left:35px;
}

.upperrightdiv .upperrightarrowdiv
{
    width: 20px;
    height: 20px;
    border: 0px;
    transform: rotate(45deg);
    position:absolute;
    top: 48px;
    right:280px;
    background-color: #f5f5f5;
    box-shadow:  9px 0 0px 0px #f5f5f5,  0 -9px 0px 0px #f5f5f5, -1px 1px 1px #818181;
}

.lowerrightdiv
{
width:250px;
background-color: #f5f5f5;
float: left;
height: 130px;
margin-top: 5%;
margin-left: 5%;
box-shadow: 0 0 7px black;
padding:5px;
color:black;
margin-left:35px;
}

.lowerrightdiv .lowerrightarrowdiv
{
    width: 20px;
    height: 20px;
    border: 0px;
    transform: rotate(45deg);
    position:absolute;
    top: 205px;
    right:280px;
    background-color: #f5f5f5;
    box-shadow:  9px 0 0px 0px #f5f5f5,  0 -9px 0px 0px #f5f5f5, -1px 1px 1px #818181;
}

.pdpdiv
{
min-height: 40%;
min-width: 80%;
position: absolute;
z-index: 1030;
display: none;
background-color:#f5f5f5;
box-shadow: 0 0 7px black;
margin-top:-40%;
left:35%;
padding: 16px;
text-align: justify;
color:black;
}

.pdpdiv .pdparrowdiv
{
    width: 20px;
    height: 20px;
    border: 0px;
    transform: rotate(45deg);
    position:absolute;
    top: 48px;
    right:248px;
    background-color: #f5f5f5;
    box-shadow:  9px 0 0px 0px #f5f5f5,  0 -9px 0px 0px #f5f5f5, -1px 1px 1px #818181;
}

.sidiv
{
min-height: 300px;
width: 580px;
position: absolute;
z-index: 1030;
display:none;
background:none;
/*box-shadow: 0 0 7px black;*/
margin-top:-65%;
left:-30%;
padding: 5px;
text-align: justify;
}

.pppdiv
{
min-height: 40%;
min-width: 30%;
position: absolute;
z-index: 1030;
display:none;
background-color:#f5f5f5;
box-shadow: 0 0 7px black;
margin-top:-40%;
left: -30%;
padding: 16px;
text-align: justify;
color:black;
}

.pppdiv .ppparrowdiv
{
    width: 20px;
    height: 20px;
    border: 0px;
    transform: rotate(45deg);
    position:absolute;
    top: 48px;
    left:258px;
    background-color: #f5f5f5;
    box-shadow:  0 9px 0px 0px #f5f5f5,  -9px 0 0px 0px #f5f5f5, 1px -1px 1px #818181;
}
.pdp:hover>*{
	display:block;}	
.ppp:hover>*{
	display:block;}	
.si:hover>*{
	display:block;}	

.pdpdiv:hover + .pdpdiv()

</style>
<div class="container">
<center style="margin-left: 50px;margin-right: 50px;margin-top:10px;">
<div style="color:black;font-size: 20px;"><spring:message code="msgBoostYourStatus" /></div>

<div style="font-size:12px;margin-top:10px;">
	<spring:message code="msgLet2" /> <b>TeacherMatch</b> <spring:message code="msgAwesomeAdvantageSearch" />  <br/><spring:message code="msgteachingOpportunityRevolutionary" /><br/>
	<spring:message code="msgDistrictsSchools" />
</div>

<div class="row" style="padding: 5px 0px 5px 0px;">
	<div class="col-sm-12 col-md-12">
		<div><img src="images/talentsearchepi.png" style="width:45%;"/></div>
	</div>
</div>
<div>
	<div style="font-size:12px;margin-top:2px;">
			<span><b><spring:message code="lblTmEPI" /></b></span> <spring:message code="msgPutsTeacherCandidates" /> <i><spring:message code="msgLike" /></i> <spring:message code="msgYouCompletely" /><br/>
			<spring:message code="msgUnbiasedEquitable90minutes" /><br/>
			<spring:message code="msgAmongNationalCandidates" /><br/>
			<spring:message code="msgHighlyTeachers" /> <br><br><br>
	</div>
</div>
</center>
</div>

<div class="row" align="center" style=" background-color: #E7EBEA;padding-bottom: 10px;">
<div class="col-sm-4 col-md-4">
	<div> <h1 style="font-size:17px;color:black;margin-left:-9px"><spring:message code="msgProfessionalDevelopProfile" /></h1> </div><br/>
	<div class="pdp"><img src="images/professionaldevelopment.png" style="width:80%;"/>
	
	<div class="pdpdiv" style="">
	<div class="pdparrowdiv"> </div>
	<p><img src="images/tick.png" style="width: 5%;">&nbsp;<spring:message code="msgHighlightsPersonalStrengths" />
									<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<spring:message code="msgopportunitiesGrowth" /></p>
									
	<p><img src="images/tick.png" style="width: 5%;">&nbsp;<spring:message code="msgFeaturesPractices" />
	<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<spring:message code="msgReinforceIntroduceAdvance" />
	<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<spring:message code="msgYourself" /></p>
	
	<p><img src="images/tick.png" style="width: 5%;">&nbsp;<spring:message code="msgProvidesTemplate" />
	<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<spring:message code="msgOwnPDPlanSuccess" />
	<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<spring:message code="msgBeforeYour" /> <i><spring:message code="msgFirst2" /><i/> <spring:message code="msgDayClass" /></p>
	 
	</div>
	</div>
</div>

<div class="col-sm-4 col-md-4" style="border-left:4px solid white;">
	<div><h1 style="font-size:17px;color:black;font-style:normal;"><spring:message code="msgSuccessIndicators" /></h1></div><br/>
	<div  class="si"><img src="images/sucessindicators.png" style="width:80%;"/>
	<div class="sidiv" style="font-style: normal;">			
						<div class="upperleftdiv">
						<div class="upperleftarrowdiv"></div>
						<div class="maroondiv">
						<b><spring:message code="msgTeachingSkills" /></b>
						</div>
						<ul>
						<li><spring:message code="msgPlanningSuccess" /></li>
						<li><spring:message code="msgCreatingLearningEnvironment" /></li>
						<li><spring:message code="msgInstructing2" /></li>
						<li><spring:message code="msgAnalyzingAdjusting" /></li>					
						</ul>
						</div>
						<div class="upperrightdiv">
						<div  class="upperrightarrowdiv"></div>
						<div class="greydiv">
						<b><spring:message code="msgCognitiveAbility" /></b>
						</div>
						<ul>
						<li><spring:message code="msgAwarenessPerception" /></li>
						<li><spring:message code="msgProblemSolving" /></li>
						<li><spring:message code="msgAnalyticalReasoning" /></li>
						<li><spring:message code="msgJudgment2" /></li>					
						</ul>
						</div>
				  		
				<div class="lowerleftdiv">
				<div  class="lowerleftarrowdiv"></div>
				<div class="greydiv">
						<b><spring:message code="headQualif" /></b>
						</div>
						<ul>
						<li><spring:message code="msgEducationBackground" /></li>
						<li><spring:message code="msgAcademicCredentials" /></li>
						<li><spring:message code="msgTeachingPositions" /></li>
						<li><spring:message code="msgProfessionalFieldwork" /></li>
						<li><spring:message code="msgCareerAccomplishments" /></li>						
						</ul>
						</div>
						<div class="lowerrightdiv">
						<div  class="lowerrightarrowdiv"></div>
						<div class="maroondiv">
						<b><spring:message code="msgAttitudinalFactors" /></b>
						</div>
						<ul>
						<li><spring:message code="msgMotivationSucceed" /></li>
						<li><spring:message code="msgPersistenceFaceAdversity" /></li>
						<li><spring:message code="msgMaintainingPositiveAttitude" /></li>										
						</ul>
						</div>
				  <div style="clear: both;"></div>
				
	
	</div>
	</div>
</div>

<div class="col-sm-4 col-md-4"  style="border-left:4px solid white;">
	<div><h1 style="font-size:17px;color:black;font-style:normal;"><spring:message code="msg25ProfilePowerPoints" /></h1></div><br/>
	<div class="ppp"><img src="images/ppi4.png" style="width:88%;"/>	
	<div class="pppdiv" style="font-style: normal;">
	<div class="ppparrowdiv"> </div>	
	<p><spring:message code="msgWhenCompleteEPI" />
	<br/><spring:message code="msggetBoostTowardBecoming" />
	<br/><strong><spring:message code="lblExpJoSeeker" /></strong> — <spring:message code="msgWhereYouCan" />:</p>
	<p><img src="images/tick.png" style="width: 5%;">&nbsp;<spring:message code="msgUnlockPickToolsResources" />
									<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<spring:message code="msgActivitiesYourselfEdge" />
									<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<spring:message code="msgYourjobSearch" /></p>
									
	<p><img src="images/tick.png" style="width: 5%;">&nbsp;<spring:message code="msgExpandQuest" /> <b><spring:message code="lblTmEPI" /> </b>
	<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<spring:message code="msgSchoolDistrictsAcrossCountry" /></p>
	
	<p><img src="images/tick.png" style="width: 5%;">&nbsp;<spring:message code="msgEPIPartnersDirectlyReach" />
	<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<spring:message code="msgOutYouConsider" />
	<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<spring:message code="msgGoodMatch2" /></p>
	</div>
	</div>
</div>
</div>

<br>
<div align="center" style="height: 200px; background-color: #007AB3; margin-top: 10px;">
<br/>
	<div style="color: #FFFFFF; margin-top: 15px;">
		<div class="font25" style="font-style: normal;">
			<spring:message code="msgPlus3" /> <b><spring:message code="msgExpertJobSeekers" /></b> <spring:message code="msgReceiveCopy2" /><br/> <br/>
			<b><spring:message code="msgPDP2" /></b> <spring:message code="msgreport3" /> <i><spring:message code="msgAtNoCharge" /> </i> ($39.95 value)
		</div>
	</div>

<div  class="row" style=" margin-top: 15px;">

<div class="col-sm-2 col-md-2" align="center"> </div>
<div class="col-sm-4 col-md-4" align="center">
	<c:choose><c:when test='${epistatus eq "comp" or epistatus eq null}'><a href="signin.do"><button class="signInButtonWhiteSec2 top10" type="button" style="font-size: 16px; background-color:FFFFFF; float: right; color:#474747"><spring:message code="msgStartYourEPINow" /></button></a></c:when><c:otherwise><a href="javascript:void(0);" onclick="checkInventory(0,null);"><button class="signInButtonWhiteSec2 top10" type="button" style="font-size: 16px; background-color:FFFFFF; float: right; color:#474747"><spring:message code="msgStartYourEPINow" /></button></a></c:otherwise></c:choose>
</div>

<div class="col-sm-4 col-md-4"  align="left">
	<c:choose><c:when test='${teacherDetail eq null}'><a href="signin.do"><button class="signInButtonWhiteSec2 top10" type="button" style="font-size: 16px; background-color:FFFFFF;float: left; color:#474747"><spring:message code="msgBacktoPersonalPlanning" /></button></a></c:when><c:otherwise><a href="userdashboard.do"><button class="signInButtonWhiteSec2 top10" type="button" style="font-size: 16px; background-color:FFFFFF;float: left; color:#474747"><spring:message code="msgBacktoPersonalPlanning" /></button></a></c:otherwise></c:choose>
</div>
<div class="col-sm-2 col-md-2" align="center"> </div>

</div>
</div>
<script type="text/javascript" src="dwr/interface/AssessmentCampaignAjax.js?ver=${resouceMap['AssessmentCampaignAjax.ajax']}"></script>
<script type='text/javascript' src="js/assessment-campaign.js?ver=${resouceMap['js/assessment-campaign.js']}"></script>
<jsp:include page="../tiles/feedbackandsupport3AfterLogin.jsp" />