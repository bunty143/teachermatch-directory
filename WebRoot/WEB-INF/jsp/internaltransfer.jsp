<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:if test="${redirectTo eq 1}">
	<link rel="stylesheet" href="css/dialogbox.css" />

	<div id="displayDiv"></div>

<script type="text/javascript" language="javascript" src="js/dialogboxforaction.js"></script>

	<script>
var divMsg="You are using Invalid URL. Please check it again or contact to the concerned person";
addDialogContent("dm","TeacherMatch",divMsg,"displayDiv");
showDialog("dm",urlClose,"650","200");
function urlClose(){
	window.location.href="signin.do";
}
</script>

</c:if>

<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<script type="text/javascript" src="js/internaltransfer.js"></script>

<script type="text/javascript" src="dwr/interface/InternalTransferAjax.js"></script>
<script type="text/javascript" src="dwr/interface/DWRAutoComplete.js"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>


<script>
function applyCategoryOnTbl()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#categoryTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 250,
        width: 850,
        minWidth: null,
        minWidthAuto: false,
        colratio:[200,570,80], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}
applyCategoryOnTbl();

</script>


<link rel="stylesheet" type="text/css" href="css/base.css" />

<%-- ============== Confirmation Div Message    =========== --%>
<div class="modal hide" id="InterMessageDiv" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true"
	data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true" onclick="return closeMessage();">
					x
				</button>
				<h3 id="myModalLabel">
					TeacherMatch
				</h3>
			</div>
			<div class="modal-body">
				<div class="control-group" id='interMessage'>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-primary" data-dismiss="modal"
					aria-hidden="true" onclick="return closeMessage();">
					<spring:message code="btnOk"/>
				</button>
			</div>
		</div>
	</div>
</div>

<div class="modal hide" id="myModalEmail" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true"
	data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true" onclick="return closeMailDiv();">
					x
				</button>
				<h3 id="myModalLabel">
					TeacherMatch
				</h3>
			</div>
			<div class="modal-body">
				<div class="control-group" id='messageEmail'>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-primary" data-dismiss="modal"
					aria-hidden="true" onclick="return closeMailDiv();">
					<spring:message code="btnOk"/>
				</button>
			</div>
		</div>
	</div>
</div>

<!--<div class="row centerline offset1 mt30">-->
<input id="redirectTo" name="redirectTo" type="hidden"
	value="${redirectTo}" />

<div class="row"
	style="margin-left: 0px; margin-right: 0px; margin-top: ">
	<div style="float: left;">
		<img src="${logoPath}" width="41" height="41" alt="">
	</div>
	<div style="float: left;">
		<div class="subheading" style="font-size: 13px;">
			<spring:message code="lblITR"/>
		</div>
	</div>

	<div style="clear: both;"></div>
	<div class="centerline"></div>
</div>



<div onkeypress="chkForEnterInternal(event)">
	<c:set var="readOnlyCheckForEdit" value="" />
	<c:if test="${not empty teacherId}">
		<c:set var="readOnlyCheckForEdit" value="readOnly" />
	</c:if>
	<div class="row top10">
		<div class="col-sm-8 col-md-8">
			<div class='divErrorMsg' id='errorcategorydiv'></div>
		</div>
	</div>

	<div class="row  ">
		<div class="col-sm-5 col-md-5">
			<label>
				<spring:message code="optDistrict"/>
				<span class="required">*</span>
			</label>
			<c:set var="readOnlyCheck" value="" />
			<c:if test="${not empty districtMaster}">
				<c:set var="readOnlyCheck" value="readOnly" />
			</c:if>
			<input type="hidden" id="districtId" name="districtId"
				value="${districtMaster.districtId}">
			<input type="text" class="form-control" maxlength="100"
				${readOnlyCheck}
					id="districtName" autocomplete="off"
				value="${districtMaster.districtName}" name="districtName"
				onfocus="getDistrictMasterAutoComp(this, event, 'divTxtDistrictData', 'districtName','districtId','');"
				onkeyup="getDistrictMasterAutoComp(this, event, 'divTxtDistrictData', 'districtName','districtId','');"
				onblur="hideDistrictMasterDiv(this,'districtId','divTxtDistrictData');" />
			<div id='divTxtDistrictData'
				style='display: none; position: absolute; z-index: 2000;'
				onmouseover="mouseOverChk('divTxtDistrictData','districtName')"
				class='result'></div>
		</div>
		<div class="col-sm-5 col-md-5">
			<label>
				<spring:message code="optSchool"/>
			</label>
			<c:set var="readOnlyCheck" value="" />
			<c:if test="${not empty schoolMaster}">
				<c:set var="readOnlyCheck" value="readOnly" />
			</c:if>
			<input type="hidden" id="schoolId" name="schoolId"
				value="${schoolMaster.schoolId}">
			<input type="text" class="form-control" maxlength="100"
				id="schoolName" autocomplete="off"
				value="${schoolMaster.schoolName}" name="schoolName"
				onfocus="getSchoolMasterAutoComp(this, event, 'divTxtSchoolData', 'schoolName','schoolId','');"
				onkeyup="getSchoolMasterAutoComp(this, event, 'divTxtSchoolData', 'schoolName','schoolId','');"
				onblur="hideSchoolMasterDiv(this,'schoolId','divTxtSchoolData');" />
			<div id='divTxtSchoolData'
				style='display: none; position: absolute; z-index: 2000;'
				onmouseover="mouseOverChk('divTxtSchoolData','schoolName')"
				class='result'></div>

		</div>
	</div>


	<div class="row">
		<div class="col-sm-5 col-md-5">
			<label>
				<spring:message code="lblFname"/>
				<span class="required">*</span>
			</label>
			<input id="firstName" name="firstName" type="text" maxlength="50"
				class="form-control" value="${teacherDetail.firstName}">
		</div>
		<div class="col-sm-5 col-md-5">
			<label>
				<spring:message code="lblLname"/>
				<span class="required">*</span>
			</label>
			<input id="lastName" name="lastName" type="text" maxlength="50"
				class="form-control" value="${teacherDetail.lastName}">
		</div>
	</div>

	<div class="row">
		<div class="col-sm-5 col-md-5">
			<label>
				<spring:message code="lblEmail"/>
				<span class="required">*</span>
			</label>
			<input id="email" name="email" ${readOnlyCheckForEdit} type="text"
				class="form-control" maxlength="75"
				onblur="return chkInternalCandidate(this);"
				value="${teacherDetail.emailAddress}">
		</div>
		<div class="col-sm-5 col-md-5">
			<label>
				<spring:message code="headPhone"/>
				<span class="required">*</span>
			</label>
			<input id="phone" name="phone" type="text" maxlength="20"
				class="form-control" value="${teacherDetail.phoneNumber}">
		</div>
	</div>

	<input type="hidden" name="subjectLen" id="subjectLen"
		value="${fn:length(lstSubjectMasters)}" />

	<div class="row top10">
		<div class="col-sm-10 col-md-10">
			<spring:message code="msgJobCategory"/>
		</div>
	</div>


	<div class="row">
		<div class="col-sm-11 col-md-11">
			<div class="pull-right" style="margin-right: 13px;">
				<a href="javascript:void(0);" onclick="addCategory();"><spring:message code="lnkAddJobCat"/></a>
			</div>
		</div>
	</div>


	<input type='hidden' name='districtDName' id='districtDName'
		value="${districtDName}" />
	<input type='hidden' name='districtDomains' id='districtDomains'
		value="${districtDomains}" />
	<input type='hidden' name='showcategorydivvalue'
		id='showcategorydivvalue' />
	<input type='hidden' name='categoryeditid' id='categoryeditid' />
	<input type='hidden' name='categoryids' id='categoryids' />
	<input type='hidden' name='teacherId' id='teacherId'
		value="${teacherId}" />
	<input type='hidden' name='internalCandidateId'
		id='internalCandidateId' value="${internalCandidateId}" />

	<div class="row">
		<div class="col-sm-4 col-md-4">
			<div id="showCategoryDiv"></div>
			<div id="onLoadCategoryDiv"></div>
		</div>
	</div>

	<div id="addCategoryDiv" class="hide">
		<div class="row">
			<div class='col-sm-8 col-md-8 divErrorMsg' id='errorinternaldiv'></div>
		</div>

		<div class="row">
			<div class="col-sm-4 col-md-4">
				<label>
					<strong><spring:message code="lblJobCat"/><span class="required">*</span>
					</strong>
				</label>
				<select class="form-control" id="category" name="category"
					onchange="getValue();">
					<option value="0">
						<spring:message code="optStrJobCat"/>
					</option>
					<c:forEach items="${jobCategoryMasterlst}" var="ct">
						<option id="ct${ct.jobCategoryId}" value="${ct.jobCategoryId}"${selected} >
							${ct.jobCategoryName}
						</option>
					</c:forEach>
				</select>
			</div>
		</div>

		<div class="row ">
			<div class="col-sm-10 col-md-10">
				<span style="display: inline-block;"><spring:message code="lblSubject(s)"/><span
					class="required">*</span>
				</br><spring:message code="msgSelectSub"/></span>
			</div>
		</div>
		<div class="row top10">
			<c:forEach items="${lstSubjectMasters}" var="sb">
				<div class="col-sm-3 col-md-3">
					<label class="checkbox inline top0">
						<input type="checkbox" name="subject" id="subject${sb.subjectId}"
							value="${sb.subjectId}||${sb.subjectName}">
						${sb.subjectName}
					</label>
				</div>
			</c:forEach>

		</div>

		<div class="row">
			<div class="col-sm-4 col-md-4">
				<a href="javascript:void(0);" onclick="validateCategory();"><spring:message code="lnkImD"/></a>&nbsp;&nbsp;&nbsp;&nbsp;
				<a href="javascript:void(0)" onclick="clearCategory();"><spring:message code="btnClr"/></a>
			</div>
		</div>

	</div>



	<div class="row top10">
		<div class="col-sm-3 col-md-3">
			<button class="btn btn-large btn-primary"
				onclick="return validateInternalTransfer()">
				<spring:message code="btnSub"/>
				<i class="icon"></i>
			</button>
		</div>
	</div>



</div>


<div class="TableContent top15">
	<div class="table-responsive" id="divJobsBoard">

	</div>
</div>



<div style="display: none;" id="loadingDiv">
	<table align="center">
		<tr>
			<td style="padding-top: 270px;" align="center">
				<!-- <img src="images/please.jpg"/> -->
			</td>
		</tr>
		<tr>
			<td style="padding-top: 0px;" align="center">
				<img src="images/loadingAnimation.gif" />
			</td>
		</tr>
		<tr id='paymentMessage'>
			<td style='padding-top: 0px;' id='spnMpro' align='center'></td>
		</tr>
	</table>
</div>

<br />

<script>
onLoadInternalPage();
</script>
