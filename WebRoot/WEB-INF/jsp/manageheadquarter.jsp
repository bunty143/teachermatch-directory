<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<script type="text/javascript" src="js/manageuser.js?ver=${resourceMap['js/manageuser.js']}"></script>
<script type="text/javascript" src="dwr/interface/HeadQuarterAjax.js?ver=${resourceMap['HeadQuarterAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type='text/javascript' src="js/manageheadquarter.js?ver=${resourceMap['js/manageheadquarter.js']}"></script>

<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" />
<script>
var $j=jQuery.noConflict();
$j(document).ready(function() {
});
function applyScrollOnTbl()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 400,
        width: 945,
        minWidth: null,
        minWidthAuto: false,
         <c:if test="${userMaster.entityType ne 5}">
           colratio:[290,150,80,70,70,60,90,60,102], // table header width
        </c:if>
                
        <c:if test="${userMaster.entityType ne 5}">
           colratio:[290,150,80,70,70,90,60,102], // table header width
        </c:if>
        
        <c:if test="${userMaster.entityType eq 5}">
           colratio:[290,150,120,100,110,60,102], // table header width
        </c:if>
        
        
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}

</script>


<style>
	.hide
	{
		display: none;
	}
</style>
<div class="row" style="margin-left: 0px;margin-right: 0px;margin-top:">
         <div style="float: left;">
         	<img src="images/manageusers.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;">Manage HeadQuarter<span id="districtOrSchoolTitle"></span></div>	
         </div>
		
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
</div>
<!--<div class="offset1 span14 mb">-->
   <c:if test="${userMaster.entityType eq 1}">
   <div class="mt10"></div>
   </c:if>   
	<div class="row" onkeypress="return chkForEnterShowHeadQuarter(event);" >		
			<form class="bs-docs-example" onsubmit="return false;">
				<div id="Searchbox" class="<c:out value="${hide}"/>">
					<div class="col-sm-3 col-md-3">
						<label>Entity Type</label>
					    <select class="form-control" id="MU_EntityType" name="MU_EntityType" class="form-control" onchange="DisplayHideSearchBox();">
								<option value="1" selected>TM</option>
								<option value="2">District</option>
								<option value="5">HeadQuarter</option>
								<option value="6">Branch</option>
						</select>
	              	</div>
		            <div id="SearchTextboxDiv"  <c:out value="${hide}"/>">	         		   
		             		<div  class="col-sm-5 col-md-5">
		             			<label id="captionDistrictOrSchool">HeadQuarter</label>
			             		<input type="text" id="headQuarter" name="headQuarter"  class="form-control"
			             		onfocus="getHeadQuarterMasterAutoComp(this, event, 'divTxtShowData', 'headQuarter','headQuarterId','');"
								onkeyup="getHeadQuarterMasterAutoComp(this, event, 'divTxtShowData', 'headQuarter','headQuarterId','');"
								onblur="hideHeadQuarterMasterDiv(this,'headQuarterId','divTxtShowData');"	/>
							<div id='divTxtShowData' style=' display:none;position:absolute;z-index:5000;' onmouseover="mouseOverChk('divTxtShowData','headQuarter')" class='result' ></div>	
			             	</div>		             
	          		</div>
					<div class="col-sm-2 col-md-2">					
						<c:if test="${fn:indexOf(roleAccess,'|5|')!=-1}">
						<button class="btn btn-primary top25-sm" type="button" onclick="searchDistrict();">Search <i class="icon"></i></button>
						</c:if>
					</div>
					 
        	  	</div>
       		 </form>
		</div>            
	
	
	<div  class="modal hide"  id="myModalUpdateMsg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"   style="z-index: 5000;"  data-backdrop="static">
		<div class="modal-dialog">
		<div class="modal-content">
		<div class="modal-header">
	  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
			<h3 id="myModalLabel">Manage Headquarter</h3>
		</div>
		<div class="modal-body">		
			<div class="control-group">
				<div class="">
			    	<span id="updateMsg"></span>	        	
				</div>
			</div>
	 	</div>
	 	<div class="modal-footer">
	 		<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button> 		
 		</div>
	</div>
</div>
</div>
  <div class="TableContent top15">        	
            <div class="table-responsive" id="divMain">          
                	         
            </div>            
   </div> 
	
    <br><br>
    <div style="display:none;" id="loadingDiv">
    <table  align="center">
 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
	</table>
	</div>
<!-- Message Div add by Rahul -->

<div  class="modal hide"  id="myModalactMsgShow"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="showMessageDiv()">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div class="modal-body">
		<div class="control-group" id='confirmDeactivate'>Are you sure want to deactivate the HeadQuarter?
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn btn-primary" data-dismiss="modal" aria-hidden="true" onclick="toggleStatus()">Yes</button>
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="showMessageDiv()">No</button>
 	</div>
 	<input type="hidden" id="acthq" name="acthq">
 	<input type="hidden" id="hqstat" name="hqstat">
 	
</div> 	
</div>
</div>


<!-- END -->
<script>
onLoadDisplayHideSearchBox(<c:out value="${userMaster.entityType}"/>);
updateMsg('<c:out value="${authKeyVal}"/>',<c:out value="${userMaster.entityType}"/>);
	
</script>
 