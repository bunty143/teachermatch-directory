<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>

<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" /> 
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type='text/javascript' src="js/myportfolio.js?ver=${resourceMap['js/myportfolio.js']}"></script>
<script type="text/javascript" src="dwr/interface/MyDashboardAjax.js?ver=${resouceMap['MyDashboardAjax.ajax']}"></script>

<script type="text/javascript" src="dwr/interface/PFExperiences.js?ver=${resourceMap['PFExperiences.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/PFCertifications.js?ver=${resourceMap['PFCertifications.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/DWRAutoComplete.js?ver=${resourceMap['DWRAutoComplete.ajax']}"></script>
<script type='text/javascript' src="js/teacher/pfAutoCompCertificate.js?ver=${resourceMap['js/teacher/pfAutoCompCertificate.js']}"></script>

<script>
var $j=jQuery.noConflict();
$j(document).ready(function() {
});
function showcertiDiv()
{
	$('#draggableDivMaster').modal('show');
}

function applyScrollOnTbl()
{
	//alert("Hi")
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblGridCertifications').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 380,
        width: 850,
        minWidth: null,
        minWidthAuto: false,
       	colratio:[250,90,95,140,185,90],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
    });			
}

</script>
<style>
.tooltip-inner 
{
  max-width: 700px;
}
.divwidth
{
float: left;
width: 40px;
}
.btn-dangerevents {
  color: #ffffff;
  text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
  background-color: #da4f49;
  *background-color: #bd362f;
  background-image: -moz-linear-gradient(top, #ee5f5b, #bd362f);
  background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#ee5f5b), to(#bd362f));
  background-image: -webkit-linear-gradient(top, #ee5f5b, #bd362f);
  background-image: -o-linear-gradient(top, #ee5f5b, #bd362f);
  background-image: linear-gradient(to bottom, #ee5f5b, #bd362f);
  background-repeat: repeat-x;
  border-color: #bd362f #bd362f #802420;
  border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffee5f5b', endColorstr='#ffbd362f', GradientType=0);
  filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);
}
.btn-dangerevents:hover,
.btn-dangerevents:focus,
.btn-dangerevents:active,
.btn-dangerevents.active,
.btn-dangerevents.disabled,
.btn-danger[disabled] {
  color: #ffffff;
  background-color: #bd362f;
  *background-color: #a9302a;
}

.btn-dangerevents:active,
.btn-dangerevents.active {
  background-color: #942a25 \9;
}

strong {
font-weight: normal;
}


</style>


<div class="row" style="margin:0px;">
         <div style="float: left;">
         	<img src="images/certifications.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;"><spring:message code="msgMyPortfolio"/> </div>	
         </div>			
		 
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
</div>
<br>
	<div class="row" >
	   <div class="col-sm-12 col-md-12" style="font-size: 14px;">
		   <spring:message code="msgWelcome"/>, ${fullName}
	   </div>
	</div>

	<input type="hidden" id="isdocumentonfileId" name="isdocumentonfileId" value="${teacherExperience.isDocumentOnFile}"/>
	<div class="row top5" >
		<div class="col-sm-12 col-md-12">
		<div class='divErrorMsg' id='errordiv0' style="display: block;"></div>
		    <spring:message code="msgUnsatisfactory"/>
		</div>
		
		<div class="col-sm-12 col-md-12">
		   <div class="radio inline col-sm-1 col-md-1">
					 <input type="radio" id="isdocumentonfile0" value="0" name="isdocumentonfile"> <spring:message code="lblNo"/>
		   </div><br>
		   <div class="radio inline col-sm-1 col-md-1 top20down">
		    	<input type="radio" id="isdocumentonfile1" value="1" name="isdocumentonfile"> <spring:message code="lblYes"/>
		    </div>
		   
		</div>
	</div>	
		
			<%-- resume section start --%>
			<div class="panel-group" id="accordion">
			 <div class="panel panel-default">
				<div class="accordion-heading">
					<a href="#collapseOne"	data-parent="#accordion" id="certificationTypeDiv" data-toggle="collapse" class="accordion-toggle minus"><spring:message code="lnkCerti/Lice"/> </a></div>
				<div class="accordion-body in" id="collapseOne" style="height: auto;">
					<div class="accordion-inner">
						<div class="offset1">
							<div class="row">
								<div style='width: 850px;'>
									<div class="pull-right add-employment1">
										<a href="javascript:void(0);"
											onclick="showForm(),clearForm();fieldsDisable(0);"><spring:message code="lnkAddCerti/Lice"/></a>
									</div>
								</div>
							</div>
							<div class="row">
							    <div class="col-sm-10 col-md-10">
								<div id="divDataGrid" class="mt10"></div>
								</div>
							</div>
							<div  id="divMainForm"
								style="display: none;">
								<iframe id='uploadCertFrameID' name='uploadCertFrame' height='0'
									width='0' frameborder='0' scrolling='yes' sytle='display:none;'>
								</iframe>
								<form id="frmCertificate" name="frmCertificate"
									enctype='multipart/form-data' method='post'
									target='uploadCertFrame'
									action='fileuploadservletforcertification.do'>
									<input type="hidden" id="certId" name="certId" />
									<div class="row top15">
									<div class="col-sm-12 col-md-12">
										<div id='divServerError' class='divErrorMsg'
											style="display: block;">
											${msgError}
										</div>
										<div class='divErrorMsg' id='errordiv' style="display: block;"></div>
									</div>

									<div class="col-sm-4 col-md-4">
										<label>
											<strong><spring:message code="lblCerti/LiceSt"/><span
												class="required">*</span>
											</strong>
										</label>
										<select class="form-control" id="certificationStatusMaster" onchange="showHideFileUpload(this.value);fieldsDisable(this.value);">
											<option value="">
												<spring:message code="lblSltCerti/LuceStatus"/>
											</option>
											<c:forEach items="${lstCertificationStatusMaster}" var="csml">
												<option id="csml${csml.certificationStatusId}"
													value="${csml.certificationStatusId}"${selected} >
													${csml.certificationStatusName}
												</option>
											</c:forEach>
										</select>
									</div>

									<div class="col-sm-3 col-md-3">
										<label>
											<strong><spring:message code="lblCertiRp"/><span class="required">*</span>
											</strong>
										</label>
										<select id="certificationtypeMaster" name="certificationtypeMaster" class="form-control">
											<option value="0"><spring:message code="optSlt"/></option>
											 <c:forEach var="cvar" items="${listCertTypeMasters}">							
								<option  value="${cvar.certificationTypeMasterId}">${cvar.certificationType}</option>
  											</c:forEach>
										</select>
									</div>

									<div class="col-sm-4 col-md-4">
										<label>
											<strong><spring:message code="lblSt"/><span class="required">*</span>
											</strong>
										</label>
										<select class="form-control" id="stateMaster"
											onchange="getPraxis(),clearCertType();">
											<option value="">
												<spring:message code="optSltSt"/>
											</option>
											<c:forEach items="${listStateMaster}" var="st">
												<option id="st${st.stateId}" value="${st.stateId}"${selected} >
													${st.stateName}
												</option>
											</c:forEach>
										</select>
									</div>
									
									<div class="col-sm-2 col-md-2" style="width:14%;"><label><strong><spring:message code="lblYearRece"/><span class="required">*</span></strong></label>
										<select id="yearReceived" name="yearReceived"  class="form-control" >
											<option value=""><spring:message code="optSlt"/></option>
											<c:forEach var="year" items="${lstLastYear}">							
												<option id="yearReceived${year}" value="${year}">${year }</option>
											</c:forEach>					
										</select>
									</div>
									
									<div class="col-sm-2 col-md-2" style="width:19.5%;">	
										<label><strong><spring:message code="lblYearExp"/><span class="required"></span></strong></label>
										<select class="form-control" name="yearexpires" id="yearexpires" style="padding-left:6px;">
											<option value=""><spring:message code="optDoesNotExpire"/></option>
											<c:forEach var="year" items="${lstComingYear}">							
												<option id="yearExpires${year}" value="${year}">${year }</option>
											</c:forEach>
										</select>
									</div>

									<div class="col-sm-7 col-md-7">
									<label><strong><spring:message code="lblCert/LiceName"/><span class="required">*</span></strong>
										<a href="#" style="margin-left: 100px;" onclick="showcertiDiv()"><spring:message code="lnkCerti/LiceNotLi"/></a>
									</label>
			
										<input type="hidden" id="certificateTypeMaster" value="">
										<input type="text" class="form-control" maxlength="500" id="certType"
											value="" name="certType"
											onfocus="getCertificateTypeAutoComp(this, event, 'divTxtCertTypeData', 'certType','certificateTypeMaster','');"
											onkeyup="getCertificateTypeAutoComp(this, event, 'divTxtCertTypeData', 'certType','certificateTypeMaster','');"
											onblur="hideCertificateType(this,'certificateTypeMaster','divTxtCertTypeData');" />
										<div id='divTxtCertTypeData' style='display: none; position: absolute;'	onmouseover="mouseOverChk('divTxtCertTypeData','certType')"	class='result'></div>
									</div>

									<div class="col-sm-2 col-md-2">
										<label><strong><spring:message code="lblDOENum"/></strong></label>
										<input type="text" class="form-control" name="doenumber" id="doenumber" maxlength="25" value="" />
									</div>

									<div class="col-sm-9 col-md-9">
										<label>
											<strong><spring:message code="lblCerti/LiceUrl"/> <a href="#" id="iconpophoverCertification" rel="tooltip" data-original-title="<spring:message code="tooltipMyPortfolio1"/>"><img src="images/qua-icon.png" width="15" height="15" alt=""></a></strong>
										</label>

										<input type="text" class="form-control" maxlength="500" id="certUrl"
											value="" name="certUrl" />

									</div>
									</div>
									<div class="row">	
									<div class="col-sm-12 col-md-12">
									<label><strong><spring:message code="lblGradeLvl"/></strong></label>
									</div>
									</div>
								<div class="row col-sm-12 col-md-12" style="margin-top: -10px;">	
									<div class="divwidth">										
											<label class="checkbox inline">
												<input type="checkbox" value="1" id="pkOffered"
													name="pkOffered">
												<spring:message code="lblPK"/>
											</label>
									</div>
									<div class="divwidth">			
											<label class="checkbox inline">
												<input type="checkbox" value="1" id="kgOffered"
													name="kgOffered">
												<spring:message code="lblKG"/>
											</label>
									</div>
									<div class="divwidth">			
											<label class="checkbox inline">
												<input type="checkbox" value="1" id="g01Offered"
													name="g01Offered">
												<spring:message code="lbl1"/>
											</label>
									</div>
									<div class="divwidth">			
											<label class="checkbox inline">
												<input type="checkbox" value="1" id="g02Offered"
													name="g02Offered">
												<spring:message code="lbl2"/>
											</label>
									</div>
									<div class="divwidth">			
											<label class="checkbox inline">
												<input type="checkbox" value="1" id="g03Offered"
													name="g03Offered">
												<spring:message code="lbl3"/>
											</label>
									</div>
									<div class="divwidth">			
											<label class="checkbox inline">
												<input type="checkbox" value="1" id="g04Offered"
													name="g04Offered">
												<spring:message code="lbl4"/>
											</label>
									</div>
									<div class="divwidth">			
											<label class="checkbox inline">
												<input type="checkbox" value="1" id="g05Offered"
													name="g05Offered">
											<spring:message code="lbl5"/>
											</label>
									</div>
									<div class="divwidth">			
											<label class="checkbox inline">
												<input type="checkbox" value="1" id="g06Offered"
													name="g06Offered">
												<spring:message code="lbl6"/>
											</label>
									</div>
									<div class="divwidth">			
											<label class="checkbox inline">
												<input type="checkbox" value="1" id="g07Offered"
													name="g07Offered">
												<spring:message code="lbl7"/>
											</label>
									</div>
									<div class="divwidth">			
											<label class="checkbox inline">
												<input type="checkbox" value="1" id="g08Offered"
													name="g08Offered">
												<spring:message code="lbl8"/>
											</label>
									</div>
									<div class="divwidth">			
											<label class="checkbox inline">
												<input type="checkbox" value="1" id="g09Offered"
													name="g09Offered">
											<spring:message code="lbl9"/>
											</label>
									</div>
									<div class="divwidth">			
											<label class="checkbox inline">
												<input type="checkbox" value="1" id="g10Offered"
													name="g10Offered">
												<spring:message code="lbl10"/>
											</label>
									</div>
									<div class="divwidth">			
											<label class="checkbox inline">
												<input type="checkbox" value="1" id="g11Offered"
													name="g11Offered">
												<spring:message code="lbl11"/>
											</label>
									</div>
									<div class="divwidth">			
											<label class="checkbox inline">
												<input type="checkbox" value="1" id="g12Offered"
													name="g12Offered">
												<spring:message code="lbl2"/>
											</label>
									</div>								
									</div>
									<div id='praxisArea' class="row">
										<div class="col-sm-2 col-md-2">										
											<label>
												<strong><spring:message code="lblReqPrxITests"/></strong>
											</label>
										</div>
										<div class="col-sm-3 col-md-3" >
											<label>
												<strong id='reading'></strong>
											</label>
											<div class="row">
											<div class="col-sm-8 col-md-8">
												<label>
												<strong><spring:message code="lblYurScore"/> <input type='text'
														maxlength="3" id='readingQualifyingScore' class="form-control"
														onkeypress="return checkForInt(event)">
												</strong>
											</label>
											</div>											
											</div>
										 </div>
	        							 <div class="col-sm-3 col-md-3">
											<label>
												<strong id='writing'></strong>
											</label>
											<div class="row">
											<div class="col-sm-8 col-md-8">
											   <label>
												<strong><spring:message code="lblYurScore"/> <input type='text'
														maxlength="3" id='writingQualifyingScore' class="form-control"
														onkeypress="return checkForInt(event)">
												</strong>
											    </label>
											</div>											
											</div>
										 </div>
	         							 <div class="col-sm-3 col-md-3">
											<label>
												<strong id='maths'></strong>
											</label>
											<div class="row">
											<div class="col-sm-8 col-md-8">
												<label>
												<strong><spring:message code="lblYurScore"/> <input type='text'
														maxlength="3" id='mathematicsQualifyingScore'
														class="form-control" onkeypress="return checkForInt(event)">
												</strong>
											    </label>
											</div>											
											</div>
										 </div>
	         							</div>
	         						  <div class="row">	  
									 <div class="col-sm-4 col-md-4" id="fileuploadDiv">
										<label>
											<spring:message code="lblCerti/LiceLtr"/>
										</label>
										<input type="hidden" id="sbtsource_cert" name="sbtsource_cert"
											value="0" />
										<input id="pathOfCertificationFile"
											name="pathOfCertificationFile" type="file" width="20px;">
										<a href="javascript:void(0)" id="clearLink" onclick="clearCertification()"><spring:message code="lnkClear"/></a>
									 </div>
									<input type="hidden" id="pathOfCertification" />
									<span class="col-sm-4 col-md-4" id="removeCert" name="removeCert"
										style="display: none;"> <label>
											&nbsp;&nbsp;
										</label> <span id="divCertName"></span> <a href="javascript:void(0)"
										onclick="removeCertificationFile()"><spring:message code="lnkRemo"/></a>&nbsp; <a
										href="#" id="iconpophover6" rel="tooltip"
										data-original-title="Remove certification/Licence file !">
											<img src="images/qua-icon.png" width="15" height="15" alt="">
									</a> </span>
									</div>
									<iframe src="" id="ifrmCert" width="100%" height="480px"
										style="display: none;">
									</iframe>
								</form>
								<input type="hidden" value="" id="certText"/>
								<div class="span16 idone" style="padding-top: 10px;">
									<a id="hrefDone" href="#"
										style="cursor: pointer; margin-left: 0px; text-decoration: none;"
										onclick="return insertOrUpdate(0)"><spring:message code="lnkImD"/></a>&nbsp;&nbsp;
									<a class="idone"
										style="cursor: pointer; margin-left: 0px; text-decoration: none;"
										onclick="return hideForm()"><spring:message code="btnClr"/></a>
								</div>
							</div>
							<!--Close form -->

						</div>
						<div class="clearfix">
							&nbsp;
						</div>
					</div>
				</div>
			</div>
			
			
			
			
			<div class="panel panel-default">
			<div class="accordion-heading"> <a href="#collapseTwo" data-parent="#accordion" data-toggle="collapse" id="resumeDiv" class="accordion-toggle plus"> <spring:message code="lblResume"/> </a> </div>
				<div class="accordion-body collapse" id="collapseTwo" style="height:auto;">
                	<div class="accordion-inner">
						<div class="offset1">
                      		<div class="row">
                      			<div class="col-sm-12 col-md-12">
                      				
									<iframe id='uploadFrameID' name='uploadFrame' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'>
									</iframe>
    								<%--<form class="form-inline"> --%>
    								<div></div>
    								<div class='divErrorMsg' id='errordiv1' style="display: block;"></div>
    								<form id='frmExpResumeUpload' enctype='multipart/form-data' method='post' target='uploadFrame' action='expResumeUploadServlet.do?f=${teacherExperience.resume}' class="form-inline">
    									<input type="hidden" id="hdnResume" value="${teacherExperience.resume}" /> 
    									<label><spring:message code="lblResume"/><span class="required">*</span></label><br/>
										<input name="resume" id="resume" type="file"><br/>
   										<!-- <input type="text" class="input-medium search-query">  <button type="submit" class="btn">Attach</button>-->
   										<div id="divResumeTxt" style="margin-top:5px;">
   											<spring:message code="lblRecentResOnFi"/>
   											<label id="lblResume">
   												<c:if test="${empty teacherExperience.resume}">
   													<spring:message code="lblNone"/>
   												</c:if>
   												<a href="javascript:void(0)" id="hrefResume" onclick="downloadResume();if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;">${teacherExperience.resume}</a>
   											</label>
   										</div>
    									<div class="row">
    										<div class="span3"><!--a href="#" onclick="return removeResume()"> Remove</a  --></div>
    									</div>
    								</form>
                      			</div>
                      		</div>
                      	</div>
						<div class="clearfix">&nbsp;</div>
                   	</div>
				</div>
				</div>
        	
            	
<%-- resume section End --%>
                
             
	        </div>  	
	 
<div class="row">
	<div class="">
	<br>
	<div class="col-sm-4 col-md-4" >
		<button class="btn btn-primary"  onclick="return saveDoumentSatisfactoryAnswer();">
			<strong><spring:message code="btnSavenExit"/> <i class="icon"></i></strong> 
		</button>
		<button onclick="canclePortfolio()" class="btn btn-dangerevents" id="save">
		   <strong><spring:message code="btnClr"/> <i class="icon"></i></strong>
		</button>
		<br><br>
	</div>
	</div>
</div>
<iframe src="" id="ifrmResume" width="100%" height="480px" style="display: none;">
</iframe>


<div  class="modal hide" id="draggableDivMaster" style="" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog-for-cgmessage">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabel"><spring:message code="lblCert/LiceName"/></h3>
	</div>
	<div class="modal-body">
		<p><spring:message code="msgWeArSoryYuNotFindingYurCerti/Lice"/></p>
		<p><spring:message code="msgMyPortfolio2"/></p>
		<p><spring:message code="msgMyPortfolio3"/> 
		<a href="mailto:clientservices@teachermatch.net" target="_top">clientservices@teachermatch.net</a>
		 <spring:message code="pHlpYuLoadRiLice/Certi"/></p> 
		<p><spring:message code="pThkForYurHlpInThisMtr"/></p>
	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnOk"/></button>
 	</div>
</div>
	</div>
</div>


<div class="modal hide"  id="deleteCertRec" style="border: 0px solid blue;" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div id="deact" class="modal-body"> 		
		<div class="control-group">
			<spring:message code="msgDeleteTheRecord"/>
		</div>
 	</div>
 	<div class="modal-footer">
 	<input type="hidden" id="certificateTypeID"/>
 	<span id=""><button class="btn  btn-primary" onclick="deleteCertificateRecord()"><spring:message code="btnOk"/><i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClr"/></button> 		
 	</div>
</div>
 	</div>
</div>


<script type="text/javascript">

showCertGrid();
checkdocumentonfile()
$('#iconpophover4').tooltip();
$('#iconpophover5').tooltip();
$('#iconpophover6').tooltip();
$('#iconpophover7').tooltip();
$('#iconpophoverCertification').tooltip();

var selectIds = $('#collapseOne,#collapseTwo');
$(function ($) {
    selectIds.on('show.bs.collapse hidden.bs.collapse', function () {
        $(this).prev().find('.accordion-toggle').toggleClass('plus minus');
    })
});


$('.accordion').collapse();
$('.accordion').on('show', function (e) {
$(e.target).prev('.accordion-heading').find('.accordion-toggle').addClass('minus').removeClass('plus');
});

$('.accordion').on('hide', function (e) {
$(this).find('.accordion-toggle').not($(e.target)).removeClass('minus').addClass('plus');
});
</script>

<script>
function fileContainsVirusDiv(msg)
{
	$('#loadingDiv').hide();
	$('#virusDivId .modal-body').html(msg);
	$('#myModalDymanicPortfolio').modal('hide');
	$('#virusDivId').modal('show');
}
</script>

<div class="modal hide" id="virusDivId" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog-for-cgcoverletter" style='width: 35%;'>
		<div class="modal-content">
			<div class="modal-header">
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body" style="max-height: 400px;overflow-y:auto;">
				<div class="control-group">
				</div>
			</div>
			<div class="modal-footer">
				<span id=""><button class="btn btn-primary" data-dismiss="modal"><spring:message code="btnOk"/> <i class="icon"></i></button></span>&nbsp;&nbsp;
			</div>
		</div>
	</div>
</div>