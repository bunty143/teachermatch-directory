<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>

<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" /> 
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type='text/javascript' src="js/wecanexport.js?ver=${resourceMap['js/wecanexport.js']}"></script>
<script type="text/javascript" src="dwr/interface/WeCanExportAjax.js?ver=${resourceMap['WeCanExportAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/ManageJobOrdersAjax.js?ver=${resourceMap['ManageJobOrdersAjax.ajax']}"></script>

<script type='text/javascript' src="js/certtypeautocomplete.js?ver=${resourceMap['js/certtypeautocomplete.js']}"></script>
<!--<script type="text/javascript" src="dwr/interface/DWRAutoComplete.js?ver=${resouceMap['DWRAutoComplete.ajax']}"></script>
-->
<script type="text/javascript" src="dwr/interface/CGServiceAjax.js?ver=${resouceMap['CGServiceAjax.ajax']}"></script>
<script src="calender/js/jscal2.js"></script>
<script src="calender/js/lang/en.js"></script>
<link rel="stylesheet" type="text/css" href="calender/css/jscal2.css" />
<link rel="stylesheet" type="text/css"
	href="calender/css/border-radius.css" />
<link rel="stylesheet" type="text/css" href="calender/css/gold/gold.css" />


<style>
div.t_fixed_header div.body {
	overflow-x	: auto;
}
</style>

<script>
var $j=jQuery.noConflict();
$j(document).ready(function() {
});


function applyScrollOnTblEEC()
{
   //alert("11")
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblGridEEC').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        //height: 300,
        width: 945,
        minWidth: null,
        minWidthAuto: false,
        colratio:[190,180,140,80,80,90,80,100,180,90,90,90,65,60,60,60],
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });	
}

function applyScrollOnPrintTable()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblGridEECPrint').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 300,
         width: 1038,
        minWidth: null,
        minWidthAuto: false,
        colratio:[190,180,140,80,80,90,80,100,180,90,90,90,65,60,60,60],
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });	
}

</script>

<style>
.pdfDivBorder
{
z-index: 5000;
border: 1px solid #999;
border: 1px solid rgba(0, 0, 0, 0.3);
-webkit-border-radius: 6px;
-moz-border-radius: 6px;
border-radius: 6px;
-webkit-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
-moz-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
-webkit-background-clip: padding-box;
-moz-background-clip: padding-box;
background-clip: padding-box; 
}
</style>

<div class="row" style="margin:0px;">
         <div style="float: left;">
          <input type="hidden" id="entityType" name="entityType" value="${userMaster.entityType}">
         	<img src="images/districtjobs-orders.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;">WECAN Export </div>	
         </div>			
		 
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
</div>
<br>
   
   
   		<div class="row" style="padding-bottom:10px;">
		       <div class="col-sm-4 col-md-4">
				     <label id="captionDistrictOrSchool">District Name</label>
				        <c:if test="${districtName==null}">
					       <span>
					        <input type="text" maxlength="255" id="districtORSchoolName" name="districtORSchoolName" class="help-inline form-control"
					            		 onfocus="getDistrictORSchoolAuto(this, event, 'divTxtShowData', 'districtORSchoolName','districtOrSchoolId','');"
													onkeyup="getDistrictORSchoolAuto(this, event, 'divTxtShowData', 'districtORSchoolName','districtOrSchoolId','');"
													onblur="hideDistrictMasterDiv(this,'districtOrSchoolId','divTxtShowData');"/>
							 </span>
							 <input type="hidden" id="districtOrSchooHiddenlId"/>							
				         </c:if>
				    		
							<c:if test="${districtName!=null}"> 
							<span>
					        <input type="text" maxlength="255"  class="help-inline form-control" value="${districtName}"  disabled="disabled"/>
							 </span>
				             	<input type="hidden" id="districtOrSchooHiddenlId" value="${districtId}"/>
				            </c:if>
				    <div id='divTxtShowData'  onmouseover="mouseOverChk('divTxtShowData','districtORSchoolName')" style=' display:none;position:absolute;z-index:5000;' class='result' ></div>	
			   </div>
			   <div class="col-sm-3 col-md-3">
								<span class=""><label class="">
										Start Date
									</label> <input type="text" id="sfromDate" name="sfromDate"
										class="help-inline form-control">
								</span>
							</div>
							<div class="col-sm-3 col-md-3">
								<span class=""><label class="">
										End Date
									</label> <input type="text" id="stoDate" name="stoDate"
										class="help-inline form-control">
								</span>
							</div>
							 <div class="col-sm-2 col-md-2 top25-sm2" style="width: 150px;">
						        <label class=""></label>
						        <button class="btn btn-primary " type="button" onclick=" searchWecanExport();">&nbsp;&nbsp;Search&nbsp;&nbsp; <i class="icon"></i></button>
						   </div>
 			</div>
	
         
   
   
   		<div id="" class="hide" style="height: 480px; width: 100%; overflow: auto;'-webkit-overflow-scrolling' : 'touch">
			 <iframe src="" id="ifrmTrans" width="100%" height="100%" scrolling="no">
			 </iframe>  
		  </div>
		  
		  <!--<div id="" class="hide" style="height: 480px; width: 100%; overflow: auto;'-webkit-overflow-scrolling' : 'touch">
			 <iframe src="" id="ifrmCSV" width="100%" height="100%" scrolling="no">
			 </iframe>  
		  </div>
          
       --><div class="row"> 
	       
		         <div class="col-sm-6 col-md-">
		         	 	<%-- 
			           <div id="searchLinkDiv" class="mt5">
						    <a href="javascript:void:(0);" onclick="displayAdvanceSearch()">Open Advanced Search</a>
				     	 </div> 
						
						<div id="hidesearchLinkDiv" class=" hide mt10">
						   <a href="javascript:void:(0);" onclick="hideAdvanceSearch()">Close Advanced Search</a>
						</div>
						--%>
	       		</div>
	       		
       		
        <div class="col-sm-6 col-md-6 hide" id="exportIconDiv">
         <table class="marginrightForTablet">
		      <tr>
		      	<td  style='padding-left:5px;'>
					<a data-original-title='Export to CSV' rel='tooltip' id='csvId' href='javascript:void(0);' onclick="downloadCSVReport(); "><span class='export icon-file-csv icon-large iconcolor'></span></a>
				 </td>
		      	  <td  style='padding-left:5px;'>
					<a data-original-title='Export to Excel' rel='tooltip' id='exlId' href='javascript:void(0);'  onclick="generateCandidateJobStatusxcel();"><span class='icon-file-excel icon-large iconcolor'></span></a>
				 </td>
				 <td style='padding-left:5px;'>
				  <a data-original-title='Export to PDF' rel='tooltip'  href="javascript:void(0);" id='pdfId' onclick='downloadCandidateJobStatusReport()'><span class='pdf-eoc icon-large iconcolor'></span></a>
				 </td>
				 <td  style='padding-left:5px;'>
					<a data-original-title='Print Preview' rel='tooltip' id='printId' href='javascript:void(0);' onclick="generateCandidateJobStatusPrintPre();"><span class='icon-print icon-large iconcolor'></span></a>
				 </td>  
			</tr>
		</table>
        </div>
       	
   </div>   
           <div style="clear: both;"></div>
           
           
           <div style="display:none;" id="loadingDiv">
			    <table  align="center">
			 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
			 		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
				</table>
			</div>
           
           
     <!-- PDF -->
<div  class="modal hide pdfDivBorder"  id="modalDownloadCandidateJobStatus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog-for-cgpdfreport">
	<div class="modal-content"  style="background-color: #ffffff;">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">WECAN Export</h3>
	</div>
		<div class="modal-body">		
			<div class="control-group">
				    <div style="width: 100%; height: 100%; -webkit-overflow-scrolling: touch;">
	                  <iframe src="" id="ifrmCJS" width="100%" height="480px">
	               </iframe>        	
				</div>
			</div>
	 	</div>
	 	<div class="modal-footer">
	 		<button class="btn" data-dismiss="modal" aria-hidden="true" >Close</button> 		
	 	</div>
     </div>
 	</div>
</div>

<!-- PDF End -->


     <!-- CSV -->
<div  class="modal hide pdfDivBorder"  id="modalDownloadCandidateCSV" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog-for-cgpdfreport">
	<div class="modal-content"  style="background-color: #ffffff;">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">WECAN Export </h3>
	</div>
		<div class="modal-body">		
			<div class="control-group">
				    <div style="width: 100%; height: 100%; -webkit-overflow-scrolling: touch;">
	                  <iframe src="" id="ifrmCSV" width="100%" height="100%" scrolling="no"></iframe>        	
				</div>
			</div>
	 	</div>
	 	<div class="modal-footer">
	 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="generateCandidateJobStatuCsv();">Save</button> 
	 		<button class="btn" data-dismiss="modal" aria-hidden="true" >Close</button> 		
	 	</div>
     </div>
 	</div>
</div>

<!-- CSV End -->         
           
    <!-- printing Start  -->
    
  <div style="clear: both;"></div>

   <div class="modal hide"  id="printCandidateJobSatatusDiv"  tabindex="-1" role="dialog" data-keyboard="false" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" >
	<div class="modal-dialog" style="width:1100px; margin-top: 0px;">
	<div class="modal-content">
	<div class="modal-header">
	   	<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="canelPrint();">x</button>
	   	<h3 id="">Print Preview</h3>
	</div>
	
	<div class="modal-body" style="max-height: 500px; overflow-y: auto;"> 		
			<div class="" id="pritCandidateJobSatatusDataTableDiv"  >          
             </div>
 	</div>
 	<div class="modal-footer">
	 	<table border=0 style="margin-left:850px;">
	 		<tr>
		 		<td  nowrap>
		 			<button class="btn  btn-large btn-primary" aria-hidden="true" onclick='printCandidateJobStatusDATA();'>&nbsp;&nbsp;&nbsp;Print&nbsp;&nbsp;&nbsp;</button>&nbsp;&nbsp;
				</td>
				<td  nowrap>
		 			<button class="btn btn-large" data-dismiss="modal" aria-hidden="true" onclick='canelPrint();'>Cancel</button>
				</td>
	 		</tr>
	 	</table>
	</div>
  </div>
 </div>
</div>
    
    
     <div style="clear: both;"></div>

   <div class="modal hide"  id="errDateCheckDiv"  tabindex="-1" role="dialog" data-keyboard="false" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" >
	<div class="modal-dialog" style="width:450px; margin-top: 0px;">
	<div class="modal-content">
	<div class="modal-header">
	   	<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="canelPrint();">x</button>
	   	<h3 id="">Alert</h3>
	</div>
	
	<div class="modal-body" style="max-height: 400px; overflow-y: auto;"> 		
			<div class="" id="errDateMsg">          
             </div>
 	</div>
 	<div class="modal-footer">
	 	<table border=0 align="right">
	 		<tr>
		 		<td  nowrap>
		 			<button class="btn btn-large" data-dismiss="modal" aria-hidden="true" onclick='canelPrint();'>Ok</button>
				</td>
				
	 		</tr>
	 	</table>
	</div>
  </div>
 </div>
</div>
    
    
    <!-- printing End -->       
       <div class="TableContent">  	
             <div class="table-responsive" id="divMainEEC" >    
           </div>       
       </div>
       
       
       <div class="TableContent">  	
             <div class="table-responsive" id="divForCSVReport" >    
           </div>       
       </div>




<script type="text/javascript">
  $('#chartId').tooltip();
  $('#pdfId').tooltip();
  $('#exlId').tooltip();
  $('#csvId').tooltip();
  $('#printId').tooltip();
</script>

<%-- 
<c:if test="${userMaster.entityType eq 1}">
		<script type="text/javascript">
			
			 var today = new Date();
			    var dd = today.getDate();
			    var mm = today.getMonth()+1; //January is 0!
			
			    var yyyy = today.getFullYear();
			    if(dd<10){
			        dd='0'+dd
			    } 
			    if(mm<10){
			        mm='0'+mm
			    } 
			    var today1 = mm+'-'+dd+'-'+yyyy;
			
				//alert("today1 :: "+today1);
			
				document.getElementById("stoDate").value=today1;
			
				today.setDate(today.getDate()-70)
			
				var today2 = new Date(today);
			    var dd = today2.getDate();
			    var mm = today2.getMonth()+1; //January is 0!
			
			    var yyyy = today2.getFullYear();
			    if(dd<10){
			        dd='0'+dd
			    } 
			    if(mm<10){
			        mm='0'+mm
			    } 
			    var today2 = mm+'-'+dd+'-'+yyyy;
			    document.getElementById("sfromDate").value=today2;
			//alert("today2 :: "+today2)
      		
		</script>
	 </c:if>
--%>	
<script type="text/javascript">//<![CDATA[
	  var cal = Calendar.setup({
          onSelect: function(cal) { cal.hide() },
          showTime: true
      });
     cal.manageFields("sfromDate", "sfromDate", "%m-%d-%Y");
     cal.manageFields("stoDate", "stoDate", "%m-%d-%Y");
</script>

 
<script type="text/javascript">
//	chkschoolBydistrict();
//	searchRecordsByEntityTypeEEC();
	displayWeCanExportDataforadmin();
</script>

	        	
	 
