<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type="text/javascript">
var questionTypeJSON=${json};
function findSelected(idd)
{
	for (x in questionTypeJSON)
  	{
	  if(idd==x){
	  	return questionTypeJSON[x];
	  }
  	}
}

</script>

<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" />
<script type="text/javascript" src="dwr/interface/ManageDspqAjax.js?ver=${resourceMap['ManageDspqAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/JobUploadTempAjax.js?ver=${resourceMap['JobUploadTempAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/interface/ReferenceChkQuestionsSetQuesAjax.js?ver=${resourceMap['ReferenceChkQuestionsSetQuesAjax.Ajax']}"></script>
<script type="text/javascript" src="js/managedspq.js?ver=${resourceMap['js/managedspq.js']}"></script>
<script type="text/javascript" src="js/selfServiceCommon.js?ver=${resourceMap['js/selfServiceCommon.js']}"></script>
<style type="text/css">
.tooltip-inner {
    text-align:left;
}
.circleIns
{
	border-radius: 13px; 
    background-color: #E2B194;
    vertical-align: middle;
    text-align: center;
    color: White;    
    width: 19px;
    height: 19px;
    font-weight: bold;
    cursor: pointer;
}
.circleInsWhite
{
	border-radius: 13px; 
    background-color: white;
    vertical-align: middle;
    text-align: center;
    color: White;    
    width: 19px;
    height: 19px;
    font-weight: bold;    
}
.circleInsHead
{
	border-radius: 13px; 
    background-color: #E2B194;
    vertical-align: middle;
    text-align: center;
    color: White;    
    width: 19px;
    height: 19px;
    margin-left:40px;
    font-weight: bold;    
}
.helper
{
	background-color:#1C92F7!important;
	cursor :default;
}
.instructions
{	
	cursor :default;
}
.iconRequired
{
	background-color: red!important;
}
.iconOptional
{
	background-color:pink!important;
}
.iconLabel
{
	background-color:maroon!important;
	cursor :default;
}
.activeIcon
{
	background-color:green!important;
}
.inactiveIcon
{
	background-color:gray!important;
}
.tabwidth
{
width:150px;
height: 35px;
font-size: 13px;
padding: 5px;
padding-left:15px;
vertical-align: top;
font-weight: bold;
}
.nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus
{
color: White;
background-color: rgb(153, 207, 119);
border: 1px solid #dddddd;
border-bottom-color: transparent;
cursor: default;
}
.nav-tabs > li > a:hover
{
background-color: #FFFFFF;
border: 1px solid #FFFFFF;
border-bottom-color: #dddddd;
}
.active, .active:hover {
font-size: 12px;
border-radius: 4px 0 0 4px;
color: #474747!important;
height: 32px;
margin: 0px 0px 0 0px!important;
text-decoration: none;
padding: 0px 0px!important;
}
#teacherMatch li
{
list-style: none;
}
#my-tab-content li
{
list-style: none;
font-size: 12px;
}
.k-multiselect-wrap {
    position: relative;
    border-width: 1px ! important;
    border-style: solid;
    border-radius: 10px;
    border-color: #c5c5c5;
    background-color: #FFF;
    min-height: 2.04em;
}

.pdparrowdiv
{
    width: 20px;
    height: 20px;
    border: 0px;
    padding-top:10px;
    margin-bottom:30px;
    transform: rotate(45deg);
    position:absolute;
    top: 8px;
    right:248px;
    background-color: #f5f5f5;
    box-shadow:  9px 0 0px 0px #f5f5f5,  0 -9px 0px 0px #f5f5f5, -1px 1px 1px #818181;
}
.accordion-heading .accordion-toggle {
color: #007fb2;
display: block;
font-family: 'Century Gothic','Open Sans', sans-serif;
font-size: 17px;
height: 40px;
line-height: 37px;
padding: 0 15px;
}
.btn-dangerevents {
	color: #ffffff;
	text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
	background-color: #da4f49; *
	background-color: #bd362f;
	background-image: -moz-linear-gradient(top, #ee5f5b, #bd362f);
	background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#ee5f5b),
		to(#bd362f) );
	background-image: -webkit-linear-gradient(top, #ee5f5b, #bd362f);
	background-image: -o-linear-gradient(top, #ee5f5b, #bd362f);
	background-image: linear-gradient(to bottom, #ee5f5b, #bd362f);
	background-repeat: repeat-x;
	border-color: #bd362f #bd362f #802420;
	border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
	filter: progid :       DXImageTransform.Microsoft.gradient (     
		 startColorstr =   
		   '#ffee5f5b', endColorstr =       '#ffbd362f', GradientType =      
		0 );
	filter: progid :       DXImageTransform.Microsoft.gradient (      
		enabled = 
		  
		  false );
}

.btn-dangerevents:hover,.btn-dangerevents:focus,.btn-dangerevents:active,.btn-dangerevents.active,.btn-dangerevents.disabled,.btn-danger[disabled]
	{
	color: #ffffff;
	background-color: #bd362f; *
	background-color: #a9302a;
}

.btn-dangerevents:active,.btn-dangerevents.active {
	background-color: #942a25 \9;
}
.btn-previousevents {
	color: #ffffff;
	text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
	background-color: #da4f49; *
	background-color: #bd362f;
	background-image: -moz-linear-gradient(top, #ee5f5b, #bd362f);
	background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#ee5f5b),
		to(#bd362f) );
	background-image: -webkit-linear-gradient(top, #ee5f5b, #bd362f);
	background-image: -o-linear-gradient(top, #ee5f5b, #bd362f);
	background-image: linear-gradient(to bottom, #ee5f5b, #bd362f);
	background-repeat: repeat-x;
	border-color: #bd362f #bd362f #802420;
	border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
	filter: progid :       DXImageTransform.Microsoft.gradient (     
		 startColorstr =   
		   '#ffee5f5b', endColorstr =       '#ffbd362f', GradientType =      
		0 );
	filter: progid :       DXImageTransform.Microsoft.gradient (      
		enabled = 
		  
		  false );
}

.btn-previousevents:hover,.btn-previousevents:focus,.btn-previousevents:active,.btn-previousevents.active,.btn-previousevents.disabled,.btn-danger[disabled]
	{
	color: #ffffff;
	background-color: #bd362f; *
	background-color: #a9302a;
}

.btn-previousevents:active,.btn-previousevents.active {
	background-color: #942a25 \9;
}

.btn-warningevents {
	color: #ffffff;
	text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
	background-color: #faa732; *
	background-color: #f89406;
	background-image: -moz-linear-gradient(top, #fbb450, #f89406);
	background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#fbb450),
		to(#f89406) );
	background-image: -webkit-linear-gradient(top, #fbb450, #f89406);
	background-image: -o-linear-gradient(top, #fbb450, #f89406);
	background-image: linear-gradient(to bottom, #fbb450, #f89406);
	background-repeat: repeat-x;
	border-color: #f89406 #f89406 #ad6704;
	border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
	filter: progid :       DXImageTransform.Microsoft.gradient (     
		 startColorstr =   
		   '#fffbb450', endColorstr =       '#fff89406', GradientType =      
		0 );
	filter: progid :       DXImageTransform.Microsoft.gradient (      
		enabled = 
		  
		  false );
}

.btn-warningevents:hover,.btn-warningevents:focus,.btn-warningevents:active,.btn-warningevents.active,.btn-warningevents.disabled,.btn-warningevents [disabled]
	{
	color: #ffffff;
	background-color: #f89406;
	background-color: #df8505;
}

.btn-warningevents:active,.btn-warningevents.active {
	background-color: #c67605 \9;
}

strong {
	font-weight: normal;
}
.divshadow
{
box-shadow: 0px 1px 1px 1px #999;
border-radius: 3px;
}
.marginpadding
{
   padding-left: 0px;
    padding-right: 0px;
}
.panel-group .panel {
  margin-bottom: -4px;
  border-radius: 4px;
  overflow:visible;
    padding-left: 15px; 
  padding-right: 15px;
}
.k-button, .k-calendar .k-header .k-link, .k-calendar .k-footer {
   text-transform:inherit !important;
   font-family: 'Century Gothic','Open Sans', sans-serif !important;
}
.grid{
height:200px !important;
}
.flatbtn
{
    width: 30%;
    height: 35px;
    padding: 0px 1px 2px 1px;
    font-size: 14px;
    color: white;
    border-radius: 3px;
    text-align: center;
    /*background: #99cf77;*/
    border: 0;
    cursor: pointer;
    font-family: 'Century Gothic','Open Sans', sans-serif;
}

.arrow_box:after, .arrow_box:before {
	bottom: 100%;
	left: 80%;
	border: solid transparent;
	content: " ";
	height: 0;
	width: 0;
	position: absolute;	
}

.arrow_box:after {
	border-color: rgba(136, 183, 213, 0);
	border-bottom-color: white;
	border-width: 10px;
	margin-left: -10px;
}
.arrow_box:before {
	border-color: rgba(194, 225, 245, 0);
	border-bottom-color: #DDDFE0;
	border-width: 12px;
	margin-left: -12px;
}
.moreSelectedColor {
	color:#007AB4;
}
.blue
{
color:#007AB4; text-align:center;  cursor: pointer;
}
.green
{
color:green; text-align:center;  cursor: pointer;
}
.gray
{
color:gray; text-align:center;  cursor: pointer;
}

.editLeftPadding1
{
padding-left: 10px;
}
.editLeftPadding2
{
padding-left: 4px;
}
.externalLeftPadding1
{
 padding-left: 11px;
}
.externalLeftPadding2
{
padding-left: 7px;
}
.externalLeftPadding3
{
padding-left: 2px;
}
.degreeLabel
{
    min-height: 20px;
    margin-top: 10px;
    margin-bottom: 10px;
    padding-left: 20px;    
}
.enableArrow
{
	color:#333333;
 	float:right;
  	cursor:pointer;
}
.disableArrow
{
	color:#94949E;
 	float:right;  	
}
</style>
<input type="hidden" id="dspqGlobalLster" name="dspqGlobalLster" value="0"/>
<input type="hidden" id="sectionStart" value=""  />
<input type="hidden" id="sectionEnds" value="" />
<input type="hidden" id="groupId" name="groupId" value="" />
<input type="hidden" id="questionId" value="" />
<input type="hidden" id="cloneID" value="" />
<input type="hidden" id="PortfolioNameIDS" value="" />
<input type="hidden" id="sectionIdOnMoreField" name="sectionIdOnMoreField" value="" />

<input type="hidden" id="dspqId" />
<input type="hidden" id="cloneEditFlag" />
<input type="hidden" id="editDistrictId" />
<input type="hidden" id="editDistrictName" />
<input type="hidden" id="ADdspqPortfolioNameId" />
<input type="hidden" id="ADstatus" />
<input type="hidden" id="showPromptType" />
<input type="hidden" id="tabId" />

<div class="row divshadow" style="width:99.6%;margin-left:3px;">
<div class="col-sm-12 col-md-12" style="box-shadow: 0px 0px 0px 0px #999;background-color: #f7f7f7;" >
<div id="loadingDiv"> <!-- style="display:none; z-index: 5000;"  -->
  	<table  align="left" >
 		<tr><td style="padding-top:270px;padding-left:600px;" align="center"><img src="images/please.jpg"/></td></tr>
 		<tr><td style="padding-top:0px;padding-left:600px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
 		<tr id='paymentMessage'><td style='padding-top:0px;padding-left:450px;' id='spnMpro' align='center'></td></tr>
	</table>
</div>
<!--<div class="row" style="width:100%;margin-left:0px;">
					<div class="col-sm-10 col-md-10" >			         
			        </div> 
			         <div class="col-sm-2 col-md-2" style="text-align: right;">
			         <span href="javascript:void(0);"  data-toggle="tooltip" data-placement="left" onclick="showVideo()" title="Show Me" style="cursor: pointer; padding-top:5px;"><i class="fa fa-television fa-lg" style="color: #007fb2;"></i>								
						</span>	
			         	<span href="javascript:void(0);"  data-toggle="tooltip" data-placement="left" title="Chat with an Expert" style="cursor: pointer; padding-top:5px;"><i class="fa fa-weixin fa-lg" style="color: green"></i> </span>
			         </div>   
 </div> 
   --><div class="row">
      <div class="col-sm-1 col-md-1" style="background-color: rgb(241, 218, 218);height: 35px;">
         	<img src="images/Recruitingdspq1.png" alt="" style="margin-top: 7px;margin-left: 5px;">
         </div>        
         <div class="col-sm-8 col-md-8" style="padding-left: 0px;">
         	<div class="subheading" style="font-size: 13px;">Candidate Portfolio</div>	
         </div>
       	 <div id="AddCandidate" class="col-sm-3 col-md-3" style="line-height: 34px;padding-left: 32px;">
					<a href="javascript:void(0);" onclick="return showPortfolio();">+ Add Candidate Portfolio						
					</a>	
						<span href="javascript:void(0);"  data-toggle="tooltip" data-placement="left" style="cursor: pointer" title="Add Candidate Portfolio">
						<img src="images/qua-icon.png" alt="">					
						</span>
		</div>
		<div id="EditCandidate" class="col-sm-3 col-md-3 hide" style="line-height: 34px;padding-left: 80px;">
					<a href="javascript:void(0);" onclick="cancelPortfolioForm();"">Back to Portfolio						
					 </a>
					 <span href="javascript:void(0);"  data-toggle="tooltip" data-placement="left" style="cursor: pointer" title="Back to Portfolio">
						<img src="images/qua-icon.png" alt="">					
						</span>
		</div> 
     </div>
    
     <input type="hidden" id="checkPopup" value="0" />
     <input type="hidden" id="checkClickOnBackPort" value="0" />
     <input type="hidden" id="renameEnterFlag" value="0" checked="checked" />
     <input type="hidden" id="portfolioID" value="0" />
     <input type="hidden" id="districtIdPage" value="" />
	</div>
	 <input type="hidden" id="entityTypeID" value="${entityType}" />
	<c:if test="${entityType==2}">
		<input type="hidden" id="districtSearchId" value="" />
		<input type="hidden" id="createPortfolio" value="2" />
	 </c:if>	
	
	<div class="col-sm-12 col-md-12 top10" >
	<div class="row">
		<div class="col-sm-12 col-md-12">
			<div class='divErrorMsg_1' id='errordiv4DSPQ' style="padding-left: 2px; padding-top: 10px;"></div>
		</div>
	</div>
	<div class="row" id="dataGrid">
		<div class="col-sm-12 col-md-12 marginpadding" >
		<div id="dspqGrid"></div>
		</div>
	</div>
	
	<div class="row hide" id="showPortfolioDiv"> 
	
	<c:if test="${entityType==1}">
	<div class="row" id="createPortfolioHideShow">
	 <div class="col-sm-4 col-md-4" style="padding-right:0px;">					  
		<label class="radio">           
          Create Portfolio for<span class="required">*</span> <span href="javascript:void(0);"  data-toggle="tooltip" data-placement="left" title="District Name"><img src="images/qua-icon.png" alt=""></span>
         </label>
       </div>
        <div class="col-sm-4 col-md-4">	
            <select class="form-control" onchange="displayOrHideDistrict();" id="createPortfolio" name="createPortfolio">
				<option value="1">District</option>
				<option value="2">TM Default</option>
			</select>
         
       </div>
	</div>
		<div class="row" id="districtHideShow">		
	  <div class="col-sm-4 col-md-4" style="padding-right:0px;">					  
		<label class="radio">           
            District Name<span class="required">*</span> <span href="javascript:void(0);"  data-toggle="tooltip" data-placement="left" title="District Name"><img src="images/qua-icon.png" alt=""></span>
         </label>
       </div>
       <div class="col-sm-6 col-md-6" style=""> 	              
	        
		        <span>
		        	<input style="width: 111%; color:#555555; font-size: 14px;"  type="text" id="districtName"  value="${DistrictName}" maxlength="100"  name="districtName" class="help-inline form-control"
		          	onfocus="getDistrictAuto(this, event, 'divTxtShowData', 'districtName','districtId','');"
					onkeyup="getDistrictAuto(this,event,'divTxtShowData', 'districtName','districtId','');"
					onblur="hideDistrictMasterDiv(this,'districtId','divTxtShowData');"	/>
		      	</span>
		      	<input type="hidden" id="districtId" value="" />
		        <div id='divTxtShowData'  onmouseover="mouseOverChk('divTxtShowData','districtName')" style=' display:none;position:absolute;z-index:5000;' class='result' ></div>
      	  
		</div>
       </div>
        </c:if>
        <c:if test="${entityType==2}">
      	   	<input type="hidden" id="districtId" name="districtId" value="${DistrictId}" />
      	   	<input type="hidden" id="districtName" name="districtName" maxlength="100"  value="${DistrictName}" readonly="readonly" class="form-control" style="width: 111%;"  />	
      	   </c:if>	
	<div class="row">  
	   <div class="col-sm-4 col-md-4">
	  	 <label class="radio">  	
	      Candidate Portfolio Name<span class="required">*</span> <span href="javascript:void(0);"  data-toggle="tooltip" data-placement="left" title="Candidate Portfolio Name"><img src="images/qua-icon.png" alt=""></span>
	        </label>
		</div>
		<div class="col-sm-6 col-md-6 top5">  	
      	   	<input type="text" id="portfolioName" name="portfolioName" class="form-control" style="width: 111%;"/>	      	
		</div>
	</div>
	
	
	    <div id="continueDiv" class="row left5" style='padding-bottom: 10px;'>
	    <div class="col-sm-6 col-md-6"> </div>		
	     <div class="col-sm-6 col-md-6 top5" style="text-align: right;padding-right: 42px;">				
			<button onclick="savePortfolioName();" class="flatbtn" style="background: #9fcf68;width:40%" id="save" title="Continue">
			  <strong>Continue <i class="fa fa-check-circle"></i></strong>
		    </button>
		 </div>
	    </div>
	</div>
	</div>
	
<div class="col-sm-12 col-md-12 hide" id="showPortfolioDiv1" > 
	<div class="row mt10">	
	<div class="col-sm-12 col-md-12 marginpadding" style="background-color: (rgb(153, 207, 119);">
	<input type="hidden" id="nextTab" value="" />
	<ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
	 <c:forEach var="avlst" items="${dspqGroupMasters}" varStatus="status">
	  <input type="hidden"  id="sectionid" value="1"/>
	 <c:set var="activePanel" value=""></c:set>
	 <c:if test="${avlst.groupId eq 1}">
	 	<c:set var="activePanel" value="active"></c:set>
	 	<c:set var="imagename" value="images/Personaldspq1.png"></c:set>
	</c:if>
	 <c:if test="${avlst.groupId eq 2}">
	 	<c:set var="imagename" value="images/Academicsdspq1.png"></c:set>
	</c:if>
	<c:if test="${avlst.groupId eq 3}">
	 	<c:set var="imagename" value="images/Credentialsdspq1.png"></c:set>
	</c:if>
	<c:if test="${avlst.groupId eq 4}">
	 	<c:set var="imagename" value="images/Experiencedpsq1.png"></c:set>
	</c:if>
	<!-- callDiv('${avlst.groupId}',0,0); -->
		<li class="${activePanel} col-md-3 marginpadding" id="litab${avlst.groupId}"><a title="${avlst.groupName}" class="tabwidth" onclick="alertOnClickTab(${avlst.groupId});" style="width:88%; cursor:pointer; padding-left: 18px;"><img src="${imagename}" id="changebk${avlst.groupId}" style="vertical-align: text-bottom;"/>&nbsp;${avlst.groupName}</a></li>
	</c:forEach>       
    </ul>
    </div>	
   
	<div class="col-sm-12 col-md-12">
	
	
	<div class="row left10">
		<div id="fieldgrid"></div>
	</div>
	
	<div class="row left10">
		<div id="my-tab-content" class="tab-content"></div>
	</div>
	
	<input type="hidden" id="editDspqID" value="" />
	
	
	
	
	<div class="row">
		<div class="col-sm-12 col-md-12 top15" style="margin-bottom: 20px; padding-left: 11px;">
			<div class="row">
				<div class="col-sm-6 col-md-6">
				<button onclick="savePortfolio(2);" class="flatbtn" style="background: #9fcf68;" id="back"><strong><i class="backicon"></i> Previous </strong></button> 
				&nbsp;&nbsp;
				<button onclick="savePortfolio(0);" class="flatbtn" style="background: #F89507;width:32%" id="save">
			           <strong>Save and Exit <i class="fa fa-check-circle"></i></strong>
		            </button>
					&nbsp;&nbsp;
				<button onclick="cancelPortfolioForm();" class="flatbtn" style="background: #da4f49;" id="cancel">
          			<strong>Cancel <i class="fa fa-times-circle"></i></strong>
          			</button>
					&nbsp;&nbsp;
					
				</div>	
				<div class="col-sm-6 col-md-6" style="text-align: right;padding-right: 28px;">				
					<button onclick="savePortfolio(1);" class="flatbtn" style="background: #9fcf68; width: 45%;" id="Next">
		               <strong>Save and Continue <i class="icon"></i></strong>
            	  </button>	
				</div>
				</div>
			</div>
		</div>
	
	<div class="row hide left16" style="padding-top: 25px;width:698px;">
		<div id="my-tab-content" class="tab-content">
			<div class="row" style="padding:5px 0px 0px 0px;background-color:#007fb2; font-weight: bold; margin:0px 0px 0px 0px; color:white;">
				<div class="col-sm-8 col-md-8" style="padding-left:10px; padding-bottom:10px;">
					District Custom Fields
				</div>
				<!--<div class="col-sm-4 col-md-4" style="padding-left:100px;padding-bottom:5px;">
					<button type="submit" class="btn btn-primary btn-primary" onclick="openDivCustomField();"><strong>Customize <i class="icon"></i></strong></button>
				</div>
			--></div>
			</div>
	</div>
	
	<!-- Question Section Starts -->
	<div id="addDspqQuesListDiv" style="width:700px;">
		<div class="row hide left10">
			<div class="pull-right add-employment1">
				<a href="javascript:void(0);" onclick="return openDivCustomField()">+Add Question</a>
			</div>
		</div>
		<div class="row left6 hide">
		    <div class="TableContent left10">        	
	         	<div class="table-responsive" id="dspqQuestionList"></div>            
		    </div>
	    </div>
	    
	</div>
	
	
	

	
		</div>
</div>
</div>
</div>
<div style="background: rgba(153, 153, 153, 0.8); " class="modal hide" id="myModalMsg1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog"
		style='padding-top: 10%; margin-left: 235px; width: 832px;'>
		<div class="modal-content">
			<div class="modal-header row" style="padding-left:0px;padding-right:0px;margin-left:0px;margin-right:0px;">
			<div class="col-sm-11 col-md-11">
			<h3 id="myModalLabel">
					<span id="fname"></span> Field Customization
				</h3>
				</div>
				<div  class="col-sm-1 col-md-1" style="text-align:right;">
				<a href="javascript:void(0);" onclick="hidepopup();" title="Close" style="color: white;"><b style="font-size: 15px;">X</b></a>
				</div>
			</div>

			<div class="modal-body" style="padding-top:0px;padding-left: 25px;padding-right: 25px;">
				<div class='row top10' style='margin-right: 5px;'>
					<div class='col-sm-2 col-md-2'
						style='height: 42px; border: 1px rgb(213, 213, 213) solid; border-left: 0px;padding-left:0px;'>
						<label style='margin-top: 10px; font-weight: bold;'>
							Candidate Type
							</label>
					</div>
                 <input type="hidden" id="fieldid"/>
					<div class='col-sm-10 col-md-10' style='padding-right: 0px;'>
						<div class='row'
							style='border: 1px rgb(213, 213, 213) solid; border-left: 0px; border-right: 0px;'>
							<div class='col-sm-4 col-md-4 checkbox' style="padding-left: 30px;">
							<label class="checkbox inline" style="margin-top: 2px;margin-bottom: 2px;margin-left: -25px;">
								<!--<input type='checkbox' id='sectionExternal1' checked disablednclick='hideOrShowApplicantType();'>
									External (E)
								-->
								External (E)</label>
							</div>
							<div class='col-sm-4 col-md-4 checkbox' style='margin-top: 10px;'>
							<label class="checkbox inline" style="margin-top: 2px;margin-bottom: 2px;margin-left: -25px;">
								<!--<input type="checkbox" id="sectionInternal1" onclick="hideOrShowApplicantType();"> 
									Internal (I) 
								-->Internal (I)</label>
							</div>
							<div class='col-sm-4 col-md-4 checkbox' style='margin-top: 10px;'>
							<label class="checkbox inline" style="margin-top: 2px;margin-bottom: 2px;margin-left: -25px;">
								<!--<input type='checkbox' id='sectionITransfer1'	onclick='hideOrShowApplicantType();'>
									Internal Transfer (IT)
								-->Internal Transfer (IT)</label>
							</div>
						</div>
					</div>
					</div>
					<div class='row top10' >
						<div class='col-sm-12 col-md-12'>
							<div class='divErrorMsg_1' id='errordivfield' style="padding-left: 2px; color:red; padding-top: 10px;"></div>
						</div>
					</div>


					<div class='row top10' style='background-color: #007fb2; font-weight: bold; padding-left: 10px; padding-top: 5px; color: white;  padding-left: 0px;'><!--
                       <div class='col-sm-1 col-md-1'>
                       <label class="top7"></label>
						</div>
						--><div class='col-sm-6 col-md-6' style="padding-left: 35px;">
							Field Name
						</div>
						<div class='col-sm-1 col-md-1' style="padding-left: 0px;">
							Required				
						</div>
						<div class='col-sm-1 col-md-1'>
							Active
						</div>
					<div class='col-sm-4 col-md-4'>
							Helper Text for Candidate
						</div>

					</div>
					
					<div class="row top10">
					<!--<div class='col-sm-1 col-md-1' style="padding-left: 0px;" title="External">
					<label class="top7">E</label>
						</div>
						--><div class="col-sm-6 col-md-6 ">
							<label class="top7" style="width: 5%; float:left;">E</label> <input class="form-control" type="text" id="fieldnameE" placeholder="Field Name" style="width:95%"/>
						</div>
						<div class="col-sm-1 col-md-1  top7">
							<!--<i id="mand1" class="fa fa-check-square fa-lg blue" title="Optional" onclick="chengeMandOptions(1)"></i>
							--><input  type="checkbox" id="mandetoryE" onclick="chengeMandOptions(1)">
						</div>
						<div class="col-sm-1 col-md-1  top7">							
							<input  type="checkbox" id="activeE" onclick="activeFieldOptions(1)">
						</div>
						<div class="col-sm-4 col-md-4 " style="padding-right: 0px;">
							<input class="form-control" type="text" id="tooltipE" placeholder="Helper Text" style="width:100%"/>
						</div>
					</div>
					
					<div class="row top10 hide" id="internalType">
					<!--<div class='col-sm-1 col-md-1' style="padding-left: 0px;" title="Internal">
					<label class="top7">I</label>
						</div>
						--><div class="col-sm-6 col-md-6 ">
							<label class="top7" style="width: 5%; float:left;">I</label> <input class="form-control" type="text" id="fieldnameI" placeholder="Field Name" style="width:95%"/>
						</div>
						<div class="col-sm-1 col-md-1  top7">
						<!--<i id="mand2" class="fa fa-check-square fa-lg blue" title="Optional" onclick="chengeMandOptions(2)"></i>
							--><input  type="checkbox" id="mandetoryI" onclick="chengeMandOptions(2)">
						</div>
						<div class="col-sm-1 col-md-1  top7">
							<input  type="checkbox" id="activeI" onclick="activeFieldOptions(2)">
						</div>
						<div class="col-sm-4 col-md-4" style="padding-right: 0px;">
							<input class="form-control" type="text" id="tooltipI" placeholder="Helper Text" style="width:100%"/>
						</div>
					</div>
					
					<div class="row top10 hide" id="internalTransferType">
					<!--<div class='col-sm-1 col-md-1' style="padding-left: 0px;" title="Internal Transfer">
					<label class="top7">IT</label>
						</div>
						--><div class="col-sm-6 col-md-6 ">
							<label class="top7" style="width: 5%; float:left;">IT</label> <input class="form-control" type="text" id="fieldnameIT" placeholder="Field Name" style="width:95%"/>
						</div>
						<div class="col-sm-1 col-md-1  top7">
							<!--<i id="mand3" class="fa fa-check-square fa-lg blue" title="Optional" ></i>
							--><input  type="checkbox" id="mandetoryIT" onclick="chengeMandOptions(3)">
						</div>
						<div class="col-sm-1 col-md-1  top7">
							<input  type="checkbox" id="activeIT" onclick="activeFieldOptions(3)" >
						</div>
						<div class="col-sm-4 col-md-4" style="padding-right: 0px;">
							<input class="form-control" type="text" id="tooltipIT" placeholder="Helper Text" style="width:100%"/>
						</div>
					</div>
					
					<div class="row mt15">
						<div class="col-sm-3 col-md-3 marginpadding" style="text-align:left;">
							<button onclick="saveRecord();" class="flatbtn" style="background: #99cf77; width: 45%;" >
								<strong>Save <i class="icon"></i>
								</strong>
							</button>
							&nbsp;
							<a href="javascript:void(0);" onclick="hidepopup();">Cancel</a>
						</div>
					</div>
				</div>
			
			</div>
		</div>
	</div>
	<div style="background: rgba(153, 153, 153, 0.8); " class="modal hide" id="sectionModalMsg1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog"
		style='padding-top: 4%; margin-left: 235px; width: 832px;'>
		<div class="modal-content">
			<div class="modal-header row" style="padding-left:0px;padding-right:0px;margin-left:0px;margin-right:0px;">
			<div class="col-sm-11 col-md-11">
			<h3 id="myModalLabel">
					<span id="sectionNameHeading"></span> Subsection Customization
				</h3>
				</div>
				<div  class="col-sm-1 col-md-1" style="text-align:right;">
				<a href="javascript:void(0);" onclick="hideSectionPopup();" title="Close" style="color: white;"><b style="font-size: 15px;">X</b></a>
				</div>
			</div>

			<div class="modal-body" style="padding-top:0px;padding-left: 25px;padding-right: 25px;">
				<div class='row top10' style='margin-right: 5px;'>
					<div class='col-sm-2 col-md-2'
						style='height: 42px; border: 1px rgb(213, 213, 213) solid; border-left: 0px;padding-left:0px;'>
						<label style='margin-top: 10px; font-weight: bold;'>
							Candidate Type
							</label>
					</div>
                 <input type="hidden" id="sectionfieldid"/>
					<div class='col-sm-10 col-md-10' style='padding-right: 0px;'>
						<div class='row'
							style='border: 1px rgb(213, 213, 213) solid; border-left: 0px; border-right: 0px;'>
							<div class='col-sm-4 col-md-4 checkbox' style="padding-left: 30px;">
							<label class="checkbox inline" style="margin-top: 2px;margin-bottom: 2px;margin-left: -25px;">
								<!--<input type='checkbox' id='sectionExternal11' checked disabled	onclick='hideOrShowSectionApplicantType();'>
									
								-->External (E)</label>
							</div>
							<div class='col-sm-4 col-md-4 checkbox' style='margin-top: 10px;'>
							<label class="checkbox inline" style="margin-top: 2px;margin-bottom: 2px;margin-left: -25px;">
								<!--<input type="checkbox" id="sectionInternal11" onclick="hideOrShowSectionApplicantType();"> 
									 
								-->Internal (I)</label>
							</div>
							<div class='col-sm-4 col-md-4 checkbox' style='margin-top: 10px;'>
							<label class="checkbox inline" style="margin-top: 2px;margin-bottom: 2px;margin-left: -25px;">
								<!--<input type='checkbox' id='sectionITransfer11'	onclick='hideOrShowSectionApplicantType();'>									
								-->Internal Transfer (IT)</label>
							</div>
						</div>
					</div>
					</div>
					<div class='row top10' >
						<div class='col-sm-12 col-md-12'>
							<div class='divErrorMsg_1' id='errordivSection' style="padding-left: 2px; color:red; padding-top: 10px;"></div>
						</div>
					</div>
					<div class="row top10" id="externalTypeSectionIns">					
						<div class="col-sm-2 col-md-2  top7">							
						Instructions for E
						</div>
						<div class="col-sm-10 col-md-10 ">
							<textarea  class="form-control" name="instruction_E" id="instruction_E" maxlength="2500" rows="2"></textarea>
						</div>											
						
					</div>
					<div class="row top10 hide" id="internalTypeSectionIns">					
						<div class="col-sm-2 col-md-2  top7">							
						Instructions for I
						</div>
						<div class="col-sm-10 col-md-10 ">
							<textarea  class="form-control" name="instruction_I" id="instruction_I" maxlength="2500" rows="2"></textarea>
						</div>											
						
					</div>
					<div class="row top10 hide" id="internalTransferTypeSectionIns">					
						<div class="col-sm-2 col-md-2  top7">							
						Instructions for IT
						</div>
						<div class="col-sm-10 col-md-10 ">
							<textarea  class="form-control" name="instruction_IT" id="instruction_IT" maxlength="2500" rows="2"></textarea>
						</div>											
						
					</div>

					<div class='row top10' style='background-color: #007fb2; font-weight: bold; padding-left: 10px; padding-top: 5px; color: white;  padding-left: 0px;'><!--
                       <div class='col-sm-1 col-md-1'>
                       <label class="top7"></label>
						</div>
						-->
						<div class='col-sm-1 col-md-1'>
							Candidate
						</div>
						<div class='col-sm-6 col-md-6' style="padding-left: 35px;">
							Section Name
						</div>
						<div class='col-sm-1 col-md-1' style="padding-left: 0px;">
							Required
						</div>						
					<div class='col-sm-4 col-md-4'>
							Helper Text for Candidate
						</div>

					</div>
					
					<div class="row top10" id="externalTypeSection">
					<!--<div class='col-sm-1 col-md-1' style="padding-left: 0px;" title="External">
					<label class="top7">E</label>
						</div>
						-->
						<div class="col-sm-1 col-md-1  top7">
							<!--<input  type="checkbox" id="sectionActiveE">
						-->
						E
						</div>
						<div class="col-sm-6 col-md-6 ">
							<input class="form-control" type="text" id="sectionNameE" placeholder="Section Name"/>
						</div>
						<div class="col-sm-1 col-md-1  top7">
						<!--<i id="sectionMand1" class="fa fa-check-square fa-lg blue" title="Optional" onclick="chengeSectiondMandOptions(1)"></i>
							--><input  type="checkbox" id="sectionMandetoryE" onclick="chengeSectiondMandOptions(1)">
						</div>
						
						<div class="col-sm-4 col-md-4 " style="padding-right: 0px;">
							<input class="form-control" type="text" id="sectionTooltipE" placeholder="Helper Text" style="width:100%"/>
						</div>
					</div>
					
					<div class="row top10 hide" id="internalTypeSection">
					<!--<div class='col-sm-1 col-md-1' style="padding-left: 0px;" title="Internal">
					<label class="top7">I</label>
						</div>
						-->
						<div class="col-sm-1 col-md-1  top7">
							<!--<input  type="checkbox" id="sectionActiveI">
						-->
						I
						</div>
						<div class="col-sm-6 col-md-6 ">
							<input class="form-control" type="text" id="sectionNameI" placeholder="Section Name" />
						</div>
						<div class="col-sm-1 col-md-1  top7">
						<!--<i id="sectionMand2" class="fa fa-check-square fa-lg blue" title="Optional" onclick="chengeSectiondMandOptions(2)"></i>
							--><input  type="checkbox" id="sectionMandetoryI" onclick="chengeSectiondMandOptions(2)">
						</div>
						
						<div class="col-sm-4 col-md-4" style="padding-right: 0px;">
							<input class="form-control" type="text" id="sectionTooltipI" placeholder="Helper Text" style="width:100%"/>
						</div>
					</div>
					
					<div class="row top10 hide" id="internalTransferTypeSection">
					<!--<div class='col-sm-1 col-md-1' style="padding-left: 0px;" title="Internal Transfer">
					<label class="top7">IT</label>
						</div>
						-->
						<div class="col-sm-1 col-md-1  top7">
							<!--<input  type="checkbox" id="sectionActiveIT">
						-->
						IT
						</div>
						<div class="col-sm-6 col-md-6 ">
							<input class="form-control" type="text" id="sectionNameIT" placeholder="Section Name"/>
						</div>
						<div class="col-sm-1 col-md-1  top7">
						<!--<i id="sectionMand3" class="fa fa-check-square fa-lg blue" title="Optional" onclick="chengeSectiondMandOptions(3)"></i>
							--><input  type="checkbox" id="sectionMandetoryIT" onclick="chengeSectiondMandOptions(3)">
						</div>						
						<div class="col-sm-4 col-md-4" style="padding-right: 0px;">
							<input class="form-control" type="text" id="sectionTooltipIT" placeholder="Helper Text" style="width:100%"/>
						</div>
					</div>
					
					
					
					
					
					<div class="row mt15">
						<div class="col-sm-3 col-md-3 marginpadding" style="text-align:left;">
							<button onclick="saveSectionRecord();" class="flatbtn" style="background: #99cf77; width: 45%;" >
								<strong>Save <i class="icon"></i>
								</strong>
							</button>
							&nbsp;
							<a href="javascript:void(0);" onclick="hideSectionPopup();">Cancel</a>
						</div>
					</div>
				</div>
			
			</div>
		</div>
	</div>
<style>

.uparrowdiv{
min-height:40px; /*min height of DIV should be set to at least 2x the width of the arrow*/
background: white;
position:absolute;
word-wrap:break-word;
-moz-border-radius:5px; /*add some nice CSS3 round corners*/
-webkit-border-radius:5px;
border-radius:5px;
margin-bottom:2em;
border-color: gray;
border-style: solid;
border-width: 1px;
z-index: 10;
}

.uparrowdiv:after{ /*arrow added to uparrowdiv DIV*/
content:'';
display:block;
position:absolute;
top:-20px; /*should be set to -border-width x 2 */
left:70px;
width:0;
height:0;
border-color: transparent transparent white transparent; /*border color should be same as div div background color*/
border-style: solid;
border-width: 10px;

}
.dspq-dialog {
    left: 50%;
    right: auto;
    width: 810px;
    padding-top: 0px;
    padding-left:10px;
    padding-right:10px; 
    padding-bottom: 30px;
    margin-left: auto;
      margin-right: auto;
      z-index: 1050;
  }
</style>

<script type="text/javascript">
	displayPortfolioMaster();
	if($("#showSessionStatus").val()!=undefined){
	if($("#entityTypeID").val()!=2){
		setEntityType();
	}
	}	
</script>

    <!--<style>html { font-size: 12px; font-family: Arial, Helvetica, sans-serif; }</style>
 
     <link href="css/kendo.common-material.min.css" rel="stylesheet"/> 
  	 <link href="css/kendo.material.min.css" rel="stylesheet"/>
 	 <script src="js/kendo.all.min.js"></script>
  <script>
     $(document).ready(function() {
         // create Editor from textarea HTML element with default set of tools
         $("#kendoEditorInstruction").kendoEditor({ resizable: {
             content: false,
             toolbar: true
         }});
         $("#kendoQuestionExplanation").kendoEditor({ resizable: {
             content: false,
             toolbar: true
         }});
     });
  </script>
  
    
    --><div  class="modal hide "  id="alertOnChangeTab"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="table-responsive">
		<div class='dspq-dialog' style="width:40%">
			<div class='modal-content'>
			<div class="modal-header">
		  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
				<h3><span  style="margin-left: 0px;">Confirmation</span></h3>				
			</div>
			<div class="modal-body">
						<div class="row">
							<div class="col-sm-12 col-md-12" id="dspqGroupMastersId"><label><strong>
							You have unsaved changes in the <span id="sectionLblName"></span> section. Would you like to Save changes or Discard them?
							<!--You may have some unsaved information on previous tab. Please click "Ok"
							button if you want to save that information and switch to new tab or click on "Cancel"
						    button if you want to switch to new tab without saving that information.-->
						    </strong></label>								
							</div>
							
						</div>
		 	<div style="text-align: right;">
		 		<button  onclick="tabChange()" class="flatbtn" style="width: 15%;" ><strong>Save <i class="icon"></i></strong></button>&nbsp;
		 		<button  onclick="cancelChange();" class="flatbtn" style="width: 17%; background: #da4f49;" ><strong>Discard <i class="fa fa-times-circle"></i></strong></button>
		 	</div>
		  </div>
		 </div>
	</div>
</div>
</div>



  <div  class="modal hide "  id="alertOnDuplicate"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="table-responsive">
		<div class='dspq-dialog' style="width:40%">
			<div class='modal-content'>
			<div class="modal-header">
		  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
				<h3><span  style="margin-left: 0px;">Confirmation</span></h3>				
			</div>
			<div class="modal-body">
						<div class="row">
							<div class="col-sm-12 col-md-12" id="duplicatePort">								
							</div>							
						</div>
		 	<div style="text-align: right;">
		 		<button  class="flatbtn" style="width: 15%;" onclick="cancelDuplicatePort();"><strong>Ok <i class="icon"></i></strong></button>&nbsp;
		 		<!--<a href="javascript:void(0);"  onclick="cancelDuplicatePort();">Cancel</a>		
		 	--></div>
		  </div>
		 </div>
	</div>
</div>
</div> 
  <div  class="modal hide "  id="alertOnError"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="table-responsive">
		<div class='dspq-dialog' style="width:40%">
			<div class='modal-content'>
			<div class="modal-header">
		  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
				<h3><span  style="margin-left: 0px;">Confirmation</span></h3>				
			</div>
			<div class="modal-body">
						<div class="row">							
							<div class="col-sm-12 col-md-12" id="errorValid">
																
							</div>
						</div>
		 	<div style="text-align: right;">
		 		<button class="flatbtn" style="width: 15%;"  onclick="hideErrorButton();"><strong>Ok <i class="icon"></i></strong></button>&nbsp;
		 			
		 	</div>
		  </div>
		 </div>
	</div>
</div>
</div>
    
   <div  class="modal hide "  id="showVideo"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="table-responsive">
		<div class='dspq-dialog' style="width:40%;padding-top: 7%;">
			<div class='modal-content'>
			<div class="modal-header">
		  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
				<h3><span  style="margin-left: 0px;">Video</span></h3>				
			</div>
			<div class="modal-body">
						<div class="row">							
							<div class="col-sm-12 col-md-12" id="manageVideo">
								<!--<iframe src="http://www.youtube.com/embed/W7qWa52k-nE" width="485" height="315" frameborder="0" allowfullscreen></iframe>
							--></div>
						</div>
		 	<div style="text-align: right;">
		 		<button class="flatbtn top10" style="width: 15%;"  onclick="hideVideo();"><strong>Ok <i class="icon"></i></strong></button>&nbsp;
		 			
		 	</div>
		  </div>
		 </div>
	</div>
</div>
</div> 
 <input type="hidden" id="defaultPortfolioId" value="" />
 <div  class="modal hide "  id="defaultPortfolio"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" >
	<div class="table-responsive">
		<div class='dspq-dialog' style="width:40%;">
			<div class='modal-content'>
			<div class="modal-header">
		  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
				<h3><span  style="margin-left: 0px;">TeacherMatch</span></h3>				
			</div>
			<div class="modal-body">
						<div class="row">
							<div class="col-sm-12 col-md-12" id="dspqGroupMastersId">
								<label>
									<strong>Do you want to Activate TM default portfolio?</strong>
								</label>
							</div>
							
						</div>
		 	<div style="text-align: right;">
		 		<button  onclick="createDefaultPortfolio()" class="flatbtn" style="width: 15%;" ><strong>Ok <i class="icon"></i></strong></button>&nbsp;
		 		<a href="javascript:void(0);"  onclick="hideDefaultPortfolioModal();">Cancel</a>		
		 	</div>
		  </div>
		 </div>
	</div>
</div>
</div>  
    
    
    
    
    
    
    <div  class="modal hide dspqQuestionDiv"  id="dspqQuestionDiv"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="table-responsive">
		<div class='dspq-dialog'>
			<div class='modal-content'>
			<div class="modal-header">
		  		<!--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		  		-->
		  		<button type="button" id="dspqGridCancel" class="close show" data-dismiss="modal" aria-hidden="true">x</button>	
		  		<button type="button" id="dspqQuestionCancel" class="close hide"  onclick="hideDspqQuestion();">x</button>
		 		
				<h3><span id="myModalLabelDspq" style="margin-left: 0px;">Custom Fields</span></h3></h3>
				<input type="hidden" id="myModalLabelDspqHidden" name="myModalLabelDspqHidden" />
				<input type="hidden" id="parentQuestionId" name="parentQuestionId" />
				<input type="hidden" id="appTypes" name="appTypes" />
			</div>
			<div class="modal-body">
			<div class="show" id="customGrid">
						<div id="dspqCustomGrid"></div>
				</div>								
					<div class="hide" id="dspqCustomQuestion">
					<div class="col-sm-12 col-md-12">
							<div id="errordiv"></div>
						</div>
						<div class="row">
							<div class="col-sm-6 col-md-6" id="dspqGroupMastersId"><label><strong>Section</strong></label><span class="required">*</span>
							<span href="javascript:void(0);"  data-toggle="tooltip" data-placement="left" title="Section">
								<img src="images/qua-icon.png" alt="">					
							</span>
								<select  class="form-control " name="dspqGroupMaster" id="dspqGroupMaster" onchange="getSubSectionByGroupId(this.value)">
							      <option value="0">Select Section</option>
							      	<c:forEach items="${dspqGroupMasters}" var="dspqGroupMasters" >	
										<option value="${dspqGroupMasters.groupId}" >${dspqGroupMasters.groupName}</option>
									</c:forEach>
								</select>
							</div>
							<div class="col-sm-6 col-md-6" id="dspqSectionMasterId"><label><strong>Sub Section</strong></label><span class="required">*</span>
								<span href="javascript:void(0);"  data-toggle="tooltip" data-placement="left" title="Sub Section">
									<img src="images/qua-icon.png" alt="">					
								</span>
								<select  class="form-control " name="dspqSectionMaster" id="dspqSectionMaster">
							      <option value="0">Select Sub Section</option>
								</select>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-6 col-md-6" id="assInstruction"><label><strong>Custom Field Name</strong></label><span class="required">*</span>
								<span href="javascript:void(0);"  data-toggle="tooltip" data-placement="left" title="Custom Field Name">
									<img src="images/qua-icon.png" alt="">					
								</span>
								<input type="text" id="fieldName" name="fieldName" value="" maxlength="500" class="form-control" style="width:100%;">
							</div>
							<div class="col-sm-6 col-md-6" id="assInstruction"><label><strong>Custom Field Display Label</strong></label><span class="required">*</span>
								<span href="javascript:void(0);"  data-toggle="tooltip" data-placement="left" title="Custom Field Display Label">
									<img src="images/qua-icon.png" alt="">					
								</span>
								<input type="text" id="fieldDisplayLabel" name="fieldDisplayLabel" maxlength="500" value="" class="form-control" style="width:100%;">
							</div>
						</div>
							<div class="row">
							<div class="col-sm-6 col-md-6" id="assInstruction"><label><strong>Custom Field Helper Text</strong></label>
								<span href="javascript:void(0);"  data-toggle="tooltip" data-placement="left" title="Custom Field Helper Text">
									<img src="images/qua-icon.png" alt="">					
								</span>
								<input type="text" id="fieldHelperText" name="fieldHelperText" maxlength="80" value="" class="form-control" style="width:100%;">
							</div>
							<div class="col-sm-6 col-md-6" id="assInstruction"><label><strong>Custom Field Type</strong></label><span class="required">*</span>
							   <span href="javascript:void(0);"  data-toggle="tooltip" data-placement="left" title="Custom Field Type">
									<img src="images/qua-icon.png" alt="">					
								</span>
							   <select  class="form-control " name="questionTypeMaster" id="questionTypeMaster" onchange="getQuestionOptions(this.value)">
							      <option value="0">Select Field Type</option>
									<c:forEach items="${questionTypeMasters}" var="questionTypeMaster" >	
										<option id="${questionTypeMaster.questionTypeId}" value="${questionTypeMaster.questionTypeId}" >${questionTypeMaster.questionType}</option>
									</c:forEach>
								</select>
							</div>
						</div>
						<div class="row">
							<!--<div class="col-sm-6 col-md-6" id="assInstruction"><label><strong>Field Helper Text</strong></label>
								<input type="text" id="tooltipText" name="tooltip" value="" class="form-control" style="width:100%;">
							</div>
							
							--><div class="col-sm-6 col-md-6" id="assInstruction">
<!--							<label style="margin-bottom: 0px;"><strong>Field Type</strong></label>-->
								<!--<div class="row">  
								<div class="col-sm-4 col-md-4">-->
									<label><strong>Custom Field is<!-- <span class="required">*</span> --></strong></label><span class="required">*</span>
									<span href="javascript:void(0);"  data-toggle="tooltip" data-placement="left" title="Custom Field is Required/Optional">
									<img src="images/qua-icon.png" alt="">					
								</span>
<!--								</div>-->
								
								<div class="col-sm-12 col-md-12">
								<div class="col-sm-6 col-md-6" style="padding-left: 0px;">
							        <label><input type="radio" name="isRequiredStatus" id="isRequired"> Required</label>
							    </div>
							    
							    <div class="col-sm-6 col-md-6">
							        <label><input type="radio" name="isRequiredStatus" id="optional"> Optional</label>
							    </div>
							    </div>
							    <!--
							    
								</div>
							--></div>
						</div>
						
							<div class="row top5" id="row0" style="display: none">
								<div class="spanfit1 col-sm-9 col-md-9"><label><strong>Options</strong></label>
								<span href="javascript:void(0);"  data-toggle="tooltip" data-placement="left" title="Select the check box against the valid option(s)">
									<img src="images/qua-icon.png" alt="">					
								</span>
								</div>
								<div class="spanfit1 col-sm-3 col-md-3 pleft40" id='addMoreOptionsDivId'>
									<label><strong><a href='javascript:void(0);' onclick="addCustomFieldMoreOtions()">Add more options</a></strong></label>
									<span href="javascript:void(0);"  data-toggle="tooltip" data-placement="left" title="Add more options"><img src="images/qua-icon.png" alt="Add more options"></span>
									<input type="hidden" id="noOfOptionRows" name="noOfOptionRows" value="3">
								</div>
							</div>
							
							<div class="row" id="rowExplain" style="display: none;">
							<div class="col-sm-12 col-md-12" id="assExplanation">
							<label><strong>Explanation</strong></label>
							<span href="javascript:void(0);"  data-toggle="tooltip" data-placement="left" title="Explanation">
									<img src="images/qua-icon.png" alt="">					
								</span>
							
								<textarea  name="kendoQuestionExplanation" id="kendoQuestionExplanation" maxlength="2500" rows="8" cols="30" class="form-control"></textarea>
							</div>
							</div>
							
							<div id="allOptionsArea"></div>
							
							<!--
							<div class="row top10" id="row1" style="display: none">	
						        <div class="spanfit1 left15 pull-left" >
								    <div class="top7 pull-left">1</div>
									<div class="pull-left" style="width: 325px;margin-left: 5px;">
									<input type="hidden" name="hid1" id="hid1"/><input type="text" name="opt1" id="opt1" class="form-control" maxlength="50" placeholder="">
									</div>
									<div class="pull-left top7" style="width: 40px;margin-left: 5px;">
									</div>				
									<div class="clearfix"></div>
								</div>	

						        <div class="spanfit1 pull-left" >
								    <div class="top7 pull-left">2</div>
									<div class="pull-left" style="width: 325px;margin-left: 5px;">
									<input type="hidden" name="hid2" id="hid2"/><input type="text" name="opt2" id="opt2" class="form-control" maxlength="50" placeholder="">
									</div>
									<div class="pull-left top7" style="width: 40px;margin-left: 5px;">
									</div>				
									<div class="clearfix"></div>
								</div>	
							</div>
						 -->
						 	
						<!--
						<div class="row top5" id="row2" style="display: none">	
					        <div class="spanfit1 left15 pull-left" >
							    <div class="top7 pull-left">3</div>
								<div class="pull-left" style="width: 325px;margin-left: 5px;">
							    <input type="hidden" name="hid3" id="hid3"/><input type="text" name="opt3" id="opt3" class="form-control" maxlength="50" placeholder="">
								</div>
								<div class="pull-left  top7" style="width: 40px;margin-left: 5px;">
								</div>				
								<div class="clearfix"></div>
							</div>	
					
					        <div class="spanfit1  pull-left" >
							    <div class="top7 pull-left">4</div>
								<div class="pull-left" style="width: 325px;margin-left: 5px;">
								 <input type="hidden" name="hid4" id="hid4"/><input type="text" name="opt4" id="opt4" class="form-control" maxlength="50" placeholder="">
								</div>
								<div class="pull-left top7" style="width: 40px;margin-left: 5px;">
								</div>				
								<div class="clearfix"></div>
							</div>	
						</div>
						 -->
						 	
						<!-- <div class="row top5" id="row3" style="display: none">	
					        <div class="spanfit1 left15 pull-left" >
							    <div class="top7 pull-left">5</div>
								<div class="pull-left" style="width: 325px;margin-left: 5px;">
							    <input type="hidden" name="hid5" id="hid5"/><input type="text" name="opt5" id="opt5" class="form-control" maxlength="50" placeholder="">
								</div>
								<div class="pull-left top7" style="width: 40px;margin-left: 5px;">
								</div>				
							<div class="clearfix"></div>
						</div>

				        	<div class="spanfit1  pull-left" >
						    <div class="top7 pull-left">6</div>
							<div class="pull-left" style="width: 325px;margin-left: 5px;">
							<input type="hidden" name="hid6" id="hid6"/><input type="text" name="opt6" id="opt6" class="form-control" maxlength="50" placeholder="">
							</div>
							<div class="pull-left top7" style="width: 40px;">
								</div>				
								<div class="clearfix"></div>
							</div>	
						</div>-->
						
						
						
						<div class="row">
							<div class="col-sm-12 col-md-12" id="assInstruction"><label><strong> Custom Field Instructions</strong></label>
							<span href="javascript:void(0);"  data-toggle="tooltip" data-placement="left" title="Custom Field Instructions">
									<img src="images/qua-icon.png" alt="">					
								</span>
								<!--<textarea  class="form-control" name="instruction_E" id="instruction_E" maxlength="2500" rows="2"></textarea>
								-->
								<textarea id="kendoEditorInstruction" name="kendoEditorInstruction" maxlength="3000" rows="8" class="form-control"></textarea>
							</div>
						</div>
				</div>	
		 	</div>
		 	<div class="modal-footer">
		 		<button id="saveQues" onclick="saveQuestionFromDspq('save')" class="flatbtn" style="background: #99cf77; width: 15%;" ><strong>Save <i class="icon"></i></strong></button>&nbsp;
		 		<!--<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="hideDiv();">Close<spring:message code="btnClose"/></button>--> 
		 			<!--<a href="javascript:void(0);" id="dspqGridCancel1" class="show" data-dismiss="modal" aria-hidden="true">Cancel</a>	
		 	--></div>
		  </div>
		 </div>
	</div>
</div>
<div  class="modal hide"  id="portfolioChangeWarningWithJobCategory" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
	<div class="modal-dialog" style="width: 850px;">
	<div class="modal-content">	
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick="portfolioChangeWarningCloseDiv();">x</button>
		<h3 id="myModalLabel">Teachermatch</h3>
	</div>
	<div class="modal-body" style="max-height: 450px;overflow-y: auto">		
		<div class="control-group">
			<div id="portfolioChangeWarningMsgWithJobCategory"></div>
		</div>
 	</div>
 	<div class="modal-footer">
 	    <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true"   onclick="portfolioWarningYesPrompt();">Yes</button>
 		<button class="btn" data-dismiss="modal" aria-hidden="true"   onclick="portfolioChangeWarningCloseDiv();">No</button>  		
 	</div>
   </div>
 </div>
</div>
<div  class="modal hide"  id="defaultTMPortfolio" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
	<div class="modal-dialog" style="width: 650px;">
	<div class="modal-content">	
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick="portfolioChangeWarningCloseDiv();">x</button>
		<h3 id="myModalLabel">Teachermatch</h3>
	</div>
	<div class="modal-body" style="max-height: 450px;overflow-y: auto">		
		<div class="control-group">
			You are about to create a TeacherMatch Portfolio template. This template portfolio will be visible across all districts.
			 To make it active for any one district, you will have to clone it into the district.<br/>
			Are you sure you want to create this portfolio template?
		</div>
 	</div>
 	<div class="modal-footer">
 	    <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true"   onclick="savePortfolioValues();">Yes</button>
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="cancelDefaultTMPortfolio();">Cancel</button>  		
 	</div>
   </div>
 </div>
</div>
<script type="text/javascript">
    $(document).on("click", function(event){
        var $trigger = $(".moreFileds1");
        if($trigger !== event.target && !$trigger.has(event.target).length){
            $('.moreFileds1').removeClass("open");  
        }            
    });
</script>
<script>
	document.addEventListener("click", globalLsterSetter);
	document.addEventListener("mouseout", globalLsterSetter);
</script>
