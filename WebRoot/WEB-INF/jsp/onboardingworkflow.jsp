<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>

<script type="text/javascript" src="dwr/interface/JobUploadTempAjax.js?ver=${resourceMap['JobUploadTempAjax.ajax']}"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" />
<script type="text/javascript" src="js/onboarding.js?ver=${resourceMap['js/onboarding.js']}"></script>
<script type="text/javascript" src="dwr/interface/OnboardingDashboardAjax.js?ver=${resourceMap['OnboardingDashboardAjax.ajax']}"></script>
<script type="text/javascript" src="js/selfServiceCommon.js?ver=${resourceMap['js/selfServiceCommon.js']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type='text/javascript' src="js/bootstrap-slider.js"></script>
<link rel="stylesheet" type="text/css" href="css/slider.css" />  

<style type="text/css">
.jqte{
	width:97%;
}
.divshadow
{
	box-shadow: 0px 1px 1px 1px #999;
	border-radius: 3px;
}
.flatbtn
{
    width: 30%;
    height: 35px;
    padding: 0px 1px 2px 1px;
    font-size: 14px;
    color: white;
    border-radius: 3px;
    text-align: center;
    /*background: #99cf77;*/
    border: 0;
    cursor: pointer;
    font-family: 'Century Gothic','Open Sans', sans-serif;
}
.widthCl
{
	width:25%;
}
.externalDiv,.internalDiv,.internalTDiv{
	padding-left:5px;
	padding-right:5px;
}
.green
{
	background-color:green; text-align:center;  cursor: pointer;
}
.grey
{
	background-color:gray; text-align:center;  cursor: pointer;
}
.lfloat
{
	float:left;
	width: 20px;
	height: 20px;
	margin-left:5px;
}
</style>
<script src="calender/js/jscal2.js"></script>
<script src="calender/js/lang/en.js"></script>
<link rel="stylesheet" type="text/css" href="calender/css/jscal2.css" />
<link rel="stylesheet" type="text/css" href="calender/css/border-radius.css" />
<link rel="stylesheet" type="text/css" href="calender/css/gold/gold.css" />
<input type="hidden" value="0" id="popuptoevalute"/>
		<div id="loadingDiv" style="display:none;z-index: 5000;" >
	     	<table  align="center" >
	 			<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
	 			<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
			</table>
	    </div>
		<div id="loadingDivOnBoarding" style="display:none;z-index: 5000;" >
	     	<table  align="center" >
		 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
		 		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
			</table>
	    </div>
<div class="row divshadow" style="width:100%;margin-left:0px;">
<div class="col-sm-12 col-md-12" style="box-shadow: 0px 0px 0px 0px #999;background-color: #f7f7f7;" >
<div class="row" >
      <div class="col-sm-1 col-md-1" style="background-color: rgb(241, 218, 218);height: 35px;">
         	<img src="images/Recruitingdspq1.png" alt="" style="margin-top: 7px;margin-left: 5px;">
         </div>        
      <div class="col-sm-8 col-md-8" style="padding-left: 0px;">
         	<div class="subheading" style="font-size: 13px;"><spring:message code="lblPNRDashboard"/></div>	
      </div>
	  <div id="AddOnboarding" class="col-sm-3 col-md-3" style="line-height: 34px;text-align: right;">
			<a href="javascript:void(0);" onclick="showOnboarding();">+ <spring:message code="lblAddOnboardingdetails"/></a>	
			<span href="#"  data-toggle="tooltip" data-placement="left" style="cursor: pointer" title="Add Onboarding details">
				<img src="images/qua-icon.png" alt="">					
			</span>
	  </div>
	  <div id="editOnboarding" class="col-sm-3 col-md-3 hide" style="line-height: 34px;text-align: right;">
			<a href="javascript:void(0);" onclick="cancelOnboardingForm();"><spring:message code="lblBacktoOnboarding"/></a>
				<span href="#"  data-toggle="tooltip" data-placement="left" style="cursor: pointer" title="Back to Onboarding">
					  <img src="images/qua-icon.png" alt="">					
				</span>
	  </div> 
     </div>
		     <input type="hidden" id="onboardID" value="" />
		     <input type="hidden" id="cloneID" value="" />
		     <input type="hidden" id="actionType" value="1" />
		     <input type="hidden" id="part"/>
		     <input type="hidden" id="checkCandidateType"/>
		     <input type="hidden" id="checkPointTransID" value="">
		     <input type="hidden" id="checkpointQuestionID">
		     <input type="hidden" id="showActionTypeCheckPointStatus" value="1">
		     <input type="hidden" id="counterinc" value="4">
		     <input type="hidden" id="editchkid" value="0">
		     <input type="hidden" id="editqsnid" value="0">
		     <input type="hidden" id="editoptid" value="0">
		     <input type="hidden" id="editprimaryofstatus" value="0">
		     <input type="hidden" id="uploadfilenames" value=""/>
		     <input type="hidden" id="readyonlycheckpointid" value="">
		     <span id="editFileNames"></span>
		     <input type="hidden" id="editReadOnlyCheckPointName" value="">
		     <input type="hidden" id="checkPointNameEdit" value="">
		     <input type="hidden" id="editCheckPointStatusNameEdit" value="">
		     <input type="hidden" id="editCandidateTypeCheck" value="">
		     <input type="hidden" id="editCandidateTypeCheck2" value="">
	</div>
	<div class="row">
		<div class="col-sm-12 col-md-12 mb10">
			<div class='divErrorMsg hide pleft22' id='errordivonboard' style="padding-top: 10px;"></div>
		</div>
	</div>
	 	<input type="hidden" id="entityTypeID" value="${entityType}" />
	<c:if test="${entityType eq 2}">
		<input type="hidden" id="districtId" value="${DistrictId}"/>
		<input type="hidden" id="districtSearchId" value='0' />
	</c:if>
	 	<% String id=session.getAttribute("displayType").toString();%>
		<input type="hidden" id="displayTypeId" value="<%=id%>"/>
<%if(id.equalsIgnoreCase("0")){ %>	
	<c:if test="${entityType eq 1}">
	<div id="searchBox">
	<div class="row" >
		<div class="col-sm-4 col-md-4 left25" >
			<spring:message code="lblEntityType"/>
		</div>
		<div class="col-sm-7 col-md-7 pleft0" >
			<span id="districtSearchLabel" class="hide"><spring:message code="lblDistrictName"/></span>			
		</div>
	</div>
	<div class="row" >
	<div class="col-sm-4 col-md-4 left25">
			<select class="form-control" onchange="displayOrHideSearchBox();" id="entityType" name="entityType">
				<option value="1"><spring:message code="sltTM"/></option>
				<option value="2"><spring:message code="lblDistrict"/></option>
			</select>
	</div>
	<div class="col-sm-5 col-md-5 hide pleft0" id="districtSearchBox" style="width:44.5%;">			
		    <span>
		        <input style="color:#555555; font-size: 14px;"  type="text" id="districtSearchName"  value="" maxlength="100"  name="districtSearchName" class="help-inline form-control"
		          	onfocus="getDistrictAuto(this, event, 'divTxtSearchShowData', 'districtSearchName','districtSearchId','');"
					onkeyup="getDistrictAuto(this,event,'divTxtSearchShowData', 'districtSearchName','districtSearchId','');"
					onblur="hideDistrictMasterDiv(this,'districtSearchId','divTxtSearchShowData');"	/>
		    </span>
		      	<input type="hidden" id="districtSearchId" value="${DistrictId}" />
		        <div id='divTxtSearchShowData'  onmouseover="mouseOverChk('divTxtSearchShowData','districtSearchName')" style=' display:none;position:absolute;z-index:5000;' class='result' ></div>
	</div>
	<div class="col-sm-2 col-md-2" style="padding:0px 20px 0px 0px;">
			<button onclick="searchRecordByDistrict();" class="flatbtn" style="background: #9fcf68;width:100%" id="search" title="<spring:message code="lnkSearch"/>">
			  		<strong><spring:message code="lnkSearch"/> <i class="fa fa-check-circle"></i></strong>
		    </button>
	</div>
	</div>
	</div>
 </c:if>
 <%} %>
 	<div class="col-sm-12 col-md-12 top10 mb10 pleftright" id="addOnboardingDiv">
		<div class="row" style="background-color:#007fb2; color:white; padding:7px 10px 6px 5px; width:100%; margin-left:0%;">
			<div class="col-sm-4 col-md-4"><spring:message code="lblOnboardingName"/></div>
			<div class="col-sm-2 col-md-2"><spring:message code="lblAppliedAt"/></div>
			<div class="col-sm-3 col-md-3"><spring:message code="lblActivationDate"/></div>
			<div class="col-sm-2 col-md-2"><spring:message code="lblStatus"/></div>
			<div class="col-sm-1 col-md-1" style="text-align:left; padding:0px;"><spring:message code="lblAct"/></div>
		</div>
        <div class="row" id="dataGrid" style="padding:7px 5px 7px 5px; width:100%; margin-left:0%;">
			<div id="onBoardGrid"></div>
		</div>
 	</div>
	<div class="col-sm-12 col-md-12 top10 hide"  id="showOnboardingDiv">
		<c:if test="${entityType==1}">
		<div class="row">
	  <div class="col-sm-4 col-md-4" style="padding:8px 0px 0px 25px;">					  
		<label>           
            District Name<span class="required">*</span> <span href="#"  data-toggle="tooltip" data-placement="left" title="District Name"><img src="images/qua-icon.png" alt=""></span>
         </label>
      </div>
      <div class="col-sm-8 col-md-8" style=""> 	              
	        
		        <span>
		        	<input style="width: 97.2%; color:#555555; font-size: 14px;"  type="text" id="districtName"  value="${DistrictName}" maxlength="100"  name="districtName" class="help-inline form-control"
		          	onfocus="getDistrictAuto(this, event, 'divTxtShowData', 'districtName','districtId','');"
					onkeyup="getDistrictAuto(this,event,'divTxtShowData', 'districtName','districtId','');"
					onblur="hideDistrictMasterDiv(this,'districtId','divTxtShowData');"	/>
		      	</span>
		      	<input type="hidden" id="districtId"/>
		        <div id='divTxtShowData'  onmouseover="mouseOverChk('divTxtShowData','districtName')" style=' display:none;position:absolute;z-index:5000;' class='result' ></div>
      	  
	  </div>
      </div>
      </c:if>
	<div class="row">
		 <div class="col-sm-4 col-md-4" style="padding:8px 0px 0px 25px;">
			<label class="mb0"><spring:message code="lblboardingDashboardName"/><span class="required">*</span> <span data-toggle="tooltip" data-placement="left" title="<spring:message code="lblboardingDashboardName"/>"><img src="images/qua-icon.png" alt=""></span></label>
		  </div>
		  <div class="col-sm-7 col-md-7" style="width:65%;">
			<input type="text" id="managedashboardname" name="managedashboardname" class="form-control mt5" >
		  </div>
	</div>
	<div class="row mt0 mb0">
	 	<div class="col-sm-4 col-md-4" style="padding:8px 0px 0px 25px;">
		 	<label><spring:message code="lblApplythisDashboard"/> <span class="required">*</span>  <span  data-toggle="tooltip" data-placement="left" title=" <spring:message code="lblApplythisDashboard"/>"><img src="images/qua-icon.png" alt=""></span></label>
	 	</div>
		<div class="col-sm-6 col-md-6">
			<label class="checkbox"><input type="checkbox" name="applyforall" id="applyforall">
			<spring:message code="lblForalljobcategories"/> </label>	
		</div>
	</div>
	<div class="row mb30 hideButton" >
	<div class="col-sm-9 col-md-9"></div>
	<div class="col-sm-3 col-md-3" style="text-align: right;margin-left:-14px;">
			<button onclick="saveAndContinue();" class="flatbtn" style="background: #9fcf68;width:80%" id="save" title="Continue">
			  <strong><spring:message code="btnConti"/>  <i class="fa fa-check-circle"></i></strong>
		    </button>
		</div>
	</div>
	
	<div class="row mb30 hideupdateButton" >
	<div class="col-sm-9 col-md-9"></div>
	<div class="col-sm-3 col-md-3" style="text-align: right;">
			<button onclick="updateAndContinue();" class="flatbtn" style="background: #9fcf68;width:60%" id="save" title="Continue">
			  <strong><spring:message code="btnConti"/>  <i class="fa fa-check-circle"></i></strong>
		    </button>
		</div>
	</div>
	</div>
	<div class="col-sm-12 col-md-12 top10 hide" id="showOnboardingDivContinue">
	<div class="row">
		<div class="col-sm-12 col-md-12">
			<div class='divErrorMsg' id='errordivManage' style="padding-left: 2px; padding-top: 10px;"></div>
		</div>
	</div>
 <div class="modal-body" style="padding-top:0px;padding-left: 25px;padding-right: 25px;">
				<div class='row top10' style='margin-right: 5px;'>
					<div class='col-sm-2 col-md-2'
						style='height: 42px; border: 1px rgb(213, 213, 213) solid; border-left: 0px;padding-left:0px;'>
						<label style='margin-top: 10px; font-weight: bold;'>
							<spring:message code="lblCandidateType"/>
							</label>
					</div>
					<div class='col-sm-10 col-md-10' style='padding-right: 0px;'>
						<div class='row'
							style='border: 1px rgb(213, 213, 213) solid; border-left: 0px; border-right: 0px;'>
							<div class='col-sm-4 col-md-4 checkbox' style="padding-left: 30px;">
							<label class="checkbox inline" style="margin-top: 2px;margin-bottom: 2px;margin-left: -25px;">
								<input type='checkbox' id='section0' checked disabled="disabled" onclick='hideOrShowCondidateType(1);'>
									<spring:message code="lblExternalE"/></label>
							</div>
							<div class='col-sm-4 col-md-4 checkbox' style='margin-top: 10px;'>
							<label class="checkbox inline" style="margin-top: 2px;margin-bottom: 2px;margin-left: -25px;">
								<input type="checkbox" id="section1" checked onclick="hideOrShowCondidateType(2);"> 
									<spring:message code="lblInternalI"/></label>
							</div>
							<div class='col-sm-4 col-md-4 checkbox' style='margin-top: 10px;'>
							<label class="checkbox inline" style="margin-top: 2px;margin-bottom: 2px;margin-left: -25px;">
								<input type='checkbox' id='section2'	checked onclick='hideOrShowCondidateType(3);'>
									<spring:message code="lblInternalTransfer"/></label>
							</div>
						</div>
					</div>
					</div>
					
				 <div class="row mt10 mb10" style="margin-bottom:10px;padding-left:16px;">
				 
				       <div class="col-sm-6 col-md-6">
				         <div class="row">
					       <div class="col-sm-6 col-md-6 pleftright">
					        <label class="circleHead grey" style="margin-left:0px;" ><spring:message code="instS" /></label> <spring:message code="lblNoCheckpointStatusdefined" />
					        </div>
					       <div class="col-sm-6 col-md-6 pleftright">
					        <label class="circleHead green" style="margin-left:0px;"><spring:message code="instS" /></label> <spring:message code="lblCheckpointStatusdefined" />
					       </div>
				        </div>
				       </div>
				       <div class="col-sm-6 col-md-6">
				         <div class="row">
					       <div class="col-sm-6 col-md-6 pleftright">
					       <label class="circleHead grey" style="margin-left:0px;" ><spring:message code="instC" /></label> <spring:message code="lblNoCommunicationdefined" />
					       </div>
					       <div class="col-sm-6 col-md-6 pleftright">
					       <label class="circleHead green" style="margin-left:0px;"><spring:message code="instC" /></label> <spring:message code="lblCommunicationdefined" />
					       </div>
					       </div>
				         </div>
				       <div class="col-sm-6 col-md-6">
				         <div class="row">
					       <div class="col-sm-6 col-md-6 pleftright">
					       <label class="circleHead grey" style="margin-left:0px;" ><spring:message code="instN" /></label> <spring:message code="lblNoActionRequired" />
					       </div>
					       <div class="col-sm-6 col-md-6 pleftright">
					       		<label class="circleHead green" style="margin-left:0px;"><spring:message code="instN" /></label>  <spring:message code="lblCandidateActionRequired" />
					        </div>
				         </div>
				       </div>
				       <div class="col-sm-6 col-md-6">
				         <div class="row">
					       <div class="col-sm-6 col-md-6 pleftright">
					       <label class="circleHead grey" style="margin-left:0px;"><spring:message code="instX" /></label> <spring:message code="optInActiv" />
					       </div>
					       <div class="col-sm-6 col-md-6 pleftright">
					       <label class="circleHead green" style="margin-left:0px;" ><spring:message code="instA" /></label> <spring:message code="optAct" />
					       </div>
				        </div>
		              </div>
		          </div>
		          
		          <!-- 			Onboarding Read Only Checkpoints Section					-->
					<div class='row top5' style='background-color: #007fb2; font-weight: bold;padding-top: 5px; color: white;  padding-left: 2px;height: 30px;'>
					<div class='col-sm-12 col-md-12' style="padding-left: 0px;">
							<spring:message code="lblOnboardingReadOnlyCheckpoints" />
						</div>
					</div>
					<div id="readOnlyCheckPointDuplicateError" class="hide DSPQRequired12" style="margin-left: -13px;"></div>
					<div class="row pleft15 " style="font-weight: bold; padding-top:5px; color:#474747;">
					   <div class="col-sm-3 col-md-3 widthCl">  </div>
					   <div class="col-sm-1 col-md-1 showhideedit pleft0"><label><spring:message code="lblEdit" /></label> </div>
					   <div class="col-sm-2 col-md-2"></div>
					   <div class="col-sm-2 col-md-2  pleftright"><label ><spring:message code="lblStatus" /></label> </div>
					   <div class="col-sm-4 col-md-4 " >	  </div>
					</div>
					
		<div id="readOnlyCheckPointDiv"></div>
		<div class="hide top10 pleft0" id="readOnlyEditSection">
			 <input type="hidden" id="readOnlyCheckPointId"/>
			 <input type="hidden" id="readOnlytableType"/>
			 <div class="row" style="border-radius: 5px;border: 1px solid rgb(0, 127, 178);">
			 <div class='row top5 mb5' style='font-weight: bold;color: #007fb2;'>
				<div class='col-sm-12 col-md-12 pleft19'>
						<spring:message code="lblReadOnlyCheckpoint" /> - <spring:message code="lblEdit" /></label>
					</div>
			</div>
			 <div class="row top5">
				 <div class="col-sm-2 col-md-2 mt10 pright0 pleft19">
					<label class="mb0"><span>  <spring:message code="lblReadOnlyCheckPointName"/></span> <span class="required">*</span> <span data-toggle="tooltip" data-placement="left" title=" <spring:message code="lblReadOnlyCheckPointName" />"><img src="images/qua-icon.png" alt=""></span></label>
				</div>
			    <div class="col-sm-4 col-md-4 pright0">
					<input type="text" id="readOnlyCheckPointname" name="readOnlyCheckPointname" class="form-control mt5" maxlength="30">
				</div>
			 </div>
			  <div class="row top5">
			      <div class="col-sm-2 col-md-2 top10 pright0 pleft19">
			        <label><span><spring:message code="lblStatus"/></span> <span data-toggle="tooltip" data-placement="left" title=" <spring:message code="lblStatus" />"><img src="images/qua-icon.png" alt=""></span></label>  
			      </div>
				  <div class="col-sm-1 col-md-1 pright0">
					<label class="radio"><input type="radio" name="readOnlyStatus" id="readOnlyActive" value="1" checked>
					<spring:message code="optAct"/> </label>	
			</div>
			<div class="col-sm-1 col-md-1 pright0">
					<label class="radio"><input type="radio" name="readOnlyStatus" id="readOnlyInactive" value="0">
					<spring:message code="optInActiv"/> </label>	
			</div>
			  </div>
			  <div class="row top15">
			  <div class="col-sm-6 col-md-6 pright0 pleft19">
			    <label>
              		<a href='javascript:void(0);' onclick="updateReadOnlyCheckPointJS();"> <spring:message code="lnkImD"/> </a> &nbsp;&nbsp;
              		<a href='javascript:void(0);' onclick="hideReadOnlyEditSection();"> <spring:message code="btnClr"/> </a>
                  </label>
                  </div>
			  </div>
			 </div>
	</div>
				<!--    End Onboarding Read Only Checkpoints Section				 -->
		          
					<div class="row">
					     <div class='col-sm-12 col-md-12' style="text-align:right;padding-right: 0px;">
							<a href="javascript:void(0);" onclick="addEditCheckPoint('0','');"><spring:message code="addCheckPoint"/>						
					</a>	
						<span  data-toggle="tooltip" data-placement="left" style="cursor: pointer" title="Add Checkpoints">
						<img src="images/qua-icon.png" alt="">					
						</span>
						</div>
					</div>
					<div class='row top5' style='background-color: #007fb2; font-weight: bold;padding-top: 5px; color: white;  padding-left: 2px;height: 30px;'>
					<div class='col-sm-12 col-md-12' style="padding-left: 0px;">
							<spring:message code="lblOnboardingCheckpoints" />
						</div>
					</div>
					<div class='row' >
						<div class='col-sm-12 col-md-12'>
							<div class='divError hide' id='errordivfield' style="padding-left: 2px; color:red; padding-top: 10px;"></div>
						</div>
					</div>
					<div class="row" style="font-weight: bold; padding-top:5px; padding-right:15px;  color:#474747;">
					   <div class="col-sm-3 col-md-3 widthCl">  </div>
					   <div class="col-sm-1 col-md-1 showhideedit"><label><spring:message code="lblEdit" /></label> </div>
					   <div class="col-sm-2 col-md-2 managewidth" id="extraWidth">	  </div>
					   <div class="col-sm-2 col-md-2  externalDiv "><label style="padding-left:40px;"><spring:message code="lblExternalE" /></label></div>
					   <div class="col-sm-2 col-md-2  internalDiv"><label style="padding-left:48px;"><spring:message code="lblInternalI" /></label></div>
					   <div class="col-sm-2 col-md-2  internalTDiv pleftright"><label style="padding-left:22px;"><spring:message code="lblInternalTransfer" /></label> </div>
					   
					</div>
					<input type="hidden" id="checkNoRecord"/>
					<input type="hidden" id="checkStatusCandidateType"/>
					<input type="hidden" id="checkPointId"/>
					<div id="checkPointDiv"></div>
						<div class="row">
			<div class="col-sm-12 col-md-12 top15" style="margin-bottom: 20px; padding-left: 11px;">
				<div class="row">
					<div class="col-sm-6 col-md-6 pleft6">
					   <button onclick="updateAndContinue();" class="flatbtn" style="background: #F89507;width:32%" id="save">
				       	<strong>Save and Exit <i class="fa fa-check-circle"></i></strong>
				       </button>
						&nbsp;&nbsp;
					     <button onclick="cancelOnboardingForm();" class="flatbtn" style="background: #da4f49;" id="cancel">
	          				<strong>Cancel <i class="fa fa-times-circle"></i></strong>
	          			</button>
					</div>	
				 </div>
				</div>
		    </div>
      	</div>
      </div>
  </div>

<div  class="modal hide"  id="manageCommunication"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" style="background: rgba(153, 153, 153, 0.8);z-index: 1050;" >
 <div class="table-responsive">
   <div class="row" style="width:70%;margin:0px 0px 0px 205px;background-color:#FFFFFF;">
	 <div class="col-sm-12 col-md-12" style="box-shadow: 0px 0px 0px 0px #999; background-color: #f5f5f5;" >
      <div class="row" >
        <div class="col-sm-1 col-md-1" style="background-color: rgb(239, 95, 95);height: 35px;">
         	<img src="images/Recruitingdspq1.png" alt="" style="margin-top: 7px;margin-left: 5px;">
         </div>        
         <div class="col-sm-10 col-md-10" style="padding-left: 0px;">
         	<div style="font-size: 13px;"><label class="subheading mb0"><spring:message code="optCommu"/></label> -<label class="subheading mb0" id="communicationdashboardname"></label></div>	
         </div>
		 <div id="AddOnboarding" class="col-sm-1 col-md-1" style="line-height: 34px;text-align: right;">
					<a href="javascript:void(0);" onclick="cancelCommunicationDiv(1);"><b style="font-size: 15px;">X</b></a>	
		 </div>
      </div>
      </div>
      <input type="hidden" id="communicationId" value="0"/>
      <div class="left25" style="width:95%;">
      <div class="row">
      	<div class="col-sm-10 col-md-10"></div>
        <div class="col-sm-2 col-md-2" style="text-align:right;"><a href="javascript:void(0);" onclick="showHideTypeMasterDiv();">+Add Template 
        </a><span  data-toggle="tooltip" data-placement="left" style="cursor: pointer" title="Add Template"><img src="images/qua-icon.png"></span>  </div>
      </div>
       <div class="col-sm-12 col-md-12 mb10 pleftright divshadow top5" id="communicationGrid">
		<div class="row" style="background-color:#007fb2; color:white; padding:7px 10px 6px 5px; width:100%; margin-left:0%;">
			<div class="col-sm-3 col-md-3"><spring:message code="lblTemplateName"/></div>
			<div class="col-sm-1 col-md-1 pleftright"><spring:message code="lblAppliedAt"/></div>
			<div class="col-sm-3 col-md-3"><spring:message code="msgSubjectLine"/></div>
			<div class="col-sm-3 col-md-3"><spring:message code="lblEmailText"/></div>
			<div class="col-sm-1 col-md-1"><spring:message code="lblStatus"/></div>
			<div class="col-sm-1 col-md-1" style="text-align:center; padding:0px;"><spring:message code="lblAct"/></div>
		</div>
        <div class="row" id="CommunicationDataGrid" style="padding:7px 5px 7px 5px; width:100%; margin-left:0%;">
		<div id="displayCommunicationGrid"></div>
		</div>
 </div>
      <div class="row">
		<div class="col-sm-12 col-md-12 mb10">
			<div class='divErrorMsg hide pleft22' id='errordivCommunication' style="padding-top: 10px;"></div>
		</div>
	</div> 
 	 <div class='row top5 hide multicandidateType'>
		<div class='col-sm-12 col-md-12'><label class="checkbox pleftright top0 mb0"><spring:message code="lblCommunicationneededonstatuschange"/> </label></div>
	</div>
	<div class="row multicandidateType hide">
		<div class="col-sm-1 col-md-1">
		</div>
		<div class="col-sm-2 col-md-2">
		 <label class="checkbox" style="margin-top: 0px;margin-bottom: 0px;"><input type="checkbox" name="candidateCheck" id="uType0" onclick="showCommunicationSection(0);"><spring:message code="headCand"/> </label>
		</div>
		<c:if test="${entityType ne 2}">
		<div class="col-sm-2 col-md-2">
		 <label class="checkbox" style="margin-top: 0px;margin-bottom: 0px;"><input type="checkbox" name="hqaCheck" id="uType1" onclick="showCommunicationSection(1);"><spring:message code="lblHQA"/> </label>
		</div>
		<div class="col-sm-2 col-md-2">
		 <label class="checkbox" style="margin-top: 0px;margin-bottom: 0px;"><input type="checkbox" name="baCheck" id="uType2" onclick="showCommunicationSection(2);"><spring:message code="lblBA"/> </label>
		</div>
		</c:if>
		<div class="col-sm-2 col-md-2">
		 <label class="checkbox" style="margin-top: 0px;margin-bottom: 0px;"><input type="checkbox" name="daCheck" id="uType3" onclick="showCommunicationSection(3);"><spring:message code="lblDA"/> </label>
		</div>
		<div class="col-sm-2 col-md-2">
		 <label class="checkbox" style="margin-top: 0px;margin-bottom: 0px;"><input type="checkbox" name="saCheck" id="uType4" onclick="showCommunicationSection(4);"><spring:message code="lblSA"/> </label>
		</div>
	</div>
	
	<div class="cType0Div hide top15">
		<div class='row top5' style='background-color: #007fb2; font-weight: bold;padding-top: 5px; color: white;  padding-left: 2px;height: 30px;width:100%;margin-left:0px;'>
			<div class='col-sm-12 col-md-12 pleft0'><spring:message code="lblCommunicationtemplateforCandidate"/></div>
		</div>
		<div class="col-sm-12 col-md-12">
		<input type="hidden" id="CAUserId"/>
			<label class="top5 mb0"><spring:message code="lblUserType"/><span class="required">*</span></label>
		   <dl class="dropdownCA mt0"> 
			    <dt>
				    <a href="javascript:void(0);" style="width:100%">
				      <span class="hidaCA" style="font-family: 'Century Gothic','Open Sans', sans-serif;padding-top: 4px;"><spring:message code="msgSelectUsertype"/></span>    
				      <p class="multiSelCA" style="margin-bottom: -8px;"></p>  
				    </a>
			    </dt>
			    <dd>
			        <ul class="mutliSelectCA" style="width:100%">
			        <li class="hide CLi"><label class='checkbox mb0 mt0'><input class="CAMClass" style='margin-top:1px;' type='checkbox' name='CAOpt' onclick='usertypeMultiSelect(1)' value="1" title="Candidate"/>Candidate</label></li>
			        <c:if test="${entityType ne 2}">
			        <li class="hide HQALi"><label class='checkbox mb0 mt0'><input class="HQAMClass" style='margin-top:1px;' type='checkbox' name='CAOpt' onclick='usertypeMultiSelect(1)' value="2" title="HQA"/>HQA</label></li>
			        <li class="hide BALi"><label class='checkbox mb0 mt0'><input class="BAMClass" style='margin-top:1px;' type='checkbox' name='CAOpt' onclick='usertypeMultiSelect(1)' value="3" title="BA"/>BA</label></li>
			        </c:if>
			        <li class="hide DALi"><label class='checkbox mb0 mt0'><input class="DAMClass" style='margin-top:1px;' type='checkbox' name='CAOpt' onclick='usertypeMultiSelect(1)' value="4" title="DA"/>DA</label></li>
			        <li class="hide SALi"><label class='checkbox mb0 mt0'><input class="SAMClass" style='margin-top:1px;' type='checkbox' name='CAOpt' onclick='usertypeMultiSelect(1)' value="5" title="SA"/>SA</label></li>
			        </ul>
                </dd>
			</dl>
	   </div>
		<div class="col-sm-12 col-md-12">
			<label class="top5 mb0"><spring:message code="lblTemplateName"/><span class="required">*</span></label>
		    <input type="text" class="form-control" id="ctempleteName" name="ctempleteName">
	   </div>
	   <div class="col-sm-12 col-md-12">
			<label class="top5 mb0"><spring:message code="msgSubjectLine"/><span class="required">*</span></label>
		    <input type="text" class="form-control" id="csubjectLine" name="csubjectLine">
	   </div>
	   <div class="col-sm-12 col-md-12">
			<label class="top5 mb0"><spring:message code="lblEmailText"/><span class="required">*</span></label>
			<div id="cemailText">
		    <textarea  class="form-control" name="cemailText1" id="cemailText1" maxlength="2500" rows="2"></textarea>
		    </div>
	   </div>
	   <div class="col-sm-12 col-md-12">
		   <div class="row">
				 <div class="col-sm-2 col-md-2 mt10" style="width:19%;">
					 <label class="top5 mb0"><span><spring:message code="lblStatus"/></span><span class="required">*</span> <span data-toggle="tooltip" data-placement="left" title="<spring:message code="lblStatus"/>"><img src="images/qua-icon.png" alt=""></span></label>
				 </div>
				<div class="col-sm-1 col-md-1">
						<label class="radio"><input type="radio" name="ccommStatus" id="cactive" value="1" checked>
						<spring:message code="optAct"/> </label>	
				</div>
				<div class="col-sm-1 col-md-1">
						<label class="radio"><input type="radio" name="ccommStatus" id="cinactive" value="0">
						<spring:message code="optInActiv"/> </label>	
				</div>
		  </div> 
	  </div>
	  <div class="col-sm-12 col-md-12">
	  <div class="row">
	       <div class="col-sm-2 col-md-2" style="width:19%;">
					 <label class="top5 mb0"><span><spring:message code="lblSetasdefault"/></span> <span data-toggle="tooltip" data-placement="left" title="<spring:message code="lblSetasdefault"/>"><img src="images/qua-icon.png" alt=""></span></label>
		   </div>
		   <div class="col-sm-1 col-md-1">
				<input class="radio" type="checkbox" name="csetDefault" id="csetDefault">	
		  </div>
	   </div>
	  </div>
	</div>
		
	<c:if test="${entityType ne 2}">
			<div class="cType1Div hide top15">
		<div class='row top5' style='background-color: #007fb2; font-weight: bold;padding-top: 5px; color: white;  padding-left: 2px;height: 30px;width:100%;margin-left:0px;'>
			<div class='col-sm-12 col-md-12 pleft0'><spring:message code="lblCommunicationtemplateforHQA"/></div>
		</div>
		<div class="col-sm-12 col-md-12">
		<input type="hidden" id="HQAUserId"/>
			<label class="top5 mb0"><spring:message code="lblUserType"/><span class="required">*</span></label>
		   <dl class="dropdownHQA mt0"> 
			    <dt>
				    <a href="javascript:void(0);" style="width:100%">
				      <span class="hidaHQA" style="font-family: 'Century Gothic','Open Sans', sans-serif;padding-top: 4px;"><spring:message code="msgSelectUsertype"/></span>    
				      <p class="multiSelHQA" style="margin-bottom: -8px;"></p>  
				    </a>
			    </dt>
			    <dd>
			        <ul class="mutliSelectHQA" style="width:100%">
			         <li class="hide CLi"><label class='checkbox mb0 mt0'><input class="CAMClass" style='margin-top:1px;' type='checkbox' name='HQAOpt' onclick='usertypeMultiSelect(2)' value="1" title="Candidate"/>Candidate</label></li>
			       <c:if test="${entityType ne 2}">
			        <li class="hide HQALi"><label class='checkbox mb0 mt0'><input class="HQAMClass" style='margin-top:1px;' type='checkbox' name='HQAOpt' onclick='usertypeMultiSelect(2)' value="2" title="HQA"/>HQA</label></li>
			        <li class="hide BALi"><label class='checkbox mb0 mt0'><input class="BAMClass" style='margin-top:1px;' type='checkbox' name='HQAOpt' onclick='usertypeMultiSelect(2)' value="3" title="BA"/>BA</label></li>
			       </c:if>
			        <li class="hide DALi"><label class='checkbox mb0 mt0'><input class="DAMClass" style='margin-top:1px;' type='checkbox' name='HQAOpt' onclick='usertypeMultiSelect(2)' value="4" title="DA"/>DA</label></li>
			        <li class="hide SALi"><label class='checkbox mb0 mt0'><input class="SAMClass" style='margin-top:1px;' type='checkbox' name='HQAOpt' onclick='usertypeMultiSelect(2)' value="5" title="SA"/>SA</label></li>
			        </ul>
                </dd>
			</dl>
	   </div>
		<div class="col-sm-12 col-md-12">
			<label class="top5 mb0"><spring:message code="lblTemplateName"/><span class="required">*</span></label>
		    <input type="text" class="form-control" id="hqtempleteName" name="hqtempleteName">
	   </div>
	   <div class="col-sm-12 col-md-12">
			<label class="top5 mb0"><spring:message code="msgSubjectLine"/><span class="required">*</span></label>
		    <input type="text" class="form-control" id="hqsubjectLine" name="hqsubjectLine">
	   </div>
	   <div class="col-sm-12 col-md-12">
			<label class="top5 mb0"><spring:message code="lblEmailText"/><span class="required">*</span></label>
			<div id="hqemailText">
		    <textarea  class="form-control" name="hqemailText1" id="hqemailText1" maxlength="2500" rows="2"></textarea>
		    </div>
	   </div>
	   <div class="col-sm-12 col-md-12">
		   <div class="row">
				 <div class="col-sm-2 col-md-2 mt10" style="width:19%;">
					 <label class="top5 mb0"><span><spring:message code="lblStatus"/></span><span class="required">*</span> <span data-toggle="tooltip" data-placement="left" title="<spring:message code="lblStatus"/>"><img src="images/qua-icon.png" alt=""></span></label>
				 </div>
				<div class="col-sm-1 col-md-1">
						<label class="radio"><input type="radio" name="hqcommStatus" id="hqactive" value="1" checked>
						<spring:message code="optAct"/> </label>	
				</div>
				<div class="col-sm-1 col-md-1">
						<label class="radio"><input type="radio" name="hqcommStatus" id="hqinactive" value="0">
						<spring:message code="optInActiv"/> </label>	
				</div>
		  </div> 
	  </div>
	  <div class="col-sm-12 col-md-12">
	  <div class="row">
	       <div class="col-sm-2 col-md-2" style="width:19%;">
					 <label class="top5 mb0"><span><spring:message code="lblSetasdefault"/></span> <span data-toggle="tooltip" data-placement="left" title="<spring:message code="lblSetasdefault"/>"><img src="images/qua-icon.png" alt=""></span></label>
		   </div>
		   <div class="col-sm-1 col-md-1">
				<input class="radio" type="checkbox" name="hqsetDefault" id="hqsetDefault">	
		  </div>
	   </div>
	  </div>
	</div>
	<div class="cType2Div hide top15">
		<div class='row top5' style='background-color: #007fb2; font-weight: bold;padding-top: 5px; color: white;  padding-left: 2px;height: 30px;width:100%;margin-left:0px;'>
			<div class='col-sm-12 col-md-12 pleft0'><spring:message code="lblCommunicationtemplateforBA"/></div>
		</div>
		<div class="col-sm-12 col-md-12">
		<input type="hidden" id="BAUserId"/>
			<label class="top5 mb0"><spring:message code="lblUserType"/><span class="required">*</span></label>
			 <dl class="dropdownBA mt0"> 
			    <dt>
				    <a href="javascript:void(0);" style="width:100%">
				      <span class="hidaBA" style="font-family: 'Century Gothic','Open Sans', sans-serif;padding-top: 4px;"><spring:message code="msgSelectUsertype"/></span>    
				      <p class="multiSelBA" style="margin-bottom: -8px;"></p>  
				    </a>
			    </dt>
			    <dd>
			        <ul class="mutliSelectBA" style="width:100%">
			        <li class="hide CLi"><label class='checkbox mb0 mt0'><input class="CAMClass" style='margin-top:1px;' type='checkbox' name='BAOpt' onclick='usertypeMultiSelect(3)' value="1" title="Candidate"/>Candidate</label></li>
			       <c:if test="${entityType ne 2}">
			        <li class="hide HQALi"><label class='checkbox mb0 mt0'><input class="HQAMClass" style='margin-top:1px;' type='checkbox' name='BAOpt' onclick='usertypeMultiSelect(3)' value="2" title="HQA"/>HQA</label></li>
			        <li class="hide BALi"><label class='checkbox mb0 mt0'><input class="BAMClass" style='margin-top:1px;' type='checkbox' name='BAOpt' onclick='usertypeMultiSelect(3)' value="3" title="BA"/>BA</label></li>
			       </c:if>
			        <li class="hide DALi"><label class='checkbox mb0 mt0'><input class="DAMClass" style='margin-top:1px;' type='checkbox' name='BAOpt' onclick='usertypeMultiSelect(3)' value="4" title="DA"/>DA</label></li>
			        <li class="hide SALi"><label class='checkbox mb0 mt0'><input class="SAMClass" style='margin-top:1px;' type='checkbox' name='BAOpt' onclick='usertypeMultiSelect(3)' value="5" title="SA"/>SA</label></li>
			        </ul>
                </dd>
			</dl>
        </div>
		<div class="col-sm-12 col-md-12">
			<label class="top5 mb0"><spring:message code="lblTemplateName"/><span class="required">*</span></label>
		    <input type="text" class="form-control" id="btempleteName" name="btempleteName">
	   </div>
	   <div class="col-sm-12 col-md-12">
			<label class="top5 mb0"><spring:message code="msgSubjectLine"/><span class="required">*</span></label>
		    <input type="text" class="form-control" id="bsubjectLine" name="bsubjectLine">
	   </div>
	   <div class="col-sm-12 col-md-12">
			<label class="top5 mb0"><spring:message code="lblEmailText"/><span class="required">*</span></label>
			<div id="bemailText">
		    <textarea  class="form-control" name="bemailText1" id="bemailText1" maxlength="2500" rows="2"></textarea>
		    </div>
	   </div>
	   <div class="col-sm-12 col-md-12">
		   <div class="row">
				 <div class="col-sm-2 col-md-2 mt10" style="width:19%;">
					 <label class="top5 mb0"><span><spring:message code="lblStatus"/></span><span class="required">*</span> <span data-toggle="tooltip" data-placement="left" title="<spring:message code="lblStatus"/>"><img src="images/qua-icon.png" alt=""></span></label>
				 </div>
				<div class="col-sm-1 col-md-1">
						<label class="radio"><input type="radio" name="bcommStatus" id="bactive" value="1" checked>
						<spring:message code="optAct"/> </label>	
				</div>
				<div class="col-sm-1 col-md-1">
						<label class="radio"><input type="radio" name="bcommStatus" id="binactive" value="0">
						<spring:message code="optInActiv"/> </label>	
				</div>
		  </div> 
	  </div>
	  <div class="col-sm-12 col-md-12">
	  <div class="row">
	       <div class="col-sm-2 col-md-2" style="width:19%;">
					 <label class="top5 mb0"><span><spring:message code="lblSetasdefault"/></span> <span data-toggle="tooltip" data-placement="left" title="<spring:message code="lblSetasdefault"/>"><img src="images/qua-icon.png" alt=""></span></label>
		   </div>
		   <div class="col-sm-1 col-md-1">
				<input class="radio" type="checkbox" name="bsetDefault" id="bsetDefault">	
		  </div>
	   </div>
	  </div>
	</div>
  </c:if>
		<div class="cType3Div hide top15">
		<div class='row top5' style='background-color: #007fb2; font-weight: bold;padding-top: 5px; color: white;  padding-left: 2px;height: 30px;width:100%;margin-left:0px;'>
			<div class='col-sm-12 col-md-12 pleft0'><spring:message code="lblCommunicationtemplateforDA"/></div>
		</div>
		<div class="col-sm-12 col-md-12">
		<input type="hidden" id="DAUserId"/>
			<label class="top5 mb0"><spring:message code="lblUserType"/><span class="required">*</span></label>
		   <dl class="dropdownDA mt0"> 
			    <dt>
				    <a href="javascript:void(0);" style="width:100%">
				      <span class="hidaDA" style="font-family: 'Century Gothic','Open Sans', sans-serif;padding-top: 4px;"><spring:message code="msgSelectUsertype"/></span>    
				      <p class="multiSelDA" style="margin-bottom: -8px;"></p>  
				    </a>
			    </dt>
			    <dd>
			        <ul class="mutliSelectDA" style="width:100%">
			        <li class="hide CLi"><label class='checkbox mb0 mt0'><input class="CAMClass" style='margin-top:1px;' type='checkbox' name='DAOpt' onclick='usertypeMultiSelect(4)' value="1" title="Candidate"/>Candidate</label></li>
			       <c:if test="${entityType ne 2}">
			        <li class="hide HQALi"><label class='checkbox mb0 mt0'><input class="HQAMClass" style='margin-top:1px;' type='checkbox' name='DAOpt' onclick='usertypeMultiSelect(4)' value="2" title="HQA"/>HQA</label></li>
			        <li class="hide BALi"><label class='checkbox mb0 mt0'><input class="BAMClass" style='margin-top:1px;' type='checkbox' name='DAOpt' onclick='usertypeMultiSelect(4)' value="3" title="BA"/>BA</label></li>
			       </c:if>
			        <li class="hide DALi"> <label class='checkbox mb0 mt0'><input class="DAMClass" style='margin-top:1px;' type='checkbox' name='DAOpt' onclick='usertypeMultiSelect(4)' value="4" title="DA"/>DA</label></li>
			        <li class="hide SALi"><label class='checkbox mb0 mt0'><input class="SAMClass" style='margin-top:1px;' type='checkbox' name='DAOpt' onclick='usertypeMultiSelect(4)' value="5" title="SA"/>SA</label></li>
			        </ul>
                </dd>
			</dl>
	   </div>
		<div class="col-sm-12 col-md-12">
			<label class="top5 mb0"><spring:message code="lblTemplateName"/><span class="required">*</span></label>
		    <input type="text" class="form-control" id="dtempleteName" name="dtempleteName">
	   </div>
	   <div class="col-sm-12 col-md-12">
			<label class="top5 mb0"><spring:message code="msgSubjectLine"/><span class="required">*</span></label>
		    <input type="text" class="form-control" id="dsubjectLine" name="dsubjectLine">
	   </div>
	   <div class="col-sm-12 col-md-12">
			<label class="top5 mb0"><spring:message code="lblEmailText"/><span class="required">*</span></label>
			<div id="demailText">
		    <textarea  class="form-control" name="demailText1" id="demailText1" maxlength="2500" rows="2"></textarea>
		    </div>
	   </div>
	   <div class="col-sm-12 col-md-12">
		   <div class="row">
				 <div class="col-sm-2 col-md-2 mt10" style="width:19%;">
					 <label class="top5 mb0"><span><spring:message code="lblStatus"/></span><span class="required">*</span> <span data-toggle="tooltip" data-placement="left" title="<spring:message code="lblStatus"/>"><img src="images/qua-icon.png" alt=""></span></label>
				 </div>
				<div class="col-sm-1 col-md-1">
						<label class="radio"><input type="radio" name="dcommStatus" id="dactive" value="1" checked>
						<spring:message code="optAct"/> </label>	
				</div>
				<div class="col-sm-1 col-md-1">
						<label class="radio"><input type="radio" name="dcommStatus" id="dinactive" value="0">
						<spring:message code="optInActiv"/> </label>	
				</div>
		  </div> 
	  </div>
	  <div class="col-sm-12 col-md-12">
	  <div class="row">
	       <div class="col-sm-2 col-md-2" style="width:19%;">
					 <label class="top5 mb0"><span><spring:message code="lblSetasdefault"/></span> <span data-toggle="tooltip" data-placement="left" title="<spring:message code="lblSetasdefault"/>"><img src="images/qua-icon.png" alt=""></span></label>
		   </div>
		   <div class="col-sm-1 col-md-1">
				<input class="radio" type="checkbox" name="dsetDefault" id="dsetDefault">	
		  </div>
	   </div>
	  </div>
	</div>
		<div class="cType4Div hide top15">
		<div class='row top5' style='background-color: #007fb2; font-weight: bold;padding-top: 5px; color: white;  padding-left: 2px;height: 30px;width:100%;margin-left:0px;'>
			<div class='col-sm-12 col-md-12 pleft0'><spring:message code="lblCommunicationtemplateforSA"/></div>
		</div>
		<div class="col-sm-12 col-md-12">
		<input type="hidden" id="SAUserId"/>
			<label class="top5 mb0"><spring:message code="lblUserType"/><span class="required">*</span></label>
			<dl class="dropdownSA mt0"> 
			    <dt>
				    <a href="javascript:void(0);" style="width:100%">
				      <span class="hidaSA" style="font-family: 'Century Gothic','Open Sans', sans-serif;padding-top: 8px;padding-left: 5px;"><spring:message code="msgSelectUsertype"/></span>    
				      <p class="multiSelSA" style="margin-bottom: -8px;"></p>  
				    </a>
			    </dt>
			    <dd>
			        <ul class="mutliSelectSA" style="width:100%">
			        <li class="hide CLi"><label class='checkbox mb0 mt0'><input class="CAMClass" style='margin-top:1px;' type='checkbox' name='SAOpt' onclick='usertypeMultiSelect(5)' value="1" title="Candidate"/>Candidate</label></li>
			       <c:if test="${entityType ne 2}">
			        <li class="hide HQALi"><label class='checkbox mb0 mt0'><input style='margin-top:1px;' class="HQAMClass" type='checkbox' name='SAOpt' onclick='usertypeMultiSelect(5)' value="2" title="HQA"/>HQA</label></li>
			        <li class="hide BALi"><label class='checkbox mb0 mt0'><input style='margin-top:1px;' class="BAMClass" type='checkbox' name='SAOpt' onclick='usertypeMultiSelect(5)' value="3" title="BA"/>BA</label></li>
			       </c:if>
			        <li class="hide DALi"><label class='checkbox mb0 mt0'><input style='margin-top:1px;' class="DAMClass"type='checkbox' name='SAOpt' onclick='usertypeMultiSelect(5)' value="4" title="DA"/>DA</label></li>
			        <li class="hide SALi"><label class='checkbox mb0 mt0'><input style='margin-top:1px;' class="SAMClass"type='checkbox' name='SAOpt' onclick='usertypeMultiSelect(5)' value="5" title="SA"/>SA</label></li>
			        </ul>
                </dd>
			</dl>
        </div>
		<div class="col-sm-12 col-md-12">
			<label class="top5 mb0"><spring:message code="lblTemplateName"/><span class="required">*</span></label>
		    <input type="text" class="form-control" id="stempleteName" name="stempleteName">
	   </div>
	   <div class="col-sm-12 col-md-12">
			<label class="top5 mb0"><spring:message code="msgSubjectLine"/><span class="required">*</span></label>
		    <input type="text" class="form-control" id="ssubjectLine" name="ssubjectLine">
	   </div>
	   <div class="col-sm-12 col-md-12">
			<label class="top5 mb0"><spring:message code="lblEmailText"/><span class="required">*</span></label>
			<div id="semailText">
		    <textarea  class="form-control" name="semailText1" id="semailText1" maxlength="2500" rows="2"></textarea>
		    </div>
	   </div>
	   <div class="col-sm-12 col-md-12">
		   <div class="row">
				 <div class="col-sm-2 col-md-2 mt10" style="width:19%;">
					 <label class="top5 mb0"><span><spring:message code="lblStatus"/></span><span class="required">*</span> <span data-toggle="tooltip" data-placement="left" title="<spring:message code="lblStatus"/>"><img src="images/qua-icon.png" alt=""></span></label>
				 </div>
				<div class="col-sm-1 col-md-1">
						<label class="radio"><input type="radio" name="scommStatus" id="sactive" value="1" checked>
						<spring:message code="optAct"/> </label>	
				</div>
				<div class="col-sm-1 col-md-1">
						<label class="radio"><input type="radio" name="scommStatus" id="sinactive" value="0">
						<spring:message code="optInActiv"/> </label>	
				</div>
		  </div> 
	  </div>
	  <div class="col-sm-12 col-md-12">
	  <div class="row">
	       <div class="col-sm-2 col-md-2" style="width:19%;">
					 <label class="top5 mb0"><span><spring:message code="lblSetasdefault"/></span> <span data-toggle="tooltip" data-placement="left" title="<spring:message code="lblSetasdefault"/>"><img src="images/qua-icon.png" alt=""></span></label>
		   </div>
		   <div class="col-sm-1 col-md-1">
				<input class="radio" type="checkbox" name="ssetDefault" id="ssetDefault">	
		  </div>
	   </div>
	  </div>
	</div>
	<div class="row mt25">
				<div class="col-sm-2 col-md-2 hide multicandidateTypeBTN" >
				   <button onclick="saveAndExitCommunication();" class="flatbtn" style="background: #F89507;width:100%">
			           <strong><spring:message code="btnSaveExit"/> <i class="fa fa-check-circle"></i></strong>
		            </button>
				</div>
				<div class="col-sm-2 col-md-2" id="communicationCancelbtn">
				<button onclick="cancelCommunicationDiv(2);" class="flatbtn managestatusbtn" style="background: #da4f49;width:85%" >
          			<strong><spring:message code="btnClr"/> <i class="fa fa-times-circle"></i></strong>
          			</button>
				</div>
				<div class="col-sm-5 col-md-5"></div>
		        <div class="col-sm-3 col-md-3 hide multicandidateTypeBTN" style="text-align: right;">
			    <button onclick="saveAndContinueCommunication();" class="flatbtn" style="background: #9fcf68;width:90%" id="saveandContine" title="Save And Continue">
			  <strong><spring:message code="btnSaveAndNextDSPQ"/>  <i class="fa fa-check-circle"></i></strong>
		    </button>
		</div>
			</div>
	<div class="row mt25"></div>
   </div>
</div>
</div>
</div>

<style>

.uparrowdiv{
min-height:40px; /*min height of DIV should be set to at least 2x the width of the arrow*/
background: white;
position:absolute;
word-wrap:break-word;
-moz-border-radius:5px; /*add some nice CSS3 round corners*/
-webkit-border-radius:5px;
border-radius:5px;
margin-bottom:2em;
border-color: gray;
border-style: solid;
border-width: 1px;
z-index: 10;
}

.uparrowdiv:after{ /*arrow added to uparrowdiv DIV*/
content:'';
display:block;
position:absolute;
top:-20px; /*should be set to -border-width x 2 */
left:70px;
width:0;
height:0;
border-color: transparent transparent white transparent; /*border color should be same as div div background color*/
border-style: solid;
border-width: 10px;

}
.dspq-dialog {
    left: 50%;
    right: auto;
    width: 810px;
    padding-top: 0px;
    padding-left:10px;
    padding-right:10px; 
    padding-bottom: 30px;
    margin-left: auto;
      margin-right: auto;
      z-index: 1050;
  }
</style>

  <div  class="modal hide "  id="alertOnDuplicate"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="table-responsive">
		<div class='dspq-dialog' style="width:40%">
			<div class='modal-content'>
			<div class="modal-header">
		  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
				<h3><span  style="margin-left: 0px;">Confirmation</span></h3>				
			</div>
			<div class="modal-body">
						<div class="row">
							<div class="col-sm-12 col-md-12" id="duplicatePort"></div>							
						</div>
		 	<div style="text-align: right;">
		 		<button  class="flatbtn" style="width: 15%;" onclick="cancelDuplicatePort();"><strong>Ok <i class="icon"></i></strong></button>&nbsp;
		 	</div>
		  </div>
		 </div>
	</div>
</div>
</div> 

<script type="text/javascript">	
if($("#entityTypeID").val()!=2){
setEntityType();
}
 displayOnBoarding();
</script>

<script type="text/javascript" src="js/color/bootstrap-formhelpers.min.js"></script>
<link rel="stylesheet" href="css/color/bootstrap-formhelpers.min.css">
<link rel="stylesheet" href="css/color/boot.css">
<input type="hidden" id="checkPointName"/>
<div  class="modal hide"  id="manageCheckPoint"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" style="background: rgba(153, 153, 153, 0.8);z-index: 1050;" >
 <div class="table-responsive">
   <div class="row" style="width:70%;margin:0px 0px 0px 205px;background-color:#FFFFFF;">
	 <div class="col-sm-12 col-md-12" style="box-shadow: 0px 0px 0px 0px #999; background-color: #f5f5f5;" >
      <div class="row" >
        <div class="col-sm-1 col-md-1" style="background-color: rgb(239, 95, 95);height: 35px;">
         	<img src="images/Recruitingdspq1.png" alt="" style="margin-top: 7px;margin-left: 5px;">
         </div>        
         <div class="col-sm-10 col-md-10" style="padding-left: 0px;">
         	<div style="font-size: 13px;"><label class="subheading mb0"><spring:message code="lblCheckpointStatus" /></label> - <label class="subheading mb0" id="dashboardname"></label></div>	
         </div>
		 <div id="AddOnboarding" class="col-sm-1 col-md-1" style="line-height: 34px;text-align: right;">
					<a href="javascript:void(0);" onclick="cancelCheckPointDiv();"><b style="font-size: 15px;">X</b></a>	
		 </div>
      </div>
      </div>
      <div class="left25" style="width:95%;">
         <div class="row mt10" style="padding-left:16px;">
				    
				    <div class="col-sm-6 col-md-6 pleft0">
				       <label class="circleHead grey" style="margin-left:0px;" ><spring:message code="instD" /></label> <spring:message code="lblNotselectedasDecisionPoint" />&nbsp;&nbsp;
				       <label class="circleHead green" style="margin-left:0px;"><spring:message code="instD" /></label> <spring:message code="lblSelectedasDecisionPoint" />
				    </div>
				    <div class="col-sm-4 col-md-4 pleft0">
				       <label class="circleHead grey" style="margin-left:0px;" ><spring:message code="instC" /></label> <spring:message code="lblNoColordefined" />&nbsp;&nbsp;
				       <label class="circleHead green" style="margin-left:0px;"><spring:message code="instC" /></label> <spring:message code="lblColordefined" />
				    </div>
				    <div class="col-sm-2 col-md-2 pleftright">
				      <label class="circleHead grey" style="margin-left:0px;"><spring:message code="instX" /></label> <spring:message code="optInActiv" />&nbsp;&nbsp;
				       <label class="circleHead green" style="margin-left:0px;" ><spring:message code="instA" /></label> <spring:message code="optAct" />
		            </div>
        </div>
  <input type="hidden" id="checkPointStatusId"/>
     <div class="row">
		 <div class='col-sm-12 col-md-12 pleftright' style="text-align:right;">
			<a href="javascript:void(0);" onclick="showAddStatus();">+<spring:message code="lblAddStatus" /></a>	
		    <span href="#"  data-toggle="tooltip" data-placement="left" style="cursor: pointer" title="Add Status"><img src="images/qua-icon.png" alt=""></span>
		</div>
	</div>
     <div class='row top5' style='background-color: #007fb2; font-weight: bold;padding-top: 5px; color: white;  padding-left: 2px;height: 30px;'>
		<div class='col-sm-12 col-md-12 pleft0'><spring:message code="lblCheckpointStatus" /></div>
	</div>
	<div class="hide" id="checkpointstatus"></div>
       <div class="hide" id="addStatus">  
        <div class="row mb10">
		<div class="col-sm-12 col-md-12">
			<div class='divErrorMsg' id='errordivStatus' style="padding-left: 2px; padding-top: 10px;"></div>
		</div>
	  </div>
      	<div class="row">
		 <div class="col-sm-2 col-md-2 mt10" style="width:19%;">
			<label class="mb0"><span>  <spring:message code="lblStatusName" /></span> <span class="required">*</span> <span data-toggle="tooltip" data-placement="left" title=" <spring:message code="lblStatusName" />"><img src="images/qua-icon.png" alt=""></span></label>
		</div>
	    <div class="col-sm-4 col-md-4">
			<input type="text" id="statusname" name="statusname" class="form-control mt5" maxlength="80">
		</div>
		
	</div>
	<div class="row">
	 <div class="col-sm-2 col-md-2" style="margin-top:7px;width:19%;">
		 <label><span><spring:message code="lblThisisaDecisionPoint"/></span> <span class="required">*</span> <span data-toggle="tooltip" data-placement="left" title=" <spring:message code="lblThisisaDecisionPoint"/>"><img src="images/qua-icon.png" alt=""></span></label>
	 </div>
		<div class="col-sm-1 col-md-1" style="width:0%;">
				<label class="checkbox"><input type="checkbox" name="decisionpoint" id="decisionpoint"> </label>	
		</div>
	</div> 
	<div class="row">
	 <div class="col-sm-2 col-md-2" style="margin-top:7px;width:19%;">
		 <label><span><spring:message code="lblColor"/></span> <span class="required">*</span> <span data-toggle="tooltip" data-placement="left" title=" <spring:message code="lblColor"/>"><img src="images/qua-icon.png" alt=""></span></label>
	 </div>
		<div class="col-sm-4 col-md-4">
		  <div class="bfh-colorpicker" data-name="colorpicker3" data-close="false"></div>
		</div>
	</div> 
	<div class="row">
	 <div class="col-sm-2 col-md-2 mt10" style="width:19%;">
		 <label><span><spring:message code="lblStatus"/></span><span class="required">*</span> <span data-toggle="tooltip" data-placement="left" title="<spring:message code="lblStatus"/>"><img src="images/qua-icon.png" alt=""></span></label>
	 </div>
		<div class="col-sm-1 col-md-1">
				<label class="radio"><input type="radio" name="status" id="active" value="1" checked>
				<spring:message code="optAct"/> </label>	
		</div>
		<div class="col-sm-1 col-md-1">
				<label class="radio"><input type="radio" name="status" id="inactive" value="0">
				<spring:message code="optInActiv"/> </label>	
		</div>
	</div> 
	<div class="row mb30">
        	<div class="col-sm-6 col-md-6">
        	  <label>
               		<a href='javascript:void(0);' onclick="addCheckStatus();"> <spring:message code="lnkImD"/> </a> &nbsp;&nbsp;
               		<a href='javascript:void(0);' onclick="hidecheckStatus();"> <spring:message code="btnClr"/> </a>
              </label>
        	</div>
        	</div> 
	</div>  
		<div class="row">
				<div class="col-sm-3 col-md-3 hide addstatusbtn">
				<button onclick="checkPointSaveAndExit();" class="flatbtn" style="background: #F89507;width:100%;">
			           <strong><spring:message code="btnSaveExit"/> <i class="fa fa-check-circle"></i></strong>
		            </button>
				</div>
				<div class="col-sm-2 col-md-2" >
				<button onclick="cancelCheckPointDiv();" class="flatbtn managestatusbtn" style="background: #da4f49;width:100%;">
          			<strong><spring:message code="btnClr"/> <i class="fa fa-times-circle"></i></strong>
          			</button>
				</div>
				<div class="col-sm-1 col-md-1"></div>
				<div class="col-sm-6 col-md-6 hide addstatusbtn" style="text-align: right;padding-right: 28px;">				
					<button onclick="checkPointSaveAndContinue();" class="flatbtn" style="background: #9fcf68; width: 45%;">
		               <strong><spring:message code="btnSaveAndNextDSPQ"/> <i class="icon"></i></strong>
            	  </button>	
				</div>
			</div>
	<div class="row" style="margin-top:15px;"></div>
   </div>
</div>
</div>
</div>
<!--start by siva(Manage edit onboard)-->
<div class="modal hide" id="editmanageCheckPoint" style="display:none;" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" style="display: block; background: rgba(153, 153, 153, 0.8);">
 <div class="table-responsive">
 <div class="row" style="width:70%;margin:0px 0px 0px 217px;background-color:#FFFFFF;height:auto;">
	 <div class="col-sm-12 col-md-12" style="box-shadow: 0px 0px 0px 0px #999; background-color: #f5f5f5;" >
      <div class="row" >
        <div class="col-sm-1 col-md-1" style="background-color: rgb(239, 95, 95);height: 35px;">
         	<img src="images/Recruitingdspq1.png" alt="" style="margin-top: 7px;margin-left: 5px;">
         </div>        
         <div class="col-sm-10 col-md-10" style="padding-left: 0px;">
         	<div style="font-size: 13px;"  id="subheadingtext"><label class="subheading mb0">Edit CheckPoint</label> - <label class="subheading mb0" id="editcheckPointname" style="margin-left: 0px;"></label></div>	
         </div>
		 <div id="AddOnboarding" class="col-sm-1 col-md-1" style="line-height: 34px;text-align: right;">
					<a href="javascript:void(0);" onclick="canceleditdiv();"><b style="font-size: 15px;">X</b></a>	
		 </div>
      </div>
      </div>
<div id="checkPointMasterSection">
<div id="candidateTypeDiv">
<div class="row top10 pleft30" style="width:99%;">
					<div class="col-sm-2 col-md-2" style="height: 42px; border: 1px rgb(213, 213, 213) solid; border-left: 0px;padding-left:0px;">
						<label style="margin-top: 10px; font-weight: bold;">
							Candidate Type
							</label>
					</div>
					<div class="col-sm-10 col-md-10" style="padding-right: 0px;">
						<div class="row" style="border: 1px rgb(213, 213, 213) solid; border-left: 0px; border-right: 0px;">
							<div class="col-sm-4 col-md-4 checkbox" style="padding-left: 30px;">
							<label class="checkbox inline" style="margin-top: 2px;margin-bottom: 2px;margin-left: -25px;">
								<input type="checkbox" id="editsection0"  checked="checked" disabled="disabled">
									External (E)</label>
							</div>
							<div class="col-sm-4 col-md-4 checkbox" style="margin-top: 10px;">
							<label class="checkbox inline" style="margin-top: 2px;margin-bottom: 2px;margin-left: -25px;">
								<input type="checkbox" id="editsection1" class="section1" value="I"> 
									Internal (I)</label>
							</div>
							<div class="col-sm-4 col-md-4 checkbox" style="margin-top: 10px;">
							<label class="checkbox inline" style="margin-top: 2px;margin-bottom: 2px;margin-left: -25px;">
								<input type="checkbox"  id="editsection2" class="section2">
									Internal Transfer (IT)</label>
							</div>
						</div>
					</div>
					</div>
				</div>
			
		          <div id="editExternalDiv">
					<div class="top5" style="background-color: #007fb2; font-weight: bold;padding-top: 5px; color: white;  padding-left: 2px;height: 30px;margin-left: 10px;margin-right: 10px;">
					<div class="col-sm-12 col-md-12" style="padding-left: 0px;" id="chkCandidateType">
							External
						</div>
					</div>
					<div class="mt5 hide DSPQRequired12" id="checkPointError" style="margin-left: 2%;"></div>
					<div class="row mb30" style="margin-left: 4.1%" id="uploadimagetxt" name="uploadimagetxt">
					</div>
					<div id="editExternalCandidate" style="padding-left: 30px;margin-top: -10px;">
					<div class="row top5">
						<div class="col-sm-3 col-md-3" style="padding:0px 0px 0px 25px;">
			<label class="mb5" id="lblchkPointName">Checkpoint Name<span class="required">*</span> 
			</label>
			</div>
			<div class="col-sm-9 col-md-9" style="width: 70.5%;" id="checkPointName">
			<input type="text" id="txtchkpointname" class="form-control" name="txtchkpointname">
			</div>	
					</div>
					<div class="row top5">
						<div class="col-sm-3 col-md-3" style="padding:0px 0px 0px 25px;">
			<label class="mb5" id="lblchkType">Checkpoint Type<span class="required">*</span> 
			</label>
			</div>
			<div class="col-sm-9 col-md-9" style="width: 70.5%;">
			<select id='checkPointTypeSelect' name='checkPointTypeSelect' onchange='checkPointTypeChange();' class='form-control'>
			<option value='' selected='true'>Select Checkpoint Type</option>
			<option value="22">Check box</option>
			<option value="20">Date</option>
			<option value="4">Single Line Text</option>
			<option value="5">Multiple Line Text</option>
			<option value="23">Document Load</option>
			<option value="2">Radio Button</option>
			</select>
			</div>	
		</div>
	<!--<form:form method="post" action="multipleDocumentUpload.do" id='multipleDocumentForm' target='multipleDocumentFormTarget'  modelAttribute="uploadDocument" enctype="multipart/form-data">-->
					<div class="row" id="editUploadDoc" style="display: none;">
						<div class="col-sm-12 col-md-12" style="padding:8px 0px 0px 25px;">
			<label class="mb5" id="lbluploaddoc">Upload Document<span class="required">*</span> 
			</label>
			<span href="javascript:void(0);"  data-toggle="tooltip" data-placement="left" title="Upload Document">
				<img src="images/qua-icon.png" alt="">					
			</span> 
			</div>
			<div class="col-sm-12 col-md-12">
					<div class="col-sm-7 col-md-7" style="margin-left: -4px;"><input type="text" id="uploadfilename1" name="uploadfilename1" readonly="readonly" class="form-control"></div>
					<div class="col-sm-2 col-md-2 mt10"><input type="file" onchange="afterFileupload(1);" id="documentName1" name="documentName1"></div>
					<div class="col-sm-2 col-md-2" style="margin-left:8%;"><a href="javascript:;" id="moredocslink">+Add More</a></div>
					</div>
			</div>
	<!--</form:form>-->
			<input type="hidden" id="inccounter" value="1">
			<div class="row" id="uploadmoredocsdiv">
			
			</div>
			<div class="row" id="saveupload" style="display:none;">
		<div class="col-sm-7 col-md-7 mt5" style="margin-left: 10px;">
			<a href="javascript:;" onclick="showUploadFiles();" style="font-size: 15px;"><spring:message code="lnkImD"/></a>
<a href="javascript:;" onclick="cancelUploadFiles();"  style="margin-left: 4%;font-size: 15px;"><spring:message code="btnClr"/></a>
			
		</div>
	</div>
							
						
						
					
					<div class="row" id="editDateType" style="display: none;">
						<div class="col-sm-12 col-md-12" style="padding:8px 0px 0px 25px;">
			<label class="mb10" id="lbldate">Question<span class="required"></span> 
			</label>
			<span href="javascript:void(0);"  data-toggle="tooltip" data-placement="left" title="Date">
									<img src="images/qua-icon.png" alt=""></span>
				<input type="text" id="date123" name="date123" class="form-control" style="width: 94%;">
			</div>
						
					</div>
					
					<div class="row" id="editSingleLineDiv" style="margin-left:-4px;margin-right:3px;display: none;">
							<div class="col-sm-12 col-md-12" style="padding:8px -2px 0px 25px;">
			<label class="mb10" id="lblsingle">Question<span class="required">*</span> 
			</label>
			<span href="javascript:void(0);"  data-toggle="tooltip" data-placement="left" title="Question">
									<img src="images/qua-icon.png" alt=""></span>
				<input type="text" id="txtSingleline" name="txtSingleline" class="form-control" style="width: 97%;">
			</div>
							</div>
							<div class="row" id="editMultipleLineDiv" style="margin-left:-4px;margin-right:3px;display: none;">
							<div class="col-sm-12 col-md-12" id="editMultipleLineLabel">
							<label id="lblmultiline">Question<span class="required">*</span></label>
							<span href="javascript:void(0);"  data-toggle="tooltip" data-placement="left" title="Question">
									<img src="images/qua-icon.png" alt="">					
								</span>
							
								<textarea  name="multiCheckPointQuestion" id="multiCheckPointQuestion" maxlength="2500" rows="8" cols="30" class="form-control" style="width: 88%;"></textarea>
							</div>
							</div>
							<div class="row" id="editRadioTypeDiv" style="display:none;margin-left: 0px;">
							<div class="row mb10">
									<div class="col-sm-12 col-md-12" style="padding:0px 0px 0px 25px;">
			<label class="mb10" id="lblrdo">Question<span class="required">*</span></label><span href="javascript:void(0);"  data-toggle="tooltip" data-placement="left" title="Question">
									<img src="images/qua-icon.png" alt="">					
								</span> 
			
			
			<input type="text" id="txtRdoboxQuestion" name="txtRdoboxQuestion" class="form-control" style="width: 92%;">
			</div></div>
							
							<div class="row pleft15" id="editOptionDiv" style="margin-left:-4px;margin-right:3px;">
								<div class="row">
								<div class="col-sm-12 col-md-12" id="editOptionsLabel">
							<label id="lblrdooptions">Options<span class="required">*</span></label>
							<span href="javascript:void(0);"  data-toggle="tooltip" data-placement="left" title="Options">
									<img src="images/qua-icon.png" alt="">					
								</span></div></div>
								<div class="row top5" id="row1" style="display: block;"><div class="col-sm-6 col-md-6"><label>1</label>&nbsp;
								<input type="text" name="rdo1" id="rdo1" class="form-control" style="display: inline-table;width: 80% !important;" maxlength="200" value="">
								<input type="radio" value="1"  id="crdo1" name="crdo1" class="crdo1">
								</div>
								<div class="col-sm-6 col-md-6"><label>2</label>&nbsp;
								<input type="text" name="rdo2" id="rdo2" class="form-control" style="display: inline-table;width: 80% !important;" maxlength="200" value="">
								<input type="radio" value="2" id="crdo2" name="crdo1" class="crdo1"/>
								</div></div>
								
								<div class="row top5" id="row2" style="display: block;">
								<div class="col-sm-6 col-md-6"><label style="width:10px;">3</label>&nbsp;<input type="text" name="rdo3" id="rdo3" class="form-control" style="display: inline-table;width: 80% !important;" maxlength="200">
								<input type="radio" value="3" id="crdo3" name="crdo1" class="crdo1"/>
								</div>
								<div class="col-sm-6 col-md-6"><label style="width:10px;">4</label>&nbsp;<input type="text" name="rdo4" id="rdo4" class="form-control" style="display: inline-table;width: 80% !important;" maxlength="200">
								<input type="radio" value="4" id="crdo4" name="crdo1" class="crdo1"/>
								</div></div>
								
								<div class="row top5" id="row3" style="display: block;">
								<div class="col-sm-6 col-md-6"><label style="width:10px;">5</label>&nbsp;<input type="text" name="rdo5" id="rdo5" class="form-control" style="display: inline-table;width: 80% !important;" maxlength="200">
								<input type="radio" value="5" id="crdo5" name="crdo1" class="crdo1"/>
								</div>
								<div class="col-sm-6 col-md-6"><label style="width:10px;">6</label>&nbsp;<input type="text" name="rdo6" id="rdo6" class="form-control" style="display: inline-table;width: 80% !important;" maxlength="200">
								<input type="radio" value="6" id="crdo6" name="crdo1" class="crdo1"/>
								</div></div>
								
								</div>
							</div>
							
							<div id="editCheckboxTypeDiv" style="display:none;">
								<div class="row mb10">
									<div class="col-sm-12 col-md-12" style="padding:8px 0px 0px 25px;">
			<label class="mb10" id="lblchkQuestion">Question<span class="required">*</span> 
			</label><span href="javascript:void(0);"  data-toggle="tooltip" data-placement="left" title="Question">
									<img src="images/qua-icon.png" alt="">					
								</span>
			<input type="text" id="txtChkboxQuestion" name="txtChkboxQuestion" class="form-control" style="width: 94%;">
			</div></div>
							<div class="row pleft15" id="editChkOptionsDiv">
							<div class="row">
								<div class="col-sm-4 col-md-4" id="editChkOptionsLabel" style="margin-top: 6px;margin-left:10px;">
							<label id="lblchkoptions">Options<span class="required">*</span>
							
							</label>
							<span href="javascript:void(0);"  data-toggle="tooltip" data-placement="left" title="Options">
									<img src="images/qua-icon.png" alt="">					
								</span>
							</div></div>
							<div class="row top5 pleft15" id="row1" style="display: block;"><div class="col-sm-6 col-md-6"><label>1</label>&nbsp;
								<input type="text" name="opt1" id="opt1" class="form-control" style="display: inline-table;width: 80% !important;" maxlength="200" value="">
								<input type="checkbox" id="chk1">
								</div>
								<div class="col-sm-6 col-md-6"><label>2</label>&nbsp;
								<input type="text" name="opt2" id="opt2" class="form-control" style="display: inline-table;width: 80% !important;" maxlength="200" value="">
								<input type="checkbox" id="chk2">
								</div></div>
								
								
								<div class="row top5 pleft15" id="row2" style="display: block;">
								<div class="col-sm-6 col-md-6"><label style="width:10px;">3</label>&nbsp;<input type="text" name="opt3" id="opt3" class="form-control" style="display: inline-table;width: 80% !important;" maxlength="200">
								<input type="checkbox" id="chk3">
								</div>
								<div class="col-sm-6 col-md-6"><label style="width:10px;">4</label>&nbsp;<input type="text" name="opt4" id="opt4" class="form-control" style="display: inline-table;width: 80% !important;" maxlength="200">
								<input type="checkbox" id="chk4">
								</div></div>
								
								<div class="row top5 pleft15" id="row3" style="display: block;">
								<div class="col-sm-6 col-md-6"><label style="width:10px;">5</label>&nbsp;<input type="text" name="opt5" id="opt5" class="form-control" style="display: inline-table;width: 80% !important;" maxlength="200">
								<input type="checkbox" id="chk5">
								</div>
								<div class="col-sm-6 col-md-6"><label style="width:10px;">6</label>&nbsp;<input type="text" name="opt6" id="opt6" class="form-control" style="display: inline-table;width: 80% !important;" maxlength="200">
								<input type="checkbox" id="chk6">
								</div></div>
							</div>
							</div>
					<div class="row top0">
						<div class="col-sm-3 col-md-3" style="padding:0px 0px 0px 25px;">
			<label class="mt10">Communication  Required 
			</label>
			</div>
			<div class="col-sm-1 col-md-1">
			<label class="checkbox">
			<input type="checkbox" id="communicatinreq" name="communicatinreq" style="background-color: #428bca;color: white;">
			</label>
			</div>
							
						
						
					</div>
					<div class="row top0">
						<div class="col-sm-3 col-md-3" style="padding:0px 0px 0px 25px;">
			<label class="mb0" id="lblActionPerformedby">Action Performed By<span class="required">*</span> 
			</label>
			</div>
			<div class="col-sm-2 col-md-2" style="width: 20%;">
			<label class="checkbox mt0">
				<input type="checkbox" name="candidate" id="candidate" value="1" style="background-color: #428bca;color: white;">
				Candidate</label>	
		</div>
		<div class="col-sm-2 col-md-2" style="margin-left: -8.2%;">
		<label class="checkbox mt0">
				<input type="checkbox" name="user" id="user" value="0"  style="background-color: #428bca;color: white;">
				User</label>
		</div>
			
							
						
						
					</div>
					<div class="row">
						<div class="col-sm-3 col-md-3" style="padding:0px 0px 0px 25px;">
			<label class="mb0" id="lblstatus">Status <span class="required">*</span>
			</label>
			</div>
			<div class="col-sm-1 col-md-1" style="width: 12%;">
				<label class="radio mt0"><input type="radio" name="statusE" id="activeE" value="1" class="status">
				Active </label>	
		</div>
			<div class="col-sm-1 col-md-1">
			<label class="radio mt0">
			<input type="radio" name="statusE" value="0" class="status" id="inactivE">Inactive
			</label>
			</div>	
					</div>
					</div></div>
					<div id="editchkpointdiv" style="margin-top:-16px;">
						<div class="col-sm-12 col-md-12 pleft50" style="text-align:right;">
			<a href="javascript:void(0);" id="editaddstatus">+Add Status</a>	
		    <span href="#" data-toggle="tooltip" data-placement="left" style="cursor: pointer" title="" data-original-title="Add Status"><img src="images/qua-icon.png" alt=""></span>
		</div>
		
<div class=" row top5" style="background-color: #007fb2; font-weight: bold;padding-top: 5px; color: white;  padding-left: 2px;height: 30px;margin-left: 10px;margin-right: 10px;">
					<div class="col-sm-12 col-md-12" style="padding-left: 0px;">
							Checkpoint Status
						</div>
					</div>
					<br/>
					
			<div id="checkPointStatusDuplicatError" class="hide DSPQRequired12 pleft25" ></div>		
<div id="checkPointStatusRecord" style="padding-left: 30px;"></div><br/>
 <div class="hide" id="editAddStatus" style="margin-left: 4%;">  
        <div class="row">
		<div class="col-sm-12 col-md-12">
			<div class='divErrorMsg' id='errordivStatus' style="padding-left: 2px; padding-top: 10px;"></div>
		</div>
	  </div>
      	<div class="row">
		 <div class="col-sm-2 col-md-2 mt10" style="width:19%;">
			<label class="mb0"><span>  <spring:message code="lblStatusName" /></span> <span class="required">*</span> <span data-toggle="tooltip" data-placement="left" title=" <spring:message code="lblStatusName" />"><img src="images/qua-icon.png" alt=""></span></label>
		</div>
	    <div class="col-sm-4 col-md-4">
			<input type="text" id="statusname2" name="statusname2" class="form-control mt5" maxlength="80">
		</div>
		
	</div>
	<div class="row">
	 <div class="col-sm-2 col-md-2" style="margin-top:7px;width:19%;">
		 <label><span><spring:message code="lblThisisaDecisionPoint"/></span> <span class="required">*</span> <span data-toggle="tooltip" data-placement="left" title=" <spring:message code="lblThisisaDecisionPoint"/>"><img src="images/qua-icon.png" alt=""></span></label>
	 </div>
		<div class="col-sm-1 col-md-1" style="width:0%;">
				<label class="checkbox"><input type="checkbox" name="decisionpoint2" id="decisionpoint2"> </label>	
		</div>
	</div> 
	<div class="row">
	 <div class="col-sm-2 col-md-2" style="margin-top:7px;width:19%;">
		 <label><span><spring:message code="lblColor"/></span> <span class="required">*</span> <span data-toggle="tooltip" data-placement="left" title=" <spring:message code="lblColor"/>"><img src="images/qua-icon.png" alt=""></span></label>
	 </div>
		<div class="col-sm-4 col-md-4">
		  <div class="bfh-colorpicker" data-name="colorpicker2" data-close="false"></div>
		</div>
	</div> 
	<div class="row">
	 <div class="col-sm-2 col-md-2 mt10" style="width:19%;">
		 <label><span><spring:message code="lblStatus"/></span><span class="required">*</span> <span data-toggle="tooltip" data-placement="left" title="<spring:message code="lblStatus"/>"><img src="images/qua-icon.png" alt=""></span></label>
	 </div>
		<div class="col-sm-1 col-md-1">
				<label class="radio"><input type="radio" name="status2" id="active2" value="1" checked>
				<spring:message code="optAct"/> </label>	
		</div>
		<div class="col-sm-1 col-md-1">
				<label class="radio"><input type="radio" name="status2" id="inactive2" value="0">
				<spring:message code="optInActiv"/> </label>	
		</div>
		
	</div> 
	<div class="row">
		<div class="col-sm-7 col-md-7">
			<a href="javascript:;" onclick="addCheckPointStatusByJS();" style="font-size: 15px;"><spring:message code="lnkImD"/></a>
<a href="javascript:;" id="editCancelAddStatus" style="margin-left: 4%;font-size: 15px;"><spring:message code="btnClr"/></a>
			
		</div>
	</div>
	 
	</div>  
<div>
			</div>

					</div>

<div id="editchykpermissionsdiv">
<div class="top5" style="background-color: #007fb2; font-weight: bold;padding-top: 5px; color: white;  padding-left: 2px;height: 30px;margin-left: 10px;margin-right: 10px;">
					<div class="col-sm-12 col-md-12" style="padding-left: 0px;">
							Access Permissions
						</div>
					</div><br/>

<div id="editCheckPointPermissionRecord" style="padding-left: 15px;margin-top: -17px;"></div>
<div class="row mt30 mb10 left50" style="margin-left: 2%;">
				<div class="col-sm-2 col-md-2" style="padding-left: 0px;">
				<button onclick="saveAndUpdateCheckPoint();" class="flatbtn" style="background: #F89507;width:100%;" id="saveAddEdit">
			           <strong><spring:message code="btnSaveExit"/> <i class="fa fa-check-circle"></i></strong>
		            </button>
				</div>
				<div class="col-sm-2 col-md-2 pleftright">
				<button onclick="canceleditdiv();" class="flatbtn managestatusbtn" style="background: #da4f49;width:70%;">
          			<strong><spring:message code="btnClr"/> <i class="fa fa-times-circle"></i></strong>
          			</button>
				</div>
			</div>
</div>

			</div>
</div>
</div>
</div>
<!--end by ssiva(Manage edit onboard)-->
 <div class="modal" id="validateconfirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" style="display: none;">		
		 <div class="modal-dialog">
          <div class="modal-content">
			<div class="modal-header">	  		
				<h3 id="myModalLabel"><spring:message code="headTm"/></h3>
			</div>
			<div class="modal-body">				
				<div class="control-group">
					<div class="" id="ddd">
						<p>Do you want to remove this document?</p>
						<input type="hidden" id="deletequestionid" value="">
					</div>
				</div>
		 	</div> 	
		 	<div class="modal-footer">
		 			<span><button class="btn btn-large btn-primary" onclick="yes();" value="Delete"><spring:message code="btnOk"/></button></span>
		 			<span><button class="btn" id="canceldelete" onclick="No();" data-dismiss="modal" aria-hidden="true">Cancel</button></span>
		 	</div>
		</div>
	  </div>
	</div>
<div  class="modal hide"  id="managePermission"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" style="background: rgba(153, 153, 153, 0.8);" >
 <div class="table-responsive">
   <div class="row" style="width:70%;margin:100px 0px 0px 205px;background-color:#FFFFFF;">
	 <div class="col-sm-12 col-md-12" style="box-shadow: 0px 0px 0px 0px #999; background-color: #f5f5f5;" >
      <div class="row" >
        <div class="col-sm-1 col-md-1" style="background-color: rgb(239, 95, 95);height: 35px;">
         	<img src="images/Recruitingdspq1.png" alt="" style="margin-top: 7px;margin-left: 5px;">
         </div>        
         <div class="col-sm-10 col-md-10" style="padding-left: 0px;">
         	<div style="font-size: 13px;"><label class="subheading mb0"><spring:message code="lblCheckpointPermission" /></label> - <label class="subheading mb0" id="checkPointNa" style="margin-left: 0px;"></label></div>	
         </div>
		 <div id="AddOnboarding" class="col-sm-1 col-md-1" style="line-height: 34px;text-align: right;">
					<a href="javascript:void(0);" onclick="cancelPermissionDiv();"><b style="font-size: 15px;">X</b></a>	
		 </div>
      </div>
      </div>
      <div class="left25" style="width:95%;">
       <input type="hidden" id="permissionId"/>
     <!--<div class='row' style='background-color: #007fb2; font-weight: bold; padding-top: 0px; color: white;  margin-left: 0px;width:100%;'>
		<div class='col-sm-12 col-md-12 pleft0'><spring:message code="lblCheckpointStatus" /></div>
	</div>
	--><div class="top10" id="permissionDiv"></div>
 
		<div class="row left10">
				<div class="col-sm-3 col-md-3">
				<button onclick="permissionSaveAndExit(0,0);" class="flatbtn" style="background: #F89507;width:100%;">
			           <strong><spring:message code="btnSaveExit"/> <i class="fa fa-check-circle"></i></strong>
		            </button>
				</div>
				<div class="col-sm-2 col-md-2" >
				<button onclick="cancelPermissionDiv();" class="flatbtn managestatusbtn" style="background: #da4f49;width:100%;">
          			<strong><spring:message code="btnClr"/> <i class="fa fa-times-circle"></i></strong>
          			</button>
				</div>
			</div>
	<div class="row" style="margin-top:15px;"></div>
   </div>
</div>
</div>
</div>

<div  class="modal hide"  id="moreCheckPoint"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" style="background: rgba(153, 153, 153, 0.8);" >
 <div class="table-responsive">
   <div class="row" style="width:70%;margin:100px 0px 0px 205px;background-color:#FFFFFF;">
	 <div class="col-sm-12 col-md-12" style="box-shadow: 0px 0px 0px 0px #999; background-color: #f5f5f5;" >
      <div class="row" >
        <div class="col-sm-1 col-md-1" style="background-color: rgb(239, 95, 95);height: 35px;">
         	<img src="images/Recruitingdspq1.png" alt="" style="margin-top: 7px;margin-left: 5px;">
         </div>        
         <div class="col-sm-10 col-md-10" style="padding-left: 0px;">
         	<div style="font-size: 13px;"><label class="subheading mb0"><spring:message code="lblOnboardingCheckpoints" /></label></div>	
         </div>
		 <div id="AddOnboarding" class="col-sm-1 col-md-1" style="line-height: 34px;text-align: right;">
			<a href="javascript:void(0);" onclick="cancelMoreCheckPointDiv();"><b style="font-size: 15px;">X</b></a>	
		 </div>
      </div>
   </div>
   <div class="row top10 left50">
   <div class="row">
		<div class="col-sm-12 col-md-12 mb10">
			<div class='divErrorMsg hide pleft0' id='errorDivCheckPoint'></div>
		</div>
	</div>
   <div class="row">
		 <div class="col-sm-2 col-md-2 mt10" style="width:19%;">
			<label class="mb0"><span>  <spring:message code="lblCheckpointName" /></span> <span class="required">*</span> <span data-toggle="tooltip" data-placement="left" title=" <spring:message code="lblCheckpointName" />"><img src="images/qua-icon.png" alt=""></span></label>
		</div>
	    <div class="col-sm-4 col-md-4">
			<input type="text" id="checkpointName" name="checkpointName" class="form-control mt5" maxlength="80">
		</div>
		
	</div>
   
   	<div class="row">
	 <div class="col-sm-2 col-md-2 mt10" style="width:19%;">
		 <label><span><spring:message code="lblActionNeeded"/></span><span class="required">*</span> <span data-toggle="tooltip" data-placement="left" title="<spring:message code="lblActionNeeded"/>"><img src="images/qua-icon.png" alt=""></span></label>
	 </div>
		<div class="col-sm-1 col-md-1">
				<label class="radio"><input type="radio" name="actionNeeded" id="aAction" value="1" checked>
				<spring:message code="optAct"/> </label>	
		</div>
		<div class="col-sm-1 col-md-1">
				<label class="radio"><input type="radio" name="actionNeeded" id="iAction" value="0">
				<spring:message code="optInActiv"/> </label>	
		</div>
	</div> 
		<div class="row">
	 <div class="col-sm-2 col-md-2 mt10" style="width:19%;">
		 <label><span><spring:message code="lblStatus"/></span><span class="required">*</span> <span data-toggle="tooltip" data-placement="left" title="<spring:message code="lblStatus"/>"><img src="images/qua-icon.png" alt=""></span></label>
	 </div>
		<div class="col-sm-1 col-md-1">
				<label class="radio"><input type="radio" name="checkStatus" id="statusActive" value="1" checked>
				<spring:message code="optAct"/> </label>	
		</div>
		<div class="col-sm-1 col-md-1">
				<label class="radio"><input type="radio" name="checkStatus" id="statusInactive" value="0">
				<spring:message code="optInActiv"/> </label>	
		</div>
	</div> 
	 </div>
		<div class="row mt10 mb30 left50">
				<div class="col-sm-2 col-md-2" style="padding-left: 0px;">
				<button onclick="saveCheckPoint();" class="flatbtn" style="background: #F89507;width:100%;">
			           <strong><spring:message code="btnSaveExit"/> <i class="fa fa-check-circle"></i></strongl>
		            </button>
				</div>
				<div class="col-sm-2 col-md-2 pleftright">
				<button onclick="cancelMoreCheckPointDiv();" class="flatbtn managestatusbtn" style="background: #da4f49;width:70%;">
          			<strong><spring:message code="btnClr"/> <i class="fa fa-times-circle"></i></strong>
          			</button>
				</div>
			</div>
		</div>
	</div>
</div>

<style>
.dropdownCA,.dropdownHQA,.dropdownBA,.dropdownDA,.dropdownSA{
margin-bottom: 0px;
}
.dropdownCA dd, .dropdownCA dt,
.dropdownHQA dd, .dropdownHQA dt,
.dropdownBA dd, .dropdownBA dt,
.dropdownDA dd, .dropdownDA dt,
.dropdownSA dd, .dropdownSA dt {
    margin:0px;
    padding:0px;
}
.dropdownCA ul,.dropdownHQA ul,.dropdownBA ul,.dropdownDA ul,.dropdownSA ul{
    margin: -1px 0 0 0;
    z-index: 50;
}
.dropdownCA dd,.dropdownHQA dd,.dropdownBA dd,.dropdownDA dd,.dropdownSA dd{
    position:relative;
}
.dropdownCA a, 
.dropdownCA a:visited,
.dropdownHQA a, 
.dropdownHQA a:visited,
.dropdownBA a, 
.dropdownBA a:visited,
.dropdownDA a, 
.dropdownDA a:visited
.dropdownSA a, 
.dropdownSA a:visited {
    text-decoration:none;
    outline:none;
    font-size: 12px;
     background: transparent;
   padding: 5px;
   line-height: 1;
   border: 0;
   border-radius: 0;
   -webkit-appearance: none;
}
.dropdownCA dt a,.dropdownHQA dt a,.dropdownBA dt a,.dropdownDA dt a,.dropdownSA dt a{
    display:block;
    max-height: 34px;
    min-height: 34px;
    line-height: 20px;
    overflow: auto;
    border:0;
    width:272px;
    background-color: #ffffff;
    border: 1px solid #cccccc;
    border-radius: 4px;
    
}
.dropdownCA dt a span, .multiSelCA span,
.dropdownHQA dt a span, .multiSelHQA span,
.dropdownBA dt a span, .multiSelBA span,
.dropdownDA dt a span, .multiSelDA span,
.dropdownSA dt a span, .multiSelSA span {
    cursor:pointer;
    display:inline-block;
    padding: 0 3px 2px 0;
    font-family: inherit;text-transform: none;font: 13.3333px Arial;    text-align: start;
    font-size: 11px;
    color: #716767;
}

.dropdownCA dd ul,.dropdownHQA dd ul,.dropdownBA dd ul,.dropdownDA dd ul,.dropdownSA dd ul{
    background-color: #ffffff;
    border: 1px solid #cccccc;
    display:none;
    left:0px;
    padding: 2px 15px 2px 5px;
    position:absolute;
    width:272px;
    list-style:none;
    overflow:auto;
}
.dropdownCA span.value,.dropdownHQA span.value,.dropdownBA span.value,.dropdownDA span.value,.dropdownSA span.value {
    display:none;
    font-family: inherit;text-transform: none;font: 13.3333px Arial;    text-align: start;
}
.dropdownCA dd ul li a,
.dropdownHQA dd ul li a ,
.dropdownBA dd ul li a ,
.dropdownDA dd ul li a ,
.dropdownSA dd ul li a {
    padding:5px;
    display:block;
}
.dropdownCA dd ul li a:hover,
.dropdownHQA dd ul li a:hover,
.dropdownBA dd ul li a:hover,
.dropdownDA dd ul li a:hover,
.dropdownCA dd ul li a:hover{
    background-color:#fff;
}
</style>                            
<script>
    $(".dropdownCA dt a").on('click', function () {
          $(".dropdownCA dd ul").slideToggle('fast');
      });

    $(".dropdownHQA dt a").on('click', function () {
          $(".dropdownHQA dd ul").slideToggle('fast');
      }); 
      
      $(".dropdownBA dt a").on('click', function () {
          $(".dropdownBA dd ul").slideToggle('fast');
      });
      
       $(".dropdownDA dt a").on('click', function () {
          $(".dropdownDA dd ul").slideToggle('fast');
      });
      $(".dropdownSA dt a").on('click', function () {
          $(".dropdownSA dd ul").slideToggle('fast');
      });
 
      $(".dropdownCA dd ul li a").on('click', function () {
          $(".dropdownCA dd ul").hide();
      });
     
        $(".dropdownHQA dd ul li a").on('click', function () {
          $(".dropdownHQA dd ul").hide();
      });

      $(".dropdownBA dd ul li a").on('click', function () {
          $(".dropdownBA dd ul").hide();
      });
      $(".dropdownDA dd ul li a").on('click', function () {
          $(".dropdownDA dd ul").hide();
      });
      $(".dropdownSA dd ul li a").on('click', function () {
          $(".dropdownSA dd ul").hide();
      });
   
      function getSelectedValue(id) {
           return $("#" + id).find("dt a span.value").html();
      }

      $(document).bind('click', function (e) {
          var $clicked = $(e.target);
          if (!$clicked.parents().hasClass("dropdownCA")) $(".dropdownCA dd ul").hide();
      });
      
      $(document).bind('click', function (e) {
          var $clicked = $(e.target);
          if (!$clicked.parents().hasClass("dropdownHQA")) $(".dropdownHQA dd ul").hide();
      });
      $(document).bind('click', function (e) {
          var $clicked = $(e.target);
          if (!$clicked.parents().hasClass("dropdownBA")) $(".dropdownBA dd ul").hide();
      });
      $(document).bind('click', function (e) {
          var $clicked = $(e.target);
          if (!$clicked.parents().hasClass("dropdownDA")) $(".dropdownDA dd ul").hide();
      });
      $(document).bind('click', function (e) {
          var $clicked = $(e.target);
          if (!$clicked.parents().hasClass("dropdownSA")) $(".dropdownSA dd ul").hide();
      });
</script> 

<script type="text/javascript">//<![CDATA[
 var cal = Calendar.setup({
          onSelect: function(cal) {cal.hide() },
          showTime: true
      });
     function call(obj){
        cal.manageFields($(obj).attr('id'), $(obj).attr('name'), "%m-%d-%Y");
        $('.DynarchCalendar-topCont').css({'z-index':'10000','top': '625px','left': '564px'});
     }
    //]]>
</script>