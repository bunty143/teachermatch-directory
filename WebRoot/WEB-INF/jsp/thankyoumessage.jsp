<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<div class="row offset4 msgPage">
	<div class="span8 mt30">
		${jobOrder.exitMessage} 
		<c:choose>
			<c:when test="${(not empty jobOrder.headQuarterMaster || not empty jobOrder.branchMaster)}">
				<br/><a href="userdashboard.do">Click here</a> to go back. 
			</c:when>
			<c:otherwise>
				<br/><a href="jobsofinterest.do">Click here</a> to go back.
			</c:otherwise>
		</c:choose>
	</div>
</div>