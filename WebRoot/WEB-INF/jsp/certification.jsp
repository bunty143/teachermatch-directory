<jsp:include page="/portfolioheader.do"></jsp:include>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
 <%@ page import="tm.utility.Utility" %>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>

<script type="text/javascript" src="dwr/interface/DWRAutoComplete.js?ver=${resourceMap['DWRAutoComplete.ajax']}"></script> 
<script type="text/javascript" src="dwr/interface/PFCertifications.js?ver=${resourceMap['PFCertifications.ajax']}"></script> 
<script type="text/javascript" src="dwr/engine.js"></script> 
<script type="text/javascript" src="dwr/util.js"></script> 
<script type='text/javascript' src="js/ac_quicktime.js?ver=${resourceMap['js/ac_quicktime.js']}"></script> 
<script type="text/javascript" src="js/teacher/pfCertifications.js?ver=${resourceMap['js/teacher/pfCertifications.js']}"></script> 
<script type="text/javascript" src="js/teacher/pfAutoCompCertificate.js?ver=${resourceMap['js/teacher/pfAutoCompCertificate.js']}"></script> 
<script type="text/javascript" src="js/jquery-1.4.1.js"></script> 
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script> 
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}"/>   

<script> 
var opendiv = "${openDiv}";
$(document).ready(function() {
	if(opendiv=="References")
	{
		$('#collapseOne').collapse('hide');
		$('#collapseTwo').collapse('show');
	}
	else
	if(opendiv=="Video")
	{
		$('#collapseOne').collapse('hide');
		$('#collapseThree').collapse('show');
	}
});
</script>

<style>
.tooltip-inner 
{
  max-width: 700px;
}
.divwidth
{
float: left;
width: 40px;
}
</style>
<script type="text/javascript"><!--

var $j=jQuery.noConflict();
        $j(document).ready(function() {
            
        });        
function applyScrollOnTbl()
{
	//alert("Hi")
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblGridCertifications').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 380,
        width: 850,
        minWidth: null,
        minWidthAuto: false,
       	colratio:[250,90,95,140,185,90],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
    });			
}
applyScrollOnTbl();

function applyScrollOnTblEleRef()
{
	//alert("Hi")
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#eleReferencesGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 350,
        width: 900,
        minWidth: null,
        minWidthAuto: false,
          colratio:[115,85,140,150,110,90,92,115],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 70,
        wrapper: false
        });
            
        });			
}
applyScrollOnTblEleRef();

function applyScrollOnTblVideoLinks()
{
	//alert("Hi")
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#videoLinktblGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 350,
        width: 850,
        minWidth: null,
        minWidthAuto: false,
          colratio:[500,150,100,100],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 70,
        wrapper: false
        });
            
        });			
}
function applyScrollOnTbl_AdditionalDocuments()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#additionalDocumentsGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 380,
        width: 850,
        minWidth: null,
        minWidthAuto: false,
        colratio:[400,300,150],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
        });			
}



applyScrollOnTblVideoLinks();
function showcertiDiv()
{
	$('#draggableDivMaster').modal('show');
}
</script>

<div  class="modal hide" id="draggableDivMaster" style="" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog-for-cgmessage">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabel"><spring:message code="lblCert/LiceName"/></h3>
	</div>
	<div class="modal-body">
		<p><spring:message code="msgWeArSoryYuNotFindingYurCerti/Lice"/></p>
		<p><spring:message code="msgPlzTypingStringInFldCerti/Lice"/></p>
		<p><spring:message code="pNotifyEmailService"/>
		<a href="mailto:clientservices@teachermatch.net" target="_top"><spring:message code="lnkClntServiceTm"/></a>
		 <spring:message code="pHlpYuLoadRiLice/Certi"/></p> 
		<p><spring:message code="pThkForYurHlpInThisMtr"/></p>
	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnOk"/></button>
 	</div>
</div>
	</div>
</div>

<div class="row" style="margin:0px;">
         <div style="float: left;">
         	<img src="images/certifications.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;"><spring:message code="lblCrede"/></div>	
         </div>		
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
</div>
<input type="hidden" id="pageflag"/> 

<div class="top15" onkeypress="chkForEnterCert(event)">
	<form class="" onsubmit="return false;">
		<input type="hidden" id="expId" name="expId" value="${teacherExperience.expId}"/>
		<div class="row">
		<div class="col-sm-12 col-md-12">
 			<div class='divErrorMsg' id='errordivExp' style="display: block;"></div>
		</div><!--
		
		<div class="col-sm-12 col-md-12">
			<label>
				<strong><spring:message code="lblCertiTeachExp"/><span class="required">*</span>
					<a href="#" id="iconpophover1" rel="tooltip" data-original-title="Years as a full-time instructor in an alternative certification program should be included"><img src="images/qua-icon.png" width="15" height="15" alt=""></a>
				</strong>
			</label>
		</div>
		
		--><c:choose>
			    <c:when test="${teacherExperience.isNonTeacher eq true}">
					<c:set var="checked" ></c:set>
			    	<c:set var="checked2" value="checked"></c:set>
			    	<c:set var="expDiv" value="hide"></c:set>
			    	<c:set var="expCertTeacherTrainingVal" value="${teacherExperience.expCertTeacherTraining }"></c:set>
			    </c:when>
			    <c:when test="${teacherExperience.isNonTeacher eq null}">
					<c:set var="checked" ></c:set>
			    	<c:set var="checked2" value="checked"></c:set>
			    	<c:set var="expDiv" value="hide"></c:set>
			    	<c:set var="expCertTeacherTrainingVal" value="${teacherExperience.expCertTeacherTraining }"></c:set>
			    </c:when>
			    <c:otherwise>			    
			    	<c:set var="checked" value="checked"></c:set>
			    	<c:set var="checked2" value=""></c:set>
			    	<c:set var="disabled" value="disabled"></c:set>
			    	<c:set var="expDiv" value=""></c:set>
			    	<c:set var="expCertTeacherTrainingVal" value="${teacherExperience.expCertTeacherTraining }"></c:set>
			    	</c:otherwise>              
			</c:choose>
		
		<div class="col-sm-12 col-md-12">
			<label>
				<strong><spring:message code="LBLAreyouacertifie"/><span class="required">*</span></strong>
			</label><br>
			 <form role="form">
			    <label class="radio-inline">
			      <input type="radio" ${checked} value="0" name="isNonTeacher" onclick="return chkNonTeacher();"><spring:message code="lblYes"/>
			    </label>
			    <label class="radio-inline">
			      <input type="radio" ${checked2} id="isNonTeacher" value="1" name="isNonTeacher" onclick="return chkNonTeacher();"><spring:message code="lblNo"/>
			    </label>
		  </form>
		</div>
		<div class="col-sm-12 col-md-12 ${expDiv} expDiv" >
			<label>
				<strong><spring:message code="LBLAreyouacertifie2"/><span class="required">*</span>
				
					
					 <% if(Utility.getValueOfPropByKey("locale").equalsIgnoreCase("en")){%>
					     <a href="#" id="iconpophover1" rel="tooltip" data-original-title="Years as a full-time instructor in an alternative certification program should be included"><img src="images/qua-icon.png" width="15" height="15" alt=""></a>
			         <% }%>
				</strong>
			</label>
		</div>

			<div class="col-sm-2 col-md-2 ${expDiv} expDiv">				
				<input type="text" id="expCertTeacherTraining" name="expCertTeacherTraining" class="form-control" onkeypress="return checkForDecimalTwo(event);" maxlength="5" value="${expCertTeacherTrainingVal}">
			</div>
</div>


<!--  khan -->
 <% if(Utility.getValueOfPropByKey("locale").equalsIgnoreCase("en")){%>
	<div class="row">	
		<div class="col-sm-12 col-md-12">
			<label>
				<strong><spring:message code="lblNatiBoardCerti/Lice"/><span class="required">*</span> <a href="#" id="iconpophover2" rel="tooltip" data-original-title="Select 'Yes' if you have a National Board Certification and enter the year you received the certification"><img src="images/qua-icon.png" width="15" height="15" alt=""></a></strong>
			</label>
		</div>
		<div class="col-sm-12 col-md-12">	
		               <div class="radio inline col-sm-1 col-md-1">
					    <input type="radio" value="nbc2" id="nbc2" name="nationalBoardCert" <c:if test="${teacherExperience.nationalBoardCert eq false or teacherExperience.nationalBoardCert eq null}"><spring:message code="lblChk"/></c:if>   onclick="chkNBCert('2')">
					<spring:message code="lblNo"/>
					    </div>
					    </br>
					    <div class="radio inline col-sm-1 col-md-1 top20down">
					    	<input type="radio" value="nbc1" id="nbc1" name="nationalBoardCert" <c:if test="${teacherExperience.nationalBoardCert}"><spring:message code="lblChk"/></c:if> onclick="chkNBCert('1')">
					 <spring:message code="lblYes"/>
					    </div>			
		</div>
		<div class="col-sm-2 col-md-2 top-sm20">			
	        <div id="divNbdCert" name="divNbdCert"  <c:choose>  <c:when test="${teacherExperience.nationalBoardCert}">style='display: block;'</c:when><c:otherwise>style='display: none;'</c:otherwise></c:choose> />
				<select id="nationalBoardCertYear" name="nationalBoardCertYear"  class="form-control" >
					<option value=""><spring:message code="optSlt"/></option>
					<c:forEach var="year" items="${lstLastYear}">	
						<c:set var="selected" value=""></c:set>					
		        		<c:if test="${year eq teacherExperience.nationalBoardCertYear}">	        			
		         			<c:set var="selected" value="selected"></c:set>	         			
		        		</c:if>							
						<option id="nationalBoardCertYear${year}" value="${year}" ${selected} >${year }</option>
					</c:forEach>					
				</select>
			</div>
		</div>
	</div>
	
	 <%} %>
	<!-- end khan -->
	
	<div class="row">	
		<div class="col-sm-12 col-md-12">
			<label>
				<strong><spring:message code="msgSubsti/PartTiTech"/><!-- <span class="required">*</span> --></strong>
			</label>
		</div>
		<div class="col-sm-12 col-md-12">      
	       
	        <div class="radio inline col-sm-1 col-md-1">
					 <input type="radio" id="canServeAsSubTeacher0" value="0" name="canServeAsSubTeacher"<c:if test="${teacherExperience.canServeAsSubTeacher eq 0}"><spring:message code="lblChk"/></c:if>><spring:message code="lblNo"/>
					    </div>
					    </br>
		    <div class="radio inline col-sm-1 col-md-1 top20down">
		    	<input type="radio" id="canServeAsSubTeacher1" value="1" name="canServeAsSubTeacher"<c:if test="${teacherExperience.canServeAsSubTeacher eq 1}"><spring:message code="lblChk"/></c:if>> <spring:message code="lblYes"/>
		    </div>
	       
	       
		</div>
	</div>	
	<% if(Utility.getValueOfPropByKey("locale").equalsIgnoreCase("en")){%>
	      <div class="row ">
			
				<div class="col-sm-3 col-md-3">
					<label>
						<strong><spring:message code="lblTchForAmericaAffi"/><span class="required">*</span>
							<a href="#" id="iconpophover4" rel="tooltip" data-original-title='<spring:message code="msgForIndicateYurStatus"/>'>
								<img src="images/qua-icon.png" width="15" height="15" alt="">
							</a>
						</strong>
					</label>
					<select  class="form-control"  id="tfaAffiliate" onchange="hideTFAFields()">   
						<option value=""><spring:message code="optSltTchForAmericaAffi"/></option>
							<c:forEach items="${lstTFAAffiliateMaster}" var="lstTFAAffilate">
								<c:set var="selected" value=""></c:set>
								<c:if test="${lstTFAAffilate.tfaAffiliateId eq teacherExperience.tfaAffiliateMaster.tfaAffiliateId}">	        			
				         			<c:set var="selected" value="selected"></c:set>	         			
				        		</c:if>
				        		<option id="${lstTFAAffilate.tfaAffiliateId}" value="${lstTFAAffilate.tfaAffiliateId}" ${selected} >${lstTFAAffilate.tfaAffiliateName}</option>
							</c:forEach>	
					</select>		
				</div>
				<c:set var="hide" value=""></c:set>
				<c:if test="${teacherExperience.tfaAffiliateMaster.tfaAffiliateId eq 3}">
					<c:set var="hide" value="hide"></c:set>
				</c:if>
				
				<div id="tfaFieldsDiv" class="${hide}">
					<div class="col-sm-3 col-md-3">
						<label>
							<strong><spring:message code="lblCorpsYear"/><span class="required">*</span></strong>
						</label>
						<select  class="form-control" id="corpsYear">   
							<option value=""><spring:message code="lblSltCorpsYear"/></option>
								<c:forEach var="corps" items="${lstCorpsYear}">	
									<c:set var="selected" value=""></c:set>					
					        		<c:if test="${corps eq teacherExperience.corpsYear}">	        			
					         			<c:set var="selected" value="selected"></c:set>	         			
					        		</c:if>							
									<option id="corps${corps}" value="${corps}" ${selected} >${corps }</option>
								</c:forEach>	
						</select>		
					</div>
					<div class="col-sm-3 col-md-3">
						<label>
							<strong><spring:message code="lblTchForAmericaRgn"/><span class="required">*</span></strong>
						</label>
						<select  class="form-control"  id="tfaRegion" onchange="">  
							<option value=""><spring:message code="lblSltTchForAmericaRgn"/></option>
							
							<c:forEach items="${lstTFARegionMaster}" var="lstTFARegion">
								<c:set var="selected" value=""></c:set>
								<c:if test="${lstTFARegion.tfaRegionId eq teacherExperience.tfaRegionMaster.tfaRegionId}">	        			
				         			<c:set var="selected" value="selected"></c:set>	         			
				        		</c:if>
				        		<option id="${lstTFARegion.tfaRegionId}" value="${lstTFARegion.tfaRegionId}" ${selected}>${lstTFARegion.tfaRegionName}</option>
							</c:forEach>	
							
						</select>		
					</div>
				</div>
			
		</div>	
	 <%} %>
	 </form>
</div>


<div class="mt30">
		<div class="accordion" id="accordion2" style="min-width:900px;">
		
		<% if(Utility.getValueOfPropByKey("locale").equalsIgnoreCase("fr")){%>
		    <%}else{ %>
			    <div class="accordion-group" onclick="setPageFlag(1);">
				<div class="accordion-heading">
					<a href="#collapseOne" onclick="getDivNo(1)"
						data-parent="#accordion2" id="certificationTypeDiv"
						data-toggle="collapse" class="accordion-toggle minus">
						<spring:message code="lnkCerti/Lice"/> </a>
				</div>
				<div class="accordion-body in" id="collapseOne" style="height: auto;">
					<div class="accordion-inner">
						<div class="offset1">
							<div class="row">
								<div style='width: 850px;'>
									<div class="pull-right add-employment1">
										<a href="javascript:void(0);"
											onclick="showForm(),clearForm();fieldsDisable(0);"><spring:message code="lnkAddCerti/Lice"/></a>
									</div>
								</div>
							</div>
							<div class="row">
							    <div class="col-sm-10 col-md-10">
								<div id="divDataGrid" class="mt10"></div>
								</div>
							</div>
							<div  id="divMainForm"
								style="display: none;"
								onkeypress="chkForEnter(event)">
								<iframe id='uploadCertFrameID' name='uploadCertFrame' height='0'
									width='0' frameborder='0' scrolling='yes' sytle='display:none;'>
								</iframe>
								<form id="frmCertificate" name="frmCertificate"
									enctype='multipart/form-data' method='post'
									target='uploadCertFrame'
									action='fileuploadservletforcertification.do'>
									<input type="hidden" id="certId" name="certId" />
									<div class="row top15">
									<div class="col-sm-12 col-md-12">
										<div id='divServerError' class='divErrorMsg'
											style="display: block;">
											${msgError}
										</div>
										<div class='divErrorMsg' id='errordiv' style="display: block;"></div>
									</div>

									<div class="col-sm-4 col-md-4">
										<label>
											<strong><spring:message code="lblCerti/LiceSt"/><span
												class="required">*</span>
											</strong>
										</label>
										<select class="form-control" id="certificationStatusMaster" onchange="showHideFileUpload(this.value);fieldsDisable(this.value);">
											<option value="">
												<spring:message code="lblSltCerti/LuceStatus"/>
											</option>
											<c:forEach items="${lstCertificationStatusMaster}" var="csml">
												<option id="csml${csml.certificationStatusId}"
													value="${csml.certificationStatusId}"${selected} >
													${csml.certificationStatusName}
												</option>
											</c:forEach>
										</select>
									</div>


									<!--<div class="span4"><label><strong><spring:message code="lblSt"/><span class="required">*</span></strong></label>
										<select  class="span4" id="stateMaster" onchange="getPraxis(),clearCertType();">   
											<option value=""><spring:message code="optSltSt"/></option>
											<c:forEach items="${listStateMaster}" var="st">
								        		<option id="st${st.stateId}" value="${st.stateId}" ${selected} >${st.stateName}</option>
											</c:forEach>				
										</select>	
									</div>
									
									 
									 <div class="span2"><label><strong><spring:message code="lblYearRece"/><span class="required">*</span></strong></label>
									<select id="yearReceived" name="yearReceived"  class="span2" >
										<option id="yearReceivedSelect" value=""><spring:message code="optSlt"/></option>
										<c:forEach var="year" items="${lstLastYear}">							
											<option id="yearReceived${year}" value="${year}">${year }</option>
										</c:forEach>					
									</select>
								</div>-->
								 
									<div class="col-sm-3 col-md-3">
										<label>
											<strong><spring:message code="lblCertiRp"/><span class="required">*</span>
											</strong>
										</label>
										<select id="certificationtypeMaster" name="certificationtypeMaster" class="form-control">
											<option value="0"><spring:message code="optSlt"/></option>
											 <c:forEach var="cvar" items="${listCertTypeMasters}">							
								<option  value="${cvar.certificationTypeMasterId}">${cvar.certificationType}</option>
  											</c:forEach>
										</select>
									</div>

									<div class="col-sm-4 col-md-4">
										<label>
											<strong><spring:message code="lblSt"/><span class="required">*</span>
											</strong>
										</label>
										<select class="form-control" id="stateMaster"
											onchange="getPraxis(),clearCertType();">
											<option value="">
												<spring:message code="optSltSt"/>
											</option>
											<c:forEach items="${listStateMaster}" var="st">
												<option id="st${st.stateId}" value="${st.stateId}"${selected} >
													${st.stateName}
												</option>
											</c:forEach>
										</select>
									</div>
									
									<div class="col-sm-2 col-md-2" style="width:14%;"><label><strong><spring:message code="lblYearRece"/><span class="required">*</span></strong></label>
										<select id="yearReceived" name="yearReceived"  class="form-control" >
											<option value=""><spring:message code="optSlt"/></option>
											<c:forEach var="year" items="${lstLastYear}">							
												<option id="yearReceived${year}" value="${year}">${year }</option>
											</c:forEach>					
										</select>
									</div>
									
									<div class="col-sm-2 col-md-2" style="width:19.5%;">	
										<label><strong><spring:message code="lblYearExp"/><span class="required"></span></strong></label>
										<select class="form-control" name="yearexpires" id="yearexpires" style="padding-left:6px;">
											<option value=""><spring:message code="optDoesNotExpire"/></option>
											<c:forEach var="year" items="${lstComingYear}">							
												<option id="yearExpires${year}" value="${year}">${year }</option>
											</c:forEach>
										</select>
									</div>

									<div class="col-sm-7 col-md-7">
									<label><strong><spring:message code="lblCert/LiceName"/><span class="required">*</span></strong>
										<a href="#" style="margin-left: 100px;" onclick="showcertiDiv()"><spring:message code="lnkCerti/LiceNotLi"/></a>
									</label>
			
										<input type="hidden" id="certificateTypeMaster" value="">
										<input type="text" class="form-control" maxlength="500" id="certType"
											value="" name="certType"
											onfocus="getCertificateTypeAutoComp(this, event, 'divTxtCertTypeData', 'certType','certificateTypeMaster','');"
											onkeyup="getCertificateTypeAutoComp(this, event, 'divTxtCertTypeData', 'certType','certificateTypeMaster','');"
											onblur="hideCertificateType(this,'certificateTypeMaster','divTxtCertTypeData');" />
										<div id='divTxtCertTypeData' style='display: none; position: absolute;'	onmouseover="mouseOverChk('divTxtCertTypeData','certType')"	class='result'></div>
									</div>

									<div class="col-sm-2 col-md-2">
										<label><strong><spring:message code="lblDOENum"/></strong></label>
										<input type="text" class="form-control" name="doenumber" id="doenumber" maxlength="25" value="" />
									</div>


									<!--<div class="span4"><label><strong><spring:message code="lblCerti/LiceSt"/><span class="required">*</span></strong></label>
										<select  class="span4" id="certificationStatusMaster">   
											<option value=""><spring:message code="lblSltCerti/LuceStatus"/></option>
											<c:forEach items="${lstCertificationStatusMaster}" var="csml">
								        		<option id="csml${csml.certificationStatusId}" value="${csml.certificationStatusId}" ${selected} >${csml.certificationStatusName}</option>
											</c:forEach>
										</select>
								   </div>
								

									--><div class="col-sm-9 col-md-9">
										<label>
											<strong><spring:message code="lblCerti/LiceUrl"/> <a href="#" id="iconpophoverCertification" rel="tooltip" data-original-title="If you are including a URL, please ensure the URL links directly to a view of your credentials and not a page that requires a log in. If a log in is required, the district will not be able to access your information."><img src="images/qua-icon.png" width="15" height="15" alt=""></a></strong>
										</label>

										<input type="text" class="form-control" maxlength="500" id="certUrl"
											value="" name="certUrl" />

									</div>
									</div>
									<div class="row">	
									<div class="col-sm-12 col-md-12">
									<label><strong><spring:message code="lblGradeLvl"/></strong></label>
									</div>
									</div>
								<div class="row col-sm-12 col-md-12" style="margin-top: -10px;">	
									<div class="divwidth">										
											<label class="checkbox inline">
												<input type="checkbox" value="1" id="pkOffered"
													name="pkOffered">
												<spring:message code="lblPK"/>
											</label>
									</div>
									<div class="divwidth">			
											<label class="checkbox inline">
												<input type="checkbox" value="1" id="kgOffered"
													name="kgOffered">
												<spring:message code="lblKG"/>
											</label>
									</div>
									<div class="divwidth">			
											<label class="checkbox inline">
												<input type="checkbox" value="1" id="g01Offered"
													name="g01Offered">
												<spring:message code="lbl1"/>
											</label>
									</div>
									<div class="divwidth">			
											<label class="checkbox inline">
												<input type="checkbox" value="1" id="g02Offered"
													name="g02Offered">
												<spring:message code="lbl2"/>
											</label>
									</div>
									<div class="divwidth">			
											<label class="checkbox inline">
												<input type="checkbox" value="1" id="g03Offered"
													name="g03Offered">
												<spring:message code="lbl3"/>
											</label>
									</div>
									<div class="divwidth">			
											<label class="checkbox inline">
												<input type="checkbox" value="1" id="g04Offered"
													name="g04Offered">
												<spring:message code="lbl4"/>
											</label>
									</div>
									<div class="divwidth">			
											<label class="checkbox inline">
												<input type="checkbox" value="1" id="g05Offered"
													name="g05Offered">
												<spring:message code="lbl5"/>
											</label>
									</div>
									<div class="divwidth">			
											<label class="checkbox inline">
												<input type="checkbox" value="1" id="g06Offered"
													name="g06Offered">
												<spring:message code="lbl6"/>
											</label>
									</div>
									<div class="divwidth">			
											<label class="checkbox inline">
												<input type="checkbox" value="1" id="g07Offered"
													name="g07Offered">
												<spring:message code="lbl7"/>
											</label>
									</div>
									<div class="divwidth">			
											<label class="checkbox inline">
												<input type="checkbox" value="1" id="g08Offered"
													name="g08Offered">
												<spring:message code="lbl8"/>
											</label>
									</div>
									<div class="divwidth">			
											<label class="checkbox inline">
												<input type="checkbox" value="1" id="g09Offered"
													name="g09Offered">
												<spring:message code="lbl9"/>
											</label>
									</div>
									<div class="divwidth">			
											<label class="checkbox inline">
												<input type="checkbox" value="1" id="g10Offered"
													name="g10Offered">
												<spring:message code="lbl10"/>
											</label>
									</div>
									<div class="divwidth">			
											<label class="checkbox inline">
												<input type="checkbox" value="1" id="g11Offered"
													name="g11Offered">
												<spring:message code="lbl11"/>
											</label>
									</div>
									<div class="divwidth">			
											<label class="checkbox inline">
												<input type="checkbox" value="1" id="g12Offered"
													name="g12Offered">
												<spring:message code="lbl12"/>
											</label>
									</div>								
									</div>
									<div id='praxisArea' class="row">
										<div class="col-sm-2 col-md-2">										
											<label>
												<strong><spring:message code="lblReqPrxITests"/></strong>
											</label>
										</div>
										<div class="col-sm-3 col-md-3" >
											<label>
												<strong id='reading'></strong>
											</label>
											<div class="row">
											<div class="col-sm-8 col-md-8">
												<label>
												<strong><spring:message code="lblYurScore"/> <input type='text'
														maxlength="3" id='readingQualifyingScore' class="form-control"
														onkeypress="return checkForInt(event)">
												</strong>
											</label>
											</div>											
											</div>
										 </div>
	        							 <div class="col-sm-3 col-md-3">
											<label>
												<strong id='writing'></strong>
											</label>
											<div class="row">
											<div class="col-sm-8 col-md-8">
											   <label>
												<strong><spring:message code="lblYurScore"/> <input type='text'
														maxlength="3" id='writingQualifyingScore' class="form-control"
														onkeypress="return checkForInt(event)">
												</strong>
											    </label>
											</div>											
											</div>
										 </div>
	         							 <div class="col-sm-3 col-md-3">
											<label>
												<strong id='maths'></strong>
											</label>
											<div class="row">
											<div class="col-sm-8 col-md-8">
												<label>
												<strong><spring:message code="lblYurScore"/> <input type='text'
														maxlength="3" id='mathematicsQualifyingScore'
														class="form-control" onkeypress="return checkForInt(event)">
												</strong>
											    </label>
											</div>											
											</div>
										 </div>
	         							</div>
	         						  <div class="row">	  
									 <div class="col-sm-4 col-md-4" id="fileuploadDiv">
										<label>
											<spring:message code="lblCerti/LiceLtr"/>
										</label>
										<input type="hidden" id="sbtsource_cert" name="sbtsource_cert"
											value="0" />
										<input id="pathOfCertificationFile"
											name="pathOfCertificationFile" type="file" width="20px;">
										<a href="javascript:void(0)" id="clearLink" onclick="clearCertification()"><spring:message code="lnkClear"/></a>
									 </div>
									<input type="hidden" id="pathOfCertification" />
									<span class="col-sm-4 col-md-4" id="removeCert" name="removeCert"
										style="display: none;"> <label>
											&nbsp;&nbsp;
										</label> <span id="divCertName"></span> <a href="javascript:void(0)"
										onclick="removeCertificationFile()"><spring:message code="lnkRemo"/></a>&nbsp; <a
										href="#" id="iconpophover6" rel="tooltip"
										data-original-title="Remove certification/Licence file !">
											<img src="images/qua-icon.png" width="15" height="15" alt="">
									</a> </span>
									</div>
									<iframe src="" id="ifrmCert" width="100%" height="480px"
										style="display: none;">
									</iframe>
								</form>
								<input type="hidden" value="" id="certText"/>
								<div class="span16 idone" style="padding-top: 10px;">
									<a id="hrefDone" href="#"
										style="cursor: pointer; margin-left: 0px; text-decoration: none;"
										onclick="return insertOrUpdate(0)"><spring:message code="lnkImD"/></a>&nbsp;&nbsp;
									<a class="idone"
										style="cursor: pointer; margin-left: 0px; text-decoration: none;"
										onclick="return hideForm()"><spring:message code="lnkCancel"/></a>
								</div>
							</div>
							
                       </div>
						<div class="clearfix">
							&nbsp;
						</div>
					</div>
				</div>
			</div>
			
		
			
				 <%} %>
			<!-- Ed. accordion Part 01  -->
				
			<!-- St. accordion Part 02  -->
			
			<div class="accordion-group" onclick="setPageFlag(2);">
					<div class="accordion-heading">
	                	<a href="#collapseTwo" onclick="getDivNo(2)" data-parent="#accordion2" id="electronicReferencesDiv" data-toggle="collapse" class="accordion-toggle plus">
	                    	<spring:message code="lblRef"/>
	                    </a>
					</div>
					<div class="accordion-body collapse" id="collapseTwo" style="height: 0px;" onkeypress="chkForEnterElectronicReferences(event)">
	                	<div class="accordion-inner">
	                    	<div style="margin-left: 20px;">
			                    	<div class="row">
										<div style='width:850px;'>	
											<div class="pull-right add-employment1" >
													<input type="hidden" id="divNo" name="divNo" value="">
													<a href="#" onclick="return showElectronicReferencesForm();" ><spring:message code="lnkAddRef"/></a>
											</div>	
										</div>
									</div>
							
									<div class="row">
									    <div class="col-sm-10 col-md-10">
										<div id="divDataElectronicReferences" class="mt10" style=''></div>
									    </div>
									</div>
									<iframe id='uploadFrameID' name='uploadFrame' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'>
									</iframe>
									<form id="frmElectronicReferences" name="frmElectronicReferences" enctype='multipart/form-data' method='post' target='uploadFrame' action='fileuploadservletforreferences.do' >
					            	
												<div class="top15" style="display: none" id="divElectronicReferences">
												    <div class="row">
												    <div class="col-sm-12 col-md-12">
													<div class='divErrorMsg' id='errordivElectronicReferences' style="display: block;"></div>
													</div>
													
														<div class="col-sm-3 col-md-3">
															<label><spring:message code="lblSalutation"/></label>
															<select class="form-control" id="salutation" name="salutation">
																<option value="0"></option>   
																<option value="4"><spring:message code="optDr"/></option>
																<option value="3"><spring:message code="optMiss"/></option>
																<option value="2"><spring:message code="optMr"/></option>
																<option value="1"><spring:message code="optMrs"/></option>
																<option value="5"><spring:message code="optMs"/></option>													
															</select>
														</div>
															
														<div class="col-sm-3 col-md-3">
															<label><spring:message code="lblFname"/><span class="required">*</span></label>
															<input type="hidden" id="elerefAutoId">
															<input type="text" id="firstName" name="firstName" class="form-control" placeholder="" maxlength="50">
														</div>
														
														<div class="col-sm-3 col-md-3">
															<label><spring:message code="lblLname"/><span class="required">*</span></label>
															<input type="text" id="lastName" name="lastName" class="form-control" placeholder="" maxlength="50">
														</div>
															
												      </div>
												      <div class="row">
												      <%if(Utility.getValueOfPropByKey("nccheckforjspandjs").equalsIgnoreCase("yes")){%>
				    	                                <div class="col-sm-3 col-md-3">
															<label><spring:message code="lblTitle"/><span class="required">*</span></label>
															<input type="text" id="designation" name="designation" class="form-control" placeholder="" maxlength="50">
														</div> 
				    	                            	<%}						 	
					                                	 	else {%>
				                	                          	<div class="col-sm-3 col-md-3">
					               								<label><spring:message code="lblTitle"/></label>
					 			    							<input type="text" id="designation" name="designation" class="form-control" placeholder="" maxlength="50">
							        							</div>
				                                       	<% }%>	
												      
												      <%if(Utility.getValueOfPropByKey("nccheckforjspandjs").equalsIgnoreCase("yes")){%>
				    	                              	<div class="col-sm-6 col-md-6">
															<label><spring:message code="lblOrga"/><span class="required">*</span></label>
															<input type="text" id="organization" name="organization" class="form-control" placeholder="" maxlength="50">
														</div> 
				    	                            	<%}						 	
					                                	 	else {%>
				                	                         	<div class="col-sm-6 col-md-6">
															<label><spring:message code="lblOrga"/></label>
															<input type="text" id="organization" name="organization" class="form-control" placeholder="" maxlength="50">
														</div> 
				                                       	<% }%>	
																										
												    </div>
													
													<div class="row">
														<div class="col-sm-3 col-md-3">
															<label><spring:message code="lblContNum"/><span class="required">*</span>&nbsp;<a href="#" id="iconpophover5" rel="tooltip" data-original-title='<spring:message code="tooltipPhoneNo"/>'><img src="images/qua-icon.png" width="15" height="15" alt=""></a></label>
															<input type="text" id="contactnumber" name="contactnumber" class="form-control" onKeyUp="numericFilter(this);"  placeholder="" maxlength="10">
														</div>
														
														<div class="col-sm-6 col-md-6">
															<label><spring:message code="lblEmail"/><span class="required">*</span></label>
															<input type="text" id="email" name="email" class="form-control" placeholder="" maxlength="50">
														</div>
													</div>
													
													<div class="row">
														<div class="col-sm-3 col-md-3">
															<label>
																<spring:message code="lblRecoLtr"/>
															</label>
															<input type="hidden" id="sbtsource_ref" name="sbtsource_ref" value="0"/>
															<input id="pathOfReferenceFile" name="pathOfReferenceFile" type="file" width="20px;">
															<a href="javascript:void(0)" onclick="clearReferences()"><spring:message code="lnkClear"/></a>
														</div>
														<input type="hidden" id="pathOfReference"/>
														<span class="col-sm-3 col-md-3" id="removeref" name="removeref" style="display: none;">
															<label>
																&nbsp;&nbsp;
															</label>
															<span id="divRefName">
															</span>
															<a href="javascript:void(0)" onclick="removeReferences()"><spring:message code="lnkRemo"/></a>&nbsp;
																<a href="#" id="iconpophover7" rel="tooltip" data-original-title="Remove recommendation letter !"><img src="images/qua-icon.png" width="15" height="15" alt=""></a>
														</span>
												  </div>
												
													
													<div class="row">
													    <div class="col-sm-12 col-md-12">
														    <label><spring:message code="lblPrsnDirectlyContByHiringAuth"/></label>
														    <div class="" id="" style="height: 40px;">
															    	<div class="radio inline col-sm-1 col-md-1">
																    <input type="radio" checked="checked" id="rdcontacted1" value="1"  name="rdcontacted"> <spring:message code="lblYes"/>
																    </div>
																    </br>
																    <div class="radio inline col-sm-1 col-md-1" style="margin-left:20px;margin-top:-10px;">
																    	<input type="radio" id="rdcontacted0" value="0" name="rdcontacted"> <spring:message code="lblNo"/>
																    </div>
															    </div>									    
													    </div>
													</div>
													<input type="hidden" id="longHaveYouKnow" vaue="">
												<iframe src="" id="ifrmRef" width="100%" height="480px" style="display: none;">
	 											</iframe>  
	 											<input type="hidden" id="referenceDetails" value=""/>
												<div class="row">
												    <div class="col-sm-12 col-md-12 idone">
												    	<a href="#" onclick="return insertOrUpdateElectronicReferences(0)"><spring:message code="lnkImD"/></a>&nbsp;
												    	<a href="#"	onclick="return hideElectronicReferencesForm()"><spring:message code="lnkCancel"/></a>
												    </div>
												</div>
										</div>
									
							</form>	
					  </div>
					</div>
                    <div class="clearfix">&nbsp;</div>
				</div>
			</div>
	
			<!-- Ed. accordion Part 03  -->
			
			<!-- St. accordion Part 03  -->
				<c:if test="${isPortfolioNeeded}">
				<div class="accordion-group" onclick="setPageFlag(3);">
					<%if(!Utility.getValueOfPropByKey("basePath").equalsIgnoreCase("https://canada-en-test.teachermatch.org/")){ %>
					<div class="accordion-heading">
	                	<a href="#collapseThree" onclick="getDivNo(3)" data-parent="#accordion2" id="videoLinksDiv" data-toggle="collapse" class="accordion-toggle plus">
	                    	<spring:message code="lnkViLnk"/>
	                    	 <%if(Utility.getValueOfPropByKey("locale").equalsIgnoreCase("fr")){%>
	                    	   (facultatif)
	                    	 <% }%>
	                    	
	                    </a>
					</div>
	                <div class="accordion-body collapse" id="collapseThree" style="height: 0px;" onkeypress="#">
	                	<div class="accordion-inner">
	                    	<div class="offset1">
	                    	
	                    	<div class="row top15">
	                    	<div class="col-sm-12 col-md-12" style="max-width: 870px;">
	                    		<label><strong><spring:message code="headPlzIncludeURLToAnyVideos"/></strong></label></div>
	                    	</div>
	                    	
							<div class="row">
								<div style='width:860px;'>	
									<div class="pull-right add-employment1" >
											<a href="#" onclick="return showVideoLinksForm()" ><spring:message code="lnkAddViLnk"/></a>
									</div>	
									
								</div>
							</div>
							<div class="row">
							    <div class="col-sm-10 col-md-10">
								<div id="divDataVideoLinks" class="mt10"></div>
								</div>
							</div>
	 						<iframe id='uploadFrameVideoID' name='uploadFrameVideo' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'>
							</iframe>
								<div class="top15" style="display: none" id="divvideoLinks" onkeypress="return checkForEnter(event);">
									<div class='divErrorMsg' id='errordivvideoLinks' style="display: block;"></div>
									<form class="" id="frmvideoLinks" name="frmvideoLinks" method='post'  enctype='multipart/form-data' target='uploadFrameVideo' action='certificationVideoUploadServlet.do'>
										<div class="row">
											<div class="col-sm-12 col-md-12" style="max-width: 880px;">
												<label><spring:message code="lnkViLnk"/></label>
												<input type="hidden" id="videolinkAutoId" name="videolinkAutoId">
												<input type="text" id="videourl" name="videourl" class="form-control" placeholder="" maxlength="200">
											</div>
										</div>
										<div class="row">
											<div class="col-sm-6 col-md-6">
												<br/>
													<label>Video</label>												
													<input type="file" id="videofile" name="videofile">												
													<span style="display: none; visibility: hidden;" id="video"></span>
											</div>										
											<span class="col-sm-6 col-md-6">
											<br/><br/>												
												<spring:message code="lblSupportedvideos"/>												
											</span>
										</div>	
										<div class="row top10">
										    <div class="col-sm-4 col-md-4 idone">
										    	<a href="javascript:void(0)" onclick="insertOrUpdatevideoLinks();"><spring:message code="lnkImD"/></a>&nbsp;
										    	<a href="#"	onclick="return hideVideoLinksForm()"><spring:message code="lnkCancel"/>
												</a>
										    </div>
										</div>
									</form>
								</div>
							</div>
						</div>
                    	<div class="clearfix">&nbsp;</div>
					</div>
					<%}%>
				</div>
			</c:if>
			</div>			
		<!-- Ed. accordion Part 03  -->
		
		<!-- St. accordion Part 03.0 (Additional Document) -->
			<div class="accordion-group" onclick="setPageFlag(4);">
					<div class="accordion-heading">
	                	<a href="#collapseFour" onclick="getDivNo(4)" data-parent="#accordion2" id="additionalDocumentDiv" data-toggle="collapse" class="accordion-toggle plus">
	                    	<spring:message code="lnkAddDocu"/>
	                    </a>
					</div>
					<div class="accordion-body collapse" id="collapseFour" style="height: 0px;">
	                	<div class="accordion-inner">
	                    	<div class="offset1">
			                    	<div class="row">
										<div style='width:850px;'>	
											<div class="pull-right add-employment1" >
													<input type="hidden" id="divNo" name="divNo" value="">
													<a href="#" onclick="return showAdditionalDocumentsForm()"><spring:message code="lnkAddAdditionalDoc"/> </a>
											</div>	
										</div>
									</div>
							
									<div class="row">
									    <div class="col-sm-10 col-md-10">
										<div id="divGridAdditionalDocuments" class="mt10" style=''></div>
									    </div>
									</div>
									
									
									<div  id="divAdditionalDocumentsRow" style="display: none; margin-top: 30px;" onkeypress="chkForAdditionalDocuments(event)">
				<iframe id='uploadFrameAdditionalDocumentsID' name='uploadFrameAdditionalDocuments' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'>
				</iframe>
				<form class="span15 textfield11" id="additionalDocumentsForm" name="multifileuploadformTarget2" enctype='multipart/form-data' method='post' target='uploadFrameAdditionalDocuments' action='additionalDocumentsUploadServlet.do'>
	
			            <div class="row">
							<div class="col-sm-9 col-md-9">
				      			<div class='divErrorMsg' id='errAdditionalDocuments' style="display: block;"></div>
							</div>
						</div>              	
					<div class="row">
						<div class="col-sm-4 col-md-4">
							<label><spring:message code="lnkAddDocu"/><span class="required">*</span></label>
							<input type="hidden" id="sbtsource_aadDoc" name="sbtsource_aadDoc" value="0"/>
							<input type="text" id="documentName" name="documentName" class="form-control" placeholder="" maxlength="250">
						</div>
						<div class="col-sm-3 col-md-3">
							<label style="margin-right: -59px;">
								<spring:message code="lblDoc(PlzUploadAF)"/><span class="required">*</span>
							</label>
							<input type="hidden" id="uploadedDocumentHidden" name="uploadedDocumentHidden" value=""/>
							<input id="uploadedDocument" name="uploadedDocument" type="file" width="20px;">
							<a href="javascript:void(0)" onclick="clearUploadedDocument()"><spring:message code="lnkClear"/></a>
						</div>
						<div class="col-sm-3 col-md-3" style="margin-top: 25px;" id="removeUploadedDocumentSpan" name="removeUploadedDocumentSpan" style="display: none;">
						<label>
							&nbsp;&nbsp;
						</label>
						<div id="divUploadedDocument">
						</div>
						</div>
					</div>			
		       </form>			
			<div class="row">
				<div class="col-sm-4 col-md-4" id="divDocumnetDone" style="display: block; ">
				<a class="idone" style="cursor: pointer;text-decoration:none;" onclick="insertOrUpdate_AdditionalDocuments(0)">
					<spring:message code="lnkImD"/>
				</a>&nbsp;&nbsp;
				<a class="idone" style="cursor: pointer;text-decoration:none;"	onclick="resetAdditionalDocumentsForm()">
					<spring:message code="lnkCancel"/>
				</a>
				</div>
			</div>
	</div>	
					  </div>
					</div>
                    <div class="clearfix">&nbsp;</div>
				</div>
			</div>
			
			<!-- Ed. accordion Part 03.0 (Additional Document) -->
		
	
</div>
















<div class="row">
	<div class="">
	<br>
	<div class="col-sm-4 col-md-4" >
		<button class="btn btn-large btn-primary" hre="#" onclick="return saveAndContinueCertifications()">
			<strong><spring:message code="btnSv&Conti"/> <i class="icon"></i></strong>
		</button>
		<br><br>
	</div>
	</div>
</div>

<div class="modal hide"  id="delVideoLnk" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><spring:message code="btnX"/></button>
		<h3 id="myModalLabel"><spring:message code="headTm"/></h3>
	</div>
	<div class="modal-body"> 		
		<div class="control-group">
		<spring:message code="msgDoUDeleteVideoLnk"/>
		</div>
 	</div>
 	<div class="modal-footer">
 	<span id=""><button class="btn  btn-primary" onclick="delVideoLnkConfirm()" ><spring:message code="btnOk"/> <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClr"/></button> 		
 	</div>
</div>
	</div>
</div>

<div class="modal hide"  id="chgstatusRef1" style="border: 0px solid blue;" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><spring:message code="btnX"/></button>
		<h3 id="myModalLabel"><spring:message code="headTm"/></h3>
	</div>
	<div id="deact" class="modal-body"> 		
		<div class="control-group">
			<spring:message code="msgDoUActRef"/>
		</div>
 	</div>
 	<div class="modal-footer">
 	<span id=""><button class="btn  btn-primary" onclick="changeStatusElectronicReferencesConfirm()" ><spring:message code="btnOk"/> <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClr"/></button> 		
 	</div>
</div>
 	</div>
</div>
<div class="modal hide"  id="chgstatusRef2" style="border: 0px solid blue;" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><spring:message code="btnX"/></button>
		<h3 id="myModalLabel"><spring:message code="headTm"/></h3>
	</div>
	<div id="deact" class="modal-body"> 		
		<div class="control-group">
			<spring:message code="msgDoUDeActRef"/>
		</div>
 	</div>
 	<div class="modal-footer">
 	<span id=""><button class="btn  btn-primary" onclick="changeStatusElectronicReferencesConfirm()" ><spring:message code="btnOk"/> <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClr"/></button> 		
 	</div>
</div>
 	</div>
</div>

<div class="modal hide"  id="deleteCertRec" style="border: 0px solid blue;" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><spring:message code="btnX"/></button>
		<h3 id="myModalLabel"><spring:message code="headTm"/></h3>
	</div>
	<div id="deact" class="modal-body"> 		
		<div class="control-group">
			<spring:message code="msgDeleteTheRecord"/>
		</div>
 	</div>
 	<div class="modal-footer">
 	<input type="hidden" id="certificateTypeID"/>
 	<span id=""><button class="btn  btn-primary" onclick="deleteCertificateRecord()"><spring:message code="btnOk"/><i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClr"/></button> 		
 	</div>
</div>
 	</div>
</div>
<div style="display:none;" id="loadingDiv">
     <table  align="center" >
 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
 		<tr id='paymentMessage'><td style='padding-top:0px;' id='spnMpro' align='center'>Your file is being uploaded...</td></tr>
	</table>
</div>
<div class="modal hide"  id="removeUploadedDocument" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><spring:message code="btnX"/></button>
		<h3 id="myModalLabel"><spring:message code="headTm"/></h3>
	</div>
	<div id="deact" class="modal-body"> 		
		<div class="control-group">
		<spring:message code="msgDeleteTheRecord"/>
		</div>
 	</div>
 	<div class="modal-footer">
 	<input type="hidden" id="academicID"/>
 	<span id=""><button class="btn  btn-primary" onclick="removeUploadedDocument()" ><spring:message code="btnOk"/> <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClr"/></button> 		
 	</div>
	</div>
	</div>
</div>
<!--  Display  Video Play Div -->
<div  class="modal hide" id="videovDiv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width: 40%; " >
		<div class="modal-content">		
			<div class="modal-header">
			 		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="">x</button>
				<h3 id="myModalLabel"><spring:message code="lblVid"/></h3>
			</div>
			<div class="modal-body">		
				<div class="control-group">
						<div style="display:none;" id="loadingDivWaitVideo">
						     <table  align="center" >						 		
						 		<tr><td align="center"><img src="images/loadingAnimation.gif"/></td></tr>
							</table>
						</div>						
						<div id="interVideoDiv"></div>
				</div>
			</div>
			<div class="modal-footer">	 			
	 		<button class="btn" id="closeBtn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClsoe"/></button>
 			</div>			
		</div>
	</div>
</div>


<input type="hidden" id="additionDocumentId" value="">
<script type="text/javascript">

$('.accordion').collapse();
$('.accordion').on('show', function (e) {
$(e.target).prev('.accordion-heading').find('.accordion-toggle').addClass('minus').removeClass('plus');
});

$('.accordion').on('hide', function (e) {
$(this).find('.accordion-toggle').not($(e.target)).removeClass('minus').addClass('plus');
});
</script>

<script type="text/javascript">
$('#iconpophover1').tooltip();
$('#iconpophover2').tooltip();
showGrid();

getElectronicReferencesGrid();
$('#iconpophover4').tooltip();
$('#iconpophover5').tooltip();
$('#iconpophover6').tooltip();
$('#iconpophover7').tooltip();
$('#iconpophoverCertification').tooltip();
getVideoLinksGrid();
showGridAdditionalDocuments();
</script>

<script>

function fileContainsVirusDiv(msg)
{
	$('#loadingDiv').hide();
	$('#virusDivId .modal-body').html(msg);
	$('#virusDivId').modal('show');
}
</script>

<div class="modal hide" id="virusDivId" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog-for-cgcoverletter" style='width: 35%;'>
		<div class="modal-content">
			<div class="modal-header">
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body" style="max-height: 400px;overflow-y:auto;">
				<div class="control-group">
				</div>
			</div>
			<div class="modal-footer">
				<span id=""><button class="btn btn-primary" data-dismiss="modal">Ok <i class="icon"></i></button></span>&nbsp;&nbsp;
			</div>
		</div>
	</div>
</div>
