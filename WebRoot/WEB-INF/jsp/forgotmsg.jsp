<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div class="row offset4 msgPage">
	<div class="span8 mt30">
		<spring:message code="msgYourRequestHasBeenSent" /><br/>
		<spring:message code="msgResetPasswordLink"/><br/> 
		<spring:message code="msgPleaseCheckYourEmail"/>		
		<c:choose>
      		<c:when test="${smartpractices eq 1}">
      			<a href="smartpractices.do"><spring:message code="lnkClickHere" /></a> <spring:message code="lblToGoBck" />
      		</c:when>
      		<c:otherwise>
	    		<a href="signin.do"><spring:message code="lnkClickHere" /></a> <spring:message code="lblToGoBck" />
      		</c:otherwise>
		</c:choose>
	</div>
</div>


