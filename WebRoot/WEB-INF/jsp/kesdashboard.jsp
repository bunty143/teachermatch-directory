<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>

<script type="text/javascript" src="dwr/interface/HeadQuarterAjax.js?ver=${resourceMap['HeadQuarterAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/interface/AutoSearchFilterAjax.js?ver=${resourceMap['AutoSearchFilterAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>

 <link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" />

<style>
#thin-donut-chart {
    width: 300px; 
    height: inherit;
	position:absolute;
    top:8.2px;
    float:left;
    left:5%;
    margin:auto;   
}
.center-label, .bottom-label {
    text-anchor: middle;
    alignment-baseline: middle;
    pointer-events: none;
    font-size: 27px;
    font-weight: bold;
    color:white;
   }
.half-donut .center-label {
    alignment-baseline: initial;
}
a {
  color: #007AB4;
  text-decoration: none;  
  font-size: 11px; /* it is uncommented for tm dashboard text*/
}
div#wrapper {
	margin:0 auto;
/* width:986px;   */
	background:#FFF;

}
div#feed {
	//width:230px;
	//padding:10px;
	float:left;
}
div#sideBar {
	width:684px;
	//padding:10px;
	margin-left:10px;
	float:left;
	//border: 1px solid red;
}
.clear { 
	clear:both; 
}
div#sticker {
	padding:10px;
	//margin:10px 0;
	//background:#AAA;
	width:684px;
}
.stick {
	position:fixed;
	top:0px;
}

<!-- ---------------------------- -->

.items{
margin-top: 0px;
}
.dcard{
//height: 125px;
//border: solid #F8F8F8 1px;
border: 1px solid #dddddd;
//border-color:#cccccc!important;
width: 285px;
margin-bottom: 7px;
//background-color: #F2FAEF;
//background-color: #F8F8F8;
background-color: #F0F8FF;
//background-color: #F0F0F0;
}

#scrolling {
    position: absolute;
    top: 110px;
    right: 20px;
    border: 1px solid black;
    //background: #eee;
}
#scrolling.fixed {
    position: fixed;
    top: 0px;
}
.circle1 {
  display: block;
  width: 60px;
  height: 60px;
  margin: 1em auto;
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center center;
  -webkit-border-radius: 99em;
  -moz-border-radius: 99em;
  border-radius: 99em;
  border: 1px solid #eee;
  box-shadow: 0 3px 2px rgba(0, 0, 0, 0.3);  
  background-color: pink;
}

.test_content{}
.scroller_anchor{}
.scroller{}

.circle_img{
 -moz-border-radius:52px;
 -webkit-border-radius:52px;
 border-radius:52px;
width:60px;
height:60px;

}
.tool{}
.profile{}
.icon-circle{
font-size: 6em;
color:pink;
}
  .popover-title {
  margin: 0;
  padding: 0px 0px 0px 0px;
  font-size: 0px;
  line-height: 0px;
  }
  .popover-content {
   margin: 0;
   padding: 0px 0px 0px 0px;
  }
.popover.right .arrow {
  top: 100px;
  left: -10px;
  margin-top: -10px;
  border-width: 10px 10px 10px 0;
  border-right-color: #ffffff;
}
.popover.right .arrow:after {
  border-width: 11px 11px 11px 0;
  border-right-color: rgba(0, 0, 0, 0.25);
  bottom: -11px;
  left: -1px;
}

.popover.bottom {
  margin-left: 10px;
}
.popover.bottom .arrow {
  top: -10px;
  left: 15%;
  margin-left: -10px;
  border-width: 0 10px 10px;
  border-bottom-color: #007AB4;
}
.popover.bottom .arrow:after {
  border-width: 0 11px 11px;
  border-bottom-color: rgba(0, 0, 0, 0.25);
  top: -1px;
  left: -11px;
}
.modal_header_profile {

  border-bottom: 1px solid #eee;
  background-color: #0078b4;
  text-overflow: ellipsis; 
  border: 1px solid #999;
  border: 1px solid rgba(0, 0, 0, 0.3);
  *border: 1px solid #999;
  -webkit-border-radius: 6px;
     -moz-border-radius: 6px;
          border-radius: 6px;
  outline: none;
  -webkit-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
     -moz-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
          box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
  -webkit-background-clip: padding-box;
     -moz-background-clip: padding-box;
          background-clip: padding-box;
}

.net-widget-footer 
{
	border-bottom: 1px solid #cccccc; 
	border-left: 1px solid #cccccc;
	border-right: 1px solid #cccccc;
	line-height:40px; 
	background-color: #F2FAEF;
	background-image: -moz-linear-gradient(top,  #FFFFFF, #FFFFFF);
	background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#FFFFFF), to(#FFFFFF));
	background-image: -webkit-linear-gradient(top,  #FFFFFF, #FFFFFF);
	background-image: -o-linear-gradient(top, #FFFFFF, #FFFFFF);
	background-image: linear-gradient(to bottom,#FFFFFF, #FFFFFF);
	background-repeat: repeat-x;
	color:#4D4D4E;
	vertical-align: middle;
	
}
.net-corner-bottom 
{ 
	-moz-border-radius-bottomleft: 12px; -webkit-border-bottom-left-radius: 12px; border-bottom-left-radius: 12px; -moz-border-radius-bottomright: 12px; -webkit-border-bottom-right-radius: 12px; border-bottom-right-radius: 12px; 
}
.joborderwidth
{
	width: 990px;
	margin-left:-446px;
}


.scrollspy_profile { height:500px; overflow-y:auto; overflow-x:hidden;padding-left:5px;
}
.divwidth
{
	width:728px;
}
.tablewidth
{
width: 900px;
}
.custom-div-border1
{
border: 1px solid #999;
border: 1px solid rgba(0, 0, 0, 0.3);
-moz-border-radius: 6px;
border-radius: 6px;
}
.custom-div-border
{
padding:1px;
border-bottom-left-radius:2em;
border-bottom-right-radius:2em;
}
.pdfDivBorder
{
z-index: 5000;
border: 1px solid #999;
border: 1px solid rgba(0, 0, 0, 0.3);
-webkit-border-radius: 6px;
-moz-border-radius: 6px;
border-radius: 6px;
-webkit-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
-moz-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
-webkit-background-clip: padding-box;
-moz-background-clip: padding-box;
background-clip: padding-box; 
}
.custom-border
{
margin-left: -32px;
margin-top: 2px;
}

 .fa-circle:before {
  content: "\f04d";
}
.iconcolor1{
color: #FF1919
}

.iconcolor2{
color: #FFF019
}
.iconcolor3{
color:#A4D53A
}

.toolTip {
    position: absolute;
    display: none;
    width: auto;
    height: auto;
    background: none repeat scroll 0 0 white;
    border: 0 none;
    border-radius: 8px 8px 8px 8px;
    box-shadow: -3px 3px 15px #888888;
    color: blue;
    font: 15px sans-serif;
    padding: 5px;
    text-align: center;
    
}
 .legend {
	            padding: 5px;
	            font: 10px sans-serif;
	            background: yellow;
	            box-shadow: 2px 2px 1px #888;
            }

</style>


<!-- Anurag for new  dashboard-->

		<style>
			.customicon{
				vertical-align:middle;
				width:35px;
				height:35px;
				border:none;
			}
			.ribbon ,.ribbon1 ,.ribbon2 {
				display:inline-block;
			}
			.ribbon:after   {
				margin-top:0.5em;
				content: "";
				float:left;
				border:23px solid #F4F6F0;
				border-right-color:transparent;
			}
			.ribbon #link{ 
				color:#000;
				text-decoration:none;
			    float:left;
			    height:52px;
				overflow:hidden;
			}
			.ribbon span {
				background:#F4F6F0;
				display:inline-block;
			 	line-height:1.5em;
				padding:0 5px;
				margin-top:0.5em;
				position:relative;
				-webkit-transition: background-color 0.2s, margin-top 0.2s;  /* Saf3.2+, Chrome */
				-moz-transition: background-color 0.2s, margin-top 0.2s;  /* FF4+ */
				-ms-transition: background-color 0.2s, margin-top 0.2s;  /* IE10 */
				-o-transition: background-color 0.2s, margin-top 0.2s;  /* Opera 10.5+ */
				transition: background-color 0.2s, margin-top 0.2s;
			}
			.ribbon1:after   {
				margin-top:0.5em;
				content: "";
				float:left;
				border:23px solid #E1ECF7;
				border-right-color:transparent;
			}
			.ribbon1 #link{ 
				color:#000;
				text-decoration:none;
			    float:left;
			   height:52px;
				overflow:hidden;
			}
			.ribbon1 span {
				background:#E1ECF7;
				display:inline-block;
			 	line-height:1.5em;
				padding:0 5px;
				margin-top:0.5em;
				position:relative;
				-webkit-transition: background-color 0.2s, margin-top 0.2s;  /* Saf3.2+, Chrome */
				-moz-transition: background-color 0.2s, margin-top 0.2s;  /* FF4+ */
				-ms-transition: background-color 0.2s, margin-top 0.2s;  /* IE10 */
				-o-transition: background-color 0.2s, margin-top 0.2s;  /* Opera 10.5+ */
				transition: background-color 0.2s, margin-top 0.2s;
			}
	  		.ribbon2:after   {
				margin-top:0.5em;
				content: "";
				float:left;
				border:23px solid #FDF8F2;
				border-right-color:transparent;
			}
 
			.ribbon2 #link { 
				color:#000;
				text-decoration:none;
			    float:left;
			 height:52px;
				overflow:hidden;
			}

			.ribbon2 span {
				background:#FDF8F2;
				display:inline-block;
			 	line-height:1.5em;
					padding:0 5px;
				margin-top:0.5em;
				position:relative;

				-webkit-transition: background-color 0.2s, margin-top 0.2s;  /* Saf3.2+, Chrome */
				-moz-transition: background-color 0.2s, margin-top 0.2s;  /* FF4+ */
				-ms-transition: background-color 0.2s, margin-top 0.2s;  /* IE10 */
				-o-transition: background-color 0.2s, margin-top 0.2s;  /* Opera 10.5+ */
				transition: background-color 0.2s, margin-top 0.2s;
			}
  
  .result_num{

font-family: 'Arial Black', 'Arial Bold', Gadget, sans-serif;
	font-size: 22px;
	font-style: normal;
	font-variant: normal;
	font-weight: 400;

	}

	.result_text{ 
		color:grey; 
	}
	.result_table{
	
	width:120px; 
	float:right;  border-collapse: collapse;
	height: 46px;
	text-align: center;
	
	}
	
	.mybtn{
	
	
  -webkit-box-shadow: 0px 1px 2px #eee;
  -moz-box-shadow: 0px 1px 2px #eee;
   box-shadow: 0px 1px 2px #eee;
  
  }
  
  
 		

 
		</style>
<input type="hidden" id="branchdashboard" value="${branchDashboard}"/>

<div class="row col-sm-12 col-md-12 modalTM hide" id='searchItem' style="margin-top: 15px;margin-left:0px;padding: 15px;min-width:800px;">	
  	   <div class='divErrorMsg' id='errordiv1' style="padding-left: 15px;"></div> 	  
       <div class="col-sm-5 col-md-5">      
          <label><spring:message code="lblBranchName"/></label>
		  <input type="text" id="branchName" name="branchName"  class="form-control" value="${branchName}"
    		onfocus="getBranchMasterAutoComp(this, event, 'divTxtShowDataBranch', 'branchName','branchSearchId','');"
			onkeyup="getBranchMasterAutoComp(this, event, 'divTxtShowDataBranch', 'branchName','branchSearchId','');"
			onblur="hideBranchMasterDiv(this,'branchSearchId','divTxtShowDataBranch');"	/>
		  <div id='divTxtShowDataBranch' style='display:none;position:absolute;z-index:5000;' onmouseover="mouseOverChk('divTxtShowDataBranch','branchName')" class='result' ></div>
			<input  type="hidden" id="branchSearchId" name="branchSearchId" value=""/>							      
       </div>
       <div class="col-md-2">       		
			<button class="btn btn-primary top25-sm" style="width: 100px;" type="button" onclick="searchData();"><spring:message code="btnSearch"/> <i class="icon"></i></button>         	
       </div>
       <label  style="float: right;margin-right:-10px;" id='closePan' class="">&nbsp;</label>
</div> 
<div class="modalsa" style="display: none;" id="sa">
	<div class="" style="padding-top: 55px;">
		<a href='javascript:void(0);' onclick="getSearchPan()"><span class='icon-search icon-large iconcolor' style="font-size: 0.99em;"></span><b>&nbsp;<spring:message code="lnkSearchAg"/><img src="images/arrow_left_animated.gif"/></b></a>
	</div>
</div>
<div id="wrapper" class="hide" style="">
	<div class="row top10">
		<div class="col-sm-6 col-md-6">
			<p style="font-size: 14px; font-weight: bold;">
				This
				<span id="resulyType"> Week </span> At A Glance
			</p>
		</div>
		<div class="col-sm-6 col-md-6 ">
			<div class="btn-group btn-group-xs" style="float: right" role="group"
				aria-label="View">
				<label style="float: left; margin-right: 10px; font-weight: bold">
					View
				</label>
				<button type="button" class="btn btn-default mybtn" id="weekly"
					style="background: #fff;" onclick="reportByTime('week')">
					Week
				</button>
				<button type="button" class="btn btn-default mybtn" id="monthly"
					style="background: #fff;" onclick="reportByTime('month')">
					Month
				</button>
			</div>
		</div>
		<!--<button style="background: #9BBB59; border:1px solid;">week</button> <button style="background: white;  border:1px solid;">Month</button>-->
	</div>
	<div class="row">
		<div class="col-md-4 col-sm-4">
			<div class='ribbon'>
				<div id="link">
					<span> <span> <IMG class="customicon"
								SRC="images/job.jpg">
							<br> </span>
						<table class="result_table" cellspacing="0" cellpadding="0">
							<tr>
								<td style="padding-top: 6px;">
									<p id="activeJobs" class="result_num" style="margin: 0px;">
										 
									</p>
									<p class="result_text" style="margin: 0px;">
										Active Jobs
									</p>
								</td>
							</tr>

						</table> </span>
				</div>

			</div>
		</div>
		<div class="col-md-4 col-sm-4">
			<div class='ribbon1'>
				<div id="link">
					<span> <span> <IMG class="customicon"
								SRC="images/candidate.JPG">
							<br> </span>
						<table class="result_table" cellspacing="0" cellpadding="0">
							<tr>
								<td style="padding-top: 6px;">
									<p id="totalApplicants" class="result_num" style="margin: 0px;">
									</p>
									<p class="result_text" style="margin: 0px;">
										Total Applicants
									</p>
								</td>
							</tr>

						</table> </span>
				</div>

			</div>
		</div>
		<c:if test="${empty branchMaster}">
		<div class="col-md-4 col-sm-4">
			<div class='ribbon2'>
				<div id="link">
					<span> <span> <IMG class="customicon"
								SRC="images/branches.JPG">
							<br> </span>
						<table class="result_table" cellspacing="0" cellpadding="0">
							<tr>
								<td style="padding-top: 6px;">
									<p id="activeBranches" class="result_num" style="margin: 0px;">
										 
									</p>
									<p class="result_text" style="margin: 0px;">
										Active Branches
									</p>
								</td>
							</tr>

						</table> </span>
				</div>

			</div>
		</div>
		</c:if>
	</div>

	<div class="row top20">
	
		<div class="col-sm-5 col-md-5" >
		<!-- mukesh -->
 
			<div id='container1' class="" style="height: 350px;margin-left: 0px;padding-bottom: 20px;padding-top: 15px;"></div>


			<div>
			<p class="center subheading"> # of Talent who have achieved " <spring:message code="msgHired2"/>"</p>
			</div>
		</div>	
		
		<div class="col-sm-7 col-md-7"  >
		   			
			<div id='container3' class="" style="height: 350px;margin-left: 0px;padding-bottom: 5px;"></div>
		 
		 
		 <div>
			<p class="center subheading">Top 3 Job Categories</p>
			</div>	
		</div>	
		
	  </div>
	  
 	<!--     <div class="row">  		 
			<div class="col-sm-12 col-md-12" style="width: 660px;">
				<div class="subheadingnew" style="padding-top: 5px;padding-bottom:3px;border: solid red 0px;">
					<span style="cursor: pointer;"> 
					<span class='btn-group'><a data-toggle='dropdown' > Candidate Stats <span class="carethead"></span></a></span>
					</span>
				</div>	
			    <div id='container4' class="table-bordered" style="height: 250px;margin-left: 0px;"></div>
			</div>			
	<%-- 		
			<div class="col-sm-6 col-md-6" style="width: 330px;">
				<div class="subheadingnew" style="padding-top: 5px;padding-bottom:3px;border: solid red 0px;">
					<span style="cursor: pointer;"> 
					<span class='btn-group'><a data-toggle='dropdown' >Hiring Velocity<span class="carethead"></span></a></span>
					</span>
				</div>	
				<div id='container2' class="table-bordered" style="height: 250px;margin-left: 0px;"></div>
			</div>	
			--%>	
	   </div>  -->
    <div class="clear"></div>
  
</div>
 

<div style="display:none; z-index: 5000;" id="loadingDiv" >
     <table  align="center" >
 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
	</table>
 </div>
<input type="hidden" id="headQuarterId"  value="${headQuarterMaster.headQuarterId}"/>
  
<script type='text/javascript' src='js/d3.js'></script>
 
<script src="highcharts/js/highcharts.js"></script>
<script src="highcharts/js/modules/exporting.js"></script>
<script src="highslide/highslide-full.min.js"></script>
<script src="highslide/highslide.config.js"></script>
<link rel="stylesheet" type="text/css" href="highslide/highslide.css" />
  
 
 <script type="text/javascript" src="js/kesdashboard.js"></script>
<script type="text/javascript" src="js/canvasjs.min.js"></script>
<script type='text/javascript' src="js/branchautoSuggest.js"></script>

<script>     
	showDivData()
</script>


