<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<style>
	p
	{
		text-align: justify;
		padding-right: 5px;
		/*text-indent: 20px;*/
	}
	.spnIndent
	{
		padding-left: 20px;
	}
	.spnIndent40
	{
		padding-left: 40px;
	}
	ul
	{
	margin-right: 10px;
	}
</style>
			

		
<div class="container">
			<div class='divErrorMsg' id='errordiv' style="display: block;">				
			</div>
		</div>
	

		<div class="container">
			 <div class="row centerline">
					<img src="images/affidavit.png" width="47" height="48" alt="">
					<b class="subheading" style="font-size: 13px;">
					<spring:message code="lnkTermsUse"/>
				    </b>
		     </div>
		     
		    <div style="text-align: center; padding-bottom: 5px;font-family:Times New Roman;font-weight:bold;font-size:16px;">
		    </br>
		    <span><spring:message code="headTmCandP"/></span>
		    </br>
		<spring:message code="headTAndCondOfUSE"/>
		    </div>		
		</div>
        <div class="container top15">
             <div class="row" align="justify">                                                   
          <div class="scrollspy-example termsheight right10" data-offset="0" data-target="#navbarExample" data-spy="scroll">
			
			<p style="text-indent: 0px;"><spring:message code="pUseCarefullyBeforeUsingTheTm"/></p>
			
			<div style="text-align: center; padding-bottom: 5px;">
				<spring:message code="pCopyrightTm"/><br/>
				<spring:message code="pAllRiRe"/><br/>
			</div>
			
			<p>
				<b><spring:message code="pOWNERSHIP"/></b>  <spring:message code="pEachOfItsCompIsCopyrightedPropertyOfTm"/></p>
			<p>
				<b><spring:message code="pAgeAdRespo"/></b> <spring:message code="pYuRepresntAtLeast(18)Yr"/></p>
			<p>
				<b><spring:message code="pAccesingThePorAdAccSecurity"/></b> <spring:message code="pEachTchCandMayBeProvidedWithUseraAdPss"/>			
				<br/><br/><span  class="spnIndent"></span><spring:message code="pInConnWithEstablishingYurAccWithUs"/></span> 
				<br/><br/><span  class="spnIndent"></span><spring:message code="pYuAcknowledgeAdAgreeThatTmHasNoResponsibility"/> <a href="mailto:clientservices@teachermatch.net">clientservices@teachermatch.net</a>. <spring:message code="pInAddDelAccCodeAtAnyTimeAdReason"/></span>
			</p>
			<p>
				<b><spring:message code="pSubmissions"/></b> 
                               <spring:message code="pAllInfoOfTm"/>
				<br/><br/><span  class="spnIndent"></span><spring:message code="pNoConfidentialRelEstabBySubMinssionByAnyCandTch"/>
			</p>
			<p>
				<b><spring:message code="pRulOfCon"/></b> <spring:message code="pWhileUThPortYuWillApplicableLaws"/>
				<div class="spnIndent40">
				<ul>  
					<li><spring:message code="pManyInfoOfTm"/></li>
					<li><spring:message code="pCivilLiability"/></li>
					<li><spring:message code="pVirusWormTrHorsEasterEggTime"/></li>
					<li><spring:message code="pAnyPersIdentiInfoOfAthrIndividual"/></li>
					<li><spring:message code="pMateralNonPuInfoAbtCompany"/></li>
				</ul>
				</div>
				<spring:message code="pFurtherInConnWithYurUseOfPortal"/>
				<div class="spnIndent40" style="padding-top: 5px;">
				<ul>
					<li><spring:message code="pUseThePorForFraudulent"/></li>
					<li><spring:message code="pImperAnyPersonOrEntity"/></li>
					<li><spring:message code="pInterfaceWiDisruptUseNet"/></li>
					<li><spring:message code="pRestOrInhibit"/></li>
					<li><spring:message code="pModiAdTransReveEngineer"/></li>
				</ul>
				
				</div>
			</p>
			<p>
				<b><spring:message code="pUSEOFPORTAL"/>
			</p>  
			<p>
				<b><spring:message code="pPRIVACY"/></b><br/>				
				
					<div  style="padding-left: 50px; "><spring:message code="pPRIVACY1"/><br/> <br/>
					</div>
					<div   style="padding-left: 50px; padding-right: 10px;"><spring:message code="pPRIVACY2"/><br/>  
					<br/><spring:message code="pPRIVACY3"/> <br/><br/>
					</div>					
					<div  style="padding-left: 50px; margin-right: 10px;" ><spring:message code="pPRIVACY4"/><br/><br/>  
					</div>
					<div class="right10"  style="padding-left: 50px;padding-right: 10px; "><spring:message code="pPRIVACY5"/><br/>
					<ul>
						<li><spring:message code="pPRIVACYL1"/></li>
						<li><spring:message code="pPRIVACYL2"/></li>
						<li><spring:message code="pPRIVACYL3"/></li>
						<li><spring:message code="pPRIVACYL4"/></li>
						<li><spring:message code="pPRIVACYL5"/></li>
					</ul>
					</div>
				
			</p>
			<p>
				<b><spring:message code="p8USEOFDATA"/></b> <spring:message code="p8USEOFDATA1"/>  
				<br/><br/><span  class="spnIndent"></span><spring:message code="p8USEOFDATA2"/>
				<br/><br/><span  class="spnIndent"></span><spring:message code="p8USEOFDATA3"/>
				<br/><br/><span  class="spnIndent"><spring:message code="p8USEOFDATA4"/>
				<br/><br/><span  class="spnIndent"><spring:message code="p8USEOFDATA5"/>	
				</p>
			<p>
				<b><spring:message code="head9EXCLUSIONOFWARRANTY"/></b> <spring:message code="pEXCLUSIONOFWARRANTY"/>	</p>
			<p>
				<b><spring:message code="head10LIMITATIONOFLIABILITY"/></b>  <spring:message code="p10LIMITATIONOFLIABILITY"/>	
				<span  class="spnIndent"></span><br/><br/><spring:message code="p10LIMITATIONOFLIABILITY2"/>
				<span  class="spnIndent"></span><br/><br/><spring:message code="p10LIMITATIONOFLIABILITY3"/>
				</p>
			<p>
				<b><spring:message code="head11INDEMNIFICATION"/></b>  <spring:message code="p11INDEMNIFICATION"/>
					</p>
			<p>
				<b><spring:message code="head12RELATIONSHIP"/> </b> <spring:message code="p12RELATIONSHIP"/>
						</p>
			<p>
				<b><spring:message code="head13GOVERNINGLAW"/></b> <spring:message code="p13GOVERNINGLAW"/></p>
			<p>
				<b><spring:message code="head14ASSIGNMENT"/></b><spring:message code="p14ASSIGNMENT"/></p>
			<p>
				<b><spring:message code="head15MODIFICATION"/></b> <spring:message code="p15MODIFICATION"/></p>
			<p>
				<b><spring:message code="head16SEVERABILITY"/></b>  <spring:message code="p16SEVERABILITY"/></p>
			<p>
				<b><spring:message code="head17HEADINGS"/></b> <spring:message code="p17HEADINGS"/></p>
			<p>
				<b><spring:message code="head18ENTIREAGREEMENT"/></b> <spring:message code="p18ENTIREAGREEMENT"/></p>	
			<p  style="text-indent: 0px;">
				<b><spring:message code="headHOWTOCONTACTUS"/></b><spring:message code="pComments" /> <a href="mailto:clientservices@teachermatch.net">clientservices@teachermatch.net</a> <spring:message code="pWrtUsAt" /></p>
			
						
				<spring:message code="pTmLLC"/><br/>
				<spring:message code="p4611NRaveU"/><br/>
				<spring:message code="pChiIL60640"/><br/>
			<spring:message code="pAttentionTe"/><br/><br/>
				
				
			
			<spring:message code="pLaRevJune"/>
			<hr/>
			<p style="text-indent: 0px;" >
				<spring:message code="pUnderstandTheForegoingTerms"/>
			</p>
			<hr/>
			<p style="text-indent: 0px;" >
				<spring:message code="pTmPotentialEmp"/>
			</p>  
		</div>  
		
		<a href="javascript:history.go(-1)"><spring:message code="btnBack"/></a>	
		
      </div>
      				  
	</div>
<input type="hidden" id="portfolioStatus" value="${portfolioDone }" />

<script type="text/javascript" src="js/teacher/portfolio.js">
</script>