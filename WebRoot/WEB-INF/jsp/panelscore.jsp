<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<script type="text/javascript" src="dwr/interface/CandidateGridAjax.js?ver=${resouceMap['CandidateGridAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type='text/javascript' src="js/report/candidategrid.js?ver=${resouceMap['js/report/candidategrid.js']}"></script>
<script type='text/javascript' src="js/report/cgstatus.js?ver=${resouceMap['js/report/cgstatus.js']}"></script>
<script type="text/javascript" src="dwr/interface/ManageStatusAjax.js?ver=${resouceMap['ManageStatusAjax.ajax']}"></script>	
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resouceMap['css/base.css']}" />
<script type="text/javascript" src="dwr/interface/PanelAjax.js?ver=${resouceMap['PanelAjax.ajax']}"></script>	
<link href="css/font-awesome.min.css?ver=${resouceMap['css/font-awesome.min.css']}" rel="stylesheet" type="text/css">
<link href="css/font-awesome-ie7.css?ver=${resouceMap['css/font-awesome-ie7.css']}" rel="stylesheet" type="text/css">
<link href="css/font-awesome-ie7.min.css?ver=${resouceMap['css/font-awesome-ie7.min.css']}" rel="stylesheet" type="text/css">
<link href="css/font-awesome.css?ver=${resouceMap['css/font-awesome.css']}" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>

<script>
var $j=jQuery.noConflict();
$j(document).ready(function() {
});

</script>
<style>
	.hide
	{
		display: none;
	}
</style>
<input type="hidden" id="panelId" value='${panelSchedule.panelId}'/>
<input type="hidden" id="userId" value='${userMaster.userId}'/>
<input type="hidden" id="jobId" value='${panelSchedule.jobOrder.jobId}'/>
	 <iframe src="" id="ifrmTrans" width="100%" height="100%" scrolling="auto" class="hide"> </iframe> 

	<br/><br/>
	<input type="hidden"  name="jobCategoryDiv" id="jobCategoryDiv" value="2"/>
	<input type="hidden"  name="jobCategoryFlag" id="jobCategoryFlag"/>
	<input type="hidden" id="offerAccepted" name="offerAccepted" value="1"/>
	
	<input type="hidden" id="doActivity" name="doActivity" value="true"/>
	<input type="hidden" id="hireFlag" name="hireFlag" value="0"/>
	<input type="hidden" id="panelOpenFlag" name="1"/>
	<input type="hidden" id="jobForTeacherIdForSNote" name="jobForTeacherIdForSNote"/>
	<input type="hidden" id="statusIdForStatusNote" name="statusIdForStatusNote" value="0"/>
	<input type="hidden" id="fitScoreForStatusNote" name="fitScoreForStatusNote" value="0"/>
	<input type="hidden" id="finalizeStatus" name="finalizeStatus" value="0"/>
	<input type="hidden" id="teacherStatusNoteId" name="teacherStatusNoteId" value="0"/>
	<input type="hidden" id="statusNoteDeleteType" name="statusNoteDeleteType" value="0"/>
	<input type="hidden" id="teacherIdForprofileGrid" name="teacherIdForprofileGrid"/>
	<input type="hidden" id="updateStatusHDRFlag" name="updateStatusHDRFlag"/>
	<input type="hidden" id="secondaryStatusIdForStatusNote" name="secondaryStatusIdForStatusNote" value="0"/>
	<div class="row col-md-12" id="showTemplateslist_mass" style="display:none">	</div>
	<div class=""  id="jWTeacherStatusNotesDiv"  tabindex="" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">	
	<div class="" style="width: 670px;">
	<div class="">
	<div class="modal-header">
		<h3 id="myModalLabel"><span id="status_title">Status</span></h3>
		<input type="hidden" id="statusName" name="statusName" value="">
		<input type="hidden" id="statusShortName" name="statusShortName" value="">
		<input type="hidden" id="isEmailTemplateChanged" name="isEmailTemplateChanged" value="0">
		<input type="hidden" id="isEmailTemplateChangedTeacher" name="isEmailTemplateChangedTeacher" value="0">
	</div>
	<div class="" >
		<div class="control-group">
			<input type="hidden" name="scoreProvided" id="scoreProvided" />
			<div class='divErrorMsg' id='errorStatusNote' style="display: block;"></div>
			<div id="divStatusNoteGrid" ></div>
			
			<div id="noteMainDiv" class="hide">
				<div class="row mt5 left5" >
				    	<div class="span6" id='statusNotes' style="width: 665px;"  >
				    	<label class="">Note<span class="required">*</span></label>
				    		<textarea readonly id="statusNotes" name="statusNotes" class="span6" rows="2"  ></textarea>
				    	</div> 
				</div>  
				<iframe id='uploadStatusFileFrameID' name='frmStatusFileFrame' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'>
				</iframe>
				<form id='frmStatusNote' enctype='multipart/form-data' method='post' target='frmStatusFileFrame' onsubmit="saveStatusNote(0);"  class="form-inline" action='statusNoteUploadServlet.do' accept-charset="UTF-8">
				<input type="hidden" id="statusTeacherId" name="statusTeacherId" value="${panelSchedule.teacherDetail.teacherId}"/>
				<div class="row left5" style="margin-top: 10px;">  	
						<div class="span4" >
				    		<div id='statusNoteFileNames'>
			        			<a href='javascript:void(0);' onclick='addStatusNoteFileType();'><img src='images/attach.png'/></a> <a href='javascript:void(0);' onclick='addStatusNoteFileType();'>Attach a File</a>
			        		</div> 
				    	</div>
				    	<div class="span4" id="showStatusNoteFile" >
			        	</div>   
				</div>
				</form>		
				
		 	</div> <!-- Main Div -->
		 	
		 	<div class="row mt10" id="schoolAutoSuggestDivId" style="display: none;">
            	<div class="span11">
            		<c:if test="${entityType eq 2}">
					  	<label>School Name<span class="required">*</span></label>
		 				<input type="text" id="schoolName" maxlength="100" name="schoolName" style="width: 645px;" placeholder="" onfocus="getSchoolAuto_djo(this, event, 'divTxtShowData2', 'schoolName','districtOrSchoolId','');"
							onkeyup="getSchoolAuto_djo(this, event, 'divTxtShowData2', 'schoolName','districtOrSchoolId','');"
							onblur="hideSchoolMasterDiv_djo(this,'districtOrSchoolId','divTxtShowData2'),requisitionNumbers();"	/>
							
					 	 <input type="hidden" id="schoolId"/>
						 <div id='divTxtShowData2' style=' display:none;' onmouseover="mouseOverChk_djo('divTxtShowData2','schoolName')" class='result' ></div>
				 	</c:if>
		    	</div>
            </div>
            
            <div class="row" id="requisitionNumbersGrid"  style="display: none;">
            		<c:if test="${entityType ne 1}">
					<label style='padding-top:10px; '>Requisition Numbers<span class="required">*</span> (It is used if we are hiring a Candidate. In case of "In-Progress", Requisition Number will not be saved)</label>
 					<span id="requisitionNumbers"> </span>
 					</c:if>
            </div>
            <span id="spanOverride" style='display:none;'>
            	<div class="row mt10">
	            	<div class="span10">
            			<label class='checkbox inline'>
            				<input type='checkbox' name='chkOverride' id='chkOverride' value='1'/>
            				Override the Status
            			</label>
			    	</div>
	            </div>
            </span>
            
		 	<span id="checkOptions" style='display:none;'>
	           
			 	<div class="row mt10" id="email_da_sa_div">
	            	<div class="span10">
	            		<label class="checkbox inline">
					 	<input type="checkbox" name="email_da_sa" ${disabledChecked} id="email_da_sa"  value="1"/>
	                 	Notify all the associated District and School Admins of the status change</label>
						<a href='javascript:void(0)' class="${hideView}" style="padding-left: 90px;" onclick="getStatusWiseEmailForAdmin()">View/Edit Message</a>
			    	</div>
	            </div>
	            
	            <div class="row mt10" id="email_ca_div">
	            	<div class="span10">
	            		<label class="checkbox inline">
					 	<input type="checkbox" name="email_ca" ${disabledChecked} id="email_ca"  value="1"/>
	                 	Notify the candidate of the status change</label>
	                	<a href='javascript:void(0)' class="${hideView}" style="padding-left: 248px;" onclick="getStatusWiseEmailForTeacher()">View/Edit Message</a>
			    	</div>
	            </div>
            </span>
		</div>
 	</div>
 	<div class="modal-footer">
	 	<table width=630 border=0>
	 		<tr>
		 		<td width=115 nowrap align=left>
		 		<span id="statusSave" style='display:none;'><button class="btn  btn-large btn-orange"  onclick='saveStatusNote(0)'>In-Progress</button>&nbsp;&nbsp;</span>
		 		</td>
		 		<td width=125  nowrap align=left>
			 		<span id="unHire" style='display:none;'><button class="btn  btn-large btn-primary" onclick='updateStatusHDR(2)'>UnHire </button></span>
			 		<span id="unDecline" style='display:none;'><button class="btn  btn-large btn-primary" onclick='updateStatusHDR(3)'>UnDecline </button></span>
			 		<span id="unRemove" style='display:none;'><button class="btn  btn-large btn-primary" onclick='updateStatusHDR(4)'>UnReject </button></span>
		 		</td>
		 		<td width=305  nowrap>
				<span id="statusFinalize" ><button class="btn  btn-large btn-primary" onclick='saveStatusNote(1)'>Finalize <i class="iconlock"></i></button>&nbsp;&nbsp;</span>		 		
		 		<!--<button class="btn btn-large" data-dismiss="modal" aria-hidden="true" onclick='jWTeacherStatusNotesOnClose()'>Cancel</button>
		 		--></td>
	 		</tr>
	 	</table>
    </div>
</div>
</div>
</div>
<div  class="modal hide" id="modalDownloadIAFN" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 5000;">
	<div class="modal-dialog-for-cgdemoschedule">
	<div class="modal-content">
	<div class="modal-header" id="modalDownloadIAFNMove" style="cursor: move;">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabelInstructionHeader"></h3>
	</div>
	<div class="modal-body">		
		<div class="control-group">
			<div id="" class="">
				<iframe src="" id="ifrmAttachInstructionFileName" width="100%" height="480px">
				 </iframe>        	
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button> 		
 	</div>
</div>
</div>
</div>
<div  class="modal hide"  id="myModalViewCompleteTeacherStatusNotes"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" >x</button>
		<h3 id="myModalLabel">Status Notes</h3>
	</div>
	<div class="modal-body">		
		<div class="control-group">
			<div id="myModalViewCompleteTeacherStatusNotesInnerText">
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" >Close</button> 		
 	</div>
</div>
</div>
</div>

<div class="modal hide"  id="modalStatusNoteMsg"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  style="z-index: 5000;"  data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  >x</button>
		<h3 id="myModalLabel">Status</h3>
	</div>
	<div class="modal-body">		
		<div class="control-group">
			<div id="statusNoteMsg">
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick='closeStatusNoteMsg(1);'>Ok</button> 
 		<button class="btn" data-dismiss="modal" aria-hidden="true" >Cancel</button> 		
 	</div>
</div>
	</div>
</div>

<div style="display:none; z-index: 5000;" id="loadingDiv" >
    <table  align="center" >
 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
 		<tr id='paymentMessage'><td style='padding-top:0px;' id='spnMpro' align='center'></td></tr>
	</table>
</div>


<div  class="modal hide"  id="myModal2Panel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="window.location.href='logout.do'">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div class="modal-body">
		<div class="control-group" id='message2showPanel'>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn btn-primary" data-dismiss="modal" aria-hidden="true" onclick="window.location.href='logout.do'">Ok</button>
 	</div>
</div>
</div>
</div>

<div class="modal hide"  id="messageDiv"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  style="z-index: 5000;"  data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  >x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div class="modal-body">		
		<div class="control-group">
			<spring:message code="msgDataSaved" /> 
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true">Ok</button> 
 	</div>
	</div>
	</div>
</div>

<script type="text/javascript">
    //$('#jWTeacherStatusNotesDiv').modal('show');
	if(${errorMsg}==0)
	{	
		//$('#message2show').html("Valid.");
		//$('#myModal2').modal('show');
		getStatusForPanel();
		
	}else if(${errorMsg}==1)
	{	
		$('#message2showPanel').html("This is not a valid URL.");
		$('#myModal2Panel').modal('show');
		$('#jWTeacherStatusNotesDiv').hide();
		
	}if(${errorMsg}==2)
	{	
		$('#message2showPanel').html("You are not an External user.");
		$('#myModal2Panel').modal('show');
		$('#jWTeacherStatusNotesDiv').hide();
			
	}if(${errorMsg}==3)
	{	
		$('#message2showPanel').html("You are an Invalid Panelist.");
		$('#myModal2Panel').modal('show');
		$('#jWTeacherStatusNotesDiv').hide();
	}
	if(${errorMsg}==4)
	{	
		$('#message2showPanel').html("Panel is Cancelled.");
		$('#myModal2Panel').modal('show');
		$('#jWTeacherStatusNotesDiv').hide();
	}if(${errorMsg}==5)
	{	
		$('#message2showPanel').html("Panel is Completed.");
		$('#myModal2Panel').modal('show');
		$('#jWTeacherStatusNotesDiv').hide();
	}if(${errorMsg}==6)
	{	
		$('#message2showPanel').html("Panel is not active.");
		$('#myModal2Panel').modal('show');
		$('#jWTeacherStatusNotesDiv').hide();
	}
</script>
                 
                 