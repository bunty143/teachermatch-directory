<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<link href="css/font-awesome.css?ver=${resourceMap['css/font-awesome.css']}"  rel="stylesheet" type="text/css" >
<link href="css/font-awesome-ie7.min.css?ver=${resourceMap['css/font-awesome-ie7.min.css']}"  rel="stylesheet" type="text/css">
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>		
<script type="text/javascript" src="dwr/interface/DashboardAjax.js?ver=${resouceMap['DashboardAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/JBServiceAjax.js?ver=${resourceMap['JBServiceAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/interface/QuestAjax.js?ver=${resourceMap['QuestAjax.ajax']}"></script>
<script type="text/javascript" src="js/jobboard/jobboard.js" charset="utf-8"></script>
<link type="text/css" rel="stylesheet" href="css/jobboard.css" charset="utf-8" />
<!-- <script type="text/javascript" src="js/jquery.1.11.2.min.js" charset="utf-8"></script> -->

<!-- shadab -->

<script type="text/javascript" src="js/jquery.min.js" charset="utf-8"></script>
<script src="js/bootstrap.min.js"></script>
<!-- shadab end -->
<style type="text/css">
.btn-info {
  color: #ffffff;
  text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
  background-color: #006dcc;
  *background-color: #0044cc;
  background-image: -moz-linear-gradient(top, #0088cc, #0044cc);
  background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#0088cc), to(#0044cc));
  background-image: -webkit-linear-gradient(top, #0088cc, #0044cc);
  background-image: -o-linear-gradient(top, #0088cc, #0044cc);
  background-image: linear-gradient(to bottom, #0088cc, #0044cc);
  background-repeat: repeat-x;
  border-color: #0044cc #0044cc #002a80;
  border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff0088cc', endColorstr='#ff0044cc', GradientType=0);
  filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);
}
.btn-info:hover
{
 color: #ffffff;
  background-color: #0044cc;
  *background-color: #003bb3;
}
textarea {
    resize: none;
}
</style>
<script type="text/javascript" src="js/jquery-te-1.3.2.2.min.js" charset="utf-8"></script>
<link type="text/css" rel="stylesheet" href="css/jquery-te-1.3.2.2.css" charset="utf-8" /> 
<script src="https://maps.google.com/maps/api/js?key=AIzaSyCxMPQC8CqpPbxMhZbIuROrjXVbqASZOvg&sensor=false" type="text/javascript"></script>
<!--<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
-->



<div class="top5 span10" style="width:100%;">
		<!-- Top Search -->
		<!-- <div class="row">
			<div class="col-sm-9 col-md-9">
				<input type="text" name="quickSearch" id="quickSearch">
			</div>
		</div> -->
		
		<!-- Part 1 -->
		
		<!--  <div class="col-sm-9 col-md-9" style="border: 2px solid;border-radius: 25px;"> -->
		<div class="col-sm-8 col-md-8" style="padding:1px;overflow: auto;">
			<div class="sParent" style="height: 100%;">
				<div class="sidebar" style="height: 440px;">
				<!-- shadab start -->
				<div class="row" style="width:650px;">
						<div class="col-sm-12 col-md-12">
							<!-- <span class='icon-leaf icon-large iconcolor'></span> -->
							<label style="font-size: 12px;margin-bottom: 4px;" >Keyword Search </label>
						 	<input  type="text" 
								class="form-control"
								maxlength="100"
								id="searchTerm" 
								value=""
								
								name="searchTerm" 
								style="width:95%;font-size:12px;"
								 onkeypress="return runScript(event)"
								/>
							
							
						</div>
						
						</div>
						
							<!-- <br> -->
						<!--<div class="row" style="margin-top: 12px;">
							<div class="col-sm-12 col-md-12">
								<span><button class="btn btn-primary" style="width: 588px; height: 40px;font-size:12px;" id="firstSearch" type="button" onclick="searcESJob()">Search</button></span>
							</div>
						</div>
						<br>
						--><!-- shadab end-->
					<!--  -->
						<!-- 1st Row -->
						<div class="row" style="width:650px;  margin-top: -13px;">
						<div class="col-sm-7 col-md-7">	
							<!-- <span class='icon-leaf icon-large iconcolor'></span> -->
							<label style="font-size: 12px;margin-top: 20px;">District ( leave blank to search across all districts )</label>
						 	<input  type="text" 
								class="form-control"
								maxlength="100"
								id="districtName" 
								value=""
								name="districtName" 
								onfocus="getDistrictMasterAutoComp(this, event, 'divTxtDistrictData', 'districtName','districtId',''); showSchool();"
								onkeyup="getDistrictMasterAutoComp(this, event, 'divTxtDistrictData', 'districtName','districtId',''); showSchool();" 
								onblur="hideDistrictMasterDiv(this,'districtId','divTxtDistrictData'); showSchool();hidestate();"
								onchange="removeSchool()"
								style="font-size: 12px;width: 94%"/>
							<input  type="hidden" id="districtId" name="districtId" value="">
							<div id='divTxtDistrictData' style=' display:none;position:absolute; z-index: 2000;width:329px' 
							onmouseover="mouseOverChk('divTxtDistrictData','districtName')" class='result' ></div>
						</div>
						
						<div class="col-sm-4 col-md-4" style="width:220px;">
						<!-- <span class='icon-home icon-large iconcolor'></span> -->
						<label style="font-size: 12px;  margin-left: -35px;margin-top: 20px;">State</label>
						<div id="stateDivId" style="width:129%;  margin-left: -37px;font-size:12px;">
						</div>
						</div>
						
					  
						</div>
						<!-- 2nd Row -->
						
						<div class="row" style="margin-top: 7px;">
						  <div class="col-sm-8 col-md-8">
					    <!-- <span class='icon-home icon-large iconcolor'></span> -->
				       	<span class=""><label style="font-size: 12px;">City</label></span>
				       	<div id="cityDivId" style="width:83%;font-size:12px">
						<select id="cityId" name="cityId" class="form-control" disabled="disabled" style="font-size:12px">
						<option value="0" selected="selected">All</option>
						</select>
						</div>
						</div>
						</div>
						<div class="row" style="margin-top: 7px;">		
							<div class="col-sm-6 col-md-6">
								<!-- <span class='icon-tasks icon-large iconcolor'></span> -->
					       		<span class=""><label style="font-size: 12px;margin-bottom: 3px;">Posted Within Month(s)</label></span>
					       		<table>
					       			<tr>
					       				<td style='padding-left:0px;' id="JBPostedWithin"></td>
					       			</tr>
					       		</table>
					       		<input type="hidden" name="postedWithin" id="postedWithin" />
				       		</div>			    
						</div>
					<!-- 3rd Row -->
					
					<div class="row" style="padding-top:12px;width:80%;" id="moreFilterBtn">
						<div class="col-sm-12 col-md-12">
							<span>
							<!-- <img src="images/dropdown.png" id="" width="15" height="15" alt="" onclick="displayJBAdvanceSearch()" style="display:block"></a> -->
								<button class="btn btn-info" style="height:40px;font-size:12px;" type="button" onclick="displayJBAdvanceSearch()">More Filter</button>
								<!-- shadab commented lines -->
								<!-- <button class="btn btn-primary" style="height: 40px;" type="button" onclick="searchJob()">Search</button> -->
							</span>
						</div>
					</div>
						
					
					<div id="moreFilterDiv" class="hide" style="padding-top: 0px;">
						<div class="row">
							<div class="col-sm-6 col-md-6">
								<!-- <span class='icon-building icon-large iconcolor'></span> -->
								<label style="font-size: 12px;">School Name</label>
								<input  type="hidden" id="schoolId1" name="schoolId1" value="">
								<input  type="text" 
								style="width: 114%;font-size:12px;"
									class="form-control"					
									maxlength="100"
									id="schoolName1" 
									disabled="disabled"
									autocomplete="off"
									value=""
									name="schoolName1" 
									onfocus="getSchoolMasterAutoComp(this, event, 'divTxtShowData4', 'schoolName1','schoolId1','districtId','');"
									onkeyup="getSchoolMasterAutoComp(this, event, 'divTxtShowData4', 'schoolName1','schoolId1','districtId','');" 
									onblur="hideSchoolMasterDiv(this,'schoolId1','divTxtShowData4');setBlankZip();"/>
								<div id='divTxtShowData4' style=' display:none;position:absolute; z-index: 2000;width:330px;' 
								onmouseover="mouseOverChk('divTxtShowData4','schoolName1')" class='result' ></div>
								
								<!-- <span class='icon-certificate icon-large iconcolor'></span> -->
					           	<label style="font-size: 12px;margin-top:7px">Certification/Licensure Name (leave blank for all)</label>
					            <input type="hidden" id="certificateTypeMaster" value="">
								<input type="text" 
									class="form-control"
									style="width: 114%;font-size:12px"
									maxlength="100"
									id="certType" 
									value=""
									name="certType" 
									onfocus="getAllCertificateTypeAutoComp(this, event, 'divTxtCertTypeData', 'certType','certificateTypeMaster','');"
									onkeyup="getAllCertificateTypeAutoComp(this, event, 'divTxtCertTypeData', 'certType','certificateTypeMaster','');" 
									onblur="hideAllCertificateTypeDiv(this,'certificateTypeMaster', 'divTxtCertTypeData');"/>
								<div id='divTxtCertTypeData' style=' display:none;position:absolute;width:330px' onmouseover="mouseOverChk('divTxtCertTypeData','certType')" class='result' ></div>
								
								<!-- <span class='icon-tag icon-large iconcolor'></span> -->
								<label style="font-size: 12px;margin-top:7px">Zip Code ( leave blank for all )</label>
							  	<input class="form-control" type="text" name="zipCode" id="zipCode" style="width: 114%;font-size: 12px;" onkeypress="return checkForInt(event); " />
							  
							</div>
							
							<div class="col-sm-6 col-md-6">
								<!-- <span class='icon-book icon-large iconcolor'></span> -->
						        <label style="font-size: 12px;margin-left: 28px;">Subject&nbsp;&nbsp;<a href="#" id="iconpophover102" rel="tooltip" data-original-title="To select more than one subject, please hold the Shift key and select multiple subjects"><img src="images/qua-icon.png" width="15" height="15" alt=""></a></label> 
								<select id="subjectId" name="subjectId" class="form-control" multiple="multiple" size="7" style="height:168px;font-size:12px;  width: 84%;margin-left: 27px;">
									<option value="0">All</option>
									<c:if test="${not empty lstMasterSubjectMasters}">	
										<c:forEach var="masterSubjectList" items="${lstMasterSubjectMasters}">
											<option value="${masterSubjectList.masterSubjectId}">${masterSubjectList.masterSubjectName}</option>				
										</c:forEach>
									</c:if>
								</select>
								
								  	 
							</div>
						</div>
						
						<!--<div class="row" style="padding-top: 20px;">
							<div class="col-sm-12 col-md-12">
								<span><button class="btn btn-primary" style="width: 592px; height: 40px;margin-bottom:-10px;font-size:12px" type="button" onclick="hideJBAdvanceSearch()">Show Listings</button></span>
							</div>
						</div>
						
					--><!-- 
						<div class="row">
							<div class="col-sm-6 col-md-6">
								<label>Label 1</label>
								Input Control 1
							</div>
							<div class="col-sm-6 col-md-6">
								<label>Label 2</label>
								Input Control 2
							</div>
						</div>
						-->
						
						
					</div>
					
					
					<div id="searchButtonDiv"  style="padding-top: 0px;">
						<div class="row" style="margin-top: 12px;">
							<div class="col-sm-12 col-md-12">
								<span><button class="btn btn-primary" style="width: 588px; height: 40px;font-size:12px;" id="firstSearch" type="button" onclick="searcESJob()">Search</button></span>
							</div>
						</div>
						<br>
						
						<div class="row hide" id="ShowListingsId"  style="padding-top: 0px;">
							<div class="col-sm-12 col-md-12">
								<span><button class="btn btn-primary" style="width: 592px; height: 40px;margin-bottom:-10px;font-size:12px" type="button" onclick="hideJBAdvanceSearch()">Show Listings</button></span>
							</div>
						</div>
					</div>
					
						
					<div class="row"  style="min-height: 0px;width:98%;padding:5px;margin-left:4px;margin-top:5px;">
							<!--<div class="col-sm-10 col-md-10">
								--><div class="TableContent top15" style="  margin-left: -10px;" >
									<div class="table-responsive" id="divJBData" ></div>
								</div><!--          
							</div>
						--></div>
						
					<!--  -->
					
				</div>
			</div>	
		</div>
		<!--  Part 2 -->
		<div class="col-sm-3 col-md-3">
			<!--  Map Area -->
			<!-- script type="text/javascript" 
			src="http://www.webestools.com/google_map_gen.js?lati=44.534384&long=-86.800047&zoom=7&width=400&height=550&mapType=normal&map_btn_normal=yes&map_btn_satelite=yes&map_btn_mixte=yes&map_small=yes&marqueur=yes&info_bulle=">
			-->
			<div id="jbMap" style="width: 460px; height: 443px;"></div>
			
			<!--  -->
		</div>  
	  									
</div> 
<div class="modal hide" id="registerModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" style="">		
		 <div class="modal-dialog">
          <div class="modal-content">
			<div class="modal-header">	 
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick='hideDiv();'>
					x
				</button> 		
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body">	
			<div class='divErrorMsg' id='signUpServerError' style="display: block;margin-bottom: 10px;"></div>			             
			    <div class='divErrorMsg' id='signUpErrordiv' style="display: block;margin-bottom: 10px;"></div>			    			
				<h1 class="font25 top0" style="">Sign Up</h1>				
				<div class="row">
					<div class="col-sm-12 col-md-12 ">
					<div class="" style="color: black;">First Name<span class="required">*</span></div>
		            <input type="text" id="fname" class="form-control" style="height: 40px; width: 100%;padding: 10px;"/>		           
					</div>
					</div>
				<div class="row">
					<div class="col-sm-12 col-md-12">
					 <div class="" style="color: black;">Last Name<span class="required">*</span></div>
		            <input type="text" id="lname" class="form-control" style="height: 40px; width: 100%;padding: 10px;"/>	          
					</div>
					</div>
						<div class="row">	           		        
			        <div class="col-sm-12 col-md-12 " >
			         <div class="" style="color: black;">Email<span class="required">*</span></div>
			           <input type="text" id="signupEmail" class="form-control" style="height: 40px; width: 100%;padding: 10px;"/>
			        </div>	           							
		 	</div>
		 	</div>
		 	<!--<div class="row">
		 	<div class="col-sm-12 col-md-12" style="color: black;">Email<span class="required">*</span></div>
	            <input type="text" id="signupEmail" class="form-control" style="height: 40px; width: 100%;padding: 10px;"/>
		 	</div>
				</div>			 
	            -->							
				 	
		 	<div class="modal-footer">			 	
		 	<button class="btn btn-primary" type="button" onclick="return signUpTempUser();">Sign Up</button>	 	
		    <button class='btn btn-primary' data-dismiss='modal' aria-hidden='true' onclick="hideDiv()">Cancel</button>		 			
		 	</div>
		</div>
	  </div>
</div>

	


<div style="display:none;" id="loadingDiv">
	<table  align="center" >
		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
	</table>
</div>



<form method="post" action="setcl.do" onsubmit="return checkCL();">
				<div  class="modal hide"  id="myModalCL" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
					<div class="modal-dialog-for-cgmessage">
					<div class="modal-content">
					<div class="modal-header">
				  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
						<h3 id="myModalLabel">Cover Letter</h3>
					</div>
					<div class="modal-body" >	
					<div style="padding-top: 10px;padding-bottom: 10px;">
									<label class="redtextmsg">When you click continue, if you are not already logged into a TeacherMatch account, you will be directed to a log-in page, where you will need to log in or set up an account to continue with your application.</label>
				   				</div>
					<p id="cvrltrTxt" class="hide">If you are applying to a principal or Central Office position, please provide a cover letter. Applications without cover letters will be considered incomplete and will not be reviewed.</p>
						<div class='divErrorMsg' id='errordivCL' style="display: block;"></div>						
						<label class="">
					        <input type="radio"  value="1" id="rdoCL1" name="reqType" onclick="setCLEnable()">
					        <span style="padding-top: 5px;">Please type in your cover letter</span>	
				   		</label>							
						<div id="divCoverLetter">
							<label class="redtextmsg">(If you are planning to copy and paste text from a web page and/or a Microsoft document,
				   			 please ensure to paste that text first into a simple text editor such as Notepad to remove
				   			 the special characters and then copy and paste it into the field below. Otherwise,
				   			 the text directly cut and pasted from a web page and/or a Microsoft document may not 
				   			 display properly to the reader)</label>
				   			<textarea id="coverLetter"  name="coverLetter" rows="7" cols="100"></textarea>
				   		</div>	
				   		</br>	
						<label class="">
					        <input type="radio"  value="1" id="rdoCL2" name="reqType"  onclick='setClBlank()'>
					        <span>I do not want to add a cover letter</span>
				   		</label>
				   		<label class="checkbox">
					        <input type="checkbox"  value="1" id="isAffilated" name="isAffilated">
					        I am currently affiliated with this District/School&nbsp;<a href="#" id="iconpophover101" rel="tooltip" data-original-title="Please select this box if you are currently working with this District and/or any School within this District as a full time or part time employee or consultant/contractor"><img src="images/qua-icon.png" width="15" height="15" alt=""></a>
				   		</label>
				 	</div>
				 	<div class="modal-footer">
				 		<button class="btn btn-large btn-primary" type="submit" ><strong>Continue <i class="icon"></i></strong></button>
				 		<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button> 		
				 	</div>
				</div>
				</div>
				</div>
	 </form>
	 <div class="modal hide" id="appliedJob" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" style="">		
		 <div class="modal-dialog">
          <div class="modal-content">
			<div class="modal-header">	  		
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body">				
				<p>You have already applied this job</p>			
		 	</div> 	
		 	<div class="modal-footer">		 	
		    <button class='btn btn-primary' data-dismiss='modal' aria-hidden='true' onclick="hideDiv()">Ok</button>		 			
		 	</div>
		</div>
	  </div>
</div>

<div  class="modal hide"  id="applyJobComplete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  style="z-index: 99999;"  data-backdrop="static">
		<div class="modal-dialog">
		<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  >x</button>
		<h3>Teachermatch</h3>
	</div>
	<div class="modal-body">	
		<div class="control-group">
			<div class="">
		    	<span><b>You have successfully applied this job</b></span>	        	
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="hideInfoBlog();">Ok</button> 		
 	</div>
</div>
</div>
</div>
			<input type="hidden" name="tempjobid" id="tempjobid"/>
		 	<input type="hidden" name="exitUrl" id="exitUrl"/>
		 	<input type="hidden" name="district" id="district"/>
		 	<input type="hidden" name="school" id="school"/>
		 	<input type="hidden" name="criteria" id="criteria"/>
		 	<input type="hidden" name="epistatus" id="epistatus"/>
		 	<div id="from">0</div>
<script type="text/javascript">
	$('#iconpophover102').tooltip();
	getSlider();
	displayState();
	
	
	/* shadab commented only single line */
//	displayRecordsInit();
//displayJobRecords();
    var map = new google.maps.Map(document.getElementById('jbMap'), {
      zoom: 4,
      center: new google.maps.LatLng(37.09024, -95.712891),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });
   
    
</script>
<script>
$('textarea').jqte();
</script>
<script type="text/javascript">
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-40462543-1', 'teachermatch.org');
  ga('send', 'pageview');
</script>
<script type="text/javascript" src="dwr/interface/AssessmentCampaignAjax.js?ver=${resouceMap['AssessmentCampaignAjax.ajax']}"></script>
<script type='text/javascript' src="js/assessment-campaign.js?ver=${resouceMap['js/assessment-campaign.js']}"></script>


<!-- shadab start -->
<script>

<%if(request.getParameter("quick")!=null)
{%>
	
	document.getElementById("searchTerm").value='<%=request.getParameter("quick") %>';
<%} %>
//searchDocument(0);
searcESJob(0);
</script>

<!-- shadab end -->