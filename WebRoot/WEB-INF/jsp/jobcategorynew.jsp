<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>

<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type='text/javascript' src="js/districtApprovalGroupNew.js?ver=${resourceMap['js/districtApprovalGroupNew.js']}"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<script type="text/javascript" src="dwr/interface/ManageJobOrdersAjax.js?ver=${resourceMap['ManageJobOrdersAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/JobCategoryAjaxNew.js?ver=${resourceMap['JobCategoryAjaxNew.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/DistrictAjax.js?ver=${resourceMap['DistrictAjax.ajax']}"></script>
<script type='text/javascript' src="js/jobcategorynew.js?ver=${resourceMap['js/jobcategorynew.js']}"></script>
<script type="text/javascript" src="dwr/interface/I4QuestionSetAjax.js?ver=${resourceMap['I4QuestionSetAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/DistrictAssessmentAjax.js?ver=${resourceMap['DistrictAssessmentAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/HqJobCategoryAjax.js?ver=${resourceMap['HqJobCategoryAjax.ajax']}"></script>
<script type="text/javascript" src="js/selfServiceCommon.js?ver=${resourceMap['js/selfServiceCommon.js']}"></script>
<script type="text/javascript">
var $j=jQuery.noConflict();
        $j(document).ready(function() {
        });
        
function applyScrollOnTbl() {
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#jobCategoryTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 400,
        width: 862,
        minWidth: null,
        minWidthAuto: false,
        colratio:[135,175,70,60,80,90,80,60,100], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
     });			
}
</script>

<style>
/*.multi1.multi2{
background-color: D3D3D3;
}*/
.input-group-addon
{
	background-color: #fff;
	border: none;
}
.disabled{
    pointer-events: none;
}
.widthDiv
{
width:85.5%;
}
.divWidth
{
width:77.5%;
}
.divshadow
{
box-shadow: 0px 1px 1px 1px #999;
border-radius: 3px;
}
.txtalign
{
text-align: center;
margin-top:6px;
}
.showhideedit
{
margin-top:15px;
}
.lfloat
{
float:left;
width: 20px;
height: 20px;
margin-left:5px;
}

.linkDisabled {
   pointer-events: none;
   cursor: default;
}
.widthCl
{
width:25%;
}

.colorNo{
background-color: #73D2EF;
}
.widthFull{
width:100%;
}

</style>
<input type="hidden" id="previousDistrictId"/>
<input type="hidden" id="editMode" value="0"/>
<input type="hidden" id="editClone" value="0"/>
<input type="hidden" id="userType" value="${entityID}"/>
<input type="hidden" name="jobcategoryId" id="jobcategoryId">
<input type="hidden" id="entityID" value="${entityID}"/>
<!-- Div For Domain Grid   -->
<div class="row divshadow" style="width:99.5%;margin-left:3px;">
  <div class="col-sm-12 col-md-12" style="box-shadow: 0px 0px 0px 0px #999;background-color: #f7f7f7;" >
    <div class="row" >
        <div class="col-sm-1 col-md-1" style="background-color: rgb(241, 218, 218);height: 35px;">
         	<img src="images/Recruitingdspq1.png" alt="" style="margin-top: 7px;margin-left: 5px;">
        </div>        
        <div class="col-sm-8 col-md-8" style="padding-left: 0px;">
         	<div class="subheading" style="font-size: 13px;"><spring:message code="headMngJoCateNew"/></div>	
        </div>
		<div class="col-sm-3 col-md-3" id="addJobCategory" style="line-height: 34px;text-align: right;">
			<a href="javascript:void(0);" onclick="addNewJobCategory(1);addJobCategoryApprovalHideGroup();"> <spring:message code="lnkAdJoCate"/></a>	
			<span data-toggle="tooltip" data-placement="left" style="cursor: pointer" title="<spring:message code="lnkAdJoCateTool"/>"><img src="images/qua-icon.png" alt="">					
			</span>
		</div>
		<div class="col-sm-3 col-md-3 hide" id="backJobCategory" style="line-height: 34px;text-align: right;">
			<a href="javascript:void(0);" onclick="cancelJobCateDiv(1);"> <spring:message code="lnkBackJoCate"/></a>	
			<span data-toggle="tooltip" data-placement="left" style="cursor: pointer" title="<spring:message code="lnkBackJoCateTool"/>"><img src="images/qua-icon.png" alt="">					
		    </span>
		</div>
    </div>
  </div>
<div class="col-sm-12 col-md-12" style="box-shadow: 0px 0px 0px 0px #999;background-color: white;" >
<div id="maingrid">
<% String id=session.getAttribute("displayType").toString();%>
<input type="hidden" id="displayTypeId" value="<%=id%>"/>
<%if(id.equalsIgnoreCase("0")){ %>
<c:if test="${entityID eq 1}">	
<div  style="margin-top:-10px;" onkeypress="return chkForEnterSearchJobOrder(event);">
	   <div class="row top15">
			<form class="bs-docs-example" onsubmit="return false;">
				<div class="<c:out value="${hide}"/>">
					<div class="col-sm-3 col-md-3">
						<label><spring:message code="lblEntityType"/> </label>
					    <select class="form-control" id="entityType" name="entityType" class="form-control" onchange="displayOrHideSearchBox();">
						 <c:if test="${not empty userMaster}">
						 	 <c:if test="${userMaster.entityType eq 1}">	
			         			<option value="1"   selected="selected" ><spring:message code="sltTM"/> </option>
								<option value="2"><spring:message code="lblDistrict"/> </option>
							</c:if>	
			        		<c:if test="${userMaster.entityType eq 2}">	
			         			<option value="2"  selected="selected" ><spring:message code="lblDistrict"/> </option>
							</c:if>
			              </c:if>
						</select>			
	              	</div>
		            <div  id="Searchbox" class="<c:out value="${hide}"/>">	         		 
		             		<div class="col-sm-7 col-md-7">
		             			<label id="captionDistrictOrSchool"> <spring:message code="lblDistrict"/> </label>
			             		<input type="text" id="districtNameFilter" name="districtNameFilter" class="form-control"
			             		onfocus="getDistrictMasterAutoComp(this, event, 'divTxtShowDataFilter', 'districtNameFilter','districtIdFilter','');"
								onkeyup="getDistrictMasterAutoComp(this, event, 'divTxtShowDataFilter', 'districtNameFilter','districtIdFilter','');"
								onblur="hideDistrictMasterDiv(this,'districtIdFilter','divTxtShowDataFilter');"/>
								<input type="hidden" id="districtIdFilter" value="${userMaster.districtId.districtId}"/>
								<div id='divTxtShowDataFilter'  onmouseover="mouseOverChk('divTxtShowDataFilter','districtNameFilter')"
								 style=' display:none;position:absolute;z-index: 5000' class='result' ></div>	
			             	</div>		             
	          		   </div>
					<div class="col-sm-2 col-md-2">	
					 <div class='mt25'>				
						<button class="btn btn-primary" style="width:100%" type="button" onclick="searchJobCat()"><spring:message code="headSearch"/> <i class="icon"></i></button>					
					 </div>
					</div>
        	  	</div>
       		 </form>
		</div>            
</div>
</c:if>
<%}%>
<div class="row">
 <div class="col-sm-12 col-md-12 top20 left15 pleftright" >
		<div class="row left0" style="background-color:#007fb2;color:white; padding:7px 0px 1px 5px; width:100%;">
			<div class="col-sm-2 col-md-2 pleft3" style="width:21%;"><spring:message code="lblJoCatN"/></div>
			<div class="col-sm-2 col-md-2 pleft0" style="width:20%;"><spring:message code="optDistrict"/></div>
			<div class="col-sm-1 col-md-1 pleft0"><spring:message code="lblCandidateType"/></div>
			<div class="col-sm-1 col-md-1 pleft0" style="width:4%;"><spring:message code="DSPQlblEPI"/></div>
			<div class="col-sm-1 col-md-1 pleft0" style="width:6%;"><spring:message code="lblPortfolio"/></div>
			<div class="col-sm-1 col-md-1 pleftright"><spring:message code="DSPQlblPreScreen"/></div>
			<div class="col-sm-1 col-md-1 pleftright"><spring:message code="msgOnBoarding"/></div>
			<div class="col-sm-1 col-md-1 pright0 pleft2"><spring:message code="DSPQlblQuestions"/></div>
			<div class="col-sm-1 col-md-1 pleft7" style="width:6.5%;"><spring:message code="lblStatus"/></div>
			<div class="col-sm-1 col-md-1"><spring:message code="lblAct"/></div>
		</div>
        <div class="row left0" id="JobCategoryDataGrid" style="background-color:white;padding:0px 0px 1px 5px; width:100%;">
		  <div id="jobCategoryGrid"></div>
		</div>
  </div>
</div>
</div>
</div>
</div>
<!--         Job  Category Grid End -->
<div class="row top5">
	  <div class="col-sm-8 col-md-8">			                         
 			<div class='divErrorMsg' id='errordiv' ></div>
	  </div>	
</div>   
	<div id="jobCatDiv" class="hide" onkeypress="return chkForEnterSaveJobOrder(event);" style="margin-left:3px;">	  
		  <div class="row top3">
			   <c:if test="${entityID eq 1}">	
		          	<div  class="col-sm-6 col-md-6">
		          		<label class="mb0"><spring:message code="lblDistrict" /> </label><span class="required"> *</span>
		          		<span data-toggle="tooltip" data-placement="top" title="" data-original-title="<spring:message code="lblDistrict"/>"><img src="images/qua-icon.png" > </span>
						<input type="text" id="districtName" name="districtName"  class="form-control mt5" onfocus="getDistrictMasterAutoComp(this, event, 'divTxtShowData', 'districtName','districtId','');"
						onkeyup="getDistrictMasterAutoComp(this, event, 'divTxtShowData', 'districtName','districtId',''); ;"
						onblur="hideDistrictMasterDiv(this,'districtId','divTxtShowData');resetSectionCheckBoxByDistrict();"	/>
						<div id='divTxtShowData' style=' display:none;position:absolute;z-index:5000;' onmouseover="mouseOverChk('divTxtShowData','districtName')" class='result' ></div>
						<input type=hidden id="districtId"/>
		           	</div>
	           	</c:if>
	        </div>  
	        <c:if test="${entityID eq 2}">	
                 <input type="hidden" id="districtId" value="${districtId}"/>
			</c:if>	        
	     <div class="row mt5">
	      <div class="col-sm-2 col-md-2" style="width: 18%;">
	      <span data-toggle="tooltip" data-placement="top" title="" data-original-title="<spring:message code="lblwouldliketotool"/>"><spring:message code="lblwouldliketo"/> <img src="images/qua-icon.png" ></span>
		    </div>
		    <input type="hidden" name="jobCatType" id="jobCatType"/>
		    <div class="col-sm-4 col-md-4">
		     <span class="radio" style="margin-top:0px;">
				<label><input type="radio" id="createJobCategory" name="jobCategoryRadio" onclick="hideOrShowCreateJob(1)">
				<strong><spring:message code="lblcreateamainjobcategory"/></strong></label>
				<span data-toggle="tooltip" data-placement="top" title="" data-original-title="<spring:message code="lblcreateamainjobcategorytool"/>"><img src="images/qua-icon.png" ></span>	
		     </span>
		    </div>
		    <div class="col-sm-4 col-md-4" style="padding-left:4px;">
		      <span class="radio" style="margin-top:0px;">
				<label>
				<input type="radio" id="selectJobCategory" name="jobCategoryRadio" onclick="hideOrShowCreateJob(2)">
				<strong><spring:message code="lblsubcategory"/></strong></label>
				<span data-toggle="tooltip" data-placement="top" title="" data-original-title="<spring:message code="lblsubcategorytool"/>"><img src="images/qua-icon.png" >
		     </span>
		     </span>
		    </div>
		 </div>
		 <div class="row newJobCategory hide">
		   	<div class="col-sm-6 col-md-6" style="text-align: center;">
				<input type="text" name="jobcategory" id="jobcategory" maxlength="100" class="form-control" style="margin-top:-5px;">
			</div>
		 </div>
	    <div class="row selectJobCategory hide">
		   <div class="col-sm-12 col-md-12 " style="height:34px;margin-top:-5px;">
		   <div class="row mb10"> 
			<div class="col-sm-6 col-md-6">
                <select id="parentJobCategoryId" name="parentJobCategoryId" class="form-control" >
                    <option value="0"><spring:message code="optStrJobCat"/> </option>
                    <c:forEach var="jbCategory" items="${lstJobCategoryMasters}">
                        <option value="${jbCategory.jobCategoryId}">${jbCategory.jobCategoryName}</option>              
                    </c:forEach>
                </select>
            </div>            	
				<div class="col-sm-6 col-md-6" id="subJobCatDiv">
                	<input type="text" name="subjobcategory" id="subjobcategory" maxlength="100" class="form-control">             
            	</div>
           </div>
		   </div>
	      </div>
            	<div class="row mt10 hide" id="subJobCatDiv">
            	<div class="col-sm-10 col-md-10 checkbox inline" style="padding-left: 15px;">
					<div class="left20">
						<input type="checkbox" name="attachDSPQFromJC" id="attachDSPQFromJC">
						<label><strong><spring:message code="lblAttachDistrictSpecificMandatory"/></strong></label>
					</div>
				</div>
				<div class="col-sm-10 col-md-10 checkbox inline" style="padding-left: 15px;">
					<div class="left20">
						<input type="checkbox" name="attachSLCFromJC" id="attachSLCFromJC">
						<label><strong><spring:message code="lblAttachStatusLifeCycle"/> </strong></label>
					</div>
				</div>
	      	    <div class="col-sm-5 col-md-5"><label><strong><spring:message code="msgSubJobCategory"/> <span class="required">*</span></strong></label>
                  <input type="text" name="subjobcategory" id="subjobcategory" maxlength="100" class="form-control" style="width:88%">             
            </div>
	      </div>
	      <div class="col-sm-12 col-md-12 mt10" style="border: 1px solid;border-color: rgb(213, 213, 213);width:100%; margin-left:0px;height:40px;">
	      <label style="font-size: 12px; height: 26px;  line-height: 35px;color:#007AB4;"><b><spring:message code="lblInst"/></b></label></div>
	      <div class="row mt10" style="width:99%; font-size:11px;  padding-right:10px; padding-left:32px;">
	      <spring:message code="msgconfigurationssettingsfor"/>  <b>“<spring:message code="lblExternal"/>”</b> <spring:message code="msgcandidatesandbydefault"/>  <b>“<spring:message code="lblInternal1"/>”</b> <spring:message code="msgand"/> <b>“<spring:message code="msginternaltransfer"/>”</b> <spring:message code="msgcandidatesalsosharethesame"/> <b>“<spring:message code="lblInternal1"/>”</b> <spring:message code="msgand"/> <b>“<spring:message code="msginternaltransfer"/>”</b> <spring:message code="msgcandidatesselectthe"/> <b>“<spring:message code="lblInternal1"/>”</b> <spring:message code="msgandor"/> <b>“<spring:message code="msginternaltransfer"/>”</b> <spring:message code="msgcandidatecheckboxesbelow"/></div>
	      <div class="row top10" style="margin-left: -10px;">
	      <div class="col-sm-2 col-md-2" style="height: 41.5px;margin-left: -5px;padding-left:31px;">
	      <label style="margin-top: 10px;font-weight:bold;"><spring:message code="lblCandidateType"/></label>
	      </div>
	      <div class="col-sm-10 col-md-10" style="width: 82%;">
	      <div class="row"><div class="col-sm-4 col-md-4 checkbox">
	      <label class="checkbox inline" style="margin-top: 2px;margin-bottom: 2px;margin-left: -18px;" title="<spring:message code="lblExternalE"/>">
	      <input type="checkbox" id="section0"  onclick="hideOrShowCondidateType(1);" style="margin-left:-8px;" checked disabled="disabled"><spring:message code="lblExternalE"/></label>
	      </div>
	      <div class="col-sm-4 col-md-4 checkbox" style="margin-top: 10px;">
	      <label class="checkbox inline" style="margin-top: 2px;margin-bottom: 2px;margin-left: -25px;" title="<spring:message code="lblInternalI"/>">
	      <input type="checkbox" id="section1" onclick="hideOrShowCondidateType(2);" checked><spring:message code="lblInternalI"/></label>
	      </div>
	      <div class="col-sm-4 col-md-4 checkbox" style="margin-top: 10px;">
	      <label class="checkbox inline" style="margin-top: 2px;margin-bottom: 2px;margin-left: -25px;" title="<spring:message code="lblInternalTransfer"/>">
	      <input type="checkbox" id="section2" onclick="hideOrShowCondidateType(3);" checked><spring:message code="lblInternalTransfer"/></label></div>
	      </div> 
	      </div>
	      </div>
	      <div class="row mt10 mb10" style="margin-bottom:10px;padding-left:16px;">
		       <div class="col-sm-5 col-md-5" style="width:50%;padding-right: 0px;">
		       <label class="circleHead" style="margin-left:0px;background-color:rgb(0, 147, 255);" ><spring:message code="instC"/></label> <spring:message code="lblConfiguration"/>
		       <label class="circleHead colorNo" style="margin-left:30px;"><spring:message code="instN"/></label> <spring:message code="lblNotConfiguration"/>
		       <label class="circleHead" style="margin-left:30px;"><spring:message code="instD"/></label> <spring:message code="lblConfigurationDefault"/>
		       </div> 
		       <div class="col-sm-3 col-md-3" style="padding-right: 0px;">
		       <label class="circleHead" style="background-color:red;"><spring:message code="instR"/></label> <spring:message code="lblRequiredd"/> 
		       <label class="circleHead" style="background-color: #ffc0cb;"><spring:message code="instO"/></label> <spring:message code="lblOptional"/> 
		       </div>
		       <div class="col-sm-3 col-md-3">
		       <label class="circleHead" style="background-color: green;"><spring:message code="instA"/></label>  <spring:message code="optAct"/> 
		       <label class="circleHead" style="background-color: grey;"><spring:message code="instX"/></label> <spring:message code="lblInActiv"/>
		       </div>
		   
	       </div>
	      
	      <div class="row top10" style="border: 1px solid;border-color: rgb(213, 213, 213);width:100%; margin-left:0px;height:40px;">
			   <div class="col-sm-12 col-md-12 leftrightmargin">
			     <label style="font-size: 12px; height: 26px;  line-height: 35px;color:#007AB4; padding-left:12px; font-weight:bold;">
					<spring:message code="msgJobCategoryRequirement"/>
				 </label>
			   </div>
    	 </div>
	  <div>
	  <div class="row" style="font-weight: bold; padding-top:5px; padding-right:15px;  color:#474747;">
	   <div class="col-sm-3 col-md-3 widthCl"><label class="left15"><spring:message code="lblJoCatN"/></label>	  </div>
	   <div class="col-sm-1 col-md-1 "><label><spring:message code="msgEditicon"/></label> </div>
	   <div class="col-sm-1 col-md-1 managewidth" id="extraWidth">	  </div>
	   <div class="col-sm-2 col-md-2 hide externalDiv"><label style="margin-left:40px;"><spring:message code="lblExternal"/></label></div>
	   <div class="col-sm-2 col-md-2 hide internalDiv"><label style="margin-left:42px;"><spring:message code="lblInternal1"/></label></div>
	   <div class="col-sm-2 col-md-2 hide  internalTDiv"><label style="margin-left:30px;"><spring:message code="lblITransfer"/></label> </div>
	   
	   </div>
	  <div class="row mb10">
	   <div class="col-sm-3 col-md-3 widthCl top5"><span class="left15"><spring:message code="lblPortfolio"/></span>  
	   <span data-toggle="tooltip" data-placement="top" title="" data-original-title="<spring:message code="lblPortfoliotool"/>"><img src="images/qua-icon.png"></span>
	   </div>
	   <div class="col-sm-1 col-md-1 showhideedit"><a href="javascript:void(0);" title="<spring:message code="msgEditicon"/>" onclick="clickView(1,'',2);">
        <i class="fa fa-pencil-square-o fa-lg"></i></a>
       </div>
	    <div class="col-sm-1 col-md-1 managewidth" id="extraWidth0">	  </div>
	    <div class="col-sm-2 col-md-2 txtalign hide externalDiv">
	   	    <div class="lfloat"> </div>
               <div class="lfloat">
                  <input type="hidden" id="1EE" name="1EE" value="0"/>
                  <input type="hidden" id="1EEid" name="1EEid" value="0"/>
               <label class="circleMendOpt colorNo" onclick="clickView(1,'E',1);" id="1E" title="" data-toggle="tooltip" data-placement="top" data-original-title="<spring:message code="msgNoPortfolioConfigured"/>" ><spring:message code="instN"/></label>              
              </div>
              <div class="lfloat">
		        <input type="hidden" id="1_1_1" value="A"/>
                  <label style="background-color: green;" class="circleMendOpt" id="1_1" href="javascript:void(0);" title="<spring:message code="optAct"/>" onclick="deactivateSection(1,1);" data-toggle="tooltip" data-placement="top" data-original-title="<spring:message code="optAct"/>">
                   <spring:message code="instA"/>
                  </label> 
                  </div>
        </div>
        <div class="col-sm-2 col-md-2 txtalign hide internalDiv">
	   	    <div class="lfloat"> </div>
               <div class="lfloat">
                  <input type="hidden" id="1II" name="1II" value="0"/>
                  <input type="hidden" id="1IIid" name="1IIid" value="0"/>
               <label class="circleMendOpt colorNo" onclick="clickView(1,'I',1);" id="1I" title="" data-toggle="tooltip" data-placement="top" data-original-title="<spring:message code="msgNoPortfolioConfigured"/>" ><spring:message code="instN"/></label>              
              </div>
              <div class="lfloat">
		        <input type="hidden" id="1_1_2" value="A"/>
                  <label style="background-color: green;" class="circleMendOpt" id="1_2" href="javascript:void(0);" title="<spring:message code="optAct"/>" onclick="deactivateSection(1,2);" data-toggle="tooltip" data-placement="top" data-original-title="<spring:message code="optAct"/>">
                   <spring:message code="instA"/>
                  </label> 
                  </div>
        </div>
        <div class="col-sm-2 col-md-2 txtalign hide internalTDiv">
           <div class="lfloat"> </div>
               <div class="lfloat">
                  <input type="hidden" id="1TT" name="1TT" value="0"/>
                  <input type="hidden" id="1TTid" name="1TTid" value="0"/>
                   <label class="circleMendOpt colorNo" onclick="clickView(1,'T',1);" id="1T" title="" data-toggle="tooltip" data-placement="top" data-original-title="<spring:message code="msgNoPortfolioConfigured"/>"><spring:message code="instN"/></label>              
              </div>
                <div class="lfloat">
		        <input type="hidden" id="1_1_3" value="A"/>
                  <label style="background-color: green;" class="circleMendOpt" id="1_3" href="javascript:void(0);" title="<spring:message code="optAct"/>" onclick="deactivateSection(1,3);" data-toggle="tooltip" data-placement="top" data-original-title="<spring:message code="optAct"/>">
                   <spring:message code="instA"/>
                  </label>
              </div>
       </div>      
	  </div>
	  <div class="row mb10 hide">
	   <div class="col-sm-3 col-md-3 widthCl top5"><span class="left15"><spring:message code="DSPQlblPreScreen"/></span>
	     <span data-toggle="tooltip" data-placement="top" title="" data-original-title="<spring:message code="DSPQlblPreScreentool"/>"><img src="images/qua-icon.png"></span>
	   </div>
	   <div class="col-sm-1 col-md-1 showhideedit"><a href="javascript:void(0);" title="<spring:message code="msgEditicon"/>" onclick="clickView(2,'',2);">
			       <i class="fa fa-pencil-square-o fa-lg"></i></a>
	   </div>
	    <div class="col-sm-1 col-md-1 managewidth" id="extraWidth1">	  </div>
	     <div class="col-sm-2 col-md-2 txtalign hide externalDiv">
	     <div class="lfloat"> </div>
                 <div class="lfloat">
                  <input type="hidden" id="2EE" name="2EE" value=""/>
                  <input type="hidden" id="2EEid" name="2EEid" value="0"/>
                <label class="circleMendOpt colorNo" onclick="clickView(2,'E',1);" id="2E" title="" data-toggle="tooltip" data-placement="top" data-original-title="<spring:message code="msgNoPreScreenConfigured" />"><spring:message code="instN" /></label>                 
            </div>
            <div class="lfloat">
		        <input type="hidden" id="2_2_1" value="A"/>
                 <label style="background-color: green;" class="circleMendOpt" id="2_1" href="javascript:void(0);" title="<spring:message code="optAct"/>" onclick="deactivateSection(2,1);" data-toggle="tooltip" data-placement="top" data-original-title="<spring:message code="optAct"/>">
                 <spring:message code="instA"/>
                 </label>
                 </div>
        </div>
	   	<div class="col-sm-2 col-md-2 txtalign hide internalDiv">
	   	    <div class="lfloat"> </div>
                <div class="lfloat">
                 <input type="hidden" id="2II" name="2II" value=""/>
                 <input type="hidden" id="2IIid" name="2IIid" value="0"/>
                <label class="circleMendOpt colorNo" onclick="clickView(2,'I',1);" id="2I" title="" data-toggle="tooltip" data-placement="top" data-original-title="<spring:message code="msgNoPreScreenConfigured" />" ><spring:message code="instN" /></label>                                
                </div>
                <div class="lfloat">
		        <input type="hidden" id="2_2_2" value="A"/>
                <label style="background-color: green;" class="circleMendOpt" id="2_2" href="javascript:void(0);" title="<spring:message code="optAct"/>" onclick="deactivateSection(2,2);" data-toggle="tooltip" data-placement="top" data-original-title="<spring:message code="optAct"/>">
                <spring:message code="instA"/>
                </label>
                </div>
          </div>
       
       	     <div class="col-sm-2 col-md-2 txtalign hide internalTDiv">
       	     	<div class="lfloat"> </div>
                  <div class="lfloat">
                   <input type="hidden" id="2TT" name="2TT" value=""/>
                   <input type="hidden" id="2TTid" name="2TTid" value="0"/>
                    <label class="circleMendOpt colorNo" onclick="clickView(2,'T',1);" id="2T" title="" data-toggle="tooltip" data-placement="top" data-original-title="<spring:message code="msgNoPreScreenConfigured" />"><spring:message code="instN"/></label>                                
                 </div>
                 <div class="lfloat">
		        <input type="hidden" id="2_2_3" value="A"/>
                  <label style="background-color: green;" class="circleMendOpt" id="2_3" href="javascript:void(0);" title="<spring:message code="optAct"/>" onclick="deactivateSection(2,3);" data-toggle="tooltip" data-placement="top" data-original-title="<spring:message code="optAct"/>">
                   <spring:message code="instA"/>
                  </label> 
                  </div>
                  </div>
	  </div>
	  <div class="row mb10 hide">
	   <div class="col-sm-3 col-md-3 widthCl top5"><span class="left15"><spring:message code="msgEPIGroup"/></span>
	   <span data-toggle="tooltip" data-placement="top" title="" data-original-title="<spring:message code="msgEPIGrouptool"/>"><img src="images/qua-icon.png"></span>
	    </div>
	    <div class="col-sm-1 col-md-1 showhideedit"><a href="javascript:void(0);" title="<spring:message code="msgEditicon"/>" onclick="clickView(3,'',2);">
	    <i class="fa fa-pencil-square-o fa-lg"></i></a>
	    </div>
	   <div class="col-sm-1 col-md-1 managewidth" id="extraWidth2">	  </div>
	     <div class="col-sm-2 col-md-2 txtalign hide externalDiv">
	     	<div class="lfloat"> </div>
                  <div class="lfloat">
                   <input type="hidden" id="3EE" name="3EE" value=""/>
                   <input type="hidden" id="3EEid" name="3EEid" value="0"/>
                <label class="circleMendOpt colorNo" onclick="clickView(3,'E',1);" id="3E" title="" data-toggle="tooltip" data-placement="top" data-original-title="<spring:message code="msgNoEPIConfigured" />" ><spring:message code="instN"/></label>                               
                </div>
                <div class="lfloat">
		        <input type="hidden" id="3_3_1" value="A"/>
                  <label style="background-color: green;" class="circleMendOpt" id="3_1" href="javascript:void(0);" title="<spring:message code="optAct"/>" onclick="deactivateSection(3,1);" data-toggle="tooltip" data-placement="top" data-original-title="<spring:message code="optAct"/>">
                   <spring:message code="instA"/>
                  </label> 
                  </div>
               </div>
	   	  <div class="col-sm-2 col-md-2 txtalign hide internalDiv">
	   	  		<div class="lfloat"> </div>
                  <div class="lfloat">
                   <input type="hidden" id="3II" name="3II" value=""/>
                   <input type="hidden" id="3IIid" name="3IIid" value="0"/>
                <label class="circleMendOpt colorNo" onclick="clickView(3,'I',1);" id="3I" title="" data-toggle="tooltip" data-placement="top" data-original-title="<spring:message code="msgNoEPIConfigured" />"><spring:message code="instN" /></label>                                 
                 </div>
                 <div class="lfloat">
		        <input type="hidden" id="3_3_2" value="A"/>
                  <label style="background-color: green;" class="circleMendOpt" id="3_2" href="javascript:void(0);" title="<spring:message code="optAct"/>" onclick="deactivateSection(3,2);" data-toggle="tooltip" data-placement="top" data-original-title="<spring:message code="optAct"/>">
                   <spring:message code="instA"/>
                  </label>
                  </div>
            </div>

       	     <div class="col-sm-2 col-md-2 txtalign hide internalTDiv">
       	     <div class="lfloat"> </div>
                  <div class="lfloat">
                  <input type="hidden" id="3TT" name="3TT" value=""/>
                  <input type="hidden" id="3TTid" name="3TTid" value="0"/>
                <label class="circleMendOpt colorNo" onclick="clickView(3,'T',1);" id="3T" title="" data-toggle="tooltip" data-placement="top" data-original-title="<spring:message code="msgNoEPIConfigured" />"><spring:message code="instN" /></label>                                                
                 </div>
                 <div class="lfloat ">
		        <input type="hidden" id="3_3_3" value="A"/>
                  <label style="background-color: green;" class="circleMendOpt" id="3_3" href="javascript:void(0);" title="<spring:message code="optAct"/>" onclick="deactivateSection(3,3);" data-toggle="tooltip" data-placement="top" data-original-title="<spring:message code="optAct"/>">
                   <spring:message code="instA"/>
                  </label>
                  </div> 
                 </div>
	  </div>
	  <div class="row mb10 hide">
	   <div class="col-sm-3 col-md-3 widthCl top5"><span class="left15"><spring:message code="DSPQlblQuestions"/> </span>
	   <span data-toggle="tooltip" data-placement="top" title="" data-original-title="<spring:message code="DSPQlblQuestionstool"/>"><img src="images/qua-icon.png"></span>
	    </div>
	    <div class="col-sm-1 col-md-1 showhideedit"><a href="javascript:void(0);" title="<spring:message code="msgEditicon"/>" onclick="clickView(4,'',2);">
			       <i class="fa fa-pencil-square-o fa-lg"></i></a>
	    </div>
	    <div class="col-sm-1 col-md-1 managewidth" id="extraWidth3">	  </div>
	     <div class="col-sm-2 col-md-2 txtalign hide externalDiv">
	     		<div class="lfloat">
                 <input type="hidden" id="etypeValue1" value="1"/>
                 <label class="etype1 circleMendOpt" id="etype1" onclick="resetOptMend(1,1);" data-toggle="tooltip" data-placement="top" data-original-title="<spring:message code="lblRequiredd"/>" style="background-color: red;"><spring:message code="instR" /></label>
                 </div>
                  <div class="lfloat">
                  <input type="hidden" id="4EE" name="4EE" value=""/>
                  <input type="hidden" id="4EEid" name="4EEid" value="0"/>
                   <label class="circleMendOpt colorNo" onclick="clickView(4,'E',1);" id="4E" title="" data-toggle="tooltip" data-placement="top" data-original-title="<spring:message code="msgNoCustomQuestionConfigured" />"><spring:message code="instN" /></label>                                                               
                 </div>
                 <div class="lfloat">
		        <input type="hidden" id="4_4_1" value="A"/>
                  <label style="background-color: green;" class="circleMendOpt" id="4_1" title="<spring:message code="optAct"/>" onclick="deactivateSection(4,1);" data-toggle="tooltip" data-placement="top" data-original-title="<spring:message code="optAct"/>">
                   <spring:message code="instA"/>
                  </label> 
                  </div>
                 
                 </div>
	        	<div class="col-sm-2 col-md-2 txtalign hide internalDiv">
	   			 <div class="lfloat">
                <input type="hidden" id="etypeValue2" value="1"/>
                <label class="etype2 circleMendOpt" id="etype2" onclick="resetOptMend(2,1);" data-toggle="tooltip" data-placement="top" data-original-title="<spring:message code="lblRequiredd"/>" style="background-color: red;"><spring:message code="instR" /></label>
                </div>
                  <div class="lfloat">
                  <input type="hidden" id="4II" name="4II" value=""/>
                  <input type="hidden" id="4IIid" name="4IIid" value="0"/>
                <label class="circleMendOpt colorNo" onclick="clickView(4,'I',1);" id="4I" title="" data-toggle="tooltip" data-placement="top" data-original-title="<spring:message code="msgNoCustomQuestionConfigured" />"><spring:message code="instN" /></label>
                 </div>
                 <div class="lfloat">
		        <input type="hidden" id="4_4_2" value="A"/>
                  <label  style="background-color: green;" class="circleMendOpt" id="4_2" title="<spring:message code="optAct"/>" onclick="deactivateSection(4,2);" data-toggle="tooltip" data-placement="top" data-original-title="<spring:message code="optAct"/>">
                   <spring:message code="instA"/>
                  </label> 
                  </div>
               
                </div>
       
       	     <div class="col-sm-2 col-md-2 txtalign hide internalTDiv">
       	            <div class="lfloat">
                  <input type="hidden" id="etypeValue3" value="1"/>
                  <label class="etype3 circleMendOpt" id="etype3" onclick="resetOptMend(3,1);" data-toggle="tooltip" data-placement="top" data-original-title="<spring:message code="lblRequiredd"/>" style="background-color: red;"><spring:message code="instR" /></label>
                  </div>        
                  <div class="lfloat">
                  <input type="hidden" id="4TT" name="4TT" value=""/>
                  <input type="hidden" id="4TTid" name="4TTid" value="0"/>
                    <label class="circleMendOpt colorNo" onclick="clickView(4,'T',1);" id="4T" title="" data-toggle="tooltip" data-placement="top" data-original-title="<spring:message code="msgNoCustomQuestionConfigured" />" ><spring:message code="instN" /></label>             
                 </div>
                  <div class="lfloat">
		        <input type="hidden" id="4_4_3" value="A"/>
                  <label style="background-color: green;" class="circleMendOpt" id="4_3" title="<spring:message code="optAct"/>" onclick="deactivateSection(4,3);" data-toggle="tooltip" data-placement="top" data-original-title="<spring:message code="optAct"/>">
                   <spring:message code="instA"/>
                  </label>
                  </div>
                  
                   </div>
                 </div>
	  <div class="row mb10" id="onBoardingMaster">
	   <div class="col-sm-3 col-md-3 widthCl top5"><span class="left15"><spring:message code="msgOnBoarding"/></span>  
	   <span data-toggle="tooltip" data-placement="top" title="" data-original-title="<spring:message code="msgOnBoardingtool"/>"><img src="images/qua-icon.png"></span>
	   </div>
	    <div class="col-sm-1 col-md-1 showhideedit"><a href="javascript:void(0);" title="<spring:message code="msgEditicon"/>" onclick="clickView(5,'',2);">
			       <i class="fa fa-pencil-square-o fa-lg"></i></a>
		 </div>
	    <div class="col-sm-1 col-md-1 managewidth" id="extraWidth4">	  </div>
	     <div class="col-sm-2 col-md-2 txtalign hide externalDiv">
	    <div class="lfloat"> </div>
                  <div class="lfloat">
                  <input type="hidden" id="5EE" name="5EE" value=""/>
                   <input type="hidden" id="5EEid" name="5EEid" value="0"/>
                 <label class="circleMendOpt colorNo" onclick="clickView(5,'E',1);" id="5E" title="" data-toggle="tooltip" data-placement="top" data-original-title="<spring:message code="msgNoOnBoardingConfigured" />" ><spring:message code="instN" /></label>                                                                   
                 </div>
                  <div class="lfloat">
		        <input type="hidden" id="5_5_1" value="A"/>
                  <label style="background-color: green;" class="circleMendOpt" id="5_1" href="javascript:void(0);" title="<spring:message code="optAct"/>" onclick="deactivateSection(5,1);" data-toggle="tooltip" data-placement="top" data-original-title="<spring:message code="optAct"/>">
                   <spring:message code="instA"/>
                  </label> 
                  </div>
                 </div>
	   	<div class="col-sm-2 col-md-2 txtalign hide internalDiv">
	   	<div class="lfloat"> </div>
                  <div class="lfloat">
                  <input type="hidden" id="5II" name="5II" value=""/>
                  <input type="hidden" id="5IIid" name="5IIid" value="0"/>
                <label class="circleMendOpt colorNo" onclick="clickView(5,'I',1);" id="5I" title="" data-toggle="tooltip" data-placement="top" data-original-title="<spring:message code="msgNoOnBoardingConfigured" />"><spring:message code="instN" /></label>                
                 </div>
                 <div class="lfloat">
		        <input type="hidden" id="5_5_2" value="A"/>
                  <label style="background-color: green;" class="circleMendOpt" id="5_2" href="javascript:void(0);" title="<spring:message code="optAct"/>" onclick="deactivateSection(5,2);" data-toggle="tooltip" data-placement="top" data-original-title="<spring:message code="optAct"/>">
                   <spring:message code="instA"/>
                  </label>
                  </div>
             </div>
       
       	     <div class="col-sm-2 col-md-2 txtalign hide internalTDiv">
       	     <div class="lfloat"> </div>
                  <div class="lfloat">
                  <input type="hidden" id="5TT" name="5TT" value=""/>
                  <input type="hidden" id="5TTid" name="5TTid" value="0"/>
                <label class="circleMendOpt colorNo" onclick="clickView(5,'T',1);" id="5T" title="" data-toggle="tooltip" data-placement="top" data-original-title="<spring:message code="msgNoOnBoardingConfigured" />" ><spring:message code="instN" /></label>              
                 </div>
                 <div class="lfloat">
		        <input type="hidden" id="5_5_3" value="A"/>
                  <label style="background-color: green;" class="circleMendOpt" id="5_3" href="javascript:void(0);" title="<spring:message code="optAct"/>" onclick="deactivateSection(5,3);" data-toggle="tooltip" data-placement="top" data-original-title="<spring:message code="optAct"/>">
                   <spring:message code="instA"/>
                  </label>  
                  </div>
                  </div>
	  </div>
	  <div class="row mb10 hide">
	   <div class="col-sm-3 col-md-3 widthCl top5" style="padding-right:0px"><span class="left15"><spring:message code="lblVVI"/></span>  
	   <span data-toggle="tooltip" data-placement="top" title="" data-original-title="<spring:message code="lblVVItool"/>"><img src="images/qua-icon.png"></span>
	   </div>
	   <input type="hidden" id="maxScoreForVVIHidden" name="maxScoreForVVIHidden" value="0"/>
	   <input type="hidden" id="sendAutoVVILinkHidden" name="sendAutoVVILinkHidden" value="0"/>
	   <input type="hidden" id="statusDivHidden" name="statusDivHidden" value="0"/>
	   <input type="hidden" id="timePerQuesHidden" name="timePerQuesHidden" value="0"/>
	   <input type="hidden" id="vviExpDaysHidden" name="vviExpDaysHidden" value="0"/>
	    <input type="hidden" id="approvalByPredefinedGroupsHidden" name="approvalByPredefinedGroupsHidden" value="0"/>
	    <div class="col-sm-1 col-md-1 showhideedit"><a href="javascript:void(0);" title="<spring:message code="msgEditicon"/>" onclick="clickView(6,'',2);">
			       <i class="fa fa-pencil-square-o fa-lg"></i></a>
	   </div>
	    <div class="col-sm-1 col-md-1 managewidth" id="extraWidth5">	  </div>
	     <div class="col-sm-2 col-md-2 txtalign hide externalDiv">
	     <div class="lfloat"> </div>
                  <div class="lfloat">
                   <input type="hidden" id="6EE" name="6EE" value=""/>
                   <input type="hidden" id="6EEid" name="6EEid" value="0"/>
                <label class="circleMendOpt colorNo" onclick="clickView(6,'E',1);" id="6E" title="" data-toggle="tooltip" data-placement="top" data-original-title="<spring:message code="msgNoVVIConfigured" />"><spring:message code="instN" /></label>               
                 </div>
                 <div class="lfloat">
		        <input type="hidden" id="6_6_1" value="A"/>
                  <label style="background-color: green;" class="circleMendOpt" id="6_1" href="javascript:void(0);" title="<spring:message code="optAct"/>" onclick="deactivateSection(6,1);" data-toggle="tooltip" data-placement="top" data-original-title="<spring:message code="optAct"/>">
                   <spring:message code="instA"/>
                  </label> 
                  </div>
                 </div>
	   	<div class="col-sm-2 col-md-2 txtalign hide internalDiv">
	   <div class="lfloat"> </div>
                  <div class="lfloat">
                  <input type="hidden" id="6II" name="6II" value=""/>
                  <input type="hidden" id="6IIid" name="6IIid" value="0"/>
                <label class="circleMendOpt colorNo" onclick="clickView(6,'I',1);" id="6I" title="" data-toggle="tooltip" data-placement="top" data-original-title="<spring:message code="msgNoVVIConfigured" />"><spring:message code="instN" /></label>               
                 </div>
                 <div class="lfloat">
		        <input type="hidden" id="6_6_2" value="A"/>
                  <label style="background-color: green;" class="circleMendOpt" id="6_2" href="javascript:void(0);" title="<spring:message code="optAct"/>" onclick="deactivateSection(6,2);" data-toggle="tooltip" data-placement="top" data-original-title="<spring:message code="optAct"/>">
                   <spring:message code="instA"/>
                  </label>
                  </div>
         </div>
       
       	     <div class="col-sm-2 col-md-2 txtalign hide internalTDiv">
       	     <div class="lfloat"> </div>
                  <div class="lfloat">
                  <input type="hidden" id="6TT" name="6TT" value=""/>
                  <input type="hidden" id="6TTid" name="6TTid" value="0"/>
                <label class="circleMendOpt colorNo" onclick="clickView(6,'T',1);" id="6T" title="" data-toggle="tooltip" data-placement="top" data-original-title="<spring:message code="msgNoVVIConfigured" />"><spring:message code="instN" /></label>               
                 </div>
                 <div class="lfloat">
		        <input type="hidden" id="6_6_3" value="A"/>
                  <label style="background-color: green;" class="circleMendOpt" id="6_3" href="javascript:void(0);" title="<spring:message code="optAct"/>" onclick="deactivateSection(6,3);" data-toggle="tooltip" data-placement="top" data-original-title="<spring:message code="optAct"/>">
                   <spring:message code="instA"/>
                  </label>
                  </div> 
                 </div>
	  </div>
	  <div class="row mb10 hide">
	   <div class="col-sm-3 col-md-3 widthCl top5"><span class="left15"><spring:message code="msgOnlineAssessmentJ"/></span> 
	   <span data-toggle="tooltip" data-placement="top" title="" data-original-title="<spring:message code="msgOnlineAssessmentJtool"/>"><img src="images/qua-icon.png"></span>
	    </div>
	      <div class="col-sm-1 col-md-1 showhideedit"><a href="javascript:void(0);" title="<spring:message code="msgEditicon"/>" onclick="clickView(7,'',2);">
			       <i class="fa fa-pencil-square-o fa-lg"></i></a>
	     </div>
	    <div class="col-sm-1 col-md-1 managewidth" id="extraWidth6">	  </div>
	     <div class="col-sm-2 col-md-2 txtalign hide externalDiv">
	     <div class="lfloat"> </div>
                  <div class="lfloat">
                  <input type="hidden" id="7EE" name="7EE" value=""/>
                  <input type="hidden" id="7EEid" name="7EEid" value="0"/>
                <label class="circleMendOpt colorNo" onclick="clickView(7,'E',1);" id="7E" title="" data-toggle="tooltip" data-placement="top" data-original-title="<spring:message code="msgNoAssessmentConfigured" />"><spring:message code="instN" /></label>              
                 </div>
                 <div class="lfloat">
		        <input type="hidden" id="7_7_1" value="A"/>
                  <label style="background-color: green;" class="circleMendOpt" id="7_1" href="javascript:void(0);" title="<spring:message code="optAct"/>" onclick="deactivateSection(7,1);" data-toggle="tooltip" data-placement="top" data-original-title="<spring:message code="optAct"/>">
                   <spring:message code="instA"/>
                  </label>  
                  </div>
                 </div>
	   	<div class="col-sm-2 col-md-2 txtalign hide internalDiv">
	   	<div class="lfloat"> </div>
                  <div class="lfloat">
                   <input type="hidden" id="7II" name="7II" value=""/>
                   <input type="hidden" id="7IIid" name="7IIid" value="0"/>
                <label class="circleMendOpt colorNo" onclick="clickView(7,'I',1);" id="7I" title="" data-toggle="tooltip" data-placement="top" data-original-title="<spring:message code="msgNoAssessmentConfigured" />"><spring:message code="instN" /></label>               
                 </div>
                 <div class="lfloat">
		        <input type="hidden" id="7_7_2" value="A"/>
                  <label style="background-color: green;" class="circleMendOpt" id="7_2" href="javascript:void(0);" title="<spring:message code="optAct"/>" onclick="deactivateSection(7,2);" data-toggle="tooltip" data-placement="top" data-original-title="<spring:message code="optAct"/>">
                   <spring:message code="instA"/>
                  </label> 
                  </div>
         </div>
       
       	     <div class="col-sm-2 col-md-2 txtalign hide internalTDiv">
       	     <div class="lfloat"> </div>
                  <div class="lfloat">
                   <input type="hidden" id="7TT" name="7TT" value=""/>
                   <input type="hidden" id="7TTid" name="7TTid" value="0"/>
                <label class="circleMendOpt colorNo" onclick="clickView(7,'T',1);" id="7T" title="" data-toggle="tooltip" data-placement="top" data-original-title="<spring:message code="msgNoAssessmentConfigured" />" ><spring:message code="instN" /></label>               
                 </div>
                 <div class="lfloat">
		        <input type="hidden" id="7_7_3" value="A"/>
                  <label style="background-color: green;" class="circleMendOpt" id="7_3" href="javascript:void(0);" title="<spring:message code="optAct"/>" onclick="deactivateSection(7,3);" data-toggle="tooltip" data-placement="top" data-original-title="<spring:message code="optAct"/>">
                  <spring:message code="instA"/>
                  </label> 
                  </div>
                 </div>
	  </div>
	  <div class="row mb20 hide">
	   <div class="col-sm-3 col-md-3 widthCl top5"><span class="left15"><spring:message code="msgEReference"/></span> 
	   <span data-toggle="tooltip" data-placement="top" title="" data-original-title="<spring:message code="msgEReferencetool"/>"><img src="images/qua-icon.png"></span>
	    </div>
	     <div class="col-sm-1 col-md-1 showhideedit"><a href="javascript:void(0);" title="<spring:message code="msgEditicon"/>" onclick="clickView(8,'',2);">
			     <i class="fa fa-pencil-square-o fa-lg"></i></a>
	     </div>
	    <div class="col-sm-1 col-md-1 managewidth" id="extraWidth7">	  </div>
	     <div class="col-sm-2 col-md-2 txtalign hide externalDiv">
	     <div class="lfloat"> </div>
                  <div class="lfloat">
                   <input type="hidden" id="8EE" name="8EE" value=""/>
                   <input type="hidden" id="8EEid" name="8EEid" value="0"/>
                <label class="circleMendOpt colorNo" onclick="clickView(8,'E',1);" id="8E" title="" data-toggle="tooltip" data-placement="top" data-original-title="<spring:message code="msgNoEReferenceConfigured" />" ><spring:message code="instN" /></label>               
                 </div>
                 <div class="lfloat">
		        <input type="hidden" id="8_8_1" value="A"/>
                 <label style="background-color: green;" class="circleMendOpt" id="8_1" href="javascript:void(0);" title="<spring:message code="optAct"/>" onclick="deactivateSection(8,1);" data-toggle="tooltip" data-placement="top" data-original-title="<spring:message code="optAct"/>">
                   <spring:message code="instA"/>
                  </label> 
                  </div>
                 </div>
	   	<div class="col-sm-2 col-md-2 txtalign hide internalDiv">
	   	<div class="lfloat"> </div>
                  <div class="lfloat">
                  <input type="hidden" id="8II" name="8II" value=""/>
                   <input type="hidden" id="8IIid" name="8IIid" value="0"/>
                <label class="circleMendOpt colorNo" onclick="clickView(8,'I',1);" id="8I" title="" data-toggle="tooltip" data-placement="top" data-original-title="<spring:message code="msgNoEReferenceConfigured" />"><spring:message code="instN" /></label>                
                 </div>
                 <div class="lfloat">
		        <input type="hidden" id="8_8_2" value="A"/>
                  <label style="background-color: green;" class="circleMendOpt" id="8_2" href="javascript:void(0);" title="<spring:message code="optAct"/>" onclick="deactivateSection(8,2);" data-toggle="tooltip" data-placement="top" data-original-title="<spring:message code="optAct"/>">
                   <spring:message code="instA"/>
                  </label>
                  </div>
         </div>
       
       	     <div class="col-sm-2 col-md-2 txtalign hide internalTDiv">
       	     <div class="lfloat"> </div>
                  <div class="lfloat">
                  <input type="hidden" id="8TT" name="8TT" value=""/>
                   <input type="hidden" id="8TTid" name="8TTid" value="0"/>
                   <label class="circleMendOpt colorNo" onclick="clickView(8,'T',1);" id="8T" title="" data-toggle="tooltip" data-placement="top" data-original-title="<spring:message code="msgNoEReferenceConfigured" />"><spring:message code="instN" /></label>               
                 </div>
                 <div class="lfloat">
		        <input type="hidden" id="8_8_3" value="A"/>
                  <label style="background-color: green;" class="circleMendOpt" id="8_3" href="javascript:void(0);" title="<spring:message code="optAct"/>" onclick="deactivateSection(8,3);" data-toggle="tooltip" data-placement="top" data-original-title="<spring:message code="optAct"/>">
                   <spring:message code="instA"/>
                  </label>
                  </div>
                      </div>
	  </div>
	  </div>
	<input type="hidden" id="activeId"/>
    <input type="hidden" id="activeType"/>
</div>

	  <!-- Job Approval process     -->
	  
	  		<div class="row hide golive mt10" style="margin-left:0px;">
	  		<div class="row">
	  		<span class="col-sm-12 col-md-12" style="font-size: 12px; height: 26px;  line-height: 35px;color:#007AB4;font-weight:bold;">
		             <spring:message code="msgJobApprovalProcess"/>
		          <span data-toggle="tooltip" data-placement="top" data-original-title="<spring:message code="msgJobApprovalProcesstool"/>"><img src="images/qua-icon.png">
		          </span>
	           </span>
	           </div>
	           <div class="row">
			  <div class="col-sm-6 col-md-6" style="margin-top:-10px;">
				<label class="checkbox">
					<input type="checkbox" id="approvalBeforeGoLive1" name="approvalBeforeGoLive1" onclick="enableDisableApprovals();">
					<spring:message code="msgApprovalprocess"/>
				</label>
			</div>
			<div class="col-sm-6 col-md-6">
			<div class="row hide" id="groupApprovalDiv">
			<div class="col-sm-7 col-md-7" style="margin: 5px -85px 0px 6px;">
				<spring:message code="msgApprovalsNeeded"/> <span data-toggle="tooltip" data-placement="top" title="" data-original-title="<spring:message code="msgApprovalsNeededtool"/>">
				<img src="images/qua-icon.png" >
		       </span>
			</div>
			
			<div class="col-sm-2 col-md-2" >
				<select class="form-control" id="noOfApprovalNeeded" name="noOfApprovalNeeded" style="width:60px;padding-left:5px;">
					<option value="0">0</option>
					<c:forEach begin="0" end="9" varStatus="loop">
						<option value="${loop.count}">${loop.count}</option>
					</c:forEach>
				</select>
			</div>
		   </div>
	     </div>
			
			</div>
        <div class="row hide" id="approvalBPGroupsDiv" >
			       <div class="left15">
				    <div class="col-sm-12 col-md-12 checkbox inline top1" id="" >
						<input type="checkbox" name="approvalByPredefinedGroups" id="approvalByPredefinedGroups" >
						<label><strong> <spring:message code="lblApprovalByPredefinedGroups"/></strong></label>
				 </div>
			</div>
	    </div>
			<!-- Start Grouping Functionality  at Job Category Level -->
		<div class="hide" id="groupApprovalDiv1" style="padding-left: 5px;">
		<div id="privilegeForDistrictErrorDiv"> </div>		
		<div class="row" id="buildApprovalGroupsDivId">
			<div class="col-sm-2 col-md-2" style="width:20%;padding-left:10px;padding-right:0px;">
               		<label class="checkbox mt0 mb0"><input type="checkbox" id="buildApprovalGroups" name="buildApprovalGroups" onclick="enableDisableApprovalsFirst();">               
	               		<spring:message code="lblBuildApprovalGroups"/>	
	               		<span data-toggle="tooltip" data-placement="top" title="" data-original-title="<spring:message code="lblBuildApprovalGroupstool"/>"><img src="images/qua-icon.png" ></span>
               		</label>
		  	</div>
		  	<div class="col-sm-3 col-md-3" style="width:31%;"></div>
		  	<div class="col-sm-2 col-md-2" style='padding-left: 10px;'>
		  		<label class="hideGroup mt0">
               		<a class="hide buildAppGroup" href="javascript:void(0);" onclick="showAddGroup()" > +<spring:message code="btnAddMember"/>
               			<span  data-toggle="tooltip" data-placement="top" title="" data-original-title="<spring:message code="btnAddMembertool"/>"><img src="images/qua-icon.png" > </span>  
               		</a>
		  		</label>
		   	</div>
		  </div>
  		</div>	
		<div class="hide buildAppGroup left15" id="groupMembersList"> </div>
		
        <div class="hide" id="addApprovalGroupAndMemberDiv">
        <div class="row mt5">
        <div class="col-sm-2 col-md-2">
        <label><strong><spring:message code="lblIwouldliketo"/></strong></label>
				<span  data-toggle="tooltip" data-placement="top" title="" data-original-title="<spring:message code="lblIwouldliketotool"/> "><img src="images/qua-icon.png" >
		      </span>
        </div>
        <div class="col-sm-4 col-md-4">
          <span class="radio" style="margin-top:0px;margin-bottom:0px;">
						<label><input type="radio" id="createGroupName" value="radioButton1" name="groupRadioButtonId"
							onclick="groupRadioButtonMain()">
				
				<strong><spring:message code="lblCreateanewgroup"/></strong></label>
				<span  data-toggle="tooltip" data-placement="top" title="" data-original-title="<spring:message code="lblCreateanewgrouptool"/>"><img src="images/qua-icon.png" >
		      </span>
		      </span>
		 </div>
		 <div class="col-sm-4 col-md-4" style="padding-left: 22px;">
       <span class="radio" style="margin-top:0px;margin-bottom:0px;">
            <label>
						<input type="radio" id="selectGroupName" value="radioButton2" name="groupRadioButtonId" onclick="groupRadioButtonMain();">
				<strong><spring:message code="lblSelectexistinggroup"/></strong>
				</label>
				<span data-toggle="tooltip" data-placement="top" title="" data-original-title="<spring:message code="lblSelectexistinggrouptool"/>"><img src="images/qua-icon.png" >
		        </span>
		     </span>
		 </div>
       </div>
	 <div class="row" style="width:100%;">
        <div class="row" style="padding-left: 16px;">
           		<input type="hidden" value="1" id="noOfApproval" name="noOfApproval" class="form-control"  maxlength="4">
           		<div class="col-sm-6 col-md-6 hide" id="createGroup">
           			<label><spring:message code="lblEnterGroupName"/> <font color="red">*</font> </label>
                     <input type="text" class="form-control" name="addGroupName" id="addGroupName">
				 </div>
				 
				 <div class="col-sm-6 col-md-6 hide" id="selectGroup">
               		<label><spring:message code="lblSelectGroupName"/> <font color="red">*</font> </label>
               			<select class="form-control" id="groupName" name="groupName">
               			 <option value=""><spring:message code="lblSelectGroupName"/></option>
               			</select>
               	</div>
               	
               	<div class="col-sm-6 col-md-6">
               		<label class="left15"> <spring:message code="lblSelectMembers"/> <font color="red">*</font> </label>
               	<input type="hidden" id="selectAssId" name="selectAssId"/>
               	<dl class="dropdownAss col-sm-12 col-md-12 mt0"> 
			    <dt>
			    <a href="javascript:void(0);" style="width:100%">
			      <span class="hidaAss" style="font-family: 'Century Gothic','Open Sans', sans-serif;"><spring:message code="lblSelectMembers"/></span>    
			      <p class="multiSelAss" style="margin-bottom: 0px;"></p>  
			    </a>
			    </dt>
			      <dd>
			        <ul class="mutliSelectAss" style="width:100%"></ul>
                 </dd>
			   </dl>
               	</div>
				 
				 
               	</div>
           <div class="row" style="padding-left: 16px;">
        	<div class="col-sm-6 col-md-6">
        	  <label>
               		<a href='javascript:void(0);' onclick="addGroupORMember()"> <spring:message code="lnkImD"/> </a> &nbsp;&nbsp;
               		<a href='javascript:void(0);' onclick="hideAddApprovalGroupAndMemberDivMain()"> <spring:message code="btnClr"/> </a>
              </label>
        	</div>
        	</div> 
		 </div> 
	   </div>
  </div>
  
<!--       Save and cancel Button -->
		<div class="hide scButton">
		 <div class="row top20">
		 <div class="col-sm-6 col-md-6">
				<button onclick="cancelJobCateDiv(2)" class="flatbtn" style="background: #da4f49;width: 30%;" id="cancel">
          			<strong><spring:message code="btnClr"/> <i class="fa fa-times-circle"></i></strong>
          			</button>	
				</div>	
				<div class="col-sm-6 col-md-6" style="text-align: right;padding-right: 33px;">
				
					<button onclick="saveJobCategory();" class="flatbtn" style="background: #99cf77; width: 30%;" id="Next">
		               <strong><spring:message code="btnSave"/> <i class="icon"></i></strong>
            	  </button>	
				</div>
		    
		</div>
		</div>
		<!--        End Save button	-->
		<div id="loadingDiv" style="display:none;z-index: 5000;" >
	     <table  align="center" >
	 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
	 		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
		</table>
	    </div>

        <div id="loadingDiv_jobCatGrid" style="display:none;z-index: 5000;" >
	     <table  align="center" >
	 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
	 		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
		</table>
	   </div>
	    
	  <div id="loadingDiv_jobEdit" style="display:none;z-index: 5000;" >
	     <table  align="center" >
	 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
	 		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
		</table>
	  </div>
	  
<!--</form>
--><script>
$(document).ready(function(){
if($("#displayTypeId").val()==1 && $("#userType").val()!=2){
    setEntityType();
    }
	displayOrHideSearchBox();
	displayJobCategories();
	//('#iconpophover').tooltip();
	hideOnBoardingMaster();
	});
</script>
<!-- add by ankit -->
<script>
		$('#imiconpophover').tooltip();
</script>
<!--add end by ankit -->

<script type="text/javascript">
	$('#addPop').click(function() {
		if ($('#lstDistrictAdmins option:selected').val() != null) {
			 $('#lstDistrictAdmins option:selected').remove().appendTo('#attachedDAList');
			 $("#attachedDAList").attr('selectedIndex','-1').find("option:selected").removeAttr("selected");
	         $("#attachedDAList").attr('selectedIndex', '-1').find("option:selected").removeAttr("selected");
	 }
	});
	
	$('#removePop').click(function() {
	       if ($('#attachedDAList option:selected').val() != null) {
	             $('#attachedDAList option:selected').remove().appendTo('#lstDistrictAdmins');
	           //  sortSelectList();
	             $("#attachedDAList").attr('selectedIndex',  '-1').find("option:selected").removeAttr("selected");
	             $("#lstDistrictAdmins").attr('selectedIndex', '-1').find("option:selected").removeAttr("selected");
	             $("#lstDistrictAdmins").attr('selectedIndex', '-1').addAttr("selected");
	             
	}
	});
	//displayAllDistrictAssessment();
</script>

<input type="hidden" id="ApprovalDistrictId" name="ApprovalDistrictId">
<input type="hidden" id="ApprovalJobCategoryId" name="ApprovalJobCategoryId" value="">

<input type="hidden" id="ApprovalNoOfApprovalNeeded" name="ApprovalNoOfApprovalNeeded" value="">
<input type="hidden" id="ApprovalBuildApprovalGroup" name="ApprovalBuildApprovalGroup" value="">
<input type="hidden" id="ApprovalApprovalBeforeGoLive" name="ApprovalApprovalBeforeGoLive" value="">
<input type="hidden" id="ApprovalGroupFromEditFlag" name="ApprovalApprovalGroupFlag" value="false">

<script>

var districtMaster = "";
var noOfApprovalNeeded="";
var approvalBeforeGoLive="";
var buildApprovalGroup="";

var DistM = "${districtMaster}";
if(DistM!=null && DistM!="")
{
	districtMaster = "${districtMaster.districtId}";
	noOfApprovalNeeded = "${districtMaster.noOfApprovalNeeded}";
	approvalBeforeGoLive = "${districtMaster.approvalBeforeGoLive}";
	buildApprovalGroup = "${districtMaster.buildApprovalGroup}";
}
//alert($("#ApprovalDistrictId").val());
$("#ApprovalDistrictId").val(districtMaster);
$("#ApprovalJobCategoryId").val("-1");
$("#ApprovalNoOfApprovalNeeded").val(noOfApprovalNeeded);
$("#ApprovalBuildApprovalGroup").val(approvalBeforeGoLive);
$("#ApprovalApprovalBeforeGoLive").val(buildApprovalGroup);

$(document).ready(function(){
	//addJobCategoryApproval();
});

function fileContainsVirusDiv(msg)
{
	$('#loadingDiv').hide();
	$('#virusDivId .modal-body').html(msg);
	$('#virusDivId').modal('show');
}
</script>
<div style="background: rgba(153, 153, 153, 0.8); " class="modal hide" id="districtAssessment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog-for-cgcoverletter" style='width: 35%;'>
		<div class="modal-content">
			<div class="modal-header">
				<h3 id="myModalLabel"><spring:message code="headTm"/></h3>
			</div>
			<div class="modal-body" style="max-height: 400px;overflow-y:auto;">
				<div class="control-group">
				<spring:message code="msgNoanyassesment"/>
				</div>
			</div>
			<div class="modal-footer">
				<span id=""><button class="btn btn-primary" data-dismiss="modal" onclick="confirmDistrictAssesment();"><spring:message code="btnOk"/> <i class="icon"></i></button></span>&nbsp;&nbsp;
			</div>
		</div>
	</div>
</div>

<div style="background: rgba(153, 153, 153, 0.8); " class="hide modal" id="tableoption" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog-for-cgcoverletter" style='width: 50%;margin-top:11%;'>
		<div class="modal-content">
			<div class="modal-header">
			 <button type="button" class="close" onclick="hidePortfolioDiv();" data-dismiss="modal" aria-hidden="true">x </button>
				<h3 id="headingDiv"></h3>
			</div>
			<input type="hidden" name="showType" id="showType"/>
			<div class="modal-body" style="max-height: 400px;overflow-y:auto;">
				<div class="control-group">	
				  <div class='divErrorMsg mb10' id='errordivportfolio'></div>                         
	              <div class="row hide porfolioHide0">
				 <div class="col-sm-3 col-md-3 mt10 pright0 portfolioLbl" id="colTitel0" style="width:25%;">
				 </div>	
				 <div class="col-sm-9 col-md-9 pleftright portfolioText" id="colTitelText0" style="width:73%;">
				 <select class="form-control" name="colname" id="colId0" onchange="portfolioChangeNotificationInJobCategory(this.value,'E');" />
				   <option value=""><spring:message code="lblSelectPortfolioname"/></option>
				 </select>
				 </div>
				</div>
				<div class="row" style="margin-left: 0px;" id="allPortfolioCheckBox">
				  <label class="checkbox">
					<input type="checkbox" id="allPortfolio" name="allPortfolio" onClick="showdisabled(0);">
					<spring:message code="lblApplytoallcandidatetypes"/>
				 </label>
				</div>
				<div class="row hide porfolioHide1 top5">
				 <div class="col-sm-3 col-md-3 mt10 pright0" id="colTitel1">
				 </div>	
				 <div class="col-sm-9 col-md-9 pleftright" style="width:73%;">
				 <select class="form-control" name="colname1" id="colId1" onchange="portfolioChangeNotificationInJobCategory(this.value,'I');"/>
				 <option value=""><spring:message code="lblSelectPortfolioname"/></option>
				 </select>
				 </div>
				</div>
				<div class="row  hide porfolioHide2 top5">
				 <div class="col-sm-3 col-md-3 mt10 pright0" id="colTitel2">
				 </div>	
				 <div class="col-sm-9 col-md-9 pleftright" style="width:73%;">
				 <select class="form-control" name="colname2" id="colId2" onchange="portfolioChangeNotificationInJobCategory(this.value,'T');"/>
				 <option value=""><spring:message code="lblSelectPortfolioname"/></option>
				 </select>
				 </div>
				</div>
				</div>
			</div>
			<div class="modal-footer" style="text-align: left;">
				<button onclick="saveDivRecord();" class="flatbtn left5" style="background: #99cf77; width: 16%;" ><strong><spring:message code="btnSave"/> <i class="icon"></i></strong></button>
			    &nbsp;<a href="javascript:void(0);" onclick="hidePortfolioDiv();"><spring:message code="btnClr"/></a>
			</div>
		</div>
	</div>
</div>

<div style="background: rgba(153, 153, 153, 0.8); " class="hide modal" id="EPIDiv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog-for-cgcoverletter" style='width: 50%;margin-top:11%;'>
		<div class="modal-content">
			<div class="modal-header">
			 <button type="button" class="close" onclick="hideEPIDiv();" data-dismiss="modal" aria-hidden="true">x </button>
				<h3 id="headingDivEPI"></h3>
			</div>
			<input type="hidden" name="showType" id="showType"/>
			<div class="modal-body" style="max-height: 400px;overflow-y:auto;">
				<div class="control-group">	
				  <div class='divErrorMsg mb10' id='errordivepi'></div>                         
	              <div class="row hide EPIHide0">
	              <div class="col-sm-3 col-md-3 pright0 epiLbl" style="width:25%;">
				    <label class="mt10" id="epiLabel0"><spring:message code="msgEPI"/></label>
				 </div>
				 <div class="col-sm-9 col-md-9 pleftright epiText" style="width:73%;">
				 <select class="form-control" name="EPIname0" id="EPIId0"/>
				   <option value=""><spring:message code="lblPleaseSelectaEPIGroup"/></option>
				 </select>
				 </div>
				</div>
				<div class="row EPIHide0" style="margin-left: 0px;">
				  <label class="checkbox">
					<input type="checkbox" id="allEPI" name="allEPI" onClick="showdisabled(2);">
					<spring:message code="lblApplytoallcandidatetypes"/>
				 </label>
				</div>
				<div class="row hide EPIHide1 top5">
				 <div class="col-sm-3 col-md-3 pright0">
				    <label class="mt10" id="epiLabel1"><spring:message code="msgEPI"/></label>
				 </div>
				 <div class="col-sm-9 col-md-9 pleftright" style="width:73%;">
				 <select class="form-control" name="EPIname1" id="EPIId1"/>
				 <option value=""><spring:message code="lblPleaseSelectaEPIGroup"/></option>
				 </select>
				 </div>
				</div>
				<div class="row  hide EPIHide2 top5">
				<div class="col-sm-3 col-md-3 pright0">
				    <label class="mt10" id="epiLabel2"><spring:message code="msgEPI"/></label>
				 </div>
				 <div class="col-sm-9 col-md-9 pleftright" style="width:73%;">
				 <select class="form-control" name="EPIname2" id="EPIId2"/>
				 <option value=""><spring:message code="lblPleaseSelectaEPIGroup"/></option>
				 </select>
				 </div>
				</div>
				</div>
			</div>
			<div class="modal-footer" style="text-align: left;">
				<button onclick="saveEPIRecord();" class="flatbtn left5" style="background: #99cf77; width: 16%;" ><strong><spring:message code="btnSave"/> <i class="icon"></i></strong></button>
			    &nbsp;<a href="javascript:void(0);" onclick="hideEPIDiv();"><spring:message code="btnClr"/></a>
			</div>
		</div>
	</div>
</div>


<div style="background: rgba(153, 153, 153, 0.8); " class="hide modal" id="JSIDiv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog-for-cgcoverletter" style='width: 50%;margin-top:11%;'>
		<div class="modal-content">
			<div class="modal-header">
			 <button type="button" class="close" onclick="hideJSIDiv();" data-dismiss="modal" aria-hidden="true">x </button>
				<h3 id="headingDivJSI"></h3>
			</div>
			<input type="hidden" name="showType" id="showType"/>
			<div class="modal-body" style="max-height: 400px;overflow-y:auto;">
				<div class="control-group">	
				  <div class='divErrorMsg mb10' id='errordivjsi'></div>                         
	              <div class="row hide JSIHide0">
	              <div class="col-sm-3 col-md-3 pright0 customLbl" style="width:32%;">
				    <label class="mt10" id="jsiLabel0"><spring:message code="lblCustomQuestion"/></label>
		            </div>  
				 <div class="col-sm-9 col-md-9 pleftright customTxt" style="width:65%;">
				 <select class="form-control" name="JSIname0" id="JSIId0"/>
				   <option value=""><spring:message code="msgPleaseSelectaSet"/></option>
				 </select>
				 </div>
			</div>
				<div class="row JSIHide0" style="margin-left: 0px;">
				  <label class="checkbox">
					<input type="checkbox" id="allJSI" name="allJSI" onClick="showdisabled(3);">
					<spring:message code="lblApplytoallcandidatetypes"/>
				 </label>
				</div>
				<div class="row hide JSIHide1 top5">
				 <div class="col-sm-3 col-md-3 pright0" style="width:32%;">
				    <label class="mt10" id="jsiLabel1"><spring:message code="lblCustomQuestion"/></label>
				 </div>
				 <div class="col-sm-9 col-md-9 pleftright" style="width:65%;">
				 <select class="form-control" name="JSIname1" id="JSIId1"/>
				 <option value=""><spring:message code="msgPleaseSelectaSet"/></option>
				 </select>
				 </div>
				</div>
				<div class="row  hide JSIHide2 top5">
				<div class="col-sm-3 col-md-3 pright0" style="width:32%;">
				    <label class="mt10" id="jsiLabel2"><spring:message code="lblCustomQuestion"/></label>
				 </div>
				 <div class="col-sm-9 col-md-9 pleftright" style="width:65%;">
				 <select class="form-control" name="JSIname2" id="JSIId2"/>
				 <option value=""><spring:message code="msgPleaseSelectaSet"/></option>
				 </select>
				 </div>
				</div>
				</div>
				</div>
			<div class="modal-footer" style="text-align: left;">
				<button onclick="saveJSIRecord();" class="flatbtn left5" style="background: #99cf77; width: 16%;" ><strong><spring:message code="btnSave"/> <i class="icon"></i></strong></button>
			    &nbsp;<a href="javascript:void(0);" onclick="hideJSIDiv();"><spring:message code="btnClr"/></a>
			</div>
		</div>
	</div>
</div>


<div style="background: rgba(153, 153, 153, 0.8); " class="hide modal" id="eref" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog-for-cgcoverletter" style='width: 50%;margin-top:7%;'>
		<div class="modal-content">
			<div class="modal-header">
			<button type="button" class="close" onclick="hideE_RefPopup();" data-dismiss="modal" aria-hidden="true">x </button>
				<h3 id="headingDiv1"></h3>
			</div>
			<div class="modal-body" style="max-height: 400px;overflow-y:auto;">
                 <div class='divErrorMsg mb10' id='errordiveref'></div>
				<div class="row control-group">		                         
	          		 <div class="col-sm-12 col-md-13">
		  <div class="row">
			<div id="reqNumberDiv">	
				<div class="row left15 hide ereference1" style="width:98%;">
				 <div class="col-sm-3 col-md-3 pleftright eRefLbl" style="width:34%;">
						<label class="mt10 demo" id="erefLabel0"><spring:message code="msgEReferenceQuestionSet"/></label>
						</div>
						<div class="col-sm-9 col-md-9 pleftright eRefTxt" style="width:63%;">
						<select id="avlbList0" name="avlbList0" class="form-control mt5"> 
						   <c:if test="${not empty avlbList}">
							<c:forEach var="avlst" items="${avlbList}" varStatus="status">
								<option value="${avlst.districtRequisitionId}">${avlst.requisitionNumber}</option>
							</c:forEach>
						</c:if>
					</select>
				</div>		
			</div>
			<div class="row left15" style="width:98%;">
					 <label class="checkbox">
					<input type="checkbox" id="allERef" name="allERef" onClick="showdisabled(7);">
					   <spring:message code="lblApplytoallcandidatetypes"/>
				    </label>
			</div>
			<div class="row left15 hide ereference2" style="width:98%;">
				 <div class="col-sm-3 col-md-3 pleftright" style="width:34%;">
						<label class="mt10 demo" id="erefLabel1"><spring:message code="msgEReferenceQuestionSet"/></label>
						</div>
						<div class="col-sm-9 col-md-9 pleftright" style="width:63%;">
						<select id="avlbList1" name="avlbList1" class="form-control mt5"> 
						   <c:if test="${not empty avlbList}">
							<c:forEach var="avlst" items="${avlbList}" varStatus="status">
								<option value="${avlst.districtRequisitionId}">${avlst.requisitionNumber}</option>
							</c:forEach>
						</c:if>
					</select>
				</div>		
			</div>
			
			<div class="row left15 hide ereference3" style="width:98%;">
				 <div class="col-sm-3 col-md-3 pleftright" style="width:34%;">
						<label class="mt10 demo" id="erefLabel2"><spring:message code="msgEReferenceQuestionSet"/> </label>
						</div>
						<div class="col-sm-9 col-md-9 pleftright" style="width:63%;">
						<select id="avlbList2" name="avlbList2" class="form-control mt5"> 
						   <c:if test="${not empty avlbList}">
							<c:forEach var="avlst" items="${avlbList}" varStatus="status">
								<option value="${avlst.districtRequisitionId}">${avlst.requisitionNumber}</option>
							</c:forEach>
						</c:if>
					</select>
				</div>		
			</div>
			</div>
		  </div>
				 </div>	
				 
				</div>
			</div>
			<div class="modal-footer" style="text-align: left;">
				<button onclick="saveERefRecord();" class="flatbtn left5" style="background: #99cf77; width: 16%;" ><strong><spring:message code="btnSave"/> <i class="icon"></i></strong></button>
			    &nbsp;<a href="javascript:void(0);" onclick="hideE_RefPopup();"><spring:message code="btnClr"/></a>
			</div>
		</div>
	</div>
</div>

<div style="background: rgba(153, 153, 153, 0.8); " class="hide modal" id="preScreen" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog-for-cgcoverletter" style='width: 50%;margin-top:10%;'>
		<div class="modal-content">
			<div class="modal-header">
			<button type="button" class="close" onclick="hidePrePopup();" data-dismiss="modal" aria-hidden="true">x </button>
				<h3 id="headingDivPre"></h3>
			</div>
			<div class="modal-body" style="max-height: 400px;overflow-y:auto;">
                 <div class='divErrorMsg mb10' id='errordivpre'></div>
				<div class="row left15 control-group leftrightmargin">		                         
				<div class="row preSecreenModel0" style="width:100%">
				   <div class="col-sm-3 col-md-3 pright0 preLbl" style="width:36%;">
						<label class="mt10" id="colname0"><spring:message code="lblPreScreenSets"/></label>
					</div>
					<div class="col-sm-9 col-md-9 pleftright preText" style="width:64%;">
						<select id="qqAvlbList0" name="qqAvlbList0" class="form-control mt5">  </select>
				   </div>
				</div>
				<div class="row preSecreenModel0" style="width:100%;margin-left:0px;">
					 <label class="checkbox">
					<input type="checkbox" id="allPre" name="allPre" onClick="showdisabled(1);">
					   <spring:message code="lblApplytoallcandidatetypes"/>
				    </label>
			</div>
				<div class="row preSecreenModel1" style="width:100%">
				  <div class="col-sm-3 col-md-3 pright0" style="width:36%;">
						<label class="mt10" id="colname1"><spring:message code="lblPreScreenSets"/></label>
					</div>
					<div class="col-sm-9 col-md-9 pleftright" style="width:64%;">
						<select id="qqAvlbList1" name="qqAvlbList1" class="form-control mt5">  </select>
				  </div>
				</div>
				<div class="row preSecreenModel2" style="width:100%;padding-right:0px;">
				  <div class="col-sm-3 col-md-3 pright0" style="width:36%;">
						<label class="mt10" id="colname2"><spring:message code="lblPreScreenSets"/></label>
					</div>
					<div class="col-sm-9 col-md-9 pleftright" style="width:64%;">
						<select id="qqAvlbList2" name="qqAvlbList2" class="form-control mt5"> </select>
				  </div>
		 </div>
							 
				</div>
			</div>
			<div class="modal-footer" style="text-align: left;">
				<button onclick="savePreScreenRecord();" class="flatbtn left5" style="background: #99cf77; width: 16%;" ><strong><spring:message code="btnSave"/> <i class="icon"></i></strong></button>
			    &nbsp;<a href="javascript:void(0);" onclick="hidePrePopup();"><spring:message code="btnClr"/></a>
			</div>
		</div>
	</div>
</div>

<div style="background: rgba(153, 153, 153, 0.8); " class="hide modal" id="preScreenOnBoarding" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog-for-cgcoverletter" style='width: 50%;margin-top:10%;'>
		<div class="modal-content">
			<div class="modal-header">
			<button type="button" class="close" onclick="hidePreOnPopup();" data-dismiss="modal" aria-hidden="true">x </button>
				<h3 id="headingDivPreOn"></h3>
			</div>
			<div class="modal-body" style="max-height: 400px;overflow-y:auto;">
                 <div class='divErrorMsg mb10' id='errordivpreon'></div>
				<div class="row left15 control-group leftrightmargin">		                         
				<div class="row preSecreenOnModel0" style="width:100%">
				   <div class="col-sm-3 col-md-3 pright0 onBoardLbl" style="width:29%">
						<label class="mt10" id="colnameOn0"><spring:message code="lblOnBoardingSets"/></label>
					</div>
					<div class="col-sm-9 col-md-9 pleftright onBoardTxt" style="width:69%;">
						<select id="qqAvlbListForOnboard0" name="qqAvlbListForOnboard0" class="form-control mt5">  </select>
				   </div>
				</div>
				<div class="row preSecreenOnModel0" style="width:100%;margin-left:0px;">
					 <label class="checkbox">
					<input type="checkbox" id="allPreOn" name="allPreOn" onClick="showdisabled(4);">
					   <spring:message code="lblApplytoallcandidatetypes"/>
				    </label>
			</div>	
				<div class="row preSecreenOnModel1" style="width:100%">
				  <div class="col-sm-3 col-md-3 pright0" style="width:29%">
						<label class="mt10" id="colnameOn1"><spring:message code="lblOnBoardingSets"/></label>
					</div>
					<div class="col-sm-9 col-md-9 pleftright" style="width:69%;">
						<select id="qqAvlbListForOnboard1" name="qqAvlbListForOnboard1" class="form-control mt5">  </select>
				  </div>
				</div>
				<div class="row preSecreenOnModel2" style="width:100%">
				  <div class="col-sm-3 col-md-3 pright0" style="width:29%">
						<label class="mt10" id="colnameOn2"><spring:message code="lblOnBoardingSets"/></label>
					</div>
					<div class="col-sm-9 col-md-9 pleftright" style="width:69%;">
						<select id="qqAvlbListForOnboard2" name="qqAvlbListForOnboard2" class="form-control mt5"> </select>
				  </div>
		 </div>			 
				</div>
			</div>
			<div class="modal-footer" style="text-align: left;">
				<button onclick="saveOnBoardRecord();" class="flatbtn left5" style="background: #99cf77; width: 16%;" ><strong><spring:message code="btnSave"/> <i class="icon"></i></strong></button>
			    &nbsp;<a href="javascript:void(0);" onclick="hidePreOnPopup();"><spring:message code="btnClr"/></a>
			</div>
		</div>
	</div>
</div>

<div style="background: rgba(153, 153, 153, 0.8);" class="hide modal" id="VVIDiv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog-for-cgcoverletter" style='width: 65%;margin-top:0%;'>
		<div class="modal-content">
			<div class="modal-header">
			<button type="button" class="close" onclick="hideVVIPopup();" data-dismiss="modal" aria-hidden="true">x </button>
				<h3 id="headingDivVVI"></h3>
			</div>
			<div class="modal-body" style="max-height: 550px;overflow-y:auto;">
                 <div class='divErrorMsg' id='errordivvvi' style="margin-left: 8px;"></div>
				<div class="row left15 control-group" style="width:98%;">
				<div class="row hide mt0 VVIMultiType">
						<div class="col-sm-6 col-md-6" style="padding-left: 7px;">
						<label class="radio"><input type="radio" id="sameQuestionSet" name="sameQuestionSet" onclick="showHideSingleMultiple();" checked/>
							<spring:message code="lblquestionsetforall"/></label>
						</div>
						<div class="col-sm-6 col-md-6" style="padding-left: 7px;">
						<label class="radio"><input type="radio" id="diffQuestionSet" name="sameQuestionSet" onclick="showHideSingleMultiple();"/>
							<spring:message code="lblDifferentquestionset"/></label>
						</div>
				</div>
				<div class="">	
				<div class="row mt0">
						<div class="col-sm-6 col-md-6 hide VVI0" id="" style="padding-left:8px;">
							<label id="captionForExternal"><spring:message code="lblPlzSeltQuesSet"/></label>
		            		<input type="hidden" id="quesId0" name="quesId0" value=""> 
		             		<input type="text" id="quesName0" name="districtName"  class="form-control"
		             		onfocus="getQuestionSetAutoComp(this, event, 'divTxtShowData0', 'quesName0','quesId0','0');"
							onkeyup="getQuestionSetAutoComp(this, event, 'divTxtShowData0', 'quesName0','quesId0','0');"
							onblur="hideDistrictMasterDiv(this,'quesId0','divTxtShowData0');"	
							value=""/>
							<div id='divTxtShowData0' style=' display:none;position:absolute;z-index:5000;' onmouseover="mouseOverChk('divTxtShowData0','quesName0')" class='result' ></div>
						</div>
						
						</div>
					
					<div class="row mt0 hide VVI1">
						<div class="col-sm-6 col-md-6 hide VVI1 mt5" id="" style="padding-left:8px;">
							<label id="captionForInternal"><spring:message code="lblPlzSeltQuesSet"/></label>
		            		<input type="hidden" id="quesId1" name="quesId1" value=""> 
		             		<input type="text" id="quesName1" name="districtName1"  class="form-control"
		             		onfocus="getQuestionSetAutoComp(this, event, 'divTxtShowData1', 'quesName1','quesId1','1');"
							onkeyup="getQuestionSetAutoComp(this, event, 'divTxtShowData1', 'quesName1','quesId1','1');"
							onblur="hideDistrictMasterDiv(this,'quesId1','divTxtShowData1');"	
							value=""/>
							<div id='divTxtShowData1' style=' display:none;position:absolute;z-index:5000;' onmouseover="mouseOverChk('divTxtShowData1','quesName1')" class='result' ></div>
						</div>
					</div>
					<div class="row mt0 hide VVI2">
						<div class="col-sm-6 col-md-6 hide VVI2 mt5" id="" style="padding-left:8px;">
							<label id="captionForInternalTransfer"><spring:message code="lblPlzSeltQuesSet"/></label>
		            		<input type="hidden" id="quesId2" name="quesId2" value=""> 
		            		<input type="hidden" id="districtMaxScore" name="districtMaxScore" value=""> 
		             		<input type="text" id="quesName2" name="districtName2"  class="form-control"
		             		onfocus="getQuestionSetAutoComp(this, event, 'divTxtShowData2', 'quesName2','quesId2','2');"
							onkeyup="getQuestionSetAutoComp(this, event, 'divTxtShowData2', 'quesName2','quesId2','2');"
							onblur="hideDistrictMasterDiv(this,'quesId2','divTxtShowData2');"	
							value=""/>
							<div id='divTxtShowData2' style=' display:none;position:absolute;z-index:5000;' onmouseover="mouseOverChk('divTxtShowData2','quesName2')" class='result'></div>
						</div>
					</div>
					<div class="row mt5 checkbox mb0" style="padding-left:13px;">
						<div class="col-sm-3 col-md-3 mt0 mb0">
									<label class="mb0"><input type="checkbox" name="wantScore" id="wantScore" onclick="showMarksDiv();"/>
									<spring:message code="lblWantSco"/></label>
						</div>
				   </div>
				    <div class="row mt0 mb0">
				         <div class="col-sm-6 col-md-6 hide " id="marksDiv">
							<label class="mb0"><spring:message code="lblMMarks"/></label>
							<input type="text" id="maxScoreForVVI" name="maxScoreForVVI" class="form-control mt5" maxlength="3" onkeypress="return checkForInt(event)">
						</div>
					</div>
				<div class="row mt0 mb0">
					<div class="col-sm-12 col-md-12 "  style="padding-left:8px;">
								<label class="checkbox"><input type="checkbox" name="sendAutoVVILink" id="sendAutoVVILink" onclick="showLinkDivNew();">
								<spring:message code="lblWntToSendInrLnkAuto"/>  </label>	
					</div>
				</div>
				<div class="hide row" id="autolinkDiv" style="margin-top:-9px;">
						<div class="col-sm-5 col-md-5" style="width:37%;">
							<label><spring:message code="lblLnkWiBeSendAudoOnceAdminSet" /></label>
							</div>
							<div class="col-sm-2 col-md-2 top25-sm23 leftrightmargin" style="text-align: center;width: 29%;">
							<select id="statusDiv" name="statusDiv" class="form-control" style="height: 30px;"></select>
							<img class="statusDivLoading" id="statusDivLoading" src="images/loadingAnimation.gif" style="margin-top: -30px;display:none;">
						    </div>
						   <div class="col-sm-3 col-md-3" style="width:28%;padding-left:8px;padding-right:0px;">
							<label class="mb0">
								<spring:message code="lblStAgApp" />
							</label>
						</div>
				</div>
				<div class="row mt5">
					<div class="col-sm-6 col-md-6" style="padding-left:8px;">
						<label class="mb0"><spring:message code="msgTimeAllowedPerQuestion"/>  	</label>
						<input type="text" class="form-control mt5" maxlength="4" id="timePerQues" onkeypress="return checkForInt(event)" />
					</div>
					<div class="col-sm-6 col-md-6" id="">
						<label class="mb0"><spring:message code="lblVVItrExInDays"/></label>
						<input type="text" class="form-control mt5" maxlength="2" id="vviExpDays" onkeypress="return checkForInt(event)" />
					</div>
				</div>
			</div>	
			
            <div class="row" style="width:100%;margin-left:-7px;">
					 <label class="checkbox">
					<input type="checkbox" id="allVVI" name="allVVI">
					   <spring:message code="lblApplytoallcandidatetypes"/>
				    </label>
			</div>				 
				</div>
			</div>
			<div class="modal-footer" style="text-align: left;">
				<button onclick="saveVVIRecord();" class="flatbtn left15" style="background: #99cf77; width: 16%;" ><strong><spring:message code="btnSave"/> <i class="icon"></i></strong></button>
			    &nbsp;<a href="javascript:void(0);" onclick="hideVVIPopup();"><spring:message code="btnClr"/></a>
			</div>
		</div>
	</div>
</div>

<div style="background: rgba(153, 153, 153, 0.8); " class="hide modal" id="assessmentDiv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog-for-cgcoverletter" style='width: 50%;margin-top:10%;'>
		<div class="modal-content">
			<div class="modal-header">
			<button type="button" class="close" onclick="hideAssPopup();" data-dismiss="modal" aria-hidden="true">x </button>
				<h3 id="headingDivAssessment"></h3>
			</div>
			<div class="modal-body" style="max-height: 400px;">
             <div class='divErrorMsg mb10' id='errordivassessment'></div>
             
			 <div class="row hide onAss0">
			  <div class="col-sm-3 col-md-3 pright0 assLbl0" style="width:44%;">
					<label id="colnameOnAss0" style="margin-top:18px;"><spring:message code="msgOnlineAssessment"/></label>
			 </div>
			 <input type="hidden" id="selectId0"/>
			 <input type="hidden" id="selectValue0"/>
			   <dl class="dropdown col-sm-9 col-md-9 pleftright assTxt0" style="width:40%;"> 
			    <dt id="dt0">
			    <a class="multi0" href="javascript:void(0);">
			      <span class="hida0" style="font-family: 'Century Gothic','Open Sans', sans-serif;"><spring:message code="lblSltDistAss"/></span>    
			      <p class="multiSel" style="margin-bottom: 0px;"></p>  
			    </a>
			    </dt>
			      <dd>
			        <div class="mutliSelect0"></div>
			    </dd>
			</dl>
			</div>
			 <div class="row hide onAss0" style="width:100%;">
                 <div class="col-sm-5 col-md-5">
					 <label class="checkbox">
					<input type="checkbox" id="allAss" name="allAss" onClick="showdisabled(6);">
					   <spring:message code="lblApplytoallcandidatetypes"/>
				    </label>
				  </div>
			</div>	
			<div class="row hide onAss1">
			  <div class="col-sm-3 col-md-3 pright0 assLbl1" style="width:44%;">
					<label id="colnameOnAss1" style="margin-top:18px;"><spring:message code="msgOnlineAssessment"/></label>
			 </div>
			 <input type="hidden" id="selectId1"/>
			 <input type="hidden" id="selectValue1"/>
			   <dl class="dropdown0 col-sm-9 col-md-9  pleftright assTxt1" style="width:40%;"> 
			    <dt id="dt1">
			    <a class="multi1" href="javascript:void(0);">
			      <span class="hida1" style="font-family: 'Century Gothic','Open Sans', sans-serif;"><spring:message code="lblSltDistAss"/></span>    
			      <p class="multiSel0" style="margin-bottom: 0px;"></p>  
			    </a>
			    </dt>
			      <dd>
			        <div class="mutliSelect1"></div>
			    </dd>
			</dl>
			</div>
			<div class="row hide onAss2">
			  <div class="col-sm-3 col-md-3 pright0 assLbl2" style="width:44%;">
					<label id="colnameOnAss2" style="margin-top:18px;"><spring:message code="msgOnlineAssessment"/></label>
			 </div>
			 <input type="hidden" id="selectId2"/>
			 <input type="hidden" id="selectValue2"/>
			   <dl class="dropdown1 col-sm-9 col-md-9  pleftright assTxt2" style="width:40%;"> 
			    <dt>
			    <a class="multi2" href="javascript:void(0);">
			      <span class="hida2" style="font-family: 'Century Gothic','Open Sans', sans-serif;"><spring:message code="lblSltDistAss"/></span>    
			      <p class="multiSel1" style="margin-bottom: 0px;"></p>  
			    </a>
			    </dt>
			      <dd>
			        <div class="mutliSelect2"></div>
			    </dd>
			</dl>
			</div>
			
			 <input type="hidden" id="schoolSelectionHidden" name="schoolSelectionHidden" value="0"/>
			<div class="row mt0 mb30">
				<div class="col-sm-12 col-md-12 inline mb0 top0">
					<label class="mb0 checkbox"><input type="checkbox" name="schoolSelection" id="schoolSelection">
					<strong><spring:message code="lnkSchoolSelection"/></strong></label>
				</div>
			</div>
         			 
			<div class="row" style="text-align: left;">
				<button onclick="saveAssessmentRecord();" class="flatbtn left15" style="background: #99cf77; width: 16%;" ><strong><spring:message code="btnSave"/> <i class="icon"></i></strong></button>
			    &nbsp;<a href="javascript:void(0);" onclick="hideAssPopup();"><spring:message code="btnClr"/></a>
			</div>
			</div>
		</div>
	  </div>
</div>


<div class="modal hide" id="virusDivId" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog-for-cgcoverletter" style='width: 35%;'>
		<div class="modal-content">
			<div class="modal-header">
				<h3 id="myModalLabel"><spring:message code="headTm"/></h3>
			</div>
			<div class="modal-body" style="max-height: 400px;overflow-y:auto;">
				<div class="control-group">
				</div>
			</div>
			<div class="modal-footer">
				<span id=""><button class="btn btn-primary" data-dismiss="modal" onclick="showDSPQDiv();"><spring:message code="btnOk"/> <i class="icon"></i></button></span>&nbsp;&nbsp;
			</div>
		</div>
	</div>
</div>

<div class="modal hide" id="removeMemberConfirmation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false" removeMemberGroupId="" removeMemberId="">
	<div class="modal-dialog-for-cgcoverletter" style='width: 35%;'>
		<div class="modal-content">
			<div class="modal-header">
				<h3 id="myModalLabel"><spring:message code="headTm"/></h3>
			</div>
			<div class="modal-body" style="max-height: 400px;overflow-y:auto;">
				<div class="control-group">
					<spring:message code="msgRemoveThisMembr"/>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-primary" data-dismiss="modal" onclick="removeMember()"><spring:message code="btnOk"/> <i class="icon"></i></button>&nbsp;
				<button class="btn" data-dismiss="modal"><spring:message code="btnClr"/></button> &nbsp;&nbsp;
			</div>
		</div>
	</div>
</div>

<div class="modal hide" id="removeGroupConfirmation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false" removeGroupId="">
	<div class="modal-dialog-for-cgcoverletter" style='width: 35%;'>
		<div class="modal-content">
			<div class="modal-header">
				<h3 id="myModalLabel"><spring:message code="headTm"/></h3>
			</div>
			<div class="modal-body" style="max-height: 400px;overflow-y:auto;">
				<div class="control-group">
					<spring:message code="msgRemoveThisGrp"/>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-primary" data-dismiss="modal" onclick="removeGroup()"><spring:message code="btnOk"/> <i class="icon"></i></button>&nbsp;
				<button class="btn" data-dismiss="modal"><spring:message code="btnClr"/></button> &nbsp;&nbsp;
			</div>
		</div>
	</div>
</div>

<style>
.dropdown,.dropdown0,.dropdown1,.dropdownAss{
margin-bottom: 0px;
}
.dropdown dd, .dropdown dt {
    margin:0px;
    padding:0px;
}
.dropdown0 dd, .dropdown0 dt {
    margin:0px;
    padding:0px;
}
.dropdown1 dd, .dropdown1 dt {
    margin:0px;
    padding:0px;
}
.dropdownAss dd, .dropdownAss dt {
    margin:0px;
    padding:0px;
}
.dropdown ul,.dropdown0 ul,.dropdown1 ul,.dropdownAss ul {
    margin: -1px 0 0 0;
    z-index: 50;
}
.dropdown dd,.dropdown0 dd,.dropdown1 dd,.dropdownAss dd {
    position:relative;
}
.dropdown a, 
.dropdown a:visited {

    text-decoration:none;
    outline:none;
    font-size: 12px;
     background: transparent;
   padding: 5px;
   line-height: 1;
   border: 0;
   border-radius: 0;
   -webkit-appearance: none;
}
.dropdown0 a, 
.dropdown0 a:visited {

    text-decoration:none;
    outline:none;
    font-size: 12px;
     background: transparent;
   padding: 5px;
   line-height: 1;
   border: 0;
   border-radius: 0;
   -webkit-appearance: none;
}
.dropdown1 a, 
.dropdown1 a:visited {

    text-decoration:none;
    outline:none;
    font-size: 12px;
     background: transparent;
   padding: 5px;
   line-height: 1;
   border: 0;
   border-radius: 0;
   -webkit-appearance: none;
}
.dropdownAss a, 
.dropdownAss a:visited {

    text-decoration:none;
    outline:none;
    font-size: 12px;
     background: transparent;
   padding: 5px;
   line-height: 1;
   border: 0;
   border-radius: 0;
   -webkit-appearance: none;
}
.dropdown dt a,.dropdown0 dt a,.dropdown1 dt a,.dropdownAss dt a  {
    display:block;
    max-height: 34px;
    min-height: 34px;
    line-height: 20px;
    overflow: auto;
    border:0;
    width:272px;
    background-color: #ffffff;
    border: 1px solid #cccccc;
    border-radius: 4px;
    
}
.dropdownAss dt a span, .multiSelAss span {
    cursor:pointer;
    display:inline-block;
    padding: 0 3px 2px 0;
    font-family: inherit;text-transform: none;font: 13.3333px Arial;    text-align: start;
    font-size: 11px;
    color: #716767;
}
.dropdown dt a span, .multiSel span {
    cursor:pointer;
    display:inline-block;
    padding: 0 3px 2px 0;
    font-family: inherit;text-transform: none;font: 13.3333px Arial;    text-align: start;
    font-size: 11px;
    color: #716767;
}
.dropdown0 dt a span, .multiSel0 span {
    cursor:pointer;
    display:inline-block;
    padding: 0 3px 2px 0;
    font-family: inherit;text-transform: none;font: 13.3333px Arial;    text-align: start;
    font-size: 11px;
    color: #716767;
}
.dropdown1 dt a span, .multiSel1 span {
    cursor:pointer;
    display:inline-block;
    padding: 0 3px 2px 0;
    font-family: inherit;text-transform: none;font: 13.3333px Arial;    text-align: start;
    font-size: 11px;
    color: #716767;
}
.dropdown dd ul,.dropdown0 dd ul,.dropdown1 dd ul,.dropdownAss dd ul {
    background-color: #ffffff;
    border: 1px solid #cccccc;
    display:none;
    left:0px;
    padding: 2px 15px 2px 5px;
    position:absolute;
    width:272px;
    list-style:none;
    overflow:auto;
}
.dropdown span.value {
    display:none;
    font-family: inherit;text-transform: none;font: 13.3333px Arial;    text-align: start;
}
.dropdown0 span.value {
    display:none;
    font-family: inherit;text-transform: none;font: 13.3333px Arial;    text-align: start;
}
.dropdown1 span.value {
    display:none;
    font-family: inherit;text-transform: none;font: 13.3333px Arial;    text-align: start;
}
.dropdownAss span.value {
    display:none;
    font-family: inherit;text-transform: none;font: 13.3333px Arial;    text-align: start;
}
.dropdown dd ul li a {
    padding:5px;
    display:block;
}
.dropdown0 dd ul li a {
    padding:5px;
    display:block;
}
.dropdown1 dd ul li a {
    padding:5px;
    display:block;
}
.dropdownAss dd ul li a {
    padding:5px;
    display:block;
}
.dropdown dd ul li a:hover {
    background-color:#fff;
}
.dropdown0 dd ul li a:hover {
    background-color:#fff;
}
.dropdown1 dd ul li a:hover {
    background-color:#fff;
}
.dropdownAss dd ul li a:hover {
    background-color:#fff;
}
#onlineAssessmentData li:hover {
 background-color:#F1F1F1; 
 }
</style>
                                
<script>
    $(".dropdown dt a").on('click', function () {
          $(".dropdown dd ul").slideToggle('fast');
      });

    $(".dropdown0 dt a").on('click', function () {
    if($("#allAss").is(":checked")){}else{
          $(".dropdown0 dd ul").slideToggle('fast');
          }
      }); 
      
      $(".dropdown1 dt a").on('click', function () {
      if($("#allAss").is(":checked")){}else{
          $(".dropdown1 dd ul").slideToggle('fast');
          }
      });
      
       $(".dropdownAss dt a").on('click', function () {
          $(".dropdownAss dd ul").slideToggle('fast');
      });
      
      $(".dropdown dd ul li a").on('click', function () {
          $(".dropdown dd ul").hide();
      });
      
        $(".dropdown0 dd ul li a").on('click', function () {
          $(".dropdown0 dd ul").hide();
      });

      $(".dropdown1 dd ul li a").on('click', function () {
          $(".dropdown1 dd ul").hide();
      });
      $(".dropdownAss dd ul li a").on('click', function () {
          $(".dropdownAss dd ul").hide();
      });
      function getSelectedValue(id) {
           return $("#" + id).find("dt a span.value").html();
      }

      $(document).bind('click', function (e) {
          var $clicked = $(e.target);
          if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
      });
      
      $(document).bind('click', function (e) {
          var $clicked = $(e.target);
          if (!$clicked.parents().hasClass("dropdown0")) $(".dropdown0 dd ul").hide();
      });
      $(document).bind('click', function (e) {
          var $clicked = $(e.target);
          if (!$clicked.parents().hasClass("dropdown1")) $(".dropdown1 dd ul").hide();
      });
      $(document).bind('click', function (e) {
          var $clicked = $(e.target);
          if (!$clicked.parents().hasClass("dropdownAss")) $(".dropdownAss dd ul").hide();
      });

</script> 


<div  class="modal hide"  id="portfoliowarning" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
	<div class="modal-dialog" style="width: 850px;">
	<div class="modal-content">	
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick="closeDiv();">x</button>
		<h3 id="myModalLabel">Teachermatch</h3>
	</div>
	<div class="modal-body" style="max-height: 450px;overflow-y: auto">		
		<div class="control-group">
			<div class="" id="warningmsg"></div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true"   onclick="closeDiv();">Close</button> 		
 	</div>
   </div>
 </div>
</div>


<div  class="modal hide"  id="portfolioChangeWarning" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
	<div class="modal-dialog" style="width: 850px;">
	<div class="modal-content">	
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick="portfolioChangeWarningCloseDiv();">x</button>
		<h3 id="myModalLabel">Teachermatch</h3>
	</div>
	<div class="modal-body" style="max-height: 450px;overflow-y: auto">		
		<div class="control-group">
			<div class="" id="portfolioChangeWarningMsg"></div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true"   onclick="portfolioChangeWarningYes();">Yes</button>
 		<button class="btn" data-dismiss="modal" aria-hidden="true"   onclick="portfolioChangeWarningCloseDiv();">No</button> 		
 	</div>
   </div>
 </div>
</div>

<input type="hidden" name="candidateTypePortChange" id="candidateTypePortChange" value="" />
<input type="hidden" name="applyToAllCandidateTypes" id="applyToAllCandidateTypes" value="" />
<input type="hidden" name="candidateType_AllEdit" id="candidateType_AllEdit" value="" />
