<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<script type="text/javascript" src="dwr/interface/AssessmentCampaignAjax.js?ver=${resourceMap['AssessmentCampaignAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type='text/javascript' src='js/assessment-campaign.js?ver=${resourceMap["js/assessment-campaign.js"]}'></script>

<c:set var="redirectURL" value="userdashboard.do"></c:set>
<div class="row">
	<div class="span14">
		<div class="span14 centerline offset1 mt30">
			<div class="span1 m0"><img width="41" height="41" src="images/jobsofinterest.png"></div>
			<div class="span7 subheading"><spring:message code="hdJDetail"/></div>
		</div>
	</div>
</div>

<div class=" offset1">
	<div class=" row">
		<div class="span14 centerline1">
			<div class=" row mt15">
			<div class="span2"><spring:message code="lblJobTitle"/></div>
			<div class="span11">${jobOrder.jobTitle}${batchJobOrder.jobTitle}&nbsp;</div>			
			<div class="span2 mt10">District Name: </div>
			<div class="span4  mt10">${jobOrder.districtMaster.districtName}${batchJobOrder.districtMaster.districtName}&nbsp;</div>
			</div>		
		</div>
		
		<div class="span14">
			<div class="row mt10">
			<div class="span14"><strong><spring:message code="headJobDescrip"/></strong></div></div>
			<p>${jobOrder.jobDescriptionHTML}${batchJobOrder.jobDescriptionHTML}&nbsp; </p>			
		</div>
		
		<br>
		       
	</div>

</div>