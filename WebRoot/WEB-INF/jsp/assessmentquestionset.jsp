<!-- @Author: Hanzala 
 * @Discription: view of edit domain Page.
 -->
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="resourceMap" value="${applicationScope.resouceMap}" />
<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>

<script type="text/javascript"
	src="dwr/interface/DistrictSpecificQuestionsSetQuesAjax.js?ver=${resourceMap['DistrictSpecificQuestionsSetQuesAjax.ajax']}"></script>
<script type="text/javascript"
	src="dwr/interface/DistrictQuestionsAjax.js?ver=${resourceMap['DistrictQuestionsAjax.ajax']}"></script>
<script type='text/javascript'
	src="js/districtquestions.js?ver=${resourceMap['js/districtquestions.js']}"></script>

<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type="text/javascript">
var questionTypeJSON=${json};
function findSelected(idd)
{
	for (x in questionTypeJSON)
  	{
	  if(idd==x){
	  	return questionTypeJSON[x];
	  }
  	}
}
 //$('textarea').jqte();
 //alert("dddd");
</script>
<script type='text/javascript' src="js/assessmentquestionset.js?ver=${resourceMap['js/assessmentquestionset.js']}"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" /> 

<style type="text/css">
	textarea{width:500px;height:100px;}
	.jqte{  }
	.errMsg {color: red;}
</style>
<script type="text/javascript">
var $j=jQuery.noConflict();
        $j(document).ready(function() {
        });
        
function applyScrollOnTbl()
{
		var $j=jQuery.noConflict();
        $j(document).ready(function() {
	        $j('#refChkQuesTable').fixheadertable({ //table id 
		        caption: '',
		        showhide: false,
		        theme: 'ui',
		        height: 280,
		        width: 945,
		        minWidth: null,
		        minWidthAuto: false,
		        colratio:[682,100,166], // table header width
		        addTitles: false,
		        zebra: true,
		        zebraClass: 'net-alternative-row',
		        sortable: false,
		        sortedColId: null,
		        //sortType:[],
		        dateFormat: 'd-m-y',
		        pager: false,
		        rowsPerPage: 10,
		        resizeCol: false,
		        minColWidth: 100,
		        wrapper: false
	        });
        });			
}

function applyScrollOnTbl_1()
{
		var $j=jQuery.noConflict();
        $j(document).ready(function() {
	        $j('#refChkQuesSetQuesTable').fixheadertable({ //table id 
		        caption: '',
		        showhide: false,
		        theme: 'ui',
		        height: 280,
		        width: 965,
		        minWidth: null,
		        minWidthAuto: false,
		        colratio:[782,166], // table header width
		        addTitles: false,
		        zebra: true,
		        zebraClass: 'net-alternative-row',
		        sortable: false,
		        sortedColId: null,
		        //sortType:[],
		        dateFormat: 'd-m-y',
		        pager: false,
		        rowsPerPage: 10,
		        resizeCol: false,
		        minColWidth: 100,
		        wrapper: false
	        });
        });			
}

</script>
<script language="javascript">
$(document).ready(function() {
$('#iconpophover').tooltip();
$('#iconpophover1').tooltip();
	});
</script>
<input type="hidden" id="districtId" value="${distID}" />
<input type="hidden" id="headQuarterId"  value="${param.headQuarterId}"/>
<div class="row" style="margin: 0px;">
	<div style="float: left;">
		<img src="images/manageusers.png" width="41" height="41" alt="">
	</div>
	<div style="float: left;">
		<div class="subheading" style="font-size: 13px;">
			<spring:message code="headMngDistSpeciQues" />
		</div>
	</div>
	<div class="pull-right add-employment1">
	<c:if test="${addQuesFlag}">
		<a href="javascript:void(0);" onclick="return addNewQues()"><spring:message
				code="lnkAddQues" />
		</a>
		</c:if>
	</div>
	<div style="clear: both;"></div>
	<div class="centerline"></div>
</div>

<div class="row col-sm-12 col-md-12">
	<div class="row">
		<div class="mt10">
			<div class="subheading" style="font-size: 13px;">
				${quesSetName}
			</div>
		</div>
	</div>
</div>
<div class="TableContent top15">
	<div class="table-responsive" id="i4QuesSetQuesGrid"></div>
</div>
<div class="hide" id="addQuesDiv">

	<div class="row col-sm-12 col-md-12">
		<div class='errMsg' id="errQuesdiv"></div>
	</div>
	<div class="row">
		<div class="col-sm-12 col-md-12">
			<div>
				<input type="hidden" name="quesDate" id="quesDate">
				<input type="hidden" name="quesSetId" id="quesSetId"
					value="${quesSetId}">
				<span id="i4QuesTxt">
					<div id="divAssessmentRow" style="display: block;">
						<div class="row top5">
							<div class="col-sm-8 col-md-8">
								<div class='divErrorMsg' id='errordiv'></div>
								<div class='divErrorMsg' id='errordiv1'></div>
							</div>
						</div>

						<div class="row top10">
							<div class="col-sm-7 col-md-7" id="assQuestion">
								<label>
									<strong><spring:message code="lblQues" /><span
										class="required">*</span>
									</strong>
								</label>
								<textarea class="form-control" name="question" id="question"
									maxlength="2500" rows="5"
									onkeyup="return chkLenOfTextArea(this,2500);"
									onblur="return chkLenOfTextArea(this,2500);"></textarea>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-3 col-md-3">
								<label>
									<strong><spring:message code="headQuesType" /><span
										class="required">*</span>
									</strong>
								</label>
								<select class="form-control " name="questionTypeMaster"
									id="questionTypeMaster"
									onchange="getQuestionOptions(this.value)">
									<option value="0">
										<spring:message code="optSltQuesType" />
									</option>
									<c:forEach items="${questionTypeMasters}"
										var="questionTypeMaster">
										<option id="${questionTypeMaster.questionTypeId}"
											value="${questionTypeMaster.questionTypeId}">
											${questionTypeMaster.questionType}
										</option>
									</c:forEach>
								</select>
							</div>

							<!--
							  <div class="col-sm-3 col-md-3">
						        	<label>Question Max Score<span class="required">*</span></label>
						        	<input type="text" id="questionMaxScore" name="questionMaxScore" maxlength="3" value="" class="form-control" style="width:92%;" onkeypress="return checkForInt(event);">
						      </div>
							-->
						</div>

<!--						<div class="row top5" id="row0" style="display: none">-->
<!--							<div class="spanfit1 col-sm5 col-md-5">-->
<!--								<label>-->
<!--									<strong><spring:message code="lblOpt" /> <a href="#"-->
<!--										id="iconpophover" rel="tooltip"-->
<!--										data-original-title="Select the check box against the valid option(s)"><img-->
<!--												src="images/qua-icon.png" width="15" height="15" alt="">-->
<!--									</a>-->
<!--									</strong>-->
<!--								</label>-->
<!--							</div>-->
<!--							<div class="spanfit1 col-sm5 col-md-5">-->
<!--								<label>-->
<!--									<strong>&nbsp;</strong>-->
<!--								</label>-->
<!--							</div>-->
<!--						</div>-->

						<div class="row" id="rowExplain" style="display: none;">
							<div class="col-sm-7 col-md-7" id="assExplanation">
								<label>
									<strong><spring:message code="lblExp" />
									</strong>
								</label>
								<textarea class="span9" name="questionExplanation"
									id="questionExplanation" maxlength="2500" rows="4"
									onkeyup="return chkLenOfTextArea(this,2500);"
									onblur="return chkLenOfTextArea(this,2500);"></textarea>
							</div>
						</div>
<div class="row top5" id="row0" style="display: none;">
<div class="col-sm3 col-md-3" style="padding-right: 0px;"><label><strong><spring:message code="lblOpt"/> <a  href="#" id="iconpophover" rel="tooltip" data-original-title="Select the check box against the valid option(s)"><img src="images/qua-icon.png" width="15" height="15" alt=""></a></strong></label>
	</div>
		<!--	hafeez-->
	<div class="col-sm3 col-md-3" id="explanationRequired" style="margin-left: 0px;padding-left: 0px;display: none;"><label><strong>&nbsp;<spring:message code="lblexplanationRequired"/>
	<a  href="#" id="iconpophover1" rel="tooltip" data-original-title="Select the check box against the explanation requird option(s)"><img src="images/qua-icon.png" width="15" height="15" alt=""></a>
	</strong></label>
	</div>
		<!--	hafeez-->
	</div>
		<div class="row top10" id="row1" style="display: none">
							<div class="spanfit1 left15 pull-left">
								<div class="top7 pull-left">
									1
								</div>
								<div class="pull-left" id="firstColumn" style="width: 200px; margin-left: 5px;">
									<input type="hidden" name="hid1" id="hid1" />
									<input type="text" name="opt1" id="opt1" class="form-control"
										maxlength="750" placeholder="">
								</div>
								<div class="pull-left top7"
									style="width: 40px; margin-left: 5px;">
									<input type="checkbox" name="valid1" id="valid1">
								</div>
								<!--	hafeez-->
				<div class="pull-left top7" id="row1WithExplanation1" style="width: 40px;margin-left: 5px;display: none;">
				<input type="checkbox"  id="explanationRequired1" name="explanationRequired1" >
				</div>	
				<!--	hafeez-->
								<div class="clearfix"></div>
							</div>

							<div class="spanfit1 pull-left">
								<div class="top7 pull-left">
									2
								</div>
								<div class="pull-left" id="secondColumn" style="width: 200px; margin-left: 5px;">
									<input type="hidden" name="hid2" id="hid2" />
									<input type="text" name="opt2" id="opt2" class="form-control"
										maxlength="750" placeholder="">
								</div>
								<div class="pull-left top7"
									style="width: 40px; margin-left: 5px;">
									<input type="checkbox" name="valid2" id="valid2">
								</div>
								<!--	hafeez-->
				<div class="pull-left top7" id="row1WithExplanation2" style="width: 40px;margin-left: 5px;display: none;">
				<input type="checkbox" id="explanationRequired2" name="explanationRequired2" >
				</div>
				<!--	hafeez-->
								<div class="clearfix"></div>
							</div>
						</div>

						<div class="row top5" id="row2" style="display: none">
							<div class="spanfit1 left15 pull-left">
								<div class="top7 pull-left">
									3
								</div>
								<div class="pull-left" style="width: 200px; margin-left: 5px;">
									<input type="hidden" name="hid3" id="hid3" />
									<input type="text" name="opt3" id="opt3" class="form-control"
										maxlength="750" placeholder="">
								</div>
								<div class="pull-left  top7"
									style="width: 40px; margin-left: 5px;">
									<input type="checkbox" name="valid3" id="valid3">
								</div>
								<div class="clearfix"></div>
							</div>

							<div class="spanfit1  pull-left">
								<div class="top7 pull-left">
									4
								</div>
								<div class="pull-left" style="width: 200px; margin-left: 5px;">
									<input type="hidden" name="hid4" id="hid4" />
									<input type="text" name="opt4" id="opt4" class="form-control"
										maxlength="750" placeholder="">
								</div>
								<div class="pull-left top7"
									style="width: 40px; margin-left: 5px;">
									<input type="checkbox" name="valid4" id="valid4">
								</div>
								<div class="clearfix"></div>
							</div>
						</div>

						<div class="row top5" id="row3" style="display: none">
							<div class="spanfit1 left15 pull-left">
								<div class="top7 pull-left">
									5
								</div>
								<div class="pull-left" style="width: 200px; margin-left: 5px;">
									<input type="hidden" name="hid5" id="hid5" />
									<input type="text" name="opt5" id="opt5" class="form-control"
										maxlength="750" placeholder="">
								</div>
								<div class="pull-left top7"
									style="width: 40px; margin-left: 5px;">
									<input type="checkbox" name="valid5" id="valid5">
								</div>
								<div class="clearfix"></div>
							</div>

							<div class="spanfit1  pull-left">
								<div class="top7 pull-left">
									6
								</div>
								<div class="pull-left" style="width: 200px; margin-left: 5px;">
									<input type="hidden" name="hid6" id="hid6" />
									<input type="text" name="opt6" id="opt6" class="form-control"
										maxlength="750" placeholder="">
								</div>
								<div class="pull-left top7" style="width: 40px;">
									&nbsp;
									<input type="checkbox" name="valid6" id="valid6">
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
						
				<c:choose>  
	              	<c:when test="${kFlag eq 'yes'}">
						
						<div class="row top5" id="row4" style="display: none">
							<div class="spanfit1 left15 pull-left">
								<div class="top7 pull-left">
									7
								</div>
								<div class="pull-left" style="width: 200px; margin-left: 5px;">
									<input type="hidden" name="hid7" id="hid7" />
									<input type="text" name="opt7" id="opt7" class="form-control"
										maxlength="750" placeholder="">
								</div>
								<div class="pull-left top7"
									style="width: 40px; margin-left: 5px;">
									<input type="checkbox" name="valid7" id="valid7">
								</div>
								<div class="clearfix"></div>
							</div>

							<div class="spanfit1  pull-left">
								<div class="top7 pull-left">
									8
								</div>
								<div class="pull-left" style="width: 200px; margin-left: 5px;">
									<input type="hidden" name="hid8" id="hid6" />
									<input type="text" name="opt8" id="opt8" class="form-control"
										maxlength="750" placeholder="">
								</div>
								<div class="pull-left top7" style="width: 40px;">
									&nbsp;&nbsp;
									<input type="checkbox" name="valid8" id="valid8">
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
						</c:when>
						<c:otherwise>
						<div class="row top5" id="row4h" style="display: none">
							<div class="spanfit1 left15 pull-left">
								<div class="top7 pull-left">
									7
								</div>
								<div class="pull-left" style="width: 200px; margin-left: 5px;">
									<input type="hidden" name="hid7" id="hid7" />
									<input type="text" name="opt7" id="opt7" class="form-control"
										maxlength="750" placeholder="">
								</div>
								<div class="pull-left top7"
									style="width: 40px; margin-left: 5px;">
									<input type="checkbox" name="valid7" id="valid7">
								</div>
								<div class="clearfix"></div>
							</div>

							<div class="spanfit1  pull-left">
								<div class="top7 pull-left">
									8
								</div>
								<div class="pull-left" style="width: 200px; margin-left: 5px;">
									<input type="hidden" name="hid8" id="hid6" />
									<input type="text" name="opt8" id="opt8" class="form-control"
										maxlength="750" placeholder="">
								</div>
								<div class="pull-left top7" style="width: 40px;">
									&nbsp;&nbsp;
									<input type="checkbox" name="valid8" id="valid8">
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
						
						</c:otherwise>
						</c:choose>
						
				<c:choose>  
	              	<c:when test="${kFlag eq 'yes'}">
						<div class="row top5" id="row5" style="display: none">
							<div class="spanfit1 left15 pull-left">
								<div class="top7 pull-left">
									9
								</div>
								<div class="pull-left" style="width: 200px; margin-left: 5px;">
									<input type="hidden" name="hid9" id="hid9" />
									<input type="text" name="opt9" id="opt9" class="form-control"
										maxlength="750" placeholder="">
								</div>
								<div class="pull-left top7"
									style="width: 40px; margin-left: 5px;">
									<input type="checkbox" name="valid9" id="valid9">
								</div>
								<div class="clearfix"></div>
							</div>

							<div class="spanfit1  pull-left">
								<div class="top7 pull-left">
									10
								</div>
								<div class="pull-left" style="width: 200px; margin-left: 5px;">
									<input type="hidden" name="hid6" id="hid6" />
									<input type="text" name="opt10" id="opt10" class="form-control"
										maxlength="750" placeholder="">
								</div>
								<div class="pull-left top7" style="width: 40px;">
									&nbsp;&nbsp;
									<input type="checkbox" name="valid10" id="valid10">
								</div>
								<div class="clearfix"></div>
							</div>
						</div>	
						</c:when>
						
							<c:otherwise>
							<div class="row top5" id="row5h" style="display: none">
							<div class="spanfit1 left15 pull-left">
								<div class="top7 pull-left">
									9
								</div>
								<div class="pull-left" style="width: 200px; margin-left: 5px;">
									<input type="hidden" name="hid9" id="hid9" />
									<input type="text" name="opt9" id="opt9" class="form-control"
										maxlength="750" placeholder="">
								</div>
								<div class="pull-left top7"
									style="width: 40px; margin-left: 5px;">
									<input type="checkbox" name="valid9" id="valid9">
								</div>
								<div class="clearfix"></div>
							</div>

							<div class="spanfit1  pull-left">
								<div class="top7 pull-left">
									10
								</div>
								<div class="pull-left" style="width: 200px; margin-left: 5px;">
									<input type="hidden" name="hid6" id="hid6" />
									<input type="text" name="opt10" id="opt10" class="form-control"
										maxlength="750" placeholder="">
								</div>
								<div class="pull-left top7" style="width: 40px;">
									&nbsp;&nbsp;
									<input type="checkbox" name="valid10" id="valid10">
								</div>
								<div class="clearfix"></div>
							</div>
						</div>	
							
							
							</c:otherwise>
						</c:choose>
					</div>



					<div class="row">
						<div class="col-sm-7 col-md-7" id="assInstruction">
							<label>
								<strong><spring:message code="lblInst"/></strong>
							</label>
							<textarea class="form-control" name="questionInstructions"
								id="questionInstructions" maxlength="2500" rows="4"
								onkeyup="return chkLenOfTextArea(this,2500);"
								onblur="return chkLenOfTextArea(this,2500);"></textarea>
						</div>
					</div>

					<div class="row top20" style="display: block;" id="divManage">
						<div class="col-sm-7 col-md-7">
							<c:if test="${fn:indexOf(roleAccess,'|8|')!=-1}">
							</c:if>
							<button id="saveQues"
								onclick="saveQuestionFromQuestionSet('save')"
								class="btn btn-large btn-primary">
								<strong><spring:message code="btnSave"/> <i class="icon"></i>
								</strong>
							</button>
							<c:if test="${fn:indexOf(roleAccess,'|8|')!=-1}">
							</c:if>
							<!--<button onclick="saveQuestionFromQuestionSet('continue')" class="btn btn-large btn-primary"><strong>Save & Continue <i class="icon"></i></strong></button>
					     	&nbsp;-->
							<a href="javascript:void(0);" onclick="clearQues()"><spring:message code="btnClr"/></a>
						</div>
					</div> </span>
			</div>
		</div>
	</div>
	<!--<div class="row mt10" id="divManage">
		  	<div class="col-sm-4 col-md-4">
		  			<button onclick="saveQuestion();" class="btn  btn-primary"><strong>Save <i class="icon"></i></strong></button>
		     	 &nbsp;<a href="javascript:void(0);" onclick="clearQues();">Cancel</a>  
		    </div>
		  </div>
		-->
</div>
<!-- Search  -->
<div class="row" style="margin: 0px;">
	<div style="float: left;">

	</div>
</div>
<div class="row col-sm-12 col-md-12">
	<div class="row">
		<div class="mt10">
			<div class="subheading" style="font-size: 13px;">
				<spring:message code="headAddFrmQuesPool"/>
			</div>
			<div class="col-sm-6 col-md-6">
				<label>
					<spring:message code="lblQues"/>
				</label>
				<input type="text" class="form-control fl" name="quesSearchText"
					id="quesSearchText">
			</div>
			<div class="col-sm-2 col-md-2" style="margin-top: 26px;">
				<button class="btn btn-primary " type="button"
					onclick="searchi4Ques()">
					<spring:message code="btnSearch"/>
					<i class="icon"></i>
				</button>
			</div>
		</div>
	</div>
</div>
<div class="TableContent top15">
	<div class="table-responsive" id="refChkQuesGrid"></div>
</div>
<!--<div class="row col-sm-12 col-md-12">
	<div class="mt10">
		<a href="referencechkquestionsSet.do">Cancel</a>
	</div>
</div>

-->
<script type="text/javascript">
	displayQues();
	displayQuesSetQues();
</script>
<!-- @Author: Hanzala 
 * @Discription: view of edit domain Page.
 -->