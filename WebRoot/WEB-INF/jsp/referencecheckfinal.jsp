<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script src="calender/js/jscal2.js"></script>
<script src="calender/js/lang/en.js"></script>
<script type="text/javascript" src="js/referencecheck.js?ver=${resouceMap['js/referencecheck.js']}"></script>
<link rel="stylesheet" type="text/css" href="calender/css/jscal2.css" />
<link rel="stylesheet" type="text/css" href="calender/css/border-radius.css" />
<link rel="stylesheet" type="text/css" href="calender/css/gold/gold.css" />
<link rel="stylesheet" href="css/dialogbox.css" />

	<div class="row" style="margin:0px;">
	       <div style="float: left;">
	       	<img src="images/manageusers.png" width="41" height="41" alt="">
	       </div>        
			<div style="float: left;">
				<div class="subheading" style="font-size: 13px;">e-Reference Submit</div>	
			</div>
		<div style="clear: both;"></div>	
	   	<div class="centerline"></div>
	</div>

	<div>
		<div>

		<c:choose>
			<c:when test="${contactStatus eq 1}">
				<h3 id="myModalLabel"><spring:message code="headAlreadySubAns"/></h3>
			</c:when>
			<c:otherwise>			
				<h3 id="myModalLabel"><spring:message code="headE-RefSubSucc"/></h3>
			</c:otherwise>
		</c:choose>
				
		</div>
		
	</div>
	

