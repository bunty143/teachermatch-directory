<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type="text/javascript" src="dwr/interface/PortfolioReminderAjax.js?ver=${resourceMap['PortfolioReminderAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/DistrictAjax.js?ver=${resourceMap['DistrictAjax.ajax']}"></script>
<script type='text/javascript' src="js/portfolioreminder.js?ver=${resourceMap['js/portfolioreminder.js']}"></script>
<script type="text/javascript" src="dwr/interface/BatchJobOrdersAjax.js?ver=${resourceMap['BatchJobOrdersAjax.ajax']}"></script>

<style>
.table-striped tbody tr:nth-child(odd) td, .table-striped tbody tr:nth-child(odd) th {
background-color: #F2FAEF;
}
.table th, .table td {
 padding:10px 4px;
}
input[type="radio"], input[type="checkbox"] {
 margin: 0 0 0;
line-height: normal;
}
</style>
<div id="loadingDiv"> <!-- style="display:none; z-index: 5000;"  -->
  	<table  align="left" >
 		<tr><td style="padding-top:270px;padding-left:450px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;padding-left:450px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
 		<tr id='paymentMessage'><td style='padding-top:0px;padding-left:450px;' id='spnMpro' align='center'></td></tr>
	</table>
</div>
<c:if test="${userMaster.entityType==1}">
<c:set var="notShow" value="display:none;"/>
	
	<div class="row  col-sm-8 col-md-8 modalTM1" id='searchItem' style="position:fixed; margin-top: 15px;padding-top: 15px;padding-bottom:15px;margin-left:15px;">	
     <div class='divErrorMsg' id='errordiv' style="padding-left: 15px;"></div>
        <div class="col-sm-10 col-md-10">
        <label><spring:message code="lblDistrictName"/></label>
           <span>
           <input type="text" id="districtName" autocomplete="off" maxlength="100"  name="districtName" class="help-inline form-control"
          		 onfocus="getDistrictAuto(this, event, 'divTxtShowData', 'districtName','districtId','');"
				 onkeyup="getDistrictAuto(this,event,'divTxtShowData', 'districtName','districtId','');"
				 onblur="hideDistrictMasterDiv(this,'districtId','divTxtShowData');" tabindex="1"/>
      	   </span>
      		<input type="hidden" id="districtId" value="${districtId}"/>
      		<div id='divTxtShowData'  onmouseover="mouseOverChk('divTxtShowData','districtName')" style=' display:none;position:absolute;z-index:5000;' class='result' ></div>	
	   </div>
 
	    <div class="col-sm-1 col-md-1" >
	        <label  style="float: right;margin-right:-25px;" id='closePan'>&nbsp;</label>
			<button class="btn btn-primary top25-sm left25-sm" style="width:90px;" type="button" onclick="searchData();"><spring:message code="lnkSearch"/>&nbsp;<i class="icon"></i></button>         	   	
	    </div>
	   </div>

 
   <div class="modalsa" style="display: none;" id="sa">
	<div class="" style="padding-top: 5px;">
	<a href='javascript:void(0);' onclick="getSearchPan()"><span class='icon-search icon-large iconcolor' style="font-size: 0.99em;"></span><b>&nbsp;<spring:message code="lnkSearchAg"/><img src="images/arrow_left_animated.gif"/></b></a>
	</div>
	</div>
	
</c:if>	

<div class="row" style="margin:0px;">
<c:if test="${userMaster.entityType eq 1}">
		<div  id="managedqCLine" class="hide">
	    <div style="float: left;">
         	<img src="images/icon1.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;" id="managedqText"><spring:message code="lblAppMang"/></div>	
         </div>			
		
		<div style="clear: both;"></div>	
	    <div class="centerline" style="width:971px;"></div>
	</div>
</c:if>
<c:if test="${userMaster.entityType ne 1}">
		<div  id="managedqCLine">
	    <div style="float: left;">
         	<img src="images/icon1.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;" id="managedqText"><spring:message code="lblAppMang"/></div>	
         </div>			

		<div style="clear: both;"></div>	
	    <div class="centerline" style="width:971px;"></div>
	</div>
</c:if>

</div>
<input type="hidden" value="${userMaster.entityType}"/>
<c:set var="notShow" />



<div class=" row">
<div class="span16" style="padding-top: 5px;" >
<c:if test="${(userMaster.entityType gt 1) || districtId gt 0}">
<input type="hidden" id="districtId" value="${districtId}"/>
<script type="text/javascript">
searchData();
</script>	
</c:if>

<c:if test="${userMaster.entityType == 1}">
	
	<c:if test="${dt ne null}">
		<table  border="0" class="table table-bordered table-striped" id="tblGrid" style="margin-left: 15px;width: 100%;"></table>
	</c:if>	
	<c:if test="${dt eq null}">
		<table  border="0" class="table table-bordered table-striped hide" id="tblGrid" style="margin-left: 15px;width: 100%;"></table>
	</c:if>
		<div class="mt30 hide" id="saveButton" style='padding-left: 15px;'>

        	<button type="submit" class="btn btn-large btn-primary" onclick="savePortfolioReminder()"><strong><spring:message code="lblSaveRem"/> <i class="icon"></i></strong></button>
          	&nbsp;&nbsp;<a href="javascript:void(0);" onclick="cancelDistrict();"><spring:message code="lnkCancel"/></a><br><br>
       </div>	
</c:if>

<c:if test="${userMaster.entityType ne 1}">
	<div class="span2" id="qdetails" style="margin-left:18px;margin-top: 10px; padding-bottom: 10px;"><a href='javascript:void(0)'><B><spring:message code="msgPlzApplicantMag"/></B></a></div>
		<table  border="0" class="table table-bordered table-striped" style="margin-left: 15px;width: 100%;" id="tblGrid">
		</table>
		
		<div class="mt30" id="saveButton1" style='padding-left: 15px;'>

	        <button type="submit" class="btn btn-large btn-primary" onclick="savePortfolioReminder()"><strong><spring:message code="lblSavePortfolio"/> <i class="icon"></i></strong></button>
	        &nbsp;&nbsp;<a href="javascript:void(0);" onclick="cancelDistrict();"><spring:message code="lnkCancel"/></a><br>

	        <!--<button type="submit" class="btn btn-large btn-primary" onclick="savePortfolioReminder()"><strong> <spring:message code="lblSavePortfolio"/> <i class="icon"></i></strong></button>
	        &nbsp;&nbsp;<a href="javascript:void(0);" onclick="cancelDistrict();"></a><br>

          --><br>
       </div>
</c:if>

</div>

</div>


<div  class="modal hide"  id="myModalUpdateMsg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"   style="z-index: 5000;"  data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
	  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
			<h3 id="myModalLabel"><spring:message code="lblMangRem"/></h3>
		</div>
		<div class="modal-body">		
			<div class="control-group">
				<div class="">
			    	<span id="updateMsg"></span>	        	
				</div>
			</div>
	 	</div>
	 	<div class="modal-footer">
	 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClose"/></button> 		
 		</div>
	</div>
</div>
</div>
<!--<script type="text/javascript">

var selectIds = $('#collapseOne,#collapseTwo,#collapseThree');
$(function ($) {
    selectIds.on('show.bs.collapse hidden.bs.collapse', function () {alert("hiiiii");
        $(this).prev().find('.accordion-toggle').toggleClass('plus minus');
    })
});


$('.accordion').collapse();
$('.accordion').on('show', function (e) {
alert("hiiiifsdfsdi");
$(e.target).prev('.accordion-heading').find('.accordion-toggle').addClass('minus').removeClass('plus');
});

$('.accordion').on('hide', function (e) {
alert("hiiiisdterterti");
$(this).find('.accordion-toggle').not($(e.target)).removeClass('minus').addClass('plus');
});

</script>
--><script type="text/javascript">
$('#districtName').focus();
$(document).ready(function(){
  		$('#eRefInstructions').find(".jqte_editor").height(225);
}) 
</script>
