<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<script type="text/javascript" src="dwr/interface/OnlineActivityAjax.js?ver=${resourceMap['OnlineActivityAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type="text/javascript" src="js/teacher/onlineactivity.js?ver=${resourceMap['js/teacher/onlineactivity.js']}"></script>

<script type="text/javascript" language="javascript">

</script>  


<!-- End ... Dynamic Portfolio -->

<link rel="stylesheet" type="text/css" href="css/base.css" />  

	<div class="row" style="margin-left: 0px;margin-right: 0px;margin-top:">
	     <div style="float: left;">
	     	<img src="images/applyfor-job.png" width="41" height="41" alt="">
	     </div>        
	     <div style="float: left;">
	     	<div class="subheading" style="font-size: 13px;"><spring:message code="msgonlineActivity"/></div>	
	     </div>	
	      <div class="pull-right add-employment1">
			<div>
			<div><b>&nbsp;</b></div>
			<div id="questionTimer" style="display: none;text-align: right;"><span class="clock" ><span class="min">0</span>:<span class="sec">0</span> <spring:message code="lblMinutes"/></span></div>
			</div>
		 </div>		
		<div style="clear: both;"></div>	
		<div class="centerline"></div>
	</div>
	<div class="row">
		<div class="col-sm-6 col-md-6">
			 <div class='divErrorMsg' id='errordivnlineActivityquestion' style="display: block;"></div>
		</div>
	</div>
	 <div class="TableContent top15">        	
	     <div class="table-responsive" id="divOnlineActivityQuestionList"></div>            
	</div> 
	<input type="hidden" name="teacherId" id="teacherId"  value="${teacherId}"/>
	<input type="hidden" name="jobCategoryId" id="jobCategoryId"  value="${jobCategoryId}"/>
	<input type="hidden" name="districtId" id="districtId"  value="${districtId}"/>
	<input type="hidden" name="questionSetId" id="questionSetId"  value="${questionSetId}"/>
	<br/>
	<div class="row">
	    <div class ="col-sm-6 col-md-6"  id="btnDiv" style="display:none;"> 
			<button class="btn btn-large btn-primary" type="button" onclick="saveQuestion(1);"><strong><spring:message code="btnSub"/> <i class="icon"></i></strong></button><br>							
		</div>	
	</div>	
	<div style="display:none;" id="loadingDiv">
		<table  align="center">
				<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
				<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
				<tr id='paymentMessage'><td style='padding-top:0px;padding-left:450px;' id='spnMpro' align='center'></td></tr>
		</table>
	</div>

<script type="text/javascript"><!--

String.prototype.toHHMMSS = function () {
    sec_numb    = parseInt(this);
    var hours   = Math.floor(sec_numb / 3600);
    var minutes = Math.floor((sec_numb - (hours * 3600)) / 60);
    var seconds = sec_numb - (hours * 3600) - (minutes * 60);
    var time = "";
    var hrs="";
    var mins="";
    
   if(hours>1)
   	hrs="s";
   if(minutes>1)
    mins="s";
    
    if(hours==0)
    	time    = minutes+' Min'+mins+' '+seconds+" Secs";
    if(hours==0 && minutes==0)
    	time    = seconds+" Secs.";
    else if(hours>0 && minutes==0)
        time    = hours+' Hour'+hrs+' '+seconds+" Secs";
    else if(hours>0)
        time    = hours+' Hour'+hrs+' '+minutes+' Min'+mins+' '+seconds+" Secs";
        
    return time;
}

getOnlineActivityQuestions(null);
</script>

