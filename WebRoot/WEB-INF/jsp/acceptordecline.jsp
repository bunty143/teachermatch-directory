<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<script type="text/javascript" src="dwr/interface/AssessmentCampaignAjax.js?ver=${resourceMap['AssessmentCampaignAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type="text/javascript" src='dwr/util.js'></script>
<script type="text/javascript" src="js/teacher/acceptordecline.js?ver=${resourceMap['js/teacher/acceptordecline.js']}"></script>
<div class="row offset4 msgPage">
	<div class="span8 mt30">
		${msgpage}
	</div>
</div>
<div id="loadingDiv"> <!-- style="display:none; z-index: 5000;"  -->
  	<table  align="left" >
 		<tr><td style="padding-top:270px;padding-left:450px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;padding-left:450px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
 		<tr id='paymentMessage'><td style='padding-top:0px;padding-left:450px;' id='spnMpro' align='center'></td></tr>
	</table>
</div>
<input type="hidden" name="offerUrl" value="${offerUrl}"/>
<input type="hidden" name="loginFlag" value="${loginFlag}"/>
<input type="hidden" name="emailAddress" value="${emailAddress}"/>
<input type="hidden" name="teacherId" value="${teacherId}"/>
<input type="hidden" name="jobId" value="${jobId}"/>
<input type="hidden" name="acceptordecline" value="${acceptordecline}"/>

<input type="hidden" name="contxtPath" value="<%=request.getContextPath()%>"/>
<div  class="modal hide"  id="myModalVerify" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog-for-cgmessage">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="window.location.href='logout.do'"><spring:message code="btnX"/></button>
		<h3 id="myModalLabel"><spring:message code="headTm"/></h3>
	</div>
	<div class="modal-body">
		<div class="control-group" id='messageVerify'>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn btn-primary" data-dismiss="modal" aria-hidden="true" onclick='verify()'><spring:message code="lblYes"/></button>&nbsp;
 		<button class="btn btn-large" data-dismiss="modal" aria-hidden="true" onclick="window.location.href='logout.do'"><spring:message code="lblNo"/></button>
 	</div>
</div>
</div>
</div>

<script type="text/javascript">
	<c:if test="${ verifyFlag=='1'}">
		$('#messageVerify').html('${msg}');
		$('#myModalVerify').modal('show');
	</c:if>
	
</script>

