<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div class="modal hide"  id="cancelPortfolioToDashBoard"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
		  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div id="deact" class="modal-body"> 		
				<div class="control-group">
					<spring:message code="jafportfoflioCancelMsg" />
				</div>
		 	</div>
	 	 <div class="modal-footer">
	 	     <span id=""><button class="btn  btn-primary" onclick="cancelApplyJobL2()" ><spring:message code="btnOk" /> <i class="icon"></i></button></span>&nbsp;&nbsp;
	 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="lnkCancel" /></button> 		
	 	</div>
	  </div>
	</div>
</div>


<div style="display: none;" id="loadingDiv">
	<table align="center">
		<tr>
			<td style="padding-top: 270px;" align="center">
				<!-- <img src="images/please.jpg"/> -->
			</td>
		</tr>
		<tr>
			<td style="padding-top: 0px;" align="center">
				<img src="images/loadingAnimation.gif" />
			</td>
		</tr>
		<tr id='paymentMessage'>
			<td style='padding-top: 0px; padding-left: 450px;' id='spnMpro'
				align='center'></td>
		</tr>
	</table>
</div>


<div style="display: none;" id="jafLoadingDiv">
	<table align="center">
		<tr>
			<td style="padding-top: 270px;" align="center">
				<!-- <img src="images/please.jpg"/> -->
			</td>
		</tr>
		<tr>
			<td style="padding-top: 0px;" align="center">
				<img src="images/loadingAnimation.gif" />
			</td>
		</tr>
	</table>
</div>

<div style="display: none;" id="jafLoadingDivL1">
	<table align="center">
		<tr>
			<td style="padding-top: 270px;" align="center">
				<!-- <img src="images/please.jpg"/> -->
			</td>
		</tr>
		<tr>
			<td style="padding-top: 0px;" align="center">
				<img src="images/loadingAnimation.gif" />
			</td>
		</tr>
	</table>
</div>

<div style="display: none;" id="jafLoadingDivL2">
	<table align="center">
		<tr>
			<td style="padding-top: 270px;" align="center">
				<!-- <img src="images/please.jpg"/> -->
			</td>
		</tr>
		<tr>
			<td style="padding-top: 0px;" align="center">
				<img src="images/loadingAnimation.gif" />
			</td>
		</tr>
	</table>
</div>

<div style="display: none;" id="jafLoadingDivL3">
	<table align="center">
		<tr>
			<td style="padding-top: 270px;" align="center">
				<!-- <img src="images/please.jpg"/> -->
			</td>
		</tr>
		<tr>
			<td style="padding-top: 0px;" align="center">
				<img src="images/loadingAnimation.gif" />
			</td>
		</tr>
	</table>
</div>

<div style="display: none;" id="jafLoadingDivL4">
	<table align="center">
		<tr>
			<td style="padding-top: 270px;" align="center">
				<!-- <img src="images/please.jpg"/> -->
			</td>
		</tr>
		<tr>
			<td style="padding-top: 0px;" align="center">
				<img src="images/loadingAnimation.gif" />
			</td>
		</tr>
	</table>
</div>

<div style="display: none;" id="jafLoadingDivL5">
	<table align="center">
		<tr>
			<td style="padding-top: 270px;" align="center">
				<!-- <img src="images/please.jpg"/> -->
			</td>
		</tr>
		<tr>
			<td style="padding-top: 0px;" align="center">
				<img src="images/loadingAnimation.gif" />
			</td>
		</tr>
	</table>
</div>

<div class="modal hide"  id="globalListenerMsg"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
		  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div id="deact" class="modal-body"> 		
				<div class="control-group">
					<spring:message code="jafportfoflioListenerMsg" />
				</div>
		 	</div>
	 	 <div class="modal-footer">
	 	     <span id=""><button class="btn  btn-primary" onclick="callSaveByCurrentPage(1)"><spring:message code="btnSaveDSPQ" /> <i class="icon"></i></button></span>&nbsp;&nbsp;
	 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="callSaveByCurrentPage(0)"><spring:message code="lnkCancel" /></button> 		
	 	</div>
	  </div>
	</div>
</div>


<div class="modal hide"  id="unDoneMsgModelId"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false" onclick="jafUnDoneRedirection()">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
		  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div id="deact" class="modal-body"> 		
				<div class="control-group">
				<p id="jafportfofliounDoneMsgId" class="DSPQRequired12"></p>
				</div>
		 	</div>
	 	 <div class="modal-footer">
	 	     <span><button class="btn  btn-primary" onclick="jafUnDoneRedirection()" ><spring:message code="btnOk" /> <i class="icon"></i></button></span>
	 	</div>
	  </div>
	</div>
</div>
<input type="hidden" id="sFirstUnDoneURL" name="sFirstUnDoneURL" value=""/>
<input type="hidden" id="sNextClickURL" name="sNextClickURL" value=""/>
<input type="hidden" id="dspqGlobalLster" name="dspqGlobalLster" value="0"/>

<script>
	document.addEventListener("click", globalLsterSetter);
	document.addEventListener("mouseout", globalLsterSetter);
</script>

<iframe src="" id="ifrmCLDownload" width="100%" height="480px" style="display: none;"></iframe>
