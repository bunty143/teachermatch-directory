<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<script type="text/javascript" src="dwr/interface/DashboardAjax.js?ver=${resouceMap['DashboardAjax.ajax']}"></script>
<link href="css/quest.css?ver=${resourceMap['css/quest.css']}" rel="stylesheet" type="text/css">	

<script type="text/javascript" src="dwr/interface/DSPQServiceAjax.js?ver=${resourceMap['DSPQServiceAjax.ajax']}"></script>
<script type="text/javascript" src="js/jobapplicationflow/jafstart.js?ver=${resouceMap['js/jobapplicationflow/jafstart.js']}"></script>		
		
<div class="container">
<center style="margin-left: 30px;margin-right: 30px;margin-top:15px;">
<div  style="color:black;font-size: 20px;">
<spring:message code="msgpowerUpEJS"/>
</div>
<div style="height:5px;"></div>
<div class="" style="margin-top: 10px;font-size: 12px;">
Whether you’re seeking a teaching, administrative, or support staff position <BR>
in K-12 education, Quest is the site to help you get the job you want. <BR><BR>
From your secure <B> OurQuest Dashboard</B> you can plan, track, and<BR>
manage all aspects of your job quest — including:</div>

<div class="row" style="margin: auto;margin-top:15px;">
	<img src="images/JobSeekDashIconRevised.png" style="width:700px;height:220px;" />
</div>
</div>

<div align="center" style="height: 160px; background-color: #007AB3; margin-top: 10px;">
<br/>
<div style="color: #FFFFFF; margin:15px 0px 10px 0px;font-size: 20px;"><spring:message code="msgBeginMakingCon"/></div>
<BR/>
<a href="candidate-sign-up.do" target="_blank"><button class="signInButtonWhite top5" type="button" style="font-size: 12px; background-color:FFFFFF;margin: auto;color:#474747;width:20%;height: 24%;"><spring:message code="lablSignUpNow"/></button></a><br/>

</div>


<div class="container">

<div>
</div>
<div align="center" class="col-sm-12 col-md-12" style="margin: auto;font-size: 12px;margin-top: 10px;">
Quest will help you create a <b>Power Profile</b> — designed to make you stand out<BR>
and rise to the top among all applicants for any position you choose to pursue
</div>
<div>
<br/>
<center>
<div class="col-sm-10 col-md-10"  style="margin: auto;margin-left: 21%;">
<div class="col-sm-2 col-md-2">
<a href="powerprofileindex.do" target="_blank"><img src="images/ppi4.png" style="width:105px;margin-top: 5px;"/></a>
</div>
<div class="col-sm-7 col-md-7" style="text-align: justify;margin-left: -5px;margin-top: 0px;" >
<div style="font-size: 12px;">
You will have your own <b>Profile PowerTracker</b> to gauge your<br/>
profile strength. When you complete the <c:choose><c:when test='${epistatus eq "comp" or epistatus eq null}'><a href="Quest-EPI.do"><b>TeacherMatch EPI</b></a></c:when><c:otherwise><a href="javascript:void(0);" onclick="checkInventory(0,null);"><b>TeacherMatch EPI</b></a></c:otherwise></c:choose><br/>
you get a big boost toward becoming an <b>Expert Job Seeker</b>,<br/>
where you can unlock your pick of tools, resources, advice,<br/>
and activities . . . not available at other education job sites.<br/>
<a href="meetyourmentor.do" target="_blank"><b>See for Yourself!</b></a>
</div>
</div>
</div>
</center>
</div>
</center>
</div>
	<script type="text/javascript" src="dwr/interface/AssessmentCampaignAjax.js?ver=${resouceMap['AssessmentCampaignAjax.ajax']}"></script>
<script type='text/javascript' src="js/assessment-campaign.js?ver=${resouceMap['js/assessment-campaign.js']}"></script>
<jsp:include page="../tiles/feedbackandsupport3AfterLogin.jsp" />
