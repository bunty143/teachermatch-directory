
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<script type="text/javascript" src="dwr/interface/JobUploadTempAjax.js?ver=${resourceMap['JobUploadTempAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type="text/javascript" src="js/jobupload.js?ver=${resourceMap['js/jobupload.js']}"></script>
<c:if test="${userMaster.headQuarterMaster != null}" >
	<script type="text/javascript" src="dwr/interface/AutoSearchFilterAjax.js?ver=${resourceMap['AutoSearchFilterAjax.ajax']}"></script>
	<script type='text/javascript' src="js/branchautoSuggest.js?ver=${resourceMap['js/branchautoSuggest.js']}"></script>
</c:if>
	<div class="row" style="margin-left: 0px;margin-right: 0px;margin-top:">
         <div style="float: left;">
         	<img src="images/icon1.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;"><spring:message code="lblImportJobDetails"/></div>	
         </div>         		
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
     </div>
<div class="row top20">
	<div class="col-sm-3 col-md-3" style="max-width: 190px;"></div>
	<div class="col-sm-7 col-md-7 importborder" style="margin-left: 15px;margin-right: 15px;">
	<iframe id='uploadFrameID' name='uploadFrame' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'>
		</iframe>
		<form id='jobUploadServlet' enctype='multipart/form-data' method='post' target='uploadFrame' action='jobUploadServlet.do' class="form-inline" onsubmit="return validateJobFile();">
		<table cellpadding="0" cellspacing="0" align="center" >
		<tr><td  style="padding-top:10px;" colspan="2">&nbsp;
			 <div class="control-group">			                         
			 	<div class='divErrorMsg' id='errordiv' ></div>
			</div>
			</td>
		</tr>
		<!-- shadab -->
		<c:if test="${userMaster.headQuarterMaster != null}" >
		<tr>
		<input type="hidden" name="headQuarterId" id="headQuarterId" value='${userMaster.headQuarterMaster.headQuarterId }' />
			<td colspan="2"><label><spring:message code="lblBranchName"/><span class="required">*</span></label></td></tr>
		<tr>
		<td colspan="2">
			 <span>
			      <input type="text" id="branchName" name="branchName"  class="form-control" value="${branchName}"
       					onfocus="getBranchMasterAutoComp(this, event, 'divTxtShowDataBranch', 'branchName','branchId','');"
						onkeyup="getBranchMasterAutoComp(this, event, 'divTxtShowDataBranch', 'branchName','branchId','');"
						onblur="hideBranchMasterDiv(this,'branchId','divTxtShowDataBranch');"
						autocomplete="off"
						onchange="resetDistrict();"
							${editFlag}/>
						<input  type="hidden" id="branchId" name="branchId" value="${bnchId}">
						<div id='divTxtShowDataBranch' style=' display:none;position:absolute;z-index:5000;' onmouseover="mouseOverChk('divTxtShowDataBranch','branchName')" class='result' ></div>								      
			      </span>		
				
		</td>
		</tr>
		</c:if>
		<!-- shadab -->
		<tr>
			<td colspan="2"><label><spring:message code="lblDistrictName"/><span class="required">*</span></label></td></tr>
		<tr><td colspan="2">
				<input type="text" id="districtName" name="districtName" class="help-inline form-control"
				    		 onfocus="getDistrictAuto(this, event, 'divTxtShowData', 'districtName','districtId','');"
				onkeyup="getDistrictAuto(this, event, 'divTxtShowData', 'districtName','districtId','');"
				onblur="hideDistrictMasterDiv(this,'districtId','divTxtShowData');"	autocomplete="off"/>
				<input type="hidden" id="districtId" value="0"/>
				<div id='divTxtShowData'  onmouseover="mouseOverChk('divTxtShowData','districtName')" style=' display:none;position:absolute;z-index:5000;' class='result' ></div>	
			</td>
		</tr>	
		<tr><td style="padding-top:10px;"><label><spring:message code="LBLJobDetails1"/><span class="required">*</span></label></td><td style="padding-top:10px;"><input name="jobfile" id="jobfile" type="file"></td></tr>	
		<tr><td style="padding-top:10px;"><button class="btn btn-primary fl" type="button" onclick="return validateJobFile();"><spring:message code="btnImport"/> <i class="icon"></i></button></td></tr>	
		</table>
		</form>
		</div>
	</div>
	<div class="row">	
	<div class="col-sm-3 col-md-3" style="max-width: 190px;"></div>
	<div class="col-sm-8 col-md-8 mt10" style="margin-right: 5px;">
			<table>
			<tr><td></td><td class="required"><spring:message code="headN"/></td></tr>
			<tr><td valign="top">*</td><td> <spring:message code="msgExelFi&SubsequntRData"/></td></tr>
			<tr><td valign="top">*</td><td><spring:message code="msgExcelFColN"/> -</td></tr>
			<tr><td></td><td><spring:message code="lblJoId"/></td></tr>
			<tr><td></td><td><spring:message code="tooljob_type"/></td></tr>
			
			<tr><td></td><td><spring:message code="lblname"/></td></tr>
			
			<tr><td></td><td><spring:message code="lbljob_start_date"/></td></tr>
			<tr><td></td><td><spring:message code="lbljob_end_date"/></td></tr>
			<c:if test="${userMaster.headQuarterMaster == null}" >
				<tr><td></td><td><spring:message code="lblno_exp_hires"/></td></tr>
			</c:if>
			
			<tr><td valign="top">*</td><td> <spring:message code="importUserMsg"/></td></tr>
			<c:if test="${userMaster.headQuarterMaster == null}" >
				<tr><td valign="top">*</td><td> <spring:message code="importUserMsg2"/> </td></tr>
				<tr><td valign="top">*</td><td> <spring:message code="importUserMsg3"/> </td></tr>
				<tr><td valign="top">*</td><td> <spring:message code="importUserMsg3a"/> </td></tr>
				<tr><td valign="top">*</td><td> <spring:message code="importUserMsg3b"/> </td></tr>
			</c:if>
			<tr><td valign="top">*</td><td><spring:message code="msgColNaOdr"/></td></tr>
			<tr><td valign="top">*</td><td><spring:message code="msgColNaCaseInSent"/></td></tr>
		</table>
	</div>
	</div>

<div style="display:none;" id="loadingDiv">
	<table  align="center" >
		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
	</table>
</div> 
