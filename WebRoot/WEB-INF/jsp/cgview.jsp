<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>

<script type="text/javascript" src="js/report/candidategridreport.js?ver=${resourceMap['js/report/candidategridreport.js']}"></script>
<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<script type="text/javascript" src="dwr/interface/CandidateReportAjax.js?ver=${resourceMap['CandidateGridAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/PFAcademics.js?ver=${resourceMap['PFAcademics.ajax']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type='text/javascript' src="js/report/cgview.js?ver=${resourceMap['js/report/cgview.js']}"></script>
<script type='text/javascript' src="js/report/cgTranscriptOrCertifications.js?ver=${resourceMap['js/report/cgTranscriptOrCertifications.js']}"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" />
 
<script type="text/javascript">
var $j=jQuery.noConflict();
        $j(document).ready(function() {
            
        });
        
function applyScrollOnTbl()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 250,
        width: 900,
        minWidth: null,
        minWidthAuto: false,
        colratio:[150,150,150,150,150,150], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}
applyScrollOnTbl();

function applyScrollOnTranscript()
{
		var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblTrans').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 350,
        width: 875,
        minWidth: null,
        minWidthAuto: false,
        colratio:[150,150,150,150,150,125], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });            
        });
}
function applyScrollOnCertification()
{
		var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblCert').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 350,
        width: 875,
        minWidth: null,
        minWidthAuto: false,
        colratio:[300,300,275], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });            
        });
}
</script>

<div id="tm-root"></div>

<div  class="modalTrans hide"  id="modalDownloadsTranscript" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 5000;"  data-backdrop="static">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="setZIndexTrans()">x</button>
		<h3 id="myModalLabel"><spring:message code="HeadTran" /></h3>
	</div>
	<div class="modal-body">		
		<div class="control-group">
			<div id="" class="">
<iframe src="" id="ifrmTrans" width="100%" height="480px">
 </iframe>        	
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="setZIndexTrans()"><spring:message code="btnClose" /></button> 		
 	</div>
</div>

<div  class="modal hide"  id="myModalTranscript" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel"><spring:message code="HeadTran" /></h3>
	</div>
	<div class="modal-body">		
		<div class="control-group">
			<div id="divTranscript" class="">
		    		        	
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClose" /></button> 		
 	</div>
</div>

<div  class="modal hide"  id="myModalCertification" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel"><spring:message code="lnkCerti/Lice" /></h3>
	</div>
	<div class="modal-body">		
		<div class="control-group">
			<div id="divCertification" class="">
		    		        	
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClose" /></button> 		
 	</div>
</div>

<div  class="modal1 hide"  id="myModalPhone" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel"><spring:message code="headPhone" /></h3>
	</div>
	<div class="modal-body">		
		<div class="control-group">
			<div id="divPhone" class="">
		    		        	
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClose" /></button> 		
 	</div>
</div>



<div class="modal1 hide"  id="divAlert" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" style="z-index: 5000;" >
	<!--<div class="modal-header dragHeader ui-dialog-titlebar ui-widget-header  " >
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick="setZIndexActDiv()">x</button>
		<h3 id="myModalLabel" >TeacherMatch</h3>
	</div>
	-->
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" id='vcloseBtnk'>x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div class="modal-body">				
		<div class="control-group">
			<div class="" id="divAlertText" >
			</div>
		</div>
 	</div> 	
 	<div class="modal-footer"> 		
 		<span><button class="btn" data-dismiss="modal" aria-hidden="true" onclick="setZIndexActDiv()"><spring:message code="btnClose" /></button></span> 		
 	</div>
</div>


<div class="modal1 hide"  id="myConfirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" >
	<!--<div class="modal-header dragHeader ui-dialog-titlebar ui-widget-header  " >
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick="setZIndexActDiv()">x</button>
		<h3 id="myModalLabel" >TeacherMatch</h3>
	</div>
	-->
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" id='vcloseBtnk'>x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div class="modal-body">				
		<div class="control-group">
			<div class="" id="ddd"><spring:message code="headRemoveCandFromCG" />
			</div>
		</div>
 	</div> 	
 	<div class="modal-footer">
 		<span id="spnRemoveTeacher"><button class="btn btn-large btn-primary" ><spring:message code="btnOk" /> <i class="icon"></i></button></span>
 		<span><button class="btn" data-dismiss="modal" aria-hidden="true"  onclick="setZIndexActDiv()"><spring:message code="btnClose" /></button></span> 		
 	</div>
</div>

<div class="modal1 hide"  id="myModalAct" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" >
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel"><spring:message code="headTakeAct" /></h3>
	</div>
	<div class="modal-body">
		<div class='divErrorMsg' id='errordivAct' style="display: block;"></div>		
		<div class="control-group">
			<div class="" id="divAct">	
			</div>
		</div>
 	</div> 	
 	<div class="modal-footer">
 		<button class="btn btn-large btn-primary"  onclick="chkActAction()"><spring:message code="btnSave" /> <i class="icon"></i></button>
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClose" /></button> 		
 	</div>
</div>

<div  class="modal hide"  id="myModalCoverLetter" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel"><spring:message code="headCoverLetr" /></h3>
	</div>
	<div class="modal-body">		
		<div class="control-group">
			<div class="">
		    	<span id="lblCL"></span>	        	
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClose" /></button> 		
 	</div>
</div>

<div class="modal hide"  id="myModalNotes" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static">
<input type="hidden" id="teacherId" name="teacherId" value="">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel"><spring:message code="headNot" />
</h3>
	</div>
	
	<div class="modal-body">	
		<div class="row" id="divNotes" style="padding-left: 15px; ">						
		</div>
				
		<div class="row mt10">
			<div class='span10 divErrorMsg' id='errordivNotes' style="display: block;"></div>
			<div class="span10" >
		    	<label><strong><spring:message code="lblEtrNot" /><span class="required">*</span></strong></label>
		    	<div class="span10" id='divTxtNode' style="padding-left: 0px; margin-left: 0px; " >
		    		<textarea readonly id="txtNotes" name="txtNotes" class="span10" rows="4"   ></textarea>
		    	</div>        	
			</div>						
		</div>
 	</div>
 	 	
 	<div class="modal-footer">
 		<c:set var="chkSaveDisp" value="inline"/>
		<c:choose>
			<c:when test="${userMaster.entityType eq 1}">
				<c:set var="chkSaveDisp" value="none" />
			</c:when>
			<c:when test="${fn:indexOf(roleAccess,'|8|') ne -1}">
				<c:set var="chkSaveDisp" value="inline" />
			</c:when>
			<c:otherwise>			
				<c:set var="chkSaveDisp" value="none" />
			</c:otherwise>
		</c:choose>
 		<span id="spnBtnCancel" style="display: ${chkSaveDisp}"><a href="#"	onclick="return cancelNotes()"><spring:message code="btnClr" /></a></span>
 		<span id="spnBtnSave" style="display: ${chkSaveDisp}"><button class="btn btn-large btn-primary"  onclick="saveNotes()"><spring:message code="btnSave" /> <i class="icon"></i></button></span>
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClose" /></button> 		
 	</div>
</div>
<!--Add message Div by  Sekhar  -->
<div  class="modal1 hide"  id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div class="modal-body">
		<div class="control-group" id='message2show'>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnOk" /></button>
 	</div>
</div>
<div class="modal1 hide" style='width:510px;' id="myModalMessage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<input type="hidden" id="teacherDetailId" name="teacherDetailId">
	<input type="hidden" id="emailId" name="emailId">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel"><spring:message code="headPostMsgToCandidate" /></h3>
	</div>
	<div  class="modal-body">
		<div class="control-group">
			<div class='divErrorMsg' id='errordivMessage' style="display: block;"></div>
		</div>
		<div class="control-group">
    		<label><strong><spring:message code="lblTo" /><br/></strong><span id="emailDiv"></span>
   		</div>
		<div id='support'>
		<div class="control-group">
			<div class="">
		    	<label><strong><spring:message code="lblSub" /></strong><span class="required">*</span></label>
	        	<input id="messageSubject" name="messageSubject" type="text" class="span8" maxlength="100" />
			</div>
		</div>
	            
        <div class="control-group">
			<div class="" id="messageSend">
		    	<label><strong><spring:message code="lblMsg" /></strong><span class="required">*</span></label>
	        	<textarea rows="5" class="span8" cols="" id="msg" name="msg" maxlength="1000"></textarea>
	        	<!--<div style='background-color: #FFFFCC;' >-->
	        </div>
		</div>
 		</div>
 		<div id='lodingImage' style="display: block;text-align:center;padding-top:4px;"></div>
 		<!-- end support div -->
  	</div>
 	
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClr" /></button>
 		<button class="btn btn-primary" onclick="validateMessage()" ><spring:message code="btnSend" /></button>
 	</div>
</div>
<!--End message Div by  Sekhar  -->
<div class="row  mt10 mb">	
	<div class="span16 centerline ">
		<div class="span1 m0"><img src="images/applyfor-job.png" width="41" height="41" alt=""></div>
		<div class="span10 subheading"><spring:message code="headCANDGRID" /></div>
		<div class="span3 pull-right add-employment1">
		<%--
			<c:if test="${fn:indexOf(roleAccess,'|4|')!=-1}">
			<a href="javascript:void(0);" id='hrefId1' onclick="downloadCandidateGridReport();if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;">View PDF</a>
		</c:if>
		 --%>		
		</div>
	</div>
	
</div>


<input type="hidden" id="jobId" name="jobId" value="${param.jobId}"/>
<input type="hidden" id="noteId" name="noteId" value=""/>



<div class="row ">
	
	
	
	<div class="mt10 span16" id="divReportGrid" style="position: relative">
			
		
			
	</div>
	<br/><br/>
</div>


<div style="display:none; z-index: 5000;" id="loadingDiv" >
    <table  align="left" >
 		<tr><td style="padding-top:270px;padding-left:450px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;padding-left:450px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
 		<tr id='paymentMessage'><td style='padding-top:0px;padding-left:450px;' id='spnMpro' align='center'></td></tr>
	</table>
</div>

<div class="modal1 hide"  id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div class="modal-body">
		<div class="control-group" id='blockMessage'>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnOk" /></button>
 	</div>
</div>

<script type="text/javascript">
getCandidateGrid();
$('#myModal').modal('hide');
$('#myModalCoverLetter').modal('hide');
$('#tpViewPDF').tooltip();
$('#tpSearch').tooltip();
$('#tpLegend').tooltip();


$(document).ready(function(){
$('#divTxtNode').find(".jqte").width(870);
});

/*

$('.modal a[rel="tooltip"]')
  .tooltip({placement: 'right'})
  .data('tooltip')
  .tip()
  .css('z-index', 10000);

$("#myConfirm").draggable({
		handle: ".modal-header"
});

$("#divAlert").draggable({
		handle: ".modal-header"
}); 
*/
</script>