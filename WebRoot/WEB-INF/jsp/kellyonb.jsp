<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="tm.bean.hqbranchesmaster.BranchMaster"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<script type='text/javascript' src="js/report/candidategrid.js?ver=${resouceMap['js/report/candidategrid.js']}"></script>
<script type="text/javascript" src="js/kellyonb.js?ver=${resouceMap['js/kellyonb.js']}"></script>

<script type="text/javascript" src="dwr/interface/KellyONRAjax.js?ver=${resourceMap['KellyONRAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/ManageStatusAjax.js?ver=${resouceMap['ManageStatusAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/CandidateGridAjaxNew.js?ver=${resouceMap['CandidateGridReportAjaxNew.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/CandidateGridAjax.js?ver=${resouceMap['CandidateGridAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/BatchJobOrdersAjax.js?ver=${resouceMap['BatchJobOrdersAjax.ajax']}"></script>
<script type="text/javascript" src="js/onr_common.js?ver=${resouceMap['js/onr_common.js']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type="text/javascript" src='dwr/util.js'></script>
<link rel="stylesheet" type="text/css" href="css/onrdashboard.css?ver=${resouceMap['css/onrdashboard.css']}" />
<link rel="stylesheet" href="css/dialogbox.css?ver=${resouceMap['css/dialogbox.css']}" />
<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<script src="calender/js/jscal2.js"></script>
<script src="calender/js/lang/en.js"></script>
<link rel="stylesheet" type="text/css" href="calender/css/jscal2.css" />
<link rel="stylesheet" type="text/css" href="calender/css/border-radius.css" />
<link rel="stylesheet" type="text/css" href="calender/css/gold/gold.css" />
<script type='text/javascript' src="js/branchautoSuggest.js?ver=${resourceMap['js/branchautoSuggest.js']}"></script>
<script type="text/javascript" src="dwr/interface/AutoSearchFilterAjax.js?ver=${resourceMap['AutoSearchFilterAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/TeacherProfileViewInDivAjax.js?ver=${resouceMap['TeacherProfileViewInDivAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/CGServiceAjax.js?ver=${resouceMap['CGServiceAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/TeacherInfotAjax.js?ver=${resouceMap['TeacherInfoAjax.ajax']}"></script>
<style>
textarea {
	width: 420px;
	height: 100px;
}
.table-bordered {
	border-collapse: separate;
	border-color: #cccccc;
}
.table-bordered td,th {
	border-left: 0px solid #dddddd;
}
.tdscore0 {
	width: 1px;
	height: 40px;
	float: left;
}
.tddetails0 {
	width: 1px;
	height: 40px;
	float: left;
}
tr.bgimage {
	background-image: url(images/color-gradients.png);
	background-repeat: no-repeat no-repeat;
}
.dropdown-menu {
	min-width: 40px;
}
.popover-title {
	margin: 0;
	padding: 0px 0px 0px 0px;
	font-size: 0px;
	line-height: 0px;
}
.popover-content {
	margin: 0;
	padding: 0px 0px 0px 0px;
}
.popover.right .arrow {
	top: 100px;
	left: -10px;
	margin-top: -10px;
	border-width: 10px 10px 10px 0;
	border-right-color: #ffffff;
}
.popover.right .arrow:after {
	border-width: 11px 11px 11px 0;
	border-right-color: #007AB4;;
	bottom: -11px;
	left: -1px;
}
.pull-left {
	margin-left: -52px;
	margin-top: 7px;
}
.tblborder {
	
}

.nobground2 {
	padding: 6px 7px 6px 7px;
	-webkit-border-top-left-radius: 30px;
	border-top-left-radius: 30px;
	-moz-border-radius-topleft: 30px;
	-webkit-border-top-right-radius: 30px;
	border-top-right-radius: 30px;
	-moz-border-radius-topright: 30px;
	-webkit-border-bottom-right-radius: 30px;
	border-bottom-right-radius: 30px;
	-moz-border-radius-bottomright: 30px;
	-webkit-border-bottom-left-radius: 30px;
	border-bottom-left-radius: 30px;
	-moz-border-radius-bottomright: 30px;
}
.nobground1 {
	padding: 6px 10px 6px 10px;
	-webkit-border-top-left-radius: 30px;
	border-top-left-radius: 30px;
	-moz-border-radius-topleft: 30px;
	-webkit-border-top-right-radius: 30px;
	border-top-right-radius: 30px;
	-moz-border-radius-topright: 30px;
	-webkit-border-bottom-right-radius: 30px;
	border-bottom-right-radius: 30px;
	-moz-border-radius-bottomright: 30px;
	-webkit-border-bottom-left-radius: 30px;
	border-bottom-left-radius: 30px;
	-moz-border-radius-bottomright: 30px;
}
.nobground3 {
	padding: 6px 4px 6px 4px;
	-webkit-border-top-left-radius: 30px;
	border-top-left-radius: 30px;
	-moz-border-radius-topleft: 30px;
	-webkit-border-top-right-radius: 30px;
	border-top-right-radius: 30px;
	-moz-border-radius-topright: 30px;
	-webkit-border-bottom-right-radius: 30px;
	border-bottom-right-radius: 30px;
	-moz-border-radius-bottomright: 30px;
	-webkit-border-bottom-left-radius: 30px;
	border-bottom-left-radius: 30px;
	-moz-border-radius-bottomright: 30px;
}
.icon-ok-circle {
	font-size: 3em;
	color: #00FF00;
}
.icon-circle {
	font-size: 3em;
	color: #E46C0A;
}
.icon-remove-circle {
	font-size: 3em;
	color: red;
}
.icon-circle-blank {
	font-size: 3em;
	color: red;
}
.icon-question-sign {
	font-size: 1.3em;
}
.icon-inner {
	font-size: 2.3333333333333333em;
	font-family: 'Bauhaus 93';
	color: red;
	margin-left: -26px;
	vertical-align: 5%;
}
.icon-inner2 {
	font-size: 2.3333333333333333em;
	font-family: 'Bauhaus 93';
	color: red;
	margin-left: -24px;
	vertical-align: 8%;
}
.circle {
	width: 48px;
	height: 48px;
	margin: 0em auto;
}
.subheading {
	font-weight: none;
}
.icon-folder-open,icon-copy,icon-cut,icon-paste,icon-remove-sign,icon-edit
	{
	color: #007AB4;
}
.marginleft20 {
	margin-left: -20px;
}
.margintop20 {
	margin-top: -20px;
}
div.t_fixed_header_main_wrapper {
	position: relative;
	overflow: visible;
}
.modal_header_profile {
	border-bottom: 1px solid #eee;
	background-color: #0078b4;
	text-overflow: ellipsis;
	border: 1px solid #999;
	border: 1px solid rgba(0, 0, 0, 0.3); *
	border: 1px solid #999;
	-webkit-border-radius: 6px;
	-moz-border-radius: 6px;
	border-radius: 6px;
	outline: none;
	-webkit-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
	-moz-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
	box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
	-webkit-background-clip: padding-box;
	-moz-background-clip: padding-box;
	background-clip: padding-box;
}
.scrollspy_profile {
	height: 500px;
	overflow-y: auto;
	overflow-x: hidden;
	padding-left: 5px;
}
.divwidth {
	width: 728px;
}
.tablewidth {
	width: 900px;
}
.net-widget-footer {
	border-bottom: 1px solid #cccccc;
	border-left: 1px solid #cccccc;
	border-right: 1px solid #cccccc;
	line-height: 40px;
	background-color: #F2FAEF;
	background-image: -moz-linear-gradient(top, #FFFFFF, #FFFFFF);
	background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#FFFFFF),
		to(#FFFFFF) );
	background-image: -webkit-linear-gradient(top, #FFFFFF, #FFFFFF);
	background-image: -o-linear-gradient(top, #FFFFFF, #FFFFFF);
	background-image: linear-gradient(to bottom, #FFFFFF, #FFFFFF);
	background-repeat: repeat-x;
	color: #4D4D4E;
	vertical-align: middle;
}
.modaljob {
	position: fixed;
	top: 40%;
	left: 45%;
	z-index: 2000;
	overflow: auto;
	width: 980px;
	margin: -250px 0 0 -440px;
	background-color: #ffffff;
	border: 1px solid #999;
	border: 1px solid rgba(0, 0, 0, 0.3); *
	border: 1px solid #999;
	/* IE6-7 */
	-webkit-border-radius: 6px;
	-moz-border-radius: 6px;
	border-radius: 6px;
	-webkit-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
	-moz-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
	box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
	-webkit-background-clip: padding-box;
	-moz-background-clip: padding-box;
	background-clip: padding-box;
}
.net-corner-bottom {
	-moz-border-radius-bottomleft: 12px;
	-webkit-border-bottom-left-radius: 12px;
	border-bottom-left-radius: 12px;
	-moz-border-radius-bottomright: 12px;
	-webkit-border-bottom-right-radius: 12px;
	border-bottom-right-radius: 12px;
}
.custom-div-border1 {
	border: 1px solid #999;
	border: 1px solid rgba(0, 0, 0, 0.3);
	-moz-border-radius: 6px;
	border-radius: 6px;
}
.modal-border { /*border: 1px solid #0A619A;*/
	
}
.custom-div-border {
	padding: 1px;
	border-bottom-left-radius: 2em;
	border-bottom-right-radius: 2em;
}
.pdfDivBorder {
	z-index: 5000;
	border: 1px solid #999;
	border: 1px solid rgba(0, 0, 0, 0.3);
	-webkit-border-radius: 6px;
	-moz-border-radius: 6px;
	border-radius: 6px;
	-webkit-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
	-moz-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
	box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
	-webkit-background-clip: padding-box;
	-moz-background-clip: padding-box;
	background-clip: padding-box;
}
.custom-border {
	margin-left: -32px;
	margin-top: 2px;
}
.dropdown-menu a {
	display: block;
	padding: 3px 15px;
	margin-padding: 100px;
	clear: both;
	font-weight: normal;
	line-height: 20px;
	color: #333333;
	white-space: nowrap;
}
.dropdown-menu a:hover {
	color: #007AB4;
}
.status-notes-image {
	margin-left: 40px;
}
.status-notes-text {
	margin-top: -20px;
	margin-left: 80px;
}

.profileContent
{
	padding: 0px;
	padding-left: 12px;
	padding-top: 10px;
	color: #474747;
	font-weight: normal;
	font-size: 10px;
}

/*added by Ram nath*/
#divStatusNoteGrid .row, #divStatusNoteGrid .jqte{
	width: 100% !important;
}
/*ended by Ram nath*/
.portfolio-dialog {
    left: 50%;
    right: auto;
    width: 600px;
    padding-top: 0px;
    padding-left: 10px;
    padding-right: 10px;
    padding-bottom: 30px;
    margin-left: auto;
    margin-right: auto;
    z-index: 1050;
}
</style>
<script type="text/javascript">
var $j=jQuery.noConflict();
	$j(document).ready(function() {
});

function applyScrollOnLEACandidatePorfolio()
{	
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#divLEACandidatePorfolioGridD').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 380,
         <c:choose>
          <c:when test="${entityType==2 || entityType==3 || entityType==5}">
             width: 830,
           </c:when>
           <c:otherwise>
             width: 730,
           </c:otherwise>
        </c:choose>
        minWidth: null,
        minWidthAuto: false,
       	<c:choose>
          <c:when test="${entityType==2 || entityType==3 || entityType==5}">
             colratio:[200,100,200,150,100,80],
           </c:when>
           <c:otherwise>
             colratio:[200,100,150,150,80,50],
           </c:otherwise>
        </c:choose>
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
    });			
}


 function applyScrollOnTblLicenseDate()
{	
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#divLicneseGridCertificationsGridD').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 380,
         <c:choose>
          <c:when test="${entityType==2 || entityType==3 || entityType==5}">
             width: 830,
           </c:when>
           <c:otherwise>
             width: 730,
           </c:otherwise>
        </c:choose>
        minWidth: null,
        minWidthAuto: false,
        <c:choose>
          <c:when test="${entityType==2 || entityType==3 || entityType==5}">
             colratio:[400,220,210],
           </c:when>
           <c:otherwise>
             colratio:[350,220,160],
           </c:otherwise>
        </c:choose>
       	addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
    });			
}

function applyScrollOnAssessmentsSP()
{		
		var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#assessmentTableSP').fixheadertable({ 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 150,
        <c:choose>
          <c:when test="${entityType==2 || entityType==3}">
             width: 830,
           </c:when>
           <c:otherwise>
             width: 700,
           </c:otherwise>
        </c:choose>
        minWidth: null,
        minWidthAuto: false,
        colratio:[180,120,150,185], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });            
        });
}

function applyScrollOnAssessments()
{		
		var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#assessmentTable').fixheadertable({ 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 150,
        <c:choose>
          <c:when test="${entityType==2 || entityType==3}">
             width: 830,
           </c:when>
           <c:otherwise>
             width: 730,
           </c:otherwise>
        </c:choose>
        minWidth: null,
        minWidthAuto: false,
        colratio:[180,120,150,185], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });            
        });
}

function applyScrollOnTbl_AssessmentDetails()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#assessmentDetails').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        //height: 100,
        width: 830,
	    minWidth: null,
        minWidthAuto: false,
        olratio:[230,200,200,200],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 70,
        wrapper: false
        });
            
        });			
}
function applyScrollOnTblLanguage()
{
    var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#teacherLanguageGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
         height: 380,
        <c:choose>
          <c:when test="${entityType==2 || entityType==3}">
             width: 830,
           </c:when>
           <c:otherwise>
             width: 730,
           </c:otherwise>
        </c:choose>
        minWidth: null,
        minWidthAuto: false,
         // table header width
        
         height: 380,
        <c:choose>
          <c:when test="${entityType==2 || entityType==3}">
             colratio:[277,276,276],
           </c:when>
           <c:otherwise>
             colratio:[244,243,243],
           </c:otherwise>
        </c:choose>
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });         
}


function applyScrollOnHonors()
{
		var $j=jQuery.noConflict();
      $j(document).ready(function() {
      $j('#honorsGrid').fixheadertable({ //table id 
      caption: '',
      showhide: false,
      theme: 'ui',
      height: 350,
      width: 730,
      minWidth: null,
      minWidthAuto: false,
      colratio:[250,250,175,176,111], // table header width
      addTitles: false,
      zebra: true,
      zebraClass: 'net-alternative-row',
      sortable: false,
      sortedColId: null,
      //sortType:[],
      dateFormat: 'd-m-y',
      pager: false,
      rowsPerPage: 10,
      resizeCol: false,
      minColWidth: 100,
      wrapper: false
      });            
      });
}


function applyScrollOnInvl()
{
		var $j=jQuery.noConflict();
      $j(document).ready(function() {
      $j('#involvementGrid').fixheadertable({ //table id 
      caption: '',
      showhide: false,
      theme: 'ui',
      height: 350,
      width: 730,
      minWidth: null,
      minWidthAuto: false,
      colratio:[250,250,175,176,111], // table header width
      addTitles: false,
      zebra: true,
      zebraClass: 'net-alternative-row',
      sortable: false,
      sortedColId: null,
      //sortType:[],
      dateFormat: 'd-m-y',
      pager: false,
      rowsPerPage: 10,
      resizeCol: false,
      minColWidth: 100,
      wrapper: false
      });            
      });
}





function applyScrollOnTblOnr(userRoleId)
{	
	if(${ncCheck}==true){
		if(userRoleId == 8){
		 	var columnSpan = [360,100,120,80,180,60,80];
		 	var tableWidth = 980;
		 }else if(userRoleId == 9){
		 	var columnSpan = [260,100,100,100,150,60,60,50,50,50,50,70];
		 	var tableWidth = 1100;
		 }else {
		 	var columnSpan = [30,260,100,120,120,150,70,70,60,60,60,60,70];
		 	var tableWidth = 1230;
		 }
	
	}else{
			if(userRoleId == 8){
		 	var columnSpan = [360,100,120,80,180,60,80];
		 	var tableWidth = 980;
		 }else if(userRoleId == 9){
		 	var columnSpan = [260,80,80,80,130,50,50,50,50,50,50,50,50,70];
		 	var tableWidth = 1100;
		 }else {
		 	var columnSpan = [30,260,80,80,80,130,50,50,50,50,50,50,50,50,50,50,70];
		 	var tableWidth = 1230;
		 }
	}

	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#onrGridTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 250,
        width: tableWidth,
        minWidth: null,
        minWidthAuto: false,
        colratio:columnSpan, // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 10,
        wrapper: false
        });
        });			
}
function applyScrollOnTblI9()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#firstTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 400,
        width: 826,
        minWidth: null,
        minWidthAuto: false,
        colratio:[90,200,100,180,256], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
        });			
}

function applyScrollOnTblFP()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#fpTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 400,
        width: 830,
        minWidth: null,
        minWidthAuto: false,
        colratio:[80,130,80,80,100,130,150,80], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
        });			
}
function applyScrollOnTblDT()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#dtTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 400,
        width: 826,
        minWidth: null,
        minWidthAuto: false,
        colratio:[90,180,80,80,130,266], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
        });			
}

function applyScrollOnTblTrans()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#transTableHistory').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 150,
        width: 826,
        minWidth: null,
        minWidthAuto: false,
        colratio:[90,200,100,180,256], // table header width
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
    });			
}

function applyScrollOnTblTransAcademic()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#academicGridAcademic').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 150,
        width: 826,
        minWidth: null,
        minWidthAuto: false,
        colratio:[35,150,180,200,185,76],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
    });	
}

function applyScrollOnTblEEOC()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#EEOCTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 400,
        width: 826,
        minWidth: null,
        minWidthAuto: false,
        colratio:[90,200,100,180,256], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
        });			
}

function applyScrollOnTblTeacherCertifications_profile()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblTeacherCertificates_Profile').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        width: 730,
        minWidth: null,
        minWidthAuto: false,
        colratio:[200,100,100,130,200],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 20,
        wrapper: false
        });
            
        });			
}

function applyScrollOnTblTeacherAcademics_profile()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblTeacherAcademics_Profile').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        width: 730,
        minWidth: null,
        minWidthAuto: false,    
        colratio:[410,180,60,60],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 20,
        wrapper: false
        });
            
        });			
}

function applyScrollOnTblWorkExp()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblWorkExp_Profile').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        width: 730,
        minWidth: null,
        minWidthAuto: false,
        colratio:[148,278,90,103,111],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}

function applyScrollOnTblEleRef_profile()
{
	//alert("Hi")
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tbleleReferences_Profile').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        width: 730,
        minWidth: null,
        minWidthAuto: false,
        colratio:[120,85,90,160,80,82,65,45],    //changed: by rajendra	
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 70,
        wrapper: false
        });
            
        });			
}

function applyScrollOnTbl_AdditionalDocuments()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#additionalDocumentsGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 380,
        width: 728,
        minWidth: null,
        minWidthAuto: false,
        colratio:[500,228,],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
        });			
}

function applyScrollOnTblVideoLinks_profile()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblelevideoLink_Profile').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        width: 730,
        minWidth: null,
        minWidthAuto: false,
        colratio:[450,150,130,],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 70,
        wrapper: false
        });
            
        });			
}

//////////Message scroll 
function applyScrollOnMessageTbl()
{
		var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblMessages').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 70,
        width: 614,
        minWidth: null,
        minWidthAuto: false,
        colratio:[100,140,100,215,60], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });            
        });
}
function applyScrollOnTblKellyOnb()
{				
	var columnSpan = [150,70,110,50,40,40,40,70,50,50,40,50,40,50,40,45,50,45,45,25];
	var tableWidth = 1100;
		
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#onbGridTable').fixheadertable({ 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 250,
        width: tableWidth,
        minWidth: null,
        minWidthAuto: false,
        colratio:columnSpan,
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 10,
        wrapper: false
        });
        });			
}
function applyScrollOnJob()
{				
	var columnSpan = [60,55,185,110,120,80,100,65,70,110];
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#jobTable').fixheadertable({ 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 235,
        width: 950,
        minWidth: null,
        minWidthAuto: false,
        colratio:columnSpan,
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 50,
        wrapper: false
        });
        });			
}

function applyScrollOnTableGrid(id)
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#'+id).fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 150,
        width: 700,
        minWidth: null,
        minWidthAuto: false,
        colratio:[50,80,160,280,130], // table header width
        // colratio:[220,100,120,120,130], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });            
        });
}

function applyScrollOnStatusNote()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('.tblStatusNote').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 100,
        width: 700,
        minWidth: null,
        minWidthAuto: false,
        colratio:[220,100,120,120,130], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });            
        });
}
</script>
<div class="row" style="width:1125px;">
	<div class="col-sm-6">
		<div style="float: left;">
			<img src="images/dashboard-icon.png" width="41" height="41" alt="">
		</div>
		<div style="float: left;">
			<div class="subheading">
			 Onboarding Dashboard
			</div>
			<input type="hidden" name="nodeColor" id="nodeColor"/>
			<input type="hidden" name="districtId" id="districtId"	value="${districtId}" />
			<input type="hidden" name="hdId" id="hdId"	value="${hdId}" />
			<input type="hidden" name="flagCanEditOrView" value="${flagCanEditOrView}" />
		</div>
	</div>	
	<div style="clear: both;margin-left:15px;border-bottom: 1px solid black;padding-bottom: 10px;width:97%;"></div>



<table width='100%'  border='0' cellspacing='0' cellpadding='5'>
  <tr>
    <td>
        <div class="col-sm-3 col-md-2">
			<span class=""><label class="">KSN ID</label><input type="text" id="ksn" name="ksn" onkeypress="return checkForInt(event);" maxlength="50"  class="form-control fl"/></span>
		</div>
		<div class="col-sm-3 col-md-3">
			<span class=""><label class="">FIRST NAME</label><input type="text" id="firstName" name="firstName"  maxlength="40"  class="help-inline form-control"/></span>
		</div>
		<div class="col-sm-3 col-md-2">
		 <div class="">
		 	<span class=""><label class="">LAST NAME</label><input type="text" id="lastName" name="lastName"  maxlength="40"  class="help-inline form-control"/></span>
		 </div>
		</div>			       
		<div class="col-sm-3 col-md-3">
		 <div><label class="">EMAIL ADDRESS</label><input type="text" id="email" name="email"  maxlength="75"  class="help-inline form-control"/>
		</div>				       
		</div>
		
		<div class="col-sm-3 col-md-2">
			<span class=""><label class="">Branch</label>		 
	          <c:if test="${branchMaster==null}">
							<span> 
								<input type="text" id="branchName" 
									maxlength="100" name="branchName"
									class="help-inline form-control"
									onfocus="getBranchMasterAutoComp(this, event, 'divTxtShowDataBranch1', 'branchName','branchSearchId','');"
                                    onkeyup="getBranchMasterAutoComp(this, event, 'divTxtShowDataBranch1', 'branchName','branchSearchId','');"
                                    onblur="hideBranchMasterDiv(this,'branchSearchId','divTxtShowDataBranch1');"/>
                                    </span>
                                    <input  type="hidden" id="branchSearchId" name="branchSearchId" value="">
                        
                            <div id='divTxtShowDataBranch1' style=' display:none;position:absolute;z-index:5000;' onmouseover="mouseOverChk('divTxtShowDataBranch1','branchName')" class='result' ></div> 
							
						</c:if>
						 <c:if test="${branchMaster!=null}">
										<input type="text" class="form-control"
											value="${branchMaster.branchName}" disabled="disabled" />
										<input type="hidden" id="branchSearchId" value="${branchMaster.branchId}" />
										<input type="hidden" id="branchName" value="${branchMaster.branchName}"
											name="branchName" />
									</c:if>
	          
			</span>
		</div>
	</td>
	<td>
	<div class="col-sm-12 col-md-12">
		 <div>
		   <label class="">JOB ID</label>
		   <input type="text" id="jobId" name="jobId" onkeypress="return checkForInt(event);"  maxlength="10"  class="help-inline form-control" style="width:150px;"/>
		</div>				       
		</div>	
    </td>
  </tr>
  <tr style="margin-top: 0px;">
   <td>
         <div class="col-sm-5 col-md-4">
			<span class=""><label class="" style="width: 130px;">MASTER JOB CATEGORY</label>		 
	        <select id="jobCategoryId" name="jobCategoryId" class="form-control" style="width: 290px;">
		      <option value="0">Select Category</option>
		         <c:if test="${not empty jobCategoryMastersList}">
	                <c:forEach var="jobCategoryMaster" items="${jobCategoryMastersList}" varStatus="status">
	        	      <option value="${jobCategoryMaster.jobCategoryId}"><c:out value="${jobCategoryMaster.jobCategoryName}"/></option>
			        </c:forEach>
	             </c:if>
	         </select>
			</span>
		</div>
		<div class="col-sm-8 col-md-11" style="margin-top: -59px; margin-left: 386px;">
			<label class="">CANDIDATE GRID FILTER</label>
			<div class="" style="margin-top: 5px;margin-left: -10px">
			 <label  style="padding-left: 15px;">
			<input type="checkbox" id="ksnId" name="CGFilter" style="margin-right: 5px;" onclick="showSubFIlterDiv();" checked/><spring:message code="onbUnlinkedorOnboarding"/>
			</label>
			<label style="padding-left: 10px;">
			<input type="checkbox" id="hired" name="CGFilter" style="margin-right: 5px;" onclick="showSubFIlterDiv();"/><spring:message code="onbHiredSPCompleted"/>
			</label>
			<label style="padding-left: 10px;">
			<input type="checkbox" id="tTIE" name="CGFilter" style="margin-right: 5px;" onclick="showSubFIlterDiv();"/><spring:message code="onbInactiveTalent"/>
			</label>
			<label style="padding-left: 10px;">
			<input type="checkbox" id="Withdrawn" name="CGFilter" style="margin-right: 5px;" onclick="showSubFIlterDiv();"/>Withdrawn
			</label>
			<label style="padding-left: 10px;">
			<input type="checkbox" id="hidden" name="CGFilter" style="margin-right: 5px;" onclick="showSubFIlterDiv();"/>Hidden
			</label>
			<label style="padding-left: 10px;">
			<button class="btn btn-primary" type="button" onclick="displayKellyONBGrid(0,1);" style="width: 95px; margin-left: 25px;"><spring:message code="lnkSearch"/><i class="icon"></i></button> 				      
		    </label>
			</div>
		</div>
    </td>
  </tr>
   <tr style="margin-top: 0px;">
   <td style="">
   <div class="col-sm-12 col-md-14" style="display: none;margin-left: 0px;" id="subFilterDiv">
			<label style="margin-top: 0px;">SUB FILTERS</label>
			<div class="" style="margin-top: 0px;margin-left:5px;width: 110%;">
			 <label  style="padding-left: 10px;">
			<input type="radio" id="ksn"  name="actionSearchId" value="6" style="margin-right: 5px;"/><b>Unlinked</b>
			</label>
			<label style="padding-left: 15px;">
			<input type="radio" id="SPI" name="actionSearchId" value="2" style="margin-right: 5px;"/><b>Smart Practices Incomplete</b>
			</label>
			<label  style="padding-left: 10px;">
			<input type="radio" id="NW1" name="actionSearchId" value="1" style="margin-right: 5px;"/><b><spring:message code="onbNeedINVtoWF1"/></b>
			</label>			
			<label style="padding-left: 15px;">
			<input type="radio" id="W1I" name="actionSearchId" value="3" style="margin-right: 5px;"/><b>Workflow 1 Incomplete</b>
			</label>
			<label style="padding-left: 15px;">
			<input type="radio" id="NW2" name="actionSearchId" value="4" style="margin-right: 5px;"/><b><spring:message code="onbNeedINVtoWF2"/></b>
			</label>
			<label style="padding-left: 15px;">
			<input type="radio" id="W2I" name="actionSearchId" value="5" style="margin-right: 5px;"/><b>Workflow 2 Incomplete</b>
			</label>
			</div>
		</div>
   </td>
   <td>
   </td>
   </tr>
</table>	
</div>
<div class="row" style="margin-top: 5px; width:1100px;"> 
<div id='tpMenuActionDiv' style='display:none;margin-left:20px;float: left;' class="col-sm-10">
	<table  class='tblborder' cellpadding=0>
		<tr>
			<td style='font-size: 12px;' vertical-align='middle'><spring:message code="lblAct"/>&nbsp;&nbsp;</td>
			<td style="margin-left: 50px;">
				<span class="btn-group">
					<a data-toggle="dropdown" href="javascript:void(0);">
						<span class='icon-collapse  iconcolor icon-large'></span>
					</a>
					<ul class="dropdown-menu pull-left">
						<li>
							<a href="javascript:void(0);" onclick='sendMessageMassTalent();'><spring:message code="btnSendMsg"/></a>
						</li>
						<li>
							<a href="javascript:void(0);" onclick='displayKellyONBGrid(1,0);'><spring:message code="lblExpToExcel"/></a>
						</li>
						<li>
							<a href="javascript:void(0);" onclick='showSelectedDataForPrint();'><spring:message code="lblPrintData"/></a>
						</li>
						<li>
							<a href="javascript:void(0);" onclick='printResume();'><spring:message code="lblPrintResume1"/></a>
						</li>
						<li>
							<a href="javascript:void(0);" onclick='callMessageQueueAPI(1);'><spring:message code="lblLabelLinkToKsn"/></a>
						</li>
						<li>
							<a href="javascript:void(0);" onclick='callMessageQueueAPI(2);'><spring:message code="lblInviteRegWF1"/></a>
						</li>
						<li>
							<a href="javascript:void(0);" onclick='callMessageQueueAPI(3);'><spring:message code="lblInviteRegWF2"/></a>
						</li>
					<!-- 	<li>
							<a href="javascript:void(0);" onclick='callMessageQueueAPI22(4);'><spring:message code="lblInviteRegWF3"/></a>
						</li>
						<li>
							<a href="javascript:void(0);" onclick='doSPSWaivedForMultiple();'>Waive SP</a>
						</li> -->
					</ul>
				</span>
			</td>
		</tr>
	</table>
</div>
	       <div class="col-sm-1" style="float: right; padding-left: 4%;" id='exportDiv'>
         <table class="">
		      <tr>
		         <td  style='padding-left:5px;'>
					<a data-original-title='Export to Excel' rel='tooltip' id='exlId' href='javascript:void(0);'  onclick="displayKellyONBGrid(2,0);"><span class='icon-file-excel icon-large iconcolor'></span></a>
				 </td>
				 <td>
				  <a data-original-title='Export to PDF' rel='tooltip'  href="javascript:void(0);" id='pdfId' onclick='displayKellyONBGrid(3,0)'><span class='pdf-eoc icon-large iconcolor'></span></a>
				 </td>
				
				 <td>
					<a data-original-title='Print' rel='tooltip' id='printId' href='javascript:void(0);' onclick="displayKellyONBGrid(4,0)"><span class='icon-print icon-large iconcolor'></span></a>
				 </td>  
			</tr>
		</table>
    </div>
   </div> 
<div class="row" style="margin-top:4px;">		
	    <div class="col-sm-12">
	    	<div class="table-responsive" id="kellyONBGrid" style="">		
		    </div>
       </div>
  </div>
<div id="loadingDiv" style="display:none; z-index: 5000;">
  	<table  align="left" >
 		<!--<tr><td style="padding-top:270px;padding-left:450px;" align="center"><img src="images/please.jpg"/></td></tr>
 		--><tr><td style="padding-top:270px;padding-left:450px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
 		<tr id='paymentMessage'><td style='padding-top:0px;padding-left:450px;' id='spnMpro' align='center'></td></tr>
	</table>
</div>
<div id="loadingDivHZS" style="display:none; z-index: 5000;">
  	<table align="center">
 		<!--<tr><td style="padding-top:270px;padding-left:0px;" align="center"><img src="images/please.jpg"/></td></tr>
 		--><tr><td style="padding-top:270px;padding-left:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
 		<tr id='paymentMessage'><td style='padding-top:0px;padding-left:0px;' id='spnMpro' align='center'></td></tr>
	</table>
</div>
<div class="modal hide" id="myModalJobList786" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true"
	data-backdrop="static">
	<div class="modal-dialog" style="width: 510px;">
		<div class='modal-content'>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true" onclick="setPageFlag();">
					x
				</button>
				<h3 id="jobDetailHeaderText">
					<spring:message code="headTm"/>
				</h3>
			</div>
			<div class="modal-body">
				<div class="control-group" style="text-align: justify;text-justify: inter-word;">
					<div id='divJobHeaderError' class='divErrorMsg'
						style="display: block;"></div>
					<div id="divJobHeader" class='row mt10'></div>
					<div id="divJob786" class="control-group">
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-primary" id="sendKellyBtn"><spring:message code="btnOk"/></button>&nbsp;
				<button class="btn"  id="msgCancelBtn" data-dismiss="modal" aria-hidden="true" onclick='resetDuplicateCheck();'><spring:message code="btnClr"/></button>&nbsp;
 			</div>
		</div>
	</div>
</div>


<div class="modal hide" id="myModalJobList" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true"
	data-backdrop="static">
	<div class="modal-dialog" style="width: 1012px;">
		<div class='modal-content'>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true" onclick="setPageFlag();">
					x
				</button>
				<h3 id="jobDetailHeaderText">
					<spring:message code="hdJDetail"/>
				</h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<div id='divJobHeaderError' class='divErrorMsg'
						style="display: block;"></div>
					<div id="divJobHeader" class='row mt10'></div>
					<div id="divJob" class="table-responsive"
						style="margin-bottom: 15px; min-width: 920px; overflow: hidden;">
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true"
					onclick="setPageFlag();">
				<spring:message code="btnClose"/>
				</button>
			</div>
		</div>
	</div>
</div>

<div class="modal hide"  id="myModalMessageMassTalent"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width:690px">
	<div class="modal-content">
	<input type="hidden" id="emailIdsMassTP" name="emailIdsMassTP">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel"><spring:message code="headPostMsgToCand"/></h3>
	</div>
	<iframe id='uploadMessageFrameIDMassTP' name='uploadMessageFrameMassTP' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'>
	</iframe>
	<form id='frmMessageUploadMassTP' enctype='multipart/form-data' method='post' target='uploadMessageFrameMassTP'  class="form-inline" onsubmit="return validateMessageMassTP(1);" action='messageUploadServletMassTalent.do' accept-charset="UTF-8">
	
	<div class="modal-body" style="max-height: 450px;overflow-y:auto;">
		<div class="" id="divMessagesMassTP">						
		</div>
		<div class="control-group" style="margin-top: 5px;">
			<div class='divErrorMsg' id='errordivMessageMassTP' style="display: block;"></div>
		</div>
		<div class="control-group row" style="padding-left: 15px;padding-top: 5px;">
    		<label><strong><spring:message code="lblTo"/><br/></strong><span id="emailDivMassTP" style="width:612px;word-wrap:break-word;padding-left:1px;"></span>
   		</div>
		<div id='support' class="row" style="padding-left:15px;" >
		<div class="control-group">
			<div class="">
		    	<label><strong><spring:message code="lblSub"/></strong><span class="required">*</span></label>
	        	<input id="messageSubjectMassTP" name="messageSubjectMassTP" type="text" class="form-control" style='width:612px;' maxlength="100" />
			</div>
		</div>
	            
        <div class="control-group">
			<div class="" id="messageSendMassTP" style="width: 612px;">
		    	<label><strong><spring:message code="lblMsg"/></strong><span class="required">*</span></label>
		    	<label class="redtextmsg"><spring:message code="msgHowToCopyPastCutDouc"/>
 				</label>
	        	<textarea rows="5" class="form-control" cols="" id="msgMassTP" name="msgMassTP" maxlength="1000"></textarea>
	        </div>
	        <input type="hidden" id="teacherIdForMessageMassTP" name="teacherIdForMessageMassTP" value="">
	    	<div id='fileMessagesMassTP' style="padding-top:12px;">
        		<a href='javascript:void(0);' onclick='addMessageFileTypeMassTP();'>
        		<img src='images/attach.png'/></a> 
        		<a href='javascript:void(0);' onclick='addMessageFileTypeMassTP();'><spring:message code="lnkAttachFile"/></a>
        	</div>   
		</div>
 		</div>
 		<div id='lodingImage' style="display: block;text-align:center;padding-top:4px;"></div>
 		<!-- end support div -->
  	</div>
  	
  	</form>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClr"/></button>&nbsp;
 		<button class="btn btn-primary" onclick="validateMessageMassTalent(1)"><spring:message code="btnSend"/></button>&nbsp;
 	</div>
</div>
</div>
</div>
<input type="hidden" id="sourcefileNameMassTP" value=""/>
<input type="hidden" id="sourcefilePathMassTP" value=""/>
<div  class="modal hide"  id="myModalMsgShow_SendMessageMassTalent"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class='modal-dialog' style="width:35%;">
	<div class='modal-content'>
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel"><spring:message code="headTm"/></h3>
	</div>
	<div class="modal-body">
		<div class="control-group" id='message2show_SendMessage'><spring:message code="msgYuMsgSuccessSentToCand"/>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnOk"/></button>
 	</div>
   </div>
  </div>
</div>
<div class="modal hide" id="confirmationMessageForPrintResume" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog-for-cgcoverletter" style='width: 35%;'>
		<div class="modal-content">
			<div class="modal-header">
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body" style="max-height: 400px;overflow-y:auto;">
				<div class="control-group">
				</div>
			</div>
			<div class="modal-footer">
				<span id=""><button class="btn btn-primary" data-dismiss="modal" onclick="showConfirmationMessageForPrintResumeOk()">Ok <i class="icon"></i></button></span>&nbsp;&nbsp;
			</div>
		</div>
	</div>
</div>
<div  class="modal hide"  id="myModalMsgShowAPIvalidation"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class='modal-dialog' style="width:35%;">
	<div class='modal-content'>
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel"><spring:message code="headTm"/></h3>
	</div>
	<div class="modal-body">
		<div class="control-group" id='messageshow'>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnOk"/></button>
 	</div>
   </div>
  </div>
</div>
<div class="modal hide" id="modalDownloadsTranscript" tabindex="-1"
	role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
	style="z-index: 5000;" data-backdrop="static">
	<div class="modal-dialog-for-cgpdfreport">
		<div class="modal-content" style="background-color: #ffffff;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true" onclick="setZIndexTrans()">
					x
				</button>
				<h3 id="myModalLabel">
				<spring:message code="headTranscript"/>
				</h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<div id="" class="">
						<iframe src="" id="ifrmTrans" width="100%" height="450px">
						</iframe>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true"
					onclick="setZIndexTrans()">
					<spring:message code="btnClose"/>
				</button>
			</div>
		</div>
	</div>
</div>

<div class="modal hide"  id="modalLinkToKSN" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"   data-backdrop="static">
	<div class="modal-dialog" style="width:760px;">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick='closeLinkToKSNModal();'>x</button>
		<h3 id="myModalLabel">Link To KSN</h3>
	</div>
	<div class="modal-body">		
		<div class="control-group">
		<div class="row">
		<div class="col-sm-12">
			<div id="commDetails"></div>
		</div>
		</div>
		<div class="row">
		<div class="col-sm-12">
			<div id="ksndetails"></div>
		</div>
		</div>
 	</div>
   </div>
   <div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick='closeLinkToKSNModal();'>Cancel</button> 		
 	</div>
  </div>
 </div>
 </div>
 
 <div class="modal hide" id="confrmteacheridTmTlntId" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width: 40%;">
		<div class="modal-content"  style="background-color: #ffffff;">
			<div class="modal-header">
  				<button type="button" class="close" onclick="closeAssessmentResetDiv();">x</button>
				<h3 id="myModalLabel">Teachermatch</h3>
			</div>
			<div class="modal-body">
			<div class="" id="internalTxt">
			  <center><spring:message code="lblConfrmMSGT"/></center>
			</div>
 			</div>
 			<div class="modal-footer">
 				<span id=""><button class="btn btn-large btn-primary"  data-dismiss="modal" aria-hidden="true" id="btnKSNAPIButtonId">Ok</i></button></span>&nbsp;&nbsp;
 				<!--<button class="btn" data-dismiss="modal" aria-hidden="true"  id="clnKSNId"><spring:message code="btnClr"/> --></button>
   			</div>
  		</div>
	</div>
</div>

<div class="modal hide" id="modalSelectKSNId" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width: 40%;">
		<div class="modal-content"  style="background-color: #ffffff;">
			<div class="modal-header">
  				<button type="button" class="close" onclick="closeAssessmentResetDiv();">x</button>
				<h3 id="myModalLabel">Teachermatch</h3>
			</div>
			<div class="modal-body">
			<div class="" id="internalTxt">
			  Please Select A Record.
			</div>
 			</div>
 			<div class="modal-footer">
 				<span id=""><button class="btn btn-large btn-primary" data-dismiss="modal"  id="oktochoseKSNNO">Ok <i class="icon"></i></button></span>&nbsp;&nbsp;
 				
   			</div>
  		</div>
	</div>
</div>
<div class="modal hide" id="modalErrContinue" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width: 40%;">
		<div class="modal-content"  style="background-color: #ffffff;">
			<div class="modal-header">
  				<button type="button" class="close" onclick="closeContinue();">x</button>
				<h3 id="myModalLabel">Teachermatch</h3>
			</div>
			<div class="modal-body">
			<div class="" id="errContinuemsg">
				
			</div>
 			</div>
 			<div class="modal-footer">
 				<span id=""><button class="btn btn-large btn-primary" data-dismiss="modal" aria-hidden="true" onclick="closeContinue();">Ok <i class="icon"></i></button></span>&nbsp;&nbsp;
 				
   			</div>
  		</div>
	</div>
</div>
<div class="modal hide" id="modalsuccessKSNId" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width: 40%;">
		<div class="modal-content"  style="background-color: #ffffff;">
			<div class="modal-header">
  				<button type="button" class="close" onclick="closeModalsuccessKSNId();">x</button>
				<h3 id="myModalLabel">Teachermatch</h3>
			</div>
			<div class="modal-body">
			<div class="" id="internalTxt1">Successfully Changed</div>
 			</div>
 			<div class="modal-footer">
 				<span id=""><button class="btn btn-large btn-primary" data-dismiss="modal" aria-hidden="true" id="oktochoseKSNNOo">Ok <i class="icon"></i></button></span>&nbsp;&nbsp;
 				
   			</div>
  		</div>
	</div>
</div>
<div class="modal hide" id="confrmmodalKSNId" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width: 40%;">
		<div class="modal-content"  style="background-color: #ffffff;">
			<div class="modal-header">
  				<button type="button" class="close" onclick="closeAssessmentResetDiv();">x</button>
				<h3 id="myModalLabel">Teachermatch</h3>
			</div>
			<div class="modal-body">
			<div class="" id="internalTxt">
			  <center><spring:message code="lblDismissTxt"/></center>
			</div>
 			</div>
 			<div class="modal-footer">
 				<span id=""><button class="btn btn-large btn-primary"  data-dismiss="modal" aria-hidden="true" id="btnKSNButtonId"><spring:message code="lblCreate"/> <i class="icon"></i></button></span>&nbsp;&nbsp;
 				<button class="btn" data-dismiss="modal" aria-hidden="true"  id="clnId"><spring:message code="btnClr"/> </button>
   			</div>
  		</div>
	</div>
</div>
<div class="modal hide" id="selectedprintData"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width: 550px;">
	<div class="modal-content">
	<div class="modal-header" id="">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick='cancelMultiPrint();'>x</button>
		<h3>Select Options</h3>
	</div>
	<div class="modal-body" style="max-height: 450px;overflow-y:auto">
		<div class="divErrorMsg" id="errForPrint"></div>
		<div id="selectedprintDataBodyDiv"  data-spy="scroll" data-offset="50" style="padding-bottom: 0px; margin-bottom: -10px;" >
 			<label class="checkbox inline">
 				<input type="checkbox" name="cqp" id="chkqq" value="chkqq"/>
 				Qualification Questionnaire
 			</label>
 				
 			<label class="checkbox inline jsiView hide">
 				<input type="checkbox" name="cqp" id="chkjsi" value="chkjsi"/>
 				JSI
 			</label>
 			
 			<label class="checkbox inline">
 				<input type="checkbox" name="cqp" id="chkportfolio" value="chkportfolio" onclick="chkportfolioOPtions()"/>
 				Portfolio Item
 			</label>
		</div>	
		<!-- mukesh -->
		<input type="hidden" name="jftIds" id="jftIds"/>
		<div  class="hide" id="portfolioItmDiv" style="padding-left: 25px;padding-top: 0px; width: 100%;" >
		 <table>
		    <tr>
			    <td width="33%">
				 <label class="checkbox-inline">
			       <input type="checkbox" name="chkportfolioItem" id="pinfo" value="pinfo">
					 Personal Information
			     </label>
			   </td>
  		   
  		  
	  		  <td width="20%">
				<label class="checkbox-inline">
			      <input type="checkbox" name="chkportfolioItem" id="resume" value="resume">
			       Resume
			    </label>
			   </td>
		   
		 
		   
		     <td width="33%">		      
				<label class="checkbox-inline">
			      <input type="checkbox" name="chkportfolioItem" id="dspqQues" value="dspqQues">
			       District Specific Questions
			    </label> 
			 </td> 
		   </tr>
  		 </table> 
		</div>
		
	</div>
	<div class="modal-footer">
	 	<table border=0 style="margin-left:270px;">
	 		<tr>
		 		<td  nowrap>
		 			<button class="btn  btn-large btn-primary" aria-hidden="true" onclick='printMultipleData();'>Print Preview</button>&nbsp;&nbsp;
				</td>
				<td   nowrap>
		 			<button class="btn btn-large" data-dismiss="modal" aria-hidden="true" onclick='cancelMultiPrint();'>Cancel</button>
				</td>
	 		</tr>
	 	</table>
	</div>
	</div>
	</div>
</div>
<div class="modal hide" id="printDataProfile"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width: 925px;">
	<div class="modal-content">
	<div class="modal-header" id="printDataProfileDivHeader">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick='canelPrintData();'>x</button>
		<h3>Print Preview</h3>
	</div>
	<div class="modal-body" style="max-height: 450px;overflow-y:auto">
		<div id="printDataProfileDiv"  data-spy="scroll" data-offset="50" >
		
		</div>	
	</div>
	<div class="modal-footer">
	 	<table border=0 style="margin-left:690px;">
	 		<tr>
		 		<td  nowrap>
		 			<button class="btn  btn-large btn-primary" aria-hidden="true" id="printButton" onclick='printAllData();'>&nbsp;&nbsp;&nbsp;Print&nbsp;&nbsp;&nbsp;</button>&nbsp;&nbsp;
				</td>
				<td   nowrap>
		 			<button class="btn btn-large" data-dismiss="modal" aria-hidden="true" onclick='canelPrintData();'>Cancel</button>
				</td>
	 		</tr>
	 	</table>
	</div>
	</div>
	</div>
</div>
<div class="modal hide"  id="printmessage1"  tabindex="-1" role="dialog" data-keyboard="false" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" >
	<div class="modal-dialog" style="width:450px; margin-top: 0px;">
	<div class="modal-content">
	<div class="modal-header">
	   	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
	   <h3 id="myModalLabel">Teachermatch</h3>
	</div>
	
	<div class="modal-body" style="max-height: 400px; overflow-y: auto;"> 		
			<div class="control-group">
			<div class="" >
			 Please make sure that pop ups are allowed on your browser.
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
	 	<table border=0 align="right">
	 		<tr>
		 		<td  nowrap>
		 			<button class="btn btn-large" data-dismiss="modal" aria-hidden="true">Ok</button>
				</td>
				
	 		</tr>
	 	</table>
	</div>
  </div>
 </div>
</div>
<div class="modal hide"  id="modalSPStatusMultiple" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"   data-backdrop="static">
	<div class="modal-dialog" style="width:760px;">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick='closeSPSWaivedModal();'>x</button>
		<h3 id="myModalLabel">SP Status</h3>
	</div>
	<div class="modal-body">		
		<div class="control-group">
		<div class='divErrorMsg left2' id='errorStatusNoteMultiple' style="display: block;"></div>
		<div class="row mt5" >
				    	<div class="left16" id='statusNotesMultiple' >
				    	<label class="">Note<span class="required">*</span>
				    	</label>
				    		<textarea readonly id="statusNotesMultiple" name="statusNotesMultiple" class="span6" rows="2"   ></textarea>
				    	</div> 
				</div> 
 	</div>
   </div>
   <div class="modal-footer">
   <span id="waived"><button class="btn  btn-large btn-primary-blue" onclick='saveSPSWaivedMultiple();'><spring:message code="lblWaived"/></button>&nbsp;&nbsp;</span>
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick='closeSPSWaivedModal();'>Cancel</button> 		
 	</div>
  </div>
 </div>
 </div>
 <div class="modal hide"  id="modalSPStatus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"   data-backdrop="static">
	<div class="modal-dialog" style="width:925px;">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick='closeSPSWaivedModal();'>x</button>
		<h3 id="myModalLabel"><div id='SPSName' style="margin-left: 0px;"></div></h3>
	</div>
	<div class="modal-body">		
		<div class="control-group">
		<div class='divErrorMsg left2' id='errorStatusNote' style="display: block;"></div>
<!--		<div class="row col-sm-12">
				<div class=" pull-right"><a href="javascript:void(0);" onclick="addStatusNotes('statusNotes');">+Add Notes</a></div>
		</div>	-->
		<div class="row">
			<div class="col-sm-12">
				<div class="" id="statusSPLog"></div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12 left5 top10">
				<div class="" id="notesDiv"></div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12 left5 top10">
				<div class="" id="gridTeacherAssessmentSP"></div>
			</div>
		</div>	
		<div class="row">
			<div class="col-sm-12 left5 top5" id="statusNoteDiv">
			 	<label class="">Waived Reason<span class="required">*</span></label><br/>
				<select id="statusNote" name="statusNote" class="form-control" style="width: 275px;">
			      <option value="0">Select Reason</option>
			         <c:if test="${not empty reasonsForWaivedList}">
		                <c:forEach var="reasonsForWaived" items="${reasonsForWaivedList}" varStatus="reason">
		        	      <option value="${reasonsForWaived.reasonId}"><c:out value="${reasonsForWaived.reasonName}"/></option>
				        </c:forEach>
		             </c:if>
		         </select>
		    </div>     	
		</div> 
 	</div>
   </div>
   <div class="modal-footer">
   <span id="undo" style="display: none"><button class="btn  btn-large btn-primary" onclick='saveSPSUndo();'>Reset</button>&nbsp;&nbsp;</span>
   <span id="waived1" style="display: none"><button class="btn  btn-large btn-primary-blue" onclick='saveSPSWaived();'>Waived</button>&nbsp;&nbsp;</span>
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick='closeSPSWaivedModal();'>Cancel</button> 		
 	</div>
  </div>
 </div>
 </div>
<div class="modal hide" id="statusLogModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" data-backdrop="static">
	<div class="modal-dialog" style="width: 60%;">
		<div class="modal-content"  style="background-color: #ffffff;">
			<div class="modal-header">
  				<button type="button" class="close" onclick="closeAssessmentResetDiv();">x</button>
				<h3 id="myModalLabel">Teachermatch</h3>
			</div>
			<div class="modal-body">
			<div class="" id="statusLogold">
			  
			</div>
 			</div>
 			<div class="modal-footer">
 				<button class="btn" data-dismiss="modal" aria-hidden="true"  id="clnId"><spring:message code="btnClr"/> </button>
   			</div>
  		</div>
	</div>
</div>

<div class="modal hide" id="modalStatusNotes"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
		<div class="modal-dialog" style="width:65%;">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
					<h3 id="myModalLabel"><span id="modalTitle"></span></h3>
				</div>
				<div class="modal-body" style="max-height: 450px; overflow-y: auto;">
					<div class="row">
						<div class="col-sm-12 top10">
							<div class="" id="statusLog"></div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-10 top20 left50">
							<div id="ksndetailsnew"></div>
						</div>
					</div>
						<div class="row col-sm-11 top10 left25">
							<div class="pull-right left5" style="">
								<table>
									<tr>
									<td style="color:red;display:none;" id="tdnotes">* Please add notes</td><td>&nbsp;</td><td>&nbsp;</td>
										<td><a data-original-title='Export Notes in PDF' rel='tooltip' id='expNotePdf' href='javascript:void(0);'  onclick='downloadSLCReport();'><span class='icon-print icon-large iconcolor'></span></a></td>
										<td><a href="javascript:void(0);" onclick="addStatusNotes('notesRow');">+Add Notes</a></td>
									</tr>
								</table>
							</div>
					</div>
					<div class="row">
						<div class="col-sm-10 top5 left50" id="notesGenDiv"></div>
					</div>
					<div id="notesRow" class="row top10 hide">
						<div class="col-sm-10 left50">
							<label class="">Notes</label>
							<div id="stNotesDiv">
		        				<textarea rows="5" class="form-control" cols="" id="notes" name="notes" maxlength="1000"></textarea>
		        			</div>
		        		</div>
					</div>
				</div>
				<div class="modal-footer">
					<div style="margin-right:5px;">
					  	<span id="resetINFbtn" style="display: none"><button class="btn  btn-large btn-primary" onclick='saveStatusResetINF();'>Reset</button>&nbsp;&nbsp;</span>
							<button class="btn btn-primary btnFinalizeid" aria-hidden="true" onclick="genericNotesSaveDiv();"><spring:message code="btnFinalize"/></button>
						<button class="btn" data-dismiss="modal" aria-hidden="true" onclick=""><spring:message code="btnClr"/></button>
					</div>
				</div>
			</div>
		</div>
</div>


<div class="modal hide" id="emptynotespopup"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="hidepopup();">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div class="modal-body"> 		
		<div class="control-group">
			Please enter notes
		</div>
 	</div>
 	<div class="modal-footer">
 	<button class="btn  btn-primary" data-dismiss="modal" aria-hidden="true" onclick="hidepopup();">ok</button> 		
 	</div>
	</div>
	</div>
</div>

<div  class="modal hide pdfDivBorder"  id="modalDownloadCGSLCReport" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog-for-cgpdfreport">
	<div class="modal-content"  style="background-color: #ffffff;">
	
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" >x</button>
		<h3 id="myModalLabelSLCNotes">SLC Notes</h3>
	</div>
	<div class="modal-body">		
		<div class="control-group">
		
		  <div style="width: 100%; height: 100%; -webkit-overflow-scrolling: touch;">
		   <iframe id="ifrmCGSLCR" width="100%" height="480px" src=""></iframe>		  
		  </div>   
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" >Close</button> 		
   </div>
  </div>
 </div>
</div>

<!--For teacher Profile-->
<div  class="modal hide" id="myModalProfileVisitHistoryShow" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width:100px;" id="mydiv">
		<div class='modal-content'>
		<div class="modal-header">
	  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="showProfilePopover()">x</button>
			<h3 id="myModalLabel"><spring:message code="msgWhAdminCanHiCandFrThJoOrdAppPo"/></h3>
		</div>
		<div class="modal-body" style="max-height: 400px;overflow-y:scroll;padding-right: 18px;">		
			<div class="control-group">
				<div id="divteacherprofilevisithistory" class="">
			    		        	
				</div>
			</div>
	 	</div>
	 	<div class="modal-footer">
	 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="showProfilePopover()"><spring:message code="btnClose"/></button> 		
	 	</div>
		</div>
	</div>
</div>

<div class="modal hide custom-div-border1" id="draggableDivMaster"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
<div id="draggableDiv"></div>
</div>

<!--  Communication Div For User Profile -->
<div  class="modal hide"  id="myModalCommunications" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
	<div class="modal-dialog" style="width: 700px;">
	<div class="modal-content">	
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick='showCommunicationsDivForSave();'>x</button>
		<h3 id="myModalLabel"><spring:message code="headCom"/></h3>
	</div>
	<div class="modal-body" style="max-height: 450px;overflow-y: auto">		
		<div class="control-group">
			<div class="" id="divCommTxt">
			</div> 
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn btn-primary" aria-hidden="true" onclick='printCommunicationslogs();'>&nbsp;&nbsp;&nbsp;Print&nbsp;&nbsp;&nbsp;</button>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true"   onclick='showCommunicationsDivForSave();'><spring:message code="btnClose"/></button> 		
 	</div>
   </div>
 </div>
</div>

<div class="modal hide"  id="saveToFolderDiv"   tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
<div class="modal-dialog" style="width:900px;">
	<div class='modal-content'>
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick="defaultTeacherGrid()";>x</button>
		<h3 id="myModalLabel"><spring:message code="headSaveCandidate"/></h3>
	</div>
	<div class="modal-body" style="max-height: 500px;overflow-y: scroll;"> 	 		
		<div class="control-group">
		<div  style="border: 0px solid green;width:220px;height:450px;float: left;" class="span5">	
		<div class="span5" id="tree_menu" style=" padding: 8px 0px 10px 2px; " >
			<a data-original-title='Create' rel='tooltip' id='createIcon'><span id="btnAddCode" style="cursor: pointer;" class='icon-folder-open  icon-large iconcolor'></span></a>&nbsp;&nbsp;
			<a data-original-title='Rename' rel='tooltip' id='renameIcon'><span id="renameFolder" class='icon-edit  icon-large iconcolor'></span></a>&nbsp;&nbsp;
			<a data-original-title='Cut' rel='tooltip' id='cutIcon'><span id="cutFolder" class='icon-cut  icon-large iconcolor'></span></a>&nbsp;&nbsp;
			<a data-original-title='Copy' rel='tooltip' id='copyIcon'><span id="copyFolder" class='icon-copy  icon-large iconcolor'></span></a>&nbsp;&nbsp;
			<a data-original-title='Paste' rel='tooltip' id='pasteIcon'><span id="pasteFolder" class='icon-paste  icon-large iconcolor'></span></a>&nbsp;&nbsp;
			<a data-original-title='Delete' rel='tooltip' id='deleteIcon'><span id="deletFolder" class='icon-remove-sign  icon-large iconcolor'></span></a>
		</div>
		<iframe id="iframeSaveCandidate"  src="tree.do" class="pull-left" scrolling="auto"  frameBorder="1" style="border: 0px; padding: 0px; margin: 0px; overflow:visible; text-align: top; vertical-align: top;width:220px;height:420px;"></iframe>
	</div>
	<%-- ======================================== Right Div ========================================================================= --%>			
		<div class="span10" style="padding-top:39px; border: 0px solid green;float: left;">
				<input type="hidden" id="savecandidatearray" name="savecandidatearray">
				<input type="hidden" id="txtoverrideFolderId" name="txtoverrideFolderId">
				<input type="hidden" id="teachetIdFromPoPUp" name="teachetIdFromPoPUp" value="">
				<input type="hidden" id="teacherIdForHover" name="teacherIdForHover" value="">
				
				<input type="hidden" id="txtflagpopover" name="txtflagpopover" value=""> <%-- If It is 1 -> that means User Clicked on Pop up --%>
				<div id="savedCandidateGrid">
				
				</div>
		</div>
		<div style="clear: both"></div>	
		</div>
		
 	</div>
 	<div class="modal-footer">
 	<span id=""><button class="btn  btn-primary" onclick="saveCandidateToFolderByUser()" ><spring:message code="btnSave"/> <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true"  onclick="defaultTeacherGrid()";><spring:message code="btnClose"/></button> 		
 	</div>
</div>
</div>
</div>

<div  class="modal hide"  id="shareConfirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="defaultTeacherGrid()";>x</button>
		<h3 id="myModalLabel">Teachermatch</h3>
	</div>
	<div class="modal-body">		
		<div class="control-group">
			<div id="saveAndShareConfirmDiv" class="">
		    	<spring:message code="msgSucceSharedCand"/>     	
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="defaultTeacherGrid()";><spring:message code="btnClose"/></button> 		
 	</div>
  </div>
 </div>
</div>

<input type="hidden" id="entityType" name="entityType"
	value="${userMaster.entityType}">
<div class="modal hide" id="shareDiv" style="border: 0px solid blue;"
	tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
	aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width: 935px;">
		<div class='modal-content'>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true" onclick="defaultTeacherGrid()";>
					x
				</button>
				<h3 id="myModalLabel">
				<spring:message code="headShareCandidate"/>
				</h3>

				<input type="hidden" id="savecandiadetidarray"
					name="savecandiadetidarray" />
				<input type="hidden" id="JFTteachetIdFromSharePoPUp"
					name="JFTteachetIdFromSharePoPUp" value="">
				<input type="hidden" id="txtflagSharepopover"
					name="txtflagSharepopover" value="">
				<%-- If It is 1 -> that means User Clicked on Pop up --%>
			</div>
			<div class="modal-body">
				<div class="control-group">

					<div class="">
						<div class="row">
							<div class="col-md-10">
								<div class='divErrorMsg span12' id='errorinvalidschooldiv'></div>
							</div>
							<div class="col-sm-4 col-md-4">
								<label>
							<spring:message code="lblDistrictName"/>
								</label>
								</br>
								<c:if test="${DistrictName!=null}">
				             	${DistrictName}
				             	<input type="hidden" id="districtId"
										value="${DistrictId}" />
									<input type="hidden" id="districtName" value="${DistrictName}"
										name="districtName" />
								</c:if>
								<div id='divTxtShowData'
									onmouseover="mouseOverChk('divTxtShowData','districtName')"
									style='display: none; position: absolute; z-index: 5000;'
									class='result'></div>

							</div>

							<div class="col-sm-6 col-md-6">
								<label>
									<spring:message code="lblSchoolName"/>
								</label>
								<c:if test="${SchoolName==null}">
									<input type="text" id="shareSchoolName" maxlength="100"
										name="shareSchoolName" class="form-control" placeholder=""
										onfocus="getSchoolAuto(this, event, 'divTxtShowData3', 'shareSchoolName','districtId','');"
										onkeyup="getSchoolAuto(this, event, 'divTxtShowData3', 'shareSchoolName','districtId','');"
										onblur="hideSchoolMasterDiv(this,'schoolId','divTxtShowData3');" />
									<input type="hidden" id="schoolId" value="0" />
								</c:if>
								<c:if test="${SchoolName!=null}">
									<%-- ${SchoolName}--%>
									<input type="text" id="shareSchoolName" maxlength="100"
										name="shareSchoolName" class="form-control" placeholder=""
										onfocus="getSchoolAuto(this, event, 'divTxtShowData3', 'shareSchoolName','districtId','');"
										onkeyup="getSchoolAuto(this, event, 'divTxtShowData3', 'shareSchoolName','districtId','');"
										onblur="hideSchoolMasterDiv(this,'schoolId','divTxtShowData3');"
										value="${SchoolName}" />
									<input type="hidden" id="schoolId" value="${SchoolId}" />
									<input type="hidden" id="loggedInschoolId" value="${SchoolId}" />
									<input type="hidden" id="loggedInschoolName"
										value="${SchoolName}" />
								</c:if>
								<div id='divTxtShowData3'
									onmouseover="mouseOverChk('divTxtShowData3','shareSchoolName')"
									style='display: none; position: absolute; z-index: 5000;'
									class='result'></div>

							</div>
							<div class="col-md-2 top25">
								<label>
									&nbsp;
								</label>
								<input type="button" id="" name="" value="Go"
									onclick="searchUserthroughPopUp(1)" class="btn  btn-primary">
							</div>
						</div>
					</div>


					<div id="divShareCandidateToUserGrid"
						style="border: 0px solid green;" class="mt30">
						<br />
						<br />
						<br />
						<br />
						<br />
						<br />
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<span id=""><button class="btn btn-primary"
						onclick="shareCandidatethroughPopUp()">
						<spring:message code="btnShare"/>
						<i class="icon"></i>
					</button>
				</span>&nbsp;&nbsp;
				<button class="btn" data-dismiss="modal" aria-hidden="true"
					onclick="defaultTeacherGrid()";>
					<spring:message code="btnClr"/>
				</button>
			</div>
		</div>
	</div>
</div>
<div class="modal hide"  id="duplicatCandidate" style="border: 0px solid blue;" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">Teachermatch</h3>
	</div>
	<div class="modal-body"> 		
		<div class="control-group">
			<spring:message code="msgCandidateSvav/Cancel"/>
		</div>
 	</div>
 	<div class="modal-footer">
 	<span id=""><button class="btn btn-primary" onclick="saveWithDuplicateRecord()" ><spring:message code="btnOk"/> <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClr"/></button> 		
 	</div>
   </div>
  </div>
</div>
 <!-- PDF -->
<div  class="modal hide pdfDivBorder"  id="modalDownloadJobOrder" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog-for-cgpdfreport">
	<div class="modal-content"  style="background-color: #ffffff;">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel"><span id="pdfDistrictOrSchoolTitle"></span> <spring:message code="headonboarding"/> </h3>
	</div>
		<div class="modal-body">		
			<div class="control-group">
				    <div style="width: 100%; height: 100%; -webkit-overflow-scrolling: touch;">
	                  <iframe src="" id="ifrmCJS" width="100%" height="480px">
	               </iframe>        	
				</div>
			</div>
	 	</div>
	 	<div class="modal-footer">
	 		<button class="btn" data-dismiss="modal" aria-hidden="true" ><spring:message code="btnClose"/></button> 		
	 	</div>
     </div>
 	</div>
</div>
<!-- PDF End -->  
<div class="modal hide" id="disconnectResponse" style="display: none;z-index: 5000" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
					<h3 id="myModalLabel">TeacherMatch</h3>
				</div>
				<div class="modal-body">
					<div class="control-group" id="disconnectRespMsg"></div>
				</div>
				<div class="modal-footer">
					<button class="btn btn-primary" data-dismiss="modal" aria-hidden="true" onclick="hideDisconnectResponse();"><spring:message code="btnOk"/></button>
				</div>
			</div>
		</div>
</div>
<!-- Message -->
<div class="modal hide"  id="myModalMessage"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width:690px">
	<div class="modal-content">
	<input type="hidden" id="emailId" name="emailId">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick='showCommunicationsDivForMsg();'>x</button>
		<h3 id="myModalLabel">Post a message to the Candidate</h3>
	</div>
	<iframe id='uploadMessageFrameID' name='uploadMessageFrame' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'>
	</iframe>
	<form id='frmMessageUpload' enctype='multipart/form-data' method='post' target='uploadMessageFrame'  class="form-inline" onsubmit="return validateMessage();" action='messageCGUploadServlet.do' accept-charset="UTF-8">
	
	<div class="modal-body" style="max-height: 450px;overflow-y:auto;">
		<div class="" id="divMessages">						
		</div>
		<div class="control-group" style="margin-top: 5px;">
			<div class='divErrorMsg' id='errordivMessage' style="display: block;"></div>
		</div>
		
		<%---------- Gagan :District wise Dynamic Template [Start]  --%>
			<div id="templateDiv" style="display: none;">		     	
		    	<div class="row col-md-12" >
		    	<div class="row col-sm-6" style="max-width:300px;">
			    	<label><strong>Template</strong></label>
		        	<br/>
		        	<select class="form-control" id="districttemplate" onchange="setTemplate()">
		        		<option value="0">Select Template</option>
		        	</select>
		         </div>	
		        	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<button class="btn btn-primary hide top26" id="btnChangetemplatelink" onclick="return getTemplate();">Apply Template</button> <input type="hidden" class="span1" id="confirmFlagforChangeTemplate" name="confirmFlagforChangeTemplate" value="0">
			
		    	</div>
		    </div>
		    
		    <div class="row col-md-12" id="documentdiv">
	    	<div class="row col-sm-6" style="max-width:300px;">
		    	<label><strong>Document</strong></label>
	        	<br/>
	        	<select class="form-control" id="districtDocument" onchange="showDocumentLink()">
	        		<option value="">Select Document</option>
	        	</select>
	        	
		
	         </div>	<br><br>
	         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	        	<a href="javascript:void(0);" id="viewDoc" class="hide top26" onclick="return vieDistrictFile('ifrmAttachment')">View File</a>
	    	</div>
		<%---------- Gagan :District wise Dynamic Template [END]  --%>
		
		<div class="control-group row" style="padding-left: 15px;padding-top: 5px;">
    		<label><strong>To<br/></strong><span id="emailDiv" style="width:612px;word-wrap:break-word;padding-left:1px;"></span>
   		</div>
		<div id='support' class="row" style="padding-left:15px;" >
		<div class="control-group">
			<div class="">
		    	<label><strong>Subject</strong><span class="required">*</span></label>
	        	<input id="messageSubject" name="messageSubject" type="text" class="form-control" style='width:612px;' maxlength="100" />
			</div>
		</div>
	            
        <div class="control-group">
			<div class="" id="messageSend" style="width: 612px;">
		    	<label><strong>Message</strong><span class="required">*</span></label>
		    	<label class="redtextmsg">(If you are planning to copy and paste text from a web page and/or a Microsoft document,
 				please ensure to paste that text first into a simple text editor such as Notepad to remove the 
 				special characters and then copy and paste it into the field below. Otherwise, the text 
 				directly cut and pasted from a web page and/or a Microsoft document may not display properly to the reader)
 				</label>
	        	<textarea rows="5" class="form-control" cols="" id="msg" name="msg" maxlength="1000"></textarea>
	        </div>
	        <input type="hidden" id="teacherIdForMessage" name="teacherIdForMessage" value="">
    		<input type="hidden"  name="messageDateTime" id="messageDateTime" value="${dateTime}"/>
	    	<div id='fileMessages' style="padding-top:12px;">
        		<a href='javascript:void(0);' onclick='addMessageFileType();'>
        		<img src='images/attach.png'/></a> 
        		<a href='javascript:void(0);' onclick='addMessageFileType();'>Attach a File</a>
        	</div>   
		</div>
 		</div>
 		<div id='lodingImage' style="display: block;text-align:center;padding-top:4px;"></div>
 		<!-- end support div -->
  	</div>
  	
  	</form>
 	<c:set var="chkSendDisp" value="inline"/>
		<c:choose>
			<c:when test="${userMaster.entityType eq 1}">
				<c:set var="chkSendDisp" value="none" />
			</c:when>
			<c:when test="${fn:indexOf(roleAccess,'|8|') ne -1}">
				<c:set var="chkSendDisp" value="inline" />
			</c:when>
			<c:otherwise>			
				<c:set var="chkSendDisp" value="none" />
			</c:otherwise>
	</c:choose>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true"  onclick='showCommunicationsDivForMsg();'>Cancel</button>&nbsp;
 		<button class="btn btn-primary" onclick="validateMessage()">Send</button>&nbsp;
 	</div>
</div>
</div>
</div>
<div  class="modal hide distAttachmentShowDiv"  id="distAttachmentShowDiv"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="table-responsive">
		<div class='onr-dialog'>
			<div class='modal-content'>
			<div class="modal-header">
		  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="hideDiv();">x</button>
				<h3 id="UserProfileNameEEOC11">Attachment</h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<div class="" id="divPnrAttachment">
					 
					</div>
					<iframe src="" id="ifrmAttachment" width="100%" height="480px"> </iframe>   
				</div>
				
		 	</div>
		 	<input type="hidden" id="eligibilityMasterId" value=""/>
		 	<div class="modal-footer">
		 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="showMessageDiv1()" >Close</button> 		
		 	</div>
		  </div>
		 </div>
	</div>
</div>
<script type="text/javascript">
  window.onload = displayKellyONBGrid(0,0);
  $('#pdfId').tooltip();
  $('#exlId').tooltip();
  $('#printId').tooltip();
  $('#expNotePdf').tooltip();
</script>
<input type="hidden" id=msgType name="msgType" value="0"/>
<input type="hidden" id="teacherJobId" value="">
<input type="hidden" id="teacherDetailId" name="teacherDetailId">
<input type="hidden" id="headQuarterId" value="${headQuarterMaster.headQuarterId}"/>
<input type="hidden" id="branchId" value="${branchMaster.branchId}"/>
<input type="hidden" name="teacherId" id="teacherId" value="" />
<input type="hidden" name="currentNode" id="currentNode" value="" />
<input type="hidden" name="jobForTeacherId" id="jobForTeacherId" value="" />
<input type="hidden" name="teacherNotesId" id="teacherNotesId" value="" />
<input type="hidden" name="secStatusId" id="secStatusId" value="" />
<input type="hidden" name="statusId" id="statusId" value="" />
<input type="hidden" name="linkedToKSN" id="linkedToKSN" value="" />
<input type="hidden" name="secStatusId" id="secStatusId" value="" />
<input type="hidden" id="teacherIdForprofileGrid" name="teacherIdForprofileGrid"/>
<input type="hidden" id="jobForTeacherGId" name="jobForTeacherGId" value="" />
<input type="hidden" id="commDivFlag" name="commDivFlag" value="" />
<input type="hidden"  name="checkboxshowHideFlag" id="checkboxshowHideFlag"/>
<input type="hidden" name="folderId" id="folderId"/>
<input type="hidden" name="pageFlag" id="pageFlag" value="0"/>
<input type="hidden" name="teachersharedId" id="teachersharedId"/>
<input type="hidden" name="userFPD" id="userFPD"/>
<input type="hidden" name="userFPS" id="userFPS"/>
<input type="hidden" name="userCPD" id="userCPD"/>
<input type="hidden" name="userCPS" id="userCPS"/>
<input type="hidden"  name="currentPopUpId" id="currentPopUpId" value=""/>
<input type="hidden" name="teacherIdForprofileGridVisitLocation" id="teacherIdForprofileGridVisitLocation" value="CG View" />
<input type="hidden" name="commDivFlagPnrCheck" id="commDivFlagPnrCheck" value="1"/>
<input type="hidden" name="duplicateCheck" id="duplicateCheck" value="1"/>
<script type="text/javascript">
  $(document).ready(function () {
      $('#ksn').bind('paste', function (e) {
       var pastedData = e.originalEvent.clipboardData.getData('text');
       var regx =/^[A-Za-z0-9]+$/;
       var rgx=/^[a-zA-Z]+$/;      
       if(!regx.test(pastedData)){      
         e.preventDefault();
       }else if(rgx.test(pastedData)){
         e.preventDefault();
       }
         // alert("You cannot paste text");
      });
   });
    $(document).ready(function () {
      $('#jobId').bind('paste', function (e) {
         var pastedData = e.originalEvent.clipboardData.getData('text');
       var regx =/^[A-Za-z0-9]+$/;
       var rgx=/^[a-zA-Z]+$/;      
       if(!regx.test(pastedData)){      
         e.preventDefault();
       }else if(rgx.test(pastedData)){
         e.preventDefault();
       }
         // alert("You cannot paste text");
      });
   });
</script>