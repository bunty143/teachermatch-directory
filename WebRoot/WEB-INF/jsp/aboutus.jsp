<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<link href="css/quest.css?ver=${resourceMap['css/quest.css']}" rel="stylesheet" type="text/css">
<style type="text/css">
#circulardiv
{
border-radius: 50%;
background-color: #e3e3e3;
width:50%;
padding: 15px;
 behavior: url(PIE.htc);
}
.row b
{
color:black;
}
</style>
<div class="container">
<center>
<div style="font-size: 20px;color:black;margin-top: 20px;">
<b>
<spring:message code="headTeacherMM"/>
</b>
</div><div style="font-size: 12px;margin-top: 10px;">
<b>TeacherMatch</b> <spring:message code="MSG2"/>,<BR/>
<spring:message code="MSG3"/>,<BR/>  
<spring:message code="MSG4"/>:</div>

<div class="row" style="width:80%;margin: auto;margin-top: 20px;">
<div class="col-sm-4 col-md-4">
<div id="circulardiv"><img src="images/man.gif" style="width:80%;"/>
<br/>
</div>
<div>
<b class="font12"><br/>
<spring:message code="MSG5"/><br/>
<spring:message code="MSG6"/><br/>
<spring:message code="MSG7"/>
</b>
</div>
</div>
<div class="col-sm-4 col-md-4">
<div id="circulardiv"><img src="images/home.gif" style="width:80%;"/><br/>
</div>
<div>
<b class="font12"><br/>
<spring:message code="MSG7"/><br/>
<spring:message code="MSGstudentacademic"/><br/>
<spring:message code="MSGachievement"/>
</b>
</div>
</div>
<div class="col-sm-4 col-md-4">
<div id="circulardiv"><img src="images/users.gif" style="width:80%;"/>
<br/>
</div>
<div>
<b class="font12"><br/>
<spring:message code="LBLBoosting"/><br/>
<spring:message code="MSGdistrictschool"/><br/>
<spring:message code="LBLperformance"/>
</b>
</div>
</div>
</div>
<div>
<div style="font-size: 12px;margin-top: 10px;">
<spring:message code="MSGschoolplaceseffective"/>,<br/>
<spring:message code="MSGschoolplaceseffective1"/><br/>
<spring:message code="MSGschoolplaceseffective2"/>!<br/><br/>
<spring:message code="MSGschoolplaceseffective3"/>!

</div>
</div>
</center>
</div>