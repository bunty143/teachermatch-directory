<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page import="tm.utility.Utility,tm.bean.TeacherPersonalInfo,java.lang.String" %>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<script type="text/javascript" src="dwr/interface/AssessmentCampaignAjax.js?ver=${resouceMap['AssessmentCampaignAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type='text/javascript' src="js/assessment-campaign.js?ver=${resouceMap['js/assessment-campaign.js']}"></script>
<script type='text/javascript' src="js/tmcommon.js?ver=${resouceMap['js/tmcommon.js']}"></script>
<script type='text/javascript' src="js/tmhome.js?ver=${resouceMap['js/tmhome.js']}"></script>
<script type='text/javascript' src="js/districtquestioncampaign.js?ver=${resouceMap['js/districtquestioncampaign.js']}"></script>
<script type='text/javascript' src="js/teacher/jobcompletenow.js?ver=${resouceMap['js/teacher/jobcompletenow.js']}"></script>
<script type="text/javascript" src="dwr/interface/DashboardAjax.js?ver=${resouceMap['DashboardAjax.ajax']}"></script>

<!-- Start ... Dynamic Portfolio -->
<script type="text/javascript" src="dwr/interface/DistrictPortfolioConfigAjax.js?ver=${resouceMap['DistrictPortfolioConfigAjax.ajax']}"></script>
<script type='text/javascript' src="js/teacher/districtportfolioconfig.js?ver=${resouceMap['js/teacher/districtportfolioconfig.js']}"></script>
<script type="text/javascript" src="js/teacher/dynamicPortfolio_Common.js?ver=${resouceMap['js/teacher/dynamicPortfolio_Common.js']}"></script>

<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>

<!-- Academics -->
<script type="text/javascript" src="dwr/interface/DWRAutoComplete.js?ver=${resouceMap['DWRAutoComplete.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/PFAcademics.js?ver=${resouceMap['PFAcademics.ajax']}"></script>
<script type='text/javascript' src="js/teacher/dynamicPortfolio_Academics.js?ver=${resouceMap['js/teacher/dynamicPortfolio_Academics.js']}"></script>
<script type='text/javascript' src="js/teacher/dynamicPortfolio_AutoCompAcademics.js?ver=${resouceMap['js/teacher/dynamicPortfolio_AutoCompAcademics.js']}"></script>
 
<!-- Certifications -->
<script type='text/javascript' src="js/teacher/dynamicPortfolio_Certifications.js?ver=${resouceMap['js/teacher/dynamicPortfolio_Certifications.js']}"></script>
<script type="text/javascript" src="dwr/interface/PFCertifications.js?ver=${resouceMap['PFCertifications.ajax']}"></script>
<script type='text/javascript' src="js/teacher/dynamicPortfolio_AutoCompCertificate.js?ver=${resouceMap['js/teacher/dynamicPortfolio_AutoCompCertificate.js']}"></script>

<!-- Resume -->
<script type="text/javascript" src="dwr/interface/PFExperiences.js?ver=${resouceMap['PFExperiences.ajax']}"></script>
<script type='text/javascript' src="js/teacher/dynamicPortfolio_Experiences.js?ver=${resouceMap['js/teacher/dynamicPortfolio_Experiences.js']}"></script>

<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resouceMap['css/base.css']}" />


<script type="text/javascript" src="dwr/interface/StateAjax.js?ver=${resourceMap['StateAjax.js']}"></script>

<!--  Non-Client Jobs -->
<script type="text/javascript" src="dwr/interface/JBServiceAjax.js?ver=${resourceMap['JBServiceAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/interface/QuestAjax.js?ver=${resourceMap['QuestAjax.ajax']}"></script>
<script type="text/javascript" src="js/jobboard/jobboard.js" charset="utf-8"></script>

<script type="text/javascript" src="dwr/interface/DSPQServiceAjax.js?ver=${resourceMap['DSPQServiceAjax.ajax']}"></script>
<script type="text/javascript" src="js/jobapplicationflow/jafstart.js?ver=${resouceMap['js/jobapplicationflow/jafstart.js']}"></script>

 <script src="calender/js/jscal2.js"></script>
<script src="calender/js/lang/en.js"></script>
<link rel="stylesheet" type="text/css" href="calender/css/jscal2.css" />
<link rel="stylesheet" type="text/css" href="calender/css/border-radius.css" />
<link rel="stylesheet" type="text/css" href="calender/css/gold/gold.css" />
 
<c:if test="${jobForTeacher.jobForTeacherId>0}">
<script type="text/javascript">
isJobAplied=true;
</script>
</c:if> 
<script type="text/javascript">   
var $j=jQuery.noConflict();
$j(document).ready(function(){});
var isCoverLetterNeeded = ${isCoverLetterNeeded};
</script>

<!-- End ... Dynamic Portfolio -->

<c:set var="redirectURL" value="" />
<c:choose>
	<c:when test="${fn:contains(header.referer, 'userdashboard.do')}">
		<c:set var="redirectURL" value="userdashboard.do" />
	</c:when>
	<c:when test="${fn:contains(header.referer, 'jobsofinterest.do')}">
		<c:set var="redirectURL" value="jobsofinterest.do" />
	</c:when>
	<c:when test="${fn:contains(header.referer, 'jobsboard.do')}">
		<c:set var="redirectURL" value="${header.referer}" />
	</c:when>
	<c:otherwise>
		<c:set var="redirectURL" value="jobsofinterest.do" />
	</c:otherwise>
</c:choose>
<script type="text/javascript">
    $(function() {
     var hq=$("#haedQId").val();  
      // alert("HeadQID :"+hq);    
    	var data = $("#myDiv").html();
        var limit = 1000;
        var chars = $("#myDiv").text(); 
       
        if(hq==2)
        {   
        }
        else
        {     
        if (chars.length > limit) 
        {        
			document.getElementById('linkVal').innerHTML="Expand";
            var visiblePart = $("<div class='topbar' id='tmpDiv'> "+ data +"</div>");
            var readMore = $("<span class='read-more'></span>");
            $("#myDiv").empty().append(visiblePart).append(readMore);
                $("#linkD").click(function()
                {
					if(document.getElementById('linkVal').innerHTML=="Collapse")
					{
						document.getElementById('linkVal').innerHTML="Expand";
				 		$("#myDiv").empty().append(visiblePart).append(readMore);   
                		$("#tmpDiv").fadeIn();          
					}
					else{
						if($("#linkVal").text()=="Expand")
						{
			              	$("#linkVal").text("Collapse");
			              	$("#myDiv").hide();
			              	$("#myDiv").append(data);
			              	$("#myDiv").fadeIn();
			              	$("#tmpDiv").hide();
						}
					}
	  		});
	  		$("#linkS").click(function()
                {
					if(document.getElementById('linkVal').innerHTML=="Collapse")
					{
						document.getElementById('linkVal').innerHTML="Expand";
				 		$("#myDiv").empty().append(visiblePart).append(readMore);   
                		$("#tmpDiv").fadeIn();          
					}
					else{
						if($("#linkVal").text()=="Expand")
						{
			              	$("#linkVal").text("Collapse");
			              	$("#myDiv").hide();
			              	$("#myDiv").append(data);
			              	$("#myDiv").fadeIn();
			              	$("#tmpDiv").hide();
						}
					}
	  	});
        }
        else
        {
        	//alert("Gagan");
        	//document.getElementById('linkVal').innerHTML="Expand";
        }
        }
        
    });
</script>
<style type="text/css">
.topbar {
	display: block;
	width: 100%;
	height: 45px;
	overflow: hidden;
}
</style>
<input type="hidden" id="jobStatusFlag" value="${jobStatusFlag}" />
<input type="hidden" id="haedQId" value="${HQID}" />
<input type="hidden" id="kesData" value="${kesData}" />
<input type="hidden" id="dID" name="dID" value="${jobOrder.districtMaster.districtId}"/>
<div class="row">  
   <div class="col-sm-4 col-md-4">     
   <div class="subheading" style="margin: 0px;"><spring:message code="lnkApplyForJob" /></div>
   </div> 	             
</div>
<div class="row">
	<div class="col-sm-10 col-md-10">
		<table border="0px" cellpadding="0" cellspacing="0" width="940px">
			<c:set var="emailForTeacher"></c:set>
			<c:set var="districtOrSchoolMessageFlag" value="0"></c:set>
			<c:choose>
				<c:when test="${not empty schoolMaster}">
					<c:set var="emailForTeacher" value="${schoolMaster.emailForTeacher}" />
					<c:if test="${schoolMaster.allowMessageTeacher==1}">
						<c:set var="districtOrSchoolMessageFlag" value="1" />
					</c:if>

						<tr>						
								<td>${schoolMaster.schoolName}</td>
								<td style="width:100px;"><b>Job posting ${jobOrder.jobId}</b></td>
							</tr>
							<tr>
							<td>${schoolMaster.address}</td>
								<c:set var="endDateDis" value=""></c:set>
							<c:choose>
								<c:when test="${endDate eq 'Dec 25, 2099'}">
									<c:set var="endDateDis"><spring:message code="msguntillfilled2" /></c:set>

								</c:when>
								<c:otherwise>
									<c:set var="endDateDis" value="${endDate},11:59 PM CST"></c:set>
								</c:otherwise>
							</c:choose>
						<td style="width: 410px;"><b><spring:message code="lbljobPostedAs" /> ${startDate},<spring:message code="lblDateUntill" /> ${endDateDis}</b></td>
					        </tr>
							<tr>
								<td style="padding-top:10px;padding-bottom:10px;"><img  src="${logoPath}"></td>
								<td align="right"  valign="bottom"  style="padding-bottom:10px;width:50px;">
									<c:set var="description" value="${schoolMaster.description}"/>
									<a href="#" id="linkS"><div id="linkVal"></div></a>
								</td>
							</tr>

							<tr>
								<td colspan="2"  style="text-align: justify;">  
									<div id="myDiv" style="border: 0px solid ;">${description}</div>
								</td>
							</tr>
						</c:when>
						<c:otherwise>
					<c:set var="emailForTeacher" value="${districtMaster.emailForTeacher}"/>
							<c:if test="${districtMaster.allowMessageTeacher==1}">
								<c:set var="districtOrSchoolMessageFlag" value="1"/>
							</c:if>
							<tr>
						<td>
							${districtMaster.districtName}
						</td>
						<td style="width: 100px;">
							<b><spring:message code="lblJobposting" /> ${jobOrder.jobId}</b>
						</td>
					</tr>
					<tr>
						<td>
							${districtAddress}
						</td>
						<c:set var="endDateDis" value=""></c:set>
						<c:choose>
							<c:when test="${endDate eq 'Dec 25, 2099'}">
								<c:set var="endDateDis"><spring:message code="msguntillfilled2" /></c:set>
							</c:when>
							<c:otherwise>
								<c:set var="endDateDis" value="${endDate},11:59 PM CST"></c:set>
								<c:set var="endDateDis" value="${endDateDis}"></c:set>
								
							</c:otherwise>
						</c:choose>
						<td style="width: 410px;">
							<b><spring:message code="lbljobPostedAs" /> ${startDate},<spring:message code="lblDateUntill" /> ${endDateDis}</b>
						</td>
					</tr>
					<tr>
						<td style="padding-top: 10px; padding-bottom: 10px;">
							<img src="${logoPath}">
						</td>
						<td align="right" valign="bottom"
							style="padding-bottom: 10px; width: 50px;">
							<c:set var="description" value="${districtMaster.description}" />
							<a href="#" id="linkD"><div id="linkVal"></div>
							</a>
						</td>

					</tr>
					<tr>
						<td colspan="2" style="text-align: justify;" width="800">
							<input type="hidden" id="getTextType" name="getTextType"
								value="${getTextType}">
							<div id="myDiv" style="border: 0px solid;">
								${description}
							</div>
						</td>
					</tr>
				</c:otherwise>
			</c:choose>
		</table>
	</div>
	<div class="col-sm-2 col-md-2"
		style="text-align: right; padding-right: 20px;">
		<c:choose>
			<c:when test="${districtOrSchoolMessageFlag==1}">
				<a href="#" onclick="return openMessageDiv();"><strong><spring:message
							code="lblContactUs" />
				</strong>
				</a>
			</c:when>
		</c:choose>
	</div>
</div>

 <div class="centerline top10"></div>
<form id="frmApplyJob" name="frmApplyJob" method="Post" action="applynow.do" onsubmit="return checkCL_dynamicPortfolio();">
<input type="hidden" id="jobId" name="jobId" value="${jobOrder.jobId}"/>
<input type="hidden" id="ok_cancelflag" name="ok_cancelflag" value="0"/>
<input type="hidden" id="internalFlag" name="internalFlag" value="${internalFlag}"/>
<input type="hidden" id="ok_cancelflag" name="ok_cancelflag" value="${param.stp}"/>
<input type="hidden" id="isFrmSubmitForJSI" name="isFrmSubmitForJSI" value="${isFrmSubmitForJSI}"/>

		        <div class="row">				
					<div class="col-sm-6 col-md-6 mt10">
					<br/><div class="subheading" style="margin-left: 0px;"><strong><spring:message code="lblJoTil" /></strong></div>					
					${jobOrder.jobTitle}
					</div>
					<div class="mt10 pull-right" style="margin-right: 15px;" id="nextStep" >
					<button class="btn btn-primary" type="button" ><strong><spring:message code="lblApply1" /> <i class="icon"></i></strong></button>
			    	</div>
				</div>
				<c:if test="${jobOrder!=null && jobOrder.districtMaster!=null && jobOrder.districtMaster.districtId == '7800047' && (not empty positionStart)}">
				 <div class="row">				
					<div class="col-sm-6 col-md-6 mt10">
					<div class="subheading" style="margin-left: 0px;"><strong><spring:message code="lblPositionStartDate" /></strong></div>
					${positionStart}
					</div>
				</div>
				</c:if>
				<div class="row">
					<div class="col-sm-6 col-md-6 mt10">
					<div class="subheading" style="margin-left: 0px;"><strong><spring:message code="lblDistrictName" /></strong></div>
					<c:set var="nameToShow"></c:set>
					<c:choose>
						<c:when test="${not empty jobOrder.districtMaster.displayName}">
							${jobOrder.districtMaster.displayName}
							<c:set var="nameToShow" value="${jobOrder.districtMaster.displayName}"></c:set>
						</c:when>
						<c:otherwise>
							${jobOrder.districtMaster.districtName}
							<c:set var="nameToShow" value="${jobOrder.districtMaster.districtName}"></c:set>
						</c:otherwise>
					</c:choose>
					</div>
				</div>
				
				<c:if test="${not empty jeffcoSpecificSchool}">		
						<div class="row">
							<div class="col-sm-6 col-md-6 mt10 subheading" style="margin-left: 15px;">
								<strong>
								<c:choose>
									<c:when test="${jobOrder.districtMaster.districtId==804800}">
										<spring:message code="lblSchoolOrDistrict" />
									</c:when>
									<c:otherwise>
										<spring:message code="lblSchoolName" />
									</c:otherwise>
								</c:choose>
								</strong>
							</div>
						</div>
						${jeffcoSpecificSchool}
				</c:if>
				
				<c:if test="${jobOrder!=null && jobOrder.districtMaster!=null && jobOrder.districtMaster.districtId == '804800'}">
					<c:if test="${not empty jobRequisition.schoolMaster.schoolName}">
						<div class="row">
							<div class="col-sm-6 col-md-6 mt10">
							<div class="subheading" style="margin-left: 0px;"><strong><spring:message code="lblSchoolOrDistrict" /></strong></div><br/>
								${jobRequisition.schoolMaster.schoolName}<br/>
								<c:if test="${not empty jobRequisition.schoolMaster.address}">
									${jobRequisition.schoolMaster.address}
								</c:if>
							</div>
						</div>
					</c:if>
				</c:if>
				<c:if test="${jobOrder!=null && jobOrder.districtMaster!=null && jobOrder.districtMaster.districtId == '804800'}">
					<c:if test="${not empty jobOrder.jobStatus}">
						<div class="row">
							<div class="col-sm-6 col-md-6 mt10">
								<div class="subheading" style="margin-left: 0px;"><strong><spring:message code="lblJobStatus" /></strong></div>
								<c:if test="${jobOrder.jobApplicationStatus == 1}">
									<spring:message code="lblOngoing"/>
								</c:if>
								<c:if test="${jobOrder.jobApplicationStatus == 2}">
									<spring:message code="lblTemporary"/>
								</c:if>
							</div>
						</div>
					</c:if>
				</c:if>
	
				<c:if test="${jobOrder!=null && jobOrder.districtMaster!=null && jobOrder.districtMaster.districtId == '804800'}">
						<div class="row">
						<c:if test="${not empty jobOrder.hoursPerDay}">
							<div class="col-sm-2 col-md-2 mt10">
							<div class="subheading" style="margin-left: 0px;"><strong><spring:message code="lblHourPerDay" /></strong></div>
								${jobOrder.hoursPerDay}
							</div>
						</c:if>
						<c:if test="${not empty jobOrder.fte}">
							<div class="col-sm-2 col-md-2 mt10">
							<div class="subheading" style="margin-left: 0px;"><strong>FTE</strong></div>
								${jobOrder.fte}
							</div>
						</c:if>
						<c:if test="${not empty jobOrder.daysWorked}">
							<div class="col-sm-2 col-md-2 mt10">
							<div class="subheading" style="margin-left: 0px;"><strong><spring:message code="lblDaysWorked" /></strong></div>
								${jobOrder.daysWorked}
							</div>
						</c:if>
						
						<c:if test="${jobOrder.jobCategoryMaster.jobCategoryName eq 'Hourly' || (not empty jobOrder.jobCategoryMaster.parentJobCategoryId && jobOrder.jobCategoryMaster.parentJobCategoryId.jobCategoryName eq 'Hourly')}">
							<c:if test="${not empty jobOrder.fsalary}">
								<div class="col-sm-2 col-md-2 mt10">
								<div class="subheading" style="margin-left: 0px;"><strong><spring:message code="lblSalary" />(Hourly)</strong></div>
								$<fmt:formatNumber type="number" maxFractionDigits="2" value="${jobOrder.fsalary}"/>
								</div>
							</c:if>
						</c:if>
						<c:if test="${jobOrder.jobCategoryMaster.jobCategoryName eq 'Administrator/Professional Technical' || (not empty jobOrder.jobCategoryMaster.parentJobCategoryId && jobOrder.jobCategoryMaster.parentJobCategoryId.jobCategoryName eq 'Administrator/Professional Technical')}">
							<c:if test="${not empty jobOrder.fsalaryA || not empty jobOrder.ssalary}">
								<div class="col-sm-2 col-md-2 mt10">
								<div class="subheading" style="margin-left: 0px;"><strong><spring:message code="lblSalary" />(Yearly)</strong></div>
								$<fmt:formatNumber type="number" maxFractionDigits="0" value="${jobOrder.fsalaryA}"/> - $<fmt:formatNumber type="number" maxFractionDigits="0" value="${jobOrder.ssalary}"/>
								</div>
							</c:if>
						</c:if>
						<c:if test="${not empty jobOrder.payGrade}">
							<div class="col-sm-2 col-md-2 mt10">
							<div class="subheading" style="margin-left: 0px;"><strong><spring:message code="lblPayGrade" /></strong></div>
								${jobOrder.payGrade}
							</div>
						</c:if>
						</div>
				</c:if>
		<%-- Mukesh --%>
		<c:if test="${not empty jobOrder.positionStartDate}">
		<div class="row">
					<div class="col-sm-6 col-md-6 mt10">
					<div class="subheading" style="margin-left: 0px;"><strong>Position Start Date</strong></div>
					${positionStartDate}
					</div>
				</div>
		</c:if>
				
				<div class="row">
					<div class="col-sm-6 col-md-6 mt10">
					<div class="subheading" style="margin-left: 0px;"><strong><spring:message code="lblPostedon" /></strong></div>
					${startDate},12:01 AM CST
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6 col-md-6 mt10">
					<div class="subheading" style="margin-left: 0px;"><strong><spring:message code="lblExpiryDate" /></strong></div>
							

					<c:if test="${endDateDis eq 'filled'}">
						<spring:message code="lblUntil" /> 
					</c:if>
					${endDateDis}
					</div>
				</div>
				<c:if test="${jobCertifications ne ''}">
				<div class="row">
					<div class="col-sm6 col-md-6 mt10">
					<div class="subheading" style="margin-left: 0px;"><strong><spring:message code="lblCertiLice" /></strong></div>
					    ${jobCertifications}
					</div>
				</div>
				</c:if>
				
<!--for strive only	-->
		<c:if test="${schoolType ne '' && schoolType ne null}">
			<div class="row">
				<div class="col-sm-6 col-md-6 subheading" style="margin-left: 15px;">
					<label><spring:message code="lblSchN"/></label>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-6 col-md-6">
						<span>${schoolType}</span>
				</div>
			</div>
		</c:if>
<!--	.........end	-->
				
					<div class="row">
					<div class="col-sm-6 col-md-6 mt10">
							<div class="subheading" style="margin-left: 0px;"><strong><spring:message code="headJobDescrip" /></strong></div>
							<c:if test="${not empty jobOrder.pathOfJobDescription}">
								<input type="hidden" id="jobDesfile" name="jobDesfile" value="${jobOrder.jobId}"/>
								<a href="#" id="iconpophover16" rel="tooltip" data-original-title="Click here to view job description">
									<span id="hrefJobDesc"  class='fa-paperclip icon-large'
										onclick="downloadJobDescription();if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;"></span>
								</a>		
							</c:if>
						</div>
					</div>
					<c:if test="${jobOrder.isPortfolioNeeded==false and teacherDetail.userType=='N' and fn:length(jobOrder.exitURL)>0 and jobOrder.districtMaster.districtId==3702970}">
					
					<c:set var="urlToRedirect" value="${jobOrder.exitURL}"></c:set>
					<c:if test="${fn:indexOf(urlToRedirect, 'http')==-1}">
						<c:set var="urlToRedirect" value="http://${jobOrder.exitURL}"/>
					</c:if>
					
					<c:set var="urlToRedirect" value="https://ats4.searchsoft.net/ats/job_board_frame?softsort=NAME&APPLICANT_TYPE_ID=00000001&COMPANY_ID=00014342"/>
					<spring:message code="msgApplyJob1" /></br>
					<spring:message code="msgApplyJob2" /> ${nameToShow} <spring:message code="msgApplyJob3" /> <a href='${urlToRedirect}' target="_blank"> ${urlToRedirect} </a> <spring:message code="msgApplyJob4" />
					<spring:message code="msgApplyJob5" /> ${nameToShow}
					<br/><br/>
					</c:if>
				
					${jobOrder.jobDescriptionHTML}		
					
					<c:if test="${not empty jobOrder.jobQualificationHTML}">
						 <div class="row">
							<div class="col-sm-6 col-md-6 mt10 subheading" style="margin-left: 15px;">
								<c:choose>
									<c:when test="${jobOrder.districtMaster.districtId eq 804800}">
										<strong><spring:message code="headJobAdditionalInformation"/></strong>
									</c:when>
									<c:otherwise>
										<strong><spring:message code="lblJoQuali" /></strong>
									</c:otherwise>
								</c:choose>
					
							
							</div>
						</div>	
						${jobOrder.jobQualificationHTML}&nbsp;
		
					</c:if>
					
					<c:if test="${jobOrder.districtMaster.districtId eq 804800}">
					<div id='jeffcoURLLINK'>
						<br/>Salary:<br/><a href="http://www.jeffcopublicschools.org/employment/salaries/index.html">http://www.jeffcopublicschools.org/employment/salaries/index.html</a><br/>Benefits:<br/><a href="http://www.jeffcopublicschools.org/employment/benefits/">http://www.jeffcopublicschools.org/employment/benefits/</a><br/><br/>
					</div>
					</c:if>
					
				<c:if test="${epiStandalone}">
				<div class="row">
					<div class ="mt10 col-sm-6 col-md-6"> 

						<a href="portaldashboard.do" style="font-size:13px;"><spring:message code="btnClr" /></a>

						<a href="portaldashboard.do" style="font-size:13px;"><spring:message code="btnClr" /> </a>

					</div>
				</div>
					
				</c:if>
				<br>
				<input type="hidden" id="portfolioAction" name="portfolioAction" value="${portfolioURL}"/>
				<c:set var="btnFlag"  value="0"/>
				<c:if test="${epiStandalone ne true}">	
					<c:choose>						
						<c:when test="${applyJobStatus}">
							<c:set var="btnFlag"  value="1"/>
							
							<div  class="modal hide"  id="myModalCL" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
								<div class="modal-dialog-for-cgmessage"> 
								<div class="modal-content"> 
								<div class="modal-header">
							  		<button type="button" class="close cvrLtrClose" data-dismiss="modal" aria-hidden="true">x</button>
									<c:if test="${jobOrder.districtMaster.districtId ne 806900}">	
										<h3 id="myModalLabel"><spring:message code="DSPQlblCoverLetter"/></h3>
									</c:if>
									<c:if test="${jobOrder.districtMaster.districtId eq 806900}">	
										<h3 id="myModalLabel"><spring:message code="lblCoverLetter"/> <c:out value="${jobOrder.districtMaster.districtName}"/></h3>
									</c:if>
								</div>
								<div class="modal-body hide forKellyCvrLtr" style="max-height: 450px;overflow-y:auto; overflow-x: hidden;">
								<div id="kellYcvRLtrMain">
										<div class="row">
											<div class="col-sm-12 col-md-12"><b>Outside of the TeacherMatch system</b>, have you ever applied to or worked for Kelly Services®?
											</div>
										</div>
										<div class="row">
											<div class="col-sm-12 col-md-12">
												<label class="checkbox-inline">
												  <input type="radio" id="workForKelly1" name="workForKelly"  value="option1"> <spring:message code="lblYes" />
												</label>
												<label class="checkbox-inline">
												  <input type="radio" id="workForKelly2" name="workForKelly" value="option2"><spring:message code="lblNo" />
												</label>
											</div>
										</div>
									</div>
									<!-- next question -->
									<div class="hide" id="kellYnxtQ">
										<div class="row">
											<div class="col-sm-12 col-md-12"><spring:message code="lblThisJobnotavailablemoment2" />
											</div>
										</div>
										<div class="row">
											<div class="col-sm-12 col-md-12">
												<label class="checkbox-inline">
												  <input type="radio" id="contactedKelly1" name="contactedKelly"  value="option1"> <spring:message code="lblYes" />
												</label>
												<label class="checkbox-inline">
												  <input type="radio" id="contactedKelly2" name="contactedKelly" value="option2"><spring:message code="lblNo" />
												</label>
											</div>
										</div>
									</div>
									<!-- end next question -->
									<div class="hide" id="kellYnxtInst">
										<div class="row">
											<div class="col-sm-12 col-md-12"><spring:message code="lblThisJobnotavailablemoment3" />
											</div>
										</div>
									</div>
									<div class="hide" id="kellYnxtDisDivInf">
										<div class="row">
											<div class="col-sm-12 col-md-12" id="kellYnxtDisDivData">
											</div>
										</div>
									</div>
								</div>
								
							
								
						<div class="modal-body forAllDistCvrLtr" style="max-height: 450px;overflow-y:auto; overflow-x: hidden;">
								<div <c:if test="${jobOrder.districtMaster.districtId eq 806900 || jobOrder.districtMaster.districtId eq 1201290}">style="display:none;"</c:if>>
									<p id="cvrltrTxt" class="hide"><spring:message code="msgIfYuApplyCentealOfficePosition" /></p>
										<div class='divErrorMsg' id='errordivCL' style="display: block;"></div>
										<div class='divErrorMsg hide' id='errordivCL1' style="display: block;"></div>
										<div class="top10"> 
										<label class="radio" id="noCoverLetter">
											<input type="radio" value="1" id="rdoCL2" name="reqType"
												onclick='setClBlank()'>
											<spring:message code="msgIdoNotWantToAddCorLetter" />
										</label>
									</div>
									
							<!-- Dhananjay Verma -->
								<div id="divCoverLetterForMartin">	
									<label class="radio">
										<input type="radio" value="1" id="rdoCL1" name="reqType"
											onclick="setCLEnable()">
										<spring:message code="msgPlzTyYurCoverLtr" />
									</label>
									
									</div>
												
									<div id="divCoverLetter">	
										
										<c:if test="${jobOrder.districtMaster.districtId ne 1201290 && jobOrder.districtMaster.districtId ne 806900}">		
											<label class="redtextmsg">
												<spring:message code="msgHowToCopyPastCutDouc" />
											</label>
											
										</c:if>
										
										<c:if test="${jobOrder.districtMaster.districtId ne 1201290 && jobOrder.districtMaster.districtId ne 806900}">
										<textarea id="coverLetter" name="coverLetter" rows="7"	cols="150"></textarea>
										</c:if>
									</div>
									
									<c:if test="${fn:length(latestCoverletter)gt 0}">
										<div class="top10">
											<label class="radio">
												<input type="radio" value="3" id="rdoCL3" name="reqType"
													onclick='setLatestCoverLetter()'>
												<spring:message code="msgApplyJob6" />
											</label>
										</div>
									</c:if>
									
								</div>
								
								
								<!--For jeffco only-->	
								<div class="jeffcoRequired hide">
								<c:if test="${jobOrder.districtMaster.districtId eq 806900}">
									<div class="col-sm-10 col-md-10" style="width:100%;margin-left: -14px;">
									   <p><spring:message code="msgAdamsEmpStatus"/></p>
									   <p>Last 4 digits of your Social Security Number and your month/day of your Date of Birth are required to verify your applicant type. Those fields will only be used by Human Resources to verify if you are a current employee, former employee, or external applicant.</p>
										<div class="col-sm-4 col-md-4">
												<label><spring:message code="lblFname"/><span class="required">*</span></label>
													<input type="text" id="firstNameForAdams" name="firstNameForAdams" class="form-control" style="width:160px !important;" value="${teacherpersonalinfo.firstName}"/>
											</div>
											<div class="col-sm-4 col-md-4" style="margin-left: 38px;">
												<label><spring:message code="lblLname"/><span class="required">*</span></label>
												<input type="text" id="lastNameForAdams" name="lastNameForAdams" class="form-control" style="width:160px !important;" value="${teacherpersonalinfo.lastName}"/>
										</div>
									</div>
								</c:if>
								<div class="col-sm-10 col-md-10" style="width:100%;">
									<label><strong>Date of Birth<span class="required">*</span>
									
									<c:set var="dob" value=""></c:set>
									  <c:if test="${not empty teacherpersonalinfo}">
								  	  <c:set var="dob" value="${teacherpersonalinfo.dob}"></c:set>
									</c:if>
									
									<input type='hidden' name='candidateDOB' value="${dob}"/></strong></label>
									<div>

									<div class="col-sm-4 col-md-4" style="padding:0px;">
									<label><strong>Month<span class="required optionalCss dobRemAst">*</span></strong></label>
									<select class="form-control" id="dobMonth1" name="dobMonth1" onchange="getDay();">
									</select>
									</div>
									
									<div class="col-sm-4 col-md-4" style="padding-left:4px;padding-right:4px;">
									<label><strong>Day<span class="required optionalCss dobRemAst">*</span></strong></label>
									
									<select class="form-control" id="dobDay1" name="dobDay1">
									</select>					
									</div>
									
									<div class="col-sm-4 col-md-4" style="padding:0px;<c:if test="${jobOrder.districtMaster.districtId eq 806900}">display:none;</c:if>">
									<label><strong>Year<span class="required optionalCss dobRemAst">*</span></strong></label>
									<input type="text" id="dobYear1" name="dobYear1" class="form-control" maxlength="4" onkeypress="return checkForInt(event);" onblur="return validateDOB('dobDay1','dobMonth1','dobYear1','errordivCL1');">
									</div>



									</div>
									</div>
									<div class="col-sm-5 col-md-5" style="width:105%;">
									<label style="width:100%;"><strong>Last 4 of SSN<span class="required">*</span></strong></label>
									<c:if test="${not empty teacherpersonalinfo}">
									<c:if test="${jobOrder.districtMaster.districtId eq 806900}">
										<% 
										String SSN=	Utility.decodeBase64(((TeacherPersonalInfo)request.getAttribute("teacherpersonalinfo")).getSSN());
										String last4SSN = "";
										if(!SSN.isEmpty() && SSN!=null){
											last4SSN = SSN.substring(SSN.length()-4);
										}
										%>
									    <input type="text" maxlength="4" id="last4SSN" name="last4SSN" class="form-control" value="<%=last4SSN%>"  style="width:170px;"/>
									</c:if>
									<c:if test="${jobOrder.districtMaster.districtId ne 806900}">
									    <input type="text" maxlength="4" id="last4SSN" name="last4SSN" class="form-control" value="<%= Utility.decodeBase64(((TeacherPersonalInfo)request.getAttribute("teacherpersonalinfo")).getSSN()) %>"  maxlength="50" style="width:170px;"/>
									</c:if>
								</c:if>
									<c:if test="${empty teacherpersonalinfo}">
									    <input type="text" maxlength="4" id="last4SSN" name="last4SSN" class="form-control" value=""  maxlength="50" style="width:170px;"/>
									</c:if>
									</div>
								</div>
								
								<!-- Dhananjay Verma -->
								<c:if test="${jobOrder.districtMaster.districtId ne '1201290'}">
									
								<c:if test="${jobOrder.districtMaster.districtId ne '806810' && jobOrder.districtMaster.districtId ne '7800294'}">
								 <div class="top10 notforJeffco" id="employeeOf">
									<label class="checkbox">
										<input type="checkbox" value="1" id="isAffilated" name="isAffilated" ${(internalFlag==true) ? 'checked' : ''}    ${(internalFlag==true) ? 'disabled' : ''}  onclick="validateCandidateType();">
										<spring:message code="msgApplyJob7" />
										<br />
										
									</label>
								</div>
								<div class="row left25 hide empCvrrLtrDiv notforJeffco">
									<div class="col-sm-5 col-md-5">
									<label><strong><spring:message code="lblEmpNum" /><span class="required">*</span></strong></label>
									
									<c:set var="employeeNumber" value=""></c:set>
									<c:if test="${not empty teacherpersonalinfo}">
									<c:set var="employeeNumber" value="${teacherpersonalinfo.employeeNumber}"></c:set>
									</c:if>
									<input type="text" autocomplete="off" id="empNumCoverLtr" name="empNumCoverLtr" class="form-control" value="${employeeNumber}" maxlength="50"/>
									
									</div>
									<div class="col-sm-5 col-md-5">
									<label><strong><spring:message code="lblZepCode" /><span class="required">*</span></strong></label>
									
									<c:set var="zipCode" value=""></c:set>
									  <c:if test="${not empty teacherpersonalinfo}">
									  <c:set var="zipCode" value="${teacherpersonalinfo.zipCode}"></c:set>
									  </c:if>
									<input type="text" id="zipCodeCoverLtr" autocomplete="off" name="zipCodeCoverLtr" class="form-control" value="${zipCode}" maxlength="10"/>
									</div>
									
								</div>
								<div class="notforJeffco" style="padding-left: 15px;">
									<label class="radio" id="inst" style="display: none;">
										<input type="radio" value="I" name="staffType">
										<spring:message code="msgApplyJob8" />
									</label>
									<label class="radio" id="partinst" style="display: none;">
										<input type="radio" value="PI" name="staffType">
										<spring:message code="msgApplyJob9" />
									</label>
									<label class="radio" id="noninst" style="display: none;">
										<input type="radio" value="N" name="staffType">
										<spring:message code="msgApplyJob10" />
									</label>
								</div>
							</c:if>
							</c:if>
							
							<c:if test="${jobOrder.districtMaster.districtId eq '806810' || jobOrder.districtMaster.districtId eq '7800294'}">
								<input type="hidden" id="isaffiliateds" name="isaffiliated">
								<input type="hidden" id="staffType" name="staffType">
								
								<div>
							       
							     
									<b>Employee Status Check</b><span class="required">*</span>
									<div class="row">
										<div class="col-sm-9 col-md-9">
											<label class="span6 radio" style="margin-top: 2px;margin-bottom: 2px;">
									    		<input type="radio" id="isAffilated" name="isAffilated" value="2"  onclick="showEmpNoDiv(1);"/><spring:message code="msgDp_commons13" /> <span id='displayDistrictNameForExt1'>${districtDName}</span>
									    	</label>
									    </div>	
									</div>
								    	<div id="fe1Div" class="hide">
									    	<div class="row left25">
										        <div class="col-sm-4 col-md-4 hide">
													<label id="lblempnumber"><strong><spring:message code="lblEmpNum" /></strong></label>
													<input type="text" id="empfe1" class="form-control" maxlength="50" onkeypress="return checkForIntEmpNum(event);"/>
												</div>
												
												<!--Name While Employed-->
												<div id="locationDiv" class="col-sm-4 col-md-4 hide" >
													<label><strong>Position</strong></label>
													<input type="text" id="empfe6" class="form-control" maxlength="50"/>
												</div>
												<div id="nameDiv" class="col-sm-5 col-md-5 hide" style="">
													<label><strong><spring:message code="lblFirstName" /></strong></label>
													<input type="text" id="f_namee" class="form-control" maxlength="50"/>
												</div>
												<div id="nameDiv" class="col-sm-5 col-md-5 hide" style="">
													<label><strong><spring:message code="lblLastName" /></strong></label>
													<input type="text" id="l_namee" class="form-control" maxlength="50"/>
												</div>
												
												<div id="nameDiv" class="col-sm-6 col-md-6" style="">
													<label><strong><spring:message code="lblNameWhileEmployed" /></strong></label>
													<input type="text" id="nameWhileEmployed" class="form-control" maxlength="50"/>
												</div>
												
												<!--end of Name While Employed-->
											</div>
											<div class="row left25 hide" id="fePosDiv">
									    		<div class="col-sm-6 col-md-6">
													<label id="lblempposition"><strong><spring:message code="lblEmpPosition" /><span class="required">*</span></strong></label>
													<input type="text" id="emppos" class="form-control" maxlength="50" onkeypress="" value="${teacherpersonalinfo.lastPositionWhenEmployed}"/>
												</div>
									    	</div>
											
											  <div class="row left25 ntPhiLfeild">
											      <div class="col-sm-12 col-md-12 hide">
													<label class="checkbox inline" style="margin-top: 2px;margin-bottom: 2px;">
														<input type="checkbox" id="empchk11" onclick="showEmpDate(0);"/>
								            			<spring:message code="msgDp_commons14" />
								           				</label> 
									           		</div>	
									           		 <div class="col-sm-3 col-md-3 left25">
									           			<div class='mt5 hide' id='rtDateDiv'>
									           				<label id="lblRetirementDate1"><strong><spring:message code="lblRetirementDate" /></strong></label>
															<input type="text" id="rtDate" name="rtDate" class="form-control" maxlength="4"/>
									           			</div>
													</div>
												</div>
												<div class="row left25 ntPhiLfeild">
												    <div class="col-sm-12 col-md-12 hide">
													<label class="checkbox inline" style="margin-top: 2px;margin-bottom: 2px;">
														<input type="checkbox" id="empchk12"  onclick="showEmpDate(1);"/>
								            			<spring:message code="msgDp_commons15" />
								            		</label>
								            		</div>	
													<div class="col-sm-3 col-md-3 left25">
								           			<div class='mt5 hide' id='wtDateDiv'>
								           				<label><strong><spring:message code="lblDateofwithdrawn" /></strong></label>
														<input type="text" id="wdDate" name="wdDate" class="form-control" maxlength="4"/>
								           			</div>
													</div>
												</div>
												
						           		</div>
								
								         <div class="row">
									
										     <div class="col-sm-9 col-md-9">
												<label class="span6 radio" style="margin-top: 2px;margin-bottom: 2px;">
													<input type="radio" id="isAffilated" name="isAffilated" value="1" onclick="showEmpNoDiv(2);"/><spring:message code="msgDp_commons17" /> <span id='displayDistrictNameForExt2'>${districtDName}</span>
												</label>
											 </div>	
										</div>
										
										 <div id="fe2Div" class="hide">
											<div class="row left25">
												<div class="col-sm-4 col-md-4 hide">
													<label><strong><spring:message code="lblEmpNum" /><span class="required">*</span></strong></label>
													<input type="text" id="empfe2" class="form-control" maxlength="50" onkeypress="return checkForIntEmpNum(event);"/>
												</div>
												<div id="positionfe2Div" class="col-sm-4 col-md-4">
													<label><strong>Location</strong></label>
													<input type="text" id="empfe2location" class="form-control" maxlength="50"/>
												</div>
												
												<div id="locationfe2Div" class="col-sm-4 col-md-4">
													<label><strong>Position</strong></label>
													<input type="text" id="empfe2position" class="form-control" maxlength="50"/>
												</div>
												<div class="col-sm-3 col-md-3 hide" id="senNumDiv">
													<label><strong><spring:message code="msgSeniorityNumber" /><span class="required"></span></strong></label>
													<input type="text" id="seniorityNumb" class="form-control" maxlength="50"/>
												</div>
						
												<div class="col-sm-5 col-md-5 top20 hide" id="jeffcoSeachDiv">
													<button class="btn btn-large btn-primary" type="button" onclick="openEmployeeNumberInfo();"><strong>Employee Information<i class="icon"></i></strong></button>
												</div>
											</div>
											<div class="row left25 col-sm-4 col-md-4 hide" id="districtEmailDiv">					
												<label><strong>District Email Address</strong><span class="required">*</span></label>
												<input type="text" id="districtEmail" class="form-control" value="${teacherpersonalinfo.districtEmail}"/>
											</div>
										
										 <!--
										<div class="span6">
											<label class="checkbox inline">
												<input type="checkbox" id="empchk2"/>
							            		I am current full time teacher
							           		</label>
						           		</div>
						           		 -->
						           		<div class="row left25">
						           		 <div class="col-sm-9 col-md-9 optionalCss hide">
													<label class="span6 radio">
														<input type="radio" id="rdCEmp1" name="rdCEmp" /><spring:message code="msgDp_commons18" />
													</label>
													<c:set var="showVal"  value="none"/>
													<c:if test="${isMiami}">
													<c:set var="showVal"  value="block"/>
													</c:if>
													<label class="span7 radio p0 left20" style="display: ${showVal};" id="partInsEmp">
														<input type="radio" id="rdCEmp3" name="rdCEmp" /><spring:message code="msgDp_commons19" />
													</label>
													<label class="span6 radio">
												    <input type="radio" id="rdCEmp2" name="rdCEmp" /><spring:message code="msgDp_commons20" />
											         </label>
										</div>
										</div>   		
						           		
						           		</div>
						           		
						           		
										<div class="row" id="fe3Div">
												<div class="col-sm-9 col-md-9">
													<label class="span15 radio" style="margin-top: 2px;margin-bottom: 2px;">
													<input type="radio" id="isAffilated" name="isAffilated" value="3" onclick="showEmpNoDiv(6);"/><spring:message code="msgDp_commons60" /> &nbsp;<span >Colorado's Public Employers Retirement Association (PERA)</span>
												</label>
												</div>
											
											<div id="position6Div" class="col-sm-5 col-md-5 left25 hide" style="">
													<label><strong>Approximate Retirement Date</strong></label>
													 <input type="text" id="empaproretdatefe6" name="empaproretdatefe6" class="help-inline form-control">
											</div>
									</div>
									
								
						
								
								<div class="row">
										<div class="col-sm-9 col-md-9">
											<label class="span15 radio" style="margin-top: 2px;margin-bottom: 2px;">
											<input type="radio" id="isAffilated" name="isAffilated" value="0" onclick="showEmpNoDiv(3);"/><spring:message code="msgDp_commons21" /> <span id='displayDistrictNameForExt3'>${districtDName}</span>
										</label>
									</div>
								</div>
								
								<div class="row" id="fe5Div">
										<div class="col-sm-9 col-md-9">
											<label class="span15 radio" style="margin-top: 2px;margin-bottom: 2px;">
											<input type="radio" id="isAffilated" value="5" name="isAffilated" onclick="showEmpNoDiv(5); validateCandidateType()"/>I am employed as a substitute teacher for <span id='displayDistrictNameForExt5'>${districtDName}</span>
										</label>
									</div>
								</div>
							</div>
						</c:if>
						
						
						<!-- Dhananjay Verma 20-02-2016 -->
						<c:if test="${jobOrder.districtMaster.districtId eq 1201290}">
														
									<div class='divErrorMsg' id='errordivMartin' style="display: block;"></div>
								
								</c:if>
						
						<c:if test="${jobOrder.districtMaster.districtId eq '1201290'}">
								<input type="hidden" id="isaffiliateds" name="isaffiliated">
								<input type="hidden" id="staffType" name="staffType">
								
								
								<c:if test="${jobOrder.jobCategoryMaster.jobCategoryName eq 'Administrative Non-Instructional' || jobOrder.jobCategoryMaster.jobCategoryName eq 'Administrative-Instructional'}">
								
									<p id="cvrltrTxt" class="hide"><spring:message code="msgIfYuApplyCentealOfficePosition" /></p>
										<div class='divErrorMsg' id='errordivCL' style="display: block;"></div>
										<div class='divErrorMsg hide' id='errordivCL1' style="display: block;"></div>
										
										<!-- Dhananjay Verma -->
								<div id="divCoverLetterForMartin">	
									<label class="radio">
										<input type="radio" value="YCL" id="rdoCL2ForMartinAdmin" name="reqType" onclick="setCLEnableForMartinAdmin()">
										<spring:message code="msgPlzTyYurCoverLtr" />
									</label>
									
									</div>

									<div id="divCoverLetterAdmin" class="hide">	
										
											<label class="redtextmsg">
												<spring:message code="msgHowToCopyPastCutDouc" />
											</label>
										
										<textarea id="coverLetter" name="coverLetter" rows="7"	cols="150"></textarea>
										
									</div>
									
									<c:if test="${fn:length(latestCoverletter)gt 0}">
										<div class="top10">
											<label class="radio">
												<input type="radio" value="3" id="rdoCL3" name="reqType"
													onclick='setLatestCoverLetterForMartinAdmin()'>
												<spring:message code="msgApplyJob6" />
											</label>
										</div>
									</c:if>
								</c:if>
								
								<div>
							        
									<b>Employee Status Check</b><span class="required">*</span>
									
									<!-- Current Employee -->
									 <div class="row">
									
										     <div class="col-sm-9 col-md-9">
												<label class="span6 radio" style="margin-top: 2px;margin-bottom: 2px;">
													<input type="radio" id="isAffilated" name="isAffilated" value="1" onclick="showEmpNoDiv('CEmpOfMartin');"/><spring:message code="msgDp_commons17" /> <span id='displayDistrictNameForExt2'>${jobOrder.districtMaster.districtName}</span>
												</label>
											 </div>	
										</div>
										
										
											<div class="row left25">
												
											<div id="currentEmployeeOfMartin" class="hide">
											
												<div id="currentEmployeeOfMartinJT" class="col-sm-4 col-md-4">
													<label><strong><spring:message code="lblJT" /> </strong></label>
													<input type="text" id="currentempjobtitle" class="form-control" maxlength="50"/>
												</div>
												
												<div id="currentEmployeeOfMartinLoc" class="col-sm-4 col-md-4">
													<label><strong><spring:message code="lblLoc" /></strong></label>
													<input type="text" id="currentemplocation" class="form-control" maxlength="50"/>
												</div>
												
												<div id="currentEmployeeOfMartinYrInLoc" class="col-sm-4 col-md-4">
													<label><strong><spring:message code="lblYearInLoc" /></strong></label>
													<input type="text" id="currentempyearinlocation" class="form-control" maxlength="50"/>
												</div>
												
												<div id="currentEmployeeOfMartinSupervisor" class="col-sm-4 col-md-4">
													<label><strong><spring:message code="lblSupervisor" /></strong></label>
													<input type="text" id="currentempsupervisor" class="form-control" maxlength="50"/>
												</div>
											</div>
											</div>
						           		<!-- End Current Employee -->
						           		
									
									<!-- Formal Employee -->
									<div class="row">
										<div class="col-sm-9 col-md-9">
											<label class="span6 radio" style="margin-top: 2px;margin-bottom: 2px;">
									    		<input type="radio" id="isAffilated" name="isAffilated" value="2"  onclick="showEmpNoDiv('FormalEmpOfMartin');"/><spring:message code="msgDp_commons13" /> <span id='displayDistrictNameForExt1'>${jobOrder.districtMaster.districtName}</span>
									    	</label>
									    </div>	
									</div>
								    	
								    	
									    	<div class="row left25">
												
											<div id="formalEmployeeOfMartin" class="hide">
												<div id="formalEmployeeOfMartinJT" class="col-sm-4 col-md-4">
													<label><strong><spring:message code="lblJT" /> </strong></label>
													<input type="text" id="fromalempjobtitle" name="fromalempjobtitle" class="form-control" maxlength="50"/>
												</div>
												
												<div id="formalEmployeeDatesOfEmployee" class="col-sm-4 col-md-4">
													<label><strong><spring:message code="datesofemp" /></strong></label>
													<input type="text" id="formalempdatesofemployee" name="formalempdatesofemployee" class="form-control" maxlength="50"/>
												</div>
												
												<div id="formalEmployeeOfMartinNameWhileEmployee" class="col-sm-4 col-md-4">
													<label><strong><spring:message code="lblNameWhil" /></strong></label>
													<input type="text" id="formalempnamewhileemployee" name="formalempnamewhileemployee" class="form-control" maxlength="50"/>
												</div>
												
												
											</div>
											</div>
						           		<!-- End Formal Employee -->
						           		
						           		<!--  Retired Employee -->
										<div class="row">
												<div class="col-sm-9 col-md-9">
													<label class="span15 radio" style="margin-top: 2px;margin-bottom: 2px;">
													<input type="radio" id="isAffilated" name="isAffilated" value="3" onclick="showEmpNoDiv('RetiredEmpOfMartin');"/><spring:message code="msgDp_commons62" /> &nbsp;
												</label>
												</div>
											</div>
											
												<div class="row left25">
											<div id="retiredEmployeeStateofFlorida" class="hide">	
												<div id="retiredEmployeeJT" class="col-sm-4 col-md-4">
													<label><strong><spring:message code="dateofretirement" /> </strong></label>
													<input type="text" id="retiredempdateofretirement"  name="retiredempdateofretirement" class="form-control" maxlength="50"/>
													
												</div>
											</div>
											</div>
											<!-- End Retired Employee -->
									
									<!-- Never Employee Of Martin District -->
									<div class="row">
										<div class="col-sm-9 col-md-9">
											<label class="span15 radio" style="margin-top: 2px;margin-bottom: 2px;">
											<input type="radio" id="isAffilated" name="isAffilated" value="0" onclick="showEmpNoDiv('never_emp');"/><spring:message code="msgDp_commons21" /> <span id='displayDistrictNameForExt3'>${districtDName}</span>
										</label>
									</div>
								</div>
								<!-- End  Never Employee Of Martin District-->
								
								
								<!-- Substitute Of Martin -->
								<div class="row">
										<div class="col-sm-9 col-md-9">
											<label class="span15 radio" style="margin-top: 2px;margin-bottom: 2px;">
											<input type="radio" id="isAffilated" value="5" name="isAffilated" onclick="showEmpNoDiv('CurrentSubstituteEmpOfMartin'); "/>I am a current substitute employee for <span id='displayDistrictNameForExt5'>${jobOrder.districtMaster.districtName}</span>
										</label>
									</div>
									
									</div>
									
										<div class="row left25">
											<div id="currentSubstituteEmployee" class="hide">			
												<div id="currentSubsEmpJT" class="col-sm-4 col-md-4">
													<label><strong><spring:message code="lblJT" /> </strong></label>
													<input type="text" id="currentsubstitutejobtitle" class="form-control" maxlength="50"/>
												</div>
												
									</div>
								</div>
								<!-- End Substitute Of Martin -->
								
							</div>
						</c:if>
						<!-- End Martin -->
						</div>	
							<div class="modal-footer">
								<button class="btn btn-primary" type="button"
									onclick="checkCL_dynamicPortfolio();">
									<strong><spring:message code="btnConti" /> <i
										class="icon"></i>
									</strong>
								</button>
								<button class="btn" data-dismiss="modal" aria-hidden="true">
									<spring:message code="btnClose" />
								</button>
							</div>
						</div>
					</div>
				</div>
				
				
				

				<div class="row">
					<div class="col-sm-6 col-md-6" id="applyToNext">
						<c:choose>
							<c:when
								test='${jobOrder.districtMaster.questDistrict eq 1  && jobOrder.districtMaster.jobApplicationCriteriaForProspects ne 0}'>
								<button class="btn btn-primary" type="button"
									onclick="chkApplyJobNonClientDashBoard('${jobOrder.jobId}','${jobOrder.districtMaster.jobApplicationCriteriaForProspects}','${jobOrder.exitURL}','${jobOrder.districtMaster.districtId}','');">
									<strong><spring:message code="lblApply1" /> <i
										class="icon"></i>
									</strong>
								</button>
							
								
								&nbsp;&nbsp;<a href="${redirectURL}" style="font-size:13px;"><spring:message
										code="btnClr" />
								</a>
								<br>
							</c:when>
							<c:otherwise>
								<button class="btn btn-primary" type="button"
									onclick="callDynamicPortfolio();">
									<strong><spring:message code="lblApply1" /> <i
										class="icon"></i>
									</strong>
								</button>
								
								&nbsp;&nbsp;<a href="${redirectURL}" style="font-size:13px;"><spring:message
										code="btnClr" />
								</a>
								<br>
							</c:otherwise>
						</c:choose>
					</div>
				</div>
			</c:when>
			<c:when test="${portfolioStatus}">
				<c:set var="btnFlag" value="1" />
				<div class="span10 mt10" id="applyToNext">
					<button class="btn btn-primary" type="button"
						onclick="window.location.href='${portfolioURL}'">
						<strong><spring:message code="btnSubmitMyCredentials" />
							<i class="icon"></i>
						</strong>
					</button>
					&nbsp;&nbsp;
					<a href="${redirectURL}" style="font-size:13px;"><spring:message code="btnClr" />
					</a>
				</div>
			</c:when>
			<c:when test="${startBaseAssessStatus}">
				<c:set var="btnFlag" value="1" />
				<div class="row">
					<div class="mt10 col-sm-6 col-md-6" id="applyToNext">
						<c:choose>
							<c:when
								test="${jobForTeacher.jobForTeacherId>0 and jobForTeacher.flagForDistrictSpecificQuestions eq null}">
								<button class="btn btn-primary" type="button"
									onclick="getTeacherCriteria(${jobOrder.jobId});">
									<strong><spring:message code="btmStartBaseInventory" />
										<i class="icon"></i>
									</strong>
								</button>&nbsp;&nbsp;<a href="${redirectURL}" style="font-size:13px;"><spring:message
										code="btnClr" />
								</a>
							</c:when>
							<c:otherwise>
								<button class="btn btn-primary" type="button"
									onclick="checkInventory(0,${jobOrder.jobId});">
									<strong><spring:message code="btmStartBaseInventory" />
										<i class="icon"></i>
									</strong>
								</button>&nbsp;&nbsp;<a href="${redirectURL}" style="font-size:13px;"><spring:message
										code="btnClr" />
								</a>
							</c:otherwise>
						</c:choose>
					</div>
				</div>
			</c:when>
			<c:when test="${resumeBaseAssessStatus}">
				<c:set var="btnFlag" value="1" />
				<div class="row">
					<div class="mt10 col-sm-6 col-md-6" id="applyToNext">
						<c:choose>
							<c:when test="${jobForTeacher.jobForTeacherId>0}">
								<button class="btn btn-primary" type="button"
									onclick="getTeacherCriteria(${jobOrder.jobId});">
									<strong><spring:message code="btnResumeBaseInventory" />
										<i class="icon"></i>
									</strong>
								</button>&nbsp;&nbsp;<a href="${redirectURL}" style="font-size:13px;"><spring:message
										code="btnClr" />
								</a>
							</c:when>
							<c:otherwise>
								<button class="btn btn-primary" type="button"
									onclick="checkInventory(0,${jobOrder.jobId});">
									<strong><spring:message code="btnResumeBaseInventory" />
										<i class="icon"></i>
									</strong>
								</button>&nbsp;&nbsp;<a href="${redirectURL}" style="font-size:13px;"><spring:message
										code="btnClr" />
								</a>
							</c:otherwise>
						</c:choose>
					</div>
				</div>
			</c:when>
			<c:when test="${startJobSpecAssessStatus}">
				<c:set var="btnFlag" value="1" />
				<div class="row">
					<div class="mt10 col-sm-6 col-md-6" id="applyToNext">
						<c:choose>
							<c:when
								test="${jobForTeacher.jobForTeacherId>0 and jobForTeacher.flagForDistrictSpecificQuestions eq null}">
								<button class="btn btn-primary" type="button"
									onclick="getTeacherCriteria(${jobOrder.jobId});">
									<strong><spring:message
											code="btnStartJobSpecificInventory" /> <i class="icon"></i>
									</strong>
								</button>&nbsp;&nbsp;
									</c:when>
							<c:otherwise>
								<button class="btn btn-primary" type="button"
									onclick="checkInventory(${jobOrder.jobId},null);">
									<strong><spring:message
											code="btnStartJobSpecificInventory" /> <i class="icon"></i>
									</strong>
								</button>&nbsp;&nbsp;
									</c:otherwise>
						</c:choose>

						<c:choose>
							<c:when test="${fn:contains(redirectURL, 'applyjob.do')}">
								<a href="jobsofinterest.do" style="font-size:13px;"><spring:message code="btnClr" />
								</a>
								<br>
							</c:when>
							<c:otherwise>
								<a href="${redirectURL}"><spring:message code="btnClr" />
								</a>
								<br>
							</c:otherwise>
						</c:choose>
					</div>
				</div>
			</c:when>
			<c:when test="${resumeJobSpecAssessStatus}">
				<c:set var="btnFlag" value="1" />
				<div class="row">
					<div class="mt10 col-sm-6 col-md-6" id="applyToNext">
						<c:choose>
							<c:when
								test="${jobForTeacher.jobForTeacherId>0 and jobForTeacher.flagForDistrictSpecificQuestions eq null}">
								<button class="btn btn-primary" type="button"
									onclick="getTeacherCriteria(${jobOrder.jobId});">
									<strong><spring:message
											code="btnResumeJobSpecificInventory" /> <i class="icon"></i>
									</strong>
								</button>&nbsp;&nbsp;
									</c:when>
							<c:otherwise>
								<button class="btn btn-primary" type="button"
									onclick="checkInventory(${jobOrder.jobId},null);">
									<strong><spring:message
											code="btnResumeJobSpecificInventory" /> <i class="icon"></i>
									</strong>
								</button>&nbsp;&nbsp;
									</c:otherwise>
						</c:choose>
					</div>
				</div>
			</c:when>
		</c:choose>

		<c:if
			test="${(jobForTeacher.jobForTeacherId>0 and jobForTeacher.status.statusShortName=='icomp' &&  btnFlag==0)}">
			<div class="row">
				<div class="mt10 col-sm-6 col-md-6" id="applyToNext">
					<button class="btn btn-primary" type="button"
						onclick="getTeacherCriteria(${jobOrder.jobId});">
						<strong><spring:message code="btnConti"/> <i class="icon"></i>
						</strong>
					</button>
					&nbsp;&nbsp;
				</div>
			</div>
		</c:if>
	</c:if>
</form>

<div class="modal hide" id="myModalDASpecificQuestions" tabindex="-1"
	role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
	data-backdrop="static">
	<div class="modal-dialog-for-cgdemoschedule">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">
					x
				</button>
				<h3 id="myModalLabel">
					<spring:message code="headMandatoryDecler"/>
				</h3>
			</div>
			<div class="modal-body"
				style="max-height: 450px; overflow-y: auto; overflow-x: hidden;">
				<span id='textForDistrictSpecificQuestions' class="divlablevalins"><spring:message code="msgDistReqAllAnsOfQues"/></span>
				<div class='divErrorMsg' id='errordiv4question'
					style="display: block; padding-bottom: 7px;"></div>
				<table width="100%" border="0"
					class="table table-bordered table-striped" id="tblGrid">
				</table>
			</div>
			<div class="modal-footer">
				<c:if test="${jobForTeacher.jobForTeacherId>0}">
					<input type="hidden" id="isApplied" name="isApplied" />
				</c:if>

				<c:choose>
					<c:when test="${jobForTeacher.jobForTeacherId>0}">
						<button class="btn btn-primary" type="button"
							onclick="setDistrictQuestions('dashboard');">
							<strong><spring:message code="btnConti"/> <i class="icon"></i>
							</strong>
						</button>
					</c:when>
					<c:otherwise>
						<button class="btn btn-primary" type="button"
							onclick="setDistrictQuestions('apply');">
							<strong><spring:message code="btnConti"/> <i class="icon"></i>
							</strong>
						</button>
					</c:otherwise>
				</c:choose>
				<button class="btn" data-dismiss="modal" aria-hidden="true"
					onclick="cancelDSQdiv()">
					<spring:message code="btnClose"/>
				</button>
			</div>
		</div>
	</div>
</div>


<input type="hidden" id="completeNow" name="completeNow" value="0"/>
<input type="hidden" id="inventory" name="inventory" value=""/>
<input type="hidden" id="portfolioStatus" name="portfolioStatus" value=""/>
<input type="hidden" id="prefStatus" name="prefStatus" value=""/>	
<input type="hidden" id="isaffilatedstatushiddenId" name="isaffilatedstatushiddenId" value="0"/>
<input type="hidden" id="savejobFlag" name="savejobFlag" value="0"/>
<input type="hidden" id="qqFlag" name="qqFlag" value="0"/>

<div class="modal hide"  id="divAlert" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" style="z-index: 5000;" >
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" id='vcloseBtnk'>x</button>
		<h3 id="myModalLabel">TeacherMatch</h3><input type="hidden" id="isresetStatus" name="isresetStatus" value="0"><input type="hidden" id="isAffilatedValue" name="isAffilatedValue" value="0">
	</div>
	<div class="modal-body">				
		<div class="control-group">
			<div class="" id="divAlertText" >
			</div>
		</div>
 	</div> 	
 	<div class="modal-footer"> 	
 		<button class="btn btn-primary" aria-hidden="true" onclick="resetjftIsAffilated()" ><strong><spring:message code="lblYes" /> <i class="icon"></i></strong></button>&nbsp;
 		<span><button class="btn btn-primary" data-dismiss="modal" aria-hidden="true" onclick="canceljftIsAffilated()"><strong><spring:message code="lblNo" /> <i class="icon"></i></strong></button></span> 
 		<span><button class="btn" data-dismiss="modal" aria-hidden="true" ><spring:message code="lnkCancel" /></button></span>		
 	</div>
</div>
</div>
</div>

<div style="display:none;" id="loadingDiv">
    <table  align="center">
 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
 		<tr id='paymentMessage'><td style='padding-top:0px;padding-left:450px;' id='spnMpro' align='center'></td></tr>
	</table>
</div>
<div style="display:none;" id="loadingDivInventory">
	 <table  align="center" >
 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
 		<tr id='paymentMessage'><td style='padding-top:0px;' id='spnMpro' align='center'><spring:message code="lblInventBengLoaded" /></td></tr>
	</table>
</div>

<div class="modal hide" id="myModalApplyJob" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog-for-cgmessage">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">
					x
				</button>
				<h3 id="myModalLabel">
					<spring:message code="lblSendMessageToAdministrator"/>
				</h3>
			</div>
			<iframe id='uploadFrameID' name='uploadFrame' height='0' width='0'
				frameborder='0' scrolling='yes' sytle='display:none;'>
			</iframe>

			<div class="modal-body" style="max-height: 450px; overflow-y: auto;">

				<form id='frmMessageUpload' enctype='multipart/form-data'
					method='post' target='uploadFrame' class="form-inline"
					onsubmit="return validateMessage();" accept-charset="UTF-8">
					<input type='hidden' id="emailForTeacher" name="emailForTeacher"
						value="${emailForTeacher}" />
					<div class="control-group">
						<div class='divErrorMsg' id='errordivMessage'
							style="display: block;"></div>
					</div>
					<div class="control-group">
						<div class="">
							<label>
								<strong><spring:message code="lblSub"/></strong><span class="required">*</span>
							</label>
							<br />
							<input id="msgMailSubject" name="msgMailSubject" type="text"
								class="form-control" maxlength="80" />
						</div>
					</div>

					<div class="control-group">
						<div class="span8" id="msgMailSpt">
							<label>
								<strong><spring:message code="lblMsg"/></strong><span class="required">*</span>
							</label>
							<label class="redtextmsg">
								<strong><spring:message code="msgHowToCopyPastCutDouc"/> </strong>
							</label>
							<textarea rows="5" cols="" id="msgMail" name="msgMail"
								maxlength="1000"></textarea>
							<div id='jobFile1' style="padding-top: 8px;">
								<a href='javascript:void(0);' onclick='addJobFileType();'><img
										src='images/attach.png' />
								</a>
								<a href='javascript:void(0);' onclick='addJobFileType();'><spring:message code="lnkAttachFile"/></a>
							</div>
						</div>
					</div>
					<div class="controls" style="margin-bottom: 15px;">
						<spring:message code="lblwhatis"/>
						<img id="imgCaptcha" height="30px" width="170px" src="verify.png" />
						<img src="images/refresh.png" style="cursor: pointer;"
							onclick="changeCaptcha('sumOfCaptchaText')" />
						<label>
							<strong><spring:message code="msgSumTwoNumber"/></strong>
						</label>
						<input id="sumOfCaptchaText" type="text" class="form-control"
							placeholder="sum of number"
							onkeypress="return checkForInt(event);" maxlength="2" />
					</div>

					<div id="lodingJobImg"
						style='display: none; text-align: center; padding-top: 4px;'>
						<img src="images/loadingAnimation.gif" />
						<spring:message code="msgMsgSending"/>
					</div>
				</form>
			</div>

			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true">
					<spring:message code="btnClr"/>
				</button>
				<button class="btn btn-primary" onclick="saveMessage()">
					<spring:message code="btnSend"/>
				</button>
			</div>
		</div>
	</div>
</div>
<div class="modal hide" id="myModalApplyJob2" tabindex="-1"
	role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">
					x
				</button>
				<h3 id="myModalLabel">
					TeacherMatch
				</h3>
			</div>
			<div class="modal-body">
				<div class="control-group" id='messageshow'>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true">
					<spring:message code="btnOk"/>
				</button>
			</div>
		</div>
	</div>
	<div class="modal-body">
		<div class="control-group" id='messageshow'>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true">Ok</button>
 	</div>
</div>
</div>
</div>

<input type="hidden" name="tempjobid" id="tempjobid" />
<input type="hidden" name="exitUrl" id="exitUrl" />
<input type="hidden" name="district" id="district" />
<input type="hidden" name="school" id="school" />
<input type="hidden" name="criteria" id="criteria" />
<input type="hidden" name="epistatus" id="epistatus" />

<div class="modal hide" id="applyJobComplete" tabindex="-1"
	role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
	style="z-index: 5000;" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">
					x
				</button>
				<h3>
					Teachermatch
				</h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<div class="">
						<span><b><spring:message code="msgSucceApldJob"/></b>
						</span>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true"
					onclick="hideUserInfoBlog();">
					<spring:message code="btnOk"/>
				</button>
			</div>
		</div>
	</div>
</div>
<!-- Gaurav Kumar -->
<div  class="modal hide"  id="columbusSetting" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
	<div class="modal-dialog-for-cgmessage"> 
				<div class="modal-content"> 
				<div class="modal-header">
			  		<button type="button" class="close columbusSettingBtn" data-dismiss="modal" aria-hidden="true">x</button>
					<h3 id="myModalLabel"><spring:message code="lblColumbusSetting"/></h3>
				</div>
			<div class="modal-body" style="max-height: 450px;overflow-y:auto; overflow-x: hidden;">
				<div class='divErrorMsg' id='errordivColumbus' style="display: block;"></div>
				<div class="row">  
			 	  <div class="col-sm-6 col-md-6">     
			   		<spring:message code="lblFrDate"/> :<input type="text" name="columbus_date1" id="columbus_date1" class="form-control">
			   	  </div> 
			   	   <div class="col-sm-6 col-md-6">     
			   		<spring:message code="lblToDate"/> :<input type="text" name="columbus_date2" id="columbus_date2" id="columbus_date1" class="form-control">
			   	  </div>
			   	  </div>
			   	  <div class="row"> 
			   	   <div class="col-sm-6 col-md-6">     
			   		<spring:message code="lblPosition"/> :<input type="text" name="columbus_position" id="columbus_position" class="form-control">
			   	  </div>	             
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-primary" type="button" id="columbusBtnSave">
					Save
				</button>
				<button class="btn columbusSettingBtn" data-dismiss="modal" aria-hidden="true">
					<spring:message code="btnClose" />
				</button>
			</div>
		</div>
	</div>
</div>
<!-- End -->
	     



<!-- start ... Attach dynamic portfolio jsp files -->
	<jsp:include page="dp_common.jsp"></jsp:include>
	<jsp:include page="jafstartcommon.jsp"></jsp:include>
	
<!-- end ... Attach dynamic portfolio jsp files -->

<script>
try{
$('#nextStep').html($('#applyToNext').html());
}catch(e){}
/*if(document.getElementById('getTextType').value==2)
{
	document.getElementById('linkVal').innerHTML="";
}
else
{
	document.getElementById('linkVal').innerHTML="Expand";
}*/
try {
   document.getElementById('divDescDM').style.display='inline';
}catch (e) {}
try {
  document.getElementById('divDescSM').style.display='inline';
}catch (e) {}
function openMessageDiv(){
	$('#myModalApplyJob').modal('show');
	$('#errordivMessage').empty();
	$('#msgMailSubject').css("background-color", "");
	$('#msgMail').css("background-color", "");
	$('#jobFile').css("background-color", "");
	$('#msgMailSpt').find(".jqte_editor").css("background-color", "");
	$('#sumOfCaptchaText').css("background-color","");
	document.getElementById('msgMailSubject').value="";
	$('#msgMailSpt').find(".jqte_editor").html("");
	removeJobFile();
}
function downloadJobDescription()
{
	DistrictPortfolioConfigAjax.downloadJobDescription(${jobOrder.jobId},{ 
		async: false,
		errorHandler:handleError,
		callback:function(data){
			document.getElementById("hrefJobDesc").href = data; 
		}
	});
}

function saveMessage()
{
	
	if(!validateMessage())
		return;
	
	$('#lodingJobImg').show();
	
	document.getElementById('frmMessageUpload').action="messageUploadServlet.do";
	$('#frmMessageUpload').submit();
	
}
function hideMessage(flg){
	$('#lodingJobImg').hide();
	$('#myModalApplyJob').modal('hide');
	if(flg==1){
		$('#messageshow').html("Your message is successfully sent to Administrator.");
		$('#myModalApplyJob2').modal('show');
	}else{
		alert("Oops: Your Session has expired!");  document.location = 'signin.do';
	}
	document.getElementById('msgMailSubject').value="";
	$('#msgMailSpt').find(".jqte_editor").html("");
	removeJobFile();
}
function addJobFileType(){
	$('#jobFile1').empty();
	$('#jobFile1').html("<a href='javascript:void(0);' onclick='removeJobFile();'><img src='images/can-icon.png' title='remove'/></a> <input id='jobFile' name='jobFile' size='20' title='Choose a File' type='file'><br>&nbsp;Maximum file size 2MB.");
}
function removeJobFile(){
	$('#jobFile1').empty();
	$('#jobFile1').html("<a href='javascript:void(0);' onclick='addJobFileType();'><img src='images/attach.png'/></a> <a href='javascript:void(0);' onclick='addJobFileType();'>Attach a File</a>");
}
function validateMessage()
{
	var sumOfCaptchaText = document.getElementById("sumOfCaptchaText");
	
	$('#errordivMessage').empty();
	$('#msgMailSubject').css("background-color", "");
	$('#msgMail').css("background-color", "");
	$('#jobFile').css("background-color", "");
	$('#msgMailSpt').find(".jqte_editor").css("background-color", "");
	$('#sumOfCaptchaText').css("background-color","");
	var cnt=0;
	var focs=0;
	
	
	var fileSize=0;		
	if ($.browser.msie==true)
 	{	
	    fileSize = 0;	   
	}
	else
	{	
		if(document.getElementById("jobFile"))
		if(document.getElementById("jobFile").files[0]!=undefined)
			fileSize = document.getElementById("jobFile").files[0].size;
	}
	if(trim($('#msgMailSubject').val())=="")
	{
		$('#errordivMessage').append("&#149; Please enter Subject.<br>");
		if(focs==0)
			$('#msgMailSubject').focus();
		$('#msgMailSubject').css("background-color", "#F5E7E1");
		cnt++;focs++;
	}
	
	if ($('#msgMailSpt').find(".jqte_editor").text().trim()=="")
	{
		$('#errordivMessage').append("&#149; Please enter Message.<br>");
		if(focs==0)
			$('#msgMailSpt').find(".jqte_editor").focus();
		$('#msgMailSpt').find(".jqte_editor").css("background-color", "#F5E7E1");
		cnt++;focs++;
	}else
	{
		var charCount=$('#msgMailSpt').find(".jqte_editor").text().trim();
		var count = charCount.length;
			if(count>1000)
			{
			$('#errordivMessage').append("&#149; Message length cannot exceed 1000 characters.<br>");
			if(focs==0)
				$('#msgMailSpt').find(".jqte_editor").focus();
			$('#msgMailSpt').find(".jqte_editor").css("background-color", "#F5E7E1");
			cnt++;focs++;
		}
	}
	if(fileSize>=2097152)
	{		
		$('#errordivMessage').append("&#149; File size must be less then 2mb.<br>");
		if(focs==0)
			$('#jobFile').focus();
		
		$('#jobFile').css("background-color","#F5E7E1");
		cnt++;focs++;	
	}	
	if(trim(sumOfCaptchaText.value)==""){
		$('#errordivMessage').append("&#149; Please Add both the numbers<br>");
		if(focs==0)
			$('#sumOfCaptchaText').focus();
		
		$('#sumOfCaptchaText').css("background-color",txtBgColor);
		cnt++;focs++;
	}else if(chkCaptcha()){
		$('#errordivMessage').append("&#149; Sum of both the numbers is wrong, please enter again<br>");
		if(focs==0)
			$('#sumOfCaptchaText').focus();
		
		$('#sumOfCaptchaText').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	

	if(cnt==0)
		return true;
	else
	{
		changeCaptcha('sumOfCaptchaText');
		$('#errordivMessage').show();
		return false;
	}
}
checkInvitation();
</script>



<input type="hidden" id="isJobApproved" name="isJobApproved" value="${jobOrder.approvalBeforeGoLive}"/>
<input type="hidden" id="redirectURL" name="redirectURL" value="${redirectURL}"/>

<div class="modal hide" id="isJobApprovedPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog-for-cgcoverletter" style='width: 35%;'>
		<div class="modal-content">
			<div class="modal-header">
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body" style="max-height: 400px;overflow-y:auto;">
				<div class="control-group">
				<spring:message code="lblThisJobnotavailablemoment" />
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-primary" data-dismiss="modal" onclick="oncancelClick()">Ok <i class="icon"></i></button>&nbsp;&nbsp;
			
			</div>
		</div>
	</div>
</div>

<script>
function oncancelClick()
{
	var redirectURL = $("#redirectURL").val();
	//alert("redirectURL:- "+redirectURL);
	if(redirectURL!=null & redirectURL!="" & redirectURL!=undefined)
	{
		window.location.assign(redirectURL);
	}
}

$(document).ready(function(){
   
    var cal = Calendar.setup({
          onSelect: function(cal) { cal.hide() },
          showTime: true
      });
     cal.manageFields("columbus_date1", "columbus_date1", "%m-%d-%Y");
     cal.manageFields("columbus_date2", "columbus_date2", "%m-%d-%Y"); 
     
	var approvalBeofreGoLive = $("#isJobApproved").val();
	
	if(approvalBeofreGoLive!="1")
	{
		$('#isJobApprovedPopup').modal('show');
		$("#applyJobTopId").hide();
	//	$("#applyJobBottomId").hide();
	}
}); 
</script>

<script type="text/javascript">
	   var cal = Calendar.setup({
	          onSelect: function(cal) { cal.hide() },
	          showTime: true
	      });
	      cal.manageFields("empaproretdatefe6", "empaproretdatefe6", "%m-%d-%Y");
	      cal.manageFields("retiredempdateofretirement", "retiredempdateofretirement", "%m-%d-%Y");
	      cal.manageFields("formalempdatesofemployee", "formalempdatesofemployee", "%m-%d-%Y");
	     
</script>
<script type="text/javascript">
   $(document).ready(function () {
      $('#ssn_pi').bind('paste', function (e) {
         e.preventDefault();
        // alert("You cannot paste text");
      });
   });
   
   if(document.location.hostname=="nccloud.teachermatch.org" || document.location.hostname=="nc.teachermatch.org")
{
	document.getElementById("employeeOf").style.display="none";   
}  
</script>