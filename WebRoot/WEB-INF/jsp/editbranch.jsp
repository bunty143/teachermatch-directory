<!-- @Author: Ankit Sharma 
 * @Discription: view of edit branch Page.
-->
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<script type="text/javascript" src="dwr/interface/DistrictAjax.js?ver=${resourceMap['DistrictAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/HeadQuarterAjax.js?ver=${resourceMap['HeadQuarterAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/BranchesAjax.js?ver=${resourceMap['BranchesAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/I4QuestionSetAjax.js?ver=${resourceMap['I4QuestionSetAjax']}"></script>
<script type="text/javascript" src="dwr/interface/DistrictAssessmentAjax.js?ver=${resourceMap['DistrictAssessmentAjax']}"></script>
<script type="text/javascript" src="dwr/interface/UserAjax.js?ver=${resourceMap['UserAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type='text/javascript' src="js/editbranch.js?ver=${resourceMap['js/editbranch.js']}"></script>
  <!-- Optionally enable responsive features in IE8 -->
    <script type='text/javascript' src="js/bootstrap-slider.js"></script>
    <link rel="stylesheet" type="text/css" href="css/slider.css" />  
<script src="calender/js/jscal2.js"></script>
<script src="calender/js/lang/en.js"></script>
<link rel="stylesheet" type="text/css" href="calender/css/jscal2.css" />
<link rel="stylesheet" type="text/css" href="calender/css/border-radius.css" />
<link rel="stylesheet" type="text/css" href="calender/css/gold/gold.css" />
<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" />  
<style>
.table th, .table td {
    padding: 8px;
    line-height: 20px;
    text-align: left;
    vertical-align: top;  
}
.divwidth
{
float: left;
width: 40px;
}
.input-group-addon {
    background-color: #fff;
	border: none;
}
</style>
<script type="text/javascript">
var $j=jQuery.noConflict();
        $j(document).ready(function() {
        });
        
function applyScrollOnTbl()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#districtSchoolsTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 280,
        width: 722,
        minWidth: null,
        minWidthAuto: false,
        colratio:[288,182,144,110], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}

function applyScrollOnTblDomain()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#domainTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 280,
        width: 600,
        minWidth: null,
        minWidthAuto: false,
        colratio:[500,100], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}

function applyScrollOnTblNotes()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#notesTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 280,
        width: 722,
        minWidth: null,
        minWidthAuto: false,
        colratio:[288,182,144,110], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}



 </script>  
<script type="text/javascript">
	
	
</script>
<style>
	.hide
	{
		display: none;
	}
	.inline
	{
		display: inline;
	}
	.nowrap
	{
		white-space: nowrap
	}
	.setSlider
	{
		margin-left: -20px; 
		margin-top: -8px;
	}
	
	.tooltip {
  position: absolute;
  z-index: 1030;
  display: block;
  font-size: 12px;
  line-height: 1.4;
  opacity: 0;
  filter: alpha(opacity=0);
  visibility: visible;
}

.tooltip.in {
  opacity: 0.9;
  filter: alpha(opacity=90);
}

.tooltip.top {
  padding: 5px 0;
  margin-top: -3px;
}

.tooltip.right {
  padding: 0 5px;
  margin-left: 3px;
}

.tooltip.bottom {
  padding: 5px 0;
  margin-top: 3px;
}

.tooltip.left {
  padding: 0 5px;
  margin-left: -3px;
}

.tooltip-inner {
  max-width: 200px;
  padding: 3px 8px;
  color: #ffffff;
  text-align: center;
  text-decoration: none;
  background-color: #000000;
  border-radius: 4px;
}

.tooltip-arrow {
  position: absolute;
  width: 0;
  height: 0;
  border-color: transparent;
  border-style: solid;
}

.tooltip.top .tooltip-arrow {
  bottom: 0;
  left: 50%;
  margin-left: -5px;
  border-top-color: #000000;
  border-width: 5px 5px 0;
}

.tooltip.top-left .tooltip-arrow {
  bottom: 0;
  left: 5px;
  border-top-color: #000000;
  border-width: 5px 5px 0;
}

.tooltip.top-right .tooltip-arrow {
  right: 5px;
  bottom: 0;
  border-top-color: #000000;
  border-width: 5px 5px 0;
}

.tooltip.right .tooltip-arrow {
  top: 50%;
  left: 0;
  margin-top: -5px;
  border-right-color: #000000;
  border-width: 5px 5px 5px 0;
}

.tooltip.left .tooltip-arrow {
  top: 50%;
  right: 0;
  margin-top: -5px;
  border-left-color: #000000;
  border-width: 5px 0 5px 5px;
}

.tooltip.bottom .tooltip-arrow {
  top: 0;
  left: 50%;
  margin-left: -5px;
  border-bottom-color: #000000;
  border-width: 0 5px 5px;
}

.tooltip.bottom-left .tooltip-arrow {
  top: 0;
  left: 5px;
  border-bottom-color: #000000;
  border-width: 0 5px 5px;
}

.tooltip.bottom-right .tooltip-arrow {
  top: 0;
  right: 5px;
  border-bottom-color: #000000;
  border-width: 0 5px 5px;
}
</style>
<form:form commandName="branchMaster"  method="post" onsubmit="return validateEditDistrict();"  enctype="multipart/form-data">
<div class="row" style="margin-left: 0px;margin-right: 0px;margin-top:">
         <div style="float: left;">
         	<img src="images/add-district.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;">Edit Branch</div>	
         </div>
		
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
</div>

    
	<div>
		<div class="mt10">
      		<div class="panel-group" id="accordion">
        		<div class="panel panel-default">
          		<div class="accordion-heading"> <a href="#collapseOne" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle minus"> General Information </a> </div>
          		<div class="accordion-body in" id="collapseOne" style="height:auto;">
            	<div class="accordion-inner">
              	<div class="offset1">
              	<div class="row">
					<div class="col-sm-12 col-md-12">			                         
					 	<div class='required' id='errorlogoddiv'></div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6 col-md-6">
						<label>Branch Name</label>
						<form:input path="branchName" cssClass="form-control" maxlength="50" tabindex="-1" placeholder=""  />
	                   </div>
                 </div>
				<div class="row">
					<div class="col-sm-6 col-md-6">
                   		<label>Address</label>
						<form:input path="address" cssClass="form-control" maxlength="50" tabindex="-1" placeholder="" />
                    </div>
				</div>
                  
				<div class="row">
                  	<div class="col-sm-2 col-md-2">
                 		<label>NCES Branch ID</label>
						<form:input path="branchId" cssClass="form-control" maxlength="50" tabindex="-1" placeholder="" readonly="true"/>
                  	</div>
                  	
                  	<div class="col-sm-2 col-md-2">
                    	<label>Branch Code</label>
						<form:input path="branchCode" cssClass="form-control" maxlength="50" tabindex="-1" placeholder="" />
                  	</div>
                  	
                  	<div class="col-sm-2 col-md-2">
                    	<label>Zip Code</label>
						<form:input path="zipCode" cssClass="form-control" maxlength="50" tabindex="-1" placeholder="" />
                  	</div>
				</div>
				
                <div  class="row">
                	<div class="col-sm-3 col-md-3">
                    	<label>State</label>
                    	<input type="text" class="form-control" value="${branchMaster.stateMaster.stateName }" tabindex="-1" readonly="true"/>
                  </div>
                  <div class="col-sm-3 col-md-3">
                    	<label>City</label>
                    	<form:input path="cityName" cssClass="form-control" maxlength="50" tabindex="-1" placeholder="" readonly="true"/>
                  </div>
                
                </div>
                  
                  <div class="row">
                    <div class="col-sm-2 col-md-2">
                      	<label>Phone</label>
						<form:input path="phoneNumber" cssClass="form-control" maxlength="20" tabindex="-1" placeholder=""/>
                    </div>
                    
                    <div class="col-sm-2 col-md-2">
                      	<label>Fax</label>
						<form:input path="faxNumber" cssClass="form-control" maxlength="20" placeholder=""/>
                    </div>
                  </div>
				
				 <div class="row">
                  	<div class="col-sm-6 col-md-6">
                      	<label>Website URL</label>
						<form:input path="websiteUrl" cssClass="form-control" maxlength="100" placeholder=""/>
                    </div>
                  </div>
                              
                  <div class="row">
                   
                    <div class="col-sm-2 col-md-2">
                      <label>Total # of Districts <a href="#"  id="iconpophover6" rel="tooltip"   data-original-title="Total number of districts in the Branch per NCES records. If this information is not accurate, please notify your TeacherMatch Account Manager"><img src="images/qua-icon.png" width="15" height="15" alt=""></a></label>
						<form:input path="totalNoOfDistricts" cssClass="form-control" maxlength="50" tabindex="-1" placeholder="" readonly="true"/>
                    </div>
                   
                    </div>
                  
                <div  class="row">
					<div class="col-sm-2 col-md-2">
						<label>Logo <a href="#" id="iconpophover2" rel="tooltip" data-original-title="Logo image for the Branch"><img src="images/qua-icon.png" width="15" height="15" alt=""></a></label>    
						
						<form:input path="logoPathFile" type="file"/>
						<!-- <a href="#" onclick="return clearFile(1)">Clear</a> -->

				 	</div>
                   	<div class="col-sm-4 col-md-4">
                   		<label>&nbsp;</label><span id="showLogo">&nbsp;</span>
                   	</div>
                </div>
                  
                  	<div class="row top5">
                    	<div class="col-sm-9 col-md-9">
                      	<p>TeacherMatch can approach me for periodic customer surveys to improve its service, quality, and efficacy. <a href="#" id="iconpophover9" rel="tooltip" data-original-title="Periodically, TeacherMatch surveysits customers for feedback on its quality, responsiveness and efficacy"><img src="images/qua-icon.png" width="15" height="15" alt=""></a></p>
                    	</div>
                  </div>
				<div class="row">
					<div class="col-sm-1 col-md-1">
						<label class="radio">
                        
                        <%-- <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked="">--%>
                        <form:radiobutton path="canTMApproach" value="1" />
                        Yes 
                        </label>
                     </div>
                     <div class="col-sm-1 col-md-1 left20-sm2">
                      	<label class="radio">
                        <form:radiobutton path="canTMApproach" value="0" />
                        <%-- <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked="">--%>
                        No </label>
                    </div>
                  </div>
              </div>
              <div class="clearfix"></div>
            </div>
          </div>
        </div>
	
	<div class="panel panel-default">
			<div class="accordion-heading"> <a href="#collapseTwo" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle plus"> Contact Information </a> </div>
          	<div class="accordion-body collapse" id="collapseTwo" style="height: 0px;">
           
           	<div class="accordion-inner">
				<div class="span14 offset1">
					<div class="row">
	                <div class="col-sm-9 col-md-9">
	              		<div id="errorkeydiv" class="required"></div>
	                </div>
                </div>
                    <div class="row hide">
	                <div class="col-sm-6 col-md-6">
	                <label>Branch Contact 1</label>
	                	<form:input path="contactNumber1" cssClass="form-control" maxlength="50" tabindex="-1" placeholder=""  />
	                </div>
	                  <div class="col-sm-6 col-md-6">
	                   <label>Branch Contact 2</label>
	                	<form:input path="contactNumber2" cssClass="form-control" maxlength="50" tabindex="-1" placeholder=""  />
	                </div>
                </div>
                <div class="row">
                	 <div class="col-sm-6 col-md-6">
	                <label>Email Address</label>
	                	<form:input path="branchEmailAddress" cssClass="form-control" maxlength="50" tabindex="-1" placeholder=""  />
	                </div>
                </div>
                
                
	                <div class="row">                               
	                      <div class="col-sm-11 col-md-11">Other Key Contacts 
	                      <div class="pull-right">
	                       <c:if test="${fn:indexOf(roleAccessCI,'|1|')!=-1}">
	                      <c:if test="${userMaster.branchMaster ==null }">
	                      	<a href="javascript:void(0);"  onclick="showKeyContact();">+ Add Key Contact</a>
	                      </c:if>  	
	                      </c:if>
	                      </div>
	                    </div>                 
	                </div>
               
                <div class="row">
                  <div  class="col-sm-11 col-md-11 mt08">
                    <div id="keyContactTable"></div>
                  </div>
                </div>
              
                <div id="addKeyContactDiv"  class="hide">
                <div class="row">
                    <div class="col-sm-4 col-md-4">
                      <label>Contact Type </label>
                      <select class="form-control" id="dropContactType" name="dropContactType">
                        <c:forEach items="${contactTypeMaster}" var="ctm">
							<c:set var="selected" value=""></c:set>					
			        		<c:if test="${st.stateId eq ctm.contactTypeId}">	        			
			         			<c:set var="selected" value="selected"></c:set>	         			
			        		</c:if>	
							<option id="${ctm.contactTypeId}" value="${ctm.contactTypeId}" $selected >${ctm.contactType}</option>
						</c:forEach>
                      </select>
                      <input type="hidden" id="keyContactId" name="keyContactId" placeholder="" class=" span3">
                    </div>
                    <div class="col-sm-4 col-md-4">
                    <label>Contact First Name<span class="required">*</span> </label>
                    <input type="text" id="keyContactFirstName" name="keyContactFirstName" maxlength="50" placeholder="" class="form-control">
                  </div>
                   <div class="col-sm-3 col-md-3">
                    <label>Contact Last Name<span class="required">*</span> </label>
                    <input type="text" id="keyContactLastName" name="keyContactLastName" maxlength="50" placeholder="" class="form-control">
                  </div>
                  </div>
                  
                  <div class="row">
                  <div class="col-sm-4 col-md-4">
                    <label>Contact Email<span class="required">*</span> </label>
                    <input type="text" id="keyContactEmailAddress" name="keyContactEmailAddress"  maxlength="100" placeholder="" class="form-control">
                  </div>
                  <div class="col-sm-4 col-md-4">
                    <label>Phone</label>
                    <input type="text" id="keyContactPhoneNumber" name="keyContactPhoneNumber"   maxlength="20" placeholder="" class="form-control">
                  </div>
                    <div class="col-sm-3 col-md-3">
                      <label>Title</label>
                      <input type="text"  id="keyContactTitle" name="keyContactTitle"  maxlength="25" class="form-control">
                    </div>
                </div>
                
                <div class="row">
                  <div class="col-sm-5 col-md-5 idone mt10"><a href="javascript:void(0);" onclick="return addKeyContact();"><spring:message code="lnkImD"/></a>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onclick="clearKeyContact();">Cancel</a></div>
                </div>               
              </div>    
            </div>
        <div class="clearfix">&nbsp;</div>
          </div>
             </div>    
    </div>   
    
    <div class="panel panel-default">
          <div class="accordion-heading"> <a href="#collapseThree" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle plus"> Users </a> </div>
          <div class="accordion-body collapse" id="collapseThree" style="height: 0px;">
            <div class="accordion-inner">
              <div class="offset1">
              <div id="errorusersdiv" class="required"></div>
               <div class="row">
                  <div class="col-sm-10 col-md-10"><label>Administrators</label>
	                  <div class="pull-right">
	                   <c:if test="${fn:indexOf(roleAccessU,'|1|')!=-1}">
	                      <c:if test="${userSession.roleId.roleId ne 11}">
	                         <a href="javascript:void(0);" onclick="addAdministrator(11);">+ Add Administrator</a>
	                      </c:if>
	                   </c:if>
	                   </div>
                   </div>
                </div>
                
                 <div class="row col-sm-10 col-md-10">            
                    <div id="branchAdministratorDiv" class="row"></div>
                 </div>
                
                <div class="row">
                  <div class="col-sm-10 col-md-10 top20"><label>Analysts</label>
	                  <div class="pull-right"> 
	                   <c:if test="${fn:indexOf(roleAccessU,'|1|')!=-1}">
	                      <c:if test="${userSession.roleId.roleId ne 11}">
	                 		 <a href="javascript:void(0);" onclick="addAdministrator(13);">+ Add Analyst</a>
	                  	  </c:if>
	                   </c:if>
	                  </div>
                  </div>
                </div>
                
                <div class="row col-sm-10 col-md-10">                
                    <div id="branchAnalystDiv"  class="row"></div>
                </div>
                
                <!-- Add Administrator Elements :  -->
                <div id="addAdministratorDiv" class="hide" style="padding-top: 8px;">
                <div class="row col-sm-10 col-md-10">
                <div class="row">
					<div class="col-sm-3 col-md-3">
						<label><strong>Salutation</strong></label>
						<select class="form-control" id="salutation" name="salutation">
							<option value="1">Mr.</option>
							<option value="2">Ms.</option>
							<option value="3">Mrs.</option>
							<option value="4">Dr.</option>
						</select>
					</div>
					<div class="col-sm-3 col-md-3">
                    	<label><strong>First Name</strong><span class="required">*</span></label>
                    	 <input class="form-control" type="hidden" id="roleId" name="roleId" value="" placeholder="">
                    	 <input class="form-control" type="text" id="firstName" name="firstName"  maxlength="50"placeholder="">
                    	<%-- <form:input type="hidden" path="userId" id="userId" />
                    	<form:input path="firstName" cssClass="span3" maxlength="50" placeholder=""/>--%>
					</div>
					<div class="col-sm-3 col-md-3">
                    	<label><strong>Last Name</strong><span class="required">*</span></label>
                    	 <input class="form-control" type="text" id="lastName" name="lastName" maxlength="50" placeholder="">
					</div>
					</div>
				</div>
				
                <div class="row col-sm-10 col-md-10">
                   <div class="row">
					<div class="col-sm-9 col-md-9">
                    	<label><strong>Title</strong></label>
                    	<input class="form-control" type="text" id="title" name="title" maxlength="200" placeholder="">
                  </div>
                  </div>
                </div>
                
                <div class="row col-sm-10 col-md-10">
                   <div class="row">
					<div class="col-sm-9 col-md-9">
                    	<label><strong>Email</strong><span class="required">*</span></label>
                    	 <input class="form-control" type="text" id="emailAddress" name="emailAddress" maxlength="75" placeholder="">
                  </div>
                </div>
                </div>
                
                <div class="row col-sm-10 col-md-10">
                   <div class="row">
					<div class="col-sm-5 col-md-5">
                    	<label><strong>Phone</strong></label>
                    	<input class="form-control" type="text" id="phone" name="phone" maxlength="20" placeholder="">
                  	</div>
                  	<div class="col-sm-4 col-md-4">
                    	<label><strong>Mobile</strong></label>
                    	<input class="form-control" type="text" id="mobileNumber" name="mobileNumber" maxlength="20" placeholder="">
                  	</div>
                </div>
                </div>
                
                <div class="row">
                  <div class="col-sm-3 col-md-3 idone top10"><a href="javascript:void(0);" onclick="validateBranchAdministratorOrAnalyst();"><spring:message code="lnkImD"/></a>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onclick="clearUser();">Cancel</a></div>
                </div>
            </div><%-- End of addAdministratorDiv --%>
                
              </div>
              <div class="clearfix">&nbsp;</div>
              </div>              
            </div>
    </div>
    
    <!--<div class="panel panel-default">
          <div class="accordion-heading"> <a href="#collapseFive" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle plus"> Social </a> </div>
          <div class="accordion-body collapse" id="collapseFive" style="height: 0px;">
            <div class="accordion-inner">
              <div class="offset1">
              <div class="row col-sm-12 col-md-12">
               <div id="errorsocialdiv" class="required"></div>
              </div>
                             
              </div>
              <div class="clearfix">&nbsp;</div>
            </div>
          </div>
    </div>-->
    
       <!--<div class="panel panel-default">
          <div class="accordion-heading"> <a href="#collapseSix" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle plus"> Mosaic </a> </div>
          <div class="accordion-body collapse" id="collapseSix" style="height: 0px;">
            <div class="accordion-inner">
              <div class="offset1">
           		
				
              </div>
              <div class="clearfix">&nbsp;</div>
            </div>
          </div>
      </div>-->
      
      <!-- <div class="panel panel-default">
          <div class="accordion-heading"> <a href="#collapseSeven" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle plus"> Messages and Notes </a> </div>
          <div class="accordion-body collapse" id="collapseSeven" style="height: 0px;">
            <div class="accordion-inner">
              <div class="offset1">
                  <div class="row">
                    <div class="span11">
                      <label class="radio">
						<form:radiobutton path="communicationsAccess" value="2" />
                        School Admins can see messages and notes from their school only</label>
                    </div>
                    <div class="span11" style="padding-right: 15px;">
                      <label class="radio">
						<form:radiobutton path="communicationsAccess" value="1" />
                        School Admins are able to see messages and notes of their District. However, School Admin of one school cannot see messages and notes of other schools </label>
                    </div>
                    <div class="span11">
                      <label class="radio">
						<form:radiobutton path="communicationsAccess" value="0" />
                        All messages and notes are visible to all the School Admins within the District </label>
                    </div>
                  </div>
                
              </div>
            </div>
          </div>
      </div>-->
      
      <div class="panel panel-default hide">
          <div class="accordion-heading"> <a href="#collapseEight" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle plus"> Candidate Grid </a> </div>
          <div class="accordion-body collapse" id="collapseEight" style="height: 0px;">
            <div class="accordion-inner">
              <div class="offset1">
              	 <div class="row mt10">
                  		<div class="span11">
                    		Display selected columns in Candidate Grid <a href="#" id="iconpophovercg" rel="tooltip" data-original-title="Check the fields you want to view in Candidate Grid" data-placement="right"><img src="images/qua-icon.png" width="15" height="15" alt=""></a>
                  		</div>
                  </div>
                  <div class="row mt10">
                  <div class="row">
                  <div class="col-sm-2 col-md-2">
                      <label class="checkbox">
						 <form:checkbox path="displayCGPA" />
                        CGPA</label>
                    </div>
                    <div class="col-sm-2 col-md-2">
                      <label class="checkbox">
						 <form:checkbox path="displayAchievementScore" />
                        Achievement Score</label>
                    </div>
                    <div class="col-sm-2 col-md-2">
                      <label class="checkbox">
						 <form:checkbox path="displayFitScore"/>
                        Fit Score </label>
                    </div>
                    <div class="col-sm-2 col-md-2">
                      <label class="checkbox">
						 <form:checkbox path="displayTFA" />
                        TFA </label>
                    </div>
                    
                     <div class="col-sm-2 col-md-2">
                      <label class="checkbox">
						 <form:checkbox path="displayPhoneInterview" />
                        Phone Interview</label>
                    </div>
                    
                    </div>
                    <div class="row">
                    <div class="col-sm-2 col-md-2">
                      <label class="checkbox" style="margin-top: 0px;">
						 <form:checkbox path="displayDemoClass" />
                        Demo Lesson</label>
                    </div>
                    <div class="col-sm-2 col-md-2">
                      <label class="checkbox" style="margin-top: 0px;">
						 <form:checkbox path="displayYearsTeaching" />
                        Teaching Years  </label>
                    </div>
                    <div class="col-sm-2 col-md-2">
                      <label class="checkbox" style="margin-top: 0px;">
						 <form:checkbox path="displayExpectedSalary"/>
                        Expected&nbsp;Salary </label>
                    </div>
                    <div class="col-sm-2 col-md-2">
                      <label class="checkbox" style="margin-top: 0px;">
						 <form:checkbox path="displayJSI" />
                        JSI</label>
                    </div>            
                    </div>
                  </div>
              </div>
            </div>
          </div>
       </div>  
        
      <!-- <div class="panel panel-default">
          <div class="accordion-heading"> <a href="#collapseNine" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle plus"> Score on Status </a> </div>
          <div class="accordion-body collapse" id="collapseNine" style="height: 0px;">
            <div class="accordion-inner">
              <div class="row">
                    <div class="col-sm-12 col-md-12">
              <div id="errorscorediv" class="required"></div>
              </div>
              </div>
    	         
				
                </div>
              <div class="clearfix">&nbsp;</div>
            </div>
       </div>-->
       
       <div class="panel panel-default hide">
          <div class="accordion-heading"> <a href="#collapseTen" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle plus">Privilege For Branch</a></div>
          <div class="accordion-body collapse" id="collapseTen" style="height: 0px;">
            <div class="accordion-inner">
    	      <div class="offset1">
    	      <div class="row mt10">
                  		<div class="span11">
                    		Job Approval
                  		</div>
                </div>
                <div class="row">
                  	<div class="col-sm-5 col-md-5" >
                  	<c:set var="disablenoOfApprovalNeeded" value=""></c:set>					
						<c:if test="${branchMaster.approvalBeforeGoLive ne true}">
				         	<c:set var="disablenoOfApprovalNeeded" value="disabled"></c:set>	 
						</c:if>
                      <label class="checkbox" style="padding-left:5px;">                      
						 <form:checkbox path="approvalBeforeGoLive"  id="approvalBeforeGoLive1" onclick="return jobApprovalFunc();"/>
                        Approval process for positions prior to them going live</label>
                    </div>
                   
                </div>

    	      	<div class="row mt10">
                  		<div class="span11">
                    		Candidate Pool
                  		</div>
                </div>
    	      	<div class="row">
                  	<div class="span11">
                      <label class="checkbox">
						 <form:checkbox path="isTeacherPoolOnLoad" />
                        Show candidates on page load</label>
                    </div>
                </div>
    	      	
                <div class="row">
                  	<div class="span11">
                      <label class="checkbox">
						 <form:checkbox path="autoNotifyCandidateOnAttachingWithJob" />
                        Auto Notify all the Candidates On attaching with a job</label>
                    </div>
                </div>
                
                 <div class="row">
                  	<div class="span11">
                      <label class="checkbox">
						 <form:checkbox path="noEPI" />
                          No EPI Score Visible</label>
                    </div>
                </div>
              
                <div class="row mt10">
                  		<div class="span11">
                  		<div class='required' id='errorpdistictdiv'></div>
                    		Job Category
                  		</div>
                </div>
                <div class="row  mt8" >
                	 <div class="span12 left20">
                	 	<label class="radio inline" style="padding-left: 0px;">
                		<c:choose>
                				<c:when test="${displayTMDefaultJobCategory eq true}">
                					<input type="radio"  name="displayTMDefaultJobCategory" id="tmdefault" checked="checked" value="1"/>
					         	  	Display TeacherMatch's default job categories.
                				</c:when>
                				<c:otherwise>
                					<input type="radio"  name="displayTMDefaultJobCategory" id="tmdefault" value="1" checked="checked"/>
					         	  	Display TeacherMatch's default job categories.
                				</c:otherwise>
                		</c:choose>
                		</label>
                		<br>
                		<label class="radio inline" style="padding-left: 0px;">
                		<c:choose>
                				<c:when test="${displayTMDefaultJobCategory eq false}">
                					 <input type="radio" name="displayTMDefaultJobCategory" id="districtdefault" checked="checked" value="0"/>
					           		 Display only Branch's default job categories.
                				</c:when>
                				<c:otherwise>
                					 <input type="radio" name="displayTMDefaultJobCategory" id="districtdefault" value="0"/>
					            	 Display only Branch's default job categories.
                				</c:otherwise>
                		</c:choose>
                		</label>
                	</div>	
                </div>
                 
                <div class="row">   
                    <div class="span12">
                        <form:checkbox path="offerVirtualVideoInterview" cssClass="checkbox inline" style="vertical-align: bottom" id="offerVirtualVideoInterview" value="1" onclick="showOfferVVI();" />
			            <span style="display: inline-block;">Offer Virtual Video Interview</span>
					</div>
					
					<div class="left15 hide" id="offerVVIDiv">
							   <div class="row">
							     <div class="col-sm-12 col-md-12">
							        	<div class="divErrorMsg" id="errorofferVVIDiv"></div>         
						           </div>
								</div>
								<div class="left15">
							<div class="row">
							     <!--<div class="col-sm-12 col-md-12 top25-sm23">
							             		<div  class="col-sm-6 col-md-6" style="margin:12px;">
							             			<label id="captionDistrictOrSchool">Please select a question set</label>
							             			<input type="hidden" id="quesId" name="quesId" value="${quesSetIDValue}"> 
								             		<input type="text" id="quesName" name="quesName"  class="form-control"
								             		onfocus="getQuestionSetAutoComp(this, event, 'divTxtShowData', 'quesName','quesId','');"
													onkeyup="getQuestionSetAutoComp(this, event, 'divTxtShowData', 'quesName','quesId','');"
													onblur="hideDistrictMasterDiv(this,'quesId','divTxtShowData');"	
													value="${quesSetNameValue}"/>
													<div id='divTxtShowData' style=' display:none;position:absolute;z-index:5000;' onmouseover="mouseOverChk('divTxtShowData','quesName')" class='result' ></div>	
								             	</div>		             
						           </div>
								--></div>
						
										<div class="row top5">
													<form:checkbox path="" cssClass="checkbox inline" style="vertical-align: bottom" id="wantScore" value="1" onclick="showMarksDiv();" />
													<span style="display: inline-block;">Want to score</span>
										</div>	
										<div class="row hide" id="marksDiv" >
								`				<div class="col-sm-3 col-md-3 top10">
													Max Marks		
													<form:input path="maxScoreForVVI" cssClass="form-control" value="10" id="maxScoreForVVI" maxlength="3" onkeypress="return checkForInt(event)" /> 
												</div>
										</div>
										<div class="row">
			                    	            <form:checkbox path="sendAutoVVILink" cssClass="checkbox inline" style="vertical-align: bottom" id="sendAutoVVILink" value="1" onclick="showLinkDiv();" />
									            <span style="display: inline-block;">Want to send the interview link automatically</span>
										</div>
								</div>	
						     
						                
			                <div class="row">
			                	<div class="mt10">
				                	<div  class="col-sm-3 col-md-3 top5">
										<label>Time Allowed Per Question In Sec</label>
										<form:input path="timeAllowedPerQuestion" id="timeAllowedPerQuestion" cssClass="form-control" maxlength="4" tabindex="-1" placeholder="" onkeypress="return checkForInt(event)" />
				                    </div>
				                    <div  class="col-sm-4 col-md-4 top5">	
				                    	<label>Virtual Video interview Expires In Days</label>
										<form:input path="VVIExpiresInDays" cssClass="form-control" style="width:200px;" maxlength="2" tabindex="-1" placeholder="" onkeypress="return checkForInt(event)" />
				            		</div>
			            		</div>
			            	</div>
			            	</div>
	            		</div>
					</div>
			    </div>
			    
			 </div>
              </div>
        
        <div class="panel panel-default hide">
          <div class="accordion-heading"> <a href="#collapseEle" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle plus">Privilege For District(s)</a></div>
          <div class="accordion-body collapse" id="collapseEle" style="height: 0px;">
            <div class="accordion-inner">
    	      <div class="offset1">
    	      	
    	      	<div class="row mt10">
                  		<div class="span11">
                    		Read/write privilege <!-- <a href="#" id="iconpophovercg" rel="tooltip" data-original-title="Check the fields you want to view in Candidate Grid" data-placement="right"><img src="images/qua-icon.png" width="15" height="15" alt=""></a> -->
                  		</div>
                </div>
                
                <div class="row  mt10">                    
                    <div class="span12 left20">
				 	<label class="checkbox inline" style="padding-left: 0px;">
			            <form:checkbox path="writePrivilegeToDistrict" value="1"/>
			            Branch and attached district(s) have read/write privilege
					</label>
					</div>                    
                </div>
                
                 <div class="row mt10">   
                    <div class="col-sm-12 col-md-12" style="margin-left: -15px;">
			            <form:checkbox path="statusPrivilegeForBranches" cssClass="checkbox inline" style="vertical-align: bottom" id=" 	statusPrivilegeForBranches" value="1" onclick="disableStatusMaster()" />
			        <span style="display: inline-block;">Branch(s) can set status for</span>
					</div>
                </div>
    	      </div>
    	       <div class="clearfix">&nbsp;</div>
            </div>
          </div>
          </div>
        
         <div class="panel panel-default hide">
          <div class="accordion-heading"> <a href="#collapseFifteen" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle plus">Privilege For Internal Transfer Candidate(s)</a></div>
          <div class="accordion-body collapse" id="collapseFifteen" style="height: 0px;">
            <div class="accordion-inner">
    	      <div class="offset1">
	    	      	<div class="row mt10">
	                  		<div class="span11">
	                    		 When a Internal Candidate applies for a job
	                  		</div>
	                </div>
	                <div class="row  mt8" >
	                	 <div class="span12 left20">
	                	 	<label class="checkbox inline" style="padding-left: 0px;">
	                	 		<form:checkbox path="offerPortfolioNeeded"/>Offer Portfolio</br>
	                	 		<form:checkbox path="offerDistrictSpecificItems"/>Offer District Specific Mandatory Items</br>
	                	 		<form:checkbox path="offerQualificationItems"/>Offer Qualification Items</br>
	                	 		<form:checkbox path="offerEPI"/>Offer EPI</br>
	                	 		<form:checkbox path="offerJSI"/>Offer JSI (if a JSI is offered with the job)</br>
	       					</label>
	                	</div>	
	               </div>
	             </div>
              <div class="clearfix">&nbsp;</div>
            </div>
          </div>
        </div>
        
        <!-- <div class="panel panel-default">
          <div class="accordion-heading"> <a href="#collapseSixteen" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle plus">Branch Domain(s)</a></div>
          <div class="accordion-body collapse" id="collapseSixteen" style="height: 0px;">
            <div class="accordion-inner">
    	      <div class="offset1">
    	      
    	         <div class="row">
            
                 </div>
    	      	  	 
                </div>
            </div>
          </div>
        </div>-->
        
        <div class="panel panel-default">
          <div class="accordion-heading"> <a href="#collapseFour" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle plus"> Account Information </a> </div>
          <div class="accordion-body collapse" id="collapseFour" style="height: 0px;">
            <div class="accordion-inner">
               <div class="offset1">
	                <div class="row">
		                <div class="">
								 	<div class='required' id='erroraccountinfodiv'></div>
								 	<div class='required' id='errorhqbranchdiv'></div>
								 	<div class='required' id='errornotediv'></div>
								 	<div class='required' id='errordatediv'></div>
						</div>	
					</div>	
					<div class="row">
		                <div class="col-sm-12 col-md-12" style="margin-left:-15px;"> 	
		                  <p> These options were defined during the initial contracting process. To propose changes or to discuss further add-ons, please contact our client partnership team at 1-(888)-312-7231 or via email at <a href="mailto:clientservices@teachermatch.net">clientservices@teachermatch.net</a></p>
		                </div>
	                </div>
              </div>
              
                <c:if test="${userSession.entityType ne 1 && userSession.entityType ne 5}">	
        			<c:set var="disable" value="true"></c:set>
         		</c:if>
              <div class="row">
                <div class="offset1">
                  <div class="row">
                        <div class="col-sm-3 col-md-3">
                        <label>Contract Start Date</label>
						<form:input path="contractStartDate" disabled="${disable}" cssClass="form-control"   value="${contractStartDate}"/>
                        </div>
                      	<div class="col-sm-3 col-md-3">
                        <label>Contract End Date</label>
						<form:input path="contractEndDate" disabled="${disable}" cssClass="form-control"   value="${contractEndDate}"/>
                 		</div>
                 	</div>	
                  <div class="row">
                    <div class="col-sm-3 col-md-3">
                      <label>Districts Under Contract</label>
					  <form:input path="districtsUnderContract" disabled="${disable}" cssClass="form-control" maxlength="9" placeholder=""/>
                    </div>
                  </div>
                  
               
                  <div class="row">
                    <div class="col-sm-3 col-md-3">
                      <label>Annual Subscription Amount</label>
						<form:input path="annualSubsciptionAmount" disabled="${disable}" cssClass="form-control" maxlength="50" placeholder=""/>
                    </div>
                     <c:if test="${userSession.entityType eq 1}">	
                      	<div class="col-sm-3 col-md-3">
                      	<label>Authentication Key</label>
							<input type="text" name="authKeyVal" readonly="readonly"  class="form-control"  value="${authKey}"  />
                    	</div>
                     </c:if>
                  </div>
                  
                  <div class="row">
                    <div class="col-sm-6 col-md-6 mt10">
                      <p>Which Districts are under contract</p>
                    </div>
                  </div>
                  
                  <div class="row">
                    <div class="col-sm-3 col-md-3">
                    <c:if test="${branchMaster.noDistrictUnderContract eq 2 && branchMaster.allDistrictsUnderContract eq 2 && branchMaster.selectedDistrictUnderContract eq 2}">	
	        			<c:set var="checked" value=""></c:set>
	         			<c:set var="checked" value="checked"></c:set>        			
	        		</c:if>
                      <label class="radio" style="margin-top: 0px;">
						<form:radiobutton path="noDistrictUnderContract" disabled="${disable}" value="1" onclick="uncheckedOtherRadio(1)" checked="${checked}" />
                        No District Attached</label>
                    </div>
                  </div>  
                                  
                  <div class="row">
                    <div class="col-sm-3 col-md-3">
                      <label class="radio" style="margin-top: 0px;">
						<form:radiobutton path="allDistrictsUnderContract" disabled="${disable}" value="1" onclick="uncheckedOtherRadio(2)"  />
                        All Districts in the Branch </label>
                    </div>
                  </div>
                  
                  <div class="row">
                     <div class="col-sm-3 col-md-3 mt10 left20">                      
	                        <div>
	                          <label class="radio inline" style="padding-left: 0px;">
								<form:radiobutton path="selectedDistrictUnderContract"  disabled="${disable}" value="1" onclick="uncheckedOtherRadio(4)"  />
	                            Selected Districts </label>
	                        </div>
	                 </div>    
                            <c:set var="showHideAddSchoolLink" value="hide"></c:set>					
			        		<c:if test="${branchMaster.selectedDistrictUnderContract eq 1}">	        			
			         			<c:set var="showHideAddSchoolLink" value=""></c:set>	         			
			        		</c:if>	
			        		<div class="col-sm-6 col-md-6 left18 mt10">
				        		<div class="pull-right ${showHideAddSchoolLink}" id="addSchoolLink">
			                         <c:if test="${fn:indexOf(roleAccessAI,'|1|')!=-1}">
			                        <a href="javascript:void(0);" onclick="addDistict();">+ Add District</a>
			                        </c:if>
	                             </div> 
			        		</div>
                        
                  </div>
                  <input type="hidden" id="gridNo" name="gridNo" value="">
                  <%-- ======== Branch District Grid ========= --%>
                 <div class="row col-sm-9 col-md-9">
                 	 <div  id="hqBranchesDistrict"  style="padding-bottom: 10px;" onclick="getHQBDGrid()">
                     </div>
                 </div>                   
                  <div id="addDistrictDiv" class="hide">
	                  <div class="row col-sm-10 col-md-10" style="max-width:755px;">
	                    <div>
	                      <label>District Name</label>
	             				<input type="text" id="districtName" name="districtName"  class="form-control"
			             		onfocus="getDistrictMasterAutoComp(this, event, 'divTxtShowData', 'districtName','districtId','');"
								onkeyup="getDistrictMasterAutoComp(this, event, 'divTxtShowData', 'districtName','districtId','');"
								onblur="hideDistrictMasterDiv(this,'districtId','divTxtShowData');"	/>
								<input type="hidden" id="districtId" value=""/>
							<div id='divTxtShowData' style=' display:none;position:absolute;z-index:5000;' onmouseover="mouseOverChk('divTxtShowData','districtName')" class='result' ></div>	
							
	                    </div>
	                  </div>
	                  <div class="row col-sm-5 col-md-5">
                 		 <div class="span2 idone"><a href="javascript:void(0);" onclick="return saveHqBranchDistrict();"><spring:message code="lnkImD"/></a>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onclick="clearHqBranchDistricts()">Cancel</a></div>
                	  </div>
                  </div>
                  
        		 <div class="clearfix"></div>
                  <div  id="uploadAssessmentDiv" class="row hide top10">				 
						 <c:if test="${fn:indexOf(roleAccessAI,'|9|')!=-1}">
						 <div class="col-sm-12 col-md-12">
						 Please upload Job Specific Inventory file (Acceptable formats are PDF, MS-Word,GIF, PNG and JPEG. Maximum file size 10MB)
						</div>
						<div class="col-sm-4 col-md-4">
							<form:input path="assessmentUploadURLFile" type="file"/>
						</div>
												
						</c:if> 
						<div class="col-sm-4 col-md-4">
						<a data-original-title='Branch' rel='tooltip' id='JSIBranch' href='javascript:void(0)'  onclick="showFile(2,'branch','${branchMaster.branchId}','${branchMaster.assessmentUploadURL}','JSIDistrict');${windowFunc}"><c:out value="${(branchMaster.assessmentUploadURL==''|| branchMaster.assessmentUploadURL==null)?'':'view'}"/></a>
						</div>					
                  </div>
                  
                  <input type="hidden" id="assessmentUploadURLVal" name="assessmentUploadURLVal" value="${(branchMaster.assessmentUploadURL != null ) ? '1' : '0'}" />
                  
                <div class="row top15">
                    <div class="col-sm-12 col-md-12">
                      <p>Hiring decision in the status workflow for District Job Order will be taken by</p>
                    </div>
                 </div>
                 
				<div class="row" >
                    <div class="col-sm-12 col-md-12">
	        			<c:set var="checked" value=""></c:set>
	                    <c:if test="${branchMaster.hiringAuthority eq ''}">	
		         			<c:set var="checked" value="checked"></c:set>        			
		        		</c:if>
	        		
                      <label class="radio" style="margin-top: 0px;">
						<form:radiobutton path="hiringAuthority" value="B" />
                        Branch Administrator(s)
                      </label>
                    </div>
                    
                    <div class="col-sm-12 col-md-12">
                      <label class="radio"  style="margin-top: 0px;">
						<form:radiobutton path="hiringAuthority"  value="D" checked="${checked}" />
                        District Administrator(s)
                      </label>
                    </div>
               </div>
                  
                   <div class="row  hide">
	                    <div class="col-sm-12 col-md-12">
	                      <p>District(s) can create their own job orders without authorization from the Branch</p>
	                    </div>
                   </div>
                   
				<div class="row hide">
                    <div class="col-sm-12 col-md-12">
                      <label class="radio">
						<form:radiobutton path="hQApproval" value="1"  checked="checked" />
                        Yes</label>
                    </div>
                    <div class="col-sm-12 col-md-12">
                      <label class="radio">
						<form:radiobutton path="hQApproval"  value="0" />
                        No </label>
                    </div>
                </div>
                  
                
                  
                   <c:if test="${userSession.entityType ne 1}">	
        			<c:set var="disableForTm" value="true"></c:set>
         		   </c:if>
                    
                   <c:if test="${userSession.entityType eq 1}">	                 
					   <div class="row top10 left5">
			                        <div class="col-sm-4 col-md-4">
										<label class="checkbox inline" style="padding-left: 0px;">
				                        	<form:checkbox path="isPortfolioNeeded" id="isPortfolioNeeded"  value="1"/>
											Is Portfolio Needed <a href="#" id="iconpophover15" rel="tooltip" data-original-title="Select this checkbox if you want the Candidates to complete their Portfolio first to complete the jobs of this district. Unselect this checkbox if you allow the Candidates to complete the jobs of this district without completing the Portfolio."><img src="images/qua-icon.png" width="15" height="15" alt=""></a>
										</label>
								  	</div>
			                        <div class="col-sm-4 col-md-4">
				                      <a href="javascript:void(0);" onclick="setEmailFormat()"> Set Email Format</a>
			                        </div>
                 			
	                   </div>
	                   
	                   <div class="row mt10 left5">
		                  <div class="col-sm-12 col-md-12">
		                        <label class="checkbox inline" style="padding-left: 0px;">
		                        <form:checkbox path="isWeeklyCgReport" id="isWeeklyCgReport"  value="1"/>
								Weekly CG Report <a href="#" id="iconpophover16" rel="tooltip" data-original-title="Select this checkbox if you want to receive the links for weekly CG Reports for all your jobs, in your mail box. Unselect this checkbox if you do not want receive the links for weekly CG Reports for all your jobs, in your mail box."><img src="images/qua-icon.png" width="15" height="15" alt=""></a>
								</label>
						   </div>
	                   </div>
                   </c:if>
                   
                   <div class="row mt10">
	                  <div class="col-sm-12 col-md-12">
	                    Job Board URL
	                    <div class="">
							<a target="blank" href="${JobBoardURL}">${JobBoardURL}</a>
	                    </div>
	                  </div>
                  </div>
                   
                   <div class="row mt10 left5">
	                  <div class="col-sm-6 col-md-6">
	                        <label class="checkbox inline" style="padding-left: 0px;">
	                        <form:checkbox path="allowMessageTeacher" id="allowMessageTeacher"  onclick="emailForTeacherDivCheck();"  value="1"/>
							Allow message from teacher
							</label>
					   </div>
                   </div>                   
                   
                    <c:if test="${userSession.entityType eq 1}">
                         <div id=""  class="row mt10">			
	                  	  	<div class="col-sm-6 col-md-6">
		                      <label >Exclusive Period (in days)</label>
		                    </div>
		                   </div> 
		                   
		                    <div class="row">  
		                    <div class="col-sm-6 col-md-6">
								<form:input path="exclusivePeriod" cssClass="form-control" maxlength="3"/>
							 </div>
							  </div>						
                   </c:if>
                   
				  
        		 <div class="clearfix"></div>                  
                   
                   <div id="emailForTeacherDiv"  class="hide row mt10">
                  	 <div class="col-sm-6 col-md-6">
                      <label >Email address to receive message from teacher.</label>
						<form:input path="emailForTeacher" cssClass="form-control" maxlength="100"/>
					 </div>
	                </div>
	                
	                 <div class="row">
                    <div class="col-sm-12 col-md-12">
                      <label class="radio">
                        <input type="radio" value="1" checked="checked" id="exitURLRadio" name="talentComplete"/>
                        Talent will complete full application process and wiil be routed to following URL <img src="images/qua-icon.png" width="15" height="15" alt=""></a> </label>
                    </div>
                  </div>
                  <div class="row">
                  <div class="col-sm-10 col-md-10" style="max-width: 750px;">
                  <form:input path="exitURL" cssClass="form-control" maxlength="250"/>
                  </div>
                  </div>
                  
                  <div class="row mt10">
                    <div class="col-sm-12 col-md-12">
                      <label class="radio">
                      <c:set var="compCheck" value=""></c:set>
                      <c:if test="${not empty branchMaster.completionMessage}">
                      <c:set var="compCheck" value="checked"></c:set>
                      </c:if>
                        <input type="radio" value="1" id="completeMessageRadio" ${compCheck} name="talentComplete"/>
                        Talent will complete full application process and be shown the following message <img src="images/qua-icon.png" width="15" height="15" alt=""></a> </label>
                    </div>
                  </div>
                  
                  <div class="row">
                  <div class="col-sm-12 col-md-12">
                    <p>Completion Message </p>
                    <div style="width:710px;"   id="eMessage">
                    <label class="redtextmsg">(If you are planning to copy and paste text from a web page and/or a Microsoft document, please ensure
					  to paste that text first into a simple text editor such as Notepad to remove the special characters and
 					  then copy and paste it into the field below. Otherwise, the text directly cut and pasted from a web page 
 					  and/or a Microsoft document may not display properly to the reader)</label>
					<form:textarea path="completionMessage" rows="3" maxlength="1500" onkeyup="return chkLenOfTextArea(this,1500);" onblur="return chkLenOfTextArea(this,1500);" class="form-control"/>
                    </div>
                  </div>
                  </div>
	                
        		</div>
              </div>
            </div>
          </div>
        </div>
        
        </div>
        </div>
        </div>
        
               <div class="modal hide"  id="myModalEmail" >
			<div class="modal-dialog-for-cgmessage">
		    <div class="modal-content">
			<div class="modal-header">
		  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
				<h3 id="myModalLabel">Teachermatch</h3>
			</div>
		
			<div  class="modal-body">
				<div class="control-group">
					<div class='divErrorMsg' id='errordivEmail'></div>
				</div>
				<div id='support'>
					<div class="control-group">
						<div class="">
					    	<label><strong>From</strong><span class="required">*</span></label>
				        	<input type="text" id="fromAddress" name="fromAddress"   class="form-control" maxlength="250" />
						</div>
					</div>
					
					<div class="control-group">
						<div class="">
					    	<label><strong>Subject</strong><span class="required">*</span></label>
				        	<input  type="text"  id="subjectLine" name="subjectLine"class="form-control" maxlength="250" />
						</div>
					</div>
				            
			        <div class="control-group">
						<div class="" id="mailBody">
					    	<label><strong>Message</strong><span class="required">*</span></label>
					    	<label class="redtextmsg">(If you are planning to copy and paste text from a web page and/or a Microsoft document, please ensure
						  	 to paste that text first into a simple text editor such as Notepad to remove the special characters and
 						  	 then copy and paste it into the field below. Otherwise, the text directly cut and pasted from a web page 
 						   	 and/or a Microsoft document may not display properly to the reader)</label>
				        	<textarea rows="5" class="form-control" cols="" id="" name=""></textarea>
						</div>
					</div>
		 		</div>
		 	</div>
		 	
		 	<div class="modal-footer">
		 		<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
		 		<button class="btn btn-primary"  onclick='return saveEmailFormat();' >Save</button>
		 	</div>
	</div>
	 	</div>
	</div>
       
       <div class="mt30">

   <c:if test="${userMaster.entityType ne 6}">  
     <c:if test="${fn:indexOf(roleAccessU,'|1|')!=-1}">
        <button type="submit" class="btn btn-large btn-primary"><strong>Save Branch <i class="icon"></i></strong></button>
         </c:if>
          </c:if> 
          &nbsp;&nbsp;<a href="javascript:void(0);" onclick="cancelBranch();">Cancel</a><br>
          <br>
        </div>
      
  
  <iframe src="" id="iframeJSI" width="100%" height="480px" style="display: none;">
  </iframe> 
   
  <div  class="modal hide"  id="myModalMsg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"   style="z-index: 5000;"  data-backdrop="static">
		<div class="modal-dialog">
		<div class="modal-content">
		<div class="modal-header">
	  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick="return aKeyContactDiv(0);">x</button>
			<h3 id="myModalLabel">Key Contact</h3>
		</div>
		<div class="modal-body">		
			<div class="control-group">
				<div class="">
			    	<span id="Msg"></span>	        	
				</div>
			</div>
	 	</div>
	 	<div class="modal-footer">
	 		<button class="btn" data-dismiss="modal" aria-hidden="true"  onclick="return aKeyContactDiv(1);">Ok</button>
	 		<button class="btn" data-dismiss="modal" aria-hidden="true"  onclick="return aKeyContactDiv(0);">Close</button> 		
 		</div>
	</div>
	</div> 
	</div>

	<div style="display:none;" id="loadingDiv">
		<table  align="center" >
			 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
		</table>
	</div>
		</form:form>
		
		
		
<script> 

	showFile(1,'branch','${branchMaster.branchId}','${branchMaster.logoPath}',null);	
</script>


<!-- @Author: Ankit 
 * @Discription: view of edit district Page.
 -->
 


<div class="modal hide" id="virusDivId" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog-for-cgcoverletter" style='width: 35%;'>
		<div class="modal-content">
			<div class="modal-header">
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body" style="max-height: 400px;overflow-y:auto;">
				<div class="control-group">
				</div>
			</div>
			<div class="modal-footer">
				<span id=""><button class="btn btn-primary" data-dismiss="modal" onclick="showDSPQDiv();">Ok <i class="icon"></i></button></span>&nbsp;&nbsp;
			</div>
		</div>
	</div>
</div>

<div class="modal hide" id="removeMemberConfirmation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false" removeMemberGroupId="" removeMemberId="">
	<div class="modal-dialog-for-cgcoverletter" style='width: 35%;'>
		<div class="modal-content">
			<div class="modal-header">
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body" style="max-height: 400px;overflow-y:auto;">
				<div class="control-group">
					Do you want to remove this user?
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-primary" data-dismiss="modal" onclick="removeMember()">Ok <i class="icon"></i></button>&nbsp;
				<button class="btn" data-dismiss="modal">Cancel</button> &nbsp;&nbsp;
			</div>
		</div>
	</div>
</div>

<input type="hidden" name="bId" id="bId" value="${param.bnchId}"/>
<c:choose>
<c:when test="${not empty hqId}">
	<input type="hidden" name="hqId" id="hqId" value="${hqId}"/>
</c:when>
<c:otherwise>
	<input type="hidden" name="hqId" id="hqId" value="0"/>
</c:otherwise>
</c:choose>
<script type="text/javascript">
	displayKeyContact();
	displayBranchAdministrator(11);
	displayBranchAnalyst(13);
	displayHqBranchDistricts();
	try
	{
	var districtSelected = ${branchMaster.selectedDistrictUnderContract};
	if(districtSelected!="" && districtSelected==1)
	{
		$("#addSchoolLink").fadeIn();
		$("#hqBranchesDistrict").fadeIn();
	}
	else
	{
		$("#hqBranchesDistrict").hide();
	}}
	catch(e){}
</script>
<script type="text/javascript">//<![CDATA[
    var cal = Calendar.setup({
        onSelect: function(cal) { cal.hide() },
        showTime: true
    });
  	 cal.manageFields("initiatedOnDate", "initiatedOnDate", "%m-%d-%Y");
     cal.manageFields("contractStartDate", "contractStartDate", "%m-%d-%Y");
     cal.manageFields("contractEndDate", "contractEndDate", "%m-%d-%Y");
    //]]></script>
    
    
    <script type="text/javascript">
	
	//displayAllDistrictAssessment();
</script>
<script type="text/javascript">

var selectIds = $('#collapseOne,#collapseTwo,#collapseThree,#collapseFour,#collapseFive,#collapseSix,#collapseSeven,#collapseEight,#collapseNine,#collapseTen,#collapseEle,#collapseFifteen,#collapseSixteen');
$(function ($) {
    selectIds.on('show.bs.collapse hidden.bs.collapse', function () {
        $(this).prev().find('.accordion-toggle').toggleClass('plus minus');
    })
});


$('.accordion').collapse();
$('.accordion').on('show', function (e) {
$(e.target).prev('.accordion-heading').find('.accordion-toggle').addClass('minus').removeClass('plus');
});

$('.accordion').on('hide', function (e) {
$(this).find('.accordion-toggle').not($(e.target)).removeClass('minus').addClass('plus');
});


function emailForTeacherDivCheck(){
	var allowMessageTeacher=document.getElementById("allowMessageTeacher").checked;
	if(allowMessageTeacher==1){
		$("#emailForTeacherDiv").fadeIn();
		$("#emailForTeacher").focus();
	}else{
		$("#emailForTeacherDiv").hide();
		$("#allowMessageTeacher").focus();
		document.getElementById("emailForTeacher").value="";
	}
}
emailForTeacherDivCheck();
</script>
<script type="text/javascript">
$('#myModal').modal('hide');
$('#iconpophover1').tooltip();
$('#iconpophover2').tooltip();
$('#iconpophover3').tooltip();
$('#iconpophover4').tooltip();
$('#iconpophover5').tooltip();
$('#iconpophover6').tooltip();
$('#iconpophover7').tooltip();
//$('#iconpophover8').tooltip();
$('#iconpophover9').tooltip();
$('#iconpophover10').tooltip();
$('#iconpophover11').tooltip();
$('#iconpophover12').tooltip();
$('#iconpophover13').tooltip();
$('#iconpophover14').tooltip();
$('#iconpophover15').tooltip();
$('#iconpophover16').tooltip();
$('#iconpophovercg').tooltip();
$('#iconpophover17').tooltip();
$('#iconpophover22').tooltip();

$(document).ready(function(){
  		$('#eMessage').find(".jqte").width(724);
  		$('#dNote').find(".jqte").width(724);
  		//$('#dDescription').find(".jqte").width(535);
}) 

</script>
<script>
	function disableSlider()
	{
		if($('#mosaicRadios1').is(':checked')) 
		{ 
	   		$("#mosaicRadiosFlag").val("1");
	   		$("#candidateFeedDaysOfNoActivity").val("0");
	   			try
	    		{
	    			document.getElementById("ifrm2").src="slideract.do?name=slider2&tickInterval=10&max=${sliderMaxLength }&swidth=200&svalue=${candidateFeedDaysOfNoActivityValue}&dslider=1";
    			}
	    		catch(e)
	    		{} 
	    }
	    else
	    {
	    	if($('#mosaicRadios2').is(':checked')) 
			{ 
	    	
	    		$("#mosaicRadiosFlag").val("0");
	    		$("#candidateFeedDaysOfNoActivity").val("0");
	    		try
	    		{
	    			document.getElementById("ifrm2").src="slideract.do?name=slider2&tickInterval=10&max=${sliderMaxLength }&swidth=200&svalue=0&dslider=0";
    			}
	    		catch(e)
	    		{} 

	    //		alert(" mosaicRadios2 is checked ");
		    }
	    }
	}
</script>