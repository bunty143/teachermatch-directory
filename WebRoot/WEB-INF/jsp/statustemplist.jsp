
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<script type="text/javascript" src="dwr/interface/StatusUploadTempAjax.js?ver=${resourceMap['StatusUploadTempAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type="text/javascript" src="js/teacher/statusupload.js?ver=${resourceMap['js/teacher/statusupload.js']}"></script>
<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" />
<script type="text/javascript">
var $j=jQuery.noConflict();
        $j(document).ready(function() {
        });
        
function applyScrollOnTbl()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tempStatusTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 280,
        width: 972,
        minWidth: null,
        minWidthAuto: false,
        colratio:[110,90,180,180,206,205], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}
 </script>

<div class="row  mt10 mb">
	
	<div class="span16 centerline ">
		<div class="span1 m0"><img src="images/icon1.png" width="41" height="41" alt=""></div>
		<div class="span10 subheading">Status List
		</div>
	</div>
	
</div>


<div class="row">
	<div class="mt10 span16" id="tempStatusGrid">
	</div>
</div>



<script type="text/javascript">
	displayTempStatus();
</script>
 <div class="row mt10">
	
		<button class="btn btn-primary" type="button" onclick="return saveStatus();">Accept <i class="icon"></i></button> &nbsp;&nbsp;<button class="btn btn-primary" type="button" onclick="return tempStatusReject('${sessionIdTxt}');">Rejected<i class="icon"></i></button>	
	
</div> 
	<div class="modal hide"  id="myModalMsg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"   onclick="return tempStatus();">x</button>
		<h3 id="myModalLabel">Status</h3>
	</div>
	<div class="modal-body"> 		
		<div class="control-group">
			Status details have been imported successfully.
		</div>
 	</div>
 	<div class="modal-footer">
 	<span id=""><button class="btn  btn-primary"   onclick="return tempStatus();" >Close</button></span>&nbsp;&nbsp;
 	</div>
	</div>
	</div>
</div> 
	
	
<div style="display:none;" id="loadingDiv">
	<table  align="left" >
		<tr><td style="padding-top:270px;padding-left:450px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
		<tr><td style="padding-top:0px;padding-left:450px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
	</table>
</div>       