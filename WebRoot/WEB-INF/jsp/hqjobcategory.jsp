<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" />
<script type="text/javascript" src="dwr/interface/ManageJobOrdersAjax.js?ver=${resourceMap['ManageJobOrdersAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/interface/HqJobCategoryAjax.js?ver=${resourceMap['HqJobCategoryAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/interface/HeadQuarterAjax.js?ver=${resourceMap['HeadQuarterAjax.Ajax']}"></script>
<script type='text/javascript' src="js/hqjobcategory.js?ver=${resourceMap['js/hqjobcategory.js']}"></script>
<script type="text/javascript" src="dwr/interface/I4QuestionSetAjax.js?ver=${resourceMap['I4QuestionSetAjax']}"></script>
<script type="text/javascript" src="dwr/interface/DistrictAssessmentAjax.js?ver=${resourceMap['DistrictAssessmentAjax']}"></script>
<script type="text/javascript" src="dwr/interface/AutoSearchFilterAjax.js?ver=${resourceMap['AutoSearchFilterAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/ReferenceChkQuestionsSetAjax.js?ver=${resourceMap['ReferenceChkQuestionsSetAjax']}"></script>
 
<script type="text/javascript">
var $j=jQuery.noConflict();
        $j(document).ready(function() {
        });
        
function applyScrollOnTbl1()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tempDistrictTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 280,
        width: 540,
        minWidth: null,
        minWidthAuto: false,
        colratio:[288,144,110], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}
function applyScrollOnTbl() {
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#jobCategoryTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 400,
        width: 945,
        minWidth: null,
        minWidthAuto: false,
        colratio:[145,165,205,80,110,65,175], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
     });			
}

 </script>
 
<div class="row" style="margin:0px;">
         <div style="float: left;">
         	<img src="images/manageusers.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;">Manage Job Category</div>	
         </div>			
		 <div class="pull-right add-employment1">
			 <c:if test="${(entityID ne 6 && userMaster.roleId.roleId ne 12)}">	
				<a href="javascript:void(0);" onclick="return addNewJobCategory()">+Add JobCategory</a>
			</c:if>			
		 </div>	
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
</div>

<input type="hidden" id="headQuarterId" value="${headQuarterMaster}">
<c:if test="${entityID eq 6}">	
<input type="hidden" id="branchId" value="${branchId}">
</c:if>

 

<div  style="margin-top:-10px;">
	   <div class="row top15">
			<form class="bs-docs-example" onsubmit="return false;">
				<div class="<c:out value="${hide}"/>">
				  	<div class="col-sm-3 col-md-3">
					<div class="">
						<span class=""><label class="">Status</label>
						     <select id="status" name="status" class="form-control">
							      <option value="All" selected >All Job Categories</option>
							      <option value="A" >Active Job Categories</option>
							      <option value="I">Inactive Job Categories</option>
						      </select>
					     </span>
			 		</div>
				</div>
					<div class="col-sm-3 col-md-3">
						<label>Entity Type</label>
					    <select class="form-control" id="entityType" name="entityType" style="min-width: 139px;" class="form-control" onchange="displayOrHideSearchBox();">
						 <c:if test="${not empty userMaster}">
						 	<c:if test="${(userMaster.entityType eq 5) || (userMaster.entityType eq 1)}">	
			         			<option value="1"   selected="selected" >Headquarter</option>
			         			<c:if test="${(userMaster.entityType eq 5) }">
								<option value="2">Branch</option>								
								<option value="3">District</option>
								</c:if>
							</c:if>	
						 	 <c:if test="${userMaster.entityType eq 6}">	
			         			<option value="1"   selected="selected" >Branch</option>
								<option value="2">District</option>
							</c:if>	
			        		<c:if test="${userMaster.entityType eq 2}">	
			         			<option value="2"  selected="selected" >District</option>
							</c:if>
			              </c:if>
						</select>			
	              	</div>
	              	<c:if test="${(userMaster.entityType eq 1)}">
	              	  <div id="SearchHeadQuarterTextboxDiv" >	 
		          		<div class="col-sm-6 col-md-6 " id="headQuarterClass">
	                        <label>Head Quarter</label>
	                        <input class="form-control" type="text" id="onlyHeadQuarterName" name="onlyHeadQuarterName"   value=""
	                          onfocus="getOnlyHeadQuarterAutoComp(this, event, 'divTxtHeadQuarterData', 'onlyHeadQuarterName','headQuarterHiddenId','');"
	                          onkeyup="getOnlyHeadQuarterAutoComp(this, event, 'divTxtHeadQuarterData', 'onlyHeadQuarterName','headQuarterHiddenId','');"
	                          onblur="hideHeadQuarterDiv(this,'headQuarterHiddenId','divTxtHeadQuarterData');setHeadQuarterId(); activateBranch();"      />
	                                
	                          <div id='divTxtHeadQuarterData' style=' display:none;position:absolute;'  onmouseover="mouseOverChk('divTxtHeadQuarterData','onlyHeadQuarterName')"  class='result' ></div>
	                          <input type="hidden" id="headQuarterHiddenId" value="" />
	                          <input type="hidden" id="isBranchExistsHiddenId" value=""/>    
	                     </div>
	          		
	          		</div>
	          		</c:if>
	              	
		            <div  id="Searchbox" class="<c:out value="${hide}"/>">	         		 
		             		<div class="col-sm-4 col-md-4">
		             			<label id="captionDistrictOrBranch">District</label>
			             		<input type="text" id="districtNameFilter" name="districtNameFilter" class="form-control"
			             		onfocus="getDistrictMasterAutoComp(this, event, 'divTxtShowDataFilter', 'districtNameFilter','districtIdFilter','');"
								onkeyup="getDistrictMasterAutoComp(this, event, 'divTxtShowDataFilter', 'districtNameFilter','districtIdFilter','');"
								onblur="hideDistrictMasterDiv(this,'districtIdFilter','divTxtShowDataFilter');"/>
								<input type="hidden" id="districtIdFilter" value="${userMaster.districtId.districtId}"/>
								<div id='divTxtShowDataFilter'  onmouseover="mouseOverChk('divTxtShowDataFilter','districtNameFilter')"
								 style=' display:none;position:absolute;z-index: 5000' class='result' ></div>	
			             	</div>		             
	          		</div>
	          		
	          		
	          		<c:set var="isDisplay" value="display: none;"></c:set>
	          		<c:set var="isDisabled" value=""></c:set>
	          		<c:if test="${entityID eq 1}">
	          		<c:set var="isDisplay" value=""></c:set>
	          		<c:set var="isDisabled" value="disabled"></c:set>
	          		</c:if>
	          		
	          		 <div  id="SearchboxHQ" style="${isDisplay}" class="<c:out value="${hide}"/>">	         		 
		             		<div class="col-sm-4 col-md-4">
		             			<label id="captionDistrictOrBranch">Branch</label>
			             		<input type="text" ${isDisabled} id="branchNameFilter" name="branchNameFilter" class="form-control"
			             		onfocus="getBranchMasterAutoComp(this, event, 'divTxtShowBranchDataFilter', 'branchNameFilter','branchIdFilter','');"
								onkeyup="getBranchMasterAutoComp(this, event, 'divTxtShowBranchDataFilter', 'branchNameFilter','branchIdFilter','');"
								onblur="hideDistrictMasterDiv(this,'branchIdFilter','divTxtShowBranchDataFilter');"/>
								<input type="hidden" id="branchIdFilter" value="${userMaster.branchMaster.branchId}"/>
								<div id='divTxtShowBranchDataFilter'  onmouseover="mouseOverChk('divTxtShowBranchDataFilter','branchNameFilter')"
								 style=' display:none;position:absolute;z-index: 5000' class='result' ></div>	
			             	</div>		             
	          		</div>
	          		
					<div class="col-sm-2 col-md-2">					
						<button class="btn btn-primary  top25" style="width: 100%; padding: 6px;" type="submit" onclick="searchJobCat()">Search <i class="icon"></i></button>					
					</div>
        	  	</div>
       		 </form>
		</div>            
</div>

<div class="TableContent top15">        	
         <div class="table-responsive" id="jobCategoryGrid">          
         </div>            
</div> 

	<div class="row top5">
	  <div class="col-sm-8 col-md-8">			                         
 			<div class='divErrorMsg' id='errordiv' ></div>
	  </div>	
	</div>   
	<div id="jobCatDiv" class="hide" onkeypress="return chkForEnterSaveJobOrder(event);">
	   <c:if test="${entityID eq 2}">
	   	<input type="hidden" id="districtId" value="${districtId}"/>
	   </c:if>	
	   	<input type="hidden" name="jobcategoryId" id="jobcategoryId">
		<input type="hidden" id="entityID" value="${entityID}"/>
	  	
	  	<c:set var="isHQ" value=""/>
	  	<c:if test="${entityID eq 1}">		
	  	<c:set var="isHQ" value="hide"/>
	  	</c:if>	 
	  	 
	  	 
	    	<c:if test="${entityID eq 1}">		
	    	  <div class="row top3" id="SearchHeadQuarterTextboxDiv" >	 
		          		<div class="col-sm-5 col-md-5 " id="headQuarterClass">
	                        <label>Head Quarter<span class="required">*</span></label>
	                        <input class="form-control" type="text" id="onlyHeadQuarterName1" name="onlyHeadQuarterName"   value=""
	                          onfocus="getOnlyHeadQuarterAutoComp(this, event, 'divTxtHeadQuarterData1', 'onlyHeadQuarterName1','headQuarterHiddenId1','');"
	                          onkeyup="getOnlyHeadQuarterAutoComp(this, event, 'divTxtHeadQuarterData1', 'onlyHeadQuarterName1','headQuarterHiddenId1','');"
	                          onblur="hideHeadQuarterDiv(this,'headQuarterHiddenId1','divTxtHeadQuarterData1');setHeadQuarterId1(); checkforDistrictandJobCat();getQQuestionSetByHeadQuarter();"      />
	                                
	                          <div id='divTxtHeadQuarterData1' style=' display:none;position:absolute;'  onmouseover="mouseOverChk('divTxtHeadQuarterData1','onlyHeadQuarterName1')"  class='result' ></div>
	                          <input type="hidden" id="headQuarterHiddenId1" value="" />
	                          <input type="hidden" id="isBranchExistsHiddenId1" value=""/>    
	                     </div>
	          		
	          		</div>
	    	</c:if>
	  	 
	  	 
	  	<c:if test="${branchSize ne 0}"> 
	  	 <div class="row top3 ">
	   	       	<div  class="col-sm-5 col-md-5">
			           		<label>Branch<span class="required ${isHQ}">*</span></label>
			          		<input type="text" id="branchName" name="branchName" class="form-control"
				             		onfocus="getBranchMasterAutoComp(this, event, 'divTxtShowBranchData', 'branchName','branchId','');"
									onkeyup="getBranchMasterAutoComp(this, event, 'divTxtShowBranchData', 'branchName','branchId','');"
									onblur="hideDistrictMasterDiv(this,'branchId','divTxtShowBranchData');  getJobCategoryByHQAndBranch(); checkforDistrictandJobCat(); "/>								
									<div id='divTxtShowBranchData'  onmouseover="mouseOverChk('divTxtShowBranchData','branchName')"
									 style=' display:none;position:absolute;z-index: 5000' class='result' ></div>
									 <input type="hidden" id="branchId" value=""/>
				 </div> 		     		   
		    </div> 
	       </c:if>
	       
	     
	      <c:if test="${entityID eq 1}">		
	       <div class="row top5"> 
	   	   	<div class="col-sm-6 col-md-6"><label><strong>Job Category <span class="required">*</span></strong></label>
				<label class="radio" style="min-height:0px;margin-bottom:-6px;margin-top:6px;"><input type="radio" name="jobCatRadio" id="jobCatRadio1"/></label>
				<input type="text" name="jobcategory" id="jobcategory" maxlength="100" class="form-control" placeholder="" style="width:82%;margin-left: 20px;">
			</div>
			<div class="col-sm-5 col-md-5"><label><strong><span class="required">&nbsp;</span></strong></label>
                <label class="radio" style="min-height:0px;margin-bottom:-6px;margin-top:6px;"><input type="radio" name="jobCatRadio" id="jobCatRadio2"/></label>
<!--                <input type="text" name="jobcategory" id="jobcategory" maxlength="100" class="form-control" placeholder="" style="width:82%;margin-left:20px;">-->
                <select id="parentJobCategoryId" name="parentJobCategoryId" disabled="disabled" class="form-control" style="width:82%;margin-left:20px;">
                    <option value="0">Select Job Category</option>
                    <c:forEach var="jbCategory" items="${lstJobCategoryMasters}">
                        <option value="${jbCategory.jobCategoryId}">${jbCategory.jobCategoryName}</option>              
                    </c:forEach>
                </select>
            </div>			       	
	      </div>
	      <div class="row" id="subJobCatDiv">
	      	    <div class="col-sm-5 col-md-5"><label><strong>Sub Job Category<span class="required">*</span></strong></label>
	                <input type="text" name="subjobcategory" id="subjobcategory" maxlength="100" class="form-control" placeholder="" style="width:88%">             
	            </div>
	      </div>
	      </c:if>
	      
	      
	      <c:if test="${entityID ne 1}">
	      <div class="row top5 "> 
	   	   	<div class="col-sm-5 col-md-5">
                <label><strong>Job Category<span class="required">*</span></strong></label>
                <select id="parentJobCategoryId" name="parentJobCategoryId" class="form-control" style="width:100%;">
                    <option value="0">Select Job Category</option>
                    <c:forEach var="jbCategory" items="${lstJobCategoryMasters}">
                        <option value="${jbCategory.jobCategoryId}">${jbCategory.jobCategoryName}</option>              
                    </c:forEach>
                </select>
            </div>
            <c:if test="${entityID eq 5}">
            	<c:if test="${kellyLiveFlag == 'yes'}">
	            	<div class="col-sm-3 col-md-3">
	                	<label><strong>Primary Job Code<span class="required">*</span></strong></label>
	                	 <input type="text" name="PrimaryJobCodename" id="PrimaryJobCodeId" class="form-control"/>             
	                </div>
                </c:if>
                <c:if test="${kellyLiveFlag == 'no'}">
	            	<div class="col-sm-3 col-md-3">
	                	 <input type="hidden" name="PrimaryJobCodename" id="PrimaryJobCodeId" class="form-control"/>             
	                </div>
                </c:if>
            </c:if>			       	
	      </div>
	       <div class="row hide" id="subJobCatDiv">
	      	    <div class="col-sm-5 col-md-5"><label><strong>Sub Job Category<span class="required">*</span></strong></label>
	                <input type="text" name="subjobcategory" id="subjobcategory" maxlength="100" class="form-control" placeholder="" style="width:100%">             
	            </div>
	      </div> 
	       </c:if>
	       
	    <!--   <c:if test="${entityID eq 1}">
	       <div class="row top5">
	       <div class="col-sm-6 col-md-6"><label><strong>Job Category <span class="required">*</span></strong></label>
				<input type="text" name="jobcategory" id="jobcategory" maxlength="100" class="form-control" placeholder="" style="width:82%;">
			</div>
			</div>
	       </c:if>   -->
	       
	       
	     	<input type="hidden" id="isGridLoad" value=""/>
	     	<input type="hidden" id="isPrescreenDefault" value=""/>
	     	<input type="hidden" id="isHqJsi" value="${isHqJsi}"/>
	     	
	     
	     
	     <div class="row" id="districtDiv">
                     <div class="col-sm-3 col-md-3 mt10 left20"> 
	                        <div id="selectDistrictDiv">
	                          <label class="radio inline" style="padding-left: 0px;">
								<input type="radio" name="selectDistrict" id="selectDistrict" value="1" onclick="uncheckedOtherRadio(4)"  />
	                            Selected Districts </label>
	                        </div>
	                 </div>    
                            <c:set var="showHideAddSchoolLink" value="hide"></c:set>		        			
			         			<c:set var="showHideAddSchoolLink" value=""></c:set>
			         		<div class="col-sm-6 col-md-6 left18 mt10 ">
				        		<div class="pull-right hide" id="addSchoolLink">
			                        <a href="javascript:void(0);" onclick="addDistictShow();">+ Add District</a>
	                             </div> 
			        		</div>
                        
                  </div>
                  <input type="hidden" id="gridNo" name="gridNo" value="">
                  <%-- ======== Branch District Grid ========= --%>
                  <div id="hqGridDistrict">
                 <div class="row col-sm-9 col-md-9 mt10">
                 	 <div  id="hqDistrict"  style="padding-bottom: 10px;" onclick="getHQBDGrid()">
                     </div>
                 </div>                   
                  <div id="addDistrictDiv" class="hide">
	                  <div class="row col-sm-10 col-md-10" style="max-width:755px;">
	                    <div>
	                      <label>District Name</label>
	             				<input type="text" id="districtName" name="districtName" disabled=""  class="form-control"
			             		onfocus="getDistrictMasterAutoComp(this, event, 'divTxtShowData', 'districtName','districtId','');"
								onkeyup="getDistrictMasterAutoComp(this, event, 'divTxtShowData', 'districtName','districtId','');"
								onblur="hideDistrictMasterDiv(this,'districtId','divTxtShowData');"	/>
								<input type="hidden" id="districtId" value=""/>
							<div id='divTxtShowData' style='display:none;position:absolute;z-index:5000;' onmouseover="mouseOverChk('divTxtShowData','districtName')" class='result' ></div>	
							
	                    </div>
	                  </div>
	                  <div class="row col-sm-5 col-md-5">
                 		 <div class="span2 idone"><a href="javascript:void(0);" onclick="return addDistrict();"><spring:message code="lnkImD"/></a>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onclick="clearHqBranchDistricts()">Cancel</a></div>
                	  </div>
                  </div>
	     </div>
	     
	     <div class="row">
	     </div>
	     
	        
	      <div class="row">
			<div class="col-sm-5 col-md-5 checkbox inline" style="padding-left: 15px;">
				<label style="font-weight: bold;">Job Category Privacy</label>
				<div class="left20">
					<input type="checkbox" name="inviteonly" id="inviteonly"  onclick="unableOrDisableInviteJob();">
					<label><strong>Job orders in this job category are hidden.</strong></label>
				</div>
			</div>
		</div>
		
			<div class="row left5 hide hiddenInviteDiv">
	            <div class="col-sm-4 col-md-4">
			         <label class="radio">
                       <input type="radio" name="hiddenJob" id="hiddenJob1" value="0" ${hiddenJobRadio1Checked}> This job category is invite-only.
                     </label>
                 </div>
                 <div class="col-sm-8 col-md-8 ">
                    <label class="radio">
                      <input type="radio" name="hiddenJob" id="hiddenJob2" value="1" ${hiddenJobRadio2Checked}> This job category is private with public job links.
                   </label>
                 </div>
	         </div>
	      	
	      	<div class="row">
			<div class="col-sm-5 col-md-5 checkbox inline" style="padding-left: 15px;">
				<label style="font-weight: bold;">Job Category Requirements</label>
				<div class="left20">
					<input type="checkbox" name="epicheck" id="epicheck"  onclick="showHideEpiGroup();">
					<label><strong>EPI</strong></label>
				</div>
				
			</div>
		</div>
	      	
		<!-- Select EPI Group Strat -->
	    <c:if test="${not empty userMaster}">
			<c:if test="${userMaster.entityType eq 1}">
				<div class="row hide epiGroupDiv">
					<div class="col-sm-6 col-md-6"><label class="mb0"><strong>Select EPI Group <span class="required">*</span></strong></label>
						<select id="assessmentGroupId" name="assessmentGroupId" class="form-control">
							<option value="0">Select EPI Group</option>
						</select>
					</div>
				</div>
			</c:if>
		</c:if>
		<c:if test="${not empty userMaster}">
			<c:if test="${(userMaster.entityType eq 5) || (userMaster.entityType eq 6)}">
				<div class="row hide epiGroupDiv">
					<div class="col-sm-6 col-md-6"><label class="mb0"><strong>Select EPI Group <span class="required">*</span></strong></label>
						<select id="assessmentGroupId" name="assessmentGroupId" class="form-control" disabled="disabled">
							<option value="0">Select EPI Group</option>
						</select>
					</div>
				</div>
			</c:if>
		</c:if>
	    <!-- Select EPI Group End -->
		
		<div class="row hide">
			<div class="col-sm-12 col-md-12 checkbox inline top1" >
					<span class="col-sm-4 col-md-4">
						<input type="checkbox" name="offerJSI" id="offerJSI">
						<label><strong>Prescreen</strong></label>
					</span>					  
			</div>
		</div>
		<div class="hide" id="JSIRadionDiv">
			<div class="row hide left5">
					<span class="col-sm-4 col-md-4">
				        <label  class="radio">
		                     <input type="radio" name="offerJSIRadio" id="offerJSIRadioMaindatory">
		                     <strong>Prescreen Is Mandatory</strong>
		                 </label>
		            </span>	
		              <span class="col-sm-4 col-md-4">
		                 <label class="radio">
		                  <input type="radio" name="offerJSIRadio" id="offerJSIRadioOptional">
							<strong>Prescreen Is Optional</strong>
		                </label>
		              </span>	
			</div>	
		<div class="row">
			<div class="col-sm-12 col-md-12" >
					<span class="col-sm-4 col-md-4">						
						<label><strong>Attach Prescreen</strong></label>
					</span>					  
			</div>
		</div>
		
		<div class="row left1" style="padding-bottom: 10px;">
			<div class="col-sm-5 col-md-5" id="jsiDiv">				
				<form id='frmJsiUpload' enctype='multipart/form-data' method='post' target='uploadFrame' action='jobCategorywiseHeadJsiUploadServlet.do' class="form-inline">
				<div>
					<span>
					 	<input type="hidden" id="districtHiddenId" name="districtHiddenId" value="${districtId}"/>
					 	 <input type="hidden" id="tempHiddenId" name="tempHiddenId" />
						<input type="hidden"  id="jsiFileName" name="jsiFileName" value="" />
						<input name="assessmentDocument" id="assessmentDocument" type="file">
					</span>
				</div>
				</form>
				<div id="removeJsi" name="removeJsi" style="display: none">
					<div style='width:20px;padding-left: 0px;' class='col-sm-2 col-md-2'  id="divJsiName"></div>
					<div class="col-sm-1 col-md-1">
					<c:if test="${entityID ne 6}">	
					<a href="javascript:void(0)" onclick="removeJsi()">Remove</a></c:if>	
					</div>
					<div class="col-sm-1 col-md-1 left20 top5">
						 <a href="#" id="iconpophover" rel="tooltip" data-original-title="Remove Jsi">
						 <img src="images/qua-icon.png" width="15" height="15" alt="">
					     </a>
					</div>
				</div>
				<iframe src="" id="ifrmJsi" width="100%" height="480px" style="display: none;">
					</iframe> 
		    </div>
		</div>
		</div>		
		
		<div class="row">
			<div class="col-sm-12 col-md-12 checkbox inline top1" >
					<span class="col-sm-4 col-md-4">
						<input type="checkbox" name="offerDSPQ" id="offerDSPQ">
						<label><strong>New Talent Application</strong></label>
					</span>					  
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12 col-md-12 checkbox inline top1" >
					<span class="col-sm-4 col-md-4" id="hireSmartPractices">
						<input type="checkbox" name="smartPractices" id="smartPractices" onclick="preHireDiv();">
						<label><strong>Pre-Hire Smart Practices</strong></label>
					</span>					  
			</div>
			<div class="hide" id="preHireDiv">	
				 <div class="col-sm-6 col-md-6 " style="border: 0px solid red;">
						<label>Smart Practice Compliance</label>
						<select id="preHireList" name="preHireList" class="form-control"> 
					    </select>
				</div>
			</div>
		</div>
			<div class="hide">
					<div class="row mt5"">
						<div class="col-sm-12 col-md-12 checkbox inline top1" id="" >
						<label style="margin-left: -5px;">Online Assessment(s)</label></br>
						<span class="col-sm-4 col-md-4" >
									<input type="checkbox" name="offerAssessmentInviteOnly" id="offerAssessmentInviteOnly" onclick="showOfferAMT();">
									<label><strong>Offer Assessment To Candidates On Invite Only</strong></label>
					</span>
			           				
						</div>
					</div>
					<div class="left15 hide" id="multiDisAdminDiv">
							   	<div class="row left5">
										<div class="col-sm-5 col-md-5" style="border: 0px solid red;">
											<label>Select District Assessment</label>
										</div>
									</div>
									<div class="row left5">
										 <div class="col-sm-5 col-md-5" style="border: 0px solid red;">
											<div id="1stSchoolDiv">
												<select multiple id="lstDistrictAdmins" name="lstDistrictAdmins" class="form-control" style="height: 150px;" > 
												</select>
											</div>
										</div>		
										<div class="col-sm-1 col-md-1 left20-sm"> 
											<div class="span2"> <span id="addPop" style="cursor:pointer;"><img alt="" src="images/rightarrow.jpg" width="30px"> </span></div>
											<div class="span2"> <span id="removePop" style="cursor:pointer;"><img alt="" src="images/leftarrow.jpg" width="30px"> </span></div>
										</div>
										<div class="col-md-5 col-md-5" style="border: 0px solid green;">
											<div id="2ndSchoolDiv">
												<select multiple class="form-control" id="attachedDAList" name="attachedDAList" style="height: 150px;">
												</select>
											</div>
										</div>	
									</div>
							</div>
					    </div>
		<!-- 
		<div class="row">
			<div class="col-sm-12 col-md-12 checkbox inline top1" >
					<span class="col-sm-4 col-md-4" id="qQuestion">
						<input type="checkbox" name="qualificationQuestion" id="qualificationQuestion">
						<label><strong>Qualification Question</strong></label>
					</span>					  
			</div>
		</div>
		 -->
		 
		<div class="row">
			<div class="col-sm-12 col-md-12 checkbox inline top1" >
					<span class="col-sm-4 col-md-4" id="qQuestion">
						<input type="checkbox" name="qualificationQuestion" id="qualificationQuestion" onclick="qqSetDiv();">
						<label><strong>Qualification Question Set</strong></label>
					</span>					  
			</div>
			<div class="hide" id="qqSetDiv">	
				 <div class="col-sm-6 col-md-6 " style="border: 0px solid red;">
						<label>Set Qualification Question Set to Job Category</label>
						<select id="qqAvlbList" name="qqAvlbList" class="form-control"> 
						   
					    </select>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div id="qqSetDiv">	
				 <div class="col-sm-6 col-md-6 " style="border: 0px solid red;">
						<label>e-Reference Question Set</label>
						<select id="eRefQuestionSetId" name="eRefQuestionSetName" class="form-control"> 
						   
					    </select>
				</div>
			</div>
		</div>
				
		 <div class="row">
		  	<div class="col-sm-5 col-md-5 top10">
		     		<c:if test="${entityID ne 6}">	
		     		    <c:if test="${(fn:indexOf(roleAccessU,'|1|')!=-1) || (entityID eq 1)}">
		     			<button onclick="saveJobCategory();" class="btn btn-primary"><strong>Save <i class="icon"></i></strong></button>
		     		</c:if>	
		     		</c:if>
		     	 &nbsp;<a href="javascript:void(0);" onclick="cancelJobCateDiv()">Cancel</a>  
		    </div>
		</div>
		</div>
		
		<div style="display:none; z-index: 5000;" id="loadingDiv" >
	     <table  align="center" >
	 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
	 		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
		</table>
	    </div>
	    
	    
<!--	 Modal for displaying job Category  history  -->
<div  class="modal hide"  id="jobCategoryHistoryModalId" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
	<div class="modal-dialog" style="width: 850px;">
	<div class="modal-content">	
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick="hideById('jobCategoryHistoryModalId')">x</button>
		<h3 id="myModalLabel">Job Category History</h3>
	</div>
	<div class="modal-body" style="max-height: 450px;overflow-y: auto">		
		<div class="control-group">
			<div class="" id="divJobHisTxt">
			
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
<!-- 		 <button class="btn btn-primary" aria-hidden="true" onclick='printCommunicationslogs();'>&nbsp;&nbsp;&nbsp;Print&nbsp;&nbsp;&nbsp;</button>&nbsp;&nbsp;-->
 		<button class="btn" data-dismiss="modal" aria-hidden="true"   onclick="hideById('jobCategoryHistoryModalId')">Close</button> 		
 	</div>
   </div>
 </div>
</div>
	    
	    

<script>
	displayOrHideSearchBox();
	displayJobCategories();
	getReferenceByDistrictList();
	//getReferenceByDistrictAndJobCategoryList();
	$('#iconpophover').tooltip();
</script>
<!-- add by ankit -->
<script>
		$('#imiconpophover').tooltip();
</script>
<!--add end by ankit -->

<script type="text/javascript">
	$('#addPop').click(function() {
		if ($('#lstDistrictAdmins option:selected').val() != null) {
			 $('#lstDistrictAdmins option:selected').remove().appendTo('#attachedDAList');
			 $("#attachedDAList").attr('selectedIndex','-1').find("option:selected").removeAttr("selected");
	         $("#attachedDAList").attr('selectedIndex', '-1').find("option:selected").removeAttr("selected");
	 }
	});
	
	$('#removePop').click(function() {
	       if ($('#attachedDAList option:selected').val() != null) {
	             $('#attachedDAList option:selected').remove().appendTo('#lstDistrictAdmins');
	           //  sortSelectList();
	             $("#attachedDAList").attr('selectedIndex',  '-1').find("option:selected").removeAttr("selected");
	             $("#lstDistrictAdmins").attr('selectedIndex', '-1').find("option:selected").removeAttr("selected");
	             $("#lstDistrictAdmins").attr('selectedIndex', '-1').addAttr("selected");
	             
	}
	});
	displayAllDistrictAssessment();
	showTemp();
	deleteTemp();
	
	 
	//getQQuestionSetByHeadQuarter();
</script>


