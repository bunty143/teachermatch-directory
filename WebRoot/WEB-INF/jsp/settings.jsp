<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<c:set var="readonlyForPortal" value="false" />
<c:choose>
	<c:when test="${epiStandalone}">
		<c:set var="redirectURL" value="portaldashboard.do" />
		<c:set var="readonlyForPortal" value="true" />
	</c:when>	
	<c:otherwise>
		<c:set var="redirectURL" value="userdashboard.do" />
	</c:otherwise>
</c:choose>

<div onkeypress="chkForEnter(event)" class="modal hide"  id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><spring:message code="btnX"/></button>
		<h3 id="myModalLabel"><spring:message code="headChngPass"/></h3>
	</div>
	<div class="modal-body">
		<div class="row col-sm-12 col-md-12">
			<div class='divErrorMsg' id='errordivPwd' style="display: block;"></div>
			
		</div>
		
		<div class="row col-sm-12 col-md-12">
			<div class="">
		    	<label><strong><spring:message code="lblCurrPass"/></strong><span class="required">*</span></label>
	        	<input id="oldpassword" type="password" class="form-control" maxlength="12" />
			</div>
		</div>
	            
        <div class="row col-sm-12 col-md-12">
			<div class="">
		    	<label><strong><spring:message code="lblNePass"/></strong><span class="required">*</span></label>
	        	<input id="newpassword" type="password" class="form-control" maxlength="12"/>
			</div>
		</div>
		<div class="row col-sm-12 col-md-12">
			<div class="">
		    	<label><strong><spring:message code="lblRe-EtrPass"/></strong><span class="required">*</span></label>
	        	<input id="repassword" type="password" class="form-control" maxlength="12" />
			</div>
		</div>
 	</div>
 	
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClr"/></button>
 		<button class="btn btn-primary" onclick="chkPassword()"><spring:message code="btnSvChng"/></button>
 	</div>
</div>
	</div>
</div>

<form:form commandName="teacherDetail" onsubmit="return validateEditSettings()" >

<div class="row col-sm-offset-3 col-md-offset-3 top15">	
		<div class="subheadinglogin"><spring:message code="lblSett"/></div>		
				<div class="row"> 
	      			<div class="col-sm-12 col-md-12">
	      				<div  id='divServerError' class='divErrorMsg' style="display: block;">${msgServer}</div>
						<div class='divErrorMsg' id='errordiv' style="display: block;"></div>
	              	</div>	              	
				</div>
				<div class="row"> 
	      			<div class="col-sm-4 col-md-4">
	      				<label><strong><spring:message code="lblFname"/></strong><span class="required">*</span></label>
	                      <c:choose>              
							  <c:when test="${isKelly eq 'true'}">					
							   	<form:input path="firstName" class="form-control" readonly="${readonlyForPortal}" maxlength="40"/>
							  </c:when>
							  <c:otherwise>
				    		    <form:input path="firstName" class="form-control" readonly="${readonlyForPortal}" maxlength="50"/>
					 		  </c:otherwise>
						</c:choose>
	              	</div>
	              	<div class="col-sm-4 col-md-4">
	      				<label><strong><spring:message code="lblLname"/></strong><span class="required">*</span></label>
	                	  <c:choose>              
							  <c:when test="${isKelly eq 'true'}">					
									   <form:input path="lastName" class="form-control" readonly="${readonlyForPortal}" maxlength="40"/>
								</c:when>					
								<c:otherwise>
								    	<form:input path="lastName" class="form-control" readonly="${readonlyForPortal}" maxlength="50"/>
								</c:otherwise>		
						</c:choose>
	              	</div>
				</div>
		       <div class="row">
		    	<div class="col-sm-8 col-md-8">
		      		<label><strong><spring:message code="lblEmail"/></strong><span class="required">*</span></label>
		           	<form:input path="emailAddress" readonly="${readonlyForPortal}" cssClass="form-control" maxlength="75" />
				</div>
				</div>
		
	            <div class="row">
		    	<div class="col-sm-4 col-md-4">
		        	<label><strong><spring:message code="lblPass"/></strong><span class="required">*</span></label>		        	
			        	<form:password path="password" class="form-control" maxlength="12" readonly="true" value="0123456789" />  
			       	</div>
					<div class="col-sm-3 col-md-3 top30-sm">
			        	    	<a href="#" onclick="return showChangePWD()"><spring:message code="lnkChngPass"/></a>
					</div>
				 </div>	
			
			   <div class="row">
		    	<div class="col-sm-8 col-md-8">
		      		<label><strong><spring:message code="lblAuthPin"/></strong></label>
		           	<form:input path="verificationCode" class="form-control" maxlength="8" disabled="true" />
				</div>
				</div>
		
		
		         <div class="row col-sm-3 col-md-3 top20">	        	       	
		          <c:if test="${epiStandalone ne true}">
					<button class="btn btn-primary" type="submit" ><spring:message code="btnSub"/> <i class="icon"></i></button>	
				</c:if>      		           
	           &nbsp;<a href="${redirectURL}"><spring:message code="btnClr" /></a>
	             </div>
</div>
<div class="clearfix"></div>

</form:form> 
<br/><br/>

<div style="display:none; z-index: 5000;" id="loadingDiv">
    <table  align="center" >
 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
 		<tr id='paymentMessage'><td style='padding-top:0px;' id='spnMpro' align='center'></td></tr>
	</table>
</div>


<script type="text/javascript" src="js/teacher/user.js">
</script>
<script>
	document.getElementById("firstName").focus();
	$('#myModal').modal('hide');
</script>