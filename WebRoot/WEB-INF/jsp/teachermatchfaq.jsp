	<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
	
	
	<style>
	
	#question
	{
		
	font-weight: bold;
	
	
	color: black;
	}
	
	p
	{

		text-align: justify;
		padding-right: 5px;
		/*text-indent: 20px;*/
		
		
	}
	.spnIndent
	{
		padding-left: 20px;
	}
	.spnIndent40
	{
		padding-left: 40px;
	}
	ul
	{
	margin-right: 10px;
	}
	li
	{
	
		text-align: left;
		padding-right: 5px;
		/*text-indent: 20px;*/
		
	
	}
</style>
			
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	<div class="container">
			 <div class="row centerline">
					<img src="images/affidavit.png" width="47" height="48" alt="">
					<b class="subheading" style="font-size: 13px;">
					<spring:message code="lblFAQ"/>
				    </b>
		     </div>
		     
		     
		      <div style="text-align: center; padding-bottom: 5px;font-family:Times New Roman;font-weight:bold;font-size:16px;">
		    </br>
		  
		   <spring:message code="msgEPI"/><br>

<spring:message code="lblFAQ"/>

		    </div>	
		     
		    	
		</div>
		
		
		        <div class="container top15">
             <div class="row" align="justify">                                                   
          <div class="right10"  data-target="#navbarExample" data-spy="scroll">
			
			<p id="question"><u><spring:message code="qnFAQ1"/></u></p>
		
		
	 <p><spring:message code="ansqnFAQ1_1"/>
<br><br>
<spring:message code="ansqnFAQ1_2"/> 
	 </p>
			
	
	<p id="question"><u><spring:message code="qnFAQ2"/></u></p>
	
	<p><spring:message code="ansqnFAQ2"/></p>
	
	
	
	
	
	
	<p id="question"><u><spring:message code="qnFAQ3"/></u></p>
<p><spring:message code="ansqnFAQ3"/></p>

<p id="question"><u><spring:message code="qnFAQ4"/></u></p>
<p><spring:message code="ansqnFAQ4"/></p>
	
	
<p id="question"><u><spring:message code="qnFAQ5"/></u></p>
<p><spring:message code="ansqnFAQ5_1"/>
<br>
<spring:message code="ansqnFAQ5_2"/> 
<br>
<spring:message code="ansqnFAQ5_3"/></p>
	
	<ul>
	<li><spring:message code="ansqnFAQ5_4"/></li>
	<li><spring:message code="ansqnFAQ5_5"/></li>
	<li>Make sure that you have at least 90 minutes of uninterrupted time to complete the EPI.-->
	</li>
	</ul>
	
	
	
	<ul class="left50 top0">
	<li><u>Do not</u> close your browser or hit the "back" button on your browser</li>
	<li>You are <u>not</u> able to skip questions.</li>
	
	
	
	
	</ul>
	
<p>

<spring:message code="ansqnFAQ5_6"/>
</p>	
	
	
	
	<p id="question"><u><spring:message code="qnFAQ6"/></u></p>
<p><spring:message code="ansqnFAQ6"/></p>

<p id="question"><u><spring:message code="qnFAQ7"/></u></p>
<p><spring:message code="ansqnFAQ7"/>  applicants@teachermatch.org.</p>

<p id="question"><u><spring:message code="qnFAQ8"/></u></p>
<p><spring:message code="ansqnFAQ8"/></p>


<p id="question"><u><spring:message code="qnFAQ9"/></u></p>
<p><spring:message code="ansqnFAQ9"/> </p>

<p id="question"><u><spring:message code="qnFAQ10"/></u></p>
<p><spring:message code="ansqnFAQ10"/></p>

<p id="question"><u><spring:message code="qnFAQ11"/></u></p>
<p><spring:message code="ansqnFAQ11"/></p>

<p id="question"><u><spring:message code="qnFAQ12"/></u></p>
<p><spring:message code="ansqnFAQ12"/></p>

<p id="question"><u><spring:message code="qnTMFAQ"/></u></p>
<p><spring:message code="ansTMFAQ13"/></p>

	
	
	
			
			
			
		
		</div>  
		
		
		
      </div>
      				  
	</div>