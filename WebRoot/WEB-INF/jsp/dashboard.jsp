<!-- @Author: Gagan 
 * @Discription: view of Admin dashboard Page.
 -->
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>

<script type="text/javascript" src="dwr/interface/DAorSADashboardAjax.js?ver=${resourceMap['DAorSADashboardAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type='text/javascript' src="js/dasadashboard.js?ver=${resourceMap['js/dasadashboard.js']}"></script>

<!--    <h1>Welcome Gagan 1</h1> <br>-->
<div class="container">
	<div class="row  mt10 mb">
		<div class="span16 centerline ">
			<div class="span1 m0"><img src="images/dashboard-icon.png" width="41" height="41" alt=""></div>
			<c:if test="${userSession.entityType eq 2}">
				<c:set var="dashboardName" value=""></c:set>
			    <c:set var="dashboardName" value="District"></c:set>        		
			</c:if>
			<c:if test="${userSession.entityType eq 3}">
				<c:set var="dashboardName" value=""></c:set>
			    <c:set var="dashboardName" value="School"></c:set>        		
			</c:if>
			<div class="span10 subheading">${dashboardName} Dashboard</div>
		</div>
	</div>

<div class="row">
	<div class="box span8">
		<table width="100%" border="0" class="table table-bordered">
			<thead class="bg">
		  	<tr>
		    	<th><img src="images/snapshot.png" width="30" height="29" class="fl districticon"> 
		    	<div class="span7 pl5"><h4 class="clearfix m0">Snapshot</h4><br/><span class="clearfix pull-left font_normal">Year to Date</span>
				</div>
				</th>
		    </tr>
		  </thead>
		  <tr>
		    <td class="dash_td" id="snapshot">
		   
		    </td>
		  </tr>
		</table>
	</div>
	<div class="box span8">
		<table width="100%" border="0" class="table table-bordered">
		  <thead class="bg">
		  <tr>    
		    <th><img src="images/joborders.png" width="33" height="35" class="fl districticon"> 
		    	<div class="span7 pl5"><h4 class="clearfix m0">Applicants By Education Level</h4><br/><span class="clearfix pull-left font_normal">Year to Date</span>
				</div>
			</th>
		    </tr>
		  </thead>
		  <tr>
		    <td class="dash_td">
		    <h3>Applicant Education Status</h3>
		    <p>&nbsp;</p>
		    <p class="pagination-centered" id="chart1"><img src="${topRightCornerImg}" width="300" alt="circle graph"></p>
		    </td>
		  </tr>
		</table>
	</div>
</div>
<div class="row">
	<div class="box span8">
		<table width="100%" border="0" class="table table-bordered">
		  <thead class="bg">
		  <tr>
		    <th><img src="images/favourite-teacher.png" class="fl districticon"> 
		    	<div class="span7 pl5"><h4 class="clearfix m0">Hired Applicants By National Board Certification/Licensure</h4><br/><span class="clearfix pull-left font_normal">Year to Date</span>
				</div>
			</th>
		    </tr>
		  </thead>
		  <tr>
		    <td class="dash_td">
		    <h3>National Board Certification/Licensure Status</h3>
		    <p>&nbsp;</p>
		    <p class="pagination-centered" id="chart2"><img src="${bottomLeftCornerImg}" width="300" alt="circle graph"></p>
		    </td>
		    </tr>
		</table>
	</div>
	<div class="box span8">
		<table width="100%" border="0" class="table table-bordered">
		  <thead class="bg">
		  <tr>
		    <th><img class="fl districticon" src="images/hiring.png"> 
		    	<div class="span7 pl5"><h4 class="clearfix m0">Applicants By CGPA</h4><br/><span class="clearfix pull-left font_normal">Year to Date</span>
				</div>
			</th>
		    </tr>
		  </thead>
		  <tr>
		    <td class="dash_td">
		    <h3>Applicant Distribution by CGPA</h3>
		    <p>&nbsp;</p>
		    <p class="pagination-centered" id="chart3"><img src="${bottomRightCornerImg}" width="300" alt="circle graph"></p>
		    </td>
		    </tr>
		</table>
	</div>
</div>
</div>
<script type="text/javascript">
getDashboardCharts();
getDashboardSnapshot();
</script>