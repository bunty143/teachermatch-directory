<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="javax.swing.Spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type="text/javascript" src="dwr/interface/DSPQServiceAjax.js?ver=${resourceMap['DSPQServiceAjax.ajax']}"></script>
<script type="text/javascript" src="js/jobapplicationflow/group01.js?ver=${resouceMap['js/jobapplicationflow/group01.js']}"></script>

<script src="calender/js/jscal2.js"></script>
<script src="calender/js/lang/en.js"></script>
<link rel="stylesheet" type="text/css" href="calender/css/jscal2.css" />
<link rel="stylesheet" type="text/css" href="calender/css/border-radius.css" />
<link rel="stylesheet" type="text/css" href="calender/css/gold/gold.css" />

<p id="portfolioHeader"></p>
<input type="hidden" id="sNextURL" name="sNextURL" value="${sNextURL}"/>
<input type="hidden" id="sPreviousURL" name="sPreviousURL" value="${sPreviousURL}"/>
<input type="hidden" id="sCurrentPageShortCode" name="sCurrentPageShortCode" value="${sCurrentPageShortCode}"/>
<div class="hide" id="divLCL">${latestCoverletter}</div>

<input type="hidden" id="isMiami" name="isMiami" value="${isMiami}"/>
<div class="row" style="margin-left: 0px;margin-right: 0px;">
         <div style="float: left;">
         	<img src="images/personalinformation.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;"><spring:message code="DSPQlblCoverLetter"/></div>	
         </div>	
         
         <div style="float: right;">
          	<div style='text-align:right; padding-right: 0px;' class="col-sm-3 col-md-3">
          		<button class='flatbtn' style='width:190px;'  type='button' onclick='return validateDSPQPersonalGroup01Section01(0);'><spring:message code="btnSaveAndNextDSPQ"/> <i class='icon'></i></button>
          	</div>
         </div>
         
         <div style="float: right;">
          	<div style='text-align:right; padding-right: 0px;' class="col-sm-3 col-md-3">
          		<button class='flatbtn' style='width:190px;'  type='button' onclick='return oneBtnJobApply();'><spring:message code="DSPQ_btnOneBtnJobApply"/> <i class='fa fa-plane'></i></button>
          	</div>
         </div>
         	
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
</div>
				
<p id="group01section01"></p>

<div class="modal hide"  id="oneBtnJobApplyModelId"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false" onclick="jafUnDoneRedirection()">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
		  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div id="deact" class="modal-body"> 		
				<div class="control-group">
				<p id="jafoneBtnJobApplyUnDoneMsgId" class="DSPQRequired12"></p>
				</div>
		 	</div>
	 	 <div class="modal-footer">
	 	     <span><button class="btn  btn-primary" onclick="jafUnDoneRedirection()" ><spring:message code="btnOk" /> <i class="icon"></i></button></span>
	 	</div>
	  </div>
	</div>
</div>

<div class="modal hide"  id="oneBtnJobApplyModelIdL2"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false" onclick="closeCompleteQuickBtn">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
		  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body"><div class="control-group"><p>This job is already completed.</p></div></div>
	 	 <div class="modal-footer">
	 	     <span><button class="btn  btn-primary" onclick="closeCompleteQuickBtn()" ><spring:message code="btnOk" /> <i class="icon"></i></button></span>
	 	</div>
	  </div>
	</div>
</div>

<!-- start ... Attach jaf jsp files -->
<jsp:include page="jafinnercommon.jsp"></jsp:include>
<!-- end ... Attach jaf jsp files -->

<script type="text/javascript">//<![CDATA[
 var cal = Calendar.setup({
  onSelect: function(cal) { cal.hide() },
  showTime: true
 });
 //]]>
    
</script>

<script>
var dspqPortfolioNameId=${dspqPortfolioName.dspqPortfolioNameId};
var candidateType="${candidateType}";
var groupId=${group};
var sectionId=${section};
var iJobId=${iJobId};
getPortfolioHeader(iJobId,dspqPortfolioNameId,candidateType,groupId);
getGroup01Section01(iJobId,dspqPortfolioNameId,candidateType,groupId,sectionId);
//getGroup01_SectionName01(iJobId,dspqPortfolioNameId,candidateType,groupId,sectionId);
</script>