<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<script type="text/javascript" src="dwr/interface/SchoolAjax.js?ver=${resourceMap['SchoolAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type="text/javascript" src="dwr/interface/DistrictAjax.js?ver=${resourceMap['DistrictAjax.ajax']}"></script>
<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" /> 
<script type="text/javascript" src="js/managedocuments.js?ver=${resourceMap['js/managedocuments.js']}"></script>
<script type="text/javascript" src="dwr/interface/AutoSearchFilterAjax.js?ver=${resourceMap['AutoSearchFilterAjax.Ajax']}"></script>

<script type="text/javascript">
var $j=jQuery.noConflict();
        $j(document).ready(function() {
        });
        
function applyScrollOnTblDistAttachment()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#distAttachmentTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        //height: 280,
        width: 920,
        minWidth: null,
        minWidthAuto: false,
        <c:choose>
        	<c:when test="${kellyupdatecodelive eq 'yes' || (userMaster.entityType!=5 && userMaster.entityType!=6)}">
        		colratio:[200,200,140,140,120,120], // table header width
        	</c:when>
        	<c:otherwise>
        		colratio:[240,240,200,120,120],
        	</c:otherwise>
        </c:choose>
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
   });			
}
</script>

<input type="hidden" value="${userMaster.roleId.roleId}" id="hiddenRoleId">
<div class="row" style="margin-left: 0px;margin-right: 0px;margin-top:">
         <div style="float: left;">
         	<img src="images/manageusers.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;"><spring:message code="headManageDoc"/></div>	
         </div>
          <div class="pull-right add-employment1">
			
			<c:if test="${userSession.roleId.roleId ne 12 && userSession.roleId.roleId ne 13 }">
						<a href="javascript:void(0);" onclick="return addNewDoc()"><spring:message code="lnkAddDoc"/></a>
		 </c:if>
		 </div>			
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
 </div>
 
  <c:if test="${not empty userMaster}">
		<c:if test="${userMaster.entityType eq 3}">	        			
			<c:set var="hide" value=""></c:set>
			<c:set var="hide" value="hide"></c:set>
		</c:if>
	</c:if>
	<c:set var="roothPath" value=""></c:set>
	<c:set var="idName" value=""></c:set>
	<c:set var="idVal" value=""></c:set>
	<c:if test="${(userMaster.entityType == 5 || userSession.entityType == 6)}">
	<input type="hidden" id="headQuarterId" value="${userMaster.headQuarterMaster.headQuarterId}"/>
	<c:set var="roothPath" value="hqRoothPath"></c:set>
	<c:set var="idName" value="hqId"></c:set>
	<c:set var="idVal" value="${userMaster.headQuarterMaster.headQuarterId}"></c:set>
	</c:if>
	<c:if test="${(userMaster.entityType == 6)}">
		<input type="hidden" id="branchId" value="${userMaster.branchMaster.branchId}"/>
		<c:set var="roothPath" value="branchRoothPath"></c:set>
		<c:set var="idName" value="branchId"></c:set>
		<c:set var="idVal" value="${userMaster.branchMaster.branchId}"></c:set>
	</c:if>

<div onkeypress="return chkForEnterRecordsByEntityType(event);">	
			<form class="bs-docs-example" onsubmit="return false;">
				<div class="row mt10 <c:out value="${hide}"/>">
				<div class='col-sm-12 col-md-12 divErrorMsg' id='errordiv' style="display: block;"></div>
					<div class="col-sm-3 col-md-3">
						<label><spring:message code="lblEntityType"/></label>
					    <select class="form-control " id="MU_EntityType" name="MU_EntityType" class="span3" onchange="displayOrHideSearchBox(<c:out value="${userMaster.entityType}"/>)">
						 <c:if test="${not empty userMaster}">
						 	 <c:if test="${userMaster.entityType eq 1}">	
						 	 	<option value="0" selected="selected"><spring:message code="optAll"/></option>
						 	 	<option value="5">HeadQuarter</option>
								<option value="6">Branch</option>
								<option value="2"><spring:message code="optDistrict"/></option>
								<option value="3"><spring:message code="optSchool"/></option>
							</c:if>	
			        		<c:if test="${userMaster.entityType eq 2}">	
			         			<option value="0" ><spring:message code="optAll"/></option>        			
			         			<option value="2"  selected="selected" ><spring:message code="optDistrict"/></option>
								<option value="3"><spring:message code="optSchool"/></option>
							</c:if>
			        		<c:if test="${userMaster.entityType eq 3}">	        			
								<option value="3" selected="selected"><spring:message code="optSchool"/></option>
 			        		</c:if>
 			        		<c:if test="${userMaster.entityType eq 5}">	        			
								<option value="5" selected="selected">HeadQuarter</option>
								<option value="6">Branch</option>
 			        		</c:if>
 			        		<c:if test="${userMaster.entityType eq 6}">	        			
								<option value="6">Branch</option>
 			        		</c:if>
			              </c:if>
						</select>			
	              	</div>
	              	
	              	<div class="col-sm-4 col-md-4 hide hqClass" id="headQuarterClass">
                    	<label>Head Quarter</label>
                    	<c:if test="${userMaster.entityType eq 5}">
                    	<input class="form-control clearback" type="text" id="onlyHeadQuarterName" name="onlyHeadQuarterName"   value="${headQuarterName}"/>
								<input type="hidden" name="headQuarterHiddenId" id="headQuarterHiddenId" value="${headQuarterId}"/>
								<input type="hidden" id="isBranchExistsHiddenId" value="${isBranchExistsHiddenId}"/>
						</c:if>
						<c:if test="${userMaster.entityType eq 1}">
                    	<input class="form-control clearback" type="text" id="onlyHeadQuarterName" name="onlyHeadQuarterName"   value="${headQuarterName}"
                    			onfocus="getOnlyHeadQuarterAutoComp(this, event, 'divTxtHeadQuarterData', 'onlyHeadQuarterName','headQuarterHiddenId','');"
								onkeyup="getOnlyHeadQuarterAutoComp(this, event, 'divTxtHeadQuarterData', 'onlyHeadQuarterName','headQuarterHiddenId','');"
								onblur="hideHeadQuarterDiv(this,'headQuarterHiddenId','divTxtHeadQuarterData');"	/>
								
								<div id='divTxtHeadQuarterData' style=' display:none;position:absolute;'  onmouseover="mouseOverChk('divTxtHeadQuarterData','onlyHeadQuarterName')"  class='result' ></div>
								<input type="hidden" name="headQuarterHiddenId" id="headQuarterHiddenId" value="${headQuarterId}"/>
								<input type="hidden" id="isBranchExistsHiddenId" value="${isBranchExistsHiddenId}"/>
						</c:if>	
					</div>
					<div class="col-sm-4 col-md-4 hide branchClass" id="branchClass">
                    	<label>Branch</label>
                    	
                    	<c:if test="${userMaster.entityType ne 6}">
                    	<input class="form-control clearback" type="text" id="onlyBranchName" name="onlyBranchName"   value="${branchName}"
                    			onfocus="getOnlyBranchAutoComp(this, event, 'divTxtBranchData', 'onlyBranchName','branchHiddenId','');"
								onkeyup="getOnlyBranchAutoComp(this, event, 'divTxtBranchData', 'onlyBranchName','branchHiddenId','');"
								onblur="hideBranchDiv(this,'branchHiddenId','divTxtBranchData');"	/>
						</c:if>
						<c:if test="${userMaster.entityType eq 6}">
						 
						<input class="form-control clearback" type="text" id="onlyBranchName" name="onlyBranchName"   value="${branchName}"		/>
						</c:if>		
								<div id='divTxtBranchData' style=' display:none;position:absolute;'  onmouseover="mouseOverChk('divTxtBranchData','onlyBranchName')"  class='result' ></div>
								<input type="hidden" name="branchHiddenId" id="branchHiddenId" value="${branchId}"/>	
					</div>
	              	
	              	<div class="col-sm-5 col-md-5 hide schoolClass" id="districtClass">
                    	<label><spring:message code="lblDistrict"/></label>	
                    	<input class="form-control" type="text" id="onlyDistrictName" name="onlyDistrictName"   value="${districtId}"
                    			onfocus="getDistrictMasterAutoComp(this, event, 'divTxtDistrictData', 'onlyDistrictName','districtHiddenId','');"
								onkeyup="getDistrictMasterAutoComp(this, event, 'divTxtDistrictData', 'onlyDistrictName','districtHiddenId','');"
								onblur="hideDistrictMasterDiv(this,'districtHiddenId','divTxtDistrictData');"	/>
								
								<div id='divTxtDistrictData' style=' display:none;position:absolute;'  onmouseover="mouseOverChk('divTxtDistrictData','onlyDistrictName')"  class='result' ></div>	
					</div>
		            <div  id="Searchbox" class="hide">
	         		    
		             		<div class="col-sm-5 col-md-5">
		             			<label id="captionDistrictOrSchool"><spring:message code="lblSchoolName"/></label>
			             		<input type="text" id="districtORSchoolName" name="districtORSchoolName" class="form-control"
			             		onfocus="getSchoolSearchAutoComp(this, event, 'divTxtShowDatas', 'districtORSchoolName','schoolHiddenId','');"
								onkeyup="getSchoolSearchAutoComp(this, event, 'divTxtShowDatas', 'districtORSchoolName','schoolHiddenId','');"
								onblur="hideDistrictMasterDiv(this,'schoolHiddenId','divTxtShowDatas');"	/>
							
								<div id='divTxtShowDatas'  onmouseover="mouseOverChk('divTxtShowDatas','districtORSchoolName')" style=' display:none;position:absolute;z-index: 5000' class='result' ></div>	
			             	</div>
			             	<input type="hidden" id="userSessionEntityTypeId" value="${userSession.entityType}"/>
							<input type="hidden" id="schoolSessionId" value="${userSession.schoolId.schoolId}"/>
			             	 <input type="hidden" id="districtSessionId" value="${userSession.districtId.districtId}"/>
			             	<input type="hidden" id="schoolHiddenId" value="${userMaster.schoolId.schoolId}"/>
			             	<input type="hidden" id="districtHiddenId" value="${userMaster.districtId.districtId}"/>
		            
	          		</div>
				</div>   
        	  	<c:if test="${userMaster.entityType eq 3}">
        	  	<div class="top10"></div>
        	  	</c:if>     	  	
       		 </form>
	</div>
	<input type="hidden" id="userEntityType" value="${userMaster.entityType}"/>
<div class="row">
				       <div class="col-sm-3 col-md-7">
				       <span class=""><label class=""><spring:message code="lbllDocName"/></label><input type="text" id="docname" name="docname"  maxlength="25"  class="help-inline form-control"></span>
				       </div>
				        <div class="col-sm-2 col-md-2">
				      
						<button class="btn btn-primary top25-sm2" type="button" onclick="searchAttachment()"><spring:message code="btnSearch"/><i class="icon"></i></button>
										      
				       </div>
				       
				       </div>
 <div class="row top15">
	                    <div class="col-sm-10 col-md-10">
	                    <div id="distAttachmentGrid">
		                  <table id="distAttachmentTable" width="100%" border="0" class="table table-bordered mt10">
		                  </table>
	                    </div>
	                   </div>
	                  </div>
<input type="hidden" id="gridNo" name="gridNo" value="">
<div class="row ">
	  <div class="col-sm-12 col-md-12">			                         
 			<div class='divErrorMsg' id='errornotediv' ></div>
	  </div>	
</div>    
 
 <div id="distAttachmentDiv" class="hide top10" >
	   
	   	<input type="hidden" id="districtId" value="${districtId}"/>
	   	<input type="hidden" id="schoolId" value="${schoolId}"/>
	   	<input type="hidden" id="headQuarterId" value="${headQuarterId}"/>
	   	<input type="hidden" id="branchId" value="${branchId}"/>

	
		  <div class="row ">
		   <c:if test="${entityID eq 1}">
          		<div  class="col-sm-5 col-md-5">
          		<label><spring:message code="lblDistrict"/><span class="required">*</span></label>
           		<input type="text" id="districtName" name="districtName"  class="form-control"
           		onfocus="getDistrictMasterAutoComp(this, event, 'divTxtShowData', 'districtName','districtId','');"
				onkeyup="getDistrictMasterAutoComp(this, event, 'divTxtShowData', 'districtName','districtId','');"
				onblur="hideDistrictMasterDiv(this,'districtId','divTxtShowData');"	/>
				<div id='divTxtShowData' style=' display:none;position:absolute;z-index:5000;'
				 onmouseover="mouseOverChk('divTxtShowData','districtName')" class='result' ></div>
				
           	</div>
           	</c:if>
           	
			  <c:if test="${entityID eq 1 || entityID eq 2}">
			    <div class="col-sm-5 col-md-5">
	          <label><spring:message code="lblSchoolName"/></label>
	           <input type="text" id="schoolName" maxlength="100" name="schoolName" class="form-control"
	           onfocus="getSchoolMasterAutoComp(this, event, 'divTxtShowData2', 'schoolName','schoolId','');"
				onkeyup="getSchoolMasterAutoComp(this, event, 'divTxtShowData2', 'schoolName','schoolId','');"
				onblur="hideSchoolMasterDiv(this,'schoolId','divTxtShowData2');"	/>	
			 	
				 <div id='divTxtShowData2'  onmouseover="mouseOverChk('divTxtShowData2','schoolName')" style=' display:none;position:absolute;z-index:5000;' class='result' ></div>
	      	       	<input type="hidden" id="schoolId" value=""/>
	      	      </div>
			  </c:if>
			   
			  <c:if test="${entityID eq 5}">
			  <div class="col-sm-5 col-md-5 hide" id="branchDiv">
                    	<label>Branch Name</label>
                    	<input class="form-control clearback" type="text" id="branchName" name="branchName"   value=""
                    			onfocus="getOnlyBranchAutoComp(this, event, 'divTxtBranchData2', 'branchName','bnchId','');"
								onkeyup="getOnlyBranchAutoComp(this, event, 'divTxtBranchData2', 'branchName','bnchId','');"
								onblur="hideBranchDiv(this,'bnchId','divTxtBranchData2');"	/>
								
								<div id='divTxtBranchData2' style=' display:none;position:absolute;'  onmouseover="mouseOverChk('divTxtBranchData2','branchName')"  class='result' ></div>
								<input type="hidden" name="bnchId" id="bnchId" value=""/>	
					</div>					
			  </c:if>
			  <c:if test="${entityID eq 6}">
			  	<input type="hidden" name="bnchId" id="bnchId" value="${branchId}"/>
			  </c:if>			  
			  <div class="col-sm-5 col-md-5 hide" id="documentDiv">
					<label>Document Name</label>
					<input class="form-control" type="text" name="docName" id="docName" value=""/>
			  </div>
			  
	      </div>
	  <div class="row">
	
		<form id='frmDistAttachmentUpload' enctype='multipart/form-data' method='post' target='uploadDistFrame' action='distAttachmentUploadServlet.do' class="form-inline">
			<div class="col-sm-4 col-md-4">
					<label>
						<spring:message code="lblUpDoc"/><span class="required">*</span>
					</label>
					<input type="file" name="distAttachmentDocument" id="distAttachmentDocument">
					<!--<a href="javascript:void(0)" onclick="clearDocFile()"><spring:message code="lnkClear"/></a>-->
			</div>
			<input type="hidden" id="editFileName"/>
			<input type="hidden" id="${idName}" value="${idVal}"/>
			<input type="hidden" id="roothPath" value="${roothPath}"/>
			<span class="col-sm-3 col-md-3 top20" id="removeTagsIcon" name="removeTagsIcon" style="display: none;">
				<label></label>
				<span id="divTagsName"></span>
			</span>
		</form>
			<div class="col-sm-5 col-md-5">
			<input type="hidden" id="docID" value=""/>
			<input type="hidden" name="secStatusId" id="secStatusId">
			<input type="hidden" id="entityID" value="${entityID}"/>
		</div>
	  </div>
	 <input type="hidden" id="crtby" value="" name="crtby">
	  <div class="row mt15">
	  	<div class="col-sm-4 col-md-4">
	     	<button onclick="uploadDistFile()" class="btn btn-primary" id="save"><strong><spring:message code="btnSave"/> <i class="icon"></i></strong></button>	     	
	       <button onclick="uploadEditDistFile()" class="btn btn-primary" id="update"><strong><spring:message code="btnUpdate"/> <i class="icon"></i></strong></button>	     	
	       &nbsp;<a href="javascript:void(0);" onclick="cancelDocDiv()"><spring:message code="lnkCancel"/></a>  
	    </div>
	  </div>
</div>

<div  class="modal hide distAttachmentShowDiv"  id="distAttachmentShowDiv"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="table-responsive">
		<div class='onr-dialog'>
			<div class='modal-content'>
			<div class="modal-header">
		  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="hideDiv();"><spring:message code="btnX"/></button>
				<h3 id="UserProfileNameEEOC11"><spring:message code="hdAttachment"/></h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<div class="" id="divPnrAttachment">
					 
					</div>
					<iframe src="" id="ifrmAttachment" width="100%" height="480px"> </iframe>   
				</div>
		 	</div>
		 	<input type="hidden" id="eligibilityMasterId" value=""/>
		 	<div class="modal-footer">
		 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="clearFrame()"><spring:message code="btnClose"/></button> 		
		 	</div>
		  </div>
		 </div>
	</div>
</div>
 <div style="display:none;" id="loadingDiv">
     <table  align="center" >
 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
	</table>
</div>
 <script>
 displayDistAttachment();		
</script>

<script>
function fileContainsVirusDiv(msg)
{
	$('#loadingDiv').hide();
	$('#virusDivId .modal-body').html(msg);
	$('#virusDivId').modal('show');
}

<c:if test="${not empty headQuarterName}">
$("#headQuarterClass").show();
$("#onlyHeadQuarterName").attr('readonly', true);

</c:if>
</script>

<div class="modal hide" id="virusDivId" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog-for-cgcoverletter" style='width: 35%;'>
		<div class="modal-content">
			<div class="modal-header">
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body" style="max-height: 400px;overflow-y:auto;">
				<div class="control-group">
				</div>
			</div>
			<div class="modal-footer">
				<span id=""><button class="btn btn-primary" data-dismiss="modal">Ok <i class="icon"></i></button></span>&nbsp;&nbsp;
			</div>
		</div>
	</div>
</div>

<div class="modal hide" id="successModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog-for-cgcoverletter" style='width: 35%;'>
		<div class="modal-content">
			<div class="modal-header">
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body" style="max-height: 400px;overflow-y:auto;">
				<div class="control-group">
					Document has been successfully added.
				</div>
			</div>
			<div class="modal-footer">
				<span id=""><button class="btn btn-primary" data-dismiss="modal" onclick="clrlPage();">Ok <i class="icon"></i></button></span>&nbsp;&nbsp;
			</div>
		</div>
	</div>
</div>
<div class="modal hide" id="successEditModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog-for-cgcoverletter" style='width: 35%;'>
		<div class="modal-content">
			<div class="modal-header">
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body" style="max-height: 400px;overflow-y:auto;">
				<div class="control-group">
					Document has been successfully updated.
				</div>
			</div>
			<div class="modal-footer">
				<span id=""><button class="btn btn-primary" data-dismiss="modal" onclick="clrlPage();">Ok <i class="icon"></i></button></span>&nbsp;&nbsp;
			</div>
		</div>
	</div>
</div>