<!-- @Author: Ankit Sharma 
 * @Discription: view of edit headquarter Page.
-->
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>

<script type="text/javascript" src="dwr/interface/DistrictDomainAjax.js?ver=${resourceMap['DistrictDomainAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/BranchesAjax.js?ver=${resourceMap['BranchesAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/HeadQuarterAjax.js?ver=${resourceMap['HeadQuarterAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/DistrictAjax.js?ver=${resourceMap['DistrictAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/I4QuestionSetAjax.js?ver=${resourceMap['I4QuestionSetAjax']}"></script>
<script type="text/javascript" src="dwr/interface/DistrictAssessmentAjax.js?ver=${resourceMap['DistrictAssessmentAjax']}"></script>
<script type="text/javascript" src="dwr/interface/UserAjax.js?ver=${resourceMap['UserAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type='text/javascript' src="js/editheadquarter.js?ver=${resourceMap['js/editheadquarter.js']}"></script>
  <!-- Optionally enable responsive features in IE8 -->
    <script type='text/javascript' src="js/bootstrap-slider.js"></script>
    <link rel="stylesheet" type="text/css" href="css/slider.css" />  
<script src="calender/js/jscal2.js"></script>
<script src="calender/js/lang/en.js"></script>
<link rel="stylesheet" type="text/css" href="calender/css/jscal2.css" />
<link rel="stylesheet" type="text/css" href="calender/css/border-radius.css" />
<link rel="stylesheet" type="text/css" href="calender/css/gold/gold.css" />
<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" />  
<style>
.table th, .table td {
    padding: 8px;
    line-height: 20px;
    text-align: left;
    vertical-align: top;  
}
.divwidth
{
float: left;
width: 40px;
}
.input-group-addon {
    background-color: #fff;
	border: none;
}
</style>
<script type="text/javascript">
var $j=jQuery.noConflict();
        $j(document).ready(function() {
        });
        
function applyScrollOnTbl()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#districtSchoolsTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 280,
        width: 722,
        minWidth: null,
        minWidthAuto: false,
        colratio:[288,182,144,110], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}

function applyScrollOnDistrictsTbl()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#districtTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 280,
        width: 722,
        minWidth: null,
        minWidthAuto: false,
        colratio:[288,182,144,110], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}

function applyScrollOnTblDomain()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#domainTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 280,
        width: 600,
        minWidth: null,
        minWidthAuto: false,
        colratio:[500,100], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}

function applyScrollOnTblNotes()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#notesTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 280,
        width: 722,
        minWidth: null,
        minWidthAuto: false,
        colratio:[288,182,144,110], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}



 </script>  
<script type="text/javascript">
	
	
</script>
<style>
	.hide
	{
		display: none;
	}
	.inline
	{
		display: inline;
	}
	.nowrap
	{
		white-space: nowrap
	}
	.setSlider
	{
		margin-left: -20px; 
		margin-top: -8px;
	}
	
	.tooltip {
  position: absolute;
  z-index: 1030;
  display: block;
  font-size: 12px;
  line-height: 1.4;
  opacity: 0;
  filter: alpha(opacity=0);
  visibility: visible;
}

.tooltip.in {
  opacity: 0.9;
  filter: alpha(opacity=90);
}

.tooltip.top {
  padding: 5px 0;
  margin-top: -3px;
}

.tooltip.right {
  padding: 0 5px;
  margin-left: 3px;
}

.tooltip.bottom {
  padding: 5px 0;
  margin-top: 3px;
}

.tooltip.left {
  padding: 0 5px;
  margin-left: -3px;
}

.tooltip-inner {
  max-width: 200px;
  padding: 3px 8px;
  color: #ffffff;
  text-align: center;
  text-decoration: none;
  background-color: #000000;
  border-radius: 4px;
}

.tooltip-arrow {
  position: absolute;
  width: 0;
  height: 0;
  border-color: transparent;
  border-style: solid;
}

.tooltip.top .tooltip-arrow {
  bottom: 0;
  left: 50%;
  margin-left: -5px;
  border-top-color: #000000;
  border-width: 5px 5px 0;
}

.tooltip.top-left .tooltip-arrow {
  bottom: 0;
  left: 5px;
  border-top-color: #000000;
  border-width: 5px 5px 0;
}

.tooltip.top-right .tooltip-arrow {
  right: 5px;
  bottom: 0;
  border-top-color: #000000;
  border-width: 5px 5px 0;
}

.tooltip.right .tooltip-arrow {
  top: 50%;
  left: 0;
  margin-top: -5px;
  border-right-color: #000000;
  border-width: 5px 5px 5px 0;
}

.tooltip.left .tooltip-arrow {
  top: 50%;
  right: 0;
  margin-top: -5px;
  border-left-color: #000000;
  border-width: 5px 0 5px 5px;
}

.tooltip.bottom .tooltip-arrow {
  top: 0;
  left: 50%;
  margin-left: -5px;
  border-bottom-color: #000000;
  border-width: 0 5px 5px;
}

.tooltip.bottom-left .tooltip-arrow {
  top: 0;
  left: 5px;
  border-bottom-color: #000000;
  border-width: 0 5px 5px;
}

.tooltip.bottom-right .tooltip-arrow {
  top: 0;
  right: 5px;
  border-bottom-color: #000000;
  border-width: 0 5px 5px;
}
</style>
<form:form commandName="headQuarterMaster"  method="post" onsubmit="return validateEditHeadQuarter();"  enctype="multipart/form-data">
<div class="row" style="margin-left: 0px;margin-right: 0px;margin-top:">
         <div style="float: left;">
         	<img src="images/add-district.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;">Edit HeadQuarter</div>	
         </div>
		
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
</div>

    
	<div>
		<div class="mt10">
      		<div class="panel-group" id="accordion">
        		<div class="panel panel-default">
          		<div class="accordion-heading"> <a href="#collapseOne" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle minus"> General Information </a> </div>
          		<div class="accordion-body in" id="collapseOne" style="height:auto;">
            	<div class="accordion-inner">
              	<div class="offset1">
              	<div class="row">
					<div class="col-sm-12 col-md-12">			                         
					 	<div class='required' id='errorlogoddiv'></div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6 col-md-6">
						<label>HeadQuarter Name</label>
						<form:input path="headQuarterName" cssClass="form-control" maxlength="50" tabindex="-1" placeholder="" readonly="true" />
	                   </div>
                 </div>
				<div class="row">
					<div class="col-sm-6 col-md-6">
                   		<label>Address</label>
						<form:input path="address" cssClass="form-control" maxlength="50" tabindex="-1" placeholder="" readonly="true"/>
                    </div>
				</div>
                  
				<div class="row">
                  	<div class="col-sm-2 col-md-2">
                 		<label>NCES HeadQuarter ID</label>
						<form:input path="headQuarterId" cssClass="form-control" maxlength="50" tabindex="-1" placeholder="" readonly="true"/>
                  	</div>
                  	
                  	<div class="col-sm-2 col-md-2">
                    	<label>HeadQuarter Code</label>
						<form:input path="headQuarterCode" cssClass="form-control" maxlength="50" tabindex="-1" placeholder="" readonly="true"/>
                  	</div>                  	
                  	
                  	
                  	<div class="col-sm-2 col-md-2">
                    	<label>Zip Code</label>
						<form:input path="zipCode" cssClass="form-control" maxlength="50" tabindex="-1" placeholder="" readonly="true"/>
                  	</div>
				</div>
				
                <div  class="row">
                	<div class="col-sm-3 col-md-3">
                    	<label>State</label>
                    	<input type="text" class="form-control" value="${headQuarterMaster.stateId.stateName }" tabindex="-1" readonly="true"/>
                  </div>
                  <div class="col-sm-3 col-md-3">
                    	<label>City</label>
                    	<form:input path="cityName" cssClass="form-control" maxlength="50" tabindex="-1" placeholder="" readonly="true"/>
                  </div>
                
                </div>
                  
                  <div class="row">
                    <div class="col-sm-2 col-md-2">
                      	<label>Phone</label>
						<form:input path="phoneNumber" cssClass="form-control" maxlength="20" tabindex="-1" placeholder="" readonly="true"/>
                    </div>
                    
                    <div class="col-sm-2 col-md-2">
                      	<label>Fax</label>
						<form:input path="faxNumber" cssClass="form-control" maxlength="20" placeholder=""/>
                    </div>
                  </div>
				
				 <div class="row">
                  	<div class="col-sm-6 col-md-6">
                      	<label>Website URL</label>
						<form:input path="websiteUrl" cssClass="form-control" maxlength="100" placeholder=""/>
                    </div>
                  </div>
                              
                  <div class="row">                   
                    <div class="col-sm-3 col-md-3">
                      <label>Total # of Branches <a href="#"  id="iconpophover6" rel="tooltip"   data-original-title="Total number of branches in the HeadQuarter per NCES records. If this information is not accurate, please notify your TeacherMatch Account Manager"><img src="images/qua-icon.png" width="15" height="15" alt=""></a></label>
						<form:input path="totalNoOfBranches" cssClass="form-control" maxlength="50" tabindex="-1" placeholder="" readonly="true"/>
                    </div>
                  </div>
                  
                <div  class="row">
					<div class="col-sm-2 col-md-2 top10">
						<label>Logo <a href="#" id="iconpophover2" rel="tooltip" data-original-title="Logo image for the Branch"><img src="images/qua-icon.png" width="15" height="15" alt=""></a></label>    
						
						<form:input path="logoPathFile" type="file"/>
						<!-- <a href="#" onclick="return clearFile(1)">Clear</a> -->

				 	</div>
                   	<div class="col-sm-4 col-md-4 top10 left25">
                   		<label>&nbsp;</label><span id="showLogo">&nbsp;</span>
                   	</div>
                </div>
                  
                  <div class="row">
                    	<div class="col-sm-9 col-md-9" id="dDescription">
                      	<label>Description</label>
                      	<label class="redtextmsg">(If you are planning to copy and paste text from a web page and/or a Microsoft document, please ensure
						   to paste that text first into a simple text editor such as Notepad to remove the special characters and
 						   then copy and paste it into the field below. Otherwise, the text directly cut and pasted from a web page 
 						   and/or a Microsoft document may not display properly to the reader)</label>
                      		<form:textarea path="description" rows="3" maxlength="1500" onkeyup="return chkLenOfTextArea(this,1500);" onblur="return chkLenOfTextArea(this,1500);" class="form-control"/>
                    	</div>
                  	</div>
                  	
                  	<div class="row top5">
                    	<div class="col-sm-9 col-md-9">
                      	<p>TeacherMatch can approach me for periodic customer surveys to improve its service, quality, and efficacy. <a href="#" id="iconpophover9" rel="tooltip" data-original-title="Periodically, TeacherMatch surveysits customers for feedback on its quality, responsiveness and efficacy"><img src="images/qua-icon.png" width="15" height="15" alt=""></a></p>
                    	</div>
                  </div>
				<div class="row">
					<div class="col-sm-1 col-md-1">
						<label class="radio">
                        
                        <%-- <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked="">--%>
                        <form:radiobutton path="canTMApproach" value="1" />
                        Yes 
                        </label>
                     </div>
                     <div class="col-sm-1 col-md-1 left20-sm2">
                      	<label class="radio">
                        <form:radiobutton path="canTMApproach" value="0" />
                        <%-- <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked="">--%>
                        No </label>
                    </div>
                  </div>
              </div>
              <div class="clearfix"></div>
            </div>
          </div>
        </div>
	
	<div class="panel panel-default">
			<div class="accordion-heading"> <a href="#collapseTwo" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle plus"> Contact Information </a> </div>
          	<div class="accordion-body collapse" id="collapseTwo" style="height: 0px;">
           
           	<div class="accordion-inner">
				<div class="span14 offset1">
					<div class="row">
	                <div class="col-sm-9 col-md-9">
	              		<div id="errorkeydiv" class="required"></div>
	                </div>
                </div>
                
                <div class="row">
<!--                  <form>-->
						<div class="col-sm-12 col-md-12">			                         
						 	<div class='required' id='errorcontactinfodiv'></div>
						 	<div class='required' id='errorpwddiv'></div>
						</div>
						<div class="col-sm-12 col-md-12">			                         
						 	<div class='required' id='errorkeydiv'></div>
						</div>
						<c:set var="disableDmFields" value=""></c:set>					

					<div class="col-sm-4 col-md-4">
						<label>Final Decision Maker Name<span class="required">*</span> <a href="#" id="iconpophover4" data-toggle="tooltip"  data-placement="right" data-original-title="The individual who has final decision-making authority over all functions of the account"><img src="images/qua-icon.png" width="15" height="15" alt=""></a></label>
                      	<%-- <input type="text" placeholder="" class=" span5">--%>
                      	<form:input path="dmName" cssClass="form-control" maxlength="50" readonly="false" placeholder=""/>
                    </div>
                    <div class="col-sm-3 col-md-3">
                      	<label>Email<span class="required">*</span></label>
                      	<%-- <input type="text" placeholder="" class=" span5">--%>
                      	<form:input path="dmEmailAddress" cssClass="form-control" maxlength="100" readonly="false" placeholder=""/>
                    </div>
                    <div class="col-sm-2 col-md-2">
                      	<label>Phone</label>
                      	<%-- <input type="text" placeholder="" class=" span5">--%>
                      	<form:input path="dmPhoneNumber" cssClass="form-control" maxlength="20" readonly="false" placeholder=""/>
                    </div>
	                    <div id="dmPwdDiv" class="col-sm-2 col-md-2">
	                      	<label>Password<span class="required">*</span></label>
	                      	<%-- <input type="text" placeholder="" class=" span5">--%>
	                      	<form:input path="dmPassword" type="password" cssClass="form-control" maxlength="20" placeholder=""/>
	                      	<%--<form:password path="dmPassword" cssClass="span2" maxlength="20" showPassword="false" />--%>
	                    </div>

                     <div class="col-sm-1 col-md-1" style="margin-left:-10px;">
	                  	 <label>&nbsp;</label>
	                  	  <c:if test="${fn:indexOf(roleAccessCI,'|13|')!=-1}">
	                      	 <button type="button" id="btnDm" name="btnDm" class="btn btn-primary" onclick="editDmFields()"><strong>Go</strong></button>
	                      </c:if>
	                 </div>

                   <input type="hidden" id="entityTypeOfSession" name="entityTypeOfSession" class="span2" maxlength="20" value="${userSession.entityType}" placeholder=""/>
                </div>
                
                <div class="row">
					<div class="col-sm-4 col-md-4">
                    	<label>Data Coordinator<span class="required">*</span> <a href="#" id="iconpophover5" rel="tooltip" data-original-title="The individual in charge of and most familiar with&#10;&#013; student standardized testing programs"><img src="images/qua-icon.png" width="15" height="15" alt=""></a></label>
						<form:input path="acName" cssClass="form-control" maxlength="50"/>
                  	</div>
                  	<div class="col-sm-3 col-md-3">
                    	<label>Email</label>
						<form:input path="acEmailAddress" cssClass="form-control" maxlength="100" placeholder=""/>
                  	</div>
                  	<div class="col-sm-2 col-md-2">
                    	<label>Phone</label>
						<form:input path="acPhoneNumber" cssClass="form-control" maxlength="20" placeholder=""/>
                  	</div>                  	
                </div>
                
                 <div class="row">
                		<%-- ========= This Div Will Readonly Mode Excepting Entity Type 1====== And In Edit Mode For TeacherMatch Admin  --%>
                		<c:set var="disableAccountManagerFields" value=""></c:set>					
						<c:if test="${userSession.entityType ne 1}">
				         	<c:set var="disableAccountManagerFields" value="true"></c:set>	 
						</c:if>
					<div class="col-sm-4 col-md-4">
                    	<label>Account Manager from Teacher Match<span class="required">*</span> <a href="#" id="iconpophover11" rel="tooltip" data-original-title="The individual from TeacherMatch who is responsible for all matters related to TeacherMatch contract"><img src="images/qua-icon.png" width="15" height="15" alt=""></a></label>
						<form:input path="amName" cssClass="form-control" maxlength="50" readonly="${disableAccountManagerFields}" placeholder=""/>
                  	</div>
                  	<div class="col-sm-3 col-md-3">
                    	<label>Email</label>
						<form:input path="amEmailAddress" cssClass="form-control" maxlength="100" readonly="${disableAccountManagerFields}" placeholder=""/>
                  	</div>
                  	<div class="col-sm-2 col-md-2">
                    	<label>Phone</label>
						<form:input path="amPhoneNumber" cssClass="form-control" maxlength="20" readonly="${disableAccountManagerFields}" placeholder=""/>
                  	</div>                  	
                </div>
                
                <div class="row hide">                               
                      <div class="col-sm-11 col-md-11">Other Key Contacts
                      <div class="pull-right">
                       <c:if test="${fn:indexOf(roleAccessCI,'|1|')!=-1}">
                      <a href="javascript:void(0);"  onclick="showKeyContact();">+ Add Key Contact</a>
                      </c:if>
                </div>
                    </div>                 
                </div>
                
                <div class="row hide">
                  <div  class="col-sm-11 col-md-11 mt08">
                    <table id="keyContactTable"  width="100%" border="0" class="table table-bordered">
                      <thead class="bg">
                        <tr>
                          <th width="20%">Contact Type</th>
                          <th width="20%">Contact Name</th>
                          <th width="15%">Email</th>
                          <th width="15%">Phone</th>
                          <th width="15%">Title</th>
                          <th width="15%">Actions</th>
                        </tr>
                      </thead>
                    </table>
                  </div>
                </div>
                
                <div id="addKeyContactDiv"  class="hide">
                <div class="row">
                    <div class="col-sm-4 col-md-4">
                      <label>Contact Type </label>
                      <select class="form-control" id="dropContactType" name="dropContactType">
                        <c:forEach items="${contactTypeMaster}" var="ctm">
							<c:set var="selected" value=""></c:set>					
			        		<c:if test="${st.stateId eq ctm.contactTypeId}">	        			
			         			<c:set var="selected" value="selected"></c:set>	         			
			        		</c:if>	
							<option id="${ctm.contactTypeId}" value="${ctm.contactTypeId}" $selected >${ctm.contactType}</option>
						</c:forEach>
                      </select>
                      <input type="hidden" id="keyContactId" name="keyContactId" placeholder="" class=" span3">
                    </div>
                    <div class="col-sm-4 col-md-4">
                    <label>Contact First Name<span class="required">*</span> </label>
                    <input type="text" id="keyContactFirstName" name="keyContactFirstName" maxlength="50" placeholder="" class="form-control">
                  </div>
                   <div class="col-sm-3 col-md-3">
                    <label>Contact Last Name<span class="required">*</span> </label>
                    <input type="text" id="keyContactLastName" name="keyContactLastName" maxlength="50" placeholder="" class="form-control">
                  </div>
                  </div>
                  
                  <div class="row">
                  <div class="col-sm-4 col-md-4">
                    <label>Contact Email<span class="required">*</span> </label>
                    <input type="text" id="keyContactEmailAddress" name="keyContactEmailAddress"  maxlength="100" placeholder="" class="form-control">
                  </div>
                  <div class="col-sm-4 col-md-4">
                    <label>Phone</label>
                    <input type="text" id="keyContactPhoneNumber" name="keyContactPhoneNumber"   maxlength="20" placeholder="" class="form-control">
                  </div>
                    <div class="col-sm-3 col-md-3">
                      <label>Title</label>
                      <input type="text"  id="keyContactTitle" name="keyContactTitle"  maxlength="25" class="form-control">
                    </div>
                </div>
                
                <div class="row">
                  <div class="col-sm-5 col-md-5 idone mt10"><a href="javascript:void(0);" onclick="return addKeyContact();"><spring:message code="lnkImD"/></a>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onclick="clearKeyContact();">Cancel</a></div>
                </div>               
              </div>    
            </div>
        <div class="clearfix">&nbsp;</div>
          </div>
             </div>    
    </div>   
    
    <div class="panel panel-default">
          <div class="accordion-heading"> <a href="#collapseThree" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle plus"> Users </a> </div>
          <div class="accordion-body collapse" id="collapseThree" style="height: 0px;">
            <div class="accordion-inner">
              <div class="offset1">
              <div id="errorusersdiv" class="required"></div>
               <div class="row">
                  <div class="col-sm-10 col-md-10"><label>Administrators</label>
	                  <div class="pull-right">
	                   <c:if test="${fn:indexOf(roleAccessU,'|1|')!=-1}">
	                   <a href="javascript:void(0);" onclick="addAdministrator(10);">+ Add Administrator</a>
	                   </c:if>
	                   </div>
                   </div>
                </div>
                
                 <div class="row col-sm-10 col-md-10">            
                    <div id="headQuarterAdministratorDiv" class="row"></div>
                 </div>
                
                <div class="row">
                  <div class="col-sm-10 col-md-10 top20"><label>Analysts</label>
	                  <div class="pull-right"> 
	                   <c:if test="${fn:indexOf(roleAccessU,'|1|')!=-1}">
	                  <a href="javascript:void(0);" onclick="addAdministrator(12);">+ Add Analyst</a>
	                  </c:if>
	                  </div>
                  </div>
                </div>
                
                <div class="row col-sm-10 col-md-10">                
                    <div id="headQuarterAnalystDiv"  class="row"></div>
                </div>
                
                <!-- Add Administrator Elements :  -->
                <div id="addAdministratorDiv" class="hide" style="padding-top: 8px;">
                <div class="row col-sm-10 col-md-10">
                <div class="row">
					<div class="col-sm-3 col-md-3">
						<label><strong>Salutation</strong></label>
						<select class="form-control" id="salutation" name="salutation">
							<option value="1">Mr.</option>
							<option value="2">Ms.</option>
							<option value="3">Mrs.</option>
							<option value="4">Dr.</option>
						</select>
					</div>
					<div class="col-sm-3 col-md-3">
                    	<label><strong>First Name</strong><span class="required">*</span></label>
                    	 <input class="form-control" type="hidden" id="roleId" name="roleId" value="" placeholder="">
                    	 <input class="form-control" type="text" id="firstName" name="firstName"  maxlength="50"placeholder="">
                    	<%-- <form:input type="hidden" path="userId" id="userId" />
                    	<form:input path="firstName" cssClass="span3" maxlength="50" placeholder=""/>--%>
					</div>
					<div class="col-sm-3 col-md-3">
                    	<label><strong>Last Name</strong><span class="required">*</span></label>
                    	 <input class="form-control" type="text" id="lastName" name="lastName" maxlength="50" placeholder="">
					</div>
					</div>
				</div>
				
                <div class="row col-sm-10 col-md-10">
                   <div class="row">
					<div class="col-sm-9 col-md-9">
                    	<label><strong>Title</strong></label>
                    	<input class="form-control" type="text" id="title" name="title" maxlength="200" placeholder="">
                  </div>
                  </div>
                </div>
                
                <div class="row col-sm-10 col-md-10">
                   <div class="row">
					<div class="col-sm-9 col-md-9">
                    	<label><strong>Email</strong><span class="required">*</span></label>
                    	 <input class="form-control" type="text" id="emailAddress" name="emailAddress" maxlength="75" placeholder="">
                  </div>
                </div>
                </div>
                
                <div class="row col-sm-10 col-md-10">
                   <div class="row">
					<div class="col-sm-5 col-md-5">
                    	<label><strong>Phone</strong></label>
                    	<input class="form-control" type="text" id="phone" name="phone" maxlength="20" placeholder="">
                  	</div>
                  	<div class="col-sm-4 col-md-4">
                    	<label><strong>Mobile</strong></label>
                    	<input class="form-control" type="text" id="mobileNumber" name="mobileNumber" maxlength="20" placeholder="">
                  	</div>
                </div>
                </div>
                
                <div class="row">
                  <div class="col-sm-3 col-md-3 idone top10"><a href="javascript:void(0);" onclick="validateHeadQuarterAdministratorOrAnalyst();"><spring:message code="lnkImD"/></a>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onclick="clearUser();">Cancel</a></div>
                </div>
            </div><%-- End of addAdministratorDiv --%>
                
              </div>
              <div class="clearfix">&nbsp;</div>
              </div>              
            </div>
    </div>
    
     <div class="panel panel-default">
          <div class="accordion-heading"> <a href="#collapseFive" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle plus"> Social </a> </div>
          <div class="accordion-body collapse" id="collapseFive" style="height: 0px;">
            <div class="accordion-inner">
              <div class="offset1">
              <div class="row col-sm-12 col-md-12">
               <div id="errorsocialdiv" class="required"></div>
              </div>
              
               <div class="row mt10">
                  		<div class="col-sm-12 col-md-12">
	                       Social Network Posting
					  	</div>
                  	</div>
                  
                  <div class="row">
	                  <div class="col-sm-12 col-md-12">
	                        <label class="checkbox" style="margin-bottom: 0px;">
							 <form:checkbox path="postingOnHQWall" id="postingOnHQWall"  value="1"/>
			                 Yes, I would like to enable posting of all the job links in my HeadQuarter to Linked-In, Twitter and Facebook <a href="#" id="iconpophover12" rel="tooltip" data-original-title="Specific settings for each network is in Settings area"><img src="images/qua-icon.png" width="15" height="15" alt=""></a> 
							</label>
					  </div>
                  </div>
                  
                  <div class="row">
	                  <div class="col-sm-12 col-md-12">
	                        <label class="checkbox">
	                        <form:checkbox path="postingOnTMWall" id="postingOnTMWall"  disabled="${disableForTm}" value="1"/>
							Yes, all the job links for this HeadQuarter shall be posted on TeacherMatch social network as well

							</label>
					   </div>
                   </div>
                             
              </div>
              <div class="clearfix">&nbsp;</div>
            </div>
          </div>
    </div>
    
      <div class="panel panel-default">
          <div class="accordion-heading"> <a href="#collapseSix" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle plus"> Mosaic </a> </div>
          <div class="accordion-body collapse" id="collapseSix" style="height: 0px;">
            <div class="accordion-inner">
              <div class="offset1">
           		<div class="row mt10">
	                  	<div class="col-sm-4 col-md-4">
							<label>Distribution Email Address</label>
	                      	<input type="text" class="form-control" maxlength="100" />
	                    </div>
                  	</div>
					
					<div class="row mt10">
                  			<div class="col-sm-4 col-md-4">
                    		Candidate Feed Criteria <a href="#" id="iconpophover13" rel="tooltip" data-original-title="Set criteria to view the Candidates in your Action Feed on Mosaic" data-placement="right"><img src="images/qua-icon.png" width="15" height="15" alt=""></a>
                  		</div>
                  	</div>

                  <div class="row mt10"> 
                  <fmt:bundle basename="resource"><fmt:message key="sliderMaxLength" var="sliderMaxLength" /></fmt:bundle>
                  <fmt:bundle basename="resource"><fmt:message key="sliderRationMaxLength" var="sliderRationMaxLength" /></fmt:bundle>
                  
				
          <%--         GAgan     <c:out value="${candidateFeedNormScoreValue } "></c:out> --%> 
                  
                  
                  			<div class="col-sm-12 col-md-12">
								<label> Post a Candidate to my action feed in Mosaic if</label>
							</div>
                  
							<div class="col-sm-4 col-md-4" style="max-width: 250px;">
								<label>  the Candidate Norm score is higher than   </label>
							</div>
							<div class="col-sm-8 col-md-8" style=" margin-top: -8px;"> 
								<label> 
									<iframe id="ifrm1"  src="slideract.do?name=slider1&tickInterval=10&max=100&swidth=200&svalue=18" scrolling="no" frameBorder="0" style="border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:280px;"></iframe>
									<form:hidden cssClass="span2" path="candidateFeedNormScore" value="${candidateFeedNormScoreValue }"/>
								</label>
							</div>
							<div class="clearfix"></div>
							<div class="col-sm-12 col-md-12">
								<label>  and  </label>
							</div>
							
							<div class="col-sm-6 col-md-6" style="max-width: 390px;">
								<label class="radio">
								
								<input type="hidden" id="mosaicRadiosFlag" name="mosaicRadiosFlag" value="1">
								<input type="radio"  name="mosaicRadios" id="mosaicRadios1" value="1" <c:if test="${districtMaster.candidateFeedDaysOfNoActivity ge 0}">checked </c:if> onclick="disableSlider()">
									<%--<form:radiobutton path="noSchoolUnderContract"  value="1" />--%>
			                        If no communications with the Candidate is conducted within 
		                        </label>
							</div>
							
							<div class="col-sm-3 col-md-3" style="min-width: 240px;">
								<label> 
									<c:set var="dsliderValueWhendbValue0" value="1"></c:set>
											<c:if test="${candidateFeedDaysOfNoActivityValue eq 0}">
												<c:set var="dsliderValueWhendbValue0" value="0"></c:set>
											</c:if>
								<iframe id="ifrm2"  src="slideract.do?name=slider2&tickInterval=10&max=100&swidth=200&svalue=72" scrolling="no" frameBorder="0" style="border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:280px;"></iframe>
									<form:hidden cssClass="span2" path="candidateFeedDaysOfNoActivity" value="${candidateFeedDaysOfNoActivityValue }"/>
								</label>
							
						    </div>
							<div class="col-sm-2 col-md-2" style="min-width: 140px;">
								<label>&nbsp;&nbsp; days of applying</label>
							</div>
							<div style="clear: both;"></div>
							<div class="col-sm-12 col-md-12" style="margin-top: -10px;">
								<label class="radio">
								<input type="radio"  name="mosaicRadios" id="mosaicRadios2" value="0" <c:if test="${districtMaster.candidateFeedDaysOfNoActivity eq 0}">checked </c:if> onclick="disableSlider()" >
									<%--<form:radiobutton path="noSchoolUnderContract"  value="1" />--%>
			                        As soon as they have applied
		                        </label>
							</div>
							
							<div class="col-sm-12 col-md-12">
					 			<label class="checkbox">
								<%-- <form:checkbox path="postingOnDistrictWall" id="postingOnDistrictWall"  value="1"/> --%>
			                 	<input type="checkbox" name="canSchoolOverrideCandidateFeed"/>	Branch can override my settings for the candidate feed
 
								</label>
							</div>				   
                   </div>
                  
                  <div class="row">
                  		<div class="col-sm-12 col-md-12">
                    		Job Feed Criteria <a href="#" id="iconpophover14" rel="tooltip" data-original-title="Set criteria to view the jobs in your Action Feed on Mosaic"><img src="images/qua-icon.png" width="15" height="15" alt=""></a>
                  		</div>
                  </div>
                  
                  
                  <div class="row">
							<div class="col-sm-4 col-md-4 top15">							
								<label>Post a job in my action feed in Mosaic and label it</label>
							</div>	
													
							<div class="col-sm-3 col-md-3 top10">		
								<input type="text" name="lblCriticialJobName" class="form-control" value="${lblCriticialJobNameValue}" maxlength="50"/> 
							</div>
							<div class="col-sm-4 col-md-4 top15" style="margin-left: -20px;">
							 ,&nbspif
							</div>                 			
							
							<div class="clearfix"></div>
							<div class="col-sm-3 col-md-3" style="max-width: 170px;">
								<label> A job has been active for  </label>
							</div>
							
							<div class="col-sm-3 col-md-3" style="min-width: 240px;">
								<label> 
								<iframe id="ifrm3"  src="slideract.do?name=slider3&tickInterval=10&max=100&swidth=200&svalue=13" scrolling="no" frameBorder="0" style="border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:280px;"></iframe>
									<input type="hidden" name="jobFeedCriticalJobActiveDays" cssClass="form-control" value="${jobFeedCriticalJobActiveDaysValue}"/>
								</label>
							</div>
							<div class="col-sm-1 col-md-1" style="min-width: 60px;">
								<label>&nbsp; days </label>
							</div>
								<div class="clearfix"></div>
							<div class="col-sm-12 col-md-12">
								<label>  and  </label>
							</div>
							
							<div class="col-sm-6 col-md-6" style="max-width: 400px;">
								<label>  for the number of candidates to number of openings ratio is less than  </label>
							</div>
							
							<div class="col-sm-3 col-md-3">
								<label> 
								<iframe id="ifrm4"  src="slideract.do?name=slider4&tickInterval=1&max=25&swidth=200&svalue=6" scrolling="no" frameBorder="0" style="border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:280px;"></iframe>
									<input type="hidden" name="jobFeedCriticalCandidateRatio" cssClass="form-control" value="${jobFeedCriticalCandidateRatioValue}"/>
								</label>
							</div>
							<div class="clearfix"></div>
							<div class="col-sm-12 col-md-12">
								<label>  where  </label>
							</div>
							
							<div class="col-sm-5 col-md-5" style="max-width: 290px;">
								<label>each applicant has a norm score greater than  </label>
							</div>
							<div class="col-sm-7 col-md-7">
								<label> 
								<iframe id="ifrm5"  src="slideract.do?name=slider5&tickInterval=10&max=100&swidth=200&svalue=32" scrolling="no" frameBorder="0" style="border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:280px;"></iframe>
									<input type="hidden" name="jobFeedCriticalNormScore" cssClass="span2" value="${jobFeedCriticalNormScoreValue}"/>
								</label>
							</div>
							
							<div class="col-sm-4 col-md-4 top15">							
								<label>Post a job in my action feed in Mosaic and label it</label>
							</div>	
													
							<div class="col-sm-3 col-md-3 top10">		
								<input type="text" name="lblAttentionJobName" class="form-control" value="${lblAttentionJobNameValue}" maxlength="50"/>
							</div>
							<div class="col-sm-4 col-md-4 top15" style="margin-left: -20px;">
							  ,&nbspif
							</div>
							<div class="clearfix"></div>
							<div class="col-sm-3 col-md-3" style="max-width: 170px;">
								<label>  A job has been active for   </label>
							</div>
							
							<div class="col-sm-3 col-md-3" style="min-width: 240px">
								<label> 
								<iframe id="ifrm6"  src="slideract.do?name=slider6&tickInterval=10&max=100&swidth=200&svalue=58" scrolling="no" frameBorder="0" style="border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:280px;"></iframe>
									<input type="hidden" name="jobFeedAttentionJobActiveDays" cssClass="span2" value="${jobFeedAttentionJobActiveDaysValue}"/>
								</label>
							</div>
							<div class="col-sm-1 col-md-1" style="min-width: 60px">
								<label>&nbsp; days </label>
							</div>
								<div class="clearfix"></div>
							<div class="col-sm-12 col-md-12">
								<label>  and  </label>
							</div>
							
							<div class="col-sm-12 col-md-12">
								<label class="checkbox">  
									<input type="checkbox" name="jobFeedAttentionJobNotFilled" value="1"/> if not filled completely (still has openings)
								</label>
							</div>
							
							<div class="col-sm-12 col-md-12">
								<label>  and  </label>
							</div>
							
							<div class="col-sm-4 col-md-4" style="max-width: 280px;">
								<label>  has applicants with norm score greater than  </label>
							</div>
							
							<div class="col-sm-4 col-md-4">
								<label> 
								<iframe id="ifrm7"  src="slideract.do?name=slider7&tickInterval=10&max=100&swidth=200&svalue=37" scrolling="no" frameBorder="0" style="border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:280px;"></iframe>
									<input type="hidden" name="jobFeedAttentionNormScore" cssClass="span2" value="${jobFeedAttentionNormScoreValue}"/>
								</label>
							</div>
							
							<div class="col-sm-12 col-md-12">
								<label>    and they have not been communicated with (a communication includes sending a message, making a phone call or changing the status of the applicant)</label>
							</div>
							 
							<div class="col-sm-6 col-md-6">
					 			<label class="checkbox">
								<%-- <form:checkbox path="postingOnDistrictWall" id="postingOnDistrictWall"  value="1"/> --%>
			                 	<input type="checkbox" name="canSchoolOverrideJobFeed"  value="1"/>
			                 		Branch can override my settings for the job feed 
								</label>
							</div>				   
				</div>
              </div>
              <div class="clearfix">&nbsp;</div>
            </div>
          </div>
      </div>
      
     <div class="panel panel-default">
          <div class="accordion-heading"> <a href="#collapseSeven" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle plus"> Messages and Notes </a> </div>
          <div class="accordion-body collapse" id="collapseSeven" style="height: 0px;">
            <div class="accordion-inner">
              <div class="offset1">
                  <div class="row">
                    <div class="span11">
                      <label class="radio">
						<form:radiobutton path="communicationsAccess" value="2" />
                        Branch Admins can see messages and notes from their districts only</label>
                    </div>
                    <div class="span11" style="padding-right: 15px;">
                      <label class="radio">
						<form:radiobutton path="communicationsAccess" value="1" />
                        Branch Admins are able to see messages and notes of their District. However, Branch Admin of one district cannot see messages and notes of other districts </label>
                    </div>
                    <div class="span11">
                      <label class="radio">
						<form:radiobutton path="communicationsAccess" value="0" />
                        All messages and notes are visible to all the Branch Admins within the HeadQuarter </label>
                    </div>
                  </div>
                
              </div>
            </div>
          </div>
      </div>
      
      <div class="panel panel-default">
          <div class="accordion-heading"> <a href="#collapseEight" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle plus"> Candidate Grid </a> </div>
          <div class="accordion-body collapse" id="collapseEight" style="height: 0px;">
            <div class="accordion-inner">
              <div class="offset1">
              	 <div class="row mt10">
                  		<div class="span11">
                    		Display selected columns in Candidate Grid <a href="#" id="iconpophovercg" rel="tooltip" data-original-title="Check the fields you want to view in Candidate Grid" data-placement="right"><img src="images/qua-icon.png" width="15" height="15" alt=""></a>
                  		</div>
                  </div>
                  <div class="row mt10">
                  <div class="row">
                  <div class="col-sm-2 col-md-2">
                      <label class="checkbox">
						 <form:checkbox path="displayCGPA" />
                        CGPA</label>
                    </div>
                    <div class="col-sm-2 col-md-2">
                      <label class="checkbox">
						 <form:checkbox path="displayAchievementScore" />
                        Achievement Score</label>
                    </div>
                    <div class="col-sm-2 col-md-2">
                      <label class="checkbox">
						 <form:checkbox path="displayFitScore"/>
                        Fit Score </label>
                    </div>
                    <div class="col-sm-2 col-md-2">
                      <label class="checkbox">
						 <form:checkbox path="displayTFA" />
                        TFA </label>
                    </div>
                    
                     <div class="col-sm-2 col-md-2">
                      <label class="checkbox">
						 <form:checkbox path="displayPhoneInterview" />
                        Phone Interview</label>
                    </div>
                    
                    </div>
                    <div class="row">
                    <div class="col-sm-2 col-md-2">
                      <label class="checkbox" style="margin-top: 0px;">
						 <form:checkbox path="displayDemoClass" />
                        Demo Lesson</label>
                    </div>
                    <div class="col-sm-2 col-md-2">
                      <label class="checkbox" style="margin-top: 0px;">
						 <form:checkbox path="displayYearsTeaching" />
                        Teaching Years  </label>
                    </div>
                    <div class="col-sm-2 col-md-2">
                      <label class="checkbox" style="margin-top: 0px;">
						 <form:checkbox path="displayExpectedSalary"/>
                        Expected&nbsp;Salary </label>
                    </div>
                    <div class="col-sm-2 col-md-2">
                      <label class="checkbox" style="margin-top: 0px;">
						 <form:checkbox path="displayJSI" />
                        JSI</label>
                    </div>   
                    <div class="col-sm-3 col-md-3">
                      <label class="checkbox" style="margin-top: 0px;">
						 <form:checkbox path="jobCompletionNeeded" id="jobCompletionNeeded" value="1"/>
                        <c:if test="${(userSession.headQuarterMaster ne null ) && (userSession.headQuarterMaster.headQuarterId eq 1)}">	
                        	<spring:message code="lbljobApplicationNeeded" /></label>
                        </c:if>	
                        <c:if test="${(userSession.headQuarterMaster eq null )}">
                        	<spring:message code="lbljobCompletionNeeded" /></label>
                        </c:if>	
                    </div>            
                    </div>
                  </div>
              </div>
            </div>
          </div>
       </div>  
        
      <!-- <div class="panel panel-default">
          <div class="accordion-heading"> <a href="#collapseNine" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle plus"> Score on Status </a> </div>
          <div class="accordion-body collapse" id="collapseNine" style="height: 0px;">
            <div class="accordion-inner">
              <div class="row">
                    <div class="col-sm-12 col-md-12">
              <div id="errorscorediv" class="required"></div>
              </div>
              </div>
    	         
				
                </div>
              <div class="clearfix">&nbsp;</div>
            </div>
       </div>-->
       
       <div class="panel panel-default">
          <div class="accordion-heading"> <a href="#collapseTen" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle plus">Privilege For HeadQuarter</a></div>
          <div class="accordion-body collapse" id="collapseTen" style="height: 0px;">
            <div class="accordion-inner">
    	      <div class="offset1">
    	      <div class="row mt10 hide">
                  		<div class="span11">
                    		Job Approval
                  		</div>
                </div>
                <div class="row hide">
                  	<div class="col-sm-5 col-md-5" >
                  	<c:set var="disablenoOfApprovalNeeded" value=""></c:set>					
						<c:if test="${headQuarter.approvalBeforeGoLive ne true}">
				         	<c:set var="disablenoOfApprovalNeeded" value="disabled"></c:set>	 
						</c:if>
                      <label class="checkbox" style="padding-left:5px;">                      
						 <form:checkbox path="approvalBeforeGoLive"  id="approvalBeforeGoLive1" onclick="return jobApprovalFunc();"/>
                        Approval process for positions prior to them going live</label>
                    </div>
                   
                </div>

    	      	<div class="row mt10">
                  		<div class="span11">
                    		Candidate Pool
                  		</div>
                </div>
    	      	<div class="row">
                  	<div class="span11">
                      <label class="checkbox">
						 <form:checkbox path="isTeacherPoolOnLoad" />
                        Show candidates on page load</label>
                    </div>
                </div>
    	      	
    	      	<div class="row mt10">
                  		<div class="span11">
                    		Status Notes
                  		</div>
                </div>
    	      	 
    	      	<div class="row">
                  	<div class="span11">
                      <label class="checkbox">
						 <form:checkbox path="statusNotes" />
						 Note is required to finalize each status</label>
                    </div>
                </div>
    	      	
    	      	<div class="row mt10">   
                    <div class="col-sm-12 col-md-12" style="margin-left: -15px;">
			            <form:checkbox path="statusPrivilegeForHeadquarter" cssClass="checkbox inline" style="vertical-align: bottom" id="statusPrivilegeForHeadquarter" value="1" onclick="disableStatusMasterEmailForHQ()" />
			        <span style="display: inline-block;">Headquarter can set status for the following nodes</span>
					</div>
					<div class="left15">
                    	 <c:forEach items="${lstStatusMaster}" var="lstsm">
	                  		<div class="col-sm-3 col-md-3 top5" style="padding-left: 20px;">
								<label class="checkbox inline" style="padding-left: 0px;">
									<form:checkbox path="chkHQStatusMaster" id="chkHQStatusMaster" value="${lstsm.statusId}" />
						            <%--  <input type="checkbox" name="chkStatusMaster" value="${lstsm.statusId}" />--%>
						            ${lstsm.status}
								</label>
							</div>
						 </c:forEach>
						
						 <c:forEach items="${lstsecondaryStatus}" var="lstSecSM">
							<div class="col-sm-3 col-md-3 top5" style="padding-left: 20px;">
								<label class="checkbox inline" style="padding-left: 0px;">
									<form:checkbox path="chkHQSecondaryStatusName" id="chkHQSecondaryStatusName" value="${lstSecSM.secondaryStatusId}" />
						           <%--  <input type="checkbox" name="chkSecondaryStatusName" value="${lstSecSM.secondaryStatusId}" />--%>
						            ${lstSecSM.secondaryStatusName}
								</label>
							</div>
						 </c:forEach>
						 </div>
                </div>
    	      	
                <div class="row">
                  	<div class="span11">
                      <label class="checkbox">
						 <form:checkbox path="autoNotifyCandidateOnAttachingWithJob" />
                        Auto-notify all the candidates on attaching with a job</label>
                    </div>
                </div>
                
                <div class="row">
                  	<div class="span11">
                      <label class="checkbox">
						 <form:checkbox path="autoNotifyOnStatusChange" id="autoNotifyOnStatusChange" value="" onclick="disableStatusMasterEmail();"/>
                       Auto-notify all of the associated Headquarters and Branch Admins of the status change for the following selected statuses</label>
                    </div>
                    <div class="left15">
					     <c:forEach items="${lstStatusMaster}" var="lstsm">
	                  		<div class="col-sm-3 col-md-3 top5" style="padding-left: 20px;">	                  		
	                  		       <label class="checkbox inline" style="padding-left: 0px;">
										<form:checkbox path="chkStatusMasterEml" id="chkStatusMasterEml" value="${lstsm.statusId}"/>
							            ${lstsm.status}
									</label>
							</div>
						 </c:forEach>
						
						 <c:forEach items="${lstsecondaryStatus}" var="lstSecSM">
							<div class="col-sm-3 col-md-3 top5" style="padding-left: 20px;">
								<label class="checkbox inline" style="padding-left: 0px;">
										<form:checkbox path="chkSecondaryStatusNameEml" id="chkSecondaryStatusNameEml" value="${lstSecSM.secondaryStatusId}" />
						            ${lstSecSM.secondaryStatusName}
								</label>
							</div>
						 </c:forEach>
				</div>
                </div>
                
                 <div class="row hide">
                  	<div class="span11">
                      <label class="checkbox">
						 <form:checkbox path="noEPI" />
                          No EPI Score Visible</label>
                    </div>
                </div>
              
                <div class="row mt10 hide">
                  		<div class="span11">
                  		<div class='required' id='errorpdistictdiv'></div>
                    		Job Category
                  		</div>
                </div>
                <div class="row  mt8 hide">
                	 <div class="span12 left20">
                	 	<label class="radio inline" style="padding-left: 0px;">
                		<c:choose>
                				<c:when test="${displayTMDefaultJobCategory eq true}">
                					<form:radiobutton  path="displayTMDefaultJobCategory" id="tmdefault" checked="checked" value="1"/>
					         	  	Display TeacherMatch's default job categories.
                				</c:when>
                				<c:otherwise>
                					<form:radiobutton  path="displayTMDefaultJobCategory" id="tmdefault"  value="1"/>
					         	  	Display TeacherMatch's default job categories.
                				</c:otherwise>
                		</c:choose>
                		</label>
                		<br>
                		<label class="radio inline" style="padding-left: 0px;">
                		<c:choose>
                				<c:when test="${displayTMDefaultJobCategory eq false}">
                					 <form:radiobutton path="displayTMDefaultJobCategory" id="districtdefault" checked="checked" value="0"/>
					           		 Display only Branch's default job categories.
                				</c:when>
                				<c:otherwise>
                					 <form:radiobutton path="displayTMDefaultJobCategory" id="districtdefault" value="0"/>
					            	 Display only Branch's default job categories.
                				</c:otherwise>
                		</c:choose>
                		</label>
                	</div>	
                </div>
                 
                 <div class="row mt10">
                  		<div class="span11">
                    		Status Work Flow
                  		</div>
                </div>
                
                <div class="row  mt8" >
                	 <div class="span12 left20">
                	 	<label class="radio inline" style="padding-left: 0px;">
	                	<c:choose>
	               				<c:when test="${headQuarterMaster.setAssociatedStatusToSetDPoints eq false}">	               					
	               					<form:radiobutton  path="setAssociatedStatusToSetDPoints" id="beforeDpoint" checked="checked" onclick="chkDStatus(false)" value="0" />
				            		Users can finalize decision point before finalizing the associated nodes.
	               				</c:when>
	               				<c:otherwise>
	               					<form:radiobutton  path="setAssociatedStatusToSetDPoints" id="beforeDpoint"  onclick="chkDStatus(false)"  value="0" />
				            		Users can finalize decision point(s) only after all associated nodes are finalized.
	               				</c:otherwise>
	                	</c:choose>
	                	</label>
	                	<br>
	                	<label class="radio inline" style="padding-left: 0px;">
	                	<c:choose>
	             				<c:when test="${headQuarterMaster.setAssociatedStatusToSetDPoints eq true}">
	             					<form:radiobutton  path="setAssociatedStatusToSetDPoints" id="afterDpoint"  onclick="chkDStatus(true)" checked="checked" value="1"/>
			            			Users can finalize decision point before finalizing the associated nodes.
	             				</c:when>
	             				<c:otherwise>
	             					<form:radiobutton  path="setAssociatedStatusToSetDPoints" id="afterDpoint"  onclick="chkDStatus(true)" value="1"/>
			            			Users can finalize decision point(s) only after all associated nodes are finalized.
	             				</c:otherwise>
	             				
	                	</c:choose>
	                	</label> 
	                </div>	
	                	<br>
	                	<div class="col-sm-12 col-md-12 left25" style="margin-top:-15px;">
	                	   <div  id="dPointsSceStatus">
						   <c:forEach items="${lstStatusMasterDPoints}" var="lstdp">
								<label class="checkbox inline col-sm-2 col-md-2" style="padding-left: 0px;margin-top: 0px;">
								<c:set var="accessDPointsVal" value="||${lstdp.statusId}||" />
								<c:choose>
									<c:when test="${fn:contains(districtMaster.accessDPoints,accessDPointsVal)}">
										<form:checkbox path="chkDStatusMaster" id="chkDStatusMaster" value="${lstdp.statusId}" checked="checked" />
									</c:when>	
									<c:otherwise>
										<form:checkbox path="chkDStatusMaster" id="chkDStatusMaster" value="${lstdp.statusId}" />
									</c:otherwise>
								</c:choose>
						            ${lstdp.status} 
								</label>
							</c:forEach>
						  </div>
						  </div>	                	
	            </div>
                 
                <div class="row hide">   
                    <div class="span12">
                        <form:checkbox path="offerVirtualVideoInterview" cssClass="checkbox inline" style="vertical-align: bottom" id="offerVirtualVideoInterview" value="1" onclick="showOfferVVI();" />
			            <span style="display: inline-block;">Offer Virtual Video Interview</span>
					</div>
					
					<div class="left15 hide" id="offerVVIDiv">
							   <div class="row">
							     <div class="col-sm-12 col-md-12">
							        	<div class="divErrorMsg" id="errorofferVVIDiv"></div>         
						           </div>
								</div>
								<div class="left15">
							<div class="row">
							     <!--<div class="col-sm-12 col-md-12 top25-sm23">
							             		<div  class="col-sm-6 col-md-6" style="margin:12px;">
							             			<label id="captionDistrictOrSchool">Please select a question set</label>
							             			<input type="hidden" id="quesId" name="quesId" value="${quesSetIDValue}"> 
								             		<input type="text" id="quesName" name="quesName"  class="form-control"
								             		onfocus="getQuestionSetAutoComp(this, event, 'divTxtShowData', 'quesName','quesId','');"
													onkeyup="getQuestionSetAutoComp(this, event, 'divTxtShowData', 'quesName','quesId','');"
													onblur="hideDistrictMasterDiv(this,'quesId','divTxtShowData');"	
													value="${quesSetNameValue}"/>
													<div id='divTxtShowData' style=' display:none;position:absolute;z-index:5000;' onmouseover="mouseOverChk('divTxtShowData','quesName')" class='result' ></div>	
								             	</div>		             
						           </div>
								--></div>
						
										<div class="row top5">
													<form:checkbox path="" cssClass="checkbox inline" style="vertical-align: bottom" id="wantScore" value="1" onclick="showMarksDiv();" />
													<span style="display: inline-block;">Want to score</span>
										</div>	
										<div class="row hide" id="marksDiv" >
								`				<div class="col-sm-3 col-md-3 top10">
													Max Marks		
													<form:input path="maxScoreForVVI" cssClass="form-control" id="maxScoreForVVI" maxlength="3" onkeypress="return checkForInt(event)" /> 
												</div>
										</div>
										<div class="row">
			                    	            <form:checkbox path="sendAutoVVILink" cssClass="checkbox inline" style="vertical-align: bottom" id="sendAutoVVILink" value="1" onclick="showLinkDiv();" />
									            <span style="display: inline-block;">Want to send the interview link automatically</span>
										</div>
								</div>	
						     
						                
			                <div class="row">
			                	<div class="mt10">
				                	<div  class="col-sm-3 col-md-3 top5">
										<label>Time Allowed Per Question In Sec</label>
										<form:input path="timeAllowedPerQuestion" id="timeAllowedPerQuestion" cssClass="form-control" maxlength="4" tabindex="-1" placeholder="" onkeypress="return checkForInt(event)" />
				                    </div>
				                    <div  class="col-sm-4 col-md-4 top5">	
				                    	<label>Virtual Video interview Expires In Days</label>
										<form:input path="VVIExpiresInDays" cssClass="form-control" style="width:200px;" maxlength="2" tabindex="-1" placeholder="" onkeypress="return checkForInt(event)" />
				            		</div>
			            		</div>
			            	</div>
			            	</div>
	            		</div>
	            		
	            <div class="row">
		           <div class="span12 top5">
						<form:checkbox path="sendReminderToIcompTalent" cssClass="checkbox inline" style="vertical-align: bottom" id="sendReminderToIcompCandiates" onclick="showReminderDiv();" value="1"/>
						<span style="display: inline-block;">Send reminder(s) to incomplete talent</span>
					</div>
				</div>
				<!-- shadab -->
				<div class="left15 hide" id="sendFrequencyReminder">
			 	<div class="row">
					<div class="col-sm-12 col-md-12">
						<div class="divErrorMsg" id="erroroReminderSet"></div>
				   </div>
				</div>
				
				<div class="row">
			        <div class="col-sm-6 col-md-6 top5">
			        	Total number of Reminders to be sent
			        </div>
			        <div class="col-sm-3 col-md-3 top5">
			        	<form:input path="noOfReminder" id="noOfReminder" cssClass="form-control" maxlength="3" placeholder="" onkeypress="return checkForInt(event);"/>
			         </div>
				</div>
			 
			 	 <div class="row">
			 		<div class="col-sm-6 col-md-6 top5">
			        	Number of days after which first Reminder will be sent 
			        </div>
			        <div class="col-sm-3 col-md-3 top5">
			        	<form:input path="reminderFrequencyInDays" id="reminderFrequencyInDays" cssClass="form-control" maxlength="3" placeholder="" onkeypress="return checkForInt(event);"/>
			        </div>
			        <div class="col-sm-2 col-md-2 top25-sm23" style="padding-top: 10px;">days</div>
			 	 </div>
			 	 
			 	 <div class="row">
			     	<div class="col-sm-6 col-md-6 top5">
						Number of days between two consecutive Reminders
					</div>
					
					<div class="col-sm-3 col-md-3 top5">
						<form:input path="reminderOfFirstFrequencyInDays" id="reminderOfFirstFrequencyInDays" cssClass="form-control" maxlength="3" placeholder="" onkeypress="return checkForInt(event);"/>
					</div>
					<div class="col-sm-2 col-md-2 top25-sm23" style="padding-top: 10px;">days</div>
			     </div>
				
			   <div class="row"> 
			        <div class="col-sm-6 col-md-6 top5">
			        	Action to be performed if applicant does not respond after all the Reminder(s)
			        </div>
			        <div class="col-sm-3 col-md-3 top5">
			        	<select class="form-control" id="statusIdReminderExpireAction" name="statusIdReminderExpireAction">
				            <option value="">No action taken </option>
		                    <c:forEach items="${lstStatusMaster_PFC}" var="lstsmForReminder">
								<c:set var="selected" value=""></c:set>						
				        		<c:if test="${lstsmForReminder.statusId eq statusIdReminderExpireAction}">
				         			<c:set var="selected" value="selected"></c:set>
				        		</c:if>
								<option value="${lstsmForReminder.statusId}" ${selected} >${lstsmForReminder.status}</option>
							</c:forEach>
		              </select>
			        </div>
			    </div>
			 </div>
				<!-- shadab end -->
				<div class="row">
		           <div class="span12 top5">
		           		<form:checkbox path="sendReferenceOnJobComplete" cssClass="checkbox inline" style="vertical-align: bottom" disabled = "disabled" id="sendReferenceOnJobCompleteId" value="1"/>
						<span style="display: inline-block;">Send e-References on the submission of the talent application.</span>
					</div>
				</div>
					
				 	<!--  Send --Reference on Finalize status  Anurag 28 Aug 15-->
					
						<div class="row top10" >
					<div class="col-sm-6 col-md-6 top2" style="margin-left: -12px;">
						<spring:message code="msgSendeReference1" />
					</div>
					<div class="col-sm-4 col-md-4 top25-sm23" style="margin-left: 20px;">
					<select class="form-control" id="statusListBoxForReminder" name="statusListBoxForReminder">
		            <option value=""></option>
                       <c:forEach items="${lstStatusMaster_PFC}" var="lstsmForReminder">
						<c:set var="selected" value=""></c:set>						
		        		<c:if test="${lstsmForReminder.statusId eq statusListBoxValueForReminder}">	        			
		         			<c:set var="selected" value="selected"></c:set>	         			
		        		</c:if>	
						<option value="${lstsmForReminder.statusId}" ${selected} >${lstsmForReminder.status}</option>
					</c:forEach>
					
				 	<c:set var="lstvalueForReminder" value=""></c:set>
					<c:forEach items="${lstsecondaryStatus_PFC}" var="lstSecSMForReminder">
						<c:set var="selected" value=""></c:set>
						<c:set var="lstvalueForReminder" value="SSIDForReminder_${lstSecSMForReminder.secondaryStatusId}"></c:set>
						
						<c:if test="${lstvalueForReminder eq statusListBoxValueForReminder}">	        			
		         			<c:set var="selected" value="selected"></c:set>	         			
		        		</c:if>
						<option value="SSIDForReminder_${lstSecSMForReminder.secondaryStatusId}" ${selected} >${lstSecSMForReminder.secondaryStatusName}</option>
					</c:forEach>
				
                     </select>
                      
					 </div>                      
				</div>
					
					
			    </div>
			    </div>
			 </div>
              </div>
        
        <div class="panel panel-default">
        <c:choose>
          <c:when test="${branchExists}">
          <div class="accordion-heading"> <a href="#collapseEle" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle plus">Privilege For Branches</a></div>
          </c:when>
          <c:otherwise>
          <div class="accordion-heading"> <a href="#collapseEle" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle plus">Privilege For District(s)</a></div>
          </c:otherwise>
          </c:choose>
          <div class="accordion-body collapse" id="collapseEle" style="height: 0px;">
            <div class="accordion-inner">
    	      <div class="offset1">
    	      	
    	      	<div class="row mt10">
                  		<div class="span11">
                    		Read/write privilege <!-- <a href="#" id="iconpophovercg" rel="tooltip" data-original-title="Check the fields you want to view in Candidate Grid" data-placement="right"><img src="images/qua-icon.png" width="15" height="15" alt=""></a> -->
                  		</div>
                </div>
                
                <div class="row  mt10">                    
                    <div class="span12 left20">
				 	<label class="checkbox inline" style="padding-left: 0px;">
			            <form:checkbox path="writePrivilegeToBranch" value="1"/>
			            Non-Attached branches may view the job order and candidate pool.
					</label>
					</div>                    
                </div>
                
                 <div class="row  mt10">                    
                    <div class="span12 left20">
				 	<label class="checkbox inline" style="padding-left: 0px;">
			            <form:checkbox path="resetQualificationIssuesPrivilegeToBranch" value="1"/>
			            Atttached branches can view the status of the qualification question responses.
					</label>
					</div>                    
                </div>
                
                 <div class="row mt10">   
                    <div class="col-sm-12 col-md-12" style="margin-left: -15px;">
			            <form:checkbox path="statusPrivilegeForBranches" cssClass="checkbox inline" style="vertical-align: bottom" id="statusPrivilegeForBranches" value="1" onclick="disableStatusMaster()" />
			        <span style="display: inline-block;">Branches can set status for the following nodes</span>
					</div>
					<div class="left15">
                    	 <c:forEach items="${lstStatusMaster}" var="lstsm">
	                  		<div class="col-sm-3 col-md-3 top5" style="padding-left: 20px;">
								<label class="checkbox inline" style="padding-left: 0px;">
									<form:checkbox path="chkStatusMaster" id="chkStatusMaster" value="${lstsm.statusId}" />
						            <%--  <input type="checkbox" name="chkStatusMaster" value="${lstsm.statusId}" />--%>
						            ${lstsm.status}
								</label>
							</div>
						 </c:forEach>
						
						 <c:forEach items="${lstsecondaryStatus}" var="lstSecSM">
							<div class="col-sm-3 col-md-3 top5" style="padding-left: 20px;">
								<label class="checkbox inline" style="padding-left: 0px;">
									<form:checkbox path="chkSecondaryStatusName" id="chkSecondaryStatusName" value="${lstSecSM.secondaryStatusId}" />
						           <%--  <input type="checkbox" name="chkSecondaryStatusName" value="${lstSecSM.secondaryStatusId}" />--%>
						            ${lstSecSM.secondaryStatusName}
								</label>
							</div>
						 </c:forEach>
						 </div>
                </div>
                
                <div class="row mt10">
                 	<div class="col-sm-6 col-md-6 top5" style="max-width:430px;">
			        Branches can view the talents in candidate grid once district admin sets 
			        </div>    
			            <div class="col-sm-3 col-md-3 top25-sm23">
			            <select class="form-control" id="statusListBox" name="statusListBox">
			            <option value=""></option>
                        <c:forEach items="${lstStatusMaster}" var="lstsm">
							<c:set var="selected" value=""></c:set>						
			        		<c:if test="${lstsm.statusId eq statusListBoxValue}">	        			
			         			<c:set var="selected" value="selected"></c:set>	         			
			        		</c:if>	
							<option value="${lstsm.statusId}" ${selected} >${lstsm.status}</option>
						</c:forEach>
						
						<c:set var="lstvalue" value=""></c:set>
						<c:forEach items="${lstsecondaryStatus}" var="lstSecSM">
							<c:set var="selected" value=""></c:set>
							<c:set var="lstvalue" value="SSID_${lstSecSM.secondaryStatusId}"></c:set>
							<c:if test="${lstvalue eq statusListBoxValue}">	        			
			         			<c:set var="selected" value="selected"></c:set>	         			
			        		</c:if>
							<option value="SSID_${lstSecSM.secondaryStatusId}" ${selected} >${lstSecSM.secondaryStatusName}</option>
						</c:forEach>
                      </select>
                      </div>                      
			       <div class="col-sm-3 col-md-3 top5 top25-sm23">
			           &nbsp;status against the talent.
					</div>
                </div>
    	      </div>
    	       <div class="clearfix">&nbsp;</div>
            </div>
          </div>
          </div>
        
         <!--<div class="panel panel-default">
          <div class="accordion-heading"> <a href="#collapseFifteen" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle plus">Privilege For Internal Transfer Candidate(s)</a></div>
          <div class="accordion-body collapse" id="collapseFifteen" style="height: 0px;">
            <div class="accordion-inner">
    	      <div class="offset1">
	    	      	<div class="row mt10">
	                  		<div class="span11">
	                    		 When a Internal Candidate applies for a job
	                  		</div>
	                </div>
	                <div class="row  mt8" >
	                	 <div class="span12 left20">
	                	 	<label class="checkbox inline" style="padding-left: 0px;">
	                	 		<form:checkbox path="offerPortfolioNeeded"/>Offer Portfolio</br>
	                	 		<form:checkbox path="offerDistrictSpecificItems"/>Offer District Specific Mandatory Items</br>
	                	 		<form:checkbox path="offerQualificationItems"/>Offer Qualification Items</br>
	                	 		<form:checkbox path="offerEPI"/>Offer EPI</br>
	                	 		<form:checkbox path="offerJSI"/>Offer JSI (if a JSI is offered with the job)</br>
	       					</label>
	                	</div>	
	               </div>
	             </div>
              <div class="clearfix">&nbsp;</div>
            </div>
          </div>
        </div>
        
         <div class="panel panel-default">
          <div class="accordion-heading"> <a href="#collapseSixteen" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle plus">Branch Domain(s)</a></div>
          <div class="accordion-body collapse" id="collapseSixteen" style="height: 0px;">
            <div class="accordion-inner">
    	      <div class="offset1">
    	      
    	         <div class="row">
            
                 </div>
    	      	  	 
                </div>
            </div>
          </div>
        </div>-->
        
        <div class="panel panel-default">
          <div class="accordion-heading"> <a href="#collapseFour" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle plus"> Account Information </a> </div>
          <div class="accordion-body collapse" id="collapseFour" style="height: 0px;">
            <div class="accordion-inner">
               <div class="offset1">
	                <div class="row">
		                <div class="">
								 	<div class='required' id='erroraccountinfodiv'></div>
								 	<div class='required' id='errorhqbranchdiv'></div>
								 	<div class='required' id='errornotediv'></div>
								 	<div class='required' id='errordatediv'></div>
						</div>	
					</div>	
					<div class="row hide">
		                <div class="col-sm-12 col-md-12" style="margin-left:-15px;"> 	
		                  <p> These options were defined during the initial contracting process. To propose changes or to discuss further add-ons, please contact our client partnership team at 1-(888)-312-7231 or via email at <a href="mailto:clientservices@teachermatch.net">clientservices@teachermatch.net</a></p>
		                </div>
	                </div>
              </div>
              
                <c:if test="${(userSession.entityType ne 1) && (userSession.entityType ne 5)}">	
        			<c:set var="disable" value="true"></c:set>
         		</c:if>
              <div class="row ">
                <div class="offset1">
                  <div class="row hide">
                        <div class="col-sm-3 col-md-3">
                        <label>Contract Start Date</label>
						<form:input path="contractStartDate" disabled="${disable}" cssClass="form-control"   value="${contractStartDate}"/>
                        </div>
                      	<div class="col-sm-3 col-md-3">
                        <label>Contract End Date</label>
						<form:input path="contractEndDate" disabled="${disable}" cssClass="form-control"   value="${contractEndDate}"/>
                 		</div>
                 	</div>	
                  <div class="row  hide">
                    <div class="col-sm-3 col-md-3">
                      <label>Districts Under Contract</label>
					  <form:input path="branchesUnderContract" disabled="${disable}" cssClass="form-control" maxlength="9" placeholder=""/>
                    </div>
                  </div>
               
                  <div class="row  hide">
                    <div class="col-sm-3 col-md-3">
                      <label>Annual Subscription Amount</label>
						<form:input path="annualSubsciptionAmount" disabled="${disable}" cssClass="form-control" maxlength="50" placeholder=""/>
                    </div>
                     <c:if test="${userSession.entityType eq 1}">	
                      	<div class="col-sm-3 col-md-3">
                      	<label>Authentication Key</label>
							<input type="text" name="authKeyVal" readonly="readonly"  class="form-control"  value="${authKey}"  />
                    	</div>
                     </c:if>
                  </div>
                  <c:if test="${branchExists}">
                  <div class="row  hide">
                    <div class="col-sm-6 col-md-6 mt10">
                      <p>Which Branch are under contract</p>
                    </div>
                  </div>
                  
                  <div class="row  hide">
                    <div class="col-sm-3 col-md-3">
                    <c:if test="${headQuarterMaster.noBranchUnderContract eq 2 && headQuarterMaster.allBranchesUnderContract eq 2 && headQuarterMaster.selectedBranchesUnderContract eq 2}">	
	        			<c:set var="checked" value=""></c:set>
	         			<c:set var="checked" value="checked"></c:set>        			
	        		</c:if>
                      <label class="radio" style="margin-top: 0px;">
						<form:radiobutton path="noBranchUnderContract" disabled="${disable}" value="1" onclick="uncheckedOtherRadio(1)" checked="${checked}" />
                        No Branch Attached</label>
                    </div>
                  </div>  
                  
                  <div class="row">
					<div class="col-sm-12 col-md-12">
						<div class="divErrorMsg" id="erroroAccInfoSet"></div>
				   </div>
				</div>
                     
                  <div class="row ">
                    <div class="col-sm-5 col-md-5">
                      <label class="radio" style="margin-top: 0px;">
						<form:radiobutton path="allBranchesUnderContract" disabled="${disable}" value="1" onclick="uncheckedOtherRadio(2)"  />
                        All branches attached to the headquarters are under contract </label>
                    </div>
                  </div>
                  
                  <div class="row  hide">
                     <div class="col-sm-3 col-md-3 mt10 left20">                      
	                        <div>
	                          <label class="radio inline" style="padding-left: 0px;">
								<form:radiobutton path="selectedBranchesUnderContract"  disabled="${disable}" value="1" onclick="uncheckedOtherRadio(4)"  />
	                            Selected Branches </label>
	                        </div>
	                 </div>    
                            <c:set var="showHideAddSchoolLink" value="hide"></c:set>					
			        		<c:if test="${branchMaster.selectedBranchesUnderContract eq 1}">	        			
			         			<c:set var="showHideAddSchoolLink" value=""></c:set>	         			
			        		</c:if>	
			        		<div class="col-sm-6 col-md-6 left18 mt10">
				        		<div class="pull-right ${showHideAddSchoolLink}" id="addSchoolLink">
			                         <c:if test="${fn:indexOf(roleAccessAI,'|1|')!=-1}">
			                        <a href="javascript:void(0);" onclick="addBranch();">+ Add Branches</a>
			                        </c:if>
	                             </div> 
			        		</div>
                        
                  </div>
                  
                  <input type="hidden" id="gridNo" name="gridNo" value="">
                  <%-- ======== District School Grid ========= --%>
                 <div class="row col-sm-9 col-md-9 hide">
                 	 <div  id="hqBranchesDistrict"  style="padding-bottom: 10px;" onclick="getHQBDGrid()">
                     </div>
                 </div>                   
                  <div id="addBranchDiv" class="hide">
	                  <div class="row col-sm-10 col-md-10" style="max-width:755px;">
	                    <div>
	                      <label>Branch Name</label>
	             						<input type="text" id="branchName" name="branchName"  class="form-control"
	             						onfocus="getBranchMasterAutoComp(this, event, 'divTxtShowDataBranch', 'branchName','branchId','');"
										onkeyup="getBranchMasterAutoComp(this, event, 'divTxtShowDataBranch', 'branchName','branchId','');"
										onblur="hideBranchMasterDiv(this,'branchId','divTxtShowDataBranch');"	/>
										<input  type="hidden" id="branchId" name="branchId" value="">
							<div id='divTxtShowDataBranch' style=' display:none;position:absolute;z-index:5000;' onmouseover="mouseOverChk('divTxtShowDataBranch','branchName')" class='result' ></div>
							
	                    </div>
	                  </div>
	                  <div class="row col-sm-5 col-md-5">
                 		 <div class="span2 idone"><a href="javascript:void(0);" onclick="return saveHqBranchDistrict();"><spring:message code="lnkImD"/></a>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onclick="clearHqBranchDistricts()">Cancel</a></div>
                	  </div>
                  </div>
                  </c:if>
                  <c:if test="${branchExists eq false}">
                  <div class="row  hide">
                    <div class="col-sm-6 col-md-6 mt10">
                      <p>Which District are under contract</p>
                    </div>
                  </div>
                  
                  <div class="row  hide">
                    <div class="col-sm-3 col-md-3">
                    <c:if test="${headQuarterMaster.noDistrictUnderContract eq 2 && headQuarterMaster.allDistrictsUnderContract eq 2 && headQuarterMaster.selectedDistrictsUnderContract eq 2}">	
	        			<c:set var="checked" value=""></c:set>
	         			<c:set var="checked" value="checked"></c:set>        			
	        		</c:if>
                      <label class="radio" style="margin-top: 0px;">
						<form:radiobutton path="noDistrictUnderContract" disabled="${disable}" value="1" onclick="uncheckedOtherRadio2(1)"  />
                        No District Attached</label>
                    </div>
                  </div>  
                                  
                  <div class="row  hide">
                    <div class="col-sm-3 col-md-3">
                      <label class="radio" style="margin-top: 0px;">
						<form:radiobutton path="allDistrictsUnderContract" disabled="${disable}" value="1" onclick="uncheckedOtherRadio2(2)"  />
                        All Districts in the HeadQuarter </label>
                    </div>
                  </div>
                  
                  <div class="row  hide">
                     <div class="col-sm-3 col-md-3 mt10 left20">                      
	                        <div>
	                          <label class="radio inline" style="padding-left: 0px;">
								<form:radiobutton path="selectedDistrictsUnderContract"  disabled="${disable}" value="1" onclick="uncheckedOtherRadio2(4)"  />
	                            Selected Districts </label>
	                        </div>
	                 </div>    
                            <c:set var="showHideAddSchoolLink" value="hide"></c:set>					
			        		<c:if test="${branchMaster.selectedDistrictUnderContract eq 1}">	        			
			         			<c:set var="showHideAddSchoolLink" value=""></c:set>	         			
			        		</c:if>	
			        		<div class="col-sm-6 col-md-6 left18 mt10">
				        		<div class="pull-right ${showHideAddSchoolLink}" id="addSchoolLink">
			                         <c:if test="${fn:indexOf(roleAccessAI,'|1|')!=-1}">
			                        <a href="javascript:void(0);" onclick="addDistict();">+ Add District</a>
			                        </c:if>
	                             </div> 
			        		</div>
                        
                  </div>
                  <input type="hidden" id="gridNo" name="gridNo" value="">
                  <%-- ======== Branch District Grid ========= --%>
                 <div class="row col-sm-9 col-md-9 hide">
                 	 <div  id="hqDistrict"  style="padding-bottom: 10px;" onclick="getHQBDGrid()">
                     </div>
                 </div>                   
                  <div id="addDistrictDiv" class="hide">
	                  <div class="row col-sm-10 col-md-10" style="max-width:755px;">
	                    <div>
	                      <label>District Name</label>
	             				<input type="text" id="districtName" name="districtName"  class="form-control"
			             		onfocus="getDistrictMasterAutoComp(this, event, 'divTxtShowData', 'districtName','districtId','');"
								onkeyup="getDistrictMasterAutoComp(this, event, 'divTxtShowData', 'districtName','districtId','');"
								onblur="hideDistrictMasterDiv(this,'districtId','divTxtShowData');"	/>
								<input type="hidden" id="districtId" value=""/>
							<div id='divTxtShowData' style=' display:none;position:absolute;z-index:5000;' onmouseover="mouseOverChk('divTxtShowData','districtName')" class='result' ></div>	
							
	                    </div>
	                  </div>
	                  <div class="row col-sm-5 col-md-5">
                 		 <div class="span2 idone"><a href="javascript:void(0);" onclick="return saveHqDistrict();"><spring:message code="lnkImD"/></a>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onclick="clearHqDistricts()">Cancel</a></div>
                	  </div>
                  </div>
                  
                  </c:if>
        		
                <div class="row top15">
                    <div class="col-sm-12 col-md-12">
                      <p>Hiring decision in the status workflow for District Job Order will be taken by</p>
                    </div>
                 </div>
                 
				<div class="row" >
                    <div class="col-sm-12 col-md-12">
	        			<c:set var="checked" value=""></c:set>
	                    <c:if test="${headQuarterMaster.hiringAuthority eq ''}">	
		         			<c:set var="checked" value="checked"></c:set>        			
		        		</c:if>
	        		
                      <label class="radio" style="margin-top: 0px;">
						<form:radiobutton path="hiringAuthority" value="H" />
                        HeadQuarter Administrator(s)
                      </label>
                    </div>
                    
                    <div class="col-sm-12 col-md-12">
                      <label class="radio"  style="margin-top: 0px;">
						<form:radiobutton path="hiringAuthority"  value="B" checked="${checked}" />
                        Branch Administrator(s)
                      </label>
                    </div>
               </div>
                  
                   <div class="row  hide">
	                    <div class="col-sm-12 col-md-12">
	                      <p>Branch(s) can create their own job orders without authorization from the HeadQuarter</p>
	                    </div>
                   </div>
                   
				<div class="row hide">
                    <div class="col-sm-12 col-md-12">
                      <label class="radio">
						<form:radiobutton path="hQApproval" value="1"  checked="checked" />
                        Yes</label>
                    </div>
                    <div class="col-sm-12 col-md-12">
                      <label class="radio">
						<form:radiobutton path="hQApproval"  value="0" />
                        No </label>
                    </div>
                </div>
                  
                
                  
                   <c:if test="${userSession.entityType ne 1}">	
        			<c:set var="disableForTm" value="true"></c:set>
         		   </c:if>
                    
                   <c:if test="${userSession.entityType eq 1}">	                 
					   <div class="row top10 left5">
			                        <div class="col-sm-4 col-md-4">
										<label class="checkbox inline" style="padding-left: 0px;">
				                        	<form:checkbox path="isPortfolioNeeded" id="isPortfolioNeeded"  value="1"/>
											Is Portfolio Needed <a href="#" id="iconpophover15" rel="tooltip" data-original-title="Select this checkbox if you want the Candidates to complete their Portfolio first to complete the jobs of this district. Unselect this checkbox if you allow the Candidates to complete the jobs of this district without completing the Portfolio."><img src="images/qua-icon.png" width="15" height="15" alt=""></a>
										</label>
								  	</div>
			                        <div class="col-sm-4 col-md-4">
				                      <a href="javascript:void(0);" onclick="setEmailFormat()"> Set Email Format</a>
			                        </div>
                 			
	                   </div>
	                   
	                   <div class="row mt10 left5">
		                  <div class="col-sm-12 col-md-12">
		                        <label class="checkbox inline" style="padding-left: 0px;">
		                        <form:checkbox path="isWeeklyCgReport" id="isWeeklyCgReport"  value="1"/>
								Weekly CG Report <a href="#" id="iconpophover16" rel="tooltip" data-original-title="Select this checkbox if you want to receive the links for weekly CG Reports for all your jobs, in your mail box. Unselect this checkbox if you do not want receive the links for weekly CG Reports for all your jobs, in your mail box."><img src="images/qua-icon.png" width="15" height="15" alt=""></a>
								</label>
						   </div>
	                   </div>
                   </c:if>
                   
                   <div class="row col-sm-11 col-md-11 left5 top10">
	                  <label class="checkbox inline" style="padding-left: 0px;">
	                  <input type="hidden" name="aJobRelation" value="${aJobRelation}"/>
	                   <input type="checkbox"  id="cbxUploadAssessment" name="cbxUploadAssessment" onclick="uploadAssessment(1);" ${(headQuarterMaster.assessmentUploadURL != null ) ? 'checked' : ''}/>
	                   Yes, I would like to attach a default Job Specific Inventory <!-- <a href="#" id="iconpophover8" rel="tooltip" data-original-title="Lorem ipsum dolor sit&#013;&#10;amet, consectetur&#013;&#10;adipiscing elit. Morbi&#013;&#10;non quam nunc"><img src="images/qua-icon.png" width="15" height="15" alt=""></a> -->
	                  </label>
                  </div>
                   <div  class="row hide top10" id="uploadAssessmentDiv">				 
						 
						 <div class="col-sm-11 col-md-11">
						 Please upload Job Specific Inventory file (Acceptable formats are PDF, MS-Word,GIF, PNG and JPEG. Maximum file size 10MB)
						</div>
						<div class="col-sm-4 col-md-4">
							<form:input path="assessmentUploadURLFile" type="file"/>
						</div>
												
						<div class="col-sm-4 col-md-4">
						<a data-original-title='HeadQuarter' rel='tooltip' id='JSIHeadQuarter' href='javascript:void(0)'  onclick="showFile(2,'headQuarter','${headQuarterMaster.headQuarterId}','${headQuarterMaster.assessmentUploadURL}','JSIHeadQuarter');${windowFunc}"><c:out value="${(headQuarterMaster.assessmentUploadURL==''|| headQuarterMaster.assessmentUploadURL==null)?'':'view'}"/></a>
						</div>					
                  </div>
                  
                  <input type="hidden" id="assessmentUploadURLVal" name="assessmentUploadURLVal" value="${(headQuarterMaster.assessmentUploadURL != null ) ? '1' : '0'}" />
                   <div style="clear:both"></div>
                   <div class="row mt10">
	                  <div class="col-sm-12 col-md-12">
	                    Job Board URL
	                    <div class="">
							<a target="blank" href="${JobBoardURL}">${JobBoardURL}</a>
	                    </div>
	                  </div>
                  </div>
                  
                   <div class="row mt10 left5 ">
	                  <div class="col-sm-6 col-md-6">
	                        <label class="checkbox inline" style="padding-left: 0px;">
	                        <form:checkbox path="allowMessageTeacher" id="allowMessageTeacher"  onclick="emailForTeacherDivCheck();"  value="1"/>
							Allow message from talent
							</label>
					   </div>
                   </div>                   
                   
                    <c:if test="${userSession.entityType eq 1}">
                         <div id=""  class="row mt10">			
	                  	  	<div class="col-sm-6 col-md-6">
		                      <label >Exclusive Period (in days)</label>
		                    </div>
		                   </div> 
		                   
		                    <div class="row">  
		                    <div class="col-sm-6 col-md-6">
								<form:input path="exclusivePeriod" cssClass="form-control" maxlength="3"/>
							 </div>
							  </div>						
                   </c:if>
                   
				  
        		 <div class="clearfix"></div>                  
                   
                   <div id="emailForTeacherDiv"  class="row mt10">
                  	 <div class="col-sm-6 col-md-6">
                      <label >Email address to receive message from talent.</label>
						<form:input path="emailForTeacher" cssClass="form-control" maxlength="100"/>
					 </div>
	                </div>
	                
	                 <div class="row">
                    <div class="col-sm-12 col-md-12">
                      <label class="radio">
                        <input type="radio" value="1" checked="checked" id="exitURLRadio" name="talentComplete"/>
                        Talent will complete full application process and wiil be routed to following URL <img src="images/qua-icon.png" width="15" height="15" alt=""></a> </label>
                    </div>
                  </div>
                  <div class="row">
                  <div class="col-sm-10 col-md-10" style="max-width: 750px;">
                  	<form:input path="exitURL" cssClass="form-control" maxlength="250"/>
                  </div>
                  </div>
                  
                  <div class="row mt10">
                    <div class="col-sm-12 col-md-12">
                      <label class="radio">
                      <c:set var="compCheck" value=""></c:set>
                      <c:if test="${not empty headQuarterMaster.completionMessage}">
                      <c:set var="compCheck" value="checked"></c:set>
                      </c:if>
                        <input type="radio" value="1" id="completeMessageRadio" ${compCheck} name="talentComplete"/>
                        Talent will complete full application process and be shown the following message <img src="images/qua-icon.png" width="15" height="15" alt=""></a> </label>
                    </div>
                  </div>
                  
                  <div class="row">
                  <div class="col-sm-12 col-md-12">
                    <p>Completion Message </p>
                    <div style="width:710px;"   id="eMessage">
                    <label class="redtextmsg">(If you are planning to copy and paste text from a web page and/or a Microsoft document, please ensure
					  to paste that text first into a simple text editor such as Notepad to remove the special characters and
 					  then copy and paste it into the field below. Otherwise, the text directly cut and pasted from a web page 
 					  and/or a Microsoft document may not display properly to the reader)</label>
					<!--<textarea class="form-control" rows="3" maxlength="1000"></textarea>
					--><form:textarea path="completionMessage" rows="3" maxlength="1500" onkeyup="return chkLenOfTextArea(this,1500);" onblur="return chkLenOfTextArea(this,1500);" class="form-control"/>
                    </div>
                  </div>
                  </div>
        		</div>
              </div>
            </div>
          </div>
        </div>
        
        </div>
        </div>
        </div>
        
               <div class="modal hide"  id="myModalEmail" >
			<div class="modal-dialog-for-cgmessage">
		    <div class="modal-content">
			<div class="modal-header">
		  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
				<h3 id="myModalLabel">Teachermatch</h3>
			</div>
		
			<div  class="modal-body">
				<div class="control-group">
					<div class='divErrorMsg' id='errordivEmail'></div>
				</div>
				<div id='support'>
					<div class="control-group">
						<div class="">
					    	<label><strong>From</strong><span class="required">*</span></label>
				        	<input type="text" id="fromAddress" name="fromAddress"   class="form-control" maxlength="250" />
						</div>
					</div>
					
					<div class="control-group">
						<div class="">
					    	<label><strong>Subject</strong><span class="required">*</span></label>
				        	<input  type="text"  id="subjectLine" name="subjectLine"class="form-control" maxlength="250" />
						</div>
					</div>
				            
			        <div class="control-group">
						<div class="" id="mailBody">
					    	<label><strong>Message</strong><span class="required">*</span></label>
					    	<label class="redtextmsg">(If you are planning to copy and paste text from a web page and/or a Microsoft document, please ensure
						  	 to paste that text first into a simple text editor such as Notepad to remove the special characters and
 						  	 then copy and paste it into the field below. Otherwise, the text directly cut and pasted from a web page 
 						   	 and/or a Microsoft document may not display properly to the reader)</label>
				        	<textarea rows="5" class="form-control" cols="" id="" name=""></textarea>
						</div>
					</div>
		 		</div>
		 	</div>
		 	
		 	<div class="modal-footer">
		 		<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
		 		<button class="btn btn-primary"  onclick='return saveEmailFormat();' >Save</button>
		 	</div>
	</div>
	 	</div>
	</div>
       
       <div class="mt30">
       
		<!--<span style="display:block"><form:input  path="completionMessage" cssClass="form-control"  /></span>
        -->
        <c:if test="${fn:indexOf(roleAccessU,'|1|')!=-1}">
        	<button type="submit" class="btn btn-large btn-primary" ><strong>Save HeadQuarter <i class="icon"></i></strong></button>
        </c:if>
          &nbsp;&nbsp;<a href="javascript:void(0);" onclick="cancelBranch();">Cancel</a><br>
          <br>
        </div>
      
  
  <iframe src="" id="iframeJSI" width="100%" height="480px" style="display: none;">
  </iframe> 
   
  <div  class="modal hide"  id="myModalMsg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"   style="z-index: 5000;"  data-backdrop="static">
		<div class="modal-dialog">
		<div class="modal-content">
		<div class="modal-header">
	  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick="return aKeyContactDiv(0);">x</button>
			<h3 id="myModalLabel">Key Contact</h3>
		</div>
		<div class="modal-body">		
			<div class="control-group">
				<div class="">
			    	<span id="Msg"></span>	        	
				</div>
			</div>
	 	</div>
	 	<div class="modal-footer">
	 		<button class="btn" data-dismiss="modal" aria-hidden="true"  onclick="return aKeyContactDiv(1);">Ok</button>
	 		<button class="btn" data-dismiss="modal" aria-hidden="true"  onclick="return aKeyContactDiv(0);">Close</button> 		
 		</div>
	</div>
	</div> 
	</div>

	<div style="display:none;" id="loadingDiv">
		<table  align="center" >
			 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
		</table>
	</div>
		</form:form>
		
		
		
<script> 
	uploadAssessment(${(headQuarterMaster.assessmentUploadURL != null ) ? '1' : '0'});
	showFile(1,'headQuarter','${headQuarterMaster.headQuarterId}','${headQuarterMaster.logoPath}',null);	
</script>


<script type="text/javascript">

var selectIds = $('#collapseOne,#collapseTwo,#collapseThree,#collapseFour,#collapseFive,#collapseSix,#collapseSeven,#collapseEight,#collapseNine,#collapseTen,#collapseEle,#collapseFifteen,#collapseSixteen');
$(function ($) {
    selectIds.on('show.bs.collapse hidden.bs.collapse', function () {
        $(this).prev().find('.accordion-toggle').toggleClass('plus minus');
    })
});


$('.accordion').collapse();
$('.accordion').on('show', function (e) {
$(e.target).prev('.accordion-heading').find('.accordion-toggle').addClass('minus').removeClass('plus');
});

$('.accordion').on('hide', function (e) {
$(this).find('.accordion-toggle').not($(e.target)).removeClass('minus').addClass('plus');
});


function emailForTeacherDivCheck(){
	var allowMessageTeacher=document.getElementById("allowMessageTeacher").checked;
	if(allowMessageTeacher==1){
		$("#emailForTeacherDiv").fadeIn();
		$("#emailForTeacher").focus();
	}else{
		$("#emailForTeacherDiv").hide();
		$("#allowMessageTeacher").focus();
		document.getElementById("emailForTeacher").value="";
	}
}
emailForTeacherDivCheck();
</script>
<script type="text/javascript">
$('#myModal').modal('hide');
$('#iconpophover1').tooltip();
$('#iconpophover2').tooltip();
$('#iconpophover3').tooltip();
$('#iconpophover4').tooltip();
$('#iconpophover5').tooltip();
$('#iconpophover6').tooltip();
$('#iconpophover7').tooltip();
$('#iconpophover8').tooltip();
$('#iconpophover9').tooltip();
$('#iconpophover10').tooltip();
$('#iconpophover11').tooltip();
$('#iconpophover12').tooltip();
$('#iconpophover13').tooltip();
$('#iconpophover14').tooltip();
$('#iconpophover15').tooltip();
$('#iconpophover16').tooltip();
$('#iconpophovercg').tooltip();
$('#iconpophover17').tooltip();
$('#iconpophover22').tooltip();

$(document).ready(function(){
  		$('#eMessage').find(".jqte").width(724);
  		$('#dNote').find(".jqte").width(724);
  		//$('#dDescription').find(".jqte").width(535);
}) 

</script>
<script><!--
	function disableSlider()
	{
		if($('#mosaicRadios1').is(':checked')) 
		{ 
	   		$("#mosaicRadiosFlag").val("1");
	   		$("#candidateFeedDaysOfNoActivity").val("0");
	   			try
	    		{
	    			document.getElementById("ifrm2").src="slideract.do?name=slider2&tickInterval=10&max=${sliderMaxLength }&swidth=200&svalue=${candidateFeedDaysOfNoActivityValue}&dslider=1";
    			}
	    		catch(e)
	    		{} 
	    }
	    else
	    {
	    	if($('#mosaicRadios2').is(':checked')) 
			{ 
	    	
	    		$("#mosaicRadiosFlag").val("0");
	    		$("#candidateFeedDaysOfNoActivity").val("0");
	    		try
	    		{
	    			document.getElementById("ifrm2").src="slideract.do?name=slider2&tickInterval=10&max=${sliderMaxLength }&swidth=200&svalue=0&dslider=0";
    			}
	    		catch(e)
	    		{} 

	    //		alert(" mosaicRadios2 is checked ");
		    }
	    }
	}
</script>


<script type="text/javascript">//<![CDATA[
    var cal = Calendar.setup({
        onSelect: function(cal) { cal.hide() },
        showTime: true
    });
  	 cal.manageFields("contractStartDate", "contractStartDate", "%m-%d-%Y");
     cal.manageFields("contractEndDate", "contractEndDate", "%m-%d-%Y");
    //]]></script>
    
<!-- @Author: Ankit 
 * @Discription: view of edit headquarter Page.
 -->
 
 <script>

function fileContainsVirusDiv(msg)
{
	$('#loadingDiv').hide();
	$('#virusDivId .modal-body').html(msg);
	$('#virusDivId').modal('show');
}
</script>

<div class="modal hide" id="virusDivId" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog-for-cgcoverletter" style='width: 35%;'>
		<div class="modal-content">
			<div class="modal-header">
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body" style="max-height: 400px;overflow-y:auto;">
				<div class="control-group">
				</div>
			</div>
			<div class="modal-footer">
				<span id=""><button class="btn btn-primary" data-dismiss="modal" onclick="showDSPQDiv();">Ok <i class="icon"></i></button></span>&nbsp;&nbsp;
			</div>
		</div>
	</div>
</div>

<div class="modal hide" id="removeMemberConfirmation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false" removeMemberGroupId="" removeMemberId="">
	<div class="modal-dialog-for-cgcoverletter" style='width: 35%;'>
		<div class="modal-content">
			<div class="modal-header">
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body" style="max-height: 400px;overflow-y:auto;">
				<div class="control-group">
					Do you want to remove this user?
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-primary" data-dismiss="modal" onclick="removeMember()">Ok <i class="icon"></i></button>&nbsp;
				<button class="btn" data-dismiss="modal">Cancel</button> &nbsp;&nbsp;
			</div>
		</div>
	</div>
</div>

<input type="hidden" name="hqId" id="hqId" value="${hqId}"/>
<script type="text/javascript">
	//displayKeyContact();
	displayHeadQuarterAdministrator(10);
	displayHeadQuarterAnalyst(12);
	//displayHqBranchDistricts();
	
	var selectedBranches	=	${headQuarterMaster.selectedBranchesUnderContract};
	if(selectedBranches==1)
	{
		$("#addSchoolLink").fadeIn();
		$("#hqBranchesDistrict").fadeIn();
	}
	var selectedDistricts	=	${headQuarterMaster.selectedDistrictsUnderContract};
	if(selectedDistricts==1)
	{
		$("#addSchoolLink").fadeIn();
		$("#hqDistrict").fadeIn();
		displayHqDistricts();
	}
	//var offerVVI= ${headQuarterMaster.offerVirtualVideoInterview};
	//if(offerVVI)
	//{
		//showMarksDiv();
		//showOfferVVI();
	//}
	showReminderDiv();
	
	
</script>