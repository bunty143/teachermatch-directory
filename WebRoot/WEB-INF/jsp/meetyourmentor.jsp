<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src="dwr/util.js"></script>
<script type="text/javascript" src="dwr/interface/DashboardAjax.js?ver=${resouceMap['DashboardAjax.ajax']}"></script>
<link href="css/quest.css?ver=${resourceMap['css/quest.css']}" rel="stylesheet" type="text/css">
<script type="text/javascript" src="dwr/interface/QuestAjax.js?ver=${resourceMap['QuestAjax.ajax']}"></script>
<script type='text/javascript' src="js/quest.js?ver=${resourceMap['js/quest.js']}"></script>

<script type="text/javascript" src="dwr/interface/DSPQServiceAjax.js?ver=${resourceMap['DSPQServiceAjax.ajax']}"></script>
<script type="text/javascript" src="js/jobapplicationflow/jafstart.js?ver=${resouceMap['js/jobapplicationflow/jafstart.js']}"></script>

<c:if test="${teacherDetail.teacherId ne null}">
 <script type='text/javascript'>
	getEPI(${teacherDetail.teacherId}); 
 </script>
 </c:if> 
 <c:if test="${teacherDetail.teacherId eq null}">
 <script type='text/javascript'> 
  $(document).ready(function() {
         $("#hidea").show(); 
         $("#show").hide();
         imghover1();
       }); 
 </script>
  </c:if> 
  <style type="text/css">
  #hidea a
  {
  color:#474747;
  }
  #hidea a:hover
  {
  color:#474747;
  }
   #show a
  {
  color:#474747;
  }
  #show a:hover
  {
  color:#474747;
  }
.a1 {
	position: relative;
	display: inline-block;
}
.a1 > i {
    max-width: 220px;
    z-index: 1030;
	font-style:normal;
	line-height: 1.4;
	color: black;
   	background: white;
	box-shadow: 0 3px 7px rgba(0,0,0,0.3);
	border: 1px solid #D4D6D6;
	text-align:left;
	position: absolute;
	width: 215px;	
	margin-left:-115px;	
	bottom: 100%;
	margin-bottom: 22px;
	visibility:hidden;
	opacity:1;
	-webkit-transition: opacity 0.5s linear;
	-moz-transition: opacity 0.5s linear;
	-ms-transition: opacity 0.5s linear;
	-o-transition: opacity 0.5s linear;
	transition: opacity 0.5s linear;
}

.a1 > i:before, .a1 > i:after {
	content: "";
	position: absolute;
	border-left: 20px solid transparent;
	border-right: 20px solid transparent;
	top: 95%;
	left: 41%;
}
.a1 > i:before {
	border-top: 21px solid rgba(0, 0, 0, 0.2);
	margin-top: 7px;
}
.a1 > i:after{
	border-top: 20px solid #fff;
	margin-top: 5px;
}
.a1:hover > i {
	visibility: visible;
	opacity: 1;
}
</style>
<style type="text/css">
#circulardiv

{
text-align:center;
background-color: #E1F4FB;
height:190px;
width:145px;
margin-top: 30px;
margin-bottom: 30px;
}
#circulardiv span
{
font-size: 14px;
font-weight: bold;
}
#circulardiv img
{
margin-top: 10px;
margin-bottom: 10px;
}
h3
{
font-size: 22px;
color:black;
font-weight: bold;
}
.row ul
{
font-weight:bold;
margin-top:12px;
color:black;
}
li
{

color:#007AB5;
font-size:11.5px;
line-height: 25px;

}
</style>
<style>
.toolTip {
z-index:1001;
    position: absolute;
    display: none;
    width: 280px;
    height: auto;
    background: rgba(0,0,0,0.6);
    border: 0 none;    
    color: white;
    font: 11px;
    padding-left:5px;
    padding-right:5px;
    padding-top:5px;
    padding-bottom:5px;
    text-align: center;    
    margin:auto;
  	border-radius: 5px 5px 5px 5px;
  	box-shadow: -3px 3px 15px #888888;
    border:1px solid;
    border-color: rgba(0, 0, 0, 0.8);  
	
}
 .table td {
 padding:10px 4px;
}
.table-bordered thead:first-child tr:first-child th:first-child, .table-bordered tbody:first-child tr:first-child td:first-child
{
border-top-left-radius:0px;
webkit-border-top-left-radius: 0px;
}
.table th
{
border-top-left-radius:0px;
padding:5px;
}
  .table
  {
  border: 0px;
  }
.bg
{
background-color: #007ab4;
}
</style>
<style type="text/css">
#thin-donut-chart {
    width: 200px; 
    height: inherit;
	position:absolute;
    top:8.2px;
    float:left;
    left:-12%;
    margin:auto;   
}
.center-label, .bottom-label {
    text-anchor: middle;
    alignment-baseline: middle;
    pointer-events: none;
    font-size: 27px;
    font-weight: bold;
    color:white;
   }
.half-donut .center-label {
    alignment-baseline: initial;
}
#search222
{
position:absolute;
display:inline-block;
left:6.2px;
top:3px;
width:inherit;
height:inherit;
}
#title
{
position:absolute;
top:103%;
margin:auto;
text-transform:uppercase;
text-align:center;
font-family: 'Century Gothic', sans-serif;
color:white;
font-size: 10px;
font-weight: bold;
width:inherit;
height:inherit;
padding:2px;
line-height: 1.4;
}
.boldcolor
{
color:#4E4D52;
}
.pclass
{
line-height: 1.2px;
font-size: 9px;
font-weight: bold;
}
.bucket
{
font-family: 'Century Gothic','Open Sans', sans-serif;
font-weight: bold;
color: white;
padding: 0px;
margin-left: 10px;
line-height: 35px;
}
.bucketheader
{
background-image: url(images/bucket_header2.png);
}
.step
{
background: url(images/step-line.png) center 19px no-repeat;
display: block;
max-width: 90%;
height: auto;
margin-top:35px;
}
.step ul li a span {
    color: #000000;
    display: block;
    font-family: tahoma;
    font-size: 11px;;
    font-weight: bold;
    padding: 40px 0 0;
}
.step ul li {
float: left;
margin: 0px 2%;
}
.step ul li:first-child { margin-left:-5px;}
.step ul li:last-child { margin-right:-9px;}
</style>

<center>
<h1 class="font12" style="font-family: Century Gothic; font-size: 20px; color: black;"><b><spring:message code="msgFreeTeacherResources"/></b> </h1><br>
<div class="container">
<div class="row" >
<div class="col-sm-4 col-md-4">
<b  style="color: black;margin-right: 90px; font-size: 12px;" > <spring:message code="lblQuestCot"/></b>
<ul style="text-align:left;"><spring:message code="msgAAEE"/>
<li><a href="http://www.aaee.org/cwt/external/wcpages/resource/publications.aspx" target="_blank"><spring:message code="msgStudentHandbook"/></a></li>
<li><a href="http://www.aaee.org/cwt/External/WCPages/WCEvents/JobsStartPage.aspx?ce=true" target="_blank"><spring:message code="msgJobFairs"/></a></li>
<li><a href="http://www.aaee.org/cwt/external/wcpages/resource/janice_jones_scholarship.aspx" target="_blank"><spring:message code="msgScholarships"/></a></li>
</ul>

<ul style="text-align:left;"><spring:message code="msgNEA"/>
<li><a href="http://www.nea.org/home/Bullyfree-Take-the-Pledge.html" target="_blank"><spring:message code="msgBullyFree"/></a></li>
<li><a href="http://www.nea.org/home/LegislativeActionCenter.html" target="_blank"><spring:message code="msgLegislationAction"/></a></li>
<li><a href="http://www.nea.org/grants/19458.htm" target="_blank"><spring:message code="msgMeetingsandEvents"/></a></li>
</ul>

<ul style="text-align:left;"><spring:message code="msgAASPA"/>
<li><a href="http://aaspa.org/leon-bradley-scholarship/" target="_blank"><spring:message code="msgScholarship"/></a></li>
<li><a href="https://aaspa.org/resources/aaspa-publications/" target="_blank"><spring:message code="msgPublications"/></a></li>
<li><a href="https://aaspa.org/events/" target="_blank"><spring:message code="msgEvents"/></a></li>
</ul>
</div>

<div class="col-sm-4 col-md-4">
<b style="color: black; font-size: 12px;"><spring:message code="lblToolktEssent"/></b>
<ul style="text-align:left;"><spring:message code="msgGraphicOrganizer"/>
<li><a href="http://www.biologycorner.com/resources/graphic_compare_contrast.gif" target="_blank"><spring:message code="msgCompareOrContrast"/></a></li>
<li><a href="http://www.lauracandler.com/CCC/PDF/BehaviorReflections.pdf" target="_blank"><spring:message code="msgBehaviorReflection"/></a></li>
</ul>

<ul style="text-align:left;"><spring:message code="msgBooks"/>
<li><a href="http://www.amazon.com/The-Academic-Search-Handbook-Edition/dp/0812220161" target="_blank"><spring:message code="msgAcademicJobSearch"/></a></li>
<li><a href="http://www.amazon.com/On-Market-Strategies-Successful-Academic/dp/1588265358" target="_blank"><spring:message code="msgOnTheMarketStrategies"/></a></li>
</ul>

<ul style="text-align:left;"><spring:message code="msgVideos"/>
<li><a href="http://www.onlinecollegecourses.com/2011/11/28/20-incredible-ted-talks-you-should-show-your-high-school-students/" target="_blank"><spring:message code="msgTedTalksToShow"/></a></li>

</ul>

</div>

<div class="col-sm-4 col-md-4">
<b style="color: black; font-size: 12px;"><spring:message code="lblInTheQ"/> </b>
<ul style="text-align:left;"><spring:message code="lnkCollegetoCareer"/>
<li><a href="http://www.huffingtonpost.com/2014/05/02/health-after-college_n_5227100.html" target="_blank"><spring:message code="msgDecortingHome_8"/></a></li>
<li><a href="http://www.hercampus.com/style/how-transition-your-wardrobe-college-career" target="_blank"><spring:message code="msgDecortingHome_33"/></a></li>
</ul>

<ul style="text-align:left;"><spring:message code="lnkFinance"/>
<li><a href="http://www.levo.com/articles/career-advice/3-better-ways-to-manage-your-student-loans" target="_blank"><spring:message code="msgDecortingHome_21"/>  </a></li>
<li><a href="http://www.pcmag.com/article2/0,2817,2400562,00.asp" target="_blank"><spring:message code="msgDecortingHome_22"/></a></li>
<li><a href="http://www.levo.com/articles/lifestyle/how-much-your-paycheck-should-you-be-saving" target="_blank"><spring:message code="msgDecortingHome_23"/></a></li>
</ul>

<ul style="text-align:left;"><spring:message code="lnkHousing"/> 
<li><a href="http://www.domainehome.com/small-apartment-decorating-ideas-spring-2014/" target="_blank"><spring:message code="msgDecortingHome_15"/></a></li>
<li><a href="http://www.buzzfeed.com/alannaokun/things-nobody-tells-you-about-your-first-apartment" target="_blank"><spring:message code="msgDecortingHome_16"/></a></li>
</ul>
</div>
</div>
<h1  style="font-family: Century Gothic; color: black; font-size: 20px;"><b><spring:message code="msgPremiumContent"/> </b> </h1>
<div class="col-sm-12 col-md-12">

<spring:message code="msgReachExpertLevel"/><br/> <spring:message code="msgPremiumContentBelow"/> <span style="color:#007AB5; font-weight: bold;"> <c:choose><c:when test='${epistatus eq "comp" or epistatus eq null}'><a href="Quest-EPI.do"><spring:message code="lblTmEPI"/></a></c:when><c:otherwise><a href="javascript:void(0);" onclick="checkInventory(0,null);"><spring:message code="lblTmEPI"/></a></c:otherwise></c:choose> </span><br>
<spring:message code="msgIncreaseYourProfilePower"/>
</div>
<div class="row">
<div class="col-sm-4 col-md-4">
</div>
</div>
<br/>
<div id="show" style="left:70px;float:left;height: 180px;width:100%;margin: auto;position: relative;margin-top:30px; display:none;">
<div align="center"  style="width:100%;">
	<div id="" style="background-color:#E1F4FB;height: 170px;margin-top:23px;width:142px;float:left;left:20px; margin-right: 5px; padding-top: 10px;">
	<b class="boldcolor"><spring:message code="lblQuestCot"/></b><br/>	
	<a href="questconnect.do" target="_blank"><img src="images/questconnectimg.png" style="width: 70%;"/></a><br/>		
			<p class="pclass" style="margin-top: 11px;"><spring:message code="lblProfess"/></p>
			<p class="pclass"><spring:message code="lblPerspectivesAdSupt"/></p>
	</div>
	<!-- Amit Kumar -->
	<div id="sdiv2" style="background-color:#E7EBEA;height: 170px;margin-top:23px;width:142px;float:left; margin-right: 5px; padding-top: 10px;">	
		<a href="javascript: void(0)" class="a1">
	<div style="width: 100%; height: 15%;">
			<b class="boldcolor"><spring:message code="lblQuestAcad"/></b>			
		</div>	
		<img src="images/questacad.png" style="width: 68%"/><br/><br/>		
		<p class="pclass" style="margin-top: -4px;"><spring:message code="lblRefAd"/></p>
		<p class="pclass"><spring:message code="lblLearnCentr"/></p>
		<i ><div style="background-color: #007AB3; font-size: 10px; font-weight:bold; padding:5px 5px 5px 5px; color:white; text-align: center;"><img src="images/lockStatus.png" width="7%"/>&nbsp;&nbsp;<spring:message code="lblComingSoon"/></div>
			 <div style="padding:3px 0px 0px 10px; font-size: 9px; text-align: left;" >
			<spring:message code="msgCountOnThisResourceLibrary"/> </div>
			</i></a>
	</div>
		
	<div id="" style="background-color:#E1F4FB;height: 200px;width:193px;margin-top:10px;float:left;margin-right: 5px; padding-top: 10px;">
	<b class="boldcolor"><spring:message code="lblMetYuMentor"/> </b>	<br/><br/>
	<a target="_blank" href="meetyourmentortab.do">
		 <img src="images/mentorLogo.png" style="width: 50%"/></a><br/><br/>		 
		<p class="pclass"><spring:message code="lblInsightfulGoTo"/></p>
		<p class="pclass"><spring:message code="lblIntrAd"/></p>
	</div>
	

	<div id="" style="background-color:#E7EBEA;height: 170px;margin-top:22px;width:143px;float:left;margin-right: 5px; padding-top: 10px;">
		<b class="boldcolor"><spring:message code="lblToolktEssent"/></b>
		<a href="toolkitessentials.do" target="_blank"><br/>
		<img src="images/ToolkitEssentials.png" style="width: 80%"/></a><BR/>	
		<p class="pclass"><spring:message code="lblSmtTp"/></p>
		<p class="pclass"><spring:message code="lblToolsAdAids"/></p>
	</div>
	

	<div id="" style="background-color:#E1F4FB;height: 170px;margin-top:21px;width:140px;float:left; padding-top: 10px;">
	<b class="boldcolor"><spring:message code="lblInTheQ"/></b>
  	<a href="intheq.do" target="_blank"><br/>
		<img src="images/intheq.png" style="width: 80%"/></a><br/>	
		<p class="pclass" ><spring:message code="lblLifeStl"/></p>
		<p class="pclass"><spring:message code="lblPerspect"/></p>
	</div>
	
</div>
</div>
<!-- Amit Kumar -->
<div id="hidea"  style="left:70px; float:left; height: 180px; width:100%; margin: auto;position: relative;margin-top:30px; display:none;">
<div align="center"  style="width:100%;">
	
	<div id="hdiv1" style="background-color:#E1F4FB; height: 170px;margin-top:23px;width:142px;float:left;left:20px; margin-right: 5px;">
		<br/>
		<a href="javascript: void(0)" class="a1">
		<div id="divv" style="width: 100%; height: 15%;">
			<b  class="boldcolor"><spring:message code="lblQuestCot"/></b>
		</div>
		<img id="img" src="images/questconnectimg.png" style="width: 68%;"/>		
		<p class="pclass" style="margin-top: -4px;"><spring:message code="lblProfess"/></p>
		<p class="pclass"><spring:message code="lblPerspectivesAdSupt"/></p>
		<i ><div style="background-color: #007AB3; font-size: 10px; font-weight:bold; padding:5px 5px 5px 5px; color:white;"><img src="images/lockStatus.png" width="7%"/>&nbsp;&nbsp; <spring:message code="msgToUnlockReachExport"/></div>
			 <div style="padding:3px 5px 10px 10px; font-size: 9px; text-align: left;" >
			 <spring:message code="msgFindValuableInterviewingAndHiring"/></div>
			</i></a>		
	</div>

	<div id="hdiv2" style="background-color:#E7EBEA;height: 170px;margin-top:24px;width:142px;float:left; margin-right: 5px;">	
	<br/>
	<a href="javascript: void(0)" class="a1">
	<div style="width: 100%; height: 15%;">
			<b class="boldcolor"><spring:message code="lblQuestAcad"/></b>			
		</div>	
		<img src="images/questacad.png" style="width: 68%"/>
		
		<p class="pclass" style="margin-top: -4px;"><spring:message code="lblRefAd"/></p>
		<p class="pclass"><spring:message code="lblLearnCentr"/></p>
		<i ><div style="background-color: #007AB3; font-size: 10px; font-weight:bold; padding:5px 5px 5px 5px; color:white; text-align: center;"><img src="images/lockStatus.png" width="7%"/>&nbsp;&nbsp;<spring:message code="lblComingSoon"/></div>
			 <div style="padding:3px 5px 10px 10px; font-size: 9px; text-align: left;" >
			<spring:message code="msgCountOnThisResourceLibrary"/></div>
			</i></a>	
	</div>
	
	<div id="hdiv3" style="background-color:#E1F4FB;height: 220px;width:193px;margin-top:-10px;float:left; padding-top:10px; margin-right: 5px;">
	<a href="javascript: void(0)" class="a1">
	 <div style="width: 100%; height: 15%;">
		 	<b class="boldcolor"><spring:message code="lblMetYuMentor"/></b>		<br/> <br/>
		 </div>	
		 <img src="images/mentorLogo.png" style="width: 85%"/><br/> <br/>
		
		<p class="pclass" style="margin-top: -3px;"><spring:message code="lblInsightfulGoTo"/> </p>
		<p class="pclass"><spring:message code="lblIntrAd"/></p>
		<i ><div style="background-color: #007AB3; font-size: 10px; font-weight:bold; padding:5px 5px 5px 5px; color:white;"><img src="images/lockStatus.png" width="7%"/>&nbsp;&nbsp; <spring:message code="msgToUnlockReachExport"/></div>
			 <div style="padding:3px 5px 10px 10px; font-size: 9px; text-align: left;" >
				<spring:message code="msgGetRealWorldInsightAndAdvice"/></div>
			</i></a>		
	</div>
	
	<div id="hdiv4" style="background-color:#E7EBEA;height: 175px;margin-top:22px;width:143px;float:left;margin-right: 5px;">
		<br/>
		<a href="javascript: void(0)" class="a1">
		<div style="width: 100%; height: 20%;">
			<b class="boldcolor"><spring:message code="lblToolktEssent"/></b>			
		</div>
		<img src="images/ToolkitEssentials.png" style="width: 68%"/>		
		<p class="pclass" style="margin-top: -6px"><spring:message code="lblSmtTp"/></p>
		<p class="pclass"><spring:message code="lblToolsAdAids"/></p>
		<i ><div style="background-color: #007AB3; font-size: 10px; font-weight:bold; padding:5px 5px 5px 5px; color:white;"><img src="images/lockStatus.png" width="7%"/> &nbsp;&nbsp;<spring:message code="msgToUnlockReachExport"/></div>
			 <div style="padding:3px 5px 10px 10px; font-size: 9px; text-align: left;" >
				<spring:message code="msgDiscoverAnArsenalOfTools"/> </div>
			</i></a>	
	</div>
	
	<div id="hdiv5" style="background-color:#E1F4FB;height: 175px;margin-top:22px;width:140px;float:left;"> 
	<br/> 
	<a href="javascript: void(0)" class="a1">
	<div style="width: 100%; height: 20%;">
			<b class="boldcolor"><spring:message code="lblInTheQ"/></b>			
		</div>
		<img src="images/intheq.png" style="width: 68%"/>		
		<p class="pclass" style="margin-top: -4px;"><spring:message code="lblLifeStl"/></p>
		<p class="pclass"><spring:message code="lblPerspect"/> </p>
		<i ><div style="background-color: #007AB3; font-size: 10px; font-weight:bold; padding:5px 5px 5px 5px; color:white;"><img src="images/lockStatus.png" width="7%"/> &nbsp;&nbsp;<spring:message code="msgToUnlockReachExport"/> </div>
			 <div style="padding:3px 5px 10px 10px; font-size: 9px; text-align: left;" >
				<spring:message code="msgInformedDecisionsAcrossEveryAspect"/> </div>
			</i></a>	
	</div>
</div>
</div>



</center>
<script type="text/javascript" src="dwr/interface/AssessmentCampaignAjax.js?ver=${resouceMap['AssessmentCampaignAjax.ajax']}"></script>
<script type='text/javascript' src="js/assessment-campaign.js?ver=${resouceMap['js/assessment-campaign.js']}"></script>
<jsp:include page="../tiles/feedbackandsupport3AfterLogin.jsp" />