<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<link href="css/quest.css?ver=${resourceMap['css/quest.css']}" rel="stylesheet" type="text/css">
<script type="text/javascript">

var ids=window.location.hash;
if(ids=="#review")
{
$(function () {    
   		 $("#whatsnew").hide();    	
		 $("#review").show();		
  	})  
  }
  if(ids=="#whatsnew")		
  {
	  $(function () {	   
	   		 $("#whatsnew").show()();    	
			 $("#review").hide();
			 
	  })
  }

</script>
<style type="text/css">

.row b
{
color:black;
}
</style>
<div style="display: none;" id="review">
<div class="container">

<div  style="color:#22749C;margin-top: 13px; ">
<b>
<a href="meetyourmentortab.do#review">
<spring:message code="msgBackMentorReview" /></a>

</b>
</div>
<div  style="margin-top: 15px; ">
<spring:message code="msgPublishedJanuary" />
</div>

<div class="font12" style="font-size: 18px;color:black;margin-top: 10px; ">
<b><spring:message code="msgYouAreHiredTeacher" /></b>

</div>
<br>
<div></div>
<br>


<div  style=" width: 100%;">
<div class="col-sm-4 col-md-4" style="padding-left: 0px; ">
<img src="images/hiredteacher.png" style="width: 100%; " />

</div>

<div class="col-sm-8 col-md-8" style="padding-left: 0px; ">

<p>
<i>
<spring:message code="msgInsiderGuideToShow" /></i></p>
<p style="margin-top: 10px;"><b><spring:message code="msgYouAreHiredTeacher" /> </b>
<spring:message code="msgNailYourInterview" /> 
<spring:message code="msgPopularReadforAnyOne" />
</p>

<p><spring:message code="msgRobynView1" /></p>

<p><spring:message code="msgRobynView2" /></p>
<p><spring:message code="msgRobynView3" /></p>

<p><spring:message code="msgRobynView4" />
</p>

<p><spring:message code="msgRobynView5" /></p>

<p><spring:message code="msgRobynView6" /></p>

<p><spring:message code="msgRobynView7" /></p>

<p><spring:message code="msgRobynView8" /></p>
<p><spring:message code="msgRobynView9" /></p>
</div>

</div>

</div>


<hr style=" border-top:2px solid #9F9F9F; margin-top: 3px;">
<div  style="color:#22749C; ">
<b><a href="meetyourmentortab.do#review">
<spring:message code="msgBackMentorReview" /></a>

</b>
</div>
<div  style="color:#22749C;margin-top: 20px; ">
<!--<b>
<a href="#">
All Reviews
</a>
</b>
--></div>
</div>




<div id="whatsnew">
<div class="container">

<div  style="color:#22749C;margin-top: 13px; ">
<b>
<a href="meetyourmentortab.do#whatsnew">
<spring:message code="msgRobynView10" /></a>

</b>
</div>
<div  style="margin-top: 15px; ">

<b style="color: black; font-size: 14px;"><spring:message code="msgGraduatingMidYear" /></b>

</div>

<div class="font12" style="color:black;margin-top: 10px; ">
<spring:message code="msgJanuary" />

</div>
<br>


<div  style=" width: 100%;">
<div class="col-sm-4 col-md-4" style="float: right;" >
<img src="images/whatsnewimg1.png" style="width: 100%;margin-left:-5%; height: 200px; " />

</div>

<div  style="margin-left: -15px;" class="col-sm-12 col-md-12">

<p>
<spring:message code="msgLandingATeachingJob" /></p>
<b style="font-size: 14px;"><spring:message code="msgRobynView11" /></b>
<p style="margin-top: 10px;"><spring:message code="msgRobynView12" /></p>

<p style="margin-top: 10px;"><b><spring:message code="msgRobynView12" /></b>

<spring:message code="msgRobynView14" />
</p>

<p style="margin-top: 10px;"><spring:message code="msgRobynView15" /></p>

<p style="margin-top: 10px;"><spring:message code="msgRobynView16" /></p>

<b style="font-size: 14px;"><spring:message code="msgRobynView17" /></b>

<p style="margin-top: 10px;"><spring:message code="msgRobynView18" />

</p>

<p style="margin-top: 10px;"><spring:message code="msgRobynView19" />

</p>

<p style="margin-top: 10px;"><spring:message code="msgRobynView20" />

</p>

<p style="margin-top: 10px;"><spring:message code="msgRobynView21" />

</p>
<p style="margin-top: 10px;"><spring:message code="msgRobynView22" />

</p>

<p style="margin-top: 10px;"><spring:message code="msgRobynView23" />

</p>
<p>
<spring:message code="msgRobynView24" />
</p>

<p><spring:message code="msgRobynView25" /> http://www.aasa.org/content.aspx?id=13418</p>

</div>

</div>

</div>


<hr style=" border-top:2px solid #9F9F9F; margin-top: 3px;">
<div  style="color:#22749C; ">
<b><a href="meetyourmentortab.do#whatsnew"><spring:message code="msgRobynView10" /></a>

</b>
</div>
<div  style="color:#22749C;margin-top: 20px; ">
<b>
<!--<a href="#">
All Articles
</a>
--></b>
</div>



</div>

