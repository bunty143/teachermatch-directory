<script type="text/javascript">
function getConfirm()
{
	//alert("d");
	$('#warningImg1').html("<img src='images/info.png' align='top'>");
	$('#message2showConfirm1').html("You are about to start the Base Inventory. Each item in Base has a stipulated time limit and each item should be responded to within it's stipulated time limit. Once you start the Base, you must complete responding to all the items as you will not be able to save your responses and return at a later time. If you continue, please observe the following guidlines:");
	var nextMsg = "<ul>  <li>Please ensure that you have at least 90 minutes to complete the Base Inventory </li> "+
	 "<li>Please ensure that you have a stable and reliable Internet connection</li> "+ 
	 "<li>You should answer all the items  in Base Inventory without any interruptions</li> "+ 
	 "<li>You should not close the Browser while you are taking the Base Inventory</li> "+ 
	 "<li>You should not hit the \"back\" button on your Browesr while you are taking the Base Inventory</li> </ul>";
	$('#nextMsg').html(nextMsg+"Click \"Ok\" if you are ready to start the Base Inventory, otherwise click \"Cancel\" to exit.");
	$('#footerbtn1').html("<button class='btn btn-primary' data-dismiss='modal' aria-hidden='true'>Ok</button> <button class='btn' data-dismiss='modal' aria-hidden='true'>Cancel</button>");
	$('#myModalv').modal('show');
	$("#myModalv").css({ top: '45%' });
	$('#vcloseBtn').html("<span>x</span>");
}
function getPopUp1()
{
	//alert("d");top: 60%;
	$("#myModalv").css({ top: '60%' });
	$('#warningImg1').html("<img src='images/warn.png' align='top'>");
	$('#message2showConfirm1').html("You just violated the stipulated time limit for an item and this is your FIRST warning. You must respond to each item within its stipulated time limit as failure to respond may result in disqualification. ");
	$('#nextMsg').html("Click \"Ok\"  if you are ready to start your <font color='red'>SECOND</font> attempt to complete the Base. <br><br>If you don't take any action, you will be taken back to the Dashboard and you can re-start the Base by selecting \"Complete Inventory\" action.  ");
	$('#footerbtn1').html("<button class='btn btn-primary' data-dismiss='modal' aria-hidden='true'>Ok</button>");
	$('#vcloseBtn').html("<span>x</span>");
	$('#myModalv').modal('show');
}
function getPopUp2()
{
	//alert("d");
	$("#myModalv").css({ top: '60%' });
	$('#warningImg1').html("<img src='images/warn.png' align='top'>");
	$('#message2showConfirm1').html("You just violated the stipulated time limit again for an item and this is your SECOND warning. You must respond to each item within its stipulated time limit as failure to respond may result in disqualification. ");
	$('#nextMsg').html("Click \"Ok\"  if you are ready to start your <font color='red'>FINAL</font> attempt to complete the Base. <br><br>If you don't take any action, you will be taken back to the Dashboard and you can re-start the Base by selecting \"Complete Inventory\" action.  ");
	$('#footerbtn1').html("<button class='btn btn-primary' data-dismiss='modal' aria-hidden='true'>Ok</button>");
	$('#vcloseBtn').html("<span>x</span>");
	$('#myModalv').modal('show');
}
function getPopUp3()
{
	//alert("d");
	$("#myModalv").css({ top: '60%' });
	$('#warningImg1').html("<img src='images/stop.png' align='top'>");
	$('#message2showConfirm1').html("You just violated the stipulated time limit again for an item during your third and final attempt. You will no longer be able to continue with the Base and your Base status will be marked \"Violation\".");
	$('#nextMsg').html("You will still be able to apply for jobs, however, your Base status will be reported as \"Violation\".<br><br>If you have any further questions regarding this matter, please call us at (888) 312 7231 or email us at clientservices@teachermatch.net.");
	$('#footerbtn1').html("<button class='btn btn-primary' data-dismiss='modal' aria-hidden='true'>Ok</button>");
	$('#vcloseBtn').html("<span>x</span>");
	$('#myModalv').modal('show');
}
//$('#myModal').removeData("modal").modal({backdrop: 'static', keyboard: false}) 

</script>
<br/><br/>
<input type="button" value="Confirmbox" onclick="getConfirm();"/>
<br/><br/>
<input type="button" value="Div 1" onclick="getPopUp1();"/>
<br/><br/>
<input type="button" value="Div 2" onclick="getPopUp2();"/>
<br/><br/>
<input type="button" value="Div 3" onclick="getPopUp3();"/>
<br/><br/>
<input type="button" value="Other Div" onclick="showDiv();"/>
<br/>

<link rel="stylesheet" href="css/dialogbox.css" />
<script  type="text/javascript" language="javascript" src="js/dialogbox.js"></script>

<div id="tm-root"></div>

<script type="text/javascript">

  function addDialogContent(divId,divTitle,divContent)
  {
  	  $('tm-root').html("");
  	  var a = document.createElement('div');
	  a.id=divId;
	  a.title=divTitle;
	  $(a).html(divContent);
	  document.getElementById('tm-root').appendChild(a);
  }
  
  function showDialog(divId)
  {
	  	$(function() {
	    $("#"+divId ).dialog({
	      modal: true,
	      buttons: {
	        Ok: function() {
	          $( this ).dialog( "close" );
	        }
	      }
	    });
	  });
  }
  function showDiv()
  {
	//var divMsg="<img src='images/info.png' align='top'> ";
	var divMsg="";
	divMsg=divMsg+"You are about to start the Base Inventory. Each item in Base has a stipulated time limit and each item should be responded to within it's stipulated time limit. Once you start the Base, you must complete responding to all the items as you will not be able to save your responses and return at a later time. Please continue only if: ";
	divMsg=divMsg+"<ul>  <li>You have at least one hour to complete the Base</li>  <li>You have a stable Internet connection</li>  <li>You are prepared to answer all the items without any interruptions </li> </ul> Click \"Ok\"  if you are ready to start the Base, otherwise click \"Cancel\" to exit. ";
	addDialogContent("dm","TeacherMatch",divMsg,"tm-root");
	showDialog("dm",null,"950","210");
  }
 /* $("#myModalv").draggable({
    handle: ".modal-header"
}); */
  //showDiv();
</script>
