<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="tm.bean.user.UserMaster"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<script type='text/javascript' src="js/quest.js?ver=${resourceMap['js/quest.js']}"></script>
<script type="text/javascript" src="dwr/interface/QuestAjax.js?ver=${resourceMap['QuestAjax.ajax']}"></script>
<style type="text/css">
#thin-donut-chart {
    width: 205px;
    height: inherit;
	position:absolute;
    top:180px;
    margin:auto;   
}
.center-label, .bottom-label {
    text-anchor: middle;
    alignment-baseline: middle;
    pointer-events: none;
    font-size: 27px;
    font-weight: bold;
    color:white;
   }
.half-donut .center-label {
    alignment-baseline: initial;
}
#search222
{
position:absolute;
display:inline-block;
left:1.5%;
top:-25.5%;
width:inherit;
height:inherit;
}
#title
{
position:absolute;
top:125%;
margin:auto;
text-transform:uppercase;
text-align:center;
font-family: 'Muli', sans-serif;
color:white;
font-size: 12px;
font-weight: bold;
width:inherit;
height:inherit;
padding:2px;
}
</style>
<center><div id="thin-donut-chart">
	<div id="search222"><img src="images/search_edit.png"/></div>	
	<div style="display:none;" id="loadingDiv">
    <table  align="center">
 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
	</table>
	</div>
	<div id="title"></div>
	</div></center>
    <br><br>
    
<script type="text/javascript" src="js/d3.js?ver=${resourceMap['js/d3.js']}"></script>
 <script>
 getPowerTracker(${teacherDetail.teacherId});
 </script>
