<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<script  type="text/javascript" language="javascript" src="dwr/interface/DistrictAssessmentCampaignAjax.js"></script>
<script  type="text/javascript" language="javascript" src="dwr/engine.js"></script>
<script  type="text/javascript" language="javascript" src='dwr/util.js'></script>
<script  type="text/javascript" language="javascript" src='js/districtAssessment-campaign.js'></script>
<script  type="text/javascript" language="javascript" src='js/tmcommon.js'></script>


<input type="hidden" id="districtAssessmentId" value="${districtAssessmentDetail.districtAssessmentId}">
<input type="hidden" id="teacherDistrictAssessmentId" value="${teacherDistrictAssessmentDetail.teacherDistrictAssessmentId}">
<input type="hidden" id="jobId" value="${jobOrder.jobId}">
<input type="hidden" id="attemptId" value="${attemptId}">
<input type="hidden" id="totalAttempted" value="${totalAttempted}">
<input type="hidden" id="newJobId" value="${newJobId}">
<input type="hidden" id="rURL" value="${rURL}"/>
<script type="text/javascript">
teacherDetail={teacherId:${teacherDetail.teacherId}};
</script>
<style>
.table th, .table td {
border-top: 0px solid #cccccc;
font-weight: normal;
line-height: 20px;
padding: 7px 4px;
text-align: left;
vertical-align: top;
font-family: 'Century Gothic','Open Sans', sans-serif;
font-size: 11px;
}
p {
margin: 0 0 10px;
}
</style>
<!-- dis -->
<script language=JavaScript>

//Disable right click script III- By Renigade (renigade@mediaone.net)
//For full source code, visit http://www.dynamicdrive.com

///////////////////////////////////
/*
var message="";
function clickIE() {if (document.all) {(message);return false;}}
function clickNS(e) {if 
(document.layers||(document.getElementById&&!document.all)) {
if (e.which==2||e.which==3) {(message);return false;}}}
if (document.layers) 
{document.captureEvents(Event.MOUSEDOWN);document.onmousedown=clickNS;}
else{document.onmouseup=clickNS;document.oncontextmenu=clickIE;}

document.oncontextmenu=new Function("return false")
*/
</script>
<!-- dis -->
<div id="tm-root"></div>
<script type="text/javascript">
//alert(document.images);
gg = function(e) {  

var keycode =(window.event) ? event.keyCode : e.keyCode; 
//alert(e.keyCode);
 /*var keycode ;
 if (window.event)
            keycode = window.event.keyCode;
        else if (e)
            keycode = e.which;*/
            
 //alert(keycode);
   switch (keycode) {  
        case 116 : //F5 button
            e.returnValue = false;
            e.keyCode = 0;
            return false; 
        case 82 : //R button
            if (e.ctrlKey) { 
                e.returnValue = false; 
                //event.keyCode = 0;
                e.keyCode = 0;  
                return false; 
            } 
    }
}
document.onkeydown = gg;

function ltrim(stringToTrim) {
return stringToTrim.replace(/^\s+/,"");
}

var epiStandalone = false;
</script>
<c:if test="${sessionScope.epiStandalone}">
<script language="javascript">
epiStandalone = true;
</script>
</c:if>
 
<script language="JavaScript">
var SealoffSource = true; /* to disable the right mouse button, so the source can not been seen. */
//function click() {if (event.button==2) {alert("Right click action is not possible")}}
//document.onmousedown=click;
</script>

<script type="text/javascript" language="javascript">


 jobOrder={jobId:dwr.util.getValue("jobId")};
 teacherDistrictAssessmentDetail={teacherAssessmentId:dwr.util.getValue("teacherDistrictAssessmentId")};
 
  var ff=function() {
  // window.history.pushState('logout', 'logout', 'logout.do');
 
    return "Your work will be lost!"; };
  
  //window.onbeforeunload = ff;
  //window.location.hash = "data=blah$stuff=bleh";
  /*var isSubmitted=0;
  function onBeforeUnload()
    {
    //alert(window.event);
     //setTimeout('window.close()',500);
    if(isSubmitted==0)
    {
   		//setDataToLMS();
   if(window.event )
   {
    	window.event.returnValue = "You should always use the 'Exit' button provided to exit from the course!"; 
    }else
    {
    	return "You should always use the 'Exit' button provided to exit from the course!"; 
    }
 
    }
    }*/
    
    //window.onbeforeunload = onBeforeUnload;
    //$(window).bind('beforeunload',onBeforeUnload);

 $(window).bind('beforeunload',function(){
   
    return 'Warning: Guidelines state you must complete the Assessment in one sitting. Please do not close your browser or do not go back.';
});

/*$(window).bind('beforeunload', function(eventObject) {
    var returnValue = undefined;
   
    eventObject.returnValue = returnValue;
    return returnValue;
});*/


//////////////////////////////////////


 
//////////////////////////////////

var totalSkippedQuestions=${totalSkippedQuestions};
var attep=${totalAttempted};
var totalStrikes=${totalStrikes};
 //alert(totalStrikes);


 //alert("SKIPED : "+totalSkippedQuestions);
 //alert("attempted: "+attep);
 //alert("totalStrikes: "+totalStrikes);
 
 //skip checker
 //skippedCheck();
 //strikeCheck(0,2);


</script>
 
<!--<p class="clock">
	<span class="min">7</span> mins <span class="sec">30</span> secs
</p>
 
-->

<script type="text/javascript" language="javascript"><!--

String.prototype.toHHMMSS = function () {
    sec_numb    = parseInt(this);
    var hours   = Math.floor(sec_numb / 3600);
    var minutes = Math.floor((sec_numb - (hours * 3600)) / 60);
    var seconds = sec_numb - (hours * 3600) - (minutes * 60);

   /* if (hours   < 10) {hours   = "0"+hours;}
    if (minutes < 10) {minutes = "0"+minutes;}
    if (seconds < 10) {seconds = "0"+seconds;}*/
    var time = "";
    var hrs="";
    var mins="";
    
   if(hours>1)
   	hrs="s";
   if(minutes>1)
    mins="s";
    
    if(hours==0)
    	time    = minutes+' min'+mins+' '+seconds+" secs.";
    if(hours==0 && minutes==0)
    	time    = seconds+" secs.";
    else if(hours>0 && minutes==0)
        time    = hours+' hour'+hrs+' '+seconds+" secs.";
    else if(hours>0)
        time    = hours+' hour'+hrs+' '+minutes+' min'+mins+' '+seconds+" secs.";
        
    return time;
}
//alert("5678".toHHMMSS());


var count =${remainingSessionTime}  
//var count =3678;  
var redirect="logout.do"  
  
function countDown(){  
 if (count <=0){  
  
  window.onbeforeunload = function() {};
  //alert('Your session time is up!');
  	
  	//$("#myModalvk").css({ top: '60%' });
	/*
	$('#warningImg1k').html("<img src='images/stop.png' align='top'>");
	$('#message2showConfirm1k').html("Your session time is up!");
	$('#nextMsgk').html("");
	$('#footerbtn1k').html("<button class='btn btn-primary' data-dismiss='modal' aria-hidden='true'  >Ok</button>");
	$('#vcloseBtnk').html("<span>x</span>");
	$('#myModalvk').modal('show');
	*/
			
  setTimeout("finalizeAnswers(false)", 4000);
  //finishAssessment(1);  
  //window.location.href='userdashboard.do'
 }else{  
  count--;  
  document.getElementById("totalTime").innerHTML = count.toString().toHHMMSS();  
  setTimeout("countDown()", 1000);
  if(count==600)
  {
  	showReminder();
  }
 }  
}  

var intervalID;

function tmTimer(totalSec,yes)
{
//alert(dde);
//alert(totalSec);
//totalSec=10;
$('#questionTimer').show();
var mm=0;
var ss=0;
if(totalSec>60)
{
	mm=parseInt(totalSec/60);
	ss=totalSec%60;
	
}else
	ss=totalSec;


$(document).ready (function () {
    $('.clock').each(function () {
       var clock = $(this);
       
       //if(yes==1)
       clock.html('<span class="min">0</span> min <span class="sec">0</span> secs.');
       
       $('.min').html(mm);
       $('.sec').html(ss);
       
       
      if(totalSec<60)
       clock.html('<span class="sec">'+ss+'</span> secs.');
       
        intervalID=setInterval(function () {
            var m = $('.min', clock),
                s = $('.sec', clock);
                
            //alert(m.html()+" "+s.html()); 
            //if (m.length == 0 && parseInt(s.html()) <= 0)
            if ((m.length == 0 || parseInt(m.html())==0) && parseInt(s.html()) <= 0)  
                {
                	clock.html('0 secs.');
                	//clock.html('<span class="sec">0</span> secs.');
                	
                	getDistrictAssessmentSectionCampaignQuestionsGrid(0);
                }
             
            if (parseInt(s.html()) <= 0 && parseInt(m.html())>0) {
                m.html(parseInt(m.html() - 1));
                s.html(60);
            }
            
            //if(totalSec>60)
            if (parseInt(m.html()) <= 0) 
                clock.html('<span class="sec">59</span> secs.');
         
        	s.html(parseInt(s.html() -1));
         
        }, 1000);
    });
});
}

//tmTimer(120);
var timeout;
</script>  

<div class="row">
	<div class="col-sm-12 col-md-12">
		<div class='divErrorMsg' id='errordiv' style="display: block;">				
		</div>
	</div>
</div>		
<div class="row" style="margin-left: 0px;margin-right: 0px;margin-top:">
         <div style="float: left;">
         	<img src="images/affidavit.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
            <c:set var="assType" value="Teacher - Base Inventory" />
			<c:set var="assType" value="${teacherDistrictAssessmentDetail.districtAssessmentName}" />
			
         	<div class="subheading" style="font-size: 13px;">         	
         	${assType}          	
         	</div>	
         </div>
		
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
</div>

<div class="row" id="mainDiv">
	<div class="col-sm-12 col-md-12">
		
		<br/>
		
		<div>
		<div><span id="sectionName" style="display: block;"></span></div>
		<div id="sessionTimer" style="display: none;text-align: right;"><b><spring:message code="headRemainingTimeForSession"/> <span id="totalTime" style="color: #007AB4;" ></span></b></div>
		</div>
		<!--  <div id="questionDiv" style="border: 2px solid #007AB4;padding-top: 20px;padding-left: 15px;padding-right: 15px;padding-bottom: 20px;" > -->
		 <div id="questionDiv" style="border: 2px solid #007AB4;padding-top: 20px;padding-left: 15px;padding-right: 15px;padding-bottom: 20px;" >
			<!--<table width="90%" border="0" class="table table-bordered table-striped" id="tblGrid">
			<tr><td><br/><br/><br/><br/><div style='display:block;text-align:center;padding-top:4px;'><img src="images/Logo with Beta300.png" alt=""><br/><br/></div>
			<br/><br/>
			</td></tr>
			</table>
		
		-->
		<div  id="tblGrid">
		<table width="90%" border="0" class="table table-bordered table-striped" >
			<tr><td><br/><br/><br/><br/><div style='display:block;text-align:center;padding-top:4px;'><img src="images/Logo with Beta300.png" alt=""><br/><br/></div>
			<br/><br/>
			</td></tr>
			</table>
		</div>
		</div>              
	   <br>
	    <div class="span10 m0">
			<button type="submit" id='sbmtbtn' class="btn btn-large btn-primary" onclick="finalizeAnswers(true);"><strong><spring:message code="btnFinalize"/> <i class="icon"></i></strong></button>
	    </div>
	</div>
	   
	</div>
	<br><br><br><br>

<div style="display:none;" id="loadingDivInventory">
	 <table  align="center">
 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
 		<tr id='paymentMessage'><td style='padding-top:0px;' id='spnMpro' align='center'><spring:message code="msgAssIsBeingLoaded"/></td></tr>
	</table>
</div>

<script type="text/javascript" language="javascript">
//$("#sbmtbtn").attr("disabled", "disabled");
//alert("dd");
//alert(${isCompleted});
//alert(" chances "+ (parseInt(attep)+totalStrikes));
//if((parseInt(attep)+totalStrikes)>3 || ${isCompleted}==true)
if((parseInt(attep)+totalStrikes)>3)
{
	//alert("attep: "+attep);
	//alert("totalStrikes : "+totalStrikes);
	finalizeAnswers(false);
}
//getAssessmentSectionCampaignQuestionsGrid(0);
checkDistrictAssessmentDone(${teacherDistrictAssessmentDetail.teacherDistrictAssessmentId});
//alert(${teacherAssessmentdetail.assessmentSessionTime});

</script>
<c:if test="${teacherDistrictAssessmentDetail.assessmentSessionTime>=0}">
	<script type="text/javascript" language="javascript">
	countDown();  
	$("#sessionTimer").show();
	
	(function(){
    var timer1;
    function transition(){
        clearTimeout(timer1);
     	updateAttempt();
        timer1 = setTimeout(transition, 30000);
    }
    transition();
})();
	</script>
</c:if>
<input type="hidden" value="${collapseIds}" id="collapseIdsText"/>

<!-- change by Amit 
	 date: 18-Mar-15 -->
	<div class="modal" style="display: none;" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog" style="width: 50%;">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
					<h3 id="myModalLabel"><spring:message code="headTm"/></h3>
				</div>
				<div class="modal-body" style="padding-top:0;padding-bottom:0;">
					<div style="text-align: left;">
						<img src='images/info.png'><span style="margin-left: 10px;"><b><spring:message code="msgAllQuesMandatory"/></b></span>
						<div style="text-align:right;margin-top: -50px;margin-bottom: 15px;">
							<div><spring:message code="lblTotalQues"/> <span style="color:#007ab4" id="totalquestion"></span></div>
							<div class="hide" id="answeredDiv"><spring:message code="lblTotalAnsweredQus"/> <span style="color:#007ab4" id="answered"></span></div>
							<div><spring:message code="lblTotalUnAnsweredQuse"/> <span style="color:#007ab4" id="unanswered"></span></div>
						</div>
					</div>
					<div id="myModalInner"></div> 
				</div>
				<div class="modal-footer">
        			<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnOk"/></button>
				</div>
			</div>
		</div>
	</div>

<!-- change by Amit 
	 date: 20-Mar-15 -->
	 
	<div id="timeMsg" style="display: none;"><div style="font-size: 20px;color: red;"><spring:message code="msgYuHaveOnly10MinRemaining"/></div></div>
	
	<script type="text/javascript">
		jQuery.fn.center = function () {
		    this.css("position","fixed");
		    this.css("top", Math.max(0, (($(window).height() - $(this).outerHeight()) / 2) + $(window).scrollTop()) + "px");
		    this.css("left", Math.max(0, (($(window).width() - $(this).outerWidth()) / 2) + $(window).scrollLeft()) + "px");
    		return this;
		}
		
		function showReminder() {
			document.getElementById('timeMsg').style.display = 'block';
			setTimeout("disappearReminder()", 5000);
		}
		
		function disappearReminder() {
			document.getElementById('timeMsg').style.display = 'none';
		}
	</script>
	<script type="text/javascript">
		$("#timeMsg").center();
	</script>
	
	<script type="text/javascript">
		var width = $(window).width();
		var heigth = $(window).height();
		var myWindow = $('#timeMsg');
		    myWindow .css({
			    top : Math.max(0, (($(window).height() - $(this).outerHeight()) / 2) + $(window).scrollTop()) + "px";
			    left: Math.max(0, (($(window).width() - $(this).outerWidth()) / 2) + $(window).scrollLeft()) + "px";
		    });
	</script>
