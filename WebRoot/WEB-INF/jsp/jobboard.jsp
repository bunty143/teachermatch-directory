<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<script type="text/javascript" src="dwr/interface/JobBoardAjax.js?ver=${resourceMap['JobBoardAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/interface/ManageJobOrdersAjax.js?ver=${resourceMap['ManageJobOrdersAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/interface/JBServiceAjax.js?ver=${resourceMap['JBServiceAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/interface/QuestAjax.js?ver=${resourceMap['QuestAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type="text/javascript" src='dwr/util.js'></script>
<link href="css/tmcommon.css?ver=${resourceMap['css/tmcommon.css']}"  rel="stylesheet" type="text/css">
<script src="js/drg.js" type="text/javascript" language="javascript"></script>

<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" />        


	<form method="post" action="setcl.do" onsubmit="return checkCL();">
				<div  class="modal hide"  id="myModalCL" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
					<div class="modal-dialog-for-cgmessage">
					<div class="modal-content">
					<div class="modal-header">
				  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
						<h3 id="myModalLabel"><spring:message code="headCoverLetr"/></h3>
					</div>
					<div class="modal-body" >	
					<div style="padding-top: 10px;padding-bottom: 10px;">
							<label class="redtextmsg"><spring:message code="msgWhenYuClickContiIfYuAreNotTm"/></label>
				   		</div>
					<p id="cvrltrTxt" class="hide"><spring:message code="msgIfYuApplyCentealOfficePosition"/></p>
						<div class='divErrorMsg' id='errordivCL' style="display: block;"></div>						
						<label class="">
					        <input type="radio"  value="1" id="rdoCL1" name="reqType" onclick="setCLEnable()">
					        <span style="padding-top: 5px;"><spring:message code="msgPlzTyYurCoverLtr"/></span>	
				   		</label>							
						<div id="divCoverLetter">
							<label class="redtextmsg"><spring:message code="msgHowToCopyPastCutDouc"/></label>
				   			<textarea id="coverLetter"  name="coverLetter" rows="7" cols="150"></textarea>
				   		</div>	
				   		</br>	
						<label class="">
					        <input type="radio"  value="1" id="rdoCL2" name="reqType"  onclick='setClBlank()'>
					        <span><spring:message code="msgIdoNotWantToAddCorLetter"/></span>
				   		</label>
				   		<label class="checkbox">
					        <input type="checkbox"  value="1" id="isAffilated" name="isAffilated">
					        <spring:message code="msgCurrentlyAffiWithDist/Schl"/>&nbsp;<a href="#" id="iconpophover101" rel="tooltip" data-original-title="Please select this box if you are currently working with this District and/or any School within this District as a full time or part time employee or consultant/contractor"><img src="images/qua-icon.png" width="15" height="15" alt=""></a>
				   		</label>
				 	</div>
				 	<div class="modal-footer">
				 		<button class="btn btn-large btn-primary" type="submit" ><strong><spring:message code="btnConti"/> <i class="icon"></i></strong></button>
				 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClose"/></button> 		
				 	</div>
				</div>
				</div>
				</div>
	 </form>

<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type='text/javascript' src='js/jobboard.js'></script>
<script type='text/javascript' src='js/tmcommon.js'></script>

<script type="text/javascript" src="js/searchjoborder.js"></script>
<script type="text/javascript" src="dwr/interface/DWRAutoComplete.js"></script>
<script type='text/javascript' src='js/certtypeautocomplete.js'></script>



<script>
var $j=jQuery.noConflict();
$j(document).ready(function() {
});

</script>
<script>
function applyScrollOnTblGeoZoneSchool()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#geozoneSchoolTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 350,
        width: 588,
        minWidth: null,
        minWidthAuto: false,
        //colratio:[55,266,70,90,100,105,70,66,151], // table header width
        //mukesh to set the table
        colratio:[380,250],
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });	
}

</script>

<style>
	.hide
	{
		display: none;
	}
	.jboardFooter
	{
		border: 0px hidden white; background-color: #E1E1E1; border-style: solid; margin-left: -5px; width:976px;
	}
	
	
	label > input[type="radio"]
	{
	    position: relative;
	    vertical-align: middle;
	}
	input[type=radio] { margin: 0; padding: 20; }
</style>


			<div class="top10 span16" onkeypress="">
				<!-- Start :: Ashish -->
				<div class="row">
						<div class="col-sm-8 col-md-8">
						    <div class="row">
							<div class="col-sm-6 col-md-6">
								<label>
									<spring:message code="lblDistrict1"/>
								</label>
							 	<input  type="text" 
									class="form-control"
									maxlength="100"
									id="districtName" 
									value=""
									name="districtName" 
									onfocus="getDistrictMasterAutoComp(this, event, 'divTxtDistrictData', 'districtName','districtId',''); showSchool();"
									onkeyup="getDistrictMasterAutoComp(this, event, 'divTxtDistrictData', 'districtName','districtId',''); showSchool();" 
									onblur="hideDistrictMasterDiv(this,'districtId','divTxtDistrictData'); showSchool();hidestate();"/>
								<input  type="hidden" id="districtId" name="districtId" value="">
								<div id='divTxtDistrictData' style=' display:none;position:absolute; z-index: 2000;' 
								onmouseover="mouseOverChk('divTxtDistrictData','districtName')" class='result' ></div>
							</div>
							<div class="col-sm-6 col-md-6">
									<label><spring:message code="lblSchoolName"/></label>
									<input  type="hidden" id="schoolId1" name="schoolId1" value="">
									<input  type="text" 
										class="form-control"					
										maxlength="100"
										id="schoolName1" 
										disabled="disabled"
										autocomplete="off"
										value=""
										name="schoolName1" 
										onfocus="getSchoolMasterAutoComp(this, event, 'divTxtShowData4', 'schoolName1','schoolId1','districtId','');"
										onkeyup="getSchoolMasterAutoComp(this, event, 'divTxtShowData4', 'schoolName1','schoolId1','districtId','');" 
										onblur="hideSchoolMasterDiv(this,'schoolId1','divTxtShowData4');setBlankZip();"/>
									<div id='divTxtShowData4' style=' display:none;position:absolute; z-index: 2000;' 
									onmouseover="mouseOverChk('divTxtShowData4','schoolName1')" class='result' ></div>
							</div>
						</div>	
						
						<div class="row">
							<div class="col-sm-6 col-md-6">
								<label>
									<spring:message code="lblSt"/>
								</label>
								<div id="stateDivId">
								</div>
							 	
							</div>
					       <div class="col-sm-6 col-md-6">
						       	<span class=""><label class=""><spring:message code="lblCity"/></label></span>
						       	<div id="cityDivId">
									<select id="cityId" name="cityId" class="form-control" disabled="disabled">
										<option value="0" selected="selected"><spring:message code="optAll"/></option>
									</select>
								</div>
						   </div>
					    </div>	 
					    
					     <div class="row"> 
							<div class="col-sm-6 col-md-6">
								 <label><spring:message code="lblZipCode1"/>
								 </label>
								 <input class="form-control" type="text" name="zipCode" id="zipCode" onkeypress="return checkForInt(event);"/>  	 
							</div>
							<div class="col-sm-6 col-md-6">
					           	<label>
					            	<spring:message code="lblCerti/LiceName"/>
					            	
					            </label>
					            <input type="hidden" id="certificateTypeMaster" value="">
								<input type="text" 
									class="form-control"
									maxlength="100"
									id="certType" 
									value=""
									name="certType" 
									onfocus="getAllCertificateTypeAutoComp(this, event, 'divTxtCertTypeData', 'certType','certificateTypeMaster','');"
									onkeyup="getAllCertificateTypeAutoComp(this, event, 'divTxtCertTypeData', 'certType','certificateTypeMaster','');" 
									onblur="hideAllCertificateTypeDiv(this,'certificateTypeMaster', 'divTxtCertTypeData');"/>
								<div id='divTxtCertTypeData' style=' display:none;position:absolute;' onmouseover="mouseOverChk('divTxtCertTypeData','certType')" class='result' ></div>
				             </div>
				             
						</div>
						<div class="row top15">
						<div class="col-sm-3 col-md-3">
						       <div class="">
						       	<span class=""><label class="">&nbsp;</label><button class="btn btn-primary " type="button" onclick="searchJob()"><spring:message code="btnSearch"/> <i class="icon"></i></button></span>
						       </div>
							</div>
						</div>	
					    
					     
					 </div>
					 
					 <div class="col-sm-4 col-md-4">
						 <div class="row">
						     <div class="col-sm-12 col-md-12">
						        <label>
								<spring:message code="lblSub"/>
									<a href="#" id="iconpophover102" rel="tooltip" data-original-title="To select more than one subject, please hold the Shift key and select multiple subjects">
									<img src="images/qua-icon.png" width="15" height="15" alt="">
									</a>
								</label> 
								<select id="subjectId" name="subjectId" class="form-control" multiple="multiple" size="4">
									<option value="0"><spring:message code="optAll"/></option>
									<c:if test="${not empty lstMasterSubjectMasters}">	
										<c:forEach var="masterSubjectList" items="${lstMasterSubjectMasters}">
											<option value="${masterSubjectList.masterSubjectId}">${masterSubjectList.masterSubjectName}</option>				
										</c:forEach>
									</c:if>
								</select>
						     </div>
						</div> 
						<div class="row"> 		
							<div class="col-sm-12 col-md-12">					     	   
							       <span class="mt10"><label class=""><spring:message code="lblPostedWithin"/></label>
							       <select id="dropdownSearch" name="dropdownSearch" class="form-control" style="margin-top:-2px;">
									      	<option value="0"><spring:message code="optAll"/></option>
									      	<option value="6"><spring:message code="optL7Ds"/></option>
									      	<option value="29"><spring:message code="opt30Ds"/></option>
									      	<option value="89"><spring:message code="opt90Ds"/></option>
									      	<option value="364"><spring:message code="optL1Y"/></option>
									      </select>
							       </span>					         
						     </div>				    
						</div>
					  </div>  
					 </div>  									
		</div>
	






	<div class="modal hide" id="geozoneschoolDiv"  tabindex="-1" role="dialog" data-keyboard="false" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" >
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="$('#geozoneschoolFlag').val(0);">x</button>
		<h3 id="zoneName"></h3>
	</div>
	<div class="modal-body" style=""> 		
		<div class="control-group">
				<input type="hidden" id="geozoneId" value="0"/>
				<input type="hidden" id="geozoneschoolFlag" value="0"/>
				<div id="geozoneschoolGrid"></div>
		</div>
 	</div>
 	<div class="modal-footer"> 	
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="$('#geozoneschoolFlag').val(0);"><spring:message code="btnClose"/></button> 		
 	</div>
</div>
</div>
</div>

<div class="TableContent top15">        	
            <div class="table-responsive" id="divMain">          
                	         
            </div>            
</div>
	 
<div style="display:none;" id="loadingDiv">
	<table  align="center" >
		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
	</table>
</div>
         


<input type="hidden" name="tempjobid" id="tempjobid"/>
		 	<input type="hidden" name="exitUrl" id="exitUrl"/>
		 	<input type="hidden" name="district" id="district"/>
		 	<input type="hidden" name="school" id="school"/>
		 	<input type="hidden" name="criteria" id="criteria"/>
		 	<input type="hidden" name="epistatus" id="epistatus"/>

<div  class="modal hide"  id="myModalJobMsg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"   style="z-index: 5000;"  data-backdrop="static">
	    <div class="modal-dialog">
	    <div class="modal-content">
		<div class="modal-header">
	  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" >x</button>
			<h3 id="myModalLabel"><spring:message code="lblJobBoard"/></h3>
		</div>
		<div class="modal-body">		
			<div class="control-group">
				<div class="">
			    	<span id="Msg"></span>	        	
				</div>
			</div>
	 	</div>
	 	<div class="modal-footer">
	 		<button class="btn" data-dismiss="modal" aria-hidden="true" ><spring:message code="btnClose"/></button> 		
 		</div>
</div>
</div>
</div>

<div class="modal hide"  id="divAlert" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" style="z-index: 5000;" >
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" id='vcloseBtnk'>x</button>
		<h3 id="myModalLabel">TeacherMatch</h3><input type="hidden" id="isresetStatus" name="isresetStatus" value="0"><input type="hidden" id="isAffilatedValue" name="isAffilatedValue" value="0">
	</div>
	<div class="modal-body">				
		<div class="control-group">
			<div class="" id="divAlertText" >
			</div>
		</div>
 	</div> 	
 	<div class="modal-footer"> 	
 		<button class="btn btn-large btn-primary" aria-hidden="true" onclick="resetjftIsAffilated()" ><strong> <spring:message code="btnYes"/><i class="icon"></i></strong></button>&nbsp;
 		<span><button class="btn btn-large btn-primary" data-dismiss="modal" aria-hidden="true" onclick="canceljftIsAffilated()"><strong><spring:message code="btnNo"/> <i class="icon"></i></strong></button></span> 
 		<span><button class="btn" data-dismiss="modal" aria-hidden="true" ><spring:message code="btnClr"/></button></span>		
 	</div>
</div>
</div>
</div>

	 <div class="modal hide" id="appliedJob" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" style="">		
		 <div class="modal-dialog">
          <div class="modal-content">
			<div class="modal-header">	  		
				<h3 id="myModalLabel"><spring:message code="headTm"/></h3>
			</div>
			<div class="modal-body">				
				<p><spring:message code="msgYuAlreadyAppiedJob"/></p>			
		 	</div> 	
		 	<div class="modal-footer">		 	
		    <button class='btn btn-primary' data-dismiss='modal' aria-hidden='true' onclick="hideDiv()"><spring:message code="btnOk"/></button>		 			
		 	</div>
		</div>
	  </div>
</div>

<div  class="modal hide"  id="applyJobComplete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  style="z-index: 99999;"  data-backdrop="static">
		<div class="modal-dialog">
		<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  >x</button>
		<h3><spring:message code="headTm"/></h3>
	</div>
	<div class="modal-body">	
		<div class="control-group">
			<div class="">
		    	<span><b><spring:message code="msgSucceApldJob"/></b></span>	        	
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="hideInfoBlog();"><spring:message code="btnOk"/></button> 		
 	</div>
</div>
</div>
</div>

<div class="modal hide" id="registerModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" style="">		
		 <div class="modal-dialog">
          <div class="modal-content">
			<div class="modal-header">	 
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick='hideDiv();'>
					x
				</button> 		
				<h3 id="myModalLabel"><spring:message code="headTm"/></h3>
			</div>
			<div class="modal-body">	
			<div class='divErrorMsg' id='signUpServerError' style="display: block;margin-bottom: 10px;"></div>			             
			    <div class='divErrorMsg' id='signUpErrordiv' style="display: block;margin-bottom: 10px;"></div>			    			
				<h1 class="font25 top0" style=""><spring:message code="btnSign"/></h1>				
				<div class="row">
					<div class="col-sm-12 col-md-12 ">
					<div class="" style="color: black;"><spring:message code="lblFname"/><span class="required">*</span></div>
		            <input type="text" id="fname" class="form-control" style="height: 40px; width: 100%;padding: 10px;"/>		           
					</div>
					</div>
				<div class="row">
					<div class="col-sm-12 col-md-12">
					 <div class="" style="color: black;"><spring:message code="lblLname"/><span class="required">*</span></div>
		            <input type="text" id="lname" class="form-control" style="height: 40px; width: 100%;padding: 10px;"/>	          
					</div>
					</div>
						<div class="row">	           		        
			        <div class="col-sm-12 col-md-12 " >
			         <div class="" style="color: black;"><spring:message code="lblEmail"/><span class="required">*</span></div>
			           <input type="text" id="signupEmail" class="form-control" style="height: 40px; width: 100%;padding: 10px;"/>
			        </div>	           							
		 	</div>
		 	</div>
		 	<!--<div class="row">
		 	<div class="col-sm-12 col-md-12" style="color: black;">Email<span class="required">*</span></div>
	            <input type="text" id="signupEmail" class="form-control" style="height: 40px; width: 100%;padding: 10px;"/>
		 	</div>
				</div>			 
	            -->							
				 	
		 	<div class="modal-footer">			 	
		 	<button class="btn btn-primary" type="button" onclick="return signUpTempUser();"><spring:message code="btnSign"/></button>	 	
		    <button class='btn btn-primary' data-dismiss='modal' aria-hidden='true' onclick="hideDiv()"><spring:message code="btnClr"/></button>		 			
		 	</div>
		</div>
	  </div>
</div>
	
<script>
 $('#iconpophover102').tooltip();
</script>



<script type="text/javascript">
	//displayJobRecords();
//	displayFilter();
	displayState();
</script>
<script type="text/javascript">//<![CDATA[
     // var cal = Calendar.setup({
       //   onSelect: function(cal) { cal.hide() },
      //    showTime: true
     // });
     //cal.manageFields("fromDate", "fromDate", "%m-%d-%Y");
   //  cal.manageFields("toDate", "toDate", "%m-%d-%Y");
    //]]>
   
</script>  