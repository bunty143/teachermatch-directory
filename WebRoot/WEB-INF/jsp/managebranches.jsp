<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<script type="text/javascript" src="js/manageuser.js?ver=${resourceMap['js/manageuser.js']}"></script>
<script type="text/javascript" src="dwr/interface/HeadQuarterAjax.js?ver=${resourceMap['HeadQuarterAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/interface/DistrictAjax.js?ver=${resourceMap['DistrictAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/interface/AutoSearchFilterAjax.js?ver=${resourceMap['AutoSearchFilterAjax.ajax']}"></script>
<script type='text/javascript' src="js/branchautoSuggest.js?ver=${resourceMap['js/branchautoSuggest.js']}"></script>

<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type='text/javascript' src="js/managebranches.js?ver=${resourceMap['js/managebranches.js']}"></script>

<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" />
<script>
var $j=jQuery.noConflict();
$j(document).ready(function() {
});
function applyScrollOnTbl()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 400,
        width: 945,
        minWidth: null,
        minWidthAuto: false,
         <c:if test="${userMaster.entityType ne 2 or userMaster.entityType ne 5}">
           colratio:[180,169,120,70,70,60,90,60,142], // table header width
        </c:if>
                
        <c:if test="${userMaster.entityType eq 2 or userMaster.entityType ne 5}">
           colratio:[180,169,120,80,70,70,100,70,102], // table header width
        </c:if>
        
        
        
        
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}


function applyScrollOnPrintTable()
	{
		var $j=jQuery.noConflict();
	        $j(document).ready(function() {
	        $j('#tblGridHired').fixheadertable({ //table id 
	        caption: '',
	        showhide: false,
	        theme: 'ui',
	        height: 300,
	         width: 940,
	        minWidth: null,
	        minWidthAuto: false,
	       colratio:[180,180,100,90,90,90,60,60,70,65,80,75,65],
	        //colratio:[175,175,110,120,200,100,85],
	        addTitles: false,
	        zebra: true,
	        zebraClass: 'net-alternative-row',
	        sortable: false,
	        sortedColId: null,
	        dateFormat: 'd-m-y',
	        pager: false,
	        rowsPerPage: 10,
	        resizeCol: false,
	        minColWidth: 100,
	        wrapper: false
	        });	            
	        });	
	}

</script>


<style>
	
</style>
<div class="row" style="margin-left: 0px;margin-right: 0px;margin-top:">
         <div style="float: left;">
         	<img src="images/manageusers.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;">Manage Branch Area<span id="districtOrSchoolTitle"></span></div>	
         </div>
		 <div class="pull-right add-employment1">	
		    <c:set var="url" value="addbranch.do?headQId=${headQuarterMaster.headQuarterId}"></c:set>
		    <c:if test="${(userMaster.roleId.roleId ne 12 && userMaster.entityType ne 6)}">	
				<a href="javascript:void(0);" onclick="addnewBranch('${url}')">+Add Branch</a>
			</c:if>
		 </div>
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
</div>
<!--<div class="offset1 span14 mb">-->
   <c:if test="${userMaster.entityType eq 1}">
   <div class="mt10"></div>
   </c:if> 
  <c:if test="${userMaster.entityType eq 6}">
   <input type="hidden" id="MU_EntityType" value="6"/>
   </c:if> 
   <c:if test="${userMaster.entityType ne 6}">  
	<div class="row" onkeypress="return chkForEnterShowDistrict(event);" >		
			<form class="bs-docs-example" onsubmit="return false;">
				<div id="Searchbox" class="<c:out value="${hide}"/>">
					<div class="col-sm-3 col-md-3">
						<label>Entity Type</label>
					    <select class="form-control" id="MU_EntityType" name="MU_EntityType" class="form-control" onchange="DisplayHideSearchBox('${userMaster.entityType}');">
								<c:if test="${userMaster.entityType eq 1}">
								<option value="1" selected>TM</option>
								</c:if>
								<option value="5">HeadQuarter</option>
								
								<option value="6">Branch</option>
						</select>
	              	</div>
	      
	              <c:choose>  
	              	<c:when test="${kFlag eq 'yes'}">	
	              	<div class="col-sm-3 col-md-3">
						<label>Status</label>
					    <select class="form-control" id="status_filter" name="status_filter" class="form-control">							
					    	<option value="ST">Select Type</option>
							<option value="A">Active</option>
							<option value="I">Inactive</option>
						</select>
	              	</div>
	          </c:when>	          
	          <c:otherwise>
	            	<div class="col-sm-3 col-md-3" style="display:none">
						<label>Status</label>
					    <select class="form-control" id="status_filter" name="status_filter" class="form-control">							
					    	<option value="ST">Select Type</option>
							<option value="A">Active</option>
							<option value="I">Inactive</option>
						</select>
	              	</div>
	          </c:otherwise>
	          </c:choose>
		           <div id="SearchTextboxDiv" style="display:none"  <c:out value="${hide}"/>">                                                   
                     <div  class="col-sm-5 col-md-5">
                                     <label id="captionDistrictOrSchool">District</label>
                                     <input type="text" id="districtName" name="districtName"  class="form-control"
                                     onfocus="getDistrictMasterAutoComp(this, event, 'divTxtShowData', 'districtName','districtId','');"
                                                                                     onkeyup="getDistrictMasterAutoComp(this, event, 'divTxtShowData', 'districtName','districtId','');"
                                                                                     onblur="hideDistrictMasterDiv(this,'districtId','divTxtShowData');"           />
                                                                     <div id='divTxtShowData' style=' display:none;position:absolute;z-index:5000;' onmouseover="mouseOverChk('divTxtShowData','districtName')" class='result' ></div>    
                     </div>                                
                     </div>
                     <div id="SearchHeadQuarterTextboxDiv"  style="display:none">	 
		          		<div class="col-sm-4 col-md-4 " id="headQuarterClass">
	                        <label>Head Quarter</label>
	                        <input class="form-control" type="text" id="onlyHeadQuarterName" name="onlyHeadQuarterName"   value="${headQuarterId}"
	                          onfocus="getOnlyHeadQuarterAutoComp(this, event, 'divTxtHeadQuarterData', 'onlyHeadQuarterName','headQuarterHiddenId','');"
	                          onkeyup="getOnlyHeadQuarterAutoComp(this, event, 'divTxtHeadQuarterData', 'onlyHeadQuarterName','headQuarterHiddenId','');"
	                          onblur="hideHeadQuarterDiv(this,'headQuarterHiddenId','divTxtHeadQuarterData');setHeadQuarterId();"      />
	                                
	                          <div id='divTxtHeadQuarterData' style=' display:none;position:absolute;'  onmouseover="mouseOverChk('divTxtHeadQuarterData','onlyHeadQuarterName')"  class='result' ></div>
	                          <input type="hidden" id="headQuarterHiddenId" value="" />
	                          <input type="hidden" id="isBranchExistsHiddenId" value=""/>    
	                     </div>
	          		
	          		</div>
                     <div id="SearchBranchTextboxDiv"  <c:out value="${hide}"/>">                 
                           <div class="col-sm-4 col-md-4">
                                  <label>Branch Name</label> 
                             <input type="text" id="branchName" name="branchName"  class="form-control"
                                 onfocus="getBranchMasterAutoComp(this, event, 'divTxtShowDataBranch', 'branchName','branchId','');"
                                  onkeyup="getBranchMasterAutoComp(this, event, 'divTxtShowDataBranch', 'branchName','branchId','');"
                                onblur="hideBranchMasterDiv(this,'branchId','divTxtShowDataBranch');"            />
                                <input  type="hidden" id="branchId" name="branchId" value="">
                         <div id='divTxtShowDataBranch' style=' display:none;position:absolute;z-index:5000;' onmouseover="mouseOverChk('divTxtShowDataBranch','branchName')" class='result' ></div>                                                                                                     
                      </div>      
   					</div>

					<div class="col-sm-2 col-md-2">					
						<c:if test="${fn:indexOf(roleAccess,'|5|')!=-1}">
						<button class="btn btn-primary top25-sm" type="button" onclick="searchDistrict();">Search <i class="icon"></i></button>
						</c:if>						
					</div>
        	  	</div>
       		 </form>
       		 
       		  <c:choose>  
	              	<c:when test="${kFlag eq 'yes'}">
       		 <div style="clear: both;"></div>            
       		  <table class="marginrightForTablet">
			      <tr>
			         <td  style='padding-left:5px;'>
						<a data-original-title='Export to Excel' rel='tooltip' id='exlId' href='javascript:void(0);'  onclick="ExportKellyBranchToExcel();"><span class='icon-file-excel icon-large iconcolor'></span></a>
					 </td>
					 <td>
					  <a data-original-title='Export to PDF' rel='tooltip'  href="javascript:void(0);" id='pdfId' onclick='ExportKellyBranchesToPDF();'><span class='pdf-eoc icon-large iconcolor'></span></a>
					 </td>					
					 <td>
						<a data-original-title='Print Preview' rel='tooltip' id='printId' href='javascript:void(0);' onclick="KellyManageBranchesPrintPre();"><span class='icon-print icon-large iconcolor'></span></a>
					 </td></tr>	
		      </table>  
		      </c:when>
		      </c:choose>
		      
		    <div style="clear: both;"></div>
		</div>  			      
	 </c:if>
	 
	
	 <!-- printing Start  -->
     
 	<div style="clear: both;"></div>

   <div class="modal hide"  id="printHiredApplicantsDiv"  tabindex="-1" role="dialog" data-keyboard="false" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" >
	<div class="modal-dialog" style="width:1000px; margin-top: 0px;">
	<div class="modal-content">
	<div class="modal-header">
	   	<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="canelPrint();">x</button>
	   	<h3 id="">Print Preview</h3>
	</div>
	
	<div class="modal-body" style="max-height: 500px; overflow-y: auto;"> 		
			<div class="" id="printhiredApplicantsDataTableDiv"  >          
             </div>
 	</div>
 	<div class="modal-footer">
	 	<table border=0 align="right">
	 		<tr>
		 		<td  nowrap>
		 			<button class="btn  btn-large btn-primary" aria-hidden="true" onclick='printKellyBranchDATA();'>&nbsp;&nbsp;&nbsp;Print&nbsp;&nbsp;&nbsp;</button>&nbsp;&nbsp;
				</td>
				<td  nowrap>
		 			<button class="btn btn-large" data-dismiss="modal" aria-hidden="true" onclick='canelPrint();'>Cancel</button>
				</td>
	 		</tr>
	 	</table>
	</div>
  </div>
 </div>
</div>

     <div style="clear: both;"></div>

   <div class="modal hide"  id="printmessage1"  tabindex="-1" role="dialog" data-keyboard="false" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" >
	<div class="modal-dialog" style="width:450px; margin-top: 0px;">
	<div class="modal-content">
	<div class="modal-header">
	   	<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="canelPrint();">x</button>
	   <h3 id="myModalLabel"><spring:message code="headTm"/></h3>
	</div>
	<div class="modal-body" style="max-height: 400px; overflow-y: auto;"> 		
			<div class="control-group">
			<div class="" >
			<spring:message code="msgPlzPopAreAlloued"/>
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
	 	<table border=0 align="right">
	 		<tr>
		 		<td  nowrap>
		 			<button class="btn btn-large" data-dismiss="modal" aria-hidden="true" onclick='canelPrint();'><spring:message code="btnOk"/></button>
				</td>
	 		</tr>
	 	</table>
	</div>
  </div>
 </div>
</div>
  <!-- printing end  -->
  
  <!-- ----------pdf start here preview -->
<div  class="modal hide pdfDivBorder"  id="modalDownloadHiredCandidates" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog-for-cgpdfreport">
	<div class="modal-content"  style="background-color: #ffffff;">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><spring:message code="btnX"/></button>
		<h3 id="myModalLabel">Manage Branch Area</h3>
	</div>
		<div class="modal-body">		
			<div class="control-group">
				    <div style="width: 100%; height: 100%; -webkit-overflow-scrolling: touch;">
	                  <iframe src="" id="ifrmCJS" width="100%" height="480px">
	               </iframe>        	
				</div>
			</div>
	 	</div>
	 	<div class="modal-footer">
	 		<button class="btn" data-dismiss="modal" aria-hidden="true" ><spring:message code="btnClose"/></button> 		
	 	</div>
     </div>
 	</div>
</div>
  
  <!--------------pdf end here ------>  
	
	   <div id="" class="hide" style="height: 480px; width: 100%; overflow: auto;'-webkit-overflow-scrolling' : 'touch">
			 <iframe src="" id="ifrmTrans" width="100%" height="100%" scrolling="no">
			 </iframe>  
		  </div>	  
	
	<div  class="modal hide"  id="myModalUpdateMsg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"   style="z-index: 5000;"  data-backdrop="static">
		<div class="modal-dialog">
		<div class="modal-content">
		<div class="modal-header">
	  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
			<h3 id="myModalLabel">Manage Branch</h3>
		</div>
		<div class="modal-body">		
			<div class="control-group">
				<div class="">
			    	<span id="updateMsg"></span>	        	
				</div>
			</div>
	 	</div>
	 	<div class="modal-footer">
	 		<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button> 		
 		</div>
	</div>
</div>
</div>
  <div class="TableContent top15">        	
            <div class="table-responsive" id="divMain">          
                	         
            </div>            
   </div> 
	
    <br><br>
    <div style="display:none;" id="loadingDiv">
    <table  align="center">
 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
	</table>
	</div>
<!-- Message Div add by Rahul -->

<div  class="modal hide"  id="myModalactMsgShow"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="showMessageDiv()">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div class="modal-body">
		<div class="control-group" id='confirmDeactivate'>Are you sure want to deactivate the Branch?
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn btn-primary" data-dismiss="modal" aria-hidden="true" onclick="toggleStatus()">Yes</button>
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="showMessageDiv()">No</button>
 	</div>
 	<input type="hidden" id="actbranch" name="actbranch">
 	<input type="hidden" id="branchstat" name="branchstat">
 	
</div> 	
</div>
</div>


<!-- END -->
 <input type="hidden"  id="headQuarterId" name="headQuarterId" value="${headQuarterMaster.headQuarterId}" />
 <input type="hidden"  id="branchMasterId" name="branchMasterId" value="${branchMaster.branchId }" />
 <input type="hidden" id="entityType" name="entityType" value="${userMaster.entityType}"/>
<script>
onLoadDisplayHideSearchBox(<c:out value="${userMaster.entityType}"/>);
updateMsg('<c:out value="${authKeyVal}"/>',<c:out value="${userMaster.entityType}"/>);
	
</script>

