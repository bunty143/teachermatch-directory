<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<link href="css/bootstrap.css" rel="stylesheet" type="text/css">
		<link href="css/style.css" rel="stylesheet" type="text/css">
		<link href="css/tmcommon.css" rel="stylesheet" type="text/css">
		<script type="text/javascript" src="js/tmcommon.js">	
		</script>
		
		<script src="js/jquery.js"></script>
		<script src="js/bootstrap.js"></script>

	<script type="text/javascript" src="dwr/interface/CandidateGridAjax.js"></script>
	<script type="text/javascript" src="dwr/engine.js"></script>
	<script type='text/javascript' src='dwr/util.js'></script>
	<script type='text/javascript' src='js/report/candidategrid.js'></script>
	
	<script src="jquery/jquery-ui.custom.js" type="text/javascript"></script>
	<script src="jquery/jquery.cookie.js" type="text/javascript"></script>

	<link href="jquery/skin/ui.dynatree.css" rel="stylesheet" type="text/css">
	<script src="jquery/jquery.dynatree.js" type="text/javascript"></script>

	
	<!-- jquery.contextmenu,  A Beautiful Site (http://abeautifulsite.net/) -->
	<script src="contextmenu/jquery.contextMenu-custom.js" type="text/javascript"></script>
	<link href="contextmenu/jquery.contextMenu.css" rel="stylesheet" type="text/css" >

	<!-- Start_Exclude: This block is not part of the sample code -->
	<link href="contextmenu/prettify.css" rel="stylesheet">
	<script src="contextmenu/prettify.js" type="text/javascript"></script>
	<link href="contextmenu/sample.css" rel="stylesheet" type="text/css">
	<script src="contextmenu/sample.js" type="text/javascript"></script>
	<!-- End_Exclude -->
	
	<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="css/font-awesome-ie7.css" rel="stylesheet" type="text/css">
	<link href="css/font-awesome-ie7.min.css" rel="stylesheet" type="text/css">
	<link href="css/font-awesome.css" rel="stylesheet" type="text/css">
  
	<style>
	.hide
	{
		display: none;
	}
	 .icon-folder-open
	{
		 font-size: 1.5em;
		color:#007AB4;
	}
	.icon-copy
	{
		font-size: 1.5em;
		color:#007AB4;
	}
	.icon-edit
	{
		font-size: 1.5em;
		color:#007AB4;
	}
	.icon-cut
	{
		font-size: 1.5em;
		color:#007AB4;
	}
	.icon-paste
	{
		font-size: 1.5em;
		color:#007AB4;
	}
	.icon-remove-sign
	{
		font-size: 1.5em;
		color:#007AB4;
	}
	.marginleft20
	{
		margin-left:-20px;
	} 
 	
	</style>


	<script type="text/javascript">
	// --- Implement Cut/Copy/Paste --------------------------------------------
	var clipboardNode = null;
	var pasteMode = null;

	function copyPaste(action, node) {
		//alert("---- Gagan : action ==== "+action+" and node value  "+node);
		switch( action ) {
		case "cut":
		case "copy":
			clipboardNode = node;
			pasteMode = action;
			break;
		case "paste":
			if( !clipboardNode ) {
				//alert("Clipoard is empty.");
				break;
			}
			if( pasteMode == "cut" ) {
			//alert("cut pasteMode "+pasteMode+" key  "+node.data.key);
				// Cut mode: check for recursion and remove source
				var isRecursive = false;
				var dictKey=null;
				var i="";
				var cb = clipboardNode.toDict(true, function(dict){
					// If one of the source nodes is the target, we must not move
					//alert("----- Key "+dict.key);
					if( dict.key == node.data.key )
						isRecursive = true;
					dictKey	=	dict.key;
					//alert("----- DICTKey"+i+" ----------"+dictKeyi);
					i+=dict.key+",";
					//i++;
				});
				if( isRecursive ) {
					//alert("Cannot move a node to a sub node.");
					return;
				}
					//alert(" \n value of i ==="+i)
					//var n=str.split("");
					var n=i.split(",");
					//alert(" Length "+n.length+" n[0] "+n[0])
					var cutFolderKey=n[0];
					//alert(" dictKey "+dictKey+" cutFolderKey "+cutFolderKey+"  node.data.key "+node.data.key);
					CandidateGridAjax.cutPasteFolder(node.data.key,cutFolderKey,
					{ 
						async: false,
						errorHandler:handleError,
						callback:function(data)
						{	
							//alert("=== data ===="+data);
						}
					});
				
				
				
				//copyPasteFolder('cut',dict.key,node.data.key);
				node.addChild(cb);
				clipboardNode.remove();
			} else {
			var dictKey=null;
			var dictTitle=null;
				//alert(" pasteMode "+pasteMode+" key  "+node.data.key);
				// Copy mode: prevent duplicate keys:
				var cb = clipboardNode.toDict(true, function(dict){
					dictTitle=dict.title;
					dict.title = "Copy of " + dict.title;
					dictKey	=	dict.key;
					delete dict.key; // Remove key, so a new one will be created
				});
				//alert(" dictTitle "+dictTitle+" dictKey "+dictKey);
				
				CandidateGridAjax.copyPasteFolder(node.data.key,dictTitle,
				{ 
					async: false,
					errorHandler:handleError,
					callback:function(data)
					{	
						//alert("=== data ===="+data);
						var childNode = node.addChild({
							title: data.folderName,
							tooltip: "",
							isFolder: true,
							expand:true,
							key: data.folderId
						});
						//node.addChild(cb);
					}
				});
				
				
				
				
				//copyPasteFolder('copy',dictKey,node.data.key);
				//node.addChild(cb);
			}
			clipboardNode = pasteMode = null;
			break;
		default:
			alert("Unhandled clipboard action '" + action + "'");
		}
	};

	// --- Contextmenu helper --------------------------------------------------
	// --- Contextmenu helper --------------------------------------------------
	function bindContextMenu(span) {
		//alert("------ bindContextMenu ----------");
		// Add context menu to this node:
		$(span).contextMenu({menu: "myMenu"}, function(action, el, pos) {
			// The event was bound to the <span> tag, but the node object
			// is stored in the parent <li> tag
			var node = $.ui.dynatree.getNode(el);
			//alert(" action "+action+" === delete ===="+node);
			switch( action ) {
			case "cut":
			case "copy":
			case "paste":
				copyPaste(action, node);
				break;
			/*============ Gagan : For Edit Folder Code Here ===================*/
			case "edit":
			editNode(node);
			break;
			case "create":
			node.activate(true);
			createUserFolder(node);
			/*var rootNode = $("#tree").dynatree("getActiveNode");
			var childNode = rootNode.addChild({
				title: "New Folder",
				tooltip: "",
				isFolder: true
			});*/
			break;
			case "delete":
			checkFolderIsHomeOrShared(node)
			//window.parent.deletesavefolder();
			//deleteFolder();
			break;
			default:
				//alert("---------------------: appply action '" + action + "' to node " + node);
			}
		});
	};
	
	function checkFolderIsHomeOrShared(rootNode)
	{
		//alert(" tree 227 rootNode "+rootNode);
		$('#errortreediv').empty();
		$('#errordeletetreediv').empty();
		$('#errordeletetreediv').hide();
		var folderId="";
		//var rootNode =$("#tree").dynatree("getActiveNode");
		//alert(" rootNode "+rootNode);
		if(rootNode==null)
		{
			//document.getElementById('errortreediv').innerHTML		=	'&#149; Please enter Domain Name';
			rootNode =$("#tree").dynatree("getActiveNode");
			folderId=rootNode.data.key;
			//$('#errortreediv').html("&#149; Please select any folder");
			//$('#errortreediv').show();
			//return false;
		}
		else
		{
			// When root Node is not null
			folderId=rootNode.data.key;
		}
		//alert(" folderId "+folderId);
	//	var folderId=rootNode.data.key;
		//alert("Before Created"+parentId);
		CandidateGridAjax.checkFolderIsHomeOrShared(folderId,
		{ 
			async: false,
			errorHandler:handleError,
			callback:function(data)
			{	
				//alert(" ==== data === "+data);
				if(data==3)
				{
					$('#errordeletetreediv').show();
					$('#errortreediv').hide();
					$('#errordeletetreediv').html("&#149; This Folder can not be deleted");
				}
				else 
				{
					if(data==4)
					{
						try{
							window.parent.deletesavefolder(rootNode);
						}catch(err)
						  {}
					}
				}
				
			}
		});
	$('#errortreediv').hide();
	}
	
	function checkFolderId()
	{
		var rootNode =$("#tree").dynatree("getActiveNode");
		//alert(" rootNode "+rootNode);
		if(rootNode==null)
		{
			$("#frame_folderId").val("");
			//document.getElementById('errortreediv').innerHTML		=	'&#149; Please enter Domain Name';
		}
		else
		{
			$("#frame_folderId").val(rootNode.data.key);
		}
		
		
	}
	
	function deleteFolder(rootNodeKey)
	{
		//alert(" rootNodeKey "+rootNodeKey);
		if(rootNodeKey!=0)
		{
			rootNode =$("#tree").dynatree("getTree").getNodeByKey(rootNodeKey);
			rootNode.activate(true);
		}
		//alert("tree 302 rootNode "+rootNode)
		//window.parent.deletesavefolder();
		//alert(" deleteFolder "+rootNode.data.key);
		
		//return false;
		
		
		//$("#tree").dynatree("getTree").getNodeByKey(rootNodeKey).activate();
		//$('#deleteFolder').modal('show');
		$('#errortreediv').empty();
		//var rootNode =$("#tree").dynatree("getActiveNode");
		//alert(" rootNode "+rootNode);
		var folderId="";
		//var rootNode =$("#tree").dynatree("getActiveNode");
		//alert(" rootNode "+rootNode);
		if(rootNode==null)
		{
			//document.getElementById('errortreediv').innerHTML		=	'&#149; Please enter Domain Name';
			rootNode =$("#tree").dynatree("getActiveNode");
			folderId=rootNode.data.key;
			//$('#errortreediv').html("&#149; Please select any folder");
			//$('#errortreediv').show();
			//return false;
		}
		else
		{
			// When root Node is not null
			folderId=rootNode.data.key;
		}
		//alert("tree 330 folderId "+folderId);
		
		//var folderId=rootNode.data.key;
		//alert("Before Created"+parentId);
		CandidateGridAjax.deleteUserFolder(folderId,
		{ 
			async: false,
			errorHandler:handleError,
			callback:function(data)
			{	
				//alert(" ==== data === "+data);
				rootNode.remove();
				$("#frame_folderId").val("");
				
				// Showing Gride By Default HoMe
				
				var tree = $('#tree').dynatree("getTree");
				var root = tree.getRoot();
				var nodeList = root.getChildren();
				nodeList[0].activate(true);
				var folderId 				=	nodeList[0].data.key;
				window.parent.displaySavedCGgridByFolderId(0,folderId,0);
			}
		});
	$('#errortreediv').hide();
	//alert("Folder Created")
}

function renameFolder(){
	var activeNode = $("#tree").dynatree("getActiveNode");
	editNode(activeNode);

}
function cutFolder(){
	var activeNode = $("#tree").dynatree("getActiveNode");
	copyPaste('cut', activeNode);

}
function copyFolder(){
	var activeNode = $("#tree").dynatree("getActiveNode");
	copyPaste('copy', activeNode);

}
function pasteFolder(){
	var activeNode = $("#tree").dynatree("getActiveNode");
	copyPaste('paste', activeNode);

}
function deletFolder(rootNodeKey){
	var rootNode = null;
	if(rootNodeKey!=0)
	{
		rootNode =$("#tree").dynatree("getTree").getNodeByKey(rootNodeKey);
	}
	//alert("tree 379 rootNode "+rootNode)
	
	 checkFolderIsHomeOrShared(rootNode);
}
function displayCandidateGridOfHomeFolder()
{
	$('#tree').dynatree({
			persist: true,
			onActivate: function(node) {
				$('#errortreediv').hide();
				$('#errordeletetreediv').hide();
				$("#echoActivated").text(node.data.title + ", key=" + node.data.key);
				$("#frame_folderId").val(node.data.key);
				window.parent.defaultSaveDiv();
				window.parent.displaySavedCGgridByFolderId(0,node.data.key,1);
			/*	var rootNode =$("#tree").dynatree("getActiveNode");
				if(rootNode==null)
				{
					//document.getElementById('errortreediv').innerHTML		=	'&#149; Please enter Domain Name';
					$('#errortreediv').html("&#149; Please select any folder");
					$('#errortreediv').show();
					return false;
				}
			*/
				//$("#frame_folderId").val($("#tree").dynatree(rootNode.data.key);
				//alert(" ----- frame_folderId ----- "+$("#frame_folderId").val());
			},
			onClick: function(node, event) {
				// Close menu on click
				if( $(".contextMenu:visible").length > 0 ){
					$(".contextMenu").hide();
//					return false;
				}
			},
			onKeydown: function(node, event) {
				// Eat keyboard events, when a menu is open
				if( $(".contextMenu:visible").length > 0 )
					return false;

				switch( event.which ) {

				// Open context menu on [Space] key (simulate right click)
				case 32: // [Space]
					$(node.span).trigger("mousedown", {
						preventDefault: true,
						button: 2
						})
					.trigger("mouseup", {
						preventDefault: true,
						pageX: node.span.offsetLeft,
						pageY: node.span.offsetTop,
						button: 2
						});
					return false;

				// Handle Ctrl-C, -X and -V
				case 67:
					if( event.ctrlKey ) { // Ctrl-C
						copyPaste("copy", node);
						return false;
					}
					break;
				case 86:
					if( event.ctrlKey ) { // Ctrl-V
						copyPaste("paste", node);
						return false;
					}
					break;
				case 88:
					if( event.ctrlKey ) { // Ctrl-X
						copyPaste("cut", node);
						return false;
					}
					break;
				}
			},
			/*Bind context menu for every node when it's DOM element is created.
			  We do it here, so we can also bind to lazy nodes, which do not
			  exist at load-time. (abeautifulsite.net menu control does not
			  support event delegation)*/
			onCreate: function(node, span){
			//alert(" onCreate Method is calling "); 
			 	node.expand(false);
			 	//$("#"+node.data.key).append("<li>Gagan</li>");
			 	//alert("Hi");
				bindContextMenu(span);
			},
			onFocus: function(node) {
				$("#echoFocused").text(node.data.title);
			},
			onBlur: function(node) {
				$("#echoFocused").text("-");
			},
			/*Load lazy content (to show that context menu will work for new items too)*/
			onLazyRead: function(node){
				node.appendAjax({
					url: "contextmenu/sample-data2.json"
				});
			},
		/* D'n'd, just to show it's compatible with a context menu.
		   See http://code.google.com/p/dynatree/issues/detail?id=174 */
		dnd: {
			preventVoidMoves: true, // Prevent dropping nodes 'before self', etc.
			onDragStart: function(node) {
				return true;
			},
			onDragEnter: function(node, sourceNode) {
				if(node.parent !== sourceNode.parent)
					return false;
				return ["before", "after"];
			},
			onDrop: function(node, sourceNode, hitMode, ui, draggable) {
				sourceNode.move(node, hitMode);
			}
		}
		});
	//@-------330---- 
 var tree = $('#tree').dynatree("getTree");
  var root = tree.getRoot(); 
 var nodeList= root.getChildren();
 if(nodeList!=null)
 {
 nodeList[0].activate(true);
 var folderId = nodeList[0].data.key;
 var pgflg=$("#pageFlag").val();
 if(pgflg==''){
  pgflg=0;
 }
 
 window.parent.displaySavedCGgridByFolderId(pgflg,folderId,pgflg);
}
}
function createUserFolder(rootNode)
{
		// refreshTree();
		//alert(" Create Folder");
		$('#errortreediv').empty();
		//var rootNode =$("#tree").dynatree("getActiveNode");
		//alert(" rootNode "+rootNode);
		if(rootNode==null)
		{
			//document.getElementById('errortreediv').innerHTML		=	'&#149; Please enter Domain Name';
			$('#errortreediv').html("&#149; Please select any folder");
			$('#errortreediv').show();
			rootNode =$("#tree").dynatree("getActiveNode");
			//return false;
		}
		//rootNode.expand(true);
		//alert(" rootNode "+rootNode+" Key "+rootNode.data.key)
		var parentId=rootNode.data.key;
		//alert("Before Created"+parentId);
		CandidateGridAjax.createUserFolder(parentId,
		{ 
			async: false,
			errorHandler:handleError,
			callback:function(data)
			{	
				//alert("=== data ===="+data);
				var childNode = rootNode.addChild({
					title: data.folderName,
					tooltip: "",
					isFolder: true,
					expand:true,
					key: data.folderId
				});
			}
		});
		//displayTreeStructure();
		//onloadscript();
		//alert("out");
		/*var childNode = rootNode.addChild({
		title: "New Folder",
		tooltip: "",
		isFolder: true,
		expand:true,
		key: "100"
	});*/
	$('#errortreediv').hide();
	//alert("Folder Created")
}
	function editNode(node){
	if(node==null)
	{
		return false;
	}
//alert("Calling Edit Function")
	var prevTitle = node.data.title,
		tree = node.tree;
	// Disable dynatree mouse- and key handling
	tree.$widget.unbind();
	// Replace node with <input>
	$(".dynatree-title", node.span).html("<input id='editNode' style='width:100px;'  maxlength='50' value='" + prevTitle + "'>");
	// Focus <input> and bind keyboard handler
	$("input#editNode")
		.focus()
		.keydown(function(event){
			switch( event.which ) {
			case 27: // [esc]
				// discard changes on [esc]
				$("input#editNode").val(prevTitle);
				$(this).blur();
				break;
			case 13: // [enter]
				// simulate blur to accept new value
				$(this).blur();
				break;
			}
		}).blur(function(event){
			// Accept new value, when user leaves <input>
			var title = $("input#editNode").val();
			//alert("Title "+title+" node Id "+node.data.key);
			if(title=="" || title.length<0 || title==prevTitle)
			{
				//alert("1");
				node.setTitle(prevTitle);
			}
			else
			{
				//alert("2");
				node.setTitle(title);
			//---------------------- Edit Or Rename Folder Name --------------------------
				CandidateGridAjax.renameUserFolder(node.data.key,title,
				{ 
					async: false,
					errorHandler:handleError,
					callback:function(data)
					{	
						//alert("=== data ===="+data);
					}
				});
				
				
				
			}
			// Re-enable mouse and keyboard handlling
			tree.$widget.bind();
			node.focus();
		});
}
	
	
	/*
	function bindContextMenu(span) {
		// Add context menu to this node:
		$(span).contextMenu({menu: "myMenu"}, function(action, el, pos) {
			// The event was bound to the <span> tag, but the node object
			// is stored in the parent <li> tag
			var node = $.ui.dynatree.getNode(el);
			switch( action ) {
			case "cut":
			case "copy":
			case "paste":
				copyPaste(action, node);
				break;
			default:
				alert("Todo: appply action '" + action + "' to node " + node);
			}
		});
	};
*/
	// --- Init dynatree during startup ----------------------------------------
// $ is now an alias to the jQuery function; creating the new alias is optional.
 var $ = jQuery.noConflict();
	$(function(){
		//alert("Treeview.js")
		$("#tree").dynatree({
			persist: true,
			onActivate: function(node) {
				$('#errortreediv').hide();
				$('#errordeletetreediv').hide();
				$("#echoActivated").text(node.data.title + ", key=" + node.data.key);
				$("#frame_folderId").val(node.data.key);
				var pgflg=$("#pageFlag").val();
				if(pgflg=="")
				pgflg=1;
				window.parent.defaultSaveDiv();
				window.parent.displaySavedCGgridByFolderId(0,node.data.key,pgflg);
			/*	var rootNode =$("#tree").dynatree("getActiveNode");
				if(rootNode==null)
				{
					//document.getElementById('errortreediv').innerHTML		=	'&#149; Please enter Domain Name';
					$('#errortreediv').html("&#149; Please select any folder");
					$('#errortreediv').show();
					return false;
				}
			*/
				//$("#frame_folderId").val($("#tree").dynatree(rootNode.data.key);
				//alert(" ----- frame_folderId ----- "+$("#frame_folderId").val());
			},
			onClick: function(node, event) {
				// Close menu on click
				if( $(".contextMenu:visible").length > 0 ){
					$(".contextMenu").hide();
//					return false;
				}
			},
			onKeydown: function(node, event) {
				// Eat keyboard events, when a menu is open
				if( $(".contextMenu:visible").length > 0 )
					return false;

				switch( event.which ) {

				// Open context menu on [Space] key (simulate right click)
				case 32: // [Space]
					$(node.span).trigger("mousedown", {
						preventDefault: true,
						button: 2
						})
					.trigger("mouseup", {
						preventDefault: true,
						pageX: node.span.offsetLeft,
						pageY: node.span.offsetTop,
						button: 2
						});
					return false;

				// Handle Ctrl-C, -X and -V
				case 67:
					if( event.ctrlKey ) { // Ctrl-C
						copyPaste("copy", node);
						return false;
					}
					break;
				case 86:
					if( event.ctrlKey ) { // Ctrl-V
						copyPaste("paste", node);
						return false;
					}
					break;
				case 88:
					if( event.ctrlKey ) { // Ctrl-X
						copyPaste("cut", node);
						return false;
					}
					break;
				}
			},
			/*Bind context menu for every node when it's DOM element is created.
			  We do it here, so we can also bind to lazy nodes, which do not
			  exist at load-time. (abeautifulsite.net menu control does not
			  support event delegation)*/
			onCreate: function(node, span){
			//alert(" onCreate Method is calling "); 
			 	node.expand(false);
			 	//$("#"+node.data.key).append("<li>Gagan</li>");
			 	//alert("Hi");
				bindContextMenu(span);
			},
			onFocus: function(node) {
				$("#echoFocused").text(node.data.title);
			},
			onBlur: function(node) {
				$("#echoFocused").text("-");
			},
			/*Load lazy content (to show that context menu will work for new items too)*/
			onLazyRead: function(node){
				node.appendAjax({
					url: "contextmenu/sample-data2.json"
				});
			},
		/* D'n'd, just to show it's compatible with a context menu.
		   See http://code.google.com/p/dynatree/issues/detail?id=174 */
		dnd: {
			preventVoidMoves: true, // Prevent dropping nodes 'before self', etc.
			onDragStart: function(node) {
				return true;
			},
			onDragEnter: function(node, sourceNode) {
				if(node.parent !== sourceNode.parent)
					return false;
				return ["before", "after"];
			},
			onDrop: function(node, sourceNode, hitMode, ui, draggable) {
				sourceNode.move(node, hitMode);
			}
		}
		});
		

/*----------------- Events  Start Here -------------------------------------------------*/	
		$(function(){
	var isMac = /Mac/.test(navigator.platform);
	$("#tree").dynatree({
		title: "Event samples",
		onClick: function(node, event) {
			if( event.shiftKey ){
				editNode(node);
				return false;
			}
		},
		onDblClick: function(node, event) {
			editNode(node);
			return false;
		},
		onKeydown: function(node, event) {//-----------------
			if( $(".contextMenu:visible").length > 0 )
					return false;

				switch( event.which ) {
				// Open context menu on [Space] key (simulate right click)
				case 32: // [Space]
					$(node.span).trigger("mousedown", {
						preventDefault: true,
						button: 2
						})
					.trigger("mouseup", {
						preventDefault: true,
						pageX: node.span.offsetLeft,
						pageY: node.span.offsetTop,
						button: 2
						});
					return false;

				// Handle Ctrl-C, -X and -V
				case 67:
					if( event.ctrlKey ) { // Ctrl-C
						copyPaste("copy", node);
						return false;
					}
					break;
				case 86:
					if( event.ctrlKey ) { // Ctrl-V
						copyPaste("paste", node);
						return false;
					}
					break;
				case 88:
					if( event.ctrlKey ) { // Ctrl-X
						copyPaste("cut", node);
						return false;
					}
					break;
				
				case 113: // [F2]
				editNode(node);
				return false;
				case 13: // [enter]
					if( isMac ){
						editNode(node);
						return false;
					}
				}
				
			
			/*
			switch( event.which ) {
			case 113: // [F2]
				editNode(node);
				return false;
			case 13: // [enter]
				if( isMac ){
					editNode(node);
					return false;
				}
			}*/ //======== Function End ============
		}
		//---------------
		
	});
});

/*----------------- Events  End Here -------------------------------------------------*/		
	});
	
	
	
	
	
</script>

<script type="text/javascript">
	/*
	$(function(){
	//alert(" btn click ")
		// Initialize the tree inside the <div>element.
		// The tree structure is read from the contained <ul> tag.
		$("#tree").dynatree({
			title: "Programming Sample",
			onActivate: function(node) {
				$("#echoActive").text(node.data.title);
//				alert(node.getKeyPath());
				if( node.data.url )
					window.open(node.data.url, node.data.target);
			},
			onDeactivate: function(node) {
				$("#echoSelected").text("-");
			},
			onFocus: function(node) {
				$("#echoFocused").text(node.data.title);
			},
			onBlur: function(node) {
				$("#echoFocused").text("-");
			},
			onLazyRead: function(node){
				var fakeJsonResult = [
					{ title: 'Lazy node 1', isLazy: true },
					{ title: 'Simple node 2', select: true }
				];
//				alert ("Let's pretend we're using this AJAX response to load the branch:\n " + jsonResult);
				function fakeAjaxResponse() {
					return function() {
						node.addChild(fakeJsonResult);
						// Remove the 'loading...' status:
						node.setLazyNodeStatus(DTNodeStatus_Ok);
					};
				}
				window.setTimeout(fakeAjaxResponse(), 1500);
			}
		});

		$("#btnAddCode").click(function(){
			// Sample: add an hierarchic branch using code.
			// This is how we would add tree nodes programatically
			var rootNode = $("#tree").dynatree("getRoot");
			var childNode = rootNode.addChild({
				title: "Programatically addded nodes",
				tooltip: "This folder and all child nodes were added programmatically.",
				isFolder: true
			});
			childNode.addChild({
				title: "Document using a custom icon",
				icon: "customdoc1.gif"
			});
		});

		$("#btnAddObject").click(function(){
			// Sample: add an hierarchic branch using an array
			var obj = [
				{ title: 'Lazy node 1', isLazy: true },
				{ title: 'Lazy node 2', isLazy: true },
				{ title: 'Folder node 3', isFolder: true,
					children: [
						{ title: 'node 3.1' },
						{ title: 'node 3.2',
							children: [
								{ title: 'node 3.2.1' },
								{ title: 'node 3.2.2',
									children: [
										{ title: 'node 3.2.2.1' }
									]
								}
							]
						}
					]
				}
			];
			$("#tree").dynatree("getRoot").addChild(obj);
		});

		$("#btnActiveNode").click(function(){
			$("#tree").dynatree("getTree").activateKey("id4.3.2");
//			$("#tree").dynatree("getTree").getNodeByKey("id4.3.2").activate();
		});
		$("#btnSetTitle").click(function(){
			var node = $("#tree").dynatree("getActiveNode");
			if( !node ) return;
			node.setTitle(node.data.title + ", " + new Date());
			// this is a shortcut for
			// node.fromDict({title: node.data.title + new Date()});
		});
		$("#btnFromDict").click(function(){
			var node = $("#tree").dynatree("getActiveNode");
			if( !node ) return;
			// Set node data and - optionally - replace children
			node.fromDict({
				title: node.data.title + new Date(),
				children: [{title: "t1"}, {title: "t2"}]
			});
		});

		$("#btnShowActive").click(function(){
			var node = $("#tree").dynatree("getActiveNode");
			if( node ){
				alert("Currently active: " + node.data.title);
			}else{
				alert("No active node.");
			}
		});

		$("#btnDisable").toggle(function(){
				$("#tree").dynatree("disable");
				$(this).text("Enable");
				return false;
			}, function(){
				$("#tree").dynatree("enable");
				$(this).text("Disable");
				return false;
			});
		$("#btnToggleExpand").click(function(){
			$("#tree").dynatree("getRoot").visit(function(node){
				node.toggleExpand();
			});
			return false;
		});
		$("#btnCollapseAll").click(function(){
			$("#tree").dynatree("getRoot").visit(function(node){
				node.expand(false);
			});
			return false;
		});
		$("#btnExpandAll").click(function(){
			$("#tree").dynatree("getRoot").visit(function(node){
				node.expand(true);
			});
			return false;
		});
		$("#btnSortActive").click(function(){
			var node = $("#tree").dynatree("getActiveNode");
			// Custom compare function (optional) that sorts case insensitive
			var cmp = function(a, b) {
				a = a.data.title.toLowerCase();
				b = b.data.title.toLowerCase();
				return a > b ? 1 : a < b ? -1 : 0;
			};
			node.sortChildren(cmp, false);
		});
		$("#btnSortAll").click(function(){
			var node = $("#tree").dynatree("getRoot");
			node.sortChildren(null, true);
		});
	});
	*/
</script>



	<!-- Definition of context menu -->
	<ul id="myMenu" class="contextMenu">
		<li><a href="#create"><span class='icon-folder-open  icon-large  marginleft20'></span> <spring:message code="lnkCret"/></a></li>
		<li><a href="#edit"><span class='icon-edit  icon-large iconcolor  marginleft20'></span> <spring:message code="lnkReNename"/></a></li>
		<li><a href="#cut"><span class='icon-cut  icon-large iconcolor  marginleft20'></span> <spring:message code="lnkCut"/></a></li>
		<li><a href="#copy"><span class='icon-copy  icon-large  iconcolor marginleft20'></span> <spring:message code="lnkCpy"/></a></li>
		<li><a href="#paste"><span class='icon-paste  icon-large  iconcolor marginleft20'></span> <spring:message code="lnkPa"/></a></li>
		<li><a href="#delete"><span class='icon-remove-sign  icon-large  iconcolor marginleft20'></span> <spring:message code="lnkDlt"/></a></li>
		<%-- <li class="quit separator"><a href="#quit">Quit</a></li>--%>
	</ul>
	
		<div class=' span12' style="color:red;" id='errordeletetreediv' > </div>
		<div class='divErrorMsg span12' id='errortreediv' ></div>
		

	<!-- Definition tree structure Gagan ${name} ---------- Tree  -->
	<div  style="border:0px solid red;width:auto;" class="span2">
	<div id="tree">
		${tree}
		<input type="hidden" id="pageFlag" name="pageFlag" value="${param.pageFlag}"/>
		<input type="hidden" id="frame_folderId" name="frame_folderId" value=""/>
		<div class="hide">Selected node: <span id="echoActivated">-</span></div>
		<div class="hide">Focused node: <span id="echoFocused">-</span></div>
		<input type="hidden" id="iframeFolderId" name="iframeFolderId" value=""/>
		<%---
		<c:if test="${param.pageFlag}==0">
		<div id="savedCGgridMyFolder">
				
		</div>
		</c:if>
		 --%>
	</div> 
	</div>
	<!-- End_Exclude -->
	<script>
	displayCandidateGridOfHomeFolder();
	$('#createIcon').tooltip();
	$('#renameIcon').tooltip();
	$('#cutIcon').tooltip();
	$('#copyIcon').tooltip();
	$('#pasteIcon').tooltip();
	$('#deleteIcon').tooltip();
	//$('#deleteFolder').modal('show');
	
	</script>

