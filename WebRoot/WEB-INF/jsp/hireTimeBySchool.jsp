<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>

<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" /> 
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type='text/javascript' src="js/hireTimeBySchool.js?ver=${resourceMap['js/hireTimeBySchool.js']}"></script>
<script type="text/javascript" src="dwr/interface/HireTimeBySchoolAjax.js?ver=${resourceMap['HireTimeBySchoolAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/ManageJobOrdersAjax.js?ver=${resourceMap['ManageJobOrdersAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/CandidatejobstatusAjax.js?ver=${resourceMap['CandidatejobstatusAjax.ajax']}"></script>

<script src="calender/js/jscal2.js"></script>
<script src="calender/js/lang/en.js"></script>
<link rel="stylesheet" type="text/css" href="calender/css/jscal2.css" />
<link rel="stylesheet" type="text/css"
	href="calender/css/border-radius.css" />
<link rel="stylesheet" type="text/css" href="calender/css/gold/gold.css" />
<script>
var $j=jQuery.noConflict();
$j(document).ready(function() {
});


function applyScrollOnTblJobList()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblGridJobList').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 400,
        width: 945,
        minWidth: null,
        minWidthAuto: false,
        colratio:[160,160,75,170,160,80,140],
        //colratio:[135,70,160,100,110,150,90,130],
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });	
}

</script>

<style>
.pdfDivBorder
{
z-index: 5000;
border: 1px solid #999;
border: 1px solid rgba(0, 0, 0, 0.3);
-webkit-border-radius: 6px;
-moz-border-radius: 6px;
border-radius: 6px;
-webkit-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
-moz-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
-webkit-background-clip: padding-box;
-moz-background-clip: padding-box;
background-clip: padding-box; 
}
</style>

<div class="row" style="margin:0px;">
         <div style="float: left;">
         	<img src="images/districtjobs-orders.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;"><spring:message code="lblHireTimebySchool"/><!--Hire Time by School Job--></div>	
         </div>			
		 
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
</div>
<br>


          <div class="row">
		       <div class="col-sm-6 col-md-6">
				     <label id="captionDistrictOrSchool"><spring:message code="lblDistrictName"/></label>
				        <c:if test="${DistrictOrSchoolName==null}">
					       <span>
					        <input type="text" maxlength="255" id="districtORSchoolName" name="districtORSchoolName" class="help-inline form-control"
					            		 onfocus="getDistrictORSchoolAuto(this, event, 'divTxtShowData', 'districtORSchoolName','districtOrSchoolId','');chkschoolBydistrict();"
													onkeyup="getDistrictORSchoolAuto(this, event, 'divTxtShowData', 'districtORSchoolName','districtOrSchoolId','');chkschoolBydistrict();"
													onblur="hideDistrictMasterDiv(this,'districtOrSchoolId','divTxtShowData');chkschoolBydistrict();getJobcategoryList2();"/><!--getStataussList();-->
							 </span>
							 <input type="hidden" id="districtOrSchooHiddenlId"/>							
				         </c:if>
				    		
							<c:if test="${DistrictOrSchoolName!=null}"> 
							<span>
					        <input type="text" maxlength="255"  class="help-inline form-control" value="${DistrictOrSchoolName}"  disabled="disabled"/>
							 </span>
				             		
				             	
				             	<input type="hidden" id="districtOrSchooHiddenlId" value="${DistrictOrSchoolId}"/>
				             	<input type="hidden" id="districtORSchoolName" name="districtORSchoolName"/>
				             	<input type="hidden" id="districtHiddenlIdForSchool"/>
				            </c:if>
				             <c:if test="${DistrictOrSchoolName==null && DistrictOrSchoolId!=null}">
				              	<input type="hidden" id="districtHiddenlIdForSchool" value="${DistrictOrSchoolId}"/>
				              </c:if>
				               <c:if test="${DistrictOrSchoolName==null && DistrictOrSchoolId==null}">
				              	<input type="hidden" id="districtHiddenlIdForSchool"/>
				              </c:if>
				       		 <!--<input type="hidden" id="JobOrderType" value="${JobOrderType}"/>-->
				        	 <div id='divTxtShowData'  onmouseover="mouseOverChk('divTxtShowData','districtORSchoolName')" style=' display:none;position:absolute;z-index:5000;' class='result' ></div>	
							             	
               </div>
            
	      
			          <c:choose>
			              <c:when  test="${DistrictOrSchoolName==null}">
			               <div class="col-sm-6 col-md-6">
			           </c:when>
		                 <c:otherwise>
		                   <div class="col-sm-6 col-md-6">
		                 </c:otherwise>
		              </c:choose> 
			          <label><spring:message code="lblSchoolName"/><!--School Name--></label>
			            <c:choose>
						    <c:when test="${userMaster.entityType eq 3 }">
						      <!--<br/> ${schoolName}
						       	--><input type="hidden" id="schoolName" name="schoolName" value="${schoolName}"/>
	         					<input type="hidden" id="schoolId" name="schoolId" value="${schoolId}"/> 
	         					<c:if test="${not empty schoolName }"> 
							<span>
					        <input type="text" id="schoolName" maxlength="100"
									name="schoolName" class="form-control" placeholder="" value="${schoolName}" disabled="disabled" />
				            </c:if>
						    </c:when>
						    <c:otherwise>
								<input type="text" id="schoolName" maxlength="100"
									name="schoolName" class="form-control" placeholder=""
									onfocus="getSchoolAuto(this, event, 'divTxtShowData2', 'schoolName','districtOrSchoolId','');"
									onkeyup="getSchoolAuto(this, event, 'divTxtShowData2', 'schoolName','districtOrSchoolId','');"
									onblur="hideSchoolMasterDiv(this,'districtOrSchoolId','divTxtShowData2');getJobcategoryList2();" 
								/>	
								<input type="hidden" id="schoolName" name="schoolName"/>
	         					<input type="hidden" id="schoolId" name="schoolId" value="0"/> 
	         					<script>getJobcategoryList2()</script>
						    </c:otherwise>
						</c:choose>
						<div id='divTxtShowData2'  onmouseover="mouseOverChk('divTxtShowData2','schoolName')" style=' display:none;position:absolute;z-index:5000;' class='result' ></div>
			        </div>
			          
	
         </div>

  <div class="row">
			 <div class="col-sm-3 col-md-3">
			    <label><spring:message code="lblJobCat"/><!--Job Category--></label>
			    <select id="jobCategoryId" class="form-control" name="jobCategoryId" >
		       			<c:if test="${userMaster.entityType eq 3 }">
		       			<option style='width:10px;' value=''>All</option>
						<c:forEach var="jobCategory" items="${jobCategoryMasterlst}">
							<option style='width:10px;' value="${jobCategory.jobCategoryId}">${jobCategory.jobCategoryName}</option>
						</c:forEach></c:if></select>
			  </div>
			  
			  <div class="col-sm-3 col-md-3">
			    <label><spring:message code="lblJobIDorJobTitle"/><!--Job ID or Job Title--></label>
		         <input id="jobOrderId" name="jobOrderId" maxlength="100" class="help-inline form-control" type="text" /><!--onkeypress="return checkForInt1(event);"-->
			  </div>
			  
			<div class="col-sm-3 col-md-3 top25-sm2" style="width: 150px;">
		       <label class=""></label>
		        <button class="btn btn-primary " type="button" onclick=" searchByButtonJobOrder();">&nbsp;<spring:message code="lnkSearch"/>&nbsp;&nbsp; <i class="icon"></i></button>
		   </div>
  </div>

<div style="display:none;" id="loadingDiv">
    <table  align="center">
 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
	</table>
	</div>
    
              <div id="" class="hide" style="height: 480px; width: 100%; overflow: auto;'-webkit-overflow-scrolling' : 'touch">
			 <iframe src="" id="ifrmTrans" width="100%" height="100%" scrolling="no">
			 </iframe>  
		  </div> 
    
    <input type="hidden" id="entityType" name="entityType" value="${userMaster.entityType}">
    <div style="clear: both;"></div>
     <br/>
   
<!-- PDF -->
<div  class="modal hide pdfDivBorder"  id="modalDownloadJobList" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog-for-cgpdfreport">
	<div class="modal-content"  style="background-color: #ffffff;">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><spring:message code="btnX"/></button>
		<h3 id="myModalLabel"><spring:message code="lblHireTimebySchool"/></h3>
	</div>
		<div class="modal-body">		
			<div class="control-group">
				    <div style="width: 100%; height: 100%; -webkit-overflow-scrolling: touch;">
	                  <iframe src="" id="ifrmCJS" width="100%" height="480px">
	               </iframe>        	
				</div>
			</div>
	 	</div>
	 	<div class="modal-footer">
	 		<button class="btn" data-dismiss="modal" aria-hidden="true" ><spring:message code="btnClose"/></button> 		
	 	</div>
     </div>
 	</div>
</div>

<!-- PDF End -->      
   
   <div class="row"> 
	         <div class="col-sm-6 col-md-6">
       </div>
        <div class="col-sm-6 col-md-6">
         <table class="a" align="right">
		      <tr>
		         <td  style='padding-left:5px;'>
					<a data-original-title='Export to Excel' rel='tooltip' id='exlId' href='javascript:void(0);'  onclick="generateJobListExcel();"><span class='icon-file-excel icon-large iconcolor'></span></a>
				 </td>
				 <td>
				  <a data-original-title='Export to PDF' rel='tooltip'  href="javascript:void(0);" id='pdfId' onclick='generateJobListPDF()'><span class='pdf-eoc icon-large iconcolor'></span></a>
				 </td>
				 <td>
					<a data-original-title='Print' rel='tooltip' id='printId' href='javascript:void(0);' onclick="generateJobListPrintPre();"><span class='icon-print icon-large iconcolor'></span></a>
				 </td>  
			</tr>
		</table>
        </div>
       	
   </div> 
    
        
         <div class="TableContent">  	
             <div class="table-responsive" id="divMainOfJobList" >    
           </div>       
         </div>

 <div class="modal hide"  id="printmessage1"  tabindex="-1" role="dialog" data-keyboard="false" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" >
	<div class="modal-dialog" style="width:450px; margin-top: 0px;">
	<div class="modal-content">
	<div class="modal-header">
	   	<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="canelPrint();"><spring:message code="btnX"/></button>
	   <h3 id="myModalLabel"><spring:message code="headTm"/></h3>
	</div>
	
	<div class="modal-body" style="max-height: 400px; overflow-y: auto;"> 		
			<div class="control-group">
			<div class="" >
			<spring:message code="msgPlzPopAreAlloued"/>
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
	 	<table border=0 align="right">
	 		<tr>
		 		<td  nowrap>
		 			<button class="btn btn-large" data-dismiss="modal" aria-hidden="true" onclick='canelPrint();'><spring:message code="btnOk"/></button>
				</td>
				
	 		</tr>
	 	</table>
	</div>
  </div>
 </div>
</div>





<script type="text/javascript">
	//$('#chartId').tooltip();
 	$('#pdfId').tooltip();
  	$('#exlId').tooltip();
  	$('#printId').tooltip();
	chkschoolBydistrict();
	searchRecordsByEntityTypeJobList();
</script>

	        	
	 
