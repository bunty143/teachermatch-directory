<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<script type="text/javascript" src="dwr/interface/QuestionUploadTempAjax.js?ver=${resourceMap['QuestionUploadTempAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type="text/javascript" src="js/importquestion.js?ver=${resourceMap['js/importquestion.js']}"></script>
<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" />
<script type="text/javascript">
var $j=jQuery.noConflict();
        $j(document).ready(function() {
        });

function applyScrollOnTbl() {
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
		        $j('#tempQuestionTable').fixheadertable({ //table id 
		        caption: '',
		        showhide: false,
		        theme: 'ui',
		        height: 280,
		        width: 3940,
		        minWidth: null,
		        minWidthAuto: false,
		        colratio:[300,120,180,180,100,100,100,200,80,80,80,150,110,200,80,80,200,80,80,200,80,80,200,80,80,200,80,80,200,80,80], // table header width
		        addTitles: false,
		        zebra: true,
		        zebraClass: 'net-alternative-row',
		        sortable: false,
		        sortedColId: null,
		        //sortType:[],
		        dateFormat: 'd-m-y',
		        pager: false,
		        rowsPerPage: 10,
		        resizeCol: false,
		        minColWidth: 100,
		        wrapper: false
        	});
        });			
  }
</script>

<div class="row  mt10 mb">
	
	<div class="span16 centerline" style="width:3935px;">
		<div class="span1 m0"><img src="images/icon1.png" width="41" height="41" alt=""></div>
		<div class="span10 subheading"><spring:message code="headQueLi"/></div>
	</div>
</div>
<div class="row">
	<div class="mt10 span16" id="tempTeacherGrid">
	</div>
</div>

<script type="text/javascript">
	//DisplayTempTeacher();
	DisplayTempQuestion();
</script>
 <div class="row mt10">
	<div  class="span16"> 
		<div>
			<button class="btn btn-primary fl" type="button" onclick="return saveQuestion();"><spring:message code="btnApt"/> <i class="icon"></i></button>
		</div>
		<div class="span3">
			<button class="btn btn-primary fl" type="button" onclick="return tempTeacherReject('${sessionIdTxt}');"><spring:message code="btnRjct"/> <i class="icon"></i></button>
		</div>	
	</div>
</div> 
<div  class="modal hide"  id="myModalMsg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 5000;" data-backdrop="static">
	<div class='modal-dialog'>
		<div class='modal-content'>
			<div class="modal-header">
	  			<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick="return tempTeacher();"><spring:message code="btnX"/></button>
				<h3 id="myModalLabel"><spring:message code="headQues"/></h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<div class=""><span id="Msg"></span></div>
				</div>
	 		</div>
	 		<div class="modal-footer">
		 		<button class="btn" data-dismiss="modal" aria-hidden="true"  onclick="return tempTeacher();"><spring:message code="btnClose"/></button> 		
	 		</div>
 		</div>
 	</div>
</div>
<div style="display:none;" id="loadingDiv">
	<table  align="left" >
		<tr><td style="padding-top:270px;padding-left:450px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
		<tr><td style="padding-top:0px;padding-left:450px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
	</table>
</div>       