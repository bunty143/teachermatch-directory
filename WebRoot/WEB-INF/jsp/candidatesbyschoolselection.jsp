<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" /> 
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type='text/javascript' src="js/candidatesbyschoolselection.js?ver=${resourceMap['js/candidatesbyschoolselection.js']}"></script>
<script type="text/javascript" src="dwr/interface/JBServiceAjax.js?ver=${resourceMap['JBServiceAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/interface/CandidatesBySchoolSelectionAjax.js?ver=${resourceMap['CandidatesBySchoolSelectionAjax.Ajax']}"></script>

<script>
var $j=jQuery.noConflict();
$j(document).ready(function() {
});

function applyScrollOnTbl()
{
	    var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#selectedCandidateTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',     
        width: 940,    
        minWidth: null,
        minWidthAuto: false,     
        colratio:[320,494,200],
	    addTitles: false,
        zebra: true,       
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 20,
        resizeCol: false,

        wrapper: false
        });
            
        });			
}

function applyScrollOnPrintTable()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#selectedCandidatePrintPre').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 300,
         width: 1038,
        minWidth: null,
        minWidthAuto: false,
        colratio:[320,494,200],
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        wrapper: false
        });
            
        });	
}

</script>

<style>
.pdfDivBorder
{
z-index: 5000;
border: 1px solid #999;
border: 1px solid rgba(0, 0, 0, 0.3);
-webkit-border-radius: 6px;
-moz-border-radius: 6px;
border-radius: 60px;
-webkit-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
-moz-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
-webkit-background-clip: padding-box;
-moz-background-clip: padding-box;
background-clip: padding-box; 
}

</style>

<div class="row" style="margin:0px;">
         <div style="float: left;">
         	<img src="images/districtjobs-orders.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;"><spring:message code="headSlctdSchlByCandRept"/> </div>	
         </div>	 
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
</div>
<br>
<div id="errordiv" style="color:red;"></div>
         <div class="row">
			 	<div class="col-sm-4 col-md-4">
			 	 <label>
				<spring:message code="lblDistrictName"/>
				  </label>
 				<input  type="text" 
				class="form-control"
				maxlength="100"
				id="districtName" 
				value="${DistrictOrSchoolName}"
				disabled="disabled"
				name="districtName" 
				onfocus="getDistrictMasterAutoComp(this, event, 'divTxtDistrictData', '${DistrictOrSchoolName}','${DistrictOrSchoolId}','');showSchool(); "
				onkeyup="getDistrictMasterAutoComp(this, event, 'divTxtDistrictData', '${DistrictOrSchoolName}','${DistrictOrSchoolId}','');showSchool();" 
				onblur="hideDistrictMasterDiv(this,'${DistrictOrSchoolId}','divTxtDistrictData');showSchool();"/>
				<input  type="hidden" id="districtId" name="districtId" value="${DistrictOrSchoolId}">
				<div id='divTxtDistrictData' style=' display:none;position:absolute; z-index: 2000;' 
				onmouseover="mouseOverChk('divTxtDistrictData','${DistrictOrSchoolName}')" class='result' ></div>
			    </div>
			     <div class="col-sm-4 col-md-4">
			 	 <label>
				   <spring:message code="lblSchoolName"/>
				  </label>
				  <input  type="hidden" id="schoolId1" name="schoolId1" value="">
					<input  type="text" 
					class="form-control"					
					maxlength="100"
					id="schoolName1" 					
					autocomplete="off"
					value=""
					name="schoolName1" 
					onfocus="getSchoolMasterAutoComp(this, event, 'divTxtShowData4', 'schoolName1','schoolId11','districtId','');"
					onkeyup="getSchoolMasterAutoComp(this, event, 'divTxtShowData4', 'schoolName1','schoolId','districtId','');" 
					onblur="hideSchoolMasterDiv(this,'schoolId1','divTxtShowData4');"/>
					<div id='divTxtShowData4' style=' display:none;position:absolute; z-index: 2000;' 
					onmouseover="mouseOverChk('divTxtShowData4','schoolName1')" class='result' ></div>
			    </div>			   
			   <div class="col-sm-2 col-md-2 top25">
		        <label class=""></label>
		        <button class="btn btn-primary" type="button" style="height: 32px;" onclick="displayCandidates();">&nbsp;&nbsp;<spring:message code="btnSearch"/>&nbsp;&nbsp; <i class="icon"></i></button>
		   </div>		    
		    </div>				
       <div class="row" style="margin-top:20px;">
	         <div class="col-sm-6 col-md-6">
	          </div>
        <div class="col-sm-6 col-md-6">
         <table class="marginrightForTablet">
		      <tr>
		         <td  style='padding-left:5px;'>
					<a data-original-title='Export to Excel' rel='tooltip' id='exlId' href='javascript:void(0);'  onclick='generateSelectedCandidateReportExcel();'><span class='icon-file-excel icon-large iconcolor'></span></a>
				 </td>
				 <td>
				  <a data-original-title='Export to PDF' rel='tooltip'  href="javascript:void(0);" id='pdfId' onclick='generateSelectedSchoolByCandidatePdf();'><span class='pdf-eoc icon-large iconcolor'></span></a>
				 </td>
				 <td>
					<a data-original-title='Print Preview' rel='tooltip' id='printId' href='javascript:void(0);' onclick="generateSelectedSchoolByCandidatePrintPre();"><span class='icon-print icon-large iconcolor'></span></a>
				 </td>  
			</tr>
		</table>
        </div>       	
   </div>   
           <div style="clear: both;"></div>           
     <!-- PDF -->
<div  class="modal hide pdfDivBorder"  id="modalDownloadSelectedSchoolByCandidate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog-for-cgpdfreport">
	<div class="modal-content"  style="background-color: #ffffff;">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel"><spring:message code="msgSchlSlctByCandRept"/> </h3>
	</div>
		<div class="modal-body">		
			<div class="control-group">
				    <div style="width: 100%; height: 100%; -webkit-overflow-scrolling: touch;">
	                  <iframe src="" id="ifrmSSBC" width="100%" height="480px">
	               </iframe>        	
				</div>
			</div>
	 	</div>
	 	<div class="modal-footer">
	 		<button class="btn" data-dismiss="modal" aria-hidden="true" ><spring:message code="btnClose"/></button> 		
	 	</div>
     </div>
 	</div>
</div>

<!-- PDF End -->      
           
    <!-- printing Start  -->
    
  <div style="clear: both;"></div>

   <div class="modal hide"  id="printSelectedSchoolByCandidateDiv"  tabindex="-1" role="dialog" data-keyboard="false" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" >
	<div class="modal-dialog" style="width:1100px; margin-top: 0px;">
	<div class="modal-content">
	<div class="modal-header">
	   	<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="cancelPrint();">x</button>
	   	<h3 id=""><spring:message code="headPrintPreview"/></h3>
	</div>
	
	<div class="modal-body" style="max-height: 500px; overflow-y: auto;"> 		
			<div class="" id="printSelectedSchoolByCandidateDataTableDiv"  >          
             </div>
 	</div>
 	<div class="modal-footer">
	 	<table border=0 style="margin-left:850px;">
	 		<tr>
		 		<td  nowrap>
		 			<button class="btn  btn-large btn-primary" aria-hidden="true" onclick='printSelectedSchoolByCandidateData();'>&nbsp;&nbsp;&nbsp;<spring:message code="btnP"/>&nbsp;&nbsp;&nbsp;</button>&nbsp;&nbsp;
				</td>
				<td  nowrap>
		 			<button class="btn btn-large" data-dismiss="modal" aria-hidden="true" onclick='cancelPrint();'><spring:message code="btnClr"/></button>
				</td>
	 		</tr>
	 	</table>
	</div>
  </div>
 </div>
</div>
    
    
     <div style="clear: both;"></div>

   <div class="modal hide"  id="errDateCheckDiv"  tabindex="-1" role="dialog" data-keyboard="false" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" >
	<div class="modal-dialog" style="width:450px; margin-top: 0px;">
	<div class="modal-content">
	<div class="modal-header">
	   	<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="canelPrint();">x</button>
	   	<h3 id=""><spring:message code="headArt"/></h3>
	</div>
	
	<div class="modal-body" style="max-height: 400px; overflow-y: auto;"> 		
			<div class="" id="errDateMsg">          
             </div>
 	</div>
 	<div class="modal-footer">
	 	<table border=0 align="right">
	 		<tr>
		 		<td  nowrap>
		 			<button class="btn btn-large" data-dismiss="modal" aria-hidden="true" onclick='canelPrint();'><spring:message code="btnOk"/></button>
				</td>
				
	 		</tr>
	 	</table>
	</div>
  </div>
 </div>
</div>   
    <!-- printing End -->       
       <div class="TableContent">        	
            <div class="table-responsive" id="divData">          
            </div>            
        </div> 	

<div style="display: none; z-index: 5000;" id="loadingDiv">
	<table align="center">
		<tr>
			<td style="padding-top: 270px;" align="center">
				<!-- <img src="images/please.jpg"/> -->
			</td>
		</tr>
		<tr>
			<td style="padding-top: 0px;" align="center">
				<img src="images/loadingAnimation.gif" />
			</td>
		</tr>
		<!-- <tr id='paymentMessage'><td style='padding-top:0px;padding-left:450px;' id='spnMpro' align='center'>Your report is being generated...</td></tr> -->
	</table>
</div>
<div id="" class="hide" style="height: 480px; width: 100%; overflow: auto;'-webkit-overflow-scrolling' : 'touch">
			 <iframe src="" id="ifrmTrans" width="100%" height="100%" scrolling="no">
			 </iframe>  
		  </div> 
<script type="text/javascript">
  $('#chartId').tooltip();
  $('#pdfId').tooltip();
  $('#exlId').tooltip();
  $('#printId').tooltip();
</script>


<script type="text/javascript">
displayCandidates();
</script>

	        	
	 
