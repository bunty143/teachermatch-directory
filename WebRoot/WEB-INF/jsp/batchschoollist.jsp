 <!-- @Author: Sekhar 
 * @Discription: view of School List
 -->
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>  
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>

<script type="text/javascript" src="dwr/interface/BatchJobOrdersAjax.js?ver=${resourceMap['BatchJobOrdersAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type='text/javascript' src="js/batchjoborder.js?ver=${resourceMap['js/batchjoborder.js']}"></script>
<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" /> 

<script>
var $j=jQuery.noConflict();      
$j(document).ready(function() {
});
function applyScrollOnTbl()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 400,
        width: 975,
        minWidth: null,
        minWidthAuto: false,
	    colratio:[78,90,255,257,120,78,100],
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}

</script>
 <style>
.hide
	{
		display: none;
	}
</style>  
<div class="container">
	<div class="row mt10">
		<div class="span16 centerline">
			<div class="span1 m0"><img src="images/manageusers.png" width="41" height="41"></div>
			<div class="span10 subheading">
			<c:if test="${type==1}"><spring:message code="lblAcceptedSclLst"/></c:if>
			<c:if test="${type==2}"><spring:message code="headSchL"/></c:if>
			( Job Order : ${BatchJobTitle} )
			</div>
			<div class="span3 pull-right add-employment1"></div>
		</div>
	<div class="clearfix"></div>
	</div>
	<div class="row">
		<div id="divMain" class="mt10 span16">
		</div>
	 <br><br>
	</div>
	<div style="display:none;" id="loadingDiv">
		<table  align="left" >
			<tr><td style="padding-top:270px;padding-left:450px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
			<tr><td style="padding-top:0px;padding-left:450px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
		</table>
	</div>
</div>
<script>
displaySchoolList(${batchJobId},${type});
</script>
           
