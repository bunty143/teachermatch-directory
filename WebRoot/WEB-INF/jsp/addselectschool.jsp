<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<script type="text/javascript" src="dwr/interface/SchoolSelectionAjax.js?ver=${resourceMap['SchoolSelectionAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type="text/javascript" src="js/teacher/addselectschool.js?ver=${resourceMap['js/teacher/addselectschool.js']}"></script>

<script type="text/javascript">
var $j=jQuery.noConflict();
$j(document).ready(function(){});
function applySelectedScrolOnTbl()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblGridSelectedSchoolByJob').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 250,
        width: 945,
        minWidth: null,
        minWidthAuto: false,
        colratio:[261,125,270,165,150], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}
</script>

<!-- End ... Dynamic Portfolio -->

<link rel="stylesheet" type="text/css" href="css/base.css" />  
<!-- add by ankit -->
<div  class="modal hide"  id="myID" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
      <div class='modal-dialog'>
      <div class='modal-content'>
      <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="hideProfile()"><spring:message code="btnX"/></button>
            <h3 id="myModalLabel"><spring:message code="lblMsg"/></h3>
      </div>
      <div class="modal-body">            
            
      </div>
      <div class="modal-footer">
            <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true"><spring:message code="btnOk"/></button> 
      </div>
  </div>
 </div>
</div>

	<div class="row" style="margin-left: 0px;margin-right: 0px;margin-top:">
	     <div style="float: left;">
	     	<img src="images/applyfor-job.png" width="41" height="41" alt="">
	     </div>        
	     <div style="float: left;">
	     	<div class="subheading" style="font-size: 13px;"><spring:message code="lblSchlConsidered"/></div>	
	     </div>	
	      <div class="pull-right add-employment1">
			<a href="javascript:void(0);" onclick="return addAndEditSchool();"><spring:message code="lnkAddSchl"/></a>
		</div>	
			
		<div style="clear: both;"></div>	
		<div class="centerline"></div>
	</div>
	
	 <div class="TableContent top15">        	
	     <div class="table-responsive" id="divSelectedSchoolList"></div>            
	</div> 
	</br>
	<div id="selectedSchoolDiv" style="display:none;">
	<div class="row">
		<div class="col-sm-5 col-md-5" style="border: 0px solid red;">
			<b><spring:message code="pSltSchlToConsider"/></b>
		</div>
	</div>	
	<div class="row">
		<div class="col-sm-9 col-md-9">
     			<div class='divErrorMsg' id='errselectschool' style="display: block;"></div>
		</div>
		
	</div> 
	<div class="row">
			 
	 <div class="col-sm-5 col-md-5" style="border: 0px solid red;">
	 		<label><spring:message code="lblSltSchl"/><span class="required">*</span></label>
	 		<div id="1stSchoolDiv">
				<select id="lstSchoolMaster" name="lstSchoolMaster" class="form-control"> 
				</select>
			</div>
		</div>	
		<input type="hidden" id="jobId" name="jobId" value="${jobId}"/>	
		<div class="col-md-5 col-md-5" style="border: 0px solid green;">
			<iframe id='uploadletterOfIntentID' name='uploadFrameletterOfIntent' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'>
				</iframe>
				<form class="span15 textfield11" id="letterOfIntentForm" name="letterOfIntentForm" enctype='multipart/form-data' method='post' target='uploadFrameletterOfIntent' action='letterOfIntentUploadServlet.do'>
	
					<div class="col-sm-5 col-md-5">
					 <label><spring:message code="lblLtrOfIntent"/><span class="required">*</span></label>	
						<input type="hidden" id="schoolIdForletterOfIntent" name="schoolIdForletterOfIntent" value=""/>
						<input type="hidden" id="jobIdForletterOfIntent" name="jobIdForletterOfIntent" value="${jobId}"/>
						<input type="hidden" id="teacherIdForletterOfIntent" name="teacherIdForletterOfIntent" value="${teacherId}"/>
						<input id="letterOfIntent" name="letterOfIntent" type="file" width="20px;">
						<a href="javascript:void(0)" onclick="clearletterOfIntent()"><spring:message code="lnkClear"/></a>
					</div>
					<div class="col-sm-3 col-md-3" style="margin-top: 25px;" id="removeletterOfIntentSpan" name="removeletterOfIntentSpan" style="display: none;">
						<label>
							&nbsp;&nbsp;
						</label>
						<div id="divletterOfIntent">
						</div>
					</div>
		       </form>			
		</div>	
	  </div>
	  </br>
	  <div class="row">
		<div class="col-sm-4 col-md-4">
			<a class="idone" style="cursor: pointer; margin-left: 0px; text-decoration:none;" onclick="saveSelectedSchools()">
			<spring:message code="lnkImD"/>
			</a>&nbsp;&nbsp;
			<a class="idone" style="cursor: pointer; margin-left: 0px; text-decoration:none;"	onclick="cancelSchool()">
				<spring:message code="lnkCancel"/>
			</a>
		</div>
	</div>
	</div>
	
	<div style="display:none;" id="loadingDiv">
		<table  align="center">
				<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
				<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
				<tr id='paymentMessage'><td style='padding-top:0px;padding-left:450px;' id='spnMpro' align='center'></td></tr>
		</table>
	</div>
 	<div id="" class="hide" style="height: 480px; width: 100%; overflow: auto;'-webkit-overflow-scrolling' : 'touch">
			 <iframe src="" id="ifrmletterOfIntent" width="100%" height="100%" scrolling="no">
			 </iframe>  
	</div> 
<script type="text/javascript">
	getSelectedSchoolsByJob();
</script>

<script>
function fileContainsVirusDiv(msg)
{
	$('#loadingDiv').hide();
	$('#virusDivId .modal-body').html(msg);
	$('#myModalDymanicPortfolio').modal('hide');
	$('#virusDivId').modal('show');
}
</script>

<div class="modal hide" id="virusDivId" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog-for-cgcoverletter" style='width: 35%;'>
		<div class="modal-content">
			<div class="modal-header">
				<h3 id="myModalLabel"><spring:message code="headTm"/></h3>
			</div>
			<div class="modal-body" style="max-height: 400px;overflow-y:auto;">
				<div class="control-group">
				</div>
			</div>
			<div class="modal-footer">
				<span id=""><button class="btn btn-primary" data-dismiss="modal"><spring:message code="btnOk"/> <i class="icon"></i></button></span>&nbsp;&nbsp;
			</div>
		</div>
	</div>
</div>