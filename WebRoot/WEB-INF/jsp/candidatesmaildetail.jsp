<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>

<script type="text/javascript" src="dwr/interface/AdminDashboardAjax.js?ver=${resourceMap['AdminDashboardAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type='text/javascript' src="js/candidatesmaildetail.js?ver=${resourceMap['js/candidatesmaildetail.js']}"></script>
<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" />  

<script src="calender/js/jscal2.js"></script>
<script src="calender/js/lang/en.js"></script>
<link rel="stylesheet" type="text/css" href="calender/css/jscal2.css" />
<link rel="stylesheet" type="text/css" href="calender/css/border-radius.css" />
<link rel="stylesheet" type="text/css" href="calender/css/gold/gold.css" />

<script type="text/javascript">
var $j=jQuery.noConflict();
        $j(document).ready(function() {
        });
        
function applyScrollOnTbl()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 250,
        //width: 840,
         width: 945,
        minWidth: null,
        minWidthAuto: false,
		//colratio:[24,260,313,115,100,170], 982
		colratio:[127,100,200,200,220,145],
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}

 </script>
 <div class="row" style="margin-left: 0px;margin-right: 0px;margin-bottom: 13px;">
         <div style="float: left;">
         	<img src="images/manageusers.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;"><spring:message code="headCandMailDetails"/></div>	
         </div>
		
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
</div> 
<spring:message code="lblTchrName"/> ${teacherDetail1.firstName} ${teacherDetail1.lastName}<br/>
<spring:message code="lblEmail"/> : ${teacherDetail1.emailAddress}

<div class="TableContent top15">        	
           <div class="table-responsive" id="divMain">          
               	         
           </div>            
</div> 



<input type='hidden' id="teacherId" value="${teacherDetail1.teacherId}"/>
<script type="text/javascript">
getCandidatesMails();

</script>
<div class="modal hide" id="myModal1setMsg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
	<div class="modal-dialog-for-cgmessage">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel"><spring:message code="headViesMag"/></h3>
	</div>
	<div  class="modal-body">
		<div class="control-group">
			<div class='divErrorMsg' id='errordivDefaultMsg' style="display: none;"></div>
		</div>
		<div class="control-group">
			<div class="">
		    	<label><strong><spring:message code="lblSub"/></strong></label>
	        	<input id="defaultSubject" name="defaultSubject" type="text" class="form-control" maxlength="200" />
			</div>
		</div>
        <div class="control-group">
			<div class="span8" id="msgDash">
		    	<label><strong><spring:message code="lblMsg"/></strong></label>
		    	<label class="redtextmsg"><spring:message code="msgHowToCopyPastCutDouc"/></label>
	        	<textarea rows="5" class="span8" cols="" id="defaultMsg" name="defaultMsg" maxlength="1000"></textarea>
			</div>
		</div>
 		
 	</div>
 	
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClr"/></button>
 	</div>
</div>
 	</div>
</div>


