<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<link href="css/bootstrap3.css?ver=${resourceMap['css/bootstrap3.css']}" rel="stylesheet" type="text/css">
<link href="css/style3.css?ver=${resourceMap['css/style3.css']}" rel="stylesheet" type="text/css">	
<link href="css/quest.css?ver=${resourceMap['css/quest.css']}" rel="stylesheet" type="text/css">	
		
<div class="container">
<center style="margin-left: 10px;margin-right: 10px;">
<h1>
<b><spring:message code="msgyourEdudationJobSearch" /></b><BR/>
</h1>
<h1 class="font25" style="margin-top: 10px;">
<spring:message code="msgWelcomeToYour" /> <b><spring:message code="msgMyQuestDashboard" /></b> — <spring:message code="msgSecureLandingPage" /> <b><spring:message code="lblPwrFrof" />.</b> <spring:message code="msgPersonalPlanningFolder" />
</h1>

</div>

<div align="center" style="height: 400px; background-color: #E7EBEA; margin-top: 10px;">
<div class="container">
	<div style="padding: 30px 0px 0px 0px;" class="col-sm-3 col-md-3">
		<img src="images/search22.png" height="170" />
	</div>
	
	<div style="text-align: justify;" class="col-sm-9 col-md-9">
	<h1 class="font25">
		<B><spring:message code="msgAllTeacherJobSeeker" /></B><BR/>
		<spring:message code="msgTkYurCandStrengthOnYor" /> <b><spring:message code="lblProPwrTrack" /></b>
		<spring:message code="lblWhYuComplt" /> <a href="#"><spring:message code="lblTmEPI" /></a> <spring:message code="msgBigBoostTowardBecoming" />
	</h1>
</div>
</div>


<div class="container">
	<div class="col-sm-3 col-md-3">
		<img src="images/questconnect.png" /> <BR/>
			<label class="heading2"><spring:message code="lblQuestCot" /></label><BR/>
			<spring:message code="lblProfess" /><BR/>
			<spring:message code="lblPerspectivesAdSupt" />
	</div>

	<div class="col-sm-2 col-md-2">
		<img src="images/questacademy.png" /> <BR/>
		<label class="heading2"><spring:message code="lblQuestAcad" /></label><BR/>
		<spring:message code="lblRefAd" /><BR/>
		<spring:message code="lblLearnCentr" />
	</div>

	<div class="col-sm-3 col-md-3">
		<img src="images/yourmentor.png" /> <BR/>
		<label class="heading2"><spring:message code="lblMetYuMentor" /></label><BR/>
		<spring:message code="lblInsightfulGoTo" /><BR/>
		<spring:message code="lblIntrAd" />
	</div>

	<div class="col-sm-2 col-md-2">
		<img src="images/toolkitessential.png" /><BR/>
		<label class="heading2"><spring:message code="lblToolktEssent" /></label><BR/>
		<spring:message code="lblSmtTp" /><BR/>
		<spring:message code="lblToolsAdAids" />
	</div>

	<div class="col-sm-2 col-md-2">
		<img src="images/intheque.png" /><BR/>
		<label class="heading2"><spring:message code="lblInTheQ" /></label><BR/>
		<spring:message code="lblLifeStl" /><BR/>
		<spring:message code="lblPerspect" />
	</div>
</div>

</div>



<div class="container">
	<div style="padding: 30px 0px 0px 0px;" class="col-sm-12 col-md-12" align="center">
		<img src="images/test-powersection1.png" />
	</div>
	
</div>




