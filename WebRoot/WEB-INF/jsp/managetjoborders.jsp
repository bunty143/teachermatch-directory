<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="tm.bean.user.UserMaster"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page import="tm.utility.Utility" %>

<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<script type="text/javascript" src="dwr/interface/ManageJobOrdersAjax.js?ver=${resourceMap['ManageJobOrdersAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/interface/BatchJobOrdersAjax.js?ver=${resourceMap['BatchJobOrdersAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/interface/ApproveJobAjax.js?ver=${resourceMap['ApproveJobAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type='text/javascript' src="js/managejoborders.js?ver=${resourceMap['js/managejoborders.js']}"></script>
<script type="text/javascript" src="dwr/interface/DWRAutoComplete.js?ver=${resourceMap['DWRAutoComplete.Ajax']}"></script>
<script type="text/javascript" src="js/jquery-1.4.1.js?ver=${resourceMap['']}"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js?ver=${resourceMap['']}"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" />
<script type='text/javascript' src="js/certtypeautocomplete.js?ver=${resourceMap['js/certtypeautocomplete.js']}"></script>
<script type='text/javascript' src="js/subjectmasterAutoComplete.js?ver=${resourceMap['js/subjectmasterAutoComplete.js']}"></script>
<script type="text/javascript" src="dwr/interface/GeoZoneAjax.js?ver=${resourceMap['GeoZoneAjax.Ajax']}"></script>
<script>

var $j=jQuery.noConflict();
$j(document).ready(function() {
});
function applyScrollOnTbl()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        //height: 400,
        width: 945,
        minWidth: null,
        minWidthAuto: false,
        //colratio:[55,266,70,90,100,105,70,66,151], // table header width
        //mukesh to set the table
        //colratio:[65,130,60,70,65,90,100,50,75,75,170],
       //colratio:[55,110,60,60,55,90,60,90,50,75,75,170],
        <c:choose>
           <c:when test="${NCDPICheack=='2'}">
           colratio:[65,100,110,60,60,60,75,55,80,60,70,75,80],
        </c:when>    
     <c:otherwise>
         colratio:[55,95,90,60,60,80,90,60,70,60,60,75,95],
      </c:otherwise>
  </c:choose>
        //colratio:[55,100,60,60,65,90,60,60,60,80,50,75,75,60],
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });	
}

function applyScrollOnTblGeoZoneSchool()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#geozoneSchoolTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 350,
        width: 588,
        minWidth: null,
        minWidthAuto: false,      
        //mukesh to set the table
        colratio:[380,250],
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });	
}

function applyScrollOnTb()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#schoolListGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 200,
        width: 680,
        minWidth: null,
        minWidthAuto: false,
        colratio:[350,330],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}


</script>
<style>
.pdfDivBorder
{
z-index: 5000;
border: 1px solid #999;
border: 1px solid rgba(0, 0, 0, 0.3);
-webkit-border-radius: 6px;
-moz-border-radius: 6px;
border-radius: 6px;
-webkit-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
-moz-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
-webkit-background-clip: padding-box;
-moz-background-clip: padding-box;
background-clip: padding-box; 
}
</style>

<style>
	.hide
	{
		display: none;
	}
	.icon-question-sign{
 	 font-size: 1.3em;
 }
</style>

<input type="hidden" id="gridNo" name="gridNo" value="">
<div  class="modal hide" onclick="getSortSecondGrid()" id="myModalforSchhol" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog" style="width: 742px;">
	<div class="modal-content">
	<div class="modal-header" id="schoolHeader">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel"><spring:message code="headSchL"/></h3>
	</div>
	<div class="modal-body" id="schoolListDiv">
		
	</div> 	
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnOk"/></button>
 	</div>
</div>
</div>
</div>
<!-- Pavan Gupta Job group details -->

<div class="modal hide" onclick="getSortSecondGrid()"
	id="myModalforapprovalgroupdetails" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog" style="width: 450px;">
		<div class="modal-content">
			<div class="modal-header" id="approvalgroupdetailsHeader">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">
					x
				</button>
				<h3 id="myModalLabel">
					Job Approval Details
				</h3>
			</div>
			<div class="modal-body" id="approvalgroupdetailsBody">
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true">
					<spring:message code="btnClose" />
				</button>
			</div>
		</div>
	</div>
</div>
<!--shriram-->
<div class="modal hide" id="confirmShowMessage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" style="">		
		 <div class="modal-dialog">
          <div class="modal-content">
			<div class="modal-header">	  		
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body">				
			<p><spring:message code="msgApproveSuccess"/></p>				
		 	</div> 	
		 	<div class="modal-footer">		 	
		    <button class='btn btn-primary' data-dismiss='modal' aria-hidden='true' onclick="afterApproveJob()"><spring:message code="btnOk"/></button>
		 		
		 			
		 	</div>
		</div>
	  </div>
</div>
<!--ended by shriram-->
<div class="row" style="margin:0px;">
         <div style="float: left;">
         	<img src="images/manageusers.png" width="41" height="41" alt="">
         </div>       
         
                         <% if(Utility.getValueOfPropByKey("locale").equalsIgnoreCase("fr")){%>
						 
                            <div style="float: left;">
                               <div class="subheading" style="font-size: 13px;">Gestion des offres d'emplois du Conseil scolaire<span style="display: none;" id="districtOrSchoolTitle"></div>	
                             </div>	  
						
						 <%
						 }else{ %>
						  <div style="float: left;">
                               <div class="subheading" style="font-size: 13px;">Manage <span id="districtOrSchoolTitle"></span> <spring:message code="lblJobOrders"/></div>	
                             </div>	 
						 <%} %>
		 <div class="pull-right add-employment1">
			<c:set var="url" value="addeditjoborder.do?JobOrderType=${JobOrderType}&jobAuthKey=${jobAuthKey}"></c:set>
			<c:if test="${(addJobFlag && fn:indexOf(roleAccess,'|1|')!=-1 && miamiShowFlag && !hrIntegrated)}">
			<a href="javascript:void(0);" onclick="addnewJob('${url}')"><spring:message code="lnkAddJobOrder"/></a>
			</c:if>

		 </div>	
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
</div>
<br>
	 <c:if test="${not empty userMaster}">
		<c:if test="${userMaster.entityType eq 3}">	        			
			<c:set var="hide" value=""></c:set>
			<c:set var="hide" value="hide"></c:set>
		</c:if>
	</c:if>
	<input type="hidden" id="entityType" name="entityType" value="${userMaster.entityType}">
	 
	<div class="row top10" >
		<div class="col-sm-12 col-md-12">
			<label><spring:message code="lblKeywordSearch1"/> </label>
				<input  type="text" 
								class="form-control"
								maxlength="100"
								id="searchTerm" 
								value=""
								
								name="searchTerm" 
								 onkeypress="return runScript(event)"
								/>
			</div>
						
		</div>
	 
	
<div onkeypress="return chkForEnterSearchJobOrder(event);">
	<form class="bs-docs-example" onsubmit="return false;">

		<div class="row top10">
			<div class="col-sm-6 col-md-6">
				<label id="captionDistrictOrSchool">
					<spring:message code="lblDistN"/>
				</label>
				<c:if test="${DistrictOrSchoolName==null}">
					<span> <input type="text" maxlength="255"
							id="districtORSchoolName" name="districtORSchoolName"
							class="help-inline form-control"
							onfocus="getDistrictORSchoolAuto(this, event, 'divTxtShowData', 'districtORSchoolName','districtOrSchoolId','');"
							onkeyup="getDistrictORSchoolAuto(this, event, 'divTxtShowData', 'districtORSchoolName','districtOrSchoolId','');"
							onblur="hideDistrictMasterDiv(this,'districtOrSchoolId','divTxtShowData');getZoneList();getJobCategoryByDistrict();" />
					</span>
					<input type="hidden" id="districtOrSchooHiddenlId" />

				</c:if>
				<c:if test="${DistrictOrSchoolName!=null}">
				<br>
		${DistrictOrSchoolName}	
		<script type="text/javascript">
	 // getList(${DistrictOrSchoolId},${userMaster.entityType});
	 // getList(${DistrictOrSchoolId});
		</script>
					<input type="hidden" id="districtOrSchooHiddenlId"
						value="${DistrictOrSchoolId}" />
					<input type="hidden" id="districtORSchoolName"
						name="districtORSchoolName" />
					<input type="hidden" id="districtHiddenlIdForSchool" />
				</c:if>
				<c:if
					test="${DistrictOrSchoolName==null && DistrictOrSchoolId!=null}">
					<input type="hidden" id="districtHiddenlIdForSchool"
						value="${DistrictOrSchoolId}" />
				</c:if>
				<c:if
					test="${DistrictOrSchoolName==null && DistrictOrSchoolId==null}">
					<input type="hidden" id="districtHiddenlIdForSchool" />
				</c:if>
				<input type="hidden" id="JobOrderType" value="${JobOrderType}" />
				<div id='divTxtShowData'
					onmouseover="mouseOverChk('divTxtShowData','districtORSchoolName')"
					style='display: none; position: absolute; z-index: 5000;'
					class='result'></div>

			</div>

			<%--    <c:if test="${JobOrderType eq 2}"> --%>
			<c:choose>
				<c:when test="${DistrictOrSchoolName==null}">
					<div class="col-sm-6 col-md-6 top20-sm-md2">
				</c:when>
				<c:otherwise>
					<div class="col-sm-6 col-md-6">
				</c:otherwise>
			</c:choose>
			<label>
				<spring:message code="lblSchoolName"/>
			</label>
			<c:choose>
				<c:when
					test="${(userMaster.entityType eq 3 && JobOrderType eq 2 && writePrivilegFlag eq false) || (userMaster.entityType eq 3 && JobOrderType eq 3)}">
				       ${schoolName}
					<input type="hidden" id="schoolName" name="schoolName"
						value="${schoolName}" />
					<input type="hidden" id="schoolId" name="schoolId"
						value="${schoolId}" />
				</c:when>
				<c:otherwise>
					<input type="text" id="schoolName" maxlength="100"
						name="schoolName" class="form-control" placeholder=""
						onfocus="getSchoolAuto(this, event, 'divTxtShowData2', 'schoolName','districtOrSchoolId','');"
						onkeyup="getSchoolAuto(this, event, 'divTxtShowData2', 'schoolName','districtOrSchoolId','');"
						onblur="hideSchoolMasterDiv(this,'districtOrSchoolId','divTxtShowData2');" />
					<input type="hidden" id="schoolName" name="schoolName" />
					<input type="hidden" id="schoolId" name="schoolId" value="0" />
				</c:otherwise>
			</c:choose>
			<div id='divTxtShowData2'
				onmouseover="mouseOverChk('divTxtShowData2','schoolName')"
				style='display: none; position: absolute; z-index: 5000;'
				class='result'></div>
		</div>
</div>

<div class="row top2">
             	<div class="col-sm-3 col-md-3">
					<div class="">
						<span class=""><label class=""><spring:message code="lblStatus"/></label>
						     <select id="status" name="status" class="form-control">
						     	<c:choose>
						     		<c:when test="${requisitionNumber!=null && requisitionNumber!=''}">
						     			<option value=""><spring:message code="optAllJobOdr"/></option>
							      		<option value="A"><spring:message code="optActJobOdr"/></option>
							      		<option value="I"><spring:message code="optInactJobOdr"/></option>
							      	<c:if test="${NCDPICheack eq 2}">	
							      		<option value="R"><spring:message code="optArchived"/></option>
							      	</c:if>	
						     		</c:when>
						     		
						     		<c:otherwise>
						     			<option value=""><spring:message code="optAllJobOdr"/></option>
							      		<option value="A" selected><spring:message code="optActJobOdr"/></option>
							      		<option value="I"><spring:message code="optInactJobOdr"/></option>
							      	<c:if test="${NCDPICheack eq 2}">	
							      		<option value="R"><spring:message code="optArchived"/></option>
						     	    </c:if>
						     		</c:otherwise>
						     	</c:choose>
						     </select>
					     </span>
			 		</div>
				</div>
				
				<div class="col-sm-3 col-md-3">
					<label class=""><spring:message code="lblJoI/JCode"/></label>
					<input type="text" id="jobOrderId" name="jobOrderId" class="form-control fl" >
				</div>
				
				<div class="col-sm-4 col-md-4">
			          <label class=""><spring:message code="lblReq/Posi"/></label>
			          <input type="text" id="reqno" name="reqno" maxlength="8" class="form-control fl" value="${requisitionNumber}"/>
			   </div> 
				
				<div class="col-sm-2 col-md-2 top25-sm22">
					<c:if test="${fn:indexOf(roleAccess,'|5|')!=-1}">
		             	<label class=""></label>
	            	 	<!-- <button class="btn btn-primary " type="button"  onclick="return searchBatchJobOrder()">&nbsp;&nbsp;&nbsp;&nbsp;<spring:message code="btnSearch"/> &nbsp;<i class="icon"></i>&nbsp;&nbsp;&nbsp;&nbsp;</button>-->
	            	 	<button class="btn btn-primary hide" type="button" id='jeffcoButtonSearchHide' onclick="return searchBatchJobOrderNewES()">&nbsp;&nbsp;&nbsp;&nbsp;<spring:message code="btnSearch"/> &nbsp;<i class="icon"></i>&nbsp;&nbsp;&nbsp;&nbsp;</button> 
	            	 	<button class="btn btn-primary hide" type="button" id='jeffcoButtonSearchShow' onclick="return searchBatchJobOrder()">&nbsp;&nbsp;&nbsp;&nbsp;<spring:message code="btnSearch"/> &nbsp;<i class="icon"></i>&nbsp;&nbsp;&nbsp;&nbsp;</button> 
	            	 	<!-- <button class="btn btn-primary " type="button"  onclick="return searchBatchJobOrder()">&nbsp;&nbsp;&nbsp;&nbsp;<spring:message code="btnSearch"/> &nbsp;<i class="icon"></i>&nbsp;&nbsp;&nbsp;&nbsp;</button> -->
	             	</c:if>
				</div>
	         </div> 
	         
	         <div id="advLabel" class="row hide" style="margin-top: 10px;">
				<div class="col-sm-10 col-md-10 " >
				 	<spring:message code="opedAdvSearchLabel"></spring:message>
				 </div>	
			</div> 
			<div  class="row">
			 	<div id='advanceCheckTooltip' class="col-sm-16 col-md-16 " style=" padding-left: 14px;" >
			 	</div>
			</div>
			<br/>
	         
	         
	         <div class="row">
		          <div class="col-sm-6 col-md-6">
			           	<div id="searchLinkDiv" class="">
			           	<a href="#" onclick="displayAdvanceSearch()"><div id="advsearch" class="show"><spring:message code="lnkOpAdvSch"/></div></a>
					   <a href="#" onclick="displayAdvanceSearch()"><div id="filterActiveLbl" class="hide"><spring:message code="lnkfilterActive"/><spring:message code="lnkOpAdvSch"/></div></a>
					    	
			     	 	</div> 
					
						<div id="hidesearchLinkDiv" class="hide">
					   		<a href="#" onclick="hideAdvanceSearch()"><spring:message code="lnkClAdvSch"/></a>
						</div>
	     		  </div>
	         </div>
	         
	         <div class="hide" id="advanceSearchDiv">
		         <div class="row" >
		         	
			             <div class="col-sm-6 col-md-6" id='zone_id'>
				         		<span class=""><label class=""><spring:message code="lblZone"/></label></span>
			           			<select id="zone" name="zone" class="form-control" disabled="disabled"></select>
				        </div><!--
				        Redesigned by deepak 
				        -->
				        <div class="col-sm-6 col-md-6 jobStatusJeffco" style='display:none;'>
				         		<span class=""><label class=""><spring:message code="lblJoStatus"/></label></span>
			           			<select id="jobApplicationStatus" name="jobApplicationStatus" class="form-control">
			           		 <option value="0"><spring:message code="sltStatis"/></option>
		       		         <option value="1"><spring:message code="lblOngoing"/></option>
		        	         <option value="2">Temp-Protected</option>
		        	 		 <option value="3">Temp-Funding</option>			           			
			           			</select>
				        </div>
			        
		         </div>
		         <div class="row" >
		         	<div class="col-sm-6 col-md-6">
		         			<label>
								<spring:message code="lblJobCat"/>
							</label>
							<select multiple="multiple" class="form-control" name="jobCateSelect"
								id="jobCateSelect" size="5" style="height:94px;" onblur="getJobSubcategory();" >
								<option value="0">
									<spring:message code="msgSltDistForRlvtJobCtay"/>
								</option>
						</select>
		         	</div>
		         	<div class="col-sm-6 col-md-6">
		         			<label>
								<spring:message code="lblJobSubCategory"/>
							</label>
							<select multiple="multiple" class="form-control" name="jobSubCateSelect"
								id="jobSubCateSelect" size="5" style="height:94px;">
								<option value="0">
									<spring:message code="msgJobSubCatlist"/>
								</option>
						</select>
		         	</div>
		         </div>
	         </div>
 
<!-- Weekly Report -->
<!--<div class="" > 
<c:if test="${userMaster.entityType eq 1}">
   <a href='districtReport.do' >Weekly Report</a><br/>
   </c:if>
 <c:if test="${userMaster.entityType eq 2}">
   <a href='javascript:void(0);' onclick='weeklyReport()' >Weekly Report</a><br/>
 </c:if>
  
 <c:if test="${userMaster.entityType eq 1}">
   <a href='districtjobreport.do' >District jobs Report</a>
   </c:if>
 <c:if test="${userMaster.entityType eq 2}">
   <a href='javascript:void(0);' onclick='districtAlljobsReport()' >District jobs Report</a>
 </c:if>
   -->
</div>
<!--
<div style="clear: both;"></div>

 All district job report 
<div class="modal hide "  id="districtAllJobReportModelDiv"  tabindex="-1" role="dialog" data-keyboard="false" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" >
	<div class="modal-dialog" style="width:1200px; margin-top: 0px;">
	 <div class="modal-content">
		 <div class="modal-header">
		   	 <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="canelPrintDataforDistrict();">x</button>
		   	 <h3 id="">Candidates Jobs Details</h3>
		 </div>
	
		    <div class="modal-body" style="max-height: 500px; overflow-y: auto;"> 		
				<div class="" id="districtAllJobReportTableDiv" >          
	            </div>
	 	   </div>
	   	 <div class="modal-footer">
		 	<table border=0 style="margin-left:850px;">
		 		<tr>
			 		<td  nowrap>
			 			<button class="btn  btn-large btn-primary" aria-hidden="true" onclick='exlExportforDistrictJobs();'>&nbsp;Export to Excel&nbsp;&nbsp;&nbsp;</button>&nbsp;&nbsp;
					</td>
					<td  nowrap>
			 			<button class="btn btn-large" data-dismiss="modal" aria-hidden="true" onclick='canelPrintDataforDistrict();'>Cancel</button>
					</td>
		 		</tr>
		 	</table>
		 </div>
   </div>
 </div>
</div>

-->


<!-- Weeekly Report 

<div style="clear: both;"></div>

<div class="modal hide "  id="weeklyReportModelDiv"  tabindex="-1" role="dialog" data-keyboard="false" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" >
	<div class="modal-dialog" style="width:1000px; margin-top: 0px;">
	 <div class="modal-content">
		 <div class="modal-header">
		   	 <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="canelPrintData();">x</button>
		   	 <h3 id="">Candidates Jobs Details</h3>
		 </div>
	
		    <div class="modal-body" style="max-height: 500px; overflow-y: auto;"> 		
				<div class="" id="weeklyReportTableDiv" >          
	             </div>
	 	   </div>
	   	 <div class="modal-footer">
		 	<table border=0 style="margin-left:700px;">
		 		<tr>
			 		<td  nowrap>
			 			<button class="btn  btn-large btn-primary" aria-hidden="true" onclick='exlExport();'>&nbsp;Export to Excel&nbsp;&nbsp;&nbsp;</button>&nbsp;&nbsp;
					</td>
					<td  nowrap>
			 			<button class="btn btn-large" data-dismiss="modal" aria-hidden="true" onclick='canelPrintData();'>Cancel</button>
					</td>
		 		</tr>
		 	</table>
		 </div>
   </div>
 </div>
</div>-->
             <div id="" class="hide" style="height: 480px; width: 100%; overflow: auto;'-webkit-overflow-scrolling' : 'touch">
				 <iframe src="" id="ifrmTrans" width="100%" height="100%" scrolling="no">
				 </iframe>  
			  </div> 

<!--
<div style="clear: both;"></div>
 
<div class="modal hide "  id="printAllJobDiv"  tabindex="-1" role="dialog" data-keyboard="false" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" >
	<div class="modal-dialog" style="width:1000px; margin-top: 0px;">
	<div class="modal-content">
	<div class="modal-header">
	   	<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="canelPrintData();">x</button>
	   	<h3 id="">Print Data</h3>
	</div>
	
	<div class="modal-body" style="max-height: 500px; overflow-y: auto;"> 		
			<div class="" id="pritDataTableDiv" >          
             </div>
 	</div>
 	<div class="modal-footer">
	 	<table border=0 style="margin-left:750px;">
	 		<tr>
		 		<td  nowrap>
		 			<button class="btn  btn-large btn-primary" aria-hidden="true" onclick='printAllData();'>&nbsp;&nbsp;&nbsp;Print&nbsp;&nbsp;&nbsp;</button>&nbsp;&nbsp;
				</td>
				<td  nowrap>
		 			<button class="btn btn-large" data-dismiss="modal" aria-hidden="true" onclick='canelPrintData();'>Cancel</button>
				</td>
	 		</tr>
	 	</table>
	</div>
  </div>
 </div>
</div> -->
 <!-- PDF -->
<div  class="modal hide pdfDivBorder"  id="modalDownloadJobOrder" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog-for-cgpdfreport">
	<div class="modal-content"  style="background-color: #ffffff;">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel"><span id="pdfDistrictOrSchoolTitle"></span> <spring:message code="lblJobOrders"/> </h3>
	</div>
		<div class="modal-body">		
			<div class="control-group">
				    <div style="width: 100%; height: 100%; -webkit-overflow-scrolling: touch;">
	                  <iframe src="" id="ifrmCJS" width="100%" height="480px">
	               </iframe>        	
				</div>
			</div>
	 	</div>
	 	<div class="modal-footer">
	 		<button class="btn" data-dismiss="modal" aria-hidden="true" ><spring:message code="btnClose"/></button> 		
	 	</div>
     </div>
 	</div>
</div>

<!-- PDF End -->   
 <div class="row mt5 top10" style="padding-right:20px;">
       <table class="marginrightForTablet">
		      <tr>
		         <td  style='padding-bottom:5px;'>
					<a data-original-title='<spring:message code="lblExporttoExcel"/>' rel='tooltip' id='exlId' href='javascript:void(0);'  onclick="downloadJobOrderExcelReport();"><span class='icon-file-excel icon-large iconcolor'></span></a>
				 </td>
				 <td style='padding-left:2px;padding-bottom:5px;'>
				  <a data-original-title='<spring:message code="lblExporttoPDF"/>' rel='tooltip'  href="javascript:void(0);" id='pdfId' onclick='downloadJobOrderPDFReport();'><span class='pdf-eoc icon-large iconcolor'></span></a>
				 </td style='padding-bottom:5px;'>
				 <td>
					<a data-original-title='<spring:message code="btnP"/>' rel='tooltip' id='printId' href='javascript:void(0);' onclick="printAllJobs();"><span class='icon-print icon-large iconcolor'></span></a>
				 </td>  
			</tr>
		</table>
		</div>


 
        <div class="TableContent">        	
            <div class="table-responsive" id="divMain" onclick="getSortFirstGrid()">          
            </div>            
        </div> 
        
	
	    
 	 
	<div style="display:none;" id="loadingDiv">
    <table  align="center">
 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
	</table>
	</div>
    <br><br>


<div class="modal hide "  id="geozoneschoolDiv"  tabindex="-1" role="dialog" data-keyboard="false" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" >
	<div class="modal-dialog" style="width:650px; margin-top: 20px;">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="$('#geozoneschoolFlag').val(0);">x</button>
		<h3  id="myModalLabelGeo"></h3>
	</div>
	<div class="modal-body"> 		
		<div class="control-group">
				<input type="hidden" id="geozoneId" value="0"/>
				<input type="hidden" id="geozoneschoolFlag" value="0"/>
				<div id="geozoneschoolGrid"></div>
		</div>
 	</div>
 	<div class="modal-footer">
 	<span id="">
 		</span>&nbsp;&nbsp;<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="$('#geozoneschoolFlag').val(0);"><spring:message code="btnClose"/></button> 		
 	</div>
  </div>
 </div>
</div>


<!--shriram-->
<div class="modal hide" id="confirmMessage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" style="">		
		 <div class="modal-dialog">
          <div class="modal-content">
			<div class="modal-header">	  		
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body">				
			<p><spring:message code="msgWantToApproveJob"/></p>				
		 	</div> 	
		 	<div class="modal-footer">
		 	        <button class="btn btn-primary" onclick="approveJobOk()"><spring:message code="btnApprove"/></button>
		 	        <button class='btn' data-dismiss='modal' aria-hidden='true'><spring:message code="btnClr"/> </button>

		 	</div>
		</div>
	  </div>
</div>


<!--Ended by shriram-->


<div  class="modal hide distAttachment"  id="distAttachment"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="table-responsive">
		<div class='onr-dialog'>
			<div class='modal-content'>
			<div class="modal-header">
		  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="hideDiv();">x</button>
				<h3 id="UserProfileNameEEOC11"><spring:message code="hdAttachment"/></h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<div class="" id="divPnrAttachment">
					 
					</div>
					<iframe src="" id="ifrmAttachment" width="100%" height="480px"> </iframe>   
				</div>
				
		 	</div>
		 	<input type="hidden" id="eligibilityMasterId" value=""/>
		 	<div class="modal-footer">
		 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="hideAttachmentDiv();"><spring:message code="btnClose"/></button> 		
		 	</div>
		  </div>
		 </div>
	</div>
</div>



<!--	 Modal for displaying job Order  history  -->
<div  class="modal hide"  id="jobOrderHistoryModalId" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
	<div class="modal-dialog" style="width: 850px;">
	<div class="modal-content">	
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick="hideById('jobOrderHistoryModalId')">x</button>
		<h3 id="myModalLabel">Job Order History</h3>
	</div>
	<div class="modal-body" style="max-height: 450px;overflow-y: auto">		
		<div class="control-group">
			<div class="" id="divJobHisTxt">
			
			
		</div>
 	</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true"   onclick="hideById('jobOrderHistoryModalId')">Close</button> 		
 	</div>
   
 </div>
</div>
</div>



<script>
var requisitionNumber = "${requisitionNumber}";
$(document).ready(function(){
	if(requisitionNumber!=null && requisitionNumber!="")
	{
		displayAdvanceSearch();
		//$("#divMain").scrollTop($("#divMain").prop('scrollHeight'))
	}
});
</script>


<script type="text/javascript">
		//displayJobRecords();
  $('#pdfId').tooltip();
  $('#exlId').tooltip();
  $('#printId').tooltip();
	</script>
<c:if test="${userMaster.entityType eq 1}">
	<script type="text/javascript">
		//shadab commented below lines
		//displayJobRecords();
		//searchBatchJobOrderES();
		
		searchBatchJobOrderNewES();
	</script>
 </c:if>	
  <c:if test="${DistrictOrSchoolName!=null}">
        	<script type="text/javascript">
        	
              	  getList(${DistrictOrSchoolId},${userMaster.entityType});
              	  try { getJobCategoryByDistrict(); } catch (e) {}
        	</script>
 </c:if>               
                 