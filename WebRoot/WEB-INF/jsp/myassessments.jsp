<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<script type="text/javascript" src="dwr/interface/CGServiceAjax.js?ver=${resouceMap['CGServiceAjax.ajax']}"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" /> 
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/teacherinfonew.js?ver=${resourceMap['js/teacherinfonew.js']}"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<script type='text/javascript' src="js/examslotselection.js?ver=${resourceMap['js/examslotselection.js']}"></script>
<script type="text/javascript" src="dwr/interface/DistrictAssessmentCampaignAjax.js?ver=${resouceMap['DistrictAssessmentCampaignAjax.ajax']}"></script>
<script type='text/javascript' src="js/districtAssessment-campaign.js?ver=${resouceMap['js/assessment-campaign.js']}"></script>

<script type="text/javascript">
function applyScrollOnTbl()
{
	    var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#examDetailsTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',     
        width: 940,    
        minWidth: null,
        minWidthAuto: false,   
        //colratio:[200,200,180,120,130,200],
         colratio:[255,320,250,100],
	    addTitles: false,
        zebra: true,       
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 20,
        resizeCol: false,
        wrapper: false
        });
        });			
}

</script>
<div class="row" style="margin:0px;">
                
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;"><spring:message code="lblMyAssessments"/></div>	
         </div>			
		 
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
</div>
<br>
<div class="row" style="margin:0px;">
<div id="examSlotData" style="margin-top: 20px;margin-left:15px;">
 	<div class="table-responsive" id="divMain"></div>   
</div>
</div>
<input type="hidden" name="teacherId" id="teacherId" value="${teacherDetail.teacherId}"/>
<script type="text/javascript">
  displayAssessmentSlotDetails();
  </script>
  
  <div style="display:none;" id="loadingDivInventory">
	 <table  align="center" >
 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
 		<tr id='paymentMessage'><td style='padding-top:0px;' id='spnMpro' align='center'> <spring:message code="msgAssIsBeingLoaded"/></td></tr>
	</table>
</div>
