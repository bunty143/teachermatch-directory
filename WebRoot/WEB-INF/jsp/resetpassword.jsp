<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%--<form:form commandName="teacherDetail" action="signin.do" onsubmit="return loginUserValidate();"> --%>
<form action="resetpassword.do" method="post" onsubmit="return chkResetpwd();">
<input type="hidden" id="key" name="key" value="${param.key}">
<input type="hidden" id="id" name="id" value="${param.id}">
 						 <div class="row col-md-offset-4 top15">                                   
                                   <div class="col-sm-8">
                                   <div class="subheadinglogin">
							<spring:message code="headReset/EstPss"/>
								    </div></div>  
                          </div>                          		               
                          <div class="row col-md-offset-4">                                   
                                    <div class="col-sm-12">
										        <div id='divServerError'  class='divErrorMsg' style="display: block;">${msgError}</div>
												<div class='divErrorMsg' id='errordiv' style="display: block;"></div>						             
								    </div>  
                          </div>
                          
                          <div class="row col-md-offset-4">                                   
                                          <div class="col-sm-6">
										    	<label><strong><spring:message code="msgPlzEnterNewPass"/></strong></label>		        		
									        	<input type="password" class="form-control"  name="txtPwd" id="txtPwd" placeholder="Enter your new password"   maxlength="12"  />
									         </div> 
                          </div> 
                                
                          <div class="row col-md-offset-4 top10">            
                                    <div class="col-sm-6">
										<label><strong><spring:message code="msgPlzRe-EtrNewPass"/></strong></label>				 
		         						<input id="txtRePwd" type="password" class="form-control" placeholder="Re-enter new password"  maxlength="12"/>
								    </div>                                
                          </div> 
                          
                          <div class="row col-md-offset-4 top20">                           
	                          	<div class="left15">     		
			            		  <button class="btn fl btn-primary" type="submit" onclick="return loginUserValidate()"><spring:message code="btnLogin"/><i class="icon"></i></button>			                
			              		</div>  
		              	  </div>
</form>
<script>
	document.getElementById("txtPwd").focus();
</script>