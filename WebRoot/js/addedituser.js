/* @Author: Gagan */
/*========  For Handling session Time out Error ===============*/
var keyContactValue="0";
function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert(resourceJSON.msgServerErr+": "+exception.javaClassName);}
}
/*=======  Add Edit User on Press Enter Key ========= */
function chkForEnterAddEditUser(evt)
{
	var charCode = ( evt.which ) ? evt.which : evt.keyCode;
	if(charCode==13)
	{
		validateAddEditUser();
	}	
}

/*======== Redirect to Manage User Page ===============*/
function cancelUser()
{
	window.location.href="manageuser.do";
}
/*========  Check Valid Email address  ===============*/
function isEmailAddress(str) 
{	
	var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
	return emailPattern.test(str);
}
/*========  Check Valid Password Patern  ===============*/
function chkPwdPtrn(str)
{
	var pat1 = /[^a-zA-Z]/g; //If any spcl char find it return true otherwise false
	var pat2 = /[a-zA-Z]/g;  //If any alphabet found it return true otherwise false
	return pat1.test(str) && pat2.test(str);
}
/*========  It will return Role Names corresponding Entity Type ===============*/
function getRoleByEntityType(loggedEntityType,onloadflag)
{
	var entity_type	=	document.getElementById("AEU_EntityType").value;
	var districtId	=	document.getElementById("districtHiddenId").value;
	var schoolId	=	document.getElementById("schoolHiddenId").value;
	var roleIdOfLoginUser	=	trim(document.getElementById("roleIdUserLogin").value);
	document.getElementById("keyContactTypeAdduser").style.display='none';
	//document.getElementById("keyContactTable").style.display='none';
	var userId= "";
	var roleId="";
	try{
		userId=$('#userId').val();		
	}catch(e){}
	try{
		roleId=$('#roleId').val();
	}catch(e){}
	
	if(loggedEntityType!=entity_type && onloadflag){
		document.getElementById("districtORSchoolName").value="";
		document.getElementById("districtOrSchooHiddenlId").value="";
	}
	if(districtId!=0 && schoolId!=0  && entity_type==3){
		document.getElementById("districtOrSchooHiddenlId").value=schoolId;
	}else if(districtId!=0 && schoolId!=0 && loggedEntityType==2 && entity_type==2){
		document.getElementById("districtOrSchooHiddenlId").value=districtId;
	}else{
		if(districtId!=''){
			document.getElementById("districtOrSchooHiddenlId").value=districtId;
		}
	}
	$(document).ready(function()
	{
		$(".schoolClass").hide();
		$(".DSClass").hide();
		$(".schoolClass").hide();
		$(".branchClass").hide();
		$(".hqClass").hide();
		$('#onlyBranchName').attr('readonly',false);	
		$('#onlyDistrictName').attr('readonly',false);
		$('#districtORSchoolName').attr('readonly',false);
		
		/*==== entity_type == 0 Means Select Entity Type 1 Means TM 2-> Districts and 3-> Schools */
		if(loggedEntityType==2 && (entity_type==2 || entity_type==3))
		{
			if(entity_type ==2)
			{
				$("#AEU_searchbox").hide();
			}
			if(entity_type ==	3)
			{
				document.getElementById("AEU_captionDistrictOrSchool").innerHTML	=	resourceJSON.msglblSchoolDep+"<span class='required'>*</span>"
				$("#AEU_searchbox").fadeIn();
			}
			
			if(entity_type ==	loggedEntityType)
			{
				$("#AEU_searchbox").hide();
				$(".schoolClass").hide();
			}
		}
		if(loggedEntityType ==	1)
		{
			if(entity_type ==	2)
			{	
				$(".DSClass").hide();
				$('#districtORSchoolName').attr('readonly', false);
				$(".schoolClass").show();
				$(".DSClass").css('display','none');
				//$(".hqClass").fadeIn();
				//$(".branchClass").fadeIn();
				// keyContactType	
				document.getElementById("keyContactTypeAdduser").style.display='block';
				if(userId==null || userId=='')
				$('#onlyHeadQuarterName').focus();
				
				document.getElementById("AEU_captionDistrictOrSchool").innerHTML	=	resourceJSON.msgDistrict+"<span class='required'>*</span>"
			}
			if(entity_type ==	3)
			{	
				$(".schoolClass").fadeIn();
				//$(".hqClass").fadeIn();
				//$(".branchClass").fadeIn();				
				if(userId==null || userId=='')
				$('#onlyHeadQuarterName').focus();	
				
			     $('#districtORSchoolName').attr('readonly', true);
				document.getElementById("AEU_captionDistrictOrSchool").innerHTML	=resourceJSON.msglblSchoolDep+"<span class='required'>*</span>"
				// keyContactType	
				document.getElementById("keyContactTypeAdduser").style.display='block';
				
			}
			$("#AEU_searchbox").fadeIn();
			if(entity_type ==	loggedEntityType)
			{
				$("#AEU_searchbox").hide();
			}
			if(entity_type ==	5)
			{	
				$(".schoolClass").hide();
				$(".hqClass").fadeIn();
				
				if(userId==null || userId=='')
				$('#onlyHeadQuarterName').focus();
				
				$(".DSClass").hide();
			}
			if(entity_type ==	6)
			{	
				$(".schoolClass").hide();
				$(".hqClass").fadeIn();
				$(".branchClass").fadeIn();
				
				if(userId==null || userId=='')
				$('#onlyHeadQuarterName').focus();
				
				$('#onlyBranchName').attr('readonly',true);
				$(".DSClass").hide();
			}
			if(entity_type ==	loggedEntityType)
			{
				$("#AEU_searchbox").hide();
			}
			if(entity_type ==	6 || entity_type ==	5 || entity_type==2){
				$("#AEU_searchbox").hide();
			}
			if(userId!=null && userId!=''){
				$('#onlyHeadQuarterName').attr('readonly',false);
				$('#onlyBranchName').attr('readonly',false);	
				$('#onlyDistrictName').attr('readonly',false);
				$('#districtORSchoolName').attr('readonly',false);
			}
		}
		if(loggedEntityType ==	2)
		{
			if(entity_type ==	2)
			{
			//	document.getElementById("AEU_captionDistrictOrSchool").innerHTML	=	"District<span class='required'>*</span>"
				$("#AEU_searchbox").hide();
			//	$('#districtORSchoolName').focus();
			}
			if(entity_type ==	3)
			{	
				document.getElementById("AEU_captionDistrictOrSchool").innerHTML	=	resourceJSON.msglblSchoolDep+"<span class='required'>*</span>"
				$("#AEU_searchbox").fadeIn();
				//$('#districtORSchoolName').focus();
			}
//			 keyContactType	
		if(roleIdOfLoginUser ==	7){
			document.getElementById("keyContactTypeAdduser").style.display='block';
			}
				
		}
		if(loggedEntityType ==	5)
		{
				$(".schoolClass").hide();
				$(".branchClass").hide();
				$(".hqClass").hide();
				if(entity_type ==	2)
				{	
					$(".DSClass").hide();
					$(".schoolClass").hide();
					$(".schoolClass").fadeIn();	
					$('#onlyDistrictName').attr('readonly',true);
					if($('#isBracnchExists').val().trim()==1){
						$(".branchClass").fadeIn();
						}else{
							$('#onlyDistrictName').attr('readonly',false);
						}
					if(userId==null || userId=='')
					$('#onlyBranchName').focus();	
					//$('#districtORSchoolName').focus();				
					document.getElementById("AEU_captionDistrictOrSchool").innerHTML	=	"District<span class='required'>*</span>"
				}
				if(entity_type ==	3)
				{
					$(".DSClass").show();	
					$('#districtORSchoolName').attr('readonly',true);
					$('#onlyDistrictName').attr('readonly',true);
					if($('#isBracnchExists').val().trim()==1){
					$(".branchClass").fadeIn();
					}else{
						$('#onlyDistrictName').attr('readonly',false);
					}
					$(".schoolClass").fadeIn();
					
					if(userId==null || userId=='')
					$('#onlyBranchName').focus();	
					
					document.getElementById("AEU_captionDistrictOrSchool").innerHTML	=	resourceJSON.msglblSchoolDep+"<span class='required'>*</span>"
				}				
				if(entity_type ==	6)
				{	
					$(".schoolClass").hide();			
					$(".branchClass").fadeIn();
					
					if(userId==null || userId=='')
					$('#onlyBranchName').focus();	
					//$('#onlyBranchName').focus();				
				}
				
				$("#Searchbox").fadeIn();
				if(entity_type ==	loggedEntityType || entity_type==0)
				{
					$("#Searchbox").hide();
					$(".schoolClass").hide();
				}
				
				if(userId!=null || userId!=''){
					$('#onlyHeadQuarterName').attr('readonly',false);
					$('#onlyBranchName').attr('readonly',false);	
					$('#onlyDistrictName').attr('readonly',false);
					$('#districtORSchoolName').attr('readonly',false);
				}
				
		}
		if(loggedEntityType ==	6)
		{
				$(".schoolClass").hide();
				$(".branchClass").hide();
				$(".hqClass").hide();
				if(entity_type ==	2)
				{	
					$(".DSClass").hide();
					$(".schoolClass").fadeIn();	
					if(userId==null || userId=='')
					$('#onlyDistrictName').focus();				
					document.getElementById("AEU_captionDistrictOrSchool").innerHTML	=	"District<span class='required'>*</span>"
				}
				if(entity_type ==	3)
				{
					$(".DSClass").show();
					$(".schoolClass").fadeIn();
					if(userId==null || userId=='')
					$('#onlyDistrictName').focus();
					document.getElementById("AEU_captionDistrictOrSchool").innerHTML	=	resourceJSON.msglblSchoolDep+"<span class='required'>*</span>"
				}
				$("#Searchbox").fadeIn();
				if(entity_type ==	loggedEntityType || entity_type==0)
				{
					$("#Searchbox").hide();
					$(".schoolClass").hide();
				}
				
				if(userId!=null || userId!=''){
					$('#onlyHeadQuarterName').attr('readonly',false);
					$('#onlyBranchName').attr('readonly',false);	
					$('#onlyDistrictName').attr('readonly',false);
					$('#districtORSchoolName').attr('readonly',false);
				}
		}
//		if(loggedEntityType ==	3)
//		{
//			document.getElementById("keyContactTypeAdduser").style.display='block';
//			
//		}
		
		if(userId!=null && userId!='' && roleId!=null && roleId!='')
		{
			getRoleNameByEntityType(entity_type,roleId);
			$('#AEU_RoleName').val($('#selectRole').val());
		}
		
		roleId=0;	
		if(onloadflag){			
		getRoleNameByEntityType(entity_type,roleId);
		}
	});
}
function getRoleNameByEntityType(entity_type,roleId)
{
	UserAjax.getRoleNameByEntityType(entity_type,roleId, { 
		async: true,
		callback: function(data)
		{
			document.getElementById("roleNamePopulate").innerHTML	=	data;
			return;
		},
		errorHandler:handleError
			});
}
/*========  Save Users  ===============*/
var districtORSchoolNameCount=0;
var onlyDistrictName=0;
function validateAddEditUser()
{
	$('#loadingDiv').fadeIn();
	$('#errordiv').empty();
	$('#firstName').css("background-color", "");
	$('#lastName').css("background-color", "");
	$('#emailAddress').css("background-color", "");
	$('#password').css("background-color", "");
	$('#AEU_RoleName').css("background-color", "");
	$('#onlyDistrictName').css("background-color", "");
	$('#districtORSchoolName').css("background-color", "");
	$('#onlyBranchName').css('background-color','');
	$('#onlyHeadQuarterName').css('background-color','');
	/*==== Geting Dropdowns selected option id from objective.jsp in domainMaster and competencyMaster===*/
	var userId				=	trim(document.getElementById("userId").value);
	var entityType			=	trim(document.getElementById("AEU_EntityType").value);
	var salutation			=	trim(document.getElementById("salutation").value);
	var firstName			=	trim(document.getElementById("firstName").value);
	var lastName			=	trim(document.getElementById("lastName").value);
	var title				=	trim(document.getElementById("title").value);
	var emailAddress		=	trim(document.getElementById("emailAddress").value);
//	var password			=	trim(document.getElementById("password").value);
	var phoneNumber			=	trim(document.getElementById("phoneNumber").value);
	var mobileNumber		=	trim(document.getElementById("mobileNumber").value);
	var AEU_RoleId			=	document.getElementById("AEU_RoleName").value;
	var authenticationCode	=	trim(document.getElementById("authenticationCode").value);
	var districtORSchoolName=   trim(document.getElementById("districtORSchoolName").value);
	var onlyDistrictName	=   trim(document.getElementById("onlyDistrictName").value);
	var userSessionEntityTypeId =	document.getElementById("userSessionEntityTypeId").value;
	var districtOrSchooHiddenlId=document.getElementById("districtOrSchooHiddenlId").value;
	var districtId	=	document.getElementById("districtSessionId").value;
	var schoolId1	=	document.getElementById("schoolSessionId").value;
	var schoolHiddenId1	=	document.getElementById("schoolHiddenId").value;
	var counter				=	0;
	var focusCount			=	0;
	var branchId=document.getElementById("branchHiddenId").value;
	var headQuarterId=document.getElementById("headQuarterHiddenId").value;
	//alert("headQuarterId ::::::::::::=>"+headQuarterId);
	//******** Adding by hafeez  ************************
	   
	 //  var conatactTypeId =	document.getElementById("dropContactTypeAdduser").value;
	//alert("conatactTypeId "+conatactTypeId);
	//******** Adding by deepak  ***************************
	if(userSessionEntityTypeId!=1){
		try{
			var headQuarterId_1 =document.getElementById("headQuarterId").value;
			if(headQuarterId==null ||headQuarterId=="")
			{
				headQuarterId=headQuarterId_1;
			}			
		}catch(e){}
	}
	
	//************* End by deepak *******************
	//alert(entityType+"     ==========="+$('#AEU_EntityType').val()+"  $('#districtHiddenId').val()==="+$('#districtHiddenId').val());
	if(entityType==2 && (districtOrSchooHiddenlId==0 || districtOrSchooHiddenlId=='')){
		districtOrSchooHiddenlId=districtId;
	}else if(entityType==3  && (districtOrSchooHiddenlId==0 || districtOrSchooHiddenlId=='')){
		districtOrSchooHiddenlId=schoolId1;
	}
	
	if(userSessionEntityTypeId==5){
		var isBracnchExists=$('#isBracnchExists').val();
		var flagerrormessage=false;
		if(entityType==3){
			var flagforfocus=true;
			if(isBracnchExists==1 && branchId==""){	
				$('#onlyBranchName').css('background-color','rgb(245, 231, 225)');
				$('#errordiv').append("&#149; Please enter Branch<br>");
				$('#onlyBranchName').focus();
				flagerrormessage=true;
				flagforfocus=false;
				counter++;									
			}
			if($('#districtHiddenId').val()==""){
				$('#onlyDistrictName').css('background-color','rgb(245, 231, 225)');
				$('#errordiv').append("&#149; Please enter District<br>");
				if(flagforfocus)
				$('#onlyDistrictName').focus();	
				flagerrormessage=true;
				flagforfocus=false;
				counter++;
			}
			if($('#districtOrSchooHiddenlId').val()==""){
				$('#districtORSchoolName').css('background-color','rgb(245, 231, 225)');
				$('#errordiv').append("&#149; Please enter School<br>");
				if(flagforfocus)
				$('#districtORSchoolName').focus();	
				flagerrormessage=true;
				counter++;
			}				
		}
		if(entityType==2){
			var flagforfocus=true;
			if(isBracnchExists==1 && branchId==""){	
				$('#onlyBranchName').css('background-color','rgb(245, 231, 225)');
				$('#errordiv').append("&#149; Please enter Branch<br>");
				$('#onlyBranchName').focus();
				flagerrormessage=true;
				flagforfocus=false;
				counter++;									
			}
			if($('#districtHiddenId').val()==""){
				$('#onlyDistrictName').css('background-color','rgb(245, 231, 225)');
				$('#errordiv').append("&#149; Please enter District<br>");
				if(flagforfocus)
				$('#onlyDistrictName').focus();	
				flagerrormessage=true;
				flagforfocus=false;
				counter++;
			}					
		}
		if(entityType==6){
			if(isBracnchExists==1 && branchId==""){	
				$('#onlyBranchName').css('background-color','rgb(245, 231, 225)');
				$('#errordiv').append("&#149; Please enter Branch<br>");
				$('#onlyBranchName').focus();
				flagerrormessage=true;
				counter++;									
			}						
		}
		if(flagerrormessage){focusCount++;}
	}	
	
	if(userSessionEntityTypeId==6){		
		var isBracnchExists=$('#isBracnchExists').val();
		var flagerrormessage=false;
		//alert('enter'+isBracnchExists+"      ==    "+branchId+" == "+$('#districtHiddenId').val()+" == "+$('#districtOrSchooHiddenlId').val());
		if(entityType==3){
			var flagforfocus=true;			
			if($('#districtHiddenId').val()=="" || $('#districtHiddenId').val()=="0"){
				$('#onlyDistrictName').css('background-color','rgb(245, 231, 225)');
				$('#errordiv').append("&#149; Please enter District<br>");
				if(flagforfocus)
				$('#onlyDistrictName').focus();	
				flagerrormessage=true;
				flagforfocus=false;
				counter++;
			}
			if($('#districtOrSchooHiddenlId').val()=="" || $('#districtOrSchooHiddenlId').val()=="0"){
				$('#districtORSchoolName').css('background-color','rgb(245, 231, 225)');
				$('#errordiv').append("&#149; Please enter School<br>");
				if(flagforfocus)
				$('#districtORSchoolName').focus();	
				flagerrormessage=true;
				counter++;
			}				
		}
		if(entityType==2){
			var flagforfocus=true;		
			if($('#districtHiddenId').val()=="" || $('#districtHiddenId').val()=="0"){
				$('#onlyDistrictName').css('background-color','rgb(245, 231, 225)');
				$('#errordiv').append("&#149; Please enter District<br>");
				if(flagforfocus)
				$('#onlyDistrictName').focus();	
				flagerrormessage=true;
				flagforfocus=false;
				counter++;
			}					
		}		
		if(flagerrormessage){focusCount++;}
	}
	
   /* if (districtORSchoolName=="" && userSessionEntityTypeId==2 && districtORSchoolNameCount!=2){
		districtORSchoolNameCount=1;
		$('#errordiv').show();
		if(entityType==3){
			$('#errordiv').append("&#149; "+resourceJSON.PlzEtrSchool+"<br>");
		
			if(focusCount	==	0)
				$('#districtORSchoolName').focus();
			$('#districtORSchoolName').css("background-color", "#F5E7E1");
			counter++;
			focusCount++;
		}
	}else if (onlyDistrictName=="" && userSessionEntityTypeId==1 && onlyDistrictName!=2  && entityType==3){
		districtORSchoolNameCount=1;
		$('#errordiv').show();			
		if(entityType==3){
			$('#errordiv').append("&#149; "+resourceJSON.PlzEtrDistrict+"<br>");		
		}
		if(focusCount	==	0)
		$('#onlyDistrictName').focus();
		$('#onlyDistrictName').css("background-color", "#F5E7E1");	
		counter++;
		focusCount++;
	}	
	else if (onlyDistrictName=="" && userSessionEntityTypeId==1 && districtORSchoolNameCount!=2  && entityType!=1 && entityType!=5 && entityType!=6){
		var fl = 0;
		
		districtORSchoolNameCount=1;
		$('#errordiv').show();
		if(entityType==2){
			$('#errordiv').append("&#149; "+resourceJSON.PlzEtrDistrict+"<br>");
			fl = 1;
		}		
		if(entityType==3){
			$('#errordiv').append("&#149; "+resourceJSON.PlzEtrSchool+"<br>");
		}
		if(focusCount	==	0){
			if(fl==1){
			$('#onlyDistrictName').focus();		
			$('#onlyDistrictName').css("background-color", "#F5E7E1");
			counter++;
			focusCount++;
			}
			
		}
		
	}
	else if (districtORSchoolName=="" && userSessionEntityTypeId==1 && districtORSchoolNameCount!=2  && entityType!=1 && entityType!=5 && entityType!=6){
		//districtORSchoolNameCount=1;
		var fl = 0;
		$('#errordiv').show();
		
		if(entityType==3){
			$('#errordiv').append("&#149; Please enter School<br>");
			fl = 1;
		}
		
		if(focusCount	==	0){
			if(fl == 1){
				$('#districtORSchoolName').focus();		
				$('#districtORSchoolName').css("background-color", "#F5E7E1");
				counter++;
				focusCount++;
			}
		}
		
	}else if (districtORSchoolName	!="" && districtORSchoolNameCount==2 && entityType!=1){
		districtORSchoolNameCount=1;
		$('#errordiv').show();
		if(entityType==2 && userSessionEntityTypeId==1 ){
			$('#errordiv').append("&#149; "+resourceJSON.msgValidDistrictName+"<br>");
			if(focusCount	==	0)
				$('#districtORSchoolName').focus();
			$('#districtORSchoolName').css("background-color", "#F5E7E1");
			counter++;
			focusCount++;
		}else if(entityType==3){
			$('#errordiv').append("&#149; "+resourceJSON.msgvalidschool+"<br>");
			if(focusCount	==	0)
				$('#districtORSchoolName').focus();
			$('#districtORSchoolName').css("background-color", "#F5E7E1");
			counter++;
			focusCount++;
		}
		
	}*/
    
    if(userSessionEntityTypeId==1){
    	var flagerrormessage=false;
        	if(entityType==2){	
    			if($('#districtHiddenId').val()=="" || $('#districtHiddenId').val()=="0"){
    				$('#onlyDistrictName').css('background-color','rgb(245, 231, 225)');
    				$('#errordiv').append("&#149; "+resourceJSON.PlzEtrDistrict+"<br>");
    				$('#onlyDistrictName').focus();	
    				counter++;
    				focusCount++;
    			}    	
        	}else if(entityType==3){    		
        		var flagforfocus=true;
        		if($('#districtHiddenId').val()=="" || $('#districtHiddenId').val()=="0"){
        			$('#onlyDistrictName').css('background-color','rgb(245, 231, 225)');
    				$('#errordiv').append("&#149; "+resourceJSON.PlzEtrDistrict+"<br>");
    				$('#onlyDistrictName').focus();	
    				flagerrormessage=true;
    				flagforfocus=false;
    				counter++;
    			}  
    			if($('#districtOrSchooHiddenlId').val()=="" || $('#districtOrSchooHiddenlId').val()=="0"){	
    				$('#districtORSchoolName').css('background-color','rgb(245, 231, 225)');
    				$('#errordiv').append("&#149; "+resourceJSON.PlzEtrSchool+"<br>");
    				if(flagforfocus)
    				$('#districtORSchoolName').focus();
    				flagerrormessage=true;
    				flagforfocus=false;
    				counter++;									
    			}
    			if(flagerrormessage){focusCount++;}
        	}else if(entityType==5){	
			if($('#headQuarterHiddenId').val()=="" || $('#headQuarterHiddenId').val()=="0"){
				$('#onlyHeadQuarterName').css('background-color','rgb(245, 231, 225)');
				$('#errordiv').append("&#149; Please enter Head Quarter<br>");
				$('#onlyHeadQuarterName').focus();	
				counter++;
				focusCount++;
			}    	
    	}else if(entityType==6){    		
    		var flagforfocus=true;
    		if($('#headQuarterHiddenId').val()=="" || $('#headQuarterHiddenId').val()=="0"){
				$('#onlyHeadQuarterName').css('background-color','rgb(245, 231, 225)');
				$('#errordiv').append("&#149; Please enter Head Quarter<br>");
				$('#onlyHeadQuarterName').focus();	
				flagerrormessage=true;
				flagforfocus=false;
				counter++;
			}  
    		var selectHQBranchExists=$('#isBranchExistsHiddenId').val();
			if((selectHQBranchExists=='true' && branchId=="") || (selectHQBranchExists!='false' && branchId=="")){	
				$('#onlyBranchName').css('background-color','rgb(245, 231, 225)');
				$('#errordiv').append("&#149; Please enter Branch<br>");
				if(flagforfocus)
				$('#onlyBranchName').focus();
				flagerrormessage=true;
				flagforfocus=false;
				counter++;									
			}
			if(flagerrormessage){focusCount++;}
    	}
    }
    
    if(userSessionEntityTypeId==2){
    	if(entityType==3){ 
    	if($('#districtOrSchooHiddenlId').val()=="" || $('#districtOrSchooHiddenlId').val()=="0"){	
			$('#districtORSchoolName').css('background-color','rgb(245, 231, 225)');
			$('#errordiv').append("&#149; "+resourceJSON.PlzEtrSchool+"<br>");
			$('#districtORSchoolName').focus();
			counter++;
			focusCount++;
		}
    	}
    }

	if (firstName	==	"")
	{
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.PlzEtrFirstName+"<br>");
		if(focusCount	==	0)
			$('#firstName').focus();
		$('#firstName').css("background-color", "#F5E7E1");
		counter++;
		focusCount++;
	}
	if (lastName	==	"")
	{
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.PlzEtrLastName+"<br>");
		if(focusCount	==	0)
			$('#lastName').focus();
		$('#lastName').css("background-color", "#F5E7E1");
		counter++;
		focusCount++;
	}
	if (emailAddress=="")
	{
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.PlzEtrEmail+"<br>");
		if(focusCount	==	0)
			$('#emailAddress').focus();
		$('#emailAddress').css("background-color", "#F5E7E1");
		counter++;
		focusCount++;
	}
	else if(!isEmailAddress(emailAddress))
	{		
		$('#errordiv').append("&#149; "+resourceJSON.PlzEtrVldEmail+"<br>");
		if(focusCount==0)
			$('#emailAddress').focus();
		
		$('#emailAddress').css("background-color", "#F5E7E1");
		counter++;
		focusCount++;
	}
	if (AEU_RoleId	==	0)
	{
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.msgPleaseselectRole+"<br>");
		if(focusCount	==	0)
			$('#AEU_RoleName').focus();
		$('#AEU_RoleName').css("background-color", "#F5E7E1");
		counter++;
		focusCount++;
		//alert("  domainId "+counter+" focusCount "+focusCount);
	}
	
	var photoPathFile	=	trim(document.getElementById("photoPathFile").value);
	var fileSize = 0;
	
	if ($.browser.msie==true)
 	{	
	    fileSize = 0;	   
	}
	else
	{	
		if(document.getElementById("photoPathFile").files[0]!=undefined)
		{
			fileSize = document.getElementById("photoPathFile").files[0].size;	
		}
	}
	
	if(photoPathFile!="" && photoPathFile!=null)
	{
		var ext = photoPathFile.substr(photoPathFile.lastIndexOf('.') + 1).toLowerCase();		
		if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png'))
		{
			$('#errordiv').show();
			$('#errordiv').append("&#149; "+resourceJSON.msgAcceptablePhotoformats+"<br>");
			if(focusCount	==	0)
			$('#photoPathFile').focus();
			$('#photoPathFile').css("background-color", "#F5E7E1");
			counter++;
			focusCount++;
		}
		else if(fileSize>=10485760)
		{
			$('#errordiv').show();
			$('#errordiv').append("&#149; "+resourceJSON.FileSizeLessthan10Mb+"<br>");
			if(focusCount	==	0)
			$('#photoPathFile').focus();
			$('#photoPathFile').css("background-color", "#F5E7E1");
			counter++;
			focusCount++;
		}
	}
	
	
	if(counter	==	0)
	{
		if(userSessionEntityTypeId==5 || userSessionEntityTypeId==6){
			districtId=$('#districtHiddenId').val();
		}
		
		UserAjax.checkDuplicateUserRecords(userId,emailAddress,districtOrSchooHiddenlId,entityType,districtId,headQuarterId,branchId,{ 
			async: false,
			callback: function(data)
			{
			//************************* Adding by Deepak ************************
			   if((data	==	10))
			   {
				   $('#errordiv').append("&#149; This member is not registered for Head Quarter.Please first register to Head Quarter.<br>");
					if(focusCount==0)
					$('#onlyHeadQuarterName').focus();
					$('#errordiv').show();
					$('#onlyHeadQuarterName').css("background-color", "#F5E7E1");
					$('#loadingDiv').hide();
					return false;
			   }
			   else if((data	==	11))
			   {
				   $('#errordiv').append("&#149; Please enter valid Head Quarter.<br>");
					if(focusCount==0)
					$('#onlyHeadQuarterName').focus();
					$('#errordiv').show();
					$('#onlyHeadQuarterName').css("background-color", "#F5E7E1");
					$('#loadingDiv').hide();
					return false;
			   }
			   else if((data	==	12))
			   {
				   $('#errordiv').append("&#149; Branch has already registered with the email, you provided. Please enter another Branch.<br>");
					if(focusCount==0)
					$('#onlyBranchName').focus();
					$('#errordiv').show();
					$('#onlyBranchName').css("background-color", "#F5E7E1");
					$('#loadingDiv').hide();
					return false;
			//*********************** END *************************************************
			   }
			   else if((data	==	3) || (data	==	4))
				{					
					$('#errordiv').append("&#149; "+resourceJSON.msgaladyemail+"<br>");
					if(focusCount==0)
						$('#emailAddress').focus();
					$('#errordiv').show();
					$('#emailAddress').css("background-color", "#F5E7E1");
					$('#loadingDiv').hide();
					return false;
				}
				else
				{
					document.getElementById("hideSave").style.display='none';
					var roleIdOfLoginUser	=	trim(document.getElementById("roleIdUserLogin").value);
					if(roleIdOfLoginUser==1||roleIdOfLoginUser==4||roleIdOfLoginUser==7)
					{
						$("#attachContactTypeAdduser option").prop('selected',true);
					}
					document.getElementById("userMaster").submit();
					$('#loadingDiv').hide();
					return true;
				}
			},
			errorHandler:handleError 
		});
	}
	else
	{
		$('#errordiv').show();
		$('#loadingDiv').hide();
		return false;
	}
}
function clearFile() {
	// line added by ashish ratan for IE 10-11
	// replace field with old html
	// comment by ashish ratan
	$('#photoPathFile')
			.replaceWith(
					"<input id='photoPathFile' name='photoPathFile' type='file' width='20px;'/>");
	document.getElementById("photoPathFile").value = "";
}
function showFile(userId,docFileName)
{
	if(docFileName!=""){
		UserAjax.showFile(userId,docFileName,{ 
			async: true,
			callback: function(data)
			{	
				document.getElementById("showLogo").innerHTML="<img src=\""+data+"\">";
			},
			errorHandler:handleError 
		});
	}
}
function sendNewPwdToUser(userId)
{
	$('#loadingDiv').fadeIn();
	dwr.engine.beginBatch();			
	UserAjax.sendNewPwdToUser(userId,{ 
		async: true,
		callback: function(data)
		{
			$('#errordiv').empty();
			$('#errordiv').append(data);
			$('#errordiv').show();
			$('#loadingDiv').hide();
		},
		errorHandler:handleError
	});
	dwr.engine.endBatch();
}





var count=0;
var index=-1;
var length=0;
var divid='';
var txtid='';
var hiddenDataArray = new Array();
var showDataArray = new Array();
var degreeTypeArray = new Array();
var hiddenId="";
function getDistrictMasterAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("districtORSchoolName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getDistrictMasterArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getOnlyDistrictAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("onlyDistrictName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getDistrictMasterOnlyArray(txtSearch.value);
		fatchData2(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getDistrictMasterOnlyArray(districtOrSchoolName){

	var searchArray = new Array();
	var entityID	=	document.getElementById("AEU_EntityType").value;
	var districtId=	document.getElementById("districtSessionId").value;
	var branchId=$('#branchHiddenId').val();
	if($('#userSessionEntityTypeId').val()==6){
		branchId=$('#branchId').val();
	}
	var headQuarterId=$('#headQuarterHiddenId').val();
		UserAjax.getFieldOfDistrictListByBranchOrHQId(districtOrSchoolName,branchId,headQuarterId,{ 
			async: false,
			callback: function(data){
			hiddenDataArray = new Array();
			showDataArray = new Array();
			for(i=0;i<data.length;i++){
				searchArray[i]=data[i].districtName;
				showDataArray[i]=data[i].districtName;
				hiddenDataArray[i]=data[i].districtId;
			}
		},
		errorHandler:handleError
		});	
	
	return searchArray;
}

function getDistrictMasterArray(districtOrSchoolName){

	var searchArray = new Array();
	var entityID	=	document.getElementById("AEU_EntityType").value;
	var districtId=	document.getElementById("districtSessionId").value;
	if(entityID==2){
		UserAjax.getFieldOfDistrictList(districtOrSchoolName,{ 
			async: false,
			callback: function(data){
			hiddenDataArray = new Array();
			showDataArray = new Array();
			for(i=0;i<data.length;i++){
				searchArray[i]=data[i].districtName;
				showDataArray[i]=data[i].districtName;
				hiddenDataArray[i]=data[i].districtId;
			}
		},
		errorHandler:handleError
		});	
	}else {		
		if($('#userId').val()!=null && $('#userId').val()!=''){
			if(districtId==null || districtId=='' || districtId < 1)
			districtId=$('#districtHiddenId').val();
		}
		UserAjax.getFieldOfSchoolList(districtId,districtOrSchoolName,{ 
			async: false,
			callback: function(data){
			//alert('distric t====='+districtId);
			hiddenDataArray = new Array();
			showDataArray = new Array();
			for(i=0;i<data.length;i++){				
				searchArray[i]=data[i].schoolName;
				showDataArray[i]=data[i].schoolName;
				hiddenDataArray[i]=data[i].schoolId;
			}
		},
		errorHandler:handleError
		});	
	}
	
	

	return searchArray;
}


var selectFirst="";

var fatchData= function(txtSearch,searchArray,txtId,txtdivid){
	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
				
			}
			
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}

var fatchData2= function(txtSearch,searchArray,txtId,txtdivid){
	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
				
			}
			
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}

function hideDistrictDiv(dis,hiddenId,divId)
{
	document.getElementById("districtSessionId").value="";
	$('#errordiv').empty();
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
	
		if(dis.value==""){
				
				$('#districtORSchoolName').attr('readonly', true);
				document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			$('#districtORSchoolName').attr('readonly', false);
			dis.value=showDataArray[index];
			document.getElementById("districtSessionId").value=hiddenDataArray[index];
		}
		
	}else{
		if(document.getElementById(hiddenId))
		{
			document.getElementById(hiddenId).value="";
		}
		if(dis.value!=""){
			var focuschk=0;
			onlyDistrictName=2;
			$('#errordiv').empty();	
			$('#errordiv').show();
			
				$('#errordiv').append("&#149; "+resourceJSON.msgValidDistrictName+"<br>");
			
			
			if(focus==0)
			$('#onlyDistrictName').focus();
			
			focus++;
		}
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}

function hideDistrictMasterDiv(dis,hiddenId,divId)
{
	document.getElementById("districtOrSchooHiddenlId").value="";
	$('#errordiv').empty();
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
	
		if(dis.value==""){
				document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			document.getElementById("districtOrSchooHiddenlId").value=hiddenDataArray[index];
		}
		
	}else{
		if(document.getElementById(hiddenId))
		{
			document.getElementById(hiddenId).value="";
		}
		if(dis.value!=""){
			var focuschk=0;
			districtORSchoolNameCount=2;
			$('#errordiv').empty();	
			$('#errordiv').show();
			if(document.getElementById("AEU_EntityType").value==2){
				$('#errordiv').append("&#149; "+resourceJSON.msgValidDistrictName+"<br>");
			}else if(document.getElementById("AEU_EntityType").value==3){
				$('#errordiv').append("&#149; "+resourceJSON.msgvalidschool+"<br>");
			}
			
			if(focus==0)
			$('#districtORSchoolName').focus();
			
			focus++;
		}
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}

var downArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index<length-1){
			for(var i=0;i<10;i++){
				{
					if(document.getElementById('divResult'+txtdivid+i))
						var div_id=document.getElementById('divResult'+txtdivid+i);
					if(div_id)
					{
						if(div_id.className=='over')
							index=div_id.id.split('divResult'+txtdivid)[1];
						div_id.className='normal';
					}
				}
			}
			index++;
			if(document.getElementById('divResult'+txtdivid+index))
			{
				var div_id=document.getElementById('divResult'+txtdivid+index);
				div_id.className='over';
				document.getElementById(txtid).value = div_id.innerHTML;
				selectFirst=div_id.innerHTML;
			}
		}
	}
}

var upArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index>0){
			for(var i=0;i<length;i++){

				var div_id=document.getElementById('divResult'+txtdivid+i);
				if(div_id)
				{
					if(div_id.className=='over')
						index=div_id.id.split('divResult'+txtdivid)[1];
					div_id.className='normal';
				}
			}
			index--;
			if(document.getElementById('divResult'+txtdivid+index))
				document.getElementById('divResult'+txtdivid+index).className='over';
			if(txtid && document.getElementById('divResult'+txtdivid+index))
			{
				document.getElementById(txtid).value=
					document.getElementById('divResult'+txtdivid+index).innerHTML;
				selectFirst=document.getElementById('divResult'+txtdivid+index).innerHTML;
			}
		}
	}
}

var overText=function (div_value,txtdivid) {

	for(var i=0;i<length;i++)
	{
		if(document.getElementById('divResult'+i))
		{
			var div_id=document.getElementById('divResult'+i);

			if(div_id.className=='over')
				index=div_id.id.split(txtdivid)[1];
			div_id.className='normal';
		}
	}
	div_value.className = 'over';
	document.getElementById(txtid).value= div_value.innerHTML;
}
function trim(s)
{
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}
function mouseOverChk(txtdivid,txtboxId)
{	
	for(var i=0;i<length;i++)
	{
		$('#divResult'+txtdivid+i).mouseover({param1: i,param2:txtdivid, param3:txtboxId}, fireMouseOverEvent);
	}
}


function fireMouseOverEvent(event)
{
	for(var i=0;i<length;i++)
	{	
		document.getElementById('divResult'+event.data.param2+i).className='normal';
	}
    document.getElementById('divResult'+event.data.param2+event.data.param1).className='over';	       
   	document.getElementById(event.data.param3).value= $('#divResult'+event.data.param2+event.data.param1).text();
    index=event.data.param1;
}


/***************************************************************auto complete ******************************************************/
//add by 18-05-2015
var isBranchExistsArray = new Array();
function getOnlyHeadQuarterAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	//alert('enter1');
	//document.getElementById("onlyHeadQuarterName").value="";
	document.getElementById("headQuarterHiddenId").value="";
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("onlyHeadQuarterName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getHeadQuarterOnlyArray(txtSearch.value);
		//alert(searchArray);
		fatchData2(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}
function getOnlyHeadQuarterAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	document.getElementById("headQuarterHiddenId").value="";
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("onlyDistrictName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		//alert('hljkhlkhl================'+txtSearch.value);
		document.getElementById(divid).style.display='block';
		searchArray = getHeadQuarterOnlyArray(txtSearch.value);
		//alert(searchArray);
		fatchData2(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}
function getHeadQuarterOnlyArray(headQuarterName){
	
	var searchArray = new Array();
	//alert("========="+headQuarterName);

			AutoSearchFilterAjax.getFieldOfHeadQuarterList(headQuarterName,{ 
			async: false,
			callback: function(data){
			isBranchExistsArray=new Array();
			hiddenDataArray = new Array();
			showDataArray = new Array();
			for(i=0;i<data.length;i++){				
				searchArray[i]=data[i].headQuarterName;
				showDataArray[i]=data[i].headQuarterName;
				hiddenDataArray[i]=data[i].headQuarterId;
				isBranchExistsArray[i]=data[i].isBranchExist;
			}
		},
		errorHandler:handleError
		});	
	
	return searchArray;
}
function hideHeadQuarterDiv(dis,hiddenId,divId)
{
	$('#errordiv').empty();
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
			document.getElementById("headQuarterHiddenId").value=hiddenDataArray[index];
		}
	
		if(dis.value==""){
				
				//$('#onlyHeadQuarterName').attr('readonly', true);
				document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			$('#onlyHeadQuarterName').attr('readonly', false);
			dis.value=showDataArray[index];
			document.getElementById("headQuarterHiddenId").value=hiddenDataArray[index];
			document.getElementById("headQuarterHiddenId").value=hiddenDataArray[index];
			document.getElementById("isBranchExistsHiddenId").value=isBranchExistsArray[index];
			
		}
		
	}else{
		if(document.getElementById(hiddenId))
		{
			document.getElementById(hiddenId).value="";
		}
		if(dis.value!=""){
			var focuschk=0;
			onlyDistrictName=2;
			$('#errordiv').empty();	
			$('#errordiv').show();
			
				$('#errordiv').append("&#149; Please enter valid Head Quarter<br>");
			
			
			if(focus==0)
			$('#onlyHeadQuarterName').focus();			
			focus++;
		}
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
		//alert($('#onlyHeadQuarterName').val());
		if($('#onlyHeadQuarterName').val().trim()!=''){			
			$('#districtORSchoolName').attr('readonly',true);
			$('#onlyBranchName').attr('readonly',true);
			$('#onlyDistrictName').attr('readonly',true);
		}
		if($('#AEU_EntityType').val()=='3' && $('#onlyHeadQuarterName').val()=='' && $('#onlyBranchName').val()==''){
			$('#districtORSchoolName').attr('readonly',true);
			$('#onlyDistrictName').attr('readonly',false);
		}
		if($('#AEU_EntityType').val()!='5' && $('#isBranchExistsHiddenId').val()!='' && $('#isBranchExistsHiddenId').val().trim()=='true'){
			$('#branchClass').fadeIn();
			$('#onlyBranchName').attr('readonly',false);
			$('#onlyBranchName').show();
		}else if($('#isBranchExistsHiddenId').val()!=''){
			$('.branchClass').hide();
			if($('#AEU_EntityType').val()=='3'){
				try{
					$('#onlyDistrictName').attr('readonly',false);
					$('#onlyDistrictName').focus();
					$('#onlyDistrictName').val('');
					$('#districtORSchoolName').val('');
					}catch(e){}			
			}else{
				try{
					$('#districtORSchoolName').attr('readonly',false);
					$('#onlyDistrictName').attr('readonly',false);
					$('#districtORSchoolName').focus();
					$('#onlyBranchName').val('');
					$('#districtORSchoolName').val('');
					}catch(e){}
			}
		}
	}
	index = -1;
	length = 0;
}

function getOnlyBranchAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	//alert('enter1');
	//document.getElementById("onlyHeadQuarterName").value="";
	document.getElementById("branchHiddenId").value="";
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("onlyBranchName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getBranchOnlyArray(txtSearch.value);
		//alert(searchArray);
		fatchData2(txtSearch,searchArray,txtId,txtdivid);		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}
function getOnlyBranchAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	document.getElementById("branchHiddenId").value="";
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("onlyBranchName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		//alert('hljkhlkhl================'+txtSearch.value);
		document.getElementById(divid).style.display='block';
		searchArray = getBranchOnlyArray(txtSearch.value);
		//alert(searchArray);
		fatchData2(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}
function getBranchOnlyArray(branchName){
	
	var searchArray = new Array();
	//alert("========="+branchName);
	var headQuarterId=$('#headQuarterHiddenId').val();
	if($('#userSessionEntityTypeId').val()==5){
		headQuarterId=$('#headQuarterId').val();
	}
	
	AutoSearchFilterAjax.getBranchListByHQ(headQuarterId,branchName,{ 
			async: false,
			callback: function(data){
			hiddenDataArray = new Array();
			showDataArray = new Array();
			for(i=0;i<data.length;i++){				
				searchArray[i]=data[i].branchName;
				showDataArray[i]=data[i].branchName;
				hiddenDataArray[i]=data[i].branchId;
			}
		},
		errorHandler:handleError
		});	
	
	return searchArray;
}
function hideBranchDiv(dis,hiddenId,divId)
{
	$('#errordiv').empty();
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
			document.getElementById("branchHiddenId").value=hiddenDataArray[index];
		}
	
		if(dis.value==""){
				
				//$('#onlyBranchName').attr('readonly', true);
				document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			$('#onlyBranchName').attr('readonly', false);
			dis.value=showDataArray[index];
			document.getElementById("branchHiddenId").value=hiddenDataArray[index];
			document.getElementById("branchHiddenId").value=hiddenDataArray[index];
		}
		
	}else{
		if(document.getElementById(hiddenId))
		{
			document.getElementById(hiddenId).value="";
		}
		if(dis.value!=""){
			var focuschk=0;
			onlyDistrictName=2;
			$('#errordiv').empty();	
			$('#errordiv').show();
			
				$('#errordiv').append("&#149; Please enter valid Branch<br>");
			
			
			if(focus==0)
			$('#onlyBranchName').focus();			
			focus++;
		}
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
		//alert($('#onlyHeadQuarterName').val());
		if($('#onlyBranchName').val().trim()!=''){			
			$('#districtORSchoolName').attr('readonly',true);
			$('#onlyDistrictName').attr('readonly',true);
		}else if($('#AEU_EntityType').val()=='3' && $('#onlyHeadQuarterName').val()!='' && $('#isBranchExistsHiddenId').val()!='' && $('#isBranchExistsHiddenId').val().trim()=='true' && $('#onlyBranchName').val().trim()==''){
			$('#districtORSchoolName').attr('readonly',true);
			$('#onlyDistrictName').attr('readonly',true);
		}else if($('#AEU_EntityType').val()=='3' && $('#onlyHeadQuarterName').val()=='' && $('#onlyBranchName').val()==''){
			$('#districtORSchoolName').attr('readonly',true);
			$('#onlyDistrictName').attr('readonly',false);
		}
		if($('#onlyBranchName').val()!=''){			
			if($('#AEU_EntityType').val()=='3'){
				try{
					$('#onlyDistrictName').attr('readonly',false);
					$('#onlyDistrictName').focus();
					$('#onlyDistrictName').val('');
					$('#districtORSchoolName').val('');
					$('#districtHiddenId').val();
					$('#schoolHiddenId').val();
					}catch(e){}			
			}else{
				try{
					$('#districtORSchoolName').attr('readonly',false);
					$('#onlyDistrictName').attr('readonly',false);
					$('#districtORSchoolName').focus();
					$('#districtORSchoolName').val('');
					$('#schoolHiddenId').val();
					$('#districtHiddenId').val();
					}catch(e){}
			}
		}
	}
	index = -1;
	length = 0;
}

$(function(){
	function clearBackground(){
		$('#onlyDistrictName').css("background-color", "");
		$('#onlyBranchName').css("background-color", "");
		$('#onlyHeadQuarterName').css("background-color", "");
		$('#districtORSchoolName').css("background-color", "");
		}
	$('.clearback').click(function(){clearBackground();});
	
	$('#AEU_EntityType').change(function(){
		$('#onlyDistrictName').val('');
		$('#districtHiddenId').val('');
		$('#districtORSchoolName').val('');
		$('#districtOrSchooHiddenlId').val('');
		$('#schoolHiddenId').val('');
		$('#onlyBranchName').val('');
		$('#branchHiddenId').val('');
		$('#onlyHeadQuarterName').val('');
		$('#headQuarterHiddenId').val('');
	});

	$('#onlyHeadQuarterName').blur(function(){		
		if($('#onlyHeadQuarterName').val()!=null && $('#onlyHeadQuarterName').val()!=""){
		$('#onlyDistrictName').val('');
		$('#districtHiddenId').val('');
		$('#districtORSchoolName').val('');
		//$('#districtOrSchooHiddenlId').val('');
		$('#schoolHiddenId').val('');
		$('#onlyBranchName').val('');
		$('#branchHiddenId').val('');
		}else{
			$('#onlyBranchName').val('');
			$('#branchHiddenId').val('');
			$('#onlyBranchName').attr('readonly',true);
		}
		});
	$('#onlyBranchName').blur(function(){
		if($('#onlyBranchName').val()!=null && $('#onlyBranchName').val()!=""){
		$('#onlyDistrictName').val('');
		$('#districtHiddenId').val('');
		$('#districtORSchoolName').val('');
		//$('#districtOrSchooHiddenlId').val('');
		$('#schoolHiddenId').val('');
		}
		});
	$('#onlyDistrictName').blur(function(){
		if($('#onlyDistrictName').val()!=null && $('#onlyDistrictName').val()!=""){		
		$('#districtORSchoolName').val('');
		//$('#districtOrSchooHiddenlId').val('');
		$('#schoolHiddenId').val('');
		}else{
			$('#districtORSchoolName').val('');
			$('#schoolHiddenId').val('');
			$('#districtORSchoolName').attr('readonly',true);
		}
		});
});

function getKeyContactTypeByEntityType()
{	
	var entity_type	=	document.getElementById("AEU_RoleName").value;
	document.getElementById("keyContactTypeAdduser").style.display='none';
	if(entity_type==7)
	{
		document.getElementById("keyContactTypeAdduser").style.display='block';
	}
	
}
function showKeyContact()
{
	document.getElementById("keyContactTable").style.display='block';
}
//end by 18-05-2015
/***************************************************************End********************************************************************/