var page = 1;
var noOfRows = 10;
var sortOrderStr="";
var sortOrderType="";
function getPaging(pageno)
{	
	if(pageno!='')
	{
		page=pageno;	
	}else
	{
		page=1;
	}
	
	noOfRows = document.getElementById("pageSize").value;
	var entityID	=	document.getElementById("entityID").value;
	if(entityID==1){
		var searchTextId	=	document.getElementById("districtIdFilter").value;
	}	
	displayExamCodes();
}
function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
	var entityID	=	document.getElementById("entityID").value;
	if(pageno!=''){
		page=pageno;	
	}else{
		page=1;
	}
	sortOrderStr	=	sortOrder;
	sortOrderType	=	sortOrderTyp;
	if(document.getElementById("pageSize")!=null){
		noOfRows = document.getElementById("pageSize").value;
	}else{
		noOfRows=10;
	}
	if(entityID==1){
		var searchTextId	=	document.getElementById("districtIdFilter").value;
	}
	displayExamCodes123("","-1","-1");
}

function chkForEnterSearchTags(evt)
{
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==13)
	{
		page = 1;
		displayExamCodes();
	}	
}
function chkForEnterSaveTags(evt)
{
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==13)
	{
		saveAndUploadTagsIcon();
	}	
}

/*
function displayExamCodes()
{	
	var entityID = document.getElementById("entityID").value;
	var districtIdFilter=0;
	if(entityID==1)
	{
		districtIdFilter = document.getElementById("districtIdFilter").value;
	}	
	
	var startDate= "";
	var endDate = "";
	var firstName = "";
	var lastName = "";
	var emailAddress = "";
	var examCode = "";
	
	ExamAjax.displayData(entityID,districtIdFilter,noOfRows,page,sortOrderStr,sortOrderType,startDate,endDate,firstName,lastName,emailAddress,examCode,"",{ 
		async: true,
		callback: function(data){
		$('#examCodesGrid').html(data);
		applyScrollOnTbl();
	},
	});
}
*/

function displayExamCodes(teachersIdStr,startDateStr,endDateStr)
{
	var entityID = document.getElementById("entityID").value;
	var districtIdFilter=0;
	if(entityID==1)
	{
		districtIdFilter = document.getElementById("districtIdFilter").value;
	}	
	
	var startDate= "-1";
	var endDate = "-1";
	var firstName = "";
	var lastName = "";
	var emailAddress = "";
	var examCode = "";
	var teachersId = "";
	
	if(startDateStr=="-1")
		startDate = startDateStr;
	else
	if($("#examDate").val() !=null)
		startDate= $("#examDate").val();
	
	if(endDateStr=="-1")
		endDate = endDateStr;
	
	if($("#examCode").val() !=null)
		examCode = $("#examCode").val();
	
	if(teachersIdStr==null || teachersIdStr=="") //get Data from teacherIds input type;
		teachersIdStr = $("#teacherIds").val();
	
	if(teachersIdStr!=null && teachersIdStr!="")
	{
		$("#teacherIds").val(teachersIdStr);
		teachersId = teachersIdStr;
	}
	
	//alert("startDate:- "+startDate+"\nendDate:- "+endDate+"\nfirstName:- "+firstName+"\nlastName:- "+lastName+"\nemailAddress:- "+emailAddress+"\nexamCode:- "+examCode+"\nteacherIds:- "+teachersId);
	ExamAjax.displayData(entityID,districtIdFilter,noOfRows,page,sortOrderStr,sortOrderType,startDate,endDate,firstName,lastName,emailAddress,examCode,teachersId,{ 
		async: true,
		callback: function(data){
		$('#examCodesGrid').html(data);
		var win = $("#kendoWindow").data("kendoWindow");
		win.open();
		},
	});
}

function scheduler_save(e) {
	//alert('scheduler_save::::');
}

function scheduler_add (e) {
	//alert('scheduler_add::::');
}

function objToString (obj)
{
	var str = '';
	for (var p in obj) {
		if (obj.hasOwnProperty(p)) {
			str += p + '::' + obj[p] + '\n';
		}
	}
	return str;
}

var event = "";
function scheduler_navigate(e)
{
	if(e.action="workView")
		event = "workView";
}

function scheduler_change(e)
{
	var events = e.events; //list of selected Scheduler events
	var centreScheduleId ="";
	try{
		centreScheduleId =  events[events.length - 1].centerId;
	}
	catch(e){}
	//alert(" centreScheduleId ::: "+centreScheduleId);
	if((centreScheduleId=="" && events.length==0  &&  event!="workView") || (centreScheduleId!=null && centreScheduleId > 0 &&  event!="workView" ))
	{
		var dt = e.start;
		var dateTime = ("0" + (dt.getMonth() + 1)).slice(-2) + '/' + ("0" + (dt.getDate())).slice(-2) + '/' +  dt.getFullYear()+" "+dt.getHours()+":"+addZero(dt.getMinutes());
		CGServiceAjax.getAvailability(dt,{ 
			async: false,
			errorHandler:handleError,
			callback: function(data){
			if(data.length > 0)
			{
				$('#availabilityGrid').html(data);
				/* overflow: auto; */
				var win = $("#availableKendoWindow").data("kendoWindow");
				win.open();
				
				document.getElementById('previousPriority').value = $("#priorityIds option:selected").val();
			}
			}
		});
	}
	else
	{
		var teacherValues=events[events.length - 1].tIds;
		if(teacherValues!=null && teacherValues!="" && event!="workView")
		{
			displayExamCodes(teacherValues,'-1','-1');
			$("#teacherIds").val(teacherValues);
		}
	}
	
}

function displayGridWithAssesmentTime()
{
	var examDate = "";
	var examCode = "";
	
	if($("#examDate").val()!=null)
		examDate = $("#examDate").val();
	if($("#examCode").val()!=null)
		examCode = $("#examCode").val();
	
	//destroy the kendo Schedule if it is created.
	$("#scheduler").empty();
	var scheduler = $("#scheduler").data("kendoScheduler");
	if(scheduler!=null)
		scheduler.destroy();
	
	var current = new Date();     // get current date
	if( !(examDate==null || examDate=="") )
		current = new Date(examDate);     // get current date
	
	var weekstart = current.getDate() - current.getDay() +1;
	//var weekend = weekstart + 4;       // end day is the first day + 6 
	var monday = new Date(current.setDate(weekstart));
	//var friday = new Date(current.setDate(weekend));
	
	var myDate = new Date();
	//if(examDate!="" && examDate!=null)
	//	myDate = new Date(examDate);
	
	
	ExamAjax.displayTeacherAssesmentTime(examDate,examCode,{ 
		async: false,
		callback: function(data){
			var bookings = [{field: "centerId",dataSource: data[1]}];
			//alert(JSON.stringify(data[1]));
			$("#scheduler").kendoScheduler({
			date:monday,
			allDaySlot: false,
			showWorkHours:true,
			height: 400,
			//currentTimeMarker: {useLocalTimezone: false},
			//timezone : "US/Central",
			//majorTick: 60,
			views: [{type: "workWeek", selected: true}],
			selectable: true,
			editable: false,
			navigate:scheduler_navigate,
			save: scheduler_save,
			add: scheduler_add,
			change: scheduler_change,			
			dataSource: data[0],
			resources: bookings
		});
	},
	});
}


/********************************** district auto completer ***********************************/

var count=0;
var index=-1;
var length=0;
var divid='';
var txtid='';
var hiddenDataArray = new Array();
var showDataArray = new Array();
var degreeTypeArray = new Array();
var hiddenId="";
var districtNameFilter=0;
function getDistrictMasterAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		if(districtNameFilter==1)
			document.getElementById("districtNameFilter").focus();
		else
			document.getElementById("districtName").focus();
		
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getDistrictMasterArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getDistrictMasterArray(districtName){
	var searchArray = new Array();
	ManageJobOrdersAjax.getFieldOfDistrictList(districtName,{ 
		async: false,
		callback: function(data){
		hiddenDataArray = new Array();
		showDataArray = new Array();
		for(i=0;i<data.length;i++){
			searchArray[i]=data[i].districtName;
			showDataArray[i]=data[i].districtName;
			hiddenDataArray[i]=data[i].districtId;
		}
	},
	errorHandler:handleError 
	});	
	return searchArray;
}


var selectFirst="";

var fatchData= function(txtSearch,searchArray,txtId,txtdivid){
	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
				
			}
			
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}

function hideDistrictMasterDiv(dis,hiddenId,divId)
{
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			
		}
		
	}else{
			if(document.getElementById(hiddenId))
				document.getElementById(hiddenId).value="";
	}
	
	if(document.getElementById(divId))
	{	
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}

var downArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index<length-1){
			for(var i=0;i<10;i++){
				{
					if(document.getElementById('divResult'+txtdivid+i))
						var div_id=document.getElementById('divResult'+txtdivid+i);
					if(div_id)
					{
						if(div_id.className=='over')
							index=div_id.id.split('divResult'+txtdivid)[1];
						div_id.className='normal';
					}
				}
			}
			index++;
			if(document.getElementById('divResult'+txtdivid+index))
			{
				var div_id=document.getElementById('divResult'+txtdivid+index);
				div_id.className='over';
				document.getElementById(txtid).value = div_id.innerHTML;
				selectFirst=div_id.innerHTML;
			}
		}
	}
}

var upArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index>0){
			for(var i=0;i<length;i++){

				var div_id=document.getElementById('divResult'+txtdivid+i);
				if(div_id)
				{
					if(div_id.className=='over')
						index=div_id.id.split('divResult'+txtdivid)[1];
					div_id.className='normal';
				}
			}
			index--;
			if(document.getElementById('divResult'+txtdivid+index))
				document.getElementById('divResult'+txtdivid+index).className='over';
			if(txtid && document.getElementById('divResult'+txtdivid+index))
			{
				document.getElementById(txtid).value=
					document.getElementById('divResult'+txtdivid+index).innerHTML;
				selectFirst=document.getElementById('divResult'+txtdivid+index).innerHTML;
			}
		}
	}
}

var overText=function (div_value,txtdivid) {

	for(var i=0;i<length;i++)
	{
		if(document.getElementById('divResult'+i))
		{
			var div_id=document.getElementById('divResult'+i);

			if(div_id.className=='over')
				index=div_id.id.split(txtdivid)[1];
			div_id.className='normal';
		}
	}
	div_value.className = 'over';
	document.getElementById(txtid).value= div_value.innerHTML;
}
function trim(s)
{
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}
function mouseOverChk(txtdivid,txtboxId)
{	
	for(var i=0;i<length;i++)
	{
		$('#divResult'+txtdivid+i).mouseover({param1: i,param2:txtdivid, param3:txtboxId}, fireMouseOverEvent);
	}
}


function fireMouseOverEvent(event)
{
	for(var i=0;i<length;i++)
	{	
		document.getElementById('divResult'+event.data.param2+i).className='normal';
	}
    document.getElementById('divResult'+event.data.param2+event.data.param1).className='over';	       
   	document.getElementById(event.data.param3).value= $('#divResult'+event.data.param2+event.data.param1).text();
    index=event.data.param1;
}

function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert(resourceJSON.msgSomeerror+": "+exception.javaClassName);}
}

function showConfirmationpopup(codeId,plock_unlock)
{
	var kwin = $("#kendoWindow").data("kendoWindow");
	var showpopupWindow = $("#showpopupKendoWindow").data("kendoWindow");
	
	kwin.close();	//default close window.
	
	$('#withoutPermissionClose').hide();
	$('#withoutPermissionCancel').hide();
	$('#withoutPermissionOk').hide();
	$('#withoutPermissionOk1').hide();
	$('#withoutPermissionOk2').hide();
	
	//add By Ram Nath
	if(plock_unlock==1){
		//end By Ram Nath
		$('#withoutPermission').hide();
		$('#withoutPermissionOk').hide();
		$('#withoutPermissionClose').hide();
		
	//$("#examCodesGridpopup").modal("hide");
	$("#codeId").attr("value",codeId);
	//$("#showpopup").modal("show");
	
	var value = $("#"+codeId).attr("name");
	if(value=="Approve")
	{
		$("#confirmationMessage1").show();
		$("#confirmationMessage2").hide();
		
		$('#withoutPermissionOk2').show();
		$('#withoutPermissionCancel').show();
		$('#withoutPermissionOk1').hide();
		$('#withoutPermissionOk').hide();
		
		
	}
	else
	{
		$("#confirmationMessage1").hide();
		$("#confirmationMessage2").show();
		
		$('#withoutPermissionOk1').show();
		$('#withoutPermissionCancel').show();
		$('#withoutPermissionOk').hide();
		$('#withoutPermissionOk2').hide();
	}
	}
	//add By Ram nath
	else{
		
		//showpopupWindow.open();
		
		$('#withoutPermissionOk1').hide();
		$('#withoutPermissionOk2').hide();
		$('#withoutPermissionCancel').hide();
		$('#withoutPermissionClose').hide();
		
		
		$("#confirmationMessage1").hide();
		$("#confirmationMessage2").hide();
		//$("#examCodesGridpopup").modal("hide");
		//$("#showpopup").modal("show");
		
		$('#withoutPermission').show();
		$('#withoutPermissionClose').show();
		
/*		$('#showpopup #withoutPermission').show();
		$('#showpopup .withoutPermission').show();
*/
	}
	showpopupWindow.open();
	//End By Ram Nath
}

function confirmationOk()
{
	var showpopupWindow = $("#showpopupKendoWindow").data("kendoWindow");
	showpopupWindow.close();
	//$("#showpopup").modal("hide");
	
	var codeId = $("#codeId").val();
	//$("#loadingDiv").modal("show");
	
	ExamAjax.ApproveAndDisapprove(codeId,{
		async: true,
		callback: function(data){
				var value = $("#"+codeId).attr("name");
				if(value=="Approve")
				{
					$("#"+codeId).attr("name","Disapprove");
					document.getElementById(""+codeId).innerHTML="Lock";
				}
				else
				{
					$("#"+codeId).attr("name","Approve");
					document.getElementById(""+codeId).innerHTML="Unlock";
				}
				
				//refresh the kendow window
				var tID = $("#teacherIds").val();
				var sDate = $("#examDate").val();
				var eDate = "-1";
				displayExamCodes(tID,sDate,eDate);
				
				//$("#examCodesGridpopup").modal("show");
			},
	});
}

function confirmationClose()
{
	var kwin = $("#kendoWindow").data("kendoWindow");
	var showpopupWindow = $("#showpopupKendoWindow").data("kendoWindow");
	
	
	showpopupWindow.close();
	kwin.open();
	
	//$("#showpopup").modal("hide");
	//$("#examCodesGridpopup").modal("show");
}

function confirmationGridClose()
{
	var kwin = $("#kendoWindow").data("kendoWindow");
	var showpopupWindow = $("#showpopupKendoWindow").data("kendoWindow");
	
	showpopupWindow.close();
	kwin.close();
}

function addZero(i) {
    if (i < 10) {
        i = "0" + i;
    }
    return i;
}

function closeAvailabilityKendo()
{
	var availabilityWindow = $("#availableKendoWindow").data("kendoWindow");
	availabilityWindow.close();	
}
function setAvailabilityFlag(flag)
{
	document.getElementById('availFlag').value=flag;
}
function setPriorityFlag()
{
	document.getElementById('priorityFlag').value=$("#priorityIds option:selected").val();
}
function saveDAAvailability()
{
	var errorCount = 0;
	var fromTime = 0;
	var toTime = 0;
	var dateTime = document.getElementById('dateTime').value;	
	//var availabilityFlag = document.getElementById('availFlag').value;
	var availabilityFlag = document.getElementById('priorityFlag').value;
	//var assessmentId = document.getElementById('assessmentIds').value;
	fromTime = parseInt(document.getElementById('fromTime').value);
	toTime =  parseInt(document.getElementById('toTime').value);
	var overrideFlag = document.getElementById('overrideFlag').value;
	/*alert("Date Time :: "+dateTime);
	alert("Availability Flag :: "+availabilityFlag);
	alert("From Time :: "+fromTime);
	alert("toTime :: "+toTime);
	alert("Availability Flag :: "+availabilityFlag);
	alert(" overrideFlag : "+overrideFlag);*/
	$('#errorDiv').empty();
	if(availabilityFlag=="")
	{
		availabilityFlag = 1;
	}
	/*if(overrideFlag=="")
	{
		overrideFlag = 1;
	}*/
	var errorId = "";
	if($("#priorityIds option:selected").val()==-1)
	{
		errorCount++;
		$('#errorDiv').show();
		$('#errorDiv').append("&#149; "+resourceJSON.msgPleaseSelectPriority+"<br>");	
		if(errorCount==1)
			$('#priorityIds').focus();
		
	}
	if(fromTime=="-1")
	{
		errorCount++;
		$('#errorDiv').show();
		$('#errorDiv').append("&#149; "+resourceJSON.msgPleaseselectstarttime+"<br>");
		if(errorCount==1)
			$('#fromTime').focus();		
	}
	if(toTime=="-1")
	{
		errorCount++;
		$('#errorDiv').show();
		$('#errorDiv').append("&#149; "+resourceJSON.msgPleaseSelectendtime+"<br>");
		
		if(errorCount==1)
			$('#toTime').focus();
		
	}
	
	
	if(fromTime > toTime && toTime!="-1")
	{
		$('#errorDiv').show();
		$('#errorDiv').append("&#149; "+resourceJSON.msgEndTimeLessStTime+"<br>");
		$('#fromTime').focus();
		errorCount++;
	}
	else if(fromTime!="-1" && toTime!="-1" &&  fromTime == toTime)
	{
		$('#errorDiv').show();
		$('#errorDiv').append("&#149; "+resourceJSON.msgEndEqualStartTime+"<br>");
		$('#fromTime').focus();
		errorCount++;
	}
	if(overrideFlag.length>0 || availabilityFlag.length>0)
	{
		
	}
	
	if(errorCount==0)
	{
		ExamAjax.saveDaAvailabilities(dateTime,availabilityFlag,fromTime,toTime,overrideFlag,{ 
			async: false,
			errorHandler:handleError,
			callback: function(data){
			var win = $("#availableKendoWindow").data("kendoWindow");
			win.close();
			displayGridWithAssesmentTime();
		}
		});
	}
}

function forcelyChange(date)
{
 	var priority = $("#priorityIds option:selected").html();
 	var priorityId =  $("#priorityIds option:selected").val();
	document.getElementById('priorityFlag').value = priorityId;
	document.getElementById('overrideFlag').value = priorityId;
	var win = $("#prioMSGKendoWindow").data("kendoWindow");
	win.open();
	document.getElementById('changePriorDate').innerHTML=date;
	document.getElementById('changePriorValue').innerHTML=priority;
	document.getElementById('lckbtn').style.display="none";
	document.getElementById('cnclbtn').style.display="block";
}

function showlockBtn()
{
	var win = $("#prioMSGKendoWindow").data("kendoWindow");
	win.close();
	document.getElementById('cnclbtn').style.display="none";
	document.getElementById('lckbtn').style.display="block";
	document.getElementById('fromTime').disabled=true;
	document.getElementById('toTime').disabled=true;
}

function closePrioInfo()
{
	var flagValue = document.getElementById('overrideFlag').value;	
	document.getElementById('priorityFlag').value=flagValue;
	document.getElementById('overrideFlag').value="";
	document.getElementById('fromTime').disabled=false;
	document.getElementById('toTime').disabled=false;
	//alert("previous flag ::"+document.getElementById('previousPriority').value);
	document.getElementById("priorityIds").value = document.getElementById('previousPriority').value;
	var win = $("#prioMSGKendoWindow").data("kendoWindow");
	win.close();
	
}