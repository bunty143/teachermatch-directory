var deviceTypeAndroid = $.browser.device = (/Android|webOS|BlackBerry|IEMobile|Opera Mini/i
		.test(navigator.userAgent.toLowerCase()));
var deviceType = $.browser.device = (/iPhone|iPad|iPod/i
		.test(navigator.userAgent.toLowerCase()));
var isSafari = /Safari/.test(navigator.userAgent)
		&& /Apple Computer/.test(navigator.vendor);

var page = 1;
var noOfRows = 10;
var sortOrderStr = "";
var sortOrderType = "";
/*var domainpage = 1;
var domainnoOfRows = 10;
var domainsortOrderStr = "";
var domainsortOrderType = "";*/

function handleError(message, exception) {
	if (exception.javaClassName == "java.lang.IllegalStateException") {
		alert(resourceJSON.oops);
		document.location = 'signin.do';
	} else {
		alert(resourceJSON.msgSomeerror+": " + exception.javaClassName);
	}
}

function getPaging(pageno) {
	if (pageno != '') {
		page = pageno;
	} else {
		page = 1;
	}

	noOfRows = document.getElementById("pageSize").value;
	displayTemplateGrids();
}

function getPagingAndSorting(pageno, sortOrder, sortOrderTyp) {
	var gridNo = document.getElementById("gridNo").value;

		if (pageno != '') {
			page = pageno;
		} else {
			page = 1;
		}

		sortOrderStr = sortOrder;
		sortOrderType = sortOrderTyp;

		if(document.getElementById("pageSize")!=null){
			noOfRows = document.getElementById("pageSize").value;
		}else{
			noOfRows=10;
		}
		
		displayTemplateGrids();
	
}

function addTemplate()
{
	$('#errordiv').empty();
	$("#addeditevent").show();
	//document.getElementById("districtId").value = "";
	//document.getElementById("districtName").value = "";
	$('#messagetext').find(".jqte_editor").html("");
	document.getElementById("eventtitle").value="";
	document.getElementById("templateId").value="";
	document.getElementById('subjectforParticipants').value="";
	document.getElementById("eventtitle").focus();
	
	var templateType = document.getElementById("templateType").value;

	if(templateType==1)
		$("#tooltipSpan").show();
	else
		$("#tooltipSpan").hide();

	clearColor();
	
}

function cancelEvent() {
	$("#maineventtableDiv").show();
	$("#eventsearchDiv").show();
	$('#errordiv').empty();
	$("#addeditevent").hide();
}

function clrEventDiv() {
	$("#addeditevent").hide();
}

function displayTemplateGrids() 
{
	$("#maineventtableDiv").show();
	if($("#districtMasterId").val()!=undefined ){   					
		if($("#districtName").val()==""){
			$("#districtId").val(0);
  			$("#districtName").val("");
		}					
	}			
		
	var templateType = document.getElementById("templateType").value;
    var districtId = document.getElementById("districtId").value;
    var statusName = document.getElementById("statusName").value;
    
    var statusFlag = false;
    
    if(templateType==5)
    {
    	if(statusName!="" && districtId!="")
    		statusFlag=true;
    	else
    		statusFlag=false;
    }
    else
    {
    	if(templateType>0 && districtId!="")
    		statusFlag=true;
    	else
    		statusFlag=false;
    }
    		
    if(statusFlag)
    	$("#addLink").show();
    else
    	$("#addLink").hide();
    
    if(districtId=="")
    	districtId=0;
    
    if(templateType==1)
    {
    	$('#loadingDiv').fadeIn();
    	 ManageTemplateAjax.displayTemplatesForDistrict(noOfRows, page, sortOrderStr, sortOrderType,districtId,templateType,{
 			async : true,
 			callback : function(data) {
     			$('#eventGrid').html(data);
 				applyScrollOnTblEvent();
 			//	$("#addLink").show();
 				$("#loadingDiv").hide();
 				
 			},
 			errorHandler : handleError
 		});
    }
    else if(templateType==2)
    {
    	$('#loadingDiv').fadeIn();
    	ManageTemplateAjax.displayTemplatesForEventDescription(noOfRows, page, sortOrderStr, sortOrderType,districtId,templateType,{
			async : true,
			callback : function(data) {
    			$('#eventGrid').html(data);
				applyScrollOnTblEvent();
		//		$("#addLink").show();
				$("#loadingDiv").hide();
			},
			errorHandler : handleError
		});
    }
    else if(templateType==3)
    {
    	$('#loadingDiv').fadeIn();
    	ManageTemplateAjax.displayTemplatesForEventFacilitator(noOfRows, page, sortOrderStr, sortOrderType,districtId,templateType,{
			async : true,
			callback : function(data) {
    			$('#eventGrid').html(data);
				applyScrollOnTblEvent();
		//		$("#addLink").show();
				$("#loadingDiv").hide();
			},
			errorHandler : handleError
		});
    }
    else if(templateType==4)
    {
    	$('#loadingDiv').fadeIn();
    	ManageTemplateAjax.displayTemplatesForEventParticipant(noOfRows, page, sortOrderStr, sortOrderType,districtId,templateType,{
			async : true,
			callback : function(data) {
    			$('#eventGrid').html(data);
				applyScrollOnTblEvent();
		//		$("#addLink").show();
				$("#loadingDiv").hide();
			},
			errorHandler : handleError
		});
    }
    else if(templateType==5)
    {
    	 var statusName = trim(document.getElementById("statusName").value);
    	 
    	 $('#loadingDiv').fadeIn();
     	ManageTemplateAjax.displayTemplatesForEventStatus(noOfRows, page, sortOrderStr, sortOrderType,districtId,statusName,{
 			async : true,
 			callback : function(data) {
     			$('#eventGrid').html(data);
 				applyScrollOnTblEvent();
 		//		$("#addLink").show();
 				$("#loadingDiv").hide();
 			},
 			errorHandler : handleError
 		});
    }
    
   
}

function showEditTemplate(templateId)
{
	clearColor();
	$('#errordiv').empty();
	
	var templateType = document.getElementById("templateType").value;
	
	if(templateType==1)
	{
		ManageTemplateAjax.showEditTemplate(templateId,{
			async : true,
			callback : function(data) {
			
			$('#addeditevent').show();
			$('#templatetooltip').tooltip();
			//	document.getElementById("districtId").value = data.districtMaster.districtId;
		 	//	document.getElementById("districtName").value = data.districtMaster.districtName;
		 		$('#messagetext').find(".jqte_editor").html(data.templateBody);
		 		document.getElementById("eventtitle").value=data.templateName;
		 		document.getElementById("templateId").value=data.templateId;
		 		document.getElementById('subjectforParticipants').value=data.subjectLine;
		 		document.getElementById("eventtitle").focus();
		 		
		 		if(templateType==1)
		 			$("#tooltipSpan").show();
		 		else
		 			$("#tooltipSpan").hide();
		 		
			},
			errorHandler : handleError
		});
	}
	else if(templateType==2)
	{
		ManageTemplateAjax.editTemplateForEventDiscription(templateId,{
			async : true,
			callback : function(data) {
			
			$('#addeditevent').show();
			$('#templatetooltip').tooltip();
				//document.getElementById("districtId").value = data.districtMaster.districtId;
		 		//document.getElementById("districtName").value = data.districtMaster.districtName;
		 		$('#messagetext').find(".jqte_editor").html(data.templateBody);
		 		document.getElementById("eventtitle").value=data.templateName;
		 		document.getElementById("templateId").value=data.templateId;
		 	//	document.getElementById('subjectforParticipants').value=data.templateName;
		 		document.getElementById("eventtitle").focus();
			},
			errorHandler : handleError
		});
	}
	else if(templateType==3)
	{
		ManageTemplateAjax.editTemplateForEventFacilitator(templateId,{
			async : true,
			callback : function(data) {
			
				$('#addeditevent').show();
				$('#templatetooltip').tooltip();
				//document.getElementById("districtId").value = data.districtMaster.districtId;
		 		//document.getElementById("districtName").value = data.districtMaster.districtName;
		 		$('#messagetext').find(".jqte_editor").html(data.templateBody);
		 		document.getElementById("eventtitle").value=data.templateName;
		 		document.getElementById("templateId").value=data.emailMessageTemplatesForFacilitatorsId;
		 		document.getElementById('subjectforParticipants').value=data.subjectLine;
		 		document.getElementById("eventtitle").focus();
			},
			errorHandler : handleError
		});
	}
	else if(templateType==4)
	{
		ManageTemplateAjax.editTemplateForEventParticipant(templateId,{
			async : true,
			callback : function(data) {
				$('#addeditevent').show();
				$('#templatetooltip').tooltip();
			//	document.getElementById("districtId").value = data.districtMaster.districtId;
		 	//	document.getElementById("districtName").value = data.districtMaster.districtName;
		 		$('#messagetext').find(".jqte_editor").html(data.templateBody);
		 		document.getElementById("eventtitle").value=data.templateName;
		 		document.getElementById("templateId").value=data.templateId;
		 		document.getElementById('subjectforParticipants').value=data.subjectLine;
		 		document.getElementById("eventtitle").focus();
			},
			errorHandler : handleError
		});
	}
	else if(templateType==5)
	{
		ManageTemplateAjax.editTemplateForStatus(templateId,{
			async : true,
			callback : function(data) {
				$('#addeditevent').show();
				$('#templatetooltip').tooltip();
			//	document.getElementById("districtId").value = data.districtMaster.districtId;
		 	//	document.getElementById("districtName").value = data.districtMaster.districtName;
		 		$('#messagetext').find(".jqte_editor").html(data.templateBody);
		 		document.getElementById("eventtitle").value=data.subSectionName;
		 		document.getElementById("templateId").value=data.emailSubSectionId;
		 	//	document.getElementById('subjectforParticipants').value=data.subSectionName;
		 		document.getElementById("eventtitle").focus();
			},
			errorHandler : handleError
		});
	}
		
}


function saveTemplate()
{
	$('#errordiv').empty();
	clearColor();
	var districtId = document.getElementById("districtId").value;
	var templateType = document.getElementById("templateType").value;
	var subjectforParticipants = "";
	var messagetext = $('#messagetext').find(".jqte_editor").html();
	var tamplateName = trim(document.getElementById("eventtitle").value);
	var templateId = trim(document.getElementById("templateId").value);
	var counter = 0;
	var statusName = "";
	
	
	if(templateType==5 || templateType==2)
		subjectforParticipants = "";
	else
		subjectforParticipants = trim(document.getElementById('subjectforParticipants').value);
	
	if(templateType==5)
		statusName = trim(document.getElementById("statusName").value);
	/*if (districtId == null || districtId == '') {
		counter = counter + 1;
		$('#errordiv').append("&#149; Please enter District<br>");
			$('#districtName').focus();
		$('#districtName').css("background-color", "#F5E7E1");
		$('#errordiv').show();
	}
	if (templateType == null || templateType == '' || templateType==0) {
		counter = counter + 1;
		$('#errordiv').append("&#149; Please enter Template Type<br>");
			$('#templateType').focus();
		$('#templateType').css("background-color", "#F5E7E1");
		$('#errordiv').show();
	}*/
	if(templateType!=5 && templateType!=2)
	{
		if (subjectforParticipants == null || subjectforParticipants == '') {
			counter = counter + 1;
			$('#errordiv').append("&#149; "+resourceJSON.PlzEtrSub+"<br>");
				$('#subjectforParticipants').focus();
			$('#subjectforParticipants').css("background-color", "#F5E7E1");
			$('#errordiv').show();
		}
	}
	if (tamplateName == null || tamplateName == '') {
		counter = counter + 1;
		$('#errordiv').append("&#149; "+resourceJSON.msgTemplateName+"<br>");
			$('#eventtitle').focus();
		$('#eventtitle').css("background-color", "#F5E7E1");
		$('#errordiv').show();
	}
	if (messagetext == null || messagetext == '') {
		counter = counter + 1;
		$('#errordiv').append("&#149; "+resourceJSON.msgTemplateBody+"<br>");
		//	$('#messagetext').focus();
		//$('#messagetext').css("background-color", "#F5E7E1");
		$('#errordiv').show();
	}
	if(counter==0)
	{
		ManageTemplateAjax.saveTemplate(templateId,districtId,templateType,tamplateName,subjectforParticipants,messagetext,statusName,{
			async : true,
			callback : function(data) {
				displayTemplateGrids();
				$('#addeditevent').hide();
			}
		});
	}
}

function clearColor()
{
	$('#districtName').css("background-color", "");
	$('#eventtitle').css("background-color", "");
	$('#subjectforParticipants').css("background-color", "");
	$('#templateType').css("background-color","");
}

function activateDeactivateTemplate(templateId,status)
{
	var templateType = document.getElementById("templateType").value;
	ManageTemplateAjax.activateDeactivateTemplate(templateId,status,templateType,{
		async : true,
		callback : function(data) {
			displayTemplateGrids();
		}
	});
}





// -------AutoSUggestionDistrict-----------
function getDistrictMasterAutoComp(txtSearch, event, txtdivid, txtId, hiddenId,
		type) {
	hiddenId = hiddenId;
	divid = txtdivid;
	txtid = txtSearch.id;
	if (event.keyCode == 40) {

		downArrowKey(txtdivid);
	} else if (event.keyCode == 38) // up key
	{
		upArrowKey(txtdivid);
	} else if (event.keyCode == 13) // RETURN
	{

		if (document.getElementById(divid))
			document.getElementById(divid).style.display = 'block';
		if (districtNameFilter == 1)
			document.getElementById("districtNameFilter").focus();
		else
			document.getElementById("districtName").focus();

	} else if (event.keyCode == 9) // Tab
	{

	} else if (txtSearch.value != '') {
		index = -1;
		length = 0;
		document.getElementById(divid).style.display = 'block';
		searchArray = getDistrictMasterArray(txtSearch.value);
		fatchData(txtSearch, searchArray, txtId, txtdivid);

	} else if (txtSearch.value == "") {
		document.getElementById(divid).style.display = 'none';
	}
}

function getDistrictMasterArray(districtName) {
	var searchArray = new Array();
	EventAjax.getFieldOfDistrictList(districtName, {
		async : false,
		callback : function(data) {
			hiddenDataArray = new Array();
			showDataArray = new Array();
			for (i = 0; i < data.length; i++) {
				searchArray[i] = data[i].districtName;
				showDataArray[i] = data[i].districtName;
				hiddenDataArray[i] = data[i].districtId;
			}
		},
		errorHandler : handleError
	});
	return searchArray;
}

var selectFirst = "";

var fatchData = function(txtSearch, searchArray, txtId, txtdivid) {
	var result = document.getElementById(txtdivid);
	try {
		result.style.display = 'block';
		result.innerHTML = '';
		var items = '';
		count = 0;
		var len = searchArray.length;
		if (document.getElementById(txtId).value != "") {
			for ( var i = 0; i < len; i++) {
				items += "<div id='divResult" + txtdivid + count
						+ "'  class='normal'>" + searchArray[i].toUpperCase()
						+ "</div>";
				count++;
				length++;
				if (count == 10)
					break;
			}
		} else {

		}
		if (count != 0)
			result.innerHTML = items;
		else {
			result.style.display = 'none';
			selectFirst = "";
		}
		if (txtSearch.value != ''
				&& document.getElementById('divResult' + txtdivid + 0)) {
			document.getElementById('divResult' + txtdivid + 0).className = 'over';
			selectFirst = document.getElementById('divResult' + txtdivid + 0).innerHTML;
		}

		scrolButtom();
	} catch (err) {
	}
}

function hideDistrictMasterDiv(dis, hiddenId, divId) {
	if (parseInt(length) > 0) {
		if (index == -1) {
			index = 0;
		}
		if (hiddenDataArray && hiddenDataArray[index]
				&& document.getElementById(hiddenId)) {
			document.getElementById(hiddenId).value = hiddenDataArray[index];
		}

		if (dis.value == "") {
			document.getElementById(hiddenId).value = "";
		} else if (showDataArray && showDataArray[index]) {
			dis.value = showDataArray[index];

		}

	} else {
		if (document.getElementById(hiddenId))
			document.getElementById(hiddenId).value = "";
	}

	if (document.getElementById(divId)) {
		document.getElementById(divId).style.display = "none";
	}
	index = -1;
	length = 0;
}

var downArrowKey = function(txtdivid) {
	if (txtdivid) {
		if (index < length - 1) {
			for ( var i = 0; i < 10; i++) {
				{
					if (document.getElementById('divResult' + txtdivid + i))
						var div_id = document.getElementById('divResult'
								+ txtdivid + i);
					if (div_id) {
						if (div_id.className == 'over')
							index = div_id.id.split('divResult' + txtdivid)[1];
						div_id.className = 'normal';
					}
				}
			}
			index++;
			if (document.getElementById('divResult' + txtdivid + index)) {
				var div_id = document.getElementById('divResult' + txtdivid
						+ index);
				div_id.className = 'over';
				document.getElementById(txtid).value = div_id.innerHTML;
				selectFirst = div_id.innerHTML;
			}
		}
	}
}

var upArrowKey = function(txtdivid) {
	if (txtdivid) {
		if (index > 0) {
			for ( var i = 0; i < length; i++) {

				var div_id = document
						.getElementById('divResult' + txtdivid + i);
				if (div_id) {
					if (div_id.className == 'over')
						index = div_id.id.split('divResult' + txtdivid)[1];
					div_id.className = 'normal';
				}
			}
			index--;
			if (document.getElementById('divResult' + txtdivid + index))
				document.getElementById('divResult' + txtdivid + index).className = 'over';
			if (txtid
					&& document.getElementById('divResult' + txtdivid + index)) {
				document.getElementById(txtid).value = document
						.getElementById('divResult' + txtdivid + index).innerHTML;
				selectFirst = document.getElementById('divResult' + txtdivid
						+ index).innerHTML;
			}
		}
	}
}

var overText = function(div_value, txtdivid) {

	for ( var i = 0; i < length; i++) {
		if (document.getElementById('divResult' + i)) {
			var div_id = document.getElementById('divResult' + i);

			if (div_id.className == 'over')
				index = div_id.id.split(txtdivid)[1];
			div_id.className = 'normal';
		}
	}
	div_value.className = 'over';
	document.getElementById(txtid).value = div_value.innerHTML;
}
function trim(s) {
	while ((s.substring(0, 1) == ' ') || (s.substring(0, 1) == '\n')
			|| (s.substring(0, 1) == '\r')) {
		s = s.substring(1, s.length);
	}
	while ((s.substring(s.length - 1, s.length) == ' ')
			|| (s.substring(s.length - 1, s.length) == '\n')
			|| (s.substring(s.length - 1, s.length) == '\r')) {
		s = s.substring(0, s.length - 1);
	}
	return s;
}

function mouseOverChk(txtdivid, txtboxId) {
	for ( var i = 0; i < length; i++) {
		$('#divResult' + txtdivid + i).mouseover( {
			param1 : i,
			param2 : txtdivid,
			param3 : txtboxId
		}, fireMouseOverEvent);
	}
}

function clrErrColor()
{
	$('#districtName').css("background-color", "");
	$('#eventtitle').css("background-color", "");
	$('#interactionType').css("background-color", "");
	/*$('#formatEvent').css("background-color", "");
	$('#channel').css("background-color", "");*/
	$('#intschedule').css("background-color", "");
	$('#quesId').css("background-color", "");
	$('#logo').css("background-color", "");
	$('#subjectforParticipants').css("background-color", "");
	$('#subjectforFacilator').css("background-color", "");
}

//////////////////////////////////////////////////////////////////////////////////////////
function searchEventByButton()
{
   displayTemplateGrids();
   page=1;
   cancelEvent();
}

function showStatusDiv()
{
	var templateType = document.getElementById("templateType").value;
	var districtId = document.getElementById("districtId").value;
	$('#errordiv').empty();
//	var statusName = document.getElementById("statusName").value;
    
    if(districtId=="")
    	districtId=0;
    $("#maineventtableDiv").hide()
	$('#addeditevent').hide();
    $("#addLink").hide();
	
	if(templateType==5)
	{
		$('#statusDiv').show()
		$('#loadingDiv').fadeIn();
		ManageTemplateAjax.getStatusNameData(districtId,{
 			async : true,
 			callback : function(data) {
	 			$('#statusName').html(data);
	 			$("#loadingDiv").hide();
 			},
 			errorHandler : handleError
 		});
	}
	else
		$('#statusDiv').hide()
		
		
	if(templateType==5 || templateType==2)
	{
		$('#dynamicColonm').val(1);
		$('#subjectDiv').hide();
	}
	else
	{
		$('#dynamicColonm').val(0);
		$('#subjectDiv').show();
	}
}

function onDistrictChange()
{
	$('#errordiv').empty();
	$("#maineventtableDiv").hide()
	$('#addeditevent').hide();
	$("#addLink").hide();
	$('#templateType').prop('disabled', false);
}


function showTemplateTooltioDiv()
{
	$('#templateTTDiv').modal('show');
}
function setEntityType()
{
	//alert(11);
	var entityString='';
	var districtSection="<div class='col-sm-12 col-md-12' style='padding-left:65px;'>"+
				"<label>District</label><br />"+				
				"<input type='text' id='districtName' name='districtName'"+
					"class='form-control'"+
					"onfocus=\"getDistrictMasterAutoComp(this, event, 'divTxtShowData', 'districtName','districtId','');\""+
					"onkeyup=\"getDistrictMasterAutoComp(this, event, 'divTxtShowData', 'districtName','districtId','');\""+
					"onblur=\"hideDistrictMasterDiv(this,'districtId','divTxtShowData');onDistrictChange();\" />"+
					"<input type='hidden' id='districtId' value='${districtId}' />"+				
				"<div id='divTxtShowData'"+
					"style='display: none; position: absolute; z-index: 5000;'"+
					"onmouseover=\"mouseOverChk('divTxtShowData','districtName')\" class='result'>"+
				"</div>"+
			"</div>";
	
	var btnRecord='<button class="btn btn-primary " type="button" onclick="searchEventByButton();">&nbsp;&nbsp;&nbsp;Search&nbsp;&nbsp;&nbsp;<i class="icon"></i></button>';
	globalSearchSection(entityString,"","",districtSection,btnRecord);
	$("#entityTypeDivMaster").hide();
	$("#districtClassMaster").show();	
	if($("#districtMasterId").val()==0){
		showSearchAgainMaster();
	}else{
		$("#districtId").val($("#districtMasterId").val());
		$("#districtName").val($("#districtMasterName").val());		
		hideSearchAgainMaster();
	}
}




