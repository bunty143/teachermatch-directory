var txtBgColor="#F5E7E1";

var page = 1;
var pagefac = 1;
var noOfRows = 10;
var noOfRowsfac = 10;
var sortOrderStr="";
var sortOrderType="";
var domainpage = 1;
var domainnoOfRows = 10;
var domainsortOrderStr="";
var domainsortOrderType="";

function getPaging(pageno)
{
	if(pageno!='') {
		page=pageno;	
	} else {
		page=1;
	}

	noOfRows = document.getElementById("pageSize").value;
	getUserList();
}

function getPagingforFacilitator(pageno)
{
	if(pageno!='') {
		pagefac=pageno;	
	} else {
		pagefac=1;
	}

	noOfRowsfac = document.getElementById("pageSize20").value;
	//getFacilitatorGrid();	
}

function getPagingAndSortingCommonMul(pageno,sortOrder,sortOrderTyp,gridname) {
	var gridNo	=	document.getElementById("gridNo").value;
		if(gridNo==3) {	
			if(pageno!='') {
				domainpage=pageno;	
			} else {
				domainpage=1;
			}
			domainsortOrderStr	=	sortOrder;
			domainsortOrderType	=	sortOrderTyp;
			if(document.getElementById("pageSize3")!=null){
				domainnoOfRows = document.getElementById("pageSize3").value;
			}else{
				domainnoOfRows=10;
			}
			//getFacilitatorGrid();
		}else if(gridNo==1){
			if(pageno!=''){
				page=pageno;	
			} else {
				page=1;
			}
			sortOrderStr	=	sortOrder;
			sortOrderType	=	sortOrderTyp;
			if(document.getElementById("pageSize")!=null){
				noOfRows = document.getElementById("pageSize").value;
			}else{
				noOfRows=10;
			}
			alert(	noOfRows);
			//getFacilitatorGrid();
		} else {
			if(pageno!=''){
				page=pageno;	
			}else{
				page=1;
			}
			sortOrderStr	=	sortOrder;
			sortOrderType	=	sortOrderTyp;
			if(document.getElementById("pageSize")!=null || document.getElementById("pageSize")!=""){
				noOfRows = document.getElementById("pageSize").value;
			}else{
				noOfRows=10;
			}
			if(gridname=="userGrid")
			{getUserList();
				
			}else{
			//getFacilitatorGrid();
				}
		}
}



function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert(resourceJSON.msgSomeerror+": "+exception.javaClassName);}
}
function trim(s) {
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}

function getUserList()
{
var eventId = document.getElementById('eventId').value;	
$("#loadingDiv").show();
  ManageFacilitatorsAjax.getUserByDistrict(eventId,noOfRows,page,sortOrderStr,sortOrderType,"","","",
		{
			async: true,
			callback: function(data)
			{
				$('#userGrid').html(data);
				applyScrollOnTblUser();
				$("#loadingDiv").hide();
				},
			errorHandler:handleError	
		}		
);
}
function saveFacilitatorBySelect(userId)
{
 $('#errordiv').empty();
	var eventId = document.getElementById('eventId').value;	
	var savefacilitator = document.getElementsByName(userId);	
	if(savefacilitator[0].checked)
	{
	   ManageFacilitatorsAjax.saveFacilitatorByUserId(userId,eventId,
			{
				async: true,
				callback: function(data)
				{
				savefacilitator[0].checked=false;	
				if(data=="success"){
				   //getFacilitatorGrid();
					getFacilitatorGridNew();	
				 }else{
					 $('#errordiv').show();
						$('#errordiv').append("&#149; "+data+"<br>");
				 }	
				},
		     errorHandler:handleError	
			});	
	 }	
}

function getFacilitatorGrid()
{
	applyScrollOnTblFacilitator();
	/*
	var eventId = document.getElementById('eventId').value;
	$("#loadingDiv").fadeIn();
	ManageFacilitatorsAjax.getFacilitatorGrid(eventId, noOfRowsfac, pagefac,
			sortOrderStr, sortOrderType, {
				async : true,
				callback : function(data) {
					$('#facilitatorGrid').html(data);
					applyScrollOnTblFacilitator();
					$("#loadingDiv").hide();
				},
				errorHandler : handleError
			});
*/}
function deleteFacilitator(facilitatorId)
{
document.getElementById("facilitatordelId").value=facilitatorId;
$('#myModalactMsgShow').modal('show');

}
function removeFacilitatorById()
{
var facilitatorId=document.getElementById("facilitatordelId").value;
ManageFacilitatorsAjax.removeFacilitator(facilitatorId,
		{
			async: true,
			callback: function(data)
			{
			//getFacilitatorGrid();
			getFacilitatorGridNew();
			
			},
			errorHandler:handleError		
		}		
		);	
hideMessageDiv();
}

function hideMessageDiv()
{
	$('#myModalactMsgShow').modal('hide');	
}



function validateFacilitatorFile()
{
	//$("#loadingDiv").fadeIn();
	$("#importfacilitatoraDiv").modal('hide');
	var facilitatorfile	=	document.getElementById("facilitatorfile").value;
	var errorCount=0;
	var aDoc="";
	$('#errordivImport').empty();
	if(facilitatorfile==""){
		$('#errordivImport').show();
		$('#errordivImport').append("&#149; "+resourceJSON.msgXlsXlsxFile+"<br>");
		errorCount=errorCount+1;
		aDoc="1";
	}
	else if(facilitatorfile!="")
	{
		
		var ext = facilitatorfile.substr(facilitatorfile.lastIndexOf('.') + 1).toLowerCase();	
	
		var fileSize = 0;
		if ($.browser.msie==true)
	 	{	
		    fileSize = 0;	   
		}
		else
		{
			if(document.getElementById("facilitatorfile").files[0]!=undefined)
			{
				fileSize = document.getElementById("facilitatorfile").files[0].size;
			}
		}
		if(!(ext=='xlsx' || ext=='xls'))
		{
			$('#errordivImport').show();
			$('#errordivImport').append("&#149; "+resourceJSON.msgXlsXlsxFileOnly+"<br>");
			errorCount=errorCount+1;
			aDoc="1";
		}
		else if(fileSize>=10485760)
		{
			$('#errordivImport').show();
			$('#errordivImport').append("&#149; "+resourceJSON.msgfilesizelessthan+"<br>");
			errorCount=errorCount+1;
			aDoc="1";	
		}
	
	}
	if(aDoc==1){
		$("#importfacilitatoraDiv").modal('show');
		$('#errordivImport').show();
		$('#facilitatorfile').focus();
		$('#facilitatorfile').css("background-color", "#F5E7E1");
	}
	
	if(errorCount==0){
		try{
			if(facilitatorfile!=""){
				document.getElementById("facilitatorUploadServlet").submit();
				
			}
		}catch(err){alert(err)}
		
	}else{
		$("#importfacilitatoraDiv").modal('show');
		$('#errordivImport').show();
		$("#loadingDiv").hide();
		return false;
	}
}

function uploadDataFacilitator(fileName,sessionId)
{
	var eventId = document.getElementById('eventId').value;
	$('#loadingDiv').fadeIn();
	ManageFacilitatorsAjax.saveFacilitatorTemp(fileName,sessionId,eventId,{ 
		async: true,
		callback: function(data){
			if(data!=''){
				getTempFacilitatorList();
			}else{
				
				$("#importfacilitatoraDiv").modal('show');
				$('#errordivImport').show();
				$('#errordivImport').append("&#149; "+resourceJSON.msgField+" "+data+" "+resourceJSON.msgDoesNotMatch+"<br>");
				$('#loadingDiv').hide();
			}
		},
		errorHandler:handleError 
	});
}

function setDefColortoErrorMsgColor()
{
	$('#facfirstName').css("background-color","");
	$('#faclasttName').css("background-color","");
	$('#facemailAddress').css("background-color","");
	$('#errordiv').empty();
}
function isEmailAddress(str) 
{	

	var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
	
	return emailPattern.test(str);	
}
function addFacilitator(count)
 {
	setDefColortoErrorMsgColor();
	var cnt=0;
	var focs=0;
	var eventId = document.getElementById('eventId').value;		
	var facfirstName = trim(document.getElementById('facfirstName').value);
	var faclasttName = trim(document.getElementById('faclasttName').value);
	var facemailAddress = trim(document.getElementById('facemailAddress').value);
	
	if(facfirstName=="")
	{
		$('#errordiv').append("&#149; "+resourceJSON.PlzEtrFirstName+"<br>");
		if(focs==0)
			$('#facfirstName').focus();
		$('#facfirstName').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	
	if(faclasttName=="")
	{
		$('#errordiv').append("&#149; "+resourceJSON.PlzEtrLastName+"<br>");
		if(focs==0)
			$('#faclasttName').focus();

		$('#faclasttName').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	
	if(facemailAddress=="")
	{
		$('#errordiv').append("&#149; "+resourceJSON.PlzEtrEmail+"<br>");
		if(focs==0)
			$('#facemailAddress').focus();
		
		$('#facemailAddress').css("background-color",txtBgColor);
		cnt++;focs++;
	}else if(!isEmailAddress(facemailAddress))
	{	
		$('#errordiv').append("&#149; "+resourceJSON.PlzEtrVldEmail+"<br>");
		if(focs==0)
			$('#facemailAddress').focus();
		
		$('#facemailAddress').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	
	if(cnt>0){
		 $('#errordiv').show();
		return false;
	}
	
	ManageFacilitatorsAjax.saveFacilitator(eventId,facfirstName,faclasttName,facemailAddress,
	{
	async: true,
	callback: function(data){
		
	if(data=="success"){
	  if(count==1){	
	    window.location.href="manageparticipant.do?eventId="+eventId;	
	}
	else
	   {
		$('#addfacilitatoraDiv').modal("hide");
		//getFacilitatorGrid();
		getFacilitatorGridNew();	
		clrAddFactDiv();
		//cancelFacilitator();
	   }
	}
	else
	{
		$('#errordiv').append("&#149;"+data+"<br>");
        $('#facemailAddress').focus();
		$('#facemailAddress').css("background-color",txtBgColor);
		$('#errordiv').show();	
		
	}},	
	errorHandler:handleError 	
  });	
}

function showFacilitatorDiv()
{
	setDefColortoErrorMsgColor();
	$('#errordiv').empty();
	$('#addfacilitator').show();	
	getFacilitatorAddDiv();
}
function getFacilitatorAddDiv()
{
	setDefColortoErrorMsgColor();
	$('#errordiv').empty();
	$('#addFact').hide();
	$('#importFact').hide();
	$('#selFact').hide();
	var checkway = document.getElementsByName('chooseoption');	
	var chooseoption='';
	for (var i = 0, length = checkway.length; i < length; i++) {
	    if (checkway[i].checked) {
	      chooseoption=checkway[i].value;
	   
	        break;
	    }}
	    if(chooseoption=='selectfacilitator')
	    {
	    	clrAddFactDiv();
	    	$('#selFact').show();	
	    }	
	    if(chooseoption=='importfacilitator')
	    {
	    	clrAddFactDiv();
	    	emptyTempFacilitator();
	    	$('#importFact').show();	
	    }
	    if(chooseoption=='addfacilitator')
	    {
	    	clrAddFactDiv();
	    	$('#addFact').show();	
	    }
}

function clrAddFactDiv()
{
	document.getElementById('facfirstName').value="";
	document.getElementById('faclasttName').value="";
	document.getElementById('facemailAddress').value="";
	
}

function cancelImport()
{
	document.getElementById('facilitatorfile').value="";	
	$('#tempTableDiv').html("");	
	$('#tempdiv').hide();
}
function getTempFacilitatorList()
{
    ManageFacilitatorsAjax.getTempFacilitatorList(
		{
		 async: true,
		 callback: function(data){
			  $('#loadingDiv').hide();
			  $('#tempfacilitatoraDiv').modal('show');
			  $('#tempfacilitatoraDivView').html(data);
		},
		errorHandler:handleError 	
		});			
}

function uploadFacilitator()
{
	$("#tempfacilitatoraDiv").modal('hide');
	$("#loadingDiv").fadeIn();
    var eventId = document.getElementById('eventId').value;
    ManageFacilitatorsAjax.saveFacilitatorByExcel(eventId,
		{
		async: true,
		callback: function(data){
	     emptyTempFacilitator();
	    // getFacilitatorGrid();
	     getFacilitatorGridNew();	
		},
		errorHandler:handleError 	
		}		
);	
}
function rejectFacilitator()
{
	$("#loadingDiv").hide();
	$('#tempfacilitatoraDiv').modal('hide');
     emptyTempFacilitator();	
}

function emptyTempFacilitator()
{
	ManageFacilitatorsAjax.truncateFacilitatorTemp(
	{
	  async: true,
	  callback: function(data){},
	  errorHandler:handleError 	
	});		
}

function cancelFacilitator()
{
	$("#addfacilitatoraDiv").modal('hide');  
	$('#errordiv').empty();
	//$('#addfacilitator').hide();
	clrAddFactDiv();
}

function moveParticipant()
{
	var eventId = document.getElementById('eventId').value;	
	var eventId = document.getElementById('eventId').value;	
	var facfirstName = trim(document.getElementById('facfirstName').value);
	var faclasttName = trim(document.getElementById('faclasttName').value);
	var facemailAddress = trim(document.getElementById('facemailAddress').value);
	if(facfirstName=='' && faclasttName=='' && facemailAddress=='')
	{
		window.location.href="manageparticipant.do?eventId="+eventId;	
	}
	else
	{
		addFacilitator(1);
	}

}

function exitFacilitator()
{
	window.location.href="manageevents.do";		
}

function backFacilitator()
{

var eventId = document.getElementById('eventId').value;		
window.location.href="eventschedule.do?eventId="+eventId;
}


function getUserListBySearchButton()
 {
	var	firstName        =	trim(document.getElementById("firstNamePros").value);
	var	lastName         =	trim(document.getElementById("lastNamePros").value);
	var	emailAddress     =	trim(document.getElementById("emailAddressPros").value);
	
	  page=1;
	  var eventId = document.getElementById('eventId').value;	
	  $("#loadingDiv").show();
	  ManageFacilitatorsAjax.getUserByDistrict(eventId,noOfRows,page,sortOrderStr,sortOrderType,firstName,lastName,emailAddress,
		{
			async: true,
			callback: function(data)
			{
				$('#userGrid').html(data);
				applyScrollOnTblUser();
				$("#loadingDiv").hide();
				},
			errorHandler:handleError	
		});		
}


function showaddfacilitatoraDiv(){
	$("#addfacilitatoraDiv").modal("show");
	setDefColortoErrorMsgColor();
	clrAddFactDiv();
}

function importfacilitatoraDiv(){
	$('#errordivImport').empty();
	document.getElementById('facilitatorfile').value="";
	$("#importfacilitatoraDiv").modal("show");
}

function hideimportDiv(){
	$("#importfacilitatoraDiv").modal(hide);
	$('#loadingDiv').hide();
}


/*------------- Candidate Auto Complete Js Starts ------------------*/

function getFacilitatorAuto(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("facilatatorName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		
		searchArray = getFacilitatorArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getFacilitatorArray(facilatatorName){
	
	var searchArray = new Array();
	var eventId	=	document.getElementById("eventId").value;
	
	ManageFacilitatorsAjax.getFieldOfUserList(eventId,facilatatorName,{ 
			async: false,
			callback: function(data){
		    //alert("data :: "+data)
			hiddenDataArray = new Array();
			showDataArray = new Array();
			for(i=0;i<data.length;i++){
				
				searchArray[i]=data[i].firstName +" "+data[i].lastName +"("+data[i].emailAddress+")";
				showDataArray[i]=data[i].firstName+" "+data[i].lastName+"("+data[i].emailAddress+")";
				hiddenDataArray[i]=data[i].userId;
			}
		},
		errorHandler:handleError
		});	
	return searchArray;
}


selectFirst="";

var fatchData= function(txtSearch,searchArray,txtId,txtdivid){

	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
				
			}
			
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}
function hideFacilitatorDiv(dis,hiddenId,divId)
{
	document.getElementById("facilitatorId").value="";
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			document.getElementById("facilitatorId").value=hiddenDataArray[index];
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}

var downArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index<length-1){
			for(var i=0;i<10;i++){
				{
					if(document.getElementById('divResult'+txtdivid+i))
						var div_id=document.getElementById('divResult'+txtdivid+i);
					if(div_id)
					{
						if(div_id.className=='over')
							index=div_id.id.split('divResult'+txtdivid)[1];
						div_id.className='normal';
					}
				}
			}
			index++;
			if(document.getElementById('divResult'+txtdivid+index))
			{
				var div_id=document.getElementById('divResult'+txtdivid+index);
				div_id.className='over';
				document.getElementById(txtid).value = div_id.innerHTML;
				selectFirst=div_id.innerHTML;
			}
		}
	}
}

var upArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index>0){
			for(var i=0;i<length;i++){

				var div_id=document.getElementById('divResult'+txtdivid+i);
				if(div_id)
				{
					if(div_id.className=='over')
						index=div_id.id.split('divResult'+txtdivid)[1];
					div_id.className='normal';
				}
			}
			index--;
			if(document.getElementById('divResult'+txtdivid+index))
				document.getElementById('divResult'+txtdivid+index).className='over';
			if(txtid && document.getElementById('divResult'+txtdivid+index))
			{
				document.getElementById(txtid).value=
					document.getElementById('divResult'+txtdivid+index).innerHTML;
				selectFirst=document.getElementById('divResult'+txtdivid+index).innerHTML;
			}
		}
	}
}

var overText=function (div_value,txtdivid) {

	for(var i=0;i<length;i++)
	{
		if(document.getElementById('divResult'+i))
		{
			var div_id=document.getElementById('divResult'+i);

			if(div_id.className=='over')
				index=div_id.id.split(txtdivid)[1];
			div_id.className='normal';
		}
	}
	div_value.className = 'over';
	document.getElementById(txtid).value= div_value.innerHTML;
}

function mouseOverChk(txtdivid,txtboxId)
{	
	for(var i=0;i<length;i++)
	{
		$('#divResult'+txtdivid+i).mouseover({param1: i,param2:txtdivid, param3:txtboxId}, fireMouseOverEvent);
	}
}


function fireMouseOverEvent(event)
{
	for(var i=0;i<length;i++)
	{	
		document.getElementById('divResult'+event.data.param2+i).className='normal';
	}
    document.getElementById('divResult'+event.data.param2+event.data.param1).className='over';	       
   	document.getElementById(event.data.param3).value= $('#divResult'+event.data.param2+event.data.param1).text();
    index=event.data.param1;
}


function addFacilatatorByAutoSujjest()
{
	
	$('#errordivAuto').empty();
	var facilitatorId = document.getElementById("facilitatorId").value;
	var eventId = document.getElementById('eventId').value;	
	if(facilitatorId!=null && facilitatorId!='' && facilitatorId!="0"){
		ManageFacilitatorsAjax.saveFacilitatorByUserId(facilitatorId,eventId,
				{
					async: true,
					callback: function(data)
					{
						if(data=="success"){
							cancleFacilatatorByAutoSujjest();
						   //getFacilitatorGrid();
							getFacilitatorGridNew();	
						 }else{
							 $('#errordivAuto').show();
								$('#errordivAuto').append("&#149; "+data+"<br>");
						 }	
					},
			     errorHandler:handleError	
				});	
	}
}

function cancleFacilatatorByAutoSujjest()
{
	$('#errordivAuto').empty();
	 document.getElementById("facilitatorId").value='';
	 document.getElementById("facilatatorName").value='';
}

function getFacilitatorGridNew()
{
var eventId = document.getElementById('eventId').value;	
$("#loadingDiv").fadeIn();
ManageFacilitatorsAjax.getFacilitatorGridNew(eventId,
{
	async: true,
	callback: function(data)
	{
	$("#loadingDiv").hide();
	$('#facilitatorNewDiv').html(data.split("####")[0]);
	var cnt = data.split("####")[1];
	for(i=1;i<cnt;i++)
	{
		$('#rmId'+i+'').tooltip();
	}},
	errorHandler:handleError	
}					
);
}
