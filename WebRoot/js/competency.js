/* @Author: Gagan */	
/*========  For Sorting  ===============*/
var page = 1;
var noOfRows = 10;
var sortOrderStr="";
var sortOrderType="";
function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
	//alert("pageno "+pageno+" sortOrder "+sortOrder+" sortOrderTyp "+sortOrderTyp);
	if(pageno!=''){
		page=pageno;	
	}else{
		page=1;
	}
	sortOrderStr	=	sortOrder;
	sortOrderType	=	sortOrderTyp;
	if(document.getElementById("pageSize")!=null){
		noOfRows = document.getElementById("pageSize").value;
	}else{
		noOfRows=10;
	}
	displayCompetency();
}
/*========  For Handling session Time out Error ===============*/
function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert(resourceJSON.msgServerErr +exception.javaClassName);}
}
/*========  Add New Competency===============*/
function addCompetency()
{
	$('#domainId').css("background-color", "");
	$('#competencyName').css("background-color", "");
	$('#competencyUId').css("background-color", "");
	$('#competencyUId').prop('disabled', false);
	$('#rank').css("background-color", "");
	dwr.util.setValues({ domainId:null,competencyId:null,competencyName:null,rank:null,competencyShortName:null,competencyUId:null});
	document.getElementById("divCompetencyForm").style.display	=	"block";
	$('#domainId').focus();
	$('#errordiv').hide();
	document.getElementById("divDone").style.display			=	"block";
	document.getElementById("divManage").style.display			=	"none";
	return false;
}
/*========  displayCompetency ===============*/
function displayCompetency()
{
	//alert("Hi");
	CompetencyAjax.displayCompetencyRecords(noOfRows,page,sortOrderStr,sortOrderType,{ 
		async: true,
		callback: function(data){
		$('#competencyGrid').html(data);
		applyScrollOnTbl();
		//document.getElementById("competencyTable").innerHTML		=	data;
	},
	errorHandler:handleError
	});
}

/*========  Clear Competency Fields ===============*/
function clearCompetency()
{
	document.getElementById("divCompetencyForm").style.display	=	"none";
	document.getElementById("divDone").style.display			=	"none";
	document.getElementById("divManage").style.display			=	"none";
	document.getElementById("competencyName").value				=	"";
	document.getElementsByName("competencyStatus")[0].checked	=	true;
	$('#competencyUId').val('');
	$('#competencyUId').prop('disabled', false);
	$('#competencyUId').css("background-color", "");
}
function chkForEnterSaveCompetency(evt)
{
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==13)
	{
		saveCompetency();
	}	
}
/*========  Save Competency  ===============*/
function saveCompetency()
{
	$('#errordiv').empty();
	$('#domainId').css("background-color", "");
	$('#competencyName').css("background-color", "");
	$('#competencyUId').css("background-color", "");
	$('#rank').css("background-color", "");
	var domainId			=	trim(document.getElementById("domainId").value);
	var competencyId		=	trim(document.getElementById("competencyId").value);
	var competencyName		=	trim(document.getElementById("competencyName").value);
	var rank				=	trim(document.getElementById("rank").value);
	var competency			=	document.getElementsByName("competencyStatus");
	var competencyShortName	=	trim(document.getElementById("competencyShortName").value);
	var competencyUId		=	trim(document.getElementById("competencyUId").value);
	var counter				=	0;
	var focusCount			=	0;
	for(i=0;i<competency.length;i++)
	{
		if(competency[i].checked	==	true)
		{
			competencyStatus		=	competency[i].value;
			break;
		}
	}
	if (domainId	==	0)
	{
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.msgDomainName+"<br>");
		if(focusCount	==	0)
			$('#domainId').focus();
		$('#domainId').css("background-color", "#F5E7E1");
		counter++;
		focusCount++;
	}
	if (competencyName	==	"")
	{
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.msgPleaseCompetencyName+"<br>");
		if(focusCount	==	0)
			$('#competencyName').focus();
		$('#competencyName').css("background-color", "#F5E7E1");
		counter++;
		focusCount++;
	}
	if (competencyUId=="")
	{
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.msgEnterCompetencyUId+"<br>");
		if(focusCount==0)
			$('#competencyUId').focus();
		$('#competencyUId').css("background-color", "#F5E7E1");
		counter++;
		focusCount++;
	}
	if (rank.length	> 0)
	{
		if(rank	==	0)
		{
			$('#errordiv').show();
			$('#errordiv').append("&#149; "+resourceJSON.msgRankgreater1+"<br>");
			if(focusCount	==	0)
				$('#rank').focus();
			$('#rank').css("background-color", "#F5E7E1");
			counter++;
			focusCount++;
		}
	}
	if(counter	==	0)
	{
		dwr.engine.beginBatch();			
		CompetencyAjax.saveCompetency(domainId,competencyId,competencyName,rank,competencyStatus,competencyShortName,competencyUId, { 
			async: true,
			callback: function(data)
			{
				if(data==3){
					document.getElementById('errordiv').innerHTML	=	'&#149; '+resourceJSON.msgPleaseUnqCompetencyName;
					$('#errordiv').show();
					$('#competencyName').css("background-color", "#F5E7E1");
					$('#competencyName').focus();
				}
				else if(data==5){
					document.getElementById('errordiv').innerHTML	=	'&#149; '+resourceJSON.msgUniqueCompetencyUId;
					$('#errordiv').show();
					$('#competencyUId').css("background-color", "#F5E7E1");
					$('#competencyUId').focus();
				}
				else if(data==4){
					document.getElementById('errordiv').innerHTML	=	'&#149; '+resourceJSON.msgUniqRank;
					$('#errordiv').show();
					$('#rank').css("background-color", "#F5E7E1");
					$('#rank').focus();
				}
				else{
					displayCompetency();
					clearCompetency();
				}
			},
		errorHandler:handleError 
		});
		dwr.engine.endBatch();
		return true;
	}
	else
	{
		$('#errordiv').show();
		return false;
	}
}
/*========  activateDeactivateCompetency ===============*/
function activateDeactivateCompetency(competencyId,status)
{
	CompetencyAjax.activateDeactivateCompetency(competencyId,status, { 
		async: true,
		callback: function(data)
		{
			displayCompetency();
			clearCompetency();
		},
		errorHandler:handleError 
	});
				
}
/*========  Edit Competency ===============*/
function editCompetency(competencyId)
{
	//alert("competencyId "+competencyId);
	$('#errordiv').hide();
	$('#errordiv').hide();
	$('#domainId').css("background-color", "");
	$('#competencyName').css("background-color", "");
	$('#rank').css("background-color", "");
	$('#competencyUId').css("background-color", "");
	document.getElementById("divDone").style.display			=	"none";
	document.getElementById("divCompetencyForm").style.display	=	"block";
	var competency	=	document.getElementsByName("competencyStatus");
	
	$('#domainId').focus();
		CompetencyAjax.getCompetencyById(competencyId, { 
			async: true,
			callback: function(data)
		{
			//alert("data "+data.status);
			dwr.util.setValues(data);
			var opts = document.getElementById('domainId').options;
			for(var i = 0, j = opts.length; i < j; i++)
			{
				  if(data.domainMaster.domainId==opts[i].value)
				  {
					  opts[i].selected	=	true;
				  }
			}
			if(data.status=='A')
				competency[0].checked	=	true;
			else
				competency[1].checked	=	true;
			$('#competencyUId').val(data.competencyUId);
			$('#competencyUId').prop('disabled', true);
		},
		errorHandler:handleError 	
		});
	
	document.getElementById("divManage").style.display="block";
	return false;
}
function isNumberKey(evt)
{
   var charCode = (evt.which) ? evt.which : event.keyCode
   if (charCode > 31 && (charCode < 48 || charCode > 57))
      return false;

   return true;
}
function isAlphabetsKey(evt)
{
	var charCode = (evt.which) ? evt.which : event.keyCode
	if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123))
		return true;
	else
		return false;
}

/* @Author: Gagan */	