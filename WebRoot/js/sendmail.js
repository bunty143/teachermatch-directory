		
function sendMail()
{               
	var email=document.getElementById("txtEmail").value.trim();
	var subject=document.getElementById("txtSubject").value.trim();
	var location=document.getElementById("txtLocation").value.trim();
	var sFromDate=document.getElementById("sfromDate").value;
	var sFromTime = document.getElementById("sfromtime").value;		
	var sToDate=document.getElementById("stoDate").value;
	var sTotime = document.getElementById("sToTime").value;	
	var description=document.getElementById("txtDescription").value.trim();
	var counter=0;
	var focuscounter=0;
	//$('#displayError').show();	
	$('#displayError').empty();
	$('#displayMsg').empty();
	$('#displayMsg').hide();
	if(email=="")
	{			
		$('#displayError').append("&#149; "+resourceJSON.PlzEtrEmail+"<br>");	
		if(focuscounter==0){
		document.getElementById("txtEmail").focus();	
		}
		focuscounter++;
		counter++;
	}
	if(subject=="")
	{				
		$('#displayError').append("&#149; "+resourceJSON.PlzEtrSub+"<br>");	
		if(focuscounter==0){
		document.getElementById("txtEmail").focus();		
		}
		focuscounter++;
		counter++;
	}
	if(location=="")
	{				
		$('#displayError').append("&#149; "+resourceJSON.PlzEtrLocation+"<br>");	
		if(focuscounter==0){
		document.getElementById("txtEmail").focus();		
		}
		focuscounter++;
		counter++;
	}
	if(description=="")
	{				
		$('#displayError').append("&#149; "+resourceJSON.PlzEtrDescription+"<br>");	
		if(focuscounter==0){
		document.getElementById("txtEmail").focus();		
		}
		focuscounter++;
		counter++;
	}	
	if(sFromDate=="")
	{				
		$('#displayError').append("&#149; "+resourceJSON.PlzEtrFormDate+"<br>");	
		if(focuscounter==0){
		document.getElementById("txtEmail").focus();		
		}
		focuscounter++;
		counter++;
	}
	if(sFromTime=="Start Time")
	{				
		$('#displayError').append("&#149; "+resourceJSON.PlzSelectStartTime+"<br>");	
		if(focuscounter==0){
		document.getElementById("txtEmail").focus();		
		}
		focuscounter++;
		counter++;
	}
	if(sToDate=="")
	{				
		$('#displayError').append("&#149; "+resourceJSON.PlzEnterEndDate+"<br>");	
		if(focuscounter==0){
		document.getElementById("txtEmail").focus();		
		}
		focuscounter++;
		counter++;
	}
	if(sTotime=="End Time")
	{				
		$('#displayError').append("&#149; "+resourceJSON.PlzSelectEndDate+"<br>");	
		if(focuscounter==0){
		document.getElementById("txtEmail").focus();		
		}
		focuscounter++;
		counter++;
	}	
	if(counter==0)
	{
		$('#displayError').hide();
		
	try
	{		
		SendMailAjax.sendMail(email,subject,location,sFromDate,sFromTime,sToDate,sTotime,description,{		     
		async: true,
		callback: function(data)	{
			if(data=="Submitted")
			{
				$('#displayMsg').show();
				$('#displayMsg').append(resourceJSON.MsgMailSentSuccessfully);	
				document.getElementById("txtEmail").value='';
				document.getElementById("txtSubject").value='';
				document.getElementById("txtLocation").value='';
				document.getElementById("sfromDate").value='';
				document.getElementById("sfromtime").value=0;		
				document.getElementById("stoDate").value='';
				document.getElementById("sToTime").value=0;	
				document.getElementById("txtDescription").value='';
			}
			else
			{
				$('#displayMsg').hide();
				$('#displayError').show();
				$('#displayError').append(data);
			}
		},
		//errorHandler:handleError 
	});
	}catch(e){alert(e)}
	}
	else		
	{
		$('#displayError').show();
	}
}
