var page = 1;
var noOfRows = 10;
var sortOrderStr="";
var sortOrderType="";

function getPaging(pageno)
{
	if(pageno!='')
	{
		page=pageno;	
	}else
	{
		page=1;
	}
		noOfRows = document.getElementById("pageSize").value;
		displaySets();
}

function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
	if(pageno!=''){
		page=pageno;	
	}else{
		page=1;
	}
	sortOrderStr	=	sortOrder;
	sortOrderType	=	sortOrderTyp;
	if(document.getElementById("pageSize")!=null){
		noOfRows = document.getElementById("pageSize").value;
	}else{
		noOfRows=10;
	}
	displaySets();
}

function searchi4Ques()
{
	hideSearchAgainMaster();
	page = 1;
	displaySets()
}


function displaySets()
{
	var refChkSetStatus 		= 	document.getElementById("refChkSetStatus").value;
	var quesSetSearchText		=	document.getElementById("quesSetSearchText").value;
	var entityType				=	document.getElementById("entityType").value;
	var districtIdFilter		=	"";
	var headQuarterId = "";
	
  		if($("#districtMasterId").val()!=undefined ){   					
  			if($("#districtNameFilter").val()!=""){
	  			//$("#districtIdFilter").val($("#districtMasterId").val());
	  			//$("#districtNameFilter").val($("#districtMasterName").val());
  			}
  			else{
  				if($("#checkShowDistrict").val()==1){  					
  					$("#checkShowDistrict").val(0);
	  				$("#districtIdFilter").val($("#districtMasterId").val());
	  	  			$("#districtNameFilter").val($("#districtMasterName").val());
  				}
  				  					
  			}
  				
  		}  		
	try{
		headQuarterId		=	trim(document.getElementById("headQuarterId").value);
	}catch(e){}
	if(entityType=='5' || entityType=='6'){
		districtIdFilter = 0 ;
	}else{
		districtIdFilter=trim(document.getElementById("districtIdFilter").value);
	}
	DistrictSpecificQuestionsSetAjax.displayQuestionSet(noOfRows,page,sortOrderStr,sortOrderType,refChkSetStatus,quesSetSearchText,districtIdFilter,headQuarterId,{ 
		async: true,
		callback: function(data){
		document.getElementById("refChkQuesSetGrid").innerHTML=data;
		applyScrollOnTbl();
		document.getElementById("addQuesSetDiv").style.display="none";
	},
	});
}


function saveQuestion()
{
	$('#errQuesSetdiv').empty();
	$('#quesSetName').css("background-color","");
	$('#districtName').css("background-color","");
	var districtId = "";
	var headQuarterId = "";
	var branchId= "";
	var entityType 			= 	trim(document.getElementById("entityType").value);
	var quesSetId			=	trim(document.getElementById("quesSetId").value);
	var quesSetName			=	trim(document.getElementById("quesSetName").value);
	try{
		districtId	=	trim(document.getElementById("districtId").value);
	}catch(e){}
	try{
		headQuarterId		=	trim(document.getElementById("headQuarterId").value);
	}catch(e){}
	try{
		branchId			=	trim(document.getElementById("branchId").value);
	}catch(e){}
	var quesSetStatus		=	document.getElementsByName("quesSetStatus");
	var quesValue ="";	
	
	var counter				=	0;
	var focuscount			=	0;
	
	for(i=0;i<quesSetStatus.length;i++)
	{
		if(quesSetStatus[i].checked	==	true)
		{
			quesValue	=	quesSetStatus[i].value;
			break;
		}
	}
	
	
	if (districtId=="" && headQuarterId=="")
	{
		$('#errQuesSetdiv').append("&#149; "+resourceJSON.PlzEtrDistrict+"<br>");
		$('#errQuesSetdiv').show();
		$('#districtName').css("background-color", "#F5E7E1");
		counter++;
		focuscount++;
	}
	
	if (quesSetName=="")
	{
		$('#errQuesSetdiv').append("&#149; "+resourceJSON.PlzEtrQuest+"");
		$('#errQuesSetdiv').show();
		$('#quesSetName').css("background-color", "#F5E7E1");
		counter++;
		focuscount++;
	}
	
	if(counter==0)
	{
		DistrictSpecificQuestionsSetAjax.saveRefChkQuestionSet(headQuarterId,quesSetId,quesSetName,quesValue,districtId,{ 
			async: true,
			callback: function(data)
			{
				if(data==1)
				{
					$('#errQuesSetdiv').show();
					$('#errQuesSetdiv').append("&#149; "+resourceJSON.MsgNotInsertDuplicateQuestSet+"");
				}
				else
				{
					$('#errQuesSetdiv').hide();
					$('#errQuesSetdiv').empty();
					displaySets();
					clearQues();
				}
			},
		});
	}
	
	
	
}

function clearQues()
{
	document.getElementById("quesSetName").value="";
	document.getElementById("addQuesSetDiv").style.display="none";
	document.getElementById("quesSetId").value="";
}

function addNewQuesSet()
{
	document.getElementById("addQuesSetDiv").style.display="block";
	document.getElementById("quesSetName").value="";
	if(document.getElementById("entityType").value==1)
	{
		document.getElementById("districtId").value="";
		document.getElementById("districtName").value="";
	}
	$('#quesSetName').css("background-color", "");
	$('#districtName').css("background-color", "");
	
	$("#errQuesSetdiv").empty();

}

function editI4Question(quesSetId)
{
	$("#errQuesSetdiv").empty();
	document.getElementById("addQuesSetDiv").style.display="block";
	var quesSetStatus		=	document.getElementsByName("quesSetStatus");
	
	DistrictSpecificQuestionsSetAjax.editRefChkQuestionSet(quesSetId,{ 
		async: true,
		callback: function(data)
		{
			if(data!=null)
			{
				document.getElementById("quesSetDate").value=data.dateCreated;
				document.getElementById("quesSetId").value=data.ID;
				document.getElementById("quesSetName").value=data.questionSetText;
				if(data.districtMaster!=null){
					try{
						document.getElementById("districtName").value=data.districtMaster.districtName;
						document.getElementById("districtId").value=data.districtMaster.districtId;
					}catch(err){}
				}
				
				if(data.status=='A')
					quesSetStatus[0].checked=true;
				else
					quesSetStatus[1].checked=true;
			}
		},
	});
}

function activateDeactivateQues(quesSetId,quesSetStatus)
{
	document.getElementById("addQuesSetDiv").style.display="block";
	//$( "span#i4QuesTxt" ).html("<textarea id='txtQuestion' ></textarea>");
	DistrictSpecificQuestionsSetAjax.activateDeactivateQuestionSet(quesSetId,quesSetStatus, { 
		async: true,
		callback: function(data)
		{
			displaySets();
			clearQues();
		},
	});
				
}

function trim(s)
{
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}
/*********************** Branch Auto Complete ******************************/
var count=0;
var index=-1;
var length=0;
var divid='';
var txtid='';
var isBranchExistsArray = new Array();
var hiddenDataArray = new Array();
var showDataArray = new Array();
var degreeTypeArray = new Array();
var hiddenId="";

function getBranchMasterAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{	
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38)
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13)
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		document.getElementById("branchName").focus();
	} 
	else if(event.keyCode==9)
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		//alert("02");
		searchArray = getBranchArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}
function getBranchDistrictFilter(){
	var branchId=0;
	try {
		branchId=document.getElementById("branchId").value;
	} catch (e) {
		// TODO: handle exception
	}
	if(branchId==null || branchId=="")
		branchId=0;
	AutoSearchFilterAjax.getFieldOfDistrictListByBranch(branchId,{  
		async: false,		
		callback: function(data){
			try{
				if(data==true){
					document.getElementById("districtName").disabled=false;
				}else{
					document.getElementById("districtName").disabled=true;
				}
			}catch(e){}
		}
	});	
}
function getBranchArray(branchName){
	
	//alert("03");
	var hqId=document.getElementById("headQuarterId").value;
	var searchArray = new Array();
	AutoSearchFilterAjax.getBranchListByHQ(hqId,branchName,{  
		async: false,		
		callback: function(data){
		hiddenDataArray = new Array();
		showDataArray = new Array();
		for(i=0;i<data.length;i++){
			searchArray[i]=data[i].branchName;
			hiddenDataArray[i]=data[i].branchId;
			showDataArray[i]=data[i].branchName;
		}
	}
	});	

	return searchArray;
}

function hideBranchMasterDiv(dis,hiddenId,divId)
{
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
		if(dis.value!="")
		{
			var cnt=0;
			var focs=0;	
			$('#errordiv').empty();	
			$('#errordiv').append("&#149; Please enter valid Branch<br>");
		}
	}
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	//getDistrictWiseSubject(document.getElementById(hiddenId).value);
	index = -1;
	length = 0;
	getBranchDistrictFilter();
}

var selectFirst="";

var fatchData= function(txtSearch,searchArray,txtId,txtdivid){
	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		//alert(" length :: "+len);
		if(document.getElementById(txtId).value!="")
		{			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i] + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
				
			}
			
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}

var downArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index<length-1){
			for(var i=0;i<10;i++){
				{
					if(document.getElementById('divResult'+txtdivid+i))
						var div_id=document.getElementById('divResult'+txtdivid+i);
					if(div_id)
					{
						if(div_id.className=='over')
							index=div_id.id.split('divResult'+txtdivid)[1];
						div_id.className='normal';
					}
				}
			}
			index++;
			if(document.getElementById('divResult'+txtdivid+index))
			{
				var div_id=document.getElementById('divResult'+txtdivid+index);
				div_id.className='over';
				document.getElementById(txtid).value = div_id.innerHTML;
				selectFirst=div_id.innerHTML;
			}
		}
	}
}

var upArrowKey = function(txtdivid){
	
	if(txtdivid)
	{
		if(index>0){
			for(var i=0;i<length;i++){

				var div_id=document.getElementById('divResult'+txtdivid+i);
				if(div_id)
				{
					if(div_id.className=='over')
						index=div_id.id.split('divResult'+txtdivid)[1];
					div_id.className='normal';
				}
			}
			index--;
			if(document.getElementById('divResult'+txtdivid+index))
				document.getElementById('divResult'+txtdivid+index).className='over';
			if(txtid && document.getElementById('divResult'+txtdivid+index))
			{
				document.getElementById(txtid).value=
					document.getElementById('divResult'+txtdivid+index).innerHTML;
				selectFirst=document.getElementById('divResult'+txtdivid+index).innerHTML;
			}
		}
	}
}

function mouseOverChk(txtdivid,txtboxId)
{	
	for(var i=0;i<length;i++)
	{
		$('#divResult'+txtdivid+i).mouseover({param1: i,param2:txtdivid, param3:txtboxId}, fireMouseOverEvent);
	}
}


function fireMouseOverEvent(event)
{
	for(var i=0;i<length;i++)
	{	
		document.getElementById('divResult'+event.data.param2+i).className='normal';
	}
   document.getElementById('divResult'+event.data.param2+event.data.param1).className='over';	       
  	document.getElementById(event.data.param3).value= $('#divResult'+event.data.param2+event.data.param1).text();
   index=event.data.param1;
}

function __mouseOverChk(txtdivid,txtboxId)
{	
	for(var i=0;i<length;i++)
	{
		document.getElementById('divResult'+txtdivid+i).className='normal';		
		if ($('#divResult'+txtdivid+i).is(':hover')) 
		{
			document.getElementById('divResult'+txtdivid+i).className='over';	       
	       	document.getElementById(txtboxId).value= $('#divResult'+txtdivid+i).text();
	        index=i;
	    }
	}

}

var overText = function (div_value,txtdivid) 
{
	for(var i=0;i<length;i++)
	{
		if(document.getElementById('divResult'+i))
		{
			var div_id=document.getElementById('divResult'+i);

			if(div_id.className=='over')
				index=div_id.id.split(txtdivid)[1];
			div_id.className='normal';
		}
	}
	div_value.className = 'over';
	document.getElementById(txtid).value= div_value.innerHTML;
}
/***************************************************************************/
function setEntityType()
{
	var entityString='';
	var districtSection="<div class='col-sm-12 col-md-12 ' style='padding-left:65px;'><label id='captionDistrictOrSchool'>District</label>"+
	"<input type='text' id='districtNameFilter' name='districtNameFilter' class='form-control' onfocus=\"getDistrictMasterAutoComp(this, event, 'divTxtShowDataFilter', 'districtNameFilter','districtIdFilter','');\""+
	"onkeyup=\"getDistrictMasterAutoComp(this, event, 'divTxtShowDataFilter', 'districtNameFilter','districtIdFilter','');\""+
	"onblur=\"hideDistrictMasterDiv(this,'districtIdFilter','divTxtShowDataFilter');\"	/>"+
	"<input type='hidden' id='districtIdFilter' value=''/>"+
	"<div id='divTxtShowDataFilter'  onmouseover=\"mouseOverChk('divTxtShowDataFilter','districtNameFilter')\""+
	"style=' display:none;position:absolute;z-index: 5000' class='result' ></div>	"+
	"</div>";
	var btnRecord='<button class="btn btn-primary " type="button" onclick="searchi4Ques()">Search <i class="icon"></i></button>';
	globalSearchSection(entityString,"","",districtSection,btnRecord);
	$("#entityTypeDivMaster").hide();
	$("#districtClassMaster").show();
	
	/*setTimeout(function(){ 
  		if($("#districtMasterId").val()!=0){ 
  			$("#districtIdFilter").val($("#districtMasterId").val());
  			$("#districtNameFilter").val($("#districtMasterName").val());
  			
  		}
  		}, 50);*/
	if($("#districtMasterId").val()==0){
		showSearchAgainMaster();
	}else{
		hideSearchAgainMaster();
	}
}


