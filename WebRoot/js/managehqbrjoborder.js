var page = 1;
var noOfRows = 50;
var sortOrderStr="";
var sortOrderType="";

var deviceTypeAndroid=$.browser.device = (/Android|webOS|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent.toLowerCase()));
var deviceType=$.browser.device = (/iPhone|iPad|iPod/i.test(navigator.userAgent.toLowerCase()));

var pageZS = 1;
var noOfRowsZS = 10;
var sortOrderStrZS="";
var sortOrderTypeZS="";

function getPaging(pageno)
{
	var pageFlag=$('#geozoneschoolFlag').val();
	if(pageFlag==1){
		if(pageno!='')
		{
			pageZS=pageno;	
		}
		else
		{
			pageZS=1;
		}
		noOfRowsZS = document.getElementById("pageSizeZoneSchool").value;
		displayJobs();
		
	}else{
		if(pageno!='')
		{
			page=pageno;	
		}
		else
		{
			page=1;
		}
		noOfRows = document.getElementById("pageSize").value;
		displayJobs();
	}
	
}
function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
	var pageFlag=$('#geozoneschoolFlag').val();
	if(pageFlag==1)
	{	
		if(pageno!=''){
			pageZS=pageno;	
		}else{
			pageZS=1;
		}
		sortOrderStrZS=sortOrder;
		sortOrderTypeZS=sortOrderTyp;
		if(document.getElementById("pageSizeZoneSchool")!=null){
			noOfRowsZS = document.getElementById("pageSizeZoneSchool").value;
		}else{
			noOfRowsZS=100;
		}
		displayJobs();
	}else
	{	
		if(pageno!=''){
			page=pageno;	
		}else{
			page=1;
		}
		sortOrderStr=sortOrder;
		sortOrderType=sortOrderTyp;
		if(document.getElementById("pageSize")!=null){
			noOfRows = document.getElementById("pageSize").value;
		}else{
			noOfRows=20;
		}
		displayJobs();
	}	
}

function showMiamiPdf()
{
	  var left = (screen.width/2)-(600/2);
	  var top = (screen.height/2)-(700/2);
	  return window.open("http://jobs.dadeschools.net/teachers/pdf/Geo_Zones_With_Schools.pdf", "Miami Geo Zone", "width=600, height=600,top="+top+",left="+left);
}

function showPhiladelphiaPdf()
{
	  var left = (screen.width/2)-(600/2);
	  var top = (screen.height/2)-(700/2);
	  return window.open("images/District_Map_LN_8x11_2014_09_05%5B1%5D%20copy.pdf", "Miami Geo Zone", "width=600, height=600,top="+top+",left="+left);
}

function searchBatchJobOrder()
{
	page = 1;
	$('#searchLinkDiv').show();
	$('#hidesearchLinkDiv').hide();
	$("#advanceSearchDiv").hide()
	searchRecordsByEntityType();
}
/*========  For Handling session Time out Error ===============*/
function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert("Oops: Your Session has expired!");  document.location = 'signin.do';}
	else{alert("Server Error: "+exception.javaClassName);}
}
/*======= SearchJobOrder on Press Enter Key ========= */
function chkForEnterSearchJobOrder(evt)
{
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==13)
	{
		//searchBatchJobOrder();
	}	
}

function checkForInt(evt) {
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==8)
		return true;
	return( (charCode >= 48 && charCode <= 57)||(charCode==13));
}
function addnewJob(url)
{
	var currentdate = new Date();
	var datetime =  currentdate.getDay()+""+currentdate.getMonth()+""+currentdate.getFullYear()+""+currentdate.getHours()+""+currentdate.getMinutes()+""+currentdate.getSeconds();
	url=url+""+datetime;
	window.location.href=url;
}

function displayJobs()
{
	$("#loadingDiv").fadeIn();
	var headQuarterId = document.getElementById("headQuarterId").value;
	var branchId = 0;
	var branchSearchId = 0;
	try{
		branchSearchId = document.getElementById("branchSearchId").value;
	}catch(e){}
	try{
		branchId = document.getElementById("branchId").value;
	}
	catch(e){}
	var jobOrderId	=0;
	try{
		jobOrderId=document.getElementById("jobOrderId").value;
	}catch(e){}

	if(branchId==0 && branchSearchId!=0){
		branchId = branchSearchId;
	}
	var status = $("#status").val();
	HeadQuarterAjax.displayHQBRJobs(headQuarterId,branchId,jobOrderId,status,noOfRows,page,sortOrderStr,sortOrderType,{ 
		async: false,
		callback: function(data)
		{
			$("#divMain").html(data);
			applyScrollOnTblJobs();
			$("#loadingDiv").hide();
		
		},
		errorHandler:handleError 
}); 
}
function downloadAttachment(filePath,fileName) {
	HeadQuarterAjax.downloadAttachment(filePath,fileName,{ 
	async: false,
	errorHandler:handleError,
	callback:function(data)
	{
		if(deviceType)
		{
			if (data.indexOf(".doc")!=-1)
			{
				$("#docfileNotOpen").css({"z-index":"3000"});
		    	$('#docfileNotOpen').show();
			}
			else if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
			{
				$("#exelfileNotOpen").css({"z-index":"3000"});
		    	$('#exelfileNotOpen').show();
			}
			else
			{
				document.getElementById(linkid).href = data;
			}
		}
		else if(deviceTypeAndroid)
		{
			if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
			{
				 $('#modalDownloadsAttachment').modal('hide');
				 document.getElementById('ifrmAttachment').src = ""+data+"";
			}
			else
			{
				document.getElementById(linkid).href = data;
			}
		}
		else if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
		{
			 $('#modalDownloadsAttachment').modal('hide');
			 document.getElementById('ifrmAttachment').src = ""+data+"";
		} else {
				$('.distAttachment').modal('show');
				document.getElementById('ifrmAttachment').src = ""+data+"";
		}
	}});
}

function activateDeactivateJob(jobId, status)
{
	//$("#loadingDiv").fadeIn();
	HeadQuarterAjax.updateStatus(jobId,status,{ 
		async: false,
		callback: function(data)
		{
			if(data =="update")
			{
				displayJobs();
			
			}
		
		},
		errorHandler:handleError 
}); 
}


function showEditHQJobHistory(jobId){
$("#jobOrderHistoryModalId").show();
//alert("111111");
document.getElementById("divJobHisTxt").innerHTML= '<img src="images/loadingAnimation.gif" style="margin-left:45%;">';

try{
	ManageJobOrdersAjax.getDataFromJobOrderHistory(jobId,{
	async:false,
	callback:function(data){
	if(data!=null){
			
		document.getElementById("divJobHisTxt").innerHTML=data;
	}

},errorHandler:handleError
});
}catch(e){}
}


function hideById(itemId){
$("#"+itemId).hide();

}