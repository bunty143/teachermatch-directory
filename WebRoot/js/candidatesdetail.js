
function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert(resourceJSON.msgServerErr +exception.javaClassName);}
}

/////////////////////////////////////////////////////////////////////////
var page = 1;
var noOfRows = 50;
var sortOrderStr="";
var sortOrderType="";
function getPaging(pageno)
{
	if(pageno!='')
	{
		page=pageno;	
	}
	else
	{
		page=1;
	}

	noOfRows = document.getElementById("pageSize").value;
	getCandidates();
}
function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
	if(pageno!=''){
		page=pageno;	
	}else{
		page=1;
	}
	sortOrderStr=sortOrder;
	sortOrderType=sortOrderTyp;
	if(document.getElementById("pageSize")!=null){
		noOfRows = document.getElementById("pageSize").value;
	}else{
		noOfRows=10;
	}

	getCandidates();

}
/*=======  searchTeacher on Press Enter Key ========= */
function chkForEnterSearchTeacher(evt)
{
	var charCode = ( evt.which ) ? evt.which : event.keyCode;	
	if(charCode==13)
	{
		searchTeacher();
	}	
}
/*========  display grid after Filter ===============*/
function searchTeacher()
{	
	page=1;
	getCandidates()
	
}
function getCandidates()
{
	//$('#totalsOfDateGrid').html("<div style='text-align:center;padding-top:150px;'><img src='images/loadingAnimation.gif' /> <br>Loading...</div>");
	
	
	var fromDate		=	$('#fromDate').val(); 
	var toDate			=	$('#toDate').val(); 
	var statusId		=	$('#statusId').val(); 
	var errorCount		=	0;
	$('#errordiv').empty();
	if(fromDate==""){
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.PlzEtrFormDate+"<br>");
		errorCount++;
		//sDate="1";
	}
	if(toDate==""){
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.msgPlzEnterToDate+"<br>");
		errorCount++;
		eDate="1";
	}
	if(fromDate!="" && toDate!=""){
		var endDate = new Date(toDate);              
	    var startDate= new Date(fromDate);   
		if(startDate > endDate){
			$('#errordiv').show();
			$('#errordiv').append("&#149; "+resourceJSON.msgFromLessToDate+"<br>");
			errorCount++;
			//sDate="1";
		} 
	}
	
	
	//alert(fromDate+" "+toDate);
	var firstName		=	$('#firstName').val(); 
	var lastName		=	$('#lastName').val();
	var emailAddress	=	$('#emailAddress').val();
	//alert(p+" "+ q+ " "+r );
	
	
	if(errorCount==0){
		AdminDashboardAjax.getCandidates(fromDate,toDate,statusId,firstName,lastName,emailAddress,noOfRows,page,sortOrderStr,sortOrderType,{ 
			async: true,
			callback: function(data){
			$('#divMain').html(data);
			applyScrollOnTbl();
			$('#gridShowId').val(statusId);
		},
		errorHandler:handleError  
		});
	}
}


function checkAll(flg)
{
	var checkBox = document.getElementsByName("case");
	for(i=0;i<checkBox.length;i++)
		checkBox[i].checked=flg;
}

function sendMessages()
{
	var statusId		=	$('#statusId').val(); 
	var gridShowId		=	$('#gridShowId').val(); 
	
	if(statusId!=gridShowId)
	{
		$('#message2show').html(resourceJSON.msgappropriateStatusb4mail);
		$('#myModal2').modal('show');
		return;
	}
	
	if(statusId==1)
		teacherStatus = "NotAuthenticated";
	else if(statusId==2)
		teacherStatus = "EPINotStarted";
	else if(statusId==3)
		teacherStatus = "EPINotCompleted";
	else if(statusId==4)
		teacherStatus = "EPICompleted";
	
	//alert($('#'+teacherStatus+"ID").val());
	
	if($('#'+teacherStatus+"ID").val() == 0 )
	{
		$('#message2show').html(resourceJSON.msgPlzEnterDefmessage);
		$('#myModal2').modal('show');
	}
	else if($('input[name=case]:checkbox:checked').length == 0 )
	{
		$('#message2show').html(resourceJSON.msgAtleast1Cand);
		$('#myModal2').modal('show');
	} 
	else
	{
		$('#loadingDivInventory').show();
		var checkBox = document.getElementsByName("case");
		var arr =[];
		for(i=0;i<checkBox.length;i++)
		{
			if(checkBox[i].checked==true)
				arr.push(checkBox[i].value);
		}

		AdminDashboardAjax.sendNotificationMessagesToApplicants(arr,teacherStatus,{ 
			async: true,
			callback: function(data){
			//alert(data);
			if(data==1)
			{
				$('#message2show').html(resourceJSON.msgSendSuccess);
				$('#myModal2').modal('show');
				$('#loadingDivInventory').hide();
				getCandidates();
			}
		},
		errorHandler:handleError  
		});
	}
}
function getDefaultMsg()
{
	$('#errordivDefaultMsg').empty();
	$('#msgDash').find(".jqte_editor").css("background-color", "");
	$('#defaultSubject').css("background-color", "");
	
	var statusId =	$('#statusId').val(); 
	var teacherStatus = "";
	if(statusId==1)
		teacherStatus = "NotAuthenticated";
	else if(statusId==2)
		teacherStatus = "EPINotStarted";
	else if(statusId==3)
		teacherStatus = "EPINotCompleted";
	else if(statusId==4)
		teacherStatus = "EPICompleted";
	
	AdminDashboardAjax.getDefaultMsg(teacherStatus,{ 
		async: true,
		callback: function(data){
		var obj = data;
		$('#defaultSubject').val(obj.subject);
		$('#msgDash').find(".jqte_editor").html(obj.message);
		$('#myModal1setMsg').modal('show');
		$('#defaultSubject').focus();
	},
	errorHandler:handleError  
	});
}

function saveDefaultMsg()
{
	$('#msgDash').find(".jqte_editor").css("background-color", "");
	$('#defaultSubject').css("background-color", "");
	$('#errordivDefaultMsg').empty();

	var cnt=0;
	var focs=0;

	if(trim($('#defaultSubject').val())=="")
	{
		$('#errordivDefaultMsg').append("&#149; "+resourceJSON.PlzEtrSub+".<br>");
		if(focs==0)
			$('#defaultSubject').focus();
		$('#defaultSubject').css("background-color", "#F5E7E1");
		cnt++;focs++;
	}
	if ($('#msgDash').find(".jqte_editor").text().trim()=="")
	{
		$('#errordivDefaultMsg').append("&#149; "+resourceJSON.PlzEtrMsg+".<br>");
		if(focs==0)
			$('#msgDash').find(".jqte_editor").focus();
		$('#msgDash').find(".jqte_editor").css("background-color", "#F5E7E1");
		cnt++;focs++;
	}
	if(cnt==0)
	{
		//return true;
	}
	else
	{
		$('#errordivDefaultMsg').show();
		return;
	}
	
	var teacherStatus = "";
	var statusId		=	$('#statusId').val(); 
	var teacherStatusMsgId = "";
	if(statusId==1)
	{
		teacherStatusMsgId = $('#NotAuthenticatedID').val();
		teacherStatus = "NotAuthenticated";
	}
	else if(statusId==2)
	{
		teacherStatusMsgId = $('#EPINotStartedID').val();
		teacherStatus = "EPINotStarted";
	}
	else if(statusId==3)
	{
		teacherStatusMsgId = $('#EPINotCompletedID').val();
		teacherStatus = "EPINotCompleted";
	}
	else if(statusId==4)
	{
		teacherStatusMsgId = $('#EPICompletedID').val();
		teacherStatus = "EPICompleted"; 
	}
	
	AdminDashboardAjax.setDefaultMsg(teacherStatusMsgId,$('#defaultSubject').val(),$('#msgDash').find(".jqte_editor").html(),teacherStatus,{ 
		async: true,
		callback: function(data){
		//alert(data);
		if(statusId==1)
			$('#NotAuthenticatedID').val(data);
		else if(statusId==2)
			$('#EPINotStartedID').val(data);
		else if(statusId==3)
			$('#EPINotCompletedID').val(data);
		else if(statusId==4)
			$('#EPICompletedID').val(data);
		
		$('#myModal1setMsg').modal('hide');
		$('#message2show').html(resourceJSON.msgUpdateSuccess);
		$('#myModal2').modal('show');

	},
	errorHandler:handleError  
	});
}

