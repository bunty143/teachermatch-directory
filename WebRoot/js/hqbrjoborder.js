var page = 1;
var noOfRows = 10;
var sortOrderStr="";
var sortOrderType="";


function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert("Oops: Your Session has expired!");  document.location = 'signin.do';}
	else{alert("Server Error: "+exception.javaClassName);}
}
function validateAddEdithqJobOrder()
{
//	alert("validateAddEdithqJobOrder");
	var jobId = trim(document.getElementById("jobId").value);
	if(jobId==""){
		jobId = 0;
	}
    var headQuarterId = trim(document.getElementById("headQuarterId").value);
    var branchId = trim(document.getElementById("branchId").value);
    var branchName = trim(document.getElementById("branchName").value);
    var districtId = trim(document.getElementById("districtId").value);
   
   
    var jobTitle = trim(document.getElementById("jobTitle").value);
    var jobStartDate = trim(document.getElementById("jobStartDate").value);
    var jobEndDate = "";
    
	if ($("#endDateRadio1").prop("checked")) {
		jobEndDate	=	trim(document.getElementById("jobEndDate").value);
	}else if ($("#endDateRadio2").prop("checked")) {
		jobEndDate	=	"12-25-2099";
	}
	
    var jDescription = trim($('#jDescription').find(".jqte_editor").html());
    var jQualification = trim($('#jQualification').find(".jqte_editor").html());
    var jobCategoryId = trim(document.getElementById("jobCategoryId").value);
    var isExpHireNotEqualToReqNo =  0;
    var isExpHireNotEqualToReqNo = false;
    //isExpHireNotEqualToReqNo = document.getElementById("isExpHireNotEqualToReqNo").checked;            
    var branchAdministrator = document.getElementById("branchAdministrator").checked;
    var noOfExpHires = trim(document.getElementById("noOfExpHires").value);
    
    var isJobAssessment				=	trim(document.getElementById("isJobAssessmentVal").value);
    var attachJobAssessmentVal 		= 	document.getElementById("attachJobAssessmentVal").value;
	var assessmentDocument			=	document.getElementById("assessmentDocument").value;
    var prescreen = 0;
    var attachJobAssessment 		= 	document.getElementById("attachJobAssessmentVal").value;
	var attachJobAssessment1		=	document.getElementById("attachJobAssessment1").checked;
	var attachJobAssessment3		=	document.getElementById("attachJobAssessment3").checked;
	var attachJobAssessment4		=	document.getElementById("attachJobAssessment4").checked;
	var assessmentDocument			=	document.getElementById("assessmentDocument").value;
	var dateTime					=	trim(document.getElementById("dateTime").value);
	
	
	var assessmentDocumentVal = "";
    var offerNewJSI = 0;
	var attachDistrict=0;
    
    var tmInventory =0;
    if($('#tmInventory').prop('checked'))
    {
    	tmInventory=$("#tmInventory").val();    	
    }
    var hprocess = trim($('#hprocess').find(".jqte_editor").html());
    $('#errordiv').empty();
    resetColour();
    var count=0;
    if(headQuarterId=="")
    {
                $('#errordiv').show();  
                $('#errordiv').append("&#149; Please enter Headquarters Name<br>");
                $('#onlyHeadQuarterName').css("background-color", "#F5E7E1");
                $("#onlyHeadQuarterName").focus();
                count++;
                
    }
    if(branchName=="")
    {
                $('#errordiv').show();  
                $('#errordiv').append("&#149; Please enter Branch Name<br>");
                $('#branchName').css("background-color", "#F5E7E1");
                $("#branchName").focus();
                count++;
                
    }
    /*if(districtId=="")
    {
                $('#errordiv').show();  
                $('#errordiv').append("&#149; Please enter District Name<br>");
                $('#districtName').css("background-color", "#F5E7E1");
                if(count==0)
                $("#districtName").focus();
                count++;
                
    }*/
    	
    if(jobTitle=="")
    {
                $('#errordiv').show();  
                $('#errordiv').append("&#149; Please enter Job Title<br>");
                $('#jobTitle').css("background-color", "#F5E7E1");
                if(count==0)
                $("#jobTitle").focus();
                count++;
    }
   
    if(jobStartDate=="")
    {
                $('#errordiv').show();  
                $('#errordiv').append("&#149; Please enter Posting Start Date<br>");
                $('#jobStartDate').css("background-color", "#F5E7E1");
                if(count==0)
                $("#jobStartDate").focus();
                count++;
    }
    if(jobEndDate=="")
    {
                $('#errordiv').show();  
                $('#errordiv').append("&#149; Please enter Posting End Date<br>");
                $('#jobEndDate').css("background-color", "#F5E7E1");
                if(count==0)
                $("#jobEndDate").focus();
                count++;
    }
    if(jobStartDate!="" && jobEndDate!=""){
		try{
			var date1 = jobStartDate.split("-");
			var	date2 = jobEndDate.split("-");
		    var sDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
		    var eDate = new Date(date2[0]+"/"+date2[1]+"/"+date2[2]);
			if(sDate > eDate){
				$('#errordiv').show();
				$('#errordiv').append("&#149; Posting Start Date must be less than or equal to Posting End Date<br>");
				if(count==0)
				$('#jobStartDate').focus();
				$('#jobStartDate').css("background-color", "#F5E7E1");
				count++;
			} 
		}catch(err){}
	}
    if(jobCategoryId=="-1" || jobCategoryId=="")
    {
                $('#errordiv').show();  
                $('#errordiv').append("&#149; Please select Job Category<br>");
                $('#jobCategoryId').css("background-color", "#F5E7E1");
                if(count==0)
                $("#jobCategoryId").focus();
                count++;
    }

    if(tmInventory == 1)
    {
    	if(hprocess == "")
    	{
    		 $('#errordiv').show();  
             $('#errordiv').append("&#149; Please enter Message<br>");
             $('#hiringprocess').css("background-color", "#F5E7E1");
             if(count==0)
            	 $("#hiringprocess").focus();
             count++;
    	}
    	
    }
    
    if(document.getElementById("attachJobAssessment3").checked == true && document.getElementById("isJobAssessment1").checked == false)
    {
    	if(assessmentDocument=="" || assessmentDocument==null)
    	{
    		 $('#errordiv').show();  
             $('#errordiv').append("&#149; Please upload Inventory<br>");
             $('#hiringprocess').css("background-color", "#F5E7E1");
             if(count==0)
            	 $("#assessmentDocument").focus();
             count++;
    	}
    }
    
    var isInviteOnly="";
	isInviteOnly=document.getElementById('isInviteOnly').checked;
    var hiddenJob  =	$('input:radio[name=hiddenJob]:checked').val();
	if(isInviteOnly && hiddenJob==undefined)
	{		
		$('#errordiv').show();
		$('#errordiv').append("&#149; Please choose hidden job type.<br>");
		if(count==0)
   			$('#hiddenJob1').focus();
		count++;
		//focusCount++;
	}
    
    if(count!=0)
    	return false;
   
    var offerJSI = 0;
    var offerNewJSI = 0;
    var jsiReq = 0;
    if(document.getElementById("isJobAssessment1").checked == true){
		offerJSI = 0;
	}
    else
    {
    	if(document.getElementById("attachJobAssessment1").checked == true){

    		if(document.getElementById("offerJSIRadioMaindatory").checked == true){
    			offerJSI = 1;
    			jsiReq = 1;
    		} else if(document.getElementById("offerJSIRadioOptional").checked == true){
    			offerJSI = 2;
    			jsiReq = 1;
    		}
    	}
    	if(document.getElementById("attachJobAssessment3").checked == true){

    		if(document.getElementById("offerJSIRadioMaindatory").checked == true){
    			offerJSI = 1;
    			jsiReq = 1;
    		} else if(document.getElementById("offerJSIRadioOptional").checked == true){
    			offerJSI = 2;
    			jsiReq = 1;
    		}
    	}
    	if(document.getElementById("attachJobAssessment4").checked == true){

    		if(document.getElementById("offerJSIRadioMaindatory").checked == true){
    			offerJSI = 1;
    			jsiReq = 1;
    		} else if(document.getElementById("offerJSIRadioOptional").checked == true){
    			offerJSI = 2;
    			jsiReq = 1;
    		}
    	}
    }
   try{
	   $("#loadingDiv").fadeIn();
	   if(assessmentDocument!="" && isJobAssessment==1 &&  attachJobAssessment==2){
		   //alert(" upload file");
		   document.getElementById("frmJobAssessmentUpload").submit();
	   }
	   else
	   {
		   HeadQuarterAjax.saveHeadQuarterJoborder(jobId,headQuarterId,branchId,districtId,jobTitle,jobStartDate,jobEndDate,jDescription,jQualification,jobCategoryId,
		    		isExpHireNotEqualToReqNo,branchAdministrator,noOfExpHires,0,tmInventory,hprocess
		    		,jobDescriptionFileName,isInviteOnly,hiddenJob,isJobAssessment,offerJSI,attachJobAssessment,assessmentDocumentVal,jsiReq,{ 
		          async: false,
		          callback: function(data)
		          {
		          	$("#loadingDiv").hide();   
		          	
		          	
		          		//HeadQuarterAjax.saveJobOrderLog(data.split("-")[1]);
		                if(data.indexOf("success")==0)
		                {
		                	$("#jobIdText").text(data.split("-")[1]);
		              	  $("#modalSuccessMsg").modal("show");
		                }
		                else if(data.indexOf("update")==0)
		                {
		                	if($("#jobStatus").val()=="I"){
		                		$("#modalUpdateMsgforInactive").modal("show");
		                	}
		                	else{
		                	$("#modalUpdateMsg").modal("show");
		                	}
		                }
		          },
		          errorHandler:handleError 
		    	}); 
		   
		  // alert("Saved");

	   }
   }
   catch(e){}
   
}


function saveRecord()
{
	//alert(" saveRecord after jsi file upload");
	var jobId = trim(document.getElementById("jobId").value);
	if(jobId==""){
		jobId = 0;
	}
    var headQuarterId = trim(document.getElementById("headQuarterId").value);
    var branchId = trim(document.getElementById("branchId").value);
    var jobTitle = trim(document.getElementById("jobTitle").value);
    var jobStartDate = trim(document.getElementById("jobStartDate").value);
    var jobEndDate = "";
    
	if ($("#endDateRadio1").prop("checked")) {
		jobEndDate	=	trim(document.getElementById("jobEndDate").value);
	}else if ($("#endDateRadio2").prop("checked")) {
		jobEndDate	=	"12-25-2099";
	}
	var jDescription = trim($('#jDescription').find(".jqte_editor").html());
    var jQualification = trim($('#jQualification').find(".jqte_editor").html());
    var jobCategoryId = trim(document.getElementById("jobCategoryId").value);
    var isExpHireNotEqualToReqNo = false;
    //isExpHireNotEqualToReqNo = document.getElementById("isExpHireNotEqualToReqNo").checked;           
    var branchAdministrator = document.getElementById("branchAdministrator").checked;
    var noOfExpHires = trim(document.getElementById("noOfExpHires").value);
    var isJobAssessment		=	trim(document.getElementById("isJobAssessmentVal").value);
    var attachDistrict = 0;
    //var distId = document.getElementById("tempDistId").value;
    var prescreen = 0;
    var attachJobAssessment 		= 	document.getElementById("attachJobAssessmentVal").value;
	var attachJobAssessment1		=	document.getElementById("attachJobAssessment1").checked;
	var attachJobAssessment3		=	document.getElementById("attachJobAssessment3").checked;
	var attachJobAssessment4		=	document.getElementById("attachJobAssessment4").checked;
	var assessmentDocument			=	document.getElementById("assessmentDocument").value;
	var dateTime					=	trim(document.getElementById("dateTime").value);
	var assessmentDocumentVal = "";
   // prescreen =  $('input:radio[name=prescreen]:checked').val();
    
    var offerJSI = 0;
    var offerNewJSI = 0;
    var jsiReq = 0;
    var jsiReq = 0;
    if(document.getElementById("isJobAssessment1").checked == true){
		offerJSI = 0;
	}
    else
    {
    	if(document.getElementById("attachJobAssessment1").checked == true){

    		if(document.getElementById("offerJSIRadioMaindatory").checked == true){
    			offerJSI = 1;
    			jsiReq = 1;
    		} else if(document.getElementById("offerJSIRadioOptional").checked == true){
    			offerJSI = 2;
    			jsiReq = 1;
    		}
    	}
    	if(document.getElementById("attachJobAssessment3").checked == true){

    		if(document.getElementById("offerJSIRadioMaindatory").checked == true){
    			offerJSI = 1;
    			jsiReq = 1;
    		} else if(document.getElementById("offerJSIRadioOptional").checked == true){
    			offerJSI = 2;
    			jsiReq = 1;
    		}
    	}
    	if(document.getElementById("attachJobAssessment4").checked == true){

    		if(document.getElementById("offerJSIRadioMaindatory").checked == true){
    			offerJSI = 1;
    			jsiReq = 1;
    		} else if(document.getElementById("offerJSIRadioOptional").checked == true){
    			offerJSI = 2;
    			jsiReq = 1;
    		}
    	}
    }
    if(document.getElementById("attachJobAssessment3").checked == true)
    {
    	if(assessmentDocument!="")
    	{
    		offerNewJSI = 1;
    	}
    }
    var tmInventory =0;
    if($('#tmInventory').prop('checked'))
    {
    	tmInventory=$("#tmInventory").val();    	
    }
    var hprocess = trim($('#hprocess').find(".jqte_editor").html());
    
    var isInviteOnly="";
	isInviteOnly=document.getElementById('isInviteOnly').checked;
    var hiddenJob  =	$('input:radio[name=hiddenJob]:checked').val();
	
    
    if(assessmentDocument!="" && isJobAssessment==1 &&  attachJobAssessment==2)
	{
		var ext = assessmentDocument.substr(assessmentDocument.lastIndexOf('.') + 1).toLowerCase();	
		assessmentDocumentVal="Inventory"+dateTime+"."+ext;
	}
    		/*alert(" JSI Upload JobOrder ");
		   alert("1 jobId "+jobId);
		   alert("2 headQuarterId "+headQuarterId);
		   alert("3 branchId "+branchId);
		   alert("4 jobTitle "+jobTitle);
		   alert("5 jobStartDate "+jobStartDate);
		   alert("6 jobEndDate "+jobEndDate);
		   alert("7 jDescription "+jDescription);
		   alert("8 jQualification "+jQualification);
		   alert("9 jobCategoryId "+jobCategoryId);
		   alert("10 isExpHireNotEqualToReqNo "+isExpHireNotEqualToReqNo);
		   alert("11 branchAdministrator "+branchAdministrator);
		   alert("12 noOfExpHires "+noOfExpHires);
		   alert("13 attachDistrict "+attachDistrict);
		   alert("14 tmInventory "+tmInventory);
		   alert("15 hprocess "+hprocess);
		   alert("16 jobDescriptionFileName "+jobDescriptionFileName);
		   //alert("17 distId "+distId);
		   alert("18 isInviteOnly "+isInviteOnly);
		   alert("19 hiddenJob "+hiddenJob);
		   alert("20 isJobAssessment "+isJobAssessment);
		   alert("21 offerJSI "+offerJSI);
		   alert("22 attachJobAssessment "+attachJobAssessment);
		   alert("23 assessmentDocumentVal "+assessmentDocumentVal);
		   alert("24 jsiReq "+jsiReq);*/
    $('#errordiv').empty();
    resetColour();
    var count=0;
    
   try{
	   HeadQuarterAjax.saveHeadQuarterJoborder(jobId,headQuarterId,branchId,jobTitle,jobStartDate,jobEndDate,jDescription,jQualification,jobCategoryId,
	    		isExpHireNotEqualToReqNo,branchAdministrator,noOfExpHires,0,tmInventory,hprocess
	    		,jobDescriptionFileName,isInviteOnly,hiddenJob,isJobAssessment,offerJSI,attachJobAssessment,assessmentDocumentVal,jsiReq,{ 
	          async: false,
	          callback: function(data)
	          {
	          	$("#loadingDiv").hide();                         
	                if(data ==  "success")
	                {
	              	  $("#modalSuccessMsg").modal("show");
	                }
	                else if(data == "update")
	                {
	                	$("#modalUpdateMsg").modal("show");
	                }
	          },
	          errorHandler:handleError 
	    	}); 
   }
   catch(e){}
}

function actionForward()
{
	var JobOrderType=document.getElementById("JobOrderType").value;
	if(JobOrderType==2){
		window.location.href="managejoborders.do";
	}else if(JobOrderType==3){
		window.location.href="schooljoborders.do";
	}
}

function resetColour()
{
    $('#onlyHeadQuarterName').css("background-color", "");
    $('#jobTitle').css("background-color", "");
    $('#jobStartDate').css("background-color", "");
    $('#jobEndDate').css("background-color", "");
    $('#jobCategoryId').css("background-color", "");
   
}

// ========================= Add district during jobOrder ======================== //
var distAdd = 0;
function showAddDistrictLink(val)
{  
	var opt	 =	document.getElementsByName("attachDistrict");
	
	if(val	==	4)
	{
		if(distAdd==0){
		$("#addDistrictLink").fadeIn();
		}
	}
}

function clearDistrictsLink()
{
	var rd4	 =	document.getElementsByName("selectDistrict");
	$("#addDistrictDiv").hide();
	$('#districtName').val("");
	$('#errorhqbranchdiv').empty();
}

function addDistict()
{
	$("#addDistrictDiv").fadeIn();
	$("#districtName").focus();
	$('#districtName').val("");
}

function addDistrict()
{
	
	var headQuarterId = trim(document.getElementById("headQuarterId").value);
	var districtId = trim(document.getElementById("districtId").value);
	if(districtName=="")
	{
			$('#errordiv').show();	
			$('#errordiv').empty();
			$('#errordiv').append("&#149; Please enter District Name<br>");
			$('#districtName').css("background-color", "#F5E7E1");
			$("#districtName").focus();
			return false;
	}
	if(districtId=="")
	{
			$('#errordiv').show();	
			$('#errordiv').empty();
			$('#errordiv').append("&#149; Please enter valid District Name<br>");
			$('#districtName').css("background-color", "#F5E7E1");
			$("#districtName").focus();
			return false;
	}
	$("#loadingDiv").fadeIn();
	HeadQuarterAjax.saveDistrict(headQuarterId, districtId,{ 
		async: false,
		callback: function(data)
		{
		$("#loadingDiv").hide();
			if(data	==	"3")
			{
				$('#errordiv').empty();
				$('#errordiv').append("&#149; The District you enterd is already added. Please enter another District Name<br>");
				$('#errordiv').focus();
				$('#errordiv').show();
				$('#districtName').css("background-color", "#F5E7E1");
				return false;
			}
			else
			{
				document.getElementById("tempDistId").value=districtId;
				showTemp();
				document.getElementById("addDistFlag").value=1;
				$("#districtName").val("");
				$("#addDistrictLink").hide();
				$("#addDistrictDiv").hide();
				distAdd = 1;
			}
		},
		errorHandler:handleError 
}); 
}


function showTemp()
{	
	HeadQuarterAjax.showTempDistricts(noOfRows,page,sortOrderStr,sortOrderType,{ 
		async: false,
		callback: function(data)
		{
			$("#hqDistrict").html(data);	
			applyScrollOnTbl();
		},
		errorHandler:handleError 
});
}

function deleteTemp()
{
	HeadQuarterAjax.deleteTemp({ 
			async: false,
			callback: function(data)
			{
				
			},
			
	}); 
}

function removeTempDistricts(districtId)
{
	HeadQuarterAjax.deleteTempDistrict(districtId,{ 
		async: false,
		callback: function(data)
		{
			document.getElementById("addDistFlag").value="";
			$("#addDistrictLink").fadeIn();
			showTemp();			
		},
		errorHandler:handleError 
}); 
}

// ====================== End ====================== //

function disableEndDate(){
	document.getElementById('changeDateHid').value=document.getElementById('jobEndDate').value;
	document.getElementById('jobEndDate').value='';
	document.getElementById('jobEndDate').disabled=true;	
}
function unDisableEndDate(){
	document.getElementById('jobEndDate').disabled=false;
	document.getElementById('jobEndDate').value=document.getElementById('changeDateHid').value;
}
function successRedirect()
{
	window.location.href="managehqbrjoborders.do";
}


function uploadJobDescriptionFile(fileName)
{
	var jobDescriptionFileUploadErrorCount=0;
	var ext = fileName.substr(fileName.lastIndexOf('.') + 1).toLowerCase();
	var headQuarterId=document.getElementById("headQuarterId").value;
	//document.getElementById("headQuarterId").value=districtId;
	//var ddisrictId=document.getElementById("headQuarterId").value;
	$("#errordiv").empty();

	if(ext!="pdf")
	{	$('#errordiv').show();
		$('#errordiv').append("&#149; Please select Acceptable Job discription formats which include PDF files.<br>");
		$('#jobDescriptionDoc').val("");
		$('#jobDescriptionDoc').focus();
		jobDescriptionFileUploadErrorCount++;
	}
	if(headQuarterId==0)
	{	$('#errordiv').show();
		$('#errordiv').append("&#149; Please enter District<br>");
		$('#headQuarterName').css("background-color", "#F5E7E1");
		if(jobDescriptionFileUploadErrorCount==0)
		$('#headQuarterName').focus();
		$('#jobDescriptionDoc').val("");
		jobDescriptionFileUploadErrorCount++;
	}
	if(jobDescriptionFileUploadErrorCount==0){
		if(fileName!=""){
			$("#loadingDiv").show();
			document.getElementById("jobDescriptionDocUpload").submit();
		}
	}	
}
var jobDescriptionFileName="";
function saveJobDesciptionFile(fileName){
	$("#loadingDiv").hide();
	var uploadFileName=document.getElementById("uploadedJobDiscriptionFile").value;
	if(uploadFileName==""){
		document.getElementById("uploadedJobDiscriptionFile").value=fileName;
	}else{
		document.getElementById("uploadedJobDiscriptionFile").value=document.getElementById("uploadedJobDiscriptionFile").value+"@##@"+fileName;
	}
	jobDescriptionFileName=fileName;
}

function redirectBack()
{
	window.location="managehqbrjoborders.do";
}

function getDistrictDiv()
{
	var jobId=document.getElementById("jobId").value;
	if(jobId!="" && jobId!='0'){
		HeadQuarterAjax.getDistrictDiv(jobId,{ 
			async: true,
			callback: function(data)
			{
				$("#hqSavedDistrict").html(data);
			},
			errorHandler:handleError 
	});
	}
}


function unableOrDisableInviteJob()
{
	 var isInviteOnlyChecked=document.getElementById("isInviteOnly").checked;
	 if(isInviteOnlyChecked)
	 {
	   $('.hiddenInviteDiv').show();
	 }
	 else
	 {
		 $('.hiddenInviteDiv').hide();
		 $('input[name=hiddenJob]').attr('checked',false);
	 }
	 
}

function checkForHiddenJob(jobCatId)
{
	if(jobCatId!=0 && jobCatId!='')
	{
		HeadQuarterAjax.checkForHiddenJob(jobCatId,{ 
			async: false,
			callback: function(data)
			{
			if(data.jobInviteOnly){
				document.getElementById("isInviteOnly").checked = data.jobInviteOnly;
				$('.hiddenInviteDiv').show();
				if(data.hiddenJob){
					document.getElementById("hiddenJob2").checked = true;
				}else{
					document.getElementById("hiddenJob1").checked = true;
				}
			}else{
				document.getElementById("isInviteOnly").checked=false;
				 $('.hiddenInviteDiv').hide();
				 $('input[name=hiddenJob]').attr('checked',false);
			}
			},
	    });
	}else{
		$('.hiddenInviteDiv').hide();
		 $('input[name=hiddenJob]').attr('checked',false);
	} 
}

function checkForDefaultJSI(jobCatId)
{
	if(jobCatId!=0 && jobCatId!='')
	{
		HeadQuarterAjax.checkForJobCatJSI(jobCatId,{ 
			async: false,
			callback: function(data)
			{
			 	//alert(data);
			 	if(data=="found")
			 	{
			 		document.getElementById("jobCatJsiFlag").value="1";
			 		document.getElementById("attachJobAssessment1").checked=false;
					document.getElementById("attachJobAssessment4").checked=true;
			 	}
			 	else
			 	{
			 		document.getElementById("jobCatJsiFlag").value="0";
			 		document.getElementById("attachJobAssessment1").checked=true;
					document.getElementById("attachJobAssessment4").checked=false;
			 	}
			},
	    });
	}else{
		$('.hiddenInviteDiv').hide();
		 $('input[name=hiddenJob]').attr('checked',false);
	}
}

function showBranchesJobCategory()
{
	 var branchId = trim(document.getElementById("branchId").value);
	 var headQuarterId = trim(document.getElementById("headQuarterId").value);
	
	HeadQuarterAjax.populateJobCategoryByBranch(branchId,headQuarterId,{ 
		async: false,
		callback: function(data)
		{
			$("#jobCategoryId").html(data);
		},
		errorHandler:handleError 
});
}



function isJobAssessment(val)
{  
	//alert(" isJobAssessment val :: "+val);
	document.getElementById("isJobAssessmentVal").value=val;
	if(val==2){
		$("#isJobAssessmentDiv").hide();
	}else{
		$("#isJobAssessmentDiv").fadeIn();
		$("#attachJobAssessmentDiv").show();
	}
}
function attachJobAssessment(val)
{  
	//alert(" attachJobAssessment val :: "+val);
	document.getElementById("attachJobAssessmentVal").value=val;
	if(val==2){
		$("#attachJobAssessmentDiv").fadeIn();
	}else{
		$("#attachJobAssessmentDiv").hide();
	}
}

function showFile(headQuarter,headQuarterId,docFileName,linkId)
{
	if(docFileName!=null && docFileName!=''){
	HeadQuarterAjax.showFile(headQuarter,headQuarterId,docFileName,{ 
			async: false,
			errorHandler:handleError,
			callback:function(data)
			{	
		//alert(" data :: "+data);
				if(data==""){
					//alert("blank............");
					//alert("Job Specific Inventory is not uploaded.")
				}else{
					if(data.indexOf(".doc")!=-1)
					{
						document.getElementById("iframeJSI").src=data;
					}
					else
					{
						document.getElementById(linkId).href = data; 
						return false;
					}
					
				}
			}
			
		});
	}
	else
	{
		
	}
}

function downloadJsi(jobCategoryId,linkId)
{
	HqJobCategoryAjax.downloadJsi(jobCategoryId,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data){
			if(data=="")
			{
				data="javascript:void(0)";
			}		
			if(deviceType)
			{
				if (data.indexOf(".doc")!=-1)
				{
					$("#docfileNotOpen").css({"z-index":"3000"});
			    	$('#docfileNotOpen').show();
				}
				else if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					$("#exelfileNotOpen").css({"z-index":"3000"});
			    	$('#exelfileNotOpen').show();
				}
				else
				{
					document.getElementById(linkId).href = data;			
				}
			}
			else if(deviceTypeAndroid)
			{
			    if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					  
					document.getElementById("ifrmJsi").src=data;
				}
				else
				{
					document.getElementById(linkId).href = data;				
				}				
			}		
			else if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
			{
				document.getElementById("ifrmJsi").src=data;
			}
			else
			{
				document.getElementById(linkId).href = data;
			}
			return false;
		}});
}


function hideDistrictMasterDiv(dis,hiddenId,divId)
{
	if(document.getElementById("districtName").value==""){
		//document.getElementById('schoolName').readOnly=true;
		//document.getElementById('schoolName').value="";
		//document.getElementById('schoolId').value="0";
	}
	document.getElementById("districtId").value="";
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			document.getElementById("districtId").value=hiddenDataArray[index];
			//document.getElementById('schoolName').readOnly=false;
			//document.getElementById('schoolName').value="";
			//document.getElementById('schoolId').value="0";
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}

function displayJobCategoryByDistrict()
{
	var txtjobCategory=document.getElementById("txtjobCategory").value;
	var districtId=document.getElementById("districtId").value;
	if(districtId==null || districtId=="")
		districtId=0;
	
	var branchId=document.getElementById("branchId").value;
	if(branchId==null || branchId=="")
		branchId=0;
	
	var headQuarterId=document.getElementById("headQuarterId").value;
	if(headQuarterId==null || headQuarterId=="")
		headQuarterId=0;
	
	HeadQuarterAjax.displayJobCategoryByDistrictBranchAndHead(headQuarterId,branchId,districtId,txtjobCategory,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			//document.getElementById("jobCategoryDiv").innerHTML=data;
		$("#jobCategoryId").html(data);
		//alert($("#jobCat").val().trim()!="");
		if($("#jobCat").val().trim()!="")
			$("#jobCategoryId").val($("#jobCat").val());
		
		}
	});
}

function resetDistrict()
{
	
	$("#districtId").val("");
	$("#districtName").val("");
}
function resetJobCategoryOnDistrictChange()
{
	
	if($("#districtName").val()=="")
	{
		showBranchesJobCategory();
	}	
	
}


function activateDeactivateJob(jobId, status)
{
	//$("#loadingDiv").fadeIn();
	HeadQuarterAjax.updateStatus(jobId,status,{ 
		async: false,
		callback: function(data)
		{
		},
		errorHandler:handleError 
}); 
}