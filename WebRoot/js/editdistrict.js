/* @Author: Gagan 
 * @Discription: editdistrict js .
 */	
var keyContactDivVal=2;
var keyContactIdVal=null;
/*=========  Paging and Sorting ==============*/
var page = 1;
var noOfRows = 10;
var sortOrderStr="";
var sortOrderType="";

var pageNotes = 1;
var noOfRowsNotes = 10;
var sortOrderStrNotes="";
var sortOrderTypeNotes="";

var pageDist 			= 	1;
var noOfRowsDist 		= 	10;
var sortOrderStrDist	=	"";
var sortOrderTypeDist	=	"";

var domainpage = 1;
var domainnoOfRows = 10;
var domainsortOrderStr="";
var domainsortOrderType="";
function getPaging(pageno)
{	
	var gridNo	=	document.getElementById("gridNo").value;
	if(gridNo==3){
		if(pageno!='')
		{	
			domainpage=pageno;	
		}
		else
		{	
			domainpage=1;
		}
		domainnoOfRows = document.getElementById("pageSize3").value;
		getDistrictDomains();
		
	}else{
		if(pageno!='')
		{
			page=pageno;	
		}
		else
		{
			page=1;
		}
		noOfRows = document.getElementById("pageSize").value;
		displayDistrictSchools();
	}
}
/******TPL-4823--- Add configuration option HireVue Integration for District start*******/
function create_hirevue_key(element){
	if(element.checked == true || "${districtMaster.isHireVueIntegration}" == true)
		$("#hireVueAPIKeydiv").show(); 
	else{
		$("#hireVueAPIKeydiv").hide();
		document.getElementById("errorhirevueAPIkeydiv").style.display = "none";
	}
}
/******TPL-4823--- Add configuration option HireVue Integration for District end*******/

function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
		var gridNo	=	document.getElementById("gridNo").value;
		if(gridNo==3)
		{
			if(pageno!='')
			{
				domainpage=pageno;	
			}
			else
			{
				domainpage=1;
			}
			domainsortOrderStr	=	sortOrder;
			domainsortOrderType	=	sortOrderTyp;
			if(document.getElementById("pageSize3")!=null){
				domainnoOfRows = document.getElementById("pageSize3").value;
			}else{
				domainnoOfRows=10;
			}
			getDistrictDomains();
		}else if(gridNo==1){
			if(pageno!=''){
				page=pageno;	
			}else{
				page=1;
			}
			sortOrderStr	=	sortOrder;
			sortOrderType	=	sortOrderTyp;
			if(document.getElementById("pageSize")!=null){
				noOfRows = document.getElementById("pageSize").value;
			}else{
				noOfRows=10;
			}
		
			displayDistrictSchools();
		}
		else
		{	
			if(pageno!=''){
				pageNotes=pageno;	
			}else{
				pageNotes=1;
			}
			sortOrderStrNotes	=	sortOrder;
			sortOrderTypeNotes	=	sortOrderTyp;
			if(document.getElementById("pageSize")!=null){
				noOfRowsNotes = document.getElementById("pageSize").value;
			}else{
				noOfRowsNotes=10;
			}
			displayNotes();
		}
}
function getSortSchoolGrid()
{
	$('#gridNo').val("1");
}

function getSortNotesGrid()
{
	$('#gridNo').val("2")
}
function getSortDomainGrid()
{
	$('#gridNo').val("3")
}

/*========  For Handling session Time out Error ===============*/
function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert( resourceJSON.msgServerErr+": "+exception.javaClassName);}
}
/*=========== For Resolving IE TextArea Maxlength Problem ================*/
function chkLenOfTextArea(textValue,maxlength)
{
	var tlen=textValue.value.length;
	if(tlen>maxlength)
	{
		document.getElementById(textValue.id).value=document.getElementById(textValue.id).value.substr(0,maxlength)
	}
}

/*======== Redirect to Manage User Page ===============*/
	function cancelDistrict()
	{
		window.location.href="managedistrict.do";
	}

//  Get city List according to state 
	function getCityListByState()
	{
		var stateId = document.getElementById("stateId").value;
		DistrictAjax.getCityListByState(stateId, {async: false, callback: function(data)
			{
				document.getElementById("cityId").innerHTML	=	data;
			},
			errorHandler:handleError 
		});
		return true;
	}

	function addAdministrator(roleId)
	{
		$('#errordistrictdiv').empty();
		$('#firstName').css("background-color", "");
		$('#lastName').css("background-color", "");
		$('#emailAddress').css("background-color", "");
		$('#attachContactTypeAdduser').html("");
		dwr.util.setValues({firstName:null,lastName:null,title:null,emailAddress:null,phone:null,mobileNumber:null});
		$("#addAdministratorDiv").fadeIn();
		$('#roleId').val(roleId);
		$('#salutation').focus();
	}
	
	function clearUser()
	{
		$('#errordistrictdiv').empty();
		$('#firstName').css("background-color", "");
		$('#lastName').css("background-color", "");
		$('#emailAddress').css("background-color", "");
		dwr.util.setValues({firstName:null,lastName:null,title:null,emailAddress:null,phone:null,mobileNumber:null});
		//$('#phoneNumber').val("");
		$("#addAdministratorDiv").hide();
	}
	
	function addNotes()
	{
		$("#addNotesDiv").fadeIn();
		$('#errornotediv').empty();
		$('#dNote').find(".jqte_editor").css("background-color", "");
		$('#dNote').find(".jqte_editor").focus();
		$('#dNote').find(".jqte_editor").html("");
	}
	function showFile(flagVal,districtORSchoool,districtORSchooolId,docFileName,linkId)
	{
		if(docFileName!=null && docFileName!=''){
			
			DistrictAjax.showFile(districtORSchoool,districtORSchooolId,docFileName,{ 
				async: false,
				errorHandler:handleError,
				callback:function(data)
				{	
					if(data==""){			
						//alert("Job Specific Inventory is not uploaded.")
					}else{
						if(flagVal==1){
							document.getElementById("showLogo").innerHTML="<img src=\""+data+"\">";
						}else if(data.indexOf(".doc")!=-1)
						{
							document.getElementById("iframeJSI").src=data;
						}
						else
						{
							document.getElementById(linkId).href = data; 
							return false;
						}
						
					}
				}
				
			});
		}
	}
	function uploadAssessment(val){
		
		if(!(val==null || val=='undefind' || val==0)){
			$("#uploadAssessmentDiv").fadeIn();
			
			if(document.getElementById("cbxUploadAssessment").checked==false)
			{
				$("#uploadAssessmentDiv").hide();
			}
		}
	}
	
	function showKeyContact()
	{
		keyContactIdVal=null;
		keyContactDivVal=2;
		dwr.util.setValues({ dropContactType:null});
		dwr.util.setValues({ keyContactId:null,keyContactFirstName:null,keyContactLastName:null,keyContactEmailAddress:null,keyContactPhoneNumber:null,keyContactTitle:null});
		$('#keyContactFirstName').css("background-color", "");
		$('#keyContactLastName').css("background-color", "");
		$('#keyContactEmailAddress').css("background-color", "");
		$('#errorkeydiv').empty();
		$("#addKeyContactDiv").fadeIn();
		$('#dropContactType').focus();
	}

	function displayKeyContact()
	{
		var districtId	=	document.getElementById("districtId").value;
		DistrictAjax.displayKeyContactGrid(districtId,{ 
			async: true,
			callback: function(data)
			{
				$('#keyContactTable').html(data);
				$('#addKeyContactDiv').hide();
			},
			errorHandler:handleError 
		});
	}
	
	/*========  Check Valid Email address  ===============*/
	function isEmailAddress(str) 
	{	
		var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
		return emailPattern.test(str);
	}

	function addKeyContact()
	{
		$('#errorkeydiv').empty();
		$('#keyContactFirstName').css("background-color", "");
		$('#keyContactLastName').css("background-color", "");
		$('#keyContactEmailAddress').css("background-color", "");
		var keyContactId						=	trim(document.getElementById("keyContactId").value);
		var districtId							=	trim(document.getElementById("districtId").value);
		var keyContactTypeId					=	trim(document.getElementById("dropContactType").value);
		var keyContactFirstName						=	trim(document.getElementById("keyContactFirstName").value);
		var keyContactLastName						=	trim(document.getElementById("keyContactLastName").value);
		var keyContactEmailAddress				=	trim(document.getElementById("keyContactEmailAddress").value);
		var keyContactPhoneNumber				=	trim(document.getElementById("keyContactPhoneNumber").value);
		var keyContactTitle						=	trim(document.getElementById("keyContactTitle").value);
		var counter								=	0;
		var focusCount							=	0;
		if(keyContactFirstName=="")
		{
			$('#errorkeydiv').show();	
			$('#errorkeydiv').append("&#149; "+resourceJSON.msgContFName+"<br>");
			$('#keyContactFirstName').css("background-color", "#F5E7E1");
			$('#keyContactFirstName').focus();
			counter++;
			focusCount++;
			//return false;
		}
		if(keyContactLastName=="")
		{
			$('#errorkeydiv').show();	
			$('#errorkeydiv').append("&#149; "+resourceJSON.msgContLName+"<br>");
			$('#keyContactLastName').css("background-color", "#F5E7E1");
			//$('#keyContactLastName').focus();
			counter++;
			focusCount++;
			//return false;
		}
		if(keyContactEmailAddress=="")
		{
			$('#errorkeydiv').show();	
			$('#errorkeydiv').append("&#149; "+resourceJSON.msgContEmail+"<br>");
			$('#keyContactEmailAddress').css("background-color", "#F5E7E1");
			//$('#keyContactEmailAddress').focus();
			counter++;
			focusCount++;
			//return false;
		}else 
		if(keyContactEmailAddress!="")
		{
			if(!isEmailAddress(keyContactEmailAddress))
			{	$('#errorkeydiv').show();	
				//$('#errorkeydiv').empty();
				$('#errorkeydiv').append("&#149; "+resourceJSON.PlzEtrVldEmail+"<br>");
				 //alert("Please enter valid email id");
				$('#keyContactEmailAddress').css("background-color", "#F5E7E1");
				//return false;
				counter++;
				focusCount++;
			}
		}
		
		if(counter	==	0)
		{
			$('#loadingDiv').fadeIn();
			DistrictAjax.saveKeyContact(keyContactDivVal,keyContactId,districtId,keyContactTypeId,keyContactFirstName,keyContactLastName,keyContactEmailAddress,keyContactPhoneNumber,keyContactTitle,keyContactIdVal, { 
				async: true,
				callback: function(data)
				{
					$('#loadingDiv').hide();
					if(data==3){
						$('#errorkeydiv').show();	
						$('#errorkeydiv').append("&#149; "+resourceJSON.msgAlreadyRegisWithEmail+"<br>");
						$('#keyContactEmailAddress').css("background-color", "#F5E7E1");
					}else if(data==4){
						$('#errorkeydiv').show();	
						$('#errorkeydiv').append("&#149; "+resourceJSON.msgKeyContWithSameEmail+"<br>");
						$('#keyContactEmailAddress').css("background-color", "#F5E7E1");
					}else{
						displayKeyContact();
					}
				},
				errorHandler:handleError
			});
		}
		else
		{
			$('#errorkeydiv').show();
			return false;
		}
		//$('#errorkeydiv').empty();
	}
	function aKeyContactDiv(val){
		keyContactDivVal=val;
		if(val==1){
			editKeyContact(keyContactIdVal);
		}
	}
	function beforeEditKeyContact(keyContactId){
		keyContactIdVal=keyContactId;
		document.getElementById("Msg").innerHTML= resourceJSON.msgAnyChangeinDetails ;
		$('#myModalMsg').modal('show');
	}
	
	/*========  Edit Domain ===============*/
	function editKeyContact(keyContactId)
	{
		$('#keyContactFirstName').css("background-color", "");
		$('#keyContactLaststName').css("background-color", "");
		$('#keyContactEmailAddress').css("background-color", "");
		$('#errorkeydiv').empty();
		$('#addKeyContactDiv').fadeIn();
		
		DistrictAjax.getKeyContactsBykeyContactId(keyContactId, { 
			async: true,
			callback: function(data)
			{
				//alert("data keyContactTypeId "+data.keyContactTypeId.contactTypeId+" Name"+data.keyContactTypeId.contactType);
				dwr.util.setValues(data);
				var optsKeyContact = document.getElementById('dropContactType').options;
				for(var i = 0, j = optsKeyContact.length; i < j; i++)
				{
					  if(data.keyContactTypeId.contactTypeId==optsKeyContact[i].value)
					  {
						  optsKeyContact[i].selected	=	true;
					  }
				}
			},
			errorHandler:handleError
		});
		return false;
	}

	function deleteKeyContact(keyContactId)
	{
		if (confirm(resourceJSON.msgDeleteCont)) {
			DistrictAjax.deleteKeyContact(keyContactId, { 
				async: true,
				callback: function(data)
				{
					displayKeyContact();
				},
				errorHandler:handleError
				});
		}
	}
	
	function clearKeyContact()
	{
		$("#addKeyContactDiv").hide();
	}
	
	/*================== Notes Functionality =========================*/
	function displayNotes()
	{
		//alert("Hi");
		dwr.util.setValues({note:null});
		var districtId	=	document.getElementById("districtId").value;
		DistrictAjax.displayNotesGrid(districtId,noOfRowsNotes,pageNotes,sortOrderStrNotes,sortOrderTypeNotes,{ 
			async: true,
			callback: function(data)
			{
				//$('#notesTable').html(data);
				$('#notesGrid').html(data);
				applyScrollOnTblNotes();
				$('#addNotesDiv').hide();
			},
			errorHandler:handleError
		});
	}
	
	function saveNotes()
	{
		var note								=	trim($('#dNote').find(".jqte_editor").html())
		var districtId							=	trim(document.getElementById("districtId").value);
		
		if($('#dNote').find(".jqte_editor").text().trim()==""){
			$('#errornotediv').show();	
			$('#errornotediv').empty();
			$('#errornotediv').append("&#149; "+resourceJSON.PlzEtrNotes+"<br>");
			$('#dNote').find(".jqte_editor").focus();
			$('#dNote').find(".jqte_editor").css("background-color", "#F5E7E1");
			return false;
		}else if ($('#dNote').find(".jqte_editor").text().trim()!=""){
			var charCount=trim($('#dNote').find(".jqte_editor").text());
			var count = charCount.length;
			if(count>500)
			{
				$('#errornotediv').show();	
				$('#errornotediv').empty();
				$('#errornotediv').append("&#149; "+resourceJSON.msgNotesLen500+"<br>");
				$('#dNote').find(".jqte_editor").focus();
				$('#dNote').find(".jqte_editor").css("background-color", "#F5E7E1");
				return false;
			}
		}
		$('#errornotediv').empty();
		
		DistrictAjax.saveNotes(note,districtId, { 
			async: true,
			callback: function(data)
			{
				displayNotes();
			},
			errorHandler:handleError
		});
	}

	function deleteNotes(notesId)
	{
		$('#note').css("background-color", "");
		$('#errornotediv').empty();
		if (confirm(  resourceJSON.msgdeleteNotes1 )) {
			DistrictAjax.deleteNotes(notesId, { 
				async: true,
				callback: function(data)
				{
					displayNotes();
				},
				errorHandler:handleError
			});
		}
	}

	function clearNotes()
	{
		$("#addNotesDiv").hide();
		$('#note').css("background-color", "");
		$('#errornotediv').empty();
	}


/*
 * 			District Attachment Functionality
 * 
 * */
	
function displayDistAttachment() {
		dwr.util.setValues({note:null});
		var districtId	=	document.getElementById("districtId").value;
		DistrictAjax.displayDistAttachmentGrid(districtId,noOfRowsDist,pageDist,sortOrderStrDist,sortOrderTypeDist,{
			async: true,
			callback: function(data)
			{
				$('#distAttachmentGrid').html(data);
				applyScrollOnTblDistAttachment();
				$('#distAttachmentDiv').hide();
			},
			errorHandler:handleError
		});
	
	}

function addDistAttachment()
{
	$("#distAttachmentDiv").fadeIn();
	$("#distAttachmentEditDiv").hide();
	$('#errornotediv').empty();
}

function uploadDistFile(){
	$('#errornotediv').empty();
	var focus 		= 	0;
	var counter		= 	0;
	var districtId	=	document.getElementById("districtId").value;
	var fileName 	= 	document.getElementById("distAttachmentDocument").value;
	var ext 		= 	fileName.substr(fileName.lastIndexOf('.') + 1).toLowerCase();
	var fileSize	=	0;
	
	var filenameSS 	= 	fileName.replace(/.*(\/|\\)/, '');
	var baseFile 	= 	filenameSS.substr(0, filenameSS.lastIndexOf('.'));
	
	var d		=	new Date();
	var year	=	d.getFullYear();
	var month	=	d.getMonth();
	var date	=	d.getDate();
	var hour	=	d.getHours();
	var minute	=	d.getMinutes();
	var second	=	d.getSeconds();
	var dateString = year+"-"+month+"-"+date+"-"+hour+"-"+minute+"-"+second;
	var dateString = dateString.replace('-', '');
	var dateString = dateString .replace('-', '');
	var dateString = dateString .replace('-', '');
	var dateString = dateString .replace('-', '');
	var dateString = dateString .replace('-', '');
	
	var fullFilename = baseFile+dateString+"."+ext;
	if ($.browser.msie==true) {
	    fileSize = 0;	   
	} else {		
		if(distAttachmentDocument.files[0]!=undefined)
		fileSize = distAttachmentDocument.files[0].size;
	}
	
	if(fileName == null || fileName == ""){
		$('#errornotediv').append("&#149; "+resourceJSON.msgPlzSelectFile+"<br>");
		if(focs==0)
			$('#distAttachmentDocument').focus();
		
		$('#distAttachmentDocument').css("background-color",txtBgColor);
		counter++;
		focs++;	
		return false;
	}
	if(fileSize>=10485760)
	{
		$('#errornotediv').append("&#149; "+resourceJSON.msgfilesizelessthan+"<br>");
			if(focs==0)
				$('#distAttachmentDocument').focus();
			
			$('#distAttachmentDocument').css("background-color",txtBgColor);
			counter++;
			focs++;	
			return false;
	}

	if(counter==0){
		if(fileName!=""){
			$("#loadingDiv").show();
			var file_data = $("#distAttachmentDocument").prop("files")[0];
			var form_data = new FormData();
			form_data.append("dateString", dateString)
			form_data.append("districtId", districtId)
			form_data.append("file", file_data)
			$.ajax({
		               url: "./distFileUploadServlet.do",
		               dataType: 'text',
		               //dataType: 'script',
		               cache: false,
		               contentType: false,
		               processData: false,
		               data: form_data,
		               type: 'post',
		               success : function(response, data123, xhr){
						
						if(response=="")
						{
							DistrictAjax.saveDistrictAttachment(districtId,fullFilename,{
								async: true,
								callback: function(data)
								{
									$('#distAttachmentGrid').html(data);
									applyScrollOnTblDistAttachment();
									displayDistAttachment();
								},
								errorHandler:handleError
							});
								$("#loadingDiv").hide();
						}
						else
							fileContainsVirusDiv(response);
					}
			})
		}
	}
}

function deleteDistrictAttachment(districtattachmentId) {
	$('#note').css("background-color", "");
	$('#errornotediv').empty();
	if (confirm( resourceJSON.msgdeleteNotes1 )) {
		DistrictAjax.deleteDistrictAttachment(districtattachmentId, {
			async: true,
			callback: function(data)
			{
				$('#distAttachmentGrid').html(data);
				applyScrollOnTblDistAttachment();
				displayDistAttachment();
			},
			errorHandler:handleError
		});
	}
}

function editDistrictAttachment(districtattachmentId) {
	$('#note').css("background-color", "");
	$("#distAttachmentEditDiv").fadeIn();
	$('#errornotediv').empty();
	$("#distAttachmentDiv").hide();
	document.getElementById("myModalViewDist").innerHTML 	= 	"<a href=\"javascript:void(0)\" onclick=\"vieDistrictFile("+districtattachmentId+")\">"+resourceJSON.ViewLink+"</a>";
	document.getElementById("myModalEditDist").innerHTML	=	"<a href=\"javascript:void(0)\" onclick=\"return uploadEditDistFile("+districtattachmentId+")\">"+resourceJSON.msgIamDone+"</a>";
}

function uploadEditDistFile(districtattachmentId) {
	$('#errornotediv').empty();
	var focus 		= 	0;
	var counter		= 	0;
	var districtId	=	document.getElementById("districtId").value;
	var fileName 	= 	document.getElementById("distAttachmentEditDocument").value;
	var ext 		= 	fileName.substr(fileName.lastIndexOf('.') + 1).toLowerCase();
	var fileSize	=	0;
	
	var filenameSS 	= 	fileName.replace(/.*(\/|\\)/, '');
	var baseFile 	= 	filenameSS.substr(0, filenameSS.lastIndexOf('.'));

	var d		=	new Date();
	var year	=	d.getFullYear();
	var month	=	d.getMonth();
	var date	=	d.getDate();
	var hour	=	d.getHours();
	var minute	=	d.getMinutes();
	var second	=	d.getSeconds();
	var dateString = year+"-"+month+"-"+date+"-"+hour+"-"+minute+"-"+second;
	var dateString = dateString.replace('-', '');
	var dateString = dateString .replace('-', '');
	var dateString = dateString .replace('-', '');
	var dateString = dateString .replace('-', '');
	var dateString = dateString .replace('-', '');

	var fullFilename = baseFile+dateString+"."+ext;
	if ($.browser.msie==true) {
	    fileSize = 0;	   
	} else {		
		if(distAttachmentEditDocument.files[0]!=undefined)
		fileSize = distAttachmentEditDocument.files[0].size;
	}
	if(fileName == null || fileName == ""){
		$('#errornotediv').append("&#149; "+resourceJSON.msgPlzSelectFile+"<br>");
		if(focs==0)
			$('#distAttachmentEditDocument').focus();
		
		$('#distAttachmentEditDocument').css("background-color",txtBgColor);
		counter++;
		focs++;	
		return false;
	}
	if(fileSize>=10485760)
	{
		$('#errornotediv').append("&#149; "+resourceJSON.msgfilesizelessthan+"<br>");
			if(focs==0)
				$('#distAttachmentEditDocument').focus();
			
			$('#distAttachmentEditDocument').css("background-color",txtBgColor);
			counter++;
			focs++;	
			return false;
	}
	
	
	if(counter==0){
		if(fileName!=""){
			$("#loadingDiv").show();
			var file_data = $("#distAttachmentEditDocument").prop("files")[0];
			var form_data = new FormData();
			form_data.append("dateString", dateString)
			form_data.append("districtId", districtId)
			//form_data.append("districtattachmentId", districtattachmentId)
			form_data.append("file", file_data)
			$.ajax({
		               url: "./distFileUploadServlet.do",
		               dataType: 'text',
		               cache: false,
		               contentType: false,
		               processData: false,
		               data: form_data,
		               type: 'post',
		               success : function(response, data123, xhr) {
							if(response=="")
							{
								DistrictAjax.editDistrictAttachment(districtId,districtattachmentId,fullFilename,{
									async: true,
									callback: function(data)
									{
										$('#distAttachmentGrid').html(data);
										applyScrollOnTblDistAttachment();
										displayDistAttachment();
									},
									errorHandler:handleError
								});
								$("#loadingDiv").hide();
							}
							else
								fileContainsVirusDiv(response);
						}
		       })
		}
	}
}



function vieDistrictFile(districtattachmentId) {
	DistrictAjax.vieDistrictFile(districtattachmentId,{
	async: true,
	errorHandler:handleError,
	callback:function(data)
	{
		if(deviceType)
		{
			if (data.indexOf(".doc")!=-1)
			{
				$("#docfileNotOpen").css({"z-index":"3000"});
		    	$('#docfileNotOpen').show();
			}
			else if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
			{
				$("#exelfileNotOpen").css({"z-index":"3000"});
		    	$('#exelfileNotOpen').show();
			}
			else
			{
				document.getElementById(linkid).href = data;
			}
		}
		else if(deviceTypeAndroid)
		{
			if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
			{
				 $('#distAttachmentDiv').modal('hide');
				 document.getElementById('ifrmAttachment').src = ""+data+"";
			}
			else
			{
				document.getElementById(linkid).href = data;
			}
		}
		else if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
		{
			 $('#distAttachmentShowDiv').modal('hide');
			 document.getElementById('ifrmAttachment').src = ""+data+"";
		} else {
				$('.distAttachmentShowDiv').modal('show');
				document.getElementById('ifrmAttachment').src = ""+data+"";
		}
	}});

	}



/*			End Attachment Functionality	*/
	
	
	
/*============ Adding District User's Administrator and Analyst ========================*/
	function validateDistrictAdministratorOrAnalyst()
	{
		$('#loadingDiv').fadeIn();
		$('#errordistrictdiv').empty();
		$('#firstName').css("background-color", "");
		$('#lastName').css("background-color", "");
		$('#emailAddress').css("background-color", "");
		var keyContactTypeId ="";
		var salutation			=	trim(document.getElementById("salutation").value);
		var firstName			=	trim(document.getElementById("firstName").value);
		var lastName			=	trim(document.getElementById("lastName").value);
		var title				=	trim(document.getElementById("title").value);
		var emailAddress		=	trim(document.getElementById("emailAddress").value);
//		var password			=	trim(document.getElementById("password").value);
		var phoneNumber			=	trim(document.getElementById("phone").value);
		var mobileNumber		=	trim(document.getElementById("mobileNumber").value);
		var roleId				=	document.getElementById("roleId").value;
		var entityType			=	"";
		var authenticationCode	=	"";
		var AEU_RoleId			=	"";
		
		var districtId			=	document.getElementById("districtId").value;
		var counter				=	0;
		var focusCount			=	0;
		
		var roleIdOfLoginUser	=	trim(document.getElementById("roleIdUserLogin").value);
		if(roleIdOfLoginUser==1||roleIdOfLoginUser==4||roleIdOfLoginUser==7)
		{
			var attachedKeyContactId ="";
			$("#attachContactTypeAdduser option").each(function()
			{
				attachedKeyContactId=attachedKeyContactId+$(this).val()+"##";
					});
			keyContactTypeId=attachedKeyContactId;
		}
		
		if (firstName	==	"")
		{
			$('#errordistrictdiv').show();
			$('#errordistrictdiv').append("&#149; "+resourceJSON.PlzEtrFirstName+"<br>");
			if(focusCount	==	0)
				$('#firstName').focus();
			$('#firstName').css("background-color", "#F5E7E1");
			counter++;
			focusCount++;
		}
		if (lastName	==	"")
		{
			$('#errordistrictdiv').show();
			$('#errordistrictdiv').append("&#149; "+resourceJSON.PlzEtrLastName+"<br>");
			if(focusCount	==	0)
				$('#lastName').focus();
			$('#lastName').css("background-color", "#F5E7E1");
			counter++;
			focusCount++;
		}
		if (emailAddress=="")
		{
			$('#errordistrictdiv').show();
			$('#errordistrictdiv').append("&#149; "+resourceJSON.PlzEtrEmail+"<br>");
			if(focusCount	==	0)
				$('#emailAddress').focus();
			$('#emailAddress').css("background-color", "#F5E7E1");
			counter++;
			focusCount++;
		}
		else if(!isEmailAddress(emailAddress))
		{		
			$('#errordistrictdiv').append("&#149; "+resourceJSON.PlzEtrVldEmail+"<br>");
			if(focusCount==0)
				$('#emailAddress').focus();
			
			$('#emailAddress').css("background-color", "#F5E7E1");
			counter++;
			focusCount++;
		}
		
		if(counter	==	0)
		{
			dwr.engine.beginBatch();
			DistrictAjax.saveDistrictAdministratorOrAnalyst(districtId,keyContactTypeId,emailAddress,firstName,lastName,salutation,entityType,title,phoneNumber,mobileNumber,authenticationCode,roleId,{ 
				async: true,
				callback: function(data)
				{
					if((data	==	3) || (data	==	4))
					{
						$('#errordistrictdiv').append("&#149; "+resourceJSON.msgaladyemail+"<br>");
						if(focusCount==0)
						$('#emailAddress').focus();
						$('#errordistrictdiv').show();
						$('#emailAddress').css("background-color", "#F5E7E1");
						$('#loadingDiv').hide();
						return false;
					}
					else
					{
						if(data	==	2)
						{
							alert(" Server Error ");
						}
						else
						{
							if(roleId==2)
							{
								displayDistrictAdministrator(roleId);
							}
							if(roleId==5)
							{
								displayDistrictAnalyst(roleId);
							}
							clearUser();
							$('#loadingDiv').hide();
						}
					}
				},
				errorHandler:handleError 
				});
				dwr.engine.endBatch();					
		}
		else
		{
			$('#errordistrictdiv').show();
			$('#loadingDiv').hide();
			return false;
		}
	}

/*=========== Displaying Administrator and Analyst Grid =====================*/	
	function displayDistrictAdministrator(roleId)
	{
		tpJbIDisable();
		var districtId			=	document.getElementById("districtId").value;
		DistrictAjax.displayDistrictAdministratorGrid(districtId,roleId,{ 
			async: true,
			callback: function(data)
			{
				$('#districtAdministratorDiv').html(data);
				/*========== for showing Tool tip on Images ============*/
				tpJbIEnable();
			},
			errorHandler:handleError 
		});
	}

	function tpJbIEnable()
	{
		var noOrRow = $('#districtAdministratorDiv').children().size();
		for(var j=1;j<=noOrRow;j++)
		{
			$('#actDeactivateUserAdministrator'+j).tooltip();
		}
	}
	function tpJbIDisable()
	{
		var noOrRow = $('#districtAdministratorDiv').children().size();
		for(var j=1;j<=noOrRow;j++)
		{
			$('#actDeactivateUserAdministrator'+j).trigger('mouseout');
		}
	}
	function displayDistrictAnalyst(roleId)
	{
		tpJbIAnalystDisable();
		var districtId			=	document.getElementById("districtId").value;
		DistrictAjax.displayDistrictAdministratorGrid(districtId,roleId,{ 
			async: true,
			callback: function(data)
			{
				$('#districtAnalystDiv').html(data);
				tpJbIAnalystEnable();
			},
			errorHandler:handleError 
		});
	}
	/*========== For Analyst Enabling Tool Tip =============*/
	function tpJbIAnalystEnable()
	{
		var noOrRow = $('#districtAnalystDiv').children().size();
		for(var j=1;j<=noOrRow;j++)
		{
			$('#actDeactivateUserAnalyst'+j).tooltip();
		}
	}
	function tpJbIAnalystDisable()
	{
		var noOrRow = $('#districtAnalystDiv').children().size();
		for(var j=1;j<=noOrRow;j++)
		{
			$('#actDeactivateUserAnalyst'+j).trigger('mouseout');
		}
	}
	/*========  activateDeactivateUser ===============*/
	function activateDeactivateDistrictAdministratorOrAnalyst(roleId,userId,status)
	{
		UserAjax.activateDeactivateUser(userId,status, { 
			async: true,
			callback: function(data)
			{
				//alert("data "+data);
				if(roleId==2)
				{
					displayDistrictAdministrator(roleId);
				}
				if(roleId==5)
				{
					displayDistrictAnalyst(roleId);
				}
			},
			errorHandler:handleError 
		});
	}
	function clearFile(val){
		// line added by ashish ratan for IE 10-11
		// replace field with old html
		// comment by ashish ratan
			
		if(val==1){
			$('#logoPathFile')
			.replaceWith(
					"<input id='logoPathFile' name='logoPathFile' type='file' width='20px;' />");	 
			
			// this property gives error in IE ....
			document.getElementById("logoPathFile").value="";
		}else{
			$('#assessmentUploadURLFile')
			.replaceWith(
					"<input id='assessmentUploadURLFile' name='assessmentUploadURLFile' type='file' width='20px;' />");	 
			
			
			document.getElementById("assessmentUploadURLFile").value="";
		}
	}
/*============ Gagan : setMosaicFlag() For making slider Disable  ==================*/

	
	function validateEditDistrict()
	{
		$('#loadingDiv').fadeIn();
		//return false;
		$('#erroroReminderSetPortfolio').empty();
		$('#errorofferVVIDiv').empty();
		$('#errorofferDASMT').empty();
		$('#errormosaicinfodiv').empty();
		$('#errorcontactinfodiv').empty();
		$('#erroraccountinfodiv').empty();
		$('#erroroReminderSet').empty();
		$('#errorlogoddiv').empty();
		$('#dmName').css("background-color", "");
		$('#dmPassword').css("background-color", "");
		//$('#acName').css("background-color", "");
		$('#amName').css("background-color", "");
		$('#dmEmailAddress').css("background-color", "");
		$('#acEmailAddress').css("background-color", "");
		$('#distributionEmail').css("background-color", "");
		$('#amEmailAddress').css("background-color", "");
		$('#emailForTeacher').css("background-color", "");
		$('#errordatediv').empty();
		document.getElementById('distASMTIDSList').value="";
		var generalInfoFlag=false;
		var contactInfoFlag=false;
		var userFlag=false;
		var accInfoFlag=false;
		var mosaicFlag=false;
		var pDistFlag=false;
		
		var dmName					=	trim(document.getElementById("dmName").value);
		var dmPassword				=	document.getElementById("dmPassword").value;
		//var acName					=	trim(document.getElementById("acName").value);
		var amName					=	trim(document.getElementById("amName").value);
		var dmEmailAddress			=	trim(document.getElementById("dmEmailAddress").value);
		var acEmailAddress			=	trim(document.getElementById("acEmailAddress").value);
		var amEmailAddress			=	trim(document.getElementById("amEmailAddress").value);
		var distributionEmail		=	trim(document.getElementById("distributionEmail").value);
		
		//var dDescription			=	$('#dDescription').find(".jqte_editor").text().trim();
		var emailForTeacher			=	trim(document.getElementById("emailForTeacher").value);
		
		var dDescription			=	trim($('#dDescription').find(".jqte_editor").text());
		var counter				=	0;
		var focusCount			=	0;
		
		var pkOffered	=	document.getElementById("pkOffered").checked;
		var kgOffered	=	document.getElementById("kgOffered").checked;
		var g01Offered	=	document.getElementById("g01Offered").checked;
		var g02Offered	=	document.getElementById("g02Offered").checked;
		var g03Offered	=	document.getElementById("g03Offered").checked;
		var g04Offered	=	document.getElementById("g04Offered").checked;
		var g05Offered	=	document.getElementById("g05Offered").checked;
		var g06Offered	=	document.getElementById("g06Offered").checked;
		var g07Offered	=	document.getElementById("g07Offered").checked;
		var g08Offered	=	document.getElementById("g08Offered").checked;
		var g09Offered	=	document.getElementById("g09Offered").checked;
		var g10Offered	=	document.getElementById("g10Offered").checked;
		var g11Offered	=	document.getElementById("g11Offered").checked;
		var g12Offered	=	document.getElementById("g12Offered").checked;
		var allGradeUnderContractVal=document.getElementById("allGradeUnderContract").checked;
		if(allGradeUnderContractVal==true && pkOffered==false && kgOffered==false && g01Offered==false && g02Offered==false && g03Offered==false && g04Offered==false && g05Offered==false && g06Offered==false && g07Offered==false && g08Offered==false &&	g09Offered==false && g10Offered==false && g11Offered==false && g12Offered==false){
			$('#errordatediv').show();
			$('#errordatediv').append("&#149; "+resourceJSON.msgselectatoneGrade+"<br>");
			if(focusCount	==	0)
				$('#allGradeUnderContract').focus();
			$('#allGradeUnderContract').css("background-color", "#F5E7E1");
			counter++;
			focusCount++;
			dateErrorFlag=1;
		}

		/******TPL-4823--- Add configuration option HireVue Integration for District start*******/
		 var hireVueAPIKeyval = document.getElementById("hireVueAPIKey").value;
		 var isHireVueIntegrationVal = document.getElementById("isHireVueIntegration");
		 if(isHireVueIntegrationVal.checked == true && (hireVueAPIKeyval == null || hireVueAPIKeyval=="")){
			 $("#errorhirevueAPIkeydiv").show();
			 $('#errorhirevueAPIkeydiv').focus();
			counter++;
		 }
		/******TPL-4823--- Add configuration option HireVue Integration for District end*******/
		
		
		var initiatedOnDate		=	trim(document.getElementById("initiatedOnDate").value);
		var contractStartDate	=	trim(document.getElementById("contractStartDate").value);
		var contractEndDate		=	trim(document.getElementById("contractEndDate").value);
		
		var logoPathFile		=	trim(document.getElementById("logoPathFile").value);
		if(logoPathFile!="" && logoPathFile!=null)
		{
			var ext = logoPathFile.substr(logoPathFile.lastIndexOf('.') + 1).toLowerCase();
			var fileSize = 0;
			if ($.browser.msie==true)
		 	{	
			    fileSize = 0;	   
			}
			else
			{
				if(document.getElementById("logoPathFile").files[0]!=undefined)
				{
					fileSize = document.getElementById("logoPathFile").files[0].size;
				}
			}
			if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png'))
			{
				$('#errorlogoddiv').show();
				$('#errorlogoddiv').append("&#149; "+resourceJSON.msgLogoFormat+"<br>");
				if(focusCount	==	0)
				$('#logoPathFile').focus();
				$('#logoPathFile').css("background-color", "#F5E7E1");
				counter++;
				focusCount++;
				generalInfoFlag=true;
			}
			else if(fileSize>=10485760)
			{			
				$('#errorlogoddiv').show();
				$('#errorlogoddiv').append("&#149; "+resourceJSON.msgfilesizelessthan+"<br>");
				if(focusCount	==	0)
				$('#logoPathFile').focus();
				$('#logoPathFile').css("background-color", "#F5E7E1");
				counter++;
				focusCount++;
				generalInfoFlag=true;
			}
		}
		
		/* Start :: Virtual Video Interview */
		var offerVVI 		= 	document.getElementById("offerVirtualVideoInterview").checked;
		var quesSetId		=	"";
		var quesName		=	"";
		var wantScore 		= 	"";
		var sendVVILink		=	"";
		var maxScore		=	"";
		var jobCompletedVVILink1		=	"";
		var jobCompletedVVILink2		=	"";
		
		//alert(" quesSetId "+quesSetId+" maxScore "+maxScore);
		
		if(offerVVI==true)
		{
			quesSetId		=	trim(document.getElementById("quesId").value);
			quesName		=	trim(document.getElementById("quesName").value);
			wantScore 		= 	document.getElementById("wantScore").checked;
			sendVVILink		=   document.getElementById("sendAutoVVILink").checked;
			if(sendVVILink==true){
				jobCompletedVVILink1	=   document.getElementById("jobCompletedVVILink1").checked;
				jobCompletedVVILink2	=   document.getElementById("jobCompletedVVILink2").checked;
			
				if(jobCompletedVVILink2==true){
					document.getElementById("statusListBoxForVVI").value="";
				}
			
			}
			
			
			var timeAllowedPerQuestion = trim(document.getElementById("timeAllowedPerQuestion").value);
			
			if(quesSetId =="" && quesName=="")
			{
				$("#errorofferVVIDiv").show();
				$('#errorofferVVIDiv').append("&#149; "+resourceJSON.msgPleaseQuestionSet+"<br>");
				if(focusCount==0)
					$('#quesName').focus();
				
				$('#quesName').css("background-color", "#F5E7E1");
				counter++;
				focusCount++;
			}
			if(wantScore==true)
			{
				maxScore	=	trim(document.getElementById("maxScoreForVVI").value);
				
				if(maxScore=="")
				{
					$("#errorofferVVIDiv").show();
					$('#errorofferVVIDiv').append("&#149; "+resourceJSON.msgEnterMaxMarks+"<br>");
					if(focusCount==0)
						$('#wantScore').focus();
					
					$('#maxScoreForVVI').css("background-color", "#F5E7E1");
					counter++;
					focusCount++;
				}
			}
			if(timeAllowedPerQuestion!="")
			{
				if(timeAllowedPerQuestion<30)
				{
					$('#errorofferVVIDiv').show();
					$('#errorofferVVIDiv').append("&#149; "+resourceJSON.msgquestionatleast30seconds+"<br>");
					
					if(focusCount==0)
						$('#timeAllowedPerQuestion').focus();
					
					$('#timeAllowedPerQuestion').css("background-color", "#F5E7E1");
					counter++;
					focusCount++;
				}
			}
		}
		/* End :: Virtual Video Interview */
		
		/*Start :: District Assessment */
		var offerDASMT 		= 	document.getElementById("offerAssessmentInviteOnly").checked;
		var opts = "";
		var arrDAIds	=	"";
		if(offerDASMT)
		{
			opts = document.getElementById('attachedDAList').options;
			
			for(var i = 0, j = opts.length; i < j; i++)
			{
				arrDAIds+=opts[i].value+"#";
			}
			
			if(document.getElementById("attachedDAList").options.length==0)
			{
				$("#errorofferDASMT").show();
				$('#errorofferDASMT').append("&#149; "+resourceJSON.msgleastoneDistrictAssessment+"<br>");
				if(focusCount==0)
					$('#attachedDAList').focus();
				
				$('#attachedDAList').css("background-color", "#F5E7E1");
				counter++;
				focusCount++;
			}
			else
			{
				document.getElementById('distASMTIDSList').value = arrDAIds;
			}
			
		}
		
		/*End :: District Assessment */
		
		
		/* Start Reminder Status */
		var sendReminder 					= 	document.getElementById("sendReminderToIcompCandiates").checked;
		var reminderFrequencyInDays 		= 	trim(document.getElementById("reminderFrequencyInDays").value);
		var noOfReminder 					= 	trim(document.getElementById("noOfReminder").value);
		var statusIdReminderExpireAction 	= 	trim(document.getElementById("statusIdReminderExpireAction").value);
		var reminderOfFirstFrequencyInDays 	= 	trim(document.getElementById("reminderOfFirstFrequencyInDays").value);

		if(sendReminder == true){
			if(reminderFrequencyInDays == "" || reminderFrequencyInDays < 1 || reminderFrequencyInDays > 365 ){
				$('#erroroReminderSet').show();
				$('#erroroReminderSet').append("&#149; "+resourceJSON.msgFreqOfReminder+"<br>");
				$('#reminderFrequencyInDays').focus();
				counter++;
			}

			if(reminderOfFirstFrequencyInDays == "" || reminderOfFirstFrequencyInDays < 1 || reminderOfFirstFrequencyInDays > 365 ){
				$('#erroroReminderSet').show();
				$('#erroroReminderSet').append("&#149; "+resourceJSON.msgFreqOfReminderAfter1+"<br>");
				$('#reminderOfFirstFrequencyInDays').focus();
				counter++;
			}
			
			if(noOfReminder == ""){
				$('#erroroReminderSet').show();
				$('#erroroReminderSet').append("&#149; "+resourceJSON.msgReminderEmpty+"<br>");
				$('#noOfReminder').focus();
				counter++;
			}
			
		}

		/*End Reminder Status */
		
		/* Start Portfolio Reminder */
		var sendReminderForPortfolio		= 	document.getElementById("sendReminderForPortfolio").checked;
		var reminderFrequencyInDaysPortfolio= 	trim(document.getElementById("reminderFrequencyInDaysPortfolio").value);
		var noOfReminderPortfolio			= 	trim(document.getElementById("noOfReminderPortfolio").value);
		var statusIdReminderExpireActionPortfolio 	= 	trim(document.getElementById("statusIdReminderExpireActionPortfolio").value);
		var reminderOfFirstFrequencyInDaysPortfolio	= 	trim(document.getElementById("reminderOfFirstFrequencyInDaysPortfolio").value);
		var statusSendMailPortfolio 	= 	trim(document.getElementById("statusSendMailPortfolio").value);
		
		if(sendReminderForPortfolio == true){
			if(reminderFrequencyInDaysPortfolio == "" || reminderFrequencyInDaysPortfolio < 1 || reminderFrequencyInDaysPortfolio > 365 ){
				$('#erroroReminderSetPortfolio').show();
				$('#erroroReminderSetPortfolio').append("&#149; "+resourceJSON.msgFreqOfReminder+"<br>");
				$('#reminderFrequencyInDays').focus();
				counter++;
			}

			if(reminderOfFirstFrequencyInDaysPortfolio == "" || reminderOfFirstFrequencyInDaysPortfolio < 1 || reminderOfFirstFrequencyInDaysPortfolio > 365 ){
				$('#erroroReminderSetPortfolio').show();
				$('#erroroReminderSetPortfolio').append("&#149; "+resourceJSON.msgFreqOfReminderAfter1+"<br>");
				$('#reminderOfFirstFrequencyInDays').focus();
				counter++;
			}
			
			if(noOfReminderPortfolio == ""){
				$('#erroroReminderSetPortfolio').show();
				$('#erroroReminderSetPortfolio').append("&#149; "+resourceJSON.msgReminderEmpty+"<br>");
				$('#noOfReminder').focus();
				counter++;
			}
			
		}

		/*End Reminder Status */
		
		if(distributionEmail!="") 
		{
			if(!isEmailAddress(distributionEmail))
			{	
				$('#errormosaicinfodiv').append("&#149; "+resourceJSON.msgVAlidDistribEmail+"<br>");
				if(focusCount==0)
					$('#distributionEmail').focus();
				
				$('#distributionEmail').css("background-color", "#F5E7E1");
				counter++;
				focusCount++;
				mosaicFlag=true;
			}
		}
		
		$('#lblCriticialJobName').css("background-color", "");
		$('#lblAttentionJobName').css("background-color", "");
		var lblCriticialJobName	=	trim(document.getElementById("lblCriticialJobName").value);
		var lblAttentionJobName	=	trim(document.getElementById("lblAttentionJobName").value);
		//alert(" lblCriticialJobName "+lblCriticialJobName+" lblAttentionJobName "+lblAttentionJobName);
		
		if(lblCriticialJobName=="")
		{
			$('#errormosaicinfodiv').show();
			$('#errormosaicinfodiv').append("&#149;"+resourceJSON.msgLblForCrictJob+"<br>");
			if(focusCount	==	0)
			$('#lblCriticialJobName').focus();
			$('#lblCriticialJobName').css("background-color", "#F5E7E1");
			counter++;
			focusCount++;
			mosaicFlag=true;
		}
		if(lblAttentionJobName=="")
		{
			$('#errormosaicinfodiv').show();
			$('#errormosaicinfodiv').append("&#149; "+resourceJSON.msgLblAttentionJobs+"<br>");
			if(focusCount	==	0)
			$('#lblAttentionJobName').focus();
			$('#lblAttentionJobName').css("background-color", "#F5E7E1");
			counter++;
			focusCount++;
			mosaicFlag=true;
		}
	
		if($('#entityTypeOfSession').val()==1)
		{
			$('#exclusivePeriod').css("background-color", "");
			var exclusivePeriod	=	trim(document.getElementById("exclusivePeriod").value);
			//alert(" exclusivePeriod "+exclusivePeriod+" Length "+exclusivePeriod.length);
			if(exclusivePeriod=="" && exclusivePeriod.length==0)
			{
				$('#erroraccountinfodiv').show();
				$('#erroraccountinfodiv').append("&#149;"+resourceJSON.msgExclusivePeriod+"<br>");
				if(focusCount	==	0)
				$('#exclusivePeriod').focus();
				$('#exclusivePeriod').css("background-color", "#F5E7E1");
				counter++;
				focusCount++;
				accInfoFlag=true;
			}
			if(exclusivePeriod==0 && exclusivePeriod.length>0)
			{
				$('#erroraccountinfodiv').show();
				$('#erroraccountinfodiv').append("&#149; "+resourceJSON.msgExclusivePGret0+"<br>");
				if(focusCount	==	0)
				$('#exclusivePeriod').focus();
				$('#exclusivePeriod').css("background-color", "#F5E7E1");
				counter++;
				focusCount++;
				accInfoFlag=true;
			}
		}
		
		var cbxUploadAssessment=document.getElementById("cbxUploadAssessment").checked;
		var assessmentUploadURLVal=document.getElementById("assessmentUploadURLVal").value;
		var assessmentUploadURLFile	=	trim(document.getElementById("assessmentUploadURLFile").value);
		if(assessmentUploadURLVal==0 && cbxUploadAssessment==true && (assessmentUploadURLFile=="" || assessmentUploadURLFile==null))
		{
			$('#errordatediv').show();
			$('#errordatediv').append("&#149; "+resourceJSON.msgPleaseuploadInventory+"<br>");
			if(focusCount	==	0)
			$('#assessmentUploadURLFile').focus();
			$('#assessmentUploadURLFile').css("background-color", "#F5E7E1");
			counter++;
			focusCount++;
			accInfoFlag=true;
		}else if(assessmentUploadURLFile!="" && assessmentUploadURLFile!=null)
		{
			var ext = assessmentUploadURLFile.substr(assessmentUploadURLFile.lastIndexOf('.') + 1).toLowerCase();
			var fileSize = 0;
			if ($.browser.msie==true)
		 	{	
			    fileSize = 0;	   
			}
			else
			{
				if(document.getElementById("assessmentUploadURLFile").files[0]!=undefined)
				{
					fileSize = document.getElementById("assessmentUploadURLFile").files[0].size;
				}
			}
			if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt'))
			{
				$('#errordatediv').show();
				$('#errordatediv').append("&#149; "+resourceJSON.msgSpecificInventoryfile+"<br>");
				if(focusCount	==	0)
				$('#assessmentUploadURLFile').focus();
				$('#assessmentUploadURLFile').css("background-color", "#F5E7E1");
				counter++;
				focusCount++;
				accInfoFlag=true;
			}
			else if(fileSize>=10485760)
			{
				$('#errordatediv').show();
				$('#errordatediv').append("&#149; "+resourceJSON.msgfilesizelessthan+"<br>");
				if(focusCount	==	0)
				$('#assessmentUploadURLFile').focus();
				$('#assessmentUploadURLFile').css("background-color", "#F5E7E1");
				counter++;
				focusCount++;
				accInfoFlag=true;
			}
		}
		
		var dateErrorFlag=0;
		if(contractStartDate!="" && initiatedOnDate!=""){
			var initDate = new Date(initiatedOnDate);              
		    var startDate= new Date(contractStartDate);   
			if(startDate < initDate){
				$('#errordatediv').show();
				$('#errordatediv').append("&#149;"+resourceJSON.msgDateInitiatedlessequalStartDate+"<br>");
				if(focusCount	==	0)
					$('#initiatedOnDate').focus();
				$('#initiatedOnDate').css("background-color", "#F5E7E1");
				counter++;
				focusCount++;
				dateErrorFlag=1;
				accInfoFlag=true;
			} 
		}
		if(initiatedOnDate!="" && contractEndDate!="" && dateErrorFlag==0){
			var endDate = new Date(contractEndDate);              
		    var initDate= new Date(initiatedOnDate);   
			if(initDate > endDate){
				$('#errordatediv').show();
				$('#errordatediv').append("&#149; "+resourceJSON.msgDateInitiatedlessequalEndDate+"<br>");
				if(focusCount	==	0)
					$('#contractStartDate').focus();
				$('#contractStartDate').css("background-color", "#F5E7E1");
				counter++;
				focusCount++;
				dateErrorFlag=1;
				accInfoFlag=true;
			} 
		}
		if(contractStartDate!="" && contractEndDate!="" && dateErrorFlag==0){
			var endDate = new Date(contractEndDate);              
		    var startDate= new Date(contractStartDate);   
			if(startDate > endDate){
				$('#errordatediv').show();
				$('#errordatediv').append("&#149; "+resourceJSON.msgContractStDatelesseqEnddate+"<br>");
				if(focusCount	==	0)
					$('#contractStartDate').focus();
				$('#contractStartDate').css("background-color", "#F5E7E1");
				counter++;
				focusCount++;
				dateErrorFlag=1;
				accInfoFlag=true;
			} 
		}
		if (trim($('#dDescription').find(".jqte_editor").text())!=""){
			var charCount=trim($('#dDescription').find(".jqte_editor").text());
			var count = charCount.length;
			if(count>2500)
			{
				$('#errorlogoddiv').show();
				$('#errorlogoddiv').append("&#149; "+resourceJSON.msgDescexcced2500+"<br>");
				if(focusCount	==	0)
					$('#dDescription').find(".jqte_editor").focus();
					$('#dDescription').find(".jqte_editor").css("background-color", "#F5E7E1");
				counter++;
				focusCount++;
				generalInfoFlag=true;
			}
		}
		if (dmName	==	"")
		{
			$('#errorcontactinfodiv').show();
			$('#errorcontactinfodiv').append("&#149; "+resourceJSON.msgFinalDesMaker+"<br>");
			if(focusCount	==	0)
				$('#dmName').focus();
			$('#dmName').css("background-color", "#F5E7E1");
			counter++;
			focusCount++;
			contactInfoFlag=true;
		}
		if(dmEmailAddress=="")
		{
			$('#errorcontactinfodiv').show();
			$('#errorcontactinfodiv').append("&#149; "+resourceJSON.msgFinalDesMakerEmail+"<br>");
				if(focusCount==0)
					$('#dmEmailAddress').focus();
				
				$('#dmEmailAddress').css("background-color", "#F5E7E1");
				counter++;
				focusCount++;
				contactInfoFlag=true;
		}
		if (dmPassword	==	"")
		{
			$('#errorcontactinfodiv').show();
			$('#errorcontactinfodiv').append("&#149; "+resourceJSON.msgPassword+"<br>");
			if(focusCount	==	0)
				$('#dmPassword').focus();
			$('#dmPassword').css("background-color", "#F5E7E1");
			counter++;
			focusCount++;
			contactInfoFlag=true;
		}
/*		if (acName	==	"")
		{
			$('#errorcontactinfodiv').show();
			$('#errorcontactinfodiv').append("&#149; "+resourceJSON.msgStudentAssessmentCoordinator+"<br>");
			if(focusCount	==	0)
				$('#acName').focus();
			$('#acName').css("background-color", "#F5E7E1");
			counter++;
			focusCount++;
			contactInfoFlag=true;
		}
*/		if (amName	==	"")
		{
			$('#errorcontactinfodiv').show();
			$('#errorcontactinfodiv').append("&#149; "+resourceJSON.msgAccountManagerTM+"<br>");
			if(focusCount	==	0)
				$('#amName').focus();
			$('#amName').css("background-color", "#F5E7E1");
			counter++;
			focusCount++;
			contactInfoFlag=true;
		}
		if(dmEmailAddress!="")
		{
			if(!isEmailAddress(dmEmailAddress))
			{	
				$('#errorcontactinfodiv').append("&#149; "+resourceJSON.msgValidDescMakerEmail+"<br>");
				if(focusCount==0)
					$('#dmEmailAddress').focus();
				
				$('#dmEmailAddress').css("background-color", "#F5E7E1");
				counter++;
				focusCount++;
				contactInfoFlag=true;
				
			}
		}
		if(acEmailAddress!="")
		{
			if(!isEmailAddress(acEmailAddress))
			{	
				$('#errorcontactinfodiv').append("&#149; "+resourceJSON.msgValidEmailStdAssecord+"<br>");
				if(focusCount==0)
					$('#acEmailAddress').focus();
				
				$('#acEmailAddress').css("background-color", "#F5E7E1");
				counter++;
				focusCount++;
				contactInfoFlag=true;
			}
		}
		if(amEmailAddress!="")
		{
			if(!isEmailAddress(amEmailAddress))
			{	
				$('#errorcontactinfodiv').append("&#149; "+resourceJSON.msgValidEmail4AM+"<br>");
				if(focusCount==0)
					$('#amEmailAddress').focus();
				
				$('#amEmailAddress').css("background-color", "#F5E7E1");
				counter++;
				focusCount++;
				contactInfoFlag=true;
			}
		}
		
		var applitrackIntegration=document.getElementById("isApplitrackIntegration").checked;
		$('#errorapplitrackdiv').html("");
		$('#clientCode').css("background-color", "");
		$('#apiUserName').css("background-color", "");
		$('#apiPassword').css("background-color", "");
		if(applitrackIntegration==true){
			var clientCode = document.getElementById("clientCode").value;
			var apiUserName = document.getElementById("apiUserName").value;
			var apiPassword = document.getElementById("apiPassword").value;
			
			if(clientCode=="")
			{	
				$('#errorapplitrackdiv').append("&#149; Please enter Client Code<br>");
				if(focusCount==0)
					$('#clientCode').focus();
				
				$('#clientCode').css("background-color", "#F5E7E1");
				counter++;
				focusCount++;
			}
			if(apiUserName=="")
			{	
				$('#errorapplitrackdiv').append("&#149; Please enter API Username<br>");
				if(focusCount==0)
					$('#apiUserName').focus();
				
				$('#apiUserName').css("background-color", "#F5E7E1");
				counter++;
				focusCount++;
			}
			if(apiPassword=="")
			{	
				$('#errorapplitrackdiv').append("&#149; Please enter API Password<br>");
				if(focusCount==0)
					$('#apiPassword').focus();
				
				$('#apiPassword').css("background-color", "#F5E7E1");
				counter++;
				focusCount++;
			}
			
		}
		
		if(amEmailAddress!="")
		{
			if(!isEmailAddress(amEmailAddress))
			{	
				$('#errorcontactinfodiv').append("&#149; "+resourceJSON.msgValidEmail4AM+"<br>");
				if(focusCount==0)
					$('#amEmailAddress').focus();
				
				$('#amEmailAddress').css("background-color", "#F5E7E1");
				counter++;
				focusCount++;
				contactInfoFlag=true;
			}
		}
		var allowMessageTeacher=document.getElementById("allowMessageTeacher").checked;
		if(allowMessageTeacher==true && emailForTeacher==""){
			$('#erroraccountinfodiv').append("&#149; "+resourceJSON.msgEmailAddToRecvMsg+"<br>");
			if(focusCount==0)
				$('#emailForTeacher').focus();
			
			$('#emailForTeacher').css("background-color", "#F5E7E1");
			counter++;
			focusCount++;
			accInfoFlag=true;
		}else if(emailForTeacher!=""){
			if(!isEmailAddress(emailForTeacher)){	
				$('#erroraccountinfodiv').append("&#149; "+resourceJSON.msgvalidEmailaddresstoreceive+"<br>");
				if(focusCount==0)
					$('#emailForTeacher').focus();
				
				$('#emailForTeacher').css("background-color", "#F5E7E1");
				counter++;
				focusCount++;
				accInfoFlag=true;
			}
		}
		
		var flagForURL	=	document.getElementsByName("flagForURL");
		for(i=0;i<flagForURL.length;i++)
		{
			if(flagForURL[i].checked	==	true)
			{
				var exitURL	=	document.getElementById("exitURL").value;
				//alert(" exitURL"+exitURL);
				if(exitURL=="")
				{
					$('#erroraccountinfodiv').show();
					$('#erroraccountinfodiv').append("&#149; "+resourceJSON.msgEnterURL+" <br>");
					if(focusCount	==	0)
						$('#exitURL').focus();
					$('#exitURL').css("background-color", "#F5E7E1");
					counter++;
					focusCount++;
					accInfoFlag=true;
				}
			}
		}
		var flagForMessage	=	document.getElementsByName("flagForMessage");
		for(i=0;i<flagForMessage.length;i++)
		{
			if(flagForMessage[i].checked	==	true)
			{
				if(trim($('#eMessage').find(".jqte_editor").text())=="")
				{
					$('#erroraccountinfodiv').show();
					$('#erroraccountinfodiv').append("&#149; "+resourceJSON.msgCompletionMessage+"<br>");
					if(focusCount	==	0)
						$('#eMessage').find(".jqte_editor").focus();
					$('#eMessage').find(".jqte_editor").css("background-color", "#F5E7E1");
					counter++;
					focusCount++;
					accInfoFlag=true;
				}else if (trim($('#eMessage').find(".jqte_editor").text())!=""){
					var charCount=trim($('#eMessage').find(".jqte_editor").text());
					var count = charCount.length;
					if(count>1000)
					{
						$('#erroraccountinfodiv').append("&#149; "+resourceJSON.msgCompletionMessagelength+"<br>");
						$('#eMessage').find(".jqte_editor").focus();
						$('#eMessage').find(".jqte_editor").css("background-color", "#F5E7E1");
			
						counter++;
						focusCount++;
						accInfoFlag=true;
					}
				}
			}
		}
		$('#errorpdistictdiv').empty();
		if(saveDistrictPrevilege()==false){
			pDistFlag=true;
			$('#errorpdistictdiv').show();
			$('#errorpdistictdiv').append("&#149; "+resourceJSON.msgatleastoneDpoint+"<br>");
			counter++;
			focusCount++;
		}
		if(generalInfoFlag){
			alert( resourceJSON.msgGISection );
		}else if(contactInfoFlag){
			alert(resourceJSON.msgISection);
		}else if(userFlag){
			alert(resourceJSON.msgAISection);
		}else if(accInfoFlag){
			alert(resourceJSON.msgAISection);
		}else if(mosaicFlag){
			alert(resourceJSON.msgMosaicSection);
		}else if(pDistFlag){
			alert(resourceJSON.msgPrivilegeDistrictsection);
		}
		/* ========== Mosaic Accordian Validation =================*/
		
		try{
			var iframeNorm = document.getElementById('ifrm1');
			var innerNorm = iframeNorm.contentDocument || iframeNorm.contentWindow.document;
			if(innerNorm.getElementById('slider1').value!=''){
				$('#candidateFeedNormScore').val(innerNorm.getElementById('slider1').value);
			}
		}catch(e){} 
		
		
		
			//alert(" value "+$("#mosaicRadiosFlag").val());
			if($("#mosaicRadiosFlag").val()==1)
			{
				try{
					var iframeNorm = document.getElementById('ifrm2');
					var innerNorm = iframeNorm.contentDocument || iframeNorm.contentWindow.document;
					if(innerNorm.getElementById('slider2').value!='')
						$('#candidateFeedDaysOfNoActivity').val(innerNorm.getElementById('slider2').value);
					}catch(e){}
			}
	
		try{
			var iframeNorm = document.getElementById('ifrm3');
			var innerNorm = iframeNorm.contentDocument || iframeNorm.contentWindow.document;
			if(innerNorm.getElementById('slider3').value!='')
				$('#jobFeedCriticalJobActiveDays').val(innerNorm.getElementById('slider3').value);
			}catch(e){} 
		try{
			var iframeNorm = document.getElementById('ifrm4');
			var innerNorm = iframeNorm.contentDocument || iframeNorm.contentWindow.document;
			if(innerNorm.getElementById('slider4').value!='')
				$('#jobFeedCriticalCandidateRatio').val(innerNorm.getElementById('slider4').value);
			}catch(e){} 
		try{
			var iframeNorm = document.getElementById('ifrm5');
			var innerNorm = iframeNorm.contentDocument || iframeNorm.contentWindow.document;
			if(innerNorm.getElementById('slider5').value!='')
				$('#jobFeedCriticalNormScore').val(innerNorm.getElementById('slider5').value);
			}catch(e){} 
			
		try{
			var iframeNorm = document.getElementById('ifrm6');
			var innerNorm = iframeNorm.contentDocument || iframeNorm.contentWindow.document;
			if(innerNorm.getElementById('slider6').value!='')
				$('#jobFeedAttentionJobActiveDays').val(innerNorm.getElementById('slider6').value);
			}catch(e){} 
		try{
			var iframeNorm = document.getElementById('ifrm7');
			var innerNorm = iframeNorm.contentDocument || iframeNorm.contentWindow.document;
			if(innerNorm.getElementById('slider7').value!='')
				$('#jobFeedAttentionNormScore').val(innerNorm.getElementById('slider7').value);
			}catch(e){} 
			
		if(counter	==	0)
		{
			$('#loadingDiv').hide();
			saveEmailFormat();
			saveDistrictDomain();
			
			var distId=$('#distId').val();
			var allStatusId	=	$("#arrStatusId").val();
			var arrStatusId	=	allStatusId.split(",");
			var arrStatusSliderValue="";
			for(var i=0;i<arrStatusId.length-1;i++)
			{
				//alert(i+"---- "+arrStatusId[i]);
				try{
					var iframeNorm = document.getElementById('ifrm_'+arrStatusId[i]);
					var innerNorm = iframeNorm.contentDocument || iframeNorm.contentWindow.document;
						if(innerNorm.getElementById('slider_'+arrStatusId[i]).value!='')
						{
							arrStatusSliderValue+=innerNorm.getElementById('slider_'+arrStatusId[i]).value+",";
						}
					}catch(e){} 
			}
			
			var allSecStatusId	=	$("#arrSecStatusId").val();
			var arrSecStatusId	=	allSecStatusId.split(",");
			var arrSecStatusSliderValue="";
			for(var i=0;i<arrSecStatusId.length-1;i++)
			{
				try{
					var iframeNorm = document.getElementById('ifrm_'+arrSecStatusId[i]);
					var innerNorm = iframeNorm.contentDocument || iframeNorm.contentWindow.document;
						if(innerNorm.getElementById('slider_'+arrSecStatusId[i]).value!='')
						{
							arrSecStatusSliderValue+=innerNorm.getElementById('slider_'+arrSecStatusId[i]).value+",";
							//alert(i+"2---- "+arrSecStatusId[i]+" value= "+innerNorm.getElementById('slider_'+arrSecStatusId[i]).value);
						}
					}catch(e){} 
			}
			
			
			
			
			DistrictAjax.saveBanchmarkStatus(distId,allStatusId,arrStatusSliderValue,allSecStatusId,arrSecStatusSliderValue, { 
				async: false,
				callback: function(data)
				{
					//alert(" data "+data);
				},
				errorHandler:handleError 
			});
			
			var sACreateEvent = $("#sACreateEvent").is(':checked');
			//alert("sACreateEvent:- "+sACreateEvent);
			DistrictAjax.ActivateOrDeactiveManageEventMenu(sACreateEvent,distId,{ 
				async: false,
				callback: function(data)
				{
				},
				errorHandler:handleError 
			});
			
			/*var distId322 = $("#distId").val();
			var noOfActiveGroup = $("#noOfApprovalNeeded").val();
			DistrictAjax.setActiveApprovalGroup(distId322,noOfActiveGroup,{ 
				async: false,
				callback: function(data)
				{
				},
				errorHandler:handleError 
			});*/
			updateDistrictUserList();
			
			
			return true;
		}
		else
		{
			$('#loadingDiv').hide();
			return false;
		}
	}	
	function chkDStatus(param){
		if(param==true){
			document.getElementById("dPointsSceStatus").style.display='inline';
		}else{
			var status=document.getElementsByName("chkDStatusMaster");
			for(var i=0; i<status.length; i++)
			{
				status[i].checked=false;
			}
			document.getElementById("dPointsSceStatus").style.display='none';
		}
	}
	/*======= Gagan : saveDistrictPrevilege For Candidate On Import ==============  */
	function saveDistrictPrevilege()
	{
		var tmdefaultRadios=document.getElementsByName("displayTMDefaultJobCategory");
		var displaycommunication=document.getElementsByName("displayCommunication");
		var dpRadios=document.getElementsByName("setAssociatedStatusToSetDPoints");
		var chkDStatusMaster=document.getElementsByName("chkDStatusMaster");
		var accessDPoints="";
		for(var i=0; i<chkDStatusMaster.length; i++)
		{
			if(chkDStatusMaster[i].checked){
				accessDPoints+="||";
				accessDPoints+=chkDStatusMaster[i].value;
			}
		}
		if(accessDPoints!=""){
			accessDPoints+="||";
		}
		var districtId=$('#distId').val()
		var tmdefaultRadiosStatus;
		var displaycommunicationStatus;
		var dpRadiosStatus;
		
		
		var qqsetId = document.getElementById('qqAvlDistList').value;
		
		if(displaycommunication[1].checked){
			displaycommunicationStatus=displaycommunication[1].value;
		}
		if(displaycommunication[0].checked){
			displaycommunicationStatus=displaycommunication[0].value;
		}
		
		if(tmdefaultRadios[0].checked){
			tmdefaultRadiosStatus=tmdefaultRadios[0].value;
		}
		if(tmdefaultRadios[1].checked){
			tmdefaultRadiosStatus=tmdefaultRadios[1].value;
		}
		if(dpRadios[0].checked){
			dpRadiosStatus=dpRadios[0].value;
		}
		if(dpRadios[1].checked){
			dpRadiosStatus=dpRadios[1].value;
		}
		if(dpRadiosStatus==1 && accessDPoints==""){
			return false;
		}else{
				DistrictAjax.saveDistrictPrevilege(districtId,tmdefaultRadiosStatus,displaycommunicationStatus,dpRadiosStatus,accessDPoints,qqsetId,{ 
				async: true,
				callback: function(data)
				{
					//return true;
				},
				errorHandler:handleError 
			});
				    
		}

	}
	
	function setEmailFormat()
	{
		$('#fromAddress').focus();
		//document.getElementById("fromAddress").focus();
		var distId=$('#distId').val();
		$('#fromAddress').focus();
		$('#errordivEmail').empty();
		$('#errordivEmail').hide();
		$('#fromAddress').css("background-color", "");
		$('#subjectLine').css("background-color", "");
		$('#mailBody').find(".jqte_editor").css("background-color", "");
		//alert(" distId "+distId);
		DistrictAjax.getEmailFormat(distId, { 
			async: true,
			callback: function(data)
			{
				//alert(" data "+data+"----"+data.fromAddress+"----"+data.subjectLine+"-----"+data.mailBody);
				if(data!=null)
				{
					$('#fromAddress').focus();
					 document.getElementById("fromAddress").focus();  
					$('#fromAddress').val(data.fromAddress);
					$('#subjectLine').val(data.subjectLine);
					$('#mailBody').find(".jqte_editor").html(data.mailBody);
				}
			},
			errorHandler:handleError 
		});
		$('#myModalEmail').modal('show');

	}
	
	function saveEmailFormat()
	{
		var distId=$('#distId').val();
		var msgFrom=$('#fromAddress').val();
		var msgSubject=$('#subjectLine').val();
		var charCount	=	$('#mailBody').find(".jqte_editor").text();
				
		$('#errordivEmail').empty();
		$('#fromAddress').css("background-color", "");
		$('#subjectLine').css("background-color", "");
		$('#mailBody').find(".jqte_editor").css("background-color", "");
		
		var cnt=0;
		var focs=0;
		
		if(trim(msgFrom)=="")
		{
			$('#errordivEmail').append("&#149; "+resourceJSON.msgEnterFrom+"<br>");
			if(focs==0)
				$('#fromAddress').focus();
			$('#fromAddress').css("background-color", "#F5E7E1");
			cnt++;focs++;
		}
		if(trim(msgSubject)=="")
		{
			$('#errordivEmail').append("&#149; "+resourceJSON.PlzEtrSub+"<br>");
			if(focs==0)
				$('#subjectLine').focus();
			$('#subjectLine').css("background-color", "#F5E7E1");
			cnt++;focs++;
		}
		if(charCount.length==0)
		{
			$('#errordivEmail').append("&#149; "+resourceJSON.PlzEtrMsg+"<br>");
			if(focs==0)
				$('#mailBody').find(".jqte_editor").focus();
			$('#mailBody').find(".jqte_editor").css("background-color", "#F5E7E1");
			cnt++;focs++;
		}
		
		//alert(" cnt "+cnt);
		if(cnt==0)
		{
			DistrictAjax.saveEmailFormat(distId,msgFrom,msgSubject,$('#mailBody').find(".jqte_editor").html(), { 
				async: true,
				callback: function(data)
				{
				},
				errorHandler:handleError 
			});
			$('#myModalEmail').modal('hide');
			return false;
		
		}
		else
		{
			$('#errordivEmail').show();
			return false;
		}
	}
	
	function uncheckedMessageRadio()
	{
		$('#erroraccountinfodiv').empty();
		$('#exitURL').css("background-color", "");
		$('#exitMessage').css("background-color", "");
		//alert("Hi")
		var flagForMessage	=	document.getElementsByName("flagForMessage");
		for(i=0;i<flagForMessage.length;i++)
		{
			if(flagForMessage[i].checked	==	true)
			{
				flagForMessage[i].checked	=	false;
				break;
			}
		}
	}
	function uncheckedUrlRadio()
	{
		$('#erroraccountinfodiv').empty();
		$('#exitURL').css("background-color", "");
		$('#exitMessage').css("background-color", "");
		var flagForURL	=	document.getElementsByName("flagForURL");
		for(i=0;i<flagForURL.length;i++)
		{
			if(flagForURL[i].checked	==	true)
			{
				flagForURL[i].checked	=	false;
				break;
			}
		}
	}
	
	/*============ Add DistrictSchool Functionality ===========================*/
	function addSchool()
	{
		dwr.util.setValues({districtORSchoolName:null,districtOrSchooHiddenlId:null});
		$("#addSchoolDiv").fadeIn();
		$("#districtORSchoolName").focus();
	}
	
	function displayDistrictSchools()
	{
		dwr.util.setValues({districtORSchoolName:null,districtOrSchooHiddenlId:null});
		var districtId	=	trim(document.getElementById("districtId").value);
		DistrictAjax.displayDistrictSchoolsGrid(districtId,noOfRows,page,sortOrderStr,sortOrderType,{ 
			async: true,
			callback: function(data)
			{
			$('#districtSchoolsGrid').html(data);
				applyScrollOnTbl();
				$('#addSchoolDiv').hide();
			},
			errorHandler:handleError 
		});
	}
	
	function saveDistrictSchools()
	{
		var schoolId							=	trim(document.getElementById("districtOrSchooHiddenlId").value);
		var districtSchoolName					=	trim(document.getElementById("districtORSchoolName").value);
		var districtId							=	trim(document.getElementById("districtId").value);
		
		if(districtSchoolName=="")
		{
				$('#errordistrictschoolediv').show();	
				$('#errordistrictschoolediv').empty();
				$('#errordistrictschoolediv').append("&#149; "+resourceJSON.msgPleaseenterSchoolName+"<br>");
				$('#districtORSchoolName').css("background-color", "#F5E7E1");
				$("#districtORSchoolName").focus();
				return false;
		}
		if(schoolId=="")
		{
				$('#errordistrictschoolediv').show();	
				$('#errordistrictschoolediv').empty();
				$('#errordistrictschoolediv').append("&#149; "+resourceJSON.msgentervalidSchoolName+"<br>");
				$('#districtORSchoolName').css("background-color", "#F5E7E1");
				$("#districtORSchoolName").focus();
				return false;
		}
		//$('#errornotediv').empty();
		
		DistrictAjax.saveDistrictSchools(districtId,schoolId, { 
			async: true,
			callback: function(data)
			{
				//alert(" data "+data);
				if(data	==	3)
				{
					$('#errordistrictschoolediv').empty();
					$('#errordistrictschoolediv').append("&#149; "+resourceJSON.msgSchoolAlreadyEntered+"<br>");
						$('#districtORSchoolName').focus();
					$('#errordistrictschoolediv').show();
					$('#districtORSchoolName').css("background-color", "#F5E7E1");
					return false;
				}
				else
				{
					if(data	==	2)
					{
						alert(" Server Error ");
					}
					else
					{
						displayDistrictSchools();
					}
				}
			},
			errorHandler:handleError 
		});
	}

	function deleteDistrictSchools(districtSchoolId)
	{
		$('#districtORSchoolName').css("background-color", "");
		$('#errordistrictschoolediv').empty();
		if (confirm(resourceJSON.msgLiekToDeleteSchool)) {
			DistrictAjax.deleteDistrictSchools(districtSchoolId, { 
				async: true,
				callback: function(data)
				{
					displayDistrictSchools();
				},
				errorHandler:handleError 
			});
		}
	}

	function clearSchool()
	{
		dwr.util.setValues({districtORSchoolName:null,districtOrSchooHiddenlId:null});
		$("#addSchoolDiv").hide();
		$('#districtORSchoolName').val("");
		$('#errordistrictschoolediv').empty();
	}
	/*============ Radio buttons ==========================*/
	function uncheckedOtherRadio(val)
	{  
		var rd1					=	document.getElementsByName("noSchoolUnderContract");
		var rd2					=	document.getElementsByName("allSchoolsUnderContract");
		var rd3					=	document.getElementsByName("allGradeUnderContract");
		var rd4					=	document.getElementsByName("selectedSchoolsUnderContract");
		var pk					=	document.getElementsByName("pkOffered");
		var kg					=	document.getElementsByName("kgOffered");
		var g01					=	document.getElementsByName("g01Offered");
		var g02					=	document.getElementsByName("g02Offered");
		var g03					=	document.getElementsByName("g03Offered");
		var g04					=	document.getElementsByName("g04Offered");
		var g05					=	document.getElementsByName("g05Offered");
		var g06					=	document.getElementsByName("g06Offered");
		var g07					=	document.getElementsByName("g07Offered");
		var g08					=	document.getElementsByName("g08Offered");
		var g09					=	document.getElementsByName("g09Offered");
		var g10					=	document.getElementsByName("g10Offered");
		var g11					=	document.getElementsByName("g11Offered");
		var g12					=	document.getElementsByName("g12Offered");
		
		if(val	==	1)
		{
			for(i=0;i<rd2.length;i++)
			{
				rd2[i].checked	=	false;
			}
			for(i=0;i<rd3.length;i++)
			{
				rd3[i].checked	=	false;
			}
			for(i=0;i<rd4.length;i++)
			{
				rd4[i].checked	=	false;
			}
			$("#gradesDiv").hide();
			$("#addSchoolLink").hide();
			$("#districtSchoolsGrid").hide();
			//$("#districtSchoolsTable").hide();
			$("#addSchoolDiv").hide();
		}
		if(val	==	2)
		{
			for(i=0;i<rd1.length;i++)
			{
				rd1[i].checked	=	false;
			}
			for(i=0;i<rd3.length;i++)
			{
				rd3[i].checked	=	false;
			}
			for(i=0;i<rd4.length;i++)
			{
				rd4[i].checked	=	false;
			}
			$("#gradesDiv").hide();
			$("#addSchoolLink").hide();
			$("#districtSchoolsGrid").hide();
			//$("#districtSchoolsTable").hide();
			$("#addSchoolDiv").hide();
		}
		if(val	==	3)
		{
			for(i=0;i<rd1.length;i++)
			{
				rd1[i].checked	=	false;
			}
			for(i=0;i<rd2.length;i++)
			{
				rd2[i].checked	=	false;
			}
			for(i=0;i<rd4.length;i++)
			{
				rd4[i].checked	=	false;
			}
			$("#gradesDiv").fadeIn();
			$("#addSchoolLink").hide();
			$("#districtSchoolsGrid").hide();
			//$("#districtSchoolsTable").hide();
			$("#addSchoolDiv").hide();
		}
		if(val	==	4)
		{
			for(i=0;i<rd1.length;i++)
			{
				rd1[i].checked	=	false;
			}
			for(i=0;i<rd2.length;i++)
			{
				rd2[i].checked	=	false;
			}
			for(i=0;i<rd3.length;i++)
			{
				rd3[i].checked	=	false;
			}
			$("#gradesDiv").hide();
			//document.getElementById("addSchoolLink").style.display="block";
			$("#addSchoolLink").fadeIn();
			$("#districtSchoolsGrid").fadeIn();
			//$("#districtSchoolsTable").fadeIn();
			//$("#addSchoolDiv").hide();
		}
		
	}
	
/*============ Make Decision Maker Feild editable==================================*/
	function editDmFields()
	{
		$('#errorpwddiv').empty();
		var cnfmPassword					=	document.getElementById("cnfmPassword").value;
		var districtId						=	trim(document.getElementById("districtId").value);
		var entityTypeOfSession				=	trim(document.getElementById("entityTypeOfSession").value);
		if(entityTypeOfSession==1)
		{
			$('#errorpwddiv').empty();
			if(cnfmPassword=='Te@cherMatch2006!')
			{
				document.getElementById("dmName").readOnly 				= 	false;
				document.getElementById("dmEmailAddress").readOnly 		= 	false;
				document.getElementById("dmPhoneNumber").readOnly 		= 	false;
				document.getElementById("btnDm").style.display	 		= 	"none";
				document.getElementById("dmPassword").value				=	document.getElementById("cnfmPassword").value;
				document.getElementById("cnfmPassword").readOnly 		= 	true;
				//alert("dmPassword "+document.getElementById("dmPassword").value);
			}
			else
			{
				//$('#errorpwddiv').append("&#149; Invalid Password <br>");
				DistrictAjax.matchPwdOfDm(districtId,cnfmPassword,{ 
					async: true,
					callback: function(data)
					{
						$('#errorpwddiv').empty();
						if(data!=null)
						{
							document.getElementById("dmName").readOnly 				= 	false;
							document.getElementById("dmEmailAddress").readOnly 		= 	false;
							document.getElementById("dmPhoneNumber").readOnly 		= 	false;
							document.getElementById("btnDm").style.display	 		= 	"none";
							//document.getElementById("dmPassword").value				=	document.getElementById("cnfmPassword").value;
							document.getElementById("cnfmPassword").readOnly 		= 	true;
							//alert("dmPassword "+document.getElementById("dmPassword").value);
						}
						else
						{
							$('#errorpwddiv').append("&#149; "+resourceJSON.msgInvalidPassword+" <br>");
						}
					},
					errorHandler:handleError 
					});
			}
		}
		else
		{
			DistrictAjax.matchPwdOfDm(districtId,cnfmPassword,{ 
				async: true,
				callback: function(data)
				{
					$('#errorpwddiv').empty();
					if(data!=null)
					{
						document.getElementById("dmName").readOnly 				= 	false;
						document.getElementById("dmEmailAddress").readOnly 		= 	false;
						document.getElementById("dmPhoneNumber").readOnly 		= 	false;
						document.getElementById("btnDm").style.display	 		= 	"none";
						//document.getElementById("dmPassword").value				=	document.getElementById("cnfmPassword").value;
						document.getElementById("cnfmPassword").readOnly 		= 	true;
						//alert("dmPassword "+document.getElementById("dmPassword").value);
					}
					else
					{
						$('#errorpwddiv').append("&#149; "+resourceJSON.msgInvalidPassword+" <br>");
					}
				},
				errorHandler:handleError 
				});
		}
	}

	/*******************************************************************************/
	function getDistrictDomains(){
		domainPageVar="domainPage";
		var districtId	=	trim(document.getElementById("districtId").value);
		DistrictDomainAjax.getDistrictDomains(districtId,domainnoOfRows,domainpage,domainsortOrderStr,domainsortOrderType,{
			async: false,callback: function(data){
				$("#domainGrid").html(data);
				$("#domainDiv").hide();
				applyScrollOnTblDomain();
				getQQInManageDistrict();
				getQQForOnboardingInManageDistrict();
			},
		errorHandler:handleError
		});
	}
	function saveDistrictDomain(){
		var districtId	=	trim(document.getElementById("districtId").value);
		var domainName=	trim(document.getElementById("domainName").value);
		var districtDomainId=	trim(document.getElementById("districtDomainId").value);
		var counter				=	0;
		var focuscount			=	0;
		$('#domainErrorDiv').empty();
		$('#domainName').css("background-color", "");
	
		if(domainName.indexOf('@')>=0){
			$('#domainErrorDiv').show();
			$('#domainErrorDiv').append("&#149; "+resourceJSON.msgEnterAtSymbol+"<br>");
			if(focuscount	==	0)
				$('#domainName').focus();
				$('#domainName').css("background-color", "#F5E7E1");
			counter++;
			focuscount++;
		}
		
		if (domainName	==	"")
		{	
			$('#domainErrorDiv').show();
			$('#domainErrorDiv').append("&#149; "+resourceJSON.msgEnterDomainName+"<br>");
			if(focuscount	==	0)
				$('#domainName').focus();
				$('#domainName').css("background-color", "#F5E7E1");
			counter++;
			focuscount++;
		}
		if(counter==0){
		{	
			DistrictDomainAjax.saveDistrictDomain(districtDomainId,districtId,domainName,{
				async: false,callback: function(data){
				if(data==1){
					$('#domainErrorDiv').show();
					$('#domainErrorDiv').append("&#149;"+resourceJSON.msgDuplicateDomain+"<br>");
					$('#domainName').focus();
					$('#domainName').css("background-color", "#F5E7E1");
				}else{
					$("#domainDiv").hide();
					getDistrictDomains();
				}
				},
			errorHandler:handleError
			});
		}
	  }
	}
	
	function addDistrictDomain(){
		$('#domainErrorDiv').empty();
		$("#domainDiv").show();
		$("#domainName").val("");
		$("#districtDomainId").val("");
		$("#domainName").focus();
		$('#domainName').css("background-color", "");
	}
	function clearDomain(){
		$("#domainDiv").hide();
	}
	function editDomain(districtDomainId){
		DistrictDomainAjax.editDistrictDomain(districtDomainId,{
			async: false,callback: function(data){
				$('#domainErrorDiv').empty();
				$("#domainDiv").show();
				$("#districtDomainId").val(data.districtDomainId);
				$("#domainName").focus();
				$("#domainName").val(data.domainName);
			},
		errorHandler:handleError
		});
	}
	
	function chkForEnterSaveDistrictDomain(evt)
	{
		var charCode = ( evt.which ) ? evt.which : event.keyCode;
		if(charCode==13)
		{
			saveDistrictDomain();
		}	
	}
	
	function deleteDomain(districtDomainId){
		DistrictDomainAjax.deleteDistrictDomain(districtDomainId,{
			async: false,callback: function(data){
			$("#domainDiv").hide();
			getDistrictDomains();
			},
		errorHandler:handleError
		});
	}
/*======================= Auto Complete Functionality =============================*/
	var count=0;
	var index=-1;
	var length=0;
	var divid='';
	var txtid='';
	var hiddenDataArray = new Array();
	var showDataArray = new Array();
	var degreeTypeArray = new Array();
	var hiddenId="";
	function getDistrictMasterAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
	{
		hiddenId=hiddenId;
		divid=txtdivid;
		txtid=txtSearch.id;
		if(event.keyCode==40){
			downArrowKey(txtdivid);
		} 
		else if(event.keyCode==38) //up key
		{
			upArrowKey(txtdivid);
		} 
		else if(event.keyCode==13) // RETURN
		{
			if(document.getElementById(divid))
				document.getElementById(divid).style.display='block';
			
			document.getElementById("districtORSchoolName").focus();
			
		} 
		else if(event.keyCode==9) // Tab
		{
			
		}
		else if(txtSearch.value!='')
		{
			index=-1;
			length=0;
			document.getElementById(divid).style.display='block';
			//searchArray = getDistrictMasterArray(txtSearch.value);
			searchArray = getSchoolMasterArray(txtSearch.value);
			fatchData(txtSearch,searchArray,txtId,txtdivid);
			
		}
		else if(txtSearch.value==""){
			document.getElementById(divid).style.display='none';
		}
	}

	function getSchoolMasterArray(districtOrSchoolName){
		var searchArray = new Array();
		var districtId=	document.getElementById("districtId").value;
	/*	if(entityID==2){
			UserAjax.getFieldOfDistrictList(districtOrSchoolName,{ 
				async: false,
				callback: function(data){
				hiddenDataArray = new Array();
				showDataArray = new Array();
				for(i=0;i<data.length;i++){
					searchArray[i]=data[i].districtName;
					showDataArray[i]=data[i].districtName;
					hiddenDataArray[i]=data[i].districtId;
				}
			}
			});	
		}else {*/
		DistrictAjax.getFieldOfSchoolList(districtId,districtOrSchoolName,{ async: false,callback: function(data)
			{
				hiddenDataArray = new Array();
				showDataArray = new Array();
				for(i=0;i<data.length;i++)
				{
					searchArray[i]=data[i].schoolName;
					showDataArray[i]=data[i].schoolName;
					hiddenDataArray[i]=data[i].schoolId;
				}
			},
			errorHandler:handleError
			});	
		//}
		
		

		return searchArray;
	}


	var selectFirst="";

	var fatchData= function(txtSearch,searchArray,txtId,txtdivid){
		var result = document.getElementById(txtdivid);
		try{
			result.style.display='block';
			result.innerHTML = '';
			var items='';
			count=0;
			var len=searchArray.length;
			if(document.getElementById(txtId).value!="")
			{
				
				for(var i=0;i<len;i++){
						items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
						searchArray[i].toUpperCase() + "</div>";
						count++;
						length++;
						
					if(count==10)
						break;
					
				}
				
			}
			else {
				
			}
			if(count!=0)
				result.innerHTML = items;
			else{
				result.style.display='none';
				selectFirst="";
			}
			if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
			{
				document.getElementById('divResult'+txtdivid+0).className='over';
				selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
			}
			
			scrolButtom();
		}catch (err){}
	}
	function hideDistrictMasterDiv(dis,hiddenId,divId)
	{
		var entityID=3;
		document.getElementById("districtOrSchooHiddenlId").value="";
		$('#errordiv').empty();
		if(parseInt(length)>0){
			if(index==-1){
				index=0;
			}
			if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
				document.getElementById(hiddenId).value=hiddenDataArray[index];
			}
		
			if(dis.value==""){
					document.getElementById(hiddenId).value="";
			}
			else if(showDataArray && showDataArray[index]){
				dis.value=showDataArray[index];
				document.getElementById("districtOrSchooHiddenlId").value=hiddenDataArray[index];
			}
			
		}else{
			if(document.getElementById(hiddenId))
			{
				document.getElementById(hiddenId).value="";
			}
			if(dis.value!=""){
				var focuschk=0;
				districtORSchoolNameCount=2;
				$('#errordiv').empty();	
				$('#errordiv').show();
				/*if(document.getElementById("AEU_EntityType").value==2){
					$('#errordiv').append("&#149; Please enter valid District<br>");
				}else if(document.getElementById("AEU_EntityType").value==3){
					$('#errordiv').append("&#149; Please enter valid School<br>");
				}*/
				if(entityID==2){
					$('#errordiv').append("&#149; "+resourceJSON.msgvaliddistrict+"<br>");
				}else if(entityID==3){
					$('#errordiv').append("&#149; "+resourceJSON.msgvalidschool+"<br>");
				}
				if(focus==0)
				$('#districtORSchoolName').focus();
				
				focus++;
			}
		}
		
		if(document.getElementById(divId))
		{
			document.getElementById(divId).style.display="none";
		}
		index = -1;
		length = 0;
	}

	var downArrowKey = function(txtdivid){
		if(txtdivid)
		{
			if(index<length-1){
				for(var i=0;i<10;i++){
					{
						if(document.getElementById('divResult'+txtdivid+i))
							var div_id=document.getElementById('divResult'+txtdivid+i);
						if(div_id)
						{
							if(div_id.className=='over')
								index=div_id.id.split('divResult'+txtdivid)[1];
							div_id.className='normal';
						}
					}
				}
				index++;
				if(document.getElementById('divResult'+txtdivid+index))
				{
					var div_id=document.getElementById('divResult'+txtdivid+index);
					div_id.className='over';
					document.getElementById(txtid).value = div_id.innerHTML;
					selectFirst=div_id.innerHTML;
				}
			}
		}
	}

	var upArrowKey = function(txtdivid){
		if(txtdivid)
		{
			if(index>0){
				for(var i=0;i<length;i++){

					var div_id=document.getElementById('divResult'+txtdivid+i);
					if(div_id)
					{
						if(div_id.className=='over')
							index=div_id.id.split('divResult'+txtdivid)[1];
						div_id.className='normal';
					}
				}
				index--;
				if(document.getElementById('divResult'+txtdivid+index))
					document.getElementById('divResult'+txtdivid+index).className='over';
				if(txtid && document.getElementById('divResult'+txtdivid+index))
				{
					document.getElementById(txtid).value=
						document.getElementById('divResult'+txtdivid+index).innerHTML;
					selectFirst=document.getElementById('divResult'+txtdivid+index).innerHTML;
				}
			}
		}
	}

	var overText=function (div_value,txtdivid) {

		for(var i=0;i<length;i++)
		{
			if(document.getElementById('divResult'+i))
			{
				var div_id=document.getElementById('divResult'+i);

				if(div_id.className=='over')
					index=div_id.id.split(txtdivid)[1];
				div_id.className='normal';
			}
		}
		div_value.className = 'over';
		document.getElementById(txtid).value= div_value.innerHTML;
	}
/************************************* Question Set auto complete**********************************************/
	function getQuestionSetAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
	{
		hiddenId=hiddenId;
		divid=txtdivid;
		txtid=txtSearch.id;
		if(event.keyCode==40){
			downArrowKey(txtdivid);
		} 
		else if(event.keyCode==38) //up key
		{
			upArrowKey(txtdivid);
		} 
		else if(event.keyCode==13) // RETURN
		{
			if(document.getElementById(divid))
				document.getElementById(divid).style.display='block';
			
			document.getElementById("quesName").focus();
			
		} 
		else if(event.keyCode==9) // Tab
		{
			
		}
		else if(txtSearch.value!='')
		{
			index=-1;
			length=0;
			document.getElementById(divid).style.display='block';
			//searchArray = getDistrictMasterArray(txtSearch.value);
			searchArray = getQuesSetArray(txtSearch.value);
			fatchData(txtSearch,searchArray,txtId,txtdivid);
			
		}
		else if(txtSearch.value==""){
			document.getElementById(divid).style.display='none';
		}
	}
	
	
	function getQuesSetArray(districtOrSchoolName)
	{
		var searchArray = new Array();
		
		var districtId = trim(document.getElementById("districtId").value);
		
		if(districtId!="")
		{
			I4QuestionSetAjax.getFieldOfQuesList(districtOrSchoolName,districtId,{ async: false,callback: function(data)
				{
					hiddenDataArray = new Array();
					showDataArray = new Array();
					for(i=0;i<data.length;i++)
					{
						searchArray[i]=data[i].questionSetText;
						showDataArray[i]=data[i].questionSetText;
						hiddenDataArray[i]=data[i].ID;
					}
				},
			});
		}
	
		return searchArray;
	}
	
	
	
	
/*************************************************************************************************/
	
	
/************************************* District Assessment auto complete**********************************************/
	function getDistrictAssessmentAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
	{
		hiddenId=hiddenId;
		divid=txtdivid;
		txtid=txtSearch.id;
		if(event.keyCode==40){
			downArrowKey(txtdivid);
		} 
		else if(event.keyCode==38) //up key
		{
			upArrowKey(txtdivid);
		} 
		else if(event.keyCode==13) // RETURN
		{
			if(document.getElementById(divid))
				document.getElementById(divid).style.display='block';
			
			document.getElementById("assessmentName").focus();
			
		} 
		else if(event.keyCode==9) // Tab
		{
			
		}
		else if(txtSearch.value!='')
		{
			index=-1;
			length=0;
			document.getElementById(divid).style.display='block';
			//searchArray = getDistrictMasterArray(txtSearch.value);
			searchArray = getDistrictAssessmentArray(txtSearch.value);
			fatchData(txtSearch,searchArray,txtId,txtdivid);
			
		}
		else if(txtSearch.value==""){
			document.getElementById(divid).style.display='none';
		}
	}
	
	
	function getDistrictAssessmentArray(distASMTName)
	{
		var searchArray = new Array();
		
		var districtId = trim(document.getElementById("districtId").value);
		if(districtId!="")
		{
			DistrictAssessmentAjax.getFieldOfDistASMT(distASMTName,districtId,{ async: false,callback: function(data)
				{
					hiddenDataArray = new Array();
					showDataArray = new Array();
					for(i=0;i<data.length;i++)
					{
						searchArray[i]=data[i].districtAssessmentName;
						showDataArray[i]=data[i].districtAssessmentName;
						hiddenDataArray[i]=data[i].districtAssessmentId;
					}
				},
			});
		}
	
		return searchArray;
	}
	
	
	
	
/*************************************************************************************************/
	
	
	function trim(s)
	{
		while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
			s = s.substring(1,s.length);
		}
		while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
			s = s.substring(0,s.length-1);
		}
		return s;
	}

	
function mouseOverChk(txtdivid,txtboxId)
{	
	for(var i=0;i<length;i++)
	{
		$('#divResult'+txtdivid+i).mouseover({param1: i,param2:txtdivid, param3:txtboxId}, fireMouseOverEvent);
	}
}


function fireMouseOverEvent(event)
{
	for(var i=0;i<length;i++)
	{	
		document.getElementById('divResult'+event.data.param2+i).className='normal';
	}
    document.getElementById('divResult'+event.data.param2+event.data.param1).className='over';	       
   	document.getElementById(event.data.param3).value= $('#divResult'+event.data.param2+event.data.param1).text();
    index=event.data.param1;
}


function loadStatusMaster()
{
	var status=document.getElementsByName("chkStatusMaster");
	var secondaryStatus=document.getElementsByName("chkSecondaryStatusName");
	
	var statusPrivilegeForSchools=document.getElementById('statusPrivilegeForSchools');
	
	if(!statusPrivilegeForSchools.checked)
	{
		for(var i=0; i<status.length; i++)
		{
			status[i].disabled=true;
			status[i].checked=true;
		}
		
		/*for(var j=0; j<status.length; j++)
		{
			//secondaryStatus[j].disabled=true;
			//secondaryStatus[j].checked=true;
		}*/
		
		for(var j=0; j<secondaryStatus.length; j++)
		{
			secondaryStatus[j].disabled=true;
			secondaryStatus[j].checked=true;
		}
		
	}

}

function disableStatusMaster()
{
	var status=document.getElementsByName("chkStatusMaster");
	var secondaryStatus=document.getElementsByName("chkSecondaryStatusName");
	
	var statusPrivilegeForSchools=document.getElementById('statusPrivilegeForSchools');
	
	if(!statusPrivilegeForSchools.checked)
	{
		for(var i=0; i<status.length; i++)
		{
			status[i].disabled=true;
			status[i].checked=true;
		}
		
		for(var j=0; j<secondaryStatus.length; j++)
		{
			secondaryStatus[j].disabled=true;
			secondaryStatus[j].checked=true;
		}
		
	}
	else
	{
		for(var i=0; i<status.length; i++)
		{
			status[i].disabled=false;
			status[i].checked=true;
		}
		
		for(var j=0; j<secondaryStatus.length; j++)
		{
			secondaryStatus[j].disabled=false;
			secondaryStatus[j].checked=true;
		}
	}
}
/* @Author: Gagan 
 * @Discription: editdistrict js .
 */	

function disableStatusMasterEmail(){
	
	var status=document.getElementsByName("chkStatusMasterEml");
	var secondaryStatus=document.getElementsByName("chkSecondaryStatusNameEml");
	
	var statusPrivilegeForSchools=document.getElementById('autoEmailRequired');
	
	if(!statusPrivilegeForSchools.checked)
	{
		for(var i=0; i<status.length; i++)
		{
			status[i].disabled=true;
			status[i].checked=true;
		}
		
		for(var j=0; j<secondaryStatus.length; j++)
		{
			secondaryStatus[j].disabled=true;
			secondaryStatus[j].checked=true;
		}
		
	}
	else
	{
		for(var i=0; i<status.length; i++)
		{
			status[i].disabled=false;
			status[i].checked=true;
		}
		
		for(var j=0; j<secondaryStatus.length; j++)
		{
			secondaryStatus[j].disabled=false;
			secondaryStatus[j].checked=true;
		}
	}
}

function jobApprovalFunc(){
  //approvalBeforeGoLive1
	//var gridNo	=	document.getElementById("approvalBeforeGoLive1").value;
	if (document.getElementById("approvalBeforeGoLive1").checked) {
		document.getElementById("noOfApprovalNeeded").disabled=false;
    } else {
    	document.getElementById("noOfApprovalNeeded").disabled=true;
    }
	buildApprovalGroup322();
}

function showOfferAMT()
{
	var offerVVI 		= 	document.getElementById("offerAssessmentInviteOnly").checked;
	
	if(offerVVI==true)
		$("#offerAMTInvite").show();
	else
		$("#offerAMTInvite").hide();
}


/* ========= Start :: Virtual Video Interview ============*/
function showOfferVVI()
{
	var offerVVI 		= 	document.getElementById("offerVirtualVideoInterview").checked;
	
	if(offerVVI==true)
		$("#offerVVIDiv").show();
	else
		$("#offerVVIDiv").hide();
}

function showMarksDiv()
{
	var wantScore 		= 	document.getElementById("wantScore").checked;
	var maxScoreForVVI 	= 	document.getElementById("maxScoreForVVI").value;
		
	if(wantScore==true)
		$("#marksDiv").show();
	else
		$("#marksDiv").hide();
	
	if(maxScoreForVVI!="")
	{
		document.getElementById("wantScore").checked=true;
		$("#marksDiv").show();
	}
	
}

function showLinkDiv()
{
	var sendAutoVVILink 	= 	document.getElementById("sendAutoVVILink").checked;
	
	if(sendAutoVVILink==true)
		$("#autolinkDiv").show();
	else
		$("#autolinkDiv").hide();
	
}
function checkForInt(evt) {

	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==8)
		return true;

	return( (charCode >= 48 && charCode <= 57)||(charCode==13) );
}

function chkTimeAndDayForVVI()
{
	var t = $("#timeAllowedPerQuestion").val();
	var d = $("#VVIExpiresInDays").val();
	
	if(t==0)
	{
		$("#timeAllowedPerQuestion").val("");
	}
		
	
	if(d==0)
	{
		$("#VVIExpiresInDays").val("");
	}
		

}

/* ========= End :: Virtual Video Interview ============*/

/* Show Reminder Div */
function showReminderDiv(){
	$('#erroroReminderSet').empty();
	var reminderCheck	= 	document.getElementById("sendReminderToIcompCandiates").checked;
	if(reminderCheck==true)
		$("#sendFrequencyReminder").show();
	else
		$("#sendFrequencyReminder").hide();
}



/*******************************************/
function chkDistrictAdmins(val)
{
	if(val==0)
	{
		$('#selecteddisAdm').checked=false;
		$('#alldisAdm').checked=true;
		$('#multiDisAdminDiv').hide();
		
	}
	else if(val==1)
	{
		$('#alldisAdm').checked=false;
		$('#selecteddisAdm').checked=true;
		$('#multiDisAdminDiv').show();
	}
}

function displayAllDistrictAssessment()
{
	var districtId=document.getElementById("districtId").value;
	if(districtId==null || districtId=="")
		districtId=0;
//	var jobCategoryId=document.getElementById("txtjobCategory").value;
//	var secondaryStatusId=document.getElementById("myFolderId").value;
	
	DistrictAjax.displayAllDistrictAssessment(districtId,
	{ 
		async: true,
		errorHandler:handleError,
		callback: function(data)
		{
			var dataArr=data.split('||');
			document.getElementById("1stSchoolDiv").innerHTML=dataArr[0];
			document.getElementById("2ndSchoolDiv").innerHTML=dataArr[1];
		//	if(dataArr[2]=='true')
		//	{
				$('#multiDisAdminDiv').show();
		//	}
			/*else if(dataArr[2]=='false')
			{
				$('#multiDisAdminDiv').hide();
			}*/
		}
	});
}
/*******************************************/

function buildApprovalGroup()
{
	//var isChecked = $("#buildApprovalGroups").attr("checked");
	var isChecked = $("#approvalBeforeGoLive1").attr("checked");
		
	if(isChecked!=null)
	{
		$("#addGroupHyperLink").show();
				
		var districtId=document.getElementById("districtId").value;
		$("#loadingDiv").show();
		DistrictAjax.displayApprovalGroupsAndMembers(districtId,
		{
			async: true,
			errorHandler:handleError,
			callback: function(data)
			{
				$("#groupMembersList").empty();
				$("#groupMembersList").html(data);
				$("#groupMembersList").show();
				$("[data-toggle='tooltip']").tooltip();
				$("#loadingDiv").hide();
				//var ch = $("#addApprovalGroupAndMemberDiv").is(':visible');
				//alert("ch:- "+ch);
				
			}
		});
	}
	else
	{
		$(".hideGroup").hide();
		$("#addApprovalGroupAndMemberDiv").hide();
	}
}

function removeMemberConfirmation(groupId, memberId)
{
	$("#removeMemberConfirmation").modal("show");
	$("#removeMemberConfirmation").attr("removeMemberGroupId",groupId);
	$("#removeMemberConfirmation").attr("removeMemberId",memberId);
}

function removeGroupConfirmation(groupId)
{
	$("#removeGroupConfirmation").modal("show");
	$("#removeGroupConfirmation").attr("removeGroupId",groupId);
}

function removeGroup()
{
	var groupId = $("#removeGroupConfirmation").attr("removeGroupId");
	$("#removeGroupConfirmation").modal("hide");
	
	//alert("groupId:- "+groupId);
	$("#loadingDiv").show();
	//alert("groupId:- "+groupId);
	DistrictAjax.removeApprovalGroupByGroupId(groupId,
	{
		async: true,
		errorHandler:handleError,
		callback: function(data)
		{
			buildApprovalGroup();
			showAddMembers();
		}
	});
}

function removeMember()
{
	var groupId = $("#removeMemberConfirmation").attr("removeMemberGroupId");
	var memberId = $("#removeMemberConfirmation").attr("removeMemberId");
	$("#removeMemberConfirmation").modal("hide");
	
	$("#loadingDiv").show();
	DistrictAjax.removeMemberFromApprovalGroup(groupId,memberId,
	{
		async: true,
		errorHandler:handleError,
		callback: function(data)
		{
			buildApprovalGroup();
			showAddMembers();
			
		}
	});
}

function showAddGroup()
{
	$(".hideGroup").show(300);
	$("#addGroupName").val("");
	//$("#addGroupHyperLink").show();
	//$("#groupMembersList").show();
	showAddMembers();
	$("#addApprovalGroupAndMemberDiv").fadeIn(500);
}

function showAddMembers()
{
	var districtId=document.getElementById("districtId").value;
	$("#loadingDiv").show();
	DistrictAjax.getApprovalGroupsAndMembersList(districtId,
	{
		async: true,
		errorHandler:handleError,
		callback: function(data)
		{
			$.each(data, function(key, value)
			{
				if(key=="groups")
				{
					$("#groupName").empty();
					$("#groupName").append("<option value='-1' selected> "+resourceJSON.msgSelectGroup+" </option>");
					$.each(value, function(groupKey, groupvalue)
					{
						$("#groupName").append("<option value='"+groupKey+"'> "+groupvalue+" </option>");
					});
				}
				else
				if(key=="members")
				{
					$("#groupMembers").empty();
					$.each(value, function(memberKey, membervalue)
					{
						$("#groupMembers").append("<option value='"+memberKey+"'> "+membervalue+" </option>");
					});
				}
	        });
			$("#loadingDiv").hide();
		}
	});
}

function groupRadioButton()
{
	var checked = $('input[name=groupRadioButtonId]:checked').val();
	if(checked=="radioButton1")
	{
		$("groupName").val("");
		$("#groupName").attr("disabled", "disabled");	//disable groupNameList
		$('#groupName').css("background-color", "#EEEEEE");
		$("#addGroupName").removeAttr("disabled");		//enable addGroup
		$('#addGroupName').css("background-color", "#FFF");
		// code here for add new group and members
	}
	else
	if(checked=="radioButton2")
	{
		$("#addGroupName").attr("disabled", "disabled");	//disable groupNameList
		$('#addGroupName').css("background-color", "#EEEEEE");
		$("#groupName").removeAttr("disabled");				//enable addGroup
		$('#groupName').css("background-color", "#FFF");
		//Code here for add members into group
	}
}

function addGroupORMember()
{
	var errorMessage="";
	$("#privilegeForDistrictErrorDiv").empty();
	$('#addGroupName').css("background-color", "#FFF");
	$('#groupName').css("background-color", "#FFF");
	$('#groupMembers').css("background-color", "#FFF");
	
	groupRadioButton();
	
	var checked = $('input[name=groupRadioButtonId]:checked').val();
	if(checked=="radioButton1")	//add new group
	{
		var groupName = $("#addGroupName").val();
		if(groupName!="")
		{
			addGroup();
			showAddMembers();
			$("#addApprovalGroupAndMemberDiv").fadeOut(500);
		}
		else
		{
			errorMessage = errorMessage+"<label class=required>* "+resourceJSON.msgGroupName+" </label><BR>";
			$('#addGroupName').css("background-color", "#F5E7E1");
			$('#addGroupName').focus();
		}
	}
	else if(checked=="radioButton2")	//add member to group
	{
		var groupId = $("#groupName option:selected").val();
		var memberIds=$("#groupMembers :selected").val();
		
		if(groupId=="-1")
			errorMessage = errorMessage+"<label class=required>* "+resourceJSON.msgGroupName+" </label><BR>";
		if(memberIds==null || memberIds=="")
			errorMessage = errorMessage+"<label class=required>* "+resourceJSON.PlzSelectMember+" </label><BR>";
		
		if(memberIds==null || memberIds=="")
		{
			$('#groupMembers').css("background-color", "#F5E7E1");
			$('#groupMembers').focus();
		}
		if(groupId=="-1")
		{
			$('#groupName').css("background-color", "#F5E7E1");
			$('#groupName').focus();
		}
		
		if(groupId!="-1" && memberIds!=null && memberIds!="")
		{
			addMember();
			showAddMembers();
			$("#addApprovalGroupAndMemberDiv").fadeOut(500);
		}
	}
	
	if(errorMessage!="")
	{
		$("#privilegeForDistrictErrorDiv").html(errorMessage);
		$("#privilegeForDistrictErrorDiv").fadeIn(500);
	}
}

function addGroup()
{
	var groupName = $("#addGroupName").val();
	var noOfApproval = $("#noOfApproval").val();
	var districtId=document.getElementById("districtId").value;
	var memberIds="";
	var i=0;
	$("#groupMembers :selected").each(function(){
		if(i==0)
			memberIds = "#"+memberIds+($(this).val())+"#";
		else
			memberIds = memberIds+($(this).val())+"#";
		i=i+1;
	});
	
	$("#loadingDiv").show();
	DistrictAjax.addApprovalGroup(districtId,groupName,noOfApproval,memberIds,true,
	{
		async: true,
		errorHandler:handleError,
		callback: function(data)
		{
			if(data=="GroupExist")
			{
				$('#virusDivId .modal-body').html(resourceJSON.msgGrpAlreadyExist);
				$('#virusDivId').modal('show');
			}
			else
				buildApprovalGroup();
		}
	});
	$("#addGroupName").val("");
}

function addMember()
{
	var groupId = $("#groupName option:selected").val();
	var memberIds="";
	var i=0;
	$("#groupMembers :selected").each(function(){
		//alert("$(this).val():- "+($(this).val()));
		if(i==0)
			memberIds = "#"+memberIds+($(this).val())+"#";
		else
			memberIds = memberIds+($(this).val())+"#";
		i=i+1;
	});
	
	$("#loadingDiv").show();
	DistrictAjax.addMemberIntoApprovalGroup(groupId,memberIds,
	{
		async: true,
		errorHandler:handleError,
		callback: function(data)
		{
			buildApprovalGroup();
			showAddMembers();
		}
	});
}

function displayAllActiveDistrictUser()
{
	var districtId = $("#districtId").val();
	var isChecked = $("#sendNotificationOnNegativeQQId").attr("checked");
	if(isChecked!=null)
	{
		$("#loadingDiv").show();
		DistrictAjax.getActiveDistrictUserList(districtId,
		{
			async: true,
			errorHandler:handleError,
			callback: function(data)
			{	
				$("#DistrictAdminListDiv").show();
				var i=0;
				$.each(data, function(key, value)
				{	
					//1stDistrictAdminList	//add it if sendNotificationOnNegativeQQ flag is false or 0 or null;
					//2ndDistrictAdminList	//add it if sendNotificationOnNegativeQQ flag is true or 1;
					if(i==0)
					{
						$("#2ndDistrictAdminList").empty();	
						$.each(value, function(newKey, newValue)
						{
							$("#2ndDistrictAdminList").append("<option value='"+newKey+"'> "+newValue+" </option>");
						});
					}
					else
					if(i==1)
					{
						$("#1stDistrictAdminList").empty();	
						$.each(value, function(newKey, newValue)
						{
							$("#1stDistrictAdminList").append("<option value='"+newKey+"'> "+newValue+" </option>");
						});
					}
					i=i+1;
				});
				$("#loadingDiv").hide();
			}
		});
	}
	else
		$("#DistrictAdminListDiv").hide();
}

function shiftElementsFromOneDivToSecondDiv(firstDivId, secondDivId)
{
	$("#"+firstDivId+" :selected").each(function(){
		var userkey = $(this).val();
		var userValue = $(this).text();
		$("#"+firstDivId+" option:selected").remove();
		$("#"+secondDivId+"").append("<option value='"+userkey+"'> "+userValue+" </option>");
	});
}

function updateDistrictUserList()
{
	var isChecked = $("#sendNotificationOnNegativeQQId").attr("checked");
	var districtId = $("#districtId").val();
	var secondUserList="";
	
	if(isChecked!=null)
	{
		var i=0;
		$("#2ndDistrictAdminList option").each(function(){
			if(i==0)
				secondUserList = "#"+$(this).val()+"#";
			else
				secondUserList = secondUserList+$(this).val()+"#";
			i=i+1;
		});
	}
	$("#sendNotificationOnNegativeQQId").val(secondUserList);
}

function hideAddApprovalGroupAndMemberDiv()
{
	$("#addApprovalGroupAndMemberDiv").fadeOut(500);
	$("#privilegeForDistrictErrorDiv").empty();
	$("#privilegeForDistrictErrorDiv").fadeOut(500);
}

function getQQInManageDistrict(){
	var districtId = document.getElementById('districtId').value;
	DistrictAjax.getQQInManageDistrict(districtId,{
		async:true,
		erroHandler:handleError,
		callback:function(data){
		if(data!=null){
			document.getElementById('qqAvlDistList').innerHTML=data;
		}
	}
	});
}
function getQQForOnboardingInManageDistrict(){
	var districtId = document.getElementById('districtId').value;
	DistrictAjax.getQQForOnboardingInManageDistrict(districtId,{
		async:true,
		erroHandler:handleError,
		callback:function(data){
		if(data!=null){
			document.getElementById('qqAvlDistListForOnboard').innerHTML=data;
		}
	}
	});
}

$(function(){
	if($('#qqThumbShowOrNot').is(':checked')){
		$('#qqThumbAndContent').prop('disabled', true);
	}else{
		$('#qqThumbAndContent').prop('disabled', false);		
	}
	
	if($('#qqThumbAndContent').is(':checked')){
		$('#qqThumbShowOrNot').prop('disabled', true);
	}else{
		$('#qqThumbShowOrNot').prop('disabled', false);		
	}
	
	$('#qqThumbShowOrNot').click(function(){
		if($('#qqThumbShowOrNot').is(':checked')){
			$('#qqThumbAndContent').prop('disabled', true);
		}else{
			$('#qqThumbAndContent').prop('disabled', false);		
		}
	});
	$('#qqThumbAndContent').click(function(){
		if($('#qqThumbAndContent').is(':checked')){
			$('#qqThumbShowOrNot').prop('disabled', true);
		}else{
			$('#qqThumbShowOrNot').prop('disabled', false);		
		}
	});
});

function showPortfolioReminderDiv(){
	$('#erroroReminderSet').empty();
	var reminderCheck	= 	document.getElementById("sendReminderForPortfolio").checked;
	if(reminderCheck==true)
		$("#sendFrequencyReminderForPortfolio").show();
	else
		$("#sendFrequencyReminderForPortfolio").hide();
}

function showHideVVILink(txt){
	
	if(txt==false){
		 document.getElementById("jobCompletedVVILink1").disabled=false;
		 document.getElementById("statusListBoxForVVI").disabled=false;
	}else if(txt==true){
		 document.getElementById("jobCompletedVVILink1").checked=false;
		 document.getElementById("statusListBoxForVVI").value="";
		 document.getElementById("statusListBoxForVVI").disabled=true;
	}
}

function showAssessmentNotifications(assessmentType){
	var epiSettingChk = document.getElementById("epiSettingChk").checked;
	var jsiSettingChk = document.getElementById("jsiSettingChk").checked;
	if(assessmentType==0){
		if(epiSettingChk==true){
			$("#epiSettingDiv").show();
		}
		else{
			$("#epiSettingDiv").hide();
		}
		if(jsiSettingChk==true){
			$("#jsiSettingDiv").show();
		}
		else{
			$("#jsiSettingDiv").hide();
		}
	}
	else if(assessmentType==1){
		if(epiSettingChk==true){
			$("#epiSettingDiv").show();
			$("#epiSetting1").prop('checked',true);
		}
		else{
			$("#epiSettingDiv").hide();
		}
	}
	else if(assessmentType==2){
		if(jsiSettingChk==true){
			$("#jsiSettingDiv").show();
			$("#jsiSetting1").prop('checked',true);
		}
		else{
			$("#jsiSettingDiv").hide();
		}
	}
}

