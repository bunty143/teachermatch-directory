var deviceTypeAndroid=$.browser.device = (/Android|webOS|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent.toLowerCase()));
var deviceType=$.browser.device = (/iPhone|iPad|iPod/i.test(navigator.userAgent.toLowerCase()));
var isSafari = /Safari/.test(navigator.userAgent) && /Apple Computer/.test(navigator.vendor);



var page = 1;
var noOfRows = 10;
var sortOrderStr="";
var sortOrderType="";
var domainpage = 1;
var domainnoOfRows = 10;
var domainsortOrderStr="";
var domainsortOrderType="";


function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert(""+resourceJSON.msgServerErr+": "+exception.javaClassName);}
}

function getPaging(pageno)
{
	if(pageno!='') {
		page=pageno;	
	} else {
		page=1;
	}

	noOfRows = document.getElementById("pageSize").value;
	displayEventGrids();
}

function getPagingAndSorting(pageno,sortOrder,sortOrderTyp) {
	var gridNo	=	document.getElementById("gridNo").value;
		if(gridNo==3) {	
			if(pageno!='') {
				domainpage=pageno;	
			} else {
				domainpage=1;
			}
			domainsortOrderStr	=	sortOrder;
			domainsortOrderType	=	sortOrderTyp;
			if(document.getElementById("pageSize3")!=null){
				domainnoOfRows = document.getElementById("pageSize3").value;
			}else{
				domainnoOfRows=10;
			}
			displayEventGrids();
		}else if(gridNo==1){
				if(pageno!=''){
				page=pageno;	
			} else {
				page=1;
			}
			sortOrderStr	=	sortOrder;
			sortOrderType	=	sortOrderTyp;
			if(document.getElementById("pageSize")!=null){
				noOfRows = document.getElementById("pageSize").value;
			}else{
				noOfRows=10;
			}
			displayEventGrids();
		} else {
				if(pageno!=''){
				page=pageno;	
			}else{
				page=1;
			}
			
			sortOrderStr	=	sortOrder;
			sortOrderType	=	sortOrderTyp;
			
			if(document.getElementById("pageSize")!=null || document.getElementById("pageSize")!=""){
				
				noOfRows = document.getElementById("pageSize").value;
				
			}else{
				
				noOfRows=10;
			}	
			displayEventGrids();
		}
}

function displayEventGrids() 
{
	/*//alert("call")
	$("#loadingDiv").show();
	try{
	   SlotSelectionAjax.displayEvents(noOfRows,page,sortOrderStr,sortOrderType,{
		async: true,
		callback: function(data)
		{
		   //alert(" data :: "+data);
			$('#eventGrid').html(data);
		    applyScrollOnTblEvent();
			$("#loadingDiv").hide();
	   },
		errorHandler:handleError
	});
	}catch(e)
	{alert(e);}
	$("#loadingDiv").hide();*/
}

function  showSchedule(eventId,status)
{
	$('#errordivSchedule').empty();
	$("#loadingDiv").show();
	try{
	   SlotSelectionAjax.showSlotGrid(eventId,status,noOfRows,page,sortOrderStr,sortOrderType,{
		async: true,
		callback: function(data)
		{
	  if(status==1)
		{
		$("#apprdiv").hide();
		}
		else
		{
			$("#apprdiv").show();	
		}	
		$('#slotsdiv').show();
			$('#scheduleGrid').html(data);
			applyScrollOnTblSlots();
			$("#loadingDiv").hide();
			},
		errorHandler:handleError
	});
	}catch(e)
	{
	alert(e);	
	}
	//$("#loadingDiv").hide();
}

function approveSlots()
{
	$('#errordivSchedule').empty();
	$('#slotsdiv').hide();
	$("#loadingDiv").fadeIn();
	
	var teacherId=document.getElementById("teacherId").value;
	var checkway = document.getElementsByName('chooseoption');	
	var eventScheduleId='';
	for(var i = 0, length = checkway.length; i < length; i=i+1){
	    if (checkway[i].checked) 
	      {
	    	eventScheduleId=checkway[i].value;
	        break;
	    }
	 }
	if(eventScheduleId!=''){
		
		SlotSelectionAjax.saveSlots(eventScheduleId,{
		    async: false,
		    callback: function(data)
			  {
		    	if(data=='success'){
		    		$("#loadingDiv").hide();
					var eventId=document.getElementById("eventId").value;
					var email=document.getElementById("email").value;
					displayEventGridsNew(eventId,email);
		    	}else{
		    		$('#slotsdiv').show();
		    		$("#loadingDiv").hide();
		    		$('#errordivSchedule').empty();
		    		if(data=="I"){
		    			$('#errordivSchedule').append("&#149;This event has been canceled, please contact your district/school.<br>");
		    		}else{
		    			$('#errordivSchedule').append("&#149;"+resourceJSON.PlzChooseAnotherSlot+"<br>");
		    		}
		    		$('#errordivSchedule').show();
		    	}
			  },
			errorHandler:handleError
		}		
	);
	}else{
		$("#loadingDiv").hide();
		$('#slotsdiv').show();
		$('#errordivSchedule').append("&#149; "+resourceJSON.PlzSelectSchedule+"<br>");
		$('#errordivSchedule').show();
	}
}

function hideSlotDiv()
{
	$("#slotsdiv").hide();	
}

function displayEventGridsNew(eventId,email) 
{
	//alert("eventId :: "+eventId);
	$("#loadingDiv").fadeIn();  
	try{
	   SlotSelectionAjax.displayEventsNew(eventId,email,noOfRows,page,sortOrderStr,sortOrderType,{
		async: true,
		callback: function(data)
		{
		   //alert(" data :: "+data);
			$('#eventGridNew').html(data);
		    applyScrollOnTblEvent();
			$("#loadingDiv").hide();
	   },
		errorHandler:handleError
	});
  }catch(e){}
	
}

