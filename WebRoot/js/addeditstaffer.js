/*========  For Sorting  ===============*/
var page 			= 	1;
var noOfRows 		= 	10;
var sortOrderStr	=	"";
var sortOrderType	=	"";
var position		=	"";

function getPaging(pageno)
{
	if(pageno!='')
	{
		page=pageno;	
	}
	else
	{
		page=1;
	}
	$("#districtId").val("");
	
	noOfRows = document.getElementById("pageSize").value;
	getStafferList();
}

function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{	
	flag=true;
	
	if(pageno!=''){
		page=pageno;	
	}else{
		page=1;
	}
	sortOrderStr	=	sortOrder;
	sortOrderType	=	sortOrderTyp;
	if(document.getElementById("pageSize1")!=null){
		noOfRows = document.getElementById("pageSize1").value;
	}else{
		noOfRows=10;
	}
	getStafferList();
}
/* ======== For Handling session Time out Error =============== */
function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert(resourceJSON.msgServerErr+": "+exception.javaClassName);}
}
/* ======= Save Panel on Press Enter Key ========= */
function chkForEnterStaffer(evt)
{
	var charCode = ( evt.which ) ? evt.which : evt.keyCode;
	if(charCode==13)
	{
		getStafferList();
	}	
}
var flag=false;
var isEdited=true;
function addStaffer()
{	
	$(".divErrorMsg").empty();
	$('input:text').css("background-color", "white ");
	$('#loadingDiv').show();
	$('#loadingDiv').fadeOut(1000);
	$("#successMsg").show();
	$("#editMsg").hide();
	isEdited=false;
	
	$('#stafferDiv').show();
	$("#schoolId").val("");
	$("#stafferId").val("");
	$("#stafferIdBean").val("");
	$("#schoolName").val("");
	$("#stafferName").val("");
	$("#districtName1").val("");
	$('#districtName1').prop('disabled',false);
	// $('#seveBtn').prop('disabled',true);
	$("#districtName1").focus();
}

function cancelStaffer()
{
	$(".divErrorMsg").empty();
	$("#successMsg").hide();
	$("#editMsg").hide();
	$('input:text').css("background-color", "white ");
	$('#stafferDiv').hide();
	$("#schoolId").val("");
	$("#stafferId").val("");
	$('#loadingDiv').show();
	$('#loadingDiv').fadeOut(1000);
}
// for autofill the staffer details and alert user staffer already exists
function stafferBySchoolId(flg){
	var schoolId=$("#schoolId").val();	
	// $(".divErrorMsg").empty();
	StafferAjax.getStafferBySchoolId(schoolId,{
		async:true,
		callback:function(data)
		{
		if(data!=null){
		var stafferId=data.stafferId;
		var userId=data.userMaster.userId;
		var stafferName=data.userMaster.firstName+" "+data.userMaster.lastName;
		// $('#stafferName').prop('disabled',false);
		if($("#stafferId").val()=='' ){
			// check if alert box should be shown or not for new staffer entry
				if(flg){				
					$('#mystaffer').modal('show');
					$("#stafferName").val('');
					$("#stafferId").val('');
					$("#schoolName").val('');
					$("#schoolid").val('');
					$("#schoolName").focus();
					$('#stafferName').prop('disabled',false);
				}else{				
					$("#stafferId").val(userId);
					$('#stafferName').val(stafferName);
					$('#stafferIdBean').val(stafferId);
				}
			
		}else{
			
			// check if staffer edit function is invoked
			if($("#stafferId").val()!='' && stafferId!='' && stafferId!=$("#stafferIdBean").val()){				
			if(flg){
				$('#mystaffer').modal('show');
				$("#stafferName").val('');
				$("#stafferId").val('');
			// $("#schoolName").val('');
			// $("#schoolid").val('');
				$("#schoolName").focus();
				$('#stafferName').prop('disabled',false);
				}
			}
			$("#stafferId").val(userId);
			$('#stafferName').val(stafferName);
			$('#stafferIdBean').val(stafferId);
		}
		
		
			
		}		
		else{
			// $('#seveBtn').prop('disabled',false);
			$("#stafferId").val("");
			$("#stafferIdBean").val("");
			$("#stafferName").val("");
		}
	},
	});
}


function addStafferList()
{
	var districtId=$("#districtId").val();
	var schoolId=$("#schoolId").val();
	var stafferIdBean=$("#stafferIdBean").val();
	var stafferId=$("#stafferId").val();
	
	$("#schoolId").val("");
	$('#districtName1').prop('disabled',false);
	if($("#schoolName").val()==0 || $("#schoolName").val()==''){
	$("#schoolId").val("");
	}
	if($("#districtName").val()==0 || $("#districtName").val()==''){
		$("#districtId").val("");
	}
	
	StafferAjax.saveStaffer(districtId,schoolId,stafferId,stafferIdBean,{
		async:true,
		callback:function(data)
		{	
			
		$("#stafferId").val("");
			$("#schoolId").val("");			
			$('#stafferDiv').hide();			
			if($("#districtName").val()==''){
			flag=true;
			$("#districtId").val("");
			}
						
			
			// check if new record is added
			if(data=='add' && data!=null){
				isEdited=false;
				$("#successMsg").show();
				delay(200);
				$("#mystafferMsg").modal('show');
			}else{
			getStafferList();
			}
		},
	});
	
}

function closeMsg(){
	$("#mystafferMsg").modal('hide');
	$("#successMsg").hide();
	$("#editMsg").hide();
	$('#loadingDiv').show();
	$('#loadingDiv').fadeOut(1000);
}
function editStafferDetails(stafferId)
{	
	$(".divErrorMsg").empty();
	$('input:text').css("background-color", "white ");
	$('#loadingDiv').show();	
	// $("#successMsg").hide();
	StafferAjax.getStaffer(stafferId,{
		async:true,
		callback:function(data)
		{	
			var stafferName="";
			if(data.userMaster.middleName!="" && data.userMaster.middleName!=null)
			{
				stafferName=data.userMaster.firstName+" "+data.userMaster.middleName+" "+data.userMaster.lastName;
			}else
			{
				stafferName=data.userMaster.firstName+" "+data.userMaster.lastName;
			}
			$("#stafferIdBean").val(stafferId);
			$("#stafferId").val(data.userMaster.userId);
			$("#stafferName").val(stafferName);
			$("#districtId").val(data.districtMaster.districtId);
			$("#districtName1").val(data.districtMaster.districtName);
			$("#schoolName").val(data.schoolMaster.schoolName);
			$("#schoolName2").val(data.schoolMaster.schoolName);
			$("#schoolId").val(data.schoolMaster.schoolId);
			$("#districtName1").prop("disabled",true);
			$('#districtName1').css("background-color", "#F5E7E1");
			$('#stafferDiv').show();
			$('#loadingDiv').fadeOut(1000);
			$("#seveBtn").focus();
		},
	});
	
}
function editSchool(){
	// editStafferDetails(stafferId);
	$(".divErrorMsg").empty();
	$('input:text').css("background-color", "white ");
	if($("#schoolId").val()!='' && $("#stafferId").val()!='' ){
		$('#mystaffer').modal('hide');
		// $("#stafferName").val('');
		// $("#stafferId").val('');
		// $("#schoolName").val('');
		// $("#schoolid").val('');
		// $("#schoolName").focus();
	}
	else{
	// $("#schoolId").val('');
	// $("#schoolName").val('');
	// $("#stafferName").val('');
		$('#mystaffer').modal('hide');
	// $("#stafferId").val('');
		$("#schoolName").focus();
	}
	
}

function enableSaveButton(){
	if($("#districtName1").val()!='' && $("#schoolName").val()!=''){
		$('#stafferName').prop('disabled',false);
	}else{
		$('#stafferName').prop('disabled',false);
	}
}


function validateStaffer(){
	$(".divErrorMsg").empty();
	$('input:text').css("background-color", "white ");
	if($("#stafferName").val()!='' && $("#districtName1").val()!='' && $("#schoolName").val()!='' && $("#districtId").val()!=''){		
	//	alert('valid');
		if($("#stafferId").val()!=undefined && $("#stafferId").val()!='' && $("#schoolId").val()!=''){
		addStafferList();
		// return true;
		}else{
			if($("#stafferId").val()==''){
				$('#errordivStafferName').show();
				$('#errordivStafferName').html("&#149; "+resourceJSON.msgStafferName+"<br>");
				$('#stafferName').focus();
				$('#stafferName').css("background-color", "#F5E7E1");
				// $("#stafferName").val('');
			}
			if($("#districtId").val()==''){
				$('#errordivDistrict1').show();
				$('#errordivDistrict1').html("&#149; "+resourceJSON.msgcorrectDistrictName+"<br>");
				$('#districtName1').focus();
				$('#districtName1').css("background-color", "#F5E7E1");
				// $("#stafferName").val('');
			}
			if($("#schoolId").val()==''){
				$('#errordivSchoolName').show();
				$('#errordivSchoolName').html("&#149; "+resourceJSON.msgcorrectSchoolName+"<br>");
				$('#schoolName').focus();
				$('#schoolName').css("background-color", "#F5E7E1");
				// $("#stafferName").val('');
			}
			return true;
		}
	}else{
		flag=false;		
		if($("#stafferName").val()==''){
			$('#errordivStafferName').show();
			$('#errordivStafferName').html("&#149; "+resourceJSON.msgPleaseenterStafferName+"<br>");
			$('#stafferName').focus();
			$('#stafferName').css("background-color", "#F5E7E1");
		}

		if($("#stafferId").val()==''){
			$('#errordivStafferName').show();
			$('#errordivStafferName').html("&#149; "+resourceJSON.msgcorrectDistrictName+"<br>");
			$('#stafferName').focus();
			$('#stafferName').css("background-color", "#F5E7E1");
			// $("#stafferName").val('');
		}
		if($("#schoolName").val()==''){
			$('#errordivSchoolName').show();
			$('#errordivSchoolName').html("&#149; "+resourceJSON.msgPleaseenterSchoolName+"<br>");
			$('#schoolName').focus();
			$('#schoolName').css("background-color", "#F5E7E1");
		}
		if($("#schoolId").val()==''){
			$('#errordivSchoolName').show();
			$('#errordivSchoolName').html("&#149; "+resourceJSON.msgcorrectSchoolName+"<br>");
			$('#schoolName').focus();
			$('#schoolName').css("background-color", "#F5E7E1");
			// $("#stafferName").val('');
		}
		if($("#districtName1").val()==''){
			$('#errordivDistrict1').show();
			$('#errordivDistrict1').html("&#149; "+resourceJSON.msgEnterDistName+"<br>");
			$('#districtName1').focus();
			$('#districtName1').css("background-color", "#F5E7E1");
		}
		if($("#districtId").val()==''){
			$('#errordivDistrict1').show();
			$('#errordivDistrict1').html("&#149; "+resourceJSON.msgcorrectDistrictName+"<br>");
			$('#districtName1').focus();
			$('#districtName1').css("background-color", "#F5E7E1");
			// $("#stafferName").val('');
		}
		
		
		
		
		return false;
	}
}

function clearSchool(){
	$("#schoolId").val('');
	$("#schoolName").val('');
}

function getStafferList()
{
	$('#errordivDistrict').empty();
	if($("#districtName").val()!=$("#districtName1").val()){
		if($("#districtName").val()=='')
		$("#districtId").val("");
		$("#schoolId").val();
		flag=true;
	}
	if($("#schoolName").val()!=$("#schoolName1").val()){
		if($("#schoolName1").val()==''){
		$("#schoolId").val('');
		}
		flag=true;
	}
	var districtId=$("#districtId").val();
	var schoolId=$("#schoolId").val();
	$('#loadingDiv').show();
	if(districtId!="" || districtId!=0 || schoolId!="" || schoolId!=0 || flag==true){
		StafferAjax.getStafferDetail(districtId,schoolId,noOfRows,page,sortOrderStr,sortOrderType,{ 
			async: true,
			callback: function(data){
				$('#stafferGrid').html(data);
				$('#loadingDiv').fadeOut('slow');
				if(districtId==undefined ){
					$("#schoolName1").val("");
					$("#schoolName1").prop("disabled",true);
				}
				if($("#districtName").val()!=undefined){
					$("#schoolId").val("");					
				}
				if($("#districtName").val().length==0 || $("#districtName").val()==''){
					$("#districtId").val("");
				}
				$('#stafferpage').show();
				$('#stafferDiv').hide();
				applyScrollOnTbl();
		},
		});
	}else{
		if(!flag){
			flag=true;
			getStafferList()
		}else{
		$('#errordivDistrict').show();
		$('#errordivDistrict').append("&#149; "+resourceJSON.msgEnterDistName+"<br>");
		$('#districtName').focus();
		$('#districtName').css("background-color", "#F5E7E1");
		}
	}
}

function showDeleteModel(stafferId)
{
	$("#stafferIdBean").val(stafferId);
	$('#deleteStaffer').modal('show');
}
function deleteStaffer(){
	var stafferIdBean=$("#stafferIdBean").val();
	StafferAjax.deleteStafferById(stafferIdBean,{ 
		async: false,
		callback: function(data){
			$('#deleteStaffer').modal('hide');
			flag=true;
			$("#schoolName2").val("");
			$("#stafferName2").val("");
			$("#districtName2").val("");
			$('#stafferDiv').hide();
			if($("#districtName").val()=='' && $("#schoolName").val().length==0){
				$("#districtId").val("");
				$("#schoolId").val("");	
			}			
			getStafferList();
	},
	errorHandler:handleError 
	});	 
}

function showSchool()
{
	var districtId = $("#districtId").val();	
	if(districtId!=''&& districtId.length>0)
	{
		document.getElementById("schoolName1").disabled=false;
		$("#schoolName1").val('');
		$("#schoolId1").val('');
		
	}else{
		$("#schoolName1").val('');
		$("#schoolId1").val('');
		document.getElementById("schoolName1").disabled=true;
	}
}

function delay(sec){
	var starttime = new Date().getTime();
	starttime = starttime+sec;
	while(true){
		if(starttime< new Date().getTime()){
			break;
		}
	}
}

// ////////////////////////////////////////////////////////////////
var count=0;
var index=-1;
var length=0;
var divid='';
var txtid='';
var hiddenDataArray = new Array();
var showDataArray = new Array();
var degreeTypeArray = new Array();
var hiddenId="";
function getDistrictAuto(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) // up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("districtName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getDistrictArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getDistrictArray(districtName){
	var searchArray = new Array();
	ManageJobOrdersAjax.getFieldOfDistrictList(districtName,{ 
			async: false,
			callback: function(data){
			hiddenDataArray = new Array();
			showDataArray = new Array();
			for(i=0;i<data.length;i++){
				searchArray[i]=data[i].districtName;
				showDataArray[i]=data[i].districtName;
				hiddenDataArray[i]=data[i].districtId;
			}
		},
		errorHandler:handleError
		});	
	return searchArray;
}


var selectFirst="";

var fatchData= function(txtSearch,searchArray,txtId,txtdivid){
	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
				
			}
			
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}
function hideDistrictMasterDiv(dis,hiddenId,divId)
{
	document.getElementById("districtId").value="";
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			document.getElementById("districtId").value=hiddenDataArray[index];
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}

var downArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index<length-1){
			for(var i=0;i<10;i++){
				{
					if(document.getElementById('divResult'+txtdivid+i))
						var div_id=document.getElementById('divResult'+txtdivid+i);
					if(div_id)
					{
						if(div_id.className=='over')
							index=div_id.id.split('divResult'+txtdivid)[1];
						div_id.className='normal';
					}
				}
			}
			index++;
			if(document.getElementById('divResult'+txtdivid+index))
			{
				var div_id=document.getElementById('divResult'+txtdivid+index);
				div_id.className='over';
				document.getElementById(txtid).value = div_id.innerHTML;
				selectFirst=div_id.innerHTML;
			}
		}
	}
}

var upArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index>0){
			for(var i=0;i<length;i++){

				var div_id=document.getElementById('divResult'+txtdivid+i);
				if(div_id)
				{
					if(div_id.className=='over')
						index=div_id.id.split('divResult'+txtdivid)[1];
					div_id.className='normal';
				}
			}
			index--;
			if(document.getElementById('divResult'+txtdivid+index))
				document.getElementById('divResult'+txtdivid+index).className='over';
			if(txtid && document.getElementById('divResult'+txtdivid+index))
			{
				document.getElementById(txtid).value=
					document.getElementById('divResult'+txtdivid+index).innerHTML;
				selectFirst=document.getElementById('divResult'+txtdivid+index).innerHTML;
			}
		}
	}
}

var overText=function (div_value,txtdivid) {

	for(var i=0;i<length;i++)
	{
		if(document.getElementById('divResult'+i))
		{
			var div_id=document.getElementById('divResult'+i);

			if(div_id.className=='over')
				index=div_id.id.split(txtdivid)[1];
			div_id.className='normal';
		}
	}
	div_value.className = 'over';
	document.getElementById(txtid).value= div_value.innerHTML;
}
function mouseOverChk(txtdivid,txtboxId)
{	
	for(var i=0;i<length;i++)
	{
		$('#divResult'+txtdivid+i).mouseover({param1: i,param2:txtdivid, param3:txtboxId}, fireMouseOverEvent);
	}
}
function chkLenOfTextArea(textValue,maxlength)
{
	var tlen=textValue.value.length;
	if(tlen>maxlength)
	{
		document.getElementById(textValue.id).value=document.getElementById(textValue.id).value.substr(0,maxlength)
	}
}
function fireMouseOverEvent(event)
{
	for(var i=0;i<length;i++)
	{	
		document.getElementById('divResult'+event.data.param2+i).className='normal';
	}
    document.getElementById('divResult'+event.data.param2+event.data.param1).className='over';	       
   	document.getElementById(event.data.param3).value= $('#divResult'+event.data.param2+event.data.param1).text();
    index=event.data.param1;
}

// /////////////////////////////// school auto list
// //////////////////////////////////////

function getSchoolAuto(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) // up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("schoolName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getSchoolArray(txtSearch.value);
		fatchData2(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getSchoolArray(schoolName){
	var searchArray = new Array();
	var districtId	=	document.getElementById("districtId").value;
	ManageJobOrdersAjax.getFieldOfSchoolList(districtId,schoolName,{ 
			async: false,
			callback: function(data){
			hiddenDataArray = new Array();
			showDataArray = new Array();
			for(i=0;i<data.length;i++){
				searchArray[i]=data[i].schoolName;
				showDataArray[i]=data[i].schoolName;
				hiddenDataArray[i]=data[i].schoolId;
			}
		},
		errorHandler:handleError
		});	
	return searchArray;
}


selectFirst="";

var fatchData2= function(txtSearch,searchArray,txtId,txtdivid){

	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
				
			}
			
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}
function hideSchoolMasterDiv(dis,hiddenId,divId)
{
	document.getElementById("schoolId").value="";
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			document.getElementById("schoolId").value=hiddenDataArray[index];
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}

var downArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index<length-1){
			for(var i=0;i<10;i++){
				{
					if(document.getElementById('divResult'+txtdivid+i))
						var div_id=document.getElementById('divResult'+txtdivid+i);
					if(div_id)
					{
						if(div_id.className=='over')
							index=div_id.id.split('divResult'+txtdivid)[1];
						div_id.className='normal';
					}
				}
			}
			index++;
			if(document.getElementById('divResult'+txtdivid+index))
			{
				var div_id=document.getElementById('divResult'+txtdivid+index);
				div_id.className='over';
				document.getElementById(txtid).value = div_id.innerHTML;
				selectFirst=div_id.innerHTML;
			}
		}
	}
}

var upArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index>0){
			for(var i=0;i<length;i++){

				var div_id=document.getElementById('divResult'+txtdivid+i);
				if(div_id)
				{
					if(div_id.className=='over')
						index=div_id.id.split('divResult'+txtdivid)[1];
					div_id.className='normal';
				}
			}
			index--;
			if(document.getElementById('divResult'+txtdivid+index))
				document.getElementById('divResult'+txtdivid+index).className='over';
			if(txtid && document.getElementById('divResult'+txtdivid+index))
			{
				document.getElementById(txtid).value=
					document.getElementById('divResult'+txtdivid+index).innerHTML;
				selectFirst=document.getElementById('divResult'+txtdivid+index).innerHTML;
			}
		}
	}
}

function mouseOverChk(txtdivid,txtboxId)
{	
	for(var i=0;i<length;i++)
	{	
		$('#divResult'+txtdivid+i).mouseover({param1: i,param2:txtdivid, param3:txtboxId}, fireMouseOverEvent);
	}
}

function fireMouseOverEvent(event)
{
	for(var i=0;i<length;i++)
	{	
		document.getElementById('divResult'+event.data.param2+i).className='normal';
	}
    document.getElementById('divResult'+event.data.param2+event.data.param1).className='over';	       
   	document.getElementById(event.data.param3).value= $('#divResult'+event.data.param2+event.data.param1).text();
    index=event.data.param1;
}



var overText=function (div_value,txtdivid) {

	for(var i=0;i<length;i++)
	{
		if(document.getElementById('divResult'+i))
		{
			var div_id=document.getElementById('divResult'+i);

			if(div_id.className=='over')
				index=div_id.id.split(txtdivid)[1];
			div_id.className='normal';
		}
	}
	div_value.className = 'over';
	document.getElementById(txtid).value= div_value.innerHTML;
}

// /////////////////////////////// staffer auto list
// //////////////////////////////////////

function getStafferAuto(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) // up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("stafferName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getStafferArray(txtSearch.value);
		fatchData2(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getStafferArray(stafferName){
	var searchArray = new Array();
	var districtId	=	document.getElementById("districtId").value;
	// alert(districtId+" ")
	StafferAjax.getFieldOfStafferList(districtId,stafferName,{ 
			async: false,
			callback: function(data){
			hiddenDataArray = new Array();
			showDataArray = new Array();
			for(i=0;i<data.length;i++){
				var name=data[i].middleName!=null && data[i].middleName!=""?data[i].firstName+" "+data[i].middleName+" "+data[i].lastName:data[i].firstName+" "+data[i].lastName;
				searchArray[i]=name;
				showDataArray[i]=name;
				hiddenDataArray[i]=data[i].userId;
			}
		},
		errorHandler:handleError
		});	
	return searchArray;
}


selectFirst="";

var fatchData2= function(txtSearch,searchArray,txtId,txtdivid){

	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
				
			}
			
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}
function hideStafferMasterDiv(dis,hiddenId,divId)
{
	document.getElementById("stafferId").value="";
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			document.getElementById("stafferId").value=hiddenDataArray[index];
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}

var downArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index<length-1){
			for(var i=0;i<10;i++){
				{
					if(document.getElementById('divResult'+txtdivid+i))
						var div_id=document.getElementById('divResult'+txtdivid+i);
					if(div_id)
					{
						if(div_id.className=='over')
							index=div_id.id.split('divResult'+txtdivid)[1];
						div_id.className='normal';
					}
				}
			}
			index++;
			if(document.getElementById('divResult'+txtdivid+index))
			{
				var div_id=document.getElementById('divResult'+txtdivid+index);
				div_id.className='over';
				document.getElementById(txtid).value = div_id.innerHTML;
				selectFirst=div_id.innerHTML;
			}
		}
	}
}

var upArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index>0){
			for(var i=0;i<length;i++){

				var div_id=document.getElementById('divResult'+txtdivid+i);
				if(div_id)
				{
					if(div_id.className=='over')
						index=div_id.id.split('divResult'+txtdivid)[1];
					div_id.className='normal';
				}
			}
			index--;
			if(document.getElementById('divResult'+txtdivid+index))
				document.getElementById('divResult'+txtdivid+index).className='over';
			if(txtid && document.getElementById('divResult'+txtdivid+index))
			{
				document.getElementById(txtid).value=
					document.getElementById('divResult'+txtdivid+index).innerHTML;
				selectFirst=document.getElementById('divResult'+txtdivid+index).innerHTML;
			}
		}
	}
}

function mouseOverChk(txtdivid,txtboxId)
{	
	for(var i=0;i<length;i++)
	{	
		$('#divResult'+txtdivid+i).mouseover({param1: i,param2:txtdivid, param3:txtboxId}, fireMouseOverEvent);
	}
}

function fireMouseOverEvent(event)
{
	for(var i=0;i<length;i++)
	{	
		document.getElementById('divResult'+event.data.param2+i).className='normal';
	}
    document.getElementById('divResult'+event.data.param2+event.data.param1).className='over';	       
   	document.getElementById(event.data.param3).value= $('#divResult'+event.data.param2+event.data.param1).text();
    index=event.data.param1;
}



var overText=function (div_value,txtdivid) {

	for(var i=0;i<length;i++)
	{
		if(document.getElementById('divResult'+i))
		{
			var div_id=document.getElementById('divResult'+i);

			if(div_id.className=='over')
				index=div_id.id.split(txtdivid)[1];
			div_id.className='normal';
		}
	}
	div_value.className = 'over';
	document.getElementById(txtid).value= div_value.innerHTML;
}


