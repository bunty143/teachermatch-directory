/* @Author: Gagan 
 * @Discription: view of edit domain js.
*/
var page = 1;
var noOfRows = 50;
var sortOrderStr="";
var sortOrderType="";
function getPagingForJobboard(pageno)
{
	if(pageno!='')
	{
		page=pageno;	
	}
	else
	{
		page=1;
	}
	
	noOfRows = document.getElementById("pageSize").value;
	displayJobRecords();
}
function getPaging(pageno)
{
	if(pageno!='')
	{
		page=pageno;	
	}
	else
	{
		page=1;
	}
	
	noOfRows = document.getElementById("pageSize").value;
	displayJobRecords();
}
function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
	if(pageno!=''){
		page=pageno;	
	}else{
		page=1;
	}
	sortOrderStr=sortOrder;
	sortOrderType=sortOrderTyp;
	if(document.getElementById("pageSize")!=null){
		noOfRows = document.getElementById("pageSize").value;
	}else{
		noOfRows=10;
	}
	displayJobRecords();
}
function jobBoardFooter()
{
	$('#pagingFooter').addClass('jboardFooter');  //will add the class
	$('#pagingFooter').removeClass('net-widget-footer'); //will remove the class
	$('#pagingFooter').removeClass('net-widget'); //will remove the class
	$('#pagingFooter').removeClass('net-corner-bottom'); //will remove the class
}

/*========  For Handling session Time out Error ===============*/
function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert(resourceJSON.msgSomeerror+": "+exception.javaClassName);}
}

/*========  SearchDistrictOrSchool ===============*/

/*=========== Display Filter =================*/
/*function displayFilter()
{
	//alert("22222222");
	
	var x=document.getElementsByName("filter");
	var value=0;
	for(i=0;i<x.length;i++)
		{
			if(x[i].checked	==	true)
			{
				value		=	x[i].value;
				break;
			}
		}
	if(value==0)
		{
			document.getElementById('dropdownSearch').selectedIndex=0;
			$("#dropdownSearch").attr("disabled", "disabled");
			$("#fromDate").removeAttr("disabled"); 
    		$("#toDate").removeAttr("disabled"); 
		}
	else
		{
			$("#dropdownSearch").removeAttr("disabled"); 
			$("#fromDate").attr("disabled", "disabled"); 
			$("#toDate").attr("disabled", "disabled"); 
			
		}
}*/
function searchJob()
{
//	alert("11111111111111111");
	
	var fromDate		=	$('#fromDate').val(); 
	var toDate			=	$('#toDate').val(); 
//	var filter1	=document.getElementById("filter1").checked;
//	var filter2	=document.getElementById("filter2").checked;
	
	/*if(filter2)
	{
		var dropdownSearch	=$('#dropdownSearch').val(); 
		if(dropdownSearch=="Select days"){
			document.getElementById("Msg").innerHTML="Please select days";
			$('#myModalJobMsg').modal('show');
		}else{
			page=1;
			noOfRows=10;
			displayJobRecords();
		}
	}else if(fromDate!="" && toDate!="" && filter1){
		var endDate = new Date(toDate);              
	    var startDate= new Date(fromDate);   
		if(startDate > endDate){
			document.getElementById("Msg").innerHTML="From Date must be less than or equal to To Date";
			$('#myModalJobMsg').modal('show');
		}else{
			page=1;
			noOfRows=10;
			displayJobRecords();
		}
	}else{
		page=1;
		noOfRows=10;
		displayJobRecords();
	}*/
	
	page=1;
	noOfRows=10;
	displayJobRecords();
}
/*========  displayJobRecords ===============*/
function displayJobRecords()
{
	$('#loadingDiv').fadeIn();
//	var fromDate		=	$('#fromDate').val(); 
//	var toDate			=	$('#toDate').val(); 
	var dayWiseSearch	=	$('#dropdownSearch').val(); 
//	var filter1	=document.getElementById("filter1").checked;
//	var filter2	=document.getElementById("filter2").checked;
	
//	alert("dayWiseSearch   "+dayWiseSearch);
	
	var districtId = $('#districtId').val();
	var schoolId1 = $('#schoolId1').val();
	var cityId = $('#cityId').val();
	var stateId = $('#stateId').val();
	var zipCode = $('#zipCode').val();
	var subjectId 		= 	document.getElementById('subjectId');;
	var certificateTypeMaster	= 	document.getElementById('certificateTypeMaster').value;
	
	var subjectIdList = "";
	for(var i=0; i<subjectId.options.length;i++)
	{	
		if(subjectId.options[i].selected == true){
			subjectIdList = subjectIdList+subjectId.options[i].value+",";
		}
	}
	
//	alert(" districtId - "+districtId+" schoolId1 - "+schoolId1+" cityId - "+cityId+" stateId - "+stateId+" zipCode - "+zipCode+" subjectId - "+subjectId);

	delay(1000);
	JobBoardAjax.displayJobsByTM(dayWiseSearch,noOfRows,page,sortOrderStr,sortOrderType,districtId,schoolId1,cityId,stateId,zipCode,subjectIdList,certificateTypeMaster,{ 
		async: true,
		callback: function(data)
		{
		var getValue = data.split("@@@");
			$('#divMain').html(getValue[0]);
			//applyScrollOnTbl();	
			$('#loadingDiv').hide();
			showDisplays();
		},
		errorHandler:handleError
	});
}
function delay(sec){
	var starttime = new Date().getTime();
	starttime = starttime+sec;
	while(true){
		if(starttime< new Date().getTime()){
			break;
		}
	}
}
function showDisplays()
{
	var noOrRow = document.getElementById("rowsappended").value;
	for(i=1;i<=noOrRow;i++)
	{	
		var sh = $('#des'+i).text();
		if(sh.length>=250)
			$('#desShow'+i).html(sh.substring(0,250)+"...");
		else
			$('#desShow'+i).html(sh);
	}
}
function showApply(showId)
{
	$('#des'+showId).toggle(2000);
	$('#aply'+showId).toggle(2000);
	$('#desShow'+showId).hide(1000);
	$('#showLink'+showId).hide(1000);
}
function hideApply(showId)
{
	$('#desShow'+showId).toggle(2000);
	$('#showLink'+showId).toggle(2000);
	$('#des'+showId).hide(1000);
	$('#aply'+showId).hide(1000);
}
var xmlHttp=null;
function createXMLHttpRequest()
{
	if (window.ActiveXObject)
		xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
	else if (window.XMLHttpRequest)
		xmlHttp = new XMLHttpRequest();
	else
		alert(resourceJSON.msgAjaxNotSupported);
}

function applyteacherjobfrommain(jobId)
{
	var cnt=0;
	var focs=0;	
	$('#errordiv').empty();

	createXMLHttpRequest();  
	queryString = "applyteacherjobfrommain.do?jobId="+jobId+"&dt="+new Date().getTime();
	xmlHttp.open("GET", queryString, false);
	xmlHttp.onreadystatechange = function()
	{
		if(xmlHttp.readyState == 4)
		{
			//alert(xmlHttp.status);
			if(xmlHttp.status==200)	
			{
				var msgStatus = xmlHttp.responseText; 
				//alert(msgStatus);
				if(msgStatus=="")
				setCoverLetter();
				else
				{
				 //window.location.href=msgStatus;
					window.open(
							msgStatus,
							  '_blank' // <- This is what makes it open in a new window.
							);
				}
			}			
			else
			{
				alert(resourceJSON.msgServerErr);
			}
		}
	}
	xmlHttp.send(null);
}

function setCoverLetter(){
	document.getElementById("divCoverLetter").style.display="block";
	$('#myModalCL').modal('show');
	$('#divCoverLetter').find(".jqte_editor").focus();
	document.getElementById("rdoCL1").checked=true;
	
	$('#iconpophover101').tooltip();
	
	$('#errordivCL').empty();
	$('#divCoverLetter').find(".jqte_editor").css("background-color", "");
}
function setCLEnable(){
	$('#divCoverLetter').find(".jqte_editor").html("");
	document.getElementById("divCoverLetter").style.display="block";

	$('#errordivCL').empty();
	$('#divCoverLetter').find(".jqte_editor").css("background-color", "");
}
function setClBlank(){
	$('#divCoverLetter').find(".jqte_editor").html("");
	document.getElementById("divCoverLetter").style.display="none";

	$('#errordivCL').empty();
	$('#divCoverLetter').find(".jqte_editor").css("background-color", "");
}
function checkCL(){
	
	$('#errordivCL').empty();
	$('#divCoverLetter').find(".jqte_editor").css("background-color", "");
	var cnt=0;
	var focs=0;
	if(document.getElementById("rdoCL1").checked==true)
	{
		if ($('#divCoverLetter').find(".jqte_editor").text().trim()==""){
			$('#errordivCL').append("&#149; "+resourceJSON.msgTypeCoverLtr+"<br>");
			if(focs==0)
				$('#divCoverLetter').find(".jqte_editor").focus();
			$('#divCoverLetter').find(".jqte_editor").css("background-color", "#F5E7E1");
			cnt++;focs++;			
		}
	}
	
	if(cnt==0){
		return true;
	}
	return false;
}

function hidestate()
{
	var districtId = $("#districtId").val();
	if(districtId!=null && districtId!="")
	{
		document.getElementById("stateId").disabled=true;
	}
	else
	{
		document.getElementById("stateId").disabled=false;
	}
}

function showSchool()
{
	var districtId = $("#districtId").val();	
	if(districtId.length>0)
	{	
		document.getElementById("schoolName1").disabled=false;
	}else{
		document.getElementById("schoolName1").disabled=true;
	}
}

function applyteacherjobfromquest(jobId)
{	
	//alert(" apply job...");
	var cnt=0;
	var focs=0;	
	$('#errordiv').empty();
	var msgStatus ="";
	createXMLHttpRequest();  
	queryString = "applyteacherjobfromquest.do?jobId="+jobId+"&dt="+new Date().getTime();
	xmlHttp.open("GET", queryString, false);
	xmlHttp.onreadystatechange = function()
	{
		//alert(xmlHttp.readyState);
		//alert(xmlHttp.status);
		if(xmlHttp.readyState == 4)
		{
			//alert(xmlHttp.status);
			if(xmlHttp.status==200)	
			{
				msgStatus = xmlHttp.responseText; 				
			}			
		}
	}	
	xmlHttp.send(null);
	return msgStatus;
}

function jobApplicationHistory(jobID,districtID,exitURL,schoolID)
{
	//alert(" jobApplicationHistory ");
	QuestAjax.jobApplicationHistory(jobID,districtID,exitURL,schoolID,{ 
		async: true,
		callback: function(data){								
		if(data=="")
		{	
			$('#loadingDiv').hide();
			hideDiv();
			/*window.open(
					exitURL,
					  '_blank' 
					);	*/						
				
		}
		else
		{
			$("#signUpServerError").html(data);
			$("#divServerError").show();
			return false;
		}				
		},errorHandler:handleError 
	});	
}

function chkApplyJobNonClient(jobId,appCriteria,exitUrl,districtId,schoolId)
{	
	$('#loadingDiv').show();	
	document.getElementById('tempjobid').value=jobId;
	document.getElementById('district').value=districtId;
	document.getElementById('school').value=schoolId;
	document.getElementById('criteria').value=appCriteria;
	var urlstatus = exitUrl.indexOf("http://");
	var urlstatus1 = exitUrl.indexOf("https://");
	if(urlstatus==-1 && urlstatus1==-1)
	{
		document.getElementById('exitUrl').value="http://"+exitUrl;
	}
	else
	{
		document.getElementById('exitUrl').value=exitUrl;
	}
		
	if(appCriteria==1)
	{
		var cnt=0;
		
		var focs=0;	
		$('#errordiv').empty();
		createXMLHttpRequest();  
		queryString = "chkQuestJobDetails.do?jobId="+jobId+"&dt="+new Date().getTime();
		xmlHttp.open("GET", queryString, false);
		xmlHttp.onreadystatechange = function()
		{
			if(xmlHttp.readyState == 4)
			{
				if(xmlHttp.status==200)	
				{
					var msgStatus = xmlHttp.responseText; 
					
					if(msgStatus==4){					
						$('#divAlert').modal('show');
						cnt++;focs++;
					}
					else if(msgStatus==1){
						$('#errordiv').append("&#149; "+resourceJSON.msgJobnotActive+"<br>");
						if(focs==0)
							$('#fremail5').focus();

						$('#fremail5').css("background-color",txtBgColor);
						cnt++;focs++;
					}
					else if(msgStatus==2){
						$('#errordiv').append("&#149;  "+resourceJSON.msgJobnotActive+"<br>");
						if(focs==0)
							$('#fremail5').focus();

						$('#fremail5').css("background-color",txtBgColor);
						cnt++;focs++;
					}
					else if(msgStatus==3){
						$('#errordiv').append("&#149; "+resourceJSON.msgJobExpired+"<br>");
						if(focs==0)
							$('#fremail5').focus();

						$('#fremail5').css("background-color",txtBgColor);
						cnt++;focs++;
					}
					else if(msgStatus==5)
					{
						cnt++;focs++;		
						$('#loadingDiv').hide();						
						$("#registerModal").modal("show");	
						$('#fname').focus();
					}					
				}				
			}
		}
		xmlHttp.send(null);
		
		if(cnt==0){				
			var jstatus ="";
			jstatus = applyteacherjobfromquest(jobId);
			if(jstatus==1)
			{
				$('#loadingDiv').hide();
				jobApplicationHistory(document.getElementById('tempjobid').value,document.getElementById('district').value,document.getElementById('exitUrl').value,document.getElementById('school').value);
				$("#appliedJob").modal("show");	
			}
			else
			{				
				$('#loadingDiv').hide();
				jobApplicationHistory(jobId,districtId,exitUrl,schoolId);
				//checkPopup(document.getElementById('exitUrl').value);
				$('#applyJobComplete').modal('show');
			}
		}else{
			$('#errordiv').show();
			return false;
		}
	}
	else if(appCriteria==3)
	{		
		var cnt=0;		
		var focs=0;	
		$('#errordiv').empty();
		createXMLHttpRequest();  
		queryString = "chkQuestJobDetails.do?jobId="+jobId+"&dt="+new Date().getTime();
		xmlHttp.open("GET", queryString, false);
		xmlHttp.onreadystatechange = function()
		{
			if(xmlHttp.readyState == 4)
			{
				if(xmlHttp.status==200)	
				{
					var msgStatus = xmlHttp.responseText; 
					
					if(msgStatus==4){					
						$('#divAlert').modal('show');
						cnt++;focs++;
					}
					else if(msgStatus==1){
						$('#errordiv').append("&#149; "+resourceJSON.msgJobnotActive+"<br>");
						if(focs==0)
							$('#fremail5').focus();

						$('#fremail5').css("background-color",txtBgColor);
						cnt++;focs++;
					}
					else if(msgStatus==2){
						$('#errordiv').append("&#149;  "+resourceJSON.msgJobnotActive+"<br>");
						if(focs==0)
							$('#fremail5').focus();

						$('#fremail5').css("background-color",txtBgColor);
						cnt++;focs++;
					}
					else if(msgStatus==3){
						$('#errordiv').append("&#149; "+resourceJSON.msgJobExpired+"<br>");
						if(focs==0)
							$('#fremail5').focus();

						$('#fremail5').css("background-color",txtBgColor);
						cnt++;focs++;
					}
					else if(msgStatus==5)
					{
						cnt++;focs++;
						try
						{						
							$('#loadingDiv').hide();
							jobApplicationHistory(jobId,districtId,exitUrl,schoolId);
							checkPopup(document.getElementById('exitUrl').value);
						}catch(e){alert(e)}													
					}					
				}				
			}
		}
		xmlHttp.send(null);
		
		if(cnt==0){
			var jstatus="";
			jstatus = applyteacherjobfromquest(jobId);			
			if(jstatus==1)
			{				
				$('#loadingDiv').hide();
				jobApplicationHistory(document.getElementById('tempjobid').value,document.getElementById('district').value,document.getElementById('exitUrl').value,document.getElementById('school').value);				
				$("#appliedJob").modal("show");	
				checkPopup(document.getElementById('exitUrl').value);
			}
			else
			{				
				$('#loadingDiv').hide();
			jobApplicationHistory(jobId,districtId,exitUrl,schoolId);	
			//checkPopup(document.getElementById('exitUrl').value);
			$('#applyJobComplete').modal('show');
			}	
		}else{
			$('#errordiv').show();
			return false;
		}
		
	}
	else if(appCriteria==0)
	{
		var cnt=0;
		var focs=0;	
		$('#errordiv').empty();
		createXMLHttpRequest();  
		queryString = "chkJobDetails.do?jobId="+jobId+"&dt="+new Date().getTime();
		xmlHttp.open("GET", queryString, false);
		xmlHttp.onreadystatechange = function()
		{
			if(xmlHttp.readyState == 4)
			{
				if(xmlHttp.status==200)	
				{
					var msgStatus = xmlHttp.responseText; 
					
					if(msgStatus==4){					
						$('#divAlert').modal('show');
						cnt++;focs++;
					}
					else if(msgStatus==1){
						$('#errordiv').append("&#149; "+resourceJSON.msgJobnotActive+"<br>");
						if(focs==0)
							$('#fremail5').focus();

						$('#fremail5').css("background-color",txtBgColor);
						cnt++;focs++;
					}
					else if(msgStatus==2){
						$('#errordiv').append("&#149;  "+resourceJSON.msgJobnotActive+"<br>");
						if(focs==0)
							$('#fremail5').focus();

						$('#fremail5').css("background-color",txtBgColor);
						cnt++;focs++;
					}
					else if(msgStatus==3){
						$('#errordiv').append("&#149; "+resourceJSON.msgJobExpired+"<br>");
						if(focs==0)
							$('#fremail5').focus();

						$('#fremail5').css("background-color",txtBgColor);
						cnt++;focs++;
					}									
				}			
			}
		}
		xmlHttp.send(null);
		
		if(cnt==0){					
			//applyteacherjobfrommain(jobId);			
			jobApplicationHistory(jobId,districtId,exitUrl,schoolId);
			$('#loadingDiv').hide();
			window.open(
					"applyteacherjob.do?jobId="+jobId,
					  '_blank' 
					);
		}else{
			$('#errordiv').show();
			return false;
		}
	}
	else if(appCriteria==2)
	{
		var cnt=0;		
		var focs=0;	
		$('#errordiv').empty();
		createXMLHttpRequest();  
		queryString = "chkQuestJobDetails.do?jobId="+jobId+"&dt="+new Date().getTime();
		xmlHttp.open("GET", queryString, false);
		xmlHttp.onreadystatechange = function()
		{
			if(xmlHttp.readyState == 4)
			{
				if(xmlHttp.status==200)	
				{
					var msgStatus = xmlHttp.responseText; 
					
					if(msgStatus==4){					
						$('#divAlert').modal('show');
						cnt++;focs++;
					}
					else if(msgStatus==1){
						$('#errordiv').append("&#149; "+resourceJSON.msgJobnotActive+"<br>");
						if(focs==0)
							$('#fremail5').focus();

						$('#fremail5').css("background-color",txtBgColor);
						cnt++;focs++;
					}
					else if(msgStatus==2){
						$('#errordiv').append("&#149;  "+resourceJSON.msgJobnotActive+"<br>");
						if(focs==0)
							$('#fremail5').focus();

						$('#fremail5').css("background-color",txtBgColor);
						cnt++;focs++;
					}
					else if(msgStatus==3){
						$('#errordiv').append("&#149; "+resourceJSON.msgJobExpired+"<br>");
						if(focs==0)
							$('#fremail5').focus();

						$('#fremail5').css("background-color",txtBgColor);
						cnt++;focs++;
					}
					else if(msgStatus==5)
					{
						cnt++;focs++;		
						$('#loadingDiv').hide();
						$("#registerModal").modal("show");						
						$('#fname').focus();
					}					
				}				
			}
		}
		xmlHttp.send(null);
		
		if(cnt==0){				
			checkInventoryStatusComplete();
			jstatus = applyteacherjobfromquest(jobId);
			if(jstatus==1)
			{
				$('#loadingDiv').hide();
				jobApplicationHistory(document.getElementById('tempjobid').value,document.getElementById('district').value,document.getElementById('exitUrl').value,document.getElementById('school').value);
				$("#appliedJob").modal("show");
				checkPopup(document.getElementById('exitUrl').value);
			}
			else
			{				
				$('#loadingDiv').hide();
				jobApplicationHistory(jobId,districtId,exitUrl,schoolId);			
				if(document.getElementById('epistatus').value=="complete")
				{
					//checkPopup(document.getElementById('exitUrl').value);
					$('#applyJobComplete').modal('show');
					//checkInventory(0);
				}
				else
				{
					checkInventory(0,jobId);
				}
			}	
			
		}else{
			$('#errordiv').show();
			return false;
		}
	}
	

	
}

function hideDiv()
{
	$('#signUpErrordiv').empty();
	document.getElementById("fname").value = "";
	document.getElementById("lname").value = "";
	document.getElementById("signupEmail").value = "";
	$('#fname').css("background-color","");
	$('#lname').css("background-color","");
	$('#signupEmail').css("background-color","");
	$("#registerModal").modal("hide");
}
function signUpTempUser()
{ 
	//checkInventoryStatusComplete();
	var jstatus="";
	var districtId = "";		
	var other =""
	var firstName = document.getElementById("fname");
	var lastName = document.getElementById("lname");
	var emailAddress = document.getElementById("signupEmail");				
try
{
	var duplicate = 0;
	var cnt=0;
	var focs=0;	
	var checkvalue=0;
	$('#signUpErrordiv').empty();
	$('#signUpServerError').css("background-color","");		
	//$('#districtName').css("background-color","");
	$('#fname').css("background-color","");
	$('#lname').css("background-color","");
	$('#signupEmail').css("background-color","");	
	var emailStatus = checkQuestEmail(emailAddress.value);
	//alert(emailStatus);
	if(trim(firstName.value)=="")
	{
		$('#signUpErrordiv').append("&#149; "+resourceJSON.PlzEtrFirstName+"<br>");
		if(focs==0)
			$('#fname').focus();
		
		$('#fname').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	if(trim(lastName.value)=="")
	{
		$('#signUpErrordiv').append("&#149; "+resourceJSON.PlzEtrLastName+"<br>");
		if(focs==0)
			$('#lname').focus();
		
		$('#lname').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	if(trim(emailAddress.value)=="")
	{
		$('#signUpErrordiv').append("&#149; "+resourceJSON.PlzEtrEmail+"<br>");
		if(focs==0)
			$('#signupEmail').focus();
		
		$('#signupEmail').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	else if(!isEmailAddress(emailAddress.value))
	{		
		$('#signUpErrordiv').append("&#149; "+resourceJSON.PlzEtrVldEmail+"<br>");
		if(focs==0)
			$('#signupEmail').focus();
		
		$('#signupEmail').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	else if(emailStatus=="teacher")
	{
		if(document.getElementById('criteria').value!=2)
		{			
			jstatus = applyteacherjobfromquest(document.getElementById('tempjobid').value);			
			if(jstatus==1)
			{
				$('#loadingDiv').hide();				
				jobApplicationHistory(document.getElementById('tempjobid').value,document.getElementById('district').value,document.getElementById('exitUrl').value,document.getElementById('school').value);
				$("#appliedJob").modal("show");
				checkPopup(document.getElementById('exitUrl').value);
			}
			else
			{
				$('#loadingDiv').hide();
				jobApplicationHistory(document.getElementById('tempjobid').value,document.getElementById('district').value,document.getElementById('exitUrl').value,document.getElementById('school').value);
				//checkPopup(document.getElementById('exitUrl').value);
				$('#applyJobComplete').modal('show');
			}
		}
		else if(document.getElementById('criteria').value == 2)
		{		
			jstatus = applyteacherjobfromquest(document.getElementById('tempjobid').value);			
			if(jstatus==1)
			{
				$('#loadingDiv').hide();
				jobApplicationHistory(document.getElementById('tempjobid').value,document.getElementById('district').value,document.getElementById('exitUrl').value,document.getElementById('school').value);
				$("#appliedJob").modal("show");
				checkPopup(document.getElementById('exitUrl').value);
			}
			else
			{
				$('#loadingDiv').hide();
				jobApplicationHistory(document.getElementById('tempjobid').value,document.getElementById('district').value,document.getElementById('exitUrl').value,document.getElementById('school').value);
				if(document.getElementById('epistatus').value=="complete")
				{
					//checkPopup(document.getElementById('exitUrl').value);
					$('#applyJobComplete').modal('show');
				}
				else
				{
					checkInventory(0,document.getElementById('tempjobid').value);
				}
			}
		}
		cnt++;focs++;
		duplicate = 1;
	}
	else if(emailStatus=="usermaster")
	{
		$('#signUpErrordiv').append("&#149; "+resourceJSON.msgaladyemail+"<br>");
		if(focs==0)
			$('#signupEmail').focus();
		
		$('#signupEmail').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	if(cnt==0 && duplicate==0)
	{			
		try
		{			
			QuestAjax.questSignUpTemp("",trim(firstName.value),trim(lastName.value),trim(emailAddress.value),"",0,0,{ 
				async: true,
				callback: function(data){				
				if(data=="")
				{						
					hideDiv();	
					if(document.getElementById('criteria').value!=2)
					{						
						jstatus = applyteacherjobfromquest(document.getElementById('tempjobid').value);			
						if(jstatus==1)
						{
							$('#loadingDiv').hide();
							jobApplicationHistory(document.getElementById('tempjobid').value,document.getElementById('district').value,document.getElementById('exitUrl').value,document.getElementById('school').value);
							$("#appliedJob").modal("show");
							//checkPopup(document.getElementById('exitUrl').value);
						}
						else
						{
							$('#loadingDiv').hide();
							jobApplicationHistory(document.getElementById('tempjobid').value,document.getElementById('district').value,document.getElementById('exitUrl').value,document.getElementById('school').value);
							//checkPopup(document.getElementById('exitUrl').value);
							$('#applyJobComplete').modal('show');
						}
					}
					else if(document.getElementById('criteria').value == 2)
					{				
						checkInventoryStatusComplete();
						jstatus = applyteacherjobfromquest(document.getElementById('tempjobid').value);			
						if(jstatus==1)
						{
							$('#loadingDiv').hide();
							jobApplicationHistory(document.getElementById('tempjobid').value,document.getElementById('district').value,document.getElementById('exitUrl').value,document.getElementById('school').value);
							$("#appliedJob").modal("show");	
							checkPopup(document.getElementById('exitUrl').value);
						}
						else
						{
							$('#loadingDiv').hide();
							jobApplicationHistory(document.getElementById('tempjobid').value,document.getElementById('district').value,document.getElementById('exitUrl').value,document.getElementById('school').value);
							if(document.getElementById('epistatus').value=="complete")
							{
								//checkPopup(document.getElementById('exitUrl').value);
								$('#applyJobComplete').modal('show');
								//checkInventory(0);
							}
							else
							{
								checkInventory(0,document.getElementById('tempjobid').value);
							}		
						}
					}					
				}
				else
				{
					$("#signUpServerError").html(data);
					$("#divServerError").show();
					return false;
				}				
				},errorHandler:handleError 
			});	
		}catch(e){alert(e)}	
	}
	else if(cnt!=0)
	{
		$('#signUpErrordiv').show();
		return false;
	}
}
catch(e){alert(e);}
}

function checkQuestEmail(emailAddress)
{
	//alert(emailAddress);
	var res;
	createXMLHttpRequest();  
	queryString = "checkemail.do?emailAddress="+emailAddress+"&dt="+new Date().getTime();
	//alert(queryString);
	xmlHttp.open("POST", queryString, false);
	xmlHttp.onreadystatechange = function()
	{
		if(xmlHttp.readyState == 4)
		{
			if(xmlHttp.status==200)	
			{	
				res = xmlHttp.responseText;				
			}			
			/*else
			{
				alert(resourceJSON.msgServerErr);
			}*/
		}
	}	
	xmlHttp.send(null);
	//alert("result :: "+res);
	if(res==1)
	{
		return "teacher";
	}
	else if(res==2)
	{
		return "usermaster";
	}
	else if(res==0)
		return "other";
	
}

function isEmailAddress(str) 
{	

	var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
	
	return emailPattern.test(str);	
}

function checkInventoryStatusComplete()
{
	try{
		DashboardAjax.getInventoryStatus(
		{
			async: false,		
			callback: function(data){		
			if(data)
			 {			
			 	document.getElementById("epistatus").value = "complete";
													
			 }
			else
			 {
				document.getElementById("epistatus").value = "";							
		      	}		
		}
		}		
	);	}catch(e){alert(e)}
}
/* @Start
 * @Ashish Kumar
 * @Description :: Display Selected Cities by state and Get All Sates
 * */
/*function selectCityByState()
{
	var stateId = $("#stateId").val();
	
			DWRAutoComplete.selectedCityByStateId(stateId,{ 
				async: false,		
				callback: function(data){
					document.getElementById("cityDivId").innerHTML=data;
				}
			});
}

function displayState(){

	DWRAutoComplete.displayStateData({ 
		async: false,		
		callback: function(data){
			document.getElementById("stateDivId").innerHTML=data;
		}
	});
}*/
/* @End
 * @Ashish Kumar
 * @Description :: Display Selected Cities by state and Get All Sates
 * */
















///////////////////////////////////////////////
/*var count=0;
var index=-1;
var length=0;
var divid='';
var txtid='';
var hiddenDataArray = new Array();
var showDataArray = new Array();
var degreeTypeArray = new Array();
var hiddenId="";
function getDistrictORSchoolAuto(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("districtORSchoolName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getDistrictORSchoolArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getDistrictORSchoolArray(districtOrSchoolName){
	var searchArray = new Array();
	var JobOrderType	=	document.getElementById("JobOrderType").value;
	if(JobOrderType==2){
		ManageJobOrdersAjax.getFieldOfDistrictList(districtOrSchoolName,{ 
			async: false,
			callback: function(data){
			hiddenDataArray = new Array();
			showDataArray = new Array();
			for(i=0;i<data.length;i++){
				searchArray[i]=data[i].districtName;
				showDataArray[i]=data[i].districtName;
				hiddenDataArray[i]=data[i].districtId;
			}
		},
		errorHandler:handleError 
		});	
	}else {
		var districtHiddenlIdForSchool=0;
		if(document.getElementById("districtHiddenlIdForSchool").value!=null){
			districtHiddenlIdForSchool=document.getElementById("districtHiddenlIdForSchool").value;
		}
		ManageJobOrdersAjax.getFieldOfSchoolList(districtHiddenlIdForSchool,districtOrSchoolName,{ 
			async: false,
			callback: function(data){
			hiddenDataArray = new Array();
			showDataArray = new Array();
			for(i=0;i<data.length;i++){
				searchArray[i]=data[i].schoolName;
				showDataArray[i]=data[i].schoolName;
				hiddenDataArray[i]=data[i].schoolId;
			}
		},
		errorHandler:handleError 
		});	
	}
	
	

	return searchArray;
}


var selectFirst="";

var fatchData= function(txtSearch,searchArray,txtId,txtdivid){
	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
				
			}
			
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}
function hideDistrictMasterDiv(dis,hiddenId,divId)
{
	document.getElementById("districtOrSchooHiddenlId").value="";
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			document.getElementById("districtOrSchooHiddenlId").value=hiddenDataArray[index];
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}

var downArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index<length-1){
			for(var i=0;i<10;i++){
				{
					if(document.getElementById('divResult'+txtdivid+i))
						var div_id=document.getElementById('divResult'+txtdivid+i);
					if(div_id)
					{
						if(div_id.className=='over')
							index=div_id.id.split('divResult'+txtdivid)[1];
						div_id.className='normal';
					}
				}
			}
			index++;
			if(document.getElementById('divResult'+txtdivid+index))
			{
				var div_id=document.getElementById('divResult'+txtdivid+index);
				div_id.className='over';
				document.getElementById(txtid).value = div_id.innerHTML;
				selectFirst=div_id.innerHTML;
			}
		}
	}
}

var upArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index>0){
			for(var i=0;i<length;i++){

				var div_id=document.getElementById('divResult'+txtdivid+i);
				if(div_id)
				{
					if(div_id.className=='over')
						index=div_id.id.split('divResult'+txtdivid)[1];
					div_id.className='normal';
				}
			}
			index--;
			if(document.getElementById('divResult'+txtdivid+index))
				document.getElementById('divResult'+txtdivid+index).className='over';
			if(txtid && document.getElementById('divResult'+txtdivid+index))
			{
				document.getElementById(txtid).value=
					document.getElementById('divResult'+txtdivid+index).innerHTML;
				selectFirst=document.getElementById('divResult'+txtdivid+index).innerHTML;
			}
		}
	}
}

var overText=function (div_value,txtdivid) {

	for(var i=0;i<length;i++)
	{
		if(document.getElementById('divResult'+i))
		{
			var div_id=document.getElementById('divResult'+i);

			if(div_id.className=='over')
				index=div_id.id.split(txtdivid)[1];
			div_id.className='normal';
		}
	}
	div_value.className = 'over';
	document.getElementById(txtid).value= div_value.innerHTML;
}
function trim(s)
{
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}
function checkForInt(evt) {

	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==8)
		return true;

	return( (charCode >= 48 && charCode <= 57)||(charCode==13) );
}
function mouseOverChk(txtdivid,txtboxId)
{	
	for(var i=0;i<length;i++)
	{
		$('#divResult'+txtdivid+i).mouseover({param1: i,param2:txtdivid, param3:txtboxId}, fireMouseOverEvent);
	}
}


function fireMouseOverEvent(event)
{
	for(var i=0;i<length;i++)
	{	
		document.getElementById('divResult'+event.data.param2+i).className='normal';
	}
    document.getElementById('divResult'+event.data.param2+event.data.param1).className='over';	       
   	document.getElementById(event.data.param3).value= $('#divResult'+event.data.param2+event.data.param1).text();
    index=event.data.param1;
}*/


