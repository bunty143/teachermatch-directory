var txtBgColor="#F5E7E1";

var page 			=	1;
var noOfRows 		=	50;
var sortOrderStr	=	"";
var sortOrderType	=	"";

var schoolIdsStore="";
/* @Start
 * @Ashish Kumar
 * @Description :: get School List and Pagination 
 * */


var pageforSchool		=	1;
var noOfRowsSchool 		=	50;
var sortOrderStrSchool	=	"";
var sortOrderTypeSchool	=	"";


var pageZS = 1;
var noOfRowsZS = 10;
var sortOrderStrZS="";
var sortOrderTypeZS="";

function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert("Oops: Your Session has expired!");  document.location = 'signin.do';}
	else{alert("Server Error: "+exception.javaClassName);}
}

function getPaging(pageno)
{
	var gridNo	=	document.getElementById("gridNo").value;
	
	if(gridNo==1)
	{
		if(pageno!='')
		{
			page=pageno;	
		}
		else
		{
			page=1;
		}
		
		noOfRows = document.getElementById("pageSize").value;
	
		searchJob();
	}
	else if(gridNo==2)
	{
		if(pageno!='')
		{
			pageforSchool=pageno;	
		}
		else
		{
			pageforSchool=1;
		}
		
		
		noOfRowsSchool = document.getElementById("pageSize1").value;
		showSchoolList(schoolIdsStore);
	}
	else if(gridNo==3)
	{

		if(pageno!='')
		{
			pageZS=pageno;	
		}
		else
		{
			pageZS=1;
		}		
		noOfRowsZS = document.getElementById("pageSizeZoneSchool").value;
		displayZoneSchoolList();
	}
	
}

function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
	var gridNo	=	document.getElementById("gridNo").value;
	if(gridNo==1)
	{
		if(pageno!=''){
			page=pageno;	
		}else{
			page=1;
		}
		sortOrderStr	=	sortOrder;
		sortOrderType	=	sortOrderTyp;
		if(document.getElementById("pageSize")!=null){
			noOfRows = document.getElementById("pageSize").value;
		}else{
			noOfRows=50;
		}
		searchJob();
	}
	else if(gridNo==2)
	{
		if(pageno!=''){
			pageforSchool=pageno;	
		}else{
			pageforSchool=1;
		}
		sortOrderStrSchool	=	sortOrder;
		sortOrderTypeSchool	=	sortOrderTyp;
		if(document.getElementById("pageSize1")!=null){
			noOfRowsSchool = document.getElementById("pageSize1").value;
		}else{
			noOfRowsSchool=50;
		}
		showSchoolList(schoolIdsStore);
	}
	else if(gridNo==3)
	{
		if(pageno!=''){
			pageforSchool=pageno;	
		}else{
			pageforSchool=1;
		}
		sortOrderStrZS	=	sortOrder;
		sortOrderTypeZS	=	sortOrderTyp;
		if(document.getElementById("pageSizeZoneSchool")!=null){
			noOfRowsZS = document.getElementById("pageSizeZoneSchool").value;
		}else{
			noOfRowsZS=100;
		}
		displayZoneSchoolList();
	}
}

function getSortFirstGrid()
{
	$('#gridNo').val("1");
}

function getSortSecondGrid()
{
	$('#gridNo').val("2");
}


function showSchoolList(schoolIds)
{
	schoolIdsStore=schoolIds;
	DWRAutoComplete.displaySchoolList(schoolIds,noOfRowsSchool,pageforSchool,sortOrderStrSchool,sortOrderTypeSchool,{ 
		async: false,		
		callback: function(data)
		{
			$('#myModalforSchhol').modal('show');
			document.getElementById("schoolListDiv").innerHTML=data;
			applyScrollOnTb();
		}
	});	
}
/* @End
 * @Ashish Kumar
 * @Description :: get School List and Pagination
 * */


var xmlHttp=null;
function createXMLHttpRequest()
{
	if (window.ActiveXObject)
    	xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
   	else if (window.XMLHttpRequest)
		xmlHttp = new XMLHttpRequest();
        else
        	alert("AJAX NOT SUPPORTED BY YOUR BROWSER");
}
function trim(s)
{
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}

function showDiv(divId)
{
	var tempDivId;
	for(var j=0;j<divArray.length;j++)
	{
		tempDivId=document.getElementById(divArray[j]).id;
		document.getElementById(tempDivId).style.display="none";
	}
	if(divId!=null)
	{
		document.getElementById(divId).style.display="block";
	}
}
function showDivMsg(divId,msgArray)
{
	
	var tempDivId;
	for(var j=0;j<msgArray.length;j++)
	{
		tempDivId=document.getElementById(msgArray[j]).id;
		document.getElementById(tempDivId).style.display="none";
	}
	if(divId!=null)
	{
		document.getElementById(divId).style.display="block";
	}
}

function setBlankCss(txtArr)
{
	var tempDivId;
	for(var j=0;j<txtArr.length;j++)
	{	
		tempDivId=document.getElementById(txtArr[j]).id;
		$('#'+tempDivId).css("background-color","");
		document.getElementById(tempDivId).style.display="none";
	}
}

function deleteData(delUrl)
{
	if(window.confirm("Are you sure to delete?"))
	{
		var url = delUrl+"&td="+new Date().getTime();
		window.location.href=url;
	}
}


function showHideBankDiv(stBank,stChequeDD,stButton)
{
	document.getElementById("divBankList").style.display=stBank;
	document.getElementById("divChequeDD").style.display=stChequeDD;
	document.getElementById("divPayButton").style.display=stButton;
}

function isNumber(field) {
	var re = /^[0-9-'.'-',']*$/;
	//var re =/^(0\.|[1-9]\d*\.)d{2}/ 
	if (!re.test(field.value)) {
//		alert('Value must be all numberic charcters, including "." or "," non numerics will be removed from field!');
		field.value = field.value.replace(/[^0-9-'.'-',']/g,"");
	}
}
function checkForInt(evt) {
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==8)
		return true;

	return( (charCode >= 48 && charCode <= 57)||(charCode==13) );
}

function setFocus(id)
{
	if(id!="")
		document.getElementById(id).focus();
}

function isEmailAddress(str) 
{	
	var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
	if(str.indexOf('teachermatch.org')>-1 || str.indexOf('teachermatch.com')>-1){
		emailPattern = /^[a-zA-Z0-9._+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
	}
	return emailPattern.test(str);	
}

function chkPwdPtrn(str)
{
	var pat1 = /[^a-zA-Z]/g; //If any spcl char find it return true otherwise false
	var pat2 = /[a-zA-Z]/g;  //If any alphabet found it return true otherwise false
	//alert(pat1.test(str) && pat2.test(str))
	return pat1.test(str) && pat2.test(str);
}
function hideDive()
{
	$("#teacherblockdiv").hide()
	$('#emailAddress').focus();
}

function loginUserValidate()
{
	
	//$("p").addClass("myClass yourClass");
	var emailAddress = document.getElementById("emailAddress");
	var password = document.getElementById("password");
	
	
	var cnt=0;
	var focs=0;	
	$('#errordiv').empty();
	
	document.getElementById("divServerError").style.display="none";	
	$('#emailAddress').css("background-color","");
	$('#password').css("background-color","");
	
	
	if(trim(emailAddress.value) == '')
	{
		$('#errordiv').append("&#149; "+resourceJSON.PlzEtrEmail+"<br>");
		if(focs==0)
			$('#emailAddress').focus();
		
		$('#emailAddress').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	if(trim(emailAddress.value) != '' && !isEmailAddress(trim(emailAddress.value)))
	{		
		$('#errordiv').append("&#149; "+resourceJSON.PlzEtrVldEmail+"<br>");
		if(focs==0)
			$('#emailAddress').focus();
		$('#emailAddress').css("background-color", txtBgColor);
		cnt++;focs++;
	}
	if(trim(password.value) == '')
	{
		$('#errordiv').append("&#149; "+resourceJSON.msgPassword+"<br>");
		if(focs==0)
			$('#password').focus();
		$('#password').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	
	if(cnt==0)
		return true;
	else
	{
		$('#errordiv').show();
		return false;
	}
}

function validateSignUp()
{	
	var firstName = document.getElementById("firstName");
	var lastName = document.getElementById("lastName");
	var emailAddress = document.getElementById("emailAddress");
	var password = document.getElementById("password");
	var branchName = null;
	if(document.getElementById("branchName")){
		branchName = document.getElementById("branchName").value;
		$('#branchName').css("background-color","");
	}
	var branchId = null;
	if(document.getElementById("branchId")){
		branchId = document.getElementById("branchId").value;
		$('#branchId').css("background-color","");
	}
	var cnt=0;
	var focs=0;	
	$('#errordiv').empty();
	$('#divServerError').css("background-color","");
	
	$('#firstName').css("background-color","");
	$('#lastName').css("background-color","");
	$('#emailAddress').css("background-color","");
	$('#password').css("background-color","");
	if(trim(firstName.value)=="")
	{
		$('#errordiv').append("&#149; "+resourceJSON.PlzEtrFirstName+"<br>");
		if(focs==0)
			$('#firstName').focus();
		
		$('#firstName').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	if(trim(lastName.value)=="")
	{
		$('#errordiv').append("&#149; "+resourceJSON.PlzEtrLastName+"<br>");
		if(focs==0)
			$('#lastName').focus();
		
		$('#lastName').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	if(trim(emailAddress.value)=="")
	{
		$('#errordiv').append("&#149; "+resourceJSON.PlzEtrEmail+"<br>");
		if(focs==0)
			$('#emailAddress').focus();
		
		$('#emailAddress').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	else if(!isEmailAddress(emailAddress.value))
	{		
		$('#errordiv').append("&#149; "+resourceJSON.PlzEtrVldEmail+"<br>");
		if(focs==0)
			$('#emailAddress').focus();
		
		$('#emailAddress').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	else if(checkEmail(emailAddress.value))
	{
		//$('#errordiv').append("&#149; "+resourceJSON.lblerrormessage+"<br>");
		if(focs==0)
			$('#emailAddress').focus();
		
		$('#emailAddress').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	if(trim(password.value)=="") 
	{
		$('#errordiv').append("&#149; "+resourceJSON.msgPassword+"<br>");
		if(focs==0)
			$('#password').focus();
		
		$('#password').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	else if(!chkPwdPtrn(password.value) || password.value.length<8 )
	{		
		$('#errordiv').append("&#149; "+resourceJSON.msgPassCombination8To12Char+"<br>");
		if(focs==0)
			$('#password').focus();
		
		$('#password').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	if(document.getElementById("branchName"))
	{
		if(branchName==null || branchName=="")
		{
			$('#errordiv').append("&#149; Please enter Branch Name<br>");
			if(focs==0)
				$('#branchName').focus();
			
			$('#branchName').css("background-color",txtBgColor);
			cnt++;focs++;
		}
		else if(branchId==null || branchId=="")
		{
			$('#errordiv').append("&#149; Please enter valid Branch Name<br>");
			if(focs==0)
				$('#branchName').focus();
			
			$('#branchName').css("background-color",txtBgColor);
			cnt++;focs++;
		}
		if(cnt==0 && branchId!=null && branchId!="")
		{
			BranchesAjax.getBranchJob(branchId,{
				async: false,
				callback: function(data)
				{
					if(data==0){
						$('#errordiv').append("&#149; Branch "+branchName+" is not currently available for the SmartPractices professional development series. Please select another branch or contact a branch administrator.<br>");
						if(focs==0)
							$('#branchName').focus();
						
						$('#branchName').css("background-color",txtBgColor);
						cnt++;focs++;
					}
					else{
						document.getElementById("jobId").value=data;
					}
				},
				errorHandler:handleError 
			});
		}
	}
	if(document.getElementById("captcharesponse").value=="failed" || document.getElementById("captcharesponse").value=='')
	{
		$('#errordiv').append("&#149; "+resourceJSON.msgCheckboxCaptchaRequired+" <br>");		
		cnt++;focs++;
	}	
	if(cnt==0)
	{
		$('#loadingDiv').show();
		return true;
	}
	else
	{
		$('#errordiv').show();
		return false;
	}
}

function chkForgotpwd()
{
	var emailAddress = document.getElementById("emailAddress");
	var sumOfCaptchaText = document.getElementById("sumOfCaptchaText");
	
	var cnt=0;
	var focs=0;	
	$('#errordiv').empty();
	$('#divServerError').css("background-color","");
	$('#emailAddress').css("background-color","");
	$('#sumOfCaptchaText').css("background-color","");
	
	
	if(trim(emailAddress.value)=="")
	{
		$('#errordiv').append("&#149; "+resourceJSON.PlzEtrEmail+"<br>");
		if(focs==0)
			$('#emailAddress').focus();
		
		$('#emailAddress').css("background-color",txtBgColor);
		cnt++;focs++;
		
	}
	else if(!isEmailAddress(emailAddress.value))
	{		
		$('#errordiv').append("&#149; "+resourceJSON.PlzEtrVldEmail+"<br>");
		if(focs==0)
			$('#emailAddress').focus();
		
		$('#emailAddress').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	if(trim(sumOfCaptchaText.value)=="")
	{
		$('#errordiv').append("&#149; "+resourceJSON.msgAddNumbers+"<br>");
		if(focs==0)
			$('#sumOfCaptchaText').focus();
		
		$('#sumOfCaptchaText').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	else if(chkCaptcha())
	{
		$('#errordiv').append("&#149; "+resourceJSON.msgSumNumbersWrong+"<br>");
		if(focs==0)
			$('#sumOfCaptchaText').focus();
		
		$('#sumOfCaptchaText').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	
	if(cnt==0)
	{		
		$('#loadingDiv').show();
		return true;
	}
	else
	{
		changeCaptcha(null);
		$('#errordiv').show();
		return false;
	}
	
}


function chkResetpwd()
{
	var txtPwd = document.getElementById("txtPwd");
	var txtRePwd = document.getElementById("txtRePwd");
	
	var cnt=0;
	var focs=0;	
	$('#errordiv').empty();
	$('#divServerError').css("background-color","");
	$('#txtPwd').css("background-color","");
	$('#txtRePwd').css("background-color","");
	
	
	if(txtPwd.value=="")
	{
		$('#errordiv').append("&#149; "+resourceJSON.msgPleaseenterNewPassword+"<br>");
		if(focs==0)
			$('#txtPwd').focus();
		
		$('#txtPwd').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	else if(!chkPwdPtrn(txtPwd.value) || txtPwd.value.length<8 )
	{		
		$('#errordiv').append("&#149; "+resourceJSON.msgPassCombination8To12Char+"<br>");
		if(focs==0)
			$('#txtPwd').focus();
		
		$('#txtPwd').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	
	if(txtRePwd.value=="")
	{
		$('#errordiv').append("&#149; "+resourceJSON.msgEnterPassword+"<br>");
		if(focs==0)
			$('#txtPwd').focus();
		
		;
		$('#txtRePwd').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	
	if(( (txtPwd.value!="" && txtRePwd.value!="" ) && !(txtPwd.value==txtRePwd.value)))
	{
		$('#errordiv').append("&#149; "+resourceJSON.msgEnterPasswordMatch+"<br>");
		if(focs==0)
			$('#sumOfCaptchaText').focus();
		
		$('#txtPwd').css("background-color",txtBgColor)
		$('#txtRePwd').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	
	if(cnt==0)
		return true;
	else
	{
		$('#errordiv').show();
		return false;
	}
}






function checkEmail(emailAddress)
{
	var res;
	createXMLHttpRequest();  
	queryString = "chkemail.do?emailAddress="+emailAddress+"&dt="+new Date().getTime();
	xmlHttp.open("POST", queryString, false);
	xmlHttp.onreadystatechange = function()
	{
		if(xmlHttp.readyState == 4)
		{
			if(xmlHttp.status==200)	
			{			
				res = xmlHttp.responseText;				
			}			
			else
			{
				alert(resourceJSON.msgServerErr);
			}
		}
	}
	xmlHttp.send(null);  
	 if(res==1){
	  $('#errordiv').append("&#149; A Member has already registered with the email address you provided. Please provide another email address to register.<br>");
	  return true;
	 }else if(res==2)
	 {
	  $('#errordiv').append("&#149; This email address is assigned to a district/school user. In order to apply for a job, you will need to use a personal email address.<br>");
	  return true;
	 }
	 else
	  return false;
	}
function chkCaptcha()
{
	
	var sumOfCaptchaText = document.getElementById("sumOfCaptchaText").value;
	
	var res;
	createXMLHttpRequest();  
	queryString = "chkCaptcha.do?sumOfCaptchaText="+sumOfCaptchaText+"&dt="+new Date().getTime();
	xmlHttp.open("POST", queryString, false);
	xmlHttp.onreadystatechange = function()
	{
		if(xmlHttp.readyState == 4)
		{
			if(xmlHttp.status==200)	
			{			
				res = xmlHttp.responseText;
				
			}			
			else
			{
				alert(resourceJSON.msgServerErr);
			}
		}
	}
	xmlHttp.send(null);		
	if(res==1)
		return true;
	else
		return false;
}
function changeCaptcha(focusId)
{	
	//document.getElementById("imgCaptcha").src="verify.png";	
	$("#imgCaptcha").attr('src', 'verify.png?time=' + new Date().getTime());
	var fid = document.getElementById(focusId);
	if(fid!=null && fid!=undefined)
	{
		fid.focus();	
	}
}


function PasswordImage(){
   var control = document.getElementById("password");
   var myString= control.value; 
   var Stringlen = myString.length;
   var ValidateDigits = /[^0-9]/g;
   var ValidateSpChar = /[a-zA-Z0-9]/g;
   var ValidateChar = /[^a-zA-Z]/g;
   var digitString = myString.replace(ValidateDigits , "");
   var specialString = myString.replace(ValidateSpChar, "");
   var charString = myString.replace(ValidateChar, "");


   if(!specialString < 1 && !digitString < 1 && !charString < 1 && Stringlen>=8 ){
 	  
  	   document.getElementById('d8').style.display='none';
       document.getElementById('d9').style.display='none';
       document.getElementById('d4').style.backgroundColor = 'lightgreen';
       document.getElementById('d5').style.backgroundColor = 'lightgreen';
       document.getElementById('d6').style.display='inline';
       document.getElementById('d7').style.display='inline';
	   document.getElementById('d10').style.display='inline'; 
	   control.focus();
   }else if(!charString  < 1 && !digitString < 1 &&Stringlen>=8 ||!charString  < 1  &&!specialString < 1 && Stringlen>=8){
 	   
       document.getElementById('d6').style.display='none';
       document.getElementById('d7').style.display='none';
       document.getElementById('d10').style.display='none';       
       document.getElementById('d8').style.display='none';
	   document.getElementById('d4').style.display='inline';
       document.getElementById('d9').style.display='inline';
       document.getElementById('d5').style.display='inline';
	   document.getElementById('d4').style.backgroundColor = 'yellow';
       document.getElementById('d5').style.backgroundColor = 'yellow';
       
   
   }else if( !specialString < 1 || !digitString < 1 || !charString < 1 || !charString && !digitString < 1 && Stringlen<6   ||!charString &&!specialString < 1 && Stringlen<6 ){
   	 
   	   document.getElementById('d5').style.display='none';
  	   document.getElementById('d6').style.display='none';
       document.getElementById('d7').style.display='none';
       document.getElementById('d9').style.display='none';
       document.getElementById('d10').style.display='none';       
	   document.getElementById('d4').style.display='inline';
       document.getElementById('d4').style.backgroundColor = 'red';
       document.getElementById('d8').style.display='inline';
	   control.focus();
   }else if(Stringlen<1){
	   document.getElementById('d4').style.display='none';
	   document.getElementById('d5').style.display='none';
  	   document.getElementById('d6').style.display='none';
       document.getElementById('d7').style.display='none';
  	   document.getElementById('d8').style.display='none';
       document.getElementById('d9').style.display='none';
       document.getElementById('d10').style.display='none';       
      
   }
 }
function chkForEnterJobSearch(evt)
{
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==13)
	{
		//shadab commenting lines to use runscript
		//searchJob();
	}	
}
// zone wise search by mukesh
function searchJob()
{
	//alert(" tmhome.js=>searchJob() ");
	var headQuarterId =0;
	try {
		headQuarterId=document.getElementById("headQuarterId").value;
	} catch (e) {
		// TODO: handle exception
	}
	var branchId =0;
	try {
		branchId = document.getElementById("branchId").value;
	} catch (e) {
		// TODO: handle exception
	}
	var districtId =0;
	try {
		districtId = document.getElementById("districtId").value;
	} catch (e) {
		// TODO: handle exception
	}
	
	//alert("headQuarterId "+headQuarterId);
	//alert("branchId "+branchId);
	//alert("districtId "+districtId);
	
	//alert(document.getElementById("zone").value);	  
      
	var stateId = 0;
	try {
		stateId=document.getElementById("stateId").value;
	} catch (e) {
		// TODO: handle exception
	}
	//alert("stateId "+stateId);
	
	var geoZoneId = 0;
	try {
		geoZoneId=document.getElementById("zone").value;
	} catch (e) {
		// TODO: handle exception
	}
	
	tpJbIDisable();
	createXMLHttpRequest();  
	//var districtId = document.getElementById("districtId").value;
	var schoolId ="";
	
	try {
		schoolId=document.getElementById("schoolId").value;
	} catch (e) {
		// TODO: handle exception
	}
	var jobCategoryId = document.getElementById("jobCategoryId").value;
	var jobSubCategoryId = document.getElementById("jobSubCategoryId").value;
	var zipCode = document.getElementById("zipCode").value;
	var redirectTo = document.getElementById("redirectTo").value;
	var subjectId=null;
	try {
		subjectId= document.getElementById('subjectId');
	} catch (e) {
		// TODO: handle exception
	}
	
	var cityName	=	0;	//document.getElementById('cityId').value;
	var url = window.location.pathname;  
	var pageName = url.substring(url.lastIndexOf('/') + 1);
	
	var gridNo = "";
	try{
		gridNo	=	document.getElementById("gridNo").value;
	}catch(e){}
	
	/* @Start
	 * @Ashish
	 * @Description :: Get selected subject IDs
	 * */
	var subjectIdList = "";
	
	if(subjectId!=null && subjectId.options!=null)
	{
		//alert("subjectId "+subjectId.options.length);
		for(var i=0; i<subjectId.options.length;i++)
		{	
			if(subjectId.options[i].selected == true){
				subjectIdList = subjectIdList+subjectId.options[i].value+",";
			}
		}
	}
	else
	{
		//alert("subjectId is null ");
	}
	
	
	/* @End
	 * @Ashish
	 * @Description :: Get selected subject IDs
	 * */
	 $('#loadingDiv').fadeIn();
	    $.ajax({
	    	type: "POST",
	    	url: "getsearchjob.do",
	    	data: "districtId="+districtId+"&schoolId="+schoolId+"&jobCategoryId="+jobCategoryId+"&zipCode="+zipCode+"&redirectTo="+redirectTo+"&noOfRows="+noOfRows+"&page="+page+"&sortOrderStr="+sortOrderStr+"&sortOrderType="+sortOrderType+"&dt="+new Date().getTime()+"&subjectIdList="+subjectIdList+"&cityName="+cityName+"&geoZoneId="+geoZoneId+"&pageName="+pageName+"&headQuarterId="+headQuarterId+"&branchId="+branchId+"&stateId="+stateId+"&gridNo="+gridNo,
	    	success: function(result){
	       	$("#divJobsBoard").html(result);
	        $('#loadingDiv').hide();
	        applyScrollOnTbl();
	        tpJbIEnable();     
	    },error: function(e) { 
	        alert("Error   ::"+e.massage); 
	    }});

}
function tpJbIEnable()
{
	var noOrRow = 0;
	try {
		document.getElementById("tblGrid").rows.length;
	} catch (e) {
		// TODO: handle exception
	}
	
	for(var j=1;j<=noOrRow;j++)
	{	
		$('#iconpophover4').tooltip(); // Ashish add for get tooltip
		$('#tpApply'+j).tooltip();
		$('#tpShare'+j).tooltip();
		
	}
	
}
function tpJbIDisable()
{
	if(document.getElementById("tblGrid")==null)
		return;
	var noOrRow = document.getElementById("tblGrid").rows.length;
	
	for(var j=1;j<=noOrRow;j++)
	{	
		$('#tpApply'+j).trigger('mouseout');
		$('#tpShare'+j).trigger('mouseout');
	}
}
function showJobAlertForm()
{	
	document.getElementById("senderName").value = trim(document.getElementById("hdnSenderName").value); 
	document.getElementById("senderEmail").value = document.getElementById("hdnSenderEmail").value;
	
	document.getElementById("districtName1").value = document.getElementById("hdnDistrictName").value;
	document.getElementById("schoolName1").value = document.getElementById("hdnSchoolName").value;
	document.getElementById("jobCategoryId1").value = "0"; 
	 document.getElementById("zipCode1").value = "";
	
	if(document.getElementById("schoolName1").value=="")
	{
		document.getElementById("zipCode1").readOnly = false;
	}
	else
	{
		document.getElementById("zipCode1").value = "";	
		document.getElementById("zipCode1").readOnly = true;
	}
	
	$('#myModal').modal('show');
	document.getElementById("senderName").focus();

}


function saveJobAlert()
{
	var senderName = document.getElementById("senderName");
	var senderEmail = document.getElementById("senderEmail");
	var districtId = document.getElementById("districtId1");
	var schoolId = document.getElementById("schoolId1");
	var jobCategoryId = document.getElementById("jobCategoryId1");
	var zipCode = document.getElementById("zipCode1");
	var cnt=0;
	var focs=0;	
	$('#errordiv').empty();
		
	$('#senderName').css("background-color","");
	$('#senderEmail').css("background-color","");
	
	
	if(trim(senderName.value) == '')
	{
		$('#errordiv').append("&#149; "+resourceJSON.msgPleaseEnterName+"<br>");
		if(focs==0)
			$('#senderName').focus();
		
		$('#senderName').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	if(trim(senderEmail.value) == '')
	{
		$('#errordiv').append("&#149; "+resourceJSON.msgPleaseEnterEmailAdd+"<br>");
		if(focs==0)
			$('#senderEmail').focus();
		
		$('#senderEmail').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	else if(trim(senderEmail.value) != '' && !isEmailAddress(trim(senderEmail.value)))
	{		
		$('#errordiv').append("&#149; "+resourceJSON.msgValidEmailAddress+"<br>");
		if(focs==0)
			$('#senderEmail').focus();
		$('#senderEmail').css("background-color", txtBgColor);
		cnt++;focs++;
	}
	
	
	if(cnt==0){
		queryString = "saveJobAlert.do?senderName="+senderName.value+"&senderEmail="+senderEmail.value+"&districtId="+districtId.value+"&schoolId="+schoolId.value+"&jobCategoryId="+jobCategoryId.value+"&zipCode="+zipCode.value+"&redirectTo="+redirectTo.value+"&dt="+new Date().getTime();
	xmlHttp.open("POST", queryString, false);
	xmlHttp.onreadystatechange = function()
	{
		if(xmlHttp.readyState == 4)
		{
			if(xmlHttp.status==200)	
			{		
				$('#myModal').modal('hide');	
				$('#message2show').html(resourceJSON.msgRegisteredJob);
				$('#myModal2').modal('show');
			}			
			else
			{
				alert(resourceJSON.msgServerErr);
			}
		}
	}
	xmlHttp.send(null);	
	}		
	else
	{
		$('#errordiv').show();
		return false;
	}
}

function chkForEnter(evt)
{	
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==13)
	{
		saveJobAlert();
	}	
}
function setBlankZip()
{
	if(document.getElementById("schoolName1").value=="")
	{
		document.getElementById("zipCode1").readOnly = false;
	}
	else
	{
		document.getElementById("zipCode1").value = "";	
		document.getElementById("zipCode1").readOnly = true;
	}
	
}

/*mathod for Anchor tag on Zone*/
var DistrictId;
function displayZoneSchoolList()
{
	var geozoneId=$('#geozoneId').val();	
	ManageJobOrdersAjax.displayGeoZoneSchoolGrid(geozoneId,DistrictId,noOfRowsZS,pageZS,sortOrderStrZS,sortOrderTypeZS,{ 
		async: true,
		callback: function(data){
		$('#gridNo').val("3");
		$('#geozoneId').val(geozoneId);
		$('#geozoneschoolFlag').val(1);
		$("#geozoneschoolDiv").modal('show');
		$("#geozoneschoolGrid").html(data);
		applyScrollOnTblGeoZoneSchool();	
	},
	errorHandler:handleError 
	});
}

function showZoneSchoolList(geozoneId,geozoneName,districtId)
{
	DistrictId=districtId;
	pageZS=1;
	noOfRowsZS = 10;
	$("#zoneName").html(geozoneName);
	$('#loadingDiv').show();	
	ManageJobOrdersAjax.displayGeoZoneSchoolGrid(geozoneId,districtId,noOfRowsZS,pageZS,sortOrderStrZS,sortOrderTypeZS,{ 
		async: true,
		callback: function(data){		
		$('#gridNo').val("3");
		$('#geozoneId').val(geozoneId);
		$('#geozoneschoolFlag').val(1);
		$("#geozoneschoolDiv").modal('show');
		$("#geozoneschoolGrid").html(data);
		applyScrollOnTblGeoZoneSchool();
		$('#loadingDiv').hide();
	},
	errorHandler:handleError 
	});
}


function showZoneSchoolPics(geozoneId,geozoneName)
{
	$("#zoneName").html(geozoneName);
	$('#loadingDiv').show();	
	ManageJobOrdersAjax.displayGeoZoneSchoolPics(geozoneId,{ 
		async: true,
		callback: function(data){		
		$('#gridNo').val("3");
		$('#geozoneId').val(geozoneId);
		$('#geozoneschoolFlag').val(1);
		$("#geozoneschoolDiv").modal('show');
		$("#geozoneschoolGrid").html(data);
		applyScrollOnTblGeoZoneSchool();
		$('#loadingDiv').hide();
	},
	errorHandler:handleError 
	});
}
//======================= Mukesh ================================
function validateCMSSignUp()
{	
	var  eventId=document.getElementById("eventId").value;
	var firstName = document.getElementById("firstName");
	var lastName = document.getElementById("lastName");
	var emailAddress = document.getElementById("emailAddress");
	var password = document.getElementById("password");
//	var sumOfCaptchaText = document.getElementById("sumOfCaptchaText");
	
	
	var cnt=0;
	var focs=0;	
	$('#errordiv').empty();
	$('#divServerError').css("background-color","");
	
	$('#firstName').css("background-color","");
	$('#lastName').css("background-color","");
	$('#emailAddress').css("background-color","");
	$('#password').css("background-color","");
	//$('#sumOfCaptchaText').css("background-color","");

		
	if(trim(firstName.value)=="")
	{
		$('#errordiv').append("&#149; "+resourceJSON.PlzEtrFirstName+"<br>");
		if(focs==0)
			$('#firstName').focus();
		
		$('#firstName').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	if(trim(lastName.value)=="")
	{
		$('#errordiv').append("&#149; "+resourceJSON.PlzEtrLastName+"<br>");
		if(focs==0)
			$('#lastName').focus();
		
		$('#lastName').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	if(trim(emailAddress.value)=="")
	{
		$('#errordiv').append("&#149; "+resourceJSON.PlzEtrEmail+"<br>");
		if(focs==0)
			$('#emailAddress').focus();
		
		$('#emailAddress').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	else if(!isEmailAddress(emailAddress.value))
	{		
		$('#errordiv').append("&#149; "+resourceJSON.PlzEtrVldEmail+"<br>");
		if(focs==0)
			$('#emailAddress').focus();
		
		$('#emailAddress').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	else if(checkEmailCMS(emailAddress.value,eventId))
	{
		$('#errordiv').append("&#149;"+resourceJSON.msgAlreadyRegisteredEvent+"<br>");
		if(focs==0)
			$('#emailAddress').focus();
		
		$('#emailAddress').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	if(trim(password.value)=="") 
	{
		$('#errordiv').append("&#149; "+resourceJSON.msgPassword+"<br>");
		if(focs==0)
			$('#password').focus();
		
		$('#password').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	else if(!chkPwdPtrn(password.value) || password.value.length<8 )
	{		
		$('#errordiv').append("&#149; "+resourceJSON.msgPassCombination8To12Char+"<br>");
		if(focs==0)
			$('#password').focus();
		
		$('#password').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	if(document.getElementById("captcharesponse").value=="failed" || document.getElementById("captcharesponse").value=='')
	{
		$('#errordiv').append("&#149; "+resourceJSON.msgCheckboxCaptchaRequired+" <br>");		
		cnt++;focs++;
	}	
	/*if(trim(sumOfCaptchaText.value)=="")
	{
		$('#errordiv').append("&#149; Please Add both the numbers<br>");
		if(focs==0)
			$('#sumOfCaptchaText').focus();
		
		$('#sumOfCaptchaText').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	else if(chkCaptcha())
	{
		$('#errordiv').append("&#149; Sum of both the numbers is wrong, please enter again<br>");
		if(focs==0)
			$('#sumOfCaptchaText').focus();
		
		$('#sumOfCaptchaText').css("background-color",txtBgColor);
		cnt++;focs++;
	}*/
	
	if(cnt==0)
	{
		$('#loadingDiv').show();
		return true;
	}
	else
	{
		changeCaptcha(null);
		$('#errordiv').show();
		return false;
	}
	
}


function checkEmailCMS(emailAddress,eventId)
{
	var res;
	createXMLHttpRequest();  
	queryString = "cmschekemail.do?emailAddress="+emailAddress+"&eventId="+eventId+"&dt="+new Date().getTime();
	xmlHttp.open("POST", queryString, false);
	xmlHttp.onreadystatechange = function()
	{
		if(xmlHttp.readyState == 4)
		{
			if(xmlHttp.status==200)	
			{			
				res = xmlHttp.responseText;				
			}			
			else
			{
				alert(resourceJSON.msgServerErr);
			}
		}
	}
	xmlHttp.send(null);		
	if(res==1)
		return true;
	else
		return false;
}

function getDistrictVisibleByBranch()
{
		var branchId =0;
		try {
			branchId=document.getElementById("branchId").value;
		} catch (e) {
			// TODO: handle exception
		}
		
		var isBranchExist =0;
		try {
			isBranchExist=document.getElementById("isBranchExist").value;
		} catch (e) {
			// TODO: handle exception
		}
		
		if(branchId=0 || branchId=='')
		{
			$("#districtName").attr("disabled", "disabled"); 
		}
		else
		{
			$("#districtName").attr("disabled", null); 
		}
		
		if(isBranchExist==0)
		{
			$("#districtName").attr("disabled", null); 
		}
}

//shadab start for districtjobboard search
function runScript(characterCode){
	//alert(characterCode.keyCode);
	if(characterCode.keyCode == 13)
	{
		page = 1;
		esSearchdistrictjobboard();
		return false;
	}
	
}

function esSearchdistrictjobboard()
{
	var headQuarterId =0;
	try {
		headQuarterId=document.getElementById("headQuarterId").value;
	} catch (e) {
		// TODO: handle exception
	}
	var branchId =0;
	try {
		branchId = document.getElementById("branchId").value;
	} catch (e) {
		// TODO: handle exception
	}
	var districtId =0;
	try {
		districtId = document.getElementById("districtId").value;
	} catch (e) {
		// TODO: handle exception
	}
	
	var stateId = 0;
	try {
		stateId=document.getElementById("stateId").value;
	} catch (e) {
		// TODO: handle exception
	}
	
	//var stateName = $("#stateId option:selected").text();
	//alert(document.getElementById("zone").value);
	//var geoZoneId = document.getElementById("zone").value;
//	var geoZoneName = $("#zone option:selected").text();//document.getElementById("zone").text;
	
	var geoZoneName = "";
	try {
		geoZoneName=$("#zone option:selected").text();
	} catch (e) {
		// TODO: handle exception
	}
	
	tpJbIDisable();
	createXMLHttpRequest();  
	//var schoolId = document.getElementById("schoolId").value;
	//var schoolName = document.getElementById("schoolName").value;
	var schoolName="";
	try {
		schoolName=document.getElementById("schoolName").value;
	} catch (e) {
		// TODO: handle exception
	}
	
	var jobCategoryName = $("#jobCategoryId option:selected").text();//document.getElementById("jobCategoryId").value;
	var jobCategoryId = document.getElementById("jobCategoryId").value;
	var zipCode="";
	try
	{
		 zipCode = document.getElementById("zipCode").value;
	}
	catch(e){}
	
	var redirectTo = document.getElementById("redirectTo").value;
	//var subjectId = document.getElementById('subjectId');
	var subjectId=null;
	try {
		subjectId= document.getElementById('subjectId');
	} catch (e) {
		// TODO: handle exception
	}
	
	//var cityName	=	0;	//document.getElementById('cityId').value;
	var subjectIdList = "";
	if(subjectId!=null && subjectId.options!=null)
	{
		for(var i=0; i<subjectId.options.length;i++)
		{	
			if(subjectId.options[i].selected == true){
				subjectIdList = subjectIdList+subjectId.options[i].value+" ";
			}
		}
	}
	
	var jobSubCategoryId=0;
	try{
		jobSubCategoryId=document.getElementById("jobSubCategoryId").value;
		document.getElementById("subjobCategoryhiddenId").value=jobSubCategoryId;
	}catch(ee){}
	if(jobSubCategoryId!=0){
		jobSubCategoryId = document.getElementById("jobSubCategoryId").value.split("||")[0];
		jobCategoryName=$("#jobSubCategoryId option:selected").text();
		vrrv = document.getElementById("jobSubCategoryId").value
		document.getElementById("subjobCategoryhiddenId").value=vrrv;
		
	}
	
	/*if(jobSubCategoryId=="")
		jobSubCategoryId = 0;
	*/
	var positionStart="";
	try{
		positionStart=document.getElementById("positionStart").value;
	}catch(ee){}
	if(positionStart===undefined)
	{
		positionStart="";
	}
	
	//Gaurav Kumar
	
	    var gradeLevel="";
	    try{
		gradeLevel=document.getElementById("gradeLevel").value;
	    }catch(ee){}
	    if(gradeLevel===undefined)
		{
	    	gradeLevel="";
		}
	
	var talentRef="";
	var teacherId="";
	try{
		talentRef = document.getElementById("talentRef").value;
		teacherId=document.getElementById("teacherId").value;
	}catch(e){}
	var requestType=$("#requestType").val();
	//queryString = "esSearchdistrictjobboard.do?districtId="+districtId+"&schoolName='"+schoolName+"'&jobCategoryName='"+jobCategoryName+"'&zipCode='"+zipCode+"'&redirectTo='"+redirectTo+"'&noOfRows='"+noOfRows+"'&page='"+page+"'&sortOrderStr='"+sortOrderStr+"'&sortOrderType='"+sortOrderType+"'&dt='"+new Date().getTime()+"'&subjectIdList='"+subjectIdList+"'&geoZoneName='"+geoZoneName+"'";; // @Ashish :: add subjectIdList parameter
	queryString = "esSearchdistrictjobboard.do?districtId="+districtId+"&schoolName="+schoolName+"&jobCategoryName="+jobCategoryName+"&zipCode="+zipCode+"&redirectTo="+redirectTo+"&noOfRows="+noOfRows+"&page="+page+"&sortOrderStr="+sortOrderStr+"&sortOrderType="+sortOrderType+"&dt="+new Date().getTime()+"&subjectNameList="+subjectIdList+"&geoZoneName="+geoZoneName+"&searchTerm="+$("#searchTerm").val()+"&headQuarterId="+headQuarterId+"&branchId="+branchId+"&stateId="+stateId+"&jobCategoryId="+jobCategoryId+"&jobSubCategoryId="+jobSubCategoryId+"&positionStart="+positionStart+"&talentRef="+talentRef+"&teacherId="+teacherId+"&gradeLevel="+gradeLevel+"&requestType="+requestType; // @Ashish :: add subjectIdList parameter
	//alert(queryString);
	//return false;
	//var xmlhttp;
//	if (window.XMLHttpRequest)
//	  {// code for IE7+, Firefox, Chrome, Opera, Safari
//	  xmlhttp=new XMLHttpRequest();
//	  }
//	else
//	  {// code for IE6, IE5
//	  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
//	  }
	$("#requestType").val("2");
	xmlHttp.open("POST", queryString, false);
	xmlHttp.onreadystatechange = function()
	{
		
		if(xmlHttp.readyState == 4)
		{
		
			if(xmlHttp.status==200)	
			{	
				res = xmlHttp.responseText;
				
				//alert(xmlHttp.responseText.toString());
				
				document.getElementById("divJobsBoard").innerHTML=xmlHttp.responseText.toString();
				//document.getElementById("divJobsBoard").innerHTML="";
				if(districtId != null && districtId == 804800){
					applyScrollOnTblHideZone();
				}else{
					applyScrollOnTbl();
				}
				tpJbIEnable();
				
			}			
			else
			{
				alert("server Error");
			}
		}
	}
	xmlHttp.send(null);
	//xmlHttp.send();

}

function getESPaging(pageno)
{
	var gridNo	=	document.getElementById("gridNo").value;
	
	if(gridNo==1)
	{
		if(pageno!='')
		{
			page=pageno;	
		}
		else
		{
			page=1;
		}
		
		noOfRows = document.getElementById("pageSize").value;
	
		esSearchdistrictjobboard();
		//searchJob();
	}
	else if(gridNo==2)
	{
		if(pageno!='')
		{
			pageforSchool=pageno;	
		}
		else
		{
			pageforSchool=1;
		}
		
		
		noOfRowsSchool = document.getElementById("pageSize1").value;
		showSchoolList(schoolIdsStore);
	}
	else if(gridNo==3)
	{

		if(pageno!='')
		{
			pageZS=pageno;	
		}
		else
		{
			pageZS=1;
		}		
		noOfRowsZS = document.getElementById("pageSizeZoneSchool").value;
		displayZoneSchoolList();
	}
	
}

function getESPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
	var gridNo	=	document.getElementById("gridNo").value;
	
	if(gridNo==1)
	{
		if(pageno!=''){
			page=pageno;	
		}else{
			page=1;
		}
		sortOrderStr	=	sortOrder;
		sortOrderType	=	sortOrderTyp;
		if(document.getElementById("pageSize")!=null){
			noOfRows = document.getElementById("pageSize").value;
		}else{
			noOfRows=50;
		}
		esSearchdistrictjobboard();
	}
	else if(gridNo==2)
	{
		if(pageno!=''){
			pageforSchool=pageno;	
		}else{
			pageforSchool=1;
		}
		sortOrderStrSchool	=	sortOrder;
		sortOrderTypeSchool	=	sortOrderTyp;
		if(document.getElementById("pageSize1")!=null){
			noOfRowsSchool = document.getElementById("pageSize1").value;
		}else{
			noOfRowsSchool=50;
		}
		showSchoolList(schoolIdsStore);
	}
	else if(gridNo==3)
	{
		if(pageno!=''){
			pageforSchool=pageno;	
		}else{
			pageforSchool=1;
		}
		sortOrderStrZS	=	sortOrder;
		sortOrderTypeZS	=	sortOrderTyp;
		if(document.getElementById("pageSizeZoneSchool")!=null){
			noOfRowsZS = document.getElementById("pageSizeZoneSchool").value;
		}else{
			noOfRowsZS=100;
		}
		displayZoneSchoolList();
	}
}

//shadab end



function getJobSubcategory()
{
	var jobcategoryId=document.getElementById("jobCategoryId").value;
	var jobId=0;
	var districtId = trim(document.getElementById("districtId").value);
	var clonejobId = 0;
	//alert("districtId------"+districtId+"    jobcategoryId=========="+jobcategoryId);
	if(districtId!="" && districtId=='804800')
	{
	try{
			ManageJobOrdersAjax.getJobSubCategory(jobId,jobcategoryId,districtId,clonejobId,
			{ 
				async: false,
				errorHandler:handleError,
				callback: function(data)
				{
					if(data!='')
					{
						document.getElementById("jobSubCateDiv").innerHTML=data;
					}
				}
			});
			}catch(ee){alert(ee);}
			if(jobcategoryId ==0){
			var select="<select id=\"jobSubCategoryId\" name=\"jobSubCategoryId\" class=\"form-control\" ><option value=\"0\">Select Job Sub Category Name</option></select>";
			document.getElementById("jobSubCateDiv").innerHTML=select;
			}
	}
}


function selectSubJobCategoryMethod(){
	var jobcategoryvalue=document.getElementById("jobCategoryId").value;
	if(jobcategoryvalue=='0' || jobcategoryvalue==''){
		
		getJobSubcategory();
		document.getElementById("jobSubCategoryId").value=0;
	}else{
		getJobSubcategory();
		var valu=document.getElementById("subjobCategoryhiddenId").value;
		document.getElementById("jobSubCategoryId").value=valu;
		
	}
}
