var page = 1;
var noOfRows = 10;
var index;
var sortOrderStr="";
var sortOrderType="";
var sortOrderType="";
var manageButtonsActivity = [];
var showDataArray;
var rdocheckdon="";var candidateTypecheck="E";
function getPaging(pageno)
{
	if(pageno!='')
	{
		page=pageno;	
	}
	else
	{
		page=1;
	}
	
	noOfRows = document.getElementById("pageSize").value;
	displayOnBoarding();
}
function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
	if(pageno!=''){
		page=pageno;	
	}else{
		page=1;
	}
	sortOrderStr=sortOrder;
	sortOrderType=sortOrderTyp;
	if(document.getElementById("pageSize")!=null){
		noOfRows = document.getElementById("pageSize").value;
	}else{
		noOfRows=10;
	}
}
function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert("Oops: Your Session has expired!");  document.location = 'signin.do';}
	else{alert("Server Error: "+exception.javaClassName);}
}

function displayOnBoarding()
{
	try{
	var districtId = $('#districtId').val();
	if(districtId=="" || districtId==undefined || districtId==null){
		districtId = $("#districtMasterId").val();
	}
	$("#loadingDivOnBoarding").show();
	OnboardingDashboardAjax.displayOnBoardingRecord(districtId,noOfRows,page,{
		async: false,
		callback: function(data)
		{
			$('#onBoardGrid').html(data);	
			$(".tempToolTip").tooltip();
		},
		errorHandler:handleError 
});
	}catch(e){alert("Error ::::   "+e);}
	$("#loadingDivOnBoarding").hide();
}

function displayCommunication()
{
	try{
	var districtId = $('#districtId').val();;
	var onboardingId=$("#onboardID").val();
	var candidateType=$("#checkStatusCandidateType").val();
	var checkPointId=$("#checkPointId").val();
	$("#loadingDivOnBoarding").show();
	OnboardingDashboardAjax.displayCommunicationRecord(districtId,noOfRows,page,onboardingId,candidateType,checkPointId,{
		async: false,
		callback: function(data)
		{
			$('#displayCommunicationGrid').html(data);	
		},
		errorHandler:handleError 
});
	}catch(e){alert(e);}
	$("#loadingDivOnBoarding").hide();
}

function displayPermissionDefault()
{
	try{
	var districtId = $('#districtId').val();;
	var onboardingId=$("#onboardID").val();
	var candidateType=$("#checkStatusCandidateType").val();
	var checkPointId=$("#checkPointId").val();
	OnboardingDashboardAjax.checkPointPermissionDefault(districtId,onboardingId,checkPointId,candidateType,{
		preHook:function(){$('#loadingDivOnBoarding').show()},
		postHook:function(){$('#loadingDivOnBoarding').hide()},
		async: false,
		callback: function(data)
		{
			$('#permissionDiv').html(data);	
			$(".tempToolTip").tooltip();
		},
		errorHandler:handleError 
});
	}catch(e){alert(e);}
}
function updateAndContinue()
{
	$('#loadingDiv').show();
	var flag=false;
	var counter=0;
	$('#errordivonboard').empty();
	var dashboardname=$("#managedashboardname").val();
	$('#managedashboardname').css("background-color", "white");
	if(dashboardname==null || dashboardname=="")
	{
		$('#errordivonboard').show();
		$('#errordivonboard').append("&#149; Please enter the Onboarding Dashboard Name<br>");
		$('#managedashboardname').css("background-color", "#F5E7E1");
		$('#managedashboardname').focus();
		counter++;
		flag=true;
		return false;
	}
	if(flag==false)
	{
		try{
		var appliedAt=0;
		var districtId=$("#districtId").val();
		var cloneID  =$("#cloneID").val();
		var onboardID=$("#onboardID").val();
		var dashboardName=$("#managedashboardname").val();
		if($("#applyforall").is(":checked"))
			appliedAt=1;
		
		OnboardingDashboardAjax.updateOnBoarding(districtId,cloneID,onboardID,appliedAt,dashboardName,{
			async: false,
			callback: function(data)
			{	
			if(data=="true")
			{
				$('#errordivonboard').show();
				$('#errordivonboard').append("&#149; Onboarding name already exists!<br>");
				$('#managedashboardname').css("background-color", "#F5E7E1");
				$('#managedashboardname').focus();
			}else{
				$("#onboardId").val(data);
				$('#AddOnboarding').hide();
				$('#editOnboarding').show();
				$("#addOnboardingDiv").hide();
				$(".hideButton").hide();
				$("#showOnboardingDivContinue").show();
				$(".hideupdateButton").hide();
			 }
			},
			errorHandler:handleError 
		});	
	}catch(e){alert("error::::::::::"+e)}
	}
	updateReadOnlyCheckPoint();
	updateCheckPointTransactionJS();
	saveCheckPointAction();
	$('#loadingDiv').hide();
	cancelOnboardingForm();
}
function activateDeactivateOnBoarding(onBoardingId,status)
{
	$("loadingDivOnBoarding").show();
	OnboardingDashboardAjax.activateDeactivateOnBoarding(onBoardingId,status,{
		async: false,
		callback: function(data)
		{
			displayOnBoarding();			
		},
		errorHandler:handleError
});
	$("loadingDivOnBoarding").hide();
}
 

function showOnboarding()
{	
	$(".hideupdateButton").hide();
	$('#AddOnboarding').hide();
	$('#editOnboarding').show();
	$("#addOnboardingDiv").hide();
	$("#showOnboardingDiv").show();
	$(".hideButton").show();
	$("#searchBox").hide();
	$("#districtName").removeAttr("disabled");
	var entityType=$("#entityTypeID").val();
	if(entityType==1){
		if($("#districtSearchId").val()!=0){
			$("#districtId").val($("#districtSearchId").val());
			$("#districtName").val($("#districtSearchName").val());
			$("#districtName").attr("disabled","disabled");
			$("#managedashboardname").focus();
		}else{
			if($("#districtMasterId").val()!=0){
				$("#districtId").val($("#districtMasterId").val());
				$("#districtName").val($("#districtMasterName").val());
				$("#districtName").attr("disabled","disabled");
				$("#managedashboardname").focus();
			}else{
				$("#districtId").val(0);
				$("#districtName").val("");
				$("#districtName").focus();
			}
	   }
   }
}
function cancelOnboardingForm()
{	
	
	$(".hideupdateButton").hide();
	$('#AddOnboarding').show();
	$('#editOnboarding').hide();
	$("#addOnboardingDiv").show();
	$("#showOnboardingDiv").hide();
	$("#showOnboardingDivContinue").hide();
	$("#managedashboardname").val("");
	$("#applyforall").prop("checked",false);
	$("#searchBox").show();
	$("#errordivonboard").empty();
	$("#errordivonboard").hide();
	$("#districtName").val("");
	$("#districtName").css('background-color', 'white');
	$("#managedashboardname").val("");
	$("#managedashboardname").css('background-color', 'white');
	if($("#entityTypeID").val()!=2){
		if($("#districtSearchId").val()!=0){
			$("#districtId").val($("#districtSearchId").val());
			$("#districtName").val($("#districtSearchName").val());
		}
	}else{
		if($("#districtMasterId").val()!=0){
		$("#districtId").val($("#districtMasterId").val());
		$("#districtName").val($("#districtMasterName").val());
		}else{
			$("#districtId").val(0);
			$("#districtName").val("");
		}
	}
	$("#applyforall").prop("checked",false);
	displayOnBoarding();
	
}

function hideOrShowCondidateType(type)
{
var sectionExternal=document.getElementById("section0");
var sectionInternal=document.getElementById("section1");
var sectionITransfer=document.getElementById("section2");
var counterFlag=0;
if(sectionExternal.checked)
{
	$(".externalDiv").show();
	counterFlag+=1;
}
else
	$(".externalDiv").hide();

if(sectionInternal.checked)
{
	$(".internalDiv").show();
	counterFlag+=1;
}
else
	$(".internalDiv").hide();

if(sectionITransfer.checked)
{
	$(".internalTDiv").show();
	counterFlag+=1;
}
else
	$(".internalTDiv").hide();

if(counterFlag==0)
	$(".showhideedit").hide();
else
	$(".showhideedit").show();

$("#extraWidth").removeAttr("class");
var recordCount=$("#checkPointRecordCount").val();
for(var i=0;i<recordCount;i+=1){
	$("#extraWidth"+i).removeAttr("class");
}
if(counterFlag==1){
	$("#extraWidth").attr("class","col-sm-6 col-md-6 managewidth");
	for(var i=0;i<recordCount;i+=1){
		$("#extraWidth"+i).attr("class","col-sm-6 col-md-6 managewidth");
	}
}
if(counterFlag==2){
	$("#extraWidth").attr("class","col-sm-4 col-md-4 managewidth");
	for(var i=0;i<recordCount;i+=1){
		$("#extraWidth"+i).attr("class","col-sm-4 col-md-4 managewidth");
	}
}
if(counterFlag==3){
	$("#extraWidth").attr("class","col-sm-2 col-md-2 managewidth");
	for(var i=0;i<recordCount;i+=1){
		$("#extraWidth"+i).attr("class","col-sm-2 col-md-2 managewidth");
	}
  }
}

function deactivateSection(canType,sectionid)
{
	var status=$("#"+canType+"_"+sectionid).attr('data-original-title');
	if(status=="Active")
	{
		$('#'+canType+'_'+canType+'_'+sectionid).val(0);
		$('#'+canType+'_'+sectionid).attr('data-original-title', 'Inactive')
		$('#'+canType+'_'+sectionid).html(resourceJSON.instX);
		$('#'+canType+'_'+sectionid).css('background-color', 'grey');
	}else{
		$('#'+canType+'_'+canType+'_'+sectionid).val(1);
		$('#'+canType+'_'+sectionid).attr('data-original-title', 'Active');
		$('#'+canType+'_'+sectionid).html(resourceJSON.instA);
		$('#'+canType+'_'+sectionid).css('background-color', 'green');
	}
}

function saveAndContinue()
{
	var flag=false;
	var counter=0;
	$('#errordivonboard').empty();
	var entityType=$("#entityType").val();
	var dashboardname=$("#managedashboardname").val();
	$('#managedashboardname').css("background-color", "white");
	if(entityType==1)
	{
		var districtId=$("#districtId").val();
		
		if(districtId==0)
		{
			$('#errordivonboard').show();
			$('#errordivonboard').append("&#149; Please select district name<br>");
			$('#districtSearchName').css("background-color", "#F5E7E1");
			$('#districtSearchName').focus();
			counter++;
			flag=true;
		}
	}
	if(dashboardname==null || dashboardname=="")
	{
		$('#errordivonboard').show();
		$('#errordivonboard').append("&#149; Please enter the Onboarding Dashboard Name<br>");
		$('#managedashboardname').css("background-color", "#F5E7E1");
		if(flag==false)
			$('#managedashboardname').focus();
		counter++;
		flag=true;
	}
	if(flag==false)
	{
		try{
		var appliedAt=0;
		var districtId=$("#districtId").val();
		var dashboardName=$("#managedashboardname").val();
		if($("#applyforall").is(":checked"))
			appliedAt=1;
		OnboardingDashboardAjax.saveOnBoarding(districtId,appliedAt,dashboardName,{
			async: false,
			callback: function(data)
			{	
			if(data=="true")
			{
				$('#errordivonboard').show();
				$('#errordivonboard').append("&#149; Onboarding name already exists!<br>");
				$('#managedashboardname').css("background-color", "#F5E7E1");
				$('#managedashboardname').focus();
			}else{
				$("#onboardID").val(data);
				$("#districtId").val(districtId);
				$('#AddOnboarding').hide();
				$('#editOnboarding').show();
				$("#addOnboardingDiv").hide();
				$(".hideButton").hide();
				$("#showOnboardingDivContinue").show();
			 }
			},
			errorHandler:handleError 
		});	
		
		
	}catch(e){alert("error::::::::::"+e)}
	getReadOnlycheckPointDefault()
	displayCheckPointRecord();
	}else{
		return false;
	}
}
function editOnBoarding(onBoardingId,cloneEditFlag,districtId,districtName)
{
	$('#loadingDiv').show();
	showOnboarding();
	var value;
	var appliedAt=0;
	$('#readyonlycheckpointid').val(districtId);
	$("#districtId").val(districtId);
	$("#districtName").val(districtName);
	$("#districtName").prop("disabled",true);
	var dashboardName=$("#managedashboardname").val();
	$(".hideupdateButton").show();
	$(".hideButton").hide();
	$('#cloneID').val(cloneEditFlag);
	$("#onboardID").val(onBoardingId);
	if($("#applyforall").is(":checked"))
	/*        Start without save continue  */
	$('#AddOnboarding').hide();
	$('#editOnboarding').show();
	$("#addOnboardingDiv").hide();
	$(".hideButton").hide();
	$("#showOnboardingDivContinue").show();
	$(".hideupdateButton").hide();
	/*        End               */
		appliedAt=1;
	OnboardingDashboardAjax.editOnboarding(onBoardingId,{
		async: true,
		callback: function(data)
		{
		value=data.split("^");
		if(cloneEditFlag==1)
			$("#managedashboardname").val("");
		else
			$("#managedashboardname").val(value[0]);
		if(value[1]=1)
			$('#applyforall').prop('checked', true);
		else
			$('#applyforall').prop('checked', true);
		},
		errorHandler:handleError
	});
	$('#loadingDiv').hide();
	getReadOnlycheckPointDefault();
	displayCheckPointRecord();
}
var dashboardname;
function clickView(checkPointId,sectionId,part,checkPointName)
{
	var secId=Number(sectionId);
	var appType=['E','I','T'];
	var canType="";
	if(sectionId==1)
		$("#checkStatusCandidateType").val("E");
	else if(sectionId==2)
		$("#checkStatusCandidateType").val("I");
	else
		$("#checkStatusCandidateType").val("T");
	
	$("#checkCandidateType").val(sectionId);
	$("#checkPointId").val(checkPointId);
	$("#checkPointName").val(checkPointName);
	$("#part").val(part);
	if(true)
	{
		if(part==1)
		{
		getcheckpointstatusdefault();
		dashboardname=$("#managedashboardname").val();
		$("#manageCheckPoint").show();
		$("#addStatus").hide();
		$("#dashboardname").html(dashboardname);
		}
		else if(part==2)
		{
			displayCommunication();
			dashboardname=$("#managedashboardname").val();
			$("#manageCommunication").show();
			$("#communicationdashboardname").html(dashboardname);
		}else if(part==3)
		{/*
			var status=$("#"+checkPointId+part+appType[secId-1]+appType[secId-1]+"id"+part).val();
			if(status=="1"){
				$("#"+checkPointId+part+appType[secId-1]+appType[secId-1]+part).val("I");
				$("#"+checkPointId+part+appType[secId-1]+appType[secId-1]+"id"+part).val(0);
				$("#"+checkPointId+part+appType[secId-1]+part).attr('data-original-title', 'No action required')
				$("#"+checkPointId+part+appType[secId-1]+part).css('background-color', 'grey');
			}else{
				$("#"+checkPointId+part+appType[secId-1]+appType[secId-1]+part).val("A");
				$("#"+checkPointId+part+appType[secId-1]+appType[secId-1]+"id"+part).val(1);
				$("#"+checkPointId+part+appType[secId-1]+part).attr('data-original-title', 'Action required')
				$("#"+checkPointId+part+appType[secId-1]+part).css('background-color', 'green');
			}
		*/}else if(part==4){
			$("#checkPointNa").html(checkPointName);
			displayPermissionDefault();
			showPermissionDiv();
		}
	}else{
		return false;
	}
}
function nameValidation()
{
	$("#errordivManage").empty();
	var dashboardName=$("#boardingdashboardname").val();
	if(dashboardName==null || dashboardName=="")
	{
		$("#errordivManage").show();
		$("#errordivManage").append("&#149; Please enter the Onboarding Dashboard Name<br>");
		$('#boardingdashboardname').css("background-color", "#F5E7E1");
		$('#boardingdashboardname').focus();
		return false;
	}
	else{
		$("#errordivManage").hide();
		return true;
		}
}
function cancelCheckPointDiv()
{
	$("#checkpointstatus").empty();
	$("#manageCheckPoint").hide();
	$(".addstatusbtn").hide();
}
function cancelManageStatusDiv()
{
	$(".addstatusbtn").hide();
	$(".managestatusbtn").show();
	$("#addStatus").hide();
}
function showAddStatus()
{
	$("#actionType").val("1");
	$("#errordivStatus").empty();
	$("#errordivStatus").hide();
	$('#statusname').css("background-color", "white");
	hidecheckStatus();
	$("#addStatus").show();
	$(".addstatusbtn").show();
	//$("#checkpointstatus").hide();
}
$(document).ready(function(){
	$('#editaddstatus').click(function(){
		$('.bfh-colorpicker-icon').css('background-color','rgb(128, 128, 128)');
		$('#statusname2').val('');$("input[name=colorpicker2]").val('#808080');
		$('#editAddStatus').toggle();
		$("#showActionTypeCheckPointStatus").val("1");
	});
	$('#editCancelAddStatus').click(function(){
		$('#editAddStatus').hide();
		$("#showActionTypeCheckPointStatus").val("1");
	});
});
	

var showCounter=0;
function showHideTypeMasterDiv()
{
		resetcheckSection();
		resetCommunication();
		$(".multicandidateType").show();
		$(".multicandidateTypeBTN").show();
		$("#communicationCancelbtn").css({"padding-left": "0px"});
		$("#actionType").val("1");
		$("#communicationId").val(0);
		showCounter=1;
}
function resetCommunicationSection()
{
	var typeArr=['CLi','HQALi','BALi','DALi','SALi'];
	for(var i=0;i<5;i+=1){
  	  $(".cType"+i+"Div").hide();
		  $("."+typeArr[i]).hide();
    }
}

function cancelCommunicationDiv(type)
{
	if(type==2){
	if(showCounter){
		resetCommunicationSection();
		$("#communicationCancelbtn").css({"padding-left": "15px"});
		$(".multicandidateType").hide();
		$(".multicandidateTypeBTN").hide();
		showCounter=0;
		$("#errordivCommunication").empty();
		$("#errordivCommunication").hide();
	}else{
		$("#communicationCancelbtn").css({"padding-left": "15px"});
		$("#manageCommunication").hide();
		resetcheckSection();
		$("#errordivCommunication").empty();
		$("#errordivCommunication").hide();
	}
	}else{
		$("#communicationCancelbtn").css({"padding-left": "15px"});
		$("#manageCommunication").hide();
		resetcheckSection();
		$("#errordivCommunication").empty();
		$("#errordivCommunication").hide();
	}
}
function showCommunicationSection(id)
{
	var typeArr=['CLi','HQALi','BALi','DALi','SALi'];
	var multiSelectArr=['CAMClass','HQAMClass','BAMClass','DAMClass','SAMClass'];
	for(var i=0;i<5;i+=1){
		if($("#uType"+i).is(":checked")){
			$(".cType"+i+"Div").show();
			$("."+typeArr[i]).show();
		}else{
			$(".cType"+i+"Div").hide();
			$("."+typeArr[i]).hide();
			$("."+multiSelectArr[i]).attr("checked",false);
		}
	}
	for(var j=1;j<=5;j+=1)
		usertypeMultiSelect(j);
}

function resetcheckSection()
{
	for(var i=0;i<=4;i+=1){
	$("#uType"+i).attr("checked",false);
	$(".cType"+i+"Div").hide();
	}
}

function displayOrHideSearchBox()
{
	districtNameFilter=1;
	var entityID	=	document.getElementById("entityType").value;
	$('#districtSearchId').val('');
	$('#districtSearchName').val('');
	if(entityID==1){
		$('#districtSearchLabel').hide();
		$('#districtSearchBox').hide();
		$('#searchPortfolio').hide();
		$('#districtClassMaster').hide();
		$("#districtSearchName").focus();
	}
	else
	{
		$('#districtSearchLabel').show();
		$('#districtSearchBox').show();
		$('#searchPortfolio').show();
		$('#districtClassMaster').show();
		$("#districtSearchName").focus();
	}
}



function getDistrictAuto(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
    hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("districtName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getDistrictArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}
var hiddenDataArray;
function getDistrictArray(districtName){
	var searchArray = new Array();
	JobUploadTempAjax.getFieldOfDistrictList(districtName,{ 
			async: false,
			callback: function(data){
			hiddenDataArray = new Array();
			showDataArray = new Array();
			for(i=0;i<data.length;i++){
				searchArray[i]=data[i].districtName;
				showDataArray[i]=data[i].districtName;
				hiddenDataArray[i]=data[i].districtId;
			}
		},
		errorHandler:handleError
		});	
	return searchArray;
}


var selectFirst="";

var fatchData= function(txtSearch,searchArray,txtId,txtdivid){
	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
			}
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}
function hideDistrictMasterDiv(dis,hiddenId,divId)
{
	//$("#districtId").val("");
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			$("#districtId").val(hiddenDataArray[index]);
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}


function mouseOverChk(txtdivid,txtboxId)
{	
	
	for(var i=0;i<length;i++)
	{
		$('#divResult'+txtdivid+i).mouseover({param1: i,param2:txtdivid, param3:txtboxId}, fireMouseOverEvent);
	}
}

function fireMouseOverEvent(event)
{
	for(var i=0;i<length;i++)
	{	
		document.getElementById('divResult'+event.data.param2+i).className='normal';
	}
    document.getElementById('divResult'+event.data.param2+event.data.param1).className='over';	       
   	document.getElementById(event.data.param3).value= $('#divResult'+event.data.param2+event.data.param1).text();
    index=event.data.param1;
}

var downArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index<length-1){
			for(var i=0;i<10;i++){
				{
					if(document.getElementById('divResult'+txtdivid+i))
						var div_id=document.getElementById('divResult'+txtdivid+i);
					if(div_id)
					{
						if(div_id.className=='over')
							index=div_id.id.split('divResult'+txtdivid)[1];
						div_id.className='normal';
					}
				}
			}
			index++;
			if(document.getElementById('divResult'+txtdivid+index))
			{
				var div_id=document.getElementById('divResult'+txtdivid+index);
				div_id.className='over';
				document.getElementById(txtid).value = div_id.innerHTML;
				selectFirst=div_id.innerHTML;
			}
		}
	}
}

var upArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index>0){
			for(var i=0;i<length;i++){

				var div_id=document.getElementById('divResult'+txtdivid+i);
				if(div_id)
				{
					if(div_id.className=='over')
						index=div_id.id.split('divResult'+txtdivid)[1];
					div_id.className='normal';
				}
			}
			index--;
			if(document.getElementById('divResult'+txtdivid+index))
				document.getElementById('divResult'+txtdivid+index).className='over';
			if(txtid && document.getElementById('divResult'+txtdivid+index))
			{
				document.getElementById(txtid).value=
					document.getElementById('divResult'+txtdivid+index).innerHTML;
				selectFirst=document.getElementById('divResult'+txtdivid+index).innerHTML;
			}
		}
	}
}

var overText=function (div_value,txtdivid) {

	for(var i=0;i<length;i++)
	{
		if(document.getElementById('divResult'+i))
		{
			var div_id=document.getElementById('divResult'+i);

			if(div_id.className=='over')
				index=div_id.id.split(txtdivid)[1];
			div_id.className='normal';
		}
	}
	div_value.className = 'over';
	document.getElementById(txtid).value= div_value.innerHTML;
}
//////////////////////Start District Search Filter/////////////////////////

function getDistrictAuto(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
    hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("districtSearchName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getDistrictArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}
function hideDistrictSearchMasterDiv(dis,hiddenId,divId)
{
	document.getElementById("districtSearchId").value="";
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			document.getElementById("districtSearchId").value=hiddenDataArray[index];
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}

//////////////////////End District Search Filter/////////////////////////


function searchRecordByDistrict()
{
	var districtId=$("#districtSearchId").val();
	$('#loadingDiv').show();
	try{
	OnboardingDashboardAjax.displayOnBoardingRecord(districtId,noOfRows,page,{
		async: false,
		callback: function(data)
		{
			$('#onBoardGrid').html(data);	
		},
		errorHandler:handleError 
});
	$('#loadingDiv').hide();
	}catch(e){alert(e);}
	hideSearchAgainMaster();
	$("#districtName").val($("#districtSearchName").val());
	$("#districtId").val($("#districtSearchId").val());
}

function getcheckpointstatusdefault()
{
	var evaluteid= $('#popuptoevalute').val();
	$('#loadingDiv').show();
	var districtId=$("#districtId").val();
	var onboardingId=$("#onboardID").val();
	var candidateType=$("#checkStatusCandidateType").val();
	var checkPointId=$("#checkPointId").val();
	try{
	OnboardingDashboardAjax.checkPointStatusDefault(districtId,onboardingId,checkPointId,candidateType,{
		async: false,
		callback: function(data)
		{
		
		    $("#checkpointstatus").show();
			$('#checkpointstatus').html(data);	
			$(".tempToolTip").tooltip();
		},
		errorHandler:handleError 
});
	$('#loadingDiv').hide();
	}catch(e){alert(e);}
}

function addCheckStatus()
{
	var chkPointRecordFlag= $('#checkPointRecordFlag').val();
	var statusname=$("#statusname").val();
	var statusId=$("#checkPointStatusId").val();
	var districtId=$("#districtId").val();
	if(districtId=="" | districtId==null)
	 districtId=$("#readyonlycheckpointid").val();
	var onBoardingId=$("#onboardID").val();
	var actionType=$("#actionType").val();
	var checkStatusType=$("#checkStatusCandidateType").val();
	var checkPointId=$("#checkPointId").val();
	var decisionpoint=0;
	var recordno=Number($("#checkNoRecord").val());
		recordno+=1;
	var errorFlag=false;
	var counter=0;
	var duplicateCounter=0;
	var checkName=$("#checkStatsNa").val();
	$("#errordivStatus").empty();
	$("#errordivStatus").hide();
	$('#statusname').css("background-color", "white");
	var preCheckPointStatusname=checkName.split("#");
	var editCheckPointStatusNameEdit= $("#editCheckPointStatusNameEdit").val();
	for(var i=0;i<preCheckPointStatusname.length;i+=1){
		if(actionType==1){
		if(preCheckPointStatusname[i].trim().toLowerCase()==statusname.trim().toLowerCase())
			if(editCheckPointStatusNameEdit.trim().toLowerCase()==statusname.trim().toLowerCase())
				duplicateCounter=0;
			else
				duplicateCounter=1;
		}else{
			for(var i=0;i<preCheckPointStatusname.length;i+=1){
				if(recordno!=i && preCheckPointStatusname[i].trim().toLowerCase()==statusname.trim().toLowerCase())
					if(editCheckPointStatusNameEdit.trim().toLowerCase()==statusname.trim().toLowerCase())
						duplicateCounter=0;
					else
						duplicateCounter=1;
			}
		}
	}
	
	if($("#decisionpoint").is(":checked"))
		decisionpoint=1;
	
	var colorCode=$("[name=colorpicker3]").val();
	var status="A";
	if($('input[name=status]:checked').val()==1){
		status="A";
	}else{
		status="I";
	}
	if(statusname==undefined || statusname=="")
	{
		$('#errordivStatus').show();
		$('#errordivStatus').append("&#149; Please enter Status Name<br>");
		$('#statusname').css("background-color", "#F5E7E1");
		$('#statusname').focus();
		counter++;
		errorFlag=true;
	}
	if(errorFlag){
		return false;
	}else{
		if(duplicateCounter){
			$('#errordivStatus').show();
			$('#errordivStatus').append("&#149; Duplicate Status Name<br>");
			$('#statusname').css("background-color", "#F5E7E1");
			$('#statusname').focus();
			return false;
		}else{
		$('#loadingDiv').show();
		try{
		OnboardingDashboardAjax.saveCheckPrintStatus(checkPointId,checkStatusType,statusId,districtId,onBoardingId,statusname,decisionpoint,colorCode,status,actionType,chkPointRecordFlag,{
			async: false,
			callback: function(data)
			{
			getcheckpointstatusdefault();
			$("#addStatus").hide();
			$(".addstatusbtn").hide();
			},
			errorHandler:handleError 
	    });
		$('#loadingDiv').hide();
		}catch(e){alert(e);}
		displayCheckPointRecord();
	}
 }
}
function hidecheckStatus()
{
	$("#checkpointstatus").show();
	$("#statusname").val("");
	$("#decisionpoint").prop("checked",false);
	$("input[name=colorpicker3]").val("#D3D3D3");
	$(".bfh-colorpicker-icon").css('background-color','#D3D3D3');
	$("#active").prop("checked",false);
	$("#addStatus").hide();
	$(".addstatusbtn").hide();
}

function editCheckPointStatus(type,id,noRecord)
{
	$('#errordivStatus').empty();
	$('#errordivStatus').hide();
	$("#statusname").css('background-color','white');
	$("#addStatus").show();
	$("#editAddStatus").show();
	$("#checkpointstatus").hide();
	$("#actionType").val("2");
	$("#showActionTypeCheckPointStatus").val("2");
	$("#editCheckPointStatusNameEdit").val($('#name_'+noRecord+'').text());
	//$(".addstatusbtn").show();
	$("#checkNoRecord").val(noRecord);
	var districtId=$("#districtId").val();
	var onBoardingId=$("#onboardID").val();
	var candidateType=$("#checkStatusCandidateType").val();
	var checkPointId=$("#checkPointId").val();
	
	var colorConfig=$("#"+noRecord+"CId").val();
	$("input[name=colorpicker2]").val(colorConfig);
	$("#statusname").val($("#name_"+noRecord).text());
	$("#statusname2").val($("#name_"+noRecord).text());
	$("#statusname3").val($("#name_"+noRecord).text());
	if($("#"+noRecord+"dId").val()!=0){
			$("#decisionpoint").prop("checked",true);
		}else{
			$("#decisionpoint").prop("checked",false);
		}
	
	$("input[name=colorpicker3]").val(colorConfig);
	$(".bfh-colorpicker-icon").css('background-color',colorConfig);
		if($("#"+noRecord+"_1T").attr("data-original-title")=='Active'){
			$("#active").prop("checked",true);
			$('#active2').prop("checked",true);
		}else{
			$("#inactive").prop("checked",true);
			$("#inactive2").prop("checked",true);
		}
		$("#checkPointStatusId").val(id);
}

function checkPointSaveAndExit()
{
	addCheckStatus();
	cancelCheckPointDiv();
	displayCheckPointRecord();
}
function checkPointSaveAndContinue()
{
	checkPointSaveAndExit();
	var candidateType=$("#checkCandidateType").val();
	var checkPointId=$("#checkPointId").val();
	var part=$("#part").val();
	clickView(checkPointId,candidateType,2,1);
}

//*********************** Function for user type multi select *************************************     
function usertypeMultiSelect(type)
{
   var checkboxes;
   var vals = "";
   var valsArr =[];
   var p=0;
   var checkCounter=0;
   var hidaId=['hidaCA','hidaHQA','hidaBA','hidaDA','hidaSA'];
   var userId=['CAUserId','HQAUserId','BAUserId','DAUserId','SAUserId'];
   switch(type){
   case 1:{checkboxes=document.getElementsByName('CAOpt');break;}
   case 2:{checkboxes=document.getElementsByName('HQAOpt');break;}
   case 3:{checkboxes=document.getElementsByName('BAOpt');break;}
   case 4:{checkboxes=document.getElementsByName('DAOpt');break;}
   case 5:{checkboxes=document.getElementsByName('SAOpt');break;}
   }
   for (var i=0, n=checkboxes.length;i<n;i+=1) {
	  if (checkboxes[i].checked){
		  checkCounter+=1;
		  valsArr[p]=checkboxes[i].value
		  if(p==0){
		  vals +=checkboxes[i].title;
		  }else{
		  vals +=", "+checkboxes[i].title;
		  }
		  p+=1;
	  }
  }
   if(checkCounter==0){
	   $("."+hidaId[Number(type-1)]).html("Select User type");
		 $("#"+userId[Number(type-1)]).val(0); 	  
   }else{
		  $("."+hidaId[Number(type-1)]).html(vals);
		  $("#"+userId[Number(type-1)]).val(valsArr);
   }
}
function saveAndExitCommunication()
{
	
	$("#errordivCommunication").empty();
	$("#errordivCommunication").hide();
	var CData=HQAData=BAData=DAData=SAData="";
	var checkCounter=0;
	var CAUserId,HQAUserId,BAUserId,DAUserId,SAUserId;
	var ctempleteName=hqtempleteName=btempleteName=dtempleteName=stempleteName="";
	var csubjectLine=hqsubjectLine=bsubjectLine=dsubjectLine=ssubjectLine="";
	var cemailText=hqemailText=bemailText=demailText=semailText="";
	var ccommStatus=hqcommStatus=bcommStatus=dcommStatus=scommStatus=0;
	var csetDefault=hqsetDefault=bsetDefault=dsetDefault=ssetDefault=0;
	var errorCounter=0;
	if($("#uType"+0).is(":checked")){
		CData="1";
		checkCounter+=1;
		if($("#CAUserId").val()==0){
			$("#errordivCommunication").show();
			$("#errordivCommunication").append("&#149; Please select User Type for Candidate <br>");
			$('#hidaCA').css("background-color", "#F5E7E1");
			$('#hidaCA').focus();
			errorCounter=1;	
		}else{
		CAUserId=$("#CAUserId").val();
		CData+="###"+$("#CAUserId").val();
		}
		if($("#ctempleteName").val()==null || $("#ctempleteName").val()==""){
			$("#errordivCommunication").show();
			$("#errordivCommunication").append("&#149; Please enter Template Name for Candidate <br>");
			if(errorCounter==0){
				$('#ctempleteName').css("background-color", "#F5E7E1");
				$('#ctempleteName').focus();
				errorCounter=1;
			}
		}else{
		ctempleteName=$("#ctempleteName").val();
		CData+="###"+$("#ctempleteName").val();
		}
		if($("#csubjectLine").val()==null || $("#csubjectLine").val()==""){
			$("#errordivCommunication").show();
			$("#errordivCommunication").append("&#149; Please enter Subject Line for Candidate <br>");
			if(errorCounter==0){
				$('#csubjectLine').css("background-color", "#F5E7E1");
				$('#csubjectLine').focus();
				errorCounter=1;
			}
		}else{
		csubjectLine=$("#csubjectLine").val();
		CData+="###"+$("#csubjectLine").val();
		}
		
		ccommStatus=$("input[name='ccommStatus']:checked").val();
		CData+="###"+$("input[name='ccommStatus']:checked").val();
		if($("#csetDefault").is(":checked")){
			csetDefault=1;
			CData+="###"+csetDefault;
		}else{
			csetDefault=0;
			CData+="###"+csetDefault;
		}
		if($("#cemailText").find(".jqte_editor").html()==null || $("#cemailText").find(".jqte_editor").html()==""){
			$("#errordivCommunication").show();
			$("#errordivCommunication").append("&#149; Please enter Email Text for Candidate <br>");
			if(errorCounter==0){
				$('#cemailText').css("background-color", "#F5E7E1");
				$('#cemailText').focus();
				errorCounter=1;
			}
		}else{
		cemailText=$("#cemailText").find(".jqte_editor").html();
		CData+="###"+$("#cemailText").find(".jqte_editor").html();
		}
	}
	if($("#uType"+1).is(":checked")){
		HQAData="2";
		if($("#HQAUserId").val()==0){
			$("#errordivCommunication").show();
			$("#errordivCommunication").append("&#149; Please select User Type for HQA <br>");
			$('#hidaHQA').css("background-color", "#F5E7E1");
			$('#hidaHQA').focus();
			errorCounter=1;	
		}else{
			HQAUserId=$("#HQAUserId").val();
			HQAData+="###"+$("#HQAUserId").val();
		}
		if($("#hqtempleteName").val()==null || $("#hqtempleteName").val()==""){
			$("#errordivCommunication").show();
			$("#errordivCommunication").append("&#149; Please enter Template Name for HQA <br>");
			if(errorCounter==0){
				$('#hqtempleteName').css("background-color", "#F5E7E1");
				$('#hqtempleteName').focus();
				errorCounter=1;
			}
		}else{
			hqtempleteName=$("#hqtempleteName").val();
			HQAData+="###"+$("#hqtempleteName").val();
		}
		if($("#hqsubjectLine").val()==null || $("#hqsubjectLine").val()==""){
			$("#errordivCommunication").show();
			$("#errordivCommunication").append("&#149; Please enter Subject Line for HQA <br>");
			if(errorCounter==0){
				$('#hqsubjectLine').css("background-color", "#F5E7E1");
				$('#hqsubjectLine').focus();
				errorCounter=1;
			}
		}else{
			hqsubjectLine=$("#hqsubjectLine").val();
			HQAData+="###"+$("#hqsubjectLine").val();
		}
		
		hqcommStatus=$("input[name='hqcommStatus']:checked").val();
		HQAData+="###"+$("input[name='hqcommStatus']:checked").val();
		if($("#hqsetDefault").is(":checked")){
			hqsetDefault=1;
			HQAData+="###"+hqsetDefault;
		}else{
			hqsetDefault=0;
			HQAData+="###"+hqsetDefault;
		}
		if($("#hqemailText").find(".jqte_editor").html()==null || $("#hqemailText").find(".jqte_editor").html()==""){
			$("#errordivCommunication").show();
			$("#errordivCommunication").append("&#149; Please enter Email Text for HQA <br>");
			if(errorCounter==0){
				$('#hqemailText').css("background-color", "#F5E7E1");
				$('#hqemailText').focus();
				errorCounter=1;
			}
		}else{
			hqemailText=$("#hqemailText").find(".jqte_editor").html();
			HQAData+="###"+$("#hqemailText").find(".jqte_editor").html();
		}
		}
	if($("#uType"+2).is(":checked")){
		BAData="3";
		if($("#BAUserId").val()==0){
			$("#errordivCommunication").show();
			$("#errordivCommunication").append("&#149; Please select User Type for BA <br>");
			$('#hidaBA').css("background-color", "#F5E7E1");
			$('#hidaBA').focus();
			errorCounter=1;	
		}else{
		BAUserId=$("#BAUserId").val();
		BAData+="###"+$("#BAUserId").val();
		}
		if($("#btempleteName").val()==null || $("#btempleteName").val()==""){
			$("#errordivCommunication").show();
			$("#errordivCommunication").append("&#149; Please enter Template Name for BA <br>");
			if(errorCounter==0){
				$('#btempleteName').css("background-color", "#F5E7E1");
				$('#btempleteName').focus();
				errorCounter=1;
			}
		}else{
		btempleteName=$("#btempleteName").val();
		BAData+="###"+$("#btempleteName").val();
		}
		if($("#bsubjectLine").val()==null || $("#bsubjectLine").val()==""){
			$("#errordivCommunication").show();
			$("#errordivCommunication").append("&#149; Please enter Subject Line for BA <br>");
			if(errorCounter==0){
				$('#bsubjectLine').css("background-color", "#F5E7E1");
				$('#bsubjectLine').focus();
				errorCounter=1;
			}
		}else{
		bsubjectLine=$("#bsubjectLine").val();
		BAData+="###"+$("#bsubjectLine").val();
		}
		
		bcommStatus=$("input[name='bcommStatus']:checked").val();
		BAData+="###"+$("input[name='bcommStatus']:checked").val();
		if($("#bsetDefault").is(":checked")){
			bsetDefault=1;
			BAData+="###"+bsetDefault;
		}else{
			bsetDefault=0;
			BAData+="###"+bsetDefault;
		}
		if($("#bemailText").find(".jqte_editor").html()==null || $("#bemailText").find(".jqte_editor").html()==""){
			$("#errordivCommunication").show();
			$("#errordivCommunication").append("&#149; Please enter Email Text for BA <br>");
			if(errorCounter==0){
				$('#bemailText').css("background-color", "#F5E7E1");
				$('#bemailText').focus();
				errorCounter=1;
			}
		}else{
		bemailText=$("#bemailText").find(".jqte_editor").html();
		BAData+="###"+$("#bemailText").find(".jqte_editor").html();
		}
	}
	if($("#uType"+3).is(":checked")){
		DAData="4";
		if($("#DAUserId").val()==0){
			$("#errordivCommunication").show();
			$("#errordivCommunication").append("&#149; Please select User Type for DA <br>");
			$('#hidaDA').css("background-color", "#F5E7E1");
			$('#hidaDA').focus();
			errorCounter=1;	
		}else{
		DAUserId=$("#DAUserId").val();
		DAData+="###"+$("#DAUserId").val();
		}
		if($("#dtempleteName").val()==null || $("#dtempleteName").val()==""){
			$("#errordivCommunication").show();
			$("#errordivCommunication").append("&#149; Please enter Template Name for DA <br>");
			if(errorCounter==0){
				$('#dtempleteName').css("background-color", "#F5E7E1");
				$('#dtempleteName').focus();
				errorCounter=1;
			}
		}else{
		dtempleteName=$("#dtempleteName").val();
		DAData+="###"+$("#dtempleteName").val();
		}
		if($("#dsubjectLine").val()==null || $("#dsubjectLine").val()==""){
			$("#errordivCommunication").show();
			$("#errordivCommunication").append("&#149; Please enter Subject Line for DA <br>");
			if(errorCounter==0){
				$('#dsubjectLine').css("background-color", "#F5E7E1");
				$('#dsubjectLine').focus();
				errorCounter=1;
			}
		}else{
		dsubjectLine=$("#dsubjectLine").val();
		DAData+="###"+$("#dsubjectLine").val();
		}
		
		dcommStatus=$("input[name='dcommStatus']:checked").val();
		DAData+="###"+$("input[name='dcommStatus']:checked").val();
		if($("#dsetDefault").is(":checked")){
			dsetDefault=1;
			DAData+="###"+dsetDefault;
		}else{
			dsetDefault=0;
			DAData+="###"+dsetDefault;	
		}
		if($("#demailText").find(".jqte_editor").html()==null || $("#demailText").find(".jqte_editor").html()==""){
			$("#errordivCommunication").show();
			$("#errordivCommunication").append("&#149; Please enter Email Text for DA <br>");
			if(errorCounter==0){
				$('#demailText').css("background-color", "#F5E7E1");
				$('#demailText').focus();
				errorCounter=1;
			}
		}else{
		demailText=$("#demailText").find(".jqte_editor").html();
		DAData+="###"+$("#demailText").find(".jqte_editor").html();
		}
	}
	if($("#uType"+4).is(":checked")){
		SAData="5";
		if($("#SAUserId").val()==0){
			$("#errordivCommunication").show();
			$("#errordivCommunication").append("&#149; Please select User Type for SA <br>");
			$('#hidaSA').css("background-color", "#F5E7E1");
			$('#hidaSA').focus();
			errorCounter=1;	
		}else{
		SAUserId=$("#SAUserId").val();
		SAData+="###"+$("#SAUserId").val();
		}
		if($("#stempleteName").val()==null || $("#stempleteName").val()==""){
			$("#errordivCommunication").show();
			$("#errordivCommunication").append("&#149; Please enter Template Name for SA <br>");
			if(errorCounter==0){
				$('#stempleteName').css("background-color", "#F5E7E1");
				$('#stempleteName').focus();
				errorCounter=1;
			}
		}else{
		stempleteName=$("#stempleteName").val();
		SAData+="###"+$("#stempleteName").val();
		}
		if($("#ssubjectLine").val()==null || $("#ssubjectLine").val()==""){
			$("#errordivCommunication").show();
			$("#errordivCommunication").append("&#149; Please enter Subject Line for SA <br>");
			if(errorCounter==0){
				$('#ssubjectLine').css("background-color", "#F5E7E1");
				$('#ssubjectLine').focus();
				errorCounter=1;
			}
		}else{
		ssubjectLine=$("#ssubjectLine").val();
		SAData+="###"+$("#ssubjectLine").val();
		}
		scommStatus=$("input[name='scommStatus']:checked").val();
		SAData+="###"+$("input[name='scommStatus']:checked").val();
		if($("#ssetDefault").is(":checked")){
			ssetDefault=1;
			SAData+="###"+ssetDefault;
		}else{
			ssetDefault=0;
			SAData+="###"+ssetDefault;
		}
		if($("#semailText").find(".jqte_editor").html()==null || $("#semailText").find(".jqte_editor").html()==""){
			$("#errordivCommunication").show();
			$("#errordivCommunication").append("&#149; Please enter Email Text for SA <br>");
			if(errorCounter==0){
				$('#semailText').css("background-color", "#F5E7E1");
				$('#semailText').focus();
				errorCounter=1;
			}
		}else{
		semailText=$("#semailText").find(".jqte_editor").html();
		SAData+="###"+$("#semailText").find(".jqte_editor").html();
		}
	}
		//*******************************************************************************
		if(errorCounter==1){
			return false;
		}else{
		var arrCount=0;
		var dataObjectArray=[];
		if(CData.length>1){
			dataObjectArray[arrCount]=CData;arrCount+=1;
		}
		if(HQAData.length>1){
			dataObjectArray[arrCount]=HQAData;arrCount+=1;
		}
		if(BAData.length>1){
			dataObjectArray[arrCount]=BAData;arrCount+=1;
		}
		if(DAData.length>1){
			dataObjectArray[arrCount]=DAData;arrCount+=1;
		}
		if(SAData.length>1){
			dataObjectArray[arrCount]=SAData;arrCount+=1;
		}
		$('#loadingDiv').show();
		try{
			
			var checkPonintRecordFlag=$('#checkPointRecordFlag').val();
			var districtId=$("#districtId").val();
			if(districtId=="" | districtId==null)
			 districtId=$("#readyonlycheckpointid").val();
			var onBoardingId=$("#onboardID").val();
			var actionType=$("#actionType").val();
			var checkPointId=$("#checkPointId").val();
			var candidateType=$("#checkStatusCandidateType").val();
			var communicationId=$("#communicationId").val();
		    OnboardingDashboardAjax.saveCommunication(communicationId,checkPointId,districtId,onBoardingId,dataObjectArray,actionType,candidateType,checkPonintRecordFlag,{
			async: false,
			callback: function(data)
			{
			displayCommunication();
			$("#communicationCancelbtn").css({"padding-left": "15px"});
			$(".multicandidateType").hide();
			$(".multicandidateTypeBTN").hide();
			resetCommunicationSection();
			showCounter=0;
			},
			errorHandler:handleError 
	    });
		$('#loadingDiv').hide();
		}catch(e){alert(e);}
	  }
}
function saveAndContinueCommunication()
{
	saveAndExitCommunication();
	var candidateType=$("#checkCandidateType").val();
	var checkPointId=$("#checkPointId").val();
	var part=$("#part").val();
	clickView(checkPointId,candidateType,4,1);
	$("#manageCommunication").hide();
	$('#managePermission').modal('hide');
	var checkPonintRecordFlag=$('#checkPointRecordFlag').val();
	displayCheckPointRecord();
}
function activateDeactivateCommunication(communicationId,status){
	$('#loadingDiv').show();
	OnboardingDashboardAjax.activateDeactivateCommunication(communicationId,status,{
		async: false,
		callback: function(data)
		{
		  displayCommunication();			
		},
		errorHandler:handleError
	});
	$('#loadingDiv').hide();
}

function editCommunication(communicationId,type,districtId)
{
	
	var typeArr=['CLi','HQALi','BALi','DALi','SALi'];
	var multiSelectArr=['CAMClass','HQAMClass','BAMClass','DAMClass','SAMClass'];
	var templeteNameArr=['ctempleteName','hqtempleteName','btempleteName','dtempleteName','stempleteName'];
	var subjectLineArr=['csubjectLine','hqsubjectLine','bsubjectLine','dsubjectLine','ssubjectLine'];
	var emailTextArr=['cemailText','hqemailText','bemailText','demailText','semailText'];
	var commStatusArr=['ccommStatus','hqcommStatus','bcommStatus','dcommStatus','scommStatus'];
	var setDefault=['csetDefault','hqsetDefault','bsetDefault','dsetDefault','ssetDefault'];
	showCounter=1;
	$(".multicandidateType").hide();
	$("#communicationId").val(communicationId);
	for(var i=0;i<5;i+=1){
			$(".cType"+i+"Div").hide();
			$("."+typeArr[i]).hide();
			$("."+multiSelectArr[i]).attr("checked",false);
		}
	$('#loadingDiv').show();
	OnboardingDashboardAjax.editCommunication(communicationId,{
		async: false,
		callback: function(data)
		{

		resetCommunicationSection();
		  $("#actionType").val("2");
	      $("#districtId").val(data['districtId']);
		  $("#onboardID").val(data['onBoardingId']);
		  $("#checkPointId").val(data['checkPointId']);
		  $("#checkStatusCandidateType").val(data['candidateType']);
		  /***********************    Show section *********************/
		  var userType=data['userType'];
		  $(".multicandidateTypeBTN").show();
		  $("#communicationCancelbtn").css({"padding-left": "0px"});
		  $("#uType"+Number(userType-1)).attr("checked",true);
		  $(".cType"+Number(userType-1)+"Div").show();
		  $("."+typeArr[Number(userType-1)]).show();
		  //************************* Fields ************************
		  var multiUser=[];
		  var entityType=data['entityType'];
		  //alert("Entity Type :::::: "+entityType+"  Length :::::: "+entityType.length);
		  if(entityType.length==1){
			  multiUser[0]=entityType;
		  }else{
			  multiUser=entityType.split(",");
		  }
		  //alert(" Value   ::::::::::  "+multiUser+" Array Length :::::::::: "+multiUser.length);
		  for(var i=0;i<multiUser.length;i+=1){
			  //alert("--------  "+multiSelectArr[Number(multiUser[i])-1]+"   ===========  "+Number(multiUser[i]));
			  $("."+typeArr[Number(multiUser[i])-1]).show();
			  //alert(" ---------- "+multiUser[i]);
			  $("."+multiSelectArr[Number(multiUser[i])-1]).attr("checked",true);
		  }
		  usertypeMultiSelect(Number(userType));
		  //**************************************************************
		  $("#"+templeteNameArr[Number(userType-1)]).val(data['templeteName']);
		  $("#"+subjectLineArr[Number(userType-1)]).val(data['subjectline']);
		  $("#"+emailTextArr[Number(userType-1)]).find(".jqte_editor").html(data['emailText']);
		  $("input[name="+commStatusArr[Number(userType-1)]+"][value='"+data['status']+"']").prop("checked",true);
		  if(data['setDefault']==1){
			  $("#"+setDefault[Number(userType-1)]).attr("checked",true);
		  }else{
			  $("#"+setDefault[Number(userType-1)]).attr("checked",false);
		  }
		  
		},
		errorHandler:handleError
	});
	$('#loadingDiv').hide();	
}
function resetCommunication()
{
	var multiSelectArr=['CAMClass','HQAMClass','BAMClass','DAMClass','SAMClass'];
	var templeteNameArr=['ctempleteName','hqtempleteName','btempleteName','dtempleteName','stempleteName'];
	var subjectLineArr=['csubjectLine','hqsubjectLine','bsubjectLine','dsubjectLine','ssubjectLine'];
	var emailTextArr=['cemailText','hqemailText','bemailText','demailText','semailText'];
	var commStatusArr=['ccommStatus','hqcommStatus','bcommStatus','dcommStatus','scommStatus'];
	var setDefault=['csetDefault','hqsetDefault','bsetDefault','dsetDefault','ssetDefault'];
	for(var i=0;i<5;i+=1)
	{
		$("."+multiSelectArr[i]).attr("checked",false);
		$("#"+templeteNameArr[i]).val("");
		$("#"+subjectLineArr[i]).val("");
		$("#"+emailTextArr[i]).find(".jqte_editor").html("");
		$("input[name="+commStatusArr[i]+"][value='1']").attr('checked', 'checked');
		$("#"+setDefault[i]).attr("checked",false);
	}
}
function canceleditdiv(){
	$('#editmanageCheckPoint').modal('hide');
}
function showPermissionDiv()
{
	$("#managePermission").modal("show");
}
function cancelPermissionDiv()
{
	$('#permissionDiv').empty();
	$("#managePermission").modal("hide");
}
function clickPermissionView(id,type){
var inActiveFlag=false;
 if($("#cp"+id+type).val()=="1"){
	 $("#cp"+id+type).val("0");
	 $("#"+id+type+"P").css("background-color", "grey");
	 $("#"+id+type+"P").attr('data-original-title', 'No Configured');
	 $("#"+id+type+"P").html(resourceJSON.instN);
 }else{
	 if(id!=3){
		 inActiveFlag=true;
		 $("#cp"+id+type).val("1");
		 $("#"+id+type+"P").css("background-color", "green");
		 $("#"+id+type+"P").attr('data-original-title', 'Configured');
		 $("#"+id+type+"P").html(resourceJSON.instC);
	 }else{
		 $("#cp"+"3"+type).val("1");
		 $("#"+"3"+type+"P").css("background-color", "green");
		 $("#"+"3"+type+"P").attr('data-original-title', 'Configured');
		 $("#"+"3"+type+"P").html(resourceJSON.instC);
		 for(var i=1;i<3;i+=1){
			 $("#cp"+i+type).val("0");
			 $("#"+i+type+"P").css("background-color", "grey");
			 $("#"+i+type+"P").attr('data-original-title', 'No Configured');
			 $("#"+i+type+"P").html(resourceJSON.instN);
		 }
	 }
 }
 if(inActiveFlag && id!=3){
	 $("#cp"+"3"+type).val("0");
	 $("#"+"3"+type+"P").css("background-color", "grey");
	 $("#"+"3"+type+"P").attr('data-original-title', 'No Configured');
	 $("#"+"3"+type+"P").html(resourceJSON.instN);
 }
}

function permissionSaveAndExit(newCheckPointId,candidateTypeNew)
{
	var chkPointPermissionFlag=$('#checkPointRecordFlag').val();
	var counter=$("#noOfPermission").val();
	var permissionArr=[];
	var perStr,count;
	for(var i=0;i<counter;i+=1){
		perStr="";
		count=0;
		for(var j=1;j<4;j+=1){
			if(count==0){
				perStr=$("#"+j+i+"id").val()+"#"+$("#"+j+i+"MId").val()+"#"+$("#"+j+i+"Name").val()+"#"+$("#cp"+j+i).val();
				count=1;
			}else{
			perStr+="#"+$("#cp"+j+i).val();
			}
		}
		permissionArr[i]=perStr;
	}
//*******************************************************************
	$('#loadingDiv').show();
	try{
		var addchkid= $('#checkPointTransID').val();
		var districtId=$("#districtId").val();
		if(districtId=="" | districtId==null)
		 districtId=$('#readyonlycheckpointid').val();
		var onBoardingId=$("#onboardID").val();
		var checkPointId=0;
		var candidateType="";
		if(candidateTypeNew==0){
			checkPointId=$("#checkPointId").val();
			candidateType=$("#checkStatusCandidateType").val();
		}else{
			checkPointId=newCheckPointId;
			candidateType=candidateTypeNew;
		}if(addchkid!=0 & addchkid!=""){
			checkPointId=addchkid;
		}
	    OnboardingDashboardAjax.savePermission(checkPointId,districtId,onBoardingId,permissionArr,candidateType,chkPointPermissionFlag,{
		async: false,
		callback: function(data)
		{
	    	cancelPermissionDiv();
		},
		errorHandler:handleError 
    });
	$('#loadingDiv').hide();
	}catch(e){}
	displayCheckPointRecord();
//*******************************************************
	$('#permissionDiv').empty();
}

function getReadOnlycheckPointDefault()
{
	$('#loadingDiv').show();
	var districtId=$("#districtId").val();
	var onboardingId=$("#onboardID").val();
	try{
	OnboardingDashboardAjax.readOnlyCheckPointDefault(districtId,onboardingId,{
		async: false,
		callback: function(data)
		{
			$('#readOnlyCheckPointDiv').html(data);	
			$(".tempToolTip").tooltip();
		},
		errorHandler:handleError 
});
	$('#loadingDiv').hide();
	}catch(e){alert(e);}
}
function addCheckPoint()
{
	/*$("#errorDivCheckPoint").hide();
	$("#errorDivCheckPoint").empty();
	$('#checkpointName').css("background-color", "#ffffff");
	$("#moreCheckPoint").modal("show");*/
}
function cancelMoreCheckPointDiv()
{
	$("#moreCheckPoint").modal("hide");
}
function saveCheckPoint()
{
	var statusVal="";
	var duplicateCounter=1;
	var districtId=$("#districtId").val();
	var onboardingId=$("#onboardID").val();
	var checkpointName=$("#checkpointName").val();
	var actionNeeded=$('input[name="actionNeeded"]:checked').val();
	var status=$('input[name="checkStatus"]:checked').val();
	var checkPointAllName=$("#checkPointName").val();
	var checkPointArr=checkPointAllName.split("####");
	for(var i=0;i<checkPointArr.length;i+=1){
		if(checkpointName==checkPointArr[i]){
			duplicateCounter=0;
		}
	}
	if(status==1)
		statusVal="A";
	else
		statusVal="I";
	if(duplicateCounter){
	$('#loadingDiv').show();
	try{
	OnboardingDashboardAjax.saveCheckPoint(districtId,onboardingId,checkpointName,actionNeeded,statusVal,{
		async: false,
		callback: function(data)
		{
		},
		errorHandler:handleError 
    });
	$('#loadingDiv').hide();
	cancelMoreCheckPointDiv();
	displayCheckPointRecord();
	}catch(e){alert(e);}
	}else{
		$("#errorDivCheckPoint").show();
		$("#errorDivCheckPoint").append("&#149; Checkpoint name already exist. Please enter another Checkpoint name.<br>");
		$('#checkpointName').css("background-color", "#F5E7E1");
		$("#checkpointName").focus();
	}
}
function displayCheckPointRecord()
{
	$('#loadingDiv').show();
	var districtId=$("#districtId").val();
	var onboardingId=$("#onboardID").val();
	try{
	OnboardingDashboardAjax.checkPointDefault(districtId,onboardingId,{
		async: false,
		callback: function(data)
		{
			$('#checkPointDiv').html(data);	
			$(".tempToolTip").tooltip();
		},
		errorHandler:handleError 
});
	$('#loadingDiv').hide();
	}catch(e){alert(e);}
}
function resetCheckPoint()
{
	$("#checkpointName").val("");
}

function activeInactiveReadOnlyCheckPoint(checkId,status){
	if($("#ck"+checkId).val()=="A"){
		 $("#ck"+checkId).val("I");
		 $("#Tool"+checkId).css("background-color", "grey");
		 $("#Tool"+checkId).attr('data-original-title', 'Inactive');
		 $("#Tool"+checkId).html(resourceJSON.instX);
	}else{
		$("#ck"+checkId).val("A");
		 $("#Tool"+checkId).css("background-color", "green");
		 $("#Tool"+checkId).attr('data-original-title', 'Active');
		 $("#Tool"+checkId).html(resourceJSON.instA);
	}
}
function editReadOnlyCheckPoint(tableType,readOnlyId,checkPointName,Status){
	$("#readOnlyEditSection").show();
	$("#readOnlytableType").val(tableType);
	$("#readOnlyCheckPointId").val(readOnlyId);
	$("#editReadOnlyCheckPointName").val(checkPointName);
	$("#readOnlyCheckPointname").val($('#'+readOnlyId+'editReadOnlyCheckPointName').text());
	if($('#ck'+readOnlyId).val()=="A"){
		$("#readOnlyActive").prop("checked",true);
	}else{
		$("#readOnlyInactive").prop("checked",true);
	}
	
	
}
function hideReadOnlyEditSection(){
	$("#readOnlyEditSection").hide();
	$('#readOnlyCheckPointDuplicateError').empty();
}

function updateReadOnlyCheckPoint(){
	var mastercount= $('#mastercount').val();
	var districtId = $("#readyonlycheckpointid").val();
	var onboardingId=$("#onboardID").val();
	var checkPointId=$("#readOnlyCheckPointId").val();
	var checkpointname="";
	var checkpointstatus="";
	var masterid="";
	var counter=0;
	if(mastercount==0){
		var counter= $('#lasttransid').val();
		var incCounter=parseInt(counter)+5;
		for(var i=counter;i<=incCounter;i++){
			checkpointname+=$('#'+i+'editReadOnlyCheckPointName').text()+"-";
			checkpointstatus+=$('#Tool'+i).text()+"-";
			}
		}
	else{
	for(var i=1;i<=6;i++){
		checkpointname+=$('#'+i+'editReadOnlyCheckPointName').text()+"-";
		checkpointstatus+=$('#Tool'+i+'').text()+"-";
	}
	}
	OnboardingDashboardAjax.updateReadOnlyCheckPoint(mastercount,checkPointId,checkpointname,checkpointstatus,districtId,onboardingId,counter,{
		async: false,
		callback: function(data)
		{
			$('#readOnlyEditSection').hide();
		},
		errorHandler:handleError 
	});
}
function updateCheckPointTransactionJS(){
	var districtId = $("#readyonlycheckpointid").val();
	var onboardingId=$("#onboardID").val();
	OnboardingDashboardAjax.updateCheckPointTransaction(districtId,onboardingId,{
		async: false,
		callback: function(data)
		{
			$('#readOnlyEditSection').hide();
		},
		errorHandler:handleError 
});
}

function updateReadOnlyCheckPointJS(){
	$('#readOnlyCheckPointDuplicateError').empty();
	var type=$("#readOnlytableType").val();
	var checkPointId=$("#readOnlyCheckPointId").val();
	var checkPointname=$("#readOnlyCheckPointname").val();
	var readOnlyCheckPointArr=[];
	var readOnlyCheckPointData=$("#readOnlyCheckPointData").val();
	readOnlyCheckPointArr=readOnlyCheckPointData.split("##");
	var editReadOnlyCheckPointName=$("#editReadOnlyCheckPointName").val();
	var readOnlyCheckPointFlag=false;
	for(var i=0;i<readOnlyCheckPointArr.length;i+=1){
		if(readOnlyCheckPointArr[i].trim().toLowerCase()==checkPointname.trim().toLowerCase()){
			if(editReadOnlyCheckPointName==checkPointname){
				readOnlyCheckPointFlag=false;
			}else{
				readOnlyCheckPointFlag=true;
				$('#readOnlyCheckPointDuplicateError').show();
				$('#readOnlyCheckPointDuplicateError').append("&#149; Read Only CheckPoint already exists<br>");
				$('#readOnlyCheckPointname').focus();
				$('#readOnlyCheckPointname').css("background-color", "#F5E7E1");;
				return false;
			}
		}
	}
	if(readOnlyCheckPointFlag==false){
	var status="";
	if($("#readOnlyActive").is(":checked")){
		$("#ck"+checkPointId).val("I");
		activeInactiveReadOnlyCheckPoint(checkPointId,'I');
	}else{
		$("#ck"+checkPointId).val("A");
		activeInactiveReadOnlyCheckPoint(checkPointId,'A');
	}
	$('#'+checkPointId+'editReadOnlyCheckPointName').text(checkPointname);
	}
	
	$('#readOnlyEditSection').hide();
}
function saveReadOnlyCheckPoint(){
	var districtId = $("#districtId").val();
	var onboardingId=$("#onboardID").val();
	var type=$("#readOnlytableType").val();
	var checkPointId=$("#readOnlyCheckPointId").val();
	var checkPointname=$("#readOnlyCheckPointname").val();
	var status="";
	if($("#readOnlyActive").is(":checked")){
		activeInactiveReadOnlyCheckPoint(checkPointId,'I');
	}else{
		activeInactiveReadOnlyCheckPoint(checkPointId,'A');
	}
	$('#'+checkPointId+'Name').text(checkPointname);
	OnboardingDashboardAjax.updateReadOnlyCheckPoint(checkPointId,checkPointId,checkPointname,status,districtId,onboardingId,{
		async: false,
		callback: function(data)
		{
			$('#readOnlyEditSection').hide();
		},
		errorHandler:handleError 
});
	//************** Record save section ****************************************
	
	try{
		OnboardingDashboardAjax.saveReadOnlyCheckPoint(districtId,onboardingId,type,checkPointId,checkPointname,status,{
			async: false,
			callback: function(data)
			{
				if(data=="true")
					hideReadOnlyEditSection();
			},
			errorHandler:handleError 
	});
		$('#loadingDiv').hide();
		}catch(e){alert(e);}
		getReadOnlycheckPointDefault();
	//****************************Start Add and edit CheckPoint by siva***********************************************
}
$(document).ready(function(){
	$('.crdo1').prop("checked",false);
	$('#chkCandidateType').html('External Candidates');
	var chk=1;shk=1;
	$('.crdo1').click(function(){
		 rdocheckdon=$(this).val();
	});
	
	$('#editsection1').click(function(){
		if(!$('#editsection1').is(':checked')){
			$('#editCandidateTypeCheck').val('1');	
		}else{$('#editCandidateTypeCheck').val('');}
	});
	$('#editsection2').click(function(){
		if(!$('#editsection2').is(':checked')){
		$('#editCandidateTypeCheck2').val('2');	
		}else{$('#editCandidateTypeCheck2').val('');}
	});
	
	
	$('#moredocslink').click(function(){
		$('#saveupload').show();$('#uploadmoredocsdiv').show();
		var counter= $('#inccounter').val();
		counter=parseInt(counter)+1;
		$('#uploadmoredocsdiv').append('<div class="div'+counter+' row mt5" ><div class="col-sm-12 col-md-12 mt5">'
				+'<div class="col-sm-7 col-md-7" style="margin-left:10px;width:56.4%">'
				+'<input type=\"text\" readonly=\"readonly\" id=\"uploadfilename'+counter+'\"  class=\"form-control\"></div>'
				+"<div class=\"col-sm-2 col-md-2 mt10\"><input type=\"file\" onchange='afterFileupload("+counter+");' id=\"documentName"+counter+"\" name=\"documentName"+counter+"\"></div>"
				+"<div class=\"col-sm-2 col-md-2\" style=\"margin-left:8%;\"><a href=\"javascript:;\" onclick=\"removeDiv("+counter+")\" class=\"removeupload\"><i class=\"fa fa-times fa-lg removeupload\"></i></div>"
			+"</div></div>");
		
		
		$('#inccounter').val(counter);
	});
	$('.section1').on('click',function(){
		chk=4;
		candidateTypecheck="";
		if($('.section1').is(":checked")){
			if(shk==4){
				candidateTypecheck=candidateTypecheck+"E-I-T";
				$('#chkCandidateType').html('External / Internal / Internal Transfer Candidates');
			}else{$('#chkCandidateType').html('External / Internal Candidates');candidateTypecheck=candidateTypecheck+"E-I";}
			
		}else{
			chk=1;
			if(shk==4){
				candidateTypecheck=candidateTypecheck+"E-T";
				$('#chkCandidateType').html('External / Internal Transfer Candidates');
			}else{$('#chkCandidateType').html('External Candidates');candidateTypecheck="E";}
			}
	});
	$('.section2').on('click',function(){
		shk=4;
		candidateTypecheck="";
		if($('.section2').is(":checked")){
			if(chk==4){
				candidateTypecheck=candidateTypecheck+"E-I-T";
			$('#chkCandidateType').html('External / Internal / Internal Transfer Candidates');
			}else{
				candidateTypecheck=candidateTypecheck+"E-T";
				$('#chkCandidateType').html('External / Internal Transfer Candidates');
			}
		}else{
			shk=1;
			if(chk==4){
				candidateTypecheck=candidateTypecheck+"E-I";
				$('#chkCandidateType').html('External / Internal Candidates');
				}else{
					$('#chkCandidateType').html('External Candidates');
					candidateTypecheck="E";
				}
			
			}
	});
	
});

function afterFileupload(id){
	var ty= $('#documentName'+id).val().replace(/C:\\fakepath\\/, '');
	$('#uploadfilename'+id).val(ty);
	$('#saveupload').show();
	
} 
function removeDiv(id){
	$('.div'+id).remove();
	$('#inccounter').val(parseInt($('#inccounter').val())-1);
}
function clearAll(){
	$("#checkPointError").empty();
	$("#checkPointError").hide();
	$('#txtchkpointname').css("background-color", "");
	$('.crdo1').prop("checked",false);
	$('#lblchkPointName').html('Checkpoint Name<span class="required">*</span>');
	$('#lblchkType').html('Checkpoint Type<span class="required">*</span>');
	$('#lblchkQuestion').html('Question<span class="required">*</span>');
	$('#lblchkoptions').html('Options<span class="required">*</span>');
	$('#lbldate').html('Date<span class="required">*</span>');
	$('#lbluploaddoc').html('Upload Document<span class="required">*</span>');
	$('#lblrdo').html('Question<span class="required">*</span> ');
	$('#lblrdooptions').html('Options<span class="required">*</span>');
$('#lblsingle').html('Question<span class="required">*</span>');
$('#lblActionPerformedby').html('Action Performed By');
$('#lblstatus').html('Status');$('#lblmultiline').html('Question<span class="required">*</span>');
$('#lblActionPerformedby').html('Action Performed By<span class="required">*</span>');
$('#lblstatus').html('Status<span class="required">*</span>');
for(var i=1;i<=6;i++){
	$('#rdo'+i+'').css("background-color", "");
	$('#opt'+i+'').css("background-color", "");
}
displayCheckPointRecord();
}
function validateCheckPoint(){
	clearAll();
	$("#checkPointError").empty();
	$("#checkPointError").hide();
	$('#checkPointStatusDuplicatError').hide();
	$('#txtchkpointname').css("background-color", "");	
	var c=1;var f=0;
	var chkType=$('#checkPointTypeSelect').val();
	var txtchkname=$('#txtchkpointname').val();
	var validateflag=true;
	var duplicateFlag=false;
	if(txtchkname==""){
		$('#lblchkPointName').html('<span class="DSPQRequired12">CheckPoint Name</span><i class="icon-warning DSPQRequired12"></i>');
		if(f==0){
			$('#txtchkpointname').focus();
			f++;
		}
		validateflag=false;
	}else{
		var checkPointName=$("#checkPointName").val();
		var checkPointNameEdit=$("#checkPointNameEdit").val();
		var checkPointnameArr=[];
		checkPointnameArr=checkPointName.split("####");
		//alert(txtchkname+" ::::::: "+checkPointNameEdit);
		for(var i=0;i<checkPointnameArr.length;i+=1){
			if(checkPointnameArr[i].trim().toLowerCase()==txtchkname.trim().toLowerCase()){
				if(checkPointNameEdit.trim().toLowerCase()==txtchkname.trim().toLowerCase()){
					duplicateFlag=false;
				}else{
					duplicateFlag=true;
					validateflag=false;
					
				}
			}
		}
		if(duplicateFlag==true){
			$("#checkPointError").show();
			$('#checkPointError').append("&#149; Checkpoint Name already exists<br>");
			$('#txtchkpointname').focus();
			$('#txtchkpointname').css("background-color", "#F5E7E1");	
	}
	}
	if(chkType==""){$('#lblchkType').html('<span class="DSPQRequired12">Checkpoint Type</span><i class="icon-warning DSPQRequired12"></i>');
	if(f==0){
	$('#checkPointTypeSelect').focus();}
	f++;
	validateflag=false;
	}
	if(chkType=="22"){
		var checkPointOptionFlag=checkOptionDuplicat('opt');
		if(checkPointOptionFlag==false){
			if($('#txtChkboxQuestion').val()==""){
				$('#lblchkQuestion').html('<span class="DSPQRequired12">Question</span><i class="icon-warning DSPQRequired12"></i>');
				if(f==0){
				$('#txtChkboxQuestion').focus();
				f++;
				}
				validateflag=false;
			}
			
			for(var i=1;i<=6;i++){
				var txtval=$('#opt'+i).val();
				if(txtval!=""){c=parseInt(c)+1;}
			}
			if(c<3){
				$(window).scrollTop(20);
				$('#lblchkoptions').html('<span class="DSPQRequired12">Options</span><i class="icon-warning DSPQRequired12"></i>');
				validateflag=false;
			if(f==0){
			$('#opt1').focus();
			f++;
			}
			
			}
			}else{
				validateflag=false;
			}
			
			
	}if(chkType=="20"){
		if($('#date123').val()==""){
			$('#lbldate').html('<span class="DSPQRequired12">Date</span><i class="icon-warning DSPQRequired12"></i>');
			if(f==0){
			$('#date123').focus();
			f++;
			}
			validateflag=false;
		}
	}
	if(chkType=="23"){
		if($('#uploadfilename1').val()==""){
			$('#lbluploaddoc').html('<span class="DSPQRequired12">Upload Document</span><i class="icon-warning DSPQRequired12"></i>');
			if(f==0){
			$('#documentName1').focus();
			f++;
			}
			validateflag=false;
		}
	}
	if(chkType=="5"){
		if($('.jqte_editor').text()==""){
			$('#lblmultiline').html('<span class="DSPQRequired12">Question</span><i class="icon-warning DSPQRequired12"></i>');
			if(f==0){
			$('.jqte_editor').focus();
			f++;
			}
			validateflag=false;
		}
	}
	if(chkType=="2"){
		var checkPointOptionFlag=checkOptionDuplicat('rdo');
		if(checkPointOptionFlag==false){
		if($('#txtRdoboxQuestion').val()==""){
			$('#lblrdo').html('<span class="DSPQRequired12">Question</span><i class="icon-warning DSPQRequired12"></i>');
			if(f==0){
			$('#txtRdoboxQuestion').focus();
			f++;
			}
			validateflag=false;
		}
		for(var i=1;i<=6;i++){
			var txtval=$('#rdo'+i).val();
			if(txtval!=""){c=parseInt(c)+1;}
		}
		if(c<3){$(window).scrollTop(20);
		$('#lblrdooptions').html('<span class="DSPQRequired12">Options</span><i class="icon-warning DSPQRequired12"></i>');
		validateflag=false;
		if(f==0){
		$('#rdo1').focus();
		f++;
		}
		}
		}else{
			validateflag=true;
		}
	}
	if(chkType=="4"){
		if($('#txtSingleline').val()==""){
			$('#lblsingle').html('<span class="DSPQRequired12">Question</span><i class="icon-warning DSPQRequired12"></i>');
			if(f==0){
			$('#txtSingleline').focus();
			f++;
			}
			validateflag=false;
		}
	}
	if(!$('#candidate').is(':checked') & !$('#user').is(':checked')){
		$('#lblActionPerformedby').html('<span class="DSPQRequired12">Action Performed By</span><i class="icon-warning DSPQRequired12"></i>');
		if(f==0){
		$('#candidate').focus();
		f++;
		}
		validateflag=false;
	}
	var st= $('.status').is(':checked');
	if(st==false){
		$('#lblstatus').html('<span class="DSPQRequired12">Status</span><i class="icon-warning DSPQRequired12"></i>');
		if(f==0){
		$('#statusE').focus();
		f++;
		}
		validateflag=false;
	}
	return validateflag;
}
function clearAllData(){
	$('#saveupload').hide();
	$('#txtSingleline').val('');
	$('#txtChkboxQuestion').val('');
	$('#opt1').val('');
	$('#opt2').val('');
	$('#opt3').val('');
	$('#opt4').val('');
	$('#opt5').val('');
	$('#opt6').val('');
	$('#date123').val('');
	$('#uploadfilename1').val('');
	$('#documentName1').val('');
	$('#uploadmoredocsdiv').empty();
	$('#uploadimagetxt').empty();
	$('#inccounter').val('1');
	$('#editMultipleLineDiv').find(".jqte_editor").text('');
	$('#txtRdoboxQuestion').val('');
	$('#rdo1').val('');
	$('#rdo2').val('');
	$('#rdo3').val('');
	$('#rdo4').val('');
	$('#rdo5').val('');
	$('#rdo6').val('');
}
function checkPointTypeChange(){
	clearAll();clearAllData();
	$('#saveupload').hide();
	$('.div1').show();
	$('#editSingleLineDiv').hide();
	$('#editMultipleLineDiv').hide();
	$('#editRadioTypeDiv').hide();
	$('#editCheckboxTypeDiv').hide();
	$('#editUploadDoc').hide();
	var val= $('#checkPointTypeSelect').val();
	$('#editDateType').hide();
	$('#saveupload').hide();
	$('#editSingleLineDiv').hide();
	$('#editMultipleLineDiv').hide();
	$('#editRadioTypeDiv').hide();
	$('#editCheckboxTypeDiv').hide();
	$('#editUploadDoc').hide();
	var val= $('#checkPointTypeSelect').val();
	$('#editDateType').hide();
	if(val=="4")$('#editSingleLineDiv').show();
	else if(val=="5")$('#editMultipleLineDiv').show();
	else if(val=="2")$('#editRadioTypeDiv').show();
	else if(val=="20")$('#editDateType').show();
	else if(val=="22"){$('#editCheckboxTypeDiv').show();
	$('#editCheckboxTypeLabel').show();
	$('#editChkOptionsDiv').show();}
	else if(val=="23"){$('#editUploadDoc').show();}
	else if(val=="22"){$('#editCheckboxTypeDiv').show();
	$('#editCheckboxTypeLabel').show();
	$('#editChkOptionsDiv').show();}
	else if(val=="23"){
		$('#editUploadDoc').show();
		$('#saveupload').show();
	}
}
function addEditCheckPoint(checkPointId,checkPointName){
	clearAll();
	$('#editAddStatus').hide();
	$('#uploadmoredocsdiv').hide();
	$('#saveupload').hide();
	$('#loadingDiv').show();
	$('#checkPointNameEdit').val(checkPointName);
	for(var op=1;op<=6;op++){
		$('#rdo'+op).val('');
		$('#opt'+op).val('');
	}

	if(checkPointId==0){
		$('#editchkid').val('0');
		$('#editqsnid').val('0');
		$('#editoptid').val('0');
		$('#editprimaryofstatus').val('0');
		$('#editsection1').prop('checked',false);$('#editsection2').prop('checked',false);
		$('select[name^="checkPointTypeSelect"] option[value=""').attr("selected","selected");
		$('#txtchkpointname').val('');
		$('input[name=communicatinreq]').prop('checked',false);
		$('.status').prop('checked',false);
		$('#candidate').prop('checked',false);$('#user').prop('checked',false);
		$('#editUploadDoc').hide()
		;$('#editDateType').hide();
		$('#editSingleLineDiv').hide();
		$('#editMultipleLineDiv').hide();
		$('#editRadioTypeDiv').hide();
		$('#editCheckboxTypeDiv').hide();$('#uploadimagetxt').empty();
		$('#subheadingtext').html("<abel class=\"subheading mb0\">Add Checkpoint</label>");
	}else{
		$('#checkPointTransID').val(checkPointId);
		 var chknam= $('#districtName').val();
		 $('#subheadingtext').html('<label class="subheading mb0">Edit CheckPoint</label> - <label class="subheading mb0" id="editcheckPointname" style="margin-left: 0px;">'+checkPointName+'</label>');
		if($('#section0').is(':checked')){
			$('#editsection0').prop('checked',true);
		}else{$('#editsection0').prop('checked',false);}
		if($('#section1').is(':checked')){
			if($('#editCandidateTypeCheck').val().indexOf('1')!="-1"){
				$('#editsection1').prop('checked',false);
			}else{
				$('#editsection1').prop('checked',true);
			}
		}else{$('#editsection1').prop('checked',false);}
		if($('#section2').is(':checked')){
			if($('#editCandidateTypeCheck2').val().indexOf('2')!="-1"){
				$('#editsection2').prop('checked',false);}
			else{
				$('#editsection2').prop('checked',true);
			}
		}else{$('#editsection2').prop('checked',false);}
	}
	$('#editmanageCheckPoint').modal('show');
	$("#checkPointname").val(checkPointName);
	var districtId;
	var onboardingId;
	var candidateType="E";
	districtId=$("#districtId").val();
	onboardingId=$("#onboardID").val();
	$('#editcheckname').text($("#districtName").val());
	if(checkPointName==""){
	}else{
	 candidateType=$("#checkStatusCandidateType").val();
	}
	
//***********************  For the checkpoint *******************
	
	try{
		if(checkPointId!=0){
			OnboardingDashboardAjax.getCandidateDetails(checkPointId,checkPointName,districtId,onboardingId,{
				async:false,
				callback:function(data){
				editDataDisplay(data,checkPointName);
			},errorHandler:handleError
			});
		}
		
	OnboardingDashboardAjax.checkPointStatusDefault(districtId,onboardingId,checkPointId,null,{
		async: false,
		callback: function(data)
		{
		$('#checkPointStatusRecord').empty();
			$('#checkPointStatusRecord').html(data);	
			$(".tempToolTip").tooltip();
		},
		errorHandler:handleError 
});
		
		OnboardingDashboardAjax.checkPointPermissionDefault(districtId,onboardingId,checkPointId,null,{
			preHook:function(){$('#loadingDivOnBoarding').show()},
			postHook:function(){$('#loadingDivOnBoarding').hide()},
			async: false,
			callback: function(data)
			{
				$('#editCheckPointPermissionRecord').html(data);	
				$(".tempToolTip").tooltip();
			},
			errorHandler:handleError 
	});
		}catch(e){alert(e);}
		$('#loadingDiv').hide();
	//*************************************************************
}
function editDataDisplay(data,checkPointName){
	candidateTypecheck="";
	$('#candidate').prop('checked',false);
	$('#user').prop('checked',false);
	$('#communicatinreq').prop('checked',false);
	$('#editSingleLineDiv').hide();
	$('#editCheckboxTypeDiv').hide();
	$('#editDateType').hide();
	$('#editMultipleLineDiv').hide();
	$('#editRadioTypeDiv').hide();
	$('#editUploadDoc').hide();
	var str= data.split('^');
	$('#editchkid').val(str[9]);
	$('#editqsnid').val(str[10]);
	$('#editoptid').val(str[11]);
	$('#editprimaryofstatus').val(str[12]);
	candidateTypecheck=str[14];
	$('#checkPointTypeSelect option[value="'+str[1]+'"]').attr("selected","selected");
	if(str[0]=="")
		$('#txtchkpointname').val(checkPointName);
	else
	$('#txtchkpointname').val(str[0]);
	if(str[3]=="1"){
	$('#communicatinreq').prop('checked',true);
	}
	if(str[4]=="1"){$('#candidate').prop('checked',true);}
	if(str[5]=="1"){$('#user').prop('checked',true);}
	if(str[6]=="1"){$('#activeE').prop('checked',true);}
	if(str[6]=="0"){$('#inactivE').prop('checked',true);}
	if(str[1]=="4"){
		$('#editSingleLineDiv').show();
		$('#txtSingleline').val(str[2]);
		
	}else if(str[1]=="22"){
		$('#editCheckboxTypeDiv').show();
		$('#txtChkboxQuestion').val(str[2]);
		if(str[7].indexOf("-") > -1){
			var options=str[7].split("-");
			for(var op=1;op<=options.length;op++){
				$('#opt'+op).val(options[op-1]);
			}
		}else{$('#opt1').val(str[7]);}
		if(str[8]!="" & str[8]!=null){
			if(str[8].indexOf("-") > -1){
			var validoptopns= str[8].split('-');
			for(var vo=1;vo<=validoptopns.length;vo++){
				$('#chk'+validoptopns[vo]).prop('checked',true);
			}
			}else{$('#chk'+str[8]).prop('checked',true);}
			
		}
			
	}
	else if(str[1]=="20"){
		$('#editDateType').show();
		$('#date123').val(str[2]);
	}
	else if(str[1]=="5"){
		$('#editMultipleLineDiv').show();
		$('#editMultipleLineLabel').show();
		$('.jqte_editor').text(str[2]);
	}
	else if(str[1]=="23"){
		$('#editUploadDoc').show();
		$('#uploadmoredocsdiv').empty();
		$('#uploadimagetxt').empty();
		var docs=str[13].split(',');
		for(var d=1;d<=docs.length;d++){
			$('#uploadimagetxt').show().append('<div class="col-sm-3 col-md-3 pleft0"><label id="removeuploadfile'+d+'">'+docs[d-1]+''
					+'<a href="javascript:void();" data-toggle="tooltip" title="Remove document" onclick="deleteuploadfile('+d+')"><img width="15" height="15" class="can" src="images/can-icon.png"></a>' 
					+'</label></div>');
			if(d==1){
				$('#uploadfilename1').val(docs[0]);
			}
			else{
				$('#uploadmoredocsdiv').show();
			$('#uploadmoredocsdiv').append('<div class="div'+d+' row mt5" ><div class="col-sm-12 col-md-12 mt5">'
					+'<div class="col-sm-7 col-md-7" style="margin-left:10px;width:56.4%">'
					+'<input type=\"text\" readonly=\"readonly\" id=\"uploadfilename'+d+'\" value='+docs[d-1]+' class=\"form-control\"></div>'
					+"<div class=\"col-sm-2 col-md-2 mt10\"><input type=\"file\" onchange='afterFileupload("+d+");' id=\"documentName"+d+"\" name=\"documentName"+d+"\"></div>"
					+"<div class=\"col-sm-2 col-md-2\" style=\"margin-left:8%;\"><a href=\"javascript:;\" onclick=\"removeDiv("+d+")\" class=\"removeupload\"><i class=\"fa fa-times fa-lg removeupload\"></i></div>"
				+"</div></div>");
			}
			
		}
		$('#inccounter').val(docs.length);
	}
	else if(str[1]=="2"){
		$('#editRadioTypeDiv').show();
		$('#txtRdoboxQuestion').val(str[2]);
		if(str[8]!="" & str[8]!=null){
			$('#crdo'+str[8]).prop('checked',true);
		}
		if(str[7].indexOf("-") > -1){
		var options=str[7].split("-");
		for(var op=1;op<=options.length;op++){
			$('#rdo'+op).val(options[op-1]);
		}
		}else{$('#rdo1').val(str[7]);}
		if(str[8]!="" & str[8]!=null){
			if(str[8].indexOf("-") > -1){
			var validoptopns= str[8].split('-');
			for(var vo=1;vo<=validoptopns.length;vo++){
				$('#crdo'+validoptopns[vo]).prop('checked',true);
			}
			}else{$('#crdo'+str[8]).prop('checked',true);}
			
		}
	}
}
function showUploadFiles(){
	$('#uploadimagetxt').empty();
	var counter= $('#inccounter').val();
	$('#lbluploaddoc').html('Upload Document<span class="required">*</span>');
	$('#saveupload').hide();
	for(var i=1;i<=counter;i++){
		var txtval= $("#uploadfilename"+i).val();
		if($("#uploadfilename"+i).val()!=""){
			if(typeof txtval==="undefined"){$("#div"+i).hide();}else{
$('#uploadimagetxt').show().append('<div class="col-sm-3 col-md-3 pleft0"><label id="removeuploadfile'+i+'">'+$("#uploadfilename"+i).val()+''
		+'<a href="javascript:void();" data-toggle="tooltip" title="Remove document" onclick="deleteuploadfile('+i+')"><img width="15" height="15" class="can" src="images/can-icon.png"></a>' 
		+'</label></div>');
			}
			
		}else{
			if(i==1){
			$('#lbluploaddoc').html('<span class="DSPQRequired12">Upload Document</span><i class="icon-warning DSPQRequired12"></i>');
			}else{
			$(".div"+i).hide();
			}
		}
	}
	
}
function deleteuploadfile(id){
	$('#deletequestionid').val(id);
	$('#editmanageCheckPoint').modal('hide');
	$('#validateconfirm').modal('show');
	
}
function yes(){
	var delid= $('#deletequestionid').val();
	if(delid==1){
		$('#inccounter').val('1');
	}
	$('#removeuploadfile'+delid).empty();
	$('#documentName'+delid).val('');
	$('#uploadfilename'+delid).val('');
	$('.div'+delid).empty();
	$('#validateconfirm').modal('hide');
	$('#editmanageCheckPoint').modal('show');
	
	
}
function No(){
	$('#validateconfirm').modal('hide');
	$('#editmanageCheckPoint').modal('show');
}

function cancelUploadFiles(){
	$('#uploadmoredocsdiv').empty();
	$('#inccounter').val('1');
	$('#uploadfilename1').val('');
	$('input[type=file]').val('');
	$('#saveupload').hide();
}
function pushToAryKeyVal(name, val) {
	  
    
	   if(manageButtonsActivity[name]!= undefined){
		   var fieldStatus = manageButtonsActivity.name;
		   if(fieldStatus=="A")
			   manageButtonsActivity.push({name : val});		   
	   }else{		   
		   manageButtonsActivity.push({name : val});
	   }
}

function uploadFiles(){
	var formData = new FormData();
	//********************************   Append all files *****************************************************
	var fileSizeCounter=$('#inccounter').val();
	for(var i=1;i<=fileSizeCounter;i++){
		formData.append('file'+i,document.getElementById("documentName"+i).files[0]);	
	}
    formData.append('uploadFilePath','teacherRootPath');
    formData.append('fileLocation','Document');
	 $.ajax({
	        url : 'multipleDocumentUpload.do',
	        data : formData,
	        processData : false,
	        contentType : false,
	        async: false,
	        type : 'POST',
	        success : function(data) {
	    	$('#uploadfilenames').val(data);
	        },
	        error : function(err) {
	            alert(err);
	        }
	    });
	 
}

//******************************************** End Add and edit CheckPoint by siva *******************************************
function setEntityType()
{
	var entityString='<label>Entity Type</label><select class="form-control" id="entityType" name="entityType" class="span3" onchange="displayOrHideSearchBox();"><option value="1" selected="selected">TM</option><option value="2">District</option>';
  	var district="<div class='col-sm-12 col-md-12 hide pleft0' id='districtSearchBox'> <span><label>District Name</label>"+
  		 "<input autocomplete='off' style='color:#555555; font-size: 14px;'  type='text' id='districtSearchName'  value='' maxlength='100'  name='districtSearchName' class='help-inline form-control'"+
  	 	 "onfocus=\"getDistrictAuto(this, event, 'divTxtSearchShowData', 'districtSearchName','districtSearchId','');\""+
  	      "onkeyup=\"getDistrictAuto(this,event,'divTxtSearchShowData', 'districtSearchName','districtSearchId','');\""+
  	      "onblur=\"hideDistrictMasterDiv(this,'districtSearchId','divTxtSearchShowData');\"/></span>"+
  		  "<input type='hidden' id='districtSearchId'/>"+
  	       "<div id='divTxtSearchShowData'  onmouseover=\"mouseOverChk('divTxtSearchShowData','districtSearchName')\" style='display:none;position:absolute;z-index:5000;' class='result' ></div>"+
  	        "</div>";
  	var btnRecord="<button onclick=\"searchRecordByDistrict();\" class='flatbtn' style='background: #9fcf68;width:60%' id='search' title='Search'><strong>Search <i class='fa fa-check-circle'></i></strong></button>";
  		globalSearchSection(entityString,"","",district,btnRecord);
  		setTimeout(function(){ 
	  		if($("#districtMasterId").val()!=0){
	  			$("select#entityType").prop('selectedIndex', 1);
	  			displayOrHideSearchBox();
	  			$("#districtSearchName").val($("#districtMasterName").val());
	  			$("#districtSearchId").val($("#districtMasterId").val());
	  			hideSearchAgainMaster();
	  		}else{
	  			showSearchAgainMaster();
	  		}
	}, 100);
}
function saveAndUpdateCheckPoint(){
	var validatFlag=validateCheckPoint();
	var duplicateFlag=false;
	var chkid=$('#editchkid').val();
	if(validatFlag==true){
		$('#loadingDiv').show();
		$('#uploadfilenames').val('');
		var districtId=$("#districtId").val();
		var onboardID=$("#onboardID").val();
		var checkPointName=$("#checkPointName").val();
		var checkPointNameEdit=$("#checkPointNameEdit").val();
		var txtchkname=$('#txtchkpointname').val();
		/*var checkPointnameArr=[];
		checkPointnameArr=checkPointName.split("####");
		var txtchkname=$('#txtchkpointname').val();
		//alert(txtchkname+" ::::::: "+checkPointNameEdit);
		for(var i=0;i<checkPointnameArr.length;i+=1){
			if(checkPointnameArr[i].trim().toLowerCase()==txtchkname.trim().toLowerCase()){
				if(checkPointNameEdit.trim().toLowerCase()==txtchkname.trim().toLowerCase()){
					duplicateFlag=false;
				}else{
					duplicateFlag=true;
				}
			}
		}
		if(duplicateFlag==true){
			$("#checkPointError").show();
			$('#checkPointError').append("&#149; Checkpoint Name already exists<br>");
			$('#txtchkpointname').focus();
			$('#txtchkpointname').css("background-color", "#F5E7E1");	
		}else{*/
		var comreq="0";
		var chkPointTypeQuestion="";
		var actionperformd="";var cand="0";var user="0";
		var validateflag=true;
		var chkPointTypeSelectId=$('#checkPointTypeSelect').val();
		if(chkPointTypeSelectId=="22"){
			chkPointTypeQuestion=$('#txtChkboxQuestion').val();
		}else if(chkPointTypeSelectId=="20"){
			chkPointTypeQuestion=$('#date123').val();
		}
		else if(chkPointTypeSelectId=="4"){
			chkPointTypeQuestion=$('#txtSingleline').val();
		}
		else if(chkPointTypeSelectId=="2"){
			chkPointTypeQuestion=$('#txtRdoboxQuestion').val();
		}
		else if(chkPointTypeSelectId=="5"){
			chkPointTypeQuestion=$('#editMultipleLineDiv').find(".jqte_editor").text();//$('.jqte_editor').text();
			
		}
		else if(chkPointTypeSelectId=="23"){
			 uploadFiles();
			
		}
		if($('#communicatinreq').is(':checked')){
			comreq="1";
		}
		if($('#candidate').is(':checked')){
			cand="1";
		}
		if($('#user').is(':checked')){
			user="1";
		}
		actionperformd=cand+","+user;
		var st= $('input[name=statusE]:radio:checked').val();
	OnboardingDashboardAjax.addCheckpoint(districtId,onboardID,txtchkname,comreq,actionperformd,st,chkid,{
		async: false,
		callback: function(data)
		{
		 $('#checkPointTransID').val(data);
		 $('#checkPointRecordFlag').val('1');
		 $('#editchkid').val('0');
		 saveQuestion(chkPointTypeSelectId,chkPointTypeQuestion)
		 $('#loadingDiv').hide();
		},
		errorHandler:handleError 
	});
	displayOnBoarding();			 
}
function saveQuestion(chkPointTypeSelectId,chkPointTypeQuestion){
	var filesUploaded= $('#uploadfilenames').val();
	var editqsn=$('#editqsnid').val();
	var ac=0;
	var districtId=$("#districtId").val();
	var onboardID=$("#onboardID").val();
	var txtchkname=$('#txtchkpointname').val();
	var comreq="0";
	var chkpointid= $('#checkPointTransID').val();
	var validateflag=true;
	if($('#communicatinreq').is(':checked')){
		comreq="1";
	}
	OnboardingDashboardAjax.addCheckpointQuestion(onboardID,districtId,chkPointTypeSelectId,chkPointTypeQuestion,candidateTypecheck,chkpointid,editqsn,filesUploaded,{
		async: false,
		callback: function(data)
		{
		$('#checkpointQuestionID').val(data);
		saveOptions();
		saveCheckPointTrans();
		permissionSaveAndExit(data,candidateTypecheck);
		},
		errorHandler:handleError 
	});
	displayCheckPointRecord();
	canceleditdiv();
}
function saveOptions(){
	var chkType=$('#checkPointTypeSelect').val();
	var qid=$('#checkpointQuestionID').val();
	var editoptid= $('#editoptid').val();
	
	if(chkType==22){
		var chkdata=getChkBoxData();
		OnboardingDashboardAjax.addCheckPointOptions(qid,chkdata,editoptid,{
			async: false,
			callback: function(data)
			{
			},
			errorHandler:handleError 
		});
	}
	if(chkType==2){
	var rdodata=getRdoBoxData();
		OnboardingDashboardAjax.addCheckPointOptions(qid,rdodata,editoptid,{
			async: false,
			callback: function(data)
			{
			},
			errorHandler:handleError 
		});
	}
}
function getChkBoxData(){
	var txtval="";
	var chkval="";
	for(var i=1;i<=6;i++){
		if($('#opt'+i).val()!=""){
			if(txtval.indexOf($('#opt'+i).val())==-1){
			txtval+=$('#opt'+i).val()+"-";
			}
		}
		if($('#chk'+i).is(":checked")){
			chkval+=""+i+"-";
		}
	}
	return txtval+","+chkval+"-";
}
function getRdoBoxData(){
	var txtval="";
	var chkval="";
	for(var i=1;i<=6;i++){
			txtval+=$('#rdo'+i).val()+"-";
	}
	rdocheckdon=rdocheckdon+"-";
	return txtval+","+rdocheckdon;
}
}
function checkOptionDuplicat(id){
	var txtval="";
	var txtoptionarry=[];
	var counter=0;
	var checkPointOptionFlag=false;
	for(var i=1;i<=6;i++){
		if($('#'+id+i+'').val()!=""){
			txtoptionarry[i]=$('#'+id+i+'').val();
		}
		
	}
	for(var i=1;i<=6;i++){
		if($('#'+id+i+'').val()!=""){
			for(var option=0;option<txtoptionarry.length;option+=1){
				if(txtoptionarry[option]==$('#'+id+i+'').val()){
					counter=counter+1;
				}
			}
			if(counter>1){
				checkPointOptionFlag=true;
				$("#checkPointError").show();
				$('#checkPointError').append("&#149; Duplicate CheckPoint Option<br>");
				$('#'+id+i+'').focus();
				$('#'+id+i+'').css("background-color", "#F5E7E1");
				i=7;
			}else
			txtval+=$('#'+id+i+'').val()+"-";
			
		}
		counter=0;
		
	}
	return checkPointOptionFlag;
}

function addCheckPointStatusByJS()
{
	$("#checkPointStatusDuplicatError").hide();
	var finalstr="";
	var chkpointnames="";
	var checkPointStatsFlag=false;
	var statusname=$("#statusname2").val();
	var checkStatsNa=$("#checkStatsNa").val();
	var checkPointNameArr=[];
	checkPointNameArr=checkStatsNa.split("#");
	var editCheckPointStatusNameEdit=$("#editCheckPointStatusNameEdit").val();
	for(var i=0;i<checkPointNameArr.length;i+=1){
		if(checkPointNameArr[i].trim().toLowerCase()==statusname.trim().toLowerCase()){
			if(editCheckPointStatusNameEdit.trim().toLowerCase()==statusname.trim().toLowerCase()){
				checkPointStatsFlag=false;
			}else{
				checkPointStatsFlag=true;
			}
		}
	}
	if(checkPointStatsFlag==false){
	var decisionpoint=0;
	if($("#decisionpoint2").is(":checked")){
		decisionpoint=1;
	}else{
		decisionpoint=0;
	}
	var colorpicker=$("[name=colorpicker2]").val();
	var status="A";
	if($('input[name=status2]:checked').val()==1){
		status="A";
	}else{
		status="I";
	}
	var counter=$("#checkStatsCounter").val();
	var flagShow=$("#showActionTypeCheckPointStatus").val();
	if(flagShow==1){
//***************** Create new status div ***************
	finalstr+='<div class="row mb5 left10">'
			+'<div class="col-sm-2 col-md-2 pleftright">'
			+'<span id=name_'+counter+'>'+statusname+' </span>'
			+'<span class="tempToolTip" data-toggle="tooltip" data-placement="top" data-original-title="'+statusname+'" title="'+statusname+'">'
			+'<img src="images/qua-icon.png"></span> </div>'
			+'<div class="col-sm-1 col-md-1 showhideedit top5" style="margin-left:2px;">'
			+'<a class="tempToolTip" data-toggle="tooltip" data-placement="top" data-original-title="Edit" href="javascript:void(0);" onclick="editCheckPointStatus(1,'+(parseInt(counter)+1)+','+(counter)+');"><i class="fa fa-pencil-square-o fa-lg"></i></a></div>'
			+'<div class="col-sm-3 col-md-3 managewidth"></div><div class="col-sm-3 col-md-3 txtalign" style="margin-left:5px;">'
			+'<div class="lfloat"><input type="hidden" id="'+counter+'dId" name="'+counter+'DId" value="'+decisionpoint+'">';
				if(decisionpoint==1)
					finalstr+='<label class="circlegrey green tempToolTip" id="'+counter+'D" data-toggle="tooltip" data-placement="top" data-original-title="Selected as Decision Point">D</label>';
				else
					finalstr+='<label class="circlegrey tempToolTip" id="'+counter+'D" data-toggle="tooltip" data-placement="top" data-original-title="Not selected as Decision Point">D</label>';
				finalstr+='</div><div class="lfloat"><input type="hidden" id="'+counter+'CId" name="'+counter+'CId" value="'+colorpicker+'">';
			
			if(colorpicker=="#808080" || colorpicker=="grey")
				finalstr+='<label class="circlegrey tempToolTip" id="'+counter+"C"+'" data-toggle="tooltip" data-placement="top" data-original-title="No Color defined">C</label>';
			else
				finalstr+='<label class="circlegrey tempToolTip" style="background-color:'+colorpicker+'"+ id="'+counter+"C"+'" data-toggle="tooltip" data-placement="top" data-original-title="Color defined">C</label>';
		
			finalstr+='</div><div class="lfloat"><input type="hidden" id="'+counter+"_"+counter+"_1edit"+'" value="'+status+'">';
			
			if(status=="A")
				finalstr+='<label class="circlegrey green tempToolTip" id="'+counter+"_IT"+'" title="" data-toggle="tooltip" data-placement="top" data-original-title="Active">A</label>';
			else
				finalstr+='<label class="circlegrey tempToolTip" id="'+counter+"_IT"+'" title="" data-toggle="tooltip" data-placement="top" data-original-title="Inactive">I</label>';
			
			finalstr+='</div></div></div>';
			$("#checkPointStatusRecord").append(finalstr);
			counter++;
			$("#checkStatsCounter").val(counter);
			$("#statusname2").val('');
			$('.bfh-colorpicker-icon').css('background-color','rgb(128, 128, 128)');
			$("[name=colorpicker2]").val('#000000');
			$('#editAddStatus').hide();
//*******************************************************
	}else{
		var noRecord=$("#checkNoRecord").val();
		$('#colorpicker2').val(colorpicker);
		$('#name_'+noRecord).text(statusname);
		if(decisionpoint==1){
			$('#'+noRecord+'dId').val('1');
			$('#'+noRecord+'D').removeClass("circlegrey tempToolTip").addClass("circlegrey green tempToolTip");
			$('#'+noRecord+'D').attr('data-original-title','Selected as Decision Point');
		}
		else{
			$('#'+noRecord+'dId').val('0');
			$('#'+noRecord+'D').removeClass("circlegrey green tempToolTip").addClass("circlegrey tempToolTip");
			$('#'+noRecord+'D').attr('data-original-title','Not selected as Decision Point');
			
		}
		if(colorpicker=="#808080" || colorpicker=="grey"){
			$('#'+noRecord+'C').attr('data-original-title','No color defined').css('background-color',colorpicker);
			$('#'+noRecord+'CId').val(colorpicker);
		}else{
		$('#'+noRecord+'C').css('background-color',colorpicker);
		$('#'+noRecord+'D').attr('data-original-title','Color defined');
		$('#'+noRecord+'CId').val(colorpicker);
		}
		if(status=="A"){
			$('#'+noRecord+'_1T').removeClass("circlegrey tempToolTip").addClass("circlegrey green tempToolTip");
			$('#'+noRecord+'_1T').text('A');
		$("#"+noRecord+"_1T").attr('data-original-title','Active');
		$('#'+noRecord+'_'+noRecord+'_1edit').val('A');
		}
		else if(status=="I"){
			$('#'+noRecord+'_1T').removeClass("circlegrey green tempToolTip").addClass("circlegrey tempToolTip");
			$('#'+noRecord+'D').attr('data-original-title','Inactive');
		$('#'+noRecord+'_1T').text('X');
		$('#'+noRecord+'_'+noRecord+'_1edit').val('I');
		$("#"+noRecord+"_1T").attr('data-original-title','Inactive');
		}
		$('#editAddStatus').hide();
	}
	}else{
		$('#checkPointStatusDuplicatError').empty();
		$("#checkPointStatusDuplicatError").show();
		$('#checkPointStatusDuplicatError').append("&#149; Checkpoint status Name already exists<br>");
		$('#statusname2').focus();
		$('#statusname2').css("background-color", "#F5E7E1");
		
		
	}
}
function saveCheckPointTrans(){
	var c=$('#checkStatsCounter').val();
	var initalCounter=$('#editInitialCounter').val();
	var chkid= $('#checkPointTransID').val();
	var districtId=$("#districtId").val();
	var onboardID=$("#onboardID").val();
	var ids=$('#editprimaryofstatus').val();
	var namesstr="";
	 var dPointStr="";
	 var colorStr2="";
	 var statusStr="";
	for(var i=0;i<c;i++){
		namesstr+=$('#name_'+i).text()+"-";
		dPointStr+=$('#'+i+'dId').val()+"-"
		colorStr2+=$('#'+i+'CId').val()+"-";
		statusStr+=$('#'+i+'_'+i+'_1edit').val()+"-";
		
	}
	OnboardingDashboardAjax.addCheckPointStatus(onboardID,chkid,candidateTypecheck,districtId,namesstr,dPointStr,colorStr2,statusStr,c,ids,initalCounter,{
		async: false,
		callback: function(data)
		{
		
		},
		errorHandler:handleError 
	});
}
function saveCheckPointAction(){
	var checkPointId=0;
	var districtId = $("#districtId").val();
	var onboardingId=$("#onboardID").val();
	var checkPointIds=$("#checkPointIds").val();
	var checkPointArr=[];
		checkPointArr=checkPointIds.split("##");
	var candType=['E','I','T'];
	var actionWithCheckPointIdArr=[];
	var candidateType=[];
	var rowActionData="";
	var rowStatusData="";
	var checkPointActionId="";
	for(var i=0;i<3;i+=1){
		if($("#section"+i).is(":checked")){
			candidateType[i]=candType[i];
		}
	}
	for(var j=0;j<checkPointArr.length;j+=1){
		checkPointId=checkPointArr[j];
		rowActionData="";
		rowStatusData="";
		checkPointActionId="";
		for(var i=0;i<3;i+=1){
			if($("#section"+i).is(":checked")){
				if(rowActionData==""){
					rowActionData=$("#"+checkPointId+"3"+candType[i]+candType[i]+"id3").val();
					rowStatusData=$("#"+checkPointId+"_"+checkPointId+"_"+(i+1)).val();
					checkPointActionId=$("#"+checkPointId+"3"+candType[i]+candType[i]+"3").val();
				}else{
					rowActionData=rowActionData+"::"+$("#"+checkPointId+"3"+candType[i]+candType[i]+"id3").val();
					rowStatusData=rowStatusData+"::"+$("#"+checkPointId+"_"+checkPointId+"_"+(i+1)).val();
					checkPointActionId=checkPointActionId+"::"+$("#"+checkPointId+"3"+candType[i]+candType[i]+"3").val();
				}
			}
		}
		actionWithCheckPointIdArr[j]=checkPointId+"###"+rowActionData+"###"+rowStatusData+"###"+checkPointActionId;
	}
	OnboardingDashboardAjax.saveCheckPointAction(districtId,onboardingId,actionWithCheckPointIdArr,candidateType,{
		async: false,
		callback: function(data)
		{
		
		},
		errorHandler:handleError 
	});
}
function actionClickView(checkPointActionId,checkPointId,sectionId,part,checkPointName)
{
	var secId=Number(sectionId);
	var appType=['E','I','T'];
	var status=$("#"+checkPointId+part+appType[secId-1]+appType[secId-1]+"id"+part).val();
	if(status=="1"){
		//$("#"+checkPointId+part+appType[secId-1]+appType[secId-1]+part).val(checkPointActionId);
		$("#"+checkPointId+part+appType[secId-1]+appType[secId-1]+"id"+part).val(0);
		$("#"+checkPointId+part+appType[secId-1]+part).attr('data-original-title', 'No action required')
		$("#"+checkPointId+part+appType[secId-1]+part).css('background-color', 'grey');
	}else{
		//$("#"+checkPointId+part+appType[secId-1]+appType[secId-1]+part).val(checkPointActionId);
		$("#"+checkPointId+part+appType[secId-1]+appType[secId-1]+"id"+part).val(1);
		$("#"+checkPointId+part+appType[secId-1]+part).attr('data-original-title', 'Action required')
		$("#"+checkPointId+part+appType[secId-1]+part).css('background-color', 'green');
	}

}