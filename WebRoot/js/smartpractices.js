var count=0;
var index=-1;
var length=0;
var divid='';
var txtid='';
var isBranchExistsArray = new Array();
var hiddenDataArray = new Array();
var showDataArray = new Array();
var degreeTypeArray = new Array();
var hiddenId="";

function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert("Oops: Your Session has expired!");  document.location = 'signin.do';}
	else{alert("Server Error: "+exception.javaClassName);}
}

function getBranchMasterAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{	
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38)
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13)
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		document.getElementById("branchName").focus();
	} 
	else if(event.keyCode==9)
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		//alert("02");
		searchArray = getBranchArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	} 
}

function getBranchArray(branchName){
	
	//alert("03");
	var hqId=document.getElementById("headQuarterId").value;
	var searchArray = new Array();
	AutoSearchFilterAjax.getActiveBranchListByHQ(hqId,branchName,{  
		async: false,		
		callback: function(data){ 
		hiddenDataArray = new Array();
		showDataArray = new Array();
		for(i=0;i<data.length;i++){
			searchArray[i]=data[i].branchName;
			hiddenDataArray[i]=data[i].branchId;
			showDataArray[i]=data[i].branchName;
		}
	}
	});	

	return searchArray;
}

function hideBranchMasterDiv(dis,hiddenId,divId)
{
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
		/*if(dis.value!="")
		{
			var cnt=0;
			var focs=0;	
			$('#errordiv').empty();	
			$('#errordiv').append("&#149; Please enter valid Branch<br>");
		}*/
	}
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	//getDistrictWiseSubject(document.getElementById(hiddenId).value);
	index = -1;
	length = 0;
}

var selectFirst="";

var fatchData= function(txtSearch,searchArray,txtId,txtdivid){
	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		//alert(" length :: "+len);
		if(document.getElementById(txtId).value!="")
		{			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i] + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
				
			}
			
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}

var downArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index<length-1){
			for(var i=0;i<10;i++){
				{
					if(document.getElementById('divResult'+txtdivid+i))
						var div_id=document.getElementById('divResult'+txtdivid+i);
					if(div_id)
					{
						if(div_id.className=='over')
							index=div_id.id.split('divResult'+txtdivid)[1];
						div_id.className='normal';
					}
				}
			}
			index++;
			if(document.getElementById('divResult'+txtdivid+index))
			{
				var div_id=document.getElementById('divResult'+txtdivid+index);
				div_id.className='over';
				document.getElementById(txtid).value = div_id.innerHTML;
				selectFirst=div_id.innerHTML;
			}
		}
	}
}

var upArrowKey = function(txtdivid){
	
	if(txtdivid)
	{
		if(index>0){
			for(var i=0;i<length;i++){

				var div_id=document.getElementById('divResult'+txtdivid+i);
				if(div_id)
				{
					if(div_id.className=='over')
						index=div_id.id.split('divResult'+txtdivid)[1];
					div_id.className='normal';
				}
			}
			index--;
			if(document.getElementById('divResult'+txtdivid+index))
				document.getElementById('divResult'+txtdivid+index).className='over';
			if(txtid && document.getElementById('divResult'+txtdivid+index))
			{
				document.getElementById(txtid).value=
					document.getElementById('divResult'+txtdivid+index).innerHTML;
				selectFirst=document.getElementById('divResult'+txtdivid+index).innerHTML;
			}
		}
	}
}

function mouseOverChk(txtdivid,txtboxId)
{	
	for(var i=0;i<length;i++)
	{
		document.getElementById('divResult'+txtdivid+i).className='normal';		
		if($('#divResult'+txtdivid+i+':hover').length!=0) 
		{
			document.getElementById('divResult'+txtdivid+i).className='over';	       
	       	document.getElementById(txtboxId).value= $('#divResult'+txtdivid+i).text();
	        index=i;
	    }
	
	}
}


function fireMouseOverEvent(event)
{
	for(var i=0;i<length;i++)
	{	
		document.getElementById('divResult'+event.data.param2+i).className='normal';
	}
   document.getElementById('divResult'+event.data.param2+event.data.param1).className='over';	       
  	document.getElementById(event.data.param3).value= $('#divResult'+event.data.param2+event.data.param1).text();
   index=event.data.param1;
}

function __mouseOverChk(txtdivid,txtboxId)
{	
	for(var i=0;i<length;i++)
	{
		document.getElementById('divResult'+txtdivid+i).className='normal';		
		if ($('#divResult'+txtdivid+i).is(':hover')) 
		{
			document.getElementById('divResult'+txtdivid+i).className='over';	       
	       	document.getElementById(txtboxId).value= $('#divResult'+txtdivid+i).text();
	        index=i;
	    }
	}

}

var overText = function (div_value,txtdivid) 
{
	for(var i=0;i<length;i++)
	{
		if(document.getElementById('divResult'+i))
		{
			var div_id=document.getElementById('divResult'+i);

			if(div_id.className=='over')
				index=div_id.id.split(txtdivid)[1];
			div_id.className='normal';
		}
	}
	div_value.className = 'over';
	document.getElementById(txtid).value= div_value.innerHTML;
}
function trim(s)
{
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}
// ************** District ***************
function getDistrictAuto(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("districtName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getDistrictArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getDistrictArray(districtName){
	var searchArray = new Array();
	var branchId=0;
	try {
		document.getElementById("branchId").value;
	} catch (e) {
		// TODO: handle exception
	}
	
	if(branchId==null || branchId=="")
		branchId=0;
	
	var headQuarterId=document.getElementById("headQuarterId").value;
	if(headQuarterId==null || headQuarterId=="")
		headQuarterId=0;
	AutoSearchFilterAjax.getFieldOfDistrictListByBranchAndHead(districtName,branchId,headQuarterId,{ 
		//BatchJobOrdersAjax.getFieldOfDistrictList(districtName,{ 
			async: false,
			callback: function(data){
			hiddenDataArray = new Array();
			showDataArray = new Array();
			for(i=0;i<data.length;i++){
				searchArray[i]=data[i].districtName;
				showDataArray[i]=data[i].districtName;
				hiddenDataArray[i]=data[i].districtId;
			}
		},
		errorHandler:handleError
		});	
	return searchArray;
}

function hideDistMasterDiv(dis,hiddenId,divId)
{
	if(document.getElementById("districtName").value==""){
		//document.getElementById('schoolName').readOnly=true;
		//document.getElementById('schoolName').value="";
		//document.getElementById('schoolId').value="0";
	}
	document.getElementById("districtId").value="";
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			document.getElementById("districtId").value=hiddenDataArray[index];
			//document.getElementById('schoolName').readOnly=false;
			//document.getElementById('schoolName').value="";
			//document.getElementById('schoolId').value="0";
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}


/*****************Make three field for use this Auto suggest   ******************
 * 
 		inputIdHeadQuarterName ::::::	onlyHeadQuarterName
		hiddenFieldHeadQuarter ::::::	headQuarterHiddenId
		headQuarterExistsOrNot ::::::	isBranchExistsHiddenId
 */
function getOnlyHeadQuarterAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	//alert('enter1');
	//document.getElementById("onlyHeadQuarterName").value="";
	document.getElementById("headQuarterHiddenId").value="";
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("onlyHeadQuarterName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getHeadQuarterOnlyArray(txtSearch.value);
		//alert(searchArray);
		fatchData2(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}
function getOnlyHeadQuarterAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	document.getElementById("headQuarterHiddenId").value="";
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("onlyDistrictName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		//alert('hljkhlkhl================'+txtSearch.value);
		document.getElementById(divid).style.display='block';
		searchArray = getHeadQuarterOnlyArray(txtSearch.value);
		//alert(searchArray);
		fatchData2(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}
function getHeadQuarterOnlyArray(headQuarterName){
	
	var searchArray = new Array();
	//alert("========="+headQuarterName);

		UserAjax.getFieldOfHeadQuarterList(headQuarterName,{ 
			async: false,
			callback: function(data){
			isBranchExistsArray=new Array();
			hiddenDataArray = new Array();
			showDataArray = new Array();
			for(i=0;i<data.length;i++){				
				searchArray[i]=data[i].headQuarterName;
				showDataArray[i]=data[i].headQuarterName;
				hiddenDataArray[i]=data[i].headQuarterId;
				isBranchExistsArray[i]=data[i].isBranchExist;
			}
		},
		errorHandler:handleError
		});	
	
	return searchArray;
}
function hideHeadQuarterDiv(dis,hiddenId,divId)
{
	$('#errordiv').empty();
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
			document.getElementById("headQuarterHiddenId").value=hiddenDataArray[index];
		}
	
		if(dis.value==""){
				
				//$('#onlyHeadQuarterName').attr('readonly', true);
				document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			$('#onlyHeadQuarterName').attr('readonly', false);
			dis.value=showDataArray[index];
			document.getElementById("headQuarterHiddenId").value=hiddenDataArray[index];
			document.getElementById("headQuarterHiddenId").value=hiddenDataArray[index];
			document.getElementById("isBranchExistsHiddenId").value=isBranchExistsArray[index];
			
		}
		
	}else{
		if(document.getElementById(hiddenId))
		{
			document.getElementById(hiddenId).value="";
		}
		if(dis.value!=""){
			var focuschk=0;
			onlyDistrictName=2;
			$('#errordiv').empty();	
			$('#errordiv').show();
			
				$('#errordiv').append("&#149; Please enter valid Head Quarter<br>");
			
			
			if(focus==0)
			$('#onlyHeadQuarterName').focus();			
			focus++;
		}
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
		//alert($('#onlyHeadQuarterName').val());
	}
	index = -1;
	length = 0;
}
/*****************end for use this Auto suggest   ******************/

//amit
function loginUserValidate1()
{
	
	//$("p").addClass("myClass yourClass");
	var emailAddress = document.getElementById("emailAddress1");
	var password = document.getElementById("password1");
	
	
	var cnt=0;
	var focs=0;	
	$('#errordiv1').empty();
	
	document.getElementById("divServerError1").style.display="none";	
	$('#emailAddress1').css("background-color","");
	$('#password1').css("background-color","");
	
	
	if(trim(emailAddress.value) == '')
	{
		$('#errordiv1').append("&#149; Please enter Email<br>");
		if(focs==0)
			$('#emailAddress1').focus();
		
		$('#emailAddress1').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	if(trim(emailAddress.value) != '' && !isEmailAddress(trim(emailAddress.value)))
	{		
		$('#errordiv1').append("&#149; Please enter valid Email<br>");
		if(focs==0)
			$('#emailAddress1').focus();
		$('#emailAddress1').css("background-color", txtBgColor);
		cnt++;focs++;
	}
	if(trim(password.value) == '')
	{
		$('#errordiv1').append("&#149; Please enter Password<br>");
		if(focs==0)
			$('#password1').focus();
		$('#password1').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	 
	
	if(cnt==0)
	  return true;	 
		 	
	else
	{
		$('#errordiv1').show();
		return false;
	}
}

function showInfoAndRedirect(sts,msg,nextmsg,url)
{
	//$("#myModalv").css({ top: '60%' });
	var img="info";
	if(sts==1)
		img = "warn";
	else if(sts==2)
		img = "stop";
	
	$('#warningImg1').html("<img src='images/"+img+".png' align='top'>");
	$('#message2showConfirm1').html(msg);
	$('#nextMsg').html(nextmsg);
	
	var onclick = "";
	if(url!=""){
		//onclick = "onclick='redirectTo()'";
		onclick = "onclick=redirectToPage('"+url+"')";
	}
	
	$('#footerbtn1').html("<button class='btn btn-primary' data-dismiss='modal' aria-hidden='true' "+onclick+">Ok</button>");
	$('#vcloseBtn').html("<span "+onclick+" >x</span>");
	$('#myModalv').modal('show');
	$('#myModalv .modal-body:first').hide();
}

function redirectToPage(url)
{
	window.location.href = url;
}

// test system setup 

function testYourSetUp() 
{
	window.open("systemsetup.do");
}





function hideSPCDiv(){
	redirectToPage("smartpractices.do");	
}


function addBranch(){
	var branchId  =   $.trim($("#branchId1").val());
	var branchName1 = $.trim($("#branchName1").val());
	$('#errordiv2').html("");
	if(branchName1 =="" || branchId==""){
		
		$('#errordiv2').html("&#149; "+resourceJSON.enaterBranchName+"<br>");
		$('#branchName1').css("background-color",txtBgColor);
		$('#branchName1').focus();
		
		
	}
	
	 	
		else if(branchId!=null || branchId!="")
		{
			BranchesAjax.getBranchJob(branchId,{
				async: false,
				callback: function(data)
				{
					if(data==0){
						$('#errordiv2').html("&#149; "+resourceJSON.labelBranch+" "+branchName1+" "+resourceJSON.labelIsnotAvail4SMPC+"<br>");
						 
						$('#branchName1').css("background-color",txtBgColor);
						$('#branchName1').focus();
					}
					else{
						document.getElementById("jobId1").value=data;
						 addBranchandDoSignin($("#teacherId").val() ,$("#jobId1").val() , $("#branchId1").val());
					}
				},
				errorHandler:handleError 
			});
		}
		
}

 function addBranchandDoSignin(teacherId,jobid,branchId){
	 
	 if(teacherId!=null && teacherId!="" && jobid!=null && jobid!=""){
		 
	// alert("teacherId  =  "+teacherId +"   ,  jobid   = "+jobid + ",  branchId	   = "+branchId);
	// addBranchAndSignin.do
	 
		$.ajax(
		  		{
		  			url: "addBranchAndSignin.do",
		  			async: false,
		  			data:
		  			{
		  			teacherId: teacherId,
		  			jobId: jobid,
		  			branchId: branchId,
		  			},
		  			success:function(result)
		  			{
		  				
		  				var myresult = $.trim(result);
		  			//	alert("Result   =  "+myresult)
		  				if(myresult == "ok")
		  				{
		  				//	alert(" loging in ...........")
		  					$("#signinForm").submit();
		  					 
		  				}
		  				else{
		  					return false;
		  				}
		  					 
		  			},
		  			error:function(jqXHR, textStatus, errorThrown)
		  			{
		  				alert("server error status:- "+textStatus+", error Thrown:- "+errorThrown+", error jqXHR:- "+jqXHR);
		  			}
		  		});	
	 
	 
	 }
	
}

