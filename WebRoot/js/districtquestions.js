function addQuestionInstruction()
{
	$('#questionInstructions').find(".jqte_editor").css("background-color", "");
	$('#questionInstructions').find(".jqte_editor").text("");
	$('#QIerrordiv').empty();
	$('#questionInstructionModel').modal('show');
		
}
function deleteInstruction()
{
	$('#deleteInstructionModel').modal('show');
}  
function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else if(exception.javaClassName=='undefined'){}
	else{alert( resourceJSON.msgServerErr+": "+exception.javaClassName);}
}
function searchData()
{	
	$("#tblGrid").show();
	$("#qdetails").show();
	$("#tblGridQInsID").show();
	$("#managedqCLine").show();
	$("#addQuestionInstrunction").show();
	$("#managedqIcon").show();
	$("#managedqText").show();
	
	$('#districtName').css("background-color", "");
	if($('#districtId').val()==0)
	{	$("#tblGrid").hide();
		$("#qdetails").hide();
		$("#tblGridQInsID").hide();
		$("#managedqCLine").hide();
		$("#addQuestionInstrunction").hide();
		$("#managedqIcon").hide();
		
		$('#errordiv').html("&#149; "+resourceJSON.PlzEtrAnyDistrictName+"<br>");
		$('#districtName').focus();
		$('#districtName').css("background-color", "#F5E7E1");
		$('#errordiv').show();
		return;
	}
	

	$('#searchItem').fadeOut(1000);
	$('#sa').fadeIn(1200);
	$('#closePan').html("<a style='padding-right: 6px;' onclick='hideSearchPan();' href='javascript:void(0);' ><i class='icon-remove icon-large' ></i></a>");
	$('#addQuestion').html("<a href='districtspecificquestions.do?districtId="+$('#districtId').val()+"' >+ "+resourceJSON.MszAddQuestion+"</a>");
	getQuestionsInstruction();
	getDistrictQuestions();
	hideSearchAgainMaster();
}
function saveQuestionInstruction()
{
	var qins;
	var flag=0;
	$('#QIerrordiv').empty();
	$('#questionInstructions').find(".jqte_editor").css("background-color", "");
	if($('#questionInstructions').find(".jqte_editor").text().trim().length>0){
		qins=$('#questionInstructions').find(".jqte_editor").html();
		flag=1;
	}else{
		$('#questionInstructions').find(".jqte_editor").css("background-color", "#F5E7E1");
		$('#QIerrordiv').html("&#149; "+resourceJSON.PlzEtrInstructions+"<br>");
		$('#QIerrordiv').show();
		flag=0;
		return;
	}
	if(flag==1){
		var districtId = document.getElementById("districtId").value;
		DistrictAjax.saveQuestionInstruction(qins,districtId,{ 
			async: true,
			errorHandler:handleError,
			callback: function(data){
			getQuestionsInstruction();
			getDistrictQuestions();
			$('#questionInstructionModel').modal('hide');
			$('#questionInstructions').find(".jqte_editor").text("");
		},
		errorHandler:handleError  
		});
	}
	
}

function editQuestionInstruction()
{
	var districtId = document.getElementById("districtId").value;
	DistrictAjax.getQuestionInstruction(districtId,{ 
		async: true,
		errorHandler:handleError,
		callback: function(data){
		$('#questionInstructionModel').modal('show');
		$('#questionInstructions').find(".jqte_editor").height(225);
		$('#questionInstructions').find(".jqte_editor").html(data);
	},
	errorHandler:handleError  
	});
}
function deleteQuestionInstruction()
{
	var districtId = document.getElementById("districtId").value;
		DistrictAjax.deleteQuestionInstruction(districtId,{ 
		async: true,
		errorHandler:handleError,
		callback: function(data){
			$('#deleteInstructionModel').modal('hide')	
			getQuestionsInstruction();
			getDistrictQuestions();
	},
	errorHandler:handleError  
	});
}

function getQuestionsInstruction()
{
	var districtId = document.getElementById("districtId").value;
	DistrictQuestionsAjax.getQuestionInstruction(districtId,{ 
		async: true,
		errorHandler:handleError,
		callback: function(data){
		if(data!=""){
			$('#addQuestionInstrunction').hide();
			$('#tblGridQIns').html(data);
			document.getElementById("addQuestion").setAttribute("style","margin-top:0px");
			document.getElementById("tblGridQInsID").style.display='inline';
		}else{
			document.getElementById("addQuestion").setAttribute("style","margin-top:-10px");
			$('#addQuestionInstrunction').show();
			document.getElementById("tblGridQInsID").style.display='none';
		}
			
	},
	errorHandler:handleError  
	});
}
function getDistrictQuestions()
{
	var districtId = document.getElementById("districtId").value;
	DistrictQuestionsAjax.getDistrictQuestions(districtId,{ 
		async: true,
		errorHandler:handleError,
		callback: function(data){  
		$('#tblGridQIns').show();
		$('#tblGrid').html(data);
	},
	errorHandler:handleError  
	});
}
function getQuestionOptions(questionTypeId)
{
	var qType=findSelected(questionTypeId);
	 if(qType=='sl'){
			for(i=0;i<=4;i++)
				$('#row'+i).hide();
			$('#rowExplain').hide();
		
		}
	 else{
	
	$('#firstColumn').width(200);
	$('#secondColumn').width(200);
	$('#row1WithExplanation1').hide();
	$('#row1WithExplanation2').hide();
	$('#explanationRequired').hide();
	var qType=findSelected(questionTypeId);
	if(questionTypeId==0 || qType=='ml')
	{
		for(i=0;i<=5;i++)
			$('#row'+i).hide();
			$('#rowExplain').hide();
		return;
	}
	else
	{
		for(i=0;i<=5;i++)
			$('#row'+i).show();
		$('#rowExplain').hide();
	}

	if(qType=='tf' || qType=='et')
	{
		$('#row2').hide();
		$('#row3').hide();
		$('#row4').hide();
		$('#row5').hide();
		if(qType=='et'){
			$('#rowExplain').show();
			$('#firstColumn').width(180);
			$('#secondColumn').width(180);
			$('#explanationRequired').show();
			$('#row1WithExplanation1').show();
			$('#row1WithExplanation2').show();
		}
		if(qType=='tf'){
			$('#rowExplain').hide();
		}
		if($('#opt1').val()=="")
			$('#opt1').attr("value","True");
		if($('#opt2').val()=="")
			$('#opt2').attr("value","False");

	} else if(qType=='slsel') {
		$('#row2').show();
		$('#row3').show();
		$('#row4').show();
		$('#row5').show();
		$('#rowExplain').hide();
	}
}

}

function clearOptions()
{
	for(i=1;i<=10;i++)
	{
		$('#opt'+i).attr("value","");
		$('#valid'+i).attr("checked",false);
	}
}

function redirectTo(redirectURL)
{	
	var url = redirectURL+"&dt="+new Date().getTime();
	window.location.href=url;
}

function validateDistrictQuestions()
{
	$('#errordiv').empty();
	$('#errordiv1').empty();
	$('#assQuestion').find(".jqte_editor").css("background-color", "");
	$('#assInstruction').find(".jqte_editor").css("background-color", "");
	$('#assExplanation').find(".jqte_editor").css("background-color", "");
	$('#questionTypeMaster').css("background-color", "");
	$('#opt1').css("background-color", "");

	var cnt=0;
	var focs=0;

	if($('#assQuestion').find(".jqte_editor").text().trim()==""){

		$('#errordiv').append("&#149; "+resourceJSON.PlzEtrQuest+"<br>");
		if(focs==0)
			$('#assQuestion').find(".jqte_editor").focus();
		$('#assQuestion').find(".jqte_editor").css("background-color", "#F5E7E1");
		cnt++;focs++;
	}else
	{
		var charCount=trim($('#assQuestion').find(".jqte_editor").text());
		var count = charCount.length;
		if(count>5000)
		{
			$('#errordiv').append("&#149; "+resourceJSON.MsgQuestionLength+"<br>");
			if(focs==0)
				$('#assQuestion').find(".jqte_editor").focus();
			$('#assQuestion').find(".jqte_editor").css("background-color", "#F5E7E1");
			cnt++;focs++;
		}
	}
	if (trim(document.getElementById("questionTypeMaster").value)==0){

		$('#errordiv').append("&#149; "+resourceJSON.PlzSelectQuestionType+"<br>");
		if(focs==0)
			$('#questionTypeMaster').focus();
		$('#questionTypeMaster').css("background-color", "#F5E7E1");
		cnt++;focs++;
	}
	
	var qType=findSelected(dwr.util.getValue("questionTypeMaster"));
	var c=0;
	var validCnt=0;
	
/* Added By Hanzala for adding Question Explanation */
	
	if(qType == "et"){
		if($('#assExplanation').find(".jqte_editor").text().trim()==""){

			$('#errordiv').append("&#149; "+resourceJSON.PlzEtrQuestionExplanation+"<br>");
			if(focs==0)
				$('#assExplanation').find(".jqte_editor").focus();
			$('#assExplanation').find(".jqte_editor").css("background-color", "#F5E7E1");
			cnt++;focs++;
		} else {
			var charCount=trim($('#assExplanation').find(".jqte_editor").text());
			var count = charCount.length;
			if(count>5000)
			{
				$('#errordiv').append("&#149; "+resourceJSON.MsgQuestionExplanationLength+"<br>");
				if(focs==0)
					$('#assExplanation').find(".jqte_editor").focus();
				$('#assExplanation').find(".jqte_editor").css("background-color", "#F5E7E1");
				cnt++;focs++;
			}
		}
		if(validateExplanationRequired()==false)
		{
			cnt++;
			$('#errordiv').append("&#149; "+resourceJSON.msgSelectExplanationRequired+"<br>");
		}
	}
/* END Section */
	if(!(qType=='ml'))
		if(document.getElementById("opt1"))
		{
			for(i=1;i<=6;i++)
			{
				if(trim(document.getElementById("opt"+i).value)=="")
					c++;
				if($('#valid'+i).prop('checked') && trim(document.getElementById("opt"+i).value)!="")
				validCnt++;
			}
			if(c==5 || c==6)
			{
				$('#errordiv').append("&#149; "+resourceJSON.PlzEtrAtLeastTwoOptions+"<br>");
				if(focs==0)
				{
					if(c==6)
						$('#opt1').focus();
					else
						$('#opt2').focus();
				}

				$('#opt1').css("background-color", "#F5E7E1");
				cnt++;focs++;
			}
		}

	
	var arr =[];

	var charCount=trim($('#assInstruction').find(".jqte_editor").text());
	var count = charCount.length;
	if(count>2500)
	{
		$('#errordiv').append("&#149; "+resourceJSON.MsgInstructionsCannotExceed+"<br>");
		if(focs==0)
			$('#assInstruction').find(".jqte_editor").focus();
		$('#assInstruction').find(".jqte_editor").css("background-color", "#F5E7E1");
		cnt++;focs++;
	}

	if(validCnt==0 && qType!='ml')
	{
		$('#errordiv').append("&#149; "+resourceJSON.msgAtleaseOneValidOption+"<br>");
		cnt++;focs++;
	}
	if(cnt==0)
		return true;
	else
	{
		$('#errordiv').show();
		return false;
	}

}
function saveQuestion(status)
{
	if(!validateDistrictQuestions())
		return;

	var questionType={questionTypeId:dwr.util.getValue("questionTypeMaster")};
	var qType=findSelected(questionType.questionTypeId);
	var districtSpecificQuestions = {questionId:dwr.util.getValue("questionId"),question:null,questionExplanation:null,questionTypeMaster:questionType,questionInstructions:null};
	var districtMaster = {districtId:dwr.util.getValue("districtId")};
	var questionoptions={};
	var arr =[];
	if(qType=='tf')
	{
		for(i=1;i<=2;i++)
		{
			if($('#opt'+i).val().trim()!="")
			{
				arr.push({ 
					"optionId" : $('#hid'+i).val(),
					"questionOption"  : $('#opt'+i).val(),
					"validOption"  : $('#valid'+i).prop('checked')
				});
			}
		}
	} 
	else if(qType=='et'){
		for(i=1;i<=2;i++)
		{
			if($('#opt'+i).val().trim()!="")
			{
				arr.push({ 
					"optionId" : $('#hid'+i).val(),
					"questionOption"  : $('#opt'+i).val(),
					"validOption"  : $('#valid'+i).prop('checked'),
					"requiredExplanation": $('#explanationRequired'+i).prop('checked')
				});
			}
		}
	} else if(qType=='slsel' || qType=='mlsel') {
		for(i=1;i<=10;i++)
		{
			if($('#opt'+i).val().trim()!="")
			{
				arr.push({ 
					"optionId" : $('#hid'+i).val(),
					"questionOption"  : $('#opt'+i).val(),
					"validOption"  : $('#valid'+i).prop('checked')
				});
			}
		}
	}
	var qSetId = document.getElementById("questionSetId").value;
	dwr.util.getValues(districtSpecificQuestions);
	dwr.engine.beginBatch();
	DistrictQuestionsAjax.saveDistrictQuestion(districtSpecificQuestions,districtMaster,arr,{ 
		async: true,
		errorHandler:handleError,
		callback: function(data)
		{
			var redirectURL = "";
			if(status=="save")
				redirectURL = "districtquestions.do?districtId="+districtMaster.districtId;
			else if(status=="continue")
				redirectURL = "districtspecificquestions.do?districtId="+districtMaster.districtId;
			else if(status=="saveQues")
				redirectURL = "assessmentQuestionSet.do?quesSetId="+qSetId+"&&districtId="+districtMaster.districtId;
			
			redirectTo(redirectURL);
		}
	});
	
	dwr.engine.endBatch();
}
function cancelQuestion(districtId)
{
	var qSetId = document.getElementById("questionSetId").value
	if(qSetId==0){
	var redirectURL = "districtquestions.do?districtId="+districtId;
	}
	else{
		redirectURL = "assessmentQuestionSet.do?quesSetId="+qSetId+"&&districtId="+districtId;
	}
		
	redirectTo(redirectURL);
}


function activateDeactivateDistrictQuestion(questionId,status)
{
	DistrictQuestionsAjax.activateDeactivateDistrictQuestion(questionId,status, { 
		async: true,
		errorHandler:handleError,
		callback:  function(data)
		{
			if(data!=null)
			{
				if(data.status=="A")
					$('#Q'+questionId).html("<a href='javascript:void(0)' onclick=\"activateDeactivateDistrictQuestion("+questionId+",'I')\">"+resourceJSON.MsgDeactivate+"</a>");
				else
					$('#Q'+questionId).html("<a href='javascript:void(0)' onclick=\"activateDeactivateDistrictQuestion("+questionId+",'A')\">"+resourceJSON.MsgActivate+"</a>");
			}
		}
	});
}
function setDistrictQuestion(questionId)
{	
	if(questionId>0)
	{
		var districtSpecificQuestions = {questionId:questionId};
		DistrictQuestionsAjax.getDistrictQuestionById(districtSpecificQuestions,{ 
			async: false,
			errorHandler:handleError,
			callback: function(data){
			
			dwr.util.setValues(data);
			$('#assQuestion').find(".jqte_editor").html(data.question)
			$('#assInstruction').find(".jqte_editor").html(data.questionInstructions)
			$('#assExplanation').find(".jqte_editor").html(data.questionExplanation)
			dwr.util.setValue("questionTypeMaster",data.questionTypeMaster.questionTypeId);
			getQuestionOptions(data.questionTypeMaster.questionTypeId);
			var cnnt=1;
			jQuery.each(assessQuesOpts, function (i, jsonSelectedOptions) {	
				$('#hid'+cnnt).attr("value",jsonSelectedOptions.optionId);
				$('#opt'+cnnt).attr("value",jsonSelectedOptions.questionOption);
				$('#valid'+cnnt).attr('checked',jsonSelectedOptions.validOption);
				if(data.questionTypeMaster.questionTypeId==8)
				$('#explanationRequired'+cnnt).attr('checked',jsonSelectedOptions.explanationRequired);
				cnnt++;
			});
			
		}
		});	
	}
}

function hideSearchPan()
{	
	$('#districtName').css("background-color", "");
	$('#searchItem').fadeOut(1000);
	$('#sa').fadeIn(1200);
}
function getSearchPan()
{
	$('#errordiv').hide();
	$('#districtName').css("background-color", "");
	$('#searchItem').fadeIn(1000);
	$('#sa').fadeOut(1200);
}
//////////////////////////////////////////////////////////////////
var count=0;
var index=-1;
var length=0;
var divid='';
var txtid='';
var hiddenDataArray = new Array();
var showDataArray = new Array();
var degreeTypeArray = new Array();
var hiddenId="";
function getDistrictAuto(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("districtName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getDistrictArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getDistrictArray(districtName){
	var searchArray = new Array();
		BatchJobOrdersAjax.getFieldOfDistrictList(districtName,{ 
			async: false,
			callback: function(data){
			hiddenDataArray = new Array();
			showDataArray = new Array();
			for(i=0;i<data.length;i++){
				searchArray[i]=data[i].districtName;
				showDataArray[i]=data[i].districtName;
				hiddenDataArray[i]=data[i].districtId;
			}
		},
		errorHandler:handleError
		});	
	return searchArray;
}


var selectFirst="";

var fatchData= function(txtSearch,searchArray,txtId,txtdivid){
	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
				
			}
			
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}
function hideDistrictMasterDiv(dis,hiddenId,divId)
{
	document.getElementById("districtId").value="";
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			document.getElementById("districtId").value=hiddenDataArray[index];
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}

var downArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index<length-1){
			for(var i=0;i<10;i++){
				{
					if(document.getElementById('divResult'+txtdivid+i))
						var div_id=document.getElementById('divResult'+txtdivid+i);
					if(div_id)
					{
						if(div_id.className=='over')
							index=div_id.id.split('divResult'+txtdivid)[1];
						div_id.className='normal';
					}
				}
			}
			index++;
			if(document.getElementById('divResult'+txtdivid+index))
			{
				var div_id=document.getElementById('divResult'+txtdivid+index);
				div_id.className='over';
				document.getElementById(txtid).value = div_id.innerHTML;
				selectFirst=div_id.innerHTML;
			}
		}
	}
}

var upArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index>0){
			for(var i=0;i<length;i++){

				var div_id=document.getElementById('divResult'+txtdivid+i);
				if(div_id)
				{
					if(div_id.className=='over')
						index=div_id.id.split('divResult'+txtdivid)[1];
					div_id.className='normal';
				}
			}
			index--;
			if(document.getElementById('divResult'+txtdivid+index))
				document.getElementById('divResult'+txtdivid+index).className='over';
			if(txtid && document.getElementById('divResult'+txtdivid+index))
			{
				document.getElementById(txtid).value=
					document.getElementById('divResult'+txtdivid+index).innerHTML;
				selectFirst=document.getElementById('divResult'+txtdivid+index).innerHTML;
			}
		}
	}
}

var overText=function (div_value,txtdivid) {

	for(var i=0;i<length;i++)
	{
		if(document.getElementById('divResult'+i))
		{
			var div_id=document.getElementById('divResult'+i);

			if(div_id.className=='over')
				index=div_id.id.split(txtdivid)[1];
			div_id.className='normal';
		}
	}
	div_value.className = 'over';
	document.getElementById(txtid).value= div_value.innerHTML;
}
function mouseOverChk(txtdivid,txtboxId)
{	
	for(var i=0;i<length;i++)
	{
		$('#divResult'+txtdivid+i).mouseover({param1: i,param2:txtdivid, param3:txtboxId}, fireMouseOverEvent);
	}
}
function chkLenOfTextArea(textValue,maxlength)
{
	var tlen=textValue.value.length;
	if(tlen>maxlength)
	{
		document.getElementById(textValue.id).value=document.getElementById(textValue.id).value.substr(0,maxlength)
	}
}
function fireMouseOverEvent(event)
{
	for(var i=0;i<length;i++)
	{	
		document.getElementById('divResult'+event.data.param2+i).className='normal';
	}
    document.getElementById('divResult'+event.data.param2+event.data.param1).className='over';	       
   	document.getElementById(event.data.param3).value= $('#divResult'+event.data.param2+event.data.param1).text();
    index=event.data.param1;
}
function setEntityType()
{
	var entityString='';
	var districtSection="<div class='col-sm-12 col-md-12' style='padding-left:65px;'>"+
        "<label>District</label>"+
           "<span>"+
          " <input type='text' id='districtName' autocomplete='off' maxlength='100'  name='districtName' class='help-inline form-control'"+
          		" onfocus=\"getDistrictAuto(this, event, 'divTxtShowData', 'districtName','districtId','');\""+
				" onkeyup=\"getDistrictAuto(this,event,'divTxtShowData', 'districtName','districtId','');\""+
				" onblur=\"hideDistrictMasterDiv(this,'districtId','divTxtShowData');\" tabindex='1'/>"+
      	  " </span>"+
      		"<input type='hidden' id='districtId' value='${districtId}'/>"+
      		"<div id='divTxtShowData'  onmouseover=\"mouseOverChk('divTxtShowData','districtName')\" style=' display:none;position:absolute;z-index:5000;' class='result' ></div>	"+
	  " </div>";
	
	var btnRecord='<button class="btn btn-primary left25-sm" style="width:60%" type="button" onclick="searchData();">Search &nbsp;<i class="icon"></i></button>';
	globalSearchSection(entityString,"","",districtSection,btnRecord);
	$("#entityTypeDivMaster").hide();
	$("#districtClassMaster").show();
	/*setTimeout(function(){ 
  		if($("#districtMasterId").val()!=0){ 
  			$("#districtIdFilter").val($("#districtMasterId").val());
  			$("#districtNameFilter").val($("#districtMasterName").val());
  			
  		}
  		}, 50);*/
	if($("#districtMasterId").val()==0){
		showSearchAgainMaster();
	}else{
		$("#districtId").val($("#districtMasterId").val());
		$("#districtName").val($("#districtMasterName").val());
		searchData();
		hideSearchAgainMaster();
	}
}
function validateExplanationRequired()
{	var check=false;
	var count = 0;
	for(i=1;i<=2;i++)
	{
		if($('#explanationRequired'+i).prop('checked') && trim(document.getElementById("opt"+i).value)!="")
			count++;
	}
	if(count>0) check=true;
		return check;	
}