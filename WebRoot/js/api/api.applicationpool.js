/* @Author: Gagan 
* @Discription: view of application pool js.
*/

/*========  For Sorting  ===============*/
var page = 1;
var noOfRows = 10;
var sortOrderStr="";
var sortOrderType="";

var pageHired = 1;
var noOfRowsHired = 10;
var sortOrderStrHired="";
var sortOrderTypeHired="";
function getPaging(pageno)
{
	var gridNo	=	document.getElementById("gridNo").value;
	if(gridNo==1)
	{
		if(pageno!='')
		{
			page=pageno;	
		}
		else
		{
			page=1;
		}
		
		noOfRows 	=	document.getElementById("pageSize").value;
		//alert(noOfRows);
		displayApplicantGrid();
	}
	else
	{
		if(pageno!='')
		{
			pageHired=pageno;	
		}
		else
		{
			pageHired=1;
		}
		
		noOfRows 	=	document.getElementById("pageSize1").value;
		displayHiredApplicantGrid();
	}
}


function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
	//alert("Hi sortOrder "+sortOrder);
	var gridNo	=	document.getElementById("gridNo").value;
	if(gridNo==1)
	{
		if(pageno!=''){
			page=pageno;	
		}else{
			page=1;
		}
		sortOrderStr	=	sortOrder;
		sortOrderType	=	sortOrderTyp;
		if(document.getElementById("pageSize")!=null){
			noOfRows = document.getElementById("pageSize").value;
		}else{
			noOfRows=10;
		}
	
		displayApplicantGrid();
	}
	else
	{
		if(pageno!=''){
			pageHired=pageno;	
		}else{
			pageHired=1;
		}
		sortOrderStrHired	=	sortOrder;
		sortOrderTypeHired	=	sortOrderTyp;
		if(document.getElementById("pageSize1")!=null){
			noOfRows = document.getElementById("pageSize1").value;
		}else{
			noOfRows=10;
		}
		displayHiredApplicantGrid();
	}
}

/*========  For Handling session Time out Error ===============*/
function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert("Oops: Your Session has expired!");  document.location = 'signin.do';}
	else{alert("Server Error: "+exception.javaClassName);}
}

function displayApplicantGrid()
{
	//alert("Hi");
	var jobId 	=	document.getElementById("jobId").value;
	//alert(" sortOrderStr Value  "+sortOrderStr); 
	ApiApplicantPoolAjax.displayApplicantGrid(jobId,noOfRows,page,sortOrderStr,sortOrderType,{ 
		async: true,
		callback: function(data){
		//alert(" data "+data);
		$('#applicantGrid').html(data);
		applyScrollOnTbl();
		tpAppEnable();
		
	},
	errorHandler:handleError  
	});
}
function tpAppEnable()
{
	var noOrRow = document.getElementById("applicantTable").rows.length;
	//var noOrRow = 10;
	for(var j=1;j<=noOrRow;j++)
	{
		
		$('#tpResume'+j).tooltip();
		$('#tpPortfolio'+j).tooltip();
		$('#tpMsg'+j).tooltip();
		$('#tpNotes'+j).tooltip();
		$('#tpPDReport'+j).tooltip();
		
	}
}
function displayHiredApplicantGrid()
{
	var jobId 	=	document.getElementById("jobId").value;
	//alert(" sortOrderStr Value  "+sortOrderStr); 
	ApiApplicantPoolAjax.displayHiredApplicantGrid(jobId,noOfRows,pageHired,sortOrderStrHired,sortOrderTypeHired,{ 
		async: true,
		callback: function(data){
		//alert(" data "+data);
		$('#applicantHiredGrid').html(data);
		applyScrollOnTblHired();
	},
	errorHandler:handleError  
	});
}
function getSortFirstGrid()
{
	$('#gridNo').val("1");
}

function getSortSecondGrid()
{
	$('#gridNo').val("2")
}
