function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert("Oops: Your Session has expired!");  document.location = '../signin.do';}
	else{alert("Server Error: "+exception.javaClassName);}
}
var rdURL = "startOrContinueEPI.do"; 
var totalAttempted=0;
var jOrder;
var vData;
var assessmentTakenCount=0;
var iJobId=null;
var spURL=null;
var jJobId=null;
function checkInventory(jobId,epiJobId)
{
	var jobOrder={jobId:jobId};
	var assessmentType='';
	var grpName='';
	if(document.getElementById("assessmentType"))
		assessmentType = document.getElementById("assessmentType").value;
	if(document.getElementById("grpName"))
		grpName = document.getElementById("grpName").value;
	
	if(jobId=='0'){
		jJobId = jobId;
		APIAssessmentCampaignAjax.checkInventory(jobOrder,epiJobId,{ 
			async: false,
			errorHandler:handleError,
			callback: function(data){
			//The Job Specific Inventory for this job has not been finalized. Please come back later to complete this application.
			var remodel="Base Inventory";
			if(jobId>0)
				remodel="Job Specific Inventory for this job";
	
			if(data.assessmentName==null && data.status=='1')
			{
				$('#message2show').html("The "+remodel+" has not been finalized. Please come back later to complete this application.");
				$('#myModal2').modal('show');
			}else if(data.assessmentName==null && data.status=='2')
			{
				$('#message2show').html("The "+remodel+" has not been finalized. Please come back later to complete this application.");
				$('#myModal2').modal('show');
			}
			else if(data.assessmentName==null && data.status=='3')
			{
				if(jobId>0)
					getTeacherJobDone(jobId);
				else
				{
					$('#message2show').html("You have already completed this Inventory.");
					$('#myModal2').modal('show');
				}
			}else if(data.assessmentName==null && data.status=='4')
			{
				$('#message2show').html("Your Inventory is expired, Please contact TeacherMatch Administrator.");
				$('#myModal2').modal('show');
			}
			else if(data.assessmentName==null && data.status=='5')
			{
				if(jobId>0)
					getTeacherJobDone(jobId);
				else
				{
					$('#message2show').html("Your Inventory is Timed Out, Please contact TeacherMatch Administrator.");
					$('#myModal2').modal('show');
				}
			}
			else if(data.assessmentName==null && data.status=='6')
			{
				$('#message2show').html("This job is not Active, Please contact TeacherMatch Administrator.");
				$('#myModal2').modal('show');
			}
			else if(data.assessmentName==null && data.status=='7')
			{
				$('#message2show').html("This job is Expired, Please contact TeacherMatch Administrator.");
				$('#myModal2').modal('show');
			}
			else
			{
				if(data.status.charAt(2)>=3)
				{
					//$("#myModalvk").css({ top: '60%' });
					$('#warningImg1k').html("<img src='../images/stop.png' align='top'>");
					$('#message2showConfirm1k').html("You did not answer the questions within three attempts. You have been \"disqualified\" and cannot continue the inventory at this time. If you have questions, please contact support at clientservices@teachermatch.net or (888) 312-7231 .");
					$('#nextMsgk').html("");
					$('#footerbtn1k').html("<button class='btn btn-primary' data-dismiss='modal' aria-hidden='true' >Ok</button>");
					$('#vcloseBtnk').html("<span>x</span>");
					$('#myModalvk').modal('show');
					if(document.getElementById("bStatus"))
					{
						$('#bStatus').html("Timed Out");
						$('#iconpophover3').trigger('mouseout');
						$('#bClick').html("");
					}
				}else
				{
					
					var st_re = data.status.charAt(2)==''?0:data.status.charAt(2);
						
					$('#warningImg1').html("<img src='../images/info.png' align='top'>");
					
					if(jobId>0)
					{
						$('#message2showConfirm1').html(data.assessmentDescription);
						$('#myModalLabelId').html("Additional Questions");
					}
					else
					{
						$('#myModalLabelId').html("TeacherMatch");
						var msg = "Educators Professional Inventory (EPI)";
						if(jobId>0)
							msg = "Job Specific Inventory";
						var infomsg = "<p>You are about to resume the "+msg+".";
						if(st_re==0 && jobId<=0)
							infomsg = "<p>You are about to start the "+msg+".";
						
						$('#message2showConfirm1').html("<b>"+resourceJSON.msgReadCarefully+"</b>");
						$('#nextMsg').html(infomsg+data.splashInstruction2);
						
						if(jobId<=0 && infomsg.indexOf('resume')!=-1){
							$('#message2showConfirm1').html("<b>Welcome back!</b>");
							$('#nextMsg').html("You are about to resume the <b>TeacherMatch Educators Professional Inventory (EPI)</b>. There are 100 questions in the inventory. They are divided into three sections: Attitudinal Factors, Cognitive Ability, and Teaching Skills, each a key success indicator for highly effective teachers.<p></p><b>BEFORE YOU BEGIN:</b> Be prepared to follow these important guidelines:<ul><li><b><font color='red'>Each item or question in the EPI has a stipulated time limit of 75 seconds</font></b> for you to submit your answer. There is a timer in the upper right corner of the screen to help you keep track of time.</li><li>A pop-up reminder will appear on your screen when you have <b>30 seconds</b> left to submit your answer to any question. It is important to respond within this time period to avoid being <b><font color='red'>&#8220;Timed Out&#8221;</font></b>.</li><li>You are <b><font color='red'>not able to skip questions</font></b> &#8212; and you <b><font color='red'>must answer all of the questions</font></b> in one session.</li><li>Once you click the <b>&#8220;Next&#8221;</b> button, <b><font color='red'>your answer is recorded as final</font></b> and cannot be changed. This means you cannot hit the <b>Back</b> arrow on your browser to get back to the previous question to change your answer.</li></ul><p></p>Please click <b>&#8220;Ok&#8221;</b> if you are ready to take the <b>EPI</b> in its entirety and abide by the guidelines presented above. Or, click <b>&#8220;Cancel&#8221;</b> to exit.");
						}
						
						/*infomsg+=" Each item in the EPI has a stipulated time limit of 75 seconds. You must respond to each item within its stipulated time limit. You must answer all of the questions in one sitting.</p>";
						
						$('#message2showConfirm1').html("<b>Please Read Carefully</b>");
						var nextMsg = infomsg+"<b>If you continue, please be prepared to follow these guidelines:</b><br><ul> "+
						"<li>Make sure that you have at least 90 minutes of uninterrupted time to complete the EPI</li> "+ 
						"<li>Make sure that you have a stable and reliable internet connection</li> "+ 
						"<li>Do not close your browser or hit the \"back\" button on your browser </li>"+
						"<li>You are not able to skip questions</li> " +
						"<li>If you are interested in a more comprehensive overview of TeacherMatch's \"Assessment Development: Policies and Procedures\" please click <a target='_blank' href='../policiesandprocedures.do'>here</a></li> " +
						"</ul>";
					
						nextMsg+="<p>We need to maintain the integrity of the EPI and ensure that everyone has a fair and equal opportunity to complete the EPI.</p>";
						nextMsg+="<p>If you do not follow these guidelines your status may become \"timed out\" and you will be unable to retake the EPI for 12 months.</p>";
						nextMsg+="<p><b>Accommodations</b> </p>";
						nextMsg+="<p>If you require accommodations for this assessment, we recommend that you contact the district to which you are applying. The district is responsible for gathering any necessary documentation from you and to make the determination of whether to provide accommodations and of what type. At that point, we will be happy to work with you and the district on administering the assessment in whatever way has been deemed appropriate.</p>";
						nextMsg+="<p><b>It is to your advantage to answer each and every item within the allocated time, even if you must guess.</b> </p>";
						$('#nextMsg').html(nextMsg+"Click \"Ok\" if you are ready to take the EPI in its entirety and prepared to abide by the guidelines presented above. Otherwise, please click \"Cancel\" to exit.");*/
					}
						$('#footerbtn1').html("<button class='btn btn-primary' data-dismiss='modal' aria-hidden='true' onclick=\"startInventory()\">Ok</button> <button class='btn' data-dismiss='modal' aria-hidden='true'>Cancel</button>");
						$('#vcloseBtn').html("<span>x</span>");
						$('#myModalv').modal('show');
						//$("#myModalv").css({ top: '45%' });
						
						jOrder=jobOrder;
						vData=data;
						assessmentTakenCount=data.status.charAt(4);
						iJobId=epiJobId;
				}
			}
		}
		});
	}
	else if(assessmentType=='3' || assessmentType=='4')
	{
		//alert('1');//amit
		APIAssessmentCampaignAjax.checkInventory(jobOrder,assessmentType,grpName,true,null,{
			async: false,
			errorHandler:handleError,
			callback: function(data){
				var remodel="";
				if(assessmentType=='3')
					remodel="Smart Practices";
				else if(assessmentType=='4')
					remodel="IPI";
		
				if(data.assessmentName==null && data.status=='1')
				{
					$('#message2show').html("The "+remodel+" has not been finalized. Please come back later to complete this application.");
					$('#myModal2').modal('show');
				}
				else if(data.assessmentName==null && data.status=='2')
				{
					$('#message2show').html("The "+remodel+" has not been finalized. Please come back later to complete this application.");
					$('#myModal2').modal('show');
				}
				else if(data.assessmentName==null && data.status=='3')
				{
					$('#message2show').html("You have already completed this Inventory.");
					$('#myModal2').modal('show');
					
				}else if(data.assessmentName==null && data.status=='4')
				{
					$('#message2show').html("Your Inventory is expired, Please contact TeacherMatch Administrator.");
					$('#myModal2').modal('show');
				}
				else if(data.assessmentName==null && data.status=='5')
				{
					$('#message2show').html("Your Inventory is Timed Out, Please contact TeacherMatch Administrator.");
					$('#myModal2').modal('show');
				}
				else if(data.assessmentName==null && data.status=='6')
				{
					spURL = '../userdashboard.do';
					$('#warningImg1').html("<img src='../images/info.png' align='top'>");
					$('#message2showConfirm1').html('You have already completed and passed the SmartPractices assessment. Please continue to your dashboard to review your certificate.');
					$('#nextMsg').html('');
					var onclick = "onclick='redirectTo()'";
					$('#footerbtn1').html("<button class='btn btn-primary' data-dismiss='modal' aria-hidden='true' "+onclick+">Ok</button>");
					$('#vcloseBtn').html("<span "+onclick+" >x</span>");
					$('#myModalv').modal('show');
				}
				else if(data.assessmentName==null && data.status=='8')
				{
					$('#message2show').html("You are not able to attempt the assessment of this Group, Please contact TeacherMatch Administrator.");
					$('#myModal2').modal('show');
				}
				else
				{
					if(data.status.charAt(2)>=3)
					{
						//$("#myModalvk").css({ top: '60%' });
						$('#warningImg1k').html("<img src='../images/stop.png' align='top'>");
						$('#message2showConfirm1k').html("You did not answer the questions within three attempts. You have been \"disqualified\" and cannot continue the inventory at this time. If you have questions, please contact support at clientservices@teachermatch.net or (888) 312-7231 .");
						$('#nextMsgk').html("");
						$('#footerbtn1k').html("<button class='btn btn-primary' data-dismiss='modal' aria-hidden='true' >Ok</button>");
						$('#vcloseBtnk').html("<span>x</span>");
						$('#myModalvk').modal('show');
						if(document.getElementById("bStatus"))
						{
							$('#bStatus').html("Timed Out");
							$('#iconpophover3').trigger('mouseout');
							$('#bClick').html("");
						}
					}
					else
					{
						var st_re = data.status.charAt(2)==''?0:data.status.charAt(2);
							
						$('#warningImg1').html("<img src='../images/info.png' align='top'>");
						
						$('#myModalLabelId').html("TeacherMatch");
						var msg = "";
						if(jobId==-3)
							msg = "Smart Practices";
						else if(jobId==-4)
							msg = "IPI";
						
						var infomsg = "<p>You are about to resume the "+msg+".";
						if(st_re==0 && jobId<0)
							infomsg = "<p>You are about to start the "+msg+".";
						
						$('#message2showConfirm1').html("<b>Please Read Carefully</b>");
						$('#nextMsg').html(infomsg+data.splashInstruction2);
						
						/*infomsg+=" Each item in the "+msg+" has a stipulated time limit of 75 seconds. You must respond to each item within its stipulated time limit. You must answer all of the questions in one sitting.</p>";
							
						$('#message2showConfirm1').html("<b>Please Read Carefully</b>");
						var nextMsg = infomsg+"<b>If you continue, please be prepared to follow these guidelines:</b><br><ul> "+
						"<li>Make sure that you have at least 90 minutes of uninterrupted time to complete the "+msg+"</li> "+ 
						"<li>Make sure that you have a stable and reliable internet connection</li> "+ 
						"<li>Do not close your browser or hit the \"back\" button on your browser </li>"+
						"<li>You are not able to skip questions</li> " +
						"<li>If you are interested in a more comprehensive overview of TeacherMatch's \"Assessment Development: Policies and Procedures\" please click <a target='_blank' href='../policiesandprocedures.do'>here</a></li> " +
						"</ul>";
						
						nextMsg+="<p>We need to maintain the integrity of the "+msg+" and ensure that everyone has a fair and equal opportunity to complete the "+msg+".</p>";
						nextMsg+="<p>If you do not follow these guidelines your status may become \"timed out\" and you will be unable to retake the "+msg+".</p>";
						nextMsg+="<p><b>Accommodations</b> </p>";
						nextMsg+="<p>If you require accommodations for this assessment, we recommend that you contact the district to which you are applying. The district is responsible for gathering any necessary documentation from you and to make the determination of whether to provide accommodations and of what type. At that point, we will be happy to work with you and the district on administering the assessment in whatever way has been deemed appropriate.</p>";
						nextMsg+="<p><b>It is to your advantage to answer each and every item within the allocated time, even if you must guess.</b> </p>";
						$('#nextMsg').html(nextMsg+"Click \"Ok\" if you are ready to take the "+msg+" in its entirety and prepared to abide by the guidelines presented above. Otherwise, please click \"Cancel\" to exit.");*/
						
						if(jobId==-3){
							nextMsg="";
							infomsg="resume";
							if(st_re==0)
								infomsg="start";
							nextMsg+="<p>Welcome valuable Kelly Educational Staffing&reg; (KES&reg;) talent. You are about to "+infomsg+" the Smart Practices Assessment. ";
							$('#nextMsg').html(nextMsg+data.splashInstruction2);
							/*nextMsg+="<p>Welcome valuable Kelly Educational Staffing&reg; (KES&reg;) talent. You are about to "+infomsg+" the Smart Practices Assessment. Each question in the Smart Practices Assessment has a time limit of 75 seconds. You must respond to each question within its stipulated time limit. You must answer all of the questions in one sitting.</p>";
							nextMsg+="<p><b>Before beginning, please note these IMPORTANT guidelines:</b></p>";
							nextMsg+="<ul><li>While the average person takes less time to complete, make sure that you have at least 90 minutes of uninterrupted time to complete the Smart Practices Assessment to avoid any \"timed out\" issues and being unable to complete the assessment.</li>";
							nextMsg+="<li>Make sure that you have a stable and reliable internet connection.</li><li>Do not close your browser or hit the \"back\" button on your browser.</li><li>You are not able to skip questions.</li><li><b>It is to your advantage to answer each and every item within the allocated time, even if you must guess.</b></li></ul>";
							nextMsg+="<p>It is important that the integrity of Smart Practices Assessment is maintained and everyone has a fair and equal opportunity to complete Smart Practices.</p>";
							nextMsg+="<p><b>Accommodations</b><br/>If you require accommodations for this assessment, we recommend that you contact your local KES branch.</p>";
							nextMsg+="<p>Please click \"Ok\" if you are ready to take the Smart Practices Assessment otherwise please click \"Cancel\" to exit.</p>";
							nextMsg+="<p>Best,</br>Kelly Educational Staffing</p>";
							$('#nextMsg').html(nextMsg);*/
						}
						
						$('#footerbtn1').html("<button class='btn btn-primary' data-dismiss='modal' aria-hidden='true' onclick=\"startInventory()\">Ok</button> <button class='btn' data-dismiss='modal' aria-hidden='true'>Cancel</button>");
						$('#vcloseBtn').html("<span>x</span>");
						$('#myModalv').modal('show');
						//$("#myModalv").css({ top: '45%' });
							
						jOrder=jobOrder;
						vData=data;
						assessmentTakenCount=data.status.charAt(4);
					}
				}
			}
		});
	}
}

function startInventory()
{
	var data = vData;
	var jobOrder = jOrder;
	var loadingShow = true;
	var epiJobId = iJobId;
	if(data.status.indexOf("#")==-1)
	{
		$('#loadingDivInventory').fadeIn();
		loadAssessmentQuestions(data,jobOrder,epiJobId);
		
	}else
	{
		var newJobId = jobOrder.jobId;
		var dd = data.status;
		var jId = dd.split("#")[3];
		
		var msg = "EPI";
		if(jId!=0)
		{
			jobOrder.jobId=jId;
			jOrder.jobId=jId;
			msg = "Job Specific Inventory";
		}
		if(jId==-3)
			msg = "Smart Practices";
		else if(jId==-4)
			msg = "IPI";
		
		if(data.status.charAt(2)==0 || data.status.charAt(2)==1)
		{
			data.status=data.status.charAt(0);
			//$("#myModalvk").css({ top: '60%' });
			$('#warningImg1k').html("<img src='../images/warn.png' align='top'>");
			$('#message2showConfirm1k').html("<b>Please Read Carefully</b>");
			if(jId==-3)
				$('#nextMsgk').html("Dear valuable Kelly Educational Staffing&reg;(KES&reg;) talent. Unforunately, you exceeded the time limit for this item. This is your <font color='red'>SECOND</font> reminder regarding the importance of abiding by the time limits and supporting the reliability, integrity and security of the Smart Pratices professional development pre-hire series training assessment. Please respond to each item within its stipulated time limit even if you must guess.");
			else
				$('#nextMsgk').html("You exceeded the time limit for this item. This is your <font color='red'>SECOND</font> reminder regarding the importance of abiding by the time limits and supporting the reliability, integrity and security of the "+msg+". Please respond to each item within its stipulated time limit even if you must guess.");
			$('#footerbtn1k').html("<button class='btn btn-primary' data-dismiss='modal' aria-hidden='true' onclick=\"runInventory(1,"+newJobId+");\">Ok</button>");
			$('#vcloseBtnk').html("<span onclick=\"runInventory(1,"+newJobId+");\">x</span>");
			$('#myModalvk').modal('show');

		}else if(data.status.charAt(2)==2)
		{
			data.status=data.status.charAt(0);
			//$("#myModalvk").css({ top: '60%' });
			$('#warningImg1k').html("<img src='../images/warn.png' align='top'>");
			$('#message2showConfirm1k').html("<b>Please Read Carefully</b>");
			var nextMsg="";
			if(jId==-3){
				nextMsg += "<p>Unfortunately, you have exceeded an item's time limit for second time. This is your third and final reminder. You have been timed out from the Smart Pratices professional development pre-hire series training assessment. As a result, you will not be required to respond to any more Smart Pratices items.</p>";
				nextMsg += "<p>If you would like to review TeacherMatch's policy regarding this issue in greater depth please click <a href='javascript:void(0);'>here</a>.</p>";
				nextMsg += "<p>If you have any further questions regarding this matter, please contact Kelly Educational Staffing via phone at 855-535-5915 or via email at <a href='mailto:KESTMSP@kellyservices.com'>KESTMSP@kellyservices.com</a>.</p>";
				nextMsg += "<p>Thank you and best of luck in your job search.</p>";
			}
			else{
				nextMsg += "<p>You have exceeded an item's time limit for second time. This is your third and final reminder. You have been timed out from the "+msg+". As a result, you will not be required to respond to any more "+msg+" items.</p>";
				if(jId==0)
					nextMsg += "<p>Your TeacherMatch status will remain \"Timed Out\" for 12 months.</p><p>You will still be able to apply to any and all positions you are interested in and schools are free to continue considering you for employment (EPI scores are merely one of the many data points schools use to guide their hiring process.)</p>";
				nextMsg += "<p>If you would like to review TeacherMatch's policy regarding this issue in greater depth please click <a href='javascript:void(0);'>here</a>.</p><p> If you have any further questions regarding this matter, please call us at (888) 312-7231 or email us at <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a>.</p> <p>Thank you and best of luck in your job search.</p>";
			}

			$('#nextMsgk').html(nextMsg);
			$('#footerbtn1k').html("<button class='btn btn-primary' data-dismiss='modal' aria-hidden='true' onclick=\"runInventory(1,"+newJobId+");\">Ok</button>");
			$('#vcloseBtnk').html("<span onclick=\"runInventory(1,"+newJobId+");\">x</span>");
			$('#myModalvk').modal('show');
			
		}
		/*else if(data.status.charAt(2)>=3)
		{
			//alert("You did not answer the question within three attempts. You have been \"disqualified\" and cannot continue the inventory at this time. Please contact technical support at (888) 312-7231 .");
			//$("#myModalvk").css({ top: '60%' });
			$('#warningImg1k').html("<img src='../images/stop.png' align='top'>");
			$('#message2showConfirm1k').html("You did not answer the question within three attempts. You have been \"disqualified\" and cannot continue the inventory at this time. If you have questions, please contact support at clientservices@teachermatch.net or (888) 312-7231 .");
			$('#nextMsgk').html("");
			$('#footerbtn1k').html("<button class='btn btn-primary' data-dismiss='modal' aria-hidden='true' >Ok</button>");
			$('#vcloseBtnk').html("<span>x</span>");
			$('#myModalvk').modal('show');
			if(document.getElementById("bStatus"))
			{
				$('#bStatus').html("Violated");
				$('#bClick').html("");
			}
		}*/
	}
}
function runInventory(flg,newJobId)
{
	var data = vData;
	var jobOrder = jOrder;
	var epiJobId = iJobId;
	runAssessment(data,jobOrder,1,newJobId,epiJobId);
}
function loadAssessmentQuestions(assessmentDetail,jobOrder,epiJobId)
{
	APIAssessmentCampaignAjax.loadAssessmentQuestions(assessmentDetail,jobOrder,epiJobId,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){
		if(data==1)
			runAssessment(assessmentDetail,jobOrder,0,0,epiJobId);
		else if(data=='vlt')
		{
			$('#loadingDivInventory').hide();
			//$("#myModalvk").css({ top: '60%' });
			$('#warningImg1k').html("<img src='../images/warn.png' align='top'>");
			$('#message2showConfirm1k').html("You have Timed Out this inventory.");
			$('#nextMsgk').html("");
			$('#footerbtn1k').html("<button class='btn btn-primary' data-dismiss='modal' aria-hidden='true'>Ok</button>");
			$('#vcloseBtnk').html("<span>x</span>");
			$('#myModalvk').modal('show');
		}else if(data=='comp')
		{
			try{$('#loadingDivInventory').hide();
			//$("#myModalvk").css({ top: '60%' });
			$('#warningImg1k').html("<img src='../images/info.png' align='top'>");
			$('#message2showConfirm1k').html("You have already completed this inventory.");
			$('#nextMsgk').html("");
			$('#footerbtn1k').html("<button class='btn btn-primary' data-dismiss='modal' aria-hidden='true'>Ok</button>");
			$('#vcloseBtnk').html("<span>x</span>");
			$('#myModalvk').modal('show');
			}catch(err)
			{
			}
		}else if(data=='icomp')
		{
			$('#loadingDivInventory').hide();
		}
	}
	});	
	
}

function runAssessment(assessmentDetail,jobOrder,doInsert,newJobId,epiJobId)
{
	$('#loadingDivInventory').fadeIn();
	if(doInsert==1)
	{
		insertTeacherAssessmentAttempt(assessmentDetail,jobOrder);
		// last Attempt insertion
		insertLastAttempt(assessmentDetail,assessmentTakenCount,jobOrder);
	}
	
	if(epiJobId==null)
		epiJobId=0;

	var redirectURL = "apirunAssessment.do?assessmentId="+assessmentDetail.assessmentId+"&jobId="+jobOrder.jobId+"&newJobId="+newJobId+"&epiJobId="+epiJobId;
	var url = redirectURL+"&dt="+new Date().getTime();
	window.location.href=url;
}
function insertTeacherAssessmentAttempt(assessmentDetail,jobOrder)
{
	APIAssessmentCampaignAjax.insertTeacherAssessmentAttempt(assessmentDetail,jobOrder,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){
	}
	});	
}
function insertLastAttempt(assessmentDetail,assessmentTakenCount,jobOrder)
{
	APIAssessmentCampaignAjax.insertLastAttempt(assessmentDetail,assessmentTakenCount,jobOrder,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){
	}
	});	
}
function getAssessmentSectionCampaignQuestionsGrid(whoClicked)
{
try
{
	var attemptId = dwr.util.getValue("attemptId");
	totalAttempted = dwr.util.getValue("totalAttempted");
	var assessmentDetail = {assessmentId:dwr.util.getValue("assessmentId")};
	var teacherAssessmentdetail = {teacherAssessmentId:dwr.util.getValue("teacherAssessmentId"),assessmentType:dwr.util.getValue("teacherAssessmentType")};
	var teacherAssessmentQuestion // = {teacherAssessmentQuestionId:dwr.util.getValue("teacherAssessmentQuestionId")};
	var teacherSectionDetail //= {teacherSectionId:dwr.util.getValue("teacherSectionId")};
	var arr =[];
	if(document.getElementById("teacherAssessmentQuestionId"))
	{
		teacherAssessmentQuestion = {teacherAssessmentQuestionId:dwr.util.getValue("teacherAssessmentQuestionId")};
		if(document.getElementById("teacherAssessmentQuestionId").value!="")
		{
			var opts=document.getElementsByName("opt");
			var questionTypeShortName = dwr.util.getValue("questionTypeShortName");
			var questionTypeMaster = {questionTypeId:dwr.util.getValue("questionTypeId")};
			var qType = dwr.util.getValue("questionTypeShortName");
			var questionWeightage = dwr.util.getValue("questionWeightage");
			var questionSessionTime = dwr.util.getValue("questionSessionTime");

			var o_maxMarks = dwr.util.getValue("o_maxMarks");
			if(qType=='tf' || qType=='slsel' || qType=='lkts' || qType=='it')
			{
				var optId="";
				var score=0;
				var questionOptionId = null;
				var questionOptionTag = null;
				if($('input[name=opt]:radio:checked').length > 0 )
				{
					optId=$('input[name=opt]:radio:checked').val();
					var opts = document.getElementsByName("opt");
					var scores = document.getElementsByName("score");
					var questionOptionIds = document.getElementsByName("questionOptionId");
					var questionOptionTags = document.getElementsByName("questionOptionTag");
					for (var i = 0; i < opts.length; i++) {
						if(optId==opts[i].value)
						{
							score = scores[i].value;
							questionOptionId = questionOptionIds[i].value;
							questionOptionTag = questionOptionTags[i].value;
							break;
						}
					}
				}
				var totalScore = questionWeightage*score;
				if(optId=='')
				{
					totalSkippedQuestions++;
					//strike checking
					checkStrikes(questionSessionTime,whoClicked);

					return;
				}

				arr.push({ 
					"selectedOptions"  : optId,
					"optionScore"      : score,
					"totalScore"       : totalScore,
					"questionWeightage" : questionWeightage,
					"questionTypeMaster" : questionTypeMaster,
					"jobOrder" : jobOrder,
					"maxMarks" : o_maxMarks,
					"questionOptionId" : (questionOptionId!='null'?questionOptionId:""),
					"questionOptionTag" : (questionOptionTag!='null'?questionOptionTag:"")
				});

			}else if(qType=='rt')
			{
				var optId="";
				var score="";
				var rank="";
				var scoreRank=0;
				var questionOptionId = "";
				var questionOptionTag = "";
				var opts = document.getElementsByName("opt");
				var scores = document.getElementsByName("score");
				var ranks = document.getElementsByName("rank");
				var o_ranks = document.getElementsByName("o_rank");
				var questionOptionIds = document.getElementsByName("questionOptionId");
				var questionOptionTags = document.getElementsByName("questionOptionTag");
				var tt=0;
				var uniqueflag=false;
				for(var i = 0; i < opts.length; i++) {
					optId += opts[i].value+"|";
					score += scores[i].value+"|";
					rank += ranks[i].value+"|";
					questionOptionId = questionOptionIds[i].value+"|";
					questionOptionTag = questionOptionTags[i].value+"|";

					if(checkUniqueRank(ranks[i]))
					{
						uniqueflag=true;
						break;
					}

					if(ranks[i].value==o_ranks[i].value)
					{
						scoreRank+=parseInt(scores[i].value);
						//alert(scoreRank);
					}
					if(ranks[i].value=="")
					{
						tt++;
					}
				}
				/////////////////////////////////////
				if(uniqueflag)
					return;

				if(tt!=0)
					optId=""; 

				if(optId=="")
				{
					totalSkippedQuestions++;
					//strike checking
					checkStrikes(questionSessionTime,whoClicked);
					return;
				}

				var totalScore = questionWeightage*scoreRank;
				arr.push({ 
					"selectedOptions"  : optId,
					"optionScore"      : score,
					"totalScore"       : totalScore,
					"insertedRanks"    : rank,
					"questionWeightage" : questionWeightage,
					"questionTypeMaster" : questionTypeMaster,
					"jobOrder" : jobOrder,
					"maxMarks" : o_maxMarks,
					"questionOptionId" : (questionOptionId!=""?questionOptionId:""),
					"questionOptionTag" : (questionOptionTag!=""?questionOptionTag:"")
				});

			}else if(qType=='sl' || qType=='ml')
			{
				if(dwr.util.getValue("opt")=='')
				{
					totalSkippedQuestions++;
					//strike checking
					checkStrikes(questionSessionTime,whoClicked);

					return;
				}

				arr.push({ 
					"insertedText"    : dwr.util.getValue("opt"),
					"questionTypeMaster" : questionTypeMaster,
					"totalScore"       : 0,
					"questionWeightage" : 0,
					"jobOrder" : jobOrder,
					"maxMarks" :o_maxMarks
				});
			}else
			{

				totalSkippedQuestions++;
				//strike checking
				checkStrikes(questionSessionTime,whoClicked);
				return;

				arr.push({ 
					"questionWeightage" : questionWeightage,
					"questionTypeMaster" : questionTypeMaster,
					"totalScore" : 0,
					"jobOrder" : jobOrder,
					"maxMarks" :o_maxMarks
				});
			}

		}

	}

	if(document.getElementById("teacherSectionId"))
		teacherSectionDetail = {teacherSectionId:dwr.util.getValue("teacherSectionId")};

	var newJobId = document.getElementById("newJobId").value;
	var epiJobId = document.getElementById("epiJobId").value;
	$('#sectionName').show();
	$("#sbmtbtn").attr("disabled", "disabled");
	APIAssessmentCampaignAjax.getAssessmentSectionCampaignQuestions(assessmentDetail,teacherAssessmentdetail,teacherAssessmentQuestion,teacherSectionDetail,arr,attemptId,newJobId,epiJobId,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){

		window.clearInterval(intervalID);

		arr =[];

		$("#sbmtbtn").attr("disabled", false);
		
		try{$('#tblGrid').html(data);
		}catch(e)
		{}
	}
	});	
}
catch(e)
{alert("e====="+e)}
}

function checkStrikes(questionSessionTime,whoClicked)
{	
	if(questionSessionTime==0)
	{
		//$("#myModalv").css({ top: '60%' });
		$('#warningImg1').html("<img src='../images/info.png' align='top'>");
		$('#message2showConfirm1').html("All the questions are mandatory, Please answer every question in the inventory.");
		$('#nextMsg').html("");
		$('#footerbtn1').html("<button class='btn btn-primary' data-dismiss='modal' aria-hidden='true'>Ok</button>");
		$('#vcloseBtn').html("<span>x</span>");
		$('#myModalv').modal('show');
	}else
	{
		if(whoClicked==1)
		{
			
			//$("#myModalv").css({ top: '60%' });@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		
			$('#warningImg1').html("<img src='../images/info.png' align='top'>");
		
			$('#message2showConfirm1').html("Please answer every question in the inventory before the timer reaches 0.");		
			$('#nextMsg').html("");			
			$('#footerbtn1').html("<button class='btn btn-primary' data-dismiss='modal' aria-hidden='true'>Ok</button>");	
			$('#vcloseBtn').html("<span>x</span>");					
			$('#myModalv').modal('show');
						
				
					
			var remSec = parseInt($('.min').html()==null?0:$('.min').html())*60 +parseInt($('.sec').html());
			if(remSec>0)
			{
				return;
			}
			

		}else
		{
			strikeCheck(document.getElementById("teacherAssessmentQuestionId").value,whoClicked);
		}
	}
}
function checkUniqueRank(dis)
{
	if(isNaN(dis.value))
	{
		dis.value="";
		dis.focus();
		return;
	}
	var arr =[];
	var ranks = document.getElementsByName("rank");
	for(i=0;i<ranks.length;i++)
	{
		if(ranks[i].value!="" && ranks[i]!=dis)
		{
			arr.push(ranks[i].value);
		}
	}
	var o_rank = document.getElementsByName("o_rank");

	var arr2 =[];
	for(i=0;i<o_rank.length;i++)
		arr2.push(o_rank[i].value);


	if(dis.value!="")
		if($.inArray(dis.value, arr2)==-1)
		{
			dis.value="";
			$('#warningImg1').html("<img src='../images/info.png' align='top'>");
			$('#message2showConfirm1').html("Please enter a valid rank");
			$('#nextMsg').html("");
			$('#footerbtn1').html("<button class='btn btn-primary' data-dismiss='modal' aria-hidden='true'>Ok</button>");
			$('#myModalv').modal('show');
			//$("#myModalv").css({ top: '45%' });
			$('#vcloseBtn').html("<span>x</span>");
			
			dis.focus();
			return false;
		}


	$.each(arr, function(j, el){
		if(parseInt(el)==parseInt(dis.value))
		{
			dis.value="";
			$('#warningImg1').html("<img src='../images/info.png' align='top'>");
			$('#message2showConfirm1').html("Please enter unique rank");
			$('#nextMsg').html("");
			$('#footerbtn1').html("<button class='btn btn-primary' data-dismiss='modal' aria-hidden='true'>Ok</button>");
			$('#myModalv').modal('show');
			//$("#myModalv").css({ top: '45%' });
			$('#vcloseBtn').html("<span>x</span>");
			dis.focus();

			return false;
		}
	});
}
var encryptText='';
try{
	$(document).ready(function(){
		encryptText = $('#encryptText').val();
	})
}catch(e){}

function redirectToDashboard(teacherAssessmentType,attempt)
{
	window.onbeforeunload = function() {};	
	var attemptId = dwr.util.getValue("attemptId");
	
	APIAssessmentCampaignAjax.setAssessmentForced(attemptId,{ 
		async: false,
		callback: function(data){
		if(epiStandalone)
			window.location.href="epiboard.do?w="+teacherAssessmentType+"&attempt="+attempt;
		else if(teacherAssessmentType=='3')
			window.location.href="inventory.do?w="+teacherAssessmentType;
		else if(teacherAssessmentType=='4')
			window.location.href="inventory.do?w="+teacherAssessmentType+"&id="+encryptText;
		else
			window.location.href=rdURL+"?w="+teacherAssessmentType+"&attempt="+attempt;
	},
	errorHandler:handleError  
	});	
}
function stopTimer()
{
	clearTimeout(timeout);
}
function strikeCheck(teacherAssessmentQuestionId,whoClicked)
{
	var teacherAssessmentType = dwr.util.getValue("teacherAssessmentType");
	var moreMsg = ".";
	var inventory = "";
	if(teacherAssessmentType==1){
		moreMsg = ".";
		inventory = "EPI";
	}
	else if(teacherAssessmentType==2){
		inventory = "JSI"
	}
		
	$('#tm-root').html("");
	var questionSessionTime = 0;
	if(document.getElementById("questionSessionTime"))
		questionSessionTime = dwr.util.getValue("questionSessionTime")==null?0:dwr.util.getValue("questionSessionTime");

	var totalChances = parseInt(totalAttempted) + totalStrikes;
	if(totalChances==1  && (whoClicked!=2))
	{
		var onclick = "";
		onclick = "onclick=\"stopTimer(),tmTimer("+questionSessionTime+",'1')\"";
		
		//$("#myModalv").css({ top: '60%' });
		$('#warningImg1').html("<img src='../images/firstVlt.png' align='top'>");
		if(teacherAssessmentType==1){
			$('#message2showConfirm1').html(resourceJSON.msgTimedOutthestipulatedtime);
			$('#nextMsg').html( resourceJSON.msgreadytoStrt1+" <font color='red'>"+resourceJSON.msgSECOND+"</font> "+resourceJSON.msgreadytoStrt2+" "+inventory+resourceJSON.msgreadytoStrt3 + moreMsg);
		}
		else{
			$('#message2showConfirm1').html("You just Timed Out the stipulated time limit for an item and this is your FIRST warning. You must respond to each item within its stipulated time limit as failure to respond may result in timed out status. ");
			$('#nextMsg').html("Click \"Ok\"  if you are ready to start your <font color='red'>SECOND</font> attempt to complete the Inventory. If you don't take any action within 30 seconds, you will be taken back to the Dashboard"+moreMsg);
		}
		$('#footerbtn1').html("<button class='btn btn-primary' data-dismiss='modal' "+onclick+" aria-hidden='true'>Ok</button>");
		$('#vcloseBtn').html("<span "+onclick+" >x</span>");
		$('#myModalv').modal('show');
		if(teacherAssessmentType==1 || teacherAssessmentType==2){
			sendMailToCandidate(teacherAssessmentType,'vlt1');
		}
		if(questionSessionTime>0 && whoClicked==0)
		{
			window.clearInterval(intervalID);
			saveStrike(teacherAssessmentQuestionId);
			timeout =  setTimeout('redirectToDashboard('+teacherAssessmentType+',1)', 30000);
		}
		return;
	}
	if(totalChances==2  && (whoClicked!=2))
	{
		var onclick = "";
		onclick = "onclick=\"stopTimer(),tmTimer("+questionSessionTime+",'1')\"";
		
		//$("#myModalv").css({ top: '60%' });
		$('#warningImg1').html("<img src='../images/secondVlt.png' align='top'>");
		if(teacherAssessmentType==1){
			$('#message2showConfirm1').html(resourceJSON.msgTimedOutthestipulatedtime2);
			$('#nextMsg').html( resourceJSON.msgreadytoStrt1+" <font color='red'>"+resourceJSON.msgTHIRD+"</font> "+resourceJSON.msgreadytoStrt4+resourceJSON.msgreadytoStrt3 +moreMsg);
		}
		else{
			$('#message2showConfirm1').html("You just Timed Out the stipulated time limit again for an item and this is your SECOND warning. You must respond to each item within its stipulated time limit as failure to respond may result in timed out status. ");
			$('#nextMsg').html("Click \"Ok\"  if you are ready to start your <font color='red'>FINAL</font> attempt to complete the Inventory. If you don't take any action within 30 seconds, you will be taken back to the Dashboard"+moreMsg);
		}
		$('#footerbtn1').html("<button class='btn btn-primary' data-dismiss='modal' "+onclick+" aria-hidden='true'>Ok</button>");
		$('#vcloseBtn').html("<span "+onclick+" >x</span>");
		$('#myModalv').modal('show');
		if(teacherAssessmentType==1 || teacherAssessmentType==2){
			sendMailToCandidate(teacherAssessmentType,'vlt2');
		}
		if(questionSessionTime>0 && whoClicked==0)
		{
			window.clearInterval(intervalID);
			saveStrike(teacherAssessmentQuestionId);
			timeout = setTimeout('redirectToDashboard('+teacherAssessmentType+',2)', 30000);

		}
		return;
	}else if((totalAttempted>=1 && totalStrikes>=1) || (totalAttempted>=2 && totalStrikes==0)) //if(totalChances>=3)
	{
	
		if(whoClicked==0)
		{
			window.clearInterval(intervalID);
		}

		finishAssessment(0);
		return;
	}

}
function redirectToURL(redirectURL)
{
	var url = redirectURL;
	window.location.href=url;
}
function redirectTo()
{
	if(document.getElementById('epiStandAlone') && $('#epiStandAlone').val()==false || $('#epiStandAlone').val()=="false")
	{
		//$(location).attr('href','epiboard.do');
		//window.location.href='epiboard.do';
		
		if(document.getElementById('rURL'))
		{
			var url = $('#rURL').val();
			window.location.href=url;
		}else
		{
			window.location.href='portalboard.do';
		}
		//window.open(url,"_tab","TeacherMatch","toolbar=yes,location=yes,scrollbars=yes,menubar=yes");
	}else
	{
		//window.location.href='userdashboard.do';
		if(document.getElementById('rURL'))
		{
			var url = $('#rURL').val();
			window.location.href=url;
		}
		else if(spURL!=null)
		{
			window.location.href=spURL;
		}
		else
		{
			window.location.href=rdURL;
		}
		//window.open(url,"_tab","TeacherMatch","toolbar=yes,location=yes,scrollbars=yes,menubar=yes");
	}
}
var isPortal = false;
function showInfoAndRedirect(sts,msg,nextmsg,url,epiFlag)
{
	//$("#myModalv").css({ top: '60%' });
	var img="info";
	if(sts==1)
		img = "warn";
	else if(sts==2)
		img = "stop";
	
		/*$('#warningImg1').html("<img src='../images/"+img+".png' align='top'>");
	$('#message2showConfirm1').html(msg);
	$('#nextMsg').html(nextmsg);*/
	
	var onclick = "";
	if(url!=""){
		if(epiFlag==1){
			if(msg=="thirdVlt"){
				msg = "<b>Timed Out &#8212; FINAL ALERT!</b><p></p>Unfortunately you have not responded to questions within the stipulated <b>75 seconds</b> for the <b><font color='red'>THIRD</font></b> time. As explained previously, after the third alert, you are now <b><font color='red'>&#8220;Timed Out&#8221;</font></b> from the <b>EPI</b> &#8212; and are not allowed to continue with the EPI.<p></p>Your TeacherMatch EPI status will remain <b><font color='red'>&#8220;Timed Out&#8221;</font></b> for 12 months.<p></p>Please note that you will still be able to apply to any and all positions you are interested in, and schools are free to continue considering you for employment. <b>EPI</b> results are merely one of the many data points schools use to guide their hiring process.<p></p>If you would like to review the TeacherMatch policy regarding this issue in greater depth, please click <a href='termsofuse.do'>here</a>. And if you have any further questions regarding this matter, please call us at 855.980.0511 or email us at <a href=\"mailto:applicants@teachermatch.org\">applicants@teachermatch.org</a>.<p></p>Thank you for your time &#8212; and best of luck in your job search!";
				img = "thirdVlt";
			}
			$('#warningImg1').html("<img src='../images/"+img+".png' align='top'>");
			$('#message2showConfirm1').html(msg);
			$('#nextMsg').html(nextmsg);
			onclick = "onclick='redirectTo()'";
			$('#footerbtn1').html("<button class='btn btn-primary' data-dismiss='modal' aria-hidden='true' "+onclick+">Ok</button>");
			$('#vcloseBtn').html("<span "+onclick+" >x</span>");
			$('#myModalv').modal('show');
		}
		else if(epiFlag==3){
			$('#rURL').val(url);
			if(msg=="Unfortunately")
				msg="Unfortunately, you have not passed the Smart Practices assessment. You may continue to review the Smart Practices PD videos but you are not able to re-take the assessment. For more information on how to proceed, Click on this link:<br/><br/><a target='_blank' href='http://www.kellyeducationalstaffing.com/US-KES/Training-Exam/'>http://www.kellyeducationalstaffing.com/US-KES/Training-Exam/</a>";
			$('#warningImg1').html("<img src='../images/"+img+".png' align='top'>");
			$('#message2showConfirm1').html(msg);
			$('#nextMsg').html(nextmsg);
			onclick = "onclick='redirectTo()'";
			$('#footerbtn1').html("<button class='btn btn-primary' data-dismiss='modal' aria-hidden='true' "+onclick+">Ok</button>");
			$('#vcloseBtn').html("<span "+onclick+" >x</span>");
			$('#myModalv').modal('show');
		}
		else if(epiFlag==4){
			//alert("epiFlag=="+epiFlag);
			//onclick = "onclick='redirectTo()'";
			//$('#rURL').val(url);
			window.location.href=url;
		}else{
			//onclick = "onclick=checkPopup('"+url+"')";
			onclick = "onclick=redirectToPage('"+url+"')";
		}
	}
	
	
	/*$('#footerbtn1').html("<button class='btn btn-primary' data-dismiss='modal' aria-hidden='true' "+onclick+">Ok</button>");
	$('#vcloseBtn').html("<span "+onclick+" >x</span>");
	$('#myModalv').modal('show');*/
}
function redirectToPage(url)
{
	//alert(url);
	checkPopup(url);
	if(isPortal)
		window.location.href="portaldashboard.do";
	else
		window.location.href=rdURL;
}
function clearDialog(divId)
{
	$("#"+divId ).dialog( "close" );
	$("#"+divId ).hide();
}
function checkPopup(url) {
	  var openWin = window.open(url,"directories=no,height=400,width=600,menubar=no,resizable=no,scrollbars=no,status=no,titlebar=no,top=0,location=no");
	  if (!openWin) {
		  	window.location.href=url;
	    } else {
	    window.open(url,"directories=no,height=400,width=600,menubar=no,resizable=no,scrollbars=no,status=no,titlebar=no,top=0,location=no");
	   }
}
function saveStrike(teacherAssessmentQuestionId)
{
	if(teacherAssessmentQuestionId==0)
		return;

	var teacherAssessmentQuestion = {teacherAssessmentQuestionId:dwr.util.getValue("teacherAssessmentQuestionId")};
	var teacherAssessmentdetail = {teacherAssessmentId:dwr.util.getValue("teacherAssessmentId"),assessmentType:dwr.util.getValue("teacherAssessmentType")};
	APIAssessmentCampaignAjax.saveStrike(teacherAssessmentdetail,teacherAssessmentQuestion,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){
		totalStrikes++;
	}
	});	
}


function finishAssessment(sessionCheck)
{

	$('#myModalvk').modal('hide');
  	$('#myModalv').modal('hide');
  	
	var newJobId = document.getElementById("newJobId").value;
	var epiJobId = document.getElementById("epiJobId").value;

	APIAssessmentCampaignAjax.finishAssessment(teacherAssessmentdetail,jobOrder,newJobId,sessionCheck,epiJobId,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){
		$('#tm-root').html(data);
	}
	});	
}

function checkAssessmentDone(teacherAssessmentId)
{
	var teacherAssessmentdetail = {teacherAssessmentId:teacherAssessmentId}
	APIAssessmentCampaignAjax.checkAssessmentDone(teacherAssessmentdetail,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){
		if(data==0)
			getAssessmentSectionCampaignQuestionsGrid(0);
		else
			finishAssessment(0)
	}
	});	
}
function portfolioURL(){
	var portfolioAction= document.getElementById("portfolioAction").value; 
	window.location.href=portfolioAction;
}

function saveAffidavit(){
	var affidavit = document.getElementById("affidavit").checked;
	APIAssessmentCampaignAjax.saveAffidavitFromAPI(affidavit,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){
		
	}
	});	
}

function sendMailToCandidate(assessmentType,status)
{
	var epiJobId = document.getElementById("epiJobId").value;
	var jobId = jJobId;
//	alert('assessmentType : '+assessmentType+' : status : '+status+' jobId : '+jobId+' epiJobId : '+epiJobId);
	APIAssessmentCampaignAjax.sendMailToCandidate(assessmentType,status,jobId,epiJobId,{
		async: true,
		errorHandler:handleError,
		callback: function(data){
			
		}
	});
}