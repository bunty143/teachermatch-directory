function createXMLHttpRequest()
{
	if (window.ActiveXObject)
		xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
	else if (window.XMLHttpRequest)
		xmlHttp = new XMLHttpRequest();
	else
		alert("AJAX NOT SUPPORTED BY YOUR BROWSER");
}
function trim(s)
{
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}

function encBase64(strInput)
{
	var temp=0;
	var data="";
	createXMLHttpRequest();  
	queryString = "encBase64.do?strInput="+strInput+"&dt="+new Date().getTime();
	xmlHttp.open("GET", queryString, false);
	xmlHttp.onreadystatechange = function()
	{
		if(xmlHttp.readyState == 4)
		{
			if(xmlHttp.status==200)	
			{
				temp=1;
				data = xmlHttp.responseText.toString()
			}			
			else
			{
				alert("server Error");
			}
		}
	}
	xmlHttp.send(null);
	if(temp==1)
		return data;
	else
		return "";
	
}

function decBase64(strInput)
{
	var temp=0;
	var data="";

	createXMLHttpRequest();  
	queryString = "decBase64.do?strInput="+strInput+"&dt="+new Date().getTime();
	xmlHttp.open("GET", queryString, false);
	xmlHttp.onreadystatechange = function()
	{
		if(xmlHttp.readyState == 4)
		{
			if(xmlHttp.status==200)	
			{
				temp=1;
				data = xmlHttp.responseText.toString()		
			}			
			else
			{
				alert("server Error");
			}
		}
	}
	xmlHttp.send(null);
	if(temp==1)
		return data;
	else
		return "";
	
}


function setEnc(){	
	var txtEncData = document.getElementById("txtEncData").value;
	var txt = encBase64(txtEncData);	
	document.getElementById("lblEncTxt").innerHTML=txt;
	document.getElementById("lblEncTxt").focus();
}
function setDec(){
	var txtDecData = document.getElementById("txtDecData").value;
	var txt = decBase64(txtDecData);	
	document.getElementById("lblDecTxt").innerHTML=txt;
	document.getElementById("lblDecTxt").focus();
}


function applyJB(authKey,orgType,jobId){	
	var candidateFirstName = document.getElementById("candidateFirstName").value;
	var candidateLastName = document.getElementById("candidateLastName").value;
	var candidateEmail = document.getElementById("candidateEmail").value;
	var url="applyJob.do?authKey="+authKey+"&orgType="+orgType+"&jobId="+jobId+"&candidateFirstName="+candidateFirstName+"&candidateLastName="+candidateLastName+"&candidateEmail="+candidateEmail	
	window.open(url)
}

function applyJBXML(authKey,orgType,jobId){	
	var candidateFirstName = document.getElementById("candidateFirstName").value;
	var candidateLastName = document.getElementById("candidateLastName").value;
	var candidateEmail = document.getElementById("candidateEmail").value;
	var url="xml/applyJob.do?authKey="+authKey+"&orgType="+orgType+"&jobId="+jobId+"&candidateFirstName="+candidateFirstName+"&candidateLastName="+candidateLastName+"&candidateEmail="+candidateEmail	
	window.open(url)
}

function applyJBJSON(authKey,orgType,jobId){	
	var candidateFirstName = document.getElementById("candidateFirstName").value;
	var candidateLastName = document.getElementById("candidateLastName").value;
	var candidateEmail = document.getElementById("candidateEmail").value;
	var url="json/applyJob.do?authKey="+authKey+"&orgType="+orgType+"&jobId="+jobId+"&candidateFirstName="+candidateFirstName+"&candidateLastName="+candidateLastName+"&candidateEmail="+candidateEmail	
	window.open(url)
}

function getCGView(authKey,orgType,jobId){
	var userEmail = document.getElementById("userEmail").value;
	var url="getCGView.do?authKey="+authKey+"&orgType="+orgType+"&jobId="+jobId+"&userEmail="+userEmail	
	window.open(url)
}

function getAppView(authKey,orgType,jobId){
	var userEmail = document.getElementById("userEmail").value;
	var url="getApplicantView.do?authKey="+authKey+"&orgType="+orgType+"&jobId="+jobId+"&userEmail="+userEmail	
	window.open(url)
}

function getEPIStatus(authKey,orgType,jobId){
	var userEmail = document.getElementById("userEmail").value;
	var url="getApplicantView.do?authKey="+authKey+"&orgType="+orgType+"&jobId="+jobId+"&userEmail="+userEmail	
	window.open(url)
}

function getCompScorByJbJSON(authKey,orgType,jobId){
	var userEmail = document.getElementById("userEmail").value;
	var url="json/getCompositeScoresForJob.do?authKey="+authKey+"&orgType="+orgType+"&jobId="+jobId+"&userEmail="+userEmail	
	window.open(url)
}

function getCompScorByJbXML(authKey,orgType,jobId){
	var userEmail = document.getElementById("userEmail").value;
	var url="xml/getCompositeScoresForJob.do?authKey="+authKey+"&orgType="+orgType+"&jobId="+jobId+"&userEmail="+userEmail
	window.open(url)
}
function storconEPI(authKey,orgType){	
	var candidateFirstName = document.getElementById("candidateFirstName").value;
	var candidateLastName = document.getElementById("candidateLastName").value;
	var candidateEmail = document.getElementById("candidateEmail").value;
	var url="startOrContinueEPI.do?authKey="+authKey+"&orgType="+orgType+"&candidateFirstName="+candidateFirstName+"&candidateLastName="+candidateLastName+"&candidateEmail="+candidateEmail
	window.open(url)
}

function epiStatusChkJSON(authKey,orgType){
	var candidateEmail = document.getElementById("candidateEmail").value;
	var url="json/epiStatus.do?authKey="+authKey+"&orgType="+orgType+"&candidateEmail="+candidateEmail
	window.open(url)
}
function epiStatusChkXML(authKey,orgType){
	var candidateEmail = document.getElementById("candidateEmail").value;
	var url="xml/epiStatus.do?authKey="+authKey+"&orgType="+orgType+"&candidateEmail="+candidateEmail
	window.open(url)
}


function getCompScoreJSON(authKey,orgType){
	var userEmail = document.getElementById("userEmail").value;
	var candidateEmail = document.getElementById("candidateEmail").value;
	var url="json/getCompositeScore.do?authKey="+authKey+"&orgType="+orgType+"&userEmail="+userEmail+"&candidateEmail="+candidateEmail
	window.open(url)
}
function getCompScoreXML(authKey,orgType){
	var userEmail = document.getElementById("userEmail").value;
	var candidateEmail = document.getElementById("candidateEmail").value;
	var url="xml/getCompositeScore.do?authKey="+authKey+"&orgType="+orgType+"&userEmail="+userEmail+"&candidateEmail="+candidateEmail
	window.open(url)
}

function addJobByXML(){
	alert($('#divJSON').find(".jqte_editor").text())
}
function addJobByJSON(){
	alert($('#divXML').find(".jqte_editor").text())
}