
function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert(resourceJSON.msgServerErr +exception.javaClassName);}
}

/////////////////////////////////////////////////////////////////////////
var page = 1;
var noOfRows = 10;
var sortOrderStr="";
var sortOrderType="1";
function getPaging(pageno)
{
	if(pageno!='')
	{
		page=pageno;	
	}
	else
	{
		page=1;
	}

	noOfRows = document.getElementById("pageSize").value;
	getCandidatesMails();
}
function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
	if(pageno!=''){
		page=pageno;	
	}else{
		page=1;
	}
	sortOrderStr=sortOrder;
	sortOrderType=sortOrderTyp;
	if(document.getElementById("pageSize")!=null){
		noOfRows = document.getElementById("pageSize").value;
	}else{
		noOfRows=10;
	}

	getCandidatesMails();

}
/*=======  searchTeacher on Press Enter Key ========= */
function chkForEnterSearchTeacher(evt)
{
	var charCode = ( evt.which ) ? evt.which : event.keyCode;	
	if(charCode==13)
	{
		searchTeacher();
	}	
}
/*========  display grid after Filter ===============*/
function searchTeacher()
{	
	page=1;
	getCandidatesMails()
	
}
function getCandidatesMails()
{
	var teacherDetail = {"teacherId":$('#teacherId').val()};
	if(teacherDetail.teacherId=="")
		return;
	
	AdminDashboardAjax.getCandidatesMails(teacherDetail,"","",noOfRows,page,sortOrderStr,sortOrderType,{ 
		async: true,
		callback: function(data){
		$('#divMain').html(data);
		applyScrollOnTbl();
	},
	errorHandler:handleError  
	});
}

function getMailMsg(mailId)
{
	
	AdminDashboardAjax.getMailMsg(mailId,{ 
		async: true,
		callback: function(data){
		var obj = data;
		$('#defaultSubject').val(obj.subject);
		$('#msgDash').find(".jqte_editor").html(obj.mailText);
		$('#myModal1setMsg').modal('show');
		$('#defaultSubject').focus();
	},
	errorHandler:handleError  
	});
}