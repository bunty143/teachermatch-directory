/* @Author: Gagan 
 * @Discription: view of edit domain js.
*/
var userIdVal=0;

var status = "";
var entityID = 0;
var userID = 0;
var page = 1;
var noOfRows = 50;
var sortOrderStr="";
var sortOrderType="";
function getPaging(pageno)
{
	if(pageno!='')
	{
		page=pageno;	
	}
	else
	{
		page=1;
	}
	
	noOfRows = document.getElementById("pageSize").value;
	var entityID	=	document.getElementById("MU_EntityType").value;
	var searchTextId	=	document.getElementById("districtOrSchooHiddenlId").value;
	if(searchTextId!=null || searchTextId!="")
	{
		searchRecordsByEntityType();
	}
	else
	{
		displayUserMasterRecords(entityID)
	}
}
function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
	if(pageno!=''){
		page=pageno;	
	}else{
		page=1;
	}
	sortOrderStr=sortOrder;
	sortOrderType=sortOrderTyp;
	if(document.getElementById("pageSize")!=null){
		noOfRows = document.getElementById("pageSize").value;
	}else{
		noOfRows=10;
	}
	searchRecordsByEntityType();
}

/*========  For Handling session Time out Error ===============*/
function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert(resourceJSON.msgSomeerror+": "+exception.javaClassName);}
}
/*======= searchRecordsByEntityType on Press Enter Key ========= */
function chkForEnterRecordsByEntityType(evt)
{
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==13)
	{
		searchUser();
	}	
}
function editUser(){
	window.location.href="addedituser.do?userFlag=1&userId="+userIdVal;
}
function beforeEditUser(userId){
	userIdVal=userId;
	document.getElementById("Msg").innerHTML=resourceJSON.msgAssociatedKeyContacts;
	$('#myModalMsg').modal('show');
}


/*========  SearchDistrictOrSchool ===============*/
$(document).ready(function(){$('.DSClass').hide();});
$(function(){
	function clearBackground(){
		$('#onlyDistrictName').css("background-color", "");
		$('#onlyBranchName').css("background-color", "");
		$('#onlyHeadQuarterName').css("background-color", "");
		$('#districtORSchoolName').css("background-color", "");
		}
	$('.clearback').click(function(){clearBackground();});
	
	$('#MU_EntityType').change(function(){
		$('#onlyDistrictName').val('');
		$('#districtHiddenId').val('');
		$('#districtORSchoolName').val('');
		$('#districtOrSchooHiddenlId').val('');
		$('#schoolHiddenId').val('');
		$('#onlyBranchName').val('');
		$('#branchHiddenId').val('');
		$('#onlyHeadQuarterName').val('');
		$('#headQuarterHiddenId').val('');
	});

	$('#onlyHeadQuarterName').blur(function(){		
		if($('#onlyHeadQuarterName').val()!=null && $('#onlyHeadQuarterName').val()!=""){
		$('#onlyDistrictName').val('');
		$('#districtHiddenId').val('');
		$('#districtORSchoolName').val('');
		//$('#districtOrSchooHiddenlId').val('');
		$('#schoolHiddenId').val('');
		$('#onlyBranchName').val('');
		$('#branchHiddenId').val('');
		}else{
			$('#onlyBranchName').val('');
			$('#branchHiddenId').val('');
		}
		});
	$('#onlyBranchName').blur(function(){
		if($('#onlyBranchName').val()!=null && $('#onlyBranchName').val()!=""){
		$('#onlyDistrictName').val('');
		$('#districtHiddenId').val('');
		$('#districtORSchoolName').val('');
		//$('#districtOrSchooHiddenlId').val('');
		$('#schoolHiddenId').val('');
		}
		});
	$('#onlyDistrictName').blur(function(){
		if($('#onlyDistrictName').val()!=null && $('#onlyDistrictName').val()!=""){		
		$('#districtORSchoolName').val('');
		//$('#districtOrSchooHiddenlId').val('');
		$('#schoolHiddenId').val('');
		}else{
			$('#districtORSchoolName').val('');
			$('#schoolHiddenId').val('');
		}
		});
});
function displayOrHideSearchBox(loggedEntityType)
{
	document.getElementById("districtORSchoolName").value="";
	document.getElementById("districtOrSchooHiddenlId").value="";
	document.getElementById("onlyDistrictName").value="";
	document.getElementById("districtHiddenId").value="";
	document.getElementById("firstName").value="";
	document.getElementById("lastName").value="";
	document.getElementById("emailAddress").value="";
	$('#errordiv').empty();
	$('#onlyDistrictName').css("background-color", "");
	$('#districtORSchoolName').css("background-color", "");
	var entity_type	=	document.getElementById("MU_EntityType").value;
	
	$(document).ready(function()
	{
		$(".schoolClass").hide();
		$(".DSClass").hide();
		$(".schoolClass").hide();
		$(".branchClass").hide();
		$(".hqClass").hide();
		$('#onlyBranchName').attr('readonly',false);	
		$('#onlyDistrictName').attr('readonly',false);
		$('#districtORSchoolName').attr('readonly',false);
		/*==== entity_type == 0 Means Select Entity Type 1 Means TM 2-> Districts and 3-> Schools */
		if(loggedEntityType ==	1)
		{
			$(".schoolClass").hide();
			if(entity_type ==	2)
			{	
				$(".DSClass").hide();
				$(".schoolClass").fadeIn();
				$('#onlyDistrictName').focus();
				$('#Searchbox').css('display','none');
				//$(".hqClass").fadeIn();
				//$(".branchClass").fadeIn();
				//$('#onlyHeadQuarterName').focus();	
				//$('#onlyBranchName').attr('readonly',true);	
				$('#onlyDistrictName').attr('readonly',false);	
				//$('#districtORSchoolName').focus();				
				document.getElementById("captionDistrictOrSchool").innerHTML	=	resourceJSON.msgDistrict;
			}
			if(entity_type ==	3)
			{
				$(".DSClass").show();
				//$(".hqClass").fadeIn();
				//$(".branchClass").fadeIn();
				$(".schoolClass").fadeIn();
				//$('#onlyHeadQuarterName').focus();	
				//$('#onlyBranchName').attr('readonly',true);					
				//$('#onlyDistrictName').focus();
				$('#districtORSchoolName').attr('readonly',true);
				document.getElementById("captionDistrictOrSchool").innerHTML	=	resourceJSON.msglblSchoolDep;
			}
			if(entity_type ==	5)
			{					
				$(".schoolClass").hide();
				$(".hqClass").fadeIn();
				$('#onlyHeadQuarterName').focus();					
			}
			if(entity_type ==	6)
			{	
				$(".schoolClass").hide();
				$(".hqClass").fadeIn();
				$(".branchClass").fadeIn();
				$('#onlyHeadQuarterName').focus();	
				$('#onlyBranchName').attr('readonly',true);								
			}
			
			$("#Searchbox").fadeIn();
			if(entity_type ==	loggedEntityType || entity_type==0)
			{
				$("#Searchbox").hide();
				$(".schoolClass").hide();
			}
		}
		if(loggedEntityType ==	2)
		{
			if(entity_type ==	2 || entity_type==0)
			{
				$("#Searchbox").hide();
			}
			if(entity_type ==	3)
			{
				document.getElementById("captionDistrictOrSchool").innerHTML	=	resourceJSON.msglblSchoolDep;
				$('.DSClass').show();
				$("#Searchbox").fadeIn();
				$('#districtORSchoolName').focus();
			}
		}
		if(loggedEntityType ==	5)
		{
				$(".schoolClass").hide();
				$(".branchClass").hide();
				$(".hqClass").hide();
				if(entity_type ==	2)
				{	
					$(".DSClass").hide();
					$(".schoolClass").fadeIn();	
					$('#onlyDistrictName').attr('readonly',true);
					$('#districtORSchoolName').attr('readonly',true);
					if($('#isBracnchExists').val().trim()==1){
					$(".branchClass").fadeIn();					
					}else{
						$('#onlyDistrictName').attr('readonly',false);
					}
					$('#onlyBranchName').focus();	
					//$('#districtORSchoolName').focus();				
					document.getElementById("captionDistrictOrSchool").innerHTML	=	resourceJSON.msglblSchoolDep;					
					
				}
				if(entity_type ==	3)
				{
					$(".DSClass").show();
					$('#onlyDistrictName').attr('readonly',true);
					$('#districtORSchoolName').attr('readonly',true);
					if($('#isBracnchExists').val().trim()==1){
					$(".branchClass").fadeIn();
					}else{
						$('#onlyDistrictName').attr('readonly',false);
					}
					$(".schoolClass").fadeIn();
					$('#onlyBranchName').focus();	
					//$('#onlyDistrictName').focus();
					document.getElementById("captionDistrictOrSchool").innerHTML	=	resourceJSON.msglblSchoolDep;
				}				
				if(entity_type ==	6)
				{	
					$(".schoolClass").hide();			
					$(".branchClass").fadeIn();
					$('#onlyBranchName').focus();	
					//$('#onlyBranchName').focus();				
				}
				
				$("#Searchbox").fadeIn();
				if(entity_type ==	loggedEntityType || entity_type==0)
				{
					$("#Searchbox").hide();
					$(".schoolClass").hide();
				}
		}
		if(loggedEntityType ==	6)
		{
				$(".schoolClass").hide();
				$(".branchClass").hide();
				$(".hqClass").hide();
				if(entity_type ==	2)
				{	
					$(".DSClass").show();
					$(".schoolClass").hide();					
					$('#districtORSchoolName').focus();				
					document.getElementById("captionDistrictOrSchool").innerHTML	=	resourceJSON.msglblSchoolDep;
				}
				if(entity_type ==	3)
				{
					$(".DSClass").show();
					$(".schoolClass").fadeIn();
					$('#onlyDistrictName').focus();
					document.getElementById("captionDistrictOrSchool").innerHTML	=	resourceJSON.msglblSchoolDep;
				}
				$("#Searchbox").fadeIn();
				if(entity_type ==	loggedEntityType || entity_type==0)
				{
					$("#Searchbox").hide();
					$(".schoolClass").hide();
				}
		}
	});
}
function searchUser()
{
	page = 1;
	searchRecordsByEntityType();
}
/*========  SearchDistrictOrSchool ===============*/
function searchRecordsByEntityType()
{
	var entityID				=	document.getElementById("MU_EntityType").value;
	var schoolId				=	document.getElementById("schoolHiddenId").value;
	var districtId				=	document.getElementById("districtHiddenId").value;
	var firstName				=	trim(document.getElementById("firstName").value); 
	var lastName				=	trim(document.getElementById("lastName").value);
	var emailAddress			=	trim(document.getElementById("emailAddress").value);
	var districtORSchoolName    =   "";
	try{
	districtORSchoolName    = trim(document.getElementById("districtORSchoolName").value);
	}catch(e){}
	var onlyDistrictName   		=   trim(document.getElementById("onlyDistrictName").value);	
	var userEntityType	=	document.getElementById("userEntityType").value;
	
	var onlyHeadQuarterName    =   trim(document.getElementById("onlyHeadQuarterName").value);
	var headQuarterId   		=   trim(document.getElementById("headQuarterHiddenId").value);
	var onlyBranchName    =   trim(document.getElementById("onlyBranchName").value);
	var branchId   		=   trim(document.getElementById("branchHiddenId").value);
	var districtId   		=   trim(document.getElementById("districtHiddenId").value);
	var schoolId   		=   trim(document.getElementById("schoolHiddenId").value);
	//alert('jlkjljl\==============='+districtId);
	var counter				=	0;
	var focusCount			=	0;
	$('#errordiv').empty();
	$('#onlyDistrictName').css("background-color", "");
	$('#districtORSchoolName').css("background-color", "");
	
	if(userEntityType==5){		
		var isBracnchExists=$('#isBracnchExists').val();
		var flagerrormessage=false;
		//alert('enter'+isBracnchExists+"      ==    "+branchId+" == "+$('#districtHiddenId').val()+" == "+$('#districtOrSchooHiddenlId').val());
		var entityType=entityID;
		if(entityType==3){
			var flagforfocus=true;
			if(isBracnchExists==1 && (branchId=="" || branchId=="0")){	
				$('#onlyBranchName').css('background-color','rgb(245, 231, 225)');
				$('#errordiv').append("&#149; Please enter Branch<br>");
				$('#onlyBranchName').focus();
				flagerrormessage=true;
				flagforfocus=false;
				counter++;									
			}
			if($('#districtHiddenId').val()=="" || $('#districtHiddenId').val()=="0"){
				$('#onlyDistrictName').css('background-color','rgb(245, 231, 225)');
				$('#errordiv').append("&#149; Please enter District<br>");
				if(flagforfocus)
				$('#onlyDistrictName').focus();	
				flagerrormessage=true;
				flagforfocus=false;
				counter++;
			}
			if($('#districtOrSchooHiddenlId').val()=="" || $('#districtOrSchooHiddenlId').val()=="0"){
				$('#districtORSchoolName').css('background-color','rgb(245, 231, 225)');
				$('#errordiv').append("&#149; Please enter School<br>");
				if(flagforfocus)
				$('#districtORSchoolName').focus();	
				flagerrormessage=true;
				counter++;
			}				
		}
		if(entityType==2){
			var flagforfocus=true;
			if(isBracnchExists==1 && (branchId=="" || branchId=="0")){	
				$('#onlyBranchName').css('background-color','rgb(245, 231, 225)');
				$('#errordiv').append("&#149; Please enter Branch<br>");
				$('#onlyBranchName').focus();
				flagerrormessage=true;
				flagforfocus=false;
				counter++;									
			}
			if($('#districtHiddenId').val()=="" || $('#districtHiddenId').val()=="0"){
				$('#onlyDistrictName').css('background-color','rgb(245, 231, 225)');
				$('#errordiv').append("&#149; Please enter District<br>");
				if(flagforfocus)
				$('#onlyDistrictName').focus();	
				flagerrormessage=true;
				flagforfocus=false;
				counter++;
			}					
		}
		if(entityType==6){
			if(isBracnchExists==1 && (branchId=="" || branchId=="0")){	
				$('#onlyBranchName').css('background-color','rgb(245, 231, 225)');
				$('#errordiv').append("&#149; Please enter Branch<br>");
				$('#onlyBranchName').focus();
				flagerrormessage=true;
				counter++;									
			}						
		}
		if(flagerrormessage){focusCount++;}
	}
	
	if(userEntityType==6){		
		var isBracnchExists=$('#isBracnchExists').val();
		var flagerrormessage=false;
		//alert('enter'+isBracnchExists+"      ==    "+branchId+" == "+$('#districtHiddenId').val()+" == "+$('#districtOrSchooHiddenlId').val());
		var entityType=entityID;
		if(entityType==3){
			var flagforfocus=true;			
			if($('#districtHiddenId').val()=="" || $('#districtHiddenId').val()=="0"){
				$('#onlyDistrictName').css('background-color','rgb(245, 231, 225)');
				$('#errordiv').append("&#149; Please enter District<br>");
				if(flagforfocus)
				$('#onlyDistrictName').focus();	
				flagerrormessage=true;
				flagforfocus=false;
				counter++;
			}
			if($('#districtOrSchooHiddenlId').val()=="" || $('#districtOrSchooHiddenlId').val()=="0"){
				$('#districtORSchoolName').css('background-color','rgb(245, 231, 225)');
				$('#errordiv').append("&#149; Please enter School<br>");
				if(flagforfocus)
				$('#districtORSchoolName').focus();	
				flagerrormessage=true;
				counter++;
			}				
		}
		if(entityType==2){
			var flagforfocus=true;		
			if($('#districtHiddenId').val()=="" || $('#districtHiddenId').val()=="0"){
				$('#onlyDistrictName').css('background-color','rgb(245, 231, 225)');
				$('#errordiv').append("&#149; Please enter District<br>");
				if(flagforfocus)
				$('#onlyDistrictName').focus();	
				flagerrormessage=true;
				flagforfocus=false;
				counter++;
			}					
		}		
		if(flagerrormessage){focusCount++;}
	}
	
	
	if(userEntityType==1){
		if(entityID==2) {			
			if(districtId=="0" || districtId==""){
				$('#errordiv').append("&#149; Please enter District<br>");			
				if(focusCount	==	0)
					$('#onlyDistrictName').focus();
				$('#onlyDistrictName').css("background-color", "#F5E7E1");				
				counter++;
				focusCount++;
			}
			/*else if(districtORSchoolName=="" && districtId=="0" || districtId==""){
				$('#errordiv').append("&#149; Please enter District<br>");			
				if(focusCount	==	0)
					$('#districtORSchoolName').focus();
				$('#districtORSchoolName').css("background-color", "#F5E7E1");
				counter++;
				focusCount++;
			}*/
		}else if(entityID==3){
			if(onlyDistrictName==""){
				$('#errordiv').append("&#149; "+resourceJSON.PlzEtrDistrict+"<br>");	
				if(focusCount	==	0)
					$('#onlyDistrictName').focus();
				$('#onlyDistrictName').css("background-color", "#F5E7E1");
				counter++;
				focusCount++;
			}
			if(districtORSchoolName==""){
				$('#errordiv').append("&#149; "+resourceJSON.PlzEtrSchool+"<br>");	
				if(focusCount	==	0)
					$('#districtORSchoolName').focus();
				$('#districtORSchoolName').css("background-color", "#F5E7E1");
				counter++;
				focusCount++;
			}
		}
	}
	if(userEntityType==1){
    	var flagerrormessage=false;
    	if(entityID==5){	
			if($('#headQuarterHiddenId').val()=="" || $('#headQuarterHiddenId').val()=="0"){
				$('#onlyHeadQuarterName').css('background-color','rgb(245, 231, 225)');
				$('#errordiv').append("&#149; Please enter Head Quarter<br>");
				$('#onlyHeadQuarterName').focus();	
				counter++;
				focusCount++;
			}    	
    	}else if(entityID==6){    		
    		var flagforfocus=true;
    		if($('#headQuarterHiddenId').val()=="" || $('#headQuarterHiddenId').val()=="0"){
				$('#onlyHeadQuarterName').css('background-color','rgb(245, 231, 225)');
				$('#errordiv').append("&#149; Please enter Head Quarter<br>");
				$('#onlyHeadQuarterName').focus();	
				flagerrormessage=true;
				flagforfocus=false;
				counter++;
			}  
    		var selectHQBranchExists=$('#isBranchExistsHiddenId').val();
    		if((selectHQBranchExists=='true' && branchId=="") || (selectHQBranchExists!='false' && branchId=="")){
				$('#onlyBranchName').css('background-color','rgb(245, 231, 225)');
				$('#errordiv').append("&#149; Please enter Branch<br>");
				if(flagforfocus)
				$('#onlyBranchName').focus();
				flagerrormessage=true;
				flagforfocus=false;
				counter++;									
			}
			if(flagerrormessage){focusCount++;}
    	}
    }
	
	if(userEntityType==2 && entityID==3){
		if(districtORSchoolName==""){
			$('#errordiv').append("&#149; "+resourceJSON.PlzEtrSchool+"<br>");	
			if(focusCount	==	0)
				$('#districtORSchoolName').focus();
			$('#districtORSchoolName').css("background-color", "#F5E7E1");
			counter++;
			focusCount++;
		}
	}
	
	var resultFlag=true;
	if(districtORSchoolName=="" && entityID==2){
		document.getElementById("districtOrSchooHiddenlId").value=districtId;
	}
	if(districtORSchoolName=="" && entityID==3){
		document.getElementById("districtOrSchooHiddenlId").value=schoolId;
	}
	var searchTextId	=	document.getElementById("districtOrSchooHiddenlId").value;
	
	if(districtORSchoolName!="" && searchTextId==0){
		resultFlag=false;
	}
	
	
	
	//UserAjax.displayRecordsByEntityType(firstName,lastName,emailAddress,resultFlag,entityID,searchTextId,noOfRows,page,sortOrderStr,sortOrderType,{
	
	if(counter	==	0)
	{
		UserAjax.displayRecordsByEntityType(resultFlag,entityID,searchTextId,noOfRows,page,sortOrderStr,sortOrderType,firstName,lastName,emailAddress,headQuarterId,branchId,districtId,{ 
			async: true,
			callback: function(data)
			{
				$('#divMain').html(data);
				applyScrollOnTbl();
			},
			errorHandler:handleError
		});
	}
}
/*========  displayUserMasterRecords ===============*/
function displayUserMasterRecords(entityID)
{
	var MU_EntityType	=	document.getElementById("MU_EntityType").value;
	var searchTextId	=	document.getElementById("districtHiddenId").value;
	if(entityID==3){
		searchTextId=document.getElementById("schoolHiddenId").value;
	}
	
	if(searchTextId	==	"" || searchTextId	==	null)
	{
		searchTextId	=	"";
	}
	if(MU_EntityType ==	0 || MU_EntityType == entityID){
		$("#Searchbox").hide();
	}
	UserAjax.displayRecordsByEntityType(true,entityID,searchTextId,noOfRows,page,sortOrderStr,sortOrderType, { 
		async: true,
		callback: function(data)
		{
			$('#divMain').html(data);
			applyScrollOnTbl();
		},
		errorHandler:handleError
	});
}

/*===================activateDeactivateQuestUser==============*/
function activateDeactivateQuestUser(entityID,userid,state)
{
	UserAjax.activateDeactivateQuestUser(userid,state, { 
		async: true,
		callback: function(data)
		{
		    searchRecordsByEntityType();
		},
		errorHandler:handleError
	});
}

function beforeAuthorisedUser(entityId, userId , state , email ){
	status = state;
	entityID = entityId;
	userID = userId;
	if(state=="A")
	{
		document.getElementById("AuthorisedMsg").innerHTML=""+resourceJSON.msgReallyAuthorize+" <b>"+ email +"</b> ?";
	}
	else if(state=="I")
	{
		document.getElementById("AuthorisedMsg").innerHTML=""+resourceJSON.msgReallyUnauthorize+" <b>"+ email +"</b> ?";
	}	
	$('#authorisedMsg').modal('show');
}

/*===================authorisedUnauthorisedQuestUser==============*/
function authorisedUnauthorisedUser()
{
	UserAjax.authorisedUnauthorisedUser(userID,status, { 
		async: true,
		callback: function(data)
		{
		    searchRecordsByEntityType();
		},
		errorHandler:handleError
	});
}

/*========  activateDeactivateUser ===============*/
function activateDeactivateUser(entityID,userId,status)
{
	UserAjax.activateDeactivateUser(userId,status, { 
		async: true,
		callback: function(data)
		{
		    searchRecordsByEntityType();
		},
		errorHandler:handleError
	});
}




var count=0;
var index=-1;
var length=0;
var divid='';
var txtid='';
var hiddenDataArray = new Array();
var isBranchExistsArray = new Array();
var showDataArray = new Array();
var degreeTypeArray = new Array();
var hiddenId="";
function getDistrictMasterAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	//alert(" hiddenId :: "+hiddenId);
	hiddenId=hiddenId;
	if(document.getElementById("userSessionEntityTypeId").value==2)
	{
	  document.getElementById("districtHiddenId").value = hiddenId;
	}
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("districtORSchoolName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getDistrictMasterArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getDistrictMasterArray(districtOrSchoolName){
	var searchArray = new Array();
	var entityID	=	document.getElementById("MU_EntityType").value;
	var districtIdForSchool	=document.getElementById("districtHiddenId").value;
	var userSessionEntityTypeId	=document.getElementById("userSessionEntityTypeId").value;
	if(userSessionEntityTypeId==1){
		//districtIdForSchool=0;
	}
	//alert(" userSessionEntityTypeId & districtOrSchoolName & districtIdForSchool :: "+userSessionEntityTypeId+"   :::::  "+districtOrSchoolName +"  :::  "+districtIdForSchool);
	
	
	if(entityID==2){
		UserAjax.getFieldOfDistrictList(districtOrSchoolName,{ 
			async: false,
			callback: function(data){
			hiddenDataArray = new Array();
			showDataArray = new Array();
			for(i=0;i<data.length;i++){
				searchArray[i]=data[i].districtName;
				showDataArray[i]=data[i].districtName;
				hiddenDataArray[i]=data[i].districtId;
			}
		},
		errorHandler:handleError
		});	
	}else {
		UserAjax.getFieldOfSchoolList(districtIdForSchool,districtOrSchoolName,{ 
			async: false,
			callback: function(data){
			hiddenDataArray = new Array();
			showDataArray = new Array();
			for(i=0;i<data.length;i++){
				searchArray[i]=data[i].schoolName;
				showDataArray[i]=data[i].schoolName;
				hiddenDataArray[i]=data[i].schoolId;
			}
		},
		errorHandler:handleError
		});	
	}
	return searchArray;
}



function getOnlyDistrictAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	//document.getElementById("districtORSchoolName").value="";
	document.getElementById("districtHiddenId").value="";
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("onlyDistrictName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getDistrictMasterOnlyArray(txtSearch.value);
		fatchData2(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getDistrictMasterOnlyArray(districtOrSchoolName){
	
	var searchArray = new Array();
	//var entityID	=	document.getElementById("AEU_EntityType").value;
	//var districtId=	document.getElementById("districtSessionId").value;
	//alert("=========");
		var branchId=$('#branchHiddenId').val();
		if($('#userEntityType').val()==6){
			branchId=$('#branchId').val();
		}
		var headQuarterId=$('#headQuarterHiddenId').val();
		UserAjax.getFieldOfDistrictListByBranchOrHQId(districtOrSchoolName,branchId,headQuarterId,{ 
			async: false,
			callback: function(data){
			hiddenDataArray = new Array();
			showDataArray = new Array();
			for(i=0;i<data.length;i++){
				searchArray[i]=data[i].districtName;
				showDataArray[i]=data[i].districtName;
				hiddenDataArray[i]=data[i].districtId;
			}
		},
		errorHandler:handleError
		});	
	
	return searchArray;
}

var selectFirst="";

var fatchData= function(txtSearch,searchArray,txtId,txtdivid){
	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
				
			}
			
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}

var fatchData2= function(txtSearch,searchArray,txtId,txtdivid){
	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
				
			}
			
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}

function hideDistrictMasterDiv(dis,hiddenId,divId)
{
	document.getElementById("districtOrSchooHiddenlId").value="";
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			document.getElementById("districtOrSchooHiddenlId").value=hiddenDataArray[index];
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}

function hideDistrictDiv(dis,hiddenId,divId)
{
	//document.getElementById("districtHiddenId").value="";
	$('#errordiv').empty();
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
			document.getElementById("districtOrSchooHiddenlId").value=hiddenDataArray[index];
		}
	
		if(dis.value==""){
				
				$('#districtORSchoolName').attr('readonly', true);
				document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			$('#districtORSchoolName').attr('readonly', false);
			dis.value=showDataArray[index];
			document.getElementById("districtOrSchooHiddenlId").value=hiddenDataArray[index];
			document.getElementById("districtOrSchooHiddenlId").value=hiddenDataArray[index];
		}
		
	}else{
		if(document.getElementById(hiddenId))
		{
			document.getElementById(hiddenId).value="";
		}
		if(dis.value!=""){
			var focuschk=0;
			onlyDistrictName=2;
			$('#errordiv').empty();	
			$('#errordiv').show();
			
				$('#errordiv').append("&#149; "+resourceJSON.msgvaliddistrict+"<br>");
			
			
			if(focus==0)
			$('#onlyDistrictName').focus();
			
			focus++;
		}
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}

var downArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index<length-1){
			for(var i=0;i<10;i++){
				{
					if(document.getElementById('divResult'+txtdivid+i))
						var div_id=document.getElementById('divResult'+txtdivid+i);
					if(div_id)
					{
						if(div_id.className=='over')
							index=div_id.id.split('divResult'+txtdivid)[1];
						div_id.className='normal';
					}
				}
			}
			index++;
			if(document.getElementById('divResult'+txtdivid+index))
			{
				var div_id=document.getElementById('divResult'+txtdivid+index);
				div_id.className='over';
				document.getElementById(txtid).value = div_id.innerHTML;
				selectFirst=div_id.innerHTML;
			}
		}
	}
}

var upArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index>0){
			for(var i=0;i<length;i++){

				var div_id=document.getElementById('divResult'+txtdivid+i);
				if(div_id)
				{
					if(div_id.className=='over')
						index=div_id.id.split('divResult'+txtdivid)[1];
					div_id.className='normal';
				}
			}
			index--;
			if(document.getElementById('divResult'+txtdivid+index))
				document.getElementById('divResult'+txtdivid+index).className='over';
			if(txtid && document.getElementById('divResult'+txtdivid+index))
			{
				document.getElementById(txtid).value=
					document.getElementById('divResult'+txtdivid+index).innerHTML;
				selectFirst=document.getElementById('divResult'+txtdivid+index).innerHTML;
			}
		}
	}
}

var overText=function (div_value,txtdivid) {

	for(var i=0;i<length;i++)
	{
		if(document.getElementById('divResult'+i))
		{
			var div_id=document.getElementById('divResult'+i);

			if(div_id.className=='over')
				index=div_id.id.split(txtdivid)[1];
			div_id.className='normal';
		}
	}
	div_value.className = 'over';
	document.getElementById(txtid).value= div_value.innerHTML;
}
function trim(s)
{
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}
function mouseOverChk(txtdivid,txtboxId)
{	
	for(var i=0;i<length;i++)
	{
		$('#divResult'+txtdivid+i).mouseover({param1: i,param2:txtdivid, param3:txtboxId}, fireMouseOverEvent);
	}
}


function fireMouseOverEvent(event)
{
	for(var i=0;i<length;i++)
	{	
		document.getElementById('divResult'+event.data.param2+i).className='normal';
	}
    document.getElementById('divResult'+event.data.param2+event.data.param1).className='over';	       
   	document.getElementById(event.data.param3).value= $('#divResult'+event.data.param2+event.data.param1).text();
    index=event.data.param1;
}


/***************************************************************auto complete ******************************************************/
//add by 18-05-2015
function getOnlyHeadQuarterAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	//alert('enter1');
	//document.getElementById("onlyHeadQuarterName").value="";
	document.getElementById("headQuarterHiddenId").value="";
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("onlyHeadQuarterName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getHeadQuarterOnlyArray(txtSearch.value);
		//alert(searchArray);
		fatchData2(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}
function getOnlyHeadQuarterAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	document.getElementById("headQuarterHiddenId").value="";
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("onlyDistrictName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		//alert('hljkhlkhl================'+txtSearch.value);
		document.getElementById(divid).style.display='block';
		searchArray = getHeadQuarterOnlyArray(txtSearch.value);
		//alert(searchArray);
		fatchData2(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}
function getHeadQuarterOnlyArray(headQuarterName){
	
	var searchArray = new Array();
	//alert("========="+headQuarterName);
			AutoSearchFilterAjax.getFieldOfHeadQuarterList(headQuarterName,{ 
			async: false,
			callback: function(data){
			isBranchExistsArray=new Array();
			hiddenDataArray = new Array();
			showDataArray = new Array();
			for(i=0;i<data.length;i++){					
				searchArray[i]=data[i].headQuarterName;
				showDataArray[i]=data[i].headQuarterName;
				hiddenDataArray[i]=data[i].headQuarterId;
				isBranchExistsArray[i]=data[i].isBranchExist;
			}
		},
		errorHandler:handleError
		});	
	
	return searchArray;
}
function hideHeadQuarterDiv(dis,hiddenId,divId)
{
	$('#errordiv').empty();
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
			document.getElementById("headQuarterHiddenId").value=hiddenDataArray[index];
		}
	
		if(dis.value==""){
				
				//$('#onlyHeadQuarterName').attr('readonly', true);
				document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			$('#onlyHeadQuarterName').attr('readonly', false);
			dis.value=showDataArray[index];
			document.getElementById("headQuarterHiddenId").value=hiddenDataArray[index];
			document.getElementById("headQuarterHiddenId").value=hiddenDataArray[index];
			document.getElementById("isBranchExistsHiddenId").value=isBranchExistsArray[index];
			
		}
		
	}else{
		if(document.getElementById(hiddenId))
		{
			document.getElementById(hiddenId).value="";
		}
		if(dis.value!=""){
			var focuschk=0;
			onlyDistrictName=2;
			$('#errordiv').empty();	
			$('#errordiv').show();
			
				$('#errordiv').append("&#149; Please enter valid Head Quarter<br>");
			
			
			if(focus==0)
			$('#onlyHeadQuarterName').focus();			
			focus++;
		}
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
		//alert($('#onlyHeadQuarterName').val());
		if($('#onlyHeadQuarterName').val().trim()!=''){			
			$('#districtORSchoolName').attr('readonly',true);
			$('#onlyBranchName').attr('readonly',true);
			$('#onlyDistrictName').attr('readonly',true);
		}
		if($('#MU_EntityType').val()=='3' && $('#onlyHeadQuarterName').val()=='' && $('#onlyBranchName').val()==''){
			$('#districtORSchoolName').attr('readonly',true);
			$('#onlyDistrictName').attr('readonly',false);
		}
		if($('#MU_EntityType').val()!='5' && $('#isBranchExistsHiddenId').val()!='' && $('#isBranchExistsHiddenId').val().trim()=='true'){
			$('#branchClass').fadeIn();
			$('#onlyBranchName').attr('readonly',false);
			$('#onlyBranchName').show();
		}else if($('#isBranchExistsHiddenId').val()!=''){
			$('.branchClass').hide();
			if($('#MU_EntityType').val()=='3'){
				try{
					$('#onlyDistrictName').attr('readonly',false);
					$('#onlyDistrictName').focus();
					$('#onlyDistrictName').val('');
					$('#districtORSchoolName').val('');
					}catch(e){}			
			}else{
				try{
					$('#onlyDistrictName').attr('readonly',false);
					$('#districtORSchoolName').attr('readonly',false);
					$('#districtORSchoolName').focus();
					$('#onlyBranchName').val('');
					$('#districtORSchoolName').val('');
					}catch(e){}
			}
		}
	}
	index = -1;
	length = 0;
}

function getOnlyBranchAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	//alert('enter1');
	//document.getElementById("onlyHeadQuarterName").value="";
	document.getElementById("branchHiddenId").value="";
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("onlyBranchName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getBranchOnlyArray(txtSearch.value);
		//alert(searchArray);
		fatchData2(txtSearch,searchArray,txtId,txtdivid);		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}
function getOnlyBranchAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	document.getElementById("branchHiddenId").value="";
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("onlyBranchName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		//alert('hljkhlkhl================'+txtSearch.value);
		document.getElementById(divid).style.display='block';
		searchArray = getBranchOnlyArray(txtSearch.value);
		//alert(searchArray);
		fatchData2(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}
function getBranchOnlyArray(branchName){
	
	var searchArray = new Array();
	//alert("========="+branchName);
	var headQuarterId=$('#headQuarterHiddenId').val();
	if($('#userEntityType').val()==5){
		headQuarterId=$('#headQuarterId').val();
	}
		AutoSearchFilterAjax.getBranchListByHQ(headQuarterId,branchName,{ 
			async: false,
			callback: function(data){
			hiddenDataArray = new Array();
			showDataArray = new Array();
			for(i=0;i<data.length;i++){				
				searchArray[i]=data[i].branchName;
				showDataArray[i]=data[i].branchName;
				hiddenDataArray[i]=data[i].branchId;
			}
		},
		errorHandler:handleError
		});	
	
	return searchArray;
}
function hideBranchDiv(dis,hiddenId,divId)
{
	$('#errordiv').empty();
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
			document.getElementById("branchHiddenId").value=hiddenDataArray[index];
		}
	
		if(dis.value==""){
				
				//$('#onlyBranchName').attr('readonly', true);
				document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			$('#onlyBranchName').attr('readonly', false);
			dis.value=showDataArray[index];
			document.getElementById("branchHiddenId").value=hiddenDataArray[index];
			document.getElementById("branchHiddenId").value=hiddenDataArray[index];
		}
		
	}else{
		if(document.getElementById(hiddenId))
		{
			document.getElementById(hiddenId).value="";
		}
		if(dis.value!=""){
			var focuschk=0;
			onlyDistrictName=2;
			$('#errordiv').empty();	
			$('#errordiv').show();
			
				$('#errordiv').append("&#149; Please enter valid Branch<br>");
			
			
			if(focus==0)
			$('#onlyBranchName').focus();			
			focus++;
		}
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
		//alert($('#onlyHeadQuarterName').val());
		if($('#onlyBranchName').val().trim()!=''){			
			$('#districtORSchoolName').attr('readonly',true);
			$('#onlyDistrictName').attr('readonly',true);
		}else if($('#MU_EntityType').val()=='3' && $('#onlyHeadQuarterName').val()!='' && $('#isBranchExistsHiddenId').val()!='' && $('#isBranchExistsHiddenId').val().trim()=='true' && $('#onlyBranchName').val().trim()==''){
			$('#districtORSchoolName').attr('readonly',true);
			$('#onlyDistrictName').attr('readonly',true);
		}else if($('#MU_EntityType').val()=='3' && $('#onlyHeadQuarterName').val()=='' && $('#onlyBranchName').val()==''){
			$('#districtORSchoolName').attr('readonly',true);
			$('#onlyDistrictName').attr('readonly',false);
		}
		if($('#onlyBranchName').val()!=''){			
			if($('#MU_EntityType').val()=='3'){
				try{
					$('#onlyDistrictName').attr('readonly',false);
					$('#onlyDistrictName').focus();
					$('#onlyDistrictName').val('');
					$('#districtORSchoolName').val('');
					$('#districtHiddenId').val();
					$('#schoolHiddenId').val();
					}catch(e){}			
			}else{
				try{					
					$('#onlyDistrictName').attr('readonly',false);
					$('#districtORSchoolName').focus();
					$('#districtORSchoolName').val('');
					$('#schoolHiddenId').val();
					$('#districtHiddenId').val();
					$('#districtORSchoolName').attr('readonly',false);
					}catch(e){}
			}
		}
	}
	index = -1;
	length = 0;
}
//end by 18-05-2015
/***************************************************************End********************************************************************/