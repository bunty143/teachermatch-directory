function getJobOrderList_msu(tId)
{
	teacherId=tId;	
	$('#divJobHeaderError').empty();
	var jobCategoryId_msu=0;
	var subjectId_msu=0;
	try
	{
		jobCategoryId_msu=$("#jobCategoryId_msu").val().trim();
		subjectId_msu=$("#subjectId_msu").val().trim();
	}catch(err)
	{}
	/*if(jobCategoryId_msu==0)
	{
		$('#divJobHeaderError').html("&#149; Please select Job Category");
		$('#divJobHeaderError').show();
		return false;
	}
	else
	{
		getJobOrderList();
	}*/
	
	getJobOrderList();
}


function displayMassStatus_msu(tId)
{
	$('#divJobHeaderError').empty();
	var checkedJobId_MSU_Values="";
	var inputs_chkjobId_msu = document.getElementsByName("chkjobId_msu");
	for (var i = 0; i < inputs_chkjobId_msu.length; i++) 
	{
		if (inputs_chkjobId_msu[i].type == 'checkbox') 
		{
        	if(inputs_chkjobId_msu[i].checked)
        	{
        		checkedJobId_MSU_Values+=inputs_chkjobId_msu[i].value+",";
            }
        }
	}
	
	if(checkedJobId_MSU_Values=="")
	{
		$('#divJobHeaderError').html("&#149; "+resourceJSON.PlzChangeStatus+"");
		$('#divJobHeaderError').show();
		return false;
	}
	else
	{
		var isOverrideForAllJob_msu="0";
		if(document.getElementById("isOverrideForAllJob").checked)
			isOverrideForAllJob_msu="1";
		
		$('#myModalJobList').modal('hide');
		$('#myModalStatus_msu').modal('show');
		displayAllStatusByTIDAndJID(tId,checkedJobId_MSU_Values,isOverrideForAllJob_msu);
	}
	
}


function closeStatus_msu()
{
	$('#myModalStatus_msu').modal('hide');
	$('#myModalJobList').modal('show');
}


function displayAllStatusByTIDAndJID(teacherId,jobIds,isOverrideForAllJob_msu)
{
	MassStatusUpdateService.displayAllStatusByTIDAndJID(teacherId,jobIds,isOverrideForAllJob_msu,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
			document.getElementById("divAllStatusByTIDAndJID_msu").innerHTML=data.toString();		
	    }
	});
}
function showStatusDetails_msu(iShowNextProcess,teacherName,statusName,statusId,secondaryStatusId,teacherId,jobIds,dclnORrem,isOverrideForAllJob_msu)
{
	document.getElementById("emailToDASA_msu").disabled=false;
	resetNoteEditor_msu();
	
	document.getElementById("statusDetailsTitle_msu").innerHTML=statusName +" "+resourceJSON.MsgEvaluationFor+" "+teacherName ;
	document.getElementById("statusInfoTitle_msu").innerHTML=statusName +" "+resourceJSON.MsgEvaluationFor+" "+teacherName ;
	
	document.getElementById("statusNotesJobIds").value=jobIds;
	document.getElementById("statusNotesTeacherId").value=teacherId;
	
	document.getElementById("updateStatusName_msu").value=statusName;
	
	
	
	if(iShowNextProcess==0)
	{
		
		$('#myModalStatus_msu').modal('hide');
		$('#myModalStatusInfo_msu').modal('show');
	}
	else
	{
		$('#myModalStatus_msu').modal('hide');
		showInprocessAndFinalizeButton("true");
		document.getElementById("noteMainDiv_msu").style.display="none";
		//$('#myModalStatus').modal('hide');
		$('#errorStatusNote_msu').hide();
		MassStatusUpdateService.getStatusDetailsInfo_msu(teacherId,jobIds,dclnORrem,statusId,secondaryStatusId,isOverrideForAllJob_msu,
		{
			async: false,
			errorHandler:handleError,
			callback:function(data)
			{
				if(data[0]!=null && data[0]=='NoofQ_NoMatch')
				{
					$('#myModalStatusInfo_msu').modal('show');
					document.getElementById("statusInfoText_msu").innerHTML=""+resourceJSON.MsgQuestDidNotMatch+"";
				}
				else if(data[2]!=null && data[2]=='NoofQ_Match_Q_Diff')
				{
					$('#myModalStatusInfo_msu').modal('show');
					document.getElementById("statusInfoText_msu").innerHTML=""+resourceJSON.MsgQuestIsMatched+"";
				}
				else if(data[1]!=null)
				{
					$('#myModalStatusDetailsInfo_msu').modal('show');
					document.getElementById("divStatusDetailsInfo").innerHTML=data[1];
					showOtherButtons(data[3]);
				}
			}
		});

	}
}


function closeStatusInfo_msu()
{
	$('#myModalStatusInfo_msu').modal('hide');
	$('#myModalStatus_msu').modal('show');
}


function setStatusTilteForStatusDetails_msu(teacherName,statusName){
	document.getElementById("statusDetailsTitle_msu").innerHTML=statusName +" "+resourceJSON.MsgEvaluationFor+" "+teacherName ;
}

function resetNoteEditor_msu(){
	$('#statusNotes_msu').find(".jqte_editor").html("");
}

function openNoteDiv_msu(){
	
	var DistrictId = document.getElementById("districtId").value;
	var statusName = $('#updateStatusName_msu').val();
	
	var userId="";
	try{userId = $("#userId").val();}catch(err){userId="";}
	
	if(userId!="")
		DistrictId = userId;
	
	ManageStatusAjax.statusWiseSectionList(DistrictId,statusName,{
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{	if(data!=null){
				document.getElementById("templateDivStatusTP").style.display="block";
				$('#statuswisesectionTP').html(data);
				var selectId = $("#statuswisesectionTP option:selected").val();
				getsectionwiselistTPGolu(selectId);
		}
		}
	});
	document.getElementById("noteMainDiv_msu").style.display="inline";
	document.getElementById("templateDivStatusTP").style.display="inline";
	document.getElementById("showStatusNoteFile_msu").innerHTML="";
	document.getElementById("showTemplateslistTP").style.display="none";
	removeStatusNoteFile_msu();
	$('#errorStatusNote_msu').hide();
	resetNoteEditor_msu();
}

/* ==== Gourav  : [Start  Js Method for the functinality of status wise email templates list]*/
	
function getsectionwiselistTPGolu(value){
	document.getElementById("showTemplateslistTP").style.display="none";
	//document.getElementById("changeTemplateBody").style.display="none";
	$('#statusNotes_msu').find(".jqte_editor").html("");
	if(value!=0){
		ManageStatusAjax.getSectionWiseTemaplteslistTPGolu(value,{
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{
			document.getElementById("addTemplateslistTP").innerHTML=data;
			document.getElementById("showTemplateslistTP").style.display="block";
			}
		});
	}
}

function setTemplateByLIstIdTPGolu(templateId){
	$('#statusNotes_msu').find(".jqte_editor").html("");
	if(templateId!=0){
		ManageStatusAjax.getTemplateByLIst(templateId,{
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{	
				//document.getElementById("statusNotes").innerHTML=data;
				$('#statusNotes_msu').find(".jqte_editor").html(data);
			}
		});
	}
}

/* ==== Gourav  : [Start  Js Method for the functinality of status wise email templates list end]*/

function addStatusNoteFileType_msu()
{
	$('#statusNoteFileNames_msu').empty();
	$('#statusNoteFileNames_msu').html("<a href='javascript:void(0);' onclick='removeStatusNoteFile_msu();'><img src='images/can-icon.png' title='remove'/></a> <input id='statusNoteFileName_msu' name='statusNoteFileName_msu' size='20' title='"+resourceJSON.ChooseFile+"' type='file' style='margin-left:20px;margin-top:-15px;'><br>&nbsp;"+resourceJSON.MaxSize10Mb+".");
}

function removeStatusNoteFile_msu()
{
	$('#statusNoteFileNames_msu').empty();
	$('#statusNoteFileNames_msu').html("<a href='javascript:void(0);' onclick='addStatusNoteFileType_msu();'><img src='images/attach.png'/></a> <a href='javascript:void(0);' onclick='addStatusNoteFileType_msu();'>"+resourceJSON.AttachFile+"</a>");
}

function showInprocessAndFinalizeButton(isShow)
{
	//document.getElementById("checkOptions_msu").style.display="inline";
	
	document.getElementById('emailToDASA_msu').checked=false;
	document.getElementById('emailToCA_msu').checked=false;
	
	if(isShow=='true')
	{
		document.getElementById("statusInProcess").style.display="inline";
		document.getElementById("statusFinalize_msu").style.display="inline";
	}
	else
	{
		document.getElementById("statusInProcess").style.display="none";
		document.getElementById("statusFinalize_msu").style.display="none";
	}
}

function showOtherButtons(dclnORrem)
{
	if(document.getElementById("statusInProcess").style.display=='inline')
	{
		
		document.getElementById("unRemove_msu").style.display="none";
		document.getElementById("unDecline_msu").style.display="none";
		
		if(dclnORrem=='dcln')
		{
			document.getElementById("unRemove_msu").style.display="none";
			document.getElementById("unDecline_msu").style.display="inline";
		}
		else if(dclnORrem=='rem')
		{
			document.getElementById("unRemove_msu").style.display="inline";
			document.getElementById("unDecline_msu").style.display="none";
		}
	}
}

function downloadAttachInstructionFileName_msu(filePath,fileName,secondaryStatusName)
{
	$('#loadingDiv').show();
	MassStatusUpdateService.downloadAttachInstructionFile_msu(filePath,fileName,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			$('#loadingDiv').hide();
			if(data!='')
			{
				if(data.indexOf(".doc")!=-1)
				{
					try{$('#modalDownloadIAFN_msu').modal('hide');}catch(err){}
				    document.getElementById('ifrmAttachInstructionFileName_msu').src = ""+data+"";
				}
				else
				{
					document.getElementById('ifrmAttachInstructionFileName_msu').src = ""+data+"";
					try{
						$( "#modalDownloadIAFN_msu" ).draggable({
							handle:'#modalDownloadIAFNMove_msu'
						});
						$('#modalDownloadIAFN_msu').modal('hide');
						$('#myModalLabelInstructionHeader_msu').html(""+resourceJSON.MsgInstruction+" "+secondaryStatusName);
						$('#modalDownloadIAFN_msu').modal('show');
					}catch(err){}
				}
			}
		}
	});
}

function saveStatusNoteOrSliderScore(flag)
{
	//alert("saveStatusNoteOrSliderScore "+flag);
	
	document.getElementById("flag_msu").value=flag;
	
	var noteDiv_msu=0;
	try{
		if(document.getElementById("noteMainDiv_msu").style.display=="inline")
		{
			noteDiv_msu=1;
		}
		//alert(" noteDiv_msu open :: "+noteDiv_msu);
	}catch(err){}
	
	var topSliderDisable=0;
	try{
		topSliderDisable=document.getElementById("topSliderDisable").value;
		//alert(" topSliderDisable "+topSliderDisable);
	}catch(err){}
	
	var questionScoreSliderDisable=0;
	try{
		questionScoreSliderDisable=document.getElementById("questionScoreSliderDisable").value;
		//alert(" questionScoreSliderDisable "+questionScoreSliderDisable);
	}catch(err){}
	
	var countQuestionSlider=0;
	try{
		countQuestionSlider=document.getElementById("countQuestionSlider").value;
		//alert(" countQuestionSlider "+countQuestionSlider);
	}catch(err){}
	
	
	// Start for save Question score
	var topSliderValue=0;
	try{
		var iframeNorm = document.getElementById('ifrmTopSlider_msu');
		var innerNorm = iframeNorm.contentDocument || iframeNorm.contentWindow.document;
		var inputNormScore = innerNorm.getElementById('topSlider_msu');
		topSliderValue=inputNormScore.value;
	}catch(e){}	
	
	//alert("top Slider Value "+topSliderValue);
	
	
	var answerId_arrary = new Array();
	var answerScore_arrary = new Array();
	
	var no_Of_Question_Slider_Value=0;
	var questionSliderScoreSum=0;
	//alert("1");
	var iCountScoreSlider=document.getElementById("countQuestionSlider").value;
	//alert("iCountScoreSlider "+iCountScoreSlider);
	if(iCountScoreSlider > 0)
	{
		//alert("If In");

		for(var i=1;i<= iCountScoreSlider;i++)
		{
			//alert(" for loop "+i);
			var questionscore=0;
			var answerId=0;
			
			try{
				var ifrmQuestion = document.getElementById('ifrmQuestion_msu_'+i);
				//alert("ifrmQuestion "+ifrmQuestion);
				if(ifrmQuestion!=null)
				{
					//alert("2nd IF In - 01");
					var innerQuestion = ifrmQuestion.contentDocument || ifrmQuestion.contentWindow.document;
					//alert("2nd IF In - 02");
					var inputQuestionFrm = innerQuestion.getElementById('questionFrm_msu');
					//alert("2nd IF In - 03");
					var answerIdFrm = innerQuestion.getElementById('answerId_msu');
					//alert("2nd IF In - 04");
					questionscore=inputQuestionFrm.value;
					//alert("2nd IF In - 05 questionscore "+questionscore);
					answerId=answerIdFrm.value;
					//alert("2nd IF In - 06 answerId "+answerId);
					questionSliderScoreSum=(parseInt(questionSliderScoreSum)+parseInt(questionscore));
					//alert("questionscore "+questionscore +" answerId "+answerId +" questionSliderScoreSum "+questionSliderScoreSum);
					answerId_arrary[i-1]=answerId;
					answerScore_arrary[i-1]=questionscore;
					
				}
			}catch(e){}
			
			if(questionscore==0)
			{
				no_Of_Question_Slider_Value++;
			}

		}
	}
	
	// End for save Question score
	var cnt=0
	$('#errorStatusNote_msu').empty();
	$('#errorStatusNote_msu').show();
	
	//alert("flag "+flag+" topSliderDisable "+topSliderDisable +" questionScoreSliderDisable "+questionScoreSliderDisable +" Text "+$('#statusNotes_msu').find(".jqte_editor").text().trim());
	//alert("flag "+ flag +" countQuestionSlider "+countQuestionSlider+"  no_Of_Question_Slider_Value "+no_Of_Question_Slider_Value+" questionSliderScoreSum "+questionSliderScoreSum +" topSliderDisable "+topSliderDisable +" questionScoreSliderDisable "+questionScoreSliderDisable);
	
	if ($('#statusNotes_msu').find(".jqte_editor").text().trim()=="" && topSliderDisable==1 && questionScoreSliderDisable==0 && topSliderValue==0)
	{
		$('#errorStatusNote_msu').append("&#149; "+resourceJSON.PlzEtrNoteOrSetScore+".<br>");
		cnt++;
	}
	else if ($('#statusNotes_msu').find(".jqte_editor").text().trim()=="" && topSliderDisable==0 && questionScoreSliderDisable==1 && countQuestionSlider > 0)
	{
		if(flag==0 && questionSliderScoreSum == 0)
		{
			$('#errorStatusNote_msu').append("&#149; "+resourceJSON.MsgSetScoreOrEnterNoteForAttribute+".<br>");
			cnt++;
		}
		else if(flag==1)
		{
			//alert("no_Of_Question_Slider_Value "+no_Of_Question_Slider_Value +" countQuestionSlider "+countQuestionSlider)
			if(countQuestionSlider >= no_Of_Question_Slider_Value && no_Of_Question_Slider_Value > 0)
			{
				$('#errorStatusNote_msu').append("&#149; "+resourceJSON.PlzSetScoreForAllAttribute+".<br>");
				cnt++;
			}
		}
	}
	else if(flag==1 && countQuestionSlider >= no_Of_Question_Slider_Value && no_Of_Question_Slider_Value > 0 && questionSliderScoreSum > 0)
	{
		$('#errorStatusNote_msu').append("&#149; "+resourceJSON.SetScoreForAllAttributeToFinalize+".<br>");
		cnt++;
	}
	else if ($('#statusNotes_msu').find(".jqte_editor").text().trim()=="" && topSliderDisable==0 && questionScoreSliderDisable==0)
	{
		$('#errorStatusNote_msu').append("&#149; "+resourceJSON.PlzEtrNotes+".<br>");
		cnt++;
	}
	else if ($('#statusNotes_msu').find(".jqte_editor").text().trim()=="" && topSliderDisable==0 && questionScoreSliderDisable==1)
	{
		$('#errorStatusNote_msu').append("&#149; "+resourceJSON.PlzEtrNotes+".<br>");
		cnt++;
	}
	else if($('#statusNotes_msu').find(".jqte_editor").text().trim()!="")
	{
		var charCount=$('#statusNotes_msu').find(".jqte_editor").text().trim();
		var count = charCount.length;
		if(count > 4000)
		{
			$('#errorStatusNote_msu').show();
			$('#errorStatusNote_msu').append("&#149; "+resourceJSON.MsgLengthnotExceed+".<br>");
			cnt++;
		}
	}
	
	var statusNoteFileName_msu=null;
	try{  
		statusNoteFileName_msu=document.getElementById("statusNoteFileName_msu").value;
	}catch(e){}
	
	if(statusNoteFileName_msu=="" && cnt==0)
	{
		$('#errorStatusNote_msu').show();
		$('#errorStatusNote_msu').append("&#149; "+resourceJSON.PlzUpldNotes+".<br>");
		cnt++;
	}
	else if(statusNoteFileName_msu!="" && statusNoteFileName_msu!=null)
	{
		var ext = statusNoteFileName_msu.substr(statusNoteFileName_msu.lastIndexOf('.') + 1).toLowerCase();	
		var fileSize = 0;
		if ($.browser.msie==true)
	 	{	
		    fileSize = 0;	   
		}
		else
		{
			if(document.getElementById("statusNoteFileName_msu").files[0]!=undefined)
			{
				fileSize = document.getElementById("statusNoteFileName_msu").files[0].size;
			}
		}
		if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt'|| ext=='xlsx' || ext=='xls'))
		{
			$('#errorStatusNote_msu').show();
			$('#errorStatusNote_msu').append("&#149; "+resourceJSON.PlzSlctAcceptableNoteFormats+".<br>");
			cnt++;
		}
		else if(fileSize>=10485760)
		{
			$('#errorStatusNote_msu').show();
			$('#errorStatusNote_msu').append("&#149; "+resourceJSON.msgfilesizelessthan+"<br>");
			cnt++;
		}
	}
	
		// Start Data Save process
	
	
	var teacherId_msu =document.getElementById("teacherId_msu").value;
	var jobIds_msu =document.getElementById("jobIds_msu").value;
	var statusId_msu =document.getElementById("statusId_msu").value;
	var secondaryStatusId_msu =document.getElementById("secondaryStatusId_msu").value;
	var dclnORrem_msu =document.getElementById("dclnORrem_msu").value;
	var isOverrideForAllJob_msu =document.getElementById("isOverrideForAllJob_msu").value;
	
	var statusNotes_msu = $('#statusNotes_msu').find(".jqte_editor").html();
	
	var bEmailToDASA_msu=document.getElementById('emailToDASA_msu').checked;
	var bEmailToCA_msu=document.getElementById('emailToCA_msu').checked;
	
	var iQuestionSliderJobId_msu =document.getElementById("iQuestionSliderJobId_msu").value;
	var iMaxFitScore_msu =document.getElementById("iMaxFitScore_msu").value;
	
	
	if(cnt==0)
	{	
		try{
			
			//alert("flag "+flag+" teacherId "+teacherId_msu +" jobIds "+jobIds_msu +" statusId "+statusId_msu +" secondaryStatusId "+secondaryStatusId_msu +" dclnORrem_msu "+dclnORrem_msu +" isOverrideForAllJob "+isOverrideForAllJob_msu +" topSliderValue "+topSliderValue+" questionSliderScoreSum "+questionSliderScoreSum +" iCountScoreSlider "+iCountScoreSlider +" statusNotes Text "+statusNotes_msu +" bEmailToDASA_msu "+bEmailToDASA_msu +" bEmailToCA_msu "+bEmailToCA_msu);
			
			//$('#loadingDiv').show();
			if(statusNoteFileName_msu!="" && statusNoteFileName_msu!=null)
			{
				//alert("statusNoteFileName_msu "+statusNoteFileName_msu);
				document.getElementById("frmStatusNote_msu").submit();
			}
			else
			{
				
				
				MassStatusUpdateService.saveStatusNotesAndSliderInfo_msu(flag,isOverrideForAllJob_msu,teacherId_msu,jobIds_msu,statusId_msu,secondaryStatusId_msu,dclnORrem_msu,statusNotes_msu,statusNoteFileName_msu,topSliderValue,answerId_arrary,answerScore_arrary,bEmailToDASA_msu,bEmailToCA_msu,iQuestionSliderJobId_msu,iMaxFitScore_msu,
				{
					async:false,
					errorHandler:handleError,
					callback:function(data)
					{	
					
					/*if(data[1]!=null && data[1]!="")
					{
						$('#errorStatusNote_msu').show();
						$('#errorStatusNote_msu').append(data[1]);
						
					}
					else */
					
					if(data[0]!=null && data[0]=="0")
					{
						$('#myModalStatusDetailsInfo_msu').modal('hide');
						//$('#myModalStatus_msu').modal('show');
						
						showSuccessMessage_msu();
						
						
						
					}
					
						/*document.getElementById("scoreProvided").value="";
						if(data.split("||||")[0]==3){
							getCandidateGrid();
							refreshStatus();
							$('#loadingDiv').hide();
							jWTeacherStatusNotesOnClose();
							//jWTeacherStatusNotesOnOpen(fitScore,statusId,secondaryStatusId);
							document.getElementById("noteMainDiv").style.display="none";
							$('#statusNotes').find(".jqte_editor").html("");	
							document.getElementById("teacherStatusNoteId").value=0;
							document.getElementById("showStatusNoteFile").innerHTML="";
						}else{
							$('#loadingDiv').hide();
							document.getElementById("noteMainDiv").style.display="none";
							$('#statusNotes').find(".jqte_editor").html("");	
							document.getElementById("teacherStatusNoteId").value=0;
							document.getElementById("showStatusNoteFile").innerHTML="";
							if(data.split("||||")[0]==2){
								jWTeacherStatusNotesOnOpen(fitScore,statusId,secondaryStatusId);
								$('#errorStatusNote').show();
								$('#errorStatusNote').append("&#149; We cannot hire new Candidate as we have already hired Candidate(s) equal to number of Expected hire(s) for this job.");
							}else if(data.split("||||")[0]==4){
								jWTeacherStatusNotesOnOpen(fitScore,statusId,secondaryStatusId);
								$('#errorStatusNote').show();
								$('#errorStatusNote').append("&#149; You can not UnHire the Candidate.");
							}else if(data.split("||||")[0]==5){
								jWTeacherStatusNotesOnOpen(fitScore,statusId,secondaryStatusId);
								$('#errorStatusNote').show();
								$('#errorStatusNote').append("&#149; Please finalize "+data.split("||||")[2]+" first to complete this step");
							}else if(data.split("||||")[1]!='null'){
								jWTeacherStatusNotesOnOpen(fitScore,statusId,secondaryStatusId);
								$('#errorStatusNote').show();
								$('#errorStatusNote').append("&#149; "+data.split("||||")[1]+".<br>");
							}else{
								jWTeacherStatusNotesOnClose();
							}
						}*/
					}
				});	
			}
		}catch(err){}
	}	
		
		
	
}

function saveStatusNoteOrSliderScoreWithFileName(statusNoteFileName_msu)
{
	//alert("Calling saveStatusNoteOrSliderScoreWithFileName "+statusNoteFileName_msu);
	
	var flag=document.getElementById("flag_msu").value;
	
	var noteDiv_msu=0;
	try{
		if(document.getElementById("noteMainDiv_msu").style.display=="inline")
		{
			noteDiv_msu=1;
		}
	}catch(err){}
	
	var topSliderDisable=0;
	try{
		topSliderDisable=document.getElementById("topSliderDisable").value;
	}catch(err){}
	
	var questionScoreSliderDisable=0;
	try{
		questionScoreSliderDisable=document.getElementById("questionScoreSliderDisable").value;
	}catch(err){}
	
	var countQuestionSlider=0;
	try{
		countQuestionSlider=document.getElementById("countQuestionSlider").value;
	}catch(err){}
	
	
	// Start for save Question score
	var topSliderValue=0;
	try{
		var iframeNorm = document.getElementById('ifrmTopSlider_msu');
		var innerNorm = iframeNorm.contentDocument || iframeNorm.contentWindow.document;
		var inputNormScore = innerNorm.getElementById('topSlider_msu');
		topSliderValue=inputNormScore.value;
	}catch(e){}	
	
	var answerId_arrary = new Array();
	var answerScore_arrary = new Array();
	
	var no_Of_Question_Slider_Value=0;
	var questionSliderScoreSum=0;
	var iCountScoreSlider=document.getElementById("countQuestionSlider").value;
	if(iCountScoreSlider > 0)
	{
		for(var i=1;i<= iCountScoreSlider;i++)
		{
			var questionscore=0;
			var answerId=0;
			
			try{
				var ifrmQuestion = document.getElementById('ifrmQuestion_msu_'+i);
				if(ifrmQuestion!=null)
				{
					var innerQuestion = ifrmQuestion.contentDocument || ifrmQuestion.contentWindow.document;
					var inputQuestionFrm = innerQuestion.getElementById('questionFrm_msu');
					var answerIdFrm = innerQuestion.getElementById('answerId_msu');
					questionscore=inputQuestionFrm.value;
					answerId=answerIdFrm.value;
					questionSliderScoreSum=(parseInt(questionSliderScoreSum)+parseInt(questionscore));
					answerId_arrary[i-1]=answerId;
					answerScore_arrary[i-1]=questionscore;
				}
			}catch(e){}
			
			if(questionscore==0)
			{
				no_Of_Question_Slider_Value++;
			}

		}
	}
	
	// End for save Question score
	
	var cnt=0
	$('#errorStatusNote_msu').empty();
	$('#errorStatusNote_msu').show();
	
	var teacherId_msu =document.getElementById("teacherId_msu").value;
	var jobIds_msu =document.getElementById("jobIds_msu").value;
	var statusId_msu =document.getElementById("statusId_msu").value;
	var secondaryStatusId_msu =document.getElementById("secondaryStatusId_msu").value;
	var dclnORrem_msu =document.getElementById("dclnORrem_msu").value;
	var isOverrideForAllJob_msu =document.getElementById("isOverrideForAllJob_msu").value;
	var statusNotes_msu = $('#statusNotes_msu').find(".jqte_editor").html();
	var bEmailToDASA_msu=document.getElementById('emailToDASA_msu').checked;
	var bEmailToCA_msu=document.getElementById('emailToCA_msu').checked;
	var iQuestionSliderJobId_msu =document.getElementById("iQuestionSliderJobId_msu").value;
	var iMaxFitScore_msu =document.getElementById("iMaxFitScore_msu").value;
	
	if(cnt==0)
	{	
		try
		{
			MassStatusUpdateService.saveStatusNotesAndSliderInfo_msu(flag,isOverrideForAllJob_msu,teacherId_msu,jobIds_msu,statusId_msu,secondaryStatusId_msu,dclnORrem_msu,statusNotes_msu,statusNoteFileName_msu,topSliderValue,answerId_arrary,answerScore_arrary,bEmailToDASA_msu,bEmailToCA_msu,iQuestionSliderJobId_msu,iMaxFitScore_msu,
			{
				async:false,
				errorHandler:handleError,
				callback:function(data)
				{	
					if(data[0]!=null && data[0]=="0")
					{
						$('#myModalStatusDetailsInfo_msu').modal('hide');
						//$('#myModalStatus_msu').modal('show');
						showSuccessMessage_msu();
					}
				}
			});	

		}
		catch(err){}
	}	
	
}

function getStatusWiseEmailForAdmin_msu()
{
	
	/*try{  
		$('#myModalEmailTeacher_msu').modal('hide');
	}catch(e){}*/
	
	var sStatusName_msu	=	$("#sStatusName_msu").val();
	var sStatusShortName_msu =	$("#sStatusShortName_msu").val();
	var jobIds_msu =	$("#jobIds_msu").val();
	var teacherId_msu =document.getElementById("teacherId_msu").value;
	//alert("getStatusWiseEmailForAdmin_msu sStatusName "+sStatusName_msu +" sStatusShortName_msu "+sStatusShortName_msu +" jobIds_msu "+jobIds_msu +" teacherId_msu "+teacherId_msu);
	
	MassStatusUpdateService.getStatusWiseEmailForAdmin_msu(sStatusName_msu,sStatusShortName_msu,jobIds_msu,teacherId_msu,
	{
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
			if(data!=null)
			{
				//alert("data If");
				$('#subjectLine_msu').val(data.subjectLine);
				$('#mailBody_msu').find(".jqte_editor").html(data.templateBody);
				try{  
					$('#myModalStatusDetailsInfo_msu').modal('hide');
					document.getElementById("noteMainDiv_msu").style.display="none";
				}catch(e){}
				
				try{  
					$('#myModalEmail_msu').modal('show');
					$("#mailBody_msu").find(".jqte_editor").attr("contenteditable","false");
				}catch(e){}
			}
			else
			{
				//alert("data else");
				document.getElementById("emailToDASA_msu").disabled=true;
			}
		}
	});
}

/*function getStatusWiseEmailForTeacher_msu()
{

	try{  
		$('#myModalEmail_msu').modal('hide');
	}catch(e){}
	
	var sStatusName_msu	=	$("#sStatusName_msu").val();
	var sStatusShortName_msu =	$("#sStatusShortName_msu").val();
	var jobIds_msu =	$("#jobIds_msu").val();
	var teacherId_msu =document.getElementById("teacherId_msu").value;
	alert("getStatusWiseEmailForTeacher_msu sStatusName "+sStatusName_msu +" sStatusShortName_msu "+sStatusShortName_msu +" jobIds_msu "+jobIds_msu +" teacherId_msu "+teacherId_msu);
	
	MassStatusUpdateService.getStatusWiseEmailForTeacher_msu(statusName,statusShortName,jobIds_msu,teacherId_msu,
	{
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
			if(data!=null)
			{
				$('#subjectLineTeacher').val(data.subjectLine);
				$('#mailBodyTeacher').find(".jqte_editor").html(data.templateBody);
				try{  
					$('#myModalStatusDetailsInfo_msu').modal('hide');
					document.getElementById("noteMainDiv_msu").style.display="none";
				}catch(e){}
				try{  
					$('#myModalEmailTeacher_msu').modal('show');
				}catch(e){}
				
			}
		}
	});
}
*/

function sendOriginalEmail_msu()
{
	$('#isEmailTemplateChanged').val(0);
	emailClose_msu();
}

/*function sendOriginalEmailTeacher_msu()
{
	$('#isEmailTemplateChangedTeacher').val(0);
	emailClose_msu();
}*/

function emailClose_msu(){
	try{
		$('#myModalEmail_msu').modal('hide'); 
		//$('#myModalEmailTeacher_msu').modal('hide'); 
		$('#myModalStatusDetailsInfo_msu').modal('show');	
	}
	catch(err){}
	//displayStatusDashboard();
}

function sendChangedEmail_msu()
{
	$('#isEmailTemplateChanged').val(1);
	var msgSubject=$('#subjectLine_msu').val();
	var charCount	=	$('#mailBody_msu').find(".jqte_editor").text();
			
	$('#errordivEmail_msu').empty();
	$('#subjectLine_msu').css("background-color", "");
	$('#mailBody_msu').find(".jqte_editor").css("background-color", "");
	
	var cnt=0;
	var focs=0;
	if(trim(msgSubject)=="")
	{
		$('#errordivEmail_msu').append("&#149; "+resourceJSON.PlzEtrSub+"<br>");
		if(focs==0)
			$('#subjectLine_msu').focus();
		$('#subjectLine_msu').css("background-color", "#F5E7E1");
		cnt++;focs++;
	}
	if(charCount.length==0)
	{
		$('#errordivEmail_msu').append("&#149; "+resourceJSON.PlzEtrMsg+"<br>");
		if(focs==0)
			$('#mailBody_msu').find(".jqte_editor").focus();
		$('#mailBody_msu').find(".jqte_editor").css("background-color", "#F5E7E1");
		cnt++;focs++;
	}
	
	if(cnt==0)
	{
		emailClose_msu();
		return false;
	
	}
	else
	{
		$('#errordivEmail_msu').show();
		return false;
	}
}

/*function sendChangedEmailTeacher_msu()
{
	
	$('#isEmailTemplateChangedTeacher').val(1);
	var msgSubjectTeacher=$('#subjectLineTeacher').val();
	var charCount	=	$('#mailBodyTeacher').find(".jqte_editor").text();
			
	$('#errordivEmailTeacher').empty();
	$('#subjectLineTeacher').css("background-color", "");
	$('#mailBodyTeacher').find(".jqte_editor").css("background-color", "");
	
	var cnt=0;
	var focs=0;
	if(trim(msgSubjectTeacher)=="")
	{
		$('#errordivEmailTeacher').append("&#149; Please enter Subject<br>");
		if(focs==0)
			$('#subjectLineTeacher').focus();
		$('#subjectLineTeacher').css("background-color", "#F5E7E1");
		cnt++;focs++;
	}
	if(charCount.length==0)
	{
		$('#errordivEmailTeacher').append("&#149; Please enter Message<br>");
		if(focs==0)
			$('#mailBodyTeacherTeacher').find(".jqte_editor").focus();
		$('#mailBodyTeacher').find(".jqte_editor").css("background-color", "#F5E7E1");
		cnt++;focs++;
	}
	
	if(cnt==0)
	{
		emailClose();
		return false;
	
	}
	else
	{
		$('#errordivEmailTeacher').show();
		return false;
	}
}*/


function updateStatusHDR_msu(flagValue)
{
	document.getElementById("updateStatusHDRFlag_msu").value=flagValue;
	
	document.getElementById("flag_msu").value=flagValue;
	
	var noteDiv_msu=0;
	try{
		if(document.getElementById("noteMainDiv_msu").style.display=="inline")
		{
			noteDiv_msu=1;
		}
		//alert(" noteDiv_msu open :: "+noteDiv_msu);
	}catch(err){}
	
	var topSliderDisable=0;
	try{
		topSliderDisable=document.getElementById("topSliderDisable").value;
		//alert(" topSliderDisable "+topSliderDisable);
	}catch(err){}
	
	// Start for save Question score
	var topSliderValue=0;
	try{
		var iframeNorm = document.getElementById('ifrmTopSlider_msu');
		var innerNorm = iframeNorm.contentDocument || iframeNorm.contentWindow.document;
		var inputNormScore = innerNorm.getElementById('topSlider_msu');
		topSliderValue=inputNormScore.value;
	}catch(e){}	
	
	// End for save Question score
	var cnt=0
	$('#errorStatusNote_msu').empty();
	$('#errorStatusNote_msu').show();
	
	
	//alert("flag "+ flag +" countQuestionSlider "+countQuestionSlider+"  no_Of_Question_Slider_Value "+no_Of_Question_Slider_Value+" questionSliderScoreSum "+questionSliderScoreSum +" topSliderDisable "+topSliderDisable +" questionScoreSliderDisable "+questionScoreSliderDisable);
	
	if ($('#statusNotes_msu').find(".jqte_editor").text().trim()=="" && topSliderDisable==1 && topSliderValue==0)
	{
		$('#errorStatusNote_msu').append("&#149; "+resourceJSON.PlzEtrNoteOrSetScore+".<br>");
		cnt++;
	}
	else if ($('#statusNotes_msu').find(".jqte_editor").text().trim()=="" && topSliderDisable==0)
	{
		$('#errorStatusNote_msu').append("&#149; "+resourceJSON.PlzEtrNotes+".<br>");
		cnt++;
	}
	else if($('#statusNotes_msu').find(".jqte_editor").text().trim()!="")
	{
		var charCount=$('#statusNotes_msu').find(".jqte_editor").text().trim();
		var count = charCount.length;
		if(count > 4000)
		{
			$('#errorStatusNote_msu').show();
			$('#errorStatusNote_msu').append("&#149; "+resourceJSON.MsgLengthnotExceed+".<br>");
			cnt++;
		}
	}
	
	var statusNoteFileName_msu=null;
	try{  
		statusNoteFileName_msu=document.getElementById("statusNoteFileName_msu").value;
	}catch(e){}
	
	if(statusNoteFileName_msu=="" && cnt==0)
	{
		$('#errorStatusNote_msu').show();
		$('#errorStatusNote_msu').append("&#149; "+resourceJSON.PlzUpldNotes+".<br>");
		cnt++;
	}
	else if(statusNoteFileName_msu!="" && statusNoteFileName_msu!=null)
	{
		var ext = statusNoteFileName_msu.substr(statusNoteFileName_msu.lastIndexOf('.') + 1).toLowerCase();	
		var fileSize = 0;
		if ($.browser.msie==true)
	 	{	
		    fileSize = 0;	   
		}
		else
		{
			if(document.getElementById("statusNoteFileName_msu").files[0]!=undefined)
			{
				fileSize = document.getElementById("statusNoteFileName_msu").files[0].size;
			}
		}
		if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt'|| ext=='xlsx' || ext=='xls'))
		{
			$('#errorStatusNote_msu').show();
			$('#errorStatusNote_msu').append("&#149; "+resourceJSON.PlzSlctAcceptableNoteFormats+".<br>");
			cnt++;
		}
		else if(fileSize>=10485760)
		{
			$('#errorStatusNote_msu').show();
			$('#errorStatusNote_msu').append("&#149; "+resourceJSON.msgfilesizelessthan+".<br>");
			cnt++;
		}
	}
	
	if(cnt==0)
	{
		$('#myModalStatusDetailsInfo_msu').modal('hide');
		/*if(flagValue==2)
		{
			document.getElementById("updateStatusNoteMsg_msu").innerHTML="Are you sure, you would like to UnHire the Candidate?";
		}
		else */
		if(flagValue==3)
		{
			document.getElementById("updateStatusNoteMsg_msu").innerHTML=""+resourceJSON.MsgUndeclinedTheCandidate+"";
		}
		else if(flagValue==4)
		{
			document.getElementById("updateStatusNoteMsg_msu").innerHTML=""+resourceJSON.MsgUnRejectTheCandidate+"";
		}
		$('#modalUpdateStatusNoteMsg_msu').modal('show');
	}
}

function closeStatusNoteMsg_msu(flag)
{
	$('#modalUpdateStatusNoteMsg_msu').modal('hide');
	$('#myModalStatusDetailsInfo_msu').modal('show');
}

function closeUpdateStatusNoteMsg_msu(flag)
{
	$('#modalUpdateStatusNoteMsg_msu').modal('hide');
	if(flag==1)
	{
		var updateStatusHDRFlag_msu=document.getElementById("updateStatusHDRFlag_msu").value;
		//alert("Ready to Call method saveStatusNoteOrSliderScore ...");
		saveStatusNoteOrSliderScore(updateStatusHDRFlag_msu);
	}
	else
	{
		$('#myModalStatusDetailsInfo_msu').modal('show');
	}
}


function showSuccessMessage_msu()
{
	var teacherFullName="";
	try
	{
		teacherFullName=document.getElementById("teacherFullName").value;
	}
	catch(err)
	{}
	
	var updateStatusName_msu=document.getElementById("updateStatusName_msu").value;
	
	$('#myModalStatusSuccessMessage_msu').modal('show');
	var completeMsg="";
	completeMsg="You have successfully updated the status";
	completeMsg=completeMsg+" for ";
	completeMsg=completeMsg+teacherFullName+" ";
	completeMsg=completeMsg+"to ";
	completeMsg=completeMsg+updateStatusName_msu;
	completeMsg=completeMsg+" status.";
	
	document.getElementById("statusSuccessMessageText_msu").innerHTML=completeMsg;
	
	var headerTextForStatus=updateStatusName_msu+" Evaluation for "+teacherFullName;
	document.getElementById("myModalStatusSuccessMessageTitle_msu").innerHTML=headerTextForStatus;
	
	
	
}

function closeSuccessUpdate_msu()
{
	//$('#myModalStatus_msu').modal('show');
	var teacherId_msu =document.getElementById("teacherId_msu").value;
	//alert("teacherId_msu "+teacherId_msu);
	getJobOrderList_msu(teacherId_msu);
}


function applyScrollOnStatusNote_msu()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblStatusNote_msu').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 150,
        width: 655,
        minWidth: null,
        minWidthAuto: false,
        colratio:[380,76,100,100], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });            
        });
}