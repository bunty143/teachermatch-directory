/* @Author: Gagan 
 * @Discription: view of edit domain js.
*/
var page = 1;
var noOfRows = 50;
var sortOrderStr="";
var sortOrderType="";
function getPaging(pageno)
{
	if(pageno!='')
	{
		page=pageno;	
	}
	else
	{
		page=1;
	}
	
	noOfRows = document.getElementById("pageSize").value;
	ShowDistrict();
}
function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
	if(pageno!=''){
		page=pageno;	
	}else{
		page=1;
	}
	sortOrderStr=sortOrder;
	sortOrderType=sortOrderTyp;
	if(document.getElementById("pageSize")!=null){
		noOfRows = document.getElementById("pageSize").value;
	}else{
		noOfRows=10;
	}
	ShowDistrict();
}
/*========  For Handling session Time out Error ===============*/
function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert(resourceJSON.msgSomeerror+": "+exception.javaClassName);}
}
/*======= Show District on Press Enter Key ========= */
function chkForEnterShowDistrict(evt)
{
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==13)
	{
		ShowDistrict();
	}	
}

function updateMsg(authKeyVal,entityType)
{
	if(authKeyVal!="0"){
		if(authKeyVal!="2" && entityType==1){
			document.getElementById("updateMsg").innerHTML=resourceJSON.MsgDetailUpdatedSuccessfully+" <br/><br/>  "+resourceJSON.msgAuthenticationKey+": "+authKeyVal;
		}else{
			document.getElementById("updateMsg").innerHTML=resourceJSON.MsgDetailUpdatedSuccessfully;
		}
		$('#myModalUpdateMsg').modal('show');
	}
}

//Search Records By EntityType

function DisplayHideSearchBox(){
	var entity_type	=	document.getElementById("MU_EntityType").value; 
	if(entity_type ==2)
	{
		$("#Searchbox").fadeIn();
		$('#SearchTextboxDiv').fadeIn();
			$("#districtClassMaster").show();
		$('#districtName').focus();
	}else{
		document.getElementById("districtName").value="";
		$("#Searchbox").fadeIn();
			$("#districtClassMaster").hide();
		$("#SearchTextboxDiv").hide();
	}
}

function onLoadDisplayHideSearchBox(entity_type){	
	if(entity_type ==2){
		$("#Searchbox").hide();
		$('#SearchTextboxDiv').hide();
	}else if(entity_type ==1){
		$("#Searchbox").fadeIn();
		$('#SearchTextboxDiv').fadeIn();
		if($("#showSessionStatus").val()==0 || $("#showSessionStatus").val()==undefined){
		document.getElementById("districtName").focus();
		document.getElementById("MU_EntityType").value=2;
		}
	}else{
		$("#Searchbox").fadeIn();
		$('#SearchTextboxDiv').hide();
	}
	ShowDistrict();
}

/*========  Show District ===============*/
function searchDistrict()
{
	page = 1;
	if($("#MU_EntityType").val()==1){
		$("#districtMasterName").val("");
	}
	ShowDistrict();
}
function ShowDistrict()
{
	$('#loadingDiv').fadeIn();
	var entityID	= document.getElementById("MU_EntityType").value;
	var searchText	="";
	try{
		if($("#districtName").val()!=""){
			searchText=document.getElementById("districtName").value;
		}else{
			if($("#districtMasterName").val()!=undefined)
				searchText=$("#districtMasterName").val();
		}
	}catch(err){
		searchText="";
	}
	delay(1000);
	DistrictAjax.SearchDistrictRecords(entityID,searchText,noOfRows,page,sortOrderStr,sortOrderType,{ 
		async: true,
		callback: function(data)
		{
			$('#divMain').html(data);
			applyScrollOnTbl();
			$('#loadingDiv').hide();
		},
		errorHandler:handleError 
	});
	hideSearchAgainMaster();
}
function delay(sec){
	var starttime = new Date().getTime();
	starttime = starttime+sec;
	while(true){
		if(starttime< new Date().getTime()){
			break;
		}
	}
}

//========  Rahul Tyagi: Changes  19/11/2014===============
function activateDeactivateDistrict(district,status)
{
	document.getElementById("actdist").value=district;
	document.getElementById("diststat").value=status;
	if(status=="I")
	{
		$('#myModalactMsgShow').modal('show');
	}
	
	else
	{
	toggleStatus();	
	}
}

function toggleStatus()
{
	var district=document.getElementById("actdist").value;
	var status=document.getElementById("diststat").value;
	DistrictAjax.activateDeactivateDistrict(district,status, { 
		async: true,
		callback: function(data)
		{
		
			
		
			ShowDistrict();
		},
		errorHandler:handleError });	
}




//===========================================================


var count=0;
var index=-1;
var length=0;
var divid='';
var txtid='';
var hiddenDataArray = new Array();
var showDataArray = new Array();
var degreeTypeArray = new Array();
var hiddenId="";
function getDistrictMasterAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("districtName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getDistrictMasterArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getDistrictMasterArray(districtName){
	var searchArray = new Array();
	DistrictAjax.getFieldOfDistrictList(districtName,{ 
		async: false,
		callback: function(data){
		hiddenDataArray = new Array();
		showDataArray = new Array();
		for(i=0;i<data.length;i++){
			searchArray[i]=data[i].districtName;
			showDataArray[i]=data[i].districtName;
			hiddenDataArray[i]=data[i].districtId;
		}
	},
		errorHandler:handleError 
	});	

	return searchArray;
}


var selectFirst="";

var fatchData= function(txtSearch,searchArray,txtId,txtdivid){
	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
				
			}
			
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}
function hideDistrictMasterDiv(dis,hiddenId,divId)
{
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}

var downArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index<length-1){
			for(var i=0;i<10;i++){
				{
					if(document.getElementById('divResult'+txtdivid+i))
						var div_id=document.getElementById('divResult'+txtdivid+i);
					if(div_id)
					{
						if(div_id.className=='over')
							index=div_id.id.split('divResult'+txtdivid)[1];
						div_id.className='normal';
					}
				}
			}
			index++;
			if(document.getElementById('divResult'+txtdivid+index))
			{
				var div_id=document.getElementById('divResult'+txtdivid+index);
				div_id.className='over';
				document.getElementById(txtid).value = div_id.innerHTML;
				selectFirst=div_id.innerHTML;
			}
		}
	}
}

var upArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index>0){
			for(var i=0;i<length;i++){

				var div_id=document.getElementById('divResult'+txtdivid+i);
				if(div_id)
				{
					if(div_id.className=='over')
						index=div_id.id.split('divResult'+txtdivid)[1];
					div_id.className='normal';
				}
			}
			index--;
			if(document.getElementById('divResult'+txtdivid+index))
				document.getElementById('divResult'+txtdivid+index).className='over';
			if(txtid && document.getElementById('divResult'+txtdivid+index))
			{
				document.getElementById(txtid).value=
					document.getElementById('divResult'+txtdivid+index).innerHTML;
				selectFirst=document.getElementById('divResult'+txtdivid+index).innerHTML;
			}
		}
	}
}

var overText=function (div_value,txtdivid) {

	for(var i=0;i<length;i++)
	{
		if(document.getElementById('divResult'+i))
		{
			var div_id=document.getElementById('divResult'+i);

			if(div_id.className=='over')
				index=div_id.id.split(txtdivid)[1];
			div_id.className='normal';
		}
	}
	div_value.className = 'over';
	document.getElementById(txtid).value= div_value.innerHTML;
}
function trim(s)
{
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}
function mouseOverChk(txtdivid,txtboxId)
{	
	for(var i=0;i<length;i++)
	{
		$('#divResult'+txtdivid+i).mouseover({param1: i,param2:txtdivid, param3:txtboxId}, fireMouseOverEvent);
	}
}


function fireMouseOverEvent(event)
{
	for(var i=0;i<length;i++)
	{	
		document.getElementById('divResult'+event.data.param2+i).className='normal';
	}
    document.getElementById('divResult'+event.data.param2+event.data.param1).className='over';	       
   	document.getElementById(event.data.param3).value= $('#divResult'+event.data.param2+event.data.param1).text();
    index=event.data.param1;
}

function setEntityType()
	{
		var entityString="<label>Entity Type</label><select class='form-control' id='MU_EntityType' name='MU_EntityType' class='form-control' onchange=\"DisplayHideSearchBox();\"><option value='1' selected>TM</option><option value='2'>District</option></select>";
		var district="<label id='captionDistrictOrSchool'>District</label>"+
			           "<input type='text' id='districtName' name='districtName'  class='form-control'"+
			            "onfocus=\"getDistrictMasterAutoComp(this, event, 'divTxtShowData', 'districtName','districtId','');\""+
						"onkeyup=\"getDistrictMasterAutoComp(this, event, 'divTxtShowData', 'districtName','districtId','');\""+
						"onblur=\"hideDistrictMasterDiv(this,'districtId','divTxtShowData');\"/>"+
						"<div id='divTxtShowData' style=' display:none;position:absolute;z-index:5000;' onmouseover=\"mouseOverChk('divTxtShowData','districtName');\" class='result' ></div>";
		var btnRecord="<c:if test=\"${fn:indexOf(roleAccess,'|5|')!=-1}\"><button class='btn btn-primary' type='button' onclick='searchDistrict();' style='width:70%;'>Search<i class='icon'></i></button></c:if>";
		globalSearchSection(entityString,"","",district,btnRecord);
		setTimeout(function(){ 
  		if($("#districtMasterId").val()!=0){
  			$("select#MU_EntityType").prop('selectedIndex', 1);
  			DisplayHideSearchBox();
  			$("#districtName").val($("#districtMasterName").val());
  			hideSearchAgainMaster();
  		}else{
  			showSearchAgainMaster();
  		}
		}, 100);
	}


//FutureDistrictByDistrictName
function getPaging(pageno)
{
	if(pageno!='')
	{
		page=pageno;	
	}
	else
	{
		page=1;
	}
	
	noOfRows = document.getElementById("pageSize").value;
	showDistrictsByDistrictName();
}
function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
	if(pageno!=''){
		page=pageno;	
	}else{
		page=1;
	}
	sortOrderStr=sortOrder;
	sortOrderType=sortOrderTyp;
	if(document.getElementById("pageSize")!=null){
		noOfRows = document.getElementById("pageSize").value;
	}else{
		noOfRows=10;
	}
	showDistrictsByDistrictName();
}

function onLoadDisplayFutureDistrict(entity_type){	
	if(entity_type ==2){
		$("#Searchbox").hide();
		$('#SearchTextboxDiv').hide();
	}else if(entity_type ==1){
		$("#Searchbox").fadeIn();
		$('#SearchTextboxDiv').fadeIn();
		if($("#showSessionStatus").val()==0 || $("#showSessionStatus").val()==undefined){
		document.getElementById("districtName").focus();
		document.getElementById("MU_EntityType").value=2;
		}
	}else{
		$("#Searchbox").fadeIn();
		$('#SearchTextboxDiv').hide();
	}
	showDistrictsByDistrictName();
}

function searchDistrictsByDistrictName()
{
	page = 1;
	if($("#MU_EntityType").val()==1){
		$("#districtMasterName").val("");
	}
	showDistrictsByDistrictName();
}
function showDistrictsByDistrictName()
{
	$('#loadingDiv').fadeIn();
	var entityID	= document.getElementById("MU_EntityType").value;
	var searchText	="";
	try{
		if($("#districtName").val()!=""){
			searchText=document.getElementById("districtName").value;
		}else{
			if($("#districtMasterName").val()!=undefined)
				searchText=$("#districtMasterName").val();
		}
	}catch(err){
		searchText="";
	}
	delay(1000);
	DistrictAjax.searchDistrictsByDistrictName(entityID,searchText,noOfRows,page,sortOrderStr,sortOrderType,{ 
		async: true,
		callback: function(data)
		{
			$('#divMain').html(data);
			applyScrollOnTbl();
			$('#loadingDiv').hide();
		},
		errorHandler:handleError 
	});
	hideSearchAgainMaster();
}

function activateNewDistrict(district)
{
	DistrictAjax.activateNewDistrict(district, { 
		async: true,
		callback: function(data)
		{
			showDistrictsByDistrictName();
		},
		errorHandler:handleError });	
}

function getFutureDistrictMasterAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("districtName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getFutureDistrictMasterArray(txtSearch.value);
		futureFatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getFutureDistrictMasterArray(districtName){
	var searchArray = new Array();
	DistrictAjax.getFutureDistrictListByDistrictName(districtName,{ 
		async: false,
		callback: function(data){
		hiddenDataArray = new Array();
		showDataArray = new Array();
		for(i=0;i<data.length;i++){
			searchArray[i]=data[i].districtName;
			showDataArray[i]=data[i].districtName;
			hiddenDataArray[i]=data[i].districtId;
		}
	},
		errorHandler:handleError 
	});	

	return searchArray;
}


var selectFirst="";

var futureFatchData= function(txtSearch,searchArray,txtId,txtdivid){
	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
				
			}
			
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}