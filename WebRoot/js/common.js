
/*=====================================demo schedule ==========================================*/

var multipleInterviewInviteId="";
function clearDemoScheduleField(){
	dwr.util.setValues({demoDate:null,demoTime:null,meridiem:null,location:null,disSchCmnt:null,timezone:null,userId:null,demostatus:null});
	$('#demoDate').css("background-color", "");
	$('#demoTime').css("background-color", "");
	$('#meridiem').css("background-color", "");
	$('#meridiem').css("background-color", "");
	$('#timezone').css("background-color", "");
	$('#location').css("background-color", "");
	$('#userId').css("background-color", "");

	$('#demostatus').attr('checked', false);
	$('#demostatus').attr("disabled", false);
	$('#demoDate').attr("disabled", false);
	$('#demoTime').attr("disabled", false);
	$('#meridiem').attr("disabled", false);
	$('#meridiem').attr("disabled", false);
	$('#timezone').attr("disabled", false);
	$('#location').attr("disabled", false);
	$('#userId').attr("disabled",false);
	$('#disSchCmnt').attr("disabled",false);
	$('#canEvtBtn').attr("disabled",false);
	$('#doneBtn').attr("disabled",false);
	$('#addAttHref').attr("disabled",false);

	if(document.getElementById("demoSchoolName"))
	{
		$('#demoSchoolName').val("");
		$('#schoolId').val("");
	}
}
function showAttendeeDiv()
{
	$("#demoDistrictdiv").show();
	$("#demoSchooldiv").show();
	$("#demoUserdiv").show();
}
var arr =[];
function getDemoSchedule(teacherId,jobId,isDemo)
{
	
	$("html, body").animate({ scrollTop: 0 }, "slow");
	clearDemoScheduleField();
	arr = [];
	$('#demoId').val("");
	$('#attendeeDiv').html("");
	var teacherDetail = {teacherId:teacherId};
	var jobOrder = {jobId:jobId};
	if(isDemo)
	{

		CandidateGridSubAjax.displayAttendee(teacherDetail,jobOrder,isDemo,{ 
			async: true,
			callback: function(data)
			{ 
			//alert(data.attendeeList);
			if(data.attendeeList!=undefined)
			{

				$('#demoDate').val(data.demoDate);
				$('#demoTime').val(data.demoTime);
				$('#attendeeDiv').html(data.attendees);
				$('#location').val(data.demoClassAddress);
				$('#disSchCmnt').val(data.demoDescription);
				$('#meridiem').val(data.timeFormat);
				$('#timezone').val(data.timeZoneId);
				$('#demoId').val(data.demoId);
				$('#demoNoteId').val(data.demoId);
				$('#demoCreatedDate').val(data.demoDate);
				
				arr = data.attendeeList;

				if(data.demoStatus=="Completed")
				{
					$('#demostatus').attr('checked', true);
					$('#demostatus').attr("disabled", "disabled");
					$('#demoDate').attr("disabled", "disabled");
					$('#demoTime').attr("disabled", "disabled");
					$('#meridiem').attr("disabled", "disabled");
					$('#meridiem').attr("disabled", "disabled");
					$('#timezone').attr("disabled", "disabled");
					$('#location').attr("disabled", "disabled");
					$('#userId').attr("disabled", "disabled");
					$('#disSchCmnt').attr("disabled", "disabled");
					$('#canEvtBtn').attr("disabled", "disabled");
					$('#doneBtn').attr("disabled", "disabled");
					$('#addAttHref').attr("disabled", "disabled");
				}

				if(data.demoNoteStatus==true){
					document.getElementById("democlassNote").innerHTML="<a href='javascript:void(0);' onclick='getNotesTimelineDivDiv(true,"+teacherId+","+jobId+");'><img src='images/note_com.png'/></a>"; 
				}else{
					document.getElementById("democlassNote").innerHTML="<a href='javascript:void(0);' onclick='getNotesTimelineDivDiv(true,"+teacherId+","+jobId+");'><img src='images/note_com1.png'/></a>";
				}
			}
			
			},
		});
	}
	
	if(document.getElementById("demoSchoolName"))
		getUsersByDistrict();

	document.getElementById("attendeeDiv").style.display='inline';

	$("#teacherId").val(teacherId);
	$("#jobId").val(jobId);

	$("#demoDistrictdiv").hide();
	$("#demoSchooldiv").hide();
	dwr.util.setValues({demoUserId:null});
	$('#errordiv').empty();
	
	try{$('#myModalDemoSchedule').modal('show');}catch(e){}
	

}
function getDemoNotesDiv(teacherId,jobId)
{
	removeNotesFile();
	hideCommunicationsDiv();
	currentPageFlag='notes';
	document.getElementById("teacherId").value=teacherId;
	document.getElementById("teacherIdForNote").value=teacherId;
	$('#demodivTxtNode').find(".jqte_editor").html("");		
	$('#demoModalDemoNotes').modal('show');	
	$('#divTxtNode').find(".jqte_editor").css("background-color", "");
	document.getElementById("noteId").value="";
	$('#errordivNotes').empty();
	$('#myModalDemoNotes').modal('hide');
	//CandidateGridAjax.getDemoNotesDetail(teacherId,jobId,page,noOfRows,sortOrderStr,sortOrderType,
	CandidateGridAjax.getDemoNotes(true,teacherId,jobId,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			document.getElementById("demoNotesTxt").innerHTML=data;
			applyScrollOnNotesTbl();
		}
	});
	
}

function getNotesTimelineDivDiv(commDivFlag,teacherId,jobId)
{	
	//document.getElementById("commDivFlag").value=commDivFlag;
	//document.getElementById("jobForTeacherGId").value=jobForTeacherGId;
	CandidateGridAjax.getDemoNotes(commDivFlag,teacherId,jobId,{ 
		async: true,
		errorHandler:handleError,
		callback: function(data){
		
		
		try{
			$("#demoNotesTxt").html(data);
			$('#myModalDemoSchedule').modal('hide');
			$('#myModalDemoNotes').modal('show');
		}catch(e){}
	}
	});	
}



/*======================save demo schedule data ==============================*/
function saveDemoSchedule(checkedFlag){

	$('#demoDate').css("background-color", "");
	$('#demoTime').css("background-color", "");
	$('#meridiem').css("background-color", "");
	$('#meridiem').css("background-color", "");
	$('#timezone').css("background-color", "");
	$('#location').css("background-color", "");
	$('#userId').css("background-color", "");

	//var demoDate 		=		new Date(trim(document.getElementById("demoDate").value));
	var demoDateVal 	=		trim(document.getElementById("demoDate").value);
	var demoTime		=		trim(document.getElementById("demoTime").value);
	var meridiem		=		trim(document.getElementById("meridiem").value);
	var location		=		trim(document.getElementById("location").value);
	var disSchCmnt		=		trim(document.getElementById("disSchCmnt").value);
	var t				=		document.getElementById("timezone");
	var timezone		=		trim(t.options[t.selectedIndex].value);
	var e				=		document.getElementById("demoUserId");
	var userId			=		trim(e.options[e.selectedIndex].value);
	var demoStatus		=		document.getElementById("demostatus");

	var teacherId		=		trim(document.getElementById("teacherId").value);
	var jobId		=		trim(document.getElementById("jobId").value);
	var demoId = $('#demoId').val();
	var counter=0;
	var focus=0;	

	$('#errordiv').empty();
	/*if (demoDate=="Invalid Date")
	{
		$('#errordiv').append("&#149; Please enter Demo Date<br>");
		if(focus==0)
			$('#demoDate').focus();
		$('#demoDate').css("background-color", "#F5E7E1");
		counter++;
	}*/
	if (demoDateVal=="" || demoDate=="Invalid Date")
	{
		$('#errordiv').append("&#149; "+resourceJSON.msgDemoDate+"<br>");
		if(focus==0)
			$('#demoDate').focus();
		$('#demoDate').css("background-color", "#F5E7E1");
		counter++;
	}

	if(demoDateVal!=""){

		try{
			var  date1 = demoDateVal.split("-");
			var currentDate = "";
			var dateMsg = "Current";
			if(demoId=="")
			  currentDate = document.getElementById("currDate").value;
			else
			{
				currentDate = document.getElementById("currDate").value;
			  //currentDate = document.getElementById("demoCreatedDate").value;
			  //dateMsg = "Demo Created";
			}
			
			var	date2 = currentDate.split("-");
			var sDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
			var eDate = new Date(date2[0]+"/"+date2[1]+"/"+date2[2]);
			if(sDate < eDate){
				$('#errordiv').append("&#149; "+resourceJSON.msgDemoDateGreater+" "+dateMsg+" "+resourceJSON.MsgDate+"<br>");
				if(focus==0)
					$('#demoDate').focus();
				$('#demoDate').css("background-color", "#F5E7E1");
				counter++;

			} 
		}catch(err){}
	}
	if (demoTime=="Select Time")
	{
		$('#errordiv').append("&#149; "+resourceJSON.msgDemoTime+"<br>");
		if(focus==0)
			$('#demoTime').focus();
		$('#demoTime').css("background-color", "#F5E7E1");
		counter++;
	}

	if (meridiem=="Select AM/PM")
	{
		$('#errordiv').append("&#149; "+resourceJSON.MsgSelectAmOrPm+"<br>");
		if(focus==0)
			$('#meridiem').focus();
		$('#meridiem').css("background-color", "#F5E7E1");
		counter++;
	}
	if (timezone=="Select Timezone")
	{
		$('#errordiv').append("&#149; "+resourceJSON.msgSelectTimeZone+"<br>");
		if(focus==0)
			$('#timezone').focus();
		$('#timezone').css("background-color", "#F5E7E1");
		counter++;
	}
	if (location=="")
	{
		$('#errordiv').append("&#149; "+resourceJSON.msgPlzenterLocation+"<br>");
		if(focus==0)
			$('#location').focus();
		$('#location').css("background-color", "#F5E7E1");
		counter++;
	}
	//if (userId=="Select Attendee" || arr.length==0)
	if (arr.length==0)
	{
		$('#errordiv').append("&#149; "+resourceJSON.PlzSlctAttendee+"<br>");
		if(focus==0)
			$('#userId').focus();
		$('#userId').css("background-color", "#F5E7E1");
		counter++;
	}

	if(demoStatus.checked && !checkedFlag)
	{
		$('#message2showConfirm').html(""+resourceJSON.msgwanttomarkthisdemo+"");
		$('#footerbtn').html("<button class='btn btn-primary' onclick='saveDemoSchedule(true)' >"+resourceJSON.btnOk+"</button> <button class='btn' data-dismiss='modal' aria-hidden='true'>"+resourceJSON.btnCancel+"</button>");
		$('#myModal3').modal('show');

		return;
	}

	if(counter==0){
		demoId=demoId==""?"0":demoId;

		CandidateGridSubAjax.saveDemoSchedule(demoDateVal,demoTime,meridiem,timezone,location,disSchCmnt,
				teacherId,jobId,demoStatus.checked,arr,demoId,{ 
			async: true,
			callback: function(data)
			{
			clearDemoScheduleField();
			arr = [];
			//document.getElementById("attendeeDiv").style.display='none';
			getCandidateGrid();
			refreshStatus();
			$('#myModalDemoSchedule').modal('hide');
			$('#myModal3').modal('hide');
			$('#message2show').html(""+resourceJSON.msgDemoLessonscheduled+".");
			$('#myModal2').modal('show');

			},
		});
	}
}

function addAttendee(){
	var e				=		document.getElementById("demoUserId");
	var userId			=		trim(e.options[e.selectedIndex].value);
	var userName		=		trim(e.options[e.selectedIndex].text);
	var attendeeDiv		=		document.getElementById("attendeeDiv");
	var strInnerHtml	=		trim("<div class=\"span3\">"+resourceJSON.msgNoAttendeeFound+"</div>");
	if(attendeeDiv.innerHTML==strInnerHtml){
		attendeeDiv.innerHTML="";
	}
	
	if(e.selectedIndex==0)
	{
		$('#errordiv').append("&#149; "+resourceJSON.msgvalidAttendee+"<br>");
		$('#userId').focus();
		return;
	}
	getAllAttendeeDivId();
	$('#errordiv').empty();
	if($.inArray(parseInt(userId), arr)==-1)
	{
		arr.push(parseInt(userId));
		var newAttendee=attendeeDiv.innerHTML +"<div id='"+userId+"' class='row col-sm-4 col-md-4'>"+userName
		+"&nbsp;<a href='javascript:void(0);' onclick='deleteAttendee(\""+userId+"\");'><img width='15' height='15' class='can' src='images/can-icon.png' alt=''></a></div>";

		$('#attendeeDiv').html(newAttendee);

	}else
	{	
		$('#errordiv').append("&#149; "+resourceJSON.msgAttendeealreadyadded+"<br>");
		$('#userId').focus();
		//$('#userId').css("background-color", "#F5E7E1");
	}
	$('#attendeeDiv').show();
}

/*========================================delete Attendee ==========================================*/
function deleteAttendee(attendeeId){

	$("#"+attendeeId).remove();
	var i = arr.indexOf(parseInt(attendeeId));

	if(i != -1) {
		arr.splice(i, 1);
	}
}
function getAllAttendeeDivId(){
	var all = $("#attendeeDiv > div").map(function() {
		//alert(this.id);
		return this.id;
	}).get();
}
/*========================================get user by school ==========================================*/
function getUsersBySchool(forWhich){

	var schoolId="schoolId";
	if(forWhich=="panel")
		 schoolId="panelSchoolId";
	
	if(document.getElementById("demoSchoolId").value!="")
	{
		var schoolId=trim(document.getElementById("demoSchoolId").value);
	
		CandidateGridSubAjax.getUsersBySchool(schoolId,false,{ 
			async: true,
			callback: function(data)
			{
			if(forWhich=="panel")
				$('#panelUserId').html(data);
			else
				$('#demoUserId').html(data);
			},
		});
	}else
		getUsersByDistrict(forWhich);

}

function getUsersByDistrict(forWhich){
	var districtId=0;
	CandidateGridSubAjax.getUsersByDistrict(districtId,false,{ 
		async: true,
		callback: function(data)
		{
			if(forWhich=="panel")
			$('#panelUserId').html(data);
			else
			$('#demoUserId').html(data);
		},
	});
}

function cancelEvent()
{
	if($('#demoId').val()=="")
	{
		$('#errordiv').append("&#149;"+resourceJSON.msgEventCancelled+"<br>");
	}else
	{
		$('#message2showConfirm').html(resourceJSON.msgWantToCancelled);
		var demoId = $('#demoId').val();
		$('#footerbtn').html("<button class='btn btn-primary' onclick=\"cancelEventOk("+demoId+")\" >"+resourceJSON.btnOk+"</button> <button class='btn' data-dismiss='modal' aria-hidden='true'>"+resourceJSON.btnCancel+"</button>");
		$('#myModal3').modal('show');
	}
}
function cancelEventOk(demoId)
{
	$('#myModal3').modal('hide');
	$('#myModalDemoSchedule').modal('hide');
	//$('#loadingDiv').modal('show');

	CandidateGridSubAjax.cancelEvent(demoId,{ 
		async: true,
		callback: function(data)
		{

		clearDemoScheduleField();
		arr = [];
		//document.getElementById("attendeeDiv").style.display='none';
		getCandidateGrid();
		refreshStatus();
		$('#message2show').html(resourceJSON.msgEventCancelled1);
		$('#myModal2').modal('show');
		//$('#loadingDiv').modal('hide');
		},
	});
}

function downloadOrOpenDemoNoteFile(filePath,fileName,linkId)
{
	//alert("downloadOrOpenNoteFile :"+downloadOrOpenNoteFile+"  demoid :"+demoid);
	CandidateGridAjax.downloadOrOpenDemoNoteFile(filePath,fileName,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			$('#loadingDiv').hide();
			if (data.indexOf(".doc") !=-1 || data.indexOf(".xls") !=-1) {
			    document.getElementById('ifrmTrans').src = ""+data+"";
			}else{
				document.getElementById(linkId).href = data; 
				return false;
			}
		}
	});
}

function saveDemoNotes(){
	var noteFileName="";
	var notes = $('#demodivTxtNode').find(".jqte_editor").html();
	var teacherId = document.getElementById("teacherId").value;
	var jobId = document.getElementById("jobId").value;
	var demoId = document.getElementById("demoId").value;
	var districtId = document.getElementById("districtId").value;
	var schoolId = document.getElementById("schoolId").value;
	
	var fileNote=null;
	
	try{
		fileNote=document.getElementById("demoNotesfile").value;
	}catch(e){}
	
	var cnt=0;
	var focs=0;
	
	$('#demoerrordivNotes').empty();
	if ($('#demodivTxtNode').find(".jqte_editor").text().trim()=="")
	{
		$('#demoerrordivNotes').append("&#149; "+resourceJSON.PlzEtrNotes+".<br>");
		if(focs==0)
			$('#demodivTxtNode').find(".jqte_editor").focus();
		$('#demodivTxtNode').find(".jqte_editor").css("background-color", "#F5E7E1");
		cnt++;
	}else if(fileNote==""){
		$('#demoerrordivNotes').show();
		$('#demoerrordivNotes').append("&#149; "+resourceJSON.PlzUpldNotes+".<br>");
		cnt++;
	}else if(fileNote!="" && fileNote!=null)
	{
		var ext = fileNote.substr(fileNote.lastIndexOf('.') + 1).toLowerCase();	
		noteFileName="demo_note"+noteDateTime+"."+ext;
		var fileSize = 0;
		if ($.browser.msie==true)
	 	{	
		    fileSize = 0;	   
		}
		else
		{
			if(document.getElementById("demoNotesfile").files[0]!=undefined)
			{
				fileSize = document.getElementById("demoNotesfile").files[0].size;
			}
		}
		
		if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt' || ext=='xlsx' || ext=='xls'))
		{
			$('#demoerrordivNotes').show();
			$('#demoerrordivNotes').append("&#149; "+resourceJSON.PlzSelectNoteFormat+".<br>");
			cnt++;
		}
		else if(fileSize>=10485760)
		{
			$('#demoerrordivNotes').show();
			$('#demoerrordivNotes').append("&#149; "+resourceJSON.msgfilesizelessthan+"<br>");
			cnt++;
		}
	}
	if(cnt==0){	
		try{
			$('#loadingDiv').show();
			if(fileNote!="" && fileNote!=null)
			{
				document.getElementById("demofrmNoteUpload").submit();
			}
			else
			{
							
				CandidateGridAjax.saveDemoNotes(demoId,teacherId,jobId,districtId,schoolId,notes,noteFileName,{
					async:false,
					errorHandler:handleError,
					callback:function(data)
					{	
						$('#loadingDiv').hide();
						getNotesTimelineDivDiv(true,teacherId,jobId);
						showDemoNotesDiv();
					}
			});	
			}
		}catch(err){}
	}
}

function saveDemoNoteFile(noteDateTime)
{	
	//alert(saveDemoNoteFile);
	var notes = $('#demodivTxtNode').find(".jqte_editor").html();
	var teacherId = document.getElementById("teacherId").value;
	var jobId = document.getElementById("jobId").value;
	var demoId = document.getElementById("demoId").value;
	var districtId = document.getElementById("districtId").value;
	var schoolId = document.getElementById("schoolId").value;
	
	var fileNote=null;
	try{
		fileNote=document.getElementById("demoNotesfile").value;
	}catch(e){}
	
	var noteFileName="";
	
	if(fileNote!="" && fileNote!=null)
	{
		var ext = fileNote.substr(fileNote.lastIndexOf('.') + 1).toLowerCase();	
		noteFileName="demo_note"+noteDateTime+"."+ext;
	}
	
	CandidateGridAjax.saveDemoNotes(demoId,teacherId,jobId,districtId,schoolId,notes,noteFileName,{
		async:false,
		errorHandler:handleError,
		callback:function(data)
		{	
			$('#loadingDiv').hide();
			getNotesTimelineDivDiv(true,teacherId,jobId);
			showDemoNotesDiv();
		}
	});
	
}


function closeDemoNotes()
{
	$('#demoNotesfile').val("" );
	$('#demotxtNotes').val("");
	$('#myModalDemoNotes').modal('show');
}

function addDemoNoteFileType()
{
	$('#demofileNotes').empty();
	$('#demofileNotes').html("<a href='javascript:void(0);' onclick='removeDemoNotesFile();'><img src='images/can-icon.png' title='remove'/></a> <input id='demoNotesfile' name='demoNotesfile' size='20' title='"+resourceJSON.ChooseFile+"' type='file' style='margin-left:20px;margin-top:-15px;'><br>&nbsp;"+resourceJSON.MaxSize10Mb+".");
}
function removeDemoNotesFile()
{
	$('#demofileNotes').empty();
	$('#demofileNotes').html("<a href='javascript:void(0);' onclick='addDemoNoteFileType();'><img src='images/attach.png'/></a> <a href='javascript:void(0);' onclick='addDemoNoteFileType();'>"+resourceJSON.AttachFile+"</a>");
}

function showDemoNotesDiv()
{
	$('#myModalMessage').modal('hide');	
	$('#myModalNotes').modal('hide');
	$("#saveToFolderDiv").modal("hide");
	$("#demoModalDemoNotes").modal("hide");
	$('#myModalDemoNotes').modal('show');
}

function showPanelistDiv()
{
	if(document.getElementById("choice3").checked)
	{
		$("#panelDistrictdiv").show();
		$("#panelSchooldiv").show();
		$("#panelUserdiv").show();
	}else
	{
		$("#panelDistrictdiv").hide();
		$("#panelSchooldiv").hide();
		$("#panelUserdiv").hide();
	}
}

var arrPanel =[];
function getPanel(teacherId,jobId,isPanel)
{
	//alert($('#jPanelStauts').val());
	$("html, body").animate({ scrollTop: 0 }, "slow");
	clearPanelField();
	
	$('#addPanelMemberDiv').hide();
	clearNewPanelMember();
	for(i=1;i<=3;i++)
		document.getElementById("choice"+i).checked=false;
	
	arrPanel = [];
	$('#panelId').val("");
	$('#panelistDiv').html("");
	var teacherDetail = {teacherId:teacherId};
	var jobOrder = {jobId:jobId};
	
	/*if(isPanel)
	{*/
		//CandidateGridSubAjax.displayPannel(teacherDetail,jobOrder,isPanel,$('#jPanelStauts').val(),{
	if(teacherId!=null && teacherId!=""){
		CandidateGridSubAjax.displayPannel(teacherDetail,jobOrder,true,$('#jPanelStauts').val(),{ 
			async: true,
			callback: function(data)
			{ 
			
			if(data.panelAttendeeList!=undefined)
			{
				
				$('#panelDate').val(data.panelDate);
				$('#panelTime').val(data.panelTime);
				
				$('#panelistDiv').html(data.panelAttendees);
				$('#panelistDiv').show();
				$('#panelLocation').val(data.panelAddress);
				$('#panelDisSchCmnt').val(data.panelDescription);
				$('#panelMeridiem').val(data.timeFormat);
				$('#panelTimezone').val(data.timeZoneId);
				$('#panelId').val(data.panelId);
				//$('#demoNoteId').val(data.demoId);
				$('#panelCreatedDate').val(data.panelDate);
				
				arrPanel = data.panelAttendeeList;
				//alert(arrPanel.length);
				if(data.panelStatus=="Completed")
				{
					$('#panelStatus').attr('checked', true);
					$('#panelDtatus').attr("disabled", "disabled");
					$('#panelDate').attr("disabled", "disabled");
					$('#panelTime').attr("disabled", "disabled");
					$('#panelMeridiem').attr("disabled", "disabled");
					$('#panelTimezone').attr("disabled", "disabled");
					$('#panelLocation').attr("disabled", "disabled");
					$('#panelUserId').attr("disabled", "disabled");
					$('#panelDisSchCmnt').attr("disabled", "disabled");
					//$('#canEvtBtn').attr("disabled", "disabled");
					$('#doneBtn').attr("disabled", "disabled");
					$('#addAttHref').attr("disabled", "disabled");
				}
			  }
			},
		});
	}
	//}
	
	if(document.getElementById("panelSchoolName"))
		getUsersByDistrictForPanel();

	document.getElementById("attendeeDiv").style.display='inline';

	$("#panelDistrictdiv").hide();
	$("#panelSchooldiv").hide();
	$("#panelUserdiv").hide();
	
	dwr.util.setValues({panelUserId:null});
	$("#pteacherId").val(teacherId);
	$("#jobId").val(jobId);
	$('#panelErrordiv').empty();
	$('#myModalPanel').modal('show');

}

function getUsersBySchoolForPanel(){

	if(document.getElementById('schoolId').value!="")
	{
		var schoolId=trim(document.getElementById('schoolId').value);
		CandidateGridSubAjax.getUsersBySchool(schoolId,true,{ 
			async: true,
			callback: function(data)
			{
			$('#panelUserId').html(data);
			},
		});
	}else
		getUsersByDistrictForPanel();

}

function getUsersByDistrictForPanel(){
	var districtId=0;
	CandidateGridSubAjax.getUsersByDistrict(districtId,true,{ 
		async: true,
		callback: function(data)
		{
		//alert(data);
		$('#panelUserId').html(data);
		},
	});
}

/*======================save demo schedule data ==============================*/
function savePanel(checkedFlag){


	$('#panelDate').css("background-color", "");
	$('#panelTime').css("background-color", "");
	$('#panelMeridiem').css("background-color", "");
	$('#panelTimezone').css("background-color", "");
	$('#panelLocation').css("background-color", "");
	$('#panelUserId').css("background-color", "");

	//var panelDate 		=		new Date(trim(document.getElementById("panelDate").value));
	var panelDateVal 	=		trim(document.getElementById("panelDate").value);
	var panelTime		=		trim(document.getElementById("panelTime").value);
	var panelMeridiem		=		trim(document.getElementById("panelMeridiem").value);
	var panelLocation		=		trim(document.getElementById("panelLocation").value);
	var disSchCmnt		=		trim(document.getElementById("panelDisSchCmnt").value);
	var t				=		document.getElementById("panelTimezone");
	var panelTimezone		=		trim(t.options[t.selectedIndex].value);
	var e				=		document.getElementById("panelUserId");
	var userId			=		trim(e.options[e.selectedIndex].value);
	var panelStatus		=		document.getElementById("panelStatus");
	
	var teacherId		=		trim(document.getElementById("pteacherId").value);
	var jobId		=		trim(document.getElementById("jobId").value);
	var panelId = $('#panelId').val();
	var counter=0;
	var focus=0;	

	$('#panelErrordiv').empty();
	
	if (panelDateVal=="" || panelDate=="Invalid Date")
	{
		$('#panelErrordiv').append("&#149; "+resourceJSON.PlzEtrPanelDate+"<br>");
		if(focus==0)
			$('#panelDate').focus();
		$('#panelDate').css("background-color", "#F5E7E1");
		counter++;
	}

	if(panelDateVal!=""){

		try{
			var  date1 = panelDateVal.split("-");
			var currentDate = "";
			var dateMsg = "Current";
			if(panelId=="")
			  currentDate = document.getElementById("currDate").value;
			else
			{
				currentDate = document.getElementById("currDate").value;
			  //currentDate = document.getElementById("panelCreatedDate").value;
			  //dateMsg = "panel Created";
			}
			
			var	date2 = currentDate.split("-");
			var sDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
			var eDate = new Date(date2[0]+"/"+date2[1]+"/"+date2[2]);
			if(sDate < eDate){
				$('#panelErrordiv').append("&#149; "+resourceJSON.MsgDateGreaterOrEqual+" "+dateMsg+" "+resourceJSON.MsgDate+"<br>");
				if(focus==0)
					$('#panelDate').focus();
				$('#panelDate').css("background-color", "#F5E7E1");
				counter++;

			} 
		}catch(err){}
	}
	if (panelTime=="Select Time")
	{
		$('#panelErrordiv').append("&#149; "+resourceJSON.PlzEtrPanelTime+"<br>");
		if(focus==0)
			$('#panelTime').focus();
		$('#panelTime').css("background-color", "#F5E7E1");
		counter++;
	}

	if (panelMeridiem=="Select AM/PM")
	{
		$('#panelErrordiv').append("&#149; "+resourceJSON.MsgSelectAmOrPm+"<br>");
		if(focus==0)
			$('#panelMeridiem').focus();
		$('#panelMeridiem').css("background-color", "#F5E7E1");
		counter++;
	}
	if (panelTimezone=="Select Timezone")
	{
		$('#panelErrordiv').append("&#149;"+resourceJSON.PlzSlctPanelTimeZone+"<br>");
		if(focus==0)
			$('#panelTimezone').focus();
		$('#panelTimezone').css("background-color", "#F5E7E1");
		counter++;
	}
	if (panelLocation=="")
	{
		$('#panelErrordiv').append("&#149; "+resourceJSON.PlzEtrPanlLocation+"<br>");
		if(focus==0)
			$('#panelLocation').focus();
		$('#panelLocation').css("background-color", "#F5E7E1");
		counter++;
	}
	//if (userId=="Select Attendee" || arrPanel.length==0)
	if (arrPanel.length==0)
	{
		$('#panelErrordiv').append("&#149; "+resourceJSON.PlzSlctAttendee+"<br>");
		if(focus==0)
			$('#userId').focus();
		$('#panelUserId').css("background-color", "#F5E7E1");
		counter++;
	}

	if(panelStatus.checked && !checkedFlag)
	{
		$('#message2showConfirm').html(resourceJSON.MsgMarkPanelCompleted);
		$('#footerbtn').html("<button class='btn btn-primary' onclick='savePanel(true)' >"+resourceJSON.btnOk+"</button> <button class='btn' data-dismiss='modal' aria-hidden='true'>"+resourceJSON.btnCancel+"</button>");
		$('#myModal3').modal('show');

		return;
	}

	if(counter==0){
		panelId=panelId==""?"0":panelId;
		
		CandidateGridSubAjax.savePanel(panelDateVal,panelTime,panelMeridiem,panelTimezone,panelLocation,disSchCmnt,
				teacherId,jobId,panelStatus.checked,arrPanel,panelId,$('#jPanelStauts').val(),{
			async: true,
			callback: function(data)
			{
			
			clearPanelField();
			arrPanel = [];
			//document.getElementById("attendeeDiv").style.display='none';
			$('#myModalPanel').modal('hide');
			$('#myModal3').modal('hide');
			$('#message2show').html(resourceJSON.MsgPanelScheduled);
			$('#myModal2').modal('show');
			try{candidateNotReviewedUtil.setNotReviewedFlag(teacherId,"JobGlobalTag");}catch(e){alert(e);}
			getCandidateGrid();
			refreshStatus();
			
			},
		});
	}
}

//addPanelist
function addPanelist(){
	var e				=		document.getElementById("panelUserId");
	var userId			=		trim(e.options[e.selectedIndex].value);
	var userName		=		trim(e.options[e.selectedIndex].text);
	var panelistDiv		=		document.getElementById("panelistDiv");
	
	var strInnerHtml	=		trim("<div class=\"span3\">"+resourceJSON.msgNoPanelistFound+"</div>");
	if(panelistDiv.innerHTML==strInnerHtml){
		panelistDiv.innerHTML="";
	}
	
	if(e.selectedIndex==0)
	{
		$('#panelErrordiv').append("&#149; "+resourceJSON.msgNoPanelistFound+"<br>");
		$('#userId').focus();
		return;
	}
	
	getAllPanelistDivId();
	$('#panelErrordiv').empty();
	if($.inArray(parseInt(userId), arrPanel)==-1)
	{
		
		arrPanel.push(parseInt(userId));

		var newAttendee=panelistDiv.innerHTML +"<div id='P"+userId+"' class='row col-sm-4 col-md-4'>"+userName
		+"&nbsp;<a href='javascript:void(0);' onclick='deletePanelist(\""+userId+"\");'><img width='15' height='15' class='can' src='images/can-icon.png' alt=''></a></div>";
		$('#panelistDiv').html(newAttendee);

	}else
	{	
		$('#panelErrordiv').append("&#149;"+resourceJSON.msgPanelistalreadyadded+"<br>");
		$('#userId').focus();
		//$('#userId').css("background-color", "#F5E7E1");
	}
	$('#panelistDiv').show();
}
/*========================================delete Panelist ==========================================*/
function deletePanelist(attendeeId){

	$("#P"+attendeeId).remove();
	var i = arrPanel.indexOf(parseInt(attendeeId));

	if(i != -1) {
		arrPanel.splice(i, 1);
	}
}
function getAllPanelistDivId(){
	var all = $("#panelistDiv > div").map(function() {
		//alert(this.id);
		return this.id;
	}).get();
}
function clearPanelField(){
	
	dwr.util.setValues({panelDate:null,panelTime:null,panelMeridiem:null,location:null,panelDisSchCmnt:null,panelTimezone:null,userId:null,panelstatus:null,panelLocation:null});
	$('#panelDate').css("background-color", "");
	$('#panelTime').css("background-color", "");
	$('#panelMeridiem').css("background-color", "");
	$('#panelTimezone').css("background-color", "");
	$('#panelLocation').css("background-color", "");
	$('#panelUserId').css("background-color", "");

	$('#panelstatus').attr('checked', false);
	$('#panelstatus').attr("disabled", false);
	$('#panelDate').attr("disabled", false);
	$('#panelTime').attr("disabled", false);
	$('#panelMeridiem').attr("disabled", false);
	$('#panelTimezone').attr("disabled", false);
	$('#panelLocation').attr("disabled", false);
	$('#panelUserId').attr("disabled",false);
	$('#panelDisSchCmnt').attr("disabled",false);
	//$('#canEvtBtn').attr("disabled",false);
	$('#doneBtn').attr("disabled",false);
	$('#addAttHref').attr("disabled",false);

	if(document.getElementById("panelSchoolName"))
	{
		$('#panelSchoolName').val("");
		$('#schoolId').val("");
	}
	$('#panelDiv').hide();
}

function cancelPanelEvent()
{
	if($('#panelId').val()=="")
	{
		$('#panelErrordiv').append("&#149; "+resourceJSON.msgEventCancelled+"<br>");
	}else
	{
		$('#message2showConfirm').html(""+resourceJSON.msgWantToCancelled+"");
		var panelId = $('#panelId').val();
		$('#footerbtn').html("<button class='btn btn-primary' onclick=\"cancelPanelEventOk("+panelId+")\" >"+resourceJSON.btnOk+"</button> <button class='btn' data-dismiss='modal' aria-hidden='true'>"+resourceJSON.btnCancel+"</button>");
		$('#myModal3').modal('show');
	}
}

function cancelPanelEventOk(panelId)
{
	$('#myModal3').modal('hide');
	$('#myModalPanel').modal('hide');
	//$('#loadingDiv').modal('show');

	CandidateGridSubAjax.cancelPanelEvent(panelId,{ 
		async: true,
		callback: function(data)
		{

		clearPanelField();
		arrPanel = [];
		//document.getElementById("attendeeDiv").style.display='none';
		getCandidateGrid();
		refreshStatus();
		$('#message2show').html(resourceJSON.msgEventCancelled1);
		$('#myModal2').modal('show');
		//$('#loadingDiv').modal('hide');
		},
	});
}
function showPanels(){
	
	if(document.getElementById("choice1").checked)
	{
		var districtId=0;
		CandidateGridSubAjax.showPanels(districtId,{ 
			async: true,
			callback: function(data)
			{
				$('#panelsId').html(data);
				$('#panelDiv').show();
				
			},
		});
	}else
	{
		//$('#panelsId').html("");
		$('#panelDiv').hide();
	}
}
function addAllPanelMebersFromPanel()
{
	$('#panelErrordiv').empty();
	$('#panelMemberErrordiv').hide();
	var panelsId=$("#panelsId").val();
	if(panelsId=='0')
	{
		$('#panelErrordiv').append("&#149; "+resourceJSON.msgleastonepanel1+".<br>");
		$('#panelErrordiv').show();
		return;
	}
	CandidateGridSubAjax.addAllPanelMebersFromPanel(panelsId,{ 
		async: true,
		callback: function(data)
		{
			if(data.length!=0)
			{
				
				var panelistDiv		=		document.getElementById("panelistDiv");
				
				var strInnerHtml	=		trim("<div class=\"span3\">"+resourceJSON.msgNoPanelistFound+"</div>");
				if(panelistDiv.innerHTML==strInnerHtml){
					panelistDiv.innerHTML="";
				}
				
				
				var arr = [];
				var nameArr = [];
				for(i=0;i<data.length;i++)
				{
					var myData = data[i].split("#@");
					/*var uId=data[i].split("#@")[0];
					var uName=data[i].split("#@")[1];*/
					if($.inArray(parseInt(myData[0]), arrPanel)==-1)
					{
						arr.push(myData[0]);
						nameArr.push(myData[1]);
					}
				}
				
				getAllPanelistDivId();
				$('#panelErrordiv').empty();
				
				$.each(arr, function( index, value ) {
					  //alert( index + ": " + value );
					  arrPanel.push(parseInt(value));
					 
					  var newAttendee=panelistDiv.innerHTML +"<div id='P"+value+"' class='col-sm-3 col-md-3'>"+nameArr[index]
						+"&nbsp;<a href='javascript:void(0);' onclick='deletePanelist(\""+value+"\");'><img width='15' height='15' class='can' src='images/can-icon.png' alt=''></a></div>";
						$('#panelistDiv').html(newAttendee);
					});
				$('#panelistDiv').show();
				$('#panelDiv').hide();
				document.getElementById("choice1").checked=false; 
			}else
			{
				$('#panelErrordiv').append("&#149;  "+resourceJSON.msgNoanyPanelMemember+"<br>");
				$('#panelErrordiv').show();
			}
			
		},
	});
}
function viewPanelMembers(){

	
	$('#panelErrordiv').empty();
	$('#panelMemberErrordiv').hide();
	var panelsId=$("#panelsId").val();
	if(panelsId=='Select Panel')
	{
		$('#panelErrordiv').append("&#149; "+resourceJSON.msg1PaneltoView+"<br>");
		$('#panelErrordiv').show();
		return;
	}
	CandidateGridSubAjax.viewPanelMembers(panelsId,{ 
		async: true,
		callback: function(data)
		{
			if(data!=0)
			{
				$('#panelMemberData').html(data);
				//$('#panelMemberDiv').show();
				try{$('#panelMemberDiv').modal('show');}catch(e){}
				try{$('#myModalPanel').modal('hide');}catch(e){}
			}else
			{
				$('#panelErrordiv').append("&#149; "+resourceJSON.msgNoanyPanelMemember+"<br>");
				$('#panelErrordiv').show();
			}
			
		},
	});
}
function checkAll(flg)
{
	var checkBox = document.getElementsByName("case");
	for(i=0;i<checkBox.length;i++)
		checkBox[i].checked=flg;
}

function addPanelistFromPanel(){
	$('#panelMemberErrordiv').hide();
	if($('input[name=case]:checkbox:checked').length == 0 )
	{
		$('#panelMemberErrordiv').append("&#149; "+resourceJSON.msgpanelmemberattach+"<br>");
		$('#panelMemberErrordiv').show();
		return;
	} 
	
	var panelistDiv		=		document.getElementById("panelistDiv");
	
	var strInnerHtml	=		trim("<div class=\"span3\">"+resourceJSON.msgNoPanelistFound+"</div>");
	if(panelistDiv.innerHTML==strInnerHtml){
		panelistDiv.innerHTML="";
	}
	
	var checkBox = document.getElementsByName("case");
	var arr = [];
	for(i=0;i<checkBox.length;i++)
	{
		if(checkBox[i].checked==true)
		{
			//alert(checkBox[i].value);
			if($.inArray(parseInt(checkBox[i].value), arrPanel)==-1)
			{
				arr.push(checkBox[i].value);
			}
		}
	}
	
	getAllPanelistDivId();
	$('#panelErrordiv').empty();
	
	$.each(arr, function( index, value ) {
		  //alert( index + ": " + value );
		  arrPanel.push(parseInt(value));
		  var newAttendee=panelistDiv.innerHTML +"<div id='P"+value+"' class='row col-sm-4 col-md-4'>"+$("#u"+value).html()
			+"&nbsp;<a href='javascript:void(0);' onclick='deletePanelist(\""+value+"\");'><img width='15' height='15' class='can' src='images/can-icon.png' alt=''></a></div>";
			$('#panelistDiv').html(newAttendee);
		});
	
	$('#panelistDiv').show();
	$('#panelMemberDiv').hide();
	hideMemberPanel();
	$('#panelDiv').hide();
	document.getElementById("choice1").checked=false; 
}
function hideMemberPanel()
{
	try{$('#panelMemberDiv').modal('hide');}catch(e){}
	try{$('#myModalPanel').modal('show');}catch(e){}
}
function hideAddMemberPanel()
{
	try{$('#myModalPanel').modal('show');}catch(e){}
	try{$('#addPanelMemberDiv').modal('hide');}catch(e){}
	clearNewPanelMember();
}
function addNewPanelist()
{
	/*try{$('#addPanelMemberDiv').modal('show');}catch(e){}
	try{$('#myModalPanel').modal('hide');}catch(e){}*/
	if(document.getElementById("choice2").checked)
	{
		$('#addPanelMemberDiv').show();
	}else
	{
		$('#addPanelMemberDiv').hide();
	}
	clearNewPanelMember();
}
function clearNewPanelMember()
{
	$('#panelMemberFirstName').css("background-color", "");
	$('#panelMemberLastName').css("background-color", "");
	$('#panelMemberEmail').css("background-color", "");
	$('#panelMemberFirstName').val("");
	$('#panelMemberLastName').val("");
	$('#panelMemberEmail').val("");
	$('#panelAddMemberErrordiv').empty();
}
function addNewPanelMember()
{

	$('#panelMemberFirstName').css("background-color", "");
	$('#panelMemberLastName').css("background-color", "");
	$('#panelMemberEmail').css("background-color", "");
	var panelMemberFirstName =	trim(document.getElementById("panelMemberFirstName").value);
	var panelMemberLastName =	trim(document.getElementById("panelMemberLastName").value);
	var panelMemberEmail =	trim(document.getElementById("panelMemberEmail").value);
	$('#panelAddMemberErrordiv').empty();
	
	var counter=0;
	var focus=0;	
	
	if (panelMemberFirstName=="")
	{
		$('#panelAddMemberErrordiv').append("&#149; "+resourceJSON.PlzEtrFirstName+"<br>");
		if(focus==0)
			document.getElementById("panelMemberFirstName").focus();
		
		$('#panelMemberFirstName').css("background-color", "#F5E7E1");
		counter++;
		focus++;
	}
	if (panelMemberLastName=="")
	{
		$('#panelAddMemberErrordiv').append("&#149; "+resourceJSON.PlzEtrLastName+"<br>");
		if(focus==0)
			document.getElementById("panelMemberLastName").focus();
		$('#panelMemberLastName').css("background-color", "#F5E7E1");
		counter++;
		focus++;
	}
	if (panelMemberEmail=="")
	{
		$('#panelAddMemberErrordiv').append("&#149; "+resourceJSON.PlzEtrEmail+"<br>");
		if(focus==0)
			document.getElementById("panelMemberEmail").focus();
		$('#panelMemberEmail').css("background-color", "#F5E7E1");
		counter++;
		focus++;
	}else if(!isEmailAddress(panelMemberEmail))
	{		
		$('#panelAddMemberErrordiv').append("&#149; "+resourceJSON.PlzEtrVldEmail+"<br>");
		if(focus==0)
			document.getElementById("panelMemberEmail").focus();
		$('#panelMemberEmail').css("background-color", "#F5E7E1");
		counter++;
		focus++;
	}
	
	if(counter>0)
	{
		$('#panelAddMemberErrordiv').show();
		return;
	}
	
	CandidateGridSubAjax.addNewPanelMember(panelMemberFirstName,panelMemberLastName,panelMemberEmail,{ 
		async: true,
		callback: function(data)
		{
			addNewPanelistMember(data[0],data[1],data[2]);
			//hideAddMemberPanel();
			clearNewPanelMember();
			$('#addPanelMemberDiv').hide();
			document.getElementById("choice2").checked=false; 
		},
	});
}

function addNewPanelistMember(userId,firstName,lastName){
	var userName		=		firstName+" "+lastName;
	var panelistDiv		=		document.getElementById("panelistDiv");
	
	var strInnerHtml	=		trim("<div class=\"span3\">"+resourceJSON.msgNoPanelistFound+"</div>");
	if(panelistDiv.innerHTML==strInnerHtml){
		panelistDiv.innerHTML="";
	}
	
	getAllPanelistDivId();
	$('#panelErrordiv').empty();
	if($.inArray(parseInt(userId), arrPanel)==-1)
	{
		
		arrPanel.push(parseInt(userId));

		var newAttendee=panelistDiv.innerHTML +"<div id='P"+userId+"' class='span3'>"+userName
		+"&nbsp;<a href='javascript:void(0);' onclick='deletePanelist(\""+userId+"\");'><img width='15' height='15' class='can' src='images/can-icon.png' alt=''></a></div>";
		$('#panelistDiv').html(newAttendee);

	}else
	{	
		$('#panelErrordiv').append("&#149; "+resourceJSON.msgPanelistalreadyadded+"<br>");
	}
	$('#panelistDiv').show();
	//$('#addPanelMemberDiv').hide();
	clearNewPanelMember();
	
}
var tId;
var jId;
var isPanelReq;
function getPanelByJobWiseStatus(teacherId,jobId,isPanel)
{
	isPanel=true;
	CandidateGridSubAjax.getPanelByJobWiseStatus(jobId,{ 
		async: true,
		callback: function(data)
		{
			if(data[0]==1)
			{	
				$('#errordivStauts').hide();
				$('#jobPanelStatusId').html(data[1]);
				$('#myJobWiseStatusDiv').modal('show');
				tId=teacherId;
				jId=jobId;
				isPanelReq=isPanel;
			}else
			{
				$('#myJobWiseStatusDiv').modal('hide');
				$('#jPanelStauts').val(data[1]);
				getPanel(teacherId,jobId,isPanel,data[1]);
			}
		},
	});
	//getPanel(teacherId,jobId,isPanel);
}

function showPanel()
{
	if($('#jobPanelStatusId').val()=="Select Status")
	{
		$('#errordivStauts').empty();
		$('#errordivStauts').html("Please select a Stauts");
		$('#errordivStauts').show();
		
	}else
	{
		$('#myJobWiseStatusDiv').modal('hide');
		$('#jPanelStauts').val($('#jobPanelStatusId').val());
		getPanel(tId,jId,isPanelReq,$('#jobPanelStatusId').val());
	}
}
function cancelOne()
{
	$('#panelDiv').hide();
	document.getElementById("choice1").checked=false; 
	$('#panelErrordiv').hide();
}
function cancelTwo()
{
	clearNewPanelMember();
	$('#addPanelMemberDiv').hide();
	document.getElementById("choice2").checked=false; 
	$('#panelErrordiv').hide();
}
//************** Mukesh Phone Interview Code Start******************************
function clearColorEvents(){
	$('#eventArrordiv').empty();
	$('#eventName').css("background-color", "");
	$('#eventDescriptionCG').find(".jqte_editor").css("background-color", "");
	$('#subjectforParticipants').css("background-color","");
	$('#msgforParticipantsCG').find(".jqte_editor").css("background-color","");
	$('#subjectforFacilator').css("background-color","");
	$('#messtoFaciliatatorCG').find(".jqte_editor").css("background-color","");
}

function validateEvents(){
	
	clearColorEvents();
	var cnt=0;
	var focs=0;
	if (trim(document.getElementById("eventName").value)==""){
		$('#eventArrordiv').append("&#149; "+resourceJSON.msgEventName+"<br>");
		if(focs==0)
			$('#eventName').focus();
		$('#eventName').css("background-color", "#F5E7E1");
		cnt++;focs++;
	}
	//schedule type validation
	var intschedule_Id = document.getElementsByName('intscheduleId');
	var intscheduleP="";
	for(var i = 0; i < intschedule_Id.length; i++){
	    if(intschedule_Id[i].checked){
	        intscheduleP = intschedule_Id[i].value;
	    }
	}
	if (intscheduleP == null || intscheduleP == "") {
		$('#eventArrordiv').append("&#149; "+resourceJSON.msgScheduleType+"<br>");
		if (focs==0)
			$('input:radio[name=intscheduleId]')[0].focus();
		cnt++;focs++;
	}
	//description validation
/*	if ($('#eventDescriptionCG').find(".jqte_editor").text().trim()==""){
		$('#eventArrordiv').append("&#149; Please enter Description<br>");
		if(focs==0)
			$('#eventDescriptionCG').find(".jqte_editor").focus();
		$('#eventDescriptionCG').find(".jqte_editor").css("background-color", "#F5E7E1");
		cnt++;focs++;
	}*/
	//participants validation
	if (trim(document.getElementById("subjectforParticipants").value)==""){
		$('#eventArrordiv').append("&#149; "+resourceJSON.msgSubjecttoCandidate+"<br>");
		if(focs==0)
			$('#subjectforParticipants').focus();
		$('#subjectforParticipants').css("background-color", "#F5E7E1");
		cnt++;focs++;
	}
	if ($('#msgforParticipantsCG').find(".jqte_editor").text().trim()==""){
		$('#eventArrordiv').append("&#149;  "+resourceJSON.msgMessagetoCandidates+"<br>");
		if(focs==0)
			$('#msgforParticipantsCG').find(".jqte_editor").focus();
		$('#msgforParticipantsCG').find(".jqte_editor").css("background-color", "#F5E7E1");
		cnt++;focs++;
	}
	
	if (trim(document.getElementById("subjectforFacilator").value)==""){
		$('#eventArrordiv').append("&#149; "+resourceJSON.msgSubjecttoFacilitators+"<br>");
		if(focs==0)
			$('#subjectforFacilator').focus();
		$('#subjectforFacilator').css("background-color", "#F5E7E1");
		cnt++;focs++;
	}
	
	if ($('#messtoFaciliatatorCG').find(".jqte_editor").text().trim()==""){
		$('#eventArrordiv').append("&#149;  "+resourceJSON.msgtofacilators+"<br>");
		if(focs==0)
			$('#messtoFaciliatatorCG').find(".jqte_editor").focus();
		$('#messtoFaciliatatorCG').find(".jqte_editor").css("background-color", "#F5E7E1");
		cnt++;focs++;
	}
	
	if(cnt==0)
		return true;
	else
	{
		$('#eventArrordiv').show();
		return false;
	}
}
function clearPhoneInterviewData(){
	dwr.util.setValues({eventId:null,eventName:null,msgtoparticipants:null,subjectforParticipants:null,subjectforFacilator:null,msgtofacilitators:null});
	$('#msgforParticipantsCG').find(".jqte_editor").html("");
	$('#messtoFaciliatatorCG').find(".jqte_editor").html("");
	$('#eventDescriptionCG').find(".jqte_editor").html("");
}

function addPhoneInterview(teacherId,jobId,isDemo)
{
	clearColorEvents();
	clearPhoneInterviewData();
	
	$('#evtsch').tooltip();
	$("html, body").animate({ scrollTop: 0 }, "slow");
	$('#eventArrordiv').empty();
	$('#addEditEventDiv').show();
	$('#facilitatorAndScheduleDiv').hide();
	document.getElementById("teacherIdforEvent").value=teacherId;
	getEventSchedule(6);
	getEmailTemplatesFacilitatorsByDistrict(document.getElementById('districtIdforEvent').value);
	getEmailTemplatesByDistrictId(document.getElementById('districtIdforEvent').value);
	try{$('#myModalPhoneInterview').modal('show');}catch(e){}
	$('#eventName').focus();
}


function savePhoneEvent(){
	if(!validateEvents())
		return;
	var eventDetails = {eventId:null,eventName:null,msgtoparticipants:null,subjectforParticipants:null,subjectforFacilator:null,msgtofacilitators:null};
	var districtId = document.getElementById('districtIdforEvent').value;
	//alert("districtId :: "+districtId)
	var intschedule_Id = document.getElementsByName('intscheduleId');
	var intschedule="";
	for(var i = 0; i < intschedule_Id.length; i++){
	    if(intschedule_Id[i].checked){
	        intschedule = intschedule_Id[i].value;
	    }
	}	
	var teacherId= document.getElementById("teacherIdforEvent").value
		
	dwr.util.getValues(eventDetails);
	dwr.engine.beginBatch(); 
	eventDetails.description=dwr.util.getValue("eventDescription");
	
	var jobOrder = {jobId:dwr.util.getValue("jobId")};
	eventDetails.jobOrder = jobOrder;
	
	EventAjax.saveCGPhoneEvent(eventDetails,districtId,intschedule,teacherId,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
		//exitfrompageforEventOnCG();
		 document.getElementById('eventId').value=data.eventId;
		 if(data.eventSchedulelId.eventScheduleName.toUpperCase()=="Fixed Date and Time".toUpperCase())
			 document.getElementById('chkDATFlag').value=1;
		 else
			 document.getElementById('chkDATFlag').value=3;
		 
		 $('#addEditEventDiv').hide();
		 $('#facilitatorAndScheduleDiv').show();
		 getDistrictUser(districtId);
		 displayPhoneInterviewFacilitator(data.eventId)
		 displayEventSchedule();
		}
	});
	dwr.engine.endBatch();
}


function getDistrictUser(districtId)
{
	EventAjax.getDistrictUserList(districtId,{
		async : true,
		errorHandler : handleError,
		callback : function(data) {
		  $("#facilitatorId").html(data);
		}
	});
}

function addFacilitatorCG(){
	var eventId = document.getElementById('eventId').value;
	var userId = document.getElementById('facilitatorId').value;
	
	if(userId!=null && userId!='' && userId!="0"){
		ManageFacilitatorsAjax.saveFacilitatorByUserId(userId,eventId,
			{
			async: true,
			callback: function(data)
			{
				if(data=="success"){
					displayPhoneInterviewFacilitator(eventId);
				 }	
			},
	      errorHandler:handleError	
		});	
	}
}


function displayPhoneInterviewFacilitator(eventId){
	ManageFacilitatorsAjax.getFacilitatorGridNew(eventId,{
		async : true,
		callback : function(data) {
		$('#facilitatorNewDivCG').html(data.split("####")[0]);
		var cnt = data.split("####")[1];
			for(i=1;i<cnt;i++)
			{
				$('#rmId'+i+'').tooltip();
			}
		},
		errorHandler : handleError
	});
	
}

function deleteFacilitator(facilitatorId)
{
	document.getElementById("facilitatordelId").value=facilitatorId;
	$('#myModalactMsgShow').modal('show');
}
function removeFacilitatorById()
{
 var eventId=document.getElementById('eventId').value
 var facilitatorId=document.getElementById("facilitatordelId").value;
 ManageFacilitatorsAjax.removeFacilitator(facilitatorId,
		{
		  async: true,
		  callback: function(data){
	      displayPhoneInterviewFacilitator(eventId);
		  },
		 errorHandler:handleError		
		});	
 $('#myModalactMsgShow').modal('hide');	
}



function getEventSchedule(evntType) 
{
	EventAjax.getEventSchedule(evntType,{
		async : false,
		callback : function(data) {
			$('#intschedule').html(data);
		},
		errorHandler : handleError
	});
}

function getEmailTemplatesFacilitatorsByDistrict(districtId) {
	EventAjax.getEmailMessageTempFacilitators(districtId,0,0, {
		async : true,
		errorHandler : handleError,
		callback : function(data) {
			// alert(" data : "+data+" Length : "+data.length);
		if (data != "" && data.length > 1) {
			$("#msgtofacilitatorsDiv1").show();
			$("#msgtofacilitatorss").html(data);
		} else {
			$("#msgtofacilitatorsDiv1").hide();
		}
	}
 });
}
function getEmailDescription1() {
	var descId = document.getElementById('msgtofacilitatorss').value;
	if (descId != null && descId != '') {
		EventAjax.getFacilitatorsEmailMessageTempBody(descId, {
			async : true,
			errorHandler : handleError,
			callback : function(data) {
			$('#messtoFaciliatatorCG').find(".jqte_editor").html(
					data.split("@@@@####@@@@")[0]);
			document.getElementById('subjectforFacilator').value = data
					.split("@@@@####@@@@")[1];
		}
		});
	} else {
		$('#messtoFaciliatatorCG').find(".jqte_editor").html("");
		document.getElementById('subjectforFacilator').value = "";
	}
}


function getEmailTemplatesByDistrictId(districtId) {
	EventAjax.getEventEmailMessageTemp(districtId,0,0, {
		async : true,
		errorHandler : handleError,
		callback : function(data) {
		if (data != "" || data.length > 0) {
			$("#messagetoprinciple").html(data);
		} else {
			$("#msgtopart").hide();
			$("#messagetoprincipleDiv").hide();
		}
	 }
	});
}

function getEmailDescription() {
	var descId = document.getElementById('messagetoprinciple').value;
	if (descId != null && descId != '') {
		EventAjax.getEventEmailMessageTempBody(descId, {
			async : true,
			errorHandler : handleError,
			callback : function(data) {
				$('#msgforParticipantsCG').find(".jqte_editor").html(
						data.split("@@@@####@@@@")[0]);
				document.getElementById('subjectforParticipants').value = data
						.split("@@@@####@@@@")[1];
			}
		});
	} else {
		$('#msgforParticipantsCG').find(".jqte_editor").html("");
		document.getElementById('subjectforParticipants').value = "";
	}
}


function editPhoneInterview(eventid) 
{
	$('#loadingDiv').fadeIn();
//	delay(1);
	EventAjax.getEventByEventId(eventid, {
		async : true,
		errorHandler : handleError,
		callback : function(data) 
		{
		
		$('#myModalPhoneInterview').modal('show');
		clearColorEvents();
		$('#eventName').focus();
		$('#evtsch').tooltip();
		$("html, body").animate({ scrollTop: 0 }, "slow");
		$('#eventArrordiv').empty();
		$('#addEditEventDiv').show();
		$('#facilitatorAndScheduleDiv').hide();
	 	dwr.util.setValues(data);
		$('#msgforParticipantsCG').find(".jqte_editor").html(data.msgtoparticipants);
		$('#messtoFaciliatatorCG').find(".jqte_editor").html(data.msgtofacilitators);
		$('#eventDescriptionCG').find(".jqte_editor").html(data.description);
		getEventSchedule(6);
		var intschedule_Id = document.getElementsByName('intscheduleId');
		$('input[name=intscheduleId][value='+data.eventSchedulelId.eventScheduleId+']').attr('checked', true);
		$('#loadingDiv').hide();
	 }
	});
	displayCommanMethods();
	
	
}

function displayCommanMethods(){
	var districtId=document.getElementById('districtIdforEvent').value
	getEmailTemplatesFacilitatorsByDistrict(districtId);
	getEmailTemplatesByDistrictId(districtId);
}

function showAddEditPhoneEvents(){
	$('#addEditEventDiv').show();
	$('#facilitatorAndScheduleDiv').hide();
}

///////////////// EventSchedule ///////////////////////////
function displayEventSchedule()
{
	$('#eventScheduleDivMain').html("");
	var eventId = document.getElementById('eventId').value;
		var chkDATFlag = document.getElementById('chkDATFlag').value;
		if(chkDATFlag!=1)
		{
			if(chkDATFlag==2)
			{
				$("#addScheduleLink").show();
				$("#addScheduleLinkForSlots").hide();
			}
			else if(chkDATFlag==3)
			{
				$("#addScheduleLink").hide();
				$("#addScheduleLinkForSlots").show();
			}
		}
		else
		{
			$("#addScheduleLink").hide();
			$("#addScheduleLinkForSlots").hide();
		}
		try{
		
			EventScheduleAjax.geteventSchedule(eventId,noOfRows,page,sortOrderStr,sortOrderType,{
				async: false,
				callback: function(data)
				{
					try{$('#eventScheduleDivMain').html(data[0]);
					}catch(e){}
					$('#noofcandidate').tooltip();
					//document.getElementById("eventScheduleDivMain").innerHTML=data[0];
					$('#inputDivCount').val(data[1]);
					var cnt = data[1];
					
					
						for(i=1;i<cnt;i++)
						{
							try{
							var cal = Calendar.setup({ onSelect: function(cal) { cal.hide()}, showTime: true});
							cal.manageFields('eventStartDate'+i+'', 'eventStartDate'+i+'', '%m-%d-%Y');
							cal.manageFields('eventEndDate'+i+'', 'eventEndDate'+i+'', '%m-%d-%Y');
							}catch(e){}
						}
					
				},
				errorHandler:handleError
			});
			
		}catch(e){alert(e);}
	
}


function addSchedule()
{
	$('#eventArrordiv').empty();
	$('#eventScheduleDivMain').show();
	
	var eventId = $("#eventId").val().trim();
	
	var inputDivCount = $("#inputDivCount").val();
	
	var chkDATFlag = trim(document.getElementById("chkDATFlag").value);

	EventScheduleAjax.createAddScheduleDiv(eventId,inputDivCount,{
		async: true,
		callback: function(data)
		{
			$("#eventScheduleDivMain").append(data);
			if(chkDATFlag==1)
				$("#inputDivCount").val(1);
			else
				$("#inputDivCount").val(parseInt(inputDivCount)+1);
		},
		errorHandler:handleError	
	});
}

function saveSchedule()
{
	$('#eventArrordiv').empty();	
	var counter=0;
	
	var inputDivCount = trim(document.getElementById("inputDivCount").value);
	var timezone = trim(document.getElementById("timezoneforevent").value);
	var eventId = trim(document.getElementById("eventId").value);
	var chkDATFlag = trim(document.getElementById("chkDATFlag").value);
	var inttype	= 6;//trim(document.getElementById("inttype").value);
	var singlebookingonly=false;
	if(document.getElementById("singlebookingonly")!=null){
		if(document.getElementById("singlebookingonly").checked){
			singlebookingonly=true
		}
	}
	var noOfCandiadte="";
	try{noOfCandiadte=document.getElementById("noofcandidateperslot").value;}catch(e){}
	if(inttype==1)
	{
		//This is vvi check
	}
	else
	{
		var scheduleIdArr = "";
		var eventStartDateArr = "";
		var starthrArr = ""; 
		var starttimeformArr = "";
		var endhrArr = ""; 
		var endtimeformArr = "";
		var locationArr = "";
		var currentDate = new Date();
		var countRecord = 0;
		
		var saveteacherarray="";
		var saveteacherarray = new Array();
		 var inputs = $("input[name=cgCBX]");
		 for (var i = 0; i < inputs.length; i++) {
				if (inputs[i].type === 'checkbox') {
					if(inputs[i].checked){	        		
						//saveteacherarray+=	$(inputs[i]).attr( "teacherid" )+",";
						saveteacherarray.push($(inputs[i]).attr( "teacherid" ));
						
					}
				}
		 } 
		if(chkDATFlag=='1')
			inputDivCount=1;
		
		
			for(var i=0;i<inputDivCount;i++)
			{
				if(i==0){
					eventStartDateArr 	= eventStartDateArr+trim(document.getElementById("eventStartDate").value)+",";
					starthrArr 			= starthrArr+trim(document.getElementById("starthr").value)+",";
					starttimeformArr 	= starttimeformArr+trim(document.getElementById("starttimeform").value)+",";
					endhrArr	 		= endhrArr+trim(document.getElementById("endhr").value)+",";
					endtimeformArr 		= endtimeformArr+trim(document.getElementById("endtimeform").value)+",";
					
					if(trim(document.getElementById("locationfirst").value)=="")
						locationArr 		= locationArr+null+"##";
					else
						locationArr 		= locationArr+trim(document.getElementById("locationfirst").value)+"##";

					scheduleIdArr		= scheduleIdArr+trim(document.getElementById("eventScheduleId").value)+",";
				}
				else{
					eventStartDateArr = eventStartDateArr+trim(document.getElementById("eventStartDate"+i).value)+",";
					starthrArr 			= starthrArr+trim(document.getElementById("starthr"+i).value)+",";
					starttimeformArr 	= starttimeformArr+trim(document.getElementById("starttimeform"+i).value)+",";
					endhrArr	 		= endhrArr+trim(document.getElementById("endhr"+i).value)+",";
					endtimeformArr 		= endtimeformArr+trim(document.getElementById("endtimeform"+i).value)+",";
					
					if(trim(document.getElementById("location"+i).value)=="")
						locationArr 		= locationArr+null+"##";
					else
						locationArr 		= locationArr+trim(document.getElementById("location"+i).value)+"##";

					scheduleIdArr		= scheduleIdArr+trim(document.getElementById("eventScheduleId"+i).value)+",";
				}
			}
			
			
			clearAllFieldsColor();
			
			//=============== Error Msg=========================		
				
				if (timezone == '') {
					if (counter == 0) {
						$('#timezoneforevent').focus();
					}
					$('#eventArrordiv').append("&#149; "+resourceJSON.msgTimeZone1+"<br>");
					$('#timezoneforevent').css("background-color", "#F5E7E1");
					$('#eventArrordiv').show();
					counter = counter + 1;
				}
			
				var eSdArrFlag = false;
				var comStartWithCurrentDateFlag = false;
				var comStartAndEndTimeFlag = false;
				var eShrArrFlag = false; 
				var eStimeformArrFlag = false;
				var eEhrArrFlag = false; 
				var eEtimeformArrFlag = false;
				
				for(var i=0;i<inputDivCount;i++)
				{
					var eSdArr = eventStartDateArr.split(",");
					var eShrArr = starthrArr.split(","); 
					var eStimeformArr = starttimeformArr.split(",");
					var eEhrArr = endhrArr.split(","); 
					var eEtimeformArr = endtimeformArr.split(",");
					var chkOneRow = false;
					
					if(i==0)
					{
						if(eSdArr[0]=='' && eShrArr[0]=='' && eEhrArr[0]=='')
			             {
							if(inputDivCount==1)
								chkOneRow=true;
			             }
						else
							chkOneRow=true;
						
						if(chkOneRow)
						{
							var startTimeDate = new Date();
							var endTimeDate = new Date();
							var startTimeHHMM = "";
							var endTimeHHMM = "";
							
							var retValue = true;
			                var stdate=eSdArr[0].split('-');
			                var dstart = new Date();
			                dstart.setFullYear(stdate[2]);
			                dstart.setMonth(stdate[0]-1);
			                dstart.setDate(stdate[1]);
			                var curdate=new Date();
							
							if (eSdArr[0] == '') {
								$('#eventStartDate').css("background-color", "#F5E7E1");
								$('#eventStartDate').focus();
								eSdArrFlag=true;
							}
							else if(curdate>dstart)
							{
								$('#eventStartDate').css("background-color", "#F5E7E1");
								$('#eventStartDate').focus();
								comStartWithCurrentDateFlag=true;
							}
								
							
							if (eShrArr[0] == '') {
								$('#starthr').focus();
								$('#starthr').css("background-color", "#F5E7E1");
								eShrArrFlag=true;
							}
							
							if (eStimeformArr[0] == '') {
								$('#starttimeform').focus();
								$('#starttimeform').css("background-color", "#F5E7E1");
								eStimeformArrFlag=true;
							}
							
							if (eEhrArr[0] == '') {
								$('#endhr').focus();
								$('#endhr').css("background-color", "#F5E7E1");
								eEhrArrFlag=true;
							}
							
							if (eEtimeformArr[0] == '') {
								$('#endtimeform').focus();
								$('#endtimeform').css("background-color", "#F5E7E1");
								eEtimeformArrFlag=true;
							}
							
							if(eShrArr[0]!='' && eStimeformArr[0]!='' && eEhrArr[0]!='' && eEtimeformArr[0]!='')
							{
								startTimeHHMM = eShrArr[0].split(":");
								endTimeHHMM = eEhrArr[0].split(":");
								if(eStimeformArr[0]=='PM')
								{
									startTimeHHMM[0] = parseInt(startTimeHHMM[0])+12;
								}
								if(eEtimeformArr[0]=='PM')
								{
									endTimeHHMM[0] = parseInt(endTimeHHMM[0])+12;
								}
								
								startTimeDate.setHours(startTimeHHMM[0], startTimeHHMM[1], '00');
								
								endTimeDate.setHours(endTimeHHMM[0], endTimeHHMM[1], '00');
								
								if(startTimeDate > endTimeDate)
								 {
									$('#starthr').focus();
									$('#starthr').css("background-color", "#F5E7E1");
									$('#starttimeform').focus();
									$('#starttimeform').css("background-color", "#F5E7E1");
									$('#endhr').focus();
									$('#endhr').css("background-color", "#F5E7E1");
									$('#endtimeform').focus();
									$('#endtimeform').css("background-color", "#F5E7E1");
									comStartAndEndTimeFlag=true;
								 }
							}
							countRecord = countRecord+1;
						}
						
					}
					else
					{
						 if(eSdArr[i]=='' && eShrArr[i]=='' && eEhrArr[i]=='')
			             {
							 
			             }else
			             {
			            	 var startTimeDate = new Date();
								var endTimeDate = new Date();
								var startTimeHHMM = "";
								var endTimeHHMM = "";
								
								var retValue = true;
				                var stdate=eSdArr[i].split('-');
				                var dstart = new Date();
				                dstart.setFullYear(stdate[2]);
				                dstart.setMonth(stdate[0]-1);
				                dstart.setDate(stdate[1]);
				                var curdate=new Date();
								
								
								
								if (eSdArr[i] == '') {
									$('#eventStartDate'+i).css("background-color", "#F5E7E1");
									$('#eventStartDate'+i).focus();
									eSdArrFlag=true;
								}else if(curdate>dstart)
								{
									$('#eventStartDate'+i).css("background-color", "#F5E7E1");
									$('#eventStartDate'+i).focus();
									comStartWithCurrentDateFlag=true;
								}
								
								if (eShrArr[i] == '') {
									$('#starthr'+i).focus();
									$('#starthr'+i).css("background-color", "#F5E7E1");
									eShrArrFlag=true;
								}
								
								if (eStimeformArr[i] == '') {
									$('#starttimeform'+i).focus();
									$('#starttimeform'+i).css("background-color", "#F5E7E1");
									eStimeformArrFlag=true;
								}
								
								
								if (eEhrArr[i] == '') {
									$('#endhr'+i).focus();
									$('#endhr'+i).css("background-color", "#F5E7E1");
									eEhrArrFlag=true;
								}
								
								if (eEtimeformArr[i] == '') {
									$('#endtimeform'+i).focus();
									$('#endtimeform'+i).css("background-color", "#F5E7E1");
									eEtimeformArrFlag=true;
								}
								
								if(eShrArr[i]!='' && eStimeformArr[i]!='' && eEhrArr[i]!='' && eEtimeformArr[i]!='')
								{
									startTimeHHMM = eShrArr[i].split(":");
									endTimeHHMM = eEhrArr[i].split(":");
									if(eStimeformArr[i]=='PM')
									{
										startTimeHHMM[i] = parseInt(startTimeHHMM[i])+12;
									}
									if(eEtimeformArr[i]=='PM')
									{
										endTimeHHMM[i] = parseInt(endTimeHHMM[i])+12;
									}
									
									startTimeDate.setHours(startTimeHHMM[0], startTimeHHMM[1], '00');
									
									endTimeDate.setHours(endTimeHHMM[0], endTimeHHMM[1], '00');
									
									if(startTimeDate > endTimeDate)
									 {
										$('#starthr'+i).focus();
										$('#starthr'+i).css("background-color", "#F5E7E1");
										$('#starttimeform'+i).focus();
										$('#starttimeform'+i).css("background-color", "#F5E7E1");
										$('#endhr'+i).focus();
										$('#endhr'+i).css("background-color", "#F5E7E1");
										$('#endtimeform'+i).focus();
										$('#endtimeform'+i).css("background-color", "#F5E7E1");
										comStartAndEndTimeFlag=true;
									 }
								}
								
								countRecord =countRecord+1;
			             }
					}
				}
				
				if(chkDATFlag==3)
				{
					if(countRecord<2)
					{
						$('#eventArrordiv').append("&#149; "+resourceJSON.msgschedulesAtLeast2+"<br>");
						$('#eventArrordiv').show();
						counter = counter + 1;
					}
				}
				
				if(chkDATFlag==2)
				{
					if(countRecord<1)
					{
						$('#eventArrordiv').append("&#149; "+resourceJSON.msgschedulesAtLeast1+"<br>");
						$('#eventArrordiv').show();
						counter = counter + 1;
					}
				}
				
				
				if(eSdArrFlag)
				{
					$('#eventArrordiv').append("&#149; "+resourceJSON.msgPlzenterDate+"<br>");
					$('#eventArrordiv').show();
					counter = counter + 1;
				}
				if(comStartWithCurrentDateFlag)
				{
					$('#eventArrordiv').append("&#149; "+resourceJSON.msgStDatelessCurrDate+"<br>");
					$('#eventArrordiv').show();
					counter = counter + 1;
				}
				if(eShrArrFlag)
				{
					$('#eventArrordiv').append("&#149; "+resourceJSON.msgPlzenterStTime+"<br>");
					$('#eventArrordiv').show();
					counter = counter + 1;
				}
						
				if(eStimeformArrFlag) {
					$('#eventArrordiv').append("&#149;"+resourceJSON.msgStartTimeFormat+"<br>");
					$('#eventArrordiv').show();
					counter = counter + 1;
				}
				
				if(eEhrArrFlag){
					$('#eventArrordiv').append("&#149; "+resourceJSON.msgEndTime1+"<br>");
					$('#eventArrordiv').show();
					counter = counter + 1;
				}
				
				if(eEtimeformArrFlag){
					$('#eventArrordiv').append("&#149; "+resourceJSON.msgEndTimeFormat+"<br>");
					$('#eventArrordiv').show();
					counter = counter + 1;
				}
				
				if(comStartAndEndTimeFlag)
				{
					$('#eventArrordiv').append("&#149; "+resourceJSON.msgEndTimeLessStTime+"<br>");
					$('#eventArrordiv').show();
					counter = counter + 1;
				}
		//================================================
	     //check for participants			
		try{
			if(counter==0){
				EventAjax.checkAvailFacilitator(eventId,{
				async: false,
				callback: function(data)
				{
				  $('#loadingDiv').hide();
				  if(data==false){
					 $('#facilitatorId').focus();
					 $('#facilitatorId').css("background-color", "#F5E7E1");
					  counter = counter + 1;
					 $('#eventArrordiv').show();
					 $('#eventArrordiv').empty();
					 $('#eventArrordiv').append("&#149;Please add Facilitators.<br>");
				  }
				},
				 errorHandler:handleError	
				});
			}
		  }catch(e){alert(e)}		
		//================================================	
		try{
			if(counter==0)
			{
				EventScheduleAjax.saveSchedule(scheduleIdArr,eventId,timezone,eventStartDateArr,starthrArr,starttimeformArr,locationArr,null,endhrArr,endtimeformArr,singlebookingonly,noOfCandiadte,
				{
							async: false,
							callback: function(data)
							{
								$("#eventTypeWiseSchedule").append("");
								//alert("data :: "+data)
								if(data=="success")
									
									//pankaj sewalia
									try{
										var teachers = "";
										for(var i=0; i<saveteacherarray.length ; i++){
											if(teachers == "")
												teachers = saveteacherarray[i];
											else
												teachers+="##"+saveteacherarray[i];
										}
										//alert(teachers);
										candidateNotReviewedUtil.setNotReviewedFlag(teachers,"CNR_IOEA");
									}catch(err) {	
										console.log("Exception in EventScheduleAjax.saveSchedule  "+err.message);
									}
								  sendPhoneInterviewFromCG();
								/*if(cont==0)
									window.location.href="manageevents.do";
								else if(cont==1)
									window.location.href="managefacilitators.do?eventId="+eventId;*/
								
							},
							errorHandler:handleError	
						});
			}
			}catch(e){alert(e)}
	}
}

function clearAllFieldsColor()
{
	var inputDivCount = $('#inputDivCount').val();
	$('#timezoneforevent').css("background-color", "");
	for(var i=0;i<inputDivCount;i++)
	{
		if(i==0){
			$('#eventStartDate').css("background-color", "");
			$('#starthr').css("background-color", "");
			$('#starttimeform').css("background-color", "");
			$('#endhr').css("background-color", "");
			$('#endtimeform').css("background-color", "");
		}
		else{
			$('#eventStartDate'+i).css("background-color", "");
			$('#starthr'+i).css("background-color", "");
			$('#starttimeform'+i).css("background-color", "");
			$('#endhr'+i).css("background-color", "");
			$('#endtimeform'+i).css("background-color", "");
		}
	}
}

///////////////////////////////////////////////////////////
function sendPhoneInterviewFromCG(){	
	$('#loadingDiv').fadeIn();
	var teacherIds="";
	
$('input[name="cgCBX"]:checked').each(function() {
		
		if($(this).attr("teacherid") !=null && ($(this).attr("teacherid")!="") )
		{
			if(teacherIds=="")
				teacherIds = teacherIds + $(this).attr("teacherid");
			else
				teacherIds = teacherIds + "," + $(this).attr("teacherid");
		}
	});
	
	var eventId=$("#eventId").val();
	if(teacherIds!=null)
	{
		try{
			ParticipantsAjax.sendEmailWithTeacherId(eventId,teacherIds,
		    {
				async: true,
				callback: function(data){ 
				  $('#loadingDiv').hide();
				  if(data==false){
					 $('#eventArrordiv').show();
					 $('#eventArrordiv').empty();
					 $('#eventArrordiv').append("&#149;"+resourceJSON.msgAddfacilators+"<br>");
				  }else{
					  $('#succesEmailMsg').html("Great! Event has been scheduled");
					  $('#sendEmailtoParticipants').modal('show');
				  }			
			}		
		     });	
		}catch(e)
		{
		 alert(e);	
		}
		
		
	}
	else
	{
	
	try{
			ParticipantsAjax.sendEmail(eventId,
		    {
				async: true,
				callback: function(data){ 
				  $('#loadingDiv').hide();
				  if(data==false){
					 $('#eventArrordiv').show();
					 $('#eventArrordiv').empty();
					 $('#eventArrordiv').append("&#149;"+resourceJSON.msgAddfacilators+"<br>");
				  }else{
					  $('#succesEmailMsg').html("Great! Event has been scheduled");
					  $('#sendEmailtoParticipants').modal('show');
				  }			
			}		
		     });	
		}catch(e)
		{
		 alert(e);	
		}
	}
}

function exitfrompageforEventOnCG(){
	try{$('#myModalPhoneInterview').modal('hide');}catch(e){}
	//getCandidateGrid();
}

function refreshCGonEvent(){
	$('#myModalPhoneInterview').modal('hide');
	getCandidateGrid();
	refreshStatus();
}

function closeListFromCG()
{
	$('#alertMsgForApply').modal('hide');
	$('#myJobDetailListFromCG').modal('hide');
	getCandidateGrid();
	refreshStatus();
}



function resendInvitePopUpConfirmed()
{
	var jobId=$("#jobId").val();
	if($('input[name=cgCBX]:checkbox:checked').length == 0 )
	{
		 $("#alertmsgToShow").html(resourceJSON.PlzSlctAtlstOneCndidate);
		 $('#alertMsgForApply').modal('show');
	}else
	{
		$('#loadingDiv').fadeIn();
		var teacherIds = "";
		$('input[name="cgCBX"]:checked').each(function(){
			var teacherId = $(this).attr("teacherid");//teacherid
			if(teacherIds=="")
				teacherIds=teacherIds+teacherId;
			else
				teacherIds=teacherIds+","+teacherId;
		});
		CandidateGridSubAjax.sendAgainInviteToCandidates(jobId,teacherIds,{ 
			async: false,
			errorHandler:handleError,
			callback: function(data){
				$('#loadingDiv').fadeOut();
				$('#message2showConfirm').html(""+resourceJSON.MsgMailSentSuccessfully+"");
				$('#footerbtn').html("<button class='btn' data-dismiss='modal' aria-hidden='true'>"+resourceJSON.btnOk+"</button>");
				$('#myModal3').modal('show');
		     }
		});		
	}
}

function resendInvitePopUp()
{
	if($('input[name=cgCBX]:checkbox:checked').length == 0 )
	{
		 $("#alertmsgToShow").html(resourceJSON.PlzSlctAtlstOneCndidate);
		 $('#alertMsgForApply').modal('show');
	}else
	{
		$('#message2showConfirm').html(""+resourceJSON.msgConfirmMail+"");
		$('#footerbtn').html("<button class='btn btn-primary' onclick=\"resendInvitePopUpConfirmed()\" >"+resourceJSON.btnOk+"</button> <button class='btn' data-dismiss='modal' aria-hidden='true'>"+resourceJSON.btnCancel+"</button>");
		$('#myModal3').modal('show');
	}

}

function senMultiInvite()
{	
	var teacherIds="";
	var eventid="";
	try
	{
		eventid=document.getElementById("interviewInviteMulti").value;
	}
	catch(e){}
	
	var jobId="";
	$('input[name="cgCBX"]:checked').each(function() {
		
		if($(this).attr("teacherid") !=null && ($(this).attr("teacherid")!="") )
		{
			if(teacherIds=="")
				teacherIds = teacherIds + $(this).attr("teacherid");
			else
				teacherIds = teacherIds + "," + $(this).attr("teacherid");
		}
	});	

	if(teacherIds!="")
	{
		document.getElementById("teacherIdforEvent").value=teacherIds;
		if(eventid!=null && eventid!="" )
		{
			$('#loadingDiv').fadeIn();
			EventAjax.getEventByEventId(eventid, {
				async : true,
				errorHandler : handleError,
				callback : function(data) 
				{				
				$('#myModalPhoneInterview').modal('show');
				clearColorEvents();
				$('#eventName').focus();
				$('#evtsch').tooltip();
				$("html, body").animate({ scrollTop: 0 }, "slow");
				$('#eventArrordiv').empty();
				$('#addEditEventDiv').show();
				$('#facilitatorAndScheduleDiv').hide();
			 	dwr.util.setValues(data);
				$('#msgforParticipantsCG').find(".jqte_editor").html(data.msgtoparticipants);
				$('#messtoFaciliatatorCG').find(".jqte_editor").html(data.msgtofacilitators);
				$('#eventDescriptionCG').find(".jqte_editor").html(data.description);
				getEventSchedule(6);
				var intschedule_Id = document.getElementsByName('intscheduleId');
				$('input[name=intscheduleId][value='+data.eventSchedulelId.eventScheduleId+']').attr('checked', true);
				$('#loadingDiv').hide();
			 }
			});
			displayCommanMethods();			
		}
		else
		{			
		clearColorEvents();
		clearPhoneInterviewData();
		$('#evtsch').tooltip();
		$("html, body").animate({ scrollTop: 0 }, "slow");
		$('#eventArrordiv').empty();
		$('#addEditEventDiv').show();
		$('#facilitatorAndScheduleDiv').hide();	
		getEventSchedule(6);
		getEmailTemplatesFacilitatorsByDistrict(document.getElementById('districtIdforEvent').value);
		getEmailTemplatesByDistrictId(document.getElementById('districtIdforEvent').value);
		try{$('#myModalPhoneInterview').modal('show');}catch(e){}
		$('#eventName').focus();		
		}
	}
	else
	{
		 $("#alertmsgToShow").html(resourceJSON.PlzSlctAtlstOneCndidate);
		 $('#alertMsgForApply').modal('show');
		
	}
	
}

function resendTalentPopUpConfirmed()
{
	var jobId=$("#jobId").val();
	if($('input[name=cgCBX]:checkbox:checked').length == 0 )
	{
		 $("#alertmsgToShow").html(resourceJSON.PlzSlctAtlstOneCndidate);
		 $('#alertMsgForApply').modal('show');
	}else
	{
		$('#loadingDiv').fadeIn();
		var teacherIds = "";
		$('input[name="cgCBX"]:checked').each(function(){
			var teacherId = $(this).attr("teacherid");//teacherid
			if(teacherIds=="")
				teacherIds=teacherIds+teacherId;
			else
				teacherIds=teacherIds+","+teacherId;
		});
		CandidateGridSubAjax.reSendTalentSignUp(jobId,teacherIds,{ 
			async: false,
			errorHandler:handleError,
			callback: function(data){
				$('#loadingDiv').fadeOut();
				$('#message2showConfirm').html(""+resourceJSON.MsgAuthMailSentSuccessfully+"");
				$('#footerbtn').html("<button class='btn' data-dismiss='modal' aria-hidden='true'>"+resourceJSON.btnOk+"</button>");
				$('#myModal3').modal('show');
		     }
		});		
	}
}

function resendTalentPopUp()
{
	if($('input[name=cgCBX]:checkbox:checked').length == 0 )
	{
		 $("#alertmsgToShow").html(resourceJSON.PlzSlctAtlstOneCndidate);
		 $('#alertMsgForApply').modal('show');
	}else
	{
		$('#message2showConfirm').html(""+resourceJSON.msgConfirmAuthMail+"");
		$('#footerbtn').html("<button class='btn btn-primary' onclick=\"resendTalentPopUpConfirmed()\" >"+resourceJSON.btnOk+"</button> <button class='btn' data-dismiss='modal' aria-hidden='true'>"+resourceJSON.btnCancel+"</button>");
		$('#myModal3').modal('show');
	}
}

function checkforSingleBooking(){
	if(document.getElementById("singlebookingonly").checked==true){
		$("#noofcandidateDiv").hide();
		document.getElementById("noofcandidateperslot").value='';
	}else{
		$("#noofcandidateDiv").show();
		document.getElementById("noofcandidateperslot").value='';
	}
}

function deleteSchedulePopUp(scheduleId)
{
	document.getElementById("deleteScheduleId").value=scheduleId;
	$('#succesEmailMsg').html("Do you really want to remove this slot?");
	$('#popupFooter').html("<button class='btn btn-primary' onclick=\"deleteScheduleByIdOnConfirmed()\" >"+resourceJSON.btnOk+"</button> <button class='btn' data-dismiss='modal' aria-hidden='true'>"+resourceJSON.btnCancel+"</button>");
	$('#sendEmailtoParticipants').modal('show');
	
}

function deleteScheduleByIdOnConfirmed()
{
	var scheduleId=document.getElementById("deleteScheduleId").value;
	$("#loadingDiv").fadeIn();	
	EventScheduleAjax.deleteEventScheduleById(scheduleId,{
		async: false,
		callback: function(data)
		{
			$("#loadingDiv").fadeOut();	
			displayEventSchedule();
			//html(""+resourceJSON.MsgMailSentSuccessfully+"");
			$('#succesEmailMsg').html("Slot has removed successfully");
			$('#popupFooter').html("<button class='btn' data-dismiss='modal' aria-hidden='true'>"+resourceJSON.btnOk+"</button>");
			$('#sendEmailtoParticipants').modal('show');
			
		},
		errorHandler:handleError	
	});	
}

function getSchoolList(jobId)
{
	document.getElementById("jobIdSchool").value=jobId; 
	$("#loadingDiv").fadeIn();	
	CandidateGridSubAjax.getSchoolListByJob(jobId,noOfRowsSchool,pageforSchool,sortOrderStrSchool,sortOrderTypeSchool,{
		async: true,
		callback: function(data)
		{
		    $("#loadingDiv").hide();
			$('#myModalforSchholList').modal('show');
			document.getElementById("schoolListDivNew").innerHTML=data;
			applyScrollOnTb();
		},
		errorHandler:handleError	
	});	
}