var page = 1;
var noOfRows = 10;
var sortOrderStr="";
var sortOrderType="";

function getPaging(pageno)
{

	if(pageno!='')
	{
		page=pageno;	
	}
	else
	{
		page=1;
	}
	noOfRows = document.getElementById("pageSize").value;
	getQuestionListByDistrictAndJobCategory();
		
}
function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
	if(pageno!=''){
		page=pageno;	
	}else{
		page=1;
	}
	sortOrderStr	=	sortOrder;
	sortOrderType	=	sortOrderTyp;
	if(document.getElementById("pageSize")!=null){
		noOfRows = document.getElementById("pageSize").value;
	}else{
		noOfRows=10;
	}
	getQuestionListByDistrictAndJobCategory();
}
function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert("Oops: Your Session has expired!");  document.location = 'signin.do';}
	else{alert("Server Error: "+exception.javaClassName);}
}


function displaySLC(){
	
	var headQuarterId=null;
	var branchId=null;
	var districtId=document.getElementById("districtId").value;
	var jobCategoryId=$("#jobCategory option:selected").val();
	var isFomManageNode=true;
	
	
	//alert("districtId:- "+districtId+", jobCategoryId:- "+jobCategoryId);
	$("#loadingDiv").show();
	ManageStatusAjax.displayStatusDashboard(headQuarterId,branchId,districtId,jobCategoryId,isFomManageNode,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
			$("#loadingDiv").hide();
			document.getElementById("statusdashboard").innerHTML=data.toString();	
		}
	});
}

function getQuestionListByDistrictAndJobCategory()	//This method is called while sorting
{
	var secondaryStatusId = $("#secondaryStatusId").val(); 
	showQuestionsGrid(secondaryStatusId);
}
function showQuestionsGrid(secondaryStatusId)
{
	$("#secondaryStatusId").val(secondaryStatusId);
	$("#questionId").val("");
	var districtId=document.getElementById("districtId").value;
	var headQuarterId = "";
	try{
		headQuarterId = document.getElementById("headQuarterId").value;
	}catch(e){}
	var jobCategoryId=$("#jobCategory option:selected").val();
		
	//alert("jobCategoryId:- "+jobCategoryId+"districtId:- "+districtId+"Secondary StatusId:- "+secondaryStatusId);
	
	$("#loadingDiv").show();
	ManageStatusAjax.getQuestionListByDistrictAndJobCategory(headQuarterId,districtId,jobCategoryId,secondaryStatusId,noOfRows,page,sortOrderStr,sortOrderType,true,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
			$("#loadingDiv").hide();
			$("#questionGridDiv").html(data);
			$("#addQuestionDiv").modal("show");
		}
	});
}

function saveOrUpdateQuestion()
{
	if(validateQuestionForm())
	{
		var questionId=null;
		if($("#questionId").val()!="")
			questionId = $("#questionId").val();
		
		var districtId = $("#districtId").val();
		var jobCategoryId = $("#jobCategory option:selected").val();
		var secondaryStatusId = $("#secondaryStatusId").val();
		var question = $("#question").val();
				
		var skillAttributeId=null;
		if($("#skillAttributeId").val()!="")
			skillAttributeId = $("#skillAttributeId").val();
		
		var maxScore=0;
		if($("#score").val()!="")
			maxScore=$("#score").val();
		
		var maxJSIScore=null;
		var isChecked=false;
		var isJSIChecked=false;
		var source="0";
		var strFileName=null;
		var isPanelChecked=false;
		var isPanelCheckedJSI=false;
		var isAutoUpdateStatusChecked=false;
		var checked_site_radio=null;
		var statusUpdateExpiryDate=null;
		var daUsersIds=null;
		var allSchoolAdm=false;
		var overrideAdm=false;
		var chkNotifyAll=false;
		var internalExternalOrBothCandidate=0;
		
		//alert("questionId:- "+questionId+"districtId:- "+districtId+"\njobCategoryId:- "+jobCategoryId+"\nsecondaryStatusId:- "+secondaryStatusId+"\nquestion:- "+question);
		ManageStatusAjax.saveOrUpdateQuestion(questionId,false,districtId,jobCategoryId,secondaryStatusId,question,skillAttributeId, maxScore, maxJSIScore, isChecked, isJSIChecked, source, strFileName, isPanelChecked, isPanelCheckedJSI, isAutoUpdateStatusChecked,  checked_site_radio, statusUpdateExpiryDate, daUsersIds, allSchoolAdm, overrideAdm,chkNotifyAll,internalExternalOrBothCandidate, 
		{
			async: false,
			errorHandler:handleError,
			callback:function(data)
			{
				cancelQuestion();
				showQuestionsGrid(secondaryStatusId);
			}
		});
	}
}

function editQuestion(questionId, isEditableBySchoolAdmin)
{
	$("#questionId").val(questionId);
	
	//alert("isEditableBySchoolAdmin:- "+isEditableBySchoolAdmin);
	if(isEditableBySchoolAdmin=="false")
	{
		$("#addQuestionDiv").modal("hide");
		$("#confirmationPopup").modal("show");
		return true;
	}
	
	$("#loadingDiv").show();
	ManageStatusAjax.editQuestion(questionId,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
			if(data!=null)
			{
				$("#loadingDiv").hide();
				
				showQuestionForm();
				if(data.skillAttributesMaster !=null)
					$("#skillAttributeId").val(data.skillAttributesMaster.skillAttributeId);
				else
					$("#skillAttributeId").val("");
				
				if(data.question!=null)
					$("#question").val(data.question);
				else
					$("#question").val("");
					
				if(data.questionId!=null)
					$("#questionId").val(data.questionId);
				else
					$("#questionId").val("");
					
				if(data.maxScore!=null)
					$("#score").val(data.maxScore);
				else
					$("#score").val("");
				
				if(data.userMaster.entityType!=3)
				{
					$("#addQuestionDiv").modal("hide");
					$("#confirmationPopup").modal("show");
				}
			}
		}
	});
}

function activateDeactivateQuestion(questionId,status,isEditableBySchoolAdmin)
{
	//alert("isEditableBySchoolAdmin (activateDeactivateQuestion):- "+isEditableBySchoolAdmin);
	if(isEditableBySchoolAdmin=="false")
	{
		$("#addQuestionDiv").modal("hide");
		$("#confirmationPopup").modal("show");
		return true;
	}
	
	ManageStatusAjax.activateDeactivateQuestion(questionId,status,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			getQuestionListByDistrictAndJobCategory();
		}
	});
	
}

function confimationOk()
{
	$("#confirmationPopup").modal("hide");
	$("#addQuestionDiv").modal("show");
}

function confimationCancel()
{
	$("#addQuestionDiv").modal("show");
	$("#confirmationPopup").modal("hide");
	
	$("#divQuestionList").hide();
	$('#errorQuestion').empty();
	$("#question").val("");
	$("#questionId").val("");
}

function showQuestionForm()
{
	$("#divQuestionList").show();
	$('#errorQuestion').empty();
	$("#question").val("");
	$("#questionId").val("");
	$("#skillAttributeId").val("");
	$("#score").val("");
	$("#question").focus();
}

function cancelQuestion()
{
	$("#divQuestionList").hide();
	$('#errorQuestion').empty();
	$("#question").val("");
	$("#questionId").val("");
}

function validateQuestionForm()
{
	var error="";
	var question = $("#question").val();
	if(question=="")
		error = error + "<font color='red'>*Please enter Question</font>";
	
	if(error!="")
	{
		$('#errorQuestion').empty();
		$('#errorQuestion').val(error);
	}
	
	if(error=="")
		return true;
	else
		return false;
}