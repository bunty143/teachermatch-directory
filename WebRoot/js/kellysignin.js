var pageEJ= 1;
var noOfRowsEJ= 10;
var sortOrderStrEJ="";
var sortOrderTypeEJ="";
function validateKellySignup(){
	
	var firstName =$.trim($("#fname").val());
	var lastName = $.trim($("#lname").val());
	var emailAddress = $.trim($("#email4signup").val());
	var password = $.trim($("#password4signup").val());
	var jobId = $.trim($("#jobId").val());
	var refType = $.trim($("#refType").val());
	var branchName = null;
	if(document.getElementById("branchName")){
		branchName = $.trim($("#branchName").val())
		$('#branchName').css("background-color","");
	}
	var branchId = null;
	if(document.getElementById("branchId")){
		branchId = $.trim($("#branchId").val());
		$('#branchId').css("background-color","");
	}
	var cnt=0;
	var focs=0;	
	$('#errorDiv4Signup').empty();
	$('#divServerError').css("background-color","");
	
	$('#fname').css("background-color","");
	$('#lname').css("background-color","");
	$('#email4signup').css("background-color","");
	$('#password4signup').css("background-color","");
	if(firstName=="")
	{
		$('#errorDiv4Signup').append("&#149; "+resourceJSON.PlzEtrFirstName+"<br>");
		if(focs==0)
			$('#fname').focus();
		
		$('#fname').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	if(lastName=="")
	{
		$('#errorDiv4Signup').append("&#149; "+resourceJSON.PlzEtrLastName+"<br>");
		if(focs==0)
			$('#lname').focus();
		
		$('#lname').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	if(emailAddress=="")
	{
		$('#errorDiv4Signup').append("&#149; "+resourceJSON.PlzEtrEmail+"<br>");
		if(focs==0)
			$('#email4signup').focus();
		
		$('#email4signup').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	else if(!isEmailAddress(emailAddress))
	{		
		$('#errorDiv4Signup').append("&#149; "+resourceJSON.PlzEtrVldEmail+"<br>");
		if(focs==0)
			$('#email4signup').focus();
		
		$('#email4signup').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	else if(checkEmail(emailAddress))
	{
		$('#errorDiv4Signup').append("&#149; "+resourceJSON.msgaladyemail+"<br>");
		if(focs==0)
			$('#email4signup').focus();
		
		$('#email4signup').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	if(password=="") 
	{
		$('#errorDiv4Signup').append("&#149; "+resourceJSON.msgPassword+"<br>");
		if(focs==0)
			$('#password4signup').focus();
		
		$('#password4signup').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	else if(!chkPwdPtrn(password) || password.length<8 )
	{		
		$('#errorDiv4Signup').append("&#149; "+resourceJSON.msgPassCombination8To12Char+"<br>");
		if(focs==0)
			$('#password4signup').focus();
		
		$('#password4signup').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	if(document.getElementById("branchName"))
	{
		if(branchName==null || branchName=="")
		{
			$('#errorDiv4Signup').append("&#149; Please enter Branch Name<br>");
			if(focs==0)
				$('#branchName').focus();
			
			$('#branchName').css("background-color",txtBgColor);
			cnt++;focs++;
		}
		else if(branchId==null || branchId=="")
		{
			$('#errorDiv4Signup').append("&#149; Please enter valid Branch Name<br>");
			if(focs==0)
				$('#branchName').focus();
			
			$('#branchName').css("background-color",txtBgColor);
			cnt++;focs++;
		}
	}
	if(document.getElementById("captcharesponse").value=="failed" || document.getElementById("captcharesponse").value=='')
	{
		$('#errorDiv4Signup').append("&#149; "+resourceJSON.msgCheckboxCaptchaRequired+" <br>");		
		cnt++;focs++;
	}	
	if(jobId=="")
		jobId = 0;
	
	if(cnt==0)
	{
		KesSignupAjax.doSignup(firstName,lastName,emailAddress,password,branchId , jobId , refType,{
			preHook:function(){$('#loadingDiv').show()},
			postHook:function(){$('#loadingDiv').hide()},
			async: false,
			callback: function(data){
			
			var val = data.split("#");
			if(val[0]=="success" && val[1]=="R"){
				
				$("#myModalMsgShow").modal("show");
				$("#mailInfo").html(val[2]);
			}
			else if(val[0]=="success" && val[1]=="N"){
				
				var encriptBranchId= (branchId*73)+1248;
				window.location = "jobsboard.do?branchId="+encriptBranchId+"&talentRef=N&teacherId="+val[3];
				
			}
		},
		});
	}
	else
	{
		$('#errorDiv4Signup').show();
		return false;
	}
}

function passwordInfo(){
	   var control = document.getElementById("password4signup");
	   var myString= control.value; 
	   var Stringlen = myString.length;
	   var ValidateDigits = /[^0-9]/g;
	   var ValidateSpChar = /[a-zA-Z0-9]/g;
	   var ValidateChar = /[^a-zA-Z]/g;
	   var digitString = myString.replace(ValidateDigits , "");
	   var specialString = myString.replace(ValidateSpChar, "");
	   var charString = myString.replace(ValidateChar, "");


	   if(!specialString < 1 && !digitString < 1 && !charString < 1 && Stringlen>=8 ){
	 	  
	  	   document.getElementById('d8').style.display='none';
	       document.getElementById('d9').style.display='none';
	       document.getElementById('d4').style.backgroundColor = 'lightgreen';
	       document.getElementById('d5').style.backgroundColor = 'lightgreen';
	       document.getElementById('d6').style.display='inline';
	       document.getElementById('d7').style.display='inline';
		   document.getElementById('d10').style.display='inline'; 
		   control.focus();
	   }else if(!charString  < 1 && !digitString < 1 &&Stringlen>=6 ||!charString  < 1  &&!specialString < 1 && Stringlen>=6){
	 	   
	       document.getElementById('d6').style.display='none';
	       document.getElementById('d7').style.display='none';
	       document.getElementById('d10').style.display='none';       
	       document.getElementById('d8').style.display='none';
		   document.getElementById('d4').style.display='inline';
	       document.getElementById('d9').style.display='inline';
	       document.getElementById('d5').style.display='inline';
		   document.getElementById('d4').style.backgroundColor = 'yellow';
	       document.getElementById('d5').style.backgroundColor = 'yellow';
	       
	   
	   }else if( !specialString < 1 || !digitString < 1 || !charString < 1 || !charString && !digitString < 1 && Stringlen<6   ||!charString &&!specialString < 1 && Stringlen<6 ){
	   	 
	   	   document.getElementById('d5').style.display='none';
	  	   document.getElementById('d6').style.display='none';
	       document.getElementById('d7').style.display='none';
	       document.getElementById('d9').style.display='none';
	       document.getElementById('d10').style.display='none';       
		   document.getElementById('d4').style.display='inline';
	       document.getElementById('d4').style.backgroundColor = 'red';
	       document.getElementById('d8').style.display='inline';
		   control.focus();
	   }else if(Stringlen<1){
		   document.getElementById('d4').style.display='none';
		   document.getElementById('d5').style.display='none';
	  	   document.getElementById('d6').style.display='none';
	       document.getElementById('d7').style.display='none';
	  	   document.getElementById('d8').style.display='none';
	       document.getElementById('d9').style.display='none';
	       document.getElementById('d10').style.display='none';       
	      
	   }
	 }

function backToSignUp(){
	window.location="signin.do";
}
function showBranchLookup()
{
	 $('#branchLookupDiv').modal('show');
	 $("#stateIdForDSPQ").val('');
	 $("#keyWord").val('');
	 document.getElementById('gridBranchLookup').style.display='none';   
}
function checkForInt(evt) {

	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==8)
		return true;

	return( (charCode >= 48 && charCode <= 57)||(charCode==13) );
}
function getCityListByStateForDSPQMain(){
	$('#loadingDiv').show();
	var queryString='';
	try{
		var stateId = document.getElementById("stateIdForDSPQ");
		if(stateId!=null && stateId!=""){
		$.ajax({
			url: $('[name="contextPath"]').val()+'/findcitybystate.do',
			type : 'POST',
	        async: false,
        	data : {"stateId" : stateId.value,'dt':new Date().getTime()},
	        success:function(data){
				var optAr = data.split("##");
				var newOption;
				$('#cityIdForDSPQ').html('');
				newOption = document.createElement('option');
			    newOption.text="Select City";
			    newOption.value="";
			    newOption.id="slcty";
			   	document.getElementById('cityIdForDSPQ').options.add(newOption);
				for(var i=0;i<optAr.length-1;i++)
				{
				    newOption = document.createElement('option');
				    newOption.id=optAr[i].split("$$")[0];
				    newOption.value=optAr[i].split("$$")[1];
				    newOption.text=optAr[i].split("$$")[2];
				    document.getElementById('cityIdForDSPQ').options.add(newOption);
				}
				$('#loadingDiv').hide();
			
	        },
	        error:function(e){
	        	alert(e);
	        }
        	
			});
		}		
	}catch(e){}
}
function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
	 if(pageno!=''){
			pageEJ=pageno;	
		}else{
			pageEJ=1;
		}
		sortOrderStrEJ=sortOrder;
		sortOrderTypeEJ=sortOrderTyp;
		{
			noOfRowsEJ=10;
		}
		searchBranchLookup();
}
function searchBranchLookup(){
	var stateId =$.trim($("#stateIdForDSPQ").val());
	var keyWord = $.trim($("#keyWord").val());
			
	KesSignupAjax.searchBranchLookup(stateId,keyWord,noOfRowsEJ,pageEJ,sortOrderStrEJ,sortOrderTypeEJ,{ 
		async: false,
		callback: function(data)
		{
		//alert(data);
		document.getElementById('gridBranchLookup').style.display='block'; 
		$('#gridBranchLookup').html(data);
		applyScrollOnBranchLookup();
		},
	errorHandler:handleError
	});
	
  }

 function selectBranchLookup(branchId,branchName){
	 //alert(branchId);
	 //alert(branchName);
	 document.getElementById('branchName').value=branchName;
	 document.getElementById('branchId').value=branchId;
	 $('#branchLookupDiv').modal('hide');
	 $("#stateIdForDSPQ").val('');
	 $("#keyWord").val('');
	 
 }

 function loginUserValidateForKelly()
 {
 	var emailAddress = document.getElementById("emailAddress1");
 	var password = document.getElementById("password1");
 	
 	var cnt=0;
 	var focs=0;	
 	$('#errordiv').empty();
 	
 	document.getElementById("divServerError").style.display="none";	
 	$('#emailAddress1').css("background-color","");
 	$('#password1').css("background-color","");
 	
 	
 	if(trim(emailAddress.value) == '')
 	{
 		$('#errordiv').append("&#149; "+resourceJSON.PlzEtrEmail+"<br>");
 		if(focs==0)
 			$('#emailAddress1').focus();
 		
 		$('#emailAddress1').css("background-color",txtBgColor);
 		cnt++;focs++;
 	}
 	if(trim(emailAddress.value) != '' && !isEmailAddress(trim(emailAddress.value)))
 	{		
 		$('#errordiv').append("&#149; "+resourceJSON.PlzEtrVldEmail+"<br>");
 		if(focs==0)
 			$('#emailAddress1').focus();
 		$('#emailAddress1').css("background-color", txtBgColor);
 		cnt++;focs++;
 	}
 	if(trim(password.value) == '')
 	{
 		$('#errordiv').append("&#149; "+resourceJSON.msgPassword+"<br>");
 		if(focs==0)
 			$('#password1').focus();
 		$('#password1').css("background-color",txtBgColor);
 		cnt++;focs++;
 	}
 	
 	if(cnt==0)
 		return true;
 	else
 	{
 		$('#errordiv').show();
 		return false;
 	}
 }