/* @Author: Gagan */
/*========  For Handling session Time out Error ===============*/

var exportIconsDiv=false;
var deviceTypeAndroid=$.browser.device = (/Android|webOS|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent.toLowerCase()));
var deviceType=$.browser.device = (/iPhone|iPad|iPod/i.test(navigator.userAgent.toLowerCase()));
var flagvalue='';
var page = 1;
var noOfRows = 20;
var sortOrderStr="";
var sortOrderType="";
var pageForTC = 1;
var sortOrderStrForTC="";
var sortOrderTypeForTC="";
var noOfRowsForTC = 100;
var noOfRowsForJob = 50;
var teacherId='';
var currentPageFlag="";
var jobForTeacherGId=0;

//open School Div
var pageforSchool		=	1;
var noOfRowsSchool 		=	50;
var sortOrderStrSchool	=	"";
var sortOrderTypeSchool	=	"";

var pageSTT = 1;
var sortOrderStrSTT ="";
var sortOrderTypeSTT ="";
var noOfRowsSTT = 10;

var pageUS = 1;
var sortOrderStrUS="";
var sortOrderTypeUS ="";
var noOfRowsUS = 10;
var teachingScore='';
var norScore='';
var CGPASlid='';
var fitScoreSlid='';
var aScoreSlid='';
var lrSlid='';

//Start for Profilr Div Grid

//videoLink
var pagevideoLink = 1;
var sortOrderStrvideoLink="";
var sortOrderTypevideoLink ="";
var noOfRowsvideoLink= 10;

//References
var pageReferences = 1;
var sortOrderStrReferences="";
var sortOrderTypeReferences ="";
var noOfRowsReferences= 10;

//workExp
var pageworkExp = 1;
var sortOrderStrworkExp="";
var sortOrderTypeworkExp ="";
var noOfRowsworkExp= 10;

//tpvh - teacherprofilevisithistory
var pagetpvh = 1;
var sortOrderStrtpvh="";
var sortOrderTypetpvh ="";
var noOfRowstpvh= 10;

//teacherAce
var pageteacherAce = 1;
var sortOrderStrteacherAce="";
var sortOrderTypeteacherAce ="";
var noOfRowsteacherAce= 10;

//teacherCerti
var pageteacherCerti = 1;
var sortOrderStrteacherCerti="";
var sortOrderTypeteacherCerti ="";
var noOfRowsteacherCerti= 10;

//End for Profilr Div Grid


//teacherCerti
var pageJobDetail = 1;
var sortOrderStrJobDetail="";
var sortOrderTypeJobDetail ="";
var noOfRowsJobDetail=50;

//zone schools 
var pageZS = 1;
var noOfRowsZS = 10;
var sortOrderStrZS="";
var sortOrderTypeZS="";


//Epi Jsi
var pageEJ= 1;
var noOfRowsEJ= 10;
var sortOrderStrEJ="";
var sortOrderTypeEJ="";

//for addtional documents
var pageaddDoc= 1;
var noOfRowsaddDoc= 10;
var sortOrderStraddDoc="";
var sortOrderTypeaddDoc="";

//for language profiency
var pageLang= 1;
var noOfRowsLang= 10;
var sortOrderStrLang="";
var sortOrderTypeLang="";



//For Assessment
var pageASMT 				= 	1;
var noOfRowsASMT			=	10;
var sortOrderStrASMT		=	"";
var sortOrderTypeASMT 		= 	10;

//for Involvement
var dp_Involvement_Rows=10;
var dp_Involvement_page=1;
var dp_Involvement_sortOrderStr="";
var dp_Involvement_sortOrderType="";

////////////
var teacherID="";
var exportType="";	//shadab


var countContactFlag=0;
function numericFilter(txb) {  
   txb.value = txb.value.replace(/[^\0-9]/ig, "");  
   if(!txb.value)
   {   
   if(countContactFlag==0)
   $('#errordivElectronicReferences').append("&#149; "+resourceJSON.errorMsgForCandidate+"<br>");
   
   countContactFlag++;
   }
}


function defaultSaveDiv(){
	pageSTT = 1;
	sortOrderStrSTT ="";
	sortOrderTypeSTT ="";
	noOfRowsSTT = 10;
	currentPageFlag="stt";
}
function defaultShareDiv(){
	pageUS = 1;
	sortOrderStrUS="";
	sortOrderTypeUS ="";
	noOfRowsUS = 10;
	currentPageFlag="us";
}
function getPaging(pageno)
{	
	 if(pageFlag==1){
		 if(pageno!='')
			{
			 	pageZS=pageno;	
			}	
			else
			{
				pageZS=1;
			}
		 	noOfRowsZS = document.getElementById("pageSizeZoneSchool").value;
			displayZoneSchoolList();
	 }
	 if(currentPageFlag=="stt"){
				if(pageno!='')
				{
					pageSTT=pageno;	
				}
				else
				{
					pageSTT=1;
				}
				noOfRowsSTT = document.getElementById("pageSize3").value;
				var folderId = document.getElementById("folderId").value;
				var checkboxshowHideFlag= document.getElementById("checkboxshowHideFlag").value;
				displaySavedCGgridByFolderId(0,folderId,checkboxshowHideFlag);
		}else if(currentPageFlag=="job"){
			if(pageno!='')
			{
				pageForTC=pageno;	
			}
			else
			{
				pageForTC=1;
			}
			noOfRowsForJob = document.getElementById("pageSize1").value;
			getJobOrderList();
		}
		else if(currentPageFlag=="workExp"){
			if(pageno!='')
			{
				pageworkExp=pageno;	
			}
			else
			{
				pageworkExp=1;
			}
			noOfRowsworkExp = document.getElementById("pageSize11").value;
			var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
			getPFEmploymentGrid_DivProfile(teacherIdForprofileGrid);
		}
		else if(currentPageFlag=="References"){
			if(pageno!='')
			{
				pageReferences=pageno;	
			}
			else
			{
				pageReferences=1;
			}
			noOfRowsReferences = document.getElementById("pageSize12").value;
			var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
			getElectronicReferencesGrid_DivProfile(teacherIdForprofileGrid)
			
		}
		else if(currentPageFlag=="videoLink"){
			if(pageno!='')
			{
				pagevideoLink=pageno;
			}
			else
			{
				pagevideoLink=1;
			}
			noOfRowsvideoLink = document.getElementById("pageSize13").value;
			var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
			getVideoLinksGrid_DivProfile(teacherIdForprofileGrid);
		}
		else if(currentPageFlag=="document"){
			if(pageno!='')
			{
				pageaddDoc=pageno;
			}
			else
			{
				pageaddDoc=1;
			}
			noOfRowsaddDoc = document.getElementById("pageSize15").value;
			var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
			getadddocumentGrid_DivProfile(teacherIdForprofileGrid);
		}else if(currentPageFlag=="languageProfiency"){
			if(pageno!='')
			{
				pageLang=pageno;
			}
			else
			{
				pageLang=1;
			}
			noOfRowsLang = document.getElementById("pageLangProf").value;
			var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
			getlanguage_DivProfile(teacherIdForprofileGrid);
		}
		else if(currentPageFlag=="tpvh"){
			if(pageno!='')
			{
				pagetpvh=pageno;
			}
			else
			{
				pagetpvh=1;
			}
			noOfRowstpvh = document.getElementById("pageSize14").value;
			var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
			getTeacherProfileVisitHistoryShow(teacherIdForprofileGrid);
		}
		else if(currentPageFlag=="teacherAce"){
			if(pageno!='')
			{
				pageteacherAce=pageno;
			}
			else
			{
				pageteacherAce=1;
			}
			noOfRowsteacherAce = document.getElementById("pageSize10").value;
			var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
			getTeacherAcademicsGrid_DivProfile(teacherIdForprofileGrid);
		}
		else if(currentPageFlag=="teacherCerti"){
			if(pageno!='')
			{
				pageteacherCerti=pageno;
			}
			else
			{
				pageteacherCerti=1;
			}
			noOfRowsteacherCerti = document.getElementById("pageSize9").value;
			var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
			getTeacherCertificationsGrid_DivProfile(teacherIdForprofileGrid);
		}
		else if(currentPageFlag=="us")
		{
			if(pageno!='')
			{
				pageUS=pageno;	
			}
			else
			{
				pageUS=1;
			}
			noOfRowsUS = document.getElementById("pageSize3").value;
			
			var userFPS=document.getElementById("userFPS").value;
			var userFPD=document.getElementById("userFPD").value;
			var userCPS=document.getElementById("userCPS").value;
			var userCPD=document.getElementById("userCPD").value;
			if(userFPS==1){
				searchUser(0);
			}else if(userFPD==1){
				displayUsergrid(teacherID,1);
			}else if(userCPS==1){
				searchUserthroughPopUp(0);
			}else if(userCPD==1){
				displayShareFolder();
			}
		}
		else if(currentPageFlag=="jobDetail")
		{
			if(pageno!='')
			{
				pageJobDetail=pageno;	
			}
			else
			{
				pageJobDetail=1;
			}
			noOfRowsJobDetail = document.getElementById("pageSize12").value;
			
			getJobDetailList($("#tId4Job").val());

		}else if(currentPageFlag=="zone")
		{	
			if(pageno!='')
			{	
				pageZS=pageno;	
			}
			else
			{	
				pageZS=1;
			}
			noOfRowsZS = document.getElementById("pageSizeZoneSchool").value;
			displayZoneSchoolList();
		} else if(currentPageFlag=="referenceCheck"){
			if(pageno!='')
			{
				pageRefChk=pageno;	
			}
			else
			{
				pageRefChk=1;
			}
			teacherId 		= document.getElementById("teacherIdForReference").value;
			teacherDetails	= document.getElementById("teacherDetailsForReference").value;
			noOfRowsRefChk 	= document.getElementById("pageSize3").value;
			getReferenceCheck(teacherDetails,teacherId);
		}else if(currentPageFlag=="assessmentDetails"){
			if(pageno!='')
			{
				pageASMT=pageno;	
			}
			else
			{
				pageASMT=1;
			}
			teacherId 		= document.getElementById("teacherIdForReference").value;
		//	teacherDetails	= document.getElementById("teacherDetailsForReference").value;
			noOfRowsASMT 	= document.getElementById("pageSize3").value;
			getdistrictAssessmetGrid_DivProfile(teacherId);
		}  
		else if(currentPageFlag=="teacherInvolve"){
			if(pageno!='')
			{
				dp_Involvement_page=pageno;	
			}
			else
			{
				dp_Involvement_page=1;
			}
			teacherId 		= document.getElementById("teacherIdForReference").value;
		//	teacherDetails	= document.getElementById("teacherDetailsForReference").value;
			dp_Involvement_Rows 	= document.getElementById("pageSize16").value;
			getInvolvementGrid(teacherId);
			//getdistrictAssessmetGrid_DivProfile(teacherId);
		} 
		else
		{
			if(pageno!='')
			{
				page=pageno;	
			}
			else
			{
				page=1;
			}
		
			noOfRows = document.getElementById("pageSize").value;
			displayTeacherGrid();
		}
}
function defaultTeacherGrid(){
	page = 1;
	noOfRows = 20;
	sortOrderStr="";
	sortOrderType="";
	currentPageFlag="";
}
function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
	if(currentPageFlag=="tran" || currentPageFlag=="cert"  || currentPageFlag=="job"){
		if(pageno!=''){
			pageForTC=pageno;	
		}else{
			pageForTC=1;
		}
		sortOrderStrForTC	=	sortOrder;
		sortOrderTypeForTC	=	sortOrderTyp;
		if(document.getElementById("pageSize")!=null){
			noOfRowsTranCert = document.getElementById("pageSize").value;
		}else{
			noOfRowsTranCert=100;
		}
		if(currentPageFlag=="job"){
			getJobOrderList();
		}else if(currentPageFlag=="tran"){
			getTranscriptGrid();
		}else{
			getCertificationGrid();
		}
	}
	else if(currentPageFlag=="epiJsi")
	{
		if(pageno!=''){
			pageEJ=pageno;	
		}else{
			pageEJ=1;
		}
		sortOrderStrEJ=sortOrder;
		sortOrderTypeEJ=sortOrderTyp;
		if(document.getElementById("pageSize")!=null){
			noOfRowsEJ = document.getElementById("pageSize").value;
		}else{
			noOfRowsEJ=10;
		}
		showEpiAndJsi1();
	}
	else if(currentPageFlag=="stt"){
		if(pageno!=''){
			pageSTT=pageno;	
		}else{
			pageSTT=1;
		}
		sortOrderStrSTT	=	sortOrder;
		sortOrderTypeSTT	=	sortOrderTyp;
		if(document.getElementById("pageSize3")!=null){
			noOfRowsSTT = document.getElementById("pageSize3").value;
		}else{
			noOfRowsSTT=10;
		}
		var folderId = document.getElementById("folderId").value;
		var checkboxshowHideFlag= document.getElementById("checkboxshowHideFlag").value;
		displaySavedCGgridByFolderId(0,folderId,checkboxshowHideFlag);
	}else if(currentPageFlag=="us"){
		if(pageno!=''){
			pageUS=pageno;	
		}else{
			pageUS=1;
		}
		sortOrderStrUS	=	sortOrder;
		sortOrderTypeUS	=	sortOrderTyp;
		if(document.getElementById("pageSize3")!=null){
			noOfRowsUS = document.getElementById("pageSize3").value;
		}else{
			noOfRowsUS=10;
		}
		var userFPS=document.getElementById("userFPS").value;
		var userFPD=document.getElementById("userFPD").value;
		var userCPS=document.getElementById("userCPS").value;
		var userCPD=document.getElementById("userCPD").value;
		if(userFPS==1){
			searchUser(0);
		}else if(userFPD==1){
			displayUsergrid(0,1);
		}else if(userCPS==1){
			searchUserthroughPopUp(0);
		}else if(userCPD==1){
			displayShareFolder();
		}
	}
	else if(currentPageFlag=="workExp"){
		if(pageno!=''){
			pageworkExp=pageno;	
		}else{
			pageworkExp=1;
		}
		sortOrderStrworkExp	=	sortOrder;
		sortOrderTypeworkExp	=	sortOrderTyp;
		if(document.getElementById("pageSize11")!=null){
			noOfRowsworkExp = document.getElementById("pageSize11").value;
		}else{
			noOfRowsworkExp=10;
		}
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getPFEmploymentGrid_DivProfile(teacherIdForprofileGrid);
	}else if(currentPageFlag=="References"){
		if(pageno!=''){
			pageReferences=pageno;	
		}else{
			pageReferences=1;
		}
		sortOrderStrReferences	=	sortOrder;
		sortOrderTypeReferences	=	sortOrderTyp;
		if(document.getElementById("pageSize12")!=null){
			noOfRowsReferences = document.getElementById("pageSize12").value;
		}else{
			noOfRowsReferences=10;
		}
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getElectronicReferencesGrid_DivProfile(teacherIdForprofileGrid);
	}else if(currentPageFlag=="videoLink"){
		if(pageno!=''){
			pagevideoLink=pageno;	
		}else{
			pagevideoLink=1;
		}
		sortOrderStrvideoLink	=	sortOrder;
		sortOrderTypevideoLink	=	sortOrderTyp;
		if(document.getElementById("pageSize13")!=null){
			noOfRowsvideoLink = document.getElementById("pageSize13").value;
		}else{
			noOfRowsvideoLink=10;
		}
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getVideoLinksGrid_DivProfile(teacherIdForprofileGrid);
	}
	else if(currentPageFlag=="document"){
		if(pageno!=''){
			pageaddDoc=pageno;	
		}else{
			pageaddDoc=1;
		}
		sortOrderStraddDoc	=	sortOrder;
		sortOrderTypeaddDoc	=	sortOrderTyp;
		if(document.getElementById("pageSize15")!=null){
			noOfRowsaddDoc = document.getElementById("pageSize15").value;
		}else{
			noOfRowsaddDoc=10;
		}
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;		
		getadddocumentGrid_DivProfile(teacherIdForprofileGrid);
	}else if(currentPageFlag=="languageProfiency"){
		if(pageno!=''){
			pageLang=pageno;	
		}else{
			pageLang=1;
		}
		sortOrderStrLang	=	sortOrder;
		sortOrderTypeLang	=	sortOrderTyp;
		if(document.getElementById("pageLangProf")!=null){
			noOfRowsLang = document.getElementById("pageLangProf").value;
		}else{
			noOfRowsLang=10;
		}
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;		
		getlanguage_DivProfile(teacherIdForprofileGrid);
	}else if(currentPageFlag=="tpvh"){
		if(pageno!=''){
			pagetpvh=pageno;	
		}else{
			pagetpvh=1;
		}
		sortOrderStrtpvh	=	sortOrder;
		sortOrderTypetpvh	=	sortOrderTyp;
		if(document.getElementById("pageSize14")!=null){
			noOfRowstpvh = document.getElementById("pageSize14").value;
		}else{
			noOfRowstpvh=10;
		}
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getTeacherProfileVisitHistoryShow(teacherIdForprofileGrid);
	}else if(currentPageFlag=="teacherAce"){
		if(pageno!=''){
			pageteacherAce=pageno;	
		}else{
			pageteacherAce=1;
		}
		sortOrderStrteacherAce	=	sortOrder;
		sortOrderTypeteacherAce	=	sortOrderTyp;
		if(document.getElementById("pageSize10")!=null){
			noOfRowsteacherAce = document.getElementById("pageSize10").value;
		}else{
			noOfRowsteacherAce=10;
		}
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getTeacherAcademicsGrid_DivProfile(teacherIdForprofileGrid);
	}else if(currentPageFlag=="teacherCerti"){
		if(pageno!=''){
			pageteacherCerti=pageno;	
		}else{
			pageteacherCerti=1;
		}
		sortOrderStrteacherCerti	=	sortOrder;
		sortOrderTypeteacherCerti	=	sortOrderTyp;
		if(document.getElementById("pageSize10")!=null){
			noOfRowsteacherCerti = document.getElementById("pageSize9").value;
		}else{
			noOfRowsteacherCerti=10;
		}
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getTeacherCertificationsGrid_DivProfile(teacherIdForprofileGrid);
	}
	else if(currentPageFlag=="jobDetail")
	{
		if(pageno!=''){
			pageJobDetail=pageno;	
		}else{
			pageJobDetail=1;
		}
		sortOrderStrJobDetail	=	sortOrder;
		sortOrderTypeJobDetail	=	sortOrderTyp;
		if(document.getElementById("pageSize12")!=null){
			noOfRowsJobDetail = document.getElementById("pageSize12").value;
		}else{
			noOfRowsJobDetail=10;
		}
		
		getJobDetailList($("#tId4Job").val());
	}else if(currentPageFlag=="zone")
	{	
		if(pageno!=''){
			pageZS=pageno;	
		}else{
			pageZS=1;
		}
		sortOrderStrZS=sortOrder;
		sortOrderTypeZS=sortOrderTyp;
		if(document.getElementById("pageSizeZoneSchool")!=null){
			noOfRowsZS = document.getElementById("pageSizeZoneSchool").value;
		}else{
			noOfRowsZS=100;
		}
		displayZoneSchoolList();
	}else if(currentPageFlag=="assessmentDetails"){
		if(pageno!=''){
			pageASMT=pageno;	
		}else{
			pageASMT=1;
		}
		sortOrderStrASMT	=	sortOrder;
		sortOrderTypeASMT	=	sortOrderTyp;
		if(document.getElementById("pageSize15")!=null){
			noOfRowsASMT = document.getElementById("pageSize15").value;
		}else{
			noOfRowsASMT=10;
		}
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getdistrictAssessmetGrid_DivProfile(teacherIdForprofileGrid);
	}	
	else if(currentPageFlag=="teacherInvolve"){
		if(pageno!=''){
			dp_Involvement_page=pageno;	
		}else{
			dp_Involvement_page=1;
		}
		dp_Involvement_sortOrderStr	=	sortOrder;
		dp_Involvement_sortOrderType	=	sortOrderTyp;
		if(document.getElementById("pageSize16")!=null){
			dp_Involvement_Rows = document.getElementById("pageSize16").value;
		}else{
			dp_Involvement_Rows=10;
		}
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getInvolvementGrid(teacherIdForprofileGrid);
		//getdistrictAssessmetGrid_DivProfile(teacherIdForprofileGrid);
	}
	else if(currentPageFlag=="teacherAssessment"){
		if(pageno!=''){
			pageEJ=pageno;	
		}else{
			pageEJ=1;
		}
		sortOrderStrEJ=sortOrder;
		sortOrderTypeEJ=sortOrderTyp;
		if(document.getElementById("pageSize")!=null){
			noOfRowsEJ = document.getElementById("pageSize").value;
		}else{
			noOfRowsEJ=10;
		}
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getTeacherAssessmentGrid_DivProfile(teacherIdForprofileGrid);
	}
	else
	{
		if(pageno!=''){
			page=pageno;	
		}else{
			page=1;
		}
		sortOrderStr=sortOrder;
		sortOrderType=sortOrderTyp;
		if(document.getElementById("pageSize")!=null){
			noOfRows = document.getElementById("pageSize").value;
		}else{
			noOfRows=100;
		}
		displayTeacherGrid();
	}
}

function setFocus(id)
{
	if(id!="")
		document.getElementById(id).focus();
}

function isEmailAddress(str) 
{
	var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
	return emailPattern.test(str);
}

function showMiamiPdf()
{
	  var left = (screen.width/2)-(600/2);
	  var top = (screen.height/2)-(700/2);
	  return window.open("http://jobs.dadeschools.net/teachers/pdf/Geo_Zones_With_Schools.pdf", "Miami Geo Zone", "width=600, height=600,top="+top+",left="+left);
}
function defaultSet(){
	pageForTC = 1;
	noOfRowsForTC =100;
	noOfRowsForJob=50;
	sortOrderStrForTC="";
	sortOrderTypeForTC="";
	currentPageFlag="";
}
function setZIndexActDiv()
{
	$("#myModalAct").css({"z-index":"3000"});
}
function downloadCertificationforCG(certId, linkId,teaId)
{		
	    TeacherProfileViewInDivAjax.downloadCertificationforCG(certId,teaId,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data){
			if(data=="")
			{
				data="javascript:void(0)";
			}
			else
			{
				if(deviceType)
				{
					if (data.indexOf(".doc")!=-1)
					{
						$("#docfileNotOpen").css({"z-index":"3000"});
				    	$('#docfileNotOpen').show();
					}
					else if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
					{
						$("#exelfileNotOpen").css({"z-index":"3000"});
				    	$('#exelfileNotOpen').show();
					}
					else
					{
						document.getElementById(linkId).href = data;	
					}
				}
				else if(deviceTypeAndroid)
				{
					if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
					{
						document.getElementById("ifrmTrans").src=data;
					}
					else
					{
						document.getElementById(linkId).href = data;	
					}	
				}
				else if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					document.getElementById("ifrmTrans").src=data;
				}
				else
				{
					document.getElementById(linkId).href = data;	
				}			
			}	
			return false;			
			
		}});
}



function getPhoneDetailShow(teacherId){
	//CandidateReportAjax
	CGServiceAjax.getPhoneDetail(teacherId,
	{
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			if(data==""){
				document.getElementById("divAlertText").innerHTML=resourceJSON.MsgPhNoIsNotAvailable;
				try
				{
					$('#divAlert').modal('show');
				}
				catch(err){}
			}
			else{
				document.getElementById("divPhone").innerHTML=data;	
				try
				{
					$('#myModalPhoneShow').modal('show');
				}
				catch(err){}
				
			}
			
		}
	});
}

/* ==== Gagan : Function For Advance Search=========*/
function displayAdvanceSearch()
{
	$('#searchLinkDiv').fadeOut();
	$('#advanceSearchDiv').slideDown('slow');
	//$('#advanceSearchDiv').toggle( "slide", { direction: "top" }, 1000);
	
	//sliderNormScoreTP
	var iFrmId="ifrmNorm";
	var name="normScoreFrm";
	var tickInterval="10";
	var max="100";
	var swidth="360";
	if(norScore!='')
	{
		var svalue=norScore;
	}
	else
	{
		var svalue="0";
	}
	
	var style="padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top; height: 40px; width: 410px; margin-top: -10px;";
	CGServiceAjax.getSlider(iFrmId,name,tickInterval,max,swidth,svalue,style,{ 
	async: false,
	callback: function(data)
	{
		$('#sliderNormScoreTP').html(data.toString())
	},
	errorHandler:handleError
	});
	
	var iFrmId1="ifrmCGPA";
	var name1="CGPAFrm";
	var tickInterval1="1";
	var max1="5";
	var swidth1="356";
	if(CGPASlid!='')
	{
		var svalue1=CGPASlid;
	}
	else
	{
		var svalue1="0";
	}
	
	var style1="padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top; height: 40px; width: 410px; margin-top: -10px;";
	CGServiceAjax.getSlider(iFrmId1,name1,tickInterval1,max1,swidth1,svalue1,style1,{ 
	async: false,
	callback: function(data)
	{
		$('#sliderCGPATP').html(data.toString())
	},
	errorHandler:handleError
	});
	
	var iFrmId2="ifrmFit";
	var name2="fitScoreFrm";
	var tickInterval2="20";
	var max2="260";
	var swidth2="360";
	if(fitScoreSlid!='')
	{
		var svalue2=fitScoreSlid;
	}
	else
	{
		var svalue2="0";
	}
	
	var style2="padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top; height: 40px; width: 410px; margin-top: -10px;";
	CGServiceAjax.getSlider(iFrmId2,name2,tickInterval2,max2,swidth2,svalue2,style2,{ 
	async: false,
	callback: function(data)
	{
		$('#sliderFitScoreTP').html(data.toString())
	},
	errorHandler:handleError
	});
	
	
	var iFrmId3="ifrmYOTE";
	var name3="YOTEFrm";
	var tickInterval3="5";
	var max3="50";
	var swidth3="356";
	
	if(teachingScore!='')
	{
		var svalue3=teachingScore;
	}
	else
	{
	   var svalue3="0";
	}
	var style3="border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top; height: 40px; width: 410px; margin-top: -10px;";
	CGServiceAjax.getSlider(iFrmId3,name3,tickInterval3,max3,swidth3,svalue3,style3,{ 
	async: false,
	callback: function(data)
	{
		
		$('#sliderYOTETP').html(data.toString())
	},
	errorHandler:handleError
	});
	
	var iFrmId4="ifrmAScore";
	var name4="AScoreFrm";
	var tickInterval4="5";
	var max4="20";
	var swidth4="360";
	if(aScoreSlid!='')
	{
		var svalue4=aScoreSlid;
	}
	else
	{
		var svalue4="0";
	}
	
	var style4="padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top; height: 40px; width: 410px; margin-top: -10px;";
	CGServiceAjax.getSlider(iFrmId4,name4,tickInterval4,max4,swidth4,svalue4,style4,{ 
	async: false,
	callback: function(data)
	{
		$('#sliderAScoreTP').html(data.toString())
	},
	errorHandler:handleError
	});
	
	
	var iFrmId5="ifrmLRScore";
	var name5="LRScoreFrm";
	var tickInterval5="5";
	var max5="20";
	var swidth5="356";
	
	if(lrSlid!='')
	{
		var svalue5=lrSlid;
	}
	else
	{
		var svalue5="0";
	}
	
	var style5="border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top; height: 40px; width: 410px; margin-top: -10px;";
	CGServiceAjax.getSlider(iFrmId5,name5,tickInterval5,max5,swidth5,svalue5,style5,{ 
	async: false,
	callback: function(data)
	{
		$('#sliderLRScoreTP').html(data.toString())
	},
	errorHandler:handleError
	});
}
function getPictureDetail(teacherId){
		TeacherInfotAjax.showPictureWeb(teacherId,{ 
		async: false,
		callback: function(data)
		{
			if(data==""){
				document.getElementById("divPicture").setAttribute("align", "left");
				document.getElementById("divPicture").innerHTML=resourceJSON.msgNoPicforCandidate;
				$('#myModalPicture').modal('show');
			}
			else{
				document.getElementById("divPicture").setAttribute("align", "center");
				document.getElementById("divPicture").innerHTML="<img src=\""+data+"\" align='middle' >";	
				//document.getElementById("teacherPicture").innerHTML="<img src=\""+data+"\"  >";
				$('#myModalPicture').modal('show');
			}
		},
		errorHandler:handleError 
	});
}

function downloadResume(teacherId,linkId)
{
	$('#loadingDiv').show();
	//CandidateReportAjax
	CGServiceAjax.downloadResume(teacherId,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			$('#loadingDiv').hide();
			if(data=="")
			{			
				alert(resourceJSON.ResumeNotUploaded)
			}
			else
			{
				if(deviceType)
				{
					if (data.indexOf(".doc")!=-1)
					{
						$("#docfileNotOpen").css({"z-index":"3000"});
				    	$('#docfileNotOpen').show();
					}
					else if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
					{
						$("#exelfileNotOpen").css({"z-index":"3000"});
				    	$('#exelfileNotOpen').show();
					}
					else
					{
						if($("#"+linkId+''+teacherId).length>0){
							document.getElementById(linkId+''+teacherId).href = data;							 
						}else{
							document.getElementById(linkId).href = data; 	
						}
					}
				}
				else if(deviceTypeAndroid)
				{
					if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
					{
						document.getElementById('ifrmTrans').src = ""+data+"";
					}
					else
					{
						if($("#"+linkId+''+teacherId).length>0){
							document.getElementById(linkId+''+teacherId).href = data;							 
						}else{
							document.getElementById(linkId).href = data; 	
						}
					}	
				}
				else if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					document.getElementById('ifrmTrans').src = ""+data+"";
				}
				else
				{
					if($("#"+linkId+''+teacherId).length>0){
							document.getElementById(linkId+''+teacherId).href = data;							 
						}else{
							document.getElementById(linkId).href = data; 	
						}
				}			
			}				
			return false;
		}
	});
}

function downloadTranscript(academicId,linkid)
{	  
	//CandidateReportAjax
	CGServiceAjax.downloadTranscript(academicId,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
			if(deviceType)
			{
				if (data.indexOf(".doc")!=-1)
				{
					$("#docfileNotOpen").css({"z-index":"3000"});
			    	$('#docfileNotOpen').show();
				}
				else if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					$("#exelfileNotOpen").css({"z-index":"3000"});
			    	$('#exelfileNotOpen').show();
				}
				else
				{
					document.getElementById(linkid).href = data;
				}
			}
			else if(deviceTypeAndroid)
			{
				if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					 $('#modalDownloadsTranscript').modal('hide');
					 document.getElementById('ifrmTrans').src = ""+data+"";
				}
				else
				{
					document.getElementById(linkid).href = data;
				}
			}
			else if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
			{
				 $('#modalDownloadsTranscript').modal('hide');
				 document.getElementById('ifrmTrans').src = ""+data+"";
			}
			else
			{
				$('#myModalTranscript').modal('hide');
				document.getElementById('ifrmTrans').src = ""+data+"";				
				$("#myModalTranscript").css({"z-index":"1000"});
				try{
					$('#modalDownloadsTranscript').modal('show');
				}catch(err)
				  {}
			}
	  }
	});
}

function setZIndexTrans(){
//	document.getElementById('ifrmTrans').src = "a.html";
//	$("#myModalTranscript").css({"z-index":"2000"});	
//	$("#myModalCertification").css({"z-index":"2000"});	
//	$('#myModalTranscript').modal('show');
}
function downloadPortfolioReport(teacherId, linkId)
{	
	$('#loadingDiv').show();
	//CandidateReportAjax
	CGServiceAjax.downloadPortfolioReport(teacherId,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#loadingDiv').hide();
			if(data=="")
			{
					document.getElementById(linkId).href = "javascript:void(0);";
					alert(resourceJSON.MsgPortfolioNotCompleted)
					return true;
			}
			else	
			{
				document.getElementById(linkId).href = data; 
				return false;
			}
				
		}
	});
}
function checkBaseCompeletedForPDR(teacherId,hrefId)
{
	//CandidateReportAjax
	CGServiceAjax.checkBaseCompeleted(teacherId,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){

		if(data==1)
		{
			generatePDReport(teacherId,hrefId);
			return false;

		}else if(data==3)
		{
			$('#message2show').html(resourceJSON.CandidateTimeOutBaseInvevtory);
			$('#myModal2').modal('show');
			return false;
		}
		else
		{
			$('#message2show').html(resourceJSON.CandidateHasNotComplete);
			$('#myModal2').modal('show');
			return false;
		}
	}
	});	

}
// ADD PDP Configuratioin 
function openDistrictlist(){
	var modalDownloadPflag=true;
	var errorflag=true;
	$('#errorDivDistrictlist').empty();
	$('#modalDistrictDetails').css("background-color", "");
	var districtDetail = document.getElementById("modalDistrictDetails");
	var districtId = districtDetail.options[districtDetail.selectedIndex].value;
	var districtName = districtDetail.options[districtDetail.selectedIndex].text;
	if(districtDetail.options[districtDetail.selectedIndex].text=="Select District"){
		$('#errorDivDistrictlist').show();
		$('#errorDivDistrictlist').append("&#149; "+"Select District"+"<br>");
    	$('#modalDistrictDetails').focus();
    	$('#modalDistrictDetails').css("background-color", "#F5E7E1");
    	errorflag=false;
    	return false;
	}
	$("#selectDistrictDiv").modal("hide");
	var teacherId=document.getElementById("districtlistdivteacherid").value;
	CGServiceAjax.generatePDPReportForMultipleJobApplyDistrict(districtId,teacherId,{		
		async: false,
		errorHandler:handleError,
		callback: function(data){
			if(deviceType)
			{	
				if (data.indexOf(".doc")!=-1)
				{
					$("#docfileNotOpen").css({"z-index":"3000"});
			    	$('#docfileNotOpen').show();
				}
				else if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					$("#exelfileNotOpen").css({"z-index":"3000"});
			    	$('#exelfileNotOpen').show();
				}
				else
				{	
					$('#modalDownloadPDR').modal('hide');
					$("#draggableDivMaster").modal('show');	
					document.getElementById(hrefId).href = data;
				
				}
			}
			else if(deviceTypeAndroid)
			{
				if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					    $('#modalDownloadPDR').modal('hide');
					    document.getElementById('ifrmPDR').src = ""+data+"";
				}
				else
				{
					$('#modalDownloadPDR').modal('hide');
					$("#draggableDivMaster").modal('show');	
					document.getElementById(hrefId).href = data;
				}	
			}
			else if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
			{
				    $('#modalDownloadPDR').modal('hide');
				    document.getElementById('ifrmPDR').src = ""+data+"";
			}
			else
			{
				    $('#modalDownloadPDR').modal('hide');
					  document.getElementById('ifrmPDR').src = ""+data+"";
				try{
					$('#modalDownloadPDR').modal('show');
				}catch(err)
				  {}
			}		
		}
	});
}
function generatePDReport(teacherId,hrefId)
{
	var moredisflag=true;
	var modalDownloadPflag=true;
	hideProfilePopover();
	$('#loadingDiv').show();
	CGServiceAjax.generatePDReport(teacherId,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){
		
		$('#loadingDiv').hide();
		document.getElementById("districtlistdivteacherid").value = teacherId;
		var districtselect=data.split("##");
		if(districtselect[1]=="Case3DistrictList"){
			var districts = districtselect[0].split("!!!!");
			document.getElementById('modalDistrictDetails').innerHTML = "";
			$('#modalDistrictDetails').append("<option value="+"0"+"selected="+"selected"+">"+"Select District"+"</option>");
			for(var i=0; i<districts.length; i++){
				var district = districts[i].split("!!!");
				$('#modalDistrictDetails').append("<option value="+district[0]+">"+district[1]+"</option>");
			}
			 moredisflag=false;
			$("#draggableDivMaster").modal('show');
			$("#selectDistrictDiv").modal("show");
				
		}
		if(moredisflag){
		if(deviceType)
		{	
			if (data.indexOf(".doc")!=-1)
			{
				$("#docfileNotOpen").css({"z-index":"3000"});
		    	$('#docfileNotOpen').show();
			}
			else if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
			{
				$("#exelfileNotOpen").css({"z-index":"3000"});
		    	$('#exelfileNotOpen').show();
			}
			else
			{	
				$('#modalDownloadPDR').modal('hide');
				$("#draggableDivMaster").modal('show');	
				document.getElementById(hrefId).href = data;
			
			}
		}
		else if(deviceTypeAndroid)
		{
			if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
			{
				    $('#modalDownloadPDR').modal('hide');
				    document.getElementById('ifrmPDR').src = ""+data+"";
			}
			else
			{
				$('#modalDownloadPDR').modal('hide');
				$("#draggableDivMaster").modal('show');	
				document.getElementById(hrefId).href = data;
			}	
		}
		else if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
		{
			    $('#modalDownloadPDR').modal('hide');
			    document.getElementById('ifrmPDR').src = ""+data+"";
		}
		else
		{
			
				  $('#modalDownloadPDR').modal('hide');
				  document.getElementById('ifrmPDR').src = ""+data+"";
		
			try{
					$('#modalDownloadPDR').modal('show');
			}catch(err)
			  {}
			  }
		}		
		}
	});	
}
function checkBaseViolatedForEPIReset(teacherId)
{
	var rsn=$('#epireset').find(".jqte_editor");
	$('#errorDivReset').empty();
	rsn.css("background-color", "");
	rsn.html('');
	//CandidateReportAjax
	CGServiceAjax.checkBaseCompeleted(teacherId,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){
		
		if(data==1)
		{
			$('#message2show').html(resourceJSON.msgcompletedBaseInventory);
			$('#myModal2').modal('show');
			
			return false;

		}else if(data==3)
		{
			/*$('#message2show').html("Candidate has \"Timed Out\" Base Inventory. You cannot see Professional Development (PD) Report.");
			$('#myModal2').modal('show');*/
			makeEPIIncomplete(teacherId);
			return false;
		}
		else
		{
			$('#message2show').html(resourceJSON.CandidateHasNotComplete);
			$('#myModal2').modal('show');
			return false;
		}
	}
	});	

}
function makeEPIIncomplete(teacherId)
{
	$('#messageConfirm').html(resourceJSON.msgCandStatusTimeout);
	$('#footerbtn2').html("<button class='btn btn-primary' onclick=\"makeIncomplete('"+teacherId+"')\" >"+resourceJSON.btnOk+"</button> <button class='btn' data-dismiss='modal' aria-hidden='true'>"+resourceJSON.btnCancel+"</button>");
	$('#resetModal').modal('show');
}

function makeIncomplete(teacherId)
{
	$('#errorDivReset').empty();
	var rsn=$('#epireset').find(".jqte_editor");
	var reason=trim(rsn.html().replace(/&nbsp;/g,'').replace(/<.*?>/g,""));
	if(reason=="")
	{//shriram
		$('#errorDivReset').append("&#149; "+resourceJSON.msgEpiReset);
		rsn.focus();
		rsn.css("background-color", "#F5E7E1");
		rsn.html('');
		return false;
	}
	var teacherDetail = {teacherId:teacherId};
	$('#resetModal').modal('hide');
	$('#loadingDiv').show();
	//AdminDashboardAjax
	CGServiceAjax.makeEPIIncomplete(teacherDetail,reason,{ 
		async: true,
		callback: function(data){
		$('#loadingDiv').hide();		
		$('#message2show').html(resourceJSON.msgEPIStatusIncomp);
		$('#myModal2').modal('show');
		displayTeacherGrid();
	},
	errorHandler:handleError  
	});
}
function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert(resourceJSON.msgServerErr+": "+exception.javaClassName);}
}
/*=======  searchTeacher on Press Enter Key ========= */
function chkForEnterSearchTeacher(evt)
{
	var charCode = ( evt.which ) ? evt.which : event.keyCode;	
	if(charCode==13)
	{
		searchTeacher();
	}	
}
/*========  display grid after Filter ===============*/
function searchTeacher()
{
	//page=1;
	$("#hidesearchLinkDiv").hide();
	displayTeacherGrid();
}
function setPageFlag(){
	//$('#myModalJobList').hide();
	$('#myModalJobList').modal('hide');
	var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
	if(teacherIdForprofileGrid!='')
	{
		showProfilePopover();
	}	
	defaultSet();
	defaultTeacherGrid();
	currentPageFlag='';
	
	try {
		$('#spanOverride').modal('hide');
	} catch (e) {}
	
}
function activecityType(chk){
	
	if(chk==0){
		document.getElementById("certType").value='';
	}else if(chk==1){
		document.getElementById("certTypeForCandidate").value='';
	}
	
}
function displayTeacherGrid()
{
	
	// by alok for dynamic div width
	exportIconsDiv=true;
   try
   {
	   var mydiv = document.getElementById("mydiv");
       var curr_width = parseInt(mydiv.style.width);
      
       if(deviceType)
       {
    	   mydiv.style.width = 630+"px";  
       }
       else
	   {
    	   mydiv.style.width = 647+"px";  
	   } 
   }
   catch(e){alert(e)}
   /*.........................................*/
	currentPageFlag="";
	 if(exportType=="")
	 {
		 $('#advanceSearchDiv').hide();
			$('#searchLinkDiv').fadeIn();
	 }	 
	
	 var hiddedSearch=false;
	 var hiddenLabel="";
	 var showDistrict=0;
	
	 try
	 {
		 showDistrict=$("#showDistrictLabel").val();
	 }
	 catch(err){}
	
	 
	 
	 
	if(document.getElementById("districtId").value==0){
		document.getElementById('schoolName').readOnly=true;
	}
	if(document.getElementById("schoolName").value==''){
		document.getElementById("schoolId").value=0;
	}
	if(document.getElementById("districtName").value==''){
		document.getElementById("districtId").value=0;
	}
	if(document.getElementById("regionName").value==''){
		document.getElementById("regionId").value=0;
	}
	if(document.getElementById("universityName").value==''){
		document.getElementById("universityId").value=0;
	}
	if(document.getElementById("degreeName").value==''){
		document.getElementById("degreeId").value=0;
	}
	if(document.getElementById("certType").value==''){
		document.getElementById("certificateTypeMaster").value=0;
	}
	
	
	
	//$('#loadingDiv').show();
	 if(exportType=="")
	 {
		 $('#exportIconsDiv').hide();
		 $("#tpMenuActionDiv").hide();
		 $('#teacherGrid').html("<table align='center' id='loaddiv'><tr><td style='padding-top: 10px;' align='center'><!--<img src='images/please.jpg'/>--></td></tr><tr><td style='padding-top: 0px;' align='center'><img src='images/loadingAnimation.gif'/></td></tr></table>");
	 }
	
	var firstName		=	trim(document.getElementById("firstName").value); 
	var lastName		=	trim(document.getElementById("lastName").value);
	var emailAddress	=	trim(document.getElementById("emailAddress").value);
	var districtId		=	trim(document.getElementById("districtId").value); 
	var schoolId		=	trim(document.getElementById("schoolId").value);
	var fromDate	=	trim(document.getElementById("fromDate").value);
	var toDate		=	trim(document.getElementById("toDate").value);
	
	if(Date.parse(fromDate)>Date.parse(toDate))
	{		$("#loaddiv").hide();
		endDateStartdate("&quot;Job Created To&quot; date must be equal to or later than &quot;Job Creation From&quot; date.");
		return false;
	}
	
	if(districtId!="" && districtId!="0")
	{
		if(showDistrict!=1)
		{
			hiddenLabel+=resourceJSON.msgDistrictName1+" || ";
			hiddedSearch=true;
		}	
		
	}
	if(schoolId!="" && schoolId!="0")
	{
		hiddenLabel+=resourceJSON.msgSchoolName+" || ";
		hiddedSearch=true;
			
		
	}
	if(fromDate!="")
	{
		hiddenLabel+=resourceJSON.lblJobCreatedFrom+" || ";
		hiddedSearch=true;
	}	
	
	if(toDate!="")
	{
		hiddenLabel+=resourceJSON.lblJobCreatedTo+" || ";
		hiddedSearch=true;
	}	
	var certType		=	trim(document.getElementById("certificateTypeMaster").value);
	if(certType!="" && certType!="0")
	{
		hiddenLabel+=resourceJSON.lblCLRJ+" || ";
		hiddedSearch=true;
	}	
	var regionId		=	trim(document.getElementById("regionId").value);
	if(regionId!="0")
	{
		hiddenLabel+=resourceJSON.lblRegion+" || ";
		hiddedSearch=true;
	}
	var degreeId		=	trim(document.getElementById("degreeId").value);
	if(degreeId!="0" && degreeId!="")
	{
		hiddenLabel+=resourceJSON.lblHighestDegreeAttained+" || ";
		hiddedSearch=true;
	}
	
	var universityId		=	trim(document.getElementById("universityId").value);
	if(universityId!="0" && universityId!="")
	{
		hiddenLabel+=resourceJSON.lblCollegeAttended+" || ";
		hiddedSearch=true;
	}
	var normScoreSelectVal		=	trim(document.getElementById("normScoreSelectVal").value);
	if(normScoreSelectVal!="0" && normScoreSelectVal!="")
	{
		hiddenLabel+=resourceJSON.lblNormScore+" || ";
		hiddedSearch=true;
	}
	var qualQues		="";
	try{
		qualQues=trim(document.getElementById("qualQues").value);
	}catch(err){}
	if(qualQues!="1" && qualQues!="")
	{
		hiddenLabel+=resourceJSON.lblQualificationQuestions+" || ";
		hiddedSearch=true;
	}
	
	var references	=	trim(document.getElementById("references").value);
	if(references!="0" && references!="")
	{
		hiddenLabel+=resourceJSON.msgReferences+" || ";
		hiddedSearch=true;
	}
	var resume		=	trim(document.getElementById("resume").value);
	if(resume!="0" && resume!="")
	{
		hiddenLabel+=resourceJSON.lblResume+" || ";
		hiddedSearch=true;
	}
	
	var tnl = document.getElementById("subjects");
	
	
	var arr =[];
	for(i=0;i<tnl.length;i++){
		if(tnl[i].selected == true){
			var dd =tnl[i].value;
			if(dd!=0)
			arr.push(dd);
		}  
	}
	if(arr.length>0)
	{
		hiddenLabel+="Subject || ";
		hiddedSearch=true;
	}
	
	var fitScoreSelectVal		=	trim(document.getElementById("fitScoreSelectVal").value);
	if(fitScoreSelectVal!="0" && fitScoreSelectVal!="")
	{
		hiddenLabel+=resourceJSON.lblFitScore+" || ";
		hiddedSearch=true;
	}
	
	var YOTESelectVal		=	trim(document.getElementById("YOTESelectVal").value);
	if(YOTESelectVal!="0" && YOTESelectVal!="")
	{
		hiddenLabel+=resourceJSON.lblYearofTeachingexperience+" || ";
		hiddedSearch=true;
	}
	var AScoreSelectVal		=	trim(document.getElementById("AScoreSelectVal").value);
	if(AScoreSelectVal!="0" && AScoreSelectVal!="")
	{
		hiddenLabel+=resourceJSON.lblAScore+" || ";
		hiddedSearch=true;
	}
	var LRScoreSelectVal		=	trim(document.getElementById("LRScoreSelectVal").value);
	if(LRScoreSelectVal!="0" && LRScoreSelectVal!="")
	{
		hiddenLabel+=resourceJSON.lblLRScore+" || ";
		hiddedSearch=true;
	}
	
	
	
	var canServeAsSubTeacherYes		=	document.getElementById("canServeAsSubTeacherYes").checked;
	var canServeAsSubTeacherNo		=	document.getElementById("canServeAsSubTeacherNo").checked;
	var canServeAsSubTeacherDA		=	document.getElementById("canServeAsSubTeacherDA").checked;
	if(canServeAsSubTeacherYes || canServeAsSubTeacherNo || canServeAsSubTeacherDA)
	{
		hiddenLabel+=resourceJSON.lblWillingToSubstitute+" || ";
		hiddedSearch=true;
	}
	var TFAA		=	document.getElementById("TFAA").checked
	var TFAC		=	document.getElementById("TFAC").checked;
	var TFAN		=	document.getElementById("TFAN").checked;
	if(TFAA || TFAC || TFAN)
	{
		hiddenLabel+=resourceJSON.lblTFA+" || ";
		hiddedSearch=true;
	}
	
	
	try{
		var ifrmFit = document.getElementById('ifrmFit');
		var innerFit = ifrmFit.contentDocument || ifrmFit.contentWindow.document;
		var inputfitScore = innerFit.getElementById('fitScoreFrm');
		fitScoreSlid=document.getElementById("fitScore").value=inputfitScore.value;
	}catch(e){}
	
	
	try{
		var ifrmYOTE = document.getElementById('ifrmYOTE');
		var innerYOTE = ifrmYOTE.contentDocument || ifrmYOTE.contentWindow.document;
		var inputYOTE = innerYOTE.getElementById('YOTEFrm');
		teachingScore=document.getElementById("YOTE").value=inputYOTE.value;		
	}catch(e){}	
	
	
	try{
		var ifrmAScore = document.getElementById('ifrmAScore');
		var innerAScore = ifrmAScore.contentDocument || ifrmAScore.contentWindow.document;
		var inputAScore = innerAScore.getElementById('AScoreFrm');
		aScoreSlid=document.getElementById("AScore").value=inputAScore.value;
	}catch(e){}
	
	
	try{
		var ifrmLRScore = document.getElementById('ifrmLRScore');
		var innerLRScore = ifrmLRScore.contentDocument || ifrmLRScore.contentWindow.document;
		var inputLRScore = innerLRScore.getElementById('LRScoreFrm');
		lrSlid=document.getElementById("LRScore").value=inputLRScore.value;
	}catch(e){}	
	
	var jobAppliedFromDate	=	trim(document.getElementById("jobAppliedFromDate").value);
	var jobAppliedToDate	=	trim(document.getElementById("jobAppliedToDate").value); 
	var daysVal = $("input[name=jobApplied]:radio:checked").val();
	
	if(Date.parse(jobAppliedFromDate)>Date.parse(jobAppliedToDate))
	{		$("#loaddiv").hide();
		endDateStartdate("&quot;Job Created To&quot; date must be equal to or later than &quot;Job Creation From&quot; date.");
		return false;
	}
	
	//alert(document.getElementById("fitScore").value);
	//alert(document.getElementById("YOTE").value);
	//alert(document.getElementById("AScore").value);
	//alert(document.getElementById("LRScore").value);
	
	var internalCandidate=document.getElementById("internalCandidate").checked;
	
	var formalCandidate=document.getElementById("frmlCandidate").checked;
	var retiredCandidate=document.getElementById("retirdCandidate").checked;
	
	if(internalCandidate)
	{
		hiddenLabel+=resourceJSON.lblInternalCandidatesonly+" || ";
		hiddedSearch=true;
	}
	var candidatetype="";
	
		if(formalCandidate)
		{
			hiddedSearch=true;
		}
		if(retiredCandidate)
		{
			hiddedSearch=true;
		}
	
	if(internalCandidate)
	{		
		candidatetype="0|";
	}
	
		if(formalCandidate)
		{		
			candidatetype=candidatetype+"2|";
		}
		if(retiredCandidate)
		{		
			candidatetype=candidatetype+"3|";
		}
	
	var stateId=document.getElementById("stateId").value;
	if(stateId!="0" && stateId!="")
	{
		hiddenLabel+=resourceJSON.lblLSRJ+" || ";
		hiddedSearch=true;
	}
	var stateForcandidate = document.getElementById("stateIdForcandidate").value;
	if(stateForcandidate!="0" && stateForcandidate!="")
	{
		hiddenLabel+=resourceJSON.lblCLSC+" || ";
		hiddedSearch=true;
	}
	var certTypeForCandidate = document.getElementById("certificateTypeMasterForCandidate").value;
	if(certTypeForCandidate!="0" && certTypeForCandidate!="")
	{
		hiddenLabel+=resourceJSON.lblCLC+" || ";
		hiddedSearch=true;
	}
	try{
		var iframeNorm = document.getElementById('ifrmNorm');
		var innerNorm = iframeNorm.contentDocument || iframeNorm.contentWindow.document;
		var inputNormScore = innerNorm.getElementById('normScoreFrm');
		norScore=document.getElementById("normScore").value=inputNormScore.value;
	}catch(e){}	
	try{
		var iframeCGPA = document.getElementById('ifrmCGPA');
		var innerCGPA = iframeCGPA.contentDocument || iframeCGPA.contentWindow.document;
		var inputCGPA = innerCGPA.getElementById('CGPAFrm');
		CGPASlid=document.getElementById("CGPA").value=inputCGPA.value;
	}catch(e){}
	
	var fitScore=document.getElementById("fitScore").value;
	var YOTE=document.getElementById("YOTE").value;
	var AScore=document.getElementById("AScore").value;
	var LRScore=document.getElementById("LRScore").value;
	
	var normScore		=	trim(document.getElementById("normScore").value);
	var CGPA		=	trim(document.getElementById("CGPA").value);
	var CGPASelectVal		=	trim(document.getElementById("CGPASelectVal").value);
	if(CGPASelectVal!="0" && CGPASelectVal!="")
	{
		hiddenLabel+=resourceJSON.lblCGPA+" || ";
		hiddedSearch=true;
	}
	var candidateStatus		=	trim(document.getElementById("candidateStatus").value);
	
	var teacherSSN	=	trim(document.getElementById("ssn").value);
	var empNo	=	trim(document.getElementById("empNo").value);
	
	var jobCategoryIds='';
    var jobCategoryId   = document.getElementById("jobCateSelect");
    for (var i = 0; i < jobCategoryId.options.length; i++) {
        if(jobCategoryId.options[i].selected ==true){
        	if(jobCategoryIds==''){
        		jobCategoryIds=  jobCategoryId.options[i].value;
        	}
      	  	else{
      	  		jobCategoryIds=jobCategoryIds+ ","+jobCategoryId.options[i].value;
      	  	}
        }
    }
    if(jobCategoryIds!="0" && jobCategoryIds!="")
	{
		hiddenLabel+=resourceJSON.lblJobCategory+" || ";
		hiddedSearch=true;
	}
    var tagsearchId="";
    if(document.getElementById("tagsearchId"))
    	tagsearchId = trim(document.getElementById("tagsearchId").value);
    
    if(tagsearchId!="")
	{
		hiddenLabel+=resourceJSON.lblTags+" || ";
		hiddedSearch=true;
	}
    /*var newcandidatesOnly  = false;
	newcandidatesOnly = $('#newcandidatesonly').is(":checked");
	alert($('#newcandidatesonly').is(":checked"));*/
    var hiddenCertTypeIds 	=	document.getElementsByName("hiddenCertTypeId");
	var certIds =[];
	for(var i=0;i<hiddenCertTypeIds.length;i++)
	{
		certIds.push(hiddenCertTypeIds[i].value);
	}
    var stateId2	=	trim(document.getElementById("stateId2").value);
    if(stateId2!="0" && stateId2!="")
	{
		hiddenLabel+=resourceJSON.lblStateName+" || ";
		hiddedSearch=true;
	}
    var zipCode		=	trim(document.getElementById("zipCode").value);
    if(zipCode!="0" && zipCode!="")
	{
		hiddenLabel+=resourceJSON.lblZipCode+" || ";
		hiddedSearch=true;
	}
    var epiFromDate	=	trim(document.getElementById("epiFromDate").value);
    if(epiFromDate!="0" && epiFromDate!="")
	{
		hiddenLabel+=resourceJSON.lblEPIDateFrom+" || ";
		hiddedSearch=true;
	}
	var epiToDate	=	trim(document.getElementById("epiToDate").value);
	if(epiToDate!="0" && epiToDate!="")
	{
		hiddenLabel+=resourceJSON.lblEPIDateTo+" || ";
		hiddedSearch=true;
	}

	
	
	var 	jobSubCategoryId   = document.getElementById("jobSubCateSelect");
    var jobSubCategoryIds='';

 	 for (var i = 0; i < jobSubCategoryId.options.length; i++) {
	        if(jobSubCategoryId.options[i].selected ==true){
	      	  if(jobSubCategoryIds==''){
	      		  jobSubCategoryIds=  jobSubCategoryId.options[i].value;
	      	  }
	      	  else{
	      		  jobSubCategoryIds=jobSubCategoryIds+ ","+jobSubCategoryId.options[i].value;
	      	  }
	        }
	     }
 	if(jobSubCategoryIds!="0" && jobSubCategoryIds!="")
	{
		hiddenLabel+=resourceJSON.lblJobSubCategory+" || ";
		hiddedSearch=true;
	}
 	 var headQuarter = "";
 	 try{
 		headQuarter = document.getElementById("headQuarterId").value;
 	 }catch(e){}
 	 var branch = "";
 	 try{
 		 branch = document.getElementById("branchSearchId").value;
 	 }catch(e){}
 	 
 	 try{
 	    // Manke Status as multi selection
 	    var candidateStatusSet='';
 	    var candidateStatusId	=	document.getElementById("candidateStatus");
 	    for (var i = 0; i < candidateStatusId.options.length; i++) {
 	        if(candidateStatusId.options[i].selected ==true){
 	        	if(candidateStatusSet==''){
 	        		candidateStatusSet=  candidateStatusId.options[i].value;
 	        	}
 	      	  	else{
 	      	  		candidateStatusSet=candidateStatusSet+ "#%#"+candidateStatusId.options[i].value;
 	      	  	}
 	        }
 	    }
 	   if(candidateStatusSet!="0" && candidateStatusSet!="")
 		{
 			hiddenLabel+=resourceJSON.lblCandidateStatus   +"   ";
 			hiddedSearch=true;
 		}
 	   

 	 }catch(err){}
 	//alert(hiddenLabel.substring(0, hiddenLabel.length - 3));
 	 if(hiddedSearch)
 	 {
 		$("#advLabel").show();
 		$("#filterActiveLbl").show();
 		$("#advsearch").hide();
 		
 	 } 
 	 else
 	 {
 		$("#advLabel").hide();
 		$("#filterActiveLbl").hide();
 		$("#advsearch").show();
 	 }	

 	/*******TPL-3682 DA/SA: Advanced Search starts********************************************/
 	var withdrawType = [];
 	if($('#withdrawTypeBox').val()!=null && $('#withdrawTypeBox').val()!='null'&&
 		$('#withdrawTypeBox').val()!=''){
 		statusSearchUtil.flush();
 		statusSearchUtil.element = document.getElementById('withdrawTypeBox');
 		statusSearchUtil.selectValue();
 		withdrawType=statusSearchUtil.values;
 	};
 	/*******TPL-3682 DA/SA: Advanced Search ends********************************************/

 	 
 	$("#advanceCheckTooltip").text(hiddenLabel.substring(0, hiddenLabel.length - 3));
 	 firstName=firstName+"@@@"+exportType;
 	var url =window.location.href;
 	$("#allPartialProfilesDiv").html("");
    if(url.indexOf("teacherinfo.do") > -1){
    	TeacherInfotAjax.displayTeacherGridNewOp(firstName,lastName,emailAddress,regionId,degreeId,universityId,normScoreSelectVal,qualQues,
 				normScore,CGPA,CGPASelectVal,candidateStatusSet,stateId,candidatetype,noOfRows,page,sortOrderStr,sortOrderType,districtId,schoolId,fromDate,toDate,certType,references,resume,fitScoreSelectVal,YOTESelectVal,AScoreSelectVal,LRScoreSelectVal,canServeAsSubTeacherYes,canServeAsSubTeacherNo, 
 				canServeAsSubTeacherDA,TFAA,TFAC,TFAN,fitScore,YOTE,AScore,LRScore,jobAppliedFromDate,jobAppliedToDate,daysVal,arr,$('#newcandidatesonly').is(":checked"),teacherSSN,stateForcandidate,certTypeForCandidate,empNo,jobCategoryIds,tagsearchId,stateId2,zipCode,certIds,epiFromDate,epiToDate,jobSubCategoryIds,headQuarter,branch,withdrawType,{ 
 			async: true,
 			callback: function(data)
 			{
 			//shadab
 			//alert("data");
 				if(exportType=="excel")
 				{
 					//alert(data);
 					$('#loadingDiv').hide();
 					if(deviceType)
 					{					
 							$("#exelfileNotOpen").css({"z-index":"3000"});
 					    	$('#exelfileNotOpen').show();
 					}
 					else
 					{
 						try
 						{
 							document.getElementById('ifrmTrans').src = "applicantspool/"+data+"";
 						}
 						catch(e)
 						{alert(e);}
 					}
 					exportType="";	
 				}
 				else if(exportType=="pdf")
 				{
 					$('#loadingDiv').hide();	
 					if(deviceType || deviceTypeAndroid)
 					{
 						window.open(data,'left=200,top=50,width=700,height=600,toolbar=1,resizable=0');			
 					}		
 					else
 					{
 						$('#modalDownloadApplicantpool').modal('hide');
 						document.getElementById('ifrmCJS').src = ""+data+"";
 						try{
 							$('#modalDownloadApplicantpool').modal('show');
 						}catch(err)
 						{}		
 				     }		
 					exportType="";
 				     return false;
 				}
 				else if(exportType=="print")
 				{
 					 
 					$('#loadingDiv').hide();
 					try{ 
 					 if (isSafari && !deviceType)
 					    {
 					    	window.document.write(data);
 							window.print();
 					    }else
 					    {
 					    	var newWindow = window.open();
 					    	newWindow.document.write(data);	
 					    	newWindow.print();
 						 }
 					}catch (e) 
 					{
 						$('#printmessage1').modal('show');
 					}
 					exportType="";
 				     return false;
 				}	
 				else
 				{
 					   $('#exportIconsDiv').show();
 						//$('#loadingDiv').hide();
 						$('#teacherGrid').html(data);			
 						applyScrollOnTbl();	
 						
 						/*========== for showing Tool tip on Images ============*/
 						tpJbIDisable();
 						//tpJbIEnable();
 						$('#loadingDiv').hide();
 				}	
 			 
 			},
 			errorHandler:handleError 
 		}); 
 	 }else{
 		TeacherInfotAjax.displayTeacherGridNewOp_new(firstName,lastName,emailAddress,regionId,degreeId,universityId,normScoreSelectVal,qualQues,
 				normScore,CGPA,CGPASelectVal,candidateStatusSet,stateId,candidatetype,noOfRows,page,sortOrderStr,sortOrderType,districtId,schoolId,fromDate,toDate,certType,references,resume,fitScoreSelectVal,YOTESelectVal,AScoreSelectVal,LRScoreSelectVal,canServeAsSubTeacherYes,canServeAsSubTeacherNo, 
 				canServeAsSubTeacherDA,TFAA,TFAC,TFAN,fitScore,YOTE,AScore,LRScore,jobAppliedFromDate,jobAppliedToDate,daysVal,arr,$('#newcandidatesonly').is(":checked"),teacherSSN,stateForcandidate,certTypeForCandidate,empNo,jobCategoryIds,tagsearchId,stateId2,zipCode,certIds,epiFromDate,epiToDate,jobSubCategoryIds,headQuarter,branch,withdrawType,{ 
 			async: true,
 			callback: function(data)
 			{
 			//shadab
 			//alert("data1");
 				if(exportType=="excel")
 				{
 					//alert(data);
 					$('#loadingDiv').hide();
 					if(deviceType)
 					{					
 							$("#exelfileNotOpen").css({"z-index":"3000"});
 					    	$('#exelfileNotOpen').show();
 					}
 					else
 					{
 						try
 						{
 							document.getElementById('ifrmTrans').src = "applicantspool/"+data+"";
 						}
 						catch(e)
 						{alert(e);}
 					}
 					exportType="";	
 				}
 				else if(exportType=="pdf")
 				{
 					$('#loadingDiv').hide();	
 					if(deviceType || deviceTypeAndroid)
 					{
 						window.open(data,'left=200,top=50,width=700,height=600,toolbar=1,resizable=0');			
 					}		
 					else
 					{
 						$('#modalDownloadApplicantpool').modal('hide');
 						document.getElementById('ifrmCJS').src = ""+data+"";
 						try{
 							$('#modalDownloadApplicantpool').modal('show');
 						}catch(err)
 						{}		
 				     }		
 					exportType="";
 				     return false;
 				}
 				else if(exportType=="print")
 				{
 					 
 					$('#loadingDiv').hide();
 					try{ 
 					 if (isSafari && !deviceType)
 					    {
 					    	window.document.write(data);
 							window.print();
 					    }else
 					    {
 					    	var newWindow = window.open();
 					    	newWindow.document.write(data);	
 					    	newWindow.print();
 						 }
 					}catch (e) 
 					{
 						$('#printmessage1').modal('show');
 					}
 					exportType="";
 				     return false;
 				}	
 				else
 				{
 					   $('#exportIconsDiv').show();
 						//$('#loadingDiv').hide();
 						$('#teacherGrid').html(data);			
 						applyScrollOnTbl();	
 						
 						/*========== for showing Tool tip on Images ============*/
 						tpJbIDisable();
 						//tpJbIEnable();
 						$('#loadingDiv').hide();
 				}	
 			 
 			},
 			errorHandler:handleError 
 		});
 	 }
    var poolRef=$('#canrePoolVal').val();
    if(poolRef=="true")
    {
    	$("#reminderAllMail").show();
    }
    else
    {
    	$("#reminderAllMail").hide();
    }
	
}
function teacherRefresh(){
	document.getElementById("firstName").value=""; 
	document.getElementById("firstName").value=""; 
	document.getElementById("lastName").value="";
	document.getElementById("emailAddress").value="";
	document.getElementById("fromDate").value="";
	document.getElementById("toDate").value=""; 
	document.getElementById("certType").value="";
	document.getElementById("regionName").value="";
}
function getJobList(tId){
	flagvalue='jobList'
	defaultSet();	
	currentPageFlag='job';
	noOfRowsForJob=50;	
	teacherId=tId;	
	MassStatusUpdateService.getJobCategoryAndSubjectListBox(teacherId,
	{ 
		async: true,
		callback: function(data)
		{
			if(data!="")
			{
				try
				{
					document.getElementById("divJobHeader").innerHTML=data;
				}catch(err)
				{}
				
				
				try
				{
					document.getElementById("btnChangeStatus").style.display="none";
				}catch(err)
				{}
				
				try
				{
					$('#jobCategoryToolTip').tooltip();
				}catch(err)
				{}
				
			}
				getJobOrderList();
		},errorHandler:handleError 
	}
	);
	
	
	
}

function getJobOrderList(){
	//alert("1.3 getJobOrderList for "+teacherId);
	$('#divJobHeaderError').empty();
	var jobCategoryId_msu=0;
	var subjectId_msu=0;
	try
	{
		jobCategoryId_msu=$("#jobCategoryId_msu").val().trim();
		subjectId_msu=$("#subjectId_msu").val().trim();
	}catch(err)
	{}
	
	$('#loadingDiv').show();
	hideProfilePopover();
	var zoneIdd=$("#zoneMM").val();
	TeacherInfotAjax.getJobListByTeacherByProfile(teacherId,noOfRowsForJob,pageForTC,sortOrderStrForTC,sortOrderTypeForTC,true,"TeacherPool",jobCategoryId_msu,subjectId_msu,zoneIdd,{ 
		async: true,
		callback: function(data)
		{
			try
			{
				$('#loadingDiv').hide();			
				//$('#myModalJobList').show(); 
				$('#myModalJobList').modal('show');
				document.getElementById("divJob").innerHTML=data;
				
				var teacherFullName="";
				try
				{
					teacherFullName=document.getElementById("teacherFullName").value;
				}
				catch(err)
				{}
				
				if(teacherFullName!='')
				{
					try
					{
						document.getElementById("jobDetailHeaderText").innerHTML="Jobs "+teacherFullName+" Has Applied For";
					}
					catch(err)
					{}
				}
				else
				{
					try
					{
						document.getElementById("jobDetailHeaderText").innerHTML="Jobs";
					}
					catch(err)
					{}
				}
				
				
				
				applyScrollOnJob();
				jobEnable();
				
				var txtJobLstNo=document.getElementById("txtJobLstNo").value;
				
				if(jobCategoryId_msu > 0 || txtJobLstNo==1)
				{
					
					var chkJobId_MSU_Values="";
					var inputs_chkjobId_msu = document.getElementsByName("chkjobId_msu");
					for (var i = 0; i < inputs_chkjobId_msu.length; i++) 
					{
						if (inputs_chkjobId_msu[i].type == 'checkbox') 
						{
							chkJobId_MSU_Values+=inputs_chkjobId_msu[i].value+",";
				        }
					}
					
					//alert("chkJobId_MSU_Values "+chkJobId_MSU_Values);
					
					if(chkJobId_MSU_Values!="")
					{
						document.getElementById("btnChangeStatus").style.display="inline";
						document.getElementById("divJobHeaderCheckBox").style.display="inline";
					}
					else
					{
						document.getElementById("btnChangeStatus").style.display="none";
						document.getElementById("divJobHeaderCheckBox").style.display="none";
					}
				}
				else
				{
					document.getElementById("btnChangeStatus").style.display="none";
					document.getElementById("divJobHeaderCheckBox").style.display="none";
				}
				
			}
			catch(err)
			{}
		},
		errorHandler:handleError 
	});
}
function generatePilarReport(teacherId,jobId,hrefId)
{
	$("#myModalJobList").css({"z-index":"1"});
	jobOrder={jobId:jobId};
	CandidateGridAjaxNew.generatePilarReport(teacherId,jobOrder,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){
		$('#loadingDiv').hide();
		if(data==0)
		{
			$('#myModalMsgShow').html(resourceJSON.NoJobSpecificInventory);
			$('#myModalJobList').modal('hide');
			$('#myModalMsg').modal('show');
			return false;
		}else if(data==1)
		{
			$('#myModalMsgShow').html(resourceJSON.CandidateHasNotCompleteThisJobOrder);
			$('#myModalJobList').modal('hide');
			$('#myModalMsg').modal('show');
			return false;
		}else if(data==2)
		{
			$('#myModalMsgShow').html(resourceJSON.CandidateHasTimeOut);
			$('#myModalJobList').modal('hide');
			$('#myModalMsg').modal('show');
			return false;
		}else if(data==3){
			$("#myModalJobList").css({"z-index":"3000"});
			generateJSAReport(teacherId,jobOrder,hrefId);
		}
	}
	});	
}
function generateJSAReport(teacherId,jobOrder,hrefId)
{
	$('#loadingDiv').show();
	CandidateGridAjaxNew.generateJSAReport(teacherId,jobOrder,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){
		$('#loadingDiv').hide();

		document.getElementById(hrefId).href=data;
	}
	});	
}
function setZIndexJobDiv()
{
	defaultSet();
	currentPageFlag="job";
	$('#myModalDesc').modal('hide');
	$('#myModalCommunications').modal('hide');
	$('#myModalCoverLetter').modal('hide');	
	$("#myModalJobList").css({"z-index":"3000"});
	if(document.getElementById("commDivFlag").value==0){
		$('#myModalJobList').modal('show');
	}else{
		currentPageFlag="";
	}
	
	var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
	if(teacherIdForprofileGrid!='')
	{
		showProfilePopover();
	}
	
}

function closeCommunication()
{
	defaultSet();
	currentPageFlag="job";
	$('#myModalDesc').modal('hide');
	$('#myModalCommunications').modal('hide');
	$('#myModalCoverLetter').modal('hide');		
	$("#myModalJobList").css({"z-index":"3000"});
	if(document.getElementById("commDivFlag").value==0){
		$('#myModalJobList').modal('show');
	}else{
		currentPageFlag="";
		$('#draggableDivMaster').modal('show');
	}
}

function showCoverLetter(teacherId, jobId)
{	
	$("#myModalJobList").css({"z-index":"1"});
	CandidateGridAjaxNew.getCoverLetter(teacherId,jobId,
	{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{	
			if(data=="")
			{
				$('#myModalMsgShow').html(resourceJSON.CandidateNotProvidedCoverLtr);
				$('#myModalJobList').modal('hide');
				$('#myModalMsg').modal('show');
			}
			else
			{
				document.getElementById("lblCL").innerHTML=""+data;
				$('#myModalJobList').modal('hide');
				$('#myModalCoverLetter').modal('show');				
			}		
		}
	});
}

function getTranscriptDetail(tId)
{	
	defaultSet();
	currentPageFlag='tran';
	teacherId=tId;
	getTranscriptGrid();
}

function getTranscriptGrid()
{
	//$("#myModalJobList").css({"z-index":"1"});
	CandidateGridAjaxNew.getTranscriptGrid(teacherId,noOfRowsForTC,pageForTC,sortOrderStrForTC,sortOrderTypeForTC,{ 
	async: true,
	errorHandler:handleError,
	callback:function(data)
	{	
		//$('#myModalJobList').modal('hide');
		$('#myModalTranscript').modal('show');
		document.getElementById("divTranscript").innerHTML=data;
		applyScrollOnTranscript();
	}});
}
function getCertificationDetail(tId)
{	
	defaultSet();
	currentPageFlag='cert';
	teacherId=tId;
	getCertificationGrid();
	
}

function getCertificationGrid()
{
	//$("#myModalJobList").css({"z-index":"1"});
	CandidateGridAjaxNew.getCertificationGrid(teacherId,noOfRowsForTC,pageForTC,sortOrderStrForTC,sortOrderTypeForTC,{ 
	async: true,
	errorHandler:handleError,
	callback:function(data)
	{	
		//$('#myModalJobList').modal('hide');
		$('#myModalCertification').modal('show');
		document.getElementById("divCertification").innerHTML=data;
		applyScrollOnCertification();
	}});
}

/*========  Activate and Deactivate Teacher ===============*/
function activateDeactivateTeacher(teacherId,status)
{
	TeacherInfotAjax.activateDeactivateTeacher(teacherId,status, { 
		async: true,
		callback: function(data)
		{
			displayTeacherGrid();
		},
		errorHandler:handleError 
	});
				
}
function tpJbIEnable()
{
	var noOrRow = document.getElementById("teacherTable").rows.length;
	for(var j=1;j<=noOrRow;j++)
	{
		$('#iconpophover'+j).tooltip();
		$('#tpApply'+j).tooltip();
		$('#tpWithdraw'+j).tooltip();
		$('#tpShare'+j).tooltip();
		$('#tpHide'+j).tooltip();
	}
}
function tpJbIDisable()
{
	
	$('#jobApplied').tooltip();
	var noOrRow=0;
	try {
	 noOrRow = document.getElementById("teacherTable").rows.length;
	}catch(err) {}
	// alert("AAAA");	 
	for(var j=1;j<=noOrRow;j++)
	{
		$('#tpTrans'+j).tooltip();
		$('#tpCert'+j).tooltip();
		$('#tpResume'+j).tooltip();
		$('#tpPortfolio'+j).tooltip();
		$('#tpJSA'+j).tooltip();
		$('#tpPDReport'+j).tooltip();
		$('#tpPhone'+j).tooltip();
		$('#tpPhoto'+j).tooltip();
		$('#tpReset'+j).tooltip();
		$('#tpInternalCandidate'+j).tooltip();
		$('#tpJobApplied'+j).tooltip();
		$('#epiAndJsi'+j).tooltip();
		$('#tpInv'+j).tooltip();
		$('#epiACC'+j).tooltip();
		$('#onLineReset'+j).tooltip();
		$('#teacherFnameAndLname'+j).tooltip();
		$('#teachrEmailAdderssP'+j).tooltip();
		try {$('#alum'+j).tooltip();} catch (e) {}
		try{$('#tpRefChk'+j).tooltip(); }catch(e){}
		try{$('#dtFailed'+j).tooltip(); }catch(e){}
		try{$('#cndNotReviewed'+j).tooltip(); }catch(e){}
	}
}
function jobEnable()
{
	var noOrRow = document.getElementById("jobTable").rows.length;
	for(var j=1;j<=noOrRow;j++)
	{
		
		$('#jobCom'+j).tooltip();
		$('#tpJSA'+j).tooltip();
		$('#jobCL'+j).tooltip();
		$('#jobTrans'+j).tooltip();
		$('#jobCert'+j).tooltip();
		$('#jobJobId'+j).tooltip();
		$('#jobJobTitle'+j).tooltip();
		$('#hireTp'+j).tooltip();
	}
}
function descriptionShow(jobId)
{
	$("#myModalJobList").css({"z-index":"1"});
	TeacherInfotAjax.getJobDescription(jobId, { 
		async: true,
		callback: function(data)
		{
			$('#myModalJobList').modal('hide');
			$('#myModalDesc').modal('show');
			document.getElementById("description").innerHTML=data;
		},
		errorHandler:handleError 
	});
	
}

var count=0;
var index=-1;
var length=0;
var divid='';
var txtid='';
var hiddenDataArray = new Array();
var showDataArray = new Array();
var degreeTypeArray = new Array();
var hiddenId="";
function getDistrictAuto(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("districtName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getDistrictArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getDistrictArray(districtName){
	var searchArray = new Array();
	//BatchJobOrdersAjax
	CGServiceAjax.getFieldOfDistrictList(districtName,{ 
			async: false,
			callback: function(data){
			hiddenDataArray = new Array();
			showDataArray = new Array();
			for(i=0;i<data.length;i++){
				searchArray[i]=data[i].districtName;
				showDataArray[i]=data[i].districtName;
				hiddenDataArray[i]=data[i].districtId;
			}
		},
		errorHandler:handleError
		});	
	return searchArray;
}


var selectFirst="";

var fatchData= function(txtSearch,searchArray,txtId,txtdivid){
	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
				
			}
			
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}
function hideDistrictMasterDiv(dis,hiddenId,divId)
{
	if(document.getElementById("districtName").value==""){
		document.getElementById('schoolName').readOnly=true;
		document.getElementById('schoolName').value="";
		document.getElementById('schoolId').value="0";
	}
	document.getElementById("districtId").value="";
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			document.getElementById("districtId").value=hiddenDataArray[index];
			document.getElementById('schoolName').readOnly=false;
			document.getElementById('schoolName').value="";
			document.getElementById('schoolId').value="0";
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}

count=0;
index=-1;
length=0;
divid='';
txtid='';
hiddenDataArray = new Array();
showDataArray = new Array();
degreeTypeArray = new Array();
hiddenId="";

function getUniversityAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38)
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13)
	{
		
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		document.getElementById("fieldName").focus();
		
	} 
	else if(event.keyCode==9)
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getUniversityArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getUniversityArray(universityName){
	
	var searchArray = new Array();
	//DWRAutoComplete
	CGServiceAjax.getUniversityMasterList(universityName,{ 
		async: false,		
		callback: function(data){
		hiddenDataArray = new Array();
		showDataArray = new Array();
		for(i=0;i<data.length;i++){
			searchArray[i]=data[i].universityName;
			hiddenDataArray[i]=data[i].universityId;
			showDataArray[i]=data[i].universityName;
		}
	}
	});	

	return searchArray;
}


function hideUniversityDiv(dis,hiddenId,divId)
{
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		document.getElementById(hiddenId).value=hiddenDataArray[index];
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
		if(dis.value!="")
		{
			var cnt=0;
			var focs=0;	
			$('#errordiv').empty();	
			setDefColortoErrorMsg();
			$('#errordiv').append("&#149; "+resourceJSON.PlzEtrVldClgAttnded+"<br>");
			if(focs==0)
				$('#universityName').focus();
			
			$('#universityName').css("background-color",txtBgColor);
			cnt++;focs++;	
		}
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}

count=0;
index=-1;
length=0;
divid='';
txtid='';
hiddenDataArray = new Array();
showDataArray = new Array();
hiddenId="";

function getRegionAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38)
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13)
	{
		
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		document.getElementById("regionName").focus();
		
	} 
	else if(event.keyCode==9)
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getRegionArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getRegionArray(regionName){
	
	var searchArray = new Array();
	//DWRAutoComplete
	CGServiceAjax.getRegionMasterList(regionName,{ 
		async: false,		
		callback: function(data){
		hiddenDataArray = new Array();
		showDataArray = new Array();
		for(i=0;i<data.length;i++){
			searchArray[i]=data[i].regionName;
			hiddenDataArray[i]=data[i].regionId;
			showDataArray[i]=data[i].regionName;
		}
	}
	});	

	return searchArray;
}


function hideRegionDiv(dis,hiddenId,divId)
{
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		document.getElementById(hiddenId).value=hiddenDataArray[index];
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
		if(dis.value!="")
		{
			var cnt=0;
			var focs=0;	
			$('#errordiv').empty();	
			$('#errordiv').append("&#149; "+resourceJSON.PlzEtrVldRegionName+"<br>");
			if(focs==0)
				$('#regionName').focus();
			
			$('#regionName').css("background-color",txtBgColor);
			cnt++;focs++;	
		}
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}


count=0;
index=-1;
length=0;
divid='';
txtid='';
hiddenDataArray = new Array();
showDataArray = new Array();
degreeTypeArray = new Array();
hiddenId="";

function getDegreeMasterAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	$('#degreeName').mousedown(function(e) {
	if( (event.keyCode == 1) ){
        alert(resourceJSON.msgleftbutton);
    }if( (event.keyCode== 3) ) {
        alert(resourceJSON.msgrightbutton);
    }else if( (event.keyCode== 2) ) {
        alert(resourceJSON.msgmiddlebutton); 
    }
	});
	
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		document.getElementById("universityName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getDegreeMasterArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getDegreeMasterArray(degreeName){
	var searchArray = new Array();
	//DWRAutoComplete
	CGServiceAjax.getDegreeMasterList(degreeName,{ 
		async: false,		
		callback: function(data){
		hiddenDataArray = new Array();
		showDataArray = new Array();
		for(i=0;i<data.length;i++){
			searchArray[i]=data[i].degreeName;
			hiddenDataArray[i]=data[i].degreeId;
			showDataArray[i]=data[i].degreeName;
			degreeTypeArray[i]=data[i].degreeType;
		}
	}
	});	

	return searchArray;
}

function hideDegreeMasterDiv(dis,hiddenId,divId)
{
	document.getElementById("degreeType").value="";
	if(parseInt(length)>0)
	{
		if(index==-1)
		{
			index=0;
		}
			document.getElementById(hiddenId).value=hiddenDataArray[index];			
			var degreeName = document.getElementById("degreeName").value;
			var degreeType = document.getElementById("degreeType").value=degreeTypeArray[index];
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
			document.getElementById("degreeType").value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
		}
	}else{
		if(document.getElementById(hiddenId))
		{
			document.getElementById(hiddenId).value="";
		}
		if(dis.value!=""){
			var cnt=0;
			var focs=0;	
			$('#errordiv').empty();	
			setDefColortoErrorMsg();
			$('#errordiv').append("&#149; "+resourceJSON.PlzEtrVldDegree+"<br>");
			if(focs==0)
				$('#degreeName').focus();
			
			$('#degreeName').css("background-color",txtBgColor);
			cnt++;focs++;	
		}
			
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}

count=0;
index=-1;
length=0;
divid='';
txtid='';
hiddenDataArray = new Array();
showDataArray = new Array();
degreeTypeArray = new Array();
hiddenId="";

function getSchoolAuto(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("schoolName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getSchoolArray(txtSearch.value);
		fatchData2(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getSchoolArray(schoolName){
	var searchArray = new Array();
	var districtId	=	document.getElementById("districtId").value;
	//BatchJobOrdersAjax
	CGServiceAjax.getFieldOfSchoolList(districtId,schoolName,{ 
			async: false,
			callback: function(data){
			hiddenDataArray = new Array();
			showDataArray = new Array();
			for(i=0;i<data.length;i++){
				searchArray[i]=data[i].schoolName;
				showDataArray[i]=data[i].schoolName;
				hiddenDataArray[i]=data[i].schoolMaster.schoolId;
			}
		},
		errorHandler:handleError
		});	
	return searchArray;
}


selectFirst="";

var fatchData2= function(txtSearch,searchArray,txtId,txtdivid){

	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
				
			}
			
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}
function hideSchoolMasterDiv(dis,hiddenId,divId)
{
	document.getElementById("schoolId").value="";
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			document.getElementById("schoolId").value=hiddenDataArray[index];
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}




var downArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index<length-1){
			for(var i=0;i<10;i++){
				{
					if(document.getElementById('divResult'+txtdivid+i))
						var div_id=document.getElementById('divResult'+txtdivid+i);
					if(div_id)
					{
						if(div_id.className=='over')
							index=div_id.id.split('divResult'+txtdivid)[1];
						div_id.className='normal';
					}
				}
			}
			index++;
			if(document.getElementById('divResult'+txtdivid+index))
			{
				var div_id=document.getElementById('divResult'+txtdivid+index);
				div_id.className='over';
				document.getElementById(txtid).value = div_id.innerHTML;
				selectFirst=div_id.innerHTML;
			}
		}
	}
}

var upArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index>0){
			for(var i=0;i<length;i++){

				var div_id=document.getElementById('divResult'+txtdivid+i);
				if(div_id)
				{
					if(div_id.className=='over')
						index=div_id.id.split('divResult'+txtdivid)[1];
					div_id.className='normal';
				}
			}
			index--;
			if(document.getElementById('divResult'+txtdivid+index))
				document.getElementById('divResult'+txtdivid+index).className='over';
			if(txtid && document.getElementById('divResult'+txtdivid+index))
			{
				document.getElementById(txtid).value=
					document.getElementById('divResult'+txtdivid+index).innerHTML;
				selectFirst=document.getElementById('divResult'+txtdivid+index).innerHTML;
			}
		}
	}
}

var overText=function (div_value,txtdivid) {

	for(var i=0;i<length;i++)
	{
		if(document.getElementById('divResult'+i))
		{
			var div_id=document.getElementById('divResult'+i);

			if(div_id.className=='over')
				index=div_id.id.split(txtdivid)[1];
			div_id.className='normal';
		}
	}
	div_value.className = 'over';
	document.getElementById(txtid).value= div_value.innerHTML;
}
function trim(s)
{
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}
function checkForInt(evt) {

	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==8)
		return true;

	return( (charCode >= 48 && charCode <= 57)||(charCode==13) );
}

function mouseOverChk(txtdivid,txtboxId)
{	
	for(var i=0;i<length;i++)
	{
		$('#divResult'+txtdivid+i).mouseover({param1: i,param2:txtdivid, param3:txtboxId}, fireMouseOverEvent);
	}
}


function fireMouseOverEvent(event)
{
	for(var i=0;i<length;i++)
	{	
		document.getElementById('divResult'+event.data.param2+i).className='normal';
	}
    document.getElementById('divResult'+event.data.param2+event.data.param1).className='over';	       
   	document.getElementById(event.data.param3).value= $('#divResult'+event.data.param2+event.data.param1).text();
    index=event.data.param1;
}

function checkForCGInt(evt) {

	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==8 || charCode==46)
		return true;

	return( (charCode >= 48 && charCode <= 57)||(charCode==13) );
}
function showProfile(teacherId)
{
	$('#myModalProfile').show();
}
function hideProfile()
{
	$('#myModalProfile').hide();
}
function getCommunicationsDiv(commDivFlag,jobForTeacherGId,teacherId,jobId)
{

	hideProfilePopover();
	document.getElementById("commDivFlag").value=commDivFlag;
	document.getElementById("jobId").value=jobId;
	document.getElementById("jobForTeacherGId").value=jobForTeacherGId;
	CandidateGridAjaxNew.getCommunications(commDivFlag,jobForTeacherGId,teacherId,jobId,false,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){
		document.getElementById("divCommTxt").innerHTML=data.toString();
		try
		{
			$('#myModalJobList').modal('hide');
			$('#myModalCommunications').modal('show');
			
			$("#myModalCommunications").attr("commDivFlag",commDivFlag);
			$("#myModalCommunications").attr("jobForTeacherGId",jobForTeacherGId);
			$("#myModalCommunications").attr("teacherId",teacherId);
			$("#myModalCommunications").attr("jobId",jobId);
		}
		catch(err){}
		
		/*$('#myModalJobList').modal('hide');
		$('#myModalCommunications').modal('show');*/
		$('#commPhone').tooltip();
		$('#commMsg').tooltip();
		$('#commNotes').tooltip();
	}
	});	
}
function downloadCommunication(filePath,fileName,linkId)
{
	$('#loadingDiv').show();
	CandidateGridAjaxNew.downloadCommunication(filePath,fileName,
			{ 
				async: false,
				errorHandler:handleError,
				callback:function(data)
				{
					$('#loadingDiv').hide();
					if (data.indexOf(".doc") !=-1 || data.indexOf(".xls") !=-1) {
					    document.getElementById('ifrmTrans').src = ""+data+"";
					}else{
						document.getElementById(linkId).href = data; 
						return false;
					}
				}
			});
}
function showCommunicationsDiv()
{
	$('#myModalMessage').modal('hide');	
	$('#myModalNotes').modal('hide');	
	$('#myModalCommunications').modal('show');
}
function showCommunicationsDivForMsg()
{
	$('#myModalMessage').modal('hide');	
	var msgType=document.getElementById("msgType").value;
	if(msgType==1){
		$('#myModalCommunications').modal('show');
	}
}
function showCommunicationsForPhone()
{
	var phoneType=document.getElementById("phoneType").value;
	if(phoneType==1){
		$('#myModalPhone').modal('hide');	
		$('#myModalCommunications').modal('show');
	}
}
function addNoteFileType()
{
	$('#fileNotes').empty();
	$('#fileNotes').html("<a href='javascript:void(0);' onclick='removeNotesFile();'><img src='images/can-icon.png' title='remove'/></a> <input id='fileNote' name='fileNote' size='20' style='margin-left:20px;margin-top:-15px;' title='Choose a File' type='file'><br>&nbsp;"+resourceJSON.MaxSize10Mb);
}
function removeNotesFile()
{
	$('#fileNotes').empty();
	$('#fileNotes').html("<a href='javascript:void(0);' onclick='addNoteFileType();'><img src='images/attach.png'/></a> <a href='javascript:void(0);' onclick='addNoteFileType();'>"+resourceJSON.AttachFile+"</a>");
}
function addPhoneFileType()
{
	$('#filePhones').empty();
	$('#filePhones').html("<a href='javascript:void(0);' onclick='removePhoneFile();'><img src='images/can-icon.png' title='remove'/></a> <input id='filePhone' name='filePhone' size='20' style='margin-left:20px;margin-top:-15px;' title='Choose a File' type='file'><br>&nbsp;"+resourceJSON.MaxSize10Mb);
}
function removePhoneFile()
{
	$('#filePhones').empty();
	$('#filePhones').html("<a href='javascript:void(0);' onclick='addPhoneFileType();'><img src='images/attach.png'/></a> <a href='javascript:void(0);' onclick='addPhoneFileType();'>"+resourceJSON.AttachFile+"</a>");
}
function addMessageFileType()
{
	$('#fileMessages').empty();
	$('#fileMessages').html("<a href='javascript:void(0);' onclick='removeMessagesFile();'><img src='images/can-icon.png' title='remove'/></a> <input id='fileMessage' name='fileNote' size='20' style='margin-left:20px;margin-top:-15px;' title='Choose a File' type='file'><br>&nbsp;"+resourceJSON.MaxSize10Mb);
}
function removeMessagesFile()
{
	$('#fileMessages').empty();
	$('#fileMessages').html("<a href='javascript:void(0);' onclick='addMessageFileType();'><img src='images/attach.png'/></a> <a href='javascript:void(0);' onclick='addMessageFileType();'>"+resourceJSON.AttachFile+"</a>");
}

function getNotesDiv(teacherId,jobId)
{
	removeNotesFile();
	hideCommunicationsDiv();
	currentPageFlag='notes';
	document.getElementById("teacherId").value=teacherId;
	document.getElementById("teacherIdForNote").value=teacherId;
	$('#divTxtNode').find(".jqte_editor").html("");		
	$('#myModalNotes').modal('show');	
	$('#divTxtNode').find(".jqte_editor").css("background-color", "");
	document.getElementById("noteId").value="";
	$('#errordivNotes').empty();
}
function getMessageDiv(teacherId,emailId,jobId,msgType)
{
	removeMessagesFile();
	hideCommunicationsDiv();
	currentPageFlag='msg';
	document.getElementById("teacherDetailId").value=teacherId;	
	document.getElementById("teacherIdForMessage").value=teacherId;	
	document.getElementById("emailId").value=emailId;
	document.getElementById("jobId").value=jobId;
	document.getElementById("emailDiv").innerHTML=emailId;
	$('#messageSend').find(".jqte_editor").html("");
	document.getElementById("messageSubject").value="";
	$('#myModalMessage').modal('show');	
	$('#messageSubject').css("background-color", "");
	$('#messageSend').find(".jqte_editor").css("background-color", "");
	$('#messageSubject').focus();
	$('#errordivMessage').empty();
	document.getElementById("msgType").value=msgType;
	if(msgType==0){
		CandidateGridAjaxNew.getMessages(teacherId,jobId,pageMsg,noOfRowsMsg,sortOrderStrMsg,sortOrderTypeMsg,
		{
			async: false,
			errorHandler:handleError,
			callback:function(data)
			{	
				document.getElementById("divMessages").innerHTML=data;
				applyScrollOnMessageTbl();
			}
		});
	}else{
		document.getElementById("divMessages").innerHTML="";	
	}
	if( ($('#entityType').val()==2) || ($('#entityType').val()==3) || ($('#entityType').val()==5) || ($('#entityType').val()==6) )
	{
		getTemplatesByDistrictId(jobId);
		getDocuments();
	}
}


function getTemplatesByDistrictId(jobId)
{
	//alert(" jobId : "+jobId);
	CandidateGridAjaxNew.getTemplatesByDistrictId(jobId,
			{
				async: true,
				errorHandler:handleError,
				callback:function(data)
				{	
					//alert(" data : "+data+" Length : "+data.length);
					if(data!="" || data.length>0)
					{
						$("#templateDiv").fadeIn();
						$("#btnChangetemplatelink").hide();
						$("#districttemplate").html(data);
					}
				}
			});
}

function setTemplate()
{ 

	var templateId	=	$("#districttemplate").val();
	if(templateId==0)
		$("#btnChangetemplatelink").fadeOut();
	else
		$("#btnChangetemplatelink").fadeIn();
}

function getTemplate()
{
	var templateId	=	$("#districttemplate").val();
	var teacherId	=	$("#teacherDetailId").val();
	var jobId		=	$("#jobId").val();
	var confirmFlagforChangeTemplate = $("#confirmFlagforChangeTemplate").val(); // Gagan : confirmFlagforChangeTemplate 1 means confirm change template 
	//alert(" confirmFlagforChangeTemplate "+confirmFlagforChangeTemplate);
	
	if(templateId!=0)
	{
		CandidateGridAjaxNew.getTemplatesByTemplateId(templateId,jobId,teacherId,
		{
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{	
				//alert(" data : "+data+" Length : "+data.length);
				if(data!=null)
				{
					if($('#messageSubject').val() && $('#messageSend').find(".jqte_editor").text().trim()!="" && confirmFlagforChangeTemplate==0)
					{
						try{
							$('#confirmChangeTemplate').modal('show');
						}	
						catch (err){}
					}
					else
					{
						$('#messageSubject').val(data.subjectLine);
						$('#messageSend').find(".jqte_editor").html(data.templateBody);
						$("#confirmFlagforChangeTemplate").val(0);
					}
				}
			}
		});
	}
	return false;
}

function confirmChangeTemplate()
{
	$("#confirmFlagforChangeTemplate").val(1);
	getTemplate();
	try{
		$('#confirmChangeTemplate').modal('hide');
	}	
	catch (err){}
}
function showMessage(messageId)
{	
	CandidateGridAjaxNew.showMessage(messageId,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			document.getElementById("messageSubject").value=data.messageSubject;
			$('#messageSend').find(".jqte_editor").html(data.messageSend);
			$('#messageSend').find(".jqte_editor").focus();
		}
	});
}

function validateMessage()
{
	$('#messageSubject').css("background-color", "");
	$('#messageSend').find(".jqte_editor").css("background-color", "");
	var messageSend = $('#messageSend').find(".jqte_editor").html();
	var messageSubject = document.getElementById("messageSubject").value;
	var teacherId = document.getElementById("teacherDetailId").value;
	var jobId = document.getElementById("jobId").value;
	var messageDateTime = document.getElementById("messageDateTime").value;
	var documentname=document.getElementById("districtDocument").value;
	$('#errordivMessage').empty();
	var messageFileName="";
	var fileMessage=null;
	try{
		fileMessage=document.getElementById("fileMessage").value;
	}catch(e){}
	
	var cnt=0;
	var focs=0;
		if(trim(messageSubject)=="")
		{
			$('#errordivMessage').append("&#149; "+resourceJSON.PlzEtrSub+"<br>");
			if(focs==0)
				$('#messageSubject').focus();
			$('#messageSubject').css("background-color", "#F5E7E1");
			cnt++;focs++;
		}else if ($('#messageSend').find(".jqte_editor").text().trim()==""){

			$('#errordivMessage').append("&#149; "+resourceJSON.PlzEtrMsg+"<br>");
			if(focs==0)
				$('#messageSend').find(".jqte_editor").focus();
			$('#messageSend').find(".jqte_editor").css("background-color", "#F5E7E1");
			cnt++;focs++;
		}/*else{
			var charCount=$('#messageSend').find(".jqte_editor").text().trim();
			var count = charCount.length;
 			if(count>1000)
 			{
				$('#errordivMessage').append("&#149; Message length cannot exceed 1000 characters.<br>");
				if(focs==0)
					$('#messageSend').find(".jqte_editor").focus();
				$('#messageSend').find(".jqte_editor").css("background-color", "#F5E7E1");
				cnt++;focs++;
			}
		}*/
		
		if(fileMessage=="" && cnt==0){
			$('#errordivMessage').show();
			$('#errordivMessage').append("&#149; "+resourceJSON.PlzUplMsg+"<br>");
			cnt++;
		}else if(fileMessage!="" && fileMessage!=null)
		{
			var ext = fileMessage.substr(fileMessage.lastIndexOf('.') + 1).toLowerCase();	
			messageFileName="message"+messageDateTime+"."+ext;
			var fileSize = 0;
			if ($.browser.msie==true)
		 	{	
			    fileSize = 0;	   
			}
			else
			{
				if(document.getElementById("fileMessage").files[0]!=undefined)
				{
					fileSize = document.getElementById("fileMessage").files[0].size;
				}
			}
			
			if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt'|| ext=='xlsx' || ext=='xls'))
			{
				$('#errordivMessage').show();
				$('#errordivMessage').append("&#149; "+resourceJSON.PlzSelectAcceptMsg+"<br>");
				cnt++;
			}
			else if(fileSize>=10485760)
			{
				$('#errordivMessage').show();
				$('#errordivMessage').append("&#149; "+resourceJSON.FileSizeLessthan10Mb+"<br>");
				cnt++;
			}
		}
		if(cnt==0){	
			try{
				$('#loadingDiv').show();
				if(fileMessage!="" && fileMessage!=null){
					document.getElementById("frmMessageUpload").submit();
				}else{
					$('#lodingImage').append("<img src=\"images/loadingAnimation.gif\" /> "+resourceJSON.msgSending+"");
					CandidateGridAjaxNew.saveMessage(teacherId,messageSubject,messageSend,jobId,messageFileName,documentname,{ 
						async: true,
						errorHandler:handleError,
						callback:function(data)
						{		
							try{candidateNotReviewedUtil.setNotReviewedFlag(teacherId,"addMessage3");}catch(e){}
							$('#loadingDiv').hide();
							var senderEmail = document.getElementById("emailDiv").innerHTML;
							confMessage(teacherId,senderEmail,jobId);
						}
					});
					return true;
				}
			}catch(err){}
		}else{
			$('#errordivMessage').show();
			return false;
		}
}
function saveMessageFile(messageDateTime){
	$('#messageSubject').css("background-color", "");
	$('#messageSend').find(".jqte_editor").css("background-color", "");
	var messageSend = $('#messageSend').find(".jqte_editor").html();
	var messageSubject = document.getElementById("messageSubject").value;
	var teacherId = document.getElementById("teacherDetailId").value;
	var jobId = document.getElementById("jobId").value;
	$('#errordivMessage').empty();
	var messageFileName="";
	var fileMessage="";
	try{
		fileMessage=document.getElementById("fileMessage").value;
	}catch(e){}
	if(fileMessage!="" && fileMessage!=null)
	{
		var ext = fileMessage.substr(fileMessage.lastIndexOf('.') + 1).toLowerCase();	
		messageFileName="message"+messageDateTime+"."+ext;
	}
	CandidateGridAjaxNew.saveMessage(teacherId,messageSubject,messageSend,jobId,messageFileName,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{	
			$('#loadingDiv').hide();
			var senderEmail = document.getElementById("emailDiv").innerHTML;
			confMessage(teacherId,senderEmail,jobId);
		}
	});
	return true;
	
}
function confMessage(teacherId,senderEmail,jobId)
{	
	$('#lodingImage').empty();
	$('#myModalMessage').modal('hide');
	$('#myMsgShow').modal('show');
	document.getElementById("message2show").innerHTML=resourceJSON.MsgSentToCandidate;
}

function showMessageDiv()
{	
	var msgType=document.getElementById("msgType").value;
	$('#myMsgShow').modal('hide');
	$('#myModalMessage').modal('show');
	var teacherId = document.getElementById("teacherDetailId").value;
	var jobId = document.getElementById("jobId").value;
	var senderEmail = document.getElementById("emailDiv").innerHTML;
	var commDivFlag=document.getElementById("commDivFlag").value;

	if(msgType==1){
		$('#myMsgShow').modal('hide');
		$('#myModalMessage').modal('hide');
		getCommunicationsDiv(commDivFlag,jobForTeacherGId,teacherId,jobId);
	}else{
		document.getElementById('messageSubject').value="";
		document.getElementById('messageSend').value="";
		getMessageDiv(teacherId,senderEmail,jobId,0);
	}
}
function hideCommunicationsDiv()
{
	$('#myModalCommunications').modal('hide');
}
function saveNotes()
{	
	var notes = $('#divTxtNode').find(".jqte_editor").html();
	var teacherId = document.getElementById("teacherId").value;
	var jobId = document.getElementById("jobId").value;
	var noteId = document.getElementById("noteId").value;
	var noteDateTime=document.getElementById("noteDateTime").value;
	var commDivFlag=document.getElementById("commDivFlag").value;

	var fileNote=null;
	var noteFileName="";
	try{
		fileNote=document.getElementById("fileNote").value;
	}catch(e){}
	var cnt=0;
	var focs=0;	
	$('#errordivNotes').empty();
	if ($('#divTxtNode').find(".jqte_editor").text().trim()=="")
	{
		$('#errordivNotes').append("&#149; "+resourceJSON.PlzEtrNotes+"<br>");
		if(focs==0)
			$('#divTxtNode').find(".jqte_editor").focus();
		$('#divTxtNode').find(".jqte_editor").css("background-color", "#F5E7E1");
		cnt++;
	}else if(fileNote==""){
		$('#errordivNotes').show();
		$('#errordivNotes').append("&#149; "+resourceJSON.PlzUpldNotes+"<br>");
		cnt++;
	}else if(fileNote!="" && fileNote!=null)
	{
		var ext = fileNote.substr(fileNote.lastIndexOf('.') + 1).toLowerCase();	
		noteFileName="note"+noteDateTime+"."+ext;
		var fileSize = 0;
		if ($.browser.msie==true)
	 	{	
		    fileSize = 0;	   
		}
		else
		{
			if(document.getElementById("fileNote").files[0]!=undefined)
			{
				fileSize = document.getElementById("fileNote").files[0].size;
			}
		}
		
		if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt' || ext=='xlsx' || ext=='xls'))
		{
			$('#errordivNotes').show();
			$('#errordivNotes').append("&#149; "+resourceJSON.PlzSelectNoteFormat+"<br>");
			cnt++;
		}
		else if(fileSize>=10485760)
		{
			$('#errordivNotes').show();
			$('#errordivNotes').append("&#149; "+resourceJSON.FileSizeLessthan10Mb+"<br>");
			cnt++;
		}
	}
	if(cnt==0){	
		try{
			$('#loadingDiv').show();
			if(fileNote!="" && fileNote!=null){
				document.getElementById("frmNoteUpload").submit();
			}else{
				CandidateGridAjaxNew.saveNotes(teacherId,notes,noteId,noteFileName,
				{
					async:false,
					errorHandler:handleError,
					callback:function(data)
					{
						try{candidateNotReviewedUtil.setNotReviewedFlag(teacherId,"addMessage3");}catch(e){}
						$('#loadingDiv').hide();	
						showCommunicationsDiv();
						getCommunicationsDiv(commDivFlag,jobForTeacherGId,teacherId,jobId);				
					}
				});	
			}
		}catch(err){}
	}
}
function saveNoteFile(noteDateTime)
{	
	var notes = $('#divTxtNode').find(".jqte_editor").html();
	var teacherId = document.getElementById("teacherId").value;
	var jobId = document.getElementById("jobId").value;
	var noteId = document.getElementById("noteId").value;
	var commDivFlag=document.getElementById("commDivFlag").value;

	var fileNote=null;
	try{
		fileNote=document.getElementById("fileNote").value;
	}catch(e){}
	var noteFileName="";
	if(fileNote!="" && fileNote!=null)
	{
		var ext = fileNote.substr(fileNote.lastIndexOf('.') + 1).toLowerCase();	
		noteFileName="note"+noteDateTime+"."+ext;
	}
	CandidateGridAjaxNew.saveNotes(teacherId,notes,noteId,noteFileName,
	{
		async:false,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#loadingDiv').hide();
			showCommunicationsDiv();
			getCommunicationsDiv(commDivFlag,jobForTeacherGId,teacherId,jobId);				
		}
	});	
}
function getPhoneDetail(teacherId,jobForTeacherGId,phoneType){
	$('#myModalCommunications').modal('hide');
	document.getElementById("teacherIdForPhone").value=teacherId;
	$('#myModalCommunications').modal('hide');
	document.getElementById("calldetrailsdiv").style.display='inline';
	document.getElementById("calldetrailsbtn").style.display='inline';
	document.getElementById("divPhoneGrid").innerHTML="";
	document.getElementById("jobForTeacherGId").value=jobForTeacherGId;
	document.getElementById("phoneType").value=phoneType;
	
	$('#errordivPhone').empty();
	$('#divTxtPhone').find(".jqte_editor").html("");
	$('#divTxtPhone').find(".jqte_editor").css("background-color", "");
	removePhoneFile();
	document.getElementById("teacherDetailId").value=teacherId;
	
	var jobId = document.getElementById("jobId").value;
	currentPageFlag='phone';
	CandidateGridAjaxNew.getPhoneDetail(teacherId,jobId,1,1,"","",
	{
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
		$('#myModalPhone').modal('show');
		}
	});
}
function savePhone()
{	
	var phoneType=document.getElementById("phoneType").value;
	var callDetail = $('#divTxtPhone').find(".jqte_editor").html();
	var teacherId = document.getElementById("teacherDetailId").value;
	var jobId = document.getElementById("jobId").value;
	jobForTeacherGId=document.getElementById("jobForTeacherGId").value;
	var phoneDateTime = document.getElementById("phoneDateTime").value;
	var commDivFlag=document.getElementById("commDivFlag").value;

	var phoneFileName="";
	var filePhone=null;
	try{
		filePhone=document.getElementById("filePhone").value;
	}catch(e){}
	var cnt=0;
	var focs=0;	
	$('#errordivPhone').empty();
	if ($('#divTxtPhone').find(".jqte_editor").text().trim()=="")
	{
		cnt=1;
		$('#errordivPhone').append("&#149; "+resourceJSON.PlzEtrClDetail+"<br>");
		if(focs==0){
			$('#divTxtPhone').find(".jqte_editor").focus();
			$('#divTxtPhone').find(".jqte_editor").css("background-color", "#F5E7E1");
		}
	
	}else if($('#divTxtPhone').find(".jqte_editor").text().trim()!=""){
		var charCount=$('#divTxtPhone').find(".jqte_editor").text().trim();
		var count = charCount.length;
		if(count>2500)
		{
			$('#errordivPhone').append("&#149; "+resourceJSON.CallDetailNotExceed2500+"<br>");
			cnt=1;
		}
	}
	if(filePhone=="" && cnt==0){
		$('#errordivPhone').show();
		$('#errordivPhone').append("&#149; "+resourceJSON.PlzUpldCallDetail+"<br>");
		cnt++;
	}else if(filePhone!="" && filePhone!=null)
	{
		var ext = filePhone.substr(filePhone.lastIndexOf('.') + 1).toLowerCase();	
		phoneFileName="note"+phoneDateTime+"."+ext;
		var fileSize = 0;
		if ($.browser.msie==true)
	 	{	
		    fileSize = 0;	   
		}
		else
		{
			if(document.getElementById("filePhone").files[0]!=undefined)
			{
				fileSize = document.getElementById("filePhone").files[0].size;
			}
		}
		if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt'|| ext=='xlsx' || ext=='xls'))
		{
			$('#errordivPhone').show();
			$('#errordivPhone').append("&#149; "+resourceJSON.PlzSelectPhoneFormat+"<br>");
			cnt++;
		}
		else if(fileSize>=10485760)
		{
			$('#errordivPhone').show();
			$('#errordivPhone').append("&#149; "+resourceJSON.FileSizeLessthan10Mb+"<br>");
			cnt++;
		}
	}
	if(cnt==0){	
		try{
			$('#loadingDiv').show();
			if(filePhone!="" && filePhone!=null){
				document.getElementById("frmPhoneUpload").submit();
			}else{
				CandidateGridAjaxNew.savePhone(teacherId,callDetail,jobId,jobForTeacherGId,phoneFileName,
						{
							async:false,
							errorHandler:handleError,
							callback:function(data)
							{
								$('#loadingDiv').hide();
								if(phoneType==0){
									getPhoneDetail(teacherId,jobForTeacherGId,0);
								}else{
									showCommunicationsForPhone();
									getCommunicationsDiv(commDivFlag,jobForTeacherGId,teacherId,jobId);	
								}
							}
			});	
			}
		}catch(err){}
	}
}
function savePhoneFile(phoneDateTime)
{	
	var phoneType=document.getElementById("phoneType").value;
	var callDetail = $('#divTxtPhone').find(".jqte_editor").html();
	var teacherId = document.getElementById("teacherDetailId").value;
	var jobId = document.getElementById("jobId").value;
	jobForTeacherGId=document.getElementById("jobForTeacherGId").value;
	var commDivFlag=document.getElementById("commDivFlag").value;

	var phoneFileName="";
	var filePhone=null;
	try{
		filePhone=document.getElementById("filePhone").value;
	}catch(e){}
	
	if(filePhone!="" && filePhone!=null)
	{
		var ext = filePhone.substr(filePhone.lastIndexOf('.') + 1).toLowerCase();	
		phoneFileName="phone"+phoneDateTime+"."+ext;
	}
	CandidateGridAjaxNew.savePhone(teacherId,callDetail,jobId,jobForTeacherGId,phoneFileName,
	{
		async:false,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#loadingDiv').hide();
			if(phoneType==0){
				getPhoneDetail(teacherId,jobForTeacherGId,0);
			}else{
				
				showCommunicationsForPhone();
				getCommunicationsDiv(commDivFlag,jobForTeacherGId,teacherId,jobId);	
			}
		}
	});	
}
function deletesavefolder(rootNode)
{
	//alert("rootNode "+rootNode);
	$('#currentObject').val(rootNode.data.key);
	$('#deleteFolder').modal('show');
	
}
function deleteconfirm()
{
	//alert("delexcv          m");
	$('#deleteFolder').modal('hide');
	var rootNodeKey =$('#currentObject').val();
	//parent.deleteFolder();
	document.getElementById('iframeSaveCandidate').contentWindow.deleteFolder(rootNodeKey);
	//document.getElementById('target_Frame').contentWindow.callingtargetFunction();
	//window.frames["iframeSaveCandidate"].deleteFolder();
	//window.frames["original_preview_iframe"].exportAndView(img_id);//
}

function saveToFolderJFTNULL(teachersaveId,teacherId,flagpopover)
{
	currentPageFlag="stt";
	document.getElementById('teacherIdForHover').value=teacherId;
/*------------- Here I am creating only Home and Shared Folder For User*/	
	$('#saveToFolderDiv').modal('show');
	// added by Anurag 
	if(document.getElementById("iframeSaveCandidate")){
		// stop loading again and again.
	}
	else{
		try{
			$('#treeiFrameTP').html('<iframe id="iframeSaveCandidate"  src="tree.do" class="pull-left" scrolling="auto"  frameBorder="1" style="border: 0px; padding: 0px; margin: 0px; overflow:visible; text-align: top; vertical-align: top;width:220px;height:420px;"></iframe>');
		}catch(e){}
	
	
	}
	
	$('#errordiv').hide();
	var iframe = document.getElementById('iframeSaveCandidate');
	var innerDoc = iframe.contentDocument || iframe.contentWindow.document;
	
	innerDoc.getElementById('errortreediv').style.display="none";
	innerDoc.getElementById('errordeletetreediv').style.display="none";
	

	
	var folderId = innerDoc.getElementById('frame_folderId').value;
	if(innerDoc.getElementById('frame_folderId').value!="" || innerDoc.getElementById('frame_folderId').value.length>0)
	{
		var checkboxshowHideFlag =0; // For hide checkboxes on Save to Folder Div
		displaySavedCGgridByFolderId(0,folderId,checkboxshowHideFlag);
		// return false;
	}
}

function saveCandidateToFolderByUser()
{
	defaultTeacherGrid();
	document.getElementById('iframeSaveCandidate').contentWindow.checkFolderId();
	var iframe = document.getElementById('iframeSaveCandidate');
	var innerDoc = iframe.contentDocument || iframe.contentWindow.document;
	var folderId = innerDoc.getElementById('frame_folderId').value;
	//alert(" frame_folderId sdfsdfsdf "+folderId+" value "+folderId.value+" :::::: "+folderId.length);
	//alert(" folderId "+folderId);
	var frameHeight=iframe.contentWindow.document.body.scrollHeight;
	$('iframe').css('height',frameHeight);
	var fId="";
	if(innerDoc.getElementById('frame_folderId').value=="" || innerDoc.getElementById('frame_folderId').value.length==0)
	{
		//alert("Yes")
		innerDoc.getElementById('errortreediv').style.display="block";
		innerDoc.getElementById('errortreediv').innerHTML="&#149; "+resourceJSON.PlzSelectAnyFolder+"";
		 return false;
	}
	else
	{
		//alert(" Else No ");
		//fId=folderId;
	}
	//alert(" sgfsdgb "+innerDoc.getElementById('frame_folderId').value);

	
	
	var teacherId=	document.getElementById('teacherIdForHover').value
	
	CandidateGridAjaxNew.saveCandidateToFolderByUserFromTeacherId(teacherId,folderId,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			//alert(" data "+data);// data will be 2: when first time Home and shared folder will be created
			if(data==1)
			{
				$("#saveToFolderDiv").modal("hide");
				$('#saveAndShareConfirmDiv').html(resourceJSON.SavedCandidate);
				$('#shareConfirm').modal("show");
				//document.getElementById("savedCandidateGrid").innerHTML=data;
				//displaySaveCandidatePopUpDiv();
				//uncheckedAllCBX();
			}
			if(data==3)
			{
				//alert("Duplicate Candaidate found ");
				$("#txtoverrideFolderId").val(folderId);
				try{
					$("#duplicatCandidate").modal("show");
				}catch(err)
				  {}
			}
		}
	});
	 
	 
}
function saveWithDuplicateRecord()
{
	$("#saveToFolderDiv").modal("hide");
	$("#duplicatCandidate").modal("hide");
	
}

function displaySavedCGgridByFolderId(hoverDisplay,folderId,checkboxshowHideFlag)
{
	document.getElementById("checkboxshowHideFlag").value=checkboxshowHideFlag;
	document.getElementById("folderId").value=folderId;
	var pageFlag= document.getElementById("pageFlag").value;
	CandidateGridAjaxNew.displaySavedCGgridByFolderId(pageFlag,folderId,pageFlag,pageSTT,noOfRowsSTT,sortOrderStrSTT,sortOrderTypeSTT,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			if(data!=null)
			{
				document.getElementById("savedCandidateGrid").innerHTML=data;
				displaySaveCandidatePopUpDiv();
			}
		}
	});
}



/* ===== Gagan : displaySaveCandidatePopUpDiv() Method =========== */
function displaySaveCandidatePopUpDiv()
{
	
	var noOrRow = document.getElementById("saveCandidateTable").rows.length;
	for(var j=5000;j<=5000+noOrRow;j++)
	{
		
		$('.profile'+j).popover({ 
		 	trigger: "manual",
		    html : true,
		    placement: 'right',
		    content: $('#profileDetails'+j).html(),
		  }).on("mouseenter", function () {
		        var _this = this;
		        $(this).popover("show");
		        $(this).siblings(".popover").on("mouseleave", function () {
		            $(_this).popover('hide');
		        });
		    }).on("mouseleave", function () {
		        var _this = this;
		        setTimeout(function () {
		            if (!$(".popover:hover").length) {
		                $(_this).popover("hide")
		            }
		        }, 100);
		    });
	}
}



/*==================Gagan: Share Div Function Start Here ======================*/
function validateDelete()
{
	//var noOrRow = document.getElementById("saveCandidateTable").rows.length;
	//alert(" noOrRow "+noOrRow);
/*	for(var j=1;j<=noOrRow;j++)
	{
		
	}*/
	var savecandiadetidarray="";
	//var inputs = document.getElementsByTagName("input"); 
	var inputs = document.getElementsByName("mycbx"); 
	 for (var i = 0; i < inputs.length; i++) {
	        if (inputs[i].type === 'checkbox') {
	        	if(inputs[i].checked){
	            	savecandiadetidarray+=	inputs[i].value+",";
	            }
	        }
	 } 
	
	//alert(" errortreediv "+savecandiadetidarray);
	if(savecandiadetidarray=="" || savecandiadetidarray.length<0)
	{
		$("#errortreediv").show();
		document.getElementById("errortreediv").innerHTML="&#149; "+resourceJSON.msgSelectCandidate+"";
		return false;
	}
	else
	{
		$("#deleteShareCandidate").modal("show");
		$("#savecandiadetidarray").val(savecandiadetidarray);
	}
	//var rootNode =$("#tree").dynatree("getActiveNode");
	//alert(" rootNode "+rootNode);
}

function deleteCandidate()
{
	var rootNode =$("#tree").dynatree("getActiveNode");
	var folderId=rootNode.data.key;
	var checkboxshowHideFlag=1; // For displaying Check box
	var savecandiadetidarray=$("#savecandiadetidarray").val();
	//alert(" folderId "+folderId+" savecandiadetidarray "+savecandiadetidarray);
	CandidateGridAjaxNew.deleteCandidate(savecandiadetidarray,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			//alert(" ==== data === "+data);
		if(data==1)
		{
			displaySavedCGgridByFolderId(0,folderId,checkboxshowHideFlag)
			$('#deleteShareCandidate').modal('hide');
			
		}
	}
	});
}


function displayUsergrid(teachersavedId,flagpopover)
{
	teacherID= teachersavedId;
	
	// flagpopover=1 that means User has clicked on pop up
	//alert(" teachersavedId,flagpopover "+teachersavedId+" -------------"+flagpopover);
	/*if(teachersavedId!="")
	{
		$("#teachersavedIdFromSharePoPUp").val(teachersavedId);
		$("#txtteachersavedIdShareflagpopover").val(flagpopover);
	}	*/
	
	document.getElementById("teachersharedId").value=teachersavedId;
	document.getElementById("userFPS").value=0;
	document.getElementById("userFPD").value=1;
	document.getElementById("userCPS").value=0;
	document.getElementById("userCPD").value=0;
	currentPageFlag="us";
    
	$('#errorinvalidschooldiv').hide();
	if($('#entityType').val()==2)
	{
		//alert("---"+$('#schoolId').val())
		$('#schoolId').val("0");
		$('#shareSchoolName').val("");
	}
	else
	{
		if($('#entityType').val()==3)
		{
			//alert("-School Id--"+$('#schoolId').val())
			$('#schoolId').val($('#loggedInschoolId').val());
			$('#shareSchoolName').val($('#loggedInschoolName').val());
			//alert("-School Id--"+$('#schoolId').val())
		}
	}
	var savecandiadetidarray="";
	//var inputs = document.getElementsByTagName("input"); 
	var inputs = document.getElementsByName("mycbx"); 
	 for (var i = 0; i < inputs.length; i++) {
	        if (inputs[i].type === 'checkbox') {
	        	if(inputs[i].checked){
	            	savecandiadetidarray+=	inputs[i].value+",";
	            }
	        }
	 } 
	 $("#savecandiadetidarray").val(savecandiadetidarray);
	if(savecandiadetidarray!="" || flagpopover==1)
	{
		//$('#schoolId').val("0");
		//$('#schoolName').val("");
		//$('#shareDiv').modal('show');
		try{
			$('#shareDiv').modal('show');
			$('#loadingDiv').fadeIn();
			}catch(err){}
		var entityType	=	$('#entityType').val();
		var districtId	=	$('#districtId').val();
		var schoolId	=	trim($('#schoolId').val());
		//alert(" schoolId "+schoolId); 
		if(schoolId=="")
		{
			$('#errorinvalidschooldiv').show();
			$('#errorinvalidschooldiv').html("&#149; "+resourceJSON.SelectValidSchool+"");
			return false;
		}
		
		if($('#entityType').val()!="")
		{
			//alert(entityType+" "+districtId+" "+schoolId);
			/* ========= Display School List ============ */
			setTimeout(function(){
			CandidateGridAjaxNew.getDistictSchoolUserList(entityType,districtId,schoolId,pageUS,noOfRowsUS,sortOrderStrUS,sortOrderTypeUS,
				{ 
					async: false,
					errorHandler:handleError,
					callback:function(data)
					{	
						$('#loadingDiv').hide();
						//alert(" ==== data === "+data);
					if(data!="")
					{
						$('#divShareCandidateToUserGrid').html(data);
					}
				}
				});
			},800)
			
		}
		/*else
		{
			if(entityType==3)
			{
				alert("School Login");
			}
		}*/
	}
	else
	{
		$("#errortreediv").show();
		$('#errortreediv').html("&#149; "+resourceJSON.msgSelectCandidate+"s");
	}
	
}
function shareCandidate()
{
	$('#errorinvalidschooldiv').hide();
	$('#errortreediv').hide();
	$('#errorinvalidschooldiv').empty();
	var userIdarray =	"";
	var myUsercbx	=	document.getElementsByName("myUsercbx");
	 for (e=0;e<myUsercbx.length;e++) 
	 {
	  if (myUsercbx[e].checked==true) 
	  {
		  userIdarray+=myUsercbx[e].value+",";
	   }
	 }
	 	if(userIdarray=="")
	 	{
	 		$('#errorinvalidschooldiv').show();
	 		$('#errorinvalidschooldiv').html("&#149; "+resourceJSON.PlzSelectUsr+"");
	 	}
	 	savecandiadetidarray = $("#savecandiadetidarray").val();
	 	//alert(" userIdarray "+userIdarray+" savecandiadetidarray "+savecandiadetidarray);
		if($("#txtteachersavedIdShareflagpopover").val()==1 && $("#teachersavedIdFromSharePoPUp").val()!="")
		{
			savecandiadetidarray	=	$("#teachersavedIdFromSharePoPUp").val();
		}	
	 
		//alert(" savecandiadetidarray "+savecandiadetidarray);
		//return false;
		
		
		CandidateGridAjaxNew.shareCandidatesToUser(userIdarray,savecandiadetidarray,
		{ 
			async: false,
			errorHandler:handleError,
			callback:function(data)
			{	
				//alert(" ==== data === "+data);
			if(data==1)
			{
				$('#shareDiv').modal("hide");
				$('#saveAndShareConfirmDiv').html(resourceJSON.msgsuccessSharedCand);
				$('#shareConfirm').modal("show");
				uncheckAllCbx();
			}
		}
		});
}
function uncheckAllCbx()
{
	
	var inputs = document.getElementsByName("mycbx"); 
	 for (var i = 0; i < inputs.length; i++) {
	        if (inputs[i].type === 'checkbox') {
	        	if(inputs[i].checked){
	            	//savecandiadetidarray+=	inputs[i].value+",";
	        		inputs[i].checked=false;
	            }
	        }
	 } 
	
}
function searchUserthroughPopUp(searchFlag)
{
	if(searchFlag==1)
	{
		defaultShareDiv();
	}
	document.getElementById("userFPS").value=0;
	document.getElementById("userFPD").value=0;
	document.getElementById("userCPS").value=1;
	document.getElementById("userCPD").value=0;
	currentPageFlag="us";
	$('#errorinvalidschooldiv').hide();
		//$('#shareDiv').modal('show');
		try{
			$('#shareDiv').modal('show');
			}catch(err){}
		var entityType	=	$('#entityType').val();
		var districtId	=	$('#districtId').val();
		var schoolId	=	trim($('#schoolId').val());
		var schoolName	=	trim($('#shareSchoolName').val());
		if(schoolId=="")
		{
			if( schoolName!="")
			{
				$('#errorinvalidschooldiv').show();
				$('#errorinvalidschooldiv').html("&#149; "+resourceJSON.SelectValidSchool+"");
				return false;
			}
			else
			{
				schoolId=0;
			}
			
		}
		if($('#entityType').val()!="")
		{
			CandidateGridAjaxNew.getDistictSchoolUserList(entityType,districtId,schoolId,pageUS,noOfRowsUS,sortOrderStrUS,sortOrderTypeUS,
			{ 
				async: false,
				errorHandler:handleError,
				callback:function(data)
				{	
				if(data!="")
				{
					$('#divShareCandidateToUserGrid').html(data);
				}
			}
			});
		}	
}
function shareCandidatethroughPopUp()
{
	defaultTeacherGrid();
	$('#errorinvalidschooldiv').hide();
	$('#errorinvalidschooldiv').empty();
	var userIdarray =	"";
	var myUsercbx	=	document.getElementsByName("myUsercbx");
	var folderId = document.getElementById('folderId').value;
	
	 for (e=0;e<myUsercbx.length;e++) 
	 {
	  if (myUsercbx[e].checked==true) 
	  {
		  userIdarray+=myUsercbx[e].value+",";
	   }
	 }
	 	if(userIdarray=="")
	 	{
	 		$('#errorinvalidschooldiv').show();
	 		$('#errorinvalidschooldiv').html("&#149; "+resourceJSON.PlzSelectUsr+"");
	 	}
	 	var teachersharedId=document.getElementById("teachersharedId").value;
		CandidateGridAjaxNew.shareCandidatesToUserForTeacher(userIdarray,teachersharedId,folderId,
		{ 
			async: false,
			errorHandler:handleError,
			callback:function(data)
			{	
				//alert(" ==== data === "+data);
			if(data==1)
			{
				$('#shareDiv').modal("hide");
				$('#saveAndShareConfirmDiv').html(resourceJSON.msgsuccessSharedCand);
				$('#shareConfirm').modal("show");
				//uncheckedAllCBX();
			}
		}
		});
	 
	 
}


// Teacher Profile

//start from by ramesh


function showProfileContentClose(){
	$("#draggableDivMaster").hide(); // @AShish :: for hide Teacher Profile (draggable Div)*****
    //$('.profile').popover('destroy');
	
    currentPageFlag="";
    var selId=$("#pnqList option:selected").val();;
    var tId=document.getElementById("teacherIdForprofileGrid").value;    
    changePnqColor(selId,tId);
    document.getElementById("teacherIdForprofileGrid").value="";
}

function showProfileContentForTeacher(dis,teacherId,noOfRecordCheck,sVisitLocation,event)
{
	//alert("T Pool showProfileContentForTeacher");
	$('.allPopOver').popover('hide');
	var scrollTopPosition = $(window).scrollTop();
	//alert("scrollTopPosition "+scrollTopPosition);
	var x=event.clientX;
	var y=event.clientY+scrollTopPosition;
	
	//alert("Top "+y+" scrollTopPosition "+scrollTopPosition);
	var z=y-335;
	if(y>550)
	{
		z=y-672;
	}
	
	//var style = $('<style>.popover { position: absolute;top: '+z+'px !important;left: 0px;width: 770px; }</style>')
	//$('html > head').append(style);
	
	
	var districtMaster = null;
	showProfileContentClose();	
	$('#loadingDiv').show();
	document.getElementById("teacherIdForprofileGrid").value=teacherId;
	TeacherProfileViewInDivAjax.showProfileContentForTeacherNew(teacherId,noOfRecordCheck,sVisitLocation,districtMaster,0,{ 
			async: true,
			callback: function(data){
				  /*@Start 
					@Ashish 
					@Description :: for Teacher Profile(Draggable Div)*/
		          $("#draggableDivMaster").modal('show');
					//var wait="<div id='removeWait' style='text-align:center;padding-top:0px;'><img src='images/loadingAnimation.gif' /> <br>Loading...</div>";
					$("#draggableDiv").html(data);
			         
					TeacherProfileViewInDivAjax.showProfileContentForTeacherNew(teacherId,noOfRecordCheck,sVisitLocation,districtMaster,1,{ 
						async: true,
						callback: function(resdata){
						setTimeout(function () {
							$("#profile_id").after(resdata);
							$('#removeWait').remove();
				        }, 100);
						
						TeacherProfileViewInDivAjax.showProfilePartialData(teacherId,noOfRecordCheck,sVisitLocation,districtMaster,0,{ 
							async: true,
							callback: function(dataPartial){
							var prvsData= $("#draggableDiv").html()+dataPartial;
							$("#draggableDiv").html(prvsData);
							if($("#achievementSlider").length>0){
								var achmVal =$("#txt_sl_slider_aca_ach").val();
								$("#achievementSlider").html("<iframe id='ifrm_aca_ach' src='slideract.do?name=slider_aca_ach&tickInterval=5&max=20&swidth=240&svalue="+achmVal+"' scrolling='no' frameBorder='0' style='border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:280px;'></iframe>");
							}
							
							if($("#leadershipSlider").length>0){
								var leadVal =$("#txt_sl_slider_lea_res").val();
								$("#leadershipSlider").html("<iframe id='ifrm_lea_res' src='slideract.do?name=slider_lea_res&tickInterval=5&max=20&swidth=240&svalue="+leadVal+"' scrolling='no' frameBorder='0' style='border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:280px;'></iframe>");
							}
						}
						});
						//$("#draggableDiv").html(resdata);
					}
					});
					/*@End 
					@Ashish 
					@Description :: for Teacher Profile(Draggable Div)*/
				  $('#loadingDiv').hide();
			
				  //$('.profile').popover('show')
				  
				  $('#tpResumeprofile').tooltip();
				  $('#tpPhoneprofile').tooltip();
				  $('#tpPDReportprofile').tooltip();
				  $('#tpTeacherProfileVisitHistory').tooltip();
				  $('#teacherProfileTooltip').tooltip();
				  $('textarea').jqte();
				  try{
					  $('#tpDisconnect').tooltip();
				  }catch(e){}
				  /*currentPageFlag="workExp";
				  getPagingAndSorting('1',"CreatedDateTime",1);
				  
				  currentPageFlag="videoLink";
				  getPagingAndSorting('1',"createdDate",1);
				  
				  currentPageFlag="teacherAce";
				  getPagingAndSorting('1',"createdDateTime",1);
				  
				  currentPageFlag="teacherCerti";
				  getPagingAndSorting('1',"createdDateTime",1);*/
				  
				  //currentPageFlag="References";
				  //getPagingAndSorting('1',"lastName",1);
				  
	},
	errorHandler:handleError  
	});
} 

function getPhoneDetailShowPro(teacherId){
	hideProfilePopover();
	TeacherProfileViewInDivAjax.getPhoneDetailByDiv(teacherId,
	{
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			if(data==""){
				document.getElementById("divAlertText").innerHTML=resourceJSON.MsgPhNoIsNotAvailable;
				//$('#divAlert').modal('show');
				try
				{
					$('#divAlert').modal('show');
				}
				catch(err){}
			}
			else{
				document.getElementById("divPhoneByPro").innerHTML=data;			
				//$('#myModalPhoneShowPro').modal('show');
				try
				{
					$('#myModalPhoneShowPro').modal('show');
				}
				catch(err){}
			}
			
		}
	});
}


function getPFEmploymentGrid_DivProfile(teacherId)
{
	var visitLocation=document.getElementById("teacherIdForprofileGridVisitLocation").value;
	TeacherProfileViewInDivAjax.getGridDataWorkExpEmployment(teacherId,noOfRowsworkExp,pageworkExp,sortOrderStrworkExp,sortOrderTypeworkExp,visitLocation, { 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#gridDataWorkExpEmployment').html(data);
			applyScrollOnTblWorkExp();
		}});
}


function getElectronicReferencesGrid_DivProfile(teacherId)
{
	var districtMaster = null;
	var schoolMaster = null;
	
	var visitLocation=document.getElementById("teacherIdForprofileGridVisitLocation").value;
	var jobId=document.getElementById("jobId").value;
	TeacherProfileViewInDivAjax.getElectronicReferencesGrid(teacherId,noOfRowsReferences,pageReferences,sortOrderStrReferences,sortOrderTypeReferences,visitLocation,jobId,districtMaster,schoolMaster, { 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
		$('#gridDataReference').html(data);
		applyScrollOnTblEleRef_profile();
		}});
}


function getVideoLinksGrid_DivProfile(teacherId)
{
	var visitLocation=document.getElementById("teacherIdForprofileGridVisitLocation").value;
	TeacherProfileViewInDivAjax.getVideoLinksGrid(teacherId,noOfRowsvideoLink,pagevideoLink,sortOrderStrvideoLink,sortOrderTypevideoLink,visitLocation, { 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#gridDataVideoLink').html(data);
			applyScrollOnTblVideoLinks_profile();
		}});
}

//For Additional Document
function getadddocumentGrid_DivProfile(teacherId)
{
	//alert(noOfRowsaddDoc+":::::::::::::"+pageaddDoc+"::"+sortOrderStraddDoc+"::"+sortOrderTypeaddDoc+"::"+teacherId);
	var visitLocation=document.getElementById("teacherIdForprofileGridVisitLocation").value;
	TeacherProfileViewInDivAjax.getAdditionalDocumentsGrid(noOfRowsaddDoc,pageaddDoc,sortOrderStraddDoc,sortOrderTypeaddDoc,teacherId,visitLocation, { 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#divGridAdditionalDocuments').html(data);
			applyScrollOnTbl_AdditionalDocuments();

		}});
}

//For language profiency
function getlanguage_DivProfile(teacherId)
{
	//alert(noOfRowsaddDoc+":::::::::::::"+pageaddDoc+"::"+sortOrderStraddDoc+"::"+sortOrderTypeaddDoc+"::"+teacherId);
	var visitLocation=document.getElementById("teacherIdForprofileGridVisitLocation").value;
	TeacherProfileViewInDivAjax.displayTeacherLanguageByTeacherId(noOfRowsLang,pageLang,sortOrderStrLang,sortOrderTypeLang,teacherId,visitLocation, { 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
		$('#divGridlanguageProfiency').html(data);
		applyScrollOnTblLanguage();

		}});
}

//For Additional Document
function getdistrictAssessmetGrid_DivProfile(teacherId)
{
	//alert(noOfRowsaddDoc+":::::::::::::"+pageaddDoc+"::"+sortOrderStraddDoc+"::"+sortOrderTypeaddDoc+"::"+teacherId);
	var visitLocation=document.getElementById("teacherIdForprofileGridVisitLocation").value;
	TeacherProfileViewInDivAjax.getdistrictAssessmetGrid(noOfRowsASMT,pageASMT,sortOrderStrASMT,sortOrderTypeASMT,teacherId,visitLocation, { 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#divGridAssessmentDetails').html(data);
			applyScrollOnTbl_AssessmentDetails();

		}});
}


function setGridProfileVariable(currentPageFlagValue){
	currentPageFlag=currentPageFlagValue;
}

function getTeacherProfileVisitHistoryShow_FTime(teacherId){
	var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
	currentPageFlag="tpvh";
	getPagingAndSorting('1',"visitedDateTime",1);
	
	hideProfilePopover();
}

function getTeacherProfileVisitHistoryShow(teacherId){
	
	$('#loadingDiv').show();	
	currentPageFlag="tpvh";
	var visitLocation=document.getElementById("teacherIdForprofileGridVisitLocation").value;
	TeacherProfileViewInDivAjax.getGridDataForTeacherProfileVisitHistoryByTeacher(teacherId,noOfRowstpvh,pagetpvh,sortOrderStrtpvh,sortOrderTypetpvh,visitLocation, { 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#loadingDiv').hide();
			try{
			$('#divteacherprofilevisithistory').html(data);
			applyScrollOnTblProfileVisitHistory();
			$('#myModalProfileVisitHistoryShow').modal('show');
			}
			catch(e){}
			/*currentPageFlag="tpvh";
			getPagingAndSorting('1',"visitedDateTime",1);*/
			
		}});
}

function getTeacherAcademicsGrid_DivProfile(teacherId)
{
	var visitLocation=document.getElementById("teacherIdForprofileGridVisitLocation").value;
	TeacherProfileViewInDivAjax.getTeacherAcademicsGrid(teacherId,noOfRowsteacherAce,pageteacherAce,sortOrderStrteacherAce,sortOrderTypeteacherAce,visitLocation, { 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#gridDataTeacherAcademics').html(data);
			applyScrollOnTblTeacherAcademics_profile();
		}});
}

function getTeacherCertificationsGrid_DivProfile(teacherId)
{
	var visitLocation=document.getElementById("teacherIdForprofileGridVisitLocation").value;
	TeacherProfileViewInDivAjax.getTeacherCertificationsGrid(teacherId,noOfRowsteacherCerti,pageteacherCerti,sortOrderStrteacherCerti,sortOrderTypeteacherCerti,visitLocation, { 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#gridDataTeacherCertifications').html(data);
			applyScrollOnTblTeacherCertifications_profile();
		}});
}


function updateAScore(teacherId,districtId){
	
	var slider_aca_ach=0;
	var slider_lea_res=0;
	try{
		var ifrm_aca_ach = document.getElementById('ifrm_aca_ach');
		var innerNorm = ifrm_aca_ach.contentDocument || ifrm_aca_ach.contentWindow.document;
		if(innerNorm.getElementById('slider_aca_ach').value!=''){
			//$('#candidateFeedNormScore').val(innerNorm.getElementById('slider_aca_ach').value);
			slider_aca_ach=innerNorm.getElementById('slider_aca_ach').value;
		}
		}catch(e){}
		
		
	try{
		var ifrm_lea_res = document.getElementById('ifrm_lea_res');
		var innerNorm1 = ifrm_lea_res.contentDocument || ifrm_lea_res.contentWindow.document;
		if(innerNorm1.getElementById('slider_lea_res').value!=''){
			//$('#candidateFeedNormScore').val(innerNorm.getElementById('slider_aca_ach').value);
			slider_lea_res=innerNorm1.getElementById('slider_lea_res').value;
		}
		}catch(e){}
	
		var txt_sl_slider_aca_ach = document.getElementById('txt_sl_slider_aca_ach').value;
		var txt_sl_slider_lea_res = document.getElementById('txt_sl_slider_lea_res').value;
		
		//if((slider_aca_ach!=0 || slider_lea_res!=0) && ( txt_sl_slider_aca_ach!=slider_aca_ach || txt_sl_slider_lea_res!=slider_lea_res)){
		if(txt_sl_slider_aca_ach!=slider_aca_ach || txt_sl_slider_lea_res!=slider_lea_res){
			TeacherProfileViewInDivAjax.getUpdateAndSaveAScore(teacherId,districtId,slider_aca_ach,slider_lea_res,{ 
				async: true,
				errorHandler:handleError,
				callback:function(data)
				{
					if(data!="")
					{
						document.getElementById('txt_sl_slider_aca_ach').value=slider_aca_ach;
						document.getElementById('txt_sl_slider_lea_res').value=slider_lea_res;
						$('#ascore_profile').html(data);	
					}
				
				}});
		}
}

// Start for reference notes

function closeRefNotesDivEditor()
{
	var eleRefId=document.getElementById("eleRefId").value;
	getRefNotesDiv(eleRefId);
}

function getRefNotesDiv(eleRefId)
{	
	
	var districtMaster = null;
	var schoolMaster = null;
	
	document.getElementById("eleRefId").value=eleRefId;
	hideProfilePopover();
	$('#myModalReferenceNotesEditor').modal('hide');
	var jobId = document.getElementById("jobId").value;
	
	TeacherProfileViewInDivAjax.getRefeNotes(eleRefId,jobId,districtMaster,schoolMaster,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){
		
		$("#divRefNotesInner").html(data);
		$('#myModalReferenceNoteView').modal('show');
		//$('#myModalReferenceNoteView').show();
		$('#refeNotesId').tooltip();
	}
	});
}

function getNotesEditorDiv()
{
	$('#myModalReferenceNoteView').modal('hide');
	
	removeRefeNotesFile();
	$('#errordivNotes_ref').empty();
	$('#divTxtNode_ref').find(".jqte_editor").html("");	
	$('#divTxtNode_ref').find(".jqte_editor").css("background-color", "");
	$('#myModalReferenceNotesEditor').modal('show');
	
}


function saveReferenceNotes(){
	var noteFileName="";
	var notes = $('#divTxtNode_ref').find(".jqte_editor").html();
	var teacherId = document.getElementById("teacherIdForprofileGrid").value;
	var eleRefId = document.getElementById("eleRefId").value;
	var jobId = document.getElementById("jobId").value;
	
	var fileNote=null;
	
	try{
		fileNote=document.getElementById("fileNote_ref").value;
	}catch(e){}
	
	var cnt=0;
	var focs=0;
	
	$('#errordivNotes_ref').empty();
	
	if ($('#divTxtNode_ref').find(".jqte_editor").text().trim()=="")
	{
		$('#errordivNotes_ref').append("&#149; "+resourceJSON.PlzEtrNotes+"<br>");
		if(focs==0)
			$('#divTxtNode_ref').find(".jqte_editor").focus();
		$('#divTxtNode_ref').find(".jqte_editor").css("background-color", "#F5E7E1");
		cnt++;
	}else if(fileNote==""){
		$('#errordivNotes_ref').show();
		$('#errordivNotes_ref').append("&#149; "+resourceJSON.PlzUpldNotes+"<br>");
		cnt++;
	}else if(fileNote!="" && fileNote!=null)
	{
		var ext = fileNote.substr(fileNote.lastIndexOf('.') + 1).toLowerCase();	
		noteFileName="ref_note"+noteDateTime+"."+ext;
		var fileSize = 0;
		if ($.browser.msie==true)
	 	{	
		    fileSize = 0;	   
		}
		else
		{
			if(document.getElementById("fileNote_ref").files[0]!=undefined)
			{
				fileSize = document.getElementById("fileNote_ref").files[0].size;
			}
		}
		
		if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt' || ext=='xlsx' || ext=='xls'))
		{
			$('#errordivNotes_ref').show();
			$('#errordivNotes_ref').append("&#149; "+resourceJSON.PlzSelectNoteFormat+"<br>");
			cnt++;
		}
		else if(fileSize>=10485760)
		{
			$('#errordivNotes_ref').show();
			$('#errordivNotes_ref').append("&#149; "+resourceJSON.FileSizeLessthan10Mb+"<br>");
			cnt++;
		}
	}
	if(cnt==0){	
		try{
			$('#loadingDiv').show();
			if(fileNote!="" && fileNote!=null)
			{
				document.getElementById("frmNoteUpload_ref").submit();
			}
			else
			{
				TeacherProfileViewInDivAjax.saveReferenceNotesByAjax(eleRefId,teacherId,jobId,notes,noteFileName,
				{
					async:false,
					errorHandler:handleError,
					callback:function(data)
					{
						$('#loadingDiv').hide();
						getRefNotesDiv(eleRefId);
					}
				});	
			}
		}catch(err){}
	}

	
}

function saveRefNoteFile(noteDateTime)
{	
	var notes = $('#divTxtNode_ref').find(".jqte_editor").html();
	var teacherId = document.getElementById("teacherIdForprofileGrid").value;
	var eleRefId = document.getElementById("eleRefId").value;
	var jobId = document.getElementById("jobId").value;
	
	var fileNote=null;
	try{
		fileNote=document.getElementById("fileNote_ref").value;
	}catch(e){}
	
	var noteFileName="";
	
	if(fileNote!="" && fileNote!=null)
	{
		var ext = fileNote.substr(fileNote.lastIndexOf('.') + 1).toLowerCase();	
		noteFileName="ref_note"+noteDateTime+"."+ext;
	}
	
	TeacherProfileViewInDivAjax.saveReferenceNotesByAjax(eleRefId,teacherId,jobId,notes,noteFileName,
			{
				async:false,
				errorHandler:handleError,
				callback:function(data)
				{
					$('#loadingDiv').hide();
					getRefNotesDiv(eleRefId);
				}
			});	
}

function addRefeNoteFileType()
{
	$('#fileRefNotes').empty();
	$('#fileRefNotes').html("<a href='javascript:void(0);' onclick='removeRefeNotesFile();'><img src='images/can-icon.png' title='remove'/></a> <input id='fileNote_ref' name='fileNote_ref' size='20' style='margin-left:20px;margin-top:-15px;' title='Choose a File' type='file'><br>&nbsp;"+resourceJSON.MaxSize10Mb+"");
}

function removeRefeNotesFile()
{
	$('#fileRefNotes').empty();
	$('#fileRefNotes').html("<a href='javascript:void(0);' onclick='addRefeNoteFileType();'><img src='images/attach.png'/></a> <a href='javascript:void(0);' onclick='addRefeNoteFileType();'>"+resourceJSON.AttachFile+"</a>");
}

function downloadReferenceForCandidate(referenceId,teacherId,linkId)
{		
	//PFCertifications
	CGServiceAjax.downloadReferenceForCandidate(referenceId,teacherId,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			if(data=="")
			{
				data="javascript:void(0)";
			}
			else
			{
				if(deviceType)
				{
					if (data.indexOf(".doc")!=-1)
					{
						$("#docfileNotOpen").css({"z-index":"3000"});
				    	$('#docfileNotOpen').show();
					}
					else if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
					{
						$("#exelfileNotOpen").css({"z-index":"3000"});
				    	$('#exelfileNotOpen').show();
					}
					else
					{
						document.getElementById(linkId).href = data;	
					}
				}
				else if(deviceTypeAndroid)
				{
					if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
					{
						document.getElementById("ifrmRef").src=data;
					}
					else
					{
						document.getElementById(linkId).href = data;	
					}
				}
				else if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					document.getElementById("ifrmRef").src=data;
				}
				else
				{
					document.getElementById(linkId).href = data;	
				}			
			}
			return false;			
			
		}});
}

function downloadReferenceNotes(filePath,fileName,linkId)
{
	$('#loadingDiv').show();
	TeacherProfileViewInDivAjax.downloadReferenceNotes(filePath,fileName,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			$('#loadingDiv').hide();
			if(deviceType)
			{
				if (data.indexOf(".doc")!=-1)
				{
					$("#docfileNotOpen").css({"z-index":"3000"});
			    	$('#docfileNotOpen').show();
				}
				else if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					$("#exelfileNotOpen").css({"z-index":"3000"});
			    	$('#exelfileNotOpen').show();
				}
				else
				{
					document.getElementById(linkId).href = data; 
				}
			}
			else if(deviceTypeAndroid)
			{
				if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					 document.getElementById('ifrmTrans').src = ""+data+"";
				}
				else
				{
					document.getElementById(linkId).href = data; 
				}	
			}
			else if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
			{
				   document.getElementById('ifrmTrans').src = ""+data+"";
			}
			else
			{
				document.getElementById(linkId).href = data; 
			}
			return false;
		}
	});
}

function showProfilePopover()
{
		//$('.profile').popover('show');
		$("#draggableDivMaster").modal('show');
		//$('#myModalJobList').hide(); // @AShish :: Hide for Job Details(myModalJobList) Div
		//$('#myModalReferenceNoteView').hide();
		
		
		$("#myModalJobList").modal('hide');
		$("#myModalReferenceNoteView").modal('hide');
		
		//Refresh for Reference Notes
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getElectronicReferencesGrid_DivProfile(teacherIdForprofileGrid);
		
		$('#tpResumeprofile').tooltip();
		$('#tpPhoneprofile').tooltip();
		$('#tpPDReportprofile').tooltip();
		$('#tpTeacherProfileVisitHistory').tooltip();
		$('#teacherProfileTooltip').tooltip();
}
function hideProfilePopover()
{
	//$('.profile').popover('hide');
	$("#draggableDivMaster").modal('hide');
}
function getDistrictSpecificQuestion_DivProfile(districtId,teacherId,sVisitLocation)
{	if(sVisitLocation==null)sVisitLocation="";//alert(33333333333);
	
	TeacherProfileViewInDivAjax.getDistrictSpecificQuestionGrid_ByDistrict(districtId,teacherId,sVisitLocation,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#getDistrictSpecificQuestion').html(data);
		}});
}
function getQuestionExplain(questionID)
{
	TeacherProfileViewInDivAjax.getQAExplaination(questionID,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){
		$("#divExplain").html(data);
		try{
				$('#myModalQAEXEditor').show();
				$("#draggableDivMaster").hide();	
			}catch(e){
				//alert(e);
			}
		}
		});
}
function showPreviousModel()
{
	$("#draggableDivMaster").show();
	$('#myModalQAEXEditor').hide();
}
function showHideJbApld(showhide)
{
	if(showhide==0)
	{
		document.getElementById("jaFrom").style.display="none";
		document.getElementById("jaTo").style.display="none";
	}else
	{
		document.getElementById("jaFrom").style.display="block";
		document.getElementById("jaTo").style.display="block";
	}
}

function getJobDetailList(teacherId)
{
	flagvalue='jobDetail';
	setDefaltSearchValue();
	currentPageFlag="jobDetail";
	//alert(teacherId+" " +noOfRowsJobDetail+" " +pageJobDetail+" " +sortOrderStrJobDetail+" " +sortOrderTypeJobDetail);
	//teacherIdForjob,noOfRowsJob,pageJob,sortOrderStrJob,sortOrderTypeJob,false,visitLocatio
	TeacherInfotAjax.getJobDetailList(teacherId,noOfRowsJobDetail,pageJobDetail,sortOrderStrJobDetail,sortOrderTypeJobDetail,{ 
		async: false,
		callback: function(data)
		{
			$("#divJobList").html(data);
			applyScrollOnJobsTbl();
			$('#myJobDetailList').modal('show');
			getAutoNotificationFlag();
			$("#tId4Job").val(teacherId);
		},
		errorHandler:handleError 
	});
}

function getZoneListAll(districtId)
{
	//ManageJobOrdersAjax
	CGServiceAjax.getZoneListAll(districtId,{ 
		async: false,		
		callback: function(data)
		{
			if(data!=null && data!="")
			{			
				$("#zone").html(data);
				$("#zone").prop('disabled', false);
			}
			else{
				$("#zone").html(data);
				$("#zone").prop('disabled', true);
			}
		
		}
	});
}



function applyJob()
{
	if($('input[name=case]:checkbox:checked').length == 0 )
	{
		$('#message2show').html(resourceJSON.PlzSlctAtlstOneJobApply);
		$('#myModal2').modal('show');

	}else
	{
		var checkBox = document.getElementsByName("case");
		var sendnitification=0;
		if(document.getElementById("sendNotification").checked)
		{
			sendnitification=1;
		}
		var arr =[];
		for(i=0;i<checkBox.length;i++)
		{
			if(checkBox[i].checked==true)
				arr.push(checkBox[i].value);
		}
		$('#loadingDiv').show();
		TeacherInfotAjax.applyJobs($("#tId4Job").val(),arr,sendnitification,{ 
			async: true,
			callback: function(data)
			{
			if(data=="success"){
				try {
					$('#loadingDiv').hide();
					changeonspecificrows($("#tId4Job").val());
					$('#message2show').html(resourceJSON.JobApplySuccessfully);
					$('#myJobDetailList').modal('hide');
					$('#myModal2').modal('show');
					//displayTeacherGrid();
					
				} catch (e) {
				
				}
			}
				//getJobDetailList($("#tId4Job").val());
				//displayTeacherGrid();
			},
			errorHandler:handleError 
		});
	}
}
/* @Start
 * @Ashish Kumar
 * @Description :: Search Job List
 * */
function searchJobDetailList(teacherId)
{	
	var reqpos= $("#regpos").val();
	
	var geoZoneId       =   $("#zone").val();
	var schoolId = "";
	try{
		schoolId =	$("#schoolId1").val().trim();
	}catch(e){}
	var jobCategoryId	=	$("#jobCategoryId").val().trim();
	var branchId ="";
	try{
		branchId = $("#bnchId").val();
	}catch(e){}
	var jobId			=	$("#jobId").val().trim();
	var subjectId 		= 	document.getElementById('subjects1');
	var subjectIdList = "";

	for(var i=0; i<subjectId.options.length;i++)
	{	
		if(subjectId.options[i].selected == true){
			subjectIdList = subjectIdList+subjectId.options[i].value+",";
		}
	}
	
	currentPageFlag="jobDetail";
	TeacherInfotAjax.searchJobDetailList(teacherId,noOfRowsJobDetail,pageJobDetail,sortOrderStrJobDetail,sortOrderTypeJobDetail,schoolId,jobCategoryId,jobId,subjectIdList,geoZoneId,branchId,reqpos,{ 
		async: false,
		callback: function(data)
		{
			$("#divJobList").html(data);
			applyScrollOnJobsTbl();
			//$('#myJobDetailList').modal('show');
			$("#tId4Job").val(teacherId);
		},
		errorHandler:handleError 
	});
}

function setDefaltSearchValue()
{
	document.getElementById('subjects1').value="0";
	try{
	document.getElementById('schoolId1').value="";
	document.getElementById('schoolName1').value="";
	document.getElementById('jobCategoryId').value="0";
	document.getElementById('jobId').value="";
	}catch(e){}
}
/* @End
 * @Ashish Kumar
 * @Description :: Search Job List
 * */


function updateAScoreFromTeacherPool(teacherId,districtId)
{
	var slider_aca_ach=0;
	var slider_lea_res=0;
	try{
		var ifrm_aca_ach = document.getElementById('ifrm_aca_ach');
		var innerNorm = ifrm_aca_ach.contentDocument || ifrm_aca_ach.contentWindow.document;
		
		if(innerNorm.getElementById('slider_aca_ach').value!=''){
			//$('#candidateFeedNormScore').val(innerNorm.getElementById('slider_aca_ach').value);
			slider_aca_ach=innerNorm.getElementById('slider_aca_ach').value;
		}
		}catch(e){}
		
		
	try{
		var ifrm_lea_res = document.getElementById('ifrm_lea_res');
		var innerNorm1 = ifrm_lea_res.contentDocument || ifrm_lea_res.contentWindow.document;
		
		if(innerNorm1.getElementById('slider_lea_res').value!=''){
			//$('#candidateFeedNormScore').val(innerNorm.getElementById('slider_aca_ach').value);
			slider_lea_res=innerNorm1.getElementById('slider_lea_res').value;
		
		}
		}catch(e){}
	
		var txt_sl_slider_aca_ach = document.getElementById('txt_sl_slider_aca_ach').value;
		var txt_sl_slider_lea_res = document.getElementById('txt_sl_slider_lea_res').value;
		
		//if((slider_aca_ach!=0 || slider_lea_res!=0) && ( txt_sl_slider_aca_ach!=slider_aca_ach || txt_sl_slider_lea_res!=slider_lea_res)){
		if(txt_sl_slider_aca_ach!=slider_aca_ach || txt_sl_slider_lea_res!=slider_lea_res){
			TeacherProfileViewInDivAjax.getUpdateAndSaveAScoreFromTeacherPool(teacherId,districtId,slider_aca_ach,slider_lea_res,{ 
				async: true,
				errorHandler:handleError,
				callback:function(data)
				{
					if(data!="")
					{
						try{
							document.getElementById('txt_sl_slider_aca_ach').value=slider_aca_ach;
							document.getElementById('txt_sl_slider_lea_res').value=slider_lea_res;
							$('#ascore_profile').html(data[0]);
							$('#lrscore_profile').html(data[1]);
							$('#teacherPoolaScore_'+teacherId).html(data[0]);
							$('#teacherPoollrScore_'+teacherId).html(data[1]);
						}catch(e){}
					}
				}});
		}
}


function downloadTranscriptNew(academicId)
{	
	CandidateGridAjaxNew.downloadTranscript(academicId,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
		    //var deviceType=$.browser.device = (/ipad/i.test(navigator.userAgent.toLowerCase()));				
			if (data.indexOf(".doc") !=-1) 
			{				
				if(deviceType)
			    {
					$("#docfileNotOpen").css({"z-index":"3000"});
			    	$('#docfileNotOpen').show();
			    }
			    else
			    {
			    	    $('#modalDownloadsTranscript').modal('hide');
					    document.getElementById('ifrmTrans').src = ""+data+"";
			    }
			  
			}
			else if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
			{
				if(deviceType)
			    {
					$("#exelfileNotOpen").css({"z-index":"3000"});
			    	$('#exelfileNotOpen').show();
			    }
			    else
			    {
			    	    $('#modalDownloadsTranscript').modal('hide');
					    document.getElementById('ifrmTrans').src = ""+data+"";
			    }	
			}	
			else
			{
				document.getElementById('ifrmTrans').src = ""+data+"";
				$("#myModalTranscript").css({"z-index":"1000"});
				try{
					$('#modalDownloadsTranscript').modal('show');
				}catch(err)
				  {}
			}
		}
	});
}

function getSubjectByDistrict()
{
	var districtId = document.getElementById("districtId").value;
	
	if(districtId!=0){
		//DWRAutoComplete
		CGServiceAjax.getSubjectByDistrict(districtId,{ 
			async: false,		 // change by Anurag 
			callback: function(data)
			{
				document.getElementById("subjects").innerHTML=data;
			}
		});
	}
	else
	{
		document.getElementById("subjects").innerHTML="<option value='0'>"+resourceJSON.msgdistrict4relevantsubject+"</option>";
	}
	
}


function getJobCategoryByDistrict()
{
	var districtId = "";
	var headQuarterId = "";
	var branchId = "";
	try{
	districtId = document.getElementById("districtId").value;
	}
	catch(e){}
	try{
		headQuarterId = document.getElementById("headQuarterId").value;
		}
	catch(e){}
	try{
		branchId = document.getElementById("branchId").value;
		}
	catch(e){}
	
	if(districtId!="" || headQuarterId!="" || branchId!="")
	{
		//DWRAutoComplete
		CGServiceAjax.getJobCategoryByDistrict(districtId,{ 
			async: false,   // change By Anurag		
			callback: function(data)
			{
				document.getElementById("jobCateSelect").innerHTML=data;
			}
		});
	}
	else
	{
		document.getElementById("jobCateSelect").innerHTML="<option value='0'>"+resourceJSON.msgDistrictJobsCategorylist+"</option>";
	}
	
}



function zoneSchoolFlags()
{
	$('#geozoneschoolFlag').val(0);
	//currentPageFlag="";
	if (flagvalue=='jobList') 
	{
		$("#myModalJobList").modal('show');
	}
	else
	{		
		$("#myJobDetailList").modal('show');
	}
	
}

function displayZoneSchoolList()
{
	var geozoneId=$('#geozoneId').val();
	//ManageJobOrdersAjax
	CGServiceAjax.displayGeoZoneSchoolGrid(geozoneId,noOfRowsZS,pageZS,sortOrderStrZS,sortOrderTypeZS,{ 
		async: true,
		callback: function(data){
		$('#geozoneId').val(geozoneId);
		currentPageFlag="zone";
		$("#myModalJobList").modal('hide');
		$("#myJobDetailList").modal('hide');
		$("#geozoneschoolDiv").modal('show');
		$("#geozoneschoolGrid").html(data);
		applyScrollOnTblGeoZoneSchool();
		
	},
	errorHandler:handleError 
	});
}

function showZoneSchoolList(geozoneId,geozoneName,districtId)
{  
	pageZS=1;
	noOfRowsZS = 10;
	$('#geozoneId').val(geozoneId);
	$('#loadingDiv').show();
	$("#myModalLabelGeo").text(geozoneName);
	//ManageJobOrdersAjax
	CGServiceAjax.displayGeoZoneSchoolGrid(geozoneId,districtId,noOfRowsZS,pageZS,sortOrderStrZS,sortOrderTypeZS,{ 
		async: true,
		callback: function(data){
		currentPageFlag="zone";
		$("#myModalJobList").modal('hide');
		$("#myJobDetailList").modal('hide');
		$("#geozoneschoolDiv").modal('show');
		$("#geozoneschoolGrid").html(data);
		applyScrollOnTblGeoZoneSchool();
		$('#loadingDiv').hide();
	},
	errorHandler:handleError 
	});
}
function hideDiv()
{
	$('#docfileNotOpen').hide();
	$('#exelfileNotOpen').hide();
}
/////  tags  \\\\\\\

var arrTagCall =[];
function closeProfileOpenTags(teacherId,id)
{
	/*
	
	try { $("#draggableDivMaster").modal('hide');	} catch (e) {}
	$('#profile'+j).popover('hide');
	
	showTagDetailsTP(teacherId,j);
	
	$('#tagPreview'+j).popover('show');
	
	/*if($.inArray(parseInt(j), arrTagCall)==-1)
	{
		arrTagCall.push(parseInt(j));
		$('#tagPreview'+j).popover('show');
	}*/
	
	// Gourav code
	
	$("#draggableDivMaster").modal('hide');
	$('.allPopOver').popover('hide');
	$(".popover" ).remove();
	$("#tagPreview"+id).click();
	//document.getElementById('tagPreview'+id).click();
		
	/*$('.popup').css('left',event.pageX);      // <<< use pageX and pageY
    $('.popup').css('top',event.pageY);*/
   $('.popup').css('display','inline');     
   $(".popover-content").css("margin-top", "10px");
    
    //input[type=radio]
   if($('.popover').find('input[type=checkbox]').length>15){   
	$('.popover').css('width','70%');
   }else{
	   $('.popover').css('width','40%');
   }
	$('#currentPopUpId').val(id);
	var popH = $('.popover').css('top').replace('px','');
	var diff =$("#tagPreview"+id).offset().top-$(".arrow").offset().top;
	var k = Number(popH) + Number(diff); 
	$('.popover').css('top',k+'px');
	
	
	
}

function closeProfileOpenTagsTP(teacherId,j)
{
	showTagDetailsTP(teacherId,j);
	
	if($.inArray(parseInt(j), arrTagCall)==-1)
	{
		arrTagCall.push(parseInt(j));
		$('#tagPreview'+j).popover('show');
	}
	
}

function saveTags(teacherId){
	/*var checkValues = $('input[name=tagsValue]:checked').map(function()
            {
                return $(this).val();
            }).get();*/
var districtId = $("#districtId").val();

var checkboxes = document.getElementsByName('tagsValue'+teacherId);
var vals = "";
for (var i=0, n=checkboxes.length;i<n;i++) {
  if (checkboxes[i].checked) 
  {
  vals += ","+checkboxes[i].value;
  }
}
if(vals!="" && vals!='undefined'){
	try{candidateNotReviewedUtil.setNotReviewedFlag(teacherId,"ApplyGlobalTag2");}catch(e){}
}
if (vals) vals = vals.substring(1);
	TeacherInfotAjax.saveTags(districtId,teacherId,vals,null,{ 
		async: false,
		callback: function(data)
		{
			var currentPopUpId =  $('#currentPopUpId').val();
			$('#tagPreview'+currentPopUpId).popover('hide');
			searchTeacher();
		},
		errorHandler:handleError 
	});
	
}

function closeActAction(counter)
{
	$(".popover" ).remove();
	$('#tagPreview'+counter).popover('hide');
	
	
}


/*$(document).mousemove(function (e)
	{	
	    var container = $(".popover");
	    var currentPopUpId =  $('#currentPopUpId').val();
	    $(".popover").on("mouseenter", function () {		    	
	    })
	    .on("mouseleave", function () {
	    	$('#tagPreview'+currentPopUpId).popover('hide');
	    	
	    });   
	    
	});	*/

function setDialogPosition()
{
	var scrollTopPosition = $(window).scrollTop();
	var x=event.clientX; 
	var y=event.clientY;
	var z=y-180+scrollTopPosition;
	//z=z+80;
	return z;
}

function getJobListStatusChange(teacherId,jobId)
{
	$('#myModalStatus').modal('show');
	document.getElementById("jobId").value=jobId;
	document.getElementById("statusTeacherId").value=teacherId;
	document.getElementById("statusTeacherId_Global").value=teacherId;
	$('#loadingDiv').show();
	TeacherInfotAjax.getParametersForCGStatusChange(teacherId,jobId,
	{ 
		async: true,
		callback: function(data)
		{
			
			
			var outerJob=data[0];
			var sTeacherFullName=data[1];
			var doActivity=data[2];
			var cgStatus=data[3];
			var StatusShortName=data[4];
			var teacherAssessmentStatusId=data[5];
			var JobForTeacherId=data[6];
			if(outerJob=="true")
			{
				$('#loadingDiv').hide();
				showOuterStatus(sTeacherFullName,doActivity,cgStatus,StatusShortName,teacherAssessmentStatusId,JobForTeacherId,teacherId);
			}
			else
			{
				$('#myModalJobList').modal('hide');
				//$('#myModalStatus').modal('show');
				document.getElementById("referenceValue").value = "";
				document.getElementById("status_l_title").innerHTML=resourceJSON.MsgStatusLifeCycleFor+" "+sTeacherFullName;
				
				document.getElementById("statusTeacherId").value=teacherId;
				document.getElementById("jobForTeacherIdForSNote").value=JobForTeacherId;
				document.getElementById("doActivity").value=doActivity;
				
				$('#loadingDiv').hide();
				document.getElementById("divStatus").innerHTML=data[7];	
				
				if(doActivity=='true')
				{
					document.getElementById("statusSave").style.display="inline";
					document.getElementById("statusFinalize").style.display="inline";
				}else
				{
					document.getElementById("statusSave").style.display="none";
					document.getElementById("statusFinalize").style.display="none";
				}
				document.getElementById('email_da_sa').checked=false;
			}
		},
		errorHandler:handleError 
	});
	
}

function hideStatusCGTP()
{
	$('#myModalStatus').hide();
	var statusTeacherId=document.getElementById("statusTeacherId_Global").value=teacherId;
	//var statusTeacherId=document.getElementById("statusTeacherId").value;
	getJobList(statusTeacherId);
}
var teacherIdForEpiAndJsi;
function showEpiAndJsi1()
{
	showEpiAndJsi(teacherIdForEpiAndJsi);
}
function showEpiAndJsi(teacherid)
{	
	teacherIdForEpiAndJsi=teacherid;
	//alert(teacherid);
	$('#loadingDiv').show();
	currentPageFlag="epiJsi";
	TeacherInfotAjax.showEpiAndJsi(teacherid,noOfRowsEJ,pageEJ,sortOrderStrEJ,sortOrderTypeEJ,
	{ 
		async: true,
		callback: function(data)
		{
             $("#epiAndJsiData").html(data); 
             $("#epiAndJsi").modal('show');
             applyepiAndJsiTable();
     		 $('#loadingDiv').hide();             
		},
		errorHandler:handleError 
	});
	
	
}

function actionMenuTP(){
	var inputs = document.getElementsByTagName("input"); 
	var chkCount=0;
	var chkCheckedCount=0;
	 for (var i = 0; i < inputs.length; i++) {
	        if (inputs[i].type === 'checkbox') {
	        	chkCount++;
	            if(inputs[i].checked){
	            	chkCheckedCount++;
	            }
	        }
	 }
	 
	 if(chkCheckedCount>0){
		 document.getElementById("tpMenuActionDiv").style.display="inline";
	 }else{
		 document.getElementById("tpMenuActionDiv").style.display="none";
	 }
}

function getTeacherIdsMassTP()
{
	var TeacherIds = new Array();
	var inputs = document.getElementsByName("chkMassTP"); 
	var countTID=0;
	for (var i = 0; i < inputs.length; i++) 
	{
		if (inputs[i].type === 'checkbox') 
		{
			if(inputs[i].checked==true)
			{
				TeacherIds[countTID]=inputs[i].value;
				countTID=countTID+1;
	        }
	    }
	} 
	return TeacherIds;
}

function sendMessageMassTP()
{
	removeMessagesFileMassTP();
	var TeacherIds = new Array();
	TeacherIds	=	getTeacherIdsMassTP();
	try{
		$('#myModalMessageMassTP').modal('show');
	}catch(err){}
	document.getElementById("teacherIdForMessageMassTP").value=TeacherIds;
	
	$('#messageSendMassTP').find(".jqte_editor").html("");
	document.getElementById("messageSubjectMassTP").value="";
	$('#messageSubjectMassTP').css("background-color", "");
	$('#messageSendMassTP').find(".jqte_editor").css("background-color", "");
	$('#messageSubjectMassTP').focus();
	$('#errordivMessageMassTP').empty();
	
	TeacherInfotAjax.getEmailAddressMassTP(TeacherIds,
	{
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			document.getElementById("emailDivMassTP").innerHTML=data;
		}
	});
	
	getTemplatesByDistrictIdTP();
	getDocumentsTP();
}

function applyTags()
{
	var teacherIds="";
	$('input[name="chkMassTP"]:checked').each(function() {
		if(teacherIds=="")
			teacherIds=teacherIds+$(this).val();
		else
			teacherIds=teacherIds+","+$(this).val();
	});
		
	var checkboxValue="";
	var checkboxText="";
	var selectTagsValue="<table border='0' class='tablecgtbl'> <tbody>";
	$("#tagsearchId option").each(function()
	{
		checkboxValue = $(this).val();
		checkboxText = $(this).text();
		
		if(checkboxValue!="" && checkboxText!="")
		{
			selectTagsValue = selectTagsValue + "<tr>";
			selectTagsValue = selectTagsValue + "<td width='10%' style='padding-left:14px;line-height:0px;'>";
			selectTagsValue = selectTagsValue + "<input type='checkbox' name='tagsValue' id='"+teacherIds+"' value='"+checkboxValue+"'>";
			selectTagsValue = selectTagsValue + "</td>";
			selectTagsValue = selectTagsValue + "<td style='padding:0px 4px 4px 4px;line-height:19px;' class='/*tdAct*/'>";
			selectTagsValue = selectTagsValue + checkboxText;
			selectTagsValue = selectTagsValue + "</td>";
			selectTagsValue = selectTagsValue + "</tr>";
		}
	});
	selectTagsValue=selectTagsValue+"</tbody></table>";
	
	$('#applyTagsId').html(selectTagsValue);
	$("#tagsForMultipleCandidateDiv").modal("show");
}

function saveApplyTags()
{
	var districtId = $("#districtId").val();
	teacherIds = $('input[name="tagsValue"]:checked').attr("id");
	
	var vals = "";
	$('input[name="tagsValue"]:checked').each(function() {
		if(vals=="")
			vals = vals+this.value;
		else
			vals = vals+","+this.value;
	});
	TeacherInfotAjax.saveTags(districtId,null,vals,teacherIds,{
		async: false,
		callback: function(data)
		{
			$("#tagsForMultipleCandidateDiv").modal("hide");
			//pankaj sewalia
			try{
				var teachers = "", tempTeacher ="";
				if(typeof(teacherIds) == 'string')
					tempTeacher=teacherIds.split(",");
				else if(typeof(teacherIds) == 'array'|| typeof(teacherIds) == 'object')
					tempTeacher = teacherIds;
				for(var i=0; i<tempTeacher.length ; i++){
					if(teachers == "")
						teachers = tempTeacher[i];
					else
						teachers+="##"+tempTeacher[i];
				}
				candidateNotReviewedUtil.setNotReviewedFlag(teachers,"ApplyGlobalTag3");
			}catch(err) {	
				console.log("Exception in TeacherInfotAjax.saveTags:5922  "+err.message);
			}
			searchTeacher();
		},
		errorHandler:handleError
	});
}

function addMessageFileTypeMassTP()
{
	$('#fileMessagesMassTP').empty();
	$('#fileMessagesMassTP').html("<a href='javascript:void(0);' onclick='removeMessagesFileMassTP();'><img src='images/can-icon.png' title='remove'/></a> <input id='AttachFileMessagesMassTP' name='AttachFileMessagesMassTP' size='20' title='Choose a File' type='file' style='margin-left:20px;margin-top:-15px;'><br>&nbsp;"+resourceJSON.MaxSize10Mb+"");
}
function removeMessagesFileMassTP()
{
	$('#fileMessagesMassTP').empty();
	$('#fileMessagesMassTP').html("<a href='javascript:void(0);' onclick='addMessageFileTypeMassTP();'><img src='images/attach.png'/></a> <a href='javascript:void(0);' onclick='addMessageFileTypeMassTP();'>"+resourceJSON.AttachFile+"</a>");
}
function callByServletMassTP(sourcefileName,sourcefilePath)
{
	document.getElementById("sourcefileNameMassTP").value=sourcefileName;
	document.getElementById("sourcefilePathMassTP").value=sourcefilePath;
	
	validateMessageMassTP(99);
}

function validateMessageMassTP(source)
{
	var sourcefileNameMassTP="";
	var sourcefilePathMassTP="";
	if(source=="1")
	{
		document.getElementById("sourcefileNameMassTP").value="";
		document.getElementById("sourcefilePathMassTP").value="";
		$('#loadingDiv').show();
	}
	else if(source=="99")
	{
		sourcefileNameMassTP=document.getElementById("sourcefileNameMassTP").value;
		sourcefilePathMassTP=document.getElementById("sourcefilePathMassTP").value;
	}
	
	$('#messageSubjectMassTP').css("background-color", "");
	$('#messageSendMassTP').find(".jqte_editor").css("background-color", "");
	
	var messageSendMassTP = $('#messageSendMassTP').find(".jqte_editor").html();
	var documentname=document.getElementById("districtDocumentTP").value;
	
	var messageSubject = document.getElementById("messageSubjectMassTP").value;
	$('#errordivMessageMassTP').empty();
	
	var messageFileName="";
	var fileMessage=null;
	try{
		fileMessage=document.getElementById("AttachFileMessagesMassTP").value;
	}catch(e){}
	
	var cnt=0;
	var focs=0;
		if(trim(messageSubject)=="")
		{
			$('#errordivMessageMassTP').append("&#149; "+resourceJSON.PlzEtrSub+"<br>");
			if(focs==0)
				$('#messageSubjectMassTP').focus();
			$('#messageSubjectMassTP').css("background-color", "#F5E7E1");
			cnt++;focs++;
		}
		if ($('#messageSendMassTP').find(".jqte_editor").text().trim()=="")
		{
			$('#errordivMessageMassTP').append("&#149; "+resourceJSON.PlzEtrMsg+"<br>");
			if(focs==0)
				$('#messageSendMassTP').find(".jqte_editor").focus();
			$('#messageSendMassTP').find(".jqte_editor").css("background-color", "#F5E7E1");
			cnt++;focs++;
		}
		
		if(fileMessage=="" && cnt==0){
			$('#errordivMessageMassTP').show();
			$('#errordivMessageMassTP').append("&#149; "+resourceJSON.PlzUplMsg+"<br>");
			cnt++;
		}
		else if(fileMessage!="" && fileMessage!=null)
		{
			var ext = fileMessage.substr(fileMessage.lastIndexOf('.') + 1).toLowerCase();	
			var fileSize = 0;
			if ($.browser.msie==true)
		 	{	
			    fileSize = 0;	   
			}
			else
			{
				if(document.getElementById("AttachFileMessagesMassTP").files[0]!=undefined)
				{
					fileSize = document.getElementById("AttachFileMessagesMassTP").files[0].size;
				}
			}
			
			if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt'|| ext=='xlsx' || ext=='xls'))
			{
				$('#errordivMessageMassTP').show();
				$('#errordivMessageMassTP').append("&#149; "+resourceJSON.PlzSelectAcceptMsg+"<br>");
				cnt++;
			}
			else if(fileSize>=10485760)
			{
				$('#errordivMessageMassTP').show();
				$('#errordivMessageMassTP').append("&#149; "+resourceJSON.FileSizeLessthan10Mb+"<br>");
				cnt++;
			}
		}
		
		if(cnt==0)
		{	
			try{
				if(fileMessage!="" && fileMessage!=null && source=="1")
				{
					document.getElementById("frmMessageUploadMassTP").submit();
				}
				else
				{
					var filePath="";
					if(source=="99")
					{
						fileMessage=sourcefileNameMassTP;
						filePath=sourcefilePathMassTP;
					}
					if(documentname=="0")
						documentname="";
					
					var teacherIds = new Array();
					teacherIds=getTeacherIdsMassTP();
					TeacherInfotAjax.SendMessageMassTP(teacherIds,messageSubject,messageSendMassTP,fileMessage,filePath,documentname,
					{
						async: false,
						errorHandler:handleError,
						callback:function(data)
						{			
							sendMessageConfirmationMassTP();
							uncheckedAllCHKMassTP();					
							//pankaj sewalia
							try{
								var teachers = "";
								var tempTeacher ="";
								if(typeof(teacherIds) == 'string')
									tempTeacher=teacherIds.split(",");
								else if(typeof(teacherIds) == 'array' || typeof(teacherIds) == 'object')
									tempTeacher = teacherIds;
								for(var i=0; i<tempTeacher.length ; i++){
									if(teachers == "")
										teachers = tempTeacher[i];
									else
										teachers+="##"+tempTeacher[i];
								}
								candidateNotReviewedUtil.setNotReviewedFlag(teachers,"addMessage3");
							}catch(err) {console.log("Exception in TeacherInfotAjax.SendMessageMassTP:6078  "+err.message);}
							$('#loadingDiv').hide();

						}
					});
					
					return true;
				}
				
			}catch(err){}
		}
		else
		{
			$('#loadingDiv').hide();
		}
}

function sendMessageConfirmationMassTP()
{	
	try{
		$('#myModalMessageMassTP').modal('hide');
		$('#myModalMsgShow_SendMessageMassTP').modal('show');
	}catch(err)
	  {}
}

function uncheckedAllCHKMassTP()
{
	 var inputs = document.getElementsByName("chkMassTP"); 
	 for (var i = 0; i < inputs.length; i++) 
	 {
	        if (inputs[i].type === 'checkbox') 
	        {
	        	inputs[i].checked=false;
	        }
	 } 
	 document.getElementById("tpMenuActionDiv").style.display="none";
}
function openEpiTime(teacherId,teacherName,isBase){
//epiTimeModal	

	$('#loadingDiv').show();
	$('#assessmentSave').show();
	try {
		TeacherInfotAjax.teacherAssessmentTime(teacherId,{ 
			async: false,
			callback: function(data)
			{
				if(data!=""){
					$('#loadingDiv').hide();
					$( "#epiTimeModal").find(".modal-dialog").css("width", "35%");
					$('#assesmentRec').html(data);
					$('#teachername').html(""+resourceJSON.msgAssessmentSession+" "+teacherName);
					if(isBase==1){
						$('#assessmentSave').hide();
					}
					
					$('#epiTimeModal').modal('show');
					var result = $( "#epiTimeModal").find(".modal-header").innerHeight();
										
					if(result>48){		
						$( "#epiTimeModal").find(".modal-dialog").css("width", "50%");
					}
				}
				else{
					$('#loadingDiv').hide();
					alert("Some error");
				}
			},
			errorHandler:handleError 
		});		
				
	} catch (e) {}

}

function confirmPortfolioReminderMail(id)
{
	document.getElementById("teachId").value=id;
	$('#mailconfrmModal').modal('show');
}
function sendPortfolioReminderMail(){
	var id=document.getElementById("teachId").value;
	PFAcademics.deletePFAcademinGrid(id,{
	async: true,
	errorHandler:handleError,
	callback:function(data)
	{
		$('#deleteAcademicRecord').modal('hide');
		showGridAcademics();
		resetUniversityForm();
		return false;
	}});
}

function accomodation(quesSessT,assSessT){
	var quesSessT 	=	document.getElementById('defQST').value;
	var assSessT	=	document.getElementById('defAST').value;
	  if(document.getElementById('accomodation').checked==true){
		  var assessper 	= document.getElementById('asssessper').value;		  
		  var quesSessTime 	= (quesSessT/100)*assessper;
		  var assSessTime	= (assSessT/100)*assessper;
		  var newquesSess 	= quesSessTime+parseInt(quesSessT);
		 
		  $('#quesSessTime').html(Math.round(newquesSess));
		  $('#assSessTime').html(Math.round(assSessTime+parseInt(assSessT)));
	  }else{
		  $('#quesSessTime').html(quesSessT);
		  $('#assSessTime').html(assSessT);
	  }
}

function insertOrUpdateAssessmentTime(){
	$('#loadingDiv').show();
	var quesSessTime = $('#quesSessTime').html();
	var assSessTime  = $('#assSessTime').html();
	var updateBoth  = $('#updateBoth').val();
	var teacherId  = $('#teacherIdASS').val();	
	try {
		TeacherInfotAjax.updateTeacherWiseAssessmentTime(teacherId,quesSessTime,assSessTime,updateBoth,{ 
			async: false,
			callback: function(data)
			{
			
				if(data){
					document.getElementById('timeIcon'+teacherId).style.color='#007AB4';
					$('#epiTimeModal').modal('hide');
					$('#loadingDiv').hide();
				}
				else{
					alert(resourceJSON.msgSomeerror);
				}
			},
			errorHandler:handleError 
		});	
	} catch (e) {}
}

function downloadUploadedDocument(additionDocumentId,linkId,teacherid)
{		
	    $("#myModalLabelText").html("Document");
	    //PFCertifications
	    CGServiceAjax.downloadUploadedDocumentForCg(additionDocumentId,teacherid,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
			if(data=="")
			{
				data="javascript:void(0)";
			}
			else
			{
				if(deviceType)
				{
					if (data.indexOf(".doc")!=-1)
					{
						$("#docfileNotOpen").css({"z-index":"3000"});
				    	$('#docfileNotOpen').show();
					}
					else if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
					{
						$("#exelfileNotOpen").css({"z-index":"3000"});
				    	$('#exelfileNotOpen').show();
					}
					else
					{
						document.getElementById(linkId).href = data;
					}
				}
				else if(deviceTypeAndroid)
				{
					if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
					{
						document.getElementById("ifrmRef").src=data;
					}
					else
					{
						document.getElementById(linkId).href = data;
					}	
				}
				else if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					document.getElementById("ifrmRef").src=data;
				}
				else
				{
					try{
						document.getElementById('ifrmTransCommon').src = ""+data+"";
						$('#modalDownloadsCommon').modal('show');								
					}catch(err)
					  {
						alert(err)
					  }
				}			
			}	
			return false;			
		}});
}

var isSafari = /Safari/.test(navigator.userAgent) && /Apple Computer/.test(navigator.vendor);

/*============== Reports ==============*/

function downloadApplicantPoolExcelReport()
{
   try
   {
	   var mydiv = document.getElementById("mydiv");
       var curr_width = parseInt(mydiv.style.width);
      
       if(deviceType)
       {
    	   mydiv.style.width = 630+"px";  
       }
       else
	   {
    	   mydiv.style.width = 647+"px";  
	   } 
   }
   catch(e){alert(e)}
   /*.........................................*/
	currentPageFlag="";
	$('#advanceSearchDiv').hide();
	$('#searchLinkDiv').fadeIn();
	if(document.getElementById("districtId").value==0){
		document.getElementById('schoolName').readOnly=true;
	}
	if(document.getElementById("schoolName").value==''){
		document.getElementById("schoolId").value=0;
	}
	if(document.getElementById("districtName").value==''){
		document.getElementById("districtId").value=0;
	}
	if(document.getElementById("regionName").value==''){
		document.getElementById("regionId").value=0;
	}
	if(document.getElementById("universityName").value==''){
		document.getElementById("universityId").value=0;
	}
	if(document.getElementById("degreeName").value==''){
		document.getElementById("degreeId").value=0;
	}
	if(document.getElementById("certType").value==''){
		document.getElementById("certificateTypeMaster").value=0;
	}
	
	$('#loadingDiv').show();
	var firstName		=	trim(document.getElementById("firstName").value); 
	var lastName		=	trim(document.getElementById("lastName").value);
	var emailAddress	=	trim(document.getElementById("emailAddress").value);
	var districtId		=	trim(document.getElementById("districtId").value); 
	var schoolId		=	trim(document.getElementById("schoolId").value);
	var fromDate	=	trim(document.getElementById("fromDate").value);
	var toDate		=	trim(document.getElementById("toDate").value); 
	var certType		=	trim(document.getElementById("certificateTypeMaster").value);
	var regionId		=	trim(document.getElementById("regionId").value);
	var degreeId		=	trim(document.getElementById("degreeId").value);
	var universityId		=	trim(document.getElementById("universityId").value);
	var normScoreSelectVal		=	trim(document.getElementById("normScoreSelectVal").value);
	var qualQues		="";
	try{
		qualQues=trim(document.getElementById("qualQues").value);
	}catch(err){}
	
	var references	=	trim(document.getElementById("references").value);
	var resume		=	trim(document.getElementById("resume").value);
	
	var tnl = document.getElementById("subjects");  
	
	var arr =[];
	for(i=0;i<tnl.length;i++){
		if(tnl[i].selected == true){
			var dd =tnl[i].value;
			if(dd!=0)
			arr.push(dd);
		}  
	}  
	
	var fitScoreSelectVal		=	trim(document.getElementById("fitScoreSelectVal").value);
	var YOTESelectVal		=	trim(document.getElementById("YOTESelectVal").value);
	var AScoreSelectVal		=	trim(document.getElementById("AScoreSelectVal").value);
	var LRScoreSelectVal		=	trim(document.getElementById("LRScoreSelectVal").value);
	
	
	var canServeAsSubTeacherYes		=	document.getElementById("canServeAsSubTeacherYes").checked;
	var canServeAsSubTeacherNo		=	document.getElementById("canServeAsSubTeacherNo").checked;
	var canServeAsSubTeacherDA		=	document.getElementById("canServeAsSubTeacherDA").checked;
	var TFAA		=	document.getElementById("TFAA").checked
	var TFAC		=	document.getElementById("TFAC").checked;
	var TFAN		=	document.getElementById("TFAN").checked;
	
	try{
		var ifrmFit = document.getElementById('ifrmFit');
		var innerFit = ifrmFit.contentDocument || ifrmFit.contentWindow.document;
		var inputfitScore = innerFit.getElementById('fitScoreFrm');
		document.getElementById("fitScore").value=inputfitScore.value;
	}catch(e){}
	
	
	try{
		var ifrmYOTE = document.getElementById('ifrmYOTE');
		var innerYOTE = ifrmYOTE.contentDocument || ifrmYOTE.contentWindow.document;
		var inputYOTE = innerYOTE.getElementById('YOTEFrm');
		document.getElementById("YOTE").value=inputYOTE.value;
	}catch(e){}	
	
	
	try{
		var ifrmAScore = document.getElementById('ifrmAScore');
		var innerAScore = ifrmAScore.contentDocument || ifrmAScore.contentWindow.document;
		var inputAScore = innerAScore.getElementById('AScoreFrm');
		document.getElementById("AScore").value=inputAScore.value;
	}catch(e){}
	
	
	try{
		var ifrmLRScore = document.getElementById('ifrmLRScore');
		var innerLRScore = ifrmLRScore.contentDocument || ifrmLRScore.contentWindow.document;
		var inputLRScore = innerLRScore.getElementById('LRScoreFrm');
		document.getElementById("LRScore").value=inputLRScore.value;
	}catch(e){}	
	
	var jobAppliedFromDate	=	trim(document.getElementById("jobAppliedFromDate").value);
	var jobAppliedToDate	=	trim(document.getElementById("jobAppliedToDate").value); 
	var daysVal = $("input[name=jobApplied]:radio:checked").val();
	
	var internalCandidate=document.getElementById("internalCandidate").checked;
	var formalCandidate=document.getElementById("frmlCandidate").checked;
	var retiredCandidate=document.getElementById("retirdCandidate").checked;
	var candidatetype="";
	if(internalCandidate)
	{		
		candidatetype="0|";
	}
	if(formalCandidate)
	{		
		candidatetype=candidatetype+"2|";
	}
	if(retiredCandidate)
	{		
		candidatetype=candidatetype+"3|";
	}
	
	var stateId=document.getElementById("stateId").value;
	
	var stateForcandidate = document.getElementById("stateIdForcandidate").value;
	var certTypeForCandidate = document.getElementById("certificateTypeMasterForCandidate").value;

	try{
		var iframeNorm = document.getElementById('ifrmNorm');
		var innerNorm = iframeNorm.contentDocument || iframeNorm.contentWindow.document;
		var inputNormScore = innerNorm.getElementById('normScoreFrm');
		document.getElementById("normScore").value=inputNormScore.value;
	}catch(e){}	
	try{
		var iframeCGPA = document.getElementById('ifrmCGPA');
		var innerCGPA = iframeCGPA.contentDocument || iframeCGPA.contentWindow.document;
		var inputCGPA = innerCGPA.getElementById('CGPAFrm');
		document.getElementById("CGPA").value=inputCGPA.value;
	}catch(e){}
	
	var fitScore=document.getElementById("fitScore").value;
	var YOTE=document.getElementById("YOTE").value;
	var AScore=document.getElementById("AScore").value;
	var LRScore=document.getElementById("LRScore").value;
	
	var normScore		=	trim(document.getElementById("normScore").value);
	var CGPA		=	trim(document.getElementById("CGPA").value);
	var CGPASelectVal		=	trim(document.getElementById("CGPASelectVal").value);
	var candidateStatus		=	trim(document.getElementById("candidateStatus").value);
	var teacherSSN	=	trim(document.getElementById("ssn").value);
	var empNo	=	trim(document.getElementById("empNo").value);
	var jobCategoryIds='';
    var jobCategoryId   = document.getElementById("jobCateSelect");
    for (var i = 0; i < jobCategoryId.options.length; i++) {
        if(jobCategoryId.options[i].selected ==true){
        	if(jobCategoryIds==''){
        		jobCategoryIds=  jobCategoryId.options[i].value;
        	}
      	  	else{
      	  		jobCategoryIds=jobCategoryIds+ ","+jobCategoryId.options[i].value;
      	  	}
        }
    }
	var tagsearchId="";
    if(document.getElementById("tagsearchId"))
    	tagsearchId = trim(document.getElementById("tagsearchId").value);
	
    var hiddenCertTypeIds 	=	document.getElementsByName("hiddenCertTypeId");
	var certIds =[];
	for(var i=0;i<hiddenCertTypeIds.length;i++)
	{
		certIds.push(hiddenCertTypeIds[i].value);
	}
	var stateId2	=	trim(document.getElementById("stateId2").value);
	var zipCode		=	trim(document.getElementById("zipCode").value);
	var epiFromDate	=	trim(document.getElementById("epiFromDate").value);
	var epiToDate	=	trim(document.getElementById("epiToDate").value);
	
	//shadab
	var branchId ="";
	try{
		branchId = $("#bnchId").val();
		
	}catch(e){}
	var headQuarter = "";
 	 try{
 		headQuarter = document.getElementById("headQuarterId").value;
 		}catch(e){}
	
	if(branchId===undefined)
	{
		branchId ="";
	}	
	
	if(headQuarter!="" ||  branchId!="" )
	{
		exportType="excel";
		displayTeacherGrid();
	}	
	
	//end
	else
	{
		TeacherInfotAjax.exportTeacherGridonExcel(firstName,lastName,emailAddress,regionId,degreeId,universityId,normScoreSelectVal,qualQues,
				normScore,CGPA,CGPASelectVal,candidateStatus,stateId,candidatetype,noOfRows,page,sortOrderStr,sortOrderType,districtId,schoolId,fromDate,toDate,certType,references,resume,fitScoreSelectVal,YOTESelectVal,AScoreSelectVal,LRScoreSelectVal,canServeAsSubTeacherYes,canServeAsSubTeacherNo, 
				canServeAsSubTeacherDA,TFAA,TFAC,TFAN,fitScore,YOTE,AScore,LRScore,jobAppliedFromDate,jobAppliedToDate,daysVal,arr,$('#newcandidatesonly').is(":checked"),teacherSSN,stateForcandidate,certTypeForCandidate,empNo,jobCategoryIds,tagsearchId,stateId2,zipCode,certIds,epiFromDate,epiToDate,{ 
			async: true,
			callback: function(data)
			{
			
		    $('#loadingDiv').hide();
			if(deviceType)
			{					
					$("#exelfileNotOpen").css({"z-index":"3000"});
			    	$('#exelfileNotOpen').show();
			}
			else
			{
				try
				{
					document.getElementById('ifrmTrans').src = "applicantspool/"+data+"";
				}
				catch(e)
				{alert(e);}
			}
		},
			errorHandler:handleError 
		});
	}	
	
	
	
	
	
}

function downloadApplicantPoolPDFReport()
{
	
	 try
	   {
		   var mydiv = document.getElementById("mydiv");
	       var curr_width = parseInt(mydiv.style.width);
	      
	       if(deviceType)
	       {
	    	   mydiv.style.width = 630+"px";  
	       }
	       else
		   {
	    	   mydiv.style.width = 647+"px";  
		   } 
	   }
	   catch(e){alert(e)}
	   /*.........................................*/
		currentPageFlag="";
		$('#advanceSearchDiv').hide();
		$('#searchLinkDiv').fadeIn();
		if(document.getElementById("districtId").value==0){
			document.getElementById('schoolName').readOnly=true;
		}
		if(document.getElementById("schoolName").value==''){
			document.getElementById("schoolId").value=0;
		}
		if(document.getElementById("districtName").value==''){
			document.getElementById("districtId").value=0;
		}
		if(document.getElementById("regionName").value==''){
			document.getElementById("regionId").value=0;
		}
		if(document.getElementById("universityName").value==''){
			document.getElementById("universityId").value=0;
		}
		if(document.getElementById("degreeName").value==''){
			document.getElementById("degreeId").value=0;
		}
		if(document.getElementById("certType").value==''){
			document.getElementById("certificateTypeMaster").value=0;
		}
		
		$('#loadingDiv').show();
		var firstName		=	trim(document.getElementById("firstName").value); 
		var lastName		=	trim(document.getElementById("lastName").value);
		var emailAddress	=	trim(document.getElementById("emailAddress").value);
		var districtId		=	trim(document.getElementById("districtId").value); 
		var schoolId		=	trim(document.getElementById("schoolId").value);
		var fromDate	=	trim(document.getElementById("fromDate").value);
		var toDate		=	trim(document.getElementById("toDate").value); 
		var certType		=	trim(document.getElementById("certificateTypeMaster").value);
		var regionId		=	trim(document.getElementById("regionId").value);
		var degreeId		=	trim(document.getElementById("degreeId").value);
		var universityId		=	trim(document.getElementById("universityId").value);
		var normScoreSelectVal		=	trim(document.getElementById("normScoreSelectVal").value);
		var qualQues		="";
		try{
			qualQues=trim(document.getElementById("qualQues").value);
		}catch(err){}
		
		var references	=	trim(document.getElementById("references").value);
		var resume		=	trim(document.getElementById("resume").value);
		
		var tnl = document.getElementById("subjects");  
		
		var arr =[];
		for(i=0;i<tnl.length;i++){
			if(tnl[i].selected == true){
				var dd =tnl[i].value;
				if(dd!=0)
				arr.push(dd);
			}  
		}  
		
		var fitScoreSelectVal		=	trim(document.getElementById("fitScoreSelectVal").value);
		var YOTESelectVal		=	trim(document.getElementById("YOTESelectVal").value);
		var AScoreSelectVal		=	trim(document.getElementById("AScoreSelectVal").value);
		var LRScoreSelectVal		=	trim(document.getElementById("LRScoreSelectVal").value);
		
		
		var canServeAsSubTeacherYes		=	document.getElementById("canServeAsSubTeacherYes").checked;
		var canServeAsSubTeacherNo		=	document.getElementById("canServeAsSubTeacherNo").checked;
		var canServeAsSubTeacherDA		=	document.getElementById("canServeAsSubTeacherDA").checked;
		var TFAA		=	document.getElementById("TFAA").checked
		var TFAC		=	document.getElementById("TFAC").checked;
		var TFAN		=	document.getElementById("TFAN").checked;
		
		try{
			var ifrmFit = document.getElementById('ifrmFit');
			var innerFit = ifrmFit.contentDocument || ifrmFit.contentWindow.document;
			var inputfitScore = innerFit.getElementById('fitScoreFrm');
			document.getElementById("fitScore").value=inputfitScore.value;
		}catch(e){}
		
		
		try{
			var ifrmYOTE = document.getElementById('ifrmYOTE');
			var innerYOTE = ifrmYOTE.contentDocument || ifrmYOTE.contentWindow.document;
			var inputYOTE = innerYOTE.getElementById('YOTEFrm');
			document.getElementById("YOTE").value=inputYOTE.value;
		}catch(e){}	
		
		
		try{
			var ifrmAScore = document.getElementById('ifrmAScore');
			var innerAScore = ifrmAScore.contentDocument || ifrmAScore.contentWindow.document;
			var inputAScore = innerAScore.getElementById('AScoreFrm');
			document.getElementById("AScore").value=inputAScore.value;
		}catch(e){}
		
		
		try{
			var ifrmLRScore = document.getElementById('ifrmLRScore');
			var innerLRScore = ifrmLRScore.contentDocument || ifrmLRScore.contentWindow.document;
			var inputLRScore = innerLRScore.getElementById('LRScoreFrm');
			document.getElementById("LRScore").value=inputLRScore.value;
		}catch(e){}	
		
		var jobAppliedFromDate	=	trim(document.getElementById("jobAppliedFromDate").value);
		var jobAppliedToDate	=	trim(document.getElementById("jobAppliedToDate").value); 
		var daysVal = $("input[name=jobApplied]:radio:checked").val();
		
		var internalCandidate=document.getElementById("internalCandidate").checked;
		var formalCandidate=document.getElementById("frmlCandidate").checked;
		var retiredCandidate=document.getElementById("retirdCandidate").checked;
		
		var candidatetype="";
		if(internalCandidate)
		{		
			candidatetype="0|";
		}
		
			if(formalCandidate)
			{		
				candidatetype=candidatetype+"2|";
			}
			if(retiredCandidate)
			{		
				candidatetype=candidatetype+"3|";
			}
		
		var stateId=document.getElementById("stateId").value;
		
		var stateForcandidate = document.getElementById("stateIdForcandidate").value;
		var certTypeForCandidate = document.getElementById("certificateTypeMasterForCandidate").value;

		try{
			var iframeNorm = document.getElementById('ifrmNorm');
			var innerNorm = iframeNorm.contentDocument || iframeNorm.contentWindow.document;
			var inputNormScore = innerNorm.getElementById('normScoreFrm');
			document.getElementById("normScore").value=inputNormScore.value;
		}catch(e){}	
		try{
			var iframeCGPA = document.getElementById('ifrmCGPA');
			var innerCGPA = iframeCGPA.contentDocument || iframeCGPA.contentWindow.document;
			var inputCGPA = innerCGPA.getElementById('CGPAFrm');
			document.getElementById("CGPA").value=inputCGPA.value;
		}catch(e){}
		
		var fitScore=document.getElementById("fitScore").value;
		var YOTE=document.getElementById("YOTE").value;
		var AScore=document.getElementById("AScore").value;
		var LRScore=document.getElementById("LRScore").value;
		
		var normScore		=	trim(document.getElementById("normScore").value);
		var CGPA		=	trim(document.getElementById("CGPA").value);
		var CGPASelectVal		=	trim(document.getElementById("CGPASelectVal").value);
		var candidateStatus		=	trim(document.getElementById("candidateStatus").value);
		var teacherSSN	=	trim(document.getElementById("ssn").value);
		var empNo	=	trim(document.getElementById("empNo").value);
		var jobCategoryIds='';
	    var jobCategoryId   = document.getElementById("jobCateSelect");
	    for (var i = 0; i < jobCategoryId.options.length; i++) {
	        if(jobCategoryId.options[i].selected ==true){
	        	if(jobCategoryIds==''){
	        		jobCategoryIds=  jobCategoryId.options[i].value;
	        	}
	      	  	else{
	      	  		jobCategoryIds=jobCategoryIds+ ","+jobCategoryId.options[i].value;
	      	  	}
	        }
	    }
		var tagsearchId="";
	    if(document.getElementById("tagsearchId"))
	    	tagsearchId = trim(document.getElementById("tagsearchId").value);
		
	    var hiddenCertTypeIds 	=	document.getElementsByName("hiddenCertTypeId");
		var certIds =[];
		for(var i=0;i<hiddenCertTypeIds.length;i++)
		{
			certIds.push(hiddenCertTypeIds[i].value);
		}
		var stateId2	=	trim(document.getElementById("stateId2").value);
		var zipCode		=	trim(document.getElementById("zipCode").value);
		var epiFromDate	=	trim(document.getElementById("epiFromDate").value);
		var epiToDate	=	trim(document.getElementById("epiToDate").value);
		
		//shadab
		var branchId ="";
		try{
			branchId = $("#bnchId").val();
		}catch(e){}
		var headQuarter = "";
	 	 try{
	 		headQuarter = document.getElementById("headQuarterId").value;
	 		}catch(e){}
 		if(branchId===undefined)
 		{
 			branchId ="";
 		}	
		if(headQuarter!="" || branchId!="")
		{
			exportType="pdf";
			displayTeacherGrid();
		}
		else
		{
			TeacherInfotAjax.exportTeacherGridonPDF(firstName,lastName,emailAddress,regionId,degreeId,universityId,normScoreSelectVal,qualQues,
					normScore,CGPA,CGPASelectVal,candidateStatus,stateId,candidatetype,noOfRows,page,sortOrderStr,sortOrderType,districtId,schoolId,fromDate,toDate,certType,references,resume,fitScoreSelectVal,YOTESelectVal,AScoreSelectVal,LRScoreSelectVal,canServeAsSubTeacherYes,canServeAsSubTeacherNo, 
					canServeAsSubTeacherDA,TFAA,TFAC,TFAN,fitScore,YOTE,AScore,LRScore,jobAppliedFromDate,jobAppliedToDate,daysVal,arr,$('#newcandidatesonly').is(":checked"),teacherSSN,stateForcandidate,certTypeForCandidate,empNo,jobCategoryIds,tagsearchId,stateId2,zipCode,certIds,epiFromDate,epiToDate,{ 
				async: true,
				callback: function(data)
				{
		
				$('#loadingDiv').hide();	
				if(deviceType || deviceTypeAndroid)
				{
					window.open(data,'left=200,top=50,width=700,height=600,toolbar=1,resizable=0');			
				}		
				else
				{
					$('#modalDownloadApplicantpool').modal('hide');
					document.getElementById('ifrmCJS').src = ""+data+"";
					try{
						$('#modalDownloadApplicantpool').modal('show');
					}catch(err)
					{}		
			     }		
			     return false;
			}});
		}	
		
		
}


function printApplicantPoolPreview()
{
 try
 {
	   var mydiv = document.getElementById("mydiv");
     var curr_width = parseInt(mydiv.style.width);
    
     if(deviceType)
     {
  	   mydiv.style.width = 630+"px";  
     }
     else
	   {
  	   mydiv.style.width = 647+"px";  
	   } 
 }
 catch(e){alert(e)}
 /*.........................................*/
	currentPageFlag="";
	$('#advanceSearchDiv').hide();
	$('#searchLinkDiv').fadeIn();
	if(document.getElementById("districtId").value==0){
		document.getElementById('schoolName').readOnly=true;
	}
	if(document.getElementById("schoolName").value==''){
		document.getElementById("schoolId").value=0;
	}
	if(document.getElementById("districtName").value==''){
		document.getElementById("districtId").value=0;
	}
	if(document.getElementById("regionName").value==''){
		document.getElementById("regionId").value=0;
	}
	if(document.getElementById("universityName").value==''){
		document.getElementById("universityId").value=0;
	}
	if(document.getElementById("degreeName").value==''){
		document.getElementById("degreeId").value=0;
	}
	if(document.getElementById("certType").value==''){
		document.getElementById("certificateTypeMaster").value=0;
	}
	
	$('#loadingDiv').show();
	var firstName		=	trim(document.getElementById("firstName").value); 
	var lastName		=	trim(document.getElementById("lastName").value);
	var emailAddress	=	trim(document.getElementById("emailAddress").value);
	var districtId		=	trim(document.getElementById("districtId").value); 
	var schoolId		=	trim(document.getElementById("schoolId").value);
	var fromDate	=	trim(document.getElementById("fromDate").value);
	var toDate		=	trim(document.getElementById("toDate").value); 
	var certType		=	trim(document.getElementById("certificateTypeMaster").value);
	var regionId		=	trim(document.getElementById("regionId").value);
	var degreeId		=	trim(document.getElementById("degreeId").value);
	var universityId		=	trim(document.getElementById("universityId").value);
	var normScoreSelectVal		=	trim(document.getElementById("normScoreSelectVal").value);
	var qualQues		="";
	
	try{
		qualQues=trim(document.getElementById("qualQues").value);
	}catch(err){}
	
	var references	=	trim(document.getElementById("references").value);
	var resume		=	trim(document.getElementById("resume").value);
	
	var tnl = document.getElementById("subjects");  
	
	var arr =[];
	for(i=0;i<tnl.length;i++){
		if(tnl[i].selected == true){
			var dd =tnl[i].value;
			if(dd!=0)
			arr.push(dd);
		}  
	}  
	
	var fitScoreSelectVal		=	trim(document.getElementById("fitScoreSelectVal").value);
	var YOTESelectVal		=	trim(document.getElementById("YOTESelectVal").value);
	var AScoreSelectVal		=	trim(document.getElementById("AScoreSelectVal").value);
	var LRScoreSelectVal		=	trim(document.getElementById("LRScoreSelectVal").value);
	
	
	var canServeAsSubTeacherYes		=	document.getElementById("canServeAsSubTeacherYes").checked;
	var canServeAsSubTeacherNo		=	document.getElementById("canServeAsSubTeacherNo").checked;
	var canServeAsSubTeacherDA		=	document.getElementById("canServeAsSubTeacherDA").checked;
	var TFAA		=	document.getElementById("TFAA").checked
	var TFAC		=	document.getElementById("TFAC").checked;
	var TFAN		=	document.getElementById("TFAN").checked;
	
	try{
		var ifrmFit = document.getElementById('ifrmFit');
		var innerFit = ifrmFit.contentDocument || ifrmFit.contentWindow.document;
		var inputfitScore = innerFit.getElementById('fitScoreFrm');
		document.getElementById("fitScore").value=inputfitScore.value;
	}catch(e){}
	
	
	try{
		var ifrmYOTE = document.getElementById('ifrmYOTE');
		var innerYOTE = ifrmYOTE.contentDocument || ifrmYOTE.contentWindow.document;
		var inputYOTE = innerYOTE.getElementById('YOTEFrm');
		document.getElementById("YOTE").value=inputYOTE.value;
	}catch(e){}	
	
	
	try{
		var ifrmAScore = document.getElementById('ifrmAScore');
		var innerAScore = ifrmAScore.contentDocument || ifrmAScore.contentWindow.document;
		var inputAScore = innerAScore.getElementById('AScoreFrm');
		document.getElementById("AScore").value=inputAScore.value;
	}catch(e){}
	
	
	try{
		var ifrmLRScore = document.getElementById('ifrmLRScore');
		var innerLRScore = ifrmLRScore.contentDocument || ifrmLRScore.contentWindow.document;
		var inputLRScore = innerLRScore.getElementById('LRScoreFrm');
		document.getElementById("LRScore").value=inputLRScore.value;
	}catch(e){}	
	
	var jobAppliedFromDate	=	trim(document.getElementById("jobAppliedFromDate").value);
	var jobAppliedToDate	=	trim(document.getElementById("jobAppliedToDate").value); 
	var daysVal = $("input[name=jobApplied]:radio:checked").val();
	
	var internalCandidate=document.getElementById("internalCandidate").checked;
	var formalCandidate=document.getElementById("frmlCandidate").checked;
	var retiredCandidate=document.getElementById("retirdCandidate").checked;
	var candidatetype="";
	
	if(internalCandidate)
	{		
		candidatetype="0|";
	}
	
		if(formalCandidate)
		{		
			candidatetype=candidatetype+"2|";
		}
		if(retiredCandidate)
		{		
			candidatetype=candidatetype+"3|";
		}
		
	var stateId=document.getElementById("stateId").value;
	
	var stateForcandidate = document.getElementById("stateIdForcandidate").value;
	var certTypeForCandidate = document.getElementById("certificateTypeMasterForCandidate").value;

	try{
		var iframeNorm = document.getElementById('ifrmNorm');
		var innerNorm = iframeNorm.contentDocument || iframeNorm.contentWindow.document;
		var inputNormScore = innerNorm.getElementById('normScoreFrm');
		document.getElementById("normScore").value=inputNormScore.value;
	}catch(e){}	
	try{
		var iframeCGPA = document.getElementById('ifrmCGPA');
		var innerCGPA = iframeCGPA.contentDocument || iframeCGPA.contentWindow.document;
		var inputCGPA = innerCGPA.getElementById('CGPAFrm');
		document.getElementById("CGPA").value=inputCGPA.value;
	}catch(e){}
	
	var fitScore=document.getElementById("fitScore").value;
	var YOTE=document.getElementById("YOTE").value;
	var AScore=document.getElementById("AScore").value;
	var LRScore=document.getElementById("LRScore").value;
	
	var normScore		=	trim(document.getElementById("normScore").value);
	var CGPA		=	trim(document.getElementById("CGPA").value);
	var CGPASelectVal		=	trim(document.getElementById("CGPASelectVal").value);
	var candidateStatus		=	trim(document.getElementById("candidateStatus").value);
	var teacherSSN	=	trim(document.getElementById("ssn").value);
	var empNo	=	trim(document.getElementById("empNo").value);
	var jobCategoryIds='';
    var jobCategoryId   = document.getElementById("jobCateSelect");
    for (var i = 0; i < jobCategoryId.options.length; i++) {
        if(jobCategoryId.options[i].selected ==true){
        	if(jobCategoryIds==''){
        		jobCategoryIds=  jobCategoryId.options[i].value;
        	}
      	  	else{
      	  		jobCategoryIds=jobCategoryIds+ ","+jobCategoryId.options[i].value;
      	  	}
        }
    }
	var tagsearchId="";
    if(document.getElementById("tagsearchId"))
    	tagsearchId = trim(document.getElementById("tagsearchId").value);
	
    var hiddenCertTypeIds 	=	document.getElementsByName("hiddenCertTypeId");
	var certIds =[];
	for(var i=0;i<hiddenCertTypeIds.length;i++)
	{
		certIds.push(hiddenCertTypeIds[i].value);
	}
	var stateId2	=	trim(document.getElementById("stateId2").value);
	var zipCode		=	trim(document.getElementById("zipCode").value);
	var epiFromDate	=	trim(document.getElementById("epiFromDate").value);
	var epiToDate	=	trim(document.getElementById("epiToDate").value);
	//shadab
	var branchId ="";
	try{
		branchId = $("#bnchId").val();
	}catch(e){}
	var headQuarter = "";
 	 try{
 		headQuarter = document.getElementById("headQuarterId").value;
 		}catch(e){}
	if(branchId===undefined)
	{
		branchId ="";
	}	
	if(headQuarter!="" || branchId!="")
	{
		exportType="print";
		displayTeacherGrid();
	}
	
	else
	{
		TeacherInfotAjax.exportTeacherGridonPrintPreview(firstName,lastName,emailAddress,regionId,degreeId,universityId,normScoreSelectVal,qualQues,
				normScore,CGPA,CGPASelectVal,candidateStatus,stateId,candidatetype,noOfRows,page,sortOrderStr,sortOrderType,districtId,schoolId,fromDate,toDate,certType,references,resume,fitScoreSelectVal,YOTESelectVal,AScoreSelectVal,LRScoreSelectVal,canServeAsSubTeacherYes,canServeAsSubTeacherNo, 
				canServeAsSubTeacherDA,TFAA,TFAC,TFAN,fitScore,YOTE,AScore,LRScore,jobAppliedFromDate,jobAppliedToDate,daysVal,arr,$('#newcandidatesonly').is(":checked"),teacherSSN,stateForcandidate,certTypeForCandidate,empNo,jobCategoryIds,tagsearchId,stateId2,zipCode,certIds,epiFromDate,epiToDate,{ 
			async: true,
			callback: function(data)
			{ 
			$('#loadingDiv').hide();
			try{ 
			 if (isSafari && !deviceType)
			    {
			    	window.document.write(data);
					window.print();
			    }else
			    {
			    	var newWindow = window.open();
			    	newWindow.document.write(data);	
			    	newWindow.print();
				 }
			}catch (e) 
			{
				$('#printmessage1').modal('show');
			}
			}});
	}	
	
	
	
	
}

function getTemplatesByDistrictIdTP()
{
	CandidateGridAjaxNew.getTemplatesByDistrictId(0,
		{
				async: true,
				errorHandler:handleError,
				callback:function(data)
				{						
					if(data!="" || data.length>0)
					{
						$("#templateDivTP").fadeIn();
						$("#btnChangetemplatelinkTP").hide();
						$("#districttemplateTP").html(data);
					}
				}
			});
}

function setTemplateTP()
{
	var templateId	=	$("#districttemplateTP").val();
	if(templateId==0)
		$("#btnChangetemplatelinkTP").fadeOut();
	else
		$("#btnChangetemplatelinkTP").fadeIn();
}

function getTemplateTP()
{
	var templateId	=	$("#districttemplateTP").val();
	var teacherId	=	$("#teacherDetailId").val();
	var jobId		=	0;
	var confirmFlagforChangeTemplate = $("#confirmFlagforChangeTemplateTP").val(); // Gagan : confirmFlagforChangeTemplate 1 means confirm change template 
	//alert(" confirmFlagforChangeTemplate "+confirmFlagforChangeTemplate);
	
	if(templateId!=0)
	{
		CandidateGridAjaxNew.getTemplatesByMultipleTeachTemplateId(templateId,jobId,
		{
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{	
				//alert(" data : "+data+" Length : "+data.length);
				if(data!=null)
				{
					if($('#messageSubjectMassTP').val() && $('#messageSendMassTP').find(".jqte_editor").text().trim()!="" && confirmFlagforChangeTemplate==0)
					{
						try{
							$('#confirmChangeTemplate').modal('show');
						}	
						catch (err){}
					}
					else
					{
						$('#messageSubjectMassTP').val(data.subjectLine);
						$('#messageSendMassTP').find(".jqte_editor").html(data.templateBody);
						$("#confirmFlagforChangeTemplateTP").val(0);
					}
				}
			}
		});
	}
	return false;
}


/*========================== TFA  Edit By Specific users ==========================================*/
function editTFAbyUser(teacherId)
{
	$("#draggableDivMaster").show();
	$('#edittfa').hide();
	$('#savetfa').show();
	$('#canceltfa').show();
	document.getElementById("tfaList").disabled=false;
}

function saveTFAbyUser()
{
	$("#tfaMsgShow").hide();
	
	var tfa = document.getElementById("tfaList").value;
	var teacherId = document.getElementById("teacherDetailId").value;
	
	TeacherInfotAjax.updateTFAByUser(teacherId,tfa,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
			var tfalist = document.getElementById("tfaList");
			if(data!=null)
			{
				for(var i=0; i<tfalist.options.length;i++)
				{	
					if(tfalist.options[i].value==data)
					{
						tfalist.options[i].selected = true;
					}
				}
			}
			else
			{
				tfalist.selectedIndex = 1;
			}
			
			$("#draggableDivMaster").show();
			$('#edittfa').show();
			$('#savetfa').hide();
			$('#canceltfa').hide();
			document.getElementById("tfaList").disabled=true;
		}
	});
}

function cancelTFAbyUser(teacherId)
{
		$("#draggableDivMaster").show();
		$('#edittfa').show();
		$('#savetfa').hide();
		$('#canceltfa').hide();
		document.getElementById("tfaList").disabled=true;
		var tfalist = document.getElementById("tfaList");
		TeacherInfotAjax.displayselectedTFA(teacherId,{ 
			async: false,
			errorHandler:handleError,
			callback:function(data)
			{
				for(var i=0; i<tfalist.options.length;i++)
				{	
					if(tfalist.options[i].value==data)
					{
						tfalist.options[i].selected = true;
					}
				}
			}
		});
		
}

function displayTFAMessageBox(teacherId)
{
	document.getElementById("teacherDetailId").value=teacherId;	
	$("#tfaMsgShow").show();
}

function closeTFAMsgBox()
{
	$("#tfaMsgShow").hide();
}

//------------------Rahul:GetDocument---17/11/2014---------------------

function getDocuments()
{
	
	
	CandidateGridAjaxNew.getDocumentsByDistrictOrSchoolId(
			{
				async: true,
				errorHandler:handleError,
				callback:function(data)
				{	
				
					if(data!="" || data.length>0)
					{
						//$("#templateDiv").fadeIn();
						//$("#btnChangetemplatelink").hide();
					
						$("#districtDocument").html(data);
					
					}else
					{
						
						$("#documentdiv").hide();
					}
				}
			});	

	
}


function getDocumentsTP()
{
	
	
	CandidateGridAjaxNew.getDocumentsByDistrictOrSchoolId(
			{
				async: true,
				errorHandler:handleError,
				callback:function(data)
				{	
				
					if(data!="" || data.length>0)
					{
						//$("#templateDiv").fadeIn();
						//$("#btnChangetemplatelink").hide();
					
						$("#districtDocumentTP").html(data);
					
					}else
					{
						
						$("#documentdivTP").hide();
					}
				}
			});	

	
}

















function showDocumentLink()
{
var docname=document.getElementById("districtDocument").value;	

if(docname.length>1)
{
	$('#viewDoc').show();
	
}

else
{
	$('#viewDoc').hide();	
}

}


function showDocumentLinkTP()
{
var docname=document.getElementById("districtDocumentTP").value;	

if(docname.length>1)
{
	$('#viewDocTP').show();
	
}

else
{
	$('#viewDocTP').hide();	
}

}





function vieDistrictFile(ids) {
	document.getElementById('ifrmAttachment').src = "";
	if(ids==0)
	var districtattachment="";
	if(ids==0)
	districtattachment=document.getElementById("districtDocument").value;
		
	if(ids==1)
	districtattachment=document.getElementById("districtDocumentTP").value;
		
		
	CandidateGridAjaxNew.vieDistrictFile(districtattachment,{
	async: true,
	errorHandler:handleError,
	callback:function(data)
	{
		if(deviceType)
		{
			
			if (data.indexOf(".doc")!=-1)
			{
				$("#docfileNotOpen").css({"z-index":"3000"});
		    	$('#docfileNotOpen').show();
			}
			else if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
			{
				$("#exelfileNotOpen").css({"z-index":"3000"});
		    	$('#exelfileNotOpen').show();
			}
			else
			{
				document.getElementById(linkid).href = data;
			}
		}
		else if(deviceTypeAndroid)
		{
		
			if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
			{
				// $('#distAttachmentDiv').modal('hide');
				 document.getElementById('ifrmAttachment').src = ""+data+"";
			}
			else
			{
				document.getElementById(linkid).href = data;
			}
		}
		else if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
		{
			 $('#distAttachmentShowDiv').modal('hide');
			 document.getElementById('ifrmAttachment').src = ""+data+"";
		} else {
			//alert(data)
			$('#myModalMessage').modal('hide');
				$('.distAttachmentShowDiv').modal('show');
				document.getElementById('ifrmAttachment').src = ""+data+"";
				return false;
		}
	}});

	}

function showMessageDiv1()
{
	$('#myModalMessage').modal('show');
	$('.distAttachmentShowDiv').modal('hide');
}

function showReferencesForm()
{
	 $("#divElectronicReferences").show();
	 resetReferenceform();
	 document.getElementById("salutation").focus();
}

function resetReferenceform()
{
	    document.getElementById("rdcontacted1").checked = true
	 	document.getElementById("salutation").value=0;
		document.getElementById("firstName1").value='';
		document.getElementById("lastName1").value='';
		document.getElementById("designation").value='';
		document.getElementById("organization").value='';
		document.getElementById("email").value='';
		document.getElementById("contactnumber").value='';
		$('#referenceDetailstext').find(".jqte_editor").html("");
		document.getElementById("pathOfReferenceFile").value="";
		document.getElementById("pathOfReferenceFile").value=null;
}

//======================= Mukesh References ===============================
function insertOrUpdateElectronicReferencesM(sbtsource)
{
	
	 		var referenceDetails	= trim($('#referenceDetailstext').find(".jqte_editor").html());
			var teacherId		=   trim(document.getElementById("teacherId").value);
			var elerefAutoId	=	document.getElementById("elerefAutoId");
			var salutation		=	document.getElementById("salutation");
			
			var firstName		=	trim(document.getElementById("firstName1").value);
			var lastName		= 	trim(document.getElementById("lastName1").value);
			var designation		= 	trim(document.getElementById("designation").value);
			
			var organization	=	trim(document.getElementById("organization").value);
			var email			=	trim(document.getElementById("email").value);
			
			var contactnumber	=	trim(document.getElementById("contactnumber").value);
			var longHaveYouKnow	=	trim(document.getElementById("longHaveYouKnow").value);
			var rdcontacted0	=   document.getElementById("rdcontacted0");
			var rdcontacted1	=   document.getElementById("rdcontacted1");
			var pathOfReferencesFile = document.getElementById("pathOfReferenceFile");
			
			var cnt=0;
			var focs=0;	
			
			var fileName = pathOfReferencesFile.value;
			var ext = fileName.substr(fileName.lastIndexOf('.') + 1).toLowerCase();
			var fileSize=0;		
			if ($.browser.msie==true){	
			    fileSize = 0;	   
			}else{		
				if(pathOfReferencesFile.files[0]!=undefined)
				fileSize = pathOfReferencesFile.files[0].size;
			}
			
			$('#errordivElectronicReferences').empty();
			setDefColortoErrorMsgToElectronicReferences();
			
			if(firstName=="")
			{
				$('#errordivElectronicReferences').append("&#149; "+resourceJSON.PlzEtrFirstName+"<br>");
				if(focs==0)
					$('#firstName1').focus();

				$('#firstName1').css("background-color",txtBgColor);
				cnt++;focs++;
			}
			if(organization=="")
			{
				$('#errordivElectronicReferences').append("&#149; "+resourceJSON.PlzEtrOrg+"<br>");
				if(focs==0)
					$('#organization').focus();
				
				$('#organization').css("background-color",txtBgColor);
				cnt++;focs++;
			}
			
			if(designation=="" &&  ((window.location.hostname=="nccloud.teachermatch.org") || (window.location.hostname=="nc.teachermatch.org") || (window.location.hostname=="localhost")))
			{
				$('#errordivElectronicReferences').append("&#149; "+resourceJSON.msgTitle+"<br>");
				if(focs==0)
					$('#designation').focus();
				
				$('#designation').css("background-color",txtBgColor);
				cnt++;focs++;
			}
			
			if(lastName=="")
			{
				$('#errordivElectronicReferences').append("&#149; "+resourceJSON.PlzEtrLastName+"<br>");
				if(focs==0)
					$('#lastName1').focus();

				$('#lastName1').css("background-color",txtBgColor);
				cnt++;focs++;
			}
			
			if(email=="")
			{
				$('#errordivElectronicReferences').append("&#149; "+resourceJSON.PlzEtrEmail+"<br>");
				if(focs==0)
					$('#email').focus();
				
				$('#email').css("background-color",txtBgColor);
				cnt++;focs++;
			}
			else if(!isEmailAddress(email))
			{		
				$('#errordivElectronicReferences').append("&#149; "+resourceJSON.PlzEtrVldEmail+"<br>");
				if(focs==0)
					$('#email').focus();
				
				$('#email').css("background-color",txtBgColor);
				cnt++;focs++;
			}
			if(contactnumber=="")
			{
				$('#errordivElectronicReferences').append("&#149; "+resourceJSON.PlzEtrCtctNum+"<br>");
				if(focs==0)
					$('#contactnumber').focus();
				
				$('#contactnumber').css("background-color",txtBgColor);
				cnt++;focs++;
			}
			else if(contactnumber.length==10)
			{
				
			}
			else
			{
				$('#errordivElectronicReferences').append("&#149; "+resourceJSON.errorMsgForNumber +"<br>");
				if(focs==0)
					$('#contactnumber').focus();

				$('#contactnumber').css("background-color",txtBgColor);
				cnt++;focs++;
			}			
			if(longHaveYouKnow=="" && $("#districtId").val()==4218990)
			{
				$('#errordivElectronicReferences').append("&#149; "+resourceJSON.msgHowLongYouKnwPers+"<br>");
				if(focs==0)
					$('#longHaveYouKnow').focus();
				
				$('#longHaveYouKnow').css("background-color",txtBgColor);
				cnt++;focs++;
			}

			var rdcontacted_value;
			
			if (rdcontacted0.checked) {
				rdcontacted_value = false;
			}
			else if (rdcontacted1.checked) {
				rdcontacted_value = true;
			}
			
			if(ext!=""){
				if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt'))
				{
					$('#errordivElectronicReferences').append("&#149; "+resourceJSON.AcceptFileFormat+"<br>");
						if(focs==0)
							$('#pathOfReferenceFile').focus();
						
						$('#pathOfReferenceFile').css("background-color",txtBgColor);
						cnt++;focs++;	
						return false;
				}	
				else if(fileSize>=10485760)
				{
					$('#errordivElectronicReferences').append("&#149; "+resourceJSON.msgDocSize10mb+"<br>");
						if(focs==0)
							$('#pathOfReferenceFile').focus();
						
						$('#pathOfReferenceFile').css("background-color",txtBgColor);
						cnt++;focs++;	
						return false;
				}
			}

			if(cnt!=0)		
			{
				$('#errordivElectronicReferences').show();
				return false;
			}
			else
			{
				var teacherElectronicReferences = {elerefAutoId:null,salutation:null, firstName:null, lastName:null, designation:null, organization:null,email:null,contactnumber:null,rdcontacted:null};
				dwr.engine.beginBatch();
				
				dwr.util.getValues(teacherElectronicReferences);
				teacherElectronicReferences.rdcontacted=rdcontacted_value;
				teacherElectronicReferences.salutation=salutation.value;
				teacherElectronicReferences.firstName=firstName;
				teacherElectronicReferences.lastName=lastName;
				teacherElectronicReferences.designation=designation;
				teacherElectronicReferences.organization=organization;
				teacherElectronicReferences.email=email;
				
				teacherElectronicReferences.referenceDetails=referenceDetails;
				teacherElectronicReferences.longHaveYouKnow=longHaveYouKnow;
				
				var refFile = document.getElementById("pathOfReference").value;
				if(refFile!=""){
					teacherElectronicReferences.pathOfReference=refFile;
				}
				if(fileName!="")
				{	
					
					CGServiceAjax.findDuplicateReferences(teacherElectronicReferences,teacherId,{ 
						async: false,
						errorHandler:handleError,
						callback: function(data)
						{
						if(data=="isDuplicate")
						{
							$('#errordivElectronicReferences').append("&#149; "+resourceJSON.ElectronicRefAlreadyReg+"<br>");
							if(focs==0)
								$('#email').focus();
							$('#email').css("background-color",txtBgColor);
							cnt++;focs++;
							$('#errordivElectronicReferences').show();
							return false;
						}else{
							document.getElementById("frmElectronicReferences").submit();
						}
						}
					});
					
				}else{
					CGServiceAjax.saveUpdateElectronicReferences(teacherElectronicReferences,teacherId,{ 
						async: true,
						errorHandler:handleError,
						callback: function(data)
						{
							if(data=="isDuplicate")
							{
								$('#errordivElectronicReferences').append("&#149; "+resourceJSON.ElectronicRefAlreadyRegWithProvidedEmail+"<br>");
								if(focs==0)
									$('#email').focus();
								$('#email').css("background-color",txtBgColor);
									cnt++;focs++;
								$('#errordivElectronicReferences').show();
								return false;
							}
							hideElectronicReferencesForm();
							getElectronicReferencesGrid_DivProfile(teacherId);
						}
					});
				}
				
				dwr.engine.endBatch();
				return true;
			}
			
}

function setDefColortoErrorMsgToElectronicReferences()
{
	$('#salutation').css("background-color","");
	$('#firstName1').css("background-color","");
	$('#lastName1').css("background-color","");
	$('#designation').css("background-color","");
	$('#organization').css("background-color","");
	$('#email').css("background-color","");
	$('#contactnumber').css("background-color","");
}

function hideElectronicReferencesForm()
{
	resetReferenceform();
	document.getElementById("elerefAutoId").value="";
	document.getElementById("divElectronicReferences").style.display="none";
	//resetReferenceform();
	return false;
}

function saveNobleReference(fileName)
{
   var referenceDetailstext	= trim($('#referenceDetailstext').find(".jqte_editor").text());
   
		var teacherId		=   trim(document.getElementById("teacherId").value);	
	    var elerefAutoId	=	document.getElementById("elerefAutoId");
		var salutation		=	document.getElementById("salutation");
		
		var firstName		=	trim(document.getElementById("firstName1").value);
		var lastName		= 	trim(document.getElementById("lastName1").value);;
		var designation		= 	trim(document.getElementById("designation").value);
		var longHaveYouKnow		= 	trim(document.getElementById("longHaveYouKnow").value);
		
		var organization	=	trim(document.getElementById("organization").value);
		var email			=	trim(document.getElementById("email").value);
		
		var contactnumber	=	trim(document.getElementById("contactnumber").value);
		var rdcontacted0	=   document.getElementById("rdcontacted0");
		var rdcontacted1	=   document.getElementById("rdcontacted1");
		
		var cnt=0;
		var focs=0;	
		
		var rdcontacted_value;
		if (rdcontacted0.checked) {
			rdcontacted_value = false;
		}
		else if (rdcontacted1.checked) {
			rdcontacted_value = true;
		}
		
		var teacherElectronicReferences = {elerefAutoId:null,salutation:null, firstName:null, lastName:null, designation:null, organization:null,email:null,contactnumber:null,rdcontacted:null,pathOfReference:null,longHaveYouKnow:null};
		dwr.engine.beginBatch();
	
		dwr.util.getValues(teacherElectronicReferences);
		teacherElectronicReferences.rdcontacted=rdcontacted_value;
		teacherElectronicReferences.salutation=salutation.value;
		teacherElectronicReferences.firstName=firstName;
		teacherElectronicReferences.lastName=lastName;;
		teacherElectronicReferences.designation=designation;
		teacherElectronicReferences.organization=organization;
		teacherElectronicReferences.email=email;
		teacherElectronicReferences.pathOfReference=fileName;
		teacherElectronicReferences.longHaveYouKnow=longHaveYouKnow;
		teacherElectronicReferences.referenceDetails=referenceDetailstext;
		

		CGServiceAjax.saveUpdateElectronicReferences(teacherElectronicReferences,teacherId,{ 
			async: true,
			errorHandler:handleError,
			callback: function(data)
			{
				if(data=="isDuplicate")
				{
					///updateReturnThreadCount("ref");
					$('#errordivElectronicReferences').append("&#149; "+resourceJSON.ElectronicRefAlreadyRegWithProvidedEmail+"<br>");
					if(focs==0)
						$('#email').focus();
					$('#email').css("background-color",txtBgColor);
						cnt++;focs++;
					$('#errordivElectronicReferences').show();
					return false;
				}
				hideElectronicReferencesForm();
				getElectronicReferencesGrid_DivProfile(teacherId);
				
			}
		});
	
		dwr.engine.endBatch();
		return true;
	}
function editFormElectronicReferences(id)
{
	$('#errordivElectronicReferences').empty();
	setDefColortoErrorMsgToElectronicReferences();
	CGServiceAjax.editElectronicReferences(id,
		{ 
			async: false,
			errorHandler:handleError,
			callback: function(data)
			{
		       showReferencesForm();
				
	        dwr.util.setValues(data);
	        document.getElementById("firstName").value	 = "";
			document.getElementById("lastName").value   = "";
		    document.getElementById("salutation").value	   =	data.salutation;
		    document.getElementById("firstName1").value	   =	data.firstName;
			document.getElementById("lastName1").value	   =	data.lastName;
			
				$('#referenceDetailstext').find(".jqte_editor").html(data.referenceDetails);
				return false;
			}
		});
	return false;
}

function removeReferences()
{
	var teacherId =  document.getElementById("teacherId").value
	var referenceId = document.getElementById("elerefAutoId").value;
	if(window.confirm(resourceJSON.msgdeletelatter))
	{
		CGServiceAjax.removeReferencesForNoble(referenceId,teacherId,
		{ 
			async: false,
			errorHandler:handleError,
			callback: function(data)
			{	
				getElectronicReferencesGrid_DivProfile(teacherId);
				hideElectronicReferencesForm();
			}
		});
	}
}
var eleRefIDForTeacher=0;
var status_eref=0;
function changeStatusElectronicReferences(id,status)
{	
	eleRefIDForTeacher=id;
	status_eref=status;
	if(status==1)
	$('#chgstatusRef1').modal('show');
	else if(status==0)
	$('#chgstatusRef2').modal('show');	
}
function changeStatusElectronicReferencesConfirm()
{	
	 var teacherId =  document.getElementById("teacherId").value
	$('#chgstatusRef1').modal('hide');
	$('#chgstatusRef2').modal('hide');
	
		CGServiceAjax.changeStatusElectronicReferences(eleRefIDForTeacher,status_eref, { 
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{
			    getElectronicReferencesGrid_DivProfile(teacherId);
				hideElectronicReferencesForm();
				return false;
			}});
		
		eleRefIDForTeacher=0;
}


function setDefColortoErrorMsgToVideoLinksForm()
{
	$('#videourl').css("background-color","");
}
function showVideoLinksForm()
{
	document.getElementById("frmvideoLinks").reset();
	document.getElementById("videolinkAutoId").value="";
	document.getElementById("divvideoLinks").style.display="block";
	document.getElementById("videourl").focus();
	document.getElementById("videofile").value="";
	document.getElementById("video").style.display="block";	
	setDefColortoErrorMsgToVideoLinksForm();
	$('#errordivvideoLinks').empty();
	$('#video').empty();
	return false;
}

function hideVideoLinksForm()
{
	document.getElementById("frmvideoLinks").reset();
	document.getElementById("videolinkAutoId").value="";
	document.getElementById("divvideoLinks").style.display="none";
	$("#video").empty();
	document.getElementById("video").style.display="none";
	return false;
}

function insertOrUpdatevideoLinks()
{
	 
	var teacherId =  document.getElementById("teacherId").value
	var elerefAutoId = document.getElementById("videolinkAutoId");
	var videourl = trim(document.getElementById("videourl").value);
	var video = trim(document.getElementById("videofile").value);	
	var videoSize = document.getElementById("videofile");
	var oldvideo = document.getElementById("video").innerHTML;
	var cnt=0;
	var focs=0;	
	
	$('#errordivvideoLinks').empty();
	setDefColortoErrorMsgToVideoLinksForm();
	
	if(videourl=="" && video =="" && oldvideo =="")
	{
		$('#errordivvideoLinks').append("&#149; "+resourceJSON.msgvideolink+"<br>");
		if(videourl==0)
			$('#videourl').focus();
		
		$('#videourl').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	if(video !="")
	{
		var ext = video.substr(video.lastIndexOf('.') + 1).toLowerCase();		
		if(!(ext=="mp4"||ext=="MP4"||ext=="ogv"||ext=="ogg"||ext=="webm"))
		{        	
		 $('#errordivvideoLinks').append("&#149; "+resourceJSON.msgacceptablevideo+"");
		 if(video==0)
			$('#videourl').focus();
		
		$('#videourl').css("background-color",txtBgColor);
		cnt++;focs++;
		}
		else if(videoSize.files[0].size>10485760)
		{        	
		 $('#errordivvideoLinks').append("&#149; "+resourceJSON.msgvideosize+"<br>");
		 if(video==0)
			$('#videourl').focus();
		
		$('#videourl').css("background-color",txtBgColor);
		cnt++;focs++;
		}
		else
		{
			video=video.replace(/^.*[\\\/]/, '');	
		}
	}
	else
	{
		video= document.getElementById('video').innerHTML;
	}
	
	if(cnt!=0 )		
	{
		$('#errordivvideoLinks').show();
		return false;
	}
	else
	{	
		$('#loadingDiv').show();
		if(videoSize.value !="")
		{	
			document.getElementById("frmvideoLinks").submit();
		}	
		else
		{
			var teacherVideoLink = {videolinkAutoId:null,videourl:null,video:null, createdDate:null};
			dwr.engine.beginBatch();
			dwr.util.getValues(teacherVideoLink);
			teacherVideoLink.videourl=videourl;
			teacherVideoLink.video=	oldvideo;
			CGServiceAjax.saveOrUpdateVideoLink(teacherVideoLink,teacherId,{ 
				async: false,
				errorHandler:handleError,
				callback: function(data)
				{
					$('#loadingDiv').hide();
					hideVideoLinksForm();
					getVideoLinksGrid_DivProfile(teacherId);
				}
			});
			dwr.engine.endBatch();
			return true;
		}
	}
}


function editFormVideoLink(id)
{
	CGServiceAjax.editVideoLink(id,
		{ 
			async: false,
			errorHandler:handleError,
			callback: function(data)
			{
				showVideoLinksForm();
				dwr.util.setValues(data);
				return false;
			}
		});
	return false;
}

function deleteFormVideoLink(id)
{
	CGServiceAjax.deleteVideoLink(id,
		{ 
			async: false,
			errorHandler:handleError,
			callback: function(data)
			{
				showVideoLinksForm();
				dwr.util.setValues(data);
				return false;
			}
		});
	return false;
}

var videolnkIDForTeacher=0;
function delVideoLink(id)
{	
	videolnkIDForTeacher=id;
	$('#delVideoLnk').modal('show');
}
function delVideoLnkConfirm()
{	
	 var teacherId =  document.getElementById("teacherId").value	
	 $('#delVideoLnk').modal('hide');
		CGServiceAjax.deleteVIdeoLink(videolnkIDForTeacher, { 
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{
			getVideoLinksGrid_DivProfile(teacherId);	
			//getVideoLinksGrid();
				hideVideoLinksForm();
				return false;
			}});
		
		videolnkIDForTeacher=0;
}

function getAutoNotificationFlag()
{
var districtId=	document.getElementById("districtId").value;


TeacherInfotAjax.getAutoNotFlag(districtId,{
	async: true,
	errorHandler:handleError,
	callback:function(data)
	{
	
	document.getElementById("sendNotification").checked=data;
	
	}
	
		}		
 );
}

function pnqSave(teacherId){
	var pnqOption = $("#pnqList option:selected").val();
	TeacherProfileViewInDivAjax.updatePNQByUser(teacherId,pnqOption,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
			
		}
	});
}

function getiFrameForTreeTP()
{
	CGServiceAjax.getiFrameForTree({ 
	async: true,
	callback: function(data)
	{
		$('#treeiFrameTP').html(data)
	},
	errorHandler:handleError
	});
}

function showTagDetailsTP(tid,j)
{
	
	TeacherInfotAjax.getTagDetailsTP(tid,j,{ 
	async: false,
	errorHandler:handleError,
	callback:function(data)
	{
	 document.getElementById("tagDivTP").innerHTML=data;	
	
  	$('#tagPreview'+j).popover({ 
    html : true,
    placement: 'right',
    content: $('#tagDivTP').html(),
  }).on("click", function () 
  {
	$('.popup').css('display','inline');     
	$(".popover-content").css("margin-top", "10px");
	if($('.popover').find('input[type=checkbox]').length>15){   
		$('.popover').css('width','70%');
	}else{
		$('.popover').css('width','40%');
	}
	$('#currentPopUpId').val(id);
	var popH = $('.popover').css('top').replace('px','');
	var diff =$("#tagPreview"+id).offset().top-$(".arrow").offset().top;
	var k = Number(popH) + Number(diff); 
	$('.popover').css('top',k+'px');	
 });
  	
	}});
}

function certTextDiv(id){
	
	$("#draggableDivMaster").modal('hide');
	
	$(".certTextContent").html($("#cerTTextContent"+id).val());
	$("#certTextDivDSPQ").modal("show");
}
function certTextDivClose(){
	 
	 $("#draggableDivMaster").modal('show');
	 ("#certTextDivDSPQ").modal("show");
	 $(".certTextContent").html("");
} 

function changePnqColor(value,id){
	var id="#teacher_PNQ"+id;
	if(value==2){
		$(id+" span").css("color","red");
	}else if(value==0){		
		$(id+" span").css("color","#535353");	
	}else if(value==1){
		$(id+" span").css("color","#007AB4");
	}
}

function displayAdvanceSearch1()
{
	$('#searchLinkDiv').hide();
	$('#hidesearchLinkDiv').show();
	$('#advanceSearchDiv').slideDown('slow');
	displayAdvanceSearch();
}

function hideAdvanceSearch()
{
	$('#searchLinkDiv').show();
	$('#hidesearchLinkDiv').hide();
	$('#advanceSearchDiv').slideUp('slow');
}

//Online Activity
function showOnlineActivity(teacherId,districtId)
{
	CGServiceAjax.getOnlineActivityData(teacherId,districtId,{ 
	async: true,
	callback: function(data)
	{
		if(data=='1')
		{
			displayTeacherGrid();
		}
		else if(data!=null)
		{
			$("#resetOnlineActivityModal").modal('show');
			$('#resetOnlineActivityDiv').html(data)
			applyScrollonActivityTable();
		}
		
	},
	errorHandler:handleError
	});
}

function resetOnActivity(onActivityId)
{
	CGServiceAjax.changeExpiryActivityStatus(onActivityId,{ 
		async: true,
		callback: function(data)
		{
				if(data!=null)
				{
					$("#resetOnlineActivityModal").modal('show');
					$('#resetOnlineActivityDiv').html(data)
					applyScrollonActivityTable();
				}
		},
		errorHandler:handleError
		});
}

function closeOnlineActivity()
{
	$("#resetOnlineActivityModal").modal('hide');
	displayTeacherGrid();
}


function changeJobCanidateStatus(tId,jobId,statusName)
{
	$('#loadingDiv').fadeIn();
	$('#candidatestatusddDiv').html("");
	
	var url =window.location.href;
	if(url.indexOf("teacherinfo.do") > -1){
		TeacherInfotAjax.getstatusDDListNew(tId,jobId,statusName,{
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{
			  $('#loadingDiv').hide();
			  $("#candidatestatusDivMain").modal('show');
			  $('#candidatestatusddDiv').html(data[0]);
			  var jobTitle=document.getElementById("jobTitlePool").value
			  $('#myModalLabelStatus').html("<h3 id='myModalLabel'>"+resourceJSON.msgCurrentStatusat+" "+jobTitle +" is "+data[1]+"</h3>");
			  document.getElementById("candidateCurrentStatus").value=data[1];
			  $('#statusErrorPoolDiv').empty();
			}
		});
	}
	else{
		TeacherInfotAjax.getstatusDDList(tId,jobId,statusName,{
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{
			  $('#loadingDiv').hide();
			  $("#candidatestatusDivMain").modal('show');
			  $('#candidatestatusddDiv').html(data[0]);
			  var jobTitle=document.getElementById("jobTitlePool").value
			  $('#myModalLabelStatus').html("<h3 id='myModalLabel'>"+resourceJSON.msgCurrentStatusat+" "+jobTitle +" is "+data[1]+"</h3>");
			  document.getElementById("candidateCurrentStatus").value=data[1];
			  $('#statusErrorPoolDiv').empty();
			}
		});
	}
	}

function changeCandidateStatusFromPool()
{
	$('#statusErrorPoolDiv').empty();
	var candidateCurrentStatus=document.getElementById("candidateCurrentStatus").value;
	var newStatusName=$("#statusChangeId option:selected").html();
	
	if(candidateCurrentStatus==newStatusName){
		$('#statusErrorPoolDiv').empty();
		$('#statusErrorPoolDiv').append("&#149; "+resourceJSON.msgChangeStatus+"");
	    $('#eventArrordiv').show();
	}else{
		$('#loadingDiv').fadeIn();
		var teacherId= document.getElementById("teacherIdByStatus").value;
		var jobId= document.getElementById("jobIdByStatus").value;
		var jobForTeacherId = document.getElementById("jobForTeacherIdForStatus").value;
		
		var newStatusChangeId = document.getElementById("statusChangeId").value;	
		
		TeacherInfotAjax.changeCandidateStatusFromPool(jobForTeacherId,teacherId,jobId,newStatusChangeId,{
			async: false,
			errorHandler:handleError,
			callback:function(data)
			{
			 // alert("call Back :: "+data);
			if(data!=null && data!="" && data!="change"){
				  $('#loadingDiv').hide();
				  $('#statusErrorPoolDiv').empty();
				  $('#statusErrorPoolDiv').append("&#149; "+data);
				  $('#eventArrordiv').show();
			  }else{
				  $('#eventArrordiv').hide();
				  $("#candidatestatusDivMain").modal('hide');
				  //getJobOrderList(); 
				  refereshStatusINChangeStatus();
			  }
			}
		});	
	}
}

function getHonorsGrid(teacherId)
{
	var visitLocation=document.getElementById("teacherIdForprofileGridVisitLocation").value;
	TeacherProfileViewInDivAjax.getHonorsGridWithoutAction(teacherId,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			document.getElementById("gridDataTeacherHonors").innerHTML=data;
			applyScrollOnHonors();
			
		}});
}



function getInvolvementGrid(teacherId)
{
	TeacherProfileViewInDivAjax.getInvolvementGrid(teacherId,dp_Involvement_Rows,dp_Involvement_page,dp_Involvement_sortOrderStr,dp_Involvement_sortOrderType,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#gridDataTeacherInvolvementOrVolunteerWork').html(data);
			applyScrollOnInvl();
		}});
}

function clickedOnYes()
{
	$("#vetranOptionDiv").show();
}

function clickedOnNo()
{
	$("#vetranOptionDiv").hide();
}

function selectAllCandidate(){
	 var flag = document.getElementById("select_all_candidate").checked;
	 if(flag==true)
	 { 
		$('input[name="chkMassTP"]').each(function() {
		$(this).prop('checked', true);
		 document.getElementById("tpMenuActionDiv").style.display="inline";
		});
	 }
	 else{
		 
		 $('input[name="chkMassTP"]').each(function() {
			 $(this).prop('checked', false);
			 document.getElementById("tpMenuActionDiv").style.display="none";
				});
	 }
}
function openDynmicDivContent(id,para){
	
	$("#draggableDivMaster").modal('hide');
	
	$(".certTextContent").html($("#"+para+id).html());
	$("#certTextDivDSPQ").modal("show");
}

//amit
function addCertificate(checkedFlag)
{
	var duplicateCert=false;
	var hiddenCertTypeIds 	=	document.getElementsByName("hiddenCertTypeId");
	var sb="";
	var stateId		=	trim(document.getElementById("stateIdForcandidate").value);
	var certType	=	trim(document.getElementById("certTypeForCandidate").value);
	var certTypeId	=	trim(document.getElementById("certificateTypeMasterForCandidate").value);
	
	if(hiddenCertTypeIds.length>0)
	{
		for(var i=0;i<hiddenCertTypeIds.length;i++)
		{
			if(hiddenCertTypeIds[i].value==certTypeId)
			{
				duplicateCert=true;
				//alert("duplicateCert=="+duplicateCert);
			}
		}
	}
	if(duplicateCert)
	{
		//$('#myModalSearch').modal('hide');
		$('#myModal222').modal('show');
	}
	else
	{
		//alert("checkedFlag=="+checkedFlag+"\nstateId=="+stateId+"\ncertType=="+certType+"\ncertTypeId=="+certTypeId);
		if(stateId!="" && stateId!=0 && certType!="" && certType!=0 && certTypeId!="" && certTypeId!=0)
		{
			document.getElementById('divCertificate').style.display='block';
			if(checkedFlag==1)
			{
				//alert("checkedFlag=="+checkedFlag+"\nstateId=="+stateId+"\ncertType=="+certType+"\ncertTypeId=="+certTypeId);
				sb+="<div class='col-sm-12 col-md-12' id='div"+certTypeId+"'>"+certType;
				sb+="<a data-original-title='Remove' rel='tooltip' id='"+certTypeId+"' href='javascript:void(0);' onclick=\"removeCertificate("+certTypeId+");\"><img width='15' height='15' class='can' src='images/can-icon.png' alt=''></a>";
				sb+="<input type='hidden' name='hiddenCertTypeId' id='c"+certTypeId+"' value='"+certTypeId+"'/>";
				sb+="<script type='text/javascript'>$('#"+certTypeId+"').tooltip();</script></div>";
				
				$('#divCertificate').append(sb);
				document.getElementById("stateIdForcandidate").value="0";
				document.getElementById("certTypeForCandidate").value="";
				document.getElementById("certificateTypeMasterForCandidate").value="0";
			}
		}
	}
}

function removeCertificate(certTypeId)
{
	if(certTypeId!="" && certTypeId!=0)
	{
		//alert("certTypeId=="+certTypeId);
		document.getElementById('div'+certTypeId).style.display='none';
		$('#div'+certTypeId).html("");
	}
}

function showAndHideModal()
{
	$('#myModal222').modal('hide');
	//$('#myModalSearch').modal('show');
	document.getElementById('certTypeForCandidate').value="";
	document.getElementById('certTypeForCandidate').focus();
}
function confirmPortfolioReminderMail(id)
{ 
	document.getElementById("teachId").value=id;
	$('#mailconfrmModal').modal('show');
}
function sendPortfolioReminderMail(){
	var id=document.getElementById("teachId").value;
	$('#mailconfrmModal').modal('hide');
	$('#loadingDiv').show();
	ManageStatusAjax.sendPortfolioReminderMail(id,{
	async: true,
	errorHandler:handleError,
	callback:function(data)
	{
		$('#loadingDiv').hide();
		$('#mailSuccessModal').modal('show');
	}});
}
/*************************TPL-3973 Starts*************************/
function confirmPortfolioReminderBulkMail(){		
	PortfolioReminderAjax.districtCandidateRefreshPool({
		async: true,
		callback: function(data){
			$('#errordivMessagePortfolio').empty();
			if(data.trim() == "true"){
				$('#bulkmailconfrmModal').modal('show');
			}else if(data == "false"){
				$('#candidatepoolrefreshstatusbox').modal('show');		
			}
			errorHandler:handleError 
		}
	});
}
function sendPortfolioReminderBulkMail(){
	
	var teacherIds = new Array();
	teacherIds	=	getTeacherIdsMassTP();
	$('#bulkmailconfrmModal').modal('hide');
	$('#loadingDiv').show();
	ManageStatusAjax.sendPortfolioReminderBulkMail(teacherIds,{
	async: true,
	errorHandler:handleError,
	callback:function(data)
	{
		$('#loadingDiv').hide();
		$('#mailSuccessModal').modal('show');
	}});
}
/*****************TPL-3973 ends ****************************/
function downloadOctUpload(octId)
{
		//alert("downloadAnswer0");
	TeacherProfileViewInDivAjax.downloadOctUpload(octId,{ 
			async: false,
			errorHandler:handleError,
			callback:function(data)
			{
			if(data!="")
				if (data.indexOf(".doc") !=-1)
				{
					document.getElementById('uploadFrameReferencesID').src = ""+data+"";
				}
				else
				{
					document.getElementById("hrefOctUpload").href = data; 
					return false;
				}
			}
		});
	}
function getOctDetails(octNo){
	 
    //var octNo = "518784";//$("#").val();
	$('#loadingDiv').fadeIn();
    TeacherProfileViewInDivAjax.getMemberQualification(octNo,{ 
        async: true,
        errorHandler:handleError,
        callback:function(data)
        {
            
    	$("#changeByName").html("OCT Details for "+$("#teacherNameInPrf").val());
    	$("#octmsgToShow").html(data);    	
    	$("#octContentDiv").modal("show");
    	applyScrollOctMemDetTbl();
    	applyScrollOctMemBasicTbl();
    	applyScrollOctMemAdditionalTbl();
    	$('#draggableDivMaster').modal('hide');
    	$('#loadingDiv').fadeOut();
        }
    });
 
}

function applyScrollOctMemDetTbl()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#octMemberDetail').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 150,
        width: 880,
        minWidth: null,
        minWidthAuto: false,
       	colratio:[100,180,180,210,210],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 20,
        wrapper: false
        });
            
        });			
}

function applyScrollOctMemBasicTbl()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#octMemberBasic').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 200,
        width: 880,
        minWidth: null,
        minWidthAuto: false,
       	colratio:[146,146,146,146,147,148],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 20,
        wrapper: false
        });
            
        });			
}

function applyScrollOctMemAdditionalTbl()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#octMemberAdditional').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        //height: 200,
        width: 880,
        minWidth: null,
        minWidthAuto: false,
       	colratio:[220,220,220,220],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 20,
        wrapper: false
        });
            
        });			
}
function printResume()
{
	var teacherIds="";
	$('input[name="chkMassTP"]:checked').each(function() {
		
		if($(this).attr("value") !=null && ($(this).attr("value")!="") )
		{
			if(teacherIds=="")
				teacherIds = teacherIds + $(this).attr("value");
			else
				teacherIds = teacherIds + "," + $(this).attr("value");
		}
	});
	
	if(teacherIds!="")
	{
		$('#loadingDiv').show();
		CandidateGridAjaxNew.printResume(teacherIds,
		{ 
			async: false,
			errorHandler:handleError,
			callback:function(data)
			{
				$('#loadingDiv').hide();
				if(data=="")	
				{
					alert("Resume is not uploaded by the candidates.")
				}
				else
				{
					var messageAndPath = data.split("##");
					var message = messageAndPath[0];
					var path = messageAndPath[1];
										
					if(message=="")
						window.open(path, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0');
					else
						showConfirmationMessageForPrintResume(message, path);
				}	
			}
		});
	}
}

function showConfirmationMessageForPrintResume(message, path)
{
	$('#confirmationMessageForPrintResume').attr("path",path);
	$('#confirmationMessageForPrintResume .modal-body').html(message);
	$('#confirmationMessageForPrintResume').modal('show');
}

function showConfirmationMessageForPrintResumeOk()
{
	var path = $('#confirmationMessageForPrintResume').attr("path");
	if(path!="")
		window.open(path, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0');
}

function showPrevProfile(){
	$('#draggableDivMaster').modal('show');
}

function getEmployeeNumberInfo(flag,table){
	
	
	if(resourceJSON.locale=="fr")
	{	
	   var height="height:500px;";
	   if(flag)
	   height="height:auto;";
	   var div="<div class='modal hide in' id='employeeInfo' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'><div class='modal-dialog' style='width:550px;'><div class='modal-content'><div class='modal-header'><!--<button type='button' class='close' data-dismiss='modal' aria-label='Close'><span class='confirmFalse' aria-hidden='true'>&times;</span></button>--><h4 class='modal-title' id='myModalLabel' style='color:white;'>Employee Information</h4></div><div class='modal-body' style='"+height+"overflow:auto;' id='tableData'></div><div class='modal-footer'><button type='button' class='btn btn-default confirmFalse' >Ok</button></div></div></div></div>";
	}
	else
	{
	
        var height="height:500px;";
	    if(flag)
	    height="height:auto;";
	    var div="<div class='modal hide in' id='employeeInfo' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'><div class='modal-dialog' style='width:850px;'><div class='modal-content'><div class='modal-header'><!--<button type='button' class='close' data-dismiss='modal' aria-label='Close'><span class='confirmFalse' aria-hidden='true'>&times;</span></button>--><h4 class='modal-title' id='myModalLabel' style='color:white;'>Employee Information</h4></div><div class='modal-body' style='"+height+"overflow:auto;' id='tableData'></div><div class='modal-footer'><button type='button' class='btn btn-default confirmFalse' >Ok</button></div></div></div></div>";
     }
	
	try{
			$('#employeeInfo').modal('hide'); 
		}catch(e){}
		try{
			$('#employeeInfo').remove(); 
		}catch(e){}
		$('#draggableDivMaster').after(div);
		$('#draggableDivMaster').modal('hide');
		$('#employeeInfo').modal({show:true,backdrop: 'static',keyboard: false,});
		$('#employeeInfo #tableData').html(table); 
		$('.confirmFalse').click(function(){  			
			$('#employeeInfo').modal('hide');   
			$('#employeeInfo').remove(); 
		 	$('#draggableDivMaster').modal('show');  
		});
	$('#loadingDiv').hide();
 }
function openEmployeeNumberInfo(empId){
$('#loadingDiv').show();
  try{
	  TeacherProfileViewInDivAjax.openEmployeeNumberInfo(empId,{
		  async: false,
		  cache: false,
		  errorHandler:handleError,
		  callback:function(data){		  
		  $('#draggableDivMaster').modal('hide');
		  $('#draggableDivMaster').hide();
		  //alert(data);
		  var divData=data.split('####');
		  if(divData[0].trim()=='1')
			  	getEmployeeNumberInfo(true,divData[1]);
		  		else
		  		getEmployeeNumberInfo(false,data);
		  }
	   });
  }catch(e){alert(e)}
  }
function printCommunicationslogs()
{
	var commDivFlag = $("#myModalCommunications").attr("commDivFlag");
	var jobForTeacherGId = $("#myModalCommunications").attr("jobForTeacherGId");
	var teacherId = $("#myModalCommunications").attr("teacherId");
	var jobId = $("#myModalCommunications").attr("jobId");
	
	//alert("commDivFlag:- "+commDivFlag+"\njobForTeacherGId:- "+jobForTeacherGId+"\nteacherId:- "+teacherId+"\njobId:- "+jobId);
	CandidateGridAjaxNew.getCommunications(commDivFlag,jobForTeacherGId,teacherId,jobId,true,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){
			
			var newWindow = window.open();
			newWindow.document.write(data);	
			newWindow.print();
		}
	});
}
function saveVideo(videoFileName)
{
	var videourl = trim(document.getElementById("videourl").value);
	var teacherId =  document.getElementById("teacherId").value
	var teacherVideoLink = {videolinkAutoId:null,videourl:null, video:null, createdDate:null};
	dwr.engine.beginBatch();
	dwr.util.getValues(teacherVideoLink);
	teacherVideoLink.videourl=videourl;	
	teacherVideoLink.video=	videoFileName;		
	CGServiceAjax.saveOrUpdateVideoLink(teacherVideoLink,teacherId,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data)
		{	
			$('#loadingDiv').hide();				
			hideVideoLinksForm();
			getVideoLinksGrid_DivProfile(teacherId);
		}
	});
	dwr.engine.endBatch();
	return true;
}
function getVideoPlay(videoLinkId)
{
	var teacherId =  document.getElementById("teacherId").value
	$('#videovDiv').modal('show');
	$('#loadingDivWaitVideo').show();
	$('#interVideoDiv').empty();
	var videoPath='';		
	CGServiceAjax.getVideoPlayDivForProfile(videoLinkId,teacherId, { 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
		$('#loadingDivWaitVideo').hide();
		if(data!=""&& data!=null)
		{			
			var videoPath="";				
			videoPath="<video poster='images/video_back.png' src="+data+" width='480' height='280' controls autoplay><source src="+data+" type='video/ogg'>"+resourceJSON.msgBrowservideotag+"<br>"+resourceJSON.msgPlsDownload+" <a target='_blank' href='https://www.apple.com/in/quicktime/download/'>"+resourceJSON.msgQuickTimePlayer+"</a><br>"+resourceJSON.msgRestartSafaribrowser+" </video>";
			$('#interVideoDiv').append(videoPath);			
		}
		else
		{
			$('#interVideoDiv').css("color","Red");
			$('#interVideoDiv').append(resourceJSON.msgProblemoccured);
		}
		}});
}

function getJobSubcategory()
{
	var jobSubCategoryId=document.getElementById("jobCateSelect");
	var jobId=0;
	var districtId = trim(document.getElementById("districtId").value);
	var clonejobId = 0;
	
	var jobSubCategoryIds='';

	 for (var i = 0; i < jobSubCategoryId.options.length; i++) {
	        if(jobSubCategoryId.options[i].selected ==true){
	      	  if(jobSubCategoryIds==''){
	      		  jobSubCategoryIds=  jobSubCategoryId.options[i].value;
	      	  }
	      	  else{
	      		  jobSubCategoryIds=jobSubCategoryIds+ ","+jobSubCategoryId.options[i].value;
	      	  }
	        }
	     }
	 
	if(districtId!="")
	{
			TeacherInfotAjax.getJobSubCategoryForSeach(jobId,jobSubCategoryIds,districtId,clonejobId,
			{ 
				async: true,
				errorHandler:handleError,
				callback: function(data)
				{
					if(data!='')
					{
						document.getElementById("jobSubCateSelect").innerHTML=data;
					}
				}
			});
	}
}

/*function confirmPortfolioReminderMail(id)
{ 
	document.getElementById("teachId").value=id;
	$('#mailconfrmModal').modal('show');
}*/
function sendPortfolioReminderMail(){
	var id=document.getElementById("teachId").value;
	$('#mailconfrmModal').modal('hide');
	$('#loadingDiv').show();
	ManageStatusAjax.sendPortfolioReminderMail(id,{
	async: true,
	errorHandler:handleError,
	callback:function(data)
	{
		$('#loadingDiv').hide();
		$('#mailSuccessModal').modal('show');
	}});
}

function downloadOctUpload(octId)
{
		//alert("downloadAnswer0");
	TeacherProfileViewInDivAjax.downloadOctUpload(octId,{ 
			async: false,
			errorHandler:handleError,
			callback:function(data)
			{
			if(data!="")
				if (data.indexOf(".doc") !=-1)
				{
					document.getElementById('uploadFrameReferencesID').src = ""+data+"";
				}
				else
				{
					document.getElementById("hrefOctUpload").href = data; 
					return false;
				}
			}
		});
	}
function getOctDetails(octNo){
	 
    //var octNo = "518784";//$("#").val();
	$('#loadingDiv').fadeIn();
    TeacherProfileViewInDivAjax.getMemberQualification(octNo,{ 
        async: true,
        errorHandler:handleError,
        callback:function(data)
        {
            
    	$("#changeByName").html("OCT Details for "+$("#teacherNameInPrf").val());
    	$("#octmsgToShow").html(data);    	
    	$("#octContentDiv").modal("show");
    	applyScrollOctMemDetTbl();
    	applyScrollOctMemBasicTbl();
    	applyScrollOctMemAdditionalTbl();
    	$('#draggableDivMaster').modal('hide');
    	$('#loadingDiv').fadeOut();
        }
    });
 
}

function applyScrollOctMemDetTbl()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#octMemberDetail').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 150,
        width: 880,
        minWidth: null,
        minWidthAuto: false,
       	colratio:[100,180,180,210,210],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 20,
        wrapper: false
        });
            
        });			
}

function applyScrollOctMemBasicTbl()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#octMemberBasic').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 200,
        width: 880,
        minWidth: null,
        minWidthAuto: false,
       	colratio:[146,146,146,146,147,148],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 20,
        wrapper: false
        });
            
        });			
}

function applyScrollOctMemAdditionalTbl()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#octMemberAdditional').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        //height: 200,
        width: 880,
        minWidth: null,
        minWidthAuto: false,
       	colratio:[220,220,220,220],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 20,
        wrapper: false
        });
            
        });			
}
function printResume()
{
	var teacherIds="";
	$('input[name="chkMassTP"]:checked').each(function() {
		
		if($(this).attr("value") !=null && ($(this).attr("value")!="") )
		{
			if(teacherIds=="")
				teacherIds = teacherIds + $(this).attr("value");
			else
				teacherIds = teacherIds + "," + $(this).attr("value");
		}
	});
	
	if(teacherIds!="")
	{
		$('#loadingDiv').show();
		CandidateGridAjaxNew.printResume(teacherIds,
		{ 
			async: false,
			errorHandler:handleError,
			callback:function(data)
			{
				$('#loadingDiv').hide();
				if(data=="")	
				{
					alert("Resume is not uploaded by the candidates.")
				}
				else
				{
					var messageAndPath = data.split("##");
					var message = messageAndPath[0];
					var path = messageAndPath[1];
										
					if(message=="")
						window.open(path, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0');
					else
						showConfirmationMessageForPrintResume(message, path);
				}	
			}
		});
	}
}

function showConfirmationMessageForPrintResume(message, path)
{
	$('#confirmationMessageForPrintResume').attr("path",path);
	$('#confirmationMessageForPrintResume .modal-body').html(message);
	$('#confirmationMessageForPrintResume').modal('show');
}

function showConfirmationMessageForPrintResumeOk()
{
	var path = $('#confirmationMessageForPrintResume').attr("path");
	if(path!="")
		window.open(path, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0');
}

function showPrevProfile(){
	$('#draggableDivMaster').modal('show');
}

function getEmployeeNumberInfo(flag,table){
	var height="height:500px;";
	if(flag)
	height="height:auto;";
	var div="<div class='modal hide in' id='employeeInfo' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'><div class='modal-dialog' style='width:850px;'><div class='modal-content'><div class='modal-header'><!--<button type='button' class='close' data-dismiss='modal' aria-label='Close'><span class='confirmFalse' aria-hidden='true'>&times;</span></button>--><h4 class='modal-title' id='myModalLabel' style='color:white;'>Employee Information</h4></div><div class='modal-body' style='"+height+"overflow:auto;' id='tableData'></div><div class='modal-footer'><button type='button' class='btn btn-default confirmFalse' >Ok</button></div></div></div></div>";
		try{
			$('#employeeInfo').modal('hide'); 
		}catch(e){}
		try{
			$('#employeeInfo').remove(); 
		}catch(e){}
		$('#draggableDivMaster').after(div);
		$('#draggableDivMaster').modal('hide');
		$('#employeeInfo').modal({show:true,backdrop: 'static',keyboard: false,});
		$('#employeeInfo #tableData').html(table); 
		$('.confirmFalse').click(function(){  			
			$('#employeeInfo').modal('hide');   
			$('#employeeInfo').remove(); 
		 	$('#draggableDivMaster').modal('show');  
		});
	$('#loadingDiv').hide();
 }



function getEmployeeNumberInfoOther(flag,table){
	
	var height="height:250px;";
	if(flag)
	height="height:auto;";
	var div="<div class='modal hide in' id='employeeInfo' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'><div class='modal-dialog' style='width:550px;'><div class='modal-content' style='width:450px;'><div class='modal-header'><!--<button type='button' class='close' data-dismiss='modal' aria-label='Close'><span class='confirmFalse' aria-hidden='true'>&times;</span></button>--><h4 class='modal-title' id='myModalLabel' style='color:white;'>Employee Information</h4></div><div class='modal-body' style='"+height+"overflow:auto;' id='tableData'></div><div class='modal-footer'><button type='button' class='btn btn-default confirmFalse' >Ok</button></div></div></div></div>";
		try{
			$('#employeeInfo').modal('hide'); 
		}catch(e){}
		try{
			$('#employeeInfo').remove(); 
		}catch(e){}
		$('#draggableDivMaster').after(div);
		$('#draggableDivMaster').modal('hide');
		$('#employeeInfo').modal({show:true,backdrop: 'static',keyboard: false,});
		$('#employeeInfo #tableData').html(table); 
		$('.confirmFalse').click(function(){  			
			$('#employeeInfo').modal('hide');   
			$('#employeeInfo').remove(); 
		 	$('#draggableDivMaster').modal('show');  
		});
	$('#loadingDiv').hide();
 }


function getEmployeeNumberInfoOther(flag,table){
	var height="height:250px;";
	if(flag)
	height="height:auto;";
	var div="<div class='modal hide in' id='employeeInfo' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'><div class='modal-dialog' style='width:550px;'><div class='modal-content' style='width:450px;' ><div class='modal-header'><!--<button type='button' class='close' data-dismiss='modal' aria-label='Close'><span class='confirmFalse' aria-hidden='true'>&times;</span></button>--><h4 class='modal-title' id='myModalLabel' style='color:white;'>Employee Information</h4></div><div class='modal-body' style='"+height+"overflow:auto;' id='tableData'></div><div class='modal-footer'><button type='button' class='btn btn-default confirmFalse' >Ok</button></div></div></div></div>";
		try{
			$('#employeeInfo').modal('hide'); 
		}catch(e){}
		try{
			$('#employeeInfo').remove(); 
		}catch(e){}
		$('#draggableDivMaster').after(div);
		$('#draggableDivMaster').modal('hide');
		$('#employeeInfo').modal({show:true,backdrop: 'static',keyboard: false,});
		$('#employeeInfo #tableData').html(table); 
		$('.confirmFalse').click(function(){  			
			$('#employeeInfo').modal('hide');   
			$('#employeeInfo').remove(); 
		 	$('#draggableDivMaster').modal('show');  
		});
	$('#loadingDiv').hide();
 }
function openEmployeeNumberInfoOther(empId)
{
   $('#loadingDiv').show();
	  try{
		  TeacherProfileViewInDivAjax.openEmployeeNumberInfoOther(empId,{
			  async: false,
			  cache: false,
			  errorHandler:handleError,
			  callback:function(data){
			  $('#draggableDivMaster').modal('hide');
			  $('#draggableDivMaster').hide();
			  var divData=data.split('####');
			  if(divData[0].trim()=='1')
				  getEmployeeNumberInfoOther(true,divData[1]);
			  		else
			      getEmployeeNumberInfoOther(false,data);
			  }
		   });
	  }catch(e){alert(e)}
	  
}
function openEmployeeNumberInfo(empId){
$('#loadingDiv').show();
  try{
	  TeacherProfileViewInDivAjax.openEmployeeNumberInfo(empId,{
		  async: false,
		  cache: false,
		  errorHandler:handleError,
		  callback:function(data){		  
		  $('#draggableDivMaster').modal('hide');
		  $('#draggableDivMaster').hide();
		  //alert(data);
		  var divData=data.split('####');
		  if(divData[0].trim()=='1')
			  	getEmployeeNumberInfo(true,divData[1]);
		  		else
		  		getEmployeeNumberInfo(false,data);
		  }
	   });
  }catch(e){alert(e)}
  }
function printCommunicationslogs()
{
	var commDivFlag = $("#myModalCommunications").attr("commDivFlag");
	var jobForTeacherGId = $("#myModalCommunications").attr("jobForTeacherGId");
	var teacherId = $("#myModalCommunications").attr("teacherId");
	var jobId = $("#myModalCommunications").attr("jobId");
	
	//alert("commDivFlag:- "+commDivFlag+"\njobForTeacherGId:- "+jobForTeacherGId+"\nteacherId:- "+teacherId+"\njobId:- "+jobId);
	CandidateGridAjaxNew.getCommunications(commDivFlag,jobForTeacherGId,teacherId,jobId,true,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){
			
			var newWindow = window.open();
			newWindow.document.write(data);	
			newWindow.print();
		}
	});
}
function getJobCategoryByHQAndBranch()
{
	var branchId = document.getElementById("branchSearchId").value;
	var headQuarterId = document.getElementById("headQuarterId").value;
	
	{
		//DWRAutoComplete
		CGServiceAjax.getJobCategoryByHQAndBranch(branchId,headQuarterId,{ 
			async: false,		   // change by Anurag
			callback: function(data)
			{
				document.getElementById("jobCateSelect").innerHTML=data;
				document.getElementById("jobSubCateSelect").innerHTML="<option value='0'>All</option>";
			}
		});
	}
		//document.getElementById("jobCateSelect").innerHTML="<option value='0'>Select Branch for relevant Jobs Category list</option>";
}
function getJobSubCategoryByCategory()
{
	var jobCategoryIds='';
    var jobCategoryId   = document.getElementById("jobCateSelect");
    for (var i = 0; i < jobCategoryId.options.length; i++) {
        if(jobCategoryId.options[i].selected ==true){
        	if(jobCategoryIds==''){
        		jobCategoryIds=  jobCategoryId.options[i].value;
        	}
      	  	else{
      	  		jobCategoryIds=jobCategoryIds+ ","+jobCategoryId.options[i].value;
      	  	}
        }
    }
	if(jobCategoryIds!='')
	{
		//DWRAutoComplete
		CGServiceAjax.getJobSubCategoryByCategory(jobCategoryIds,{ 
			async: false,		
			callback: function(data)
			{
				document.getElementById("jobSubCateSelect").innerHTML=data;
			}
		});
	}else{
		document.getElementById("jobSubCateSelect").innerHTML="<option value='0'>All;</option>";
	}
}

function getTeacherEducationGrid_DivProfile(teacherId){
 TeacherProfileViewInDivAjax.getTeacherEducationGrid(teacherId, { 
  async: true,
  errorHandler:handleError,
  callback:function(data)
  {
   $('#gridDataTeacherEducations').html(data);
   //applyScrollOnTblTeacherAcademics_profile();
   applyScrollOnEducation();
  }});
}
function hide_box(){
	try { $('#candidatepoolrefreshstatusbox').modal('hide'); } catch (e) {}
}
function sendPortfolioReminderMailToSelectedUser(){
	$('#errordivMessagePortfolio').empty();
	var cnt=0;
	var teacherIds 	= 	new Array();
	teacherIds		=	getTeacherIdsMassTP();
	var districtId 	= 	document.getElementById("districtId").value;
	
	if(teacherIds=="" || teacherIds=="null"){
		$('#errordivMessagePortfolio').show();
		$('#errordivMessagePortfolio').append("&#149; Please select atleast one candidate<br>");
		cnt++;
	}

	if(cnt==0){
		$('#allCandidateSendMailModal').modal('hide');
		$('#loadingDiv').show();
		PortfolioReminderAjax.sendPortfolioReminderMailToSelectedUser(teacherIds,districtId,{
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#loadingDiv').hide();
			$('#allCandidateSendMailModal').modal('hide');
			$('#mailSuccessModal').modal('show');
		}});
	}
}
function confirmPortfolioReminderAllMail(){ 
	PortfolioReminderAjax.districtCandidateRefreshPool({
		async: true,
		callback: function(data){
			$('#errordivMessagePortfolio').empty();
			if(data.trim() == "true"){
				$('#allCandidateSendMailModal').modal('show');
			}else if(data == "false"){
				$('#candidatepoolrefreshstatusbox').modal('show');		
			}
			errorHandler:handleError 
		}
	});
}

function sendPortfolioReminderMailToAllUser(){

	   try
	   {
		   var mydiv = document.getElementById("mydiv");
	       var curr_width = parseInt(mydiv.style.width);
	      
	       if(deviceType)
	       {
	    	   mydiv.style.width = 630+"px";  
	       }
	       else
		   {
	    	   mydiv.style.width = 647+"px";  
		   } 
	   }
	   catch(e){alert(e)}
	   /*.........................................*/
		currentPageFlag="";
		$('#advanceSearchDiv').hide();
		$('#searchLinkDiv').fadeIn();
		if(document.getElementById("districtId").value==0){
			document.getElementById('schoolName').readOnly=true;
		}
		if(document.getElementById("schoolName").value==''){
			document.getElementById("schoolId").value=0;
		}
		if(document.getElementById("districtName").value==''){
			document.getElementById("districtId").value=0;
		}
		if(document.getElementById("regionName").value==''){
			document.getElementById("regionId").value=0;
		}
		if(document.getElementById("universityName").value==''){
			document.getElementById("universityId").value=0;
		}
		if(document.getElementById("degreeName").value==''){
			document.getElementById("degreeId").value=0;
		}
		if(document.getElementById("certType").value==''){
			document.getElementById("certificateTypeMaster").value=0;
		}
		
		$('#loadingDiv').show();
		var firstName		=	trim(document.getElementById("firstName").value); 
		var lastName		=	trim(document.getElementById("lastName").value);
		var emailAddress	=	trim(document.getElementById("emailAddress").value);
		var districtId		=	trim(document.getElementById("districtId").value); 
		var schoolId		=	trim(document.getElementById("schoolId").value);
		var fromDate	=	trim(document.getElementById("fromDate").value);
		var toDate		=	trim(document.getElementById("toDate").value); 
		var certType		=	trim(document.getElementById("certificateTypeMaster").value);
		var regionId		=	trim(document.getElementById("regionId").value);
		var degreeId		=	trim(document.getElementById("degreeId").value);
		var universityId		=	trim(document.getElementById("universityId").value);
		var normScoreSelectVal		=	trim(document.getElementById("normScoreSelectVal").value);
		var qualQues		="";
		try{
			qualQues=trim(document.getElementById("qualQues").value);
		}catch(err){}
		
		var references	=	trim(document.getElementById("references").value);
		var resume		=	trim(document.getElementById("resume").value);
		
		var tnl = document.getElementById("subjects");  
		
		var arr =[];
		for(i=0;i<tnl.length;i++){
			if(tnl[i].selected == true){
				var dd =tnl[i].value;
				if(dd!=0)
				arr.push(dd);
			}  
		}  
		
		var fitScoreSelectVal		=	trim(document.getElementById("fitScoreSelectVal").value);
		var YOTESelectVal		=	trim(document.getElementById("YOTESelectVal").value);
		var AScoreSelectVal		=	trim(document.getElementById("AScoreSelectVal").value);
		var LRScoreSelectVal		=	trim(document.getElementById("LRScoreSelectVal").value);
		
		
		var canServeAsSubTeacherYes		=	document.getElementById("canServeAsSubTeacherYes").checked;
		var canServeAsSubTeacherNo		=	document.getElementById("canServeAsSubTeacherNo").checked;
		var canServeAsSubTeacherDA		=	document.getElementById("canServeAsSubTeacherDA").checked;
		var TFAA		=	document.getElementById("TFAA").checked
		var TFAC		=	document.getElementById("TFAC").checked;
		var TFAN		=	document.getElementById("TFAN").checked;
		
		try{
			var ifrmFit = document.getElementById('ifrmFit');
			var innerFit = ifrmFit.contentDocument || ifrmFit.contentWindow.document;
			var inputfitScore = innerFit.getElementById('fitScoreFrm');
			document.getElementById("fitScore").value=inputfitScore.value;
		}catch(e){}
		
		
		try{
			var ifrmYOTE = document.getElementById('ifrmYOTE');
			var innerYOTE = ifrmYOTE.contentDocument || ifrmYOTE.contentWindow.document;
			var inputYOTE = innerYOTE.getElementById('YOTEFrm');
			document.getElementById("YOTE").value=inputYOTE.value;
		}catch(e){}	
		
		
		try{
			var ifrmAScore = document.getElementById('ifrmAScore');
			var innerAScore = ifrmAScore.contentDocument || ifrmAScore.contentWindow.document;
			var inputAScore = innerAScore.getElementById('AScoreFrm');
			document.getElementById("AScore").value=inputAScore.value;
		}catch(e){}
		
		try{
			var ifrmLRScore = document.getElementById('ifrmLRScore');
			var innerLRScore = ifrmLRScore.contentDocument || ifrmLRScore.contentWindow.document;
			var inputLRScore = innerLRScore.getElementById('LRScoreFrm');
			document.getElementById("LRScore").value=inputLRScore.value;
		}catch(e){}	
		
		var jobAppliedFromDate	=	trim(document.getElementById("jobAppliedFromDate").value);
		var jobAppliedToDate	=	trim(document.getElementById("jobAppliedToDate").value); 
		var daysVal = $("input[name=jobApplied]:radio:checked").val();
		
		var internalCandidate=document.getElementById("internalCandidate").checked;
		var stateId=document.getElementById("stateId").value;
		
		var stateForcandidate = document.getElementById("stateIdForcandidate").value;
		var certTypeForCandidate = document.getElementById("certificateTypeMasterForCandidate").value;

		try{
			var iframeNorm = document.getElementById('ifrmNorm');
			var innerNorm = iframeNorm.contentDocument || iframeNorm.contentWindow.document;
			var inputNormScore = innerNorm.getElementById('normScoreFrm');
			document.getElementById("normScore").value=inputNormScore.value;
		}catch(e){}	
		try{
			var iframeCGPA = document.getElementById('ifrmCGPA');
			var innerCGPA = iframeCGPA.contentDocument || iframeCGPA.contentWindow.document;
			var inputCGPA = innerCGPA.getElementById('CGPAFrm');
			document.getElementById("CGPA").value=inputCGPA.value;
		}catch(e){}
		
		var fitScore=document.getElementById("fitScore").value;
		var YOTE=document.getElementById("YOTE").value;
		var AScore=document.getElementById("AScore").value;
		var LRScore=document.getElementById("LRScore").value;
		
		var normScore		=	trim(document.getElementById("normScore").value);
		var CGPA		=	trim(document.getElementById("CGPA").value);
		var CGPASelectVal		=	trim(document.getElementById("CGPASelectVal").value);
		var candidateStatus		=	trim(document.getElementById("candidateStatus").value);
		var teacherSSN	=	trim(document.getElementById("ssn").value);
		var empNo	=	trim(document.getElementById("empNo").value);
		var jobCategoryIds='';
	    var jobCategoryId   = document.getElementById("jobCateSelect");
	    for (var i = 0; i < jobCategoryId.options.length; i++) {
	        if(jobCategoryId.options[i].selected ==true){
	        	if(jobCategoryIds==''){
	        		jobCategoryIds=  jobCategoryId.options[i].value;
	        	}
	      	  	else{
	      	  		jobCategoryIds=jobCategoryIds+ ","+jobCategoryId.options[i].value;
	      	  	}
	        }
	    }
		var tagsearchId="";
	    if(document.getElementById("tagsearchId"))
	    	tagsearchId = trim(document.getElementById("tagsearchId").value);
		
	    var hiddenCertTypeIds 	=	document.getElementsByName("hiddenCertTypeId");
		var certIds =[];
		for(var i=0;i<hiddenCertTypeIds.length;i++)
		{
			certIds.push(hiddenCertTypeIds[i].value);
		}
		var stateId2	=	trim(document.getElementById("stateId2").value);
		var zipCode		=	trim(document.getElementById("zipCode").value);
		var epiFromDate	=	trim(document.getElementById("epiFromDate").value);
		var epiToDate	=	trim(document.getElementById("epiToDate").value);
		
		try{
	 	    // Manke Status as multi selection
	 	    var candidateStatusSet='';
	 	    var candidateStatusId	=	document.getElementById("candidateStatus");
	 	    for (var i = 0; i < candidateStatusId.options.length; i++) {
	 	        if(candidateStatusId.options[i].selected ==true){
	 	        	if(candidateStatusSet==''){
	 	        		candidateStatusSet=  candidateStatusId.options[i].value;
	 	        	}
	 	      	  	else{
	 	      	  		candidateStatusSet=candidateStatusSet+ "#%#"+candidateStatusId.options[i].value;
	 	      	  	}
	 	        }
	 	    }

	 	 }catch(err){}
		
		PortfolioReminderAjax.sendPortfolioReminderMailToAllUser(firstName,lastName,emailAddress,regionId,degreeId,universityId,normScoreSelectVal,qualQues,
				normScore,CGPA,CGPASelectVal,candidateStatusSet,stateId,internalCandidate,noOfRows,page,sortOrderStr,sortOrderType,districtId,schoolId,fromDate,toDate,certType,references,resume,fitScoreSelectVal,YOTESelectVal,AScoreSelectVal,LRScoreSelectVal,canServeAsSubTeacherYes,canServeAsSubTeacherNo, 
				canServeAsSubTeacherDA,TFAA,TFAC,TFAN,fitScore,YOTE,AScore,LRScore,jobAppliedFromDate,jobAppliedToDate,daysVal,arr,$('#newcandidatesonly').is(":checked"),teacherSSN,stateForcandidate,certTypeForCandidate,empNo,jobCategoryIds,tagsearchId,stateId2,zipCode,certIds,epiFromDate,epiToDate,{ 
			async: true,
			callback: function(data)
			{
			    $('#loadingDiv').hide();
			    $('#allCandidateSendMailModal').modal('hide');
			    $('#mailSuccessModal').modal('show');
			},
			errorHandler:handleError 
		});
   }



function generateSkillPDF(teacherId)
{
	$('#loadingDiv').show();
	PrintCertificateAjax.generateSkillPDF(teacherId,{
		async: true,
		errorHandler:handleError,
		callback: function(data){
			//window.open(data);
			$('#loadingDiv').hide();
			if(deviceType)
			{
				if (data.indexOf(".doc")!=-1)
				{
					$("#docfileNotOpen").css({"z-index":"3000"});
			    	$('#docfileNotOpen').show();
				}
				else if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					$("#exelfileNotOpen").css({"z-index":"3000"});
			    	$('#exelfileNotOpen').show();
				}
				else
				{
					$('#modalDownloadSPC').modal('hide');
				}
			}
			else if(deviceTypeAndroid)
			{
				if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					try{
						$('#modalDownloadSPC').modal('hide');
					}catch(err){}
					document.getElementById('ifrmSPC').src = ""+data+"";
				}
				else
				{
					$('#modalDownloadSPC').modal('hide');
				}
			}
			else if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
			{
				try{
					$('#modalDownloadSPC').modal('hide');
				}catch(err){}
				document.getElementById('ifrmSPC').src = ""+data+"";
			}
			else
			{
				$('#modalDownloadSPC').modal('hide');
				document.getElementById('ifrmSPC').src = ""+data+"";
				try{
					$('#modalDownloadSPC').modal('show');
				}catch(err){}
			}
		}
	});
}

function getTeacherAssessmentGrid_DivProfile(teacherId)
{
	TeacherProfileViewInDivAjax.getTeacherAssessmentGrid(teacherId,noOfRowsEJ,pageEJ,sortOrderStrEJ,sortOrderTypeEJ, { 
  async: true,
  errorHandler:handleError,
  callback:function(data)
  {
   $('#gridTeacherAssessment').html(data);
   //applyScrollOnTblTeacherAcademics_profile();
   applyScrollOnAssessments();
  }});
}

function advanceCheck()
{
	var everyChild = document.querySelectorAll("#advanceSearchDiv *");
	for (var i = 1; i<everyChild.length; i++) {
		if (everyChild[i].id && everyChild[i].value!=undefined 
				&& everyChild[i].value!=0 && everyChild[i].value!=-1
				//&& document.getElementById("qualQues").value!=1
				&& everyChild[i].value!="" && $(everyChild[i]).attr('type')!='checkbox'
				&& $(everyChild[i]).attr('type')!='hidden'	
				&&	everyChild[i].id!='qualQues' 
				//&& everyChild[i].id!=districtName 
				) 
	    	
		{
			alert(everyChild[i].value+"---"+everyChild[i].id+"----"+$(everyChild[i]).attr('type'));
			$('#advanceCheckTooltip').show();
				break;
			
			
			
		}
		else if($(everyChild[i]).attr('type')=='checkbox' && $(everyChild[i]).is(':checked'))
		{
			$('#advanceCheckTooltip').show();
			break;
		}	
			
		else
			$('#advanceCheckTooltip').hide();
		
	}
}

function checkSpViolatedForSpReset(teacherId)
{
	CGServiceAjax.checkSpCompleted(teacherId,{
		async: false,
		errorHandler:handleError,
		callback: function(data){
			if(data==1)
			{
				$('#message2show').html(resourceJSON.msgCompletedSpInventory);
				$('#myModal2').modal('show');
				return false;
			}
			else if(data==3)
			{
				makeSpIncomplete(teacherId);
				return false;
			}
			else
			{
				$('#message2show').html(resourceJSON.msgCandidateHasNotCompleteSp);
				$('#myModal2').modal('show');
				return false;
			}
		}
	});
}
var teacherIdForResetSp = '';
function makeSpIncomplete(teacherId)
{
	$("#draggableDivMaster").modal('hide');
	$('#btnResetSp').attr("onclick", "makeIncompleteSp("+teacherId+")");
	document.getElementById("teacherIdForResetSp").value = teacherId;
	teacherIdForResetSp = teacherId;
	$('#errordivResetSp').empty();
	$('#resetSpDescription').find(".jqte_editor").css("background-color", "");
	$('#resetSpDescription').find(".jqte_editor").html("");
	$('#modalResetSp').modal('show');
}

function makeIncompleteSp(teacherId)
{	
	$('#errordivResetSp').empty();
	$('#resetSpDescription').find(".jqte_editor").css("background-color", "");
	var teacherDetail = {teacherId:teacherId};
	if ($('#resetSpDescription').find(".jqte_editor").text().trim()=="")
	{
		$('#errordivResetSp').append("&#149; "+resourceJSON.msgResetSp+"<br>");
		$('#resetSpDescription').find(".jqte_editor").focus();
		$('#resetSpDescription').find(".jqte_editor").css("background-color", "#F5E7E1");
		return false;
	}
	else
	{
		var spResetMsg = trim($('#resetSpDescription').find(".jqte_editor").html());
		$('#modalResetSp').modal('hide');
		$('#loadingDiv').show();
		CGServiceAjax.makeSpIncomplete(teacherDetail,spResetMsg,{ 
			async: true,
			callback: function(data){
			$('#loadingDiv').hide();
			$('#resetMsg2Show').html(resourceJSON.msgSpStatusIncomp1+" "+data+"'s "+resourceJSON.msgSpStatusIncomp2);
			$('#modalResetSpMsg').modal('show');
		},
		errorHandler:handleError  
		});
	}
}

function closeResetSp()
{
	$('#modalResetSp').modal('hide');
	$('#modalResetSpMsg').modal('hide');
	showProfileContentForTeacher(this,teacherIdForResetSp,1,'Teacher Pool',event);
}

function getTeacherLicenceGrid_DivProfile(teacherId){
	
	TeacherProfileViewInDivAjax.displayRecordsBySSNForDate(teacherId,{ 
		async: true,
		callback: function(data)
		{	
		//alert(data);
		if(data==null||data=="")
		{				
		//ssnNotFound("No License data found for your Social Security");
		return false;
		}
		else
		{
			//	$('#licenseDivDate').show();
				$('#divLicneseGridCertificationsGridDate').html(data);			
			//    $('#loadingDiv').hide();				
			   applyScrollOnTblLicenseDate();
		}
		
		},
	errorHandler:handleError
	});
}


function getLEACandidatePortfolio_DivProfile(teacherId){
	
	TeacherProfileViewInDivAjax.getLEACandidatePortfolio(teacherId,{ 
		async: true,
		callback: function(data)
		{	
		//alert(data);
		if(data==null||data=="")
		{				
		ssnNotFound("No License data found for your Social Security");
		return false;
		}
		else
		{
			//	$('#licenseDivDate').show();
				$('#divLEACandidatePotfolioDiv').html(data);			
			//    $('#loadingDiv').hide();				
				applyScrollOnLEACandidatePorfolio();
		}
		
		},
	errorHandler:handleError
	});
}

//---------------------- assessment reset timed out to incomplete start ----------------------
var teacherIdForResetAssessment = '';
function checkViolatedForAssessmentReset(teacherId,assessmentType,teacherAssessmentStatusId)
{
	CGServiceAjax.checkAssessmentCompleted(teacherId,assessmentType,teacherAssessmentStatusId,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){
			if(data==1){
				if(assessmentType==1){
					$('#message2show').html(resourceJSON.msgcompletedBaseInventory);
				}else if(assessmentType==2){
					$('#message2show').html(resourceJSON.msgCompletedJsiInventory);
				}else if(assessmentType==3){
					$('#message2show').html(resourceJSON.msgCompletedSpInventory);
				}else if(assessmentType==4){
					$('#message2show').html(resourceJSON.msgCompletedIpiInventory);
				}
				$('#myModal2').modal('show');
				return false;
			}
			else if(data==3){
				$("#draggableDivMaster").modal('hide');
				$('#btnResetAssessment').attr("onclick", "makeAssessmentIncomplete("+teacherId+","+assessmentType+","+teacherAssessmentStatusId+")");
				document.getElementById("teacherIdForResetAssessment").value = teacherId;
				teacherIdForResetAssessment = teacherId;
				$('#errordivResetAssessment').empty();
				$('#resetAssessmentDescription').find(".jqte_editor").css("background-color", "");
				$('#resetAssessmentDescription').find(".jqte_editor").html("");
				$('#modalResetAssessment').modal('show');
				return false;
			}
			else{
				if(assessmentType==1){
					$('#message2show').html(resourceJSON.CandidateHasNotComplete);
				}else if(assessmentType==2){
					$('#message2show').html(resourceJSON.msgCandidateHasNotCompleteJsi);
				}else if(assessmentType==3){
					$('#message2show').html(resourceJSON.msgCandidateHasNotCompleteSp);
				}else if(assessmentType==4){
					$('#message2show').html(resourceJSON.msgCandidateHasNotCompleteIpi);
				}
				$('#myModal2').modal('show');
				return false;
			}
		}
	});	

}

function makeAssessmentIncomplete(teacherId,assessmentType,teacherAssessmentStatusId)
{
	$('#errordivResetAssessment').empty();
	$('#resetAssessmentDescription').find(".jqte_editor").css("background-color", "");
	var teacherDetail = {teacherId:teacherId};
	if ($('#resetAssessmentDescription').find(".jqte_editor").text().trim()=="")
	{
		$('#errordivResetAssessment').append("&#149; "+resourceJSON.msgResetSp+"<br>");
		$('#resetAssessmentDescription').find(".jqte_editor").focus();
		$('#resetAssessmentDescription').find(".jqte_editor").css("background-color", "#F5E7E1");
		return false;
	}
	else
	{
		var resetMsg = trim($('#resetAssessmentDescription').find(".jqte_editor").html());
		$('#modalResetAssessment').modal('hide');
		$('#loadingDiv').show();
		CGServiceAjax.makeAssessmentIncomplete(teacherDetail,assessmentType,teacherAssessmentStatusId,resetMsg,{
			async: false,
			errorHandler:handleError,
			callback: function(data){
				$('#loadingDiv').hide();
				$('#resetAssessmentMsg2Show').html(resourceJSON.msgSpStatusIncomp1+" "+data+"'s "+resourceJSON.msgSpStatusIncomp2);
				$('#modalResetAssessmentMsg').modal('show');
			}
		});
	}
}

function closeAssessmentResetDiv()
{
	$('#modalResetAssessment').modal('hide');
	$('#modalResetAssessmentMsg').modal('hide');
	showProfileContentForTeacher(this,teacherIdForResetAssessment,1,'Teacher Pool',event);
}

//---------------------- assessment reset timed out to incomplete end ----------------------



// get advance serach data 

function getAdvanceSerachData(){
	
	TeacherInfotAjax.getAdvanceSearchRecord({ 
		async: false,
		callback: function(data)
		{
	 
		$("#stateId").html(data[0]);
		$("#stateIdForcandidate").html(data[0]);
		$("#candidateStatus").html(data[1]);		
		$(".tagsearchClass").html(data[4]);
		$("#stateId2").html(data[0]);
		
		
		//	 alert(data[0]); // for Tags
		//	 alert(data[1]); // 
		},
		errorHandler:handleError 
	});	

}

//---------------------- assessment reset timed out to incomplete end ----------------------

function getSchoolList(jobId)
{
	document.getElementById("jobIdSchool").value=jobId; 
	$("#loadingDiv").fadeIn();	
	TeacherInfotAjax.getSchoolListByJob(jobId,noOfRowsSchool,pageforSchool,sortOrderStrSchool,sortOrderTypeSchool,{
		async: true,
		callback: function(data)
		{
		    $("#loadingDiv").hide();
			$('#myModalforSchholList').modal('show');
			document.getElementById("schoolListDivNew").innerHTML=data;
			applyScrollOnTb();
		},
		errorHandler:handleError	
	});	
}

function disconnectTalent(){
	$("#errorDiv").empty();
	$("#errorDiv").hide();
	var teacherId="";
	var newksnId ="";
	newksnId = trim(document.getElementById("inputKSN").value);
	//alert("newksnId :: "+newksnId)
	teacherId = document.getElementById("disconnectTalentId").value;
	var errorCount = 0;
	if(newksnId.length==0){
		$("#inputKSN").focus();
		$("#errorDiv").show();
		$('#errorDiv').append("&#149; Please enter KSN ID<br>");
		errorCount++;
	}

	if(errorCount==0)
		//CandidateGridSubAjax.disconnectQueue(teacherId,newksnId, {
		ManageStatusAjax.disconnectQueue(teacherId,newksnId, {
		preHook:function(){$('#loadingDiv').show()},
		postHook:function(){$('#loadingDiv').hide()},
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			//alert(data);
			var result = data.split("###");
			if(result.length>0){
				if(result[0]=="error"){
					$("#inputKSN").focus();
					$("#errorDiv").show();
					$('#errorDiv').append(result[1]);
				}
				else{
					if(data.length>0){
						$("#draggableDivMaster").modal("hide");
						$("#disconnectResponse").modal("show");
						if(result[0]=="Success")
							$("#disconnectRespMsg").text(result[0]+": "+result[1]);
						else
							$("#disconnectRespMsg").text(data);
					}
				}
			}
		}
	});	

}

function hideDisconnectResponse()
{
	$("#disconnectResponse").modal("hide");
	$("#draggableDivMaster").modal("show");
	//$("#tpDisconnect").hide();
	//$("#inputKSN").val("");
	showLinkToKSNResponse(document.getElementById("disconnectTalentId").value);
}

function checkForInt(evt) {
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==8)
		return true;
	return( (charCode >= 48 && charCode <= 57)||(charCode==13));
}


function refereshStatusINChangeStatus(){
	var teacherId= document.getElementById("teacherIdByStatus").value;
	var jobId= document.getElementById("jobIdByStatus").value;
	var currentRow = $(".shortFuncChng_"+jobId).attr("id");
	currentRow = currentRow.replace("hireTpChng", "");
	
	var candidateCurrentStatus=document.getElementById("candidateCurrentStatus").value;
	var changeFunctionAttribute = $(".shortFuncChng_"+jobId).attr("onclick");
	var newStatusName=$("#statusChangeId option:selected").html();
	var tempIdOld = parseInt($("#statusChangeId" + " option").filter(function() { return $(this).text() == candidateCurrentStatus}).attr("recorder"));
	var tempIdNew = parseInt($("#statusChangeId option:selected").attr("recorder"));

	if(tempIdNew>tempIdOld){

	if(candidateCurrentStatus=="Hired" && candidateCurrentStatus=="Declined" && candidateCurrentStatus=="Rejected" && candidateCurrentStatus=="Withdrew"){
		
		}else{
		
			$("#candidateCurrentStatus").html(newStatusName);
			$("#shortStatusChng_"+jobId).html(newStatusName);
			changeFunctionAttribute = changeFunctionAttribute.replace(candidateCurrentStatus, newStatusName);
			$(".shortFuncChng_"+jobId).attr("onclick",changeFunctionAttribute);
		}

		if(newStatusName=="Hired"){

			var countTotalRows = $('#jobTable tr').length;

			var counthired = $('#jobTable .fa-graduation-cap').length;

			var currentRow = $(".shortFuncChng_"+jobId).attr("id");			

			currentRow = currentRow.replace("hireTpChng", "");			

			var getHiredJobTitle =$("#jobJobTitle"+currentRow).html();

			var contetnt = "";
			var m_names = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
			var d = new Date();
			var curr_date = d.getDate();
			var curr_month = d.getMonth();
			var curr_year = d.getFullYear();
			var dateShow =m_names[curr_month]+ " " + curr_date+ ", " + curr_year;
			if(counthired>0){
				var dataContent =  $(".hireGradCap").attr("data-original-title");

				dataContent = dataContent.replace("<table>", "");

				dataContent = dataContent.replace("</table>", "");

				var newcontetnt = "<table>"+dataContent+"<tr><td>Date of Hiring: "+dateShow+"<br>Job Order: "+getHiredJobTitle+"</td></tr></table>";

				contetnt = newcontetnt;
				//alert("17");
			}else{
				contetnt = "<table><tr><td>Date of Hiring: "+dateShow+"<br>Job Order: "+getHiredJobTitle+"</td></tr></table>";
				var finalContent = "&nbsp;<a data-html='true' class='hireGradCap' data-original-title=\'"+contetnt+"\' rel='tooltip'><span class='fa-graduation-cap icon-large iconcolor'></span></a>";
				for (i = 1; i<= countTotalRows; i++) {  
					$("#hireTpChng"+i).after(finalContent);
				}
				//alert("18");
				$(".hireGradCap").tooltip();
			}
			
			$(".hireGradCap").attr("data-original-title",contetnt);
		}
	}
	$("#loadingDiv").hide();
}

function changeonspecificrows(teacherIds){
	if(teacherIds.length>0){
		CandidateGridSubAjax.updateAppliedJobsonCG(teacherIds, { 
			async:true,
			errorHandler:handleError,
			callback:function(data)
			{
				if(data!=null){
					for(var i=0;i<data.length;i++){
						var subpart = data[i].split("###");
						$("#appjob"+subpart[0]).html(subpart[1]);
					}
				}
			}
		});
	}
}

function showProfileContentForTeacher2(teacherId,noOfRecordCheck,sVisitLocation)
{	//alert("call "+noOfRecordCheck);
	var districtMaster = null;
	TeacherProfileViewInDivAjax.showProfileContentForTeacherNew2(teacherId,noOfRecordCheck,sVisitLocation,districtMaster,0,{ 
			async: true,
			callback: function(data){
		var allPartialProfilesDiv = $("#allPartialProfilesDiv").html();
		$("#allPartialProfilesDiv").html(allPartialProfilesDiv+data);
	},
	errorHandler:handleError  
	});
} 

function displayProfileContentForTeacher(dis,teacherId,noOfRecordCheck,sVisitLocation,event)
{

	var checkProfileExist = $("#profileDivUtId"+teacherId).length;
	
	if(checkProfileExist>0){
		$('#loadingDiv').show();	
		$('.allPopOver').popover('hide');
		var scrollTopPosition = $(window).scrollTop();
		
		var x=event.clientX;
		var y=event.clientY+scrollTopPosition;		
		var z=y-335;
		if(y>550)
		{
			z=y-672;
		}
		
		var districtMaster = null;
		showProfileContentClose();	
		
		document.getElementById("teacherIdForprofileGrid").value=teacherId;
		
		$("#draggableDiv").html($("#profileDivUtId"+teacherId).html());
		$('#loadingDiv').hide();
        $("#draggableDivMaster").modal('show');
            
		TeacherProfileViewInDivAjax.showProfileContentForTeacherNew(teacherId,noOfRecordCheck,sVisitLocation,districtMaster,1,{ 
	
			async: true,
			callback: function(resdata){
			setTimeout(function () {
				$("#profile_id").after(resdata);
				$('#removeWait').remove();
				$('#iconpophover7').tooltip();
	        }, 100);
			
			TeacherProfileViewInDivAjax.showProfilePartialData(teacherId,noOfRecordCheck,sVisitLocation,districtMaster,0,{ 
				async: true,
				callback: function(dataPartial){
				var prvsData= $("#draggableDiv").html()+dataPartial;
				$("#draggableDiv").html(prvsData);
				if($("#achievementSlider").length>0){					
					var aScore = $("#teacherPoolaScore_"+teacherId).html();
					$("#ascore_profile").html(aScore);					
					var achmVal =0;
					if(aScore!="N/A" && aScore!=""){
						var aScoreSplit = aScore.split("/");
						achmVal =aScoreSplit[0];
					}
					$("#txt_sl_slider_aca_ach").val(achmVal);					
					$("#achievementSlider").html("<iframe id='ifrm_aca_ach' src='slideract.do?name=slider_aca_ach&tickInterval=5&max=20&swidth=240&svalue="+achmVal+"' scrolling='no' frameBorder='0' style='border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:280px;'></iframe>");
				}
				if($("#leadershipSlider").length>0){
					var lScore = $("#teacherPoollrScore_"+teacherId).html();
					$("#lrscore_profile").html(lScore);					
					var leadVal =0;
					if(lScore!="N/A" && lScore!=""){
						var lScoreSplit = lScore.split("/");
						leadVal =lScoreSplit[0];
					}
					$("#txt_sl_slider_lea_res").val(leadVal);					
					$("#leadershipSlider").html("<iframe id='ifrm_lea_res' src='slideract.do?name=slider_lea_res&tickInterval=5&max=20&swidth=240&svalue="+leadVal+"' scrolling='no' frameBorder='0' style='border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:280px;'></iframe>");
				}
				try{
					if($("#normScoreByT"+teacherId).length>0){
						var normScoreByT = $("#normScoreByT"+teacherId).html();
						
						if(normScoreByT!="N/A"){
							$("#baseFirst"+teacherId).show();
							$("#baseSecond"+teacherId).hide();
						}
					}
					
				}catch (e) {
					// TODO: handle exception
				}
				$('#iconpophover7').tooltip();
			}
			});
			//$("#draggableDiv").html(resdata);
		}
		});
		/*@End 
		@Ashish 
		@Description :: for Teacher Profile(Draggable Div)*/
	  $('#loadingDiv').hide();

	  //$('.profile').popover('show')
	  $('#tpResumeprofile').tooltip();
	  $('#tpPhoneprofile').tooltip();
	  $('#tpPDReportprofile').tooltip();
	  $('#tpTeacherProfileVisitHistory').tooltip();
	  $('#teacherProfileTooltip').tooltip();
	  $('textarea').jqte();
	  try{
		  $('#tpDisconnect').tooltip();
	  }catch(e){}
	}else{
		showProfileContentForTeacher(dis,teacherId,noOfRecordCheck,sVisitLocation,event);		
	}
}

function showProfileContentForMultipleTeacher(teachers){
	var districtMaster = null;
	var location = 'Teacher Pool';
	TeacherProfileViewInDivAjax.showProfileContentForTeacherNewMul(teachers,location,{ 
			async: true,
			callback: function(data){
		var allPartialProfilesDiv = $("#allPartialProfilesDiv").html();
		$("#allPartialProfilesDiv").html(allPartialProfilesDiv+data);
		//$("#allPartialProfilesDiv").show();
	},
	errorHandler:handleError  
	});
}

function showLinkToKSNResponse(teacherId){
	ManageStatusAjax.getLinktoKSNResponse(teacherId,{ 
		async: true,
		callback: function(data){
		$("#ksndetails").empty();
		$("#ksndetails").html(data);
		try{
			$("#profileDivUtId"+teacherId).remove();
			showProfileContentForMultipleTeacher(teacherId);
		}catch(e){}

},
errorHandler:handleError  
});
}

function getBackgroundCheckGrid_DivProfile(teacherId)
{
	TeacherProfileViewInDivAjax.getBackgroundCheckGrid_DivProfile(teacherId, { 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#gridDataBackgroundCheck').html(data);			
			applyScrollOnTblBackgroundCheck_profile();
		}});
}
function downloadBackgroundCheck(teacherId,linkId)
{	
	$('#loadingDiv').show();
	CandidateGridAjaxNew.downloadBackgroundCheck(teacherId,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{			
			$('#loadingDiv').hide();
			if(data=="")	
			{
				alert("Background Check is not uploaded by the candidate.")
			}
			else
			{
				if(deviceType)
				{
					if (data.indexOf(".doc")!=-1)
					{
						$("#docfileNotOpen").css({"z-index":"3000"});
				    	$('#docfileNotOpen').show();
					}
					else if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
					{
						$("#exelfileNotOpen").css({"z-index":"3000"});
				    	$('#exelfileNotOpen').show();
					}
					else
					{
						document.getElementById(linkId).href = data; 	
					}
				}
				else if(deviceTypeAndroid)
				{
					if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
					{
						document.getElementById('ifrmTrans').src = ""+data+"";
					}
					else
					{
						document.getElementById(linkId).href = data; 		
					}
				}
				else if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					document.getElementById('ifrmTrans').src = ""+data+"";
				}
				else
				{
					document.getElementById(linkId).href = data; 		
				}			
			}	
			return false;			
							
		}
	});
}