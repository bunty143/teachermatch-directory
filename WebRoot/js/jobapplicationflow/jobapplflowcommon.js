var digitToString=new Array("","one","two","three","four","five","six","seven","eight","nine","ten");
var dspqCalInstance=null;
var isJobAplied=false;

/*========= Start ... Paging and Sorting ===========*/
var dp_Academics_page = 1;
var dp_Academics_noOfRows = 100;
var dp_Academics_sortOrderStr="";
var dp_Academics_sortOrderType="";

var dp_Certification_page = 1;
var dp_Certification_noOfRows = 10;
var dp_Certification_sortOrderStr="";
var dp_Certification_sortOrderType="";

var dp_Reference_page = 1;
var dp_Reference_noOfRows = 10;
var dp_Reference_sortOrderStr="";
var dp_Reference_sortOrderType="";

var dp_AdditionalDocuments_page = 1;
var dp_AdditionalDocuments_noOfRows = 10;
var dp_AdditionalDocuments_sortOrderStr="";
var dp_AdditionalDocuments_sortOrderType="";

var dp_Employment_page=1;
var dp_Employment_noOfRows=100;
var dp_Employment_sortOrderStr="";
var dp_Employment_sortOrderType="";

var dp_SubjectArea_page = 1;
var dp_SubjectArea_noOfRows = 10;
var dp_SubjectArea_sortOrderStr="";
var dp_SubjectArea_sortOrderType="";

var dp_VideoLink_Rows=10
var dp_VideoLink_page=1
var dp_VideoLink_sortOrderStr=""
var dp_VideoLink_sortOrderType=""

var dp_StdTchrExp_Rows=10;
var dp_StdTchrExp_page=1;
var dp_StdTchrExp_sortOrderStr="";
var dp_StdTchrExp_sortOrderType="";

var dp_Residency_Rows=10;
var dp_Residency_page=1;
var dp_Residency_sortOrderStr="";
var dp_Residency_sortOrderType="";

var dp_Involvement_Rows=10;
var dp_Involvement_page=1;
var dp_Involvement_sortOrderStr="";
var dp_Involvement_sortOrderType="";

var dp_TchrLang_Rows=10;
var dp_TchrLang_page=1;
var dp_TchrLang_sortOrderStr="";
var dp_TchrLang_sortOrderType="";
var txtBgColor="#F5E7E1";

function getPaging(pageno)
{
	var gridNameFlag = document.getElementById("gridNameFlag").value;
	
	if(gridNameFlag=="subjectAreas"){
		if(pageno!='')
		{
			dp_SubjectArea_page=pageno;	
		}
		else
		{
			dp_SubjectArea_page=1;
		}
		dp_SubjectArea_noOfRows = document.getElementById("pageSize").value;
		showGridSubjectAreasGrid();
	}
	else if(gridNameFlag=="additionalDocuments"){
		if(pageno!='')
		{
			dp_AdditionalDocuments_page=pageno;	
		}
		else
		{
			dp_AdditionalDocuments_page=1;
		}
		dp_AdditionalDocuments_noOfRows = document.getElementById("pageSize").value;
		showGridAdditionalDocuments();
	}else if(gridNameFlag=="academics"){
		if(pageno!='')
		{
			dp_Academics_page=pageno;	
		}
		else
		{
			dp_Academics_page=1;
		}
		dp_Academics_noOfRows = document.getElementById("pageSize").value;
		showGridAcademics();
	}else if(gridNameFlag=="certification"){
		if(pageno!='')
		{
			dp_Certification_page=pageno;	
		}
		else
		{
			dp_Certification_page=1;
		}
		dp_Certification_noOfRows = document.getElementById("pageSize3").value;
		showGridCredentials();
	}else if(gridNameFlag=="reference"){
		if(pageno!='')
		{
			dp_Reference_page=pageno;	
		}
		else
		{
			dp_Reference_page=1;
		}
		dp_Reference_noOfRows = document.getElementById("pageSize").value;
		getElectronicReferencesGrid();
	}else if(gridNameFlag=="workExperience"){
		if(pageno!='')
		{
			dp_Employment_page=pageno;	
		}
		else
		{
			dp_Employment_page=1;
		}
		dp_Employment_noOfRows = document.getElementById("pageSize").value;
		getPFEmploymentDataGrid();
	}else if(gridNameFlag=="videolinks"){
		if(pageno!='')
		{
			dp_VideoLink_page=pageno;	
		}
		else
		{
			dp_VideoLink_page=1;
		}
		dp_VideoLink_Rows = document.getElementById("pageSize").value;
		getVideoLinksGrid();
	}else if(gridNameFlag=="stdTchrGrid"){
		if(pageno!='')
		{
			dp_StdTchrExp_page=pageno;	
		}
		else
		{
			dp_StdTchrExp_page=1;
		}
		dp_StdTchrExp_Rows = document.getElementById("pageSize").value;
		displayStdTchrExp();
	}
	else if(gridNameFlag=="involvement"){
		if(pageno!='')
		{
			dp_Involvement_page=pageno;	
		}
		else
		{
			dp_Involvement_page=1;
		}
		dp_Involvement_Rows = document.getElementById("pageSize").value;
		getInvolvementGrid();
	}else if(gridNameFlag=="language"){
		if(pageno!='')
		{
			dp_TchrLang_page=pageno;	
		}
		else
		{
			dp_TchrLang_page=1;
		}
		dp_TchrLang_Rows = document.getElementById("pageSize").value;
		displayTchrLanguage();
	}else if(gridNameFlag=="residency"){
		if(pageno!='')
		{
			dp_Residency_page=pageno;	
		}
		else
		{
			dp_Residency_page=1;
		}
		dp_Residency_Rows = document.getElementById("residencyGridPager").value;
		getResidencyGrid();
	}
	
	//videolinks
	
}

function getPagingAndSortingSSPF(pageno,sortOrder,sortOrderTyp)
{
	var gridNameFlag = document.getElementById("gridNameFlag").value;
	if(gridNameFlag=="subjectAreas")
	{
		if(pageno!=''){
			dp_SubjectArea_page=pageno;	
		}else{
			dp_SubjectArea_page=1;
		}
		dp_SubjectArea_sortOrderStr	=	sortOrder;
		dp_SubjectArea_sortOrderType	=	sortOrderTyp;
		if(document.getElementById("pageSize1")!=null){
			dp_SubjectArea_noOfRows = document.getElementById("pageSize1").value;
		}else{
			dp_SubjectArea_noOfRows=10;
		}
		showGridSubjectAreasGrid();
	}
	else if(gridNameFlag=="additionalDocuments"){
		if(pageno!=''){
			dp_AdditionalDocuments_page=pageno;	
		}else{
			dp_AdditionalDocuments_page=1;
		}
		dp_AdditionalDocuments_sortOrderStr	=	sortOrder;
		dp_AdditionalDocuments_sortOrderType	=	sortOrderTyp;
		if(document.getElementById("pageSize1")!=null){
			dp_AdditionalDocuments_noOfRows = document.getElementById("pageSize1").value;
		}else{
			dp_AdditionalDocuments_noOfRows=10;
		}
		showGridAdditionalDocuments();
	}else if(gridNameFlag=="academics"){
		if(pageno!=''){
			dp_Academics_page=pageno;	
		}else{
			dp_Academics_page=1;
		}
		dp_Academics_sortOrderStr	=	sortOrder;
		dp_Academics_sortOrderType	=	sortOrderTyp;
		if(document.getElementById("pageSize1")!=null){
			dp_Academics_noOfRows = document.getElementById("pageSize1").value;
		}else{
			dp_Academics_noOfRows=10;
		}
		showGridAcademics();
	}else if(gridNameFlag=="certification"){
		if(pageno!=''){
			dp_Certification_page=pageno;	
		}else{
			dp_Certification_page=1;
		}
		dp_Certification_sortOrderStr	=	sortOrder;
		dp_Certification_sortOrderType	=	sortOrderTyp;
		if(document.getElementById("pageSize3")!=null){
			dp_Certification_noOfRows = document.getElementById("pageSize3").value;
		}else{
			dp_Certification_noOfRows=10;
		}
		showGridCredentials();
	}else if(gridNameFlag=="reference"){
		if(pageno!=''){
			dp_Reference_page=pageno;	
		}else{
			dp_Reference_page=1;
		}
		dp_Reference_sortOrderStr	=	sortOrder;
		dp_Reference_sortOrderType	=	sortOrderTyp;
		if(document.getElementById("pageSize1")!=null){
			dp_Reference_noOfRows = document.getElementById("pageSize1").value;
		}else{
			dp_Reference_noOfRows=10;
		}
		getElectronicReferencesGrid();
	}else if(gridNameFlag=="workExperience"){
		if(pageno!=''){
			dp_Employment_page=pageno;	
		}else{
			dp_Employment_page=1;
		}
		dp_Employment_sortOrderStr	=	sortOrder;
		dp_Employment_sortOrderType	=	sortOrderTyp;
		if(document.getElementById("pageSize1")!=null){
			dp_Employment_noOfRows = document.getElementById("pageSize1").value;
		}else{
			dp_Employment_noOfRows=10;
		}
		getPFEmploymentDataGrid();
	}else if(gridNameFlag=="videolinks"){

		if(pageno!=''){
			dp_VideoLink_page=pageno;	
		}else{
			dp_VideoLink_page=1;
		}
		dp_VideoLink_sortOrderStr	=	sortOrder;
		dp_VideoLink_sortOrderType	=	sortOrderTyp;
		if(document.getElementById("pageSize1")!=null){
			dp_VideoLink_Rows = document.getElementById("pageSize1").value;
		}else{
			dp_VideoLink_Rows=10;
		}
		getVideoLinksGrid();
	
	}else if(gridNameFlag=="stdTchrGrid"){

		if(pageno!=''){
			dp_StdTchrExp_page=pageno;	
		}else{
			dp_StdTchrExp_page=1;
		}
		dp_StdTchrExp_sortOrderStr	=	sortOrder;
		dp_StdTchrExp_sortOrderType	=	sortOrderTyp;
		if(document.getElementById("pageSize1")!=null){
			dp_StdTchrExp_Rows = document.getElementById("pageSize1").value;
		}else{
			dp_StdTchrExp_Rows=10;
		}
		displayStdTchrExp();
	
	}
	else if(gridNameFlag=="involvement"){
		if(pageno!=''){
			dp_Involvement_page=pageno;	
		}else{
			dp_Involvement_page=1;
		}
		dp_Involvement_sortOrderStr	=	sortOrder;
		dp_Involvement_sortOrderType	=	sortOrderTyp;
		if(document.getElementById("pageSize1")!=null){
			dp_Involvement_Rows = document.getElementById("pageSize1").value;
		}else{
			dp_Involvement_Rows=10;
		}
		getInvolvementGrid();	
	}else if(gridNameFlag=="language"){

		if(pageno!=''){
			dp_TchrLang_page=pageno;	
		}else{
			dp_TchrLang_page=1;
		}
		dp_TchrLang_sortOrderStr	=	sortOrder;
		dp_TchrLang_sortOrderType	=	sortOrderTyp;
		if(document.getElementById("pageSize1")!=null){
			dp_TchrLang_Rows = document.getElementById("pageSize1").value;
		}else{
			dp_TchrLang_Rows=10;
		}
		displayTchrLanguage();
	
	}else if(gridNameFlag=="residency"){

		if(pageno!=''){
			dp_Residency_page=pageno;	
		}else{
			dp_Residency_page=1;
		}
		dp_Residency_sortOrderStr	=	sortOrder;
		dp_Residency_sortOrderType	=	sortOrderTyp;
		if(document.getElementById("residencyGridPager")!=null){
			dp_Residency_Rows = document.getElementById("residencyGridPager").value;
		}else{
			dp_Residency_Rows=10;
		}
		getResidencyGrid();
	
	}
}

/*function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
	if(pageno!=''){
		dynamicPortfolio_Academics_page=pageno;	
	}else{
		dynamicPortfolio_Academics_page=1;
	}
	dp_Academics_sortOrderStr=sortOrder;
	dp_Academics_sortOrderType=sortOrderTyp;
	if(document.getElementById("pageSize")!=null){
		dp_Academics_noOfRows = document.getElementById("pageSize").value;
	}else{
		dp_Academics_noOfRows=100;
	}
	showGridAcademics();
}*/

/*========= End ... Paging and Sorting ===========*/

function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert("Server Error: "+exception.javaClassName);}
}
function trim(s)
{
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}
function isNumber(field) {
	var re = /^[0-9-'.'-',']*$/;
	if (!re.test(field.value)) {
		field.value = field.value.replace(/[^0-9-'.'-',']/g,"");
	}
}
function checkForInt(evt) {

	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==8)
		return true;

	return( (charCode >= 48 && charCode <= 57)||(charCode==13) );
}

function checkForDecimalTwo(evt) {

	var count=true;
	var str=evt.srcElement.value;
	if (str.lastIndexOf('.') == -1) str += ".";
	var decNum = str.substring(str.lastIndexOf('.')+1, str.length);
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==8)
		return true;

	var parts = evt.srcElement.value.split('.');
    if(parts.length > 1 && charCode==46)
        return false;
    
	return( ((charCode >= 48 || charCode==46)&& count && charCode <= 57)||(charCode==13) );
}

function setFocus(id)
{
	if(id!="")
		document.getElementById(id).focus();
}

function showForm(divFormGrid)
{
	document.getElementById(divFormGrid).style.display="block";
}

function setGridNameFlag(gridName)
{
	document.getElementById("gridNameFlag").value=gridName;
}

function isEmailAddress(str) 
{
	var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
	return emailPattern.test(str);
}

function applyScrollOnTbl_EmployeementHistory()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#employeementGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 350,
        width: 940,
        minWidth: null,
        minWidthAuto: false,
          colratio:[200,200,225,110,140,105],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}

function applyScrollOnTbl_Academic()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#academicGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 380,
        width: 940,
        minWidth: null,
        minWidthAuto: false,
        colratio:[185,130,155,144,136,125,100], //144,130,136,185,136,146,97
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
        });			
}

/*function applyScrollOnTbl_AdditionalDocuments()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#additionalDocumentsGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 380,
        width: 940,
        minWidth: null,
        minWidthAuto: false,
        colratio:[440,416,100],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
        });			
}*/

function applyScrollOnTbl_Certifications()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblGridCertifications').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 380,
        width: 940,
        minWidth: null,
        minWidthAuto: false,
        colratio:[255,100,115,200,205,100], //125,90,400,120,80
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}

function applyScrollOnTblEleRef()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#eleReferencesGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 350,
        width: 940,
        minWidth: null,
        minWidthAuto: false,
        colratio:[170,90,130,170,120,117,80,80], //170,100,120,180,80,70,110
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 70,
        wrapper: false
        });
            
        });			
}


function applyScrollOnTbl_AdditionalDocuments()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#additionalDocumentsGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 380,
        width: 940,
        minWidth: null,
        minWidthAuto: false,
        colratio:[440,436,100],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
        });			
}


function applyScrollOnTbl_SubjectAreas()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#subjectAreasGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 380,
        width: 940,
        minWidth: null,
        minWidthAuto: false,
        colratio:[200,200,300,157,100],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
        });			
}

function applyScrollOnTbl_CertificationsNoble()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblGridCertifications').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 380,
        width: 940,
        minWidth: null,
        minWidthAuto: false,
        colratio:[300,560,100], //125,90,400,120,80
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}
function applyScrollOnTblVideoLinks()
{
	//alert("Hi")
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#videoLinktblGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 350,
        width: 940,
        minWidth: null,
        minWidthAuto: false,
          colratio:[450,300,120,130],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 70,
        wrapper: false
        });
            
        });			
}
function applyScrollOnTblStdTchr()
{
	//alert("Hi")
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#StdTchrExpLinktblGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 350,
        width: 940,
        minWidth: null,
        minWidthAuto: false,
          colratio:[250,200,200,200,112],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 70,
        wrapper: false
        });
            
        });			
}
function applyScrollOnInvl()
{
		var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#involvementGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 350,
        width: 940,
        minWidth: null,
        minWidthAuto: false,
        colratio:[250,250,200,176,111], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });            
        });
}
function applyScrollOnHonors()
{
		var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#honorsGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 350,
        width: 940,
        minWidth: null,
        minWidthAuto: false,
        colratio:[475,400,100], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });            
        });
}
function applyScrollOnTblteacherLang()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#teacherLanguageGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 350,
        width: 940,
        minWidth: null,
        minWidthAuto: false,
          colratio:[450,225,200,112],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 70,
        wrapper: false
        });
            
        });		
}

function applyScrollOnTblResidencyDiv()
{
	//alert("Hi")
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tchrResidencytblGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 350,
        width: 940,
        minWidth: null,
        minWidthAuto: false,
          colratio:[209,120,120,110,128,168,105],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 70,
        wrapper: false
        });
            
        });			
}

function applyScrollOnTblLicense()
{	
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#divLicneseGridCertifications').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 380,
        width: 940,
        minWidth: null,
        minWidthAuto: false,
       	colratio:[250,200,290,215],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
    });			
}
