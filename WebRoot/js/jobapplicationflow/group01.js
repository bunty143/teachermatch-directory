var iListenerCounter=0;
function createXMLHttpRequest()
{
	if (window.ActiveXObject)
		xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
	else if (window.XMLHttpRequest)
		xmlHttp = new XMLHttpRequest();
	else
		alert("AJAX NOT SUPPORTED BY YOUR BROWSER");
}

function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert(resourceJSON.msgServerErr+": "+exception.javaClassName);}
}

function trim(s)
{
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}

function checkForInt(evt) {

	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==8)
		return true;

	return( (charCode >= 48 && charCode <= 57)||(charCode==13) );
}

function checkForDecimalTwo(evt) {

	var count=true;
	var str=evt.srcElement.value;
	if (str.lastIndexOf('.') == -1) str += ".";
	var decNum = str.substring(str.lastIndexOf('.')+1, str.length);
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==8)
		return true;

	var parts = evt.srcElement.value.split('.');
    if(parts.length > 1 && charCode==46)
        return false;
    
	return( ((charCode >= 48 || charCode==46)&& count && charCode <= 57)||(charCode==13) );
}

function setFocus(inputControlId)
{
	if(inputControlId!="")
	{
		document.getElementById(inputControlId).focus();
	}
}

function getPortfolioHeader(jobId,dspqPortfolioNameId,candidateType,groupId)
{
	$('#jafLoadingDivL2').show();
	DSPQServiceAjax.getPortfolioHeader(jobId,dspqPortfolioNameId,candidateType,groupId,{ 
	async: false,
	callback: function(data)
	{
		document.getElementById("portfolioHeader").innerHTML=data;
		$('#jafLoadingDivL2').hide();
	},
	errorHandler:handleError  
	});
}

function getGroup01(jobId,dspqPortfolioNameId,candidateType,groupId)
{
	$('#loadingDiv').show();
	DSPQServiceAjax.getGroup01(jobId,dspqPortfolioNameId,candidateType,groupId,{ 
		async: false,
		callback: function(data){
		var dwr_response = data.split("|||");
		if(dwr_response.length==3)
		{
			if(dwr_response[0]!=null && dwr_response[0]!="")
			{
				document.getElementById("group01").innerHTML=dwr_response[0];
				var tooltips=dwr_response[1];
				var tooltipsArr = tooltips.split(",");
				
				//alert("tooltipsArr.length "+tooltipsArr.length);
				
				for(var i=1; i<tooltipsArr.length; i++)
				{
					if(tooltipsArr[i]!='')
					{
						//alert("tooltipsArr[i] "+tooltipsArr[i]);
						try {$('#'+tooltipsArr[i]).tooltip();} catch (e) {}
					}
				}
				
				var calDates=dwr_response[2];
				var calDatesArr = calDates.split(",");
				for(var i=1; i<calDatesArr.length; i++)
				{
					var calDateInputName=calDatesArr[i];
					if(calDateInputName!='')
					{
						try {cal.manageFields(calDateInputName, calDateInputName, "%m-%d-%Y");} catch (e) {}
					}
				}
				
				getHidePreviousBtn();
				showLanguageGrid();
				
				
				//Call Jeffco API for fetching Runtime data
				var dspqjobflowdistrictId=0;
				try { dspqjobflowdistrictId=document.getElementById("dspqjobflowdistrictId").value; } catch (e) {}
				if(dspqjobflowdistrictId=="804800")
				{
					openJeffcoEmployeeNumberInfo();
				}
				
				//Call Veteran for OSCEOLA District 
				if(dspqjobflowdistrictId==1201470){
					if( $("#vt1").is(':checked'))
						showdistrictspeciFicVeteran();
				}
			}
			
			//displayTchrLanguage();
			
			$('#loadingDiv').hide();
		}
		//try {$('#iconpophoverSolutation').tooltip();} catch (e) {}
	},
	errorHandler:handleError  
	});
}

function getGroup02(jobId,dspqPortfolioNameId,candidateType,groupId)
{
	$('#loadingDiv').show();
	DSPQServiceAjax.getGroup02(jobId,dspqPortfolioNameId,candidateType,groupId,{ 
		async: false,
		callback: function(data){
		var dwr_response = data.split("|||");
		if(dwr_response.length==2)
		{
			if(dwr_response[0]!=null && dwr_response[0]!="")
			{
				document.getElementById("group02").innerHTML=dwr_response[0];
				var tooltips=dwr_response[1];
				var tooltipsArr = tooltips.split(",");
				
				for(var i=1; i<tooltipsArr.length; i++)
				{
					if(tooltipsArr[i]!='')
					{
						try {$('#'+tooltipsArr[i]).tooltip();} catch (e) {}
					}
				}
				
				getHidePreviousBtn();
			}
			
			
			
			$('#loadingDiv').hide();
		}
		//try {$('#iconpophoverSolutation').tooltip();} catch (e) {}
	},
	errorHandler:handleError  
	});
}

function getGroup02_2(jobId,dspqPortfolioNameId,candidateType,groupId)
{
	$('#loadingDiv').show();
	DSPQServiceAjax.getGroup02_2(jobId,dspqPortfolioNameId,candidateType,groupId,{ 
		async: false,
		callback: function(data){
		var dwr_response = data.split("|||");
		if(dwr_response.length==3)
		{
			if(dwr_response[0]!=null && dwr_response[0]!="")
			{
				document.getElementById("group02_2").innerHTML=dwr_response[0];
				var tooltips=dwr_response[1];
				var tooltipsArr = tooltips.split(",");
				
				for(var i=1; i<tooltipsArr.length; i++)
				{
					if(tooltipsArr[i]!='')
					{
						try {$('#'+tooltipsArr[i]).tooltip();} catch (e) {}
					}
				}
				
				var calDates=dwr_response[2];
				var calDatesArr = calDates.split(",");
				for(var i=1; i<calDatesArr.length; i++)
				{
					var calDateInputName=calDatesArr[i];
					if(calDateInputName!='')
					{
						try {cal.manageFields(calDateInputName, calDateInputName, "%m-%d-%Y");} catch (e) {}
					}
				}
				
				showGridAcademics();
			}
			
			
			
			$('#loadingDiv').hide();
			
			
		}
		//try {$('#iconpophoverSolutation').tooltip();} catch (e) {}
	},
	errorHandler:handleError  
	});
}

function getGroup03(jobId,dspqPortfolioNameId,candidateType,groupId)
{
	$('#loadingDiv').show();
	DSPQServiceAjax.getGroup03(jobId,dspqPortfolioNameId,candidateType,groupId,{ 
		async: false,
		callback: function(data){
		var dwr_response = data.split("|||");
		if(dwr_response.length==2)
		{
			if(dwr_response[0]!=null && dwr_response[0]!="")
			{
				document.getElementById("group03").innerHTML=dwr_response[0];
				var tooltips=dwr_response[1];
				var tooltipsArr = tooltips.split(",");
				
				for(var i=1; i<tooltipsArr.length; i++)
				{
					if(tooltipsArr[i]!='')
					{
						try {$('#'+tooltipsArr[i]).tooltip();} catch (e) {}
					}
				}
				
				getHidePreviousBtn();
			}
			
			
			
			$('#loadingDiv').hide();
		}
		//try {$('#iconpophoverSolutation').tooltip();} catch (e) {}
	},
	errorHandler:handleError  
	});
}

function getGroup03_8(jobId,dspqPortfolioNameId,candidateType,groupId)
{
	$('#loadingDiv').show();
	DSPQServiceAjax.getGroup03_8(jobId,dspqPortfolioNameId,candidateType,groupId,{ 
		async: false,
		callback: function(data){
		var dwr_response = data.split("|||");
		if(dwr_response.length==3)
		{
			if(dwr_response[0]!=null && dwr_response[0]!="")
			{
				document.getElementById("group03_8").innerHTML=dwr_response[0];
				var tooltips=dwr_response[1];
				var tooltipsArr = tooltips.split(",");
				
				for(var i=1; i<tooltipsArr.length; i++)
				{
					if(tooltipsArr[i]!='')
					{
						try {$('#'+tooltipsArr[i]).tooltip();} catch (e) {}
					}
				}
				
				var calDates=dwr_response[2];
				var calDatesArr = calDates.split(",");
				for(var i=1; i<calDatesArr.length; i++)
				{
					var calDateInputName=calDatesArr[i];
					if(calDateInputName!='')
					{
						try {cal.manageFields(calDateInputName, calDateInputName, "%m-%d-%Y");} catch (e) {}
					}
				}
				
				showGridCredentials();
				
			}
			
			
			
			$('#loadingDiv').hide();
			
		}
		//try {$('#iconpophoverSolutation').tooltip();} catch (e) {}
	},
	errorHandler:handleError  
	});
}

function getGroup03_9(jobId,dspqPortfolioNameId,candidateType,groupId)
{
	$('#loadingDiv').show();
	DSPQServiceAjax.getGroup03_9(jobId,dspqPortfolioNameId,candidateType,groupId,{ 
		async: false,
		callback: function(data){
		var dwr_response = data.split("|||");
		if(dwr_response.length==3)
		{
			if(dwr_response[0]!=null && dwr_response[0]!="")
			{
				document.getElementById("group03_9").innerHTML=dwr_response[0];
				var tooltips=dwr_response[1];
				var tooltipsArr = tooltips.split(",");
				//alert("tooltipsArr.length "+tooltipsArr.length);
				for(var i=1; i<tooltipsArr.length; i++)
				{
					if(tooltipsArr[i]!='')
					{
						try {$('#'+tooltipsArr[i]).tooltip();} catch (e) {}
					}
				}
				
				var calDates=dwr_response[2];
				var calDatesArr = calDates.split(",");
				for(var i=1; i<calDatesArr.length; i++)
				{
					var calDateInputName=calDatesArr[i];
					if(calDateInputName!='')
					{
						try {cal.manageFields(calDateInputName, calDateInputName, "%m-%d-%Y");} catch (e) {}
					}
				}
				
				getElectronicReferencesGrid();
			}
			
			
			$('#loadingDiv').hide();

		}
		//try {$('#iconpophoverSolutation').tooltip();} catch (e) {}
	},
	errorHandler:handleError  
	});
}

function getGroup03_10(jobId,dspqPortfolioNameId,candidateType,groupId)
{
	$('#loadingDiv').show();
	DSPQServiceAjax.getGroup03_10(jobId,dspqPortfolioNameId,candidateType,groupId,{ 
		async: false,
		callback: function(data){
		var dwr_response = data.split("|||");
		if(dwr_response.length==3)
		{
			if(dwr_response[0]!=null && dwr_response[0]!="")
			{
				document.getElementById("group03_10").innerHTML=dwr_response[0];
				var tooltips=dwr_response[1];
				var tooltipsArr = tooltips.split(",");
				//alert("tooltipsArr.length "+tooltipsArr.length);
				for(var i=1; i<tooltipsArr.length; i++)
				{
					if(tooltipsArr[i]!='')
					{
						try {$('#'+tooltipsArr[i]).tooltip();} catch (e) {}
					}
				}
				
				var calDates=dwr_response[2];
				var calDatesArr = calDates.split(",");
				for(var i=1; i<calDatesArr.length; i++)
				{
					var calDateInputName=calDatesArr[i];
					if(calDateInputName!='')
					{
						try {cal.manageFields(calDateInputName, calDateInputName, "%m-%d-%Y");} catch (e) {}
					}
				}
				
				getVideoLinksGrid();
			}
			
			
			$('#loadingDiv').hide();

		}
		//try {$('#iconpophoverSolutation').tooltip();} catch (e) {}
	},
	errorHandler:handleError  
	});
}

function getGroup03_11(jobId,dspqPortfolioNameId,candidateType,groupId)
{
	$('#loadingDiv').show();
	DSPQServiceAjax.getGroup03_11(jobId,dspqPortfolioNameId,candidateType,groupId,{ 
		async: false,
		callback: function(data){
		var dwr_response = data.split("|||");
		if(dwr_response.length==3)
		{
			if(dwr_response[0]!=null && dwr_response[0]!="")
			{
				document.getElementById("group03_11").innerHTML=dwr_response[0];
				var tooltips=dwr_response[1];
				var tooltipsArr = tooltips.split(",");
				//alert("tooltipsArr.length "+tooltipsArr.length);
				for(var i=1; i<tooltipsArr.length; i++)
				{
					if(tooltipsArr[i]!='')
					{
						try {$('#'+tooltipsArr[i]).tooltip();} catch (e) {}
					}
				}
				
				var calDates=dwr_response[2];
				var calDatesArr = calDates.split(",");
				for(var i=1; i<calDatesArr.length; i++)
				{
					var calDateInputName=calDatesArr[i];
					if(calDateInputName!='')
					{
						try {cal.manageFields(calDateInputName, calDateInputName, "%m-%d-%Y");} catch (e) {}
					}
				}
				
				showGridAdditionalDocuments();
			}
			
			
			$('#loadingDiv').hide();

		}
		//try {$('#iconpophoverSolutation').tooltip();} catch (e) {}
	},
	errorHandler:handleError  
	});
}

function getGroup03_21(jobId,dspqPortfolioNameId,candidateType,groupId)
{
	$('#loadingDiv').show();
	DSPQServiceAjax.getGroup03_21(jobId,dspqPortfolioNameId,candidateType,groupId,{ 
		async: false,
		callback: function(data){
		var dwr_response = data.split("|||");
		if(dwr_response.length==3)
		{
			if(dwr_response[0]!=null && dwr_response[0]!="")
			{
				document.getElementById("group03_21").innerHTML=dwr_response[0];
				var tooltips=dwr_response[1];
				var tooltipsArr = tooltips.split(",");
				//alert("tooltipsArr.length "+tooltipsArr.length);
				for(var i=1; i<tooltipsArr.length; i++)
				{
					if(tooltipsArr[i]!='')
					{
						try {$('#'+tooltipsArr[i]).tooltip();} catch (e) {}
					}
				}
				
				var calDates=dwr_response[2];
				var calDatesArr = calDates.split(",");
				for(var i=1; i<calDatesArr.length; i++)
				{
					var calDateInputName=calDatesArr[i];
					if(calDateInputName!='')
					{
						try {cal.manageFields(calDateInputName, calDateInputName, "%m-%d-%Y");} catch (e) {}
					}
				}
				var InputCtrlId_nationalBoardCert="nationalBoardCert";
				var nationalBoardCert=getInputCtrlRadioValue(InputCtrlId_nationalBoardCert);
				chkNBCert(nationalBoardCert);
				
				hideTFAFieldsNJAF();
				
				var InputCtrlId_isNonTeacher="isNonTeacher";
				var isNonTeacher=getInputCtrlRadioValue(InputCtrlId_isNonTeacher);
				JAFchkNonTeacher(isNonTeacher);
				
			}
			
			
			
			$('#loadingDiv').hide();
		}
		//try {$('#iconpophoverSolutation').tooltip();} catch (e) {}
	},
	errorHandler:handleError  
	});
}

function getGroup03_27(jobId,dspqPortfolioNameId,candidateType,groupId)
{
	$('#loadingDiv').show();
	DSPQServiceAjax.getGroup03_27(jobId,dspqPortfolioNameId,candidateType,groupId,{ 
		async: false,
		callback: function(data){
		var dwr_response = data.split("|||");
		if(dwr_response.length==3)
		{
			if(dwr_response[0]!=null && dwr_response[0]!="")
			{
				document.getElementById("group03_27").innerHTML=dwr_response[0];
				var tooltips=dwr_response[1];
				var tooltipsArr = tooltips.split(",");
				for(var i=1; i<tooltipsArr.length; i++)
				{
					if(tooltipsArr[i]!='')
					{
						try {$('#'+tooltipsArr[i]).tooltip();} catch (e) {}
					}
				}
				
				var calDates=dwr_response[2];
				var calDatesArr = calDates.split(",");
				for(var i=1; i<calDatesArr.length; i++)
				{
					var calDateInputName=calDatesArr[i];
					if(calDateInputName!='')
					{
						try {cal.manageFields(calDateInputName, calDateInputName, "%m-%d-%Y");} catch (e) {}
					}
				}
				var InputCtrlId_nationalBoardCert="nationalBoardCert";
				var nationalBoardCert=getInputCtrlRadioValue(InputCtrlId_nationalBoardCert);
				chkNBCert(nationalBoardCert);
				
				hideTFAFieldsNJAF();
				
				var InputCtrlId_isNonTeacher="isNonTeacher";
				var isNonTeacher=getInputCtrlRadioValue(InputCtrlId_isNonTeacher);
				JAFchkNonTeacher(isNonTeacher);
				
			}
			$('#loadingDiv').hide();
		}
	},
	errorHandler:handleError  
	});
}
function getGroup03_28(jobId,dspqPortfolioNameId,candidateType,groupId)
{
	$('#loadingDiv').show();
	DSPQServiceAjax.getGroup03_28(jobId,dspqPortfolioNameId,candidateType,groupId,{ 
		async: false,
		callback: function(data){
		var dwr_response = data.split("|||");
		if(dwr_response.length==3)
		{
			if(dwr_response[0]!=null && dwr_response[0]!="")
			{
				document.getElementById("group03_28").innerHTML=dwr_response[0];
				var tooltips=dwr_response[1];
				var tooltipsArr = tooltips.split(",");
				
				for(var i=1; i<tooltipsArr.length; i++)
				{
					if(tooltipsArr[i]!='')
					{
						try {$('#'+tooltipsArr[i]).tooltip();} catch (e) {}
					}
				}
				
				var calDates=dwr_response[2];
				var calDatesArr = calDates.split(",");
				for(var i=1; i<calDatesArr.length; i++)
				{
					var calDateInputName=calDatesArr[i];
					if(calDateInputName!='')
					{
						try {cal.manageFields(calDateInputName, calDateInputName, "%m-%d-%Y");} catch (e) {}
					}
				}
				
				showGridSubjectAreasGrid();
				
			}
			
			
			
			$('#loadingDiv').hide();
			
		}
		//try {$('#iconpophoverSolutation').tooltip();} catch (e) {}
	},
	errorHandler:handleError  
	});
}
function getGroup04(jobId,dspqPortfolioNameId,candidateType,groupId)
{
	$('#loadingDiv').show();
	DSPQServiceAjax.getGroup04(jobId,dspqPortfolioNameId,candidateType,groupId,{ 
		async: false,
		callback: function(data){
		var dwr_response = data.split("|||");
		if(dwr_response.length==2)
		{
			if(dwr_response[0]!=null && dwr_response[0]!="")
			{
				document.getElementById("group04").innerHTML=dwr_response[0];
				var tooltips=dwr_response[1];
				var tooltipsArr = tooltips.split(",");
				
				for(var i=1; i<tooltipsArr.length; i++)
				{
					if(tooltipsArr[i]!='')
					{
						try {$('#'+tooltipsArr[i]).tooltip();} catch (e) {}
					}
				}
				
				getHidePreviousBtn();
			}
			
			
			
			$('#loadingDiv').hide();
		}
		//try {$('#iconpophoverSolutation').tooltip();} catch (e) {}
	},
	errorHandler:handleError  
	});
}


function getGroup04Section25Name(jobId,dspqPortfolioNameId,candidateType,groupId)
{
	$('#loadingDiv').show();
	DSPQServiceAjax.getGroup04Section25Name(jobId,dspqPortfolioNameId,candidateType,groupId,{ 
		async: false,
		callback: function(data){
		var dwr_response = data.split("|||");
		if(dwr_response.length==2)
		{
			if(dwr_response[0]!=null && dwr_response[0]!="")
			{
				document.getElementById("group04Section25Name").innerHTML=dwr_response[0];
				var tooltips=dwr_response[1];
				var tooltipsArr = tooltips.split(",");
				
				for(var i=1; i<tooltipsArr.length; i++)
				{
					if(tooltipsArr[i]!='')
					{
						try {$('#'+tooltipsArr[i]).tooltip();} catch (e) {}
					}
				}
			}
			
			
			
			//getHidePreviousBtn();
			
			$('#loadingDiv').hide();
		}
		//try {$('#iconpophoverSolutation').tooltip();} catch (e) {}
	},
	errorHandler:handleError  
	});
}

function getGroup04Section25(jobId,dspqPortfolioNameId,candidateType,groupId)
{
	$('#loadingDiv').show();
	DSPQServiceAjax.getGroup04Section25(jobId,dspqPortfolioNameId,candidateType,groupId,{ 
		async: false,
		callback: function(data){
		var dwr_response = data.split("|||");
		if(dwr_response.length==2)
		{
			if(dwr_response[0]!=null && dwr_response[0]!="")
			{
				document.getElementById("group04Section25").innerHTML=dwr_response[0];
				var tooltips=dwr_response[1];
				var tooltipsArr = tooltips.split(",");
				
				for(var i=1; i<tooltipsArr.length; i++)
				{
					if(tooltipsArr[i]!='')
					{
						try {$('#'+tooltipsArr[i]).tooltip();} catch (e) {}
					}
				}
				
				getHidePreviousBtn();
			}
			
			
			
			$('#loadingDiv').hide();
		}
		//try {$('#iconpophoverSolutation').tooltip();} catch (e) {}
	},
	errorHandler:handleError  
	});
}







var isSetFocus=false;
function validateDSPQPersonalGroup(inputValue)
{
	//alert("validateDSPQPersonalGroup 1");
	$('#jafLoadingDiv').show();
	isSetFocus=false;
	var jobId=""; 
	try { jobId=document.getElementById("dspqJobId").value; } catch (e) {}

	var dspqct=""; 
	try { dspqct=document.getElementById("dspqct").value; } catch (e) {}
	
	var dspqjobflowdistrictId="";
	try { dspqjobflowdistrictId=document.getElementById("dspqjobflowdistrictId").value; } catch (e) {}
	
	var dspqPortfolioNameId="";
	try { dspqPortfolioNameId=document.getElementById("dspqPortfolioNameId").value; } catch (e) {}
	
	var iReturnValidateTotalValue=0;
	var iReturnValidateValue=0;
	
	var PI_Section=0;
	var PI_Section_Name="tooltipSection02";
	
	var InputCtrlId_salutation_pi="salutation_pi";
	var salutation_pi=getInputCtrlTextValue(InputCtrlId_salutation_pi);
	iReturnValidateValue=validateInputListControlStr(salutation_pi,InputCtrlId_salutation_pi,"lblFieldId_"+InputCtrlId_salutation_pi,"lblFieldIdWarning_"+InputCtrlId_salutation_pi);
	if(iReturnValidateValue=="1")
	{
		PI_Section=PI_Section+1;
		iReturnValidateTotalValue=iReturnValidateTotalValue+PI_Section;
	}
	
	var InputCtrlId_firstName_pi="firstName_pi";
	var firstName_pi=getInputCtrlTextValue(InputCtrlId_firstName_pi);
	iReturnValidateValue=validateInputTextControlStr(firstName_pi,InputCtrlId_firstName_pi,"lblFieldId_"+InputCtrlId_firstName_pi,"lblFieldIdWarning_"+InputCtrlId_firstName_pi);
	if(iReturnValidateValue=="1")
	{
		PI_Section=PI_Section+1;
		iReturnValidateTotalValue=iReturnValidateTotalValue+PI_Section;
	}
	
	var InputCtrlId_middleName_pi="middleName_pi";
	var middleName_pi=getInputCtrlTextValue(InputCtrlId_middleName_pi);
	iReturnValidateValue=validateInputTextControlStr(middleName_pi,InputCtrlId_middleName_pi,"lblFieldId_"+InputCtrlId_middleName_pi,"lblFieldIdWarning_"+InputCtrlId_middleName_pi);
	if(iReturnValidateValue=="1")
	{
		PI_Section=PI_Section+1;
		iReturnValidateTotalValue=iReturnValidateTotalValue+PI_Section;
	}
	
	var InputCtrlId_lastName_pi="lastName_pi";
	var lastName_pi=getInputCtrlTextValue(InputCtrlId_lastName_pi);
	iReturnValidateValue=validateInputTextControlStr(lastName_pi,InputCtrlId_lastName_pi,"lblFieldId_"+InputCtrlId_lastName_pi,"lblFieldIdWarning_"+InputCtrlId_lastName_pi);
	if(iReturnValidateValue=="1")
	{
		PI_Section=PI_Section+1;
		iReturnValidateTotalValue=iReturnValidateTotalValue+PI_Section;
	}
	
	var InputCtrlId_anotherName="anotherName";
	var anotherName=getInputCtrlTextValue(InputCtrlId_anotherName);
	iReturnValidateValue=validateInputTextControlStr(anotherName,InputCtrlId_anotherName,"lblFieldId_"+InputCtrlId_anotherName,"lblFieldIdWarning_"+InputCtrlId_anotherName);
	if(iReturnValidateValue=="1")
	{
		PI_Section=PI_Section+1;
		iReturnValidateTotalValue=iReturnValidateTotalValue+PI_Section;
	}
	
	var InputCtrlId_ssn_pi="ssn_pi";
	var ssn_pi=getInputCtrlTextValue(InputCtrlId_ssn_pi);
	iReturnValidateValue=validateInputTextControlStr(ssn_pi,InputCtrlId_ssn_pi,"lblFieldId_"+InputCtrlId_ssn_pi,"lblFieldIdWarning_"+InputCtrlId_ssn_pi);
	if(iReturnValidateValue=="1")
	{
		PI_Section=PI_Section+1;
		iReturnValidateTotalValue=iReturnValidateTotalValue+PI_Section;
	}
	
	var InputCtrlId_expectedSalary="expectedSalary";
	var expectedSalary=getInputCtrlTextValue(InputCtrlId_expectedSalary);
	iReturnValidateValue=validateInputTextControlStr(expectedSalary,InputCtrlId_expectedSalary,"lblFieldId_"+InputCtrlId_expectedSalary,"lblFieldIdWarning_"+InputCtrlId_expectedSalary);
	if(iReturnValidateValue=="1")
	{
		PI_Section=PI_Section+1;
		iReturnValidateTotalValue=iReturnValidateTotalValue+PI_Section;
	}
	
	var InputCtrlId_phoneNumber="phoneNumber";
	
	var InputCtrlId_phoneNumber1="phoneNumber1";
	var phoneNumber1=getInputCtrlTextValue(InputCtrlId_phoneNumber1);
	
	var InputCtrlId_phoneNumber2="phoneNumber2";
	var phoneNumber2=getInputCtrlTextValue(InputCtrlId_phoneNumber2);
	
	var InputCtrlId_phoneNumber3="phoneNumber3";
	var phoneNumber3=getInputCtrlTextValue(InputCtrlId_phoneNumber3);
	
	iReturnValidateValue=validateInputTextPhoneOrMobileControlStr(phoneNumber1,phoneNumber2,phoneNumber3,InputCtrlId_phoneNumber1,InputCtrlId_phoneNumber2,InputCtrlId_phoneNumber3,"lblFieldId_"+InputCtrlId_phoneNumber,"lblFieldIdWarning_"+InputCtrlId_phoneNumber,3,3,4);
	if(iReturnValidateValue=="1")
	{
		PI_Section=PI_Section+1;
		iReturnValidateTotalValue=iReturnValidateTotalValue+PI_Section;
	}
	
	
	var InputCtrlId_mobileNumber="mobileNumber";
	
	var InputCtrlId_mobileNumber1="mobileNumber1";
	var mobileNumber1=getInputCtrlTextValue(InputCtrlId_mobileNumber1);
	
	var InputCtrlId_mobileNumber2="mobileNumber2";
	var mobileNumber2=getInputCtrlTextValue(InputCtrlId_mobileNumber2);
	
	var InputCtrlId_mobileNumber3="mobileNumber3";
	var mobileNumber3=getInputCtrlTextValue(InputCtrlId_mobileNumber3);
	
	iReturnValidateValue=validateInputTextPhoneOrMobileControlStr(mobileNumber1,mobileNumber2,mobileNumber3,InputCtrlId_mobileNumber1,InputCtrlId_mobileNumber2,InputCtrlId_mobileNumber3,"lblFieldId_"+InputCtrlId_mobileNumber,"lblFieldIdWarning_"+InputCtrlId_mobileNumber,3,3,4);
	if(iReturnValidateValue=="1")
	{
		PI_Section=PI_Section+1;
		iReturnValidateTotalValue=iReturnValidateTotalValue+PI_Section;
	}
	
	var PI_Section1=validateCustomAll(PI_Section_Name);
	iReturnValidateTotalValue=iReturnValidateTotalValue+PI_Section1;
	PI_Section=PI_Section+PI_Section1;

	// End ... Custom Fld for PI
	
	var mobileNumber="";
	var phoneNumber="";
	
	if(PI_Section>0)
	{
		window.location.hash = '#lblSectionId_'+PI_Section_Name;
		$("#lblSectionId_"+PI_Section_Name).addClass("DSPQRequired12");
		PI_Section=0;
	}
	else
	{
		$("#lblSectionId_"+PI_Section_Name).removeClass("DSPQRequired12");
		
		mobileNumber=mobileNumber1+"-"+mobileNumber2+"-"+mobileNumber3;
		phoneNumber=phoneNumber1+"-"+phoneNumber2+"-"+phoneNumber3;
	}
	
	var DOB_Section=0;
	var DOB_Section_Name="tooltipSection15";
	
	var InputCtrlId_dobMonth="dobMonth";
	var dobMonth=getInputCtrlTextValue(InputCtrlId_dobMonth);
	iReturnValidateValue=validateInputListControlStr(dobMonth,InputCtrlId_dobMonth,"lblFieldId_"+InputCtrlId_dobMonth,"lblFieldIdWarning_"+InputCtrlId_dobMonth);
	if(iReturnValidateValue=="1")
	{
		DOB_Section=DOB_Section+1;
		iReturnValidateTotalValue=iReturnValidateTotalValue+DOB_Section;
	}
	
	var InputCtrlId_dobDay="dobDay";
	var dobDay=getInputCtrlTextValue(InputCtrlId_dobDay);
	iReturnValidateValue=validateInputListControlStr(dobDay,InputCtrlId_dobDay,"lblFieldId_"+InputCtrlId_dobDay,"lblFieldIdWarning_"+InputCtrlId_dobDay);
	if(iReturnValidateValue=="1")
	{
		DOB_Section=DOB_Section+1;
		iReturnValidateTotalValue=iReturnValidateTotalValue+DOB_Section;
	}
	
	var InputCtrlId_dobYear="dobYear";
	var dobYear=getInputCtrlTextValue(InputCtrlId_dobYear);
	iReturnValidateValue=validateInputTextControlStr(dobYear,InputCtrlId_dobYear,"lblFieldId_"+InputCtrlId_dobYear,"lblFieldIdWarning_"+InputCtrlId_dobYear);
	if(iReturnValidateValue=="1")
	{
		DOB_Section=DOB_Section+1;
		iReturnValidateTotalValue=iReturnValidateTotalValue+DOB_Section;
	}
	
	if(dobYear!='')
	{
		var idobYear = new String(parseInt(trim(dobYear)));
		var currentFullYear = new Date().getFullYear();
		currentFullYear=currentFullYear-1;
		if((dobYear=="") || ( dobYear==0 || idobYear=="NaN") || (idobYear > currentFullYear || idobYear < 1931))
		{
			MarkedLabelAsRed(InputCtrlId_dobYear,"lblFieldId_"+InputCtrlId_dobYear,"lblFieldIdWarning_"+InputCtrlId_dobYear)
			DOB_Section=DOB_Section+1;
			iReturnValidateTotalValue=iReturnValidateTotalValue+DOB_Section;
		}
		else
		{
			UnMarkedLabelAsRed(InputCtrlId_dobYear,"lblFieldId_"+InputCtrlId_dobYear,"lblFieldIdWarning_"+InputCtrlId_dobYear)
		}
	}
	
	var DOB_Section1=validateCustomAll(DOB_Section_Name);
	iReturnValidateTotalValue=iReturnValidateTotalValue+DOB_Section1;
	DOB_Section=DOB_Section+DOB_Section1;
	
	var dob="";
	if(DOB_Section>0)
	{
		window.location.hash = '#lblSectionId_'+DOB_Section_Name;
		$("#lblSectionId_"+DOB_Section_Name).addClass("DSPQRequired12");
		PI_Section=0;
	}
	else
	{
		$("#lblSectionId_"+DOB_Section_Name).removeClass("DSPQRequired12");
		if(dobMonth!=null && dobMonth!="" && dobDay!=null && dobDay!="" && dobYear!=null && dobYear!="")
		{
			dob=trim(dobMonth)+"-"+trim(dobDay)+"-"+trim(dobYear);
		}
	}
	
	
	
	var Add_Section=0;
	var Add_Section_Name="tooltipSection4";
	
	
	var InputCtrlId_addressLine1="addressLine1";
	var addressLine1=getInputCtrlTextValue(InputCtrlId_addressLine1);
	iReturnValidateValue=validateInputTextControlStr(addressLine1,InputCtrlId_addressLine1,"lblFieldId_"+InputCtrlId_addressLine1,"lblFieldIdWarning_"+InputCtrlId_addressLine1);
	if(iReturnValidateValue=="1")
	{
		Add_Section=Add_Section+1;
		iReturnValidateTotalValue=iReturnValidateTotalValue+Add_Section;
	}
	
	var InputCtrlId_addressLine2="addressLine2";
	var addressLine2=getInputCtrlTextValue(InputCtrlId_addressLine2);
	iReturnValidateValue=validateInputTextControlStr(addressLine2,InputCtrlId_addressLine2,"lblFieldId_"+InputCtrlId_addressLine2,"lblFieldIdWarning_"+InputCtrlId_addressLine2);
	if(iReturnValidateValue=="1")
	{
		Add_Section=Add_Section+1;
		iReturnValidateTotalValue=iReturnValidateTotalValue+Add_Section;
	}
	
	var InputCtrlId_countryId="countryId";
	var countryId=getInputCtrlTextValue(InputCtrlId_countryId);
	iReturnValidateValue=validateInputListControlStr(countryId,InputCtrlId_countryId,"lblFieldId_"+InputCtrlId_countryId,"lblFieldIdWarning_"+InputCtrlId_countryId);
	if(iReturnValidateValue=="1")
	{
		Add_Section=Add_Section+1;
		iReturnValidateTotalValue=iReturnValidateTotalValue+Add_Section;
	}
	
	
	var InputCtrlId_zipCode="zipCode";
	var zipCode=getInputCtrlTextValue(InputCtrlId_zipCode);
	iReturnValidateValue=validateInputTextControlStr(zipCode,InputCtrlId_zipCode,"lblFieldId_"+InputCtrlId_zipCode,"lblFieldIdWarning_"+InputCtrlId_zipCode);
	if(iReturnValidateValue=="1")
	{
		Add_Section=Add_Section+1;
		iReturnValidateTotalValue=iReturnValidateTotalValue+Add_Section;
	}
	
	var InputCtrlId_stateIdForDSPQ="stateIdForDSPQ";
	var stateIdForDSPQ=getInputCtrlTextValue(InputCtrlId_stateIdForDSPQ);
	iReturnValidateValue=validateInputListControlStr(stateIdForDSPQ,InputCtrlId_stateIdForDSPQ,"lblFieldId_"+InputCtrlId_stateIdForDSPQ,"lblFieldIdWarning_"+InputCtrlId_stateIdForDSPQ);
	if(iReturnValidateValue=="1")
	{
		Add_Section=Add_Section+1;
		iReturnValidateTotalValue=iReturnValidateTotalValue+Add_Section;
	}
	
	var InputCtrlId_cityIdForDSPQ="cityIdForDSPQ";
	var cityIdForDSPQ=getInputCtrlTextValue(InputCtrlId_cityIdForDSPQ);
	iReturnValidateValue=validateInputListControlStr(cityIdForDSPQ,InputCtrlId_cityIdForDSPQ,"lblFieldId_"+InputCtrlId_cityIdForDSPQ,"lblFieldIdWarning_"+InputCtrlId_cityIdForDSPQ);
	if(iReturnValidateValue=="1")
	{
		Add_Section=Add_Section+1;
		iReturnValidateTotalValue=iReturnValidateTotalValue+Add_Section;
	}
	
	var InputCtrlId_otherState="otherState";
	var otherState=getInputCtrlTextValue(InputCtrlId_otherState);
	iReturnValidateValue=validateInputTextControlStr(otherState,InputCtrlId_otherState,"lblFieldId_"+InputCtrlId_otherState,"lblFieldIdWarning_"+InputCtrlId_otherState);
	if(iReturnValidateValue=="1")
	{
		Add_Section=Add_Section+1;
		iReturnValidateTotalValue=iReturnValidateTotalValue+Add_Section;
	}
	
	var InputCtrlId_otherCity="otherCity";
	var otherCity=getInputCtrlTextValue(InputCtrlId_otherCity);
	iReturnValidateValue=validateInputTextControlStr(otherCity,InputCtrlId_otherCity,"lblFieldId_"+InputCtrlId_otherCity,"lblFieldIdWarning_"+InputCtrlId_otherCity);
	if(iReturnValidateValue=="1")
	{
		Add_Section=Add_Section+1;
		iReturnValidateTotalValue=iReturnValidateTotalValue+Add_Section;
	}
	
	var Add_Section1=validateCustomAll(Add_Section_Name);
	iReturnValidateTotalValue=iReturnValidateTotalValue+Add_Section1;
	Add_Section=Add_Section+Add_Section1;
	
	if(Add_Section>0)
	{
		window.location.hash = '#lblSectionId_'+Add_Section_Name;
		$("#lblSectionId_"+Add_Section_Name).addClass("DSPQRequired12");
		Add_Section=0;
	}
	else
	{
		$("#lblSectionId_"+Add_Section_Name).removeClass("DSPQRequired12");
	}
	
	
	
	var PresentAdd_Section=0;
	var PresentAdd_Section_Name="tooltipSection17";
	
	
	var InputCtrlId_addressLinePr="addressLinePr";
	var addressLinePr=getInputCtrlTextValue(InputCtrlId_addressLinePr);
	iReturnValidateValue=validateInputTextControlStr(addressLinePr,InputCtrlId_addressLinePr,"lblFieldId_"+InputCtrlId_addressLinePr,"lblFieldIdWarning_"+InputCtrlId_addressLinePr);
	if(iReturnValidateValue=="1")
	{
		PresentAdd_Section=PresentAdd_Section+1;
		iReturnValidateTotalValue=iReturnValidateTotalValue+PresentAdd_Section;
	}
	
	var InputCtrlId_addressLine2Pr="addressLine2Pr";
	var addressLine2Pr=getInputCtrlTextValue(InputCtrlId_addressLine2Pr);
	iReturnValidateValue=validateInputTextControlStr(addressLine2Pr,InputCtrlId_addressLine2Pr,"lblFieldId_"+InputCtrlId_addressLine2Pr,"lblFieldIdWarning_"+InputCtrlId_addressLine2Pr);
	if(iReturnValidateValue=="1")
	{
		PresentAdd_Section=PresentAdd_Section+1;
		iReturnValidateTotalValue=iReturnValidateTotalValue+PresentAdd_Section;
	}
	
	var InputCtrlId_countryIdPr="countryIdPr";
	var countryIdPr=getInputCtrlTextValue(InputCtrlId_countryIdPr);
	iReturnValidateValue=validateInputListControlStr(countryIdPr,InputCtrlId_countryIdPr,"lblFieldId_"+InputCtrlId_countryIdPr,"lblFieldIdWarning_"+InputCtrlId_countryIdPr);
	if(iReturnValidateValue=="1")
	{
		PresentAdd_Section=PresentAdd_Section+1;
		iReturnValidateTotalValue=iReturnValidateTotalValue+PresentAdd_Section;
	}
	
	
	var InputCtrlId_zipCodePr="zipCodePr";
	var zipCodePr=getInputCtrlTextValue(InputCtrlId_zipCodePr);
	iReturnValidateValue=validateInputTextControlStr(zipCodePr,InputCtrlId_zipCodePr,"lblFieldId_"+InputCtrlId_zipCodePr,"lblFieldIdWarning_"+InputCtrlId_zipCodePr);
	if(iReturnValidateValue=="1")
	{
		PresentAdd_Section=PresentAdd_Section+1;
		iReturnValidateTotalValue=iReturnValidateTotalValue+PresentAdd_Section;
	}
	
	var InputCtrlId_stateIdForDSPQPr="stateIdForDSPQPr";
	var stateIdForDSPQPr=getInputCtrlTextValue(InputCtrlId_stateIdForDSPQPr);
	iReturnValidateValue=validateInputListControlStr(stateIdForDSPQPr,InputCtrlId_stateIdForDSPQPr,"lblFieldId_"+InputCtrlId_stateIdForDSPQPr,"lblFieldIdWarning_"+InputCtrlId_stateIdForDSPQPr);
	if(iReturnValidateValue=="1")
	{
		PresentAdd_Section=PresentAdd_Section+1;
		iReturnValidateTotalValue=iReturnValidateTotalValue+PresentAdd_Section;
	}
	
	var InputCtrlId_cityIdForDSPQPr="cityIdForDSPQPr";
	var cityIdForDSPQPr=getInputCtrlTextValue(InputCtrlId_cityIdForDSPQPr);
	iReturnValidateValue=validateInputListControlStr(cityIdForDSPQPr,InputCtrlId_cityIdForDSPQPr,"lblFieldId_"+InputCtrlId_cityIdForDSPQPr,"lblFieldIdWarning_"+InputCtrlId_cityIdForDSPQPr);
	if(iReturnValidateValue=="1")
	{
		PresentAdd_Section=PresentAdd_Section+1;
		iReturnValidateTotalValue=iReturnValidateTotalValue+PresentAdd_Section;
	}
	
	var InputCtrlId_otherStatePr="otherStatePr";
	var otherStatePr=getInputCtrlTextValue(InputCtrlId_otherStatePr);
	iReturnValidateValue=validateInputTextControlStr(otherStatePr,InputCtrlId_otherStatePr,"lblFieldId_"+InputCtrlId_otherStatePr,"lblFieldIdWarning_"+InputCtrlId_otherStatePr);
	if(iReturnValidateValue=="1")
	{
		PresentAdd_Section=PresentAdd_Section+1;
		iReturnValidateTotalValue=iReturnValidateTotalValue+PresentAdd_Section;
	}
	
	var InputCtrlId_otherCityPr="otherCityPr";
	var otherCityPr=getInputCtrlTextValue(InputCtrlId_otherCityPr);
	iReturnValidateValue=validateInputTextControlStr(otherCityPr,InputCtrlId_otherCityPr,"lblFieldId_"+InputCtrlId_otherCityPr,"lblFieldIdWarning_"+InputCtrlId_otherCityPr);
	if(iReturnValidateValue=="1")
	{
		PresentAdd_Section=PresentAdd_Section+1;
		iReturnValidateTotalValue=iReturnValidateTotalValue+PresentAdd_Section;
	}
	
	
	var PresentAdd_Section1=validateCustomAll(PresentAdd_Section_Name);
	iReturnValidateTotalValue=iReturnValidateTotalValue+PresentAdd_Section1;
	PresentAdd_Section=PresentAdd_Section+PresentAdd_Section1;
	
	if(PresentAdd_Section>0)
	{
		window.location.hash = '#lblSectionId_'+PresentAdd_Section_Name;
		$("#lblSectionId_"+PresentAdd_Section_Name).addClass("DSPQRequired12");
		PresentAdd_Section=0;
	}
	else
	{
		$("#lblSectionId_"+PresentAdd_Section_Name).removeClass("DSPQRequired12");
	}
	
	
	
	var EEOC_Section=0;
	var EEOC_Section_Name="tooltipSection3";
	
	var InputCtrlId_ethnicOriginIdr="ethnicOriginId";
	var ethnicOriginId=getInputCtrlRadioValue(InputCtrlId_ethnicOriginIdr);
	iReturnValidateValue=validateInputRadioControlStr(ethnicOriginId,InputCtrlId_ethnicOriginIdr,"lblFieldId_"+InputCtrlId_ethnicOriginIdr,"lblFieldIdWarning_"+InputCtrlId_ethnicOriginIdr,"hdn_"+InputCtrlId_ethnicOriginIdr);
	if(iReturnValidateValue=="1")
	{
		EEOC_Section=EEOC_Section+1;
		iReturnValidateTotalValue=iReturnValidateTotalValue+EEOC_Section;
	}
	
	var InputCtrlId_ethinicityId="ethinicityId";
	var ethinicityId=getInputCtrlRadioValue(InputCtrlId_ethinicityId);
	iReturnValidateValue=validateInputRadioControlStr(ethinicityId,InputCtrlId_ethinicityId,"lblFieldId_"+InputCtrlId_ethinicityId,"lblFieldIdWarning_"+InputCtrlId_ethinicityId,"hdn_"+InputCtrlId_ethinicityId);
	if(iReturnValidateValue=="1")
	{
		EEOC_Section=EEOC_Section+1;
		iReturnValidateTotalValue=iReturnValidateTotalValue+EEOC_Section;
	}
	
	var InputCtrlId_raceId="raceId";
	var raceId=getInputCtrlCheckBoxValue(InputCtrlId_raceId);
	iReturnValidateValue=validateInputCheckBoxControlStr(raceId,InputCtrlId_raceId,"lblFieldId_"+InputCtrlId_raceId,"lblFieldIdWarning_"+InputCtrlId_raceId,"hdn_"+InputCtrlId_raceId);
	if(iReturnValidateValue=="1")
	{
		EEOC_Section=EEOC_Section+1;
		iReturnValidateTotalValue=iReturnValidateTotalValue+EEOC_Section;
	}
	
	var InputCtrlId_genderId="genderId";
	var genderId=getInputCtrlRadioValue(InputCtrlId_genderId);
	iReturnValidateValue=validateInputRadioControlStr(genderId,InputCtrlId_genderId,"lblFieldId_"+InputCtrlId_genderId,"lblFieldIdWarning_"+InputCtrlId_genderId,"hdn_"+InputCtrlId_genderId);
	if(iReturnValidateValue=="1")
	{
		EEOC_Section=EEOC_Section+1;
		iReturnValidateTotalValue=iReturnValidateTotalValue+EEOC_Section;
	}
	
	var EEOC_Section1=validateCustomAll(EEOC_Section_Name);
	iReturnValidateTotalValue=iReturnValidateTotalValue+EEOC_Section1;
	EEOC_Section=EEOC_Section+EEOC_Section1;
	
	if(EEOC_Section>0)
	{
		window.location.hash = '#lblSectionId_'+EEOC_Section_Name;
		$("#lblSectionId_"+EEOC_Section_Name).addClass("DSPQRequired12");
		EEOC_Section=0;
	}
	else
	{
		$("#lblSectionId_"+EEOC_Section_Name).removeClass("DSPQRequired12");
	}
	
	
	var Veteran_Section=0;
	var Veteran_Section_Name="tooltipSection20";
	
	var InputCtrlId_veteran="veteran";
	var veteran=getInputCtrlRadioValue(InputCtrlId_veteran);
	iReturnValidateValue=validateInputRadioControlStr(veteran,InputCtrlId_veteran,"lblFieldId_"+InputCtrlId_veteran,"lblFieldIdWarning_"+InputCtrlId_veteran,"hdn_"+InputCtrlId_veteran);
	if(iReturnValidateValue=="1")
	{
		Veteran_Section=Veteran_Section+1;
		iReturnValidateTotalValue=iReturnValidateTotalValue+Veteran_Section;
	}
	
	var Veteran_Section1=validateCustomAll(Veteran_Section_Name);
	iReturnValidateTotalValue=iReturnValidateTotalValue+Veteran_Section1;
	Veteran_Section=Veteran_Section+Veteran_Section1;
	
	if(Veteran_Section>0)
	{
		window.location.hash = '#lblSectionId_'+Veteran_Section_Name;
		$("#lblSectionId_"+Veteran_Section_Name).addClass("DSPQRequired12");
		Veteran_Section=0;
	}
	else
	{
		$("#lblSectionId_"+Veteran_Section_Name).removeClass("DSPQRequired12");
	}
	if(veteran==1)
	{
		var districtIdForDSPQ=document.getElementById("dspqjobflowdistrictId").value;
		if(districtIdForDSPQ==1201470){
			if($("#veteranOptS:checked").length==0){
				validateInputRadioControlStr(-1,InputCtrlId_veteran,"lblFieldId_"+InputCtrlId_veteran,"lblFieldIdWarning_"+InputCtrlId_veteran,"hdn_"+InputCtrlId_veteran);
				window.location.hash = '#lblSectionId_'+Veteran_Section_Name;
				$("#lblSectionId_"+Veteran_Section_Name).addClass("DSPQRequired12");
				iReturnValidateTotalValue=1;				
				
			}else{
				setDistrictSpecificVeteranValues();
				iReturnValidateTotalValue=0;
			}
		}
	}
	
	
	var DL_Section=0;
	var DL_Section_Name="tooltipSection22";
	
	var InputCtrlId_drivingLicNum="drivingLicNum";
	var drivingLicNum=getInputCtrlTextValue(InputCtrlId_drivingLicNum);
	iReturnValidateValue=validateInputTextControlStr(drivingLicNum,InputCtrlId_drivingLicNum,"lblFieldId_"+InputCtrlId_drivingLicNum,"lblFieldIdWarning_"+InputCtrlId_drivingLicNum);
	if(iReturnValidateValue=="1")
	{
		DL_Section=DL_Section+1;
		iReturnValidateTotalValue=iReturnValidateTotalValue+DL_Section;
	}
	
	
	var InputCtrlId_drivingLicState="drivingLicState";
	var drivingLicState=getInputCtrlTextValue(InputCtrlId_drivingLicState);
	iReturnValidateValue=validateInputListControlStr(drivingLicState,InputCtrlId_drivingLicState,"lblFieldId_"+InputCtrlId_drivingLicState,"lblFieldIdWarning_"+InputCtrlId_drivingLicState);
	if(iReturnValidateValue=="1")
	{
		DL_Section=DL_Section+1;
		iReturnValidateTotalValue=iReturnValidateTotalValue+DL_Section;
	}
	
	var DL_Section1=validateCustomAll(DL_Section_Name);
	iReturnValidateTotalValue=iReturnValidateTotalValue+DL_Section1;
	DL_Section=DL_Section+DL_Section1;
	
	if(DL_Section>0)
	{
		window.location.hash = '#lblSectionId_'+DL_Section_Name;
		$("#lblSectionId_"+DL_Section_Name).addClass("DSPQRequired12");
		DL_Section=0;
	}
	else
	{
		$("#lblSectionId_"+DL_Section_Name).removeClass("DSPQRequired12");
	}
	var Lang_Error=0;
	if($("#divLanguageForm").is(':visible'))
	{
		Lang_Error=insertOrUpdateTchrLang();
		iReturnValidateTotalValue=iReturnValidateTotalValue+Lang_Error;
	}
	
	var Lang_Section_Name="tooltipSection18";
	var Lang_Error1=validateCustomAll(Lang_Section_Name);
	iReturnValidateTotalValue=iReturnValidateTotalValue+Lang_Error1;
	
	
	var knowLanguageChkValue="-1";
	if($("#lblSectionId_tooltipSection18").is(':visible'))
	{
		knowLanguageChkValue = $('input[name=knowLanguage]:checked').val();
		
	}
	//alert("iReturnValidateTotalValue "+iReturnValidateTotalValue);
	
	if(iReturnValidateTotalValue==0)
	{
		
		DSPQServiceAjax.savePersonalInfoData(jobId,dspqct,salutation_pi,firstName_pi,middleName_pi,lastName_pi,anotherName,ssn_pi,expectedSalary,phoneNumber,dob,addressLine1,addressLine2,countryId,zipCode,stateIdForDSPQ,cityIdForDSPQ,otherState,otherCity,addressLinePr,addressLine2Pr,countryIdPr,zipCodePr,stateIdForDSPQPr,cityIdForDSPQPr,otherStatePr,otherCityPr,ethnicOriginId,ethinicityId,raceId,genderId,veteran,mobileNumber,drivingLicNum,drivingLicState,knowLanguageChkValue,{ 
			async: false,
			errorHandler:handleError,
			callback:function(data)
			{
				$('#jafLoadingDiv').hide();
				if(data=="OK")
				{
					if(inputValue=="0")
						getNextPage();
					else if(inputValue=="1")
						SaveAndExitPortfolioL2();
				}
			}});
		$('#jafLoadingDiv').hide();
			return true;
	}
	else
	{
		$('#jafLoadingDiv').hide();
		return false;
	}
	
}

function validateDSPQAcademicGroup(inputValue)
{
	$('#jafLoadingDiv').show();
	
	var jobId=""; 
	try { jobId=document.getElementById("dspqJobId").value; } catch (e) {}

	var dspqct=""; 
	try { dspqct=document.getElementById("dspqct").value; } catch (e) {}
	
	var dspqjobflowdistrictId="";
	try { dspqjobflowdistrictId=document.getElementById("dspqjobflowdistrictId").value; } catch (e) {}
	
	var dspqPortfolioNameId="";
	try { dspqPortfolioNameId=document.getElementById("dspqPortfolioNameId").value; } catch (e) {}
	
	var iReturnValidateTotalValue=0;
	var iReturnValidateValue=0;
	
	iReturnValidateValue=insertOrUpdate_Academics()
	iReturnValidateTotalValue=iReturnValidateTotalValue+iReturnValidateValue;
	
	var Academic_Section_Name="tooltipSection06";
	var Academic_Section1=validateCustomAll(Academic_Section_Name);
	iReturnValidateTotalValue=iReturnValidateTotalValue+Academic_Section1;
	
	if(iReturnValidateTotalValue>0)
	{
		window.location.hash = '#lblSectionId_'+Academic_Section_Name;
		
		$("#lblSectionId_"+Academic_Section_Name).addClass("DSPQRequired12");
		Academic_Section=0;
		$('#jafLoadingDiv').hide();
		return false;
		//$('#loadingDiv').hide();
	}
	else if(iReturnValidateTotalValue==0)
	{
		$("#lblSectionId_"+Academic_Section_Name).removeClass("DSPQRequired12");
		
		DSPQServiceAjax.updatedspqJobWiseStatusAcademic(dspqPortfolioNameId,dspqct,jobId,{ 
			async: false,
			errorHandler:handleError,
			callback:function(data)
			{
				$('#jafLoadingDiv').hide();
				//alert("data "+data);
				var res = data.split("||");
				
				if(res[0]=="OK" && res[1]=="" && res[2]=="" && res[3]=="")
				{
					if(inputValue=="0")
						getNextPage();
					else if(inputValue=="1")
						SaveAndExitPortfolioL2();
				}
				else if(res[1]!="" || res[2]!="" || res[3]!="")
				{
					showAcademicGroupError();
					$('#academicGroupErrorMsg').empty();
					var SectionName06="tooltipSection06";
					$("#lblSectionId_"+SectionName06).addClass("DSPQRequired12");
					
					if(res[1]!="")
					{
						$('#academicGroupErrorMsg').append("&#149;Please enter academic details.<br>");
					}
					else if(res[3]!="")
					{
						$('#academicGroupErrorMsg').append("&#149;"+res[3]+"<br>");
					}
					else if(res[2]!="")
					{
						$('#academicGroupErrorMsg').append("&#149;"+res[2]+"<br>");
					}
				}
				
				
				
			}});
		$('#jafLoadingDiv').hide();
		return true;
	}
	else
	{
		$('#jafLoadingDiv').hide();
		return false;
	}
}

function validateDSPQCredentialsGroup(inputValue)
{

	$('#jafLoadingDiv').show();
	
	var jobId=""; 
	try { jobId=document.getElementById("dspqJobId").value; } catch (e) {}

	var dspqct=""; 
	try { dspqct=document.getElementById("dspqct").value; } catch (e) {}
	
	var dspqjobflowdistrictId="";
	try { dspqjobflowdistrictId=document.getElementById("dspqjobflowdistrictId").value; } catch (e) {}
	
	var dspqPortfolioNameId="";
	try { dspqPortfolioNameId=document.getElementById("dspqPortfolioNameId").value; } catch (e) {}
	
	
	
	var iReturnValidateTotalValue=0;
	var iReturnValidateValue=0;
	
	///////////////////
	
	var gk_Section=0;
	var gk_Section_Name="tooltipSection27";

	iReturnValidateValue=saveGeneralKnw();
	//alert("iReturnValidateValue "+iReturnValidateValue);
	iReturnValidateTotalValue=iReturnValidateTotalValue+iReturnValidateValue;
	gk_Section=gk_Section+iReturnValidateValue;
	
	var gk_Section1=validateCustomAll(gk_Section_Name);
	iReturnValidateTotalValue=iReturnValidateTotalValue+gk_Section1;
	gk_Section=gk_Section+gk_Section1;
	
	if(gk_Section>0)
	{
		window.location.hash = '#lblSectionId_'+gk_Section_Name;
		$("#lblSectionId_"+gk_Section_Name).addClass("DSPQRequired12");
	}
	else
	{
		$("#lblSectionId_"+gk_Section_Name).removeClass("DSPQRequired12");
	}
	
	var certifiedTeacher_Section=0;
	var certifiedTeacher_Section_Name="tooltipSection29";
	
	var InputCtrlId_isNonTeacher="isNonTeacher";
	var isNonTeacher=getInputCtrlRadioValue(InputCtrlId_isNonTeacher);
	iReturnValidateValue=validateInputRadioControlStr(isNonTeacher,InputCtrlId_isNonTeacher,"lblFieldId_"+InputCtrlId_isNonTeacher,"lblFieldIdWarning_"+InputCtrlId_isNonTeacher,"hdn_"+InputCtrlId_isNonTeacher);
	if(iReturnValidateValue=="1")
	{
		certifiedTeacher_Section=certifiedTeacher_Section+1;
		iReturnValidateTotalValue=iReturnValidateTotalValue+certifiedTeacher_Section;
	}
	var expCertTeacherTraining=0;
	if(isNonTeacher==1)
	{
		var InputCtrlId_expCertTeacherTraining="expCertTeacherTraining";
		expCertTeacherTraining=getInputCtrlTextValue(InputCtrlId_expCertTeacherTraining);
		iReturnValidateValue=validateInputTextControlNumber(expCertTeacherTraining,InputCtrlId_expCertTeacherTraining,"lblFieldId_"+InputCtrlId_expCertTeacherTraining,"lblFieldIdWarning_"+InputCtrlId_expCertTeacherTraining);
		if(iReturnValidateValue=="1")
		{
			certifiedTeacher_Section=certifiedTeacher_Section+1;
			iReturnValidateTotalValue=iReturnValidateTotalValue+certifiedTeacher_Section;
		}
	}
	
	
	var certifiedTeacher_Section1=validateCustomAll(certifiedTeacher_Section_Name);
	iReturnValidateTotalValue=iReturnValidateTotalValue+certifiedTeacher_Section1;
	certifiedTeacher_Section=certifiedTeacher_Section+certifiedTeacher_Section1;
	
	if(certifiedTeacher_Section>0)
	{
		window.location.hash = '#lblSectionId_'+certifiedTeacher_Section_Name;
		$("#lblSectionId_"+certifiedTeacher_Section_Name).addClass("DSPQRequired12");
	}
	else
	{
		$("#lblSectionId_"+certifiedTeacher_Section_Name).removeClass("DSPQRequired12");	
	}
	
	//////////////////
	
	var Cred_Section=0;
	var Cred_Section_Name="tooltipSection7";
	
	var InputCtrlId_nationalBoardCert="nationalBoardCert";
	var nationalBoardCert=getInputCtrlRadioValue(InputCtrlId_nationalBoardCert);
	iReturnValidateValue=validateInputRadioControlStr(nationalBoardCert,InputCtrlId_nationalBoardCert,"lblFieldId_"+InputCtrlId_nationalBoardCert,"lblFieldIdWarning_"+InputCtrlId_nationalBoardCert,"hdn_"+InputCtrlId_nationalBoardCert);
	if(iReturnValidateValue=="1")
	{
		Cred_Section=Cred_Section+1;
		iReturnValidateTotalValue=iReturnValidateTotalValue+Cred_Section;
	}
	
	var InputCtrlId_nationalBoardCertYear="nationalBoardCertYear";
	var nationalBoardCertYear=getInputCtrlTextValue(InputCtrlId_nationalBoardCertYear);
	if(nationalBoardCert==1)
	{
		iReturnValidateValue=validateInputListControlStr(nationalBoardCertYear,InputCtrlId_nationalBoardCertYear,"lblFieldId_"+InputCtrlId_nationalBoardCertYear,"lblFieldIdWarning_"+InputCtrlId_nationalBoardCertYear);
		if(iReturnValidateValue=="1")
		{
			Cred_Section=Cred_Section+1;
			iReturnValidateTotalValue=iReturnValidateTotalValue+Cred_Section;
		}
	}
	else
	{
		UnMarkedLabelAsRed(InputCtrlId_nationalBoardCertYear,"lblFieldId_"+InputCtrlId_nationalBoardCertYear,"lblFieldIdWarning_"+InputCtrlId_nationalBoardCertYear);
	}
	
	var Cred_Section1=validateCustomAll(Cred_Section_Name);
	iReturnValidateTotalValue=iReturnValidateTotalValue+Cred_Section1;
	Cred_Section=Cred_Section+Cred_Section1;
	
	if(Cred_Section>0)
	{
		window.location.hash = '#lblSectionId_'+Cred_Section_Name;
		$("#lblSectionId_"+Cred_Section_Name).addClass("DSPQRequired12");
	}
	else
	{
		$("#lblSectionId_"+Cred_Section_Name).removeClass("DSPQRequired12");	
	}
	
	var Sub_Thr_Section=0;
	var Sub_Thr_Section_Name="tooltipSection21";
	
	var InputCtrlId_SubTea="Substitute_Teacher";
	var Substitute_Teacher=getInputCtrlRadioValue(InputCtrlId_SubTea);
	iReturnValidateValue=validateInputRadioControlStr(Substitute_Teacher,InputCtrlId_SubTea,"lblFieldId_"+InputCtrlId_SubTea,"lblFieldIdWarning_"+InputCtrlId_SubTea,"hdn_"+InputCtrlId_SubTea);
	if(iReturnValidateValue=="1")
	{
		Sub_Thr_Section=Sub_Thr_Section+1;
		iReturnValidateTotalValue=iReturnValidateTotalValue+Sub_Thr_Section;
	}
	
	var Sub_Thr_Section1=validateCustomAll(Sub_Thr_Section_Name);
	iReturnValidateTotalValue=iReturnValidateTotalValue+Sub_Thr_Section1;
	Sub_Thr_Section=Sub_Thr_Section+Sub_Thr_Section1;
	
	if(Sub_Thr_Section>0)
	{
		window.location.hash = '#lblSectionId_'+Sub_Thr_Section_Name;
		$("#lblSectionId_"+Sub_Thr_Section_Name).addClass("DSPQRequired12");
	}
	else
	{
		$("#lblSectionId_"+Sub_Thr_Section_Name).removeClass("DSPQRequired12");	
	}
	
	var TFA_Section=0;
	var TFA_Section_Name="tooltipSection23";
	
	var InputCtrlId_tfaAffiliate="tfaAffiliate";
	var tfaAffiliate=getInputCtrlTextValue(InputCtrlId_tfaAffiliate);
	iReturnValidateValue=validateInputListControlStr(tfaAffiliate,InputCtrlId_tfaAffiliate,"lblFieldId_"+InputCtrlId_tfaAffiliate,"lblFieldIdWarning_"+InputCtrlId_tfaAffiliate);
	if(iReturnValidateValue=="1")
	{
		TFA_Section=TFA_Section+1;
		iReturnValidateTotalValue=iReturnValidateTotalValue+TFA_Section;
	}
	
	var InputCtrlId_corpsYear="corpsYear";
	var corpsYear=getInputCtrlTextValue(InputCtrlId_corpsYear);
	
	var InputCtrlId_tfaRegion="tfaRegion";
	var tfaRegion=getInputCtrlTextValue(InputCtrlId_tfaRegion);
	
	if(tfaAffiliate ==1 || tfaAffiliate ==2)
	{
		iReturnValidateValue=validateInputListControlStr(corpsYear,InputCtrlId_corpsYear,"lblFieldId_"+InputCtrlId_corpsYear,"lblFieldIdWarning_"+InputCtrlId_corpsYear);
		if(iReturnValidateValue=="1")
		{
			TFA_Section=TFA_Section+1;
			iReturnValidateTotalValue=iReturnValidateTotalValue+TFA_Section;
		}
		
		iReturnValidateValue=validateInputListControlStr(tfaRegion,InputCtrlId_tfaRegion,"lblFieldId_"+InputCtrlId_tfaRegion,"lblFieldIdWarning_"+InputCtrlId_tfaRegion);
		if(iReturnValidateValue=="1")
		{
			TFA_Section=TFA_Section+1;
			iReturnValidateTotalValue=iReturnValidateTotalValue+TFA_Section;
		}
	}
	else
	{
		UnMarkedLabelAsRed(InputCtrlId_corpsYear,"lblFieldId_"+InputCtrlId_corpsYear,"lblFieldIdWarning_"+InputCtrlId_corpsYear);
		UnMarkedLabelAsRed(InputCtrlId_tfaRegion,"lblFieldId_"+InputCtrlId_tfaRegion,"lblFieldIdWarning_"+InputCtrlId_tfaRegion);
	}
	
	var TFA_Section1=validateCustomAll(TFA_Section_Name);
	iReturnValidateTotalValue=iReturnValidateTotalValue+TFA_Section1;
	TFA_Section=TFA_Section+TFA_Section1;
	
	if(TFA_Section>0)
	{
		window.location.hash = '#lblSectionId_'+TFA_Section_Name;
		$("#lblSectionId_"+TFA_Section_Name).addClass("DSPQRequired12");
	}
	else
	{
		$("#lblSectionId_"+TFA_Section_Name).removeClass("DSPQRequired12");	
	}
	
	
	iReturnValidateValue=insertOrUpdate_Certification()
	iReturnValidateTotalValue=iReturnValidateTotalValue+iReturnValidateValue;
	
	var Cert_Section_Name="tooltipSection8";
	var Cert_Section1=validateCustomAll(Cert_Section_Name);
	iReturnValidateTotalValue=iReturnValidateTotalValue+Cert_Section1;
	
	if(Cert_Section1>0)
	{
		window.location.hash = '#lblSectionId_'+Cert_Section_Name;
		$("#lblSectionId_"+Cert_Section_Name).addClass("DSPQRequired12");
	}
	
	
	iReturnValidateValue=insertOrUpdateElectronicReferences()
	iReturnValidateTotalValue=iReturnValidateTotalValue+iReturnValidateValue;
	
	var Ref_Section_Name="tooltipSection09";
	var Ref_Section1=validateCustomAll(Ref_Section_Name);
	iReturnValidateTotalValue=iReturnValidateTotalValue+Ref_Section1;
	
	if(Ref_Section1>0)
	{
		window.location.hash = '#lblSectionId_'+Ref_Section_Name;
		$("#lblSectionId_"+Ref_Section_Name).addClass("DSPQRequired12");
	}
	
	iReturnValidateValue=insertOrUpdatevideoLinks();
	iReturnValidateTotalValue=iReturnValidateTotalValue+iReturnValidateValue;
	
	var Video_Section_Name="tooltipSection10";
	var video_Section1=validateCustomAll(Video_Section_Name);
	iReturnValidateTotalValue=iReturnValidateTotalValue+video_Section1;
	
	if(video_Section1>0)
	{
		window.location.hash = '#lblSectionId_'+Video_Section_Name;
		$("#lblSectionId_"+Video_Section_Name).addClass("DSPQRequired12");
	}
	
	// Add Doc
	
	iReturnValidateValue=insertOrUpdate_AdditionalDocuments();
	//alert("0 iReturnValidateValue "+iReturnValidateValue);
	iReturnValidateTotalValue=iReturnValidateTotalValue+iReturnValidateValue;
	
	var AddDoc_Section_Name="tooltipSection11";
	var AddDoc_Section1=validateCustomAll(AddDoc_Section_Name);
	iReturnValidateTotalValue=iReturnValidateTotalValue+AddDoc_Section1;
	
	if(AddDoc_Section1>0)
	{
		window.location.hash = '#lblSectionId_'+AddDoc_Section_Name;
		$("#lblSectionId_"+AddDoc_Section_Name).addClass("DSPQRequired12");
	}
	iReturnValidateValue=insertOrUpdateSubjectArea();
	iReturnValidateTotalValue=iReturnValidateTotalValue+iReturnValidateValue;
	
	var SubjectArea_Section_Name="tooltipSection28";
	var SubjectArea_Section_Name1=validateCustomAll(SubjectArea_Section_Name);
	iReturnValidateTotalValue=iReturnValidateTotalValue+SubjectArea_Section_Name1;
	
	if(SubjectArea_Section_Name1>0)
	{
		window.location.hash = '#lblSectionId_'+SubjectArea_Section_Name;
		$("#lblSectionId_"+SubjectArea_Section_Name).addClass("DSPQRequired12");
	}	
	//alert("iReturnValidateTotalValue "+iReturnValidateTotalValue);
	
	if(iReturnValidateTotalValue==0)
	{
		DSPQServiceAjax.updateCredentialsGroup(dspqPortfolioNameId,dspqct,jobId,Substitute_Teacher,tfaAffiliate,corpsYear,tfaRegion,nationalBoardCert,nationalBoardCertYear,isNonTeacher,expCertTeacherTraining,{ 
			async: false,
			errorHandler:handleError,
			callback:function(data)
			{
				$('#jafLoadingDiv').hide();
				
				var res = data.split("||");
				if(res[0]=="OK")
				{
					if(inputValue=="0")
						getNextPage();
					else if(inputValue=="1")
						SaveAndExitPortfolioL2();
				}
				else if(res[1]!="")
				{
						showCredentialGroupError();
						$('#credentialGroupErrorMsg').empty();
						
						var res1 = res[1].split(",");
						for(var i=0;i<res1.length;i++)
						{
							if(res1[i]==9)
							{
								showNumberRequiredMsg(res1[i]);
							}
							else if(res1[i]==8)
							{
								var SectionName08="tooltipSection8";
								$("#lblSectionId_"+SectionName08).addClass("DSPQRequired12");
								$('#credentialGroupErrorMsg').append("&#149;Please enter Certification Licensure details.<br>");
							}
							else if(res1[i]==10)
							{
								var SectionName10="tooltipSection10";
								$("#lblSectionId_"+SectionName10).addClass("DSPQRequired12");
								$('#credentialGroupErrorMsg').append("&#149;Please enter Video Link details.<br>");
							}
							else if(res1[i]==11)
							{
								var SectionName11="tooltipSection11";
								$("#lblSectionId_"+SectionName11).addClass("DSPQRequired12");
								$('#credentialGroupErrorMsg').append("&#149;Please enter Additional Document details.<br>");
							}
						}
				}
		}});
		
		$('#jafLoadingDiv').hide();
		return true;
	}
	else
	{
		$('#jafLoadingDiv').hide();
		return false;
	}

}

function callFromServletSaveGeneralKnw(UploadedFileName)
{
	//alert("servlet UploadedFileName "+UploadedFileName);
	saveGeneralKnw(UploadedFileName);	
}

function saveGeneralKnw(UploadedFileName)
{
	var iReturnValidateTotalValue=0;
	var iReturnValidateValue=0;
	
	if($("#lblSectionId_tooltipSection27").is(':visible'))
	{
		var gk_Section=0;
		var gk_Section_Name="tooltipSection27";
		
		var InputCtrlId_generalKnowledgeExamStatus="generalKnowledgeExamStatus";
		var generalKnowledgeExamStatus=getInputCtrlTextValue(InputCtrlId_generalKnowledgeExamStatus);
		iReturnValidateValue=validateInputListControlStr(generalKnowledgeExamStatus,InputCtrlId_generalKnowledgeExamStatus,"lblFieldId_"+InputCtrlId_generalKnowledgeExamStatus,"lblFieldIdWarning_"+InputCtrlId_generalKnowledgeExamStatus);
		if(iReturnValidateValue=="1")
		{
			gk_Section=gk_Section+1;
			iReturnValidateTotalValue=iReturnValidateTotalValue+gk_Section;
		}
		
		
		var InputCtrlId_generalKnowledgeExamDate="generalKnowledgeExamDate";
		var generalKnowledgeExamDate=getInputCtrlTextValue(InputCtrlId_generalKnowledgeExamDate);
		iReturnValidateValue=validateInputTextControlStr(generalKnowledgeExamDate,InputCtrlId_generalKnowledgeExamDate,"lblFieldId_"+InputCtrlId_generalKnowledgeExamDate,"lblFieldIdWarning_"+InputCtrlId_generalKnowledgeExamDate);
		if(iReturnValidateValue=="1")
		{
			gk_Section=gk_Section+1;
			iReturnValidateTotalValue=iReturnValidateTotalValue+gk_Section;
		}
		
		var InputCtrlId_generalExamNote="generalExamNote";
		var generalExamNote=getTextAreaEditorCtrlTextValue(InputCtrlId_generalExamNote);
		iReturnValidateValue=validateInputTextAreaControlStr(generalExamNote,InputCtrlId_generalExamNote,"lblFieldId_"+InputCtrlId_generalExamNote,"lblFieldIdWarning_"+InputCtrlId_generalExamNote);
		if(iReturnValidateValue=="1")
		{
			gk_Section=gk_Section+1;
			iReturnValidateTotalValue=iReturnValidateTotalValue+gk_Section;
		}
		
		var generalKnowledgeScoreReport="";
		if(UploadedFileName==undefined)
		{
			
			var db_generalKnowledgeScoreReport=getInputCtrlTextValue("db_generalKnowledgeScoreReport");
			var InputCtrlId_generalKnowledgeScoreReport="generalKnowledgeScoreReport";
			generalKnowledgeScoreReport=getInputCtrlTextValue(InputCtrlId_generalKnowledgeScoreReport);
			iReturnValidateValue=validateInputFileControlStr(generalKnowledgeScoreReport,InputCtrlId_generalKnowledgeScoreReport,"lblFieldId_"+InputCtrlId_generalKnowledgeScoreReport,"lblFieldIdWarning_"+InputCtrlId_generalKnowledgeScoreReport,db_generalKnowledgeScoreReport);
			if(iReturnValidateValue=="1")
			{
				gk_Section=gk_Section+1;
				iReturnValidateTotalValue=iReturnValidateTotalValue+gk_Section;
			}
			
			if(gk_Section>0)
			{
				$("#lblSectionId_"+gk_Section_Name).addClass("DSPQRequired12");
				//gk_Section=0;
			}
			else
			{
				$("#lblSectionId_"+gk_Section_Name).removeClass("DSPQRequired12");
			}
		}	
			
		if(iReturnValidateTotalValue==0)
		{
			if(generalKnowledgeScoreReport!="")
			{
				//document.getElementById("jafFrmMultifileuploadform").submit();
				try
				{
					var sampleFile = document.getElementById("generalKnowledgeScoreReport").files[0];
				    var formdata = new FormData();
				    formdata.append("generalKnowledgeScoreReport", sampleFile);
				    var xhr = new XMLHttpRequest();       
				    xhr.open("POST","teacherGeneralKnowledgeServlet.do", true);
				    xhr.send(formdata);	
				    xhr.onload = function(e) 
				    {
				        if (this.status == 200) 
				        {
				        	//alert("responseText" +this.responseText);
				        	if(this.responseText!="Error")
				        		UploadedFileName=this.responseText;
				        	
				        	DSPQServiceAjax.saveGeneralKnw(generalKnowledgeExamStatus,generalKnowledgeExamDate,UploadedFileName,generalExamNote,{
								async: false,
								errorHandler:handleError,
								callback:function(data)
								{
									if(data=="OK")
									{
										//document.getElementById("jafFrmMultifileuploadform").reset();
										//$("a#hrefResume").html(UploadedFileName);
										iReturnValidateTotalValue="0";
									}
									else
										iReturnValidateTotalValue="1";
								}
								});
				        }
				    }; 
						    
					
				}catch(err){}
				
				//return 1;
			}
			else
			{
				if(UploadedFileName==null || UploadedFileName=="" || UploadedFileName==undefined)
				{
					UploadedFileName=getInputCtrlTextValue("db_generalKnowledgeScoreReport");
				}
					
				DSPQServiceAjax.saveGeneralKnw(generalKnowledgeExamStatus,generalKnowledgeExamDate,UploadedFileName,generalExamNote,{
					async: false,
					errorHandler:handleError,
					callback:function(data)
					{
						if(data=="OK")
						{
							//document.getElementById("jafFrmMultifileuploadform").reset();
							//$("a#hrefResume").html(UploadedFileName);
							iReturnValidateTotalValue="0";
						}
						else
							iReturnValidateTotalValue="1";
					}
					});
				
				
			}
		}
	}
	return iReturnValidateTotalValue;
	
}

function validateDSPQProfessionalGroup(inputValue)
{
	$('#jafLoadingDiv').show();
	
	var jobId=""; 
	try { jobId=document.getElementById("dspqJobId").value; } catch (e) {}

	var dspqct=""; 
	try { dspqct=document.getElementById("dspqct").value; } catch (e) {}
	
	var dspqjobflowdistrictId="";
	try { dspqjobflowdistrictId=document.getElementById("dspqjobflowdistrictId").value; } catch (e) {}
	
	var dspqPortfolioNameId="";
	try { dspqPortfolioNameId=document.getElementById("dspqPortfolioNameId").value; } catch (e) {}
	
	var iReturnValidateTotalValue=0;
	var iReturnValidateValue=0;
	
	var Resume_Section=0;
	var Resume_Section_Name="tooltipSection12";

	iReturnValidateValue=uploadResume();
	iReturnValidateTotalValue=iReturnValidateTotalValue+iReturnValidateValue;
	Resume_Section=Resume_Section+iReturnValidateValue;
	
	var Resume_Section1=validateCustomAll(Resume_Section_Name);
	iReturnValidateTotalValue=iReturnValidateTotalValue+Resume_Section1;
	Resume_Section=Resume_Section+Resume_Section1;
	
	if(Resume_Section>0)
	{
		window.location.hash = '#lblSectionId_'+Resume_Section_Name;
		$("#lblSectionId_"+Resume_Section_Name).addClass("DSPQRequired12");
	}
	else
	{
		$("#lblSectionId_"+Resume_Section_Name).removeClass("DSPQRequired12");
	}
	
	var WorkExp_Section=0;
	iReturnValidateValue=insertOrUpdateEmployment()
	iReturnValidateTotalValue=iReturnValidateTotalValue+iReturnValidateValue;
	WorkExp_Section=WorkExp_Section+iReturnValidateValue;
	
	var WorkExp_Section_Name="tooltipSection13";
	var WorkExp_Section1=validateCustomAll(WorkExp_Section_Name);
	iReturnValidateTotalValue=iReturnValidateTotalValue+WorkExp_Section1;
	WorkExp_Section=WorkExp_Section+WorkExp_Section1;
	
	if(WorkExp_Section>0)
	{
		window.location.hash = '#lblSectionId_'+WorkExp_Section_Name;
		$("#lblSectionId_"+WorkExp_Section_Name).addClass("DSPQRequired12");
	}
	else
	{
		$("#lblSectionId_"+WorkExp_Section_Name).removeClass("DSPQRequired12");
	}
	
	var Inv_Section=0;
	iReturnValidateValue=saveOrUpdateInvolvement()
	iReturnValidateTotalValue=iReturnValidateTotalValue+iReturnValidateValue;
	Inv_Section=Inv_Section+iReturnValidateValue;
	
	var Inv_Section_Name="tooltipSection14";
	var Inv_Section1=validateCustomAll(Inv_Section_Name);
	iReturnValidateTotalValue=iReturnValidateTotalValue+Inv_Section1;
	Inv_Section=Inv_Section+Inv_Section1;
	
	if(Inv_Section>0)
	{
		window.location.hash = '#lblSectionId_'+Inv_Section_Name;
		$("#lblSectionId_"+Inv_Section_Name).addClass("DSPQRequired12");
	}
	else
	{
		$("#lblSectionId_"+Inv_Section_Name).removeClass("DSPQRequired12");
	}
	
	var Honor_Section=0;
	iReturnValidateValue=saveOrUpdateHonors();
	iReturnValidateTotalValue=iReturnValidateTotalValue+iReturnValidateValue;
	Honor_Section=Honor_Section+iReturnValidateValue;
	
	var Honor_Section_Name="tooltipSection15";
	var Honor_Section1=validateCustomAll(Honor_Section_Name);
	iReturnValidateTotalValue=iReturnValidateTotalValue+Honor_Section1;
	Honor_Section=Honor_Section+Honor_Section1;
	
	if(Honor_Section>0)
	{
		window.location.hash = '#lblSectionId_'+Honor_Section_Name;
		$("#lblSectionId_"+Honor_Section_Name).addClass("DSPQRequired12");
	}
	else
	{
		$("#lblSectionId_"+Honor_Section_Name).removeClass("DSPQRequired12");
	}
	
	var retire_Section=0;
	var retire_Section_Name="tooltipSection24";
	
	var InputCtrlId_isretired="isretired";
	var isretired=getInputCtrlRadioValue(InputCtrlId_isretired);
	iReturnValidateValue=validateInputRadioControlStr(isretired,InputCtrlId_isretired,"lblFieldId_"+InputCtrlId_isretired,"lblFieldIdWarning_"+InputCtrlId_isretired,"hdn_"+InputCtrlId_isretired);
	if(iReturnValidateValue=="1")
	{
		retire_Section=retire_Section+1;
		iReturnValidateTotalValue=iReturnValidateTotalValue+retire_Section;
	}
	
	if(isretired==1)
	{
		var InputCtrlId_retireddistrictId="retireddistrictId";
		var InputCtrlIdAutoSuggest_retireddistrictName="retireddistrictName";
		var retireddistrictId=getInputCtrlTextValue(InputCtrlId_retireddistrictId);
		iReturnValidateValue=validateInputTextControlStrAutoSuggest(retireddistrictId,InputCtrlId_retireddistrictId,"lblFieldId_"+InputCtrlIdAutoSuggest_retireddistrictName,"lblFieldIdWarning_"+InputCtrlIdAutoSuggest_retireddistrictName,InputCtrlIdAutoSuggest_retireddistrictName);
		if(iReturnValidateValue=="1")
		{
			retire_Section=retire_Section+1;
			iReturnValidateTotalValue=iReturnValidateTotalValue+retire_Section;
		}
		
		var InputCtrlId_stMForretire="stMForretire";
		var stMForretire=getInputCtrlTextValue(InputCtrlId_stMForretire);
		iReturnValidateValue=validateInputListControlStr(stMForretire,InputCtrlId_stMForretire,"lblFieldId_"+InputCtrlId_stMForretire,"lblFieldIdWarning_"+InputCtrlId_stMForretire);
		if(iReturnValidateValue=="1")
		{
			retire_Section=retire_Section+1;
			iReturnValidateTotalValue=iReturnValidateTotalValue+retire_Section;
		}
		
		var InputCtrlId_retireNo="retireNo";
		var retireNo=getInputCtrlTextValue(InputCtrlId_retireNo);
		iReturnValidateValue=validateInputTextControlStr(retireNo,InputCtrlId_retireNo,"lblFieldId_"+InputCtrlId_retireNo,"lblFieldIdWarning_"+InputCtrlId_retireNo);
		if(iReturnValidateValue=="1")
		{
			retire_Section=retire_Section+1;
			iReturnValidateTotalValue=iReturnValidateTotalValue+retire_Section;
		}
	}
	
	var retire_Section1=validateCustomAll(retire_Section_Name);
	iReturnValidateTotalValue=iReturnValidateTotalValue+retire_Section1;
	retire_Section=retire_Section+retire_Section1;
	
	if(retire_Section>0)
	{
		window.location.hash = '#lblSectionId_'+retire_Section_Name;
		$("#lblSectionId_"+retire_Section_Name).addClass("DSPQRequired12");
	}
	else
	{
		$("#lblSectionId_"+retire_Section_Name).removeClass("DSPQRequired12");
	}
	
	// Current Employment
	var currentEmployment_Section=0;
	var currentEmployment_Section_Name="tooltipSection26";
	
	var InputCtrlId_employmentStatus="employmentStatus";
	var employmentStatus=getInputCtrlRadioValue(InputCtrlId_employmentStatus);
	iReturnValidateValue=validateInputRadioControlStr(employmentStatus,InputCtrlId_employmentStatus,"lblFieldId_"+InputCtrlId_employmentStatus,"lblFieldIdWarning_"+InputCtrlId_employmentStatus,"hdn_"+InputCtrlId_employmentStatus);
	//alert("iReturnValidateValue "+iReturnValidateValue);
	
	var employeeNumberOpt1="";
	var employeeNumberOpt2="";
	var staffTypeOnExperiences="";
	var RetirementDate=null;
	var dateOfWithdrawn=null;
	var noLongerEmployed="";
	
	if(employmentStatus=="-1" && iReturnValidateValue=="1")
	{
		currentEmployment_Section=currentEmployment_Section+1;
		iReturnValidateTotalValue=iReturnValidateTotalValue+currentEmployment_Section;
		
		var rdchk_employmentStatus_1="employmentStatus_1";
		MarkedLabelAsRed(rdchk_employmentStatus_1,"lblFieldId_"+rdchk_employmentStatus_1,"lblFieldIdWarning_"+rdchk_employmentStatus_1);
		
		var rdchk_employmentStatus_2="employmentStatus_2";
		MarkedLabelAsRed(rdchk_employmentStatus_2,"lblFieldId_"+rdchk_employmentStatus_2,"lblFieldIdWarning_"+rdchk_employmentStatus_2);
		
		var rdchk_employmentStatus_3="employmentStatus_3";
		MarkedLabelAsRed(rdchk_employmentStatus_3,"lblFieldId_"+rdchk_employmentStatus_3,"lblFieldIdWarning_"+rdchk_employmentStatus_3);
	}
	else
	{
		var rdchk_employmentStatus_1="employmentStatus_1";
		UnMarkedLabelAsRed(rdchk_employmentStatus_1,"lblFieldId_"+rdchk_employmentStatus_1,"lblFieldIdWarning_"+rdchk_employmentStatus_1);
		
		var rdchk_employmentStatus_2="employmentStatus_2";
		UnMarkedLabelAsRed(rdchk_employmentStatus_2,"lblFieldId_"+rdchk_employmentStatus_2,"lblFieldIdWarning_"+rdchk_employmentStatus_2);
		
		var rdchk_employmentStatus_3="employmentStatus_3";
		UnMarkedLabelAsRed(rdchk_employmentStatus_3,"lblFieldId_"+rdchk_employmentStatus_3,"lblFieldIdWarning_"+rdchk_employmentStatus_3);
		
		
		if(employmentStatus=="0")
		{
			var InputCtrlId_employeeNumberOpt1="employeeNumberOpt1";
			employeeNumberOpt1=getInputCtrlTextValue(InputCtrlId_employeeNumberOpt1);
			iReturnValidateValue=validateInputTextControlStr(employeeNumberOpt1,InputCtrlId_employeeNumberOpt1,"lblFieldId_"+InputCtrlId_employeeNumberOpt1,"lblFieldIdWarning_"+InputCtrlId_employeeNumberOpt1);
			if(iReturnValidateValue=="1")
			{
				currentEmployment_Section=currentEmployment_Section+1;
				iReturnValidateTotalValue=iReturnValidateTotalValue+currentEmployment_Section;
			}
			
			var InputCtrlId_chkRetiredFrom="chkRetiredFrom";
			var chkRetiredFrom=getInputCtrlCheckBoxValue(InputCtrlId_chkRetiredFrom);
			iReturnValidateValue=validateInputCheckBoxControlStr(chkRetiredFrom,InputCtrlId_chkRetiredFrom,"lblFieldId_"+InputCtrlId_chkRetiredFrom,"lblFieldIdWarning_"+InputCtrlId_chkRetiredFrom,"hdn_"+InputCtrlId_chkRetiredFrom);
			if(iReturnValidateValue=="1")
			{
				currentEmployment_Section=currentEmployment_Section+1;
				iReturnValidateTotalValue=iReturnValidateTotalValue+currentEmployment_Section;
			}
			
			var InputCtrlId_RetirementDate="RetirementDate";
			RetirementDate=getInputCtrlTextValue(InputCtrlId_RetirementDate);
			iReturnValidateValue=validateInputTextControlStr(RetirementDate,InputCtrlId_RetirementDate,"lblFieldId_"+InputCtrlId_RetirementDate,"lblFieldIdWarning_"+InputCtrlId_RetirementDate);
			if(iReturnValidateValue=="1")
			{
				currentEmployment_Section=currentEmployment_Section+1;
				iReturnValidateTotalValue=iReturnValidateTotalValue+currentEmployment_Section;
			}
			
			var InputCtrlId_memberWithdrawnFunds="memberWithdrawnFunds";
			var memberWithdrawnFunds=getInputCtrlCheckBoxValue(InputCtrlId_memberWithdrawnFunds);
			iReturnValidateValue=validateInputCheckBoxControlStr(memberWithdrawnFunds,InputCtrlId_memberWithdrawnFunds,"lblFieldId_"+InputCtrlId_memberWithdrawnFunds,"lblFieldIdWarning_"+InputCtrlId_memberWithdrawnFunds,"hdn_"+InputCtrlId_memberWithdrawnFunds);
			if(iReturnValidateValue=="1")
			{
				currentEmployment_Section=currentEmployment_Section+1;
				iReturnValidateTotalValue=iReturnValidateTotalValue+currentEmployment_Section;
			}
			
			var InputCtrlId_dateOfWithdrawn="dateOfWithdrawn";
			dateOfWithdrawn=getInputCtrlTextValue(InputCtrlId_dateOfWithdrawn);
			iReturnValidateValue=validateInputTextControlStr(dateOfWithdrawn,InputCtrlId_dateOfWithdrawn,"lblFieldId_"+InputCtrlId_dateOfWithdrawn,"lblFieldIdWarning_"+InputCtrlId_dateOfWithdrawn);
			if(iReturnValidateValue=="1")
			{
				currentEmployment_Section=currentEmployment_Section+1;
				iReturnValidateTotalValue=iReturnValidateTotalValue+currentEmployment_Section;
			}
			
			//
			
			var InputCtrlId_noLongerEmployed="noLongerEmployed";
			noLongerEmployed=getTextAreaEditorCtrlTextValue(InputCtrlId_noLongerEmployed);
			iReturnValidateValue=validateInputTextAreaControlStr(noLongerEmployed,InputCtrlId_noLongerEmployed,"lblFieldId_"+InputCtrlId_noLongerEmployed,"lblFieldIdWarning_"+InputCtrlId_noLongerEmployed);
			if(iReturnValidateValue=="1")
			{
				currentEmployment_Section=currentEmployment_Section+1;
				iReturnValidateTotalValue=iReturnValidateTotalValue+currentEmployment_Section;
			}
			
			//
			
		}
		else if(employmentStatus=="1")
		{
			
			var InputCtrlId_employeeNumberOpt2="employeeNumberOpt2";
			employeeNumberOpt2=getInputCtrlTextValue(InputCtrlId_employeeNumberOpt2);
			iReturnValidateValue=validateInputTextControlStr(employeeNumberOpt2,InputCtrlId_employeeNumberOpt2,"lblFieldId_"+InputCtrlId_employeeNumberOpt2,"lblFieldIdWarning_"+InputCtrlId_employeeNumberOpt2);
			if(iReturnValidateValue=="1")
			{
				currentEmployment_Section=currentEmployment_Section+1;
				iReturnValidateTotalValue=iReturnValidateTotalValue+currentEmployment_Section;
			}
			
			
			var InputCtrlId_staffTypeOnExperiences="staffTypeOnExperiences";
			staffTypeOnExperiences=getInputCtrlRadioValue(InputCtrlId_staffTypeOnExperiences);
			if(staffTypeOnExperiences=="-1")
			{
				var rdchk_staffType_I="staffType_I";
				MarkedLabelAsRed(rdchk_staffType_I,"lblFieldId_"+rdchk_staffType_I,"lblFieldIdWarning_"+rdchk_staffType_I);
				
				var rdchk_staffType_PI="staffType_PI";
				MarkedLabelAsRed(rdchk_staffType_PI,"lblFieldId_"+rdchk_staffType_PI,"lblFieldIdWarning_"+rdchk_staffType_PI);
				
				var rdchk_staffTyp_Ne="staffTyp_Ne";
				MarkedLabelAsRed(rdchk_staffTyp_Ne,"lblFieldId_"+rdchk_staffTyp_Ne,"lblFieldIdWarning_"+rdchk_staffTyp_Ne);
			}
			else
			{
				var rdchk_staffType_I="staffType_I";
				UnMarkedLabelAsRed(rdchk_staffType_I,"lblFieldId_"+rdchk_staffType_I,"lblFieldIdWarning_"+rdchk_staffType_I);
				
				var rdchk_staffType_PI="staffType_PI";
				UnMarkedLabelAsRed(rdchk_staffType_PI,"lblFieldId_"+rdchk_staffType_PI,"lblFieldIdWarning_"+rdchk_staffType_PI);
				
				var rdchk_staffTyp_Ne="staffTyp_Ne";
				UnMarkedLabelAsRed(rdchk_staffTyp_Ne,"lblFieldId_"+rdchk_staffTyp_Ne,"lblFieldIdWarning_"+rdchk_staffTyp_Ne);
			}
			
		}
		else if(employmentStatus=="2")
		{
			// Nothing to do
		}
		
	}
	
	var currentEmployment_Section1=validateCustomAll(currentEmployment_Section_Name);
	iReturnValidateTotalValue=iReturnValidateTotalValue+currentEmployment_Section1;
	currentEmployment_Section=currentEmployment_Section+currentEmployment_Section1;
	
	if(currentEmployment_Section>0)
	{
		window.location.hash = '#lblSectionId_'+currentEmployment_Section_Name;
		$("#lblSectionId_"+currentEmployment_Section_Name).addClass("DSPQRequired12");
	}
	else
	{
		$("#lblSectionId_"+currentEmployment_Section_Name).removeClass("DSPQRequired12");
	}
	
	var employeeNumberOpt="";
	if(employeeNumberOpt1!=null && employeeNumberOpt1!="")
		employeeNumberOpt=employeeNumberOpt1;
	else if(employeeNumberOpt2!=null && employeeNumberOpt2!="")
		employeeNumberOpt=employeeNumberOpt2;
	
	var IsCurrentFullTimeTeacher=null;
	if(staffTypeOnExperiences!=null && staffTypeOnExperiences!="")
	{
		if(staffTypeOnExperiences=="PI")
			IsCurrentFullTimeTeacher=2;
		else if(staffTypeOnExperiences=="I")
			IsCurrentFullTimeTeacher=1;
		else if(staffTypeOnExperiences=="N")
			IsCurrentFullTimeTeacher=0;
	}
	
	//employmentStatus,IsCurrentFullTimeTeacher,employeeNumberOpt,RetirementDate,dateOfWithdrawn
	//alert("employmentStatus "+employmentStatus+" ,staffTypeOnExperiences "+staffTypeOnExperiences+" ["+IsCurrentFullTimeTeacher+"] ,RetirementDate "+RetirementDate+" ,dateOfWithdrawn "+dateOfWithdrawn+" ,employeeNumberOpt "+employeeNumberOpt);
	
	if(iReturnValidateTotalValue==0)
	{
		DSPQServiceAjax.updateProfessionalGroup(dspqPortfolioNameId,dspqct,jobId,isretired,retireddistrictId,stMForretire,retireNo,employmentStatus,IsCurrentFullTimeTeacher,employeeNumberOpt,RetirementDate,dateOfWithdrawn,noLongerEmployed,{ 
			async: false,
			errorHandler:handleError,
			callback:function(data)
			{
				$('#jafLoadingDiv').hide();
				var res = data.split("||");
				//alert("res[0] "+res[0] +" res[1] "+res[1]);
				if(res[0]=="OK" && res[1]=="")
				{
					var returnValue=0;
					
					//alert("dspqjobflowdistrictId "+dspqjobflowdistrictId);
					
					if(dspqjobflowdistrictId=="1200390")
					{
						returnValue=jafMiamiValidation();
						//alert("returnValue "+returnValue);
						if(returnValue > 0)
						{
							//try{$('#jobApplyOrNot').modal('show');}catch(e){}
							return false;
						}
						
					}
					
					if(returnValue==0)
					{
						if(inputValue=="0")
							getNextPage();
						else if(inputValue=="1")
							SaveAndExitPortfolioL2();
					}
					
				}
				else if(res[1]!="")
				{
						showProfessionalGroupError();
						$('#professionalGroupErrorMsg').empty();
						
						var res1 = res[1].split(",");
						for(var i=0;i<res1.length;i++)
						{
							if(res1[i]==13)
							{
								var SectionName13="tooltipSection13";
								$("#lblSectionId_"+SectionName13).addClass("DSPQRequired12");
								$('#professionalGroupErrorMsg').append("&#149;Please enter Employment History details.<br>");
							}
							else if(res1[i]==14)
							{
								var SectionName14="tooltipSection14";
								$("#lblSectionId_"+SectionName14).addClass("DSPQRequired12");
								$('#professionalGroupErrorMsg').append("&#149;Please enter Involvement/Volunteer Work/Student Teaching details.<br>");
							}
							else if(res1[i]==15)
							{
								var SectionName15="tooltipSection15";
								$("#lblSectionId_"+SectionName15).addClass("DSPQRequired12");
								$('#professionalGroupErrorMsg').append("&#149;Please enter Honors details.<br>");
							}
						}
				}
			}
		});
		$('#jafLoadingDiv').hide();	
		return true;
	}
	else
	{
		$('#jafLoadingDiv').hide();	
		return false;
	}
	$('#jafLoadingDiv').hide();	
}


function validateDSPQG4Section25(inputValue)
{
	$('#jafLoadingDiv').show();
	
	var jobId=""; 
	try { jobId=document.getElementById("dspqJobId").value; } catch (e) {}

	var dspqct=""; 
	try { dspqct=document.getElementById("dspqct").value; } catch (e) {}
	
	var dspqjobflowdistrictId="";
	try { dspqjobflowdistrictId=document.getElementById("dspqjobflowdistrictId").value; } catch (e) {}
	
	var dspqPortfolioNameId="";
	try { dspqPortfolioNameId=document.getElementById("dspqPortfolioNameId").value; } catch (e) {}
	
	var iReturnValidateTotalValue=0;
	var iReturnValidateValue=0;
	
	var affidavit_Section_Name="tooltipSection25";
	var affidavit_Section=0;
	var InputCtrlId_affidavit="affidavit";
	var affidavit=getInputCtrlCheckBoxValue(InputCtrlId_affidavit);
	iReturnValidateValue=validateInputCheckBoxControlStr(affidavit,InputCtrlId_affidavit,"lblFieldId_"+InputCtrlId_affidavit,"lblFieldIdWarning_"+InputCtrlId_affidavit,"hdn_"+InputCtrlId_affidavit);
	if(iReturnValidateValue=="1")
	{
		affidavit_Section=affidavit_Section+1;
		iReturnValidateTotalValue=iReturnValidateTotalValue+iReturnValidateValue;
	}
	
	if(affidavit_Section>0)
	{
		window.location.hash = '#lblSectionId_'+affidavit_Section_Name;
		$("#lblSectionId_"+affidavit_Section_Name).addClass("DSPQRequired12");
	}
	else
	{
		$("#lblSectionId_"+affidavit_Section_Name).removeClass("DSPQRequired12");
	}
	
	if(iReturnValidateTotalValue==0)
	{
		DSPQServiceAjax.updateGroup04Section25(dspqPortfolioNameId,dspqct,jobId,affidavit,{ 
			async: false,
			errorHandler:handleError,
			callback:function(data)
			{
				$('#jafLoadingDiv').hide();
				var res = data.split("||");
				if(res[0]=="OK" && res[1]=="")
				{
					if(inputValue=="0")
						getNextPage();
					else if(inputValue=="1")
						SaveAndExitPortfolioL2();
					
					//getNextPage();
				}
				else if(res[1]!="")
				{
						//alert("res[1] "+res[1]);
						showProfessionalGroupError();
						$('#professionalGroupErrorMsg').empty();
						
						var res1 = res[1].split(",");
						for(var i=0;i<res1.length;i++)
						{
							/*if(res1[i]==13)
							{
								var SectionName13="tooltipSection13";
								$("#lblSectionId_"+SectionName13).addClass("DSPQRequired12");
								$('#professionalGroupErrorMsg').append("&#149;Please enter Employment History details.<br>");
							}
							else if(res1[i]==14)
							{
								var SectionName14="tooltipSection14";
								$("#lblSectionId_"+SectionName14).addClass("DSPQRequired12");
								$('#professionalGroupErrorMsg').append("&#149;Please enter Involvement/Volunteer Work/Student Teaching details.<br>");
							}
							else if(res1[i]==15)
							{
								var SectionName15="tooltipSection15";
								$("#lblSectionId_"+SectionName15).addClass("DSPQRequired12");
								$('#professionalGroupErrorMsg').append("&#149;Please enter Honors details.<br>");
							}*/
						}
					
				}
			}
		});
		$('#jafLoadingDiv').hide();	
		return true;
	}
	else
	{
		$('#jafLoadingDiv').hide();	
		return false;
	}
	$('#jafLoadingDiv').hide();	
}

function saveResume(UploadedFileName)
{
	//alert("0 UploadedFileName "+UploadedFileName);
	uploadResume(UploadedFileName);	
}

function uploadResume(UploadedFileName)
{
	//alert("1 UploadedFileName "+UploadedFileName);
	isSetFocus=false;
	var iReturnValidateTotalValue=0;
	var iReturnValidateValue=0;
	
	if($("#resumeDiv").is(':visible'))
	{
		//alert("2");
		var Resume_Section=0;
		var Resume_Section_Name="tooltipSection12";
		
		var resume="";
		if(UploadedFileName==undefined)
		{
			//alert("3");
			var db_resume=getInputCtrlTextValue("db_resume");
			var InputCtrlId_resume="resume";
			resume=getInputCtrlTextValue(InputCtrlId_resume);
			iReturnValidateValue=validateInputFileControlStr(resume,InputCtrlId_resume,"lblFieldId_"+InputCtrlId_resume,"lblFieldIdWarning_"+InputCtrlId_resume,db_resume);
			if(iReturnValidateValue=="1")
			{
				Resume_Section=Resume_Section+1;
				iReturnValidateTotalValue=iReturnValidateTotalValue+Resume_Section;
			}
			
			if(Resume_Section>0)
			{
				
				//window.location.hash = '#lblSectionId_'+Resume_Section_Name;
				$("#lblSectionId_"+Resume_Section_Name).addClass("DSPQRequired12");
				Resume_Section=0;
			}
			else
			{
				$("#lblSectionId_"+Resume_Section_Name).removeClass("DSPQRequired12");
			}
		}	
			
			if(iReturnValidateTotalValue==0)
			{
				//$('#loadingDiv').show();
				if(resume!="")
				{

					document.getElementById("frmExpResumeUpload").submit();
				}
				else
				{
					//alert("UploadedFileName "+UploadedFileName);
					if(UploadedFileName==null || UploadedFileName=="" || UploadedFileName==undefined)
					{
						UploadedFileName=getInputCtrlTextValue("db_resume");
						
					}
					else
					{
						DSPQServiceAjax.UpdateResume(UploadedFileName,{
							async: false,
							errorHandler:handleError,
							callback:function(data)
							{
								if(data=="OK")
								{
									document.getElementById("frmExpResumeUpload").reset();
									$("a#hrefResume").html(UploadedFileName);
									iReturnValidateTotalValue="0";
								}
								else
									iReturnValidateTotalValue="1";
							}
							});
					}
					
				}
			}
	}
	return iReturnValidateTotalValue;
	
}

function validateInputTextControlStr(InputValue,Id1,Id2,Id3)
{
	var iReturnValidateValue=0;
	var InputValue_dspqRequired=0;
	try { InputValue_dspqRequired=$("#"+Id1).attr("dspqRequired"); } catch (e) {}
	if(InputValue_dspqRequired=="1")
	{
		//var InputValue_dspqRequired_dspqLabelName="";
		//try { InputValue_dspqRequired_dspqLabelName=$("#"+Id1).attr("dspqLabelName"); } catch (e) {}
		if(InputValue=="")
		{
			$("#"+Id2).addClass("DSPQRequired12");
			$("#"+Id3).addClass("icon-warning DSPQRequired12");
			$("#lblFieldIdWarningBeforeSpace_"+Id1).html("&nbsp;");
			$("#lblFieldIdWarningAfterSpace_"+Id1).html("&nbsp;");
			iReturnValidateValue=1;
			
			if(!isSetFocus)
			{
				$("#"+Id1).focus();
				isSetFocus=true;
			}
			
		}
		else
		{
			$("#"+Id2).removeClass("DSPQRequired12");
			$("#"+Id3).removeClass("icon-warning DSPQRequired12");
			
			$("#lblFieldIdWarningBeforeSpace_"+Id1).html("");
			$("#lblFieldIdWarningAfterSpace_"+Id1).html("");
			
			iReturnValidateValue=0;
		}
			
	}
	
	return iReturnValidateValue;
	
}


function validateInputTextControlStrWithMinLength(InputValue,Id1,Id2,Id3,minLength)
{
	var iReturnValidateValue=0;
	var InputValue_dspqRequired=0;
	try { InputValue_dspqRequired=$("#"+Id1).attr("dspqRequired"); } catch (e) {}
	if(InputValue_dspqRequired=="1")
	{
		//var InputValue_dspqRequired_dspqLabelName="";
		//try { InputValue_dspqRequired_dspqLabelName=$("#"+Id1).attr("dspqLabelName"); } catch (e) {}
		if(InputValue=="" || ( InputValue!="" && InputValue.length<minLength) )
		{
			$("#"+Id2).addClass("DSPQRequired12");
			$("#"+Id3).addClass("icon-warning DSPQRequired12");
			$("#lblFieldIdWarningBeforeSpace_"+Id1).html("&nbsp;");
			$("#lblFieldIdWarningAfterSpace_"+Id1).html("&nbsp;");
			iReturnValidateValue=1;
			
			if(!isSetFocus)
			{
				$("#"+Id1).focus();
				isSetFocus=true;
			}
			
		}
		else
		{
			$("#"+Id2).removeClass("DSPQRequired12");
			$("#"+Id3).removeClass("icon-warning DSPQRequired12");
			
			$("#lblFieldIdWarningBeforeSpace_"+Id1).html("");
			$("#lblFieldIdWarningAfterSpace_"+Id1).html("");
			
			iReturnValidateValue=0;
		}
			
	}
	
	return iReturnValidateValue;
	
}

function validateInputTextControlNumberWithMinLength(InputValue,Id1,Id2,Id3,minLength)
{
	var iReturnValidateValue=0;
	var InputValue_dspqRequired=0;
	try { InputValue_dspqRequired=$("#"+Id1).attr("dspqRequired"); } catch (e) {}
	if(InputValue_dspqRequired=="1")
	{
		//var InputValue_dspqRequired_dspqLabelName="";
		//try { InputValue_dspqRequired_dspqLabelName=$("#"+Id1).attr("dspqLabelName"); } catch (e) {}
		var iInputValue = new String(parseInt(trim(InputValue)));
		if(InputValue=="" || ( InputValue!="" && InputValue.length<minLength) || iInputValue=="NaN")
		{
			$("#"+Id2).addClass("DSPQRequired12");
			$("#"+Id3).addClass("icon-warning DSPQRequired12");
			$("#lblFieldIdWarningBeforeSpace_"+Id1).html("&nbsp;");
			$("#lblFieldIdWarningAfterSpace_"+Id1).html("&nbsp;");
			iReturnValidateValue=1;
			
			if(!isSetFocus)
			{
				$("#"+Id1).focus();
				isSetFocus=true;
			}
			
		}
		else
		{
			$("#"+Id2).removeClass("DSPQRequired12");
			$("#"+Id3).removeClass("icon-warning DSPQRequired12");
			
			$("#lblFieldIdWarningBeforeSpace_"+Id1).html("");
			$("#lblFieldIdWarningAfterSpace_"+Id1).html("");
			
			iReturnValidateValue=0;
		}
			
	}
	
	return iReturnValidateValue;
	
}

function validateInputTextControlNumber(InputValue,Id1,Id2,Id3)
{
	var iReturnValidateValue=0;
	var InputValue_dspqRequired=0;
	try { InputValue_dspqRequired=$("#"+Id1).attr("dspqRequired"); } catch (e) {}
	if(InputValue_dspqRequired=="1")
	{
		//var InputValue_dspqRequired_dspqLabelName="";
		//try { InputValue_dspqRequired_dspqLabelName=$("#"+Id1).attr("dspqLabelName"); } catch (e) {}
		var iInputValue = new String(parseInt(trim(InputValue)));
		if(InputValue=="" || InputValue=="0" || InputValue=="0.0" || iInputValue=="NaN" || iInputValue < 0 || iInputValue == 0)
		{
			$("#"+Id2).addClass("DSPQRequired12");
			$("#"+Id3).addClass("icon-warning DSPQRequired12");
			$("#lblFieldIdWarningBeforeSpace_"+Id1).html("&nbsp;");
			$("#lblFieldIdWarningAfterSpace_"+Id1).html("&nbsp;");
			iReturnValidateValue=1;
			
			if(!isSetFocus)
			{
				$("#"+Id1).focus();
				isSetFocus=true;
			}
			
		}
		else
		{
			$("#"+Id2).removeClass("DSPQRequired12");
			$("#"+Id3).removeClass("icon-warning DSPQRequired12");
			
			$("#lblFieldIdWarningBeforeSpace_"+Id1).html("");
			$("#lblFieldIdWarningAfterSpace_"+Id1).html("");
			
			iReturnValidateValue=0;
		}
			
	}
	
	return iReturnValidateValue;
	
}

function validateInputTextControlStrEmailAddress(InputValue,Id1,Id2,Id3)
{
	var iReturnValidateValue=0;
	var InputValue_dspqRequired=0;
	try { InputValue_dspqRequired=$("#"+Id1).attr("dspqRequired"); } catch (e) {}
	if(InputValue_dspqRequired=="1")
	{
		//var InputValue_dspqRequired_dspqLabelName="";
		//try { InputValue_dspqRequired_dspqLabelName=$("#"+Id1).attr("dspqLabelName"); } catch (e) {}
		if(InputValue=="")
		{
			$("#"+Id2).addClass("DSPQRequired12");
			$("#"+Id3).addClass("icon-warning DSPQRequired12");
			$("#lblFieldIdWarningBeforeSpace_"+Id1).html("&nbsp;");
			$("#lblFieldIdWarningAfterSpace_"+Id1).html("&nbsp;");
			iReturnValidateValue=1;
			
			if(!isSetFocus)
			{
				$("#"+Id1).focus();
				isSetFocus=true;
			}
			
		}
		else if(InputValue!="")
		{
			if(!isEmailAddress(InputValue))
			{
				$("#"+Id2).addClass("DSPQRequired12");
				$("#"+Id3).addClass("icon-warning DSPQRequired12");
				$("#lblFieldIdWarningBeforeSpace_"+Id1).html("&nbsp;");
				$("#lblFieldIdWarningAfterSpace_"+Id1).html("&nbsp;");
				iReturnValidateValue=1;
				
				if(!isSetFocus)
				{
					$("#"+Id1).focus();
					isSetFocus=true;
				}
				
			}
			else
			{
				$("#"+Id2).removeClass("DSPQRequired12");
				$("#"+Id3).removeClass("icon-warning DSPQRequired12");
				
				$("#lblFieldIdWarningBeforeSpace_"+Id1).html("");
				$("#lblFieldIdWarningAfterSpace_"+Id1).html("");
				
				iReturnValidateValue=0;
			}
		}
		else
		{
			$("#"+Id2).removeClass("DSPQRequired12");
			$("#"+Id3).removeClass("icon-warning DSPQRequired12");
			
			$("#lblFieldIdWarningBeforeSpace_"+Id1).html("");
			$("#lblFieldIdWarningAfterSpace_"+Id1).html("");
			
			iReturnValidateValue=0;
		}
			
	}
	
	
	return iReturnValidateValue;
	
}

function validateInputTextControlStrAutoSuggest(InputValue,Id1,Id2,Id3,FocusControl)
{
	var iReturnValidateValue=0;
	var InputValue_dspqRequired=0;
	try { InputValue_dspqRequired=$("#"+Id1).attr("dspqRequired"); } catch (e) {}
	if(InputValue_dspqRequired=="1")
	{
		//var InputValue_dspqRequired_dspqLabelName="";
		//try { InputValue_dspqRequired_dspqLabelName=$("#"+Id1).attr("dspqLabelName"); } catch (e) {}
		if(InputValue=="")
		{
			$("#"+Id2).addClass("DSPQRequired12");
			$("#"+Id3).addClass("icon-warning DSPQRequired12");
			$("#lblFieldIdWarningBeforeSpace_"+FocusControl).html("&nbsp;");
			$("#lblFieldIdWarningAfterSpace_"+FocusControl).html("&nbsp;");
			iReturnValidateValue=1;
			//alert("isSetFocus "+isSetFocus);
			if(!isSetFocus)
			{
				//alert("yes");
				$("#"+FocusControl).focus();
				isSetFocus=true;
			}
			
		}
		else
		{
			$("#"+Id2).removeClass("DSPQRequired12");
			$("#"+Id3).removeClass("icon-warning DSPQRequired12");
			
			$("#lblFieldIdWarningBeforeSpace_"+FocusControl).html("");
			$("#lblFieldIdWarningAfterSpace_"+FocusControl).html("");
			
			iReturnValidateValue=0;
		}
			
	}
	
	return iReturnValidateValue;
	
}

function validateCustomFldControlStr(ShortCode,InputValue1,InputValue2,Required,CustomFieldAutoId,SectionNameId,CustFldCounter)
{
	return validateCustomFldFileUploadControlStr(ShortCode, InputValue1, InputValue2, Required, CustomFieldAutoId, SectionNameId, CustFldCounter,"",null);
}

function validateCustomFldFileUploadControlStr(ShortCode,InputValue1,InputValue2,Required,CustomFieldAutoId,SectionNameId,CustFldCounter,InputValue3,InputCtrlFileUploadName)
{
	var iReturnValidateValue=0;
	if(Required=="1")
	{
		if(ShortCode=="et" || ShortCode=="ml")
		{
			if(InputValue1=="")
			{
				$("#lblCustomFieldId_"+SectionNameId+"_"+CustFldCounter).addClass("DSPQRequired12");
				$("#lblCustomFieldIdWarning_"+CustomFieldAutoId).addClass("icon-warning DSPQRequired12");
				$("#lblCustomFieldIdWarningBeforeSpace_"+CustomFieldAutoId).html("&nbsp;");
				$("#lblCustomFieldIdWarningAfterSpace_"+CustomFieldAutoId).html("&nbsp;");
				iReturnValidateValue=1;
				
				if(!isSetFocus)
				{
					//$("#"+CustomFieldAutoId).focus();
					$("#divOpt_"+CustomFieldAutoId+"_"+CustFldCounter).focus();
					isSetFocus=true;
				}
				
			}
			else
			{
				$("#lblCustomFieldId_"+SectionNameId+"_"+CustFldCounter).removeClass("DSPQRequired12");
				$("#lblCustomFieldIdWarning_"+CustomFieldAutoId).removeClass("icon-warning DSPQRequired12");
				
				$("#lblCustomFieldIdWarningBeforeSpace_"+CustomFieldAutoId).html("");
				$("#lblCustomFieldIdWarningAfterSpace_"+CustomFieldAutoId).html("");
				
				iReturnValidateValue=0;
			}
		}
		else if(ShortCode=="mlsel")
		{
			if(InputValue1=="")
			{
				$("#lblCustomFieldId_"+SectionNameId+"_"+CustFldCounter).addClass("DSPQRequired12");
				$("#lblCustomFieldIdWarning_"+CustomFieldAutoId).addClass("icon-warning DSPQRequired12");
				$("#lblCustomFieldIdWarningBeforeSpace_"+CustomFieldAutoId).html("&nbsp;");
				$("#lblCustomFieldIdWarningAfterSpace_"+CustomFieldAutoId).html("&nbsp;");
				iReturnValidateValue=1;
				
				if(!isSetFocus)
				{
					$("#opt_"+CustomFieldAutoId+"_"+CustFldCounter).focus();
					isSetFocus=true;
				}
				
			}
			else
			{
				$("#lblCustomFieldId_"+SectionNameId+"_"+CustFldCounter).removeClass("DSPQRequired12");
				$("#lblCustomFieldIdWarning_"+CustomFieldAutoId).removeClass("icon-warning DSPQRequired12");
				
				$("#lblCustomFieldIdWarningBeforeSpace_"+CustomFieldAutoId).html("");
				$("#lblCustomFieldIdWarningAfterSpace_"+CustomFieldAutoId).html("");
				
				iReturnValidateValue=0;
			}
		}
		else if(ShortCode=="mloet")
		{
			if(InputValue1=="" || InputValue2=="")
			{
				$("#lblCustomFieldId_"+SectionNameId+"_"+CustFldCounter).addClass("DSPQRequired12");
				$("#lblCustomFieldIdWarning_"+CustomFieldAutoId).addClass("icon-warning DSPQRequired12");
				$("#lblCustomFieldIdWarningBeforeSpace_"+CustomFieldAutoId).html("&nbsp;");
				$("#lblCustomFieldIdWarningAfterSpace_"+CustomFieldAutoId).html("&nbsp;");
				iReturnValidateValue=1;
				
				if(!isSetFocus)
				{
					$("#opt_"+CustomFieldAutoId+"_"+CustFldCounter).focus();
					isSetFocus=true;
				}
				
			}
			else
			{
				$("#lblCustomFieldId_"+SectionNameId+"_"+CustFldCounter).removeClass("DSPQRequired12");
				$("#lblCustomFieldIdWarning_"+CustomFieldAutoId).removeClass("icon-warning DSPQRequired12");
				
				$("#lblCustomFieldIdWarningBeforeSpace_"+CustomFieldAutoId).html("");
				$("#lblCustomFieldIdWarningAfterSpace_"+CustomFieldAutoId).html("");
				
				iReturnValidateValue=0;
			}
		}
		else if(ShortCode=="sloet")
		{
			if(InputValue1=="-1" || InputValue2=="")
			{
				$("#lblCustomFieldId_"+SectionNameId+"_"+CustFldCounter).addClass("DSPQRequired12");
				$("#lblCustomFieldIdWarning_"+CustomFieldAutoId).addClass("icon-warning DSPQRequired12");
				$("#lblCustomFieldIdWarningBeforeSpace_"+CustomFieldAutoId).html("&nbsp;");
				$("#lblCustomFieldIdWarningAfterSpace_"+CustomFieldAutoId).html("&nbsp;");
				iReturnValidateValue=1;
				
				if(!isSetFocus)
				{
					$("#opt_"+CustomFieldAutoId+"_"+CustFldCounter).focus();
					isSetFocus=true;
				}
				
			}
			else
			{
				$("#lblCustomFieldId_"+SectionNameId+"_"+CustFldCounter).removeClass("DSPQRequired12");
				$("#lblCustomFieldIdWarning_"+CustomFieldAutoId).removeClass("icon-warning DSPQRequired12");
				
				$("#lblCustomFieldIdWarningBeforeSpace_"+CustomFieldAutoId).html("");
				$("#lblCustomFieldIdWarningAfterSpace_"+CustomFieldAutoId).html("");
				
				iReturnValidateValue=0;
			}
		}
		else if(ShortCode=="tf" || ShortCode=="slsel")
		{
			if(InputValue1=="-1")
			{
				$("#lblCustomFieldId_"+SectionNameId+"_"+CustFldCounter).addClass("DSPQRequired12");
				$("#lblCustomFieldIdWarning_"+CustomFieldAutoId).addClass("icon-warning DSPQRequired12");
				$("#lblCustomFieldIdWarningBeforeSpace_"+CustomFieldAutoId).html("&nbsp;");
				$("#lblCustomFieldIdWarningAfterSpace_"+CustomFieldAutoId).html("&nbsp;");
				iReturnValidateValue=1;
				
				if(!isSetFocus)
				{
					$("#opt_"+CustomFieldAutoId+"_"+CustFldCounter).focus();
					isSetFocus=true;
				}
				
			}
			else
			{
				$("#lblCustomFieldId_"+SectionNameId+"_"+CustFldCounter).removeClass("DSPQRequired12");
				$("#lblCustomFieldIdWarning_"+CustomFieldAutoId).removeClass("icon-warning DSPQRequired12");
				
				$("#lblCustomFieldIdWarningBeforeSpace_"+CustomFieldAutoId).html("");
				$("#lblCustomFieldIdWarningAfterSpace_"+CustomFieldAutoId).html("");
				
				iReturnValidateValue=0;
			}
		}
		else if(ShortCode=="drsls" || ShortCode=="DD")
		{
			if(InputValue1=="0")
			{
				$("#lblCustomFieldId_"+SectionNameId+"_"+CustFldCounter).addClass("DSPQRequired12");
				$("#lblCustomFieldIdWarning_"+CustomFieldAutoId).addClass("icon-warning DSPQRequired12");
				$("#lblCustomFieldIdWarningBeforeSpace_"+CustomFieldAutoId).html("&nbsp;");
				$("#lblCustomFieldIdWarningAfterSpace_"+CustomFieldAutoId).html("&nbsp;");
				iReturnValidateValue=1;
				
				if(!isSetFocus)
				{
					$("#opt_"+CustomFieldAutoId+"_"+CustFldCounter).focus();
					isSetFocus=true;
				}
				
			}
			else
			{
				$("#lblCustomFieldId_"+SectionNameId+"_"+CustFldCounter).removeClass("DSPQRequired12");
				$("#lblCustomFieldIdWarning_"+CustomFieldAutoId).removeClass("icon-warning DSPQRequired12");
				
				$("#lblCustomFieldIdWarningBeforeSpace_"+CustomFieldAutoId).html("");
				$("#lblCustomFieldIdWarningAfterSpace_"+CustomFieldAutoId).html("");
				
				iReturnValidateValue=0;
			}
		}
		else if(ShortCode=="dt" || ShortCode=="sl")
		{
			if(InputValue1=="")
			{
				$("#lblCustomFieldId_"+SectionNameId+"_"+CustFldCounter).addClass("DSPQRequired12");
				$("#lblCustomFieldIdWarning_"+CustomFieldAutoId).addClass("icon-warning DSPQRequired12");
				$("#lblCustomFieldIdWarningBeforeSpace_"+CustomFieldAutoId).html("&nbsp;");
				$("#lblCustomFieldIdWarningAfterSpace_"+CustomFieldAutoId).html("&nbsp;");
				iReturnValidateValue=1;
				//alert("isSetFocus1 "+isSetFocus +" CustomFieldAutoId "+CustomFieldAutoId);
				if(!isSetFocus)
				{
					$("#opt_"+CustomFieldAutoId+"_"+CustFldCounter).focus();
					isSetFocus=true;
					//alert("isSetFocus2 "+isSetFocus);
				}
				
			}
			else
			{
				$("#lblCustomFieldId_"+SectionNameId+"_"+CustFldCounter).removeClass("DSPQRequired12");
				$("#lblCustomFieldIdWarning_"+CustomFieldAutoId).removeClass("icon-warning DSPQRequired12");
				
				$("#lblCustomFieldIdWarningBeforeSpace_"+CustomFieldAutoId).html("");
				$("#lblCustomFieldIdWarningAfterSpace_"+CustomFieldAutoId).html("");
				
				iReturnValidateValue=0;
			}
		}
		else if(ShortCode=="UAT")
		{
			var tempReturnaValue=validateCustFileName(InputCtrlFileUploadName);
			if(tempReturnaValue=="1")
			{
				$("#lblCustomFieldId_"+SectionNameId+"_"+CustFldCounter).addClass("DSPQRequired12");
				$("#lblCustomFieldIdWarning_"+CustomFieldAutoId).addClass("icon-warning DSPQRequired12");
				$("#lblCustomFieldIdWarningBeforeSpace_"+CustomFieldAutoId).html("&nbsp;");
				$("#lblCustomFieldIdWarningAfterSpace_"+CustomFieldAutoId).html("&nbsp;");
				iReturnValidateValue=1;
				if(!isSetFocus)
				{
					$("#opt_"+CustomFieldAutoId+"_"+CustFldCounter).focus();
					isSetFocus=true;
				}
				
			}
			else if((InputValue1=="" && InputValue3=="") || tempReturnaValue=="1")
			{
				$("#lblCustomFieldId_"+SectionNameId+"_"+CustFldCounter).addClass("DSPQRequired12");
				$("#lblCustomFieldIdWarning_"+CustomFieldAutoId).addClass("icon-warning DSPQRequired12");
				$("#lblCustomFieldIdWarningBeforeSpace_"+CustomFieldAutoId).html("&nbsp;");
				$("#lblCustomFieldIdWarningAfterSpace_"+CustomFieldAutoId).html("&nbsp;");
				iReturnValidateValue=1;
				if(!isSetFocus)
				{
					$("#opt_"+CustomFieldAutoId+"_"+CustFldCounter).focus();
					isSetFocus=true;
				}
				
			}
			else
			{
				
				$("#lblCustomFieldId_"+SectionNameId+"_"+CustFldCounter).removeClass("DSPQRequired12");
				$("#lblCustomFieldIdWarning_"+CustomFieldAutoId).removeClass("icon-warning DSPQRequired12");
				
				$("#lblCustomFieldIdWarningBeforeSpace_"+CustomFieldAutoId).html("");
				$("#lblCustomFieldIdWarningAfterSpace_"+CustomFieldAutoId).html("");
				
				iReturnValidateValue=0;
				
			}
		}
		else if(ShortCode=="UATEF")
		{
			var sTempFileName="";
			if(InputValue1!="")
			{
				sTempFileName=InputValue1;
			}
			else if(InputValue3!="")
			{
				sTempFileName=InputValue3;
			}
			
			var tempReturnaValue=validateCustFileName(InputCtrlFileUploadName);
			if(tempReturnaValue=="1")
			{
				$("#lblCustomFieldId_"+SectionNameId+"_"+CustFldCounter).addClass("DSPQRequired12");
				$("#lblCustomFieldIdWarning_"+CustomFieldAutoId).addClass("icon-warning DSPQRequired12");
				$("#lblCustomFieldIdWarningBeforeSpace_"+CustomFieldAutoId).html("&nbsp;");
				$("#lblCustomFieldIdWarningAfterSpace_"+CustomFieldAutoId).html("&nbsp;");
				iReturnValidateValue=1;
				if(!isSetFocus)
				{
					$("#opt_"+CustomFieldAutoId+"_"+CustFldCounter).focus();
					isSetFocus=true;
				}
				
			}
			else if(InputValue2==undefined || InputValue2=="" || sTempFileName=="" || sTempFileName=="1")
			{
				$("#lblCustomFieldId_"+SectionNameId+"_"+CustFldCounter).addClass("DSPQRequired12");
				$("#lblCustomFieldIdWarning_"+CustomFieldAutoId).addClass("icon-warning DSPQRequired12");
				$("#lblCustomFieldIdWarningBeforeSpace_"+CustomFieldAutoId).html("&nbsp;");
				$("#lblCustomFieldIdWarningAfterSpace_"+CustomFieldAutoId).html("&nbsp;");
				iReturnValidateValue=1;
				if(!isSetFocus)
				{
					$("#opt_"+CustomFieldAutoId+"_"+CustFldCounter).focus();
					isSetFocus=true;
				}
				
			}
			else
			{
				$("#lblCustomFieldId_"+SectionNameId+"_"+CustFldCounter).removeClass("DSPQRequired12");
				$("#lblCustomFieldIdWarning_"+CustomFieldAutoId).removeClass("icon-warning DSPQRequired12");
				
				$("#lblCustomFieldIdWarningBeforeSpace_"+CustomFieldAutoId).html("");
				$("#lblCustomFieldIdWarningAfterSpace_"+CustomFieldAutoId).html("");
				
				iReturnValidateValue=0;
			}
		}
		else if(ShortCode=="OSONP")
		{
			if(InputValue1=="-1")
			{
				$("#lblCustomFieldId_"+SectionNameId+"_"+CustFldCounter).addClass("DSPQRequired12");
				$("#lblCustomFieldIdWarning_"+CustomFieldAutoId).addClass("icon-warning DSPQRequired12");
				$("#lblCustomFieldIdWarningBeforeSpace_"+CustomFieldAutoId).html("&nbsp;");
				$("#lblCustomFieldIdWarningAfterSpace_"+CustomFieldAutoId).html("&nbsp;");
				iReturnValidateValue=1;
				
				if(!isSetFocus)
				{
					$("#opt_"+CustomFieldAutoId+"_"+CustFldCounter).focus();
					isSetFocus=true;
				}
				
			}
			else
			{
				$("#lblCustomFieldId_"+SectionNameId+"_"+CustFldCounter).removeClass("DSPQRequired12");
				$("#lblCustomFieldIdWarning_"+CustomFieldAutoId).removeClass("icon-warning DSPQRequired12");
				
				$("#lblCustomFieldIdWarningBeforeSpace_"+CustomFieldAutoId).html("");
				$("#lblCustomFieldIdWarningAfterSpace_"+CustomFieldAutoId).html("");
				
				iReturnValidateValue=0;
			}
		}
		else if(ShortCode=="sswc")
		{
			if(InputValue1=="-1")
			{
				$("#lblCustomFieldId_"+SectionNameId+"_"+CustFldCounter).addClass("DSPQRequired12");
				$("#lblCustomFieldIdWarning_"+CustomFieldAutoId).addClass("icon-warning DSPQRequired12");
				$("#lblCustomFieldIdWarningBeforeSpace_"+CustomFieldAutoId).html("&nbsp;");
				$("#lblCustomFieldIdWarningAfterSpace_"+CustomFieldAutoId).html("&nbsp;");
				iReturnValidateValue=1;
				
				if(!isSetFocus)
				{
					$("#opt_"+CustomFieldAutoId+"_"+CustFldCounter).focus();
					isSetFocus=true;
				}
				
			}
			else
			{
				$("#lblCustomFieldId_"+SectionNameId+"_"+CustFldCounter).removeClass("DSPQRequired12");
				$("#lblCustomFieldIdWarning_"+CustomFieldAutoId).removeClass("icon-warning DSPQRequired12");
				
				$("#lblCustomFieldIdWarningBeforeSpace_"+CustomFieldAutoId).html("");
				$("#lblCustomFieldIdWarningAfterSpace_"+CustomFieldAutoId).html("");
				
				iReturnValidateValue=0;
			}
		}
		
	}
	
	return iReturnValidateValue;
	
}

function validateInputTextPhoneOrMobileControlStr(InputValue1,InputValue2,InputValue3,Id1_1,Id1_2,Id1_3,Id2,Id3,Id1MaxSize,Id2MaxSize,Id3MaxSize)
{
	var iReturnValidateValue=0;
	var InputValue_dspqRequired=0;
	try { InputValue_dspqRequired=$("#"+Id1_1).attr("dspqRequired"); } catch (e) {}
	if(InputValue_dspqRequired=="1")
	{
		//var InputValue_dspqRequired_dspqLabelName="";
		//try { InputValue_dspqRequired_dspqLabelName=$("#"+Id1).attr("dspqLabelName"); } catch (e) {}
		if(InputValue1=="" || InputValue2=="" || InputValue3=="")
		{
			$("#"+Id2).addClass("DSPQRequired12");
			$("#"+Id3).addClass("icon-warning DSPQRequired12");
			$("#lblFieldIdWarningBeforeSpace_"+Id1_1).html("&nbsp;");
			$("#lblFieldIdWarningAfterSpace_"+Id1_1).html("&nbsp;");
			iReturnValidateValue=1;
			
			if(InputValue1=="")
			{
				if(!isSetFocus)
				{
					$("#"+Id1_1).focus();
					isSetFocus=true;
				}
			}
			else if(InputValue2=="")
			{
				if(!isSetFocus)
				{
					$("#"+Id1_2).focus();
					isSetFocus=true;
				}
			}
			else if(InputValue3=="")
			{
				if(!isSetFocus)
				{
					$("#"+Id1_3).focus();
					isSetFocus=true;
				}
			}
			
		}
		else
		{
			if(InputValue1.length!=Id1MaxSize || InputValue2.length!=Id2MaxSize || InputValue3.length!=Id3MaxSize)
			{
				if(InputValue1.length!=Id1MaxSize)
				{
					$("#"+Id2).addClass("DSPQRequired12");
					$("#"+Id3).addClass("icon-warning DSPQRequired12");
					$("#lblFieldIdWarningBeforeSpace_"+Id1_1).html("&nbsp;");
					$("#lblFieldIdWarningAfterSpace_"+Id1_1).html("&nbsp;");
					iReturnValidateValue=1;
					
					if(!isSetFocus)
					{
						$("#"+Id1_1).focus();
						isSetFocus=true;
					}
					
				}
				else if(InputValue2.length!=Id2MaxSize)
				{
					$("#"+Id2).addClass("DSPQRequired12");
					$("#"+Id3).addClass("icon-warning DSPQRequired12");
					$("#lblFieldIdWarningBeforeSpace_"+Id1_1).html("&nbsp;");
					$("#lblFieldIdWarningAfterSpace_"+Id1_1).html("&nbsp;");
					iReturnValidateValue=1;
					
					if(!isSetFocus)
					{
						$("#"+Id1_2).focus();
						isSetFocus=true;
					}

				}
				else if(InputValue3.length!=Id3MaxSize)
				{
					$("#"+Id2).addClass("DSPQRequired12");
					$("#"+Id3).addClass("icon-warning DSPQRequired12");
					$("#lblFieldIdWarningBeforeSpace_"+Id1_1).html("&nbsp;");
					$("#lblFieldIdWarningAfterSpace_"+Id1_1).html("&nbsp;");
					iReturnValidateValue=1;
					
					if(!isSetFocus)
					{
						$("#"+Id1_3).focus();
						isSetFocus=true;
					}
					
				}
			}
			else
			{
				$("#"+Id2).removeClass("DSPQRequired12");
				$("#"+Id3).removeClass("icon-warning DSPQRequired12");
				
				$("#lblFieldIdWarningBeforeSpace_"+Id1_1).html("");
				$("#lblFieldIdWarningAfterSpace_"+Id1_1).html("");
				
				iReturnValidateValue=0;
			}
		}
			
	}
	
	return iReturnValidateValue;
	
}

function validateInputListControlStr(InputValue,Id1,Id2,Id3)
{
	var iReturnValidateValue=0;
	var InputValue_dspqRequired=0;
	try { InputValue_dspqRequired=$("#"+Id1).attr("dspqRequired"); } catch (e) {}
	
	if(InputValue_dspqRequired=="1")
	{
		//var InputValue_dspqRequired_dspqLabelName="";
		//try { InputValue_dspqRequired_dspqLabelName=$("#"+Id1).attr("dspqLabelName"); } catch (e) {}
		
		if(InputValue=="0")
		{
			$("#"+Id2).addClass("DSPQRequired12");
			$("#"+Id3).addClass("icon-warning DSPQRequired12");
			$("#lblFieldIdWarningBeforeSpace_"+Id1).html("&nbsp;");
			$("#lblFieldIdWarningAfterSpace_"+Id1).html("&nbsp;");
			iReturnValidateValue=1;
			
			//alert("isSetFocus1 "+isSetFocus);
			if(!isSetFocus)
			{
				$("#"+Id1).focus();
				isSetFocus=true;
				//alert("isSetFocus2 "+isSetFocus);
			}
			
		}
		else
		{
			$("#"+Id2).removeClass("DSPQRequired12");
			$("#"+Id3).removeClass("icon-warning DSPQRequired12");
			
			$("#lblFieldIdWarningBeforeSpace_"+Id1).html("");
			$("#lblFieldIdWarningAfterSpace_"+Id1).html("");
			
			iReturnValidateValue=0;
		}
			
	}
	
	return iReturnValidateValue;
	
}

function validateInputRadioControlStr(InputValue,Id1,Id2,Id3,Id4HiddenCtr)
{
	var iReturnValidateValue=0;
	var InputValue_dspqRequired=0;
	try { InputValue_dspqRequired=$("#"+Id4HiddenCtr).attr("dspqRequired"); } catch (e) {}
	
	if(InputValue_dspqRequired=="1")
	{
		//var InputValue_dspqRequired_dspqLabelName="";
		//try { InputValue_dspqRequired_dspqLabelName=$("#"+Id1).attr("dspqLabelName"); } catch (e) {}
		
		if(InputValue=="-1")
		{
			$("#"+Id2).addClass("DSPQRequired12");
			$("#"+Id3).addClass("icon-warning DSPQRequired12");
			$("#lblFieldIdWarningBeforeSpace_"+Id1).html("&nbsp;");
			$("#lblFieldIdWarningAfterSpace_"+Id1).html("&nbsp;");
			iReturnValidateValue=1;
		}
		else
		{
			$("#"+Id2).removeClass("DSPQRequired12");
			$("#"+Id3).removeClass("icon-warning DSPQRequired12");
			
			$("#lblFieldIdWarningBeforeSpace_"+Id1).html("");
			$("#lblFieldIdWarningAfterSpace_"+Id1).html("");
			
			iReturnValidateValue=0;
		}
			
	}
	
	return iReturnValidateValue;
	
}


function validateInputCheckBoxControlStr(InputValue,Id1,Id2,Id3,Id4HiddenCtr)
{
	var iReturnValidateValue=0;
	var InputValue_dspqRequired=0;
	try { InputValue_dspqRequired=$("#"+Id4HiddenCtr).attr("dspqRequired"); } catch (e) {}
	
	if(InputValue_dspqRequired=="1")
	{
		//var InputValue_dspqRequired_dspqLabelName="";
		//try { InputValue_dspqRequired_dspqLabelName=$("#"+Id1).attr("dspqLabelName"); } catch (e) {}
		
		if(InputValue=="")
		{
			$("#"+Id2).addClass("DSPQRequired12");
			$("#"+Id3).addClass("icon-warning DSPQRequired12");
			$("#lblFieldIdWarningBeforeSpace_"+Id1).html("&nbsp;");
			$("#lblFieldIdWarningAfterSpace_"+Id1).html("&nbsp;");
			iReturnValidateValue=1;
		}
		else
		{
			$("#"+Id2).removeClass("DSPQRequired12");
			$("#"+Id3).removeClass("icon-warning DSPQRequired12");
			
			$("#lblFieldIdWarningBeforeSpace_"+Id1).html("");
			$("#lblFieldIdWarningAfterSpace_"+Id1).html("");
			
			iReturnValidateValue=0;
		}
			
	}
	
	return iReturnValidateValue;
	
}

function getInputCtrlTextValue(InputCtrlTextId)
{
	var inputCtrlValue="";
	try { inputCtrlValue=trim($("#"+InputCtrlTextId).val()); } catch (e) {}
	return inputCtrlValue;
}

function getTextAreaEditorCtrlTextValue_CustFld(InputCtrlId)
{
	var inputCtrlValue="";
	try { inputCtrlValue=trim($('#divOpt_'+InputCtrlId).find(".jqte_editor").text().trim()); } catch (e) {}
	if(inputCtrlValue==null || inputCtrlValue=="")
	{
		try { inputCtrlValue=$('#txtAreaOpt_'+InputCtrlId).val(); } catch (e) {}
	}
	
	//try { inputCtrlValue=trim($('#divOpt_'+InputCtrlId).find(".jqte_editor").html().trim()); } catch (e) {}
	
	/*if(inputCtrlValue==null || inputCtrlValue=="")
	{
		inputCtrlValue = document.getElementsByName("opt_"+InputCtrlId);
	}*/
	return inputCtrlValue;
}

function getInputCtrlRadioValue(InputCtrlTextId)
{
	var inputCtrlValue="-1";
	try {
		var elements = document.getElementsByName(InputCtrlTextId);
		for (i=0;i<elements.length;i++) 
		{
		  if(elements[i].checked) 
		  {
			  inputCtrlValue=elements[i].value;
		  }
		}
	} catch (e) {}
	
	return inputCtrlValue;
}

function getInputCtrlCheckBoxValue(InputCtrlTextId)
{
	var inputCtrlValue="";
	try {
		var elements = document.getElementsByName(InputCtrlTextId);
		for (i=0;i<elements.length;i++) 
		{
		  if(elements[i].checked) 
		  {
			  if(inputCtrlValue=="")
				  inputCtrlValue=elements[i].value;
			  else
				  inputCtrlValue=inputCtrlValue+","+elements[i].value;
		  }
		}
	} catch (e) {}
	
	return inputCtrlValue;
}
function getCheckedValueCounter(InputCtrlTextId)
{
	var inputCtrlValue="";
	try {
		var elements = document.getElementsByName(InputCtrlTextId);
		for (i=0;i<elements.length;i++) 
		{
		  if(elements[i].checked) 
		  {
			  if(inputCtrlValue=="")
				  inputCtrlValue=$(elements[i]).attr('textValue');
			  else
				  inputCtrlValue=inputCtrlValue+","+$(elements[i]).attr('textValue');
		  }
		}
	} catch (e) {}
	
	return inputCtrlValue;
	
}

function getInputCtrlCheckBoxValueGrade(InputCtrlTextId)
{
	var inputCtrlValue=false;
	try {
		var elements = document.getElementsByName(InputCtrlTextId);
		for (i=0;i<elements.length;i++) 
		{
		  if(elements[i].checked) 
		  {
				  inputCtrlValue=true;
		  }
		}
	} catch (e) {}
	
	return inputCtrlValue;
}

function MarkedLabelAsRed(Id1,Id2,Id3)
{
	try { $("#"+Id2).addClass("DSPQRequired12"); } catch (e) {}
	try { $("#"+Id3).addClass("icon-warning DSPQRequired12"); } catch (e) {}
	try { $("#lblFieldIdWarningBeforeSpace_"+Id1).html("&nbsp;"); } catch (e) {}
	try { $("#lblFieldIdWarningAfterSpace_"+Id1).html("&nbsp;"); } catch (e) {}
}

function UnMarkedLabelAsRed(Id1,Id2,Id3)
{
	try { $("#"+Id2).removeClass("DSPQRequired12"); } catch (e) {}
	try { $("#"+Id3).removeClass("icon-warning DSPQRequired12"); } catch (e) {}
	try { $("#lblFieldIdWarningBeforeSpace_"+Id1).html(""); } catch (e) {}
	try { $("#lblFieldIdWarningAfterSpace_"+Id1).html(""); } catch (e) {}
}

//*********** Certi **************
function showGridCredentials()
{
	var dspqjobflowdistrictId="";
	var sectionId=8;
	var teacherId=0;
	try { dspqjobflowdistrictId=document.getElementById("dspqjobflowdistrictId").value; } catch (e) {}
	
	/*if(dspqjobflowdistrictId==7800038)
	{
		DSPQServiceAjax.getPFCertificationsGridDspqNoble(dp_Certification_noOfRows,dp_Certification_page,dp_Certification_sortOrderStr,dp_Certification_sortOrderType,{ 
			async: false,
			errorHandler:handleError,
			callback:function(data)
			{
				$('#divDataGridCertifications').html(data);
				applyScrollOnTbl_CertificationsNoble();
			}});
	}
	else
	{*/
		//alert("dspqPortfolioNameId ::: "+dspqPortfolioNameId+" candidateTypeNew  "+candidateType+" SECTIONiD   "+sectionId);
		DSPQServiceAjax.getPFCertificationsGrid(dp_Certification_noOfRows,dp_Certification_page,dp_Certification_sortOrderStr,dp_Certification_sortOrderType,teacherId,dspqPortfolioNameId,candidateType,sectionId,{ 
			async: false,
			errorHandler:handleError,
			callback:function(data)
			{
				$('#divDataGridCertifications').html(data);
				applyScrollOnTbl_Certifications();
			
				if(dspqjobflowdistrictId==4218990 && $('#isPrinciplePhiladelphia').val()=="1")
				{	
					$("#certSRCHeader").html("");
					$(".certSRCRec").html("");
				}
			}
		});
		
	//}
}

function getElectronicReferencesGrid()
{
	var sectionId=9;
	var teacherId=0;
		DSPQServiceAjax.getElectronicReferencesGrid(dp_Reference_noOfRows,dp_Reference_page,dp_Reference_sortOrderStr,dp_Reference_sortOrderType,teacherId,dspqPortfolioNameId,candidateType,sectionId, { 
			async: false,
			errorHandler:handleError,
			callback:function(data)
			{
				$('#divDataElectronicReferences').html(data);
				applyScrollOnTblEleRef();
			}});
}

function getVideoLinksGrid()
{
	var teacherId=0;
	var sectionId=10;
	DSPQServiceAjax.getVideoLinksGrid(dp_VideoLink_Rows,dp_VideoLink_page,dp_VideoLink_sortOrderStr,dp_VideoLink_sortOrderType,teacherId,dspqPortfolioNameId,candidateType,sectionId, { 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#divDataVideoLinks').html(data);
			applyScrollOnTblVideoLinks();
		}});
}

function showGridAdditionalDocuments()
{
	var teacherId=0;
	var sectionId=11;
	DSPQServiceAjax.getAdditionalDocumentsGrid(dp_AdditionalDocuments_noOfRows,dp_AdditionalDocuments_page,dp_AdditionalDocuments_sortOrderStr,dp_AdditionalDocuments_sortOrderType,teacherId,dspqPortfolioNameId,candidateType,sectionId,{  
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#divGridAdditionalDocuments').html(data);
			applyScrollOnTbl_AdditionalDocuments();
		}});
}


function showForm_Certification()
{
	document.getElementById("divMainForm").style.display="block";
	document.getElementById("frmCertificate").reset();
	try { $("#praxisArea").hide(); } catch (e) {}
	try { $("#reading").html(""); } catch (e) {}
	try { $("#writing").html(""); } catch (e) {}
	try { $("#maths").html(""); } catch (e) {}
	document.getElementById("certId").value="";
	try { document.getElementById("stateMaster").value=0; } catch (e) {}
	try { document.getElementById("certificationStatusMaster").value="0"; } catch (e) {}
	try { document.getElementById("certificationtypeMaster").value="0"; } catch (e) {}
	try { document.getElementById("doenumber").value=""; } catch (e) {}
	try { document.getElementById("yearexpires").text="0"; } catch (e) {}
	try { document.getElementById("yearReceived").value="0"; } catch (e) {}
	try { document.getElementById("certUrl").value=""; } catch (e) {}
	//dwr.util.setValues(null);
	try { document.getElementById("certType").value=""; } catch (e) {}
	try { document.getElementById("certificateTypeMaster").value=""; } catch (e) {}
	try { document.getElementById("divCertName").style.display="none"; } catch (e) {}
	try { document.getElementById("removeCert").style.display="none"; } catch (e) {}
	try { document.getElementById("pathOfCertificationFile").value=""; } catch (e) {}
	try { document.getElementById("pathOfCertification").value="0"; } catch (e) {}
	
	try { document.getElementById("pathOfCertification").value="0"; } catch (e) {}
	return false;
}

/*function callNewSelfServiceApplicationFlow(jobId,candidateType)
{
	DSPQServiceAjax.callNewSelfServiceApplicationFlow(jobId,candidateType,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			if(data!='')
				window.location.href="./"+data;
			else
			{
				//alert("DSPQ is not required. Calling normal folw");
				getDistrictSpecificQuestion(jobId)
			}
		}});
}*/


// *************** COver Letter ***********

function validateCandidateType()
{
	var jobId=""; 
	try { jobId=document.getElementById("dspqJobId").value; } catch (e) {}

	var dspqjobflowdistrictId="";
	try { dspqjobflowdistrictId=document.getElementById("dspqjobflowdistrictId").value; } catch (e) {}
	
	var isAffilated=0;
	var candidateType="";
	
	if(dspqjobflowdistrictId==4218990)
	{
		
	}
	else
	{
		if(document.getElementById("isAffilated").checked)
		{
			isAffilated=1;
			$("#inst").show();
			$("#noninst").show();
			try{if(document.getElementById("isMiami").value=="true")
			 $("#partinst").show();}catch(e){}
			 
			 if(dspqjobflowdistrictId==5304860 || dspqjobflowdistrictId==614730)
				{
					var tempStr=$("#partinst").html();
					var chngdd=tempStr.replace(resourceJSON.msgCurrentlyinstructional+"", ""+resourceJSON.msgCurrentsubstitute);
					$("#partinst").html(chngdd);
					$("#partinst").show();
				}
			 if(dspqjobflowdistrictId==7800049 || dspqjobflowdistrictId==7800048 || dspqjobflowdistrictId==7800050 || dspqjobflowdistrictId==7800051 || dspqjobflowdistrictId==7800053)
				{
					var tempStr=$("#partinst").html();
					var chngdd=tempStr.replace("I am currently a part-time instructional employee (Teacher)","I am currently a part-time, substitute, LTO, etc employee.");
					$("#partinst").html(chngdd);
					$("#partinst").show();
				}
			 if(dspqjobflowdistrictId==804800)
			 {
					$(".empCvrrLtrDiv").show();
			}
		}
		else
		{
			/*$("#inst").hide();
			$("#noninst").hide();
			$("#partinst").hide();*/
			
			try { $("#inst").hide(); } catch (e) {}
			try { $("#noninst").hide(); } catch (e) {}
			try { $("#partinst").hide(); } catch (e) {}
			//try { $("#divEmpNumCoverLtr").hide(); } catch (e) {}
			//try { $("#divZipCodeCoverLtr").hide(); } catch (e) {}
			
			$(".empCvrrLtrDiv").hide();
		}
	}
	
	if(isAffilated==1)
		candidateType="I";
	else
		candidateType="E";
	
	if(jobId > 0 )
	{
	}
}



function hideForm_Certification()
{
	try { document.getElementById("divMainForm").style.display="none"; } catch (e) {}
}

function getGroup04_12(jobId,dspqPortfolioNameId,candidateType,groupId)
{
	$('#loadingDiv').show();
	DSPQServiceAjax.getGroup04_12(jobId,dspqPortfolioNameId,candidateType,groupId,{ 
		async: false,
		callback: function(data){
		var dwr_response = data.split("|||");
		if(dwr_response.length==3)
		{
			if(dwr_response[0]!=null && dwr_response[0]!="")
			{
				document.getElementById("group04_12").innerHTML=dwr_response[0];
				var tooltips=dwr_response[1];
				var tooltipsArr = tooltips.split(",");
				
				for(var i=1; i<tooltipsArr.length; i++)
				{
					if(tooltipsArr[i]!='')
					{
						try {$('#'+tooltipsArr[i]).tooltip();} catch (e) {}
					}
				}
				
				var calDates=dwr_response[2];
				var calDatesArr = calDates.split(",");
				for(var i=1; i<calDatesArr.length; i++)
				{
					var calDateInputName=calDatesArr[i];
					if(calDateInputName!='')
					{
						try {cal.manageFields(calDateInputName, calDateInputName, "%m-%d-%Y");} catch (e) {}
					}
				}
				
				
				chkRetiredLoad();
				jafDateOfWithdrawnDivId();
				jafShowDivForRetirementDate();
			}
			
			
			
			$('#loadingDiv').hide();
		}
		//try {$('#iconpophoverSolutation').tooltip();} catch (e) {}
	},
	errorHandler:handleError  
	});
}

function getNextPage()
{
	var sNextURL=""; 
	try { sNextURL=document.getElementById("sNextURL").value; } catch (e) {}
	if(sNextURL!=null && sNextURL!="")
	{
		window.location.replace(sNextURL);
	}
	else
	{
		//alert("Portfolio complete");
		var jobId=""; 
		try { jobId=document.getElementById("dspqJobId").value; } catch (e) {}

		var dspqct=""; 
		try { dspqct=document.getElementById("dspqct").value; } catch (e) {}
		
		var dspqjobflowdistrictId="";
		try { dspqjobflowdistrictId=document.getElementById("dspqjobflowdistrictId").value; } catch (e) {}
		
		var dspqPortfolioNameId="";
		try { dspqPortfolioNameId=document.getElementById("dspqPortfolioNameId").value; } catch (e) {}
		
		DSPQServiceAjax.completedspqJobWiseStatus(jobId,{ 
			async: false,
			callback: function(data)
			{
				if(data=="OK")
				{
					// Need to forward QQ/JSI/EPI
					//window.location.replace("./applyjob.do?jobId="+jobId+"&jaf=1");
					window.location.replace("./jobapplicationflowpc.do?jobId="+jobId+"&jaf=1");
					
					//getDistrictSpecificQuestionJAF(jobId);
				}
			},
		errorHandler:handleError  
		});
		
		
		
		
	}
}

function getPreviousPage()
{
	var sPreviousURL=""; 
	try { sPreviousURL=document.getElementById("sPreviousURL").value; } catch (e) {}
	if(sPreviousURL!=null)
	{
		window.location.replace(sPreviousURL);
	}
}

function getHidePreviousBtn()
{
	var sPreviousURL=""; 
	try { sPreviousURL=document.getElementById("sPreviousURL").value; } catch (e) {}
	if(sPreviousURL=='')
	{
		try { $("#btnprev").hide(); } catch (e) { }
	}
}








function getStateByCountryForDspq(stateModuleFlag)
{
	//alert("stateModuleFlag "+stateModuleFlag);
	var countryId = "";
	try {
		countryId = document.getElementById("countryId").value;
	} catch (e) {
		// TODO: handle exception
	}
	//alert("countryId "+countryId);
	if(countryId!='')
	{
		DSPQServiceAjax.getStateByCountry(countryId,stateModuleFlag,{ 
			async: true,
			callback: function(data)
			{
				if(data[0]!="")
				{
					//document.getElementById("countryCheck").value=1;
					if(stateModuleFlag=="dspq")
					{
						$("#stateIdForDSPQ").html(data[0]);
						if(!(countryId==data[3]))
						{
							document.getElementById("slst").selected=true;
							document.getElementById("slcty").selected=true;
						}
							
					}
					
					
				}
				else
				{
					
				}
			},
		});
	}
	
	if(countryId=='' || countryId=='0')
	{
		if(resourceJSON.locale=="fr")
		{
			countryId='38';
			document.getElementById("ctry38").selected=true;
		}else
		{
			countryId='223';
			document.getElementById("ctry223").selected=true;
		}
	
		
		DSPQServiceAjax.getStateByCountry(countryId,stateModuleFlag,{ 
			async: true,
			callback: function(data)
			{
				if(data[0]!="")
				{
					//document.getElementById("countryCheck").value=1;
					if(stateModuleFlag=="dspq")
					{
						$("#stateIdForDSPQ").html(data[0]);
			
						if(!(countryId==data[3]))
							document.getElementById("slst").selected=true;
					}
					
					
				}
				else
				{
				}
			},
		});
	}
}

function getStateByCountryForDspqPr(stateModuleFlag)
{
	//alert("stateModuleFlag "+stateModuleFlag);
	var countryIdPr = "";
	try {
		countryIdPr = document.getElementById("countryIdPr").value;
	} catch (e) {
		// TODO: handle exception
	}
	if(countryIdPr!='' && countryIdPr!='0')
	{
		DSPQServiceAjax.getStateByCountryPresent(countryIdPr,stateModuleFlag,{ 
			async: true,
			callback: function(data)
			{
				if(data[0]!="")
				{
					//document.getElementById("countryCheck").value=1;
					if(stateModuleFlag=="dspq")
					{
						$("#stateIdForDSPQPr").html(data[0]);
						if(!(countryIdPr==data[3]))
						{
							document.getElementById("slstPr").selected=true;
							document.getElementById("slctyPr").selected=true;
						}
					}
					
					
				}
				else
				{
					
				}
			},
		});
	}
	
	if(countryIdPr=='' || countryIdPr=='0')
	{
		if(resourceJSON.locale=="fr")
		{
			countryId='38';
			document.getElementById("ctryPr38").selected=true;
		}else
		{
			countryId='223';
			document.getElementById("ctryPr223").selected=true;
		}
	
		
		DSPQServiceAjax.getStateByCountry(countryIdPr,stateModuleFlag,{ 
			async: true,
			callback: function(data)
			{
				if(data[0]!="")
				{
					//document.getElementById("countryCheck").value=1;
					if(stateModuleFlag=="dspq")
					{
						$("#stateIdForDSPQPr").html(data[0]);
			
						if(!(countryId==data[3]))
							document.getElementById("slstPr").selected=true;
					}
					
					
				}
				else
				{
					
					}
			},
		});
	}
}

function getGroup04_13(jobId,dspqPortfolioNameId,candidateType,groupId)
{
	$('#loadingDiv').show();
	DSPQServiceAjax.getGroup04_13(jobId,dspqPortfolioNameId,candidateType,groupId,{ 
		async: false,
		callback: function(data){
		var dwr_response = data.split("|||");
		if(dwr_response.length==3)
		{
			if(dwr_response[0]!=null && dwr_response[0]!="")
			{
				document.getElementById("group04_13").innerHTML=dwr_response[0];
				var tooltips=dwr_response[1];
				var tooltipsArr = tooltips.split(",");
				
				for(var i=1; i<tooltipsArr.length; i++)
				{
					if(tooltipsArr[i]!='')
					{
						try {$('#'+tooltipsArr[i]).tooltip();} catch (e) {}
					}
				}
				
				var calDates=dwr_response[2];
				var calDatesArr = calDates.split(",");
				for(var i=1; i<calDatesArr.length; i++)
				{
					var calDateInputName=calDatesArr[i];
					if(calDateInputName!='')
					{
						try {cal.manageFields(calDateInputName, calDateInputName, "%m-%d-%Y");} catch (e) {}
					}
				}
			}
			
			
			
			$('#loadingDiv').hide();
		}
		//try {$('#iconpophoverSolutation').tooltip();} catch (e) {}
		getPFEmploymentDataGrid();
	},
	errorHandler:handleError  
	});
}
function getPFEmploymentDataGrid()
{
	var dspqjobflowdistrictId="";
	try { dspqjobflowdistrictId=document.getElementById("dspqjobflowdistrictId").value; } catch (e) {}
	var teacherId=0;
	var sectionId=13;
	DSPQServiceAjax.getPFEmploymentGrid(dp_Employment_noOfRows,dp_Employment_page,dp_Employment_sortOrderStr,dp_Employment_sortOrderType,teacherId,dspqPortfolioNameId,candidateType,sectionId,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#divDataGridEmployment').html(data);
			applyScrollOnTbl_EmployeementHistory();
			
		}});
}



function getCityListByStateForDSPQ()
{
	$('#loadingDiv').show();	
	var stateId = "0";
	try {
		stateId=document.getElementById("stateIdForDSPQ");
	} catch (e) {
		// TODO: handle exception
	}
	var res;
	createXMLHttpRequest();
	queryString = "findcityhtmlbystate.do?stateId="+stateId.value+"&dt="+new Date().getTime();
	xmlHttp.open("POST", queryString, false);
	xmlHttp.onreadystatechange = function()
	{
		if(xmlHttp.readyState == 4)
		{
			if(xmlHttp.status==200)	
			{
				while (document.getElementById("cityIdForDSPQ").childNodes.length >= 1 )
				{
				    document.getElementById("cityIdForDSPQ").removeChild(document.getElementById("cityIdForDSPQ").firstChild);       
				}
				var optAr = xmlHttp.responseText.split("##");
				var newOption;
				
				newOption = document.createElement('option');
			    newOption.text=resourceJSON.msgSelectCity;
			    newOption.value="";
			    newOption.id="slcty";
			   	document.getElementById('cityIdForDSPQ').options.add(newOption);
				for(var i=0;i<optAr.length-1;i++)
				{
				    newOption = document.createElement('option');
				    newOption.id=optAr[i].split("$$")[0];
				    newOption.value=optAr[i].split("$$")[1];
				    newOption.text=optAr[i].split("$$")[2];
				    document.getElementById('cityIdForDSPQ').options.add(newOption);
				}
				$('#loadingDiv').hide();
			}else{
				alert(resourceJSON.msgServerErr);
			}
		}
	}
	xmlHttp.send(null);	
}

function getCityListByStateForDSPQPr()
{
	$('#loadingDiv').show();	
	var stateId = document.getElementById("stateIdForDSPQPr");
	
	var res;
	createXMLHttpRequest();
	queryString = "findcityhtmlbystate.do?stateId="+stateId.value+"&dt="+new Date().getTime();
	xmlHttp.open("POST", queryString, false);
	xmlHttp.onreadystatechange = function()
	{
		if(xmlHttp.readyState == 4)
		{
			if(xmlHttp.status==200)	
			{
				
				//alert(xmlHttp.responseText)
				//alert(document.getElementById("divSelectCity")+">>>>")				
				while (document.getElementById("cityIdForDSPQPr").childNodes.length >= 1 )
				{
				    document.getElementById("cityIdForDSPQPr").removeChild(document.getElementById("cityIdForDSPQPr").firstChild);
				}
				
				//document.getElementById("cityId").innerHTML=xmlHttp.responseText;
				//alert(xmlHttp.responseText)
				var optAr = xmlHttp.responseText.split("##");
				var newOption;
				
				newOption = document.createElement('option');
			    newOption.text="Select City";
			    newOption.value="";
			    newOption.id="slctyPr";
			   	document.getElementById('cityIdForDSPQPr').options.add(newOption);
				for(var i=0;i<optAr.length-1;i++)
				{
				    newOption = document.createElement('option');
				    newOption.id=optAr[i].split("$$")[0];
				    newOption.value=optAr[i].split("$$")[1];
				    newOption.text=optAr[i].split("$$")[2];
				    //document.getElementById("cityId").appendChild(newOption);
				    document.getElementById('cityIdForDSPQPr').options.add(newOption);
				}
				$('#loadingDiv').hide();
			}else{
				//alert("server Error");
			}
		}
	}
	xmlHttp.send(null);	
}

function getGroup04_14(jobId,dspqPortfolioNameId,candidateType,groupId)
{
	$('#loadingDiv').show();
	DSPQServiceAjax.getGroup04_14(jobId,dspqPortfolioNameId,candidateType,groupId,{ 
		async: false,
		callback: function(data){
		var dwr_response = data.split("|||");
		if(dwr_response.length==3)
		{
			if(dwr_response[0]!=null && dwr_response[0]!="")
			{
				document.getElementById("group04_14").innerHTML=dwr_response[0];
				var tooltips=dwr_response[1];
				var tooltipsArr = tooltips.split(",");
				
				for(var i=1; i<tooltipsArr.length; i++)
				{
					if(tooltipsArr[i]!='')
					{
						try {$('#'+tooltipsArr[i]).tooltip();} catch (e) {}
					}
				}
				
				var calDates=dwr_response[2];
				var calDatesArr = calDates.split(",");
				for(var i=1; i<calDatesArr.length; i++)
				{
					var calDateInputName=calDatesArr[i];
					if(calDateInputName!='')
					{
						try {cal.manageFields(calDateInputName, calDateInputName, "%m-%d-%Y");} catch (e) {}
					}
				}
			}
			
			
			
			$('#loadingDiv').hide();
		}
		//try {$('#iconpophoverSolutation').tooltip();} catch (e) {}
		getInvolvementGrid();
	},
	errorHandler:handleError  
	});
}

function getInvolvementGrid()
{
	
	//getInvolvementGridDistrictSpecific
	//var districtId = document.getElementById("districtIdForDSPQ").value;
	
	var dspqjobflowdistrictId="";
	try { dspqjobflowdistrictId=document.getElementById("dspqjobflowdistrictId").value; } catch (e) {}
	
	//try { var jobCat=$('#jobcategoryDsp').val().trim();} catch (e) {}
	/*if(dspqjobflowdistrictId!="" && dspqjobflowdistrictId==3703120){
		DSPQServiceAjax.getInvolvementGridDistrictSpecific(dp_Involvement_Rows,dp_Involvement_page,dp_Involvement_sortOrderStr,dp_Involvement_sortOrderType,districtId,jobCat,{ 
			async: false,
			errorHandler:handleError,
			callback:function(data)
			{
			//document.getElementById("divDataInvolvement").innerHTML=data;
				$('#divDataInvolvement').html(data);
				applyScrollOnInvl();
				if(dspqjobflowdistrictId==3703120 && $('#jobcategoryDsp').val().trim().indexOf("Bus Driver") !=-1){
					$(".pplRang").html("");
					$(".yNldPPl").html("");
				}
				
			}});
	}
	else
	{*/
	var teacherId=0;
	var sectionId=14;
		DSPQServiceAjax.getInvolvementGrid(dp_Involvement_Rows,dp_Involvement_page,dp_Involvement_sortOrderStr,dp_Involvement_sortOrderType,teacherId,dspqPortfolioNameId,candidateType,sectionId,{ 
			async: false,
			errorHandler:handleError,
			callback:function(data)
			{
				$('#divDataInvolvement').html(data);
				applyScrollOnInvl();
			}});
	//}

}



function getGroup04_15(jobId,dspqPortfolioNameId,candidateType,groupId)
{
	$('#loadingDiv').show();
	DSPQServiceAjax.getGroup04_15(jobId,dspqPortfolioNameId,candidateType,groupId,{ 
		async: false,
		callback: function(data){
		var dwr_response = data.split("|||");
		if(dwr_response.length==3)
		{
			if(dwr_response[0]!=null && dwr_response[0]!="")
			{
				document.getElementById("group04_15").innerHTML=dwr_response[0];
				var tooltips=dwr_response[1];
				
				var tooltipsArr = tooltips.split(",");
				
				for(var i=1; i<tooltipsArr.length; i++)
				{
					if(tooltipsArr[i]!='')
					{
						try {$('#'+tooltipsArr[i]).tooltip();} catch (e) {}
					}
				}
				
				var calDates=dwr_response[2];
				var calDatesArr = calDates.split(",");
				for(var i=1; i<calDatesArr.length; i++)
				{
					var calDateInputName=calDatesArr[i];
					if(calDateInputName!='')
					{
						try {cal.manageFields(calDateInputName, calDateInputName, "%m-%d-%Y");} catch (e) {}
					}
				}
				
				getHonorsGrid();
			}
			
			
			
			$('#loadingDiv').hide();
		}
		//try {$('#iconpophoverSolutation').tooltip();} catch (e) {}
		
		//getHonorsGrid();
		
	},
	errorHandler:handleError  
	});
}
function getHonorsGrid()
{
	var teacherId=0;
	var sectionId=15;
	DSPQServiceAjax.getHonorsGrid(teacherId,dspqPortfolioNameId,candidateType,sectionId,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
			document.getElementById("divDataHoner").innerHTML=data;
			applyScrollOnHonors();
		}});
}


function chkLenOfTextArea(textValue,maxlength)
{
	var tlen=textValue.value.length;
	if(tlen>maxlength)
	{
		document.getElementById(textValue.id).value=document.getElementById(textValue.id).value.substr(0,maxlength)
	}
}

/*
//document.getElementById("lblSectionId_"+PI_Section_Name).scrollTop=0;
//$('html, body').animate({scrollTop : 0},800);
//$( "#lblSectionId_"+PI_Section_Name ).focus();*/



function validateCustomAll(PI_Section_Name)
{
	var arrSection=[];
	var jobId=""; 
	try { jobId=document.getElementById("dspqJobId").value; } catch (e) {}
	
	var IsGoNext=0;
	var IsSave=0;
	var Section_customFldCount=getInputCtrlTextValue("customFld_"+PI_Section_Name);
	for(var i=1;i<=Section_customFldCount; i++)
	{
		IsSave=1;
		var customFieldQNo=0;
		try { customFieldQNo=$("#lblCustomFieldId_"+PI_Section_Name+"_"+i).attr("customFieldQNo"); } catch (e) {}
		
		var customFieldShortCode="";
		try { customFieldShortCode=$("#lblCustomFieldId_"+PI_Section_Name+"_"+i).attr("customFieldShortCode"); } catch (e) {}
		
		var customFieldRequired=0;
		try { customFieldRequired=$("#lblCustomFieldId_"+PI_Section_Name+"_"+i).attr("customFieldRequired"); } catch (e) {}
		
		var customFieldOtionCount=0;
		try { customFieldOtionCount=$("#lblCustomFieldId_"+PI_Section_Name+"_"+i).attr("customFieldOtionCount"); } catch (e) {}
		
		var customFieldQuestionTypeId=0;
		try { customFieldQuestionTypeId=$("#lblCustomFieldId_"+PI_Section_Name+"_"+i).attr("customFieldQuestionTypeId"); } catch (e) {}
		
		var customFieldLabelAnswer="";
		try { customFieldLabelAnswer=$("#lblCustomFieldId_"+PI_Section_Name+"_"+i).attr("customFieldLabelAnswer"); } catch (e) {}
		
		var customFieldAnswerID=0;
		try { customFieldAnswerID=$("#lblCustomFieldId_"+PI_Section_Name+"_"+i).attr("customFieldAnswerID"); } catch (e) {}
		
		
		//alert("customFieldQuestionTypeId "+customFieldQuestionTypeId +" customFieldLabelAnswer "+customFieldLabelAnswer);
		
		//alert("customFieldQNo "+customFieldQNo +" customFieldShortCode "+customFieldShortCode +" customFieldRequired "+customFieldRequired +" customFieldOtionCount "+customFieldOtionCount);
		//alert("101");
		
		var UserOptionId="";
		var UserTextArea="";
		var sCustomFileName="";
		var bNeedToUpload=false;
		
		var custonFileData = "";
	    var formdata = new FormData();
	    
		if(customFieldShortCode=='et' || customFieldShortCode=='ml')
		{
			var txtAreaValue_et_ml=getTextAreaEditorCtrlTextValue_CustFld(customFieldQNo+"_"+i);
			iReturnValidateValue=validateCustomFldControlStr(customFieldShortCode, txtAreaValue_et_ml,"", customFieldRequired, customFieldQNo,PI_Section_Name,i);
			if(iReturnValidateValue=="1")
			{
				//PI_Section=PI_Section+1;
				//iReturnValidateTotalValue=iReturnValidateTotalValue+PI_Section;
				IsGoNext++;
			}
			UserTextArea=txtAreaValue_et_ml;
		}
		else if(customFieldShortCode=='mlsel')
		{
			var checkBoxValue_mlsel=getInputCtrlCheckBoxValue("opt_"+customFieldQNo+"_"+i);
			iReturnValidateValue=validateCustomFldControlStr(customFieldShortCode, checkBoxValue_mlsel,"", customFieldRequired, customFieldQNo,PI_Section_Name,i);
			if(iReturnValidateValue=="1")
			{
				//PI_Section=PI_Section+1;
				//iReturnValidateTotalValue=iReturnValidateTotalValue+PI_Section;
				IsGoNext++;
			}
			UserOptionId=checkBoxValue_mlsel;
		}
		else if(customFieldShortCode=='mloet')
		{
			var checkBoxValue_mloet=getInputCtrlCheckBoxValue("opt_"+customFieldQNo+"_"+i);
			var txtAreaValue_mloet=getTextAreaEditorCtrlTextValue_CustFld(customFieldQNo+"_"+i);
			iReturnValidateValue=validateCustomFldControlStr(customFieldShortCode, checkBoxValue_mloet,txtAreaValue_mloet, customFieldRequired, customFieldQNo,PI_Section_Name,i);
			if(iReturnValidateValue=="1")
			{
				//PI_Section=PI_Section+1;
				//iReturnValidateTotalValue=iReturnValidateTotalValue+PI_Section;
				IsGoNext++;
			}
			UserTextArea=txtAreaValue_mloet;
			UserOptionId=checkBoxValue_mloet;
		}
		else if(customFieldShortCode=='sloet')
		{
			var checkBoxValue_sloet=getInputCtrlRadioValue("opt_"+customFieldQNo+"_"+i);
			var txtAreaValue_sloet=getTextAreaEditorCtrlTextValue_CustFld(customFieldQNo+"_"+i);
			iReturnValidateValue=validateCustomFldControlStr(customFieldShortCode, checkBoxValue_sloet,txtAreaValue_sloet, customFieldRequired, customFieldQNo,PI_Section_Name,i);
			if(iReturnValidateValue=="1")
			{
				//PI_Section=PI_Section+1;
				//iReturnValidateTotalValue=iReturnValidateTotalValue+PI_Section;
				IsGoNext++;
			}
			UserTextArea=txtAreaValue_sloet;
			UserOptionId=checkBoxValue_sloet;
		}
		else if(customFieldShortCode=='tf' || customFieldShortCode=='slsel')
		{
			var checkBoxValue_tf_slsel=getInputCtrlRadioValue("opt_"+customFieldQNo+"_"+i);
			iReturnValidateValue=validateCustomFldControlStr(customFieldShortCode, checkBoxValue_tf_slsel,"", customFieldRequired, customFieldQNo,PI_Section_Name,i);
			//alert("checkBoxValue_tf_slsel "+checkBoxValue_tf_slsel +" iReturnValidateValue "+iReturnValidateValue);
			if(iReturnValidateValue=="1")
			{
				//PI_Section=PI_Section+1;
				//iReturnValidateTotalValue=iReturnValidateTotalValue+PI_Section;
				IsGoNext++;
			}
			UserOptionId=checkBoxValue_tf_slsel;
		 }
		else if(customFieldShortCode=='drsls' || customFieldShortCode=="DD")
		{
			var listBoxValue_drsls=getInputCtrlTextValue("opt_"+customFieldQNo+"_"+i);
			iReturnValidateValue=validateCustomFldControlStr(customFieldShortCode, listBoxValue_drsls,"", customFieldRequired, customFieldQNo,PI_Section_Name,i);
			if(iReturnValidateValue=="1")
			{
				//PI_Section=PI_Section+1;
				//iReturnValidateTotalValue=iReturnValidateTotalValue+PI_Section;
				IsGoNext++;
			}
			UserOptionId=listBoxValue_drsls;
		 }
		else if(customFieldShortCode=='dt')
		{
			var textValue_drsls=getInputCtrlTextValue("opt_"+customFieldQNo+"_"+i);
			iReturnValidateValue=validateCustomFldControlStr(customFieldShortCode, textValue_drsls,"", customFieldRequired, customFieldQNo,PI_Section_Name,i);
			if(iReturnValidateValue=="1")
			{
				//PI_Section=PI_Section+1;
				//iReturnValidateTotalValue=iReturnValidateTotalValue+PI_Section;
				IsGoNext++;
			}
			UserOptionId=textValue_drsls;
		 }
		else if(customFieldShortCode=='sl')
		{
			var textValue_drsls=getInputCtrlTextValue("opt_"+customFieldQNo+"_"+i);
			iReturnValidateValue=validateCustomFldControlStr(customFieldShortCode, textValue_drsls,"", customFieldRequired, customFieldQNo,PI_Section_Name,i);
			if(iReturnValidateValue=="1")
			{
				//PI_Section=PI_Section+1;
				//iReturnValidateTotalValue=iReturnValidateTotalValue+PI_Section;
				IsGoNext++;
			}
			UserTextArea=textValue_drsls;
		 }
		else if(customFieldShortCode=='UAT')
		{
			var db_InputCtrlId_CustFld=getInputCtrlTextValue("opt_"+customFieldQNo+"_"+i+"DBFileName");
			var InputCtrlId_CustFld="opt_"+customFieldQNo+"_"+i+"File";
			var sFileNameCustFld=getInputCtrlTextValue(InputCtrlId_CustFld);
			
			iReturnValidateValue=validateCustomFldFileUploadControlStr(customFieldShortCode, sFileNameCustFld,"", customFieldRequired, customFieldQNo,PI_Section_Name,i,db_InputCtrlId_CustFld,InputCtrlId_CustFld);
			if(iReturnValidateValue=="1")
			{
				IsGoNext++;
			}
			else
			{
				if(sFileNameCustFld!=null && sFileNameCustFld!="")
					bNeedToUpload=true;
				
				custonFileData = document.getElementById(InputCtrlId_CustFld).files[0];
			    formdata.append(InputCtrlId_CustFld, custonFileData);
			    
			}
			sCustomFileName=db_InputCtrlId_CustFld;
		 }
		else if(customFieldShortCode=='UATEF')
		{
			var db_InputCtrlId_CustFld=getInputCtrlTextValue("opt_"+customFieldQNo+"_"+i+"DBFileName");
			var InputCtrlId_CustFld="opt_"+customFieldQNo+"_"+i+"File";
			var sFileNameCustFld=getInputCtrlTextValue(InputCtrlId_CustFld);
			
			var	txtAreaValue_FileUpload=getTextAreaEditorCtrlTextValue_CustFld(customFieldQNo+"_"+i);
			
			iReturnValidateValue=validateCustomFldFileUploadControlStr(customFieldShortCode, sFileNameCustFld,txtAreaValue_FileUpload, customFieldRequired, customFieldQNo,PI_Section_Name,i,db_InputCtrlId_CustFld,InputCtrlId_CustFld);
			if(iReturnValidateValue=="1")
			{
				IsGoNext++;
			}
			else
			{
				if(sFileNameCustFld!=null && sFileNameCustFld!="")
					bNeedToUpload=true;
				
				custonFileData = document.getElementById(InputCtrlId_CustFld).files[0];
			    formdata.append(InputCtrlId_CustFld, custonFileData);

			}
			sCustomFileName=db_InputCtrlId_CustFld;
			UserTextArea=txtAreaValue_FileUpload;
		 }
		else if(customFieldShortCode=='OSONP')
		{
			var checkBoxValue_tf_slsel=getInputCtrlRadioValue("opt_"+customFieldQNo+"_"+i);
			iReturnValidateValue=validateCustomFldControlStr(customFieldShortCode, checkBoxValue_tf_slsel,"", customFieldRequired, customFieldQNo,PI_Section_Name,i);
			//alert("checkBoxValue_tf_slsel "+checkBoxValue_tf_slsel +" iReturnValidateValue "+iReturnValidateValue);
			if(iReturnValidateValue=="1")
			{
				//PI_Section=PI_Section+1;
				//iReturnValidateTotalValue=iReturnValidateTotalValue+PI_Section;
				IsGoNext++;
			}
			UserOptionId=checkBoxValue_tf_slsel;
		 }
		else if(customFieldShortCode=='sswc')
		{
			var checkBoxValue_mloet=getInputCtrlRadioValue("opt_"+customFieldQNo+"_"+i);
			var txtValue_sswc=getInputCtrlTextValue("opt_"+customFieldQNo+"_"+i);
			iReturnValidateValue=validateCustomFldControlStr(customFieldShortCode, checkBoxValue_mloet,txtAreaValue_mloet, customFieldRequired, customFieldQNo,PI_Section_Name,i);
			if(iReturnValidateValue=="1")
			{				
				IsGoNext++;
			}
			UserTextArea=txtValue_sswc;
			UserOptionId=checkBoxValue_mloet;
		}
		
		if(bNeedToUpload)
		{
			try
			{
			    /*var xhr = new XMLHttpRequest();       
			    xhr.open("POST","selfServiceCustomFieldFileUploadServlet.do", true);
			    xhr.send(formdata);	
			    xhr.onload = function(e) 
			    {
			        if (this.status == 200) 
			        {
			        	if(this.responseText!="Error")
			        		sCustomFileName=this.responseText;
			        }
			    };*/
			    
				sCustomFileName=$.ajax({
		    	  url : 'selfServiceCustomFieldFileUploadServlet.do',
		    	        type : 'POST',
		    	        async: false,
		    	        cache: false,
		    	        contentType: false,
		    	        processData: false,
		    	        data : formdata,
		    	        success : function(data) { 
		    	        }
		    	 }).responseText;
			    
			}catch(err){
				alert(err);
			}
		}
		
		var jobOrder = {jobId:jobId};
		var questionTypeMaster = {questionTypeId:customFieldQuestionTypeId};
		var districtSpecificQuestion = {questionId:customFieldQNo};
		
		arrSection.push({ 
			"selectedOptions"  : UserOptionId,
			"question"  : customFieldLabelAnswer,
			"questionTypeMaster" : questionTypeMaster,
			"questionType" : customFieldShortCode,
			"districtSpecificPortfolioQuestions" : districtSpecificQuestion,
			"jobOrder" : jobOrder,
			"insertedText" : UserTextArea,
			"answerId" :customFieldAnswerID,
			"fileName" : sCustomFileName
		});
		
	}
	
	if(IsGoNext==0 && IsSave==1)
	{
		DSPQServiceAjax.saveCustomFieldAnswer(arrSection,{
			async: false,
			errorHandler:handleError,
			callback:function(data)
			{
				if(data=="error")
				{
					IsGoNext++;
				}
			}
		  });
	}
	
	return IsGoNext;
}

function validateInputListControlToAndFromYear(InputValue1,InputValue2,Id1,Id2,Id3)
{
	var iReturnValidateValue=0;
	var InputValue_dspqRequired=0;
	try { InputValue_dspqRequired=$("#"+Id1).attr("dspqRequired"); } catch (e) {}
	
	if(InputValue_dspqRequired=="1")
	{
		//var InputValue_dspqRequired_dspqLabelName="";
		//try { InputValue_dspqRequired_dspqLabelName=$("#"+Id1).attr("dspqLabelName"); } catch (e) {}
		
		if(InputValue1=="0" || InputValue2=="0" || InputValue1 > InputValue2)
		{
			$("#"+Id2).addClass("DSPQRequired12");
			$("#"+Id3).addClass("icon-warning DSPQRequired12");
			$("#lblFieldIdWarningBeforeSpace_"+Id1).html("&nbsp;");
			$("#lblFieldIdWarningAfterSpace_"+Id1).html("&nbsp;");
			iReturnValidateValue=1;
			
		}
		else
		{
			$("#"+Id2).removeClass("DSPQRequired12");
			$("#"+Id3).removeClass("icon-warning DSPQRequired12");
			
			$("#lblFieldIdWarningBeforeSpace_"+Id1).html("");
			$("#lblFieldIdWarningAfterSpace_"+Id1).html("");
			
			iReturnValidateValue=0;
		}
			
	}
	
	return iReturnValidateValue;
	
}

function validateInputListControlTwo(InputValue1,InputValue2,Id1,Id2,Id3)
{
	var iReturnValidateValue=0;
	var InputValue_dspqRequired=0;
	try { InputValue_dspqRequired=$("#"+Id1).attr("dspqRequired"); } catch (e) {}
	
	if(InputValue_dspqRequired=="1")
	{
		//var InputValue_dspqRequired_dspqLabelName="";
		//try { InputValue_dspqRequired_dspqLabelName=$("#"+Id1).attr("dspqLabelName"); } catch (e) {}
		
		if(InputValue1=="0" || InputValue2=="0")
		{
			$("#"+Id2).addClass("DSPQRequired12");
			$("#"+Id3).addClass("icon-warning DSPQRequired12");
			$("#lblFieldIdWarningBeforeSpace_"+Id1).html("&nbsp;");
			$("#lblFieldIdWarningAfterSpace_"+Id1).html("&nbsp;");
			iReturnValidateValue=1;
			
		}
		else
		{
			$("#"+Id2).removeClass("DSPQRequired12");
			$("#"+Id3).removeClass("icon-warning DSPQRequired12");
			
			$("#lblFieldIdWarningBeforeSpace_"+Id1).html("");
			$("#lblFieldIdWarningAfterSpace_"+Id1).html("");
			
			iReturnValidateValue=0;
		}
			
	}
	
	return iReturnValidateValue;
	
}

function delRow_academic(id)
{	
	document.getElementById("academicID").value=id;
	$('#deleteAcademicRecord').modal('show');
}

function deleteAcademicRecord(){
	var id=document.getElementById("academicID").value;
	DSPQServiceAjax.deletePFAcademinGrid(id,{
	async: false,
	errorHandler:handleError,
	callback:function(data)
	{
		$('#deleteAcademicRecord').modal('hide');
		showGridAcademics();
	}
  });
}


function cancelApplyJob()
{
	$('#cancelPortfolioToDashBoard').modal('show');
	//window.location.href='applyjob.do?jobId='+$('#dspqJobId').val();
	//window.location.href='userdashboard.do';
}

function cancelApplyJobL2()
{
	$('#cancelPortfolioToDashBoard').modal('hide');
	window.location.href='userdashboard.do';
}

function showRecordToForm(academicId,statusCheck)
{
	/*var dspqjobflowdistrictId="";
	try { dspqjobflowdistrictId=document.getElementById("dspqjobflowdistrictId").value; } catch (e) {}*/
	
	document.getElementById("frmAcadamin").reset();	
	try { document.getElementById("divGPARowOther").style.display="none"; } catch (e) {}
	
	DSPQServiceAjax.showEditAcademics(academicId,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
	    {
		
			try { document.getElementById("divAcademicRow").style.display="block"; } catch (e) {}
			try { document.getElementById("academicId").value=data.academicId;	} catch (e) { }
			try { document.getElementById("degreeName").value=data.degreeId.degreeName;	} catch (e) { }
			try { document.getElementById("degreeId").value=data.degreeId.degreeId;	} catch (e) { }
			try { document.getElementById("degreeType").value=data.degreeId.degreeType;	} catch (e) { }
			
			try { 
				if(data.universityId!=null)
				{
					document.getElementById("universityId").value=data.universityId.universityId;		
					document.getElementById("universityName").value=data.universityId.universityName;
				}
			} catch (e) { }
			
			try { 
				if(data.fieldId!=null)
				{
					document.getElementById("fieldId").value=data.fieldId.fieldId;
					document.getElementById("fieldName").value=data.fieldId.fieldName;
				}
			} catch (e) { }
			
			try { 
				if(data.pathOfTranscript!=null && data.pathOfTranscript!="")
				{
					document.getElementById("divResumeSection").style.display="block";
					document.getElementById("divResumerName").innerHTML="<a href='javascript:void(0)' id='hrefEditTrans' onclick=\"downloadTranscript('"+data.academicId+"','hrefEditTrans');if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;\">"+data.pathOfTranscript+"</a>";
					document.getElementById("aca_file").value="1";
				}
				else
				{
					document.getElementById("divResumeSection").style.display="none";
					document.getElementById("aca_file").value="0";
					//document.getElementById("divResumerName").innerHTML = "";
				}
			} catch (e) { }
			
			try { 
				if(data.attendedInYear!=null)
					document.getElementById("attendedInYear".toString()+(data.attendedInYear)).selected=true;
				
			} catch (e) { }
			
			try { 
				if(data.leftInYear!=null)
					document.getElementById("leftInYear".toString()+(data.leftInYear)).selected=true;
			} catch (e) { }
			
			try { 
				if(document.getElementById("degreeType").value=="B" || document.getElementById("degreeType").value=="b")
				{
					try { document.getElementById("divGPARow").style.display="block"; } catch (e) {}
					try { document.getElementById("gpaFreshmanYear").value =  data.gpaFreshmanYear; } catch (e) {}
					try { document.getElementById("gpaJuniorYear").value =  data.gpaJuniorYear; } catch (e) {}
					try { document.getElementById("gpaSophomoreYear").value =  data.gpaSophomoreYear; } catch (e) {}
					try { document.getElementById("gpaSeniorYear").value =  data.gpaSeniorYear;  } catch (e) {}
					try { document.getElementById("gpaCumulative").value =  data.gpaCumulative; } catch (e) {}
				}
				else if(document.getElementById("degreeType").value=="ND")
				{
					
					//try {  } catch (e) {}
					try { document.getElementById("divGPARow").style.display="none"; } catch (e) {}
					try { document.getElementById("divGPARowOther").style.display="block"; } catch (e) {}
					try { document.getElementById("gpaCumulative1").value =  data.gpaCumulative; } catch (e) {}
				}
				else
				{
					try { document.getElementById("divGPARow").style.display="none"; } catch (e) {}
					try { document.getElementById("divGPARowOther").style.display="block"; } catch (e) {}
					try { document.getElementById("gpaCumulative1").value =  data.gpaCumulative; } catch (e) {}
					
					/*if(isMiamiChk=='false' && statusCheck=='V')
					{
						document.getElementById("divGPARowOther").disabled=true;
						document.getElementById("gpaCumulative1").disabled=true;
					}
					else
					{
						document.getElementById("divGPARowOther").disabled=false;
						document.getElementById("gpaCumulative1").disabled=false;
					}*/
				}
			} catch (e) { }
			
			try {
				/*if(data.degreeId.degreeName=="High School or GED")
				{
					document.getElementById('universityName').disabled=true;
					document.getElementById('fieldName').disabled=true;
					document.getElementById('gpaCumulative1').disabled=true;
				}
				else
				{
					document.getElementById('universityName').disabled=false;
					document.getElementById('fieldName').disabled=false;
					document.getElementById('gpaCumulative1').disabled=false;
				}*/
			} catch (e) {
				// TODO: handle exception
			}
		
			try {
				document.getElementById("degreeName").focus();
			} catch (e) {
				// TODO: handle exception
			}
			
			
		
	}})
	return false;
}



function validateInputFileControlStr(InputValue,Id1,Id2,Id3,edithidden_file)
{
	var iReturnValidateValue=0;
	var InputValue_dspqRequired=0;
	try { InputValue_dspqRequired=$("#"+Id1).attr("dspqRequired"); } catch (e) {}
	if(InputValue_dspqRequired=="1")
	{
		if(InputValue=="" && edithidden_file=="0")
		{
			iReturnValidateValue=1;
		}
	}
		
	if(InputValue!="")
	{
		var ext = InputValue.substr(InputValue.lastIndexOf('.') + 1).toLowerCase();
		var fileSize=0;		
		if ($.browser.msie==true)
	 	{	
		    fileSize = 0;	   
		}
		else
		{		
			if(document.getElementById(Id1).files[0]!=undefined)
				fileSize = document.getElementById(Id1).files[0].size;
		}
		
		if(ext!="")
		{
			if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt'))
			{
				iReturnValidateValue=1;
			}	
			else if(fileSize>=10485760)
			{
				iReturnValidateValue=1;
			}
			else
			{
				iReturnValidateValue=0;
			}
		}
	}
	
	if(iReturnValidateValue==0)
	{
		$("#"+Id2).removeClass("DSPQRequired12");
		$("#"+Id3).removeClass("icon-warning DSPQRequired12");
		
		$("#lblFieldIdWarningBeforeSpace_"+Id1).html("");
		$("#lblFieldIdWarningAfterSpace_"+Id1).html("");
		
		iReturnValidateValue=0;
	}
	else
	{
		$("#"+Id2).addClass("DSPQRequired12");
		$("#"+Id3).addClass("icon-warning DSPQRequired12");
		$("#lblFieldIdWarningBeforeSpace_"+Id1).html("&nbsp;");
		$("#lblFieldIdWarningAfterSpace_"+Id1).html("&nbsp;");
		
		if(!isSetFocus)
		{
			$("#"+Id1).focus();
			isSetFocus=true;
		}
	}
	
	return iReturnValidateValue;
	
}


function validateInputVideoFileControlStr(InputValue,Id1,Id2,Id3,edithidden_file)
{
	var iReturnValidateValue=0;
	var InputValue_dspqRequired=0;
	try { InputValue_dspqRequired=$("#"+Id1).attr("dspqRequired"); } catch (e) {}
	if(InputValue_dspqRequired=="1")
	{
		if(InputValue=="" && (edithidden_file=="0" || edithidden_file==""))
		{
			iReturnValidateValue=1;
		}
	}
		
	if(InputValue!="")
	{
		var ext = InputValue.substr(InputValue.lastIndexOf('.') + 1).toLowerCase();
		var fileSize=0;		
		if ($.browser.msie==true)
	 	{	
		    fileSize = 0;	   
		}
		else
		{		
			if(document.getElementById(Id1).files[0]!=undefined)
				fileSize = document.getElementById(Id1).files[0].size;
		}
		
		if(ext!="")
		{
			if(!(ext=="mp4"||ext=="MP4"||ext=="ogv"||ext=="ogg"||ext=="webm"||ext=="wmv"))
			{
				
				iReturnValidateValue=1;
			}	
			else if(fileSize>=10485760)
			{
				iReturnValidateValue=1;
			}
			else
			{
				iReturnValidateValue=0;
			}
		}
	}
	
	if(iReturnValidateValue==0)
	{
		$("#"+Id2).removeClass("DSPQRequired12");
		$("#"+Id3).removeClass("icon-warning DSPQRequired12");
		
		$("#lblFieldIdWarningBeforeSpace_"+Id1).html("");
		$("#lblFieldIdWarningAfterSpace_"+Id1).html("");
		
		iReturnValidateValue=0;
	}
	else
	{
		$("#"+Id2).addClass("DSPQRequired12");
		$("#"+Id3).addClass("icon-warning DSPQRequired12");
		$("#lblFieldIdWarningBeforeSpace_"+Id1).html("&nbsp;");
		$("#lblFieldIdWarningAfterSpace_"+Id1).html("&nbsp;");
		
		if(!isSetFocus)
		{
			$("#"+Id1).focus();
			isSetFocus=true;
		}
	}
	
	return iReturnValidateValue;
	
}

function displayTchrLanguage()
{
	
	if($("#divDataLangTchrGrid").is(':visible'))
	{
		var teacherId=0;
		var sectionId=18;
		DSPQServiceAjax.displayTeacherLanguage(dp_TchrLang_Rows,dp_TchrLang_page,dp_TchrLang_sortOrderStr,dp_TchrLang_sortOrderType,teacherId,dspqPortfolioNameId,candidateType,sectionId,{ 
			async: false,
			errorHandler:handleError,
			callback: function(data)
			{
				$("#divDataLangTchrGrid").html(data);
				applyScrollOnTblteacherLang();
			}
		});
	}
	
}

function showLanguageGrid(){
	if($("#lblSectionId_tooltipSection18").is(':visible'))
	{
		var checkedVal = $('input[name=knowLanguage]:checked').val();
		if(checkedVal=="1"){
			$('#divDataLangTchrGrid').show();
			$('#lanAddDiv').show();
			displayTchrLanguage();
		}else{
			$("#divDataLangTchrGrid").hide();
			$('#lanAddDiv').hide();
			lanAddDiv
			hideTchrLang();
		}
	}
	
}

function showLanguageForm()
{
	try { $("#spokenLanguage").val("0"); } catch (e) {}
	try { $("#languageText").val(""); } catch (e) {}
	try { $("#oralSkills").val("0"); } catch (e) {}
	try { $("#writtenSkills").val("0"); } catch (e) {}
	setDefColortoErrorMsgToTchLang();
	try { $("#errordivTchLang").html(""); } catch (e) {}
	try { $("#errordivTchLang").hide(); } catch (e) {}
	try { $("#divLanguageForm").show(); } catch (e) {}
	hideShowSpknLng();
}

function setDefColortoErrorMsgToTchLang()
{	
	try { $("#spokenLanguage").css("background-color",""); } catch (e) {}
	try { $("#languageText").css("background-color",""); } catch (e) {}
	try { $("#oralSkills").css("background-color","");  } catch (e) {}
	try { $("#writtenSkills").css("background-color","");  } catch (e) {}
}

function hideTchrLang()
{
	document.getElementById("divLanguageForm").style.display="none";
	return false;
}
function insertOrUpdateTchrLang()
{
	var Lan_Section=0;
	var Lan_Section_Name="tooltipSection18";
	
	var languageText="";
	var InputCtrlId_spokenLanguage="spokenLanguage";
	var spokenLanguage=getInputCtrlTextValue(InputCtrlId_spokenLanguage);
	if(spokenLanguage==-1)
	{
		var InputCtrlId_languageText="languageText";
		languageText=getInputCtrlTextValue(InputCtrlId_languageText);
		//iReturnValidateValue=validateInputTextControlStr(languageText,InputCtrlId_languageText,"lblFieldId_"+InputCtrlId_languageText,"lblFieldIdWarning_"+InputCtrlId_languageText);
		iReturnValidateValue=validateInputTextControlStr(languageText,InputCtrlId_spokenLanguage,"lblFieldId_"+InputCtrlId_spokenLanguage,"lblFieldIdWarning_"+InputCtrlId_spokenLanguage);
		iReturnValidateValue=validateInputTextControlStr(languageText,InputCtrlId_languageText,"lblFieldId_"+InputCtrlId_languageText,"lblFieldIdWarning_"+InputCtrlId_languageText);
		if(iReturnValidateValue=="1")
		{
			Lan_Section=Lan_Section+1;
		}
	}
	else
	{
		iReturnValidateValue=validateInputListControlStr(spokenLanguage,InputCtrlId_spokenLanguage,"lblFieldId_"+InputCtrlId_spokenLanguage,"lblFieldIdWarning_"+InputCtrlId_spokenLanguage);
		if(iReturnValidateValue=="1")
		{
			Lan_Section=Lan_Section+1;
		}
	}
	
	var InputCtrlId_oralSkills="oralSkills";
	var oralSkills=getInputCtrlTextValue(InputCtrlId_oralSkills);
	iReturnValidateValue=validateInputListControlStr(oralSkills,InputCtrlId_oralSkills,"lblFieldId_"+InputCtrlId_oralSkills,"lblFieldIdWarning_"+InputCtrlId_oralSkills);
	if(iReturnValidateValue=="1")
	{
		Lan_Section=Lan_Section+1;
	}
	
	var InputCtrlId_writtenSkills="writtenSkills";
	var writtenSkills=getInputCtrlTextValue(InputCtrlId_writtenSkills);
	iReturnValidateValue=validateInputListControlStr(writtenSkills,InputCtrlId_writtenSkills,"lblFieldId_"+InputCtrlId_writtenSkills,"lblFieldIdWarning_"+InputCtrlId_writtenSkills);
	if(iReturnValidateValue=="1")
	{
		Lan_Section=Lan_Section+1;
	}
	
	if(Lan_Section>0)
	{
		window.location.hash = '#lblSectionId_'+Lan_Section_Name;
		$("#lblSectionId_"+Lan_Section_Name).addClass("DSPQRequired12");
		return Lan_Section;
		
	}
	else
	{
		var teacherLanguageId=getInputCtrlTextValue("teacherLanguageId");
		
		$("#lblSectionId_"+Lan_Section_Name).removeClass("DSPQRequired12");
		setDefColortoErrorMsgToTchLang();
		DSPQServiceAjax.insertOrUpdateTchrLang(languageText,oralSkills,writtenSkills,teacherLanguageId,spokenLanguage,
		{ 
				async: false,
				errorHandler:handleError,
				callback: function(data)
				{
					hideTchrLang();			
					displayTchrLanguage();
					window.location.hash = '#lblSectionId_'+Lan_Section_Name;
					return Lan_Section;
				}
		});
	}
}

function delTeacherLanguage(id)
{
	document.getElementById("langdelID").value=id;
	$('#deleteLangRecord').modal('show');
}

function delLangTeacher()
{
	$('#deleteLangRecord').modal('hide');
	var langdelID=document.getElementById("langdelID").value;
		DSPQServiceAjax.delTeacherLanguage(langdelID,
		{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			hideTchrLang();			
			displayTchrLanguage();
		}});
}

function editTeacherLanguage(id){
	
	DSPQServiceAjax.showEditLang(id,
		{ 
			async: false,
			errorHandler:handleError,
			callback: function(data)
			{		
				showLanguageForm();
				if(data.language!=null && data.language!='')
				{
					try { $("#spokenLanguage").val(-1); } catch (e) {}
					try { $("#languageText").val(data.language); } catch (e) {}
				}
				else if(data.spokenLanguageMaster!=null)
				{
					try { $("#spokenLanguage").val(data.spokenLanguageMaster.spokenlanguagemasterid); } catch (e) {}
					
				}
				
				hideShowSpknLng();
				
				try { $("#oralSkills").val(data.oralSkills); } catch (e) {}
				try { $("#writtenSkills").val(data.writtenSkills); } catch (e) {}
				try { $("#teacherLanguageId").val(data.teacherLanguageId); } catch (e) {}
				
			}
		});

		return false;
		
}


function downloadTranscript(academicId, linkId)
{	
	DSPQServiceAjax.downloadTranscript(academicId,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	if(data=="")
			{
				data="javascript:void(0)";
			}
			else if(data.indexOf(".doc")!=-1)
			{
				document.getElementById("ifrmTrans").src=data;
			}
			else
			{
				document.getElementById(linkId).href = data;
			}
			return false;
		}});
}

function removeTranscriptL2()
{
	var academicId = document.getElementById("academicId").value;
	$('#deleteAcademicTrans').modal('hide');
	DSPQServiceAjax.removeTranscript(academicId,
	{ 
		async: false,
		errorHandler:handleError,
		callback: function(data)
		{	
			showGridAcademics();
			document.getElementById("divResumeSection").style.display="none";
			document.getElementById("aca_file").value="0";
		}
	});
}
function removeTranscript()
{
	$('#deleteAcademicTrans').modal('show');
}

function insertOrUpdate_Academics(UploadedFileName)
{
	//alert("UploadedFileName 2 "+UploadedFileName);
	
	isSetFocus=false;
	var iReturnValidateTotalValue=0;
	var iReturnValidateValue=0;
	
	var Academic_Section=0;
	var Academic_Section_Name="tooltipSection06";
	
	if($("#divAcademicRow").is(':visible'))
	{
		var InputCtrlId_degreeId="degreeId";
		var InputCtrlIdAutoSuggest_degreeName="degreeName";
		var degreeId=getInputCtrlTextValue(InputCtrlId_degreeId);
		iReturnValidateValue=validateInputTextControlStrAutoSuggest(degreeId,InputCtrlId_degreeId,"lblFieldId_"+InputCtrlIdAutoSuggest_degreeName,"lblFieldIdWarning_"+InputCtrlIdAutoSuggest_degreeName,InputCtrlIdAutoSuggest_degreeName);
		if(iReturnValidateValue=="1")
		{
			Academic_Section=Academic_Section+1;
			iReturnValidateTotalValue=iReturnValidateTotalValue+Academic_Section;
		}
		
		
		var InputCtrlId_universityId="universityId";
		var InputCtrlIdAutoSuggest_universityName="universityName";
		var universityId=getInputCtrlTextValue(InputCtrlId_universityId);
		iReturnValidateValue=validateInputTextControlStrAutoSuggest(universityId,InputCtrlId_universityId,"lblFieldId_"+InputCtrlIdAutoSuggest_universityName,"lblFieldIdWarning_"+InputCtrlIdAutoSuggest_universityName,InputCtrlIdAutoSuggest_universityName);
		if(iReturnValidateValue=="1")
		{
			Academic_Section=Academic_Section+1;
			iReturnValidateTotalValue=iReturnValidateTotalValue+Academic_Section;
		}
		
		var InputCtrlId_fieldId="fieldId";
		var InputCtrlIdAutoSuggest_fieldName="fieldName";
		var fieldId=getInputCtrlTextValue(InputCtrlId_fieldId);
		iReturnValidateValue=validateInputTextControlStrAutoSuggest(fieldId,InputCtrlId_fieldId,"lblFieldId_"+InputCtrlIdAutoSuggest_fieldName,"lblFieldIdWarning_"+InputCtrlIdAutoSuggest_fieldName,InputCtrlIdAutoSuggest_fieldName);
		if(iReturnValidateValue=="1")
		{
			Academic_Section=Academic_Section+1;
			iReturnValidateTotalValue=iReturnValidateTotalValue+Academic_Section;
		}
		
		var InputCtrlId_Dates_Attended="attendedInYear";
		var InputCtrlId_leftInYear="leftInYear";
		
		var attendedInYear=getInputCtrlTextValue(InputCtrlId_Dates_Attended);
		var leftInYear=getInputCtrlTextValue(InputCtrlId_leftInYear);
		iReturnValidateValue=validateInputListControlToAndFromYear(attendedInYear,leftInYear,InputCtrlId_Dates_Attended,"lblFieldId_"+InputCtrlId_Dates_Attended,"lblFieldIdWarning_"+InputCtrlId_Dates_Attended);
		if(iReturnValidateValue=="1")
		{
			Academic_Section=Academic_Section+1;
			iReturnValidateTotalValue=iReturnValidateTotalValue+Academic_Section;
		}
		
		var degreeType = document.getElementById("degreeType").value;
		
		//GPA/CGPA
		var gpaCumulative;
		var gpaFreshmanYear;
		var gpaSophomoreYear;
		var gpaJuniorYear;
		var gpaSeniorYear;
		
		if($("#divGPARow").is(':visible'))
		{
			var InputCtrlId_gpaFreshmanYear="gpaFreshmanYear";
			gpaFreshmanYear=getInputCtrlTextValue(InputCtrlId_gpaFreshmanYear);
			iReturnValidateValue=validateInputTextControlStr(gpaFreshmanYear,InputCtrlId_gpaFreshmanYear,"lblFieldId_"+InputCtrlId_gpaFreshmanYear,"lblFieldIdWarning_"+InputCtrlId_gpaFreshmanYear);
			if(iReturnValidateValue=="1")
			{
				Academic_Section=Academic_Section+1;
				iReturnValidateTotalValue=iReturnValidateTotalValue+Academic_Section;
			}
			
			var InputCtrlId_gpaSophomoreYear="gpaSophomoreYear";
			var gpaSophomoreYear=getInputCtrlTextValue(InputCtrlId_gpaSophomoreYear);
			iReturnValidateValue=validateInputTextControlStr(gpaSophomoreYear,InputCtrlId_gpaSophomoreYear,"lblFieldId_"+InputCtrlId_gpaSophomoreYear,"lblFieldIdWarning_"+InputCtrlId_gpaSophomoreYear);
			if(iReturnValidateValue=="1")
			{
				Academic_Section=Academic_Section+1;
				iReturnValidateTotalValue=iReturnValidateTotalValue+Academic_Section;
			}
			
			var InputCtrlId_gpaJuniorYear="gpaJuniorYear";
				gpaJuniorYear=getInputCtrlTextValue(InputCtrlId_gpaJuniorYear);
			iReturnValidateValue=validateInputTextControlStr(gpaJuniorYear,InputCtrlId_gpaJuniorYear,"lblFieldId_"+InputCtrlId_gpaJuniorYear,"lblFieldIdWarning_"+InputCtrlId_gpaJuniorYear);
			if(iReturnValidateValue=="1")
			{
				Academic_Section=Academic_Section+1;
				iReturnValidateTotalValue=iReturnValidateTotalValue+Academic_Section;
			}
			
			var InputCtrlId_gpaSeniorYear="gpaSeniorYear";
			    gpaSeniorYear=getInputCtrlTextValue(InputCtrlId_gpaJuniorYear);
			iReturnValidateValue=validateInputTextControlStr(gpaSeniorYear,InputCtrlId_gpaSeniorYear,"lblFieldId_"+InputCtrlId_gpaSeniorYear,"lblFieldIdWarning_"+InputCtrlId_gpaSeniorYear);
			if(iReturnValidateValue=="1")
			{
				Academic_Section=Academic_Section+1;
				iReturnValidateTotalValue=iReturnValidateTotalValue+Academic_Section;
			}
			
			var InputCtrlId_gpaCumulative="gpaCumulative";
			    gpaCumulative=getInputCtrlTextValue(InputCtrlId_gpaCumulative);
			iReturnValidateValue=validateInputTextControlStr(gpaCumulative,InputCtrlId_gpaCumulative,"lblFieldId_"+InputCtrlId_gpaCumulative,"lblFieldIdWarning_"+InputCtrlId_gpaCumulative);
			if(iReturnValidateValue=="1")
			{
				Academic_Section=Academic_Section+1;
				iReturnValidateTotalValue=iReturnValidateTotalValue+Academic_Section;
			}
			
		}

		if($("#divGPARowOther").is(':visible'))
		{
			var InputCtrlId_gpaCumulative="gpaCumulative1";
			gpaCumulative=getInputCtrlTextValue(InputCtrlId_gpaCumulative);
			iReturnValidateValue=validateInputTextControlStr(gpaCumulative,InputCtrlId_gpaCumulative,"lblFieldId_"+InputCtrlId_gpaCumulative,"lblFieldIdWarning_"+InputCtrlId_gpaCumulative);
			if(iReturnValidateValue=="1")
			{
				Academic_Section=Academic_Section+1;
				iReturnValidateTotalValue=iReturnValidateTotalValue+Academic_Section;
			}
			
		}
		
		
		var pathOfTranscript="";
		if(UploadedFileName==undefined)
		{
			var aca_file=getInputCtrlTextValue("aca_file");
			var InputCtrlId_pathOfTranscript="pathOfTranscript";
			pathOfTranscript=getInputCtrlTextValue(InputCtrlId_pathOfTranscript);
			iReturnValidateValue=validateInputFileControlStr(pathOfTranscript,InputCtrlId_pathOfTranscript,"lblFieldId_"+InputCtrlId_pathOfTranscript,"lblFieldIdWarning_"+InputCtrlId_pathOfTranscript,aca_file);
			if(iReturnValidateValue=="1")
			{
				Academic_Section=Academic_Section+1;
				iReturnValidateTotalValue=iReturnValidateTotalValue+Academic_Section;
			}
		}
		
	if(iReturnValidateValue=="1")
	{
		Academic_Section=Academic_Section+1;
		iReturnValidateTotalValue=iReturnValidateTotalValue+Academic_Section;
	}
	
	if(Academic_Section>0)
	{
		window.location.hash = '#lblSectionId_'+Academic_Section_Name;
		$("#lblSectionId_"+Academic_Section_Name).addClass("DSPQRequired12");
		Academic_Section=0;
	}
	else
	{
		$("#lblSectionId_"+Academic_Section_Name).removeClass("DSPQRequired12");
	}
	
	if(iReturnValidateTotalValue==0 && $("#divAcademicRow").is(':visible')==true)
	{
		$('#loadingDiv').show();
		if(pathOfTranscript!="")
		{
			//$('#loadingDiv').show();
			document.getElementById("frmAcadamin").submit();
		}
		else{
			DSPQServiceAjax.saveOrUpdateAcademics(document.getElementById("academicId").value,degreeId, universityId, fieldId, attendedInYear, leftInYear, gpaCumulative, gpaFreshmanYear, gpaSophomoreYear, gpaJuniorYear, gpaSeniorYear,degreeType,UploadedFileName,{ 
			async: false,
			callback: function(data)
			{
				showGridAcademics();
				resetUniversityForm();
			 },
			 errorHandler:handleError  
		  });
		}
		$('#loadingDiv').hide();
	}
	
	}
	return iReturnValidateTotalValue;
}

function saveAcademics(fileName,sbtsource)
{ 
	insertOrUpdate_Academics(fileName);
}

function showGridAcademics()
{
	var isMiami=0;
	try { isMiami=document.getElementById("isMiami").value; } catch (e) {}
	
	var dspqjobflowdistrictId="";
	try { dspqjobflowdistrictId=document.getElementById("dspqjobflowdistrictId").value; } catch (e) {}
	
	var transcriptFlag="";
	try { transcriptFlag=trim(document.getElementById("academicTranscriptFlag").value); } catch (e) {}
	var teacherId=0;
	var sectionId=6;

		DSPQServiceAjax.getPFAcademinGridForDspq(dp_Academics_noOfRows,dp_Academics_page,dp_Academics_sortOrderStr,dp_Academics_sortOrderType,isMiami,transcriptFlag,teacherId,dspqPortfolioNameId,candidateType,sectionId,{ 
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{
				$('#divDataGridAcademin').html(data);
				applyScrollOnTbl_Academic();
			}});

}

function chkForEnter_Academic(evt)
{	
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==13)
	{
		//insertOrUpdate_Academic();
	}	
}


///// Academic
function showUniversityForm()
{
	document.getElementById("insertOrUpdateLink").value='';
	document.getElementById("frmAcadamin").reset();
	document.getElementById("academicId").value="";
	
	try{ document.getElementById("divGPARowOther").style.display="none"; }catch (e){}
	try{ document.getElementById("divResumeSection").style.display="none"; }catch (e){}
	try{ document.getElementById("divAcademicRow").style.display="block"; }catch (e){}
	try{ document.getElementById("divGPARow").style.display="none"; }catch (e){}
	
	try{ document.getElementById("aca_file").value="0"; }catch (e){}
	try{ document.getElementById("degreeName").focus(); }catch (e){}
	try{ document.getElementById("universityId").value=""; }catch (e){}
	try{ document.getElementById("fieldId").value=""; }catch (e){}
	
	resetLabels("degreeName");
	resetLabels("universityName");
	resetLabels("fieldName");
	resetLabels("attendedInYear");
	resetLabels("pathOfTranscript");
	//resetLabels("degreeName");
	//resetLabels("degreeName");
	
	try { document.getElementById("frmGPA").reset(); } catch (e) {}
	try { document.getElementById("frmGPA1").reset(); } catch (e) {}
	
	return false;
}

function resetUniversityForm()
{
	showUniversityForm();
	document.getElementById("divAcademicRow").style.display="none";
}


function resetLabels(Id1)
{
	try { $("#lblFieldId_"+Id1).removeClass("DSPQRequired12"); } catch (e) { }
	try { $("#lblFieldIdWarning_"+Id1).removeClass("icon-warning DSPQRequired12");  } catch (e) { }
	try { $("#lblFieldIdWarningBeforeSpace_"+Id1).html("");	} catch (e) { }
	try { $("#lblFieldIdWarningAfterSpace_"+Id1).html("");} catch (e) { }
}

function hideTFAFieldsNJAF()
{
	try { $('#errordivExp').empty(); } catch (e) { }
	try { $('#expCertTeacherTraining').css("background-color",""); } catch (e) { }
	try { $('#tfaAffiliate').css("background-color",""); } catch (e) { }
	try { $('#corpsYear').css("background-color",""); } catch (e) { }
	try { $('#tfaRegion').css("background-color",""); } catch (e) { }
	
	var InputCtrlId_tfaAffiliate="tfaAffiliate";
	tfaAffiliate=getInputCtrlTextValue(InputCtrlId_tfaAffiliate);
	if(tfaAffiliate=="3")
	{
		try { document.getElementById("corpsYear").value="0"; } catch (e) {}
		try { document.getElementById("tfaRegion").value="0"; } catch (e) {}
		try { $("#tfaFieldsDiv").fadeOut(); } catch (e) {}
	}
	else
	{
		try { $("#tfaFieldsDiv").fadeIn(); } catch (e) {}
	}
}

function saveCertification(fileName,sbtsource)
{ 
	insertOrUpdate_Certification(fileName);
}

function insertOrUpdate_Certification(UploadedFileName)
{
	isSetFocus=false;
	var iReturnValidateTotalValue=0;
	var iReturnValidateValue=0;
	
	var CertificationLicensure_Section=0;
	var CertificationLicensure_Section_Name="tooltipSection8";
	
	if($("#divMainForm").is(':visible'))
	{
		
		var InputCtrlId_certId="certId";
		var certId=getInputCtrlTextValue(InputCtrlId_certId);
		if(certId==null || certId=="")
			certId=null;
		
		var InputCtrlId_certificationStatusMaster="certificationStatusMaster";
		var certificationStatusMaster=getInputCtrlTextValue(InputCtrlId_certificationStatusMaster);
		iReturnValidateValue=validateInputListControlStr(certificationStatusMaster,InputCtrlId_certificationStatusMaster,"lblFieldId_"+InputCtrlId_certificationStatusMaster,"lblFieldIdWarning_"+InputCtrlId_certificationStatusMaster);
		if(iReturnValidateValue=="1")
		{
			CertificationLicensure_Section=CertificationLicensure_Section+1;
			iReturnValidateTotalValue=iReturnValidateTotalValue+CertificationLicensure_Section;
		}
		
		var InputCtrlId_certificationtypeMaster="certificationtypeMaster";
		var certificationtypeMaster=getInputCtrlTextValue(InputCtrlId_certificationtypeMaster);
		iReturnValidateValue=validateInputListControlStr(certificationtypeMaster,InputCtrlId_certificationtypeMaster,"lblFieldId_"+InputCtrlId_certificationtypeMaster,"lblFieldIdWarning_"+InputCtrlId_certificationtypeMaster);
		//alert("certificationtypeMaster "+certificationtypeMaster +" iReturnValidateValue "+iReturnValidateValue);
		if(iReturnValidateValue=="1")
		{
			CertificationLicensure_Section=CertificationLicensure_Section+1;
			iReturnValidateTotalValue=iReturnValidateTotalValue+CertificationLicensure_Section;
		}
		
		var InputCtrlId_stateMasterCert="stateMaster";
		var stateMasterCert=getInputCtrlTextValue(InputCtrlId_stateMasterCert);
		iReturnValidateValue=validateInputListControlStr(stateMasterCert,InputCtrlId_stateMasterCert,"lblFieldId_"+InputCtrlId_stateMasterCert,"lblFieldIdWarning_"+InputCtrlId_stateMasterCert);
		if(iReturnValidateValue=="1")
		{
			CertificationLicensure_Section=CertificationLicensure_Section+1;
			iReturnValidateTotalValue=iReturnValidateTotalValue+CertificationLicensure_Section;
		}
		
		
		var InputCtrlId_yearReceivedCert="yearReceived";
		var yearReceivedCert=getInputCtrlTextValue(InputCtrlId_yearReceivedCert);
		iReturnValidateValue=validateInputListControlStr(yearReceivedCert,InputCtrlId_yearReceivedCert,"lblFieldId_"+InputCtrlId_yearReceivedCert,"lblFieldIdWarning_"+InputCtrlId_yearReceivedCert);
		if(iReturnValidateValue=="1")
		{
			CertificationLicensure_Section=CertificationLicensure_Section+1;
			iReturnValidateTotalValue=iReturnValidateTotalValue+CertificationLicensure_Section;
		}
		
		var InputCtrlId_yearexpiresCert="yearexpires";
		var yearexpiresCert=getInputCtrlTextValue(InputCtrlId_yearexpiresCert);
		iReturnValidateValue=validateInputListControlStr(yearexpiresCert,InputCtrlId_yearexpiresCert,"lblFieldId_"+InputCtrlId_yearexpiresCert,"lblFieldIdWarning_"+InputCtrlId_yearexpiresCert);
		if(iReturnValidateValue=="1")
		{
			CertificationLicensure_Section=CertificationLicensure_Section+1;
			iReturnValidateTotalValue=iReturnValidateTotalValue+CertificationLicensure_Section;
		}
		
		var InputCtrlId_certificateTypeMaster="certificateTypeMaster";
		var InputCtrlIdAutoSuggest_certType="certType";
		var certType=getInputCtrlTextValue(InputCtrlId_certificateTypeMaster);
		iReturnValidateValue=validateInputTextControlStrAutoSuggest(certType,InputCtrlId_certificateTypeMaster,"lblFieldId_"+InputCtrlIdAutoSuggest_certType,"lblFieldIdWarning_"+InputCtrlIdAutoSuggest_certType,InputCtrlIdAutoSuggest_certType);
		if(iReturnValidateValue=="1")
		{
			CertificationLicensure_Section=CertificationLicensure_Section+1;
			iReturnValidateTotalValue=iReturnValidateTotalValue+CertificationLicensure_Section;
		}
		
		var InputCtrlId_doenumber="doenumber";
		var doenumber=getInputCtrlTextValue(InputCtrlId_doenumber);
		iReturnValidateValue=validateInputTextControlStr(doenumber,InputCtrlId_doenumber,"lblFieldId_"+InputCtrlId_doenumber,"lblFieldIdWarning_"+InputCtrlId_doenumber);
		if(iReturnValidateValue=="1")
		{
			CertificationLicensure_Section=CertificationLicensure_Section+1;
			iReturnValidateTotalValue=iReturnValidateTotalValue+CertificationLicensure_Section;
		}
		
		var InputCtrlId_certUrl="certUrl";
		var certUrl=getInputCtrlTextValue(InputCtrlId_certUrl);
		iReturnValidateValue=validateInputTextControlStr(certUrl,InputCtrlId_certUrl,"lblFieldId_"+InputCtrlId_certUrl,"lblFieldIdWarning_"+InputCtrlId_certUrl);
		if(iReturnValidateValue=="1")
		{
			CertificationLicensure_Section=CertificationLicensure_Section+1;
			iReturnValidateTotalValue=iReturnValidateTotalValue+CertificationLicensure_Section;
		}
		var InputCtrlId_certExp="certExp";
		var certExp=getInputCtrlTextValue(InputCtrlId_certExp);
		iReturnValidateValue=validateInputTextControlStr(certExp,InputCtrlId_certExp,"lblFieldId_"+InputCtrlId_certExp,"lblFieldIdWarning_"+InputCtrlId_certExp);
		if(iReturnValidateValue=="1")
		{
			CertificationLicensure_Section=CertificationLicensure_Section+1;
			iReturnValidateTotalValue=iReturnValidateTotalValue+CertificationLicensure_Section;
		}
		
		var GradeLevel=0;
		var chk_pkOffered=getInputCtrlCheckBoxValueGrade("pkOffered");
		GradeLevel=GradeLevel+chkGradeBlank(chk_pkOffered);
		//alert("chk_pkOffered "+chk_pkOffered +" GradeLevel "+GradeLevel);
		var chk_kgOffered=getInputCtrlCheckBoxValueGrade("kgOffered");
		GradeLevel=GradeLevel+chkGradeBlank(chk_kgOffered);
		var chk_g01Offered=getInputCtrlCheckBoxValueGrade("g01Offered");
		GradeLevel=GradeLevel+chkGradeBlank(chk_g01Offered);
		var chk_g02Offered=getInputCtrlCheckBoxValueGrade("g02Offered");
		GradeLevel=GradeLevel+chkGradeBlank(chk_g02Offered);
		var chk_g03Offered=getInputCtrlCheckBoxValueGrade("g03Offered");
		GradeLevel=GradeLevel+chkGradeBlank(chk_g03Offered);
		var chk_g04Offered=getInputCtrlCheckBoxValueGrade("g04Offered");
		GradeLevel=GradeLevel+chkGradeBlank(chk_g04Offered);
		var chk_g05Offered=getInputCtrlCheckBoxValueGrade("g05Offered");
		GradeLevel=GradeLevel+chkGradeBlank(chk_g05Offered);
		var chk_g06Offered=getInputCtrlCheckBoxValueGrade("g06Offered");
		GradeLevel=GradeLevel+chkGradeBlank(chk_g06Offered);
		var chk_g07Offered=getInputCtrlCheckBoxValueGrade("g07Offered");
		GradeLevel=GradeLevel+chkGradeBlank(chk_g07Offered);
		var chk_g08Offered=getInputCtrlCheckBoxValueGrade("g08Offered");
		GradeLevel=GradeLevel+chkGradeBlank(chk_g08Offered);
		var chk_g09Offered=getInputCtrlCheckBoxValueGrade("g09Offered");
		GradeLevel=GradeLevel+chkGradeBlank(chk_g09Offered);
		var chk_g10Offered=getInputCtrlCheckBoxValueGrade("g10Offered");
		GradeLevel=GradeLevel+chkGradeBlank(chk_g10Offered);
		var chk_g11Offered=getInputCtrlCheckBoxValueGrade("g11Offered");
		GradeLevel=GradeLevel+chkGradeBlank(chk_g11Offered);
		var chk_g12Offered=getInputCtrlCheckBoxValueGrade("g12Offered");
		GradeLevel=GradeLevel+chkGradeBlank(chk_g12Offered);
		
		if(GradeLevel >0)
		{
			try {
				document.getElementById("GradeLevel").value=GradeLevel;
			} catch (e) {}
		}
		else
		{
			try {
				document.getElementById("GradeLevel").value='';
			} catch (e) {}
		}
		
		var InputCtrlId_GradeLevel="GradeLevel";
		var GradeLevel=getInputCtrlTextValue(InputCtrlId_GradeLevel);
		iReturnValidateValue=validateInputTextControlStr(GradeLevel,InputCtrlId_GradeLevel,"lblFieldId_"+InputCtrlId_GradeLevel,"lblFieldIdWarning_"+InputCtrlId_GradeLevel);
		if(iReturnValidateValue=="1")
		{
			CertificationLicensure_Section=CertificationLicensure_Section+1;
			iReturnValidateTotalValue=iReturnValidateTotalValue+CertificationLicensure_Section;
		}
		
		var InputCtrlId_readingQualifyingScore="readingQualifyingScore";
		var readingQualifyingScore=getInputCtrlTextValue(InputCtrlId_readingQualifyingScore);
		var InputCtrlId_writingQualifyingScore="writingQualifyingScore";
		var writingQualifyingScore=getInputCtrlTextValue(InputCtrlId_writingQualifyingScore);
		var InputCtrlId_mathematicsQualifyingScore="mathematicsQualifyingScore";
		var mathematicsQualifyingScore=getInputCtrlTextValue(InputCtrlId_writingQualifyingScore);
		if($("#praxisArea").is(':visible')==true)
		{
			iReturnValidateValue=validateInputTextControlStr(readingQualifyingScore,InputCtrlId_readingQualifyingScore,"lblFieldId_"+InputCtrlId_readingQualifyingScore,"lblFieldIdWarning_"+InputCtrlId_readingQualifyingScore);
			if(iReturnValidateValue=="1")
			{
				CertificationLicensure_Section=CertificationLicensure_Section+1;
				iReturnValidateTotalValue=iReturnValidateTotalValue+CertificationLicensure_Section;
			}
			
			iReturnValidateValue=validateInputTextControlStr(writingQualifyingScore,InputCtrlId_writingQualifyingScore,"lblFieldId_"+InputCtrlId_writingQualifyingScore,"lblFieldIdWarning_"+InputCtrlId_writingQualifyingScore);
			if(iReturnValidateValue=="1")
			{
				CertificationLicensure_Section=CertificationLicensure_Section+1;
				iReturnValidateTotalValue=iReturnValidateTotalValue+CertificationLicensure_Section;
			}
			
			iReturnValidateValue=validateInputTextControlStr(mathematicsQualifyingScore,InputCtrlId_mathematicsQualifyingScore,"lblFieldId_"+InputCtrlId_mathematicsQualifyingScore,"lblFieldIdWarning_"+InputCtrlId_mathematicsQualifyingScore);
			if(iReturnValidateValue=="1")
			{
				CertificationLicensure_Section=CertificationLicensure_Section+1;
				iReturnValidateTotalValue=iReturnValidateTotalValue+CertificationLicensure_Section;
			}
		}
		
		var pathOfCertificationFile="";
		if(UploadedFileName==undefined)
		{
			var pathOfCertification=getInputCtrlTextValue("pathOfCertification");
			var InputCtrlId_pathOfCertificationFile="pathOfCertificationFile";
			pathOfCertificationFile=getInputCtrlTextValue(InputCtrlId_pathOfCertificationFile);
			iReturnValidateValue=validateInputFileControlStr(pathOfCertificationFile,InputCtrlId_pathOfCertificationFile,"lblFieldId_"+InputCtrlId_pathOfCertificationFile,"lblFieldIdWarning_"+InputCtrlId_pathOfCertificationFile,pathOfCertification);
			if(iReturnValidateValue=="1")
			{
				CertificationLicensure_Section=CertificationLicensure_Section+1;
				iReturnValidateTotalValue=iReturnValidateTotalValue+CertificationLicensure_Section;
			}
		}
		
		if(CertificationLicensure_Section>0)
		{
			
			//window.location.hash = '#lblSectionId_'+CertificationLicensure_Section_Name;
			$("#lblSectionId_"+CertificationLicensure_Section_Name).addClass("DSPQRequired12");
			CertificationLicensure_Section=0;
		}
		else
		{
			$("#lblSectionId_"+CertificationLicensure_Section_Name).removeClass("DSPQRequired12");
		}
		
		if(iReturnValidateTotalValue==0 && $("#divMainForm").is(':visible')==true)
		{
			$('#loadingDiv').show();
			if(pathOfCertificationFile!="")
			{

				document.getElementById("frmCertificate").submit();
			}
			else
			{

				var stateObj = {stateId:stateMasterCert};
				var certificationStatusMasterObj= {certificationStatusId:certificationStatusMaster};
				var certificationtypeMasterObj = {certificationTypeMasterId:certificationtypeMaster};
				var certificateTypeMasteronj={certTypeId:certType};
				
				var certificate =null;
				certificate = {
						certId:certId,
						certificationStatusMaster:certificationStatusMasterObj,
						certificationTypeMaster:certificationtypeMasterObj,
						certificateTypeMaster:certificateTypeMasteronj,
						certUrl:certUrl,
						stateMaster:stateObj,
						yearReceived:yearReceivedCert,
						yearExpired:yearexpiresCert,
						certType:certType,
						pkOffered:chk_pkOffered,
						kgOffered:chk_kgOffered,
						g01Offered:chk_g01Offered,
						g02Offered:chk_g02Offered,
						g03Offered:chk_g03Offered,
						g04Offered:chk_g04Offered,
						g05Offered:chk_g05Offered,
						g06Offered:chk_g06Offered,
						g07Offered:chk_g07Offered,
						g08Offered:chk_g08Offered,
						g09Offered:chk_g09Offered,
						g10Offered:chk_g10Offered,
						g11Offered:chk_g11Offered,
						g12Offered:chk_g12Offered,
						readingQualifyingScore:readingQualifyingScore,
						writingQualifyingScore:writingQualifyingScore,
						mathematicsQualifyingScore:mathematicsQualifyingScore,
						doeNumber:doenumber,
						certiExpiration:certExp
						};
				
				if(UploadedFileName==null || UploadedFileName=="" || UploadedFileName==undefined)
					UploadedFileName=getInputCtrlTextValue("pathOfCertification");
				
				DSPQServiceAjax.saveOrUpdateCert(certificate,UploadedFileName,{
				async: true,
				errorHandler:handleError,
				callback:function(data)
				{
					hideForm_Certification();
					showGridCredentials();
				}
				});
			
			}
			$('#loadingDiv').hide();
		}
		
	}
	
	
	
	
	
	return iReturnValidateTotalValue;
	
}

function chkNBCert(chk)
{
	if(chk=="0")
	{
		document.getElementById("divNBCY").style.display="none";
	}
	else if(chk=="1")
	{
		document.getElementById("divNBCY").style.display="block";
	}
}

function chkForEnter_Certifications(evt)
{	
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==13)
	{
		insertOrUpdate_Certification();
	}	
}

function getPraxis()
{
	
	
	var jobcategoryName="";
	if ($('#jobCategoryName').length > 0) {
		jobcategoryName=document.getElementById("jobCategoryName").value;
	}
	
	var InputCtrlId_stateMasterCert="stateMaster";
	var stateMasterId=getInputCtrlTextValue(InputCtrlId_stateMasterCert);
	var dspqjobflowdistrictId="";
	try { dspqjobflowdistrictId=document.getElementById("dspqjobflowdistrictId").value; } catch (e) {}
	if(dspqjobflowdistrictId!=4218990 || jobcategoryName!='Administrative Non-Instructional')
	{
		var statemaster = {stateId:stateMasterId};
		DSPQServiceAjax.getPraxis(statemaster,{ 
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{
				if(data!=null)
				{
					$("#praxisArea").show();
					$("#reading").html(data.praxisIReading);
					$("#writing").html(data.praxisIWriting);
					$("#maths").html(data.praxisIMathematics);
				}else
				{
					$("#praxisArea").hide();
					$("#reading").html("");
					$("#writing").html("");
					$("#maths").html("");
				}
		}});
	}
}

function clearCertType()
{
	try { $("#certificateTypeMaster").val(""); 	} catch (e) {}
	try { $("#certType").val(""); 	} catch (e) {}
	
}

function setDefColortoErrorMsg_Certification()
{
	try { $('#certificationtypeMaster').css("background-color","");	} catch (e) {}
	try {  $('#stateMaster').css("background-color","");	} catch (e) {}
	try { $('#yearReceived').css("background-color",""); 	} catch (e) {}
	try {  $('#yearexpires').css("background-color","");	} catch (e) {}
	try { $('#certType').css("background-color",""); 	} catch (e) {}
	try { $('#certName').css("background-color",""); 	} catch (e) {}
	try { $('#certificationStatusMaster').css("background-color",""); 	} catch (e) {}
	try { $('#certUrl').css("background-color",""); 	} catch (e) {}
	
	var dspqjobflowdistrictId="";
	try { dspqjobflowdistrictId=document.getElementById("dspqjobflowdistrictId").value; } catch (e) {}
	
	try {  
		if(dspqjobflowdistrictId==7800040)
		{
			$('#IEINNumber').css("background-color","");
		}
	} catch (e) {}
	
}


function chkGradeBlank(chkValue)
{
	var returnValue=0;
	if(chkValue!=null && chkValue==true)
		returnValue=1
	return returnValue;
}

function deleteRecord_Certificate(id)
{	
	try {
		document.getElementById("certificateTypeID").value=id;
	} catch (e) {
		// TODO: handle exception
	}
	$('#deleteCertRec').modal('show');
}
function deleteCertificateRecord()
{
	var id=document.getElementById("certificateTypeID").value;
	DSPQServiceAjax.deleteRecordCert(id,{ 
	async: true,
	errorHandler:handleError,
	callback:function(data)
	{
		$('#deleteCertRec').modal('hide');
		hideForm_Certification();
		showGridCredentials();
		return false;
	}});
}

function showEditForm(id)
{
	var certificate = {certId:null, certificationStatusMaster:null,stateMaster:null,yearReceived:null,certType:null,pkOffered:null,kgOffered:null,g01Offered:null,g02Offered:null,g03Offered:null,g04Offered:null,g05Offered:null,g06Offered:null,g07Offered:null,g08Offered:null,g09Offered:null,g10Offered:null,g11Offered:null,g12Offered:null,canServeAsSubTeacher:null};
	DSPQServiceAjax.showEditFormCert(id,
	{ 
		async: false,
		errorHandler:handleError,
		callback: function(data)
		{
			showForm_Certification();
						
			document.getElementById("certId").value=data.certId;
			if(data.stateMaster!=null && data.stateMaster.stateId!=null)
			{
				try { document.getElementById("stateMaster").value=data.stateMaster.stateId; } catch (e) {}
			}
			
			if(data.certificationStatusMaster!=null)
				try { document.getElementById("certificationStatusMaster").value=data.certificationStatusMaster.certificationStatusId; } catch (e) {}
			else
				try { document.getElementById("certificationStatusMaster").value=""; } catch (e) {}
				
			
			if(data.certificationTypeMaster!=null)
				try { document.getElementById("certificationtypeMaster").value=data.certificationTypeMaster.certificationTypeMasterId; } catch (e) {}
			else
				try { document.getElementById("certificationtypeMaster").value="0"; } catch (e) {}
			
			if(data.doeNumber==0)
			{
				try { document.getElementById("doenumber").value=""; } catch (e) {}
			}	
			else
			{
				try { document.getElementById("doenumber").value=data.doeNumber; } catch (e) {}
			}
			
			if(data.yearExpired==0 || data.yearExpired==null || data.yearExpired=='')
			{
				try { document.getElementById("yearexpires").text=""; } catch (e) {}
			}
			else
			{
				try { document.getElementById("yearexpires").value=data.yearExpired; } catch (e) {}
			}	
			
			try { document.getElementById("yearReceived").value=data.yearReceived; } catch (e) {}
			try { document.getElementById("certUrl").value=data.certUrl; } catch (e) {}
			try { document.getElementById("certExp").value=data.certiExpiration; } catch (e) {}
			dwr.util.setValues(data);
			
			if(data.certificateTypeMaster!=null)
			{
				try { document.getElementById("certType").value=data.certificateTypeMaster.certType; } catch (e) {}
				try { document.getElementById("certificateTypeMaster").value=data.certificateTypeMaster.certTypeId; } catch (e) {}
			}
			else
			{
				try { document.getElementById("certType").value=""; } catch (e) {}
				try { document.getElementById("certificateTypeMaster").value=""; } catch (e) {}
			}
			
			if(data.pathOfCertification!=null && data.pathOfCertification!="")
			{
				try { document.getElementById("removeCert").style.display="block"; } catch (e) {}
				try { document.getElementById("divCertName").style.display="block"; } catch (e) {}
				try { document.getElementById("pathOfCertification").value=data.pathOfCertification; } catch (e) {}
				try { 
					document.getElementById("divCertName").innerHTML="<a href='javascript:void(0)' id='hrefEditRef' onclick=\"downloadCertification('"+data.certId+"','hrefEditRef');" +
					"if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0');" +
					"return false;\">"+data.pathOfCertification+"</a>";
				} catch (e) {}
				
			}
			else
			{
				try { document.getElementById("divCertName").style.display="none"; } catch (e) {}
				try { document.getElementById("removeCert").style.display="none"; } catch (e) {}
				try { document.getElementById("pathOfCertificationFile").value=""; } catch (e) {}
				try { document.getElementById("pathOfCertification").value="0"; } catch (e) {}
			}
			//fieldsDisable(data.certificationStatusMaster.certificationStatusId);
			
			getPraxis();
			return false;
		}
	});
	return false;
}

function downloadCertification(certId, linkId)
{		
	DSPQServiceAjax.downloadCertification(certId,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data){
			if(data=="")
			{
				data="javascript:void(0)";
			}
			else if(data.indexOf(".doc")!=-1)
			{
				document.getElementById("ifrmCert").src=data;
			}else
			{
				document.getElementById(linkId).href = data;
			}
			return false;
		}});
}

function fieldsDisable(value)
{}


function saveReference(fileName,sbtsource)
{
	insertOrUpdateElectronicReferences(fileName);
}
function insertOrUpdateElectronicReferences(UploadedFileName)
{
	isSetFocus=false;
	var iReturnValidateTotalValue=0;
	var iReturnValidateValue=0;
	
	var Ref_Section=0;
	var Ref_Section_Name="tooltipSection09";
	
	if($("#divMainForm_ref").is(':visible')==true)
	{
		var InputCtrlId_elerefAutoId="elerefAutoId";
		var elerefAutoId=getInputCtrlTextValue(InputCtrlId_elerefAutoId);
		
		var InputCtrlId_salutation="salutation";
		var salutation=getInputCtrlTextValue(InputCtrlId_salutation);
		iReturnValidateValue=validateInputListControlStr(salutation,InputCtrlId_salutation,"lblFieldId_"+InputCtrlId_salutation,"lblFieldIdWarning_"+InputCtrlId_salutation);
		if(iReturnValidateValue=="1")
		{
			Ref_Section=Ref_Section+1;
			iReturnValidateTotalValue=iReturnValidateTotalValue+Ref_Section;
		}
		
		var InputCtrlId_firstName="firstName";
		var firstName=getInputCtrlTextValue(InputCtrlId_firstName);
		iReturnValidateValue=validateInputTextControlStr(firstName,InputCtrlId_firstName,"lblFieldId_"+InputCtrlId_firstName,"lblFieldIdWarning_"+InputCtrlId_firstName);
		if(iReturnValidateValue=="1")
		{
			Ref_Section=Ref_Section+1;
			iReturnValidateTotalValue=iReturnValidateTotalValue+Ref_Section;
		}
		
		var InputCtrlId_lastName="lastName";
		var lastName=getInputCtrlTextValue(InputCtrlId_lastName);
		iReturnValidateValue=validateInputTextControlStr(lastName,InputCtrlId_lastName,"lblFieldId_"+InputCtrlId_lastName,"lblFieldIdWarning_"+InputCtrlId_lastName);
		if(iReturnValidateValue=="1")
		{
			Ref_Section=Ref_Section+1;
			iReturnValidateTotalValue=iReturnValidateTotalValue+Ref_Section;
		}
		
		var InputCtrlId_designation="designation";
		var designation=getInputCtrlTextValue(InputCtrlId_designation);
		iReturnValidateValue=validateInputTextControlStr(designation,InputCtrlId_designation,"lblFieldId_"+InputCtrlId_designation,"lblFieldIdWarning_"+InputCtrlId_designation);
		if(iReturnValidateValue=="1")
		{
			Ref_Section=Ref_Section+1;
			iReturnValidateTotalValue=iReturnValidateTotalValue+Ref_Section;
		}
		
		var InputCtrlId_organization="organization";
		var organization=getInputCtrlTextValue(InputCtrlId_organization);
		iReturnValidateValue=validateInputTextControlStr(organization,InputCtrlId_organization,"lblFieldId_"+InputCtrlId_organization,"lblFieldIdWarning_"+InputCtrlId_organization);
		if(iReturnValidateValue=="1")
		{
			Ref_Section=Ref_Section+1;
			iReturnValidateTotalValue=iReturnValidateTotalValue+Ref_Section;
		}
		
		var InputCtrlId_contactnumber="contactnumber";
		var contactnumber=getInputCtrlTextValue(InputCtrlId_contactnumber);
		iReturnValidateValue=validateInputTextControlStr(contactnumber,InputCtrlId_contactnumber,"lblFieldId_"+InputCtrlId_contactnumber,"lblFieldIdWarning_"+InputCtrlId_contactnumber);
		if(iReturnValidateValue=="1")
		{
			Ref_Section=Ref_Section+1;
			iReturnValidateTotalValue=iReturnValidateTotalValue+Ref_Section;
		}
		
		
		var InputCtrlId_email="email";
		var email=getInputCtrlTextValue(InputCtrlId_email);
		iReturnValidateValue=validateInputTextControlStrEmailAddress(email,InputCtrlId_email,"lblFieldId_"+InputCtrlId_email,"lblFieldIdWarning_"+InputCtrlId_email);
		if(iReturnValidateValue=="1")
		{
			Ref_Section=Ref_Section+1;
			iReturnValidateTotalValue=iReturnValidateTotalValue+Ref_Section;
		}
		
		var pathOfReferenceFile="";
		if(UploadedFileName==undefined)
		{
			var pathOfReference=getInputCtrlTextValue("pathOfReference");
			var InputCtrlId_pathOfReferenceFile="pathOfReferenceFile";
			pathOfReferenceFile=getInputCtrlTextValue(InputCtrlId_pathOfReferenceFile);
			iReturnValidateValue=validateInputFileControlStr(pathOfReferenceFile,InputCtrlId_pathOfReferenceFile,"lblFieldId_"+InputCtrlId_pathOfReferenceFile,"lblFieldIdWarning_"+InputCtrlId_pathOfReferenceFile,pathOfReference);
			if(iReturnValidateValue=="1")
			{
				Ref_Section=Ref_Section+1;
				iReturnValidateTotalValue=iReturnValidateTotalValue+Ref_Section;
			}
		}
		
		var InputCtrlId_rdcontacted="rdcontacted";
		var rdcontacted=getInputCtrlRadioValue(InputCtrlId_rdcontacted);
		iReturnValidateValue=validateInputRadioControlStr(rdcontacted,InputCtrlId_rdcontacted,"lblFieldId_"+InputCtrlId_rdcontacted,"lblFieldIdWarning_"+InputCtrlId_rdcontacted,"hdn_"+InputCtrlId_rdcontacted);
		if(iReturnValidateValue=="1")
		{
			Ref_Section=Ref_Section+1;
			iReturnValidateTotalValue=iReturnValidateTotalValue+Ref_Section;
		}
		
		var rdcontacted_value=false;
		if(rdcontacted=="1")
			rdcontacted_value=true;
		
		var InputCtrlId_longHaveYouKnow="longHaveYouKnow";
		var longHaveYouKnow=getInputCtrlTextValue(InputCtrlId_longHaveYouKnow);
		iReturnValidateValue=validateInputTextControlStr(longHaveYouKnow,InputCtrlId_longHaveYouKnow,"lblFieldId_"+InputCtrlId_longHaveYouKnow,"lblFieldIdWarning_"+InputCtrlId_longHaveYouKnow);
		if(iReturnValidateValue=="1")
		{
			Ref_Section=Ref_Section+1;
			iReturnValidateTotalValue=iReturnValidateTotalValue+Ref_Section;
		}
		
		
		var InputCtrlId_referenceDetailText="referenceDetailText";
		var referenceDetailText=getTextAreaEditorCtrlTextValue(InputCtrlId_referenceDetailText);
		iReturnValidateValue=validateInputTextAreaControlStr(referenceDetailText,InputCtrlId_referenceDetailText,"lblFieldId_"+InputCtrlId_referenceDetailText,"lblFieldIdWarning_"+InputCtrlId_referenceDetailText);
		if(iReturnValidateValue=="1")
		{
			Ref_Section=Ref_Section+1;
			iReturnValidateTotalValue=iReturnValidateTotalValue+Ref_Section;
		}
		
		
		if(Ref_Section>0)
		{
			window.location.hash = '#lblSectionId_'+Ref_Section_Name;
			$("#lblSectionId_"+Ref_Section_Name).addClass("DSPQRequired12");
			Ref_Section=0;
		}
		else
		{
			$("#lblSectionId_"+Ref_Section_Name).removeClass("DSPQRequired12");
		}

		if(iReturnValidateTotalValue==0 && $("#divMainForm_ref").is(':visible')==true)
		{
			$('#loadingDiv').show();
			if(pathOfReferenceFile!="")
			{
				document.getElementById("frmElectronicReferences").submit();
			}
			else
			{
				var teacherElectronicReferences = {elerefAutoId:null,salutation:null, firstName:null, lastName:null, designation:null, organization:null,email:null,contactnumber:null,rdcontacted:null,canContOnOffer:null,referenceDetails:null,longHaveYouKnow:null};
				dwr.engine.beginBatch();
				dwr.util.getValues(teacherElectronicReferences);
				
				if(elerefAutoId >0)
					teacherElectronicReferences.elerefAutoId=elerefAutoId;
				
				teacherElectronicReferences.salutation=salutation;
				teacherElectronicReferences.firstName=firstName;
				teacherElectronicReferences.lastName=lastName;;
				teacherElectronicReferences.designation=designation;
				teacherElectronicReferences.organization=organization;
				teacherElectronicReferences.email=email;
				teacherElectronicReferences.contactnumber=contactnumber;
				teacherElectronicReferences.longHaveYouKnow=longHaveYouKnow;
				teacherElectronicReferences.referenceDetails=referenceDetailText;
				teacherElectronicReferences.rdcontacted=rdcontacted_value;
				teacherElectronicReferences.pathOfReference=UploadedFileName;
				
				DSPQServiceAjax.saveOrUpdateElectronicReferences(teacherElectronicReferences,{ 
					async: true,
					errorHandler:handleError,
					callback: function(data)
					{
						hideElectronicReferencesForm();
						getElectronicReferencesGrid();
					}
				});
				dwr.engine.endBatch();
			}
			$('#loadingDiv').hide();
		}
	}
	return iReturnValidateTotalValue;
		
}
function showAndHideCanContOnOffer(from)
{}


function getTextAreaEditorCtrlTextValue(Id1)
{
	var inputCtrlValue="";
	try { inputCtrlValue=trim($('#'+Id1).find(".jqte_editor").text().trim()); } catch (e) {}
	if(inputCtrlValue==null || inputCtrlValue=='')
	{
		try { inputCtrlValue=$('#'+Id1).val(); } catch (e) {}
	}
	
	return inputCtrlValue;
}

function validateInputTextAreaControlStr(InputValue,Id1,Id2,Id3)
{
	var iReturnValidateValue=0;
	var InputValue_dspqRequired=0;
	try { InputValue_dspqRequired=$("#"+Id1).attr("dspqRequired"); } catch (e) {}
	if(InputValue_dspqRequired=="1")
	{
		if(InputValue=="")
		{
			$("#"+Id2).addClass("DSPQRequired12");
			$("#"+Id3).addClass("icon-warning DSPQRequired12");
			$("#lblFieldIdWarningBeforeSpace_"+Id1).html("&nbsp;");
			$("#lblFieldIdWarningAfterSpace_"+Id1).html("&nbsp;");
			iReturnValidateValue=1;
			
			if(!isSetFocus)
			{
				$("#"+Id1).focus();
				isSetFocus=true;
			}
			
		}
		else
		{
			$("#"+Id2).removeClass("DSPQRequired12");
			$("#"+Id3).removeClass("icon-warning DSPQRequired12");
			
			$("#lblFieldIdWarningBeforeSpace_"+Id1).html("");
			$("#lblFieldIdWarningAfterSpace_"+Id1).html("");
			
			iReturnValidateValue=0;
		}
			
	}
	
	return iReturnValidateValue;
	
}

function downloadReference(referenceId, linkId)
{		
	DSPQServiceAjax.downloadReference(referenceId,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	if(data=="")
			{
				data="javascript:void(0)";
			}
			else if(data.indexOf(".doc")!=-1)
			{
				document.getElementById("ifrmRef").src=data;
			}else
			{
				document.getElementById(linkId).href = data;
			}
			return false;
		}});
}


function editFormElectronicReferences(id)
{
	DSPQServiceAjax.editElectronicReferences(id,
		{ 
			async: false,
			errorHandler:handleError,
			callback: function(data)
			{
				showElectronicReferencesForm();
				dwr.util.setValues(data);
				showAndHideCanContOnOffer(0);
				try { $("#referenceDetailText").find(".jqte_editor").html(data.referenceDetails); } catch (e) {}
				
				if(data.pathOfReference!=null && data.pathOfReference!="")
				{	
					try { document.getElementById("removeref").style.display="block"; } catch (e) {}
					try { document.getElementById("pathOfReference").value=data.pathOfReference; } catch (e) {}
					try { document.getElementById("divRefName").style.display="block"; } catch (e) {}
					try { 
						document.getElementById("divRefName").innerHTML="<a href='javascript:void(0)' id='hrefEditRef' onclick=\"downloadReference('"+data.elerefAutoId+"','hrefEditRef');" +
						"if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0');" +
						"return false;\">"+data.pathOfReference+"</a>";
					} catch (e) {}
					
				}else
				{
					try { document.getElementById("divRefName").style.display="none"; } catch (e) {}
					try { document.getElementById("removeref").style.display="none"; } catch (e) {}
					try { document.getElementById("pathOfReference").value=""; } catch (e) {}
				}
				
				return false;
			}
		});
	return false;
}

var eleRefIDForTeacher=0;
var status_eref=0;
function changeStatusElectronicReferences(id,status)
{	
	eleRefIDForTeacher=id;
	status_eref=status;
	if(status==1)
		$('#chgstatusRef1').modal('show');
	else if(status==0)
		$('#chgstatusRef2').modal('show');	
}

function changeStatusElectronicReferencesConfirm()
{	
	$('#chgstatusRef1').modal('hide');
	$('#chgstatusRef2').modal('hide');
	
	DSPQServiceAjax.changeStatusElectronicReferences(eleRefIDForTeacher,status_eref, { 
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{
				getElectronicReferencesGrid();
				hideElectronicReferencesForm();
				return false;
			}});
		
		eleRefIDForTeacher=0;
}

function removeReferences()
{
	$('#deleteRefAttachFile').modal('show');
}

function removeReferencesL2()
{
	$('#deleteRefAttachFile').modal('hide');
	var referenceId = document.getElementById("elerefAutoId").value;
	DSPQServiceAjax.removeReferences(referenceId,
		{ 
			async: false,
			errorHandler:handleError,
			callback: function(data)
			{	
				document.getElementById("removeref").style.display="none";
				getElectronicReferencesGrid();
				hideElectronicReferencesForm();
			}
		});
	
}
function showElectronicReferencesForm()
{
	document.getElementById("divMainForm_ref").style.display="block";
	$('#frmElectronicReferences')[0].reset();
	document.getElementById("elerefAutoId").value="";
	
	$("#lblSectionId_tooltipSection09").removeClass("DSPQRequired12");
	
	resetLabels("salutation");
	resetLabels("firstName");
	resetLabels("lastName");
	resetLabels("designation");
	resetLabels("organization");
	resetLabels("contactnumber");
	resetLabels("email");
	resetLabels("pathOfReferenceFile");
	resetLabels("rdcontacted");
	resetLabels("longHaveYouKnow");
	resetLabels("referenceDetailText");
}

function hideElectronicReferencesForm()
{
	$('#frmElectronicReferences')[0].reset();
	document.getElementById("elerefAutoId").value="";
	document.getElementById("divMainForm_ref").style.display="none";
	
	$("#lblSectionId_tooltipSection09").removeClass("DSPQRequired12");
}

function SaveAndExitPortfolioL2()
{
	window.location.href='userdashboard.do';
}

function saveVideo(UploadedFileName)
{
	insertOrUpdatevideoLinks(UploadedFileName);
}

function insertOrUpdatevideoLinks(UploadedFileName)
{
	isSetFocus=false;
	var iReturnValidateTotalValue=0;
	var iReturnValidateValue=0;
	
	var Video_Section=0;
	var Video_Section_Name="tooltipSection10";
	
	if($("#divvideoLinks").is(':visible'))
	{
		
		var videolinkAutoId=getInputCtrlTextValue("videolinkAutoId");
		
		var InputCtrlId_videourl="videourl";
		var videourl=getInputCtrlTextValue(InputCtrlId_videourl);
		iReturnValidateValue=validateInputTextControlStr(videourl,InputCtrlId_videourl,"lblFieldId_"+InputCtrlId_videourl,"lblFieldIdWarning_"+InputCtrlId_videourl);
		if(iReturnValidateValue=="1")
		{
			Video_Section=Video_Section+1;
			iReturnValidateTotalValue=iReturnValidateTotalValue+Video_Section;
		}
		
		var videofile="";
		if(UploadedFileName==undefined)
		{
			var dbvideofile=getInputCtrlTextValue("dbvideofile");
			var InputCtrlId_videofile="videofile";
			videofile=getInputCtrlTextValue(InputCtrlId_videofile);
			iReturnValidateValue=validateInputVideoFileControlStr(videofile,InputCtrlId_videofile,"lblFieldId_"+InputCtrlId_videofile,"lblFieldIdWarning_"+InputCtrlId_videofile,dbvideofile);
			if(iReturnValidateValue=="1")
			{
				Video_Section=Video_Section+1;
				iReturnValidateTotalValue=iReturnValidateTotalValue+Video_Section;
			}
		}
		
		if(Video_Section>0)
		{
			
			//window.location.hash = '#lblSectionId_'+Video_Section_Name;
			$("#lblSectionId_"+Video_Section_Name).addClass("DSPQRequired12");
			Video_Section=0;
		}
		else
		{
			$("#lblSectionId_"+Video_Section_Name).removeClass("DSPQRequired12");
		}
		
		if(iReturnValidateTotalValue==0 && $("#divvideoLinks").is(':visible')==true)
		{
			$('#loadingDiv').show();
			if(videofile!="")
			{
				document.getElementById("frmvideoLinks").submit();
			}
			else
			{

				if(UploadedFileName==null || UploadedFileName=="" || UploadedFileName==undefined)
					UploadedFileName=dbvideofile;
				
				var teacherVideoLink = {videolinkAutoId:null,videourl:null, video:null, createdDate:null};
				dwr.engine.beginBatch();
				dwr.util.getValues(teacherVideoLink);
				teacherVideoLink.videolinkAutoId=videolinkAutoId;
				teacherVideoLink.videourl=videourl;		
				teacherVideoLink.video=	UploadedFileName;	
				DSPQServiceAjax.saveOrUpdateVideoLinks(teacherVideoLink,videolinkAutoId,{ 
					async: false,
					errorHandler:handleError,
					callback: function(data)
					{			
						$('#loadingDiv').hide();					
						hideVideoLinksForm();
						getVideoLinksGrid();
					}
				});
				dwr.engine.endBatch();
				return true;
			
			}
			$('#loadingDiv').hide();
		}
	}
	return iReturnValidateTotalValue;
}


var videolnkIDForTeacher=0;
function delVideoLink(id)
{	
	videolnkIDForTeacher=id;
	$('#delVideoLnk').modal('show');
}

function delVideoLnkConfirm()
{	
		$('#delVideoLnk').modal('hide');
		DSPQServiceAjax.deleteVideoLink(videolnkIDForTeacher, { 
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{
				getVideoLinksGrid();
				hideVideoLinksForm();
				return false;
			}});
		
		videolnkIDForTeacher=0;
}

function editFormVideoLink(id)
{
	DSPQServiceAjax.editVideoLink(id,
		{ 
			async: false,
			errorHandler:handleError,
			callback: function(data)
			{
				showVideoLinksForm();
				dwr.util.setValues(data);
				try { document.getElementById("dbvideofile").value=data.video; } catch (e) {}
				return false;
			}
		});
	return false;
}



function getVideoPlay(videoLinkId)
{
	$('#videovDiv').modal('show');
	$('#loadingDivWaitVideo').show();
	$('#interVideoDiv').empty();
	var videoPath='';		
	DSPQServiceAjax.getVideoPlayDiv(videoLinkId, { 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
		$('#loadingDivWaitVideo').hide();
		if(data!=""&& data!=null)
		{			
			var videoPath="";				
			videoPath="<video poster='images/video_back.png' src="+data+" width='480' height='280' controls autoplay><source src="+data+" type='video/ogg'>"+resourceJSON.msgBrowservideotag+"<br>"+resourceJSON.msgPlsDownload+" <a target='_blank' href='https://www.apple.com/in/quicktime/download/'>"+resourceJSON.msgQuickTimePlayer+"</a><br>"+resourceJSON.msgRestartSafaribrowser+" </video>";
			$('#interVideoDiv').append(videoPath);			
		}
		else
		{
			$('#interVideoDiv').css("color","Red");
			$('#interVideoDiv').append(resourceJSON.msgProblemoccured);
		}
		}});
}

function hideVideoLinksForm()
{
	$('#frmvideoLinks')[0].reset();
	try { 		document.getElementById("divvideoLinks").style.display="none"; 	} catch (e) {}
	try { 		window.location.hash = '#addVideoIdDSPQ'; 	} catch (e) {}
	return false;
}

function showVideoLinksForm()
{
	document.getElementById("divvideoLinks").style.display="block";
	$('#frmvideoLinks')[0].reset();
}

//////// AD

function chkForAdditionalDocuments(evt)
{	
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==13)
	{
		insertOrUpdate_AdditionalDocuments();
	}	
}


function showAdditionalDocumentsForm()
{
	document.getElementById("divAdditionalDocumentsRow").style.display="block";
	try { document.getElementById("documentName").focus(); } catch (e) {}
	$('#additionalDocumentsForm')[0].reset();
	return false;
}
function resetAdditionalDocumentsForm()
{
	document.getElementById("divAdditionalDocumentsRow").style.display="none";
	
	try {	document.getElementById("additionDocumentId").value=""; } catch (e) {}
	try {	document.getElementById("documentName").value=""; } catch (e) {}
	try {	document.getElementById("uploadedDocument").value=""; } catch (e) {}
	try {	document.getElementById("db_uploadedDocument").value=""; } catch (e) {}
	
	
	$('#additionalDocumentsForm')[0].reset();
	
	return false;
}

function saveAdditionalDocuments(fileName,sbtsource)
{
	insertOrUpdate_AdditionalDocuments(fileName);
}

function insertOrUpdate_AdditionalDocuments(UploadedFileName)
{
	
	isSetFocus=false;
	var iReturnValidateTotalValue=0;
	var iReturnValidateValue=0;
	
	var AddDoc_Section=0;
	var AddDoc_Section_Name="tooltipSection11";
	
	if($("#divAdditionalDocumentsRow").is(':visible'))
	{
		var InputCtrlId_additionDocumentId="additionDocumentId";
		var additionDocumentId=getInputCtrlTextValue(InputCtrlId_additionDocumentId);
		if(additionDocumentId==null || additionDocumentId=="" || additionDocumentId=="0")
			additionDocumentId=null;
		
		var InputCtrlId_documentName="documentName";
		var documentName=getInputCtrlTextValue(InputCtrlId_documentName);
		iReturnValidateValue=validateInputTextControlStr(documentName,InputCtrlId_documentName,"lblFieldId_"+InputCtrlId_documentName,"lblFieldIdWarning_"+InputCtrlId_documentName);
		if(iReturnValidateValue=="1")
		{
			AddDoc_Section=AddDoc_Section+1;
			iReturnValidateTotalValue=iReturnValidateTotalValue+AddDoc_Section;
		}
		
		var uploadedDocument="";
		if(UploadedFileName==undefined)
		{
			var db_uploadedDocument=getInputCtrlTextValue("db_uploadedDocument");
			var InputCtrlId_uploadedDocument="uploadedDocument";
			uploadedDocument=getInputCtrlTextValue(InputCtrlId_uploadedDocument);
			iReturnValidateValue=validateInputFileControlStr(uploadedDocument,InputCtrlId_uploadedDocument,"lblFieldId_"+InputCtrlId_uploadedDocument,"lblFieldIdWarning_"+InputCtrlId_uploadedDocument,db_uploadedDocument);
			if(iReturnValidateValue=="1")
			{
				AddDoc_Section=AddDoc_Section+1;
				iReturnValidateTotalValue=iReturnValidateTotalValue+AddDoc_Section;
			}
		}
		//alert("11 iReturnValidateTotalValue "+iReturnValidateTotalValue);
		if(AddDoc_Section>0)
		{
			
			//window.location.hash = '#lblSectionId_'+AddDoc_Section_Name;
			$("#lblSectionId_"+AddDoc_Section_Name).addClass("DSPQRequired12");
			AddDoc_Section=0;
		}
		else
		{
			$("#lblSectionId_"+AddDoc_Section_Name).removeClass("DSPQRequired12");
		}
		
		if(iReturnValidateTotalValue==0 && $("#divAdditionalDocumentsRow").is(':visible')==true)
		{
			$('#loadingDiv').show();
			if(uploadedDocument!="")
			{

				document.getElementById("additionalDocumentsForm").submit();
			}
			else
			{

				if(UploadedFileName==null || UploadedFileName=="" || UploadedFileName==undefined)
					UploadedFileName=getInputCtrlTextValue("db_uploadedDocument");
				//alert(additionDocumentId+" , "+documentName+" , "+UploadedFileName);
				DSPQServiceAjax.saveOrUpdateAdditionalDocuments(additionDocumentId,documentName,UploadedFileName,
				{ 
					async: false,
					errorHandler:handleError,
					callback: function(data)
					{	
						showGridAdditionalDocuments()		
						resetAdditionalDocumentsForm();
					}
				});
				
			
			}
			$('#loadingDiv').hide();
		}
	}
	//alert("12 iReturnValidateTotalValue "+iReturnValidateTotalValue);
	return iReturnValidateTotalValue;
}

function delRow_Document(id)
{	
	document.getElementById("additionDocumentId").value=id;
	$('#removeUploadedDocument').modal('show');
}

function removeUploadedDocument()
{
	//alert("1");
	var additionDocumentId=document.getElementById("additionDocumentId").value;
	//alert("1 additionDocumentId "+additionDocumentId);
	DSPQServiceAjax.removeUploadedDocument(additionDocumentId,{
	async: true,
	errorHandler:handleError,
	callback:function(data)
	{
		$('#removeUploadedDocument').modal('hide');
		showGridAdditionalDocuments();
		resetAdditionalDocumentsForm();
		return false;
	}});
}

function showEditTeacherAdditionalDocuments(additionDocumentId)
{
	showAdditionalDocumentsForm();
	DSPQServiceAjax.showEditTeacherAdditionalDocuments(additionDocumentId,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			document.getElementById("additionDocumentId").value=data.additionDocumentId;
			try{ document.getElementById("documentName").value=data.documentName; }catch(err){}
			try{ document.getElementById("db_uploadedDocument").value=data.uploadedDocument; }catch(err){}
		
			if(data.uploadedDocument!=null && data.uploadedDocument!="")
			{
				try{ document.getElementById("removeUploadedDocumentSpan").style.display="inline"; }catch(err){}
				try{ document.getElementById("divUploadedDocument").style.display="inline"; }catch(err){}
				try{ 
					document.getElementById("divUploadedDocument").innerHTML="<a href='javascript:void(0)' id='hrefAdditionalDocuments' onclick=\"downloadUploadedDocument('"+data.additionDocumentId+"','hrefAdditionalDocuments');" +
					"if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0');" +
					"return false;\">"+data.uploadedDocument+"</a>";
				}catch(err){}
			}
			else
			{
				try{ document.getElementById("divUploadedDocument").style.display="none"; }catch(err){}
				try{ document.getElementById("removeUploadedDocumentSpan").style.display="none"; }catch(err){}
				try{ document.getElementById("uploadedDocument").value="";; }catch(err){}
			}
		
		
	}})
	return false;
}

function downloadUploadedDocument(additionDocumentId, linkId)
{		
	DSPQServiceAjax.downloadUploadedDocument(additionDocumentId,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
			if(data=="")
			{
				data="javascript:void(0)";
			}
			else if(data.indexOf(".doc")!=-1)
			{
				document.getElementById("ifrmRef").src=data;
			}else
			{
				document.getElementById(linkId).href = data;
			}
			return false;
		}});
}


function showNumberRequiredMsg(sectionId)
{
	//alert("sectionId "+sectionId);
	var NumberRequire=0;
	if(sectionId!='')
	{
		if(sectionId='9')
		{
			var SectionName09="tooltipSection09";
			try { NumberRequire=$("#lblSectionId_"+SectionName09).attr("numberrequired"); } catch (e) {}
			$("#lblSectionId_"+SectionName09).addClass("DSPQRequired12");
			
			//$('#errordivElectronicReferences').append("&#149; A Electronic References has already registered with the email.<br>");
			//$('#errordivElectronicReferences').append("Please enter atleast "+NumberRequire+" reference(s).<br>");
			$('#credentialGroupErrorMsg').append("&#149;Please enter atleast "+NumberRequire+" reference(s).<br>");
			
			//$('#errordivElectronicReferences').show();
		}
		
	}
}

function showCredentialGroupError()
{
	$('#credentialGroupError').modal('show');
}

function CloseCredentialGroupError()
{
	$('#credentialGroupError').modal('hide');
}

function showAcademicGroupError()
{
	$('#academicGroupError').modal('show');
}

function CloseAcademicGroupError()
{
	$('#academicGroupError').modal('hide');
}

function showProfessionalGroupError()
{
	$('#professionalGroupError').modal('show');
}

function CloseProfessionalGroupError()
{
	$('#professionalGroupError').modal('hide');
}

function jafDownloadResume()
{
	//alert("1");
	DSPQServiceAjax.downloadResume(
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
			//alert("2 data "+data);
			if (data.indexOf(".doc") !=-1) {
			    document.getElementById('ifrmResume').src = ""+data+"";
			}
			else
			{
				document.getElementById("hrefResume").href = data;
				return false;
			}
			
			if(deviceType)
			{
				if (data.indexOf(".doc")!=-1)
				{
					$("#docfileNotOpen").css({"z-index":"3000"});
			    	$('#docfileNotOpen').show();
				}
				else if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					$("#exelfileNotOpen").css({"z-index":"3000"});
			    	$('#exelfileNotOpen').show();
				}
				else
				{
					document.getElementById("hrefResume").href = data;
				}
			}
			else if(deviceTypeAndroid)
			{
				if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					document.getElementById('ifrmResume').src = ""+data+"";
				}
				else
				{
					document.getElementById("hrefResume").href = data;	
				}	
			}
			else if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
			{
				document.getElementById('ifrmResume').src = ""+data+"";
			}
			else
			{
				document.getElementById("hrefResume").href = data;	
			}			 
			return false;
			
		}
	});
}

function showEmploymentForm()
{
	document.getElementById("divEmployment").style.display="block";
	$('#frmEmployment')[0].reset();
	$("#lblSectionId_tooltipSection13").removeClass("DSPQRequired12");
}

function hideEmploymentForm()
{
	document.getElementById("divEmployment").style.display="none";
	$('#frmEmployment')[0].reset();
	return false;
}


function insertOrUpdateEmployment()
{
	
	isSetFocus=false;
	var iReturnValidateTotalValue=0;
	var iReturnValidateValue=0;
	
	var Emp_Section=0;
	var Emp_Section_Name="tooltipSection13";
	
	if($("#divEmployment").is(':visible'))
	{
		var InputCtrlId_roleId="roleId";
		var roleId=getInputCtrlTextValue(InputCtrlId_roleId);
		
		var InputCtrlId_idonothaveworkexp="idonothaveworkexp";
		var idonothaveworkexp=getInputCtrlCheckBoxValue(InputCtrlId_idonothaveworkexp);
		if(idonothaveworkexp!="1")
		{
			
			var InputCtrlId_fieldId2="empPosition";
			var empPosition=getInputCtrlTextValue(InputCtrlId_fieldId2);
			iReturnValidateValue=validateInputListControlStr(empPosition,InputCtrlId_fieldId2,"lblFieldId_"+InputCtrlId_fieldId2,"lblFieldIdWarning_"+InputCtrlId_fieldId2);
			if(iReturnValidateValue=="1")
			{
				Emp_Section=Emp_Section+1;
				iReturnValidateTotalValue=iReturnValidateTotalValue+Emp_Section;
			}
			
			var InputCtrlId_role="role";
			var role=getInputCtrlTextValue(InputCtrlId_role);
			iReturnValidateValue=validateInputTextControlStr(role,InputCtrlId_role,"lblFieldId_"+InputCtrlId_role,"lblFieldIdWarning_"+InputCtrlId_role);
			if(iReturnValidateValue=="1")
			{
				Emp_Section=Emp_Section+1;
				iReturnValidateTotalValue=iReturnValidateTotalValue+Emp_Section;
			}
			
			var InputCtrlId_fieldId2="fieldId2";
			var fieldId2=getInputCtrlTextValue(InputCtrlId_fieldId2);
			iReturnValidateValue=validateInputListControlStr(fieldId2,InputCtrlId_fieldId2,"lblFieldId_"+InputCtrlId_fieldId2,"lblFieldIdWarning_"+InputCtrlId_fieldId2);
			if(iReturnValidateValue=="1")
			{
				Emp_Section=Emp_Section+1;
				iReturnValidateTotalValue=iReturnValidateTotalValue+Emp_Section;
			}
			
			var InputCtrlId_empOrg="empOrg";
			var empOrg=getInputCtrlTextValue(InputCtrlId_empOrg);
			iReturnValidateValue=validateInputTextControlStr(empOrg,InputCtrlId_empOrg,"lblFieldId_"+InputCtrlId_empOrg,"lblFieldIdWarning_"+InputCtrlId_empOrg);
			if(iReturnValidateValue=="1")
			{
				Emp_Section=Emp_Section+1;
				iReturnValidateTotalValue=iReturnValidateTotalValue+Emp_Section;
			}
			
			
			var InputCtrlId_cityEmp="cityEmp";
			var cityEmp=getInputCtrlTextValue(InputCtrlId_cityEmp);
			iReturnValidateValue=validateInputTextControlStr(cityEmp,InputCtrlId_cityEmp,"lblFieldId_"+InputCtrlId_cityEmp,"lblFieldIdWarning_"+InputCtrlId_cityEmp);
			if(iReturnValidateValue=="1")
			{
				Emp_Section=Emp_Section+1;
				iReturnValidateTotalValue=iReturnValidateTotalValue+Emp_Section;
			}
			
			var InputCtrlId_stateOfOrg="stateOfOrg";
			var stateOfOrg=getInputCtrlTextValue(InputCtrlId_stateOfOrg);
			iReturnValidateValue=validateInputTextControlStr(stateOfOrg,InputCtrlId_stateOfOrg,"lblFieldId_"+InputCtrlId_stateOfOrg,"lblFieldIdWarning_"+InputCtrlId_stateOfOrg);
			if(iReturnValidateValue=="1")
			{
				Emp_Section=Emp_Section+1;
				iReturnValidateTotalValue=iReturnValidateTotalValue+Emp_Section;
			}
			
			
			var InputCtrlId_currentlyWorking="currentlyWorking";
			var currentlyWorking=getInputCtrlCheckBoxValue(InputCtrlId_currentlyWorking);
			iReturnValidateValue=validateInputCheckBoxControlStr(currentlyWorking,InputCtrlId_currentlyWorking,"lblFieldId_"+InputCtrlId_currentlyWorking,"lblFieldIdWarning_"+InputCtrlId_currentlyWorking,"hdn_"+InputCtrlId_currentlyWorking);
			if(iReturnValidateValue=="1")
			{
				Emp_Section=Emp_Section+1;
				iReturnValidateTotalValue=iReturnValidateTotalValue+Emp_Section;
			}
			
			var InputCtrlId_roleStartMonth="roleStartMonth";
			var roleStartMonth=getInputCtrlTextValue(InputCtrlId_roleStartMonth);
			var InputCtrlId_roleStartYear="roleStartYear";
			var roleStartYear=getInputCtrlTextValue(InputCtrlId_roleStartYear);
			
			iReturnValidateValue=validateInputListControlTwo(roleStartMonth,roleStartYear,InputCtrlId_roleStartMonth,"lblFieldId_"+InputCtrlId_roleStartMonth,"lblFieldIdWarning_"+InputCtrlId_roleStartMonth);
			if(iReturnValidateValue=="1")
			{
				Emp_Section=Emp_Section+1;
				iReturnValidateTotalValue=iReturnValidateTotalValue+Emp_Section;
			}
			
			var InputCtrlId_roleEndMonth="roleEndMonth";
			var roleEndMonth=getInputCtrlTextValue(InputCtrlId_roleEndMonth);
			var InputCtrlId_roleEndYear="roleEndYear";
			var roleEndYear=getInputCtrlTextValue(InputCtrlId_roleEndYear);
			if(currentlyWorking!="on")
			{
				iReturnValidateValue=validateInputListControlTwo(roleEndMonth,roleEndYear,InputCtrlId_roleEndMonth,"lblFieldId_"+InputCtrlId_roleEndMonth,"lblFieldIdWarning_"+InputCtrlId_roleEndMonth);
				if(iReturnValidateValue=="1")
				{
					Emp_Section=Emp_Section+1;
					iReturnValidateTotalValue=iReturnValidateTotalValue+Emp_Section;
				}
			}
			/////////
			
			var InputCtrlId_amount="amount";
			var amount=getInputCtrlTextValue(InputCtrlId_amount);
			iReturnValidateValue=validateInputTextControlStr(amount,InputCtrlId_amount,"lblFieldId_"+InputCtrlId_amount,"lblFieldIdWarning_"+InputCtrlId_amount);
			if(iReturnValidateValue=="1")
			{
				Emp_Section=Emp_Section+1;
				iReturnValidateTotalValue=iReturnValidateTotalValue+Emp_Section;
			}
			
			var InputCtrlId_empRoleTypeId="empRoleTypeId";
			var empRoleTypeId=getInputCtrlRadioValue(InputCtrlId_empRoleTypeId);
			iReturnValidateValue=validateInputRadioControlStr(empRoleTypeId,InputCtrlId_empRoleTypeId,"lblFieldId_"+InputCtrlId_empRoleTypeId,"lblFieldIdWarning_"+InputCtrlId_empRoleTypeId,"hdn_"+InputCtrlId_empRoleTypeId);
			if(iReturnValidateValue=="1")
			{
				Emp_Section=Emp_Section+1;
				iReturnValidateTotalValue=iReturnValidateTotalValue+Emp_Section;
			}
			
			
			var InputCtrlId_primaryResp="primaryResp";
			var primaryResp=getTextAreaEditorCtrlTextValue(InputCtrlId_primaryResp);
			iReturnValidateValue=validateInputTextAreaControlStr(primaryResp,InputCtrlId_primaryResp,"lblFieldId_"+InputCtrlId_primaryResp,"lblFieldIdWarning_"+InputCtrlId_primaryResp);
			if(iReturnValidateValue=="1")
			{
				Emp_Section=Emp_Section+1;
				iReturnValidateTotalValue=iReturnValidateTotalValue+Emp_Section;
			}
			
			var InputCtrlId_mostSignCont="mostSignCont";
			var mostSignCont=getTextAreaEditorCtrlTextValue(InputCtrlId_mostSignCont);
			iReturnValidateValue=validateInputTextAreaControlStr(mostSignCont,InputCtrlId_mostSignCont,"lblFieldId_"+InputCtrlId_mostSignCont,"lblFieldIdWarning_"+InputCtrlId_mostSignCont);
			if(iReturnValidateValue=="1")
			{
				Emp_Section=Emp_Section+1;
				iReturnValidateTotalValue=iReturnValidateTotalValue+Emp_Section;
			}
			
			var InputCtrlId_reasonForLea="reasonForLea";
			var reasonForLea=getTextAreaEditorCtrlTextValue(InputCtrlId_reasonForLea);
			iReturnValidateValue=validateInputTextAreaControlStr(reasonForLea,InputCtrlId_reasonForLea,"lblFieldId_"+InputCtrlId_reasonForLea,"lblFieldIdWarning_"+InputCtrlId_reasonForLea);
			if(iReturnValidateValue=="1")
			{
				Emp_Section=Emp_Section+1;
				iReturnValidateTotalValue=iReturnValidateTotalValue+Emp_Section;
			}
			
		}
		
		if(iReturnValidateTotalValue==0)		
		{
			if(idonothaveworkexp!="1")
			{
				var roleTypeObj=null;
				if(empRoleTypeId!="-1")
				{
					roleTypeObj = {empRoleTypeId:empRoleTypeId};
				}
				else
				{
					roleTypeObj=null;
				}
				
				var fieldObj =null;
				if(fieldId2!="" && fieldId2!="0")
					fieldObj={fieldId:fieldId2};
				else
					fieldObj=null;
				
				var empRoleTypeMasterObj =null;
				if(fieldId2!="" && fieldId2!="0")
					empRoleTypeMasterObj={empRoleTypeId:empRoleTypeId};
				else
					empRoleTypeMasterObj=null;
				
				var employment = {position:null,roleId:null,fieldMaster:fieldObj,empRoleTypeMaster:roleTypeObj,role:null, roleStartMonth:null, roleStartYear:null, roleEndMonth:null, roleEndYear:null,amount:null,primaryResp:null,mostSignCont:null,reasonForLeaving:null};
				dwr.engine.beginBatch();
				
				dwr.util.getValues(employment);
				
				employment.position=empPosition;
				employment.roleId=roleId;
				employment.fieldMaster=fieldObj;
				employment.empRoleTypeMaster=empRoleTypeMasterObj;
				employment.role=role;
				employment.roleStartMonth=roleStartMonth;
				employment.roleStartYear=roleStartYear;
				employment.roleEndMonth=roleEndMonth;
				employment.roleEndYear=roleEndYear;
				employment.amount=amount;
				employment.primaryResp=primaryResp;
				employment.mostSignCont=mostSignCont;
				employment.reasonForLeaving=reasonForLea;
				//alert("currentlyWorking "+currentlyWorking);
				if(currentlyWorking=="on")
				{
					employment.currentlyWorking=true;
				}
				else
				{
					employment.currentlyWorking=false;
				}
				
				employment.organization=empOrg;
				employment.city=cityEmp;
				employment.state=stateOfOrg;
				employment.noWorkExp=false;
			}
			else
			{
				var employment = {position:null,roleId:null,fieldMaster:null,empRoleTypeMaster:null,role:null, roleStartMonth:null, roleStartYear:null, roleEndMonth:null, roleEndYear:null,amount:null,primaryResp:null,mostSignCont:null,reasonForLeaving:null};
				dwr.engine.beginBatch();
				employment.noWorkExp=true;
				employment.roleId=roleId;
			}
			
			DSPQServiceAjax.saveOrUpdateEmployment(employment,{ 
				async: false,
				errorHandler:handleError,
				callback: function(data)
				{
					hideEmploymentForm();
					getPFEmploymentDataGrid();
				}
			});
			dwr.engine.endBatch();
			return true;
		}
		else
		{
			
		}
		
	}
	return iReturnValidateTotalValue;
	
}

function deleteEmployment(id)
{	
	document.getElementById("empHistoryID").value=id;
	$('#deleteEmpHistory').modal('show');
}

function deleteEmploymentDSPQ()
{	
	$('#deleteEmpHistory').modal('hide');
	var id=document.getElementById("empHistoryID").value;
	DSPQServiceAjax.deleteEmployment(id, { 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			getPFEmploymentDataGrid();
			hideEmploymentForm();
			return false;
		}});
}


function showEditFormEmployment(id)
{
	DSPQServiceAjax.showEditFormEmployment(id,
		{ 
			async: false,
			errorHandler:handleError,
			callback: function(data)
			{
					showEmploymentForm();
					try { document.getElementById("roleId").value=id; } catch (e) {}
					if(data.noWorkExp==null || data.noWorkExp==0)
					{
						try { document.getElementById("empPosition").value=data.position; } catch (e) {}
						try { document.getElementById("role").value=data.role; } catch (e) {}
						try { document.getElementById("fieldId2").value=data.fieldMaster.fieldId; } catch (e) {}
						try { document.getElementById("amount").value=data.amount; } catch (e) {}
						try { document.getElementById("empOrg").value=data.organization; } catch (e) {}
						try { document.getElementById("cityEmp").value=data.city; } catch (e) {}
						try { document.getElementById("stateOfOrg").value=data.state; } catch (e) {}
						
						//alert("data.currentlyWorking "+data.currentlyWorking);
						if(data.currentlyWorking!=null && data.currentlyWorking)
						{
							//alert("checked");
							
							//try { document.getElementById("currentlyWorking").click(); } catch (e) {}
							try { document.getElementById("currentlyWorking").checked=true; } catch (e) {}
							try { $("#currentlyWorking").prop( "checked", true ); } catch (e) {}
							hideToExp();
							
							
							try { document.getElementById("roleStartMonth").value=data.roleStartMonth; } catch (e) {}
							try { document.getElementById("roleStartYear").value=data.roleStartYear; } catch (e) {}
						}
						else
						{
							//alert("no checked");
							try { document.getElementById("currentlyWorking").checked=false; } catch (e) {}
							try { document.getElementById("divToMonth").style.display="block"; } catch (e) {}
							try { document.getElementById("divToYear").style.display="block"; } catch (e) {}
							try { document.getElementById("roleStartMonth").value=data.roleStartMonth; } catch (e) {}
							
							try { document.getElementById("roleStartYear").value=data.roleStartYear; } catch (e) {}
							try { document.getElementById("roleEndMonth").value=data.roleEndMonth; } catch (e) {}
							try { document.getElementById("roleEndYear").value=data.roleEndYear; } catch (e) {}
							
						}
						
						try { document.getElementById("empRoleTypeId_"+data.empRoleTypeMaster.empRoleTypeId).checked = true;} catch (e) {}
						
						
						try { $('#primaryResp').find(".jqte_editor").html(data.primaryResp); } catch (e) {}
						try { $('#mostSignCont').find(".jqte_editor").html(data.mostSignCont); } catch (e) {}
						try { $('#reasonForLea').find(".jqte_editor").html(data.reasonForLeaving); } catch (e) {}
						return false;
					}
					else
					{
						alert(" else ");
						try { document.getElementById("idonothaveworkexp").checked=true; } catch (e) {}
						try { $('#primaryResp').find(".jqte_editor").html(""); } catch (e) {}
						try { $('#mostSignCont').find(".jqte_editor").html(""); } catch (e) {}
						try { $('#reasonForLea').find(".jqte_editor").html(""); } catch (e) {}
						jafhideworkexp();
						
					}
					
					}
			});

	return false;

}


function hideToExp()
{
	var chk=getInputCtrlCheckBoxValue("currentlyWorking");
	if(chk)
	{
		try { document.getElementById("divToMonth").style.display="none"; } catch (e) {}
		try { document.getElementById("divToYear").style.display="none"; } catch (e) {}
	}
	else
	{
		try { document.getElementById("divToMonth").style.display="block"; } catch (e) {}
		try { document.getElementById("divToYear").style.display="block"; } catch (e) {}
	}
}

function jafhideworkexp()
{
	var idonothaveworkexp=getInputCtrlCheckBoxValue("idonothaveworkexp");
	if(idonothaveworkexp=="1")
	{
		$(".jafHideWorkExp").prop('disabled', true);
		$(".jafHideWorkExp").val("");
		try { $('#primaryResp').find(".jqte_editor").html(""); } catch (e) {}
		try { $('#mostSignCont').find(".jqte_editor").html(""); } catch (e) {}
		try { $('#reasonForLea').find(".jqte_editor").html(""); } catch (e) {}
	}
	else
	{
		$(".jafHideWorkExp").prop('disabled', false);
		$(".jafHideWorkExp").prop('disabled', false);
	}
}

function showInvolvementForm()
{
	document.getElementById("divInvolvement").style.display="block";
	$('#frmInvolvement')[0].reset();
	showAndHideLeadNoOfPeople(0);
}

function hideInvolvement()
{
	document.getElementById("divInvolvement").style.display="none";
}

function saveOrUpdateInvolvement()
{
	isSetFocus=false;
	var iReturnValidateTotalValue=0;
	var iReturnValidateValue=0;
	
	var Inv_Section=0;
	var Inv_Section_Name="tooltipSection14";
	if($("#divInvolvement").is(':visible'))
	{
		var InputCtrlId_involvementId="involvementId";
		var involvementId=getInputCtrlTextValue(InputCtrlId_involvementId);
		
		var InputCtrlId_organizationInv="organizationInv";
		var organizationInv=getInputCtrlTextValue(InputCtrlId_organizationInv);
		iReturnValidateValue=validateInputTextControlStr(organizationInv,InputCtrlId_organizationInv,"lblFieldId_"+InputCtrlId_organizationInv,"lblFieldIdWarning_"+InputCtrlId_organizationInv);
		if(iReturnValidateValue=="1")
		{
			Inv_Section=Inv_Section+1;
			iReturnValidateTotalValue=iReturnValidateTotalValue+Inv_Section;
		}

		var InputCtrlId_orgTypeId="orgTypeId";
		var orgTypeId=getInputCtrlTextValue(InputCtrlId_orgTypeId);
		iReturnValidateValue=validateInputListControlStr(orgTypeId,InputCtrlId_orgTypeId,"lblFieldId_"+InputCtrlId_orgTypeId,"lblFieldIdWarning_"+InputCtrlId_orgTypeId);
		if(iReturnValidateValue=="1")
		{
			Inv_Section=Inv_Section+1;
			iReturnValidateTotalValue=iReturnValidateTotalValue+Inv_Section;
		}

		var InputCtrlId_rangeId="rangeId";
		var rangeId=getInputCtrlTextValue(InputCtrlId_rangeId);
		iReturnValidateValue=validateInputListControlStr(rangeId,InputCtrlId_rangeId,"lblFieldId_"+InputCtrlId_rangeId,"lblFieldIdWarning_"+InputCtrlId_rangeId);
		if(iReturnValidateValue=="1")
		{
			Inv_Section=Inv_Section+1;
			iReturnValidateTotalValue=iReturnValidateTotalValue+Inv_Section;
		}

		var InputCtrlId_optionsRadios="optionsRadios";
		var optionsRadios=getInputCtrlRadioValue(InputCtrlId_optionsRadios);
		iReturnValidateValue=validateInputRadioControlStr(optionsRadios,InputCtrlId_optionsRadios,"lblFieldId_"+InputCtrlId_optionsRadios,"lblFieldIdWarning_"+InputCtrlId_optionsRadios,"hdn_"+InputCtrlId_optionsRadios);
		if(iReturnValidateValue=="1")
		{
			Inv_Section=Inv_Section+1;
			iReturnValidateTotalValue=iReturnValidateTotalValue+Inv_Section;
		}
		
		//alert("optionsRadios "+optionsRadios);
		
		var InputCtrlId_leadNoOfPeople="leadNoOfPeople";
		var leadNoOfPeople=getInputCtrlTextValue(InputCtrlId_leadNoOfPeople);
		if(optionsRadios==1)
		{
			iReturnValidateValue=validateInputTextControlStr(leadNoOfPeople,InputCtrlId_leadNoOfPeople,"lblFieldId_"+InputCtrlId_leadNoOfPeople,"lblFieldIdWarning_"+InputCtrlId_leadNoOfPeople);
			if(iReturnValidateValue=="1")
			{
				Inv_Section=Inv_Section+1;
				iReturnValidateTotalValue=iReturnValidateTotalValue+Inv_Section;
			}
		}
		
		//alert("28 iReturnValidateTotalValue "+iReturnValidateTotalValue);
		if(iReturnValidateTotalValue==0)
		{
			
			var orgObj = null;
			if(orgTypeId!="0" && orgTypeId!="")
			{
				orgObj = {orgTypeId:orgTypeId};
			}
			
			var peopleRangeObj = null;
			if(rangeId!="0" && rangeId!="")
			{
				peopleRangeObj = {rangeId:rangeId};
			}
			
			var involvement = {involvementId:null, orgTypeMaster:orgObj, peopleRangeMaster:peopleRangeObj, leadNoOfPeople:null};
			
			dwr.engine.beginBatch();
			
			dwr.util.getValues(involvement);
			
			involvement.organization= organizationInv;
			
			DSPQServiceAjax.saveOrUpdateInvolvement(involvement,
			{ 
				async: false,
				errorHandler:handleError,
				callback: function(data)
				{
					hideInvolvement();
					getInvolvementGrid();
				}
			});
			dwr.engine.endBatch();
		}
		
	}
	
	return iReturnValidateTotalValue;
}
function showEditFormInvolvement(id)
{
	DSPQServiceAjax.showEditFormInvolvement(id,
	{ 
		async: false,
		errorHandler:handleError,
		callback: function(data)
		{		
			showInvolvementForm();
			if(data.leadNoOfPeople!=null && data.leadNoOfPeople!="1")
			{
				showAndHideLeadNoOfPeople('1');
				//document.getElementById("rdo1").click();
			}
			else
			{
				showAndHideLeadNoOfPeople('0');
				document.getElementById("divLeadNoOfPeople").style.display="none";
				document.getElementById("leadNoOfPeople").value="";		
			}
			
			try { document.getElementById("involvementId").value=id; } catch (e) {}
			try { document.getElementById("organizationInv").value=data.organization; } catch (e) {}
			try { document.getElementById("orgTypeId").value=data.orgTypeMaster.orgTypeId; } catch (e) {}
			try { document.getElementById("rangeId").value=data.peopleRangeMaster.rangeId; } catch (e) {}
			try { document.getElementById("leadNoOfPeople").value=data.leadNoOfPeople; } catch (e) {}

		}
	});

	return false;
}
function showAndHideLeadNoOfPeople(opt)
{
	if(opt==1)
	{
		try { document.getElementById("divLeadNoOfPeople").style.display="block"; } catch (e) {}
	}
	else
	{
		try { document.getElementById("divLeadNoOfPeople").style.display="none"; } catch (e) {}
		try { document.getElementById("leadNoOfPeople").value=""; } catch (e) {}
	}
}


function showHonorForm()
{
	document.getElementById("divHonor").style.display="block";
	$('#frmInvolvement')[0].reset();
}

function hideHonor()
{
	document.getElementById("divHonor").style.display="none";
}

function deleteRecordInvolvment(id)
{
	document.getElementById("InvolvmentDelID").value=id;
	$('#deleteInvolvment').modal('show');
}

function deleteRecordInvolvmentL2(id)
{
	$('#deleteInvolvment').modal('hide');
	var id=document.getElementById("InvolvmentDelID").value;
	
		DSPQServiceAjax.deleteRecordInvolvment(id, { 
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{
			hideInvolvement();
			getInvolvementGrid();

			return false;
			}});
	
	return false;
}

function saveOrUpdateHonors()
{
	isSetFocus=false;
	var iReturnValidateTotalValue=0;
	var iReturnValidateValue=0;
	
	var Honor_Section=0;
	var Honor_Section_Name="tooltipSection15";
	if($("#divHonor").is(':visible'))
	{
		var InputCtrlId_honorId="honorId";
		var honorId=getInputCtrlTextValue(InputCtrlId_honorId);
		
		var InputCtrlId_honor="honor";
		var honor=getInputCtrlTextValue(InputCtrlId_honor);
		iReturnValidateValue=validateInputTextControlStr(honor,InputCtrlId_honor,"lblFieldId_"+InputCtrlId_honor,"lblFieldIdWarning_"+InputCtrlId_honor);
		if(iReturnValidateValue=="1")
		{
			Honor_Section=Honor_Section+1;
			iReturnValidateTotalValue=iReturnValidateTotalValue+Honor_Section;
		}

		var InputCtrlId_honorYear="honorYear";
		var honorYear=getInputCtrlTextValue(InputCtrlId_honorYear);
		iReturnValidateValue=validateInputListControlStr(honorYear,InputCtrlId_honorYear,"lblFieldId_"+InputCtrlId_honorYear,"lblFieldIdWarning_"+InputCtrlId_honorYear);
		if(iReturnValidateValue=="1")
		{
			Honor_Section=Honor_Section+1;
			iReturnValidateTotalValue=iReturnValidateTotalValue+Honor_Section;
		}
		
		if(iReturnValidateTotalValue==0)
		{
			var honer = {honorId:null, honor:null, honorYear:null};

			dwr.engine.beginBatch();	
			dwr.util.getValues(honer);	
			DSPQServiceAjax.saveOrUpdateHonors(honer,
			{ 
				async: false,
				errorHandler:handleError,
				callback: function(data)
				{
					hideHonor();
					getHonorsGrid();
					return false;
				}
			});
			dwr.engine.endBatch();
		}
	}
	
	return iReturnValidateTotalValue;
}

function showEditHonors(id)
{
	DSPQServiceAjax.showEditHonors(id,
	{ 
		async: false,
		errorHandler:handleError,
		callback: function(data)
		{		
			showHonorForm();
			try {document.getElementById("honorId").value=id;} catch (e) {}
			try {document.getElementById("honor").value=data.honor;} catch (e) {}
			try {document.getElementById("honorYear").value=data.honorYear;} catch (e) {}
		}
	});
return false;
}


function deleteRecordHonors(id)
{
	document.getElementById("HonorDelID").value=id;
	$('#deleteHonor').modal('show');
}


function deleteRecordHonorsL2()
{
	$('#deleteHonor').modal('hide');
	var id=document.getElementById("HonorDelID").value;
	
	DSPQServiceAjax.deleteRecordHonors(id,{ 
	async: true,
	errorHandler:handleError,
	callback:function(data)
	{
		hideHonor();
		getHonorsGrid();
		return false;
	}});
	
	return false;
}

function chkRetired()
{
	var isretired=getInputCtrlCheckBoxValue("isretired");
	if(isretired==1)
	{
		try {	$('#isRetiredDiv').show();		} catch (e) {	}
	}
	else
	{
		try {	$('#isRetiredDiv').hide();		} catch (e) {	}
		
	}
}

function chkRetiredLoad()
{
	var InputCtrlId_retireddistrictId="retireddistrictId";
	var retireddistrictId=getInputCtrlTextValue(InputCtrlId_retireddistrictId);
	
	var InputCtrlId_stMForretire="stMForretire";
	var stMForretire=getInputCtrlTextValue(InputCtrlId_stMForretire);
	
	var InputCtrlId_retireNo="retireNo";
	var retireNo=getInputCtrlTextValue(InputCtrlId_retireNo);
	
	if(retireddistrictId > 0 || stMForretire > 0 || (retireNo!=null && retireNo!=''))
	{
		
		try { document.getElementById("isretired_1").checked=true; } catch (e) {}
	}
	else
	{
		try { document.getElementById("isretired_0").checked=true; } catch (e) {}
		
	}
	
	chkRetired();
}

function getGroup01_SectionName01(jobId,dspqPortfolioNameId,candidateType,groupId,sectionId)
{
	//$('#loadingDiv').show();
	DSPQServiceAjax.getGroup01_SectionName01(jobId,dspqPortfolioNameId,candidateType,groupId,{ 
		async: true,
		callback: function(data){
		var dwr_response = data.split("|||");
		if(dwr_response.length==3)
		{
			if(dwr_response[0]!=null && dwr_response[0]!="")
			{
				document.getElementById("portfolioHeaderName").innerHTML=dwr_response[0];
				var tooltips=dwr_response[1];
				var tooltipsArr = tooltips.split(",");
				
				for(var i=1; i<tooltipsArr.length; i++)
				{
					if(tooltipsArr[i]!='')
					{
						try {$('#'+tooltipsArr[i]).tooltip();} catch (e) {}
					}
				}
			}
			
			
			
			//$('#loadingDiv').hide();	
		}
	},
	errorHandler:handleError  
	});
}

function showAffidavitDiv()
{
	$('#affidavitDataDiv').modal('show');
}


function getPreScreenFooter(jobId,dspqPortfolioNameId,candidateType,groupId)
{
	$('#jafLoadingDivL3').show();
	DSPQServiceAjax.getPreScreenFooter(jobId,dspqPortfolioNameId,candidateType,groupId,{ 
		async: true,
		callback: function(data)
		{
			document.getElementById("preScreenFooter").innerHTML=data;
			getHidePreviousBtn();
			$('#jafLoadingDivL3').hide();
	},
	errorHandler:handleError  
	});
	//$('#loadingDiv').hide();
}

function validatePreScreen(inputValue)
{
	$('#jafLoadingDiv').show();
	
	var jobId=""; 
	try { jobId=document.getElementById("dspqJobId").value; } catch (e) {}

	var dspqct=""; 
	try { dspqct=document.getElementById("dspqct").value; } catch (e) {}
	
	var dspqjobflowdistrictId="";
	try { dspqjobflowdistrictId=document.getElementById("dspqjobflowdistrictId").value; } catch (e) {}
	
	var dspqPortfolioNameId="";
	try { dspqPortfolioNameId=document.getElementById("dspqPortfolioNameId").value; } catch (e) {}
	
	var iReturnValidateTotalValue=0;
	var iReturnValidateValue=0;

	var arrPreScreen=[];
	
	var preScreenTotalQNo=getInputCtrlTextValue("preScreenTotalQNo");
	var preScreenSetId=null;
	try { preScreenSetId=$("#preScreenTotalQNo").attr("preScreenSetId"); } catch (e) {}
	var questionSets = {ID:preScreenSetId};
	var jobOrder = {jobId:jobId};
	
	var rdCheckedValueGlobal=null;
	var checkBoxValueGlobal=null;
	for(var i=1;i<=preScreenTotalQNo; i++)
	{
		var preScreenQId=0;
		try { preScreenQId=$("#preScreenSNo_"+i).attr("preScreenQId"); } catch (e) {}
		
		var preScreenShortCode="";
		try { preScreenShortCode=$("#preScreenSNo_"+i).attr("preScreenShortCode"); } catch (e) {}
		
		var preScreenQRequired=0;
		try { preScreenQRequired=$("#preScreenSNo_"+i).attr("preScreenQRequired"); } catch (e) {}

		var preScreenQuestionTypeId=0;
		try { preScreenQuestionTypeId=$("#preScreenSNo_"+i).attr("preScreenQuestionTypeId"); } catch (e) {}
		
		var preScreenQuestion=$("#preScreenSNo_"+i).html();
		var districtSpecificQuestion = {questionId:preScreenQId};
		var questionTypeMaster = {questionTypeId:preScreenQuestionTypeId};
		
		var validAnswerId=getInputCtrlTextValue("bValidOption_"+i);
		var validAnswerFlag=false;
		var validAnswerExists=validAnswerId.split(",")
		var validOption=false;
		var questionOptionGlobal="";
		
		var UserOptionId="";
		var UserTextArea="";
		
		if(preScreenShortCode=='et')
		{
			var checkedRdValue=getInputCtrlRadioValue("preScreenOpt"+i);
			if(checkedRdValue!="-1"){
				validAnswerFlag = getMatchStatus(validAnswerExists,checkedRdValue);
				if(validAnswerFlag)
					validOption=true;
			}
			var txtAreaValue=getTextAreaEditorCtrlTextValue_PreScreen(i);
			iReturnValidateValue=validatePreScreenControlStr(i,preScreenQRequired,preScreenShortCode,checkedRdValue,txtAreaValue);
			if(iReturnValidateValue=="1")
			{
				iReturnValidateTotalValue++;
			}
			rdCheckedValueGlobal=checkedRdValue;
			UserTextArea=txtAreaValue;
			
			questionOptionGlobal=getCustAttByNameForCheckedRadio("preScreenOpt"+i);
			
		}
		else if(preScreenShortCode=='ml')
		{
			var txtAreaValue=getTextAreaEditorCtrlTextValue_PreScreen(i);
			iReturnValidateValue=validatePreScreenControlStr(i,preScreenQRequired,preScreenShortCode,"",txtAreaValue);
			if(iReturnValidateValue=="1")
			{
				iReturnValidateTotalValue++;
			}
			UserTextArea=txtAreaValue;
			questionOptionGlobal="";
		}
		else if(preScreenShortCode=='tf' || preScreenShortCode=='slsel')
		{
			var checkedRdValue=getInputCtrlRadioValue("preScreenOpt"+i);
			if(checkedRdValue!="-1"){
				validAnswerFlag = getMatchStatus(validAnswerExists,checkedRdValue);
				if(validAnswerFlag)
					validOption=true;
			}
			iReturnValidateValue=validatePreScreenControlStr(i,preScreenQRequired,preScreenShortCode,checkedRdValue,"");
			if(iReturnValidateValue=="1")
			{
				iReturnValidateTotalValue++;
			}
			rdCheckedValueGlobal=checkedRdValue;
			UserTextArea="";
			
			questionOptionGlobal=getCustAttByNameForCheckedRadio("preScreenOpt"+i);
			
		}
		else if(preScreenShortCode=='mlsel')
		{
			var validF=false;
			var checkBoxValue=getInputCtrlCheckBoxValue("preScreenOpt"+i);
			var tempCheckBoxValue=checkBoxValue.split(",")
			if(tempCheckBoxValue.length==validAnswerExists.length){
				for(var j=0; validAnswerExists.length > j; j++)
				{
					validF = getMatchStatus(tempCheckBoxValue,validAnswerExists[j]);
					if(!validF)
						break;
				}
				if(validF)
					validOption=true;
			}
			//alert("validF "+validF +"  validAnswerExists "+validAnswerExists.length +"  tempCheckBoxValue"+tempCheckBoxValue.length);
			iReturnValidateValue=validatePreScreenControlStr(i,preScreenQRequired,preScreenShortCode,checkBoxValue,"");
			if(iReturnValidateValue=="1")
			{				
				iReturnValidateTotalValue++;
			}
			checkBoxValueGlobal=checkBoxValue;
			questionOptionGlobal=getCheckedValueCounter("preScreenOpt"+i);
			UserTextArea=null;
			rdCheckedValueGlobal=null;
		}
		
		arrPreScreen.push({ 
			"selectedOptions"  : rdCheckedValueGlobal,
			"selectedoptionmlsel"  : checkBoxValueGlobal,
			"question"  : preScreenQuestion,
			"questionTypeMaster" : questionTypeMaster,
			"questionType" : preScreenShortCode,
			"questionOption" : questionOptionGlobal,
			"districtSpecificQuestions" : districtSpecificQuestion,
			"questionSets"	: questionSets,
			"isValidAnswer" : validOption,
			"jobOrder" : jobOrder,
			"insertedText"    : UserTextArea
		});
		
	}
	if(iReturnValidateTotalValue==0)
	{
		DSPQServiceAjax.savePreScreenAnswerBySet(jobId,dspqct,arrPreScreen,questionSets,{
			async: false,
			errorHandler:handleError,
			callback: function(data){
			if(data!=null)
			{
				$('#jafLoadingDiv').hide();
				if(data=="OK")
				{
					if(inputValue=="0")
						getNextPage();
					else if(inputValue=="1")
						SaveAndExitPortfolioL2();
				}
				arrPreScreen =[];
			}
		}
		});
		$('#jafLoadingDiv').hide();	
		return true;
	}
	else
	{
		$('#jafLoadingDiv').hide();	
		return false;
	}
	
	$('#jafLoadingDiv').hide();	
}

function getPreScreenData(jobId,dspqPortfolioNameId,candidateType,groupId)
{
	$('#jafLoadingDivL1').show();
	DSPQServiceAjax.getDistrictSpecificQuestion(jobId,candidateType,{ 
		async: true,
		callback: function(data)
		{
			document.getElementById("preScreenDataAreaId").innerHTML=data;
			$('#jafLoadingDivL1').hide();
	},
	errorHandler:handleError  
	});
	//$('#loadingDiv').hide();
}

function getEPIFooter(jobId,dspqPortfolioNameId,candidateType,groupId)
{
	var txtEPIStatus=""; 
	try { txtEPIStatus=document.getElementById("txtEPIStatus").value; } catch (e) {}
	
	if(txtEPIStatus=="comp" || txtEPIStatus=="vlt")
	{
		$('#loadingDiv').show();
		DSPQServiceAjax.getEPIFooter(jobId,dspqPortfolioNameId,candidateType,groupId,{ 
		async: false,
		callback: function(data)
		{
			document.getElementById("epiFooter").innerHTML=data;
			getHidePreviousBtn();
			$('#loadingDiv').hide();
			showHideTopEPIBtn_Temp();
		},
		errorHandler:handleError  
		});
		$('#loadingDiv').hide();
		
		$(".rowGapEPIFooter").addClass("jafTop150");
	}
	
	
}

function showHideTopEPIBtn_Temp()
{
	var txtEPIStatus="";
	try { txtEPIStatus=document.getElementById("txtEPIStatus").value; } catch (e) {}
	//toprightEPIbtnCompVlt
	
	try {
		$('#topRightInvDivId').show();
	} catch (e) {
		// TODO: handle exception
	}
	
	if(txtEPIStatus=="" || txtEPIStatus=="icomp")
	{
		try {
			$('#toprightbtnInvCompVlt').hide();
			$('#toprightbtnInvL2').show();
			$('#toprightbtnInvL3').hide();
		} catch (e) {
			// TODO: handle exception
		}
	}
	else if(txtEPIStatus=="comp" || txtEPIStatus=="vlt")
	{
		try {
			$('#toprightbtnInvCompVlt').show();
			$('#toprightbtnInvL2').hide();
			$('#toprightbtnInvL3').hide();
		} catch (e) {
		// TODO: handle exception
		}
	}
}


function getJSIFooter(jobId,dspqPortfolioNameId,candidateType,groupId)
{
	var txtEPIStatus=""; 
	try { txtEPIStatus=document.getElementById("txtEPIStatus").value; } catch (e) {}
	
	if(txtEPIStatus=="comp" || txtEPIStatus=="vlt")
	{
		$('#loadingDiv').show();
		DSPQServiceAjax.getJSIFooter(jobId,dspqPortfolioNameId,candidateType,groupId,{ 
		async: false,
		callback: function(data)
		{
			document.getElementById("epiFooter").innerHTML=data;
			getHidePreviousBtn();
			$('#loadingDiv').hide();
			showHideTopEPIBtn_Temp();
		},
		errorHandler:handleError  
		});
		$('#loadingDiv').hide();
		
		$(".rowGapEPIFooter").addClass("jafTop150");
	}
}

function getTextAreaEditorCtrlTextValue_PreScreen(InputCtrlId)
{
	var inputCtrlValue="";
	try { inputCtrlValue=trim($('#divOpt_'+InputCtrlId).find(".jqte_editor").text().trim()); } catch (e) {}
	if(inputCtrlValue==null || inputCtrlValue=="")
	{
		try { inputCtrlValue=$('#txtAreaOpt_'+InputCtrlId).val(); } catch (e) {}
		if(inputCtrlValue==undefined)
			inputCtrlValue="";
	}
	return inputCtrlValue;
}

function validatePreScreenControlStr(SNo,Required,ShortCode,InputValue1,InputValue2)
{
	var iReturnValidateValue=0;
	if(Required=="1")
	{
		if(ShortCode=="et" && (InputValue1=="-1" || InputValue2==""))
		{
			if(InputValue1!="-1"){
				if($("#divOpt_"+SNo).is(':visible') && InputValue2=="")
					iReturnValidateValue=1;
				else
					iReturnValidateValue=0;
			}
			else
				iReturnValidateValue=1;
		}
		else if(ShortCode=="ml" && InputValue2=="")
		{
			iReturnValidateValue=1;
		}
		else if((ShortCode=="tf" || ShortCode=="slsel") && InputValue1=="-1")
		{
			iReturnValidateValue=1;
		}
		else if(ShortCode=="mlsel" && InputValue1=="")
		{
			iReturnValidateValue=1;
		}
		if(iReturnValidateValue=="1")
		{
			$("#preScreenSNo_"+SNo).addClass("DSPQRequired12");
			$("#lblpreScreenWarning_"+SNo).addClass("icon-warning DSPQRequired12");
			$("#lblpreScreenWarningBeforeSpace_"+SNo).html("&nbsp;");
			$("#lblpreScreenWarningAfterSpace_"+SNo).html("&nbsp;");
			iReturnValidateValue=1;
		}
		else
		{
			$("#preScreenSNo_"+SNo).removeClass("DSPQRequired12");
			$("#lblpreScreenWarning_"+SNo).removeClass("icon-warning DSPQRequired12");
			$("#lblpreScreenWarningBeforeSpace_"+SNo).html("");
			$("#lblpreScreenWarningAfterSpace_"+SNo).html("");
			iReturnValidateValue=0;
		}
	}
	return iReturnValidateValue;
}


function getCustAttByNameForCheckedRadio(inputControlName)
{
	var returnCustValue="";
	try { returnCustValue = $("input[name="+inputControlName+"]:checked").attr('questionoption');	} catch (e) {	}
	if(returnCustValue==undefined)
		returnCustValue="";
	return returnCustValue;
}

function getEPIData(jobId,dspqPortfolioNameId,candidateType,groupId)
{
	$('#jafLoadingDivL1').show();
	DSPQServiceAjax.getEPIServiceDataL1(jobId,candidateType,{ 
		async: false,
		callback: function(data)
		{
			document.getElementById("epiDataAreaId").innerHTML=data;
			try {$('#iconpophoverBase').tooltip();} catch (e) {}
			$('#jafLoadingDivL1').hide();
			getEPIFooter(jobId, dspqPortfolioNameId, candidateType, groupId);
						
	},
	errorHandler:handleError  
	});
	//$('#loadingDiv').hide();
}


function validateEPI(inputValue)
{
	var iReturnValidateTotalValue=0;
	var iReturnValidateValue=0;
	
	$('#jafLoadingDiv').show();
	
	var jobId=""; 
	try { jobId=document.getElementById("dspqJobId").value; } catch (e) {}

	var dspqct=""; 
	try { dspqct=document.getElementById("dspqct").value; } catch (e) {}
	
	var dspqjobflowdistrictId="";
	try { dspqjobflowdistrictId=document.getElementById("dspqjobflowdistrictId").value; } catch (e) {}
	
	var dspqPortfolioNameId="";
	try { dspqPortfolioNameId=document.getElementById("dspqPortfolioNameId").value; } catch (e) {}
	
	var txtEPIStatus="";
	try { txtEPIStatus=document.getElementById("txtEPIStatus").value; } catch (e) {}
	
	/*if(txtEPIStatus=="vlt")
	{
		iReturnValidateTotalValue++;
		$('#epiVltMsg').modal('show');
	}*/

	if(iReturnValidateTotalValue==0)
	{
		//alert("Going to next page");
		DSPQServiceAjax.jafValidationInventoryEPI(jobId,dspqct,txtEPIStatus,{
			async: false,
			errorHandler:handleError,
			callback: function(data){
			if(data!=null)
			{
				//alert("I am back");
				$('#jafLoadingDiv').hide();
				if(data=="OK")
				{
					if(inputValue=="0")
						getNextPage();
					else
						SaveAndExitPortfolioL2();
				}
			}
		}
		});
	}
	
	$('#jafLoadingDiv').hide();	
}

function validateJSI(inputValue)
{
	var iReturnValidateTotalValue=0;
	var iReturnValidateValue=0;
	
	$('#jafLoadingDiv').show();
	
	var jobId=""; 
	try { jobId=document.getElementById("dspqJobId").value; } catch (e) {}

	var dspqct=""; 
	try { dspqct=document.getElementById("dspqct").value; } catch (e) {}
	
	var dspqjobflowdistrictId="";
	try { dspqjobflowdistrictId=document.getElementById("dspqjobflowdistrictId").value; } catch (e) {}
	
	var dspqPortfolioNameId="";
	try { dspqPortfolioNameId=document.getElementById("dspqPortfolioNameId").value; } catch (e) {}
	
	var txtEPIStatus="";
	try { txtEPIStatus=document.getElementById("txtEPIStatus").value; } catch (e) {}
	
	/*if(txtEPIStatus=="vlt")
	{
		iReturnValidateTotalValue++;
		$('#jsiVltMsg').modal('show');
		//return false;
	}*/

	if(iReturnValidateTotalValue==0)
	{
		//alert("Going to next page");
		DSPQServiceAjax.jafValidationInventoryJSI(jobId,dspqct,txtEPIStatus,{
			async: false,
			errorHandler:handleError,
			callback: function(data){
			if(data!=null)
			{
				//alert("I am back");
				$('#jafLoadingDiv').hide();
				if(data=="OK")
				{
					if(inputValue=="0")
						getNextPage();
					else
						SaveAndExitPortfolioL2();
				}
			}
		}
		});
	}
	
	$('#jafLoadingDiv').hide();	
}

function CloseEPIVltMsg()
{
	$('#epiVltMsg').modal('hide');
}

function CloseJSIVltMsg()
{
	$('#jsiVltMsg').modal('hide');
}

function getJSIData(jobId,dspqPortfolioNameId,candidateType,groupId)
{
	$('#jafLoadingDivL1').show();
	DSPQServiceAjax.getJSIServiceDataL1(jobId,candidateType,{ 
		async: false,
		callback: function(data)
		{
			document.getElementById("epiDataAreaId").innerHTML=data;
			try {$('#iconpophoverBase').tooltip();} catch (e) {}
			
			$('#jafLoadingDivL1').hide();
			var txtEPIStatus=""; 
			try { txtEPIStatus=document.getElementById("txtEPIStatus").value; } catch (e) {}
			
			if(txtEPIStatus=="comp" || txtEPIStatus=="vlt")
			{
				getJSIFooter(jobId, dspqPortfolioNameId, candidateType, groupId);
			}
			else
			{
				getJSIFooterL2(jobId, dspqPortfolioNameId, candidateType, groupId);
			}
			
	},
	errorHandler:handleError  
	});
	//$('#loadingDiv').hide();
}


function getJSIFooterL2(jobId,dspqPortfolioNameId,candidateType,groupId)
{
	var txtEPIStatus=""; 
	try { txtEPIStatus=document.getElementById("txtEPIStatus").value; } catch (e) {}
	
	if(txtEPIStatus!="comp" && txtEPIStatus!="vlt")
	{
		$('#loadingDiv').show();
		DSPQServiceAjax.getJSIFooterL2(jobId,dspqPortfolioNameId,candidateType,groupId,{ 
		async: false,
		callback: function(data)
		{
			document.getElementById("epiFooter").innerHTML=data;
			getHidePreviousBtn();
			$('#loadingDiv').hide();
		},
		errorHandler:handleError  
		});
		$('#loadingDiv').hide();
		
		//$(".rowGapEPIFooter").addClass("jafTop150");
		try { $(".rowGapEPIFooter").addClass("jafTop150"); } catch (e) {}
	}
}


function getCurrentURL(sNextClickURL,sNextPageShortCode)
{
	var sCurrentPageShortCode=""; 
	try { sCurrentPageShortCode=document.getElementById("sCurrentPageShortCode").value; } catch (e) {}
	
	if(sNextPageShortCode==sCurrentPageShortCode)
	{
		//alert("Do not any process...");
		return false;
	}
	else
	{
		var dspqGlobalLster=0; 
		try { dspqGlobalLster=document.getElementById("dspqGlobalLster").value; } catch (e) {}
		
		if(dspqGlobalLster > 0)
		{
			try { document.getElementById("sNextClickURL").value=sNextClickURL; } catch (e) {}
			$('#globalListenerMsg').modal('show');
			return false;
		}
		else
		{
			$('#globalListenerMsg').modal('hide');
			return true;
		}
	}
}

function callSaveByCurrentPage(iInputValue)
{
	var sCurrentPageShortCode=""; 
	try { sCurrentPageShortCode=document.getElementById("sCurrentPageShortCode").value; } catch (e) {}
	
	var sNextClickURL=""; 
	try { sNextClickURL=document.getElementById("sNextClickURL").value; } catch (e) {}
	
	//alert("iInputValue"+iInputValue+" sCurrentPageShortCode "+sCurrentPageShortCode +" sNextClickURL "+sNextClickURL);
	
	if(iInputValue==0)
	{
		$('#globalListenerMsg').modal('hide');
		window.location.replace(sNextClickURL);
	}
	else if(iInputValue==1)
	{
		//alert("iInputValue "+iInputValue);
		$('#globalListenerMsg').modal('hide');
		if(sCurrentPageShortCode=="CL")
		{
			//alert("I am in "+sCurrentPageShortCode);
			/*var bReturnValue=validateDSPQPersonalGroup01Section01(2);
			if(bReturnValue)
				window.location.replace(sNextClickURL);*/
		}
		else if(sCurrentPageShortCode=="PI")
		{
			//alert("I am in "+sCurrentPageShortCode);
			var bReturnValue=validateDSPQPersonalGroup(2);
			if(bReturnValue)
				window.location.replace(sNextClickURL);
		}
		else if(sCurrentPageShortCode=="AC")
		{
			//alert("I am in "+sCurrentPageShortCode);
			var bReturnValue=validateDSPQAcademicGroup(2);
			if(bReturnValue)
				window.location.replace(sNextClickURL);
		}
		else if(sCurrentPageShortCode=="CR")
		{
			//alert("I am in "+sCurrentPageShortCode);
			var bReturnValue=validateDSPQCredentialsGroup(2);
			if(bReturnValue)
				window.location.replace(sNextClickURL);
		}
		else if(sCurrentPageShortCode=="PR")
		{
			//alert("I am in "+sCurrentPageShortCode);
			var bReturnValue=validateDSPQProfessionalGroup(2);
			if(bReturnValue)
				window.location.replace(sNextClickURL);
		}
		else if(sCurrentPageShortCode=="AF")
		{
			//alert("I am in "+sCurrentPageShortCode);
			var bReturnValue=validateDSPQG4Section25(2);
			if(bReturnValue)
				window.location.replace(sNextClickURL);
		}
		else if(sCurrentPageShortCode=="QQ")
		{
			//alert("I am in "+sCurrentPageShortCode);
			var bReturnValue=validatePreScreen(2);
			if(bReturnValue)
				window.location.replace(sNextClickURL);
		}
		else if(sCurrentPageShortCode=="EPI")
		{
			//alert("I am in "+sCurrentPageShortCode);
		}
		else if(sCurrentPageShortCode=="JSI")
		{
			//alert("I am in "+sCurrentPageShortCode);
		}
		else if(sCurrentPageShortCode=="Comp")
		{
			//alert("I am in "+sCurrentPageShortCode);
		}
	}
	
}

function getJAFCompleteProcess(jobId,dspqPortfolioNameId,candidateType,groupId)
{
	$('#jafLoadingDivL1').show();
	DSPQServiceAjax.getJAFCompleteProcess(jobId,dspqPortfolioNameId,candidateType,groupId,{ 
		async: false,
		callback: function(data)
		{
		
			var dwr_response = data.split("||");
			if(dwr_response.length>0)
			{
				if(dwr_response[0] > 0)
				{
					$('#unDoneMsgModelId').modal('show');
					document.getElementById("jafportfofliounDoneMsgId").innerHTML=dwr_response[2];
					document.getElementById("sFirstUnDoneURL").value=dwr_response[1];
					
					//$('#toprightcompbtn').hide();
					$('#comptopheaderDivId').hide();
					
				}
				else
				{
					var frdMsgType=""; 
					try { frdMsgType=document.getElementById("frdMsgType").value; } catch (e) {}
					
					if(frdMsgType==0 || frdMsgType==2 )
					{
						document.getElementById("completedDivId").innerHTML=value=dwr_response[3];;
					}
					//$('#toprightcompbtn').show();
					$('#comptopheaderDivId').show();
				}
			}
		
			
			/*else if(frdMsgType==1)
			{
				jafCompleteRedirection();
			}*/
			
			$('#jafLoadingDivL1').hide();
	},
	errorHandler:handleError  
	});

}

function validateJAFCompleteProcess()
{
	var iReturnValidateTotalValue=0;
	var iReturnValidateValue=0;
	
	//$('#jafLoadingDiv').show();
	
	var jobId=""; 
	try { jobId=document.getElementById("dspqJobId").value; } catch (e) {}

	var dspqct=""; 
	try { dspqct=document.getElementById("dspqct").value; } catch (e) {}
	
	var dspqjobflowdistrictId="";
	try { dspqjobflowdistrictId=document.getElementById("dspqjobflowdistrictId").value; } catch (e) {}
	
	var dspqPortfolioNameId="";
	try { dspqPortfolioNameId=document.getElementById("dspqPortfolioNameId").value; } catch (e) {}
	
	$('#jafLoadingDivL1').show();
	
	DSPQServiceAjax.validateJAFCompleteProcess(jobId,dspqPortfolioNameId,dspqct,{ 
	async: false,
	callback: function(data)
	{
		var dwr_response = data.split(",");
		if(dwr_response.length>0)
		{
			if(dwr_response[0]==0)
			{
				jafCompleteRedirection(); // If It is OK
			}
			else if(dwr_response[0]==1)
			{
				document.getElementById("completedErrorDivId").innerHTML=dwr_response[2];
			}
		}
		$('#jafLoadingDivL1').hide();
	},
	errorHandler:handleError  
	});
	//$('#jafLoadingDiv').show();
}

function jafCompleteRedirection()
{
	var jafDAFarwardURL=""; 
	try { jafDAFarwardURL=document.getElementById("jafDAFarwardURL").value; } catch (e) {}
	
	if(jafDAFarwardURL!="")
	{
		window.open(jafDAFarwardURL);
	}
	window.location.href='userdashboard.do';
}

function jafUnDoneRedirection()
{
	var sFirstUnDoneURL=""; 
	try { sFirstUnDoneURL=document.getElementById("sFirstUnDoneURL").value; } catch (e) {}
	
	if(sFirstUnDoneURL!="")
	{
		window.location.href=sFirstUnDoneURL;
	}
}

function globalLsterSetter() 
{
    $(".form-control").click(function() 
	{
		globalLsterSetterL2();
	});
	
	$(".radio").click(function() 
	{
		globalLsterSetterL2();
	});
	
	$(".checkbox").click(function() 
	{
		globalLsterSetterL2();
	});
	
	$(".custlstner").click(function() 
	{
		globalLsterSetterL2();
	});
}

function globalLsterSetterL2() 
{
	var dspqGlobalLster=0;
	try { dspqGlobalLster=document.getElementById("dspqGlobalLster").value; } catch (e) {}
	dspqGlobalLster++;
	try { document.getElementById("dspqGlobalLster").value=dspqGlobalLster; } catch (e) {}
}

//********************** Cover Letter *********************

function getGroup01Section01(jobId,dspqPortfolioNameId,candidateType,groupId,sectionId)
{
	$('#loadingDiv').show();
	DSPQServiceAjax.getGroup01Section01(jobId,dspqPortfolioNameId,candidateType,groupId,sectionId,{ 
		async: true,
		callback: function(data){
		var dwr_response = data.split("|||");
		if(dwr_response.length==3)
		{
			if(dwr_response[0]!=null && dwr_response[0]!="")
			{
				document.getElementById("group01section01").innerHTML=dwr_response[0];
				$('textarea').jqte();
				var tooltips=dwr_response[1];
				var tooltipsArr = tooltips.split(",");
				
				for(var i=1; i<tooltipsArr.length; i++)
				{
					if(tooltipsArr[i]!='')
					{
						try {$('#'+tooltipsArr[i]).tooltip();} catch (e) {alert(e);}
					}
				}
				
				var calDates=dwr_response[2];
				var calDatesArr = calDates.split(",");
				for(var i=1; i<calDatesArr.length; i++)
				{
					var calDateInputName=calDatesArr[i];
					if(calDateInputName!='')
					{
						try {cal.manageFields(calDateInputName, calDateInputName, "%m-%d-%Y");} catch (e) {}
					}
				}
				
				var txtOtherIdonotWant="-1";
				try { txtOtherIdonotWant=document.getElementById("txtOtherIdonotWant").value; } catch (e) {}
				if(txtOtherIdonotWant=="0")
				{
					try { $("#CLTypeL1_1").prop( "checked", true ); } catch (e) {}
				}
				else
				{
					try { $("#CLTypeL1_2").prop( "checked", true ); } catch (e) {}
				}
				
				var isCLTypeL1_2=false;
				try { isCLTypeL1_2=$("#CLTypeL1_2").is(':visible'); } catch (e) {}
				if(isCLTypeL1_2)
				{
					showCLTypOptions();
				}
				populateCLdata(jobId);
				try {$('#hrefToolTioCLUploadFileId').tooltip();} catch (e) {}
				
			}
			
			$('#loadingDiv').hide();	
		}
		
	},
	errorHandler:handleError  
	});
}

function validateIsAffilated()
{
	if(document.getElementById("isAffilated").checked)
	{
		//try {  } catch (e) {}
		try { $("#inst").show(); } catch (e) {}
		try { $("#noninst").show(); } catch (e) {}
		try { $("#partinst").show(); } catch (e) {}
		//try { $("#divEmpNumCoverLtr").show(); } catch (e) {}
		//try { $("#divZipCodeCoverLtr").show(); } catch (e) {}
	}
	else
	{
		try { $("#inst").hide(); } catch (e) {}
		try { $("#noninst").hide(); } catch (e) {}
		try { $("#partinst").hide(); } catch (e) {}
		//try { $("#divEmpNumCoverLtr").hide(); } catch (e) {}
		//try { $("#divZipCodeCoverLtr").hide(); } catch (e) {}
		
		/*$("#inst").hide();
		$("#noninst").hide();
		$("#partinst").hide();*/
		try { $("#staffType_I").prop( "checked", false ); } catch (e) {}
		try { $("#staffType_PI").prop( "checked", false ); } catch (e) {}
		try { $("#staffType_N").prop( "checked", false ); } catch (e) {}

	}
}

function populateCLdata(jobId)
{
	var dspqjobflowdistrictId="";
	try { dspqjobflowdistrictId=document.getElementById("dspqjobflowdistrictId").value; } catch (e) {}
	
	DSPQServiceAjax.getCLData(jobId,{ 
		async: true,
		callback: function(data){
		if(data!=null)
		{
			if(data.isAffilated=="1")
			{
				try { $("#isAffilated").prop( "checked", true ); } catch (e) {}
				if(dspqjobflowdistrictId!="804800")
				{
					validateIsAffilated();
				}
					
				
				if(data.staffType=="I")
				{
					try { $("#staffType_I").prop( "checked", true ); } catch (e) {}
					
				}
				else if(data.staffType=="PI")
				{
					try { $("#staffType_PI").prop( "checked", true ); } catch (e) {}
					
				}
				else if(data.staffType=="N")
				{
					try { $("#staffType_N").prop( "checked", true ); } catch (e) {}
					
				}
				//validateCandidateType();
			}
			else
			{
				
				try { $("#staffType_I").prop( "checked", true ); } catch (e) {}
				try { $("#isAffilated").prop( "checked", false ); } catch (e) {}
				try { $("#staffType_I").prop( "checked", false ); } catch (e) {}
				try { $("#staffType_PI").prop( "checked", false ); } catch (e) {}
				try { $("#staffType_N").prop( "checked", false ); } catch (e) {}
			}
			
			if(data.coverLetter!='')
			{
				try { $('#divTypeCoverLetter').find(".jqte_editor").html(data.coverLetter); } catch (e) {}
				try { $("textarea#txtAReaCoverLetter").val(data.coverLetter); } catch (e) {}
			}
			else if(data.coverLetterFileName!='' && data.coverLetterFileName!='0')
			{
				try { document.getElementById("CLTypeL1_5").checked=true; } catch (e) {}
				CLFileUpload();
			}
			else
			{
				try { $('#divTypeCoverLetter').find(".jqte_editor").html(""); } catch (e) {}
			}
			
			/*if(data.jobForTeacherId=='0')
			{
				$("#isAffilated").prop( "checked", false );
				$("#staffType_I").prop( "checked", false );
				$("#staffType_PI").prop( "checked", false );
				$("#staffType_N").prop( "checked", false );
				$('#divCoverLetter').find(".jqte_editor").html("");
				activeCLType();
			}*/
		}
		else
		{
			/*$("#isAffilated").prop( "checked", false );
			$("#staffType_I").prop( "checked", false );
			$("#staffType_PI").prop( "checked", false );
			$("#staffType_N").prop( "checked", false );
			$('#divCoverLetter').find(".jqte_editor").html("");
			activeCLType();*/
			
		}
	},
	errorHandler:handleError  
	});
}

function setLatestCoverLetter()
{
	var latestCoverletter=""; 
	latestCoverletter=$("#divLCL" ).html();
	$('#divTypeCoverLetter').find(".jqte_editor").html(latestCoverletter);
	
	$("textarea#txtAReaCoverLetter").val(latestCoverletter);
	
	document.getElementById("divTypeCoverLetter").style.display="block";
	$('#errordivCL').empty();
	$('#divTypeCoverLetter').find(".jqte_editor").css("background-color", "");
}

function setClBlank()
{
	$('#divTypeCoverLetter').find(".jqte_editor").html("");
	document.getElementById("divTypeCoverLetter").style.display="none";

	$('#errordivCL').empty();
	$('#divTypeCoverLetter').find(".jqte_editor").css("background-color", "");
	
	$("#spanrd2").removeClass("DSPQRequired12");
	$("#spanrd2Warn").removeClass("icon-warning DSPQRequired12");
	
	document.getElementById("addCLType").style.display="none";
	document.getElementById("lblCL1_1").style.display="block";
	
}

function CL_IdonotWant()
{
	//$("#spanrd2").removeClass("DSPQRequired12");
	//$("#spanrd2Warn").removeClass("icon-warning DSPQRequired12");
	
	//try { $('#divTypeCoverLetter').find(".jqte_editor").html(""); } catch (e) {}
	//try { document.getElementById("divTypeCoverLetter").style.display="none"; } catch (e) {}
	try { $('#divTypeCoverLetter').find(".jqte_editor").css("background-color", ""); } catch (e) {}
	try { document.getElementById("rdCLTypeOptions").style.display="none"; } catch (e) {}
	try { document.getElementById("addCoverLetter").style.display="block"; } catch (e) {}
	try { $('#errordivCL').empty(); } catch (e) {}
}

function setCLEnable(){
	$('#divCoverLetter').find(".jqte_editor").html("");
	document.getElementById("divCoverLetter").style.display="block";

	$('#errordivCL').empty();
	$('#divCoverLetter').find(".jqte_editor").css("background-color", "");
}

function showTypeCLEditor()
{
	try { $('#divTypeCoverLetter').find(".jqte_editor").html(""); } catch (e) {}
	try { document.getElementById("divTypeCoverLetter").style.display="block"; } catch (e) {}
	try { $('#divTypeCoverLetter').find(".jqte_editor").css("background-color", ""); } catch (e) {}
	try { $('#errordivCL').empty(); } catch (e) {}
}

function CLFileUpload()
{
	try { $('#divTypeCoverLetter').find(".jqte_editor").html(""); } catch (e) {}
	try { $('#divTypeCoverLetter').find(".jqte_editor").css("background-color", ""); } catch (e) {}
	try { document.getElementById("divTypeCoverLetter").style.display="none"; } catch (e) {}
	try { $('#errordivCL').empty(); } catch (e) {}
	
	try { document.getElementById("spncluploadId").style.display="block"; } catch (e) {}
	
}

function activeCLType()
{
	document.getElementById("addCLType").style.display="block";
	setCLEnable();
	$("#rdoCL1").prop( "checked", true );
	
	document.getElementById("lblCL1_1").style.display="none";
}

function showCLTypOptions()
{
	try { document.getElementById("rdCLTypeOptions").style.display="block"; } catch (e) {}
	try { document.getElementById("addCoverLetter").style.display="none"; } catch (e) {}
	
	var txtInputTypeCtrlIdForFocus=""; 
	try { txtInputTypeCtrlIdForFocus=document.getElementById("txtInputTypeCtrlIdForFocus").value; } catch (e) {}
	try { $("#"+txtInputTypeCtrlIdForFocus).prop( "checked", true ); } catch (e) {}
	
	if(txtInputTypeCtrlIdForFocus=="CLTypeL1_3")
	{
		showTypeCLEditor();
	}
	
}

function getTextAreaEditorCtrlTextValueCL(DivId,txtAreaId)
{
	var inputCtrlValue="";
	try { inputCtrlValue=trim($('#'+DivId).find(".jqte_editor").text().trim()); } catch (e) {}
	if(inputCtrlValue==null || inputCtrlValue=='')
	{
		try { inputCtrlValue=$('#'+txtAreaId).val(); } catch (e) {}
	}
	return inputCtrlValue;
}

function validateDSPQPersonalGroup01Section01(inputValue)
{
	//$('#jafLoadingDiv').show();
	var iReturnValidateTotalValue=0;
	var iReturnValidateValue=0;
	var clUploadFileName="";
	
	var jobId=""; 
	try { jobId=document.getElementById("dspqJobId").value; } catch (e) {}

	var dspqct=""; 
	try { dspqct=document.getElementById("dspqct").value; } catch (e) {}
	
	var dspqjobflowdistrictId="";
	try { dspqjobflowdistrictId=document.getElementById("dspqjobflowdistrictId").value; } catch (e) {}
	
	/*var isRequiredSection01="";
	try { isRequiredSection01=document.getElementById("isRequiredSection01").value; } catch (e) {}*/
	
	var CL_Section=0;
	var CL_Section_Name="tooltipSection01";
	
	$('#errordivCL').empty();
	
	
	try { $('#divTypeCoverLetter').find(".jqte_editor").css("background-color", ""); } catch (e) {}
	//try { $("#zipCodeCoverLtr").css("background-color", ""); } catch (e) {}
	//try { $("#empNumCoverLtr").css("background-color", ""); } catch (e) {}
	
	
	var CLInputSelectionTypeRequiredL1="";
	var InputValue_dspqRequired_CLTypeL1_1="";
	try { InputValue_dspqRequired_CLTypeL1_1=$("#CLTypeL1_1").attr("dspqRequired"); } catch (e) {}
	if(InputValue_dspqRequired_CLTypeL1_1=="1")
	{
		CLInputSelectionTypeRequiredL1="1";
	}
	
	var InputValue_dspqRequired_CLTypeL1_2="";
	try { InputValue_dspqRequired_CLTypeL1_2=$("#CLTypeL1_2").attr("dspqRequired"); } catch (e) {}
	if(InputValue_dspqRequired_CLTypeL1_2=="1")
	{
		CLInputSelectionTypeRequiredL1="1";
	}
	
	var CLInputSelectionTypeRequiredL2="";
	var CLInputSelectionTypeRequiredL2Value="";
	try { InputValue_dspqRequired_CLTypeL1_3=$("#CLTypeL1_3").attr("dspqRequired"); } catch (e) {}
	if(CLInputSelectionTypeRequiredL1=="1" && InputValue_dspqRequired_CLTypeL1_3=="1")
	{
		CLInputSelectionTypeRequiredL2="1";
	}
	
	var InputValue_dspqRequired_CLTypeL1_4="";
	try { InputValue_dspqRequired_CLTypeL1_4=$("#CLTypeL1_4").attr("dspqRequired"); } catch (e) {}
	if(CLInputSelectionTypeRequiredL1=="1" && InputValue_dspqRequired_CLTypeL1_4=="1")
	{
		CLInputSelectionTypeRequiredL2="1";
	}
	
	var InputValue_dspqRequired_CLTypeL1_5="";
	try { InputValue_dspqRequired_CLTypeL1_5=$("#CLTypeL1_5").attr("dspqRequired"); } catch (e) {}
	if(CLInputSelectionTypeRequiredL1=="1" && InputValue_dspqRequired_CLTypeL1_5=="1")
	{
		CLInputSelectionTypeRequiredL2="1";
	}
	
	var CLText="";
	CLText=getTextAreaEditorCtrlTextValueCL("divTypeCoverLetter", "txtAReaCoverLetter");
	if(CLText==undefined)
		CLText="";
	
	var InputCtrlId_CLTypeL1="CLTypeL1";
	var CLInputSelectionTypeRequiredL2Value=getInputCtrlRadioValue(InputCtrlId_CLTypeL1);
	//alert("CLInputSelectionTypeRequiredL2Value "+CLInputSelectionTypeRequiredL2Value);
	
	//alert("CLInputSelectionTypeRequiredL1 "+CLInputSelectionTypeRequiredL1 +" CLInputSelectionTypeRequiredL2 "+CLInputSelectionTypeRequiredL2);
	
	if(CLInputSelectionTypeRequiredL1=="1" && CLInputSelectionTypeRequiredL2=="1")
	{
		//alert("CLInputSelectionTypeRequiredL2Value "+CLInputSelectionTypeRequiredL2Value);
		if(CLInputSelectionTypeRequiredL2Value=="1")
		{
			iReturnValidateTotalValue=iReturnValidateTotalValue+1;
			var chk_2="CLTypeL1_2";
			MarkedLabelAsRed(chk_2,"lblFieldId_"+chk_2,"lblFieldIdWarning_"+chk_2);
		}
		else if(CLInputSelectionTypeRequiredL2Value=="3" || CLInputSelectionTypeRequiredL2Value=="4")
		{
			if(CLText=="")
			{
				var chk_3="CLTypeL1_3";
				MarkedLabelAsRed(chk_3,"lblFieldId_"+chk_3,"lblFieldIdWarning_"+chk_3);
				
				var chk_4="CLTypeL1_4";
				MarkedLabelAsRed(chk_4,"lblFieldId_"+chk_4,"lblFieldIdWarning_"+chk_4);
				
				var chk_5="CLTypeL1_5";
				MarkedLabelAsRed(chk_5,"lblFieldId_"+chk_5,"lblFieldIdWarning_"+chk_5);
				
				iReturnValidateTotalValue=iReturnValidateTotalValue+1;
			}
			else
			{
				var chk_3="CLTypeL1_3";
				UnMarkedLabelAsRed(chk_3,"lblFieldId_"+chk_3,"lblFieldIdWarning_"+chk_3);
				
				var chk_4="CLTypeL1_4";
				UnMarkedLabelAsRed(chk_4,"lblFieldId_"+chk_4,"lblFieldIdWarning_"+chk_4);
				
				var chk_5="CLTypeL1_5";
				UnMarkedLabelAsRed(chk_5,"lblFieldId_"+chk_5,"lblFieldIdWarning_"+chk_5);
			}
		}
		else if(CLInputSelectionTypeRequiredL2Value=="5")
		{
			try { clUploadFileName=document.getElementById("db_CLTypeL1_5").value; } catch (e) {}
			
			//alert("clUploadFileName "+clUploadFileName);
			
			if(clUploadFileName=="" || clUploadFileName=="0")
			{
				/*var chk_3="CLTypeL1_3";
				MarkedLabelAsRed(chk_3,"lblFieldId_"+chk_3,"lblFieldIdWarning_"+chk_3);
				
				var chk_4="CLTypeL1_4";
				MarkedLabelAsRed(chk_4,"lblFieldId_"+chk_4,"lblFieldIdWarning_"+chk_4);*/
				
				var chk_3="CLTypeL1_3";
				UnMarkedLabelAsRed(chk_3,"lblFieldId_"+chk_3,"lblFieldIdWarning_"+chk_3);
				
				var chk_4="CLTypeL1_4";
				UnMarkedLabelAsRed(chk_4,"lblFieldId_"+chk_4,"lblFieldIdWarning_"+chk_4);
				
				var chk_5="CLTypeL1_5";
				MarkedLabelAsRed(chk_5,"lblFieldId_"+chk_5,"lblFieldIdWarning_"+chk_5);
				
				iReturnValidateTotalValue=iReturnValidateTotalValue+1;
			}
			else
			{
				var chk_3="CLTypeL1_3";
				UnMarkedLabelAsRed(chk_3,"lblFieldId_"+chk_3,"lblFieldIdWarning_"+chk_3);
				
				var chk_4="CLTypeL1_4";
				UnMarkedLabelAsRed(chk_4,"lblFieldId_"+chk_4,"lblFieldIdWarning_"+chk_4);
				
				var chk_5="CLTypeL1_5";
				UnMarkedLabelAsRed(chk_5,"lblFieldId_"+chk_5,"lblFieldIdWarning_"+chk_5);
			}
		}
	}
	
	//Start ... DOB
	var InputCtrlId_dobMonth="dobMonth";
	var dobMonth=getInputCtrlTextValue(InputCtrlId_dobMonth);
	iReturnValidateValue=validateInputListControlStr(dobMonth,InputCtrlId_dobMonth,"lblFieldId_"+InputCtrlId_dobMonth,"lblFieldIdWarning_"+InputCtrlId_dobMonth);
	if(iReturnValidateValue=="1")
	{
		CL_Section=CL_Section+1;
		iReturnValidateTotalValue=iReturnValidateTotalValue+CL_Section;
	}
	
	var InputCtrlId_dobDay="dobDay";
	var dobDay=getInputCtrlTextValue(InputCtrlId_dobDay);
	iReturnValidateValue=validateInputListControlStr(dobDay,InputCtrlId_dobDay,"lblFieldId_"+InputCtrlId_dobDay,"lblFieldIdWarning_"+InputCtrlId_dobDay);
	if(iReturnValidateValue=="1")
	{
		CL_Section=CL_Section+1;
		iReturnValidateTotalValue=iReturnValidateTotalValue+CL_Section;
	}
	
	var InputCtrlId_dobYear="dobYear";
	var dobYear=getInputCtrlTextValue(InputCtrlId_dobYear);
	iReturnValidateValue=validateInputTextControlStr(dobYear,InputCtrlId_dobYear,"lblFieldId_"+InputCtrlId_dobYear,"lblFieldIdWarning_"+InputCtrlId_dobYear);
	if(iReturnValidateValue=="1")
	{
		CL_Section=CL_Section+1;
		iReturnValidateTotalValue=iReturnValidateTotalValue+CL_Section;
	}
	
	if(dobYear!='')
	{
		var idobYear = new String(parseInt(trim(dobYear)));
		var currentFullYear = new Date().getFullYear();
		currentFullYear=currentFullYear-1;
		if((dobYear=="") || ( dobYear==0 || idobYear=="NaN") || (idobYear > currentFullYear || idobYear < 1931))
		{
			MarkedLabelAsRed(InputCtrlId_dobYear,"lblFieldId_"+InputCtrlId_dobYear,"lblFieldIdWarning_"+InputCtrlId_dobYear)
			CL_Section=CL_Section+1;
			iReturnValidateTotalValue=iReturnValidateTotalValue+DOB_Section;
		}
		else
		{
			UnMarkedLabelAsRed(InputCtrlId_dobYear,"lblFieldId_"+InputCtrlId_dobYear,"lblFieldIdWarning_"+InputCtrlId_dobYear)
		}
	}
	
	var dob="";
	if(dobMonth!=null && dobMonth!="" && dobDay!=null && dobDay!="" && dobYear!=null && dobYear!="")
	{
		//dob=trim(dobMonth)+"-"+trim(dobDay)+"-"+trim(dobYear);
		//dob=trim(dobMonth)+""+trim(dobDay)+""+trim(dobYear);
		dob=trim(dobMonth.trim().length==1?"0"+dobMonth.trim():dobMonth.trim())+trim(dobDay.trim().length==1?"0"+dobDay.trim():dobDay.trim())+trim(dobYear);
		//alert("dob "+dob);
	}

	//End ... DOB
	
	
	var InputCtrlId_isAffilated="isAffilated";
	var isAffilated=getInputCtrlCheckBoxValue(InputCtrlId_isAffilated);
	iReturnValidateValue=validateInputCheckBoxControlStr(isAffilated,InputCtrlId_isAffilated,"lblFieldId_"+InputCtrlId_isAffilated,"lblFieldIdWarning_"+InputCtrlId_isAffilated,"hdn_"+InputCtrlId_isAffilated);
	if(iReturnValidateValue=="1")
	{
		CL_Section=CL_Section+1;
		iReturnValidateTotalValue=iReturnValidateTotalValue+CL_Section;
	}
	
	
	var InputCtrlId_CL_SSNLast4="CL_SSNLast4";
	var CL_SSNLast4=getInputCtrlTextValue(InputCtrlId_CL_SSNLast4);
	iReturnValidateValue=validateInputTextControlNumberWithMinLength(CL_SSNLast4,InputCtrlId_CL_SSNLast4,"lblFieldId_"+InputCtrlId_CL_SSNLast4,"lblFieldIdWarning_"+InputCtrlId_CL_SSNLast4,4);
	if(iReturnValidateValue=="1")
	{
		CL_Section=CL_Section+1;
		iReturnValidateTotalValue=iReturnValidateTotalValue+CL_Section;
	}
	
	var isAffilated=0;
	try {
		if(document.getElementById("isAffilated").checked)
			isAffilated=1;
	} catch (e) {}
	
	var empNumCoverLtr="";
	//var zipCodeCoverLtr="";
	if(isAffilated==1)
	{
		/*var InputCtrlId_empNumCoverLtr="empNumCoverLtr";
		empNumCoverLtr=getInputCtrlTextValue(InputCtrlId_empNumCoverLtr);
		iReturnValidateValue=validateInputTextControlStr(empNumCoverLtr,InputCtrlId_empNumCoverLtr,"lblFieldId_"+InputCtrlId_empNumCoverLtr,"lblFieldIdWarning_"+InputCtrlId_empNumCoverLtr);
		if(iReturnValidateValue=="1")
		{
			CL_Section=CL_Section+1;
			iReturnValidateTotalValue=iReturnValidateTotalValue+CL_Section;
		}
		
		
		var InputCtrlId_zipCodeCoverLtr="zipCodeCoverLtr";
		zipCodeCoverLtr=getInputCtrlTextValue(InputCtrlId_zipCodeCoverLtr);
		iReturnValidateValue=validateInputTextControlStr(zipCodeCoverLtr,InputCtrlId_zipCodeCoverLtr,"lblFieldId_"+InputCtrlId_zipCodeCoverLtr,"lblFieldIdWarning_"+InputCtrlId_zipCodeCoverLtr);
		if(iReturnValidateValue=="1")
		{
			CL_Section=CL_Section+1;
			iReturnValidateTotalValue=iReturnValidateTotalValue+CL_Section;
		}*/
		
		
		var InputCtrlId_staffType="staffType";
		var staffType=getInputCtrlRadioValue(InputCtrlId_staffType);
		iReturnValidateValue=validateInputRadioControlStr(staffType,InputCtrlId_staffType,"lblFieldId_"+InputCtrlId_staffType,"lblFieldIdWarning_"+InputCtrlId_staffType,"hdn_"+InputCtrlId_staffType);
		
		//alert("staffType "+staffType +" iReturnValidateValue "+iReturnValidateValue);
		
		if(iReturnValidateValue=="1")
		{
			CL_Section=CL_Section+1;
			iReturnValidateTotalValue=iReturnValidateTotalValue+CL_Section;
			
			var rdchk_staffType_I="staffType_I";
			MarkedLabelAsRed(rdchk_staffType_I,"lblFieldId_"+rdchk_staffType_I,"lblFieldIdWarning_"+rdchk_staffType_I);
			
			var rdchk_staffType_PI="staffType_PI";
			MarkedLabelAsRed(rdchk_staffType_PI,"lblFieldId_"+rdchk_staffType_PI,"lblFieldIdWarning_"+rdchk_staffType_PI);
			
			var rdchk_staffTyp_Ne="staffTyp_Ne";
			MarkedLabelAsRed(rdchk_staffTyp_Ne,"lblFieldId_"+rdchk_staffTyp_Ne,"lblFieldIdWarning_"+rdchk_staffTyp_Ne);
			
		}
		else
		{
			
			var rdchk_staffType_I="staffType_I";
			UnMarkedLabelAsRed(rdchk_staffType_I,"lblFieldId_"+rdchk_staffType_I,"lblFieldIdWarning_"+rdchk_staffType_I);
			
			var rdchk_staffType_PI="staffType_PI";
			UnMarkedLabelAsRed(rdchk_staffType_PI,"lblFieldId_"+rdchk_staffType_PI,"lblFieldIdWarning_"+rdchk_staffType_PI);
			
			var rdchk_staffTyp_Ne="staffTyp_Ne";
			UnMarkedLabelAsRed(rdchk_staffTyp_Ne,"lblFieldId_"+rdchk_staffTyp_Ne,"lblFieldIdWarning_"+rdchk_staffTyp_Ne);
			
		}
		
		
		/*if(staffType=="-1")
		{
			var rdchk_staffType_I="staffType_I";
			MarkedLabelAsRed(rdchk_staffType_I,"lblFieldId_"+rdchk_staffType_I,"lblFieldIdWarning_"+rdchk_staffType_I);
			
			var rdchk_staffType_PI="staffType_PI";
			MarkedLabelAsRed(rdchk_staffType_PI,"lblFieldId_"+rdchk_staffType_PI,"lblFieldIdWarning_"+rdchk_staffType_PI);
			
			var rdchk_staffTyp_Ne="staffTyp_Ne";
			MarkedLabelAsRed(rdchk_staffTyp_Ne,"lblFieldId_"+rdchk_staffTyp_Ne,"lblFieldIdWarning_"+rdchk_staffTyp_Ne);
		}
		else
		{
			var rdchk_staffType_I="staffType_I";
			UnMarkedLabelAsRed(rdchk_staffType_I,"lblFieldId_"+rdchk_staffType_I,"lblFieldIdWarning_"+rdchk_staffType_I);
			
			var rdchk_staffType_PI="staffType_PI";
			UnMarkedLabelAsRed(rdchk_staffType_PI,"lblFieldId_"+rdchk_staffType_PI,"lblFieldIdWarning_"+rdchk_staffType_PI);
			
			var rdchk_staffTyp_Ne="staffTyp_Ne";
			UnMarkedLabelAsRed(rdchk_staffTyp_Ne,"lblFieldId_"+rdchk_staffTyp_Ne,"lblFieldIdWarning_"+rdchk_staffTyp_Ne);
		}*/
		
	}
	
	var CL_Section1=validateCustomAll(CL_Section_Name);
	iReturnValidateTotalValue=iReturnValidateTotalValue+CL_Section1;
	
	if(iReturnValidateTotalValue>0)
	{
		window.location.hash = '#lblSectionId_'+CL_Section_Name;
		$("#lblSectionId_"+CL_Section_Name).addClass("DSPQRequired12");
	}
	else
	{
		$("#lblSectionId_"+CL_Section_Name).removeClass("DSPQRequired12");
	}
	
	if(iReturnValidateTotalValue==0)
	{
		var clUploadFileName="";
		try { clUploadFileName=document.getElementById("db_CLTypeL1_5").value; } catch (e) {}
		//alert("1 clUploadFileName "+clUploadFileName);
		
		//Start ... Jeffco specific code
		var isNext=true;
		try{
			if(dspqjobflowdistrictId==804800)
			{
				if(messageShowOrNot(dob,CL_SSNLast4,dspqjobflowdistrictId))
				{
					isNext=false;
				}
				isAffilated=getInputCtrlTextValue("txtHdnFld_Jeffco_isAffilated");
				staffType=getInputCtrlTextValue("txtHdnFld_Jeffco_staffType");
				empNumCoverLtr=getInputCtrlTextValue("txtHdnFld_Jeffco_empNumCoverLtr");
			}
		}catch(e){alert(e);}
		//End ... Jeffco specific code
		if(isNext)
		{
			
			//alert("isAffilated "+isAffilated);
			
			DSPQServiceAjax.saveGroup01Section01CoverLetter(jobId,isAffilated,CLText,staffType,empNumCoverLtr,clUploadFileName,{ 
				async: false,
				callback: function(data)
				{
					$('#jafLoadingDiv').hide();
					var arrData=data.split("||");
					if(arrData[0]=="OK")
					{
						if(arrData[1]!="")
						{
							document.getElementById("dspqct").value=arrData[1];
							document.getElementById("sNextURL").value=arrData[2];
						}
						if(inputValue=="0")
							getNextPage();
						else if(inputValue=="1")
							SaveAndExitPortfolioL2();
						else if(inputValue=="11")
						{
							$('#jafLoadingDiv').hide();
							return true;
						}
					}
				},
				errorHandler:handleError  
			});
			$('#jafLoadingDiv').hide();
			return true;
			
		}
		else // need to review
		{
			$('#jafLoadingDiv').hide();
			return false;
		}
		
	}
	else
	{
		$('#jafLoadingDiv').hide();
		return false;
	}
}

function jafUploadCLFile(fileName)
{
	//alert("Start upload process ...");
	var CL_Section_Name="tooltipSection01";
	var iReturnValidateValue=0;
	var db_resume=getInputCtrlTextValue("db_CLTypeL1_5");
	var InputCtrlId_clupload="clupload";
	resume=getInputCtrlTextValue(InputCtrlId_clupload);
	iReturnValidateValue=validateInputFileControlStr(resume,InputCtrlId_clupload,"lblFieldId_"+InputCtrlId_clupload,"lblFieldIdWarning_"+InputCtrlId_clupload,db_resume);
	if(iReturnValidateValue=="1")
	{
		iReturnValidateValue=iReturnValidateValue+1;
	}
	if(iReturnValidateValue>0)
	{
		
		var chk_5="CLTypeL1_5";
		MarkedLabelAsRed(chk_5,"lblFieldId_"+chk_5,"lblFieldIdWarning_"+chk_5);
		
		$("#lblSectionId_"+CL_Section_Name).addClass("DSPQRequired12");
	}
	else
	{
		var chk_5="CLTypeL1_5";
		UnMarkedLabelAsRed(chk_5,"lblFieldId_"+chk_5,"lblFieldIdWarning_"+chk_5);
		
		$("#lblSectionId_"+CL_Section_Name).removeClass("DSPQRequired12");
		document.getElementById("jafCLUploadfrm").submit();
	}
}

function jafUploadCLFileByServlet(fileName)
{
	try { document.getElementById("db_CLTypeL1_5").value=fileName; } catch (e) {}
	
	/*var clUploadFileName="";
	try { clUploadFileName=document.getElementById("db_CLTypeL1_5").value; } catch (e) {}
	alert("0 clUploadFileName "+clUploadFileName);*/
	
}


function downloadCLUploadFile(linkId)
{	
	var jobId=""; 
	try { jobId=document.getElementById("dspqJobId").value; } catch (e) {}
	
	DSPQServiceAjax.downloadCLUploadFile(jobId,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	if(data=="")
			{
				data="javascript:void(0)";
			}
			else if(data.indexOf(".doc")!=-1)
			{
				document.getElementById("ifrmCLDownload").src=data;
			}
			else
			{
				document.getElementById(linkId).href = data;
			}
			return false;
		}});
}


function jafCheckForIntEmpNum(evt)
{
	var dspqjobflowdistrictId="";
	try { dspqjobflowdistrictId=document.getElementById("dspqjobflowdistrictId").value; } catch (e) {}
	
	if(dspqjobflowdistrictId==1200390)
	{
		var charCode = ( evt.which ) ? evt.which : event.keyCode;
		if(charCode==8)
			return true;
	
		return( (charCode >= 48 && charCode <= 57)||(charCode==13) );
	}
}


function jafShowEmpNoDiv(divId)
{
	if(divId==0)
	{
		try { document.getElementById("empDivId1").style.display="block"; } catch (e) {}
		try { document.getElementById("empDivId2").style.display="none"; } catch (e) {}
	}
	else if(divId==1)
	{
		try { document.getElementById("empDivId1").style.display="none"; } catch (e) {}
		try { document.getElementById("empDivId2").style.display="block"; } catch (e) {}
	}
	else if(divId==2)
	{
		try { document.getElementById("empDivId1").style.display="none"; } catch (e) {}
		try { document.getElementById("empDivId2").style.display="none"; } catch (e) {}
	}
}

function jafCheckEmployeeDetail()
{
	var returnValue=0;
	var jobId=""; 
	try { jobId=document.getElementById("dspqJobId").value; } catch (e) {}
	
	DSPQServiceAjax.jafCheckEmployeeDetail(jobId,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data)
		{
			
			if(data[2]=="1" && data[1]=="RD")
				data[0]="1";
		
			if(data[0]=="0")
			{
				//alert("3");
				//alert("Do not Apply");
				/*
				  R - Restricted (PC)
				  A - Allowed
				  NFP - Not Fulltime nor Parttime
				  P - Parttime
				  F - Fulltime
				  RD - Redirect
				 */
				var retVal = data[1];
				
				var msg = "";
				if(retVal=="R")//PC
				{
					msg=resourceJSON.msgOfficeProfessionalStandards;
					returnValue=2;
				}
				else if(retVal=="RD")
				{
					msg=resourceJSON.msgCurrEmpOfMiami+" <a href='http://jobs.dadeschools.net/teachers/Index.asp'>http://jobs.dadeschools.net/teachers/Index.asp</a>";
					returnValue=2;
				}
				else if(retVal=="NFP")
				{
					msg=resourceJSON.msgRecordsIndicatePosition;
					returnValue=2;
				}
				else if(retVal=="P")
				{
					msg=resourceJSON.msgParttimeemploymentwithMiamiDade+"<a href='mailto:employeeservices@dadeschools.net'>employeeservices@dadeschools.net</a>. ";
					try{$('#jobApplyOrNot').modal('show');}catch(e){}
					returnValue=2;

				}
				else if(retVal=="F")
				{
					msg=resourceJSON.msgRecordsIndicatePosition;
					returnValue=2;
				}
				
				//alert("L2 msg "+msg +" retVal "+retVal +" returnValue "+returnValue);
				if(returnValue=="2")
				{
					$("#notApplyMsg").html(msg);
					$("#perform").val(retVal);
					try{$('#jobApplyOrNot').modal('show');}catch(e){}
				}
				//return;
			}
		}
	});
	
	return returnValue;
}

function performAction()
{
	var retVal= $("#perform").val();
	var msg="";
	if(retVal=="R")//PC
		notifyUsers(retVal);
	else if(retVal=="RD")
	{
		checkPopup('http://jobs.dadeschools.net/teachers/Index.asp');
		notifyUsers(retVal);
	}else if(retVal=="NFP")
		//msg="You cannot apply either Full Time job or Part Time job.";
		notifyUsers(retVal);
	else if(retVal=="P")
		//msg="You are a former employee of Miami Dade but not eligible for applying this job as it is a part time job.";
		notifyUsers(retVal);
	else if(retVal=="F")
		notifyUsers(retVal);
}

function checkPopup(url) {
	var openWin = window.open(url,"directories=no,height=400,width=600,menubar=no,resizable=no,scrollbars=no,status=no,titlebar=no,top=0,location=no");
	if (!openWin) {
		window.location.href=url;
	} else {
		window.open(url,"directories=no,height=400,width=600,menubar=no,resizable=no,scrollbars=no,status=no,titlebar=no,top=0,location=no");
	}
}

function notifyUsers(retVal)
{
	var jobId=0; 
	try { jobId=document.getElementById("dspqJobId").value; } catch (e) {}
	
	if(jobId!=null && jobId!="" && jobId > 0 )
	{
		DSPQServiceAjax.jafNotifyUsers(jobId,retVal,{ 
			async: true,
			errorHandler:handleError,
			callback: function(data)
			{
				//alert("data: "+retVal);
				window.location.href="userdashboard.do";
			}
			});
	}
}


function jafMiamiValidation()
{
	var returnValue=0;
	var jobId=""; 
	try { jobId=document.getElementById("dspqJobId").value; } catch (e) {}

	/*var dspqct=""; 
	try { dspqct=document.getElementById("dspqct").value; } catch (e) {}*/
	
	var dspqjobflowdistrictId=0;
	try { dspqjobflowdistrictId=document.getElementById("dspqjobflowdistrictId").value; } catch (e) {}
	
	var isContinue=false;
	
	if(jobId!=null && jobId!="" && jobId > 0 )
	{
		DSPQServiceAjax.jafMiamiValidation(dspqjobflowdistrictId,jobId,{ 
			async: false,
			errorHandler:handleError,
			callback: function(data)
			{
				if(data[0]==1)
				{
					isContinue=true;
				}
				else
				{
					isContinue=false;
					if(data[1]==0)
					{
						var msg=resourceJSON.msgEmployeeOfMiami+"<a href='http://jobs.dadeschools.net/teachers/Index.asp'>http://jobs.dadeschools.net/teachers/Index.asp</a>";
						$("#notApplyMsg").html(msg);
						$("#perform").val("RD");
						try{$('#jobApplyOrNot').modal('show');}catch(e){}
						returnValue=2;
					}
					else if(data[1]==3) //Philadaphia check
					{
						var msg=resourceJSON.msgDearApplicant+"<br><br>"+resourceJSON.msgIntrestOfTeching+" " +
						""+resourceJSON.msgadditionalquestions+"<a href='mailto:recruitment@philasd.org' target='_top'>recruitment@philasd.org</a>." +
						"<br><br>"+resourceJSON.msgSincerely+",<br>" +
						resourceJSON.msgOfficeofTalent;
						
						$('#jafMiamiMessage2showConfirm').html(msg);
						try{ $('#jafMiamiModal3').modal('show'); }catch(ee){}
						returnValue=2;
					}            
					else if(data[1]==2) //DrugFailed
					{
						console.log("3 ");
						$('#jafMiamiMessage2showConfirm').html(resourceJSON.msgPositivedrugreport);
						try{ $('#jafMiamiModal3').modal('show'); }catch(ee){}
						returnValue=2;
					}
					else 
					{
						console.log("4 ");
						$('#jafMiamiMessage2showConfirm').html(resourceJSON.msgCurrentEmploymentsection);
						try{ $('#jafMiamiModal3').modal('show'); }catch(ee){}
						returnValue=2;
					}
				}
		
			}
			});
		
		if(isContinue)
		{
			returnValue=jafCheckEmployeeDetail();
		}
	}
	
	return returnValue;
}

function hideShowSpknLng()
{
	var spokenLanguage = document.getElementById("spokenLanguage");
	var spokenLanguageValue = spokenLanguage.options[spokenLanguage.selectedIndex].value;
	if(spokenLanguageValue=="-1")
	{
		document.getElementById("divlanguageText").style.display="block";
	}
	else
	{
		document.getElementById("divlanguageText").style.display="none";
	}
	
}

function jafShowDivForRetirementDate()
{
	var chkRetiredFrom;
	try {chkRetiredFrom = document.getElementById("chkRetiredFrom").checked; } catch (e) {}
	if(chkRetiredFrom)
	{
		try { document.getElementById("retirementDateDivId").style.display="block"; } catch (e) {}
		
	}
	else
	{
		try { document.getElementById("retirementDateDivId").style.display="none"; } catch (e) {}
		
	}
}

function jafDateOfWithdrawnDivId()
{
	var memberWithdrawnFunds;
	try {memberWithdrawnFunds= document.getElementById("memberWithdrawnFunds").checked; } catch (e) {}
	if(memberWithdrawnFunds)
	{
		try { document.getElementById("dateOfWithdrawnDivId").style.display="block"; } catch (e) {}
		
	}
	else
	{
		try { document.getElementById("dateOfWithdrawnDivId").style.display="none"; } catch (e) {}
		
	}
}

function oneBtnJobApply()
{
	var jobId=""; 
	try { jobId=document.getElementById("dspqJobId").value; } catch (e) {}

	var dspqct=""; 
	try { dspqct=document.getElementById("dspqct").value; } catch (e) {}
	
	var dspqjobflowdistrictId="";
	try { dspqjobflowdistrictId=document.getElementById("dspqjobflowdistrictId").value; } catch (e) {}
	
	var dspqPortfolioNameId="";
	try { dspqPortfolioNameId=document.getElementById("dspqPortfolioNameId").value; } catch (e) {}
	
	var clStatusForQuickJobApply=validateDSPQPersonalGroup01Section01(11);
	if(clStatusForQuickJobApply)
	{
		$('#jafLoadingDivL1').show();
		DSPQServiceAjax.oneBtnJobApply(jobId,dspqPortfolioNameId,candidateType,{ 
			async: false,
			callback: function(data)
			{
			
				var dwr_response = data.split("||");
				if(dwr_response.length>0)
				{
					if(dwr_response[0] > 0)
					{
						$('#oneBtnJobApplyModelId').modal('show');
						document.getElementById("jafoneBtnJobApplyUnDoneMsgId").innerHTML=dwr_response[2];
						document.getElementById("sFirstUnDoneURL").value=dwr_response[1];
					}
					else
					{
						$('#oneBtnJobApplyModelIdL2').modal('show');
					}
				}
			
				
				/*else if(frdMsgType==1)
				{
					jafCompleteRedirection();
				}*/
				
				$('#jafLoadingDivL1').hide();
		},
		errorHandler:handleError  
		});
	}
	else
	{
		
	}
	

}


//Start .. Jeffco specific code

var arrNoOfDaysByMonthNo={};
arrNoOfDaysByMonthNo[1]=31;
arrNoOfDaysByMonthNo[2]=29;
arrNoOfDaysByMonthNo[3]=31;
arrNoOfDaysByMonthNo[4]=30;
arrNoOfDaysByMonthNo[5]=31;
arrNoOfDaysByMonthNo[6]=30;
arrNoOfDaysByMonthNo[7]=31;
arrNoOfDaysByMonthNo[8]=31;
arrNoOfDaysByMonthNo[9]=30;
arrNoOfDaysByMonthNo[10]=31;
arrNoOfDaysByMonthNo[11]=30;
arrNoOfDaysByMonthNo[12]=31;

function getDayByMonthNo(iMonthValue)
{
	var dayListValue="<option value='0'>Select Day</option>";
	try{
	for(var i=1; i<=arrNoOfDaysByMonthNo[iMonthValue]; i++)
		dayListValue+="<option value=\""+i+"\">"+i+"</option>";
	}catch(e){}
	$('#dobDay').html(dayListValue);
}

function messageShowOrNot(dob,last4SSN,dspqjobflowdistrictId)
{
	var employeeId="0";
	var messageShowFlag=false;
	try{
	var checkDistrictId=dspqjobflowdistrictId;
	var isAffilated=false;
	if(checkDistrictId!=null && checkDistrictId==804800)
	{
		try{
			DSPQServiceAjax.doNotHiredForJeffco(dob,last4SSN.trim(),{
	              async: false,
	              errorHandler:handleError,
	              callback:function(data){
		    		if(data!=null)
		    		{
			    		if(data.split("##")[1]=='A')
			    		{
			    			$('#txtHdnFld_Jeffco_staffType').val("I");
			    			$('#txtHdnFld_Jeffco_isAffilated').val(1);
			    			
			    		}
			    		if(data.split("##")[0].trim()!='')
			    			$('#txtHdnFld_Jeffco_empNumCoverLtr').val(data.split("##")[0]);
		    		}
		    		
	              //alert("success");
	              //messageShowFlag= true;
	        }
	              
	        });

		}catch(err){}
		//}
		//=============================================Ended For Employee Master =======================================
		try{
			try{
				employeeId=$('#txtHdnFld_Jeffco_empNumCoverLtr').val();
				//alert(employeeId);
			}catch(ee){employeeId="0";}
			DSPQServiceAjax.getDoNotHireStatusByEmpoloyeeId(employeeId,checkDistrictId,dob,last4SSN.trim(),{
			//preHook:function(){$('#loadingDiv').show()},
			//postHook:function(){$('#loadingDiv').hide()},
			async: false,
			errorHandler:handleError,
			callback: function(data)
			{
				var callbackInfo=data.split('##');
				if(callbackInfo[0]==1){
					messageShowFlag=true;
					getConfirmBox(callbackInfo[1],callbackInfo[2], "Ok");
				}
			}
		});
		}catch(e){alert(e);}
	}
	}catch(ee){}
	if(messageShowFlag)
		return true;
	else 
		return false;
}

function getConfirmBox(header,confirmMessage,buttonNameChange)
{
	$('#loadingDiv').show();
	var div="<div class='modal hide in' id='jeffcoConfirmbox' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true' style='z-index:999999;'>"+
			"<div class='modal-dialog' style='width:550px;'>"+
			"<div class='modal-content'>"+
			"<div class='modal-header'>"+
	        "<button type='button' class='close confirmCloseCut' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>"+
	        "<h4 class='modal-title' id='myModalLabel' style='color:white;'></h4>"+
	      "</div>"+
	      "<div class='modal-body' id='confirmMessage'>"+	      
	      "</div>"+
	      "<div class='modal-footer'>"+
	      "<button type='button' class='btn btn-primary' id='confirmTrue' ></button>"+
	        "<!--<button type='button' class='btn btn-default' id='confirmFalse'>"+buttonNameChange+"</button>-->"+	        
	      "</div>"+
	    "</div>"+
	  "</div>"+
	"</div>";
	$('#loadingDiv').show();
	 var coverShowOrNot=0;
	 var timeDur=0;
	    try{coverShowOrNot=$('#coverShowOrNot').val();timeDur=6000;}catch(e){coverShowOrNot=0;}
	try{$('#myModalv').modal('hide');}catch(e){}
	$('#loadingDiv').show();
	setTimeout(function(){ 
	$('#loadingDiv').show();
	try{
	$('#loadingDiv').after(div);
	}catch(e){}
	
    confirmMessage = confirmMessage || '';
    $('#jeffcoConfirmbox').modal({show:true,
                            backdrop: "static",
                            keyboard: false,
    });
    $('#loadingDiv').hide();
    //try{$('#myModalCL').css('z-index','1000');}catch(e){}
    $('#jeffcoConfirmbox').css('z-index','99999');

    $('#jeffcoConfirmbox #confirmMessage').html(confirmMessage);
    $('#jeffcoConfirmbox #myModalLabel').html(header);
    $('#jeffcoConfirmbox #confirmTrue').html(buttonNameChange);
    var height=(screen.height/2)-$('#jeffcoConfirmbox .modal-dialog').height();
    $('#jeffcoConfirmbox .modal-dialog').css('margin-top',height);
    $('#confirmTrue').click(function(){
    	//try{$('#myModalCL').css('z-index','99999');}catch(e){}
    	$('#jeffcoConfirmbox').modal('hide');
    	//try{if(coverShowOrNot==0)$('#myModalCL').modal('show');}catch(e){}
        $('#jeffcoConfirmbox').remove();
    });
    $('.confirmCloseCut').click(function(){
    	//try{$('#myModalCL').css('z-index','99999');}catch(e){}
    	$('#jeffcoConfirmbox').modal('hide');
    	//try{if(coverShowOrNot==0)$('#myModalCL').modal('show');}catch(e){}
        $('#jeffcoConfirmbox').remove();
    });
   },timeDur);
}


function openJeffcoEmployeeNumberInfo()
{
	var empId="";
	try {
		empId=document.getElementById("jeffcoEmpNo").value;
	} catch (e) {
		// TODO: handle exception
	}
	
	var zipCode="";
	try {
		zipCode=document.getElementById("jeffcoZipCode").value;
	} catch (e) {
		// TODO: handle exception
	}
	
	var empfe2= empId;//$("#empfe2").val();
	var tempCode=$("#jeffcoSeachDiv").html();
	$("#jeFFcoEmployeeInf").hide();
	if(empfe2=="")
	{		
		$("#jeffcoSeachDiv").html("<div class='required' id='empInfoDetError'>&#149; Please provide Employee Number.</div>"+tempCode);
		$("#jeffcoSeachDiv").addClass("top5");
	    $("#jeffcoSeachDiv").removeClass("top20");
	}
	else
	{
		$("#empInfoDetError").remove();
		$("#jeffcoSeachDiv").addClass("top20");
	    $("#jeffcoSeachDiv").removeClass("top5");
	    $('#loadingDiv_dspq_ie').show();
	    
	  try{
		  DSPQServiceAjax.openEmployeeNumberInfo(empfe2,{
			  async: false,
			  cache: false,
			  errorHandler:handleError,
			  callback:function(data){			 
			  var divData=data.split('####');
			  if(divData[0].trim()=='1'){
				  //$("#jeFFcoEmployeeInf").show();
				  //	$('#jeFFcoEmployeeInf').html(divData[1]);
				  	
				  	$("#jeffcoSeachDiv").html("<div class='required' id='empInfoDetError'>&#149; "+divData[1]+".</div>"+tempCode);
					$("#jeffcoSeachDiv").addClass("top5");
				    $("#jeffcoSeachDiv").removeClass("top20");
		 		    
				    //for Employee master
				    try{
						  $('[name="apiResponseFlag"]').remove();
					  }catch(e){}
				    $("#jeFFcoEmployeeInf").html($("#jeFFcoEmployeeInf").html()+"<input type='hidden' value='1' name='apiResponseFlag'/>");
				    //ended for Employee master
			  }else{				  
			var apiZipCode=$("#jeFFcoEmployeeInf .jeffCoApiPostal:nth-child(3)").text();
			  var currentZipCode=zipCode;//$("#zipCode").val();//$("#zipCodeCoverLtr").val();
			  
			  $("#empInfoDetError").remove();
				$("#jeffcoSeachDiv").addClass("top20");
			    $("#jeffcoSeachDiv").removeClass("top5");
			    
			  	$('#jeFFcoEmployeeInf').html("<p style='padding-left:25px;'>In order to change any of the pre-populated information below, please contact the Jeffco HR team.</p>"+data);
			  	
				  //for Employee master
				  try{
					  $('[name="apiResponseFlag"]').remove();
				  }catch(e){}
				  $("#jeFFcoEmployeeInf").html($("#jeFFcoEmployeeInf").html()+"<input type='hidden' value='0' name='apiResponseFlag'/>");
				  //ended for Employee master
				  
				  //if(apiZipCode==zipCode)
				  {
					  $("#jeFFcoEmployeeInf").show();
					  
					  if($("#addressLine1").val()==""){
						  $("#addressLine1").val($("#jeFFcoEmployeeInf .jeffCoApiAdd1:nth-child(3)").text());
					  }
					  if($("#addressLine2").val()==""){
						  $("#addressLine2").val($("#jeFFcoEmployeeInf .jeffCoApiAdd2:nth-child(3)").text());
					  }
					  //changeCityStateByZipForDSPQ() zipcode 
					  //getCityListByStateForDSPQ()   city
					  //alert("hello    "+$('#countryId').find('option[text="Austria"]').val());
					  if($("#countryId").val()==""){

						  try{
							  setTimeout(function(){ 							  
							  var tempCoutyText = $("#jeFFcoEmployeeInf .jeffCoApiCountry:nth-child(3)").text();
							  var tempCountyId = $("#countryId" + " option").filter(function() {  return this.text == tempCoutyText}).val();
							  $("#countryId").val(tempCountyId).attr("selected","selected");
							  getStateByCountryForDspq('dspq');
							  }, 1000);
						  } catch (e) {}
					  }
					  if($("#zipCode").val()==""){
						  $("#zipCode").val(apiZipCode);
						  try{
							  changeCityStateByZipForDSPQ();
						  } catch (e) {}
					  }
					  if($("#stateIdForDSPQ").val()==""){
						  try{
							  var tempStateText = $("#jeFFcoEmployeeInf .jeffCoApiState:nth-child(3)").text();					  
							  var stVal = $('#stateIdForDSPQ option[shortname="'+tempStateText+'"]').val();
							  setTimeout(function(){ $("#stateIdForDSPQ option[value='"+stVal+"']").attr("selected","selected");getCityListByStateForDSPQ();}, 3000);
						  } catch (e) {}
					  }
					  
					  if($("#cityIdForDSPQ").val()==""){
							try{
							  setTimeout(function(){ 
								  var tempCityText = $("#jeFFcoEmployeeInf .jeffCoApiCity:nth-child(3)").text().toUpperCase();						  
								  var tempCityId = $("#cityIdForDSPQ" + " option").filter(function() { return this.text == tempCityText}).val();						  
								  $("#cityIdForDSPQ").val(tempCityId).attr("selected","selected");
							  }, 3500);
							} catch (e) {}
					  }
					  
					
					 // $("#cityIdForDSPQ").val();
				  }
			  }
			  $('#loadingDiv_dspq_ie').hide();
			  }
		   });
	  }catch(e){alert(e)}
	}
	  $('#loadingDiv_dspq_ie').hide();
}

//End ... Start Jeffco specific code 

function JAFchkNonTeacher(isNonTeacher)
{
	if(isNonTeacher==1)
	{		
		$("#expCertTeacherTrainingDivId").show();
	}
	else
	{
		$("#expCertTeacherTrainingDivId").hide();
	}
}

function closeCompleteQuickBtn()
{
	$('#oneBtnJobApplyModelIdL2').modal('hide');
}
function showdistrictspeciFicVeteran(){
	$("#vetranOptionDiv").hide();
	var dspqjobflowdistrictId=0;
	try { dspqjobflowdistrictId=document.getElementById("dspqjobflowdistrictId").value; } catch (e) {}	
	if(dspqjobflowdistrictId==1200390 && $("#vt1").is(':checked')){/*
		$("#vetranOptionDiv").html("Please <a href='javascript:void(0);' onclick=\"window.open('https://platform.teachermatch.org/6888VeteranPreference.pdf','_blank', 'width = 1050px, height = 600px');\" download>download</a>, complete and submit this form.");
		$("#vetranOptionDiv").show();

	*/}else if(dspqjobflowdistrictId==1201470 && $("#vt1").is(':checked')){
	var districtMaster = {districtId:dwr.util.getValue("dspqjobflowdistrictId")};
	DSPQServiceAjax.getTeacherVeteran(districtMaster,
			{ 
				async: false,
				errorHandler:handleError,
				callback: function(data)
				{				
					var res = data.split("#");
					$("input[id=veteranOptS][value='"+res[0]+"']").prop("checked",true);
					$("input[id=veteranPreference1][value='"+res[1]+"']").prop("checked",true);
					$("input[id=veteranPreference2][value='"+res[2]+"']").prop("checked",true);
					$("#vetranOptionDiv").show();
				}
			});	
	
	}
}
function setDistrictSpecificVeteranValues(){
	var multiSelectOptArray="";
	var inputs="",input2="",input3="";
	inputs = $('#veteranOptS:checked').val();	
	input2 =$('#veteranPreference1:checked').val();
	input3 =$('#veteranPreference2:checked').val();
	if(input2==undefined)
		input2="";
	if(input3==undefined)
		input3="";
	 
	var othText = "Veteran Value Oscelo";
	var districtMaster = {districtId:dwr.util.getValue("dspqjobflowdistrictId")};
	var finalInputs= inputs+"#"+input2+"#"+input3;
	DSPQServiceAjax.setDistrictSpecificVeteranValues(districtMaster,finalInputs,othText,
			{ 
				async: false,
				errorHandler:handleError,
				callback: function(data)
				{}
			});	

}

function jafDownloadGeneralKnowledge(generalKnowledgeExamId, linkId)
{		
	DSPQServiceAjax.jafDownloadGeneralKnowledge(generalKnowledgeExamId,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	if(data=="")
			{
				data="javascript:void(0)";
			}
			else if(data.indexOf(".doc")!=-1)
			{
				document.getElementById("ifrmGK").src=data;
			}else
			{
				document.getElementById(linkId).href = data;
			}
			return false;
		}});
}

function downloadCustomFileUpload(answerId)
{
	DSPQServiceAjax.downloadCustomFileUpload(answerId,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
			if (data.indexOf(".doc") !=-1)
			{
				document.getElementById('downloadCustomFileUploadIFrameId').src = ""+data+"";
			}
			else
			{
				document.getElementById("jafHREFAnswerFile"+answerId).href = data; 
				return false;
			}
		}
	});
	
}

function validateCustFileName(InputCtrlFileUploadName)
{
	try{
		var iReturnValidateValue=0;
		var fileNameCustomCtrl = dwr.util.getValue(InputCtrlFileUploadName).value;
		var ext = fileNameCustomCtrl.substr(fileNameCustomCtrl.lastIndexOf('.') + 1).toLowerCase();
		var fileSize=0;
		
		if ($.browser.msie==true)
	 	{	
		    fileSize = 0;	   
		}
		else
		{		
			if(document.getElementById(InputCtrlFileUploadName).files[0]!=undefined)
				fileSize = document.getElementById(InputCtrlFileUploadName).files[0].size;
		}
		if(ext!="")
		{
			if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt'))
			{
				iReturnValidateValue=1;
			}	
			else if(fileSize>=10485760)
			{
				iReturnValidateValue=1;
			}
			else
			{
				iReturnValidateValue=0;
			}
		}
		
		
	}
	catch(err){}
	finally
	{
		return iReturnValidateValue;
	}
}
function showGridSubjectAreasGrid()
{
	DSPQServiceAjax.getSubjectAreasGrid(dp_SubjectArea_noOfRows,dp_SubjectArea_page,dp_SubjectArea_sortOrderStr,dp_SubjectArea_sortOrderType,{  
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#divGridSubjectAreas').html(data);
			applyScrollOnTbl_SubjectAreas();
		}});
}
function hideSubjectAreaForm()
{
	$('#frmSubjectArea')[0].reset();
	try { 		document.getElementById("divSubjectArea").style.display="none"; 	} catch (e) {}
	try { 		window.location.hash = '#addSubjectAreaIdDSPQ'; 	} catch (e) {}
	return false;
}
function showSubjectAreaForm()
{
	document.getElementById("divSubjectArea").style.display="block";
	$('#frmSubjectArea')[0].reset();
}
function delRow_subjectArea(id)
{	
	document.getElementById("teacherSubjectAreaExamId").value=id;
	$('#deleteSubjectArea').modal('show');
}
function deleteSubjectAreaConfirm()
{
	$('#deleteSubjectArea').modal('hide');
	var teacherSubjectAreaExamId=document.getElementById("teacherSubjectAreaExamId").value;
	DSPQServiceAjax.deleteSubjectArea(teacherSubjectAreaExamId,
	{ 
		async: false,
		errorHandler:handleError,
		callback: function(data)
		{	
			showGridSubjectAreasGrid();
		}
	});
}
function saveSubjectAreasByServlet(sbtsource,scoreReport)
{
	insertOrUpdateSubjectArea(scoreReport);
}
function insertOrUpdateSubjectArea(UploadedFileName)
{
	isSetFocus=false;
	var iReturnValidateTotalValue=0;
	var iReturnValidateValue=0;
	
	var SubjectAreaExam_Section=0;
	var SubjectAreaExam_Section_Name="tooltipSection28";
	
	if($("#divSubjectArea").is(':visible'))
	{
		var InputCtrlId_SubjectAreaId="teacherSubjectAreaExamId";
		var teacherSubjectAreaExamId=getInputCtrlTextValue(InputCtrlId_SubjectAreaId);
		if(teacherSubjectAreaExamId==null || teacherSubjectAreaExamId=="")
			teacherSubjectAreaExamId=null;
		
		var InputCtrlId_subjectAreaStatus="examStatus";
		var subjectAreaStatus=getInputCtrlTextValue(InputCtrlId_subjectAreaStatus);
		iReturnValidateValue=validateInputListControlStr(subjectAreaStatus,InputCtrlId_subjectAreaStatus,"lblFieldId_"+InputCtrlId_subjectAreaStatus,"lblFieldIdWarning_"+InputCtrlId_subjectAreaStatus);
		if(iReturnValidateValue=="1")
		{
			SubjectAreaExam_Section=SubjectAreaExam_Section+1;
			iReturnValidateTotalValue=iReturnValidateTotalValue+SubjectAreaExam_Section;
		}
		
		var InputCtrlId_examDate="examDate";
		var subjectAreaExamDate=getInputCtrlTextValue(InputCtrlId_examDate);
		iReturnValidateValue=validateInputTextControlStr(subjectAreaExamDate,InputCtrlId_examDate,"lblFieldId_"+InputCtrlId_examDate,"lblFieldIdWarning_"+InputCtrlId_examDate);
		if(iReturnValidateValue=="1")
		{
			SubjectAreaExam_Section=SubjectAreaExam_Section+1;
			iReturnValidateTotalValue=iReturnValidateTotalValue+SubjectAreaExam_Section;
		}		
		var InputCtrlId_subjectAreaExamMaster="subjectIdforDSPQ";
		var subjectAreaExamMaster=getInputCtrlTextValue(InputCtrlId_subjectAreaExamMaster);
		iReturnValidateValue=validateInputListControlStr(subjectAreaExamMaster,InputCtrlId_subjectAreaExamMaster,"lblFieldId_"+InputCtrlId_subjectAreaExamMaster,"lblFieldIdWarning_"+InputCtrlId_subjectAreaExamMaster);
		if(iReturnValidateValue=="1")
		{
			SubjectAreaExam_Section=SubjectAreaExam_Section+1;
			iReturnValidateTotalValue=iReturnValidateTotalValue+SubjectAreaExam_Section;
		}		
		
		var InputCtrlId_subjectAreaScoreReport="scoreReport";
		var subjectAreaScoreReport=getInputCtrlTextValue(InputCtrlId_subjectAreaScoreReport);
		iReturnValidateValue=validateInputFileControlStr(subjectAreaScoreReport,InputCtrlId_subjectAreaScoreReport,"lblFieldId_"+InputCtrlId_subjectAreaScoreReport,"lblFieldIdWarning_"+InputCtrlId_subjectAreaScoreReport);
		if(iReturnValidateValue=="1")
		{
			SubjectAreaExam_Section=SubjectAreaExam_Section+1;
			iReturnValidateTotalValue=iReturnValidateTotalValue+SubjectAreaExam_Section;
		}
		
		var InputCtrlId_subjectExamNote="subjectExamTextarea";
		var subjectExamNote=getTextAreaEditorCtrlTextValue(InputCtrlId_subjectExamNote);
		iReturnValidateValue=validateInputTextAreaControlStr(subjectExamNote,InputCtrlId_subjectExamNote,"lblFieldId_"+InputCtrlId_subjectExamNote,"lblFieldIdWarning_"+InputCtrlId_subjectExamNote);
		if(iReturnValidateValue=="1")
		{
			SubjectAreaExam_Section=SubjectAreaExam_Section+1;
			iReturnValidateTotalValue=iReturnValidateTotalValue+SubjectAreaExam_Section;
		}
		
		var pathOfScoreReportFile="";
		if(UploadedFileName==undefined)
		{
			var pathOfScoreReport=getInputCtrlTextValue("scoreReportHidden");
			var InputCtrlId_pathOfScoreReportFile="scoreReport";
			pathOfScoreReportFile=getInputCtrlTextValue(InputCtrlId_pathOfScoreReportFile);			
			iReturnValidateValue=validateInputFileControlStr(pathOfScoreReportFile,InputCtrlId_pathOfScoreReportFile,"lblFieldId_"+InputCtrlId_pathOfScoreReportFile,"lblFieldIdWarning_"+InputCtrlId_pathOfScoreReportFile,pathOfScoreReport);
			if(iReturnValidateValue=="1")
			{
				SubjectAreaExam_Section=SubjectAreaExam_Section+1;
				iReturnValidateTotalValue=iReturnValidateTotalValue+SubjectAreaExam_Section;
			}
		}
		if(SubjectAreaExam_Section>0)
		{
			//window.location.hash = '#lblSectionId_'+SubjectAreaExam_Section_Name;
			$("#lblSectionId_"+SubjectAreaExam_Section_Name).addClass("DSPQRequired12");
			SubjectAreaExam_Section=0;
		}
		else
		{
			$("#lblSectionId_"+SubjectAreaExam_Section_Name).removeClass("DSPQRequired12");
		}
		if(iReturnValidateTotalValue==0 && $("#divSubjectArea").is(':visible')==true)
		{
			$('#loadingDiv').show();
			if(pathOfScoreReportFile!="")
			{
				document.getElementById("frmSubjectArea").submit();
			}
			else
			{
				var subjectAreaExamMasterObj = {subjectAreaExamId:subjectAreaExamMaster};
				var teacherSubjectAreaExam =null;
				teacherSubjectAreaExam = {
						teacherSubjectAreaExamId:teacherSubjectAreaExamId,
						examStatus:subjectAreaStatus,
						examDate:new Date(subjectAreaExamDate),
						subjectAreaExamMaster:subjectAreaExamMasterObj,
						examNote:subjectExamNote
						};
				if(UploadedFileName==null || UploadedFileName=="" || UploadedFileName==undefined)
					UploadedFileName=getInputCtrlTextValue("scoreReportHidden");
				
				DSPQServiceAjax.saveOrUpdateSubjectAreaExam(teacherSubjectAreaExam,UploadedFileName,{
				async: true,
				errorHandler:handleError,
				callback:function(data)
				{
					hideSubjectAreaForm();
					showGridSubjectAreasGrid();
				}
				});
			
			}
			$('#loadingDiv').hide();
		}
		
	}
	return iReturnValidateTotalValue;	
}

function showEditSubject(id)
{
	document.getElementById("teacherSubjectAreaExamId").value=id;
	var teacherSubjectAreaExamId=document.getElementById("teacherSubjectAreaExamId").value;
	DSPQServiceAjax.showEditSubject(teacherSubjectAreaExamId,
	{ 
		async: false,
		errorHandler:handleError,
		callback: function(data)
		{
		showSubjectAreaForm();
		
		try{
			
			document.getElementById("teacherSubjectAreaExamId").value=data.teacherSubjectAreaExamId;
			
			document.getElementById("examStatus").value=data.examStatus;
			try{
				document.getElementById("examDate").value=(new Date(data.examDate).getMonth()+1)+"-"+(new Date(data.examDate).getDate())+"-"+(new Date(data.examDate).getFullYear()); 
			}catch(err){}
			try{
				document.getElementById("subjectIdforDSPQ").value=data.subjectAreaExamMaster.subjectAreaExamId;
			}catch(err){}
			try{
				document.getElementById("scoreReport").value=data.scoreReport; 
			}catch(err){}
			try{
				document.getElementById("scoreReportHidden").value=data.scoreReport;
			}catch(err){}
			try{
				$("#subjectExamTextarea").find(".jqte_editor").html(data.examNote);
				$('[name="subjectExamTextarea"]').text(data.examNote);
			}catch(err){}
			
			if(data.scoreReport!=null && data.scoreReport!="")
			{	
				document.getElementById("removeScoreReportSpan").style.display="inline";
				document.getElementById("divScoreReport").style.display="inline";
				document.getElementById("divScoreReport").innerHTML=resourceJSON.msgRecentscorereportonfile+": <a href='javascript:void(0)' id='hrefSubjectArea' onclick=\"downloadSubjectAreaExam('"+data.teacherSubjectAreaExamId+"','hrefSubjectArea');" +
				"if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0');" +
				"return false;\">"+data.scoreReport+"</a>";
				
				//$('#hrefSubjectArea').tooltip();
				
			}else{
				document.getElementById("removeScoreReportSpan").style.display="none";
				document.getElementById("divScoreReport").style.display="none";
				document.getElementById("scoreReport").value="";
			}
		}catch(err){}
		}
	});
}
function downloadSubjectAreaExam(subjectAreaExamId, linkId)
{		
	DSPQServiceAjax.downloadSubjectAreaExam(subjectAreaExamId,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
			if(data=="")
			{
				data="javascript:void(0)";
			}
			else if(data.indexOf(".doc")!=-1)
			{
				document.getElementById("ifrmRef").src=data;
			}else
			{
				document.getElementById(linkId).href = data;
			}
			return false;
		}});
}
function showHidePreScreenExplanationField(divIdCounter,hideShowFlag){
	if(!hideShowFlag)
		$('#divOpt_'+divIdCounter).hide();
	else
		$('#divOpt_'+divIdCounter).show();
}
function getMatchStatus(arrayValue,val)
{
	try {
		var isMatch=false;
		if(arrayValue!=null && arrayValue.length >0)
		{
			var iVal=parseInt(val);
			for(var i=0; i < arrayValue.length; i++)
			{
				//alert(orderingSection +" :: "+orderingSection[i] +" == "+iPrevIdResult);
				var iTempValue = parseInt(arrayValue[i]);
				if(iTempValue!="NaN" && iTempValue==iVal)
				{
					isMatch=true;
					break;
				}
			}
		}
	} catch (e) {
		alert(e);
	}
	
	return isMatch;
}