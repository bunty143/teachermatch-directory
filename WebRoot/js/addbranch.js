/* @Author: Ankit Sharma 
 * @Discription: addbranch js .
 */	
var keyContactDivVal=2;
var keyContactIdVal=null;
/*=========  Paging and Sorting ==============*/
var page = 1;
var noOfRows = 10;
var sortOrderStr="";
var sortOrderType="";

var pageNotes = 1;
var noOfRowsNotes = 10;
var sortOrderStrNotes="";
var sortOrderTypeNotes="";

var pageDist 			= 	1;
var noOfRowsDist 		= 	10;
var sortOrderStrDist	=	"";
var sortOrderTypeDist	=	"";

var domainpage = 1;
var domainnoOfRows = 10;
var domainsortOrderStr="";
var domainsortOrderType="";
function getPaging(pageno)
{	
	var gridNo	=	document.getElementById("gridNo").value;
	if(gridNo==3){
		if(pageno!='')
		{	
			domainpage=pageno;	
		}
		else
		{	
			domainpage=1;
		}
		domainnoOfRows = document.getElementById("pageSize3").value;
		getDistrictDomains();
		
	}else{
		if(pageno!='')
		{
			page=pageno;	
		}
		else
		{
			page=1;
		}
		noOfRows = document.getElementById("pageSize").value;
		displayHqBranchDistricts();
	}
}

function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
		var gridNo	=	document.getElementById("gridNo").value;
		if(gridNo==3)
		{
			if(pageno!='')
			{
				domainpage=pageno;	
			}
			else
			{
				domainpage=1;
			}
			domainsortOrderStr	=	sortOrder;
			domainsortOrderType	=	sortOrderTyp;
			if(document.getElementById("pageSize3")!=null){
				domainnoOfRows = document.getElementById("pageSize3").value;
			}else{
				domainnoOfRows=10;
			}
			getDistrictDomains();
		}else if(gridNo==1){
			if(pageno!=''){
				page=pageno;	
			}else{
				page=1;
			}
			sortOrderStr	=	sortOrder;
			sortOrderType	=	sortOrderTyp;
			if(document.getElementById("pageSize")!=null){
				noOfRows = document.getElementById("pageSize").value;
			}else{
				noOfRows=10;
			}
		
			displayHqBranchDistricts();
		}
		else
		{	
			if(pageno!=''){
				pageNotes=pageno;	
			}else{
				pageNotes=1;
			}
			sortOrderStrNotes	=	sortOrder;
			sortOrderTypeNotes	=	sortOrderTyp;
			if(document.getElementById("pageSize")!=null){
				noOfRowsNotes = document.getElementById("pageSize").value;
			}else{
				noOfRowsNotes=10;
			}
			displayNotes();
		}
}

function getSortNotesGrid()
{
	$('#gridNo').val("2")
}
function getSortDomainGrid()
{
	$('#gridNo').val("3")
}

/*========  For Handling session Time out Error ===============*/
function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert("Oops: Your Session has expired!");  document.location = 'signin.do';}
	else{alert("Server Error: "+exception.javaClassName);}
}
/*=========== For Resolving IE TextArea Maxlength Problem ================*/
function chkLenOfTextArea(textValue,maxlength)
{
	var tlen=textValue.value.length;
	if(tlen>maxlength)
	{
		document.getElementById(textValue.id).value=document.getElementById(textValue.id).value.substr(0,maxlength)
	}
}

function displayState(){
	JBServiceAjax.displayStateData({ 
		async: false,		
		callback: function(data){
			document.getElementById("stateDivId").innerHTML=data;
			 var stateId = $("#stateId").val();
				if(stateId==0)
				{
					$('#districtName').val("");
			        document.getElementById("districtName").disabled = true;
			        $('#doneDiv').hide();
			        $('#cancelDiv').show();
				}
				else
				{
					$('#cancelDiv').hide();
				    $('#doneDiv').show();
				}
		}
	});
}

//  Get city List according to state 
function selectCityByState()
{
	var stateId = $("#stateId").val();
	JBServiceAjax.selectedCityByStateId(stateId,{ 
		async: false,		
		callback: function(data){
			document.getElementById("cityId").innerHTML=data;
			document.getElementById("cityId").disabled = false;
			showRadioDistrict();
		}
	});
}
	
	/*============ Add HeadQuarterBranches Functionality ===========================*/
	function addDistict()
	{
		$("#addDistrictDiv").fadeIn();
		$("#districtName").focus();
		$('#districtName').val("");
	}
	
	function displayHqBranchDistricts()
	{
		var branchId	=	trim(document.getElementById("bId").value);
		BranchesAjax.displayHqBranchesDistrictGrid(branchId,noOfRows,page,sortOrderStr,sortOrderType,{ 
			async: false,
			callback: function(data)
			{
				$('#hqBranchesDistrict').html(data);
				applyScrollOnTbl();
				$('#districtName').val("");
				$('#addDistrict').hide();				
			},
			errorHandler:handleError 
		});
	}
	
	function saveHqBranchDistrict()
	{
		var headQuarterId  =	 trim(document.getElementById("hqId").value);
		var branchId  =	 trim(document.getElementById("bId").value);
		var districtId = trim(document.getElementById("districtId").value);
		
		if(districtName=="")
		{
				$('#errorhqbranchdiv').show();	
				$('#errorhqbranchdiv').empty();
				$('#errorhqbranchdiv').append("&#149; Please enter District Name<br>");
				$('#districtName').css("background-color", "#F5E7E1");
				$("#districtName").focus();
				return false;
		}
		if(districtId=="")
		{
				$('#errorhqbranchdiv').show();	
				$('#errorhqbranchdiv').empty();
				$('#errorhqbranchdiv').append("&#149; Please enter valid District Name<br>");
				$('#districtName').css("background-color", "#F5E7E1");
				$("#districtName").focus();
				return false;
		}
		//$('#errornotediv').empty();
		
		HeadQuarterAjax.saveHeadQuarterBranchesDistricts(headQuarterId,branchId,districtId, { 
			async: false,
			callback: function(data)
			{
				//alert(" data "+data);
				if(data	==	3)
				{
					$('#errorhqbranchdiv').empty();
					$('#errorhqbranchdiv').append("&#149; The District you enterd is already added. Please enter another District Name<br>");
					$('#districtName').focus();
					$('#errorhqbranchdiv').show();
					$('#districtName').css("background-color", "#F5E7E1");
					return false;
				}
				else
				{
					if(data	==	2)
					{
						alert(" Server Error ");
					}
					else
					{
						displayHqBranchDistricts();
						$('#districtName').val("");
						$('#districtId').val("");
					}
				}
			},
			errorHandler:handleError 
		});
	}

	function deleteHqBranchDistricts(hqbdId,distId)
	{
		$('#districtName').css("background-color", "");
		$('#errorhqbranchdiv').empty();
		if (confirm("Are you sure, you would like to delete this District?")) {
			BranchesAjax.deleteHqBranchesDistrict(hqbdId,distId, { 
				async: false,
				callback: function(data)
				{
					displayHqBranchDistricts();
					$('#districtName').val("");
				},
				errorHandler:handleError 
			});
		}
	}

	function clearHqBranchDistricts()
	{
		var rd4	 =	document.getElementsByName("selectDistrict");
		$("#addDistrictDiv").hide();
		$('#districtName').val("");
		$('#errorhqbranchdiv').empty();
	}
	

	function uncheckedOtherRadio(val)
	{  
		var rd4	 =	document.getElementsByName("selectDistrict");
		
		if(val	==	4)
		{
			$("#addSchoolLink").fadeIn();
		}
	}
	
	function getHQBDGrid()
	{
		$('#gridNo').val("1");
	}
	
	
	/*=====================================================================*/
	var count=0;
	var index=-1;
	var length=0;
	var divid='';
	var txtid='';
	var hiddenDataArray = new Array();
	var showDataArray = new Array();
	var degreeTypeArray = new Array();
	var hiddenId="";
	function getDistrictMasterAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
	{
		hiddenId=hiddenId;
		divid=txtdivid;
		txtid=txtSearch.id;
		if(event.keyCode==40){
			downArrowKey(txtdivid);
		} 
		else if(event.keyCode==38) //up key
		{
			upArrowKey(txtdivid);
		} 
		else if(event.keyCode==13) // RETURN
		{
			if(document.getElementById(divid))
				document.getElementById(divid).style.display='block';
			
			document.getElementById("districtName").focus();
			
		} 
		else if(event.keyCode==9) // Tab
		{
			
		}
		else if(txtSearch.value!='')
		{
			index=-1;
			length=0;
			document.getElementById(divid).style.display='block';
			searchArray = getDistrictMasterArray(txtSearch.value);
			fatchData(txtSearch,searchArray,txtId,txtdivid);
			
		}
		else if(txtSearch.value==""){
			document.getElementById(divid).style.display='none';
		}
	}

	function getDistrictMasterArray(districtName){
		var searchArray = new Array();
		DistrictAjax.getActiveFieldOfDistrictList(districtName,{ 
			async: false,
			callback: function(data){
			hiddenDataArray = new Array();
			showDataArray = new Array();
			for(i=0;i<data.length;i++){
				searchArray[i]=data[i].districtName;
				showDataArray[i]=data[i].districtName;
				hiddenDataArray[i]=data[i].districtId;
			}
		},
			errorHandler:handleError 
		});	

		return searchArray;
	}


	var selectFirst="";

	var fatchData= function(txtSearch,searchArray,txtId,txtdivid){
		var result = document.getElementById(txtdivid);
		try{
			result.style.display='block';
			result.innerHTML = '';
			var items='';
			count=0;
			var len=searchArray.length;
			if(document.getElementById(txtId).value!="")
			{
				
				for(var i=0;i<len;i++){
						items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
						searchArray[i].toUpperCase() + "</div>";
						count++;
						length++;
						
					if(count==10)
						break;
					
				}
				
			}
			else {
				
			}
			if(count!=0)
				result.innerHTML = items;
			else{
				result.style.display='none';
				selectFirst="";
			}
			if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
			{
				document.getElementById('divResult'+txtdivid+0).className='over';
				selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
			}
			
			scrolButtom();
		}catch (err){}
	}
	function hideDistrictMasterDiv(dis,hiddenId,divId)
	{
		if(parseInt(length)>0){
			if(index==-1){
				index=0;
			}
			if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
				document.getElementById(hiddenId).value=hiddenDataArray[index];
			}
			
			if(dis.value==""){
				document.getElementById(hiddenId).value="";
			}
			else if(showDataArray && showDataArray[index]){
				dis.value=showDataArray[index];
				
			}
			
		}else{
			if(document.getElementById(hiddenId))
				document.getElementById(hiddenId).value="";
		}
		
		if(document.getElementById(divId))
		{
			document.getElementById(divId).style.display="none";
		}
		index = -1;
		length = 0;
	}

	var downArrowKey = function(txtdivid){
		if(txtdivid)
		{
			if(index<length-1){
				for(var i=0;i<10;i++){
					{
						if(document.getElementById('divResult'+txtdivid+i))
							var div_id=document.getElementById('divResult'+txtdivid+i);
						if(div_id)
						{
							if(div_id.className=='over')
								index=div_id.id.split('divResult'+txtdivid)[1];
							div_id.className='normal';
						}
					}
				}
				index++;
				if(document.getElementById('divResult'+txtdivid+index))
				{
					var div_id=document.getElementById('divResult'+txtdivid+index);
					div_id.className='over';
					document.getElementById(txtid).value = div_id.innerHTML;
					selectFirst=div_id.innerHTML;
				}
			}
		}
	}

	var upArrowKey = function(txtdivid){
		if(txtdivid)
		{
			if(index>0){
				for(var i=0;i<length;i++){

					var div_id=document.getElementById('divResult'+txtdivid+i);
					if(div_id)
					{
						if(div_id.className=='over')
							index=div_id.id.split('divResult'+txtdivid)[1];
						div_id.className='normal';
					}
				}
				index--;
				if(document.getElementById('divResult'+txtdivid+index))
					document.getElementById('divResult'+txtdivid+index).className='over';
				if(txtid && document.getElementById('divResult'+txtdivid+index))
				{
					document.getElementById(txtid).value=
						document.getElementById('divResult'+txtdivid+index).innerHTML;
					selectFirst=document.getElementById('divResult'+txtdivid+index).innerHTML;
				}
			}
		}
	}

	var overText=function (div_value,txtdivid) {

		for(var i=0;i<length;i++)
		{
			if(document.getElementById('divResult'+i))
			{
				var div_id=document.getElementById('divResult'+i);

				if(div_id.className=='over')
					index=div_id.id.split(txtdivid)[1];
				div_id.className='normal';
			}
		}
		div_value.className = 'over';
		document.getElementById(txtid).value= div_value.innerHTML;
	}
	function trim(s)
	{
		while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
			s = s.substring(1,s.length);
		}
		while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
			s = s.substring(0,s.length-1);
		}
		return s;
	}
	function mouseOverChk(txtdivid,txtboxId)
	{	
		for(var i=0;i<length;i++)
		{
			$('#divResult'+txtdivid+i).mouseover({param1: i,param2:txtdivid, param3:txtboxId}, fireMouseOverEvent);
		}
	}


	function fireMouseOverEvent(event)
	{
		for(var i=0;i<length;i++)
		{	
			document.getElementById('divResult'+event.data.param2+i).className='normal';
		}
	    document.getElementById('divResult'+event.data.param2+event.data.param1).className='over';	       
	   	document.getElementById(event.data.param3).value= $('#divResult'+event.data.param2+event.data.param1).text();
	    index=event.data.param1;
	}

	//Search Records By EntityType

	function DisplayHideSearchBox(){
		var entity_type	=	document.getElementById("MU_EntityType").value; 
		if(entity_type ==2)
		{
			$("#Searchbox").fadeIn();
			$('#SearchTextboxDiv').fadeIn();
			$('#districtName').focus();
		}else{
			document.getElementById("districtName").value="";
			$("#Searchbox").fadeIn();
			$("#SearchTextboxDiv").hide();
		}
	}

	function onLoadDisplayHideSearchBox(entity_type){
		
		if(entity_type ==2){
			$("#Searchbox").hide();
			$('#SearchTextboxDiv').hide();
			document.getElementById("MU_EntityType").value=2;
		}
		else if(entity_type ==5) // for headquarter
		{
			$("#Searchbox").hide();
			$('#SearchTextboxDiv').hide();
			document.getElementById("MU_EntityType").value=5;
		}
		else if(entity_type ==1){
			$("#Searchbox").fadeIn();
			$('#SearchTextboxDiv').fadeIn();
			document.getElementById("districtName").focus();
			document.getElementById("MU_EntityType").value=2;
		}else{
			$("#Searchbox").fadeIn();
			$('#SearchTextboxDiv').hide();
		}
		ShowDistrict();
	}

	/*========  Show District ===============*/
	function searchDistrict()
	{
		page = 1;
		ShowDistrict();
	}
	function ShowDistrict()
	{
		$('#loadingDiv').fadeIn();
		var branchExist = $('#branchExist').val();
		var branchId = "";
		var status = "";
		var entityID	=	document.getElementById("MU_EntityType").value;
		var searchText	="";
		try{
			searchText=document.getElementById("districtName").value;
		}catch(err){
			searchText="";
		}

			branchId = $('#branchId').val();
			
			status = "All";
				delay(1000);
				DistrictAjax.SearchDistrictRecords(entityID,searchText,noOfRows,page,sortOrderStr,sortOrderType,branchId,status,{ 
					async: true,
					callback: function(data)
					{
					$('#divMain').html(data);
					applyScrollOnTbl();
					$('#loadingDiv').hide();
					},
					errorHandler:handleError 
				});
	}
	function delay(sec){
		var starttime = new Date().getTime();
		starttime = starttime+sec;
		while(true){
			if(starttime< new Date().getTime()){
				break;
			}
		}
	}

	function activateDeactivateDistrict(district,status)
	{
		document.getElementById("actdist").value=district;
		document.getElementById("diststat").value=status;
		if(status=="I")
		{
			$('#myModalactMsgShow').modal('show');
		}
		
		else
		{
		toggleStatus();	
		}
	}

	function toggleStatus()
	{
		var district=document.getElementById("actdist").value;
		var status=document.getElementById("diststat").value;
		DistrictAjax.activateDeactivateDistrict(district,status, { 
			async: true,
			callback: function(data)
			{
			
				
			
				ShowDistrict();
			},
			errorHandler:handleError });	
	}




	//===========================================================
	 // for branch auto search

	function getBranchMasterAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
	{	
		//alert("01");
		hiddenId=hiddenId;
		divid=txtdivid;
		txtid=txtSearch.id;
		if(event.keyCode==40){
			downArrowKey(txtdivid);
		} 
		else if(event.keyCode==38)
		{
			upArrowKey(txtdivid);
		} 
		else if(event.keyCode==13)
		{
			if(document.getElementById(divid))
				document.getElementById(divid).style.display='block';
			document.getElementById("branchName").focus();
		} 
		else if(event.keyCode==9)
		{
			
		}
		else if(txtSearch.value!='')
		{
			index=-1;
			length=0;
			document.getElementById(divid).style.display='block';
			//alert("02");
			searchArray = getBranchArray(txtSearch.value);
			fatchData(txtSearch,searchArray,txtId,txtdivid);
		}
		else if(txtSearch.value==""){
			document.getElementById(divid).style.display='none';
		}
	}

	function getBranchArray(branchName){
		
		//alert("03");
		var searchArray = new Array();
		BranchesAjax.getFieldOfBranchList(branchName,{  
			async: false,		
			callback: function(data){
			hiddenDataArray = new Array();
			showDataArray = new Array();
			for(i=0;i<data.length;i++){
				searchArray[i]=data[i].branchName;
				hiddenDataArray[i]=data[i].branchId;
				showDataArray[i]=data[i].branchName;
			}
		}
		});	

		return searchArray;
	}

	function hideBranchMasterDiv(dis,hiddenId,divId)
	{
		if(parseInt(length)>0){
			if(index==-1){
				index=0;
			}
				document.getElementById(hiddenId).value=hiddenDataArray[index];
			
			if(dis.value==""){
				document.getElementById(hiddenId).value="";
			}
			else if(showDataArray && showDataArray[index]){
				dis.value=showDataArray[index];
				
			}
			
		}else{
			if(document.getElementById(hiddenId))
				document.getElementById(hiddenId).value="";
			if(dis.value!="")
			{
				var cnt=0;
				var focs=0;	
				$('#errordiv').empty();	
				$('#errordiv').append("&#149; Please enter valid Branch<br>");
			}
		}
		if(document.getElementById(divId))
		{
			document.getElementById(divId).style.display="none";
		}
		//getDistrictWiseSubject(document.getElementById(hiddenId).value);
		index = -1;
		length = 0;
	}

	var selectFirst="";

	function addDistrict()
	{
		var headQuarterId = trim(document.getElementById("hqId").value);
		var districtId = trim(document.getElementById("districtId").value);
		if(districtName=="")
		{
				$('#errordiv').show();	
				$('#errordiv').empty();
				$('#errordiv').append("&#149; Please enter District Name<br>");
				$('#districtName').css("background-color", "#F5E7E1");
				$("#districtName").focus();
				return false;
		}
		if(districtId=="")
		{
				$('#errordiv').show();	
				$('#errordiv').empty();
				$('#errordiv').append("&#149; Please enter valid District Name<br>");
				$('#districtName').css("background-color", "#F5E7E1");
				$("#districtName").focus();
				return false;
		}
		$("#loadingDiv").fadeIn();
		HeadQuarterAjax.saveDistrict(headQuarterId, districtId,{ 
			async: false,
			callback: function(data)
			{
			$("#loadingDiv").hide();
				if(data	==	"3")
				{
					$('#errordiv').empty();
					$('#errordiv').append("&#149; The District you enterd is already added. Please enter another District Name<br>");
					$('#errordiv').focus();
					$('#errordiv').show();
					$('#districtName').css("background-color", "#F5E7E1");
					return false;
				}
				else
				{
					showTemp();		
					$("#districtName").val("");
				}
			},
			errorHandler:handleError 
	}); 
	}
	
	
	function showTemp()
	{	
		HeadQuarterAjax.showTempDistricts(noOfRows,page,sortOrderStr,sortOrderType,{ 
			async: false,
			callback: function(data)
			{
				$("#hqDistrict").html(data);	
				applyScrollOnTbl();
			},
			errorHandler:handleError 
	});
	}
	
	function deleteTemp()
	{
		HeadQuarterAjax.deleteTemp({ 
				async: false,
				callback: function(data)
				{},
				
		}); 
	}
	
	function removeTempDistricts(districtId)
	{
		HeadQuarterAjax.deleteTempDistrict(districtId,{ 
			async: false,
			callback: function(data)
			{
				showTemp();			
			},
			errorHandler:handleError 
	}); 
	}
	
	function validateAddBranch()
    {
          var headQuarterId = trim(document.getElementById("hqId").value);
          var branchCode = trim(document.getElementById("branchCode").value);
          var branchEmail = trim(document.getElementById("branchEmail").value);
          var bbName = trim(document.getElementById("bbName").value);
          var timezone = trim(document.getElementById("timezone").value);
          var bdName = trim(document.getElementById("bdName").value);
          var contactNumber1 = trim(document.getElementById("contactNumber1").value);
          var branchAdress1 = trim(document.getElementById("branchAdress1").value);
          var contactNumber2 = trim(document.getElementById("contactNumber2").value);
          var branchAdress2 = trim(document.getElementById("branchAdress2").value);           
          var stateId = trim(document.getElementById("stateId").value);           
          var cityId = trim($("#cityId option:selected").val());
          var zip = trim(document.getElementById("zip").value);
          var phoneNumber = trim(document.getElementById("phoneNumber").value);
          $('#errordiv').empty();
          resetColour();
          var count=0;
          if(branchCode=="")
          {
                      $('#errordiv').show();  
                      //$('#errordiv').empty();
                      $('#errordiv').append("&#149; Please enter Branch Code<br>");
                      $('#branchCode').css("background-color", "#F5E7E1");
                      $("#branchCode").focus();
                      count++;
          }
          if(branchEmail=="")
          {                
                      $('#errordiv').show();  
                      //$('#errordiv').empty();
                      $('#errordiv').append("&#149; Please enter Branch Email<br>");
                      $('#branchEmail').css("background-color", "#F5E7E1");
                      
                      if(count==0)
                      $("#branchEmail").focus();
                      count++;
          }
          
          if(branchEmail!="")
          {
                $('#errordiv').show();  
                //$('#errordiv').empty();
                
                
                if(!isEmailAddress(branchEmail))
                {
                      $('#errordiv').append("&#149; Please enter valid Email Address<br>");
                      $('#branchEmail').css("background-color", "#F5E7E1");
                      if(count==0)
                            $("#branchEmail").focus();
                            count++;
                }     
          }     
          if(bbName=="")
          {
                      $('#errordiv').show();  
                      //$('#errordiv').empty();
                      $('#errordiv').append("&#149; Please enter Branch Business Name<br>");
                      $('#bbName').css("background-color", "#F5E7E1");
                      if(count==0)
                      $("#bbName").focus();
                      count++;
          }
          if(timezone=="0")
          {
                      $('#errordiv').show();  
                      //$('#errordiv').empty();
                      $('#errordiv').append("&#149; Please select Time Zone<br>");
                      $('#timezone').css("background-color", "#F5E7E1");
                      if(count==0)
                      $("#bbtimezoneName").focus();
                      count++;
          }
          if(bdName=="")
          {
                      $('#errordiv').show();  
                      //$('#errordiv').empty();
                      $('#errordiv').append("&#149; Please enter Branch Display Name<br>");
                      $('#bdName').css("background-color", "#F5E7E1");
                      if(count==0)
                      $("#bdName").focus();
                      count++;
          }
          /*if(contactNumber1=="")
          {
                      $('#errordiv').show();  
                      //$('#errordiv').empty();
                      $('#errordiv').append("&#149; Please enter Branch Contact<br>");
                      $('#contactNumber1').css("background-color", "#F5E7E1");
                      if(count==0)
                      $("#contactNumber1").focus();
                      count++;
          }*/
          if(branchAdress1=="")
          {
                      $('#errordiv').show();  
                      //$('#errordiv').empty();
                      $('#errordiv').append("&#149; Please enter Branch Address <br>");
                      $('#branchAdress1').css("background-color", "#F5E7E1");
                      if(count==0)
                      $("#branchAdress1").focus();
                      count++;
          }
          /*if(branchContact2=="")
          {
                      $('#errordiv').show();  
                      //$('#errordiv').empty();
                      $('#errordiv').append("&#149; Please enter Branch Contact<br>");
                      $('#branchContact2').css("background-color", "#F5E7E1");
                      if(count==0)
                      $("#branchContact2").focus();
                      count++;
          }*/
          /*if(branchAdress2=="")
          {
                      $('#errordiv').show();  
                      //$('#errordiv').empty();
                      $('#errordiv').append("&#149; Please enter Adress 2<br>");
                      $('#branchAdress2').css("background-color", "#F5E7E1");
                      if(count==0)
                      $("#branchAdress2").focus();
                      count++;
          }*/
          if(stateId=="0")
          {
                      $('#errordiv').show();  
                      //$('#errordiv').empty();
                      $('#errordiv').append("&#149; Please select State<br>");
                      $('#stateId').css("background-color", "#F5E7E1");
                      if(count==0)
                      $("#stateId").focus();
                      count++;
          }
          if(cityId=="0")
          {
                      $('#errordiv').show();  
                      //$('#errordiv').empty();
                      $('#errordiv').append("&#149; Please select City<br>");
                      $('#cityId').css("background-color", "#F5E7E1");
                      if(count==0)
                      $("#cityId").focus();
                      count++;
          }
          if(zip=="")
          {
                      $('#errordiv').show();  
                      //$('#errordiv').empty();
                      $('#errordiv').append("&#149; Please enter Zip<br>");
                      $('#zip').css("background-color", "#F5E7E1");
                      if(count==0)
                      $("#zip").focus();
                      count++;
          }
          if(phoneNumber=="")
          {
                      $('#errordiv').show();  
                      //$('#errordiv').empty();
                      $('#errordiv').append("&#149; Please enter Phone Number<br>");
                      $('#phoneNumber').css("background-color", "#F5E7E1");
                      
                      if(count==0)
                      $("#phoneNumber").focus();
                      count++;
          }
          if(phoneNumber!="")
          {
                      $('#errordiv').show();  
                      if(isNaN(phoneNumber))
                      {
                            $('#errordiv').append("&#149; Please enter valid Phone Number<br>");
                            $('#phoneNumber').css("background-color", "#F5E7E1");
                            
                            if(count==0)
                            $("#phoneNumber").focus();
                            count++;
                      }     
                      if(phoneNumber.length!=10)
                      {
                            $('#errordiv').append("&#149; Please enter 10 digit Phone Number<br>");
                            $('#phoneNumber').css("background-color", "#F5E7E1");
                            
                            if(count==0)
                            $("#phoneNumber").focus();
                            count++;
                      }                   
          }
          if(count!=0)
          return false;
          
          var selectDistrict = trim(document.getElementById("selectDistrict").value);
         try{ 
          HeadQuarterAjax.saveHeadQuarterBranch(headQuarterId,branchCode,branchEmail,bbName,bdName,branchAdress1,branchAdress2,stateId,
                      cityId,zip,phoneNumber,timezone,contactNumber1,contactNumber2,{ 
                async: false,
                callback: function(data)
                {
                	$("#loadingDiv").hide();                         
                      if(data ==  "managebranches")
                      {
                    	  $("#modalSuccessMsg").modal("show");
                      }
                      else if(data =="duplicate")
                      { 
                    	  alert(data);
                    	  $('#errordiv').show();  
                    	  $('#errordiv').append("&#149; Please enter unique Branch Code & Branch Business Name<br>");
                      }
                },
                errorHandler:handleError 
          	}); 
         }
         catch(e){}
          
    }
    
function resetColour()
{

    $('#branchCode').css("background-color", "");
    $('#branchEmail').css("background-color", "");
    $('#bbName').css("background-color", "");
    $('#timezone').css("background-color", "");
    $('#bdName').css("background-color", "");
    $('#contactNumber2').css("background-color", "");
    $('#branchAdress1').css("background-color", "");
    $('#branchAdress2').css("background-color", "");
    $('#contactNumber2').css("background-color", "");
    $('#contactNumber2').css("background-color", "");
    $('#stateId').css("background-color", "");
    $('#cityId').css("background-color", "");
    $('#zip').css("background-color", "");
    $('#phoneNumber').css("background-color", "");
}

function isEmailAddress(str) 
{
    var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
    return emailPattern.test(str);
}

function successRedirect()
{
	window.location="managebranches.do";
}
function showRadioDistrict()
{ 
  var stateId = $("#stateId").val();
	if(stateId>0)
	{
		$('#cancelDiv').hide();
		$('#doneDiv').show();
	    document.getElementById("districtName").disabled = false;
	}
	else
	{
		 $('#doneDiv').hide();
		 $('#cancelDiv').show();
		 $('#districtName').val("");
		 document.getElementById("districtName").disabled = true;
	}
}
function getStateDistrictMasterAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	var stateId = $("#stateId").val();
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("districtName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getStateDistrictMasterArray(txtSearch.value,stateId);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getStateDistrictMasterArray(districtName,stateId){
	var searchArray = new Array();
	DistrictAjax.getActiveDistrictListByState(districtName,stateId,{ 
		async: false,
		callback: function(data){
		hiddenDataArray = new Array();
		showDataArray = new Array();
		for(i=0;i<data.length;i++){
			searchArray[i]=data[i].districtName;
			showDataArray[i]=data[i].districtName;
			hiddenDataArray[i]=data[i].districtId;
		}
	},
		errorHandler:handleError 
	});	

	return searchArray;
}