
/*========  For Handling session Time out Error ===============*/
function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert( resourceJSON.msgServerErr+": "+exception.javaClassName);}
}
function getDistrictQuestionsSet(jobId)
{
	var isAffilated=0;
	try{
		if(document.getElementById("ok_cancelflag").value==1)
		{	try{
				document.getElementById("isAffilated").checked=true;
			}catch(err){}
			isAffilated=1;
		}
	}catch(err){}
	
	try{
		if(document.getElementById("isAffilated").checked){
			isAffilated=1;
		}
	}catch(err){}
	AssessmentCampaignAjax.getDistrictSpecificQuestion(jobId,isAffilated,{
	//AssessmentCampaignAjax.getDistrictSpecificQuestionBySet(jobId,isAffilated,{
		async: false,
		errorHandler:handleError,
		callback: function(data)
		{
		if($("#isPrinciplePhiladelphia").length>0 && $("#isPrinciplePhiladelphia").val()=="1"){
			data=null;
			saveDistrictSpecificQuestion();
			return;
		}	
		
		if(data!=null)
		{
			try
			{
				var dataArray = data.split("@##@");
				totalQuestions = dataArray[1];
				var textmsg = dataArray[2];
				if(textmsg!=null && textmsg!="" && textmsg!='null')
					$('#textForDistrictSpecificQuestions').html(textmsg);
				
				//alert(" totalQuestions "+totalQuestions+" \n textmsg : "+textmsg);				
				if(totalQuestions==0)
				{
					saveDistrictSpecificQuestion();
					return;
				}else
				{
					$('#tblGrid').html(dataArray[0]);
					$('#myModalv').modal('hide');
					$('#loadingDiv_dspq_ie').hide();
					$('#myModalDASpecificQuestions').modal('show');
				}
				
			}catch(err)
			{}
		}

		}
	});	
}

function cancelDSQdiv()
{
	$('#myModalv').modal('hide');
	var txtDistrictPortfolioConfig=document.getElementById("txtDistrictPortfolioConfig").value
	if(txtDistrictPortfolioConfig > 0)
		validatePortfolioErrorMessageAndGridData('level1');
}

function setDistrictQuestions(fromWhere)
{
	$('#errordiv4question').empty();
	var errorDivId="errordiv4question";
	var arr =[];
	var jobOrder = {jobId:document.getElementById("jobId").value};
	for(i=1;i<=totalQuestions;i++)
	{
		var districtSpecificQuestion = {questionId:dwr.util.getValue("Q"+i+"questionId")};
		var questionTypeShortName = dwr.util.getValue("Q"+i+"questionTypeShortName");
		var questionTypeMaster = {questionTypeId:dwr.util.getValue("Q"+i+"questionTypeId")};
		var qType = dwr.util.getValue("Q"+i+"questionTypeShortName");
		var questionSets = {ID:dwr.util.getValue("qqQuestionSetID")};
		if(qType=='tf' || qType=='slsel')
		{
			var optId="";

			if($("input[name=Q"+i+"opt]:radio:checked").length > 0 )
			{
				optId=$("input[name=Q"+i+"opt]:radio:checked").val();
			}else
			{
				$("#errordiv4question").html("&#149; "+resourceJSON.msgProvideResToAllQ+"<br>");
				navigateErroOnTopPosition(errorDivId);
				return;
			}
			
			arr.push({ 
				"selectedOptions"  : optId,
				"question"  : dwr.util.getValue("Q"+i+"question"),
				"questionTypeMaster" : questionTypeMaster,
				"questionType" : questionTypeShortName,
				"questionOption" : dwr.util.getValue("qOpt"+optId),
				"districtSpecificQuestions" : districtSpecificQuestion,
				"questionSets"	: questionSets,
				"isValidAnswer" : dwr.util.getValue("Q"+optId+"validQuestion"),
				"jobOrder" : jobOrder,
			});
		}else if(qType=='mlsel')
		{
			var optId="";
			var optVal="";
			var flagValidAns='true';
			var selectedAns = document.getElementsByName("Q"+i+"opt");
			if(selectedAns.length > 0 )
			{
				if(selectedAns[0].checked)
				{
					optId = selectedAns[0].value;
					optVal = dwr.util.getValue("qOpt"+optId);
					
					if(dwr.util.getValue("Q"+optId+"validQuestion")=='false')
						flagValidAns='false';
				}
				
				for(var j=1;j<selectedAns.length;j++){
					if(selectedAns[j].checked){
						optId=optId+","+selectedAns[j].value;
						optVal = optVal+","+dwr.util.getValue("qOpt"+selectedAns[j].value);
						
						if(dwr.util.getValue("Q"+selectedAns[j].value+"validQuestion")=='false')
							flagValidAns='false';
					}
				}
				
			}else
			{
				$("#errordiv4question").html("&#149; "+resourceJSON.msgProvideResToAllQ+"<br>");
				navigateErroOnTopPosition(errorDivId);
				return;
			}
			
			arr.push({
				"selectedoptionmlsel" :optId,
				"selectedOptions"  : null,
				"question"  : dwr.util.getValue("Q"+i+"question"),
				"questionTypeMaster" : questionTypeMaster,
				"questionType" : questionTypeShortName,
				"questionOption" : optVal,
				"districtSpecificQuestions" : districtSpecificQuestion,
				"questionSets"	: questionSets,
				"isValidAnswer" : flagValidAns,
				"jobOrder" : jobOrder,
			});
		}
		else if(qType=='ml')
		{
			var insertedText = dwr.util.getValue("Q"+i+"opt");
			if(insertedText!=null && insertedText!="")
			{
				arr.push({ 
					"insertedText"    : insertedText,
					"question"  : dwr.util.getValue("Q"+i+"question"),
					"questionTypeMaster" : questionTypeMaster,
					"questionType" : questionTypeShortName,
					"districtSpecificQuestions" : districtSpecificQuestion,
					"questionSets"	: questionSets,
					"jobOrder" : jobOrder
				});
			}else
			{
				$("#errordiv4question").html("&#149; "+resourceJSON.msgProvideResToAllQ+"<br>");
				navigateErroOnTopPosition(errorDivId);
				return;
			}
		}else if(qType=='et')
		{
			var optId="";

			if($("input[name=Q"+i+"opt]:radio:checked").length > 0 )
			{
				optId=$("input[name=Q"+i+"opt]:radio:checked").val();
			}else
			{
				$("#errordiv4question").html("&#149; "+resourceJSON.msgProvideResToAllQ+"<br>");
				navigateErroOnTopPosition(errorDivId);
				return;
			}
			
			var insertedText = dwr.util.getValue("Q"+i+"optet");
			var isValidAnswer = dwr.util.getValue("Q"+optId+"validQuestion");
			var requiredExplanation=document.getElementById("Q"+optId+"requiredExplanation").value;
			//if(isValidAnswer=="true" && insertedText.trim()=="")
			if(requiredExplanation.trim()=="true" && insertedText.trim()=="")
			{
				$("#errordiv4question").html("&#149; "+resourceJSON.msgProvideResToAllQ+"<br>");
				navigateErroOnTopPosition(errorDivId);
				return;
			}
				
			arr.push({ 
				"selectedOptions"  : optId,
				"question"  : dwr.util.getValue("Q"+i+"question"),
				"insertedText"    : insertedText,
				"questionTypeMaster" : questionTypeMaster,
				"questionType" : questionTypeShortName,
				"questionOption" : dwr.util.getValue("qOpt"+optId),
				"districtSpecificQuestions" : districtSpecificQuestion,
				"questionSets"	: questionSets,
				"isValidAnswer" : isValidAnswer,
				"jobOrder" : jobOrder,
			});
		}
	}
	if(arr.length==totalQuestions)
	{
		AssessmentCampaignAjax.setDistrictQuestionsBySet(arr,questionSets,{
		//AssessmentCampaignAjax.setDistrictQuestions(arr,{ 
				async: false,
				errorHandler:handleError,
				callback: function(data){
				if(data!=null)
				{
					if(fromWhere=='apply')
					{
						document.getElementById("frmApplyJob").submit();					
						if($('#isresetStatus').val()==1)
						{
							resetjftIsAffilatedAfterDSQ();
						}
					}
					else
					{
						saveDistrictSpecificQuestion();
					}
					arr =[];
				}
			}
		});	
	}else
	{
		$("#errordiv4question").html("&#149; "+resourceJSON.msgProvideResToAllQ+"<br>");
		navigateErroOnTopPosition(errorDivId);
		return;
	}
}

function saveDistrictSpecificQuestion()
{
	var isaffilatedstatus=document.getElementById("isaffilatedstatushiddenId").value;
	var jobId = document.getElementById("jobId").value;
	if(isaffilatedstatus==1)
	{
		try
		{
			try{$('#myModalv').modal('hide');}catch(err){}
			$('#myModalDASpecificQuestions').modal('hide');
			//$('#epiIncompAlert').modal('show');
		}catch(err)
		  {}
	}
	AssessmentCampaignAjax.saveJobForTeacher(jobId,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){
		if(data==1)
		{
			
			try
			{
				$('#myModalDASpecificQuestions').modal('hide');
				//try{$('#myModalv').modal('show');}catch(err){} show after Job will apply 
			}catch(err)
			{}
			
				// Start ... Dynamic portfolio for external job
				var completeNow = document.getElementById("completeNow").value;
				if(completeNow==1)
				{
					continueCompleteNow();
				}else
				{
					getTeacherJobDone(jobId);
					//$('#redirectToDashboard').modal('show');
					$('#myModalCL').modal('hide');
				}
				// End ... Dynamic portfolio for external job
			}
	}
	});	
	
}

/**
 * 
 * @param divId
 * @return
 * @author Mukesh
 * 
 */
function navigateErroOnTopPosition(divId)
{
	try 
	{ document.getElementById(divId).scrollIntoView(); } catch (e)	{ }
}