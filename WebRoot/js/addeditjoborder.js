var certificationDivCount=0;
var schoolDivCount=0;
var schoolDivCountCheck=0;
var schoolDivVal="";
var globalJobCategoryName=null;
var jeffcoSchoolId =null;
var specialEdFlag = null;
var schoolIdCount ="";


var arrNewRequisition=[];

/* @Author: Gagan 
 * @Discription: view of edit domain js.
*/
/*========  For Handling session Time out Error ===============*/
function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert(resourceJSON.msgServerErr+": "+exception.javaClassName);}
}
/*======= Save AddEditJobOrder on Press Enter Key ========= */
function chkForEnterAddEditJobOrder(evt)
{
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if((charCode==13) && (evt.srcElement.className!='jqte_editor'))
	{
		validateAddEditJobOrder();
	}	
}
/*=========== For Resolving IE TextArea Maxlength Problem ================*/
function chkLenOfTextArea(textValue,maxlength)
{
	var tlen=textValue.value.length;
	if(tlen>maxlength)
	{
		document.getElementById(textValue.id).value=document.getElementById(textValue.id).value.substr(0,maxlength)
	}
}

function checkJobStartDate()
{
	if($("#jobId").val()==""){
		var month = new Array();
		month[0] = "January";
		month[1] = "February";
		month[2] = "March";
		month[3] = "April";
		month[4] = "May";
		month[5] = "June";
		month[6] = "July";
		month[7] = "August";
		month[8] = "September";
		month[9] = "October";
		month[10] = "November";
		month[11] = "December";
		$('#errordiv').empty();
		$('#jobStartDate').css("background-color", "");
		var jobStartDate	=	new Date(trim(document.getElementById("jobStartDate").value));
		if(trim(document.getElementById("jobStartDate").value)==""){
			$('#errordiv').show();
			$('#errordiv').append("&#149; "+resourceJSON.msgPostingStartDate+"");
			$('#jobStartDate').focus();
			$('#jobStartDate').css("background-color", "#F5E7E1");
			$("#jsiDiv").hide();
		}else{
			$("#jsiDiv").fadeIn();
			var twoDaysAgoDate  =	new Date();
			twoDaysAgoDate = new Date(twoDaysAgoDate.setDate(twoDaysAgoDate.getDate()+2));
			var diffDays =  twoDaysAgoDate.getTime()-jobStartDate.getTime();
			diffDays=parseInt(diffDays / (1000 * 60 * 60 * 24));
			if(!(diffDays<=0)){
				$("#jsiDiv").hide();
				document.getElementById("textJsiDateMsg").innerHTML=resourceJSON.msgJobStratDateless+" "+month[twoDaysAgoDate.getMonth()]+" "+twoDaysAgoDate.getDate()+", "+twoDaysAgoDate.getFullYear()+""
				$("#textJsiDate").modal("show");
				$('#jobStartDate').focus();
				$('#jobStartDate').css("background-color", "#F5E7E1");
			}else{
				$("#jsiDiv").fadeIn();
			}
		}	
	}
}
/*======== Redirect to Manage User Page ===============*/
function cancelJobOrder()
{
	var JobOrderType=document.getElementById("JobOrderType").value;
	if(JobOrderType==2){
		window.location.href="managejoborders.do";
	}else if(JobOrderType==3){
		window.location.href="schooljoborders.do";
	}
}
function deleteSchoolInJobOrder(jobId,schoolId)
{
	if (confirm(resourceJSON.msgLiekToDeleteSchool)) {
		if(jobId=='')jobId=0;
		if(schoolId=='')schoolId=0;
		ManageJobOrdersAjax.deleteSchoolInJobOrder(jobId,schoolId, {
			async: true,
			callback: function(data)
			{
				if(data)
				{   displayRequisationNumber();
					ShowSchool();
					getRequistionNoByDID();
					cancelSchool();
				}
				else
				{
					var divMsg="<div>"+resourceJSON.msgdeletethisSchool+"</div>";
					document.getElementById("Msg").innerHTML=divMsg;
					$('#closeBtn').hide();
					$('#myModalMsg').modal('show');
				}
			},
			errorHandler:handleError 
		});
	}
}

function deleteSchoolInJobOrderTemp(schoolId)
{
	if (confirm(resourceJSON.msgLiekToDeleteSchool)) {
		if(schoolId=='')schoolId=0;
		ManageJobOrdersAjax.deleteSchoolInJobOrderTemp(schoolId, { 
			async: true,
			callback: function(data)
			{
				if(data)
				{
					ShowSchoolTemp();
					getRequistionNoByDIDTemp();
					cancelSchool();
				}
			},
			errorHandler:handleError 
		});
	}
}

function deleteSchoolInJobOrderTemp_NoReq(schoolId)
{
	if (confirm(resourceJSON.msgLiekToDeleteSchool)) {
		if(schoolId=='')schoolId=0;
		ManageJobOrdersAjax.deleteSchoolInJobOrderTemp(schoolId, { 
			async: true,
			callback: function(data)
			{
				if(data)
				{
					showSchoolTemp_NoReq();	
				}
			},
			errorHandler:handleError 
		});
	}
}

function getRequistionNoByDIDTemp(){
	var districtOrSchooHiddenlId=$("#districtOrSchooHiddenlId").val();
	ManageJobOrdersAjax.getRequistionNoByDIDTemp(districtOrSchooHiddenlId,{ 
		async: true,
		callback: function(data)
		{
			$('#avlbListSchool').html(data);
		},
		errorHandler:handleError 
	});
}

function deleteCertification(jobCertificationId)
{
	if (confirm(resourceJSON.msgDeleteCertf)) {
		if(jobCertificationId=='')jobCertificationId=0;
		ManageJobOrdersAjax.deleteCertification(jobCertificationId, { 
			async: true,
			callback: function(data)
			{
				DisplayCertification();
			},
			errorHandler:handleError 
		});
	}
}
function removeAssessment()
{
	var jobId	=	document.getElementById("jobId").value;
	var JobOrderType=document.getElementById("JobOrderType").value;
	if(window.confirm(resourceJSON.msgRemoveInventry))
	{
		document.getElementById("frmJobAssessmentUpload").reset();
		ManageJobOrdersAjax.deleteAssessment(jobId,JobOrderType,{ 
			async: true,
			callback: function(data)
		{
			document.getElementById("divAssessmentDocument").style.display='none';
		},
		errorHandler:handleError 
		});
	}
	return false;
}
function clearAssessment()
{
	if(window.confirm(resourceJSON.msgclearinventory))
	{
		$('#errordiv').empty();
		document.getElementById("frmJobAssessmentUpload").reset();
	}
	return false;
}
function disableJobCategory()
{
	var jobId	="";
	
	if(document.getElementById("jobId").value!=''){
		jobId=document.getElementById("jobId").value;
	}else if(document.getElementById("clonejobId").value){
		jobId=document.getElementById("clonejobId").value;
	}
	if(jobId=="" && $('#entityType').val()==1)
	{
		$('#jobCategoryId').prop('disabled', true);
	}
	
	/*===== Gagan : For Edit mode check display JSI radio btn or Not ======= */ 
	if(jobId!=0)
	{
		var jobCategoryIdvalue=$("#jobCategoryId").val();
		var n=jobCategoryIdvalue.indexOf("||");
		var jobCategoryVal=jobCategoryIdvalue.split("||");
		
		if(n!=-1 && jobCategoryVal[2]!='')
		{
			$('#jcjsidiv').show();
			$("#ViewJSIURL").fadeIn();
		}
	}
}

function getJobCategoryByDistrictId()
{
	var jobId	=	$('#jobId').val();
	var districtRequisitionId	=	$('#districtRequisitionId').val();
	var requisitionNumber	=	$('#requisitionNumber').val();
	if(jobId=="" && $('#entityType').val()==1)
	{
		if($('#distHiddenId').val()!=$('#districtOrSchooHiddenlId').val())
		{
			var jobCategoryId	=	document.getElementById("jobCategoryId").value;
			if( $('#distHiddenId').val()!=0 && jobCategoryId!=0)
			{
				var divMsg="<div>"+resourceJSON.msgJobCategoryName+"</div>";
				$('#EPIMessage').modal('show');
				document.getElementById("EPIMsg").innerHTML=divMsg;
			}
			
			if( $('#distHiddenId').val()!=0 && (districtRequisitionId!=0 || requisitionNumber!=""))
			{
				var divMsg="<div>"+resourceJSON.msgRequisitionNumber+"</div>";
				$("#attachedListSchool option").remove();
				$("#avlbListSchool option").remove();
				document.getElementById("showSchoolDiv").innerHTML="";
				$('#EPIMessage').modal('show');
				document.getElementById("EPIMsg").innerHTML=divMsg;
				$('#districtRequisitionId').val("");
				$('#requisitionNumber').val("");
			}
			
			$('#distHiddenId').val($('#districtOrSchooHiddenlId').val());
			
			ManageJobOrdersAjax.getJobCategoryByDistrictId($('#districtOrSchooHiddenlId').val(),{ 
				async: false,
				callback: function(data)
				{
					if(data!="")
					{
						$('#jobCategoryDiv').html(data);
						$("#jobSubCateDiv").show();
					}else{
						$("#jobSubCateDiv").hide();
					}
				},
			errorHandler:handleError 
			});	
			
			/*============ Show Hide Requisation Field against District Id ---------------*/
			if($('#districtOrSchooHiddenlId').val()!="")
			{
				ManageJobOrdersAjax.getDistrictApprovalFromDistrictId($('#districtOrSchooHiddenlId').val(),{ 
					async: false,
					callback: function(data_approval)
					{
						if(data_approval!=null)
						{
							/*================ Gagan [Start] : Functionaliy for Auto select  state acording to district while adding certificate on joborder ==================*/
								clearCertType();
								var optstate = document.getElementById('stateMaster').options;
								for(var i = 0, j = optstate.length; i < j; i++)
								{
									  if(data_approval.stateId.stateId==optstate[i].value)
									  {
										  optstate[i].selected	=	true;
										  break;
									  }
								}

								$("#districtReqNumFlag").val(data_approval.isReqNoRequired);
								
								ManageJobOrdersAjax.getRequisitionByDistrictId($('#districtOrSchooHiddenlId').val(),{ 
									async: false,
									callback: function(data_Req)
									{
										if(data_Req!="")
										{
											$('#avlbList').html(data_Req);
											$('#avlbListSchool').html(data_Req);
										}
									},
								errorHandler:handleError 
								});	
								
								$("#reqNumberDiv").fadeIn();
								
								if(data_approval.isReqNoRequired)
								{
									$("#isReqNoFlagByDistrict").val(1);   
								}
								else
								{	
									$("#addReqLink").hide();
									$("#isReqNoFlagByDistrict").val(0);   
									/* ==== If Hired By the School ==========*/
									$("#reqNumberSchoolDiv").hide();
								}
								/*======= Removing Requisition Number in case of Adding Job Order ============*/
								$('#districtRequisitionId').val("");
								$('#requisitionNumber').val("");
								$('#arrReqNum').val("");
							/*============================== Gagan [END] :==================================================== */
							
							if($('#jobOrderType').val()==2)
							{
								// Set read write privilege acording to District Set up read write privilege  
								if(data_approval.writePrivilegeToSchool==true)
									document.getElementById("writePrivilegeToSchool").checked=true;
								else
									document.getElementById("writePrivilegeToSchool").checked=false;
							}
							else
							{
								if(data_approval.districtApproval==0)
									$("#requisitionDiv").fadeIn();
								else
								{
									if(data_approval.districtApproval==1)
									$("#requisitionDiv").fadeOut();
								}
							}
						}
					},
				errorHandler:handleError 
				});	
			 }
		}
	}
}

function getRequisitionByDistrictId(distId)
{
	ManageJobOrdersAjax.getRequisitionByDistrictId(distId,{ 
		async: false,
		callback: function(data)
		{
			if(data!="")
			{
				$('#avlbList').html(data);
			}
		},
	errorHandler:handleError 
	});	
}

function shiftRight() {
	
}

function shiftLeft() {

}

function der() {
	var values = $('#attachedList').val();
}
/*========  SearchDistrictOrSchool ===============*/

function validateNewReq() {
	
	document.getElementById("newReqCountFlag").value=0;
	document.getElementById("newReqDuplicateFlag").value=0;

	$('#textDuplicateReqSpan').empty();
	var rdReqNoSource=0;
	if($("input[name=rdReqNoSource]:checked").val()!=undefined)
		rdReqNoSource=$("input[name=rdReqNoSource]:checked").val();
	if(rdReqNoSource=="1")
	{
		var noOfExpHires	=	trim(document.getElementById("noOfExpHires").value);
		var isReqNoFlagByDistrict =	$("#isReqNoFlagByDistrict").val();
		
		var newTextRequisitionNumbers=trim(document.getElementById("newTextRequisitionNumbers").value);
		if(newTextRequisitionNumbers.length > 1)
		{
			if(noOfExpHires=="")
			{
				$('#errordiv').show();
				$('#errordiv').append("&#149; "+resourceJSON.msgExpectedHire+"<br>");
				$('#noOfExpHires').focus();
				$('#noOfExpHires').css("background-color", "#F5E7E1");
			}
			else
			{
				$('#noOfExpHires').css("background-color", "");
				
				$('#loadingDiv').show();
				var jobId	=	document.getElementById("jobId").value;
				var districtId	=	document.getElementById("districtHiddenId").value;
				ManageJobOrdersAjax.validateNewReq(districtId,jobId,newTextRequisitionNumbers,{ 
					async: true,
					callback: function(data)
					{	
						if(data!=null)
						{
							
							var reqCount = Number(data[0]);  
							document.getElementById("newReqCountFlag").value=reqCount;
							if(noOfExpHires!=0 && data[0]!=null && reqCount > noOfExpHires )
							{
								$('#duplicateReqModal').modal('show');
								$('#textDuplicateReqSpan').append("&#149; "+resourceJSON.msgNumberofEnterRequisitionNumber+"<br>");
							}
							
							if(data[1]!=null && data[1]!="")
							{
								$('#duplicateReqModal').modal('show');
								$('#textDuplicateReqSpan').append("&#149; "+data[1]+"<br>");
							}
							
							var newReqCountFlag = Number(data[2]); 
							if(newReqCountFlag==2)
							{
								document.getElementById("newTextRequisitionNumbers").value="";
								$('#newTextRequisitionNumbers').css("background-color", "#F5E7E1");
							}
							else if(newReqCountFlag==1)
							{
								document.getElementById("newReqDuplicateFlag").value=newReqCountFlag;
							}
						}	
						$('#loadingDiv').hide();
					},
				});
			}
		}
	}
}

function validateNewReqSchool()
{
	$('#newTextRequisitionNumbersSchool').css("background-color", "");
	
	document.getElementById("newReqCountFlag").value=0;
	document.getElementById("newReqDuplicateFlag").value=0;
	
	$('#textDuplicateReqSpan').empty();
	var rdReqNoSource=0;
	if($("input[name=rdReqNoSourceSchool]:checked").val()!=undefined)
		rdReqNoSource=$("input[name=rdReqNoSourceSchool]:checked").val();
	if(rdReqNoSource=="1")
	{
		var noOfExpHires	=	trim(document.getElementById("noOfSchoolExpHires").value);
		var isReqNoFlagByDistrict =	$("#isReqNoFlagByDistrict").val();
		
		var newTextRequisitionNumbers=trim(document.getElementById("newTextRequisitionNumbersSchool").value);
		if(newTextRequisitionNumbers.length > 0)
		{
			if(noOfExpHires=="")
			{
				$('#errordiv').show();
				$('#errordiv').append("&#149; "+resourceJSON.msgExpectedHire+"<br>");
				$('#noOfSchoolExpHires').focus();
				$('#noOfSchoolExpHires').css("background-color", "#F5E7E1");
			}
			else
			{
				$('#noOfSchoolExpHires').css("background-color", "");
				
				$('#loadingDiv').show();
				var jobId	=	document.getElementById("jobId").value;
				var districtId	=	document.getElementById("districtHiddenId").value;
				ManageJobOrdersAjax.validateNewReq(districtId,jobId,newTextRequisitionNumbers,{ 
					async: true,
					callback: function(data)
					{	
						if(data!=null)
						{
							var reqCount = Number(data[0]);  
							document.getElementById("newReqCountFlag").value=reqCount;
							if(noOfExpHires!=0 && data[0]!=null && reqCount > noOfExpHires )
							{
								$('#duplicateReqModal').modal('show');
								$('#textDuplicateReqSpan').append("&#149; "+resourceJSON.msgNumberofEnterRequisitionNumber+"<br>");
								$('#newTextRequisitionNumbersSchool').css("background-color", "#F5E7E1");
							}
							
							if(data[1]!=null && data[1]!="")
							{
								$('#duplicateReqModal').modal('show');
								$('#textDuplicateReqSpan').append("&#149; "+data[1]+"<br>");
							}

							var newReqCountFlag = Number(data[2]); 
							if(newReqCountFlag==2)
							{
								document.getElementById("newTextRequisitionNumbersSchool").value="";
								$('#newTextRequisitionNumbersSchool').css("background-color", "#F5E7E1");
							}
							else if(newReqCountFlag==1)
							{
								document.getElementById("newReqDuplicateFlag").value=newReqCountFlag;
							}
						}	
						$('#loadingDiv').hide();
					},
				});
			}
		}
	}
}

//sandeep

function validateAddEditNdCloneJobOrder(){
	
	$('#loadingDiv').hide();
	//document.getElementById("copy_text").value=dd[0];
	//var copyURLMsg='';
	//if(checkFlash()==true){
	//	copyURLMsg="<div id='d_clip_container'><div id='d_clip_button' style='width:70px;text-align:center; border:0px solid black; background-color:#FFFFFF; cursor:default;'> "+resourceJSON.msgCopyURL+"</div></div>";
	//}
	var divMsg="<div>"+resourceJSON.msgCloneJobCreation+"</div>";
	$('#jobOrderCloneSend').modal('show');
	document.getElementById("jobOrderCloneText").innerHTML=divMsg;
	init_swf();
	return false;
	
}
function cancelAddEditJobOrder()
{
	$('#jobOrderCloneSend').modal('hide');
}

//end

//New method

function validateAddEditJobOrder()
{
	
	var jobStartTime='';
	var jobEndTime='';
	var jobTimeZoneId='';
	
	if($("#jobStartTime").val()!=undefined)
	jobStartTime=$("#jobStartTime").val();
	
	if($("#jobEndTime").val()!=undefined)
	jobEndTime=$("#jobEndTime").val();
	
	if($("#jobtimezone").val()!=undefined)
	jobTimeZoneId=$("#jobtimezone").val();
	
	statusActiveOrNot="";
	
	try{
		statusActiveOrNot=document.getElementById("statusActiveOrNot").value;
		
	}catch(err){}
	
	var rdReqNoSource=0;
	
	var newTextRequisitionNumbers="";
	
	
	//For Hide And Show For Requisition Section At Job Order. 
	if($("#isReqNoRequiredRequisition").val()!=undefined && ($("#isReqNoRequiredRequisition").val().trim() == 'true' || $('#isReqNoHiringRequisition').val().trim() == 'true'))
	{
		if($("input[name=rdReqNoSource]:checked").val()!=undefined)
			rdReqNoSource=$("input[name=rdReqNoSource]:checked").val();
		
		newTextRequisitionNumbers=trim(document.getElementById("newTextRequisitionNumbers").value);
	}
	
	
	var newReqCountFlag=document.getElementById("newReqCountFlag").value;
	
	var newReqDuplicateFlag=document.getElementById("newReqDuplicateFlag").value;
	
	var jobType=document.getElementById("jobType").value;
	
	var reqPositionNum=$(".reqPositionNum").val();
	
	var disIdforjeffco	=	$('#districtOrSchooHiddenlId').val();
	
	if(disIdforjeffco=='804800'){
		
       var jeffcoSpecialEdFlag=document.getElementById("jeffcoSpecialEdFlag").value;
    }
	
	var geoZoneId=0;
	var isZoneAvailable=document.getElementById("isZoneAvailable").value;
	geoZoneId=document.getElementById("zone").value;
	
	var isExpHireNotEqualToReqNoValue	=	false;
	var JobOrderType					=	document.getElementById("JobOrderType").value;
	var districtOrSchooHiddenlId		=	document.getElementById("districtOrSchooHiddenlId").value;
	var jobTitle						=	trim(document.getElementById("jobTitle").value);
	var refno ="";
	if(districtOrSchooHiddenlId !=806900)
	if(jQuery("#refno").length>0)
	 refno								=	trim(document.getElementById("refno").value);
	var jobStartDate					=	trim(document.getElementById("jobStartDate").value);
	//************ Deepak  *********************
	var positionStartDate				=	trim(document.getElementById("positionStartDate").value);
	var positionEndDate					=	trim(document.getElementById("positionEndDate").value);
	//Added by Gaurav Kumar
	var positionStart                   = "";
	
	if($('#positionStart').length)
	  positionStart        			    =   trim(document.getElementById("positionStart").value);
	var tempType				        =	document.getElementById("tempType");
	tempType=tempType.options[tempType.selectedIndex].value;
	var jobApplicationStatus	     =	document.getElementById("jobApplicationStatus");
	jobApplicationStatus=jobApplicationStatus.options[jobApplicationStatus.selectedIndex].value;
	var jobEndDate 					= 	"";

	var jobEndDate = "";
	if ($("#endDateRadio1").prop("checked")) {
		jobEndDate	=	trim(document.getElementById("jobEndDate").value);
	}else if ($("#endDateRadio2").prop("checked")) {
		jobEndDate	=	"12-25-2099";
	}
	
	var districtORSchoolName			=	trim(document.getElementById("districtORSchoolName").value);
	var jobDescription					=	trim($('#jDescription').find(".jqte_editor").html());
	var jobQualification				=	$('#jQualification').find(".jqte_editor").html();
	var jobIntNotes						=	$('#jobIntNotes').find(".jqte_editor").html();
	var charCountforQDescription		=	trim($('#jQualification').find(".jqte_editor").text());
	
	var noOfExpHires					=	trim(document.getElementById("noOfExpHires").value);
	
	var isReqNoFlagByDistrict 			=	$("#isReqNoFlagByDistrict").val();
	var attachedList					=	document.getElementById("attachedList");
	var noOfExpHiresSchool				=	trim(document.getElementById("noOfExpHiresSchool").value);
	var currentSchoolExpectedHires 		=	$('#currentSchoolExpectedHires').val();
	var errorCount=0;
	var focusCount=0;
	$('#errordiv').empty();
	var jobAuthKey 			= 	$('#jobAuthKey').val();
	var isJobAssessment		=	trim(document.getElementById("isJobAssessmentVal").value);
	var attachJobAssessment	=	trim(document.getElementById("attachJobAssessmentVal").value);
	var assessmentDocument	=	$("#assessmentDocument").val();
	var exitURLMessageVal	=	document.getElementById("exitURLMessageVal").value;
	var exitURL				=	trim(document.getElementById("exitURL").value);
	var exitMessage			=	trim($('#eMessage').find(".jqte_editor").html());
	var districtHiddenId	=	document.getElementById("districtHiddenId").value;
	var jobId				=	document.getElementById("jobId").value;
	var jobIdNC				=	document.getElementById("jobIdNC").value;
	var ncStatusId			=	"";
	try{ncStatusId = document.getElementById("ncStatusId").value;}catch(e){}
	
	var schoolTyp="";
	if(disIdforjeffco=='7800047'){
		 schoolTyp           =   trim(document.getElementById("selSclTyTId").value);
	}
	var totalReqNo;
	var arraySchoolGrid  	= 	"";
	var allSchoolGradeDistrictVal	=	trim(document.getElementById("allSchoolGradeDistrictVal").value);
	
	var arrReqNum	=	"";
	
	var serviceTechnician			=	trim(document.getElementById("primaryESTechnician").value);
	var salaryRange					=	trim(document.getElementById("salaryRange").value);
	

	//for district administrator adding requisitioin no 
	var arrReqNumAndSchoolId			=	"";
	var arrReqNumVal=0;
	var offerJSI;
	if(document.getElementById("isJobAssessment1").checked == true){
		offerJSI = 0;
	} else if(document.getElementById("isJobAssessment2").checked == true){
		
			if(document.getElementById("offerJSIRadioMaindatory").checked == true){
					offerJSI = 1;
				} else if(document.getElementById("offerJSIRadioOptional").checked == true){
					offerJSI = 2;
				}
	}

	if(allSchoolGradeDistrictVal==1)
	{
		var opts = document.getElementById('attachedList').options;
		for(var i = 0, j = opts.length; i < j; i++)
		{
			arrReqNum+=opts[i].value+"#";
		}
		if(arrReqNum!="")
			arrReqNumVal=(arrReqNum.split("#").length-1);
	}

	if(jobId!='') {
		if(allSchoolGradeDistrictVal == 1) {
			ManageJobOrdersAjax.countTotalReqNo(jobId,{
				async: false,
				callback: function(data_countReqNum){
					 if(data_countReqNum!=null){
						  totalReqNo = data_countReqNum.length;
					 }
				},
			});
		} else {
			ManageJobOrdersAjax.isReqNoEqualToNoOfExpHired(jobId,{
				async: false,
				callback: function(data_countReqNum){
					 if(data_countReqNum!=""){
						 arraySchoolGrid  = data_countReqNum.split(",");
					 }
				},
			});
		}
	}	
	var schoolId=document.getElementById("schoolId").value;
	
	var dateTime=trim(document.getElementById("dateTime").value);
	
	var attachJobAssessment1	=	document.getElementById("attachJobAssessment1").checked;
	var attachJobAssessment2	=	document.getElementById("attachJobAssessment2").checked;
	var attachJobAssessment3	=	document.getElementById("attachJobAssessment3").checked;
	var jobCategoryId			=	0;//document.getElementById("jobCategoryId").value;
	var subjectId ="";
	if($("#subjectId").length>0)
	 subjectId				=	document.getElementById("subjectId").value;
	
	if(document.getElementById("jobSubCategoryId")!=null && document.getElementById("jobSubCategoryId").value!="0")
		jobCategoryId = document.getElementById("jobSubCategoryId").value;
	else
		jobCategoryId = document.getElementById("jobCategoryId").value;
	
	
	$('#noOfExpHires').css("background-color", "");

	var assessmentDocumentVal="";
	var jDec="",qDes="",dName="",jTitle="",nHires="",sNHires="",aDoc="",eUrl="",eMsg="",sDate="",eDate="",sChk="",jobCId="",subId="";
	$("#errordiv").empty();
	$('#jobDescriptionErrordiv').empty();
	
	var jDescription	=	trim($('#jDescription').find(".jqte_editor").text());
	
	$('#districtORSchoolName').css("background-color", "");
	$('#jobTitle').css("background-color", "");
	$('#jobStartDate').css("background-color", "");
	$('#positionStartDate').css("background-color", "");
	$('#positionEndDate').css("background-color", "");
	$('#jobEndDate').css("background-color", "");
	$('#jobCategoryId').css("background-color", "");
	$('#tempType').css("background-color", "");
	$('#avlbList').css("background-color", "");
		if(districtORSchoolName=="" && districtOrSchooHiddenlId==""){
			$('#errordiv').show();
			$('#errordiv').append("&#149; "+resourceJSON.PlzEtrDistrict+"<br>");
			if(focusCount==0)
			$('#districtORSchoolName').focus();
			$('#districtORSchoolName').css("background-color", "#F5E7E1");
			errorCount++;
			focusCount++;
		}else if(districtOrSchooHiddenlId==""){
			$('#errordiv').show();
			$('#errordiv').append("&#149; "+resourceJSON.msgvaliddistrict+"<br>");
			if(focusCount==0)
			$('#districtORSchoolName').focus();
			$('#districtORSchoolName').css("background-color", "#F5E7E1");
			errorCount++;
			focusCount++;
		}
	if(JobOrderType==3){
		$('#schoolName').css("background-color", "");
		var schoolName=trim(document.getElementById("schoolName").value);
		if(schoolName==''){
			$('#errordiv').show();
			$('#errordiv').append("&#149; "+resourceJSON.msgPleaseenterSchoolName+"<br>");
			if(focusCount==0)
			$('#schoolName').focus();
			$('#schoolName').css("background-color", "#F5E7E1");
			errorCount++;
			focusCount++;
		}else if(schoolId==""){
			$('#errordiv').show();
			$('#errordiv').append("&#149; "+resourceJSON.msgvalidschool+"<br>");
			if(focusCount==0)
			$('#schoolName').focus();
			$('#schoolName').css("background-color", "#F5E7E1");
			errorCount++;
			focusCount++;
		}
	}else{
		schoolId=0;
	}
	var districtId	=	$('#districtOrSchooHiddenlId').val();
	if($("#refno").val()=='' && districtId=='806900'){
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.msgJobCode+"<br>");
		$("#refno").css("background-color", "#F5E7E1");
		errorCount++;
		focusCount++;	
	}
	
	
	if(jobTitle==""){
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.msgjobtitleenter+"<br>");
		if(focusCount==0)
		$('#jobTitle').focus();
		$('#jobTitle').css("background-color", "#F5E7E1");
		errorCount++;
		focusCount++;
	}
	
	var countforJobDescription = jobDescription.length;
	if(countforJobDescription > 65000)
	{
		errorCount++;
		focusCount++;
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.msgDescriptionlengthisexceeded+"<br>");
	}
	
	if(jobStartDate==""){
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.msgPostingStartDate+"<br>");
		if(focusCount==0)
		$('#jobStartDate').focus();
		$('#jobStartDate').css("background-color", "#F5E7E1");
		errorCount++;
		focusCount++;
	}
	if(jobEndDate==""){
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.msgPostingEndDate+"<br>");
		if(focusCount==0)
		$('#jobEndDate').focus();
		$('#jobEndDate').css("background-color", "#F5E7E1");
		errorCount++;
		focusCount++;
	}
	
	//jeffco 
	var jeffcoExtraFields=new Array();
	if(districtId=='804800'){
		
		var fte=$('[name="fte"]').val();
		var paygrade=$('[name="paygrade"]').val();
		var daysworked=$('[name="daysworked"]').val();
		var fsalary=$('[name="fsalary"]').val();
		var fsalaryA=$('[name="fsalaryA"]').val();
		var ssalary=$('[name="ssalary"]').val();
		jeffcoExtraFields.push(fte);
		jeffcoExtraFields.push(paygrade);
		jeffcoExtraFields.push(daysworked);
		jeffcoExtraFields.push(fsalary);
		jeffcoExtraFields.push(fsalaryA);
		jeffcoExtraFields.push(ssalary);
	if(reqPositionNum=="" || reqPositionNum=="0"){
		$('#errordiv').show();
		$('#errordiv').prepend("&#149; Select Position Number<br>");
		$('#avlbList').focus();
		$('#avlbList').css("background-color", "#F5E7E1");
		errorCount++;
		focusCount++;
	}
	if(positionStartDate==""){
		$('#errordiv').show();
		$('#errordiv').append("&#149; Please enter Position Start Date<br>");
		if(focusCount==0)
		$('#positionStartDate').focus();
		$('#positionStartDate').css("background-color", "#F5E7E1");
		errorCount++;
		focusCount++;
	}
	
	if(jobApplicationStatus!=1){
		if(positionEndDate==""){
			$('#errordiv').show();
			$('#errordiv').append("&#149; Please enter Position End Date<br>");
			if(focusCount==0)
			$('#positionEndDate').focus();
			$('#positionEndDate').css("background-color", "#F5E7E1");
			errorCount++;
			focusCount++;
		}
	}
	
	if(jobApplicationStatus==2){
		if(tempType=="0"){
			$('#errordiv').show();
			$('#errordiv').append("&#149; Please select Temporary Type<br>");
			if(focusCount==0)
			$('#tempType').focus();
			$('#tempType').css("background-color", "#F5E7E1");
			errorCount++;
			focusCount++;
		}
	}
	
	if(serviceTechnician=="" || serviceTechnician=="0")
	{
		$('#errordiv').show();
		$('#errordiv').append("&#149; Select Primary Employment Services Technician<br>");
		if(focusCount==0)
		$('#primaryESTechnician').focus();
		$('#primaryESTechnician').css("background-color", "#F5E7E1");
		errorCount++;
		focusCount++;
	}
	
	if(positionStartDate!="" && positionEndDate!=""){
		try{
			var date1 = positionStartDate.split("-");
			var	date2 = positionEndDate.split("-");
		    var sDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);		    
		    var eDate = new Date(date2[0]+"/"+date2[1]+"/"+date2[2]);
		    if(sDate > eDate){
				$('#errordiv').show();
				$('#errordiv').append("&#149; Position Start Date must be less than or equal to Position End Date<br>");
				if(focusCount==0)
				$('#positionStartDate').focus();
				$('#positionStartDate').css("background-color", "#F5E7E1");
				errorCount++;
				focusCount++;
			} 
		}catch(err){}
	  }	
	
	if(jobStartDate!="" && jobEndDate!=""  && jobId==''){
		var dateFirst = new Date(jobStartDate);
		var dateSecond = new Date(jobEndDate);
		var diffDaysttt = parseInt((dateSecond - dateFirst) / (1000 * 60 * 60 * 24)); 
			if(diffDaysttt < 7){
	        	    $('#errordiv').show();
					$('#errordiv').append("&#149; Positions must be posted for 7 days<br>");
					if(focusCount==0)
					$('#jobEndDate').focus();
					$('#jobEndDate').css("background-color", "#F5E7E1");
					errorCount++;
					focusCount++;
	        }
	}

	if(jobStartDate!="" && jobEndDate!="" && jobId!=''){
		globalJobCategoryName = document.getElementById("jobCategoryId").value;
		var jobId				=	document.getElementById("jobId").value;
			ManageJobOrdersAjax.checkDifferenceBetweenStartAndEndDate(jobId,districtHiddenId,jobStartDate,jobEndDate,globalJobCategoryName,{
			async: false,
			callback: function(data){
				 if(data!=1){
					document.getElementById("hideSave").style.display='inline';
					$('#loadingDiv').hide();
					$('#errordiv').show();
					$('#errordiv').append("&#149; "+data+"<br>");
					errorCount++;
				}
			},
		});
	}
}
	
	/*Added by Gaurav Kumar*/
		var positionNumber="";
		var strAdams =  "";
		var salaryAdminPlan = "";
		var approvalCode = "";
		var step = "";
		var jobApprovalProcessId="";
	
	if(districtId=='806900'){
		var adamsExtraFields=new Array();
		var fte=$('#fteforAdams').val();
		var paygrade=$('#paygrade').val();
		var monthlySalary = $('[name="monthlySalary"]').val();
		var accountCode   = $('[name="accountCode"]').val();
		var positionType  = $('[name="positionType"]').val();
		var monthsPerYear = $('[name="monthsPerYear"]').val();
		var jobCode=trim(document.getElementById("refno").value);
	//	var departmentApproval = $('[name="departmentApproval"]').val();
		positionNumber     = $("#positionNumber").val();
		var hourPrday      = $("#hourPrday").val();
		salaryAdminPlan    = $("#salaryAdminPlan").val();
		approvalCode	   = $("#approvalCode").val();
		step   			   = $("#step").val();
		
		jobApprovalProcessId = $("#jobApprovalProcessId").val();
		
		if(fte!="")
			adamsExtraFields[0] = fte;
		else
			adamsExtraFields[0] = "##";	
		if(paygrade!="")	
			adamsExtraFields[1] = paygrade;
		else
			adamsExtraFields[1] = "##";
		if(monthlySalary!="")	
			adamsExtraFields[2] = monthlySalary;
		else
			adamsExtraFields[2] = "##";
		if(accountCode!="")	
			adamsExtraFields[3] = accountCode;
		else
			adamsExtraFields[3] = "##";
		if(positionType!="")	
			adamsExtraFields[4] = positionType;
		else
			adamsExtraFields[4] = "##";
		if(monthsPerYear!="")
			adamsExtraFields[5] = monthsPerYear;
		else
			adamsExtraFields[5] = "##";
		//adamsExtraFields[6] = departmentApproval;
		if(jobCode!="")
		adamsExtraFields[6]=jobCode;
		else
		adamsExtraFields[6] = "##";
		strAdams = adamsExtraFields.join("~");
		
		//adamsExtraFields.push(monthlySalary);
		//adamsExtraFields.push(accountCode);
		//adamsExtraFields.push(positionType);
		//adamsExtraFields.push(monthsPerYear);
		//alert("strAdams"+strAdams);
		
		
		if(accountCode==''){
			$('#errordiv').show();
			$('#errordiv').append("&#149; "+resourceJSON.msgAccountCode+"<br>");
			$("#accountCode").css("background-color", "#F5E7E1");
			errorCount++;
			focusCount++;	
			
		}
		
		if(positionType==''){
			$('#errordiv').show();
			$('#errordiv').append("&#149; "+resourceJSON.msgPositionType+"<br>");
			$("#positionType").css("background-color", "#F5E7E1");
			errorCount++;
			focusCount++;
		}
		if(hourPrday==""){
			$('#errordiv').show();
			$('#errordiv').append("&#149; "+resourceJSON.msgHourPerWeek+"<br>");
			$("#hourPrday").css("background-color", "#F5E7E1");
			errorCount++;
			focusCount++;
		}
		
		/*if(monthsPerYear==''){
			$('#errordiv').show();
			$('#errordiv').append("&#149; "+resourceJSON.msgMonthPerYear+"<br>");
			$("#monthsPerYear").css("background-color", "#F5E7E1");
			errorCount++;
			focusCount++;
		}*/
		if(jobApprovalProcessId==''){
			$('#errordiv').show();
			$('#errordiv').append("&#149; "+resourceJSON.msgJobApprovalProcess+"<br>");
			$("#jobApprovalProcessId").css("background-color", "#F5E7E1");
			errorCount++;
			focusCount++;
			
		}
		
		if(jobApplicationStatus==0){
			$('#errordiv').show();
			$('#errordiv').append("&#149; "+resourceJSON.msgJobStatus+"<br>");
			$("#jobApplicationStatus").css("background-color", "#F5E7E1");
			errorCount++;
			focusCount++;
		}
		
		/*if(fte==''){
			$('#errordiv').show();
			$('#errordiv').append("&#149; "+resourceJSON.msgFte+"<br>");
			$("#fteforAdams").css("background-color", "#F5E7E1");
			errorCount++;
			focusCount++;
		}*/else{
			
			$("#fteforAdams").css("background-color", "none");
		}
		/*if(monthlySalary==''){
			$('#errordiv').show();
			$('#errordiv').append("&#149; "+resourceJSON.msgMinimumSalary+"<br>");
			$("#monthlySalary").css("background-color", "#F5E7E1");
			errorCount++;
			focusCount++;
		}*/
		
		jQuery("input[name='fte']").hide();
	}
	
	
	/* Dhananjay Verma Job Approval Processs For Global TPL 4719*/
	jobApprovalProcessId = $("#jobApprovalProcessId").val();
	
	/*if(jobApprovalProcessId==''){
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.msgJobApprovalProcess+"<br>");
		$("#jobApprovalProcessId").css("background-color", "#F5E7E1");
		errorCount++;
		focusCount++;
		
	}*/
	
	if(jobApprovalProcessId=='' || jobApprovalProcessId ==undefined){
		
		jobApprovalProcessId=""; 
	}
		
	
	
	
	
	if(($('#positionStart').length==1) && positionStart==''){
			$('#errordiv').show();
			$('#errordiv').append("&#149; Please choose Position Start Date<br>");
			if(focusCount==0)
			$('#positionStart').focus();
			$('#positionStart').css("background-color", "#F5E7E1");
			errorCount++;
			focusCount++;
	}
	/*if(paygrade==''){
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.msgPayGrade+"<br>");
		if(focusCount==0)
		$('#paygrade').focus();
		$('#paygrade').css("background-color", "#F5E7E1");
		errorCount++;
		focusCount++;
		
	}*/
	
	
	/* For VVI */
	var offerVirtualVideoInterview="";
	var quesId="";
	var quesName="";
	var maxScoreForVVI="";
	var sendAutoVVILink="";
	var slctStatusID="";
	var timePerQues="";
	var vviExpDays="";
	var jobCompletedVVILink="";
	var jobCompletedVVILink1="";
	var jobCompletedVVILink2="";
	
	offerVirtualVideoInterview=document.getElementById('offerVirtualVideoInterview').checked;
	
	var isInviteOnly="";
	isInviteOnly=document.getElementById('isInviteOnly').checked;
	var senDnotificationToschools = document.getElementById('isSendNotification').checked;

	if(offerVirtualVideoInterview==true)
	{
		wantScore=document.getElementById('wantScore').checked;
		sendAutoVVILink=document.getElementById('sendAutoVVILink').checked;
		timePerQues=trim(document.getElementById('timePerQues').value);
		vviExpDays=trim(document.getElementById('vviExpDays').value);
		quesId=trim(document.getElementById('quesId').value);
		quesName=trim(document.getElementById('quesName').value);
		
		if(quesName=="")
		{
			$('#errordiv').append("&#149; "+resourceJSON.msgPleaseQuestionSet+"<br>");
			$('#quesName').css("background-color", "#F5E7E1");
			if(focusCount==0)
				$('#quesName').focus();
			errorCount++;
			focusCount++;
		}
		
		if(wantScore==true)
		{
			maxScoreForVVI=trim(document.getElementById('maxScoreForVVI').value);
			if(maxScoreForVVI=="")
			{
				$('#errordiv').append("&#149; "+resourceJSON.msgPleaseMaxScore+"<br>");
				$('#maxScoreForVVI').css("background-color", "#F5E7E1");
				if(focusCount==0)
					$('#maxScoreForVVI').focus();
				errorCount++;
				focusCount++;
			}
		}
		
		if(sendAutoVVILink==true){
			jobCompletedVVILink1=document.getElementById('jobCompletedVVILink1').checked;
			jobCompletedVVILink2=document.getElementById('jobCompletedVVILink2').checked;
			
			if(jobCompletedVVILink1==true)
				jobCompletedVVILink = false;
			else if(jobCompletedVVILink2==true)
				jobCompletedVVILink = true;
			
			if(jobCompletedVVILink==false)
				slctStatusID=trim(document.getElementById('slctStatusID').value);
		}
			
		
		if(timePerQues!="")
		{
			if(timePerQues<30)
			{
				$('#errordiv').show();
				$('#errordiv').append("&#149; "+resourceJSON.msgquestionatleast30seconds+"<br>");
				
				if(focusCount==0)
					$('#timePerQues').focus();
				
				$('#timePerQues').css("background-color", "#F5E7E1");
				counter++;
				focusCount++;
			}
		}
	}
	
	
	/*Start :: District Assessment */
	var offerDASMT 		= 	document.getElementById("offerAssessmentInviteOnly").checked;
//	var distASMTId	=	"";
	var opts = "";
	var arrDAIds = "";
	
	if(offerDASMT)
	{
	//	distASMTId	=	trim(document.getElementById("assessmentId").value);
		
		opts = document.getElementById('attachedDAList').options;
		
		for(var i = 0, j = opts.length; i < j; i++)
		{
			arrDAIds+=opts[i].value+"#";
		}
		
		if(document.getElementById("attachedDAList").options.length==0)
		{
			$("#errordiv").show();
			$('#errordiv').append("&#149; "+resourceJSON.msgleastoneDistrictAssessment+"<br>");
			if(focusCount==0)
				$('#assessmentName').focus();
			
			$('#assessmentName').css("background-color", "#F5E7E1");
			counter++;
			focusCount++;
		}
	}
	/*End :: District Assessment */
	
	
	
	
	if(jobStartDate!="" && jobEndDate!=""){
		try{
			var date1 = jobStartDate.split("-");
			var	date2 = jobEndDate.split("-");
		    var sDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
		    var eDate = new Date(date2[0]+"/"+date2[1]+"/"+date2[2]);
			if(sDate > eDate){
				$('#errordiv').show();
				$('#errordiv').append("&#149; "+resourceJSON.msggreaterthanenddate+"<br>");
				if(focusCount==0)
				$('#jobStartDate').focus();
				$('#jobStartDate').css("background-color", "#F5E7E1");
				errorCount++;
				focusCount++;
			} 
		}catch(err){}
	}
	
	
	
	var countforQDescription = charCountforQDescription.length;
	if(countforQDescription>1000)
	{
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.msgcannotexceed1000characters+"<br>");
		//if(focs==0)
			$('#jQualification').find(".jqte_editor").focus();
		$('#jQualification').find(".jqte_editor").css("background-color", "#F5E7E1");
	//	cnt++;focs++;
		//qDes="1";
		errorCount++;
		focusCount++;
	}
	if(jobCategoryId==0){
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.msgJobCategoryName+"<br>");
		if(focusCount==0)
		$('#jobCategoryId').focus();
		$('#jobCategoryId').css("background-color", "#F5E7E1");
		errorCount++;
		focusCount++;
	}
	if(disIdforjeffco=='7800047'){
	if(schoolTyp==""){
		//alert("in the schoolTyp== ");
		
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.msgforSelSchoolType+"<br>");
		if(focusCount==0)
		$('#selSclTyTId').focus();
		$('#selSclTyTId').css("background-color", "#F5E7E1");
		errorCount++;
		focusCount++;
	}else{
		$('#selSclTyTId').css("background-color", "");
	}
} 
	var entityType = $("#entityType").val();
	if(entityType!=3){
	
	if(JobOrderType==2 && allSchoolGradeDistrictVal==1 && ( noOfExpHires=="" || noOfExpHires==0) && ncStatusId != 2){
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.msgExpectedHire+"<br>");
		if(focusCount==0)
		$('#noOfExpHires').focus();
		$('#noOfExpHires').css("background-color", "#F5E7E1");
		focusCount++;
		errorCount++;
		nHires="1";
	}else if((JobOrderType==2) && ncStatusId != 2){
		
		if($("#isReqNoRequired").val()=="true"){
			
			if($("#isExpHireNotEqualToReqNo").is(':checked')){
				
				if(isReqNoFlagByDistrict==1 && allSchoolGradeDistrictVal==1){
					if(arrReqNumVal==0){
						$('#errordiv').show();
						$('#errordiv').append("&#149; "+resourceJSON.msgprovideRequisitionNumber+"<br>");
							if(focusCount==0)
								$('#attachedList').focus();
						$('#attachedListSchool').css("background-color", "#F5E7E1");
						focusCount++;
						errorCount++;
					}else if(arrReqNumVal>noOfExpHires){
						$('#errordiv').show();
						$('#errordiv').append("&#149; "+resourceJSON.msgRequisitionNumbershould+"<br>");
						if(focusCount==0)
							$('#attachedList').focus();
							$('#attachedList').css("background-color", "#F5E7E1");
						focusCount++;
						errorCount++;
					}
				}
			 }else{
				if(allSchoolGradeDistrictVal==1 && ncStatusId != 2){
					
					if(rdReqNoSource==1)
					{
						if(newReqCountFlag!=noOfExpHires || newReqCountFlag==0){
							$('#errordiv').show();
							$('#errordiv').append("&#149; "+resourceJSON.msgPleaseprovideRequisitionNumber+"<br>");
							if(focusCount==0)
								$('#newTextRequisitionNumbers').focus();	
						
							$('#newTextRequisitionNumbers').css("background-color", "#F5E7E1");
							focusCount++;
							errorCount++;
						}
					}
					else if(rdReqNoSource==2)
					{
						if(arrReqNumVal!=noOfExpHires || arrReqNumVal==0){
							$('#errordiv').show();
							$('#errordiv').append("&#149; "+resourceJSON.msgPleaseprovideRequisitionNumber+"<br>");
							if(focusCount==0)
								$('#attachedList').focus();
								$('#attachedList').css("background-color", "#F5E7E1");
							focusCount++;
							errorCount++;
						}
					}
					
				}
				else if(arrReqNumVal!=noOfExpHires && ncStatusId != 2){
					$('#errordiv').show();
					$('#errordiv').append("&#149; "+resourceJSON.msgPleaseprovideRequisitionNumber+"<br>");
					if(focusCount==0)
						$('#attachedList').focus();
						$('#attachedList').css("background-color", "#F5E7E1");
					focusCount++;
					errorCount++;
				}
			}
		}
		else
		{
			if(allSchoolGradeDistrictVal==1 && ncStatusId != 2)
			{
				if(rdReqNoSource==1)
				{
					if(newReqDuplicateFlag==1)
					{
						$('#errordiv').show();
						$('#errordiv').append("&#149; "+resourceJSON.msgPleaseremoveduplicateEnter+"<br>");
						$('#newTextRequisitionNumbers').css("background-color", "#F5E7E1");
						$('#newTextRequisitionNumbers').focus();
						errorCount++;
					}
					
					if(newReqCountFlag > noOfExpHires)
					{
						$('#errordiv').show();
						$('#errordiv').append("&#149; "+resourceJSON.msgNumberofEnterRequisitionNumber+"<br>");
						$('#newTextRequisitionNumbers').css("background-color", "#F5E7E1");
						errorCount++;
					}
				}
				else if(rdReqNoSource==2)
				{
					if(arrReqNumVal > noOfExpHires)
					{
						$('#errordiv').show();
						$('#errordiv').append("&#149; "+resourceJSON.msgRequisitionNumbershould+"<br>");
						if(focusCount==0)
							$('#attachedList').focus();
							$('#attachedList').css("background-color", "#F5E7E1");
						focusCount++;
						errorCount++;
					}
				}
			}
		}
		
		if(JobOrderType==3){
			noOfExpHires=noOfExpHiresSchool;
		}
	}else if(JobOrderType==3 && ( noOfExpHiresSchool=="" || noOfExpHiresSchool==0) && ncStatusId != 2){
		noOfExpHires=noOfExpHiresSchool;
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.msgExpectedHire+"<br>");
		if(focusCount==0)
		$('#noOfExpHires').focus();
		$('#noOfExpHires').css("background-color", "#F5E7E1");
		focusCount++;
		errorCount++;
	}else if(JobOrderType==3){
		noOfExpHires=noOfExpHiresSchool;
	 }
	}
	 if(document.getElementById("hideSAUploadURL").style.display=='none' || document.getElementById("hideSAUploadURL").style.display==''){
		 attachJobAssessment1==false;
	 }
	 if(document.getElementById("hideDAUploadURL").style.display=='none' || document.getElementById("hideDAUploadURL").style.display==''){
		 attachJobAssessment2==false;
	 }
	
	 var assessmentDocumentFileName="";
	 try {
		 assessmentDocumentFileName=document.getElementById("assessmentDocumentFileName").value;
	} catch (e) {
		// TODO: handle exception
	}
	
	 var isJobAssessment2Temp=document.getElementById("isJobAssessment2").checked;
	 if(attachJobAssessment3==true && isJobAssessment2Temp==true)
	 {
		 if($("#attachJobAssessment3").is(':visible'))
		 {
			 if(assessmentDocument=="" && (assessmentDocumentFileName==null || assessmentDocumentFileName==""))
			 {
				 $('#errordiv').show();
				 $('#errordiv').append("&#149; "+resourceJSON.msgPleaseuploadInventory+"<br>");
				 $('#assessmentDocument').css("background-color", "#F5E7E1");
				 errorCount++;
				 focusCount++;
			 }
				
			 	$("#jsiDiv").fadeIn();
				var month = new Array();
				month[0] = "January";
				month[1] = "February";
				month[2] = "March";
				month[3] = "April";
				month[4] = "May";
				month[5] = "June";
				month[6] = "July";
				month[7] = "August";
				month[8] = "September";
				month[9] = "October";
				month[10] = "November";
				month[11] = "December";
				
				$('#jobStartDate').css("background-color", "");
				var jobStartDateVal = trim(document.getElementById("jobStartDate").value);
				jobStartDateVal = jobStartDateVal.replace(/-/g, "/");
				var jobStartDateTemp	=	new Date(jobStartDateVal);
				if(trim(document.getElementById("jobStartDate").value)==""){
					$('#errordiv').show();
					$('#errordiv').append("&#149; "+resourceJSON.msgPostingStartDate+"");
					$('#jobStartDate').focus();
					$('#jobStartDate').css("background-color", "#F5E7E1");
				} else {
					$("#jsiDiv").fadeIn();
					var twoDaysAgoDate  =	new Date();
					twoDaysAgoDate = new Date(twoDaysAgoDate.setDate(twoDaysAgoDate.getDate()+2));
					var diffDays =  twoDaysAgoDate.getTime()-jobStartDateTemp.getTime();
					diffDays=parseInt(diffDays / (1000 * 60 * 60 * 24));
					if(!(diffDays<=0)){
						var tempDate=twoDaysAgoDate.getMonth()+1+"-"+twoDaysAgoDate.getDate()+"-"+twoDaysAgoDate.getFullYear();
						document.getElementById("jobStartDate").value=tempDate;
						document.getElementById("textJsiDateMsg").innerHTML=""+resourceJSON.msgJobstartdateshould+""+month[twoDaysAgoDate.getMonth()]+" "+twoDaysAgoDate.getDate()+", "+twoDaysAgoDate.getFullYear()+""
						$("#textJsiDate").modal("show");
						$('#jobStartDate').focus();
						$('#jobStartDate').css("background-color", "#F5E7E1");
						errorCount++;
						focusCount++;
					}else{
						$("#jsiDiv").fadeIn();
					}
				}	
		 } 
	 }
	 else if(isJobAssessment==1 && attachJobAssessment1==false && attachJobAssessment2==false &&  attachJobAssessment3==false &&  attachJobAssessment4==false){
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.msgleastonejobrequires+"<br>");
		if(focusCount==0)
		$('#assessmentDocument').focus();
		$('#assessmentDocument').css("background-color", "#F5E7E1");
		errorCount++;
		focusCount++;
	}else if(assessmentDocument=="" && isJobAssessment==1 &&  attachJobAssessment==2 && document.getElementById("divAssessmentDocument").style.display!='inline'){
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.msgPleaseuploadInventory+"<br>");
		if(focusCount==0)
		$('#assessmentDocument').focus();
		$('#assessmentDocument').css("background-color", "#F5E7E1");
		errorCount++;
		focusCount++;
	}
	else if(assessmentDocument!="" && isJobAssessment==1 &&  attachJobAssessment==2)
	{
		var ext = assessmentDocument.substr(assessmentDocument.lastIndexOf('.') + 1).toLowerCase();	
		
		var fileSize = 0;
		if ($.browser.msie==true)
	 	{	
		    fileSize = 0;	   
		}
		else
		{
			if(document.getElementById("assessmentDocument").files[0]!=undefined)
			{
				fileSize = document.getElementById("assessmentDocument").files[0].size;
			}
		}

		if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt'))
		{
			$('#errordiv').show();
			$('#errordiv').append("&#149; "+resourceJSON.msgAcceptableInventoryformats+"<br>");
			errorCount++;
			aDoc="1";
		}
		else if(fileSize>=10485760)
		{
			$('#errordiv').show();
			$('#errordiv').append("&#149; "+resourceJSON.msgfilesizelessthan+"<br>");
			errorCount++;
		}
		assessmentDocumentVal="Inventory"+dateTime+"."+ext;
		//alert(assessmentDocumentVal);
	}
	/*	
	 * 	Dated		:	17 Oct 2014
	 *  Purpose		:	Validatetion for District Attachment
	 *  By			:	Hanzala Subhani
	 * 
	 * */

	 /*if(uploadedDistAttachmentFile != null && uploadedDistAttachmentFile != "") {
		var ext = distAttachmentDocument.substr(distAttachmentDocument.lastIndexOf('.') + 1).toLowerCase();	
		var fileSize = 0;
		if ($.browser.msie==true) {
		    fileSize = 0;	   
		} else {
			if(document.getElementById("distAttachmentDocument").files[0]!=undefined) {
				fileSize = document.getElementById("distAttachmentDocument").files[0].size;
			}
		}
		if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt')) {
			$('#errordiv').show();
			$('#errordiv').append("&#149; Please select Acceptable Inventory formats which include PDF, MS-Word, GIF, PNG, and JPEG  files.<br>");
			errorCount++;
			aDoc="1";
		} else if(fileSize>=10485760) {
			$('#errordiv').show();
			$('#errordiv').append("&#149; File size must be less than 10mb.<br>");
			errorCount++;
		}
			distAttachmentDocumentVal	=	uploadedDistAttachmentFile;
	   } else {
		   distAttachmentDocumentVal 	= 	uploadedDistAttachmentFileEdit;
	 }*/
	
	 /*	End District Attachment Validation */

	if(exitURLMessageVal==1 &&  exitURL=="") {
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.msgapplicantshouldberedirected+"<br>");
		errorCount++;
	}

	if (exitURLMessageVal==2 &&  trim($('#eMessage').find(".jqte_editor").text())==""){
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.msgCompletionMessage+"<br>");
		$('#errordiv').find(".jqte_editor").css("background-color", "#F5E7E1");
		errorCount++;
	} else {
		var charCount=trim($('#eMessage').find(".jqte_editor").text());
		var count = charCount.length;
		if(count>1000) {
			$('#errordiv').show();
			$('#errordiv').append("&#149; "+resourceJSON.msgCompletionMessagelength+"<br>");
			$('#errordiv').find(".jqte_editor").css("background-color", "#F5E7E1");
			errorCount++;
			eMsg="1";
		}
	}

	var hiddenJob  =	$('input:radio[name=hiddenJob]:checked').val();
	if(isInviteOnly && hiddenJob==undefined)
	{		
		$('#errordiv').show();
		$('#errordiv').append("&#149; Please choose hidden job type.<br>");
		if(errorCount==0)
   			$('#hiddenJob1').focus();
		errorCount++;
		focusCount++;
	}
	
	if(eUrl==1){
		$('#exitURL').focus();
		$('#exitURL').css("background-color", "#F5E7E1");
	}else if(eMsg==1){
		$('#eMessage').find(".jqte_editor").focus();
		$('#eMessage').find(".jqte_editor").css("background-color", "#F5E7E1");
	}
	
	
	
	//panel check boxes
	var status		=	document.getElementsByName("status").length;
	var secstatus	=	document.getElementsByName("secstatus").length;
	var statusIds	=	"";
	var secStatusIds=	"";

	for(var x=1;x<=status;x=x+1){
		var xx=document.getElementById("status_"+x).checked;
		var idvl=document.getElementById("status_"+x).value;
		if(xx){
			statusIds=statusIds+","+idvl+"#@@#"+true;
		}else{
			statusIds=statusIds+","+idvl+"#@@#"+false;
		}
	}
	statusIds = statusIds.substr(1, statusIds.length);
	for(var x=1;x<=secstatus;x=x+1){
		var xx=document.getElementById("secstatus_"+x).checked;
		var idvl=document.getElementById("secstatus_"+x).value;
		if(xx){
			secStatusIds=secStatusIds+","+idvl+"#@@#"+true;
		}else{
			secStatusIds=secStatusIds+","+idvl+"#@@#"+false;
		}
	}
	secStatusIds = secStatusIds.substr(1, secStatusIds.length);
	
	var writePrivilegeValue	=	0;
	//alert("Reached there...........");
	if(districtHiddenId!=804800 && districtHiddenId!=806900){
		if(refno!=0){
			ManageJobOrdersAjax.findExistingRefNo(refno,jobId,districtHiddenId,{
				async: false,
				callback: function(data_refno){
					 if(data_refno==true){
						document.getElementById("hideSave").style.display='inline';
						$('#loadingDiv').hide();
						$('#errordiv').show();
						$('#errordiv').append("&#149; "+resourceJSON.msgUniqReferenceNoJobCode+"<br>");
						errorCount++;
					}
				},
			});
			$('#loadingDiv').hide();
		}
	}
	if(errorCount==0){
		if(JobOrderType==2 && ncStatusId != 2)
		{
			
			//alert("running on page..."+JobOrderType+"::allSchoolGradeDistrictVal ::::::"+allSchoolGradeDistrictVal);
			if(allSchoolGradeDistrictVal==2 || allSchoolGradeDistrictVal==3)
			{
				if($("#schoolName").is(':visible'))
				{
					var goFlagForSchool=validateAddSchoolwithflag();
					
					if(goFlagForSchool==0)
					{
						schoolIdCount=getTRAndTDForSchoolCount();
					}
					else if(goFlagForSchool==2)
					{
						schoolIdCount=0;
						//$('#loadingDiv').hide();
						$('#loadingDiv').fadeOut();
						return false;
					}
					else if(goFlagForSchool==1)
					{
						schoolIdCount=1;
					}
				}
				else
				{
					schoolIdCount=getTRAndTDForSchoolCount();	
				}
			}
			else
			{
				if(allSchoolGradeDistrictVal==1)
				{
					schoolIdCount=0;
				}
			}
			
	
			var postionStatusId			=	"";
			try{postionStatusId = document.getElementById("postionStatusId").value;
			}catch(e){}
			
			var writePrivilegeToSchool			=	"";
			try{writePrivilegeToSchool	=	document.getElementById("writePrivilegeToSchool");}catch(e){}
			
			if(postionStatusId != 51 && districtHiddenId!=806900){
				
			
					//var writePrivilegeToSchool	=	document.getElementById("writePrivilegeToSchool");
					//alert("++++++++++++++++++++++++++++++++++++++++"+schoolIdCount+"           |       "+schoolIdCount);
					if((districtHiddenId==804800 && isCloneJob==false) || districtHiddenId!=804800)
					if(writePrivilegeToSchool.checked	==	true )//&& allSchoolGradeDistrictVal==2
					{
						if(schoolIdCount ==0)
						{	
							errorCount++;
							try{
								//var divMsg="<div>"+resourceJSON.msgreadwriteprivilege+"</div>";
								var divMsg="<div>"+resourceJSON.msgUnabletosave+"</div>";
								document.getElementById("EPIMsg").innerHTML=divMsg;
								$('#EPIMessage').modal('show');
								$('#loadingDiv').fadeOut();
								return false;
							}catch(err){}
						}else{
							writePrivilegeValue=1;
						}
					}else
					{
						if(schoolIdCount==0 && allSchoolGradeDistrictVal==2)
						{	
							errorCount++;
							try{
								addSchool();
								var divMsg="<div>"+resourceJSON.msgattachedtothisJobOrder+" </div>";
								document.getElementById("EPIMsg").innerHTML=divMsg;
								$('#EPIMessage').modal('show');
								$('#loadingDiv').fadeOut();
								return false;
							}catch(err){}
						}
					}
			}		
			//alert("#######################");
			try
			{	
				var isExpHireNotEqualToReqNo;
					isExpHireNotEqualToReqNo=document.getElementById("isExpHireNotEqualToReqNo");
				if(isExpHireNotEqualToReqNo.checked==true)
					isExpHireNotEqualToReqNoValue=true;
				else
					isExpHireNotEqualToReqNoValue=false;
				
			}
			catch(err)
			{}
		}
	
		document.getElementById("hideSave").style.display='none';
		var schoolIds="";
		var certificationType="";
		try{
			if(getTRAndTDForSchool()!=null){
			schoolIds=getTRAndTDForSchool();
			}
		}catch(err){}
		
		try{
			if(getTRAndTDForDiv()!=null){
				if(document.getElementById("clonejobId").value!=""){
					var certIds = document.getElementsByTagName("label");
					for(i=0;i<certIds.length;i++)
						if(certIds[i].getAttribute("certtypeidcl")!=null && certIds[i].getAttribute("certtypeidcl")!="")
						certificationType+=certIds[i].getAttribute("certtypeidcl")+",";	
				}else{
					var certIds = document.getElementsByName("certificateTypeIds");
					for(i=0;i<certIds.length;i++)
						certificationType+=certIds[i].value+",";
				}			
				
			}
		}catch(err){}
		
		try{
			var jobCategoryVal=jobCategoryId.split("||");
			var jobCategory=jobCategoryVal[0];
			//alert("running on page..."+assessmentDocument+"   "+isJobAssessment+"  "+attachJobAssessment);
			if(assessmentDocument!="" && isJobAssessment==1 &&  attachJobAssessment==2){
					document.getElementById("frmJobAssessmentUpload").submit();
			}else
			{	
				var isProcessNext=true;
				if(jobId!="" && isReqNoFlagByDistrict==1)
				{
					ManageJobOrdersAjax.validateCheckedMultiHireForReqNumber(jobId,isReqNoFlagByDistrict,isExpHireNotEqualToReqNoValue,{
						async: false,
						callback: function(dataHRV)
						{
							 if(dataHRV!=null)
							 {
								if(dataHRV==1)
								{
									document.getElementById("hideSave").style.display='block';
									isProcessNext=false;
									
									$('#finalValidateReqModal').modal('show');
									
									if(isExpHireNotEqualToReqNoValue==1)
										document.getElementById("textfinalValidateSpan").innerHTML="&#149; "+resourceJSON.msgatleastoneRequisitionforonemoreschool+"<br>";
									else if(isExpHireNotEqualToReqNoValue==0)
										document.getElementById("textfinalValidateSpan").innerHTML="&#149; "+resourceJSON.msgprovideRequisitionNumberoneormoreschool+"<br>";
								}
							}
						},
					});
				}
				
		
				if(isProcessNext)
				{
					var hoursPerDay= $("#hourPrday").val();
					
					var clonejobId=0;
					try{
						clonejobId=document.getElementById("clonejobId").value;
					}catch(e){}
					
					/*if(districtId=='804800' && isCloneJob){
						savingDJobEequisitionNumbersTempOnlyJeffcoClone(jobId,clonejobId,reqPositionNum);
					}*/
					//alert("GO TO AddeditJobOrder function call.... "+positionStartDate+" End :"+positionEndDate+" Temp: "+tempType+" Application:"+jobApplicationStatus);
					$('#loadingDiv').show();
									
					ManageJobOrdersAjax.addEditJobOrder(jobStartTime,jobEndTime,jobTimeZoneId,jobCategory,schoolId,jobId,districtHiddenId,
							exitURLMessageVal,exitMessage,exitURL,certificationType,schoolIds,
							JobOrderType,districtOrSchooHiddenlId,jobTitle,refno,jobDescription,jobQualification,
							noOfExpHires,jobStartDate,jobEndDate,positionStartDate,positionEndDate,tempType,jobApplicationStatus,allSchoolGradeDistrictVal,isJobAssessment,offerJSI,
							attachJobAssessment,assessmentDocumentVal,writePrivilegeValue,
							subjectId,jobDescriptionFileName,arrReqNum,arrReqNumAndSchoolId,jobAuthKey,isExpHireNotEqualToReqNoValue,jobType,geoZoneId,statusIds,secStatusIds,rdReqNoSource,newTextRequisitionNumbers,
							offerVirtualVideoInterview,quesId,maxScoreForVVI,sendAutoVVILink,slctStatusID,timePerQues,vviExpDays,isInviteOnly,offerDASMT,arrDAIds,senDnotificationToschools,jobIntNotes,serviceTechnician,salaryRange,additionalDocumentFileName,hiddenJob,hoursPerDay,reqPositionNum,jeffcoSpecialEdFlag,jobIdNC,jeffcoExtraFields,positionStart,schoolTyp,statusActiveOrNot,jobCompletedVVILink,strAdams,positionNumber,salaryAdminPlan,approvalCode,step,jobApprovalProcessId,clonejobId,{ 
						async: true,
						callback: function(data)
						{
						
						
												
						//ManageJobOrdersAjax.saveJobOrderLog(data.split("||")[1],"");
							var dd=data.split("||");
							if(jobId==0){
								$('#loadingDiv').hide();
								document.getElementById("copy_text").value=dd[0];
								var copyURLMsg='';
								if(checkFlash()==true){
									copyURLMsg="<div id='d_clip_container'><div id='d_clip_button' style='width:70px;text-align:center; border:0px solid black; background-color:#FFFFFF; cursor:default;'> "+resourceJSON.msgCopyURL+"</div></div>";
								}
								var divMsg="<div>"+resourceJSON.msgsuccessfullycreatedajoborder+"<br/><br/>"+resourceJSON.msgTeacherMatchJobURLis+": <div id='jobOrderUrl'><a target='blank' href=\""+dd[0]+"\">"+dd[0]+"</a></div><div id='linkDiv' class='span2 pull-right'>"+copyURLMsg+"</div>";
								$('#jobOrderSend').modal('show');
								document.getElementById("jobOrderText").innerHTML=divMsg;
								init_swf();
								return false;
								
							}else{  
							if($("#jobStatus").val()=="I" &&  $("#hrIntegrated").val()!="true"){
									$("#modalUpdateMsgforInactive").modal("show");
							}
							else{
							actionForward();
							}
							}
							
						},
						//errorHandler:handleError 
					});
				}			
			}
			
		}catch(err){}
	}else{
		$('#loadingDiv').hide();
		$('#errordiv').show();
		$('html, body').animate({scrollTop : 0},800);
		return false;
	}
}

//End Gaurav Kumar
function addStatusSecStatus(jobId){
	var status=document.getElementsByName("status").length;
	var secstatus=document.getElementsByName("secstatus").length;
	var statusIds="";
	var secStatusIds="";
	
	for(var x=1;x<=status;x=x+1){
		var xx=document.getElementById("status_"+x).checked;
		var idvl=document.getElementById("status_"+x).value;
		if(xx){
			statusIds=statusIds+","+idvl+"#@@#"+true;
		}else{
			statusIds=statusIds+","+idvl+"#@@#"+false;
		}
	}
	statusIds = statusIds.substr(1, statusIds.length);
	for(var x=1;x<=secstatus;x=x+1){
		var xx=document.getElementById("secstatus_"+x).checked;
		var idvl=document.getElementById("secstatus_"+x).value;
		if(xx){
			secStatusIds=secStatusIds+","+idvl+"#@@#"+true;
		}else{
			secStatusIds=secStatusIds+","+idvl+"#@@#"+false;
		}
	}
	secStatusIds = secStatusIds.substr(1, secStatusIds.length);
	ManageJobOrdersAjax.addStatusSecStatus(jobId,statusIds,secStatusIds,{ 
		async: true,
		callback: function(data){
			
		},
		errorHandler:handleError 
	}); 
}
var jobDescriptionFileUploadErrorCount=0;
function uploadJobDescriptionFile(fileName){
	var ext = fileName.substr(fileName.lastIndexOf('.') + 1).toLowerCase();	
	var districtId=document.getElementById("districtOrSchooHiddenlId").value;
	document.getElementById("disHiddenId").value=districtId;
	var ddisrictId=document.getElementById("disHiddenId").value;
	$("#errordiv").empty();
	$("#jobDescriptionErrordiv").empty();
	
	$('#districtORSchoolName').css("background-color", "");
	$('#jobTitle').css("background-color", "");
	$('#jobStartDate').css("background-color", "");
	$('#jobEndDate').css("background-color", "");
	$('#jobCategoryId').css("background-color", "");
	$('#noOfExpHires').css("background-color", "");	
	if(ext!="pdf")
	{	$('#jobDescriptionErrordiv').show();
		$('#jobDescriptionErrordiv').append("&#149; "+resourceJSON.msgAcceptableJobformatswhichincludePDFfiles+"<br>");
		$('#jobDescriptionDoc').val("");
		$('#jobDescriptionDoc').focus();
		jobDescriptionFileUploadErrorCount++;
	}
	if(districtId==0 && ddisrictId==0)
	{	$('#jobDescriptionErrordiv').show();
		$('#jobDescriptionErrordiv').append("&#149; "+resourceJSON.PlzEtrDistrict+"<br>");
		$('#districtORSchoolName').css("background-color", "#F5E7E1");
		if(jobDescriptionFileUploadErrorCount==0)
		$('#districtORSchoolName').focus();
		$('#jobDescriptionDoc').val("");
		jobDescriptionFileUploadErrorCount++;
	}
	if(jobDescriptionFileUploadErrorCount==0){
		if(fileName!=""){
			$("#loadingDiv").show();
			document.getElementById("jobDescriptionDocUpload").submit();
		}
	}	
}

//jeffco added by additional document
var additionalDocumentFileUploadErrorCount=0;
function uploadAdditionalDocumentFile(fileName){
	var ext = fileName.substr(fileName.lastIndexOf('.') + 1).toLowerCase();	
	var districtId=document.getElementById("districtOrSchooHiddenlId").value;
	document.getElementById("disHiddenId").value=districtId;
	var ddisrictId=document.getElementById("disHiddenId").value;
	$("#errordiv").empty();
	$("#additionalDocumentErrorLabel").empty();
	
	$('#districtORSchoolName').css("background-color", "");
	$('#jobTitle').css("background-color", "");
	$('#jobStartDate').css("background-color", "");
	$('#jobEndDate').css("background-color", "");
	$('#jobCategoryId').css("background-color", "");
	$('#noOfExpHires').css("background-color", "");	
	if(ext!="pdf")
	{	$('#additionalDocumentErrorLabel').show();
		$('#additionalDocumentErrorLabel').append("&#149; Please select Acceptable Additional Document formats which include PDF files.<br>");
		$('#additionalDocumentDoc').val("");
		$('#additionalDocumentDoc').focus();
		additionalDocumentFileUploadErrorCount++;
	}
	if(districtId==0 && ddisrictId==0)
	{	$('#additionalDocumentErrorLabel').show();
		$('#additionalDocumentErrorLabel').append("&#149; "+resourceJSON.PlzEtrDistrict+"<br>");
		$('#districtORSchoolName').css("background-color", "#F5E7E1");
		if(additionalDocumentFileUploadErrorCount==0)
		$('#districtORSchoolName').focus();
		$('#additionalDocumentDoc').val("");
		additionalDocumentFileUploadErrorCount++;
	}
	if(additionalDocumentFileUploadErrorCount==0){
		if(fileName!=""){
			$("#loadingDiv").show();
			document.getElementById("additionalDocumentDocUpload").submit();
		}
	}	
}
//end

var distAttachmentFileUploadErrorCount=0;
function uploadDistFile(fileName){
	var ext = fileName.substr(fileName.lastIndexOf('.') + 1).toLowerCase();
	var districtId=document.getElementById("districtOrSchooHiddenlId").value;
	document.getElementById("disHiddenId").value=districtId;
	var ddisrictId=document.getElementById("disHiddenId").value;
	$("#errordiv").empty();
	$("#jobDescriptionErrordiv").empty();
	
	$('#districtORSchoolName').css("background-color", "");
	$('#jobTitle').css("background-color", "");
	$('#jobStartDate').css("background-color", "");
	$('#jobEndDate').css("background-color", "");
	$('#jobCategoryId').css("background-color", "");
	$('#noOfExpHires').css("background-color", "");
	var districtId=$('#districtOrSchooHiddenlId').val();
	document.getElementById("districtORSchoolNameAdmin").value=districtId;

	if(districtId==0 && ddisrictId==0)
	{	$('#jobDescriptionErrordiv').show();
		$('#jobDescriptionErrordiv').append("&#149; Please enter District<br>");
		$('#districtORSchoolName').css("background-color", "#F5E7E1");
		if(distAttachmentFileUploadErrorCount==0)
		$('#districtORSchoolName').focus();
		$('#jobDescriptionDoc').val("");
		distAttachmentFileUploadErrorCount++;
	}
	if(distAttachmentFileUploadErrorCount==0){
		if(fileName!=""){
			$("#loadingDiv").show();
			document.getElementById("frmDistAttachmentUpload").submit();
		}
	}	
}

var jobDescriptionFileName="";
function saveJobDesciptionFile(fileName){
	$("#loadingDiv").hide();
	var uploadFileName=document.getElementById("uploadedJobDiscriptionFile").value;
	if(uploadFileName==""){
		document.getElementById("uploadedJobDiscriptionFile").value=fileName;
	}else{
		document.getElementById("uploadedJobDiscriptionFile").value=document.getElementById("uploadedJobDiscriptionFile").value+"@##@"+fileName;
	}
	jobDescriptionFileName=fileName;
}


//jeffco added by
var additionalDocumentFileName="";
function saveAdditionalDocumentFile(fileName){
	$("#loadingDiv").hide();
	var uploadFileName=document.getElementById("uploadedAdditionalDocumentFile").value;
	if(uploadFileName==""){
		document.getElementById("uploadedAdditionalDocumentFile").value=fileName;
	}else{
		document.getElementById("uploadedAdditionalDocumentFile").value=document.getElementById("uploadedAdditionalDocumentFile").value+"@##@"+fileName;
	}
	additionalDocumentFileName=fileName;
}
function removeAdditionalDocumentFile(savecancelFlag){
	var fileNames=$('#uploadedAdditionalDocumentFile').val();
	var districtId=$('#districtOrSchooHiddenlId').val();
	if(fileNames!=""){
	ManageJobOrdersAjax.removeJobDescriptionFile(districtId,fileNames,savecancelFlag,{
		async:false,
		callback:function(data){
		},
		errorHandler:handleError
	});
  }
}
//end

var jobDistFileName="";
function saveDistFile(fileName){
	$("#loadingDiv").hide();
	var uploadFileName=document.getElementById("uploadedDistAttachmentFile").value;
	if(uploadFileName==""){
		document.getElementById("uploadedDistAttachmentFile").value=fileName;
	}else{
		document.getElementById("uploadedDistAttachmentFile").value=document.getElementById("uploadedDistAttachmentFile").value+"@##@"+fileName;
	}
	jobDistFileName=fileName;
}

function removeJobDescriptionFile(savecancelFlag){
	var fileNames=$('#uploadedJobDiscriptionFile').val();
	var districtId=$('#districtOrSchooHiddenlId').val();
	if(fileNames!=""){
	ManageJobOrdersAjax.removeJobDescriptionFile(districtId,fileNames,savecancelFlag,{
		async:true,
		callback:function(data){
		},
		errorHandler:handleError
	});
  }
}



function checkFlash(){
	
	function getFlashVersion(desc) {
	    var matches = desc.match(/[\d]+/g);
	    matches.length = 3;  // To standardize IE vs FF
	    return matches.join('.');
	  }

	  var hasFlash = false;
	  var flashVersion = '';

	  if (navigator.plugins && navigator.plugins.length) {
	    var plugin = navigator.plugins['Shockwave Flash'];
	    if (plugin) {
	      hasFlash = true;
	      if (plugin.description) {
	        flashVersion = getFlashVersion(plugin.description);
	      }
	    }

	    if (navigator.plugins['Shockwave Flash 2.0']) {
	      hasFlash = true;
	      flashVersion = '2.0.0.11';
	    }

	  } else if (navigator.mimeTypes && navigator.mimeTypes.length) {
	    var mimeType = navigator.mimeTypes['application/x-shockwave-flash'];
	    hasFlash = mimeType && mimeType.enabledPlugin;
	    if (hasFlash) {
	      flashVersion = getFlashVersion(mimeType.enabledPlugin.description);
	    }

	  } else {
	    try {
	      // Try 7 first, since we know we can use GetVariable with it
	      var ax = new ActiveXObject('ShockwaveFlash.ShockwaveFlash.7');
	      hasFlash = true;
	      flashVersion = getFlashVersion(ax.GetVariable('$version'));
	    } catch (e) {
	      // Try 6 next, some versions are known to crash with GetVariable calls
	      try {
	        var ax = new ActiveXObject('ShockwaveFlash.ShockwaveFlash.6');
	        hasFlash = true;
	        flashVersion = '6.0.21';  // First public version of Flash 6
	      } catch (e) {
	        try {
	          // Try the default activeX
	          var ax = new ActiveXObject('ShockwaveFlash.ShockwaveFlash');
	          hasFlash = true;
	          flashVersion = getFlashVersion(ax.GetVariable('$version'));
	        } catch (e) {
	          // No flash
	        }
	      }
	    }
	  }
	return hasFlash;
}

function showDivMsg(jobCategoryId){
var districtid=	document.getElementById("districtHiddenId").value;
	if(districtid!=806900){
		linkShowOrHide();}
	if(jobCategoryId!=0)
	{
		var n=jobCategoryId.indexOf("||");
		var jobCategoryVal	=	jobCategoryId.split("||");
		var jobCategorId	=	jobCategoryVal[0];
		var jobCategoryEPI	=	jobCategoryVal[1];
		var jobCategoryJSI	=	jobCategoryVal[2];
	
		JobCategoryAjax.getJobCategoryId(jobCategorId,{
			async: true,
			callback: function(data){
					if(data.offerJSI == 0){
						document.getElementById("offerJSIRadioMaindatory").checked	=	true;
					} else if(data.offerJSI == 1){
						document.getElementById("offerJSIRadioMaindatory").checked	=	true;
					} else if(data.offerJSI == 2){
						document.getElementById("offerJSIRadioOptional").checked	=	true;
					}
				},
			});
	
		if(n==-1)
			jobCategoryJSI="";
	
		if ( (jobCategoryJSI!=''))  /* ===== Gagan : Means there is a job category specific JSI =============== */
		{
			document.getElementById("attachJobAssessmentVal").value=4;
			document.getElementById("attachJobAssessment4").checked	=	true;
			$("#jcjsidiv").fadeIn();
			$("#ViewJSIURL").fadeIn();
			document.getElementById("ViewJSIURL").innerHTML="<a href='javascript:void(0)' id='hrefEditRef' onclick=\"downloadJsi('"+jobCategorId+"','hrefEditRef');" +
			"if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0');" +
			"return false;\">view</a>";
			document.getElementById("isJobAssessment2").checked	=	true;
			document.getElementById("isJobAssessmentVal").value=1;
			$("#isJobAssessmentDiv").fadeIn();
			$("#attachJobAssessmentDiv").hide();
		}
		else
		{
			$("#jcjsidiv").hide();
			document.getElementById("isJobAssessment1").checked	=	true;
			document.getElementById("isJobAssessmentVal").value=2;
			$("#isJobAssessmentDiv").hide();
			document.getElementById("attachJobAssessmentVal").value=2;
			document.getElementById("attachJobAssessment3").checked=true;
			$("#attachJobAssessmentDiv").show();
		}
		
		var jobId	=	document.getElementById("jobId").value;
		if(jobCategoryEPI=="false" && jobId==""){
			var divMsg="<div>"+resourceJSON.msganonteachingjobcategory+" "+resourceJSON.msgteachingjobsonly+"</div>";
			$('#EPIMessage').modal('show');
			document.getElementById("EPIMsg").innerHTML=divMsg;
		}
	}
		var districtId=document.getElementById("districtOrSchooHiddenlId").value;
		getStatusList(districtId,jobCategorId);
		getVVIDefaultFields();
		displayAllDistrictAssessment();
		getJobSubcategory();
		if(districtId!=806900)
		joblibrarydescription1();
}
function getJobSubcategory()
{
	var jobcategoryId=document.getElementById("jobCategoryId").value;
	var jobId=document.getElementById("jobId").value;
	var districtId = trim(document.getElementById("districtOrSchooHiddenlId").value);
	var clonejobId = document.getElementById("clonejobId").value;
	var jobCate = jobcategoryId.split("||");
	var jobCateId = 0;
	
	if(document.getElementById("clonejobId").value=="")
		clonejobId = 0;
	
	if(districtId!="")
	{
		if(jobCate[0]!='' && jobCate[0]>0)
			jobCateId = jobCate[0];
		
		var url =window.location.href;
		 if(url.indexOf("editjobordertest.do") > -1){
			 ManageJobOrdersAjax.getJobSubCategory_New_Op(jobId,jobCateId,districtId,clonejobId,
						{ 
							async: true,
							errorHandler:handleError,
							callback: function(data)
							{
						//	alert(data);
								if(data!='')
								{
									$("#jobSubCateDiv").show();
									document.getElementById("jobSubCategoryDiv").innerHTML=data;
								}
								else
								{
									$("#jobSubCateDiv").hide();
								}
								hideEditJobCategoryAndSubCategory();
							}
						}); 
		 }
		
		 else{
			ManageJobOrdersAjax.getJobSubCategory(jobId,jobCateId,districtId,clonejobId,
			{ 
				async: false,
				errorHandler:handleError,
				callback: function(data)
				{
			//	alert(data);
					if(data!='')
					{
						$("#jobSubCateDiv").show();
						document.getElementById("jobSubCategoryDiv").innerHTML=data;
					}
					else
					{
						$("#jobSubCateDiv").hide();
					}
					hideEditJobCategoryAndSubCategory();
				}
			});
		 }
	}
}

//.......................brajesh...............
function joblibrarydescription1(){
	var jobcategoryId = trim(document.getElementById('jobCategoryId').value);
	$('#districtjobdescriptionLibreryname').empty();
	$('#loadingDiv').show();
	$('#districtjobdescriptionLibrery').hide();
	ManageJobOrdersAjax.districtJobDescriptionNmae(jobcategoryId,{ 
		async: true,
		callback: function(data)
		{	//alert("dataaaa  "+data);
					$("#districtjobdescriptionLibreryname").html(data);
					if(data!='no')
						$('#districtjobdescriptionLibrery').show();
					$('#loadingDiv').hide();
		},
	errorHandler:handleError
	});
}



function districtJobDescription(){
	var districtJbDescriptionLibraryId = trim(document.getElementById('jobcategoryfordescriptionId').value);
	$('#jDescription').find(".jqte_editor").empty();
	$('#loadingDiv').show();
	ManageJobOrdersAjax.districtJobDescriptionn(districtJbDescriptionLibraryId,{ 
		async: true,
		callback: function(data)
		{	
					$('#jDescription').find(".jqte_editor").html(data);
					//$(".jqte_editor").html(data);
					$('#loadingDiv').hide();
		},
	errorHandler:handleError
	});
}

//..........................................

/* ======= Gagan : for viewing Job category wise Jsi =======*/
function downloadJsi(jobCategoryId,linkId)
{		
	JobCategoryAjax.downloadJsi(jobCategoryId,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data){
			if(data=="")
			{
				data="javascript:void(0)";
			}
			else if(data.indexOf(".doc")!=-1)
			{
				document.getElementById("ifrmJsi").src=data;
			}else
			{
				document.getElementById(linkId).href = data;
			}
			return false;
		}});
}

function actionForward()
{
	var JobOrderType=document.getElementById("JobOrderType").value;
	if(JobOrderType==2){
		window.location.href="managejoborders.do";
	}else if(JobOrderType==3){
		window.location.href="schooljoborders.do";
	}
}
function ClipBoard() 
{
	var url	=	$('#jobOrderUrl').text();
	try{
		url =  url.substr(0,60);
	}catch(err){}
	ManageJobOrdersAjax.copyToClipBoard(url, { 
		errorHandler:handleError 
	});
}
function saveRecord()
{
	var rdReqNoSource=0;
	if($("input[name=rdReqNoSource]:checked").val()!=undefined)
		rdReqNoSource=$("input[name=rdReqNoSource]:checked").val();
	
	var newTextRequisitionNumbers=trim(document.getElementById("newTextRequisitionNumbers").value);
	var newReqCountFlag=document.getElementById("newReqCountFlag").value;
	var newReqDuplicateFlag=document.getElementById("newReqDuplicateFlag").value;
	var reqPositionNum=$(".reqPositionNum").val();
	
	var jobType=document.getElementById("jobType").value;
	
	var geoZoneId=0;
	var isZoneAvailable=document.getElementById("isZoneAvailable").value;
	geoZoneId=document.getElementById("zone").value;
	
	var JobOrderType				=	document.getElementById("JobOrderType").value;
	var districtOrSchooHiddenlId	=	document.getElementById("districtOrSchooHiddenlId").value;
	var jobTitle					=	trim(document.getElementById("jobTitle").value);
	var refno						=	trim(document.getElementById("refno").value);
	var jobStartDate				=	trim(document.getElementById("jobStartDate").value);
	var positionStartDate			=	trim(document.getElementById("positionStartDate").value);
	var positionEndDate				=	trim(document.getElementById("positionEndDate").value);
	var tempType				    =	document.getElementById("tempType");
	tempType=tempType.options[tempType.selectedIndex].value;
	var jobApplicationStatus	     =	document.getElementById("jobApplicationStatus");
	jobApplicationStatus=jobApplicationStatus.options[jobApplicationStatus.selectedIndex].value;
	var jobEndDate 					= 	"";
	 //alert("tempType :"+tempType+" jobApplicationStatus: "+jobApplicationStatus);
	if ($("#endDateRadio1").prop("checked")) {
		jobEndDate	=	trim(document.getElementById("jobEndDate").value);
	}else if ($("#endDateRadio2").prop("checked")) {
		jobEndDate	=	"12-25-2099";
	
	}
	var districtORSchoolName		=	trim(document.getElementById("districtORSchoolName").value);
	var jobDescription				=	trim($('#jDescription').find(".jqte_editor").html());
	var jobQualification			=	$('#jQualification').find(".jqte_editor").html();
	var jobIntNotes				=	trim($('#jobIntNotes').find(".jqte_editor").html());
	var noOfExpHires				=	trim(document.getElementById("noOfExpHires").value);
	var noOfExpHiresSchool			=	trim(document.getElementById("noOfExpHiresSchool").value);
	var allSchoolGradeDistrictVal	=	trim(document.getElementById("allSchoolGradeDistrictVal").value);
	var isJobAssessment				=	trim(document.getElementById("isJobAssessmentVal").value);
	var attachJobAssessment			=	trim(document.getElementById("attachJobAssessmentVal").value);
	var assessmentDocument			=	document.getElementById("assessmentDocument").value;
	var exitURLMessageVal			=	document.getElementById("exitURLMessageVal").value;
	var exitURL						=	trim(document.getElementById("exitURL").value);
	var exitMessage					=	trim($('#eMessage').find(".jqte_editor").text());
	var districtHiddenId			=	document.getElementById("districtHiddenId").value;
	var jobId						=	document.getElementById("jobId").value;
	var schoolId					=	document.getElementById("schoolId").value;
	var dateTime					=	trim(document.getElementById("dateTime").value);
	var attachJobAssessment1		=	document.getElementById("attachJobAssessment1").checked;
	var attachJobAssessment2		=	document.getElementById("attachJobAssessment2").checked;
	var attachJobAssessment3		=	document.getElementById("attachJobAssessment3").checked;
	var subjectId					=	document.getElementById("subjectId").value;
	var jobCategoryId				=	"";//document.getElementById("jobCategoryId").value;
	
	
	var writePrivilegeValue			=	0;
	
	/*var distAttachmentDocument			=	document.getElementById("distAttachmentDocument").value;
	var uploadedDistAttachmentFile		=	document.getElementById("uploadedDistAttachmentFile").value;
	var uploadedDistAttachmentFileEdit	=	document.getElementById("uploadedDistAttachmentFileEdit").value;
	var distAttachmentDocumentVal 		= 	"";*/
	
	if(document.getElementById("jobSubCategoryId")!=null && document.getElementById("jobSubCategoryId").value!="0")
		jobCategoryId = document.getElementById("jobSubCategoryId").value;
	else
		jobCategoryId = document.getElementById("jobCategoryId").value;
	
	
	var offerJSI;
	if(document.getElementById("isJobAssessment1").checked == true){
		offerJSI = 0;
	} else if(document.getElementById("isJobAssessment2").checked == true){
		
			if(document.getElementById("offerJSIRadioMaindatory").checked == true){
					offerJSI = 1;
				} else if(document.getElementById("offerJSIRadioOptional").checked == true){
					offerJSI = 2;
				}
	}

	
	
	/*if(uploadedDistAttachmentFile != "") {
		distAttachmentDocumentVal 	= 	uploadedDistAttachmentFile;
	} else {
		distAttachmentDocumentVal	=	uploadedDistAttachmentFileEdit;
	}*/

	if(JobOrderType==2)
	{
		writePrivilegeValue =	document.getElementById("writePrivilegeValue").value;
	}
	if(JobOrderType==2 && allSchoolGradeDistrictVal==1 && ( noOfExpHires=="" || noOfExpHires==0)){
	}else if(JobOrderType==3 && ( noOfExpHiresSchool=="" || noOfExpHiresSchool==0)){
		noOfExpHires=noOfExpHiresSchool;
	}else if(JobOrderType==3){
		noOfExpHires=noOfExpHiresSchool;
	}
	
	var arrReqNum	=	"";
	var arrReqNumAndSchoolId	=	"";
	if(allSchoolGradeDistrictVal==1)
	{
		var opts = document.getElementById('attachedList').options;
		for(var i = 0, j = opts.length; i < j; i++)
		{
			arrReqNum+=opts[i].value+"#";
		}
	}

	//panel check boxes
	var status=document.getElementsByName("status").length;
	var secstatus=document.getElementsByName("secstatus").length;
	var statusIds="";
	var secStatusIds="";
	
	for(var x=1;x<=status;x=x+1){
		var xx=document.getElementById("status_"+x).checked;
		var idvl=document.getElementById("status_"+x).value;
		if(xx){
			statusIds=statusIds+","+idvl+"#@@#"+true;
		}else{
			statusIds=statusIds+","+idvl+"#@@#"+false;
		}
	}
	statusIds = statusIds.substr(1, statusIds.length);
	for(var x=1;x<=secstatus;x=x+1){
		var xx=document.getElementById("secstatus_"+x).checked;
		var idvl=document.getElementById("secstatus_"+x).value;
		if(xx){
			secStatusIds=secStatusIds+","+idvl+"#@@#"+true;
		}else{
			secStatusIds=secStatusIds+","+idvl+"#@@#"+false;
		}
	}
	secStatusIds = secStatusIds.substr(1, secStatusIds.length);
	var jobAuthKey = 	$('#jobAuthKey').val();
	var assessmentDocumentVal="";
	var errorCount=0;
	var schoolIds="";
	
	var certificationType="";
	try{
		if(getTRAndTDForSchool()!=null){
		schoolIds=getTRAndTDForSchool();
		}
	}catch(err){}
	
	try{
		if(getTRAndTDForDiv()!=null){
			//certificationType=getTRAndTDForDiv();

			if(document.getElementById("clonejobId").value!=""){
				var certIds = document.getElementsByTagName("label");
				for(i=0;i<certIds.length;i++)
					if(certIds[i].getAttribute("certtypeidcl")!=null && certIds[i].getAttribute("certtypeidcl")!="")
					certificationType+=certIds[i].getAttribute("certtypeidcl")+",";	
			}else{
				var certIds = document.getElementsByName("certificateTypeIds");
				for(i=0;i<certIds.length;i++)
					certificationType+=certIds[i].value+",";
			}			
			
		
		}
	}catch(err){}
	
	if(assessmentDocument!="" && isJobAssessment==1 &&  attachJobAssessment==2)
	{
		var ext = assessmentDocument.substr(assessmentDocument.lastIndexOf('.') + 1).toLowerCase();	
		assessmentDocumentVal="Inventory"+dateTime+"."+ext;
	}
	var jobCategoryVal=jobCategoryId.split("||");
	var jobCategory=jobCategoryVal[0];
	$('#loadingDiv').show();
	try
	{
		var isExpHireNotEqualToReqNo=document.getElementById("isExpHireNotEqualToReqNo");
		if(isExpHireNotEqualToReqNo.checked==true)
			isExpHireNotEqualToReqNoValue=true;
		else
			isExpHireNotEqualToReqNoValue=false;
	}
	catch(err)
	{}
	var disIdforjeffco	=	$('#districtOrSchooHiddenlId').val();
	if(disIdforjeffco=='804800'){
	   var jeffcoSpecialEdFlag=document.getElementById("jeffcoSpecialEdFlag").value;
	}
	var schoolTyp="";
	if(disIdforjeffco=='7800047'){
		 schoolTyp           =   trim(document.getElementById("selSclTyTId").value);
	}
	var hiddenJob  =	$('input:radio[name=hiddenJob]:checked').val();
	var refno						=	trim(document.getElementById("refno").value);
	var jobStartDate				=	trim(document.getElementById("jobStartDate").value);
	
	var positionStart                   = "";
	//alert($('#positionStart').length);
	if($('#positionStart').length)
	  positionStart        			    =   trim(document.getElementById("positionStart").value);
	
	var offerVirtualVideoInterview="";
	var quesId="";
	var quesName="";
	var maxScoreForVVI="";
	var sendAutoVVILink="";
	var slctStatusID="";
	var timePerQues="";
	var vviExpDays="";
	var jobCompletedVVILink="";
	var jobCompletedVVILink1="";
	var jobCompletedVVILink2="";
	
	var serviceTechnician			=	trim(document.getElementById("primaryESTechnician").value);
	var salaryRange					=	trim(document.getElementById("salaryRange").value);
	var offerDASMT 		= 	document.getElementById("offerAssessmentInviteOnly").checked;
	var arrDAIds = "";
	
	if(offerDASMT)
	{
	//	distASMTId	=	trim(document.getElementById("assessmentId").value);		
		opts = document.getElementById('attachedDAList').options;		
		for(var i = 0, j = opts.length; i < j; i++)
		{
			arrDAIds+=opts[i].value+"#";
		}
	}
	
	offerVirtualVideoInterview=document.getElementById('offerVirtualVideoInterview').checked;
	
	var isInviteOnly="";
	isInviteOnly=document.getElementById('isInviteOnly').checked;
	var senDnotificationToschools = document.getElementById('isSendNotification').checked;
	if(offerVirtualVideoInterview==true)
	{
		wantScore=document.getElementById('wantScore').checked;
		sendAutoVVILink=document.getElementById('sendAutoVVILink').checked;
		timePerQues=trim(document.getElementById('timePerQues').value);
		vviExpDays=trim(document.getElementById('vviExpDays').value);
		quesId=trim(document.getElementById('quesId').value);
		quesName=trim(document.getElementById('quesName').value);
		if(wantScore==true)
		{
			maxScoreForVVI=trim(document.getElementById('maxScoreForVVI').value);
		}
		if(sendAutoVVILink==true){
			jobCompletedVVILink1=document.getElementById('jobCompletedVVILink1').checked;
			jobCompletedVVILink2=document.getElementById('jobCompletedVVILink2').checked;
			
			if(jobCompletedVVILink1==true)
				jobCompletedVVILink = false;
			else if(jobCompletedVVILink2==true)
				jobCompletedVVILink = true;
			
			if(jobCompletedVVILink==false)
				slctStatusID=trim(document.getElementById('slctStatusID').value);
		}
			
	}
	
	//jeffco 
	var districtId	=	$('#districtOrSchooHiddenlId').val();
	var jeffcoExtraFields=new Array();
	if(districtId=='804800'){
		var fte=$('[name="fte"]').val();
		var paygrade=$('[name="paygrade"]').val();
		var daysworked=$('[name="daysworked"]').val();
		var fsalary=$('[name="fsalary"]').val();
		var fsalaryA=$('[name="fsalaryA"]').val();
		var ssalary=$('[name="ssalary"]').val();
		jeffcoExtraFields.push(fte);
		jeffcoExtraFields.push(paygrade);
		jeffcoExtraFields.push(daysworked);
		jeffcoExtraFields.push(fsalary);
		jeffcoExtraFields.push(fsalaryA);
		jeffcoExtraFields.push(ssalary);
	}
	
	
	//Admam 12 Gaurav Kumar
	
	var positionNumber="";
	var strAdams =  "";
	var salaryAdminPlan = "";
	var approvalCode = "";
	var step = "";
	var jobApprovalProcessId="";
	
	if(districtId=='806900'){
		var adamsExtraFields=new Array();
		var fte=$('#fteforAdams').val();
		var paygrade=$('#paygrade').val();
		var monthlySalary = $('[name="monthlySalary"]').val();
		var accountCode   = $('[name="accountCode"]').val();
		var positionType  = $('[name="positionType"]').val();
		var monthsPerYear = $('[name="monthsPerYear"]').val();
	//	var departmentApproval = $('[name="departmentApproval"]').val();
		positionNumber     = $("#positionNumber").val();
		var hourPrday      = $("#hourPrday").val();
		salaryAdminPlan    = $("#salaryAdminPlan").val();
		approvalCode	   = $("#approvalCode").val();
		step   			   = $("#step").val();
		
		jobApprovalProcessId = $("#jobApprovalProcessId").val();
		
		adamsExtraFields[0] = fte;
		adamsExtraFields[1] = paygrade;
		adamsExtraFields[2] = monthlySalary;
		adamsExtraFields[3] = accountCode;
		adamsExtraFields[4] = positionType;
		adamsExtraFields[5] = monthsPerYear;
		strAdams = adamsExtraFields.join("~");
	}

	
	//alert("Calling function");
		var jobIdNC=0;
		var senDnotificationToschools = document.getElementById('isSendNotification').checked;
		var hoursPerDay= $("#hourPrday").val();
		var statusActiveOrNot="";
		try{
			statusActiveOrNot=document.getElementById("statusActiveOrNot").value;
		}catch(err){}
		var clonejobId=0;
		try{
			clonejobId=document.getElementById("clonejobId").value;
		}catch(e){}

	var jobStartTime='';
	var jobEndTime='';
	var jobTimeZoneId='';
	
	if($("#jobStartTime").val()!=undefined)
	jobStartTime=$("#jobStartTime").val();
	if($("#jobEndTime").val()!=undefined)
	jobEndTime=$("#jobEndTime").val();
	if($("#jobtimezone").val()!=undefined)
	jobTimeZoneId=$("#jobtimezone").val();

	$('#loadingDiv').show();
		ManageJobOrdersAjax.addEditJobOrder(jobStartTime,jobEndTime,jobTimeZoneId,jobCategory,schoolId,jobId,districtHiddenId,
				exitURLMessageVal,exitMessage,exitURL,certificationType+"",schoolIds+"",
				JobOrderType,districtOrSchooHiddenlId,jobTitle,refno,jobDescription,jobQualification,
				noOfExpHires,jobStartDate,jobEndDate,positionStartDate,positionEndDate,tempType,jobApplicationStatus,allSchoolGradeDistrictVal,isJobAssessment,offerJSI,
				attachJobAssessment,assessmentDocumentVal,writePrivilegeValue,
				subjectId,jobDescriptionFileName,arrReqNum,arrReqNumAndSchoolId,jobAuthKey,isExpHireNotEqualToReqNoValue,jobType,geoZoneId,statusIds,secStatusIds,rdReqNoSource,newTextRequisitionNumbers,
				offerVirtualVideoInterview,quesId,maxScoreForVVI,sendAutoVVILink,slctStatusID,timePerQues,vviExpDays,isInviteOnly,offerDASMT,arrDAIds,senDnotificationToschools,jobIntNotes,serviceTechnician,salaryRange,additionalDocumentFileName,hiddenJob,hoursPerDay,reqPositionNum,jeffcoSpecialEdFlag,jobIdNC,jeffcoExtraFields,positionStart,schoolTyp,statusActiveOrNot,jobCompletedVVILink,strAdams,positionNumber,salaryAdminPlan,approvalCode,step,jobApprovalProcessId,clonejobId,{ 
			async: true,
			callback: function(data)
			{		
				var dd=data.split("||");
				if(jobId==0){
				$('#loadingDiv').hide();
				document.getElementById("copy_text").value=dd[0];
				var copyURLMsg='';
				if(checkFlash()==true){
					copyURLMsg="<div id='d_clip_container'><div id='d_clip_button' style='width:70px;text-align:center; border:0px solid black; background-color:#FFFFFF; cursor:default;'> "+resourceJSON.msgCopyURL+"</div></div>";
				}
				var divMsg="<div>"+resourceJSON.msgsuccessfullycreatedajoborder+"<br/><br/>"+resourceJSON.msgTeacherMatchJobURLis+": <div id='jobOrderUrl'><a target='blank' href=\""+dd[0]+"\">"+dd[0]+"</a></div><div id='linkDiv' class='span2 pull-right'>"+copyURLMsg+"</div>";
				$('#jobOrderSend').modal('show');
				document.getElementById("jobOrderText").innerHTML=divMsg;
				init_swf();
				return false;
			}else{
				actionForward();
			}
		},
		errorHandler:handleError
	});
	$('#errordiv').hide();
}

function getTRAndTDForDiv(){
	var idArr = [];
	var trs =document.getElementsByTagName('div');
	for(var i=0;i<trs.length;i++){
		var ids=trs[i].id;
		if(trs[i].style.display!='none' && i!=0 && ids.indexOf("CertificationGrid") !=-1){
			var lableText=document.getElementById(trs[i].id).innerHTML;
				lableText=lableText.substr(7,lableText.indexOf("<a")-7);
				idArr.push(lableText.replace(',','<|>'));
		}
	}
	return idArr;
}
function getCertificationTypeCheck(certificationType){
	var certificateTypeTexts =document.getElementsByName('certificateTypeText');
	var isIdExist =false;
	try{
		
		for(var i=0;i<certificateTypeTexts.length;i++){
				var lableText=trim(certificateTypeTexts[i].innerText);
				if(lableText===certificationType){
					isIdExist=true;
				}
			}
	}catch(err){ }
	return isIdExist;
}
function getTRAndTDForSchool(){
	var idArr = [];
	var trs =document.getElementById("schoolTable").getElementsByTagName('TR'); 
	for(var i=0;i<trs.length;i++){
		var tdchk=trs[i].getElementsByTagName('TD');
		if(trs[i].style.display!='none' && i!=0){
			var getId=trs[i].id.split("_");
			idArr.push(getId[0]+"-"+tdchk[2].innerHTML);
		}
	}
	return idArr;
}
function getTRAndTDForSchoolCount(){
	var idArr =0;
	try{
		
	var trs =document.getElementById("schoolTable").getElementsByTagName('TR'); 
	
	for(var i=0;i<trs.length;i++){
		var tdchk=trs[i].getElementsByTagName('TD');
		if(trs[i].style.display!='none' && i!=0){
			idArr++;
		}
	}
	}catch(e){}
	return idArr;
}
function getSchoolIdCheck(schoolId){
	var isIdExist =false;
	try{
	var trs =document.getElementById("schoolTable").getElementsByTagName('TR'); 
	for(var i=0;i<trs.length;i++){
		var tdchk=trs[i].getElementsByTagName('TD');
		if(trs[i].style.display!='none' && i!=0){
			var getId=trs[i].id.split("_");
			if(getId[0]==schoolId){
				isIdExist=true;
			}
		}
	}
	}catch(err){}
	return isIdExist;
}
function removeCertification(val){
	document.getElementById("CertificationGrid"+val).innerHTML='';
	document.getElementById("CertificationGrid"+val).style.display='none';
}

function validateAddCertification()
{	
	var statemaster = {stateId:dwr.util.getValue("stateMaster")};
	var certType=trim(document.getElementById("certType").value);
	var certTypeId=trim(document.getElementById("certificateTypeMaster").value);
	var certNameCheck=false;
	if(certType!=""){
		certNameCheck=getCertificationTypeCheck(certType);
	}
	$('#errordiv').empty();
	if(statemaster.stateId==0){
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.msgState+"<br>");
		$('#stateMaster').focus();
	}
	if(certType==''){
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.msgentercertificatename+"<br>");
		$('#certType').focus();
	}else if(certTypeId==''){
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.msgvalidCertificationLicensureName+"<br>");
		$('#certType').focus();
	}else if(certNameCheck){
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.msgcannotaddduplicateCertification1+"<br>");
		$('#certType').focus();
	}else{
		var jobId=document.getElementById("jobId").value;
		if(certType!="" && jobId==''){
			certificationDivCount++;
			document.getElementById("showCertificationDiv").innerHTML	+=	"<div class=\"\" id=\"CertificationGrid"+certificationDivCount+"\" nowrap><input type=\"hidden\" name=\"certificateTypeIds\" id=\"certificateTypeId"+certificationDivCount+"\" value=\""+certTypeId+"\" ><label certtypeidcl=\""+certTypeId+"\" name=\"certificateTypeText\">"+certType+" <a href=\"javascript:void(0);\" onclick=\"removeCertification("+certificationDivCount+");\"><img width=\"15\" height=\"15\"  class=\"can\" src=\"images/can-icon.png\"></a></label></div>";
		}else if(jobId!=''){
			ManageJobOrdersAjax.addCertification(jobId,certTypeId,statemaster.stateId,{ 
				async: true,
				callback: function(data)
					{
						if(data>0){
							$('#errordiv').show();
							$('#errordiv').append("&#149; "+resourceJSON.msgcannotaddduplicateCertification1+"<br>");
							$('#certType').focus();
						}else{
							DisplayCertification();
							$("#addCertificationDiv").hide();
						}
					},
					errorHandler:handleError 
					});
				}
			
		document.getElementById("certType").value="";
	}
}

var updateRow=0;
var editColId=0;
function editReqNumber(schoolId,SchoolName,noOfSchoolExpHires,editRowId)
{	var listReq;
	updateRow=1
	editColId=editRowId;
	$("#addSchoolDiv").fadeIn();
	$('#attachedListSchool option').remove();
	$("#reqNumberSchoolDiv").fadeIn();
	$("#schoolId").val(schoolId);
	$("#schoolName").val(SchoolName);
	$('#schoolName').removeAttr("disabled");
	$("#noOfSchoolExpHires").val(noOfSchoolExpHires);
	$('#attachedListSchool').append($("#editOption").val());
	var jobId=$("#jobId").val();
	if(jobId!=0){
		listReq=document.getElementById(editRowId).value;
	}else{
		var row=document.getElementById(editRowId);
		listReq=row.cells[5].childNodes[0].data;
	} 
	
	ManageJobOrdersAjax.getRequistionNos(listReq,{ 
		async: true,
		callback: function(data){
			$('#attachedListSchool option').remove();
			$('#attachedListSchool').append(data);
		},
		errorHandler:handleError 
	}); 
}
function editSchoolInJobOrder(jobId,schoolId){
	
	$("#rdReqNoSourceSchool1" ).prop( "disabled", true );
	$("#newTextRequisitionNumbersSchool" ).prop( "disabled", true );
	
	$("#rdReqNoSourceSchool2" ).prop( "disabled", false );
	$("#rdReqNoSourceSchool2" ).prop( "checked", true );
	
	updateRow=1;
	$("#addSchoolDiv").fadeIn();
	$('#schoolId').val("");
	$('#schoolName').val("");
	$('#noOfSchoolExpHires').val("");
	
	$('#schoolName').css("background-color", "");
	$('#noOfSchoolExpHires').css("background-color", "");
	$('#attachedListSchool').css("background-color", "");
	
	$('#attachedListSchool option').remove();
	ManageJobOrdersAjax.editSchoolInJobOrder(jobId,schoolId,{ 
		async: true,
		callback: function(data){
			var s0=data.split("###");
			var s1=s0[0].split("##");
			var s2=s1[0].split("!!");
			$('#schoolId').val(s2[0]);
			$('#schoolName').val(s2[1]);
			$('#schoolName').attr("disabled", "disabled");
			$('#noOfSchoolExpHires').val(s1[1]);
			$('#attachedListSchool').append(s0[1]);
			$('#deleteAvailableRow').val(schoolId);
		},
		errorHandler:handleError 
	}); 
	
}
function getRequisiontForJobOrder(){
	var jobId=$("#jobId").val();
	var schoolId=0;
	if(jobId!=""){
		ManageJobOrdersAjax.editSchoolInJobOrder(jobId,schoolId,{ 
			async: true,
			callback: function(data){
				$('#tempList').append(data);
				$('#avlbList').append(data);
				$('#avlbListSchool').append(data);
				$("#tempList option").remove();
				sortSelectList();
				$('#showSchoolDiv').html("");
				$('#schoolRecords').html("");
				
			},
			errorHandler:handleError 
		});
	}
}

function getRequistionNoByDID(){
	var districtOrSchooHiddenlId=$("#districtOrSchooHiddenlId").val();
	
	if(jobId!=""){
		ManageJobOrdersAjax.getRequistionNoByDID(districtOrSchooHiddenlId,{ 
			async: true,
			callback: function(data)
			{
				$('#avlbListSchool').html(data);
			},
			errorHandler:handleError 
		});
	}
}

var addNewSchoolFlagForExpertRequistionHire=0;
function addSchool(){
	
	var districtReqNumFlag = $('#districtReqNumFlag').val();
	var districtReqHireFlag = $('#districtReqHireFlag').val();
	
	$("#rdReqNoSourceSchool1" ).prop( "disabled", false );
	$("#newTextRequisitionNumbersSchool" ).prop( "disabled", false );
	$("#rdReqNoSourceSchool2" ).prop( "disabled", false );
	$("#rdReqNoSourceSchool2" ).prop( "checked", true );
	
	try{
		document.getElementById("reqNumberDiv").style.display='inline';
	}catch(err){}

	try{
		
		if(districtReqNumFlag == "true" || districtReqHireFlag == "true")
		{
				
			$("#hiderequisitionnumbeForAddSchool").show();
		}
		
		else
		{
		
			$("#hiderequisitionnumbeForAddSchool").hide();
		}
		
		document.getElementById("reqNumberForSchoolDiv").style.display='inline';
	}catch(err){}
	
	try{
		
	}catch(err){}
	updateRow=0;
	addNewSchoolFlagForExpertRequistionHire=1;
	$('#errordiv').empty();
	$("#addSchoolDiv").fadeIn();
	$('#schoolId').val("");
	$('#schoolName').val("");
	$('#schoolName').focus();
	$('#schoolName').css("background-color", "");
	$('#noOfSchoolExpHires').val("");
	$('#schoolName').removeAttr("disabled");
	$("#attachedListSchool option").remove();
	
	var districtReqFlag = $('#districtReqFlag').val();
	var jobId = $('#jobId').val();
	
	if(districtReqFlag==1)
	{
		if(jobId!=null && jobId > 0)
		{
			getRequistionNoByDID();
		}
		else
		{
			getRequistionNoByDIDTemp();
		}
	}
	
	var currentSchoolId = $('#currentSchoolId').val();
    var currentSchoolExpectedHires =	$('#currentSchoolExpectedHires').val();
    var arrReqNumforSchool = $('#arrReqNumforSchool').val();
    var isReqNoFlagByDistrict =	$("#isReqNoFlagByDistrict").val();
    var arrReqNumValforSchool=0;
    var arraySchoolGrid  = "";
    var districtORSchoolName	=	trim(document.getElementById("districtORSchoolName").value);
    var districtOrSchooHiddenlId	=	document.getElementById("districtOrSchooHiddenlId").value;
    
    $('#newTextRequisitionNumbersSchool').val("");
    $("#rdReqNoSourceSchool2" ).prop( "checked", true );
    
    var focusCount=0;
    var errorCount=0;
   
    if(districtORSchoolName=="" && districtOrSchooHiddenlId==""){
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.PlzEtrDistrict+"<br>");
		if(focusCount==0)
		$('#districtORSchoolName').focus();
		$('#districtORSchoolName').css("background-color", "#F5E7E1");
		errorCount++;
		focusCount++;
	}else if(districtOrSchooHiddenlId==""){
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.msgvaliddistrict+"<br>");
		if(focusCount==0)
		$('#districtORSchoolName').focus();
		$('#districtORSchoolName').css("background-color", "#F5E7E1");
		errorCount++;
		focusCount++;
	}
}

var schoolDivValTD	=	"";
var schoolDivVal	=	"";
var tempTable		=	"";
var tempTableTD		=	"";
var tempTableFlag	=	0;
var tableFlag		=	0;
var tempRowNo		=	0;

function validateAddSchool()
{
	
	var rdReqNoSourceSchool=0;
	if($("input[name=rdReqNoSourceSchool]:checked").val()!=undefined)
		rdReqNoSourceSchool=$("input[name=rdReqNoSourceSchool]:checked").val();
	var newTextRequisitionNumbersSchool=trim(document.getElementById("newTextRequisitionNumbersSchool").value);
	var newReqCountFlag=document.getElementById("newReqCountFlag").value;
	var newReqDuplicateFlag=document.getElementById("newReqDuplicateFlag").value;
	var errorCount=0;
	var schoolName=trim(document.getElementById("schoolName").value);
	var noOfSchoolExpHires=trim(document.getElementById("noOfSchoolExpHires").value);
	var schoolId=document.getElementById("schoolId").value;
	var isReqNoFlagByDistrict =$("#isReqNoFlagByDistrict").val();
	var errorCounter=0;
	var focusCounter=0;
	
	if(schoolId!=""){

	}
	
	var arrReqNum	=	"";
	var arrReqNumAndSchoolId	=	"";
	
	var arrReqNumIdText	="";
	var opts = document.getElementById('attachedListSchool').options;
	if(allSchoolGradeDistrictVal==2)
	{
		arrReqNum	=	trim(document.getElementById("arrTotalReqNumforSchool").value);
		arrReqNumAndSchoolId	=	trim(document.getElementById("arrTotalReqNumAndSchoolId").value);
	}else{
		for(var i = 0, j = opts.length; i < j; i++)
		{
			arrReqNum+=opts[i].value+"#";
		}
		for(var i = 0, j = opts.length; i < j; i++)
		{
			arrReqNumIdText+="<option value='"+opts[i].value+"'>"+opts[i].text+"</option>";
		}
	}
	var schoolListIds=document.getElementById("requiSchoolIdsForHire").value;
	if(schoolListIds.length > 0)
	{
		schoolListIds=schoolListIds.split(",");
		var checkAvailableSchool=false;
		for(var i=0;i<schoolListIds.length;i++){
			//alert(schoolId+" == "+schoolListIds[i]);
			if(schoolId==schoolListIds[i])
			{
				var checkAvailableSchool=true;
				errorCount++;
				break;
			}
		}
	}

	$("#editOption").val(arrReqNumIdText);
	arrReqNumIdText=arrReqNumIdText.substr(0,arrReqNum.length-1);

	$('#schoolName').css("background-color", "");
	$('#noOfSchoolExpHires').css("background-color", "");
	$('#attachedListSchool').css("background-color", "");
	$('#errordiv').empty();
	
	if(updateRow==0){
		if(document.getElementById("schoolRecords").innerHTML!=""){ // change By Alpha
			var schoolTable = document.getElementById("schoolTable");
			var schoolTableRowCount = $('#schoolTable tr').length;
			if(schoolTableRowCount > 1)
			{
				for (var i = 1, row; row = schoolTable.rows[i]; i++) {
					  if(schoolName==row.cells[0].innerText){
						  $('#errordiv').show();
							$('#errordiv').append("&#149; "+resourceJSON.magcannotaddexistingSchool+"<br>");
							if(focusCounter==0)
								$('#schoolName').focus();
							$('#schoolName').css("background-color", "#F5E7E1");
							errorCounter++;
							focusCounter++;
					  }
			      }
			  }
		  }
	}
	if(schoolName==''){
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.msgPleaseenterSchoolName+"<br>");
		if(focusCounter==0)
			$('#schoolName').focus();
		$('#schoolName').css("background-color", "#F5E7E1");
		errorCounter++;
		focusCounter++;
	}else if(schoolId==''){
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.msgentervalidSchoolName+"<br>");
		if(focusCounter==0)
			$('#schoolName').focus();
		$('#schoolName').css("background-color", "#F5E7E1");
		errorCounter++;
		focusCounter++;
	}if(updateRow==0){
		if(checkAvailableSchool){
			$('#errordiv').show();
			$('#errordiv').append("&#149; "+resourceJSON.magcannotaddexistingSchool+"<br>");
			if(focusCounter==0)
				$('#schoolName').focus();
			$('#schoolName').css("background-color", "#F5E7E1");
			errorCounter++;
			focusCounter++;
		}
	}if(noOfSchoolExpHires==''){
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.msgExpectedHire+"<br>");
		if(focusCounter==0)
			$('#noOfSchoolExpHires').focus();
		$('#noOfSchoolExpHires').css("background-color", "#F5E7E1");
		focusCounter++;
		errorCounter++;
	}
	
	if(document.getElementById("isReqNoRequired").value=="true"){
		if(document.getElementById("isExpHireNotEqualToReqNo").checked==true){
			
			if(rdReqNoSourceSchool==1)
			{
				if(newReqCountFlag > noOfSchoolExpHires)
				{
					$('#errordiv').show();
					$('#errordiv').append("&#149; "+resourceJSON.msgNumberofEnterRequisitionNumber+"<br>");
					errorCounter++;
				}
				if(newReqCountFlag == 0)
				{
					$('#errordiv').show();
					$('#errordiv').append("&#149; "+resourceJSON.msgPleaseprovideatleastoneRequisitionNumber+"<br>");
					$('#newTextRequisitionNumbersSchool').css("background-color", "#F5E7E1");
					if(focusCounter==0)
						$('#newTextRequisitionNumbersSchool').focus();
					errorCounter++;
					focusCounter++;
				}
			}
			else if(rdReqNoSourceSchool==2)
			{
				if(noOfSchoolExpHires<opts.length){
					$('#errordiv').show();
					$('#errordiv').append("&#149; "+resourceJSON.msgPleaseprovideRequisitionNumber+"<br>");
					$('#attachedListSchool').css("background-color", "#F5E7E1");
					if(focusCounter==0)
						$('#attachedListSchool').focus();
					errorCounter++;
					focusCounter++;
				}else if(opts.length==0){
					$('#errordiv').show();
					$('#errordiv').append("&#149; "+resourceJSON.msgPleaseprovideatleastoneRequisitionNumber+"<br>");
					$('#attachedListSchool').css("background-color", "#F5E7E1");
					if(focusCounter==0)
						$('#attachedListSchool').focus();
					errorCounter++;
					focusCounter++;
				}
			}
			
		}else{
			if(rdReqNoSourceSchool==1)
			{
				if(newReqCountFlag > noOfSchoolExpHires)
				{
					$('#errordiv').show();
					$('#errordiv').append("&#149; "+resourceJSON.msgNumberofEnterRequisitionNumber+"<br>");
					errorCounter++;
				}
				if(newReqCountFlag != noOfSchoolExpHires)
				{
					$('#errordiv').show();
					$('#errordiv').append("&#149; "+resourceJSON.msgPleaseprovideRequisitionNumber+"<br>");
					$('#newTextRequisitionNumbersSchool').css("background-color", "#F5E7E1");
					if(focusCounter==0)
						$('#newTextRequisitionNumbersSchool').focus();
					errorCounter++;
					focusCounter++;
				}
			}
			else if(rdReqNoSourceSchool==2)
			{
				if(opts.length!=noOfSchoolExpHires || opts.length==0)
				{
					$('#errordiv').show();
					$('#errordiv').append("&#149; "+resourceJSON.msgPleaseprovideRequisitionNumber+"<br>");
					$('#attachedListSchool').css("background-color", "#F5E7E1");
					if(focusCounter==0)
						$('#attachedListSchool').focus();
					if(opts.length>0){
					}
					errorCounter++;
					focusCounter++;
				}
			}
		}
	}
	else
	{
		if(rdReqNoSourceSchool==1)
		{
			if(newReqCountFlag > noOfSchoolExpHires)
			{
				$('#errordiv').show();
				$('#errordiv').append("&#149; "+resourceJSON.msgNumberofEnterRequisitionNumber+"<br>");
				$('#newTextRequisitionNumbersSchool').css("background-color", "#F5E7E1");
				errorCounter++;
			}
		}
		else if(rdReqNoSourceSchool==2)
		{
			if(opts.length > noOfSchoolExpHires)
			{
				$('#errordiv').show();
				$('#errordiv').append("&#149; "+resourceJSON.msgRequisitionNumbershould+"<br>");
				$('#attachedListSchool').css("background-color", "#F5E7E1");
				if(focusCounter==0)
					$('#attachedListSchool').focus();
				if(opts.length>0){
				}
				errorCounter++;
				focusCounter++;
			}
		}
	}
		if(errorCounter==0)
		{
		//$('#loadingSchoolRowDiv').show();
		arrReqNum=arrReqNum.substr(0,arrReqNum.length-1);
		var jobId=document.getElementById("jobId").value;
		if(schoolName!="" && jobId=='')
		{
				var newreqtext=rdReqNoSourceSchool+"=="+newTextRequisitionNumbersSchool;
				var arrReqNumAndSchoolId_temp11=newreqtext+"%%"+schoolId+"!!"+noOfSchoolExpHires+"@@"+arrReqNum+"##";
				ManageJobOrdersAjax.saveJobEequisitionNumbersTemp(arrReqNumAndSchoolId_temp11,{ 
					async: false,
					callback: function(data)
					{
						ShowSchoolTemp();
					},
					errorHandler:handleError 
				});
			}
		else if(schoolName!="" && jobId!='')
		{
				var arrReqNumAndSchoolId_temp=schoolId+"!!"+noOfSchoolExpHires+"@@"+arrReqNum+"##";
				ManageJobOrdersAjax.saveJobEequisitionNumbers(jobId,arrReqNumAndSchoolId_temp,rdReqNoSourceSchool,newTextRequisitionNumbersSchool,{ 
					async: false,
					callback: function(data)
					{
						ShowSchool();
						if(data!=0 && $('#entityType').val()==3 && $('#rdReqNoSourceSchool1').is(':checked'))
						{
							$("#attachedListSchool option").each(function()
							{
								$('#avlbListSchool').append("<option value="+ $(this).val() +">" + $(this).text() + "</option>");
							});
							$("#attachedListSchool option").remove();
							$('#attachedListSchool').append(data);
						}
					},
					errorHandler:handleError 
				});
			
			$("#reqNumberSchoolDiv").fadeIn();
			schoolDivVal="";
			schoolDivValTD="";
		}
	//$('#loadingSchoolRowDiv').hide();
	if($('#entityType').val()!=3)
		$("#attachedListSchool option").remove();	
	document.getElementById("schoolName").value="";
	document.getElementById("noOfSchoolExpHires").value="";
	document.getElementById("schoolId").value="";
	addIdIntoSchoolList();
	cancelSchool();
	}
}

function validateAddSchoolwithflag()
{
	var goFlagForSchool = 0;
	
	var rdReqNoSourceSchool=0;
	if($("input[name=rdReqNoSourceSchool]:checked").val()!=undefined)
		rdReqNoSourceSchool=$("input[name=rdReqNoSourceSchool]:checked").val();
	var newTextRequisitionNumbersSchool=trim(document.getElementById("newTextRequisitionNumbersSchool").value);
	var newReqCountFlag=document.getElementById("newReqCountFlag").value;
	var newReqDuplicateFlag=document.getElementById("newReqDuplicateFlag").value;
	var errorCount=0;
	var schoolName=trim(document.getElementById("schoolName").value);
	var noOfSchoolExpHires=trim(document.getElementById("noOfSchoolExpHires").value);
	var schoolId=document.getElementById("schoolId").value;
	var isReqNoFlagByDistrict =$("#isReqNoFlagByDistrict").val();
	var errorCounter=0;
	var focusCounter=0;
	
	if(schoolId!=""){

	}
	
	var arrReqNum	=	"";
	var arrReqNumAndSchoolId	=	"";
	
	var arrReqNumIdText	="";
	var opts = document.getElementById('attachedListSchool').options;
	if(allSchoolGradeDistrictVal==2)
	{
		arrReqNum	=	trim(document.getElementById("arrTotalReqNumforSchool").value);
		arrReqNumAndSchoolId	=	trim(document.getElementById("arrTotalReqNumAndSchoolId").value);
	}else{
		for(var i = 0, j = opts.length; i < j; i++)
		{
			arrReqNum+=opts[i].value+"#";
		}
		for(var i = 0, j = opts.length; i < j; i++)
		{
			arrReqNumIdText+="<option value='"+opts[i].value+"'>"+opts[i].text+"</option>";
		}
	}
	var schoolListIds=document.getElementById("requiSchoolIdsForHire").value;
	if(schoolListIds.length > 0)
	{
		schoolListIds=schoolListIds.split(",");
		var checkAvailableSchool=false;
		for(var i=0;i<schoolListIds.length;i++){
			//alert(schoolId+" == "+schoolListIds[i]);
			if(schoolId==schoolListIds[i])
			{
				var checkAvailableSchool=true;
				errorCount++;
				break;
			}
		}
	}
	
	
	$("#editOption").val(arrReqNumIdText);
	arrReqNumIdText=arrReqNumIdText.substr(0,arrReqNum.length-1);
	
	$('#schoolName').css("background-color", "");
	$('#noOfSchoolExpHires').css("background-color", "");
	$('#attachedListSchool').css("background-color", "");
	$('#errordiv').empty();
	
	if(updateRow==0){
		if(document.getElementById("schoolRecords").innerHTML!=""){ // change By Alpha
			var schoolTable = document.getElementById("schoolTable");
			var schoolTableRowCount = $('#schoolTable tr').length;

			if(schoolTableRowCount > 1)
			{
				for (var i = 1, row; row = schoolTable.rows[i]; i++) {
					  if(schoolName==row.cells[0].innerText){
						  $('#errordiv').show();
							$('#errordiv').append("&#149; "+resourceJSON.magcannotaddexistingSchool+"<br>");
							if(focusCounter==0)
								$('#schoolName').focus();
							$('#schoolName').css("background-color", "#F5E7E1");
							errorCounter++;
							focusCounter++;
					  }
			      }
			 }
		 }
	 }
	if(schoolName==''){
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.msgPleaseenterSchoolName+"<br>");
		if(focusCounter==0)
			$('#schoolName').focus();
		$('#schoolName').css("background-color", "#F5E7E1");
		errorCounter++;
		focusCounter++;
	}else if(schoolId==''){
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.msgentervalidSchoolName+"<br>");
		if(focusCounter==0)
			$('#schoolName').focus();
		$('#schoolName').css("background-color", "#F5E7E1");
		errorCounter++;
		focusCounter++;
	}if(updateRow==0){
		if(checkAvailableSchool){
			$('#errordiv').show();
			$('#errordiv').append("&#149; "+resourceJSON.magcannotaddexistingSchool+"<br>");
			if(focusCounter==0)
				$('#schoolName').focus();
			$('#schoolName').css("background-color", "#F5E7E1");
			errorCounter++;
			focusCounter++;
		}
	}if(noOfSchoolExpHires==''){
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.msgExpectedHire+"<br>");
		if(focusCounter==0)
			$('#noOfSchoolExpHires').focus();
		$('#noOfSchoolExpHires').css("background-color", "#F5E7E1");
		focusCounter++;
		errorCounter++;
	}
	
	if(document.getElementById("isReqNoRequired").value=="true"){
		if(document.getElementById("isExpHireNotEqualToReqNo").checked==true){
			
			if(rdReqNoSourceSchool==1)
			{
				if(newReqCountFlag > noOfSchoolExpHires)
				{
					$('#errordiv').show();
					$('#errordiv').append("&#149; "+resourceJSON.msgNumberofEnterRequisitionNumber+"<br>");
					errorCounter++;
				}
				if(newReqCountFlag == 0)
				{
					$('#errordiv').show();
					$('#errordiv').append("&#149; "+resourceJSON.msgPleaseprovideatleastoneRequisitionNumber+"<br>");
					$('#newTextRequisitionNumbersSchool').css("background-color", "#F5E7E1");
					if(focusCounter==0)
						$('#newTextRequisitionNumbersSchool').focus();
					errorCounter++;
					focusCounter++;
				}
			}
			else if(rdReqNoSourceSchool==2)
			{
				if(noOfSchoolExpHires<opts.length){
					$('#errordiv').show();
					$('#errordiv').append("&#149; "+resourceJSON.msgPleaseprovideRequisitionNumber+"<br>");
					$('#attachedListSchool').css("background-color", "#F5E7E1");
					if(focusCounter==0)
						$('#attachedListSchool').focus();
					errorCounter++;
					focusCounter++;
				}else if(opts.length==0){
					$('#errordiv').show();
					$('#errordiv').append("&#149; "+resourceJSON.msgPleaseprovideatleastoneRequisitionNumber+"'s<br>");
					$('#attachedListSchool').css("background-color", "#F5E7E1");
					if(focusCounter==0)
						$('#attachedListSchool').focus();
					errorCounter++;
					focusCounter++;
				}
			}
			
		}else{
			if(rdReqNoSourceSchool==1)
			{
				if(newReqCountFlag > noOfSchoolExpHires)
				{
					$('#errordiv').show();
					$('#errordiv').append("&#149; "+resourceJSON.msgNumberofEnterRequisitionNumber+"<br>");
					errorCounter++;
				}
				if(newReqCountFlag != noOfSchoolExpHires)
				{
					$('#errordiv').show();
					$('#errordiv').append("&#149; "+resourceJSON.msgPleaseprovideRequisitionNumber+"<br>");
					$('#newTextRequisitionNumbersSchool').css("background-color", "#F5E7E1");
					if(focusCounter==0)
						$('#newTextRequisitionNumbersSchool').focus();
					errorCounter++;
					focusCounter++;
				}
			}
			else if(rdReqNoSourceSchool==2)
			{
				if(opts.length!=noOfSchoolExpHires || opts.length==0)
				{
					$('#errordiv').show();
					$('#errordiv').append("&#149; "+resourceJSON.msgPleaseprovideRequisitionNumber+"<br>");
					$('#attachedListSchool').css("background-color", "#F5E7E1");
					if(focusCounter==0)
						$('#attachedListSchool').focus();
					if(opts.length>0){

					}
					errorCounter++;
					focusCounter++;
				}
			}
		}
	}
	else
	{
		
		if(rdReqNoSourceSchool==1)
		{
			if(newReqCountFlag > noOfSchoolExpHires)
			{
				$('#errordiv').show();
				$('#errordiv').append("&#149; "+resourceJSON.msgNumberofEnterRequisitionNumber+"<br>");
				$('#newTextRequisitionNumbersSchool').css("background-color", "#F5E7E1");
				errorCounter++;
			}
		}
		else if(rdReqNoSourceSchool==2)
		{
			if(opts.length > noOfSchoolExpHires)
			{
				$('#errordiv').show();
				$('#errordiv').append("&#149; "+resourceJSON.msgRequisitionNumbershould+"<br>");
				$('#attachedListSchool').css("background-color", "#F5E7E1");
				if(focusCounter==0)
					$('#attachedListSchool').focus();
				if(opts.length>0){
				}
				errorCounter++;
				focusCounter++;
			}
		}
	}
		if(errorCounter==0)
		{
		arrReqNum=arrReqNum.substr(0,arrReqNum.length-1);
		var jobId=document.getElementById("jobId").value;
		$('#loadingDiv').show();
		if(schoolName!="" && jobId=='')
		{
				var newreqtext=rdReqNoSourceSchool+"=="+newTextRequisitionNumbersSchool;
				var arrReqNumAndSchoolId_temp11=newreqtext+"%%"+schoolId+"!!"+noOfSchoolExpHires+"@@"+arrReqNum+"##";
				ManageJobOrdersAjax.saveJobEequisitionNumbersTemp(arrReqNumAndSchoolId_temp11,{ 
					async: false,
					callback: function(data)
					{
						goFlagForSchool=1;
						ShowSchoolTemp();
					},
					errorHandler:handleError 
				});
			}
		else if(schoolName!="" && jobId!='')
		{
			
				var arrReqNumAndSchoolId_temp=schoolId+"!!"+noOfSchoolExpHires+"@@"+arrReqNum+"##";
				ManageJobOrdersAjax.saveJobEequisitionNumbers(jobId,arrReqNumAndSchoolId_temp,rdReqNoSourceSchool,newTextRequisitionNumbersSchool,{ 
					async: false,
					callback: function(data)
					{
						goFlagForSchool=1;
						ShowSchool();					
					},
					errorHandler:handleError 
				});
				
			$("#reqNumberSchoolDiv").fadeIn();
			schoolDivVal="";
			schoolDivValTD="";
		}
	 $('#loadingDiv').hide();
	$("#attachedListSchool option").remove();	
	document.getElementById("schoolName").value="";
	document.getElementById("noOfSchoolExpHires").value="";
	document.getElementById("schoolId").value="";
	cancelSchool();
	}
	else
	{
		goFlagForSchool=2;
	}
		
	return goFlagForSchool;
}

function deleteReqByJobId(){
	try{
		var jobId=$("#jobId").val();
		if(jobId>0){
			ManageJobOrdersAjax.deleteByJobId(jobId,{ 
				async: true,
				callback: function(data){
					$('#avlbListSchool').append(data);
					$('#avlbList').append(data);
					sortSelectList();
				},
				errorHandler:handleError 
			});
		}
	}catch(e){}
	
	
	document.getElementById("requiSchoolIdsForHire").value="";
	
}
function findNoOfTableRow(tableId){
	var noOfRow=0;
	try{
		var table=document.getElementById(tableId);
		noOfRow=table.rows.length;
	}catch(e){alert(e);}
	return noOfRow;
}
function existingTableRow(tableID,rowId){
	 try {
	        var table = document.getElementById(tableID);
	        var rowCount = table.rows.length;
	        for (var i = 0; i < rowCount; i++) {
	        	    var row = table.rows[i];	
	        	    if(row.id==rowId){
	        	    	return true;
	        	    }
	        }
	    } catch (e) {
	        alert(e);
	    }
}
function deleteRow(tableName, rowId,addEditFlag) 
{ 
	var row = document.getElementById(rowId);
	var reqno=row.cells[5].childNodes[0].data;
		ManageJobOrdersAjax.getRequistionNos(reqno,{ 
		async: true,
		callback: function(data){
			$('#avlbListSchool').append(data);
			$('#avlbList').append(data);
			sortSelectList();
			deleteTableRow(tableName,rowId);
		},
		errorHandler:handleError 
	 });
}

function deleteTableRow(tableID,rowId) {
    try {
        var table = document.getElementById(tableID);
        var rowCount = table.rows.length;
        for (var i = 1; i < rowCount; i++) {
        	    var row = table.rows[i];
        	    if(row.id==rowId){
        	    	table.deleteRow(i);
        	    	break;
        	    }
        }
        if(rowCount==2){
        	schoolDivCount=0;
			schoolDivCountCheck=0;
			updateRow=0;
			tableFlag=0;
        	document.getElementById("showSchoolDiv").innerHTML="";
        }
    } catch (e) {
        alert(e);
    }
}

function getRequistionNos(requiText){
	ManageJobOrdersAjax.getRequistionNos(requiText,{
		async:true,
		callback:function(data){
			$('#avlbListSchool').append(data);
			$('#avlbList').append(data);
		},errorHandler:handleError 
		});
	return true;
}

function cancelSchool()
{
	$("#addSchoolDiv").hide();
	document.getElementById("schoolName").value="";
	document.getElementById("noOfSchoolExpHires").value="";
	document.getElementById("schoolId").value="";
	document.getElementById("newTextRequisitionNumbersSchool").value="";
}

function ShowSchool()
{
	   $("#loadingSchoolRowDiv").show();
	var jobId=0;
	if(document.getElementById("schoolId").value!=null){
		jobId=document.getElementById("jobId").value;
	}
	var JobOrderType=document.getElementById("JobOrderType").value;
	var url =window.location.href;
	 if(url.indexOf("editjobordertest.do") > -1){
		 $('#schoolRecords').html("<table align='center' id='loaddiv'><tr><td style='padding-top: 10px;' align='center'><!--<img src='images/please.jpg'/>--></td></tr><tr><td style='padding-top: 0px;' align='center'><img src='images/loadingAnimation.gif'/></td></tr></table>");
			ManageJobOrdersAjax.displaySchool_New_Op(jobId,JobOrderType, { 
				async: true,
				callback: function(data)
				{
				
					document.getElementById("schoolRecords").innerHTML=data;
					var locationurl=window.location.href;
				     var districtId= $("#districtOrSchooHiddenlId").val();
					 if(locationurl.indexOf("addeditjoborder") > -1 && $('#entityType').val()==3 && districtId==806900)
					 {
						 $("#loadingDiv").hide();
					 }
					 
				},
				errorHandler:handleError 
			}); 
		 
	 }
	 else{
	ManageJobOrdersAjax.displaySchool(jobId,JobOrderType, { 
		async: false,
		callback: function(data)
		{
			document.getElementById("schoolRecords").innerHTML=data;
			var locationurl=window.location.href;
		     var districtId= $("#districtOrSchooHiddenlId").val();
			 if(locationurl.indexOf("addeditjoborder") > -1 && $('#entityType').val()==3 && districtId==806900)
			 {
				 $("#loadingDiv").hide();
			 }
			 
		},
		errorHandler:handleError 
	});
	 }
	$("#loadingSchoolRowDiv").hide();
}


function ShowSchoolTemp()
{
   $("#loadingSchoolRowDiv").show();
	var JobOrderType=document.getElementById("JobOrderType").value;
	ManageJobOrdersAjax.displaySchoolTemp(JobOrderType, { 
		async: false,
		callback: function(data)
		{
			document.getElementById("schoolRecords").innerHTML=data;
			var locationurl=window.location.href;
		     var districtId= $("#districtOrSchooHiddenlId").val();
			 if(locationurl.indexOf("addeditjoborder") > -1 && $('#entityType').val()==3 && districtId==806900)
			 {	
				 console.log("::::::::::THis is testing :::::::::::");
			     $('#loadingDiv').hide();
			 }
		},
		errorHandler:handleError 
	});
	$("#loadingSchoolRowDiv").hide();
}


function showSchoolTemp_NoReq()
{
	var JobOrderType=document.getElementById("JobOrderType").value;
	ManageJobOrdersAjax.displaySchoolTemp_NoReq(JobOrderType, { 
		async: true,
		callback: function(data)
		{
			document.getElementById("schoolRecords").innerHTML=data;
		},
		errorHandler:handleError 
	});
}

function jobStartDateDiv(){
	 var cal = Calendar.setup({
         onSelect: function(cal) { 
		 autoPostingEndTime();
		 cal.hide() },
         
         showTime: true
    });
	cal.manageFields("jobStartDate", "jobStartDate", "%m-%d-%Y");
}
function DisplayCertification()
{
	var jobId=0;
	var jobStartDate	=	trim(document.getElementById("jobStartDate").value);
	
	var isJobApplied	="";
	try{
		isJobApplied=document.getElementById("isJobApplied").value;
	}catch(err){e}
	if(document.getElementById("jobId").value!=''){
		jobId=document.getElementById("jobId").value;
	}else if(document.getElementById("clonejobId").value){
		jobId=document.getElementById("clonejobId").value;
	}
	if(jobId==0){
		jobStartDateDiv();
	}else{
		if(jobStartDate!=""){
			var startDate= new Date(jobStartDate);   
			if(startDate < new Date() && !isCloneJob){
				if(isJobApplied=='true'){
					document.getElementById('jobStartDate').readOnly=true;
				}else{
					jobStartDateDiv();
				}
			}else{
				jobStartDateDiv();
			}
		}else{
			jobStartDateDiv();
		}
	}
	var JobOrderType=document.getElementById("JobOrderType").value;
	ManageJobOrdersAjax.displayCertification(jobId,JobOrderType, { 
		async: true,
		callback: function(data)
		{	
			document.getElementById("certificationRecords").innerHTML=data;
		},
		errorHandler:handleError 
	});
}

/*========  displayJobRecords ===============*/
function displayJobRecords()
{
	var JobOrderType	=null;	
	var schoolId=null;
	var resultFlag=true;
	try{
		JobOrderType=document.getElementById("JobOrderType").value;
	}catch(err){
		JobOrderType=0;
	}
	try{
		schoolId=document.getElementById("schoolId").value;
	}catch(err){
		schoolId=0;
	}
	
	if(JobOrderType	==	"" || JobOrderType	==	null)
	{
		JobOrderType	=0;
	}
	if(document.getElementById("jobId").value!=''){
		document.getElementById("districtORSchoolName").disabled=true;
		document.getElementById("schoolName").disabled=true;
	}
	if(JobOrderType==2){
		document.getElementById("titleDistrictOrSchool").innerHTML	=	resourceJSON.msgDistrict;
		document.getElementById("hideForSchool").style.display='inline';
		document.getElementById("addHiresDiv").style.display='none';
		document.getElementById("schoolName").disabled=false;
	}else if(JobOrderType==3){
		document.getElementById("hideForSchool").style.display='none';
		document.getElementById("addHiresDiv").style.display='inline';
		document.getElementById("titleDistrictOrSchool").innerHTML	=	resourceJSON.msgSchool;
		document.getElementById("schoolName").disabled=true;
		if(schoolId==0){
			document.getElementById("schoolName").disabled=true;
		}else{
			document.getElementById("districtORSchoolName").disabled=true;
		}
	}
}
/*========  activateDeactivateUser ===============*/
function activateDeactivateJob(entityID,jobid,status)
{
	ManageJobOrdersAjax.activateDeactivateJob(jobid,status, { 
		async: true,
		callback: function(data)
		{
			displayJobRecords(entityID)
		},
		errorHandler:handleError 
	});
}

function getStatusList(districtId,jobCategoryId)
{	
	if(jobCategoryId!=0){
		setStatusList("",districtId,jobCategoryId);
	}else{
		$("#interviewPanel").hide();
	}
}

function setStatusList(jobId,districtId,jobCategoryId)
{	
	if(jobId==""){
		jobId=0;
	}
	var url =window.location.href;
	
	 if(url.indexOf("editjobordertest.do") > -1){
		 
		 ManageJobOrdersAjax.getStatusList_New_Op(jobId,districtId,jobCategoryId,{ 
				async: true,
				callback: function(data)
				{	
					if(data!=""){
						$("#secStatusList").html(data);
						var nds=document.getElementById("noofsecStatusCheckbox").value;
						if(nds!=0){
							$("#interviewPanel").show();
						}
					}else{
						$("#interviewPanel").hide();
					}
				},
				errorHandler:handleError 
			}); 
	 }
	 else{
	ManageJobOrdersAjax.getStatusList(jobId,districtId,jobCategoryId,{ 
		async: true,
		callback: function(data)
		{	
			if(data!=""){
				$("#secStatusList").html(data);
				var nds=document.getElementById("noofsecStatusCheckbox").value;
				if(nds!=0){
					$("#interviewPanel").show();
				}
			}else{
				$("#interviewPanel").hide();
			}
		},
		errorHandler:handleError 
	});
	 }
}

function lockGeozone(flag)
{
	if(flag==1){
		if($("#isZoneAvailable").val()!=0){
			$("#zone").attr('disabled', 'disabled');
		}	
	}else if(flag==2){
		if($("#isZoneAvailable").val()!=0){
			$('#zone').removeAttr('disabled');
		}	
	}
}

/*********************************************************************************************************/
var count	=	0;
var index	=	-1;
var length	=	0;
var divid	=	'';
var txtid	=	'';

var hiddenDataArray			= 	new Array();
var showDataArray 			= 	new Array();
var degreeTypeArray 		= 	new Array();
var hiddenExitURLArray 		= 	new Array();
var hiddenExitMessageArray 	= 	new Array();
var hiddenFlagForUrlArray 	= 	new Array();
var hiddenFlagForMessageArray = new Array();
var hiddenDAUploadURLArray 	= 	new Array();
var hiddenSAUploadURLArray 	= 	new Array();
var districtHiddenIdArray 	= 	new Array();
var isReqNoRequiredArray 	= 	new Array();

var hiddenId="";
function getDistrictORSchoolAuto(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("districtORSchoolName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getDistrictORSchoolArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getDistrictORSchoolArray(districtOrSchoolName){
	var searchArray = new Array();
	var JobOrderType	=	document.getElementById("JobOrderType").value;
	ManageJobOrdersAjax.getFieldOfDistrictList(districtOrSchoolName,{ 
		async: false,
		callback: function(data){
		
		hiddenDataArray = new Array();
		showDataArray = new Array();
		for(i=0;i<data.length;i++){
			searchArray[i]=data[i].districtName;
			showDataArray[i]=data[i].districtName;
			hiddenDataArray[i]=data[i].districtId;
			districtHiddenIdArray[i]=data[i].districtId;
			isReqNoRequiredArray[i]=data[i].isReqNoRequired;
			hiddenExitURLArray[i] = data[i].exitURL;
			hiddenExitMessageArray[i] = data[i].exitMessage;
			hiddenFlagForMessageArray[i]=data[i].flagForMessage;
			hiddenDAUploadURLArray[i]=data[i].assessmentUploadURL;
			hiddenSAUploadURLArray[i]=null;
		}
	},
	errorHandler:handleError 
	});	
	return searchArray;
}


var selectFirst="";

var fatchData= function(txtSearch,searchArray,txtId,txtdivid){
	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
			}
		}
		else {

		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}
function hideDistrictMasterDiv(dis,hiddenId,divId)
{
	document.getElementById("districtOrSchooHiddenlId").value="";
	document.getElementById("districtSchoolHiddenId").value="";
	document.getElementById("exitURL").value="";
	$('#eMessage').find(".jqte_editor").html("");
	document.getElementById("districtHiddenId").value="";
	document.getElementById("schoolName").value="";
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			document.getElementById("districtOrSchooHiddenlId").value=districtHiddenIdArray[index];
			document.getElementById("districtSchoolHiddenId").value=hiddenDataArray[index];
			document.getElementById("districtHiddenId").value=districtHiddenIdArray[index];
			document.getElementById("isReqNoRequired").value=isReqNoRequiredArray[index];
			document.getElementById("reqNumberDiv").style.display='inline';
			if(document.getElementById("isReqNoRequired").value=="true"){
				document.getElementById("reqNumberForSchoolDiv").style.display='inline';
			}else{
				document.getElementById("reqNumberForSchoolDiv").style.display='none';
			}

			document.getElementById("schoolName").disabled=false;
			try{
				document.getElementById("exitURL").value=hiddenExitURLArray[index];
			}catch(err){}
			try{
				$('#eMessage').find(".jqte_editor").html(hiddenExitMessageArray[index]);
			}catch(err){}
			try{
				/* ----- Gagan : It means  District is  Changed ======*/
				$("#jcjsidiv").hide();
				var windowFunc=document.getElementById("windowFunc").value;
				if(hiddenFlagForMessageArray[index]==1){
					document.getElementById("exitURLMessageVal").value=2;
					document.getElementById("exitUrlMessage2").checked=true;
					document.getElementById("showExitURLDiv").style.display='none';
					document.getElementById("showExitMessageDiv").style.display='inline';
				}else{
					document.getElementById("exitURLMessageVal").value=1;
					document.getElementById("exitUrlMessage1").checked=true;
					document.getElementById("showExitURLDiv").style.display='inline';
					document.getElementById("showExitMessageDiv").style.display='none';
				}
				if(hiddenDAUploadURLArray[index]!=null && hiddenDAUploadURLArray[index]!=''){
					document.getElementById("hideDAUploadURL").style.display='inline';
					
					document.getElementById("isJobAssessmentDiv").style.display='inline';
					document.getElementById("isJobAssessmentVal").value=1;
					document.getElementById("isJobAssessment2").checked=true;
					
					/* ----- Gagan : It means  District is  Changed ======*/
					document.getElementById("attachJobAssessmentVal").value=1;
					document.getElementById("attachJobAssessment1").checked=true;
					document.getElementById("DAssessmentUploadURL").value=hiddenDAUploadURLArray[index];
					document.getElementById("ViewDAURL").style.display='inline';
					document.getElementById("ViewDAURL").innerHTML="<a data-original-title='JSI' rel='tooltip' id='JSIdistrict3' href='javascript:void(0)' onclick=\"showFile('district','"+districtHiddenIdArray[index]+"','"+hiddenDAUploadURLArray[index]+"','JSIdistrict3');"+windowFunc+"\">"+resourceJSON.ViewLink+"</a>";
				}else{
					document.getElementById("hideDAUploadURL").style.display='none';
					document.getElementById("ViewDAURL").style.display='none';
					document.getElementById("isJobAssessmentDiv").style.display='none';
					document.getElementById("isJobAssessmentVal").value=2;
					document.getElementById("attachJobAssessmentVal").value=2;
					document.getElementById("attachJobAssessment3").checked=true;
					document.getElementById("isJobAssessment1").checked=true;
				}

				if(hiddenSAUploadURLArray[index]!=null  && hiddenSAUploadURLArray[index]!=''){
					document.getElementById("hideSAUploadURL").style.display='inline';
					document.getElementById("ViewSAURL").style.display='inline';
					document.getElementById("ViewSAURL").innerHTML="<a data-original-title='JSI' rel='tooltip' id='JSIschool3' href='javascript:void(0)' onclick=\"showFile('school','"+hiddenDataArray[index]+"','"+hiddenSAUploadURLArray[index]+"','JSIschool3');"+windowFunc+"\">"+resourceJSON.ViewLink+"</a>";
				}else{
					document.getElementById("hideSAUploadURL").style.display='none';
					document.getElementById("ViewSAURL").style.display='none';
				}

			}catch(err){
				document.getElementById("exitURLMessageVal").value=1;
				document.getElementById("exitUrlMessage1").checked=true;
			}
		}
		
	}else{
		document.getElementById("isJobAssessmentDiv").style.display='none';
		document.getElementById("isJobAssessmentVal").value=2;
		document.getElementById("attachJobAssessmentVal").value=2;
		document.getElementById("isJobAssessment1").checked=true;
		document.getElementById("attachJobAssessmentVal").value=2;
		document.getElementById("attachJobAssessment3").checked=true;
		document.getElementById("hideDAUploadURL").style.display='none';

		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
		document.getElementById("schoolName").disabled=true;
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;

	if($('#entityType').val()==1)
	{
		getJobCategoryByDistrictId();
	}
}
function showFile(districtORSchoool,districtORSchooolId,docFileName,linkId)
{
	ManageJobOrdersAjax.showFile(districtORSchoool,districtORSchooolId,docFileName,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			if(data=="")
			{
				alert("JSI is not uploaded.")
			}
			else if(data.indexOf(".doc")!=-1)
			{
				document.getElementById("iframeJSI").src=data;
			}
			else
			{
				document.getElementById(linkId).href = data; 
				return false;
			}
		} 
	});
}

count=0;
index=-1;
length=0;
divid='';
txtid='';
hiddenDataArray = new Array();
showDataArray = new Array();
degreeTypeArray = new Array();
hiddenId="";
hiddenExitURLArray = new Array();
hiddenExitMessageArray = new Array();
hiddenFlagForUrlArray = new Array();
hiddenFlagForMessageArray = new Array();
hiddenDAUploadURLArray = new Array();
hiddenSAUploadURLArray = new Array();
districtHiddenIdArray = new Array();

function getSchoolAuto(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("schoolName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getSchoolArray(txtSearch.value);
		fatchData2(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getSchoolArray(schoolName){
	var searchArray = new Array();
	var JobOrderType	=	document.getElementById("JobOrderType").value;
	var districtId	=	document.getElementById("districtOrSchooHiddenlId").value;
	
	if($("#isZoneAvailable").val()!=0){
		var zoneId	=	document.getElementById("zone").value;
		if(zoneId==""){
			zoneId=0;
		}
		ManageJobOrdersAjax.getFieldOfSchoolListByZones(districtId,schoolName,zoneId,{ 
			async: false,
			callback: function(data){                  
			hiddenDataArray = new Array();
			showDataArray = new Array();			
			for(i=0;i<data.length;i++){
				var location ="";
				if(data[i].locationCode!=null && data[i].locationCode!="") 
				{
					location = " ("+data[i].locationCode+")";
				}
				
				//alert('03');
				searchArray[i]=data[i].schoolName+location;
				showDataArray[i]=data[i].schoolName+location;
				hiddenDataArray[i]=data[i].schoolId;
				
				districtHiddenIdArray[i]=data[i].districtId.districtId;
				hiddenExitURLArray[i] = data[i].exitURL;
				hiddenExitMessageArray[i] = data[i].exitMessage;
				hiddenFlagForMessageArray[i]=data[i].flagForMessage;
				hiddenDAUploadURLArray[i]=data[i].districtId.assessmentUploadURL;;
				hiddenSAUploadURLArray[i]=data[i].assessmentUploadURL;
			}
		},
		errorHandler:handleError 
		});	
	}else{
		ManageJobOrdersAjax.getFieldOfSchoolList(districtId,schoolName,{ 
			async: false,
			callback: function(data){                  
			hiddenDataArray = new Array();
			showDataArray = new Array();
			for(i=0;i<data.length;i++){
				
				var location ="";
				if(data[i].locationCode!=null && data[i].locationCode!="") 
				{
					location = " ("+data[i].locationCode+")";
				}
				
				searchArray[i]=data[i].schoolName+location;
				showDataArray[i]=data[i].schoolName+location;
				hiddenDataArray[i]=data[i].schoolId;
				
				districtHiddenIdArray[i]=data[i].districtId.districtId;
				hiddenExitURLArray[i] = data[i].exitURL;
				hiddenExitMessageArray[i] = data[i].exitMessage;
				hiddenFlagForMessageArray[i]=data[i].flagForMessage;
				hiddenDAUploadURLArray[i]=data[i].districtId.assessmentUploadURL;;
				hiddenSAUploadURLArray[i]=data[i].assessmentUploadURL;
			}
		},
		errorHandler:handleError 
		});	
	}
	
	return searchArray;
}


selectFirst="";

var fatchData2= function(txtSearch,searchArray,txtId,txtdivid){

	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
			}
		}
		else {

		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}
function hideSchoolMasterDiv(dis,hiddenId,divId)
{
	document.getElementById("schoolId").value="";
	var JobOrderType=document.getElementById("JobOrderType").value;
	var jobCategoryId	=	document.getElementById("jobCategoryId").value;
	var jobCategoryVal=jobCategoryId.split("||");
	var jobCategory=jobCategoryVal[0];

	if(JobOrderType==3){
		document.getElementById("districtSchoolHiddenId").value="";
	}
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			document.getElementById("schoolId").value=hiddenDataArray[index];
			
			if(JobOrderType==3){
				document.getElementById("districtSchoolHiddenId").value=hiddenDataArray[index];
				try{
					document.getElementById("exitURL").value=hiddenExitURLArray[index];
				}catch(err){}
				try{
					$('#eMessage').find(".jqte_editor").html(hiddenExitMessageArray[index]);
				}catch(err){}
				try{
					var windowFunc=document.getElementById("windowFunc").value;
					if(hiddenFlagForMessageArray[index]==1){
						document.getElementById("exitURLMessageVal").value=2;
						document.getElementById("exitUrlMessage2").checked=true;
						document.getElementById("showExitURLDiv").style.display='none';
						document.getElementById("showExitMessageDiv").style.display='inline';
					}else{
						document.getElementById("exitURLMessageVal").value=1;
						document.getElementById("exitUrlMessage1").checked=true;
						document.getElementById("showExitURLDiv").style.display='inline';
						document.getElementById("showExitMessageDiv").style.display='none';
					}
					if(hiddenDAUploadURLArray[index]!=null && hiddenDAUploadURLArray[index]!=''){
						document.getElementById("hideDAUploadURL").style.display='inline';
						document.getElementById("DAssessmentUploadURL").value=hiddenDAUploadURLArray[index];
						document.getElementById("ViewDAURL").style.display='inline';
						document.getElementById("ViewDAURL").innerHTML="<a data-original-title='JSI' rel='tooltip' id='JSIdistrict4' href='javascript:void(0)' onclick=\"showFile('district','"+districtHiddenIdArray[index]+"','"+hiddenDAUploadURLArray[index]+"','JSIdistrict4');"+windowFunc+"\">"+resourceJSON.ViewLink+"</a>";
					}else{
						document.getElementById("hideDAUploadURL").style.display='none';
						document.getElementById("ViewDAURL").style.display='none';
					}
					
					if(hiddenSAUploadURLArray[index]!=null  && hiddenSAUploadURLArray[index]!=''){
						document.getElementById("hideSAUploadURL").style.display='inline';
						document.getElementById("ViewSAURL").style.display='inline';
						
						document.getElementById("isJobAssessmentDiv").style.display='inline';
						document.getElementById("isJobAssessmentVal").value=1;
						document.getElementById("isJobAssessment2").checked=true;

						if(jobCategory==0) //Gagan : It means if Job Category is not selected then default radio btn  of JSI at School will be selcted // 
						{
							document.getElementById("attachJobAssessment2").checked=true;
							document.getElementById("attachJobAssessmentVal").value=3;
						}
						document.getElementById("ViewSAURL").innerHTML="<a data-original-title='JSI' rel='tooltip' id='JSIschool4' href='javascript:void(0)' onclick=\"showFile('school','"+hiddenDataArray[index]+"','"+hiddenSAUploadURLArray[index]+"','JSIschool4');"+windowFunc+"\">"+resourceJSON.ViewLink+"</a>";
					}else{
						document.getElementById("hideSAUploadURL").style.display='none';
						document.getElementById("ViewSAURL").style.display='none';
						
						if(jobCategory==0) //Gagan : It means if Job Category is not selected and assesment document is not available then default radio btn  of JSI at School will be selcted otherwise Jobcategory JSI radi vutton will be selected // 
						{
							document.getElementById("isJobAssessmentDiv").style.display='none';
							document.getElementById("isJobAssessmentVal").value=2;
							document.getElementById("attachJobAssessmentVal").value=2;
							document.getElementById("attachJobAssessment3").checked=true;
							document.getElementById("isJobAssessment1").checked=true;
						}
					}
					
				}catch(err){
					document.getElementById("exitURLMessageVal").value=1;
					document.getElementById("exitUrlMessage1").checked=true;
				}
			}
		}
		
	}else{
		document.getElementById("isJobAssessmentDiv").style.display='none';
		document.getElementById("isJobAssessmentVal").value=2;
		document.getElementById("attachJobAssessmentVal").value=2;
		document.getElementById("isJobAssessment1").checked=true;
		document.getElementById("attachJobAssessmentVal").value=2;
		document.getElementById("attachJobAssessment3").checked=true;
		document.getElementById("hideDAUploadURL").style.display='none';
		document.getElementById("hideSAUploadURL").style.display='none';
		
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}

var downArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index<length-1){
			for(var i=0;i<10;i++){
				{
					if(document.getElementById('divResult'+txtdivid+i))
						var div_id=document.getElementById('divResult'+txtdivid+i);
					if(div_id)
					{
						if(div_id.className=='over')
							index=div_id.id.split('divResult'+txtdivid)[1];
						div_id.className='normal';
					}
				}
			}
			index++;
			if(document.getElementById('divResult'+txtdivid+index))
			{
				var div_id=document.getElementById('divResult'+txtdivid+index);
				div_id.className='over';
				document.getElementById(txtid).value = div_id.innerHTML;
				selectFirst=div_id.innerHTML;
			}
		}
	}
}

var upArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index>0){
			for(var i=0;i<length;i++){

				var div_id=document.getElementById('divResult'+txtdivid+i);
				if(div_id)
				{
					if(div_id.className=='over')
						index=div_id.id.split('divResult'+txtdivid)[1];
					div_id.className='normal';
				}
			}
			index--;
			if(document.getElementById('divResult'+txtdivid+index))
				document.getElementById('divResult'+txtdivid+index).className='over';
			if(txtid && document.getElementById('divResult'+txtdivid+index))
			{
				document.getElementById(txtid).value=
					document.getElementById('divResult'+txtdivid+index).innerHTML;
				selectFirst=document.getElementById('divResult'+txtdivid+index).innerHTML;
			}
		}
	}
}

function mouseOverChk(txtdivid,txtboxId)
{	
	for(var i=0;i<length;i++)
	{	
		$('#divResult'+txtdivid+i).mouseover({param1: i,param2:txtdivid, param3:txtboxId}, fireMouseOverEvent);
	}
}

function fireMouseOverEvent(event)
{
	for(var i=0;i<length;i++)
	{	
		document.getElementById('divResult'+event.data.param2+i).className='normal';
	}
    document.getElementById('divResult'+event.data.param2+event.data.param1).className='over';	       
   	document.getElementById(event.data.param3).value= $('#divResult'+event.data.param2+event.data.param1).text();
    index=event.data.param1;
}



var overText=function (div_value,txtdivid) {

	for(var i=0;i<length;i++)
	{
		if(document.getElementById('divResult'+i))
		{
			var div_id=document.getElementById('divResult'+i);

			if(div_id.className=='over')
				index=div_id.id.split(txtdivid)[1];
			div_id.className='normal';
		}
	}
	div_value.className = 'over';
	document.getElementById(txtid).value= div_value.innerHTML;
}
function trim(s)
{
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}
function checkForInt(evt) {

	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==8)
		return true;
	return( (charCode >= 48 && charCode <= 57)||(charCode==13) );
}
function DateFormatChange(dateParam){
	da = new Date(dateParam)
	db = da.toGMTString()	// Convert to a String in "predictable formt"
	dc = db.split(" ")	// Split the string on spaces
	if ( eval( dc[3] ) < 1970 ) dc[3] = eval( dc[3] ) +100	// Correct any date purporting to be before 1970
	db = dc[2] + " " + dc[1] + ", " + dc[3]	// Ignore day of week and combine date, month and year
	return db;
}

function getSubjectByDistrict()
{
	var districtId	=	$('#districtOrSchooHiddenlId').val();
	
	//jeffco 
	if(districtId=='804800'){
		callForPrimaryESTech();
	}else{
		$('.onlyjeffco').hide();
	}
	
	DWRAutoComplete.getSubjectByDistrictForAddJobOrder(districtId,{ 
		async: false,		
		callback: function(data)
		{
			document.getElementById("subjectId").innerHTML=data;
		}
	});
}

function editSchoolInJobOrderTemp(schoolId){
	updateRow=1;
		var locationurl=window.location.href;
		var isReqNoRequired = $('#districtReqNumFlag').val();
	if($('#entityType').val()==3 && !isReqNoRequired){
		$("#rdReqNoSourceSchool1" ).prop( "disabled", true );
		$("#newTextRequisitionNumbersSchool" ).prop( "disabled", true );
		$("#rdReqNoSourceSchool2" ).prop( "disabled", true );
		$("#avlbListSchool" ).prop( "disabled", true );
		$("#attachedListSchool" ).prop( "disabled", true );
		$("#addPopSchool" ).prop( "disabled", true );
		$("#removePopSchool" ).prop( "disabled", true );
		}
	else
		$("#rdReqNoSourceSchool2" ).prop( "checked", true );
	$("#addSchoolDiv").fadeIn();
	$('#schoolId').val("");
	$('#schoolName').val("");
	$('#noOfSchoolExpHires').val("");
	$('#newTextRequisitionNumbersSchool').val("");
	
	$('#schoolName').css("background-color", "");
	$('#noOfSchoolExpHires').css("background-color", "");
	$('#attachedListSchool').css("background-color", "");
	$('#newTextRequisitionNumbersSchool').css("background-color", "");
	if($('#entityType').val()!=3)
		$('#attachedListSchool option').remove();
	ManageJobOrdersAjax.editSchoolInJobOrderTemp(schoolId,{ 
		async: true,
		callback: function(data){
			var s0=data.split("###");
			var s1=s0[0].split("##");
			var s2=s1[0].split("!!");
			$('#schoolId').val(s2[0]);
			$('#schoolName').val(s2[1]);
			$('#schoolName').attr("disabled", "disabled");
			var locationurl=window.location.href;
			if(!((locationurl.indexOf("addeditjoborder.do") > -1)) && $('#entityType').val()==3){
				//alert(876);
				$('#noOfSchoolExpHires').val($('#hidNoOfExpHires').val());
			}
			else if($('#entityType').val()==1 || $('#entityType').val()==2)
			{
				$('#noOfSchoolExpHires').val($('#hidNoOfExpHires').val());
			}
			else{
				$('#noOfSchoolExpHires').val(s1[1]);
			}
				
			
			var s3=s0[1].split("==");
			if(s3[1]=="1")
			{
				$('#newTextRequisitionNumbersSchool').val(s3[0]);
				$( "#rdReqNoSourceSchool1" ).prop( "checked", true );
			}
			if(s3[1]=="2")
			{
				$('#attachedListSchool').append(s3[0]);
				$( "#rdReqNoSourceSchool2" ).prop( "checked", true );
			}
			
			$('#deleteAvailableRow').val(schoolId);
		},
		errorHandler:handleError 
	}); 
}

function editSchoolInJobOrderTemp_NoReq(schoolId){
	updateRow=1;
	$("#addSchoolDiv").fadeIn();
	$('#schoolId').val("");
	$('#schoolName').val("");
	$('#noOfSchoolExpHires').val("");
	
	$('#schoolName').css("background-color", "");
	$('#noOfSchoolExpHires').css("background-color", "");
	$('#attachedListSchool').css("background-color", "");
	
	$('#attachedListSchool option').remove();
	ManageJobOrdersAjax.editSchoolInJobOrderTemp_NoReq(schoolId,{ 
		async: true,
		callback: function(data){
			$('#schoolId').val(data.schoolMaster.schoolId);
			$('#schoolName').val(data.schoolMaster.schoolName);
			$('#schoolName').attr("disabled", "disabled");
			$('#noOfSchoolExpHires').val(data.noOfSchoolExpHires);
		},
		errorHandler:handleError 
	}); 
}

function deleteTempReq()
{
	ManageJobOrdersAjax.deleteTempReq
	(
	{ 
	async: true,
	callback: function(data)
	{
	},
	errorHandler:handleError 
	}
	); 
}

function setDistrictReqFlag()
{
	var districtId=$('#districtOrSchooHiddenlId').val();
	if(districtId > 0)
	{
		ManageJobOrdersAjax.getDistrictReqFlag(districtId,{ 
			async: true,		
			callback: function(data)
			{
				$('#districtReqFlag').val(data);
			}
		});
	}
}

//Getting isReqNoForHiring and isReqNoRequired By Dhananjay Verma
function setDistrictReqAndHiringFlag()
{
	
	var  districtORSchoolName = $('#districtORSchoolName').val();
	var districtId=$('#districtOrSchooHiddenlId').val();
	
	if(districtId > 0 && districtORSchoolName.trim() !='')
	{
		
		ManageJobOrdersAjax.getHiringRequireFlagValue(districtId,{ 
			async: false,		
			
			callback: function(data)
			{
				
				var dataArr=data.split('~');
								
				if(dataArr[0] == '1' && dataArr[1] == '0')
				{
				
					$('#isReqNoRequiredRequisition').val("true");
					$('#isReqNoHiringRequisition').val("false");
					
					$("#requisitionSectionJobOrder").show();
				}
				else if(dataArr[0] == '0' && dataArr[1] == '1')
				{
					
					$('#isReqNoRequiredRequisition').val("false");
					$('#isReqNoHiringRequisition').val("true");
					
					$("#requisitionSectionJobOrder").show();
				}
				else if(dataArr[0] == '1' && dataArr[1] == '1')
				{
					
					$('#isReqNoRequiredRequisition').val("true");
					$('#isReqNoHiringRequisition').val("true");
					
					$("#requisitionSectionJobOrder").show();
				}
				else if(dataArr[0] == '0' && dataArr[1] == '0')
				{
					
					$('#isReqNoRequiredRequisition').val("");
					$('#isReqNoHiringRequisition').val("");
						$("#requisitionSectionJobOrder").hide();
				}
				else
				{
					$('#isReqNoRequiredRequisition').val("");
					$('#isReqNoHiringRequisition').val("");
					$("#requisitionSectionJobOrder").hide();
				}
			}
		});
	}
	else{
		
		$('#isReqNoRequiredRequisition').val("");
		$('#isReqNoHiringRequisition').val("");
		$("#requisitionSectionJobOrder").hide();
	}
}

//Getting Job Approval Process Based On District Id By Dhananjay Verma

function getJobApprovalP()
{
	var districtId=$('#districtOrSchooHiddenlId').val();
	if(districtId > 0)
	{
		try{
			ManageJobOrdersAjax.getJobApprovalProcess(districtId,{ 
			async: true,
			errorHandler:handleError,
			callback: function(data)
			{	
				if(data!=null && data!="")
				{
						
					$("#jobApprovalProcessId").html(data);
				}
				
			}
		});
		}catch(e){alert(e);	}
	}
	
}


function getZoneName()
{
	var districtOrSchooHiddenlId=document.getElementById("districtOrSchooHiddenlId").value;
	ManageJobOrdersAjax.getZoneList(districtOrSchooHiddenlId,{ 
		async: true,
		callback: function(data)
		{	
		
			if(data!=null && data!="")
			{
				$("#isZoneAvailable").val(1);			
				$("#displayZone").show();			
				$("#zone").html(data);
			}
			else{
				$("#displayZone").hide();
				$("#zone").html(data);				
				$("#isZoneAvailable").val(0);
			}	
		},
	});
}


function getZoneNameDA(dsId)
{
	var districtOrSchooHiddenlId=dsId;
	ManageJobOrdersAjax.getZoneList(districtOrSchooHiddenlId,{ 
		async: true,
		callback: function(data)
		{	
			if(data!=null && data!="")
			{
				$("#isZoneAvailable").val(1);			
				$("#displayZone").show();			
				$("#zone").html(data);
			}
			else{
				$("#displayZone").hide();
				$("#zone").html(data);				
				$("#isZoneAvailable").val(0);
			}	
		},
	});
}

function getZoneNameCheck(districtId)
{
	ManageJobOrdersAjax.getZoneList(districtId,{ 
		async: true,
		callback: function(data)
		{	
			if(data!=null && data!="")
			{
				$("#isZoneAvailable").val(1);
				$("#zoneLableRed").show();
				$("#displayZone").show();
				$("#zone").show();
				$("#zone").html(data);
			}
			else{
				$("#zone").hide();
				$("#displayZone").hide();
			}	
		},
	});
}

function getZoneDA(dsId)
{
	var districtOrSchooHiddenlId=dsId;
	ManageJobOrdersAjax.getZoneListAll(districtOrSchooHiddenlId,{ 
		async: true,
		callback: function(data)
		{	
			if(data!=null && data!="")
			{
				$("#isZoneAvailable").val(1);			
				$("#displayZone").show();			
				$("#zone").html(data);
			} else {
				$("#displayZone").hide();
				$("#zone").html(data);				
				$("#isZoneAvailable").val(0);
			}	
		},
	});
}

function getZoneDAForEJob(dsId)
{
	var districtOrSchooHiddenlId=dsId;
	ManageJobOrdersAjax.getZoneListAllForEJob(districtOrSchooHiddenlId,{ 
		async: true,
		callback: function(data)
		{	
			if(data!=null && data!="")
			{
				$("#isZoneAvailable").val(1);			
				$("#displayZone").show();			
				$("#zone").html(data);
			}
			else{
				$("#displayZone").hide();
				$("#zone").html(data);				
				$("#isZoneAvailable").val(0);
			}	
		},
	});
}


function showDistFile(distId,fileName) {
ManageJobOrdersAjax.showDistFile(distId,fileName,{
async: true,
errorHandler:handleError,
callback:function(data)
{
	if(deviceType)
	{
		if (data.indexOf(".doc")!=-1)
		{
			$("#docfileNotOpen").css({"z-index":"3000"});
	    	$('#docfileNotOpen').show();
		}
		else if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
		{
			$("#exelfileNotOpen").css({"z-index":"3000"});
	    	$('#exelfileNotOpen').show();
		}
		else
		{
			document.getElementById(linkid).href = data;
		}
	}
	else if(deviceTypeAndroid)
	{
		if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
		{
			 $('#modalDownloadsAttachment').modal('hide');
			 document.getElementById('ifrmAttachment').src = ""+data+"";
		}
		else
		{
			document.getElementById(linkid).href = data;
		}
	}
	else if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
	{
		 $('#modalDownloadsAttachment').modal('hide');
		 document.getElementById('ifrmAttachment').src = ""+data+"";
	} else {
			$('.distAttachment').modal('show');
			document.getElementById('ifrmAttachment').src = ""+data+"";
	}
}});

}

function viewJobDescription(jobId)
{	
	ManageJobOrdersAjax.viewJobDescription(jobId,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data){
			document.getElementById("hrefJobDesc").href = data; 
		}
	});
}

function viewAdditionalDocument(jobId)
{	
	ManageJobOrdersAjax.viewAdditionalDocument(jobId,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data){
			document.getElementById("hrefAdditionalDocumentDesc").href = data; 
		}
	});
}

function disableEndDate(){
	document.getElementById('changeDateHid').value=document.getElementById('jobEndDate').value;
	document.getElementById('jobEndDate').value='';
	document.getElementById('jobEndDate').disabled=true;	
}
function unDisableEndDate(){
	document.getElementById('jobEndDate').disabled=false;
	document.getElementById('jobEndDate').value=document.getElementById('changeDateHid').value;
}


/* ========= Start :: Virtual Video Interview ============*/
function showOfferVVI()
{
	var offerVVI 		= 	document.getElementById("offerVirtualVideoInterview").checked;
	
	if(offerVVI==true)
		$("#offerVVIDiv").show();
	else
		$("#offerVVIDiv").hide();
}

function showMarksDiv()
{
	var wantScore 		= 	document.getElementById("wantScore").checked;
	var maxScoreForVVI 	= 	document.getElementById("maxScoreForVVI").value;
		
	if(wantScore==true)
		$("#marksDiv").show();
	else
		$("#marksDiv").hide();
	
	if(maxScoreForVVI!="")
	{
		document.getElementById("wantScore").checked=true;
		$("#marksDiv").show();
	}
	
}

function showLinkDiv()
{
	var sendAutoVVILink 	= 	document.getElementById("sendAutoVVILink").checked;
	
	if(sendAutoVVILink==true)
		$("#autolinkDiv").show();
	else 
		$("#autolinkDiv").hide();
	
}

function getStatusListForVII()
{
	var districtId = trim(document.getElementById("districtOrSchooHiddenlId").value);
	var jobId = trim(document.getElementById("jobId").value);
	
	//alert(" jobId :: "+jobId+" districtId :: "+districtId);
	
	if(districtId!="")
	{
		ManageJobOrdersAjax.getStatusListForVVI(districtId,jobId,{ 
			async: false,
			callback: function(data)
			{
					if(data!=null)
						document.getElementById("statusDiv").innerHTML=data;	
			
			},errorHandler:handleError
		});
	}
	
}

function getVVIDefaultFields()
{
	var jobCategoryId = trim(document.getElementById("jobCategoryId").value);
	var jobId = trim(document.getElementById("jobId").value);
	
	if(jobCategoryId!="" && jobId=="")
	{
		clearVVIFields();
		var jobCate = jobCategoryId.split("||");
		
		$('#loadingDiv').show();
		ManageJobOrdersAjax.getVVIDefaultFields(jobCate[0],{ 
			async: true,
			callback: function(data)
			{
				if(data!=null)
				{
					if(data.offerVirtualVideoInterview==true)
					{
						// get All Status List
						getStatusList();
						$("#offerVVIDiv").show();
						document.getElementById('offerVirtualVideoInterview').checked=true;
				
						document.getElementById('quesId').value=data.i4QuestionSets.ID;
						document.getElementById('quesName').value=data.i4QuestionSets.questionSetText;
						
						if(data.maxScoreForVVI!=null && data.maxScoreForVVI!="")
						{
							$("#marksDiv").show();
							document.getElementById('wantScore').checked=true;
							
							document.getElementById('maxScoreForVVI').value=data.maxScoreForVVI;
						}
						else
						{
							$("#marksDiv").hide();
							document.getElementById('wantScore').checked=false;
						}
						
						if(data.sendAutoVVILink==true)
						{
							$("#autolinkDiv").show();
							document.getElementById('sendAutoVVILink').checked=true;
							
							if(data.jobCompletedVVILink==null){
								document.getElementById('jobCompletedVVILink1').checked=false;
								document.getElementById('jobCompletedVVILink2').checked=false;
							}else if(data.jobCompletedVVILink==false){
								document.getElementById('jobCompletedVVILink1').checked=true;
								var itemList = document.getElementById('slctStatusID');
								if(data.statusIdForAutoVVILink!=null)
								{
									for (var i = 0; i < itemList.length; i++) 
									{
										if (itemList.options[i].value== data.statusIdForAutoVVILink) {
											itemList.options[i].selected = true;
											break;
										}
									}
								}else if(data.secondaryStatusIdForAutoVVILink!=null)
								{
									for (var j = 0; j < itemList.length; j++) 
									{
										if (itemList.options[j].value== "SSID_"+data.secondaryStatusIdForAutoVVILink) {
											itemList.options[j].selected = true;
											break;
										}
									}
								}
							}else if(data.jobCompletedVVILink==true){
								document.getElementById('jobCompletedVVILink1').checked=false;
								document.getElementById('jobCompletedVVILink2').checked=true;
								document.getElementById('slctStatusID').value="";
							}
							
						}else
						{
							$("#autolinkDiv").hide();
						}
						
						document.getElementById('timePerQues').value=data.timeAllowedPerQuestion;
						document.getElementById('vviExpDays').value=data.VVIExpiresInDays;
						
					}else
					{
						$("#offerVVIDiv").hide();
					}
				}
				$('#loadingDiv').hide();
			},errorHandler:handleError
		});
	}
}

function clearVVIFields()
{
	document.getElementById('offerVirtualVideoInterview').checked=false;
	document.getElementById('quesId').value="";
	document.getElementById('quesName').value="";
	
	document.getElementById('wantScore').checked=false;
	document.getElementById('maxScoreForVVI').value="";
	
	document.getElementById('sendAutoVVILink').checked=false;
	document.getElementById('slctStatusID').value = "";
	
	document.getElementById('timePerQues').value="";
	document.getElementById('vviExpDays').value="";
	
	document.getElementById('offerAssessmentInviteOnly').checked=false;
	//document.getElementById('#attachedDAList').value="";
}

/* ========= End :: Virtual Video Interview ============*/

/************************************* Question Set auto complete**********************************************/
function getQuestionSetAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("quesName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		//searchArray = getDistrictMasterArray(txtSearch.value);
		searchArray = getQuesSetArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}


function getQuesSetArray(districtOrSchoolName)
{
	var searchArray = new Array();
	

	var districtId = trim(document.getElementById("districtOrSchooHiddenlId").value);
		
		if(districtId!="")
		{
			I4QuestionSetAjax.getFieldOfQuesList(districtOrSchoolName,districtId,{ async: false,callback: function(data)
				{
					hiddenDataArray = new Array();
					showDataArray = new Array();
					for(i=0;i<data.length;i++)
					{
						searchArray[i]=data[i].questionSetText;
						showDataArray[i]=data[i].questionSetText;
						hiddenDataArray[i]=data[i].ID;
					}
				},
			});
		}

	return searchArray;
}

function hideDistrictMasterDivForVVI(dis,hiddenId,divId)
{
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
	}
	
	if(document.getElementById(divId))
	{	
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}


/*************************************************************************************************/

/************************************* District Assessment auto complete**********************************************/
function getDistrictAssessmentAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("assessmentName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		//searchArray = getDistrictMasterArray(txtSearch.value);
		searchArray = getDistrictAssessmentArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}


function getDistrictAssessmentArray(distASMTName)
{
	var searchArray = new Array();
	
	var districtId = trim(document.getElementById("districtOrSchooHiddenlId").value);
	if(districtId!="")
	{
		DistrictAssessmentAjax.getFieldOfDistASMT(distASMTName,districtId,{ async: false,callback: function(data)
			{
				hiddenDataArray = new Array();
				showDataArray = new Array();
				for(i=0;i<data.length;i++)
				{
					searchArray[i]=data[i].districtAssessmentName;
					showDataArray[i]=data[i].districtAssessmentName;
					hiddenDataArray[i]=data[i].districtAssessmentId;
				}
			},
		});
	}

	return searchArray;
}

/*
function hideDistrictASMTDiv(dis,hiddenId,divId)
{
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
	}
	
	if(document.getElementById(divId))
	{	
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}*/

/*************************************************************************************************/



function chkTimeAndDayForVVI()
{
	var t = $("#timeAllowedPerQuestion").val();
	var d = $("#vviExpDays").val();
	
	if(t==0)
	{
		$("#timeAllowedPerQuestion").val("");
	}
	if(d==0)
	{
		$("#vviExpDays").val("");
	}
}


function displayAllDistrictAssessment()
{
	//alert('enteriii');
	var jobcategoryId=document.getElementById("jobCategoryId").value;
	var jobId=document.getElementById("jobId").value;
	var jobCate = jobcategoryId.split("||");
	
	if(jobCate[0]!='' && jobCate[0]>0)
	{
		ManageJobOrdersAjax.displayAllDistrictAssessment(jobCate[0],jobId,
		{ 
			async: true,
			errorHandler:handleError,
			callback: function(data)
			{
				var dataArr=data.split('||');
				document.getElementById("1stSchoolDiv").innerHTML=dataArr[0];
				document.getElementById("2ndSchoolDiv").innerHTML=dataArr[1];
				if(dataArr[2]=='true')
					{
						document.getElementById("offerAssessmentInviteOnly").checked=true;
						$('#multiDisAdminDiv').show();
					}
					else
					{
						document.getElementById("offerAssessmentInviteOnly").checked=false;
						$('#multiDisAdminDiv').hide();
					}
			}
		});
	}
	
}


function showOfferAMT()
{
	var offerVVI 		= 	document.getElementById("offerAssessmentInviteOnly").checked;
	
	if(offerVVI==true)
		$("#multiDisAdminDiv").show();
	else
		$("#multiDisAdminDiv").hide();
}

function chkJobSubCategory()
{
	var chksubCate = document.getElementById("chksubCate").value;
	
	if(chksubCate==1)
		$("#jobSubCateDiv").show();
	else
		$("#jobSubCateDiv").hide();
}

function unableOrDisableInviteJob()
{
	 var isInviteOnlyChecked=document.getElementById("isInviteOnly").checked;
	 if(isInviteOnlyChecked)
	 {
	   $('.hiddenInviteDiv').show();
	 }
	 else
	 {
		 $('.hiddenInviteDiv').hide();
		 $('input[name=hiddenJob]').attr('checked',false);
	 }
	 
}


function callForPrimaryESTech(){
  try{
        $('#jobDescriptionDocUpload').parents().parents('.row').css('display','none');
  }catch(e){}

	$('.onlyjeffco').show();
	setTimeout(function(){$('.iconpophover16').tooltip();},100);
	try{
		var flagforedit=false;
		var primaryESTechnicianHidden=$('[name="primaryESTechnicianHidden"]').val();
		//alert('primaryESTechnicianHidden==='+primaryESTechnicianHidden);
		if(primaryESTechnicianHidden!=null && primaryESTechnicianHidden!='undefined'){
			flagforedit=true;
		}
		ManageJobOrdersAjax.getPrimaryESTech({ 
					async: false,
					errorHandler:handleError,
					callback: function(data)
					{
						var option ="<option value='0'>Select Primary Technician</option>";						
						$.each(data,function(k,v){							
							if(flagforedit){
								if(primaryESTechnicianHidden.trim()==k){
									option+="<option value='"+k+"' selected='selected'>"+v+"</option>";
								}else{
									option+="<option value='"+k+"'>"+v+"</option>";
								}
							}else{
							option+="<option value='"+k+"'>"+v+"</option>";
							}
						});
						$('#primaryESTechnician').html(option);
						if(flagforedit){
							$('#primaryESTechnician').trigger("change");
						}
					}
				});		
	}catch(e){alert(e);
	}
	linkShowOrHide();
}

function setBackupESTechnician(obj){
	try{
		$('#backupESTechnician').html();
		//alert(obj);
		ManageJobOrdersAjax.getBackupESTech(obj.value,{ 
					async: true,
					errorHandler:handleError,
					callback: function(data)
					{
						var option ="";						
						$.each(data,function(k,v){
							option+="<option value='"+k+"'>"+v+"</option>";
						});
						$('#backupESTechnician').html(option);
					}
				});
	}catch(e){}
}


function jobShowDiscriptionOnchng()
{
	var refno= $("#refno").val();
	$(".jeffcoCombo").hide();
	if(refno=="2500" || refno=="2550"){
		$(".jeffcoCombo").show();
	}
}
function jobListDiscription()
{  
    var jobList = document.getElementById("jobList").value;
    try{
	if(jobList != 0){	
	    ManageJobOrdersAjax.populateJobDiscription(jobList,{ 
		async: true,
		errorHandler:handleError,
		callback: function(data){
	    	try{
				var dataHtml= replaceAll("&amp;#x13&amp;#x10;","<br/>",data);// jobDesc.replace(new RegExp("&amp;#x13&amp;#x10;", 'g'), '<br/>');
				dataHtml=replaceAll("&#x13&#x10;","<br/>",dataHtml);
			}catch(e){}
		 $('[name="jobDescription"]').parents().parents('.jqte').find(".jqte_editor").html(dataHtml);
	    }
	});
}
	}catch(e){}
}

function populateJobOrderInfo(jobId){
	$(".jeffcoCombo").hide();
	var districtRequisionNumber = document.getElementById("avlbList").value;
	if(isCloneJob){
		districtRequisionNumber=$('.reqPositionNum').val();
	}
	document.getElementById("refno").disabled = false;
	$('#avlbList').css("background-color", "");
	document.getElementById("jobTitle").value="";
	document.getElementById("refno").value="";
	$('[name="jobDescription"]').parents().parents('.jqte').find(".jqte_editor").text("");
	document.getElementById("jobType").value="F";
	document.getElementById("hourPrday").value="";

	

	document.getElementById("tempType").value= "0";
	
	if(!isCloneJob){
		document.getElementById("jobApplicationStatus").value= "0";
		document.getElementById("subjectId").value= "0";
		document.getElementById("jobStartDate").value= "";
		document.getElementById("jobEndDate").value= "";
		document.getElementById("positionStartDate").value= "";
		document.getElementById("positionEndDate").value= "";
	}
	
	document.getElementById("jobCategoryId").value="0";
	
	
	document.getElementById("tempType").value= "0";
	if(!isCloneJob){
		document.getElementById("jobApplicationStatus").value= "0";
		document.getElementById("subjectId").value= "0";
	    document.getElementById("jobStartDate").value= "";
		document.getElementById("jobEndDate").value= "";
		document.getElementById("positionStartDate").value= "";
		document.getElementById("positionEndDate").value= "";
    }
	$('[name="jobQualification"]').parents().parents('.jqte').find(".jqte_editor").text("");
	document.getElementById("jeffcoSpecialEdFlag").value="N";
	$("#jeffcoSpecialEdFlag").prop('disabled',true);
	
	$('#errordiv').empty();
	$('#loadingDiv').show();
		ManageJobOrdersAjax.populateJobOrderInfo(districtRequisionNumber,jobId,{
		async: true,
		errorHandler:handleError,
		callback:function(data){
	
		var json = JSON.parse(data);
		var status =json.status;
		
		if(status)
		{
			var jobCode = json.jobCode;
			var locationID = json.locationID;
			var fullPart = json.FullPart;
			var jobType = json.jobType;
			var jobCategory = json.jobCategory;
			var jobTitle = json.jobTitle;
			var jobDesc = json.jobDesc;
			var StdHours = json.StdHours;
			var reqMsg=json.reqMsg;
			var jobCatStatus = json.jobCatStatus;
			var schoolName = json.schoolName;
			jeffcoSchoolId = json.jeffcoSchoolId;
			specialEdFlag = json.SpecialEdFlag;
			
			var fte=json.fte;
			var payGrade=json.payGrade;
			var daysWorks=json.daysWorks;
			var fsalary=json.minRtHourly;
			var fsalaryA=json.minRtAnnual;
			var ssalary=json.maxRtAnnual;
			//alert(jeffcoSchoolId+"  ||  "+grantFlag+"  ||  "+specialEdFlag);
			if(jobCode=="2500" || jobCode=="2550"){
				$(".jeffcoCombo").show();
			}
			
			if(reqMsg=="success"){
				ShowSchoolTemp();
				document.getElementById("jeffcoSpcSchlName").value = schoolName;
				$('#jeffcoSpcSchlName').prop('disabled',true);
			}else{
				$('#errordiv').show();
				$('#errordiv').append("&#149; "+reqMsg+" <br>");
				jeffcoSchoolId = null;
				ShowSchoolTemp();
			}
			//$('[name="fte"]').prop('disabled',true);
			$('[name="fte"]').val(fte);
			/*if($('#entityType').val()==2){
				$('[name="fte"]').prop('disabled',false);
			}*/
			$('[name="paygrade"]').prop('disabled',true);
			$('[name="daysworked"]').prop('disabled',true);
			$('[name="fsalary"]').prop('disabled',true);
			$('[name="fsalaryA"]').prop('disabled',true);
			$('[name="ssalary"]').prop('disabled',true);
			
			$('[name="paygrade"]').val(payGrade);
			$('[name="daysworked"]').val(daysWorks);
			$('[name="fsalary"]').val(fsalary);
			$('[name="fsalaryA"]').val(fsalaryA);
			$('[name="ssalary"]').val(ssalary);
			document.getElementById("jobTitle").value=jobTitle;
			document.getElementById("refno").value=jobCode;
			document.getElementById("hourPrday").value=StdHours;
			document.getElementById("refno").disabled = true;
		
			if(fullPart=="F"){
				document.getElementById("jobType").value=fullPart;
			}
			if(fullPart=="P"){
				document.getElementById("jobType").value=fullPart;	
			}
			
			document.getElementById("jeffcoSpecialEdFlag").value=specialEdFlag;
			
			try{
				var dataHtml= replaceAll("&amp;#x13&amp;#x10;","<br/>",jobDesc);
				dataHtml=replaceAll("&#x13&#x10;","<br/>",dataHtml);
			}catch(e){}
			$('[name="jobDescription"]').parents().parents('.jqte').find(".jqte_editor").html(dataHtml);//();
			
			if(jobCatStatus)
			{
				var isSubJobCat = json.isSubJobCat;
				var parentJobCatName=json.parentJobCatName;
			//	var jeffcodistrictId=document.getElementById("districtOrSchooHiddenlId").value;
				if(isSubJobCat){
					var subJobCatName = json.subJobCatName;
					var jobCategoryIdJson = json.jobCategoryId;
					//alert("Parent: "+parentJobCatName+" Sub: "+subJobCatName);
					$("#jobCategoryId option:contains("+parentJobCatName+")").attr('selected', 'selected');
					$('#jobCategoryId').trigger("change");
					var subJobCatId = json.subJobCategoryId;
					getJobSubcategoryByJobcode(jobCategoryIdJson,subJobCatName);
					//if(jeffcodistrictId=='804800')
						setTimeout(function(){$("#jobSubCategoryId").prop('disabled',true);$("#jobCategoryId").prop('disabled',true);},1000);
				}else{
					$("#jobCategoryId option:contains("+parentJobCatName+")").attr('selected', 'selected');
					$('#jobCategoryId').trigger("change");
					//if(jeffcodistrictId=='804800')
						setTimeout(function(){$("#jobSubCategoryId").prop('disabled',true);$("#jobCategoryId").prop('disabled',true);},1000);
					//$("#jobSubCateDiv").hide();
				}
			}else{
				setTimeout(function(){$("#jobCategoryId").prop('disabled',false);$("#jobSubCategoryId").prop('disabled',false);},1010);
				/*$('#errordiv').show();
				$('#errordiv').append("&#149; Job Category Name, "+jobCategory+" does not exist. <br>");
				$('#jobCategoryId').css("background-color", "#F5E7E1");*/
			}
		}else{
			var DESCR_4_WRONG_POSITION = json.DESCR_4_WRONG_POSITION;
			$('#errordiv').show();
			$('#errordiv').append("&#149; "+DESCR_4_WRONG_POSITION+". <br>");
			ShowSchoolTemp();
		}
		if(reqMsg!="success"){
			$('[name="paygrade"]').val('');
			$('[name="daysworked"]').val('');
			$('[name="fsalary"]').val('');
			$('[name="fsalaryA"]').val('');
			$('[name="ssalary"]').val('');
			$('[name="paygrade"]').prop('disabled',false);
			$('[name="daysworked"]').prop('disabled',false);
			$('[name="fsalary"]').prop('disabled',false);
			$('[name="fsalaryA"]').prop('disabled',false);
			$('[name="ssalary"]').prop('disabled',false);
			$('#jeffcoSpcSchlName').val('');
			$('#jeffcoSpcSchlName').prop('disabled',false);
			$('#jeffcoSpcSchlName').prop('readonly',false);
			setTimeout(function(){$("#jobCategoryId").prop('disabled',false);$("#jobSubCategoryId").prop('disabled',false);},1012);
			$("#jeffcoSpecialEdFlag").prop('disabled',false);
		}
		$('#loadingDiv').hide();
	}
	});	
	linkShowOrHide();
}

function getJobSubcategoryByJobcode(jobCategoryId,subJobCatName)
{
	var jobcategoryId=jobCategoryId;
	var jobId=document.getElementById("jobId").value;
	var districtId = trim(document.getElementById("districtOrSchooHiddenlId").value);
	var clonejobId = document.getElementById("clonejobId").value;
	
	if(document.getElementById("clonejobId").value=="")
		clonejobId = 0;
	
	if(districtId!="")
	{
		var url =window.location.href;
		 if(url.indexOf("editjobordertest.do") > -1){
			 ManageJobOrdersAjax.getJobSubCategory_New_Op(jobId,jobcategoryId,districtId,clonejobId,
						{ 
							async: false,
							errorHandler:handleError,
							callback: function(data)
							{
								if(data!='')
								{
									$("#jobSubCateDiv").show();
									document.getElementById("jobSubCategoryDiv").innerHTML=data;
									$("#jobSubCategoryId option:contains("+subJobCatName+")").attr('selected','selected');
								}
								else
								{
									$("#jobSubCateDiv").hide();
								}
								hideEditJobCategoryAndSubCategory();
							}
						}); 
		 }
		 else{
			ManageJobOrdersAjax.getJobSubCategory(jobId,jobcategoryId,districtId,clonejobId,
			{ 
				async: false,
				errorHandler:handleError,
				callback: function(data)
				{
					if(data!='')
					{
						$("#jobSubCateDiv").show();
						document.getElementById("jobSubCategoryDiv").innerHTML=data;
						$("#jobSubCategoryId option:contains("+subJobCatName+")").attr('selected','selected');
					}
					else
					{
						$("#jobSubCateDiv").hide();
					}
					hideEditJobCategoryAndSubCategory();
				}
			});
		 }
			if(($('#errordiv').html().indexOf("does not exist") > -1) && districtId.trim()=='804800'){
				$("#jobSubCategoryId").prop('disabled',false);
			}
	}
}


function showHideVisibilityOFTemp(){
	var statuID = document.getElementById("jobApplicationStatus").value;
	$('#temporaryType').hide();
	$('#posEndDateDiv').show();	
	if(statuID!=0){
		if(statuID!=1){
			$('#temporaryType').show();
			$('#changeCss1').removeClass('col-sm-3 col-md-3').addClass('col-sm-2 col-md-2');
			$('#changeCss2').removeClass('col-sm-3 col-md-3').addClass('col-sm-2 col-md-2');
			/*unDisableEndDate();
			document.getElementById("endDateRadio2").checked=false;
			document.getElementById("endDateRadio1").checked=true;*/
			$('#posEndDateDiv').show();
		}else{
			$('#changeCss1').removeClass('col-sm-2 col-md-2').addClass('col-sm-3 col-md-3');
			$('#changeCss2').removeClass('col-sm-2 col-md-2').addClass('col-sm-3 col-md-3');
			$('#temporaryType').hide();
			$('#posEndDateDiv').hide();		
			/*disableEndDate();
			document.getElementById("endDateRadio2").checked=true;*/
		}
	}
}

function onlyHour(evt, element) {

	var count=true;
	var str=evt.srcElement.value;
	if (str.lastIndexOf('.') == -1) str += ".";
	var decNum = str.substring(str.lastIndexOf('.')+1, str.length);
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==8)
		return true;

	var parts = evt.srcElement.value.split('.');
    if(parts.length > 1 && charCode==46)
        return false;
    
	return( ((charCode >= 48 || charCode==46)&& count && charCode <= 57)||(charCode==13) );
}    

$(function(){
	 $('#hourPrday').keypress(function (event) {
         
         return onlyHour(event, this);

     });  
});


function autoPostingEndTime() {
  var districtId=$('#districtOrSchooHiddenlId').val();  
  var startDate = ""+document.getElementById('jobStartDate').value;
  var selectedDate=startDate;
  var selectedDateArray = selectedDate.split("-");
  var enddate = new Date(selectedDateArray[2], parseInt(selectedDateArray[0]) - 1, selectedDateArray[1]);
  var newdate = enddate;
  if(districtId=='804800'){
  newdate.setDate(newdate.getDate() + 7);
  var yyyyN = newdate.getFullYear();
  var mmN = newdate.getMonth() < 9 ? "0" + (newdate.getMonth() + 1) : (newdate.getMonth() + 1); // getMonth() is zero-based
  var ddN  = newdate.getDate() < 10 ? "0" + newdate.getDate() : newdate.getDate();
  var new2date = mmN+"-"+ddN+"-"+yyyyN;
  var statuID = document.getElementById("jobApplicationStatus").value;
  document.getElementById('jobEndDate').value = new2date;
  }
}


function replaceAll(find, replace, str) {
    return str.replace(new RegExp(find, 'g'), replace);
  }

function saveSchoolId()
{
	var schoolId = $("#schoolId").val();
	$("#tempSchoolId").val(schoolId);
	//alert("Now School Name:- "+schoolId);
}

function addIdIntoSchoolList()
{
	var tempSchoolId =  $("#tempSchoolId").val();
	var tempAllSchoolId = $("#tempAllSchoolId").val();
	
	if(tempAllSchoolId=="")
		tempAllSchoolId = tempSchoolId;
	else
		tempAllSchoolId = tempAllSchoolId+","+tempSchoolId;
	
	$("#tempAllSchoolId").val(tempAllSchoolId);
}

function showApprovalGroupDetails(jobId)
{

	var schoolId = "";
	if(jeffcoSchoolId!=null)
		schoolId = jeffcoSchoolId;
	
	var otherSChoolId = $("#tempAllSchoolId").val();
	if(schoolId!=null & schoolId!="")
	{
		if(otherSChoolId!=null & otherSChoolId!="")
			schoolId = schoolId+","+otherSChoolId;
	}
	

	if(jobId==null)
		jobId=0;
	
	//alert("jobId:- "+jobId);
	
	//var schoolId = "";
	//if(jeffcoSchoolId!=null)
	
	var schoolId=jeffcoSchoolId;
	var otherSChoolId = $("#tempAllSchoolId").val();
	if(schoolId!=null & schoolId!="")
	{
		if(otherSChoolId!=null & otherSChoolId!="")
			schoolId = schoolId+","+otherSChoolId;
	}
	

	var primaryESTechnician = $("#primaryESTechnician").val();
	var backupESTechnician = $("#backupESTechnician").val();
	var specialEdFlagVal = false; 
	
	var sg = $("#jeffcoSpecialEdFlag").val();
	
	if(sg!=null & sg!="")
	{
		if(sg=="Y")
			specialEdFlagVal=true;
	}
	
	//alert("schoolId:-"+schoolId+"\n primaryESTechnician:- "+primaryESTechnician+"\n backupESTechnician:- "+backupESTechnician+"\ngrantFlagVal:- "+grantFlagVal+"\nspecialEdFlagVal:- "+specialEdFlagVal);
	
	if(/*schoolId!=null & schoolId!="" & */primaryESTechnician!=null & primaryESTechnician!="" & backupESTechnician!=null & backupESTechnician!="")
	{
		//alert("inside if");
		ManageJobOrdersAjax.getApprovalInfoDetails(schoolId,primaryESTechnician,backupESTechnician,specialEdFlagVal,jobId,$('#districtOrSchooHiddenlId').val(),{ 
			async: false,
			errorHandler:handleError,
			callback: function(data)
			{
				$('#loadingDiv').hide();
				$('#info322 .modal-body').html(data);
				$('#info322').modal('show');
			}
		});
	}
	else
	{
		$('#virusDivId .modal-body').html('Please select ES Tech.');
		$('#virusDivId').modal('show');
	}
}

/*function validateApprovalGroupDetails()
{
	
	validateAddEditJobOrder();
	removeJobDescriptionFile(1);
}
*/

//Gaurav Kumar stars for Adams 12
/*function validateApprovalGroupDetails()
{
	
	//validateAddEditJobOrder();
	//removeJobDescriptionFile(1);
	//alert("jobId:- "+jobId);
	
	var schoolId=jeffcoSchoolId;
	var otherSChoolId = $("#tempAllSchoolId").val();
	if(schoolId!=null & schoolId!="")
	{
		if(otherSChoolId!=null & otherSChoolId!="")
			schoolId = schoolId+","+otherSChoolId;
	}
	
	//You may hide
	if(schoolId==null)
		 schoolId="";
	
	if(otherSChoolId!=null & otherSChoolId!="")
		schoolId = schoolId+","+otherSChoolId;
	
	var primaryESTechnician = $("#primaryESTechnician").val();
	var backupESTechnician = $("#backupESTechnician").val();
	var specialEdFlagVal = "N"; 
	
	var sg = $("#jeffcoSpecialEdFlag").val();
	
	if(sg!=null & sg!="")
	{
		if(sg=="Y")
			specialEdFlagVal="Y";
	}
	
	//alert("schoolId:-"+schoolId+"\n primaryESTechnician:- "+primaryESTechnician+"\n backupESTechnician:- "+backupESTechnician+"\ngrantFlagVal:- "+grantFlagVal+"\nspecialEdFlagVal:- "+specialEdFlagVal);
	
	//grantFlagVal="Y";
	//specialEdFlagVal="Y";
	
	$('#loadingDiv').show();
	ManageJobOrdersAjax.validateApprovalGroupDetails(schoolId,specialEdFlagVal,{ 
			async: false,
			errorHandler:handleError,
			callback: function(data)
			{
				if(data=="########")
				{
					$('#loadingDiv').hide();
					validateAddEditJobOrder();
					removeJobDescriptionFile(1);
				}
				else
				{
					var ay = data.split("####");
					var msg = "";
					var membernotExist = "";
					var groupnotexist = "";
					schoolnotAttatched = "";
					
					if(ay[0]!="")
						msg = "No member found in "+ay[0].substring(1)+"<br>";
					
					if(ay[1]!="")
						msg = msg+(ay[1].substring(1))+" does not exist.<br>";
					
					if(ay[2]!="")
						msg = msg+"No School Admin is attach in the School '"+ay[2]+"'";
					
					
					$('#loadingDiv').hide();
					$('#info322 .modal-body').html(msg);
					$('#info322').modal('show');
					return false;
				}
			}
		});
}*/


function validateApprovalGroupDetails()
{
	var districtId=$('#districtOrSchooHiddenlId').val();
	if(districtId==806900){
		validateAddEditJobOrder();
		removeJobDescriptionFile(1);	
	}else{

	//validateAddEditJobOrder();
	//removeJobDescriptionFile(1);
	//alert("jobId:- "+jobId);
	
	var schoolId=jeffcoSchoolId;
	var otherSChoolId = $("#tempAllSchoolId").val();
	if(schoolId!=null & schoolId!="")
	{
		if(otherSChoolId!=null & otherSChoolId!="")
			schoolId = schoolId+","+otherSChoolId;
	}
	
	//You may hide
	if(schoolId==null)
		 schoolId="";
	
	if(otherSChoolId!=null & otherSChoolId!="")
		schoolId = schoolId+","+otherSChoolId;
	
	var primaryESTechnician = $("#primaryESTechnician").val();
	var backupESTechnician = $("#backupESTechnician").val();
	var specialEdFlagVal = "N"; 
	
	var sg = $("#jeffcoSpecialEdFlag").val();
	
	if(sg!=null & sg!="")
	{
		if(sg=="Y")
			specialEdFlagVal="Y";
	}
	
	//alert("schoolId:-"+schoolId+"\n primaryESTechnician:- "+primaryESTechnician+"\n backupESTechnician:- "+backupESTechnician+"\ngrantFlagVal:- "+grantFlagVal+"\nspecialEdFlagVal:- "+specialEdFlagVal);
	
	//grantFlagVal="Y";
	//specialEdFlagVal="Y";
	
	$('#loadingDiv').show();
	ManageJobOrdersAjax.validateApprovalGroupDetails(schoolId,specialEdFlagVal,{ 
			async: false,
			errorHandler:handleError,
			callback: function(data)
			{
				if(data=="########")
				{
					$('#loadingDiv').hide();
					validateAddEditJobOrder();
					removeJobDescriptionFile(1);
				}
				else
				{
					var ay = data.split("####");
					var msg = "";
					var membernotExist = "";
					var groupnotexist = "";
					schoolnotAttatched = "";
					
					if(ay[0]!="")
						msg = "No member found in "+ay[0].substring(1)+"<br>";
					
					if(ay[1]!="")
						msg = msg+(ay[1].substring(1))+" does not exist.<br>";
					
					if(ay[2]!="")
						msg = msg+"No School Admin is attach in the School '"+ay[2]+"'";
					
					
					$('#loadingDiv').hide();
					$('#info322 .modal-body').html(msg);
					$('#info322').modal('show');
					return false;
				}
			}
		});
	}
}

//End Gaurav kumar code

function linkShowOrHide(){
	var linkShowFlag="";
	try{
		linkShowFlag=$('#jobCategoryId').find(":selected").attr('linkShow');
	}catch(ee){}
	try{
		linkShowFlag=$('#jobSubCategoryId').find(":selected").attr('linkShow');
		var subCategoryValue=$('#jobSubCategoryId').val();
		if(subCategoryValue==0){
			linkShowFlag=$('#jobCategoryId').find(":selected").attr('linkShow');
		}
	}catch(ee){}
	try{
		if(linkShowFlag!="" && linkShowFlag!='false' && linkShowFlag!="null"){
			$('.linkApprovalDetails').show();
		}else{
			$('.linkApprovalDetails').hide();
		}
	}catch(ee){};
	try{
	var jobCategoryText=$('#jobCategoryId').find(":selected").text().trim();
	var jobId=$('#jobId').val().trim();
		if(jobCategoryText.indexOf('Hourly') >= 0){
			$('#extraFieldJeffco').show();
			$('#ssalaryId').hide();
			$('#fsalaryId').show();
			$('[name="fsalary"]').show();
			$('[name="fsalaryA"]').hide();
		}else if(jobCategoryText.indexOf('Administrator/Professional Technical') >= 0){
			$('#extraFieldJeffco').show();
			$('#fsalaryId').show();
			$('#ssalaryId').show();
			$('[name="fsalary"]').hide();
			$('[name="fsalaryA"]').show();
		}else if($('#jobCategoryId').find(":selected").val().trim()=='0'){
			$('#extraFieldJeffco').hide();
		}else{
			$('#extraFieldJeffco').show();
			$('#fsalaryId').hide();
			$('#ssalaryId').hide();
		}
	}catch(e){}
}

$(document).ready(function(){
	hideEditJobCategoryAndSubCategory();
	/*$('[name="avlbList"]').focus(function(){
		getConfirmEditOrNot("Search by Position Number/School Name",insertDivSearch ,"Ok");
	});*/
	$('#idFilter').click(function(){
		getConfirmEditOrNot("Search by Position Number/School Name",insertDivSearch ,"Ok");
	});
});
function hideEditJobCategoryAndSubCategory(){
	try{
		var jobId=$("#jobId").val();
			if(jobId!=null && jobId!=undefined && jobId!=0){
				$('#jobCategoryId').attr('disabled','disabled');
				//setTimeout(function(){
					$('#jobSubCategoryId').attr('disabled','disabled');
					//},2000);
			}else{
				//hafeez
			//$('#jobSubCategoryId').attr('disabled','disabled');
			}
		}catch(ee){}
}

var positionNumberDiv='<div class="col-sm-12 col-md-14 positionClass" id="positionClass" style="padding:0px;">'+
						'<div class="col-md-12 top5" style="margin-top:15px;">'+
							'<label for="text" style="float:left;margin-right:5px;"><b>Position Number</b></label>'+
								'<div style="float:right;width:390px;">'+
									'<input class="form-control clearback" type="text" id="onlyPositionNumName" name="onlyPositionNumName"   value=""'+
										'onfocus="getOnlyPositionNumAutoComp(this, event, \'divTxtPositionNumData\', \'onlyPositionNumName\',\'positionNumHiddenId\',\'\');"'+
										'onkeyup="getOnlyPositionNumAutoComp(this, event, \'divTxtPositionNumData\', \'onlyPositionNumName\',\'positionNumHiddenId\',\'\');"'+
										'onblur="hidePositionNumDiv(this,\'positionNumHiddenId\',\'divTxtPositionNumData\');"	/>'+					
										'<div id=\'divTxtPositionNumData\' style=\' display:none;position:absolute;\'  onmouseover="mouseOverChk(\'divTxtPositionNumData\',\'onlyPositionNumName\')"  class=\'result\' ></div>'+
										'<input type="hidden" name="positionNumHiddenId" id="positionNumHiddenId" value=""/>'+		
								'</div>'+
						 '</div>'+
					 '</div>';
var schoolIdDiv='<div class="col-sm-12 col-md-14 SClass" id="SClass" style="padding:0px;">'+
    				'<div class="col-md-12 top5" style="margin-top:15px;">'+
    					'<label for="text" style="float:left;margin-right:5px;"><b>School Name</b></label>'+
    						'<div style="float:right;width:390px;">'+
    							'<input class="form-control clearback" type="text" id="onlySchoolName" name="onlySchoolName"   value=""'+
    								'onfocus="getSchoolMasterInDRNumAutoComp(this, event,\'divTxtSchoolData\', \'onlySchoolName\',\'schoolHiddenId\',\'\');"'+
    								'onkeyup="getSchoolMasterInDRNumAutoComp(this, event, \'divTxtSchoolData\', \'onlySchoolName\',\'schoolHiddenId\',\'\');"'+
    								'onblur="hideSchoolMasterInDRNumDiv(this,\'schoolHiddenId\',\'divTxtSchoolData\');"	/>'+
    								'<div id=\'divTxtSchoolData\' style=\' display:none;position:absolute;\'  onmouseover="mouseOverChk(\'divTxtSchoolData\',\'onlySchoolName\')"  class=\'result\' ></div>'+
    								'<input type="hidden" name="schoolHiddenId" id="schoolHiddenId" name="schoolHiddenId" value=""/>'+	
    						'</div>'+
    				'</div>'+
				'</div>';
var  insertDivSearch='<div class="row searchRow">'+
						'<div class="control-group">'+
							'<div class=\'divErrorMsg col-sm-12 col-md-12\' id=\'searchErrorMsg\' ></div>'+
							'<div class="col-sm-12 col-md-12" id="searchByRadioButton">'+
								'<label class="radio" style="margin:0px;">'+
									'<input type="radio" name="searchByPosOrSch" value="position" onchange="change(this);"/>&nbsp;&nbsp;&nbsp;&nbsp;Search by Position Number'+
								'</label>'+
								'<label class="radio" style="margin:0px;">'+
									'<input type="radio" name="searchByPosOrSch" value="school" onchange="change(this);"/>&nbsp;&nbsp;&nbsp;&nbsp;Search by School Name'+
								'</label>'+
							'</div>'+
						'</div>'+
					  '</div>'+
					  '<div class="row">'+
					     '<div class="optionAllSearch col-sm-12 col-md-12 mt15"></div>'+
					  '</div>';

function change(obj){
	$('#searchErrorMsg').html('');
	try{
		$('#positionClass').remove();
	}catch(e){}
	try{
		$('#SClass').remove();
	}catch(e){}
    if (obj.value == 'position') {
    	$('.optionAllSearch').html('');
    	$('#searchByRadioButton').css('padding-bottom:10px;');
        $('#searchByRadioButton').after(positionNumberDiv);
        $('[name="onlyPositionNumName"]').focus();
    }
    else if (obj.value == 'school') {
    	$('.optionAllSearch').html('');
    	$('#searchByRadioButton').css('padding-bottom:10px;');
    	$('#searchByRadioButton').after(schoolIdDiv);    	
    	if($('#entityType').val()==3){
    		var fillSchoolId=$('[name="fillSchoolId"]').val();
    		var fillSchoolName=$('[name="fillSchoolName"]').val();
    		$('[name="onlySchoolName"]').val(fillSchoolName);
    		$('[name="schoolHiddenId"]').val(fillSchoolId);
    		$('[name="onlySchoolName"]').prop('disabled',true);
    		searchPositionOrSchool('school',$('[name="schoolHiddenId"]').val());
    	}else{
    		$('[name="onlySchoolName"]').prop('disabled',false);
    		$('[name="onlySchoolName"]').focus();
    	}
    }
}

function getConfirmEditOrNot(header,confirmMessage,buttonNameChange){
	$('#searchErrorMsg').hide();
	var div="<div class='modal hide in' id='searchBox' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'>"+
			"<div class='modal-dialog' style='width:550px;height:500px;'>"+
			"<div class='modal-content'>"+
			"<div class='modal-header'>"+
	        "<button type='button' class='close confirmFalse' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>"+
	        "<h4 class='modal-title' id='myModalLabel' style='color:white;'></h4>"+
	      "</div>"+
	      "<div class='modal-body' id='confirmMessage'>"+	      
	      "</div>"+
	      "<div class='modal-footer'>"+
	      "<button type='button' class='btn btn-primary' id='confirmTrue' ></button>"+
	        "<button type='button' class='btn btn-default confirmFalse' id=''>"+resourceJSON.btnCancel+"</button>"+	        
	      "</div>"+
	    "</div>"+
	  "</div>"+
	"</div>";
	$('#loadingDiv').after(div);
    confirmMessage = confirmMessage || '';
    $('#searchBox').modal({show:true,
                            backdrop: "static",
                            keyboard: false,
    });

    $('#searchBox #confirmMessage').html(confirmMessage);
    $('#searchBox #myModalLabel').html(header);
    $('#searchBox #confirmTrue').html(buttonNameChange);
    $('.confirmFalse').click(function(){
    	$('#searchBox').modal('hide');
        $('#searchBox').remove();
    });
    $('#confirmTrue').click(function(){
    	var okFlag=true;
    	var schoolOrPosition="";
    	var positionOrSchoolBy=$('input:radio[name="searchByPosOrSch"]:checked').val();
    	$('#searchErrorMsg').empty();	
    	var positionNameSearch='';
    	var positionNumberSearch='';
    	var schoolNameSearch='';
    	var schoolIdSearch='';
    	if(positionOrSchoolBy=='school'){
    		schoolNameSearch=$('[name="onlySchoolName"]').val();
        	schoolIdSearch=$('[name="schoolHiddenId"]').val();
        	if(okFlag && (schoolNameSearch.trim()=='' || schoolIdSearch.trim()=='')){
        		$('#searchErrorMsg').show();
        		$('#searchErrorMsg').append("&#149; Please enter School Name<br>");
        		$('[name="onlySchoolName"]').focus();
        		okFlag=false;
        	}else{
        		if($('input:radio[name="selectedPositionOrSchool"]').is(':checked')){
        		positionNumberSearch=$('input:radio[name="selectedPositionOrSchool"]:checked').val();
        		var id=$('input[type=radio][name=selectedPositionOrSchool]:checked').attr('id');
        		positionNameSearch=$('#'+positionNumberSearch+'_'+id).html().trim().split(">")[1].trim();
        		positionNameSearch=replaceAll("&nbsp;","",positionNameSearch);
        		}else{
        			$('#searchErrorMsg').show();
            		$('#searchErrorMsg').append("&#149; Please select Position Number<br>");
            		$('[name="selectedPositionOrSchool"]:eq(0)').focus();
            		okFlag=false;
        		}
        	}
    	}else if (positionOrSchoolBy=='position'){
    		positionNameSearch=$('[name="onlyPositionNumName"]').val();
        	positionNumberSearch=$('[name="positionNumHiddenId"]').val();
        	if(okFlag && (positionNameSearch.trim()=='' || positionNumberSearch.trim()=='')){
        		$('#searchErrorMsg').show();
        		$('#searchErrorMsg').append("&#149; Please enter Position Number<br>");
        		$('[name="onlyPositionNumName"]').focus();
        		okFlag=false;
        	}else{
        		if(!$('input:radio[name="selectedPositionOrSchool"]').is(':checked')){
        			$('#searchErrorMsg').show();
            		$('#searchErrorMsg').append("&#149; Please select School Name<br>");
            		$('[name="selectedPositionOrSchool"].eq(0)').focus();
            		okFlag=false;
        		}
        	}
    	} 	
    	if(okFlag){
    		/*var option ='<option value="'+positionNumberSearch+'">'+positionNameSearch+'</option>';
	    	$('[name="avlbList"]').html(option);//*/	
    		$('[name="avlbList"]').val(positionNumberSearch);
	    	$('[name="avlbList"]').trigger("change");
	    	$('#searchBox').modal('hide');
	        $('#searchBox').remove();
    	}
    });
} 

function searchPositionOrSchool(positionOrSchoolBy,positionOrSchool){
	var allRadioPositionOrSchool="";
	ManageJobOrdersAjax.getAllRadioPositionOrSchool(positionOrSchoolBy,positionOrSchool,{ 
		preHook:function(){$('#loadingDiv').show()},
		postHook:function(){$('#loadingDiv').hide()},
		async: false,
		callback: function(data){
		var dataLength=data.length;
		var height ="";
		if(dataLength>10){
			height="height:200px;overflow:auto;";
		}
		if(positionOrSchoolBy=='school'){
			allRadioPositionOrSchool="<div><div><B>Select Position Number</B></div><div style='"+height+"'>";
		for(i=0;i<dataLength;i++){	
			var searchData=data[i].requisitionNumber+" ("+data[i].jobTitle+")";
			allRadioPositionOrSchool+='<label class="radio" style="margin:0px;" id="'+data[i].requisitionNumber+'_'+i+'"><input type="radio" name="selectedPositionOrSchool" id="'+i+'" value="'+data[i].requisitionNumber+'" "/>&nbsp;&nbsp;&nbsp;&nbsp;'+searchData+'</label>';		
		}
		allRadioPositionOrSchool+="</div>";
		$('.optionAllSearch').html(allRadioPositionOrSchool);
		}
		if(positionOrSchoolBy=='position'){
			allRadioPositionOrSchool="<div><div><B>Select School Name</B></div><div>";
			for(i=0;i<dataLength;i++){	
				allRadioPositionOrSchool+='<label class="radio" style="margin:0px;"><input type="radio" name="selectedPositionOrSchool" value="'+data[i].schoolId+'" "/>&nbsp;&nbsp;&nbsp;&nbsp;'+data[i].schoolName+'</label>';			
			}
			allRadioPositionOrSchool+="</div>";
			$('.optionAllSearch').html(allRadioPositionOrSchool);
		}
	},
	errorHandler:handleError
	});	
}

/***************************************************************auto complete ******************************************************/
function getOnlyPositionNumAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		document.getElementById("onlyPositionNumName").focus();		
	} 
	else if(event.keyCode==9) // Tab
	{
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getHeadQuarterOnlyArray(txtSearch.value);
		fatchData2(txtSearch,searchArray,txtId,txtdivid);
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}
function getOnlyPositionNumAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		document.getElementById("onlyPositionNumName").focus();
	} 
	else if(event.keyCode==9) // Tab
	{
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getPositionNumOnlyArray(txtSearch.value);;
		fatchData2(txtSearch,searchArray,txtId,txtdivid);
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}
function getPositionNumOnlyArray(positionNumName){
	var searchArray = new Array();
	var schoolId="";
	try{schoolId=$("#fillSchoolId").val();}catch(e){}
		ManageJobOrdersAjax.getFieldOfPositionNumList(positionNumName,schoolId,{ 
			async: false,
			callback: function(data){
			hiddenDataArray = new Array();
			showDataArray = new Array();
			for(i=0;i<data.length;i++){		
				var searchData=data[i].requisitionNumber+" ("+data[i].jobTitle+")";
				searchArray[i]=searchData;
				showDataArray[i]=searchData;
				hiddenDataArray[i]=data[i].requisitionNumber;
			}
		},
		errorHandler:handleError
		});	
	return searchArray;
}
function hidePositionNumDiv(dis,hiddenId,divId)
{	
	$('#searchErrorMsg').empty();
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
			$("#positionNumHiddenId").val(hiddenDataArray[index]);
			var positionOrSchoolBy=$('input:radio[name="searchByPosOrSch"]:checked').val();
			searchPositionOrSchool(positionOrSchoolBy,$('#positionNumHiddenId').val());			
		}
		if(dis.value==""){				
				document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			$('#onlyPositionNumName').attr('readonly', false);
			dis.value=showDataArray[index];
			document.getElementById("positionNumHiddenId").value=hiddenDataArray[index];
			document.getElementById("positionNumHiddenId").value=hiddenDataArray[index];			
		}
	}else{
		if(document.getElementById(hiddenId))
		{
			document.getElementById(hiddenId).value="";
		}
		if(dis.value!=""){
			var focuschk=0;
			onlyDistrictName=2;
			$('#searchErrorMsg').empty();	
			$('#searchErrorMsg').show();			
				$('#searchErrorMsg').append("&#149; Please enter valid Position Number<br>");
			if(focus==0)
			$('#onlyPositionNumName').focus();			
			focus++;
		}
	}
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
	if($('#positionNumHiddenId').val().trim()=="")
	 $('.optionAllSearch').html('');	
}

function getSchoolMasterInDRNumAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		document.getElementById("onlySchoolName").focus();
	} 
	else if(event.keyCode==9) // Tab
	{
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getSchoolOnlyArray(txtSearch.value);
		fatchData2(txtSearch,searchArray,txtId,txtdivid);		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}
function getSchoolMasterInDRNumAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	document.getElementById("schoolHiddenId").value="";
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		document.getElementById("onlySchoolName").focus();
	} 
	else if(event.keyCode==9) // Tab
	{
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getSchoolOnlyArray(txtSearch.value);
		fatchData2(txtSearch,searchArray,txtId,txtdivid);
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}
function getSchoolOnlyArray(schoolName){
	
	var searchArray = new Array();
	var positionNum="";
	try{positionNum=$('#positionNumHiddenId').val();}catch(e){}
	ManageJobOrdersAjax.getFieldOfSchoolNameList(schoolName,positionNum,{ 
			async: false,
			callback: function(data){
			hiddenDataArray = new Array();
			showDataArray = new Array();
			for(i=0;i<data.length;i++){				
				searchArray[i]=data[i].schoolName;
				showDataArray[i]=data[i].schoolName;
				hiddenDataArray[i]=data[i].schoolId;
			}
		},
		errorHandler:handleError
		});	
	return searchArray;
}
function hideSchoolMasterInDRNumDiv(dis,hiddenId,divId)
{
	$('#searchErrorMsg').empty();
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
			document.getElementById("schoolHiddenId").value=hiddenDataArray[index];
			var positionOrSchoolBy=$('input:radio[name="searchByPosOrSch"]:checked').val();
			searchPositionOrSchool(positionOrSchoolBy,$('#schoolHiddenId').val());
		}
		if(dis.value==""){				
				document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			$('#onlySchoolName').attr('readonly', false);
			dis.value=showDataArray[index];
			document.getElementById("schoolHiddenId").value=hiddenDataArray[index];
			document.getElementById("schoolHiddenId").value=hiddenDataArray[index];
		}
	}else{
		if(document.getElementById(hiddenId))
		{
			document.getElementById(hiddenId).value="";
		}
		if(dis.value!=""){
			var focuschk=0;
			onlyDistrictName=2;
			$('#searchErrorMsg').empty();	
			$('#searchErrorMsg').show();
				$('#searchErrorMsg').append("&#149; Please enter valid School Name<br>");
			if(focus==0)
			$('#onlySchoolName').focus();			
			focus++;
		}
	}
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
	if($('#schoolHiddenId').val().trim()=="")
		$('.optionAllSearch').html('');	
}


function showschoolTypeDiv(){
	var disIdforStrive	=	$('#districtOrSchooHiddenlId').val();
//	alert("in the function showschoolTypeDiv(){    "+disIdforjeffco);
	if(disIdforStrive!="" && disIdforStrive=='7800047')
		document.getElementById("schoolTyleDiv").style.display="block";
	else
		document.getElementById("schoolTyleDiv").style.display="none";
}
function selectSchoolType(){
	
	var schoolType	=	$('#schoolTypeeIdd').val();
	var disIdforStrive	=	$('#districtOrSchooHiddenlId').val();
	//alert("in the function showschoolTypeDiv(){    "+schoolType);
	if(disIdforStrive!="" && disIdforStrive=='7800047'){
		document.getElementById("selSclTyTId").value=schoolType;
			
	
	}
}
function keydownAmount(e,obj) {
	var amount=$(obj).val().split(".");
 	var keynum = ( e.which ) ? e.which : e.keyCode;
 	if(keynum==8)return true;
    var keychar = String.fromCharCode(keynum).replace(/[^0-9]/g, '');
    if(amount.length<2 && keynum==46)return true;
    //only two decimal point
    //if(amount.length>1 && amount[1].length>1)return false;
    if(keychar.trim()!='') 	return true;  else return false;
}

function setStatusToActive(status){
	document.getElementById("statusActiveOrNot").value=status;
	validateAddEditJobOrder();
	removeJobDescriptionFile(1);
	actionForward();
}
 
/***************************************************************End********************************************************************/


/************** Optimization Start ***************************/

function getStatusListForVII_Op()
{
	var districtId = trim(document.getElementById("districtOrSchooHiddenlId").value);
	var jobId = trim(document.getElementById("jobId").value);
	
	//alert(" jobId :: "+jobId+" districtId :: "+districtId);
	
	if(districtId!="")
	{
		ManageJobOrdersAjax.getStatusListForVVI_Op(districtId,jobId,{ 
			async: true,
			callback: function(data)
			{
					if(data!=null)
						document.getElementById("statusDiv").innerHTML=data;	
			
			},errorHandler:handleError
		});
	}
	
}


function validateJobOrder(){
	  $('#loadingDiv').show();
	 setTimeout(function() {validateAddEditJobOrder();}, 10);
	
}
 
/***************************************************************End********************************************************************/
function showHideVVILink(txt){
	
	if(txt==false){
		 document.getElementById("jobCompletedVVILink1").disabled=false;
		 document.getElementById("slctStatusID").disabled=false;
	}else if(txt==true){
		 document.getElementById("jobCompletedVVILink1").checked=false;
		 document.getElementById("slctStatusID").value="";
		 document.getElementById("slctStatusID").disabled=true;
	}
}

/*Added by Gaurav Kumar*/


function getAccountingCodeAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	
	if(txtSearch.value!='')
	{
		
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';	
		getAccountingCodeArray(txtSearch.value);			
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
	
}
function getAccountingCodeArray(){
	
	var districtId=$('#districtOrSchooHiddenlId').val();
	var accountCode = $("#accountCode").val();
	var searchArray = new Array();
	//console.log("districtId"+districtId+"==accountCode"+accountCode);

	jQuery("#divTxtAccountCode").hide();
	ManageJobOrdersAjax.getAccountingCodeMasterList(accountCode,districtId,{ 
		async: false,		
		callback: function(data){
		hiddenDataArray = new Array();
		showDataArray = new Array();
		var html ="";
		var count=0;
		for(var i=0;i<data.length;i++){
			console.log(data[i]);	
			//alert(data[i].accountingCode);
			searchArray[i] = data[i].accountingCode;
			var code = data[i].accountingCode;
			var accountCodeId= data[i].accountingCodeId;
			if(count<10){
			 html += '<div class="normal hoveraccounting" id="divResultdivTxtAccountCode'+i+'" onclick=clickAccountDiv('+accountCodeId+',"'+code+'") onmouseover=mouseOverAccountDiv('+accountCodeId+',"'+code+'")>'+code+'</div>';
			}
			count++;
		}
		jQuery("#divTxtAccountCode").html(html);
		jQuery("#divTxtAccountCode").show();
	}
	});	
}

function mouseOverAccountDiv(accountCodeId,code){
	console.log("MouseOver is invoked"+code);
	jQuery("#accountCode").val(code);
	jQuery("#accountingCodeId").val(accountCodeId);
}

function clickAccountDiv(accountCodeId,code){
	console.log("Click is invoked"+code);
	jQuery("#divTxtAccountCode").hide();
	jQuery("#accountCode").val(code);
	jQuery("#accountingCodeId").val(accountCodeId);
}

function getJobCodeAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type){
	console.log("JobCodeAutoComp is called");
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	
	 if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';	
		searchArray = getJobCodeArray(txtSearch.value); 
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
			$('#jDescription').find(".jqte_editor").html('');
			$("#jobTitle").val('');
			$("#hourPrday").val('');
			$("#salaryAdminPlan").val('');
			$("#step").val('');
			$("#paygrade").val('');
			$("#jobCategoryId").val('');
			document.getElementById("jobSubCateDiv").style.display='none';
			document.getElementById("jobCategoryId").value="0";
			document.getElementById("jobSubCategoryId").value="0";
	}
		

}
function getJobCodeArray(searchValue){
	console.log("searchValue:"+searchValue)
	var districtId=$('#districtOrSchooHiddenlId').val();
	var jobCode = $("#refno").val();
	var searchArray = new Array();
	console.log("jobCode:"+jobCode);
	jQuery("#divTxtDiscrictJobCode").hide();
	ManageJobOrdersAjax.getJobCodeMasterList(jobCode,districtId,{ 
		async: false,		
		callback: function(data){
		hiddenDataArray = new Array();
		showDataArray = new Array();
		var html ="";
		var count=0;
		for(var i=0;i<data.length;i++){
			console.log(data[i]);	
			searchArray[i] = data[i].jobCode;
			var code = data[i].jobCode;
			var jobTitle = data[i].jobTitle;
			var distjobcodeId= data[i].distjobcodeId;
			if(count<10){
			 html += '<div class="normal hoveraccounting" id="divResultdivTxtJobCode'+i+'" onclick=clickJobCodeDiv('+distjobcodeId+',"'+code+'") onmouseover=mouseOverJobCodeDiv('+distjobcodeId+',"'+code+'")>'+code+' - '+jobTitle+'</div>';
		}
			count++;
		}
		jQuery("#divTxtDiscrictJobCode").html(html);
		jQuery("#divTxtDiscrictJobCode").show();
	}
	});		
	
	
}

function mouseOverJobCodeDiv(jobCodeId,code){
	console.log("MouseOver is invoked"+code);
	jQuery("#refno").val(code);
	jQuery("#distjobcodeId").val(jobCodeId);
	if($('#districtOrSchooHiddenlId').val()==806900)
	getrefno(code);
}

function clickJobCodeDiv(jobCodeId,code){
	
	console.log("Click is invoked"+code);
	jQuery("#divTxtDiscrictJobCode").hide();
	jQuery("#refno").val(code);
	jQuery("#distjobcodeId").val(jobCodeId);
}
function getrefno(refno) {
	var districtId=$('#districtOrSchooHiddenlId').val();
	ManageJobOrdersAjax.getJobCodeDetails(districtId,refno,{ 
		async: false,		
		callback: function(data){
		  var obj = JSON.parse(data);
		  $('#jDescription').find(".jqte_editor").html(obj.JobDescription);
		  $("#jobTitle").val(obj.JobTitle);
		  $("#hourPrday").val(obj.Hours);
		  
		  $("#salaryAdminPlan").val(obj.salaryAdminPlan);
		  $("#step").val(obj.step);
		  $("#paygrade").val(obj.gradePay);

		 document.getElementById("jobCategoryId").value=obj.jobcategory;
		 getJobSubcategory();
		 if(districtId!=806900)
		 joblibrarydescription1();
		}
	});
}

$(document).ready(function(){
	var districtId=$('#districtOrSchooHiddenlId').val();
	jQuery("#refno").blur(function(){
	
		jQuery("#divTxtDiscrictJobCode").hide();
	  	
	});
	
	jQuery("#accountCode").blur(function(){
		
		jQuery("#divTxtAccountCode").hide();
	  	
	});
	
	if(districtId=='804800' && isCloneJob){
		populateJobOrderInfo(0);
	}
});

function savingDJobEequisitionNumbersTempOnlyJeffcoClone(jobId,clonejobId,reqPositionNum)
{
	ManageJobOrdersAjax.savingDJobEequisitionNumbersTempOnlyJeffcoClone(jobId,clonejobId,reqPositionNum,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
			
		}
	});
}
function showApprovalInfoDetailsForAdams(jobId){
	var jobApprovalProcessId= $('#jobApprovalProcessId').val();
	if(jobApprovalProcessId!=null && jobApprovalProcessId!="")
	{
		
		ManageJobOrdersAjax.getApprovalInfoDetailsForAdams(jobId,jobApprovalProcessId,$('#districtOrSchooHiddenlId').val(),{ 
			async: false,
			errorHandler:handleError,
			callback: function(data)
			{
				$('#loadingDiv').hide();
				$('#info322 .modal-body').html(data);
				$('#info322').modal('show');
			}
		});
	}
	else
	{
		$('#virusDivId .modal-body').html('Please select Job Approval Process.');
		$('#virusDivId').modal('show');
	}
}
function showTempCloneSchool(jobId)
{
	var reqPositionNum=$(".reqPositionNum").val()
	savingDJobEequisitionNumbersTempOnlyJeffcoClone(0,jobId,reqPositionNum);
	/*ManageJobOrdersAjax.displaySchoolTemp(document.getElementById("JobOrderType").value, { 
		async: false,
		callback: function(data)
		{
			document.getElementById("schoolRecords").innerHTML=data;
		},
	});*/
}