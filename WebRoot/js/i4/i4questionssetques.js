var page = 1;
var noOfRows = 10;
var sortOrderStr="";
var sortOrderType="";

function getPaging(pageno)
{
	if(pageno!='')
	{
		page=pageno;	
	}else
	{
		page=1;
	}
		noOfRows = document.getElementById("pageSize").value;
		displayQues();
}

function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
	if(pageno!=''){
		page=pageno;	
	}else{
		page=1;
	}
	sortOrderStr	=	sortOrder;
	sortOrderType	=	sortOrderTyp;
	if(document.getElementById("pageSize")!=null){
		noOfRows = document.getElementById("pageSize").value;
	}else{
		noOfRows=10;
	}
	displayQues();
}

function searchi4Ques()
{
	page = 1;
	displayQues();
}


function displayQues()
{
	var txtQuestion		=	document.getElementById("quesSearchText").value;
	var quesSetId		=	document.getElementById("quesSetId").value;
	var districtId		=	document.getElementById("districtId").value;
	var headQuarterId  =  "";
	
	try{
		headQuarterId = document.getElementById("headQuarterId").value;
	}catch(e){}
	
	I4QuestionsSetQuesAjax.displayQuestions(noOfRows,page,sortOrderStr,sortOrderType,txtQuestion,quesSetId,districtId,headQuarterId,{ 
		async: true,
		callback: function(data){
		document.getElementById("i4QuesGrid").innerHTML=data;
		applyScrollOnTbl();
		document.getElementById("addQuesDiv").style.display="none";
	},
	});
}

function displayQuesSetQues()
{
	var quesSetId		=	document.getElementById("quesSetId").value;
	var districtId		=	document.getElementById("districtId").value;
	var headQuarterId  = "";
	
	try{
		headQuarterId = document.getElementById("headQuarterId").value;
	}catch(e){}
	
//	alert("*************   displayQuesSetQues()   *********** hqID = "+headQuarterId);
	I4QuestionsSetQuesAjax.displayQuesSetQues(quesSetId,districtId,headQuarterId,{ 
		async: true,
		callback: function(data){
		document.getElementById("i4QuesSetQuesGrid").innerHTML=data;
		applyScrollOnTbl_1();
		document.getElementById("addQuesDiv").style.display="none";
	},
	});
}

/* Add New Question to Question Set*/
function saveQuestion()
{
	var quesSetId			=	trim(document.getElementById("quesSetId").value);
	var txtQuestion		=	trim(document.getElementById("txtQuestion").value);
	var districtId		=	trim(document.getElementById("districtId").value);
	
	if (txtQuestion=="")
	{
		document.getElementById('errQuesdiv').innerHTML		=	"&#149; "+resourceJSON.PlzEtrQuest+"";
		$('#errQuesdiv').show();
		$('#txtQuestion').css("background-color", "#F5E7E1");
		document.getElementById("txtQuestion").focus();
		return false;
	}else
	{
		if(txtQuestion.length>256)
		{
			document.getElementById('errQuesdiv').innerHTML		=	"&#149; "+resourceJSON.PlzEtrQuestLength+"";
			$('#errQuesdiv').show();
			$('#txtQuestion').css("background-color", "#F5E7E1");
			document.getElementById("txtQuestion").focus();
			return false;
		}
	}
	$("#loadingDiv").show();
	I4QuestionsSetQuesAjax.saveI4QuesInQuesSet(quesSetId,txtQuestion,districtId,{ 
		async: true,
		callback: function(data)
		{
			displayQuesSetQues();
			displayQues()
			clearQues();
			$("#loadingDiv").hide();
		},
	});
}

function clearQues()
{
	document.getElementById("txtQuestion").value="";
	document.getElementById("addQuesDiv").style.display="none";
	document.getElementById("quesId").value="";
}

/* Display Question Grid */
function addNewQues()
{
	document.getElementById("addQuesDiv").style.display="block";
	$("#errQuesdiv").empty();
	$( "span#i4QuesTxt" ).html("<textarea id='txtQuestion' ></textarea>");	
}

//save Question from Question Pool to Question Set
function addQuesFromQPtoQS(quesID)
{
	var quesSetId			=	trim(document.getElementById("quesSetId").value);
	var districtId			=	trim(document.getElementById("districtId").value);
	$("#loadingDiv").show();
	 I4QuestionsSetQuesAjax.addQuesFromQPtoQS(quesID,quesSetId,districtId,{ 
			async: false,
			callback:function(data)
			{
		 		if(data==1)
		 		{
		 			displayQuesSetQues();
					displayQues()
		 		}
		 		$("#loadingDiv").hide();
			}
		});
}

//Delete Question from Question Set And display in Question Pool
function deleteQuesFromQuesSet(quesSetQuesID)
{
	$("#loadingDiv").show();
	I4QuestionsSetQuesAjax.deleteQuesFromQuesSet(quesSetQuesID,{ 
		async: false,
		callback:function(data)
		{
			if(data==1)
	 		{
	 			displayQuesSetQues();
				displayQues()
	 		}
			$("#loadingDiv").hide();
		}
	});

}

// Move Up Question in Question set
function moveUpQues(quesSetQuesID)
{
	var districtId = $("#districtId").val();
	var headQuarterId  =  $("#headQuarterId").val();
	$("#loadingDiv").show();
	I4QuestionsSetQuesAjax.moveUpQues(quesSetQuesID,districtId,headQuarterId,{ 
		async: false,
		callback:function(data)
		{
			if(data==1)
	 		{
	 			displayQuesSetQues();
			}
			$("#loadingDiv").hide();
		}
	});
}

//Move Down Question in Question set
function moveDownQues(quesSetQuesID)
{
	var districtId = $("#districtId").val();
	var headQuarterId= $("#headQuarterId").val();		// @ Anurag
	
	$("#loadingDiv").show();
	I4QuestionsSetQuesAjax.moveDownQues(quesSetQuesID,districtId,headQuarterId,{ 
		async: false,
		callback:function(data)
		{
			if(data==1)
	 		{
	 			displayQuesSetQues();
			}
			$("#loadingDiv").hide();
		}
	});
}



function trim(s)
{
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}