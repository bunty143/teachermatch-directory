var count=0;
var index=-1;
var length=0;
var divid='';
var txtid='';
var page = 1;
var noOfRows = 50;
var sortOrderStr="";
var sortOrderType="";
var deviceTypeAndroid=$.browser.device = (/Android|webOS|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent.toLowerCase()));
var deviceType=$.browser.device = (/iPhone|iPad|iPod/i.test(navigator.userAgent.toLowerCase()));
var hiddenDataArray = new Array();
var showDataArray = new Array();
var degreeTypeArray = new Array();
var hiddenId="";
var districtNameFilter=0;
 
var downArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index<length-1){
			for(var i=0;i<10;i++){
				{
					if(document.getElementById('divResult'+txtdivid+i))
						var div_id=document.getElementById('divResult'+txtdivid+i);
					if(div_id)
					{
						if(div_id.className=='over')
							index=div_id.id.split('divResult'+txtdivid)[1];
						div_id.className='normal';
					}
				}
			}
			index++;
			if(document.getElementById('divResult'+txtdivid+index))
			{
				var div_id=document.getElementById('divResult'+txtdivid+index);
				div_id.className='over';
				document.getElementById(txtid).value = div_id.innerHTML;
				selectFirst=div_id.innerHTML;
			}
		}
	}
}
var upArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index>0){
			for(var i=0;i<length;i++){

				var div_id=document.getElementById('divResult'+txtdivid+i);
				if(div_id)
				{
					if(div_id.className=='over')
						index=div_id.id.split('divResult'+txtdivid)[1];
					div_id.className='normal';
				}
			}
			index--;
			if(document.getElementById('divResult'+txtdivid+index))
				document.getElementById('divResult'+txtdivid+index).className='over';
			if(txtid && document.getElementById('divResult'+txtdivid+index))
			{
				document.getElementById(txtid).value=
					document.getElementById('divResult'+txtdivid+index).innerHTML;
				selectFirst=document.getElementById('divResult'+txtdivid+index).innerHTML;
			}
		}
	}
}
function getDistrictMasterArray(districtName){
	var searchArray = new Array();
	ManageJobOrdersAjax.getFieldOfDistrictList(districtName,{ 
		async: false,
		callback: function(data){
		hiddenDataArray = new Array();
		showDataArray = new Array();
		for(i=0;i<data.length;i++){
			searchArray[i]=data[i].districtName;
			showDataArray[i]=data[i].districtName;
			hiddenDataArray[i]=data[i].districtId;
			
		}
	},
	errorHandler:handleError 
	});	
	return searchArray;
}
var selectFirst="";
var fatchData= function(txtSearch,searchArray,txtId,txtdivid){
	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
				if(count==10)
					break;
			}
		}
		else {
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		scrolButtom();
	}catch (err){}
}

function getJobcategoryList1(){
	var searchTextId	 =	document.getElementById("districtOrSchooHiddenlId").value;
	  f_searchTextId     =  searchTextId
	$("#jobCategoryIdd").empty();
	HireTimeAjax.getJobCategoryNewList(f_searchTextId, { 
	async: true,
	callback: function(data)
	{
	    if(data!='')
		{
			document.getElementById("jobCategoryIdd").innerHTML=data;
		}
	},
	errorHandler:handleError
});
	
}
//...............................................

var f_searchTextId
//.......................................................................

function searchRecordsByNobleCandidate()
{ 
	var searchTextId	 =	document.getElementById("districtOrSchooHiddenlId").value;
	  f_searchTextId     =  searchTextId
	var jobcategoryID=jQuery.trim($("#jobCategoryIdd").val());
	var jobID=jQuery.trim($("#jobIDD").val());
	
	
	if(jobcategoryID==null || jobcategoryID==''){
		jobcategoryID=0;
		}
	if(jobID==''){
		jobID=0;
		}
		
	$('#loadingDiv').fadeIn();
	
		HireTimeAjax.displayRecordsByEntityType(noOfRows,page,sortOrderStr,sortOrderType,f_searchTextId,jobcategoryID,jobID, {
		async: true,
		callback: function(data)
		{	
		try
		{
		document.getElementById("divMainHired").innerHTML	=data;
		$('#loadingDiv').hide();
		applyScrollOnTblVisit();
		}
		catch(e)
		{alert(e);}
		},
	errorHandler:handleError
	});
	
}


function generateJobOrderEeocEXL()
{
	$('#loadingDiv').fadeIn();
	var searchTextId	 =	document.getElementById("districtOrSchooHiddenlId").value;
	  f_searchTextId     =  searchTextId
	  var jobcategoryID=jQuery.trim($("#jobCategoryIdd").val());
		var jobID=jQuery.trim($("#jobIDD").val());
		
		
		if(jobcategoryID==null || jobcategoryID==''){
			jobcategoryID=0;
			}
		if(jobID==''){
			jobID=0;
			}
	HireTimeAjax.displayRecordsByEntityTypeEXL(noOfRows,page,sortOrderStr,sortOrderType,f_searchTextId,jobcategoryID,jobID,{
		async: true,
		callback: function(data)
		{
		    $('#loadingDiv').hide();
			if(deviceType)
			{					
					$("#exelfileNotOpen").css({"z-index":"3000"});
			    	$('#exelfileNotOpen').show();
			}
			else
			{
				try
				{
					document.getElementById('ifrmTrans').src = "hireTimeEXL/"+data+"";
				}
				catch(e)
				{alert(e);}
			}
		},
	errorHandler:handleError
	});
}

function generateJobOrderEeocPDF()
{
	var searchTextId	 =	document.getElementById("districtOrSchooHiddenlId").value;
	  f_searchTextId     =  searchTextId
	  var jobcategoryID=jQuery.trim($("#jobCategoryIdd").val());
		var jobID=jQuery.trim($("#jobIDD").val());
		
		
		if(jobcategoryID==null || jobcategoryID==''){
			jobcategoryID=0;
			}
		if(jobID==''){
			jobID=0;
			}
	
	$('#loadingDiv').fadeIn();
	
	HireTimeAjax.displayRecordsByEntityTypePDF(noOfRows,page,sortOrderStr,sortOrderType,f_searchTextId,jobcategoryID,jobID,{
		async: true,
		callback: function(data)
		{
		//alert(data);
		$('#loadingDiv').hide();	
		if(deviceType || deviceTypeAndroid)
		{
			window.open(data,'left=200,top=50,width=700,height=600,toolbar=1,resizable=0');			
		}		
		else
		{
			$('#modalDownloadHiredCandidates').modal('hide');
			document.getElementById('ifrmCJS').src = ""+data+"";
			try{
				$('#modalDownloadHiredCandidates').modal('show');
			}catch(err)
			{}		
	     }		
	     return false;
}});
		
	

}
 
function generateJobOrderEeocPrintPre()
{
	var searchTextId	 =	document.getElementById("districtOrSchooHiddenlId").value;
	  f_searchTextId     =  searchTextId
	  var jobcategoryID=jQuery.trim($("#jobCategoryIdd").val());
		var jobID=jQuery.trim($("#jobIDD").val());
		
		
		if(jobcategoryID==null || jobcategoryID==''){
			jobcategoryID=0;
			}
		if(jobID==''){
			jobID=0;
			}
	
	$('#loadingDiv').fadeIn();
    HireTimeAjax.displayRecordsByEntityTypePrintPreview(noOfRows,page,sortOrderStr,sortOrderType,f_searchTextId,jobcategoryID,jobID,{
		async: true,
		callback: function(data)
		{	
			$('#loadingDiv').hide();
			$('#printhiredApplicantsDataTableDiv').html(data);
			applyScrollOnPrintTable();
			$("#printHiredApplicantsDiv").modal('show');
		},
	errorHandler:handleError
	});
} 

function printNobleJobsDATA()
{
	var searchTextId	 =	document.getElementById("districtOrSchooHiddenlId").value;
	  f_searchTextId     =  searchTextId
	var jobcategoryID=$("#jobCategoryIdd").val();
	var jobID=$("#jobIDD").val();
	
	if(jobcategoryID==null){
		jobcategoryID=0;
		}
	if(jobID==''){
		jobID=0;
		}
	
	$('#loadingDiv').fadeIn();
    HireTimeAjax.displayRecordsByEntityTypePrintPreview(noOfRows,page,sortOrderStr,sortOrderType,f_searchTextId,jobcategoryID,jobID,{
		async: true,
		callback: function(data)
		{	$('#loadingDiv').hide();
		try{ 
			if (isSafari && !deviceType)
			    {
			    	window.document.write(data);
					window.print();
			    }else
			    {
			    	var newWindow = window.open();
			    	newWindow.document.write(data);	
			    	newWindow.print();
				 } 
			}catch (e) 
			{
				$('#printmessage1').modal('show');							 
			}
		},
	errorHandler:handleError
	});
	
}

function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert(""+resourceJSON.msgServerErr+": "+exception.javaClassName);}
}

//for paging and sorting
function getPaging(pageno)
{
        //alert("Paging"+pageno);
		if(pageno!='')
		{
			page=pageno;	
		}
		else
		{
			page=1;
		}
		noOfRows = document.getElementById("pageSize").value;
		
		searchRecordsByNobleCandidate();
	
}


function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{	
	  if(pageno!=''){
			page=pageno;
			//alert("page :: "+page);
		}else{
			page=1;
			//alert("default");
		}
		sortOrderStr=sortOrder;
		//alert("sortOrderStr :: "+sortOrderStr);
		sortOrderType=sortOrderTyp;
		if(document.getElementById("pageSize")!=null){
			//alert("AA")
			noOfRows = document.getElementById("pageSize").value;
		}else{
		//	alert("BB");
			noOfRows=50;
		}
		searchRecordsByNobleCandidate();
		
	
}


/////////////////////////////////////////////////    for the district and category //////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function hideDiv()
{
	$('#docfileNotOpen').hide();
	$('#exelfileNotOpen').hide();
}
function getDistrictORSchoolAuto(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("districtORSchoolName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getDistrictORSchoolArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getDistrictORSchoolArray(districtOrSchoolName){
	var searchArray = new Array();
	ManageJobOrdersAjax.getFieldOfDistrictList(districtOrSchoolName,{ 
		async: false,
		callback: function(data){
		hiddenDataArray = new Array();
		showDataArray = new Array();
		for(i=0;i<data.length;i++){
			searchArray[i]=data[i].districtName;
			showDataArray[i]=data[i].districtName;
			hiddenDataArray[i]=data[i].districtId;
		}
	},
	errorHandler:handleError 
	});	

	return searchArray;
}


var selectFirst="";

var fatchData= function(txtSearch,searchArray,txtId,txtdivid){
	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
				
			}
			
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}

function hideDistrictMasterDiv(dis,hiddenId,divId)
{
	document.getElementById("districtOrSchooHiddenlId").value="";
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		if(dis.value==""){
			//alert(" Hi ");
			//$('#schoolName').attr('readonly', true);
			//document.getElementById('schoolName').value="";
			//document.getElementById('schoolId').value="0";
			document.getElementById(hiddenId).value="";
			
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			document.getElementById("districtOrSchooHiddenlId").value=hiddenDataArray[index];
			//document.getElementById('schoolName').readOnly=false;
			//document.getElementById('schoolName').value="";
			//document.getElementById('schoolId').value="0";
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}







function mouseOverChk(txtdivid,txtboxId)
{	
	for(var i=0;i<length;i++)
	{
		$('#divResult'+txtdivid+i).mouseover({param1: i,param2:txtdivid, param3:txtboxId}, fireMouseOverEvent);
	}
}


function fireMouseOverEvent(event)
{
	for(var i=0;i<length;i++)
	{	
		document.getElementById('divResult'+event.data.param2+i).className='normal';
	}
    document.getElementById('divResult'+event.data.param2+event.data.param1).className='over';	       
   	document.getElementById(event.data.param3).value= $('#divResult'+event.data.param2+event.data.param1).text();
    index=event.data.param1;
}

function checkForCGInt(evt) {

	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==8 || charCode==46)
		return true;

	return( (charCode >= 48 && charCode <= 57)||(charCode==13) );
}


function activecityType(){
	document.getElementById("certType").value='';
}


function tpJbIEnable()
{
	var noOrRow = document.getElementById("tblGridEEC").rows.length;
	for(var j=1;j<=noOrRow;j++)
	{	
		$('#cg'+j).tooltip();
		$('#cgn'+j).tooltip();
	}
}



 
 function canelPrint()
 {
	 $('#errDateCheckDiv').hide();
	 $('#printOfferReadyDiv').hide();
	 $('#loadingDiv').hide();
		
 }

function chkschoolBydistrict()
{
	var districtid = trim(document.getElementById('districtORSchoolName').value);
	if(trim(document.getElementById('entityType').value)==1)
	if(districtid=="")
	{
		//document.getElementById('schoolName').disabled=true;
	}
	else
	{
		//document.getElementById('schoolName').disabled=false;
	}
}

 //*************:::::::::::::::::::::::::::::::::::::::::::      ::::::::::::::::::::::::****************************************
 function hideDistrictMasterDiv(dis,hiddenId,divId)
 {
 	document.getElementById("districtOrSchooHiddenlId").value="";
 	if(parseInt(length)>0){
 		if(index==-1){
 			index=0;
 		}
 		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
 			document.getElementById(hiddenId).value=hiddenDataArray[index];
 		}
 		if(dis.value==""){
 			
 		} else if(showDataArray && showDataArray[index]) {
 			dis.value=showDataArray[index];
 			document.getElementById("districtOrSchooHiddenlId").value=hiddenDataArray[index];
 			
 		}
 		
 	} else {
 		if(document.getElementById(hiddenId))
 			document.getElementById(hiddenId).value="";
 	}

 	if(document.getElementById(divId)) {
 		document.getElementById(divId).style.display="none";
 	}
 	index = -1;
 	length = 0;
 }


