function addJobCategoryApprovalHideGroup()
{
	$("#groupApprovalDiv").hide();
	$("#groupApprovalDiv1").hide();
	$("#ApprovalGroupFromEditFlag").val("false");
}

function addJobCategoryApproval()
{
	var districtId = $("#districtId").val();
	var jobcategoryId = $("#ApprovalJobCategoryId").val();
	var noOfApprovalNeeded = $("#ApprovalNoOfApprovalNeeded").val();
	var buildApprovalGroup = $("#ApprovalBuildApprovalGroup").val();
	var approvalBeforeGoLive = $("#ApprovalApprovalBeforeGoLive").val();
	var flag = $("#ApprovalGroupFromEditFlag").val();
	
	if(districtId!=null && districtId!="" && flag=="false")
	{
		$("#groupApprovalDiv").show();
		$("#groupApprovalDiv1").show();
		$("#ApprovalJobCategoryId").val("-1");
		jobcategoryId = $("#ApprovalJobCategoryId").val();		
		$("#ApprovalDistrictId").val(districtId);
		$("#ApprovalJobCategoryId").val(jobcategoryId);
		
		getApprovalGroupsInfo(districtId,jobcategoryId);
	}
}
function enableDisableApprovalsFirst()
{
	if($("#buildApprovalGroups").is(":checked")){
		$(".buildAppGroup").show();
	}else{
		$(".buildAppGroup").hide();
		hideAddApprovalGroupAndMemberDivMain();
	}
}
function enableDisableApprovals()
{
	$('#errordiv').empty();
	var enableFlag=0;
	var districtId= $("#districtId").val();
	var previousDistrictId=$("#previousDistrictId").val();
	if(districtId!="")
	{
	var jobcategoryId= $("#ApprovalJobCategoryId").val();
	var isChecked = $("#approvalBeforeGoLive1").is(":checked");
	resetStackAndDiv();
	if(isChecked)
	{
		$("#groupApprovalDiv").show();
		$("#groupApprovalDiv1").show();
		$(".hideGroup").fadeIn(500);
		if(districtId==804800){
			$('#approvalBPGroupsDiv').show();
		}
		else{
			$('#approvalBPGroupsDiv').hide();
		}
	    if(previousDistrictId!=districtId)
	    {
	    	getApprovalGroupsInfo(districtId,jobcategoryId);
	    	$("#previousDistrictId").val(districtId);
	    	enableFlag=1;
	    }
	    else
	    {
	    	if(enableFlag==0)
	    	{
	    		getApprovalGroupsInfo(districtId,jobcategoryId);
		    	$("#previousDistrictId").val(districtId);
		    	enableFlag=1;	
	    	}
	    }
	}
	else
	{
		$(".hideGroup").fadeOut(500);
		$("#groupApprovalDiv").hide();
		$("#groupApprovalDiv1").hide();
		$("#approvalByPredefinedGroups").prop("checked",false);
		$('#approvalBPGroupsDiv').hide();
	}
	}else{
		$('#errordiv').show();
		$('#districtName').focus();
		$('#errordiv').append(resourceJSON.msgDistrictNamejobcategory);
		document.getElementById("approvalBeforeGoLive1").checked=false;
	 }
	$(".buildAppGroup").hide();
	enableDisableApprovalsFirst();
}

function getApprovalGroupsInfo(districtId,jobcategoryId)
{
	resetStackAndDiv();
	DistrictAjax.getApprovalGroupsInfo(districtId,jobcategoryId,
	{
		async: false,
		errorHandler:handleError,
		callback: function(data)
		{
			$("#groupName").empty();
			$(".mutliSelectAss").empty();
			$("#groupMembersList").empty();
			$("#groupMembers").empty();
			$("#groupName").append("<option value='-1'> "+resourceJSON.msgSelectGroup+" </option>");
			$.each(data, function(key, value)
			{
				if(key=="groupAndMemberList")
				{
					var groupAndMemberList="";
					$.each(value, function(groupKey, groupvalue)
					{
						var groupInfo = groupKey;
						var groupMemberInfo = groupvalue.split(",");
						
						var groupId = groupInfo.split("_")[0];
						var groupName = groupInfo.split("_")[1];
												
						appendGroup(groupId,groupName);	             //Append the group into Div
						addGroupAndMemberIntoStack(groupId, "");	 //Append the group with no member
						
						for(i=0;i<groupMemberInfo.length;i++)
						{
							if(groupMemberInfo!=null && groupMemberInfo!="" && groupMemberInfo[i]!=null && groupMemberInfo[i]!="")
							{
								var memberId=groupMemberInfo[i].split("_")[0];
								var memberName=groupMemberInfo[i].split("_")[1];
															
								appendMemberIntoGroup(groupId,memberId,memberName);	//Append the member into group
								addGroupAndMemberIntoStack(groupId, memberId);
								
							}
						}
					});
				}
				else if(key=="memberList")
				{
					var myarr = (""+value).split(",");
					//alert("Member lists ::::::: "+myarr.length);
					/*var arr=['Deepak','Anurag','Sanaver','Ankit','Asish','Mukesh'];
					for(i=0;i<6;i++)
					{
						$(".mutliSelectAss").append("<li><label class='checkbox mt0 mb0'><input type='checkbox' name='assData' onClick='selectAssList();' name='memberList'  value='"+i+"' title='"+arr[i]+"' />"+arr[i]+"</label></li>");
						
					}*/
					for(i=0;i<myarr.length;i+=1)
					{
						if(myarr!=null && myarr!="" && myarr[i]!=null && myarr[i]!="")
						{
							var memberInfo=myarr[i].split("_");
							var memberId=memberInfo[0];
							var memberName=memberInfo[1];
							$(".mutliSelectAss").append("<li><label class='checkbox mt0 mb0'><input type='checkbox' name='assData' onClick='selectAssList();' name='memberList'  value='"+memberId+"' title='"+memberName+"' />"+memberName+"</label></li>");
						}
					}
				}
				else
				if(key=="noOfApprovalNeeded")
				{
					$("#noOfApprovalNeeded option[value="+value+"]").attr("selected","selected");
					$("#ApprovalNoOfApprovalNeeded").val(value);
				}
				else
				if(key=="buildApprovalGroup")
				{
					if(value!=null && value=="true")
					{
						$("#buildApprovalGroups").attr("checked","checked");
					}
					else
					{
						$("#buildApprovalGroups").removeAttr("checked");
					}
					$("#ApprovalBuildApprovalGroup").val(value);
				}
				else
				if(key=="approvalBeforeGoLive")
				{
					if(value!=null && value=="true")
						$("#approvalBeforeGoLive1").attr("checked","checked");
				}
			});
			
			var isChecked = $("#approvalBeforeGoLive1").is(":checked");
			if(isChecked==false)
				$("#noOfApprovalNeeded").attr("disabled","disabled");
			else
				$("#noOfApprovalNeeded").removeAttr("disabled");
			
			$("#buildApprovalGroupsDivId").show();
			$("#approvalBeforeGoLive1").show();
			$("#groupMembersList").show();
			$("#addApprovalGroupAndMemberDiv").hide();
				
		}
	});
}

function removeMember()
{
	var groupId = $("#removeMemberConfirmation").attr("removeMemberGroupId");
	var memberId = $("#removeMemberConfirmation").attr("removeMemberId");
	var memberName = $("#"+groupId+"_"+memberId).attr("name");
	
	$("#removeMemberConfirmation").modal("hide");
	
	var d = document.getElementById(groupId+"_"+memberId);
	var olddiv = document.getElementById(groupId+"_"+memberId);
	d.remove(olddiv);
	
	removeMemberFromStack(groupId, memberId);
}

function removeMemberConfirmation(groupId, memberId)
{
	$("#removeMemberConfirmation").modal("show");
	$("#removeMemberConfirmation").attr("removeMemberGroupId",groupId);
	$("#removeMemberConfirmation").attr("removeMemberId",memberId);
}

function removeGroup()
{
	var groupId = $("#removeGroupConfirmation").attr("removeGroupId");
	$("#removeGroupConfirmation").modal("hide");
	
	$("#groupName option[value='"+groupId+"']").remove();	//remove group from select
	var memberIds = getMembersByGroupFromStack(groupId);	//append the group member into multiple select textbox

	var d = document.getElementById(groupId);		//remove group and its all member
	var olddiv = document.getElementById(groupId);
	d.remove(olddiv);
	
	removeGroupFromStack(groupId);
}

function removeGroupConfirmation(groupId)
{
	$("#removeGroupConfirmation").modal("show");
	$("#removeGroupConfirmation").attr("removeGroupId",groupId);
}

function addGroupORMember()
{
	var cnt = validateApprovalGroup();
	if(cnt==0)
	{
		var groupId;
		var memberIds=$("#selectAssId").val();
		
		var checked = $('input[name=groupRadioButtonId]:checked').val();
		
		if(checked=="radioButton1")	//add new group
		{
			var groupName = $("#addGroupName").val();
			groupId = "";//"newGroup"+(new Date()).getMilliseconds();
			var groupNameArray = groupName.split(" ");
			for(var i=0;i<groupNameArray.length; i++ )
				if(i==0)
					groupId = groupId+groupNameArray[i].trim();
				else
					groupId = groupId+"-"+groupNameArray[i].trim();
			
			appendGroup(groupId,groupName);				//Append the group
			addGroupAndMemberIntoStack(groupId, "");	//Append the new group into stack;
						
			var selectAssId=$("#selectAssId").val();
			var memberNa=$(".hidaAss").html();
			
			var groupMemberId="#"+selectAssId+"#";
			
			
			var selectAssIdArr=selectAssId.split("#");
			var memberNameArr=memberNa.split(",");
			
			//alert("Array Length ::::"+selectAssIdArr.length+" Member Name ::::"+memberNameArr+" Name Length:::"+memberNameArr.length);
			for(var i=0;i<selectAssIdArr.length;i+=1)
			{
				if(!isMemberExistInGroup(selectAssIdArr[i],groupId))
				{
					appendMemberIntoGroup(groupId,selectAssIdArr[i],memberNameArr[i]);	//Append group in div
					addGroupAndMemberIntoStack(groupId, selectAssIdArr[i]);		//Append the member to group

				}
			}
			
		}
		else
		if(checked=="radioButton2")	//add member to group
		{
			groupId = $("#groupName option:selected").val();
		
			var selectAssId=$("#selectAssId").val();
			var memberNa=$(".hidaAss").html();
			
			var selectAssIdArr=selectAssId.split("#");
			var memberNameArr=memberNa.split(",");
			for(var i=0;i<selectAssIdArr.length;i+=1)
			{
				if(!isMemberExistInGroup(selectAssIdArr[i],groupId))
				{
					appendMemberIntoGroup(groupId,selectAssIdArr[i],memberNameArr[i]);	//Append group in div
					addGroupAndMemberIntoStack(groupId, selectAssIdArr[i]);		//Append the member to group
				}
			}
		}
		//$("#groupMembers :selected").remove();	//remove group from select textbox
		$("#addGroupName").val();
		//$("#addApprovalGroupAndMemberDiv").fadeOut(500);
		hideAddApprovalGroupAndMemberDivMain();
		resetSelectAssList();
	}
}

function validateApprovalGroup()
{
	var count=0;
	
	var errorMessage="";
	$("#privilegeForDistrictErrorDiv").empty();
	$('#addGroupName').css("background-color", "#FFF");
	$('#groupName').css("background-color", "#FFF");
	$('#groupMembers').css("background-color", "#FFF");
	$('.hidaAss').css("background-color", "#FFF");
	//groupRadioButton();
	
	var checked = $('input[name=groupRadioButtonId]:checked').val();
	if(checked=="radioButton1")	//add new group
	{
		var groupName = $("#addGroupName").val();
		if(groupName=="")
		{
			count++;
			errorMessage = errorMessage+"<label class=required>* "+resourceJSON.msgGroupName+" </label><BR>";
			$('#addGroupName').css("background-color", "#F5E7E1");
			$('#addGroupName').focus();
		}
	}
	else if(checked=="radioButton2")	//add member to group
	{
		var groupId = $("#groupName option:selected").val();
		var memberIds=$("#selectAssId").val();
		if(groupId=="-1")
		{
			count++;
			errorMessage = errorMessage+"<label class=required>* "+resourceJSON.msgGroupName+" </label><BR>";
			$('#groupName').css("background-color", "#F5E7E1");
		}

		if(memberIds.length<=1)
		{
			count++;
			errorMessage = errorMessage+"<label class=required>* "+resourceJSON.PlzSelectMember+" </label><BR>";
			$('.hidaAss').css("background-color", "#F5E7E1");
		}
		
		if(groupId=="-1")
			$('#groupName').focus();
		else
			$('#groupMembers').focus();
	}
	
	if(errorMessage!="")
	{
		$("#privilegeForDistrictErrorDiv").html(errorMessage);
		$("#privilegeForDistrictErrorDiv").fadeIn(500);
	}
	
	return count;
}

function groupRadioButton()
{
	var checked = $('input[name=groupRadioButtonId]:checked').val();
	if(checked=="radioButton1")
	{
		$("groupName").val("");
		$("#groupName").attr("disabled", "disabled");	//disable groupNameList
		$('#groupName').css("background-color", "#EEEEEE");
		$("#addGroupName").removeAttr("disabled");		//enable addGroup
		$('#addGroupName').css("background-color", "#FFF");
	}
	else
	if(checked=="radioButton2")
	{
		$("#addGroupName").attr("disabled", "disabled");	//disable groupNameList
		$('#addGroupName').css("background-color", "#EEEEEE");
		$("#groupName").removeAttr("disabled");				//enable addGroup
		$('#groupName').css("background-color", "#FFF");
	}
}

function showAddGroup()	
{
	$("#addGroupName").val("");
     $("#addApprovalGroupAndMemberDiv").show();
     $("#createGroupName").prop('checked', true);
     groupRadioButtonMain();
    // groupRadioButton();
}

function hideAddApprovalGroupAndMemberDiv()
{
	$("#addApprovalGroupAndMemberDiv").fadeOut(500);
	$("#privilegeForDistrictErrorDiv").empty();
	$("#privilegeForDistrictErrorDiv").fadeOut(500);
}

function addOrUpdateDistrictApprovalGroupJobCategorywise(jobcategoryId)
{
	var districtId = $("#districtId").val();
	var noOfApprovals = $("#noOfApproval").val();
	var groupInfo = getStack();
	$('#loadingDiv').show();
	DistrictAjax.addOrUpdateDistrictApprovalGroupJobCategorywise(districtId,jobcategoryId,noOfApprovals,groupInfo,{
		async: false,
		callback: function(data){
    		$('#loadingDiv').hide();
		},
	});
}

/**************************************************Start HTML Methods**************************************************/
function appendGroup(groupId,groupName)
{
	var htmlText="";
	htmlText = htmlText+("<div class='row mt5' id='"+groupId+"' name='"+groupName+"'>");
	htmlText = htmlText+("<label> "+groupName+"&nbsp<a href='javascript:void(0);' data-toggle='tooltip' title='Remove Group' onclick='removeGroupConfirmation(\""+groupId+"\")'><img width='15' height='15' class='can' src='images/can-icon.png'></a> </label>");
	htmlText = htmlText+"</div>";
	$("#groupMembersList").append(htmlText);
	$("#groupName").append("<option value='"+groupId+"'> "+groupName+" </option>");
	//return htmlText;
}

function appendMemberIntoGroup(groupId,memberId,memberName)
{
	var htmlText="";
	htmlText=htmlText+("<div style='margin-top: -6px;'>");
	htmlText = htmlText+("<div class='col-sm-3 col-md-3' id='"+groupId+"_"+memberId+"' name='"+memberName+"'>"+memberName+"&nbsp<a href='javascript:void(0);' data-toggle='tooltip' title='Remove Member' onclick='removeMemberConfirmation(\""+groupId+"\","+memberId+")' ><img width='15' height='15' class='can' src='images/can-icon.png'></a></div>");
	htmlText = htmlText+("</div>");
	$("#"+groupId).append(htmlText);
	
	//return htmlText;
}

/***************************************************End HTML Methods**************************************************/

/**************************************************Start Stack Methods*************************************************/
var groupStack="#";
function addGroupAndMemberIntoStack(groupId, memberId)
{
	var activeGroups = groupStack.split("#");
	var groupIdWithMemberId="";
	for(var i=0;i<activeGroups.length;i++)	//getting the group with member
	{
		var pos = activeGroups[i].indexOf(groupId+"_");
		if(pos==0)
		{
			groupIdWithMemberId = activeGroups[i];
			break;
		}
	}
	
	var group="";
	if(groupIdWithMemberId=="")											
		group = groupId+"_";								//add new group
	else
		group = groupIdWithMemberId;	//group exist so add the member into group
	
	if(memberId!="")												//add new member
		group = group+memberId+"_";		
	
	if(groupIdWithMemberId!="")						//This is a new Group
		groupStack = groupStack.replace("#"+groupIdWithMemberId+"#",("#"+group+"#") );		//remove the previous group
	else
	if(groupStack=="")												//add new upodated group and member into stack
		groupStack = "#"+groupStack+group+"#";
	else
		groupStack = groupStack+group+"#";
	
	groupStack = groupStack.replace("##","#");
	//alert("addGroupAndMemberIntoStack groupStack:- "+groupStack);
	return groupStack;
}

function removeGroupFromStack(groupId)
{
	var activeGroups = groupStack.split("#");
	for(var i=0;i<activeGroups.length;i++)	//getting the group with member
	{
		var pos = activeGroups[i].indexOf(groupId+"_");
		if(pos==0)
			groupStack = groupStack.replace("#"+activeGroups[i]+"#","#");		//remove the group
	}	
	
	groupStack = groupStack.replace("##","#");
	//alert("removeGroupFromStack groupStack:- "+groupStack);
	return groupStack;
}

function removeMemberFromStack(groupId, memberId)
{
	var activeGroups = groupStack.split("#");
	var groupIdWithMemberId="";
	for(var i=0;i<activeGroups.length;i++)	//getting the group with member
	{
		var pos = activeGroups[i].indexOf(groupId+"_");
		if(pos==0)
		{
			groupIdWithMemberId = activeGroups[i];
			break;
		}
	}
	
	var previousGroup = groupIdWithMemberId;
	if(groupIdWithMemberId!="")
	{
		groupIdWithMemberId = groupIdWithMemberId.replace("_"+memberId+"_","_");
		groupIdWithMemberId = groupIdWithMemberId.replace("__","_");
		
		groupStack = groupStack.replace("#"+previousGroup+"#",("#"+groupIdWithMemberId+"#") );	//remove the previous group
		//groupStack = groupStack+groupIdWithMemberId+"#";
	}
	
	groupStack = groupStack.replace("##","#");
	//alert("removeMemberFromStack groupStack:- "+groupStack);
	return groupStack;
}

function getMembersByGroupFromStack(groupId)
{
	var activeGroups = groupStack.split("#");
	var groupIdWithMemberId="";
	for(var i=0;i<activeGroups.length;i++)	//getting the group with member
	{
		var pos = activeGroups[i].indexOf(groupId+"_");
		if(pos==0)
		{
			groupIdWithMemberId = activeGroups[i];
			break;
		}
	}
	
	groupIdWithMemberId = groupIdWithMemberId.substring(groupIdWithMemberId.indexOf("_"), groupIdWithMemberId.length);
	
	//alert("getMembersByGroupFromStack Members:- "+groupIdWithMemberId.split("_"));
	return groupIdWithMemberId.split("_");
}

function isMemberExistInGroup(memberId, groupId)
{
	var groupMember = getMembersByGroupFromStack(groupId);
	var flag=false;
	for(var i=0;i<groupMember.length;i++)
	{
		if(memberId==groupMember[i])
		{
			flag=true;
			break;
		}
	}
	
	//alert("isMemberExistInGroup:- "+flag);
	return flag;
}

function resetStackAndDiv()
{
	groupStack="#";
	$("#groupMembersList").empty();
}

function getStack()
{
	return groupStack;
}
/**************************************************End Stack Methods*************************************************/


/*********************************************************        Adding by deepak       *****************/
function groupRadioButtonMain()
{
	var checked = $('input[name=groupRadioButtonId]:checked').val();
	if(checked=="radioButton1")
	{
		$('#createGroup').show();
		$('#selectGroup').hide();
	}
	else
	if(checked=="radioButton2")
	{
		$('#createGroup').hide();
		$('#selectGroup').show();
	}
	$('#saveGroup').show();
}

function hideAddApprovalGroupAndMemberDivMain()
{
	$("#addApprovalGroupAndMemberDiv").fadeOut(500);
	$("#privilegeForDistrictErrorDiv").empty();
	$("#privilegeForDistrictErrorDiv").fadeOut(500);
	$('#createGroupName').prop("checked",false);
	$('#selectGroupName').prop("checked",false);
	$('#createGroup').hide();
	$('#selectGroup').hide();
	$('#saveGroup').hide();
	resetSelectAssList();
}