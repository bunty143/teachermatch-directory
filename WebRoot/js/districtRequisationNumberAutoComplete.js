/* Gagan : Auto Complete for District Requisition Number 
 */

var requisationDivCount=0;
var requisationDivCountCheck=0;
var requisationDivVal="";
var requisationDivValSchool="";
function validateAddRequisationNumber()
{
	$('#requisitionNumber').css("background-color","");
	var requisitionNumber=trim(document.getElementById("requisitionNumber").value);
	var noOfExpHires=trim(document.getElementById("noOfExpHires").value);
	var districtRequisitionId=document.getElementById("districtRequisitionId").value;
	var allSchoolGradeDistrictVal	=	trim(document.getElementById("allSchoolGradeDistrictVal").value);
	var districtRequisitionIdCheck=false;
	/*======= Gagan : Duplicate will check only in case of Add Job order mode and In edit mode duplicate will check on Database level ============= */
	if(districtRequisitionId!="" && jobId!='')
	{
		districtRequisitionIdCheck=getRequisationIdCheck(districtRequisitionId);
	}
	$('#errordiv').empty();
	if(requisitionNumber==''){
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.msgRequisitionNumber+"<br>");
		$('#requisitionNumber').focus();
	}else if(districtRequisitionId==''){
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.msgvalidRequisitionNumber+"<br>");
		$('#requisitionNumber').focus();
	}else if(districtRequisitionIdCheck){
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.msgAnotherRequisitionNumber+"<br>");
		$('#requisitionNumber').focus();
		$('#districtRequisitionId').val("");
		$('#requisitionNumber').val("");
		$('#requisitionNumber').css("background-color","#F5E7E1");
	}else{
		var jobId=document.getElementById("jobId").value;
		//alert(" requisitionNumber : "+requisitionNumber+" jobId : "+jobId);
		if(requisitionNumber!="" && jobId==''){
			requisationDivCount++;
			requisationDivCountCheck++;
			//requisationDivVal+="<div class='span3' id='reqChild_"+districtRequisitionId+"'>"+requisitionNumber+" <a href='javascript:void(0);' onclick=\"return deleteReqNum("+districtRequisitionId+");\"><img width='15' height='15' class='can' src='images/can-icon.png' alt=''></a></div>";
			requisationDivVal="<div class='span3' id='reqChild_"+districtRequisitionId+"'>"+requisitionNumber+" <a href='javascript:void(0);' onclick=\"return deleteReqNum("+districtRequisitionId+");\"><img width='15' height='15' class='can' src='images/can-icon.png' alt=''></a></div>";
			//requisationDivVal+="<div class='span3' id='reqChild_"+districtRequisitionId+"'>"+requisitionNumber+"</div>";
			//document.getElementById("districtReqNumberDiv").innerHTML	+=requisationDivVal+""	;
			//alert(" requisationDivVal "+requisationDivVal);
			//$("#districtReqNumberDiv").html("");
			$("#districtReqNumberDiv").append(requisationDivVal);
			
		}else if(jobId!=''){
			//alert(" Else Block ");	
			$("#arrReqNum").val("");
			
			/* ===== Gagan : It is for giving alert that Requisition Number attached to other Radio will be deleted ======*/
			/*var divMsg="<div>It will delete all Requisition Number added before.</div>";
			document.getElementById("Msg").innerHTML=divMsg;
			$('#myModalMsg').modal('show');*/

			
			var schoolId ="";
			ManageJobOrdersAjax.addRequisationNumber(jobId,districtRequisitionId,schoolId, { 
				async: true,
				callback: function(data)
				{
					//alert(" data : "+data);
					if(data==3){
						$('#errordiv').show();
						$('#errordiv').append("&#149; "+resourceJSON.msgAnotherRequisitionNumber+"<br>");
						$('#requisitionNumber').focus();
					}else{
						if(data==1)
						displayRequisationNumber();
					}
				},
				errorHandler:handleError 
			});
		}
	$("#addRequisationDiv").hide();	
	document.getElementById("requisitionNumber").value="";
	document.getElementById("noOfSchoolExpHires").value="";
	document.getElementById("districtRequisitionId").value="";
	}
}




/*==================  Gagan : This Js method is used for adding unic Requsiation Id and also check duplicate districtRequisationId ========================*/
function getRequisationIdCheck(districtRequisitionId){
	
	//alert(" getRequisationIdCheck ");
	var isIdExist =false;
	try{
		var isCommaExist =0;
		var arrReqNum =$("#arrReqNum").val();
		//alert(" arrReqNum : "+arrReqNum);
		//isCommaExist = arrReqNum.search(",");
		if(arrReqNum!="")
		{
			isCommaExist = arrReqNum.search(",");
			//alert(" isCommaExist "+isCommaExist+" arrReqNum :  "+arrReqNum);
			if(isCommaExist>0)
			{
				var arrReqNumVal=arrReqNum.split(",");
				for(var i=0;i<arrReqNumVal.length;i++){
						if(arrReqNumVal[i]==districtRequisitionId){
							isIdExist=true;
						}
					}
			}	
			else
			{
				if(arrReqNum==districtRequisitionId){
					isIdExist=true;
				}
			}
		}
		else
		{
			/*====== First time adding value to Field ======*/
			//districtRequisitionId=","+districtRequisitionId+",";
			//alert(" First time  : districtRequisitionId :"+districtRequisitionId);
			$("#arrReqNum").val(districtRequisitionId+",");
			return false;
		}
		
		if(!isIdExist)
		{
			/*========== Gagan :  It will call =========*/
			//alert(" isIdExist : "+isIdExist);
			$("#arrReqNum").val($("#arrReqNum").val()+""+districtRequisitionId+",");
		}
		
		
	
	}catch(err){}
	return isIdExist;
}


function checkArray()
{
	//alert("arr "+arr);
	var currentdate = new Date();
	var datetime =  currentdate.getDay()+""+currentdate.getMonth()+""+currentdate.getFullYear()+""+currentdate.getHours()+""+currentdate.getMinutes()+""+currentdate.getSeconds();
	
	var arrReqNum = "100422-13,100414-14,100414-15,100414-16,";
	var arrReqNumVal=arrReqNum.split(",");
	//alert(" arrReqNumVal.length "+arrReqNumVal.length);
	for(var i=0;i<arrReqNumVal.length-1;i++){
		
		alert(i+" "+arrReqNumVal[i]);
		var subArrReqNumVal = arrReqNumVal[i].split("-");
		alert(i+" SchoolId  "+subArrReqNumVal[0]+" Req Id : "+subArrReqNumVal[1]);
	}
}

function deleteReqNum(delReqId)
{
	//alert(" delReqId : "+delReqId);
	$('#delReqId').val(delReqId);
	//$("#reqChild_"+delReqId).remove();
	
	//var arrReqNum =$("#arrReqNum").val();
	//var res = str.replace("Microsoft","W3Schools");
	
	var divMsg="<div>"+resourceJSON.msgRemoveReqNo+"</div>";
	document.getElementById("Msg").innerHTML=divMsg;
	$('#delBtn').show();
	$('#closeBtn').show();
	$('#msjBtn').hide();
	$('#myModalMsg').modal('show');
	
}

function deleteRequisationNumber()
{
	var delReqId = $('#delReqId').val();
	var jobAuthKey = 	$('#jobAuthKey').val();
	var jobId	=	document.getElementById("jobId").value;
	var allSchoolGradeDistrictVal	=	trim(document.getElementById("allSchoolGradeDistrictVal").value);

	
	//alert(" jobId "+jobId+" delReqId : "+delReqId);
	if(jobId!="" && jobId!=0)
	{
		var jobRequisitionId = delReqId;
		ManageJobOrdersAjax.deleteRequisationNumber(jobRequisitionId,jobId,allSchoolGradeDistrictVal,{ 
			async: true,
			callback: function(data)
				{
					//alert(" data "+data);
					if(data){
						$("#reqChild_"+delReqId).remove();
						 	displayRequisationNumber();
						 
						 if(allSchoolGradeDistrictVal==2)
							 ShowSchool();
					}else{
						//alert("You can not delete this Requsisition Number ");
						var divMsg="<div>"+resourceJSON.msgCantDelReqNo+"</div>";
						document.getElementById("Msg").innerHTML=divMsg;
						$('#delBtn').hide();
						$('#closeBtn').hide();
						$('#msjBtn').show();
						$('#myModalMsg').modal('show');
					}
				},
				errorHandler:handleError 
				});
		
	}
	else
	{
		$("#reqChild_"+delReqId).remove();
		//delReqId=","+delReqId+",";
		var arrValues="";
		var arrReqNum ="";
		if(allSchoolGradeDistrictVal==2)
		{
			arrReqNum	=	trim(document.getElementById("arrTotalReqNumforSchool").value);
		}
		else
		{
			if(allSchoolGradeDistrictVal==1)
			{
				arrReqNum	=	trim($("#arrReqNum").val());
			}
		}
		var isDelIdExist = arrReqNum.search(delReqId);
		//alert("isDelIdExist  : "+isDelIdExist);
		if(isDelIdExist!=-1)
		{
			//alert(" [ If Block ] ");
			var arrReqNumVal=arrReqNum.split(",");
			//alert(" arrReqNumVal.length "+arrReqNumVal.length);
			for(var i=0;i<arrReqNumVal.length-1;i++){
					if(arrReqNumVal[i]==delReqId){
						isIdExist=true;
					}
					else
					{
						arrValues+=arrReqNumVal[i]+",";
						//alert(" i :"+i+" arrReqNumVal[i] : "+arrReqNumVal[i])
					}
				}
			
			
			if(allSchoolGradeDistrictVal==2)
			{
				//alert(" Deleting Values if he selected School Radio Button : allSchoolGradeDistrictVal : "+allSchoolGradeDistrictVal)
				$("#arrTotalReqNumforSchool").val(arrValues);
				ManageJobOrdersAjax.deleteTempRequisationNumber(delReqId,jobAuthKey,{ 
					async: true,
					callback: function(data)
						{
							//alert(" data "+data);
							if(data){
								// displayRequisationNumber();
								 
								 /*if(allSchoolGradeDistrictVal==2)
									 ShowSchool();*/
							}
						},
						errorHandler:handleError 
						});
			}
			else
			{
				if(allSchoolGradeDistrictVal==1)
				{
					$("#arrReqNum").val(arrValues);
				}
			}
		}
		//else
		//{
			//alert(" [ Else Block ] ");
			/* ====== Gagan : In this case Controle come only when Req No deleted during Addition ====== */
			/*if(allSchoolGradeDistrictVal==2)
			{
				arrReqNum	=	trim(document.getElementById("arrReqNumforSchool").value);
			}
			var arrReqNumVal=arrReqNum.split(",");
			//alert(" arrReqNumVal.length "+arrReqNumVal.length);
			for(var i=0;i<arrReqNumVal.length-1;i++){
					if(arrReqNumVal[i]==delReqId){
						isIdExist=true;
					}
					else
					{
						arrValues+=arrReqNumVal[i]+",";
						//alert(" i :"+i+" arrReqNumVal[i] : "+arrReqNumVal[i])
					}
				}
			
			
			if(allSchoolGradeDistrictVal==2)
			{
				//alert(" Deleting Values if he selected School Radio Button : allSchoolGradeDistrictVal : "+allSchoolGradeDistrictVal)
				$("#arrReqNumforSchool").val(arrValues);
			}
			*/
		//}
		$("#delReqId").val("");
	}
	
	
}

function cancelReqNumber()
{
	//var isReqNoFlagByDistrict =	$("#isReqNoFlagByDistrict").val();
	$("#reqNumberDiv").hide();

	//if(isReqNoFlagByDistrict==1)
		//$("#hideAddReqLink").fadeIn();
}

function cancelReqNumberSchool()
{
	$("#reqNumberSchoolDiv").hide();
}

function showOnlyDistrictReqNo()
{
	$("#reqNumberDiv").fadeIn();
}

function displayRequisationNumber()
{
	//$("#reqNumberDiv").hide();
	var jobId	=	document.getElementById("jobId").value;
	var allSchoolGradeDistrictVal	=	trim(document.getElementById("allSchoolGradeDistrictVal").value);
	if(jobId!="" && jobId!=0)
	{
		ManageJobOrdersAjax.getRequisationNumberByJobId(jobId,allSchoolGradeDistrictVal,{ 
			async: true,
			callback: function(data)
				{
				//alert(" data "+data);
					if(data!=""){
						if(allSchoolGradeDistrictVal==1)
							$("#districtReqNumberDiv").html(data);
						else
							$("#districtReqNumberSchoolDiv").html(data);
					}else{
					}
				},
				errorHandler:handleError 
				});
	}
}

function addRequisationNumber(jobId,schoolId)
{
	//alert(" jobId "+jobId+" schoolId : "+schoolId)
	if(jobId!=0)
	{
		//$('#requisitionNumberSchool').focus();
		document.getElementById("requisitionNumberSchool").focus();
		$('#currentSchoolId').val(schoolId);
		$('#reqNumberSchoolDiv').show();
	}
}
function tpJbIEnableReq()
{
	var noOrRow = document.getElementById("schoolTable").rows.length;
	//alert(" noOrRow  "+noOrRow);
	for(var j=1;j<=noOrRow;j++)
	{	
		$('#req'+j).tooltip();
		//$('#cgn'+j).tooltip();
	}
}

//===================================================== [ Validate Add,Delete Requistion Number Start ] =======================================

function validateAddRequisationNumberForSchool()
{
	//alert(" validateAddRequisationNumberForSchool ");
	//$('#requisitionNumber').css("background-color","");
	var requisitionNumberSchool=trim(document.getElementById("requisitionNumberSchool").value);
	var noOfExpHires=trim(document.getElementById("noOfSchoolExpHires").value);
	var districtRequisitionIdSchool=document.getElementById("districtRequisitionIdSchool").value;
	var isReqNoFlagByDistrict = $("#isReqNoFlagByDistrict").val();

	var districtRequisitionIdCheckForSchool=false;
	
	/*======= Gagan : Duplicate will check only in case of Add Job order mode and In edit mode duplicate will check on Database level ============= */
	if(districtRequisitionIdSchool!="" && jobId!='')
	{
		districtRequisitionIdCheckForSchool=getRequisationIdCheckForSchool(districtRequisitionIdSchool);
	}
	$('#errordiv').empty();
	if(requisitionNumberSchool==''){
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.msgRequisitionNumber+"<br>");
		$('#requisitionNumberSchool').focus();
	}else if(districtRequisitionIdSchool==''){
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.msgvalidRequisitionNumber+"<br>");
		$('#requisitionNumberSchool').focus();
	}else if(districtRequisitionIdCheckForSchool){
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.msgAnotherRequisitionNumber+"<br>");
		$('#requisitionNumberSchool').focus();
		$('#districtRequisitionIdSchool').val("");
		$('#requisitionNumberSchool').val("");
		$('#requisitionNumberSchool').css("background-color","#F5E7E1");
	}else{
		
			var jobId=document.getElementById("jobId").value;
		//alert(" requisitionNumber : "+requisitionNumberSchool+" jobId : "+jobId);
		if(requisitionNumberSchool!="" && jobId=='')
		{
			//requisationDivValSchool+="<div class='span3' id='reqChild_"+districtRequisitionIdSchool+"'>"+requisitionNumberSchool+" <a href='javascript:void(0);' onclick=\"return deleteReqNum("+districtRequisitionIdSchool+");\"><img width='15' height='15' class='can' src='images/can-icon.png' alt=''></a></div>";
			requisationDivValSchool="<div class='span3' id='reqChild_"+districtRequisitionIdSchool+"'>"+requisitionNumberSchool+" <a href='javascript:void(0);' onclick=\"return deleteReqNum("+districtRequisitionIdSchool+");\"><img width='15' height='15' class='can' src='images/can-icon.png' alt=''></a></div>";
			//requisationDivValSchool+="<div class='span3' id='reqChild_"+districtRequisitionIdSchool+"'>"+requisitionNumberSchool+" </div>";
			//$("#districtReqNumberSchoolDiv").html("");
			$("#districtReqNumberSchoolDiv").append(requisationDivValSchool);
			
		}else if(jobId!=''){
			var currentSchoolId = $('#currentSchoolId').val();
			//alert(" Else Block ");	
			$("#arrReqNum").val("");
			ManageJobOrdersAjax.addRequisationNumber(jobId,districtRequisitionIdSchool,currentSchoolId, { 
				async: true,
				callback: function(data)
				{
					//alert(" data : "+data);
					if(data==3){
						$('#errordiv').show();
						$('#errordiv').append("&#149; "+resourceJSON.msgAnotherRequisitionNumber+"<br>");
						$('#requisitionNumberSchool').focus();
					}else{
						if(data==1)
						{
							displayRequisationNumber();
							ShowSchool();
						}
					}
				},
				errorHandler:handleError 
			});
		}
	$("#addRequisationDiv").hide();	
	document.getElementById("requisitionNumberSchool").value="";
	document.getElementById("noOfSchoolExpHires").value="";
	document.getElementById("districtRequisitionIdSchool").value="";
	}
}


function getRequisationIdCheckForSchool(districtRequisitionIdSchool){
	
	//alert(" Js Method ");
	//alert(" getRequisationIdCheckForSchool [ js method ] validateAddRequisationNumberForSchool :  ");
	var isIdExist =	false;
	try{
		//$('#currentSchoolId').val(schoolId);
		var isCommaExist =0;
		var arrReqNum =		$("#arrReqNumforSchool").val();
		var arrTotalReqNumforSchool  =		$("#arrTotalReqNumforSchool").val();
		var currentSchoolId = 	$('#currentSchoolId').val();
		var jobAuthKey = 	$('#jobAuthKey').val();
		var currentSchoolExpectedHires = 	$('#currentSchoolExpectedHires').val();
		var noOfExpHires=trim(document.getElementById("noOfSchoolExpHires").value);
		var arrSchoolReqNumVal=0;
		
		if(arrReqNum!="")
		{
			isCommaExist = arrReqNum.search(",");
			//alert(" isCommaExist "+isCommaExist+" arrReqNum :  "+arrReqNum);
			if(isCommaExist>0)
			{
				var arrReqNumVal=arrReqNum.split(",");
				for(var i=0;i<arrReqNumVal.length;i++){
						if(arrReqNumVal[i]==districtRequisitionIdSchool){
							isIdExist=true;
						}
					}
			}	
			else
			{
				if(arrReqNum==districtRequisitionIdSchool){
					isIdExist=true;
				}
			}
		}
		else
		{
			/*====== First time adding value to Field ======*/
			//alert(" ====== First time adding value to Field ======* ");
			
			if(arrTotalReqNumforSchool=="")
			{
				$("#arrReqNumforSchool").val(districtRequisitionIdSchool+",");
				
				//arrReqNumAndSchoolId
				$("#arrReqNumAndSchoolId").val(currentSchoolId+"-"+districtRequisitionIdSchool+",");
				
				arrReqNum =$("#arrReqNumforSchool").val();
				//alert(" arrReqNum : "+arrReqNum);
				if(arrReqNum!='')
				{
					arrSchoolReqNumVal=arrReqNum.split(",");
					//alert(" arrReqNum : "+arrReqNum+" arrSchoolReqNumVal : "+arrSchoolReqNumVal);
				}
				if(currentSchoolExpectedHires==(arrSchoolReqNumVal.length-1))
				{
					$("#reqNumberSchoolDiv").hide();
					
					$("#arrTotalReqNumforSchool").val($("#arrTotalReqNumforSchool").val()+""+$("#arrReqNumforSchool").val());
					$("#arrTotalReqNumAndSchoolId").val($("#arrTotalReqNumAndSchoolId").val()+""+$("#arrReqNumAndSchoolId").val());
					
					
					$("#arrReqNumforSchool").val("");
					$("#arrReqNumAndSchoolId").val("");
					$("#flagIfEqualSchoolReqNoAndNoofExpHire").val(1);
					
					/* ==================== Gagan :  Data Adding for Temp table in case of only School ===========*/
					//alert(" jobAuthKey 111111 : "+jobAuthKey);
					//alert(" First Time "+currentSchoolId+" --- "+districtRequisitionIdSchool+" -----"+currentSchoolExpectedHires);
					ManageJobOrdersAjax.addTempRequisationNumber(currentSchoolId,districtRequisitionIdSchool,currentSchoolExpectedHires,jobAuthKey, { 
						async: true,
						callback: function(data)
						{
							//alert(" data : "+data);
							if(data==3){
								$('#errordiv').show();
								$('#errordiv').append("&#149; "+resourceJSON.msgAnotherRequisitionNumber+"<br>");
								$('#requisitionNumberSchool').focus();
							}
						},
						errorHandler:handleError 
					});
					
					/* ==================== [ ENd ]===========*/
					
				}
				return false;
			}
		}
		
		if(arrTotalReqNumforSchool!="")
		{
			//alert("========================= Array Total Length  =======================");
			var arrReqNumTotalVal=arrTotalReqNumforSchool.split(",");
			for(var i=0;i<arrReqNumTotalVal.length;i++){
					//alert(" arrReqNumTotalVal[i] : "+arrReqNumTotalVal[i]+" districtRequisitionIdSchool :"+districtRequisitionIdSchool)
					if(arrReqNumTotalVal[i]==districtRequisitionIdSchool){
						//alert(" Alert come Here ");
						isIdExist=true;
					}
				}
		}
		if(!isIdExist)
		{
			//alert(" isIdExist : "+isIdExist);
			$("#arrReqNumforSchool").val($("#arrReqNumforSchool").val()+""+districtRequisitionIdSchool+",");
			$("#arrReqNumAndSchoolId").val($("#arrReqNumAndSchoolId").val()+""+currentSchoolId+"-"+districtRequisitionIdSchool+",");
			
			/* ==================== Gagan :  Data Adding for Temp table in case of only School ===========*/
			//alert(" Second  Time ");
			//alert(" Second Time "+currentSchoolId+" --- "+districtRequisitionIdSchool+" -----"+currentSchoolExpectedHires);
			ManageJobOrdersAjax.addTempRequisationNumber(currentSchoolId,districtRequisitionIdSchool,currentSchoolExpectedHires,jobAuthKey, { 
				async: true,
				callback: function(data)
				{
					//alert(" data : "+data);
					if(data==3){
						$('#errordiv').show();
						$('#errordiv').append("&#149; "+resourceJSON.msgAnotherRequisitionNumber+"<br>");
						$('#requisitionNumberSchool').focus();
					}
				},
				errorHandler:handleError 
			});
			/* ==================== END ===========*/
		}
		/*else
		{
			return false;
		}*/
		arrReqNum =$("#arrReqNumforSchool").val();
		//alert(" arrReqNum : "+arrReqNum);
		if(arrReqNum!='')
		{
			arrSchoolReqNumVal=arrReqNum.split(",");
			//alert(" arrReqNum : "+arrReqNum+" arrSchoolReqNumVal : "+arrSchoolReqNumVal);
		}
		//alert(" arrSchoolReqNumVal : "+arrSchoolReqNumVal)
		
		
		//alert("--------------- currentSchoolExpectedHires : "+currentSchoolExpectedHires+" \n arrSchoolReqNumVal : "+arrSchoolReqNumVal+" \n Length : "+arrSchoolReqNumVal.length);
		
		if(currentSchoolExpectedHires==(arrSchoolReqNumVal.length-1))
		{
			//alert(" ------------- If Block --------------");
			$("#reqNumberSchoolDiv").hide();
			
			$("#arrTotalReqNumforSchool").val($("#arrTotalReqNumforSchool").val()+""+$("#arrReqNumforSchool").val());
			$("#arrTotalReqNumAndSchoolId").val($("#arrTotalReqNumAndSchoolId").val()+""+$("#arrReqNumAndSchoolId").val());
			$("#arrReqNumforSchool").val("");
			$("#arrReqNumAndSchoolId").val("");
			$("#flagIfEqualSchoolReqNoAndNoofExpHire").val(1);
			
			
		}
		
		
	
	
	}catch(err){}
	return isIdExist;
}


// ======================================================= Auto complete Js Plugin for district Requisition Number =====================================


function getRequisationAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	//alert(" getRequisationAutoComp "+districtObj.value);
	//return false;
	
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38)
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13)
	{
		
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		//searchArray = getRequisationArray(txtSearch.value);
		//fatchData(txtSearch,searchArray,txtId,txtdivid);
		//document.getElementById("attendedInYear").focus();
		
	} 
	else if(event.keyCode==9)
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		//alert(" Else [ Value Part ] "+txtSearch.value);
		searchArray = getRequisationArray(txtSearch.value);
	//	alert(searchArray);
		fatchData3(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getRequisationArray(requisitionNumber){
	
	var districtId = $("#districtOrSchooHiddenlId").val();
	if(districtId==0 || districtId=="" || districtId==null)
	{
		$('#errordiv').empty();
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.PlzEtrDistrict+"<br>");
		$('#districtORSchoolName').focus();
		return false;
	}
	
	var searchArray = new Array();
	DWRAutoComplete.getRequisationList(requisitionNumber,districtId,{ 
		async: false,		
		callback: function(data){
		
		//alert(" data "+data);
		
		hiddenDataArray = new Array();
		showDataArray = new Array();
		for(i=0;i<data.length;i++){
			searchArray[i]=data[i].requisitionNumber;
			hiddenDataArray[i]=data[i].districtRequisitionId;
			showDataArray[i]=data[i].requisitionNumber;
		}
	}
	});	

	//alert(" districtOrSchooHiddenlId : "+districtOrSchooHiddenlId);
	return searchArray;
}


var selectFirst="";
var fatchData3= function(txtSearch,searchArray,txtId,txtdivid){
	var result = document.getElementById(txtdivid);
	try{
		//alert(" fatchData3 "+fatchData3);
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
				
			}
			
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}


function hideRequisationDiv(dis,hiddenId,divId)
{
	//setDefColortoErrorMsg();
	//$('#errordiv').empty();
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		//alert(hiddenId);
		//alert(hiddenDataArray[index]);
		//if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		//}
		
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
		//dis.value="";
	/*	if(dis.value!="")
		{
			var cnt=0;
			var focs=0;	
			$('#errordiv').empty();	
			setDefColortoErrorMsg();
			$('#errordiv').append("&#149; Please enter valid KK of Study<br>");
			if(focs==0)
				$('#requisitionNumber').focus();
			
			$('#requisitionNumber').css("background-color",txtBgColor);
			cnt++;focs++;	
		}*/
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}


function mouseOverChk(txtdivid,txtboxId)
{	
	//alert("Hi");
	for(var i=0;i<length;i++)
	{
		$('#divResult'+txtdivid+i).mouseover({param1: i,param2:txtdivid, param3:txtboxId}, fireMouseOverEvent);
	}
}
