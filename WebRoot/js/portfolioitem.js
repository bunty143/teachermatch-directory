function addNewItem()
{
	$("#itemDiv").show();
	$('#itemquantityDiv').hide();
	$("#districtName").val("");
	document.getElementById("itemId").options[0].selected=true;
	$('#itemquantity').val("");
	
}
function hideItemDiv(){
	$("#itemDiv").hide();
}
function checkForInt(evt) {

	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==8)
		return true;
	return( (charCode >= 48 && charCode <= 57)||(charCode==13) );
}
var itemStatusFlag=0;
function chkRequired(itemId)
{	
	if(itemId!=0){
		ManagePortfolioItemAjax.getItemQuantityRequiredStatus(itemId,{ 
			async: false,
			callback: function(data){
			if(data==1){
				$('#itemquantityDiv').show();
				itemStatusFlag=1;
			}else if(data==0){
				$('#itemquantityDiv').hide();
				itemStatusFlag=0;
			}
		},
		errorHandler:handleError 
		});
	}
}
function getPortfolioItemList(districtId)
{	
		var districtId=document.getElementById("districtIdFilter").value;
			DistrictPortfolioConfigAjax.getPortfolioItemList(districtId,{ 
				async: false,
				callback: function(data){
				$("#itemGrid").html(data);
				hideItemDiv();
				applyScrollOnTbl();
			},
			errorHandler:handleError 
			});
}
function savePortfolioItem()
{
	$('#errordiv').empty();
	$('#districtName').css("background-color","");
	$('#itemId').css("background-color","");
	$('#itemquantity').css("background-color","");
	
	var districtId=document.getElementById("districtId").value;
	var itemId=document.getElementById("itemId").value;
	var itemquantity=0;
	
	var cnt=0;
	var focs=0;
	
	if(districtId=="")
	{
		$('#errordiv').append("&#149; "+resourceJSON.PlzEtrDistrict+"<br>");
		if(focs==0)
			$('#districtName').focus();
		$('#districtName').css("background-color","#F5E7E1");
		cnt++;focs++;
	}
	
	if(itemId==0)
	{
		$('#errordiv').append("&#149; "+resourceJSON.MsgSelectItem+"<br>");
		if(focs==0)
			$('#itemId').focus();
		$('#itemId').css("background-color","#F5E7E1");
		cnt++;focs++;
	}
	if(itemStatusFlag==1){
		itemquantity=document.getElementById("itemquantity").value;
		if(itemquantity=="")
		{
			$('#errordiv').append("&#149; "+resourceJSON.PlzEnterQuantity+"<br>");
			if(focs==0)
				$('#itemquantity').focus();
			$('#itemquantity').css("background-color","#F5E7E1");
			cnt++;focs++;
		}
	}
	if(cnt!=0)		
	{
		$('#errordiv').show();
		return false;
	}else{
		DistrictPortfolioConfigAjax.savePortfolioItem(districtId,itemId,itemquantity,{ 
			async: false,
			callback: function(data){
			hideItemDiv();
			getPortfolioItemList(districtId);
		},
		errorHandler:handleError 
		});
		
	}
}

function editPortfolioItem(districtportfolioconfigId,portfolioItemName){
	var itemList=document.getElementById("itemId");
	DistrictPortfolioConfigAjax.getPortfolioItemStatus(districtportfolioconfigId,{ 
		async: false,
		callback: function(data){
			$("#itemDiv").show();
			$("#districtPortfolioConfigId").val(data.districtPortfolioConfigId);
			$("#districtName").val(data.districtMaster.districtName);
			$("#districtId").val(data.districtMaster.districtId);
			for (var i = 0; i < itemList.options.length; i++) {
		        if (itemList.options[i].text== portfolioItemName) {
		        	itemList.options[i].selected = true;
		        	break;
		        }
		    }
			if(portfolioItemName=="Cover Letter"||portfolioItemName=="Resume"){
				$('#itemquantityDiv').hide();
				itemStatusFlag=0;
			}else{
				$('#itemquantityDiv').show();
				itemStatusFlag=1;
			}
			if(portfolioItemName=="Academic"){
				$('#itemquantity').val(data.academic);
				itemStatusFlag=1;
			}
			if(portfolioItemName=="Academic Transcript"){
				$('#itemquantity').val(data.academicTranscript);
				itemStatusFlag=1;
			}
			if(portfolioItemName=="Certification"){
				$('#itemquantity').val(data.certification);
				itemStatusFlag=1;
			}
			if(portfolioItemName=="ProofOfCertification"){
				$('#itemquantity').val(data.proofOfCertification);
				itemStatusFlag=1;
			}
			if(portfolioItemName=="Reference"){
				$('#itemquantity').val(data.reference);
				itemStatusFlag=1;
			}
			if(portfolioItemName=="ReferenceLettersOfRecommentation"){
				$('#itemquantity').val(data.referenceLettersOfRecommendation);
				itemStatusFlag=1;
			}
		},
	errorHandler:handleError 
	});
}
function deletePortfolioItem(districtportfolioconfigId,portfolioItemName){
	DistrictPortfolioConfigAjax.deletePortfolioItem(districtportfolioconfigId,portfolioItemName,{ 
		async: false,
		callback: function(data){
		hideItemDiv();
		getPortfolioItemList();
	},
	errorHandler:handleError 
	});
}
/********************************** district auto completer ***********************************/
var count=0;
var index=-1;
var length=0;
var divid='';
var txtid='';
var hiddenDataArray = new Array();
var showDataArray = new Array();
var degreeTypeArray = new Array();
var hiddenId="";
var districtNameFilter=0;
function getDistrictMasterAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		if(districtNameFilter==1)
			document.getElementById("districtNameFilter").focus();
		else
			document.getElementById("districtName").focus();
		
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getDistrictMasterArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getDistrictMasterArray(districtName){
	var searchArray = new Array();
	ManageJobOrdersAjax.getFieldOfDistrictList(districtName,{ 
		async: false,
		callback: function(data){
		hiddenDataArray = new Array();
		showDataArray = new Array();
		for(i=0;i<data.length;i++){
			searchArray[i]=data[i].districtName;
			showDataArray[i]=data[i].districtName;
			hiddenDataArray[i]=data[i].districtId;
		}
	},
	errorHandler:handleError 
	});	
	return searchArray;
}


var selectFirst="";

var fatchData= function(txtSearch,searchArray,txtId,txtdivid){
	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
				
			}
			
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}

function hideDistrictMasterDiv(dis,hiddenId,divId)
{
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			
		}
		
	}else{
			if(document.getElementById(hiddenId))
				document.getElementById(hiddenId).value="";
	}
	
	if(document.getElementById(divId))
	{	
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}

var downArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index<length-1){
			for(var i=0;i<10;i++){
				{
					if(document.getElementById('divResult'+txtdivid+i))
						var div_id=document.getElementById('divResult'+txtdivid+i);
					if(div_id)
					{
						if(div_id.className=='over')
							index=div_id.id.split('divResult'+txtdivid)[1];
						div_id.className='normal';
					}
				}
			}
			index++;
			if(document.getElementById('divResult'+txtdivid+index))
			{
				var div_id=document.getElementById('divResult'+txtdivid+index);
				div_id.className='over';
				document.getElementById(txtid).value = div_id.innerHTML;
				selectFirst=div_id.innerHTML;
			}
		}
	}
}

var upArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index>0){
			for(var i=0;i<length;i++){

				var div_id=document.getElementById('divResult'+txtdivid+i);
				if(div_id)
				{
					if(div_id.className=='over')
						index=div_id.id.split('divResult'+txtdivid)[1];
					div_id.className='normal';
				}
			}
			index--;
			if(document.getElementById('divResult'+txtdivid+index))
				document.getElementById('divResult'+txtdivid+index).className='over';
			if(txtid && document.getElementById('divResult'+txtdivid+index))
			{
				document.getElementById(txtid).value=
					document.getElementById('divResult'+txtdivid+index).innerHTML;
				selectFirst=document.getElementById('divResult'+txtdivid+index).innerHTML;
			}
		}
	}
}

var overText=function (div_value,txtdivid) {

	for(var i=0;i<length;i++)
	{
		if(document.getElementById('divResult'+i))
		{
			var div_id=document.getElementById('divResult'+i);

			if(div_id.className=='over')
				index=div_id.id.split(txtdivid)[1];
			div_id.className='normal';
		}
	}
	div_value.className = 'over';
	document.getElementById(txtid).value= div_value.innerHTML;
}
function trim(s)
{
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}
function mouseOverChk(txtdivid,txtboxId)
{	
	for(var i=0;i<length;i++)
	{
		$('#divResult'+txtdivid+i).mouseover({param1: i,param2:txtdivid, param3:txtboxId}, fireMouseOverEvent);
	}
}


function fireMouseOverEvent(event)
{
	for(var i=0;i<length;i++)
	{	
		document.getElementById('divResult'+event.data.param2+i).className='normal';
	}
    document.getElementById('divResult'+event.data.param2+event.data.param1).className='over';	       
   	document.getElementById(event.data.param3).value= $('#divResult'+event.data.param2+event.data.param1).text();
    index=event.data.param1;
}


/*========  For Handling session Time out Error ===============*/
function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert(""+resourceJSON.msgServerErr+": "+exception.javaClassName);}
}


