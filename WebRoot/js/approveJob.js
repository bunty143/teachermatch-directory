

function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert(resourceJSON.msgSomeerror+": "+exception.javaClassName);}
}


function ShowSchool()
{	
	var jobId=0;
	if(document.getElementById("schoolId").value!=null){
		jobId=document.getElementById("jobId").value;
	}
	var JobOrderType=document.getElementById("JobOrderType").value;
	ApproveJobAjax.displaySchool(jobId,JobOrderType, { 
		async: true,
		callback: function(data)
		{
			document.getElementById("schoolRecords").innerHTML=data;			
		},
		errorHandler:handleError 
	});

}


function jobStartDateDiv(){
	 var cal = Calendar.setup({
         onSelect: function(cal) { cal.hide() },
         showTime: true
    });
	cal.manageFields("jobStartDate", "jobStartDate", "%m-%d-%Y");
}
function DisplayCertification()
{
	var jobId=0;
	var jobStartDate	=	trim(document.getElementById("jobStartDate").value);
	
	if(document.getElementById("jobId").value!=''){
		jobId=document.getElementById("jobId").value;
	}
	if(jobId==0){
		jobStartDateDiv();
	}else{
		if(jobStartDate!=""){
			var startDate= new Date(jobStartDate);   
			if(startDate < new Date()){
				document.getElementById('jobStartDate').readOnly=true;
			}else{
				jobStartDateDiv();
			}
		}else{
			jobStartDateDiv();
		}
	}	
	var JobOrderType=document.getElementById("JobOrderType").value;
	ApproveJobAjax.displayCertification(jobId,JobOrderType, { 
		async: true,
		callback: function(data)
		{	
			document.getElementById("certificationRecords").innerHTML=data;
		},
		errorHandler:handleError 
	});
	
}

/*========  displayJobRecords ===============*/
function displayJobRecords()
{
	var JobOrderType	=null;	
	var schoolId=null;
	var resultFlag=true;
	try{
		JobOrderType=document.getElementById("JobOrderType").value;
	}catch(err){
		JobOrderType=0;
	}
	try{
		schoolId=document.getElementById("schoolId").value;
	}catch(err){
		schoolId=0;
	}
	
	if(JobOrderType	==	"" || JobOrderType	==	null)
	{
		JobOrderType	=0;
	}
	if(document.getElementById("jobId").value!=''){
		document.getElementById("districtORSchoolName").disabled=true;
		document.getElementById("schoolName").disabled=true;
	}
	if(JobOrderType==2){
	//	document.getElementById("captionDistrictOrSchool").innerHTML	=	"District<span class=\"required\">*</span>";
		
		document.getElementById("hideForSchool").style.display='inline';
		document.getElementById("addHiresDiv").style.display='none';
		document.getElementById("schoolName").disabled=false;
	}else if(JobOrderType==3){
		document.getElementById("hideForSchool").style.display='none';
		document.getElementById("addHiresDiv").style.display='inline';
		//document.getElementById("captionDistrictOrSchool").innerHTML	=	"School<span class=\"required\">*</span>";
	
		document.getElementById("schoolName").disabled=true;
		if(schoolId==0){
			document.getElementById("schoolName").disabled=true;
		}else{
			document.getElementById("districtORSchoolName").disabled=true;
		}
		
	}
}


function getStatusList(districtId,jobCategoryId)
{	
	if(jobCategoryId!=0){
		setStatusList("",districtId,jobCategoryId);
	}else{
		$("#interviewPanel").hide();
	}
}

function setStatusList(jobId,districtId,jobCategoryId)
{	
	if(jobId==""){
		jobId=0;
	}
	ApproveJobAjax.getStatusList(jobId,districtId,jobCategoryId,{ 
		async: true,
		callback: function(data)
		{	
			if(data!=""){
				$("#secStatusList").html(data);
				var nds=document.getElementById("noofsecStatusCheckbox").value;
				if(nds!=0){
					$("#interviewPanel").show();
				}
			}else{
				$("#interviewPanel").hide();
			}
		},
		errorHandler:handleError 
	});
}
function hideDiv()
{ 
	 window.location.href="signin.do";
}
function approveJob()
{	
	
	var jobId=document.getElementById("jobId").value;
	try{
	ApproveJobAjax.isApproveOrNot(jobId,{ 
		async: false,
		callback: function(data)
		{
			if(data==1){
				$('#confirmMessage').modal('show');
			} else if(data=2){
				$('#confirmShowMessage .modal-body').html(resourceJSON.deactivateJobApprovalProcess);
				$('#confirmShowMessage').modal('show');	
			}else{
				$('#confirmShowMessage .modal-body').html("<p>Currently you are not eligable to approve this job.</p>");
				$('#confirmShowMessage').modal('show');	
			}
		},
		errorHandler:handleError
	});
	}catch(e){alert(e);}
}
function denyJob()
{	
			var div="<div class='modal hide in' id='confirmbox' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'>"+
			"<div class='modal-dialog' style='width:550px;'>"+
			"<div class='modal-content'>"+
			"<div class='modal-header'>"+
		    "<!--<button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>-->"+
		    "<h4 class='modal-title' id='myModalLabel' style='color:white;'></h4>"+
			"</div>"+
			"<div class='modal-body' id='confirmMessage'>"+	      
			"</div>"+
			"<div class='modal-footer'>"+
			"<button type='button' class='btn btn-primary' id='confirmTrue' ></button>"+
			"<button type='button' class='btn btn-default' id='confirmFalse'>"+resourceJSON.btnCancel+"</button>"+	        
			"</div>"+
			"</div>"+
			"</div>"+
			"</div>";
		$('#confirmMessage').after(div);
		confirmMessage = confirmMessage || '';
		$('#confirmMessage').modal('hide');
		$('#confirmbox').modal({show:true,
		                    backdrop: "static",
		                    keyboard: false,
		});
		$('#confirmbox #confirmMessage').html("<div class='divErrorMsg' id='errorDiv' style=\"display: block;\"></div><div id='denyDiv' class='col-sm-12 col-md-12'>" +
				"<label>Comments<span class='required'>*</span></label>:" +
				"<textarea class=\"span12\" rows=\"2\" id=\"comments\" name=\"comments\" maxlength=\"500\"></textarea>" +
				"</div>");
		$('#confirmbox #myModalLabel').html("TeacherMatch");
		$('#confirmbox #confirmTrue').html("Send to Posting Creator");
		$('#confirmFalse').click(function(){
		$('#confirmbox').modal('hide');
		$('#confirmbox').remove();
		//$('#jWTeacherStatusNotesDiv').modal('show');
		});
		$('#confirmTrue').click(function(){
		if(denyJobConfirm()){
			/*$('#confirmbox').modal('hide');
			$('#confirmbox').remove();*/
			//$('#jWTeacherStatusNotesDiv').modal('show');
		}
		});
		$('#comments').jqte();
}
function denyJobConfirm(){
	$('#errorDiv').hide();
	var jobId=document.getElementById("jobId").value;
	var comment=$("[name='comments']").parents().parents('.jqte').find(".jqte_editor").html();
	//alert(comment);
	if(comment.trim()==''){
		$('#errorDiv').html('');
		$('#errorDiv').append("&nbsp;&nbsp;&nbsp;&nbsp;&#149; Please enter Comment<br>");
		$("[name='comments']").parents().parents('.jqte').find(".jqte_editor").focus();
		$('#errorDiv').show();
		return false;
	}
	ApproveJobAjax.denyJobApprovalJobOrderWithComment(jobId,comment,{ 
		async: false,
		callback: function(data)
		{
			var checkReturn=""+data;
			var message="<p>This job is denied successfully.</p>";
			if(checkReturn==0){
				message="<p>You have been already denied this job.</p>";
			}
			$('#confirmShowMessage .modal-body').html(message);
			$('#confirmShowMessage').modal('show');	
			$('#denyButton').hide();
			$('#confirmbox').modal('hide');
			$('#confirmbox').remove();
			return true;
			
		},
		errorHandler:handleError
	});
	return false;
}
function approveJobOk()
{
	var jobId=document.getElementById("jobId").value;
	var userId=document.getElementById("userId").value;
	var entityType=document.getElementById("entityType").value;
	//alert(jobId+"   :::    "+userId+"     ::   "+entityType)	
//	var districtId=document.getElementById("districtHiddenId").value;
//	if(districtId==806900){
//		
//		ApproveJobAjax.approveJobOkForAdams(entityType,jobId,userId, { 
//			async: true,
//			callback: function(data)
//			{
//				$('#confirmMessage').modal('hide');
//				$('#confirmShowMessage').modal('show');			
//			},
//			errorHandler:handleError
//		});
//	}
//	else{
	ApproveJobAjax.approveJobOk(entityType,jobId,userId, { 
		async: true,
		callback: function(data)
		{
			$('#confirmMessage').modal('hide');
			$('#confirmShowMessage').modal('show');			
		},
		errorHandler:handleError
	});
	//}
}
function afterApproveJob()
{
	$("#approveButtonHide").hide();
}
