var dp_moduleName="";
var ncCertificationFlag="false";
var ncEducationFlag="false";
function callDynamicPortfolio_externalJobApply(savejobFlagByJsp){
	
	resetBottomPart();
	var savejobFlag=document.getElementById("savejobFlag").value;
	var jobApplySource=0;
	if(savejobFlag!="")
	{
		var jobId=document.getElementById("jobId").value;
		if(jobId > 0)
		{
			document.getElementById("txtDistrictPortfolioConfig").value=0;
			
			DistrictPortfolioConfigAjax.isJobApplied(jobId,{ 
				async: true,
				errorHandler:handleError,
				callback: function(data)
				{
					if(data!=null && data==false) // If Job is not applied
					{
						////////
						var candidateType="E";
						
						var isAffilated=0;
						try{
							isAffilated=document.getElementById("txtCandidateType").value;
						}catch(err){}
						
						if(isAffilated==1)
							candidateType="I";
						else
							candidateType="E";
						
						DistrictPortfolioConfigAjax.getPortfolioConfigByJobId(jobId,candidateType,{ 
							async: true,
							errorHandler:handleError,
							callback: function(data)
							{
								if(data!=null)
								{
									resetSBTNSource();									
									document.getElementById("IsSIForMiami").value=data.isSubstituteInstructionalForMiami;
									document.getElementById("isItvtForMiami").value=data.isInterventionistsForMiami;
									document.getElementById("jobTitleFeild").value=data.jobTitle;
									displayGKAndSubject();
									try{ transcriptUpload = data.transcriptUpload;}catch(e){}
									
									if(data.districtSpecificPortfolioQuestions==0 && data.academic ==0 && data.academicTranscript ==0 && data.certification ==0 && data.proofOfCertification ==0 && data.reference ==0 && data.referenceLettersOfRecommendation ==0 && data.resume==0 && data.tfaAffiliate==0 && data.willingAsSubstituteTeacher==0 && data.phoneNumber==0 && data.personalinfo==0 && data.ssn==0 && data.race==0 && data.formeremployee==0 && data.honors==0 && data.involvement==0 && data.additionalDocuments==0)
										document.getElementById("txtDistrictPortfolioConfig").value=0;
									else
										document.getElementById("txtDistrictPortfolioConfig").value=data.districtPortfolioConfigId;
									
									if(data.academic > 0 || data.academicTranscript > 0 || data.certification > 0 || data.proofOfCertification > 0 || data.reference > 0 || data.referenceLettersOfRecommendation > 0 || data.resume==1 || data.tfaAffiliate==1 || data.willingAsSubstituteTeacher==1 || data.phoneNumber==1 || data.personalinfo==1 || data.dateOfBirth==1 || data.ssn==1 || data.race==1 || data.formeremployee==1 || data.videoLink==1 || data.districtSpecificPortfolioQuestions==1 || data.honors==1 || data.involvement==1 || data.additionalDocuments==1)
									{	
										$("#divStdTchrExp").hide();
										$("#languageDiv").hide();
										if(data.districtMaster!=null && data.districtMaster.districtId==614730 && data.jobCategoryName=="School Nurse"){
											data.tfaAffiliate=0;
										}
										$("#addressOpt").show();
										//headQuaterId
										if(data.districtMaster!=null && data.districtMaster.districtId==3628590)
										{
											data.coverLetter=0;
										}
										$("#tfaOptional").val(data.tfaOptional);
										$("#nationalBoardOptional").val(data.nationalBoardOptional);
										$("#certfiedTeachingExpOptional").val(data.certfiedTeachingExpOptional);
										$("#substituteOptional").val(data.substituteOptional);
										$("#eEocOptional").val(data.eEOCOptional);
										$("#videoSecOptional").val(data.videoSecOptional);
										$("#gpaOptional").val(data.gpaOptional);
										$("#empSecSalaryOptional").val(data.empSecSalaryOptional);
										$("#empSecRoleOptional").val(data.empSecRoleOptional);
										$("#empSecPrirOptional").val(data.empSecPrirOptional);
										$("#empSecMscrOptional").val(data.empSecMscrOptional);
										$("#ressumeOptional").val(data.ressumeOptional);
										$("#addressOptional").val(data.addressOptional);
										$("#expectedSalarySection").val(data.expectedSalary);
										$("#certificationUrl").val(data.certificationUrl);
										$("#certificationDoeNumber").val(data.certificationDoeNumber);
			     						$("#ssnOptional").val(data.ssnOptional);
			     						$("#academicsDatesOptional").val(data.academicsDatesOptional);			     						
										$("#empDatesOptional").val(data.empDatesOptional);
										$("#certiDatesOptional").val(data.certiDatesOptional);
										$("#certiGrades").val(data.certiGrades);
										$("#empSecReasonForLeavOptional").val(data.empSecReasonForLeavOptional);
										$("#transcriptOptional").val(data.academicTranscript);
										$("#degreeOptional").val(data.degreeOptional);
			     						$("#schoolOptional").val(data.schoolOptional);
			     						
			     						$("#empPositionOptional").val(data.empPositionOptional);
										$("#empOrganizationOptional").val(data.empOrganizationOptional);
										$("#empCityOptional").val(data.empCityOptional);
			     						$("#empStateOptional").val(data.empStateOptional);
			     						$("#licenseLetterOptional").val(data.licenseLetterOptional);
			     						
			     						$("#fieldOfStudyOptional").val(data.fieldOfStudyOptional);
										  if(data.districtMaster!=null && data.districtMaster.headQuarterMaster!=null && data.districtMaster.headQuarterMaster.headQuarterId==2){
											
											  $("#addLicenseCerti").show();
												$("#showNCDPIOnlySsn").show();
												$("#loadEducationDiv").show();									
												$("#troops").show();												
												$("#ncCetrification").show();
												$("#normalTeacherCerti").hide();
												$("#licenseDivNBPTS").show();												
												$("#AcadHistoryProvidedCand").show();
												$("#headQuaterIdForDspq").val(data.districtMaster.headQuarterMaster.headQuarterId);
												$("#reqReferenceAstrick").show();
											}

										if(data.coverLetterOptional==false){
											data.coverLetter=0;
										}
										if(data.districtMaster!=null){
											document.getElementById("districtIdForDSPQ").value=data.districtMaster.districtId;
										}
										$(".pritr").hide();
										$('#divStdTchrExp').hide();
										$('#languageDiv').hide();
										$(".mscitr").hide();	
										$("#crequired").css("color", "red");
										$("#crequired2").css("color", "red");									
										$("#addressOpt").show();
										//headQuaterId
										
										$("#dspqQuestionHeading").html("District Specific Questions");
										if(data.dspqName!=null && data.dspqName!=""){
											$("#dspqName").val(data.dspqName);	
										}
										if(data.headQuarterMaster!=null){
																					
											$("#headQuaterIdForDspq").val(data.headQuarterMaster.headQuarterId);
											$("#dspqName").val(data.dspqName);
											if(data.headQuarterMaster.headQuarterId==1){
												
												data.coverLetter=0;
												$("#forKellyCvrLtr").hide();
												if($("#myModalCL #myModalLabel").html()=="Cover Letter"){
													$("#myModalCL #myModalLabel").html("Kelly Educational Staffing");
												}
												$("#kellYcvRLtrMain").show();
												$("#kellYnxtInst").hide();
												$("#kellYnxtDisDivInf").hide();
												$("#forAllDistCvrLtr").show();
												$("#workForKelly2").prop("checked",false);
												$("#workForKelly1").prop("checked",false);
												$("#contactedKelly1").prop("checked",false);
												$("#contactedKelly2").prop("checked",false);
												$(".forAllDistCvrLtr").hide();
												$(".forKellyCvrLtr").show();										
												$(".continueBtnNxt").hide();											
												$("#dspqQuestionHeading").html("Application Questions");
												
											}else if(data.headQuarterMaster.headQuarterId==2){
												//$(".addressOpt").hide();
												$(".tfarequired").hide();
												$("#expectedSalaryDiv").show();
											}
										}
										
										if(data.districtMaster!=null && data.districtMaster.districtId==7800040)
										{
											data.coverLetter=0;
											$("#expectedSalaryDiv").show();
											$(".expSalaryCss").show();
											$("#languageDiv").show();
											checkKnowLanguage();
										}
										else
										{
											$("#expectedSalaryDiv").hide();
											$(".expSalaryCss").hide();
										}

										if((data.districtMaster!=null && data.districtMaster.districtId==806900)||(data.districtMaster!=null && data.districtMaster.districtId==806810))
										{											
											$("#languageDiv").show();
											checkKnowLanguage();
										}

										if(data.districtMaster!=null && data.districtMaster.districtId==3702340)
										{
											$("#expectedSalaryDiv").show();
										}
										$("#jeffcoSeachDiv").hide();
										if(data.districtMaster!=null && data.districtMaster.districtId==804800){
											$("#jeffcoSeachDiv").show();
										}
										if(data.districtMaster!=null && data.districtMaster.districtId==804800 && (data.jobCategoryName.indexOf("Hourly") !=-1 || data.jobCategoryName=="Substitute Teacher"))
										{
											data.coverLetter=0;
										}
										
										if(data.districtMaster!=null && data.districtMaster.districtId==804800 && data.jobCategoryName=="Administrator/Protech")
										{
											$("#expectedSalaryDiv").show();
										}
										if(data.expectedSalary==1){
											$("#expectedSalaryDiv").show();
											$(".expSalaryCss").hide();
										}else if(data.expectedSalary==2){
											$("#expectedSalaryDiv").show();
											$(".expSalaryCss").show();
										}
										if(data.districtMaster!=null && data.districtMaster.districtId==3700112){
											data.coverLetter=0;
											$("#expectedSalaryDiv").show();
										}
										if(data.districtMaster!=null && (data.districtMaster.districtId==3703120 || data.districtMaster.districtId==3700690))
										{											
											$("#expectedSalaryDiv").show();
											if(data.districtMaster.districtId==3703120 && $('#jobcategoryDsp').val().trim().indexOf("Licensed") !=-1)
												$(".expSalaryCss").show();
										}
										$('#jobcategoryDsp').val(data.jobCategoryName);
										$(".pritr").hide();
										$(".mscitr").hide();
										$("#crequired").css("color", "red");
										$("#crequired2").css("color", "red");
										$(".philadelphiaCssRef").hide();
										if(data.districtMaster!=null && data.districtMaster.districtId==5510470){
											$('.newBerlinCss').show();
										}
										if(data.districtMaster!=null && data.districtMaster.districtId==3904380){
											$("#crequired").css("color", "white");
											$("#crequired2").css("color", "white");
											if(data.isNonTeacher==true){
												data.videoLink=false;
												data.additionalDocuments=false;
											}
										}
										
										if(data.districtMaster!=null && data.districtMaster.districtId==4218990)
										{
											$("#languageDiv").show();
											checkKnowLanguage();
											
											$(".optionalCss").hide();
											//$('#expectedSalaryDiv').show();
											$('.philadelphiaCss').show();
											$(".pritr").show();
											$(".mscitr").show();	
											$(".philadelphiaCssRef").show();
											$(".portfolio_Subheading:contains('"+resourceJSON.msgEmploymentHistory+"')").html(resourceJSON.msgEmployment);
											if(data.isSchoolSupportPhiladelphia==false && data.isNonTeacher==false){
												$('#divStdTchrExp').show();											
												displayStdTchrExp();
											}
											
										if(data.districtMaster!=null && data.districtMaster.districtId==4218990 && data.isNonTeacher==true){
											//$('#cvrltrTxt').show();
											$('.philNT').hide();
											$('.philadelphiaNTCss').show();
											$('#isnontj').val(true);
										}
										else if(data.districtMaster!=null && data.districtMaster.districtId==4218990 && data.isSchoolSupportPhiladelphia==true && data.coverLetter==1){
											data.coverLetter=0;
											data.videoLink=false;
											data.expCertTeacherTraining=false;
											data.academicTranscript=0;
											data.nationalBoardCert=false;
											data.willingAsSubstituteTeacher=false;
											data.tfaAffiliate=false;
											data.certification=0;
											data.proofOfCertification=0;
											data.additionalDocuments=false;
											data.dateOfBirth=false;
											data.reference=2;
											data.referenceLettersOfRecommendation=0;											
											$('#isSchoolSupportPhiladelphia').val("1");
											$('#cvrltrTxt').hide();
											$('.philNT').hide();
											$('.philadelphiaNTCss').show();																				
											$("#crequired").css("color", "white");
											$("#crequired2").css("color", "white");
											$(".philadelphiaCss:contains('"+resourceJSON.msgPhiladelphiaReq+"')").html(resourceJSON.msgPhiladelphiawith3Ref);
											
										}
										//tpl-1647 for custom configuration
										if(data.districtMaster!=null && data.districtMaster.districtId==4218990 && (data.isPrinciplePhiladelphia==true || $('input:radio[name=staffType]').is(":checked") || $("#staffTypeSession").val()=="I"))//for active principal(SA) & principal jobcategory  
										{
											if($('input:radio[name=staffType]').is(":checked") || $("#staffTypeSession").val()=="I"){
												$('#fe2').prop('checked', true);
												$("#fe2Div").show();									
											}
											$(".philadelphiaCss:contains('"+resourceJSON.msgPhiladelphiaReq+"')").html(resourceJSON.msgPhiladelphiawith3Ref);

											$("#isPrinciplePhiladelphia").val("1");
											data.reference=2;
											data.referenceLettersOfRecommendation=0;
											data.coverLetter=0;
											////////////////////
											data.academic=0;
											data.academicTranscript=0;
											data.dateOfBirth=0;
											data.employment=0;
											data.expCertTeacherTraining=0;
											data.tfaAffiliate=0;
											data.willingAsSubstituteTeacher=0;
											data.resume=0;
											$("#tfaDistSpecificoption").hide();
										//	data.districtSpecificPortfolioQuestions=0;
											////////////////////
											$('.philadelphiaNTCss1').show();
											$('.philadelphiaNTCss').hide();
											$('#expectedSalaryDiv').hide();
										}
										else
										{
											$('#expectedSalaryDiv').show();
										}
									}
										
										
										var isDspqReqForKelly=document.getElementById("isDspqReqForKelly").value;	
										var isKelly=document.getElementById("isKelly").value;	
										if(isKelly=="true" && isDspqReqForKelly=="false")
										{
											getDistrictQuestionsSet(jobId);
										}
										else
										{
											$('#myModalv').modal('hide');
											//alert("try to show loadingDiv_dspq_ie from external");
											$('#loadingDiv_dspq_ie').show();
											$('#myModalDymanicPortfolio').modal('show');
											//Empty All error message div
											$('#divErrorMsg_dynamicPortfolio').empty();
											$('#errordiv_AcademicForm').empty();
											resetUniversityForm();
											setDefColortoErrorMsg_Academic();
											
											$('#errordiv_Certification').empty();
											hideForm_Certification();
											
											$('#errordivElectronicReferences').empty();
											setDefColortoErrorMsgToElectronicReferences();
											hideElectronicReferencesForm();
											
											$('#errorDivResume').empty();
											$('#errDOB').empty();
											
											var countConfig_Residency=0;
											var countConfig_Academic=0;
											var countConfig_AcademicTranscript=0;
											var countConfig_Certification=0;
											var countConfig_ProofOfCertification=0;
											var countConfig_Reference=0;
											var countConfig_ReferenceLettersOfRecommendation=0;
											var resume_config=false;
											var tfaAffiliate_config=false;
											var willingAsSubstituteTeacher_config=false;
											var phoneNumber_config=false;
											var address_config=false;
											var exp_config=false;
											var nbc_config=false;
											
											var personalinfo_config=false;
											var dateOfBirth_config=false;
											var involvement_config=false;
											var honors_config=false;
											var ssn_config=false;
											var race_config=false;
											var formeremployee_config=false;
											var veteran_config=false;
											var ethnicOrigin_config=false;
											var ethinicity_config=false;
											var employment_config=false;
											var gender_config=false;
											var retireNo_config=false;
											
											var generalKnowledge_config=false;
											var subjectAreaExam_config=false;
											var additionalDocuments_config=false;
																					
											countConfig_Residency=data.residency;
											countConfig_Academic=data.academic;
											countConfig_AcademicTranscript=data.academicTranscript;
											countConfig_Certification=data.certification;
											countConfig_ProofOfCertification=data.proofOfCertification;
											countConfig_Reference=data.reference;
											countConfig_ReferenceLettersOfRecommendation=data.referenceLettersOfRecommendation;
											resume_config=data.resume;
											tfaAffiliate_config=data.tfaAffiliate;
											willingAsSubstituteTeacher_config=data.willingAsSubstituteTeacher;
											phoneNumber_config=data.phoneNumber;
											address_config = data.address;
											exp_config = data.expCertTeacherTraining;
											nbc_config = data.nationalBoardCert;
											var affidavit_config=data.affidavit;
											var candidateType = data.candidateType;
											personalinfo_config = data.personalinfo;
											dateOfBirth_config = data.dateOfBirth;
											involvement_config=data.involvement;
											honors_config=data.honors;
											ssn_config = data.ssn;
											race_config = data.race;
											formeremployee_config = data.formeremployee;
											veteran_config = data.veteran;
											ethnicOrigin_config = data.ethnicorigin;
											ethinicity_config = data.ethinicity;
											employment_config = data.employment;
											retireNo_config = data.retirementnumber;
											
											dSPQuestions_config = data.districtSpecificPortfolioQuestions;
											if(data.districtMaster!=null && data.districtMaster.districtId==4218990 && data.isNonTeacher==true){										
												videoLink_config=false;
											}else{
												videoLink_config=data.videoLink;
											}
											
											generalKnowledge_config = data.generalKnowledgeExam;
											subjectAreaExam_config = data.subjectAreaExam;
											additionalDocuments_config = data.additionalDocuments;
											gender_config = data.genderId;
											
											var IsSIForMiami=data.isSubstituteInstructionalForMiami;
											var isItvtForMiami=data.isInterventionistsForMiami;
											
											validateDynamicPortfolio(candidateType,countConfig_Academic,countConfig_AcademicTranscript,countConfig_Certification,countConfig_ProofOfCertification,countConfig_Reference,countConfig_ReferenceLettersOfRecommendation,resume_config,tfaAffiliate_config,willingAsSubstituteTeacher_config,phoneNumber_config,'level1',address_config,exp_config,nbc_config,affidavit_config,personalinfo_config,ssn_config,race_config,formeremployee_config,generalKnowledge_config,subjectAreaExam_config,additionalDocuments_config,veteran_config,ethnicOrigin_config,ethinicity_config,employment_config,gender_config,IsSIForMiami,isItvtForMiami,retireNo_config,videoLink_config,dSPQuestions_config,dateOfBirth_config,involvement_config,honors_config,countConfig_Residency,data);
											
										}
										
									}
									else
									{
										if(savejobFlag==1)
										{
											getDistrictQuestionsSet(jobId);
										}
									}
								}
								else // Call default Rule
								{
									if(savejobFlag==1)
									{
										getDistrictQuestionsSet(jobId);
									}
								}
							}
						});
						
						///////
					}
					else if(data!=null && data==true)
					{
						//$('#myModalv').modal('show');
						$('#loadingDiv_dspq_ie').hide();
						var isJobCompltd = $("#isJobCompltd").val();
						if(isJobCompltd!='Y')
						{
							DSPQServiceAjax.callNewSelfServiceApplicationFlow(jobId,'E',{ 
								async: false,
								errorHandler:handleError,
								callback:function(data)
								{
									if(data!='' && data!="DSPQNC" && data!="OldProcess" && data!="AlreadyCompleted")
									{
										window.location.href="./"+data;
									}
									else if(data!='' && data=="DSPQNC")
									{
										$('#loadingDiv_dspq_ie').hide();
										$('#DSPQNCDivNotification').modal('show');
									}
									else if(data!='' && data=="AlreadyCompleted")
									{
										$('#loadingDiv_dspq_ie').hide();
										$('#DSPQCompletedDivNotification').modal('show');
									}
									else
									{
										//alert("JCN Call Old Process");
										//Start ... Call Old Process
										DashboardAjax.getTeacherCriteria(jobId,{ 
											async: true,
											errorHandler:handleError,
											callback: function(data)
											{
												if(data!="")
												{
													//$('#loadingDiv_dspq_ie').hide();
													$('.nobleCssShow').hide();
													$('.newBerlinCss').hide();
													$("#crequired").css("color", "red");
													$("#crequired2").css("color", "red");
													try{
														/*if(data[3]=='epi')
														{
															$('#loadingDiv_dspq_ie').hide();
															try {
																$('#myModalDymanicPortfolio').modal('hide');
															} catch (e) {}
															
															checkInventory(0,jobId);
															return false;
														}*/
														
														document.getElementById('districtIdForDSPQ').value=data[7];
														if(data[7]!=1200390)
														{
															$('#transcriptDiv').show();
															$('#partInsEmp').hide();
														}
														else
														{
															$('#transcriptDiv').hide();
															$('#partInsEmp').show();
														}
														
														var districtIdForDSPQ="";	
														if ($('#districtIdForDSPQ').length > 0) {
															$('#districtIdForDSPQ').val(data[7]);
														}
														
														if(data[7]==7800038)
														{							
															$('.nobleCssHide').hide();
															$('.nobleCssShow').show();
															$('.#transcriptDiv').hide();
															$('#partInsEmp').show();					
														}
														
														if(data[7]==5510470)
														{
															$('.newBerlinCss').show();
														}
														
														if(data[7]==4218990){
															$(".optionalCss").hide();
															$(".philadelphiaCss").show();
															$('#expectedSalaryDiv').show();
															$(".pritr").show();
															$(".mscitr").show();						
														}
														
														if(data[7]==4218990 && data[9]=="true"){						
															$('.philNT').hide();
															$("#tfarequired").hide();
															$("#tfarequired").html("");
															$("#sSubTrequired").html("");
															$('.philadelphiaNTCss').show();
															$('#isnontj').val(true);									
														
														}else if(data[7]==4218990 && data[10]=="true"){						
															$('#isSchoolSupportPhiladelphia').val("1");						
															$('#cvrltrTxt').hide();
															$('.philNT').hide();
															$('.philadelphiaNTCss').show();
															$("#crequired").css("color", "white");
															$("#crequired2").css("color", "white");
															$(".philadelphiaCss:contains('"+resourceJSON.msgschooldistrictreferences+"')").html(resourceJSON.msgPhiladelphiawith3Ref);																					
														}
													}catch(err){}
													try{
														document.getElementById('teacherIdForDSPQ').value=data[8];
													}catch(err){}
													completeNow(data[0],data[1],data[2],data[3],data[5]);
													if(document.getElementById('isaffilatedstatushiddenId'))
													{
														try{
															document.getElementById("isDspqReqForKelly").value = data[11];	
															document.getElementById("isKelly").value=data[12];
														}catch(e){
															document.getElementById("isDspqReqForKelly").value = "false";	
															document.getElementById("isKelly").value= "false";
														}
														document.getElementById('isaffilatedstatushiddenId').value=data[4];
														$('#myModalLabelDynamicPortfolio').html(resourceJSON.msgRequiredApplicationItemsfor+data[6]);
														$('#displayDistrictNameForExt1').html(data[6]);
														$('#displayDistrictNameForExt2').html(data[6]);
														$('#displayDistrictNameForExt3').html(data[6]);
														
														try{$('#myModalCL').modal('hide');}catch(e){}
													}
													else{
														try{
															document.getElementById("isDspqReqForKelly").value = data[11];	
															document.getElementById("isKelly").value=data[12];
														}catch(e){
															document.getElementById("isDspqReqForKelly").value = "false";	
															document.getElementById("isKelly").value= "false";
														}
													}
													callForMiamiDistrict();
												}
											}
										});	
										
										//End ... Call Old Process
									}
									
								}});
						}
					}
				}
			});
			
		}
	}
}

function validateDynamicPortfolio(candidateType,countConfig_Academic,countConfig_AcademicTranscript,countConfig_Certification,countConfig_ProofOfCertification,countConfig_Reference,countConfig_ReferenceLettersOfRecommendation,resume_config,tfaAffiliate_config,willingAsSubstituteTeacher_config,phoneNumber_config,source,address_config,exp_config,nbc_config,affidavit_config,personalinfo_config,ssn_config,race_config,formeremployee_config,generalKnowledge_config,subjectAreaExam_config,additionalDocuments_config,veteran_config,ethnicOrigin_config,ethinicity_config,employment_config,gender_config,IsSIForMiami,isItvtForMiami,retireNo_config,videoLink_config,dSPQuestions_config,dateOfBirth_config,involvement_config,honors_config,countConfig_Residency,data){
	
  
	
	/*if(document.getElementById("districtIdForDSPQ").value==7800292)
	showschoollist12();*/
	var iErrorCount=0;
	var countSource_Academic=0;
	var countSource_AcademicTranscript=0;
	var countSource_Certification=0;
	var countSource_ProofOfCertification=0;
	var countSource_Reference=0;
	var countSource_ReferenceLettersOfRecommendation=0;
	var refrence_contact_no=0;
	var resume_source=false;
	var tfaAffiliate_source=false;
	var willingAsSubstituteTeacher_source=false;
	var phoneNumber_source=false;
	//var address_source=false;
	var exp_source=false;
	var nbc_source=false;
	var affidavit_source=false;
	
	var personalinfo_source=false;
	var ssn_source=false;
	var race_source=false;
	var formeremployee_source=false;
	var veteranValue_source=false;
	var ethnicOrigin_source=false;
	var ethnicity_source=false;
	var employment_source=false;
	var gender_source=false;
	
	var generalKnowledge_source=0;
	var subjectAreaExam_source=0;
	var additionalDocuments_source=0;
	var retireNo_source=0;
	var videoLink_source=0;
	var dSPQuestions_source=0;
	
	resetBottomPart();
	
	
	// Set Academic Transcript value
	document.getElementById("academicTranscriptFlag").value=countConfig_AcademicTranscript;
	
	//Display Grid Data
	showGridAcademics();
	showGridCertifications();
	getElectronicReferencesGrid();
	showGridAdditionalDocuments();
	getPFEmploymentDataGrid();
	showGridSubjectAreasGrid();
	if(dSPQuestionsErrorCount==0){
		showDistrictSpecificPortfolioQuestions();
	}
	getVideoLinksGrid();
	//resetTopAndSubDivErrors();
	
	var displayPassFailGK1=document.getElementById("displayPassFailGK").value;
	
	if(source=='level1'){
		
		resetSBTNSource();
		
		$('#errordiv_bottomPart_TFA').empty();
		if($('#errordiv_bottomPart_tfaOptions').length>0){
			$('#errordiv_bottomPart_tfaOptions').empty();
		}
		$('#errordiv_bottomPart_wst').empty();
		$('#errordiv_bottomPart_resume').empty();
		$('#errordiv_bottomPart_phone').empty();
		$('#errAddress1').empty();
//		$('#multyErrDiv').empty();
		$('#errAddressPr').empty();
		$('#errCountry').empty();
		$('#errZip').empty();
		$('#errState').empty();
		$('#errCity').empty();
		
		$('#errCountry').empty();
		$('#errZip').empty();
		$('#errState').empty();
		$('#errCity').empty();
		$('#errExpCTT').empty();
		$('#errNBCY').empty();
		$('#errAffidavit').empty();
		$('#errPersonalInfoAndSSN').empty();
		$('#errPersonalInfoAndSIN').empty();
		$('#errFormerEmployee').empty();
		$('#errRace').empty();
		$('#errGeneralKnowledge').empty();
		//$('#errSubjectArea').empty();
		$('#errAdditionalDocuments').empty();
		$('#errEthnicOrigin').empty();
		$('#errEthinicity').empty();
		$('#errGender').empty();
		$('#errDOB').empty();
		
		$('#tfaAffiliate').css("background-color","");
		$('#corpsYear').css("background-color","");
		$('#tfaRegion').css("background-color","");
		
		$('#resume').css("background-color","");
		
		//$('#phoneNumber').css("background-color","");
		$('#phoneNumber1').css("background-color","");
		$('#phoneNumber2').css("background-color","");
		$('#phoneNumber3').css("background-color","");
		
		$('#addressLine1').css("background-color","");
		$('#zipCode').css("background-color","");
		$('#stateIdForDSPQ').css("background-color","");
		$('#cityIdForDSPQ').css("background-color","");
		$('#otherState').css("background-color","");
		$('#otherCity').css("background-color","");
		$('#countryId').css("background-color","");
		
		$('#salutation_pi').css("background-color","");
		$('#firstName_pi').css("background-color","");
		$('#middleName_pi').css("background-color","");
		$('#lastName_pi').css("background-color","");
		$('#ssn_pi').css("background-color","");
		
		$('#dobMonth').css("background-color","");
		$('#dobDay').css("background-color","");
		$('#dobYear').css("background-color","");
		
		$('#rtDate').css("background-color","");
		$('#wdDate').css("background-color","");
		$('#rwdDate').css("background-color","");
		$('#retireNo').css("background-color","");
		
		try{
			$('#generalKnowledgeExamStatus').css("background-color","");
			$('#generalKnowledgeExamDate').css("background-color","");
			$('#generalKnowledgeScoreReport').css("background-color","");
		}catch(err){}
		try{
			$('#examStatus').css("background-color","");
			$('#examDate').css("background-color","");
			$('#subjectIdforDSPQ').css("background-color","");
			$('#scoreReport').css("background-color","");
		}catch(err){}
		
		
		setDefColortoErrorMsg_Academic();
		$('#errordiv_AcademicForm').empty();
		resetUniversityForm();
		
		hideForm_Certification();
		hideElectronicReferencesForm();
		
		cancelSubjectAreaForm();
		resetAdditionalDocumentsForm();

		setDefColortoErrorMsgToResidency();
		$('#errordivResidency').empty();
		closeResidencyForm();

		if(countConfig_Residency>0){
			getResidencyGrid();
			$('#residency').show();
		}else{
			$('#residency').hide();
		}
		
		//$('#dynamicPortfolioInformation_Inner').show();
		$('#dynamicPortfolioInformation_Inner').hide();
		$('#dynamicPortfolioInformation_Inner').empty();
				
		if(countConfig_Academic>0){
			$('#dynamicPortfolioInformation_Inner').append("&#149; "+resourceJSON.msgDetailsleast+" "+digitToString[countConfig_Academic]+" ("+countConfig_Academic+") "+resourceJSON.msgacademicdegree+"  ("+resourceJSON.msgfillacedmicdegree+")</BR>");
			$('#academicsDiv').show();
			if(document.getElementById("districtIdForDSPQ").value==7800040)
				$('#reqAcademicAstrick').show();
			else
				$('#reqAcademicAstrick').hide();
			
			$("#acaddemicHelpDiv").hide();
						if(document.getElementById("districtIdForDSPQ").value==1302010 && ($("#jobcategoryDsp").val()=="Classified" || $("#jobcategoryDsp").val()=="Substitutes")){
				$("#academicHelpTooltip").attr("data-original-title", "<p align='left'>"+resourceJSON.msgHighSchoolCollegeEdu+"</p>");						
				$("#acaddemicHelpDiv").show();
			}
						
			if(document.getElementById("districtIdForDSPQ").value==806900){
				$("#academicHelpTooltip").attr("data-original-title", "<p align='left'>Minimum of a high school diploma is required for this position</p>");						
				$("#acaddemicHelpDiv").show();
			}
			if(document.getElementById("districtIdForDSPQ").value==5304860){
				if($("#jobcategoryDsp").val()=="Paraprofessional"){
						$("#transUploadTooltip").attr("data-original-title", resourceJSON.msgGEDhighschoolhigher);
						$("#academicHelpTooltip").attr("data-original-title", "<p align='left'>"+resourceJSON.msgMustEnterTwoRecd+"</p>");						
						$("#acaddemicHelpDiv").show();
				}				
				
				if($("#jobcategoryDsp").val()=="Classified"){
					$("#transUploadTooltip").attr("data-original-title",resourceJSON.msguploadyourtranscript);
					$("#academicHelpTooltip").attr("data-original-title", "<p align='left'>"+resourceJSON.msgGEDhighschoolhigher+"<br>"+resourceJSON.msguploadGEDdiplomahigher+"</p>");
					$("#acaddemicHelpDiv").show();			
				}
			}
		
			if(data.districtMaster!=null && data.districtMaster.districtId==7800031){
				
					if($("#jobcategoryDsp").val().trim()=="Teacher Assistant"){						
							$("#academicHelpTooltip").attr("data-original-title", "<p align='left'>High School Diploma or GED minimum education requirement plus criteria outlined by ISBE</p>");						
							$("#acaddemicHelpDiv").show();
					}				
			}
		}
		$('#acadTransReq').hide();
		if(countConfig_AcademicTranscript==1){
			$('#dynamicPortfolioInformation_Inner').append("&#149; "+resourceJSON.msgUploadLettersforacademic+"</BR>");
			$('#academicsDiv').show();
			$('#acadTransReq').show();
		}
		if(countConfig_Academic==0 && countConfig_AcademicTranscript==0){
			$('#academicsDiv').hide();
		}
		
		if(countConfig_Certification>0){
			$('#dynamicPortfolioInformation_Inner').append("&#149; "+resourceJSON.msgDetailsleast+" "+digitToString[countConfig_Certification]+" ("+countConfig_Certification+") "+resourceJSON.msgCertificationLicense+"   ("+resourceJSON.msgfeelfreeprovideCertLic+")</BR>");
			$('#certificationDiv').show();
			if((document.getElementById("districtIdForDSPQ").value==7800033 && (($("#jobcategoryDsp").val()=="Student Support Services") || ($("#jobcategoryDsp").val()=="Instructional and SLT"))) || document.getElementById("districtIdForDSPQ").value==7800040  || document.getElementById("districtIdForDSPQ").value==806900)
				$('#reqCerificationAstrick').show();
			else
				$('#reqCerificationAstrick').hide();
			if(document.getElementById("districtIdForDSPQ").value==5304860 && $("#jobcategoryDsp").val().trim()=="Certificated"){
				$("#proofCertTooltip").show();
			}else if(document.getElementById("districtIdForDSPQ").value==1302010 && ($("#jobcategoryDsp").val().trim()=="Classified" || $("#jobcategoryDsp").val().trim()=="Substitutes")){
				$('#proofCertTooltip').attr('data-original-title', resourceJSON.msgUploadparaprofcert);
				$("#proofCertTooltip").show();
			}else{
				$("#proofCertTooltip").hide();
			}
			
			if(document.getElementById("districtIdForDSPQ").value==7800294){
				var jobCatName=$('#jobcategoryDsp').val().trim();
				if($('#jobcategoryDsp').val().trim()=="Clerical" || $('#jobcategoryDsp').val().trim().indexOf("Custodial/Maintenance") !=-1){
					$(".certiGuiText").html("This section is optional");							
					$(".certiGuiText").show();
				}
			}

			if(document.getElementById("districtIdForDSPQ").value==804800){

				if($('#jobcategoryDsp').val().trim()=="Licensed" || $('#jobcategoryDsp').val().trim().indexOf("Administrator/Protech") !=-1){
					$(".certiGuiText").html("Please ensure one of your certificates references your current status in Colorado.");							
					$(".certiGuiText").show();
				}
				var jobCatName=$('#jobcategoryDsp').val().trim();				
				if(jobCatName.indexOf("Hourly") !=-1){
					$(".certiGuiText").html("Not all hourly positions for Jeffco Public Schools require certifications. Please review the job description to understand if the position you are applying to requires specific certifications. If you have a certificate, please include it within this section.");							
					$(".certiGuiText").show();
				}
			}	
		}
		if(countConfig_ProofOfCertification==1){
			$('#dynamicPortfolioInformation_Inner').append("&#149; "+resourceJSON.msgUploadCertLicLtr+"</BR>");
			$('#certificationDiv').show();
			$("#proofOfCertificationReq").val(countConfig_ProofOfCertification);
			/*for(var i=0;i<document.getElementById("certificationStatusMaster").length;i++ )
			{
				if(document.getElementById("certificationStatusMaster")[i].value==3)
				{
					document.getElementById("certificationStatusMaster")[i].disabled = true;
				}
			}*/
		}
		if(countConfig_Certification==0 && countConfig_ProofOfCertification==0){
			$('#certificationDiv').hide();
		}
		
		/*if(IsSIForMiami)
		{
			$('#certificationDiv').hide();
			document.getElementById("displayGKAndSubject").value=false;
		}*/
		
		if(countConfig_Reference>0){
			$('#dynamicPortfolioInformation_Inner').append("&#149; "+resourceJSON.msgDetailsleast+" "+digitToString[countConfig_Reference]+" ("+countConfig_Reference+") "+resourceJSON.msgReferences+" ("+resourceJSON.msgprovidereferences+")</BR>");
			$('#referenceDiv').show();
			
			if(document.getElementById("districtIdForDSPQ").value==7800294){
				$(".refTextHeader").html("One reference must be a current or former supervisor/manager");
				$(".refTextHeader").show();
			}
			if(document.getElementById("districtIdForDSPQ").value==3904380){
				$(".refTextHeader").html(resourceJSON.msgProvide3Ref);
				$(".refTextHeader").show();
			}
			if(document.getElementById("districtIdForDSPQ").value==7800049 || document.getElementById("districtIdForDSPQ").value==7800048 || document.getElementById("districtIdForDSPQ").value==7800050 || document.getElementById("districtIdForDSPQ").value==7800051 || document.getElementById("districtIdForDSPQ").value==7800053 || document.getElementById("districtIdForDSPQ").value==7800053){
				$(".refTextHeader").html(resourceJSON.msgContactedImed);
				$(".refTextHeader").show();
			}
			if(document.getElementById("districtIdForDSPQ").value==1201470){
				$(".refTextHeader").html("Please note that references listed will be contacted immediately upon application. If you do not want a reference to be contacted at this point, please do not list them as part of your application. ");
				$(".refTextHeader").show();
			}
			if($("#districtIdForDSPQ").val()==806810 && data.referenceOptional==true){
				$("#reqReferenceAstrick").show();
			}
		}
		if(countConfig_ReferenceLettersOfRecommendation==1){
			$('#dynamicPortfolioInformation_Inner').append("&#149; "+resourceJSON.msgUploadLetterRecommendation+"</BR>");
			$('#referenceDiv').show();
			$('#recommLetter').show();
			if(document.getElementById("districtIdForDSPQ").value==1200390)
				$('#recommLetter').hide();
		}
		if(countConfig_Reference==0 && countConfig_ReferenceLettersOfRecommendation==0){
			$('#referenceDiv').hide();
		}
		$('.tfarequired').html("*");
		if(document.getElementById("districtIdForDSPQ").value==3700112 || document.getElementById("districtIdForDSPQ").value==1201470 || document.getElementById("districtIdForDSPQ").value==614730 || document.getElementById("districtIdForDSPQ").value==1302010 || document.getElementById("districtIdForDSPQ").value==3700690){
			$('.tfarequired').html("");
		}
		if(tfaAffiliate_config==1){
			
			$('#tfaTeacherDiv').show();
			$('#dynamicPortfolioInformation_Inner').append("&#149; "+resourceJSON.msgDetailsForAmericaAffiliate+"</BR>");
			document.getElementById("tFA_config").value=1;
			//$('#tfarequired').modal('show');
			$('#tfarequired').show();
			if(data!=null && data.tfaOptional==false){
				$('.tfarequired').html("");
			}
		}
		else
		{
			document.getElementById("tFA_config").value=0;
			//$('#tfarequired').modal('hide');
			$('#tfarequired').hide();
			$('#tfaTeacherDiv').hide();
		}
		getTFAValues();
		if(document.getElementById("districtIdForDSPQ").value==7800049 || document.getElementById("districtIdForDSPQ").value==7800048 || document.getElementById("districtIdForDSPQ").value==7800050 || document.getElementById("districtIdForDSPQ").value==7800051 || document.getElementById("districtIdForDSPQ").value==7800053){			
			$("#senNumDiv").show();
			getOctFields();
			getSeniorityNumber();
			$("#octCanadaDiv").show();
		}else{
			$("#senNumDiv").hide();
			$("#octCanadaDiv").hide();
		}
		
		if(document.getElementById("districtIdForDSPQ").value==4218990){
			if($("#isPrinciplePhiladelphia").val()==1)
				$("#tfaDistSpecificoption").hide();
			else
				$("#tfaDistSpecificoption").show();
			
			if($('#isSchoolSupportPhiladelphia').val()!="" && $('#isSchoolSupportPhiladelphia').val()=="1" ){
				$("#tfaDistSpecificoption").hide();
			}
			var districtMaster = {districtId:dwr.util.getValue("districtIdForDSPQ")};
			getDistrictSpecificTFAValues(districtMaster);			
		}else{
			$("#tfaDistSpecificoption").hide();
		}
		if(willingAsSubstituteTeacher_config==1){
			$('#dynamicPortfolioInformation_Inner').append("&#149; "+resourceJSON.msgDetailsSubstituteTeacher+"</BR>");
			document.getElementById("wst_config").value=1;
			$('#sSubTrequired').show();
			$('#substituteTeacherDiv').show();
			$("#textForSubs").html(resourceJSON.msgwillingserveteacher);
			if(document.getElementById("districtIdForDSPQ").value==804800 && ($('#jobcategoryDsp').val()==resourceJSON.msgTeacherLicensed || $('#jobcategoryDsp').val()==  resourceJSON.msgDigitalTeacherLibrarian || $('#jobcategoryDsp').val()==resourceJSON.msgSpecialEducationTeacher)){	
				$("#textForSubs").html(resourceJSON.msgnothiredanongoing);
			}
			if(document.getElementById("districtIdForDSPQ").value==806810){	
				$("#textForSubs").html(resourceJSON.msgwillingserveteacherSummitSchoolDis);
			}
			if($("#districtIdForDSPQ").val()==806810 && data.substituteOptional==true){
				$("#substituteAstrict").show();
			}
			if(data!=null && data.substituteOptional==false){
				$('#sSubTrequired').hide();
			}
		}
		else
		{
			document.getElementById("wst_config").value=0;
			$('#sSubTrequired').hide();
			$('#substituteTeacherDiv').hide();
		}
		if(phoneNumber_config==1){
			$('#dynamicPortfolioInformation_Inner').append("&#149; "+resourceJSON.msgDetailsofPhoneNumber+"</BR>");
			document.getElementById("phone_config").value=1;
			$('#phoneDiv').show();
			
			if(document.getElementById("districtIdForDSPQ").value==3904493){
				$("#phoneDiv .required").hide();
			}
		}
		else
		{
			document.getElementById("phone_config").value=0;
			$('#phoneDiv').hide();
		}
		//address validation
		$("#addressDivPresent").hide();
		if(address_config==1){
			$('#dynamicPortfolioInformation_Inner').append("&#149; "+resourceJSON.msgDetailsAddress+"</BR>");
			document.getElementById("address_config").value=1;
			$('#addressDiv').show();
			$('#addressDiv .required').show();
			if(data.addressOptional==false){
				$('#addressDiv .required').hide();
			}
			if(document.getElementById("districtIdForDSPQ").value==4503810 || document.getElementById("districtIdForDSPQ").value==7800294){
				$("#addressDivPresent").show();
			}
		}
		else
		{
			document.getElementById("address_config").value=0;
			$('#addressDiv').hide();
		}
		$("#striveExpFld").hide();
		if(exp_config==1){
            $('#dynamicPortfolioInformation_Inner').append("&#149; "+resourceJSON.msgDetailOfTeachingExperience+"</BR>");
            document.getElementById("exp_config").value=1;
            $('#yrOfcertTchrTxt').html(resourceJSON.msgCertifiedTeachingExperience+"<span class='required teacherExpReq'>*</span><a href='#' id='iconpophover10' rel='tooltip' data-original-title='"+resourceJSON.msgYearsfulltimeinstructor+"'><img src='images/qua-icon.png' width='15' height='15' alt=''></a>");
            if(document.getElementById("districtIdForDSPQ").value==7800047){
                //$('#yrOfcertTchrTxt').html(resourceJSON.msgYearstraditionalK12+"<span class='required teacherExpReq'>*</span><a href='#' id='iconpophover10' rel='tooltip' data-original-title='"+resourceJSON.msgcurrentlyteaching+"'><img src='images/qua-icon.png' width='15' height='15' alt=''></a>");
            	$('#yrOfcertTchrTxt').html(resourceJSON.msgYearstraditionalKK+"<span class='required teacherExpReq'>*&nbsp;</span><a href='#' id='iconpophover10' rel='tooltip' data-original-title='"+resourceJSON.msgcurrentlyteachingStrive+"'><img src='images/qua-icon.png' width='15' height='15' alt=''></a>");
            	//$('#yrOfcertTchrTxt').html("How many years of K-12 classroom teaching experience do you have?<span class='required'>*</span>");
            	$("#striveExpFld").hide();
            	$("#expCertTeacherTraining").show();
            }
            $('#expCertTeacherTrainingDiv').show();
            if(data!=null && data.certfiedTeachingExpOptional==false){
				$(".teacherExpReq").hide();
			}
        }

		else
		{
			document.getElementById("exp_config").value=0;
			$('#expCertTeacherTrainingDiv').hide();
		}
		
		if(document.getElementById("districtIdForDSPQ").value==804800 && $("#txtCandidateType").val()==1){		
			$(".teacherExpReq").hide();
		}
		if(nbc_config==1){
			$('#dynamicPortfolioInformation_Inner').append("&#149; "+resourceJSON.msgNBCLic+"</BR>");
			document.getElementById("nbc_config").value=1;
			$('#nationalBoardCertDiv').show();
			if(data!=null && data.nationalBoardOptional==false){
				$("#nationalBoardCertDiv  .required").hide();
			}
		}
		else
		{
			document.getElementById("nbc_config").value=0;
			$('#nationalBoardCertDiv').hide();
		}
		
		getAffidavitValue();
		if(affidavit_config==1)
		{
			$('#dynamicPortfolioInformation_Inner').append("&#149; "+resourceJSON.msgAffidavit+"</BR>");
			document.getElementById("affidavit_config").value=1;

			var affidavit = document.getElementsByName("affidavit");
			if(affidavit[0].checked)
				$('#affidavitDiv').hide();
			else
				$('#affidavitDiv').show();
		}
		else
		{
			document.getElementById("affidavit_config").value=0;
			$('#affidavitDiv').hide();
		}
		//getTeacherDetails();
		
		if(resume_config==1){
			$('#dynamicPortfolioInformation_Inner').append("&#149; "+resourceJSON.msgrecentresume+" ("+resourceJSON.msgPdfMsWordJpeg+")");
			document.getElementById("resume_config").value=1;
			$('#resumeDiv').show();
			if($("#headQuaterIdForDspq").val()!=null && $("headQuaterIdForDspq").val()==1){		
				$("#requiredRessume").hide();				
			}
			if(data!=null && data.ressumeOptional==false){
				$("#requiredRessume").hide();
				$("#requiredRessumeHeader").hide();
			}			
		}
		else
		{
			document.getElementById("resume_config").value=0;
			$('#resumeDiv').hide();
		}
		
		/******************************************************************************************/
		getStateByCountryForDspq("dspq");
		getStateByCountryForDspqPr("dspq");
		getPersonalInfoValues();
		if(document.getElementById("districtIdForDSPQ").value==1201470){
			$('#anotherNameDiv').show();
		}
		if(personalinfo_config==1)
		{
			$('#dynamicPortfolioInformation_Inner').append("&#149; "+resourceJSON.msgDetailsPI+"</BR>");
			document.getElementById("personalinfo_config").value=1;
			$('#peronalInformationDiv').show();
		}
		else
		{
			document.getElementById("personalinfo_config").value=0;
			$('#peronalInformationDiv').hide();
		}
		
		if(dateOfBirth_config==1){
			
		
			$('#dynamicPortfolioInformation_Inner').append("&#149; "+resourceJSON.msgDetailsDOB+"</BR>");
			
			document.getElementById("dateOfBirth_config").value=1;
			$('#dobDiv').show();
			$(".dobRemAst").show();
			if(document.getElementById("districtIdForDSPQ").value==4218990 || document.getElementById("districtIdForDSPQ").value==1302010){
				$(".dobRemAst").hide();
			}
		}
		else
		{
			document.getElementById("dateOfBirth_config").value=0;
			$('#dobDiv').hide();
		}
		
		if(race_config==1)
		{
			$('#dynamicPortfolioInformation_Inner').append("&#149; "+resourceJSON.msgDetailsRace+"</BR>");
			document.getElementById("race_config").value=1;
			$('#raceDiv').show();
		}
		else
		{
			document.getElementById("race_config").value=0;
			$('#raceDiv').hide();
		}
		
		if(gender_config==1)
		{
			$('#dynamicPortfolioInformation_Inner').append("&#149; "+resourceJSON.msgDetailsGender+"</BR>");
			document.getElementById("gender_config").value=1;
			$('#genderDiv').show();
		}
		else
		{
			document.getElementById("gender_config").value=0;
			$('#genderDiv').hide();
		}
		
		
		if(ssn_config==1)
		{
			var isAffilatedCheck=0;			
		    if($("#isaffilatedstatushiddenId").val()>0)
			{
				isAffilatedCheck = $("#isaffilatedstatushiddenId").val();	
			}	
			$('#dynamicPortfolioInformation_Inner').append("&#149; "+resourceJSON.msgDetailsSSN+"</BR>");
			document.getElementById("ssn_config").value=1;
			$('#ssnDiv').show();
			$('#ssnDiv .required').show();
			if(data.ssnOptional==false){
				$('#ssnDiv .required').hide();
				if(isAffilatedCheck && $('#headQuaterIdForDspq').val()==2)
				{					
					$('#ssnDiv .required').show();
				}
			}
		}
		else
		{
			document.getElementById("ssn_config").value=0;
			$('#ssnDiv').hide();
		}
		
		
		if(veteran_config==1)
		{
			$('#dynamicPortfolioInformation_Inner').append("&#149; "+resourceJSON.msgDetailsVeteran+"</BR>");
			document.getElementById("veteran_config").value=1;
			$('#veteranDiv').show();
		}
		else
		{
			document.getElementById("veteran_config").value=0;
			$('#veteranDiv').hide();
		}
		
		
		if(ethnicOrigin_config==1)
		{
			$('#dynamicPortfolioInformation_Inner').append("&#149; "+resourceJSON.msgDetailsEthnicOrigin+"</BR>");
			document.getElementById("ethnicOrigin_config").value=1;
			$('#ethnicOriginDiv').show();
			if(document.getElementById("districtIdForDSPQ").value==7800047){
				$('#eeodForOthers').html("<label><strong>"+resourceJSON.msgdataforEEOCinquiries+"</strong></label>");
			}else if(document.getElementById("districtIdForDSPQ").value==7800056){
				$('#eeodForOthers').html("<label><strong>EEOC information is optional.</strong></label>");
			}else{
				$('#eeodForOthers').html("<label><strong>"+resourceJSON.msgGovernmentagenciesrequire+"</strong></label>");
			}
		}
		else
		{
			document.getElementById("ethnicOrigin_config").value=0;
			$('#ethnicOriginDiv').hide();
		}
		
		
		if(ethinicity_config==1)
		{
			$('#dynamicPortfolioInformation_Inner').append("&#149; "+resourceJSON.msgDetailsEthnicity+"</BR>");
			document.getElementById("ethinicity_config").value=1;
			$('#ethinicityMasterDiv').show();
		}
		else
		{
			document.getElementById("ethinicity_config").value=0;
			$('#ethinicityMasterDiv').hide();
		}
		
		
		if(employment_config==1)
		{
			$("#empSecText").html();
			$("#empSecText").hide();
			$('#dynamicPortfolioInformation_Inner').append("&#149; "+resourceJSON.msgDetailsWorkExperience+"</BR>");
			$('#employmentDiv').show();
			if(document.getElementById("districtIdForDSPQ").value==1201470){
				$("#empSecText").html(resourceJSON.msgOsceolawillaskyou);
				$("#empSecText").show();
			}
			if($("#districtIdForDSPQ").val()==806810 && data.employeementOptional==true){
				$("#reqEmpAstrick").show();
			}else
			if(document.getElementById("districtIdForDSPQ").value==7800033 || document.getElementById("districtIdForDSPQ").value==7800040 || document.getElementById("districtIdForDSPQ").value==806900 || document.location.hostname=="nccloud.teachermatch.org" || document.location.hostname=="nc.teachermatch.org"  )
				$('#reqEmpAstrick').show();
			else
				$('#reqEmpAstrick').hide();
		}
		else
		{
			$('#employmentDiv').hide();
		}
		
		
		if(formeremployee_config==1)
		{
			if(document.getElementById("districtIdForDSPQ").value==4218990){
				$(".ntPhiLfeild").hide();
			}
			$('#dynamicPortfolioInformation_Inner').append("&#149; "+resourceJSON.msgDetailsFormerEmployee+"</BR>");
			document.getElementById("formeremployee_config").value=1;
			$('#formerEmployeeDiv').show();
		}
		else
		{
			document.getElementById("formeremployee_config").value=0;
			$('#formerEmployeeDiv').hide();
		}

		if(retireNo_config==1)
		{
			$('#dynamicPortfolioInformation_Inner').append("&#149; "+resourceJSON.msgRetirementNumber+"</BR>");
			document.getElementById("retireNo_config").value=1;
			$('#retirenoDiv').show();
		}
		else
		{
			document.getElementById("retireNo_config").value=0;
			$('#retirenoDiv').hide();
		}
		
		getGKEValues();
		if(generalKnowledge_config==1){
			$('#dynamicPortfolioInformation_Inner').append("&#149; "+resourceJSON.msgGeneralKnowledgeExam+"</BR>");
			document.getElementById("generalKnowledge_config").value=1;
			$('#generalKnowledgeDiv').show();
			if(data.generalKnowledgeExamOptional==false){
				$("#generalKnowledgeDiv .required").hide();	
			}
		}
		else
		{
			document.getElementById("generalKnowledge_config").value=0;
			$('#generalKnowledgeDiv').hide();
		}
		getSubjectByDistrictForDSPQ();
		getSAEValues();
		if(subjectAreaExam_config==1){
			$('#dynamicPortfolioInformation_Inner').append("&#149; "+resourceJSON.msgDetailsSubjectAreaExam+"</BR>");
			document.getElementById("subjectAreaExam_config").value=1;
			$('#subjectAreaDiv').show();
		}
		else
		{
			document.getElementById("subjectAreaExam_config").value=0;
			$('#subjectAreaDiv').hide();
		}
		
		if(document.getElementById("districtIdForDSPQ").value==614730){
			$("#divErrorMsg_top_gk_header").html("Pass/Fail CBEST Exam");
			$(".headingGkE").html("Pass/Fail CBEST Exam");
			
			$("#divErrorMsg_top_subarea_header").html("Pass/Fail CSET Exam");
			$(".headingSAE").html("Pass/Fail CSET Exam");
			$("#addLinkSAE").html("Add Pass/Fail CSET Exam");
		}else{
			$("#divErrorMsg_top_gk_header").html("Pass/Fail General Knowledge Exam");
			$(".headingGkE").html("Pass/Fail General Knowledge Exam");
			
			$("#divErrorMsg_top_subarea_header").html("Pass/Fail Subject Area Exam");
			$(".headingSAE").html("Pass/Fail Subject Area Exam");
			$("#addLinkSAE").html("Add Pass/Fail Subject Area Exam");
		}
		/*if(additionalDocuments_config==1){
			$('#dynamicPortfolioInformation_Inner').append("&#149; Details of Additional Documents.</BR>");
			document.getElementById("additionalDocuments_config").value=1;
			$('#additionalDocumentsDiv').show();
		}
		else
		{
			document.getElementById("additionalDocuments_config").value=0;
			$('#additionalDocumentsDiv').hide();
		}*/
		
		if(document.getElementById("districtIdForDSPQ").value==7800296 || document.getElementById("districtIdForDSPQ").value==7800047 || document.getElementById("districtIdForDSPQ").value==3904493 || document.getElementById("districtIdForDSPQ").value==614730 || document.getElementById("districtIdForDSPQ").value==100006 || document.getElementById("districtIdForDSPQ").value==7800045 || document.getElementById("districtIdForDSPQ").value==7800040 || (document.getElementById("districtIdForDSPQ").value==614730 && ($('#jobcategoryDsp').val()=="Single Subject" || $('#jobcategoryDsp').val()=="Multiple Subject")) || (document.getElementById("districtIdForDSPQ").value==2633090 && ($('#jobcategoryDsp').val()=="Internal Posting"))
				|| ($("#jobcategoryDsp").val().trim()=="Prospect" && document.getElementById("districtIdForDSPQ").value==7800031)) //
		{
			document.getElementById("additionalDocuments_config").value=0;
			$('#additionalDocumentsDiv').hide();
		}
		if(data.districtMaster!=null && data.districtMaster.districtId==7800031 && $("#jobcategoryDsp").val().trim()=="Teacher")
	    {
			$('.additionalDocumentsHeaderText').html("Please attach a lesson plan that reflects your teaching style, practice, and experience");
			$('.additionalDocumentsHeaderText').show();
	    }
		if(document.getElementById("districtIdForDSPQ").value==3904380) //
		{
			$('.additionalDocumentsHeaderText').html(resourceJSON.msgUploadLessonplans+"<br>"+resourceJSON.msgUploadsamplelesson+"");
			$('.additionalDocumentsHeaderText').show();
			if(additionalDocuments_config==1){
				$('#dynamicPortfolioInformation_Inner').append("&#149; "+resourceJSON.msgAdditionalDocuments+"</BR>");
				document.getElementById("additionalDocuments_config").value=1;
				$('#additionalDocumentsDiv').show();
			}
			else
			{
				document.getElementById("additionalDocuments_config").value=0;
				$('#additionalDocumentsDiv').hide();
			}
		}
		
		

		var dobValue=data.dateOfBirthOptional;
	    if(dobValue==1)
		{
			$('#dobOptional1').hide();
			$('#dobOptional2').hide();
			$('#dobOptional3').hide();
		    document.getElementById("dateOfBirthOptional").value=1;
		}
		
	    
	    /// khan
	    var sinValue=data.SINOptional;
	    if(sinValue==0){
			// hide
				$('#sinDiv').hide();
				document.getElementById("SIN_config").value=0;
				
				}else if(sinValue==1){
				// req
					$('#sinDiv').show();
					$('#4sinlastReq').show();
					$('#dynamicPortfolioInformation_Inner').append("&#149; "+resourceJSON.msgDetailsSIN+"</BR>");
					document.getElementById("SIN_config").value=1;
				}else if(sinValue==2){
				$('#sinDiv').show();
				$('#4sinlastReq').hide();
			    document.getElementById("SIN_config").value=2;
				// optional
			}else{
				// Other
				$('#sinDiv').hide();
			}
		
		
		

	

	  
		
		

		
		if(document.getElementById("districtIdForDSPQ").value==4218990) //
		{				
				document.getElementById("additionalDocuments_config").value=0;
				$('#additionalDocumentsDiv').hide();
			
			var nonteacherFlag=$("#isnontj").val();
			
			if(nonteacherFlag=="" || nonteacherFlag!="true"){
				if(additionalDocuments_config==true){
					document.getElementById("additionalDocuments_config").value=1;
					$('#additionalDocumentsDiv').show();
				}
			}else{
				document.getElementById("additionalDocuments_config").value=0;
				$('#additionalDocumentsDiv').hide();
			}
		}
		if(videoLink_config==1)
		{	if(document.getElementById("districtIdForDSPQ").value==3904380) //
			{
				$('#commonTextVideo').html(resourceJSON.msgurlvideolession);
			}
			$('#dynamicPortfolioInformation_Inner').append("&#149; "+resourceJSON.msgVideoLink+"</BR>");
			document.getElementById("videoLink_config").value=1;
			$('#videoLinkDiv').show();
		}
		else
		{
			document.getElementById("videoLink_config").value=0;
			$('#videoLinkDiv').hide();
		}
		
		if(involvement_config==1)
		{	getInvolvementGrid();		
			$('#dynamicPortfolioInformation_Inner').append("&#149; "+resourceJSON.msgVolunteerWork+"</BR>");
			document.getElementById("involvement_config").value=1;
			$('#involvementDiv').show();			
		}
		else
		{
			document.getElementById("involvement_config").value=0;
			$('#involvementDiv').hide();
		}
		
		if(honors_config==1)
		{	getHonorsGrid();		
			$('#dynamicPortfolioInformation_Inner').append("&#149; "+resourceJSON.msgHonors+"</BR>");
			document.getElementById("honors_config").value=1;
			$('#honorsDiv').show();			
		}
		else
		{
			document.getElementById("honors_config").value=0;
			$('#honorsDiv').hide();
		}
		
		if(document.getElementById("districtIdForDSPQ").value==4503810){
			$('#drivingDiv').show();
		}else{
			$('#drivingDiv').hide();
		}
		
		//District Specific Portfolio Questions Start
		if(dSPQuestions_config==1)
		{
			$('#dynamicPortfolioInformation_Inner').append("&#149; "+resourceJSON.msgDistrictSpecificQuestions+"</BR>");
			document.getElementById("dSPQuestions_config").value=1;
			$('#QuestionDiv').show();
		}
		else
		{
			document.getElementById("dSPQuestions_config").value=0;
			$('#QuestionDiv').hide();
		}
		
		//District Specific Portfolio Questions End
		/**************************************************************************************/
		
		$('#divErrorMsg_dynamicPortfolio').empty();
		$('#divErrorMsg_dynamicPortfolio').hide();
		
		$('#dynamicPortfolioInformation_Sub').hide();
		
		//getTFAValues();
		$('#errDOB').empty();
		$('#dobMonth').css("background-color","");
		$('#dobDay').css("background-color","");
		$('#dobYear').css("background-color","");
		
		cancelSubjectAreaForm();
		
		//alert("try to hide loadingDiv_dspq_ie from external");
		hideLoadingDiv_DSPQ();
		
	/*	if(document.getElementById("districtIdForDSPQ").value==804800){			
			openEmployeeNumberInfo($("#empfe2").val());
		}*/
		
	}
	else
	{
		//resetTopAndSubDivErrors();
		$('#dynamicPortfolioInformation_Sub').show();
		
		$('#dynamicPortfolioInformation_Inner').hide();
		
		$('#divErrorMsg_dynamicPortfolio').show();
		$('#divErrorMsg_dynamicPortfolio').empty();
	}
	
	showHideGK_SubjectArea();
	
	/*$('#errFormerEmployee').show();
	$('#errFormerEmployee').empty();*/
	
	var isMiamiChk=document.getElementById("isMiami").value;
	//getGenderByDistrict();
	if(isMiamiChk=="true")
	{
		$('#eeodForMiamiHeader').show();
		$('#eeodForMiami').show();
		$('#eeodForOthers').hide();
	}
	else
	{
		$('#eeodForMiamiHeader').hide();
		$('#eeodForMiami').hide();
		$('#eeodForOthers').show();
	}
	
	
	$('#iconpophover1').tooltip();
	$('#iconpophover2').tooltip();
	$('#iconpophover4').tooltip();
	$('#iconpophover5').tooltip();
	$('#iconpophover6').tooltip();
	$('#iconpophover7').tooltip();
	$('#iconpophover8').tooltip();
	$('#iconpophover9').tooltip();
	$('#iconpophover10').tooltip();
	$('#iconpophover11').tooltip();
	$('#iconpophover12').tooltip();
	$('#iconpophover13').tooltip();
	$('#iconpophoverPhone').tooltip();
	$('#iconpophoverSSN').tooltip();
	$('#iconpophoverSSNNCDPI').tooltip();
	$('#iconpophoverSolutation').tooltip();
	$('#iconpophoverCertification').tooltip();
	$('#iconpophoverLicensure').tooltip();
	$('#iconpophoverNCDPIcertificationtooltip').tooltip();
	$('#iconpoplicensure').tooltip();
	$('#iconpophoverSSNForNC').tooltip();
	
	
	
	
	// ********* Validate Portfolio ******************
	DistrictPortfolioConfigAjax.validatePortfolio({ 
		async: false,
		errorHandler:handleError,
		callback: function(portfolio)
		{
		
			$('#divErrorMsg_dynamicPortfolio').empty();
		
			//var isMiamiChk=document.getElementById("isMiami").value;
			if(document.getElementById("districtIdForDSPQ").value==7800047 && source=='level2'){
				ethinicity_config=0;
				ethnicOrigin_config=0;
				race_config=0;
				countConfig_Academic=0;
				countConfig_Certification=0;
				//countConfig_Reference=0;
				employment_config=0;
				videoLink_config=0;
			}
			if(document.getElementById("districtIdForDSPQ").value==3700690 && source=='level2'){
				ethinicity_config=0;
				ethnicOrigin_config=0;
				race_config=0;
				videoLink_config=0;
				if($('#jobcategoryDsp').val().trim().indexOf("Classified") !=-1 || $('#jobcategoryDsp').val().trim().indexOf("Bus Driver") !=-1){
					countConfig_Certification=0;
					exp_config=0;
					nbc_config=0;
					willingAsSubstituteTeacher_config=0;
					tfaAffiliate_config=0;
				}
			}
			
			if(document.getElementById("districtIdForDSPQ").value==3703120 && source=='level2'){
				if($('#jobcategoryDsp').val().trim().indexOf("Bus Driver") !=-1){
					exp_config=0;
					nbc_config=0;
					willingAsSubstituteTeacher_config=0;
				}
			}
			if(data!=null && data.eEOCOptional==false){
				ethinicity_config=false;
				ethnicOrigin_config=false;
				race_config=false;
			}
			if(data!=null && data.referenceOptional==false){
				countConfig_Reference=0;
			}
			
			if(data!=null && data.employeementOptional==false){
				employment_config=false;
			}
			// ********* Academic ******************
			countSource_Academic=portfolio[0];
			if(document.getElementById("districtIdForDSPQ").value==7800049 && formeremployee_config==true && $('#jobcategoryDsp').val().trim()=="Personnel enseignant"){
				countConfig_Academic=0;
			}
			
			if(document.getElementById("districtIdForDSPQ").value==3703120 && $('#jobcategoryDsp').val().trim().indexOf("Bus Driver")!=-1){
				countConfig_Academic=0;
			}
			if(document.getElementById("districtIdForDSPQ").value==804800 && $('#jobcategoryDsp').val().trim()!="Licensed"){
				countConfig_Academic=0;
			}
			if(data!=null && data.academicsOptional==false){
				countConfig_Academic=0;
				$('#reqAcademicAstrick').hide();
			}
			else
			{
				$('#reqAcademicAstrick').show();
			}
			
			try
			{
				if(data!=null)
				{				
					    if(data.districtMaster!=null && data.districtMaster.headQuarterMaster!=null && data.districtMaster.headQuarterMaster.headQuarterId==2)
					    {					    	
			                  if(document.getElementById("ncEducationCheck").value=='1')
			                  {			                	
			                	  ncEducationFlag="true";
			                  }
					    }
				}
			}catch(e){}		
			
			/*if(data.districtMaster!=null && data.districtMaster.headQuarterMaster!=null && data.districtMaster.headQuarterMaster.headQuarterId==2)
		    {
				if(ncEducationFlag=='false')
				{
					
				}
				else
				{
					if((countSource_Academic < countConfig_Academic)&& isMiamiChk=="false")
					{
						
						$('#divErrorMsg_dynamicPortfolio').append("&#149; "+resourceJSON.msgProvideAtLeast+" "+digitToString[countConfig_Academic]+" ("+countConfig_Academic+") "+resourceJSON.msgacademicdegree+" ("+resourceJSON.msgfillacedmicdegree+")</BR>");
						iErrorCount++;
						document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
					}					
				}			
			}
			else*/ 
			if(data.districtMaster!=null && data.districtMaster.districtId==7800031 && $("#jobcategoryDsp").val().trim()=="Administrator")
		    {
				var degreeTypeVals = new Array(); 
				if($(".degreeTypeVal").length>0)
					$(".degreeTypeVal").each(function() {
					    var degreeTypeVal = $(this).attr("degreeTypeVal");
					    degreeTypeVals.push(degreeTypeVal);
					});
				if($.inArray('M', degreeTypeVals) > -1==false){
					$('#divErrorMsg_dynamicPortfolio').append("&#149; "+resourceJSON.msgprovideMasterdegrees+"</BR>");
					iErrorCount++;
					document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
				}
			}else if(data.districtMaster!=null && data.districtMaster.districtId==7800031 && $("#jobcategoryDsp").val().trim()=="Teacher" ||(data.districtMaster.districtId==1201290 && $("#jobcategoryDsp").val().trim()=="Instructional")){
				var degreeTypeVals = new Array(); 
				if($(".degreeTypeVal").length>0)
					$(".degreeTypeVal").each(function() {
					    var degreeTypeVal = $(this).attr("degreeTypeVal");
					    degreeTypeVals.push(degreeTypeVal);
					});
				if($.inArray('B', degreeTypeVals) > -1==false){
					$('#divErrorMsg_dynamicPortfolio').append("&#149; "+resourceJSON.msgprovideBachelordegrees+"</BR>");
					iErrorCount++;
					document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
				}
			}
			else {
			var divDataEducationVal= document.getElementById("divDataEducationVal").value;
			if(divDataEducationVal==0 && (countSource_Academic < countConfig_Academic)&& isMiamiChk=="false")
			{
				$('#divErrorMsg_dynamicPortfolio').append("&#149; "+resourceJSON.msgProvideAtLeast+" "+digitToString[countConfig_Academic]+" ("+countConfig_Academic+") "+resourceJSON.msgacademicdegree+"  ("+resourceJSON.msgfillacedmicdegree+")</BR>");
				iErrorCount++;
				document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
			}else if(document.getElementById("districtIdForDSPQ").value==5304860 && (portfolio[26] < countConfig_Academic)){
								if($('#jobcategoryDsp').val()=="Administrator" || $('#jobcategoryDsp').val()=="Principal / Asst Principal" || $('#jobcategoryDsp').val()=="Certificated"){
					$('#divErrorMsg_dynamicPortfolio').append("&#149; "+resourceJSON.msgProvideAtLeast+" "+digitToString[countConfig_Academic]+" ("+countConfig_Academic+") "+resourceJSON.msgacademicBachelor+"  ("+resourceJSON.msgfillacedmicdegree+")</BR>");
					iErrorCount++;
					document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
				}else if($('#jobcategoryDsp').val()=="Paraprofessional"){
					
					var degreeTypeVals = new Array(); 
					if($(".degreeTypeVal").length>0)
						$(".degreeTypeVal").each(function() {
						    var degreeTypeVal = $(this).attr("degreeTypeVal");
						    degreeTypeVals.push(degreeTypeVal);
						});
					
					var checkColgDg=true;
					var checkColgDgCon="";
						if($.inArray('H', degreeTypeVals) > -1==false){
							$('#divErrorMsg_dynamicPortfolio').append("&#149; "+resourceJSON.msgoneHighSchoolorGED+"</BR>");
							iErrorCount++;
							document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
						}
						
						if($.inArray('A', degreeTypeVals) > -1){
							checkColgDgCon="contains";
						}
						
						if($.inArray('B', degreeTypeVals) > -1){
							checkColgDgCon="contains";
						}
						if($.inArray('D', degreeTypeVals) > -1){
							checkColgDgCon="contains";
						}
						if($.inArray('M', degreeTypeVals) > -1){
							checkColgDgCon="contains";
						}
						
						if(checkColgDgCon==""){
							$('#divErrorMsg_dynamicPortfolio').append("&#149; "+resourceJSON.msgcollagedevitcredit+"</BR>");
							iErrorCount++;
							document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
						}
				}
			}else if(document.getElementById("districtIdForDSPQ").value==7800038){
				
				var degreeTypeVals = new Array(); 
				if($(".degreeTypeVal").length>0)
					$(".degreeTypeVal").each(function() {
					    var degreeTypeVal = $(this).attr("degreeTypeVal");
					    degreeTypeVals.push(degreeTypeVal);
					});
				
				if($.inArray('D', degreeTypeVals) > -1){					
					if($.inArray('M', degreeTypeVals) > -1==false){
						$('#divErrorMsg_dynamicPortfolio').append("&#149; "+resourceJSON.msgprovideMasterdegrees+"</BR>");
						iErrorCount++;
						document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
					}else if($.inArray('B', degreeTypeVals) > -1==false){
						$('#divErrorMsg_dynamicPortfolio').append("&#149; "+resourceJSON.msgprovideBachelordegrees+"</BR>");
						iErrorCount++;
						document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
					}
					
				}else if($.inArray('M', degreeTypeVals) > -1){
					if($.inArray('B', degreeTypeVals) > -1==false){
						$('#divErrorMsg_dynamicPortfolio').append("&#149; "+resourceJSON.msgprovideBachelordegrees+"</BR>");
						iErrorCount++;
						document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
					}
				}
			}else if(countConfig_Academic && document.getElementById("districtIdForDSPQ").value==7800031 && $(".degreeTypeVal").length==1){
				var degreeTypeVals = new Array(); 
				if($(".degreeTypeVal").length>0)
					$(".degreeTypeVal").each(function() {
					    var degreeTypeVal = $(this).attr("degreeTypeVal");
					    degreeTypeVals.push(degreeTypeVal);
					});
				
				if($.inArray('N', degreeTypeVals) > -1){
					$('#divErrorMsg_dynamicPortfolio').append("&#149; Please provide atleast one  Bachelor's Degree or master's Degree </BR>");
					iErrorCount++;
					document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
				}
			}
			}
			//*************Honor****************
			if(portfolio[34]==0){
				
			}
		//*************End Honor****************				
		//*************Involvment****************		
			if(portfolio[33]==0 && (document.getElementById("districtIdForDSPQ").value==3700690 && ($('#jobcategoryDsp').val().trim().indexOf("Licensed") !=-1 || $('#jobcategoryDsp').val().trim().indexOf("Substitute") !=-1))&&($('#headQuaterIdForDspq').val()!=2)){

				$('#divErrorMsg_dynamicPortfolio').append("&#149; Please provide Involvement/Volunteer Work</BR>");
				iErrorCount++;
				document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
						
			}			
			/*if(portfolio[33]==0 && (document.getElementById("districtIdForDSPQ").value==804800 && $('#jobcategoryDsp').val().trim().indexOf("Licensed") !=-1)){

				$('#divErrorMsg_dynamicPortfolio').append("&#149; Please provide Involvement/Volunteer Work</BR>");
				iErrorCount++;
				document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
						
			}*/
		//*************End Involvment****************
			if(document.getElementById("districtIdForDSPQ").value==7800049){
				$('#divErrorMsg_top_currMemIn_header').hide();
				$('#divErrorMsg_top_currMemIn').hide();
				var octChk=false;
				$("#divErrorMsg_top_currMemIn").empty();
				var octNumber=$("#octNumber").val();
				var octText =$('#octText').find(".jqte_editor").html();			
				var hdnOctUpload =document.getElementById("hdnOctUpload").value;
				var octUpload =document.getElementById("octUpload").value;
				var octExt = octUpload.substr(octUpload.lastIndexOf('.') + 1).toLowerCase();
				var cnt_divErrorMsg_top_curMemeInf=0;
				if(document.getElementById("currMemOct1").checked){/*
					if(octNumber==""){
						$("#divErrorMsg_top_currMemIn").append("&#149; Please provide OCT Number</BR>");
						//octChk=true;
						cnt_divErrorMsg_top_curMemeInf++;
					}
					if(octText==""){
						//octChk=true;					
					}				
					if(octUpload=="" && hdnOctUpload==""){
						$("#divErrorMsg_top_currMemIn").append("&#149; Please upload OCT Card</BR>");
						//octChk=true;
						cnt_divErrorMsg_top_curMemeInf++;
					}
				*/}
				if(octExt!="")
				{
					if(!(octExt=='jpg' || octExt=='jpeg' || octExt=='gif' || octExt=='png' || octExt=='pdf' || octExt=='doc' || octExt=='docx' || octExt=='txt'))
					{
						$('#divErrorMsg_top_currMemIn').append("&#149; Please select Acceptable "+dwr.util.getValue("QS"+i+"question")+" formats which include PDF, MS-Word, GIF, PNG, and JPEG  files</BR>");
						cnt_divErrorMsg_top_curMemeInf++;
					}
				}
				if(cnt_divErrorMsg_top_curMemeInf!=0)
				{
					$('#divErrorMsg_top_currMemIn_header').show();
					$('#divErrorMsg_top_currMemIn').show();
					iErrorCount++;
				}
				else
				{
					$('#divErrorMsg_top_currMemIn_header').hide();
					$('#divErrorMsg_top_currMemIn').hide();
				}
				
				/*if(octChk){
					$('#divErrorMsg_dynamicPortfolio').append("&#149; Please provide current member information</BR>");
					iErrorCount++;
					document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
				}*/
			}
			if((portfolio[26] < countConfig_Academic) && isMiamiChk=="true")
			{
				var jobIdDSPQ="";
				var jobTitle="";
				try { 
					jobIdDSPQ=document.getElementById("jobId").value;
					jobTitle=document.getElementById("jobTitleFeild").value;
					
				} catch (e) {}
				if(jobTitle!="Teach For America 2015-2016")
				if(jobIdDSPQ!="6545")
				{
					$('#divErrorMsg_dynamicPortfolio').append("&#149; "+resourceJSON.msgProvideAtLeast+" "+digitToString[countConfig_Academic]+" ("+countConfig_Academic+") "+resourceJSON.msgacademicBachelor+"  ("+resourceJSON.msgfillacedmicdegree+")</BR>");
					iErrorCount++;
					document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
				}
			}
			// ********* Academic Transcript ******************
			countSource_AcademicTranscript=portfolio[1];
			if(isMiamiChk=='false'  && $("#divDataEducationVal").val()=="0")
			{
				if((countSource_AcademicTranscript > 0 || countSource_Academic==0) && countConfig_AcademicTranscript > 0)
				{
					$('#divErrorMsg_dynamicPortfolio').append("&#149; "+resourceJSON.msguploadLettersforacademic+"</BR>");
					iErrorCount++;
					document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
				}
			}
			
			
			
			// ********* Certification ******************
			countSource_Certification=portfolio[2];
			if(document.getElementById("districtIdForDSPQ").value==804800){
				if($("#jobcategoryDsp").val().indexOf("Administrator/Protech") !=-1){
					countConfig_Certification=0;
				}
			}
			if(document.getElementById("districtIdForDSPQ").value==3703120 && ($('#jobcategoryDsp').val().trim().indexOf("Classified") !=-1 || $('#jobcategoryDsp').val().trim().indexOf("Substitute") !=-1)){
				countConfig_Certification=0;
			}
			if(data!=null && data.certificationptional==false){
				countConfig_Certification=0;
			}
			
			
			try
			{
				if(data!=null)
				{				
					    if(data.districtMaster!=null && data.districtMaster.headQuarterMaster!=null && data.districtMaster.headQuarterMaster.headQuarterId==2)
					    {					    	
			                  if(document.getElementById("ncCertificationCheck").value=='1')
			                  {			                	
			                      ncCertificationFlag="true";
			                  }
					    }
				}
			}catch(e){}			
			
			if(data.districtMaster!=null && data.districtMaster.headQuarterMaster!=null && data.districtMaster.headQuarterMaster.headQuarterId==2)
		    {
				if(ncCertificationFlag=='false')
				{
					
				}
				else
				{
					if(countSource_Certification < countConfig_Certification && !IsSIForMiami)
					{
						$('#divErrorMsg_dynamicPortfolio').append("&#149; "+resourceJSON.msgProvideAtLeast+" "+digitToString[countConfig_Certification]+" ("+countConfig_Certification+") "+resourceJSON.msgCertificationLicense+"   ("+resourceJSON.msgfeelfreeprovideCertLic+")</BR>");
						iErrorCount++;
						document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
					}					
				}				
			}
			else
			{				
			if(document.getElementById("districtIdForDSPQ").value!=7800038)
			if(countSource_Certification < countConfig_Certification && !IsSIForMiami)
			{
				$('#divErrorMsg_dynamicPortfolio').append("&#149; "+resourceJSON.msgProvideAtLeast+" "+digitToString[countConfig_Certification]+" ("+countConfig_Certification+") "+resourceJSON.msgCertificationLicense+"   ("+resourceJSON.msgfeelfreeprovideCertLic+")</BR>");
				iErrorCount++;
				document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
			}
			}
			

			// ********* Proof Of Certification ******************
			countSource_ProofOfCertification=portfolio[3];
			if((countSource_ProofOfCertification > 0 || countSource_Certification==0) && countConfig_ProofOfCertification > 0 && !IsSIForMiami)
			{
				if(data!=null && data.certificationUrl==false){
					$('#divErrorMsg_dynamicPortfolio').append("&#149; "+resourceJSON.msgOnlyUploadCertification+"</BR>");
				}else{
					$('#divErrorMsg_dynamicPortfolio').append("&#149; "+resourceJSON.msgeitherCertificationorEnterURL+"</BR>");
				}
				iErrorCount++;
				document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
			
			}
			if((countSource_Certification!=0 && countSource_Certification!=portfolio[31]) && countConfig_Certification > 0 && document.getElementById("districtIdForDSPQ").value==7800040)
			{
				$('#divErrorMsg_dynamicPortfolio').append("&#149; "+resourceJSON.msgIEINNumberforeachCertification+"</BR>");
				iErrorCount++;
				document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
			}	
			// ********* Reference ******************
			countSource_Reference=portfolio[4];
			if(((document.getElementById("districtIdForDSPQ").value==7800049 || document.getElementById("districtIdForDSPQ").value==7800048 || document.getElementById("districtIdForDSPQ").value==7800050 || document.getElementById("districtIdForDSPQ").value==7800051 || document.getElementById("districtIdForDSPQ").value==7800053) && candidateType=="I" && $('#jobcategoryDsp').val().trim()=="Personnel enseignant")){
				countConfig_Reference=0;
			}
			if(countSource_Reference < countConfig_Reference)
			{
				$('#divErrorMsg_dynamicPortfolio').append("&#149; "+resourceJSON.msgProvideAtLeast+" "+digitToString[countConfig_Reference]+" ("+countConfig_Reference+") "+resourceJSON.msgReferences+" ("+resourceJSON.msgprovidereferences+")</BR>");
				iErrorCount++;
				document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
			}
			
			
			// ********* Reference Letters Of Recommendation ******************
			countSource_ReferenceLettersOfRecommendation=portfolio[5];
			var cntRecom=countSource_Reference-$('.recomTit').filter(function(){return $(this).is(':empty')}).length;
			if((countSource_ReferenceLettersOfRecommendation > 0 || countSource_Reference==0) && countConfig_ReferenceLettersOfRecommendation > 0 && cntRecom<countConfig_ReferenceLettersOfRecommendation)
			{
				$('#divErrorMsg_dynamicPortfolio').append("&#149; "+resourceJSON.msgLettersofRecommendation+"</BR>");
				iErrorCount++;
				document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
			}
			// ********* TFA ******************
			if(document.getElementById("districtIdForDSPQ").value==4218990){
				var nonteacherFlag=$("#isnontj").val();			
				if(nonteacherFlag=="true"){
					willingAsSubstituteTeacher_config=false;
					tfaAffiliate_config=false;					
				}
			}
			if((document.getElementById("districtIdForDSPQ").value==7800056 || document.getElementById("districtIdForDSPQ").value==3700112 || document.getElementById("districtIdForDSPQ").value==1201470 || document.getElementById("districtIdForDSPQ").value==614730 || document.getElementById("districtIdForDSPQ").value==1302010  || document.getElementById("districtIdForDSPQ").value==3700690 ||
					(document.getElementById("districtIdForDSPQ").value==3703120 && $('#jobcategoryDsp').val().trim().indexOf("Classified") !=-1) || (document.getElementById("districtIdForDSPQ").value==3703120 && $('#jobcategoryDsp').val().trim().indexOf("Substitute") !=-1)) || (($("#headQuaterIdForDspq").val()==2 && ($("#dspqName").val()=="Option A" || $("#dspqName").val()=="Option C")) )){
				tfaAffiliate_config=false;
			}
			if(data!=null && data.tfaOptional==false){
				tfaAffiliate_config=false;
			}
			hideTFAFields_DP();
			tfaAffiliate_source=portfolio[7];
			if(tfaAffiliate_config==true && tfaAffiliate_source==0)
			{
				$('#divErrorMsg_dynamicPortfolio').append("&#149; "+resourceJSON.msgTeachForAmericaTFA+"</BR>");
				iErrorCount++;
				document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
			}
			else if(tfaAffiliate_config==true)
			{
				
				var tfaAffiliate1 	= 	document.getElementById("tfaAffiliate").value;
				var corpsYear1 		= 	document.getElementById("corpsYear").value;
				var tfaRegion1 		= 	document.getElementById("tfaRegion").value;
				
				/*$('#divErrorMsg_dynamicPortfolio').append("&#149; Please provide Teach For America (TFA) Affiliate</BR>");
				iErrorCount++;
				document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;*/
				
				if(trim(tfaAffiliate1)=="")
				{
					iErrorCount++;
					document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
				}
				
				if(tfaAffiliate1!="3" && tfaAffiliate1!="")
				{
					if(trim(corpsYear1)=="")
					{
						iErrorCount++;
						document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;		
					}
					
					if(trim(tfaRegion1)=="")
					{
						iErrorCount++;
						document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
					}
				}
			}
			
			if(document.getElementById("districtIdForDSPQ").value==4218990){
				
				var nonteacherFlag=$("#isnontj").val();
				var atLeastOneIsChecked = $('.tfaOptId:checked').length;
				var isSchoolsupport=$("#isSchoolSupportPhiladelphia").val();		
				
				if((isSchoolsupport=="" || isSchoolsupport=="0") && (nonteacherFlag=="" || nonteacherFlag=="false")){
					if(document.getElementById('StuTchrChk').checked==true && $('#ttlRecStdExp').val()==0){
						$('#divErrorMsg_dynamicPortfolio').append("&#149; "+resourceJSON.msgStdTeachingExp+"</BR>");
						iErrorCount++;
						document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
					}
				}
				/*if(nonteacherFlag=="" && nonteacherFlag!="true"){
				
					if(atLeastOneIsChecked==0){						
						$('#divErrorMsg_dynamicPortfolio').append("&#149; PLEASE SELECT ONE OR PROGRAM YOU'VE BEEN INVOLVED IN</BR>");
						iErrorCount++;
						document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
					}
				}*/
			}
			
			//*********** video **********
			
			videoLink_source = portfolio[29];
			if((document.getElementById("districtIdForDSPQ").value==4218990 || document.getElementById("districtIdForDSPQ").value==1201470 || document.getElementById("districtIdForDSPQ").value==7800036 || document.getElementById("districtIdForDSPQ").value==3904380 || document.getElementById("districtIdForDSPQ").value==804800 || document.getElementById("districtIdForDSPQ").value==3700112 || document.getElementById("districtIdForDSPQ").value==3702040) || (($("#headQuaterIdForDspq").val()==2 && ($("#dspqName").val()=="Option A" || $("#dspqName").val()=="Option A")) )) 
			{
				videoLink_config=false;
			}
			if(data!=null && data.videoSecOptional==false){
				videoLink_config=false;
			}
			if(videoLink_config==true && videoLink_source==0){
				$('#divErrorMsg_dynamicPortfolio').append("&#149; "+resourceJSON.msgPlzProvideVideoLink+"</BR>");
				iErrorCount++;
				document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
			}
			// ********* willingAsSubstituteTeacher ******************
			willingAsSubstituteTeacher_source=portfolio[8];
			$("#sSubTrequired").show();
			if((document.getElementById("districtIdForDSPQ").value==804800 && $('#jobcategoryDsp').val().trim()=="Licensed") || (($("#headQuaterIdForDspq").val()==2 && ($("#dspqName").val()=="Option A" || $("#dspqName").val()=="Option C")))){
				willingAsSubstituteTeacher_config=false;
				$("#sSubTrequired").hide();
			}
			
			if(document.getElementById("districtIdForDSPQ").value==3702040){
				willingAsSubstituteTeacher_config=false;
				$("#sSubTrequired").hide();
			} 
			if(data!=null && data.substituteOptional==false){
				willingAsSubstituteTeacher_config=false;
				$('#sSubTrequired').hide();
			}
			if(willingAsSubstituteTeacher_config==true && willingAsSubstituteTeacher_source==0)
			{
				//$('#divErrorMsg_dynamicPortfolio').append("&#149; Please select if you are willing to work as substitute teacher or not</BR>");
				//iErrorCount++;
				
				//document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
				
				var canServeAsSubTeacher=2;
				try{
					if (document.getElementById('canServeAsSubTeacher0').checked) {
						canServeAsSubTeacher = document.getElementById('canServeAsSubTeacher0').value;
					}else if (document.getElementById('canServeAsSubTeacher1').checked) {
						canServeAsSubTeacher = document.getElementById('canServeAsSubTeacher1').value;
					}
				}catch(err){alert(err);}
				if(canServeAsSubTeacher==2)
				{
					$('#divErrorMsg_dynamicPortfolio').append("&#149; "+resourceJSON.msgwillingworkassubstitute+"</BR>");
					iErrorCount++;
				
					document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
				}
			}
			
			// ********* phoneNumber ******************
			phoneNumber_source=portfolio[9];
			phoneTypeUSNonUS=portfolio[34];
			
			if(document.getElementById("districtIdForDSPQ").value==3904493){
				phoneNumber_config=false;
			}
			if(phoneTypeUSNonUS!=1){
				var phonnumber="";
				var NonUsPhonNumnerId1=$("#NonUsPhonNumnerId1").val();
				//var NonUsPhonNumnerId2=$("#NonUsPhonNumnerId2").val();
				var NonUsPhonNumnerId3=$("#NonUsPhonNumnerId3").val();
				if(NonUsPhonNumnerId1!="" && NonUsPhonNumnerId3!="")
					phonnumber=NonUsPhonNumnerId1+NonUsPhonNumnerId3;
				
				$("#NonUsPhonNumnerId1").css("background-color","");
				//$("#NonUsPhonNumnerId2").css("background-color","");
				$("#NonUsPhonNumnerId3").css("background-color","");
				
				if(phoneNumber_config==true && NonUsPhonNumnerId1.length!=0 && NonUsPhonNumnerId1.length < 3)
				{
					$('#divErrorMsg_dynamicPortfolio').append("&#149; "+resourceJSON.msgValidPhoneNo+"</BR>");
					$("#NonUsPhonNumnerId1").css("background-color","F5E7E1");
					//$("#NonUsPhonNumnerId2").css("background-color","F5E7E1");
					$("#NonUsPhonNumnerId3").css("background-color","F5E7E1");
					iErrorCount++;
				}else if(phoneNumber_config==true && phonnumber=="" )
				{
					$('#divErrorMsg_dynamicPortfolio').append("&#149; "+resourceJSON.msgprovidePhoneNo+"</BR>");
					$("#NonUsPhonNumnerId1").css("background-color","F5E7E1");
					//$("#NonUsPhonNumnerId2").css("background-color","F5E7E1");
					$("#NonUsPhonNumnerId3").css("background-color","F5E7E1");
					iErrorCount++;
				}
				
			/*else if(phoneNumber_config==true){
				alert("in the JJJJJJJJJJJJJJJJJJJ");
				$('#divErrorMsg_dynamicPortfolio').append("&#149; "+resourceJSON.msgprovidePhoneNo+"</BR>");
				iErrorCount++;
			}*/
			document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
			
		}else{
			if(phoneNumber_config==true && phoneNumber_source==0)
			{
				var phoneNumber=""; //document.getElementById("phoneNumber").value;
				
				var phoneNumber1=document.getElementById("phoneNumber1").value;
				var phoneNumber2=document.getElementById("phoneNumber2").value;
				var phoneNumber3=document.getElementById("phoneNumber3").value;
				
				if(phoneNumber1!="" && phoneNumber2!="" && phoneNumber3!="")
					if(phoneNumber1.length==3 && phoneNumber2.length==3 && phoneNumber3.length==4)
						phoneNumber=phoneNumber1+phoneNumber2+phoneNumber3;
				
				if(phoneNumber=="")
				{
					if(phoneNumber1=="" && phoneNumber2=="" && phoneNumber3=="")
					{
						$('#divErrorMsg_dynamicPortfolio').append("&#149; "+resourceJSON.msgprovidePhoneNo+"</BR>");
					}
					else if(phoneNumber1.length!=3 || phoneNumber2.length!=3 || phoneNumber3.length!=4)
					{
						$('#divErrorMsg_dynamicPortfolio').append("&#149; "+resourceJSON.msgValidPhoneNo+"</BR>");
					}	
					iErrorCount++;
					document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
				}
				/*$('#divErrorMsg_dynamicPortfolio').append("&#149; Please provide Phone Number</BR>");
				iErrorCount++;
				document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;*/
			}
			else if(phoneNumber_config==true)
			{
				var phoneNumber="";//document.getElementById("phoneNumber").value;
				
				var phoneNumber1=document.getElementById("phoneNumber1").value;
				var phoneNumber2=document.getElementById("phoneNumber2").value;
				var phoneNumber3=document.getElementById("phoneNumber3").value;
				if(phoneNumber1!="" && phoneNumber2!="" && phoneNumber3!="")
					if(phoneNumber1.length==3 && phoneNumber2.length==3 && phoneNumber3.length==4)
						phoneNumber=phoneNumber1+phoneNumber2+phoneNumber3;
				
				
				if(trim(phoneNumber)=="")
				{
					if(phoneNumber1=="" && phoneNumber2=="" && phoneNumber3=="")
						$('#divErrorMsg_dynamicPortfolio').append("&#149; "+resourceJSON.msgprovidePhoneNo+"</BR>");
					else if(phoneNumber1.length!=3 || phoneNumber2.length!=3 || phoneNumber3.length!=4)
						$('#divErrorMsg_dynamicPortfolio').append("&#149; "+resourceJSON.msgValidPhoneNo+"</BR>");
					
					iErrorCount++;
					document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
				}
			}
			}
			////////////////////sekhar
			if(dSPQuestions_config==true){
				$('#errordivspecificquestion').empty();
				var arr =[];
				var jobOrder = {jobId:document.getElementById("jobId").value};
				var isRequiredCount=0;
				var ansRequiredCount=0;
				var errorFlagCheck=0;
				for(i=1;i<=totalQuestionsList;i++)
				{   
					var schoolMaster="";
					var isRequiredAns=0;
					var isRequired = dwr.util.getValue("QS"+i+"isRequired");
					if(isRequired==1){
						isRequiredCount++;
					}
					var districtSpecificQuestion = {questionId:dwr.util.getValue("QS"+i+"questionId")};
					var questionTypeShortName = dwr.util.getValue("QS"+i+"questionTypeShortName");
					var questionTypeMaster = {questionTypeId:dwr.util.getValue("QS"+i+"questionTypeId")};
					var qType = dwr.util.getValue("QS"+i+"questionTypeShortName");
					var o_maxMarks = dwr.util.getValue("o_maxMarksS");
					
					if(isRequired==1 && (qType=="drsls" || qType=="dt" || qType=="sloet"))
					{
						ansRequiredCount++;
					}
					
					if(qType=='tf' || qType=='slsel' ||  qType=='slsel')
					{
						var optId="";
						var errorFlag=1;
						var isValidAnswer=false;
						if($("input[name=QS"+i+"opt]:radio:checked").length > 0 )
						{
							 errorFlag=0;
							optId=$("input[name=QS"+i+"opt]:radio:checked").val();
						}else if(isRequired==1){
							$("#errordivspecificquestion").html("&#149; "+resourceJSON.PlzProvideResponseToAllQues+".<br>");
							if(errorFlagCheck==0){
								errorFlagCheck=1;
								$("#divErrorMsg_dynamicPortfolio").append("&#149; "+resourceJSON.PlzProvideResponseToAllQues+".<br>");
							}
							iErrorCount++;
							document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
							dSPQuestionsErrorCount=1;
							errorFlag=1;
						}

						isValidAnswer =dwr.util.getValue("QS"+optId+"validQuestion")
						if(dwr.util.getValue("QS"+i+"question").indexOf("Are you willing to accept a part time teaching position") >= 0 && document.getElementById("districtIdForDSPQ").value==614730){
							var nextQ = i+1;
							document.getElementById("QS"+nextQ+"isRequired").value=0;
							if(isValidAnswer==true){
								document.getElementById("QS"+nextQ+"isRequired").value=1;
							}
						}else if(dwr.util.getValue("QS"+i+"question").indexOf("Have you ever applied or worked for Kelly Services?") >= 0 && (document.getElementById("districtIdForDSPQ").value==614730 || document.getElementById("districtIdForDSPQ").value==100006)){
									var nextQ = i+1;
									document.getElementById("QS"+nextQ+"isRequired").value=0;
									if(isValidAnswer==true){
										document.getElementById("QS"+nextQ+"isRequired").value=1;
									}
								}
								else if(dwr.util.getValue("QS"+i+"question").indexOf("Have you ever applied for work in any public or private schools?") >= 0 && (document.getElementById("districtIdForDSPQ").value==614730 || document.getElementById("districtIdForDSPQ").value==100006)){
									var nextQ = i+1;
									document.getElementById("QS"+nextQ+"isRequired").value=0;
									if(isValidAnswer==true){
										document.getElementById("QS"+nextQ+"isRequired").value=1;
									}
							}else if(dwr.util.getValue("QS"+i+"question").indexOf("Have you ever been an intern or volunteer at any public or private schools?") >= 0 && (document.getElementById("districtIdForDSPQ").value==614730 || document.getElementById("districtIdForDSPQ").value==100006)){
									var nextQ = i+1;
									document.getElementById("QS"+nextQ+"isRequired").value=0;
									if(isValidAnswer==true){
										document.getElementById("QS"+nextQ+"isRequired").value=1;
									}
								}
						
						if(errorFlag==0){
							if(isRequired==1){
								isRequiredAns=1;
							}
							arr.push({ 
								"selectedOptions"  : optId,
								"question"  : dwr.util.getValue("QS"+i+"question"),
								"questionTypeMaster" : questionTypeMaster,
								"questionType" : questionTypeShortName,
								"questionOption" : dwr.util.getValue("qOptS"+optId),
								"districtSpecificPortfolioQuestions" : districtSpecificQuestion,
								"isValidAnswer" : dwr.util.getValue("QS"+optId+"validQuestion"),
								"jobOrder" : jobOrder,
							});
						}
			
					}else if(qType=='ml' || qType=='sl')
					{
						var insertedText = dwr.util.getValue("QS"+i+"opt");
						if(insertedText!=null && insertedText!="")
						{
							if(isRequired==1){
								isRequiredAns=1;
							}
							arr.push({ 
								"insertedText"    : insertedText,
								"question"  : dwr.util.getValue("QS"+i+"question"),
								"questionTypeMaster" : questionTypeMaster,
								"questionType" : questionTypeShortName,
								"districtSpecificPortfolioQuestions" : districtSpecificQuestion,
								"jobOrder" : jobOrder
							});
						}else
						{
							if(isRequired==1){
								$("#errordivspecificquestion").html("&#149; "+resourceJSON.PlzProvideResponseToAllQues+".<br>");
								if(errorFlagCheck==0){
									errorFlagCheck=1;
									$("#divErrorMsg_dynamicPortfolio").append("&#149; "+resourceJSON.PlzProvideResponseToAllQues+".<br>");
								}
								iErrorCount++;
								document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
								dSPQuestionsErrorCount=1;
							}
						}
					}else if(qType=='et' || qType=='sswc')
					{
						var optId="";
						var errorFlag=1;
						if($("input[name=QS"+i+"opt]:radio:checked").length > 0 )
						{
							errorFlag=0;
							optId=$("input[name=QS"+i+"opt]:radio:checked").val();
						}else
						{
							if(isRequired==1){
								errorFlag=1;
								$("#errordivspecificquestion").html("&#149; "+resourceJSON.PlzProvideResponseToAllQues+".<br>");
								dSPQuestionsErrorCount=1;
							}
						}
						if($("input[name=QS"+i+"opt]:radio").length==0)
							errorFlag=0;
						
						var insertedText = "";
						var isValidAnswer = dwr.util.getValue("QS"+optId+"validQuestion");
						
						if(dwr.util.getValue("QS"+i+"question")==resourceJSON.msgpreviouslyworkedforUNO){					
							if(isValidAnswer==true && (dwr.util.getValue("QS"+i+"optet1").trim()=="" || dwr.util.getValue("QS"+i+"optet2").trim()=="")){
								if(isRequired==1){
									$("#errordivspecificquestion").html("&#149; "+resourceJSON.PlzProvideResponseToAllQues+".<br>");
									if(errorFlagCheck==0){
										errorFlagCheck=1;
										$("#divErrorMsg_dynamicPortfolio").append("&#149; "+resourceJSON.PlzProvideResponseToAllQues+".<br>");
									}
									iErrorCount++;
									document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
									dSPQuestionsErrorCount=1;
								}
							}
						}else{
							var insertedText=dwr.util.getValue("QS"+i+"optet");
							if(isValidAnswer==true && insertedText.trim()==""){
								if(isRequired==1){
									$("#errordivspecificquestion").html("&#149; "+resourceJSON.PlzProvideResponseToAllQues+".<br>");
									if(errorFlagCheck==0){
										errorFlagCheck=1;
										$("#divErrorMsg_dynamicPortfolio").append("&#149; "+resourceJSON.PlzProvideResponseToAllQues+".<br>");
									}
									iErrorCount++;
									document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
									dSPQuestionsErrorCount=1;
								}
							}
						}
						if(errorFlag==0){
							if(isRequired==1){
								isRequiredAns=1;
							}
							arr.push({ 
								"selectedOptions"  : optId,
								"question"  : dwr.util.getValue("QS"+i+"question"),
								"insertedText"    : insertedText,
								"questionTypeMaster" : questionTypeMaster,
								"questionType" : questionTypeShortName,
								"questionOption" : dwr.util.getValue("qOptS"+optId),
								"districtSpecificPortfolioQuestions" : districtSpecificQuestion,
								"isValidAnswer" : isValidAnswer,
								"jobOrder" : jobOrder
							});
						}
					}else if(qType=='mlsel'){
						try{
							 var multiSelectArray="";
							 var inputs = document.getElementsByName("multiSelect"+i); 
							 for (var j = 0; j < inputs.length; j++) {
							        if (inputs[j].type === 'checkbox') {
							        	if(inputs[j].checked){
							        		multiSelectArray+=	inputs[j].value+"|";
							            }
							        }
							} 
							
							if(multiSelectArray!=""){
								if(isRequired==1){
									isRequiredAns=1;
								} 
								arr.push({ 
									"selectedOptions"  : multiSelectArray,
									"question"  : dwr.util.getValue("QS"+i+"question"),
									"insertedText"    : insertedText,
									"questionTypeMaster" : questionTypeMaster,
									"questionType" : questionTypeShortName,
									//"questionOption" : dwr.util.getValue("qOptS"+optId),
									"districtSpecificPortfolioQuestions" : districtSpecificQuestion,
									"isValidAnswer" : isValidAnswer,
									"jobOrder" : jobOrder
								});
							}else{
								if(isRequired==1){
									$("#errordivspecificquestion").html("&#149; "+resourceJSON.PlzProvideResponseToAllQues+".<br>");
									if(errorFlagCheck==0){
										errorFlagCheck=1;
										$("#divErrorMsg_dynamicPortfolio").append("&#149; "+resourceJSON.PlzProvideResponseToAllQues+".<br>");
									}
									iErrorCount++;
									document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
									dSPQuestionsErrorCount=1;
								}
							}
						}catch(err){}
					}else if(qType=='mloet'){
						try{
							 var multiSelectArray="";
							 var inputs = document.getElementsByName("multiSelect"+i); 
							 for (var j = 0; j < inputs.length; j++) {
							        if (inputs[j].type === 'checkbox') {
							        	if(inputs[j].checked){
							        		multiSelectArray+=	inputs[j].value+"|";
							            }
							        }
							} 
							var insertedText = dwr.util.getValue("QS"+i+"optmloet"); 
							if(multiSelectArray!=""){
								if(isRequired==1){
									isRequiredAns=1;
								} 
								arr.push({ 
									"selectedOptions"  : multiSelectArray,
									"question"  : dwr.util.getValue("QS"+i+"question"),
									"insertedText"    : insertedText,
									"questionTypeMaster" : questionTypeMaster,
									"questionType" : questionTypeShortName,
									//"questionOption" : dwr.util.getValue("qOptS"+optId),
									"districtSpecificPortfolioQuestions" : districtSpecificQuestion,
									"isValidAnswer" : isValidAnswer,
									"jobOrder" : jobOrder
								});
							}else{
								if(isRequired==1){
									if(errorFlagCheck==0){
										errorFlagCheck=1;
										$("#divErrorMsg_dynamicPortfolio").append("&#149; "+resourceJSON.PlzProvideResponseToAllQues+".<br>");
									}
									dSPQuestionsErrorCount=1;
								}
							}
						}catch(err){}
					}else if(qType=='rt'){
						var optId="";
						var score="";
						var rank="";
						var scoreRank=0;
						var opts = document.getElementsByName("optS");
						var scores = document.getElementsByName("scoreS");
						var ranks = document.getElementsByName("rankS");
						var o_ranks = document.getElementsByName("o_rankS");
						
						var tt=0;
						var uniqueflag=false;
						for(var i = 0; i < opts.length; i++) {
							optId += opts[i].value+"|";
							score += scores[i].value+"|";
							rank += ranks[i].value+"|";
							if(checkUniqueRankForPortfolio(ranks[i]))
							{
								uniqueflag=true;
								break;
							}
			
							if(ranks[i].value==o_ranks[i].value)
							{
								scoreRank+=parseInt(scores[i].value);
							}
							if(ranks[i].value=="")
							{
								tt++;
							}
						}
						if(uniqueflag)
							return;
			
						if(tt!=0)
							optId=""; 
			
						if(optId=="")
						{
							//totalSkippedQuestions++;
							//strike checking
						}
						var totalScore = scoreRank;
						if(isRequired==1){
							isRequiredAns=1;
						} 
						arr.push({ 
							"selectedOptions"  : optId,
							"question"  : dwr.util.getValue("QS"+i+"question"),
							"insertedText"    : insertedText,
							"questionTypeMaster" : questionTypeMaster,
							"questionType" : questionTypeShortName,
							"questionOption" : dwr.util.getValue("qOptS"+optId),
							"districtSpecificPortfolioQuestions" : districtSpecificQuestion,
							"isValidAnswer" : isValidAnswer,
							"jobOrder" : jobOrder,
							"optionScore"      : score,
							"totalScore"       : totalScore,
							"insertedRanks"    : rank,
							"maxMarks" :o_maxMarks
						});
					}else if(qType=='OSONP'){
						//	alert("hello "+qType);
						try{
							 var multiSelectArray="";
							 var inputs = document.getElementsByClassName("OSONP"+i); 
							 for (var j = 0; j < inputs.length; j++) {
							        if (inputs[j].type === 'radio') {
							        	if(inputs[j].checked){
							        		multiSelectArray+=	inputs[j].value+"|";
							            }
							        }
							}
							
							var insertedText = dwr.util.getValue("QS"+i+"OSONP");
							var isValidAnswer = dwr.util.getValue("QS"+multiSelectArray+"validQuestion");							
							if(isValidAnswer==true && insertedText.trim()==""){
								if(isRequired==1){
									$("#errordivspecificquestion").html("&#149; "+resourceJSON.PlzProvideResponseToAllQues+".<br>");
									if(errorFlagCheck==0){
										errorFlagCheck=1;
										$("#divErrorMsg_dynamicPortfolio").append("&#149; "+resourceJSON.PlzProvideResponseToAllQues+".<br>");
									}
									iErrorCount++;
									document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
									dSPQuestionsErrorCount=1;
								}
							}
							if(multiSelectArray!="" || isRequired==0){
								if(isRequired==1){
									isRequiredAns=1;
								} 
								arr.push({ 
									"selectedOptions"  : multiSelectArray,
									"question"  : dwr.util.getValue("QS"+i+"question"),
									"insertedText"    : insertedText,
									"questionTypeMaster" : questionTypeMaster,
									"questionType" : questionTypeShortName,
									//"questionOption" : dwr.util.getValue("qOptS"+optId),
									"districtSpecificPortfolioQuestions" : districtSpecificQuestion,
									"isValidAnswer" : isValidAnswer,
									"jobOrder" : jobOrder
								});
							}else{
								if(isRequired==1){
									$("#errordivspecificquestion").html("&#149; "+resourceJSON.PlzProvideResponseToAllQues+".<br>");
									if(errorFlagCheck==0){
										errorFlagCheck=1;
										$("#divErrorMsg_dynamicPortfolio").append("&#149; "+resourceJSON.PlzProvideResponseToAllQues+".<br>");
									}
									iErrorCount++;
									document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
									dSPQuestionsErrorCount=1;
								}
							}
						}catch(err){}
					}else if(qType=='DD'){
						try{
							
							 var multiSelectArray=$("#dropdown"+i+" :selected").val() ;
							if(multiSelectArray!=""  || isRequired==0){
								if(isRequired==1){
									isRequiredAns=1;
								} 
								arr.push({ 
									"selectedOptions"  : multiSelectArray,
									"question"  : dwr.util.getValue("QS"+i+"question"),
									"questionTypeMaster" : questionTypeMaster,
									"questionType" : questionTypeShortName,
									//"questionOption" : dwr.util.getValue("qOptS"+optId),
									"districtSpecificPortfolioQuestions" : districtSpecificQuestion,
									"isValidAnswer" : isValidAnswer,
									"jobOrder" : jobOrder
								});
							}else{
								if(isRequired==1){
									$("#errordivspecificquestion").html("&#149; "+resourceJSON.msgProvideResToAllQ+"<br>");
									if(errorFlagCheck==0){
										errorFlagCheck=1;
										$("#divErrorMsg_dynamicPortfolio").append("&#149; "+resourceJSON.msgProvideResToAllQ+"<br>");
									}
									iErrorCount++;
									document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
									dSPQuestionsErrorCount=1;
								}
							}
						}catch(err){}
					}else if(qType=='sscb'){
						try{
							
							var isValidAnswer = false;
							var insertedText = dwr.util.getValue("QS"+i+"multiselectText");
							 var multiSelectArray="";
							 var inputs = document.getElementsByName("multiSelect"+i); 
							 for (var j = 0; j < inputs.length; j++) {
							        if (inputs[j].type === 'checkbox') {
							        	if(inputs[j].checked){
							        		multiSelectArray+=	inputs[j].value+"|";
							        		if(isValidAnswer==false)
							        			isValidAnswer = dwr.util.getValue("QS"+inputs[j].value+"validQuestion")
							            }
							        }
							} 
							 
							
							 if($(".school"+i).length>0){
									schoolMaster=$(".school"+i).val();
								}
							 var errorrFlag=0;
							 $("#errordivspecificquestionUpload").hide();
							 $("#errordivspecificquestionUpload").empty();
							 if(isValidAnswer==true && insertedText.trim()==""){
								 errorrFlag=1;
								 $("#errordivspecificquestion").html("&#149; "+resourceJSON.PlzProvideResponseToAllQues+".<br>");
									if(errorFlagCheck==0){
										errorFlagCheck=1;
										$("#divErrorMsg_dynamicPortfolio").append("&#149; "+resourceJSON.PlzProvideResponseToAllQues+".<br>");
									}
									iErrorCount++;
									document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
									dSPQuestionsErrorCount=1;
							}else{
								var sectCnt = $(".secHeading").length;
								var showsecerror=false;
									for ( var int = 0; int < sectCnt; int++) {			
										var cntSec= int+1;
										//alert($(".sectionCnt"+cntSec+":checked").length);
										var atLeastOneIsCheckedInsec = $(".sectionCnt"+cntSec+":checked").length;
										if(atLeastOneIsCheckedInsec==0){
											showsecerror=true;
										}
									}
									if(showsecerror){
										$("#errordivspecificquestionUpload").append("&#149; "+resourceJSON.msgeachsectioninQuestion+" "+i+"<br>");
										$("#errordivspecificquestionUpload").show();
										$("#divErrorMsg_dynamicPortfolio").append("&#149; "+resourceJSON.msgeachsectioninQuestion+""+i+"<br>");
										iErrorCount++;
										document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
										dSPQuestionsErrorCount=1;
									}
								}
							if(errorrFlag==0){ 
							if(multiSelectArray!=""){
								if(isRequired==1){
									isRequiredAns=1;
								} 
								arr.push({ 
									"selectedOptions"  : multiSelectArray,
									"question"  : dwr.util.getValue("QS"+i+"question"),
									"insertedText"    : insertedText,
									"questionTypeMaster" : questionTypeMaster,
									"questionType" : questionTypeShortName,
									"schoolIdTemp" : schoolMaster,
									"districtSpecificPortfolioQuestions" : districtSpecificQuestion,
									"isValidAnswer" : isValidAnswer,
									"jobOrder" : jobOrder
								});
							}else{
								if(isRequired==1){
									$("#errordivspecificquestion").html("&#149; "+resourceJSON.PlzProvideResponseToAllQues+".<br>");
									if(errorFlagCheck==0){
										errorFlagCheck=1;
										$("#divErrorMsg_dynamicPortfolio").append("&#149; "+resourceJSON.PlzProvideResponseToAllQues+".<br>");
									}
									iErrorCount++;
									document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
									dSPQuestionsErrorCount=1;
								}
							}
						}
						}catch(err){}
					}
					if(qType=='UAT' || qType=='UATEF'){
						
						try{
							var errorFlag=0;
							
							var cntErr=0;
							var fileNameQuestion = dwr.util.getValue("QS"+i+"File").value;
							var ext = fileNameQuestion.substr(fileNameQuestion.lastIndexOf('.') + 1).toLowerCase();

							if(isRequired==1 && fileNameQuestion=="")
							{
								$('#errordivspecificquestionUpload').html("&#149; "+resourceJSON.msgPleaseupload+dwr.util.getValue("QS"+i+"question")+".</BR>");
								errorFlag=1;
								cntErr++;
							}
							if(ext!="")
							{
								if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt'))
								{
									$('#errordivspecificquestionUpload').html("&#149; "+resourceJSON.msgselectAcceptable+dwr.util.getValue("QS"+i+"question")+" "+resourceJSON.msgFormatsWhichIncludes+"</BR>");
									errorFlag=1;
									cntErr++;
								}
							}
							
							if(cntErr==0 && fileNameQuestion!=""){
								var newName="proof_of_highly_qualified_"+fileNameQuestion.substr(fileNameQuestion.lastIndexOf('\\') + 1).toLowerCase()						
								$('#answerFileName').val(newName);
								document.getElementById('frmAnswerUpload').submit();						
							}else{
								$("#errordivspecificquestionUpload").show();
							}
							
							var isValidAnswer = false;
							var insertedText = dwr.util.getValue("QS"+i+"Text");
							var file = dwr.util.getValue("answerFileName");
							var multiSelectArray=$("#dropdown"+i+" :selected").val() ;

							if(cntErr==0 && errorFlag==0){
								if(multiSelectArray!=""){
									if(isRequired==1){
										isRequiredAns=1;
									} 
									if(fileNameQuestion==""){
										file=dwr.util.getValue("editCaseFile");
									}

									arr.push({ 
										"selectedOptions"  : multiSelectArray,
										"question"  : dwr.util.getValue("QS"+i+"question"),
										"insertedText"    : insertedText,
										"fileName"    : file,
										"questionTypeMaster" : questionTypeMaster,
										"questionType" : questionTypeShortName,
										//"schoolIdTemp" : schoolMaster,
										"districtSpecificPortfolioQuestions" : districtSpecificQuestion,
										"isValidAnswer" : isValidAnswer,
										"jobOrder" : jobOrder
									});
								}else{
									if(isRequired==1){
										$("#errordivspecificquestion").html("&#149; "+resourceJSON.PlzProvideResponseToAllQues+".<br>");
										dSPQuestionsErrorCount=1;
									}
								}
							}
						}catch(err){}
					}
					if(isRequiredAns==1){
						ansRequiredCount++;
					}
				}
				//alert('isRequiredCount:::'+isRequiredCount);
				//alert('ansRequiredCount:::'+ansRequiredCount);
				if(isRequiredCount==ansRequiredCount) 
				{}else
				{
					$("#errordivspecificquestion").html("&#149; "+resourceJSON.PlzProvideResponseToAllQues+".<br>");
					if(errorFlagCheck==0){
						errorFlagCheck=1;
						$("#divErrorMsg_dynamicPortfolio").append("&#149; "+resourceJSON.PlzProvideResponseToAllQues+".<br>");
					}
					iErrorCount++;
					dSPQuestionsErrorCount=1;
					document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
				}
			}
			/////////////////////////////////
			
			// ********* contactNumber ******************
			refrence_contact_no=portfolio[10];
			if(refrence_contact_no==1 && countConfig_Reference>0)
			{
				$('#divErrorMsg_dynamicPortfolio').append("&#149; "+resourceJSON.msgRefContactNo+"</BR>");
				iErrorCount++;
				
				document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
			}
			address_source=portfolio[11];
			if(address_config==true && address_source==0)
			{
				var addressLine1 = document.getElementById("addressLine1");
				var zipCode = document.getElementById("zipCode");
				var stateIdForDSPQ = "";//document.getElementById("stateIdForDSPQ");
				var cityIdForDSPQ = "";//document.getElementById("cityIdForDSPQ");
				var countryId = document.getElementById("countryId").value;
				
				
				try
				{
					if(countryId==223)
					{
						stateIdForDSPQ = document.getElementById("stateIdForDSPQ").value;
						cityIdForDSPQ = document.getElementById("cityIdForDSPQ").value;
					}
					else
					{
						if(document.getElementById("countryCheck").value==1)
						{
							stateIdForDSPQ = document.getElementById("stateIdForDSPQ").value;
							cityIdForDSPQ = document.getElementById("otherCity").value;
						}
						else
						{
							stateIdForDSPQ = document.getElementById("otherState").value;
							cityIdForDSPQ = document.getElementById("otherCity").value;
						}
					}
					/*if(document.getElementById("countryCheck").value==1)
					{
						stateIdForDSPQ = document.getElementById("stateIdForDSPQ").value;
						cityIdForDSPQ = document.getElementById("cityIdForDSPQ").value;
					}
					else if(document.getElementById("countryCheck").value==0)
					{
						//getStateByCountry("dspq");
						
						stateIdForDSPQ = document.getElementById("otherState").value;
						cityIdForDSPQ = document.getElementById("otherCity").value;
					}*/
				}catch(err){}
				
				var addFlg=false;
				if(document.getElementById("districtIdForDSPQ").value==4503810 || document.getElementById("districtIdForDSPQ").value==7800294){
				if ($("#addressLinePr")!= undefined && $("#addressLinePr") != null){
					if($("#addressLinePr").val()=="")
						addFlg=true;
				}
				}
				if(trim(addressLine1.value)=="")
					 addFlg=true;
				if(trim(zipCode.value)=="")
					addFlg=true;				
			
				/*if(trim(countryId.value)=="")
					addFlg=true;*/
				
				
				/*if(trim(countryId.value)!="")
				{
					if(trim(countryId.value)=="223")
					{*/
						if(trim(stateIdForDSPQ)=="")
							addFlg=true;
						if(trim(cityIdForDSPQ)=="")
							addFlg=true;
					/*}
					else
					{
						if(trim(otherState.value)=="")
							addFlg=true;
						if(trim(otherCity.value)=="")
							addFlg=true;
					}
				}
				else
				{
					addFlg=true;
				}*/					
					 if($("#addressOptional").val()=="false"){
							addFlg=0;
						}
				if(addFlg)
				{
					$('#divErrorMsg_dynamicPortfolio').append("&#149; "+resourceJSON.msgprovideAddress+"</BR>");
					iErrorCount++;
					
					document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
				}
			}
			
			/*if($("#ttlRecResidency").val()==0){
				$('#divErrorMsg_dynamicPortfolio').append("&#149; Please provide Residency.</BR>");
				iErrorCount++;
				document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
			}*/
			if( $("#affidavit_config").val()==1)
			{				
				$(".affidavitReq").show();
			}
			else
			{				
					$(".affidavitReq").hide();				
			}
			
			
			$(".teacherExpReq").show();
			exp_source=portfolio[12];
			if((document.getElementById("districtIdForDSPQ").value==7800049 || document.getElementById("districtIdForDSPQ").value==7800048 || document.getElementById("districtIdForDSPQ").value==7800050 || document.getElementById("districtIdForDSPQ").value==7800051 || document.getElementById("districtIdForDSPQ").value==7800053) && candidateType=="I" && $('#jobcategoryDsp').val().trim()=="Personnel enseignant"){
				exp_config=false;
				$(".teacherExpReq").hide();
			}else if(document.getElementById("districtIdForDSPQ").value==804800 && $("#txtCandidateType").val()==1){
				exp_config=false;
				$(".teacherExpReq").hide();
			}			
			else if(document.getElementById("districtIdForDSPQ").value==804800 && $('#jobcategoryDsp').val().trim()=="Substitute Teachers"){
					exp_config=false;
					$(".teacherExpReq").hide();
				}else if(document.getElementById("districtIdForDSPQ").value==1302010 && ($('#jobcategoryDsp').val()==resourceJSON.msgClassified || $('#jobcategoryDsp').val()==resourceJSON.msgSubstitutes)){
					exp_config=false;
					$(".teacherExpReq").hide();
				}
			if(data!=null && data.certfiedTeachingExpOptional==false){
				exp_config=false;
				$(".teacherExpReq").hide();
			}
			if(exp_config==true)
			{
				var expCertTeacherTraining = document.getElementById("expCertTeacherTraining").value;
				
				if(trim(expCertTeacherTraining)=="")
				{
					if(document.getElementById("districtIdForDSPQ").value==7800047)
						$('#divErrorMsg_dynamicPortfolio').append("&#149; Please enter years of K-12 classroom teaching experience.<br>");
					else
						$('#divErrorMsg_dynamicPortfolio').append("&#149; "+resourceJSON.msgPzProvideCertifiedExp+"</BR>");
					iErrorCount++;
					document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
				}
			}
			nbc_source=portfolio[13];
			if($("#nationalBoardOptional").val()=="false"){
				nbc_config=false;
			}
			if(nbc_config==true && nbc_source==0)
			{
				var nationalBoardCertYear = document.getElementById("nationalBoardCertYear").value;
				var nbc1 = document.getElementById("nbc1");
				if($("#headQuaterIdForDspq").val()==2 && ($("#dspqName").val()=="Option A" || $("#dspqName").val()=="Option C"))
				{
				}
				else if(nbc1.checked && trim(nationalBoardCertYear)=="")
				{
					$('#divErrorMsg_dynamicPortfolio').append("&#149; "+resourceJSON.msgPlzProvideNBICL+"</BR>");
					iErrorCount++;
					document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
				}
				
			}
			
			
			affidavit_source=portfolio[14];
			if(affidavit_config==true && affidavit_source==0)
			{
				var affidavit=document.getElementsByName("affidavit");
				var affflag;
				
				if(!affidavit[0].checked)
				{
					$('#divErrorMsg_dynamicPortfolio').append("&#149; "+resourceJSON.msgafidevitconfirm+"</BR>");
					iErrorCount++;
					document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
				}
			}
			
			//**************************** Teacher personal info ***********************************
			/*if(personalinfo_config==true && personalinfo_source==0)
			{
					$('#divErrorMsg_dynamicPortfolio').append("&#149; Personal Information </BR>");
					iErrorCount++;
					document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
			}*/
			
			personalinfo_source=portfolio[15];
			
			if(personalinfo_config==true && personalinfo_source==0)
			{
				var chkflag=false;
				
				var firstName = document.getElementById("firstName_pi").value;
				var lastName = document.getElementById("lastName_pi").value;
				var dob="";
				
				var dobMonth=$("#dobMonth").val();
				var dobDay=$("#dobDay").val();
				var dobYear=$("#dobYear").val();
				
				var idobYear = new String(parseInt(trim(dobYear)));
				var currentFullYear = new Date().getFullYear();
				currentFullYear=currentFullYear-1;
				
				if((dobYear=="") || ( dobYear==0 || idobYear=="NaN") || (idobYear > currentFullYear || idobYear < 1931))
				{
					document.getElementById("dob").value="";
				}
				else if(trim(dobMonth)> 0 && trim(dobDay) > 0 && trim(dobYear) > 0)
				{
					dob=trim(dobMonth)+"-"+trim(dobDay)+"-"+trim(dobYear);
					document.getElementById("dob").value=dob;
				}
				else
				{
					document.getElementById("dob").value="";
				}
				
				if(trim(firstName)=="")
					chkflag=true;
				
				if(trim(lastName)=="")
					chkflag=true;
				
				
				/*if(document.getElementById("districtIdForDSPQ").value==7800038)
				{
					if(trim(dob)=="")
						chkflag=false;
				}else if(document.getElementById("districtIdForDSPQ").value==4218990)
				{
					if(trim(dob)=="")
						chkflag=false;
				}else{
					if(trim(dob)=="")
						chkflag=true;
				}*/
				
				if(chkflag)
				{
					$('#divErrorMsg_dynamicPortfolio').append("&#149; "+resourceJSON.msgPlzProvidePI+"</BR>");
					iErrorCount++;
					document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
				}
			}
			
			
			ssn_source=portfolio[16];
			var ssn = document.getElementById("ssn_pi").value;
			var issn = new String(parseInt(ssn));
			if(data.ssnOptional==false){
				ssn_config=false;
			}
			if(ssn_config==true && ssn_source==0)
			{
				if(trim(ssn)==""){
					$('#divErrorMsg_dynamicPortfolio').append("&#149; "+resourceJSON.msgPlzProvideSSN+"</BR>");
					iErrorCount++;
					document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
				}
				else if(issn=="NaN")
				{
					$('#divErrorMsg_dynamicPortfolio').append("&#149; "+resourceJSON.msgPlzProvideSSN+"</BR>");
					iErrorCount++;
					document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
				}
				else 
				{
					if(document.getElementById("districtIdForDSPQ").value==1200390 || document.getElementById("districtIdForDSPQ").value==806810)
					if(ssn.length!=9)
					{
						$('#divErrorMsg_dynamicPortfolio').append("&#149; "+resourceJSON.msgPlzProvideSSN9digit+"</BR>");
						iErrorCount++;
						document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
					}
					
					if(document.getElementById("districtIdForDSPQ").value==3680340)
					if(ssn.length!=4)
					{
						$('#divErrorMsg_dynamicPortfolio').append("&#149; "+resourceJSON.msgPlzProvideSSN4digit+"</BR>");
						iErrorCount++;
						document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
					}
				}
					
			}
			else if(ssn_config==true)
			{
				if(trim(ssn)==""){
					$('#divErrorMsg_dynamicPortfolio').append("&#149; "+resourceJSON.msgPlzProvideSSN+"</BR>");
					iErrorCount++;
					document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
				}
				else if(issn=="NaN")
				{
					$('#divErrorMsg_dynamicPortfolio').append("&#149; "+resourceJSON.msgPlzProvideValidSSN+"</BR>");
					iErrorCount++;
					document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
				}
				else
				{
					if(document.getElementById("districtIdForDSPQ").value==1200390 || document.getElementById("districtIdForDSPQ").value==806810)
						if(ssn.length!=9)
						{
							$('#divErrorMsg_dynamicPortfolio').append("&#149; "+resourceJSON.msgPlzProvideSSN9digit+"</BR>");
							iErrorCount++;
							document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
						}

						if(document.getElementById("districtIdForDSPQ").value==3680340)
						if(ssn.length!=4)
						{
							$('#divErrorMsg_dynamicPortfolio').append("&#149; "+resourceJSON.msgPlzProvideSSN4digit+"</BR>");
							iErrorCount++;
							document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
						}
				}
				
			}
			
			
			veteranValue_source=portfolio[22];
			if(veteran_config==true && veteranValue_source==0)
			{
				var veteranValue="";
				if (document.getElementById('vt1').checked) {
					veteranValue = document.getElementById('vt1').value;
				}else if (document.getElementById('vt2').checked) {
					veteranValue = document.getElementById('vt2').value;
				}
				if(trim(veteranValue)==""){
					$('#divErrorMsg_dynamicPortfolio').append("&#149; "+resourceJSON.msgifNotVeteran+"</BR>");
					iErrorCount++;
					document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
				}
			}
			
			if(!$("#vt1").is(':checked')){
				$("#vetranOptionDiv").hide();
			}
			if(districtIdForDSPQ.value==1201470 && $("#vt1").is(':checked')){
				showdistrictspeciFicVeteran();
				if($("#veteranOptS:checked").length==0){
					$('#divErrorMsg_dynamicPortfolio').append("&#149; "+resourceJSON.msgPleaseVeteranPrefrence+"<br>");			
					iErrorCount++;
					document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;					
				}
			}
			ethnicOrigin_source=portfolio[23];
			if(ethnicOrigin_config==true && ethnicOrigin_source==0)
			{
				var ethnicOriginValue=-1;
				var elements = document.getElementsByName('ethnicOriginId');
				for (i=0;i<elements.length;i++) 
				{
				  if(elements[i].checked) 
				  {
					  ethnicOriginValue=elements[i].value;
				  }
				}
				if(document.getElementById("districtIdForDSPQ").value==7800038){}
				else if(document.getElementById("districtIdForDSPQ").value==4218990){}
				else if(document.getElementById("districtIdForDSPQ").value==3703120){}
				else if(document.getElementById("districtIdForDSPQ").value==3904380){}
				else if(document.getElementById("districtIdForDSPQ").value==1201470){}
				else if(document.getElementById("districtIdForDSPQ").value==7800040){}
				else if(document.getElementById("districtIdForDSPQ").value==614730){}
				else if(document.getElementById("districtIdForDSPQ").value==5304860){}
				else if(document.getElementById("districtIdForDSPQ").value==804800){}
				else if(document.getElementById("districtIdForDSPQ").value==1302010){}
				else if(document.getElementById("districtIdForDSPQ").value==3700690){}
				else if(document.getElementById("districtIdForDSPQ").value==3700112){}
				else if(document.getElementById("districtIdForDSPQ").value==3702040){}
				else if(document.getElementById("districtIdForDSPQ").value==3702640){}
				else if($("#headQuaterIdForDspq").val()==2){}
				else if(document.getElementById("districtIdForDSPQ").value==1200390){
				if(ethnicOriginValue==-1)
				{
					$('#divErrorMsg_dynamicPortfolio').append("&#149; "+resourceJSON.msgSelectEthnicOrigin+"</BR>");
					iErrorCount++;
					document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
				}
				}
			}
			
			
			ethnicity_source=portfolio[24];
			if(ethinicity_config==true && ethnicity_source==0)
			{
				var ethinicityValue=-1;
				var elements = document.getElementsByName('ethinicityId');
				for (i=0;i<elements.length;i++) 
				{
					if(elements[i].checked) 
					{
						ethinicityValue=elements[i].value;
					}
				}
				if(document.getElementById("districtIdForDSPQ").value==7800038){}
				else if(document.getElementById("districtIdForDSPQ").value==4218990){}
				else if(document.getElementById("districtIdForDSPQ").value==3703120){}
				else if(document.getElementById("districtIdForDSPQ").value==3904380){}
				else if(document.getElementById("districtIdForDSPQ").value==1201470){}
				else if(document.getElementById("districtIdForDSPQ").value==7800040){}
				else if(document.getElementById("districtIdForDSPQ").value==614730){}
				else if(document.getElementById("districtIdForDSPQ").value==5304860){}
				else if(document.getElementById("districtIdForDSPQ").value==804800){}
				else if(document.getElementById("districtIdForDSPQ").value==1302010){}
				else if(document.getElementById("districtIdForDSPQ").value==3700690){}
				else if(document.getElementById("districtIdForDSPQ").value==3700112){}
				else if(document.getElementById("districtIdForDSPQ").value==3702040){}
				else if($("#headQuaterIdForDspq").val()==2){}
				else if(document.getElementById("districtIdForDSPQ").value==1200390){
					if(ethinicityValue==-1)
					{
						$('#divErrorMsg_dynamicPortfolio').append("&#149; "+resourceJSON.msgSelectEthnicity1+"</BR>");
						iErrorCount++;//alert(iErrorCount+"  <36");
						document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
					}
				}
			}
			
			if($("#expectedSalarySection").val()==2 || document.getElementById("districtIdForDSPQ").value==7800040 || (document.getElementById("districtIdForDSPQ").value==3703120 && $('#jobcategoryDsp').val().trim().indexOf("Licensed")!=-1))
			{
				if(document.getElementById("expectedSalary").value=="")
				{
					$('#divErrorMsg_dynamicPortfolio').append("&#149; "+resourceJSON.msgExpctedSal+"</BR>");
					iErrorCount++;//alert(iErrorCount+"  <36");
					document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
				}
			}
			if(document.getElementById("districtIdForDSPQ").value==4503810)
			{
				if(document.getElementById("drivingLicNum").value=="")
				{
					$('#divErrorMsg_dynamicPortfolio').append("&#149; Please provide Drivers License Number</BR>");
					iErrorCount++;//alert(iErrorCount+"  <36");
					document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
				}

				if(document.getElementById("drivingLicState").value=="")
				{
					$('#divErrorMsg_dynamicPortfolio').append("&#149; Please provide Drivers License State</BR>");
					iErrorCount++;//alert(iErrorCount+"  <36");
					document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
				}
			}
			
			employment_source=portfolio[25];
			if(document.getElementById("districtIdForDSPQ").value==3702040 && $('#jobcategoryDsp').val()=="Bus Driver"){
				employment_config=false;
			}
			
			if($("#txtCandidateType").val()==1 && document.getElementById("districtIdForDSPQ").value==804800){
				employment_config=false;
			}
			if(document.getElementById("districtIdForDSPQ").value!=3680340)
			if(employment_config==true && employment_source==0)
			{
				$('#divErrorMsg_dynamicPortfolio').append("&#149; "+resourceJSON.msgPlsProvideEmpHis+"</BR>");
				iErrorCount++;
				document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
			}
			
			retireNo_source=portfolio[28];
			if(retireNo_config==true && retireNo_source==0)
			{
				if(document.getElementById("isretired").checked==true)
				{	
					if(document.getElementById("retireNo").value=="")
					{
						$('#divErrorMsg_dynamicPortfolio').append("&#149; "+resourceJSON.msgTeacherRETNO+"</BR>");
						iErrorCount++;
						document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
					}
					if(document.getElementById("stMForretire").value=="")
					{
						$('#divErrorMsg_dynamicPortfolio').append("&#149; "+resourceJSON.msgRetirefromState+"</BR>");
						iErrorCount++;
						document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
					}
					if(document.getElementById("retireddistrictId").value=="")
					{
						$('#divErrorMsg_dynamicPortfolio').append("&#149; "+resourceJSON.msgRetiredFromDistrict+"</BR>");
						iErrorCount++;
						document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
					}
				}
			}
			else if(retireNo_config==true)
			{
				if(document.getElementById("retireNo").value=="")
				{
					$('#divErrorMsg_dynamicPortfolio').append("&#149; "+resourceJSON.msgTeacherRETNO+"</BR>");
					iErrorCount++;
					document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
				}
				if(document.getElementById("stMForretire").value=="")
				{
					$('#divErrorMsg_dynamicPortfolio').append("&#149; "+resourceJSON.msgRetirefromState+"</BR>");
					iErrorCount++;
					document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
				}
				if(document.getElementById("retireddistrictId").value=="")
				{
					$('#divErrorMsg_dynamicPortfolio').append("&#149; "+resourceJSON.msgRetiredFromDistrict+"</BR>");
					iErrorCount++;
					document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
				}
			}
			
			race_source=portfolio[17];
			if(race_config==true && race_source==0)
			{
				var raceValue="";
				var elements = document.getElementsByName('raceId');
				
				for (i=0;i<elements.length;i++) 
				{
				  if(elements[i].checked) 
				  {
					  raceValue+=elements[i].value+",";
				  }
				}
				if(document.getElementById("districtIdForDSPQ").value==7800038){}
				else if(document.getElementById("districtIdForDSPQ").value==4218990){}
				else if(document.getElementById("districtIdForDSPQ").value==3703120){}
				else if(document.getElementById("districtIdForDSPQ").value==3904380){}
				else if(document.getElementById("districtIdForDSPQ").value==1201470){}
				else if(document.getElementById("districtIdForDSPQ").value==7800040){}
				else if(document.getElementById("districtIdForDSPQ").value==614730){}
				else if(document.getElementById("districtIdForDSPQ").value==5304860){}
				else if(document.getElementById("districtIdForDSPQ").value==804800){}
				else if(document.getElementById("districtIdForDSPQ").value==1302010){}
				else if(document.getElementById("districtIdForDSPQ").value==3700690){}
				else if(document.getElementById("districtIdForDSPQ").value==3700112){}
				else if(document.getElementById("districtIdForDSPQ").value==3702040){}
				else if(document.getElementById("districtIdForDSPQ").value==3702640){}
				else if((document.getElementById("districtIdForDSPQ").value==2633090) && ($('#jobcategoryDsp').val().trim()=="Professional Certified" || $('#jobcategoryDsp').val().trim()=="Drivers" || $('#jobcategoryDsp').val().trim()=="Clerical" || $('#jobcategoryDsp').val().trim()=="Other")){}
				else if($("#headQuaterIdForDspq").val()==2){}
				else{
					if(raceValue=="")
					{
						$('#divErrorMsg_dynamicPortfolio').append("&#149; "+resourceJSON.msgPleaseprovideRace+"</BR>");
						iErrorCount++;//alert(iErrorCount+"  <44");
						document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
					}
				}
			}
			else if(race_config==true)
			{
				var raceValue="";
				var elements = document.getElementsByName('raceId');
				
				for (i=0;i<elements.length;i++) 
				{
				  if(elements[i].checked) 
				  {
					  raceValue+=elements[i].value+",";
				  }
				}
				if(document.getElementById("districtIdForDSPQ").value==7800038){}
				else if(document.getElementById("districtIdForDSPQ").value==4218990){}
				else if(document.getElementById("districtIdForDSPQ").value==3703120){}
				else if(document.getElementById("districtIdForDSPQ").value==3904380){}
				else if(document.getElementById("districtIdForDSPQ").value==1201470){}
				else if(document.getElementById("districtIdForDSPQ").value==7800040){}
				else if(document.getElementById("districtIdForDSPQ").value==614730){}
				else if(document.getElementById("districtIdForDSPQ").value==5304860){}
				else if(document.getElementById("districtIdForDSPQ").value==804800){}
				else if(document.getElementById("districtIdForDSPQ").value==1302010){}
				else if(document.getElementById("districtIdForDSPQ").value==3700690){}
				else if(document.getElementById("districtIdForDSPQ").value==3700112){}
				else if(document.getElementById("districtIdForDSPQ").value==3702040){}
				else if(document.getElementById("districtIdForDSPQ").value==3702640){}
				else if((document.getElementById("districtIdForDSPQ").value==2633090) && ($('#jobcategoryDsp').val().trim()=="Professional Certified" || $('#jobcategoryDsp').val().trim()=="Drivers" || $('#jobcategoryDsp').val().trim()=="Clerical" || $('#jobcategoryDsp').val().trim()=="Other")){}
				else if($("#headQuaterIdForDspq").val()==2){}
				else{
					if(raceValue=="")
					{
						$('#divErrorMsg_dynamicPortfolio').append("&#149; "+resourceJSON.msgPleaseprovideRace+"</BR>");
						iErrorCount++;//alert(iErrorCount+"  <44");
						document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
					}
				}
			}
			
			
			// Gender
			var miami = document.getElementById("isMiami").value;
			gender_source=portfolio[27];
			if(document.getElementById("districtIdForDSPQ").value==614730){}
			else if(document.getElementById("districtIdForDSPQ").value==5304860){}
			else if(document.getElementById("districtIdForDSPQ").value==1302010){}
			else if((document.getElementById("districtIdForDSPQ").value==2633090) && ($('#jobcategoryDsp').val().trim()=="Professional Certified" || $('#jobcategoryDsp').val().trim()=="Drivers" || $('#jobcategoryDsp').val().trim()=="Clerical" || $('#jobcategoryDsp').val().trim()=="Other")){}
			else if($("#headQuaterIdForDspq").val()==2){}
			else if(gender_config==true && gender_source==0)
			{
				var genderValue=-1;
				//var miami = document.getElementById("isMiami").value;

				var genderElements = document.getElementsByName('genderId');
				
				for (i=0;i<genderElements.length;i++) 
				{
					if(genderElements[i].value!=0)
					{
						/*if(miami=='true')
						{
							if(genderElements[i].value==1 || genderElements[i].value==2)
								if(genderElements[i].checked) 
									  genderValue=genderElements[i].value;
						}
						else 
						{*/
							if(genderElements[i].checked) 
								  genderValue=genderElements[i].value;
						/*}*/
					}
				}
				
				if(genderValue==-1)
				{
					$('#divErrorMsg_dynamicPortfolio').append("&#149; "+resourceJSON.msgprovideGender+"</BR>");
					iErrorCount++;
					document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
				}
			}
			/*else if(gender_config==true && gender_source==1 && miami=='true')
			{
				
				var genderValue=-1;
				var genderElements = document.getElementsByName('genderId');
				for (i=0;i<genderElements.length;i++) 
				{
					if(genderElements[i].value!=0)
					{
						if(miami=='true')
						{
							if(genderElements[i].value==1 || genderElements[i].value==2)
							{
								if(genderElements[i].checked) 
									  genderValue=genderElements[i].value;
							}
						}
					}
					
				}
				
				if(genderValue==-1)
				{
					$('#divErrorMsg_dynamicPortfolio').append("&#149; "+resourceJSON.msgprovideGender+"</BR>");
					iErrorCount++;
					document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
				}
			}*/
			
			formeremployee_source=portfolio[18];
			if(formeremployee_config==true && formeremployee_source==1)
			{
				if($('#fe2').is(':checked') || $('#fe1').is(':checked') || $('#fe3').is(':checked') || $('#fe5').is(':checked'))
				{
					
				}
				else
				{
					$('#divErrorMsg_dynamicPortfolio').append("&#149; "+resourceJSON.msgCurrentEmployment1+"</BR>");
					iErrorCount++;
					document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
				}
				
				if($('#fe2').is(':checked'))
				{
					var currentEmployeeNo=$("#empfe2").val();
					if(currentEmployeeNo==null || trim(currentEmployeeNo)=='')
					{
						$('#divErrorMsg_dynamicPortfolio').append("&#149; "+resourceJSON.msgCurrentEmployment1+"</BR>");
						iErrorCount++;
						document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
						
					}else if(document.getElementById("districtIdForDSPQ").value==1200390 && (currentEmployeeNo!=null || trim(currentEmployeeNo)!='') && isNaN(currentEmployeeNo)){
						$('#divErrorMsg_dynamicPortfolio').append("&#149; Employee Number is only a numerical value. Please enter the correct information, or contact MDCPS for your employee number</BR>");
						iErrorCount++;
						document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
					}else if(document.getElementById("districtIdForDSPQ").value!=4218990)
					{
						$('#empfe2').css("background-color","");
						if(!$('input[name=rdCEmp]').is(":checked"))
						{
							$('#divErrorMsg_dynamicPortfolio').append("&#149; "+resourceJSON.msgStaffMemtype+"</BR>");
							iErrorCount++;
							document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
							iErrorCount++;
						}
					}
				}
				
			}
			if($("#districtIdForDSPQ").val()==3904374 && $("#jobCategoryName").val()=="Substitute Teachers")
			{
				if($("#hrefBackgroundCheck").html()=="")
				{
					if($("#BGcheck").is(':checked') && $("#backgroundCheck").val()=="")
					{
						$('#divErrorMsg_dynamicPortfolio').append("&#149; Please upload Background Check document</BR>");
						iErrorCount++;
					}
				}
				else
					$("#BGcheck").prop('checked', true);
			}
			
			var displayGKAndSubject=document.getElementById("displayGKAndSubject").value;
			generalKnowledge_source=portfolio[19];
			
			if(data.generalKnowledgeExamOptional==false){
				generalError=0;
				generalKnowledge_config=0;
				generalKnowledgeExamOptional=false;
			}
			if(isItvtForMiami==false)
			{
				if((generalKnowledge_config==1 && displayGKAndSubject=='true' && isMiamiChk=="true" && !IsSIForMiami) || (generalKnowledge_config==1 && isMiamiChk=="true" && IsSIForMiami && displayPassFailGK1=="false") || (generalKnowledge_config==1 && displayGKAndSubject=='true' && document.getElementById("districtIdForDSPQ").value=="614730")) // && generalKnowledge_source==0
				{
					var generalError=0;
					var generalKnowledgeExamStatus = document.getElementById("generalKnowledgeExamStatus").value;
					if(trim(generalKnowledgeExamStatus)=="0"){
						generalError=1;
					}
					var generalKnowledgeExamDate = document.getElementById("generalKnowledgeExamDate").value;
					if(trim(generalKnowledgeExamDate)==""){
						generalError=1;
					}
					var generalKnowledgeScoreReport = document.getElementById("generalKnowledgeScoreReport").value;
					var generalKnowledgeScoreReportHidden = document.getElementById("generalKnowledgeScoreReportHidden").value;
					
					if(trim(generalKnowledgeScoreReport)=="" && trim(generalKnowledgeScoreReportHidden)==""){
						generalError=1;
					}else{
						if(trim(generalKnowledgeScoreReport)!=""){
							var ext = generalKnowledgeScoreReport.substr(generalKnowledgeScoreReport.lastIndexOf('.') + 1).toLowerCase();	
							
							var fileSize = 0;
							if ($.browser.msie==true)
						 	{	
							    fileSize = 0;	   
							}
							else
							{
								if(document.getElementById("generalKnowledgeScoreReport").files[0]!=undefined)
								{
									fileSize = document.getElementById("generalKnowledgeScoreReport").files[0].size;
								}
							}
							
							if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt'))
							{
								generalError=1;
							}else if(fileSize>=10485760){
								generalError=1;
							}
						}
					}
					if(generalError==1 && displayGKAndSubject=='true' && document.getElementById("districtIdForDSPQ").value=="1302010"){
						$('#divErrorMsg_dynamicPortfolio').append("&#149; "+resourceJSON.msgGeneralKnowledge+"</BR>");
						iErrorCount++;
						document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
					}else if(generalError==1 && displayGKAndSubject=='true' && document.getElementById("districtIdForDSPQ").value=="614730"){
						$('#divErrorMsg_dynamicPortfolio').append("&#149; Please provide details of CBEST Exam</BR>");
						iErrorCount++;
						document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
					}
					if(generalError==1 && displayGKAndSubject=='true' && isMiamiChk=="true"){
						$('#divErrorMsg_dynamicPortfolio').append("&#149; "+resourceJSON.msgGeneralKnowledge+"</BR>");
						iErrorCount++;
						document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
					}
					else if(generalError==1 && isMiamiChk=="true" && IsSIForMiami ){
						
						if(displayPassFailGK1=="false" && countSource_Certification!=0)
						{
							$('#divErrorMsg_dynamicPortfolio').append("&#149; "+resourceJSON.msgGeneralKnowledge+"</BR>");
							iErrorCount++;
							document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
						}
					}
				}
				
				
				subjectAreaExam_source=portfolio[20];
				if(subjectAreaExam_source==0 && isMiamiChk=="true" && !IsSIForMiami)
				{
					if(displayGKAndSubject=='true' && isMiamiChk=="true"){
						$('#divErrorMsg_dynamicPortfolio').append("&#149; "+resourceJSON.msgSubjectAreaExam+"</BR>");
						iErrorCount++;
						document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
					}
				}
				
				if(subjectAreaExam_source==0 && document.getElementById("districtIdForDSPQ").value==614730){
					if(displayGKAndSubject=='true' && $("#subjectAreaDiv").is(':visible')){
						$('#divErrorMsg_dynamicPortfolio').append("&#149; Please provide details of CSET Exam.</BR>");
						iErrorCount++;
						document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
					}
				}
			}
			
			
			
			
			
			additionalDocuments_source=portfolio[21];
			
			if(additionalDocuments_config==true && additionalDocuments_source==0 && document.getElementById("districtIdForDSPQ").value==7800031 && $("#jobcategoryDsp").val().trim()=="Teacher")
			{
				var documentError=0;
				var documentName = document.getElementById("documentName").value;
				if(trim(documentName)==""){
					documentError=1;
				}
				var uploadedDocument = document.getElementById("uploadedDocument").value;
				var uploadedDocumentHidden = document.getElementById("uploadedDocumentHidden").value;
				if(trim(uploadedDocument)==""   && trim(uploadedDocumentHidden)==""){
					documentError=1;
				}else{
					if(trim(uploadedDocument)!=""){
						var ext = uploadedDocument.substr(uploadedDocument.lastIndexOf('.') + 1).toLowerCase();	
						
						var fileSize = 0;
						if ($.browser.msie==true)
					 	{	
						    fileSize = 0;	   
						}
						else
						{
							if(document.getElementById("uploadedDocument").files[0]!=undefined)
							{
								fileSize = document.getElementById("uploadedDocument").files[0].size;
							}
						}
						
						if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt'))
						{
							documentError=1;
						}else if(fileSize>=10485760){
							documentError=1;
						}
					}
				}
				if(documentError==1){
					$('#divErrorMsg_dynamicPortfolio').append("&#149; "+resourceJSON.msgdetailsofAdditionalDocuments+"</BR>");
					iErrorCount++;//alert(iErrorCount+"  <56");
					document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
				}
					
			}
			/*additionalDocuments_source=portfolio[21];
			if(additionalDocuments_config==true && additionalDocuments_source==0)
			{
				var documentError=0;
				var documentName = document.getElementById("documentName").value;
				if(trim(documentName)==""){
					documentError=1;
				}
				
				var uploadedDocument = document.getElementById("uploadedDocument").value;
				var uploadedDocumentHidden = document.getElementById("uploadedDocumentHidden").value;
				if(trim(uploadedDocument)==""   && trim(uploadedDocumentHidden)==""){
					documentError=1;
				}else{
					if(trim(uploadedDocument)!=""){
						var ext = uploadedDocument.substr(uploadedDocument.lastIndexOf('.') + 1).toLowerCase();	
						
						var fileSize = 0;
						if ($.browser.msie==true)
					 	{	
						    fileSize = 0;	   
						}
						else
						{
							if(document.getElementById("uploadedDocument").files[0]!=undefined)
							{
								fileSize = document.getElementById("uploadedDocument").files[0].size;
							}
						}
						
						if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt'))
						{
							documentError=1;
						}else if(fileSize>=10485760){
							documentError=1;
						}
					}
				}
				
				if(documentError==1){
					$('#divErrorMsg_dynamicPortfolio').append("&#149; Please provide details of Additional Documents.</BR>");
					iErrorCount++;
					document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
				}
			}*/
			if(document.getElementById("districtIdForDSPQ").value==4218990){				
				additionalDocuments_source=portfolio[21];					
					var nonteacherFlag=$("#isnontj").val();
					var isSchoolsupport=$("#isSchoolSupportPhiladelphia").val();
				if((nonteacherFlag=="" || nonteacherFlag!='true') && (isSchoolsupport=="" || isSchoolsupport!="1"))
				if(additionalDocuments_config==true && additionalDocuments_source==0)
				{
					var documentError=0;
					var documentName = document.getElementById("documentName").value;
					if(trim(documentName)==""){
						documentError=1;
					}
					var uploadedDocument = document.getElementById("uploadedDocument").value;
					var uploadedDocumentHidden = document.getElementById("uploadedDocumentHidden").value;
					if(trim(uploadedDocument)==""   && trim(uploadedDocumentHidden)==""){
						documentError=1;
					}else{
						if(trim(uploadedDocument)!=""){
							var ext = uploadedDocument.substr(uploadedDocument.lastIndexOf('.') + 1).toLowerCase();	
							
							var fileSize = 0;
							if ($.browser.msie==true)
						 	{	
							    fileSize = 0;	   
							}
							else
							{
								if(document.getElementById("uploadedDocument").files[0]!=undefined)
								{
									fileSize = document.getElementById("uploadedDocument").files[0].size;
								}
							}
							
							if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt'))
							{
								documentError=1;
							}else if(fileSize>=10485760){
								documentError=1;
							}
						}
					}
					if(documentError==1){
						$('#divErrorMsg_dynamicPortfolio').append("&#149;"+resourceJSON.msgdetailsofAdditionalDocuments+"</BR>");
						iErrorCount++;//alert(iErrorCount+"  <56");
						document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
					}
						
				}
		}
			
			//********** Validation******************
			if($("#residencyStreetAddress").is(':visible'))
			{
				var streetAddress		=	$("#residencyStreetAddress").val().trim();
				var residencyCity		=	$("#residencyCity").val().trim();
				var residencyState		=	$("#residencyState").val().trim();
				var residencyZip		=	$("#residencyZip").val().trim();
				var residencyCountry	=	$("#residencyCountry").val().trim();
				var residencyFromDate	=	$("#residencyFromDate").val().trim();
				var residencyToDate		=	$("#residencyToDate").val().trim();
				
				var cntdivErrorMsg_top_Resi=0;				
				$('#divErrorMsg_top_Residency').empty();
				
				if(streetAddress==""){
					$('#divErrorMsg_top_Residency').append("&#149; Please enter Street Address<br>");
					cntdivErrorMsg_top_Resi++;
				}	
				if(residencyCity==""){
					$('#divErrorMsg_top_Residency').append("&#149; Please enter City<br>");
					cntdivErrorMsg_top_Resi++;
				}	
				if(residencyState==""){
					$('#divErrorMsg_top_Residency').append("&#149; Please enter State<br>");
					cntdivErrorMsg_top_Resi++;
				}
				if(residencyZip==""){
					$('#divErrorMsg_top_Residency').append("&#149; Please enter Zip<br>");
					cntdivErrorMsg_top_Resi++;
				}	
				if(residencyCountry==""){
					$('#divErrorMsg_top_Residency').append("&#149; Please enter Country<br>");
					cntdivErrorMsg_top_Resi++;
				}
				if(residencyFromDate==""){
					$('#divErrorMsg_top_Residency').append("&#149; Please enter From Date<br>");
					cntdivErrorMsg_top_Resi++;
				}
				if(residencyToDate==""){
					$('#divErrorMsg_top_Residency').append("&#149; Please enter To Date<br>");
					cntdivErrorMsg_top_Resi++;
				}
				var startDate	=	new Date(trim(residencyFromDate));
				var endDate	=	new Date(trim(residencyToDate));

				if(startDate!="" && endDate && (startDate>endDate)){
					$('#divErrorMsg_top_Residency').append("&#149; Dates Residency from cannot be greater than Dates Residency to<br>");
					cntdivErrorMsg_top_Resi++;
				}
					
					if(cntdivErrorMsg_top_Resi!=0)		
					{
						$('#divErrorMsg_top_Residency_header').show();
						$('#divErrorMsg_top_Residency').show();
						iErrorCount++;
					}
					else
					{
						$('#divErrorMsg_top_Residency_header').hide();
						$('#divErrorMsg_top_Residency').hide();
					}
			}else
			{
				$('#divErrorMsg_top_Residency').empty();
				$('#divErrorMsg_top_Residency').hide();
				$('#divErrorMsg_top_Residency_header').hide();
			}
			if($("#degreeName").is(':visible'))
			{
				var academicId = document.getElementById("academicId");
				var degreeId = document.getElementById("degreeId");
				var degreeName = document.getElementById("degreeName");
				var universityId = document.getElementById("universityId");
				var universityName = document.getElementById("universityName");
				var fieldId = document.getElementById("fieldId");     
				var fieldName = document.getElementById("fieldName");     
				var attendedInYear = document.getElementById("attendedInYear"); 
				var pathOfTranscript = document.getElementById("pathOfTranscript");
				var leftInYear = document.getElementById("leftInYear");     
				var gpaFreshmanYear = document.getElementById("gpaFreshmanYear");     
				var gpaJuniorYear = document.getElementById("gpaJuniorYear");     
				var gpaSophomoreYear = document.getElementById("gpaSophomoreYear");     
				var gpaSeniorYear = document.getElementById("gpaSeniorYear");     
				var gpaCumulative = document.getElementById("gpaCumulative"); 
				var degreeType = document.getElementById("degreeType");
				
				var fileName = pathOfTranscript.value;
				
				var GEDFlag=false;
				if($('#degreeName').val()== resourceJSON.msgHighSchoolorGED){
					GEDFlag=true;
				}

				
				var ext = fileName.substr(fileName.lastIndexOf('.') + 1).toLowerCase();
				var fileSize=0;		
				if ($.browser.msie==true)
			 	{	
				    fileSize = 0;	   
				}
				else
				{		
					if(pathOfTranscript.files[0]!=undefined)
						fileSize = pathOfTranscript.files[0].size;
				}
				var cntdivErrorMsg_top_aca=0;
				
				$('#divErrorMsg_top_aca').empty();
				if(trim(degreeName.value)=="" && $("#degreeOptional").val()=="true" )
				{
					$('#divErrorMsg_top_aca').append("&#149;"+resourceJSON.magPleaseenterDegree+"<br>");
					cntdivErrorMsg_top_aca++;
					
				}
				if(trim(universityName.value)=="" && GEDFlag==false && $("#schoolOptional").val()=="true" )
				{
					$('#divErrorMsg_top_aca').append("&#149; "+resourceJSON.PlzEtrSchool+"<br>");
					cntdivErrorMsg_top_aca++;
				}
				if(resourceJSON.platformName=="NC" && trim(universityName.value)=="Other" && trim($("#otherUniversityName").val())=="" &&  GEDFlag==false && $("#schoolOptional").val()=="true")
				{
					$('#divErrorMsg_top_aca').append("&#149; "+resourceJSON.PlzEtrOtherSchool+"<br>");
					cntdivErrorMsg_top_aca++;
				}
				if(!(document.getElementById("districtIdForDSPQ").value==2633090 && ($('#jobcategoryDsp').val()=="Drivers" || $('#jobcategoryDsp').val()=="Other" || $('#jobcategoryDsp').val()=="Clerical")))
					if(trim(fieldName.value)=="" && GEDFlag==false && $("#fieldOfStudyOptional").val()=="true")
					{
						$('#divErrorMsg_top_aca').append("&#149; "+resourceJSON.msgFieldofStudy+"<br>");
						cntdivErrorMsg_top_aca++;
					}
				if($("#academicsDatesOptional").val()=="true")
					if((document.getElementById("districtIdForDSPQ").value!=804800) || ($("#headQuaterIdForDspq").val()==2 &&($("#dspqName").val()=="Option C" || $("#dspqName").val()=="Option D"))){
						if(degreeId.value=="46"){}
						else{		
					if(trim(attendedInYear.value)=="")
					{
						$('#divErrorMsg_top_aca').append("&#149; "+resourceJSON.msgdateattended+"<br>");
						cntdivErrorMsg_top_aca++;
					}
					
					if(trim(leftInYear.value)=="")
					{
						$('#divErrorMsg_top_aca').append("&#149; "+resourceJSON.msgdatetoattended+"<br>");
						cntdivErrorMsg_top_aca++;	
					}
						}
				}
				if((trim(attendedInYear.value)!="") && (trim(leftInYear.value)!="") &&(trim(attendedInYear.value) > trim(leftInYear.value)))
				{
					if(degreeId.value=="46"){}
					else{
					$('#divErrorMsg_top_aca').append("&#149; "+resourceJSON.msgDatesAttended+"<br>");
					cntdivErrorMsg_top_aca++;
					}
				}
					
				if(GEDFlag==false && $("#transcriptOptional").val()=="1"){
					if(trim(pathOfTranscript.value)=="")
					{
						var aca_file=0;
						try {
							aca_file=document.getElementById("aca_file").value;
						} catch (e) {
							// TODO: handle exception
						}
						
						if(aca_file=="0")
						{
							$('#divErrorMsg_top_aca').append("&#149;  "+resourceJSON.msguploadtrascript+"<br>");
							cntdivErrorMsg_top_aca++;
						}
					}
				}
				if(ext!="")
				if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt'))
				{
					$('#divErrorMsg_top_aca').append("&#149; "+resourceJSON.AcceptFileFormat+"<br>");
					cntdivErrorMsg_top_aca++;
				}	
				else if(fileSize>=10485760)
				{
					$('#divErrorMsg_top_aca').append("&#149; "+resourceJSON.msgfilesizelessthan+"<br>");
					cntdivErrorMsg_top_aca++;
				}
				if($("#divGPARow").is(':visible') || $("#divGPARowOther").is(':visible'))
				if(trim(degreeType.value)=="B")
				{	
					if(trim(gpaFreshmanYear.value)=="" || trim(gpaFreshmanYear.value)==".")
					{
						/*$('#errordiv').append("&#149; Please enter valid Freshman GPA<br>");
						if(focs==0)
							$('#gpaFreshmanYear').focus();
						
						$('#gpaFreshmanYear').css("background-color",txtBgColor);
						cnt++;focs++;*/
					}
					else if(parseFloat(trim(gpaFreshmanYear.value))>5.00)
					{
						$('#divErrorMsg_top_aca').append("&#149; "+resourceJSON.msgfreshmangpa+"<br>");
						cntdivErrorMsg_top_aca++;
					}
					
					if(trim(gpaSophomoreYear.value)=="" || trim(gpaSophomoreYear.value)==".")
					{
						/*$('#errordiv').append("&#149; Please enter valid Sophomore GPA<br>");
						if(focs==0)
							$('#gpaSophomoreYear').focus();
						
						$('#gpaSophomoreYear').css("background-color",txtBgColor);
						cnt++;focs++;*/
					}
					else if(parseFloat(trim(gpaSophomoreYear.value))>5.00)
					{
						$('#divErrorMsg_top_aca').append("&#149; "+resourceJSON.msgSophomoreGPA+"<br>");
						cntdivErrorMsg_top_aca++;
					}
					
					if(trim(gpaJuniorYear.value)=="" || trim(gpaJuniorYear.value)==".")
					{
						/*$('#errordiv').append("&#149; Please enter valid Junior GPA<br>");
						if(focs==0)
							$('#gpaJuniorYear').focus();
						
						$('#gpaJuniorYear').css("background-color",txtBgColor);
						cnt++;focs++;*/
					}
					else if(parseFloat(trim(gpaJuniorYear.value))>5.00)
					{
						$('#divErrorMsg_top_aca').append("&#149; "+resourceJSON.msgJuniorGPA+"<br>");
						cntdivErrorMsg_top_aca++;
					}
						
					if(trim(gpaSeniorYear.value)=="" || trim(gpaSeniorYear.value)==".")
					{
						/*$('#errordiv').append("&#149; Please enter valid Senior GPA<br>");
						if(focs==0)
							$('#gpaSeniorYear').focus();
						
						$('#gpaSeniorYear').css("background-color",txtBgColor);
						cnt++;focs++;*/
					}
					else if(parseFloat(trim(gpaSeniorYear.value))>5.00)
					{
						$('#divErrorMsg_top_aca').append("&#149; "+resourceJSON.msgSeniorGPA+"<br>");
						cntdivErrorMsg_top_aca++;
					}
					gpaCumulative = document.getElementById("gpaCumulative").value;
					if(document.getElementById("districtIdForDSPQ").value==4218990){
						var nonteacherFlag=$("#isnontj").val();
						if($('#isSchoolSupportPhiladelphia').val()!=""){}
						else if(nonteacherFlag=="" || nonteacherFlag!="true"){
						if(trim(gpaCumulative)=="" || trim(gpaCumulative)==".")
										{
											if(!document.getElementById('international').checked){
												$('#divErrorMsg_top_aca').append("&#149; "+resourceJSON.msgvalidCumulativeGPA+"<br>");
												cntdivErrorMsg_top_aca++;
											}
										}
										else if(parseFloat(trim(gpaCumulative))>5.00)
										{
											$('#divErrorMsg_top_aca').append("&#149; "+resourceJSON.msgCumulativeGPA+"<br>");
											cntdivErrorMsg_top_aca++;
										}
										
						}
					}else if($("#gpaOptional").val()=="false" || document.getElementById("districtIdForDSPQ").value==3904380 || document.getElementById("districtIdForDSPQ").value==3700112 || document.getElementById("districtIdForDSPQ").value==1201470 || document.getElementById("districtIdForDSPQ").value==7800040 || document.getElementById("districtIdForDSPQ").value==7800047
							 || document.getElementById("districtIdForDSPQ").value==804800 || document.getElementById("districtIdForDSPQ").value==3700690 || (document.getElementById("districtIdForDSPQ").value==3703120 && $('#jobcategoryDsp').val().trim().indexOf("Bus Driver") !=-1) || (document.getElementById("districtIdForDSPQ").value==3703120 && $('#jobcategoryDsp').val().trim().indexOf("Substitute") !=-1)){
						if(parseFloat(trim(gpaCumulative))>5.00)
						{
							$('#divErrorMsg_top_aca').append("&#149; "+resourceJSON.msgCumulativeGPA+"<br>");
							cntdivErrorMsg_top_aca++;
						}
					}
					else{
						if(trim(gpaCumulative)=="" || trim(gpaCumulative)==".")
										{
											if(!document.getElementById('international').checked){
												$('#divErrorMsg_top_aca').append("&#149; "+resourceJSON.msgvalidCumulativeGPA+"<br>");
												cntdivErrorMsg_top_aca++;
											}
										}
										else if(parseFloat(trim(gpaCumulative))>5.00)
										{
											$('#divErrorMsg_top_aca').append("&#149; "+resourceJSON.msgCumulativeGPA+"<br>");
											cntdivErrorMsg_top_aca++;
										}
										gpaCumulative = document.getElementById("gpaCumulative").value;
					}
				}
				else if(GEDFlag==false)
				{

					gpaCumulative = document.getElementById("gpaCumulative1").value;
					if(document.getElementById("districtIdForDSPQ").value==4218990){
						var nonteacherFlag=$("#isnontj").val();
						if($('#isSchoolSupportPhiladelphia').val()!=""){}
						else if(nonteacherFlag=="" || nonteacherFlag!="true"){
						
										if((trim(degreeName.value)!="") && (trim(gpaCumulative)=="" || trim(gpaCumulative)=="."))
										{
											if(!document.getElementById('international2').checked){
												$('#divErrorMsg_top_aca').append("&#149; "+resourceJSON.msgvalidCumulativeGPA+"<br>");
												cntdivErrorMsg_top_aca++;
											}
										}
										else if(trim(degreeName.value)!="" && parseFloat(trim(gpaCumulative))>5.00)
										{
											$('#divErrorMsg_top_aca').append("&#149; "+resourceJSON.msgCumulativeGPA+"<br>");
											cntdivErrorMsg_top_aca++;
										}
						}
					}else if($("#gpaOptional").val()=="false" || document.getElementById("districtIdForDSPQ").value==3904380 || document.getElementById("districtIdForDSPQ").value==3700112 || document.getElementById("districtIdForDSPQ").value==1201470 || document.getElementById("districtIdForDSPQ").value==7800040 || document.getElementById("districtIdForDSPQ").value==7800047
							 || document.getElementById("districtIdForDSPQ").value==804800 || document.getElementById("districtIdForDSPQ").value==3700690  || (document.getElementById("districtIdForDSPQ").value==3703120 && $('#jobcategoryDsp').val().trim().indexOf("Bus Driver") !=-1) || (document.getElementById("districtIdForDSPQ").value==3703120 && $('#jobcategoryDsp').val().trim().indexOf("Substitute") !=-1)){
						if(trim(degreeName.value)!="" && parseFloat(trim(gpaCumulative))>5.00)
						{
							$('#divErrorMsg_top_aca').append("&#149; Cumulative GPA must be less than or equal to 5.00<br>");
							cntdivErrorMsg_top_aca++;
						}
					}else{
										if((trim(degreeName.value)!="") && (trim(gpaCumulative)=="" || trim(gpaCumulative)=="."))
										{
											if(!document.getElementById('international2').checked){
												$('#divErrorMsg_top_aca').append("&#149; "+resourceJSON.msgvalidCumulativeGPA+"<br>");
												cntdivErrorMsg_top_aca++;
											}
										}
										else if(trim(degreeName.value)!="" && parseFloat(trim(gpaCumulative))>5.00)
										{
											$('#divErrorMsg_top_aca').append("&#149; "+resourceJSON.msgCumulativeGPA+"<br>");
											cntdivErrorMsg_top_aca++;
										}
					}
				
				}
				
				if(document.getElementById("districtIdForDSPQ").value!=806900 && document.getElementById("districtIdForDSPQ").value!=804800 && document.getElementById("districtIdForDSPQ").value!=7800031)
				{
					if(GEDFlag==true){
						if(document.getElementById("pathOfTranscript").value==""){
							$('#divErrorMsg_top_aca').append("&#149;  "+resourceJSON.msguploadtrascript+"<br>");
							cntdivErrorMsg_top_aca++;
						}
					}
				}
				
				
				if(cntdivErrorMsg_top_aca!=0)		
				{
					$('#divErrorMsg_top_aca_header').show();
					$('#divErrorMsg_top_aca').show();
					iErrorCount++;
				}
				else
				{
					$('#divErrorMsg_top_aca_header').hide();
					$('#divErrorMsg_top_aca').hide();
				}
			}
			else
			{
				$('#divErrorMsg_top_aca').empty();
				$('#divErrorMsg_top_aca').hide();
				$('#divErrorMsg_top_aca_header').hide();
			}
			
			// Cert Validation 
			
			if($("#stateMaster").is(':visible'))
			{
				var certId = document.getElementById("certId");
				
				var stateMaster = document.getElementById("stateMaster");
				var yearReceived = document.getElementById("yearReceived");
				var certType = document.getElementById("certType");
				var certUrl = trim(document.getElementById("certUrl").value);

				var certificationtypeMasterObj = {certificationTypeMasterId:dwr.util.getValue("certificationtypeMaster")};
				var certificationTypeMaster = document.getElementById("certificationtypeMaster").value;
				var yearExpires = document.getElementById("yearexpires").value;
				var doenumber = trim(document.getElementById("doenumber").value);
				var proofCertReq=$("#proofOfCertificationReq").val();			
				var pathOfCertificationFile = document.getElementById("pathOfCertificationFile");
				var licenseLetterOptional = document.getElementById("licenseLetterOptional");
				var divErrorMsg_top_certi=0;
				
				var fileName = pathOfCertificationFile.value;
				var ext = fileName.substr(fileName.lastIndexOf('.') + 1).toLowerCase();
				var fileSize=0;	
				
				if ($.browser.msie==true){	
				    fileSize = 0;	   
				}else{		
					if(pathOfCertificationFile.files[0]!=undefined)
					fileSize = pathOfCertificationFile.files[0].size;
				}
				
				if(doenumber=="")
				{
					doenumber=0;
				}
				
				$('#divErrorMsg_top_certi').empty();
				var certificationStatusMaster = document.getElementById("certificationStatusMaster").value;
				if(certificationStatusMaster=="")
				{
					$('#divErrorMsg_top_certi').append("&#149; "+resourceJSON.msgcertificate+"<br>");
					divErrorMsg_top_certi++;
				}
				
				var certificationtypeMasterObj = {certificationTypeMasterId:dwr.util.getValue("certificationtypeMaster")};
				if(trim(certificationtypeMasterObj.certificationTypeMasterId)==0)
				{
					$('#divErrorMsg_top_certi').append("&#149; "+resourceJSON.msgceritificationtype+"<br>");
					divErrorMsg_top_certi++;
				}
				
				if(trim(stateMaster.value)=="")
				{
					$('#divErrorMsg_top_certi').append("&#149; "+resourceJSON.msgState+"<br>");
					divErrorMsg_top_certi++;
				}
				
				if($("#certiDatesOptional").val()=="true")
				if(trim(yearReceived.value)=="")
				{
					$('#divErrorMsg_top_certi').append("&#149; "+resourceJSON.msgyearreciverd+"<br>");
					divErrorMsg_top_certi++;
				}

				if($("#headQuaterIdForDspq").val()==2 && ($('#dspqName').val()=="Option A" || $('#dspqName').val()=="Option C"))
				{
					if(trim(yearExpires)=="")
					{
						$('#divErrorMsg_top_certi').append("&#149; Please select Year Expires<br>");
						divErrorMsg_top_certi++;
					}
					if(trim(yearExpires)=="Does")
						yearExpires="";	
				}				
				
				if(document.getElementById("districtIdForDSPQ").value==7800040)
				{
					IEINNumber = trim(document.getElementById("IEINNumber").value);
					
					if(IEINNumber=="" && certificationStatusMaster!=5)
					{
						$('#divErrorMsg_top_certi').append("&#149; "+resourceJSON.msgPleaseIEINNumber+"<br>");
						divErrorMsg_top_certi++;
					}
				}
				
				if((document.getElementById("districtIdForDSPQ").value==2633090) && ($('#jobcategoryDsp').val()=="Professional Certified")||document.getElementById("districtIdForDSPQ").value==804800 || (window.location.hostname=="nccloud.teachermatch.org" && document.getElementById("districtIdForDSPQ").value==7800143) || 
						(window.location.hostname=="nc.teachermatch.org" && document.getElementById("districtIdForDSPQ").value==7800161))
				{				
					if(doenumber=="" && certificationStatusMaster!=5)
					{
						$('#divErrorMsg_top_certi').append("&#149; "+resourceJSON.msgDOENumber+"<br>");
						divErrorMsg_top_certi++;
					}
					
					var selectedGrades = $( ".gradeLvl input:checked" ).length;
					if(selectedGrades==0){
						$('#divErrorMsg_top_certi').append("&#149; "+resourceJSON.msgGradeLevel+"<br>");
						divErrorMsg_top_certi++;			
					}
				}
				
				if($('#headQuaterIdForDspq').val()=="2" && $('#certiGrades').val()=="2" &&(($('#dspqName').val()=="Option C")))
				{
					var selectedGrades = $( ".gradeLvl input:checked" ).length;
					if(selectedGrades==0 && $("#certificationStatusMaster").val()!="5"){
						$('#divErrorMsg_top_certi').append("&#149; Please select Grade Level(s)<br>");
						divErrorMsg_top_certi++;			
					}
				}
				else if($('#certiGrades').val()=="2"){
					var selectedGrades = $( ".gradeLvl input:checked" ).length;
					if(selectedGrades==0){
						$('#divErrorMsg_top_certi').append("&#149; Please select Grade Level(s)<br>");
						divErrorMsg_top_certi++;			
					}
				}
				if(fileName=="" && $("#licenseLetterOptional").val()=="true" && $("#certificationStatusMaster").val()!="5")
				{
					$('#divErrorMsg_top_certi').append("&#149; "+resourceJSON.licenseLetterOptional+"<br>");
					divErrorMsg_top_certi++;
				}
				if($("#headQuaterIdForDspq").val()==2 && $("#dspqName").val()=="Option A")
				{
				}
				else if(trim(certType.value)=="")
				{
					$('#divErrorMsg_top_certi').append("&#149; "+resourceJSON.msgentercertificatename+"<br>");
					divErrorMsg_top_certi++;
				}
				var certTypeObj = {certTypeId:dwr.util.getValue("certificateTypeMaster")};
				if(trim(certTypeObj.certTypeId)=="" && trim(certType.value)!="")
				{
					$('#divErrorMsg_top_certi').append("&#149; "+resourceJSON.msgnotexistselecstate+"<br>");
					divErrorMsg_top_certi++;	
				}
				
				if(document.getElementById("districtIdForDSPQ").value==4218990){}
				else{
					if(proofCertReq==1 &&(certificationStatusMaster==1 || certificationStatusMaster==2))
					{
						var pathOfCertification = document.getElementById("pathOfCertification").value;
						if(certUrl=="" && pathOfCertification=="" && pathOfCertificationFile.value=="")
						{
							$('#divErrorMsg_top_certi').append("&#149; "+resourceJSON.msgCertificationLicensureUrl+"<br>");
							divErrorMsg_top_certi++;
						}
					}
				}
				
				if(ext!=""){
					if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt'))
					{
						$('#divErrorMsg_top_certi').append("&#149; "+resourceJSON.AcceptFileFormat+"<br>");
						divErrorMsg_top_certi++;
					}	
					else if(fileSize>=10485760)
					{
						$('#divErrorMsg_top_certi').append("&#149; "+resourceJSON.FileSizeLessthan10Mb+"<br>");
						divErrorMsg_top_certi++;
					}
				}
				
				var certIdTemp=0;
				var stateIdTemp=0;
				var certTypeTemp=certType.value;
				if(certId.value!="")
					certIdTemp=certId.value;
				if(stateMaster.value!="")
					stateIdTemp=stateMaster.value;
				var certificateTypeMasterTemp = document.getElementById("certificateTypeMaster").value;
				PFCertifications.findDuplicateCertificateDSPQ(certIdTemp,stateIdTemp,certTypeTemp,certificateTypeMasterTemp,{
				async: false,
				errorHandler:handleError,
				callback:function(data)
				{
					if(data)
					{
						$('#divErrorMsg_top_certi').append("&#149; "+resourceJSON.msgCertificationLicensure+"<br>");
						divErrorMsg_top_certi++;
					}
				}
				});
				
				if(divErrorMsg_top_certi!=0)		
				{
					$('#divErrorMsg_top_certi_header').show();
					$('#divErrorMsg_top_certi').show();
					iErrorCount++;
				}
				else
				{
					$('#divErrorMsg_top_certi_header').hide();
					$('#divErrorMsg_top_certi').hide();
				}
			}
			else
			{
				$('#divErrorMsg_top_certi').empty();
				$('#divErrorMsg_top_certi').hide();
				$('#divErrorMsg_top_certi_header').hide();
			}
			
			var experiance=$("#norecord").val();
			if($("#role").is(':visible'))
			{
				var role = document.getElementById("role");
				var empOrg = document.getElementById("empOrg");
				var fieldId = document.getElementById("fieldId2");
				var currentlyWorking = document.getElementById("currentlyWorking");
				var roleStartMonth = document.getElementById("roleStartMonth");
				var roleStartYear = document.getElementById("roleStartYear");	
				var roleEndMonth = document.getElementById("roleEndMonth");
				var roleEndYear = document.getElementById("roleEndYear");
				var empRoleTypeId=document.getElementsByName("empRoleTypeId");
				var amount=document.getElementById("amount");
				var cityEmp = document.getElementById("cityEmp");
				var stateOfOrg = document.getElementById("stateOfOrg");
				var position = document.getElementById("empPosition");
				var cntdivErrorMsg_top_emphis=0;
				//place here by shriram
				$('#divErrorMsg_top_emphis').empty();
				
				
				
				
				var amountFlag=false;
				if(amount!=""){
					amountFlag=isNaN(amount.value);
				}
				
				var charCount=$('#primaryRespdiv').find(".jqte_editor").text().trim();
				var countPrimaryResp = charCount.length;
				
				charCount=$('#mostSignContdiv').find(".jqte_editor").text().trim();;
				var countmostSignCont = charCount.length;
				var reasonForLea = $('#reasonForLeadiv').find(".jqte_editor").text().trim();
				//commented by shriram
				//$('#divErrorMsg_top_emphis').empty();

				var amountValid=true;
				
				if(document.getElementById("districtIdForDSPQ").value==804800 && document.getElementById("districtIdForDSPQ").value==3703120 && document.getElementById("districtIdForDSPQ").value==3700690 && document.getElementById("districtIdForDSPQ").value==806810){	
					amountValid=false;
				}
				if($("#empSecSalaryOptional").val()=="false"){
					amountValid=false;
				}
				if(amountValid){	
					if(amount.value=="" && $("#amount").is(':visible')){
						$('#divErrorMsg_top_emphis').append("&#149; "+resourceJSON.msgEneterAmount+"<br>");
						cntdivErrorMsg_top_emphis++;
					}
				}
				if(amountFlag)
				{
					$('#divErrorMsg_top_emphis').append("&#149; "+resourceJSON.msgnumericamount+"<br>");
					cntdivErrorMsg_top_emphis++;
				}
				
				if($("#empHisPos").is(':visible') && trim(position.value)=="0" && $("#empPositionOptional").val()=="true" )
				{
					$('#divErrorMsg_top_emphis').append("&#149; "+resourceJSON.msgposition+"<br>");
					cntdivErrorMsg_top_emphis++;
				}
				if(trim(role.value)=="")
				{				
						$('#divErrorMsg_top_emphis').append("&#149; "+resourceJSON.msgTitleDesignationRole+"<br>");
						cntdivErrorMsg_top_emphis++;
				}
				
				//12 October 2015 Shriram
				//Distirct check for error message for UPPER ARLINGTON CITY  
				if(document.getElementById("districtIdForDSPQ").value==3904493 )
				{
					
					if(trim(empOrg.value)=="")
					{
						$('#divErrorMsg_top_emphis').append("&#149; "+resourceJSON.errmsgCompanyName+"<br>");
						
						cntdivErrorMsg_top_emphis++;
					}
					
					if(trim(cityEmp.value)=="")
					{
						$('#divErrorMsg_top_emphis').append("&#149; "+resourceJSON.errmsgCompanyCity+"<br>");
						
						cntdivErrorMsg_top_emphis++;
					}
					
					if(trim(stateOfOrg.value)=="")
					{
					
						$('#divErrorMsg_top_emphis').append("&#149; "+resourceJSON.errmsgCompanyState+"<br>");
						
						cntdivErrorMsg_top_emphis++;
					}
			
				}
				else
				{
					if(trim(empOrg.value)=="" && $("#empOrganizationOptional").val()=="true")
					{
						$('#divErrorMsg_top_emphis').append("&#149; "+resourceJSON.msgorganization+"<br>");
						
						cntdivErrorMsg_top_emphis++;
					}
					
					if(trim(cityEmp.value)=="" && $("#empCityOptional").val()=="true")
					{
						$('#divErrorMsg_top_emphis').append("&#149; "+resourceJSON.msgentercity+"<br>");
						
						cntdivErrorMsg_top_emphis++;
					}
					
					if(trim(stateOfOrg.value)=="" && $("#empStateOptional").val()=="true")
					{
						$('#divErrorMsg_top_emphis').append("&#149; "+resourceJSON.msgOrganisation+"<br>");
						
						cntdivErrorMsg_top_emphis++;
					}
					
				}
				//ENDED BY SHRIRAM///////////
				//shriram///
				if(document.getElementById("districtIdForDSPQ").value==3904493 )
				{
				var compTelephone = $("#compTelephone").val().trim();
				var compSupervisor = $("#compSupervisor").val().trim();
				var validTelephone=false;
				var validCompSupervisor=true;
				if(compTelephone!="")
					validTelephone=isNaN(compTelephone);
				
				
				////////////////////////////////////////
				//shriram/////////////////////////////////////Telephone
				$('#compTelephone').val( compTelephone.replace(/^\s\s*/, '').replace(/\s\s*$/, ''));
				if(compTelephone=="" )
				{ 
					$('#divErrorMsg_top_emphis').append("&#149; "+resourceJSON.msgCompanyTelephone+"<br>");
					cntdivErrorMsg_top_emphis++;
				}

			
			
			/*if(validTelephone)
			{	
				$('#divErrorMsg_top_emphis').append("&#149; "+resourceJSON.msgCompanyTelephone+"<br>");
				cntdivErrorMsg_top_emphis++;
			}
			*/
			////////////////////////Shriram Company Supervisor//////////
			$('#compSupervisor').val( compSupervisor.replace(/^\s\s*/, '').replace(/\s\s*$/, ''));
			if(compSupervisor=="" )
			{ 
				$('#divErrorMsg_top_emphis').append("&#149; "+resourceJSON.msgCompanySupervisor+"<br>");
				cntdivErrorMsg_top_emphis++;
			}
				}

				//shrirma ///	
				if($("#ndustry_FieldDspq").is(':visible') && trim(fieldId.value)=="")
				{
					$('#divErrorMsg_top_emphis').append("&#149; "+resourceJSON.msgPleaseselectField+"<br>");
					cntdivErrorMsg_top_emphis++;
				}
				if($("#empDatesOptional").val()=="true"){
					if(trim(roleStartMonth.value)=="0")
					{
						$('#divErrorMsg_top_emphis').append("&#149; "+resourceJSON.msgPleaseselectFromMonth+"<br>");
						cntdivErrorMsg_top_emphis++;
					}
					if(trim(roleStartYear.value)=="0")
					{
						$('#divErrorMsg_top_emphis').append("&#149; "+resourceJSON.msgPleaseselectFromYear+"<br>");
						cntdivErrorMsg_top_emphis++;
					}
				}
				if(currentlyWorking.checked==false)
				{
					if($("#empDatesOptional").val()=="true"){
						if(trim(roleEndMonth.value)=="0")
						{
							$('#divErrorMsg_top_emphis').append("&#149; "+resourceJSON.msgPleaseselectToMonth+"<br>");
							cntdivErrorMsg_top_emphis++;
						}
						if(trim(roleEndYear.value)=="0")
						{
							$('#divErrorMsg_top_emphis').append("&#149; "+resourceJSON.msgPleaseselectToYear+"<br>");
							cntdivErrorMsg_top_emphis++;
						}
					}

					if(trim(roleStartMonth.value)!="0" && trim(roleStartYear.value)!="0" && trim(roleEndMonth.value)!="0" && trim(roleEndYear.value)!="0")
					{
						if(trim(roleStartYear.value)>trim(roleEndYear.value))
						{
							$('#divErrorMsg_top_emphis').append("&#149; "+resourceJSON.msgFromgreaterthan+"<br>");
							cntdivErrorMsg_top_emphis++;
						}
						else if(parseInt(trim(roleStartMonth.value))>parseInt(trim(roleEndMonth.value)) && (trim(roleStartYear.value)==trim(roleEndYear.value)))
						{
							$('#divErrorMsg_top_emphis').append("&#149; "+resourceJSON.msgFromgreaterthan+"<br>");
							cntdivErrorMsg_top_emphis++;
						}
					}
				}
				var chkRole=true;
				for(i=0;i<empRoleTypeId.length;i++)
				{
					if(empRoleTypeId[i].checked==true)
					{
						chkRole=false;
						break;
					}
				}
				if(document.getElementById("districtIdForDSPQ").value==4218990 || document.getElementById("districtIdForDSPQ").value==3904380 || document.getElementById("districtIdForDSPQ").value==3700690 || document.getElementById("districtIdForDSPQ").value==3700112 || ($("#headQuaterIdForDspq").val()==2 && ($("#dspqName").val()=="Option A" || $("#dspqName").val()=="Option B"))){
					chkRole=false;
				}
				if(chkRole)
				{
					$('#divErrorMsg_top_emphis').append("&#149; "+resourceJSON.msgtypeRole+"<br>");
					cntdivErrorMsg_top_emphis++;
				}
				if($("#empSecRoleOptional").val()=="false"){
					chkRole=false;
				}
				if(document.getElementById("districtIdForDSPQ").value==4218990){
					var nonteacherFlag=$("#isnontj").val();
					if(nonteacherFlag=="" || nonteacherFlag!="true"){

						if(countPrimaryResp==0)
						{
							$('#divErrorMsg_top_emphis').append("&#149; "+resourceJSON.msgprimaryresponsibilities+"<br>");
							cntdivErrorMsg_top_emphis++;
						}
					
					}
				
				}
				
				if($("#empSecPrirOptional").val()=="2" || (document.getElementById("districtIdForDSPQ").value==3703120 || document.getElementById("districtIdForDSPQ").value==3700112) || ($("#headQuaterIdForDspq").val()==2 && ($("#dspqName").val()!="Option A" && $("#dspqName").val()!="Option B"))){			
					if(countPrimaryResp==0)
					{
						$('#divErrorMsg_top_emphis').append("&#149; Please provide primary responsibilities in this role<br>");
						cntdivErrorMsg_top_emphis++;
					}
				}
				if(countmostSignCont==0 && ($("#empSecMscrOptional").val()=="2" || document.getElementById("districtIdForDSPQ").value==3700112 || document.getElementById("districtIdForDSPQ").value==3702040 || document.getElementById("districtIdForDSPQ").value==3702640))
					{
						$('#divErrorMsg_top_emphis').append("&#149; Please provide most significant contributions in this role<br>");
						cntdivErrorMsg_top_emphis++;
					}
				if(document.getElementById("districtIdForDSPQ").value==804800){			
					if(countPrimaryResp==0)
					{
						$('#divErrorMsg_top_emphis').append("&#149; "+resourceJSON.msgprimaryresponsibilities+"<br>");
						cntdivErrorMsg_top_emphis++;
					}
					
					if(countmostSignCont==0 && $('#jobcategoryDsp').val().trim().indexOf("Hourly")==-1)
					{
						$('#divErrorMsg_top_emphis').append("&#149; "+resourceJSON.msgmostsignificantcontributions+"<br>");
						cntdivErrorMsg_top_emphis++;
					}
			}
				if(document.getElementById("districtIdForDSPQ").value==1201470){			
					if(countPrimaryResp==0)
					{
						$('#divErrorMsg_top_emphis').append("&#149; "+resourceJSON.msgprimaryresponsibilities+"<br>");
						cntdivErrorMsg_top_emphis++;
					}			
					
					if(countmostSignCont==0)
					{
						$('#divErrorMsg_top_emphis').append("&#149;" +resourceJSON.msgmostsignificantcontributions+"<br>");
						cntdivErrorMsg_top_emphis++;
					}
			}
				if(countPrimaryResp>5000)
				{
					$('#divErrorMsg_top_emphis').append("&#149; "+resourceJSON.msgMAxLenPrimaryResponsibilities+"<br>");
					cntdivErrorMsg_top_emphis++;
				}
				
				if(countmostSignCont>5000)
				{
					$('#divErrorMsg_top_emphis').append("&#149; "+resourceJSON.msgMAxLenSignificantContributions+"<br>");
					cntdivErrorMsg_top_emphis++;
				}
				if($(".reasonForLeavdiv").is(':visible') &&  roleEndYear.value!=0 && roleEndMonth.value!=0){
					if(reasonForLea.length==0)
					{
						$('#divErrorMsg_top_emphis').append("&#149; "+resourceJSON.msgReasonforleaving+"<br>");
							cntdivErrorMsg_top_emphis++;
					}
				}
				
				if(reasonForLea.length>5000)
				{
					$('#divErrorMsg_top_emphis').append("&#149; "+resourceJSON.msgMaxLenReasonforleaving+"<br>");
					cntdivErrorMsg_top_emphis++;
				}
				if(cntdivErrorMsg_top_emphis!=0)		
				{
					$('#divErrorMsg_top_emphis_header').show();
					$('#divErrorMsg_top_emphis').show();
					iErrorCount++;
				}	
				else
				{
					$('#divErrorMsg_top_emphis_header').hide();
					$('#divErrorMsg_top_emphis').hide();
				}
			}
			
			else if(($("#role").is(':hidden'))&&(experiance.indexOf("No record found")>-1)&&($('#headQuaterIdForDspq').val()==2))
			{
         	$('#divErrorMsg_top_emphis_header').show();
			$('#divErrorMsg_top_emphis').show();
			$('#divErrorMsg_top_emphis').empty();
			
			$('#divErrorMsg_top_emphis').append("&#149;"+resourceJSON.msgTitleDesignationRole+"<br>");
			$('#divErrorMsg_top_emphis').append("&#149; "+resourceJSON.msgorganization+"<br>");
			$('#divErrorMsg_top_emphis').append("&#149; "+resourceJSON.msgentercity+"<br>");
			 $('#divErrorMsg_top_emphis').append("&#149; "+resourceJSON.msgOrganisation+"<br>");
			 $('#divErrorMsg_top_emphis').append("&#149; "+resourceJSON.msgPleaseselectFromMonth+"<br>");
			 $('#divErrorMsg_top_emphis').append("&#149; "+resourceJSON.msgPleaseselectFromYear+"<br>");
			 $('#divErrorMsg_top_emphis').append("&#149; "+resourceJSON.msgPleaseselectToMonth+"<br>");
			 $('#divErrorMsg_top_emphis').append("&#149; "+resourceJSON.msgPleaseselectToYear+"<br>");
			 $('#divErrorMsg_top_emphis').append("&#149; "+resourceJSON.msgtypeRole+"<br>");
			 $('#divErrorMsg_top_emphis').append("&#149; "+resourceJSON.msgprimaryresponsibilities+"<br>")
			 $('#divErrorMsg_top_emphis').append("&#149; "+resourceJSON.msgmostsignificantcontributions+"<br>");
			}
		
			else
			{
				$('#divErrorMsg_top_emphis').empty();
				$('#divErrorMsg_top_emphis').hide();
				$('#divErrorMsg_top_emphis_header').hide();
			}
			
			if(isItvtForMiami==false)
			{
				if($("#examStatus").is(':visible') && ((isMiamiChk=="true" && !IsSIForMiami) || document.getElementById("districtIdForDSPQ").value=="614730"))
				{
					$('#divErrorMsg_top_subarea').empty();
					var cnt_divErrorMsg_top_subarea=0;
					
					var teacherSubjectAreaExamId=document.getElementById("teacherSubjectAreaExamId").value;
					var examStatus = document.getElementById("examStatus").value;
					var examDate = document.getElementById("examDate").value;
					var subjectIdforDSPQ = document.getElementById("subjectIdforDSPQ").value;
					var scoreReport = document.getElementById("scoreReport").value;
					var scoreReportHidden = document.getElementById("scoreReportHidden").value;
					
					
						if(trim(examStatus)=="0"){
							$('#divErrorMsg_top_subarea').append("&#149; "+resourceJSON.msgExamStatus+"</BR>");
							cnt_divErrorMsg_top_subarea++;
						}
						
						
						if(trim(examDate)==""){
							$('#divErrorMsg_top_subarea').append("&#149; "+resourceJSON.msgExamDate+"</BR>");
							cnt_divErrorMsg_top_subarea++;
							
						}
						
						
						if(trim(subjectIdforDSPQ)=="0"){
							$('#divErrorMsg_top_subarea').append("&#149; "+resourceJSON.msgSubject+"</BR>");
							cnt_divErrorMsg_top_subarea++;
						}
						
						
						if(trim(scoreReport)=="" && trim(scoreReportHidden)==""){
							$('#divErrorMsg_top_subarea').append("&#149; "+resourceJSON.msgUploadScoreReport+"</BR>");
							cnt_divErrorMsg_top_subarea++;
						}else{
							
							if(trim(scoreReport)!=""){
								var ext = scoreReport.substr(scoreReport.lastIndexOf('.') + 1).toLowerCase();	
								
								var fileSize = 0;
								if ($.browser.msie==true)
							 	{	
								    fileSize = 0;	   
								}
								else
								{
									if(document.getElementById("scoreReport").files[0]!=undefined)
									{
										fileSize = document.getElementById("scoreReport").files[0].size;
									}
								}
								
								if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt'))
								{
									$('#divErrorMsg_top_subarea').append("&#149; "+resourceJSON.msgAcceptableScoreReportformats+"</BR>");
									cnt_divErrorMsg_top_subarea++;
								}else if(fileSize>=10485760){
									$('#divErrorMsg_top_subarea').append("&#149; "+resourceJSON.msgScoreReportFilesize+"<br>");
									cnt_divErrorMsg_top_subarea++;
								}
								
							}
						}
						if(cnt_divErrorMsg_top_subarea!=0)		
						{
							$('#divErrorMsg_top_subarea_header').show();
							$('#divErrorMsg_top_subarea').show();
							iErrorCount++;
						}
						else
						{
							$('#divErrorMsg_top_subarea_header').hide();
							$('#divErrorMsg_top_subarea').hide();
						}
				}
				else
				{
					$('#divErrorMsg_top_subarea').empty();
					$('#divErrorMsg_top_subarea').hide();
					$('#divErrorMsg_top_subarea_header').hide();
				}
			}
			
			
			if($("#documentName").is(':visible'))
			{
				var additionDocumentId=document.getElementById("additionDocumentId").value;
				$('#divErrorMsg_top_AddDoc').empty();
				var cnt_divErrorMsg_top_AddDoc=0;
				var documentName = document.getElementById("documentName").value;
				if(trim(documentName)==""){
					$('#divErrorMsg_top_AddDoc').append("&#149; "+resourceJSON.msgdocumentname+"</BR>");
					cnt_divErrorMsg_top_AddDoc++;
				}
				var uploadedDocument = document.getElementById("uploadedDocument").value;
				var uploadedDocumentHidden = document.getElementById("uploadedDocumentHidden").value;
				if(trim(uploadedDocument)=="" && trim(uploadedDocumentHidden)==""){
					$('#divErrorMsg_top_AddDoc').append("&#149; "+resourceJSON.msguploaddocument+"</BR>");
					cnt_divErrorMsg_top_AddDoc++;
				}else{
					if(trim(uploadedDocument)!=""){
						var ext = uploadedDocument.substr(uploadedDocument.lastIndexOf('.') + 1).toLowerCase();	
						
						var fileSize = 0;
						if ($.browser.msie==true)
					 	{	
						    fileSize = 0;	   
						}
						else
						{
							if(document.getElementById("uploadedDocument").files[0]!=undefined)
							{
								fileSize = document.getElementById("uploadedDocument").files[0].size;
							}
						}
						if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt'))
						{
							$('#divErrorMsg_top_AddDoc').append("&#149; "+resourceJSON.msgacceptabledocformate+"</BR>");
							cnt_divErrorMsg_top_AddDoc++;
						}else if(fileSize>=10485760){
							$('#divErrorMsg_top_AddDoc').append("&#149; "+resourceJSON.msgDocSize10mb+"<br>");
							cnt_divErrorMsg_top_AddDoc++;
						}
					}
				}
				if(cnt_divErrorMsg_top_AddDoc!=0)		
				{
					$('#divErrorMsg_top_AddDoc_header').show();
					$('#divErrorMsg_top_AddDoc').show();
					iErrorCount++;
				}
				else
				{
					$('#divErrorMsg_top_AddDoc_header').hide();
					$('#divErrorMsg_top_AddDoc').hide();
				}
			}
			else
			{
				$('#divErrorMsg_top_AddDoc').empty();
				$('#divErrorMsg_top_AddDoc_header').hide();
				$('#divErrorMsg_top_AddDoc').hide();
			}
			
			if($("#videourl").is(':visible'))			
			{
				var cnt_divErrorMsg_top_Video=0;
				$("#divErrorMsg_top_Video").empty();
				
				var videourl = document.getElementById("videourl").value;
				if(trim(videourl)==""){
					$('#divErrorMsg_top_Video').append("&#149; "+resourceJSON.PlzEtrVideoLink+"</BR>");
					cnt_divErrorMsg_top_Video++;
				}
				
				if(cnt_divErrorMsg_top_Video!=0)		
				{
					$('#divErrorMsg_top_Video_header').show();
					$('#divErrorMsg_top_Video').show();
					iErrorCount++;
				}
				else
				{
					$('#divErrorMsg_top_Video_header').hide();
					$('#divErrorMsg_top_Video').hide();
				}
				
			}else{
				$("divErrorMsg_top_Video").empty();
				$('#divErrorMsg_top_Video_header').hide();
				$('#divErrorMsg_top_Video').hide();
			}
			
			if($("#salutation").is(':visible'))
			{
				var elerefAutoId	=	document.getElementById("elerefAutoId");
				var salutation		=	document.getElementById("salutation");
				
				var firstName		=	trim(document.getElementById("firstName").value);
				var lastName		= 	trim(document.getElementById("lastName").value);;
				var designation		= 	trim(document.getElementById("designation").value);
				
				var organization	=	trim(document.getElementById("organization").value);
				var email			=	trim(document.getElementById("email").value);
				
				var contactnumber	=	trim(document.getElementById("contactnumber").value);
				var longHaveYouKnow	=	trim(document.getElementById("longHaveYouKnow").value);
				var rdcontacted0	=   document.getElementById("rdcontacted0");
				var rdcontacted1	=   document.getElementById("rdcontacted1");
				var pathOfReferencesFile = document.getElementById("pathOfReferenceFile");
				
				var cnt_divErrorMsg_top_ref=0;
				
				
				var fileName = pathOfReferencesFile.value;
				var ext = fileName.substr(fileName.lastIndexOf('.') + 1).toLowerCase();
				var fileSize=0;		
				if ($.browser.msie==true){	
				    fileSize = 0;	   
				}else{		
					if(pathOfReferencesFile.files[0]!=undefined)
					fileSize = pathOfReferencesFile.files[0].size;
				}
				
				$('#divErrorMsg_top_ref').empty();
				setDefColortoErrorMsgToElectronicReferences();
				
				if(firstName=="")
				{
					$('#divErrorMsg_top_ref').append("&#149; "+resourceJSON.PlzEtrFirstName+"<br>");
					cnt_divErrorMsg_top_ref++;
				}
				if(lastName=="")
				{
					$('#divErrorMsg_top_ref').append("&#149; "+resourceJSON.PlzEtrLastName+"<br>");
					cnt_divErrorMsg_top_ref++;	
				}
				if(designation=="" && (($("#dspqName").val()=="Option C" || $("#dspqName").val()=="Option D" || document.getElementById("districtIdForDSPQ").value==1201470 || document.getElementById("districtIdForDSPQ").value==4218990 || document.getElementById("districtIdForDSPQ").value==5510470) 
						)&& document.getElementById("districtIdForDSPQ").value!=3702340)
				{				
					$('#divErrorMsg_top_ref').append("&#149; "+resourceJSON.msgTitle+"<br>");
					cnt_divErrorMsg_top_ref++;	
				}
				
				if(organization=="" && (($("#dspqName").val()=="Option C" || $("#dspqName").val()=="Option D" || document.getElementById("districtIdForDSPQ").value==1201470 || document.getElementById("districtIdForDSPQ").value==7800038 || document.getElementById("districtIdForDSPQ").value==4218990 || document.getElementById("districtIdForDSPQ").value==3904380
						|| document.getElementById("districtIdForDSPQ").value==5510470 || document.getElementById("districtIdForDSPQ").value==7800144 || document.getElementById("districtIdForDSPQ").value==7800202)
						) && document.getElementById("districtIdForDSPQ").value!=3702340){
					$('#divErrorMsg_top_ref').append("&#149; "+resourceJSON.PlzEtrOrg+"<br>");
					cnt_divErrorMsg_top_ref++;
				}
				
				if(email=="")
				{
					$('#divErrorMsg_top_ref').append("&#149; "+resourceJSON.PlzEtrEmail+"<br>");
					cnt_divErrorMsg_top_ref++;
				}
				else if(!isEmailAddress(email))
				{		
					$('#divErrorMsg_top_ref').append("&#149; "+resourceJSON.PlzEtrVldEmail+"<br>");
					cnt_divErrorMsg_top_ref++;
				}if(contactnumber=="")
				{
					$('#divErrorMsg_top_ref').append("&#149; "+resourceJSON.PlzEtrCtctNum+"<br>");
					cnt_divErrorMsg_top_ref++;
				}
				else if(contactnumber.length==10)
				{
					
				}
				else
				{
					$('#divErrorMsg_top_ref').append("&#149; "+resourceJSON.errorMsgForNumber +"<br>");
					cnt_divErrorMsg_top_ref++;
				}
				
				if(longHaveYouKnow=="" && document.getElementById("districtIdForDSPQ").value==4218990)
				{
					$('#divErrorMsg_top_ref').append("&#149; "+resourceJSON.msgHowLongYouKnwPers+"<br>");
					cnt_divErrorMsg_top_ref++;
				}
				
				var rdcontacted_value;
				
				if (rdcontacted0.checked) {
					rdcontacted_value = false;
				}
				else if (rdcontacted1.checked) {
					rdcontacted_value = true;
				}
				else if($("#headQuaterIdForDspq").val()==2)
				{
					$('#divErrorMsg_top_ref').append("&#149; Please indicate if reference may be contacted.");
					cnt_divErrorMsg_top_ref++;
				}
				else
				{
				rdcontacted_value = true;
				}

				if(countConfig_ReferenceLettersOfRecommendation==1){
					if(fileName==""){
						$('#divErrorMsg_top_ref').append("&#149; "+resourceJSON.msgUploadRecommendation+"</BR>");
						cnt_divErrorMsg_top_ref++;
					}
				}
				
				if(ext!=""){
					if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt'))
					{
						$('#divErrorMsg_top_ref').append("&#149; "+resourceJSON.AcceptFileFormat+"<br>");
						cnt_divErrorMsg_top_ref++;
					}	
					else if(fileSize>=10485760)
					{
						$('#divErrorMsg_top_ref').append("&#149; "+resourceJSON.msgentercertificatename+"<br>");
						cnt_divErrorMsg_top_ref++;
					}
				}
				
				/*else if(isEmailAddress(trim(email.value)))
				{
					PFCertifications.checkEmailForEleRef(email.value,
					{ 
						async: true,
						errorHandler:handleError,
						callback:function(data)
						{
							if(data==true)
							{
								$('#errordivElectronicReferences').append("&#149; A Electronic References has already registered with the email.<br>");
								if(focs==0)
									$('#email').focus();
								$('#email').css("background-color",txtBgColor);
									cnt++;focs++;
							}
						}
					});
				}*/

				if(cnt_divErrorMsg_top_ref!=0)		
				{
					$('#divErrorMsg_top_ref_header').show();
					$('#divErrorMsg_top_ref').show();
					iErrorCount++;
				}
				else
				{
					$('#divErrorMsg_top_ref_header').hide();
					$('#divErrorMsg_top_ref').hide();
				}
			}
			else
			{
				$('#divErrorMsg_top_ref').empty();
				$('#divErrorMsg_top_ref').hide();
				$('#divErrorMsg_top_ref_header').hide();
			}
			if($("#languageText").is(':visible')){
				
				var cnt_divErrorMsg_top_lang=0;
				var language = $("#languageText").val();
				var oralSkill=$("#oralSkills").val();
				var writtenSkill=$("#writtenSkills").val();
				var teacherLanguageId=$("#teacherLanguageId").val();
				$('#divErrorMsg_top_lang').empty();
				if(trim(language)==""){					
					$('#divErrorMsg_top_lang').append("&#149; "+resourceJSON.msgEnterLang+"<br>");
					cnt_divErrorMsg_top_lang++;
				}
				if(trim(oralSkill)=="0")
				{
					$('#divErrorMsg_top_lang').append("&#149; "+resourceJSON.msgOralskill+"<br>");
					cnt_divErrorMsg_top_lang++;
				}
				if(trim(writtenSkill)=="0")
				{
					$('#divErrorMsg_top_lang').append("&#149; "+resourceJSON.msgWrittenSkill+"<br>");
					cnt_divErrorMsg_top_lang++;
				}
				
				if(cnt_divErrorMsg_top_ref!=0)		
				{
					$('#divErrorMsg_top_lang_header').show();
					$('#divErrorMsg_top_lang').show();
					iErrorCount++;
				}
				else
				{
					$('#divErrorMsg_top_lang').empty();
					$('#divErrorMsg_top_lang').hide();
					$('#divErrorMsg_top_lang_header').hide();
				}
			}else{
				$('#divErrorMsg_top_lang').empty();
				$('#divErrorMsg_top_lang').hide();
				$('#divErrorMsg_top_lang_header').hide();
			}
			if($("#schoolNameStdTch").is(':visible'))
			{		
				var cnt_divErrorMsg_top_StdTchExp=0;
				var schoolNamestdTch  = trim(document.getElementById('schoolNameStdTch').value);
				var subjectStdTch 	  = trim(document.getElementById('subjectStdTch').value);	
				var fromDate 		  = trim(document.getElementById('fromStdTch').value);
				var toDate 		  = trim(document.getElementById('toStdTch').value);
					
				if(schoolNamestdTch==""){
					$('#divErrorMsg_top_StdTchExp').append("&#149; "+resourceJSON.msgfillSchoolName+"<br>");
					cnt_divErrorMsg_top_StdTchExp++;
				}
				
				if(subjectStdTch==""){
					$('#divErrorMsg_top_StdTchExp').append("&#149; "+resourceJSON.msgFillSubj+"<br>");
					cnt_divErrorMsg_top_StdTchExp++;
					
				}
				
				if(fromDate==""){
					$('#divErrorMsg_top_StdTchExp').append("&#149; "+resourceJSON.msgfillfromDate+"<br>");
					cnt_divErrorMsg_top_StdTchExp++;
				}
				if(toDate==""){
					$('#divErrorMsg_top_StdTchExp').append("&#149; "+resourceJSON.msgfillToDate+"<br>");
					cnt_divErrorMsg_top_StdTchExp++;
				}
				
				if(cnt_divErrorMsg_top_ref!=0)		
				{
					$('#divErrorMsg_top_StdTchExp').show();
					$('#divErrorMsg_top_StdTchExp_header').show();
					iErrorCount++;
				}
				else
				{
					$('#divErrorMsg_top_StdTchExp').hide();
					$('#divErrorMsg_top_StdTchExp_header').hide();
				}
			}else
			{
				$('#divErrorMsg_top_StdTchExp').empty();
				$('#divErrorMsg_top_StdTchExp').hide();
				$('#divErrorMsg_top_StdTchExp_header').hide();
			}
			
			if($("#organizationInv").is(':visible'))
			{	var cnt_divErrorMsg_top_invol=0;
				var organization = document.getElementById("organizationInv");
				var orgTypeId = document.getElementById("orgTypeId");
				var rangeId = document.getElementById("rangeId");
				var leadNoOfPeople =  document.getElementById("leadNoOfPeople");
				$('#divErrorMsg_top_invol').empty();
				if(trim(organization.value)=="")
				{
					$('#divErrorMsg_top_invol').append("&#149; "+resourceJSON.PlzEtrOrg+"<br>");
					cnt_divErrorMsg_top_invol++;
				}

				if($("#orgTypDivInv").is(':visible') && trim(orgTypeId.value)=="")
				{
					$('#divErrorMsg_top_invol').append("&#149; "+resourceJSON.msgorgtype+"<br>");
					cnt_divErrorMsg_top_invol++;
				}

				if(document.getElementById("districtIdForDSPQ").value!=3703120)	
				if($("#rangeIdspq").is(':visible') && trim(rangeId.value)=="")
				{
					$('#divErrorMsg_top_invol').append("&#149; "+resourceJSON.msgnumberofpeople+"<br>");
					cnt_divErrorMsg_top_invol++;
				}

				
				if(rdo1.checked && trim(leadNoOfPeople.value)=="")
				{
					$('#divErrorMsg_top_invol').append("&#149; "+resourceJSON.msgnumberofpeople+"<br>");
					cnt_divErrorMsg_top_invol++;
				}

				if(cnt_divErrorMsg_top_invol!=0)		
				{
					$('#divErrorMsg_top_invol').show();
					$('#divErrorMsg_top_invol_header').show();
					iErrorCount++;
				}else{
					$('#divErrorMsg_top_invol').hide();
					$('#divErrorMsg_top_invol_header').hide();
				}
				
			}else{
				$('#divErrorMsg_top_invol').empty();
				$('#divErrorMsg_top_invol').hide();
				$('#divErrorMsg_top_invol_header').hide();
			}
			
			if($("#honor").is(':visible'))
			{	
				var cnt_divErrorMsg_top_honors=0;
				var honor = document.getElementById("honor");
				var honorYear = document.getElementById("honorYear");
				$('#divErrorMsg_top_honors').empty();
				if(trim(honor.value)=="")
				{
					$('#divErrorMsg_top_honors').append("&#149; "+resourceJSON.msgaward+"<br>");		
					cnt_divErrorMsg_top_honors++;
				}

				if(trim(honorYear.value)=="0")
				{
					$('#divErrorMsg_top_honors').append("&#149; "+resourceJSON.msgawardyear+"<br>");
					cnt_divErrorMsg_top_honors++;
				}
				
				if(cnt_divErrorMsg_top_honors!=0){
					$('#divErrorMsg_top_honors').show();
					$('#divErrorMsg_top_honors_header').show();
					iErrorCount++;
				}
				else{
					$('#divErrorMsg_top_honors').hide();
					$('#divErrorMsg_top_honors_header').hide();
				}
			}else{
				$('#divErrorMsg_top_honors').empty();
				$('#divErrorMsg_top_honors').hide();
				$('#divErrorMsg_top_honors_header').hide();
			}
			// other
			
			var affidavit=document.getElementsByName("affidavit");
			var affflag;
			
			if(affidavit[0].checked)
				affflag=true;
			else 
				affflag=false;
			
			var tfaAffiliate 	= 	document.getElementById("tfaAffiliate").value;
			var corpsYear 		= 	document.getElementById("corpsYear").value;
			var tfaRegion 		= 	document.getElementById("tfaRegion").value;
			
			var phoneNumber="";//document.getElementById("phoneNumber").value;
		
			var phoneNumber1=document.getElementById("phoneNumber1").value;
			var phoneNumber2=document.getElementById("phoneNumber2").value;
			var phoneNumber3=document.getElementById("phoneNumber3").value;
			if(phoneNumber1!="" && phoneNumber2!="" && phoneNumber3!="")
				if(phoneNumber1.length==3 && phoneNumber2.length==3 && phoneNumber3.length==4)
					phoneNumber=phoneNumber1+phoneNumber2+phoneNumber3;
			
			

			var hdnResume = document.getElementById("hdnResume").value;
			var resumeFile = document.getElementById("resume");
			
			var addressLine1 = document.getElementById("addressLine1");
			var addressLine2 = document.getElementById("addressLine2");
			var zipCode = document.getElementById("zipCode");
			
			var stateIdForDSPQ = "";
			var cityIdForDSPQ = "";
			var countryId = document.getElementById("countryId").value;
			
			
			
			if(countryId==223)
			{
				stateIdForDSPQ = document.getElementById("stateIdForDSPQ").value;
				cityIdForDSPQ = document.getElementById("cityIdForDSPQ").value;
			}
			else
			{
				if(document.getElementById("countryCheck").value==1)
				{
					stateIdForDSPQ = document.getElementById("stateIdForDSPQ").value;
					cityIdForDSPQ = document.getElementById("otherCity").value;
				}
				else
				{
					stateIdForDSPQ = document.getElementById("otherState").value;
					cityIdForDSPQ = document.getElementById("otherCity").value;
				}
			}
			
			
			/*if(document.getElementById("countryCheck").value==1)
			{
				stateIdForDSPQ = document.getElementById("stateIdForDSPQ").value;
				cityIdForDSPQ = document.getElementById("cityIdForDSPQ").value;
			}
			else if(document.getElementById("countryCheck").value==0)
			{
				stateIdForDSPQ = document.getElementById("otherState").value;
				cityIdForDSPQ = document.getElementById("otherCity").value;
			}*/
			
			
			
			var expCertTeacherTraining = document.getElementById("expCertTeacherTraining").value;
			var nbc1 = document.getElementById("nbc1");
			var nbc2 = document.getElementById("nbc2");
			var nationalBoardCertYear = document.getElementById("nationalBoardCertYear").value;
			
			//
			
			var salutation_pi=$("#salutation_pi").val();
			var firstName_pi=$("#firstName_pi").val();
			var middleName_pi=$("#middleName_pi").val();
			var lastName_pi=$("#lastName_pi").val();
			var ssn_pi=$("#ssn_pi").val();
			
			var dobMonth=$("#dobMonth").val();
			var dobDay=$("#dobDay").val();
			var dobYear=$("#dobYear").val();
			
			var dob="";
			
			var vt1=$("#vt1").val();
			var vt2=$("#vt2").val();
			var veteranValue="";
			
			if (document.getElementById('vt1').checked) {
				veteranValue = document.getElementById('vt1').value;
			}else if (document.getElementById('vt2').checked) {
				veteranValue = document.getElementById('vt2').value;
			}
			
			
			
			var rtDate=$("#rtDate").val();
			var wdDate=$("#wdDate").val();
			var rwdDate=$("#rwdDate").val();
			
			var employeeType=0;
			var formerEmployeeNo=0;;
			//var isRetiredEmployee=0;
			var currentEmployeeNo=0;;
			var isCurrentFullTimeTeacher=0;
			
			//
			
			var fileName = resumeFile.value;
			var ext = fileName.substr(fileName.lastIndexOf('.') + 1).toLowerCase();
			var fileSize=0;		
			if ($.browser.msie==true){	
			    fileSize = 0;	   
			}else{		
				if(resumeFile.files[0]!=undefined)
				fileSize = resumeFile.files[0].size;
			}
			
			
			var canServeAsSubTeacher=2;
			try{
				if (document.getElementById('canServeAsSubTeacher0').checked) {
					canServeAsSubTeacher = document.getElementById('canServeAsSubTeacher0').value;
				}else if (document.getElementById('canServeAsSubTeacher1').checked) {
					canServeAsSubTeacher = document.getElementById('canServeAsSubTeacher1').value;
				}
			}catch(err){alert(err);}
			
			var cnt_resume=0;
			var cnt_tfa=0;
			var cnt_wst=0;
			var cnt_ph=0;
			var cnt_address1=0;
			var cnt_zip=0;
			var cnt_state=0;
			var cnt_city=0;
			var cnt_Country=0;
			var cnt_ectt=0;
			var cnt_nbcy=0;
			var cnt_affdt=0;
			var focs=0;	

			var cnt_PersonalInfo_other=0;
			var cnt_SSN=0;
			var cnt_Race=0;
			var cnt_EthnicOrigin=0;
			var cnt_Ethinicity=0;
			var cnt_FormerEmployee=0;
			var cnt_Veteran=0;
			var cnt_Gender=0;
			
			var cnt_GeneralKnowledge=0;
			var cnt_SubjectAreaExam=0;
			var cnt_AdditionalDocuments=0;
			var cnt_RetireNo=0;
			
			$('#divErrorMsg_top_pInfo').empty();
			//if(personalinfo_config==1 || ssn_config==1)
			if(personalinfo_config==1)
			{
				if(trim(firstName_pi)=="")
				{
					$('#divErrorMsg_top_pInfo').append("&#149; "+resourceJSON.PlzEtrFirstName+"<br>");
					cnt_PersonalInfo_other++;
				}
				
				if(trim(lastName_pi)=="")
				{
					$('#divErrorMsg_top_pInfo').append("&#149; "+resourceJSON.PlzEtrLastName+"<br>");
					cnt_PersonalInfo_other++;
				}				
			
				/*if(trim(ssn_pi)=="")
				{
					$('#divErrorMsg_top_pInfo').append("&#149; Please enter SSN<br>");
					cnt_PersonalInfo_other++;
				}*/
				
				if(cnt_PersonalInfo_other!=0)
				{
					$('#divErrorMsg_top_pInfo_header').show();
					$('#divErrorMsg_top_pInfo').show();
					iErrorCount++;
				}
				else
				{
					$('#divErrorMsg_top_pInfo_header').hide();
					$('#divErrorMsg_top_pInfo').hide();
				}
				
				
			}
		
			var con_divErrorMsg_top_DOB=0;
			if(!(document.getElementById("districtIdForDSPQ").value==7800033 && ($('#jobcategoryDsp').val().trim()=="Student Support Services" || $('#jobcategoryDsp').val().trim()=="Instructional and SLT"))){
			if(document.getElementById("districtIdForDSPQ").value==4218990 || document.getElementById("districtIdForDSPQ").value==1302010){
				$('#divErrorMsg_top_DOB').empty();
				var idobYear = new String(parseInt(trim(dobYear)));
				var currentFullYear = new Date().getFullYear();
				currentFullYear=currentFullYear-1;
				
				if(trim(dobMonth)!="0"){
					if(trim(dobDay)=="0")
					{
						$('#divErrorMsg_top_DOB').append("&#149; "+resourceJSON.msgDayofBirth+"<br>");
						con_divErrorMsg_top_DOB++;
					}
					
					if((dobYear=="") || ( dobYear==0 || idobYear=="NaN") || (idobYear > currentFullYear || idobYear < 1931))
					{
						$('#divErrorMsg_top_DOB').append("&#149; "+resourceJSON.msgValidYearBirth+"<br>");
						con_divErrorMsg_top_DOB++;
					}
				
				}else if(trim(dobDay)!="0" ){	
					if(trim(dobMonth)=="0")
					{
						$('#divErrorMsg_top_DOB').append("&#149; "+resourceJSON.msgMonthOfbirth+"<br>");
						con_divErrorMsg_top_DOB++;
					}
					
					if((dobYear=="") || ( dobYear==0 || idobYear=="NaN") || (idobYear > currentFullYear || idobYear < 1931))
					{
						$('#divErrorMsg_top_DOB').append("&#149; "+resourceJSON.msgValidYearBirth+"<br>");
						con_divErrorMsg_top_DOB++;
					}
				}else if((dobYear!="") || ( dobYear!=0 || idobYear!="NaN") || (idobYear < currentFullYear || idobYear > 1931)){
					if(trim(dobMonth)=="0")
					{
						$('#divErrorMsg_top_DOB').append("&#149; "+resourceJSON.msgMonthOfbirth+"<br>");
						con_divErrorMsg_top_DOB++;
					}
					
					if(trim(dobDay)=="0")
					{
						$('#divErrorMsg_top_DOB').append("&#149; "+resourceJSON.msgDayofBirth+"<br>");
						con_divErrorMsg_top_DOB++;
					}
				}
				
				if(con_divErrorMsg_top_DOB!=0)
				{	$('#divErrorMsg_top_DOB_header').show();
					$('#divErrorMsg_top_DOB').show();
					iErrorCount++;
				}
				else
				{
					$('#divErrorMsg_top_DOB_header').hide();
					$('#divErrorMsg_top_DOB').hide();
				}
				
			}
			else if(dateOfBirth_config==1){
				
				var dobMonth=$("#dobMonth").val();
				var dobDay=$("#dobDay").val();
				var dobYear=$("#dobYear").val();
				
				if(!((trim(dobMonth)==0 && trim(dobDay)==0 && (trim(dobYear)==0||trim(dobYear)==""))&& document.getElementById('dateOfBirthOptional').value==1)){
					
				$('#divErrorMsg_top_DOB').empty();
				if(trim(dobMonth)=="0")
				{
					$('#divErrorMsg_top_DOB').append("&#149; "+resourceJSON.msgMonthOfbirth+"<br>");
					con_divErrorMsg_top_DOB++;
				}
				
				if(trim(dobDay)=="0")
				{
					$('#divErrorMsg_top_DOB').append("&#149; "+resourceJSON.msgDayofBirth+"<br>");
					con_divErrorMsg_top_DOB++;
				}
												
				var idobYear = new String(parseInt(trim(dobYear)));
				var currentFullYear = new Date().getFullYear();
				currentFullYear=currentFullYear-1;
				
				
				
				if((dobYear=="") || ( dobYear==0 || idobYear=="NaN") || (idobYear > currentFullYear || idobYear < 1931))
				{
					$('#divErrorMsg_top_DOB').append("&#149; "+resourceJSON.msgValidYearBirth+"<br>");
					con_divErrorMsg_top_DOB++;
				}
				
				if(con_divErrorMsg_top_DOB!=0)
				{
					$('#divErrorMsg_top_DOB_header').show();
					$('#divErrorMsg_top_DOB').show();
					iErrorCount++;
				}
				else
				{
					$('#divErrorMsg_top_DOB_header').hide();
					$('#divErrorMsg_top_DOB').hide();
				}
			}
		  }
		}	
			
			var con_divErrorMsg_top_Address=0;
			 if($("#addressOptional").val()=="false"){
				address_config=0;
			}
			if(address_config==1)
			{
				$('#divErrorMsg_top_Address').empty();
				if(trim(addressLine1.value)=="")
				{
					$('#divErrorMsg_top_Address').append("&#149; "+resourceJSON.msgaddresslinefirst+"<br>");
					con_divErrorMsg_top_Address++;
				}	
				if(trim(zipCode.value)=="")
				{
					$('#divErrorMsg_top_Address').append("&#149; "+resourceJSON.msgenterzipcode+"<br>");
					con_divErrorMsg_top_Address++;	
				}
				
				if(countryId=="")
				{
					$('#divErrorMsg_top_Address').append("&#149; "+resourceJSON.msgPleaseaCountry+"<br>");
					con_divErrorMsg_top_Address++;
				}
				else
				{
					
					if(document.getElementById("countryCheck").value==1)
					{
						if(trim(stateIdForDSPQ)=="")
						{
							$('#divErrorMsg_top_Address').append("&#149; "+resourceJSON.msgPleaseselectaState+"<br>");
							con_divErrorMsg_top_Address++;
						}
						if(trim(cityIdForDSPQ)=="")
						{
							$('#divErrorMsg_top_Address').append("&#149; "+resourceJSON.msgPleaseselectaCity+"<br>");
							con_divErrorMsg_top_Address++;
						}
					}
					else if(document.getElementById("countryCheck").value==0)
					{
						if(trim(stateIdForDSPQ)=="")
						{
							$('#divErrorMsg_top_Address').append("&#149; "+resourceJSON.msgPleaseselectaState+"<br>");
							con_divErrorMsg_top_Address++;
						}
						if(trim(cityIdForDSPQ)=="")
						{
							$('#divErrorMsg_top_Address').append("&#149; "+resourceJSON.msgPleaseselectaCity+"<br>");
							con_divErrorMsg_top_Address++;
						}
					}
				}
				
				if(con_divErrorMsg_top_Address!=0)
				{
					$('#divErrorMsg_top_Address_pInfo_header').show();
					$('#divErrorMsg_top_Address').show();
					iErrorCount++;
				}
				else
				{
					$('#divErrorMsg_top_Address_pInfo_header').hide();
					$('#divErrorMsg_top_Address').hide();
				}
			}
			
			var praddressLine1 = document.getElementById("addressLinePr");
			var prpraddressLine2 = document.getElementById("addressLine2Pr");
			var przipCode = document.getElementById("zipCodePr");
			
			var prstateIdForDSPQ = "";
			var prcityIdForDSPQ = "";
			var prcountryId = document.getElementById("countryIdPr").value;
			
			if(prcountryId!="")
			{
				if(prcountryId==223)
				{
					prstateIdForDSPQ = document.getElementById("stateIdForDSPQPr").value;
					prcityIdForDSPQ = document.getElementById("cityIdForDSPQPr").value;
				}
				else
				{
					if(document.getElementById("countryCheckPr").value==1)
					{
						prstateIdForDSPQ = document.getElementById("stateIdForDSPQPr").value;
						prcityIdForDSPQ = document.getElementById("otherCityPr").value;
					}
					else
					{
						prstateIdForDSPQ = document.getElementById("otherStatePr").value;
						prcityIdForDSPQ = document.getElementById("otherCityPr").value;
					}
				}
			}
			
			var con_divErrorMsg_top_PerAddress=0;
			if(address_config==1 && (document.getElementById("districtIdForDSPQ").value==4503810 || document.getElementById("districtIdForDSPQ").value==7800294))
			{
				$('#divErrorMsg_top_preAddress').empty();
				if(trim(praddressLine1.value)=="")
				{
					$('#divErrorMsg_top_preAddress').append("&#149; Please enter Address Line 1<br>");
					con_divErrorMsg_top_PerAddress++;
				}	
				if(trim(przipCode.value)=="")
				{
					$('#divErrorMsg_top_preAddress').append("&#149; Please enter Zip Code<br>");
					con_divErrorMsg_top_PerAddress++;	
				}
				
				if(prcountryId=="")
				{
					$('#divErrorMsg_top_preAddress').append("&#149; Please select a Country<br>");
					con_divErrorMsg_top_PerAddress++;
				}
				else
				{
					if(document.getElementById("countryCheckPr").value==1)
					{
						if(trim(prstateIdForDSPQ)=="")
						{
							$('#divErrorMsg_top_preAddress').append("&#149; Please select a State<br>");
							con_divErrorMsg_top_PerAddress++;
						}
						if(trim(prcityIdForDSPQ)=="")
						{
							$('#divErrorMsg_top_preAddress').append("&#149; Please select a City<br>");
							con_divErrorMsg_top_PerAddress++;
						}
					}
					else if(document.getElementById("countryCheckPr").value==0)
					{
						if(trim(prstateIdForDSPQ)=="")
						{
							$('#divErrorMsg_top_preAddress').append("&#149; Please select a State<br>");
							con_divErrorMsg_top_PerAddress++;
						}
						if(trim(prcityIdForDSPQ)=="")
						{
							$('#divErrorMsg_top_preAddress').append("&#149; Please select a City<br>");
							con_divErrorMsg_top_PerAddress++;
						}
					}
				}
				
				if(con_divErrorMsg_top_PerAddress!=0)
				{
					$('#divErrorMsg_top_preAddress_pInfo_header').show();
					$('#divErrorMsg_top_preAddress').show();
					iErrorCount++;
				}
				else
				{
					$('#divErrorMsg_top_preAddress_pInfo_header').hide();
					$('#divErrorMsg_top_preAddress').hide();
				}
			}
			

			
			// khan
		
               var cnt_divErrorMsg_top_SIN=0;
               $( "#divErrorMsg_top_SIN" ).empty();
			    if(document.getElementById('SIN_config').value==1 && trim(document.getElementById('sin_pi').value)=="")
				{
			    	
				        $('#divErrorMsg_top_SIN').append("&#149;"+resourceJSON.msgPlzSIN+"</BR>");
				        cnt_divErrorMsg_top_SIN++;
						
				}else if(document.getElementById('SIN_config').value==1 && trim(document.getElementById('sin_pi').value).length!=9){
					  $('#divErrorMsg_top_SIN').append("&#149;"+resourceJSON.msgPlzSINValid+"</BR>");
					  cnt_divErrorMsg_top_SIN++;
					
			   } else if(document.getElementById('SIN_config').value==2 && trim(document.getElementById('sin_pi').value).length!=9 && trim(document.getElementById('sin_pi').value).length>0 ){
			          $('#divErrorMsg_top_SIN').append("&#149; "+resourceJSON.msgPlzSINValid+"</BR>");
			          cnt_divErrorMsg_top_SIN++;
					  
			   }	
			    
				if(cnt_divErrorMsg_top_SIN>0){
				
				$('#divErrorMsg_top_SIN_header').show();
				$('#divErrorMsg_top_SIN').show();
			    iErrorCount++;//alert(iErrorCount+"  <67");
				}

			
	


			if(document.getElementById("districtIdForDSPQ").value==4218990){
				var nonteacherFlag=$("#isnontj").val();
				if(nonteacherFlag=="" || nonteacherFlag!="true"){
					if($("#tfaAffiliate").is(':visible'))
					{
						$('#divErrorMsg_top_TFA').empty();
						var cnt_divErrorMsg_top_TFA=0;
						if(trim(tfaAffiliate)=="")
						{
							$('#divErrorMsg_top_TFA').append("&#149; "+resourceJSON.msgTeachForAmericaAffiliate+"<br>");
							cnt_divErrorMsg_top_TFA++;
						}
						
						if(tfaAffiliate!="3" && tfaAffiliate!="")
						{
							if(trim(corpsYear)=="")
							{
								$('#divErrorMsg_top_TFA').append("&#149; "+resourceJSON.msgcorpsyear+"<br>");
								cnt_divErrorMsg_top_TFA++;		
							}
							
							if(trim(tfaRegion)=="")
							{
								$('#divErrorMsg_top_TFA').append("&#149; "+resourceJSON.msgtfaregion+"<br>");
								cnt_divErrorMsg_top_TFA++;
							}
						}
						
						
						if(cnt_divErrorMsg_top_TFA!=0)
						{
							$('#divErrorMsg_top_TFA_header').show();
							$('#divErrorMsg_top_TFA').show();
							iErrorCount++;//alert(iErrorCount+"  <67");
						}
						else
						{
							$('#divErrorMsg_top_TFA_header').hide();
							$('#divErrorMsg_top_TFA').hide();
						}
					}
				}
			}else if(data.tfaOptional==false || document.getElementById("districtIdForDSPQ").value==1201470 || document.getElementById("districtIdForDSPQ").value==3700112 || document.getElementById("districtIdForDSPQ").value==614730 || document.getElementById("districtIdForDSPQ").value==1302010  || document.getElementById("districtIdForDSPQ").value==3700690 || document.getElementById("districtIdForDSPQ").value==3700112 ||  
					(document.getElementById("districtIdForDSPQ").value==3703120 && $('#jobcategoryDsp').val().trim().indexOf("Classified") !=-1) || (document.getElementById("districtIdForDSPQ").value==3703120 && $('#jobcategoryDsp').val().trim().indexOf("Substitute") !=-1) || ($("#headQuaterIdForDspq").val()==2 && $("#dspqName").val()=="Option A")){

				$('#divErrorMsg_top_TFA').empty();
				var cnt_divErrorMsg_top_TFA=0;
				if(tfaAffiliate!="3" && tfaAffiliate!="")
				{
					if(trim(corpsYear)=="")
					{
						$('#divErrorMsg_top_TFA').append("&#149; "+resourceJSON.msgcorpsyear+"<br>");
						cnt_divErrorMsg_top_TFA++;		
					}
					
					if(trim(tfaRegion)=="")
					{
						$('#divErrorMsg_top_TFA').append("&#149; "+resourceJSON.msgtfaregion+"<br>");
						cnt_divErrorMsg_top_TFA++;
					}
				}
				
				
				if(cnt_divErrorMsg_top_TFA!=0)
				{
					$('#divErrorMsg_top_TFA_header').show();
					$('#divErrorMsg_top_TFA').show();
					iErrorCount++;//alert(iErrorCount+"  <68");
				}
				else
				{
					$('#divErrorMsg_top_TFA_header').hide();
					$('#divErrorMsg_top_TFA').hide();
				}
			
			}
			else{
			
			if($("#tfaAffiliate").is(':visible'))
			{
				$('#divErrorMsg_top_TFA').empty();
				var cnt_divErrorMsg_top_TFA=0;
				if(trim(tfaAffiliate)=="")
				{
					$('#divErrorMsg_top_TFA').append("&#149; "+resourceJSON.msgSelectTechForTFA+"<br>");
					cnt_divErrorMsg_top_TFA++;
				}
				
				if(tfaAffiliate!="3" && tfaAffiliate!="")
				{
					if(trim(corpsYear)=="")
					{
						$('#divErrorMsg_top_TFA').append("&#149; "+resourceJSON.msgcorpsyear+"<br>");
						cnt_divErrorMsg_top_TFA++;		
					}
					
					if(trim(tfaRegion)=="")
					{
						$('#divErrorMsg_top_TFA').append("&#149; "+resourceJSON.msgtfaregion+"<br>");
						cnt_divErrorMsg_top_TFA++;
					}
				}
				
				
				if(cnt_divErrorMsg_top_TFA!=0)
				{
					$('#divErrorMsg_top_TFA_header').show();
					$('#divErrorMsg_top_TFA').show();
					iErrorCount++;//alert(iErrorCount+"  <68");
				}
				else
				{
					$('#divErrorMsg_top_TFA_header').hide();
					$('#divErrorMsg_top_TFA').hide();
				}
			}
			}
			
			if(isItvtForMiami==false)
			{
				if((displayGKAndSubject=="true" && isMiamiChk=="true" && !IsSIForMiami) || (isMiamiChk=="true" && IsSIForMiami && displayPassFailGK1=="false") || (displayGKAndSubject=='true' && document.getElementById("districtIdForDSPQ").value=="614730"))
				{
					$('#divErrorMsg_top_gk').empty();
					var cnt_divErrorMsg_top_gk=0;
					
					if(generalKnowledge_config==1){
						
						var generalKnowledgeExamStatus = document.getElementById("generalKnowledgeExamStatus").value;
						var generalKnowledgeExamDate = document.getElementById("generalKnowledgeExamDate").value;
						var generalKnowledgeScoreReport = document.getElementById("generalKnowledgeScoreReport").value;
						var generalKnowledgeScoreReportHidden = document.getElementById("generalKnowledgeScoreReportHidden").value;
						if(countSource_Certification!=0)
						{
						  if(jobTitle=="Teach For America 2015-2016"){
							if(generalKnowledgeExamStatus!=0 || generalKnowledgeExamDate!="" || trim(generalKnowledgeScoreReport)!="" && trim(generalKnowledgeScoreReportHidden)!=""){
							if(trim(generalKnowledgeExamStatus)=="0"){
								$('#divErrorMsg_top_gk').append("&#149; "+resourceJSON.msgExamStatus+"</BR>");
								cnt_divErrorMsg_top_gk++;
							}
							
							
							if(trim(generalKnowledgeExamDate)==""){
								$('#divErrorMsg_top_gk').append("&#149; "+resourceJSON.msgExamDate+"</BR>");
								cnt_divErrorMsg_top_gk++;
							}
							
						
							if(trim(generalKnowledgeScoreReport)=="" && trim(generalKnowledgeScoreReportHidden)==""){
								$('#divErrorMsg_top_gk').append("&#149; "+resourceJSON.msgUploadScoreReport+"</BR>");
								cnt_divErrorMsg_top_gk++;
							}else{
								if(trim(generalKnowledgeScoreReport)!=""){
									var ext = generalKnowledgeScoreReport.substr(generalKnowledgeScoreReport.lastIndexOf('.') + 1).toLowerCase();	
									
									var fileSize = 0;
									if ($.browser.msie==true)
								 	{	
									    fileSize = 0;	   
									}
									else
									{
										if(document.getElementById("generalKnowledgeScoreReport").files[0]!=undefined)
										{
											fileSize = document.getElementById("generalKnowledgeScoreReport").files[0].size;
										}
									}
									
									if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt'))
									{
										$('#divErrorMsg_top_gk').append("&#149; "+resourceJSON.msgAcceptableScoreReportformats+"</BR>");
										cnt_divErrorMsg_top_gk++;
									}else if(fileSize>=10485760){
										$('#divErrorMsg_top_gk').append("&#149; "+resourceJSON.msgScoreReportFilesize+".<br>");
										cnt_divErrorMsg_top_gk++;
									}
								}
							}
								}
						}else{

							if(trim(generalKnowledgeExamStatus)=="0"){
								$('#divErrorMsg_top_gk').append("&#149; "+resourceJSON.msgExamStatus+"</BR>");
								cnt_divErrorMsg_top_gk++;
							}
							
							
							if(trim(generalKnowledgeExamDate)==""){
								$('#divErrorMsg_top_gk').append("&#149; "+resourceJSON.msgExamDate+"</BR>");
								cnt_divErrorMsg_top_gk++;
							}
							
						
							if(trim(generalKnowledgeScoreReport)=="" && trim(generalKnowledgeScoreReportHidden)==""){
								$('#divErrorMsg_top_gk').append("&#149; "+resourceJSON.msgUploadScoreReport+"</BR>");
								cnt_divErrorMsg_top_gk++;
							}else{
								if(trim(generalKnowledgeScoreReport)!=""){
									var ext = generalKnowledgeScoreReport.substr(generalKnowledgeScoreReport.lastIndexOf('.') + 1).toLowerCase();	
									
									var fileSize = 0;
									if ($.browser.msie==true)
								 	{	
									    fileSize = 0;	   
									}
									else
									{
										if(document.getElementById("generalKnowledgeScoreReport").files[0]!=undefined)
										{
											fileSize = document.getElementById("generalKnowledgeScoreReport").files[0].size;
										}
									}
									
									if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt'))
									{
										$('#divErrorMsg_top_gk').append("&#149; "+resourceJSON.msgAcceptableScoreReportformats+"</BR>");
										cnt_divErrorMsg_top_gk++;
									}else if(fileSize>=10485760){
										$('#divErrorMsg_top_gk').append("&#149; "+resourceJSON.msgScoreReportFilesize+".<br>");
										cnt_divErrorMsg_top_gk++;
									}
								}
							}
						
						}
					}
					}
					if(cnt_divErrorMsg_top_gk!=0)
					{
						$('#divErrorMsg_top_gk_header').show();
						$('#divErrorMsg_top_gk').show();
						iErrorCount++;
					}
					else
					{
						$('#divErrorMsg_top_gk_header').hide();
						$('#divErrorMsg_top_gk').hide();
					}
				}
				else
				{
					$('#divErrorMsg_top_gk').empty();
					$('#divErrorMsg_top_gk_header').hide();
					$('#divErrorMsg_top_gk').hide();	
				}
			}
			
			
			if($("#fe1").is(':visible'))
			{
				$('#divErrorMsg_top_Curr_Emp').empty();
				var cnt_divErrorMsg_top_Curr_Emp=0;
				
				if($('#fe2').is(':checked') || $('#fe1').is(':checked') || $('#fe3').is(':checked') || $('#fe5').is(':checked'))
				{
					
				}
				else
				{
					$('#divErrorMsg_top_Curr_Emp').append("&#149; "+resourceJSON.msgCurrentEmployment1+"</BR>");
					cnt_divErrorMsg_top_Curr_Emp++;
					
				}
				
				if($('#fe2').is(':checked'))
				{
					var currentEmployeeNo=$("#empfe2").val();
					var senNo=$("#seniorityNumb").val();
					if(currentEmployeeNo==null || trim(currentEmployeeNo)=='')
					{
						$('#divErrorMsg_top_Curr_Emp').append("&#149; "+resourceJSON.msgEnterEmployeeNo+"</BR>");
						$('#empfe2').css("background-color","#F5E7E1");
						cnt_divErrorMsg_top_Curr_Emp++;
					}else if(document.getElementById("districtIdForDSPQ").value==1200390 && (currentEmployeeNo!=null || trim(currentEmployeeNo)!='') && isNaN(currentEmployeeNo)){
						$('#divErrorMsg_top_Curr_Emp').append("&#149; Employee Number is only a numerical value. Please enter the correct information, or contact MDCPS for your employee number</BR>");
						$('#empfe2').css("background-color","#F5E7E1");
						cnt_divErrorMsg_top_Curr_Emp++;
					}
					else
					{
						$('#empfe2').css("background-color","");
						if(!$('input[name=rdCEmp]').is(":checked"))
						{
							$('#errFormerEmployee').append("&#149; "+resourceJSON.msgStaffMemtype+"</BR>");
							cnt_divErrorMsg_top_Curr_Emp++;
						}
					}
					
				}
				if(cnt_divErrorMsg_top_Curr_Emp!=0)
				{
					$('#divErrorMsg_top_Curr_Emp_header').show();
					$('#divErrorMsg_top_Curr_Emp').show();
					iErrorCount++;
				}
				else
				{
					$('#divErrorMsg_top_Curr_Emp_header').hide();
					$('#divErrorMsg_top_Curr_Emp').hide();
				}
			}
			
			// ******** End *************************
			
			
			var callForwardCount=document.getElementById("callForwardCount").value;
			var icallForwardCount = parseInt(callForwardCount);
			icallForwardCount=icallForwardCount+1;
			document.getElementById("callForwardCount").value=icallForwardCount;
			//alert("E callForwardCount "+callForwardCount +" icallForwardCount "+icallForwardCount);
			
			
			//**************************************************************************************
			// ********* Resume ******************
			resume_source=portfolio[6];
			if((document.getElementById("districtIdForDSPQ").value=="804800" && $('#jobcategoryDsp').val().trim().indexOf("Hourly") !=-1) || ($("#headQuaterIdForDspq").val()!=null && $("#headQuaterIdForDspq").val()==1)){
				resume_config=false;
				$("#requiredRessume").hide();
			}else if(document.getElementById("districtIdForDSPQ").value==7800049 && formeremployee_config==true && $('#jobcategoryDsp').val().trim()=="Personnel enseignant"){
				resume_config=false;
				$("#requiredRessume").hide();
			}
			if(data!=null && data.ressumeOptional==false){
				resume_config=false;
			}
			if(resume_config==true && resume_source==0)
			{
				$('#divErrorMsg_dynamicPortfolio').append("&#149; "+resourceJSON.msgplzProviderecentresume+" ("+resourceJSON.msgPdfMsWordJpeg+")");
				iErrorCount++;
				document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
				
				if(iErrorCount > 0 && source=='level2')
				{
					hideLoadingDiv_DSPQ();
					hideAllPortfolioModel();
					var threadCount=document.getElementById("threadCount").value;
					var returnThreadCount=document.getElementById("returnThreadCount").value;
					var callForwardCount_temp=document.getElementById("callForwardCount").value;
					
					//if(returnThreadCount >= threadCount)
					//{
						try { $('#topErrorMessageDSPQ').modal('show'); } catch (e) {}
						//alert("1 threadCount :: "+threadCount+" , returnThreadCount :: "+returnThreadCount +" icallForwardCount "+icallForwardCount );
					//}
				}
				
			}
			else
			{
				if(iErrorCount==0 && source=='level2') // ********* getDistrictSpecificQuestion ******************
				{
					
					var jobId=document.getElementById("jobId").value;
					if(jobId > 0)
					{
						var savejobFlag=document.getElementById("savejobFlag").value;
						if(savejobFlag==1)
						{
							if(applyFlag)
							{
								//getDistrictSpecificQuestion();
								hideAllPortfolioModel();
								hideLoadingDiv_DSPQ();
								setDspqFlag(jobId);
								var threadCount=document.getElementById("threadCount").value;
								var returnThreadCount=document.getElementById("returnThreadCount").value;
								
								//if(returnThreadCount >= threadCount)
								//{
									getDistrictQuestionsSet(jobId);
									try{$('#jobApplyOrNot').modal('hide');}catch(e){}
									//alert("2 threadCount :: "+threadCount+" , returnThreadCount :: "+returnThreadCount +" icallForwardCount "+icallForwardCount);
								//}
							}else
							{
									hideAllPortfolioModel();
									
									var threadCount=document.getElementById("threadCount").value;
									var returnThreadCount=document.getElementById("returnThreadCount").value;
									
									//if(returnThreadCount >= threadCount)
									//{
										$('#myModalDASpecificQuestions').modal('hide');
										if($("#perform").val()!="P")
										try{$('#jobApplyOrNot').modal('show');}catch(e){}
										//alert("3 threadCount :: "+threadCount+" , returnThreadCount :: "+returnThreadCount +" icallForwardCount "+icallForwardCount);
									//}
							}
							
						}
						else
						{
							document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
						}
					}
				}
				else if(iErrorCount > 0 && source=='level2')
				{
					hideAllPortfolioModel();
					hideLoadingDiv_DSPQ();
					
					var threadCount=document.getElementById("threadCount").value;
					var returnThreadCount=document.getElementById("returnThreadCount").value;
					
					//if(returnThreadCount >= threadCount)
					//{
						try { $('#topErrorMessageDSPQ').modal('show'); } catch (e) {}
						//alert("4 threadCount :: "+threadCount+" , returnThreadCount :: "+returnThreadCount +" icallForwardCount "+icallForwardCount);
					//}

				}
				else
				{
					document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
				}
			}
		}
	});
}
function getSubjectByDistrictForDSPQ()
{
	var districtId	=document.getElementById("districtIdForDSPQ").value;
	var teacherId	=document.getElementById("teacherIdForDSPQ").value;
	DWRAutoComplete.getSubjectByDistrictForDSPQ(teacherId,districtId,{ 
		async: true,		
		callback: function(data)
		{
			document.getElementById("subjectShowDiv").innerHTML=data;
		}
	});
}
/*function hideAllPortfolioModel()
{
	//try { $('#myModalCL').modal('hide'); } catch (e) {}
	try { $('#myModalDymanicPortfolio').modal('hide'); } catch (e) {}
}*/

function setDspqFlag(jobId)
{
	DistrictPortfolioConfigAjax.setDspqFlag(jobId,{
		async: true,
		errorHandler:handleError,
		callback: function(data){}
	});
}

/*function showschoollist12()
{
	if(document.getElementById("districtIdForDSPQ").value==7800292){
	var jobId=document.getElementById("jobId").value;
	var districtId=document.getElementById("districtIdForDSPQ").value;
		DistrictPortfolioConfigAjax.schoolpreference(jobId,districtId,{
			async: true,
			errorHandler:handleError,
			callback: function(data){
			document.getElementById("showschoollistdwr").innerHTML=data;
		}
		});
	}
}
function showschoollist(value)
{
	var jobId=document.getElementById("jobId").value;
	if(value=='1'){
		$('#showschoollist').show();
	}
	else
	{
		$('#showschoollist').hide();
	}
}*/