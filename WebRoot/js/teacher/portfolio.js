var xmlHttp=null;
var txtBgColor="#F5E7E1";
function createXMLHttpRequest()
{
	if (window.ActiveXObject)
    	xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
   	else if (window.XMLHttpRequest)
		xmlHttp = new XMLHttpRequest();
        else
        	alert(resourceJSON.msgAjaxNotSupported);
}
/*========  For Handling session Time out Error ===============*/
function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert(resourceJSON.msgServerErr+": "+exception.javaClassName);}
}
function getDivNo(divNo)
{
	$('#divNo').val(divNo);
}
function showPicture(teacherId,imgName,height,width){

	document.getElementById("screen").innerHTML="";
	$('#screen').css("width",width);
	$('#screen').css("height",height);
	TeacherInfotAjax.showFile(teacherId,imgName,{ 
		async: false,
		callback: function(data)
		{
		$('#loadingDiv').hide();
		document.getElementById('identityVPicture').value=imgName;
		document.getElementById("takeapic").style.display='inline';
		document.getElementById("browsepic").style.display='none';
		document.getElementById("takeapicweb").style.display='none';
		document.getElementById("screen").innerHTML="<img src=\""+data+"\">";
		},
		errorHandler:handleError 
	});
}
function showPictureWeb(){
	var teacherId=document.getElementById('teacherId').value;
	TeacherInfotAjax.showPictureWeb(teacherId,{ 
		async: false,
		callback: function(data)
		{
			document.getElementById("teacherPicture").innerHTML = ""; 
			document.getElementById("teacherPicture").innerHTML="<img src=\""+data+"\"  >";
		},
		errorHandler:handleError 
	});
}
function SubmitForm() {
	var documentUpload	=	document.getElementById("identityVerificationPicture").value;
	var ext = documentUpload.substr(documentUpload.lastIndexOf('.') + 1).toLowerCase();	
	$('#errorPic').empty();
	var fileSize = 0;
	if ($.browser.msie==true)
 	{	
	    fileSize = 0;	   
	}
	else
	{
		if(documentUpload.files!=undefined)
		{
			fileSize = documentUpload.files[0].size;
		}
	}
	
	if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png')){
		$('#errorPic').show();
		$('#errorPic').append("&#149; "+resourceJSON.msgacceptableidentification+"<br>");
	}else if(fileSize>=10485760){
		$('#errorPic').show();
		$('#errorPic').append("&#149; "+resourceJSON.msgfilesizelessthan+"<br>");
	}else{
		$('#errorPic').empty();
		$('#loadingDiv').show();
		var oForm = document.getElementById("frmIdentityVerificationUpload");
	    if (oForm){
	        oForm.submit(); 
	    }
	    else {
	        alert(resourceJSON.msgDEBUGelement);
	    }
	}
}

function chkForEnterSavePersonalInfo(evt)
{
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==13)
	{
		validatePersonalInfo();
	}	
}
function tryAgain(){
	document.getElementById("takeapic").style.display='none';
	document.getElementById("browsepic").style.display='inline';
	document.getElementById("takeapicweb").style.display='none';
	document.getElementById("identityVerificationPicture").value="";
	var screen =  $('#screen');
	screen.html(
			webcam.get_html(screen.width(), screen.height())
	);
}

function takeAPicture(){
	document.getElementById("usepicweb").style.display='inline';
	document.getElementById("takeapicweb").style.display='none';
	document.getElementById("browsepic").style.display='none';
}

function tryAgainWeb(){
	document.getElementById("usepicweb").style.display='none';
	document.getElementById("takeapicweb").style.display='inline';
	document.getElementById("browsepic").style.display='inline';
}

function showTeacherPicture(teacherId,imgName){
	TeacherInfotAjax.showFile(teacherId,imgName,{ 
		async: false,
		callback: function(data)
		{
			document.getElementById("teacherPicture").innerHTML = "";
			if(data!=''){
				document.getElementById("teacherPicture").innerHTML="<img src=\""+data+"\"  >";
			}
		},
		errorHandler:handleError 
	});
}

function savePicture(){
	var identityVPicture=document.getElementById('identityVPicture').value;
	var teacherId=document.getElementById('teacherId').value;
	
	TeacherInfotAjax.savePicture(teacherId,identityVPicture,{ 
		async: false,
		callback: function(data)
		{
		document.getElementById("screen").innerHTML="";
		var screen =  $('#screen');
		screen.html(
				webcam.get_html(screen.width(), screen.height())
		);
		document.getElementById("takeapic").style.display='none';
		document.getElementById("browsepic").style.display='inline';
		document.getElementById("teacherPicture").innerHTML="<img src=\""+data+"\">";
		},
		errorHandler:handleError 
	});
	
}

function trim(s)
{
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}
function showDiv(divId)
{
	var tempDivId;
	for(var j=0;j<divArray.length;j++)
	{
		tempDivId=document.getElementById(divArray[j]).id;
		document.getElementById(tempDivId).style.display="none";
	}
	if(divId!=null)
	{
		document.getElementById(divId).style.display="block";
	}
}
function showDivMsg(divId,msgArray)
{
	var tempDivId;
	for(var j=0;j<msgArray.length;j++)
	{
		tempDivId=document.getElementById(msgArray[j]).id;
		document.getElementById(tempDivId).style.display="none";
	}
	if(divId!=null)
	{
		document.getElementById(divId).style.display="block";
	}
}
function isEmailAddress(str) 
{
	var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
	return emailPattern.test(str);
}
function deleteData(delUrl)
{
	if(window.confirm(resourceJSON.cnfMsgDelete))
	{
		var url = delUrl+"&td="+new Date().getTime();
		window.location.href=url;
	}
}


function showHideBankDiv(stBank,stChequeDD,stButton)
{
	document.getElementById("divBankList").style.display=stBank;
	document.getElementById("divChequeDD").style.display=stChequeDD;
	document.getElementById("divPayButton").style.display=stButton;
}

function isNumber(field) {
	var re = /^[0-9-'.'-',']*$/;
	//var re =/^(0\.|[1-9]\d*\.)d{2}/ 
	if (!re.test(field.value)) {
//		alert('Value must be all numberic charcters, including "." or "," non numerics will be removed from field!');
		field.value = field.value.replace(/[^0-9-'.'-',']/g,"");
	}
}
function checkForInt(evt) {

	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==8)
		return true;

	return( (charCode >= 48 && charCode <= 57)||(charCode==13) );
}
function setFocus(id)
{
	if(id!="")
		document.getElementById(id).focus();
}

function isEmailAddress(str) 
{
	var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
	return emailPattern.test(str);
}

function chkPwdPtrn(str)
{
	var pat1 = /[^a-zA-Z]/g;//If any spcl char find it return true otherwise false
	var pat2 = /[a-zA-Z]/g;//If any alphabet found it return true otherwise false
	//alert(pat1.test(str) && pat2.test(str))
	return pat1.test(str) && pat2.test(str);
}


function validatePersonalInfo()
{
		
	
	var firstName = document.getElementById("firstName");
	var lastName = document.getElementById("lastName");
	var addressLine1 = document.getElementById("addressLine1");
	var addressLine2 = document.getElementById("addressLine2");
	var zipCode = document.getElementById("zipCode");
	
	var stateId = document.getElementById("stateId");
	var cityId = document.getElementById("cityId");
	var countryId = document.getElementById("countryId");
	
	var otherstate = document.getElementById("otherState");
	var othercity = document.getElementById("otherCity");
	
	/*if(countryId.value=='')
	{
		alert("Set USA Country ");	
		document.getElementById("ctry223").selected=true;
	}
	else
	{
		alert("ALready Set Country ");
	}*/
	
	
	var cnt=0;
	var focs=0;	
	$('#errordiv').empty();
	$('#divServerError').css("background-color","");
	
	$('#firstName').css("background-color","");
	$('#lastName').css("background-color","");
	$('#addressLine1').css("background-color","");
	$('#addressLine2').css("background-color","");
	$('#zipCode').css("background-color","");
	$('#stateId').css("background-color","");
	$('#cityId').css("background-color","");
	$('#countryId').css("background-color","");
	$('#otherCity').css("background-color","");
	$('#otherState').css("background-color","");
	
	var phoneNumber=""; //document.getElementById("phoneNumber").value;
	var mobileNumber=""; //document.getElementById("phoneNumber").value;
	var typeOfPhonvalue=$("#typeOfPhonId").val();
	if(typeOfPhonvalue==2){
		var p1=$("#UsPhonNumnerId1").val().trim();
		//var p2=$("#UsPhonNumnerId2").val().trim();
		var p3=$("#UsPhonNumnerId3").val().trim();
		
			phoneNumber=p1+p3;
		
		document.getElementById("phoneNumber").value=phoneNumber;
	}else{
	var phoneNumber1=document.getElementById("phoneNumber1").value;
	var phoneNumber2=document.getElementById("phoneNumber2").value;
	var phoneNumber3=document.getElementById("phoneNumber3").value;
	
	if(phoneNumber1!="" && phoneNumber2!="" && phoneNumber3!="")
	{
		if(phoneNumber1.length==3 && phoneNumber2.length==3 && phoneNumber3.length==4)
		{
			phoneNumber=phoneNumber1+phoneNumber2+phoneNumber3;
			document.getElementById("phoneNumber").value=phoneNumber;
		}
	}
	else if(phoneNumber1!="" && phoneNumber2!="" && phoneNumber3!="")
	{
		document.getElementById("phoneNumber").value="";
	}
	}
	
	var mobileNumber1=document.getElementById("mobileNumber1").value;
	var mobileNumber2=document.getElementById("mobileNumber2").value;
	var mobileNumber3=document.getElementById("mobileNumber3").value;
	
	if(mobileNumber1!="" && mobileNumber2!="" && mobileNumber3!="")
	{
		if(mobileNumber1.length==3 && mobileNumber2.length==3 && mobileNumber3.length==4)
		{
			mobileNumber=mobileNumber1+mobileNumber2+mobileNumber3;
			document.getElementById("mobileNumber").value=mobileNumber;
		}
	}
	else if(mobileNumber1=="" && mobileNumber2=="" && mobileNumber3=="")
	{
		document.getElementById("mobileNumber").value="";
	}
	
	
	if(trim(firstName.value)=="")
	{
		$('#errordiv').append("&#149; "+resourceJSON.PlzEtrFirstName +"<br>");
		if(focs==0)
			$('#firstName').focus();
		
		$('#firstName').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	if(trim(lastName.value)=="")
	{
		$('#errordiv').append("&#149; "+resourceJSON.PlzEtrLastName +"<br>");
		if(focs==0)
			$('#lastName').focus();
		
		$('#lastName').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	if(trim(addressLine1.value)=="")
	{
		$('#errordiv').append("&#149; "+resourceJSON.msgaddresslinefirst+"<br>");
		if(focs==0)
			$('#addressLine1').focus();
		
		$('#addressLine1').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	if(trim(countryId.value)=="")
	{
		$('#errordiv').append("&#149; "+resourceJSON.msgPleaseaCountry+"<br>");
		if(focs==0)
			$('#countryId').focus();
		
		$('#countryId').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	else
	{
		//alert("countryId.value "+countryId.value);
		if(countryId.value=='223')
		{
			if(trim(stateId.value)=="")
			{
				$('#errordiv').append("&#149; "+resourceJSON.msgPleaseselectaState+"<br>");
				if(focs==0)
					$('#stateId').focus();
				
				$('#stateId').css("background-color",txtBgColor);
				cnt++;focs++;
			}
			if(trim(cityId.value)=="")
			{
				$('#errordiv').append("&#149; "+resourceJSON.msgPleaseselectaCity+"<br>");
				if(focs==0)
					$('#cityId').focus();
				
				$('#cityId').css("background-color",txtBgColor);
				cnt++;focs++;
			}
		}
		else
		{
			if(	$("#stateId").is(':visible'))
			{
				if(trim(stateId.value)=="")
				{
					$('#errordiv').append("&#149; "+resourceJSON.msgPleaseselectaState+"<br>");
					if(focs==0)
						$('#stateId').focus();
					
					$('#stateId').css("background-color",txtBgColor);
					cnt++;focs++;
				}
				
				if(trim(othercity.value)=="")
				{
					$('#errordiv').append("&#149; "+resourceJSON.msgentercity+"<br>");
					if(focs==0)
						$('#otherCity').focus();
					
					$('#otherCity').css("background-color",txtBgColor);
					cnt++;focs++;
				}
			}
			else
			{
				if(trim(otherstate.value)=="")
				{
					$('#errordiv').append("&#149; "+resourceJSON.msgenterstate+"<br>");
					if(focs==0)
						$('#otherState').focus();
					
					$('#otherState').css("background-color",txtBgColor);
					cnt++;focs++;
				}
				
				if(trim(othercity.value)=="")
				{
					$('#errordiv').append("&#149; "+resourceJSON.msgentercity+"<br>");
					if(focs==0)
						$('#otherCity').focus();
					
					$('#otherCity').css("background-color",txtBgColor);
					cnt++;focs++;
				}
			}
			
			/*if(trim(otherstate.value)=="")
			{
				$('#errordiv').append("&#149; Please enter State<br>");
				if(focs==0)
					$('#otherState').focus();
				
				$('#otherState').css("background-color",txtBgColor);
				cnt++;focs++;
			}
			
			if(trim(othercity.value)=="")
			{
				$('#errordiv').append("&#149; Please enter City<br>");
				if(focs==0)
					$('#otherCity').focus();
				
				$('#otherCity').css("background-color",txtBgColor);
				cnt++;focs++;
			}*/
			
		}
		
		
	}
	
	if(trim(zipCode.value)=="")
	{
		$('#errordiv').append("&#149; "+resourceJSON.msgenterzipcode+"<br>");
		if(focs==0)
			$('#zipCode').focus();
		
		$('#zipCode').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	else if(resourceJSON.locale=="en")
	{
		if (!/^\d{5}$/.test(zipCode.value))
		{			
			$('#errordiv').append("&#149; "+resourceJSON.msgvalidzipcode+"<br>");			
			if(focs==0)
				$('#zipCode').focus();
			
			$('#zipCode').css("background-color",txtBgColor);
			cnt++;focs++;
		}
	}	
	
	
	
	if(cnt==0)
	{
		var oForm = document.getElementById("teacherpersonalinfoform");
	    if (oForm){
	        oForm.submit(); 
	    }
		return true;
	}
	else
	{
		$('#errordiv').show();
		return false;
	}
}
function delay(sec){
	var starttime = new Date().getTime();
	starttime = starttime+sec;
	while(true){
		if(starttime< new Date().getTime()){
			break;
		}
	}
}
function changeCityStateByZip()
{	
	chageStateAndCityByZipCode("stateId","cityId","loadingDiv");
	/*if($("#divUSAState").is(':visible') && $("#divUSACity").is(':visible'))
	{
		var zipCode = document.getElementById("zipCode");
		
		$('#loadingDiv').show();
		delay(1000);
		var res;
		createXMLHttpRequest();  
		queryString = "findcitybyzip.do?zipCode="+zipCode.value+"&dt="+new Date().getTime();
		xmlHttp.open("POST", queryString, true);
		xmlHttp.onreadystatechange = function()
		{
			if(xmlHttp.readyState == 4)
			{
				if(xmlHttp.status==200)	
				{	
					var res = xmlHttp.responseText.split("|");	
					if(res.length!=1)
					{
						var cityId = trim(res[0].toString());
						var stateId = trim(res[1].toString());
						document.getElementById("st"+stateId).selected=true;
						
						document.getElementById('stateId').onchange();									
						document.getElementById("ct"+cityId).selected=true;
					}
					$('#loadingDiv').hide();
				}			
				else
				{
					alert(resourceJSON.msgServerErr);
				}
			}
		}
		xmlHttp.send(null);	
	}*/
}

function changeCityStateByZipForDSPQOld()
{	
	if($("#divUSAState").is(':visible') && $("#divUSACity").is(':visible'))
	{
		var zipCode = document.getElementById("zipCode");
		
		$('#loadingDivWait').show();
		delay(1000);
		var res;
		createXMLHttpRequest();  
		queryString = "findcitybyzip.do?zipCode="+zipCode.value+"&dt="+new Date().getTime();
		xmlHttp.open("POST", queryString, true);
		xmlHttp.onreadystatechange = function()
		{
			if(xmlHttp.readyState == 4)
			{
				if(xmlHttp.status==200)	
				{	
					var res = xmlHttp.responseText.split("|");	
					if(res.length!=1)
					{
						var cityId = trim(res[0].toString());
						var stateId = trim(res[1].toString());
						try
						{
						document.getElementById("st__"+stateId).selected=true;					
						document.getElementById('stateIdForDSPQ').onchange();			
						document.getElementById("ct"+cityId).selected=true;
						}
						catch(e)
						{}
					}
					$('#loadingDivWait').hide();
				}			
				else
				{
					alert(resourceJSON.msgServerErr);
				}
			}
		}
		xmlHttp.send(null);	
	}
}

function changeCityStateByZipForDSPQPr()
{	
	if($("#divUSAStatePr").is(':visible') && $("#divUSACityPr").is(':visible'))
	{
		var zipCode = document.getElementById("zipCodePr");
		$('#loadingDivWait').show();
		delay(1000);
		var res;
		createXMLHttpRequest();  
		queryString = "findcitybyzip.do?zipCode="+zipCode.value+"&dt="+new Date().getTime();
		xmlHttp.open("POST", queryString, true);
		xmlHttp.onreadystatechange = function()
		{
			if(xmlHttp.readyState == 4)
			{
				if(xmlHttp.status==200)	
				{	
					var res = xmlHttp.responseText.split("|");	
					if(res.length!=1)
					{
						var cityId = trim(res[0].toString());
						var stateId = trim(res[1].toString());
						try
						{
						document.getElementById("st_Pr"+stateId).selected=true;					
						document.getElementById('stateIdForDSPQPr').onchange();			
						document.getElementById("ctPr"+cityId).selected=true;
						}
						catch(e)
						{}
					}
					$('#loadingDivWait').hide();
				}			
				else
				{
					alert("server Error");
				}
			}
		}
		xmlHttp.send(null);	
	}
}

function getCityListByState()
{
	changeCityByStateIdAndzipCode("stateId","cityId","zipCode","loadingDiv");
	/*$('#loadingDiv').show();	
	var stateId = document.getElementById("stateId");
	
	var res;
	createXMLHttpRequest();
	queryString = "findcityhtmlbystate.do?stateId="+stateId.value+"&dt="+new Date().getTime();
	xmlHttp.open("POST", queryString, false);
	xmlHttp.onreadystatechange = function()
	{
		if(xmlHttp.readyState == 4)
		{
			if(xmlHttp.status==200)	
			{
				
				//alert(xmlHttp.responseText)
				//alert(document.getElementById("divSelectCity")+">>>>")		
				while (document.getElementById("cityId").childNodes.length >= 1 )
				{
				    document.getElementById("cityId").removeChild(document.getElementById("cityId").firstChild);       
				}
				//document.getElementById("cityId").innerHTML=xmlHttp.responseText;
				//alert(xmlHttp.responseText)
				var optAr = xmlHttp.responseText.split("##");
				var newOption;
				
				newOption = document.createElement('option');
			    newOption.text="Select City";
			    newOption.id="slcty";
			    newOption.value="";
			   	document.getElementById('cityId').options.add(newOption);
				for(var i=0;i<optAr.length-1;i++)
				{
				    newOption = document.createElement('option');
				    newOption.id=optAr[i].split("$$")[0];
				    newOption.value=optAr[i].split("$$")[1];
				    newOption.text=optAr[i].split("$$")[2];
				    //document.getElementById("cityId").appendChild(newOption);
				    document.getElementById('cityId').options.add(newOption);
				}
				$('#loadingDiv').hide();
			}else{
				alert(resourceJSON.msgServerErr);
			}
		}
	}
	xmlHttp.send(null);	*/
}
function getCityListByStateForDSPQOld()
{
	$('#loadingDiv').show();	
	var stateId = document.getElementById("stateIdForDSPQ");
	
	var res;
	createXMLHttpRequest();
	queryString = "findcityhtmlbystate.do?stateId="+stateId.value+"&dt="+new Date().getTime();
	xmlHttp.open("POST", queryString, false);
	xmlHttp.onreadystatechange = function()
	{
		if(xmlHttp.readyState == 4)
		{
			if(xmlHttp.status==200)	
			{
				
				//alert(xmlHttp.responseText)
				//alert(document.getElementById("divSelectCity")+">>>>")		
				while (document.getElementById("cityIdForDSPQ").childNodes.length >= 1 )
				{
				    document.getElementById("cityIdForDSPQ").removeChild(document.getElementById("cityIdForDSPQ").firstChild);       
				}
				//document.getElementById("cityId").innerHTML=xmlHttp.responseText;
				//alert(xmlHttp.responseText)
				var optAr = xmlHttp.responseText.split("##");
				var newOption;
				
				newOption = document.createElement('option');
			    newOption.text=resourceJSON.msgSelectCity;
			    newOption.value="";
			    newOption.id="slcty";
			   	document.getElementById('cityIdForDSPQ').options.add(newOption);
				for(var i=0;i<optAr.length-1;i++)
				{
				    newOption = document.createElement('option');
				    newOption.id=optAr[i].split("$$")[0];
				    newOption.value=optAr[i].split("$$")[1];
				    newOption.text=optAr[i].split("$$")[2];
				    //document.getElementById("cityId").appendChild(newOption);
				    document.getElementById('cityIdForDSPQ').options.add(newOption);
				}
				$('#loadingDiv').hide();
			}else{
				alert(resourceJSON.msgServerErr);
			}
		}
	}
	xmlHttp.send(null);	
}

function getCityListByStateForDSPQPr()
{
	$('#loadingDiv').show();	
	var stateId = document.getElementById("stateIdForDSPQPr");
	
	var res;
	createXMLHttpRequest();
	queryString = "findcityhtmlbystate.do?stateId="+stateId.value+"&dt="+new Date().getTime();
	xmlHttp.open("POST", queryString, false);
	xmlHttp.onreadystatechange = function()
	{
		if(xmlHttp.readyState == 4)
		{
			if(xmlHttp.status==200)	
			{
				
				//alert(xmlHttp.responseText)
				//alert(document.getElementById("divSelectCity")+">>>>")				
				while (document.getElementById("cityIdForDSPQPr").childNodes.length >= 1 )
				{
				    document.getElementById("cityIdForDSPQPr").removeChild(document.getElementById("cityIdForDSPQPr").firstChild);
				}
				
				//document.getElementById("cityId").innerHTML=xmlHttp.responseText;
				//alert(xmlHttp.responseText)
				var optAr = xmlHttp.responseText.split("##");
				var newOption;
				
				newOption = document.createElement('option');
			    newOption.text="Select City";
			    newOption.value="";
			    newOption.id="slctyPr";
			   	document.getElementById('cityIdForDSPQPr').options.add(newOption);
				for(var i=0;i<optAr.length-1;i++)
				{
				    newOption = document.createElement('option');
				    newOption.id=optAr[i].split("$$")[0];
				    newOption.value=optAr[i].split("$$")[1];
				    newOption.text=optAr[i].split("$$")[2];
				    //document.getElementById("cityId").appendChild(newOption);
				    document.getElementById('cityIdForDSPQPr').options.add(newOption);
				}
				$('#loadingDiv').hide();
			}else{
				alert("server Error");
			}
		}
	}
	xmlHttp.send(null);	
}
function getStateByCountry(stateModuleFlag)
{
	var countryId = document.getElementById("countryId").value;
	if(countryId!='')
	{
		StateAjax.getStateByCountry(countryId,stateModuleFlag,{ 
			async: false,
			callback: function(data)
			{
				if(data[0]!="")
				{
					document.getElementById("countryCheck").value=1;
					if(stateModuleFlag=="dspq")
					{
						$("#stateIdForDSPQ").html(data[0]);
						if(!(countryId==data[3]))
						{
							document.getElementById("slst").selected=true;
							document.getElementById("slcty").selected=true;
						}
					}
					else if(stateModuleFlag=="portfolio")
					{
						$("#stateId").html(data[0]);
						
						if(!(countryId==data[3]))
						{
							document.getElementById("slst").selected=true;
							document.getElementById("slcty").selected=true;
						}
					}
					document.getElementById("divUSAState").style.display="inline";
					document.getElementById("divotherstate").style.display="none";
					
					if(countryId!=223)
					{
						document.getElementById("divUSACity").style.display="none";
						document.getElementById("divothercity").style.display="inline";
						
						if(countryId==data[3])
							document.getElementById("otherCity").value=data[2];
						else
							document.getElementById("otherCity").value="";
					}
					else
					{
						document.getElementById("divUSACity").style.display="inline";
						document.getElementById("divothercity").style.display="none";
						getCityListByStateSet(data[0]);
						
						if(countryId==data[3])
						{
							if(data[4]!=null && data[4]!="")
								document.getElementById("ct"+data[4]).selected=true;
							else
								document.getElementById("slcty").selected=true;
						}
					}
				}
				else
				{
					document.getElementById("countryCheck").value=0;
					document.getElementById("divUSAState").style.display="none";
					document.getElementById("divUSACity").style.display="none";
					
					if(countryId==data[3])
					{
						document.getElementById("otherState").value=data[1];
						document.getElementById("otherCity").value=data[2];
					}
					else
					{
						document.getElementById("otherState").value="";
						document.getElementById("otherCity").value="";
					}
					document.getElementById("divotherstate").style.display="inline";
					document.getElementById("divothercity").style.display="inline";
				}
			},
		});
	}
	
	if(countryId=='')
	{
		if(resourceJSON.locale=="fr")
		{
			countryId='38';
			document.getElementById("ctry38").selected=true;
		}else
		{
			countryId='223';
			document.getElementById("ctry223").selected=true;
		}
		StateAjax.getStateByCountry(countryId,stateModuleFlag,{ 
			async: false,
			callback: function(data)
			{
				if(data[0]!="")
				{
					document.getElementById("countryCheck").value=1;
					if(stateModuleFlag=="dspq")
					{
						$("#stateIdForDSPQ").html(data[0]);
						if(!(countryId==data[3]))
						{
							document.getElementById("slst").selected=true;
							document.getElementById("slcty").selected=true;
						}
							
					}
					else if(stateModuleFlag=="portfolio")
					{
						$("#stateId").html(data[0]);
						if(!(countryId==data[3]))
						{
							document.getElementById("slcty").selected=true;
							document.getElementById("slcty").selected=true;
						}
					}
					document.getElementById("divUSAState").style.display="inline";
					document.getElementById("divUSACity").style.display="inline";
					document.getElementById("divotherstate").style.display="none";
					document.getElementById("divothercity").style.display="none";
				}
				else
				{
					document.getElementById("countryCheck").value=0;
					document.getElementById("divUSAState").style.display="none";
					document.getElementById("divUSACity").style.display="none";
					
					if(countryId==data[3])
					{
						document.getElementById("otherState").value=data[1];
						document.getElementById("otherCity").value=data[2];
					}
					else
					{
						document.getElementById("otherState").value="";
						document.getElementById("otherCity").value="";
					}
					document.getElementById("divotherstate").style.display="inline";
					document.getElementById("divothercity").style.display="inline";
				}
			},
		});
	}
	
	
	/*if(countryId=='')
	{
		document.getElementById("ctry223").selected=true;
		countryId = document.getElementById("countryId").value;
	}*/
	
/*	
	if(countryId=='223')
	{
		document.getElementById("divUSAState").style.display="inline";
		document.getElementById("divUSACity").style.display="inline";
		
		document.getElementById("divotherstate").style.display="none";
		document.getElementById("divothercity").style.display="none";
	}
	else
	{
		//alert("Other countryId "+countryId);
		document.getElementById("divUSAState").style.display="none";
		document.getElementById("divUSACity").style.display="none";
		
		document.getElementById("divotherstate").style.display="inline";
		document.getElementById("divothercity").style.display="inline";
	}*/
}



function chkAffidavit()
{

	var portfolioStatus = document.getElementById("portfolioStatus");
	var isDone = document.getElementById("isDone1");

	
	var cnt=0;
	var focs=0;	
	$('#errordiv').empty();
	if(isDone.checked==false)
	{
		//alert("Please fill your other portfolio pages.")
		$('#errordiv').append("&#149; "+resourceJSON.msgafidevitconfirm+"<br>");
		return false;
	}
	else if(!trim(portfolioStatus.value)=="1")
	{
		/*		 
		$('#errordiv').append("&#149; You have not completed the mandatory information of four tabs of portfolio. " +
		"Please complete the mandatory information to accept the terms of affidavit.<br>");
		*/
		
		$('#errordiv').append(resourceJSON.msgmendetryfotofolio +
				resourceJSON.msgmendetorafidevit +
				resourceJSON.msgvisitfotofolio);
		return false;
	}
	
	else
	{
		return true;	
	}
}
function getCityListByStateSet(data1)
{
	$('#loadingDiv').show();	
	var stateId = document.getElementById("stateId");
	
	var res;
	createXMLHttpRequest();
	queryString = "findcityhtmlbystate.do?stateId="+stateId.value+"&dt="+new Date().getTime();
	xmlHttp.open("POST", queryString, false);
	xmlHttp.onreadystatechange = function()
	{
		if(xmlHttp.readyState == 4)
		{
			if(xmlHttp.status==200)	
			{
				
				//alert(xmlHttp.responseText)
				//alert(document.getElementById("divSelectCity")+">>>>")		
				while (document.getElementById("cityId").childNodes.length >= 1 )
				{
				    document.getElementById("cityId").removeChild(document.getElementById("cityId").firstChild);       
				}
				//document.getElementById("cityId").innerHTML=xmlHttp.responseText;
				//alert(xmlHttp.responseText)
				var optAr = xmlHttp.responseText.split("##");
				var newOption;
				
				newOption = document.createElement('option');
			    newOption.text=resourceJSON.msgSelectCity;
			    newOption.value="";
			    newOption.id="slcty";
			    document.getElementById('cityId').options.add(newOption);
				for(var i=0;i<optAr.length-1;i++)
				{
				    newOption = document.createElement('option');
				    newOption.id="ct"+optAr[i].split("$$")[1]
				    newOption.value=optAr[i].split("$$")[1];
				    newOption.text=optAr[i].split("$$")[2];
				    //document.getElementById("cityId").appendChild(newOption);
				    document.getElementById('cityId').options.add(newOption);
				}
				$('#loadingDiv').hide();
				
				if(data1.cityId!=null)
					document.getElementById("ct"+data1.cityId.cityId).selected=true;
				else
					document.getElementById("slcty").selected=true;
				
			}else{
				alert(resourceJSON.msgServerErr);
			}
		}
	}
	xmlHttp.send(null);	
}

function showEmpNoDiv(radioFlag)
{	
	
	var nextProcess=true;
	$('#errordivCL').empty();
	/*if($("#districtIdForDSPQ").length > 0 && document.getElementById("districtIdForDSPQ").value==4218990 && $("input:checkbox[name=isAffilated]").is(":checked")){
		if($('input:radio[name=staffType]').is(":checked") && radioFlag!=2){
			//callDynamicPortfolio();
			$("#myModalDymanicPortfolio").hide();
			
			$("#affDivPHiL").hide();
			$("#dynamicInfoMsg").html("You have declared yourself as an current Internal Candidate while filling the Cover Letter details but now you are changing your declaration. Please first change the declaration in Cover Letter screen.");
			$("#wrongDivPHiL").show();
			nextProcess=false;
		}else if(!$('input:radio[name=staffType]').is(":checked") && radioFlag==2){
			$("#myModalDymanicPortfolio").hide();
			$("#affDivPHiL").hide();
			$("#dynamicInfoMsg").html("You have declared yourself as an External Candidate while filling the Cover Letter details but now you are changing your declaration. Please first change the declaration in Cover Letter screen.");
			$("#wrongDivPHiL").show();
			nextProcess=false;
		}
		
	}*/
	if(nextProcess==true)
	if(radioFlag==1)
	{
		$("#empfewDiv").hide();
		$("#fe1Div").show();
		$("#fe2Div").hide();
		$("#position6Div").hide();
		
		if($("#districtIdForDSPQ").length > 0 && document.getElementById("districtIdForDSPQ").value==1302010){
			$("#empfewDiv").show();
		}
	}
	else if(radioFlag==2)
	{
		$("#fe1Div").hide();
		$("#fe2Div").show();
		$("#position6Div").hide();
		if($("#districtIdForDSPQ").length > 0 && document.getElementById("districtIdForDSPQ").value==4218990){
			$("#myModalDymanicPortfolio").hide();
			$("#affDivPHiL").show();
			$("#rdCEmp1").hide();
			$("#rdCEmp2").hide();
		}		
	}
	
	//For Martin District
	else if(radioFlag == 'CEmpOfMartin')
	{
		
		$("#currentSubstituteEmployee").hide();
		$("#retiredEmployeeStateofFlorida").hide();
		$("#formalEmployeeOfMartin").hide();
		
		$("#currentEmployeeOfMartin").show();
		
		
	}
	else if(radioFlag == 'FormalEmpOfMartin')
	{
		
		$("#currentSubstituteEmployee").hide();
		$("#retiredEmployeeStateofFlorida").hide();
		$("#currentEmployeeOfMartin").hide();
		
		$("#formalEmployeeOfMartin").show();
				
		
	}
	else if(radioFlag == 'RetiredEmpOfMartin')
	{
		
		$("#currentSubstituteEmployee").hide();
		$("#formalEmployeeOfMartin").hide();
		$("#currentEmployeeOfMartin").hide();
		
		$("#retiredEmployeeStateofFlorida").show();
		
		
	}
	else if(radioFlag == 'CurrentSubstituteEmpOfMartin')
	{
		
		$("#currentSubstituteEmployee").show();
		$("#retiredEmployeeStateofFlorida").hide();
		$("#formalEmployeeOfMartin").hide();
		$("#currentEmployeeOfMartin").hide();
		
	}
	else if(radioFlag ==  'never_emp')
	{
		$("#currentSubstituteEmployee").hide();
		$("#retiredEmployeeStateofFlorida").hide();
		$("#formalEmployeeOfMartin").hide();
		$("#currentEmployeeOfMartin").hide();
	}
	//End
	
	else if(radioFlag==6)
	{
		$("#fe1Div").hide();
		$("#fe2Div").hide();
		$("#position6Div").show();
		if($("#districtIdForDSPQ").length > 0 && document.getElementById("districtIdForDSPQ").value==4218990){
			$("#myModalDymanicPortfolio").hide();
			$("#affDivPHiL").hide();
			$("#rdCEmp1").hide();
			$("#rdCEmp2").hide();
			
			
		}		
	}
	else
	{
		$("#fe1Div").hide();
		$("#fe2Div").hide();
		$("#position6Div").hide();
	}
	if($("#districtIdForDSPQ").length > 0 && document.getElementById("districtIdForDSPQ").value==7800294)
	{
		if(radioFlag==1)
		{
			$("#districtEmailDiv").hide();
			$("#fePosDiv").show();
		}
		else if(radioFlag==2)
		{
			$("#fePosDiv").hide();
			$("#districtEmailDiv").show();
		}
		$('#districtEmail').css("background-color","");
		$('#emppos').css("background-color","");
		validateCandidateType();		
	}
	if($("#districtIdForDSPQ").val()==3904374 && $("#jobCategoryName").val()=="Substitute Teachers")
	{
		$("#backgroundcheckMain").show();
	}
}

function showEmpDate(chkflag)
{
	if(chkflag==0)
	{
		if($('#empchk11').is(':checked'))
			$('#rtDateDiv').show();
		else
			$('#rtDateDiv').hide();
	}
	else if(chkflag==1)
	{
		if($('#empchk12').is(':checked'))
			$("#wtDateDiv").show();
		else
			$("#wtDateDiv").hide();
	}
	else if(chkflag==2)
	{
		if($('#empchk13').is(':checked'))
			$("#rwtDateDiv").show();
		else
			$("#rwtDateDiv").hide();
	}
}

// Get Gender By District
function getGenderByDistrict()
{
	var isMiami=document.getElementById('isMiami').value;
	DistrictPortfolioConfigAjax.getGenderByDistrict(isMiami,{ 
		async: false,
		callback: function(data)
		{
			document.getElementById("genderCheckDiv").innerHTML=data;
		},
	});
}

function getRacedata()
{
	DistrictPortfolioConfigAjax.getRaceData({ 
		async: false,
		callback: function(data)
		{
			document.getElementById("raceData").innerHTML=data;
		},
	});
}



function getPhonDivMethod(){
	var phonType=  $("#typeOfPhonId").val();
	if(phonType==1){
		$("#NonUSNumberId").hide();
		$("#USNumberDivID").show();
		$("#mobilenumberDivId").show();
		
	}else{
		$("#USNumberDivID").hide();
		$("#NonUSNumberId").show();
		$("#mobilenumberDivId").show();
	}
	
}

function changeCityStateByZipForDSPQ()
{	
	chageStateAndCityByZipCode("stateIdForDSPQ","cityIdForDSPQ","loadingDivWait");
}

function chageStateAndCityByZipCode(stateSelectId,citySelectId,loadingDivId){
	//alert('yes======'+stateSelectId+"   "+citySelectId+"    "+loadingDivId);
	if($("#divUSAState").is(':visible') && $("#divUSACity").is(':visible'))
	{
		var zipCode = document.getElementById("zipCode");
		$('#'+loadingDivId).show();
		delay(1000);
		
		if(zipCode.value.trim()!=''){
		var resultText=$.ajax({
			url: "findstateAndcitybyzip.do",
			data : {'zipCode' : zipCode.value,'dt':new Date().getTime()},
			type : 'POST',
	        async: false,
	        beforeSend: function() {$('#'+loadingDivId).show()},
        	complete: function() {$('#'+loadingDivId).hide()},
	        success:function(data){	
	        }
		}).responseText;
		var res = resultText.split("@#@");	
		if(resultText.trim()!='' && res.length>0){
			var stateInfoArray=new Array();
				for(var c=0;c<res.length;c++){
					var stateInfo=res[c].split("#");
					var stateId=stateInfo[0];
					stateInfoArray.push(stateId);
				}
			var stateInfo=res[0].split("#");
			var stateId=stateInfo[0];
			var allCityId=trim(stateInfo[1]).split("@");
			//var cityId = allCityId[0];
			try
			{
				hideShowDropdownOptions(stateSelectId,stateInfoArray,false);
				//document.getElementById("st__"+stateId).selected=true;					
				//document.getElementById('stateIdForDSPQ').onchange();	
				hideShowDropdownOptions(citySelectId,allCityId,false);
				//document.getElementByValue(cityId).selected=true;
			}catch(e){}
		}else{
			hideShowDropdownOptions(stateSelectId,null,true);
		}
		}else if(zipCode.value.trim()==''){
			hideShowDropdownOptions(stateSelectId,null,true);
			hideShowDropdownOptions(citySelectId,null,true);
		}
		$('#'+loadingDivId).hide();
	}
}
function hideShowDropdownOptions(id,arrayStateOrCity,isShow) {
	var isSelect=false;
    var dropdown = document.getElementById(id);
    for(var check=0;check<dropdown.length;check++){
    	var current_value = dropdown.options[check].value;
    	var flag=false;
    	if(isShow)
    		flag=true;
    	else
    		flag=isValidCode(current_value,arrayStateOrCity);
	    if (flag) {
	    	dropdown.options[check].style.display = "block";
	    	if(!isSelect){
	    		//dropdown.options[check].selected=true;
	    		$(dropdown.options[check]).attr('selected','selected');
	    		$('#'+id).trigger("change");
	    		isSelect=true;
	    	}
	    }
	    else {
	    	dropdown.options[check].style.display = "none";
	    }
    }
    
}
function isValidCode(code,codes){
    return ($.inArray(code, codes) > -1);
}
function getCityListByStateForDSPQ()
{
	changeCityByStateIdAndzipCode("stateIdForDSPQ","cityIdForDSPQ","zipCode","loadingDiv");
}
function changeCityByStateIdAndzipCode(stateSelectId,citySelectId,zipCodeId,lodingDivId ){
	$('#'+lodingDivId).show();	
	var stateId = document.getElementById(stateSelectId);
	var zipCode = document.getElementById(zipCodeId).value;
	
	var resultText=$.ajax({
		url: "findcityhtmlbystate.do",
		data : {'stateId':stateId.value,'zipCode' : zipCode,'dt':new Date().getTime()},
		type : 'POST',
        async: false,
        beforeSend: function() {$('#'+lodingDivId).show()},
    	complete: function() {$('#'+lodingDivId).hide()},
        success:function(data){	
        }
	}).responseText;
	while (document.getElementById(citySelectId).childNodes.length >= 1 )
	{
	    document.getElementById(citySelectId).removeChild(document.getElementById(citySelectId).firstChild);       
	}
	var optAr = resultText.split("##");
    var optionHtml="<option id='slcty' value=''>"+resourceJSON.msgSelectCity+"</option>";
	for(var i=0;i<optAr.length-1;i++)
	{
	    optionHtml+="<option id='"+optAr[i].split("$$")[0]+"' value='"+optAr[i].split("$$")[1]+"'>"+optAr[i].split("$$")[2]+"</option>";
	}
	$('#'+citySelectId).html(optionHtml);
	$('#'+lodingDivId).hide();
}
$(function(){
	$('#zipCode').keypress(function(e){
		var keynum = ( e.which ) ? e.which : e.keyCode;
	    if(keynum == 13){
	    	try{
	    	changeCityStateByZipForDSPQ();
	    	}catch(e){}
	    	try{
	    		changeCityStateByZip();
		    }catch(e){}
	    	return false;
	    }
	});
});