//========================================================Added By Ram Nath==============================================================================

/**********************************
     ***********************
	*  @Author Ram Nath  *
   **********************
******************************/

var deviceTypeAndroid=$.browser.device = (/Android|webOS|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent.toLowerCase()));
var deviceType=$.browser.device = (/iPhone|iPad|iPod/i.test(navigator.userAgent.toLowerCase()));
var isSafari = /Safari/.test(navigator.userAgent) && /Apple Computer/.test(navigator.vendor);
var pageOB 		= 	1;
var noOfRowsOB 	= 	10;
var sortOrderStrOB	=	"";
var sortOrderTypeOB=	"";
var txtBgColor="#F5E7E1";
var active="A";
var printData=false;
function getStatusTypeONB(slectedStatus)
{
	//alert('enter=='+slectedStatus);
	active=slectedStatus;
	displayHiredCandidate();
}
$(document).ready(function(){
	setHeader();
	displayHiredCandidate();
	$('#exlId').tooltip();
	$('#printId').tooltip();
	$('#pdfId').tooltip();
});
function searchDisplayHiredCandidate(){
	pageOB 		= 	1;
	displayHiredCandidate();
}
function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert(""+resourceJSON.msgServerErr+": "+exception.javaClassName);}
}

var allInputFieldByNode = new Object();
function getAllInputFieldByNode(k) {
	return allInputFieldByNode[k];
}
allInputFieldByNode['emp']='<label class="col-sm-4 col-md-4 employee"  style="margin-top: 0px;">'+
	'Employee ID<span class="required">*</span> <input type="text" value="" id="employeeId" name="employeeId" class="form-control" maxlength="6" onkeypress="return keydown(event);">'+
	'</label>';
allInputFieldByNode['salary']='<label class="col-sm-4 col-md-4 salary"  style="margin-top: 0px;">'+
	'Salary <input type="text" value="" id="salaryId" name="salary" class="form-control" maxlength="10" onkeypress="return keydownAmount(event,this);">'+
	'</label>'+
	/*'<label class="col-sm-4 col-md-4 salary"  style="margin-top: 0px;">'+
	'Grade<input type="text" value="" id="gradeId" name="grade" class="form-control" maxlength="50">'+
	'</label>'+*/
	'<label class="col-sm-4 col-md-4 salary"  style="margin-top: 0px;">'+
	'Step <input type="text" value="" id="stepId" name="step" class="form-control" maxlength="10" onkeypress="return keydown(event);">'+ 
	'</label>';
allInputFieldByNode['salaryExtra']='<div class="row salaryExtra" style="padding-bottom:15px;">'+
	'<div class="span12 top5 col-md-10 form-group" style="width:100%;">'+
	'<div class="col-md-4 top5" style="padding:0px;">'+
	'<label for="text" style="float:left;margin-right:5px;">Start Date<span class="required">*</span></label>'+
	'<div><input type="text" value="" id="startDateId" name="startDate" class="form-control" maxlength="50" onmouseup="setDateField(this);" style="width:150px;"></div>'+
	'</div>'+
	'<div class="col-md-4 top5" >'+
	'<label for="text" style="float:left;margin-right:5px;">End Date</label>'+
	'<div><input type="text" value="" id="endDateId" name="endDate" class="form-control" maxlength="50" onmouseup="setDateField(this);" style="width:150px;"></div>'+
	'</div>'+
	'</div>'+
	'<div class="span12 top5 col-md-2 form-group">'+
	'<div style="float:left;margin-right:5px;"><input type="checkbox" value="" id="yearRound" name="yearRound" checked="checked" onclick="hideshowDaysWorked();"></div>'+
	'<label for="text" id="yearRoundLabel">Year Round</label>'+
	'</div>'+
	'<div class="span12 top5 col-md-2 ml5 form-group salaryCheck" style="width:100%;">Days Worked:</div>'+
	'<div class="span12 top5 col-lg-8 col-md-12 mb5 form-group salaryCheck">'+
	'<div class="col-md-4">'+
	'<label for="text" style="float:left;margin-right:5px;">Actual Days</label>'+
	'<div><input type="text" value="" id="actualDaysId" name="actualDays" class="form-control" style="width:150px;"></div>'+
	'</div>'+
	'<div class="col-md-4">'+
	'<label for="text" style="float:left;margin-right:5px;">Total Days</label>'+
	'<div><input type="text" value="" id="totalDaysId" name="totalDays" class="form-control" style="width:150px;"></div>'+
	'</div>'+
	'</div>'+
	'</div>';
	/*'<div class="span12 top5 col-sm-2 col-md-2 form-group salaryCheck">'+
	'<label for="text" style="float:left;margin-right:5px;">Total Days</label>'+
	'<div><input type="text" value="" id="totalDaysId" name="totalDays" class="form-control" style="width:150px;"></div>'+
	'</div>';*/

allInputFieldByNode['']='';
var cal =null;
function setDateField(obj){
		if(cal==null){
			cal = Calendar.setup({
		    onSelect: function(cal) { cal.hide() },
		    showTime: true
		});
	}
cal.manageFields($(obj).attr('id'), $(obj).attr('id'), "%m-%d-%Y");
}
var allColorReturn = new Object();
allColorReturn['00882B']=true;
allColorReturn['FF0000']=false;
allColorReturn['FFFF00']=true;
allColorReturn['000000']=false;
allColorReturn['0000CC']=false;
allColorReturn['7F7F7F']=false;
allColorReturn['473F3F']=false;
allColorReturn['FFA500']=false;
allColorReturn['800080']=true;
function getAllColorReturn(k) {
	return allColorReturn[k];
}

var allColorValueReturn = new Object();
allColorValueReturn['00882B']=resourceJSON.lblYes;
allColorValueReturn['FF0000']=resourceJSON.lblNo;
allColorValueReturn['FFFF00']=resourceJSON.lblWaived;
allColorValueReturn['000000']=resourceJSON.lblReprint;
allColorValueReturn['0000CC']=resourceJSON.lblPending;
allColorValueReturn['7F7F7F']=resourceJSON.lblIncomplete;
allColorValueReturn['473F3F']=resourceJSON.lblPrinted;
allColorValueReturn['FFA500']=resourceJSON.lblCHRPending;
allColorValueReturn['800080']='Provided by CDE';
function getAllColorValueReturn(k) {
	return allColorValueReturn[k];
}

var allMap = new Object();
function getAllMap(k) {
   	 return allMap[k];
	}
function replaceAll(find, replace, str) {
    return str.replace(new RegExp(find, 'g'), replace);
  }

function displayHiredCandidate(){
	try{
	allMap = new Object();
	allMap['hdId'] = $('#hdId').val();
	allMap['districtId'] = $('#districtId').val();
	allMap['sortingFlag_ZeroAsc_OneDesc'] = (sortOrderStrOB==""?"0":sortOrderStrOB);
	allMap['sortingField'] = sortOrderTypeOB;
	allMap['pageNo'] = pageOB;
	allMap['noOfRow']=noOfRowsOB;
	allMap['firstName'] = $('#firstName').val();
	allMap['lastName'] = $('#lastName').val();
	allMap['position']=$('#position').val();
	allMap['status']=active;
	allMap['report']='false';
	allMap['estechId']=$('[name="estechId"]').val();
	allMap['schoolId']=$('[name="schoolId"]').val();
	var qqoIcon = ""; 
	//$.each(allMap,function(k,v){alert(v);});
	ONBServiceAjax.displayONBGrid(allMap,{
		preHook:function(){$('#loadingDiv').show()},
		postHook:function(){$('#loadingDiv').hide()},
		async: true,
		dataType: 'json',
		errorHandler:handleError,
		callback:function(data)
		{	
		var districtId1=getAllMap("districtId");
		var sortingFlag_ZeroAsc_OneDesc1=getAllMap("sortingFlag_ZeroAsc_OneDesc")!=null?(getAllMap("sortingFlag_ZeroAsc_OneDesc")==""?0:parseInt(getAllMap("sortingFlag_ZeroAsc_OneDesc"))):0;
		var sortingField1=getAllMap("sortingField");
		var pageNo1=getAllMap("pageNo")!=null?(getAllMap("pageNo")==""?0:parseInt(getAllMap("pageNo"))):0;
		var noOfRow1=getAllMap("noOfRow")!=null?(getAllMap("noOfRow")==""?10:parseInt(getAllMap("noOfRow"))):10;
		if(sortingField1.trim()==""){
			sortingField1="firstName";
			sortingFlag_ZeroAsc_OneDesc1=0;
		}
		//setHeader();
		var sb = "";
		sb+="<table border=0 class='table' id='onboardingHiredCanTable'>";
		sb+="<thead class='bg'>";
		var responseText="";
		$.each(ONBOARDING_HEADER_SORTING,function(k,v){	
			if(k.indexOf("noSort")>-1){
			//Without Sorting Header
			sb+="<th valign='top'>"+v+""+(v.trim()=="Status"?getActionList(active):"")+"</th>";
			}else{
			//Sorting Header
			responseText=responseSorting("onboardingSorting",v,k,k,(sortingField1==k?""+sortingFlag_ZeroAsc_OneDesc1:""),pageNo1);
			sb+="<th valign='top'>"+responseText+"</th>";
			}
		});
		sb+="</thead>";
		var noOfRecordCheck=0;
		var totalRecords=0;
		var candName="",teacherEmail="",postionNumber="",hiredDate="",teacherHistoryId="",teacherId="",jobId="",status="",schoolName="";
		 var inputTextHideOrShow="none",OSColor="7F7F7F";
		 
		$.each(data,function(k,val){
			qqoIcon = "";
			
				if(k==0){totalRecords=val;}else{
				var row=val;
				
				dataExists=true; candName=row[0]+" "+row[1]; teacherEmail=row[2]; postionNumber=row[3]; hiredDate=row[4]; teacherHistoryId=row[5];teacherId=row[6]; jobId=row[7]; status=row[8];schoolName=row[10];
				var candName1=replaceAll("'","\\'",candName);
	            //alert("candName===="+candName+"("+teacherEmail+")"+"  postionNumber=="+postionNumber+" hiredDate=="+hiredDate+" teacherHistoryId=="+teacherHistoryId+" teacherId=="+teacherId+"  jobId=="+jobId);
				//alert('circle=='+row[9]+"  ==  "+row[10]+" ==  "+row[11]);
	            sb+="<tr>";
	            inputTextHideOrShow=getShowHideAndOS(row[11],row[12],row[14],row[15],0);
	            if(status.trim()==resourceJSON.lblSTPeopleSoft.trim())
	            	inputTextHideOrShow='none';
	            //alert(inputTextHideOrShow+"        show");
	           
	            qqoIcon = getThumnb(jobId,row[16],teacherId,districtId1);// For getting Thumb Icon
	            
	            
	            sb+="<td><input type='checkBox' value='"+teacherHistoryId+"' name='hiredCheckBox' style='display:"+inputTextHideOrShow+";'/></td>";
	            sb+="<td><a class=\"profile\" data-placement=\"above\" href=\"javascript:void(0);\" onclick=\"showProfileContentForTeacherForONB(this,"+teacherId+","+jobId+","+(++noOfRecordCheck)+",'Onboarding Dashboard',event);\">"+candName+"</a><br/><a href=\"javascript:void(0);\" onclick=\"getMessageDiv('"+teacherId+"','"+teacherEmail+"','"+jobId+"',0);\">"+"("+teacherEmail+")"+"</a>"+qqoIcon+"</td>";
	            sb+="<td>"+postionNumber+"</td>";	
	            sb+="<td>"+schoolName+"</td>";
	            sb+="<td>"+hiredDate+"</td>";
	            sb+="<td><a href='javascript:void(0)' onclick=\"open_popupONB('Employee ID/BISI','"+teacherHistoryId+"','"+candName1+"','"+row[9].split(',')[0]+"','"+status+"');\"><span class='nobground1' style='font-size: 7px;font-weight: bold;background-color: #"+row[11]+";'></span></a></td>";	
	            //sb+="<td><a href='javascript:void(0)' onclick=\"open_popupONB('BISI','"+teacherHistoryId+"','"+candName1+"','"+row[9].split(',')[2]+"','"+status+"');\"><span class='nobground1' style='font-size: 7px;font-weight: bold;background-color: #"+row[13]+";'></span></a></td>";	
	            sb+="<td><a href='javascript:void(0)' onclick=\"open_popupONB('Physical','"+teacherHistoryId+"','"+candName1+"','"+row[9].split(',')[3]+"','"+status+"');\"><span class='nobground1' style='font-size: 7px;font-weight: bold;background-color: #"+row[14]+";'></span></a></td>";
	            sb+="<td><a href='javascript:void(0)' onclick=\"open_popupONB('Paperwork','"+teacherHistoryId+"','"+candName1+"','"+row[9].split(',')[4]+"','"+status+"');\"><span class='nobground1' style='font-size: 7px;font-weight: bold;background-color: #"+row[15]+";'></span></a></td>";
	            sb+="<td><a href='javascript:void(0)' onclick=\"open_popupONB('Salary','"+teacherHistoryId+"','"+candName1+"','"+row[9].split(',')[1]+"','"+status+"');\"><span class='nobground1' style='font-size: 7px;font-weight: bold;background-color: #"+row[12]+";'></span></a></td>";
	            OSColor=getShowHideAndOS(row[11],row[12],row[14],row[15],1);
	            //alert(OSColor+"        OSColor");
	            sb+="<td><span class='nobground1' style='font-size: 7px;font-weight: bold;background-color: #"+OSColor+";'><span></td>";	
	            sb+="<td>"+status+"</td>";	
	            sb+="</tr>";
	            }
	        });	
		
			 if(totalRecords==0){
				 sb+="<tr><td colspan='8'>"+resourceJSON.lblNRFound+"</td></tr>";
			 }
			sb+="</table>";
			//alert("totalRecords===="+totalRecords);
			sb+=getAlwaysPaginationString(totalRecords,noOfRow1,pageNo1,true,"1000px","setPageSize");
			document.getElementById("panelGrid").innerHTML=sb;
			applyStyleTable();
		}
		});
		}catch(e){alert(e);}
}

	function getShowHideAndOS(empColor,salaryColor,background,paperwork ,keyReturn){
		/*var salaryFlag=getAllColorReturn(salaryColor);
		var empFlag=getAllColorReturn(empColor);
		alert("salaryColor=="+salaryColor+"    empColor=="+empColor);
		alert("salaryFlag=="+salaryFlag+"    empFlag=="+empFlag);*/
		if(getAllColorReturn(empColor) && getAllColorReturn(salaryColor)  && getAllColorReturn(background) && getAllColorReturn(paperwork)){
			if(keyReturn==0){
				return "block";
			}
			if(keyReturn==1){
				return "00882B";
			}
		}else{
			if(keyReturn==0){
				return "none";
			}
			if(keyReturn==1){
				return "7F7F7F";
			}
		}
	}
	function getShowHideAndOSValue(empColor,salaryColor,background,paperwork ,keyReturn){
		if(getAllColorReturn(empColor) && getAllColorReturn(salaryColor) && getAllColorReturn(background) && getAllColorReturn(paperwork)){
			if(keyReturn==0){
				return "block";
			}
			if(keyReturn==1){
				return resourceJSON.lblComplete;
			}
		}else{
			if(keyReturn==0){
				return "none";
			}
			if(keyReturn==1){
				return resourceJSON.lblIncomplete;
			}
		}
	}
	var ONBOARDING_HEADER_SORTING = new Object();
	function setHeader(){
		ONBOARDING_HEADER_SORTING=new Object();
		ONBServiceAjax.getDistrictWiseColumn($('#districtId').val(),{
			preHook:function(){$('#loadingDiv').show()},
			postHook:function(){$('#loadingDiv').hide()},
			async: false,
			dataType: 'json',
			errorHandler:handleError,
			callback:function(data)
			{	
				ONBOARDING_HEADER_SORTING=data;
			}
		});
	}
	
	function getHeader(k) {
   	 return ONBOARDING_HEADER_SORTING[k];
	}

function getActionList(active){
		//String[] actionAll={"","","","New","Send to PeopleSoft"};
		var actionAll = new Object();
		actionAll['A'] =resourceJSON.optAll ;
		actionAll['F'] = resourceJSON.lblFailed;
		//actionAll['Q'] = resourceJSON.lblIntheQueue;
		actionAll['N'] = resourceJSON.lblNew;
		//actionAll['S'] = resourceJSON.lblSTPeopleSoft;
		var dmRecords="";
		dmRecords+="&nbsp;<span class=\"btn-group\"><a data-toggle=\"dropdown\" href=\"javascript:void(0);\"><span class=\"icon-collapse  iconcolorOnr icon-large\"></span></a>";
		dmRecords+="<ul class=\"dropdown-menu pull-left\" style=\"margin-left:-135px;\">";
		$.each(actionAll,function(k,v){
			dmRecords+="<li class='"+(k.trim()==active?"active-link":"")+"'><a href=javascript:void(0); onclick='getStatusTypeONB(\""+k+"\");'>"+v+"</a></li>";
		});
		dmRecords+="</ul></span></span>";
		return dmRecords;
	}

function applyStyleTable()
{
	var columnSpan = [20,210,115,120,90,80,60,80,70,55,100];
 	var tableWidth = 1000;
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#onboardingHiredCanTable').fixheadertable({ //table id 
        theme: 'ui',
        height: 250,
        width: tableWidth,
        colratio:columnSpan, // table header width
        zebra: true,
        zebraClass: 'net-alternative-row',
        wrapper: false
        });            
        });
}
var columnSpan = [];
function applyStyleTableallInfo()
{
 	var tableWidth = 830;
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#statusInfo').fixheadertable({ //table id 
        theme: 'ui',
        height: 100,
        width: tableWidth,
        colratio:columnSpan, // table header width
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: true,
        wrapper: false
        });            
        });
}

function open_popupONB(statusName, teacherHistoryId, candName, eligibilityId, status){
	//alert('eligibilityId========'+eligibilityId);
	var radioStatus="";
	var startDateJFT="";
	var salaryAmountJO="";
	var employeeCodeByEM="";
	var BISICleared="";
	try{
		$('.salaryExtra').remove();
	}catch(e){}
	$('#statusRadioDiv').html('');
	ONBServiceAjax.getStatusByEidJSON(eligibilityId,{
		preHook:function(){$('#loadingDiv').show()},
		postHook:function(){$('#loadingDiv').hide()},
		async: false,
		dataType: 'json',
		errorHandler:handleError,
		callback:function(data)
		{
		var count=0;
			$.each(data,function(index,allInfo){
				var classCol='col-sm-2 col-md-2';
				if(allInfo.statusName=='Provided by CDE')
					classCol='col-sm-3 col-md-3';
				radioStatus+='<label class="'+classCol+' radio"  style="margin-top: 0px;"> <input type="radio" value="'+allInfo.statusName+'" id="status'+(++count)+'" name="status">'+allInfo.statusName+'</label>';
			});
			$('#statusRadioDiv').html(radioStatus);
		}
	});
	$('#addStatusId').show();
	//$('#status3').parents('.radio').show();
	$('[name="isSendToPeopleSoft"]').val(status.trim());
	if($('[name="flagCanEditOrView"]').val()==0){
		$('#addStatusId').hide();
	}
	if($('[name="isSendToPeopleSoft"]').val().trim()!=resourceJSON.lblNew.trim() && $('[name="isSendToPeopleSoft"]').val().trim()!=resourceJSON.lblFailed.trim()){
		$('#addStatusId').hide();
	}
	$('#loadingDiv').show();
		$('.employee').hide();
		$('.salary').hide();
		if(statusName=='Employee ID/BISI'){
			$('[name="empOrSalary"]').val('emp');
			//$('#status3').parents('.radio').hide();
		}else if(statusName=='Salary'){
			$('[name="empOrSalary"]').val('salary');
		}else{
				$('[name="empOrSalary"]').val('');
		}
		$('[name="eligibilityId"]').val(eligibilityId);
		$('[name="teacherHistoryId"]').val(teacherHistoryId);
		cancelDiv();
		var allMapStatus = new Object();
		allMapStatus['empOrSalary'] = $('[name="empOrSalary"]').val();
		allMapStatus['teacherHistoryId'] = teacherHistoryId;
		allMapStatus['eligibilityId'] = eligibilityId;
		
		$('#onr_popup #myModalLabel').html(statusName+" for "+candName);
 		var table='<table width="90%" id="statusInfo" class="" border="0">';
 		table+='';
 		table+="<thead class='bg'>";
		table+="<tr>";
		var th=getHeaderForSalaryOrEmp();
		if(th!=undefined)
		table+=th;
		//table+="<th  valign='top'>"+resourceJSON.MsgDate+"</th>";
		table+="<th  valign='top'>"+resourceJSON.lblUser+"</th>";
		if($('[name="empOrSalary"]').val().trim()=='emp')
		table+="<th  valign='top'>BISI Cleared?</th>";	
		else
		table+="<th  valign='top'>"+resourceJSON.MsgStatus+"</th>"; 
		//table+="<th  valign='top'>"+resourceJSON.lblAttachment +"</th>";
		table+="<th  valign='top'>"+resourceJSON.lblNotes+"/"+resourceJSON.lblComments+"</th>";
		table+="<th  valign='top'>"+resourceJSON.lblRStatus +"</th>";
		table+="</tr>";
		table+="</thead>";
		var totalRecords=0;
		ONBServiceAjax.getStatusJSON(allMapStatus,{
			preHook:function(){$('#loadingDiv').show()},
			postHook:function(){$('#loadingDiv').hide()},
			async: false,
			dataType: 'json',
			errorHandler:handleError,
			callback:function(data)
			{	
			//alert("data===="+data);
			$.each(data,function(k,val){
				//alert("key==="+k+"    value==="+val);
				if(k==0){totalRecords=val.split('##')[0];startDateJFT=val.split('##')[1];salaryAmountJO=val.split('##')[2];employeeCodeByEM=val.split('##')[3];BISICleared=val.split('##')[4];}
				else{
					var row=val;
					//alert("row.length==="+row.length);
					for(var r=0;r<row.length;r++){
						if(r==0)
						table+="<tr><td>"+row[r]+"</td>";
						else if(r==(row.length-1)){
							table+="<td>";
							if(($('[name="isSendToPeopleSoft"]').val().trim()==resourceJSON.lblNew || $('[name="isSendToPeopleSoft"]').val().trim()==resourceJSON.lblFailed.trim()) && $('[name="flagCanEditOrView"]').val()!='0'){
							 table+="<a href=javascript:void(0); onclick='deleteStatusONB(\""+row[r]+"\",\""+statusName+"\", \""+teacherHistoryId+"\", \""+candName+"\", \""+eligibilityId+"\");'>"+resourceJSON.lblRemove+"</a>";
							}
							table+="</td></tr>";
						}else{
						table+="<td>"+row[r]+"</td>";
							//alert("yessssss==="+row[r]);
						}
						
					}
				}
			});
			}
		});
		//alert("totalRecords===="+totalRecords);
		if(totalRecords==0)
			table+="<tr><td colspan='6'>"+resourceJSON.lblNRFound +"</td></tr>";
 		table+='</table><input type="hidden" name="startDateJFT" value="'+startDateJFT+'" /><input type="hidden" name="salaryAmountJO" value="'+salaryAmountJO+'" /><input type="hidden" name="employeeCodeByEM" value="'+employeeCodeByEM+'" /><input type="hidden" name="BISICleared" value="'+BISICleared+'" />';
 		$('#allInfo').html(table);
 		//$('body').css('class','boxed modal-open');
 		//$('#onr_popup').show();
 		$('#onr_popup').modal('show');
 		applyStyleTableallInfo();
 		$('#loadingDiv').hide();
		//getOnrGridFrsCode('1', '1189', '1', 'Ram Nath', null);
}

function getHeaderForSalaryOrEmp(){
	if($('[name="empOrSalary"]').val()=='emp'){
		//columnSpan = [100,100,150,100,150,120,100];
		columnSpan = [150,150,150,100,150,130];
		return "<th  valign='top'>"+resourceJSON.lblEmpId+"</th>"+
			   "<th  valign='top'>"+resourceJSON.MsgDate+"</th>";
	}
	else if($('[name="empOrSalary"]').val()=='salary'){
		//columnSpan = [90,90,100,100,90,90,80,100,80];
		//columnSpan = [100,100,100,100,100,100,120,110]; //remove grade
		columnSpan = [100,100,50,50,100,100,100,120,110];
		return "<th  valign='top'>"+resourceJSON.lblSalary +"</th>" +
				/*"<th  valign='top'>"+resourceJSON.lblGrade+"</th>" +*/
				"<th  valign='top'>"+resourceJSON.lblStep+"</th>"+
				"<th  valign='top'>"+resourceJSON.lblSDate+"</th>"+
				"<th  valign='top'>"+resourceJSON.lblEDate+"</th>";
	}
	else{
		return "<th  valign='top'>"+resourceJSON.MsgDate+"</th>";
		columnSpan = [200,200,150,150,130];
	}
}

function open_divONB(){
	//alert($('[name="empOrSalary"]').val());
	try{
		$('.salaryExtra').remove();
	}catch(e){}
	$('#allTextField').html(getAllInputFieldByNode($('[name="empOrSalary"]').val()));
	 if($('[name="empOrSalary"]').val()=='salary'){
		 $('.EmpOrSalary').after(getAllInputFieldByNode($('[name="empOrSalary"]').val()+'Extra')); 
		 hideshowDaysWorked();
		 $('[name="startDate"]').val($('[name="startDateJFT"]').val());
		 $('[name="salary"]').val($('[name="salaryAmountJO"]').val());
		 
	 }
	 if($('[name="empOrSalary"]').val().trim()=='emp'){
		 $('[name="employeeId"]').val($('[name="employeeCodeByEM"]').val());
		 $('input[name="status"][value="' + $('[name="BISICleared"]').val() + '"]').attr('checked', 'checked');
		 $('#statusRadioLabel').html('BISI Cleared?<span class="required">*</span>');
	 }
	 else
	 $('#statusRadioLabel').html('Status<span class="required">*</span>');
	/*$('.employee').hide();
	$('.salary').hide();
	if($('[name="empOrSalary"]').val()=='emp'){
		$('.employee').show();
		$('.salary').hide();
	}else if($('[name="empOrSalary"]').val()=='salary'){
		$('.employee').hide();
		$('.salary').show();
	}*/
	document.getElementById("add_action").style.display="block";
/*	document.getElementById("status1").checked = false;
	document.getElementById("status2").checked = false;
	document.getElementById("status3").checked = false;*/
	$('#notesmsg').find(".jqte_editor").html("");
	$("#eligibilyFile").val('');
}

function cancelDivONB() { 
	$('#notesmsg').find(".jqte_editor").text() 		== 	""
	$('#add_action').hide();
	$('#errordiv').hide();
	try{
		$('.salaryExtra').remove();
	}catch(e){}
	$('#allTextField').html('');
	/*$('.employee').hide();
	$('.salary').hide();*/
}

function onb_popupHide(){
	$('#onr_popup').hide();
	displayHiredCandidate();
}
function deleteStatusONB(EVHId,statusName, teacherHistoryId, candName, eligibilityId){
	openConfirmBox(resourceJSON.headTm,resourceJSON.lblRUSure,true,"onr_popup",function(data){
		if(data){
			ONBServiceAjax.deleteStatusONB(EVHId,{
				preHook:function(){$('#loadingDiv').show()},
				postHook:function(){$('#loadingDiv').hide()},
				async: false,
				errorHandler:handleError,
				callback:function(data)
				{
				/*alert("sucess========"+data);
				if(data)
				alert("delete========="+EVHId);*/
				}
			});
			//displayHiredCandidate();
			open_popupONB(statusName, teacherHistoryId, candName, eligibilityId, $('[name="isSendToPeopleSoft"]').val());
		}
	});
}
function clearAllText(){
	$('[name="employeeId"]').val('');
	$('[name="salary"]').val('');
	$('[name="grade"]').val('');
	$('[name="step"]').val('');
	$("input:radio").attr("checked", false);
	//$('[name="status"]:checked').val();
	$('[name="notes"]').parents().parents('.jqte').find(".jqte_editor").html('');
}
function saveNotesONB(){
	$('[name="employeeId"]').css("background-color","");
	$('[name="salary"]').css("background-color","");
	$('[name="grade"]').css("background-color","");
	$('[name="step"]').css("background-color","");
	$('[name="startDate"]').css("background-color","");
	$('[name="endDate"]').css("background-color","");
	$('#errordiv').hide();
	$('#errordiv').html('');
	var saveFlag=true;
	var employeeId='';
	var salary='';
	var grade='';
	var step='';
	var startDate='';
	var endDate='';
	var yearRound='';
	var actualDays='';
	var totalDays='';
	var status=$('[name="status"]:checked').val();
	var notes= $('[name="notes"]').parents().parents('.jqte').find(".jqte_editor").html();
	var eligibilityId=$('[name="eligibilityId"]').val();
	var teacherHistoryId=$('[name="teacherHistoryId"]').val();
	var statusName=$('#onr_popup .modal-header #myModalLabel').html().split('for')[0].trim();
	var candName=$('#onr_popup .modal-header #myModalLabel').html().split('for')[1].trim();
	//alert('enter=='+status);
	//alert(statusName+" for "+candName);
	//alert('$([name=empOrSalary]).val()=='+$('[name="empOrSalary"]').val()+"   status=="+status );
	if(status==null || status==undefined || status==''){
		//alert('enter');
		if($('[name="empOrSalary"]').val().trim()=='emp')
			$('#errordiv').append("&#149; Please select BISI Cleared?");
			else
			$('#errordiv').append("&#149; "+resourceJSON.lblPSStatus+"<br>");
		$('#errordiv').show();
		saveFlag=false;
	}else{
		if($('[name="empOrSalary"]').val().trim()=='emp'){
			employeeId=$('[name="employeeId"]').val().trim();
			if(employeeId==''){
				$('[name="employeeId"]').css("background-color",txtBgColor);
				$('#errordiv').append("&#149; "+resourceJSON.lblPEEID+"<br>");
				$('#errordiv').show();
				saveFlag=false;
			}
			if(saveFlag && employeeId.length < 6){
				$('[name="employeeId"]').css("background-color",txtBgColor);
				$('#errordiv').append("&#149; "+resourceJSON.sixDigitALTMSG+"<br>");
				$('#errordiv').show();
				saveFlag=false;
			}
		}
		if($('[name="empOrSalary"]').val().trim()=='salary'){
			salary=$('[name="salary"]').val();
			grade=$('[name="grade"]').val();
			step=$('[name="step"]').val();
			startDate=$('[name="startDate"]').val();
			endDate=$('[name="endDate"]').val();
			yearRound=$("[name='yearRound']").is(':checked')?"1":"0";
			actualDays=$('[name="actualDays"]').val();
			totalDays=$('[name="totalDays"]').val();
			if(startDate.trim()==''){
				$('[name="startDate"]').css("background-color",txtBgColor);
				$('#errordiv').append("&#149; "+resourceJSON.PlzEtrSTDateMSG+"<br>");
				$('#errordiv').show();
				saveFlag=false;
			}
			if(startDate.trim()!="" && endDate.trim()!=""){
				try{
					var date1 = startDate.trim().split("-");
					var	date2 = endDate.trim().split("-");
				    var sDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);		    
				    var eDate = new Date(date2[0]+"/"+date2[1]+"/"+date2[2]);
				    if(sDate > eDate){
						$('#errordiv').show();
						$('#errordiv').append("&#149; End Date must be greater than or equal to Start Date<br>");
						$('[name="endDate"]').focus();
						$('[name="endDate"]').css("background-color", "#F5E7E1");
						saveFlag=false;
					} 
				}catch(err){}
			  }	
			/*if(endDate.trim()==''){
				$('[name="endDate"]').css("background-color",txtBgColor);
				$('#errordiv').append("&#149; "+resourceJSON.enterEDateALTMSG+"<br>");
				$('#errordiv').show();
				saveFlag=false;
			}*/
				if(status=='Yes'){
					if(salary.trim()=='' && step.trim()==''){
						$('#errordiv').append("&#149; Salary or Step is required<br>");
						$('#errordiv').show();
						saveFlag=false;
					}
					/*if(salary.trim()==''){
						$('[name="salary"]').css("background-color",txtBgColor);
						$('#errordiv').append("&#149; "+resourceJSON.lblPESlry+"<br>");
						$('#errordiv').show();
						saveFlag=false;
					}*/
					
					/*if(grade.trim()==''){
						$('[name="grade"]').css("background-color",txtBgColor);
						$('#errordiv').append("&#149; "+resourceJSON.lblPEG+"<br>");
						$('#errordiv').show();
						saveFlag=false;
					}*/
					/*if(step.trim()==''){
						$('[name="step"]').css("background-color",txtBgColor);
						$('#errordiv').append("&#149; "+resourceJSON.lblPES+"<br>");
						$('#errordiv').show();
						saveFlag=false;
					}*/
				}
			}
	}
	if(saveFlag){
		var allStatusInfo = new Object();
		allStatusInfo['employeeId']=employeeId;
		allStatusInfo['salary']=salary;
		allStatusInfo['grade']=grade;
		allStatusInfo['step']=step;
		allStatusInfo['status']=status;
		allStatusInfo['notes']=notes;
		allStatusInfo['empOrSalary']=$('[name="empOrSalary"]').val();
		allStatusInfo['eligibilityId']=eligibilityId;
		allStatusInfo['teacherHistoryId']=teacherHistoryId;
		allStatusInfo['startDate']=startDate;
		allStatusInfo['endDate']=endDate;
		allStatusInfo['yearRound']=yearRound;
		allStatusInfo['actualDays']=actualDays;
		allStatusInfo['totalDays']=totalDays;
		ONBServiceAjax.saveNotesONB(allStatusInfo,{
			preHook:function(){$('#loadingDiv').show()},
			postHook:function(){$('#loadingDiv').hide()},
			async: false,
			dataType: 'json',
			errorHandler:handleError,
			callback:function(data)
			{
			//alert("sucess========"+data);
			if(data){
				//onBoardingSendMailToTeacher(teacherHistoryId);
				open_popupONB(statusName, teacherHistoryId, candName, eligibilityId,$('[name="isSendToPeopleSoft"]').val());
				clearAllText();
				cancelDivONB();
			//alert("delete========="+EVHId);
			}
			}
		});

	}
	//alert("employeeId==="+employeeId+"  status==="+status+"  notes=="+notes);
}

function sendToPeopleSoft(){
	
		var checkCnt=0;
		var selectedTSHId = new Array();
		var inputs = jQuery("[name='hiredCheckBox']:checked").length;
		//alert('select check box==='+inputs);
	    if (inputs > 0){
	        jQuery("[name='hiredCheckBox']:checked").each(function(){
	        	selectedTSHId.push($(this).val());
	        	checkCnt++;
	        });
	    }
	    //alert(selectedTSHId);
		if(inputs==0) {
			openConfirmBox(resourceJSON.headTm,resourceJSON.lblPEALOCandidate,false,"",function(data){});
			return false;
		}
		if(inputs>1) {
			openConfirmBox(resourceJSON.headTm,resourceJSON.lblPSOOCandidate,false,"",function(data){});
			return false;
		}
		if(inputs>0){
			ONBServiceAjax.sendToPeopleSoftONB(selectedTSHId,{
				preHook:function(){$('#loadingDiv').show()},
				postHook:function(){$('#loadingDiv').hide()},
				async: false,
				errorHandler:handleError,
				callback:function(data)
				{
				//alert("sucess========"+data);
				if(data){
					displayHiredCandidate();
				//alert("delete========="+EVHId);
				}else{
					openConfirmBox(resourceJSON.headTm,resourceJSON.lblPTA,false,"",function(data){});
					$('#loadingDiv').hide();
				}
				}
			});
		}
}

//add By Ram Nath
function openConfirmBox(header,confirmMessage,flag,divId,callback){
	if(divId.trim()!='')
    	$('#'+divId).hide();
	var OkOrOkAndCancel="";
	if(flag){
		OkOrOkAndCancel="<button type='button' class='btn btn-default' id='confirmTrue'>"+resourceJSON.btnOk+"</button>";
		OkOrOkAndCancel+="<button type='button' class='btn btn-default' id='confirmFalse'>"+resourceJSON.btnCancel+"</button>";
		
	}else{
		OkOrOkAndCancel="<button type='button' class='btn btn-default' id='confirmFalse'>"+resourceJSON.btnOk+"</button>";
	}
	var div="<div class='modal hide in' id='confirmbox' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'>"+
			"<div class='modal-dialog' style='width:450px;'>"+
			"<div class='modal-content'>"+
			"<div class='modal-header'>"+
	        "<button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>"+
	        "<h4 class='modal-title' id='myModalLabel' style='color:white;'></h4>"+
	      "</div>"+
	      "<div class='modal-body' id='confirmMessage'>"+	      
	      "</div>"+
	      "<div class='modal-footer'>"+
	      OkOrOkAndCancel +	        
	      "</div>"+
	    "</div>"+
	  "</div>"+
	"</div>";
	$('#onr_popup').after(div);
    confirmMessage = confirmMessage || '';
    $('#confirmbox').modal({show:true,
                            backdrop: "static",
                            keyboard: false,
    });

    $('#confirmbox #confirmMessage').html(confirmMessage);
    $('#confirmbox #myModalLabel').html(header);
    $('#confirmFalse').click(function(){
    	if(divId.trim()!='')
        $('#'+divId).show();
    	$('#confirmbox').modal('hide');
        $('#confirmbox').remove();
        if (callback) callback(false);
    });
    $('#confirmTrue').click(function(){
    	if(divId.trim()!='')
    	$('#'+divId).show();
    	$('#confirmbox').modal('hide');
        $('#confirmbox').remove();
        if (callback) callback(true);
    });
}

function printOnboardingDATA()
{
	allMap['report']='true';
	ONBServiceAjax.displayONBGrid(allMap,{
		preHook:function(){$('#loadingDiv').show()},
		postHook:function(){$('#loadingDiv').hide()},
		async: true,
		dataType: 'json',
		errorHandler:handleError,
		callback:function(data)
		{	
		setHeader();
		var sb = "";
		var userName=data[0].split("##")[1];
		var currentDate=data[0].split("##")[2];
		sb+="<style>.nobground1 {padding: 6px 10px 6px 10px;-webkit-border-top-left-radius: 30px; border-top-left-radius: 30px;-moz-border-radius-topleft: 30px;-webkit-border-top-right-radius: 30px; border-top-right-radius: 30px; -moz-border-radius-topright: 30px; -webkit-border-bottom-right-radius: 30px; border-bottom-right-radius: 30px; -moz-border-radius-bottomright: 30px;-webkit-border-bottom-left-radius: 30px;border-bottom-left-radius: 30px;-moz-border-radius-bottomright: 30px;}</style>";
		sb+="<div style='text-align: center; font-size: 25px; font-weight:bold;'> "+"Onboarding Dashboard"+"</div><br/>";
		sb+="<div style='width:100%'>";
		sb+="<div style='width:50%; float:left; font-size: 15px; '> "+"Created By "+": "+userName+"</div>";
		sb+="<div style='width:50%; float:right; font-size: 15px; text-align: right; '>"+"Date of Print "+": "+currentDate+"</div>";
		sb+="<br/><br/>";
		sb+="</div>";
		sb+="<table class='table' width='100%' border='0'>";
		sb+="<thead class='bg'>";
		var responseText="";
		$.each(ONBOARDING_HEADER_SORTING,function(k,v){	
			sb+="<th style='text-align:left' valign='top'>"+v+"</th>";
		});
		sb+="</thead>";
		var noOfRecordCheck=0;
		var totalRecords=0;
		var candName="",teacherEmail="",postionNumber="",hiredDate="",teacherHistoryId="",teacherId="",jobId="",status="",schoolName="";
		 var inputTextHideOrShow="none",OSColor="0000CC";
		$.each(data,function(k,val){
				if(k==0){totalRecords=parseInt(val.split("##")[0]);}else{
				var row=val;
				dataExists=true; candName=row[0]+" "+row[1]; teacherEmail=row[2]; postionNumber=row[3]; hiredDate=row[4]; teacherHistoryId=row[5];teacherId=row[6]; jobId=row[7]; status=row[8];schoolName=row[10];
	            sb+="<tr>";
	            sb+="<td><input type='checkBox' value='"+teacherHistoryId+"' name='hiredCheckBox' style='display:"+inputTextHideOrShow+";'/></td>";
	            sb+="<td>"+candName+"<br/>"+"("+teacherEmail+")"+"</td>";
	            sb+="<td>"+postionNumber+"</td>";		
	            sb+="<td>"+schoolName+"</td>";	
	            sb+="<td>"+hiredDate+"</td>";
	            sb+="<td>"+getAllColorValueReturn(row[11])+"</td>";	
	            //sb+="<td>"+getAllColorValueReturn(row[13])+"</td>";	
	            sb+="<td>"+getAllColorValueReturn(row[14])+"</td>";
	            sb+="<td>"+getAllColorValueReturn(row[15])+"</td>";	
	            sb+="<td>"+getAllColorValueReturn(row[12])+"</td>";
	            OSColor=getShowHideAndOSValue(row[11],row[12],row[14],row[15],1);
	            sb+="<td>"+OSColor+"</td>";	
	            sb+="<td>"+status+"</td>";	
	            sb+="</tr>";
	            }
	        });			
			 if(totalRecords==0){
				 sb+="<tr><td colspan='8'>No record found.</td></tr>";
			 }
			sb+="</table>";
			try{
			   if (isSafari && !deviceType)
			    {
			    	window.document.write(sb);
					window.print();
			    }else
			    {
			    	var newWindow = window.open();
			    	newWindow.document.write(sb);	
			    	newWindow.print();			    	
				 } 
			}catch (e) 
			{
				alert(e);							 
			}
			
		},
	errorHandler:handleError
	});
}

function generateOnboardingPDF()
{
	allMap['report']='true';
	ONBServiceAjax.generateOnboardingPDF(allMap,{
		preHook:function(){$('#loadingDiv').show()},
		postHook:function(){$('#loadingDiv').hide()},
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{			
				if(deviceType || deviceTypeAndroid)
				{
					window.open(data,'left=200,top=50,width=700,height=600,toolbar=1,resizable=0');			
				}		
				else
				{
					$('#modalDownloadJobList').modal('hide');
					document.getElementById('ifrmCJS').src = ""+data+"";
					try{
						$('#modalDownloadJobList').modal('show');
					}catch(err)
					{}		
			     }		
			     return false;
			}
		});
}

function generateOnboardingExel()
{
	allMap['report']='true';
	ONBServiceAjax.generateOnboardingExcel(allMap,{
		preHook:function(){$('#loadingDiv').show()},
		postHook:function(){$('#loadingDiv').hide()},
		async: true,
		errorHandler:handleError,
		callback: function(data)
		{
			if(deviceType)
			{					
					$("#exelfileNotOpen").css({"z-index":"3000"});
			    	$('#exelfileNotOpen').show();
			}
			else
			{
				try
				{
					document.getElementById('ifrmTrans').src = "joblist/"+data+"";
				}
				catch(e)
				{alert(e);}
			}
		},
	errorHandler:handleError
	});
}


function responseSorting(callingmethodName,fieldLabel,sortOrderFieldName,fieldName,sortOrderTypeVal,pgNo){
		var sbResponseText = "";
		try{
				sbResponseText+="<span style=''>";
				if( sortOrderTypeVal=="1" && sortOrderFieldName==fieldName){
					sbResponseText+="<table border='0px'style='width: auto;'>";
					sbResponseText+="<tr>";
					sbResponseText+="<td valign='top'>"+fieldLabel+"</td>";
					sbResponseText+="<td valign='top'><div style='margin-bottom: -5px;'>&nbsp;<a class='net-header-text' href=\"javascript:"+callingmethodName+"('"+(pgNo)+"','"+fieldName+"',0)\"><span class=\"dscorder\"></span></a></div></td>";
					sbResponseText+="</table>";
				}else if( sortOrderTypeVal=="0" && sortOrderFieldName==fieldName){
					sbResponseText+="<table border='0px'style='width: auto;'>";
					sbResponseText+="<tr>";
					sbResponseText+="<td valign='top'>"+fieldLabel+"</td>";
					sbResponseText+="<td valign='top'><div style='margin-top: 0px;'>&nbsp;<a class='net-header-text' href=\"javascript:"+callingmethodName+"('"+(pgNo)+"','"+fieldName+"',1)\"><span class=\"ascorder\"></span></a></div></td>";
					sbResponseText+="</table>";
				}else{
					sbResponseText+="<table border='0px'style='width: auto;'>";
					sbResponseText+="<tr>";
					sbResponseText+="<td valign='top'rowspan='2'>"+fieldLabel+"</td>";
					sbResponseText+="<td valign='top'><div style='margin-bottom: -5px;'>&nbsp;<a class='net-header-text' href=\"javascript:"+callingmethodName+"('"+(pgNo)+"','"+fieldName+"',0)\"><span class=\"dscorder\"></span></a></div></td>";
					sbResponseText+="</tr>";
					sbResponseText+="<tr>";
					sbResponseText+="<td valign='top'><div style='margin-top: -5px;'>&nbsp;<a class='net-header-text' href=\"javascript:"+callingmethodName+"('"+(pgNo)+"','"+fieldName+"',1)\"><span class=\"ascorder\"></span></a></div></td>";
					sbResponseText+="</tr>";
					sbResponseText+="</table>";
				}
			sbResponseText+="</span>";
		}catch(e){
			alert(e);
		}
		return sbResponseText+"";
	}

//added by Ram Nath
	function getAlwaysPaginationString(totalRecords,noOfRowInPage,pageNoStr,footerFlag,footerWidth,setPageFunctionForJS)
	{		
		//--Pagination and Sorting
		var sb = "";
		var pageNo = parseInt(pageNoStr);
		var k;
		//
		var noOfPage=0;
		var totalRow=totalRecords;
		var noOfRow=parseInt(noOfRowInPage);
		var start;
		var end;
		//
		noOfPage=parseInt((totalRow+noOfRow-1)/noOfRow);
		var recordFrom=(noOfRow*(pageNo-1))+1;
		var recordTo;
		if(((noOfRow*(pageNo-1))+1+noOfRow)>totalRow)
		{
			recordTo=totalRow;
		}
		else
		{
			recordTo=(noOfRow*(pageNo-1))+noOfRow;
		}
		var gridPageSize = "10,20,50,100".split(",");
		var minPgSize = parseInt(gridPageSize[0]);
		$.each(gridPageSize,function(index,no){
			if(parseInt(no)<minPgSize)
				minPgSize=parseInt(no);
		});
		if(footerFlag && minPgSize <= 10)
			minPgSize=0;
		if(totalRow>minPgSize)
		{
			//alert("Add---------------------------------------------------------------");	
			sb+="<div class='net-widget-footer  net-corner-bottom' style='width:"+footerWidth+";'>";
			sb+="<div class=''>";
			sb+="<table width='100%' cellspacing='0px' cellpadding='0px' >";
			sb+="<tr>";
			sb+="<td width='1%' ></td>";
			sb+="<td width='28%' nowrap align='left'>";
			sb+=""+recordFrom+" - "+recordTo+" of "+totalRow+" "+resourceJSON.lblRecords;
			sb+="</td>";
			sb+="<td width='15%'  nowrap>";
			if(pageNo!=1)
			{
				sb+="<A class='pagingLink' HREF=\"javascript:"+setPageFunctionForJS+"('1') \">";
				sb+=resourceJSON.lblFirst+"&nbsp;&nbsp;|";
				sb+="</A>";
				sb+="<A class='pagingLink' HREF=\"javascript:"+setPageFunctionForJS+"('"+(pageNo-1)+"') \" >";
				sb+="&nbsp;&nbsp;<&lt;"+resourceJSON.lblPrevious+"&nbsp;&nbsp;|";
				sb+="</A>";
			}
			sb+="</td>";
			sb+="<td  width='25%'  align='left' nowrap>";
			if(noOfPage>5 && pageNo>3)
			{
				if(pageNo+2>noOfPage)
				{
					start=noOfPage-4;
					end=noOfPage;
				}
				else
				{
					start=pageNo-2;
					end=pageNo+2;
				}
			}
			else
			{
				if(noOfPage<5)
				{
					start=1;
					end=noOfPage;
				}
				else
				{
					start=1;
					end=5;
				}
			}
			for(k=start;k<=end;k++)
			{
				if(k==pageNo)
				{
					sb+="<B>";		
					sb+=""+k+"&nbsp;</B>";
				}
				else
				{
					sb+="<A class='pagingLink' HREF=\"javascript:"+setPageFunctionForJS+"('"+k+"') \" >";
					sb+=k+"&nbsp;";
					sb+="</A>";
				}
			}
			if(pageNo<(k-1))
			{
				sb+="<A class='pagingLink' HREF=\"javascript:"+setPageFunctionForJS+"('"+(pageNo+1)+"') \"  >";
				sb+="&nbsp;|&nbsp;&nbsp;"+resourceJSON.lblNext+"&gt;>&nbsp;&nbsp;";
				sb+="</A>";
	
				sb+="<A class='pagingLink' HREF=\"javascript:"+setPageFunctionForJS+"('"+(noOfPage)+"') \"   >";
				sb+="|&nbsp;&nbsp;&nbsp;"+resourceJSON.lblLast;
				sb+="</A>";	
			}
			sb+="</td>";
			sb+="<td  width='29%' align='right'  class='pagingBoldText' nowrap>";
			sb+=resourceJSON.lblRecordsPerPage+" ";
			sb+="<select name='pageSize' id='pageSize' style='width: 60px;height:28px; color: #555555; ' onchange=\""+setPageFunctionForJS+"('');\" >";
			$.each(gridPageSize,function(index,no)
			{
				sb+="<option value='"+no+"' "+(noOfRow==parseInt(no)?"selected":"")+" >"+no+"</option>";
			});		
			sb+="</select>";
			sb+="</td>";
			sb+="<td  width='1%'></td>";
			sb+="</tr>";
			sb+="</table>";
			sb+="</div>";
			sb+="</div>";	
		}
		//alert("sb====="+sb);
		return sb;
	}

function onboardingSorting(pageno,sortOrderType,sortOrder){
//alert("pageno==="+pageno+"   sortOrderType==="+sortOrderType+"   sortOrder==="+sortOrder);
	if(pageno!=''){
			pageOB=pageno;	
		}else{
			pageOB=1;
		}
		sortOrderStrOB	=	sortOrder;
		sortOrderTypeOB	=	sortOrderType;
		if(document.getElementById("pageSize")!=null){
			noOfRowsOB = document.getElementById("pageSize").value;
		}else{
			noOfRowsOB=10;
		}
		displayHiredCandidate();
}
function setPageSize(pageno)
{
	if(pageno!='')
	{
		pageOB=pageno;	
	}
	else
	{
		pageOB=1;
	}
	
	noOfRowsOB = document.getElementById("pageSize").value;
	displayHiredCandidate();
}

function onBoardingSendMailToTeacher(teacherHistoryId)
{
	//alert("teacherId:- "+teacherId+"\njobId:- "+jobId);
	ONBServiceAjax.onBoardingSendMailToTeacher(teacherHistoryId,{
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
			//displayPNRGrid();
		}
	});
}

function saveCandidateToFolderByUserForONB()
{
	document.getElementById('iframeSaveCandidate').contentWindow.checkFolderId();
	var iframe = document.getElementById('iframeSaveCandidate');
	var innerDoc = iframe.contentDocument || iframe.contentWindow.document;
	var folderId = innerDoc.getElementById('frame_folderId').value;
	var frameHeight=iframe.contentWindow.document.body.scrollHeight;
	$('iframe').css('height',frameHeight);
	var fId="";
	if(innerDoc.getElementById('frame_folderId').value=="" || innerDoc.getElementById('frame_folderId').value.length==0)
	{
		innerDoc.getElementById('errortreediv').style.display="block";
		innerDoc.getElementById('errortreediv').innerHTML="&#149; "+resourceJSON.PlzSelectAnyFolder+"";
		 return false;
	}
	else
	{
		//fId=folderId;
	}
	var savecandidatearray="";
	 $("#savecandidatearray").val($("[name='jobforteacherId']").val());
	 try{
	 		if(savecandidatearray.trim()==''){
	 			savecandidatearray=$("[name='jobforteacherId']").val();
	 		 $("#savecandidatearray").val(savecandidatearray);
	 		}
		 } catch(e){}
	 
	CandidateGridAjax.saveCandidateToFolderByUser(savecandidatearray,folderId,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			if(data==1)
			{
				try{
					$("#saveToFolderDiv").modal("hide");
					$('#saveAndShareConfirmDiv').html(resourceJSON.SavedCandidate);
					$('#shareConfirm').modal("show");
					$('#draggableDivMaster').show();
				}catch(err)
				  {}
			}
			if(data==3)
			{
				$("#txtoverrideFolderId").val(folderId);
				try{
					$("#duplicatCandidate").modal("show");
				}catch(err)
				  {}
			}
		}
	});
}


function showProfileContentForTeacherForONB(dis,teacherId,jobId,noOfRecordCheck,sVisitLocation,event)
{	
	var jftId = new Object();jftId['teacherId']=teacherId;jftId['jobId']=jobId;
	ONBServiceAjax.getJFTIdByJobIdAndTeacherId(jftId,{
		preHook:function(){$('#loadingDiv').show()},
		postHook:function(){$('#loadingDiv').hide()},
		async: false,
		errorHandler:handleError,
		callback: function(data)
		{
			$("[name='jobforteacherId']").val(data);
		},
	errorHandler:handleError
	});
	   try
	   {
		   var mydiv = document.getElementById("mydiv");
	       var curr_width = parseInt(mydiv.style.width);
	      
	       if(deviceType)
	       {
	    	   mydiv.style.width = 630+"px";  
	       }
	       else
		   {
	    	   mydiv.style.width = 647+"px";  
		   }      
	   }
	   catch(e){alert(e)}
	   
	var districtMaster = null;
	showProfileContentClose_ShareFolder();
	$('#loadingDiv').show();
	document.getElementById("teacherIdForprofileGrid").value=teacherId;
	TeacherProfileViewInDivAjax.showProfileContentForTeacher(teacherId,noOfRecordCheck,sVisitLocation,districtMaster,0,{ 
	async: true,
	callback: function(data)
	{
	  $('#loadingDiv').hide();
	  $("#draggableDivMaster").modal('show');
	  //var wait="<div id='removeWait' style='text-align:center;padding-top:0px;'><img src='images/loadingAnimation.gif' /> <br>Loading...</div>";
		$("#draggableDiv").html(data);
       
		TeacherProfileViewInDivAjax.showProfileContentForTeacher(teacherId,noOfRecordCheck,sVisitLocation,districtMaster,1,{ 
			async: true,
			callback: function(resdata){
			setTimeout(function () {
				$("#profile_id").after(resdata);
				$('#removeWait').remove();
	        }, 100);
			
			
		}
		});
	  
	  $('#tpResumeprofile').tooltip();
	  $('#tpPhoneprofile').tooltip();
	  $('#tpPDReportprofile').tooltip();
	  $('#tpTeacherProfileVisitHistory').tooltip();
	  $('#teacherProfileTooltip').tooltip();
	  $('textarea').jqte();
	},
	errorHandler:handleError  
	});
} 

function showQualificationDivForONB(jobId,teacherId,districtId,headQuarterId,branchId)
{
	     document.getElementById("teacherIdforPrint").value='';
	     document.getElementById("districtIdforPrint").value='';
	     document.getElementById("headQuarterIdforPrint").value='';
	     document.getElementById("branchIdforPrint").value='';
         document.getElementById("teacherIdforPrint").value=teacherId;
         document.getElementById("districtIdforPrint").value=districtId;
         document.getElementById("headQuarterIdforPrint").value=headQuarterId;
         document.getElementById("branchIdforPrint").value=branchId;
        CandidateGridAjaxNew.getQualificationDetailsForONB(jobId,teacherId,districtId,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
	    	$('#qualificationDiv').modal('show');
			$('#qualificationDivBody').html(data);
			try{
				if(document.getElementById("userType").value==2 || document.getElementById("userType").value==5)
				{
					if(document.getElementById("flagForDistrictSpecificQuestions").value=="null" || document.getElementById("flagForDistrictSpecificQuestions").value=="false")
					{
						if((document.getElementById("flagForFinalize").value=='null' || document.getElementById("flagForFinalize").value=="false"))
						{	
							$("#qStatusSave").show();
							$("#qStatusFinalize").show();
						}
						else
						{
							$("#qStatusSave").hide();
							$("#qStatusFinalize").hide();
						}
						
					}else if(document.getElementById("flagForDistrictSpecificQuestions").value=="true"){
						$("#qStatusSave").hide();
						$("#qStatusFinalize").hide();
					}
				}
				else
				{
					$("#qStatusSave").hide();
					$("#qStatusFinalize").hide();
				}
			}catch(err){}
		}
	});
}

function getDistrictSpecificQuestionDataForONB(districtId,teacherId,jobId)
{	
	dId=districtId;
	tId=teacherId;
	CandidateGridAjaxNew.getDistrictSpecificQuestionDataForONB(districtId,teacherId,jobId,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{	
			$('#getDistrictSpecificQuestion').html(data);
		}
	});
}

function printQualificationDivForONB()
{	
	var teacherId = document.getElementById("teacherIdforPrint").value;
	var districtId = document.getElementById("districtIdforPrint").value;
	var headQuarterId = document.getElementById("headQuarterIdforPrint").value;
	var branchId = document.getElementById("branchIdforPrint").value;
	 $('#loadingDiv').fadeIn();
	    CandidateGridAjaxNew.getQualificationDetailsForONBPrint(teacherId,districtId,headQuarterId,branchId,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
	     	$('#loadingDiv').hide();
			try
			{
			    	var newWindow = window.open();
			    	newWindow.document.write(data);			
					newWindow.print();							   
			}
			catch (e) 
			{							
				$('#printmessage1').modal('show');
			}
		}
	});
}

function getThumnb(jobId,arrFlag,teacherId,districtId){
	
	var qqoIcon = "";
	
	if(arrFlag!=undefined && arrFlag!=""){
		qqoflag 	= arrFlag.split("##");
		
		var isOnBoardingFlag 	= qqoflag[0];
		var isDistrictSpecificNoteFinalizeONBFlag	= qqoflag[1];
		var flagForDistrictSpecificQuestionsONBFlag = qqoflag[2];
		
		if(flagForDistrictSpecificQuestionsONBFlag==0 || flagForDistrictSpecificQuestionsONBFlag=='false'){
			qqoIcon = '<a data-original-title="Qualification Issues" rel="tooltip" id="tpInv" href="javascript:void(0);"><span class="fa-thumbs-down icon-large iconcolorRed" onclick="showQualificationDivForONB('+jobId+','+teacherId+','+districtId+',0,0);"></span></a>';
		}else if(isDistrictSpecificNoteFinalizeONBFlag==0 || isDistrictSpecificNoteFinalizeONBFlag=='false'){
			qqoIcon = '<a data-original-title="Qualification Issues" rel="tooltip" id="tpInv" href="javascript:void(0);"><span class="icon-thumbs-up icon-large thumbUpIconColor" onclick="showQualificationDivForONB('+jobId+','+teacherId+','+districtId+',0,0);"></span></a>';
		}else if(isDistrictSpecificNoteFinalizeONBFlag==1 || isDistrictSpecificNoteFinalizeONBFlag=='true'){
			qqoIcon = '<a data-original-title="Qualification Issues" rel="tooltip" id="tpInv" href="javascript:void(0);"><span class="icon-thumbs-up icon-large thumbUpIconBlueColor" onclick="showQualificationDivForONB('+jobId+','+teacherId+','+districtId+',0,0);"></span></a>';
		}
	 }
	return qqoIcon;
}
function hideshowDaysWorked() {
	$("[name='yearRound']").hide();
	$("#yearRoundLabel").hide();
	var isChecked=$("[name='yearRound']").is(':checked');
	if(isChecked){
		$('.salaryCheck').hide();
		$("[name='totalDays']").val('');
		$("[name='actualDays']").val('');
	}
	else{
		$('.salaryCheck').show();
		$("[name='totalDays']").val('');
		$("[name='actualDays']").val('');
	}
}
$(document).ready(function() {
   $("[name='employeeId']").keydown(function (e) {
    	//alert('enter=====');
        // Allow: backspace, delete, tab, escape, enter and . , 190=.
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
   });
});
function keydown(e) {
 	var keynum = ( e.which ) ? e.which : e.keyCode;
 	if(keynum==8)return true;
    var keychar = String.fromCharCode(keynum).replace(/[^0-9]/g, '');
    if(keychar.trim()!='')return true; else return false;
}
function keydownAmount(e,obj) {
	var amount=$(obj).val().split(".");
 	var keynum = ( e.which ) ? e.which : e.keyCode;
 	if(keynum==8)return true;
    var keychar = String.fromCharCode(keynum).replace(/[^0-9]/g, '');
    if(amount.length<2 && keynum==46)return true;
    //only two decimal point
    if(amount.length>1 && amount[1].length>1)return false;
    if(keychar.trim()!='') 	return true;  else return false;
}
//========================================================Ended By Ram Nath==============================================================================