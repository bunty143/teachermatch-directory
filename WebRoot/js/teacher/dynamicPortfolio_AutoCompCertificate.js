var count=0;
var index=-1;
var length=0;
var divid='';
var txtid='';
var hiddenDataArray = new Array();
var showDataArray = new Array();
var degreeTypeArray = new Array();
var hiddenId="";

//=========== In Case of final =====================
//For State Text Box

function getCertificateTypeAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	if($("#stateMaster").val()=='')
		return;
	
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38)
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13)
	{
		
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		//searchArray = getCertificateTypeArray(txtSearch.value);
		//fatchData(txtSearch,searchArray,txtId,txtdivid);
		document.getElementById("certName").focus();
		
	} 
	else if(event.keyCode==9)
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getCertificateTypeArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getCertificateTypeArray(certType){
	
	var stateIdForDSPQ = document.getElementById("stateMaster").value;
	var searchArray = new Array();
	DWRAutoComplete.getCertificateTypeList(certType,stateIdForDSPQ,{ 
		async: false,
		callback: function(data){
		hiddenDataArray = new Array();
		showDataArray = new Array();
		for(i=0;i<data.length;i++){
			searchArray[i]=data[i].certType;
			hiddenDataArray[i]=data[i].certTypeId;
			showDataArray[i]=data[i].certType;
		}
	}
	});	

	return searchArray;
}

function hideCertificateType(dis,hiddenId,divId)
{
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		//alert(hiddenId);
		//alert(hiddenDataArray[index]);
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
		//dis.value="";
		
		if(dis.value!="")
		{
			//alert(dis.value);
			/*var cnt=0;
			var focs=0;	
			$('#errordiv').empty();	
			setDefColortoErrorMsg();
			$('#errordiv').append("&#149; Please enter valid Certificate Type based on State<br>");
			if(focs==0)
				$('#certType').focus();
			
			$('#certType').css("background-color",txtBgColor);
			cnt++;focs++;*/	
		}
		else
		{
			$('#errordiv_Certification').empty();	
			setDefColortoErrorMsg_Certification();
		}
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}

function getCertificateNameAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38)
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13)
	{
		
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		//searchArray = getCertificateNameArray(txtSearch.value);
		//fatchData(txtSearch,searchArray,txtId,txtdivid);
		document.getElementById("hrefDone").focus();
		
	} 
	else if(event.keyCode==9)
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getCertificateNameArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getCertificateNameArray(certName){
	
	var certificateTypeMaster = document.getElementById("certificateTypeMaster").value;
		if(certificateTypeMaster=='')
			return;
		
	var searchArray = new Array();	
	DWRAutoComplete.getCertificateNameList(certName,certificateTypeMaster,{ 
		async: false,
		callback: function(data){
		//alert(data);
		hiddenDataArray = new Array();
		showDataArray = new Array();
		for(i=0;i<data.length;i++){
			searchArray[i]=data[i].certName;
			hiddenDataArray[i]=data[i].certNameId;
			showDataArray[i]=data[i].certName;
		}
	}
	});	

	return searchArray;
}

function hideCertificateNameDiv(dis,hiddenId,divId)
{
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		//alert(hiddenId);
		//alert(hiddenDataArray[index]);
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
		//dis.value="";
		
		if(dis.value!="")
		{
			var cnt=0;
			var focs=0;	
			$('#errordiv_Certification').empty();	
			setDefColortoErrorMsg_Certification();
			$('#errordiv_Certification').append("&#149; "+resourceJSON.msgcertificatenamebased+"<br>");
			if(focs==0)
				$('#certName').focus();
			
			$('#certName').css("background-color",txtBgColor);
			cnt++;focs++;	
		}
		else
		{
			$('#errordiv_Certification').empty();	
			setDefColortoErrorMsg_Certification();
		}
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}





var selectFirst="";

var fatchData= function(txtSearch,searchArray,txtId,txtdivid){
	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i] + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
				
			}
			
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}

var downArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index<length-1){
			for(var i=0;i<10;i++){
				{
					if(document.getElementById('divResult'+txtdivid+i))
						var div_id=document.getElementById('divResult'+txtdivid+i);
					if(div_id)
					{
						if(div_id.className=='over')
							index=div_id.id.split('divResult'+txtdivid)[1];
						div_id.className='normal';
					}
				}
			}
			index++;
			if(document.getElementById('divResult'+txtdivid+index))
			{
				var div_id=document.getElementById('divResult'+txtdivid+index);
				div_id.className='over';
				document.getElementById(txtid).value = div_id.innerHTML;
				selectFirst=div_id.innerHTML;
			}
		}
	}
}

var upArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index>0){
			for(var i=0;i<length;i++){

				var div_id=document.getElementById('divResult'+txtdivid+i);
				if(div_id)
				{
					if(div_id.className=='over')
						index=div_id.id.split('divResult'+txtdivid)[1];
					div_id.className='normal';
				}
			}
			index--;
			if(document.getElementById('divResult'+txtdivid+index))
				document.getElementById('divResult'+txtdivid+index).className='over';
			if(txtid && document.getElementById('divResult'+txtdivid+index))
			{
				document.getElementById(txtid).value=
					document.getElementById('divResult'+txtdivid+index).innerHTML;
				selectFirst=document.getElementById('divResult'+txtdivid+index).innerHTML;
			}
		}
	}
}

var overText=function (div_value,txtdivid) {
	for(var i=0;i<length;i++)
	{
		if(document.getElementById('divResult'+i))
		{
			var div_id=document.getElementById('divResult'+i);

			if(div_id.className=='over')
				index=div_id.id.split(txtdivid)[1];
			div_id.className='normal';
		}
	}
	div_value.className = 'over';
	document.getElementById(txtid).value= div_value.innerHTML;
}
function trim(s)
{
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}
