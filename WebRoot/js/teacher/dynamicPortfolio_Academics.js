
//var chkCallFrom=0;

function showGridAcademics()
{
	var isMiami=document.getElementById("isMiami").value;
	var districtIdForDSPQ="";	
	if ($('#districtIdForDSPQ').length > 0) {
		districtIdForDSPQ=document.getElementById("districtIdForDSPQ").value;
	}
	
	var transcriptFlag=trim(document.getElementById("academicTranscriptFlag").value);
	
	if(districtIdForDSPQ==7800038)
	{
		//WOT : without transcript
		PFAcademics.getPFAcademinGridForDspqWOT(dp_Academics_noOfRows,dp_Academics_page,dp_Academics_sortOrderStr,dp_Academics_sortOrderType,isMiami,transcriptFlag,{ 
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{
				$('#divDataGridAcademin').html(data);
				applyScrollOnTbl_Academic();
			}});
	}else{
	PFAcademics.getPFAcademinGridForDspq(dp_Academics_noOfRows,dp_Academics_page,dp_Academics_sortOrderStr,dp_Academics_sortOrderType,isMiami,transcriptFlag,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#divDataGridAcademin').html(data);
			applyScrollOnTbl_Academic();
		}});
	}
}
/*function showForm()
{
	document.getElementById("divFormGrid").style.display="block";
}*/

/*function delRow_academic(id)
{
	if(window.confirm("Are you sure, you would like to delete the record?"))
	{
		PFAcademics.deletePFAcademinGrid(id, { 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			showGridAcademics();
			resetUniversityForm();
			return false;
		}});
	}
}*/

function delRow_academic(id)
{	
	document.getElementById("academicID").value=id;
	$('#deleteAcademicRecord').modal('show');
}
function deleteAcademicRecord(){
	var id=document.getElementById("academicID").value;
	PFAcademics.deletePFAcademinGrid(id,{
	async: true,
	errorHandler:handleError,
	callback:function(data)
	{
		$('#deleteAcademicRecord').modal('hide');
		showGridAcademics();
		resetUniversityForm();
		return false;
	}});
}
var transcriptUpload = false;
function showUniversityForm()
{
	$("#otherUniversityName").hide();
	document.getElementById("aca_file").value="0";
	
	document.getElementById("degreeName").disabled=false;
	document.getElementById("degreeId").disabled=false;
	document.getElementById("degreeType").disabled=false;
	
	document.getElementById("universityId").disabled=false;
	document.getElementById("universityName").disabled=false;
	
	document.getElementById("fieldId").disabled=false;
	document.getElementById("fieldName").disabled=false;
	document.getElementById("attendedInYear").disabled=false;
	document.getElementById("leftInYear").disabled=false;
	
	document.getElementById("gpaFreshmanYear").disabled=false;
	document.getElementById("gpaJuniorYear").disabled=false;     
	document.getElementById("gpaSophomoreYear").disabled=false;     
	document.getElementById("gpaSeniorYear").disabled=false;     
	document.getElementById("gpaCumulative").disabled=false;
	
	document.getElementById("divGPARowOther").disabled=false;
	document.getElementById("gpaCumulative1").disabled=false;
	document.getElementById("divAcademicRow").style.display="block";
	document.getElementById("divDone").style.display="block";
	document.getElementById("divGPARow").style.display="none";
	document.getElementById("divGPARowOther").style.display="none";
	try{document.getElementById("divResumeSection").style.display="none";}catch (e){}
	document.getElementById("academicId").value="";
	document.getElementById("frmAcadamin").reset();
	document.getElementById("frmGPA").reset();
	document.getElementById("frmGPA1").reset();	
	document.getElementById("degreeName").focus();
	
	document.getElementById("universityId").value="";
	document.getElementById("fieldId").value="";
	
	setDefColortoErrorMsg_Academic();
	if($("#gpaOptional").val()=="false" || document.getElementById("districtIdForDSPQ").value==3904380 || document.getElementById("districtIdForDSPQ").value==1201470 || document.getElementById("districtIdForDSPQ").value==7800040 || document.getElementById("districtIdForDSPQ").value==7800047 || document.getElementById("districtIdForDSPQ").value==3700690 || ($("#headQuaterIdForDspq").val()==2 &&($("#dspqName").val()=="Option A" || $("#dspqName").val()=="Option B")) ){
		$("#crequired").css("color", "white");
		$("#crequired2").css("color", "white");
	}
	if(document.getElementById("districtIdForDSPQ").value==5304860){
		$("#transUploadTooltip").attr("data-original-title", "Copy of official college transcript or paraprofessional assessment test upload, upload high school transcript.");
	}else if(document.getElementById("districtIdForDSPQ").value==1302010 && ($("#jobcategoryDsp").val()=="Classified" || $("#jobcategoryDsp").val()=="Substitutes")){
		$("#transUploadTooltip").attr("data-original-title", "Paraprofessional applicants must include transcripts for all education above high school level.");
	}else{
		$("#transUploadTooltip").attr("data-original-title", "Please upload your transcript. Transcripts can be unofficial.");
	}
	if(document.getElementById("districtIdForDSPQ").value==3904493  || document.getElementById("districtIdForDSPQ").value==7800049 || document.getElementById("districtIdForDSPQ").value==7800048 || document.getElementById("districtIdForDSPQ").value==7800050 || document.getElementById("districtIdForDSPQ").value==7800051 || document.getElementById("districtIdForDSPQ").value==7800053){
		$("#transcriptDiv").hide();
		$("#divGPARow").hide();
		$("#divGPARowOther").hide();
	}
	$(".acadDatesReq").css("color", "red");
	if(document.getElementById("districtIdForDSPQ").value==804800){
		$(".acadDatesReq").css("color", "white");
	}
	if(document.getElementById("districtIdForDSPQ").value==614730){
		$("#transcriptDiv").hide();
	}
	if(transcriptUpload || ($("#districtIdForDSPQ").val()==806810 && ($('#jobcategoryDsp').val()=="Support Staff" || $('#jobcategoryDsp').val()=="Coaching"  || $('#jobcategoryDsp').val()=="Paraprofessional")))
	{
		$("#transcriptDiv").hide();
	}
	$('#errordiv_AcademicForm').empty();
	return false;
}

function resetUniversityForm()
{
	
	document.getElementById("universityId").value="";
	document.getElementById("fieldId").value="";
	
	document.getElementById("divAcademicRow").style.display="none";
	document.getElementById("divDone").style.display="none";
	document.getElementById("divGPARow").style.display="none";
	document.getElementById("divGPARowOther").style.display="none";
	document.getElementById("frmAcadamin").reset();
	window.location.hash = '#addSchoolIdDSPQ';
	return false;
}




function insertOrUpdate_Academic(sbtsource)
{ 
	//$('#testcall').append("&#149; insertOrUpdate_Academic<br>");
	if(sbtsource=="0")
	{
		resetSBTNSource();
	}
	
	var threadCount_aca=document.getElementById("threadCount_aca").value;
	if(threadCount_aca=="0")
	{
		document.getElementById("threadCount_aca").value=1;
		
		updateThreadCount("aca");
		
		var academicId = document.getElementById("academicId");
		var degreeId = document.getElementById("degreeId");
		var degreeName = document.getElementById("degreeName");
		var universityId = document.getElementById("universityId");
		var universityName = document.getElementById("universityName");
		var fieldId = document.getElementById("fieldId");     
		var fieldName = document.getElementById("fieldName");     
		var attendedInYear = document.getElementById("attendedInYear"); 
		var leftInYear = document.getElementById("leftInYear");     
		var gpaFreshmanYear = document.getElementById("gpaFreshmanYear");     
		var gpaJuniorYear = document.getElementById("gpaJuniorYear");     
		var gpaSophomoreYear = document.getElementById("gpaSophomoreYear");     
		var gpaSeniorYear = document.getElementById("gpaSeniorYear");     
		var gpaCumulative = document.getElementById("gpaCumulative"); 
		var degreeType = document.getElementById("degreeType");
		var isMiamiChk=document.getElementById("isMiami").value;
		var international=0;
		var otherUniversityName=trim($("#otherUniversityName").val());
		
		//alert(degreeType.value+"   "+document.document.getElementById('international2').checked);
        if(degreeType.value=="B" && document.getElementById('international').checked){
            international=1;
        }else if(degreeType.value=="M" && document.getElementById('international2').checked){
        	international=1;        	
        }else if(degreeType.value=="A" && document.getElementById('international2').checked){
        	international=1;        	
        }else if(degreeType.value=="D" && document.getElementById('international2').checked){
        	international=1;        	
        }
		var pathOfTranscript="";
		var ext="";
		var fileName="";

		if(isMiamiChk=='false')
		{
			pathOfTranscript = document.getElementById("pathOfTranscript");
			fileName = pathOfTranscript.value;
			
			ext = fileName.substr(fileName.lastIndexOf('.') + 1).toLowerCase();
			var fileSize=0;		
			if ($.browser.msie==true)
		 	{	
			    fileSize = 0;	   
			}
			else
			{		
				if(pathOfTranscript.files[0]!=undefined)
					fileSize = pathOfTranscript.files[0].size;
			}
		}
			
		
		var cnt=0;
		var focs=0;	
		
		$('#errordiv_AcademicForm').empty();
		setDefColortoErrorMsg_Academic();	
		if(trim(degreeName.value)=="" && $("#degreeOptional").val()=="true")
		{
			$('#errordiv_AcademicForm').append("&#149; Please enter Degree<br>");
			if(focs==0)
				$('#degreeName').focus();
			
			$('#degreeName').css("background-color",txtBgColor);
			cnt++;focs++;
		}
		var GEDFlag=false;
		
		if($('#degreeName').val()=="High School or GED"){
			GEDFlag=true;
		}
		/*if(document.getElementById("districtIdForDSPQ").value==7800031){
			GEDFlag=false;		
		}*/
		if($('#degreeName').val()!="No Degree"){
					if(trim(universityName.value)=="" && GEDFlag==false && $("#schoolOptional").val()=="true")
					{
						$('#errordiv_AcademicForm').append("&#149; Please enter School<br>");
						if(focs==0)
							$('#universityName').focus();
						
						$('#universityName').css("background-color",txtBgColor);
						cnt++;focs++;
					}
					if(resourceJSON.platformName=="NC" && trim(universityName.value)=="Other" && GEDFlag==false && $("#schoolOptional").val()=="true" && otherUniversityName=="")
					{
						$('#errordiv_AcademicForm').append("&#149; Please enter Other School<br>");
						if(focs==0)
							$('#otherUniversityName').focus();
						
						$('#otherUniversityName').css("background-color",txtBgColor);
						cnt++;focs++;
					}
					
					if(!(document.getElementById("districtIdForDSPQ").value==2633090 && ($('#jobcategoryDsp').val()=="Drivers" || $('#jobcategoryDsp').val()=="Other" || $('#jobcategoryDsp').val()=="Clerical")))
						if(trim(fieldName.value)=="" && GEDFlag==false && $("#fieldOfStudyOptional").val()=="true")
						{
							$('#errordiv_AcademicForm').append("&#149; Please enter Field of Study<br>");
							if(focs==0)
								$('#fieldName').focus();
							
							$('#fieldName').css("background-color",txtBgColor);
							cnt++;focs++;
						}					
					if($("#academicsDatesOptional").val()=="true")
					if((document.getElementById("districtIdForDSPQ").value==806900 && !GEDFlag) && (document.getElementById("districtIdForDSPQ").value!=804800) || ($("#headQuaterIdForDspq").val()==2 &&($("#dspqName").val()=="Option C" || $("#dspqName").val()=="Option D"))){
							if(degreeId.value=="46"){$('#showhide1').hide();$('#showhide2').hide();$('#showhide3').hide();$('#showhide4').hide();}
							else{
								$('#showhide1').show();$('#showhide2').show();$('#showhide3').show();$('#showhide4').show();
								
						if(trim(attendedInYear.value)=="")
						{
							$('#errordiv_AcademicForm').append("&#149; Please enter Dates Attended from<br>");
							if(focs==0)
								$('#attendedInYear').focus();
							
							$('#attendedInYear').css("background-color",txtBgColor);
							cnt++;focs++;
						}
							
						if(trim(leftInYear.value)=="")
						{
							$('#errordiv_AcademicForm').append("&#149; Please enter Dates Attended to<br>");
							if(focs==0)
								$('#leftInYear').focus();
							
							$('#leftInYear').css("background-color",txtBgColor);
							cnt++;focs++;
						}
							}
					}
					if((trim(attendedInYear.value)!="") && (trim(leftInYear.value)!="") &&(trim(attendedInYear.value) > trim(leftInYear.value)))
					{
						$('#errordiv_AcademicForm').append("&#149; Dates Attended from cannot be greater than Dates Attended to<br>");
						if(focs==0)
							$('#attendedInYear').focus();
						
						$('#attendedInYear').css("background-color",txtBgColor);
						$('#leftInYear').css("background-color",txtBgColor);
						cnt++;focs++;
					}
					if(GEDFlag==false && $("#transcriptOptional").val()=="1"){
						if(trim(pathOfTranscript.value)=="")
							{
								var aca_file=0;
								try {
									aca_file=document.getElementById("aca_file").value;
								} catch (e) {
									// TODO: handle exception
								}
								
								if(aca_file=="0")
								{
									$('#errordiv_AcademicForm').append("&#149; Please upload Transcript<br>");
									if(focs==0)
										$('#pathOfTranscript').focus();
									
									$('#pathOfTranscript').css("background-color",txtBgColor);
									cnt++;focs++;
								}
							}
					}
					if(document.getElementById("districtIdForDSPQ").value!=806900 && document.getElementById("districtIdForDSPQ").value!=804800 && document.getElementById("districtIdForDSPQ").value!=7800031)
					{
						
						if(GEDFlag==true){	
							if(trim(pathOfTranscript.value)=="")
							{
								
								var aca_file=0;
								try {
									aca_file=document.getElementById("aca_file").value;
								} catch (e) {
									// TODO: handle exception
								}
								
								if(aca_file=="0")
								{
									$('#errordiv_AcademicForm').append("&#149; Please upload Transcript<br>");
									if(focs==0)
										$('#pathOfTranscript').focus();
									
									$('#pathOfTranscript').css("background-color",txtBgColor);
									cnt++;focs++;
								}
							}
							/*else if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt'))
							{
								$('#errordiv_AcademicForm').append("&#149; Please select Acceptable resume formats include PDF, MS-Word, GIF, PNG, and JPEG  files.<br>");
									if(focs==0)
										$('#pathOfTranscript').focus();
									
									$('#pathOfTranscript').css("background-color",txtBgColor);
									cnt++;focs++;	
									return false;
							}*/
						}
					}
					
					
		}

		/*if(document.getElementById("districtIdForDSPQ").value==4218990){
			if($('#isSchoolSupportPhiladelphia').val()!="")
			if(document.getElementById("pathOfTranscript").value=="" && $('#divResumerName').html()==""){		
				$('#errordiv_AcademicForm').append("&#149;  Please upload Transcript<br>");
				if(focs==0)
					$('#pathOfTranscript').focus();
				
				$('#pathOfTranscript').css("background-color",txtBgColor);
				cnt++;focs++;
			}
		}
		*/
		if(ext!="")
		if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt'))
		{
			$('#errordiv_AcademicForm').append("&#149; Acceptable file formats include PDF, MS-Word, GIF, PNG, and JPEG files<br>");
				if(focs==0)
					$('#pathOfTranscript').focus();
				
				$('#pathOfTranscript').css("background-color",txtBgColor);
				cnt++;focs++;	
				return false;
		}	
		else if(fileSize>=10485760)
		{
			$('#errordiv_AcademicForm').append("&#149; File size must be less than 10mb<br>");
				if(focs==0)
					$('#pathOfTranscript').focus();
				
				$('#pathOfTranscript').css("background-color",txtBgColor);
				cnt++;focs++;	
				return false;
		}
		
		
		if($("#divGPARow").is(':visible') || $("#divGPARowOther").is(':visible'))		
		if(trim(degreeType.value)=="B")
		{	
			
			
			if(trim(gpaFreshmanYear.value)=="" || trim(gpaFreshmanYear.value)==".")
			{
				/*$('#errordiv').append("&#149; Please enter valid Freshman GPA<br>");
				if(focs==0)
					$('#gpaFreshmanYear').focus();
				
				$('#gpaFreshmanYear').css("background-color",txtBgColor);
				cnt++;focs++;*/
			}
			else if(parseFloat(trim(gpaFreshmanYear.value))>5.00)
			{
				$('#errordiv_AcademicForm').append("&#149; Freshman GPA must be less than or equal to 5.00<br>");
				if(focs==0)
					$('#gpaFreshmanYear').focus();
				
				$('#gpaFreshmanYear').css("background-color",txtBgColor);
				cnt++;focs++;
			}
			
			if(trim(gpaSophomoreYear.value)=="" || trim(gpaSophomoreYear.value)==".")
			{
				/*$('#errordiv').append("&#149; Please enter valid Sophomore GPA<br>");
				if(focs==0)
					$('#gpaSophomoreYear').focus();
				
				$('#gpaSophomoreYear').css("background-color",txtBgColor);
				cnt++;focs++;*/
			}
			else if(parseFloat(trim(gpaSophomoreYear.value))>5.00)
			{
				$('#errordiv_AcademicForm').append("&#149; Sophomore GPA must be less than or equal to 5.00<br>");
				if(focs==0)
					$('#gpaSophomoreYear').focus();
				
				$('#gpaSophomoreYear').css("background-color",txtBgColor);
				cnt++;focs++;
			}
			
			if(trim(gpaJuniorYear.value)=="" || trim(gpaJuniorYear.value)==".")
			{
				/*$('#errordiv').append("&#149; Please enter valid Junior GPA<br>");
				if(focs==0)
					$('#gpaJuniorYear').focus();
				
				$('#gpaJuniorYear').css("background-color",txtBgColor);
				cnt++;focs++;*/
			}
			else if(parseFloat(trim(gpaJuniorYear.value))>5.00)
			{
				$('#errordiv_AcademicForm').append("&#149; Junior GPA must be less than or equal to 5.00<br>");
				if(focs==0)
					$('#gpaJuniorYear').focus();
				
				$('#gpaJuniorYear').css("background-color",txtBgColor);
				cnt++;focs++;
			}
				
			if(trim(gpaSeniorYear.value)=="" || trim(gpaSeniorYear.value)==".")
			{
				/*$('#errordiv').append("&#149; Please enter valid Senior GPA<br>");
				if(focs==0)
					$('#gpaSeniorYear').focus();
				
				$('#gpaSeniorYear').css("background-color",txtBgColor);
				cnt++;focs++;*/
			}
			else if(parseFloat(trim(gpaSeniorYear.value))>5.00)
			{
				$('#errordiv_AcademicForm').append("&#149; Senior GPA must be less than or equal to 5.00<br>");
				if(focs==0)
					$('#gpaSeniorYear').focus();
				
				$('#gpaSeniorYear').css("background-color",txtBgColor);
				cnt++;focs++;
			}
			gpaCumulative = document.getElementById("gpaCumulative").value;
			if(document.getElementById("districtIdForDSPQ").value==4218990){
				var nonteacherFlag=$("#isnontj").val();
				if($('#isSchoolSupportPhiladelphia').val()!=""){}
				else if(nonteacherFlag=="" || nonteacherFlag!="true"){
					if(trim(gpaCumulative)=="" || trim(gpaCumulative)==".")
					{
						if(!document.getElementById('international').checked){
						$('#errordiv_AcademicForm').append("&#149; Please enter valid Cumulative  GPA<br>");
						if(focs==0)
						$('#gpaCumulative').focus();

						$('#gpaCumulative').css("background-color",txtBgColor);
						cnt++;focs++;
						}
					}
					else if(parseFloat(trim(gpaCumulative))>5.00)
					{
						$('#errordiv_AcademicForm').append("&#149; Cumulative GPA must be less than or equal to 5.00<br>");
						if(focs==0)
						$('#gpaCumulative').focus();

						$('#gpaCumulative').css("background-color",txtBgColor);
						cnt++;focs++;
					}

				}
			}else if($("#gpaOptional").val()=="false" || document.getElementById("districtIdForDSPQ").value==3904380 || document.getElementById("districtIdForDSPQ").value==3700112 || document.getElementById("districtIdForDSPQ").value==1201470 || document.getElementById("districtIdForDSPQ").value==7800040 || document.getElementById("districtIdForDSPQ").value==7800047
					 || document.getElementById("districtIdForDSPQ").value==804800 || document.getElementById("districtIdForDSPQ").value==3700690 || (document.getElementById("districtIdForDSPQ").value==3703120 && $('#jobcategoryDsp').val().trim().indexOf("Bus Driver") !=-1) || (document.getElementById("districtIdForDSPQ").value==3703120 && $('#jobcategoryDsp').val().trim().indexOf("Substitute") !=-1) || $("#headQuaterIdForDspq").val()==2 && ($("#dspqName").val()=="Option A" || $("#dspqName").val()=="Option B")){
				if(parseFloat(trim(gpaCumulative))>5.00)
				{
					$('#errordiv_AcademicForm').append("&#149; Cumulative GPA must be less than or equal to 5.00<br>");
					if(focs==0)
					$('#gpaCumulative').focus();

					$('#gpaCumulative').css("background-color",txtBgColor);
					cnt++;focs++;
				}

			}else{
				if(trim(gpaCumulative)=="" || trim(gpaCumulative)==".")
				{
					if(!document.getElementById('international').checked){
					$('#errordiv_AcademicForm').append("&#149; Please enter valid Cumulative  GPA<br>");
					if(focs==0)
					$('#gpaCumulative').focus();

					$('#gpaCumulative').css("background-color",txtBgColor);
					cnt++;focs++;
					}
				}
				else if(parseFloat(trim(gpaCumulative))>5.00)
				{
					$('#errordiv_AcademicForm').append("&#149; Cumulative GPA must be less than or equal to 5.00<br>");
					if(focs==0)
					$('#gpaCumulative').focus();

					$('#gpaCumulative').css("background-color",txtBgColor);
					cnt++;focs++;
				}
			}
		}
		else if(GEDFlag==false)			
		{	
			gpaCumulative = document.getElementById("gpaCumulative1").value;
			if($('#degreeName').val()!="No Degree")
				if($("#gpaOptional").val()=="false" || document.getElementById("districtIdForDSPQ").value==3904380 || document.getElementById("districtIdForDSPQ").value==3700112 || document.getElementById("districtIdForDSPQ").value==1201470 || document.getElementById("districtIdForDSPQ").value==7800040 || document.getElementById("districtIdForDSPQ").value==7800047
						 || document.getElementById("districtIdForDSPQ").value==804800 || document.getElementById("districtIdForDSPQ").value==3700690 || (document.getElementById("districtIdForDSPQ").value==3703120 && $('#jobcategoryDsp').val().trim().indexOf("Bus Driver") !=-1) || (document.getElementById("districtIdForDSPQ").value==3703120 && $('#jobcategoryDsp').val().trim().indexOf("Substitute") !=-1)){
					if(trim(degreeName.value)!="" && parseFloat(trim(gpaCumulative))>5.00)
					{
						$('#errordiv_AcademicForm').append("&#149; Cumulative GPA must be less than or equal to 5.00<br>");
								if(focs==0)
									$('#gpaCumulative1').focus();
								
								$('#gpaCumulative1').css("background-color",txtBgColor);
								cnt++;focs++;
					}
				}
			else if(document.getElementById("districtIdForDSPQ").value==4218990){
			var nonteacherFlag=$("#isnontj").val();
			if($('#isSchoolSupportPhiladelphia').val()!=""){}
			else if(nonteacherFlag=="" || nonteacherFlag!="true"){

				if((trim(degreeName.value)!="") && (trim(gpaCumulative)=="" || trim(gpaCumulative)=="."))
				{
					if(!document.getElementById('international2').checked){
								$('#errordiv_AcademicForm').append("&#149; Please enter valid Cumulative GPA<br>");
								if(focs==0)
									$('#gpaCumulative1').focus();
								
								$('#gpaCumulative1').css("background-color",txtBgColor);
								cnt++;focs++;
							}
				}
				else if(trim(degreeName.value)!="" && parseFloat(trim(gpaCumulative))>5.00)
				{
					$('#errordiv_AcademicForm').append("&#149; Cumulative GPA must be less than or equal to 5.00<br>");
							if(focs==0)
								$('#gpaCumulative1').focus();
							
							$('#gpaCumulative1').css("background-color",txtBgColor);
							cnt++;focs++;
				}
			}
			}else{
				if((trim(degreeName.value)!="") && (trim(gpaCumulative)=="" || trim(gpaCumulative)=="."))
				{
					if(!document.getElementById('international2').checked){
						$('#errordiv_AcademicForm').append("&#149; Please enter valid Cumulative GPA<br>");
						if(focs==0)
							$('#gpaCumulative1').focus();
						
						$('#gpaCumulative1').css("background-color",txtBgColor);
						cnt++;focs++;
					}
				}
				else if(trim(degreeName.value)!="" && parseFloat(trim(gpaCumulative))>5.00)
				{
					$('#errordiv_AcademicForm').append("&#149; Cumulative GPA must be less than or equal to 5.00<br>");
					if(focs==0)
						$('#gpaCumulative1').focus();
					
					$('#gpaCumulative1').css("background-color",txtBgColor);
					cnt++;focs++;
				}
			}
		}
		
		if((!$("#divGPARow").is(':visible') && !$("#divGPARowOther").is(':visible')) || gpaCumulative=="" && $("#headQuaterIdForDspq").val()==2 && ($("#dspqName").val()=="Option A" || $("#dspqName").val()=="Option B") || gpaCumulative.value==""){
			gpaCumulative="0.0";
		}
		
		if(cnt!=0)		
		{
			updateReturnThreadCount("aca");
			$('#errordiv_AcademicForm').show();
			return false;
		}
		else
		{	
			if(fileName!="")
			{
				//$('#loadingDiv').show();
				document.getElementById("frmAcadamin").submit();
			}
			else
			{
		
				PFAcademics.saveOrUpdateAcademics(academicId.value,degreeId.value,universityId.value,fieldId.value,attendedInYear.value,leftInYear.value,gpaFreshmanYear.value,gpaJuniorYear.value,gpaSophomoreYear.value,gpaSeniorYear.value,gpaCumulative,degreeType.value,ext,international,otherUniversityName,
				{ 
				async: false,
				errorHandler:handleError,
				callback: function(data)
					{	
						showGridAcademics();
						/*if(ext!="")
						{
							$('#loadingDiv').show();
							document.getElementById("frmAcadamin").submit();
						}*/				
						$("#hrefAcademics").addClass("stepactive");
						resetUniversityForm();
						if(sbtsource==1)
						{
							saveAllDSPQ();
							updateReturnThreadCount("aca");
							validatePortfolioErrorMessageAndGridData('level2');
						}
					}
				});
			}
		}
	}
	
}

function saveAcademics(fileName,sbtsource)
{
	//$('#testcall').append("&#149; saveAcademics<br>");
	var academicId = document.getElementById("academicId");
	
	var degreeId = document.getElementById("degreeId");
	var universityId = document.getElementById("universityId");
	var fieldId = document.getElementById("fieldId");   
	
	var attendedInYear = document.getElementById("attendedInYear"); 
	var leftInYear = document.getElementById("leftInYear");     
	var gpaFreshmanYear = document.getElementById("gpaFreshmanYear");     
	var gpaJuniorYear = document.getElementById("gpaJuniorYear");     
	var gpaSophomoreYear = document.getElementById("gpaSophomoreYear");     
	var gpaSeniorYear = document.getElementById("gpaSeniorYear");     
	var gpaCumulative = ""; 
	var degreeType = document.getElementById("degreeType");
	if(trim(degreeType.value)=="B"){
		gpaCumulative = document.getElementById("gpaCumulative");
	}else{
		gpaCumulative = document.getElementById("gpaCumulative1")
	}
	var international=0;
	if(degreeType.value=="B" && document.getElementById('international').checked){
        international=1;
    }else if(degreeType.value=="M" && document.getElementById('international2').checked){
    	international=1;        	
    }else if(degreeType.value=="A" && document.getElementById('international2').checked){
    	international=1;        	
    }else if(degreeType.value=="D" && document.getElementById('international2').checked){
    	international=1;        	
    }
	
	PFAcademics.saveOrUpdateAcademics(academicId.value,degreeId.value,universityId.value,fieldId.value,
			attendedInYear.value,leftInYear.value,gpaFreshmanYear.value,gpaJuniorYear.value,
			gpaSophomoreYear.value,gpaSeniorYear.value,gpaCumulative.value,degreeType.value,fileName,international,"",
	{ 
	async: false,
	errorHandler:handleError,
	callback: function(data)
		{	
			//$('#loadingDiv').hide();
			showGridAcademics();
			$("#hrefAcademics").addClass("stepactive");
			resetUniversityForm();
			if(sbtsource==1)
			{
				saveAllDSPQ();
				updateReturnThreadCount("aca");
				validatePortfolioErrorMessageAndGridData('level2');
			}
		}
	});
	
	/*if(finalcallStatus==1)
	{
		sendToOtherPager();
	}*/
}

/*function sendToOtherPager()
{
	
	$('#loadingDiv').hide();
	if(chkCallFrom==1)
	{
		window.location.href="certification.do";
	}
}
*/

function showRecordToForm(academicId,statusCheck)
{

	document.getElementById("frmAcadamin").reset();	
	document.getElementById("divGPARowOther").style.display="none";
	document.getElementById("international2").checked=false;
	document.getElementById("international").checked=false;
	PFAcademics.showEditAcademics(academicId,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
	{
		//alert(data.leftInYear)
		//alert(data.degreeId.degreeId)
		//alert(data.degreeId.degreeName)
	
		document.getElementById("divAcademicRow").style.display="block";
		document.getElementById("divDone").style.display="block";
			
		document.getElementById("academicId").value=data.academicId;
		document.getElementById("degreeName").value=data.degreeId.degreeName;
		document.getElementById("degreeId").value=data.degreeId.degreeId;
		document.getElementById("degreeType").value=data.degreeId.degreeType;
		
		if(data.universityId!=null){
		document.getElementById("universityId").value=data.universityId.universityId;		
		document.getElementById("universityName").value=data.universityId.universityName;
		if(resourceJSON.platformName=="NC" && data.universityId.universityName=="Other")
		{
			$("#otherUniversityName").val(data.otherUniversityName);
			$("#otherUniversityName").show();
		}
		else
		{
			$("#otherUniversityName").hide();
		}	
			
		}
		
		if(data.fieldId!=null){
		document.getElementById("fieldId").value=data.fieldId.fieldId;
		document.getElementById("fieldName").value=data.fieldId.fieldName;
		}
		
		
		if(data.degreeId.degreeType=="M" && data.international){
			document.getElementById("international2").checked=data.international;
		}else if(data.degreeId.degreeType=="A" && data.international){
			document.getElementById("international2").checked=data.international;
		}else if(data.degreeId.degreeType=="D" && data.international){
			document.getElementById("international2").checked=data.international;
		}else{
			document.getElementById("international").checked=data.international;
		}
		
		var isMiamiChk=document.getElementById("isMiami").value;
		
		//alert(data.pathOfTranscript)
		
		//alert(" statusCheck :: "+statusCheck);
		
		if(isMiamiChk=='false')
		{
			if(statusCheck=='V')
			{
				document.getElementById("degreeName").disabled=true;
				document.getElementById("degreeId").disabled=true;
				document.getElementById("degreeType").disabled=true;
				document.getElementById("universityId").disabled=true;
				document.getElementById("universityName").disabled=true;
				document.getElementById("fieldId").disabled=true;
				document.getElementById("fieldName").disabled=true;
				document.getElementById("attendedInYear").disabled=true;
				document.getElementById("leftInYear").disabled=true;
				
				//alert(data.pathOfTranscript);
				if(data.pathOfTranscript!=null && data.pathOfTranscript!="")
				{
					document.getElementById("divResumeSection").style.display="block";
					document.getElementById("divResumerName").innerHTML="<a href='javascript:void(0)' id='hrefEditTrans' onclick=\"downloadTranscript('"+data.academicId+"','hrefEditTrans');if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;\">"+data.pathOfTranscript+"</a>";
					document.getElementById("aca_file").value="1";
					
				}
				else
				{
					document.getElementById("divResumeSection").style.display="none";
					//document.getElementById("divResumerName").innerHTML = "";
					document.getElementById("aca_file").value="0";
				}
			}
			else
			{
				document.getElementById("degreeName").disabled=false;
				document.getElementById("degreeId").disabled=false;
				document.getElementById("degreeType").disabled=false;
				
				document.getElementById("universityId").disabled=false;
				document.getElementById("universityName").disabled=false;
				
				document.getElementById("fieldId").disabled=false;
				document.getElementById("fieldName").disabled=false;
				document.getElementById("attendedInYear").disabled=false;
				document.getElementById("leftInYear").disabled=false;
				
				if(data.pathOfTranscript!=null && data.pathOfTranscript!="")
				{
					document.getElementById("divResumeSection").style.display="block";
					document.getElementById("divResumerName").innerHTML="<a href='javascript:void(0)' id='hrefEditTrans' onclick=\"downloadTranscript('"+data.academicId+"','hrefEditTrans');if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;\">"+data.pathOfTranscript+"</a>";
					document.getElementById("aca_file").value="1";
				}
				else
				{
					document.getElementById("divResumeSection").style.display="none";
					document.getElementById("aca_file").value="0";
					//document.getElementById("divResumerName").innerHTML = "";
				}
			}
			
		}
		
		if(data.attendedInYear!=null)
		document.getElementById("attendedInYear".toString()+(data.attendedInYear)).selected=true;
		if(data.leftInYear!=null)
		document.getElementById("leftInYear".toString()+(data.leftInYear)).selected=true;
		
		if(document.getElementById("degreeType").value=="B" || document.getElementById("degreeType").value=="b")
		{
			document.getElementById("divGPARow").style.display="block";
			document.getElementById("gpaFreshmanYear").value =  data.gpaFreshmanYear;
			document.getElementById("gpaJuniorYear").value =  data.gpaJuniorYear;     
			document.getElementById("gpaSophomoreYear").value =  data.gpaSophomoreYear;     
			document.getElementById("gpaSeniorYear").value =  data.gpaSeniorYear;     
			document.getElementById("gpaCumulative").value =  data.gpaCumulative;
			
			if(isMiamiChk=='false' && statusCheck=='V')
			{
				document.getElementById("gpaFreshmanYear").disabled=true;
				document.getElementById("gpaJuniorYear").disabled=true;     
				document.getElementById("gpaSophomoreYear").disabled=true;     
				document.getElementById("gpaSeniorYear").disabled=true;     
				document.getElementById("gpaCumulative").disabled=true;
			}
			else
			{
				document.getElementById("gpaFreshmanYear").disabled=false;
				document.getElementById("gpaJuniorYear").disabled=false;     
				document.getElementById("gpaSophomoreYear").disabled=false;     
				document.getElementById("gpaSeniorYear").disabled=false;     
				document.getElementById("gpaCumulative").disabled=false;
			}
		}else if(document.getElementById("degreeType").value=="ND"){
			document.getElementById("divGPARow").style.display="none";
			document.getElementById("divGPARowOther").style.display="block";
			document.getElementById("gpaCumulative1").value =  data.gpaCumulative;
		}
		else
		{
			document.getElementById("divGPARow").style.display="none";
			document.getElementById("divGPARowOther").style.display="block";
			document.getElementById("gpaCumulative1").value =  data.gpaCumulative;
			
			if(isMiamiChk=='false' && statusCheck=='V')
			{
				document.getElementById("divGPARowOther").disabled=true;
				document.getElementById("gpaCumulative1").disabled=true;
			}
			else
			{
				document.getElementById("divGPARowOther").disabled=false;
				document.getElementById("gpaCumulative1").disabled=false;
			}
		}
		if(data.degreeId.degreeName=="High School or GED"){
			document.getElementById('universityName').disabled=true;
			document.getElementById('fieldName').disabled=true;
			document.getElementById('gpaCumulative1').disabled=true;
		}else{
			document.getElementById('universityName').disabled=false;
			document.getElementById('fieldName').disabled=false;
			document.getElementById('gpaCumulative1').disabled=false;
		}
		
		document.getElementById("degreeName").focus();
		if(document.getElementById("districtIdForDSPQ").value==7800049 || document.getElementById("districtIdForDSPQ").value==7800048 || document.getElementById("districtIdForDSPQ").value==7800050 || document.getElementById("districtIdForDSPQ").value==7800051 || document.getElementById("districtIdForDSPQ").value==7800053){
			$("#transcriptDiv").hide();
			$("#divGPARow").hide();
			document.getElementById("divGPARowOther").style.display="block";
		}
		checkGED();
	}})
	return false;
}
function downloadTranscript(academicId, linkId)
{	
		PFAcademics.downloadTranscript(academicId,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	if(data=="")
			{
				data="javascript:void(0)";
			}
			else if(data.indexOf(".doc")!=-1)
			{
				document.getElementById("ifrmTrans").src=data;
			}
			else
			{
				document.getElementById(linkId).href = data;
			}
			return false;
		}});
}



/*function saveAndContinueAcademics()
{
	var idList = new Array("degreeName","universityName","fieldName");
	var count = 0;
	for(i=0;i<idList.length;i++)
	{
		if(trim(document.getElementById(idList[i]).value)!="")
		{
			count++;	
		}
	}
	if(count==0)
	{
		PFAcademics.updateAcademicsPortfolioStatus( { 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			
		}});
		
	}
	else
	{
		//chkCallFrom=1;
		insertOrUpdate_Academic();	
		return false;
	}
	
	
}*/

function setDefColortoErrorMsg_Academic()
{
	//$('#divServerError').css("background-color","");	
	$('#degreeName').css("background-color","");
	$('#universityName').css("background-color","");
	$('#fieldName').css("background-color","");
	$('#attendedInYear').css("background-color","");
	$('#leftInYear').css("background-color","");
	$('#pathOfTranscript').css("background-color","");	
	$('#gpaFreshmanYear').css("background-color","");
	$('#gpaJuniorYear').css("background-color","");
	$('#gpaSophomoreYear').css("background-color","");
	$('#gpaSeniorYear').css("background-color","");
	$('#gpaCumulative').css("background-color","");
	$('#gpaCumulative1').css("background-color","");
	$('#otherUniversityName').css("background-color","");
	
}

/*function testAuto()
{
	
	//alert(document.getElementById("degreeType").value)
	//alert(">>"+document.getElementById("degreeId").value);
	DWRAutoComplete.getFieldOfStudyList("h", function(data)
	{
		alert(data);
	});
}*/


/*function uploadFiles() 
{
	//alert("upload")
	
  var image = document.getElementById('imgToUpload');
	
	//alert(image)

  PFAcademics.uploadFiles(image,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data) {
	  //alert(data)
   // dwr.util.setValue('image', data);
  }});
	
	
}*/

function removeTranscript()
{
	
	var academicId = document.getElementById("academicId").value;
	if(window.confirm("Are you sure, you would like to delete the Transcript?"))
	{
		PFAcademics.removeTranscript(academicId,
		{ 
			async: false,
			errorHandler:handleError,
			callback: function(data)
			{	
				//alert(data)
				showGridAcademics();
				document.getElementById("divResumeSection").style.display="none";
				
			}
		});
	}
}
function clearTranscript()
{
	document.getElementById("pathOfTranscript").value=null;
	$('#pathOfTranscript').css("background-color","");	
}

function chkForEnter_Academic(evt)
{	
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==13)
	{
		insertOrUpdate_Academic();
	}	
}

function chkInternational()
{
      if (document.getElementById('international').checked)
      {
             document.getElementById("crequired").innerHTML = ""; 
             document.getElementById("crequired2").innerHTML = "";
       }
      else
      {
            document.getElementById("crequired").innerHTML ="*";
            document.getElementById("crequired2").innerHTML ="*";
      }
}

function checkGED(){
	if($('#degreeName').val()=="High School or GED"){
		//document.getElementById('universityName').disabled=true;
		document.getElementById('fieldName').disabled=true;
		//document.getElementById('gpaCumulative1').disabled=true;
		
		if(document.getElementById("districtIdForDSPQ").value==804800)
		{
			//document.getElementById('attendedInYear').disabled=true;
			//document.getElementById('leftInYear').disabled=true;
			try { document.getElementById('gpaCumulative1').disabled=false; } catch (e) {}
			try { document.getElementById('gpaCumulative').disabled=false; } catch (e) {}
			try { document.getElementById('universityName').disabled=false; } catch (e) {}
		}
	}else{
		document.getElementById('universityName').disabled=false;
		document.getElementById('fieldName').disabled=false;
		document.getElementById('gpaCumulative1').disabled=false;
		
		if(document.getElementById("districtIdForDSPQ").value==804800)
		{
			document.getElementById('attendedInYear').disabled=false;
			document.getElementById('leftInYear').disabled=false;
		}
	}
}

///start sandeep 

function applyScrollOnEducation()
{
		var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#educationGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 350,
        width: 953,
        minWidth: null,
        minWidthAuto: false,
        colratio:[305,200,200,260], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });            
        });
}




function getEducation(){
	
	var ssn = document.getElementById("ssn_pi").value;
	//document.getElementById("divLblEducation").style.display="block";
	//element = document.getElementById('loadEducation');
	//element.style.display = "none";
	if(ssn!=null || ssn=='')
	{
	
	PFLicenseEducation.getLicenseEducation(ssn,
			{ 
				async: false,
				errorHandler:handleError,
				callback: function(data)
				{	
			
					document.getElementById("divDataEducation").innerHTML=data;
					if(data.indexOf("No record found")==-1)
					{
						document.getElementById("divDataEducationVal").value=1;
					}
					else
					{
						document.getElementById("divDataEducationVal").value=0;
					}	
					document.getElementById("divLblEducation").style.display="block";
					applyScrollOnEducation();
					//document.getElementById("divAddEducation").style.display="block";
					
				}
			});
	}
	
}	



function showEducation(){
	
	showEducationForm();
	
	
}
function showEducationForm()
{
	document.getElementById("frmEducation").reset();
	document.getElementById("licensureeducationId").value="";	
	document.getElementById("divEducation").style.display="block";
	document.getElementById("degreeTypeEd").focus();
	
	
	
	setDefColortoErrorMsgToHoner();
	$('#errordivEducation').empty();
	
	return false;
}

function hideEducation()
{
	document.getElementById("frmEducation").reset();
	document.getElementById("licensureeducationId").value="";	
	document.getElementById("divEducation").style.display="none";
	window.location.hash = '#addeducationDSPQ';
	return false;
}


function saveOrUpdateEducation()
{

	var DegreeType = document.getElementById("degreeIdEd").value;
	var IHECode = document.getElementById("universityIdEd").value;
	var GraduationDate = document.getElementById("graduationDate").value;
	var Major = document.getElementById("fieldIdEd").value;
	var ssn = document.getElementById("ssn_pi").value;
	var licensureeducationId = document.getElementById("licensureeducationId").value;
    
	var cnt=0;
	var focs=0;	

	$('#errordivEducation').empty();
	setDefColortoErrorMsgToHoner();

	
	
	
	

	if(trim(DegreeType)=="")
	{
		$('#errordivEducation').append("&#149; Please enter Degree<br>");
		if(focs==0)
			$('#degreeType').focus();

		$('#degreeType').css("background-color",txtBgColor);
		cnt++;focs++;
	}

	if(trim(IHECode)=="")
	{
		$('#errordivEducation').append("&#149; Please enter School<br>");
		if(focs==0)
			$('#iHECode').focus();

		$('#iHECode').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	
	
	if(trim(GraduationDate)=="")
	{
		$('#errordivEducation').append("&#149; Please enter Graduation Date<br>");
		if(focs==0)
			$('#graduationDate').focus();

		$('#graduationDate').css("background-color",txtBgColor);
		cnt++;focs++;
	}

	if(trim(Major)=="")
	{
		$('#errordivEducation').append("&#149; Please enter Major<br>");
		if(focs==0)
			$('#major').focus();

		$('#major').css("background-color",txtBgColor);
		cnt++;focs++;
	}


	if(cnt!=0)		
	{
		$('#errordivEducation').show();
		return false;
	}
	else
	{
		//var education = {licensureeducationId:null, Major:null};

		//dwr.engine.beginBatch();	
		//dwr.util.getValues(education);
		
		PFLicenseEducation.saveEducation(licensureeducationId,ssn,DegreeType,IHECode,Major,GraduationDate,
				{ 
			async: false,
			errorHandler:handleError,
			callback: function(data)
			{

			hideEducation();
			getEducation();

			return false;
			}
				});
	//	dwr.engine.endBatch();
	}
	return true;

}



function showEditEducationLicense(id)
{
	PFLicenseEducation.showEditEducation(id,
			{ 
		async: false,
		errorHandler:handleError,
		callback: function(data)
		{		
		showEducationForm();
		document.getElementById("licensureeducationId").value=id;
		document.getElementById("degreeNameEd").value=data["degreeName"];
		document.getElementById("degreeIdEd").value=data["degreeId"];
		
		document.getElementById("fieldIdEd").value=data["fieldId"];
		document.getElementById("fieldNameEd").value=data["fieldName"];
		
		document.getElementById("universityNameEd").value=data["universityName"];
		document.getElementById("universityIdEd").value=data["universityId"];
		document.getElementById("graduationDate").value=data["GraduationDate"];
		//document.getElementById("major").value=data["Major"];
		

		}
			});

	return false;
}
function showHideOtherUniversity()
{
	var universityName = document.getElementById("universityName");
	if(resourceJSON.platformName=="NC" && trim(universityName.value)=="Other")
	{
		$("#otherUniversityName").show();
	}
	else
	{
		$("#otherUniversityName").hide();
	}	
}
///end sandeep
