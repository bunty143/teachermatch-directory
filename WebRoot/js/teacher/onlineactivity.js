var xmlHttp=null;
var deviceTypeAndroid=$.browser.device = (/Android|webOS|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent.toLowerCase()));
var deviceType=$.browser.device = (/iPhone|iPad|iPod/i.test(navigator.userAgent.toLowerCase()));
var page = 1;
var noOfRows = 10;
var sortOrderStr	=	"";
var sortOrderType	=	"";
var totalQuestionsList=0;
var timeCount=0;
var timeCountFlag=false;//add by Ram nath
function trim(s)
{
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}
function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{
		//alert("Oops: Your Session has expired!");  document.location = 'signin.do';
	}
	//else{alert("Server Error: "+exception.javaClassName);}
}

function createXMLHttpRequest()
{
	if (window.ActiveXObject)
		xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
	else if (window.XMLHttpRequest)
		xmlHttp = new XMLHttpRequest();
	else
		alert(resourceJSON.msgAjaxNotSupported);
}

function getOnlineActivityQuestions(flagforsubmit){	
	var teacherId=document.getElementById("teacherId").value;
	var jobCategoryId=document.getElementById("jobCategoryId").value;
	var districtId=document.getElementById("districtId").value;
	var questionSetId=document.getElementById("questionSetId").value;
	//add by Ram nath
	//flagforsubmit (1 for submit, 0 for auto submit and null is only load question)
	var previousStatus=null;
	try{
		previousStatus=document.getElementById("previousStatus").value;		
	}catch(e){}
	var bothpreviousStatusAndsubmitflag=previousStatus+"##~##"+flagforsubmit;
	//end by Ram nath
	OnlineActivityAjax.getOnlineActivityQuestions(teacherId,jobCategoryId,districtId,questionSetId,bothpreviousStatusAndsubmitflag,{  
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			var dataArray = data.split("@##@");
			totalQuestionsList = dataArray[1];
			var time = dataArray[2];
			if(time==0){
				document.getElementById("questionTimer").innerHTML="";
				if(totalQuestionsList!=0){
					document.getElementById("btnDiv").style.display="inline";
				}
			}else{
				document.getElementById("btnDiv").style.display="inline";
				document.getElementById("questionTimer").style.display="inline";
				timeCount=time;
				timeCountFlag=true;//add by Ram nath
				countDown();
				
			}
			$('#divOnlineActivityQuestionList').html(dataArray[0]);
		}});
}

function countDown(){  	
	 if (timeCount <=0){ 
		 if(timeCountFlag)//add by Ram nath
		 saveQuestion(2);
	 }else{  
		 timeCount--;  
	  document.getElementById("questionTimer").innerHTML = timeCount.toString().toHHMMSS();  
	  setTimeout("countDown()", 1000)  
	 }  
}  
function saveQuestion(flag){
	$('#errordivnlineActivityquestion').empty();
	var teacherId=document.getElementById("teacherId").value;
	var jobCategoryId=document.getElementById("jobCategoryId").value;
	var districtId=document.getElementById("districtId").value;
	var questionSetId=document.getElementById("questionSetId").value;
	var arr =[];
	var isRequiredCount=0;
	var ansRequiredCount=0;
	for(i=1;i<=totalQuestionsList;i++)
	{   
		var schoolMaster="";
		var isRequiredAns=0;
		var isRequired = dwr.util.getValue("QS"+i+"isRequired");
		if(isRequired==1){
			isRequiredCount++;
		}			
		var onlineActivityQuestions = {questionId:dwr.util.getValue("QS"+i+"questionId")};
		var questionTypeShortName = dwr.util.getValue("QS"+i+"questionTypeShortName");
		var questionTypeMaster = {questionTypeId:dwr.util.getValue("QS"+i+"questionTypeId")};			
		var qType = dwr.util.getValue("QS"+i+"questionTypeShortName");
		if(qType=='tf' || qType=='slsel' ||  qType=='slsel')
		{
			var optId="";
			var errorFlag=1;
			if($("input[name=QS"+i+"opt]:radio:checked").length > 0 )
			{
				 errorFlag=0;
				optId=$("input[name=QS"+i+"opt]:radio:checked").val();
			}else if(isRequired==1){
				$("#errordivnlineActivityquestion").html("&#149; "+resourceJSON.msgProvideResToAllQ+"<br>");
				dSPQuestionsErrorCount=1;
				errorFlag=1;
			}
			
			if(errorFlag==0){
				if(isRequired==1){
					isRequiredAns=1;
				}
				arr.push({ 
					"selectedOptions"  : optId,
					"question"  : dwr.util.getValue("QS"+i+"question"),
					"questionTypeMaster" : questionTypeMaster,
					"questionType" : questionTypeShortName,
					"questionOption" : dwr.util.getValue("qOptS"+optId),
					"onlineActivityQuestions" : onlineActivityQuestions
				});
			}

		}else if(qType=='ml' || qType=='sl')
		{
			var insertedText = dwr.util.getValue("QS"+i+"opt");
			if((insertedText!=null && insertedText!="") || isRequired==0)
			{
				if(isRequired==1){
					isRequiredAns=1;
				}
				arr.push({ 
					"insertedText"    : insertedText,
					"question"  : dwr.util.getValue("QS"+i+"question"),
					"questionTypeMaster" : questionTypeMaster,
					"questionType" : questionTypeShortName,
					"onlineActivityQuestions" : onlineActivityQuestions
				});
			}else
			{
				if(isRequired==1){
					$("#errordivnlineActivityquestion").html("&#149; "+resourceJSON.msgProvideResToAllQ+"<br>");
					dSPQuestionsErrorCount=1;
				}
			}
		}else if(qType=='et'){
			var optId="";
			var errorFlag=1;
			if($("input[name=QS"+i+"opt]:radio:checked").length > 0 )
			{
				errorFlag=0;
				optId=$("input[name=QS"+i+"opt]:radio:checked").val();
			}else
			{
				if(isRequired==1){
					errorFlag=1;
					$("#errordivnlineActivityquestion").html("&#149; "+resourceJSON.msgProvideResToAllQ+"<br>");
					dSPQuestionsErrorCount=1;
				}
			}
			
			var insertedText = dwr.util.getValue("QS"+i+"optet");
				
			if(errorFlag==0){
				if(isRequired==1){
					isRequiredAns=1;
				}
				arr.push({ 
					"selectedOptions"  : optId,
					"question"  : dwr.util.getValue("QS"+i+"question"),
					"insertedText"    : insertedText,
					"questionTypeMaster" : questionTypeMaster,
					"questionType" : questionTypeShortName,
					"questionOption" : dwr.util.getValue("qOptS"+optId),
					"onlineActivityQuestions" : onlineActivityQuestions
				});
			}
		}else if(qType=='mlsel'){
			try{
				 var multiSelectArray="";
				 var inputs = document.getElementsByName("multiSelect"+i); 
				 for (var j = 0; j < inputs.length; j++) {
				        if (inputs[j].type === 'checkbox') {
				        	if(inputs[j].checked){
				        		multiSelectArray+=	inputs[j].value+"|";
				            }
				        }
				} 
				if(multiSelectArray!=""  || isRequired==0){
					if(isRequired==1){
						isRequiredAns=1;
					} 
					arr.push({ 
						"selectedOptions"  : multiSelectArray,
						"question"  : dwr.util.getValue("QS"+i+"question"),
						"questionTypeMaster" : questionTypeMaster,
						"questionType" : questionTypeShortName,
						"onlineActivityQuestions" : onlineActivityQuestions
					});
				}else{
					if(isRequired==1){
						$("#errordivnlineActivityquestion").html("&#149; "+resourceJSON.msgProvideResToAllQ+"<br>");
						dSPQuestionsErrorCount=1;
					}
				}
			}catch(err){}
		}if(qType=='mloet'){
			try{
				 var multiSelectArray="";
				 var inputs = document.getElementsByName("multiSelect"+i); 
				 for (var j = 0; j < inputs.length; j++) {
				        if (inputs[j].type === 'checkbox') {
				        	if(inputs[j].checked){
				        		multiSelectArray+=	inputs[j].value+"|";
				            }
				        }
				} 
				var insertedText = dwr.util.getValue("QS"+i+"optmloet"); 
				if(multiSelectArray!="" || isRequired==0){
					if(isRequired==1){
						isRequiredAns=1;
					} 
					arr.push({ 
						"selectedOptions"  : multiSelectArray,
						"question"  : dwr.util.getValue("QS"+i+"question"),
						"insertedText"    : insertedText,
						"questionTypeMaster" : questionTypeMaster,
						"questionType" : questionTypeShortName,
						"onlineActivityQuestions" : onlineActivityQuestions
					});
				}else{
					if(isRequired==1){
						$("#errordivnlineActivityquestion").html("&#149; "+resourceJSON.msgProvideResToAllQ+"<br>");
						dSPQuestionsErrorCount=1;
					}
				}
			}catch(err){}
		}else if(qType=='rt'){
			var optId="";
			var score="";
			var rank="";
			var scoreRank=0;
			var opts = document.getElementsByName("optS");
			var scores = document.getElementsByName("scoreS");
			var ranks = document.getElementsByName("rankS");
			var o_ranks = document.getElementsByName("o_rankS");
			
			var tt=0;
			var uniqueflag=false;
			for(var i = 0; i < opts.length; i++) {
				optId += opts[i].value+"|";
				score += scores[i].value+"|";
				rank += ranks[i].value+"|";
				if(checkUniqueRankForPortfolio(ranks[i]))
				{
					uniqueflag=true;
					break;
				}

				if(ranks[i].value==o_ranks[i].value)
				{
					scoreRank+=parseInt(scores[i].value);
				}
				if(ranks[i].value=="")
				{
					tt++;
				}
			}
			if(uniqueflag)
				return;

			if(tt!=0)
				optId=""; 

			if(optId=="")
			{
				//totalSkippedQuestions++;
				//strike checking
			}
			var totalScore = scoreRank;
			if(isRequired==1){
				isRequiredAns=1;
			} 
			arr.push({ 
				"selectedOptions"  : optId,
				"question"  : dwr.util.getValue("QS"+i+"question"),
				"questionTypeMaster" : questionTypeMaster,
				"questionType" : questionTypeShortName,
				"questionOption" : dwr.util.getValue("qOptS"+optId),
				"onlineActivityQuestions" : onlineActivityQuestions,
				"optionScore"      : score,
				"totalScore"       : totalScore,
				"insertedRanks"    : rank
			});
		}
		
		if(isRequiredAns==1){
			ansRequiredCount++;
		}
	}

	var errorCount=true;
	if(isRequiredCount==ansRequiredCount){
		errorCount=false;
	}
	OnlineActivityAjax.setOnlineActivityQuestions(arr,teacherId,jobCategoryId,districtId,questionSetId,errorCount,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){
		if(data!=null)
		{
			arr =[];
			if(!errorCount || flag==2){ 
				timeCountFlag=false;//add by Ram nath
				getOnlineActivityQuestions(flag);
				$('#errordivnlineActivityquestion').empty();
				document.getElementById("btnDiv").style.display="none";
				document.getElementById("questionTimer").style.display="none";
			}
		}
	}
	});	
}
