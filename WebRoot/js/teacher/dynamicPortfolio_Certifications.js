var txtBgColor="#F5E7E1";
var dSPQuestionsErrorCount=0;
var a = 0;
function delRow(id)
{
	//alert(id)
	if(window.confirm(resourceJSON.msgsuredelete))
	{
		PFAcademics.deletePFAcademinGrid(id, function(data)
		{
			showGridCertifications();
			return false;
		});
	}
}

var countForRef=0;
function numericFilter(txb) {  
   txb.value = txb.value.replace(/[^\0-9]/ig, "");  
   if(!txb.value)
   {   
   if(countForRef==0)
   $('#errordivElectronicReferences').append("&#149; "+resourceJSON.errorMsgForCandidate+"<br>");
   
   countForRef++;
   }
}

function showGridCertifications()
{
	var districtIdForDSPQ="";	
	if ($('#districtIdForDSPQ').length > 0) {
		districtIdForDSPQ=document.getElementById("districtIdForDSPQ").value;
	}
	if(districtIdForDSPQ==7800038)
	PFCertifications.getPFCertificationsGridDspqNoble(dp_Certification_noOfRows,dp_Certification_page,dp_Certification_sortOrderStr,dp_Certification_sortOrderType,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
		$('#divDataGridCertifications').html(data);
		applyScrollOnTbl_CertificationsNoble();
	}});
	else
		PFCertifications.getPFCertificationsGrid(dp_Certification_noOfRows,dp_Certification_page,dp_Certification_sortOrderStr,dp_Certification_sortOrderType,{ 
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{
			$('#divDataGridCertifications').html(data);
			applyScrollOnTbl_Certifications();
		if(districtIdForDSPQ==4218990 && $('#isPrinciplePhiladelphia').val()=="1"){	
			$("#certSRCHeader").html("");
			$(".certSRCRec").html("");
		}
		}});
}
function getPraxis()
{
	var districtIdForDSPQ="";
	
	var jobcategoryName="";
	
	if ($('#districtIdForDSPQ').length > 0) {
		districtIdForDSPQ=document.getElementById("districtIdForDSPQ").value;
	}
	
	if ($('#jobCategoryName').length > 0) {
		jobcategoryName=document.getElementById("jobCategoryName").value;
	}
	
	
	if(districtIdForDSPQ!=4218990 && jobcategoryName!='Administrative Non-Instructional'){
		
	var statemaster = {stateId:dwr.util.getValue("stateMaster")};
	PFCertifications.getPraxis(statemaster,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			if(data!=null)
			{
				$("#praxisArea").show();
				$("#reading").html(data.praxisIReading);
				$("#writing").html(data.praxisIWriting);
				$("#maths").html(data.praxisIMathematics);
			}else
			{
				$("#praxisArea").hide();
				$("#reading").html("");
				$("#writing").html("");
				$("#maths").html("");
			}
	}});
	}
}

function saveSSNBySubmitButton()
{
	var ssn=document.getElementById("ssn_pi").value;
	var isAffilatedCheck="";
	var jobIdForEmp="";
			
	if($("#isAffilated").val()>0)
	{		
		isAffilatedCheck=document.getElementById("isAffilated").checked;
		jobIdForEmp=document.getElementById("jobId").value;
	}
	else if($("#isaffilatedstatushiddenId").val()>0)
	{		
		isAffilatedCheck = $("#isaffilatedstatushiddenId").val();
		jobIdForEmp=document.getElementById("jobId").value;
	}	
	if(ssn.length!= 9 && ssn.length>0 )
	{	$('#errPersonalInfoAndSSN').empty();
		$('#errPersonalInfoAndSSN').append("&#149; "+resourceJSON.msgPlzProvideSSN9digit+"</BR>");
		$('#errPersonalInfoAndSSN').show();
	}
	else if(isAffilatedCheck && ssn.length< 9 )
	{
		$('#errPersonalInfoAndSSN').empty();
		$('#errPersonalInfoAndSSN').append("&#149; "+resourceJSON.msgPlzProvideSSN9digit+"</BR>");
		$('#errPersonalInfoAndSSN').show();
	}
	else
	{
		$('#errPersonalInfoAndSSN').empty();
		HrmsLicenseAjax.saveSSNOnSubmitButton(ssn,isAffilatedCheck,jobIdForEmp,{
			async: true,
			callback: function(data)
			{			
			if(data=='X')
			{
				formerEmployeeForNc("We are unable to locate a current employee record using the SSN you have entered. If you have entered your SSN correctly, please contact your local HR office.");
			}
			else if(data=="NoStatus")
			{				
				formerEmployeeForNc("We are unable to locate a current employee record using the SSN you have entered. If you have entered your SSN correctly, please contact your local HR office.");	
			}
			else if(data=="NoRecordFound")
			{
				formerEmployeeForNc("We are unable to locate a current employee record using the SSN you have entered. If you have entered your SSN correctly, please contact your local HR office.");
			}
			
			if(ssn!=null && ssn!='')
			{				
				document.getElementById("ssn_pi").value=ssn;
				document.getElementById('BlankSSNvalue').value=ssn;
				//alert(document.getElementById("ssn_pi").value);
			}
			
			if(ssn=='')
			{
				var x = document.getElementById("ssn_pi");
		    	x.setAttribute("value", " ");
			document.getElementById("ssn_pi").value="";			
			document.getElementById('BlankSSNvalue').value="";
    	  	
			}
			
			},
		errorHandler:handleError
		});
	}
}

function showForm_LicenceFromSubmit()
{	
	saveSSNBySubmitButton();
			
	var ssn=document.getElementById("ssn_pi").value;	
	var address=document.getElementById("addressLine1").value;
	var phoneNo1=document.getElementById("phoneNumber1").value;
	var phoneNo2=document.getElementById("phoneNumber2").value;
	var phoneNo3=document.getElementById("phoneNumber3").value;
	var zipCode=document.getElementById("zipCode").value;
	var state=document.getElementById("stateIdForDSPQ").value;
	var city=document.getElementById("otherCity").value;
	
		
		if(ssn!='' && ssn.length==9)
		{
		$("#4ssnlast").hide();
		var x = document.getElementById("ssn_pi");
    	x.setAttribute("type", "hidden");						
      	var aftrsnn=ssn.slice(-4);   				     	
       	document.getElementById("ssn_pifornc").innerHTML=aftrsnn;				           	
       	$("#last4snnfornc").show();  
		}
		else
		{		
			$("#4ssnlast").show();
			var x = document.getElementById("ssn_pi");
	    	x.setAttribute("type", "text");
	    	  	$("#last4snnfornc").hide();  
	    }       			
		var isAffilatedCheck="";
		var jobIdForEmp="";
		if($("#isAffilated").val()>0)
		{			
			isAffilatedCheck=document.getElementById("isAffilated").checked;
			jobIdForEmp=document.getElementById("jobId").value;
		}
		else if($("#isaffilatedstatushiddenId").val()>0)
		{			
			isAffilatedCheck = $("#isaffilatedstatushiddenId").val();
			jobIdForEmp=document.getElementById("jobId").value;
		}	
		HrmsLicenseAjax.displayRecordsBySSN(ssn,isAffilatedCheck,jobIdForEmp,{ 
			async: true,
			callback: function(data)
			{	
		
			if(data==null||data=="")
			{				
			ssnNotFound("No License data found for your Social Security");
			return false;
			}
			else
			{									
					$('#licenseDiv').show();
					$('#divLicneseGridCertificationsGrid').html(data);				
					$('#loadingDiv').hide();
					$('#addLicenseCerti').hide();
					$('#licenseButton').show();
					applyScrollOnTblLicense();					
			}
			
			},
		errorHandler:handleError
		});
		
		HrmsLicenseAjax.displayRecordsBySSNForDate(ssn,{ 
			async: true,
			callback: function(data)
			{	
			//alert(data);
			if(data==null||data=="")
			{				
			ssnNotFound("No License data found for your Social Security");
			return false;
			}
			else
			{									
					$('#licenseDivDate').show();
					$('#divLicneseGridCertificationsGridDate').html(data);			
				    $('#loadingDiv').hide();				
				applyScrollOnTblLicenseDate();			
				
			}
			
			},
		errorHandler:handleError
		});
		
		HrmsLicenseAjax.displayRecordsBySSNForArea(ssn,{ 
			async: true,
			callback: function(data)
			{	
			//alert(data);
			if(data==null||data=="")
			{				
			ssnNotFound("No License data found for your Social Security");
			return false;
			}
			else
			{		
									
					$('#licenseDivArea').show();
					$('#ncCetrificationNC').show();
					$('#ncCetrification').hide();
					$('#divLicneseGridCertificationsGridArea').html(data);			
				    $('#loadingDiv').hide();				
				    applyScrollOnTblLicenseArea();			
				
			}
			
			},
		errorHandler:handleError
		});
		
				
		//  Anurag Kumar.......
		
		HrmsLicenseAjax.getNBCLicense(ssn,{ 
			async: true,
			callback: function(data)
			{			
			
			 if(data.indexOf("No record found") !=-1)
			    {
			    $('#nbptsRadio').show();;
			    $('#licenseDivNBPTS').hide();
			    }
			    else
			    {
			     $('#licenseDivNBPTS').show();
			        $('#nbcstatus').html(data);
			     $('#loadingDiv').hide(); 
			     $('#nbptsRadio').hide();;
			    }
			applyScrollOnTblNBPTSCertification();
			
			},
		errorHandler:handleError
		});
		getEducationFromSubmit();
		
		if((address==null || address=='')&&(phoneNo1==null || phoneNo1==''))
		{
           
			var DistrictId=document.getElementById("districtIdForDSPQ").value;
			HrmsLicenseAjax.displayTeacherInfoSSN(ssn,DistrictId,{ 
				async: true,
				callback: function(data)
				{	
					var pn1 = "";
					var pn2 = "";
					var pn3 = "";
					
					if(data!=null){
						if(data[0].address1!="" && data[0].address1!=null)
						{
						$('#addressLine1').val(data[0].address1);
						}
						
						if(data[0].phone!="" &&data[0].phone!=null)
						{
							pn1 = data[0].phone.substring(0,3);
							pn2 = data[0].phone.substring(3,6);
							pn3 = data[0].phone.substring(6,10);
							
						}
						$('#phoneNumber1').val(pn1);
						$('#phoneNumber2').val(pn2);
						$('#phoneNumber3').val(pn3);
					}
					
				},
			errorHandler:handleError
			});
		
		} 
		else if((address==null || address=='')&&(phoneNo1!=null || !phoneNo1==''))
		{	           
			var DistrictId=document.getElementById("districtIdForDSPQ").value;
			HrmsLicenseAjax.displayTeacherInfoSSN(ssn,DistrictId,{ 
				async: true,
				callback: function(data)
				{					
					if(data!=null){
						if(data[0].address1!="" && data[0].address1!=null)
						{
						$('#addressLine1').val(data[0].address1);
						}
					}
				},
			errorHandler:handleError
			});
		}
		
		if((address!=null || !address=='')&&(phoneNo1==null || phoneNo1==''))
		{
			var DistrictId=document.getElementById("districtIdForDSPQ").value;
			HrmsLicenseAjax.displayTeacherInfoSSN(ssn,DistrictId,{ 
				async: true,
				callback: function(data)
				{	
					var pn1 = "";
					var pn2 = "";
					var pn3 = "";
					
					if(data!=null){
												
						if(data[0].phone!="" &&data[0].phone!=null)
						{
							pn1 = data[0].phone.substring(0,3);
							pn2 = data[0].phone.substring(3,6);
							pn3 = data[0].phone.substring(6,10);
							
						}
						$('#phoneNumber1').val(pn1);
						$('#phoneNumber2').val(pn2);
						$('#phoneNumber3').val(pn3);
					}
					
				},
			errorHandler:handleError
			});
			
		}
		
		if(zipCode==null || zipCode=='')
		{	           
			var DistrictId=document.getElementById("districtIdForDSPQ").value;
			HrmsLicenseAjax.displayTeacherInfoSSN(ssn,DistrictId,{ 
				async: true,
				callback: function(data)
				{					
					if(data!=null){
						if(data[0].zipCode!="" && data[0].zipCode!=null)
						{
						$('#zipCode').val(data[0].zipCode);
						}
					}
				},
			errorHandler:handleError
			});
					
		}
		
		if(state==null || state=='')
		{
			
			var DistrictId=document.getElementById("districtIdForDSPQ").value;
			HrmsLicenseAjax.displayTeacherInfoSSN(ssn,DistrictId,{ 
				async: false,
				callback: function(data)
				{					
					if(data!=null){
						if(data[0].stateMaster!="" && data[0].stateMaster!=null)
						{ 
							setTimeout(function(){$("#stateIdForDSPQ option[value='"+data[0].stateMaster.stateId+"']").attr("selected","selected");}, 1000);
							document.getElementById("stateIdForDSPQ").value=data[0].stateMaster.stateId;
							getCityListByStateForDSPQ();
						}
					}
				},
			errorHandler:handleError
			});
			
						
				var DistrictId=document.getElementById("districtIdForDSPQ").value;
				HrmsLicenseAjax.displayTeacherInfoSSN(ssn,DistrictId,{ 
					async: true,
					callback: function(data)
					{					
						if(data!=null){
							if(data[0].cityName!="" && data[0].cityName!=null)
							{ 
																					
								setTimeout(function(){$("#cityIdForDSPQ option[id='"+'ct'+data[0].cityName+"']").attr("selected","selected");}, 1000);
								document.getElementById("cityIdForDSPQ").value=data[0].cityName;
							
							}
						}
					},
				errorHandler:handleError
				});
			
			
		}
		else
	{
		
				
	}	
}

function showForm_Licence()
{

	
	/*document.getElementById('submitSSN').value='ButtonClick';	
	$('#normalTeacherCerti').show();*/
	
	var ssn=document.getElementById("ssn_pi").value;	
	var address=document.getElementById("addressLine1").value;
	var phoneNo1=document.getElementById("phoneNumber1").value;
	var phoneNo2=document.getElementById("phoneNumber2").value;
	var phoneNo3=document.getElementById("phoneNumber3").value;
	var zipCode=document.getElementById("zipCode").value;
	var state=document.getElementById("stateIdForDSPQ").value;
	var city=document.getElementById("otherCity").value;
	
		
	if(ssn!=null || ssn=='')
	{	
		if(ssn!=null && ssn!='' && ssn.length==9)
		{
		$("#4ssnlast").hide();
		var x = document.getElementById("ssn_pi");
    	x.setAttribute("type", "hidden");						
      	var aftrsnn=ssn.slice(-4);   				     	
       	document.getElementById("ssn_pifornc").innerHTML=aftrsnn;				           	
       	$("#last4snnfornc").show();  
		}
		else
		{			
			$('#dynamicSaveDspq').click(function(){	
				
				var BlankSSN=document.getElementById('BlankSSNvalue').value;				
				document.getElementById("ssn_pi").value=document.getElementById('BlankSSNvalue').value;
				if(BlankSSN!=null && BlankSSN!='')
				{
				$("#4ssnlast").hide();
				var x = document.getElementById("ssn_pi");
		    	x.setAttribute("type", "hidden");						
		      	var aftrsnn=BlankSSN.slice(-4);   				     	
		       	document.getElementById("ssn_pifornc").innerHTML=aftrsnn;				           	
		       	$("#last4snnfornc").show();	
				}
				else
				{
					$("#4ssnlast").show();
					var x = document.getElementById("ssn_pi");
			    	x.setAttribute("type", "text");	     					           	
			       	$("#last4snnfornc").hide();  	
				}
					return true;
				});			
			
		}       			
		var isAffilatedCheck="";
		var jobIdForEmp="";
		if($("#isAffilated").val()>0)
		{			
			isAffilatedCheck=document.getElementById("isAffilated").checked;
			jobIdForEmp=document.getElementById("jobId").value;
		}
		else if($("#isaffilatedstatushiddenId").val()>0)
		{			
			isAffilatedCheck = $("#isaffilatedstatushiddenId").val();
			jobIdForEmp=document.getElementById("jobId").value;
		}
		if(ssn!='')
		HrmsLicenseAjax.displayRecordsBySSN(ssn,isAffilatedCheck,jobIdForEmp,{ 
			async: true,
			callback: function(data)
			{	
			//alert(data);
			if(data==null||data=="")
			{				
			ssnNotFound("No License data found for your Social Security");
			return false;
			}
			else
			{
									
					$('#licenseDiv').show();
					$('#divLicneseGridCertificationsGrid').html(data);
				//$("#divDataGridCertifications tr:last").after(data);
				$('#loadingDiv').hide();
				$('#addLicenseCerti').hide();
				$('#licenseButton').show();
				applyScrollOnTblLicense();			
				
			}
			
			},
		errorHandler:handleError
		});
		
		if(ssn!='')
		HrmsLicenseAjax.displayRecordsBySSNForDate(ssn,{ 
			async: true,
			callback: function(data)
			{	
			//alert(data);
			if(data==null||data=="")
			{				
			ssnNotFound("No License data found for your Social Security");
			return false;
			}
			else
			{									
					$('#licenseDivDate').show();
					$('#divLicneseGridCertificationsGridDate').html(data);			
				    $('#loadingDiv').hide();				
				applyScrollOnTblLicenseDate();			
				
			}
			
			},
		errorHandler:handleError
		});
		
		if(ssn!='')
		HrmsLicenseAjax.displayRecordsBySSNForArea(ssn,{ 
			async: true,
			callback: function(data)
			{	
			//alert(data);
			if(data==null||data=="")
			{				
			ssnNotFound("No License data found for your Social Security");
			return false;
			}
			else
			{		
									
					$('#licenseDivArea').show();
					$('#ncCetrificationNC').show();
					$('#ncCetrification').hide();
					$('#divLicneseGridCertificationsGridArea').html(data);			
				    $('#loadingDiv').hide();				
				    applyScrollOnTblLicenseArea();			
				
			}
			
			},
		errorHandler:handleError
		});
		
				
		//  Anurag Kumar.......
		
		if(ssn!='')
		HrmsLicenseAjax.getNBCLicense(ssn,{ 
			async: true,
			callback: function(data)
			{	
			
			 if(data.indexOf("No record found") !=-1)
			    {
			    $('#nbptsRadio').show();;
			    $('#licenseDivNBPTS').hide();
			    }
			    else
			    {
			     $('#licenseDivNBPTS').show();
			        $('#nbcstatus').html(data);
			     $('#loadingDiv').hide(); 
			     $('#nbptsRadio').hide();;
			    }
			applyScrollOnTblNBPTSCertification();
			
			},
		errorHandler:handleError
		});
		getEducation();	
		
		if((address==null || address=='')&&(phoneNo1==null || phoneNo1==''))
		{
           
			var DistrictId=document.getElementById("districtIdForDSPQ").value;
			HrmsLicenseAjax.displayTeacherInfoSSN(ssn,DistrictId,{ 
				async: true,
				callback: function(data)
				{	
					var pn1 = "";
					var pn2 = "";
					var pn3 = "";
					
					if(data!=null){
						if(data[0].address1!="" && data[0].address1!=null)
						{
						$('#addressLine1').val(data[0].address1);
						}
						
						if(data[0].phone!="" &&data[0].phone!=null)
						{
							pn1 = data[0].phone.substring(0,3);
							pn2 = data[0].phone.substring(3,6);
							pn3 = data[0].phone.substring(6,10);
							
						}
						$('#phoneNumber1').val(pn1);
						$('#phoneNumber2').val(pn2);
						$('#phoneNumber3').val(pn3);
					}
					
				},
			errorHandler:handleError
			});
		
		} 
		else if((address==null || address=='')&&(phoneNo1!=null || !phoneNo1==''))
		{	           
			var DistrictId=document.getElementById("districtIdForDSPQ").value;
			HrmsLicenseAjax.displayTeacherInfoSSN(ssn,DistrictId,{ 
				async: true,
				callback: function(data)
				{					
					if(data!=null){
						if(data[0].address1!="" && data[0].address1!=null)
						{
						$('#addressLine1').val(data[0].address1);
						}
					}
				},
			errorHandler:handleError
			});
		}
		
		if((address!=null || !address=='')&&(phoneNo1==null || phoneNo1==''))
		{
			var DistrictId=document.getElementById("districtIdForDSPQ").value;
			HrmsLicenseAjax.displayTeacherInfoSSN(ssn,DistrictId,{ 
				async: true,
				callback: function(data)
				{	
					var pn1 = "";
					var pn2 = "";
					var pn3 = "";
					
					if(data!=null){
												
						if(data[0].phone!="" &&data[0].phone!=null)
						{
							pn1 = data[0].phone.substring(0,3);
							pn2 = data[0].phone.substring(3,6);
							pn3 = data[0].phone.substring(6,10);
							
						}
						$('#phoneNumber1').val(pn1);
						$('#phoneNumber2').val(pn2);
						$('#phoneNumber3').val(pn3);
					}
					
				},
			errorHandler:handleError
			});
			
		}
		
		if(zipCode==null || zipCode=='')
		{	           
			var DistrictId=document.getElementById("districtIdForDSPQ").value;
			HrmsLicenseAjax.displayTeacherInfoSSN(ssn,DistrictId,{ 
				async: true,
				callback: function(data)
				{					
					if(data!=null){
						if(data[0].zipCode!="" && data[0].zipCode!=null)
						{
						$('#zipCode').val(data[0].zipCode);
						}
					}
				},
			errorHandler:handleError
			});
					
		}
		
		if(state==null || state=='')
		{
			
			var DistrictId=document.getElementById("districtIdForDSPQ").value;
			HrmsLicenseAjax.displayTeacherInfoSSN(ssn,DistrictId,{ 
				async: false,
				callback: function(data)
				{					
					if(data!=null){
						if(data[0].stateMaster!="" && data[0].stateMaster!=null)
						{ 
							setTimeout(function(){$("#stateIdForDSPQ option[value='"+data[0].stateMaster.stateId+"']").attr("selected","selected");}, 1000);
							document.getElementById("stateIdForDSPQ").value=data[0].stateMaster.stateId;
							getCityListByStateForDSPQ();
						}
					}
				},
			errorHandler:handleError
			});
			
						
				var DistrictId=document.getElementById("districtIdForDSPQ").value;
				HrmsLicenseAjax.displayTeacherInfoSSN(ssn,DistrictId,{ 
					async: true,
					callback: function(data)
					{					
						if(data!=null){
							if(data[0].cityName!="" && data[0].cityName!=null)
							{ 
																					
								setTimeout(function(){$("#cityIdForDSPQ option[id='"+'ct'+data[0].cityName+"']").attr("selected","selected");}, 1000);
								document.getElementById("cityIdForDSPQ").value=data[0].cityName;
							
							}
						}
					},
				errorHandler:handleError
				});
			
			
		}
		
		
	}
	else
	{
		
				
	}
	}
function getEducationFromSubmit()
{



	
	var ssn = document.getElementById("ssn_pi").value;
	//document.getElementById("divLblEducation").style.display="block";
	//element = document.getElementById('loadEducation');
	//element.style.display = "none";
	if(ssn!=null || ssn=='')
	{
	
	PFLicenseEducation.getLicenseEducation(ssn,
			{ 
				async: false,
				errorHandler:handleError,
				callback: function(data)
				{	
			
					document.getElementById("divDataEducation").innerHTML=data;
					if(data.indexOf("No record found")==-1)
					{
						document.getElementById("divDataEducationVal").value=1;
					}
					else
					{
						document.getElementById("divDataEducationVal").value=0;
					}	
					document.getElementById("divLblEducation").style.display="block";
					applyScrollOnEducation();
					//document.getElementById("divAddEducation").style.display="block";
					
				}
			});
	}
	

}
function getEducation(){
	var ssn = document.getElementById("ssn_pi").value;
//document.getElementById("divLblEducation").style.display="block";
//element = document.getElementById('loadEducation');
//element.style.display = "none";
if(ssn!=null || ssn=='')
{
	if(ssn!='')
PFLicenseEducation.getLicenseEducation(ssn,
		{ 
			async: false,
			errorHandler:handleError,
			callback: function(data)
			{	
		
				document.getElementById("divDataEducation").innerHTML=data;
				if(data.indexOf("No record found")==-1)
				{
					document.getElementById("divDataEducationVal").value=1;
				}
				else
				{
					document.getElementById("divDataEducationVal").value=0;
				}	
				document.getElementById("divLblEducation").style.display="block";
				applyScrollOnEducation();
				//document.getElementById("divAddEducation").style.display="block";
				
			}
		});
}	
}



function showForm_Certification()
{	
	document.getElementById("frmCertificate").reset();
	document.getElementById("certId").value="";	
	document.getElementById("divMainForm").style.display="block";
	document.getElementById("removeCert").style.display="none";
	document.getElementById("certificationStatusMaster").focus();
	
	for (var i = 4; i <= 11; i++) { 
		$("#certificationtypeMaster option[value='"+i+"']").hide();		
	}
	$(".certText").hide();
	var districtIdForDSPQ="";	
	if ($('#districtIdForDSPQ').length > 0) {
		districtIdForDSPQ=document.getElementById("districtIdForDSPQ").value;
	}
	$(".certText").find(".jqte_editor").html("");
	document.getElementById("pathOfCertification").value="";
	if(districtIdForDSPQ==7800038){
		$(".certClass").hide();
		$(".certText").show();
	}else if(districtIdForDSPQ==3904380 || districtIdForDSPQ==7800040 || districtIdForDSPQ==3702040){
		$(".dOENumberDiv").hide();
		$(".cluDiv").hide();
		if(document.getElementById("districtIdForDSPQ").value==3702040 && $("#jobcategoryDsp").val()=="Licensed"){
			$(".gradeLvl").show();
		}else{
			$(".gradeLvl").hide();
		}
	}else if($("#headQuaterIdForDspq").val()==2){
		//$(".dOENumberDiv").hide();
		//$(".cluDiv").hide();
	}
	else{
		$(".certText").hide();
		$(".certClass").show();
		
	}	
	if($("#headQuaterIdForDspq").val()==2 && $("#dspqName").val()=="Option A")
	{
		$("#cert_LicName").html("Certification/Licensure Name");
	}
	else
	{
		$("#cert_LicName").html("Certification/Licensure Name<span class='required'>*</span>");
	}	
	$("#CertTypeLabel").html("Certification Type<span class='required'>*</span>");
	if(districtIdForDSPQ==804800){
		$(".cluDiv").hide();
		$(".doeReq").show();		
		$("#cert_LicName").html("Endorsement Type<span class='required'>*</span>");
		$("#CertTypeLabel").html("License Type<span class='required'>*</span>");
	}
	
	if(districtIdForDSPQ==806810  && ($("#jobcategoryDsp").val()=="Administrator" || $("#jobcategoryDsp").val()=="Teacher")){
		$("#cert_LicName").html("Endorsement Area<span class='required'>*</span>");
		$("#CertTypeLabel").html("License Type<span class='required'>*</span>");
	}
	if(districtIdForDSPQ==4218990){
		$(".doeShow").hide();
		$(".ppdiShow").show();
		$(".gradeLvl").hide();
		$('#clsToolTip').show();
		if($('#isPrinciplePhiladelphia').val()=="1"){
			$(".certSrc").hide();
		}
		for (var i = 4; i <= 11; i++) { 
			$("#certificationtypeMaster option[value='"+i+"']").show();
		}
	}else{
		$(".ppdiShow").hide();
		$(".doeShow").show();
		$(".certSrc").show();
	}
	
	$("#certiExpiration").val("");
	if(districtIdForDSPQ==1200390)
		$('#certiExpirationDiv').show();
	else
		$('#certiExpirationDiv').hide();
	
	if(districtIdForDSPQ==7800040)
		$('#ieinNumberDiv').show();
	else
		$('#ieinNumberDiv').hide();
	
	$("#certificationtypeMasterTool").hide();
	if(districtIdForDSPQ==5304860  && ($("#jobcategoryDsp").val()=="Administrator" || $("#jobcategoryDsp").val()=="Principal / Asst Principal")){
		$("#certificationtypeMasterTool").show();
	}else if(districtIdForDSPQ==1302010 && ($("#jobcategoryDsp").val()=="Classified" || $("#jobcategoryDsp").val()=="Substitutes")){
		$('#certificationtypeMasterTool').attr('data-original-title', resourceJSON.msgallcertificationsheld);
		$("#certificationtypeMasterTool").show();
	}
	
	//certiDatesReq
	$(".certiDatesReq").show();
	if($("#certiDatesOptional").val()=="false"){
		$(".certiDatesReq").hide();
	}
	$(".certDatesFlReq").show();
	if(document.getElementById("districtIdForDSPQ").value==614730){
		$(".certDatesFlReq").hide();
	}
	if(document.getElementById("districtIdForDSPQ").value==3703120 && $('#jobcategoryDsp').val().trim().indexOf("Substitute") !=-1){
		//$(".gradeLvl").hide();
		$(".cluDiv").hide();
		$(".dOENumberDiv").hide();
	}
	if(document.getElementById("districtIdForDSPQ").value==3703120 && $('#jobcategoryDsp').val().trim().indexOf("Bus Driver") !=-1){
		$(".gradeLvl").hide();
		$(".cluDiv").hide();
		$(".dOENumberDiv").hide();
	}
	if($("#certificationUrl").val()=="false"){
		$(".cluDiv").hide();
	}
	if($("#certificationDoeNumber").val()=="false"){
		$(".dOENumberDiv").hide();
	}
	if($("#certiGrades").val()!="0"){
		$(".gradeLvl").hide();
	}
	if($('#headQuaterIdForDspq').val()=="2" && ($('#dspqName').val()=="Option C"))
	{		
		$(".gradeLvl").show();
		$('#gradelevelreq').show();		
	}
	if($('#headQuaterIdForDspq').val()=="2" && ($('#dspqName').val()=="Option A" || $('#dspqName').val()=="Option C"))
	{
		$('#yearexp').show();
	}
	if(document.getElementById("districtIdForDSPQ").value==3702340)
	{
		$('#gradelevelreq').hide();		
	}
	if($("#licenseLetterOptional").val()=="true")
	{
		$('#licenseceasterik').show();		
	}
	else
	{
		$('#licenseceasterik').hide();	
	}
	$('#errordiv_Certification').empty();
	setDefColortoErrorMsg_Certification();
	fieldsDisable(0);
	return false;
}

function clearForm_Certification()
{
	$("#praxisArea").hide();
	$("#reading").html("");
	$("#writing").html("");
	$("#maths").html("");
}

function clearCertType()
{
	$("#certificateTypeMaster").val("");
	$("#certType").val("");
}
function insertOrUpdate_Certification(sbtsource)
{
	
	
	if(sbtsource=="0")
	{
		resetSBTNSource();
	}
	
	var threadCount_cert=document.getElementById("threadCount_cert").value;
	if(threadCount_cert=="0")
	{
		document.getElementById("threadCount_cert").value=1;
		
		updateThreadCount("cert");
		
		//$('#testcall').append("&#149; insertOrUpdate_Certification<br>");
		
		var certId = document.getElementById("certId");		
		var certText = $(".certText").find(".jqte_editor").html();	
		var stateMaster = document.getElementById("stateMaster");
		var yearReceived = document.getElementById("yearReceived"); 
		var certType = document.getElementById("certType");
		var certUrl = trim(document.getElementById("certUrl").value);
		var certiExpiration =$("#certiExpiration").val();
		var certificationtypeMasterObj = {certificationTypeMasterId:dwr.util.getValue("certificationtypeMaster")};
		var certificationTypeMaster = document.getElementById("certificationtypeMaster").value;
		var yearExpires = document.getElementById("yearexpires").value;
		var doenumber = trim(document.getElementById("doenumber").value);
		
		var IEINNumber = "";
		
		var proofCertReq=$("#proofOfCertificationReq").val();
		var pathOfCertificationFile = document.getElementById("pathOfCertificationFile");
		var cnt=0;
		var focs=0;	
		
		var fileName = pathOfCertificationFile.value;
		//alert("fileName "+fileName);
		var ext = fileName.substr(fileName.lastIndexOf('.') + 1).toLowerCase();
		var fileSize=0;		
		if ($.browser.msie==true){	
		    fileSize = 0;	   
		}else{		
			if(pathOfCertificationFile.files[0]!=undefined)
			fileSize = pathOfCertificationFile.files[0].size;
		}
		
		if(doenumber=="")
		{
			doenumber=0;
		}
		/*if(yearExpires=="")
		{
			yearExpires=0;
		}*/
		var districtIdForDSPQ="";	
		if ($('#districtIdForDSPQ').length > 0) {
			districtIdForDSPQ=document.getElementById("districtIdForDSPQ").value;
		}	
		
		$('#errordiv_Certification').empty();
		setDefColortoErrorMsg_Certification();
		var certificationStatusMaster = document.getElementById("certificationStatusMaster").value;
		if(certificationStatusMaster=="")
		{
			$('#errordiv_Certification').append("&#149; "+resourceJSON.msgcertificate+"<br>");
				if(focs==0)
					$('#certificationStatusMaster').focus();
				
			$('#certificationStatusMaster').css("background-color",txtBgColor);
				cnt++;focs++;	
				//return false;
		}
		//districtIdForDSPQ noble
		if(districtIdForDSPQ==7800040)
		{
			IEINNumber = trim(document.getElementById("IEINNumber").value);			
			if(IEINNumber=="" && certificationStatusMaster!="5")
			{
				$('#errordiv_Certification').append("&#149; "+resourceJSON.msgPleaseIEINNumber+"<br>");
					if(focs==0)
						$('#IEINNumber').focus();
					
				$('#IEINNumber').css("background-color",txtBgColor);
					cnt++;focs++;	
					//return false;
			}
		}
		
		if((document.getElementById("districtIdForDSPQ").value==2633090) && ($('#jobcategoryDsp').val()=="Professional Certified") ||document.getElementById("districtIdForDSPQ").value==804800 || (window.location.hostname=="nccloud.teachermatch.org" && document.getElementById("districtIdForDSPQ").value==7800143) || 
				(window.location.hostname=="nc.teachermatch.org" && document.getElementById("districtIdForDSPQ").value==7800161))
		{
			
			if(doenumber=="" && certificationStatusMaster!=5)
			{

				$('#errordiv_Certification').append("&#149; "+resourceJSON.msgDOENumber+"<br>");
					if(focs==0)
						$('#doenumber').focus();
					
				$('#doenumber').css("background-color",txtBgColor);
					cnt++;focs++;	
					//return false;
			
			}
			var selectedGrades = $( ".gradeLvl input:checked" ).length;			
			if(selectedGrades==0 && document.getElementById("districtIdForDSPQ").value!=804800){
				$('#errordiv_Certification').append("&#149; "+resourceJSON.msgGradeLevel+"<br>");
				if(focs==0)
					$('#pkOffered').focus();
				
				$('#pkOffered').css("background-color",txtBgColor);
				cnt++;focs++;	
			}
		}		
		if($('#headQuaterIdForDspq').val()=="2" && $('#certiGrades').val()=="2" && $('#dspqName').val()=="Option C")
		{
			
			var selectedGrades = $( ".gradeLvl input:checked" ).length;			
			if(selectedGrades==0 && $("#certificationStatusMaster").val()!="5"){				
				$('#errordiv_Certification').append("&#149; Please select Grade Level(s)<br>");
				if(focs==0)
					$('#pkOffered').focus();
				$('#pkOffered').css("background-color",txtBgColor);
				cnt++;focs++;
				
			}
		}
		else if($('#certiGrades').val()=="2"){
			
			var selectedGrades = $( ".gradeLvl input:checked" ).length;			
			if(selectedGrades==0){
				$('#errordiv_Certification').append("&#149; Please select Grade Level(s)<br>");
				if(focs==0)
					$('#pkOffered').focus();
				$('#pkOffered').css("background-color",txtBgColor);
				cnt++;focs++;
			}
		}
		if(districtIdForDSPQ!=7800038){
		if(certificationStatusMaster!=5){
		var certificationtypeMasterObj = {certificationTypeMasterId:dwr.util.getValue("certificationtypeMaster")};
		if(trim(certificationtypeMasterObj.certificationTypeMasterId)==0)
		{
			$('#errordiv_Certification').append("&#149; "+resourceJSON.msgceritificationtype+"<br>");
			if(focs==0)
				$('#certificationtypeMaster').focus();
			
			$('#certificationtypeMaster').css("background-color",txtBgColor);
				cnt++;focs++;	
		}
		
		if(trim(stateMaster.value)=="")
		{
			$('#errordiv_Certification').append("&#149; "+resourceJSON.msgState+"<br>");
			if(focs==0)
				$('#stateMaster').focus();
			
			$('#stateMaster').css("background-color",txtBgColor);
			cnt++;focs++;
		}
		
		if(certificationStatusMaster!=3)
		{
			if($("#certiDatesOptional").val()=="true")
			if(trim(yearReceived.value)=="")
			{
				$('#errordiv_Certification').append("&#149; "+resourceJSON.msgyearreciverd+"<br>");
				if(focs==0)
					$('#yearReceived').focus();
				
				$('#yearReceived').css("background-color",txtBgColor);
				cnt++;focs++;
			}
			
			
			if($('#jobcategoryDsp').val().trim()=="Professional Certified"){
				if(trim(yearExpires)=="")
				{
					$('#errordiv_Certification').append("&#149; Please select Year Expires<br>");
					if(focs==0)
						$('#yearexpires').focus();
					
					$('#yearexpires').css("background-color",txtBgColor);
					cnt++;focs++;
				}
			}					
				if($("#headQuaterIdForDspq").val()==2 && ($('#dspqName').val()=="Option A" || $('#dspqName').val()=="Option C"))
				{					
					if(trim(yearExpires)=="")
					{
						$('#errordiv_Certification').append("&#149; Please select Year Expires<br>");
						if(focs==0)
							$('#yearexpires').focus();
						
						$('#yearexpires').css("background-color",txtBgColor);
						cnt++;focs++;
					}
				}
				if(trim(yearExpires)=="0")
					yearExpires="";				
			}
	
		if($("#headQuaterIdForDspq").val()==2 && $("#dspqName").val()=="Option A")
		{
		}
		else if(trim(certType.value)=="")
		{
			$('#errordiv_Certification').append("&#149; "+resourceJSON.msgentercertificatename+"<br>");
			if(focs==0)
				$('#certType').focus();
			
			$('#certType').css("background-color",txtBgColor);
			cnt++;focs++;
		}
		var certTypeObj = {certTypeId:dwr.util.getValue("certificateTypeMaster")};
		//alert((trim(certTypeObj.certTypeId)=="" && trim(certType.value)!=""));
		if(trim(certTypeObj.certTypeId)=="" && trim(certType.value)!="")
		{
			$('#errordiv_Certification').append("&#149; "+resourceJSON.msgnotexistselecstate+"<br>");
			if(focs==0)
				$('#certType').focus();
			$('#certType').css("background-color",txtBgColor);
			cnt++;focs++;
		}
		
		if(districtIdForDSPQ==4218990 && (certificationStatusMaster==1 || certificationStatusMaster==2)){}
		else{
		if(proofCertReq==1 && (certificationStatusMaster==1 || certificationStatusMaster==2))
		{
			var pathOfCertification = document.getElementById("pathOfCertification").value;
			if(certUrl=="" && pathOfCertification=="" && pathOfCertificationFile.value=="")
			{
				$('#errordiv_Certification').append("&#149; "+resourceJSON.msgentervalidlicence+"<br>");
					if(focs==0)
						$('#certUrl').focus();
					$('#certUrl').css("background-color",txtBgColor);
					cnt++;focs++;	
					return false;
			}
		}
		}
		
		if(ext!=""){
			if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt'))
			{
				$('#errordiv').append("&#149; "+resourceJSON.AcceptFileFormat+"<br>");
					if(focs==0)
						$('#pathOfCertification').focus();
					
					$('#pathOfCertification').css("background-color",txtBgColor);
					cnt++;focs++;	
					return false;
			}	
			else if(fileSize>=10485760)
			{
				$('#errordiv').append("&#149; "+resourceJSON.FileSizeLessthan10Mb+"<br>");
					if(focs==0)
						$('#pathOfCertification').focus();
					
					$('#pathOfCertification').css("background-color",txtBgColor);
					cnt++;focs++;	
					return false;
			}
		}
	}
	
 }	
		if(fileName=="" && $("#licenseLetterOptional").val()=="true" && $("#certificationStatusMaster").val()!="5")
		{
			$('#errordiv_Certification').append("&#149; "+resourceJSON.licenseLetterOptional+"<br>");
			cnt++;focs++;	
		}
		
		//		val end
		if(cnt!=0)		
		{
			updateReturnThreadCount("cert");
			$('#errordiv_Certification').show();
			return false;
		}else if(districtIdForDSPQ==7800038){
		
			/*var certificationStatusMasterObj= {certificationStatusId:dwr.util.getValue("certificationStatusMaster")};
			certificate = {certId:certId.value, stateMaster:null,yearReceived:null,certType:null,certUrl:null,
					certificationStatusMaster:certificationStatusMasterObj,pkOffered:null,kgOffered:null,g01Offered:null,g02Offered:null,
					g03Offered:null,g04Offered:null,g05Offered:null,g06Offered:null,g07Offered:null,g08Offered:null,
					g09Offered:null,g10Offered:null,g11Offered:null,g12Offered:null,readingQualifyingScore:null,
					writingQualifyingScore:null,mathematicsQualifyingScore:null,certificationTypeMaster:null,
					yearExpired:null,doeNumber:null,certText:certText};*/

			PFCertifications.saveOrUpdateCertificationsDSPQNoble(certId.value,certificationStatusMaster,certText,{
				async: true,
				errorHandler:handleError,
				callback:function(data)
					{
				
				if(data==0){
					errorHandler:handleError
					   }

					hideForm_Certification();
					showGridCertifications();

					if(sbtsource==1)
									{
										/*if(certificationTypeMaster==2 || certificationTypeMaster==3)
										{
											try{ document.getElementById("displayGKAndSubject").value=true; } catch(e){}
											showHideGK_SubjectArea();
											saveAllDSPQ();
											saveAndContinueBottomPart();
											updateReturnThreadCount("cert");
											validatePortfolioErrorMessageAndGridData('level2');
										}
										else
										{
											try{ document.getElementById("displayGKAndSubject").value=false; } catch(e){}
											showHideGK_SubjectArea();
											saveAllDSPQ();
											saveAndContinueBottomPart();
											updateReturnThreadCount("cert");
											validatePortfolioErrorMessageAndGridData('level2');
										}*/
										
										displayGKAndSubject();
										saveAllDSPQ();
										saveAndContinueBottomPart();
										updateReturnThreadCount("cert");
										validatePortfolioErrorMessageAndGridData('level2');
										
									}
									else
									{
										/*if(certificationTypeMaster==2 || certificationTypeMaster==3)
										{
											try{ document.getElementById("displayGKAndSubject").value=true; } catch(e){}
											showHideGK_SubjectArea();
										}
										else
										{
											try{ document.getElementById("displayGKAndSubject").value=false; } catch(e){}
											showHideGK_SubjectArea();
										}*/
										displayGKAndSubject();
									}
					}
			});
		}
		else
		{
			var stateObj = {stateId:dwr.util.getValue("stateMaster")};
			var certificationStatusMasterObj= {certificationStatusId:dwr.util.getValue("certificationStatusMaster")};
			//var certTypeObj = {certTypeId:dwr.util.getValue("certificateTypeMaster")};
			
			var certificate =null;
			
			if(dwr.util.getValue("certificationStatusMaster")=="")
			{
				certificate = {certId:null, stateMaster:stateObj,yearReceived:trim(yearReceived.value),certType:null,certUrl:null,
						certificateTypeMaster:certTypeObj,pkOffered:null,kgOffered:null,g01Offered:null,g02Offered:null,
						g03Offered:null,g04Offered:null,g05Offered:null,g06Offered:null,g07Offered:null,g08Offered:null,
						g09Offered:null,g10Offered:null,g11Offered:null,g12Offered:null,readingQualifyingScore:null,
						writingQualifyingScore:null,mathematicsQualifyingScore:null,certText:null,ieinNumber:IEINNumber,certiExpiration:certiExpiration};
			}
			else
			{
				certificate = {certId:null,certificationStatusMaster:certificationStatusMasterObj,certUrl:null,
						stateMaster:stateObj,yearReceived:trim(yearReceived.value),certType:trim(certType.value),certificateTypeMaster:certTypeObj,
						pkOffered:null,kgOffered:null,g01Offered:null,g02Offered:null,g03Offered:null,
						g04Offered:null,g05Offered:null,g06Offered:null,g07Offered:null,g08Offered:null,
						g09Offered:null,g10Offered:null,g11Offered:null,g12Offered:null,readingQualifyingScore:null,
						writingQualifyingScore:null,mathematicsQualifyingScore:null,certificationTypeMaster:certificationtypeMasterObj,
						yearExpired:yearExpires,doeNumber:doenumber,certText:certText,ieinNumber:IEINNumber,certiExpiration:certiExpiration};
			}
		
			dwr.util.getValues(certificate);
			dwr.engine.beginBatch();
			
			var certiFile = document.getElementById("pathOfCertification").value;
			//alert("certiFile "+certiFile);
			if(certiFile!=""){
				certificate.pathOfCertification=certiFile;
			}
			if(fileName!="")
			{	
				//$('#loadingDiv').show();	
				//document.getElementById("frmCertificate").submit();
			
				PFCertifications.findDuplicateCertificate(certificate,{
					async: true,
					errorHandler:handleError,
					callback:function(data)
						{
							if(data)
							{
								$('#errordiv').append("&#149; "+resourceJSON.msgCertificationLicensure+"<br>");
								$('#certType').focus();
								$('#certType').css("background-color",txtBgColor);
								updateReturnThreadCount("cert");
								$('#errordiv').show();
								return false;
							}else{
								//$('#loadingDiv').show();
								document.getElementById("frmCertificate").submit();
							}
						}
					});
			
			}
			else
			{
				//alert(" IEINNumber "+IEINNumber+" certificationStatusMaster "+certificationStatusMaster);
				if(certificationStatusMaster==5){
					certificate = {certId:certId.value, stateMaster:null,yearReceived:null,certType:null,certUrl:null,
							certificationStatusMaster:certificationStatusMasterObj,pkOffered:null,kgOffered:null,g01Offered:null,g02Offered:null,
							g03Offered:null,g04Offered:null,g05Offered:null,g06Offered:null,g07Offered:null,g08Offered:null,
							g09Offered:null,g10Offered:null,g11Offered:null,g12Offered:null,readingQualifyingScore:null,
							writingQualifyingScore:null,mathematicsQualifyingScore:null,certificationTypeMaster:null,
							yearExpired:null,doeNumber:null,certText:certText,ieinNumber:IEINNumber,certiExpiration:certiExpiration};
				
				
				}

				PFCertifications.saveOrUpdate(certificate,{
					async: true,
					errorHandler:handleError,
					callback:function(data)
						{
							if(data==1)
							{
								updateReturnThreadCount("cert");
								$('#errordiv_Certification').append("&#149; "+resourceJSON.msgCertificationLicensure+"<br>");
								$('#certType').focus();
								$('#certType').css("background-color",txtBgColor);
								$('#errordiv_Certification').show();
								return false;
							}else if(data==0){
								errorHandler:handleError
							}
							//$('#loadingDiv').hide();
							hideForm_Certification();
							showGridCertifications();
							if(sbtsource==1)
							{
								/*if(certificationTypeMaster==2 || certificationTypeMaster==3)
								{
									try{ document.getElementById("displayGKAndSubject").value=true; } catch(e){}
									showHideGK_SubjectArea();
									saveAllDSPQ();
									saveAndContinueBottomPart();
									updateReturnThreadCount("cert");
									validatePortfolioErrorMessageAndGridData('level2');
								}
								else
								{
									try{ document.getElementById("displayGKAndSubject").value=false; } catch(e){}
									showHideGK_SubjectArea();
									saveAllDSPQ();
									saveAndContinueBottomPart();
									updateReturnThreadCount("cert");
									validatePortfolioErrorMessageAndGridData('level2');
								}*/
								
								displayGKAndSubject();
								saveAllDSPQ();
								saveAndContinueBottomPart();
								updateReturnThreadCount("cert");
								validatePortfolioErrorMessageAndGridData('level2');
								
							}
							else
							{
								/*if(certificationTypeMaster==2 || certificationTypeMaster==3)
								{
									try{ document.getElementById("displayGKAndSubject").value=true; } catch(e){}
									showHideGK_SubjectArea();
								}
								else
								{
									try{ document.getElementById("displayGKAndSubject").value=false; } catch(e){}
									showHideGK_SubjectArea();
								}*/
								displayGKAndSubject();
							}
							
						}
					});
			}
			/*PFCertifications.saveOrUpdate(certificate,certUrl,
			{ 
				async: true,
				errorHandler:handleError,
				callback:function(data)
				{
					if(data==0)
					{
						
						$('#errordiv_Certification').append("&#149; This Certification Name is already added, Please select another Certification Name<br>");
						$('#certType').focus();
						$('#certType').css("background-color",txtBgColor);
						$('#errordiv_Certification').show();
						return false;
					}
					showGridCertifications();
					//$("#hrefCertifications").addClass("stepactive");
					hideForm_Certification();
					validatePortfolioErrorMessageAndGridData('level2');
					return true;
				}
			});*/
			dwr.engine.endBatch();
			return true;
		}
	}
}
function removeCertificationFile()
{
	
	var certId = document.getElementById("certId").value;
	if(window.confirm(resourceJSON.msgdeleteCertificationLicensure))
	{
		PFCertifications.removeCertification(certId,{ 
			async: false,
			callback: function(data){	
				document.getElementById("removeCert").style.display="none";
				hideForm_Certification();
				showGridCertifications();
				//validatePortfolioErrorMessageAndGridData('level2');
			},errorHandler:handleError
		});
	}
}

function removeReferences()
{
	
	var referenceId = document.getElementById("elerefAutoId").value;
	if(window.confirm(resourceJSON.msgdeletelatter))
	{
		PFCertifications.removeReferences(referenceId,
		{ 
			async: false,
			errorHandler:handleError,
			callback: function(data)
			{	
				document.getElementById("removeref").style.display="none";
				getElectronicReferencesGrid();
				hideElectronicReferencesForm();
				//validatePortfolioErrorMessageAndGridData('level2');
			}
		});
	}
}

function saveCertification(fileName,sbtsource){
	
	//alert("saveCertification fileName "+fileName +" sbtsource "+sbtsource);
	//$('#testcall').append("&#149; saveCertification<br>");
	
	var certId = document.getElementById("certId");
	
	var stateMaster = document.getElementById("stateMaster");
	var yearReceived = document.getElementById("yearReceived");
	
	var certType = document.getElementById("certType");
	var certUrl = trim(document.getElementById("certUrl").value);
	
	
	var certificationtypeMasterObj = {certificationTypeMasterId:dwr.util.getValue("certificationtypeMaster")};
	var certificationTypeMaster = document.getElementById("certificationtypeMaster").value;
	var yearExpires = document.getElementById("yearexpires").value;
	var doenumber = trim(document.getElementById("doenumber").value);
	
	var stateObj = {stateId:dwr.util.getValue("stateMaster")};
	var certificationStatusMasterObj= {certificationStatusId:dwr.util.getValue("certificationStatusMaster")};
	var certTypeObj = {certTypeId:dwr.util.getValue("certificateTypeMaster")};
	var certificate =null;
	
	if(dwr.util.getValue("certificationStatusMaster")==""){
		certificate = {certId:null, stateMaster:stateObj,yearReceived:trim(yearReceived.value),certType:null,certUrl:null,
				certificateTypeMaster:certTypeObj,pkOffered:null,kgOffered:null,g01Offered:null,g02Offered:null,
				g03Offered:null,g04Offered:null,g05Offered:null,g06Offered:null,g07Offered:null,g08Offered:null,
				g09Offered:null,g10Offered:null,g11Offered:null,g12Offered:null,readingQualifyingScore:null,
				writingQualifyingScore:null,mathematicsQualifyingScore:null};
	}else{
		certificate = {certId:null,certificationStatusMaster:certificationStatusMasterObj,certUrl:null,
				stateMaster:stateObj,yearReceived:trim(yearReceived.value),certType:trim(certType.value),certificateTypeMaster:certTypeObj,
				pkOffered:null,kgOffered:null,g01Offered:null,g02Offered:null,g03Offered:null,
				g04Offered:null,g05Offered:null,g06Offered:null,g07Offered:null,g08Offered:null,
				g09Offered:null,g10Offered:null,g11Offered:null,g12Offered:null,readingQualifyingScore:null,
				writingQualifyingScore:null,mathematicsQualifyingScore:null,certificationTypeMaster:certificationtypeMasterObj,
				yearExpired:yearExpires,doeNumber:doenumber};
	}
	
	dwr.engine.beginBatch();
	dwr.util.getValues(certificate);
	
	if(fileName!=""){
		certificate.pathOfCertification=fileName;
	}
	PFCertifications.saveOrUpdate(certificate,{
		async: true,
		errorHandler:handleError,
		callback:function(data)
			{
				if(data==1)
				{
					$('#errordiv_Certification').append("&#149; "+resourceJSON.msgCertificationLicensure+"<br>");
					$('#certType').focus();
					$('#certType').css("background-color",txtBgColor);
					$('#errordiv_Certification').show();
					return false;
				}else if(data==0){
					errorHandler:handleError
				}	
				//$('#loadingDiv').hide();
				hideForm_Certification();
				showGridCertifications();
				if(sbtsource==1)
				{
					/*if(certificationTypeMaster==2 || certificationTypeMaster==3)
					{
						try{ document.getElementById("displayGKAndSubject").value=true; } catch(e){}
						showHideGK_SubjectArea();
						saveAllDSPQ();
						saveAndContinueBottomPart();
						validatePortfolioErrorMessageAndGridData('level2');
					}
					else
					{
						try{ document.getElementById("displayGKAndSubject").value=false; } catch(e){}
						showHideGK_SubjectArea();
						saveAllDSPQ();
						saveAndContinueBottomPart();
						validatePortfolioErrorMessageAndGridData('level2');
					}*/
					
					displayGKAndSubject();
					saveAllDSPQ();
					saveAndContinueBottomPart();
					updateReturnThreadCount("cert");
					validatePortfolioErrorMessageAndGridData('level2');
				}
				else
				{
					/*if(certificationTypeMaster==2 || certificationTypeMaster==3)
					{
						try{ document.getElementById("displayGKAndSubject").value=true; } catch(e){}
						showHideGK_SubjectArea();
					}
					else
					{
						try{ document.getElementById("displayGKAndSubject").value=false; } catch(e){}
						showHideGK_SubjectArea();
					}*/
					
					displayGKAndSubject();
				}
			}
		});
	dwr.engine.endBatch();
	//sendToOtherPager();
}

function clearReferences()
{
	document.getElementById("pathOfReferenceFile").value=null;
	document.getElementById("pathOfReferenceFile").reset();
	$('#pathOfReferenceFile').css("background-color",""); 
}

function deleteRecord_Certificate(id)
{	
	document.getElementById("certificateTypeID").value=id;
	$('#deleteCertRec').modal('show');
}
function deleteCertificateRecord(){
	var id=document.getElementById("certificateTypeID").value;
	PFCertifications.deleteRecord(id,{ 
	async: true,
	errorHandler:handleError,
	callback:function(data)
	{
		$('#deleteCertRec').modal('hide');
		showGridCertifications();
		hideForm_Certification();
		
		displayGKAndSubject();
		
		return false;
	}});
}


function showEditForm(id)
{
	var certificate = {certId:null, certificationStatusMaster:null,stateMaster:null,yearReceived:null,certType:null,pkOffered:null,kgOffered:null,g01Offered:null,g02Offered:null,g03Offered:null,g04Offered:null,g05Offered:null,g06Offered:null,g07Offered:null,g08Offered:null,g09Offered:null,g10Offered:null,g11Offered:null,g12Offered:null,canServeAsSubTeacher:null};
	
	PFCertifications.showEditForm(id,
	{ 
		async: false,
		errorHandler:handleError,
		callback: function(data)
		{
			showForm_Certification();
						
			document.getElementById("certId").value=data.certId;
			if(data.stateMaster!=null && data.stateMaster.stateId!=null){
				document.getElementById("stateMaster").value=data.stateMaster.stateId;
			}
			
			var districtIdForDSPQ="";	
			if ($('#districtIdForDSPQ').length > 0) {
				districtIdForDSPQ=document.getElementById("districtIdForDSPQ").value;
			}
			if(districtIdForDSPQ==7800038){
				$(".certText").find(".jqte_editor").html(data.certText);
			}
			
			if(districtIdForDSPQ==7800040)
				$('#IEINNumber').val(data.ieinNumber);
			
			if(data.certificationStatusMaster!=null)
				document.getElementById("certificationStatusMaster").value=data.certificationStatusMaster.certificationStatusId;
			else
				document.getElementById("certificationStatusMaster").value="";
			
			if(data.certificationTypeMaster!=null)
				document.getElementById("certificationtypeMaster").value=data.certificationTypeMaster.certificationTypeMasterId;
			else
				document.getElementById("certificationtypeMaster").value="0";
			
			if(data.doeNumber==0)
			{
				document.getElementById("doenumber").value="";
			}	
			else
			{
				document.getElementById("doenumber").value=data.doeNumber;
			}
			
			if(data.yearExpired==0 || data.yearExpired==null || data.yearExpired=='')
			{
				document.getElementById("yearexpires").text="";
			}
			else
			{
				document.getElementById("yearexpires").value=data.yearExpired;
			}	
			
			
			document.getElementById("yearReceived").value=data.yearReceived;
			document.getElementById("certUrl").value=data.certUrl;
			dwr.util.setValues(data);
			if(data.certificateTypeMaster!=null)
			{
				document.getElementById("certType").value=data.certificateTypeMaster.certType;
				document.getElementById("certificateTypeMaster").value=data.certificateTypeMaster.certTypeId;
			}
			else
			{
				document.getElementById("certType").value="";
				document.getElementById("certificateTypeMaster").value="";
			}
			
			if(data.pathOfCertification!=null && data.pathOfCertification!="")
			{	
				//alert("yes");
				document.getElementById("removeCert").style.display="block";
				document.getElementById("divCertName").style.display="block";
				document.getElementById("pathOfCertification").value=data.pathOfCertification;
				
				document.getElementById("divCertName").innerHTML="<a href='javascript:void(0)' id='hrefEditRef' onclick=\"downloadCertification('"+data.certId+"','hrefEditRef');" +
				"if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0');" +
				"return false;\">"+data.pathOfCertification+"</a>";
			}else{
				//alert("No");
				document.getElementById("divCertName").style.display="none";
				document.getElementById("removeCert").style.display="none";
				document.getElementById("pathOfCertificationFile").value="";
				document.getElementById("pathOfCertification").value="";
			}
			fieldsDisable(data.certificationStatusMaster.certificationStatusId);
			getPraxis();
			return false;
		}
	});
	return false;
}
/*function hideTFAFields()
{
	$('#errordivExp').empty();
	$('#expCertTeacherTraining').css("background-color","");
	$('#tfaAffiliate').css("background-color","");
	$('#corpsYear').css("background-color","");
	$('#tfaRegion').css("background-color","");
	
	var tfaAffiliate 	= 	document.getElementById("tfaAffiliate").value;
	if(tfaAffiliate=="3")
	{
		$("#tfaFieldsDiv").fadeOut();
	}
	else
	{
		$("#tfaFieldsDiv").fadeIn();
	}
}*/


/*function saveAndContinueCertifications()
{
	if($("#stateMaster").is(':visible'))
		insertOrUpdate_Certification();
}

function saveAndContinueReferences()
{
	if($("#salutation").is(':visible'))
		insertOrUpdateElectronicReferences();
}*/

function hideForm_Certification()
{
	
	document.getElementById("frmCertificate").reset();
	document.getElementById("certId").value="";	
	document.getElementById("divMainForm").style.display="none";
	$('#errordiv_Certification').empty();
	setDefColortoErrorMsg_Certification();
	window.location.hash = '#addCertificationIdDSPQ';
	return false;
}
function chkNBCert(chk)
{
	if(chk=="1")
	{	
		if($("#headQuaterIdForDspq").val()==2 && ($("#dspqName").val()=="Option A" || $("#dspqName").val()=="Option C"))
		{
			document.getElementById("divNbdCert").style.display="none";
		}
		else
		{
			document.getElementById("divNbdCert").style.display="block";
		}
		
	}
	else
	{
		document.getElementById("divNbdCert").style.display="none";
	}
	document.getElementById("")	
}

function setDefColortoErrorMsgToExp()
{	
	$('#expCertTeacherTraining').css("background-color","");
	$('#nationalBoardCert').css("background-color","");
	$('#nationalBoardCertYear').css("background-color","");
}
function setDefColortoErrorMsg_Certification()
{
	$('#certificationtypeMaster').css("background-color","");
	$('#stateMaster').css("background-color","");
	$('#yearReceived').css("background-color","");
	$('#yearexpires').css("background-color","");
	$('#certType').css("background-color","");
	$('#certName').css("background-color","");
	$('#certificationStatusMaster').css("background-color","");
	$('#certUrl').css("background-color","");
	var districtIdForDSPQ="";	
	if ($('#districtIdForDSPQ').length > 0) {
		districtIdForDSPQ=document.getElementById("districtIdForDSPQ").value;
	}
	
	if(districtIdForDSPQ==7800040)
	{
		$('#IEINNumber').css("background-color","");
	}
	
}
function chkCertificateForm()
{
	var idList = new Array(resourceJSON.msgstateMaster,resourceJSON.msgyearReceived,resourceJSON.msgcertType);
	var count = 0;
	for(i=0;i<idList.length;i++)
	{
		if(trim(document.getElementById(idList[i]).value)!="")
		{
			count++;	
		}
	}
	if(count==0)
	{
		return true;	
	}
	else
	{
		insertOrUpdate_Certification()
		if(count==3){
			return true;	
		}
	}
}
function chkForEnter_Certifications(evt)
{	
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==13)
	{
		insertOrUpdate_Certification();
	}	
}
/*function chkForEnterCert(evt)
{	
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==13)
	{
		saveAndContinueCertifications();
	}	
}*/

//Ramesh :: St. for ElectronicReferences

function chkForEnterElectronicReferences(evt)
{
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	//alert(">?>"+evt.srcElement.id)
	if(charCode==13 && evt.srcElement.id!="")
	{
		insertOrUpdateElectronicReferences();
	}	
}

function showElectronicReferencesForm()
{
	document.getElementById("frmElectronicReferences").reset();
	document.getElementById("elerefAutoId").value="";
	document.getElementById("divElectronicReferences").style.display="block";
	document.getElementById("removeref").style.display="none";
	document.getElementById("salutation").focus();
	setDefColortoErrorMsgToElectronicReferences();
	$('#errordivElectronicReferences').empty();
	$("#referenceDetailText").find(".jqte_editor").html("");
	var districtIdForDSPQ="";	
	if ($('#districtIdForDSPQ').length > 0) {
		districtIdForDSPQ=document.getElementById("districtIdForDSPQ").value;
	}
	if($("#districtIdForDSPQ").val()==806810 && ($("#jobcategoryDsp").val().trim()=='Support Staff' || $("#jobcategoryDsp").val().trim()=='Coaching')){
		$("#recomendationOpt").hide();
	}
	if(districtIdForDSPQ==7800038){
		$('.nobleCssShow').show();
	}
	if(districtIdForDSPQ==4218990){
		$('.PHLreq').show();
		$('.onlyPHL').show();
	}
	if(districtIdForDSPQ==7800285){
		$('.recomDiv').hide();
	}	
if(districtIdForDSPQ==1201470 || districtIdForDSPQ==5510470 || ($("#headQuaterIdForDspq").val()==2 && ($('#dspqName').val()!='Option A' && $('#dspqName').val()!='Option B' ) &&(districtIdForDSPQ!=3702340)) ){
		
		$('.PHLreq').show();		
	}
if($("#headQuaterIdForDspq").val()==2)
{
	$('#cancontactastrik').show();
}
	showAndHideCanContOnOffer(1);
	return false;
}

function hideElectronicReferencesForm()
{
	document.getElementById("frmElectronicReferences").reset();
	document.getElementById("elerefAutoId").value="";
	document.getElementById("divElectronicReferences").style.display="none";
	window.location.hash = '#addReferIdDSPQ';
	return false;
}


function insertOrUpdateElectronicReferences(sbtsource)
{
	var testHereReff="true";
	if(sbtsource=="0")
	{
		resetSBTNSource();
	}
	
	var threadCount_ref=document.getElementById("threadCount_ref").value;
	if(threadCount_ref=="0")
	{
		document.getElementById("threadCount_ref").value=1;
		
		updateThreadCount("ref");
		//$('#testcall').append("&#149; insertOrUpdateElectronicReferences<br>");
		
		var elerefAutoId	=	document.getElementById("elerefAutoId");
		var salutation		=	document.getElementById("salutation");
		
		var firstName		=	trim(document.getElementById("firstName").value);
		var lastName		= 	trim(document.getElementById("lastName").value);;
		var designation		= 	trim(document.getElementById("designation").value);		
		var organization	=	trim(document.getElementById("organization").value);
		var email			=	trim(document.getElementById("email").value);
		
		var contactnumber	=	trim(document.getElementById("contactnumber").value);
		var rdcontacted0	=   document.getElementById("rdcontacted0");
		var rdcontacted1	=   document.getElementById("rdcontacted1");
		var canContOnOffer0	=   document.getElementById("canContOnOffer0");
		var canContOnOffer1	=   document.getElementById("canContOnOffer1");
		var pathOfReferencesFile = document.getElementById("pathOfReferenceFile");
		var referenceDetails 	= trim($("#referenceDetailText").find(".jqte_editor").html());
		var longHaveYouKnow	=   trim(document.getElementById("longHaveYouKnow").value);
		
		
		var cnt=0;
		var focs=0;	
		
		var fileName = pathOfReferencesFile.value;
		var ext = fileName.substr(fileName.lastIndexOf('.') + 1).toLowerCase();
		var fileSize=0;		
		if ($.browser.msie==true){	
		    fileSize = 0;	   
		}else{		
			if(pathOfReferencesFile.files[0]!=undefined)
			fileSize = pathOfReferencesFile.files[0].size;
		}
		//pawan kumar
		/*if(fileSize>=10485760)
		{
			testHereReff="false";
			return false;
		}*/
		$('#errordivElectronicReferences').empty();
		setDefColortoErrorMsgToElectronicReferences();

		var districtIdForDSPQ="";	
		if ($('#districtIdForDSPQ').length > 0) {
			districtIdForDSPQ=document.getElementById("districtIdForDSPQ").value;
		}
		
		
		if(firstName=="")
		{
			$('#errordivElectronicReferences').append("&#149; "+resourceJSON.PlzEtrFirstName+"<br>");
			if(focs==0)
				$('#firstName').focus();

			$('#firstName').css("background-color",txtBgColor);
			cnt++;focs++;
		}
		if(lastName=="")
		{
			$('#errordivElectronicReferences').append("&#149; "+resourceJSON.PlzEtrLastName+"<br>");
			if(focs==0)
				$('#lastName').focus();

			$('#lastName').css("background-color",txtBgColor);
			cnt++;focs++;
		}
		if(designation=="" && ($("#dspqName").val()=="Option C" || $("#dspqName").val()=="Option D" || districtIdForDSPQ==4218990 || districtIdForDSPQ==3904380 || districtIdForDSPQ==1201470 || districtIdForDSPQ==5510470)&&(districtIdForDSPQ!=3702340))
		{
			$('#errordivElectronicReferences').append("&#149; "+resourceJSON.msgTitle+"<br>");
			if(focs==0)
				$('#designation').focus();

			$('#designation').css("background-color",txtBgColor);
			cnt++;focs++;
		}	
		
		//Gaurav Kumar
		if(designation=="" && (districtIdForDSPQ==4800061 || (districtIdForDSPQ==2633090 && ($('#jobcategoryDsp').val().trim()=="Professional Certified")))){
			$('#errordivElectronicReferences').append("&#149; "+resourceJSON.msgTitle+"<br>");
			if(focs==0)
				$('#designation').focus();

			$('#designation').css("background-color",txtBgColor);
			cnt++;focs++;	
		}

		if(organization=="" && ($("#dspqName").val()=="Option C" || $("#dspqName").val()=="Option D" || districtIdForDSPQ==7800038 || districtIdForDSPQ==4218990 || districtIdForDSPQ==3904380  || districtIdForDSPQ==1201470 || districtIdForDSPQ==5510470 || document.getElementById("districtIdForDSPQ").value==7800144 || document.getElementById("districtIdForDSPQ").value==7800202)&&(districtIdForDSPQ!=3702340)){
			$('#errordivElectronicReferences').append("&#149; "+resourceJSON.PlzEtrOrg+"<br>");
			if(focs==0)
				$('#organization').focus();

			$('#organization').css("background-color",txtBgColor);
			cnt++;focs++;
		}
		//Gaurav Kumar
		if(organization=="" && (districtIdForDSPQ==4800061 || (districtIdForDSPQ==2633090 &&(($('#jobcategoryDsp').val().trim()=="Professional Certified")||($('#jobcategoryDsp').val().trim()=="Drivers")||($('#jobcategoryDsp').val().trim()=="Clerical")||($('#jobcategoryDsp').val().trim()=="Other"))))){
			$('#errordivElectronicReferences').append("&#149; "+resourceJSON.PlzEtrOrg+"<br>");
			if(focs==0)
				$('#organization').focus();

			$('#organization').css("background-color",txtBgColor);
			cnt++;focs++;	
		}
		
		if(email=="")
		{
			$('#errordivElectronicReferences').append("&#149; "+resourceJSON.PlzEtrEmail+"<br>");
			if(focs==0)
				$('#email').focus();
			
			$('#email').css("background-color",txtBgColor);
			cnt++;focs++;
		}
		else if(!isEmailAddress(email))
		{		
			$('#errordivElectronicReferences').append("&#149; "+resourceJSON.PlzEtrVldEmail+"<br>");
			if(focs==0)
				$('#email').focus();
			
			$('#email').css("background-color",txtBgColor);
			cnt++;focs++;
		}
		if(contactnumber=="")
		{
			$('#errordivElectronicReferences').append("&#149; "+resourceJSON.PlzEtrCtctNum+"<br>");
			if(focs==0)
				$('#contactnumber').focus();

			$('#contactnumber').css("background-color",txtBgColor);
			cnt++;focs++;
		}
		else if(contactnumber.length==10)
		{
			
		}
		else
		{
			$('#errordivElectronicReferences').append("&#149; "+resourceJSON.errorMsgForNumber +"<br>");
			if(focs==0)
				$('#contactnumber').focus();

			$('#contactnumber').css("background-color",txtBgColor);
			cnt++;focs++;
		}
		
		if(longHaveYouKnow=="" && districtIdForDSPQ==4218990)
		{
			$('#errordivElectronicReferences').append("&#149; "+resourceJSON.msgYouKnowThisPerson+"?<br>");
			if(focs==0)
				$('#longHaveYouKnow').focus();

			$('#longHaveYouKnow').css("background-color",txtBgColor);
			cnt++;focs++;
		}
		
        var rdcontacted_value;
		
		if (rdcontacted0.checked) {
			rdcontacted_value = false;
		}
		
		else if (rdcontacted1.checked) {
			rdcontacted_value = true;
		}
	    else if($("#headQuaterIdForDspq").val()==2)
		{
			$('#errordivElectronicReferences').append("&#149; Please indicate if reference may be contacted.");
			if(focs==0)
				$('#rdcontacted1').focus();

			$('#rdcontacted1').css("background-color",txtBgColor);
			cnt++;focs++;			
		}
		var canContOnOffer_value=false;
		
		if (canContOnOffer0.checked) {
			canContOnOffer_value = false;
		}
		else if (canContOnOffer1.checked) {
			canContOnOffer_value = true;
		}
		
		
		if($("#recommLetter").is(':visible')){
			if(fileName==""){
				$('#errordivElectronicReferences').append("&#149; "+resourceJSON.msgUploadRecommendation+"</BR>");
				if(focs==0)
					$('#pathOfReferenceFile').focus();
				
				$('#pathOfReferenceFile').css("background-color",txtBgColor);
				cnt++;focs++;	
			}
		}
		
		if(pathOfReferenceFile!=null && fileName!=null || pathOfReferenceFile!="" && fileName!="")
		{
		if(ext!=""){
			if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt'))
			{
				$('#errordivElectronicReferences').append("&#149; "+resourceJSON.AcceptFileFormat+"<br>");
					if(focs==0)
						$('#pathOfReferenceFile').focus();
					
					$('#pathOfReferenceFile').css("background-color",txtBgColor);
					cnt++;focs++;	
					return false;
			}	
			else if(fileSize>=10485760)
			{
				$('#errordivElectronicReferences').append("&#149; "+resourceJSON.FileSizeLessthan10Mb+"<br>");
					if(focs==0)
						$('#pathOfReferenceFile').focus();
					
					$('#pathOfReferenceFile').css("background-color",txtBgColor);
					cnt++;focs++;	
					return false;
			}
		}
		}
		
		/*else if(isEmailAddress(trim(email.value)))
		{
			PFCertifications.checkEmailForEleRef(email.value,
			{ 
				async: true,
				errorHandler:handleError,
				callback:function(data)
				{
					if(data==true)
					{
						$('#errordivElectronicReferences').append("&#149; A Electronic References has already registered with the email.<br>");
						if(focs==0)
							$('#email').focus();
						$('#email').css("background-color",txtBgColor);
							cnt++;focs++;
					}
				}
			});
		}*/

		if(cnt!=0)		
		{
			updateReturnThreadCount("ref");
			$('#errordivElectronicReferences').show();
			return false;
		}
		else
		{
			var teacherElectronicReferences = {elerefAutoId:null,salutation:null, firstName:null, lastName:null, designation:null, organization:null,email:null,contactnumber:null,rdcontacted:null,canContOnOffer:null,referenceDetails:null,longHaveYouKnow:null};
			dwr.engine.beginBatch();
			
			
			dwr.util.getValues(teacherElectronicReferences);
			teacherElectronicReferences.rdcontacted=rdcontacted_value;
			teacherElectronicReferences.canContOnOffer=canContOnOffer_value;
			teacherElectronicReferences.salutation=salutation.value;
			teacherElectronicReferences.firstName=firstName;
			teacherElectronicReferences.lastName=lastName;;
			teacherElectronicReferences.designation=designation;
			teacherElectronicReferences.organization=organization;
			teacherElectronicReferences.email=email;
			teacherElectronicReferences.referenceDetails=referenceDetails;
			teacherElectronicReferences.longHaveYouKnow=longHaveYouKnow;
			//referenceDetails
			
			var refFile = document.getElementById("pathOfReference").value;
			if(refFile!=""){
				teacherElectronicReferences.pathOfReference=refFile;
			}
			if(fileName!="")
			{	
				//$('#loadingDiv').show();
				//document.getElementById("frmElectronicReferences").submit();
				PFCertifications.findDuplicateEleReferences(teacherElectronicReferences,{ 
					async: false,
					errorHandler:handleError,
					callback: function(data)
					{
					if(data=="isDuplicate")
					{
						updateReturnThreadCount("ref");
						$('#errordivElectronicReferences').append("&#149; "+resourceJSON.ElectronicRefAlreadyReg+"<br>");
						if(focs==0)
							$('#email').focus();
						$('#email').css("background-color",txtBgColor);
						cnt++;focs++;
						$('#errordivElectronicReferences').show();
						return false;
					}else{
						//$('#loadingDiv').show();
						document.getElementById("frmElectronicReferences").submit();
					}
					}
				});
				
			}else{
				PFCertifications.saveOrUpdateElectronicReferences(teacherElectronicReferences,{ 
					async: true,
					errorHandler:handleError,
					callback: function(data)
					{
						if(data=="isDuplicate")
						{
							updateReturnThreadCount("ref");
							$('#errordivElectronicReferences').append("&#149; "+resourceJSON.ElectronicRefAlreadyRegWithProvidedEmail+"<br>");
							if(focs==0)
								$('#email').focus();
							$('#email').css("background-color",txtBgColor);
								cnt++;focs++;
							$('#errordivElectronicReferences').show();
							return false;
						}
						hideElectronicReferencesForm();
						getElectronicReferencesGrid();
						if(sbtsource==1)
						{
							saveAllDSPQ();
							updateReturnThreadCount("ref");
							if(testHereReff){
								validatePortfolioErrorMessageAndGridData('level2');
							}
						}
					}
				});
			}
			
			/*PFCertifications.saveOrUpdateElectronicReferences(teacherElectronicReferences,{ 
				async: false,
				errorHandler:handleError,
				callback: function(data)
				{
					if(data=="isDuplicate")
					{
						$('#errordivElectronicReferences').append("&#149; An Electronic Reference has already registered with the provided Email.<br>");
						if(focs==0)
							$('#email').focus();
						$('#email').css("background-color",txtBgColor);
							cnt++;focs++;
						$('#errordivElectronicReferences').show();
						return false;
					}
					else
					{
						hideElectronicReferencesForm();
						getElectronicReferencesGrid();
						
						validatePortfolioErrorMessageAndGridData('level2');
					}
				}
			});*/
			dwr.engine.endBatch();
			return true;
		}
	}
	return testHereReff;
}

var saveRefFlag=0;
function saveReference(fileName,sbtsource){
	//$('#testcall').append("&#149; saveReference<br>");
	
	var elerefAutoId	=	document.getElementById("elerefAutoId");
	var salutation		=	document.getElementById("salutation");
	
	var firstName		=	trim(document.getElementById("firstName").value);
	var lastName		= 	trim(document.getElementById("lastName").value);;
	var designation		= 	trim(document.getElementById("designation").value);
	
	var organization	=	trim(document.getElementById("organization").value);
	var email			=	trim(document.getElementById("email").value);
	
	var contactnumber	=	trim(document.getElementById("contactnumber").value);
	var rdcontacted0	=   document.getElementById("rdcontacted0");
	var rdcontacted1	=   document.getElementById("rdcontacted1");
	var longHaveYouKnow	=   trim(document.getElementById("longHaveYouKnow").value);
	var referenceDetails 	= trim($("#referenceDetailText").find(".jqte_editor").html());
	var cnt=0;
	var focs=0;	
	
	var rdcontacted_value;
	if (rdcontacted0.checked) {
		rdcontacted_value = false;
	}
	else if (rdcontacted1.checked) {
		rdcontacted_value = true;
	}else if($("#headQuaterIdForDspq").val()==2)
	{
		$('#errordivElectronicReferences').append("&#149; Please indicate if reference may be contacted.");
		if(focs==0)
			$('#rdcontacted1').focus();

		$('#rdcontacted1').css("background-color",txtBgColor);
		cnt++;focs++;			
	}
	else
	{
		rdcontacted_value=true;
	}
	var teacherElectronicReferences = {elerefAutoId:null,salutation:null, firstName:null, lastName:null, designation:null, organization:null,email:null,contactnumber:null,rdcontacted:null,pathOfReference:null,referenceDetails:null,longHaveYouKnow:null};
	dwr.engine.beginBatch();
	dwr.util.getValues(teacherElectronicReferences);
	
	teacherElectronicReferences.salutation=salutation.value;
	teacherElectronicReferences.rdcontacted=rdcontacted_value;
	teacherElectronicReferences.firstName=firstName;
	teacherElectronicReferences.lastName=lastName;;
	teacherElectronicReferences.designation=designation;
	teacherElectronicReferences.organization=organization;
	teacherElectronicReferences.email=email;
	teacherElectronicReferences.pathOfReference=fileName;
	teacherElectronicReferences.referenceDetails=referenceDetails;
	teacherElectronicReferences.longHaveYouKnow=longHaveYouKnow;
	PFCertifications.saveOrUpdateElectronicReferences(teacherElectronicReferences,{ 
		async: true,
		errorHandler:handleError,
		callback: function(data)
		{
			if(data=="isDuplicate")
			{
				updateReturnThreadCount("ref");
				$('#errordivElectronicReferences').append("&#149; "+resourceJSON.ElectronicRefAlreadyRegWithProvidedEmail+"<br>");
				if(focs==0)
					$('#email').focus();
				$('#email').css("background-color",txtBgColor);
					cnt++;focs++;
				$('#errordivElectronicReferences').show();
				return false;
			}
			//$('#loadingDiv').hide();
			saveRefFlag=1;
			getElectronicReferencesGrid();
			hideElectronicReferencesForm();
			if(sbtsource==1)
			{
				saveAllDSPQ();
				updateReturnThreadCount("ref");
				validatePortfolioErrorMessageAndGridData('level2');
			}
		}
	});
	dwr.engine.endBatch();
	return true;
}

function setDefColortoErrorMsgToElectronicReferences()
{
	$('#salutation').css("background-color","");
	$('#firstName').css("background-color","");
	$('#lastName').css("background-color","");
	$('#designation').css("background-color","");
	$('#organization').css("background-color","");
	$('#email').css("background-color","");
	$('#contactnumber').css("background-color","");
}


function getElectronicReferencesGrid()
{
	var districtIdForDSPQ="";	
	if ($('#districtIdForDSPQ').length > 0) {
		districtIdForDSPQ=document.getElementById("districtIdForDSPQ").value;
	}
	
	if(districtIdForDSPQ==7800038)
	PFCertifications.getElectronicReferencesGridNoble(dp_Reference_noOfRows,dp_Reference_page,dp_Reference_sortOrderStr,dp_Reference_sortOrderType, { 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
		$('#divDataElectronicReferences').html(data);
		applyScrollOnTblEleRef();
		}});
	else
		PFCertifications.getElectronicReferencesGrid(dp_Reference_noOfRows,dp_Reference_page,dp_Reference_sortOrderStr,dp_Reference_sortOrderType, { 
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{
			$('#divDataElectronicReferences').html(data);
			applyScrollOnTblEleRef();
			if(districtIdForDSPQ==7800285){
				$(".recomTit").html("");
			}
			}});
}

var eleRefIDForTeacher=0;
var status_eref=0;
function changeStatusElectronicReferences(id,status)
{	
	eleRefIDForTeacher=id;
	status_eref=status;
	if(status==1)
	$('#chgstatusRef1').modal('show');
	else if(status==0)
	$('#chgstatusRef2').modal('show');	
}

function changeStatusElectronicReferencesConfirm()
{	
	$('#chgstatusRef1').modal('hide');
	$('#chgstatusRef2').modal('hide');
	
		PFCertifications.changeStatusElectronicReferences(eleRefIDForTeacher,status_eref, { 
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{
				getElectronicReferencesGrid();
				hideElectronicReferencesForm();
				return false;
			}});
		
		eleRefIDForTeacher=0;
}

function editFormElectronicReferences(id)
{
	PFCertifications.editElectronicReferences(id,
		{ 
			async: false,
			errorHandler:handleError,
			callback: function(data)
			{
				showElectronicReferencesForm();
				dwr.util.setValues(data);
				showAndHideCanContOnOffer(0);
				$("#referenceDetailText").find(".jqte_editor").html(data.referenceDetails);
				//alert(data.referenceDetails);
				if(data.pathOfReference!=null && data.pathOfReference!="")
				{	
					document.getElementById("removeref").style.display="block";
					document.getElementById("pathOfReference").value=data.pathOfReference;
					document.getElementById("divRefName").style.display="block";
					document.getElementById("divRefName").innerHTML="<a href='javascript:void(0)' id='hrefEditRef' onclick=\"downloadReference('"+data.elerefAutoId+"','hrefEditRef');" +
					"if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0');" +
					"return false;\">"+data.pathOfReference+"</a>";
				}else{
					document.getElementById("divRefName").style.display="none";
					document.getElementById("removeref").style.display="none";
					document.getElementById("pathOfReference").value="";
				}
				
				return false;
			}
		});
	return false;
}

function downloadReference(referenceId, linkId)
{		
		PFCertifications.downloadReference(referenceId,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	if(data=="")
			{
				data="javascript:void(0)";
			}
			else if(data.indexOf(".doc")!=-1)
			{
				document.getElementById("ifrmRef").src=data;
			}else
			{
				document.getElementById(linkId).href = data;
			}
			return false;
		}});
}

function downloadCertification(certId, linkId)
{		
		PFCertifications.downloadCertification(certId,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data){
			if(data=="")
			{
				data="javascript:void(0)";
			}
			else if(data.indexOf(".doc")!=-1)
			{
				document.getElementById("ifrmCert").src=data;
			}else
			{
				document.getElementById(linkId).href = data;
			}
			return false;
		}});
}

//Ed. for ElectronicReferences

// Ramesh :: St Video Links

function showVideoLinksForm()
{
	document.getElementById("frmvideoLinks").reset();
	document.getElementById("videolinkAutoId").value="";
	document.getElementById("videofile").value="";
	document.getElementById("video").style.display="block";
	document.getElementById("divvideoLinks").style.display="block";
	document.getElementById("videourl").focus();
	setDefColortoErrorMsgToVideoLinksForm();
	$('#errordivvideoLinks').empty();
	$('#video').empty();
	return false;
}

function setDefColortoErrorMsgToVideoLinksForm()
{
	$('#videourl').css("background-color","");
}


function hideVideoLinksForm()
{
	document.getElementById("frmvideoLinks").reset();
	document.getElementById("videolinkAutoId").value="";
	document.getElementById("divvideoLinks").style.display="none";
	window.location.hash = '#addVideoIdDSPQ';
	return false;
}


function getVideoLinksGrid()
{
	PFCertifications.getVideoLinksGrid(dp_VideoLink_Rows,dp_VideoLink_page,dp_VideoLink_sortOrderStr,dp_VideoLink_sortOrderType, { 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#divDataVideoLinks').html(data);
			applyScrollOnTblVideoLinks();
		}});
}

var videolnkIDForTeacher=0;
function delVideoLink(id)
{	
	videolnkIDForTeacher=id;
	$('#delVideoLnk').modal('show');
}

function delVideoLnkConfirm()
{	
		$('#delVideoLnk').modal('hide');
		PFCertifications.deleteVIdeoLink(videolnkIDForTeacher, { 
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{
				getVideoLinksGrid();
				hideVideoLinksForm();
				return false;
			}});
		
		videolnkIDForTeacher=0;
}


function insertOrUpdatevideoLinks()
{
	var elerefAutoId = document.getElementById("videolinkAutoId");
	var videourl = trim(document.getElementById("videourl").value);
	var video = trim(document.getElementById("videofile").value);	
	var videoSize = document.getElementById("videofile");
	var oldvideo = document.getElementById("video").innerHTML;
	var cnt=0;
	var focs=0;	
	
	$('#errordivvideoLinks').empty();
	setDefColortoErrorMsgToVideoLinksForm();
	
	if(videourl=="" && video =="" && oldvideo =="")
	{
		$('#errordivvideoLinks').append("&#149; "+resourceJSON.msgvideolink+"<br>");
		if(videourl==0)
			$('#videourl').focus();
		
		$('#videourl').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	if(video !="")
	{
		var ext = video.substr(video.lastIndexOf('.') + 1).toLowerCase();		
		if(!(ext=="mp4"||ext=="MP4"||ext=="ogv"||ext=="ogg"||ext=="webm"))
		{        	
		 $('#errordivvideoLinks').append("&#149; "+resourceJSON.msgacceptablevideo+"");
		 if(video==0)
			$('#videourl').focus();
		
		$('#videourl').css("background-color",txtBgColor);
		cnt++;focs++;
		}
		else if(videoSize.files[0].size>10485760)
		{        	
		 $('#errordivvideoLinks').append("&#149; "+resourceJSON.msgvideosize+"<br>");
		 if(video==0)
			$('#videourl').focus();
		
		$('#videourl').css("background-color",txtBgColor);
		cnt++;focs++;
		}
		else
		{
			video=video.replace(/^.*[\\\/]/, '');	
		}
	}
	else
	{
		video= document.getElementById('video').innerHTML;
	}
	if(cnt!=0 )		
	{
		$('#errordivvideoLinks').show();
		return false;
	}
	else
	{	
		$('#loadingDiv').show();		
		if(videoSize.value !="")
		{			
			document.getElementById("frmvideoLinks").submit();
		}	
		else
		{
		var teacherVideoLink = {videolinkAutoId:null,videourl:null, video:null, createdDate:null};
		dwr.engine.beginBatch();
		dwr.util.getValues(teacherVideoLink);
		teacherVideoLink.videourl=videourl;		
		teacherVideoLink.video=	oldvideo;	
		PFCertifications.saveOrUpdateVideoLinks(teacherVideoLink,{ 
			async: false,
			errorHandler:handleError,
			callback: function(data)
			{			
				$('#loadingDiv').hide();					
				hideVideoLinksForm();
				getVideoLinksGrid();
			}
		});
		dwr.engine.endBatch();
		return true;
	}   
  }
}


function editFormVideoLink(id)
{
	PFCertifications.editVideoLink(id,
		{ 
			async: false,
			errorHandler:handleError,
			callback: function(data)
			{
				showVideoLinksForm();
				dwr.util.setValues(data);
				return false;
			}
		});
	return false;
}
function checkForEnter(evt)
{
	var charCode = (evt.which) ? evt.which : event.keyCode;
	if(charCode==13){
		insertOrUpdatevideoLinks();
		return true;
	}
}
//Ramesh :: End Video Links


function clearScoreReport()
{
	document.getElementById("scoreReport").value=null;
	document.getElementById("scoreReport").reset();
	$('#scoreReport').css("background-color","");	
}

function removeScoreReport()
{
	
	var scoreReport = document.getElementById("scoreReport").value;
	if(window.confirm(resourceJSON.msgSubjectAreaScoreReport))
	{
		PFCertifications.removeScoreReport(scoreReport,
		{ 
			async: false,
			errorHandler:handleError,
			callback: function(data)
			{	
				getSAEValues();
			}
		});
	}
}


function clearGeneralKnowledgeScoreReport()
{
	document.getElementById("generalKnowledgeScoreReport").value=null;
	document.getElementById("generalKnowledgeScoreReport").reset();
	$('#generalKnowledgeScoreReport').css("background-color","");	
}

function removeGeneralKnowledgeScoreReport()
{
	
	var generalKnowledgeScoreReport = document.getElementById("generalKnowledgeScoreReport").value;
	if(window.confirm(resourceJSON.msgGeneralKnowledgeScoreReport))
	{
		PFCertifications.removeGeneralKnowledgeScoreReport(generalKnowledgeScoreReport,
		{ 
			async: false,
			errorHandler:handleError,
			callback: function(data)
			{	
				getGKEValues();
			//	document.getElementById("removeref").style.display="none";
			//	getElectronicReferencesGrid();
				//hideElectronicReferencesForm();
				//validatePortfolioErrorMessageAndGridData('level2');
			}
		});
	}
}
function clearUploadedDocument()
{
	document.getElementById("uploadedDocument").value=null;
	document.getElementById("uploadedDocument").reset();
	$('#uploadedDocument').css("background-color","");	
}

function downloadGeneralKnowledge(generalKnowledgeExamId, linkId)
{		
		PFCertifications.downloadGeneralKnowledge(generalKnowledgeExamId,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	if(data=="")
			{
				data="javascript:void(0)";
			}
			else if(data.indexOf(".doc")!=-1)
			{
				document.getElementById("ifrmRef").src=data;
			}else
			{
				document.getElementById(linkId).href = data;
			}
			return false;
		}});
}
function downloadSubjectAreaExam(subjectAreaExamId, linkId)
{		
		PFCertifications.downloadSubjectAreaExam(subjectAreaExamId,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
			if(data=="")
			{
				data="javascript:void(0)";
			}
			else if(data.indexOf(".doc")!=-1)
			{
				document.getElementById("ifrmRef").src=data;
			}else
			{
				document.getElementById(linkId).href = data;
			}
			return false;
		}});
}

function downloadUploadedDocument(additionDocumentId, linkId)
{		
		PFCertifications.downloadUploadedDocument(additionDocumentId,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
			if(data=="")
			{
				data="javascript:void(0)";
			}
			else if(data.indexOf(".doc")!=-1)
			{
				document.getElementById("ifrmRef").src=data;
			}else
			{
				document.getElementById(linkId).href = data;
			}
			return false;
		}});
}

function getGKEValues()
{
	PFCertifications.getGKEValues({ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
		try{
			document.getElementById("generalKnowledgeExamStatus").value=data.generalKnowledgeExamStatus;
			try{
				document.getElementById("generalKnowledgeExamDate").value=(new Date(data.generalKnowledgeExamDate).getMonth()+1)+"-"+(new Date(data.generalKnowledgeExamDate).getDate())+"-"+(new Date(data.generalKnowledgeExamDate).getFullYear()); 
			}catch(err){}
			try{
				document.getElementById("generalKnowledgeScoreReport").value=data.generalKnowledgeScoreReport;
			}catch(err){}
			try{
				document.getElementById("generalKnowledgeScoreReportHidden").value=data.generalKnowledgeScoreReport;
			}catch(err){}
			try{
				$("#generalExamNote").find(".jqte_editor").html(data.generalExamNote);
				$('[name="generalExamNote"]').text(data.generalExamNote);
			}catch(err){}
			
			if(data.generalKnowledgeScoreReport!=null && data.generalKnowledgeScoreReport!="")
			{	
				document.getElementById("removeGeneralKnowledgeScoreReportSpan").style.display="inline";
				document.getElementById("divGeneralKnowledgeScoreReport").style.display="inline";
				document.getElementById("divGeneralKnowledgeScoreReport").innerHTML=resourceJSON.msgRecentscorereportonfile+": <a href='javascript:void(0)' id='hrefGeneralKnowledge' onclick=\"downloadGeneralKnowledge('"+data.generalKnowledgeExamId+"','hrefGeneralKnowledge');" +
				"if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0');" +
				"return false;\">"+data.generalKnowledgeScoreReport+"</a>";
				
				//$('#hrefGeneralKnowledge').tooltip();
				
			}else{
				document.getElementById("divGeneralKnowledgeScoreReport").style.display="none";
				document.getElementById("removeGeneralKnowledgeScoreReportSpan").style.display="none";
				document.getElementById("generalKnowledgeScoreReport").value="";
			}
		}catch(err){}
		}});
}
function getSAEValues()
{
	PFCertifications.getSAEValues({ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
		try{
			document.getElementById("examStatus").value=data.examStatus;
			try{
				document.getElementById("examDate").value=(new Date(data.examDate).getMonth()+1)+"-"+(new Date(data.examDate).getDate())+"-"+(new Date(data.examDate).getFullYear()); 
			}catch(err){}
			try{
				document.getElementById("subjectIdforDSPQ").value=data.subjectMaster.subjectId;
			}catch(err){}
			try{
				document.getElementById("scoreReport").value=data.scoreReport; 
			}catch(err){}
			try{
				document.getElementById("scoreReportHidden").value=data.scoreReport;
			}catch(err){}
			try{
				$("#subjectExamTextarea").find(".jqte_editor").html(data.examNote);
				$('[name="subjectExamTextarea"]').text(data.examNote);
			}catch(err){}
			if(data.scoreReport!=null && data.scoreReport!="")
			{	
				document.getElementById("removeScoreReportSpan").style.display="inline";
				document.getElementById("divScoreReport").style.display="inline";
				//
				document.getElementById("divScoreReport").innerHTML=resourceJSON.msgRecentscorereportonfile+": <a href='javascript:void(0)' id='hrefSubjectArea' onclick=\"downloadSubjectAreaExam('"+data.teacherSubjectAreaExamId+"','hrefSubjectArea');" +
				"if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0');" +
				"return false;\">"+data.scoreReport+"</a>";
				
				//$('#hrefSubjectArea').tooltip();
				
			}else{
				document.getElementById("removeScoreReportSpan").style.display="none";
				document.getElementById("divScoreReport").style.display="none";
				document.getElementById("scoreReport").value="";
			}
		}catch(err){}
		}});
}
function getADValues()
{
	PFCertifications.getADValues({ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
		try{
			document.getElementById("documentName").value=data.documentName;
			
			try{
				document.getElementById("uploadedDocument").value=data.uploadedDocument;
			}catch(err){}
			try{
				document.getElementById("uploadedDocumentHidden").value=data.uploadedDocument;
			}catch(err){}
			
			if(data.uploadedDocument!=null && data.uploadedDocument!="")
			{	
				document.getElementById("removeUploadedDocumentSpan").style.display="inline";
				document.getElementById("divUploadedDocument").style.display="inline";
				document.getElementById("divUploadedDocument").innerHTML="<a href='javascript:void(0)' id='hrefAdditionalDocuments' onclick=\"downloadUploadedDocument('"+data.additionDocumentId+"','hrefAdditionalDocuments');" +
				"if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0');" +
				"return false;\">"+data.uploadedDocument+"</a>";
			}else{
				document.getElementById("divUploadedDocument").style.display="none";
				document.getElementById("removeUploadedDocumentSpan").style.display="none";
				document.getElementById("uploadedDocument").value="";
			}
		}catch(err){}
		}});
}


function chkForAdditionalDocuments(evt)
{	
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==13)
	{
		insertOrUpdate_AdditionalDocuments();
	}	
}
function insertOrUpdate_AdditionalDocuments(sbtsource)
{
	
	if(sbtsource=="0")
	{
		resetSBTNSource();
	}
	
	var threadCount_adddoc=document.getElementById("threadCount_adddoc").value;
	if(threadCount_adddoc=="0")
	{
		document.getElementById("threadCount_adddoc").value=1;
		
		updateThreadCount("adddoc");
		
		var additionDocumentId=document.getElementById("additionDocumentId").value;
		$('#errAdditionalDocuments').empty();
		var cnt=0;
		var documentName = document.getElementById("documentName").value;
		if(trim(documentName)==""){
			$('#errAdditionalDocuments').append("&#149; "+resourceJSON.msgdocumentname+"</BR>");
			$('#documentName').css("background-color",txtBgColor);
			cnt++;
		}
		var uploadedDocument = document.getElementById("uploadedDocument").value;
		var uploadedDocumentHidden = document.getElementById("uploadedDocumentHidden").value;
		if(trim(uploadedDocument)=="" && trim(uploadedDocumentHidden)==""){
			$('#errAdditionalDocuments').append("&#149; "+resourceJSON.msguploaddocument+"</BR>");
			$('#uploadedDocument').css("background-color",txtBgColor);
			cnt++;
		}else{
			if(trim(uploadedDocument)!=""){
				var ext = uploadedDocument.substr(uploadedDocument.lastIndexOf('.') + 1).toLowerCase();	
				
				var fileSize = 0;
				if ($.browser.msie==true)
			 	{	
				    fileSize = 0;	   
				}
				else
				{
					if(document.getElementById("uploadedDocument").files[0]!=undefined)
					{
						fileSize = document.getElementById("uploadedDocument").files[0].size;
					}
				}
				if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt'))
				{
					$('#errAdditionalDocuments').append("&#149; "+resourceJSON.msgacceptabledocformate+"</BR>");
					$('#uploadedDocument').css("background-color",txtBgColor);
					cnt++;
				}else if(fileSize>=10485760){
					$('#errAdditionalDocuments').append("&#149; "+resourceJSON.msgDocSize10mb+"<br>");
					$('#uploadedDocument').css("background-color",txtBgColor);
					cnt++;
				}
			}
		}
		if(cnt!=0)		
		{
			updateReturnThreadCount("adddoc");
			$('#errAdditionalDocuments').show();
			return false;
		}else if(cnt==0){
			if(trim(uploadedDocument)!=""){
				try{
					document.getElementById("additionalDocumentsForm").submit();
				}catch(err){}
			}else{
				PFCertifications.saveOrUpdateAdditionalDocuments(additionDocumentId,documentName,null,uploadedDocumentHidden,
				{ 
				async: false,
				errorHandler:handleError,
				callback: function(data)
					{	
						showGridAdditionalDocuments()		
						resetAdditionalDocumentsForm();
						if(sbtsource=="1")
						{
							saveAllDSPQ();
							updateReturnThreadCount("adddoc");
							validatePortfolioErrorMessageAndGridData('level2');
						}
						
					}
				});
			}
		}
		
	}
	
	
	
	
}

function saveAdditionalDocuments(fileName,sbtsource)
{
	//alert("fileName "+fileName+" sbtsource "+sbtsource);
	var documentName = document.getElementById("documentName").value;
	var uploadedDocumentHidden = document.getElementById("uploadedDocumentHidden").value;
	var additionDocumentId=document.getElementById("additionDocumentId").value;
	
	PFCertifications.saveOrUpdateAdditionalDocuments(additionDocumentId,documentName,fileName,uploadedDocumentHidden,
	{ 
	async: false,
	errorHandler:handleError,
	callback: function(data)
		{	
			showGridAdditionalDocuments()		
			resetAdditionalDocumentsForm();
			if(sbtsource==1)
			{
				saveAllDSPQ();
				updateReturnThreadCount("adddoc");
				validatePortfolioErrorMessageAndGridData('level2');
			}
		}
	});
	
}
function showAdditionalDocumentsForm()
{
	document.getElementById("additionDocumentId").value="";
	document.getElementById("divAdditionalDocumentsRow").style.display="block";
	document.getElementById("divDocumnetDone").style.display="block";
	document.getElementById("documentName").value="";
	document.getElementById("uploadedDocument").value="";
	document.getElementById("uploadedDocumentHidden").value="";
	document.getElementById("divUploadedDocument").style.display="none";
	document.getElementById("removeUploadedDocumentSpan").style.display="none";
	document.getElementById("uploadedDocument").value="";
	document.getElementById("documentName").focus();
	$('#documentName').css("background-color","");
	$('#uploadedDocument').css("background-color","");
	$('#errAdditionalDocuments').empty();
	return false;
}
function resetAdditionalDocumentsForm()
{
	document.getElementById("divAdditionalDocumentsRow").style.display="none";
	document.getElementById("divDocumnetDone").style.display="none";
	document.getElementById("additionDocumentId").value="";
	document.getElementById("documentName").value="";
	document.getElementById("uploadedDocument").value="";
	document.getElementById("uploadedDocumentHidden").value="";
	document.getElementById("divUploadedDocument").style.display="none";
	document.getElementById("removeUploadedDocumentSpan").style.display="none";
	document.getElementById("additionalDocumentsForm").reset();
	return false;
}

function showGridAdditionalDocuments()
{
	PFCertifications.getAdditionalDocumentsGrid(dp_AdditionalDocuments_noOfRows,dp_AdditionalDocuments_page,dp_AdditionalDocuments_sortOrderStr,dp_AdditionalDocuments_sortOrderType,{  
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#divGridAdditionalDocuments').html(data);
			applyScrollOnTbl_AdditionalDocuments();
		}});
}

function delRow_Document(id)
{	
	document.getElementById("additionDocumentId").value=id;
	$('#removeUploadedDocument').modal('show');
}

function removeUploadedDocument(){
	
	var additionDocumentId=document.getElementById("additionDocumentId").value;
	PFCertifications.removeUploadedDocument(additionDocumentId,{
	async: true,
	errorHandler:handleError,
	callback:function(data)
	{
		$('#removeUploadedDocument').modal('hide');
		showGridAdditionalDocuments();
		resetAdditionalDocumentsForm();
		return false;
	}});
}

function showEditTeacherAdditionalDocuments(additionDocumentId)
{
	$('#errAdditionalDocuments').empty();
	$('#documentName').css("background-color","");
	$('#uploadedDocument').css("background-color","");
	
	document.getElementById("additionalDocumentsForm").reset();	
	PFCertifications.showEditTeacherAdditionalDocuments(additionDocumentId,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
	{
		document.getElementById("divAdditionalDocumentsRow").style.display="block";
		document.getElementById("divDocumnetDone").style.display="block";
		
		document.getElementById("additionDocumentId").value=data.additionDocumentId;
		document.getElementById("documentName").value=data.documentName;
		/*try{
			document.getElementById("uploadedDocument").value=data.uploadedDocument;
		}catch(err){}*/
		try{
			document.getElementById("uploadedDocumentHidden").value=data.uploadedDocument;
		}catch(err){}
		
		
		//alert(data.pathOfTranscript)
		if(data.uploadedDocument!=null && data.uploadedDocument!="")
		{	
			document.getElementById("removeUploadedDocumentSpan").style.display="inline";
			document.getElementById("divUploadedDocument").style.display="inline";
			document.getElementById("divUploadedDocument").innerHTML="<a href='javascript:void(0)' id='hrefAdditionalDocuments' onclick=\"downloadUploadedDocument('"+data.additionDocumentId+"','hrefAdditionalDocuments');" +
			"if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0');" +
			"return false;\">"+data.uploadedDocument+"</a>";
		}
		else
		{
			document.getElementById("divUploadedDocument").style.display="none";
			document.getElementById("removeUploadedDocumentSpan").style.display="none";
			document.getElementById("uploadedDocument").value="";
		}
		document.getElementById("documentName").focus();
		
	}})
	return false;
}


function showGridSubjectAreasGrid()
{
	PFCertifications.getSubjectAreasGrid(dp_SubjectArea_noOfRows,dp_SubjectArea_page,dp_SubjectArea_sortOrderStr,dp_SubjectArea_sortOrderType,{  
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#divGridSubjectAreas').html(data);
			applyScrollOnTbl_SubjectAreas();
		}});
}
function showSubjectAreaForm()
{
	document.getElementById("divSubjectAreaInput").style.display="block";
	document.getElementById("examStatus").focus();
	
	$('#examStatus').css("background-color","");
	$('#examDate').css("background-color","");
	$('#subjectIdforDSPQ').css("background-color","");
	$('#scoreReport').css("background-color","");
	resetSubjectAreaForm();
	
	
	document.getElementById("removeScoreReportSpan").style.display="none";
	document.getElementById("divScoreReport").style.display="none";
	document.getElementById("scoreReport").value="";
	$("#subjectExamTextarea").find(".jqte_editor").html("");	
	$('[name="subjectExamTextarea"]').text("");
	
	$('#errSubjectArea').empty();
	return false;
}
function resetSubjectAreaForm()
{
	document.getElementById("examStatus").value="0";
	document.getElementById("examDate").value="";
	document.getElementById("subjectIdforDSPQ").value="0";
	document.getElementById("scoreReport").value="";
	document.getElementById("scoreReportHidden").value="";
	
	document.getElementById("teacherSubjectAreaExamId").value=0;
	//document.getElementById("additionalDocumentsForm").reset();
	return false;
}

function cancelSubjectAreaForm()
{
	resetSubjectAreaForm();
	document.getElementById("divSubjectAreaInput").style.display="none";
	closeDSPQCal();
	return false;
}

function delRow_subjectArea(id)
{	
	document.getElementById("teacherSubjectAreaExamId").value=id;
	$('#deleteSubjectArea').modal('show');
}

function deleteSubjectAreaConfirm()
{
	$('#deleteSubjectArea').modal('hide');
	var teacherSubjectAreaExamId=document.getElementById("teacherSubjectAreaExamId").value;
	PFCertifications.deleteSubjectArea(teacherSubjectAreaExamId,
	{ 
		async: false,
		errorHandler:handleError,
		callback: function(data)
		{	
			showGridSubjectAreasGrid();
		}
	});
}

function showEditSubject(id)
{
	document.getElementById("teacherSubjectAreaExamId").value=id;
	var teacherSubjectAreaExamId=document.getElementById("teacherSubjectAreaExamId").value;
	PFCertifications.showEditSubject(teacherSubjectAreaExamId,
	{ 
		async: false,
		errorHandler:handleError,
		callback: function(data)
		{
		showSubjectAreaForm();
		
		try{
			
			document.getElementById("teacherSubjectAreaExamId").value=data.teacherSubjectAreaExamId;
			
			document.getElementById("examStatus").value=data.examStatus;
			try{
				document.getElementById("examDate").value=(new Date(data.examDate).getMonth()+1)+"-"+(new Date(data.examDate).getDate())+"-"+(new Date(data.examDate).getFullYear()); 
			}catch(err){}
			try{
				document.getElementById("subjectIdforDSPQ").value=data.subjectAreaExamMaster.subjectAreaExamId;
			}catch(err){}
			try{
				document.getElementById("scoreReport").value=data.scoreReport; 
			}catch(err){}
			try{
				document.getElementById("scoreReportHidden").value=data.scoreReport;
			}catch(err){}
			try{
				$("#subjectExamTextarea").find(".jqte_editor").html(data.examNote);
				$('[name="subjectExamTextarea"]').text(data.examNote);
			}catch(err){}
			
			if(data.scoreReport!=null && data.scoreReport!="")
			{	
				document.getElementById("removeScoreReportSpan").style.display="inline";
				document.getElementById("divScoreReport").style.display="inline";
				document.getElementById("divScoreReport").innerHTML=resourceJSON.msgRecentscorereportonfile+": <a href='javascript:void(0)' id='hrefSubjectArea' onclick=\"downloadSubjectAreaExam('"+data.teacherSubjectAreaExamId+"','hrefSubjectArea');" +
				"if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0');" +
				"return false;\">"+data.scoreReport+"</a>";
				
				//$('#hrefSubjectArea').tooltip();
				
			}else{
				document.getElementById("removeScoreReportSpan").style.display="none";
				document.getElementById("divScoreReport").style.display="none";
				document.getElementById("scoreReport").value="";
			}
		}catch(err){}
		}
	});
}


function saveSubjectAreas(sbtsource)
{
	
	if(sbtsource=="0")
	{
		resetSBTNSource();
	}
	
	var threadCount_subarea=document.getElementById("threadCount_subarea").value;
	if(threadCount_subarea=="0")
	{
		document.getElementById("threadCount_subarea").value=1;
		
		updateThreadCount("subarea");
		
		$('#errSubjectArea').empty();
		var errorCount_subArea=0;
		var focs=0;
		
		var teacherSubjectAreaExamId=document.getElementById("teacherSubjectAreaExamId").value;
		var examStatus = document.getElementById("examStatus").value;
		var examDate = document.getElementById("examDate").value;
		var subjectIdforDSPQ = document.getElementById("subjectIdforDSPQ").value;
		var scoreReport = document.getElementById("scoreReport").value;
		var scoreReportHidden = document.getElementById("scoreReportHidden").value;
		var subjectExamNote = 	$("#subjectExamTextarea").find(".jqte_editor").html();
		
			if(trim(examStatus)=="0"){
				$('#errSubjectArea').append("&#149; "+resourceJSON.msgExamStatus+"</BR>");
				$('#examStatus').css("background-color",txtBgColor);
				errorCount_subArea++;focs++;
			}
			else
			{
				$('#examStatus').css("background-color","");
			}
			
			if(trim(examDate)==""){
				$('#errSubjectArea').append("&#149; "+resourceJSON.msgExamDate+"</BR>");
				$('#examDate').css("background-color",txtBgColor);
				errorCount_subArea++;focs++;
				
			}
			else
			{
				$('#examDate').css("background-color","");
			}
			
			if(trim(subjectIdforDSPQ)=="0"){
				$('#errSubjectArea').append("&#149; "+resourceJSON.msgSubject+"</BR>");
				$('#subjectIdforDSPQ').css("background-color",txtBgColor);
				errorCount_subArea++;focs++;
			}
			else
			{
				$('#subjectIdforDSPQ').css("background-color","");
			}
			
			if(trim(scoreReport)=="" && trim(scoreReportHidden)==""){
				$('#errSubjectArea').append("&#149; "+resourceJSON.msgUploadScoreReport+"</BR>");
				$('#scoreReport').css("background-color",txtBgColor);
				errorCount_subArea++;focs++;
			}else{
				$('#scoreReport').css("background-color","");
				if(trim(scoreReport)!=""){
					var ext = scoreReport.substr(scoreReport.lastIndexOf('.') + 1).toLowerCase();	
					
					var fileSize = 0;
					if ($.browser.msie==true)
				 	{	
					    fileSize = 0;	   
					}
					else
					{
						if(document.getElementById("scoreReport").files[0]!=undefined)
						{
							fileSize = document.getElementById("scoreReport").files[0].size;
						}
					}
					
					if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt'))
					{
						$('#errSubjectArea').append("&#149; "+resourceJSON.msgAcceptableScoreReportformats+"</BR>");
						$('#scoreReport').css("background-color",txtBgColor);
						errorCount_subArea++;focs++;
					}else if(fileSize>=10485760){
						$('#errSubjectArea').append("&#149; "+resourceJSON.msgScoreReportFilesize+"<br>");
						$('#scoreReport').css("background-color",txtBgColor);
						errorCount_subArea++;focs++;
					}
					else
					{
						$('#scoreReport').css("background-color","");
					}
				}
			}
			if(errorCount_subArea!=0)		
			{
				updateReturnThreadCount("subarea");
				$('#errSubjectArea').show();
				return false;
			}
			else
			{
				if(scoreReport!="")
				{	
					document.getElementById("frmSubjectArea").submit();
				}
				else
				{
					PFCertifications.InserOrUpdateSubject(teacherSubjectAreaExamId,examStatus,examDate,subjectIdforDSPQ,scoreReport,subjectExamNote,
					{ 
						async: false,
						errorHandler:handleError,
						callback: function(data)
						{	
							cancelSubjectAreaForm();
							showGridSubjectAreasGrid();
							if(sbtsource==1)
							{
								saveAllDSPQ();
								updateReturnThreadCount("subarea");
								validatePortfolioErrorMessageAndGridData('level2');
							}
						}
					});
				}
			}
			
	}
	
	
	
		
}

function saveSubjectAreasByServlet(sbtsource,scoreReport)
{
	var teacherSubjectAreaExamId=document.getElementById("teacherSubjectAreaExamId").value;
	var examStatus = document.getElementById("examStatus").value;
	var examDate = document.getElementById("examDate").value;
	var subjectIdforDSPQ = document.getElementById("subjectIdforDSPQ").value;
	var subjectExamNote = 	$("#subjectExamTextarea").find(".jqte_editor").html();
	//var scoreReport = document.getElementById("scoreReport").value;
	//var scoreReportHidden = document.getElementById("scoreReportHidden").value;
		
	PFCertifications.InserOrUpdateSubject(teacherSubjectAreaExamId,examStatus,examDate,subjectIdforDSPQ,scoreReport,subjectExamNote,
	{ 
		async: false,
		errorHandler:handleError,
		callback: function(data)
		{	
			cancelSubjectAreaForm();
			showGridSubjectAreasGrid();
			
			if(sbtsource==1)
			{
				saveAllDSPQ();
				updateReturnThreadCount("subarea");
				validatePortfolioErrorMessageAndGridData('level2');
			}
			
		}
	});
			
}

function clearCertification(){
	document.getElementById("pathOfCertificationFile").value=null;
	document.getElementById("pathOfCertificationFile").reset();
	$('#pathOfCertificationFile').css("background-color","");
	}
function fieldsDisable(value){
	
	if(value==5){
		$('#errordiv_Certification').empty();
		$( "#certificationDiv" ).find( "span.required" ).css( "color", "white" );
		setDefColortoErrorMsg_Certification();
		document.getElementById("certificationtypeMaster").value=0;
		document.getElementById("stateMaster").value="";
		document.getElementById("yearReceived").value="";
		document.getElementById("yearexpires").value="";
		document.getElementById("certType").value="";
		document.getElementById("doenumber").value="";
		document.getElementById("certUrl").value="";
		document.getElementById("pathOfCertificationFile").value="";
		document.getElementById("pkOffered").checked=false;
		document.getElementById("kgOffered").checked=false;
		document.getElementById("g01Offered").checked=false;
		document.getElementById("g02Offered").checked=false;
		document.getElementById("g03Offered").checked=false;
		document.getElementById("g04Offered").checked=false;
		document.getElementById("g05Offered").checked=false;
		document.getElementById("g06Offered").checked=false;
		document.getElementById("g07Offered").checked=false;
		document.getElementById("g08Offered").checked=false;
		document.getElementById("g09Offered").checked=false;
		document.getElementById("g10Offered").checked=false;
		document.getElementById("g11Offered").checked=false;
		document.getElementById("g12Offered").checked=false;
		document.getElementById("clearLink").style.display="none";
		getPraxis();
		
		document.getElementById("certificationtypeMaster").disabled=true;
		document.getElementById("stateMaster").disabled=true;		
		document.getElementById("yearReceived").disabled=true;
		document.getElementById("yearexpires").disabled=true;
		document.getElementById("certType").disabled=true;
		document.getElementById("doenumber").disabled=true;
		document.getElementById("certUrl").disabled=true;
		document.getElementById("pathOfCertificationFile").disabled=true;
		document.getElementById("pkOffered").disabled=true;
		document.getElementById("kgOffered").disabled=true;
		document.getElementById("g01Offered").disabled=true;
		document.getElementById("g02Offered").disabled=true;
		document.getElementById("g03Offered").disabled=true;
		document.getElementById("g04Offered").disabled=true;
		document.getElementById("g05Offered").disabled=true;
		document.getElementById("g06Offered").disabled=true;
		document.getElementById("g07Offered").disabled=true;
		document.getElementById("g08Offered").disabled=true;
		document.getElementById("g09Offered").disabled=true;
		document.getElementById("g10Offered").disabled=true;
		document.getElementById("g11Offered").disabled=true;
		document.getElementById("g12Offered").disabled=true;
		document.getElementById("clearLink").style.display="none";
		
	}else{
		$( "#certificationDiv" ).find( "span.required" ).css( "color", "red" );
		document.getElementById("clearLink").style.display="block";
		document.getElementById("certificationtypeMaster").disabled=false;
		document.getElementById("stateMaster").disabled=false;		
		document.getElementById("yearReceived").disabled=false;
		document.getElementById("yearexpires").disabled=false;
		document.getElementById("certType").disabled=false;
		document.getElementById("doenumber").disabled=false;
		document.getElementById("certUrl").disabled=false;
		document.getElementById("pathOfCertificationFile").disabled=false;
		document.getElementById("pkOffered").disabled=false;
		document.getElementById("kgOffered").disabled=false;
		document.getElementById("g01Offered").disabled=false;
		document.getElementById("g02Offered").disabled=false;
		document.getElementById("g03Offered").disabled=false;
		document.getElementById("g04Offered").disabled=false;
		document.getElementById("g05Offered").disabled=false;
		document.getElementById("g06Offered").disabled=false;
		document.getElementById("g07Offered").disabled=false;
		document.getElementById("g08Offered").disabled=false;
		document.getElementById("g09Offered").disabled=false;
		document.getElementById("g10Offered").disabled=false;
		document.getElementById("g11Offered").disabled=false;
		document.getElementById("g12Offered").disabled=false;
	}	
}
var totalQuestionsList=0;
function showDistrictSpecificPortfolioQuestions()
{	var jobId=document.getElementById("jobId").value;
	PFCertifications.getDistrictSpecificPortfolioQuestions(jobId,{  
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			var dataArray = data.split("@##@");
			totalQuestionsList = dataArray[1];
			var textmsg = dataArray[2];
			if(dataArray[0]=="")
				$('#QuestionDiv').hide();
			else
				$('#QuestionDiv').show();
			$('#divGridDistrictSpecificQuestions').html(dataArray[0]);
		}});
}
function getVideoPlay(videoLinkId)
{
	$('#videovDiv').modal('show');
	$('#loadingDivWaitVideo').show();
	$('#interVideoDiv').empty();
	var videoPath='';		
	PFCertifications.getVideoPlayDiv(videoLinkId, { 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
		$('#loadingDivWaitVideo').hide();
		if(data!=""&& data!=null)
		{			
			var videoPath="";				
			videoPath="<video poster='images/video_back.png' src="+data+" width='480' height='280' controls autoplay><source src="+data+" type='video/ogg'>"+resourceJSON.msgBrowservideotag+"<br>"+resourceJSON.msgPlsDownload+" <a target='_blank' href='https://www.apple.com/in/quicktime/download/'>"+resourceJSON.msgQuickTimePlayer+"</a><br>"+resourceJSON.msgRestartSafaribrowser+" </video>";
			$('#interVideoDiv').append(videoPath);			
		}
		else
		{
			$('#interVideoDiv').css("color","Red");
			$('#interVideoDiv').append(resourceJSON.msgProblemoccured);
		}
		}});
}
function saveVideo(videoFileName)
{	
	var videourl = trim(document.getElementById("videourl").value);
	
	var teacherVideoLink = {videolinkAutoId:null,videourl:null, video:null, createdDate:null};
	dwr.engine.beginBatch();
	dwr.util.getValues(teacherVideoLink);
	teacherVideoLink.videourl=videourl;	
	teacherVideoLink.video=	videoFileName;		
	PFCertifications.saveOrUpdateVideoLinks(teacherVideoLink,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data)
		{	
			$('#loadingDiv').hide();				
			hideVideoLinksForm();
			getVideoLinksGrid();
		}
	});
	dwr.engine.endBatch();
	return true;
}
//amit
function showAndHideCanContOnOffer(from)
{
	var districtIdForDSPQ="";	
	if ($('#districtIdForDSPQ').length > 0) {
		districtIdForDSPQ=document.getElementById("districtIdForDSPQ").value;
	}
	if(districtIdForDSPQ==804800){
		var rdcontacted0	=   document.getElementById("rdcontacted0");
		var rdcontacted1	=   document.getElementById("rdcontacted1");
		
		if (rdcontacted0.checked) {
			document.getElementById("canContOnOfferDiv").style.display="block";
			if(from=="1")
				$('#canContOnOffer0').prop('checked', true);
		}
		else if (rdcontacted1.checked) {
			document.getElementById("canContOnOfferDiv").style.display="none";
		}
	}
}

