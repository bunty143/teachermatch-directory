var count=0;
var index=-1;
var length=0;
var divid='';
var txtid='';
var hiddenDataArray = new Array();
var showDataArray = new Array();
var degreeTypeArray = new Array();
function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert(resourceJSON.msgServerErr+": "+exception.javaClassName);}
}
function GetXMLHttp()
{
	var temp=null;
	if (window.ActiveXObject) 
		temp = new ActiveXObject("Microsoft.XMLHTTP");
	else if (window.XMLHttpRequest) 
		temp = new XMLHttpRequest();
	else
		alert(resourceJSON.msgAjaxNotSupported);
	return temp;   		
}

var autoHttpxmlRequest='';

//=========== In Case of final =====================
//For State Text Box
var panNoArray = new Array(resourceJSON.msgPANNOTAVBL,resourceJSON.msgPANAPPLIED,resourceJSON.msgPANNOTREQD);
function getPANAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} else if(event.keyCode==38){
		upArrowKey(txtdivid);
	} else if(event.keyCode==13){
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		searchArray = panNoArray;
		fatchData(txtSearch,searchArray,txtId,txtdivid);
	} 
	else if(event.keyCode==9){

	}else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = panNoArray;
		fatchData(txtSearch,searchArray,txtId,txtdivid);
	}
}



var hiddenId="";
function getDegreeMasterAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	$('#degreeName').mousedown(function(e) {
	if( (event.keyCode == 1) ){
        alert(resourceJSON.msgleftbutton);
    }if( (event.keyCode== 3) ) {
        alert(resourceJSON.msgrightbutton);
    }else if( (event.keyCode== 2) ) {
        alert(resourceJSON.msgmiddlebutton); 
    }
	});
	var gedflag=false;
	if($('#degreeName').val()==resourceJSON.msgHighSchoolorGED){
		gedflag=true;
	}
	/*if(document.getElementById("districtIdForDSPQ").value==7800031){
			GEDFlag=false;		
		}*/	
	if($('#degreeName').val()==resourceJSON.msgNoDegree){
		$("#universityName").prop('readonly', true);
		$("#fieldName").prop('readonly', true);
		$("#attendedInYear").prop('disabled', true);
		$("#leftInYear").prop('disabled', true);
		$("#pathOfTranscriptFile").prop('disabled', true);
		$("#gpaCumulative1").prop('readonly', true);
		
		$("#universityName").val("");
		$("#fieldName").val("");
		$("#attendedInYear").val("");
		$("#leftInYear").val("");
		if(degreeId.value=="46"){$('#showhide1').hide();$('#showhide2').hide();$('#showhide3').hide();$('#showhide4').hide();}
		else{
			$('#showhide1').show();$('#showhide2').show();$('#showhide3').show();$('#showhide4').show();
		}
		$("#pathOfTranscriptFile").val("");
		$("#gpaCumulative1").val("");
	}else{
		if(degreeId.value=="46"){$('#showhide1').hide();$('#showhide2').hide();$('#showhide3').hide();$('#showhide4').hide();}
		else{
			$('#showhide1').show();$('#showhide2').show();$('#showhide3').show();$('#showhide4').show();
		}
		$("#universityName").prop('readonly', false);
		$("#fieldName").prop('readonly', false);
		$("#attendedInYear").prop('disabled', false);
		$("#leftInYear").prop('disabled', false);
		$("#pathOfTranscriptFile").prop('disabled', false);
		$("#gpaCumulative1").prop('readonly', false);
	}
		if($('#degreeName').val()==resourceJSON.msgNoDegree || gedflag==true){
			$('.nondgrReq').hide();
			
			if(gedflag==true){
				if(document.getElementById("districtIdForDSPQ").value!=806900){
					$('.nondgrReq1').show();
				}
				$('.nondgrReq2').show();
			}else{
				$('.nondgrReq1').hide();
				$('.nondgrReq2').hide();
			}
		}else{
			if($("#degreeOptional").val()=="true")
			{
				$('.nondgrReq').show();
			}else
			{
				$('.nondgrReq').hide();
			}
			if($("#academicsDatesOptional").val()=="true")
			{
				$('.nondgrReq1').show();
			}else
			{
				$('.nondgrReq1').hide();
			}			
			$('.nondgrReq2').hide();
		}	
		
		
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		//searchArray = getDegreeMasterArray(txtSearch.value);
		//fatchData(txtSearch,searchArray,txtId,txtdivid);
		document.getElementById("universityName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getDegreeMasterArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
	
	if($('#degreeName').val()==resourceJSON.msgNoDegree){
		$("#universityName").prop('readonly', true);
		$("#fieldName").prop('readonly', true);
		$("#attendedInYear").prop('disabled', true);
		$("#leftInYear").prop('disabled', true);
		$("#pathOfTranscriptFile").prop('disabled', true);
		$("#gpaCumulative1").prop('readonly', true);
		
		$("#universityName").val("");
		$("#fieldName").val("");
		if(degreeId.value=="46"){$('#showhide1').hide();$('#showhide2').hide();$('#showhide3').hide();$('#showhide4').hide();}
		else{
			$('#showhide1').show();$('#showhide2').show();$('#showhide3').show();$('#showhide4').show();
		}
		$("#attendedInYear").val("");
		$("#leftInYear").val("");
		$("#pathOfTranscriptFile").val("");
		$("#gpaCumulative1").val("");
	}else{
		$("#universityName").prop('readonly', false);
		$("#fieldName").prop('readonly', false);
		if(degreeId.value=="46"){$('#showhide1').hide();$('#showhide2').hide();$('#showhide3').hide();$('#showhide4').hide();}
		else{
			$('#showhide1').show();$('#showhide2').show();$('#showhide3').show();$('#showhide4').show();
		}
		$("#attendedInYear").prop('disabled', false);
		$("#leftInYear").prop('disabled', false);
		$("#pathOfTranscriptFile").prop('disabled', false);
		$("#gpaCumulative1").prop('readonly', false);
	}
		
		if($('#degreeName').val()==resourceJSON.msgNoDegree || $('#degreeName').val()==resourceJSON.msgHighSchoolorGED){
			$('.nondgrReq').hide();
			if($('#degreeName').val()==resourceJSON.msgHighSchoolorGED){
				$('.nondgrReq1').show();
				if(document.getElementById("districtIdForDSPQ").value==806900){
					$('.nondgrReq1').hide();
				}
				$('.nondgrReq2').show();
			}else{
				$('.nondgrReq1').hide();
				$('.nondgrReq2').hide();
			}
		}else{			
			if($("#degreeOptional").val()=="true")
			{
				$('.nondgrReq').show();
			}else
			{
				$('.nondgrReq').hide();
			}
			if($("#academicsDatesOptional").val()=="true")
			{
				$('.nondgrReq1').show();
			}else
			{
				$('.nondgrReq1').hide();
			}			
			$('.nondgrReq2').hide();
		}
	
}

function getDegreeMasterArray(degreeName){
	
	var searchArray = new Array();
	DWRAutoComplete.getDegreeMasterList(degreeName,{ 
		async: false,		
		callback: function(data){
		hiddenDataArray = new Array();
		showDataArray = new Array();
		for(i=0;i<data.length;i++){
			searchArray[i]=data[i].degreeName;
			hiddenDataArray[i]=data[i].degreeId;
			showDataArray[i]=data[i].degreeName;
			degreeTypeArray[i]=data[i].degreeType;
		}
	}
	});	

	return searchArray;
}



function getUniversityAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38)
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13)
	{
		
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		//searchArray = getUniversityArray(txtSearch.value);
		//fatchData(txtSearch,searchArray,txtId,txtdivid);
		document.getElementById("fieldName").focus();
		
	} 
	else if(event.keyCode==9)
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getUniversityArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getUniversityArray(universityName){
	
	var searchArray = new Array();
	DWRAutoComplete.getUniversityMasterList(universityName,{ 
		async: false,		
		callback: function(data){
		hiddenDataArray = new Array();
		showDataArray = new Array();
		for(i=0;i<data.length;i++){
			searchArray[i]=data[i].universityName;
			hiddenDataArray[i]=data[i].universityId;
			showDataArray[i]=data[i].universityName;
		}
	}
	});	

	return searchArray;
}

function getFieldOfStudyAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38)
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13)
	{
		
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		//searchArray = getFieldOfStudyArray(txtSearch.value);
		//fatchData(txtSearch,searchArray,txtId,txtdivid);
		document.getElementById("attendedInYear").focus();
		
	} 
	else if(event.keyCode==9)
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getFieldOfStudyArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getFieldOfStudyArray(fieldName){
	
	var searchArray = new Array();
	DWRAutoComplete.getFieldOfStudyList(fieldName,{ 
		async: false,		
		callback: function(data){
		hiddenDataArray = new Array();
		showDataArray = new Array();
		for(i=0;i<data.length;i++){
			searchArray[i]=data[i].fieldName;
			hiddenDataArray[i]=data[i].fieldId;
			showDataArray[i]=data[i].fieldName;
		}
	}
	});	

	return searchArray;
}



function hideDegreeMasterDiv(dis,hiddenId,divId)
{
	//setDefColortoErrorMsg_Academic();
	//$('#errordiv').empty();
	document.getElementById("degreeType").value="";
	document.getElementById("divGPARow").style.display="none";
	var degreeId = document.getElementById("degreeId");
	//alert(hiddenDataArray);
	//alert(degreeTypeArray);
	//alert(index)
	//alert(degreeTypeArray[index]);
	//alert(hiddenDataArray[index])
	if(parseInt(length)>0)
	{
		if(index==-1)
		{
			index=0;
		}
		//alert(hiddenId);
		//alert(hiddenDataArray[index]);
		//if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId))
		//{
			//alert("Enter in ...")
			document.getElementById(hiddenId).value=hiddenDataArray[index];			
			var degreeName = document.getElementById("degreeName").value;
			var degreeType = document.getElementById("degreeType").value=degreeTypeArray[index];
			if(degreeId.value=="46"){$('#showhide1').hide();$('#showhide2').hide();$('#showhide3').hide();$('#showhide4').hide();}
			else{
				$('#showhide1').show();$('#showhide2').show();$('#showhide3').show();$('#showhide4').show();
			}
			if(document.getElementById("districtIdForDSPQ").value!=3904493 && document.getElementById("districtIdForDSPQ").value!=7800049 && document.getElementById("districtIdForDSPQ").value!=7800048 && document.getElementById("districtIdForDSPQ").value!=7800050 && document.getElementById("districtIdForDSPQ").value!=7800051 && document.getElementById("districtIdForDSPQ").value!=7800053){
			if(trim(degreeName)!="" && degreeType=="B")
			{
				document.getElementById("divGPARow").style.display="block";
				document.getElementById("divGPARowOther").style.display="none";
				
			}
			else
			{
				document.getElementById("divGPARow").style.display="none";
				document.getElementById("divGPARowOther").style.display="block";
			}
			}
			//alert("degreeType"+degreeType)
			
			
		//}
		
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
			document.getElementById("degreeType").value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			
		}
		
	}
	else
	{
		if(document.getElementById(hiddenId))
		{
			document.getElementById(hiddenId).value="";
		}
		//dis.value="";
		if(dis.value!="")
		{
			var cnt=0;
			var focs=0;	
			$('#errordiv_AcademicForm').empty();	
			setDefColortoErrorMsg_Academic();
			$('#errordiv_AcademicForm').append("&#149; "+resourceJSON.PlzEtrVldDegree+"<br>");
			if(focs==0)
				$('#degreeName').focus();
			
			$('#degreeName').css("background-color",txtBgColor);
			cnt++;focs++;	
		}
			
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	
	if($('#degreeName').val()=="No Degree"){
		$("#universityName").prop('readonly', true);
		$("#fieldName").prop('readonly', true);
		$("#attendedInYear").prop('disabled', true);
		$("#leftInYear").prop('disabled', true);
		$("#pathOfTranscriptFile").prop('disabled', true);
		$("#gpaCumulative1").prop('readonly', true);
		
		$("#universityName").val("");
		$("#fieldName").val("");
		if(degreeId.value=="46"){$('#showhide1').hide();$('#showhide2').hide();$('#showhide3').hide();$('#showhide4').hide();}
		else{
			$('#showhide1').show();$('#showhide2').show();$('#showhide3').show();$('#showhide4').show();
		
		$("#attendedInYear").prop('disabled', false);
		$("#leftInYear").prop('disabled', false);
		}
		$("#attendedInYear").val("");
		$("#leftInYear").val("");
		$("#pathOfTranscriptFile").val("");
		$("#gpaCumulative1").val("");
	}else{
		$("#universityName").prop('readonly', false);
		$("#fieldName").prop('readonly', false);
		if(degreeId.value=="46"){$('#showhide1').hide();$('#showhide2').hide();$('#showhide3').hide();$('#showhide4').hide();}
		else{
			$('#showhide1').show();$('#showhide2').show();$('#showhide3').show();$('#showhide4').show();
		
		$("#attendedInYear").prop('disabled', false);
		$("#leftInYear").prop('disabled', false);
		}
		$("#pathOfTranscriptFile").prop('disabled', false);
		$("#gpaCumulative1").prop('readonly', false);
	}
		if($('#degreeName').val()==resourceJSON.msgNoDegree || $('#degreeName').val()==resourceJSON.msgHighSchoolorGED){
			$('.nondgrReq').hide();
			if($('#degreeName').val()==resourceJSON.msgHighSchoolorGED){
				$('.nondgrReq1').show();
				if(document.getElementById("districtIdForDSPQ").value==806900){
					$('.nondgrReq1').hide();
				}
				$('.nondgrReq2').show();
			}else{
				$('.nondgrReq1').hide();
				$('.nondgrReq2').hide();
			}
		}else{
			
			if($("#degreeOptional").val()=="true")
			{
				$('.nondgrReq').show();
			}else
			{
				$('.nondgrReq').hide();
			}
			if($("#academicsDatesOptional").val()=="true")
			{
				$('.nondgrReq1').show();
			}else
			{
				$('.nondgrReq1').hide();
			}			
			$('.nondgrReq2').hide();
		}
	
	index = -1;
	length = 0;
}
function hideUniversityDiv(dis,hiddenId,divId)
{
	//setDefColortoErrorMsg_Academic();
	//$('#errordiv').empty();	
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		//alert(hiddenId);
		//alert(hiddenDataArray[index]);
		//if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		//}
		
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
		//dis.value="";
		if(dis.value!="")
		{
			var cnt=0;
			var focs=0;	
			$('#errordiv_AcademicForm').empty();	
			setDefColortoErrorMsg_Academic();
			$('#errordiv_AcademicForm').append("&#149; "+resourceJSON.msgvalidschool+"<br>");
			if(focs==0)
				$('#universityName').focus();
			
			$('#universityName').css("background-color",txtBgColor);
			cnt++;focs++;	
		}
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}

function hideFeildOfStudyDiv(dis,hiddenId,divId)
{
	//setDefColortoErrorMsg_Academic();
	//$('#errordiv').empty();
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		//alert(hiddenId);
		//alert(hiddenDataArray[index]);
		//if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		//}
		
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
		//dis.value="";
		if(dis.value!="")
		{
			var cnt=0;
			var focs=0;	
			$('#errordiv_AcademicForm').empty();	
			setDefColortoErrorMsg_Academic();
			$('#errordiv_AcademicForm').append("&#149; "+resourceJSON.msgvalidstudy+"<br>");
			if(focs==0)
				$('#fieldName').focus();
			
			$('#fieldName').css("background-color",txtBgColor);
			cnt++;focs++;	
		}
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}





function hideDiv(dis,hiddenId,divId)
{
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		//alert(hiddenId);
		//alert(hiddenDataArray[index]);
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
		dis.value="";
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}





function hideDiv(dis,hiddenId,divId)
{
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		//alert(hiddenId);
		//alert(hiddenDataArray[index]);
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
		dis.value="";
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}

function hideDivStatic(dis,hiddenId,divId)
{
	index = -1;
	length = 0;
	if(document.getElementById(divId))
	{
		index=-1;
		document.getElementById(divId).style.display="none";
		//if(dis && dis.value!="" && selectFirst!="")
			//dis.value=selectFirst;
		
		selectFirst="";
	}
}

var selectFirst="";

var fatchData= function(txtSearch,searchArray,txtId,txtdivid){
	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i] + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
				
			}
			
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}

function getUniversityAutoCompQuestion(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38)
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13)
	{
		
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		//searchArray = getUniversityArray(txtSearch.value);
		//fatchData(txtSearch,searchArray,txtId,txtdivid);		
		
	} 
	else if(event.keyCode==9)
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getUniversityArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}


var downArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index<length-1){
			for(var i=0;i<10;i++){
				{
					if(document.getElementById('divResult'+txtdivid+i))
						var div_id=document.getElementById('divResult'+txtdivid+i);
					if(div_id)
					{
						if(div_id.className=='over')
							index=div_id.id.split('divResult'+txtdivid)[1];
						div_id.className='normal';
					}
				}
			}
			index++;
			if(document.getElementById('divResult'+txtdivid+index))
			{
				var div_id=document.getElementById('divResult'+txtdivid+index);
				div_id.className='over';
				document.getElementById(txtid).value = div_id.innerHTML;
				selectFirst=div_id.innerHTML;
			}
		}
	}
}

var upArrowKey = function(txtdivid){
	
	if(txtdivid)
	{
		if(index>0){
			for(var i=0;i<length;i++){

				var div_id=document.getElementById('divResult'+txtdivid+i);
				if(div_id)
				{
					if(div_id.className=='over')
						index=div_id.id.split('divResult'+txtdivid)[1];
					div_id.className='normal';
				}
			}
			index--;
			if(document.getElementById('divResult'+txtdivid+index))
				document.getElementById('divResult'+txtdivid+index).className='over';
			if(txtid && document.getElementById('divResult'+txtdivid+index))
			{
				document.getElementById(txtid).value=
					document.getElementById('divResult'+txtdivid+index).innerHTML;
				selectFirst=document.getElementById('divResult'+txtdivid+index).innerHTML;
			}
		}
	}
}

function mouseOverChk(txtdivid,txtboxId)
{	
	for(var i=0;i<length;i++)
	{
		$('#divResult'+txtdivid+i).mouseover({param1: i,param2:txtdivid, param3:txtboxId}, fireMouseOverEvent);
	}
}


function fireMouseOverEvent(event)
{
	for(var i=0;i<length;i++)
	{	
		document.getElementById('divResult'+event.data.param2+i).className='normal';
	}
    document.getElementById('divResult'+event.data.param2+event.data.param1).className='over';	       
   	document.getElementById(event.data.param3).value= $('#divResult'+event.data.param2+event.data.param1).text();
    index=event.data.param1;
}

function __mouseOverChk(txtdivid,txtboxId)
{	
	for(var i=0;i<length;i++)
	{
		document.getElementById('divResult'+txtdivid+i).className='normal';		
		if ($('#divResult'+txtdivid+i).is(':hover')) 
		{
			document.getElementById('divResult'+txtdivid+i).className='over';	       
	       	document.getElementById(txtboxId).value= $('#divResult'+txtdivid+i).text();
	        index=i;
	    }
	}

}

var overText = function (div_value,txtdivid) 
{
	for(var i=0;i<length;i++)
	{
		if(document.getElementById('divResult'+i))
		{
			var div_id=document.getElementById('divResult'+i);

			if(div_id.className=='over')
				index=div_id.id.split(txtdivid)[1];
			div_id.className='normal';
		}
	}
	div_value.className = 'over';
	document.getElementById(txtid).value= div_value.innerHTML;
}
function trim(s)
{
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}



var hiddenId="";
function getDegreeTypeMasterAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	$('#degreeName').mousedown(function(e) {
	if( (event.keyCode == 1) ){
        alert(resourceJSON.msgleftbutton);
    }if( (event.keyCode== 3) ) {
        alert(resourceJSON.msgrightbutton);
    }else if( (event.keyCode== 2) ) {
        alert(resourceJSON.msgmiddlebutton); 
    }
	});
	var gedflag=false;
	if($('#degreeName').val()==resourceJSON.msgHighSchoolorGED){
		gedflag=true;
	}
		
	if($('#degreeName').val()==resourceJSON.msgNoDegree){
		$("#universityName").prop('readonly', true);
		$("#fieldName").prop('readonly', true);
		$("#attendedInYear").prop('disabled', true);
		$("#leftInYear").prop('disabled', true);
		$("#pathOfTranscriptFile").prop('disabled', true);
		$("#gpaCumulative1").prop('readonly', true);
		
		$("#universityName").val("");
		$("#fieldName").val("");
		$("#attendedInYear").val("");
		$("#leftInYear").val("");
		$("#pathOfTranscriptFile").val("");
		$("#gpaCumulative1").val("");
	}else{
		$("#universityName").prop('readonly', false);
		$("#fieldName").prop('readonly', false);
		$("#attendedInYear").prop('disabled', false);
		$("#leftInYear").prop('disabled', false);
		$("#pathOfTranscriptFile").prop('disabled', false);
		$("#gpaCumulative1").prop('readonly', false);
	}
		if($('#degreeName').val()==resourceJSON.msgNoDegree || gedflag==true){
			$('.nondgrReq').hide();
			
			if(gedflag==true){
				if(document.getElementById("districtIdForDSPQ").value!=806900){
					$('.nondgrReq1').show();
				}
				$('.nondgrReq2').show();
			}else{
				$('.nondgrReq1').hide();
				$('.nondgrReq2').hide();
			}
		}else{			
			if($("#degreeOptional").val()=="true")
			{
				$('.nondgrReq').show();
			}else
			{
				$('.nondgrReq').hide();
			}
			if($("#academicsDatesOptional").val()=="true")
			{
				$('.nondgrReq1').show();
			}else
			{
				$('.nondgrReq1').hide();
			}			
			$('.nondgrReq2').hide();
		}	
		
		
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		//searchArray = getDegreeMasterArray(txtSearch.value);
		//fatchData(txtSearch,searchArray,txtId,txtdivid);
		document.getElementById("universityName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getDegreeTypeMasterArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
	
	if($('#degreeName').val()==resourceJSON.msgNoDegree){
		$("#universityName").prop('readonly', true);
		$("#fieldName").prop('readonly', true);
		$("#attendedInYear").prop('disabled', true);
		$("#leftInYear").prop('disabled', true);
		$("#pathOfTranscriptFile").prop('disabled', true);
		$("#gpaCumulative1").prop('readonly', true);
		
		$("#universityName").val("");
		$("#fieldName").val("");
		$("#attendedInYear").val("");
		$("#leftInYear").val("");
		$("#pathOfTranscriptFile").val("");
		$("#gpaCumulative1").val("");
	}else{
		$("#universityName").prop('readonly', false);
		$("#fieldName").prop('readonly', false);
		$("#attendedInYear").prop('disabled', false);
		$("#leftInYear").prop('disabled', false);
		$("#pathOfTranscriptFile").prop('disabled', false);
		$("#gpaCumulative1").prop('readonly', false);
	}
		
		if($('#degreeName').val()==resourceJSON.msgNoDegree || $('#degreeName').val()==resourceJSON.msgHighSchoolorGED){
			$('.nondgrReq').hide();
			if($('#degreeName').val()==resourceJSON.msgHighSchoolorGED){
				$('.nondgrReq1').show();
				if(document.getElementById("districtIdForDSPQ").value==806900){
					$('.nondgrReq1').hide();
				}
				$('.nondgrReq2').show();
			}else{
				$('.nondgrReq1').hide();
				$('.nondgrReq2').hide();
			}
		}else{			
			if($("#degreeOptional").val()=="true")
			{
				$('.nondgrReq').show();
			}else
			{
				$('.nondgrReq').hide();
			}
			if($("#academicsDatesOptional").val()=="true")
			{
				$('.nondgrReq1').show();
			}else
			{
				$('.nondgrReq1').hide();
			}				
			$('.nondgrReq2').hide();
		}
	
}

function getDegreeTypeMasterArray(degreeName){
	
	var searchArray = new Array();
	DWRAutoComplete.getDegreeTypeMasterList(degreeName,{ 
		async: false,		
		callback: function(data){
		hiddenDataArray = new Array();
		showDataArray = new Array();
		for(i=0;i<data.length;i++){
			searchArray[i]=data[i].degreeName;
			hiddenDataArray[i]=data[i].degreeTypeId;
			showDataArray[i]=data[i].degreeName;
			degreeTypeArray[i]=data[i].degreeTypeCode;
		}
	}
	});	

	return searchArray;
}
