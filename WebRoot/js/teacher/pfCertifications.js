var txtBgColor="#F5E7E1";
var chkCallFrom=0;
var chkForALLDiv=0;

/*========  For Sorting  ===============*/
var page = 1;
var noOfRows = 10;
var sortOrderStr="";
var sortOrderType="";

var page2 = 1; 
var noOfRows2 = 10;
var sortOrderStr2="";
var sortOrderType2="";

var page3 = 1;
var noOfRows3 = 10;
var sortOrderStr3="";
var sortOrderType3="";

var dp_AdditionalDocuments_page = 1;
var dp_AdditionalDocuments_noOfRows = 10;
var dp_AdditionalDocuments_sortOrderStr="";
var dp_AdditionalDocuments_sortOrderType="";


var count=0;
function numericFilter(txb) {  
   txb.value = txb.value.replace(/[^\0-9]/ig, "");  
   if(!txb.value)
   {   
   if(count==0)
   $('#errordivElectronicReferences').append("&#149; "+resourceJSON.errorMsgForCandidate+"<br>");
   
   count++;
   }
}

function setPageFlag(pgNo)
{
	document.getElementById("pageflag").value=pgNo;
}
function getPaging(pageno)
{
	var currentPageFlag = document.getElementById("pageflag").value;
	
	if(currentPageFlag=="2"){
		if(pageno!='')
		{
			page2=pageno;	
		}
		else
		{
			page2=1;
		}
		noOfRows2 = document.getElementById("pageSize1").value;
		getElectronicReferencesGrid();
	}else if(currentPageFlag=="3"){
		if(pageno!='')
		{
			page3=pageno;	
		}
		else
		{
			page3=1;
		}
		noOfRows3 = document.getElementById("pageSize3").value;
		getVideoLinksGrid();
	}else if(currentPageFlag=="4"){
		
		if(pageno!='')
		{
			dp_AdditionalDocuments_page=pageno;	
		}
		else
		{
			dp_AdditionalDocuments_page=1;
		}
		dp_AdditionalDocuments_noOfRows = document.getElementById("pageSize4").value;
		showGridAdditionalDocuments();
	}else{
		if(pageno!='')
		{
			page=pageno;	
		}
		else
		{
			page=1;
		}
		noOfRows = document.getElementById("pageSize").value;
		showGrid();
	}		
}
function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
	var currentPageFlag = document.getElementById("pageflag").value;
	if(currentPageFlag=="2"){
		if(pageno!=''){
			page2=pageno;	
		}else{
			page2=1;
		}
		sortOrderStr2	=	sortOrder;
		sortOrderType2	=	sortOrderTyp;
		if(document.getElementById("pageSize1")!=null){
			noOfRows2 = document.getElementById("pageSize1").value;
		}else{
			noOfRows2=10;
		}
		getElectronicReferencesGrid();
	}else if(currentPageFlag=="3"){
		if(pageno!=''){
			page3=pageno;	
		}else{
			page3=1;
		}
		sortOrderStr3	=	sortOrder;
		sortOrderType3	=	sortOrderTyp;
		if(document.getElementById("pageSize3")!=null){
			noOfRows3 = document.getElementById("pageSize3").value;
		}else{
			noOfRows3=10;
		}
		getVideoLinksGrid();
	}else if(currentPageFlag=="4"){
		if(pageno!=''){
			dp_AdditionalDocuments_page=pageno;	
		}else{
			dp_AdditionalDocuments_page=1;
		}
		dp_AdditionalDocuments_sortOrderStr	=	sortOrder;
		dp_AdditionalDocuments_sortOrderType	=	sortOrderTyp;
		if(document.getElementById("pageSize4")!=null){
			dp_AdditionalDocuments_noOfRows = document.getElementById("pageSize4").value;
		}else{
			dp_AdditionalDocuments_noOfRows=10;
		}
		showGridAdditionalDocuments();
	}else{
		if(pageno!=''){
			page=pageno;	
		}else{
			page=1;
		}
		sortOrderStr	=	sortOrder;
		sortOrderType	=	sortOrderTyp;
		if(document.getElementById("pageSize")!=null){
			noOfRows = document.getElementById("pageSize").value;
		}else{
			noOfRows=10;
		}
		showGrid();
	}
}

function getDivNo(divNo)
{
	$('#divNo').val(divNo);
}

function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert(resourceJSON.msgServerErr+": "+exception.javaClassName);}
}
function trim(s)
{
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}
function isNumber(field) {
	var re = /^[0-9-'.'-',']*$/;
	//var re =/^(0\.|[1-9]\d*\.)d{2}/ 
	if (!re.test(field.value)) {
//		alert('Value must be all numberic charcters, including "." or "," non numerics will be removed from field!');
		field.value = field.value.replace(/[^0-9-'.'-',']/g,"");
	}
}
function checkForInt(evt) {

	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==8)
		return true;

	return( (charCode >= 48 && charCode <= 57)||(charCode==13) );
}

function setFocus(id)
{
	if(id!="")
		document.getElementById(id).focus();
}

function delRow(id)
{
	//alert(id)
	if(window.confirm(resourceJSON.msgsuredelete))
	{
		PFAcademics.deletePFAcademinGrid(id, function(data)
		{
			showGrid();
			return false;
		});
	}
}
function showGrid()
{
	PFCertifications.getPFCertificationsGrid(noOfRows,page,sortOrderStr,sortOrderType,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
		$('#divDataGrid').html(data);
		applyScrollOnTbl();
	}});
}
function getPraxis()
{
	
	var statemaster = {stateId:dwr.util.getValue("stateMaster")};
	PFCertifications.getPraxis(statemaster,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			if(data!=null)
			{	
				//alert("reading "+data.praxisIReading+" writing "+data.praxisIWriting+" maths "+data.praxisIMathematics);
				$("#praxisArea").show();
				$("#reading").html(data.praxisIReading);
				$("#writing").html(data.praxisIWriting);
				$("#maths").html(data.praxisIMathematics);
			}else
			{
				$("#praxisArea").hide();
				$("#reading").html("");
				$("#writing").html("");
				$("#maths").html("");
			}
	}});
}
function showForm()
{
	document.getElementById("frmCertificate").reset();
	document.getElementById("certId").value="";
	document.getElementById("divMainForm").style.display="block";
	document.getElementById("removeCert").style.display="none";
	document.getElementById("stateMaster").focus();
	
	$('#errordiv').empty();
	setDefColortoErrorMsg();
	return false;
}

function clearForm()
{
	$("#praxisArea").hide();
	$("#reading").html("");
	$("#writing").html("");
	$("#maths").html("");
}

function clearCertType()
{
	$("#certificateTypeMaster").val("");
	$("#certType").val("");
}
function showHideFileUpload(option)
{
	if(option==3)
	{
		$("#fileuploadDiv").hide();
	}else{
		$("#fileuploadDiv").show();
	}
}
function insertOrUpdate(callStatus)
{
	finalCallStatus = callStatus;
	var certId = document.getElementById("certId");

	var stateMaster = document.getElementById("stateMaster");
	var yearReceived = document.getElementById("yearReceived");
	var certificationStatusMaster = document.getElementById("certificationStatusMaster").value;
	var certType = document.getElementById("certType");
	var certUrl = trim(document.getElementById("certUrl").value);
	
	var certificationtypeMasterObj = {certificationTypeMasterId:dwr.util.getValue("certificationtypeMaster")};
	var certificationTypeMaster = document.getElementById("certificationtypeMaster").value;
	var yearExpires = document.getElementById("yearexpires").value;
	var doenumber = trim(document.getElementById("doenumber").value);
	var certText = trim(document.getElementById("certText").value);
	
	var pathOfCertificationFile = document.getElementById("pathOfCertificationFile");
	var cnt=0;
	var focs=0;	
	
	var fileName = pathOfCertificationFile.value;
	var ext = fileName.substr(fileName.lastIndexOf('.') + 1).toLowerCase();
	var fileSize=0;		
	if ($.browser.msie==true){	
	    fileSize = 0;	   
	}else{		
		if(pathOfCertificationFile.files[0]!=undefined)
		fileSize = pathOfCertificationFile.files[0].size;
	}	
	if(doenumber=="")
	{
		doenumber=0;
	}
	if(yearExpires=="")
	{
		yearExpires=0;
	}
	$('#errordiv').empty();
	setDefColortoErrorMsg();
	
	if(certificationStatusMaster=="")
	{
		$('#errordiv').append("&#149; "+resourceJSON.msgcertificate+"<br>");
			if(focs==0)
				$('#certificationStatusMaster').focus();
			
		$('#certificationStatusMaster').css("background-color",txtBgColor);
			cnt++;focs++;	
	}
	
	if(certificationStatusMaster!=5){
		var certificationtypeMasterObj = {certificationTypeMasterId:dwr.util.getValue("certificationtypeMaster")};
		if(trim(certificationtypeMasterObj.certificationTypeMasterId)==0)
		{
			$('#errordiv').append("&#149; "+resourceJSON.msgceritificationtype+"<br>");
			if(focs==0)
				$('#certificationtypeMaster').focus();
			
			$('#certificationtypeMaster').css("background-color",txtBgColor);
				cnt++;focs++;	
		}
		
		
		
		if(trim(stateMaster.value)=="")
		{
			$('#errordiv').append("&#149; "+resourceJSON.msgPleaseselectaState+"<br>");
			if(focs==0)
				$('#stateMaster').focus();
			
			$('#stateMaster').css("background-color",txtBgColor);
			cnt++;focs++;
		}
		
		if(certificationStatusMaster!=3)
		{
			if(trim(yearReceived.value)=="")
			{
				$('#errordiv').append("&#149; "+resourceJSON.msgyearreciverd+"<br>");
				if(focs==0)
					$('#yearReceived').focus();
				
				$('#yearReceived').css("background-color",txtBgColor);
				cnt++;focs++;
			}
			/*if(yearExpires==0)
			{
				$('#errordiv').append("&#149; Please select Year Expires<br>");
				if(focs==0)
					$('#yearexpires').focus();
				
				$('#yearexpires').css("background-color",txtBgColor);
				cnt++;focs++;
			}*/
		}
		
		
		var certTypeObj = {certTypeId:dwr.util.getValue("certificateTypeMaster")};
		if(trim(certTypeObj.certTypeId)=="" && trim(certType.value)!="")
		{
			$('#errordiv').append("&#149; "+resourceJSON.msgnotexistselecstate+"<br>");
			if(focs==0)
				$('#certType').focus();
			
			$('#certType').css("background-color",txtBgColor);
			cnt++;focs++;
		}
		if(trim(certType.value)=="")
		{
			$('#errordiv').append("&#149; "+resourceJSON.msgentercertificatename+"<br>");
			if(focs==0)
				$('#certType').focus();
			
			$('#certType').css("background-color",txtBgColor);
			cnt++;focs++;
		}
		if(certificationStatusMaster==1 || certificationStatusMaster==2)
		{
			var pathOfCertification = document.getElementById("pathOfCertification").value;
			if(certUrl=="" && pathOfCertification=="" && pathOfCertificationFile.value=="")
			{
				$('#errordiv').append("&#149; "+resourceJSON.msgentervalidlicence+"<br>");
					if(focs==0)
					$('#certUrl').focus();
					$('#certUrl').css("background-color",txtBgColor);
					cnt++;focs++;	
					return false;
			}
		}
		if(ext!=""){
			if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt'))
			{
				$('#errordiv').append("&#149; "+resourceJSON.msgaacptfileformate+"<br>");
					if(focs==0)
						$('#pathOfCertification').focus();
					
					$('#pathOfCertification').css("background-color",txtBgColor);
					cnt++;focs++;	
					return false;
			}	
			else if(fileSize>=10485760)
			{
				$('#errordiv').append("&#149; "+resourceJSON.msgfilesizelessthan+"<br>");
					if(focs==0)
						$('#pathOfCertification').focus();
					
					$('#pathOfCertification').css("background-color",txtBgColor);
					cnt++;focs++;	
					return false;
			}
		}
	}
	if(cnt!=0)		
	{
		$('#errordiv').show();
		return false;
	}
	else
	{
		var stateObj = {stateId:dwr.util.getValue("stateMaster")};
		var certificationStatusMasterObj= {certificationStatusId:dwr.util.getValue("certificationStatusMaster")};
		var certificate =null;
		
		if(dwr.util.getValue("certificationStatusMaster")==""){
			certificate = {certId:null, stateMaster:stateObj,yearReceived:trim(yearReceived.value),certType:null,certUrl:null,
					certificateTypeMaster:certTypeObj,pkOffered:null,kgOffered:null,g01Offered:null,g02Offered:null,
					g03Offered:null,g04Offered:null,g05Offered:null,g06Offered:null,g07Offered:null,g08Offered:null,
					g09Offered:null,g10Offered:null,g11Offered:null,g12Offered:null,readingQualifyingScore:null,
					writingQualifyingScore:null,mathematicsQualifyingScore:null,certificationTypeMaster:null,
					yearExpired:trim(yearexpires.value),doeNumber:null,certText:null};
		}else{
			certificate = {certId:null,certificationStatusMaster:certificationStatusMasterObj,certUrl:null,
					stateMaster:stateObj,yearReceived:trim(yearReceived.value),certType:trim(certType.value),certificateTypeMaster:certTypeObj,
					pkOffered:null,kgOffered:null,g01Offered:null,g02Offered:null,g03Offered:null,
					g04Offered:null,g05Offered:null,g06Offered:null,g07Offered:null,g08Offered:null,
					g09Offered:null,g10Offered:null,g11Offered:null,g12Offered:null,readingQualifyingScore:null,
					writingQualifyingScore:null,mathematicsQualifyingScore:null,certificationTypeMaster:certificationtypeMasterObj,
					yearExpired:yearExpires,doeNumber:doenumber,certText:certText};
		}
		dwr.engine.beginBatch();
		dwr.util.getValues(certificate);
		
		var certiFile = document.getElementById("pathOfCertification").value;
		if(certiFile!=""){
			certificate.pathOfCertification=certiFile;
		}
		if(fileName!="")
		{	
			PFCertifications.findDuplicateCertificate(certificate,{
				async: true,
				errorHandler:handleError,
				callback:function(data)
					{
						if(data)
						{
							$('#errordiv').append("&#149; "+resourceJSON.msgduplicateceritificate+"<br>");
							$('#certType').focus();
							$('#certType').css("background-color",txtBgColor);
							$('#errordiv').show();
							return false;
						}else{
							$('#loadingDiv').show();
							chkForALLDiv=1;
							document.getElementById("frmCertificate").submit();
						}
					}
				});
				dwr.engine.endBatch();
				return true;
		}else{
			if(certificationStatusMaster==5){
				certificate = {certId:certId.value, stateMaster:null,yearReceived:null,certType:null,certUrl:null,
						certificationStatusMaster:certificationStatusMasterObj,pkOffered:null,kgOffered:null,g01Offered:null,g02Offered:null,
						g03Offered:null,g04Offered:null,g05Offered:null,g06Offered:null,g07Offered:null,g08Offered:null,
						g09Offered:null,g10Offered:null,g11Offered:null,g12Offered:null,readingQualifyingScore:null,
						writingQualifyingScore:null,mathematicsQualifyingScore:null,certificationTypeMaster:null,
						yearExpired:null,doeNumber:null,certText:null};
			}
			PFCertifications.saveOrUpdate(certificate,{
				async: true,
				errorHandler:handleError,
				callback:function(data)
					{
						if(data==1)
						{
							$('#errordiv').append("&#149; "+resourceJSON.msgduplicateceritificate+"<br>");
							$('#certType').focus();
							$('#certType').css("background-color",txtBgColor);
							$('#errordiv').show();
							return false;
						}else if(data==0){
							errorHandler:handleError
						}
						$('#loadingDiv').hide();
						hideForm();
						showGrid();
					}
				});
				dwr.engine.endBatch();
				return true;
			}
		}	
		
		//sendToOtherPager();
		return true;
	//}	
}
		 
function saveCertification(fileName){
	var certId = document.getElementById("certId");
	var certText = trim(document.getElementById("certText").value);
	var stateMaster = document.getElementById("stateMaster");
	var yearReceived = document.getElementById("yearReceived");
	
	var certType = document.getElementById("certType");
	var certUrl = trim(document.getElementById("certUrl").value);
	
	var stateObj = {stateId:dwr.util.getValue("stateMaster")};
	var certificationStatusMasterObj= {certificationStatusId:dwr.util.getValue("certificationStatusMaster")};
	var certTypeObj = {certTypeId:dwr.util.getValue("certificateTypeMaster")};
	var certificate =null;
	
	var certificationtypeMasterObj = {certificationTypeMasterId:dwr.util.getValue("certificationtypeMaster")};
	var yearExpires = document.getElementById("yearexpires").value;
	var doenumber = trim(document.getElementById("doenumber").value);
	
	if(doenumber=="")
	{
		doenumber=0;
	}
	if(yearExpires=="")
	{
		yearExpires=0;
	}
	
	if(dwr.util.getValue("certificationStatusMaster")==""){
		certificate = {certId:null, stateMaster:stateObj,yearReceived:trim(yearReceived.value),certType:null,certUrl:null,
				certificateTypeMaster:certTypeObj,pkOffered:null,kgOffered:null,g01Offered:null,g02Offered:null,
				g03Offered:null,g04Offered:null,g05Offered:null,g06Offered:null,g07Offered:null,g08Offered:null,
				g09Offered:null,g10Offered:null,g11Offered:null,g12Offered:null,readingQualifyingScore:null,
				writingQualifyingScore:null,mathematicsQualifyingScore:null,certificationTypeMaster:null,
				yearExpired:trim(yearexpires.value),doeNumber:null,certText:null};
	}else{
		certificate = {certId:null,certificationStatusMaster:certificationStatusMasterObj,certUrl:null,
				stateMaster:stateObj,yearReceived:trim(yearReceived.value),certType:trim(certType.value),certificateTypeMaster:certTypeObj,
				pkOffered:null,kgOffered:null,g01Offered:null,g02Offered:null,g03Offered:null,
				g04Offered:null,g05Offered:null,g06Offered:null,g07Offered:null,g08Offered:null,
				g09Offered:null,g10Offered:null,g11Offered:null,g12Offered:null,readingQualifyingScore:null,
				writingQualifyingScore:null,mathematicsQualifyingScore:null,certificationTypeMaster:certificationtypeMasterObj,
				yearExpired:yearExpires,doeNumber:doenumber,certText:certText};
	}
	
	dwr.engine.beginBatch();
	dwr.util.getValues(certificate);

	if(fileName!=""){
		certificate.pathOfCertification=fileName;
	}
	//alert("fileName :: "+fileName+"\n>>>> "+certificate.pathOfCertification);
	PFCertifications.saveOrUpdate(certificate,{
		async: false,
		errorHandler:handleError,
		callback:function(data)
			{
				if(data==1)
				{
					$('#errordiv').append("&#149; "+resourceJSON.msgduplicateceritificate+"<br>");
					$('#certType').focus();
					$('#certType').css("background-color",txtBgColor);
					$('#errordiv').show();
					return false;
				}else if(data==0){
					errorHandler:handleError
				}
				$('#loadingDiv').hide();
				hideForm();
				showGrid();
				if(finalCallStatus==1)
				{
					window.location.href="experiences.do";
				}
			}
		});
	dwr.engine.endBatch();
	sendToOtherPager();
}

function clearReferences()
{
	// line added by ashish ratan for IE 10-11
	// replace field with old html
	// comment by ashish ratan
		$('#pathOfReferenceFile')
		.replaceWith(
				"<input id='pathOfReferenceFile' name='pathOfReferenceFile' type='file' width='20px;' />");	 
		document.getElementById("pathOfReferenceFile").value=null;
		// this property gives error in IE ....
	 document.getElementById("pathOfReferenceFile").reset();
	$('#pathOfReferenceFile').css("background-color","");	
}
function clearCertification(){	
// $('#pathOfCertificationFile1').html(
// "<input id='pathOfCertificationFile' name='pathOfCertificationFile'
// type='file' width='20px;' />'");
//		
	$('#pathOfCertificationFile').replaceWith(
			"<input id='pathOfCertificationFile' name='pathOfCertificationFile' type='file' width='20px;' />");

	document.getElementById("pathOfCertificationFile").value="";
	 document.getElementById("pathOfCertificationFile").reset();
	$('#pathOfCertificationFile').css("background-color","");
}
function sendToOtherPager()
{
	
	$('#loadingDiv').hide();
	if(chkCallFrom==1)
	{
		window.location.href="experiences.do";
	}
}

function deleteRecord_Certificate(id)
{	
	document.getElementById("certificateTypeID").value=id;
	$('#deleteCertRec').modal('show');
}

function deleteCertificateRecord(){
	var id=document.getElementById("certificateTypeID").value;
	
	PFCertifications.removeCertification(id,{ 
		async: false,
		callback: function(data){
		},errorHandler:handleError
	});
	
	PFCertifications.deleteRecord(id,{ 
	async: true,
	errorHandler:handleError,
	callback:function(data)
	{
		if(data){
			$('#deleteCertRec').modal('hide');
			showGrid();
			hideForm();
		}
		return false;
	}});
}

function removeReferences()
{
	var referenceId = document.getElementById("elerefAutoId").value;	
	if(window.confirm(resourceJSON.msgdeletelatter))
	{
		PFCertifications.removeReferences(referenceId,
		{ 
			async: false,
			errorHandler:handleError,
			callback: function(data)
			{	
				document.getElementById("removeref").style.display="none";
				getElectronicReferencesGrid();
				hideElectronicReferencesForm();
			}
		});
	}
}

function removeCertificationFile()
{
	var certId = document.getElementById("certId").value;
	if(window.confirm(resourceJSON.msgdeletecerticicate))
	{
		PFCertifications.removeCertification(certId,{ 
			async: false,
			callback: function(data){	
				document.getElementById("removeCert").style.display="none";
				showGrid();
				hideForm();
			},errorHandler:handleError
		});
	}
}


function showEditForm(id)
{
	document.getElementById('certText').value="";
	PFCertifications.showEditForm(id,{
		async: true,
		callback: function(data)
		{	
			showForm();
			try{
				dwr.util.setValues(data);
				
				document.getElementById("certId").value=data.certId;
				/*alert(""+data.stateMaster.stateId);*/
				if(data.stateMaster!=null && data.stateMaster.stateId!=null){
					document.getElementById("stateMaster").value=data.stateMaster.stateId;
				}
				
				document.getElementById('certText').value=data.certText;
				if(data.doeNumber==0)
				{
					document.getElementById("doenumber").value="";
				}	
				else
				{
					document.getElementById("doenumber").value=data.doeNumber;
				}
					
				if(data.certificationStatusMaster!=null){
					document.getElementById("certificationStatusMaster").value=data.certificationStatusMaster.certificationStatusId;
					if(data.certificationStatusMaster.certificationStatusId==3){
						showHideFileUpload(data.certificationStatusMaster.certificationStatusId);
					}else{
						showHideFileUpload(data.certificationStatusMaster.certificationStatusId);
					}
						
				}	
				else{
					document.getElementById("certificationStatusMaster").value="";
				}	
				
				if(data.certificationTypeMaster!=null){
					document.getElementById("certificationtypeMaster").value=data.certificationTypeMaster.certificationTypeMasterId;
					document.getElementById("certificationtypeMaster").text=data.certificationTypeMaster.certificationType;
				}
				
				if(data.yearReceived!=null && data.yearReceived!="")
					document.getElementById("yearReceived").value=data.yearReceived;
				else
					document.getElementById("yearReceived").text="Select";
					
				if(data.yearExpired==null || data.yearExpired==0)
				{
					document.getElementById("yearexpires").text="Select Year";
				}
				else
				{
					document.getElementById("yearexpires").value=data.yearExpired;
				}	
				document.getElementById("certUrl").value=data.certUrl;
				
				
				if(data.certificateTypeMaster!=null){
						document.getElementById("certType").value=data.certificateTypeMaster.certType;
						document.getElementById("certificateTypeMaster").value=data.certificateTypeMaster.certTypeId;
				}else{
						document.getElementById("certType").value="";
						document.getElementById("certificateTypeMaster").value="";
				}
				
				if(data.pathOfCertification!=null && data.pathOfCertification!="")
				{	
					document.getElementById("removeCert").style.display="block";
					document.getElementById("divCertName").style.display="block";
					document.getElementById("pathOfCertification").value=data.pathOfCertification;
					
					document.getElementById("divCertName").innerHTML="<a href='javascript:void(0)' id='hrefEditRef' onclick=\"downloadCertification('"+data.certId+"','hrefEditRef');" +
					"if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0');" +
					"return false;\">"+data.pathOfCertification+"</a>";
				}else{
					document.getElementById("divCertName").style.display="none";
					document.getElementById("removeCert").style.display="none";
					document.getElementById("pathOfCertificationFile").value="";
				}
				fieldsDisable(data.certificationStatusMaster.certificationStatusId);
				getPraxis();
				return false;
			}catch(e){
				alert(e);
			}
		},errorHandler:handleError
	});
	return false;
}
function hideTFAFields()
{
	$('#errordivExp').empty();
	$('#expCertTeacherTraining').css("background-color","");
	$('#tfaAffiliate').css("background-color","");
	$('#corpsYear').css("background-color","");
	$('#tfaRegion').css("background-color","");
	
	var tfaAffiliate 	= 	document.getElementById("tfaAffiliate").value;
	if(tfaAffiliate=="3")
	{
		$("#tfaFieldsDiv").fadeOut();
	}
	else
	{
		$("#tfaFieldsDiv").fadeIn();
	}
}

var finalCallStatus = 1;//--------------------------------------------


function saveAndContinueCertifications()
{

	
	
	
	var certificationType=0;
	var divvideourl=0;
	var divEReferences=0;
	var divAdditionalDoc=0;
	var msg=[];
	var block=0;
	
	if($("#stateMaster").is(':visible'))
		if(!insertOrUpdate(1))
		{
			certificationType=1;
			msg.push("Certification Type");

			var opened=true;
			$(".accordion-body").each(function(){
		        if($(this).hasClass("in")) {
		        	if($(this).attr('id')=="collapseOne")
		        	opened=false;
		        }
		    });
			if(block==0 && opened)
			$("#certificationTypeDiv").click();
			
			block++;
		}


	if($("#salutation").is(':visible'))
		if(!insertOrUpdateElectronicReferences(1))
		{
			divEReferences=1;
			msg.push("E-References");

			var opened=true;
			$(".accordion-body").each(function(){
		        if($(this).hasClass("in")) {
		        	if($(this).attr('id')=="collapseTwo")
		        	opened=false;
		        }
		    });
			if(block==0 && opened)
			$("#electronicReferencesDiv").click();
			
			block++;
		}

	if($("#videourl").is(':visible'))
		if(!insertOrUpdatevideoLinks())
		{
			divvideourl=1;
			msg.push(" Video Links");
			
			var opened=true;
			$(".accordion-body").each(function(){
		        if($(this).hasClass("in")) {
		        	if($(this).attr('id')=="collapseThree")
		        	opened=false;
		        }
		    });
			if(block==0 && opened)
			{
				$("#videoLinksDiv").click();
			}
			block++;
		}
	
	
	
	if($("#documentName").is(':visible'))
	{
		if(!insertOrUpdate_AdditionalDocuments(1))
		{
			divAdditionalDoc=1;
			msg.push(" Additional Documents");
			
			var opened=true;
			$(".accordion-body").each(function(){
		        if($(this).hasClass("in")) {
		        	if($(this).attr('id')=="collapseFour")
		        	opened=false;
		        }
		    });
			
			if(block==0 && opened)
			{
				$("#additionalDocumentDiv").click();
			}
			block++;
		}
	}


	if((certificationType+divvideourl+divEReferences+divAdditionalDoc)>0)
	{
		$('#message2show').html(resourceJSON.msgpleasecomplete+msg+resourceJSON.msgsavecontinue+"\.");
		$('#myModal2').modal('show');
		return;
	}
	
	
	var expCertTeacherTraining = document.getElementById("expCertTeacherTraining");
	var isNonTeacher = false;
	
	if(document.getElementById("isNonTeacher").checked){
		var isNonTeacher = true;	
	}
	
	var nbc1 = document.getElementById("nbc1");
	var nbc2 = document.getElementById("nbc2");
	
	
	var nationalBoardCertYear = document.getElementById("nationalBoardCertYear");
	var tfaAffiliate;
	var corpsYear; 
	var tfaRegion; 
	
	if(resourceJSON.locale=="en"){
	   var tfaAffiliate 	= 	document.getElementById("tfaAffiliate").value;

	   var corpsYear 		= 	document.getElementById("corpsYear").value;

	   var tfaRegion 		= 	document.getElementById("tfaRegion").value;
	}
	var cnt=0;
	var focs=0;	
	
	$('#errordivExp').empty();
	setDefColortoErrorMsgToExp();
	
	$('#expCertTeacherTraining').css("background-color","");
	$('#tfaAffiliate').css("background-color","");
	$('#corpsYear').css("background-color","");
	$('#tfaRegion').css("background-color","");
	
	var canServeAsSubTeacher=2;
	try{
		if (document.getElementById('canServeAsSubTeacher0').checked) {
			canServeAsSubTeacher = document.getElementById('canServeAsSubTeacher0').value;
		}else if (document.getElementById('canServeAsSubTeacher1').checked) {
			canServeAsSubTeacher = document.getElementById('canServeAsSubTeacher1').value;
		}
	}catch(err){alert(err);}

	if(trim(expCertTeacherTraining.value)=="" && !isNonTeacher)
	{
		$('#errordivExp').append("&#149; "+resourceJSON.msgteachingcertificate+"<br>");
		if(focs==0)
			$('#expCertTeacherTraining').focus();
		
		$('#expCertTeacherTraining').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	if(resourceJSON.locale=="en"){
	if(nbc1.checked && trim(nationalBoardCertYear.value)=="")
	{
		$('#errordivExp').append("&#149; "+resourceJSON.msgnationalboard+"<br>");
		if(focs==0)
			$('#nationalBoardCertYear').focus();
		
		$('#nationalBoardCertYear').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	
	if(trim(tfaAffiliate)=="")
	{
		$('#errordivExp').append("&#149; "+resourceJSON.msgtfa+"<br>");
		if(focs==0)
			$('#tfaAffiliate').focus();
		
		$('#tfaAffiliate').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	
	if(tfaAffiliate!="3" && tfaAffiliate!="")
	{
		if(trim(corpsYear)=="")
		{
			$('#errordivExp').append("&#149; "+resourceJSON.msgcorpsyear+"<br>");
			if(focs==0)
				$('#corpsYear').focus();
			
			$('#corpsYear').css("background-color",txtBgColor);
			cnt++;focs++;
		}
		
		if(trim(tfaRegion)=="")
		{
			$('#errordivExp').append("&#149; "+resourceJSON.msgtfaregion+"<br>");
			if(focs==0)
				$('#tfaRegion').focus();
			
			$('#tfaRegion').css("background-color",txtBgColor);
			cnt++;focs++;
		}
	  }
	
	}
	if(cnt!=0)		
	{
		
		$('#errordiv').show();
		return false;
	}
	else
	{
		
		var teacherExp = {expId:null,expCertTeacherTraining:null,canServeAsSubTeacher:null,isNonTeacher:null};
		
		if(resourceJSON.locale=="en"){
		if(nbc1.checked)
		{
			teacherExp.nationalBoardCertYear=trim(nationalBoardCertYear.value);
		}
		}
	    dwr.util.getValues(teacherExp);
		teacherExp.canServeAsSubTeacher=canServeAsSubTeacher;
		dwr.engine.beginBatch();
	
	
		PFCertifications.insertUpdateTeacherExp(teacherExp,tfaAffiliate,corpsYear,tfaRegion,canServeAsSubTeacher,
		{ 
		
		async: true,
		errorHandler:handleError,
		callback: function(data)
			{
			
				if(chkCertificateForm())
				{
					
					//if(chkForALLDiv==0)
						window.location.href="experiences.do";
				}
			}
		});
		
		dwr.engine.endBatch();
	}
}



function hideForm()
{

	document.getElementById("frmCertificate").reset();
	document.getElementById("certId").value="";	
	document.getElementById("divMainForm").style.display="none";
	$('#errordiv').empty();
	setDefColortoErrorMsg();
	return false;
}
function chkNBCert(chk)
{
	if(chk=="1")
	{
		document.getElementById("divNbdCert").style.display="block";
	}
	else
	{
		document.getElementById("divNbdCert").style.display="none";
	}
	document.getElementById("")	
}

function setDefColortoErrorMsgToExp()
{	
	$('#expCertTeacherTraining').css("background-color","");
	$('#nationalBoardCert').css("background-color","");
	$('#nationalBoardCertYear').css("background-color","");
}
function setDefColortoErrorMsg()
{
	$('#certificationtypeMaster').css("background-color","");
	$('#stateMaster').css("background-color","");
	$('#yearReceived').css("background-color","");
	$('#certType').css("background-color","");
	$('#certName').css("background-color","");
	$('#certUrl').css("background-color","");
	$('#certificationStatusMaster').css("background-color","");
	$('#yearexpires').css("background-color","");
}
function chkCertificateForm()
{
	
	var idList = new Array("stateMaster","yearReceived","certType");
	var count = 0;
	
	for(i=0;i<idList.length;i++)
	{
		
	    if(resourceJSON.locale=="fr")
	    {
	      	
	    }
	      else
	      {
	       if(trim(document.getElementById(idList[i]).value)!="")
		   {
		 	 count++;	
		   }
	   }
	}

	if(count==0)
	{
		return true;	
	}
	else
	{
		chkCallFrom=1;
		insertOrUpdate();
		if(count==3){
			return true;	
		}
	}
}
function chkForEnter(evt)
{	
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==13)
	{
		insertOrUpdate();
	}	
}
function chkForEnterCert(evt)
{	
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==13)
	{
		saveAndContinueCertifications();
	}	
}

//Ramesh :: St. for ElectronicReferences

function chkForEnterElectronicReferences(evt)
{
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	//alert(">?>"+evt.srcElement.id)
	if(charCode==13 && evt.srcElement.id!="")
	{
		insertOrUpdateElectronicReferences();
	}	
}



function hideElectronicReferencesForm()
{
	document.getElementById("frmElectronicReferences").reset();
	document.getElementById("elerefAutoId").value="";
	document.getElementById("divElectronicReferences").style.display="none";
}

var txtBgColor="#F5E7E1";
function insertOrUpdateElectronicReferences(callStatus)
{
	finalCallStatus = callStatus;
	var elerefAutoId	=	document.getElementById("elerefAutoId");
	var salutation		=	document.getElementById("salutation");
	
	var firstName		=	trim(document.getElementById("firstName").value);
	var lastName		= 	trim(document.getElementById("lastName").value);;
	var designation		= 	trim(document.getElementById("designation").value);
	
	var organization	=	trim(document.getElementById("organization").value);
	var email			=	trim(document.getElementById("email").value);
	
	var contactnumber	=	trim(document.getElementById("contactnumber").value);
	var rdcontacted0	=   document.getElementById("rdcontacted0");
	var rdcontacted1	=   document.getElementById("rdcontacted1");
	var longHaveYouKnow	=   trim(document.getElementById("longHaveYouKnow").value);
	
	var pathOfReferencesFile = document.getElementById("pathOfReferenceFile");
	var cnt=0;
	var focs=0;
			
	var referenceDetails = trim(document.getElementById("referenceDetails").value);	
	var fileName = pathOfReferencesFile.value;
	var ext = fileName.substr(fileName.lastIndexOf('.') + 1).toLowerCase();
	var fileSize=0;		
	if ($.browser.msie==true){	
	    fileSize = 0;	   
	}else{		
		if(pathOfReferencesFile.files[0]!=undefined)
		fileSize = pathOfReferencesFile.files[0].size;
	}
	
	$('#errordivElectronicReferences').empty();
	setDefColortoErrorMsgToElectronicReferences();
	
	if(firstName=="")
	{
		$('#errordivElectronicReferences').append("&#149; "+resourceJSON.PlzEtrFirstName +"<br>");
		if(focs==0)
			$('#firstName').focus();

		$('#firstName').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	if(lastName=="")
	{
		$('#errordivElectronicReferences').append("&#149; "+resourceJSON.PlzEtrLastName +"<br>");
		if(focs==0)
			$('#lastName').focus();

		$('#lastName').css("background-color",txtBgColor);
		cnt++;focs++;
	}	
	
	if(designation=="" &&  ((window.location.hostname=="nccloud.teachermatch.org") || (window.location.hostname=="nc.teachermatch.org") || (window.location.hostname=="localhost")))
	{
		$('#errordivElectronicReferences').append("&#149; "+resourceJSON.msgTitle +"<br>");
		if(focs==0)
			$('#designation').focus();

		$('#designation').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	
	if(organization=="" && ((window.location.hostname=="nccloud.teachermatch.org") || (window.location.hostname=="nc.teachermatch.org") || (window.location.hostname=="localhost")))
	{
		$('#errordivElectronicReferences').append("&#149; "+resourceJSON.PlzEtrOrg +"<br>");
		if(focs==0)
			$('#organization').focus();

		$('#organization').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	
	
	if(email=="")
	{
		$('#errordivElectronicReferences').append("&#149; "+resourceJSON.PlzEtrEmail +"<br>");
		if(focs==0)
			$('#email').focus();
		
		$('#email').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	else if(!isEmailAddress(email))
	{		
		$('#errordivElectronicReferences').append("&#149;" +resourceJSON.PlzEtrVldEmail +"<br>");
		if(focs==0)
			$('#email').focus();
		
		$('#email').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	
	if(contactnumber=="")
	{
		$('#errordivElectronicReferences').append("&#149; "+resourceJSON.PlzEtrCtctNum +"<br>");
		if(focs==0)
			$('#contactnumber').focus();

		$('#contactnumber').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	else if(contactnumber.length==10)
	{
		
	}
	else
	{
		$('#errordivElectronicReferences').append("&#149; "+resourceJSON.errorMsgForNumber +"<br>");
		if(focs==0)
			$('#contactnumber').focus();

		$('#contactnumber').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	
	var rdcontacted_value;
	
	if (rdcontacted0.checked) {
		rdcontacted_value = false;
	}
	else if (rdcontacted1.checked) {
		rdcontacted_value = true;
	}
	if(pathOfReferenceFile!=null && fileName!=null || pathOfReferenceFile!="" && fileName!=null)
	{
	if(ext!=""){
		if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt'))
		{
			$('#errordivElectronicReferences').append("&#149; "+resourceJSON.msgaacptfileformate+"<br>");
				if(focs==0)
					$('#pathOfReferenceFile').focus();
				
				$('#pathOfReferenceFile').css("background-color",txtBgColor);
				cnt++;focs++;	
				return false;
		}	
		else if(fileSize>=10485760)
		{
			$('#errordivElectronicReferences').append("&#149; "+resourceJSON.msgfilesizelessthan+"<br>");
				if(focs==0)
					$('#pathOfReferenceFile').focus();
				
				$('#pathOfReferenceFile').css("background-color",txtBgColor);
				cnt++;focs++;	
				return false;
		}
	}
}
	
	if(cnt!=0)		
	{
		$('#errordivElectronicReferences').show();
		return false;
	}else{
		var teacherElectronicReferences = {elerefAutoId:null,salutation:null, firstName:null, lastName:null, designation:null, organization:null,email:null,contactnumber:null,rdcontacted:null,referenceDetails:null,longHaveYouKnow:null};
		dwr.engine.beginBatch();
		dwr.util.getValues(teacherElectronicReferences);
		
		teacherElectronicReferences.salutation=salutation.value;
		teacherElectronicReferences.rdcontacted=rdcontacted_value;
		teacherElectronicReferences.firstName=firstName;
		teacherElectronicReferences.lastName=lastName;;
		teacherElectronicReferences.designation=designation;
		teacherElectronicReferences.organization=organization;
		teacherElectronicReferences.email=email;
		teacherElectronicReferences.referenceDetails=referenceDetails;
		teacherElectronicReferences.longHaveYouKnow=longHaveYouKnow;
		
		var refFile = document.getElementById("pathOfReference").value;
		if(refFile!=""){
			teacherElectronicReferences.pathOfReference=refFile;
		}
		if(fileName!="")
		{	
			PFCertifications.findDuplicateEleReferences(teacherElectronicReferences,{ 
				async: false,
				errorHandler:handleError,
				callback: function(data)
				{
				if(data=="isDuplicate")
				{
					$('#errordivElectronicReferences').append("&#149; "+resourceJSON.msgelectronicemail+"<br>");
					if(focs==0)
						$('#email').focus();
					$('#email').css("background-color",txtBgColor);
					cnt++;focs++;
					$('#errordivElectronicReferences').show();
					return false;
				}else{
					$('#loadingDiv').show();
					chkForALLDiv=1;
					document.getElementById("frmElectronicReferences").submit();
					return true;
				}
				}
			});
			
			
		}else{
			PFCertifications.saveOrUpdateElectronicReferences(teacherElectronicReferences,{ 
				async: true,
				errorHandler:handleError,
				callback: function(data)
				{
					if(data=="isDuplicate")
					{
						$('#errordivElectronicReferences').append("&#149; "+resourceJSON.msgelectronicemail+"<br>");
						if(focs==0)
							$('#email').focus();
						$('#email').css("background-color",txtBgColor);
							cnt++;focs++;
						$('#errordivElectronicReferences').show();
						return false;
					}
					hideElectronicReferencesForm();
					getElectronicReferencesGrid();
				}
			});
		}
		dwr.engine.endBatch();
		return true;
		//sendToOtherPager();
	}
}

function saveReference(fileName){
	var pathOfReferencesFile = document.getElementById("pathOfReferenceFile");
	var elerefAutoId	=	document.getElementById("elerefAutoId");
	var salutation		=	document.getElementById("salutation");
	
	var firstName		=	trim(document.getElementById("firstName").value);
	var lastName		= 	trim(document.getElementById("lastName").value);;
	var designation		= 	trim(document.getElementById("designation").value);
	
	var organization	=	trim(document.getElementById("organization").value);
	var email			=	trim(document.getElementById("email").value);
	
	var contactnumber	=	trim(document.getElementById("contactnumber").value);
	var rdcontacted0	=   document.getElementById("rdcontacted0");
	var rdcontacted1	=   document.getElementById("rdcontacted1");
	var referenceDetails = trim(document.getElementById("referenceDetails").value);
	var longHaveYouKnow	=   trim(document.getElementById("longHaveYouKnow").value);
	var cnt=0;
	var focs=0;	
	
	var rdcontacted_value;
	if (rdcontacted0.checked) {
		rdcontacted_value = false;
	}
	else if (rdcontacted1.checked) {
		rdcontacted_value = true;
	}
	
	var teacherElectronicReferences = {elerefAutoId:null,salutation:null, firstName:null, lastName:null, designation:null, organization:null,email:null,contactnumber:null,rdcontacted:null,pathOfReference:null,referenceDetails:null,longHaveYouKnow:null};
	dwr.engine.beginBatch();
	dwr.util.getValues(teacherElectronicReferences);
	
	teacherElectronicReferences.salutation=salutation.value;
	teacherElectronicReferences.rdcontacted=rdcontacted_value;
	teacherElectronicReferences.firstName=firstName;
	teacherElectronicReferences.lastName=lastName;;
	teacherElectronicReferences.designation=designation;
	teacherElectronicReferences.organization=organization;
	teacherElectronicReferences.email=email;
	teacherElectronicReferences.referenceDetails=referenceDetails;
	teacherElectronicReferences.pathOfReference=fileName;
	teacherElectronicReferences.longHaveYouKnow=longHaveYouKnow;
	if(pathOfReferencesFile!=null || pathOfReferencesFile!="")
	{
	
	PFCertifications.saveOrUpdateElectronicReferences(teacherElectronicReferences,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data)
		{
			$('#loadingDiv').hide();
			getElectronicReferencesGrid();
			hideElectronicReferencesForm();
			if(finalCallStatus==1)
			{
				window.location.href="experiences.do";
			}
				
		}
	});
	
	}
	else
	{
		dwr.engine.endBatch();
		return false;
		
	}
	dwr.engine.endBatch();
	return true;
}

function setDefColortoErrorMsgToElectronicReferences()
{
	$('#salutation').css("background-color","");
	$('#firstName').css("background-color","");
	$('#lastName').css("background-color","");
	$('#designation').css("background-color","");
	$('#organization').css("background-color","");
	$('#email').css("background-color","");
	$('#contactnumber').css("background-color","");
	$('#pathOfReferences').css("background-color","");
}


function isEmailAddress(str) 
{
	var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
	return emailPattern.test(str);
}

function getElectronicReferencesGrid()
{
	PFCertifications.getElectronicReferencesGrid(noOfRows2,page2,sortOrderStr2,sortOrderType2, { 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
		$('#divDataElectronicReferences').html(data);
		applyScrollOnTblEleRef();
		}});
}

var eleRefIDForTeacher=0;
var status_eref=0;
function changeStatusElectronicReferences(id,status)
{	
	eleRefIDForTeacher=id;
	status_eref=status;
	if(status==1)
	$('#chgstatusRef1').modal('show');
	else if(status==0)
	$('#chgstatusRef2').modal('show');	
}

function changeStatusElectronicReferencesConfirm()
{	
	$('#chgstatusRef1').modal('hide');
	$('#chgstatusRef2').modal('hide');
	
		PFCertifications.changeStatusElectronicReferences(eleRefIDForTeacher,status_eref, { 
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{
				getElectronicReferencesGrid();
				hideElectronicReferencesForm();
				return false;
			}});
		
		eleRefIDForTeacher=0;
}

function showElectronicReferencesForm()
{
	document.getElementById("frmElectronicReferences").reset();
	document.getElementById("elerefAutoId").value="";
	document.getElementById("divElectronicReferences").style.display="block";
	document.getElementById("removeref").style.display="none";
	document.getElementById("salutation").focus();
	setDefColortoErrorMsgToElectronicReferences();
	$('#errordivElectronicReferences').empty();
	return false;
}
function editFormElectronicReferences(id)
{  
	document.getElementById('referenceDetails').value="";
	PFCertifications.editElectronicReferences(id,{ 
			async: false,
			errorHandler:handleError,
			callback: function(data){		
				showElectronicReferencesForm();
				dwr.util.setValues(data);
				if(data.pathOfReference!=null && data.pathOfReference!="")
				{	
					document.getElementById("removeref").style.display="block";
					document.getElementById("pathOfReference").value=data.pathOfReference;
					document.getElementById("divRefName").style.display="block";
					document.getElementById("divRefName").innerHTML="<a href='javascript:void(0)' id='hrefEditRef' onclick=\"downloadReference('"+data.elerefAutoId+"','hrefEditRef');" +
					"if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0');" +
					"return false;\">"+data.pathOfReference+"</a>";
				}else{
					document.getElementById("divRefName").style.display="none";
					document.getElementById("removeref").style.display="none";
					document.getElementById("pathOfReference").value="";
				}
				return false;
			}
		});
	return false;
}



function downloadReference(referenceId, linkId)
{		
		PFCertifications.downloadReference(referenceId,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	if(data=="")
			{
				data="javascript:void(0)";
			}
			else if(data.indexOf(".doc")!=-1)
			{
				document.getElementById("ifrmRef").src=data;
			}else
			{
				document.getElementById(linkId).href = data;
			}
			return false;
		}});
}

function downloadCertification(certId, linkId)
{		
		PFCertifications.downloadCertification(certId,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data){
			if(data=="")
			{
				data="javascript:void(0)";
			}
			else if(data.indexOf(".doc")!=-1)
			{
				document.getElementById("ifrmCert").src=data;
			}else
			{
				document.getElementById(linkId).href = data;
			}
			return false;
		}});
}

//Ed. for ElectronicReferences

// Ramesh :: St Video Links

function showVideoLinksForm()
{
	document.getElementById("frmvideoLinks").reset();
	document.getElementById("videolinkAutoId").value="";
	document.getElementById("videofile").value="";
	document.getElementById("video").style.display="block";	
	document.getElementById("divvideoLinks").style.display="block";
	document.getElementById("videourl").focus();
	setDefColortoErrorMsgToVideoLinksForm();
	$('#errordivvideoLinks').empty();
	$('#video').empty();
	return false;
}

function setDefColortoErrorMsgToVideoLinksForm()
{
	$('#videourl').css("background-color","");
}


function hideVideoLinksForm()
{
	document.getElementById("frmvideoLinks").reset();
	document.getElementById("videolinkAutoId").value="";
	document.getElementById("divvideoLinks").style.display="none";
	$("#video").empty();
	document.getElementById("video").style.display="none";
	return false;
}

/*function insertOrUpdatevideo()
{	
	var video= document.getElementById("video");1048576
	alert(video.value);
	if(video.files[0].size>10485760)
         alert("video size Max 10 MB "+video.files[0].size);
	else
		alert("video size Min 10 MB"+video.files[0].size);
  
}*/
function getVideoLinksGrid()
{
	PFCertifications.getVideoLinksGrid(noOfRows3,page3,sortOrderStr3,sortOrderType3, { 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#divDataVideoLinks').html(data);
			applyScrollOnTblVideoLinks();
		}});
}


var videolnkIDForTeacher=0;
function delVideoLink(id)
{	
	videolnkIDForTeacher=id;
	$('#delVideoLnk').modal('show');
}

function delVideoLnkConfirm()
{	
		$('#delVideoLnk').modal('hide');
		PFCertifications.deleteVIdeoLink(videolnkIDForTeacher, { 
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{
				getVideoLinksGrid();
				hideVideoLinksForm();
				return false;
			}});
		
		videolnkIDForTeacher=0;
}


function insertOrUpdatevideoLinks()
{
	var elerefAutoId = document.getElementById("videolinkAutoId");
	var videourl = trim(document.getElementById("videourl").value);
	var video = trim(document.getElementById("videofile").value);	
	var videoSize = document.getElementById("videofile");
	var oldvideo = document.getElementById("video").innerHTML;
	var cnt=0;
	var focs=0;	
	
	$('#errordivvideoLinks').empty();
	setDefColortoErrorMsgToVideoLinksForm();
	
	if(videourl=="" && video =="" && oldvideo =="")
	{
		$('#errordivvideoLinks').append("&#149; "+resourceJSON.msgvideolink+"<br>");
		if(videourl==0)
			$('#videourl').focus();
		
		$('#videourl').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	if(video !="")
	{
		var ext = video.substr(video.lastIndexOf('.') + 1).toLowerCase();		
		if(!(ext=="mp4"||ext=="MP4"||ext=="ogv"||ext=="ogg"||ext=="webm"))
		{        	
		 $('#errordivvideoLinks').append("&#149; "+resourceJSON.msgacceptablevideo+"");
		 if(video==0)
			$('#videourl').focus();
		
		$('#videourl').css("background-color",txtBgColor);
		cnt++;focs++;
		}
		else if(videoSize.files[0].size>10485760)
		{        	
		 $('#errordivvideoLinks').append("&#149; "+resourceJSON.msgvideosize+"<br>");
		 if(video==0)
			$('#videourl').focus();
		
		$('#videourl').css("background-color",txtBgColor);
		cnt++;focs++;
		}
		else
		{
			video=video.replace(/^.*[\\\/]/, '');	
		}
	}
	else
	{
		video= document.getElementById('video').innerHTML;
	}
	
	if(cnt!=0 )		
	{
		$('#errordivvideoLinks').show();
		return false;
	}
	else
	{	
		
		$('#loadingDiv').show();		
		if(videoSize.value !="")
		{			
			document.getElementById("frmvideoLinks").submit();
		}	
		else
		{
		var teacherVideoLink = {videolinkAutoId:null,videourl:null, video:null, createdDate:null};
		dwr.engine.beginBatch();
		dwr.util.getValues(teacherVideoLink);
		teacherVideoLink.videourl=videourl;		
		teacherVideoLink.video=	oldvideo;	
		PFCertifications.saveOrUpdateVideoLinks(teacherVideoLink,{ 
			async: false,
			errorHandler:handleError,
			callback: function(data)
			{			
				$('#loadingDiv').hide();					
				hideVideoLinksForm();
				getVideoLinksGrid();
			}
		});
		dwr.engine.endBatch();
		return true;
	}   
  }
}
 
function saveVideo(videoFileName)
{	
	var videourl = trim(document.getElementById("videourl").value);
	
	var teacherVideoLink = {videolinkAutoId:null,videourl:null, video:null, createdDate:null};
	dwr.engine.beginBatch();
	dwr.util.getValues(teacherVideoLink);
	teacherVideoLink.videourl=videourl;	
	teacherVideoLink.video=	videoFileName;		
	PFCertifications.saveOrUpdateVideoLinks(teacherVideoLink,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data)
		{	
			$('#loadingDiv').hide();				
			hideVideoLinksForm();
			getVideoLinksGrid();
		}
	});
	dwr.engine.endBatch();
	return true;
}
function editFormVideoLink(id)
{
	PFCertifications.editVideoLink(id,
			{ 
				async: false,
				errorHandler:handleError,
				callback: function(data)
				{
					showVideoLinksForm();
					dwr.util.setValues(data);
					return false;
				}
			});
		return false;
}
function checkForEnter(evt)
{
	var charCode = (evt.which) ? evt.which : event.keyCode;
	if(charCode==13){
		insertOrUpdatevideoLinks();
		return true;
	}
}

function checkForDecimalTwo(evt) {
	var count=true;
	var str=evt.srcElement.value;
	if (str.lastIndexOf('.') == -1) str += ".";
	var decNum = str.substring(str.lastIndexOf('.')+1, str.length);
	if (decNum.length > 1)//here is the key u can just change from 2 to 3,45 etc to restict no of digits aftre decimal
	{
		//alert("Invalid more than 2 digits after decimal")
		//count=false;
	}
	//alert(count);
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==8)
		return true;

	var parts = evt.srcElement.value.split('.');
	if(parts.length > 1 && charCode==46)
		return false;

	return( ((charCode >= 48 || charCode==46)&& count && charCode <= 57)||(charCode==13) );
}

//Ramesh :: End Video Links

//======================= Start :: for additional document
function showAdditionalDocumentsForm()
{
	document.getElementById("additionDocumentId").value="";
	document.getElementById("divAdditionalDocumentsRow").style.display="block";
	document.getElementById("divDocumnetDone").style.display="block";
	document.getElementById("documentName").value="";
	document.getElementById("uploadedDocument").value="";
	document.getElementById("uploadedDocumentHidden").value="";
	document.getElementById("divUploadedDocument").style.display="none";
	document.getElementById("removeUploadedDocumentSpan").style.display="none";
	document.getElementById("uploadedDocument").value="";
	document.getElementById("documentName").focus();
	$('#documentName').css("background-color","");
	$('#uploadedDocument').css("background-color","");
	$('#errAdditionalDocuments').empty();
	return false;
}



function resetAdditionalDocumentsForm()
{
	document.getElementById("divAdditionalDocumentsRow").style.display="none";
	document.getElementById("divDocumnetDone").style.display="none";
	document.getElementById("additionDocumentId").value="";
	document.getElementById("documentName").value="";
	document.getElementById("uploadedDocument").value="";
	document.getElementById("uploadedDocumentHidden").value="";
	document.getElementById("divUploadedDocument").style.display="none";
	document.getElementById("removeUploadedDocumentSpan").style.display="none";
	document.getElementById("additionalDocumentsForm").reset();
	return false;
}
function showGridAdditionalDocuments()
{
	PFCertifications.getAdditionalDocumentsGrid(dp_AdditionalDocuments_noOfRows,dp_AdditionalDocuments_page,dp_AdditionalDocuments_sortOrderStr,dp_AdditionalDocuments_sortOrderType,{  
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#divGridAdditionalDocuments').html(data);
			applyScrollOnTbl_AdditionalDocuments();
		}});
}
function insertOrUpdate_AdditionalDocuments(callStatus)
{
		finalCallStatus = callStatus;
	
		var additionDocumentId=document.getElementById("additionDocumentId").value;
		$('#errAdditionalDocuments').empty();
		var cnt=0;
		var documentName = document.getElementById("documentName").value;
		if(trim(documentName)==""){
			$('#errAdditionalDocuments').append("&#149; "+resourceJSON.msgdocumentname+"</BR>");
			$('#documentName').css("background-color",txtBgColor);
			cnt++;
		}
		var uploadedDocument = document.getElementById("uploadedDocument").value;
		var uploadedDocumentHidden = document.getElementById("uploadedDocumentHidden").value;
		if(trim(uploadedDocument)=="" && trim(uploadedDocumentHidden)==""){
			$('#errAdditionalDocuments').append("&#149; "+resourceJSON.msguploaddocument+"</BR>");
			$('#uploadedDocument').css("background-color",txtBgColor);
			cnt++;
		}else{
			if(trim(uploadedDocument)!=""){
				var ext = uploadedDocument.substr(uploadedDocument.lastIndexOf('.') + 1).toLowerCase();	
				
				var fileSize = 0;
				if ($.browser.msie==true)
			 	{	
				    fileSize = 0;	   
				}
				else
				{
					if(document.getElementById("uploadedDocument").files[0]!=undefined)
					{
						fileSize = document.getElementById("uploadedDocument").files[0].size;
					}
				}
				
				if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt'))
				{
					$('#errAdditionalDocuments').append("&#149; "+resourceJSON.msgacceptabledocformate+"</BR>");
					$('#uploadedDocument').css("background-color",txtBgColor);
					cnt++;
				}else if(fileSize>=10485760){
					$('#errAdditionalDocuments').append("&#149; "+resourceJSON.msgDocSize10mb+"<br>");
					$('#uploadedDocument').css("background-color",txtBgColor);
					cnt++;
				}
			}
		}
			
		if(cnt!=0)		
		{
			//updateReturnThreadCount("adddoc");
			$('#errAdditionalDocuments').show();
			return false;
		}else if(cnt==0){
			if(trim(uploadedDocument)!=""){
				try{
					chkForALLDiv=1;
					document.getElementById("additionalDocumentsForm").submit();
				}catch(err){}
			}else{
				PFCertifications.saveOrUpdateAdditionalDocuments(additionDocumentId,documentName,null,uploadedDocumentHidden,
				{ 
				async: false,
				errorHandler:handleError,
				callback: function(data)
					{	
						showGridAdditionalDocuments()		
						resetAdditionalDocumentsForm();
					}
				});
			}
			return true;
		}
}

function saveAdditionalDocuments(fileName)
{
	//alert("fileName "+fileName);
	if(fileName!="")
	{
		var documentName = document.getElementById("documentName").value;
		var uploadedDocumentHidden = document.getElementById("uploadedDocumentHidden").value;
		var additionDocumentId=document.getElementById("additionDocumentId").value;
		
		PFCertifications.saveOrUpdateAdditionalDocuments(additionDocumentId,documentName,fileName,uploadedDocumentHidden,
		{ 
		async: false,
		errorHandler:handleError,
		callback: function(data)
			{	
				showGridAdditionalDocuments()		
				resetAdditionalDocumentsForm();
				if(finalCallStatus==1)
				{
					window.location.href="experiences.do";
				}
					
			}
		});
	}
	return false;
}

function showEditTeacherAdditionalDocuments(additionDocumentId)
{
	$('#errAdditionalDocuments').empty();
	$('#documentName').css("background-color","");
	$('#uploadedDocument').css("background-color","");
	
	document.getElementById("additionalDocumentsForm").reset();	
	PFCertifications.showEditTeacherAdditionalDocuments(additionDocumentId,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
	{
		document.getElementById("divAdditionalDocumentsRow").style.display="block";
		document.getElementById("divDocumnetDone").style.display="block";
		
		document.getElementById("additionDocumentId").value=data.additionDocumentId;
		document.getElementById("documentName").value=data.documentName;
		/*try{
			document.getElementById("uploadedDocument").value=data.uploadedDocument;
		}catch(err){}*/
		try{
			document.getElementById("uploadedDocumentHidden").value=data.uploadedDocument;
		}catch(err){}
		
		
		//alert(data.pathOfTranscript)
		if(data.uploadedDocument!=null && data.uploadedDocument!="")
		{	
			document.getElementById("removeUploadedDocumentSpan").style.display="inline";
			document.getElementById("divUploadedDocument").style.display="inline";
			document.getElementById("divUploadedDocument").innerHTML="<a href='javascript:void(0)' id='hrefAdditionalDocuments' onclick=\"downloadUploadedDocument('"+data.additionDocumentId+"','hrefAdditionalDocuments');" +
			"if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0');" +
			"return false;\">"+data.uploadedDocument+"</a>";
		}
		else
		{
			document.getElementById("divUploadedDocument").style.display="none";
			document.getElementById("removeUploadedDocumentSpan").style.display="none";
			document.getElementById("uploadedDocument").value="";
		}
		document.getElementById("documentName").focus();
		
	}})
	return false;
}

function delRow_Document(id)
{	
	document.getElementById("additionDocumentId").value=id;
	$('#removeUploadedDocument').modal('show');
}

function removeUploadedDocument(){
	
	var additionDocumentId=document.getElementById("additionDocumentId").value;
	PFCertifications.removeUploadedDocument(additionDocumentId,{
	async: true,
	errorHandler:handleError,
	callback:function(data)
	{
		$('#removeUploadedDocument').modal('hide');
		showGridAdditionalDocuments();
		resetAdditionalDocumentsForm();
		return false;
	}});
}
function downloadUploadedDocument(additionDocumentId, linkId)
{		
		PFCertifications.downloadUploadedDocument(additionDocumentId,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
			if(data=="")
			{
				data="javascript:void(0)";
			}
			else if(data.indexOf(".doc")!=-1)
			{
				document.getElementById("ifrmRef").src=data;
			}else
			{
				document.getElementById(linkId).href = data;
			}
			return false;
		}});
}
function clearUploadedDocument()
{
	document.getElementById("uploadedDocument").value=null;
	document.getElementById("uploadedDocument").reset();
	$('#uploadedDocument').css("background-color","");	
}
function setGridNameFlag(gridName)
{
	document.getElementById("gridNameFlag").value=gridName;
}
function chkForAdditionalDocuments(evt)
{	
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==13)
	{
		insertOrUpdate_AdditionalDocuments(1);
	}	
}

//======================= End :: for additional document

function chkNonTeacher(){
	if(document.getElementById("isNonTeacher").checked){		
	document.getElementById("expCertTeacherTraining").value="0.0";
	$(".expDiv").hide();
}else{
	document.getElementById("expCertTeacherTraining").value="";		
	$(".expDiv").show();
}
}

/*function chkNonTeacher(){
	if(document.getElementById("isNonTeacher").checked){		
		document.getElementById("expCertTeacherTraining").value="0.0";
		$(".expDiv").hide();
	}else{
		document.getElementById("expCertTeacherTraining").value="";		
		$(".expDiv").show();
	}
}*/

function fieldsDisable(value){
	if(value==5){
		$('#errordiv').empty();
		$( "#divMainForm" ).find( "span" ).css( "color", "white" );
		setDefColortoErrorMsg();
		document.getElementById("certificationtypeMaster").value=0;
		document.getElementById("stateMaster").value="";
		document.getElementById("yearReceived").value="";
		document.getElementById("yearexpires").value="";
		document.getElementById("certType").value="";
		document.getElementById("doenumber").value="";
		document.getElementById("certUrl").value="";
		document.getElementById("pathOfCertificationFile").value="";
		document.getElementById("pkOffered").checked=false;
		document.getElementById("kgOffered").checked=false;
		document.getElementById("g01Offered").checked=false;
		document.getElementById("g02Offered").checked=false;
		document.getElementById("g03Offered").checked=false;
		document.getElementById("g04Offered").checked=false;
		document.getElementById("g05Offered").checked=false;
		document.getElementById("g06Offered").checked=false;
		document.getElementById("g07Offered").checked=false;
		document.getElementById("g08Offered").checked=false;
		document.getElementById("g09Offered").checked=false;
		document.getElementById("g10Offered").checked=false;
		document.getElementById("g11Offered").checked=false;
		document.getElementById("g12Offered").checked=false;
		document.getElementById("clearLink").style.display="none";
		getPraxis();
		
		document.getElementById("certificationtypeMaster").disabled=true;
		document.getElementById("stateMaster").disabled=true;		
		document.getElementById("yearReceived").disabled=true;
		document.getElementById("yearexpires").disabled=true;
		document.getElementById("certType").disabled=true;
		document.getElementById("doenumber").disabled=true;
		document.getElementById("certUrl").disabled=true;
		document.getElementById("pathOfCertificationFile").disabled=true;
		document.getElementById("pkOffered").disabled=true;
		document.getElementById("kgOffered").disabled=true;
		document.getElementById("g01Offered").disabled=true;
		document.getElementById("g02Offered").disabled=true;
		document.getElementById("g03Offered").disabled=true;
		document.getElementById("g04Offered").disabled=true;
		document.getElementById("g05Offered").disabled=true;
		document.getElementById("g06Offered").disabled=true;
		document.getElementById("g07Offered").disabled=true;
		document.getElementById("g08Offered").disabled=true;
		document.getElementById("g09Offered").disabled=true;
		document.getElementById("g10Offered").disabled=true;
		document.getElementById("g11Offered").disabled=true;
		document.getElementById("g12Offered").disabled=true;
		document.getElementById("clearLink").style.display="none";
		
	}else{
		$( "#divMainForm" ).find( "span" ).css( "color", "red" );
		document.getElementById("clearLink").style.display="block";
		document.getElementById("certificationtypeMaster").disabled=false;
		document.getElementById("stateMaster").disabled=false;		
		document.getElementById("yearReceived").disabled=false;
		document.getElementById("yearexpires").disabled=false;
		document.getElementById("certType").disabled=false;
		document.getElementById("doenumber").disabled=false;
		document.getElementById("certUrl").disabled=false;
		document.getElementById("pathOfCertificationFile").disabled=false;
		document.getElementById("pkOffered").disabled=false;
		document.getElementById("kgOffered").disabled=false;
		document.getElementById("g01Offered").disabled=false;
		document.getElementById("g02Offered").disabled=false;
		document.getElementById("g03Offered").disabled=false;
		document.getElementById("g04Offered").disabled=false;
		document.getElementById("g05Offered").disabled=false;
		document.getElementById("g06Offered").disabled=false;
		document.getElementById("g07Offered").disabled=false;
		document.getElementById("g08Offered").disabled=false;
		document.getElementById("g09Offered").disabled=false;
		document.getElementById("g10Offered").disabled=false;
		document.getElementById("g11Offered").disabled=false;
		document.getElementById("g12Offered").disabled=false;
	}
}
function getVideoPlay(videoLinkId)
{
	$('#videovDiv').modal('show');
	$('#loadingDivWaitVideo').show();
	$('#interVideoDiv').empty();
	var videoPath='';		
	PFCertifications.getVideoPlayDiv(videoLinkId, { 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
		$('#loadingDivWaitVideo').hide();
		if(data!=""&& data!=null)
		{			
			var videoPath="";				
			videoPath="<video poster='images/video_back.png' src="+data+" width='480' height='280' controls autoplay><source src="+data+" type='video/ogg'>Your browser does not support the video tag.<br>Please Download <a target='_blank' href='https://www.apple.com/in/quicktime/download/'>QuickTime Player</a><br>Restart your Safari browser. </video>";
			$('#interVideoDiv').append(videoPath);			
		}
		else
		{
			$('#interVideoDiv').css("color","Red");
			$('#interVideoDiv').append(resourceJSON.msgProblemoccured);
		}
		}});
}