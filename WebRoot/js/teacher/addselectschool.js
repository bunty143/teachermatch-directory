var xmlHttp=null;
var deviceTypeAndroid=$.browser.device = (/Android|webOS|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent.toLowerCase()));
var deviceType=$.browser.device = (/iPhone|iPad|iPod/i.test(navigator.userAgent.toLowerCase()));
var page = 1;
var noOfRows = 10;
var sortOrderStr	=	"";
var sortOrderType	=	"";
function trim(s)
{
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}
function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert(resourceJSON.alertServerError + ": "+exception.javaClassName);}
}

function defaultSet(){
	
	 page = 10;
	 noOfRows = 2;
	 sortOrderStr="";
	 sortOrderType="";
}


function getPaging(pageno)
{	
	if(pageno!='')
	{
		page=pageno;	
	}
	else
	{
		page=1;
	}
	noOfRows = document.getElementById("pageSize").value;
	getSelectedSchoolsByJob();
	
}
function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{	
	if(pageno!=''){
		page=pageno;	
	}else{
		page=1;
	}
	sortOrderStr	=	sortOrder;
	sortOrderType	=	sortOrderTyp;
	if(document.getElementById("pageSize")!=null){
		noOfRows = document.getElementById("pageSize").value;
	}else{
		noOfRows=10;
	}
	getSelectedSchoolsByJob();
}
function checkSession(responseText)
{
	if(responseText=='100001')
	{
		alert(resourceJSON.oops)
		window.location.href="signin.do";
	}	
}
function createXMLHttpRequest()
{
	if (window.ActiveXObject)
		xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
	else if (window.XMLHttpRequest)
		xmlHttp = new XMLHttpRequest();
	else
		alert(resourceJSON.msgAjaxNotSupported);
}
function addAndEditSchool(){
	var jobId=document.getElementById("jobId").value;
	$('#loadingDiv').show();
	SchoolSelectionAjax.getSelectedSchools(jobId,
	{ 
		async: true,
		errorHandler:handleError,
		callback: function(data)
		{
			if(data!=null && data!="")
			{
				document.getElementById("1stSchoolDiv").innerHTML=data;
				 var isTeacher=$('[name="isTeacher"]').val();
                 if(isTeacher==1){
                	 	$('#letterOfIntentForm .required').html('');
                 }else{
                       $('#letterOfIntentForm .required').html('*');
                 }
                 $('#loadingDiv').hide();
			}
			else
			{
				$('#loadingDiv').hide();
			}
		}
	});	
	document.getElementById("selectedSchoolDiv").style.display="inline";
	document.getElementById("schoolIdForletterOfIntent").value="";
	document.getElementById("letterOfIntent").value=null;
}
function saveSelectedSchools(){
	$('#errselectschool').empty();
	var schoolId=document.getElementById("schoolIdForletterOfIntent").value;
	$('#errselectschool').show();
	var cnt=0;
	if(schoolId==""){
		$('#errselectschool').append("&#149; "+resourceJSON.msgPleaseselectSchool+"</br>");
		cnt++;
	}
	 if($('[name="isTeacher"]').val()!=1)
     {

		var letterOfIntent = document.getElementById("letterOfIntent").value;
		if(trim(letterOfIntent)==""){
			$('#errselectschool').append("&#149; "+resourceJSON.msguploadLetterIntent+"</BR>");
			$('#letterOfIntent').css("background-color",txtBgColor);
			cnt++;
		}else{
			if(trim(letterOfIntent)!=""){
				var ext = letterOfIntent.substr(letterOfIntent.lastIndexOf('.') + 1).toLowerCase();	
				var fileSize = 0;
				if ($.browser.msie==true)
			 	{	
				    fileSize = 0;	   
				}
				else
				{
					if(document.getElementById("letterOfIntent").files[0]!=undefined)
					{
						fileSize = document.getElementById("letterOfIntent").files[0].size;
					}
				}
				
				if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt'))
				{
					$('#errselectschool').append("&#149; "+resourceJSON.msgAcceptableIntentFormat+"</BR>");
					$('#letterOfIntent').css("background-color",txtBgColor);
					cnt++;
				}else if(fileSize>=10485760){
					$('#errselectschool').append("&#149; "+resourceJSON.msgDocSize10mb+"<br>");
				$('#letterOfIntent').css("background-color",txtBgColor);
				cnt++;
			}
		}
	}
     }
	if(cnt==0){
		document.getElementById("selectedSchoolDiv").style.display="none";
		var jobId=document.getElementById("jobId").value;
		try{
			document.getElementById("letterOfIntentForm").submit();
		}catch(err){}
	}
}
function saveSelectedSchoolsByServlet(fileName){
	var jobId=document.getElementById("jobId").value;
	var schoolId=document.getElementById("schoolIdForletterOfIntent").value;
	$('#loadingDiv').hide();
	SchoolSelectionAjax.saveSelectedSchools(jobId,schoolId,fileName,
	{ 
		async: true,
		errorHandler:handleError,
		callback: function(data)
		{
			getSelectedSchoolsByJob();
		}
	});	
	document.getElementById("selectedSchoolDiv").style.display="inline";
}
function cancelSchool(){
	document.getElementById("selectedSchoolDiv").style.display="none";
}
function clearletterOfIntent()
{
	document.getElementById("letterOfIntent").value=null;
	document.getElementById("letterOfIntent").reset();
	$('#letterOfIntent').css("background-color","");	
}

function getSelectedSchoolsByJob()
{	
	document.getElementById("selectedSchoolDiv").style.display="none";
	var jobId=document.getElementById("jobId").value;
	$('#loadingDiv').show();
	SchoolSelectionAjax.getSelectedSchoolsByJob(jobId,noOfRows,page,sortOrderStr,sortOrderType,
	{ 
		async: true,
		errorHandler:handleError,
		callback: function(data)
		{
			//alert('data:'+data);
			if(data!=null && data!="")
			{
				document.getElementById("divSelectedSchoolList").innerHTML=data;
				applySelectedScrolOnTbl();				
				$('#loadingDiv').hide();
			}
			else
			{
				$('#loadingDiv').hide();
			}
		}
	});	
}
function removeSchool(jobId,schoolId){
	var remCheck=confirm("Are you sure to remove School?");
	if(remCheck){
		var jobId=document.getElementById("jobId").value;
		$('#loadingDiv').show();
		SchoolSelectionAjax.removeSelectedSchool(jobId,schoolId,
		{ 
			async: true,
			errorHandler:handleError,
			callback: function(data)
			{
				$('#loadingDiv').hide();
				getSelectedSchoolsByJob();
			}
		});	
	}
}
function setSchoolId(schoolId) {
	document.getElementById("schoolIdForletterOfIntent").value=schoolId.value;
}
function clearletterOfIntent()
{
	document.getElementById("letterOfIntent").value=null;
	document.getElementById("letterOfIntent").reset();
	$('#letterOfIntent').css("background-color","");	
}
function showLetterOfIntent(jobId,schoolId,linkId){
	SchoolSelectionAjax.showLetterOfIntent(jobId,schoolId,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#loadingDiv').hide();
			if(data=="")
			{			
				alert(resourceJSON.msgLetterNotUpld)
			}
			else
			{
				if(deviceType)
				{
					if (data.indexOf(".doc")!=-1)
					{
						$("#docfileNotOpen").css({"z-index":"3000"});
				    	$('#docfileNotOpen').show();
					}
					else if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
					{
						$("#exelfileNotOpen").css({"z-index":"3000"});
				    	$('#exelfileNotOpen').show();
					}
					else
					{
						document.getElementById(linkId).href = data; 	
					}
				}
				else if(deviceTypeAndroid)
				{
					if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
					{
						document.getElementById('ifrmletterOfIntent').src = ""+data+"";
					}
					else
					{
						document.getElementById(linkId).href = data; 	
					}	
				}
				else if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					document.getElementById('ifrmletterOfIntent').src = ""+data+"";
				}
				else
				{
					document.getElementById(linkId).href = data; 	
				}			
			}				
			return false;
		}
	});
}