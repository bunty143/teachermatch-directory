var xmlHttp=null;

var page = 1;
var noOfRows = 10;
var sortOrderStr	=	"";
var sortOrderType	=	"";

function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert(resourceJSON.msgServerErr+": "+exception.javaClassName);}
}

function defaultSet(){
	
	 page = 1;
	 noOfRows = 10;
	 sortOrderStr="";
	 sortOrderType="";
}


function getPaging(pageno)
{	
	if(pageno!='')
	{
		page=pageno;	
	}
	else
	{
		page=1;
	}
	noOfRows = document.getElementById("pageSize").value;
	getSelectedSchoolJobs();
	
}
function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{	
	if(pageno!=''){
		page=pageno;	
	}else{
		page=1;
	}
	sortOrderStr	=	sortOrder;
	sortOrderType	=	sortOrderTyp;
	if(document.getElementById("pageSize")!=null){
		noOfRows = document.getElementById("pageSize").value;
	}else{
		noOfRows=10;
	}
	getSelectedSchoolJobs();
}

function createXMLHttpRequest()
{
	if (window.ActiveXObject)
		xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
	else if (window.XMLHttpRequest)
		xmlHttp = new XMLHttpRequest();
	else
		alert(resourceJSON.msgAjaxNotSupported);
}
function getSelectedSchoolJobs()
{	
	$('#loadingDiv').show();
	SchoolSelectionAjax.getSelectedSchoolJobs(noOfRows,page,sortOrderStr,sortOrderType,
	{ 
		async: true,
		errorHandler:handleError,
		callback: function(data)
		{
			if(data!=null && data!=""){
				document.getElementById("divSelectedSchoolList").innerHTML=data;
				applySelectedScrolOnTbl();				
				$('#loadingDiv').hide();
			}else{
				$('#loadingDiv').hide();
			}
		}
	});	
}
