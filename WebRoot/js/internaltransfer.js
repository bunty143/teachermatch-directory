var xmlHttp=null;
function createXMLHttpRequest()
{
	if (window.ActiveXObject)
		xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
	else if (window.XMLHttpRequest)
		xmlHttp = new XMLHttpRequest();
	else
		alert(resourceJSON.msgAjaxNotSupported);
}
function setFocus(id)
{
	if(id!="")
		document.getElementById(id).focus();
}
var page = 1;
var noOfRows = 100;
var sortOrderStr="";
var sortOrderType="";
function getPaging(pageno)
{
	if(pageno!='')
	{
		page=pageno;	
	}
	else
	{
		page=1;
	}
	
	noOfRows = document.getElementById("pageSize").value;
	searchRecordsByEntityType();
}
function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
	if(pageno!=''){
		page=pageno;	
	}else{
		page=1;
	}
	sortOrderStr=sortOrder;
	sortOrderType=sortOrderTyp;
	if(document.getElementById("pageSize")!=null){
		noOfRows = document.getElementById("pageSize").value;
	}else{
		noOfRows=100;
	}
	searchRecordsByEntityType();
}

var count=0;
var index=-1;
var length=0;
var divid='';
var txtid='';
var hiddenDataArray = new Array();
var showDataArray = new Array();
var degreeTypeArray = new Array();
var editFlag=0;
function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert(resourceJSON.msgServerErr+": "+exception.javaClassName);}
}
function GetXMLHttp()
{
	var temp=null;
	if (window.ActiveXObject) 
		temp = new ActiveXObject("Microsoft.XMLHTTP");
	else if (window.XMLHttpRequest) 
		temp = new XMLHttpRequest();
	else
		alert(resourceJSON.msgAjaxNotSupported);
	return temp;   		
}

var autoHttpxmlRequest='';

//=========== In Case of final =====================
//For State Text Box

var hiddenId="";
function getDistrictMasterAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{	
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38)
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13)
	{
		
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		//searchArray = getUniversityArray(txtSearch.value);
		//fatchData(txtSearch,searchArray,txtId,txtdivid);
		document.getElementById("schoolName").focus();
		
	} 
	else if(event.keyCode==9)
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getDistrictArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getDistrictArray(districtName){
	
	var searchArray = new Array();
	DWRAutoComplete.getDistrictMasterList(districtName,{ 
		async: false,		
		callback: function(data){
		hiddenDataArray = new Array();
		showDataArray = new Array();
		for(i=0;i<data.length;i++){
			searchArray[i]=data[i].districtName;
			hiddenDataArray[i]=data[i].districtId;
			showDataArray[i]=data[i].districtName;
		}
	}
	});	

	return searchArray;
}

function hideDistrictMasterDiv(dis,hiddenId,divId)
{
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
		if(dis.value!="")
		{
			var cnt=0;
			var focs=0;	
			$('#errordiv').empty();	
			
			$('#errordiv').append("&#149; "+resourceJSON.msgvalidschool+"<br>");
			if(focs==0)
				$('#universityName').focus();
			
			$('#universityName').css("background-color",txtBgColor);
			cnt++;focs++;	
		}
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}
//----

function getSchoolMasterAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38)
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13)
	{
		
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		document.getElementById("jobCategoryId").focus();
		
	} 
	else if(event.keyCode==9)
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getSchoolArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getSchoolArray(schoolName){
	
	var districtId = document.getElementById("districtId").value;
	var searchArray = new Array();
	DWRAutoComplete.getSchoolMasterList(schoolName,districtId,{ 
		async: false,		
		callback: function(data){
		hiddenDataArray = new Array();
		showDataArray = new Array();
		for(i=0;i<data.length;i++){
			
			searchArray[i]=data[i].schoolMaster.schoolName;			
			hiddenDataArray[i]=data[i].schoolMaster.schoolId;
			showDataArray[i]=data[i].schoolMaster.schoolName;
		}
	}
	});	

	return searchArray;
}

function hideSchoolMasterDiv(dis,hiddenId,divId)
{
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
		if(dis.value!="")
		{
			var cnt=0;
			var focs=0;	
			$('#errordiv').empty();	
			$('#errordiv').append("&#149; "+resourceJSON.msgvalidschool+"<br>");
			if(focs==0)
				$('#universityName').focus();
			
			$('#universityName').css("background-color",txtBgColor);
			cnt++;focs++;	
		}
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}
//----

var selectFirst="";

var fatchData= function(txtSearch,searchArray,txtId,txtdivid){
	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i] + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
				
			}
			
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}

var downArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index<length-1){
			for(var i=0;i<10;i++){
				{
					if(document.getElementById('divResult'+txtdivid+i))
						var div_id=document.getElementById('divResult'+txtdivid+i);
					if(div_id)
					{
						if(div_id.className=='over')
							index=div_id.id.split('divResult'+txtdivid)[1];
						div_id.className='normal';
					}
				}
			}
			index++;
			if(document.getElementById('divResult'+txtdivid+index))
			{
				var div_id=document.getElementById('divResult'+txtdivid+index);
				div_id.className='over';
				document.getElementById(txtid).value = div_id.innerHTML;
				selectFirst=div_id.innerHTML;
			}
		}
	}
}

var upArrowKey = function(txtdivid){
	
	if(txtdivid)
	{
		if(index>0){
			for(var i=0;i<length;i++){

				var div_id=document.getElementById('divResult'+txtdivid+i);
				if(div_id)
				{
					if(div_id.className=='over')
						index=div_id.id.split('divResult'+txtdivid)[1];
					div_id.className='normal';
				}
			}
			index--;
			if(document.getElementById('divResult'+txtdivid+index))
				document.getElementById('divResult'+txtdivid+index).className='over';
			if(txtid && document.getElementById('divResult'+txtdivid+index))
			{
				document.getElementById(txtid).value=
					document.getElementById('divResult'+txtdivid+index).innerHTML;
				selectFirst=document.getElementById('divResult'+txtdivid+index).innerHTML;
			}
		}
	}
}

function mouseOverChk(txtdivid,txtboxId)
{	
	for(var i=0;i<length;i++)
	{
		$('#divResult'+txtdivid+i).mouseover({param1: i,param2:txtdivid, param3:txtboxId}, fireMouseOverEvent);
	}
}


function fireMouseOverEvent(event)
{
	for(var i=0;i<length;i++)
	{	
		document.getElementById('divResult'+event.data.param2+i).className='normal';
	}
    document.getElementById('divResult'+event.data.param2+event.data.param1).className='over';	       
   	document.getElementById(event.data.param3).value= $('#divResult'+event.data.param2+event.data.param1).text();
    index=event.data.param1;
}

function __mouseOverChk(txtdivid,txtboxId)
{	
	for(var i=0;i<length;i++)
	{
		document.getElementById('divResult'+txtdivid+i).className='normal';		
		if ($('#divResult'+txtdivid+i).is(':hover')) 
		{
			document.getElementById('divResult'+txtdivid+i).className='over';	       
	       	document.getElementById(txtboxId).value= $('#divResult'+txtdivid+i).text();
	        index=i;
	    }
	}

}

var overText = function (div_value,txtdivid) 
{
	for(var i=0;i<length;i++)
	{
		if(document.getElementById('divResult'+i))
		{
			var div_id=document.getElementById('divResult'+i);

			if(div_id.className=='over')
				index=div_id.id.split(txtdivid)[1];
			div_id.className='normal';
		}
	}
	div_value.className = 'over';
	document.getElementById(txtid).value= div_value.innerHTML;
}
function trim(s)
{
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}

/* @Start
 * @Ashish Kumar
 * @Description :: Display Selected Cities by state and Get All Sates
 * */
function selectCityByState()
{
	var stateId = $("#stateId").val();
	
			DWRAutoComplete.selectedCityByStateId(stateId,{ 
				async: false,		
				callback: function(data){
					document.getElementById("cityDivId").innerHTML=data;
				}
			});
}

function displayState(){

	DWRAutoComplete.displayStateData({ 
		async: false,		
		callback: function(data){
			document.getElementById("stateDivId").innerHTML=data;
		}
	});
}
/* @End
 * @Ashish Kumar
 * @Description :: Display Selected Cities by state and Get All Sates
 * */

function addCategory(){
	clearCategory();
	document.getElementById("categoryeditid").value="";
	document.getElementById("addCategoryDiv").style.display='inline';
}
function cancelCategory(){
	document.getElementById("category").value=0;
	document.getElementById("addCategoryDiv").style.display='none';
}
function validateCategory(){
	
	var errorCount=0;
	//$('#errorcategorydiv').empty();
	$('#errorinternaldiv').empty();
	var certificationDivCount=0;
	var categoryId=0;
	var category="";
	var categoryIds="";
	if(document.getElementById("showcategorydivvalue").value!=""){
		document.getElementById("onLoadCategoryDiv").style.display='none';
	}
	var categoryeditid=document.getElementById("categoryeditid").value;
	var categoryObj = document.getElementById("category");
	try{
		categoryId = categoryObj.options[categoryObj.selectedIndex].value;
		category = categoryObj.options[categoryObj.selectedIndex].text;
	}catch(err){}
	categoryIds=document.getElementById("categoryids").value;
	var teacherId=document.getElementById("teacherId").value;
	if(document.getElementById("addCategoryDiv").style.display=="inline"){
		
		if(categoryId==0){
			$('#errorinternaldiv').show();
			$('#errorinternaldiv').append("&#149; "+resourceJSON.msgPlZSelectJobCategory+"<br>");
			errorCount++;
		}
		if(errorCount==0 && teacherId!="" && categoryId!=0){
			if(checkCategory(teacherId,categoryId,1) && categoryId!=categoryeditid){
				$('#errorinternaldiv').show();
				$('#errorinternaldiv').append("&#149; "+resourceJSON.msgAlreadynotherJobCategory+"<br>");
				errorCount++;
			}
		}
		if(errorCount==0)
		if((categoryIds.indexOf("||"+categoryId+"||")!=-1 && editFlag==0) || (categoryIds.indexOf("||"+categoryId+"||")!=-1 && categoryIds.indexOf("||"+categoryeditid+"||")!=-1 &&  editFlag>0 && categoryId!=categoryeditid)){
			$('#errorinternaldiv').show();
			$('#errorinternaldiv').append("&#149; "+resourceJSON.msgAlreadynotherJobCategory+"<br>");
			errorCount++;
		}
	
		try{
			var subjectList="";
			var subjectIds="";
			var inputs = document.getElementsByName("subject");
			var counter=0;
			 for (var i = 0; i < inputs.length; i++) {
		        if (inputs[i].type === 'checkbox') {
		        	if(inputs[i].checked){
		        		var subjectVal=inputs[i].value;
		        		var subjectText=subjectVal.split("||");
		        		if(counter==1){
		        			subjectList+=", ";
		        			subjectIds+="-";
		        		}
		        		subjectIds+=subjectText[0];
		        		subjectList+=subjectText[1];
		        		counter=1;
		            }
		        }
			 }
		}catch(err){}
		if(subjectIds==""){
			$('#errorinternaldiv').show();
			$('#errorinternaldiv').append("&#149; "+resourceJSON.msgPlzAtleastSubject+"<br>");
			errorCount++;
		}
		if(errorCount==0){
			if(teacherId!=null && teacherId!=""){
				InternalTransferAjax.saveInternalJobCategory(teacherId,categoryId,subjectIds,categoryeditid,{
					async: true,
					callback: function(data){
						document.getElementById("addCategoryDiv").style.display='none';
						onLoadInternalPage();
					},
				});
			}else{
				document.getElementById("onLoadCategoryDiv").style.display='none';
				if(editFlag==0){
					document.getElementById("showCategoryDiv").innerHTML="";
					if(document.getElementById("showcategorydivvalue").value==""){
						document.getElementById("showcategorydivvalue").value="<table  id='categoryTable' width='100%' border='0' ><thead class='bg'><tr><th width='55%' valign='top'>"+resourceJSON.msgCategory1+"</th><th width='15%' valign='top'>"+resourceJSON.msgSubject1+"</th><th width='15%'>"+resourceJSON.msgActions+"</th></tr></thead>";
						document.getElementById("showcategorydivvalue").value+="<tr id="+categoryId+"><td>"+category+"</td><td>"+subjectList+"</td><td><a href='javascript:void(0);' onclick=\"return editCategory("+categoryId+",'"+subjectIds+"')\">"+resourceJSON.msgEdit1+"</a>|<a href='javascript:void(0);' onclick='return deleteCategory("+categoryId+")'>Delete</a></td></tr></table>";
					}else{
						var store=document.getElementById("showcategorydivvalue").value;
						var storeFilter=store.replace("</table>","");
						document.getElementById("showcategorydivvalue").value=storeFilter+"<tr id="+categoryId+"><td>"+category+"</td><td>"+subjectList+"</td><td><a href='javascript:void(0);' onclick=\"return editCategory("+categoryId+",'"+subjectIds+"')\">"+resourceJSON.msgEdit1+"</a>|<a href='javascript:void(0);' onclick='return deleteCategory("+categoryId+")'>"+resourceJSON.msgDelete+"</a></td></tr></table>";
					}
					
					document.getElementById("showCategoryDiv").innerHTML=document.getElementById("showcategorydivvalue").value;
					document.getElementById("addCategoryDiv").style.display='none';
					applyCategoryOnTbl();
					document.getElementById("categoryids").value+="||"+categoryId+"||";
					clearCategory();
					$('#errorinternaldiv').empty();
				}else{
					try{
						var divValue=document.getElementById("showcategorydivvalue").value;
						var headValue=divValue.substring(0,198);
						var allTrValue=divValue.replace(headValue,"");
						var finalValue="";
						var categoryDivId="";
						var upadteCategoryIds="";
						try{
							for (var i = -1; (i = allTrValue.indexOf("<tr id=", i + 1)) != -1; ) {
								var trValue="";
								trValue=allTrValue.substring(allTrValue.indexOf("id=")-4);
								trValue=trValue.substring(0,trValue.indexOf("</tr>")+5);
								allTrValue = allTrValue.substring(allTrValue.indexOf("id=")+3);
								categoryDivId= allTrValue.substring(0,allTrValue.indexOf(">"));
								if(categoryId==categoryDivId){
									var editSubjectValue1=trValue.substring(trValue.indexOf("</td><td>")+5);
									var editSubjectValue2=editSubjectValue1.substring(0,editSubjectValue1.indexOf("</td><td>")+5);
									var editSubjectValue3=trValue.replace(editSubjectValue2,"<td>"+subjectList+"</td>");
									var editSubjectValue4=editSubjectValue3.substring(editSubjectValue3.indexOf("editCategory(")+12);
									editSubjectValue4=editSubjectValue4.substring(editSubjectValue4.indexOf("'"));
									
									var editSubjectValue5=editSubjectValue4.substring(0,editSubjectValue4.indexOf(")"));
									var editSubjectValue6=editSubjectValue3.replace(editSubjectValue5,"'"+subjectIds+"'");
									finalValue=divValue.replace(trValue,editSubjectValue6);
								}else if(categoryeditid==categoryDivId){
									if(categoryIds!=null && categoryIds!=""){
										upadteCategoryIds=categoryIds.replace("||"+categoryeditid+"||","||"+categoryId+"||");
									}
									var editVal1=trValue.replace("id="+categoryeditid+">","id="+categoryId+">");
									var editVal2=editVal1.replace("editCategory("+categoryeditid+",","editCategory("+categoryId+",");
									var editVal3=editVal2.replace("deleteCategory("+categoryeditid+")","deleteCategory("+categoryId+")");
									
									var editVal4=editVal3.substring(editVal3.indexOf("<td>"));
									var editVal5=editVal4.substring(0,editVal4.indexOf("</td>")+5);
									var editVal6=editVal3.replace(editVal5,"<td>"+category+"</td>");
										
									var editSubjectValue1=editVal6.substring(editVal6.indexOf("</td><td>")+5);
									var editSubjectValue2=editSubjectValue1.substring(0,editSubjectValue1.indexOf("</td><td>")+5);
									var editSubjectValue3=editVal6.replace(editSubjectValue2,"<td>"+subjectList+"</td>");
									var editSubjectValue4=editSubjectValue3.substring(editSubjectValue3.indexOf("editCategory(")+12);
									editSubjectValue4=editSubjectValue4.substring(editSubjectValue4.indexOf("'"));
									
									var editSubjectValue5=editSubjectValue4.substring(0,editSubjectValue4.indexOf(")"));
									var editSubjectValue6=editSubjectValue3.replace(editSubjectValue5,"'"+subjectIds+"'");
									finalValue=divValue.replace(trValue,editSubjectValue6);
								}
							}
						}catch(err){}
						if(upadteCategoryIds!="")
						document.getElementById("categoryids").value=upadteCategoryIds;
						document.getElementById("showcategorydivvalue").value=finalValue;
						document.getElementById("showCategoryDiv").innerHTML=document.getElementById("showcategorydivvalue").value;
						document.getElementById("addCategoryDiv").style.display='none';
						applyCategoryOnTbl();
						clearCategory();
						editFlag=0;
						$('#errorinternaldiv').empty();
					}catch(err){}
				}
			}
		}//Close errorCount
	}
	return errorCount;
}

function clearCategory()
{
	document.getElementById("category").value=0;
	var inputs = document.getElementsByName("subject");
	for (var i = 0; i < inputs.length; i++) {
		 if (inputs[i].type == 'checkbox') {
			 inputs[i].checked=false;
         }
	} 
	document.getElementById("addCategoryDiv").style.display='none';
}
function editCategory(categoryId,subjectIds){
	clearCategory();
	document.getElementById("category").value=categoryId;
	document.getElementById("categoryeditid").value=categoryId;
	
	editFlag=categoryId;
	var subjectIdArr=subjectIds.split("-");
	for (var i=0;i<subjectIdArr.length; i++) {
		document.getElementById("subject"+subjectIdArr[i]).checked=true;
	} 
	document.getElementById("addCategoryDiv").style.display='inline';
}
function deleteCategory(categoryId){
	if (confirm(resourceJSON.msgWouldLikeDeleteJobCategory)) {
		try{
			var teacherId=document.getElementById("teacherId").value;
			if(teacherId!=null && teacherId!=""){
				InternalTransferAjax.deleteInternalJobCategory(teacherId,categoryId,{
					async: true,
					callback: function(data){
						//document.getElementById("addCategoryDiv").style.display='none';
						onLoadInternalPage();
					},
				});
			}else{
				var categoryIds=document.getElementById("categoryids").value;
				if(categoryIds!=null && categoryIds!=""){
					document.getElementById("categoryids").value=categoryIds.replace("||"+categoryId+"||","");
				}
				var divValue=document.getElementById("showcategorydivvalue").value;
				var fVal=divValue.substring(0,198);
				var rVal=divValue.replace(fVal,"");
				var fVal="";
				var categoryDivId="";
				var counter=0;
				for (var i = -1; (i = rVal.indexOf("<tr id=", i + 1)) != -1; ) {
					counter++;
					var tVal="";
					tVal=rVal.substring(rVal.indexOf("id=")-4);
					tVal=tVal.substring(0,tVal.indexOf("</tr>")+5);
					rVal = rVal.substring(rVal.indexOf("id=")+3);
					categoryDivId= rVal.substring(0,rVal.indexOf(">"));
					if(categoryId==categoryDivId){
						fVal=divValue.replace(tVal,"");
					}
				}
				document.getElementById("showcategorydivvalue").value=fVal;
				var row=document.getElementById(categoryId);
				$(row).remove();
				if(counter==1){
					document.getElementById("showcategorydivvalue").value="";
					document.getElementById("showCategoryDiv").innerHTML="";
					onLoadInternalPage();
					applyCategoryOnTbl();
				}
			}
		}catch(err){}
	}
}


function validateInternalTransfer(){
	var errorCount=0;
	$('#errorinternaldiv').empty();
	$('#errorcategorydiv').empty();
	var certificationDivCount=0;
	var categoryId=0;
	var category="";
	var categoryIds="";
	var categoryeditid=document.getElementById("categoryeditid").value;
	var categoryObj = document.getElementById("category");
	try{
		categoryId = categoryObj.options[categoryObj.selectedIndex].value;
		category = categoryObj.options[categoryObj.selectedIndex].text;
	}catch(err){}
	categoryIds=document.getElementById("categoryids").value;
	
	var districtId=trim(document.getElementById("districtId").value);
	var schoolId=trim(document.getElementById("schoolId").value);
	var firstName=trim(document.getElementById("firstName").value);
	var lastName=trim(document.getElementById("lastName").value);
	var email=trim(document.getElementById("email").value);
	var phone=trim(document.getElementById("phone").value);
	
	var districtDomains=document.getElementById("districtDomains").value;
	
	var districtDName=trim(document.getElementById("districtDName").value);
	
	var schoolName=trim(document.getElementById("schoolName").value);
	var teacherId=document.getElementById("teacherId").value;
	
	var optEmails=true;
	if(districtId==null || districtId==""){
		$('#errorcategorydiv').show();
		$('#errorcategorydiv').append("&#149; "+resourceJSON.PlzEtrDistrict+"<br>");
		errorCount++;
	}
	/*if(schoolName=null || schoolName==""){
		$('#errorcategorydiv').show();
		$('#errorcategorydiv').append("&#149; Please enter School.<br>");
		if(errorCount==0)
			$('#schoolName').focus();
		
		$('#schoolName').css("background-color", "#F5E7E1");
		errorCount++;
	}else */if((schoolId==null || schoolId=="") && schoolName!=null && schoolName!=""){
		$('#errorcategorydiv').show();
		$('#errorcategorydiv').append("&#149; "+resourceJSON.msgvalidschool+".<br>");
		if(errorCount==0)
			$('#schoolName').focus();
		
		$('#schoolName').css("background-color", "#F5E7E1");
		errorCount++;
	}
		
	if(firstName==null || firstName==""){
		$('#errorcategorydiv').show();
		$('#errorcategorydiv').append("&#149; "+resourceJSON.PlzEtrFirstName+"<br>");
		if(errorCount==0)
			$('#firstName').focus();
		
		$('#firstName').css("background-color", "#F5E7E1");
		errorCount++;
	}
	if(lastName==null || lastName==""){
		$('#errorcategorydiv').show();
		$('#errorcategorydiv').append("&#149; "+resourceJSON.PlzEtrLastName+"<br>");
		if(errorCount==0)
			$('#lastName').focus();
		
		$('#lastName').css("background-color", "#F5E7E1");
		errorCount++;
	}
	if(email==null || email==""){
		$('#errorcategorydiv').show();
		$('#errorcategorydiv').append("&#149; "+resourceJSON.PlzEtrEmail+"<br>");
		if(errorCount==0)
			$('#email').focus();
		
		$('#email').css("background-color", "#F5E7E1");
		errorCount++;
	}else if(!isEmailAddress(email)){		
		$('#errorcategorydiv').append("&#149; "+resourceJSON.PlzEtrVldEmail+"<br>");
		if(errorCount==0)
			$('#email').focus();
		
		$('#email').css("background-color", "#F5E7E1");
		errorCount++;
	}else{
		var emailDomainChk=false;
		if(districtDomains!=null && districtDomains!=""){
			var emailDomain=email.split("@");
			if(districtDomains.indexOf("@"+emailDomain[1]+"@")!=-1){
				emailDomainChk=true;
			}
		}
		if(emailDomainChk==false){
			$('#errorcategorydiv').append("&#149; "+resourceJSON.msgRegisterInternalCandidate+"<br>");
			if(errorCount==0)
				$('#email').focus();
			
			$('#email').css("background-color", "#F5E7E1");
			errorCount++;
		}else{
			if(checkUserEmail(email)){
				$('#errorcategorydiv').append("&#149; "+resourceJSON.msgAlreadyregisteredTeacherMatch+"<br>");
				if(errorCount==0)
					$('#email').focus();
				
				$('#email').css("background-color", "#F5E7E1");
				errorCount++;
			}else if(chkInternalTeacher(email)){
				$('#errorcategorydiv').append("&#149; "+resourceJSON.msgRegisteredInternalCandidate+"<br>");
				if(errorCount==0)
					$('#email').focus();
				
				$('#email').css("background-color", "#F5E7E1");
				errorCount++;
			}
		}
	}
	
	if(phone==null || phone==""){
		$('#errorcategorydiv').show();
		$('#errorcategorydiv').append("&#149; "+resourceJSON.msgPhoneNumber+"<br>");
		if(errorCount==0)
			$('#phone').focus();
		
		$('#phone').css("background-color", "#F5E7E1");
		errorCount++;
	}
	var categoryError=validateCategory();
	if(categoryError==0 && document.getElementById("addCategoryDiv").style.display!='inline'){
		if(teacherId!=null && teacherId!='' && !checkCategory(teacherId,categoryId,0)){
			$('#errorcategorydiv').show();
			$('#errorcategorydiv').append("&#149; "+resourceJSON.msgAddatleastoneJobCategory+"<br>");
			errorCount++;
		}else if(teacherId==null || teacherId==''){
			categoryIds=document.getElementById("categoryids").value
			if(categoryIds==null || categoryIds==""){
				$('#errorcategorydiv').show();
				$('#errorcategorydiv').append("&#149; "+resourceJSON.msgAddatleastoneJobCategory+"<br>");
				errorCount++;
			}
		}
	}else{
		errorCount=categoryError;
	}
	var categorydivvalue=document.getElementById("showcategorydivvalue").value;
	if(errorCount==0){
		$('#loadingDiv').show();
		InternalTransferAjax.saveInternalCandidate(optEmails,teacherId,districtId,schoolId,firstName,lastName,email,phone,categorydivvalue,{
			async: true,
			callback: function(data){
			$('#loadingDiv').hide();
				if(data=="0"){
					$('#interMessage').html(resourceJSON.msgThankYouforRegistering);
					$('#InterMessageDiv').modal('show');
				}else if(data=="2"){
					$('#interMessage').html(resourceJSON.msgDetailshavebeenSavedSuccessfully);
					$('#InterMessageDiv').modal('show');
				}else{
					$('#interMessage').html(""+resourceJSON.msgTeacherMatchInternalCandidate+"</br>"+resourceJSON.msgEmailWithLoginDetails+"</br>"+resourceJSON.msgCheckYourEmailAuthenticate);
					$('#InterMessageDiv').modal('show');
				}
			},
		});
	}
}
function closeMessage(){
	var teacherId=document.getElementById("teacherId").value;
	if(teacherId!=null && teacherId!=''){
		window.location.href="userdashboard.do";
	}else{
		window.location.href="signin.do";
	}
}
function trim(s)
{
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}
function isEmailAddress(str) 
{	
	var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
	return emailPattern.test(str);
}

function checkUserEmail(emailAddress){
	var res;
	createXMLHttpRequest();  
	queryString = "chkUserEmail.do?emailAddress="+emailAddress+"&dt="+new Date().getTime();
	xmlHttp.open("POST", queryString, false);
	xmlHttp.onreadystatechange = function(){
		if(xmlHttp.readyState == 4){
			if(xmlHttp.status==200){			
				res = xmlHttp.responseText;				
			}else{
				alert(resourceJSON.msgServerErr);
			}
		}
	}
	xmlHttp.send(null);		
	if(res==1)
		return true;
	else
		return false;
}

function checkCategory(teacherId,categoryId,categoryFlag){
	var res;
	createXMLHttpRequest();  
	queryString = "chkCategory.do?categoryId="+categoryId+"&teacherId="+teacherId+"&categoryFlag="+categoryFlag+"&dt="+new Date().getTime();
	xmlHttp.open("POST", queryString, false);
	xmlHttp.onreadystatechange = function(){
		if(xmlHttp.readyState == 4){
			if(xmlHttp.status==200){			
				res = xmlHttp.responseText;				
			}else{
				alert(resourceJSON.msgServerErr);
			}
		}
	}
	xmlHttp.send(null);		
	if(res==1)
		return true;
	else
		return false;
}


function chkInternalTeacher(emailAddress){
	var teacherId=document.getElementById("teacherId").value;
	var res;
	createXMLHttpRequest();  
	queryString = "chkInternalTeacher.do?emailAddress="+emailAddress+"&teacherId="+teacherId+"&dt="+new Date().getTime();
	xmlHttp.open("POST", queryString, false);
	xmlHttp.onreadystatechange = function(){
		if(xmlHttp.readyState == 4){
			if(xmlHttp.status==200){			
				res = xmlHttp.responseText;				
			}else{
				alert(resourceJSON.msgServerErr);
			}
		}
	}
	xmlHttp.send(null);		
	if(res==1)
		return true;
	else
		return false;
}

function chkInternalCandidate(email){
	
	var msgFlag=false;
	if(trim(email.value)!=""){
		if(chkInternalTeacher(email.value)){
			$('#messageEmail').html(resourceJSON.msgRegisteredInternalCandidate);
			$('#myModalEmail').modal('show');
			msgFlag=true;
		}
		if(msgFlag==false){
			chkDomainEmail(email.value);
		}
	}
}

function chkDomainEmail(email){
	var districtDomains=document.getElementById("districtDomains").value;
	var emailDomainChk=false;
	if(districtDomains!=null && districtDomains!=""){
		var emailDomain=email.split("@");
		if(districtDomains.indexOf("@"+emailDomain[1]+"@")!=-1){
			emailDomainChk=true;
		}
	}
	if(emailDomainChk==false){
		$('#messageEmail').html(resourceJSON.msgValiddistrictemailaddress);
		$('#myModalEmail').modal('show');
	}
}

function closeMailDiv(){
	document.getElementById("email").value="";
	$('#myModalEmail').modal('hide');
	$('#email').focus();
}
function onLoadInternalPage(){
	var teacherId=document.getElementById("teacherId").value;
	if(teacherId!=null && teacherId!=""){
		InternalTransferAjax.displayCategoryByTeacher(teacherId,noOfRows,page,sortOrderStr,sortOrderType, { 
			async: true,
			callback: function(data)
			{
				document.getElementById("showCategoryDiv").innerHTML=data;
				applyCategoryOnTbl();
			},
		errorHandler:handleError
		});
	}else{
		document.getElementById("onLoadCategoryDiv").innerHTML="<table  id='categoryTable' width='100%' border='0' ><thead class='bg'><tr><th width='55%' valign='top'>"+resourceJSON.msgCategory1+"</th><th width='15%' valign='top'>"+resourceJSON.msgSubject1+"</th><th width='15%'>"+resourceJSON.msgActions+"</th></tr></thead><tr><td colspan=3>"+resourceJSON.msgNoRecordFound+"</td></tr></table>";
		document.getElementById("onLoadCategoryDiv").style.display='inline';
		$('#schoolName').focus();
	}
}
function chkForEnterInternal(evt)
{
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==13)
	{
		validateInternalTransfer();
	}	
}

