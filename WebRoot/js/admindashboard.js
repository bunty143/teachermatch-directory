
function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else if(exception.javaClassName=='undefined'){}
	else{alert(resourceJSON.msgSomeerror+": "+exception.javaClassName);}
}

/*function errorHandler(message, ex) 
{
    dwr.util.setValue("error", "<font color='red'>Cannot connect to server. Initializing retry logic.</font>", {escapeHtml:false});
	setTimeout(function() { dwr.util.setValue("error", ""); }, 5000)
}*/
function getYesterdayApplicants()
{
	$('#yesterdayGrid').html("<div style='text-align:center;padding-top:150px;'><img src='images/loadingAnimation.gif' /> <br>"+resourceJSON.MsgLoad+"</div>");
	AdminDashboardAjax.getYesterdayApplicants({ 
		async: true,
		callback: function(data){
		$('#yesterdayGrid').html(data);
	},
	errorHandler:handleError  
	});
}
function getTotalsOfDateApplicants()
{
	$('#totalsOfDateGrid').html("<div style='text-align:center;padding-top:150px;'><img src='images/loadingAnimation.gif' /> <br>"+resourceJSON.MsgLoad+"</div>");
	AdminDashboardAjax.getTotalsOfDateApplicants({ 
		async: true,
		callback: function(data){
		$('#totalsOfDateGrid').html(data);
	},
	errorHandler:handleError  
	});
}

function getCandidates(dflg,flg,cflg)
{
	//alert(dflg+" "+ flg + " "+cflg );
	//$('#loadingDivInventory').fadeIn();

	var redirectURL = "candidatesboard.do?p="+dflg+"&q="+flg+"&r="+cflg;
	window.location.href=redirectURL;
}
/////////////////////////////////////////////////////////////////////////
var page = 1;
var noOfRows = 50;
var sortOrderStr="";
var sortOrderType="";
function getPaging(pageno)
{
	if(pageno!='')
	{
		page=pageno;	
	}
	else
	{
		page=1;
	}

	noOfRows = document.getElementById("pageSize").value;
	getApplicants();
}
function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
	if(pageno!=''){
		page=pageno;	
	}else{
		page=1;
	}
	sortOrderStr=sortOrder;
	sortOrderType=sortOrderTyp;
	if(document.getElementById("pageSize")!=null){
		noOfRows = document.getElementById("pageSize").value;
	}else{
		noOfRows=10;
	}

	getApplicants();

}
/*=======  searchTeacher on Press Enter Key ========= */
function chkForEnterSearchTeacher(evt)
{
	var charCode = ( evt.which ) ? evt.which : event.keyCode;	
	if(charCode==13)
	{
		searchTeacher();
	}	
}
/*========  display grid after Filter ===============*/
function searchTeacher()
{	
	page=1;
	getApplicants()
	
}
function getApplicants()
{
	//$('#totalsOfDateGrid').html("<div style='text-align:center;padding-top:150px;'><img src='images/loadingAnimation.gif' /> <br>"+resourceJSON.MsgLoad+"</div>");
	var p = $('#p').val();
	var q = $('#q').val();
	var r = $('#r').val();
	
	var firstName		=	$('#firstName').val(); 
	var lastName		=	$('#lastName').val();
	var emailAddress	=	$('#emailAddress').val();
	//alert(p+" "+ q+ " "+r );

	AdminDashboardAjax.getApplicants(firstName,lastName,emailAddress,p,q,r,noOfRows,page,sortOrderStr,sortOrderType,{ 
		async: true,
		callback: function(data){
		$('#divMain').html(data);
		applyScrollOnTbl();
		if(r=2)
		tpIncompleteEnable();
	},
	errorHandler:handleError  
	});
}

function tpIncompleteEnable()
{
	var noOrRow = document.getElementById("tblGrid").rows.length;

	for(var j=1;j<=noOrRow;j++)
	{
		$('#tpIncomplete'+j).tooltip();
	}
}
function tpIncompleteDisable()
{
	var noOrRow = document.getElementById("tblGrid").rows.length;

	for(var j=1;j<=noOrRow;j++)
	{
		$('#tpIncomplete'+j).trigger('mouseout');
	}
}
function checkAll(flg)
{
	var checkBox = document.getElementsByName("case");
	for(i=0;i<checkBox.length;i++)
		checkBox[i].checked=flg;
}

function sendMessages()
{
	if($('#messageId').val() == 0 )
	{
		$('#message2show').html(resourceJSON.msgPlzEnterDefmessage);
		$('#myModal2').modal('show');
	}
	else if($('input[name=case]:checkbox:checked').length == 0 )
	{
		$('#message2show').html(resourceJSON.msgAtleast1Cand);
		$('#myModal2').modal('show');
	} 
	else
	{
		//$('#paymentMessage').html("The Messages are being sent...");
		$('#loadingDivInventory').show();
		var checkBox = document.getElementsByName("case");
		var arr =[];
		for(i=0;i<checkBox.length;i++)
		{
			if(checkBox[i].checked==true)
				arr.push(checkBox[i].value);
		}

		AdminDashboardAjax.sendMessagesToApplicants(arr,{ 
			async: true,
			callback: function(data){
			//alert(data);
			if(data==1)
			{
				$('#message2show').html(resourceJSON.msgSendSuccess);
				$('#myModal2').modal('show');
				$('#loadingDivInventory').hide();
				//checkAll(false);
				getApplicants();
			}
		},
		errorHandler:handleError  
		});
	}
}
function getDefaultMsg()
{
	$('#errordivDefaultMsg').empty();
	$('#msgDash').find(".jqte_editor").css("background-color", "");
	$('#defaultSubject').css("background-color", "");
	AdminDashboardAjax.getDefaultMsg("DashBoradMail",{ 
		async: true,
		callback: function(data){
		//alert(data);
		//var obj = eval ("(" + data + ")");
		var obj = data;
		//alert(obj.message);
		//alert(obj.subject);
		$('#defaultSubject').val(obj.subject);
		$('#msgDash').find(".jqte_editor").html(obj.message);
		$('#myModal1setMsg').modal('show');
		$('#defaultSubject').focus();

	},
	errorHandler:handleError  
	});
}

function saveDefaultMsg()
{
	$('#msgDash').find(".jqte_editor").css("background-color", "");
	$('#defaultSubject').css("background-color", "");
	$('#errordivDefaultMsg').empty();

	var cnt=0;
	var focs=0;

	if(trim($('#defaultSubject').val())=="")
	{
		$('#errordivDefaultMsg').append("&#149; "+resourceJSON.PlzEtrSub+"<br>");
		if(focs==0)
			$('#defaultSubject').focus();
		$('#defaultSubject').css("background-color", "#F5E7E1");
		cnt++;focs++;
	}
	if ($('#msgDash').find(".jqte_editor").text().trim()=="")
	{
		$('#errordivDefaultMsg').append("&#149; "+resourceJSON.PlzEtrMsg+"<br>");
		if(focs==0)
			$('#msgDash').find(".jqte_editor").focus();
		$('#msgDash').find(".jqte_editor").css("background-color", "#F5E7E1");
		cnt++;focs++;
	}
	if(cnt==0)
	{
		//return true;
	}
	else
	{
		$('#errordivDefaultMsg').show();
		return;
	}
	AdminDashboardAjax.setDefaultMsg($('#messageId').val(),$('#defaultSubject').val(),$('#msgDash').find(".jqte_editor").html(),"DashBoradMail",{ 
		async: true,
		callback: function(data){
		//alert(data);
		$('#messageId').val(data);
		$('#myModal1setMsg').modal('hide');
		$('#message2show').html(resourceJSON.msgUpdateSuccess);
		$('#myModal2').modal('show');

	},
	errorHandler:handleError  
	});
}

function makeEPIIncomplete(teacherId)
{

	$('#message2showConfirm').html(resourceJSON.msgCandidateEPIIncomplete);
	$('#footerbtn').html("<button class='btn btn-primary' onclick=\"makeIncomplete('"+teacherId+"')\" >"+resourceJSON.btnOk+"</button> <button class='btn' data-dismiss='modal' aria-hidden='true'>"+resourceJSON.btnCancel+"</button>");
	$('#myModal3').modal('show');

}

function makeIncomplete(teacherId)
{
	var teacherDetail = {teacherId:teacherId};
	//alert(teacherDetail.teacherId);
	$('#myModal3').modal('hide');
	$('#loadingDivInventory').show();
	
	AdminDashboardAjax.makeEPIIncomplete(teacherDetail,{ 
		async: true,
		callback: function(data){
		$('#loadingDivInventory').hide();
		$('#message2show').html(resourceJSON.msgEPIStatusIncomp);
		$('#myModal2').modal('show');
		tpIncompleteDisable();
		getApplicants();
		
	},
	errorHandler:handleError  
	});
}
