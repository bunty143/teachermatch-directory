var certificationDivCount=0;
var schoolDivCount=0;
var schoolDivCountCheck=0;
var schoolDivVal="";
var formBtnVal=0;
/* @Author: Gagan 
 * @Discription: view of edit domain js.
*/
/*========  For Handling session Time out Error ===============*/
function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert(resourceJSON.msgServerErr+": "+exception.javaClassName);}
}
/*=======Search For AddEditBatchJobOrder on Press Enter Key ========= */
function chkForEnterAddEditBatchJobOrder(evt)
{	
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if((charCode==13) && (evt.srcElement.className!='jqte_editor'))
	{
		validateAddEditJobOrder(1);
	}	
}
/*=========== For Resolving IE TextArea Maxlength Problem ================*/
function chkLenOfTextArea(textValue,maxlength)
{
	var tlen=textValue.value.length;
	if(tlen>maxlength)
	{
		document.getElementById(textValue.id).value=document.getElementById(textValue.id).value.substr(0,maxlength)
	}
}
/*======== Redirect to Manage User Page ===============*/
function cancelBatchJobOrder()
{
	var schoolBatchId=document.getElementById("schoolBatchId").value;
	if(schoolBatchId!=""){
		window.location.href="batchschooljoborder.do";
	}else{
		window.location.href="batchjoborder.do";
	}
	
}
function showFile(districtORSchoool,districtORSchooolId,docFileName,linkId)
{
	BatchJobOrdersAjax.showFile(districtORSchoool,districtORSchooolId,docFileName,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			if(data=="")			
				alert(resourceJSON.msgJSIisnotuploaded)
			else if(data.indexOf(".doc")!=-1)
			{
				document.getElementById("iframeJSI").src=data;
			}
			else
			{
				document.getElementById(linkId).href = data; 
				return false;
			}
		} 
	});
}
function deleteSchoolInBatchJob(batchJobId,schoolId)
{
	if (confirm(resourceJSON.msgsuredeleteschool)) {
		if( (batchJobId==''|| batchJobId==0))batchJobId=0;
		if(schoolId=='')schoolId=0;
		BatchJobOrdersAjax.deleteSchoolInBatchJob(batchJobId,schoolId, { 
			async: true,
			callback: function(data)
			{
				ShowSchool();
			},
			errorHandler:handleError 
		});
	}

}
function deleteCertification(certificationDivCount,batchJobId,jobCertificationId)
{
	/*var addBatchJobFlag=document.getElementById("addBatchJobFlag").value;
	var copyBatchJobId=document.getElementById("copyBatchJobId").value;
	if((addBatchJobFlag=="false" && copyBatchJobId==0) || copyBatchJobId!=0){
		removeCertification(certificationDivCount);
	}else{*/
		if (confirm(resourceJSON.msgsuredeletecertificationname)) {
			if(jobCertificationId=='')jobCertificationId=0;
			if( (batchJobId==''|| batchJobId==0))batchJobId=0;
			BatchJobOrdersAjax.deleteCertification(batchJobId,jobCertificationId, { 
				async: true,
				callback: function(data)
				{
					DisplayCertification();
				},
				errorHandler:handleError 
			});
		}
	//}
}
function removeAssessment()
{
	var batchJobId	=	document.getElementById("batchJobId").value;
	var JobOrderType=document.getElementById("JobOrderType").value;
	if(window.confirm(resourceJSON.msgRemoveInventry))
	{
		document.getElementById("frmJobAssessmentUpload").reset();
		BatchJobOrdersAjax.deleteAssessment(batchJobId,JobOrderType,{ 
			async: true,
			callback: function(data)
			{
				document.getElementById("divAssessmentDocument").style.display='none';
			},
			errorHandler:handleError 
		});
	}
	return false;
}
function clearAssessment()
{
	
	if(window.confirm(resourceJSON.msgclearinventory))
	{
		$('#errordiv').empty();
		document.getElementById("frmJobAssessmentUpload").reset();
	}
	return false;
}
/*========  SearchDistrictOrSchool ===============*/
function validateAddEditJobOrder(btnVal)
{
	var JobOrderType=document.getElementById("JobOrderType").value;
	var districtOrSchooHiddenlId	=	document.getElementById("districtOrSchooHiddenlId").value;
	var jobTitle	=	trim(document.getElementById("jobTitle").value);

	var jobStartDate	=trim(document.getElementById("jobStartDate").value);
	var jobEndDate	= trim(document.getElementById("jobEndDate").value);

	var districtORSchoolName	=	trim(document.getElementById("districtORSchoolName").value);
	var jobDescription	=		trim($('#jDescription').find(".jqte_editor").html());
	var jobQualification	=		trim($('#jQualification').find(".jqte_editor").html());
	
	var noOfExpHires	= trim(document.getElementById("noOfExpHires").value);
	var noOfExpHiresSchool	=0;	//trim(document.getElementById("noOfExpHiresSchool").value);
	
	var allSchoolGradeDistrictVal	=	trim(document.getElementById("allSchoolGradeDistrictVal").value);
	
	var isJobAssessment	=	trim(document.getElementById("isJobAssessmentVal").value);
	var attachJobAssessment	=	trim(document.getElementById("attachJobAssessmentVal").value);
	var assessmentDocument	=	document.getElementById("assessmentDocument").value;
	var exitURLMessageVal	=	document.getElementById("exitURLMessageVal").value;
	var exitURL	=	trim(document.getElementById("exitURL").value);
	var exitMessage	=		trim($('#eMessage').find(".jqte_editor").html());
	
	var pkOffered	=	document.getElementById("pkOffered").checked;
	var kgOffered	=	document.getElementById("kgOffered").checked;
	var g01Offered	=	document.getElementById("g01Offered").checked;
	var g02Offered	=	document.getElementById("g02Offered").checked;
	var g03Offered	=	document.getElementById("g03Offered").checked;
	var g04Offered	=	document.getElementById("g04Offered").checked;
	var g05Offered	=	document.getElementById("g05Offered").checked;
	var g06Offered	=	document.getElementById("g06Offered").checked;
	var g07Offered	=	document.getElementById("g07Offered").checked;
	var g08Offered	=	document.getElementById("g08Offered").checked;
	var g09Offered	=	document.getElementById("g09Offered").checked;
	var g10Offered	=	document.getElementById("g10Offered").checked;
	var g11Offered	=	document.getElementById("g11Offered").checked;
	var g12Offered	=	document.getElementById("g12Offered").checked;
	
	var allSchoolGradeDistrict1	=	document.getElementById("allSchoolGradeDistrict1").checked;
	var allSchoolGradeDistrict2	=	document.getElementById("allSchoolGradeDistrict2").checked;
	var allSchoolGradeDistrict3	=	document.getElementById("allSchoolGradeDistrict3").checked;
	
	
	var addBatchJobFlag=	document.getElementById("addBatchJobFlag").value;
	var districtHiddenId	=	document.getElementById("districtHiddenId").value;
	var batchJobId	=	document.getElementById("batchJobId").value;
	var dateTime=trim(document.getElementById("dateTime").value);
	
	var jobCategoryId	=	document.getElementById("jobCategoryId").value;
	
	var assessmentDocumentVal="";
	var errorCount=0;
	var jDec="",qDes="",dName="",jTitle="",nHires="",sNHires="",aDoc="",eUrl="",eMsg="",sDate="",eDate="",sAll="",jobCId="";
	$('#errordiv').empty();
	
	if(districtORSchoolName=="" && districtOrSchooHiddenlId==""){
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.PlzEtrDistrict+"<br>");
		errorCount++;
		dName="1";
	}else if(districtOrSchooHiddenlId==""){
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.msgvaliddistrict+"<br>");
		$('#districtORSchoolName').focus();
		$('#districtORSchoolName').css("background-color", "#F5E7E1");
		errorCount++;
		dName="1";
	}if(jobTitle==""){
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.msgjobtitleenter+"<br>");
		errorCount++;
		jTitle="1";
	}
	var jDescription	=	trim($('#jDescription').find(".jqte_editor").text());
	var charCountforQDescription=trim($('#jQualification').find(".jqte_editor").text());
	
	var countforQDescription = charCountforQDescription.length;
	if(countforQDescription>500)
	{
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.msgQualificationDescritionlength+"<br>");
		if(focs==0)
			$('#jQualification').find(".jqte_editor").focus();
		$('#jQualification').find(".jqte_editor").css("background-color", "#F5E7E1");
		cnt++;focs++;
		qDes="1";
	}
	
	
	/*if (jDescription!=""){
		var charCount=trim($('#jDescription').find(".jqte_editor").text());
		var count = charCount.length;
		if(count>6000)
		{
			$('#errordiv').show();
			$('#errordiv').append("&#149; Job Description length cannot exceed 6000 characters<br>");
			errorCount++;
			jDec="1";
		}
	}*/
	
	if(addBatchJobFlag=="false"){
		if(jobStartDate==""){
			$('#errordiv').show();
			$('#errordiv').append("&#149; "+resourceJSON.msgPostingStartDate+"<br>");
			errorCount++;
			sDate="1";
		}
		if(jobEndDate==""){
			$('#errordiv').show();
			$('#errordiv').append("&#149; "+resourceJSON.msgPostingEndDate+"<br>");
			errorCount++;
			eDate="1";
		}
		if(jobStartDate!="" && jobEndDate!=""){
			var endDate = new Date();  
			var startDate= new Date(jobStartDate); 
		    startDate.setHours(startDate.getHours() +23);
		    startDate.setTime(startDate.getTime() + (59 * 60 * 1000));
		 	if(startDate < endDate){
				$('#errordiv').show();
				$('#errordiv').append("&#149; "+resourceJSON.msggreaterthancurrentdate+"<br>");
				errorCount++;
				sDate="1";
			} 
		}
		if(jobStartDate!="" && jobEndDate!="" && sDate!="1"){
			var endDate = new Date(jobEndDate);              
		    var startDate= new Date(jobStartDate);   
			if(startDate > endDate){
				$('#errordiv').show();
				$('#errordiv').append("&#149; "+resourceJSON.msggreaterthanenddate+"<br>");
				errorCount++;
				sDate="1";
			} 
		}
		if(jobCategoryId==0){
			$('#errordiv').show();
			$('#errordiv').append("&#149; "+resourceJSON.msgJobCategoryName+"<br>");
			errorCount++;
			jobCId="1";
		}
	}
	if((allSchoolGradeDistrictVal!=3 && addBatchJobFlag=="true") || addBatchJobFlag=="false"){
		if(( noOfExpHires=="" || noOfExpHires==0)){
			$('#errordiv').show();
			$('#errordiv').append("&#149; "+resourceJSON.msgExpectedHire+"<br>");
			errorCount++;
			nHires="1";
		}
	}
	
	if((allSchoolGradeDistrict1==false && allSchoolGradeDistrict2==false)&& allSchoolGradeDistrict3==false){
			$('#errordiv').show();
			$('#errordiv').append("&#149; "+resourceJSON.msgleastoneSchool+"<br>");
			errorCount++;
			sAll="1";
	}
	
	/*else if(JobOrderType==3 && ( noOfExpHiresSchool=="" || noOfExpHiresSchool==0)){
		$('#errordiv').show();
		$('#errordiv').append("&#149; Please enter # of Expected Hire(s)<br>");
		errorCount++;
		sNHires="1";
	}else if(JobOrderType==3){
		noOfExpHires=noOfExpHiresSchool;
	}*/
	if(addBatchJobFlag=="true"){
		if(allSchoolGradeDistrictVal==2 && pkOffered==false && kgOffered==false && g01Offered==false && g02Offered==false && g03Offered==false && g04Offered==false && g05Offered==false && g06Offered==false && g07Offered==false && g08Offered==false &&	g09Offered==false && g10Offered==false && g11Offered==false && g12Offered==false){
			$('#errordiv').show();
			$('#errordiv').append("&#149; "+resourceJSON.msgselectatoneGrade+"<br>");
			errorCount++;
		}else if((allSchoolGradeDistrictVal==3 && schoolDivCountCheck==0 &&  (batchJobId==''|| batchJobId==0)) || (allSchoolGradeDistrictVal==3 && trim(document.getElementById("schoolRecords").innerHTML)=='' && (batchJobId!='' && batchJobId!=0))){
			$('#errordiv').show();
			$('#errordiv').append("&#149; "+resourceJSON.msgaddatleastoneSchoolName+"<br>");
			errorCount++;
		}
		
	}
	if(addBatchJobFlag=="false"){
		var attachJobAssessment1=document.getElementById("attachJobAssessment1").checked;
		var attachJobAssessment2=document.getElementById("attachJobAssessment2").checked;
		var attachJobAssessment3=document.getElementById("attachJobAssessment3").checked;
		
		if(isJobAssessment==1 && attachJobAssessment1==false && attachJobAssessment2==false &&  attachJobAssessment3==false){
			$('#errordiv').show();
			$('#errordiv').append("&#149; Please select at least one job requires a job specific inventory.<br>");
			errorCount++;
			aDoc="1";
		}else if(assessmentDocument=="" && isJobAssessment==1 &&  attachJobAssessment==2 && document.getElementById("divAssessmentDocument").style.display!='inline'){
			$('#errordiv').show();
			$('#errordiv').append("&#149; Please upload Inventory.<br>");
			errorCount++;
			aDoc="1";
		}
		else if(assessmentDocument!="" && isJobAssessment==1 &&  attachJobAssessment==2)
		{
			var ext = assessmentDocument.substr(assessmentDocument.lastIndexOf('.') + 1).toLowerCase();
			var fileSize = 0;
			if ($.browser.msie==true)
		 	{	
			    fileSize = 0;	   
			}
			else
			{
				if(document.getElementById("assessmentDocument").files[0]!=undefined)
				{
					fileSize = document.getElementById("assessmentDocument").files[0].size;
				}
			}
			if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt'))
			{
				$('#errordiv').show();
				$('#errordiv').append("&#149; "+resourceJSON.msgAcceptableInventoryformats+"<br>");
				errorCount++;
				aDoc="1";
			}
			else if(fileSize>=10485760)
			{
				$('#errordiv').show();
				$('#errordiv').append("&#149; "+resourceJSON.msgfilesizelessthan+"<br>");
				errorCount++;
				aDoc="1";	
				
				assessmentDocumentVal="Inventory"+dateTime+"."+ext;
			}
		}
		if(exitURLMessageVal==1 &&  exitURL==""){
			$('#errordiv').show();
			$('#errordiv').append("&#149; "+resourceJSON.msgapplicantshouldberedirected+"<br>");
			errorCount++;
			eUrl="1";
		}
		if (exitURLMessageVal==2 &&  trim($('#eMessage').find(".jqte_editor").text())==""){
			$('#errordiv').show();
			$('#errordiv').append("&#149; "+resourceJSON.msgCompletionMessage+"<br>");
			errorCount++;
			eMsg="1";
		}else{
			var charCount=trim($('#eMessage').find(".jqte_editor").text());
			var count = charCount.length;
			if(count>1000)
			{
				$('#errordiv').show();
				$('#errordiv').append("&#149; "+resourceJSON.msgCompletionMessagelength+"<br>");
				errorCount++;
				eMsg="1";
			}
		}
	}
	
	if(dName==1){
		$('#districtORSchoolName').focus();
		$('#districtORSchoolName').css("background-color", "#F5E7E1");
	}else if(jTitle==1){
		$('#jobTitle').focus();
		$('#jobTitle').css("background-color", "#F5E7E1");
	}else if(sDate==1){
		$('#jobStartDate').focus();
		$('#jobStartDate').css("background-color", "#F5E7E1");
	}else if(eDate==1){
		$('#jobEndDate').focus();
		$('#jobEndDate').css("background-color", "#F5E7E1");
	}else if(jDec==1){
		$('#jDescription').find(".jqte_editor").focus();
		$('#jDescription').find(".jqte_editor").css("background-color", "#F5E7E1");
	}else if(qDes==1){
		$('#jQualification').find(".jqte_editor").focus();
		$('#jQualification').find(".jqte_editor").css("background-color", "#F5E7E1");
	}else if(jobCId==1){
		$('#jobCategoryId').focus();
		$('#jobCategoryId').css("background-color", "#F5E7E1");
	}else if(nHires==1){
		$('#noOfExpHires').focus();
		$('#noOfExpHires').css("background-color", "#F5E7E1");
	}else if(sNHires==1){
		$('#noOfExpHiresSchool').focus();
		$('#noOfExpHiresSchool').css("background-color", "#F5E7E1");
	}else if(sAll==1){
		$('#allSchoolGradeDistrict1').focus();
		$('#allSchoolGradeDistrict1').css("background-color", "#F5E7E1");
	}else if(aDoc==1){
		$('#assessmentDocument').focus();
		$('#assessmentDocument').css("background-color", "#F5E7E1");
	}else if(eUrl==1){
		$('#exitURL').focus();
		$('#exitURL').css("background-color", "#F5E7E1");
	}else if(eMsg==1){
		$('#eMessage').find(".jqte_editor").focus();
		$('#eMessage').find(".jqte_editor").css("background-color", "#F5E7E1");
	}
	if(errorCount==0){
		if(jobTitle!="" && batchJobId==0){
			BatchJobOrdersAjax.checkDuplicateJobTitle(jobTitle,districtHiddenId, { 
				async: true,
				callback: function(data){
				if(data>0){
					$('#errordiv').show();
					$('#errordiv').append("&#149; "+resourceJSON.msganothertitlejoborder+"<br>");
					$('#jobTitle').focus();
					$('#jobTitle').css("background-color", "#F5E7E1");
				}else{
					BatchJobOrdersAjax.checkSchoolExist(districtHiddenId,{
						async: true,
						callback: function(data){
						if(data==0){
							$('#errordiv').show();
							$('#errordiv').append("&#149; "+resourceJSON.msgnoschoolattacheddistrict+"<br>");
							$('#districtORSchoolName').focus();
							$('#districtORSchoolName').css("background-color", "#F5E7E1");
						}else{
							validateBatchJobOrder(btnVal);
						}
					},
					errorHandler:handleError
					});
				}
			},
			errorHandler:handleError
			});
		}else{
			BatchJobOrdersAjax.checkSchoolExist(districtHiddenId,{
				async: true,
				callback: function(data){
				if(data==0){
					$('#errordiv').show();
					$('#errordiv').append("&#149; "+resourceJSON.msgnoschoolattacheddistrict+"<br>");
					$('#districtORSchoolName').focus();
					$('#districtORSchoolName').css("background-color", "#F5E7E1");
				}else{
					validateBatchJobOrder(btnVal);
				}
			},
			errorHandler:handleError
			});
		}
		
	}else{
		$('#errordiv').show();
		return false;
	}
	
	
}
function checkFlash(){
	
	function getFlashVersion(desc) {
	    var matches = desc.match(/[\d]+/g);
	    matches.length = 3;  // To standardize IE vs FF
	    return matches.join('.');
	  }

	  var hasFlash = false;
	  var flashVersion = '';

	  if (navigator.plugins && navigator.plugins.length) {
	    var plugin = navigator.plugins['Shockwave Flash'];
	    if (plugin) {
	      hasFlash = true;
	      if (plugin.description) {
	        flashVersion = getFlashVersion(plugin.description);
	      }
	    }

	    if (navigator.plugins['Shockwave Flash 2.0']) {
	      hasFlash = true;
	      flashVersion = '2.0.0.11';
	    }

	  } else if (navigator.mimeTypes && navigator.mimeTypes.length) {
	    var mimeType = navigator.mimeTypes['application/x-shockwave-flash'];
	    hasFlash = mimeType && mimeType.enabledPlugin;
	    if (hasFlash) {
	      flashVersion = getFlashVersion(mimeType.enabledPlugin.description);
	    }

	  } else {
	    try {
	      // Try 7 first, since we know we can use GetVariable with it
	      var ax = new ActiveXObject('ShockwaveFlash.ShockwaveFlash.7');
	      hasFlash = true;
	      flashVersion = getFlashVersion(ax.GetVariable('$version'));
	    } catch (e) {
	      // Try 6 next, some versions are known to crash with GetVariable calls
	      try {
	        var ax = new ActiveXObject('ShockwaveFlash.ShockwaveFlash.6');
	        hasFlash = true;
	        flashVersion = '6.0.21';  // First public version of Flash 6
	      } catch (e) {
	        try {
	          // Try the default activeX
	          var ax = new ActiveXObject('ShockwaveFlash.ShockwaveFlash');
	          hasFlash = true;
	          flashVersion = getFlashVersion(ax.GetVariable('$version'));
	        } catch (e) {
	          // No flash
	        }
	      }
	    }
	  }
	return hasFlash;
}
function validateBatchJobOrder(btnVal)
{
	var JobOrderType=document.getElementById("JobOrderType").value;
	var districtOrSchooHiddenlId	=	document.getElementById("districtOrSchooHiddenlId").value;
	var jobTitle	=	trim(document.getElementById("jobTitle").value);
	var jobStartDate	=trim(document.getElementById("jobStartDate").value);
	var jobEndDate	= trim(document.getElementById("jobEndDate").value);
	var districtORSchoolName	=	trim(document.getElementById("districtORSchoolName").value);
	var jobDescription	=			trim($('#jDescription').find(".jqte_editor").html());
	var jobQualification	=		trim($('#jQualification').find(".jqte_editor").html());
	
	var noOfExpHires	=	trim(document.getElementById("noOfExpHires").value);
	var noOfExpHiresSchool	=0;//trim(document.getElementById("noOfExpHiresSchool").value);
	
	var allSchoolGradeDistrictVal	=	trim(document.getElementById("allSchoolGradeDistrictVal").value);
	
	var isJobAssessment	=	trim(document.getElementById("isJobAssessmentVal").value);
	var attachJobAssessment	=	trim(document.getElementById("attachJobAssessmentVal").value);
	var assessmentDocument	=	document.getElementById("assessmentDocument").value;
	var exitURLMessageVal	=	document.getElementById("exitURLMessageVal").value;
	var exitURL	=	trim(document.getElementById("exitURL").value);
	var exitMessage	=			trim($('#eMessage').find(".jqte_editor").html());
	
	var districtHiddenId	=	document.getElementById("districtHiddenId").value;
	var batchJobId	=	document.getElementById("batchJobId").value;
	
	
	var pkOffered	=	document.getElementById("pkOffered").checked;
	var kgOffered	=	document.getElementById("kgOffered").checked;
	var g01Offered	=	document.getElementById("g01Offered").checked;
	var g02Offered	=	document.getElementById("g02Offered").checked;
	var g03Offered	=	document.getElementById("g03Offered").checked;
	var g04Offered	=	document.getElementById("g04Offered").checked;
	var g05Offered	=	document.getElementById("g05Offered").checked;
	var g06Offered	=	document.getElementById("g06Offered").checked;
	var g07Offered	=	document.getElementById("g07Offered").checked;
	var g08Offered	=	document.getElementById("g08Offered").checked;
	var g09Offered	=	document.getElementById("g09Offered").checked;
	var g10Offered	=	document.getElementById("g10Offered").checked;
	var g11Offered	=	document.getElementById("g11Offered").checked;
	var g12Offered	=	document.getElementById("g12Offered").checked;
	var dateTime=trim(document.getElementById("dateTime").value);
	
	var schoolBatchId=trim(document.getElementById("schoolBatchId").value);
	
	var addBatchJobFlag=	document.getElementById("addBatchJobFlag").value;
	var assessmentDocumentVal="";
	var errorCount=0;
	var jobCategoryId	=	document.getElementById("jobCategoryId").value;
	
	var dName="",jTitle="",nHires="",sNHires="",aDoc="",eUrl="",eMsg="",sDate="",eDate="";
	$('#errordiv').empty();
	
	formBtnVal=btnVal;
	if(assessmentDocument!="" && isJobAssessment==1 &&  attachJobAssessment==2){
		var ext = assessmentDocument.substr(assessmentDocument.lastIndexOf('.') + 1).toLowerCase();		
		if(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt')
		{
			assessmentDocumentVal="Inventory"+dateTime+"."+ext;
		}
	}
	if(errorCount==0){
		document.getElementById("hideSave").style.display='none';
		var schoolIds="";
		var certificationType="";
		try{
			if(getTRAndTDForSchool()!=null){
			schoolIds=getTRAndTDForSchool();
			}
		}catch(err){}
		
		try{
			if(getTRAndTDForDiv()!=null){
				//certificationType=getTRAndTDForDiv();
				var certIds = document.getElementsByName("certificateTypeIds");
				//alert(certIds.length);
				for(i=0;i<certIds.length;i++)
					certificationType+=certIds[i].value+",";
			}
		}catch(err){}
		
		try{
			var jobCategoryVal=jobCategoryId.split("||");
			var jobCategory=jobCategoryVal[0];
			
			$('#loadingDiv').show();
			if(assessmentDocument!="" && isJobAssessment==1 &&  attachJobAssessment==2){
				document.getElementById("frmJobAssessmentUpload").submit();
			}else if(addBatchJobFlag=="false" || (addBatchJobFlag=="true" && JobOrderType==3)){
				BatchJobOrdersAjax.addSchoolJobOrder(jobCategory,schoolBatchId,pkOffered,kgOffered,g01Offered,g02Offered,g03Offered,g04Offered,g05Offered,g06Offered,g07Offered,g08Offered,g09Offered,g10Offered,g11Offered,g12Offered,btnVal,batchJobId,districtHiddenId,exitURLMessageVal,exitMessage,exitURL,certificationType+"",schoolIds+"",JobOrderType,districtOrSchooHiddenlId,jobTitle,jobDescription,jobQualification,noOfExpHires,jobStartDate,jobEndDate,allSchoolGradeDistrictVal,isJobAssessment,attachJobAssessment,assessmentDocumentVal, { 
					async: true,
					callback: function(data){
							$('#loadingDiv').hide();
							document.getElementById("copy_text").value=data;
							var copyURLMsg='';
							if(checkFlash()==true){
								copyURLMsg="<div id='d_clip_container'><div id='d_clip_button' style='width:70px;text-align:center; border:0px solid black; background-color:#FFFFFF; cursor:default;'> "+resourceJSON.msgCopyURL+"</div></div>";
							}
							var divMsg="<div>"+resourceJSON.msgsuccessfullycreatedajoborder+"<br/><br/>"+resourceJSON.msgTeacherMatchJobURLis+": <div id='jobOrderUrl'><a target='blank' href=\""+data+"\">"+data+"</a></div><div id='linkDiv' class='span2 pull-right'>"+copyURLMsg+"</div>";
							$('#jobOrderSend').modal('show');
							document.getElementById("jobOrderText").innerHTML=divMsg;
							init_swf();
							return false;
						},
						errorHandler:handleError 
						});
			}else{
				BatchJobOrdersAjax.addEditBatchJobOrder(pkOffered,kgOffered,g01Offered,g02Offered,g03Offered,g04Offered,g05Offered,g06Offered,g07Offered,g08Offered,g09Offered,g10Offered,g11Offered,g12Offered,btnVal,batchJobId,districtHiddenId,exitURLMessageVal,exitMessage,exitURL,certificationType+"",schoolIds+"",JobOrderType,districtOrSchooHiddenlId,jobTitle,jobDescription,jobQualification,noOfExpHires,jobStartDate,jobEndDate,allSchoolGradeDistrictVal,isJobAssessment,attachJobAssessment,assessmentDocumentVal, { 
					async: true,
					callback: function(data)
					{
					var schoolBatchId=document.getElementById("schoolBatchId").value;
					if(schoolBatchId!=""){
						window.location.href="batchschooljoborder.do";
					}else{
						window.location.href="batchjoborder.do";
					}
				},
				errorHandler:handleError 
				});
			}
		}catch(err){}
		
	}else{
		$('#errordiv').show();
		return false;
	}
}
function showDivMsg(jobCategoryId){
	var jobCategoryVal=jobCategoryId.split("||");
	var jobCategoryEPI=jobCategoryVal[1];
	if(jobCategoryEPI=="false"){
		var divMsg="<div>"+resourceJSON.msganonteachingjobcategory+"<br/> "+resourceJSON.msgteachingjobsonly+"</div>";
		$('#EPIMessage').modal('show');
		document.getElementById("EPIMsg").innerHTML=divMsg;
	}
}
function actionForward()
{
	var schoolBatchId=document.getElementById("schoolBatchId").value;
	if(schoolBatchId!=""){
		window.location.href="batchschooljoborder.do";
	}else{
		window.location.href="batchjoborder.do";
	}
}
function ClipBoard() 
{
	var url	=	$('#jobOrderUrl').text();
	try{
		url =  url.substr(0,60);
	}catch(err){}
	BatchJobOrdersAjax.copyToClipBoard(url, { 
		errorHandler:handleError 
	});
}
function saveRecord()
{
	var JobOrderType=document.getElementById("JobOrderType").value;
	var districtOrSchooHiddenlId	=	document.getElementById("districtOrSchooHiddenlId").value;
	var jobTitle	=	trim(document.getElementById("jobTitle").value);
	var jobStartDate	=trim(document.getElementById("jobStartDate").value);
	var jobEndDate	= trim(document.getElementById("jobEndDate").value);
	var districtORSchoolName	=	trim(document.getElementById("districtORSchoolName").value);
	var jobDescription	=			trim($('#jDescription').find(".jqte_editor").html());
	var jobQualification	=		trim($('#jQualification').find(".jqte_editor").html());
	var noOfExpHires	=	trim(document.getElementById("noOfExpHires").value);
	var noOfExpHiresSchool	=0;//trim(document.getElementById("noOfExpHiresSchool").value);
	
	var allSchoolGradeDistrictVal	=	trim(document.getElementById("allSchoolGradeDistrictVal").value);
	
	var isJobAssessment	=	trim(document.getElementById("isJobAssessmentVal").value);
	var attachJobAssessment	=	trim(document.getElementById("attachJobAssessmentVal").value);
	var assessmentDocument	=	document.getElementById("assessmentDocument").value;
	var exitURLMessageVal	=	document.getElementById("exitURLMessageVal").value;
	var exitURL	=	trim(document.getElementById("exitURL").value);
	var exitMessage	=	trim($('#eMessage').find(".jqte_editor").html());
	
	var districtHiddenId	=	document.getElementById("districtHiddenId").value;
	var batchJobId	=	document.getElementById("batchJobId").value;
	
	
	var pkOffered	=	document.getElementById("pkOffered").checked;
	var kgOffered	=	document.getElementById("kgOffered").checked;
	var g01Offered	=	document.getElementById("g01Offered").checked;
	var g02Offered	=	document.getElementById("g02Offered").checked;
	var g03Offered	=	document.getElementById("g03Offered").checked;
	var g04Offered	=	document.getElementById("g04Offered").checked;
	var g05Offered	=	document.getElementById("g05Offered").checked;
	var g06Offered	=	document.getElementById("g06Offered").checked;
	var g07Offered	=	document.getElementById("g07Offered").checked;
	var g08Offered	=	document.getElementById("g08Offered").checked;
	var g09Offered	=	document.getElementById("g09Offered").checked;
	var g10Offered	=	document.getElementById("g10Offered").checked;
	var g11Offered	=	document.getElementById("g11Offered").checked;
	var g12Offered	=	document.getElementById("g12Offered").checked;
	var dateTime=trim(document.getElementById("dateTime").value);
	
	var schoolBatchId=trim(document.getElementById("schoolBatchId").value);
	
	var addBatchJobFlag=	document.getElementById("addBatchJobFlag").value;
	var jobCategoryId	=	document.getElementById("jobCategoryId").value;
	var assessmentDocumentVal="";
	var errorCount=0;
	
	var assessmentDocumentVal="";
	var errorCount=0;
	
	$('#errordiv').empty();
	var schoolIds="";
	var certificationType="";
	try{
		if(getTRAndTDForSchool()!=null){
		schoolIds=getTRAndTDForSchool();
		}
	}catch(err){}
	
	try{
		if(getTRAndTDForDiv()!=null){
			//certificationType=getTRAndTDForDiv();
			var certIds = document.getElementsByName("certificateTypeIds");
			//alert(certIds.length);
			for(i=0;i<certIds.length;i++)
				certificationType+=certIds[i].value+",";
		}
	}catch(err){}
	
	if(assessmentDocument!="" && isJobAssessment==1 &&  attachJobAssessment==2){
		var ext = assessmentDocument.substr(assessmentDocument.lastIndexOf('.') + 1).toLowerCase();		
		if(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt')
		{
			assessmentDocumentVal="Inventory"+dateTime+"."+ext;
		}
	}
	var jobCategoryVal=jobCategoryId.split("||");
	var jobCategory=jobCategoryVal[0];
	$('#loadingDiv').show();
	if(addBatchJobFlag=="false"){
		BatchJobOrdersAjax.addSchoolJobOrder(jobCategory,schoolBatchId,pkOffered,kgOffered,g01Offered,g02Offered,g03Offered,g04Offered,g05Offered,g06Offered,g07Offered,g08Offered,g09Offered,g10Offered,g11Offered,g12Offered,formBtnVal,batchJobId,districtHiddenId,exitURLMessageVal,exitMessage,exitURL,certificationType+"",schoolIds+"",JobOrderType,districtOrSchooHiddenlId,jobTitle,jobDescription,jobQualification,noOfExpHires,jobStartDate,jobEndDate,allSchoolGradeDistrictVal,isJobAssessment,attachJobAssessment,assessmentDocumentVal, { 
			async: true,
			callback: function(data)
				{
					$('#loadingDiv').hide();
					document.getElementById("copy_text").value=data;
					var copyURLMsg='';
					if(checkFlash()==true){
						copyURLMsg="<div id='d_clip_container'><div id='d_clip_button' style='width:70px;text-align:center; border:0px solid black; background-color:#FFFFFF; cursor:default;'> "+resourceJSON.msgCopyURL+"</div></div>";
					}
					var divMsg="<div>"+resourceJSON.msgsuccessfullycreatedajoborder+"<br/><br/>"+resourceJSON.msgTeacherMatchJobURLis+": <div id='jobOrderUrl'><a target='blank' href=\""+data+"\">"+data+"</a></div><div id='linkDiv' class='span2 pull-right'>"+copyURLMsg+"</div>";
					$('#jobOrderSend').modal('show');
					document.getElementById("jobOrderText").innerHTML=divMsg;
					init_swf();
					return false;
				},
				errorHandler:handleError 
				});
	}else{
		BatchJobOrdersAjax.addEditBatchJobOrder(pkOffered,kgOffered,g01Offered,g02Offered,g03Offered,g04Offered,g05Offered,g06Offered,g07Offered,g08Offered,g09Offered,g10Offered,g11Offered,g12Offered,formBtnVal,batchJobId,districtHiddenId,exitURLMessageVal,exitMessage,exitURL,certificationType+"",schoolIds+"",JobOrderType,districtOrSchooHiddenlId,jobTitle,jobDescription,jobQualification,noOfExpHires,jobStartDate,jobEndDate,allSchoolGradeDistrictVal,isJobAssessment,attachJobAssessment,assessmentDocumentVal, { 
			async: true,
			callback: function(data)
			{
				var schoolBatchId=document.getElementById("schoolBatchId").value;
				if(schoolBatchId!=""){
					window.location.href="batchschooljoborder.do";
				}else{
					window.location.href="batchjoborder.do";
				}
				},
			errorHandler:handleError
		});
	}
}
function getTRAndTDForDiv(){
	var idArr = [];
	var trs =document.getElementsByTagName('div');
	for(var i=0;i<trs.length;i++){
		var ids=trs[i].id;
		if(trs[i].style.display!='none' && i!=0 && ids.indexOf("CertificationGrid") !=-1){
			var lableText=document.getElementById(trs[i].id).innerHTML;
				lableText=lableText.substr(7,lableText.indexOf("<a")-7);
				idArr.push(lableText.replace(',','<|>'));
		}
	}
	return idArr;
}
function getCertificationTypeCheck(certificationType){
	var isIdExist =false;
	try{
		var divs =document.getElementsByTagName('div');
		for(var i=0;i<divs.length;i++){
			var ids=divs[i].id;
			if(divs[i].style.display!='none' && i!=0 && ids.indexOf("CertificationGrid") !=-1){
				var lableText=document.getElementById(divs[i].id).innerHTML;
					lableText="||"+lableText.substr(7,lableText.indexOf("<a")-7)+"||";
					if(lableText=="||"+certificationType+"||"){
						isIdExist=true;
					}
			}
		}
	}catch(err){}
	return isIdExist;
}
function getTRAndTDForSchool(){
	var idArr = [];
	var trs =document.getElementById("schoolTable").getElementsByTagName('TR'); 
	for(var i=0;i<trs.length;i++){
		var tdchk=trs[i].getElementsByTagName('TD');
		if(trs[i].style.display!='none' && i!=0){
			var getId=trs[i].id.split("_");
			idArr.push(getId[0]+"-"+tdchk[2].innerHTML);
		}
	}
	return idArr;
}
function getSchoolIdCheck(schoolId){
	var isIdExist =false;
	try{
	var trs =document.getElementById("schoolTable").getElementsByTagName('TR'); 
	for(var i=0;i<trs.length;i++){
		var tdchk=trs[i].getElementsByTagName('TD');
		if(trs[i].style.display!='none' && i!=0){
			var getId=trs[i].id.split("_");
			if(getId[0]==schoolId){
				isIdExist=true;
			}
		}
	}
	}catch(err){}
	return isIdExist;
}
function removeCertification(val){
	if (confirm(resourceJSON.msgsuredeletecertificationname)) {
		document.getElementById("CertificationGrid"+val).style.display='none';
	}
}
function getCertificationTypeCount(){
	var isIdExist =0;
	try{
		if(certificationDivCount==0){
			var divs =document.getElementsByTagName('div');
			for(var i=0;i<divs.length;i++){
				var ids=divs[i].id;
				if(divs[i].style.display!='none' && i!=0 && ids.indexOf("CertificationGrid") !=-1){
					var lableText=document.getElementById(divs[i].id).innerHTML;
					certificationDivCount++;
				}
			}
		}
	}catch(err){}
	return isIdExist;
}
function validateAddCertification()
{	
	var statemaster = {stateId:dwr.util.getValue("stateMaster")};
	var certType=trim(document.getElementById("certType").value);
	var certTypeId=trim(document.getElementById("certificateTypeMaster").value);
	//alert(certType);
	var certNameCheck=false;
	if(certType!=""){
		certNameCheck=getCertificationTypeCheck(certType);
	}
	var count=0;
	if(certType!=""){
		certNameCheck=getCertificationTypeCheck(certType);
	}
	var addBatchJobFlag=document.getElementById("addBatchJobFlag").value;
	var copyBatchJobId=document.getElementById("copyBatchJobId").value;
	if(addBatchJobFlag=="false" || copyBatchJobId!=0){
		getCertificationTypeCount();
	}
	$('#errordiv').empty();
	if(statemaster.stateId==0){
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.msgState+"<br>");
		$('#stateMaster').focus();
	}
	if(certType==''){
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.msgentercertificatename+" <br>");
		$('#certType').focus();
	}else if(certTypeId==''){
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.msgvalidCertificationLicensure+" <br>");
		$('#certType').focus();
	}else if(certNameCheck){
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.msgaddduplicateCertificationLicensure+"<br>");
		$('#certType').focus();
	}else{
		var batchJobId=document.getElementById("batchJobId").value;
		if(certType!="" &&  (batchJobId==''|| batchJobId==0 || addBatchJobFlag=="false")){
			certificationDivCount++;
			document.getElementById("showCertificationDiv").innerHTML	+=	"<div class=\"\"" +
					" id=\"CertificationGrid"+certificationDivCount+"\" nowrap><input type=\"hidden\"" +
					" name=\"certificateTypeIds\" id=\"certificateTypeId"+certificationDivCount+"\" " +
					"value=\""+certTypeId+"\" ><label name=\"certificateTypeText\">"+certType+
					"<a href=\"javascript:void(0);\" onclick=\"removeCertification("+certificationDivCount+");\"><img width=\"15\" height=\"15\"  class=\"can\" src=\"images/can-icon.png\"></a></label></div>";
		}else if((batchJobId!='' && batchJobId!=0)){
			BatchJobOrdersAjax.addCertification(batchJobId,certTypeId,statemaster.stateId,{ 
				async: true,
				callback: function(data)
						{	
							if(data>0){
								$('#errordiv').show();
								$('#errordiv').append("&#149; "+resourceJSON.msgaddduplicateCertificationLicensure+"<br>");
								$('#certType').focus();
							}else{
								DisplayCertification();
							}
						},
						errorHandler:handleError
					});
		}
		$("#addCertificationDiv").hide();
	}
	
}
function removeSchool(schoolId,val){
	if (confirm(resourceJSON.msgsuredeleteschool)) {
		var str1="schoolGrid"+val+"\" style=\"display:none";
		var str2="schoolGrid"+val;
		document.getElementById(schoolId+"_schoolGrid"+val).style.display='none';
		schoolDivVal=schoolDivVal.replace(str2,str1);
		schoolDivCountCheck--;
		if(schoolDivCountCheck==0){
			schoolDivVal="";
			schoolDivCount=0;
			document.getElementById("showSchoolDiv").innerHTML="";
		}
	}
	
}

function validateAddSchool()
{
	var schoolName=trim(document.getElementById("schoolName").value);
	var noOfSchoolExpHires=trim(document.getElementById("noOfSchoolExpHires").value);
	var schoolId=document.getElementById("schoolId").value;
	var schoolIdCheck=false;
	if(schoolId!=""){
		schoolIdCheck=getSchoolIdCheck(schoolId);
	}
	$('#errordiv').empty();
	if(schoolName==''){
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.msgPleaseenterSchoolName+"<br>");
		$('#schoolName').focus();
	}else if(schoolId==''){
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.msgentervalidSchoolName+"<br>");
		$('#schoolName').focus();
	}else if(schoolIdCheck){
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.magcannotaddexistingSchool+"<br>");
		$('#schoolName').focus();
	}else if(noOfSchoolExpHires==''|| noOfSchoolExpHires==0){
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.msgExpectedHire+"<br>");
		$('#noOfSchoolExpHires').focus();
	}else{
		var batchJobId=document.getElementById("batchJobId").value;
		var addBatchJobFlag=document.getElementById("addBatchJobFlag").value;
		var copyBatchJobId=document.getElementById("copyBatchJobId").value;
		if(schoolName!="" && (batchJobId==''|| batchJobId==0 && (addBatchJobFlag=="false" || copyBatchJobId!=0)) ){
			document.getElementById("showSchoolDiv").innerHTML="";
			schoolDivCount++;
			schoolDivCountCheck++;
			if(schoolDivCount==1){
				schoolDivVal="<table id=\"schoolTable\" width=\"100%\" border=\"0\" class=\"table table-bordered span11 mt10\"><thead class=\"bg\"><tr><th>"+resourceJSON.msgSchoolName+"</th><th>"+resourceJSON.msgAddedon+"</th><th> #"+resourceJSON.msgofExpectedHire+"</th><th>"+resourceJSON.msgActions+"</th></tr></thead><tbody>";
			}
			schoolDivVal+="<tr id=\""+schoolId+"_schoolGrid"+schoolDivCount+"\" ><td nowrap>"+schoolName+"</td><td>"+DateFormatChange(new Date())+"</td><td>"+noOfSchoolExpHires+"</td><td>&nbsp;<a href=\"javascript:void(0);\" onclick=\"removeSchool("+schoolId+","+schoolDivCount+");\">"+resourceJSON.msgDelete+"</a></td></tr>";
			document.getElementById("showSchoolDiv").innerHTML	+=schoolDivVal+"</tbody></table>"	;
		}else if((batchJobId!='' && batchJobId!=0)){
			BatchJobOrdersAjax.addSchoolInBatchJob(batchJobId,schoolId,noOfSchoolExpHires, { 
				async: true,
				callback: function(data)
				{
					if(data>0){
						$('#errordiv').show();
						$('#errordiv').append("&#149; "+resourceJSON.magcannotaddexistingSchool+"<br>");
						$('#schoolName').focus();
					}else{
						ShowSchool();
					}
				},
				errorHandler:handleError 
			});
		}
	$("#addSchoolDiv").hide();	
	document.getElementById("schoolName").value="";
	document.getElementById("noOfSchoolExpHires").value="";
	document.getElementById("schoolId").value="";
	}
}

function ShowSchool()
{
	var batchJobId=0;
	if(document.getElementById("schoolId").value!=null){
		batchJobId=document.getElementById("batchJobId").value;
	}
	BatchJobOrdersAjax.displaySchool(batchJobId, { 
		async: true,
		callback: function(data)
		{
			document.getElementById("schoolRecords").innerHTML=data;
			try
			{
				applyScrollOnTblSchool_List();	
			}
			catch(e)
			{alert(e)}
		},
		errorHandler:handleError 
	});
}
function jobStartDateDiv(){
	 var cal = Calendar.setup({
         onSelect: function(cal) { cal.hide() },
         showTime: true
    });
	cal.manageFields("jobStartDate", "jobStartDate", "%m-%d-%Y");
}
function DisplayCertification()
{
	var batchJobId=0;
	var jobStartDate	=	trim(document.getElementById("jobStartDate").value);
	if(document.getElementById("batchJobId").value!=''){
		batchJobId=document.getElementById("batchJobId").value;
	}
	
	if(batchJobId!=0){ 
		document.getElementById("jobTitle").readOnly=true;
	}else{
		document.getElementById("jobTitle").value="";
	}
	if(batchJobId==0){
		jobStartDateDiv();
	}else{
		if(jobStartDate!=""){
			var startDate= new Date(jobStartDate);   
			if(startDate < new Date()){
				document.getElementById('jobStartDate').readOnly=true;
			}else{
				jobStartDateDiv();
			}
		}else{
			jobStartDateDiv();
		}
	}
	var copyBatchJobId=document.getElementById("copyBatchJobId").value;
	if(copyBatchJobId!=0){
		batchJobId=copyBatchJobId;
	}
	BatchJobOrdersAjax.displayCertification(batchJobId, { 
		async: true,
		callback: function(data)
		{
			document.getElementById("certificationRecords").innerHTML=data;
		},
		errorHandler:handleError 
	});
}

/*========  displayJobRecords ===============*/
function displayJobRecords()
{
	var JobOrderType	=null;	
	var addBatchJobFlag	=null;	
	var allSchoolGradeDistrictVal	=null;	
	var resultFlag=true;
	var copyBatchJobId=null;
	try{
		addBatchJobFlag=document.getElementById("addBatchJobFlag").value;
		JobOrderType=document.getElementById("JobOrderType").value;
		allSchoolGradeDistrictVal=document.getElementById("allSchoolGradeDistrictVal").value;
		copyBatchJobId=document.getElementById("copyBatchJobId").value;
	}catch(err){
		JobOrderType=0;
	}
	if(JobOrderType	==	"" || JobOrderType	==	null)
	{
		JobOrderType	=0;
	}
	if(copyBatchJobId!=0){
		document.getElementById("pkOffered").checked=false;
		document.getElementById("kgOffered").checked=false;
		document.getElementById("g01Offered").checked=false;
		document.getElementById("g02Offered").checked=false;
		document.getElementById("g03Offered").checked=false;
		document.getElementById("g04Offered").checked=false;
		document.getElementById("g05Offered").checked=false;
		document.getElementById("g06Offered").checked=false;
		document.getElementById("g07Offered").checked=false;
		document.getElementById("g08Offered").checked=false;
		document.getElementById("g09Offered").checked=false;
		document.getElementById("g10Offered").checked=false;
		document.getElementById("g11Offered").checked=false;
		document.getElementById("g12Offered").checked=false;
		
		document.getElementById("allSchoolGradeDistrict1").checked=false;
		document.getElementById("allSchoolGradeDistrict2").checked=false;
		document.getElementById("allSchoolGradeDistrict3").checked=false;
		document.getElementById("allSchoolGradeDistrictVal").value="";
	}
	if(JobOrderType==2){
		if(allSchoolGradeDistrictVal!=3){
			document.getElementById("addHiresDiv").style.display='inline';
		}else{
			document.getElementById("noOfExpHires").value="";
			document.getElementById("addHiresDiv").style.display='none';	
		}
		
		document.getElementById("showExitURLDiv").style.display='none';
		document.getElementById("showExitMessageDiv").style.display='none';
		if(addBatchJobFlag=="true"){
			document.getElementById("hideForDistrict").style.display='none';
			document.getElementById("hideForSchool").style.display='inline';
			document.getElementById("hideDateDiv").style.display='none';
			document.getElementById("categoryForSchool").style.display='none';
			document.getElementById("captionDistrictOrSchool").innerHTML	=resourceJSON.msgDistrict+"<span class=\"required\">*</span>";
		}else{
			document.getElementById("hideForDistrict").style.display='inline';
			document.getElementById("hideForSchool").style.display='none';
			document.getElementById("hideDateDiv").style.display='inline';
			document.getElementById("categoryForSchool").style.display='inline';
			document.getElementById("captionDistrictOrSchool").innerHTML	=	"";
		}
	}else if(JobOrderType==3){
		if(addBatchJobFlag=="true"){
			document.getElementById("hideForDistrict").style.display='none';
			document.getElementById("hideForSchool").style.display='inline';
			document.getElementById("hideDateDiv").style.display='none';
			document.getElementById("categoryForSchool").style.display='none';
			document.getElementById("captionDistrictOrSchool").innerHTML	=resourceJSON.msgDistrict+"<span class=\"required\">*</span>";
		}else{
			document.getElementById("hideForDistrict").style.display='inline';
			document.getElementById("hideForSchool").style.display='none';
			document.getElementById("hideDateDiv").style.display='inline';
			document.getElementById("categoryForSchool").style.display='inline';
			document.getElementById("captionDistrictOrSchool").innerHTML	=	"";
		}
		//document.getElementById("hideForSchool").style.display='none';
		document.getElementById("addHiresDiv").style.display='inline';
		///document.getElementById("captionDistrictOrSchool").innerHTML	=	"School:<span class=\"required\">*</span>";
	}
	
}
/*========  activateDeactivateUser ===============*/
function activateDeactivateJob(entityID,jobid,status)
{
	BatchJobOrdersAjax.activateDeactivateJob(jobid,status,{ 
		async: true,
		callback: function(data)
		{
			displayJobRecords(entityID)
		},
		errorHandler:handleError 
	});
}

function getDistrictObjByDistrictId()
{
	//alert(" getDistrictObjByDistrictId [Js : Method ] ");
	BatchJobOrdersAjax.getDistrictObjByDistrictId($('#districtOrSchooHiddenlId').val(),{ 
		async: true,
		callback: function(data)
		{
			//alert(" data "+data);
			clearCertType();
			var optstate = document.getElementById('stateMaster').options;
			for(var i = 0, j = optstate.length; i < j; i++)
			{
				  if(data.stateId.stateId==optstate[i].value)
				  {
					  optstate[i].selected	=	true;
					  break;
				  }
			}
			
		},
		errorHandler:handleError 
	});
}

var count=0;
var index=-1;
var length=0;
var divid='';
var txtid='';
var hiddenDataArray = new Array();
var showDataArray = new Array();
var degreeTypeArray = new Array();

var hiddenExitURLArray = new Array();
var hiddenExitMessageArray = new Array();
var hiddenFlagForUrlArray = new Array();
var hiddenFlagForMessageArray = new Array();
var hiddenDAUploadURLArray = new Array();
var hiddenSAUploadURLArray = new Array();
var districtHiddenIdArray = new Array();

var hiddenId="";
function getDistrictORSchoolAuto(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("districtORSchoolName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getDistrictORSchoolArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getDistrictORSchoolArray(districtOrSchoolName){
	var searchArray = new Array();
	var JobOrderType	=	document.getElementById("JobOrderType").value;
	if(JobOrderType==2){
		BatchJobOrdersAjax.getFieldOfDistrictList(districtOrSchoolName,{ 
			async: false,
			callback: function(data){
			hiddenDataArray = new Array();
			showDataArray = new Array();
			for(i=0;i<data.length;i++){
				searchArray[i]=data[i].districtName;
				showDataArray[i]=data[i].districtName;
				hiddenDataArray[i]=data[i].districtId;
				districtHiddenIdArray[i]=data[i].districtId;
				hiddenExitURLArray[i] = data[i].exitURL;
				hiddenExitMessageArray[i] = data[i].exitMessage;
				hiddenFlagForMessageArray[i]=data[i].flagForMessage;
				hiddenDAUploadURLArray[i]=data[i].assessmentUploadURL;
				hiddenSAUploadURLArray[i]=null;
			}
		},
		errorHandler:handleError 
		});	
	}else {
		BatchJobOrdersAjax.getFieldOfSchoolList(0,districtOrSchoolName,{ 
			async: false,
			callback: function(data){
			hiddenDataArray = new Array();
			showDataArray = new Array();
			for(i=0;i<data.length;i++){
				searchArray[i]=data[i].schoolName;
				showDataArray[i]=data[i].schoolName;
				hiddenDataArray[i]=data[i].schoolMaster.schoolId;
				districtHiddenIdArray[i]=data[i].districtMaster.districtId;
				hiddenExitURLArray[i] = data[i].exitURL;
				hiddenExitMessageArray[i] = data[i].exitMessage;
				hiddenFlagForMessageArray[i]=data[i].flagForMessage;
				hiddenDAUploadURLArray[i]=data[i].districtMaster.assessmentUploadURL;;
				hiddenSAUploadURLArray[i]=data[i].assessmentUploadURL;
			}
		},
		errorHandler:handleError 
		});	
	}
	
	

	return searchArray;
}


var selectFirst="";

var fatchData= function(txtSearch,searchArray,txtId,txtdivid){
	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
				
			}
			
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}
function hideDistrictMasterDiv(dis,hiddenId,divId)
{	
	document.getElementById("districtOrSchooHiddenlId").value="";
	document.getElementById("districtSchoolHiddenId").value="";
	document.getElementById("exitURL").value="";
	$('#eMessage').find(".jqte_editor").html("");
	document.getElementById("districtHiddenId").value="";
	
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			document.getElementById("districtOrSchooHiddenlId").value=hiddenDataArray[index];
			document.getElementById("districtSchoolHiddenId").value=hiddenDataArray[index];
			document.getElementById("districtHiddenId").value=districtHiddenIdArray[index];
			
			try{
				document.getElementById("exitURL").value=hiddenExitURLArray[index];
			}catch(err){}
			try{
				$('#eMessage').find(".jqte_editor").html(hiddenExitMessageArray[index]);
			}catch(err){}
			try{
				var windowFunc=document.getElementById("windowFunc").value;
				if(hiddenFlagForMessageArray[index]==1){
					document.getElementById("exitURLMessageVal").value=2;
					document.getElementById("exitUrlMessage2").checked=true;
					//document.getElementById("showExitURLDiv").style.display='none';
					//document.getElementById("showExitMessageDiv").style.display='inline';
				}else{
					document.getElementById("exitURLMessageVal").value=1;
					document.getElementById("exitUrlMessage1").checked=true;
				}
				if(hiddenDAUploadURLArray[index]!=null && hiddenDAUploadURLArray[index]!=''){
					document.getElementById("hideDAUploadURL").style.display='inline';
					document.getElementById("DAssessmentUploadURL").value=hiddenDAUploadURLArray[index];
					document.getElementById("ViewDAURL").style.display='inline';
					document.getElementById("ViewDAURL").innerHTML="<a data-original-title='JSI' rel='tooltip' id='JSIdistrict3' href='javascript:void(0)' onclick=\"showFile('district','"+districtHiddenIdArray[index]+"','"+hiddenDAUploadURLArray[index]+"','JSIdistrict3');"+windowFunc+"\">view</a>";
				}else{
					document.getElementById("hideDAUploadURL").style.display='none';
					document.getElementById("ViewDAURL").style.display='none';
				}
				
				if(hiddenSAUploadURLArray[index]!=null  && hiddenSAUploadURLArray[index]!=''){
					document.getElementById("hideSAUploadURL").style.display='inline';
					document.getElementById("ViewSAURL").style.display='inline';
					document.getElementById("ViewSAURL").innerHTML="<a data-original-title='JSI' rel='tooltip' id='JSIschool3' href='javascript:void(0)' onclick=\"showFile('school','"+hiddenDataArray[index]+"','"+hiddenSAUploadURLArray[index]+"','JSIschool3');"+windowFunc+"\">view</a>";
				}else{
					document.getElementById("hideSAUploadURL").style.display='none';
					document.getElementById("ViewSAURL").style.display='none';
				}
				
			}catch(err){
				document.getElementById("exitURLMessageVal").value=1;
				document.getElementById("exitUrlMessage1").checked=true;
			}
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
	
	if($('#entityType').val()==1)
	{
		var a=	document.getElementById("districtOrSchooHiddenlId").value   
		if(!a=="")
			getDistrictObjByDistrictId();	
	}
	
}



count=0;
index=-1;
length=0;
divid='';
txtid='';
hiddenDataArray = new Array();
showDataArray = new Array();
degreeTypeArray = new Array();
hiddenId="";

function getSchoolAuto(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("schoolName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getSchoolArray(txtSearch.value);
		fatchData2(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getSchoolArray(schoolName){
	var searchArray = new Array();
	var JobOrderType	=	document.getElementById("JobOrderType").value;
	var districtId	=	document.getElementById("districtOrSchooHiddenlId").value;
		BatchJobOrdersAjax.getFieldOfSchoolList(districtId,schoolName,{ 
			async: false,
			callback: function(data){
			hiddenDataArray = new Array();
			showDataArray = new Array();
			for(i=0;i<data.length;i++){
				searchArray[i]=data[i].schoolName;
				showDataArray[i]=data[i].schoolName;
				hiddenDataArray[i]=data[i].schoolMaster.schoolId;
			}
		},
		errorHandler:handleError 
		});	
	return searchArray;
}


selectFirst="";

var fatchData2= function(txtSearch,searchArray,txtId,txtdivid){

	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
				
			}
			
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}
function hideSchoolMasterDiv(dis,hiddenId,divId)
{
	document.getElementById("schoolId").value="";
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			document.getElementById("schoolId").value=hiddenDataArray[index];
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}

var downArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index<length-1){
			for(var i=0;i<10;i++){
				{
					if(document.getElementById('divResult'+txtdivid+i))
						var div_id=document.getElementById('divResult'+txtdivid+i);
					if(div_id)
					{
						if(div_id.className=='over')
							index=div_id.id.split('divResult'+txtdivid)[1];
						div_id.className='normal';
					}
				}
			}
			index++;
			if(document.getElementById('divResult'+txtdivid+index))
			{
				var div_id=document.getElementById('divResult'+txtdivid+index);
				div_id.className='over';
				document.getElementById(txtid).value = div_id.innerHTML;
				selectFirst=div_id.innerHTML;
			}
		}
	}
}

var upArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index>0){
			for(var i=0;i<length;i++){

				var div_id=document.getElementById('divResult'+txtdivid+i);
				if(div_id)
				{
					if(div_id.className=='over')
						index=div_id.id.split('divResult'+txtdivid)[1];
					div_id.className='normal';
				}
			}
			index--;
			if(document.getElementById('divResult'+txtdivid+index))
				document.getElementById('divResult'+txtdivid+index).className='over';
			if(txtid && document.getElementById('divResult'+txtdivid+index))
			{
				document.getElementById(txtid).value=
					document.getElementById('divResult'+txtdivid+index).innerHTML;
				selectFirst=document.getElementById('divResult'+txtdivid+index).innerHTML;
			}
		}
	}
}

var overText=function (div_value,txtdivid) {

	for(var i=0;i<length;i++)
	{
		if(document.getElementById('divResult'+i))
		{
			var div_id=document.getElementById('divResult'+i);

			if(div_id.className=='over')
				index=div_id.id.split(txtdivid)[1];
			div_id.className='normal';
		}
	}
	div_value.className = 'over';
	document.getElementById(txtid).value= div_value.innerHTML;
}
function trim(s)
{
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}
function checkForInt(evt) {

	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==8)
		return true;

	return( (charCode >= 48 && charCode <= 57)||(charCode==13) );
}
function DateFormatChange(dateParam){
	da = new Date(dateParam)
	db = da.toGMTString()	// Convert to a String in "predictable formt"
	dc = db.split(" ")	// Split the string on spaces
	if ( eval( dc[3] ) < 1970 ) dc[3] = eval( dc[3] ) +100	// Correct any date purporting to be before 1970
	db = dc[2] + " " + dc[1] + ", " + dc[3]	// Ignore day of week and combine date, month and year
	return db;
}


function mouseOverChk(txtdivid,txtboxId)
{	
	for(var i=0;i<length;i++)
	{
		$('#divResult'+txtdivid+i).mouseover({param1: i,param2:txtdivid, param3:txtboxId}, fireMouseOverEvent);
	}
}


function fireMouseOverEvent(event)
{
	for(var i=0;i<length;i++)
	{	
		document.getElementById('divResult'+event.data.param2+i).className='normal';
	}
    document.getElementById('divResult'+event.data.param2+event.data.param1).className='over';	       
   	document.getElementById(event.data.param3).value= $('#divResult'+event.data.param2+event.data.param1).text();
    index=event.data.param1;
}
