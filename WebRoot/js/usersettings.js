var txtBgColor="#F5E7E1";
var xmlHttp=null;
function checkSession(responseText)
{
	if(responseText=='100001')
	{
		alert(resourceJSON.oops)
		window.location.href="signin.do";
	}	
}
function createXMLHttpRequest()
{
	if (window.ActiveXObject)
		xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
	else if (window.XMLHttpRequest)
		xmlHttp = new XMLHttpRequest();
	else
		alert(resourceJSON.msgAjaxNotSupported);
}
function trim(s)
{
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}


function isEmailAddress(str) 
{
	var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
	return emailPattern.test(str);
}
function deleteData(delUrl)
{
	if(window.confirm(resourceJSON.cnfMsgDelete))
	{
		var url = delUrl+"&td="+new Date().getTime();
		window.location.href=url;
	}
}


function showHideBankDiv(stBank,stChequeDD,stButton)
{
	document.getElementById("divBankList").style.display=stBank;
	document.getElementById("divChequeDD").style.display=stChequeDD;
	document.getElementById("divPayButton").style.display=stButton;
}

function isNumber(field) {
	var re = /^[0-9-'.'-',']*$/;
	//var re =/^(0\.|[1-9]\d*\.)d{2}/ 
	if (!re.test(field.value)) {
//		alert('Value must be all numberic charcters, including "." or "," non numerics will be removed from field!');
		field.value = field.value.replace(/[^0-9-'.'-',']/g,"");
	}
}
function checkForInt(evt) {

	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==8)
		return true;

	return( (charCode >= 48 && charCode <= 57)||(charCode==13) );
}

function setFocus(id)
{
	if(id!="")
		document.getElementById(id).focus();
}

function isEmailAddress(str) 
{
	var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
	return emailPattern.test(str);
}

function chkPwdPtrn(str)
{
	var pat1 = /[^a-zA-Z]/g; //If any spcl char find it return true otherwise false
	var pat2 = /[a-zA-Z]/g;  //If any alphabet found it return true otherwise false
	//alert(pat1.test(str) && pat2.test(str))
	return pat1.test(str) && pat2.test(str);
}

//alert(navigator.userAgent);

function checkEmailSetting(emailAddress)
{
	var res;
	createXMLHttpRequest();  
	queryString = "chkusersettingemailexist.do?emailAddress="+emailAddress+"&dt="+new Date().getTime();
	xmlHttp.open("POST", queryString, false);
	xmlHttp.onreadystatechange = function()
	{
		if(xmlHttp.readyState == 4)
		{
			if(xmlHttp.status==200)	
			{	
				checkSession(xmlHttp.responseText);
				res = xmlHttp.responseText;				
			}			
			else
			{
				alert(resourceJSON.msgServerErr);
			}
		}
	}
	xmlHttp.send(null);		
	if(res==1)
		return true;
	else
		return false;
}
function validateEditSettings()
{	


	var firstName = document.getElementById("firstName");
	var lastName = document.getElementById("lastName");
	var emailAddress = document.getElementById("emailAddress");

	var cnt=0;
	var focs=0;	
	$('#errordiv').empty();
	$('#divServerError').css("background-color","");
	$('#firstName').css("background-color","");
	$('#lastName').css("background-color","");
	$('#emailAddress').css("background-color","");

	if(trim(firstName.value)=="")
	{
		$('#errordiv').append("&#149; "+resourceJSON.PlzEtrFirstName+"<br>");
		if(focs==0)
			$('#firstName').focus();

		$('#firstName').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	if(trim(lastName.value)=="")
	{
		$('#errordiv').append("&#149; "+resourceJSON.PlzEtrLastName+"<br>");
		if(focs==0)
			$('#lastName').focus();

		$('#lastName').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	if(trim(emailAddress.value)=="")
	{
		$('#errordiv').append("&#149; "+resourceJSON.PlzEtrEmail+"<br>");
		if(focs==0)
			$('#emailAddress').focus();

		$('#emailAddress').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	else if(!isEmailAddress(trim(emailAddress.value)))
	{		
		$('#errordiv').append("&#149; "+resourceJSON.PlzEtrVldEmail+"<br>");
		if(focs==0)
			$('#emailAddress').focus();

		$('#emailAddress').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	else if(checkEmailSetting(trim(emailAddress.value)))
	{
		$('#errordiv').append("&#149; "+resourceJSON.msgaladyemail+" <br>");
		if(focs==0)
			$('#emailAddress').focus();

		$('#emailAddress').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	if(cnt==0)
	{
		$('#loadingDiv').show();
		return true;
	}
	else
	{
		$('#errordiv').show();
		return false;
	}
}

function showChangePWD()
{
	document.getElementById("oldpassword").value="";
	document.getElementById("newpassword").value="";
	document.getElementById("repassword").value="";
	$('#errordivPwd').empty();
	$('#divServerError').css("background-color","");
	$('#oldpassword').css("background-color","");
	$('#newpassword').css("background-color","");
	$('#repassword').css("background-color","");
	$('#myModal').modal('show');
	document.getElementById("oldpassword").focus();
	return false;
}


function chkPassword()
{
	var oldpassword = document.getElementById("oldpassword");
	var newpassword = document.getElementById("newpassword");
	var repassword = document.getElementById("repassword");


	var cnt=0;
	var focs=0;	
	$('#errordivPwd').empty();
	$('#divServerError').css("background-color","");
	$('#oldpassword').css("background-color","");
	$('#newpassword').css("background-color","");
	$('#repassword').css("background-color","");


	if(trim(oldpassword.value)=="")
	{
		$('#errordivPwd').append("&#149; "+resourceJSON.msgcurrentpsw+" <br>");
		if(focs==0)
			$('#oldpassword').focus();

		$('#oldpassword').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	else if(!chkPasswordExist(oldpassword.value))
	{
		$('#errordivPwd').append("&#149; "+resourceJSON.msgCurrentPassworddoesnotexis+" <br>");
		if(focs==0)
			$('#oldpassword').focus();

		$('#oldpassword').css("background-color",txtBgColor);
		cnt++;focs++;
	}

	if(trim(newpassword.value)=="")
	{
		$('#errordivPwd').append("&#149; "+resourceJSON.msgPleaseenterNewPassword+"<br>");
		if(focs==0)
			$('#newpassword').focus();

		$('#newpassword').css("background-color",txtBgColor);
		cnt++;focs++;
	} 
	else if(!chkPwdPtrn(newpassword.value) || newpassword.value.length<8 )
	{		
		$('#errordivPwd').append("&#149; "+resourceJSON.msgPassCobination8Char+" <br>");
		if(focs==0)
			$('#newpassword').focus();

		$('#newpassword').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	if(trim(repassword.value)=="")
	{
		$('#errordivPwd').append("&#149; "+resourceJSON.msgReenterPassword+"<br>");
		if(focs==0)
			$('#repassword').focus();

		$('#repassword').css("background-color",txtBgColor);
		cnt++;focs++;
	} 
	if(trim(newpassword.value)!="" && trim(repassword.value)!="" && !(trim(newpassword.value)==trim(repassword.value)))
	{
		$('#errordivPwd').append("&#149; "+resourceJSON.msgrepasswordnotmatch+" <br>");
		if(focs==0)
			$('#newpassword').focus();

		$('#newpassword').css("background-color",txtBgColor);
		$('#repassword').css("background-color",txtBgColor);
		cnt++;focs++;
	}

	if(cnt==0)
	{
		$('#loadingDiv').show();
		document.getElementById("password").value=newpassword.value;
		$('#errordivPwd').empty();
		$('#divServerError').css("background-color","");
		$('#oldpassword').css("background-color","");
		$('#newpassword').css("background-color","");
		$('#repassword').css("background-color","");



		createXMLHttpRequest();  
		queryString = "changeusrpwd.do?password="+newpassword.value+"&dt="+new Date().getTime();
		xmlHttp.open("POST", queryString, true);
		xmlHttp.onreadystatechange = function()
		{
			if(xmlHttp.readyState == 4)
			{
				if(xmlHttp.status==200)	
				{			
					checkSession(xmlHttp.responseText)
					$('#myModal').modal('hide');
					$('#loadingDiv').hide();
				}			
				else
				{
					alert(resourceJSON.msgServerErr);
				}
			}
		}
		xmlHttp.send(null);	


		return true;
	}
	else
	{
		$('#errordivPwd').show();
		return false;
	}

}




function chkPasswordExist(pwd)
{
	var res;
	createXMLHttpRequest();  
	queryString = "chkuserpwdexist.do?password="+pwd+"&dt="+new Date().getTime();
	xmlHttp.open("POST", queryString, false);
	xmlHttp.onreadystatechange = function()
	{
		if(xmlHttp.readyState == 4)
		{
			if(xmlHttp.status==200)	
			{		
				checkSession(xmlHttp.responseText)
				res = xmlHttp.responseText;				
			}			
			else
			{
				alert(resourceJSON.msgServerErr);
			}
		}
	}
	xmlHttp.send(null);		
	if(res==1)
		return true;
	else
		return false;

}



function chkForEnter(evt)
{

	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==13)
	{
		chkPassword();
		
	}
}

function chkForEnter1(evt)
{

	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==13)
	{
		return saveEmailRecord();
	}
}
function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert(resourceJSON.msgServerErr+": "+exception.javaClassName);}
}

function saveFacebookConfig(facebookId,accessToken)
{
	//alert(facebookId);
	//alert(accessToken);
	SocialServiceAjax.saveFacebookConfig(facebookId,accessToken,{ 
		async: false,
		callback: function(data){

		//alert(data);
		$("#fbImg").attr("src","images/on.png");
		if(data==1)
		{
			$('#message2show').html(""+resourceJSON.msgFacebookConfig+"");
			$('#myModal2').modal('show');
		}
		else
		{
			var obj = eval ("(" + data + ")");
			//alert(obj);
			var html = resourceJSON.msgFacebookSave+" \""+resourceJSON.BtnSave+"\" "+resourceJSON.msgButtonClick+" \""+resourceJSON.BtnSave+"\" "+resourceJSON.msgButtonSelect+"<br><br>";
			$.each(obj, function(i, item) {
				//alert(item.access_token);
				html+="<label class='radio'><input type='radio' name='pages' value='"+item.id+"@@@"+item.access_token+"'>"+item.name+"</label>";
			});
			html+="</div>";

			$("#myModalvk").css({ top: '60%' });
			$('#warningImg1k').html("");
			$('#message2showConfirm1k').html("");
			$('#nextMsgk').html(html);
			$('#footerbtn1k').html("<button class='btn btn-primary' data-dismiss='modal' aria-hidden='true' onclick=\"setPageOption();\">"+resourceJSON.BtnSave+"</button>&nbsp;<button class='btn' data-dismiss='modal' aria-hidden='true' >"+resourceJSON.btnCancel+"</button>");
			$('#vcloseBtnk').html("<span>x</span>");
			$('#myModalvk').modal('show');

			//$('#message2show').html(html);
		}

		FB.logout();
	},
	errorHandler:handleError  
	});

}
function setPageOption()
{
	//alert("hi");
	var optId="";
	//alert($('input[name=pages]:radio:checked').length);
	if($('input[name=pages]:radio:checked').length > 0 )
	{
		optId=$('input[name=pages]:radio:checked').val();
		//alert(optId);
		SocialServiceAjax.savePageId(optId,{ 
			async: true,
			callback: function(data){

			//alert(data);
			$('#myModalTwr').modal('hide');
			$('#message2show').html(""+resourceJSON.msgFacebookConfigPage+"");
			$('#myModal2').modal('show');
		},
		errorHandler:handleError  
		});
	}
}
function showTwitterConfigDiv()
{

	SocialServiceAjax.getTwitterConfig({ 
		async: true,
		callback: function(data){

		var obj = eval ("(" + data + ")");
		//alert(obj.accessToken);
		//alert(obj.consumerKey);
		document.getElementById("consumerKey").value=obj.consumerKey;
		document.getElementById("consumerSecret").value=obj.consumerSecret;
		document.getElementById("accessToken").value=obj.accessToken;
		document.getElementById("accessTokenSecret").value=obj.accessTokenSecret;
		document.getElementById("twitterConfigId").value=obj.twitterConfigId;

		$('#myModalTwr').modal('show');
		document.getElementById("consumerKey").focus();
	},
	errorHandler:handleError  
	});


	$('#errordivTwr').empty();
	$('#consumerKey').css("background-color","");
	$('#consumerKeySecret').css("background-color","");
	$('#accessToken').css("background-color","");
	$('#accessTokenSecret').css("background-color","");

	return false;
}
function validateTwiterConfig()
{

	$('#errordivTwr').empty();
	$('#consumerKey').css("background-color", "");
	$('#consumerSecret').css("background-color", "");
	$('#accessToken').css("background-color", "");
	$('#accessTokenSecret').css("background-color", "");

	var cnt=0;
	var focs=0;

	if(trim($('#consumerKey').val())=="")
	{
		$('#errordivTwr').append("&#149; "+resourceJSON.msgConsumerKey+"<br>");
		if(focs==0)
			$('#consumerKey').focus();
		$('#consumerKey').css("background-color", "#F5E7E1");
		cnt++;focs++;
	}
	if(trim($('#consumerSecret').val())=="")
	{
		$('#errordivTwr').append("&#149; "+resourceJSON.msgConsumerSecret+"<br>");
		if(focs==0)
			$('#consumerSecret').focus();
		$('#consumerSecret').css("background-color", "#F5E7E1");
		cnt++;focs++;
	}
	if(trim($('#accessToken').val())=="")
	{
		$('#errordivTwr').append("&#149; "+resourceJSON.msgAccessToken+"<br>");
		if(focs==0)
			$('#accessToken').focus();
		$('#accessToken').css("background-color", "#F5E7E1");
		cnt++;focs++;
	}
	if(trim($('#accessTokenSecret').val())=="")
	{
		$('#errordivTwr').append("&#149; "+resourceJSON.msgAccessTokenSecret+"<br>");
		if(focs==0)
			$('#accessTokenSecret').focus();
		$('#accessTokenSecret').css("background-color", "#F5E7E1");
		cnt++;focs++;
	}
	if(cnt==0)
		return true;
	else
	{
		$('#errordivTwr').show();
		return false;
	}
}
function saveTwiterConfig()
{

	if(!validateTwiterConfig())
		return;

	var twitterConfigId = document.getElementById("twitterConfigId").value;
	var consumerKey = document.getElementById("consumerKey").value;
	var consumerSecret = document.getElementById("consumerSecret").value;
	var accessToken = document.getElementById("accessToken").value;
	var accessTokenSecret = document.getElementById("accessTokenSecret").value;

	SocialServiceAjax.saveTwiterConfig(twitterConfigId,consumerKey,consumerSecret,accessToken,accessTokenSecret,{ 
		async: true,
		callback: function(data){

		$("#twrImg").attr("src","images/on.png");
		//alert(data);
		$('#myModalTwr').modal('hide');
		$('#message2show').html(""+resourceJSON.msgTwitterconfigurations+"");
		$('#myModal2').modal('show');
	},
	errorHandler:handleError  
	});

}
function onLinkedIn()
{
	$("#inImg").attr("src","images/on.png");
	$('#message2show').html(resourceJSON.msgLinkedInConfigurations);
	$('#myModal2').modal('show');
}

//added by Amit
function displayRecordsByDistrictAndEmail()
{
	$('#loadingDiv').fadeIn();
	var DistrictId = document.getElementById("DistrictId").value;
	var emailAddress = document.getElementById("emailAddress").value;
	//alert("DistrictId=="+DistrictId+" emailAddress=="+emailAddress);
	SocialServiceAjax.displayRecordsByDistrictAndEmail(DistrictId,emailAddress,
	{
		async: true,
		callback: function(data)
		{	
			document.getElementById("divMainOfList").innerHTML	=data;
			$('#loadingDiv').hide();
			applyScrollOnTblList();
		},
		errorHandler:handleError
	});
}

function changeLocation()
{
	var userId = $('input:radio[name=cl]:checked').val();
	$('#loadingDiv').fadeIn();
	var DistrictId = document.getElementById("DistrictId").value;
	var emailAddress = document.getElementById("emailAddress").value;
	SocialServiceAjax.changeLocation(userId,
	{
		async: true,
		callback: function(data)
		{	
			$('#loadingDiv').hide();
			if(data=="true")
			{
				window.location.href="tmdashboard.do";
			}
			else if(data=="true#Branch"){
				window.location.href="kesdashboard.do";
			}
			else
			{
				$('#message2show').html(resourceJSON.msgSwitchLocaion);
				$('#myModal2').modal('show');
			}
		},
		errorHandler:handleError
	});
}

////////////////////shriram//////////////


function emailValidate()
{
document.getElementById('emailIdError').innerHTML="";
document.getElementById('passwordError').innerHTML="";
document.getElementById('mailHostError').innerHTML="";
document.getElementById('mailPortError').innerHTML="";
	var mailId=true;
	var password=true;
	var host=true;
	var port=true;
	$("#mailPort").val($('#mailPort').val().replace(/^\s\s*/, '').replace(/\s\s*$/, ''))
	$("#mailHost").val($('#mailHost').val().replace(/^\s\s*/, '').replace(/\s\s*$/, ''))
	$("#personalEmailAddress").val($('#personalEmailAddress').val().replace(/^\s\s*/, '').replace(/\s\s*$/, ''))
	$("#mailPassword").val($('#mailPassword').val().replace(/^\s\s*/, '').replace(/\s\s*$/, ''));
	if(isNaN ($("#mailPort").val().trim()))
		port=false;
	
	if($("#mailPort").val().trim() == '')
	port=false;
	
		if($("#mailHost").val().trim() == '')
			host=false;
			if($("#mailPassword").val().trim() == '')
			password=false;
			if($("#personalEmailAddress").val().trim() == '')
					mailId=false
					 var emailfilter=/^\w+[\+\.\w-]*@([\w-]+\.)*\w+[\w-]*\.([a-z]{2,4}|\d+)$/i;
					var b=emailfilter.test($("#personalEmailAddress").val().trim());
					if(b==false)
					{
						mailId=false
					}
					
					
					
					if(port==false || host==false || password==false || mailId==false )					
					{			
						if(isNaN ($("#mailPort").val().trim()))
						{
							document.getElementById('mailPortError').innerHTML="Please enter Mail Port only in numeric <br>";
							document.getElementById('mailPort').focus();
							
						}
						
						if($("#mailPort").val().trim() == '')
						{
							document.getElementById('mailPortError').innerHTML="Please enter Mail Port <br>";
							document.getElementById('mailPort').focus();

						}
						if($("#mailHost").val().trim() == '')
						{
							document.getElementById('mailHostError').innerHTML="Please enter Mail Host <br>";
							document.getElementById('mailHost').focus();
						}
						if($("#mailPassword").val().trim() == '')
						{
							document.getElementById('passwordError').innerHTML="please enter Email Password <br>";
							document.getElementById('mailPassword').focus();
						}
						if($("#personalEmailAddress").val().trim() == '')
						{	
							document.getElementById('emailIdError').innerHTML="Please enter Email Address <br>";
							document.getElementById('personalEmailAddress').focus();
						}
						else if(b==false)
						{
							document.getElementById('emailIdError').innerHTML="Please enter valid Email Address <br>";
							document.getElementById('personalEmailAddress').focus();
						}
						$("#errorDiv").show();
						return false;
					}
return true;
}

//Fetch the record of the user email credentials//////
function fetchEmailRecord()
{	
	$("#errorDiv").hide();
	var userId=$("#userId").val();
	PersonalEmailTrackAjax.getPErsonalEmailId(userId,{
		  async: true,
		 errorHandler:handleError,
		  
		  callback:  function(data)
		  {		
		if(data=="null" || data==""){					
			$("#emailCrid").modal('show');	
			document.getElementById("personalEmailAddress").value="";
			document.getElementById("mailPassword").value="";
						
			document.getElementById("personalEmailAddress").focus();}
		else
		{
		var personalEmailTrack= jQuery.parseJSON(data);
		$("#userId").val(personalEmailTrack.userId);
		$("#personalEmailAddress").val(personalEmailTrack.personalEmailAddress);
		$("#mailPassword").val(personalEmailTrack.personalEmailPassword);
		$("#mailHost").val(personalEmailTrack.mailHost);
		$("#mailPort").val(personalEmailTrack.mailPort);
		
		$("#emailCrid").modal('show');
		document.getElementById("personalEmailAddress").focus();
		}
		  },
		  errorHandler:handleError
		  
		 });
	
}





function saveEmailRecord()
{
	if(emailValidate())
	{
		$("#errorDiv").hide();
		
var emailId=document.getElementById("personalEmailAddress").value;

 var password=document.getElementById("mailPassword").value;

 var hostName=document.getElementById("mailHost").value;
 var hostPort=document.getElementById("mailPort").value
 var userId=document.getElementById("userId").value
 //alert("inside saveeeeeeee"+emailId+"----------"+password+"--------"+hostName+"------------"+hostPort+"--------"+userId);
  
 PersonalEmailTrackAjax.saveEmailRecords(userId,emailId,password,hostName,hostPort,{
  async: true,
  errorHandler:handleError,
  callback:  function(data)
  {
	 //$("#emailCrid").hide(); 
	 $('#emailCrid').modal('hide');
  },
  errorHandler:handleError
 });
}
	else {
		$("#errorDiv").show();
		return false;
	}
}




