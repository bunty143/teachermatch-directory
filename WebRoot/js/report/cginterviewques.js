//jobId,teacherId,i4QuesNo
function displayIIQuesForRed(jobId,teacherId)
{
	var iiFlag = "red";
	CGInviteInterviewAjax.displayInviteInterviewQues(jobId,teacherId,iiFlag,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#interviewQuesRed').modal('show');
			$('#interviewQuesRedBody').html(data);
			
			if($("#quesSize").val()=="0")
				$("#inviteOK").hide();
			else
				$("#inviteOK").show();
		}
	});
	
}

function displayIIQuesForGray(jobId,teacherId)
{
	var iiFlag = "grey";
	
	CGInviteInterviewAjax.displayInviteInterviewQues(jobId,teacherId,iiFlag,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#interviewQuesGrey').modal('show');
			$('#interviewQuesGreyBody').html(data);
			
			if($("#quesSize").val()=="0")
				$("#reInviteOK").hide();
			else
				$("#reInviteOK").show();
		}
	});
	
}

function displayIIQuesForQrange(jobId,teacherId)
{
	
	var iiFlag = "orange";
	
	CGInviteInterviewAjax.displayInviteInterviewQues(jobId,teacherId,iiFlag,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#interviewQuesOrange').modal('show');
			$('#interviewQuesOrangeBody').html(data);
		}
	});
}

function saveIIStatus()
{
	 var teacherIdForII = $("#teacherIdForII").val();
	 var jobIdForII		= $("#jobIdForII").val();
	 var quesSetId		= $("#quesSetId").val();
	
	 CGInviteInterviewAjax.saveIIStatus(jobIdForII,teacherIdForII,quesSetId,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#interviewQuesRed').modal('hide');
			getCandidateGrid();
			refreshStatus();
		}
	});
}

function saveIIStatusForGrey()
{
	 var teacherIdForII = $("#teacherIdForII").val();
	 var jobIdForII		= $("#jobIdForII").val();
	 var quesSetId		= $("#quesSetId").val();
	
	 CGInviteInterviewAjax.saveIIStatus(jobIdForII,teacherIdForII,quesSetId,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#interviewQuesGrey').modal('hide');
			getCandidateGrid();
			refreshStatus();
		}
	});
}


function openInterViewLink(jobId,teacherId,linkId)
{
	
	//alert(" jobId "+jobId+" teacherId "+teacherId+" linkId "+linkId);
	
	CGInviteInterviewAjax.openInterviewURL(jobId,teacherId,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#videoInterViewLinkDiv').modal('show');
			document.getElementById("interViewDiv").innerHTML=data[0];
			
			//alert(data[1]);
			
			if(data[1]=='true')
				$("#saveVVIId").show();
			else if(data[1]=='false')
				$("#saveVVIId").hide();
		}
	});
}

function saveInterviewValue()
{
	var iframeNorm = document.getElementById('ifrmForVideoII');
	var innerNorm = iframeNorm.contentDocument || iframeNorm.contentWindow.document;
	var inputNormScore = innerNorm.getElementById('normScoreFrm');
	document.getElementById("ifrmForVideoII").value=inputNormScore.value;
	
	var inviteInterviewID = $("#inviteInterviewID").val();
	
	CGInviteInterviewAjax.saveInterviewValue(inviteInterviewID,inputNormScore.value,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#videoInterViewLinkDiv').modal('hide');
		}
	});
}

function getVideoURL(vviUrl)
{
	$('#loadingDiv').show();
	CGInviteInterviewAjax.getPathOfVideoUrl(vviUrl,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			if(data!=null && data!=""){
				$('#videoInterViewLinkDiv').modal('show');
				document.getElementById("ifrmTrans").src=data;
				$('#loadingDiv').hide();
			}
		}
	});
}

