//for Involvement
var dp_Involvement_Rows=10;
var dp_Involvement_page=1;
var dp_Involvement_sortOrderStr="";
var dp_Involvement_sortOrderType="";


var deviceTypeAndroid=$.browser.device = (/Android|webOS|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent.toLowerCase()));
//var deviceType=$.browser.device = (/iPhone|iPad|iPod/i.test(navigator.userAgent.tgetHonorsGridoLowerCase()));

var orderColumn = "normScore";
var sortingOrder = "0";

var expanddiv=0;
var totalNoOfRecord=0;
var sectionRecordVlt=0;
var sectionRecordRemove=0;
var sectionRecordInter=0;
var sectionRecordInComp=0;
var sectionRecordWithDr=0;
var sectionRecordHired=0;
var statusTxt='',statusValue='';
var jobForTeacherActionId=0;
var statusPrev="";
var jobForTeacherGId=0;


var page = 1;
var noOfRows = 10;
var sortOrderStr="";
var sortOrderType="";
var txtBgColor="#F5E7E1";

var pageForTC = 1;
var sortOrderStrForTC="";
var sortOrderTypeForTC="";
var noOfRowsForTC = 100;
var teacherId='';
var currentPageFlag="";


var pageMsg = 1;
var sortOrderStrMsg="";
var sortOrderTypeMsg="";
var noOfRowsMsg = 10;

var pagePhone = 1;
var sortOrderStrPhone ="";
var sortOrderTypePhone ="";
var noOfRowsPhone = 10;

var pageSTT = 1;
var sortOrderStrSTT ="";
var sortOrderTypeSTT ="";
var noOfRowsSTT = 10;

var pageUS = 1;
var sortOrderStrUS="";
var sortOrderTypeUS ="";
var noOfRowsUS = 10;

var pageJob = 1;
var sortOrderStrJob="";
var sortOrderTypeJob ="";
var noOfRowsJob= 10;

var pageSNote = 1;
var noOfRowsSNote=10;
var sortOrderStrSNote="";
var sortOrderTypeSNote = 10;

//Start for Profilr Div Grid

//videoLink
var pagevideoLink = 1;
var sortOrderStrvideoLink="";
var sortOrderTypevideoLink ="";
var noOfRowsvideoLink= 10;

//References
var pageReferences = 1;
var sortOrderStrReferences="";
var sortOrderTypeReferences ="";
var noOfRowsReferences= 10;

//workExp
var pageworkExp = 1;
var sortOrderStrworkExp="";
var sortOrderTypeworkExp ="";
var noOfRowsworkExp= 10;

//tpvh - teacherprofilevisithistory
var pagetpvh = 1;
var sortOrderStrtpvh="";
var sortOrderTypetpvh ="";
var noOfRowstpvh= 10;

//teacherAce
var pageteacherAce = 1;
var sortOrderStrteacherAce="";
var sortOrderTypeteacherAce ="";
var noOfRowsteacherAce= 10;

//teacherCerti
var pageteacherCerti = 1;
var sortOrderStrteacherCerti="";
var sortOrderTypeteacherCerti ="";
var noOfRowsteacherCerti= 10;

//for Additional Document
var pageaddDoc= 1;
var noOfRowsaddDoc= 10;
var sortOrderStraddDoc="";
var sortOrderTypeaddDoc="";
//for language profiency
var pageLang= 1;
var noOfRowsLang= 10;
var sortOrderStrLang="";
var sortOrderTypeLang="";

//End for Profilr Div Grid

var orderColumn = "normScore";
var sortingOrder = "0";

var expanddiv=0;
var totalNoOfRecord=0;
var sectionRecordVlt=0;
var sectionRecordRemove=0;
var sectionRecordInter=0;
var sectionRecordInComp=0;
var sectionRecordWithDr=0;
var sectionRecordHired=0;
var statusTxt='',statusValue='';
var jobForTeacherActionId=0;
var statusPrev="";
var jobForTeacherGId=0;

//For Assessment
var pageASMT 				= 	1;
var noOfRowsASMT			=	10;
var sortOrderStrASMT		=	"";
var sortOrderTypeASMT 		= 	10;

//teacher assessemnt
var pageEJ= 1;
var noOfRowsEJ= 10;
var sortOrderStrEJ="";
var sortOrderTypeEJ="";

function defaultSaveDiv(){
	pageSTT = 1;
	sortOrderStrSTT ="";
	sortOrderTypeSTT ="";
	noOfRowsSTT = 10;
	currentPageFlag="stt";
}
function defaultShareDiv(){
	pageUS = 1;
	sortOrderStrUS="";
	sortOrderTypeUS ="";
	noOfRowsUS = 10;
	currentPageFlag="us";
}
function getPaging(pageno)
{
	if(currentPageFlag=="statusNote"){
		if(pageno!='')
		{
			pageSNote=pageno;	
		}
		else
		{
			pageSNote=1;
		}
		noOfRowsSNote = document.getElementById("pageSize3").value;
		var statusId=document.getElementById("statusIdForStatusNote").value;
		var secondaryStatusId=document.getElementById("secondaryStatusIdForStatusNote").value;
		var fitScore=document.getElementById("fitScoreForStatusNote").value;
		jWTeacherStatusNotesOnOpen(fitScore,statusId,secondaryStatusId);
	}else if(currentPageFlag=="job"){
		if(pageno!='')
		{
			pageJob=pageno;	
		}
		else
		{
			pageJob=1;
		}
		noOfRowsForJob = document.getElementById("pageSize1").value;
		getJobOrderList();
	}
	else if(currentPageFlag=="workExp"){
		if(pageno!='')
		{
			pageworkExp=pageno;	
		}
		else
		{
			pageworkExp=1;
		}
		noOfRowsworkExp = document.getElementById("pageSize11").value;
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getPFEmploymentGrid_DivProfile(teacherIdForprofileGrid);
	}
	else if(currentPageFlag=="References"){
		if(pageno!='')
		{
			pageReferences=pageno;	
		}
		else
		{
			pageReferences=1;
		}
		noOfRowsReferences = document.getElementById("pageSize12").value;
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getElectronicReferencesGrid_DivProfile(teacherIdForprofileGrid)
		
	}
	else if(currentPageFlag=="videoLink"){
		if(pageno!='')
		{
			pagevideoLink=pageno;
		}
		else
		{
			pagevideoLink=1;
		}
		noOfRowsvideoLink = document.getElementById("pageSize13").value;
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getVideoLinksGrid_DivProfile(teacherIdForprofileGrid);
	}
	else if(currentPageFlag=="document"){
		if(pageno!='')
		{
			pageaddDoc=pageno;
		}
		else
		{
			pageaddDoc=1;
		}
		noOfRowsaddDoc = document.getElementById("pageSize15").value;
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getadddocumentGrid_DivProfile(teacherIdForprofileGrid);
	}else if(currentPageFlag=="languageProfiency"){
		if(pageno!='')
		{
			pageLang=pageno;
		}
		else
		{
			pageLang=1;
		}
		noOfRowsLang = document.getElementById("pageLangProf").value;
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getlanguage_DivProfile(teacherIdForprofileGrid);
	}else if(currentPageFlag=="assessmentDetails"){
		if(pageno!='')
		{
			pageASMT=pageno;	
		}
		else
		{
			pageASMT=1;
		}
		teacherId 		= document.getElementById("teacherIdForReference").value;
	//	teacherDetails	= document.getElementById("teacherDetailsForReference").value;
		noOfRowsASMT 	= document.getElementById("pageSize3").value;
		getdistrictAssessmetGrid_DivProfile(teacherId);
	}
	else if(currentPageFlag=="tpvh"){
		if(pageno!='')
		{
			pagetpvh=pageno;
		}
		else
		{
			pagetpvh=1;
		}
		noOfRowstpvh = document.getElementById("pageSize14").value;
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getTeacherProfileVisitHistoryShow(teacherIdForprofileGrid);
	}
	else if(currentPageFlag=="teacherAce"){
		if(pageno!='')
		{
			pageteacherAce=pageno;
		}
		else
		{
			pageteacherAce=1;
		}
		noOfRowsteacherAce = document.getElementById("pageSize10").value;
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getTeacherAcademicsGrid_DivProfile(teacherIdForprofileGrid);
	}
	else if(currentPageFlag=="teacherCerti"){
		if(pageno!='')
		{
			pageteacherCerti=pageno;
		}
		else
		{
			pageteacherCerti=1;
		}
		noOfRowsteacherCerti = document.getElementById("pageSize9").value;
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getTeacherCertificationsGrid_DivProfile(teacherIdForprofileGrid);
	}
	else if(currentPageFlag=="us"){
		if(pageno!='')
		{
			pageUS=pageno;	
		}
		else
		{
			pageUS=1;
		}
		noOfRowsUS = document.getElementById("pageSize3").value;
		
		var userFPS=document.getElementById("userFPS").value;
		var userFPD=document.getElementById("userFPD").value;
		var userCPS=document.getElementById("userCPS").value;
		var userCPD=document.getElementById("userCPD").value;
		if(userFPS==1){
			searchUser(0);
		}else if(userFPD==1){
			var jftTeacherIdF=document.getElementById("teacherIdFromSharePoPUp").value;
			var jftTxtFlagF=document.getElementById("txtteacherIdShareflagpopover").value;
			displayUsergrid(jftTeacherIdF,jftTxtFlagF);
		}else if(userCPS==1){
			searchUserthroughPopUp(0);
		}else if(userCPD==1){
			var jftTeacherId=document.getElementById("JFTteachetIdFromSharePoPUp").value;
			var jftTxtFlag=document.getElementById("txtflagSharepopover").value;
			displayShareFolder(jftTeacherId,jftTxtFlag);
		}
	}else if(currentPageFlag=="stt"){
		if(pageno!='')
		{
			pageSTT=pageno;	
		}
		else
		{
			pageSTT=1;
		}
		noOfRowsSTT = document.getElementById("pageSize3").value;
		var folderId = document.getElementById("folderId").value;
		var checkboxshowHideFlag= document.getElementById("checkboxshowHideFlag").value;
		var pageFlag= document.getElementById("pageFlag").value;
		if(pageFlag==1)
		{
			folderId = document.getElementById("frame_folderId").value;
		}	
		
		displaySavedCGgridByFolderId(pageFlag,folderId,pageFlag);
	}else if(currentPageFlag=="phone"){
		if(pageno!='')
		{
			pagePhone=pageno;	
		}
		else
		{
			pagePhone=1;
		}
		noOfRowsPhone = document.getElementById("pageSize3").value;
		var teacherIdp = document.getElementById("teacherDetailId").value;
		jobForTeacherGId=document.getElementById("jobForTeacherGId").value;
		getPhoneDetail(teacherIdp,jobForTeacherGId,0);
	}else if(currentPageFlag=="msg"){
		if(pageno!='')
		{
			pageMsg=pageno;	
		}
		else
		{
			pageMsg=1;
		}
		noOfRowsMsg = document.getElementById("pageSize3").value;
		var teacherIdp = document.getElementById("teacherDetailId").value;
		var jobIdp=document.getElementById("jobId").value;
		var emailIdp=document.getElementById("emailId").value;
		getMessageDiv(teacherIdp,emailIdp,jobIdp,0);
	}
	else if(currentPageFlag=="teacherInvolve"){
		if(pageno!='')
		{
			dp_Involvement_page=pageno;	
		}
		else
		{
			dp_Involvement_page=1;
		}
		teacherId 		= document.getElementById("teacherIdForReference").value;
	//	teacherDetails	= document.getElementById("teacherDetailsForReference").value;
		dp_Involvement_Rows 	= document.getElementById("pageSize16").value;
		getInvolvementGrid(teacherId);
		//getdistrictAssessmetGrid_DivProfile(teacherId);
	}
	else{
	
		if(pageno!='')
		{
			page=pageno;	
		}
		else
		{
			page=1;
		}
	
		noOfRows = document.getElementById("pageSize3").value;
		teacherId = document.getElementById("teacherId").value;
		getNotesDiv(teacherId);
	}
	
}

/// ***********(Lines (399 to 761 ))************* => by khan  

function getHonorsGrid(teacherId)
{
    var visitLocation;
    try
    {
    	visitLocation=document.getElementById("teacherIdForprofileGridVisitLocation").value;
    }
    catch(err){}
	 TeacherProfileViewInDivAjax.getHonorsGridWithoutAction(teacherId,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			document.getElementById("gridDataTeacherHonors").innerHTML=data;
			applyScrollOnHonors();
			
		}});
}


function getInvolvementGrid(teacherId)
{
	TeacherProfileViewInDivAjax.getInvolvementGrid(teacherId,dp_Involvement_Rows,dp_Involvement_page,dp_Involvement_sortOrderStr,dp_Involvement_sortOrderType,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#gridDataTeacherInvolvementOrVolunteerWork').html(data);
			applyScrollOnInvl();
		}});
}
function clickedOnYes()
{
	$("#vetranOptionDiv").show();
}

function clickedOnNo()
{
	$("#vetranOptionDiv").hide();
}

function setGridProfileVariable(currentPageFlagValue){
	
	currentPageFlag=currentPageFlagValue;
	
}

function getPagingAndSorting(pageno,sortOrder,sortOrderTyp){
	//var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
	
	if(currentPageFlag=="tran" || currentPageFlag=="cert"){
		if(pageno!=''){
			pageForTC=pageno;	
		}else{
			pageForTC=1;
		}
		sortOrderStrForTC	=	sortOrder;
		sortOrderTypeForTC	=	sortOrderTyp;
		if(document.getElementById("pageSize3")!=null){
			noOfRowsTranCert = document.getElementById("pageSize3").value;
		}else{
			noOfRowsTranCert=100;
		}
		if(currentPageFlag=="tran"){
			getTranscriptGridNew();
		}else{
			getCertificationGridNew();
		}
	}else if(currentPageFlag=="msg"){
		if(pageno!=''){
			pageMsg=pageno;	
		}else{
			pageMsg=1;
		}
		sortOrderStrMsg	=	sortOrder;
		sortOrderTypeMsg	=	sortOrderTyp;
		if(document.getElementById("pageSize3")!=null){
			noOfRowsMsg = document.getElementById("pageSize3").value;
		}else{
			noOfRowsMsg=10;
		}
		var teacherIdp = document.getElementById("teacherDetailId").value;
		var jobIdp=document.getElementById("jobId").value;
		var emailIdp=document.getElementById("emailId").value;
		getMessageDiv(teacherIdp,emailIdp,jobIdp,0);
	}else if(currentPageFlag=="phone"){
		if(pageno!=''){
			pagePhone=pageno;	
		}else{
			pagePhone=1;
		}
		sortOrderStrPhone	=	sortOrder;
		sortOrderTypePhone	=	sortOrderTyp;
		if(document.getElementById("pageSize3")!=null){
			noOfRowsPhone = document.getElementById("pageSize3").value;
		}else{
			noOfRowsPhone=10;
		}
		var teacherIdp = document.getElementById("teacherDetailId").value;
		jobForTeacherGId=document.getElementById("jobForTeacherGId").value;
		getPhoneDetail(teacherIdp,jobForTeacherGId,0);
	}else if(currentPageFlag=="stt"){
		if(pageno!=''){
			pageSTT=pageno;	
		}else{
			pageSTT=1;
		}
		sortOrderStrSTT	=	sortOrder;
		sortOrderTypeSTT	=	sortOrderTyp;
		if(document.getElementById("pageSize3")!=null){
			noOfRowsSTT = document.getElementById("pageSize3").value;
		}else{
			noOfRowsSTT=10;
		}
		var folderId = document.getElementById("folderId").value;
		var checkboxshowHideFlag= document.getElementById("checkboxshowHideFlag").value;
		var pageFlag= document.getElementById("pageFlag").value;
		if(pageFlag==1)
		{
			folderId = document.getElementById("frame_folderId").value;
		}	
		
		//alert(" folderId "+folderId+" pageFlag "+pageFlag);
		displaySavedCGgridByFolderId(pageFlag,folderId,pageFlag);
	}else if(currentPageFlag=="us"){
		if(pageno!=''){
			pageUS=pageno;	
		}else{
			pageUS=1;
		}
		sortOrderStrUS	=	sortOrder;
		sortOrderTypeUS	=	sortOrderTyp;
		if(document.getElementById("pageSize3")!=null){
			noOfRowsUS = document.getElementById("pageSize3").value;
		}else{
			noOfRowsUS=10;
		}
		var userFPS=document.getElementById("userFPS").value;
		var userFPD=document.getElementById("userFPD").value;
		var userCPS=document.getElementById("userCPS").value;
		var userCPD=document.getElementById("userCPD").value;
		
		if(userFPS==1){
			searchUser(0);
		}else if(userFPD==1){
			var jftTeacherIdF=document.getElementById("teacherIdFromSharePoPUp").value;
			var jftTxtFlagF=document.getElementById("txtteacherIdShareflagpopover").value;
			displayUsergrid(jftTeacherIdF,jftTxtFlagF);
		}else if(userCPS==1){
			searchUserthroughPopUp(0);
		}else if(userCPD==1){
			var jftTeacherId=document.getElementById("JFTteachetIdFromSharePoPUp").value;
			var jftTxtFlag=document.getElementById("txtflagSharepopover").value;
			displayShareFolder(jftTeacherId,jftTxtFlag);
		}
	}else if(currentPageFlag=="job"){
		if(pageno!=''){
			pageJob=pageno;	
		}else{
			pageJob=1;
		}
		sortOrderStrJob	=	sortOrder;
		sortOrderTypeJob	=	sortOrderTyp;
		if(document.getElementById("pageSize1")!=null){
			noOfRowsJob = document.getElementById("pageSize1").value;
		}else{
			noOfRowsJob=10;
		}
		
		getJobOrderList();
	}else if(currentPageFlag=="statusNote"){
		if(pageno!=''){
			pageSNote=pageno;	
		}else{
			pageSNote=1;
		}
		sortOrderStrSNote=	sortOrder;
		sortOrderTypeSNote	=	sortOrderTyp;
		if(document.getElementById("pageSize3")!=null){
			noOfRowsJobSNote = document.getElementById("pageSize3").value;
		}else{
			noOfRowsJobSNote=10;
		}
		var statusId=document.getElementById("statusIdForStatusNote").value;
		var secondaryStatusId=document.getElementById("secondaryStatusIdForStatusNote").value;
		var fitScore=document.getElementById("fitScoreForStatusNote").value;
		jWTeacherStatusNotesOnOpen(fitScore,statusId,secondaryStatusId);

	}else if(currentPageFlag=="workExp"){
		if(pageno!=''){
			pageworkExp=pageno;	
		}else{
			pageworkExp=1;
		}
		sortOrderStrworkExp	=	sortOrder;
		sortOrderTypeworkExp	=	sortOrderTyp;
		if(document.getElementById("pageSize11")!=null){
			noOfRowsworkExp = document.getElementById("pageSize11").value;
		}else{
			noOfRowsworkExp=10;
		}
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getPFEmploymentGrid_DivProfile(teacherIdForprofileGrid);
	}else if(currentPageFlag=="References"){
		if(pageno!=''){
			pageReferences=pageno;	
		}else{
			pageReferences=1;
		}
		sortOrderStrReferences	=	sortOrder;
		sortOrderTypeReferences	=	sortOrderTyp;
		if(document.getElementById("pageSize12")!=null){
			noOfRowsReferences = document.getElementById("pageSize12").value;
		}else{
			noOfRowsReferences=10;
		}
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getElectronicReferencesGrid_DivProfile(teacherIdForprofileGrid);
	}else if(currentPageFlag=="videoLink"){
		if(pageno!=''){
			pagevideoLink=pageno;	
		}else{
			pagevideoLink=1;
		}
		sortOrderStrvideoLink	=	sortOrder;
		sortOrderTypevideoLink	=	sortOrderTyp;
		if(document.getElementById("pageSize13")!=null){
			noOfRowsvideoLink = document.getElementById("pageSize13").value;
		}else{
			noOfRowsvideoLink=10;
		}
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getVideoLinksGrid_DivProfile(teacherIdForprofileGrid);
	}
	else if(currentPageFlag=="document"){
		if(pageno!=''){
			pageaddDoc=pageno;	
		}else{
			pageaddDoc=1;
		}
		sortOrderStraddDoc	=	sortOrder;
		sortOrderTypeaddDoc	=	sortOrderTyp;
		if(document.getElementById("pageSize15")!=null){
			noOfRowsaddDoc = document.getElementById("pageSize15").value;
		}else{
			noOfRowsaddDoc=10;
		}
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;		
		getadddocumentGrid_DivProfile(teacherIdForprofileGrid);
	}else if(currentPageFlag=="languageProfiency"){
		//alert("01");
		if(pageno!=''){
			pageLang=pageno;	
		}else{
			pageLang=1;
		}
		sortOrderStrLang	=	sortOrder;
		sortOrderTypeLang	=	sortOrderTyp;
		if(document.getElementById("pageLangProf")!=null){
			noOfRowsLang = document.getElementById("pageLangProf").value;
		}else{
			noOfRowsLang=10;
		}
		//alert(document.getElementById("teacherIdForprofileGrid").value);
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;		
		getlanguage_DivProfile(teacherIdForprofileGrid);
	}else if(currentPageFlag=="assessmentDetails"){
		if(pageno!=''){
			pageASMT=pageno;	
		}else{
			pageASMT=1;
		}
		sortOrderStrASMT	=	sortOrder;
		sortOrderTypeASMT	=	sortOrderTyp;
		if(document.getElementById("pageSize15")!=null){
			noOfRowsASMT = document.getElementById("pageSize15").value;
		}else{
			noOfRowsASMT=10;
		}
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getdistrictAssessmetGrid_DivProfile(teacherIdForprofileGrid);
	}else if(currentPageFlag=="tpvh"){
		if(pageno!=''){
			pagetpvh=pageno;	
		}else{
			pagetpvh=1;
		}
		sortOrderStrtpvh	=	sortOrder;
		sortOrderTypetpvh	=	sortOrderTyp;
		if(document.getElementById("pageSize14")!=null){
			noOfRowstpvh = document.getElementById("pageSize14").value;
		}else{
			noOfRowstpvh=10;
		}
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getTeacherProfileVisitHistoryShow(teacherIdForprofileGrid);
	}else if(currentPageFlag=="teacherAce"){
		if(pageno!=''){
			pageteacherAce=pageno;	
		}else{
			pageteacherAce=1;
		}
		sortOrderStrteacherAce	=	sortOrder;
		sortOrderTypeteacherAce	=	sortOrderTyp;
		if(document.getElementById("pageSize10")!=null){
			noOfRowsteacherAce = document.getElementById("pageSize10").value;
		}else{
			noOfRowsteacherAce=10;
		}
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getTeacherAcademicsGrid_DivProfile(teacherIdForprofileGrid);
	}else if(currentPageFlag=="teacherCerti"){
		if(pageno!=''){
			pageteacherCerti=pageno;	
		}else{
			pageteacherCerti=1;
		}
		sortOrderStrteacherCerti	=	sortOrder;
		sortOrderTypeteacherCerti	=	sortOrderTyp;
		if(document.getElementById("pageSize10")!=null){
			noOfRowsteacherCerti = document.getElementById("pageSize9").value;
		}else{
			noOfRowsteacherCerti=10;
		}
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getTeacherCertificationsGrid_DivProfile(teacherIdForprofileGrid);
		
		}
  
	  else if(currentPageFlag=="teacherInvolve"){
		  if(pageno!=''){
				dp_Involvement_page=pageno;	
			}else{
				dp_Involvement_page=1;
			}
			dp_Involvement_sortOrderStr	=	sortOrder;
			dp_Involvement_sortOrderType	=	sortOrderTyp;
			if(document.getElementById("pageSize16")!=null){
				dp_Involvement_Rows = document.getElementById("pageSize16").value;
			}else{
				dp_Involvement_Rows=10;
			}
			var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
			getInvolvementGrid(teacherIdForprofileGrid);
			//getdistrictAssessmetGrid_DivProfile(teacherIdForprofileGrid);
		}
	  else if(currentPageFlag=="teacherAssessment"){
			if(pageno!=''){
				pageEJ=pageno;	
			}else{
				pageEJ=1;
			}
			sortOrderStrEJ=sortOrder;
			sortOrderTypeEJ=sortOrderTyp;
			if(document.getElementById("pageSize")!=null){
				noOfRowsEJ = document.getElementById("pageSize").value;
			}else{
				noOfRowsEJ=10;
			}
			var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
			getTeacherAssessmentGrid_DivProfile(teacherIdForprofileGrid);
		}
}

function handleError(message, exception)
{
	/*if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert("Oops: Your Session has expired!");  document.location = 'signin.do';}
	else{alert("Server Error: "+exception.javaClassName);}*/
}

function orderingCG(oColumn, sOrder){
	orderColumn = oColumn;
	sortingOrder = sOrder;
	getCandidateGrid();
	refreshStatus();
}
function checkValue(val)
{  
	document.getElementById("contacted").value=val;
}
function checkStatusValue(val)
{  
	document.getElementById("candidateStatus").value=val;
}
function getCandidateGrid()
{
	// by alok for dynamic div width
   try
   {
	   var mydiv = document.getElementById("mydiv");
       var curr_width = parseInt(mydiv.style.width);
      
       if(deviceType)
       {
    	   mydiv.style.width = 630+"px";  
       }
       else
	   {
    	   mydiv.style.width = 647+"px";  
	   } 
   }
   catch(e){alert(e)}
   /*.........................................*/
   
	$('#loadingDiv').show();
	var jobId=document.getElementById("jobId").value;
	var firstName		=	trim(document.getElementById("firstName").value); 
	var lastName		=	trim(document.getElementById("lastName").value);
	var emailAddress	=	trim(document.getElementById("emailAddress").value);
	var certType		=	trim(document.getElementById("certificateTypeMaster").value);
	var regionId		=	trim(document.getElementById("regionId").value);
	var degreeId		=	trim(document.getElementById("degreeId").value);
	var universityId		=	trim(document.getElementById("universityId").value);
	var normScoreSelectVal		=	trim(document.getElementById("normScoreSelectVal").value);
	var CGPASelectVal		=	trim(document.getElementById("CGPASelectVal").value);
	var contacted		=	document.getElementById("contacted").value;
	var candidateStatus		=	trim(document.getElementById("candidateStatus").value);
	var stateId=document.getElementById("stateId").value;
	try{
		var iframeNorm = document.getElementById('ifrmNorm');
		var innerNorm = iframeNorm.contentDocument || iframeNorm.contentWindow.document;
		var inputNormScore = innerNorm.getElementById('normScoreFrm');
		document.getElementById("normScore").value=inputNormScore.value;
	}catch(e){}	
	try{
		var iframeCGPA = document.getElementById('ifrmCGPA');
		var innerCGPA = iframeCGPA.contentDocument || iframeCGPA.contentWindow.document;
		var inputCGPA = innerCGPA.getElementById('CGPAFrm');
		document.getElementById("CGPA").value=inputCGPA.value;
	}catch(e){}
	var normScore		=	trim(document.getElementById("normScore").value);
	var CGPA		=	trim(document.getElementById("CGPA").value);
	CandidateGridAjax.getCandidateReportGrid(jobId,firstName,lastName,emailAddress,certType,regionId,degreeId,universityId,normScoreSelectVal,
			normScore,CGPA,CGPASelectVal,contacted,candidateStatus,stateId,orderColumn, sortingOrder,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
		
			$('#loadingDiv').hide();
			//$('#myModalSearch').modal("hide");
			try{
				$('#myModalSearch').modal("hide");
			}catch(err)
			  {}
			
			document.getElementById("divReportGrid").innerHTML=data;
			tpJbIDisable();
			tpJbIEnable();
			displayTotalCGRecord(1,0);
			
			
		}});
}

function displayTotalCGRecord(sectionFlag,minusNo)
{
	if(sectionFlag==1){
		totalNoOfRecord=document.getElementById('totalNoOfRecord').value;
	}else if(sectionFlag==2){
		sectionRecordInter=document.getElementById('sectionRecordInter').value;
		if(minusNo==0){
			totalNoOfRecord=document.getElementById('totalNoOfRecordInter').value;
		}else{
			totalNoOfRecord-=sectionRecordInter;
		}
	}else if(sectionFlag==3){
		sectionRecordVlt=document.getElementById('sectionRecordVlt').value;
		if(minusNo==0){
			totalNoOfRecord=document.getElementById('totalNoOfRecordVlt').value;
		}else{
			totalNoOfRecord-=sectionRecordVlt;
		}
	}else if(sectionFlag==4){
		sectionRecordRemove=document.getElementById('sectionRecordRemove').value;
		if(minusNo==0){
			totalNoOfRecord=document.getElementById('totalNoOfRecordRemove').value;
		}else{
			totalNoOfRecord-=sectionRecordRemove;
		}
	}else if(sectionFlag==5){
		sectionRecordInComp=document.getElementById('sectionRecordInComp').value;
		if(minusNo==0){
			totalNoOfRecord=document.getElementById('totalNoOfRecordInComp').value;
		}else{
			totalNoOfRecord-=sectionRecordInComp;
		}
	}else if(sectionFlag==6){
		sectionRecordWithDr=document.getElementById('sectionRecordWithDr').value;
		if(minusNo==0){
			totalNoOfRecord=document.getElementById('totalNoOfRecordWithDr').value;
		}else{
			totalNoOfRecord-=sectionRecordWithDr;
		}
	}else if(sectionFlag==7){
		sectionRecordHired=document.getElementById('sectionRecordHired').value;
		if(minusNo==0){
			totalNoOfRecord=document.getElementById('totalNoOfRecordHired').value;
		}else{
			totalNoOfRecord-=sectionRecordHired;
		}
	}
}
function hireTeacher(assessmentStatusId, jobForTeacherId)
{	
	CandidateGridAjax.hireTeacher(assessmentStatusId, jobForTeacherId, { 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			if(data)
			{
				//$('#myModalAct').modal('hide');
				try{
					$('#myModalAct').modal('hide');
				}catch(err)
				  {}
				getCandidateGrid();				
			}
			else
			{
				$("#myModalAct").css({"z-index":"1000"});
				document.getElementById("divAlertText").innerHTML=""+resourceJSON.MsgNotHireTheCandidate+".";
				//$('#divAlert').modal('show');
				try{
					$('#divAlert').modal('show');
				}catch(err)
				  {}
				
			}			
		}});	
}
function unHireTeacher(assessmentStatusId, jobForTeacherId)
{	
	CandidateGridAjax.unHireTeacher(assessmentStatusId, jobForTeacherId,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			if(data)
			{
				//$('#myModalAct').modal('hide');
				try{
					$('#myModalAct').modal('hide');
				}catch(err)
				  {}
				getCandidateGrid();				
			}
			else
			{
				//alert("You can not unhire the Candidate.")				
				$("#myModalAct").css({"z-index":"1000"});
				document.getElementById("divAlertText").innerHTML=""+resourceJSON.MsgCannotUnHired+".";
				//$('#divAlert').modal('show');
				try{
					$('#divAlert').modal('show');
				}catch(err)
				  {}
			}		
	}});
}
function removeTeacher(assessmentStatusId, jobForTeacherId)
{
	var divMsg="You have not completed";
	var divMsg1="You have not completed....";	
	//$('#myModalAct').modal('hide');
	//$("#myModalAct").css({"z-index":"1000"});
	//$('#myConfirm').modal('show');
	
	try{
		$('#myModalAct').modal('hide');
		$("#myModalAct").css({"z-index":"1000"});
		$('#myConfirm').modal('show');
	}catch(err)
	  {}
	
	document.getElementById("spnRemoveTeacher").innerHTML="<button class='btn btn-large btn-primary' onclick=\"removeTeacherAfterConfirm('"+assessmentStatusId+"','"+jobForTeacherId+"')\" >"+resourceJSON.btnOk+" <i class='icon'></i></button>";
}

function setZIndexActDiv()
{
	//$('#myConfirm').modal('hide');
	//$("#myModalAct").css({"z-index":"3000"});
	//$('#myModalAct').modal('show');
	
	try{
		$('#myConfirm').modal('hide');
		$("#myModalAct").css({"z-index":"3000"});
		$('#myModalAct').modal('show');
	}catch(err)
	  {}
}

function setZIndexActOtherDiv()
{
	$("#myModalAct").css({"z-index":"3000"});
}

function removeTeacherAfterConfirm(assessmentStatusId, jobForTeacherId){
	
	CandidateGridAjax.removeTeacher(assessmentStatusId, jobForTeacherId, { 
	async: true,
	errorHandler:handleError,
	callback:function(data)
	{
		//$('#myConfirm').modal('hide');
		//$('#myModalAct').modal('hide');
		try{
			$('#myConfirm').modal('hide');
			$('#myModalAct').modal('hide');
		}catch(err)
		  {}
		getCandidateGrid();			
		
	}});	
}
function test(){
	
}
function downloadCandidateGridReport()
{
	$('#loadingDiv').show();
	
	var jobId=document.getElementById("jobId").value;
	var firstName		=	trim(document.getElementById("firstName").value); 
	var lastName		=	trim(document.getElementById("lastName").value);
	var emailAddress	=	trim(document.getElementById("emailAddress").value);
	var certType		=	trim(document.getElementById("certificateTypeMaster").value);
	var regionId		=	trim(document.getElementById("regionId").value);
	var degreeId		=	trim(document.getElementById("degreeId").value);
	var universityId		=	trim(document.getElementById("universityId").value);
	var normScoreSelectVal		=	trim(document.getElementById("normScoreSelectVal").value);
	var normScore		=	trim(document.getElementById("normScore").value);
	var CGPA		=	trim(document.getElementById("CGPA").value);
	var CGPASelectVal		=	trim(document.getElementById("CGPASelectVal").value);
	var contacted		=	trim(document.getElementById("contacted").value);
	var candidateStatus		=	trim(document.getElementById("candidateStatus").value);
	var stateId=document.getElementById("stateId").value;
	
	CandidateGridAjax.downloadCandidateGridReport(jobId,firstName,lastName,emailAddress,certType,regionId,degreeId,universityId,normScoreSelectVal,
	normScore,CGPA,CGPASelectVal,contacted,candidateStatus,stateId,orderColumn, sortingOrder,totalNoOfRecord,false,null,{
	async: true,
		errorHandler:handleError,
		callback:function(data)
		{	
			$('#loadingDiv').hide();
			//window.open(data);
			//openWindow(data,'Support',650,490);
			
			$('#loadingDiv').hide();
			if(deviceType)
			{
				if (data.indexOf(".doc")!=-1)
				{
					$("#docfileNotOpen").css({"z-index":"3000"});
			    	$('#docfileNotOpen').show();
				}
				else if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					$("#exelfileNotOpen").css({"z-index":"3000"});
			    	$('#exelfileNotOpen').show();
				}
				else
				{
					$('#modalDownloadCGR').modal('hide');
					document.getElementById(hrefId).href=data;				
				}
			}
			else if(deviceTypeAndroid)
			{
				if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					  try{
					    $('#modalDownloadCGR').modal('hide');
						}catch(err)
						  {}
					    document.getElementById('ifrmCGR').src = ""+data+"";
				}
				else
				{
					$('#modalDownloadCGR').modal('hide');
					document.getElementById(hrefId).href=data;					
				}				
			}
			else if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
			{
				  try{
				    	$('#modalDownloadCGR').modal('hide');
					}catch(err)
					  {}
				    document.getElementById('ifrmCGR').src = ""+data+"";
			}
			else
			{
				$('#modalDownloadCGR').modal('hide');
				document.getElementById('ifrmCGR').src = ""+data+"";
				try{
					$('#modalDownloadCGR').modal('show');
				}catch(err)
				  {}
				
			}		
			
			return false;
		}});
}
function downloadResume(teacherId,linkId)
{	
	$('#loadingDiv').show();
	CandidateGridAjax.downloadResume(teacherId,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{			
			$('#loadingDiv').hide();
			if(data=="")	
			{
				alert(resourceJSON.ResumeNotUploaded)
			}
			else
			{
				if(deviceType)
				{
					if (data.indexOf(".doc")!=-1)
					{
						$("#docfileNotOpen").css({"z-index":"3000"});
				    	$('#docfileNotOpen').show();
					}
					else if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
					{
						$("#exelfileNotOpen").css({"z-index":"3000"});
				    	$('#exelfileNotOpen').show();
					}
					else
					{
						document.getElementById(linkId).href = data; 	
					}
				}
				else if(deviceTypeAndroid)
				{
					if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
					{
						document.getElementById('ifrmTrans').src = ""+data+"";
					}
					else
					{
						document.getElementById(linkId).href = data; 		
					}
				}
				else if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					document.getElementById('ifrmTrans').src = ""+data+"";
				}
				else
				{
					document.getElementById(linkId).href = data; 		
				}			
			}	
			return false;			
							
		}
	});
}
function hideDiv()
{
	$('#docfileNotOpen').hide();
	$('#exelfileNotOpen').hide();
}

function downloadPortfolioReport(teacherId, linkId)
{	
	$('#loadingDiv').show();
	CandidateGridAjax.downloadPortfolioReport(teacherId,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#loadingDiv').hide();
			if(data=="")
			{
					document.getElementById(linkId).href = "javascript:void(0)";
					alert(resourceJSON.MsgPortfolioNotCompleted)
					return true;
			}
			else	
			{
				
				//window.location.href=data;
  				//window.focus();
				document.getElementById(linkId).href = data; 
				return false;
			}
				
		}
	});
}
function getNotesDiv(teacherId,jobId)
{
	removeNotesFile();
	hideCommunicationsDiv();
	currentPageFlag='notes';
	document.getElementById("teacherId").value=teacherId;
	document.getElementById("teacherIdForNote").value=teacherId;
	$('#divTxtNode').find(".jqte_editor").html("");		
	//$('#myModalNotes').modal('show');
	try{
		$('#myModalNotes').modal('show');
	}catch(err)
	  {}
	$('#divTxtNode').find(".jqte_editor").css("background-color", "");
	document.getElementById("noteId").value="";
	$('#errordivNotes').empty();
	CandidateGridAjax.getNotesDetail(teacherId,page,noOfRows,sortOrderStr,sortOrderType,
	{
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{	
			try{
				document.getElementById("divNotes").innerHTML=data;
			}catch(err)
			  {}
			//document.getElementById("divNotes").innerHTML=data;
			var pageFlag= document.getElementById("pageFlag").value;
			//alert(" pageFlag : "+pageFlag);
			//if(pageFlag==0)// Means it is open through [CG] 
			applyScrollOnNotesTbl();
			//else
			//{
				//if(pageFlag==1)// Means it is open through [My Folder]
					//document.getElementById("divNotes").innerHTML=data;
					//applyScrollOnNotesTblMyfolder();
				
			//}
		}
	});
}
function getMessageDiv(teacherId,emailId,jobId,msgType)
{
	removeMessagesFile();
	hideCommunicationsDiv();
	currentPageFlag='msg';
	document.getElementById("teacherDetailId").value=teacherId;	
	document.getElementById("teacherIdForMessage").value=teacherId;	
	document.getElementById("emailId").value=emailId;
	document.getElementById("jobId").value=jobId;
	document.getElementById("emailDiv").innerHTML=emailId;
	$('#messageSend').find(".jqte_editor").html("");
	document.getElementById("messageSubject").value="";
	//$('#myModalMessage').modal('show');
	try{
		$('#myModalMessage').modal('show');
	}catch(err)
	  {}
	$('#messageSubject').css("background-color", "");
	$('#messageSend').find(".jqte_editor").css("background-color", "");
	$('#messageSubject').focus();
	$('#errordivMessage').empty();
	document.getElementById("msgType").value=msgType;
	if(msgType==0){
		CandidateGridAjax.getMessages(teacherId,jobId,pageMsg,noOfRowsMsg,sortOrderStrMsg,sortOrderTypeMsg,
		{
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{	
				document.getElementById("divMessages").innerHTML=data;
				applyScrollOnMessageTbl();
			}
		});
	}else{
		document.getElementById("divMessages").innerHTML="";	
	}
	//jobId=12;
	
//	if( ($('#entityType').val()==2) || ($('#entityType').val()==3) )
//	{
		
		if((msgType==1 || msgType==0))
		{
			getTemplatesByDistrictId(jobId);
			getDocuments();
		}
		else
		{
		//	if(msgType==2)
			{
				//$("#templateDiv").hide();
			//	$("#btnChangetemplatelink").hide();
			}
			getTemplatesByDistrictId(jobId);
			getDocuments();
		}
//	}
	
	
}

function getTemplatesByDistrictId(jobId)
{
	
	CandidateGridAjax.getTemplatesByDistrictId(jobId,
			{
				async: true,
				errorHandler:handleError,
				callback:function(data)
				{	
					//alert(" data : "+data+" Length : "+data.length);
					if(data!="" || data.length>0)
					{
						$("#templateDiv").fadeIn();
						$("#btnChangetemplatelink").hide();
						$("#districttemplate").html(data);
					}
				}
			});
}



function setTemplate()
{
	var templateId	=	$("#districttemplate").val();
	if(templateId==0)
		$("#btnChangetemplatelink").fadeOut();
	else
		$("#btnChangetemplatelink").fadeIn();
}

function getTemplate()
{
	var templateId	=	$("#districttemplate").val();
	var teacherId	=	$("#teacherDetailId").val();
	var jobId		=	$("#jobId").val();
	var msgType		=	$("#msgType").val();
	var confirmFlagforChangeTemplate = $("#confirmFlagforChangeTemplate").val(); // Gagan : confirmFlagforChangeTemplate 1 means confirm change template 
	
	if(templateId!=0)
	{
		if(msgType==2){

			CandidateGridAjax.getTemplatesByMultipleTeachTemplateId(templateId,jobId,
			{
				async: true,
				errorHandler:handleError,
				callback:function(data)
				{	
					//alert(" data : "+data+" Length : "+data.length);
					if(data!=null)
					{
						if($('#messageSubject').val() && $('#messageSend').find(".jqte_editor").text().trim()!="" && confirmFlagforChangeTemplate==0)
						{
							try{
								$('#confirmChangeTemplate').modal('show');
							}	
							catch (err){}
						}
						else
						{
							$('#messageSubject').val(data.subjectLine);
							$('#messageSend').find(".jqte_editor").html(data.templateBody);
							$("#confirmFlagforChangeTemplate").val(0);
						}
					}
				}
			});
		}
		else{
			CandidateGridAjax.getTemplatesByTemplateId(templateId,jobId,teacherId,
			{
				async: true,
				errorHandler:handleError,
				callback:function(data)
				{	
					//alert(" data : "+data+" Length : "+data.length);
					if(data!=null)
					{
						if($('#messageSubject').val() && $('#messageSend').find(".jqte_editor").text().trim()!="" && confirmFlagforChangeTemplate==0)
						{
							try{
								$('#confirmChangeTemplate').modal('show');
							}	
							catch (err){}
						}
						else
						{
							$('#messageSubject').val(data.subjectLine);
							$('#messageSend').find(".jqte_editor").html(data.templateBody);
							$("#confirmFlagforChangeTemplate").val(0);
						}
					}
				}
			});
		}
	}
	return false;
}

function confirmChangeTemplate()
{
	$("#confirmFlagforChangeTemplate").val(1);
	getTemplate();
	try{
		$('#confirmChangeTemplate').modal('hide');
	}	
	catch (err){}
}
function showMessage(messageId)
{	
	CandidateGridAjax.showMessage(messageId,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
			document.getElementById("messageSubject").value=data.messageSubject;
			$('#messageSend').find(".jqte_editor").html(data.messageSend);
		//	document.getElementById("emailDiv").innerHTML=data.teacherEmailAddress;
			$('#messageSend').find(".jqte_editor").focus();
		}
	});
}

function validateMessage()
{
	
	$('#messageSubject').css("background-color", "");
	$('#messageSend').find(".jqte_editor").css("background-color", "");
	var messageSend = $('#messageSend').find(".jqte_editor").html();
	var messageSubject = document.getElementById("messageSubject").value;
	var jobId = document.getElementById("jobId").value;
	
	var msgType=document.getElementById("msgType").value;
	var teacherId;
	if(msgType!=2)
		teacherId = document.getElementById("teacherDetailId").value;
	var teacherIds = new Array();
	
	var messageDateTime = document.getElementById("messageDateTime").value;
	
	var documentname="";
	
	try{
		documentname=document.getElementById("districtDocument").value;
	}catch(e){}
	
	$('#errordivMessage').empty();
	var messageFileName="";
	var fileMessage=null;
	try{
		fileMessage=document.getElementById("fileMessage").value;
	}catch(e){}
	
	var cnt=0;
	var focs=0;
		if(trim(messageSubject)=="")
		{
			$('#errordivMessage').append("&#149; "+resourceJSON.PlzEtrSub+".<br>");
			if(focs==0)
				$('#messageSubject').focus();
			$('#messageSubject').css("background-color", "#F5E7E1");
			cnt++;focs++;
		}else if ($('#messageSend').find(".jqte_editor").text().trim()==""){

			$('#errordivMessage').append("&#149; "+resourceJSON.PlzEtrMsg+".<br>");
			if(focs==0)
				$('#messageSend').find(".jqte_editor").focus();
			$('#messageSend').find(".jqte_editor").css("background-color", "#F5E7E1");
			cnt++;focs++;
		}/*else{
			var charCount=$('#messageSend').find(".jqte_editor").text().trim();
			var count = charCount.length;
 			if(count>1000)
 			{
				$('#errordivMessage').append("&#149; Message length cannot exceed 1000 characters.<br>");
				if(focs==0)
					$('#messageSend').find(".jqte_editor").focus();
				$('#messageSend').find(".jqte_editor").css("background-color", "#F5E7E1");
				cnt++;focs++;
			}
		}*/
		
		if(fileMessage=="" && cnt==0){
			$('#errordivMessage').show();
			$('#errordivMessage').append("&#149; "+resourceJSON.PlzUplMsg+".<br>");
			cnt++;
		}else if(fileMessage!="" && fileMessage!=null)
		{
			var ext = fileMessage.substr(fileMessage.lastIndexOf('.') + 1).toLowerCase();	
			messageFileName="message"+messageDateTime+"."+ext;
			var fileSize = 0;
			if ($.browser.msie==true)
		 	{	
			    fileSize = 0;	   
			}
			else
			{
				if(document.getElementById("fileMessage").files[0]!=undefined)
				{
					fileSize = document.getElementById("fileMessage").files[0].size;
				}
			}
			
			if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt'|| ext=='xlsx' || ext=='xls'))
			{
				$('#errordivMessage').show();
				$('#errordivMessage').append("&#149; "+resourceJSON.PlzSelectAcceptMsg+".<br>");
				cnt++;
			}
			else if(fileSize>=10485760)
			{
				$('#errordivMessage').show();
				$('#errordivMessage').append("&#149; "+resourceJSON.FileSizeLessthan10Mb+".<br>");
				cnt++;
			}
		}
		if(cnt==0){	
			try{
				$('#loadingDiv').show();
				if(fileMessage!="" && fileMessage!=null){
					document.getElementById("frmMessageUpload").submit();
				}else{
					$('#lodingImage').append("<img src=\"images/loadingAnimation.gif\" /> "+resourceJSON.Send+"...");
					if(msgType==2)
					{
						teacherIds=getSendMessageTeacherIds();
						TeacherSendMessageAjax.saveMessage(teacherIds,messageSubject,messageSend,jobId,messageFileName,msgType,documentname,{ 
							async: true,
							errorHandler:handleError,
							callback:function(data)
							{	
								$('#loadingDiv').hide();
								confMessageForSendMessage();
								uncheckedAllCBX();
							}
						});
					}
					else
					{
						CandidateGridAjax.saveMessage(teacherId,messageSubject,messageSend,jobId,messageFileName,documentname,{ 
							async: true,
							errorHandler:handleError,
							callback:function(data)
							{	
								$('#loadingDiv').hide();
								var senderEmail = document.getElementById("emailDiv").innerHTML;
								confMessage(teacherId,senderEmail,jobId);
							}
						});
					}
					
					return true;
				}
			}catch(err){}
		}else{
			$('#errordivMessage').show();
			return false;
		}
}
function saveMessageFile(messageDateTime){
	$('#messageSubject').css("background-color", "");
	$('#messageSend').find(".jqte_editor").css("background-color", "");
	var messageSend = $('#messageSend').find(".jqte_editor").html();
	var messageSubject = document.getElementById("messageSubject").value;
	//var teacherId = document.getElementById("teacherDetailId").value;
	var jobId = document.getElementById("jobId").value;
	
	var msgType=document.getElementById("msgType").value;
	var teacherId;
	if(msgType!=2)
		teacherId = document.getElementById("teacherDetailId").value;
	var teacherIds = new Array();
	
	
	$('#errordivMessage').empty();
	var messageFileName="";
	var fileMessage="";
	try{
		fileMessage=document.getElementById("fileMessage").value;
	}catch(e){}
	if(fileMessage!="" && fileMessage!=null)
	{
		var ext = fileMessage.substr(fileMessage.lastIndexOf('.') + 1).toLowerCase();	
		messageFileName="message"+messageDateTime+"."+ext;
	}
	
	if(msgType==2)
	{
		teacherIds=getSendMessageTeacherIds();
		TeacherSendMessageAjax.saveMessage(teacherIds,messageSubject,messageSend,jobId,messageFileName,msgType,{ 
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{	
				$('#loadingDiv').hide();
				confMessageForSendMessage();
				uncheckedAllCBX();
			}
		});
	}
	else
	{
		CandidateGridAjax.saveMessage(teacherId,messageSubject,messageSend,jobId,messageFileName,{ 
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{	
				$('#loadingDiv').hide();
				var senderEmail = document.getElementById("emailDiv").innerHTML;
				confMessage(teacherId,senderEmail,jobId);
			}
		});
	}
	
	return true;
	
}
function confMessage(teacherId,senderEmail,jobId)
{	
	$('#lodingImage').empty();
	//$('#myModalMessage').modal('hide');
	//$('#myModalMsgShow').modal('show');
	try{
		$('#myModalMessage').modal('hide');
		$('#myModalMsgShow').modal('show');
	}catch(err)
	  {}
	document.getElementById("message2show").innerHTML=""+resourceJSON.MsgSentToCandidate+"";
}

function showMessageDiv()
{	
	var msgType=document.getElementById("msgType").value;
	//$('#myModalMsgShow').modal('hide');
	//$('#myModalMessage').modal('show');
	try{
		$('#myModalMsgShow').modal('hide');
		$('#myModalMessage').modal('show');
	}catch(err)
	  {}
	var teacherId = document.getElementById("teacherDetailId").value;
	var jobId = document.getElementById("jobId").value;
	var senderEmail = document.getElementById("emailDiv").innerHTML;
	jobForTeacherGId=document.getElementById("jobForTeacherGId").value;
	var commDivFlag=document.getElementById("commDivFlag").value;
	if(msgType==1){
		//$('#myModalMsgShow').modal('hide');
		//$('#myModalMessage').modal('hide');
		try{
			$('#myModalMsgShow').modal('hide');
			$('#myModalMessage').modal('hide');
		}catch(err)
		  {}
		getCommunicationsDiv(commDivFlag,jobForTeacherGId,teacherId,jobId);
	}else{
		document.getElementById('messageSubject').value="";
		document.getElementById('messageSend').value="";
		getMessageDiv(teacherId,senderEmail,jobId,0);
	}
}


function cancelNotes()
{
	$('#divTxtNode').find(".jqte_editor").html("");		
	//$('#myModalNotes').modal('show');
	try{
		$('#myModalNotes').modal('show');
	}catch(err)
	  {}
	$('#errordivNotes').empty();
	$('#divTxtNode').find(".jqte_editor").css("background-color", "");
	document.getElementById("noteId").value="";
	document.getElementById("spnBtnSave").style.display="inline";
}

function saveNotes()
{	
	jobForTeacherGId=document.getElementById("jobForTeacherGId").value;
	var notes = $('#divTxtNode').find(".jqte_editor").html();
	var teacherId = document.getElementById("teacherId").value;
	var jobId = document.getElementById("jobId").value;
	var noteId = document.getElementById("noteId").value;
	var noteDateTime=document.getElementById("noteDateTime").value;
	var commDivFlag=document.getElementById("commDivFlag").value;
	var fileNote=null;
	var noteFileName="";
	try{
		fileNote=document.getElementById("fileNote").value;
	}catch(e){}
	var cnt=0;
	var focs=0;	
	$('#errordivNotes').empty();
	if ($('#divTxtNode').find(".jqte_editor").text().trim()=="")
	{
		$('#errordivNotes').append("&#149; "+resourceJSON.PlzEtrNotes+".<br>");
		if(focs==0)
			$('#divTxtNode').find(".jqte_editor").focus();
		$('#divTxtNode').find(".jqte_editor").css("background-color", "#F5E7E1");
		cnt++;
	}else if(fileNote==""){
		$('#errordivNotes').show();
		$('#errordivNotes').append("&#149; "+resourceJSON.PlzUpldNotes+".<br>");
		cnt++;
	}else if(fileNote!="" && fileNote!=null)
	{
		var ext = fileNote.substr(fileNote.lastIndexOf('.') + 1).toLowerCase();	
		noteFileName="note"+noteDateTime+"."+ext;
		var fileSize = 0;
		if ($.browser.msie==true)
	 	{	
		    fileSize = 0;	   
		}
		else
		{
			if(document.getElementById("fileNote").files[0]!=undefined)
			{
				fileSize = document.getElementById("fileNote").files[0].size;
			}
		}
		
		if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt' || ext=='xlsx' || ext=='xls'))
		{
			$('#errordivNotes').show();
			$('#errordivNotes').append("&#149; "+resourceJSON.PlzSelectNoteFormat+".<br>");
			cnt++;
		}
		else if(fileSize>=10485760)
		{
			$('#errordivNotes').show();
			$('#errordivNotes').append("&#149; "+resourceJSON.FileSizeLessthan10Mb+".<br>");
			cnt++;
		}
	}
	if(cnt==0){	
		try{
			$('#loadingDiv').show();
			if(fileNote!="" && fileNote!=null){
				document.getElementById("frmNoteUpload").submit();
			}else{
				CandidateGridAjax.saveNotes(teacherId,notes,noteId,noteFileName,
				{
					async:false,
					errorHandler:handleError,
					callback:function(data)
					{
						$('#loadingDiv').hide();	
						showCommunicationsDiv();
						getCommunicationsDiv(commDivFlag,jobForTeacherGId,teacherId,jobId);				
					}
				});	
			}
		}catch(err){}
	}
}
function saveNoteFile(noteDateTime)
{	
	var notes = $('#divTxtNode').find(".jqte_editor").html();
	var teacherId = document.getElementById("teacherId").value;
	var jobId = document.getElementById("jobId").value;
	var noteId = document.getElementById("noteId").value;
	var commDivFlag=document.getElementById("commDivFlag").value;
	var fileNote=null;
	try{
		fileNote=document.getElementById("fileNote").value;
	}catch(e){}
	var noteFileName="";
	if(fileNote!="" && fileNote!=null)
	{
		var ext = fileNote.substr(fileNote.lastIndexOf('.') + 1).toLowerCase();	
		noteFileName="note"+noteDateTime+"."+ext;
	}
	CandidateGridAjax.saveNotes(teacherId,notes,noteId,noteFileName,
	{
		async:false,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#loadingDiv').hide();
			showCommunicationsDiv();
			getCommunicationsDiv(commDivFlag,jobForTeacherGId,teacherId,jobId);				
		}
	});	
}

function editNoted(notesId, viewOnly)
{	
	$('#errordivAct').empty();
	CandidateGridAjax.showEditNotes(notesId,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			document.getElementById("noteId").value=data.noteId;
			$('#divTxtNode').find(".jqte_editor").html(data.note);
			
			$('#divTxtNode').find(".jqte_editor").focus();
			if(viewOnly!=undefined)
				document.getElementById("spnBtnSave").style.display="none";
			else
				document.getElementById("spnBtnSave").style.display="inline";
		}
	});
}


function showActDiv(status,teacherAssessmentStatusId,jobForTeacherId)
{	
	$("#myModalAct").css({"z-index":"3000"});
	$('#errordivAct').empty();	
	CandidateGridAjax.showActDiv( status,teacherAssessmentStatusId,jobForTeacherId,{ 
	async: true,
	errorHandler:handleError,
	callback:function(data)
	{	
		document.getElementById("divAct").innerHTML=data.toString();
		//$('#myModalAct').modal('show');
		try{
			$('#myModalAct').modal('show');
		}catch(err)
		  {}
	}});	
}
function closeActAction(counter)
{
	$('#tag'+counter).popover('hide');
	
}
function removeStatus(teacherId)
{
	$('#removeFlag'+teacherId).val("1"); 
	$('#removeStatus'+teacherId).html("&nbsp; ");
	var k =document.getElementById("k3"+teacherId).value;
	var rdoSecStatus = document.getElementById("rdoSecStatus"+k+"_"+teacherId);
	//alert(" checked Status "+rdoSecStatus.checked+" -------- rdoSecStatus"+k+"_"+teacherId)
	rdoSecStatus.checked=false;
}

var tests="";
function UncheckSecStatus(id)
{
	
	if(document.getElementById(id).checked==false){
		tests+="##"+id+"##";
	}
	
	if(document.getElementById(id).checked==true){
		var newv = tests.replace('##'+id+'##','');
		tests = newv;
	}
}
function chkActAction(teacherId){
	
	var rdoSecStatus = document.getElementsByName("rdoSecStatus");
	var tId="";
	var secStusId="0";
	var jbFtId="";
	
	var ids="";
	var vals="";
	for(i=0;i<rdoSecStatus.length;i++)
	{
		var idCheck=false;
		try{
			if(teacherId==rdoSecStatus[i].id.split("_")[0]){
				idCheck=true;
			}
		}catch(err){}
		if(rdoSecStatus[i].checked==true && idCheck)
		{
			var rdoId="##"+rdoSecStatus[i].id+"##";
			var uncheckflag=0;
			if(tests.indexOf(rdoId)!=-1){
				uncheckflag=1;
			}
			if(ids.indexOf(rdoId)==-1){
				if(uncheckflag==0){
					ids +=rdoId;
					vals += ","+rdoSecStatus[i].value.split(",")[1];
				}
			}
		}
	}
	if (vals) vals = vals.substring(1);
	tests="";
		
	CandidateGridAjax.addOrUpdateTags(teacherId,vals,{
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			//$('#myModalAct').modal('hide');
			try{
				$('#myModalAct').modal('hide');
			}catch(err)
			  {}
			getCandidateGrid();
			refreshStatus();
		}
	});	
}
function savePhone()
{	
	var phoneType=document.getElementById("phoneType").value;
	var callDetail = $('#divTxtPhone').find(".jqte_editor").html();
	var teacherId = document.getElementById("teacherDetailId").value;
	var jobId = document.getElementById("jobId").value;
	jobForTeacherGId=document.getElementById("jobForTeacherGId").value;
	var phoneDateTime = document.getElementById("phoneDateTime").value;
	var commDivFlag=document.getElementById("commDivFlag").value;
	var phoneFileName="";
	var filePhone=null;
	try{
		filePhone=document.getElementById("filePhone").value;
	}catch(e){}
	var cnt=0;
	var focs=0;	
	$('#errordivPhone').empty();
	if ($('#divTxtPhone').find(".jqte_editor").text().trim()=="")
	{
		cnt=1;
		$('#errordivPhone').append("&#149; "+resourceJSON.PlzEtrClDetail+".<br>");
		if(focs==0){
			$('#divTxtPhone').find(".jqte_editor").focus();
			$('#divTxtPhone').find(".jqte_editor").css("background-color", "#F5E7E1");
		}
	
	}else if($('#divTxtPhone').find(".jqte_editor").text().trim()!=""){
		var charCount=$('#divTxtPhone').find(".jqte_editor").text().trim();
		var count = charCount.length;
		if(count>2500)
		{
			$('#errordivPhone').append("&#149; "+resourceJSON.CallDetailNotExceed2500+".<br>");
			cnt=1;
		}
	}
	if(filePhone=="" && cnt==0){
		$('#errordivPhone').show();
		$('#errordivPhone').append("&#149; "+resourceJSON.PlzUpldCallDetail+".<br>");
		cnt++;
	}else if(filePhone!="" && filePhone!=null)
	{
		var ext = filePhone.substr(filePhone.lastIndexOf('.') + 1).toLowerCase();	
		phoneFileName="note"+phoneDateTime+"."+ext;
		var fileSize = 0;
		if ($.browser.msie==true)
	 	{	
		    fileSize = 0;	   
		}
		else
		{
			if(document.getElementById("filePhone").files[0]!=undefined)
			{
				fileSize = document.getElementById("filePhone").files[0].size;
			}
		}
		if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt'|| ext=='xlsx' || ext=='xls'))
		{
			$('#errordivPhone').show();
			$('#errordivPhone').append("&#149; "+resourceJSON.PlzSelectPhoneFormat+".<br>");
			cnt++;
		}
		else if(fileSize>=10485760)
		{
			$('#errordivPhone').show();
			$('#errordivPhone').append("&#149; "+resourceJSON.FileSizeLessthan10Mb+".<br>");
			cnt++;
		}
	}
	if(cnt==0){	
		try{
			$('#loadingDiv').show();
			if(filePhone!="" && filePhone!=null){
				document.getElementById("frmPhoneUpload").submit();
			}else{
				CandidateGridAjax.savePhone(teacherId,callDetail,jobId,jobForTeacherGId,phoneFileName,
						{
							async:false,
							errorHandler:handleError,
							callback:function(data)
							{
								$('#loadingDiv').hide();
								if(phoneType==0){
									getPhoneDetail(teacherId,jobForTeacherGId,0);
								}else{
									showCommunicationsForPhone();
									getCommunicationsDiv(commDivFlag,jobForTeacherGId,teacherId,jobId);	
								}
							}
			});	
			}
		}catch(err){}
	}
}
function savePhoneFile(phoneDateTime)
{	
	var phoneType=document.getElementById("phoneType").value;
	var callDetail = $('#divTxtPhone').find(".jqte_editor").html();
	var teacherId = document.getElementById("teacherDetailId").value;
	var jobId = document.getElementById("jobId").value;
	jobForTeacherGId=document.getElementById("jobForTeacherGId").value;
	var commDivFlag=document.getElementById("commDivFlag").value;
	var phoneFileName="";
	var filePhone=null;
	try{
		filePhone=document.getElementById("filePhone").value;
	}catch(e){}
	
	if(filePhone!="" && filePhone!=null)
	{
		var ext = filePhone.substr(filePhone.lastIndexOf('.') + 1).toLowerCase();	
		phoneFileName="phone"+phoneDateTime+"."+ext;
	}
	CandidateGridAjax.savePhone(teacherId,callDetail,jobId,jobForTeacherGId,phoneFileName,
	{
		async:false,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#loadingDiv').hide();
			if(phoneType==0){
				getPhoneDetail(teacherId,jobForTeacherGId,0);
			}else{
				
				showCommunicationsForPhone();
				getCommunicationsDiv(commDivFlag,jobForTeacherGId,teacherId,jobId);	
			}
		}
	});	
}
function showCallDetails(){
	document.getElementById("calldetrailsdiv").style.display='inline';
	document.getElementById("calldetrailsbtn").style.display='inline';
}
function getPhoneDetail(teacherId,jobForTeacherGId,phoneType){

	document.getElementById("teacherIdForPhone").value=teacherId;
	if(phoneType==1){
		//$('#myModalCommunications').modal('hide');
		try{
			$('#myModalCommunications').modal('hide');
		}catch(err)
		  {}
		document.getElementById("calldetrailsdiv").style.display='inline';
		document.getElementById("calldetrailsbtn").style.display='inline';
		document.getElementById("divPhoneGrid").innerHTML="";
	}else{
		document.getElementById("calldetrailsdiv").style.display='none';
		document.getElementById("calldetrailsbtn").style.display='none';
	}
	document.getElementById("jobForTeacherGId").value=jobForTeacherGId;
	document.getElementById("phoneType").value=phoneType;
	
	$('#errordivPhone').empty();
	$('#divTxtPhone').find(".jqte_editor").html("");
	$('#divTxtPhone').find(".jqte_editor").css("background-color", "");
	removePhoneFile();
	document.getElementById("teacherDetailId").value=teacherId;
	
	var jobId = document.getElementById("jobId").value;
	currentPageFlag='phone';
	CandidateGridAjax.getPhoneDetail(teacherId,jobId,pagePhone,noOfRowsPhone,sortOrderStrPhone,sortOrderTypePhone,
	{
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			if(data==""){
				document.getElementById("divAlertText").innerHTML=""+resourceJSON.MsgPhNoIsNotAvailable+"";
				//$('#divAlert').modal('show');
				try{
					$('#divAlert').modal('show');
				}catch(err)
				  {}
			}
			else{
				if(phoneType==0){
					document.getElementById("divPhoneGrid").innerHTML=data;
				}
				//$('#myModalPhone').modal('show');
				try{
					$('#myModalPhone').modal('show');
				}catch(err)
				  {}
				applyScrollOnPhone();
			}
			
		}
	});
}
function divToggelClose(){
	if(document.getElementById("divToggel").style.display=='block'){
		document.getElementById("divToggel").style.display='none';
	}
}
function getRemoveCandidateGrid()
{		
	$('#removeWait').show();
	var jobId=document.getElementById("jobId").value;
	var firstName		=	trim(document.getElementById("firstName").value); 
	var lastName		=	trim(document.getElementById("lastName").value);
	var emailAddress	=	trim(document.getElementById("emailAddress").value);
	var certType		=	trim(document.getElementById("certificateTypeMaster").value);
	var regionId		=	trim(document.getElementById("regionId").value);
	var degreeId		=	trim(document.getElementById("degreeId").value);
	var universityId		=	trim(document.getElementById("universityId").value);
	var normScoreSelectVal		=	trim(document.getElementById("normScoreSelectVal").value);
	var normScore		=	trim(document.getElementById("normScore").value);
	var CGPA		=	trim(document.getElementById("CGPA").value);
	var CGPASelectVal		=	trim(document.getElementById("CGPASelectVal").value);
	var contacted		=	trim(document.getElementById("contacted").value);
	var candidateStatus		=	trim(document.getElementById("candidateStatus").value);
	var stateId=document.getElementById("stateId").value;
	CandidateGridAjax.getRemoveCandidateReportGrid(jobId,firstName,lastName,emailAddress,certType,regionId,degreeId,universityId,normScoreSelectVal,
			normScore,CGPA,CGPASelectVal,contacted,candidateStatus,stateId,orderColumn,sortingOrder,totalNoOfRecord,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			document.getElementById("divRemoveCGGrid").innerHTML=data;
			displayTotalCGRecord(4,0);
			var startPoint=totalNoOfRecord-sectionRecordRemove;
			for(var j=startPoint;j<=totalNoOfRecord;j++)
			{	
				$('#tpResume'+j).tooltip();
				$('#tpPortfolio'+j).tooltip();
				$('#tpJSA'+j).tooltip();
				$('#hireTp'+j).tooltip();
				$('#tpPDReport'+j).tooltip();
				$('#tpCL'+j).tooltip();
				$('#tpAct'+j).tooltip();
				$('#tpMsg'+j).tooltip();
				$('#tpNotes'+j).tooltip();		
				$('#tpTrans'+j).tooltip();
				$('#tpCert'+j).tooltip();
				$('#tpMessage'+j).tooltip();
				$('#tpPhone'+j).tooltip();
				$('#tpInv'+j).tooltip();
				$('#tprr'+j).tooltip();
				$('#ftsbu'+j).tooltip();
				$('#tooltipPreview'+j).tooltip()
				$('#tpref'+j).tooltip();
				$('#teacherName_'+j).tooltip();
				$('#i4Ques'+j).tooltip();
				
				try { $('#alum'+j).tooltip(); } catch (e) {}
				try{ $('#tpInternalCandidate'+j).tooltip(); }catch(e){}
				
				var candidateFlag=document.getElementById("candidateflag").value;
				if(candidateFlag==1)
				{
					var noOfTagsFile=document.getElementById("noOfTagsFile").value;
					if(noOfTagsFile>15){
						iWidth=600;
					}
					else
					{
						iWidth=300;
					}
				}
				else
				{
					iWidth=300;
				}
				$('#tag'+j).popover({ 
				    html : true,
				    placement: 'right',
				    content: $('#tagDiv'+j).html(),
				  }).on("mouseenter", function () {
					  var height=setDialogPosition();
					  height=height+80;
					  	var style = $('<style>.popover { position: absolute;top: '+height+'px !important;left: 0px;width: '+iWidth+'px; }</style>')
						$('html > head').append(style);
					  	
				        var _this = this;
				        $(this).popover("show");
				        $(this).siblings(".popover").on("mouseleave", function () {
				            $(_this).popover('hide');
				        });
				    }).on("mouseleave", function () {
				        var _this = this;
				        setTimeout(function () {
				            if (!$(".popover:hover").length) {
				                $(_this).popover("hide")
				            }
				        }, 100);
				    });
				
				$('.profile'+j).popover({ 
				 	trigger: "manual",
				    html : true,
				    placement: 'right',
				    content: $('#profileDetails'+j).html(),
				  }).on("mouseenter", function () {
				        var _this = this;
				        $(this).popover("show");
				        $(this).siblings(".popover").on("mouseleave", function () {
				            $(_this).popover('hide');
				        });
				    }).on("mouseleave", function () {
				        var _this = this;
				        setTimeout(function () {
				            if (!$(".popover:hover").length) {
				                $(_this).popover("hide")
				            }
				        }, 100);
				    });
			}
			
			getRejectScoreDetails();
			
		}});
}
function hideRemoveCandidateGrid()
{		
	document.getElementById("divRemoveCGGrid").innerHTML="<div id='removeWait' style='display:none;text-align:center;padding-top:0px;'><img src='images/loadingAnimation.gif' /> <br>"+resourceJSON.MsgLoad+"</div>";
}

function getInCompleteCandidateGrid()
{		
	$('#InCompWait').show();
	var jobId=document.getElementById("jobId").value;
	var firstName		=	trim(document.getElementById("firstName").value); 
	var lastName		=	trim(document.getElementById("lastName").value);
	var emailAddress	=	trim(document.getElementById("emailAddress").value);
	var certType		=	trim(document.getElementById("certificateTypeMaster").value);
	var regionId		=	trim(document.getElementById("regionId").value);
	var degreeId		=	trim(document.getElementById("degreeId").value);
	var universityId		=	trim(document.getElementById("universityId").value);
	var normScoreSelectVal		=	trim(document.getElementById("normScoreSelectVal").value);
	var normScore		=	trim(document.getElementById("normScore").value);
	var CGPA		=	trim(document.getElementById("CGPA").value);
	var CGPASelectVal		=	trim(document.getElementById("CGPASelectVal").value);
	var contacted		=	trim(document.getElementById("contacted").value);
	var candidateStatus		=	trim(document.getElementById("candidateStatus").value);
	var stateId=document.getElementById("stateId").value;
	CandidateGridAjax.getInCompleteCandidateReportGrid(jobId,firstName,lastName,emailAddress,certType,regionId,degreeId,universityId,normScoreSelectVal,
			normScore,CGPA,CGPASelectVal,contacted,candidateStatus,stateId,orderColumn, sortingOrder,totalNoOfRecord,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			
			document.getElementById("divInCompCGGrid").innerHTML=data;
			displayTotalCGRecord(5,0);
			var startPoint=totalNoOfRecord-sectionRecordInComp;
			for(var j=startPoint;j<=totalNoOfRecord;j++)
			{	
				$('#tpResume'+j).tooltip();
				$('#tpPortfolio'+j).tooltip();
				$('#tpJSA'+j).tooltip();
				$('#hireTp'+j).tooltip();
				$('#tpPDReport'+j).tooltip();
				$('#tpCL'+j).tooltip();
				$('#tpAct'+j).tooltip();
				$('#tpMsg'+j).tooltip();
				$('#tpNotes'+j).tooltip();		
				$('#tpTrans'+j).tooltip();
				$('#tpCert'+j).tooltip();
				$('#tpMessage'+j).tooltip();
				$('#tpPhone'+j).tooltip();
				$('#tpInv'+j).tooltip();
				$('#tprr'+j).tooltip();	
				$('#ftsbu'+j).tooltip();
				$('#tooltipPreview'+j).tooltip()
				$('#tpref'+j).tooltip();
				$('#teacherName_'+j).tooltip();
				$('#i4Ques'+j).tooltip();
				
				try { $('#alum'+j).tooltip(); } catch (e) {}
				try{ $('#tpInternalCandidate'+j).tooltip(); }catch(e){}
				
				var candidateFlag=document.getElementById("candidateflag").value;
				var iWidth=0;
				if(candidateFlag==1)
				{
					var noOfTagsFile=document.getElementById("noOfTagsFile").value;
					if(noOfTagsFile>15){
						iWidth=600;
					}
					else
					{
						iWidth=300;
					}
				}
				else
				{
					iWidth=300;
				}
				$('#tag'+j).popover({ 
				    html : true,
				    placement: 'right',
				    content: $('#tagDiv'+j).html(),
				  }).on("mouseenter", function () {
					  var height=setDialogPosition();
					  height=height+80;
					  	var style = $('<style>.popover { position: absolute;top: '+height+'px !important;left: 0px;width: '+iWidth+'px; }</style>')
						$('html > head').append(style);
					  
				        var _this = this;
				        $(this).popover("show");
				        $(this).siblings(".popover").on("mouseleave", function () {
				            $(_this).popover('hide');
				        });
				    }).on("mouseleave", function () {
				    	
				        var _this = this;
				        setTimeout(function () {
				            if (!$(".popover:hover").length) {
				                $(_this).popover("hide")
				            }
				        }, 100);
				    });
				
				$('.profile'+j).popover({ 
				 	trigger: "manual",
				    html : true,
				    placement: 'right',
				    content: $('#profileDetails'+j).html(),
				  }).on("mouseenter", function () {
				        var _this = this;
				        $(this).popover("show");
				        $(this).siblings(".popover").on("mouseleave", function () {
				            $(_this).popover('hide');
				        });
				    }).on("mouseleave", function () {
				        var _this = this;
				        setTimeout(function () {
				            if (!$(".popover:hover").length) {
				                $(_this).popover("hide")
				            }
				        }, 100);
				    });
			}
			
			getIncompleteScoreDetails();
			
		}});
}
function setDialogPosition()
{
	var scrollTopPosition = $(window).scrollTop();
	var x=event.clientX; 
	var y=event.clientY;
	var z=y-180+scrollTopPosition;
	//z=z+80;
	return z;
}


function hideInCompleteCandidateGrid()
{		
	document.getElementById("divInCompCGGrid").innerHTML="<div id='InCompWait' style='display:none;text-align:center;padding-top:0px;'><img src='images/loadingAnimation.gif' /> <br>"+resourceJSON.MsgLoad+"</div>";
}

function getWithdrawnCandidateGrid()
{		
	$('#WithDrWait').show();
	var jobId=document.getElementById("jobId").value;
	var firstName		=	trim(document.getElementById("firstName").value); 
	var lastName		=	trim(document.getElementById("lastName").value);
	var emailAddress	=	trim(document.getElementById("emailAddress").value);
	var certType		=	trim(document.getElementById("certificateTypeMaster").value);
	var regionId		=	trim(document.getElementById("regionId").value);
	var degreeId		=	trim(document.getElementById("degreeId").value);
	var universityId		=	trim(document.getElementById("universityId").value);
	var normScoreSelectVal		=	trim(document.getElementById("normScoreSelectVal").value);
	var normScore		=	trim(document.getElementById("normScore").value);
	var CGPA		=	trim(document.getElementById("CGPA").value);
	var CGPASelectVal		=	trim(document.getElementById("CGPASelectVal").value);
	var contacted		=	trim(document.getElementById("contacted").value);
	var candidateStatus		=	trim(document.getElementById("candidateStatus").value);
	var stateId=document.getElementById("stateId").value;
	CandidateGridAjax.getWithdrawnCandidateReportGrid(jobId,firstName,lastName,emailAddress,certType,regionId,degreeId,universityId,normScoreSelectVal,
			normScore,CGPA,CGPASelectVal,contacted,candidateStatus,stateId,orderColumn, sortingOrder,totalNoOfRecord,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			document.getElementById("divWithDrCGGrid").innerHTML=data;
			displayTotalCGRecord(6,0);
			var startPoint=totalNoOfRecord-sectionRecordWithDr;
			for(var j=startPoint;j<=totalNoOfRecord;j++)
			{	
				$('#tpResume'+j).tooltip();
				$('#tpPortfolio'+j).tooltip();
				$('#tpJSA'+j).tooltip();
				$('#hireTp'+j).tooltip();
				$('#tpPDReport'+j).tooltip();
				$('#tpCL'+j).tooltip();
				$('#tpAct'+j).tooltip();
				$('#tpMsg'+j).tooltip();
				$('#tpNotes'+j).tooltip();		
				$('#tpTrans'+j).tooltip();
				$('#tpCert'+j).tooltip();
				$('#tpMessage'+j).tooltip();
				$('#tpPhone'+j).tooltip();
				$('#tpInv'+j).tooltip();
				$('#tprr'+j).tooltip();	
				$('#ftsbu'+j).tooltip();
				$('#tooltipPreview'+j).tooltip()
				$('#tpref'+j).tooltip();
				$('#teacherName_'+j).tooltip();
				$('#i4Ques'+j).tooltip();
				try { $('#alum'+j).tooltip(); } catch (e) {}
				try{ $('#tpInternalCandidate'+j).tooltip(); }catch(e){}
				var candidateFlag=document.getElementById("candidateflag").value;
				if(candidateFlag==1)
				{
					var noOfTagsFile=document.getElementById("noOfTagsFile").value;
					if(noOfTagsFile>15){
						iWidth=600;
					}
					else
					{
						iWidth=300;
					}
				}
				else
				{
					iWidth=300;
				}
				$('#tag'+j).popover({ 
				    html : true,
				    placement: 'right',
				    content: $('#tagDiv'+j).html(),
				  }).on("mouseenter", function () {
					  var height=setDialogPosition();
					  height=height+80;
					  	var style = $('<style>.popover { position: absolute;top: '+height+'px !important;left: 0px;width: '+iWidth+'px; }</style>')
						$('html > head').append(style);
					  	
				        var _this = this;
				        $(this).popover("show");
				        $(this).siblings(".popover").on("mouseleave", function () {
				            $(_this).popover('hide');
				        });
				    }).on("mouseleave", function () {
				        var _this = this;
				        setTimeout(function () {
				            if (!$(".popover:hover").length) {
				                $(_this).popover("hide")
				            }
				        }, 100);
				    });
				
				$('.profile'+j).popover({ 
				 	trigger: "manual",
				    html : true,
				    placement: 'right',
				    content: $('#profileDetails'+j).html(),
				  }).on("mouseenter", function () {
				        var _this = this;
				        $(this).popover("show");
				        $(this).siblings(".popover").on("mouseleave", function () {
				            $(_this).popover('hide');
				        });
				    }).on("mouseleave", function () {
				        var _this = this;
				        setTimeout(function () {
				            if (!$(".popover:hover").length) {
				                $(_this).popover("hide")
				            }
				        }, 100);
				    });
			}
			
			getWithdrawnScoreDetails();
			
		}});
}
function hideWithdrawnCandidateGrid()
{		
	document.getElementById("divWithDrCGGrid").innerHTML="<div id='WithDrWait' style='display:none;text-align:center;padding-top:0px;'><img src='images/loadingAnimation.gif' /> <br>"+resourceJSON.MsgLoad+"</div>";
}
function getInternalCandidateGrid()
{	
	var delayMin=100;
	$('#InterWait').show();
	var jobId=document.getElementById("jobId").value;
	var firstName		=	trim(document.getElementById("firstName").value); 
	var lastName		=	trim(document.getElementById("lastName").value);
	var emailAddress	=	trim(document.getElementById("emailAddress").value);
	var certType		=	trim(document.getElementById("certificateTypeMaster").value);
	var regionId		=	trim(document.getElementById("regionId").value);
	var degreeId		=	trim(document.getElementById("degreeId").value);
	var universityId		=	trim(document.getElementById("universityId").value);
	var normScoreSelectVal		=	trim(document.getElementById("normScoreSelectVal").value);
	var normScore		=	trim(document.getElementById("normScore").value);
	var CGPA		=	trim(document.getElementById("CGPA").value);
	var CGPASelectVal		=	trim(document.getElementById("CGPASelectVal").value);
	var contacted		=	trim(document.getElementById("contacted").value);
	var candidateStatus		=	trim(document.getElementById("candidateStatus").value);
	var stateId=document.getElementById("stateId").value;
	CandidateGridAjax.getInternalCandidateReportGrid(jobId,firstName,lastName,emailAddress,certType,regionId,degreeId,universityId,normScoreSelectVal,
			normScore,CGPA,CGPASelectVal,contacted,candidateStatus,stateId,orderColumn, sortingOrder,totalNoOfRecord,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			document.getElementById("divInternalCGGrid").innerHTML=data;
		
			displayTotalCGRecord(2,0)
			var startPoint=totalNoOfRecord-sectionRecordInter;
			for(var j=startPoint;j<=totalNoOfRecord;j++)
			{
				
				
				$('#tpResume'+j).tooltip();
				$('#tpPortfolio'+j).tooltip();
				$('#tpJSA'+j).tooltip();
				$('#hireTp'+j).tooltip();
				$('#tpPDReport'+j).tooltip();
				$('#tpCL'+j).tooltip();
				$('#tpAct'+j).tooltip();
				$('#tpMsg'+j).tooltip();
				$('#tpNotes'+j).tooltip();		
				$('#tpTrans'+j).tooltip();
				$('#tpCert'+j).tooltip();
				$('#tpMessage'+j).tooltip();
				$('#tpPhone'+j).tooltip();
				$('#tpInv'+j).tooltip();
				$('#tprr'+j).tooltip();	
				$('#ftsbu'+j).tooltip();
				$('#tooltipPreview'+j).tooltip()
				$('#tpref'+j).tooltip();
				$('#teacherName_'+j).tooltip();
				$('#i4Ques'+j).tooltip();
				try { $('#alum'+j).tooltip(); } catch (e) {}
				try{ $('#tpInternalCandidate'+j).tooltip(); }catch(e){}
				var candidateFlag=document.getElementById("candidateflag").value;
				if(candidateFlag==1)
				{
					var noOfTagsFile=document.getElementById("noOfTagsFile").value;
					if(noOfTagsFile>15){
						iWidth=600;
					}
					else
					{
						iWidth=300;
					}
				}
				else
				{
					iWidth=300;
				}
				$('#tag'+j).popover({ 
				    html : true,
				    placement: 'right',
				    content: $('#tagDiv'+j).html(),
				  }).on("mouseenter", function () {
					  var height=setDialogPosition();
					  height=height+80;	
					  var style = $('<style>.popover { position: absolute;top: '+height+'px !important;left: 0px;width: '+iWidth+'px; }</style>')
						$('html > head').append(style);
					  	
				        var _this = this;
				        $(this).popover("show");
				        $(this).siblings(".popover").on("mouseleave", function () {
				            $(_this).popover('hide');
				        });
				    }).on("mouseleave", function () {
				        var _this = this;
				        setTimeout(function () {
				            if (!$(".popover:hover").length) {
				                $(_this).popover("hide")
				            }
				        }, 100);
				    });
				
				$('.profile'+j).popover({ 
				 	trigger: "manual",
				    html : true,
				    placement: 'right',
				    content: $('#profileDetails'+j).html(),
				  }).on("mouseenter", function () {
				        var _this = this;
				        $(this).popover("show");
				        $(this).siblings(".popover").on("mouseleave", function () {
				            $(_this).popover('hide');
				        });
				    }).on("mouseleave", function () {
				        var _this = this;
				        setTimeout(function () {
				            if (!$(".popover:hover").length) {
				                $(_this).popover("hide")
				            }
				        }, 100);
				    });
			}
			
			getInternalScoreDetails(); // Ramesh
		}});
}
function hideInternalCandidateGrid()
{		
	document.getElementById("divInternalCGGrid").innerHTML="<div id='InterWait' style='display:none;text-align:center;padding-top:0px;'><img src='images/loadingAnimation.gif' /> <br>"+resourceJSON.MsgLoad+"</div>";
}

function getHiredCandidateGrid()
{		
	$('#hiredWait').show();
	var jobId=document.getElementById("jobId").value;
	var firstName		=	trim(document.getElementById("firstName").value); 
	var lastName		=	trim(document.getElementById("lastName").value);
	var emailAddress	=	trim(document.getElementById("emailAddress").value);
	var certType		=	trim(document.getElementById("certificateTypeMaster").value);
	var regionId		=	trim(document.getElementById("regionId").value);
	var degreeId		=	trim(document.getElementById("degreeId").value);
	var universityId		=	trim(document.getElementById("universityId").value);
	var normScoreSelectVal		=	trim(document.getElementById("normScoreSelectVal").value);
	var normScore		=	trim(document.getElementById("normScore").value);
	var CGPA		=	trim(document.getElementById("CGPA").value);
	var CGPASelectVal		=	trim(document.getElementById("CGPASelectVal").value);
	var contacted		=	trim(document.getElementById("contacted").value);
	var candidateStatus		=	trim(document.getElementById("candidateStatus").value);
	var stateId=document.getElementById("stateId").value;
	CandidateGridAjax.getHiredCandidateReportGrid(jobId,firstName,lastName,emailAddress,certType,regionId,degreeId,universityId,normScoreSelectVal,
			normScore,CGPA,CGPASelectVal,contacted,candidateStatus,stateId,orderColumn, sortingOrder,totalNoOfRecord,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			document.getElementById("divHiredCGGrid").innerHTML=data;
			displayTotalCGRecord(7,0)
			var startPoint=totalNoOfRecord-sectionRecordHired;
			for(var j=startPoint;j<=totalNoOfRecord;j++)
			{	
				$('#tpResume'+j).tooltip();
				$('#tpPortfolio'+j).tooltip();
				$('#tpJSA'+j).tooltip();
				$('#hireTp'+j).tooltip();
				$('#tpPDReport'+j).tooltip();
				$('#tpCL'+j).tooltip();
				$('#tpAct'+j).tooltip();
				$('#tpMsg'+j).tooltip();
				$('#tpNotes'+j).tooltip();		
				$('#tpTrans'+j).tooltip();
				$('#tpCert'+j).tooltip();
				$('#tpMessage'+j).tooltip();
				$('#tpPhone'+j).tooltip();
				$('#tpInv'+j).tooltip();
				$('#tprr'+j).tooltip();	
				$('#ftsbu'+j).tooltip();
				$('#tooltipPreview'+j).tooltip()
				$('#tpref'+j).tooltip();
				$('#teacherName_'+j).tooltip();
				$('#i4Ques'+j).tooltip();
				try { $('#alum'+j).tooltip(); } catch (e) {}
				try{ $('#tpInternalCandidate'+j).tooltip(); }catch(e){}
				var candidateFlag=document.getElementById("candidateflag").value;
				if(candidateFlag==1)
				{
					var noOfTagsFile=document.getElementById("noOfTagsFile").value;
					if(noOfTagsFile>15){
						iWidth=600;
					}
					else
					{
						iWidth=300;
					}
				}
				else
				{
					iWidth=300;
				}
				$('#tag'+j).popover({ 
				    html : true,
				    placement: 'right',
				    content: $('#tagDiv'+j).html(),
				  }).on("mouseenter", function () {
					  var height=setDialogPosition();
					  height=height+80;	
					  	var style = $('<style>.popover { position: absolute;top: '+height+'px !important;left: 0px;width: '+iWidth+'px; }</style>')
						$('html > head').append(style);
					  	
				        var _this = this;
				        $(this).popover("show");
				        $(this).siblings(".popover").on("mouseleave", function () {
				            $(_this).popover('hide');
				        });
				    }).on("mouseleave", function () {
				        var _this = this;
				        setTimeout(function () {
				            if (!$(".popover:hover").length) {
				                $(_this).popover("hide")
				            }
				        }, 100);
				    });
				
				$('.profile'+j).popover({ 
				 	trigger: "manual",
				    html : true,
				    placement: 'right',
				    content: $('#profileDetails'+j).html(),
				  }).on("mouseenter", function () {
				        var _this = this;
				        $(this).popover("show");
				        $(this).siblings(".popover").on("mouseleave", function () {
				            $(_this).popover('hide');
				        });
				    }).on("mouseleave", function () {
				        var _this = this;
				        setTimeout(function () {
				            if (!$(".popover:hover").length) {
				                $(_this).popover("hide")
				            }
				        }, 100);
				    });
			}
			
			getHiredScoreDetails();
		}});
}
function hideHiredCandidateGrid()
{		
	document.getElementById("divHiredCGGrid").innerHTML="<div id='hiredWait' style='display:none;text-align:center;padding-top:0px;'><img src='images/loadingAnimation.gif' /> <br>"+resourceJSON.MsgLoad+"</div>";
}
function getVltCandidateGrid()
{		
	$('#vltWait').show();
	var jobId=document.getElementById("jobId").value;
	var firstName		=	trim(document.getElementById("firstName").value); 
	var lastName		=	trim(document.getElementById("lastName").value);
	var emailAddress	=	trim(document.getElementById("emailAddress").value);
	var certType		=	trim(document.getElementById("certificateTypeMaster").value);
	var regionId		=	trim(document.getElementById("regionId").value);
	var degreeId		=	trim(document.getElementById("degreeId").value);
	var universityId		=	trim(document.getElementById("universityId").value);
	var normScoreSelectVal		=	trim(document.getElementById("normScoreSelectVal").value);
	var normScore		=	trim(document.getElementById("normScore").value);
	var CGPA		=	trim(document.getElementById("CGPA").value);
	var CGPASelectVal		=	trim(document.getElementById("CGPASelectVal").value);
	var contacted		=	trim(document.getElementById("contacted").value);
	var candidateStatus		=	trim(document.getElementById("candidateStatus").value);
	var stateId=document.getElementById("stateId").value;
	CandidateGridAjax.getVltCandidateReportGrid(jobId,firstName,lastName,emailAddress,certType,regionId,degreeId,universityId,normScoreSelectVal,
			normScore,CGPA,CGPASelectVal,contacted,candidateStatus,stateId,orderColumn, sortingOrder,totalNoOfRecord,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			document.getElementById("divVltCGGrid").innerHTML=data;
			displayTotalCGRecord(3,0)
			var startPoint=totalNoOfRecord-sectionRecordVlt;
			for(var j=startPoint;j<=totalNoOfRecord;j++)
			{	
				$('#tpResume'+j).tooltip();
				$('#tpPortfolio'+j).tooltip();
				$('#tpJSA'+j).tooltip();
				$('#hireTp'+j).tooltip();
				$('#tpPDReport'+j).tooltip();
				$('#tpCL'+j).tooltip();
				$('#tpAct'+j).tooltip();
				$('#tpMsg'+j).tooltip();
				$('#tpNotes'+j).tooltip();		
				$('#tpTrans'+j).tooltip();
				$('#tpCert'+j).tooltip();
				$('#tpMessage'+j).tooltip();
				$('#tpPhone'+j).tooltip();
				$('#tpInv'+j).tooltip();
				$('#tprr'+j).tooltip();	
				$('#ftsbu'+j).tooltip();
				$('#tooltipPreview'+j).tooltip()
				$('#tpref'+j).tooltip();
				$('#teacherName_'+j).tooltip();
				$('#i4Ques'+j).tooltip();
				try { $('#alum'+j).tooltip(); } catch (e) {}
				try{ $('#tpInternalCandidate'+j).tooltip(); }catch(e){}
				var candidateFlag=document.getElementById("candidateflag").value;
				if(candidateFlag==1)
				{
					var noOfTagsFile=document.getElementById("noOfTagsFile").value;
					if(noOfTagsFile>15){
						iWidth=600;
					}
					else
					{
						iWidth=300;
					}
				}
				else
				{
					iWidth=300;
				}
				$('#tag'+j).popover({ 
				    html : true,
				    placement: 'right',
				    content: $('#tagDiv'+j).html(),
				  }).on("mouseenter", function () {
					  var height=setDialogPosition();
					  height=height+80;	
					  	var style = $('<style>.popover { position: absolute;top: '+height+'px !important;left: 0px;width: '+iWidth+'px; }</style>')
						$('html > head').append(style);
					  	
					  	
				        var _this = this;
				        $(this).popover("show");
				        $(this).siblings(".popover").on("mouseleave", function () {
				            $(_this).popover('hide');
				        });
				    }).on("mouseleave", function () {
				        var _this = this;
				        setTimeout(function () {
				            if (!$(".popover:hover").length) {
				                $(_this).popover("hide")
				            }
				        }, 100);
				    });
				
				$('.profile'+j).popover({ 
				 	trigger: "manual",
				    html : true,
				    placement: 'right',
				    content: $('#profileDetails'+j).html(),
				  }).on("mouseenter", function () {
				        var _this = this;
				        $(this).popover("show");
				        $(this).siblings(".popover").on("mouseleave", function () {
				            $(_this).popover('hide');
				        });
				    }).on("mouseleave", function () {
				        var _this = this;
				        setTimeout(function () {
				            if (!$(".popover:hover").length) {
				                $(_this).popover("hide")
				            }
				        }, 100);
				    });
				
			}
			
			getTimeoutScoreDetails();
		}});
}
function hideVltCandidateGrid()
{		
	document.getElementById("divVltCGGrid").innerHTML="<div id='vltWait' style='display:none;text-align:center;padding-top:0px;'><img src='images/loadingAnimation.gif' /> <br>"+resourceJSON.MsgLoad+"</div>";
}
function tpJbIEnable()
{
	expanddiv=0;
	var noOrRow = document.getElementById("totalNoOfRecord").value;
	$('#hrefPDF').tooltip();
	$('#tpSearch').tooltip();
	$('#tpLegend').tooltip();
	$('#datailsGrid').tooltip();
	$('#scoreGrid').tooltip();
	$('#normCurve').tooltip();
	$('#tpexport').tooltip();
	$('#jobHired').tooltip();
	
	for(var i=1;i<=6;i++){
		$('#jobApplied'+i).tooltip();
	}
	$(document).ready(function(){
		 
		$("#tpLegend").click(function(){
	    	$("#divToggel").slideToggle("slow");			
	  	});
		var delayVal=1000;
		var delayMin=100;
		
		$("#scoreGrid").click(function(){ 
				if(document.getElementById("expandchk").value==0){
					expanddiv++;
					if(expanddiv>=2){
						document.getElementById("expandchk").value=1;
						document.getElementById("scoredf").colSpan="2";
						document.getElementById("detailsdf").colSpan="1";
						document.getElementById("tdscore0tbl").style.textIndent="0px";
						//document.getElementById("compositeScoreGrid").style.textIndent="0px";
						document.getElementById("scoreright").style.display='inline';
						document.getElementById("scoreleft").style.display='none';
						$("#scoreGrid").attr('data-original-title', resourceJSON.MsgCollapseTheColumn).tooltip('show');
						
							 $( ".tdscore0:first" ).animate({
							    left :0,
								 width:256
							  }, {
							    duration: 1000
							  });
						for(var j=1;j<=noOrRow;j++)
						{
							$( "#tdscore"+j ).delay(delayVal).fadeIn(delayVal);
						}
						getInternalScoreDetails();
						getIncompleteScoreDetails();
						getRejectScoreDetails(); 
						getWithdrawnScoreDetails();
						getTimeoutScoreDetails();
						getHiredScoreDetails();
						
						$( "#tdscoremean").delay(delayVal).fadeIn(delayVal);
					}else{
						document.getElementById("expandchk").value=1;
						document.getElementById("scoredf").colSpan="2";
						document.getElementById("tdscore0tbl").style.textIndent="0px";
						//document.getElementById("compositeScoreGrid").style.textIndent="0px";
						document.getElementById("scoreright").style.display='inline';
						document.getElementById("scoreleft").style.display='none';
						$("#scoreGrid").attr('data-original-title', resourceJSON.MsgCollapseTheColumn).tooltip('show');
							 $( ".tdscore0:first" ).animate({
							    left :0,
								 width:256
							  }, {
							    duration: 1000
							  });
						for(var j=1;j<=noOrRow;j++)
						{
							$( "#tdscore"+j ).delay(delayVal).fadeIn(delayVal);
							
							
						}
						
						getInternalScoreDetails();
						getIncompleteScoreDetails();
						getRejectScoreDetails(); 
						getWithdrawnScoreDetails();
						getTimeoutScoreDetails();
						getHiredScoreDetails();
						$( "#tdscoremean").delay(delayVal).fadeIn(delayVal);
					}
				}else{
					expanddiv--;
					document.getElementById("expandchk").value=0;
					document.getElementById("scoredf").colSpan="2";
					document.getElementById("tdscore0tbl").style.textIndent="99999px";
					//document.getElementById("compositeScoreGrid").style.textIndent="99999px";
					document.getElementById("scoreright").style.display='none';
					document.getElementById("scoreleft").style.display='inline';
					$("#scoreGrid").attr('data-original-title', resourceJSON.MsgCollapseTheColumnSeeScore).tooltip('show');
					
					 $( ".tdscore0:first" ).animate({
						    left :0,
							 width:1
						  }, {
						    duration: 1000
						  });
					for(var j=1;j<=noOrRow;j++)
					{
						$( "#tdscore"+j ).delay(delayMin).fadeOut(delayMin);
						
					}
					getInternalScoreDetails(); 
					getIncompleteScoreDetails();
					getRejectScoreDetails();
					getWithdrawnScoreDetails();
					getTimeoutScoreDetails();
					getHiredScoreDetails();
					$( "#tdscoremean").delay(delayMin).fadeOut(delayMin);
				}
				
	  	});
		
		$("#removeGrid").click(function(){
			if(document.getElementById("expandremovechk").value==0){
				getRemoveCandidateGrid();
				document.getElementById("expandremovechk").value=1;
				document.getElementById("removeasc").style.display='none';
				document.getElementById("removedsc").style.display='inline';
			}else{
				displayTotalCGRecord(4,1);
				document.getElementById("expandremovechk").value=0;
				document.getElementById("removeasc").style.display='inline';
				document.getElementById("removedsc").style.display='none';
				hideRemoveCandidateGrid();
			}
			
		});
		
		$("#incompGrid").click(function(){
			if(document.getElementById("expandincompchk").value==0){
				getInCompleteCandidateGrid();
				document.getElementById("expandincompchk").value=1;
				document.getElementById("incompasc").style.display='none';
				document.getElementById("incompdsc").style.display='inline';
			}else{
				displayTotalCGRecord(5,1);
				document.getElementById("expandincompchk").value=0;
				document.getElementById("incompasc").style.display='inline';
				document.getElementById("incompdsc").style.display='none';
				hideInCompleteCandidateGrid();
			}
			
		});
		$("#interGrid").click(function(){
			if(document.getElementById("expandinterchk").value==0){
				getInternalCandidateGrid();
				document.getElementById("expandinterchk").value=1;
				document.getElementById("interasc").style.display='none';
				document.getElementById("interdsc").style.display='inline';
			}else{
				displayTotalCGRecord(2,1);
				document.getElementById("expandinterchk").value=0;
				document.getElementById("interasc").style.display='inline';
				document.getElementById("interdsc").style.display='none';
				hideInternalCandidateGrid();
			}
			
			
			
		});
		
		$("#hiredGrid").click(function(){
			if(document.getElementById("expandhiredchk").value==0){
				getHiredCandidateGrid();
				document.getElementById("expandhiredchk").value=1;
				document.getElementById("hiredasc").style.display='none';
				document.getElementById("hireddsc").style.display='inline';
			}else{
				displayTotalCGRecord(7,1);
				document.getElementById("expandhiredchk").value=0;
				document.getElementById("hiredasc").style.display='inline';
				document.getElementById("hireddsc").style.display='none';
				hideHiredCandidateGrid();
			}
			
		});
		$("#vltGrid").click(function(){
			if(document.getElementById("expandvltchk").value==0){
				getVltCandidateGrid();
				document.getElementById("expandvltchk").value=1;
				document.getElementById("vltasc").style.display='none';
				document.getElementById("vltdsc").style.display='inline';
			}else{
				displayTotalCGRecord(3,1);
				document.getElementById("expandvltchk").value=0;
				document.getElementById("vltasc").style.display='inline';
				document.getElementById("vltdsc").style.display='none';
				hideVltCandidateGrid();
			}
			
		});
		$("#withdrGrid").click(function(){
			if(document.getElementById("expandwithdrchk").value==0){
				getWithdrawnCandidateGrid();
				document.getElementById("expandwithdrchk").value=1;
				document.getElementById("withdrasc").style.display='none';
				document.getElementById("withdrdsc").style.display='inline';
			}else{
				displayTotalCGRecord(6,1);
				document.getElementById("expandwithdrchk").value=0;
				document.getElementById("withdrasc").style.display='inline';
				document.getElementById("withdrdsc").style.display='none';
				hideWithdrawnCandidateGrid();
			}
		});
		
	});
	
	
	for(var j=1;j<=noOrRow;j++)
	{	
		$('#tpResume'+j).tooltip();
		$('#tpPortfolio'+j).tooltip();
		$('#hireTp'+j).tooltip();
		$('#hireTp'+j).tooltip();
		$('#tpJSA'+j).tooltip();
		$('#tpPDReport'+j).tooltip();
		$('#tpCL'+j).tooltip();
		$('#tpAct'+j).tooltip();
		$('#tpMsg'+j).tooltip();
		$('#tpNotes'+j).tooltip();		
		$('#tpTrans'+j).tooltip();
		$('#tpCert'+j).tooltip();
		$('#tpMessage'+j).tooltip();
		$('#tpPhone'+j).tooltip();
		$('#aNotes'+j).tooltip();
		$('#tpref'+j).tooltip();
		$('#tpInv'+j).tooltip();
		$('#tprr'+j).tooltip();	
		$('#ftsbu'+j).tooltip();
		$('#tooltipPreview'+j).tooltip()
		$('#removeStatus'+j).tooltip();
		$('#teacherName_'+j).tooltip();
		$('#i4Ques'+j).tooltip();
		try { $('#alum'+j).tooltip(); } catch (e) {}
		try{ $('#tpInternalCandidate'+j).tooltip(); }catch(e){}
		var candidateFlag=document.getElementById("candidateflag").value;
		if(candidateFlag==1)
		{
			var noOfTagsFile=document.getElementById("noOfTagsFile").value;
			if(noOfTagsFile>15){
				iWidth=600;
			}
			else
			{
				iWidth=300;
			}
		}
		else
		{
			iWidth=300;
		}
		$('#tag'+j).popover({ 
		    html : true,
		    placement: 'right',
		    content: $('#tagDiv'+j).html(),
		  }).on("mouseenter", function () {
			  var height=setDialogPosition();
			  height=height+80;	
			  	var style = $('<style>.popover { position: absolute;top: '+height+'px !important;left: 0px;width: '+iWidth+'px; }</style>')
				$('html > head').append(style);
			  	
			  	
		        var _this = this;
		        $(this).popover("show");
		        $(this).siblings(".popover").on("mouseleave", function () {
		            $(_this).popover('hide');
		        });
		    }).on("mouseleave", function () {
		        var _this = this;
		        setTimeout(function () {
		            if (!$(".popover:hover").length) {
		                $(_this).popover("hide")
		            }
		        }, 100);
		    });
		
		$('.profile'+j).popover({ 
		 	trigger: "manual",
		    html : true,
		    placement: 'right',
		    content: $('#profileDetails'+j).html(),
		  }).on("mouseenter", function () {
		        var _this = this;
		        $(this).popover("show");
		        $(this).siblings(".popover").on("mouseleave", function () {
		            $(_this).popover('hide');
		        });
		    }).on("mouseleave", function () {
		        var _this = this;
		        setTimeout(function () {
		            if (!$(".popover:hover").length) {
		                $(_this).popover("hide")
		            }
		        }, 100);
		    });
		
		
		$('#tagaction'+j).popover({ 
		    html : true,
		    placement: 'right',
		    content: $('#tagDiv'+j).html(),
		  });
	}
}

function closeTagActAction(j)
{
	//alert(" closeActAction tagDiv"+j)
	//$('#cgTeacherDivMaster').modal('hide');
	try{
		$('#cgTeacherDivMaster').modal('hide');
	}catch(err)
	  {}
	$('#profile'+j).popover('hide');
	$('#tag'+j).popover('show');
	$('#tag'+j).siblings(".popover").on("click", function () {
		}).on("mouseleave", function () {
			setTimeout(function () {
			if (!$(".popover:hover").length) {
			$('#tag'+j).popover("hide")
			}
			}, 100);
	});
		
}
function closeAllTag(no,val)
{
	//alert(val);
	var noOrRow = no;
	//alert("closeAllTag noOrRow "+noOrRow);
	for(var j=1;j<=noOrRow;j++)
	{	
	//	$('#tag'+j).popover('hide');
	}
}

function tpJbIDisable()
{
	var noOrRow = document.getElementById("tblGrid").rows.length;
	
	for(var j=1;j<=noOrRow;j++)
	{
		$('#tpResume'+j).tooltip();
		$('#tpPortfolio'+j).tooltip();
		$('#tpJSA'+j).tooltip();
		$('#hireTp'+j).tooltip();
		$('#tpPDReport'+j).tooltip();
		$('#tpCL'+j).tooltip();
		$('#tpAct'+j).tooltip();
		$('#tpMsg'+j).tooltip();
		$('#tpNotes'+j).tooltip();
		$('#tpTrans'+j).tooltip();	
		$('#tpCert'+j).tooltip();
		$('#tpMessage'+j).tooltip();
		$('#tpInv'+j).tooltip();
		$('#tprr'+j).tooltip();	
		$('#ftsbu'+j).tooltip();
		$('#tooltipPreview'+j).tooltip()
		$('#teacherName_'+j).tooltip();
		$('#i4Ques'+j).tooltip();
		try { $('#alum'+j).tooltip(); } catch (e) {}
		try{ $('#tpInternalCandidate'+j).tooltip(); }catch(e){}
	}
}
////////// Note scroll 
function applyScrollOnNotesTbl()
{
	try
	{
		var $j;//=jQuery.noConflict(false);
        jQuery(document).ready(function($j) {
        $j('#tblNotes').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 100,
        width: 875,
        minWidth: null,
        minWidthAuto: false,
        colratio:[100,150,475,150], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });            
        });
	}catch(e){alert(e)}
}
//////////Message scroll 
function applyScrollOnMessageTbl()
{
		var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblMessages').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 70,
        width: 614,
        minWidth: null,
        minWidthAuto: false,
        colratio:[100,140,100,215,60], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });            
        });
}


function openWindow(url,windowName,width,height)
{
	windowName=windowName||"_blank";width=width||900;height=height||600;
	var left=(window.screen.availWidth-width)/2;
	var top=(window.screen.availHeight-height)/2;
	var mywindow=window.open(url,windowName,'width='+width+',height='+height+',left='+left+',top='+top+',scrollbars=1');
	var len;

	try{
		len=mywindow.length;
	}catch(err){}

	if ((!mywindow) || len==undefined || mywindow.length!=0 ){
		{
			$('#blockMessage').html(""+resourceJSON.PopUpWindowBlock+" <a target = '_blank' href='"+url+"'>"+resourceJSON.BtnClick+"</a> "+resourceJSON.ToOpenNewWindow+".");
			//$('#myModal').modal('show');
			try{
				$('#myModal').modal('show');
			}catch(err)
			  {}
		}
	}else
		mywindow.focus();

	//for chrome only
	if (navigator && (navigator.userAgent.toLowerCase()).indexOf("chrome") > -1) {
		if (!mywindow)
		{
			$('#blockMessage').html(""+resourceJSON.PopUpWindowBlock+" <a target = '_blank' href='"+url+"'>"+resourceJSON.BtnClick+"</a> "+resourceJSON.ToOpenNewWindow+".");
			//$('#myModal').modal('show');
			try{
				$('#myModal').modal('show');
			}catch(err)
			  {}
		}
		else {
			mywindow.onload = function() {
				setTimeout(function() {
					if (mywindow.screenX === 0) {
						$('#blockMessage').html(""+resourceJSON.PopUpWindowBlock+" <a target = '_blank' href='"+url+"'>"+resourceJSON.BtnClick+"</a> "+resourceJSON.ToOpenNewWindow+".");
						//$('#myModal').modal('show');
						try{
							$('#myModal').modal('show');
						}catch(err)
						  {}
						mywindow.close();
					}
				}, 0);
			};
		}
	}
}
function pausecomp(ms) {
	ms += new Date().getTime();
	while (new Date() < ms){}
	} 
function generatePDReport(teacherId,hrefId)
{
	hideProfilePopover();
	$('#loadingDiv').show();
	CandidateGridAjax.generatePDReport(teacherId,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data)
		{	
			$('#loadingDiv').hide();
			if(deviceType)
			{
				if (data.indexOf(".doc")!=-1)
				{
					$("#docfileNotOpen").css({"z-index":"3000"});
			    	$('#docfileNotOpen').show();
				}
				else if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					$("#exelfileNotOpen").css({"z-index":"3000"});
			    	$('#exelfileNotOpen').show();
				}
				else
				{
					$('#modalDownloadPDR').modal('hide');
					$("#cgTeacherDivMaster").modal('show');
					document.getElementById(hrefId).href=data;				
				}
			}
			else if(deviceTypeAndroid)
			{
				if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					  try{
					    $('#modalDownloadPDR').modal('hide');
						}catch(err)
						  {}
					    document.getElementById('ifrmPDR').src = ""+data+"";
				}
				else
				{
					$('#modalDownloadPDR').modal('hide');
					$("#cgTeacherDivMaster").modal('show');
					document.getElementById(hrefId).href=data;					
				}				
			}
			else if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
			{
				  try{
				    	$('#modalDownloadPDR').modal('hide');
					}catch(err)
					  {}
				    document.getElementById('ifrmPDR').src = ""+data+"";
			}
			else
			{
				$('#modalDownloadPDR').modal('hide');
				document.getElementById('ifrmPDR').src = ""+data+"";
				try{
					$('#modalDownloadPDR').modal('show');
				}catch(err)
				  {}
				
			}		
	  }
	});	
	
}

function downloadGeneralKnowledge(generalKnowledgeExamId,teacherId, linkId)
{		
		CandidateReportAjax.downloadGeneralKnowledge(generalKnowledgeExamId,teacherId,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	if(data=="")
			{
				data="javascript:void(0)";
			}
			else if(data.indexOf(".doc")!=-1)
			{
				document.getElementById("ifrmRef").src=data;
			}else
			{
				document.getElementById(linkId).href = data;
			}
			return false;
		}});
}
function downloadSubjectAreaExam(subjectAreaExamId,teacherId, linkId)
{		
		CandidateReportAjax.downloadSubjectAreaExam(subjectAreaExamId,teacherId,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
			if(data=="")
			{
				data="javascript:void(0)";
			}
			else if(data.indexOf(".doc")!=-1)
			{
				document.getElementById("ifrmRef").src=data;
			}else
			{
				document.getElementById(linkId).href = data;
			}
			return false;
		}});
}



//called from teacher dashboard
function checkBaseCompeleted(teacherId)
{
	CandidateGridAjax.checkBaseCompeleted(teacherId,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){

		//alert(data);
		if(data==1)
		{
			$('#message2show').html(""+resourceJSON.ThankForYourInterest+"clientservices@teachermatch.net  "+resourceJSON.MsgPurchaseYourReport+"");
			//$('#myModal2').modal('show');
			try{
				$('#myModal2').modal('show');
			}catch(err)
			  {}
			notifyTMAdmins(teacherId);

		}else if(data==3)
		{
			$('#message2show').html(""+resourceJSON.BaseInventoryIsTimeOut+" <a href=\"mailto:clientservices@teachermatch.net\">clientservices@teachermatch.net</a> "+resourceJSON.AnyFurtherQues+".");
			//$('#myModal2').modal('show');
			try{
				$('#myModal2').modal('show');
			}catch(err)
			  {}
		}
		else
		{
			$('#message2show').html(""+resourceJSON.CompleteYourBaseInventory+".");
			//$('#myModal2').modal('show');
			try{
				$('#myModal2').modal('show');
			}catch(err)
			  {}
		}
	}
	});	

}
//called form admin
function checkBaseCompeletedForPDR(teacherId,hrefId)
{
	CandidateGridAjax.checkBaseCompeleted(teacherId,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){
		if(data==1)
		{
			generatePDReport(teacherId,hrefId);
			return false;

		}else if(data==3)
		{
			$('#message2show').html(""+resourceJSON.CandidateTimeOutBaseInvevtory+".");
			//$('#myModal2').modal('show');
			try{
				$('#myModal2').modal('show');
			}catch(err)
			  {}
			return false;
		}
		else
		{
			$('#message2show').html(""+resourceJSON.CandidateHasNotComplete+".");
			//$('#myModal2').modal('show');
			try{
				$('#myModal2').modal('show');
			}catch(err)
			  {}
			return false;
		}
	}
	});	

}
function notifyTMAdmins(teacherId)
{
	CandidateGridAjax.notifyTMAdmins(teacherId,{ 
		async: true,
		errorHandler:handleError,
		callback: function(data){
	}
	});	
}

function generatePilarReport(teacherId,jobId,hrefId)
{
	jobOrder={jobId:jobId};
	CandidateGridAjax.generatePilarReport(teacherId,jobOrder,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){

		$('#loadingDiv').hide();
		if(data==0)
		{
			$('#message2show').html(resourceJSON.NoJobSpecificInventory);
			//$('#myModal2').modal('show');
			try{
				$('#myModal2').modal('show');
			}catch(err)
			  {}
			return false;
		}else if(data==1)
		{
			$('#message2show').html(resourceJSON.CandidateHasNotCompleteThisJobOrder);
			//$('#myModal2').modal('show');
			try{
				$('#myModal2').modal('show');
			}catch(err)
			  {}
			return false;
		}else if(data==2)
		{
			$('#message2show').html(resourceJSON.CandidateHasTimeOut);
			//$('#myModal2').modal('show');
			try{
				$('#myModal2').modal('show');
			}catch(err)
			  {}
			return false;
		}else if(data==3)
			generateJSAReport(teacherId,jobOrder,hrefId);
	}
	});	
}

function generateJSAReport(teacherId,jobOrder,hrefId)
{
	$('#loadingDiv').show();	
	CandidateGridAjax.generateJSAReport(teacherId,jobOrder,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){
		$('#loadingDiv').hide();	
		if(deviceType || deviceTypeAndroid)
		{
			document.getElementById(hrefId).href=data;
		}		
		else
		{
			document.getElementById(hrefId).href=data;
		}
		
	}
	});
	
}
function showProfile(teacherId)
{
	$('#myModalProfile').show();
}
function hideProfile()
{
	$('#myModalProfile').hide();
}
function statusChange(imgFlag,status){
	statusTxt=status;
	statusValue=imgFlag;
	if(status=='H'){
		if(imgFlag==0){
			document.getElementById("pinkHired").style.display="inline";
			document.getElementById("greenHired").style.display="none";
			document.getElementById("greenRemoved").style.display="none";
			document.getElementById("pinkRemoved").style.display="inline";
			document.getElementById("pinkRemovedOnly").style.display="none";
		}else{
			document.getElementById("pinkHired").style.display="none";
			document.getElementById("greenHired").style.display="inline";
			document.getElementById("greenRemoved").style.display="none";
			document.getElementById("pinkRemoved").style.display="none";
			document.getElementById("pinkRemovedOnly").style.display="inline";
		}
	}else if(status=='R'){
		if(imgFlag==0){
			document.getElementById("pinkRemoved").style.display="inline";
			document.getElementById("greenRemoved").style.display="none";
			document.getElementById("pinkHired").style.display="inline";
			document.getElementById("greenHired").style.display="none";
			document.getElementById("pinkHiredOnly").style.display="none";
		}else{
			document.getElementById("pinkRemoved").style.display="none";
			document.getElementById("greenRemoved").style.display="inline";
			document.getElementById("pinkHired").style.display="none";
			document.getElementById("greenHired").style.display="none";
			document.getElementById("pinkHiredOnly").style.display="inline";
		}
	}
}
function deletesavefolder(rootNode)
{
	//alert(" deletesavefolder rootNode "+rootNode);
	$('#currentObject').val(rootNode.data.key);
	//$('#deleteFolder').modal('show');
	try{
		$('#deleteFolder').modal('show');
	}catch(err)
	  {}

	
}
function deleteconfirm()
{
	//alert("delexcv          m");
	//$('#deleteFolder').modal('hide');
	try{
		$('#deleteFolder').modal('hide');
	}catch(err)
	  {}
	var rootNodeKey =$('#currentObject').val();
	//parent.deleteFolder();
	document.getElementById('iframeSaveCandidate').contentWindow.deleteFolder(rootNodeKey);
	//document.getElementById('target_Frame').contentWindow.callingtargetFunction();
	//window.frames["iframeSaveCandidate"].deleteFolder();
	//window.frames["original_preview_iframe"].exportAndView(img_id);//
}

function saveToFolder(jftteacherId,flagpopover)
{	
	defaultSaveDiv();
	//alert(" teacherId "+teacherId +" === flagpopover "+flagpopover);
	if(jftteacherId!="")
	{
		$("#teachetIdFromPoPUp").val(jftteacherId);
		$("#txtflagpopover").val(flagpopover);
	}	
/*------------- Here I am creating only Home and Shared Folder For User*/	
	//$('#saveToFolderDiv').modal('show');
	try{
		$('#saveToFolderDiv').modal('show');
	}catch(err)
	  {}
	$('#errordiv').hide();
	var iframe = document.getElementById('iframeSaveCandidate');
	var innerDoc = iframe.contentDocument || iframe.contentWindow.document;
	
	innerDoc.getElementById('errortreediv').style.display="none";
	innerDoc.getElementById('errordeletetreediv').style.display="none";
	

	
	var folderId = innerDoc.getElementById('frame_folderId').value;
	if(innerDoc.getElementById('frame_folderId').value!="" || innerDoc.getElementById('frame_folderId').value.length>0)
	{
		var checkboxshowHideFlag =0; // For hide checkboxes on Save to Folder Div
		displaySavedCGgridByFolderId(0,folderId,checkboxshowHideFlag);
		// return false;
	}
	
	CandidateGridAjax.createHomeFolder(
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			//alert(" data "+data); data will be 2: when first time Home and shared folder will be created
			if(data==2)
			{
				document.getElementById("iframeSaveCandidate").src="tree.do";
			}
		}
	});
}

function saveCandidate() //=== It will only for display Candidate before Save 
{
	//alert("saveCandidate ");
	var savecandidatearray="";
	/*var inputs = document.getElementsByTagName("input"); 
	 for (var i = 0; i < inputs.length; i++) {
	        if (inputs[i].type === 'checkbox') {
	            if(inputs[i].checked){
	            	savecandidatearray+=	inputs[i].value+",";
	            }
	        }
	 } */
	 
	 var inputs = document.getElementsByName("cgCBX"); 
	 for (var i = 0; i < inputs.length; i++) {
	        if (inputs[i].type === 'checkbox') {
	        	if(inputs[i].checked){
	        		savecandidatearray+=	inputs[i].value+",";
	            }
	        }
	 } 
	 
	 $("#savecandidatearray").val(savecandidatearray);
	 	//alert(" savecandidatearray "+savecandidatearray);
	 
		CandidateGridAjax.saveCandidate(savecandidatearray,
		{ 
			async: false,
			errorHandler:handleError,
			callback:function(data)
			{	
				//alert(" data "+data); data will be 2: when first time Home and shared folder will be created
				if(data!=null)
				{
					document.getElementById("savedCandidateGrid").innerHTML=data;
					displaySaveCandidatePopUpDiv();
				}
			}
		});
		
}

function saveCandidateToFolderByUser()
{
	document.getElementById('iframeSaveCandidate').contentWindow.checkFolderId();
	var iframe = document.getElementById('iframeSaveCandidate');
	var innerDoc = iframe.contentDocument || iframe.contentWindow.document;
	var folderId = innerDoc.getElementById('frame_folderId').value;
	//alert(" frame_folderId sdfsdfsdf "+folderId+" value "+folderId.value+" :::::: "+folderId.length);
	//alert(" folderId "+folderId);
	var frameHeight=iframe.contentWindow.document.body.scrollHeight;
	$('iframe').css('height',frameHeight);
	var fId="";
	if(innerDoc.getElementById('frame_folderId').value=="" || innerDoc.getElementById('frame_folderId').value.length==0)
	{
		//alert("Yes")
		innerDoc.getElementById('errortreediv').style.display="block";
		innerDoc.getElementById('errortreediv').innerHTML="&#149; "+resourceJSON.PlzSelectAnyFolder+"";
		 return false;
	}
	else
	{
		//alert(" Else No ");
		//fId=folderId;
	}
	//alert(" sgfsdgb "+innerDoc.getElementById('frame_folderId').value);
	var savecandidatearray="";
	/*var inputs = document.getElementsByTagName("input"); 
	 for (var i = 0; i < inputs.length; i++) {
	        if (inputs[i].type === 'checkbox') {
	            if(inputs[i].checked){
	            	savecandidatearray+=	inputs[i].value+",";
	            }
	        }
	 }*/
	
	
//	alert(document.getElementsByName("cgCBX").length);
	
	 var inputs = document.getElementsByName("cgCBX");
	 
	 
	 
	 
	 
	 for (var i = 0; i < inputs.length; i++) {
	        if (inputs[i].type === 'checkbox') {
	        	if(inputs[i].checked){
	        		savecandidatearray+=	inputs[i].value+",";
	            }
	        }
	 } 
	 	
	 	if($("#txtflagpopover").val()==1 && $("#teachetIdFromPoPUp").val()!="")
		{
	 		savecandidatearray	=	$("#teachetIdFromPoPUp").val();
		}	
	
	 	//alert(" savecandidatearray "+savecandidatearray);
	 	//return false;
	 	
	 $("#savecandidatearray").val(savecandidatearray);
	CandidateGridAjax.saveCandidateToFolderByUser(savecandidatearray,folderId,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			//alert(" data "+data);// data will be 2: when first time Home and shared folder will be created
			if(data==1)
			{
				//$("#saveToFolderDiv").modal("hide");
				//$('#saveAndShareConfirmDiv').html("You have successfully saved the Candidates.");
				//$('#shareConfirm').modal("show");
				
				try{
					$("#saveToFolderDiv").modal("hide");
					$('#saveAndShareConfirmDiv').html(resourceJSON.SavedCandidate);
					$('#shareConfirm').modal("show");
				}catch(err)
				  {}
				//document.getElementById("savedCandidateGrid").innerHTML=data;
				//displaySaveCandidatePopUpDiv();
				uncheckedAllCBX();
			}
			if(data==3)
			{
				//alert("Duplicate Candaidate found ");
				$("#txtoverrideFolderId").val(folderId);
				try{
					$("#duplicatCandidate").modal("show");
				}catch(err)
				  {}
			}
		}
	});
	 
	 
}
function saveWithDuplicateRecord()
{
	var folderId= $("#txtoverrideFolderId").val();
	var savecandidatearray= $("#savecandidatearray").val();
	//alert(" savecandidatearray "+savecandidatearray+" folderId "+folderId);
	CandidateGridAjax.saveWithDuplicateRecord(savecandidatearray,folderId,
			{ 
				async: false,
				errorHandler:handleError,
				callback:function(data)
				{	
					//alert(" data "+data);// data will be 2: when first time Home and shared folder will be created
					if(data==1)
					{
						//$("#saveToFolderDiv").modal("hide");
						//$("#duplicatCandidate").modal("hide");
						try{
							$("#saveToFolderDiv").modal("hide");
							$("#duplicatCandidate").modal("hide");
						}catch(err)
						  {}
						uncheckedAllCBX();
						//document.getElementById("savedCandidateGrid").innerHTML=data;
						//displaySaveCandidatePopUpDiv();
					}
				}
			});
	
}

function displaySavedCGgridByFolderId(hoverDisplay,folderId,checkboxshowHideFlag)
{
	var pageFlag= document.getElementById("pageFlag").value;
	currentPageFlag="stt";
	//document.getElementById("checkboxshowHideFlag").value=checkboxshowHideFlag;
	document.getElementById("folderId").value=folderId;
	CandidateGridAjax.displaySavedCGgridByFolderId(pageFlag,folderId,pageFlag,pageSTT,noOfRowsSTT,sortOrderStrSTT,sortOrderTypeSTT,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			if(data!=null)
			{
				if(pageFlag==1){
					document.getElementById("savedCGgridMyFolder").innerHTML=data;
				}else{
					document.getElementById("savedCandidateGrid").innerHTML=data;
				}
				//$("#home_folderId").val(folderId);
				displaySaveCandidatePopUpDiv();
			}
		}
	});
}


function uncheckedAllCBX()
{
	//alert("unchecked");
/*	var inputs = document.getElementsByTagName("input"); 
	 for (var i = 0; i < inputs.length; i++) {
	        if (inputs[i].type === 'checkbox') {
	        	inputs[i].checked=false;
	        }
	 } */
	 
	 var inputs = document.getElementsByName("cgCBX"); 
	 for (var i = 0; i < inputs.length; i++) {
	        if (inputs[i].type === 'checkbox') {
	        	inputs[i].checked=false;
	        }
	 } 
	 $("#actionDiv").hide();
}

/* ===== Gagan : displaySaveCandidatePopUpDiv() Method =========== */
function displaySaveCandidatePopUpDiv()
{
	
	var noOrRow = document.getElementById("saveCandidateTable").rows.length;
	for(var j=5000;j<=5000+noOrRow;j++)
	{
		
		$('.profile'+j).popover({ 
		 	trigger: "manual",
		    html : true,
		    placement: 'right',
		    content: $('#profileDetails'+j).html(),
		  }).on("mouseenter", function () {
		        var _this = this;
		        $(this).popover("show");
		        $(this).siblings(".popover").on("mouseleave", function () {
		            $(_this).popover('hide');
		        });
		    }).on("mouseleave", function () {
		        var _this = this;
		        setTimeout(function () {
		            if (!$(".popover:hover").length) {
		                $(_this).popover("hide")
		            }
		        }, 100);
		    });
	}
}



/*==================Gagan: Share Div Function Start Here ======================*/
function displayShareFolder(jftteacherId,flagpopover)
{
	if(jftteacherId!="")
	{
		$("#JFTteachetIdFromSharePoPUp").val(jftteacherId);
		$("#txtflagSharepopover").val(flagpopover);
	}	
	
	
	document.getElementById("userFPS").value=0;
	document.getElementById("userFPD").value=0;
	document.getElementById("userCPS").value=0;
	document.getElementById("userCPD").value=1;
	currentPageFlag="us";
	$('#errorinvalidschooldiv').hide();
	if($('#entityType').val()==2)
	{
		$('#schoolId').val("0");
		$('#schoolName').val("");
	}
	else
	{
		if($('#entityType').val()==3)
		{
			$('#schoolId').val($('#loggedInschoolId').val());
			$('#schoolName').val($('#loggedInschoolName').val());
		}
	}

	var savecandiadetidarray="";	
	 
	 var inputs = document.getElementsByName("cgCBX"); 
	 for (var i = 0; i < inputs.length; i++) {
	        if (inputs[i].type === 'checkbox') {
	        	if(inputs[i].checked){
	        		savecandiadetidarray+=	inputs[i].value+",";
	            }
	        }
	 } 
	 //alert(" savecandiadetidarray "+savecandiadetidarray);
	 $("#savecandiadetidarray").val(savecandiadetidarray);
	/*if(savecandiadetidarray!="")
	{*/
		try{
			$('#shareDiv').modal('show');
		}catch(e){}
		
		var entityType	=	$('#entityType').val();
		var districtId	=	$('#districtId').val();
		var schoolId	=	trim($('#schoolId').val());
		if(schoolId=="")
		{
			$('#errorinvalidschooldiv').show();
			$('#errorinvalidschooldiv').html("&#149; "+resourceJSON.SelectValidSchool+"");
			return false;
		}
		if($('#entityType').val()!="")
		{
			/* ========= Display School List ============ */
			CandidateGridAjax.getDistictSchoolUserList(entityType,districtId,schoolId,pageUS,noOfRowsUS,sortOrderStrUS,sortOrderTypeUS,
			{ 
				async: false,
				errorHandler:handleError,
				callback:function(data)
				{	
				if(data!="")
				{
					$('#divShareCandidateToUserGrid').html(data);
				}
			}
			});
		}
	
	/*}
	else
	{
		$("#errortreediv").show();
		$('#errortreediv').html("&#149; Please select Candidates");
	}
	*/
}


function searchUserthroughPopUp(searchFlag)
{
	if(searchFlag==1)
	{
		defaultShareDiv();
	}
	document.getElementById("userFPS").value=0;
	document.getElementById("userFPD").value=0;
	document.getElementById("userCPS").value=1;
	document.getElementById("userCPD").value=0;
	currentPageFlag="us";
	$('#errorinvalidschooldiv').hide();
	var savecandiadetidarray="";
	 
	 var inputs = document.getElementsByName("cgCBX"); 
	 for (var i = 0; i < inputs.length; i++) {
	        if (inputs[i].type === 'checkbox') {
	        	if(inputs[i].checked){
	        		savecandiadetidarray+=	inputs[i].value+",";
	            }
	        }
	 } 
	
	if(savecandiadetidarray!="" || $("#txtflagSharepopover").val()==1)
	{
		//$('#shareDiv').modal('show');
		try{
			$('#shareDiv').modal('show');
		}catch(err)
		  {}
		var entityType	=	$('#entityType').val();
		var districtId	=	$('#districtId').val();
		var schoolId	=	trim($('#schoolId').val());
		var schoolName	=	trim($('#schoolName').val());
		if(schoolId=="")
		{
			if( schoolName!="")
			{
				$('#errorinvalidschooldiv').show();
				$('#errorinvalidschooldiv').html("&#149; "+resourceJSON.SelectValidSchool+"");
				return false;
			}
			else
			{
				schoolId=0;
			}
			
		}
		if($('#entityType').val()!="")
		{
			CandidateGridAjax.getDistictSchoolUserList(entityType,districtId,schoolId,pageUS,noOfRowsUS,sortOrderStrUS,sortOrderTypeUS,
			{ 
				async: false,
				errorHandler:handleError,
				callback:function(data)
				{	
				if(data!="")
				{
					$('#divShareCandidateToUserGrid').html(data);
				}
			}
			});
		}
	}
	
}




function shareCandidatethroughPopUp()
{
	defaultSaveDiv();
	$('#errorinvalidschooldiv').hide();
	$('#errorinvalidschooldiv').empty();
	var userIdarray =	"";
	var myUsercbx	=	document.getElementsByName("myUsercbx");
	
	var folderId = document.getElementById('folderId').value;
	
	 for (e=0;e<myUsercbx.length;e++) 
	 {
	  if (myUsercbx[e].checked==true) 
	  {
		  userIdarray+=myUsercbx[e].value+",";
	   }
	 }
	 	if(userIdarray=="")
	 	{
	 		$('#errorinvalidschooldiv').show();
	 		$('#errorinvalidschooldiv').html("&#149; "+resourceJSON.PlzSelectUsr+"");
	 	}
	 	
	 	var jobforteacheridarray	=	"";
	 /*	var inputs = document.getElementsByTagName("input"); 
		 for (var i = 0; i < inputs.length; i++) {
		        if (inputs[i].type === 'checkbox') {
		            if(inputs[i].checked){
		            	jobforteacheridarray+=	inputs[i].value+",";
		            }
		        }
		 } 
	 	*/
		 var inputs = document.getElementsByName("cgCBX"); 
		 for (var i = 0; i < inputs.length; i++) {
		        if (inputs[i].type === 'checkbox') {
		        	if(inputs[i].checked){
		        		jobforteacheridarray+=	inputs[i].value+",";
		            }
		        }
		 } 
		 if($("#txtflagSharepopover").val()==1 && $("#JFTteachetIdFromSharePoPUp").val()!="")
			{
			 jobforteacheridarray	=	$("#JFTteachetIdFromSharePoPUp").val();
			}	
		 //alert(" jobforteacheridarray  "+jobforteacheridarray);
		 //return false;
	 	//jobfor teacheridarray = $("#savecandiadetidarray").val();
	 	//alert(" userIdarray "+userIdarray+" jobforteacheridarray "+jobforteacheridarray);
	
		CandidateGridAjax.shareCandidatesToUserFromCG(userIdarray,jobforteacheridarray,folderId,
		{ 
			async: false,
			errorHandler:handleError,
			callback:function(data)
			{	
				//alert(" ==== data === "+data);
			if(data==1)
			{
				//$('#shareDiv').modal("hide");
				//$('#saveAndShareConfirmDiv').html("You have successfully shared the Candidates to the selected Users.");
				//$('#shareConfirm').modal("show");
				
				try{
					$('#shareDiv').modal("hide");
					$('#saveAndShareConfirmDiv').html(resourceJSON.SucceccfullySharedCandidates);
					$('#shareConfirm').modal("show");
				}catch(err)
				  {}
				uncheckAllCbx();
				uncheckedAllCBX();
			}
		}
		});
	 
	 
}
function uncheckAllCbx()
{
	
	var inputs = document.getElementsByName("mycbx"); 
	 for (var i = 0; i < inputs.length; i++) {
	        if (inputs[i].type === 'checkbox') {
	        	if(inputs[i].checked){
	            	//savecandiadetidarray+=	inputs[i].value+",";
	        		inputs[i].checked=false;
	            }
	        }
	 } 
	
}
function refreshStatus(){
	//alert("refreshStatus");
	document.getElementById("expandremovechk").value=0;
	document.getElementById("expandincompchk").value=0;
	document.getElementById("expandinterchk").value=0;
	document.getElementById("expandvltchk").value=0;
	document.getElementById("expandwithdrchk").value=0;
	document.getElementById("expandhiredchk").value=0;
}
function hideStatus()
{
	//$('#myModalStatus').hide();
	$('#myModalStatus').modal('hide');
	//getCandidateGrid();
	//refreshStatus();
	try {
		$('#spanOverride').modal('hide');
	} catch (e) {}
}
var teacherId;
var noOfRowsForJob=10;
function defaultSet(){
	pageForTC = 1;
	noOfRowsForTC =100;
	noOfRowsForJob=10;
	sortOrderStrForTC="";
	sortOrderTypeForTC="";
	currentPageFlag="";
	//alert(noOfRowsForJob);
}

function getJobList(tId){
	
	hideProfilePopover();
	
	defaultSet();
	currentPageFlag='job';
	teacherId=tId;
	$('#loadingDiv').show();
	getJobOrderList();

}

function getJobOrderList(){
	$('#loadingDiv').show();
	currentPageFlag='job';
	var visitLocation=document.getElementById("teacherIdForprofileGridVisitLocation").value;
	if(visitLocation=='My Folders'){
		TeacherInfotAjax.getJobListByTeacherByProfile(teacherId,noOfRowsForJob,pageJob,sortOrderStrJob,sortOrderTypeJob,false,visitLocation,0,0, { 
			async: true,
			callback: function(data)
			{
			try{
			$('#loadingDiv').hide();	
			//$('#myModalJobList').show(); 
			$('#myModalJobList').modal('show');
			document.getElementById("divJob").innerHTML=data;
			//applyScrollOnJob();
			jobEnable();
			}catch(err)
			{}
			},
			errorHandler:handleError 
		});
	}
	else
	{
		TeacherInfotAjax.getJobListByTeacher(teacherId,noOfRowsForJob,pageJob,sortOrderStrJob,sortOrderTypeJob,false, { 
			async: true,
			callback: function(data)
			{
				try{
				$('#loadingDiv').hide();	
				//$('#myModalJobList').show();
				$('#myModalJobList').modal('show');
				document.getElementById("divJob").innerHTML=data;
				applyScrollOnJob();
				jobEnable();
				}catch(err){}
			},
			errorHandler:handleError 
		});
	}
}
function descriptionShow(jobId)
{
	$("#myModalJobList").css({"z-index":"1"});
	TeacherInfotAjax.getJobDescription(jobId, { 
		async: true,
		callback: function(data)
		{
			//$('#myModalJobList').modal('hide');
			//$('#myModalDesc').modal('show');
			try{
				$('#myModalJobList').modal('hide');
				$('#myModalDesc').modal('show');
			}catch(err)
			  {}
			document.getElementById("description").innerHTML=data;
		},
		errorHandler:handleError 
	});
	
}

function jobEnable()
{
	var noOrRow = document.getElementById("jobTable").rows.length;
	for(var j=1;j<=noOrRow;j++)
	{
		
		$('#jobCom'+j).tooltip();
		$('#tpJSA'+j).tooltip();
		$('#hireTp'+j).tooltip();
		$('#jobCL'+j).tooltip();
		$('#jobTrans'+j).tooltip();
		$('#jobCert'+j).tooltip();
		$('#jobJobId'+j).tooltip();
		$('#jobJobTitle'+j).tooltip();
	}
}
function setZIndexJobDiv()
{
	defaultSet();
	currentPageFlag="job";
	//$('#myModalDesc').modal('hide');
	//$('#myModalCommunications').modal('hide');
	//$('#myModalCoverLetter').modal('hide');
	try{
		$('#myModalDesc').modal('hide');
		$('#myModalCommunications').modal('hide');
		$('#myModalCoverLetter').modal('hide');
	}catch(err)
	  {}
	
	$("#myModalJobList").css({"z-index":"3000"});
	if(document.getElementById("commDivFlag").value==0){
		//$('#myModalJobList').modal('show');
		try{
			$('#myModalJobList').modal('show');
		}catch(err)
		  {}
	}
}
function showCoverLetter(teacherId, jobId)
{	
	CandidateReportAjax.getCoverLetter(teacherId,jobId,
	{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{	
			if(data=="")
			{
				$('#myModalMsgShow').html(resourceJSON.CandidateNotProvidedCoverLtr);
				//$('#myModalJobList').modal('hide');
				//$('#myModalMsg').modal('show');
				try{
					$('#myModalJobList').modal('hide');
					$('#myModalMsg').modal('show');
				}catch(err)
				  {}

			}
			else
			{
				document.getElementById("lblCL").innerHTML=""+data;
				//$('#myModalJobList').modal('hide');
				//$('#myModalCoverLetter').modal('show');
				try{
					$('#myModalJobList').modal('hide');
					$('#myModalCoverLetter').modal('show');
				}catch(err)
				  {}
			}		
		}
	});
}

function hideJob()
{
	$('#myModalJob').hide();
}

function showTag(status,teacherAssessmentStatusId,jobForTeacherId,teacherId)
{
	//alert("showTag ");
	//$('#myModalTag').show();
	//$('#tag'+teacherId).show();
	//document.getElementById("tag"+teacherId).click();
	//$('#errordivAct').empty();	
	CandidateGridAjax.showActDiv(status,teacherAssessmentStatusId,jobForTeacherId,{ 
	async: true,
	errorHandler:handleError,
	callback:function(data)
	{	
		//document.getElementById("divAct").innerHTML=data.toString();
		//$('#myModalAct').modal('show');		
		//alert(" data "+data);
		$('#tagbody'+teacherId).html(data.toString())
		$('#tag'+teacherId).popover({ 
		    html : true,
		    placement: 'right',
		    content: $('#tagDiv'+teacherId).html(),
		  });
		
		
	}});	
	
	
	
	/*$('#tag'+teacherId).popover({ 
	    html : true,
	    placement: 'right',
	    content: $('#tagDiv'+teacherId).html(),
	  });*/
	
	/*$('.profile'+divNo).popover({ 
	 	trigger: "manual",
	    html : true,
	    placement: 'right',
	    content: function() {
	      return $('#profileDetails'+divNo).html();
	    }
	  }).on("mouseenter", function () {
	        var _this = this;
	        $(this).popover("show");
	        $(this).siblings(".popover").on("mouseleave", function () {
	            $(_this).popover('hide');
	        });
	    }).on("mouseleave", function () {
	        var _this = this;
	        setTimeout(function () {
	            if (!$(".popover:hover").length) {
	                $(_this).popover("hide")
	            }
	        }, 100);
	    });
	    */
}
function hideTag()
{
	$('#myModalTag').hide();
}
function showNorm(teacherId,jobId,normScore)
{
	/*//alert(teacherId+" "+jobId);
	var teacherDetail = {teacherId:teacherId};
	var jobOrder = {jobId:jobId};
	
	CandidateGridAjax.generateNormalCurve(teacherDetail,jobOrder,normScore,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{	
			 var chart = new FusionCharts("fusioncharts/MSSplineArea.swf", "ChartId", "500", "330", "0", "1");
			   chart.setXMLUrl("fusioncharts/xmls/"+data);		
			    chart.setTransparent(true);
			   chart.render("divNorm");
			   
			//$('#myModalNorm').show();
			//$('#myModalNorm').fadeIn(500);
			$('#myModalNorm').modal('toggle');
		
		}});*/
}
function hideNorm()
{
	//$('#myModalNorm').fadeOut(500);
	//$('#myModalNorm').modal('toggle');
	try{
		$('#myModalNorm').modal('toggle');
	}catch(err)
	  {}
}

function showNormDistribution(jobId)
{
	/*var jobOrder = {jobId:jobId};
	
	CandidateGridAjax.generateNormDistribution(jobOrder,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{	
			 var chart = new FusionCharts("fusioncharts/MSSplineArea.swf", "ChartId", "500", "330", "0", "1");
			   chart.setXMLUrl("fusioncharts/xmls/"+data);		
			    chart.setTransparent(true);
			   chart.render("divNorm");
			   
			//$('#myModalNorm').show();
			//$('#myModalNorm').fadeIn(500);
			$('#myModalNorm').modal('toggle');
		
		}});*/
	
}
function showProfileDetails(divNo)
{
	$('.profile'+divNo).popover({ 
	 	trigger: "manual",
	    html : true,
	    placement: 'right',
	    content: function() {
	      return $('#profileDetails'+divNo).html();
	    }
	  }).on("mouseenter", function () {
	        var _this = this;
	        $(this).popover("show");
	        $(this).siblings(".popover").on("mouseleave", function () {
	            $(_this).popover('hide');
	        });
	    }).on("mouseleave", function () {
	        var _this = this;
	        setTimeout(function () {
	            if (!$(".popover:hover").length) {
	                $(_this).popover("hide")
	            }
	        }, 100);
	    });
}
function hideProfileDetails(teacherId)
{
	$('#myModalProfileDetails'+teacherId).hide();
}

function actionMenu(name,id){
	var inputs = document.getElementsByTagName("input"); 
	var chkCount=0;
	var chkCheckedCount=0;
	 for (var i = 0; i < inputs.length; i++) {
	        if (inputs[i].type === 'checkbox') {
	        	chkCount++;
	            if(inputs[i].checked){
	            	chkCheckedCount++;
	            }
	        }
	 } 
	 if(chkCheckedCount>0){
		 document.getElementById("actionDiv").style.display="inline";
	 }else{
		 document.getElementById("actionDiv").style.display="none";
	 }
}
function displayAdvanceSearch()
{
	$('#searchLinkDiv').fadeOut();
	$('#advanceSearchDiv').slideDown('slow');
}

var count=0;
var index=-1;
var length=0;
var divid='';
var txtid='';
var hiddenDataArray = new Array();
var showDataArray = new Array();
var degreeTypeArray = new Array();
var hiddenId="";
function getDistrictAuto(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("districtName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getDistrictArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getDistrictArray(districtName){
	var searchArray = new Array();
		BatchJobOrdersAjax.getFieldOfDistrictList(districtName,{ 
			async: false,
			callback: function(data){
			hiddenDataArray = new Array();
			showDataArray = new Array();
			for(i=0;i<data.length;i++){
				searchArray[i]=data[i].districtName;
				showDataArray[i]=data[i].districtName;
				hiddenDataArray[i]=data[i].districtId;
			}
		},
		errorHandler:handleError
		});	
	return searchArray;
}


var selectFirst="";

var fatchData= function(txtSearch,searchArray,txtId,txtdivid){
	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
				
			}
			
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}
function hideDistrictMasterDiv(dis,hiddenId,divId)
{
	document.getElementById("districtId").value="";
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			document.getElementById("districtId").value=hiddenDataArray[index];
			document.getElementById('schoolName').readOnly=false;
			document.getElementById('schoolName').value="";
			document.getElementById('schoolId').value="0";
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}

count=0;
index=-1;
length=0;
divid='';
txtid='';
hiddenDataArray = new Array();
showDataArray = new Array();
degreeTypeArray = new Array();
hiddenId="";

function getUniversityAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38)
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13)
	{
		
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		document.getElementById("fieldName").focus();
		
	} 
	else if(event.keyCode==9)
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getUniversityArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getUniversityArray(universityName){
	
	var searchArray = new Array();
	DWRAutoComplete.getUniversityMasterList(universityName,{ 
		async: false,		
		callback: function(data){
		hiddenDataArray = new Array();
		showDataArray = new Array();
		for(i=0;i<data.length;i++){
			searchArray[i]=data[i].universityName;
			hiddenDataArray[i]=data[i].universityId;
			showDataArray[i]=data[i].universityName;
		}
	}
	});	

	return searchArray;
}


function hideUniversityDiv(dis,hiddenId,divId)
{
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		document.getElementById(hiddenId).value=hiddenDataArray[index];
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
		if(dis.value!="")
		{
			var cnt=0;
			var focs=0;	
			$('#errordiv').empty();	
			setDefColortoErrorMsg();
			$('#errordiv').append("&#149; "+resourceJSON.PlzEtrValidClg+"<br>");
			if(focs==0)
				$('#universityName').focus();
			
			$('#universityName').css("background-color",txtBgColor);
			cnt++;focs++;	
		}
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}

count=0;
index=-1;
length=0;
divid='';
txtid='';
hiddenDataArray = new Array();
showDataArray = new Array();
hiddenId="";

function getRegionAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38)
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13)
	{
		
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		document.getElementById("regionName").focus();
		
	} 
	else if(event.keyCode==9)
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getRegionArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getRegionArray(regionName){
	
	var searchArray = new Array();
	DWRAutoComplete.getRegionMasterList(regionName,{ 
		async: false,		
		callback: function(data){
		hiddenDataArray = new Array();
		showDataArray = new Array();
		for(i=0;i<data.length;i++){
			searchArray[i]=data[i].regionName;
			hiddenDataArray[i]=data[i].regionId;
			showDataArray[i]=data[i].regionName;
		}
	}
	});	

	return searchArray;
}


function hideRegionDiv(dis,hiddenId,divId)
{
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		document.getElementById(hiddenId).value=hiddenDataArray[index];
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
		if(dis.value!="")
		{
			var cnt=0;
			var focs=0;	
			$('#errordiv').empty();	
			$('#errordiv').append("&#149; "+resourceJSON.PlzEtrVldRegionName+"<br>");
			if(focs==0)
				$('#regionName').focus();
			
			$('#regionName').css("background-color",txtBgColor);
			cnt++;focs++;	
		}
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}


count=0;
index=-1;
length=0;
divid='';
txtid='';
hiddenDataArray = new Array();
showDataArray = new Array();
degreeTypeArray = new Array();
hiddenId="";

function getDegreeMasterAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	$('#degreeName').mousedown(function(e) {
	if( (event.keyCode == 1) ){
        alert("left button");
    }if( (event.keyCode== 3) ) {
        alert("right button");
    }else if( (event.keyCode== 2) ) {
        alert("middle button"); 
    }
	});
	
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		document.getElementById("universityName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getDegreeMasterArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getDegreeMasterArray(degreeName){
	var searchArray = new Array();
	DWRAutoComplete.getDegreeMasterList(degreeName,{ 
		async: false,		
		callback: function(data){
		hiddenDataArray = new Array();
		showDataArray = new Array();
		for(i=0;i<data.length;i++){
			searchArray[i]=data[i].degreeName;
			hiddenDataArray[i]=data[i].degreeId;
			showDataArray[i]=data[i].degreeName;
			degreeTypeArray[i]=data[i].degreeType;
		}
	}
	});	

	return searchArray;
}

function hideDegreeMasterDiv(dis,hiddenId,divId)
{
	document.getElementById("degreeType").value="";
	if(parseInt(length)>0)
	{
		if(index==-1)
		{
			index=0;
		}
			document.getElementById(hiddenId).value=hiddenDataArray[index];			
			var degreeName = document.getElementById("degreeName").value;
			var degreeType = document.getElementById("degreeType").value=degreeTypeArray[index];
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
			document.getElementById("degreeType").value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
		}
	}else{
		if(document.getElementById(hiddenId))
		{
			document.getElementById(hiddenId).value="";
		}
		if(dis.value!=""){
			var cnt=0;
			var focs=0;	
			$('#errordiv').empty();	
			setDefColortoErrorMsg();
			$('#errordiv').append("&#149; "+resourceJSON.PlzEtrVldDegree+"<br>");
			if(focs==0)
				$('#degreeName').focus();
			
			$('#degreeName').css("background-color",txtBgColor);
			cnt++;focs++;	
		}
			
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}



count=0;
index=-1;
length=0;
divid='';
txtid='';
hiddenDataArray = new Array();
showDataArray = new Array();
degreeTypeArray = new Array();
hiddenId="";

function getSchoolAuto(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("schoolName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getSchoolArray(txtSearch.value);
		fatchData2(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getSchoolArray(schoolName){
	var searchArray = new Array();
	var districtId	=	document.getElementById("districtId").value;
		BatchJobOrdersAjax.getFieldOfSchoolList(districtId,schoolName,"all",{
			async: false,
			callback: function(data){
			hiddenDataArray = new Array();
			showDataArray = new Array();
			for(i=0;i<data.length;i++){
				searchArray[i]=data[i].schoolName;
				showDataArray[i]=data[i].schoolName;
				hiddenDataArray[i]=data[i].schoolMaster.schoolId;
			}
		},
		errorHandler:handleError
		});	
	return searchArray;
}


selectFirst="";

var fatchData2= function(txtSearch,searchArray,txtId,txtdivid){

	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
				
			}
			
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}
function hideSchoolMasterDiv(dis,hiddenId,divId)
{
	document.getElementById("schoolId").value="";
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			document.getElementById("schoolId").value=hiddenDataArray[index];
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}




var downArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index<length-1){
			for(var i=0;i<10;i++){
				{
					if(document.getElementById('divResult'+txtdivid+i))
						var div_id=document.getElementById('divResult'+txtdivid+i);
					if(div_id)
					{
						if(div_id.className=='over')
							index=div_id.id.split('divResult'+txtdivid)[1];
						div_id.className='normal';
					}
				}
			}
			index++;
			if(document.getElementById('divResult'+txtdivid+index))
			{
				var div_id=document.getElementById('divResult'+txtdivid+index);
				div_id.className='over';
				document.getElementById(txtid).value = div_id.innerHTML;
				selectFirst=div_id.innerHTML;
			}
		}
	}
}

var upArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index>0){
			for(var i=0;i<length;i++){

				var div_id=document.getElementById('divResult'+txtdivid+i);
				if(div_id)
				{
					if(div_id.className=='over')
						index=div_id.id.split('divResult'+txtdivid)[1];
					div_id.className='normal';
				}
			}
			index--;
			if(document.getElementById('divResult'+txtdivid+index))
				document.getElementById('divResult'+txtdivid+index).className='over';
			if(txtid && document.getElementById('divResult'+txtdivid+index))
			{
				document.getElementById(txtid).value=
					document.getElementById('divResult'+txtdivid+index).innerHTML;
				selectFirst=document.getElementById('divResult'+txtdivid+index).innerHTML;
			}
		}
	}
}

var overText=function (div_value,txtdivid) {

	for(var i=0;i<length;i++)
	{
		if(document.getElementById('divResult'+i))
		{
			var div_id=document.getElementById('divResult'+i);

			if(div_id.className=='over')
				index=div_id.id.split(txtdivid)[1];
			div_id.className='normal';
		}
	}
	div_value.className = 'over';
	document.getElementById(txtid).value= div_value.innerHTML;
}
function trim(s)
{
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}
function checkForInt(evt) {

	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==8)
		return true;

	return( (charCode >= 48 && charCode <= 57)||(charCode==13) );
}

function mouseOverChk(txtdivid,txtboxId)
{	
	for(var i=0;i<length;i++)
	{
		$('#divResult'+txtdivid+i).mouseover({param1: i,param2:txtdivid, param3:txtboxId}, fireMouseOverEvent);
	}
}


function fireMouseOverEvent(event)
{
	for(var i=0;i<length;i++)
	{	
		document.getElementById('divResult'+event.data.param2+i).className='normal';
	}
    document.getElementById('divResult'+event.data.param2+event.data.param1).className='over';	       
   	document.getElementById(event.data.param3).value= $('#divResult'+event.data.param2+event.data.param1).text();
    index=event.data.param1;
}

/*=======  searchTeacher on Press Enter Key ========= */
function chkForEnterSearchTeacher(evt)
{
	var charCode = ( evt.which ) ? evt.which : event.keyCode;	
	if(charCode==13)
	{
		searchCG();
	}	
}
/*========  display grid after Filter ===============*/
function searchCG()
{	
	getCandidateGrid();
	refreshStatus();
	
}
function activecityType(){
	document.getElementById("certType").value='';
}
function showSearchDiv()
{
	document.getElementById("firstName").value=""; 
	document.getElementById("lastName").value="";
	document.getElementById("emailAddress").value="";
	document.getElementById("certType").value="";
	document.getElementById("regionName").value="";
	document.getElementById("degreeName").value="";
	document.getElementById("universityName").value="";
	document.getElementById("normScoreSelectVal").value="5";
	document.getElementById("normScore").value="";
	document.getElementById("CGPA").value="";
	document.getElementById("CGPASelectVal").value="5";
	document.getElementById("contacted").value="0";
	document.getElementById("contactedRd0").checked=true;
	document.getElementById("candidateStatusRd0").checked=true;
	document.getElementById("candidateStatus").value="0";
	document.getElementById("certificateTypeMaster").value="0";
	document.getElementById("regionId").value="0";
	document.getElementById("degreeId").value="0";
	document.getElementById("stateId").value="0";
	document.getElementById("universityId").value="0";
	try{
		document.getElementById("ifrmNorm").src="slideract.do?name=normScoreFrm&tickInterval=10&max=100&swidth=230&svalue=0";
	}catch(e){}	
	try{
		document.getElementById("ifrmCGPA").src="slideract.do?name=CGPAFrm&tickInterval=1&max=5&swidth=230&svalue=0";
	}catch(e){}
	
	//$('#myModalSearch').modal('show');
	try{
		$('#myModalSearch').modal('show');
	}catch(err)
	  {}
	$('#advanceSearchDiv').hide();
	$('#searchLinkDiv').show();
	
}
function hideSearchDiv()
{
	//$('#myModalSearch').modal('hide');
	try{
		$('#myModalSearch').modal('hide');
	}catch(err)
	  {}
}
function checkForCGInt(evt) {

	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==8 || charCode==46)
		return true;

	return( (charCode >= 48 && charCode <= 57)||(charCode==13) );
}
function chkLenOfTextArea(textValue,maxlength)
{
	var tlen=textValue.value.length;
	if(tlen>maxlength)
	{
		document.getElementById(textValue.id).value=document.getElementById(textValue.id).value.substr(0,maxlength)
	}
}
function hideCommunicationsDiv()
{
	//$('#myModalCommunications').modal('hide');
	try{
		$('#myModalCommunications').modal('hide');
	}catch(err)
	  {}
}
function getCommunicationsDiv(commDivFlag,jobForTeacherGId,teacherId,jobId)
{
	
	hideProfilePopover();
	
	document.getElementById("commDivFlag").value=commDivFlag;
	document.getElementById("jobForTeacherGId").value=jobForTeacherGId;
	
	//alert(" commDivFlag :"+commDivFlag+" jobForTeacherGId: "+jobForTeacherGId+" teacherId :"+teacherId+" jobId: "+jobId);
	CandidateGridAjax.getCommunications(commDivFlag,jobForTeacherGId,teacherId,jobId,false,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){
		
		//document.getElementById("divCommTxt").innerHTML=data.toString();
		$("#divCommTxt").html(data);
		
		try{
			$("#saveToFolderDiv").modal("hide");
			$('#myModalCommunications').modal('show');
			
			$("#myModalCommunications").attr("commDivFlag",commDivFlag);
			$("#myModalCommunications").attr("jobForTeacherGId",jobForTeacherGId);
			$("#myModalCommunications").attr("teacherId",teacherId);
			$("#myModalCommunications").attr("jobId",jobId);
				
		}catch(e){}
		$('#commPhone').tooltip();
		$('#commMsg').tooltip();
		$('#commNotes').tooltip();
	}
	});	
}
function printCommunicationslogs()
{
	var commDivFlag = $("#myModalCommunications").attr("commDivFlag");
	var jobForTeacherGId = $("#myModalCommunications").attr("jobForTeacherGId");
	var teacherId = $("#myModalCommunications").attr("teacherId");
	var jobId = $("#myModalCommunications").attr("jobId");
	
	//alert("commDivFlag:- "+commDivFlag+"\njobForTeacherGId:- "+jobForTeacherGId+"\nteacherId:- "+teacherId+"\njobId:- "+jobId);
	CandidateGridAjax.getCommunications(commDivFlag,jobForTeacherGId,teacherId,jobId,true,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){
			
			//alert("Before CommunicationLogData:- "+data);
			var newWindow = window.open();
			newWindow.document.write(data);	
			newWindow.print();
		}
	});
}

function showCommunicationsDiv()
{
	//$('#myModalMessage').modal('hide');
	try{
		$('#myModalMessage').modal('hide');
		$('#myModalNotes').modal('hide');
		$("#saveToFolderDiv").modal("hide");
		$('#myModalCommunications').modal('show');
	}catch(err)
	  {}
	//$('#myModalNotes').modal('hide');
	//$("#saveToFolderDiv").modal("hide");
	//$('#myModalCommunications').modal('show');
}

function showCommunicationsDivForSave()
{
	
	showProfilePopover();
	
	
	//$('#myModalCommunications').modal('hide');
	try{
		$('#myModalCommunications').modal('hide');
	}catch(err)
	  {}
	if(document.getElementById("commDivFlag").value==1){
		//$('#saveToFolderDiv').modal('show');
		try{
			$('#saveToFolderDiv').modal('show');
		}catch(err)
		  {}
	}
}
function showCommunicationsDivForMsg()
{
	//$('#myModalMessage').modal('hide');
	try{
		$('#myModalMessage').modal('hide');
	}catch(err)
	  {}
	var msgType=document.getElementById("msgType").value;
	if(msgType==1){
		//$('#myModalCommunications').modal('show');
		try{
			$('#myModalCommunications').modal('show');
		}catch(err)
		  {}
	}
}
function showCommunicationsForPhone()
{
	var phoneType=document.getElementById("phoneType").value;
	if(phoneType==1){
		//$('#myModalPhone').modal('hide');	
		//$('#myModalCommunications').modal('show');
		try{
			$('#myModalPhone').modal('hide');	
			$('#myModalCommunications').modal('show');
		}catch(err)
		  {}
	}
}
function downloadCommunication(filePath,fileName,linkId)
{
	$('#loadingDiv').show();
	CandidateGridAjax.downloadCommunication(filePath,fileName,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			$('#loadingDiv').hide();
			if(deviceType)
			{
				if (data.indexOf(".doc")!=-1)
				{
					$("#docfileNotOpen").css({"z-index":"3000"});
			    	$('#docfileNotOpen').show();
				}
				else if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					$("#exelfileNotOpen").css({"z-index":"3000"});
			    	$('#exelfileNotOpen').show();
				}
				else
				{
					document.getElementById(linkId).href = data; 			
				}
			}
			else if(deviceTypeAndroid)
			{
				if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					 document.getElementById('ifrmTrans').src = ""+data+"";
				}
				else
				{
					document.getElementById(linkId).href = data; 					
				}				
			}
			else if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
			{
				  
				document.getElementById('ifrmTrans').src = ""+data+"";
			}
			else
			{
				document.getElementById(linkId).href = data; 			
			}		
			return false;			
		}
	});
}
function addNoteFileType()
{
	$('#fileNotes').empty();
	$('#fileNotes').html("<a href='javascript:void(0);' onclick='removeNotesFile();'><img src='images/can-icon.png' title='remove'/></a> <input id='fileNote' name='fileNote' size='20' title='"+resourceJSON.ChooseFile+"' type='file' style='margin-left:20px;margin-top:-15px;'><br>&nbsp;"+resourceJSON.MaxSize10Mb+".");
}
function removeNotesFile()
{
	$('#fileNotes').empty();
	$('#fileNotes').html("<a href='javascript:void(0);' onclick='addNoteFileType();'><img src='images/attach.png'/></a> <a href='javascript:void(0);' onclick='addNoteFileType();'>"+resourceJSON.AttachFile+"</a>");
}
function addPhoneFileType()
{
	$('#filePhones').empty();
	$('#filePhones').html("<a href='javascript:void(0);' onclick='removePhoneFile();'><img src='images/can-icon.png' title='remove'/></a> <input id='filePhone' name='filePhone' size='20' title='"+resourceJSON.ChooseFile+"' type='file' style='margin-left:20px;margin-top:-15px;'><br>&nbsp;"+resourceJSON.MaxSize10Mb+".");
}
function removePhoneFile()
{
	$('#filePhones').empty();
	$('#filePhones').html("<a href='javascript:void(0);' onclick='addPhoneFileType();'><img src='images/attach.png'/></a> <a href='javascript:void(0);' onclick='addPhoneFileType();'>"+resourceJSON.AttachFile+"</a>");
}
function addMessageFileType()
{
	$('#fileMessages').empty();
	$('#fileMessages').html("<a href='javascript:void(0);' onclick='removeMessagesFile();'><img src='images/can-icon.png' title='remove'/></a> <input id='fileMessage' name='fileNote' size='20' title='"+resourceJSON.ChooseFile+"' type='file' style='margin-left:20px;margin-top:-15px;'><br>&nbsp;"+resourceJSON.MaxSize10Mb+".");
}
function removeMessagesFile()
{
	$('#fileMessages').empty();
	$('#fileMessages').html("<a href='javascript:void(0);' onclick='addMessageFileType();'><img src='images/attach.png'/></a> <a href='javascript:void(0);' onclick='addMessageFileType();'>"+resourceJSON.AttachFile+"</a>");
}

function generateCGExcel()
{
	//alert("GenerateCGExcel: "+jobId);	
	$('#loadingDiv').show();
	var jobId=document.getElementById("jobId").value;
	var firstName		=	trim(document.getElementById("firstName").value); 
	var lastName		=	trim(document.getElementById("lastName").value);
	var emailAddress	=	trim(document.getElementById("emailAddress").value);
	var certType		=	trim(document.getElementById("certificateTypeMaster").value);
	var regionId		=	trim(document.getElementById("regionId").value);
	var degreeId		=	trim(document.getElementById("degreeId").value);
	var universityId		=	trim(document.getElementById("universityId").value);
	var normScoreSelectVal		=	trim(document.getElementById("normScoreSelectVal").value);
	var normScore		=	trim(document.getElementById("normScore").value);
	var CGPA		=	trim(document.getElementById("CGPA").value);
	var CGPASelectVal		=	trim(document.getElementById("CGPASelectVal").value);
	var contacted		=	trim(document.getElementById("contacted").value);
	var candidateStatus		=	trim(document.getElementById("candidateStatus").value);
	var stateId=document.getElementById("stateId").value;
	CandidateGridAjax.generateCGExcel(jobId,firstName,lastName,emailAddress,certType,regionId,degreeId,universityId,normScoreSelectVal,
			normScore,CGPA,CGPASelectVal,contacted,candidateStatus,stateId,orderColumn, sortingOrder,totalNoOfRecord,false,null,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{		
		$('#loadingDiv').hide();
		if(deviceType)
		{
			if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
			{
				$("#exelfileNotOpen").css({"z-index":"3000"});
		    	$('#exelfileNotOpen').show();
			}		
		}	
		else
		{
			document.getElementById('ifrmTrans').src = "candidate/"+data+"";	
		}	
		}});

}
function generateCGExcelForTeachers()
{
	//alert("GenerateCGExcel: "+jobId);
	
	$('#loadingDiv').show();
	var jobId=document.getElementById("jobId").value;
	var firstName		=	""; 
	var lastName		=	"";
	var emailAddress	=	"";
	var certType		=	"";
	var regionId		=	"";
	var degreeId		=	"";
	var universityId		=	"";
	var normScoreSelectVal		=	"";
	var normScore		=	"";
	var CGPA		=	"";
	var CGPASelectVal		=	"";
	var contacted		=	"";
	var candidateStatus		=	"";
	var stateId="0";
	
	var savecandidatearray="";
	 
	var inputs = document.getElementsByName("cgCBX"); 
	 for (var i = 0; i < inputs.length; i++) {
	        if (inputs[i].type === 'checkbox') {
	        	if(inputs[i].checked){
	        		//alert("LLL "+inputs[i].id);
	        		var hid=inputs[i].id;
	        		var cc =hid.replace("cgCBX","");
	        		var secType=document.getElementById("sel"+inputs[i].value).value;
	        		
	        		savecandidatearray+=secType+inputs[i].value+",";
	            }
	        }
	 }
	 
	 CandidateGridAjax.generateCGExcel(jobId,firstName,lastName,emailAddress,certType,regionId,degreeId,universityId,normScoreSelectVal,
			normScore,CGPA,CGPASelectVal,contacted,candidateStatus,stateId,orderColumn, sortingOrder,totalNoOfRecord,true,savecandidatearray,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
		//alert(data);
		$('#loadingDiv').hide();
		document.getElementById('ifrmTrans').src = "candidate/"+data+"";
		}});
	// $('#loadingDiv').hide();
}
function generateCGPDFForTeachers()
{
	
	$('#loadingDiv').show();
	var jobId=document.getElementById("jobId").value;
	var firstName		=	""; 
	var lastName		=	"";
	var emailAddress	=	"";
	var certType		=	"";
	var regionId		=	"";
	var degreeId		=	"";
	var universityId		=	"";
	var normScoreSelectVal		=	"";
	var normScore		=	"";
	var CGPA		=	"";
	var CGPASelectVal		=	"";
	var contacted		=	"";
	var candidateStatus		=	"";
	var stateId="0";
	
	var savecandidatearray="";
	 
	var inputs = document.getElementsByName("cgCBX"); 
	 for (var i = 0; i < inputs.length; i++) {
	        if (inputs[i].type === 'checkbox') {
	        	if(inputs[i].checked){
	        		//alert("LLL "+inputs[i].id);
	        		var hid=inputs[i].id;
	        		var cc =hid.replace("cgCBX","");
	        		var secType=document.getElementById("sel"+inputs[i].value).value;
	        		
	        		savecandidatearray+=secType+inputs[i].value+",";
	            }
	        }
	 }
	 
	 
	 CandidateGridAjax.downloadCandidateGridReport(jobId,firstName,lastName,emailAddress,certType,regionId,degreeId,universityId,normScoreSelectVal,
				normScore,CGPA,CGPASelectVal,contacted,candidateStatus,stateId,orderColumn, sortingOrder,totalNoOfRecord,true,savecandidatearray,{
				async: true,
					errorHandler:handleError,
					callback:function(data)
					{	
						$('#loadingDiv').hide();
						//window.open(data);
						//openWindow(data,'Support',650,490);
						
						$('#loadingDiv').hide();
						if(deviceType)
						{
							if (data.indexOf(".doc")!=-1)
							{
								$("#docfileNotOpen").css({"z-index":"3000"});
						    	$('#docfileNotOpen').show();
							}
							else if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
							{
								$("#exelfileNotOpen").css({"z-index":"3000"});
						    	$('#exelfileNotOpen').show();
							}
							else
							{
								$('#modalDownloadCGR').modal('hide');
								document.getElementById(hrefId).href=data;				
							}
						}
						else if(deviceTypeAndroid)
						{
							if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
							{
								  try{
								    $('#modalDownloadCGR').modal('hide');
									}catch(err)
									  {}
								    document.getElementById('ifrmCGR').src = ""+data+"";
							}
							else
							{
								$('#modalDownloadCGR').modal('hide');
								document.getElementById(hrefId).href=data;					
							}				
						}
						else if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
						{
							  try{
							    	$('#modalDownloadCGR').modal('hide');
								}catch(err)
								  {}
							    document.getElementById('ifrmCGR').src = ""+data+"";
						}
						else
						{
							$('#modalDownloadCGR').modal('hide');
							document.getElementById('ifrmCGR').src = ""+data+"";
							try{
								$('#modalDownloadCGR').modal('show');
							}catch(err)
							  {}
							
						}		
						
						return false;
					}});
}
//start from by ramesh

function showProfileContentClose(){	
	if(deviceType || deviceTypeAndroid)
	{
		$('#cgTeacherDivMaster').modal('hide'); 
		$('#cgTeacherDivMaster').hide();  
		document.getElementById("teacherIdForprofileGrid").value="";
		currentPageFlag="";	
	}
	else
	{
		$('#cgTeacherDivMaster').modal('hide');  
		document.getElementById("teacherIdForprofileGrid").value="";
		currentPageFlag="";
	}
	
}

function showProfileContent(dis,jobforteacherId,teacherId,normscore,jobId,colorName,noOfRecordCheck,event)
{
	var scrollTopPosition = $(window).scrollTop();
	var x=event.clientX;
	var y=event.clientY;
	var z=y-180+scrollTopPosition;
	z=z+80;	
	var style = $('<style>.popover { position: absolute;top: '+z+'px !important;left: 0px;width: 770px; }</style>')
	$('html > head').append(style);	
	showProfileContentClose();
	$('#loadingDiv').show();	
	document.getElementById("teacherIdForprofileGrid").value=teacherId;		
	TeacherProfileViewInDivAjax.showProfileContent(jobforteacherId,teacherId,normscore,jobId,colorName,noOfRecordCheck,0,{ 

	async: true,
	callback: function(data)
	{  
		//$("#cgTeacherDivMaster").modal('show');
		
			$("#cgTeacherDivMaster").modal('show');
			//var wait="<div id='removeWait' style='text-align:center;padding-top:0px;'><img src='images/loadingAnimation.gif' /> <br>Loading...</div>";
			$("#cgTeacherDivBody").html(data);
	         
			TeacherProfileViewInDivAjax.showProfileContent(jobforteacherId,teacherId,normscore,jobId,colorName,noOfRecordCheck,1,{ 
				async: true,
				callback: function(redata){
				setTimeout(function () {
					$("#profile_id").after(redata);
					$('#removeWait').remove();
		        }, 100);
			}
			});
		
		
		//$("#cgTeacherDivBody").html(data);		
		$('#loadingDiv').hide();
		  $('#tpResumeprofile').tooltip();
		  $('#tpPhoneprofile').tooltip();
		  $('#tpPDReportprofile').tooltip();
		  $('#tpScoreReportGE').tooltip();
		  $('#tpScoreReportSA').tooltip();
		  $('#tpJSAProfile').tooltip();
		  $('#tpTeacherProfileVisitHistory').tooltip();
		  $('textarea').jqte();
	},
	errorHandler:handleError  
	});
} 

function getPhoneDetailShow(teacherId){
	hideProfilePopover();
	
	TeacherProfileViewInDivAjax.getPhoneDetailByDiv(teacherId,
	{
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			if(data==""){
				document.getElementById("divAlertText").innerHTML=""+resourceJSON.MsgPhNoIsNotAvailable+"";
				try{$('#divAlert').modal('show');}catch(err){}
			}
			else{
				document.getElementById("divPhoneByPro").innerHTML=data;			
				try{
					$('#myModalPhoneShow').modal('show');
				}catch(err){}
			}
			
		}
	});
}


function getPFEmploymentGrid_DivProfile(teacherId)
{
	var visitLocation=document.getElementById("teacherIdForprofileGridVisitLocation").value;
	
	TeacherProfileViewInDivAjax.getGridDataWorkExpEmployment(teacherId,noOfRowsworkExp,pageworkExp,sortOrderStrworkExp,sortOrderTypeworkExp,visitLocation, { 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#gridDataWorkExpEmployment').html(data);
			if(visitLocation=="CG View"){
				applyScrollOnTblWorkExp();
			}

		}});
}


function getElectronicReferencesGrid_DivProfile(teacherId)
{
	var districtMaster = null;
	var schoolMaster = null;
	
	var visitLocation=document.getElementById("teacherIdForprofileGridVisitLocation").value;
	var jobId=document.getElementById("jobId").value;
	
	TeacherProfileViewInDivAjax.getElectronicReferencesGrid(teacherId,noOfRowsReferences,pageReferences,sortOrderStrReferences,sortOrderTypeReferences,visitLocation,jobId,districtMaster,schoolMaster, { 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
		$('#gridDataReference').html(data);
		if(visitLocation=="CG View"){
			applyScrollOnTblEleRef_profile();
		}

		}});
}


function getVideoLinksGrid_DivProfile(teacherId)
{
	var visitLocation=document.getElementById("teacherIdForprofileGridVisitLocation").value;	
	TeacherProfileViewInDivAjax.getVideoLinksGrid(teacherId,noOfRowsvideoLink,pagevideoLink,sortOrderStrvideoLink,sortOrderTypevideoLink,visitLocation, { 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#gridDataVideoLink').html(data);
			if(visitLocation=="CG View"){
				applyScrollOnTblVideoLinks_profile();
			}

		}});
}

//For language profiency
function getlanguage_DivProfile(teacherId)
{
	var visitLocation=document.getElementById("teacherIdForprofileGridVisitLocation").value;
	TeacherProfileViewInDivAjax.displayTeacherLanguageByTeacherId(noOfRowsLang,pageLang,sortOrderStrLang,sortOrderTypeLang,teacherId,visitLocation, { 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#divGridlanguageProfiency').html(data);
			applyScrollOnTblLanguage();

		}});
}
function getadddocumentGrid_DivProfile(teacherId)
{
	//alert(noOfRowsvideoLink+":::::::::::::"+pagevideoLink+"::"+sortOrderStrvideoLink+"::"+sortOrderTypevideoLink+"::"+teacherId);
	var visitLocation=document.getElementById("teacherIdForprofileGridVisitLocation").value;	
	TeacherProfileViewInDivAjax.getAdditionalDocumentsGrid(noOfRowsaddDoc,pageaddDoc,sortOrderStraddDoc,sortOrderTypeaddDoc,teacherId,visitLocation, { 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#divGridAdditionalDocuments').html(data);
			if(visitLocation=="CG View"){
				applyScrollOnTbl_AdditionalDocuments();
			}
		}});
}
function setGridProfileVariable(currentPageFlagValue){
	currentPageFlag=currentPageFlagValue;
}

function getTeacherProfileVisitHistoryShow_FTime(teacherId){
	
	
	var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
	currentPageFlag="tpvh";
	getPagingAndSorting('1',"visitedDateTime",1);
	
	hideProfilePopover();
}

function getTeacherProfileVisitHistoryShow(teacherId){
	
	$('#loadingDiv').show();
	
	currentPageFlag="tpvh";
	
	var visitLocation=document.getElementById("teacherIdForprofileGridVisitLocation").value;
	
	TeacherProfileViewInDivAjax.getGridDataForTeacherProfileVisitHistoryByTeacher(teacherId,noOfRowstpvh,pagetpvh,sortOrderStrtpvh,sortOrderTypetpvh,visitLocation, { 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#loadingDiv').hide();
			try{
				$('#divteacherprofilevisithistory').html(data);
				if(visitLocation=="CG View"){
					applyScrollOnTblProfileVisitHistory();
				}
				$('#myModalProfileVisitHistoryShow').modal('show');
			}catch(e){}
		}});
}

function getTeacherAcademicsGrid_DivProfile(teacherId)
{
	var visitLocation=document.getElementById("teacherIdForprofileGridVisitLocation").value;
	TeacherProfileViewInDivAjax.getTeacherAcademicsGrid(teacherId,noOfRowsteacherAce,pageteacherAce,sortOrderStrteacherAce,sortOrderTypeteacherAce,visitLocation, { 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#gridDataTeacherAcademics').html(data);
			if(visitLocation=="CG View"){
				applyScrollOnTblTeacherAcademics_profile();
			}
		}});
}

function getDistrictSpecificQuestionData(districtId,teacherId)
{	
	dId=districtId;
	tId=teacherId;
	CandidateGridAjax.getDistrictSpecificQuestionData(districtId,teacherId,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{	
			$('#getDistrictSpecificQuestion').html(data);
		}
	});
}



function getTeacherCertificationsGrid_DivProfile(teacherId)
{
	var visitLocation=document.getElementById("teacherIdForprofileGridVisitLocation").value;
	TeacherProfileViewInDivAjax.getTeacherCertificationsGrid(teacherId,noOfRowsteacherCerti,pageteacherCerti,sortOrderStrteacherCerti,sortOrderTypeteacherCerti,visitLocation, { 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#gridDataTeacherCertifications').html(data);
			if(visitLocation=="CG View"){
				applyScrollOnTblTeacherCertifications_profile();
			}
		}});
}


var dId=0;
var tId=0;
function printQualificationQuestion()
{	
	if(dId!=0 && tId!=0){
		CandidateGridAjax.getDistrictSpecificQuestionData(dId,tId,{ 
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{	
				var teacherEmailAddress=$("#teacherEmailAddress").val();
				var statusNote=$("#statusNoteID").val();
				document.write("<html><body><table>");
				document.write("<tr><td><b>E-mail:&nbsp;&nbsp;</b><a href='mailTo:"+teacherEmailAddress+"'>"+teacherEmailAddress+"</a></td></tr>");
				document.write("<tr></tr>");
				
				if(statusNote!=""){
					document.write("<tr><td><b>Notes:&nbsp;</b>"+statusNote+"</td></tr>");
					document.write("<tr></tr>");
				}
				
				document.write("<tr>");
				document.write("<td>"+data+"</td>");
				document.write("</tr>");
				document.write("</table></body></html>");
				window.print();
			}
		});
	}
}

function getDistrictSpecificQuestion_DivProfile(jobId,teacherId,sVisitLocation)
{	
	TeacherProfileViewInDivAjax.getDistrictSpecificQuestionGrid(jobId,teacherId,sVisitLocation,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{	
			$('#getDistrictSpecificQuestion').html(data);
		}
	});
}







function getDistrictSpecificQuestion_DivProfile_FromMyFolders(districtId,teacherId,sVisitLocation)
{	
	if(sVisitLocation==null)sVisitLocation="";
	TeacherProfileViewInDivAjax.getDistrictSpecificQuestionGrid_ByDistrict(districtId,teacherId,sVisitLocation,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#getDistrictSpecificQuestion').html(data);
		}});
}

function getQuestionExplain(questionID)
{
	TeacherProfileViewInDivAjax.getQAExplaination(questionID,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){
		$("#divExplain").html(data);
		try{
				$('#myModalQAEXEditor').show();
				
				$("#draggableDivMaster").hide();	
				$("#cgTeacherDivMaster").hide();
			}catch(e){
				//alert(e);
			}
		}
		});
}

function showPreviousModel()
{
	$("#draggableDivMaster").show();
	$("#cgTeacherDivMaster").show();
	$('#myModalQAEXEditor').hide();
}

function updateAScore(teacherId,districtId){
	
	var slider_aca_ach=0;
	var slider_lea_res=0;
	try{
		var ifrm_aca_ach = document.getElementById('ifrm_aca_ach');
		var innerNorm = ifrm_aca_ach.contentDocument || ifrm_aca_ach.contentWindow.document;
		if(innerNorm.getElementById('slider_aca_ach').value!=''){
			//$('#candidateFeedNormScore').val(innerNorm.getElementById('slider_aca_ach').value);
			slider_aca_ach=innerNorm.getElementById('slider_aca_ach').value;
		}
		}catch(e){}
		
		
	try{
		var ifrm_lea_res = document.getElementById('ifrm_lea_res');
		var innerNorm1 = ifrm_lea_res.contentDocument || ifrm_lea_res.contentWindow.document;
		if(innerNorm1.getElementById('slider_lea_res').value!=''){
			//$('#candidateFeedNormScore').val(innerNorm.getElementById('slider_aca_ach').value);
			slider_lea_res=innerNorm1.getElementById('slider_lea_res').value;
		}
		}catch(e){}
	
		var txt_sl_slider_aca_ach = document.getElementById('txt_sl_slider_aca_ach').value;
		var txt_sl_slider_lea_res = document.getElementById('txt_sl_slider_lea_res').value;
		
		//if((slider_aca_ach!=0 || slider_lea_res!=0) && ( txt_sl_slider_aca_ach!=slider_aca_ach || txt_sl_slider_lea_res!=slider_lea_res)){
		if(txt_sl_slider_aca_ach!=slider_aca_ach || txt_sl_slider_lea_res!=slider_lea_res){
			TeacherProfileViewInDivAjax.getUpdateAndSaveAScore(teacherId,districtId,slider_aca_ach,slider_lea_res,{ 
				async: true,
				errorHandler:handleError,
				callback:function(data)
				{
					if(data!="")
					{
						document.getElementById('txt_sl_slider_aca_ach').value=slider_aca_ach;
						document.getElementById('txt_sl_slider_lea_res').value=slider_lea_res;
						$('#ascore_profile').html(data[0]);
						$('#lrscore_profile').html(data[1]);
						$("#tdCGViewAScore_"+teacherId).html(data[0]);
						$("#tdCGViewlrScore_"+teacherId).html(data[1]);
					
					}
					
				}});
		}
}


function showProfileContentClose_ShareFolder(){
	defaultSaveDiv();
    //$('.profile').popover('destroy');
	document.getElementById("teacherIdForprofileGrid").value="";
    
}

function showProfileContentForTeacher(dis,teacherId,noOfRecordCheck,sVisitLocation,event)
{	
	// by alok for dynamic div width
	   try
	   {
		   var mydiv = document.getElementById("mydiv");
	       var curr_width = parseInt(mydiv.style.width);
	      
	       if(deviceType)
	       {
	    	   mydiv.style.width = 630+"px";  
	       }
	       else
		   {
	    	   mydiv.style.width = 647+"px";  
		   }      
	   }
	   catch(e){alert(e)}
	   
	var districtMaster = null;
	showProfileContentClose_ShareFolder();
	$('#loadingDiv').show();
	document.getElementById("teacherIdForprofileGrid").value=teacherId;
	TeacherProfileViewInDivAjax.showProfileContentForTeacher(teacherId,noOfRecordCheck,sVisitLocation,districtMaster,0,{ 
	async: true,
	callback: function(data)
	{
	  $('#loadingDiv').hide();
	  $("#draggableDivMaster").modal('show');
	  //var wait="<div id='removeWait' style='text-align:center;padding-top:0px;'><img src='images/loadingAnimation.gif' /> <br>Loading...</div>";
		$("#draggableDiv").html(data);
       
		TeacherProfileViewInDivAjax.showProfileContentForTeacher(teacherId,noOfRecordCheck,sVisitLocation,districtMaster,1,{ 
			async: true,
			callback: function(resdata){
			setTimeout(function () {
				$("#profile_id").after(resdata);
				$('#removeWait').remove();
	        }, 100);
			
			
		}
		});
	  
	  //$("#draggableDiv").html(data);
	  $('#tpResumeprofile').tooltip();
	  $('#tpPhoneprofile').tooltip();
	  $('#tpPDReportprofile').tooltip();
	  $('#tpTeacherProfileVisitHistory').tooltip();
	  $('#teacherProfileTooltip').tooltip();
	  $('textarea').jqte();
	},
	errorHandler:handleError  
	});
} 

function showProfilePopover()
{
		//$("#cgTeacherDivMaster").modal('show');
		try
		{
			$("#draggableDivMaster").modal('show');
			$("#cgTeacherDivMaster").modal('show');
		}
		catch(e){}
		
		//$('#myModalJobList').hide(); // @AShish :: Hide for Job Details(myModalJobList) Div
		//$('#myModalReferenceNoteView').hide();
		
		//Refresh for Reference Notes
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getElectronicReferencesGrid_DivProfile(teacherIdForprofileGrid);
		$('#tpResumeprofile').tooltip();
		$('#tpPhoneprofile').tooltip();
		$('#tpPDReportprofile').tooltip();
		$('#tpJSAProfile').tooltip();
		$('#tpTeacherProfileVisitHistory').tooltip();
}
function hideProfilePopover()
{
	//$("#cgTeacherDivMaster").modal('hide');
	
	try
	{
		$("#cgTeacherDivMaster").modal('hide');
		$("#draggableDivMaster").modal('hide');	
	}
	catch(e){}
}


function closeRefNotesDivEditor()
{
	var eleRefId=document.getElementById("eleRefId").value;
	getRefNotesDiv(eleRefId);
}

function getRefNotesDiv(eleRefId)
{
	var districtMaster = null;
	var schoolMaster = null;
	
	
	hideProfilePopover();
	
	document.getElementById("eleRefId").value=eleRefId;
	
	//$('#myModalReferenceNotesEditor').modal('hide');
	try{
		$('#myModalReferenceNotesEditor').modal('hide');
		$('#myModalReferenceNotesEditor').modal('hide');
	}catch(err)
	  {}
	//$('#myModalReferenceNotesEditor').modal('hide');
	
	var jobId = document.getElementById("jobId").value;
	TeacherProfileViewInDivAjax.getRefeNotes(eleRefId,jobId,districtMaster,schoolMaster,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){
		
		$("#divRefNotesInner").html(data);
		$('#myModalReferenceNoteView').modal('show');
		//$('#myModalReferenceNoteView').show();
		$('#refeNotesId').tooltip();
	}
	});
}

function getNotesEditorDiv()
{
	//$('#myModalReferenceNoteView').modal('hide');
	try{
		$('#myModalReferenceNoteView').modal('hide');
	}catch(err)
	  {}
	
	$('#myModalReferenceNoteView').modal('hide');
	//$('#myModalReferenceNoteView').hide();
	removeRefeNotesFile();
	$('#errordivNotes_ref').empty();
	$('#divTxtNode_ref').find(".jqte_editor").html("");	
	$('#divTxtNode_ref').find(".jqte_editor").css("background-color", "");
	//$('#myModalReferenceNotesEditor').modal('show');
	try{
		$('#myModalReferenceNotesEditor').modal('show');
	}catch(err)
	  {}
	
}


function saveReferenceNotes(){
	var noteFileName="";
	var notes = $('#divTxtNode_ref').find(".jqte_editor").html();
	var teacherId = document.getElementById("teacherIdForprofileGrid").value;
	var eleRefId = document.getElementById("eleRefId").value;
	var jobId = document.getElementById("jobId").value;
	
	var fileNote=null;
	
	try{
		fileNote=document.getElementById("fileNote_ref").value;
	}catch(e){}
	
	var cnt=0;
	var focs=0;
	
	$('#errordivNotes_ref').empty();
	
	if ($('#divTxtNode_ref').find(".jqte_editor").text().trim()=="")
	{
		$('#errordivNotes_ref').append("&#149; "+resourceJSON.PlzEtrNotes+".<br>");
		if(focs==0)
			$('#divTxtNode_ref').find(".jqte_editor").focus();
		$('#divTxtNode_ref').find(".jqte_editor").css("background-color", "#F5E7E1");
		cnt++;
	}else if(fileNote==""){
		$('#errordivNotes_ref').show();
		$('#errordivNotes_ref').append("&#149; "+resourceJSON.PlzUpldNotes+".<br>");
		cnt++;
	}else if(fileNote!="" && fileNote!=null)
	{
		var ext = fileNote.substr(fileNote.lastIndexOf('.') + 1).toLowerCase();	
		noteFileName="ref_note"+noteDateTime+"."+ext;
		var fileSize = 0;
		if ($.browser.msie==true)
	 	{	
		    fileSize = 0;	   
		}
		else
		{
			if(document.getElementById("fileNote_ref").files[0]!=undefined)
			{
				fileSize = document.getElementById("fileNote_ref").files[0].size;
			}
		}
		
		if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt' || ext=='xlsx' || ext=='xls'))
		{
			$('#errordivNotes_ref').show();
			$('#errordivNotes_ref').append("&#149; "+resourceJSON.PlzSelectNoteFormat+".<br>");
			cnt++;
		}
		else if(fileSize>=10485760)
		{
			$('#errordivNotes_ref').show();
			$('#errordivNotes_ref').append("&#149; "+resourceJSON.FileSizeLessthan10Mb+".<br>");
			cnt++;
		}
	}
	if(cnt==0){	
		try{
			$('#loadingDiv').show();
			if(fileNote!="" && fileNote!=null)
			{
				document.getElementById("frmNoteUpload_ref").submit();
			}
			else
			{
				TeacherProfileViewInDivAjax.saveReferenceNotesByAjax(eleRefId,teacherId,jobId,notes,noteFileName,
				{
					async:false,
					errorHandler:handleError,
					callback:function(data)
					{
						$('#loadingDiv').hide();
						getRefNotesDiv(eleRefId);
					}
				});	
			}
		}catch(err){}
	}

	
}

function saveRefNoteFile(noteDateTime)
{	
	var notes = $('#divTxtNode_ref').find(".jqte_editor").html();
	var teacherId = document.getElementById("teacherIdForprofileGrid").value;
	var eleRefId = document.getElementById("eleRefId").value;
	var jobId = document.getElementById("jobId").value;
	
	var fileNote=null;
	try{
		fileNote=document.getElementById("fileNote_ref").value;
	}catch(e){}
	
	var noteFileName="";
	
	if(fileNote!="" && fileNote!=null)
	{
		var ext = fileNote.substr(fileNote.lastIndexOf('.') + 1).toLowerCase();	
		noteFileName="ref_note"+noteDateTime+"."+ext;
	}
	
	TeacherProfileViewInDivAjax.saveReferenceNotesByAjax(eleRefId,teacherId,jobId,notes,noteFileName,
			{
				async:false,
				errorHandler:handleError,
				callback:function(data)
				{
					$('#loadingDiv').hide();
					getRefNotesDiv(eleRefId);
				}
			});	
}

function addRefeNoteFileType()
{
	$('#fileRefNotes').empty();
	$('#fileRefNotes').html("<a href='javascript:void(0);' onclick='removeRefeNotesFile();'><img src='images/can-icon.png' title='remove'/></a> <input id='fileNote_ref' name='fileNote_ref' size='20' style='margin-left:20px;margin-top:-15px;' title='"+resourceJSON.ChooseFile+"' type='file'><br>&nbsp;"+resourceJSON.MaxSize10Mb+".");
}

function removeRefeNotesFile()
{
	$('#fileRefNotes').empty();
	$('#fileRefNotes').html("<a href='javascript:void(0);' onclick='addRefeNoteFileType();'><img src='images/attach.png'/></a> <a href='javascript:void(0);' onclick='addRefeNoteFileType();'>"+resourceJSON.AttachFile+"</a>");
}

function downloadReferenceNotes(filePath,fileName,linkId)
{
	$('#loadingDiv').show();
	TeacherProfileViewInDivAjax.downloadReferenceNotes(filePath,fileName,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			$('#loadingDiv').hide();
			if(deviceType)
			{
				if (data.indexOf(".doc")!=-1)
				{
					$("#docfileNotOpen").css({"z-index":"3000"});
			    	$('#docfileNotOpen').show();
				}
				else if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					$("#exelfileNotOpen").css({"z-index":"3000"});
			    	$('#exelfileNotOpen').show();
				}
				else
				{
					document.getElementById(linkId).href = data; 
				}
			}
			else if(deviceTypeAndroid)
			{
				if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					document.getElementById('ifrmTrans').src = ""+data+"";
				}
				else
				{
					document.getElementById(linkId).href = data; 	
				}	
			}
			else if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
			{
				document.getElementById('ifrmTrans').src = ""+data+"";
			}
			else
			{
				document.getElementById(linkId).href = data; 	
			}			 
			return false;		
		}
	});
}
function setPageFlag()
{	
	$('#myModalJobList').hide();
	var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
	if(teacherIdForprofileGrid!='')
	{
		showProfilePopover();
	}
}

function getPhoneDetailShowPro(teacherId){
	hideProfilePopover();
	TeacherProfileViewInDivAjax.getPhoneDetailByDiv(teacherId,
	{
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			if(data==""){
				document.getElementById("divAlertText").innerHTML="No Phone number is available for this candidate.";
				try{$('#divAlert').modal('show');
								}catch(err){}
			}
			else{
				document.getElementById("divPhoneByPro").innerHTML=data;			
				try{$('#myModalPhoneShowPro').modal('show');
								}catch(err){}
			}
			
		}
	});
}



/**************************** Send Message ****************/

function sendMessage()
{
	var msgType	=	2;
	var jobForTeacherIds = new Array();
	jobForTeacherIds	=	getSendMessageTeacherIds();
	var jobId	=	$("#jobId").val();
	var emailId	=	"";
	getMessageDiv(jobForTeacherIds,emailId,jobId,msgType)
	
	TeacherSendMessageAjax.getEmailAddress(jobForTeacherIds,
	{
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			document.getElementById("emailDiv").innerHTML=data;
		}
	});
	
	TeacherSendMessageAjax.getTeacherIds(jobForTeacherIds,
	{
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			document.getElementById("teacherIdForMessage").value=data;	
			//document.getElementById("emailDiv").innerHTML=data;
		}
	});
	
}



function getSendMessageTeacherIds()
{
	var jobForTeacherIds = new Array();
	var inputs = document.getElementsByName("cgCBX"); 
	for (var i = 0; i < inputs.length; i++) 
	{
		if (inputs[i].type === 'checkbox') 
		{
			if(inputs[i].checked)
			{
				jobForTeacherIds[i]=inputs[i].value;
	        }
	    }
	} 
	return jobForTeacherIds;
}

function confMessageForSendMessage()
{	
	$('#lodingImage').empty();
	//$('#myModalMessage').modal('hide');
	//$('#myModalMsgShow_SendMessage').modal('show');
	try{
		$('#myModalMessage').modal('hide');
		$('#myModalMsgShow_SendMessage').modal('show');
	}catch(err)
	  {}
	//document.getElementById("message2show_SendMessage").innerHTML="Your message is successfully sent to the Candidate.";
}

/* @Start
 * @Ashish Kumar
 * @Description :: Show Qualification details Div and save Qualification Note
 * */

//Show Qualification Details Div
function showQualificationDiv(jobForTeacherId,teacherId,districtId)
{
	    CandidateGridAjax.getQualificationDetails(jobForTeacherId,teacherId,districtId,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#qualificationDiv').modal('show');
			$('#qualificationDivBody').html(data);
			try{
			if(document.getElementById("userType").value==2)
			{
				if(document.getElementById("flagForDistrictSpecificQuestions").value=="null" || document.getElementById("flagForDistrictSpecificQuestions").value=="false")
				{
					if(document.getElementById("flagForFinalize").value=="null" || document.getElementById("flagForFinalize").value=="false")
					{
						$("#qStatusSave").show();
						$("#qStatusFinalize").show();
					}
					else
					{
						$("#qStatusSave").hide();
						$("#qStatusFinalize").hide();
					}
					
				}else if(document.getElementById("flagForDistrictSpecificQuestions").value=="true"){
					$("#qStatusSave").hide();
					$("#qStatusFinalize").hide();
				}
			}
			else
			{
				$("#qStatusSave").hide();
				$("#qStatusFinalize").hide();
			}
			}catch(err){}
		}
	});
}
	// show qualification Div In TeacherPool
	function showQualificationDivForTP(teacherId,districtId)
	{		   
		    CandidateGridAjax.getQualificationDetailsForTP(teacherId,districtId,{ 
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{
		    	$('#qualificationDiv').modal('show');
				$('#qualificationDivBody').html(data);
				try{
				if(document.getElementById("userType").value==2)
				{
					if(document.getElementById("flagForDistrictSpecificQuestions").value=="null" || document.getElementById("flagForDistrictSpecificQuestions").value=="false")
					{
						if(document.getElementById("flagForFinalize").value=="null" || document.getElementById("flagForFinalize").value=="false")
						{
							$("#qStatusSave").show();
							$("#qStatusFinalize").show();
						}
						else
						{
							$("#qStatusSave").hide();
							$("#qStatusFinalize").hide();
						}
						
					}else if(document.getElementById("flagForDistrictSpecificQuestions").value=="true"){
						$("#qStatusSave").hide();
						$("#qStatusFinalize").hide();
					}
				}
				else
				{
					$("#qStatusSave").hide();
					$("#qStatusFinalize").hide();
				}
				}catch(err){}
			}
		});
	}
	//save qualification Note
	function saveQualificationNote(saveStatus)
	{
		var teacherId = document.getElementById("teacherId").value;
		var districtId = document.getElementById("districtId").value;
		var note = $("#statusNoteID").val();
		$('#errorQStatus').empty();
		if(note==''){
			$('#errorQStatus').show();
			$('#errorQStatus').append("&#149; "+resourceJSON.PlzEtrNotes+".<br>");
		}else{
			CandidateGridAjax.saveQualificationNote(teacherId,districtId,note,saveStatus,{ 
				async: true,
				errorHandler:handleError,
				callback:function(data)
				{
					$('#qualificationDiv').modal('hide');
					getCandidateGrid();
					refreshStatus();
				}
			});
		}
	}
	
	// Save Qualification Note For Teacher Pool
	function saveQualificationNoteForTP(saveStatus)
	{
		var teacherId = document.getElementById("teacherId").value;
		var districtId = document.getElementById("districtId").value;
		var note = $("#statusNoteID").val();		
		$('#errorQStatus').empty();	
		if(note==''){
			$('#errorQStatus').show();
			$('#errorQStatus').append("&#149; "+resourceJSON.PlzEtrNotes+".<br>");
		}
		else
		{
			//alert(teacherId+"........"+districtId+"..........."+note+"...."+saveStatus);
			CandidateGridAjax.saveQualificationNote(teacherId,districtId, note,saveStatus,{ 
				async: true,
				errorHandler:handleError,
				callback:function(data)
				{
				    //alert(data);
					$('#qualificationDiv').modal('hide');
					applyScrollOnJobsTbl();
					displayTeacherGrid();
				}
			});
		}
	}
	function cancelQualificationIssuse()
	{
		$('#errorQStatus').empty();
	}
	
	//For Print Data(QQ, Cover Letter, Portfolio)
	/*function showPrintDataDiv(jobForTeacherId,teacherId,districtId)
	{
		CandidateGridAjax.getPrintData(jobForTeacherId,teacherId,districtId,{ 
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{
				try{
					$('#printDataProfile').modal('show');
					$('#printDataProfileDiv').html(data);
				}catch(err){}
			}
		});
	}*/
	
	
	// Get Qualification Questions for Print 
	/*function getDistrictSpecificQuestionDataForPrint(districtId,teacherId,count,flag)
	{	
		dId=districtId;
		tId=teacherId;
		if(flag==0)
		{
			CandidateGridAjax.getDistrictSpecificQuestionData(districtId,teacherId,{ 
				async: true,
				errorHandler:handleError,
				callback:function(data)
				{	
					$('#getDistrictSpecificQuestionForPrint'+count).html(data);
				}
			});
		}
		else if(flag==1)
		{
			CandidateGridAjax.getDistrictSpecificQuestionDataForPrint(districtId,teacherId,{ 
				async: true,
				errorHandler:handleError,
				callback:function(data)
				{	
					$('#getDistrictSpecificQuestionForPrint'+count).html(data);
				}
			});
		}
	}*/
	
	function canelPrintData(){
		$('#printDataProfile').modal('hide');
		$("#portfolioItmDiv").hide();
		var elements = document.getElementsByName("chkportfolioItem");
		 for (i=0;i<elements.length;i++) 
		    elements[i].checked = false;
		 
		//var siteURL=this.location.href;
		//window.open(siteURL, '_blank'); // <- This is what makes it open in a new window.);
	}
	
	// Print All Data FOR pdf
	function printAllData()
	{	
		//var dataNotFoundFlag = document.getElementById("dataNotFoundFlag").value;
		$('#loadingDiv').fadeIn();
		var inputs = document.getElementById("jftIds").value;
		var chkcl = document.getElementById("chkcl").value;
		var chkqq = document.getElementById("chkqq").value;
		var chkportfolio=document.getElementById("chkportfolio").value;	
		
		var chkjsi=document.getElementById("chkjsi").value;	
		
		
		var pointscheck = new Array();
			
			if (document.getElementById("academics").checked==true)
			{
		      pointscheck.push("acd");
			}	
			
			if (document.getElementById("certification").checked==true) 
			{	
				pointscheck.push("cert");
			}
			
			if (document.getElementById("references").checked==true) 
			{
				
				pointscheck.push("ref");
			}
			
			if (document.getElementById("experience").checked==true) 
			{
				pointscheck.push("exp");
			}
			
			if (document.getElementById("involvement").checked==true) 
			{
				
				pointscheck.push("invol");
			}
			if (document.getElementById("honors").checked==true) 
			{
				
				pointscheck.push("honors");
			}
			if (document.getElementById("pinfo").checked==true) 
			{
				pointscheck.push("pinfo");
			}
			if (document.getElementById("resume").checked==true) 
			{
				pointscheck.push("resume");
			}
			/*if (document.getElementById("eeocdata").checked==true) 
			{
				pointscheck.push("eeocdata");
			}*/
		
		
		
		var chkForPrint = 1;		
			if(inputs!="")
			{
				    CandidateGridAjax.getPrintMultipleData(inputs,chkForPrint,chkcl,chkqq,chkportfolio, chkjsi,pointscheck,{ 
					async: true,
					errorHandler:handleError,
					callback:function(data)
					{
				    	$('#loadingDiv').hide();
						try
						{
						    	var newWindow = window.open();
						    	newWindow.document.write(data);			
								newWindow.print();							   
						}
						catch (e) 
						{							
							$('#printmessage').modal('show');							 
						}	
					}
				});
			}
	}	
	
	//mukesh
	function chkportfolioOPtions()
	{
		 var elements = document.getElementsByName("chkportfolioItem");
		// alert(document.getElementsByName("cqp")[3].checked)
		 if(document.getElementsByName("cqp")[3].checked==true)
		 {
			 $("#portfolioItmDiv").show();
			 for (i=0;i<elements.length;i++) 
			 {
				elements[i].checked = true;
				elements[i].disabled=false;
			 }
		 }
		 else{
			 $("#portfolioItmDiv").hide();
			 for (i=0;i<elements.length;i++) 
			 {
				elements[i].checked = true;
				elements[i].disabled=true;
			 }
		 }
		 	
	}


	// Print Multiple CG Data
	function printMultipleData()
	{
		
		$('#errForPrint').empty();
		var savecandidatearray="";
		var chkForPrint = 0 ;
		
		var inputs = document.getElementsByName("cgCBX"); 
		 for (var i = 0; i < inputs.length; i++) {
		        if (inputs[i].type === 'checkbox') {
		        	if(inputs[i].checked){
		        		savecandidatearray+=	inputs[i].value+",";
		            }
		        }
		 } 
		// alert("array length=="+savecandidatearray.length)
		 var chkcl=false;
		 var chkqq=false;
		 var chkjsi=false;
		 var chkportfolio=false;
		 var chkforPortfolios=0;
		 
		 var chkacademics=false;
		 var chkcertification=false;
		 var chkreferences=false;
		 var chkexperience=false;
		 var chkinvolvement=false;
		 
		 var pointscheck = new Array();
		
		 var cqpinputs = document.getElementsByName("cqp"); 	
		 for (var i = 0; i < cqpinputs.length; i++) {
		        if (cqpinputs[i].type === 'checkbox') {
		        	if(cqpinputs[i].checked){		        		
		        		 if(cqpinputs[i].value=="chkcl")
		        			 chkcl=true;
		        		
		        		 if(cqpinputs[i].value=="chkqq")
		        			 chkqq=true;
		        		 
		        		 if(cqpinputs[i].value=="chkjsi")
		        			 chkjsi=true;
		        		
		        		 if(cqpinputs[i].value=="chkportfolio")
		        		 {
		        			chkportfolio=true;
		        			
		        			if (document.getElementById("academics").checked==true)
		        			{
		        				chkforPortfolios=chkforPortfolios+1
		        				pointscheck.push("acd");
		        			}	
		        			
	        				if (document.getElementById("certification").checked==true) 
	        				{	
		        				chkforPortfolios=chkforPortfolios+1
		        				pointscheck.push("cert");
	        				}
		        			
	        				if (document.getElementById("references").checked==true) 
	        				{
		        				chkforPortfolios=chkforPortfolios+1
		        				pointscheck.push("ref");
	        				}
		        			
	        				if (document.getElementById("experience").checked==true) 
	        				{
		        				chkforPortfolios=chkforPortfolios+1
		        				pointscheck.push("exp");
	        				}
		        			
	        				if (document.getElementById("involvement").checked==true) 
	        				{
		        				chkforPortfolios=chkforPortfolios+1
		        				pointscheck.push("invol");
	        				}
	        				if (document.getElementById("honors").checked==true) 
	        				{
		        				chkforPortfolios=chkforPortfolios+1
		        				pointscheck.push("honors");
	        				}
	        				if (document.getElementById("pinfo").checked==true) 
	        				{
		        				chkforPortfolios=chkforPortfolios+1
		        				pointscheck.push("pinfo");
	        				}
	        				if (document.getElementById("resume").checked==true) 
	        				{
		        				chkforPortfolios=chkforPortfolios+1
		        				pointscheck.push("resume");
	        				}
	        				/*if (document.getElementById("eeocdata").checked==true) 
	        				{
		        				chkforPortfolios=chkforPortfolios+1
		        				pointscheck.push("eeocdata");
	        				}*/
						      
		        		 }
		        		
		            }
		        }
		 } 	
		 if(chkforPortfolios==0 && chkportfolio==true)
		 {
			 $('#errForPrint').show();
			 $('#errForPrint').append("&#149; "+resourceJSON.PlzSelectPortfolio+"<br>");
			 return false;
		 }
		 if(chkcl==false && chkqq==false && chkportfolio==false && chkjsi==false)
		 {
			 $('#errForPrint').show();
			 $('#errForPrint').append("&#149; "+resourceJSON.PlzSelectOneOption+"<br>");
		 }
		 else
		 {
			 $('#loadingDiv').fadeIn();
			 CandidateGridAjax.getPrintMultipleData(savecandidatearray,chkForPrint,chkcl,chkqq,chkportfolio, chkjsi, pointscheck,{ 
					async: true,
					errorHandler:handleError,
					callback:function(data)
					{
				     $('#loadingDiv').hide();
						try{
							$('#selectedprintData').modal('hide');
							$('#printDataProfile').modal('show');
							
							for (var i = 0; i < cqpinputs.length; i++) {						      
						        	if(cqpinputs[i].checked==true){		        		
						        		cqpinputs[i].checked=false; 
						            }						      
						    } 							
							$('#printDataProfileDiv').html(data);
						}catch(err){}
						
					}
				});
			 
			 
		 }
	}
	
	function showSelectedDataForPrint()
	{
		$('#errForPrint').empty();
		$('#errForPrint').hide();
		$('#selectedprintData').modal('show');
          var elements = document.getElementsByName("chkportfolioItem");
			 for (i=0;i<elements.length;i++) 
			 {
				elements[i].checked = true;
				elements[i].disabled=true;
			 }
		
		//$('#selectedprintDiv').html("data selection div for print");
	}
	
	function cancelMultiPrint()
	{
		$('#errForPrint').empty();
		$('#errForPrint').hide();
		$("#portfolioItmDiv").hide();
		 var cqpinputs = document.getElementsByName("cqp"); 	
		for (var i = 0; i < cqpinputs.length; i++) {						      
        	if(cqpinputs[i].checked==true){		        		
        		cqpinputs[i].checked=false; 
            }						      
        } 	
		$('#selectedprintData').modal('hide');
		
		var elements = document.getElementsByName("chkportfolioItem");
			 for (i=0;i<elements.length;i++) 
			   elements[i].checked = false;
		
	}
	
	
/* @End
 * @Ashish Kumar
 * @Description :: Show Qualification details Div and save Qualification Note
 * */
function downloadAttachInstructionFileName(filePath,fileName,secondaryStatusName)
{
	$('#loadingDiv').show();
	ManageStatusAjax.downloadAttachInstructionFile(filePath,fileName,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			$('#loadingDiv').hide();
			if(data!='')
			{
				if (data.indexOf(".doc") !=-1) 
				{
					try{$('#modalDownloadIAFN').modal('hide');}catch(err){}
				    document.getElementById('ifrmAttachInstructionFileName').src = ""+data+"";
				}
				else
				{
					document.getElementById('ifrmAttachInstructionFileName').src = ""+data+"";
					try{
						$( "#modalDownloadIAFN" ).draggable({
							handle:'#modalDownloadIAFNMove'
						});
						$('#modalDownloadIAFN').modal('hide');
						$('#myModalLabelInstructionHeader').html(""+resourceJSON.MsgInstruction+" "+secondaryStatusName);
						$('#modalDownloadIAFN').modal('show');
					}catch(err){}
				}
			}
		}
	});
}
function downloadCertificationforCG(certId, linkId,teaId)
{		
	    $("#myModalLabelText").html(resourceJSON.MsgCertification);	
	    TeacherProfileViewInDivAjax.downloadCertificationforCG(certId,teaId,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data){
			if(data=="")
			{
				data="javascript:void(0)";
			}
			else
			{
				if(deviceType)
				{
					if (data.indexOf(".doc")!=-1)
					{
						$("#docfileNotOpen").css({"z-index":"3000"});
				    	$('#docfileNotOpen').show();
					}
					else if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
					{
						$("#exelfileNotOpen").css({"z-index":"3000"});
				    	$('#exelfileNotOpen').show();
					}
					else
					{
						document.getElementById(linkId).href = data;
					}
				}
				else if(deviceTypeAndroid)
				{
					if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
					{
						document.getElementById("ifrmCert").src=data;
					}
					else
					{
						document.getElementById(linkId).href = data;	
					}	
				}
				else if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					document.getElementById("ifrmCert").src=data;
				}
				else
				{
					try{
						document.getElementById('ifrmTransCommon').src = ""+data+"";
						$('#modalDownloadsCommon').modal('show');
					}catch(err)
					  {}
				}			
			}	
			return false;
		}});
}



function getPosition(element) {
    var xPosition = 0;
    var yPosition = 0;
  
    while(element) {
        xPosition += (element.offsetLeft - element.scrollLeft + element.clientLeft);
        yPosition += (element.offsetTop - element.scrollTop + element.clientTop);
        element = element.offsetParent;
    }
    return { x: xPosition, y: yPosition };
}

//changed: by rajendra
function downloadReferenceForCandidate(referenceId,teacherId,linkId)
{	
	$("#myModalLabelText").html(resourceJSON.MsgReference);	
		PFCertifications.downloadReferenceForCandidate(referenceId,teacherId,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
		if(data=="")
		{
			data="javascript:void(0)";
		}
		else
		{
			if(deviceType)
			{
				if (data.indexOf(".doc")!=-1)
				{
					$("#docfileNotOpen").css({"z-index":"3000"});
			    	$('#docfileNotOpen').show();
				}
				else if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					$("#exelfileNotOpen").css({"z-index":"3000"});
			    	$('#exelfileNotOpen').show();
				}
				else
				{
					document.getElementById(linkId).href = data;
				}
			}
			else if(deviceTypeAndroid)
			{
				if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					document.getElementById("ifrmRef").src=data;
				}
				else
				{
					document.getElementById(linkId).href = data;
				}	
			}
			else if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
			{
				document.getElementById("ifrmRef").src=data;
			}
			else
			{
				try{
					document.getElementById('ifrmTransCommon').src = ""+data+"";
					$('#modalDownloadsCommon').modal('show');								
				}catch(err)
				  {
					alert(err)
				  }
			}			
		}	
		return false;
		}});
}
function downloadUploadedDocument(additionDocumentId,linkId,teacherid)
{		
	    $("#myModalLabelText").html(resourceJSON.MsgDocument);	
		PFCertifications.downloadUploadedDocumentForCg(additionDocumentId,teacherid,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
			if(data=="")
			{
				data="javascript:void(0)";
			}
			else
			{
				if(deviceType)
				{
					if (data.indexOf(".doc")!=-1)
					{
						$("#docfileNotOpen").css({"z-index":"3000"});
				    	$('#docfileNotOpen').show();
					}
					else if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
					{
						$("#exelfileNotOpen").css({"z-index":"3000"});
				    	$('#exelfileNotOpen').show();
					}
					else
					{
						document.getElementById(linkId).href = data;
					}
				}
				else if(deviceTypeAndroid)
				{
					if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
					{
						document.getElementById("ifrmRef").src=data;
					}
					else
					{
						document.getElementById(linkId).href = data;
					}	
				}
				else if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					document.getElementById("ifrmRef").src=data;
				}
				else
				{
					try{
						document.getElementById('ifrmTransCommon').src = ""+data+"";
						$('#modalDownloadsCommon').modal('show');								
					}catch(err)
					  {
						alert(err)
					  }
				}			
			}	
			return false;			
		}});
}


function viewCompleteTeacherStatusNotesClose(){
	try
	{
		$('#myModalViewCompleteTeacherStatusNotes').modal('hide');
		$("#jWTeacherStatusNotesDiv").css({"z-index":"3000"});
		$('#jWTeacherStatusNotesDiv').modal('show');
	}catch(err){}
}

function viewCompleteTeacherStatusNotes(teacherStatusNoteId){
	try
	{
		CandidateGridAjax.viewCompleteTeacherStatusNotes(teacherStatusNoteId,{ 
			async: false,
			errorHandler:handleError,
			callback:function(data)
			{	if(data!="")
				{
				    $("#jWTeacherStatusNotesDiv").css({"z-index":"100"});
					$('#jWTeacherStatusNotesDiv').modal('hide');					
					document.getElementById("myModalViewCompleteTeacherStatusNotesInnerText").innerHTML=data;
					$('#myModalViewCompleteTeacherStatusNotes').modal('show');
				}
			}});
	}catch(err){}	
}

function notExternal(externalFlag){	
var jobId=document.getElementById("jobId").value;
var savecandidatearray=""; 
var inputs = document.getElementsByName("cgCBX"); 
	 for (var i = 0; i < inputs.length; i++) {
	        if (inputs[i].type === 'checkbox') {
	        	if(inputs[i].checked){
	        		savecandidatearray+=	inputs[i].value+",";
	            }
	        }
	 } 	
	 CandidateGridAjax.notExternal(jobId,savecandidatearray,externalFlag,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#loadingDiv').hide();			
			if(data==0){
				document.getElementById("internalTxt").innerHTML=""+resourceJSON.PlzSelectExtCand+"" ;
			}else if(data==1){
				document.getElementById("internalTxt").innerHTML=""+resourceJSON.MsgOneOrMoreCandidateSelected+" <a href='#' onclick='showInternalTrans()'>"+resourceJSON.BtnClick+"</a> "+resourceJSON.MsgDetailOfInternalTransferCandidate+"";	
			}else if(data==2){
				document.getElementById("internalTxt").innerHTML=""+resourceJSON.MsgRegisteredAsInternalTransfer+" <a href='#' onclick='showInternalTrans()'>"+resourceJSON.BtnClick+"</a> "+resourceJSON.MsgDetailOfInternalTransferCandidate+"";	
			}else if(data==3){
				//document.getElementById("internalTxt").innerHTML="EPI invites are successfully sent to the selected candidates.";	
				document.getElementById("internalTxt").innerHTML=""+resourceJSON.MsgEPIInviteSuccessfully+"";
			}else if(data==4){
				//document.getElementById("internalTxt").innerHTML="EPI invites are successfully sent to the selected candidates. One or more Candidate(s) have already completed the EPI. Email for EPI invite is not sent to such candidate(s).";	
				document.getElementById("internalTxt").innerHTML=""+resourceJSON.MsgEPIInviteSuccessfully+"";
			}else{
				document.getElementById("internalTxt").innerHTML=""+resourceJSON.MsgStatusofExternalCandidate+"";
			}
			$('#internalDiv').modal('show');
		}});
	
}

function notInternal(interFlag){
	$('#loadingDiv').show();
	var jobId=document.getElementById("jobId").value;
	var savecandidatearray="";
	 
	var inputs = document.getElementsByName("cgCBX"); 
	 for (var i = 0; i < inputs.length; i++) {
	        if (inputs[i].type === 'checkbox') {
	        	if(inputs[i].checked){
	        		savecandidatearray+=	inputs[i].value+",";
	            }
	        }
	 } 
 	CandidateGridAjax.notInternal(jobId,savecandidatearray,interFlag,{ 
	async: true,
	errorHandler:handleError,
	callback:function(data)
	{
		$('#loadingDiv').hide();
		if(data==0){
			document.getElementById("internalTxt").innerHTML=""+resourceJSON.PlzSelectInternalCandidates+"";
		}else if(data==1){
			document.getElementById("internalTxt").innerHTML=""+resourceJSON.MsgOneOrMoreCandidateSelected+" <a href='#' onclick='showInternalTrans()'>"+resourceJSON.BtnClick+"</a> "+resourceJSON.MsgDetailOfInternalTransferCandidate+"";	
		}else if(data==2){
			document.getElementById("internalTxt").innerHTML=""+resourceJSON.MsgRegisteredAsInternalTransfer+" <a href='#' onclick='showInternalTrans()'>"+resourceJSON.BtnClick+"</a> "+resourceJSON.MsgDetailOfInternalTransferCandidate+"";	
		}else if(data==3){
			//document.getElementById("internalTxt").innerHTML="EPI invites are successfully sent to the selected candidates.";	
			document.getElementById("internalTxt").innerHTML=""+resourceJSON.MsgEPIInviteSuccessfully+"";
		}else if(data==4){
			//document.getElementById("internalTxt").innerHTML="EPI invites are successfully sent to the selected candidates. One or more Candidate(s) have already completed the EPI. Email for EPI invite is not sent to such candidate(s).";	
			document.getElementById("internalTxt").innerHTML=""+resourceJSON.MsgEPIInviteWereSuccessfully+"";
		}else{
			document.getElementById("internalTxt").innerHTML=""+resourceJSON.MsgStatusofInternalCandidate+"";
		}
		$('#internalDiv').modal('show');
	}});
}
function backInternal(){
	getCandidateGrid();
	refreshStatus();
}
function showInternalTrans(){
	var jobId=document.getElementById("jobId").value;
	var savecandidatearray="";
	 
	var inputs = document.getElementsByName("cgCBX"); 
	 for (var i = 0; i < inputs.length; i++) {
	        if (inputs[i].type === 'checkbox') {
	        	if(inputs[i].checked){
	        		savecandidatearray+=	inputs[i].value+",";
	            }
	        }
	 } 
	CandidateGridAjax.displayInternalTrans(jobId,savecandidatearray,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#loadingDiv').hide();
			document.getElementById("internalTxt").innerHTML=data;
			$('#internalDiv').modal('show');
			applyScrollOnInterTrans();
		}});
}

function getInternalScoreDetails()
{
	var expandchk=document.getElementById("expandchk").value;
	if(expandchk >= 1)
	{
		$( ".tdscore0_internal:first" ).animate({
		    left :0,
			 width:256
		  }, {
		    duration: 1000
		  });
		
		try { 
			document.getElementById("tdscore0tbl_internal").style.textIndent="0px";
		} catch (e) {
			// TODO: handle exception
		}
		for(var i=1;i<=sectionRecordInter;i++)
		{
			try {
				document.getElementById("tdscore_internal"+i).style.display="inline";
			} catch (e) {
				// TODO: handle exception
			}
			
		}
	}
	else
	{

		 $( ".tdscore0_internal:first" ).animate({
			    left :0,
				 width:1
			  }, {
			    duration: 1000
			  });
		 
		 try {
			 	document.getElementById("tdscore0tbl_internal").style.textIndent="-99999px";
		 } catch (e) {
				// TODO: handle exception
			}
		 
		for(var i=1;i<=sectionRecordInter;i++)
		{
			try {
				document.getElementById("tdscore_internal"+i).style.display="none";
			} catch (e) {
				// TODO: handle exception
			}
		}
	}
}


function getIncompleteScoreDetails()
{
	var expandchk=document.getElementById("expandchk").value;
	if(expandchk >= 1)
	{
		$( ".tdscore0_incomplete:first" ).animate({
		    left :0,
			 width:256
		  }, {
		    duration: 1000
		  });
		try {
		document.getElementById("tdscore0tbl_incomplete").style.textIndent="0px";
		} catch (e) {
			// TODO: handle exception
		}
		for(var i=1;i<=sectionRecordInComp;i++)
		{
			try {
				document.getElementById("tdscore_incomplete"+i).style.display="inline";
			} catch (e) {
				// TODO: handle exception
			}
			
		}
	}
	else
	{
		 $( ".tdscore0_incomplete:first" ).animate({
			    left :0,
				 width:1
			  }, {
			    duration: 1000
			  });
		
		 try {
			 document.getElementById("tdscore0tbl_incomplete").style.textIndent="-99999px";
		 } catch (e) {
				// TODO: handle exception
			}
		for(var i=1;i<=sectionRecordInComp;i++)
		{
			try {
				document.getElementById("tdscore_incomplete"+i).style.display="none";
			} catch (e) {
				// TODO: handle exception
			}
			
			
		}
	}
}

function getRejectScoreDetails()
{
	var expandchk=document.getElementById("expandchk").value;
	if(expandchk >= 1)
	{
		$( ".tdscore0_reject:first" ).animate({
		    left :0,
			 width:256
		  }, {
		    duration: 1000
		  });
		try {
		document.getElementById("tdscore0tbl_reject").style.textIndent="0px";
		} catch (e) {
			// TODO: handle exception
		}
		for(var i=1;i<=sectionRecordRemove;i++)
		{
			try {
				document.getElementById("tdscore_reject"+i).style.display="inline";
			} catch (e) {
				// TODO: handle exception
			}
			
		}
	}
	else
	{
		 $( ".tdscore0_reject:first" ).animate({
			    left :0,
				 width:1
			  }, {
			    duration: 1000
			  });
		
		 try {
			 document.getElementById("tdscore0tbl_reject").style.textIndent="-99999px";
		 } catch (e) {
				// TODO: handle exception
			}
		for(var i=1;i<=sectionRecordRemove;i++)
		{
			try {
				document.getElementById("tdscore_reject"+i).style.display="none";
			} catch (e) {
				// TODO: handle exception
			}
			
			
		}
	}
}


function getWithdrawnScoreDetails()
{
	var expandchk=document.getElementById("expandchk").value;
	if(expandchk >= 1)
	{
		$( ".tdscore0_withdrawn:first" ).animate({
		    left :0,
			 width:256
		  }, {
		    duration: 1000
		  });
		try {
		document.getElementById("tdscore0tbl_withdrawn").style.textIndent="0px";
		} catch (e) {
			// TODO: handle exception
		}
		for(var i=1;i<=sectionRecordWithDr;i++)
		{
			try {
				document.getElementById("tdscore_withdrawn"+i).style.display="inline";
			} catch (e) {
				// TODO: handle exception
			}
			
		}
	}
	else
	{
		 $( ".tdscore0_withdrawn:first" ).animate({
			    left :0,
				 width:1
			  }, {
			    duration: 1000
			  });
		
		 try {
			 document.getElementById("tdscore0tbl_withdrawn").style.textIndent="-99999px";
		 } catch (e) {
				// TODO: handle exception
			}
		for(var i=1;i<=sectionRecordWithDr;i++)
		{
			try {
				document.getElementById("tdscore_withdrawn"+i).style.display="none";
			} catch (e) {
				// TODO: handle exception
			}
			
			
		}
	}
}
function getHiredScoreDetails()
{
	var expandchk=document.getElementById("expandchk").value;
	if(expandchk >= 1)
	{
		$( ".tdscore0_hired:first" ).animate({
		    left :0,
			 width:256
		  }, {
		    duration: 1000
		  });
		try {
		document.getElementById("tdscore0tbl_hired").style.textIndent="0px";
		} catch (e) {
			// TODO: handle exception
		}
		for(var i=1;i<=sectionRecordHired;i++)
		{
			try {
				document.getElementById("tdscore_hired"+i).style.display="inline";
			} catch (e) {
				// TODO: handle exception
			}
			
		}
	}
	else
	{
		 $( ".tdscore0_hired:first" ).animate({
			    left :0,
				 width:1
			  }, {
			    duration: 1000
			  });
		
		 try {
			 document.getElementById("tdscore0tbl_hired").style.textIndent="-99999px";
		 } catch (e) {
				// TODO: handle exception
			}
		for(var i=1;i<=sectionRecordHired;i++)
		{
			try {
				document.getElementById("tdscore_hired"+i).style.display="none";
			} catch (e) {
				// TODO: handle exception
			}
		}
	}
}

function getTimeoutScoreDetails()
{
	var expandchk=document.getElementById("expandchk").value;
	if(expandchk >= 1)
	{
		$( ".tdscore0_timeout:first" ).animate({
		    left :0,
			 width:256
		  }, {
		    duration: 1000
		  });
		try {
		document.getElementById("tdscore0tbl_timeout").style.textIndent="0px";
		} catch (e) {
			// TODO: handle exception
		}
		for(var i=1;i<=sectionRecordVlt;i++)
		{
			try {
				document.getElementById("tdscore_timeout"+i).style.display="inline";
			} catch (e) {
				// TODO: handle exception
			}
			
		}
	}
	else
	{
		 $( ".tdscore0_timeout:first" ).animate({
			    left :0,
				 width:1
			  }, {
			    duration: 1000
			  });
		
		 try {
			 document.getElementById("tdscore0tbl_timeout").style.textIndent="-99999px";
		 } catch (e) {
				// TODO: handle exception
			}
		for(var i=1;i<=sectionRecordVlt;i++)
		{
			try {
				document.getElementById("tdscore_timeout"+i).style.display="none";
			} catch (e) {
				// TODO: handle exception
			}
		}
	}
}


function sendEpiInVites(){
	$('#loadingDiv').show();
	var jobId=document.getElementById("jobId").value;
	var savecandidatearray="";
	 
	var inputs = document.getElementsByName("cgCBX");
	var comp=0;
	 for (var i = 0; i < inputs.length; i++) {
	        if (inputs[i].type === 'checkbox') {
	        	if(inputs[i].checked){
	        		//if(inputs[i].getAttribute("jsi")=="noJsi"){	        		
	        			savecandidatearray+=	inputs[i].value+",";
	        		/*}else if(inputs[i].getAttribute("jsi")=="comp"){
	        			comp++;
	        		}*/
	            }
	        }
	 } 
	 
	 	CandidateGridAjax.sendEpiInVites(jobId,savecandidatearray,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#loadingDiv').hide();
			if(data==1){
				document.getElementById("internalTxt").innerHTML=""+resourceJSON.MsgEPIInviteCouldNotBeSent+":<p style='margin-left:18px;'>1. "+resourceJSON.MsgCompleteOrTimedOut+"<br>2. "+resourceJSON.OneOrMoreSelectedCandidates+"</p>"+resourceJSON.OtherSelectedCandidate+"";
			}else{
				document.getElementById("internalTxt").innerHTML="<p>"+resourceJSON.MsgFoundThatEPIInviteCouldNotBeSent+".</p>"+resourceJSON.OtherSelectedCandidate+"";
			}
			/*if(data==0){
				document.getElementById("internalTxt").innerHTML="Please select external Candidates only from accordion \"External Candidates\".";
			}
			else if(data==1){
				document.getElementById("internalTxt").innerHTML="We have successfully sent the JSI invite to all those Candidates who have not taken the JSI for this job. We found that one or more Candidates have already taken the JSI for this job. JSI invite is not sent to such Candidates.";
			}else if(data==0){
				document.getElementById("internalTxt").innerHTML="JSI Invites were sent successfully to the selected candidates..";
			}			
			*/
			$('#internalDiv').find('.modal-dialog').css({width:'67%'});
			$('#internalDiv').modal('show');
		}});
	 
}

function sendJsiInVites(){
	$('#loadingDiv').show();
	var jobId=document.getElementById("jobId").value;
	var savecandidatearray="";
	 
	var inputs = document.getElementsByName("cgCBX");
	var comp=0;
	 for (var i = 0; i < inputs.length; i++) {
	        if (inputs[i].type === 'checkbox') {
	        	if(inputs[i].checked){
	        		//if(inputs[i].getAttribute("jsi")=="noJsi"){	        		
	        			savecandidatearray+=	inputs[i].value+",";
	        		/*}else if(inputs[i].getAttribute("jsi")=="comp"){
	        			comp++;
	        		}*/
	            }
	        }
	 } 
	 
	 	CandidateGridAjax.sendJsiInVites(jobId,savecandidatearray,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#loadingDiv').hide();
			if(data==1){
				document.getElementById("internalTxt").innerHTML=""+resourceJSON.MsgJSIInviteCouldNotBeSent+":<p style='margin-left:18px;'>1. "+resourceJSON.AlreadyTakenJSIForThisJob+"<br>2. "+resourceJSON.OneOrMoreSelectedCandidates+"</p>"+resourceJSON.MsgJSIInviteSuccessfullySent+"";
			}else if(data==3){
				document.getElementById("internalTxt").innerHTML=""+resourceJSON.JSISentSuccessfully+"";
			}else{
				document.getElementById("internalTxt").innerHTML="<p>"+resourceJSON.MsgCompleteJobForJSI+"</p>"+resourceJSON.MsgJSIInviteSuccessfullySent+"";
			}
			/*if(data==0){
				document.getElementById("internalTxt").innerHTML="Please select external Candidates only from accordion \"External Candidates\".";
			}
			else if(data==1){
				document.getElementById("internalTxt").innerHTML="We have successfully sent the JSI invite to all those Candidates who have not taken the JSI for this job. We found that one or more Candidates have already taken the JSI for this job. JSI invite is not sent to such Candidates.";
			}else if(data==0){
				document.getElementById("internalTxt").innerHTML="JSI Invites were sent successfully to the selected candidates..";
			}			
			*/
			$('#internalDiv').find('.modal-dialog').css({width:'67%'});
			$('#internalDiv').modal('show');
		}});
	 
}


/*========================== TFA  Edit By Specific users ==========================================*/
function editTFAbyUser(teacherId)
{
	$("#draggableDivMaster").show();
	$('#edittfa').hide();
	$('#savetfa').show();
	$('#canceltfa').show();
	document.getElementById("tfaList").disabled=false;
}

function saveTFAbyUser()
{
	$("#tfaMsgShow").hide();
	
	var tfa = document.getElementById("tfaList").value;
	var teacherId = document.getElementById("teacherDetailId").value;
	
	TeacherInfotAjax.updateTFAByUser(teacherId,tfa,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
			var tfalist = document.getElementById("tfaList");
			if(data!=null)
			{
				for(var i=0; i<tfalist.options.length;i++)
				{	
					if(tfalist.options[i].value==data)
					{
						tfalist.options[i].selected = true;
					}
				}
			}
			else
			{
				tfalist.selectedIndex = 1;
			}
			
			$("#draggableDivMaster").show();
			$('#edittfa').show();
			$('#savetfa').hide();
			$('#canceltfa').hide();
			document.getElementById("tfaList").disabled=true;
		}
	});
}

function cancelTFAbyUser(teacherId)
{
		$("#draggableDivMaster").show();
		$('#edittfa').show();
		$('#savetfa').hide();
		$('#canceltfa').hide();
		document.getElementById("tfaList").disabled=true;
		var tfalist = document.getElementById("tfaList");
		TeacherInfotAjax.displayselectedTFA(teacherId,{ 
			async: false,
			errorHandler:handleError,
			callback:function(data)
			{
				for(var i=0; i<tfalist.options.length;i++)
				{	
					if(tfalist.options[i].value==data)
					{
						tfalist.options[i].selected = true;
					}
				}
			}
		});
}

function displayTFAMessageBox(teacherId)
{
	document.getElementById("teacherDetailId").value=teacherId;	
	$("#tfaMsgShow").show();
}

function closeTFAMsgBox()
{
	$("#tfaMsgShow").hide();
}

//------------------Rahul:GetDocument---14/11/2014---------------------

function getDocuments()
{
	CandidateGridAjax.getDocumentsByDistrictOrSchoolId(
			{
				async: true,
				errorHandler:handleError,
				callback:function(data)
				{	
				
					if(data!="" || data.length>0)
					{
						//$("#templateDiv").fadeIn();
						//$("#btnChangetemplatelink").hide();
						$("#districtDocument").html(data);
					}else
					{
						$("#documentdiv").hide();
					}
				}
			});	

	
}

function showDocumentLink()
{
var docname=document.getElementById("districtDocument").value;	

if(docname.length>1)
{
	$('#viewDoc').show();
	
}

else
{
	$('#viewDoc').hide();	
}

}




function vieDistrictFile() {
	
	var districtattachment=document.getElementById("districtDocument").value;
		
	CandidateGridAjax.vieDistrictFile(districtattachment,{
	async: true,
	errorHandler:handleError,
	callback:function(data)
	{
		if(deviceType)
		{
			
			if (data.indexOf(".doc")!=-1)
			{
				$("#docfileNotOpen").css({"z-index":"3000"});
		    	$('#docfileNotOpen').show();
			}
			else if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
			{
				$("#exelfileNotOpen").css({"z-index":"3000"});
		    	$('#exelfileNotOpen').show();
			}
			else
			{
				document.getElementById(linkid).href = data;
			}
		}
		else if(deviceTypeAndroid)
		{
		
			if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
			{
				// $('#distAttachmentDiv').modal('hide');
				 document.getElementById('ifrmAttachment').src = ""+data+"";
			}
			else
			{
				document.getElementById(linkid).href = data;
			}
		}
		else if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
		{
			 $('#distAttachmentShowDiv').modal('hide');
			 document.getElementById('ifrmAttachment').src = ""+data+"";
		} else {
			//alert(data)
			$('#myModalMessage').modal('hide');
				$('.distAttachmentShowDiv').modal('show');
				document.getElementById('ifrmAttachment').src = ""+data+"";
				return false;
		}
	}});

	}


function showMessageDiv1()
{
	$('#myModalMessage').modal('show');
	$('.distAttachmentShowDiv').modal('hide');
}

function showReferencesForm()
{
	 $("#divElectronicReferences").show();
	 resetReferenceform();
	 document.getElementById("salutation").focus();
}


//======================= Mukesh References ===============================
function insertOrUpdateElectronicReferencesM(sbtsource)
{
	var myfolder=0;
	var	sUrl = window.location.pathname;
	 if(sUrl.indexOf("myfolder") > -1)
		myfolder=1;
	 var firstName="";
	 var lastName="";
	 		//resetReferenceform();   
	 		var referenceDetails	= trim($('#referenceDetailstext').find(".jqte_editor").html());
			var teacherId		=   trim(document.getElementById("teacherId").value);
			var elerefAutoId	=	document.getElementById("elerefAutoId");
			var salutation		=	document.getElementById("salutation");
			
			if(myfolder==1)
			{
			 firstName		=	trim(document.getElementById("firstName1").value);
			 lastName		= 	trim(document.getElementById("lastName1").value);
			}else{
			 firstName		=	trim(document.getElementById("firstName").value);
			 lastName		= 	trim(document.getElementById("lastName").value);
			}
			var designation		= 	trim(document.getElementById("designation").value);
			
			var organization	=	trim(document.getElementById("organization").value);
			var email			=	trim(document.getElementById("email").value);
			
			var contactnumber	=	trim(document.getElementById("contactnumber").value);
			var rdcontacted0	=   document.getElementById("rdcontacted0");
			var rdcontacted1	=   document.getElementById("rdcontacted1");
			var pathOfReferencesFile = document.getElementById("pathOfReferenceFile");
			
			var cnt=0;
			var focs=0;	
			
			var fileName = pathOfReferencesFile.value;
			var ext = fileName.substr(fileName.lastIndexOf('.') + 1).toLowerCase();
			var fileSize=0;		
			if ($.browser.msie==true){	
			    fileSize = 0;	   
			}else{		
				if(pathOfReferencesFile.files[0]!=undefined)
				fileSize = pathOfReferencesFile.files[0].size;
			}
			$('#errordivElectronicReferences').empty();
			setDefColortoErrorMsgToElectronicReferences();
			if(firstName=="")
			{
				$('#errordivElectronicReferences').append("&#149; "+resourceJSON.PlzEtrFirstName+"<br>");
				if(focs==0)
				{ 
					if(myfolder==1)
					 $('#firstName1').focus();
					else
					 $('#firstName').focus();
				}
				if(myfolder==1)
				 $('#firstName1').css("background-color",txtBgColor);
				else
				 $('#firstName1').css("background-color",txtBgColor);
				cnt++;focs++;
			}
			if(lastName=="")
			{
				$('#errordivElectronicReferences').append("&#149; "+resourceJSON.PlzEtrLastName+"<br>");
				if(focs==0){
					if(myfolder==1)
					  $('#lastName1').focus();
					else
					  $('#lastName').focus();
				}
				if(myfolder==1)
				 $('#lastName1').css("background-color",txtBgColor);
				else
				 $('#lastName').css("background-color",txtBgColor);
				cnt++;focs++;
			}
			
			if(email=="")
			{
				$('#errordivElectronicReferences').append("&#149; "+resourceJSON.PlzEtrEmail+"<br>");
				if(focs==0)
					$('#email').focus();
				
				$('#email').css("background-color",txtBgColor);
				cnt++;focs++;
			}
			else if(!isEmailAddress(email))
			{		
				$('#errordivElectronicReferences').append("&#149; "+resourceJSON.PlzEtrVldEmail+"<br>");
				if(focs==0)
					$('#email').focus();
				
				$('#email').css("background-color",txtBgColor);
				cnt++;focs++;
			}if(contactnumber=="")
			{
				$('#errordivElectronicReferences').append("&#149; "+resourceJSON.PlzEtrCtctNum+"<br>");
				if(focs==0)
					$('#contactnumber').focus();
				
				$('#contactnumber').css("background-color",txtBgColor);
				cnt++;focs++;
			}
			if(organization=="")
			{
				$('#errordivElectronicReferences').append("&#149; "+resourceJSON.PlzEtrOrg+"<br>");
				if(focs==0)
					$('#organization').focus();
				
				$('#organization').css("background-color",txtBgColor);
				cnt++;focs++;
			}
			
			
			var rdcontacted_value;
			
			if (rdcontacted0.checked) {
				rdcontacted_value = false;
			}
			else if (rdcontacted1.checked) {
				rdcontacted_value = true;
			}
			
			if(ext!=""){
				if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt'))
				{
					$('#errordivElectronicReferences').append("&#149; "+resourceJSON.AcceptFileFormat+"<br>");
						if(focs==0)
							$('#pathOfReferenceFile').focus();
						
						$('#pathOfReferenceFile').css("background-color",txtBgColor);
						cnt++;focs++;	
						return false;
				}	
				else if(fileSize>=10485760)
				{
					$('#errordivElectronicReferences').append("&#149; "+resourceJSON.FileSizeLessthan10Mb+"<br>");
						if(focs==0)
							$('#pathOfReferenceFile').focus();
						
						$('#pathOfReferenceFile').css("background-color",txtBgColor);
						cnt++;focs++;	
						return false;
				}
			}
			

			if(cnt!=0)		
			{
				$('#errordivElectronicReferences').show();
				return false;
			}
			else
			{
				var teacherElectronicReferences = {elerefAutoId:null,salutation:null, firstName:null, lastName:null, designation:null, organization:null,email:null,contactnumber:null,rdcontacted:null};
				dwr.engine.beginBatch();
				
				dwr.util.getValues(teacherElectronicReferences);
				teacherElectronicReferences.rdcontacted=rdcontacted_value;
				teacherElectronicReferences.salutation=salutation.value;
				teacherElectronicReferences.firstName=firstName;
				teacherElectronicReferences.lastName=lastName;;
				teacherElectronicReferences.designation=designation;
				teacherElectronicReferences.organization=organization;
				teacherElectronicReferences.email=email;
				
				teacherElectronicReferences.referenceDetails=referenceDetails;
				
				var refFile = document.getElementById("pathOfReference").value;
				if(refFile!=""){
					teacherElectronicReferences.pathOfReference=refFile;
				}
				if(fileName!="")
				{	
					
					PFCertifications.findDuplicateReferences(teacherElectronicReferences,teacherId,{ 
						async: false,
						errorHandler:handleError,
						callback: function(data)
						{
						if(data=="isDuplicate")
						{
							$('#errordivElectronicReferences').append("&#149; "+resourceJSON.ElectronicRefAlreadyReg+"<br>");
							if(focs==0)
								$('#email').focus();
							$('#email').css("background-color",txtBgColor);
							cnt++;focs++;
							$('#errordivElectronicReferences').show();
							return false;
						}else{
							document.getElementById("frmElectronicReferences").submit();
						}
						}
					});
					
				}else{
					PFCertifications.saveUpdateElectronicReferences(teacherElectronicReferences,teacherId,{ 
						async: true,
						errorHandler:handleError,
						callback: function(data)
						{
							if(data=="isDuplicate")
							{
								$('#errordivElectronicReferences').append("&#149; "+resourceJSON.ElectronicRefAlreadyRegWithProvidedEmail+"<br>");
								if(focs==0)
									$('#email').focus();
								$('#email').css("background-color",txtBgColor);
									cnt++;focs++;
								$('#errordivElectronicReferences').show();
								return false;
							}
							hideElectronicReferencesForm();
							getElectronicReferencesGrid_DivProfile(teacherId);
						}
					});
				}
				
				dwr.engine.endBatch();
				return true;
			}
			
}

function setDefColortoErrorMsgToElectronicReferences()
{
	var myfolder=0;
	var sUrl = window.location.pathname;
	 if(sUrl.indexOf("myfolder") > -1)
		myfolder=1;
	$('#salutation').css("background-color","");
	
	if(myfolder==1){
	 	$('#firstName1').css("background-color","");
	 	$('#lastName1').css("background-color","");
	}else{
		$('#firstName').css("background-color","");
	 	$('#lastName').css("background-color","");
	}
	
	$('#designation').css("background-color","");
	$('#organization').css("background-color","");
	$('#email').css("background-color","");
	$('#contactnumber').css("background-color","");
}

function hideElectronicReferencesForm()
{
	resetReferenceform();
	document.getElementById("elerefAutoId").value="";
	document.getElementById("divElectronicReferences").style.display="none";
	//resetReferenceform();
	return false;
}
function resetReferenceform()
{
	var myfolder=0;
	var sUrl = window.location.pathname;
	 if(sUrl.indexOf("myfolder") > -1)
		myfolder=1;
	    document.getElementById("rdcontacted1").checked = true
	 	document.getElementById("salutation").value=0;
	    if(myfolder==1){
			document.getElementById("firstName1").value='';
			document.getElementById("lastName1").value='';
	    }else{
	    	document.getElementById("firstName").value='';
			document.getElementById("lastName").value='';
	    }
		document.getElementById("designation").value='';
		document.getElementById("organization").value='';
		document.getElementById("email").value='';
		document.getElementById("contactnumber").value='';
		$('#referenceDetailstext').find(".jqte_editor").html("");
		document.getElementById("pathOfReferenceFile").value="";
		document.getElementById("pathOfReferenceFile").value=null;
}


function saveNobleReference(fileName)
{
    var myfolder=0;
	var	sUrl = window.location.pathname;
	 if(sUrl.indexOf("myfolder") > -1)
		myfolder=1;
	    var firstName="";
	    var lastName="";
       var referenceDetailstext	= trim($('#referenceDetailstext').find(".jqte_editor").text());
		var teacherId		=   trim(document.getElementById("teacherId").value);	
	    var elerefAutoId	=	document.getElementById("elerefAutoId");
		var salutation		=	document.getElementById("salutation");
		if(myfolder==1){
		 firstName		=	trim(document.getElementById("firstName1").value);
		 lastName		= 	trim(document.getElementById("lastName1").value);
		}else{
			 firstName		=	trim(document.getElementById("firstName").value);
			 lastName		= 	trim(document.getElementById("lastName").value);
		}
		var designation		= 	trim(document.getElementById("designation").value);
		
		var organization	=	trim(document.getElementById("organization").value);
		var email			=	trim(document.getElementById("email").value);
		
		var contactnumber	=	trim(document.getElementById("contactnumber").value);
		var rdcontacted0	=   document.getElementById("rdcontacted0");
		var rdcontacted1	=   document.getElementById("rdcontacted1");
		
		var cnt=0;
		var focs=0;	
		
		var rdcontacted_value;
		if (rdcontacted0.checked) {
			rdcontacted_value = false;
		}
		else if (rdcontacted1.checked) {
			rdcontacted_value = true;
		}
		
		var teacherElectronicReferences = {elerefAutoId:null,salutation:null, firstName:null, lastName:null, designation:null, organization:null,email:null,contactnumber:null,rdcontacted:null,pathOfReference:null};
		dwr.engine.beginBatch();
	
		dwr.util.getValues(teacherElectronicReferences);
		teacherElectronicReferences.rdcontacted=rdcontacted_value;
		teacherElectronicReferences.salutation=salutation.value;
		teacherElectronicReferences.firstName=firstName;
		teacherElectronicReferences.lastName=lastName;;
		teacherElectronicReferences.designation=designation;
		teacherElectronicReferences.organization=organization;
		teacherElectronicReferences.email=email;
		teacherElectronicReferences.pathOfReference=fileName;
		teacherElectronicReferences.referenceDetails=referenceDetailstext;
		

		PFCertifications.saveUpdateElectronicReferences(teacherElectronicReferences,teacherId,{ 
			async: true,
			errorHandler:handleError,
			callback: function(data)
			{
				if(data=="isDuplicate")
				{
					///updateReturnThreadCount("ref");
					$('#errordivElectronicReferences').append("&#149; "+resourceJSON.ElectronicRefAlreadyRegWithProvidedEmail+"<br>");
					if(focs==0)
						$('#email').focus();
					$('#email').css("background-color",txtBgColor);
						cnt++;focs++;
					$('#errordivElectronicReferences').show();
					return false;
				}
				hideElectronicReferencesForm();
				getElectronicReferencesGrid_DivProfile(teacherId);
				
			}
		});
	
		dwr.engine.endBatch();
		return true;
	}
function editFormElectronicReferences(id)
{
	var myfolder=0;
	var	sUrl = window.location.pathname;
	 if(sUrl.indexOf("myfolder") > -1)
		myfolder=1;

	 $('#errordivElectronicReferences').empty();
	 setDefColortoErrorMsgToElectronicReferences();
	 PFCertifications.editElectronicReferences(id,
		{ 
		  async: false,
		  errorHandler:handleError,
		  callback: function(data)
		  {
	        showReferencesForm();
	        dwr.util.setValues(data);
	      
	        if(sUrl.indexOf("myfolder") > -1){
	    	  document.getElementById("firstName1").value  =data.firstName;
			  document.getElementById("lastName1").value	   =data.lastName;
	        }
		       
			$('#referenceDetailstext').find(".jqte_editor").html(data.referenceDetails);
			if(data.pathOfReference!=null && data.pathOfReference!="")
			{	
				 document.getElementById("pathOfReference").value=data.pathOfReference;
			}else{
				document.getElementById("pathOfReference").value="";
			}
			
			return false;
			}
		});
	return false;
}

function removeReferences()
{
	var teacherId =  document.getElementById("teacherId").value
	var referenceId = document.getElementById("elerefAutoId").value;
	if(window.confirm("Are you sure, you would like to delete the Recommendation Letter."))
	{
		PFCertifications.removeReferencesForNoble(referenceId,teacherId,
		{ 
			async: false,
			errorHandler:handleError,
			callback: function(data)
			{	
				getElectronicReferencesGrid_DivProfile(teacherId);
			//	document.getElementById("removeref").style.display="none";
				hideElectronicReferencesForm();
				
			}
		});
	}
}
var eleRefIDForTeacher=0;
var status_eref=0;
function changeStatusElectronicReferences(id,status)
{	
	eleRefIDForTeacher=id;
	status_eref=status;
	if(status==1)
	$('#chgstatusRef1').modal('show');
	else if(status==0)
	 $('#chgstatusRef2').modal('show');	
}
function changeStatusElectronicReferencesConfirm()
{	
	 var teacherId =  document.getElementById("teacherId").value
	$('#chgstatusRef1').modal('hide');
	$('#chgstatusRef2').modal('hide');
	
		PFCertifications.changeStatusElectronicReferences(eleRefIDForTeacher,status_eref, { 
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{
			    getElectronicReferencesGrid_DivProfile(teacherId);
				hideElectronicReferencesForm();
				return false;
			}});
		
		eleRefIDForTeacher=0;
}


function setDefColortoErrorMsgToVideoLinksForm()
{
	$('#videourl').css("background-color","");
}
function showVideoLinksForm()
{
	document.getElementById("frmvideoLinks").reset();
	document.getElementById("videolinkAutoId").value="";
	document.getElementById("divvideoLinks").style.display="block";
	document.getElementById("videourl").focus();
	setDefColortoErrorMsgToVideoLinksForm();
	$('#errordivvideoLinks').empty();
	return false;
}

function hideVideoLinksForm()
{
	document.getElementById("frmvideoLinks").reset();
	document.getElementById("videolinkAutoId").value="";
	document.getElementById("divvideoLinks").style.display="none";
	return false;
}

function insertOrUpdatevideoLinks()
{
	 
	 var teacherId =  document.getElementById("teacherId").value
	var elerefAutoId = document.getElementById("videolinkAutoId");
	var videourl = trim(document.getElementById("videourl").value);
	var cnt=0;
	var focs=0;	
	
	$('#errordivvideoLinks').empty();
	setDefColortoErrorMsgToVideoLinksForm();
	
	if(videourl=="")
	{
		$('#errordivvideoLinks').append("&#149; "+resourceJSON.PlzEtrVideoLink+"<br>");
		if(videourl==0)
			$('#videourl').focus();
		
		$('#videourl').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	
	if(cnt!=0 )		
	{
		$('#errordivvideoLinks').show();
		return false;
	}
	else
	{	
		var teacherVideoLink = {videolinkAutoId:null,videourl:null, createdDate:null};
		dwr.engine.beginBatch();
		dwr.util.getValues(teacherVideoLink);
		teacherVideoLink.videourl=videourl;
		PFCertifications.saveOrUpdateVideoLink(teacherVideoLink,teacherId,{ 
			async: false,
			errorHandler:handleError,
			callback: function(data)
			{
				hideVideoLinksForm();
				getVideoLinksGrid_DivProfile(teacherId);
			}
		});
		dwr.engine.endBatch();
		return true;
	}
}


function editFormVideoLink(id)
{
	PFCertifications.editVideoLink(id,
		{ 
			async: false,
			errorHandler:handleError,
			callback: function(data)
			{
				showVideoLinksForm();
				dwr.util.setValues(data);
				return false;
			}
		});
	return false;
}

function deleteFormVideoLink(id)
{
	PFCertifications.deleteVideoLink(id,
		{ 
			async: false,
			errorHandler:handleError,
			callback: function(data)
			{
				showVideoLinksForm();
				dwr.util.setValues(data);
				return false;
			}
		});
	return false;
}

var videolnkIDForTeacher=0;
function delVideoLink(id)
{	
	videolnkIDForTeacher=id;
	$('#delVideoLnk').modal('show');
}
function delVideoLnkConfirm()
{	
	 var teacherId =  document.getElementById("teacherId").value	
	 $('#delVideoLnk').modal('hide');
		PFCertifications.deleteVIdeoLink(videolnkIDForTeacher, { 
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{
			getVideoLinksGrid_DivProfile(teacherId);	
			//getVideoLinksGrid();
				hideVideoLinksForm();
				return false;
			}});
		
		videolnkIDForTeacher=0;
}

function saveDistrictSpecificQues(teacherId){	
	$('#errordivspecificquestion').empty();
	var arr =[];
	var jobOrder = {jobId:document.getElementById("jobId").value};
	var dspqType=document.getElementById("dspqType").value;
	var isRequiredCount=0;
	var ansRequiredCount=0;

	var totalQuestionsList=document.getElementById("totalQuestionsList").value;

	for(i=1;i<=totalQuestionsList;i++)
	{   
		var isRequiredAns=0;
		var isRequired = dwr.util.getValue("QS"+i+"isRequired");
		if(isRequired==1){
			isRequiredCount++;
		}
		var districtSpecificQuestion = {questionId:dwr.util.getValue("QS"+i+"questionId")};
		var questionTypeShortName = dwr.util.getValue("QS"+i+"questionTypeShortName");
		var questionTypeMaster = {questionTypeId:dwr.util.getValue("QS"+i+"questionTypeId")};
		var qType = dwr.util.getValue("QS"+i+"questionTypeShortName");
		var o_maxMarks = dwr.util.getValue("o_maxMarksS");
		
		var scroreVal=$('#dstQuesSlid'+i).val();		
			if ($("#ifrmQ"+i).length > 0) {
				var iframeNorm = document.getElementById("ifrmQ"+i);
				var innerNorm = iframeNorm.contentDocument || iframeNorm.contentWindow.document;
				if(innerNorm.getElementById('sliderQ'+i).value!='')
				$('#dstQuesSlid'+i).val(innerNorm.getElementById('sliderQ'+i).value);
				scroreVal=innerNorm.getElementById('sliderQ'+i).value;		
			}
		
		if(qType=='tf' || qType=='slsel' ||  qType=='slsel')
		{
			var optId="";
			var errorFlag=1;
			if($("input[name=QS"+i+"opt]:radio:checked").length > 0 )
			{
				 errorFlag=0;
				optId=$("input[name=QS"+i+"opt]:radio:checked").val();
			}else if(isRequired==1){
				$("#errordivspecificquestion").html("&#149; "+resourceJSON.PlzProvideResponseToAllQues+".<br>");
				dSPQuestionsErrorCount=1;
				errorFlag=1;
			}
			if(errorFlag==0){
				if(isRequired==1){
					isRequiredAns=1;
				}
				arr.push({ 
					"selectedOptions"  : optId,
					"question"  : dwr.util.getValue("QS"+i+"question"),
					"questionTypeMaster" : questionTypeMaster,
					"questionType" : questionTypeShortName,
					"questionOption" : dwr.util.getValue("qOptS"+optId),
					"districtSpecificPortfolioQuestions" : districtSpecificQuestion,
					"isValidAnswer" : dwr.util.getValue("QS"+optId+"validQuestion"),
					"jobOrder" : jobOrder,
					"sliderScore" : scroreVal
				});
			}

		}else if(qType=='ml' || qType=='sl')
		{
			var insertedText = dwr.util.getValue("QS"+i+"opt");
			if((insertedText!=null && insertedText!="") || isRequired==0)
			{
				if(isRequired==1){
					isRequiredAns=1;
				}
				var schoolMaster="";
				if($(".school"+i).length>0){
					schoolMaster=$(".school"+i).val();
				}
				
				arr.push({ 
					"insertedText"    : insertedText,
					"question"  : dwr.util.getValue("QS"+i+"question"),
					"questionTypeMaster" : questionTypeMaster,
					"questionType" : questionTypeShortName,
					"districtSpecificPortfolioQuestions" : districtSpecificQuestion,
					"jobOrder" : jobOrder,
					"schoolIdTemp" : schoolMaster
				});
			}else
			{
				if(isRequired==1){
					$("#errordivspecificquestion").html("&#149; "+resourceJSON.PlzProvideResponseToAllQues+".<br>");
					dSPQuestionsErrorCount=1;
				}
			}
		}else if(qType=='et'){
			var optId="";
			var errorFlag=1;
			if($("input[name=QS"+i+"opt]:radio:checked").length > 0 )
			{
				errorFlag=0;
				optId=$("input[name=QS"+i+"opt]:radio:checked").val();
			}else
			{
				if(isRequired==1){
					errorFlag=1;
					$("#errordivspecificquestion").html("&#149; "+resourceJSON.PlzProvideResponseToAllQues+".<br>");
					dSPQuestionsErrorCount=1;
				}
			}
			
			var insertedText = dwr.util.getValue("QS"+i+"optet");
			var isValidAnswer = dwr.util.getValue("QS"+optId+"validQuestion");
			if(isValidAnswer=="false" && insertedText.trim()==""){
				if(isRequired==1){
					$("#errordivspecificquestion").html("&#149; "+resourceJSON.PlzProvideResponseToAllQues+".<br>");
					dSPQuestionsErrorCount=1;
					errorFlag=1;
				}
			}
			if(errorFlag==0){
				if(isRequired==1){
					isRequiredAns=1;
				}
				arr.push({ 
					"selectedOptions"  : optId,
					"question"  : dwr.util.getValue("QS"+i+"question"),
					"insertedText"    : insertedText,
					"questionTypeMaster" : questionTypeMaster,
					"questionType" : questionTypeShortName,
					"questionOption" : dwr.util.getValue("qOptS"+optId),
					"districtSpecificPortfolioQuestions" : districtSpecificQuestion,
					"isValidAnswer" : isValidAnswer,
					"jobOrder" : jobOrder,
					"sliderScore" : scroreVal
				});
			}
		}else if(qType=='mlsel'){
			try{
				 var multiSelectArray="";
				 var inputs = document.getElementsByName("multiSelect"+i); 
				 for (var j = 0; j < inputs.length; j++) {
				        if (inputs[j].type === 'checkbox') {
				        	if(inputs[j].checked){
				        		multiSelectArray+=	inputs[j].value+"|";
				            }
				        }
				} 
				if(multiSelectArray!=""  || isRequired==0){
					if(isRequired==1){
						isRequiredAns=1;
					} 
					arr.push({ 
						"selectedOptions"  : multiSelectArray,
						"question"  : dwr.util.getValue("QS"+i+"question"),
						"questionTypeMaster" : questionTypeMaster,
						"questionType" : questionTypeShortName,
						//"questionOption" : dwr.util.getValue("qOptS"+optId),
						"districtSpecificPortfolioQuestions" : districtSpecificQuestion,
						"isValidAnswer" : isValidAnswer,
						"jobOrder" : jobOrder,
						"sliderScore" : scroreVal
					});
				}else{
					if(isRequired==1){
						$("#errordivspecificquestion").html("&#149; "+resourceJSON.PlzProvideResponseToAllQues+".<br>");
						dSPQuestionsErrorCount=1;
					}
				}
			}catch(err){}
		}if(qType=='mloet'){
			try{
				 var multiSelectArray="";
				 var inputs = document.getElementsByName("multiSelect"+i); 
				 for (var j = 0; j < inputs.length; j++) {
				        if (inputs[j].type === 'checkbox') {
				        	if(inputs[j].checked){
				        		multiSelectArray+=	inputs[j].value+"|";
				            }
				        }
				} 
				var insertedText = dwr.util.getValue("QS"+i+"optmloet"); 
				if(multiSelectArray!="" || isRequired==0){
					if(isRequired==1){
						isRequiredAns=1;
					} 
					arr.push({ 
						"selectedOptions"  : multiSelectArray,
						"question"  : dwr.util.getValue("QS"+i+"question"),
						"insertedText"    : insertedText,
						"questionTypeMaster" : questionTypeMaster,
						"questionType" : questionTypeShortName,
						//"questionOption" : dwr.util.getValue("qOptS"+optId),
						"districtSpecificPortfolioQuestions" : districtSpecificQuestion,
						"isValidAnswer" : isValidAnswer,
						"jobOrder" : jobOrder,
						"sliderScore" : scroreVal
					});
				}else{
					if(isRequired==1){
						$("#errordivspecificquestion").html("&#149; "+resourceJSON.PlzProvideResponseToAllQues+".<br>");
						dSPQuestionsErrorCount=1;
					}
				}
			}catch(err){}
		}else if(qType=='rt'){
			var optId="";
			var score="";
			var rank="";
			var scoreRank=0;
			var opts = document.getElementsByName("optS");
			var scores = document.getElementsByName("scoreS");
			var ranks = document.getElementsByName("rankS");
			var o_ranks = document.getElementsByName("o_rankS");
			
			var tt=0;
			var uniqueflag=false;
			for(var i = 0; i < opts.length; i++) {
				optId += opts[i].value+"|";
				score += scores[i].value+"|";
				rank += ranks[i].value+"|";
				if(checkUniqueRankForPortfolio(ranks[i]))
				{
					uniqueflag=true;
					break;
				}

				if(ranks[i].value==o_ranks[i].value)
				{
					scoreRank+=parseInt(scores[i].value);
				}
				if(ranks[i].value=="")
				{
					tt++;
				}
			}
			if(uniqueflag)
				return;

			if(tt!=0)
				optId=""; 

			if(optId=="")
			{
				//totalSkippedQuestions++;
				//strike checking
			}
			var totalScore = scoreRank;
			if(isRequired==1){
				isRequiredAns=1;
			} 
			arr.push({ 
				"selectedOptions"  : optId,
				"question"  : dwr.util.getValue("QS"+i+"question"),
				"questionTypeMaster" : questionTypeMaster,
				"questionType" : questionTypeShortName,
				"questionOption" : dwr.util.getValue("qOptS"+optId),
				"districtSpecificPortfolioQuestions" : districtSpecificQuestion,
				"isValidAnswer" : isValidAnswer,
				"jobOrder" : jobOrder,
				"optionScore"      : score,
				"totalScore"       : totalScore,
				"insertedRanks"    : rank,
				"maxMarks" :o_maxMarks,
				"sliderScore" : scroreVal
			});
		}else if(qType=='OSONP'){
			//	alert("hello "+qType);
			try{
				 var multiSelectArray="";
				 var inputs = document.getElementsByClassName("OSONP"+i); 
				 for (var j = 0; j < inputs.length; j++) {
				        if (inputs[j].type === 'radio') {
				        	if(inputs[j].checked){
				        		multiSelectArray+=	inputs[j].value+"|";
				            }
				        }
				} 
				var insertedText = dwr.util.getValue("QS"+i+"OSONP");
				
				if(multiSelectArray!="" || isRequired==0){
					if(isRequired==1){
						isRequiredAns=1;
					} 
					arr.push({ 
						"selectedOptions"  : multiSelectArray,
						"question"  : dwr.util.getValue("QS"+i+"question"),
						"insertedText"    : insertedText,
						"questionTypeMaster" : questionTypeMaster,
						"questionType" : questionTypeShortName,
						//"questionOption" : dwr.util.getValue("qOptS"+optId),
						"districtSpecificPortfolioQuestions" : districtSpecificQuestion,
						"isValidAnswer" : isValidAnswer,
						"jobOrder" : jobOrder
					});
				}else{
					if(isRequired==1){
						$("#errordivspecificquestion").html("&#149; "+resourceJSON.PlzProvideResponseToAllQues+".<br>");
						dSPQuestionsErrorCount=1;
					}
				}
			}catch(err){}
		}else if(qType=='DD'){
			try{
				
				 var multiSelectArray=$("#dropdown"+i+" :selected").val() ;				
			
				if(multiSelectArray!=""  || isRequired==0){
					if(isRequired==1){
						isRequiredAns=1;
					} 
					arr.push({ 
						"selectedOptions"  : multiSelectArray,
						"question"  : dwr.util.getValue("QS"+i+"question"),
						"questionTypeMaster" : questionTypeMaster,
						"questionType" : questionTypeShortName,
						//"questionOption" : dwr.util.getValue("qOptS"+optId),
						"districtSpecificPortfolioQuestions" : districtSpecificQuestion,
						"isValidAnswer" : isValidAnswer,
						"jobOrder" : jobOrder
					});
				}else{
					if(isRequired==1){
						$("#errordivspecificquestion").html("&#149; "+resourceJSON.PlzProvideResponseToAllQues+".<br>");
						dSPQuestionsErrorCount=1;
					}
				}
			}catch(err){}
		}
		if(qType=='sscb'){
			try{
				
				var insertedText = dwr.util.getValue("QS"+i+"multiselectText");
				 var multiSelectArray="";
				 var inputs = document.getElementsByName("multiSelect"+i); 
				 for (var j = 0; j < inputs.length; j++) {
				        if (inputs[j].type === 'checkbox') {
				        	if(inputs[j].checked){
				        		multiSelectArray+=	inputs[j].value+"|";
				            }
				        }
				} 
				 
				 if($(".school"+i).length>0){
						schoolMaster=$(".school"+i).val();
					}
				if(multiSelectArray!=""){
					if(isRequired==1){
						isRequiredAns=1;
					} 
					arr.push({ 
						"selectedOptions"  : multiSelectArray,
						"question"  : dwr.util.getValue("QS"+i+"question"),
						"insertedText"    : insertedText,
						"questionTypeMaster" : questionTypeMaster,
						"questionType" : questionTypeShortName,
						"schoolIdTemp" : schoolMaster,
						"districtSpecificPortfolioQuestions" : districtSpecificQuestion,
						"isValidAnswer" : isValidAnswer,
						"jobOrder" : jobOrder
					});
				}else{
					if(isRequired==1){
						$("#errordivspecificquestion").html("&#149; "+resourceJSON.PlzProvideResponseToAllQues+".<br>");
						dSPQuestionsErrorCount=1;
					}
				}
			}catch(err){}
		}
		
		if(isRequiredAns==1){
			ansRequiredCount++;
		}
	}
	//alert('isRequiredCount:::'+isRequiredCount);
	//alert('ansRequiredCount:::'+ansRequiredCount);
	if(isRequiredCount==ansRequiredCount) 
	{
		CandidateGridSubAjax.setDistrictQPortfoliouestions(arr,dspqType,teacherId,{ 
			async: false,
			errorHandler:handleError,
			callback: function(data){
			if(data!=null)
			{
				arr =[];
				$("#saveDataLoading").modal("show");
			}
		}
		});	
	}else
	{
		$("#errordivspecificquestion").html("&#149; "+resourceJSON.PlzProvideResponseToAllQues+".<br>");
		dSPQuestionsErrorCount=1;
	}
}
/*====================================================================*/
function showVideo(email)
{
	CandidateGridAjax.showVideo(email,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){
		$('#myModalVideoInterview').modal("show");
		//document.getElementById("iframeVideo").src="https://platform.teachermatch.org";
		document.getElementById("videoInterViewDiv").innerHTML=data;
	}
	});	
}

function pnqSave(teacherId){

	var pnqOption = $("#pnqList option:selected").val();
	TeacherProfileViewInDivAjax.updatePNQByUser(teacherId,pnqOption,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
			
		}
	});
}
/*====================================================================*/
function certTextDiv(id){
	$("#myModalDymanicPortfolio").modal("hide");
	//$("#cerTTextContent"+id).val();
	$(".certTextContent").html($("#cerTTextContent"+id).val());
	$("#certTextDivDSPQ").modal("show");
}
function certTextDiv(id){
	
	$("#draggableDivMaster").modal('hide');
	
	$(".certTextContent").html($("#cerTTextContent"+id).val());
	$("#certTextDivDSPQ").modal("show");
}
function certTextDivClose(){
	 
	 $("#draggableDivMaster").modal('show');
	 ("#certTextDivDSPQ").modal("show");
	 $(".certTextContent").html("");
} 
function onSelectOpenOptions(classNameCnt,optId){
	//var checkedVal = $("input:radio."+classNameCnt+":checked").val();
	
	$("."+classNameCnt).hide();	
	
	if ($("."+classNameCnt).css('display') == 'none') {
	    if(classNameCnt=='p1' && event.type=="click"){
	    	$(".p1 input:radio:checked").prop( "checked", false );
	    	$(".p2").hide();
	    	$(".p2 input:radio:checked").prop( "checked", false );
	    	$(".p3").hide();
	    }
	    if(classNameCnt=='p2' && event.type=="click"){
	    	$(".p3").hide();
	    }
	}
	if($("."+classNameCnt+optId).length>0){		
		$("."+classNameCnt+optId).show();
	}
}
/*====================================================================*/

//For Additional Document
function getdistrictAssessmetGrid_DivProfile(teacherId)
{
	//alert(noOfRowsaddDoc+":::::::::::::"+pageaddDoc+"::"+sortOrderStraddDoc+"::"+sortOrderTypeaddDoc+"::"+teacherId);
	var visitLocation=document.getElementById("teacherIdForprofileGridVisitLocation").value;
	TeacherProfileViewInDivAjax.getdistrictAssessmetGrid(noOfRowsASMT,pageASMT,sortOrderStrASMT,sortOrderTypeASMT,teacherId,visitLocation, { 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#divGridAssessmentDetails').html(data);
			applyScrollOnTbl_AssessmentDetails();

		}});
}

function downloadOctUpload(octId)
{
		//alert("downloadAnswer0");
	TeacherProfileViewInDivAjax.downloadOctUpload(octId,{ 
			async: false,
			errorHandler:handleError,
			callback:function(data)
			{
			if(data!="")
				if (data.indexOf(".doc") !=-1)
				{
					document.getElementById('uploadFrameReferencesID').src = ""+data+"";
				}
				else
				{
					document.getElementById("hrefOctUpload").href = data; 
					return false;
				}
			}
		});
	}

function getEmployeeNumberInfo(flag,table){
	var height="height:500px;";
	if(flag)
	height="height:auto;";
	var div="<div class='modal hide in' id='employeeInfo' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'><div class='modal-dialog' style='width:850px;'><div class='modal-content'><div class='modal-header'><!--<button type='button' class='close' data-dismiss='modal' aria-label='Close'><span class='confirmFalse' aria-hidden='true'>&times;</span></button>--><h4 class='modal-title' id='myModalLabel' style='color:white;'>Employee Information</h4></div><div class='modal-body' style='"+height+"overflow:auto;' id='tableData'></div><div class='modal-footer'><button type='button' class='btn btn-default confirmFalse' >Ok</button></div></div></div></div>";		
	try{
			$('#employeeInfo').modal('hide'); 
		}catch(e){}
		try{
			$('#employeeInfo').remove(); 
		}catch(e){}
		$('#draggableDivMaster').after(div);
		$('#draggableDivMaster').modal('hide');
		$('#employeeInfo').modal({show:true,backdrop: 'static',keyboard: false,});
		$('#employeeInfo #tableData').html(table); 
		$('.confirmFalse').click(function(){  	
			$('#employeeInfo').modal('hide');   
			$('#employeeInfo').remove(); 
		 	$('#draggableDivMaster').modal('show');  
		});
	$('#loadingDiv').hide();
 }
function openEmployeeNumberInfo(empId){
$('#loadingDiv').show();
  try{
	  TeacherProfileViewInDivAjax.openEmployeeNumberInfo(empId,{
		  async: false,
		  cache: false,
		  errorHandler:handleError,
		  callback:function(data){
		  $('#draggableDivMaster').modal('hide');
		  $('#draggableDivMaster').hide();
		  //alert(data);
		  var divData=data.split('####');
		  if(divData[0].trim()=='1')
			  	getEmployeeNumberInfo(true,divData[1]);
		  		else
		  		getEmployeeNumberInfo(false,data);
		  }
	   });
  }catch(e){alert(e)}
  }
function getTeacherAssessmentGrid_DivProfile(teacherId)
{
	var visitLocation=document.getElementById("teacherIdForprofileGridVisitLocation").value;
	
	TeacherProfileViewInDivAjax.getTeacherAssessmentGrid(teacherId,noOfRowsEJ,pageEJ,sortOrderStrEJ,sortOrderTypeEJ, { 
  async: true,
  errorHandler:handleError,
  callback:function(data)
  {
   $('#gridTeacherAssessment').html(data);
   
	   applyScrollOnAssessments();

  }});
}
function getBackgroundCheckGrid_DivProfile(teacherId)
{
	TeacherProfileViewInDivAjax.getBackgroundCheckGrid_DivProfile(teacherId, { 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#gridDataBackgroundCheck').html(data);			
			applyScrollOnTblBackgroundCheck_profile();
		}});
}
function downloadBackgroundCheck(teacherId,linkId)
{	
	$('#loadingDiv').show();
	CandidateGridAjaxNew.downloadBackgroundCheck(teacherId,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{			
			$('#loadingDiv').hide();
			if(data=="")	
			{
				alert("Background Check is not uploaded by the candidate.")
			}
			else
			{
				if(deviceType)
				{
					if (data.indexOf(".doc")!=-1)
					{
						$("#docfileNotOpen").css({"z-index":"3000"});
				    	$('#docfileNotOpen').show();
					}
					else if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
					{
						$("#exelfileNotOpen").css({"z-index":"3000"});
				    	$('#exelfileNotOpen').show();
					}
					else
					{
						document.getElementById(linkId).href = data; 	
					}
				}
				else if(deviceTypeAndroid)
				{
					if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
					{
						document.getElementById('ifrmTrans').src = ""+data+"";
					}
					else
					{
						document.getElementById(linkId).href = data; 		
					}
				}
				else if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					document.getElementById('ifrmTrans').src = ""+data+"";
				}
				else
				{
					document.getElementById(linkId).href = data; 		
				}			
			}	
			return false;			
							
		}
	});
}
