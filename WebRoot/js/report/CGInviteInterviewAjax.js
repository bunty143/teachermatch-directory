if (window['dojo']) dojo.provide('dwr.interface.CGInviteInterviewAjax');
if (typeof this['CGInviteInterviewAjax'] == 'undefined') CGInviteInterviewAjax = {};
CGInviteInterviewAjax._path = '/dwr';
CGInviteInterviewAjax.displayInviteInterviewQues = function(p0, p1, p2, callback) {
return dwr.engine._execute(CGInviteInterviewAjax._path, 'CGInviteInterviewAjax', 'displayInviteInterviewQues', arguments);
};
CGInviteInterviewAjax.saveIIStatus = function(p0, p1, p2, callback) {
return dwr.engine._execute(CGInviteInterviewAjax._path, 'CGInviteInterviewAjax', 'saveIIStatus', arguments);
};
CGInviteInterviewAjax.openInterviewURL = function(p0, p1, callback) {
return dwr.engine._execute(CGInviteInterviewAjax._path, 'CGInviteInterviewAjax', 'openInterviewURL', arguments);
};
CGInviteInterviewAjax.saveInterviewValue = function(p0, p1, callback) {
return dwr.engine._execute(CGInviteInterviewAjax._path, 'CGInviteInterviewAjax', 'saveInterviewValue', arguments);
};
CGInviteInterviewAjax.saveIIStatusForEvent = function(p0, callback) {
return dwr.engine._execute(CGInviteInterviewAjax._path, 'CGInviteInterviewAjax', 'saveIIStatusForEvent', arguments);
};
CGInviteInterviewAjax.getPathOfVideoUrl = function(p0, callback) {
return dwr.engine._execute(CGInviteInterviewAjax._path, 'CGInviteInterviewAjax', 'getPathOfVideoUrl', arguments);
};
