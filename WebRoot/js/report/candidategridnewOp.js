var deviceType=$.browser.device = (/iPhone|iPad|iPod/i.test(navigator.userAgent.toLowerCase()));

var displayStatus="";
//applyJobs
var pageJobDetail = 1;
var sortOrderStrJobDetail="";
var sortOrderTypeJobDetail ="";
var noOfRowsJobDetail=50;
var arrGloble =[];

var pageSS = 1;
var sortOrderStrSS="";
var sortOrderTypeSS ="";
var noOfRowsSS= 10;

var pageforSchool		=	1;
var noOfRowsSchool 		=	50;
var sortOrderStrSchool	=	"";
var sortOrderTypeSchool	=	"";
var schoolIdsStore="";

var page = 1;
var noOfRows = 10;
var sortOrderStr="";
var sortOrderType="";
var txtBgColor="#F5E7E1";

var pageForTC = 1;
var sortOrderStrForTC="";
var sortOrderTypeForTC="";
var noOfRowsForTC = 100;
var teacherId='';
var currentPageFlag="";


var pageMsg = 1;
var sortOrderStrMsg="";
var sortOrderTypeMsg="";
var noOfRowsMsg = 10;

var pagePhone = 1;
var sortOrderStrPhone ="";
var sortOrderTypePhone ="";
var noOfRowsPhone = 10;

var pageSTT = 1;
var sortOrderStrSTT ="";
var sortOrderTypeSTT ="";
var noOfRowsSTT = 10;

var pageUS = 1;
var sortOrderStrUS="";
var sortOrderTypeUS ="";
var noOfRowsUS = 10;

var pageJob = 1;
var sortOrderStrJob="";
var sortOrderTypeJob ="";
var noOfRowsJob= 10;

var pageSNote = 1;
var noOfRowsSNote=10;
var sortOrderStrSNote="";
var sortOrderTypeSNote = 10;

//Start for Profilr Div Grid

//videoLink
var pagevideoLink = 1;
var sortOrderStrvideoLink="";
var sortOrderTypevideoLink ="";
var noOfRowsvideoLink= 10;

//References
var pageReferences = 1;
var sortOrderStrReferences="";
var sortOrderTypeReferences ="";
var noOfRowsReferences= 10;

//workExp
var pageworkExp = 1;
var sortOrderStrworkExp="";
var sortOrderTypeworkExp ="";
var noOfRowsworkExp= 10;

//tpvh - teacherprofilevisithistory
var pagetpvh = 1;
var sortOrderStrtpvh="";
var sortOrderTypetpvh ="";
var noOfRowstpvh= 10;

//teacherAce
var pageteacherAce = 1;
var sortOrderStrteacherAce="";
var sortOrderTypeteacherAce ="";
var noOfRowsteacherAce= 10;

//teacherCerti
var pageteacherCerti = 1;
var sortOrderStrteacherCerti="";
var sortOrderTypeteacherCerti ="";
var noOfRowsteacherCerti= 10;

//for Additional Document
var pageaddDoc= 1;
var noOfRowsaddDoc= 10;
var sortOrderStraddDoc="";
var sortOrderTypeaddDoc="";
//End for Profilr Div Grid

//for language profiency
var pageLang= 1;
var noOfRowsLang= 10;
var sortOrderStrLang="";
var sortOrderTypeLang="";

// For Reference Check

var pageRefChk 				= 	1;
var noOfRowsRefChk			=	10;
var sortOrderStrRefChk		=	"";
var sortOrderTypeRefChk 	= 	10;

//For Assessment
var pageASMT 				= 	1;
var noOfRowsASMT			=	10;
var sortOrderStrASMT		=	"";
var sortOrderTypeASMT 		= 	10;

//for Involvement
var dp_Involvement_Rows=10;
var dp_Involvement_page=1;
var dp_Involvement_sortOrderStr="";
var dp_Involvement_sortOrderType="";

//teacher assessemnt
var pageEJ= 1;
var noOfRowsEJ= 10;
var sortOrderStrEJ="";
var sortOrderTypeEJ="";

//Verify Transcript
var pageForTVerify 			= 	1;
var noOfRowsForTVerify 		= 	10;
var sortOrderStrForTVerify	=	"";
var sortOrderTypeForTVerify	=	"";


var orderColumn = "normScore";
var sortingOrder = "0";

var expanddiv=0;
var totalNoOfRecord=0;
var sectionRecordVlt=0;
var sectionRecordRemove=0;
var sectionRecordInter=0;
var sectionRecordInComp=0;
var sectionRecordWithDr=0;
var sectionRecordHired=0;
var statusTxt='',statusValue='';
var jobForTeacherActionId=0;
var statusPrev="";
var jobForTeacherGId=0;

//for break record by 10
var callNoofRecords=10; //add by ram nath
var callbreakup=true;//add by ram nath

//for smart practices reset.
var spJobforteacherId = "";
var spTeacherId = "";
var spNormscore = "";
var spJobId = "";
var spColorName = "";
var spNoOfRecordCheck = "";
var spEvent = "";

//for assessment reset.
var assessmentJobforteacherId = "";
var assessmentTeacherId = "";
var assessmentNormscore = "";
var assessmentJobId = "";
var assessmentColorName = "";
var assessmentNoOfRecordCheck = "";
var assessmentEvent = "";

function defaultSaveDiv(){
	pageSTT = 1;
	sortOrderStrSTT ="";
	sortOrderTypeSTT ="";
	noOfRowsSTT = 10;
	currentPageFlag="stt";
}
function defaultShareDiv(){
	pageUS = 1;
	sortOrderStrUS="";
	sortOrderTypeUS ="";
	noOfRowsUS = 10;
	currentPageFlag="us";
}
function getPaging(pageno)
{
	if($("#gridNo").val()==2)
	{
		if(pageno!='')
		{
			pageforSchool=pageno;	
		}
		else
		{
			pageforSchool=1;
		}
		
		
		noOfRowsSchool = document.getElementById("pageSize1").value;
		showSchoolList(schoolIdsStore);
		return;
	}	
	
	if(currentPageFlag=="statusNote"){
		if(pageno!='')
		{
			pageSNote=pageno;	
		}
		else
		{
			pageSNote=1;
		}
		noOfRowsSNote = document.getElementById("pageSize3").value;
		var statusId=document.getElementById("statusIdForStatusNote").value;
		var secondaryStatusId=document.getElementById("secondaryStatusIdForStatusNote").value;
		var fitScore=document.getElementById("fitScoreForStatusNote").value;
		jWTeacherStatusNotesOnOpen(fitScore,statusId,secondaryStatusId);
	}else if(currentPageFlag=="job"){
		if(pageno!='')
		{
			pageJob=pageno;	
		}
		else
		{
			pageJob=1;
		}
		noOfRowsForJob = document.getElementById("pageSize1").value;
		getJobOrderList();
	}
	else if(currentPageFlag=="workExp"){
		if(pageno!='')
		{
			pageworkExp=pageno;	
		}
		else
		{
			pageworkExp=1;
		}
		noOfRowsworkExp = document.getElementById("pageSize11").value;
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getPFEmploymentGrid_DivProfile(teacherIdForprofileGrid);
	}
	else if(currentPageFlag=="References"){
		if(pageno!='')
		{
			pageReferences=pageno;	
		}
		else
		{
			pageReferences=1;
		}
		noOfRowsReferences = document.getElementById("pageSize12").value;
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getElectronicReferencesGrid_DivProfile(teacherIdForprofileGrid)
		
	}
	else if(currentPageFlag=="videoLink"){
		if(pageno!='')
		{
			pagevideoLink=pageno;
		}
		else
		{
			pagevideoLink=1;
		}
		noOfRowsvideoLink = document.getElementById("pageSize13").value;
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getVideoLinksGrid_DivProfile(teacherIdForprofileGrid);
	}
	else if(currentPageFlag=="document"){
		if(pageno!='')
		{
			pageaddDoc=pageno;
		}
		else
		{
			pageaddDoc=1;
		}
		noOfRowsaddDoc = document.getElementById("pageSize15").value;
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getadddocumentGrid_DivProfile(teacherIdForprofileGrid);
	}else if(currentPageFlag=="languageProfiency"){
		if(pageno!='')
		{
			pageLang=pageno;
		}
		else
		{
			pageLang=1;
		}
		noOfRowsLang = document.getElementById("pageLangProf").value;
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getlanguage_DivProfile(teacherIdForprofileGrid);
	}
	else if(currentPageFlag=="tpvh"){
		if(pageno!='')
		{
			pagetpvh=pageno;
		}
		else
		{
			pagetpvh=1;
		}
		noOfRowstpvh = document.getElementById("pageSize14").value;
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getTeacherProfileVisitHistoryShow(teacherIdForprofileGrid);
	}
	else if(currentPageFlag=="teacherAce"){
		if(pageno!='')
		{
			pageteacherAce=pageno;
		}
		else
		{
			pageteacherAce=1;
		}
		noOfRowsteacherAce = document.getElementById("pageSize10").value;
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getTeacherAcademicsGrid_DivProfile(teacherIdForprofileGrid);
	}
	else if(currentPageFlag=="teacherCerti"){
		if(pageno!='')
		{
			pageteacherCerti=pageno;
		}
		else
		{
			pageteacherCerti=1;
		}
		noOfRowsteacherCerti = document.getElementById("pageSize9").value;
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getTeacherCertificationsGrid_DivProfile(teacherIdForprofileGrid);
	}
	else if(currentPageFlag=="us"){
		if(pageno!='')
		{
			pageUS=pageno;	
		}
		else
		{
			pageUS=1;
		}
		noOfRowsUS = document.getElementById("pageSize3").value;
		
		var userFPS=document.getElementById("userFPS").value;
		var userFPD=document.getElementById("userFPD").value;
		var userCPS=document.getElementById("userCPS").value;
		var userCPD=document.getElementById("userCPD").value;
		if(userFPS==1){
			searchUser(0);
		}else if(userFPD==1){
			var jftTeacherIdF=document.getElementById("teacherIdFromSharePoPUp").value;
			var jftTxtFlagF=document.getElementById("txtteacherIdShareflagpopover").value;
			displayUsergrid(jftTeacherIdF,jftTxtFlagF);
		}else if(userCPS==1){
			searchUserthroughPopUp(0);
		}else if(userCPD==1){
			var jftTeacherId=document.getElementById("JFTteachetIdFromSharePoPUp").value;
			var jftTxtFlag=document.getElementById("txtflagSharepopover").value;
			displayShareFolder(jftTeacherId,jftTxtFlag);
		}
	}else if(currentPageFlag=="stt"){
		if(pageno!='')
		{
			pageSTT=pageno;	
		}
		else
		{
			pageSTT=1;
		}
		noOfRowsSTT = document.getElementById("pageSize3").value;
		var folderId = document.getElementById("folderId").value;
		var checkboxshowHideFlag= document.getElementById("checkboxshowHideFlag").value;
		var pageFlag= document.getElementById("pageFlag").value;
		if(pageFlag==1)
		{
			folderId = document.getElementById("frame_folderId").value;
		}	
		
		displaySavedCGgridByFolderId(pageFlag,folderId,pageFlag);
	}else if(currentPageFlag=="phone"){
		if(pageno!='')
		{
			pagePhone=pageno;	
		}
		else
		{
			pagePhone=1;
		}
		noOfRowsPhone = document.getElementById("pageSize3").value;
		var teacherIdp = document.getElementById("teacherDetailId").value;
		jobForTeacherGId=document.getElementById("jobForTeacherGId").value;
		getPhoneDetail(teacherIdp,jobForTeacherGId,0);
	}else if(currentPageFlag=="SS"){
		if(pageno!='')
		{
			pageSS=pageno;	
		}
		else
		{
			pageSS=1;
		}
		noOfRowsSS = document.getElementById("pageSize3").value;
		var teacherIdp = document.getElementById("teacherIdForSchoolSelection").value;
		var jobIdp=document.getElementById("jobId").value;
		getSelectedSchoolsList(jobIdp,teacherIdp);
	}else if(currentPageFlag=="msg"){
		if(pageno!='')
		{
			pageMsg=pageno;	
		}
		else
		{
			pageMsg=1;
		}
		noOfRowsMsg = document.getElementById("pageSize3").value;
		var teacherIdp = document.getElementById("teacherDetailId").value;
		var jobIdp=document.getElementById("jobId").value;
		var emailIdp=document.getElementById("emailId").value;
		getMessageDiv(teacherIdp,emailIdp,jobIdp,0);
	}else if(currentPageFlag=="referenceCheck"){
		if(pageno!='')
		{
			pageRefChk=pageno;	
		}
		else
		{
			pageRefChk=1;
		}
		teacherId 		= document.getElementById("teacherIdForReference").value;
		teacherDetails	= document.getElementById("teacherDetailsForReference").value;
		noOfRowsRefChk 	= document.getElementById("pageSize3").value;
		getReferenceCheck(teacherDetails,teacherId);
	}else if(currentPageFlag=="assessmentDetails"){
		if(pageno!='')
		{
			pageASMT=pageno;	
		}
		else
		{
			pageASMT=1;
		}
		teacherId 		= document.getElementById("teacherIdForReference").value;
	//	teacherDetails	= document.getElementById("teacherDetailsForReference").value;
		noOfRowsASMT 	= document.getElementById("pageSize3").value;
		getdistrictAssessmetGrid_DivProfile(teacherId);
	}  
	else if(currentPageFlag=="teacherInvolve"){
		if(pageno!='')
		{
			dp_Involvement_page=pageno;	
		}
		else
		{
			dp_Involvement_page=1;
		}
		teacherId 		= document.getElementById("teacherIdForReference").value;
		dp_Involvement_Rows 	= document.getElementById("pageSize16").value;
		getInvolvementGrid(teacherId);
		//getdistrictAssessmetGrid_DivProfile(teacherId);
	}else if(currentPageFlag=="jobApplyFromCG"){
		if(pageno!='')
		{
			pageJobDetail =pageno;	
		}
		else
		{
			pageJobDetail =1;
		}
		noOfRowsJobDetail 	= document.getElementById("pageSize12").value;
		getJobDetailListFromCG();
	} 
	else {

		if(pageno!='')
		{
			page=pageno;	
		}
		else
		{
			page=1;
		}
	
		noOfRows = document.getElementById("pageSize3").value;
		teacherId = document.getElementById("teacherId").value;
		getNotesDiv(teacherId);
	}
	
}

function getPagingAndSorting(pageno,sortOrder,sortOrderTyp){
	//var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
	if($("#gridNo").val()==2)
	{
		if(pageno!=''){
			pageforSchool=pageno;	
		}else{
			pageforSchool=1;
		}
		sortOrderStrSchool	=	sortOrder;
		sortOrderTypeSchool	=	sortOrderTyp;
		if(document.getElementById("pageSize1")!=null){
			noOfRowsSchool = document.getElementById("pageSize1").value;
		}else{
			noOfRowsSchool=50;
		}
		showSchoolList(schoolIdsStore);
		return;
	}	
	
	if(currentPageFlag=="tran" || currentPageFlag=="cert"){
		if(pageno!=''){
			pageForTC=pageno;	
		}else{
			pageForTC=1;
		}
		sortOrderStrForTC	=	sortOrder;
		sortOrderTypeForTC	=	sortOrderTyp;
		if(document.getElementById("pageSize3")!=null){
			noOfRowsTranCert = document.getElementById("pageSize3").value;
		}else{
			noOfRowsTranCert=100;
		}
		if(currentPageFlag=="tran"){
			getTranscriptGridNew();
		}else{
			getCertificationGridNew();
		}
	}else if(currentPageFlag=="SS"){
		if(pageno!=''){
			pageSS=pageno;	
		}else{
			pageSS=1;
		}
		sortOrderStrSS	=	sortOrder;
		sortOrderTypeSS	=	sortOrderTyp;
		if(document.getElementById("pageSize3")!=null){
			noOfRowsSS = document.getElementById("pageSize3").value;
		}else{
			noOfRowsSS=10;
		}
		var teacherIdp = document.getElementById("teacherIdForSchoolSelection").value;
		var jobIdp=document.getElementById("jobId").value;
		getSelectedSchoolsList(jobIdp,teacherIdp);
	}else if(currentPageFlag=="msg"){
		if(pageno!=''){
			pageMsg=pageno;	
		}else{
			pageMsg=1;
		}
		sortOrderStrMsg	=	sortOrder;
		sortOrderTypeMsg	=	sortOrderTyp;
		if(document.getElementById("pageSize3")!=null){
			noOfRowsMsg = document.getElementById("pageSize3").value;
		}else{
			noOfRowsMsg=10;
		}
		var teacherIdp = document.getElementById("teacherDetailId").value;
		var jobIdp=document.getElementById("jobId").value;
		var emailIdp=document.getElementById("emailId").value;
		getMessageDiv(teacherIdp,emailIdp,jobIdp,0);
	}else if(currentPageFlag=="phone"){
		if(pageno!=''){
			pagePhone=pageno;	
		}else{
			pagePhone=1;
		}
		sortOrderStrPhone	=	sortOrder;
		sortOrderTypePhone	=	sortOrderTyp;
		if(document.getElementById("pageSize3")!=null){
			noOfRowsPhone = document.getElementById("pageSize3").value;
		}else{
			noOfRowsPhone=10;
		}
		var teacherIdp = document.getElementById("teacherDetailId").value;
		jobForTeacherGId=document.getElementById("jobForTeacherGId").value;
		getPhoneDetail(teacherIdp,jobForTeacherGId,0);
	}else if(currentPageFlag=="stt"){
		if(pageno!=''){
			pageSTT=pageno;	
		}else{
			pageSTT=1;
		}
		sortOrderStrSTT	=	sortOrder;
		sortOrderTypeSTT	=	sortOrderTyp;
		if(document.getElementById("pageSize3")!=null){
			noOfRowsSTT = document.getElementById("pageSize3").value;
		}else{
			noOfRowsSTT=10;
		}
		var folderId = document.getElementById("folderId").value;
		var checkboxshowHideFlag= document.getElementById("checkboxshowHideFlag").value;
		var pageFlag= document.getElementById("pageFlag").value;
		if(pageFlag==1)
		{
			folderId = document.getElementById("frame_folderId").value;
		}	
		
		//alert(" folderId "+folderId+" pageFlag "+pageFlag);
		displaySavedCGgridByFolderId(pageFlag,folderId,pageFlag);
	}else if(currentPageFlag=="us"){
		if(pageno!=''){
			pageUS=pageno;	
		}else{
			pageUS=1;
		}
		sortOrderStrUS	=	sortOrder;
		sortOrderTypeUS	=	sortOrderTyp;
		if(document.getElementById("pageSize3")!=null){
			noOfRowsUS = document.getElementById("pageSize3").value;
		}else{
			noOfRowsUS=10;
		}
		var userFPS=document.getElementById("userFPS").value;
		var userFPD=document.getElementById("userFPD").value;
		var userCPS=document.getElementById("userCPS").value;
		var userCPD=document.getElementById("userCPD").value;
		
		if(userFPS==1){
			searchUser(0);
		}else if(userFPD==1){
			var jftTeacherIdF=document.getElementById("teacherIdFromSharePoPUp").value;
			var jftTxtFlagF=document.getElementById("txtteacherIdShareflagpopover").value;
			displayUsergrid(jftTeacherIdF,jftTxtFlagF);
		}else if(userCPS==1){
			searchUserthroughPopUp(0);
		}else if(userCPD==1){
			var jftTeacherId=document.getElementById("JFTteachetIdFromSharePoPUp").value;
			var jftTxtFlag=document.getElementById("txtflagSharepopover").value;
			displayShareFolder(jftTeacherId,jftTxtFlag);
		}
	}else if(currentPageFlag=="job"){
		if(pageno!=''){
			pageJob=pageno;	
		}else{
			pageJob=1;
		}
		sortOrderStrJob	=	sortOrder;
		sortOrderTypeJob	=	sortOrderTyp;
		if(document.getElementById("pageSize1")!=null){
			noOfRowsJob = document.getElementById("pageSize1").value;
		}else{
			noOfRowsJob=10;
		}
		
		getJobOrderList();
	}else if(currentPageFlag=="statusNote"){
		if(pageno!=''){
			pageSNote=pageno;	
		}else{
			pageSNote=1;
		}
		sortOrderStrSNote=	sortOrder;
		sortOrderTypeSNote	=	sortOrderTyp;
		if(document.getElementById("pageSize3")!=null){
			noOfRowsJobSNote = document.getElementById("pageSize3").value;
		}else{
			noOfRowsJobSNote=10;
		}
		var statusId=document.getElementById("statusIdForStatusNote").value;
		var secondaryStatusId=document.getElementById("secondaryStatusIdForStatusNote").value;
		var fitScore=document.getElementById("fitScoreForStatusNote").value;
		jWTeacherStatusNotesOnOpen(fitScore,statusId,secondaryStatusId);

	}else if(currentPageFlag=="workExp"){
		if(pageno!=''){
			pageworkExp=pageno;	
		}else{
			pageworkExp=1;
		}
		sortOrderStrworkExp	=	sortOrder;
		sortOrderTypeworkExp	=	sortOrderTyp;
		if(document.getElementById("pageSize11")!=null){
			noOfRowsworkExp = document.getElementById("pageSize11").value;
		}else{
			noOfRowsworkExp=10;
		}
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getPFEmploymentGrid_DivProfile(teacherIdForprofileGrid);
	}else if(currentPageFlag=="References"){
		if(pageno!=''){
			pageReferences=pageno;	
		}else{
			pageReferences=1;
		}
		sortOrderStrReferences	=	sortOrder;
		sortOrderTypeReferences	=	sortOrderTyp;
		if(document.getElementById("pageSize12")!=null){
			noOfRowsReferences = document.getElementById("pageSize12").value;
		}else{
			noOfRowsReferences=10;
		}
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getElectronicReferencesGrid_DivProfile(teacherIdForprofileGrid);
	}else if(currentPageFlag=="videoLink"){
		if(pageno!=''){
			pagevideoLink=pageno;	
		}else{
			pagevideoLink=1;
		}
		sortOrderStrvideoLink	=	sortOrder;
		sortOrderTypevideoLink	=	sortOrderTyp;
		if(document.getElementById("pageSize13")!=null){
			noOfRowsvideoLink = document.getElementById("pageSize13").value;
		}else{
			noOfRowsvideoLink=10;
		}
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getVideoLinksGrid_DivProfile(teacherIdForprofileGrid);
	}
	else if(currentPageFlag=="document"){
		if(pageno!=''){
			pageaddDoc=pageno;	
		}else{
			pageaddDoc=1;
		}
		sortOrderStraddDoc	=	sortOrder;
		sortOrderTypeaddDoc	=	sortOrderTyp;
		if(document.getElementById("pageSize15")!=null){
			noOfRowsaddDoc = document.getElementById("pageSize15").value;
		}else{
			noOfRowsaddDoc=10;
		}
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;		
		getadddocumentGrid_DivProfile(teacherIdForprofileGrid);
	}else if(currentPageFlag=="languageProfiency"){
		if(pageno!=''){
			pageLang=pageno;	
		}else{
			pageLang=1;
		}
		sortOrderStrLang	=	sortOrder;
		sortOrderTypeLang	=	sortOrderTyp;
		if(document.getElementById("pageLangProf")!=null){
			noOfRowsLang = document.getElementById("pageLangProf").value;
		}else{
			noOfRowsLang=10;
		}
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;		
		getlanguage_DivProfile(teacherIdForprofileGrid);
	}else if(currentPageFlag=="tpvh"){
		if(pageno!=''){
			pagetpvh=pageno;	
		}else{
			pagetpvh=1;
		}
		sortOrderStrtpvh	=	sortOrder;
		sortOrderTypetpvh	=	sortOrderTyp;
		if(document.getElementById("pageSize14")!=null){
			noOfRowstpvh = document.getElementById("pageSize14").value;
		}else{
			noOfRowstpvh=10;
		}
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getTeacherProfileVisitHistoryShow(teacherIdForprofileGrid);
	}else if(currentPageFlag=="teacherAce"){
		if(pageno!=''){
			pageteacherAce=pageno;	
		}else{
			pageteacherAce=1;
		}
		sortOrderStrteacherAce	=	sortOrder;
		sortOrderTypeteacherAce	=	sortOrderTyp;
		if(document.getElementById("pageSize10")!=null){
			noOfRowsteacherAce = document.getElementById("pageSize10").value;
		}else{
			noOfRowsteacherAce=10;
		}
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getTeacherAcademicsGrid_DivProfile(teacherIdForprofileGrid);
	}else if(currentPageFlag=="teacherCerti"){
		if(pageno!=''){
			pageteacherCerti=pageno;	
		}else{
			pageteacherCerti=1;
		}
		sortOrderStrteacherCerti	=	sortOrder;
		sortOrderTypeteacherCerti	=	sortOrderTyp;
		if(document.getElementById("pageSize10")!=null){
			noOfRowsteacherCerti = document.getElementById("pageSize9").value;
		}else{
			noOfRowsteacherCerti=10;
		}
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getTeacherCertificationsGrid_DivProfile(teacherIdForprofileGrid);
	}else if(currentPageFlag=="assessmentDetails"){
		if(pageno!=''){
			pageASMT=pageno;	
		}else{
			pageASMT=1;
		}
		sortOrderStrASMT	=	sortOrder;
		sortOrderTypeASMT	=	sortOrderTyp;
		if(document.getElementById("pageSize13")!=null){
			noOfRowsASMT = document.getElementById("pageSize3").value;
		}else{
			noOfRowsASMT=10;
		}
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getdistrictAssessmetGrid_DivProfile(teacherIdForprofileGrid);
	}
	else if(currentPageFlag=="teacherInvolve"){
		if(pageno!=''){
			dp_Involvement_page=pageno;	
		}else{
			dp_Involvement_page=1;
		}
		dp_Involvement_sortOrderStr	=	sortOrder;
		dp_Involvement_sortOrderType	=	sortOrderTyp;
		if(document.getElementById("pageSize16")!=null){
			dp_Involvement_Rows = document.getElementById("pageSize16").value;
		}else{
			dp_Involvement_Rows=10;
		}
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getInvolvementGrid(teacherIdForprofileGrid);
		//getdistrictAssessmetGrid_DivProfile(teacherIdForprofileGrid);
	}else if(currentPageFlag=="jobApplyFromCG"){
		if(pageno!=''){
			pageJobDetail =pageno;	
		}else{
			pageJobDetail =1;
		}
		sortOrderStrJobDetail	=	sortOrder;
		sortOrderTypeJobDetail 	=	sortOrderTyp;
		if(document.getElementById("pageSize13")!=null){
			noOfRowsJobDetail = document.getElementById("pageSize12").value;
		}else{
			noOfRowsJobDetail=50;
		}
		getJobDetailListFromCG();
	}
	else if(currentPageFlag=="teacherAssessment"){
		if(pageno!=''){
			pageEJ=pageno;	
		}else{
			pageEJ=1;
		}
		sortOrderStrEJ=sortOrder;
		sortOrderTypeEJ=sortOrderTyp;
		if(document.getElementById("pageSize")!=null){
			noOfRowsEJ = document.getElementById("pageSize").value;
		}else{
			noOfRowsEJ=10;
		}
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getTeacherAssessmentGrid_DivProfile(teacherIdForprofileGrid);
	}
	else
		{
		if(pageno!=''){
			page=pageno;	
		}else{
			page=1;
		}
		sortOrderStr	=	sortOrder;
		sortOrderType	=	sortOrderTyp;
		if(document.getElementById("pageSize3")!=null){
			noOfRows = document.getElementById("pageSize3").value;
		}else{
			noOfRows=10;
		}
		var teacherIdp =document.getElementById("teacherId").value;
		getNotesDiv(teacherIdp);
	}
}
function handleError(message, exception)
{
	/*if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert("Oops: Your Session has expired!");  document.location = 'signin.do';}
	else{alert("Server Error: "+exception.javaClassName);}*/
}

function orderingCG(oColumn, sOrder){
	orderColumn = oColumn;
	sortingOrder = sOrder;
	getCandidateGrid();
	refreshStatus();
}
function checkValue(val)
{  
	document.getElementById("contacted").value=val;
}
function checkStatusValue(val)
{  
	document.getElementById("candidateStatus").value=val;
}


function getCandidateGridLoad()
{
	arrTagCall =[];
	var callbreakup=false;
	var callNoofRecords=10;
	$('#loadingDiv').show();
	var jobId=document.getElementById("jobId").value;
	CandidateGridAjaxNew.getCandidateReportGrid(jobId,"","","","0","0","0","0","5","","","5","0","0","0",orderColumn, sortingOrder,callbreakup,callNoofRecords,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#loadingDiv').hide();
			try{
				$('#myModalSearch').modal("hide");
			}catch(err)
			  {}
			
			document.getElementById("divReportGrid").innerHTML=data;
			tpJbIDisable();
			tpJbIEnable();
			displayTotalCGRecord(1,0);
			var txtgridloadbreakup=document.getElementById("txtgridloadbreakup").value;
			if(txtgridloadbreakup=="yes")
			{
				var txtjftcount=document.getElementById("txtjftcount").value;
				var txtgridloadInterval=document.getElementById("txtgridloadInterval").value;
				var txtloopCallTimes=document.getElementById("txtloopCallTimes").value;
				var totjft = parseInt(txtjftcount);
				var gridInterval = parseInt(txtgridloadInterval);
				var loopCallTimes = parseInt(txtloopCallTimes);
				
				for(var i=0; i < loopCallTimes-1; i++)
				{
					var lasttrId="#avltr_"+callNoofRecords;
					var sessionList="lstAbvGrdData";//add by ram nath
					CGServiceAjax.getBreakUpData(i,sessionList, { 
						async: true,
						errorHandler:handleError,
						callback:function(data)
						{
							$(lasttrId).after(data);
							
							if(loopCallTimes-1==i)
							{
								tpJbIDisable();
							}
						}
					});	
				}
			}
		}});
	
}


function getBranchContact(){
	//alert("in the function getBranchContact(tchrfulName,tchrId){");
	var jobid="";
	var teacherId="";
	var counter=0;
	refreshStatus();
	jQuery("input[name='cgCBX']").each(function(){
		if(jQuery(this).is(":checked")){
			teacherId+="~"+jQuery(this).attr('teacherId');
			jobid+="~"+jQuery(this).attr('jobidd');
			counter++;
		}
	});
	//alert("teacherId  "+teacherId+"   jobid    "+jobid);
	if(counter!=0){
	CandidateGridAjaxNew.updateBranchContact(teacherId,jobid,1, { 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
		getCandidateGrid();
		}
	});	
	}
}

function getNoBranchContact(){
	//alert("in function getNoBranchContact(tchrfulName,tchrId){");
	var jobid="";
	var teacherId="";
	var counter=0;
	refreshStatus();
	
	jQuery("input[name='cgCBX']").each(function(){
		if(jQuery(this).is(":checked")){
			teacherId+="~"+jQuery(this).attr('teacherId');
			jobid+="~"+jQuery(this).attr('jobidd');
			counter++;
		}
	});
	//alert("teacherId  "+teacherId+"   jobid    "+jobid);
	if(counter!=0){
	CandidateGridAjaxNew.updateBranchContact(teacherId,jobid,0, { 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
		getCandidateGrid();
		}
	});	
	}
	
}

function getCandidateGrid()
{
	$('#divReportGrid').html("<table align='center' id='loaddiv'><tr><td style='padding-top: 10px;' align='center'><!--<img src='images/please.jpg'/>--></td></tr><tr><td style='padding-top: 0px;' align='center'><img src='images/loadingAnimation.gif'/></td></tr></table>");
	
	var chkstr="";
	jQuery("input[name='cgCBX']").each(function(){
		if(jQuery(this).is(":checked"))
		chkstr += "~"+jQuery(this).val();
	});
	
	arrTagCall =[];
	// by alok for dynamic div width
   try
   {
	   var mydiv = document.getElementById("mydiv");
       var curr_width = parseInt(mydiv.style.width);
      
       if(deviceType)
       {
    	   mydiv.style.width = 630+"px";  
       }
       else
	   {
    	   mydiv.style.width = 647+"px";  
	   } 
   }
   catch(e){alert(e)}
   /*.........................................*/
    //alert("1");
	$('#loadingDiv').show();
   
	//$("#loadingDiv").modal('show');
	//alert("1");
	var jobId=document.getElementById("jobId").value;
	var firstName		=	trim(document.getElementById("firstName").value); 
	var lastName		=	trim(document.getElementById("lastName").value);
	var emailAddress	=	trim(document.getElementById("emailAddress").value);
	var certType		=	trim(document.getElementById("certificateTypeMaster").value);
	var regionId		=	trim(document.getElementById("regionId").value);
	var degreeId		=	trim(document.getElementById("degreeId").value);
	var universityId		=	trim(document.getElementById("universityId").value);
	var normScoreSelectVal		=	trim(document.getElementById("normScoreSelectVal").value);
	var CGPASelectVal		=	trim(document.getElementById("CGPASelectVal").value);
	var contacted		=	document.getElementById("contacted").value;
	var candidateStatus		=	trim(document.getElementById("candidateStatus").value);
	var stateId=document.getElementById("stateId").value;
	
	try{
		var ssn	= trim(document.getElementById("ssn").value);
		var notReviewedFlag	= document.getElementById("notReviewedFlag").checked;
	}catch(err){}
	
	try{
		var iframeNorm = document.getElementById('ifrmNorm');
		var innerNorm = iframeNorm.contentDocument || iframeNorm.contentWindow.document;
		var inputNormScore = innerNorm.getElementById('normScoreFrm');
		document.getElementById("normScore").value=inputNormScore.value;
	}catch(e){}	
	try{
		var iframeCGPA = document.getElementById('ifrmCGPA');
		var innerCGPA = iframeCGPA.contentDocument || iframeCGPA.contentWindow.document;
		var inputCGPA = innerCGPA.getElementById('CGPAFrm');
		document.getElementById("CGPA").value=inputCGPA.value;
	}catch(e){}
	var normScore		=	trim(document.getElementById("normScore").value);
	var CGPA		=	trim(document.getElementById("CGPA").value);
	
	var tagsearchId		=	trim(document.getElementById("tagsearchId").value);
	
	try{
		var ifrmYOTE = document.getElementById('ifrmYOTE');
		var innerYOTE = ifrmYOTE.contentDocument || ifrmYOTE.contentWindow.document;
		var inputYOTE = innerYOTE.getElementById('YOTEFrm');
		document.getElementById("YOTE").value=inputYOTE.value;
	}catch(e){}
	var YOTESelectVal		=	trim(document.getElementById("YOTESelectVal").value);
	var YOTE				=	document.getElementById("YOTE").value;
	var hiddenCertTypeIds 	=	document.getElementsByName("hiddenCertTypeId");
	var stateId2	=	trim(document.getElementById("stateId2").value);
    var zipCode		=	trim(document.getElementById("zipCode").value);
	var certIds =[];
	for(var i=0;i<hiddenCertTypeIds.length;i++)
	{
		certIds.push(hiddenCertTypeIds[i].value);
	}
	var epiFromDate = "";
	var epiToDate = "";
     try{
	var epiFromDate	=	trim(document.getElementById("epiFromDate").value);
    var epiToDate	=	trim(document.getElementById("epiToDate").value);
     }catch(err){}
	
	CandidateGridAjaxNew.getCandidateReportGrid(jobId,firstName,lastName,emailAddress,certType,regionId,degreeId,universityId,normScoreSelectVal,
			normScore,CGPA,CGPASelectVal,contacted,candidateStatus,stateId,orderColumn, sortingOrder,callbreakup,callNoofRecords,tagsearchId,
			certIds,YOTESelectVal,YOTE,stateId2,zipCode,epiFromDate,epiToDate,chkstr,ssn,notReviewedFlag,{
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			/*$('#loadingDiv').hide();
			try{
				$('#myModalSearch').modal("hide");
			}catch(err)
			  {}
			
			document.getElementById("divReportGrid").innerHTML=data;
			tpJbIDisable();
			tpJbIEnable();
			displayTotalCGRecord(1,0);
			*/
		
		$('#loadingDiv').hide();
		try{
			$('#myModalSearch').modal("hide");
		}catch(err)
		  {}
	 
		document.getElementById("divReportGrid").innerHTML=data;
		
		try{
			var totalNoOfRecordVal=document.getElementById("totalNoOfRecord").value;
			document.getElementById("noOfApp1").innerHTML=totalNoOfRecordVal;
			getHiredCandidateReports();
		}catch(err)
		  {}
		
		tpJbIDisable();
		tpJbIEnable();
		displayTotalCGRecord(1,0);
		var txtgridloadbreakup="";
		try{
			txtgridloadbreakup=document.getElementById("txtgridloadbreakup").value;
		}catch(err){}
		if(txtgridloadbreakup=="yes")
		{
			var txtjftcount=document.getElementById("txtjftcount").value;
			var txtgridloadInterval=document.getElementById("txtgridloadInterval").value;
			var txtloopCallTimes=document.getElementById("txtloopCallTimes").value;
			var totjft = parseInt(txtjftcount);
			var gridInterval = parseInt(txtgridloadInterval);
			var loopCallTimes = parseInt(txtloopCallTimes);
			
			var table="divReportGrid";//add by Ram Nath
			getBreakUpRecords(table,(loopCallTimes-1),'avltr_',$("[name='sessionListNameAvl"+jobId+"']").val(),0); //add by Ram Nath
			/*for(var i=0; i < loopCallTimes-1; i++)
			{
				var lasttrId="#avltr_"+callNoofRecords;
				
				CGServiceAjax.getBreakUpData(i,document.getElementById("sessionListNameAvl").value, { 
					async: true,
					errorHandler:handleError,
					callback:function(data)
					{
							$(lasttrId).after(data);	
						if(loopCallTimes-1==i)
						{
							tpJbIDisable();
						}
					}
				});	
				
			}*/
		}
	
			
			
		}});
}

function displayTotalCGRecord(sectionFlag,minusNo)
{
	if(sectionFlag==1){
		totalNoOfRecord=document.getElementById('totalNoOfRecord').value;
	}else if(sectionFlag==2){
		sectionRecordInter=document.getElementById('sectionRecordInter').value;
		if(minusNo==0){
			totalNoOfRecord=document.getElementById('totalNoOfRecordInter').value;
		}else{
			totalNoOfRecord-=sectionRecordInter;
		}
	}else if(sectionFlag==3){
		sectionRecordVlt=document.getElementById('sectionRecordVlt').value;
		if(minusNo==0){
			totalNoOfRecord=document.getElementById('totalNoOfRecordVlt').value;
		}else{
			totalNoOfRecord-=sectionRecordVlt;
		}
	}else if(sectionFlag==4){
		sectionRecordRemove=document.getElementById('sectionRecordRemove').value;
		if(minusNo==0){
			totalNoOfRecord=document.getElementById('totalNoOfRecordRemove').value;
		}else{
			totalNoOfRecord-=sectionRecordRemove;
		}
	}else if(sectionFlag==5){
		sectionRecordInComp=document.getElementById('sectionRecordInComp').value;
		if(minusNo==0){
			totalNoOfRecord=document.getElementById('totalNoOfRecordInComp').value;
		}else{
			totalNoOfRecord-=sectionRecordInComp;
		}
	}else if(sectionFlag==6){
		sectionRecordWithDr=document.getElementById('sectionRecordWithDr').value;
		if(minusNo==0){
			totalNoOfRecord=document.getElementById('totalNoOfRecordWithDr').value;
		}else{
			totalNoOfRecord-=sectionRecordWithDr;
		}
	}else if(sectionFlag==7){
		sectionRecordHired=document.getElementById('sectionRecordHired').value;
		if(minusNo==0){
			totalNoOfRecord=document.getElementById('totalNoOfRecordHired').value;
		}else{
			totalNoOfRecord-=sectionRecordHired;
		}
	}
	else if(sectionFlag==8){
		sectionRecordHidden=document.getElementById('sectionRecordHidden').value;
		if(minusNo==0){
			totalNoOfRecord=document.getElementById('totalNoOfRecordHidden').value;
		}else{
			totalNoOfRecord-=sectionRecordHidden;
		}
	}
}
function hireTeacher(assessmentStatusId, jobForTeacherId)
{	
	CandidateGridAjaxNew.hireTeacher(assessmentStatusId, jobForTeacherId, { 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			if(data)
			{
				//$('#myModalAct').modal('hide');
				try{
					$('#myModalAct').modal('hide');
				}catch(err)
				  {}
				getCandidateGrid();				
			}
			else
			{
				$("#myModalAct").css({"z-index":"1000"});
				document.getElementById("divAlertText").innerHTML="You can not hire the Candidate.";
				//$('#divAlert').modal('show');
				try{
					$('#divAlert').modal('show');
				}catch(err)
				  {}
				
			}			
		}});	
}
function unHireTeacher(assessmentStatusId, jobForTeacherId)
{	
	CandidateGridAjaxNew.unHireTeacher(assessmentStatusId, jobForTeacherId,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			if(data)
			{
				//$('#myModalAct').modal('hide');
				try{
					$('#myModalAct').modal('hide');
				}catch(err)
				  {}
				getCandidateGrid();				
			}
			else
			{
				//alert("You can not unhire the Candidate.")				
				$("#myModalAct").css({"z-index":"1000"});
				document.getElementById("divAlertText").innerHTML=""+resourceJSON.MsgCannotUnHired+".";
				//$('#divAlert').modal('show');
				try{
					$('#divAlert').modal('show');
				}catch(err)
				  {}
			}		
	}});
}
function removeTeacher(assessmentStatusId, jobForTeacherId)
{
	var divMsg="You have not completed";
	var divMsg1="You have not completed....";	
	//$('#myModalAct').modal('hide');
	//$("#myModalAct").css({"z-index":"1000"});
	//$('#myConfirm').modal('show');
	
	try{
		$('#myModalAct').modal('hide');
		$("#myModalAct").css({"z-index":"1000"});
		$('#myConfirm').modal('show');
	}catch(err)
	  {}
	
	document.getElementById("spnRemoveTeacher").innerHTML="<button class='btn btn-large btn-primary' onclick=\"removeTeacherAfterConfirm('"+assessmentStatusId+"','"+jobForTeacherId+"')\" >"+resourceJSON.btnOk+" <i class='icon'></i></button>";
}

function setZIndexActDiv()
{
	//$('#myConfirm').modal('hide');
	//$("#myModalAct").css({"z-index":"3000"});
	//$('#myModalAct').modal('show');
	
	try{
		$('#myConfirm').modal('hide');
		$("#myModalAct").css({"z-index":"3000"});
		$('#myModalAct').modal('show');
	}catch(err)
	  {}
}

function setZIndexActOtherDiv()
{
	$("#myModalAct").css({"z-index":"3000"});
}

function removeTeacherAfterConfirm(assessmentStatusId, jobForTeacherId){
	
	CandidateGridAjaxNew.removeTeacher(assessmentStatusId, jobForTeacherId, { 
	async: true,
	errorHandler:handleError,
	callback:function(data)
	{
		//$('#myConfirm').modal('hide');
		//$('#myModalAct').modal('hide');
		try{
			$('#myConfirm').modal('hide');
			$('#myModalAct').modal('hide');
		}catch(err)
		  {}
		getCandidateGrid();			
		
	}});	
}
function test(){
	
}
function downloadCandidateGridReport()
{
	$('#loadingDiv').show();
	
	var jobId=document.getElementById("jobId").value;
	var firstName		=	trim(document.getElementById("firstName").value); 
	var lastName		=	trim(document.getElementById("lastName").value);
	var emailAddress	=	trim(document.getElementById("emailAddress").value);
	var certType		=	trim(document.getElementById("certificateTypeMaster").value);
	var regionId		=	trim(document.getElementById("regionId").value);
	var degreeId		=	trim(document.getElementById("degreeId").value);
	var universityId		=	trim(document.getElementById("universityId").value);
	var normScoreSelectVal		=	trim(document.getElementById("normScoreSelectVal").value);
	var normScore		=	trim(document.getElementById("normScore").value);
	var CGPA		=	trim(document.getElementById("CGPA").value);
	var CGPASelectVal		=	trim(document.getElementById("CGPASelectVal").value);
	var contacted		=	trim(document.getElementById("contacted").value);
	var candidateStatus		=	trim(document.getElementById("candidateStatus").value);
	var stateId=document.getElementById("stateId").value;
	try{
		var ifrmYOTE = document.getElementById('ifrmYOTE');
		var innerYOTE = ifrmYOTE.contentDocument || ifrmYOTE.contentWindow.document;
		var inputYOTE = innerYOTE.getElementById('YOTEFrm');
		document.getElementById("YOTE").value=inputYOTE.value;
	}catch(e){}
	var YOTESelectVal		=	trim(document.getElementById("YOTESelectVal").value);
	var YOTE				=	document.getElementById("YOTE").value;
	var hiddenCertTypeIds 	=	document.getElementsByName("hiddenCertTypeId");
	var stateId2	=	trim(document.getElementById("stateId2").value);
    var zipCode		=	trim(document.getElementById("zipCode").value);
	var certIds =[];
	for(var i=0;i<hiddenCertTypeIds.length;i++)
	{
		certIds.push(hiddenCertTypeIds[i].value);
	}
	var epiFromDate = "";
	var epiToDate = "";
     try{
	var epiFromDate	=	trim(document.getElementById("epiFromDate").value);
    var epiToDate	=	trim(document.getElementById("epiToDate").value);
     }catch(err){}
	
	CandidateGridAjaxNew.downloadCandidateGridReport(jobId,firstName,lastName,emailAddress,certType,regionId,degreeId,universityId,normScoreSelectVal,
	normScore,CGPA,CGPASelectVal,contacted,candidateStatus,stateId,orderColumn, sortingOrder,totalNoOfRecord,false,null,certIds,YOTESelectVal,YOTE,stateId2,zipCode,epiFromDate,epiToDate,{
	async: true,
		errorHandler:handleError,
		callback:function(data)
		{	
			$('#loadingDiv').hide();
			//window.open(data);
			//openWindow(data,'Support',650,490);
			
			$('#loadingDiv').hide();
			if(deviceType)
			{
				if (data.indexOf(".doc")!=-1)
				{
					$("#docfileNotOpen").css({"z-index":"3000"});
			    	$('#docfileNotOpen').show();
				}
				else if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					$("#exelfileNotOpen").css({"z-index":"3000"});
			    	$('#exelfileNotOpen').show();
				}
				else
				{
					$('#modalDownloadCGR').modal('hide');
					document.getElementById(hrefId).href=data;				
				}
			}
			else if(deviceTypeAndroid)
			{
				if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					  try{
					    $('#modalDownloadCGR').modal('hide');
						}catch(err)
						  {}
					    document.getElementById('ifrmCGR').src = ""+data+"";
				}
				else
				{
					$('#modalDownloadCGR').modal('hide');
					document.getElementById(hrefId).href=data;					
				}				
			}
			else if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
			{
				  try{
				    	$('#modalDownloadCGR').modal('hide');
					}catch(err)
					  {}
				    document.getElementById('ifrmCGR').src = ""+data+"";
			}
			else
			{
				$('#modalDownloadCGR').modal('hide');
				document.getElementById('ifrmCGR').src = ""+data+"";
				try{
					$('#modalDownloadCGR').modal('show');
				}catch(err)
				  {}
				
			}		
			
			return false;
		}});
}

function downloadSLCReport()
{
	//alert("downloadSLCReport");
	
	$('#loadingDiv').show();
	
	var statusName = document.getElementById("statusName").value;
	var jobForTeacherId =document.getElementById("jobForTeacherIdForSNote").value;
	var statusId=document.getElementById("statusIdForStatusNote").value;
	var secondaryStatusId=document.getElementById("secondaryStatusIdForStatusNote").value;
	
	//alert("jobForTeacherId "+jobForTeacherId +" statusId "+ statusId + " secondaryStatusId "+secondaryStatusId);
	
	CandidateGridAjaxNew.downloadSLCReport(jobForTeacherId,statusId,secondaryStatusId,{
	async: true,
		errorHandler:handleError,
		callback:function(data)
		{	
			$('#loadingDiv').hide();
			
			document.getElementById("myModalLabelSLCNotes").innerHTML="SLC "+statusName+" Notes";
			
			$('#loadingDiv').hide();
			if(deviceType)
			{
				if (data.indexOf(".doc")!=-1)
				{
					$("#docfileNotOpen").css({"z-index":"3000"});
			    	$('#docfileNotOpen').show();
				}
				else if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					$("#exelfileNotOpen").css({"z-index":"3000"});
			    	$('#exelfileNotOpen').show();
				}
				else
				{
					$('#modalDownloadCGSLCReport').modal('hide');
					document.getElementById(hrefId).href=data;				
				}
			}
			else if(deviceTypeAndroid)
			{
				if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					  try{
					    $('#modalDownloadCGSLCReport').modal('hide');
						}catch(err)
						  {}
					    document.getElementById('ifrmCGSLCR').src = ""+data+"";
				}
				else
				{
					$('#modalDownloadCGSLCReport').modal('hide');
					document.getElementById(hrefId).href=data;					
				}				
			}
			else if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
			{
				  try{
				    	$('#modalDownloadCGSLCReport').modal('hide');
					}catch(err)
					  {}
				    document.getElementById('ifrmCGSLCR').src = ""+data+"";
			}
			else
			{
				$('#modalDownloadCGSLCReport').modal('hide');
				document.getElementById('ifrmCGSLCR').src = ""+data+"";
				try{
					$('#modalDownloadCGSLCReport').modal('show');
				}catch(err)
				  {}
				
			}		
			
			return false;
		}});
}

function downloadResume(teacherId,linkId)
{	
	$('#loadingDiv').show();
	CandidateGridAjaxNew.downloadResume(teacherId,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{			
			$('#loadingDiv').hide();
			if(data=="")	
			{
				alert(resourceJSON.ResumeNotUploaded)
			}
			else
			{
				if(deviceType)
				{
					if (data.indexOf(".doc")!=-1)
					{
						$("#docfileNotOpen").css({"z-index":"3000"});
				    	$('#docfileNotOpen').show();
					}
					else if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
					{
						$("#exelfileNotOpen").css({"z-index":"3000"});
				    	$('#exelfileNotOpen').show();
					}
					else
					{
						document.getElementById(linkId).href = data; 	
					}
				}
				else if(deviceTypeAndroid)
				{
					if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
					{
						document.getElementById('ifrmTrans').src = ""+data+"";
					}
					else
					{
						document.getElementById(linkId).href = data; 		
					}
				}
				else if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					document.getElementById('ifrmTrans').src = ""+data+"";
				}
				else
				{
					document.getElementById(linkId).href = data; 		
				}			
			}	
			return false;			
							
		}
	});
}
function hideDiv()
{
	$('#docfileNotOpen').hide();
	$('#exelfileNotOpen').hide();
}

function downloadPortfolioReport(teacherId, linkId)
{	
	$('#loadingDiv').show();
	CandidateGridAjaxNew.downloadPortfolioReport(teacherId,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#loadingDiv').hide();
			if(data=="")
			{
					document.getElementById(linkId).href = "javascript:void(0)";
					alert(resourceJSON.MsgPortfolioNotCompleted)
					return true;
			}
			else	
			{
				
				//window.location.href=data;
  				//window.focus();
				document.getElementById(linkId).href = data; 
				return false;
			}
				
		}
	});
}
function getNotesDiv(teacherId,jobId)
{
	removeNotesFile();
	hideCommunicationsDiv();
	currentPageFlag='notes';
	document.getElementById("teacherId").value=teacherId;
	document.getElementById("teacherIdForNote").value=teacherId;
	$('#divTxtNode').find(".jqte_editor").html("");		
	//$('#myModalNotes').modal('show');
	try{
		$('#myModalNotes').modal('show');
	}catch(err)
	  {}
	$('#divTxtNode').find(".jqte_editor").css("background-color", "");
	document.getElementById("noteId").value="";
	$('#errordivNotes').empty();
	CandidateGridAjaxNew.getNotesDetail(teacherId,page,noOfRows,sortOrderStr,sortOrderType,
	{
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{	
			try{
				document.getElementById("divNotes").innerHTML=data;
			}catch(err)
			  {}
			//document.getElementById("divNotes").innerHTML=data;
			var pageFlag= document.getElementById("pageFlag").value;
			//alert(" pageFlag : "+pageFlag);
			//if(pageFlag==0)// Means it is open through [CG] 
			applyScrollOnNotesTbl();
			//else
			//{
				//if(pageFlag==1)// Means it is open through [My Folder]
					//document.getElementById("divNotes").innerHTML=data;
					//applyScrollOnNotesTblMyfolder();
				
			//}
		}
	});
}
function getMessageDiv(teacherId,emailId,jobId,msgType)
{
	removeMessagesFile();
	hideCommunicationsDiv();
	currentPageFlag='msg';
	document.getElementById("teacherDetailId").value=teacherId;	
	document.getElementById("teacherIdForMessage").value=teacherId;	
	document.getElementById("emailId").value=emailId;
	document.getElementById("jobId").value=jobId;
	document.getElementById("emailDiv").innerHTML=emailId;
	$('#messageSend').find(".jqte_editor").html("");
	document.getElementById("messageSubject").value="";
	//$('#myModalMessage').modal('show');
	try{
		$('#myModalMessage').modal('show');
	}catch(err)
	  {}
	$('#messageSubject').css("background-color", "");
	$('#messageSend').find(".jqte_editor").css("background-color", "");
	$('#messageSubject').focus();
	$('#errordivMessage').empty();
	document.getElementById("msgType").value=msgType;
	if(msgType==0){
		CandidateGridAjaxNew.getMessages(teacherId,jobId,pageMsg,noOfRowsMsg,sortOrderStrMsg,sortOrderTypeMsg,
		{
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{	
				document.getElementById("divMessages").innerHTML=data;
				applyScrollOnMessageTbl();
			}
		});
	}else{
		document.getElementById("divMessages").innerHTML="";	
	}
	//jobId=12;
	if( ($('#entityTypeId').val()==2) || ($('#entityTypeId').val()==3) || ($('#entityTypeId').val()==5) || ($('#entityTypeId').val()==6))
	{
		//alert(" jobId : "+jobId+" teacherId "+teacherId+" msgType : "+msgType);
		if((msgType==1 || msgType==0))
		{
		getTemplatesByDistrictId(jobId);
		getDocuments();
		}
		else
		{
			if(msgType==2)
			{
				$("#templateDiv").hide();
				$("#btnChangetemplatelink").hide();
			}
			getTemplatesByDistrictId(jobId);
			getDocuments();
		}
	}
	
	
}

function getTemplatesByDistrictId(jobId)
{
	CandidateGridAjaxNew.getTemplatesByDistrictId(jobId,
			{
				async: true,
				errorHandler:handleError,
				callback:function(data)
				{	
					//alert(" data : "+data+" Length : "+data.length);
					if(data!="" || data.length>0)
					{
						$("#templateDiv").fadeIn();
						$("#btnChangetemplatelink").hide();
						$("#districttemplate").html(data);
					}
				}
			});
}

function setTemplate()
{
	var templateId	=	$("#districttemplate").val();
	if(templateId==0)
		$("#btnChangetemplatelink").fadeOut();
	else
		$("#btnChangetemplatelink").fadeIn();
}

function getTemplate()
{
	var templateId	=	$("#districttemplate").val();
	var teacherId	=	$("#teacherDetailId").val();
	var jobId		=	$("#jobId").val();
	var msgType		=	$("#msgType").val();
	var confirmFlagforChangeTemplate = $("#confirmFlagforChangeTemplate").val(); // Gagan : confirmFlagforChangeTemplate 1 means confirm change template 
	
	if(templateId!=0)
	{
		if(msgType==2){

			CandidateGridAjaxNew.getTemplatesByMultipleTeachTemplateId(templateId,jobId,
			{
				async: true,
				errorHandler:handleError,
				callback:function(data)
				{	
					//alert(" data : "+data+" Length : "+data.length);
					if(data!=null)
					{
						if($('#messageSubject').val() && $('#messageSend').find(".jqte_editor").text().trim()!="" && confirmFlagforChangeTemplate==0)
						{
							try{
								$('#confirmChangeTemplate').modal('show');
							}	
							catch (err){}
						}
						else
						{
							$('#messageSubject').val(data.subjectLine);
							$('#messageSend').find(".jqte_editor").html(data.templateBody);
							$("#confirmFlagforChangeTemplate").val(0);
						}
					}
				}
			});
		}
		else{
			CandidateGridAjaxNew.getTemplatesByTemplateId(templateId,jobId,teacherId,
			{
				async: true,
				errorHandler:handleError,
				callback:function(data)
				{	
					//alert(" data : "+data+" Length : "+data.length);
					if(data!=null)
					{
						if($('#messageSubject').val() && $('#messageSend').find(".jqte_editor").text().trim()!="" && confirmFlagforChangeTemplate==0)
						{
							try{
								$('#confirmChangeTemplate').modal('show');
							}	
							catch (err){}
						}
						else
						{
							$('#messageSubject').val(data.subjectLine);
							$('#messageSend').find(".jqte_editor").html(data.templateBody);
							$("#confirmFlagforChangeTemplate").val(0);
						}
					}
				}
			});
		}
	}
	return false;
}

function confirmChangeTemplate()
{
	$("#confirmFlagforChangeTemplate").val(1);
	getTemplate();
	try{
		$('#confirmChangeTemplate').modal('hide');
	}	
	catch (err){}
}
function showMessage(messageId)
{	
	CandidateGridAjaxNew.showMessage(messageId,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			document.getElementById("messageSubject").value=data.messageSubject;
			$('#messageSend').find(".jqte_editor").html(data.messageSend);
		//	document.getElementById("emailDiv").innerHTML=data.teacherEmailAddress;
			$('#messageSend').find(".jqte_editor").focus();
		}
	});
}

function validateMessage()
{
	$('#messageSubject').css("background-color", "");
	$('#messageSend').find(".jqte_editor").css("background-color", "");
	var messageSend = $('#messageSend').find(".jqte_editor").html();
	var messageSubject = document.getElementById("messageSubject").value;
	var jobId = document.getElementById("jobId").value;
	
	var msgType=document.getElementById("msgType").value;
	var teacherId;
	if(msgType!=2)
		teacherId = document.getElementById("teacherDetailId").value;
	var teacherIds = new Array();
	
	var messageDateTime = document.getElementById("messageDateTime").value;
	var documentname="";
	try{
		documentname = document.getElementById("districtDocument").value;
	}catch(e){}
	
	
	
	$('#errordivMessage').empty();
	var messageFileName="";
	var fileMessage=null;
	try{
		fileMessage=document.getElementById("fileMessage").value;
	}catch(e){}
	
	var cnt=0;
	var focs=0;
		if(trim(messageSubject)=="")
		{
			$('#errordivMessage').append("&#149; "+resourceJSON.PlzEtrSub+".<br>");
			if(focs==0)
				$('#messageSubject').focus();
			$('#messageSubject').css("background-color", "#F5E7E1");
			cnt++;focs++;
		}else if ($('#messageSend').find(".jqte_editor").text().trim()==""){

			$('#errordivMessage').append("&#149; "+resourceJSON.PlzEtrMsg+".<br>");
			if(focs==0)
				$('#messageSend').find(".jqte_editor").focus();
			$('#messageSend').find(".jqte_editor").css("background-color", "#F5E7E1");
			cnt++;focs++;
		}/*else{
			var charCount=$('#messageSend').find(".jqte_editor").text().trim();
			var count = charCount.length;
 			if(count>1000)
 			{
				$('#errordivMessage').append("&#149; Message length cannot exceed 1000 characters.<br>");
				if(focs==0)
					$('#messageSend').find(".jqte_editor").focus();
				$('#messageSend').find(".jqte_editor").css("background-color", "#F5E7E1");
				cnt++;focs++;
			}
		}*/
		
		if(fileMessage=="" && cnt==0){
			$('#errordivMessage').show();
			$('#errordivMessage').append("&#149; "+resourceJSON.PlzUplMsg+".<br>");
			cnt++;
		}else if(fileMessage!="" && fileMessage!=null)
		{
			var ext = fileMessage.substr(fileMessage.lastIndexOf('.') + 1).toLowerCase();	
			messageFileName="message"+messageDateTime+"."+ext;
			var fileSize = 0;
			if ($.browser.msie==true)
		 	{	
			    fileSize = 0;	   
			}
			else
			{
				if(document.getElementById("fileMessage").files[0]!=undefined)
				{
					fileSize = document.getElementById("fileMessage").files[0].size;
				}
			}
			
			if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt'|| ext=='xlsx' || ext=='xls'))
			{
				$('#errordivMessage').show();
				$('#errordivMessage').append("&#149; "+resourceJSON.PlzSelectAcceptMsg+".<br>");
				cnt++;
			}
			else if(fileSize>=10485760)
			{
				$('#errordivMessage').show();
				$('#errordivMessage').append("&#149; "+resourceJSON.FileSizeLessthan10Mb+".<br>");
				cnt++;
			}
		}
		if(cnt==0){	
			try{
				$('#loadingDiv').show();
				if(fileMessage!="" && fileMessage!=null){
					document.getElementById("frmMessageUpload").submit();
				}else{
					$('#lodingImage').append("<img src=\"images/loadingAnimation.gif\" /> "+resourceJSON.send+"...");
					if(msgType==2)
					{
						teacherIds=getSendMessageTeacherIds();
						//TeacherSendMessageAjax
						CGServiceAjax.saveMessage(teacherIds,messageSubject,messageSend,jobId,messageFileName,msgType,documentname,{ 
							async: true,
							errorHandler:handleError,
							callback:function(data)
							{	
								$('#loadingDiv').hide();
								confMessageForSendMessage();
								//uncheckedAllCBX();
							}
						});
					}
					else
					{
						CandidateGridAjaxNew.saveMessage(teacherId,messageSubject,messageSend,jobId,messageFileName,documentname,{ 
							async: true,
							errorHandler:handleError,
							callback:function(data)
							{	
								$('#loadingDiv').hide();
								var senderEmail = document.getElementById("emailDiv").innerHTML;
								confMessage(teacherId,senderEmail,jobId);
							}
						});
					}
					
					return true;
				}
			}catch(err){}
		}else{
			$('#errordivMessage').show();
			return false;
		}
}
function saveMessageFile(messageDateTime){
	$('#messageSubject').css("background-color", "");
	$('#messageSend').find(".jqte_editor").css("background-color", "");
	var messageSend = $('#messageSend').find(".jqte_editor").html();
	var messageSubject = document.getElementById("messageSubject").value;
	//var teacherId = document.getElementById("teacherDetailId").value;
	var jobId = document.getElementById("jobId").value;
	
	var msgType=document.getElementById("msgType").value;
	var teacherId;
	if(msgType!=2)
		teacherId = document.getElementById("teacherDetailId").value;
	var teacherIds = new Array();
	var documentname="";
	try{
		documentname = document.getElementById("districtDocument").value;
	}catch(e){}
	
	$('#errordivMessage').empty();
	var messageFileName="";
	var fileMessage="";
	try{
		fileMessage=document.getElementById("fileMessage").value;
	}catch(e){}
	if(fileMessage!="" && fileMessage!=null)
	{
		var ext = fileMessage.substr(fileMessage.lastIndexOf('.') + 1).toLowerCase();	
		messageFileName="message"+messageDateTime+"."+ext;
	}
	
	if(msgType==2)
	{
		teacherIds=getSendMessageTeacherIds();
		//TeacherSendMessageAjax
		CGServiceAjax.saveMessage(teacherIds,messageSubject,messageSend,jobId,messageFileName,msgType,documentname,{ 
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{	
				$('#loadingDiv').hide();
				confMessageForSendMessage();
				//uncheckedAllCBX();
			}
		});
	}
	else
	{
		CandidateGridAjaxNew.saveMessage(teacherId,messageSubject,messageSend,jobId,messageFileName,documentname,{ 
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{	
				$('#loadingDiv').hide();
				var senderEmail = document.getElementById("emailDiv").innerHTML;
				confMessage(teacherId,senderEmail,jobId);
			}
		});
	}
	
	return true;
	
}
function confMessage(teacherId,senderEmail,jobId)
{	
	$('#lodingImage').empty();
	//$('#myModalMessage').modal('hide');
	//$('#myModalMsgShow').modal('show');
	try{
		$('#myModalMessage').modal('hide');
		$('#myModalMsgShow').modal('show');
	}catch(err)
	  {}
	document.getElementById("message2show").innerHTML=""+resourceJSON.MsgSentToCandidate+"";
}

function showMessageDiv()
{	
	var msgType=document.getElementById("msgType").value;
	//$('#myModalMsgShow').modal('hide');
	//$('#myModalMessage').modal('show');
	try{
		$('#myModalMsgShow').modal('hide');
		$('#myModalMessage').modal('show');
	}catch(err)
	  {}
	var teacherId = document.getElementById("teacherDetailId").value;
	var jobId = document.getElementById("jobId").value;
	var senderEmail = document.getElementById("emailDiv").innerHTML;
	jobForTeacherGId=document.getElementById("jobForTeacherGId").value;
	var commDivFlag=document.getElementById("commDivFlag").value;
	if(msgType==1){
		//$('#myModalMsgShow').modal('hide');
		//$('#myModalMessage').modal('hide');
		try{
			$('#myModalMsgShow').modal('hide');
			$('#myModalMessage').modal('hide');
		}catch(err)
		  {}
		getCommunicationsDiv(commDivFlag,jobForTeacherGId,teacherId,jobId);
	}else{
		document.getElementById('messageSubject').value="";
		document.getElementById('messageSend').value="";
		getMessageDiv(teacherId,senderEmail,jobId,0);
	}
}


function cancelNotes()
{
	$('#divTxtNode').find(".jqte_editor").html("");		
	//$('#myModalNotes').modal('show');
	try{
		$('#myModalNotes').modal('show');
	}catch(err)
	  {}
	$('#errordivNotes').empty();
	$('#divTxtNode').find(".jqte_editor").css("background-color", "");
	document.getElementById("noteId").value="";
	document.getElementById("spnBtnSave").style.display="inline";
}

function saveNotes()
{	
	jobForTeacherGId=document.getElementById("jobForTeacherGId").value;
	var notes = $('#divTxtNode').find(".jqte_editor").html();
	var teacherId = document.getElementById("teacherId").value;
	var jobId = document.getElementById("jobId").value;
	var noteId = document.getElementById("noteId").value;
	var noteDateTime=document.getElementById("noteDateTime").value;
	var commDivFlag=document.getElementById("commDivFlag").value;
	var fileNote=null;
	var noteFileName="";
	try{
		fileNote=document.getElementById("fileNote").value;
	}catch(e){}
	var cnt=0;
	var focs=0;	
	$('#errordivNotes').empty();
	if ($('#divTxtNode').find(".jqte_editor").text().trim()=="")
	{
		$('#errordivNotes').append("&#149; "+resourceJSON.PlzEtrNotes+".<br>");
		if(focs==0)
			$('#divTxtNode').find(".jqte_editor").focus();
		$('#divTxtNode').find(".jqte_editor").css("background-color", "#F5E7E1");
		cnt++;
	}else if(fileNote==""){
		$('#errordivNotes').show();
		$('#errordivNotes').append("&#149; "+resourceJSON.PlzUpldNotes+".<br>");
		cnt++;
	}else if(fileNote!="" && fileNote!=null)
	{
		var ext = fileNote.substr(fileNote.lastIndexOf('.') + 1).toLowerCase();	
		noteFileName="note"+noteDateTime+"."+ext;
		var fileSize = 0;
		if ($.browser.msie==true)
	 	{	
		    fileSize = 0;	   
		}
		else
		{
			if(document.getElementById("fileNote").files[0]!=undefined)
			{
				fileSize = document.getElementById("fileNote").files[0].size;
			}
		}
		
		if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt' || ext=='xlsx' || ext=='xls'))
		{
			$('#errordivNotes').show();
			$('#errordivNotes').append("&#149; "+resourceJSON.PlzSelectNoteFormat+".<br>");
			cnt++;
		}
		else if(fileSize>=10485760)
		{
			$('#errordivNotes').show();
			$('#errordivNotes').append("&#149; "+resourceJSON.FileSizeLessthan10Mb+".<br>");
			cnt++;
		}
	}
	if(cnt==0){	
		try{
			$('#loadingDiv').show();
			if(fileNote!="" && fileNote!=null){
				document.getElementById("frmNoteUpload").submit();
			}else{
				CandidateGridAjaxNew.saveNotes(teacherId,notes,noteId,noteFileName,
				{
					async:false,
					errorHandler:handleError,
					callback:function(data)
					{
						$('#loadingDiv').hide();	
						showCommunicationsDiv();
						getCommunicationsDiv(commDivFlag,jobForTeacherGId,teacherId,jobId);				
					}
				});	
			}
		}catch(err){}
	}
}
function saveNoteFile(noteDateTime)
{	
	var notes = $('#divTxtNode').find(".jqte_editor").html();
	var teacherId = document.getElementById("teacherId").value;
	var jobId = document.getElementById("jobId").value;
	var noteId = document.getElementById("noteId").value;
	var commDivFlag=document.getElementById("commDivFlag").value;
	var fileNote=null;
	try{
		fileNote=document.getElementById("fileNote").value;
	}catch(e){}
	var noteFileName="";
	if(fileNote!="" && fileNote!=null)
	{
		var ext = fileNote.substr(fileNote.lastIndexOf('.') + 1).toLowerCase();	
		noteFileName="note"+noteDateTime+"."+ext;
	}
	CandidateGridAjaxNew.saveNotes(teacherId,notes,noteId,noteFileName,
	{
		async:false,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#loadingDiv').hide();
			showCommunicationsDiv();
			getCommunicationsDiv(commDivFlag,jobForTeacherGId,teacherId,jobId);				
		}
	});	
}

function editNoted(notesId, viewOnly)
{	
	$('#errordivAct').empty();
	CandidateGridAjaxNew.showEditNotes(notesId,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			document.getElementById("noteId").value=data.noteId;
			$('#divTxtNode').find(".jqte_editor").html(data.note);
			
			$('#divTxtNode').find(".jqte_editor").focus();
			if(viewOnly!=undefined)
				document.getElementById("spnBtnSave").style.display="none";
			else
				document.getElementById("spnBtnSave").style.display="inline";
		}
	});
}


function showActDiv(status,teacherAssessmentStatusId,jobForTeacherId)
{	
	$("#myModalAct").css({"z-index":"3000"});
	$('#errordivAct').empty();	
	CandidateGridAjaxNew.showActDiv( status,teacherAssessmentStatusId,jobForTeacherId,{ 
	async: true,
	errorHandler:handleError,
	callback:function(data)
	{	
		document.getElementById("divAct").innerHTML=data.toString();
		//$('#myModalAct').modal('show');
		try{
			$('#myModalAct').modal('show');
		}catch(err)
		  {}
	}});	
}
function closeActAction(counter)
{
	$('#tag'+counter).popover('hide');
	/*try{ $('#tag'+counter).popover('destroy'); }
	catch(err){}*/
	
}
function removeStatus(teacherId)
{
	$('#removeFlag'+teacherId).val("1"); 
	$('#removeStatus'+teacherId).html("&nbsp; ");
	var k =document.getElementById("k3"+teacherId).value;
	var rdoSecStatus = document.getElementById("rdoSecStatus"+k+"_"+teacherId);
	//alert(" checked Status "+rdoSecStatus.checked+" -------- rdoSecStatus"+k+"_"+teacherId)
	rdoSecStatus.checked=false;
}

var tests="";
function UncheckSecStatus(id)
{
	
	if(document.getElementById(id).checked==false){
		tests+="##"+id+"##";
	}
	
	if(document.getElementById(id).checked==true){
		var newv = tests.replace('##'+id+'##','');
		tests = newv;
	}
}
function chkActAction(teacherId){
	var rdoSecStatus = document.getElementsByName("rdoSecStatus");
	var tId="";
	var secStusId="0";
	var jbFtId="";
	var teacherIdtemp="";
	var districtId = $("#districtId").val();
	
	var ids="";
	var vals="";			
			
	for(i=0;i<rdoSecStatus.length;i++)
	{
		var idCheck=false;
		try{
			if(teacherId==rdoSecStatus[i].id.split("_")[0]){
				idCheck=true;
			}
		}catch(err){}
		
		if(rdoSecStatus[i].checked==true && idCheck)
		{
			var rdoId="##"+rdoSecStatus[i].id+"##";
			var uncheckflag=0;
			if(tests.indexOf(rdoId)!=-1){
				uncheckflag=1;
			}
			if(ids.indexOf(rdoId)==-1){
				if(uncheckflag==0){
					ids +=rdoId;
				vals += ","+rdoSecStatus[i].value.split(",")[1];					
				}
			}
		}
	}
	if (vals) vals = vals.substring(1);
		
	//Start of my code.
	/*vals=",";
	$('input[type=checkbox]').each(function ()
	{
		var checkboxName = $(this).attr("name");
		if(checkboxName=="rdoSecStatus")
        {
        	var myId = $(this).attr("id");
        	if($("#"+myId).is(':checked') )
        	{
        		var myar = $(this).attr("value").split(",");
        		if(vals.indexOf(","+myar[1]+",")==-1)
        			vals = vals+myar[1]+",";
        	}
        }
	});
	if(vals!=",")
		vals = vals.substring(1, vals.length-1);
	if(vals==",")
		vals="";
	*/
	vals.replace("undefined", "");
	
	tests="";
	if($("#aplyToAlldistrict"+teacherId).is(':checked')){
		teacherIdtemp=rdoSecStatus[0].value.split(",")[2];
		CGServiceAjax.saveTags(districtId,teacherIdtemp,vals,null,{ 
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{	
				try{
					$('#myModalAct').modal('hide');
				}catch(err)
				  {}
				getCandidateGrid();
				refreshStatus();
			}
		});
	}
	else
	//if($("#aplyToAll"+teacherId).is(':checked'))
	{
		CandidateGridAjaxNew.addOrUpdateTags(teacherId,vals,null,{
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			//$('#myModalAct').modal('hide');
				try{
					$('#myModalAct').modal('hide');
				}catch(err)
				  {}
				getCandidateGrid();
				refreshStatus();
			}
		});	
	}
}

function saveApplyTags()
{
	var tagIdValues="";
	$('input[name="rdoSecStatus"]:checked').each(function(){
		var tagId = $(this).attr("id");
		var indexValue = tagId.indexOf("_");
		
		if(indexValue==-1)
			if(tagIdValues=="")
				tagIdValues=""+tagId;
			else
				tagIdValues=tagIdValues+","+tagId;
	});
	
	var districtSpecificOrJobSpecificTags = "";
	$('input[name="jobSpecificOrDistrictSpecific"]:checked').each(function(){
		districtSpecificOrJobSpecificTags = $(this).val();
		//alert( $(this).val() );
	});
	
	$("#tagsForMultipleCandidateDiv").modal("hide");
	$("#loadingDiv").show();

	if(districtSpecificOrJobSpecificTags=="job"){
		
		var jobForTeacherIds="";
		$('input[name="cgCBX"]:checked').each(function(){
			var jobForTeacherId = $(this).attr("value");//teacherid
			if(jobForTeacherIds=="")
				jobForTeacherIds=jobForTeacherIds+jobForTeacherId;
			else
				jobForTeacherIds=jobForTeacherIds+","+jobForTeacherId;
		});
		
		CandidateGridAjaxNew.addOrUpdateTags(null,tagIdValues,jobForTeacherIds,{
			async: false,
			errorHandler:handleError,
			callback:function(data)
			{
				$("#loadingDiv").hide();
				getCandidateGrid();
				refreshStatus();
			}
		});	
	}
	else
	if(districtSpecificOrJobSpecificTags=="district"){
		
		var teacherIds="";
		var districtId = $("#districtId").val();
		$('input[name="cgCBX"]:checked').each(function(){
			var teacherId = $(this).attr("teacherid");//teacherid
			if(teacherIds=="")
				teacherIds=teacherIds+teacherId;
			else
				teacherIds=teacherIds+","+teacherId;
		});
		
		CGServiceAjax.saveTags(districtId,null,tagIdValues,teacherIds,{
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{	
				$("#loadingDiv").hide();
				getCandidateGrid();
				refreshStatus();
			}
		});
	}
	else
		alert("Invalid value of districtSpecificOrJobSpecificTags:- "+districtSpecificOrJobSpecificTags);
}

function applyTags()
{
	$("#loadingDiv").show();
	CandidateGridAjaxNew.getTagDetails(null,null,null,null,{
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
			$("#loadingDiv").hide();
			$('#applyTagsId').html(data);
			$("input[type=radio][name=jobSpecificOrDistrictSpecific][value='job']").prop("checked",true);
			$("#tagsForMultipleCandidateDiv").modal("show");
			
		}
	});
	$("#loadingDiv").hide();
}

function radioTagsChange(jobId,teacherId,jobForTid,noOfRecordCheck,jobOrDistrict)
{
	//alert($('#teacherJobId').val());
	
	$('#loadingDiv').show();
	if(jobOrDistrict=="job")
		showTagDetails(noOfRecordCheck, jobForTid, teacherId, $('#teacherJobId').val());
	else
		showTagDetails(noOfRecordCheck, jobForTid, teacherId, null);	//send jobId=null;
	$('#loadingDiv').hide();
}

function savePhone()
{	
	var phoneType=document.getElementById("phoneType").value;
	var callDetail = $('#divTxtPhone').find(".jqte_editor").html();
	var teacherId = document.getElementById("teacherDetailId").value;
	var jobId = document.getElementById("jobId").value;
	jobForTeacherGId=document.getElementById("jobForTeacherGId").value;
	var phoneDateTime = document.getElementById("phoneDateTime").value;
	var commDivFlag=document.getElementById("commDivFlag").value;
	var phoneFileName="";
	var filePhone=null;
	try{
		filePhone=document.getElementById("filePhone").value;
	}catch(e){}
	var cnt=0;
	var focs=0;	
	$('#errordivPhone').empty();
	if ($('#divTxtPhone').find(".jqte_editor").text().trim()=="")
	{
		cnt=1;
		$('#errordivPhone').append("&#149; "+resourceJSON.PlzEtrClDetail+".<br>");
		if(focs==0){
			$('#divTxtPhone').find(".jqte_editor").focus();
			$('#divTxtPhone').find(".jqte_editor").css("background-color", "#F5E7E1");
		}
	
	}else if($('#divTxtPhone').find(".jqte_editor").text().trim()!=""){
		var charCount=$('#divTxtPhone').find(".jqte_editor").text().trim();
		var count = charCount.length;
		if(count>2500)
		{
			$('#errordivPhone').append("&#149; "+resourceJSON.CallDetailNotExceed2500+".<br>");
			cnt=1;
		}
	}
	if(filePhone=="" && cnt==0){
		$('#errordivPhone').show();
		$('#errordivPhone').append("&#149; "+resourceJSON.PlzUpldCallDetail+".<br>");
		cnt++;
	}else if(filePhone!="" && filePhone!=null)
	{
		var ext = filePhone.substr(filePhone.lastIndexOf('.') + 1).toLowerCase();	
		phoneFileName="note"+phoneDateTime+"."+ext;
		var fileSize = 0;
		if ($.browser.msie==true)
	 	{	
		    fileSize = 0;	   
		}
		else
		{
			if(document.getElementById("filePhone").files[0]!=undefined)
			{
				fileSize = document.getElementById("filePhone").files[0].size;
			}
		}
		if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt'|| ext=='xlsx' || ext=='xls'))
		{
			$('#errordivPhone').show();
			$('#errordivPhone').append("&#149; "+resourceJSON.PlzSelectPhoneFormat+".<br>");
			cnt++;
		}
		else if(fileSize>=10485760)
		{
			$('#errordivPhone').show();
			$('#errordivPhone').append("&#149; "+resourceJSON.FileSizeLessthan10Mb+".<br>");
			cnt++;
		}
	}
	if(cnt==0){	
		try{
			$('#loadingDiv').show();
			if(filePhone!="" && filePhone!=null){
				document.getElementById("frmPhoneUpload").submit();
			}else{
				CandidateGridAjaxNew.savePhone(teacherId,callDetail,jobId,jobForTeacherGId,phoneFileName,
						{
							async:false,
							errorHandler:handleError,
							callback:function(data)
							{
								$('#loadingDiv').hide();
								if(phoneType==0){
									getPhoneDetail(teacherId,jobForTeacherGId,0);
								}else{
									showCommunicationsForPhone();
									getCommunicationsDiv(commDivFlag,jobForTeacherGId,teacherId,jobId);	
								}
							}
			});	
			}
		}catch(err){}
	}
}
function savePhoneFile(phoneDateTime)
{	
	var phoneType=document.getElementById("phoneType").value;
	var callDetail = $('#divTxtPhone').find(".jqte_editor").html();
	var teacherId = document.getElementById("teacherDetailId").value;
	var jobId = document.getElementById("jobId").value;
	jobForTeacherGId=document.getElementById("jobForTeacherGId").value;
	var commDivFlag=document.getElementById("commDivFlag").value;
	var phoneFileName="";
	var filePhone=null;
	try{
		filePhone=document.getElementById("filePhone").value;
	}catch(e){}
	
	if(filePhone!="" && filePhone!=null)
	{
		var ext = filePhone.substr(filePhone.lastIndexOf('.') + 1).toLowerCase();	
		phoneFileName="phone"+phoneDateTime+"."+ext;
	}
	CandidateGridAjaxNew.savePhone(teacherId,callDetail,jobId,jobForTeacherGId,phoneFileName,
	{
		async:false,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#loadingDiv').hide();
			if(phoneType==0){
				getPhoneDetail(teacherId,jobForTeacherGId,0);
			}else{
				
				showCommunicationsForPhone();
				getCommunicationsDiv(commDivFlag,jobForTeacherGId,teacherId,jobId);	
			}
		}
	});	
}
function showCallDetails(){
	document.getElementById("calldetrailsdiv").style.display='inline';
	document.getElementById("calldetrailsbtn").style.display='inline';
}
function getPhoneDetail(teacherId,jobForTeacherGId,phoneType){

	document.getElementById("teacherIdForPhone").value=teacherId;
	if(phoneType==1){
		//$('#myModalCommunications').modal('hide');
		try{
			$('#myModalCommunications').modal('hide');
		}catch(err)
		  {}
		document.getElementById("calldetrailsdiv").style.display='inline';
		document.getElementById("calldetrailsbtn").style.display='inline';
		document.getElementById("divPhoneGrid").innerHTML="";
	}else{
		document.getElementById("calldetrailsdiv").style.display='none';
		document.getElementById("calldetrailsbtn").style.display='none';
	}
	document.getElementById("jobForTeacherGId").value=jobForTeacherGId;
	document.getElementById("phoneType").value=phoneType;
	
	$('#errordivPhone').empty();
	$('#divTxtPhone').find(".jqte_editor").html("");
	$('#divTxtPhone').find(".jqte_editor").css("background-color", "");
	removePhoneFile();
	document.getElementById("teacherDetailId").value=teacherId;
	
	var jobId = document.getElementById("jobId").value;
	currentPageFlag='phone';
	CandidateGridAjaxNew.getPhoneDetail(teacherId,jobId,pagePhone,noOfRowsPhone,sortOrderStrPhone,sortOrderTypePhone,
	{
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			if(data==""){
				document.getElementById("divAlertText").innerHTML=""+resourceJSON.MsgPhNoIsNotAvailable+"";
				//$('#divAlert').modal('show');
				try{
					$('#divAlert').modal('show');
				}catch(err)
				  {}
			}
			else{
				if(phoneType==0){
					document.getElementById("divPhoneGrid").innerHTML=data;
				}
				//$('#myModalPhone').modal('show');
				try{
					$('#myModalPhone').modal('show');
				}catch(err)
				  {}
				applyScrollOnPhone();
			}
			
		}
	});
}
function divToggelClose(){
	if(document.getElementById("divToggel").style.display=='block'){
		document.getElementById("divToggel").style.display='none';
	}
}
function getRemoveCandidateGrid()
{	
	$('#removeWait').show();
	var jobId=document.getElementById("jobId").value;
	var firstName		=	trim(document.getElementById("firstName").value); 
	var lastName		=	trim(document.getElementById("lastName").value);
	var emailAddress	=	trim(document.getElementById("emailAddress").value);
	var certType		=	trim(document.getElementById("certificateTypeMaster").value);
	var regionId		=	trim(document.getElementById("regionId").value);
	var degreeId		=	trim(document.getElementById("degreeId").value);
	var universityId		=	trim(document.getElementById("universityId").value);
	var normScoreSelectVal		=	trim(document.getElementById("normScoreSelectVal").value);
	var normScore		=	trim(document.getElementById("normScore").value);
	var CGPA		=	trim(document.getElementById("CGPA").value);
	var CGPASelectVal		=	trim(document.getElementById("CGPASelectVal").value);
	var contacted		=	trim(document.getElementById("contacted").value);
	var candidateStatus		=	trim(document.getElementById("candidateStatus").value);
	var stateId=document.getElementById("stateId").value;
	var tagsearchId		=	trim(document.getElementById("tagsearchId").value);
	
	try{
		var ssn	= trim(document.getElementById("ssn").value);
	}catch(err){}
	
	try{
		var ifrmYOTE = document.getElementById('ifrmYOTE');
		var innerYOTE = ifrmYOTE.contentDocument || ifrmYOTE.contentWindow.document;
		var inputYOTE = innerYOTE.getElementById('YOTEFrm');
		document.getElementById("YOTE").value=inputYOTE.value;
	}catch(e){}
	var YOTESelectVal		=	trim(document.getElementById("YOTESelectVal").value);
	var YOTE				=	document.getElementById("YOTE").value;
	var hiddenCertTypeIds 	=	document.getElementsByName("hiddenCertTypeId");
	var stateId2	=	trim(document.getElementById("stateId2").value);
    var zipCode		=	trim(document.getElementById("zipCode").value);
	var certIds =[];
	for(var i=0;i<hiddenCertTypeIds.length;i++)
	{
		certIds.push(hiddenCertTypeIds[i].value);
	}
	var epiFromDate = "";
	var epiToDate = "";
     try{
	var epiFromDate	=	trim(document.getElementById("epiFromDate").value);
    var epiToDate	=	trim(document.getElementById("epiToDate").value);
     }catch(err){}
	
	CandidateGridAjaxNew.getRemoveCandidateReportGrid(jobId,firstName,lastName,emailAddress,certType,regionId,degreeId,universityId,normScoreSelectVal,
			normScore,CGPA,CGPASelectVal,contacted,candidateStatus,stateId,orderColumn,sortingOrder,totalNoOfRecord,tagsearchId,callbreakup,callNoofRecords,
			certIds,YOTESelectVal,YOTE,stateId2,zipCode,epiFromDate,epiToDate,ssn,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
		
		document.getElementById("divRemoveCGGrid").innerHTML=data;
		
		//add by Ram Nath
		var table="divRemoveCGGrid";//add by Ram Nath
		var listLength=document.getElementById("rejectedListSize").value;
		if(callbreakup)
			if(listLength>1)
				getBreakUpRecords(table,listLength,'Rejectedtr_',$("[name='sessionListNameRejected"+jobId+"']").val(),1);
		//end by Ram Nath
			
			displayTotalCGRecord(4,0);
			var startPoint=totalNoOfRecord-sectionRecordRemove;
			for(var j=startPoint;j<=totalNoOfRecord;j++)
			{	
				$('#tpResume'+j).tooltip();
				$('#tpPortfolio'+j).tooltip();
				$('#tpJSA'+j).tooltip();
				$('#hireTp'+j).tooltip();
				$('#tpPDReport'+j).tooltip();
				$('#tpCL'+j).tooltip();
				$('#tpAct'+j).tooltip();
				$('#tpMsg'+j).tooltip();
				$('#tpNotes'+j).tooltip();		
				$('#tpTrans'+j).tooltip();
				$('#tpCert'+j).tooltip();
				$('#tpMessage'+j).tooltip();
				$('#tpPhone'+j).tooltip();
				$('#tpInv'+j).tooltip();
				$('#tprr'+j).tooltip();
				$('#ftsbu'+j).tooltip();
				$('#tooltipPreview'+j).tooltip()
				$('#tpref'+j).tooltip();
				$('#teacherName_'+j).tooltip();
				$('#i4Ques'+j).tooltip();
				$('#teacher_PNQ'+j).tooltip();
				try { $('#alum'+j).tooltip(); } catch (e) {}
				try{ $('#tpInternalCandidate'+j).tooltip(); }catch(e){}
				try{ $('.formerEmployee').tooltip(); }catch(e){}
				try{ $('.returedEmployee').tooltip(); }catch(e){}
				try{$('#tpAssessmentinvite'+j).tooltip(); }catch(e){}
				try{$('#pc'+j).tooltip(); }catch(e){}
				try{$('#cndNotReviewed'+j).tooltip(); }catch(e){}
				
				var candidateFlag=document.getElementById("candidateflag").value;
				if(candidateFlag==1)
				{
					var noOfTagsFile=document.getElementById("noOfTagsFile").value;
					if(noOfTagsFile>15){
						iWidth=600;
					}
					else
					{
						iWidth=300;
					}
				}
				else
				{
					iWidth=300;
				}
				$('#tag'+j).popover({ 
				    html : true,
				    placement: 'right',
				    content: $('#tagDiv'+j).html(),
				  }).on("mouseenter", function () {
					  var height=setDialogPositionNew();
					  height=height+80;
					  	var style = $('<style>.popover { position: absolute;top: '+height+'px !important;left: 0px;width: '+iWidth+'px; }</style>')
						$('html > head').append(style);
					  	
				        var _this = this;
				        $(this).popover("show");
				        $(this).siblings(".popover").on("mouseleave", function () {
				            //$(_this).popover('hide');
				        });
				    }).on("mouseleave", function () {
				        var _this = this;
				        setTimeout(function () {
				            if (!$(".popover:hover").length) {
				                //$(_this).popover("hide")
				            }
				        }, 100);
				    });
				
				/*$('.profile'+j).popover({ 
				 	trigger: "manual",
				    html : true,
				    placement: 'right',
				    content: $('#profileDetails'+j).html(),
				  }).on("mouseenter", function () {
				        var _this = this;
				        $(this).popover("show");
				        $(this).siblings(".popover").on("mouseleave", function () {
				            $(_this).popover('hide');
				        });
				    }).on("mouseleave", function () {
				        var _this = this;
				        setTimeout(function () {
				            if (!$(".popover:hover").length) {
				                $(_this).popover("hide")
				            }
				        }, 100);
				    });*/
			}
			
			getRejectScoreDetails();
			
		}});
}
function hideRemoveCandidateGrid()
{		
	document.getElementById("divRemoveCGGrid").innerHTML="<div id='removeWait' style='display:none;text-align:center;padding-top:0px;'><img src='images/loadingAnimation.gif' /> <br>"+resourceJSON.MsgLoad+"</div>";
}

function getInCompleteCandidateGrid()
{	
	$('#InCompWait').show();
	var jobId=document.getElementById("jobId").value;
	var firstName		=	trim(document.getElementById("firstName").value); 
	var lastName		=	trim(document.getElementById("lastName").value);
	var emailAddress	=	trim(document.getElementById("emailAddress").value);
	var certType		=	trim(document.getElementById("certificateTypeMaster").value);
	var regionId		=	trim(document.getElementById("regionId").value);
	var degreeId		=	trim(document.getElementById("degreeId").value);
	var universityId		=	trim(document.getElementById("universityId").value);
	var normScoreSelectVal		=	trim(document.getElementById("normScoreSelectVal").value);
	var normScore		=	trim(document.getElementById("normScore").value);
	var CGPA		=	trim(document.getElementById("CGPA").value);
	var CGPASelectVal		=	trim(document.getElementById("CGPASelectVal").value);
	var contacted		=	trim(document.getElementById("contacted").value);
	var candidateStatus		=	trim(document.getElementById("candidateStatus").value);
	var stateId=document.getElementById("stateId").value;
	var tagsearchId		=	trim(document.getElementById("tagsearchId").value);
	
	try{
		var ssn	= trim(document.getElementById("ssn").value);
	}catch(err){}
	
	try{
		var ifrmYOTE = document.getElementById('ifrmYOTE');
		var innerYOTE = ifrmYOTE.contentDocument || ifrmYOTE.contentWindow.document;
		var inputYOTE = innerYOTE.getElementById('YOTEFrm');
		document.getElementById("YOTE").value=inputYOTE.value;
	}catch(e){}
	var YOTESelectVal		=	trim(document.getElementById("YOTESelectVal").value);
	var YOTE				=	document.getElementById("YOTE").value;
	var hiddenCertTypeIds 	=	document.getElementsByName("hiddenCertTypeId");
	var stateId2	=	trim(document.getElementById("stateId2").value);
    var zipCode		=	trim(document.getElementById("zipCode").value);
	var certIds =[];
	for(var i=0;i<hiddenCertTypeIds.length;i++)
	{
		certIds.push(hiddenCertTypeIds[i].value);
	}
	var epiFromDate = "";
	var epiToDate = "";
     try{
	var epiFromDate	=	trim(document.getElementById("epiFromDate").value);
    var epiToDate	=	trim(document.getElementById("epiToDate").value);
     }catch(err){}
	
	CandidateGridAjaxNew.getInCompleteCandidateReportGrid(jobId,firstName,lastName,emailAddress,certType,regionId,degreeId,universityId,normScoreSelectVal,
			normScore,CGPA,CGPASelectVal,contacted,candidateStatus,stateId,orderColumn, sortingOrder,totalNoOfRecord,tagsearchId,callbreakup,callNoofRecords,
			certIds,YOTESelectVal,YOTE,stateId2,zipCode,epiFromDate,epiToDate,ssn,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			
		document.getElementById("divInCompCGGrid").innerHTML=data;	
		
		//add by Ram Nath
		var table="divInCompCGGrid";//add by Ram Nath
		listLength=document.getElementById("InComplListSize").value;//add by Ram nath
		if(callbreakup)
			if(listLength>1)
				getBreakUpRecords(table,listLength,'InCompltr_',$("[name='sessionListNameInComp"+jobId+"']").val(),1);
		//end by Ram Nath
			
			displayTotalCGRecord(5,0);
			var startPoint=totalNoOfRecord-sectionRecordInComp;
			for(var j=startPoint;j<=totalNoOfRecord;j++)
			{	
				$('#tpResume'+j).tooltip();
				$('#tpPortfolio'+j).tooltip();
				$('#tpJSA'+j).tooltip();
				$('#hireTp'+j).tooltip();
				$('#tpPDReport'+j).tooltip();
				$('#tpCL'+j).tooltip();
				$('#tpAct'+j).tooltip();
				$('#tpMsg'+j).tooltip();
				$('#tpNotes'+j).tooltip();		
				$('#tpTrans'+j).tooltip();
				$('#tpCert'+j).tooltip();
				$('#tpMessage'+j).tooltip();
				$('#tpPhone'+j).tooltip();
				$('#tpInv'+j).tooltip();
				$('#tprr'+j).tooltip();	
				$('#ftsbu'+j).tooltip();
				$('#tooltipPreview'+j).tooltip()
				$('#tpref'+j).tooltip();
				$('#teacherName_'+j).tooltip();
				$('#i4Ques'+j).tooltip();
				$('#teacher_PNQ'+j).tooltip();
				try { $('#alum'+j).tooltip(); } catch (e) {}
				try{ $('#tpInternalCandidate'+j).tooltip(); }catch(e){}
				try{ $('.formerEmployee').tooltip(); }catch(e){}
				try{ $('.returedEmployee').tooltip(); }catch(e){}
				try{$('#tpRefChk'+j).tooltip(); }catch(e){}
				try{$('#dtFailed'+j).tooltip(); }catch(e){}
				try{$('#tpAssessmentinvite'+j).tooltip(); }catch(e){}
				try{$('#pc'+j).tooltip(); }catch(e){}
				try{$('#cndNotReviewed'+j).tooltip(); }catch(e){}
				var candidateFlag=document.getElementById("candidateflag").value;
				var iWidth=0;
				if(candidateFlag==1)
				{
					var noOfTagsFile=document.getElementById("noOfTagsFile").value;
					if(noOfTagsFile>15){
						iWidth=600;
					}
					else
					{
						iWidth=300;
					}
				}
				else
				{
					iWidth=300;
				}
				$('#tag'+j).popover({ 
				    html : true,
				    placement: 'right',
				    content: $('#tagDiv'+j).html(),
				  }).on("mouseenter", function () {
					  var height=setDialogPositionNew();
					  height=height+80;
					  	var style = $('<style>.popover { position: absolute;top: '+height+'px !important;left: 0px;width: '+iWidth+'px; }</style>')
						$('html > head').append(style);
					  
				        var _this = this;
				        $(this).popover("show");
				        $(this).siblings(".popover").on("mouseleave", function () {
				            //$(_this).popover('hide');
				        });
				    }).on("mouseleave", function () {
				    	
				        var _this = this;
				        setTimeout(function () {
				            if (!$(".popover:hover").length) {
				                //$(_this).popover("hide")
				            }
				        }, 100);
				    });
				
				/*$('.profile'+j).popover({ 
				 	trigger: "manual",
				    html : true,
				    placement: 'right',
				    content: $('#profileDetails'+j).html(),
				  }).on("mouseenter", function () {
				        var _this = this;
				        $(this).popover("show");
				        $(this).siblings(".popover").on("mouseleave", function () {
				            $(_this).popover('hide');
				        });
				    }).on("mouseleave", function () {
				        var _this = this;
				        setTimeout(function () {
				            if (!$(".popover:hover").length) {
				                $(_this).popover("hide")
				            }
				        }, 100);
				    });*/
			}
			
			getIncompleteScoreDetails();
			
		}});
}

function setDialogPositionNew()
{
	//alert("123");
	var scrollTopPosition = $(window).scrollTop();
	//alert("123");
	var x=event.clientX;
	//alert("123");
	var y=event.clientY;
	var z=y-180+scrollTopPosition;
	//z=z+80;
	return z;
}


function setDialogPosition()
{
	var scrollTopPosition = $(window).scrollTop();
	var x=event.clientX; 
	var y=event.clientY;
	var z=y-180+scrollTopPosition;
	//z=z+80;
	return z;
}


function hideInCompleteCandidateGrid()
{		
	document.getElementById("divInCompCGGrid").innerHTML="<div id='InCompWait' style='display:none;text-align:center;padding-top:0px;'><img src='images/loadingAnimation.gif' /> <br>"+resourceJSON.MsgLoad+"</div>";
}

function getWithdrawnCandidateGrid()
{	
	$('#WithDrWait').show();
	var jobId=document.getElementById("jobId").value;
	var firstName		=	trim(document.getElementById("firstName").value); 
	var lastName		=	trim(document.getElementById("lastName").value);
	var emailAddress	=	trim(document.getElementById("emailAddress").value);
	var certType		=	trim(document.getElementById("certificateTypeMaster").value);
	var regionId		=	trim(document.getElementById("regionId").value);
	var degreeId		=	trim(document.getElementById("degreeId").value);
	var universityId		=	trim(document.getElementById("universityId").value);
	var normScoreSelectVal		=	trim(document.getElementById("normScoreSelectVal").value);
	var normScore		=	trim(document.getElementById("normScore").value);
	var CGPA		=	trim(document.getElementById("CGPA").value);
	var CGPASelectVal		=	trim(document.getElementById("CGPASelectVal").value);
	var contacted		=	trim(document.getElementById("contacted").value);
	var candidateStatus		=	trim(document.getElementById("candidateStatus").value);
	var stateId=document.getElementById("stateId").value;
	var tagsearchId		=	trim(document.getElementById("tagsearchId").value);
	
	try{
		var ssn	= trim(document.getElementById("ssn").value);
	}catch(err){}
	
	try{
		var ifrmYOTE = document.getElementById('ifrmYOTE');
		var innerYOTE = ifrmYOTE.contentDocument || ifrmYOTE.contentWindow.document;
		var inputYOTE = innerYOTE.getElementById('YOTEFrm');
		document.getElementById("YOTE").value=inputYOTE.value;
	}catch(e){}
	var YOTESelectVal		=	trim(document.getElementById("YOTESelectVal").value);
	var YOTE				=	document.getElementById("YOTE").value;
	var hiddenCertTypeIds 	=	document.getElementsByName("hiddenCertTypeId");
	var stateId2	=	trim(document.getElementById("stateId2").value);
    var zipCode		=	trim(document.getElementById("zipCode").value);
	var certIds =[];
	for(var i=0;i<hiddenCertTypeIds.length;i++)
	{
		certIds.push(hiddenCertTypeIds[i].value);
	}
	var epiFromDate = "";
	var epiToDate = "";
     try{
	var epiFromDate	=	trim(document.getElementById("epiFromDate").value);
    var epiToDate	=	trim(document.getElementById("epiToDate").value);
     }catch(err){}
	
	CandidateGridAjaxNew.getWithdrawnCandidateReportGrid(jobId,firstName,lastName,emailAddress,certType,regionId,degreeId,universityId,normScoreSelectVal,
			normScore,CGPA,CGPASelectVal,contacted,candidateStatus,stateId,orderColumn, sortingOrder,totalNoOfRecord,tagsearchId,callbreakup,callNoofRecords,
			certIds,YOTESelectVal,YOTE,stateId2,zipCode,epiFromDate,epiToDate,ssn,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{		
		document.getElementById("divWithDrCGGrid").innerHTML=data;
		
		//add by Ram Nath
		var table="divWithDrCGGrid";//add by Ram Nath
		var listLength=document.getElementById("withDrawListSize").value;
		if(callbreakup)
			if(listLength>1)
				getBreakUpRecords(table,listLength,'withDrawtr_',$("[name='sessionListNameWithDraw"+jobId+"']").val(),1);
		//end by Ram Nath
			
			displayTotalCGRecord(6,0);
			var startPoint=totalNoOfRecord-sectionRecordWithDr;
			for(var j=startPoint;j<=totalNoOfRecord;j++)
			{	
				$('#tpResume'+j).tooltip();
				$('#tpPortfolio'+j).tooltip();
				$('#tpJSA'+j).tooltip();
				$('#hireTp'+j).tooltip();
				$('#tpPDReport'+j).tooltip();
				$('#tpCL'+j).tooltip();
				$('#tpAct'+j).tooltip();
				$('#tpMsg'+j).tooltip();
				$('#tpNotes'+j).tooltip();		
				$('#tpTrans'+j).tooltip();
				$('#tpCert'+j).tooltip();
				$('#tpMessage'+j).tooltip();
				$('#tpPhone'+j).tooltip();
				$('#tpInv'+j).tooltip();
				$('#tprr'+j).tooltip();	
				$('#ftsbu'+j).tooltip();
				$('#tooltipPreview'+j).tooltip()
				$('#tpref'+j).tooltip();
				$('#teacherName_'+j).tooltip();
				$('#i4Ques'+j).tooltip();
				$('#teacher_PNQ'+j).tooltip();
				try { $('#alum'+j).tooltip(); } catch (e) {}
				try{ $('#tpInternalCandidate'+j).tooltip(); }catch(e){}
				try{ $('.formerEmployee').tooltip(); }catch(e){}
				try{ $('.returedEmployee').tooltip(); }catch(e){}
				try{$('#tpRefChk'+j).tooltip(); }catch(e){}
				try{$('#dtFailed'+j).tooltip(); }catch(e){}
				try{$('#tpAssessmentinvite'+j).tooltip(); }catch(e){}
				try{$('#pc'+j).tooltip(); }catch(e){}
				try{$('#cndNotReviewed'+j).tooltip(); }catch(e){}
				var candidateFlag=document.getElementById("candidateflag").value;
				if(candidateFlag==1)
				{
					var noOfTagsFile=document.getElementById("noOfTagsFile").value;
					if(noOfTagsFile>15){
						iWidth=600;
					}
					else
					{
						iWidth=300;
					}
				}
				else
				{
					iWidth=300;
				}
				$('#tag'+j).popover({ 
				    html : true,
				    placement: 'right',
				    content: $('#tagDiv'+j).html(),
				  }).on("mouseenter", function () {
					  var height=setDialogPosition();
					  height=height+80;
					  	var style = $('<style>.popover { position: absolute;top: '+height+'px !important;left: 0px;width: '+iWidth+'px; }</style>')
						$('html > head').append(style);
					  	
				        var _this = this;
				        $(this).popover("show");
				        $(this).siblings(".popover").on("mouseleave", function () {
				            //$(_this).popover('hide');
				        });
				    }).on("mouseleave", function () {
				        var _this = this;
				        setTimeout(function () {
				            if (!$(".popover:hover").length) {
				                //$(_this).popover("hide")
				            }
				        }, 100);
				    });
				
				/*$('.profile'+j).popover({ 
				 	trigger: "manual",
				    html : true,
				    placement: 'right',
				    content: $('#profileDetails'+j).html(),
				  }).on("mouseenter", function () {
				        var _this = this;
				        $(this).popover("show");
				        $(this).siblings(".popover").on("mouseleave", function () {
				            $(_this).popover('hide');
				        });
				    }).on("mouseleave", function () {
				        var _this = this;
				        setTimeout(function () {
				            if (!$(".popover:hover").length) {
				                $(_this).popover("hide")
				            }
				        }, 100);
				    });*/
			}
			
			getWithdrawnScoreDetails();
			
		}});
}
function hideWithdrawnCandidateGrid()
{		
	document.getElementById("divWithDrCGGrid").innerHTML="<div id='WithDrWait' style='display:none;text-align:center;padding-top:0px;'><img src='images/loadingAnimation.gif' /> <br>"+resourceJSON.MsgLoad+"</div>";
}
function getInternalCandidateGrid()
{	
	var delayMin=100;
	$('#InterWait').show();
	var jobId=document.getElementById("jobId").value;
	var firstName		=	trim(document.getElementById("firstName").value); 
	var lastName		=	trim(document.getElementById("lastName").value);
	var emailAddress	=	trim(document.getElementById("emailAddress").value);
	var certType		=	trim(document.getElementById("certificateTypeMaster").value);
	var regionId		=	trim(document.getElementById("regionId").value);
	var degreeId		=	trim(document.getElementById("degreeId").value);
	var universityId		=	trim(document.getElementById("universityId").value);
	var normScoreSelectVal		=	trim(document.getElementById("normScoreSelectVal").value);
	var normScore		=	trim(document.getElementById("normScore").value);
	var CGPA		=	trim(document.getElementById("CGPA").value);
	var CGPASelectVal		=	trim(document.getElementById("CGPASelectVal").value);
	var contacted		=	trim(document.getElementById("contacted").value);
	var candidateStatus		=	trim(document.getElementById("candidateStatus").value);
	var stateId=document.getElementById("stateId").value;
	CandidateGridAjaxNew.getInternalCandidateReportGrid(jobId,firstName,lastName,emailAddress,certType,regionId,degreeId,universityId,normScoreSelectVal,
			normScore,CGPA,CGPASelectVal,contacted,candidateStatus,stateId,orderColumn, sortingOrder,totalNoOfRecord,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			document.getElementById("divInternalCGGrid").innerHTML=data;
		
			displayTotalCGRecord(2,0)
			var startPoint=totalNoOfRecord-sectionRecordInter;
			for(var j=startPoint;j<=totalNoOfRecord;j++)
			{
				
				
				$('#tpResume'+j).tooltip();
				$('#tpPortfolio'+j).tooltip();
				$('#tpJSA'+j).tooltip();
				$('#hireTp'+j).tooltip();
				$('#tpPDReport'+j).tooltip();
				$('#tpCL'+j).tooltip();
				$('#tpAct'+j).tooltip();
				$('#tpMsg'+j).tooltip();
				$('#tpNotes'+j).tooltip();		
				$('#tpTrans'+j).tooltip();
				$('#tpCert'+j).tooltip();
				$('#tpMessage'+j).tooltip();
				$('#tpPhone'+j).tooltip();
				$('#tpInv'+j).tooltip();
				$('#tprr'+j).tooltip();	
				$('#ftsbu'+j).tooltip();
				$('#tooltipPreview'+j).tooltip()
				$('#tpref'+j).tooltip();
				$('#teacherName_'+j).tooltip();
				$('#i4Ques'+j).tooltip();
				$('#teacher_PNQ'+j).tooltip();
				try { $('#alum'+j).tooltip(); } catch (e) {}
				try{ $('#tpInternalCandidate'+j).tooltip(); }catch(e){}
				try{ $('.formerEmployee').tooltip(); }catch(e){}
				try{ $('.returedEmployee').tooltip(); }catch(e){}
				try{$('#tpRefChk'+j).tooltip(); }catch(e){}
				try{$('#dtFailed'+j).tooltip(); }catch(e){}
				try{$('#tpAssessmentinvite'+j).tooltip(); }catch(e){}
				try{$('#pc'+j).tooltip(); }catch(e){}
				try{$('#cndNotReviewed'+j).tooltip(); }catch(e){}
				var candidateFlag=document.getElementById("candidateflag").value;
				if(candidateFlag==1)
				{
					var noOfTagsFile=document.getElementById("noOfTagsFile").value;
					if(noOfTagsFile>15){
						iWidth=600;
					}
					else
					{
						iWidth=300;
					}
				}
				else
				{
					iWidth=300;
				}
				$('#tag'+j).popover({ 
				    html : true,
				    placement: 'right',
				    content: $('#tagDiv'+j).html(),
				  }).on("mouseenter", function () {
					  var height=setDialogPosition();
					  height=height+80;	
					  var style = $('<style>.popover { position: absolute;top: '+height+'px !important;left: 0px;width: '+iWidth+'px; }</style>')
						$('html > head').append(style);
					  	
				        var _this = this;
				        $(this).popover("show");
				        $(this).siblings(".popover").on("mouseleave", function () {
				            //$(_this).popover('hide');
				        });
				    }).on("mouseleave", function () {
				        var _this = this;
				        setTimeout(function () {
				            if (!$(".popover:hover").length) {
				                //$(_this).popover("hide")
				            }
				        }, 100);
				    });
				
				/*$('.profile'+j).popover({ 
				 	trigger: "manual",
				    html : true,
				    placement: 'right',
				    content: $('#profileDetails'+j).html(),
				  }).on("mouseenter", function () {
				        var _this = this;
				        $(this).popover("show");
				        $(this).siblings(".popover").on("mouseleave", function () {
				            $(_this).popover('hide');
				        });
				    }).on("mouseleave", function () {
				        var _this = this;
				        setTimeout(function () {
				            if (!$(".popover:hover").length) {
				                $(_this).popover("hide")
				            }
				        }, 100);
				    });*/
			}
			
			getInternalScoreDetails(); // Ramesh
		}});
}
function hideInternalCandidateGrid()
{		
	document.getElementById("divInternalCGGrid").innerHTML="<div id='InterWait' style='display:none;text-align:center;padding-top:0px;'><img src='images/loadingAnimation.gif' /> <br>"+resourceJSON.MsgLoad+"</div>";
}

function getHiredCandidateGrid()
{	
	
	$('#hiredWait').show();
	var jobId=document.getElementById("jobId").value;
	var firstName		=	trim(document.getElementById("firstName").value); 
	var lastName		=	trim(document.getElementById("lastName").value);
	var emailAddress	=	trim(document.getElementById("emailAddress").value);
	var certType		=	trim(document.getElementById("certificateTypeMaster").value);
	var regionId		=	trim(document.getElementById("regionId").value);
	var degreeId		=	trim(document.getElementById("degreeId").value);
	var universityId		=	trim(document.getElementById("universityId").value);
	var normScoreSelectVal		=	trim(document.getElementById("normScoreSelectVal").value);
	var normScore		=	trim(document.getElementById("normScore").value);
	var CGPA		=	trim(document.getElementById("CGPA").value);
	var CGPASelectVal		=	trim(document.getElementById("CGPASelectVal").value);
	var contacted		=	trim(document.getElementById("contacted").value);
	var candidateStatus		=	trim(document.getElementById("candidateStatus").value);
	var stateId=document.getElementById("stateId").value;
	var tagsearchId		=	trim(document.getElementById("tagsearchId").value);
	
	try{
		var ssn	= trim(document.getElementById("ssn").value);
		var notReviewedFlag	= document.getElementById("notReviewedFlag").checked;
	}catch(err){}
	
	try{
		var ifrmYOTE = document.getElementById('ifrmYOTE');
		var innerYOTE = ifrmYOTE.contentDocument || ifrmYOTE.contentWindow.document;
		var inputYOTE = innerYOTE.getElementById('YOTEFrm');
		document.getElementById("YOTE").value=inputYOTE.value;
	}catch(e){}
	var YOTESelectVal		=	trim(document.getElementById("YOTESelectVal").value);
	var YOTE				=	document.getElementById("YOTE").value;
	var hiddenCertTypeIds 	=	document.getElementsByName("hiddenCertTypeId");
	var stateId2	=	trim(document.getElementById("stateId2").value);
    var zipCode		=	trim(document.getElementById("zipCode").value);
	var certIds =[];
	for(var i=0;i<hiddenCertTypeIds.length;i++)
	{
		certIds.push(hiddenCertTypeIds[i].value);
	}
	var epiFromDate = "";
	var epiToDate = "";
     try{
	var epiFromDate	=	trim(document.getElementById("epiFromDate").value);
    var epiToDate	=	trim(document.getElementById("epiToDate").value);
     }catch(err){}
	
	CandidateGridAjaxNew.getHiredCandidateReportGrid(jobId,firstName,lastName,emailAddress,certType,regionId,degreeId,universityId,normScoreSelectVal,
			normScore,CGPA,CGPASelectVal,contacted,candidateStatus,stateId,orderColumn, sortingOrder,totalNoOfRecord,tagsearchId,callbreakup,callNoofRecords,
			certIds,YOTESelectVal,YOTE,stateId2,zipCode,epiFromDate,epiToDate,ssn,notReviewedFlag,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{		
		document.getElementById("divHiredCGGrid").innerHTML=data;
		
		//add by ram nath for 10-10 record
		var table="divHiredCGGrid";//add by Ram Nath
		var listLength=document.getElementById("hiredListSize").value;
		if(callbreakup)
			if(listLength>1)
				getBreakUpRecords(table,listLength,'Hiredtr_',$("[name='sessionListNameHired"+jobId+"']").val(),1);
		//end by ram nath for 10-10 record
			
			displayTotalCGRecord(7,0)
			var startPoint=totalNoOfRecord-sectionRecordHired;
			for(var j=startPoint;j<=totalNoOfRecord;j++)
			{	
				$('#tpResume'+j).tooltip();
				$('#tpPortfolio'+j).tooltip();
				$('#tpJSA'+j).tooltip();
				$('#hireTp'+j).tooltip();
				$('#tpPDReport'+j).tooltip();
				$('#tpCL'+j).tooltip();
				$('#tpAct'+j).tooltip();
				$('#tpMsg'+j).tooltip();
				$('#tpNotes'+j).tooltip();		
				$('#tpTrans'+j).tooltip();
				$('#tpCert'+j).tooltip();
				$('#tpMessage'+j).tooltip();
				$('#tpPhone'+j).tooltip();
				$('#tpInv'+j).tooltip();
				$('#tprr'+j).tooltip();	
				$('#ftsbu'+j).tooltip();
				$('#tooltipPreview'+j).tooltip()
				$('#tpref'+j).tooltip();
				$('#teacherName_'+j).tooltip();
				$('#i4Ques'+j).tooltip();
				$('#teacher_PNQ'+j).tooltip();
				try { $('#alum'+j).tooltip(); } catch (e) {}
				try{ $('#tpInternalCandidate'+j).tooltip(); }catch(e){}
				try{ $('.formerEmployee').tooltip(); }catch(e){}
				try{ $('.returedEmployee').tooltip(); }catch(e){}
				try{$('#tpRefChk'+j).tooltip(); }catch(e){}
				try{$('#dtFailed'+j).tooltip(); }catch(e){}
				try{$('#tpAssessmentinvite'+j).tooltip(); }catch(e){}
				try{$('#pc'+j).tooltip(); }catch(e){}
				try{$('#cndNotReviewed'+j).tooltip(); }catch(e){}
				var candidateFlag=document.getElementById("candidateflag").value;
				if(candidateFlag==1)
				{
					var noOfTagsFile=document.getElementById("noOfTagsFile").value;
					if(noOfTagsFile>15){
						iWidth=600;
					}
					else
					{
						iWidth=300;
					}
				}
				else
				{
					iWidth=300;
				}
				$('#tag'+j).popover({ 
				    html : true,
				    placement: 'right',
				    content: $('#tagDiv'+j).html(),
				  }).on("mouseenter", function () {
					  var height=setDialogPosition();
					  height=height+80;	
					  	var style = $('<style>.popover { position: absolute;top: '+height+'px !important;left: 0px;width: '+iWidth+'px; }</style>')
						$('html > head').append(style);
					  	
				        var _this = this;
				        $(this).popover("show");
				        $(this).siblings(".popover").on("mouseleave", function () {
				            //$(_this).popover('hide');
				        });
				    }).on("mouseleave", function () {
				        var _this = this;
				        setTimeout(function () {
				            if (!$(".popover:hover").length) {
				                //$(_this).popover("hide")
				            }
				        }, 100);
				    });
				
				/*$('.profile'+j).popover({ 
				 	trigger: "manual",
				    html : true,
				    placement: 'right',
				    content: $('#profileDetails'+j).html(),
				  }).on("mouseenter", function () {
				        var _this = this;
				        $(this).popover("show");
				        $(this).siblings(".popover").on("mouseleave", function () {
				            $(_this).popover('hide');
				        });
				    }).on("mouseleave", function () {
				        var _this = this;
				        setTimeout(function () {
				            if (!$(".popover:hover").length) {
				                $(_this).popover("hide")
				            }
				        }, 100);
				    });*/
			}
			
			getHiredScoreDetails();
		}});
}
function hideHiredCandidateGrid()
{		
	document.getElementById("divHiredCGGrid").innerHTML="<div id='hiredWait' style='display:none;text-align:center;padding-top:0px;'><img src='images/loadingAnimation.gif' /> <br>"+resourceJSON.MsgLoad+"</div>";
}
function getVltCandidateGrid()
{	
	$('#vltWait').show();
	var jobId=document.getElementById("jobId").value;
	var firstName		=	trim(document.getElementById("firstName").value); 
	var lastName		=	trim(document.getElementById("lastName").value);
	var emailAddress	=	trim(document.getElementById("emailAddress").value);
	var certType		=	trim(document.getElementById("certificateTypeMaster").value);
	var regionId		=	trim(document.getElementById("regionId").value);
	var degreeId		=	trim(document.getElementById("degreeId").value);
	var universityId		=	trim(document.getElementById("universityId").value);
	var normScoreSelectVal		=	trim(document.getElementById("normScoreSelectVal").value);
	var normScore		=	trim(document.getElementById("normScore").value);
	var CGPA		=	trim(document.getElementById("CGPA").value);
	var CGPASelectVal		=	trim(document.getElementById("CGPASelectVal").value);
	var contacted		=	trim(document.getElementById("contacted").value);
	var candidateStatus		=	trim(document.getElementById("candidateStatus").value);
	var stateId=document.getElementById("stateId").value;
	var tagsearchId		=	trim(document.getElementById("tagsearchId").value);
	
	try{
		var ssn	= trim(document.getElementById("ssn").value);
	}catch(err){}
	
	try{
		var ifrmYOTE = document.getElementById('ifrmYOTE');
		var innerYOTE = ifrmYOTE.contentDocument || ifrmYOTE.contentWindow.document;
		var inputYOTE = innerYOTE.getElementById('YOTEFrm');
		document.getElementById("YOTE").value=inputYOTE.value;
	}catch(e){}
	var YOTESelectVal		=	trim(document.getElementById("YOTESelectVal").value);
	var YOTE				=	document.getElementById("YOTE").value;
	var hiddenCertTypeIds 	=	document.getElementsByName("hiddenCertTypeId");
	var stateId2	=	trim(document.getElementById("stateId2").value);
    var zipCode		=	trim(document.getElementById("zipCode").value);
	var certIds =[];
	for(var i=0;i<hiddenCertTypeIds.length;i++)
	{
		certIds.push(hiddenCertTypeIds[i].value);
	}
	var epiFromDate = "";
	var epiToDate = "";
     try{
	var epiFromDate	=	trim(document.getElementById("epiFromDate").value);
    var epiToDate	=	trim(document.getElementById("epiToDate").value);
     }catch(err){}

	
	CandidateGridAjaxNew.getVltCandidateReportGrid(jobId,firstName,lastName,emailAddress,certType,regionId,degreeId,universityId,normScoreSelectVal,
			normScore,CGPA,CGPASelectVal,contacted,candidateStatus,stateId,orderColumn, sortingOrder,totalNoOfRecord,tagsearchId,callbreakup,callNoofRecords,
			certIds,YOTESelectVal,YOTE,stateId2,zipCode,epiFromDate,epiToDate,ssn,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
		
		document.getElementById("divVltCGGrid").innerHTML=data;
		
		//add by ram nath for 10-10 record
		var table="divVltCGGrid";//add by Ram Nath
		var listLength=document.getElementById("timeOutListSize").value;
		if(callbreakup)
			if(listLength>1)
				getBreakUpRecords(table,listLength,'TimeOuttr_',$("[name='sessionListNameTimeOut"+jobId+"']").val(),1);
		//end by ram nath for 10-10 record
			
			displayTotalCGRecord(3,0)
			var startPoint=totalNoOfRecord-sectionRecordVlt;
			for(var j=startPoint;j<=totalNoOfRecord;j++)
			{	
				$('#tpResume'+j).tooltip();
				$('#tpPortfolio'+j).tooltip();
				$('#tpJSA'+j).tooltip();
				$('#hireTp'+j).tooltip();
				$('#tpPDReport'+j).tooltip();
				$('#tpCL'+j).tooltip();
				$('#tpAct'+j).tooltip();
				$('#tpMsg'+j).tooltip();
				$('#tpNotes'+j).tooltip();		
				$('#tpTrans'+j).tooltip();
				$('#tpCert'+j).tooltip();
				$('#tpMessage'+j).tooltip();
				$('#tpPhone'+j).tooltip();
				$('#tpInv'+j).tooltip();
				$('#tprr'+j).tooltip();	
				$('#ftsbu'+j).tooltip();
				$('#tooltipPreview'+j).tooltip()
				$('#tpref'+j).tooltip();
				$('#teacherName_'+j).tooltip();
				$('#i4Ques'+j).tooltip();
				$('#teacher_PNQ'+j).tooltip();
				try { $('#alum'+j).tooltip(); } catch (e) {}
				try{ $('#tpInternalCandidate'+j).tooltip(); }catch(e){}
				try{ $('.formerEmployee').tooltip(); }catch(e){}
				try{ $('.returedEmployee').tooltip(); }catch(e){}
				try{$('#tpRefChk'+j).tooltip(); }catch(e){}
				try{$('#dtFailed'+j).tooltip(); }catch(e){}
				try{$('#tpAssessmentinvite'+j).tooltip(); }catch(e){}
				try{$('#pc'+j).tooltip(); }catch(e){}
				try{$('#cndNotReviewed'+j).tooltip(); }catch(e){}
				var candidateFlag=document.getElementById("candidateflag").value;
				if(candidateFlag==1)
				{
					var noOfTagsFile=document.getElementById("noOfTagsFile").value;
					if(noOfTagsFile>15){
						iWidth=600;
					}
					else
					{
						iWidth=300;
					}
				}
				else
				{
					iWidth=300;
				}
				$('#tag'+j).popover({ 
				    html : true,
				    placement: 'right',
				    content: $('#tagDiv'+j).html(),
				  }).on("mouseenter", function () {
					  var height=setDialogPosition();
					  height=height+80;	
					  	var style = $('<style>.popover { position: absolute;top: '+height+'px !important;left: 0px;width: '+iWidth+'px; }</style>')
						$('html > head').append(style);
					  	
					  	
				        var _this = this;
				        $(this).popover("show");
				        $(this).siblings(".popover").on("mouseleave", function () {
				            //$(_this).popover('hide');
				        });
				    }).on("mouseleave", function () {
				        var _this = this;
				        setTimeout(function () {
				            if (!$(".popover:hover").length) {
				                //$(_this).popover("hide")
				            }
				        }, 100);
				    });
				
				/*$('.profile'+j).popover({ 
				 	trigger: "manual",
				    html : true,
				    placement: 'right',
				    content: $('#profileDetails'+j).html(),
				  }).on("mouseenter", function () {
				        var _this = this;
				        $(this).popover("show");
				        $(this).siblings(".popover").on("mouseleave", function () {
				            $(_this).popover('hide');
				        });
				    }).on("mouseleave", function () {
				        var _this = this;
				        setTimeout(function () {
				            if (!$(".popover:hover").length) {
				                $(_this).popover("hide")
				            }
				        }, 100);
				    });*/
				
			}
			
			getTimeoutScoreDetails();
		}});
}
function hideVltCandidateGrid()
{		
	document.getElementById("divVltCGGrid").innerHTML="<div id='vltWait' style='display:none;text-align:center;padding-top:0px;'><img src='images/loadingAnimation.gif' /> <br>"+resourceJSON.MsgLoad+"</div>";
}
function tpJbIEnable()
{
	expanddiv=0;
	var noOrRow = document.getElementById("totalNoOfRecord").value;
	$('#hrefPDF').tooltip();
	$('#tpSearch').tooltip();
	$('#tpLegend').tooltip();
	$('#datailsGrid').tooltip();
	$('#scoreGrid').tooltip();
	$('#normCurve').tooltip();
	$('#tpexport').tooltip();
	$('#jobHired').tooltip();
	
	for(var i=1;i<=7;i++){
		$('#jobApplied'+i).tooltip();
	}
	for(var i=1;i<=7;i++){
		$('#assessScore1'+i).tooltip();
	}
	for(var i=1;i<=7;i++){
		$('#assessScore2'+i).tooltip();
	}
	for(var i=1;i<=7;i++){
		$('#assessScore3'+i).tooltip();
	}
	for(var i=1;i<=7;i++){
		$('#assessScore4'+i).tooltip();
	}
	for(var i=1;i<=7;i++){
		$('#assessScore5'+i).tooltip();
	}
	for(var i=1;i<=7;i++){
		$('#assessScore6'+i).tooltip();
	}


	for(var i=1;i<=3;i++){
		$('#oaScore1'+i).tooltip();
	}
	for(var i=1;i<=3;i++){
		$('#oaScore2'+i).tooltip();
	}
	for(var i=1;i<=3;i++){
		$('#oaScore3'+i).tooltip();
	}
	for(var i=1;i<=3;i++){
		$('#oaScore4'+i).tooltip();
	}
	for(var i=1;i<=3;i++){
		$('#oaScore5'+i).tooltip();
	}
	for(var i=1;i<=3;i++){
		$('#oaScore6'+i).tooltip();
	}
	
	
	for(var i=1;i<=6;i++){
		$('#HNS'+i).tooltip();
	}
	$(document).ready(function(){
		 
		/*$("#tpLegend").click(function(){
	    	$("#divToggel").slideToggle("slow");			
	  	});*/
		
		var delayVal=1000;
		var delayMin=100;
		
		$("#scoreGrid").click(function(){ 
				if(document.getElementById("expandchk").value==0){
					expanddiv++;
					if(expanddiv>=2){
						document.getElementById("expandchk").value=1;
						document.getElementById("scoredf").colSpan="2";
						if($("#detailsdf").length>0)
						document.getElementById("detailsdf").colSpan="1";
						document.getElementById("tdscore0tbl").style.textIndent="0px";
						//document.getElementById("compositeScoreGrid").style.textIndent="0px";
						//document.getElementById("scoreright").style.display='inline';
						//document.getElementById("scoreleft").style.display='none';
						$("#scoreGrid").attr('data-original-title', resourceJSON.MsgCollapseTheColumn).tooltip('show');
						
							 $( ".tdscore0:first" ).animate({
							    left :0,
								 width:256
							  }, {
							    duration: 1000
							  });
							 
							 $( "#tdscoremean").delay(delayVal).fadeIn(delayVal);
								
								setTimeout(function(){
								jQuery("#tblGrid tr:first").addClass("bg");
								$("#tdscore0").show();
								jQuery("#scoreright").show();
								jQuery("#scoreleft").hide();
								},1001);
								
						for(var j=1;j<=noOrRow;j++)
						{
							$( "#tdscore"+j ).delay(delayVal).fadeIn(delayVal);
						}
						getInternalScoreDetails();
						getIncompleteScoreDetails();
						getRejectScoreDetails(); 
						getWithdrawnScoreDetails();
						getTimeoutScoreDetails();
						getHiredScoreDetails();
						getHiddenScoreDetails();
						
						$( "#tdscoremean").delay(delayVal).fadeIn(delayVal);
					}else{
						document.getElementById("expandchk").value=1;
						document.getElementById("scoredf").colSpan="2";
						document.getElementById("tdscore0tbl").style.textIndent="0px";
						//document.getElementById("compositeScoreGrid").style.textIndent="0px";
						//document.getElementById("scoreright").style.display='inline';
						//document.getElementById("scoreleft").style.display='none';
						$("#scoreGrid").attr('data-original-title', resourceJSON.MsgCollapseTheColumn).tooltip('show');
							 $( ".tdscore0:first" ).animate({
							    left :0,
								 width:256
							  }, {
								    duration: 1000
							  });
							 setTimeout(function(){
									
									jQuery("#tblGrid tr:first").addClass("bg");
									$("#tdscore0").show();
									jQuery("#scoreright").show();
									jQuery("#scoreleft").hide();
									
								},1001);
							 
							 
						for(var j=1;j<=noOrRow;j++)
						{
							$( "#tdscore"+j ).delay(delayVal).fadeIn(delayVal);
							
							
						}
						
						getInternalScoreDetails();
						getIncompleteScoreDetails();
						getRejectScoreDetails(); 
						getWithdrawnScoreDetails();
						getTimeoutScoreDetails();
						getHiredScoreDetails();
						getHiddenScoreDetails();
						$( "#tdscoremean").delay(delayVal).fadeIn(delayVal);
					}
				}else{
					expanddiv--;
					document.getElementById("expandchk").value=0;
					document.getElementById("scoredf").colSpan="2";
					document.getElementById("tdscore0tbl").style.textIndent="99999px";
					//document.getElementById("compositeScoreGrid").style.textIndent="99999px";
					document.getElementById("scoreright").style.display='none';
					document.getElementById("scoreleft").style.display='inline';
					$("#scoreGrid").attr('data-original-title', resourceJSON.MsgCollapseTheColumn).tooltip('show');
					
					 $( ".tdscore0:first" ).animate({
						    left :0,
							 width:1
						  }, {
						    duration: 1000
						  });
					 setTimeout(function(){
					   $("#tdscore0").hide();
					   jQuery("#tblGrid tr:first").removeClass("bg");
					    $("#scoredefhead").attr("colspan","2");
						},500);
					 
					for(var j=1;j<=noOrRow;j++)
					{
						$( "#tdscore"+j ).delay(delayMin).fadeOut(delayMin);
						
					}
					getInternalScoreDetails(); 
					getIncompleteScoreDetails();
					getRejectScoreDetails();
					getWithdrawnScoreDetails();
					getTimeoutScoreDetails();
					getHiredScoreDetails();
					getHiddenScoreDetails();
					$( "#tdscoremean").delay(delayMin).fadeOut(delayMin);
				}
				
	  	});
		
		$("#removeGrid").click(function(){
			if(document.getElementById("expandremovechk").value==0){
				getRemoveCandidateGrid();
				document.getElementById("expandremovechk").value=1;
				document.getElementById("removeasc").style.display='none';
				document.getElementById("removedsc").style.display='inline';
			}else{
				displayTotalCGRecord(4,1);
				document.getElementById("expandremovechk").value=0;
				document.getElementById("removeasc").style.display='inline';
				document.getElementById("removedsc").style.display='none';
				hideRemoveCandidateGrid();
			}
			
		});
		
		$("#incompGrid").click(function(){
			if(document.getElementById("expandincompchk").value==0){
				getInCompleteCandidateGrid();
				document.getElementById("expandincompchk").value=1;
				document.getElementById("incompasc").style.display='none';
				document.getElementById("incompdsc").style.display='inline';
			}else{
				displayTotalCGRecord(5,1);
				document.getElementById("expandincompchk").value=0;
				document.getElementById("incompasc").style.display='inline';
				document.getElementById("incompdsc").style.display='none';
				hideInCompleteCandidateGrid();
			}
			
		});
		$("#interGrid").click(function(){
			if(document.getElementById("expandinterchk").value==0){
				getInternalCandidateGrid();
				document.getElementById("expandinterchk").value=1;
				document.getElementById("interasc").style.display='none';
				document.getElementById("interdsc").style.display='inline';
			}else{
				displayTotalCGRecord(2,1);
				document.getElementById("expandinterchk").value=0;
				document.getElementById("interasc").style.display='inline';
				document.getElementById("interdsc").style.display='none';
				hideInternalCandidateGrid();
			}
			
			
			
		});
		
		$("#hiredGrid").click(function(){
			if(document.getElementById("expandhiredchk").value==0){
				getHiredCandidateGrid();
				document.getElementById("expandhiredchk").value=1;
				document.getElementById("hiredasc").style.display='none';
				document.getElementById("hireddsc").style.display='inline';
			}else{
				displayTotalCGRecord(7,1);
				document.getElementById("expandhiredchk").value=0;
				document.getElementById("hiredasc").style.display='inline';
				document.getElementById("hireddsc").style.display='none';
				hideHiredCandidateGrid();
			}
			
		});
		$("#vltGrid").click(function(){
			if(document.getElementById("expandvltchk").value==0){
				getVltCandidateGrid();
				document.getElementById("expandvltchk").value=1;
				document.getElementById("vltasc").style.display='none';
				document.getElementById("vltdsc").style.display='inline';
			}else{
				displayTotalCGRecord(3,1);
				document.getElementById("expandvltchk").value=0;
				document.getElementById("vltasc").style.display='inline';
				document.getElementById("vltdsc").style.display='none';
				hideVltCandidateGrid();
			}
			
		});
		$("#withdrGrid").click(function(){
			if(document.getElementById("expandwithdrchk").value==0){
				getWithdrawnCandidateGrid();
				document.getElementById("expandwithdrchk").value=1;
				document.getElementById("withdrasc").style.display='none';
				document.getElementById("withdrdsc").style.display='inline';
			}else{
				displayTotalCGRecord(6,1);
				document.getElementById("expandwithdrchk").value=0;
				document.getElementById("withdrasc").style.display='inline';
				document.getElementById("withdrdsc").style.display='none';
				hideWithdrawnCandidateGrid();
			}
		});
		
		$("#hiddenGrid").click(function(){
			if(document.getElementById("expandhiddenchk").value==0){
				getHiddenCandidateGrid();
				document.getElementById("expandhiddenchk").value=1;
				document.getElementById("hiddenasc").style.display='none';
				document.getElementById("hiddendsc").style.display='inline';
			}else{
				displayTotalCGRecord(8,1);
				document.getElementById("expandhiddenchk").value=0;
				document.getElementById("hiddenasc").style.display='inline';
				document.getElementById("hiddendsc").style.display='none';
				hideHiddenCandidateGrid();
			}
		});
		
	});
	
	
	for(var j=1;j<=noOrRow;j++)
	{	
		$('#tpResume'+j).tooltip();
		$('#tpPortfolio'+j).tooltip();
		$('#hireTp'+j).tooltip();
		$('#hireTp'+j).tooltip();
		$('#tpJSA'+j).tooltip();
		$('#tpPDReport'+j).tooltip();
		$('#tpCL'+j).tooltip();
		$('#tpAct'+j).tooltip();
		$('#tpMsg'+j).tooltip();
		$('#tpNotes'+j).tooltip();		
		$('#tpTrans'+j).tooltip();
		$('#tpCert'+j).tooltip();
		$('#tpMessage'+j).tooltip();
		$('#tpPhone'+j).tooltip();
		$('#aNotes'+j).tooltip();
		$('#tpref'+j).tooltip();
		$('#tpInv'+j).tooltip();
		$('#tprr'+j).tooltip();	
		$('#ftsbu'+j).tooltip();
		$('#tooltipPreview'+j).tooltip()
		$('#removeStatus'+j).tooltip();
		$('#teacherName_'+j).tooltip();
		$('#i4Ques'+j).tooltip();
		$('#teacher_PNQ'+j).tooltip();
		try { $('#alum'+j).tooltip(); } catch (e) {}
		try{ $('#tpInternalCandidate'+j).tooltip(); }catch(e){}
		try{ $('.formerEmployee').tooltip(); }catch(e){}
		try{ $('.returedEmployee').tooltip(); }catch(e){}
		try{$('#tpRefChk'+j).tooltip(); }catch(e){}
		try{$('#dtFailed'+j).tooltip(); }catch(e){}
		try{$('#tpAssessmentinvite'+j).tooltip(); }catch(e){}
		try{$('#pc'+j).tooltip(); }catch(e){}
		try{$('#cndNotReviewed'+j).tooltip(); }catch(e){}
		/*var candidateFlag=document.getElementById("candidateflag").value;
		if(candidateFlag==1)
		{
			var noOfTagsFile=document.getElementById("noOfTagsFile").value;
			if(noOfTagsFile>15){
				iWidth=600;
			}
			else
			{
				iWidth=300;
			}
		}
		else
		{
			iWidth=300;
		}*/
		
		/*$('#tag'+j).popover({ 
		    html : true,
		    placement: 'right',
		    content: $('#tagDiv'+j).html(),
		  }).on("mouseenter", function () {
			  var height=setDialogPosition();
			  height=height+80;	
			  	var style = $('<style>.popover { position: absolute;top: '+height+'px !important;left: 0px;width: '+iWidth+'px; }</style>')
				$('html > head').append(style);
			  	
			  	
		        var _this = this;
		        $(this).popover("show");
		        $(this).siblings(".popover").on("mouseleave", function () {
		            $(_this).popover('hide');
		        });
		    }).on("mouseleave", function () {
		        var _this = this;
		        setTimeout(function () {
		            if (!$(".popover:hover").length) {
		                $(_this).popover("hide")
		            }
		        }, 100);
		    });*/
		
		/*$('.profile'+j).popover({ 
		 	trigger: "manual",
		    html : true,
		    placement: 'right',
		    content: $('#profileDetails'+j).html(),
		  }).on("mouseenter", function () {
		        var _this = this;
		        $(this).popover("show");
		        $(this).siblings(".popover").on("mouseleave", function () {
		            $(_this).popover('hide');
		        });
		    }).on("mouseleave", function () {
		        var _this = this;
		        setTimeout(function () {
		            if (!$(".popover:hover").length) {
		                $(_this).popover("hide")
		            }
		        }, 100);
		    });*/
		
		
		/*$('#tagaction'+j).popover({ 
		    html : true,
		    placement: 'right',
		    content: $('#tagDiv'+j).html(),
		  });*/
	}
}

function closeTagActAction(j,jftId,tid,jid)
{
	//alert(" closeActAction tagDiv"+j)
	//$('#cgTeacherDivMaster').modal('hide');
	var showType=$("#showHideType").val();
	try{
		if(showType==0)
			$('#cgTeacherDivMaster').modal('hide');
		else
			$('#cgTeacherMasterDiv').modal('hide');   
	}catch(err)
	  {}
	$('#profile'+j).popover('hide');
	
	showTagDetails(j,jftId,tid,jid);
	
	$('#tag'+j).popover('show');
	$('#tag'+j).siblings(".popover").on("click", function () {
		})/*.on("mouseleave", function () {
			setTimeout(function () {
			if (!$(".popover:hover").length) {
				//$('#tag'+j).popover("hide")
			}
			}, 1000);
	})*/;
		
}

function closeAllTag(no,val)
{
	//alert(val);
	var noOrRow = no;
	//alert("closeAllTag noOrRow "+noOrRow);
	for(var j=1;j<=noOrRow;j++)
	{	
	//	$('#tag'+j).popover('hide');
	}
}

function tpJbIDisable()
{
	var noOrRow = document.getElementById("tblGrid").rows.length;
	
	for(var j=1;j<=noOrRow;j++)
	{
		$('#tpResume'+j).tooltip();
		$('#tpPortfolio'+j).tooltip();
		$('#tpJSA'+j).tooltip();
		$('#hireTp'+j).tooltip();
		$('#tpPDReport'+j).tooltip();
		$('#tpCL'+j).tooltip();
		$('#tpAct'+j).tooltip();
		$('#tpMsg'+j).tooltip();
		$('#tpNotes'+j).tooltip();
		$('#tpTrans'+j).tooltip();	
		$('#tpCert'+j).tooltip();
		$('#tpMessage'+j).tooltip();
		$('#tpInv'+j).tooltip();
		$('#tprr'+j).tooltip();	
		$('#ftsbu'+j).tooltip();
		$('#tooltipPreview'+j).tooltip()
		$('#teacherName_'+j).tooltip();
		$('#i4Ques'+j).tooltip();
		$('#teacher_PNQ'+j).tooltip();
		try { $('#alum'+j).tooltip(); } catch (e) {}
		try{ $('#tpInternalCandidate'+j).tooltip(); }catch(e){}
		try{ $('.formerEmployee').tooltip(); }catch(e){}
		try{ $('.returedEmployee').tooltip(); }catch(e){}
		try{$('#tpRefChk'+j).tooltip(); }catch(e){}
		try{$('#dtFailed'+j).tooltip(); }catch(e){}
		try{$('#tpAssessmentinvite'+j).tooltip(); }catch(e){}
		try{$('#pc'+j).tooltip(); }catch(e){}
		try{$('#cndNotReviewed'+j).tooltip(); }catch(e){}
	}
}
////////// Note scroll 
function applyScrollOnNotesTbl()
{
	try
	{
		var $j;//=jQuery.noConflict(false);
        jQuery(document).ready(function($j) {
        $j('#tblNotes').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 100,
        width: 875,
        minWidth: null,
        minWidthAuto: false,
        colratio:[100,150,475,150], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });            
        });
	}catch(e){alert(e)}
}
//////////Message scroll 
function applyScrollOnMessageTbl()
{
		var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblMessages').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 70,
        width: 614,
        minWidth: null,
        minWidthAuto: false,
        colratio:[100,140,100,215,60], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });            
        });
}

function openWindow(url,windowName,width,height)
{
	windowName=windowName||"_blank";width=width||900;height=height||600;
	var left=(window.screen.availWidth-width)/2;
	var top=(window.screen.availHeight-height)/2;
	var mywindow=window.open(url,windowName,'width='+width+',height='+height+',left='+left+',top='+top+',scrollbars=1');
	var len;

	try{
		len=mywindow.length;
	}catch(err){}

	if ((!mywindow) || len==undefined || mywindow.length!=0 ){
		{
			$('#blockMessage').html(""+resourceJSON.PopUpWindowBlock+" <a target = '_blank' href='"+url+"'>"+resourceJSON.BtnClick+"</a> "+resourceJSON.ToOpenNewWindow+".");
			//$('#myModal').modal('show');
			try{
				$('#myModal').modal('show');
			}catch(err)
			  {}
		}
	}else
		mywindow.focus();

	//for chrome only
	if (navigator && (navigator.userAgent.toLowerCase()).indexOf("chrome") > -1) {
		if (!mywindow)
		{
			$('#blockMessage').html(""+resourceJSON.PopUpWindowBlock+" <a target = '_blank' href='"+url+"'>"+resourceJSON.BtnClick+"</a> "+resourceJSON.ToOpenNewWindow+".");
			//$('#myModal').modal('show');
			try{
				$('#myModal').modal('show');
			}catch(err)
			  {}
		}
		else {
			mywindow.onload = function() {
				setTimeout(function() {
					if (mywindow.screenX === 0) {
						$('#blockMessage').html(""+resourceJSON.PopUpWindowBlock+" <a target = '_blank' href='"+url+"'>"+resourceJSON.BtnClick+"</a> "+resourceJSON.ToOpenNewWindow+".");
						//$('#myModal').modal('show');
						try{
							$('#myModal').modal('show');
						}catch(err)
						  {}
						mywindow.close();
					}
				}, 0);
			};
		}
	}
}
function pausecomp(ms) {
	ms += new Date().getTime();
	while (new Date() < ms){}
	} 
function generatePDReport(teacherId,hrefId)
{
	hideProfilePopover();
	$('#loadingDiv').show();
	CandidateGridAjaxNew.generatePDReport(teacherId,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data)
		{	
			$('#loadingDiv').hide();
			if(deviceType)
			{
				if (data.indexOf(".doc")!=-1)
				{
					$("#docfileNotOpen").css({"z-index":"3000"});
			    	$('#docfileNotOpen').show();
				}
				else if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					$("#exelfileNotOpen").css({"z-index":"3000"});
			    	$('#exelfileNotOpen').show();
				}
				else
				{
					$('#modalDownloadPDR').modal('hide');
					$("#cgTeacherDivMaster").modal('show');
					document.getElementById(hrefId).href=data;				
				}
			}
			else if(deviceTypeAndroid)
			{
				if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					  try{
					    $('#modalDownloadPDR').modal('hide');
						}catch(err)
						  {}
					    document.getElementById('ifrmPDR').src = ""+data+"";
				}
				else
				{
					$('#modalDownloadPDR').modal('hide');
					$("#cgTeacherDivMaster").modal('show');
					document.getElementById(hrefId).href=data;					
				}				
			}
			else if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
			{
				  try{
				    	$('#modalDownloadPDR').modal('hide');
					}catch(err)
					  {}
				    document.getElementById('ifrmPDR').src = ""+data+"";
			}
			else
			{
				$('#modalDownloadPDR').modal('hide');
				document.getElementById('ifrmPDR').src = ""+data+"";
				try{
					$('#modalDownloadPDR').modal('show');
				}catch(err)
				  {}
				
			}		
	  }
	});	
	
}

function downloadGeneralKnowledge(generalKnowledgeExamId,teacherId, linkId)
{		
	//CandidateReportAjax
	CGServiceAjax.downloadGeneralKnowledge(generalKnowledgeExamId,teacherId,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	if(data=="")
			{
				data="javascript:void(0)";
			}
			else if(data.indexOf(".doc")!=-1)
			{
				document.getElementById("ifrmRef").src=data;
			}else
			{
				document.getElementById(linkId).href = data;
			}
			return false;
		}});
}
function downloadSubjectAreaExam(subjectAreaExamId,teacherId, linkId)
{		
	//CandidateReportAjax
	CGServiceAjax.downloadSubjectAreaExam(subjectAreaExamId,teacherId,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
			if(data=="")
			{
				data="javascript:void(0)";
			}
			else if(data.indexOf(".doc")!=-1)
			{
				document.getElementById("ifrmRef").src=data;
			}else
			{
				document.getElementById(linkId).href = data;
			}
			return false;
		}});
}



//called from teacher dashboard
function checkBaseCompeleted(teacherId)
{
	CandidateGridAjaxNew.checkBaseCompeleted(teacherId,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){

		//alert(data);
		if(data==1)
		{
			$('#message2show').html(""+resourceJSON.ThankForYourInterest+"clientservices@teachermatch.net  "+resourceJSON.MsgPurchaseYourReport+"");
			//$('#myModal2').modal('show');
			try{
				$('#myModal2').modal('show');
			}catch(err)
			  {}
			notifyTMAdmins(teacherId);

		}else if(data==3)
		{
			$('#message2show').html(""+resourceJSON.BaseInventoryIsTimeOut+" <a href=\"mailto:clientservices@teachermatch.net\">clientservices@teachermatch.net</a> "+resourceJSON.AnyFurtherQues+".");
			//$('#myModal2').modal('show');
			try{
				$('#myModal2').modal('show');
			}catch(err)
			  {}
		}
		else
		{
			$('#message2show').html(""+resourceJSON.CompleteYourBaseInventory+".");
			//$('#myModal2').modal('show');
			try{
				$('#myModal2').modal('show');
			}catch(err)
			  {}
		}
	}
	});	

}
//called form admin
function checkBaseCompeletedForPDR(teacherId,hrefId)
{
	CandidateGridAjaxNew.checkBaseCompeleted(teacherId,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){
		if(data==1)
		{
			generatePDReport(teacherId,hrefId);
			return false;

		}else if(data==3)
		{
			$('#message2show').html(""+resourceJSON.CandidateTimeOutBaseInvevtory+".");
			//$('#myModal2').modal('show');
			try{
				$('#myModal2').modal('show');
			}catch(err)
			  {}
			return false;
		}
		else
		{
			$('#message2show').html(""+resourceJSON.CandidateHasNotComplete+".");
			//$('#myModal2').modal('show');
			try{
				$('#myModal2').modal('show');
			}catch(err)
			  {}
			return false;
		}
	}
	});	

}
function notifyTMAdmins(teacherId)
{
	CandidateGridAjaxNew.notifyTMAdmins(teacherId,{ 
		async: true,
		errorHandler:handleError,
		callback: function(data){
	}
	});	
}

function generatePilarReport(teacherId,jobId,hrefId)
{
	jobOrder={jobId:jobId};
	CandidateGridAjaxNew.generatePilarReport(teacherId,jobOrder,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){

		$('#loadingDiv').hide();
		if(data==0)
		{
			$('#message2show').html(resourceJSON.NoJobSpecificInventory);
			//$('#myModal2').modal('show');
			try{
				$('#myModal2').modal('show');
			}catch(err)
			  {}
			return false;
		}else if(data==1)
		{
			$('#message2show').html(resourceJSON.CandidateHasNotCompleteThisJobOrder);
			//$('#myModal2').modal('show');
			try{
				$('#myModal2').modal('show');
			}catch(err)
			  {}
			return false;
		}else if(data==2)
		{
			$('#message2show').html(resourceJSON.CandidateHasTimeOut);
			//$('#myModal2').modal('show');
			try{
				$('#myModal2').modal('show');
			}catch(err)
			  {}
			return false;
		}else if(data==3)
			generateJSAReport(teacherId,jobOrder,hrefId);
	}
	});	
}

function generateJSAReport(teacherId,jobOrder,hrefId)
{
	$('#loadingDiv').show();	
	CandidateGridAjaxNew.generateJSAReport(teacherId,jobOrder,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){
		$('#loadingDiv').hide();	
		if(deviceType || deviceTypeAndroid)
		{
			document.getElementById(hrefId).href=data;
		}		
		else
		{
			document.getElementById(hrefId).href=data;
		}
		
	}
	});
	
}
function showProfile(teacherId)
{
	$('#myModalProfile').show();
}
function hideProfile()
{
	$('#myModalProfile').hide();
}
function statusChange(imgFlag,status){
	statusTxt=status;
	statusValue=imgFlag;
	if(status=='H'){
		if(imgFlag==0){
			document.getElementById("pinkHired").style.display="inline";
			document.getElementById("greenHired").style.display="none";
			document.getElementById("greenRemoved").style.display="none";
			document.getElementById("pinkRemoved").style.display="inline";
			document.getElementById("pinkRemovedOnly").style.display="none";
		}else{
			document.getElementById("pinkHired").style.display="none";
			document.getElementById("greenHired").style.display="inline";
			document.getElementById("greenRemoved").style.display="none";
			document.getElementById("pinkRemoved").style.display="none";
			document.getElementById("pinkRemovedOnly").style.display="inline";
		}
	}else if(status=='R'){
		if(imgFlag==0){
			document.getElementById("pinkRemoved").style.display="inline";
			document.getElementById("greenRemoved").style.display="none";
			document.getElementById("pinkHired").style.display="inline";
			document.getElementById("greenHired").style.display="none";
			document.getElementById("pinkHiredOnly").style.display="none";
		}else{
			document.getElementById("pinkRemoved").style.display="none";
			document.getElementById("greenRemoved").style.display="inline";
			document.getElementById("pinkHired").style.display="none";
			document.getElementById("greenHired").style.display="none";
			document.getElementById("pinkHiredOnly").style.display="inline";
		}
	}
}
function deletesavefolder(rootNode)
{
	//alert(" deletesavefolder rootNode "+rootNode);
	$('#currentObject').val(rootNode.data.key);
	//$('#deleteFolder').modal('show');
	try{
		$('#deleteFolder').modal('show');
	}catch(err)
	  {}

	
}
function deleteconfirm()
{
	//alert("delexcv          m");
	//$('#deleteFolder').modal('hide');
	try{
		$('#deleteFolder').modal('hide');
	}catch(err)
	  {}
	var rootNodeKey =$('#currentObject').val();
	//parent.deleteFolder();
	document.getElementById('iframeSaveCandidate').contentWindow.deleteFolder(rootNodeKey);
	//document.getElementById('target_Frame').contentWindow.callingtargetFunction();
	//window.frames["iframeSaveCandidate"].deleteFolder();
	//window.frames["original_preview_iframe"].exportAndView(img_id);//
}

function saveToFolder(jftteacherId,flagpopover)
{	
	getiFrameForTree();
	defaultSaveDiv();
	//alert(" teacherId "+teacherId +" === flagpopover "+flagpopover);
	if(jftteacherId!="")
	{
		$("#teachetIdFromPoPUp").val(jftteacherId);
		$("#txtflagpopover").val(flagpopover);
	}	
/*------------- Here I am creating only Home and Shared Folder For User*/	
	//$('#saveToFolderDiv').modal('show');
	try{
		$('#saveToFolderDiv').modal('show');
	}catch(err)
	  {}
	$('#errordiv').hide();
	var iframe = document.getElementById('iframeSaveCandidate');
	var innerDoc = iframe.contentDocument || iframe.contentWindow.document;
	
	innerDoc.getElementById('errortreediv').style.display="none";
	innerDoc.getElementById('errordeletetreediv').style.display="none";
	

	
	var folderId = innerDoc.getElementById('frame_folderId').value;
	if(innerDoc.getElementById('frame_folderId').value!="" || innerDoc.getElementById('frame_folderId').value.length>0)
	{
		var checkboxshowHideFlag =0; // For hide checkboxes on Save to Folder Div
		displaySavedCGgridByFolderId(0,folderId,checkboxshowHideFlag);
		// return false;
	}
	
	CandidateGridAjaxNew.createHomeFolder(
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			//alert(" data "+data); data will be 2: when first time Home and shared folder will be created
			if(data==2)
			{
				document.getElementById("iframeSaveCandidate").src="tree.do";
			}
		}
	});
}

function saveCandidate() //=== It will only for display Candidate before Save 
{
	//alert("saveCandidate ");
	var savecandidatearray="";
	/*var inputs = document.getElementsByTagName("input"); 
	 for (var i = 0; i < inputs.length; i++) {
	        if (inputs[i].type === 'checkbox') {
	            if(inputs[i].checked){
	            	savecandidatearray+=	inputs[i].value+",";
	            }
	        }
	 } */
	 
	 var inputs = document.getElementsByName("cgCBX"); 
	 for (var i = 0; i < inputs.length; i++) {
	        if (inputs[i].type === 'checkbox') {
	        	if(inputs[i].checked){
	        		savecandidatearray+=	inputs[i].value+",";
	            }
	        }
	 } 
	 
	 $("#savecandidatearray").val(savecandidatearray);
	 	//alert(" savecandidatearray "+savecandidatearray);
	 
		CandidateGridAjaxNew.saveCandidate(savecandidatearray,
		{ 
			async: false,
			errorHandler:handleError,
			callback:function(data)
			{	
				//alert(" data "+data); data will be 2: when first time Home and shared folder will be created
				if(data!=null)
				{
					document.getElementById("savedCandidateGrid").innerHTML=data;
					displaySaveCandidatePopUpDiv();
				}
			}
		});
		
}

function saveCandidateToFolderByUser()
{
	document.getElementById('iframeSaveCandidate').contentWindow.checkFolderId();
	var iframe = document.getElementById('iframeSaveCandidate');
	var innerDoc = iframe.contentDocument || iframe.contentWindow.document;
	var folderId = innerDoc.getElementById('frame_folderId').value;
	//alert(" frame_folderId sdfsdfsdf "+folderId+" value "+folderId.value+" :::::: "+folderId.length);
	//alert(" folderId "+folderId);
	var frameHeight=iframe.contentWindow.document.body.scrollHeight;
	$('iframe').css('height',frameHeight);
	var fId="";
	if(innerDoc.getElementById('frame_folderId').value=="" || innerDoc.getElementById('frame_folderId').value.length==0)
	{
		//alert("Yes")
		innerDoc.getElementById('errortreediv').style.display="block";
		innerDoc.getElementById('errortreediv').innerHTML="&#149; Please select any folder";
		 return false;
	}
	else
	{
		//alert(" Else No ");
		//fId=folderId;
	}
	//alert(" sgfsdgb "+innerDoc.getElementById('frame_folderId').value);
	var savecandidatearray="";
	/*var inputs = document.getElementsByTagName("input"); 
	 for (var i = 0; i < inputs.length; i++) {
	        if (inputs[i].type === 'checkbox') {
	            if(inputs[i].checked){
	            	savecandidatearray+=	inputs[i].value+",";
	            }
	        }
	 }*/
	
	 var inputs = document.getElementsByName("cgCBX"); 
	 for (var i = 0; i < inputs.length; i++) {
	        if (inputs[i].type === 'checkbox') {
	        	if(inputs[i].checked){
	        		savecandidatearray+=	inputs[i].value+",";
	            }
	        }
	 } 
	 	
	 	if($("#txtflagpopover").val()==1 && $("#teachetIdFromPoPUp").val()!="")
		{
	 		savecandidatearray	=	$("#teachetIdFromPoPUp").val();
		}	
	
	 	//alert(" savecandidatearray "+savecandidatearray);
	 	//return false;
	 	
	 $("#savecandidatearray").val(savecandidatearray);
	CandidateGridAjaxNew.saveCandidateToFolderByUser(savecandidatearray,folderId,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			//alert(" data "+data);// data will be 2: when first time Home and shared folder will be created
			if(data==1)
			{
				//$("#saveToFolderDiv").modal("hide");
				//$('#saveAndShareConfirmDiv').html("You have successfully saved the Candidates.");
				//$('#shareConfirm').modal("show");
				
				try{
					$("#saveToFolderDiv").modal("hide");
					$('#saveAndShareConfirmDiv').html(resourceJSON.SavedCandidate);
					$('#shareConfirm').modal("show");
				}catch(err)
				  {}
				//document.getElementById("savedCandidateGrid").innerHTML=data;
				//displaySaveCandidatePopUpDiv();
				//uncheckedAllCBX();
			}
			if(data==3)
			{
				//alert("Duplicate Candaidate found ");
				$("#txtoverrideFolderId").val(folderId);
				try{
					$("#duplicatCandidate").modal("show");
				}catch(err)
				  {}
			}
		}
	});
	 
	 
}
function saveWithDuplicateRecord()
{
	var folderId= $("#txtoverrideFolderId").val();
	var savecandidatearray= $("#savecandidatearray").val();
	//alert(" savecandidatearray "+savecandidatearray+" folderId "+folderId);
	CandidateGridAjaxNew.saveWithDuplicateRecord(savecandidatearray,folderId,
			{ 
				async: false,
				errorHandler:handleError,
				callback:function(data)
				{	
					//alert(" data "+data);// data will be 2: when first time Home and shared folder will be created
					if(data==1)
					{
						//$("#saveToFolderDiv").modal("hide");
						//$("#duplicatCandidate").modal("hide");
						try{
							$("#saveToFolderDiv").modal("hide");
							$("#duplicatCandidate").modal("hide");
						}catch(err)
						  {}
						//uncheckedAllCBX();
						//document.getElementById("savedCandidateGrid").innerHTML=data;
						//displaySaveCandidatePopUpDiv();
					}
				}
			});
	
}

function displaySavedCGgridByFolderId(hoverDisplay,folderId,checkboxshowHideFlag)
{
	var pageFlag= document.getElementById("pageFlag").value;
	currentPageFlag="stt";
	//document.getElementById("checkboxshowHideFlag").value=checkboxshowHideFlag;
	document.getElementById("folderId").value=folderId;
	CandidateGridAjaxNew.displaySavedCGgridByFolderId(pageFlag,folderId,pageFlag,pageSTT,noOfRowsSTT,sortOrderStrSTT,sortOrderTypeSTT,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			if(data!=null)
			{
				if(pageFlag==1){
					document.getElementById("savedCGgridMyFolder").innerHTML=data;
				}else{
					document.getElementById("savedCandidateGrid").innerHTML=data;
				}
				//$("#home_folderId").val(folderId);
				displaySaveCandidatePopUpDiv();
			}
		}
	});
}


function uncheckedAllCBX()
{
	//alert("unchecked");
/*	var inputs = document.getElementsByTagName("input"); 
	 for (var i = 0; i < inputs.length; i++) {
	        if (inputs[i].type === 'checkbox') {
	        	inputs[i].checked=false;
	        }
	 } */
	 
	 var inputs = document.getElementsByName("cgCBX"); 
	 for (var i = 0; i < inputs.length; i++) {
	        if (inputs[i].type === 'checkbox') {
	        	inputs[i].checked=false;
	        }
	 } 
	 document.getElementById("actionDiv").style.display="none";
}

/* ===== Gagan : displaySaveCandidatePopUpDiv() Method =========== */
function displaySaveCandidatePopUpDiv()
{
	
	var noOrRow = document.getElementById("saveCandidateTable").rows.length;
	for(var j=5000;j<=5000+noOrRow;j++)
	{
		
		/*$('.profile'+j).popover({ 
		 	trigger: "manual",
		    html : true,
		    placement: 'right',
		    content: $('#profileDetails'+j).html(),
		  }).on("mouseenter", function () {
		        var _this = this;
		        $(this).popover("show");
		        $(this).siblings(".popover").on("mouseleave", function () {
		            $(_this).popover('hide');
		        });
		    }).on("mouseleave", function () {
		        var _this = this;
		        setTimeout(function () {
		            if (!$(".popover:hover").length) {
		                $(_this).popover("hide")
		            }
		        }, 100);
		    });*/
	}
}



/*==================Gagan: Share Div Function Start Here ======================*/
function displayShareFolder(jftteacherId,flagpopover)
{
	if(jftteacherId!="")
	{
		$("#JFTteachetIdFromSharePoPUp").val(jftteacherId);
		$("#txtflagSharepopover").val(flagpopover);
	}	
	
	
	document.getElementById("userFPS").value=0;
	document.getElementById("userFPD").value=0;
	document.getElementById("userCPS").value=0;
	document.getElementById("userCPD").value=1;
	currentPageFlag="us";
	$('#errorinvalidschooldiv').hide();
	if($('#entityType').val()==2)
	{
		$('#schoolId15').val("0");
		$('#schoolName15').val("");
	}
	else
	{
		if($('#entityType').val()==3)
		{
			$('#schoolId15').val($('#loggedInschoolId').val());
			$('#schoolName15').val($('#loggedInschoolName').val());
		}
	}

	var savecandiadetidarray="";	
	 
	 var inputs = document.getElementsByName("cgCBX"); 
	 for (var i = 0; i < inputs.length; i++) {
	        if (inputs[i].type === 'checkbox') {
	        	if(inputs[i].checked){
	        		savecandiadetidarray+=	inputs[i].value+",";
	            }
	        }
	 } 
	 //alert(" savecandiadetidarray "+savecandiadetidarray);
	 $("#savecandiadetidarray").val(savecandiadetidarray);
	/*if(savecandiadetidarray!="")
	{*/
		try{
			$('#shareDiv').modal('show');
		}catch(e){}
		
		var entityType	=	$('#entityType').val();
		var districtId	=	$('#districtId').val();
		var schoolId	=	trim($('#schoolId15').val());
		if(schoolId=="")
		{
			$('#errorinvalidschooldiv').show();
			$('#errorinvalidschooldiv').html("&#149;  "+resourceJSON.SelectValidSchool+"");
			return false;
		}
		if($('#entityType').val()!="")
		{
			/* ========= Display School List ============ */
			CandidateGridAjaxNew.getDistictSchoolUserList(entityType,districtId,schoolId,pageUS,noOfRowsUS,sortOrderStrUS,sortOrderTypeUS,
			{ 
				async: false,
				errorHandler:handleError,
				callback:function(data)
				{	
				if(data!="")
				{
					$('#divShareCandidateToUserGrid').html(data);
				}
			}
			});
		}
	
	/*}
	else
	{
		$("#errortreediv").show();
		$('#errortreediv').html("&#149; Please select Candidates");
	}
	*/
}


function searchUserthroughPopUp(searchFlag)
{
	if(searchFlag==1)
	{
		defaultShareDiv();
	}
	document.getElementById("userFPS").value=0;
	document.getElementById("userFPD").value=0;
	document.getElementById("userCPS").value=1;
	document.getElementById("userCPD").value=0;
	currentPageFlag="us";
	$('#errorinvalidschooldiv').hide();
	var savecandiadetidarray="";
	 
	 var inputs = document.getElementsByName("cgCBX"); 
	 for (var i = 0; i < inputs.length; i++) {
	        if (inputs[i].type === 'checkbox') {
	        	if(inputs[i].checked){
	        		savecandiadetidarray+=	inputs[i].value+",";
	            }
	        }
	 } 
	
	if(savecandiadetidarray!="" || $("#txtflagSharepopover").val()==1)
	{
		//$('#shareDiv').modal('show');
		try{
			$('#shareDiv').modal('show');
		}catch(err)
		  {}
		var entityType	=	$('#entityType').val();
		var districtId	=	$('#districtId').val();
		var schoolId	=	trim($('#schoolId15').val());
		var schoolName	=	trim($('#schoolName15').val());
		if(schoolId=="")
		{
			if( schoolName!="")
			{
				$('#errorinvalidschooldiv').show();
				$('#errorinvalidschooldiv').html("&#149;  "+resourceJSON.SelectValidSchool+"");
				return false;
			}
			else
			{
				schoolId=0;
			}
			
		}
		if($('#entityType').val()!="")
		{
			CandidateGridAjaxNew.getDistictSchoolUserList(entityType,districtId,schoolId,pageUS,noOfRowsUS,sortOrderStrUS,sortOrderTypeUS,
			{ 
				async: false,
				errorHandler:handleError,
				callback:function(data)
				{
					if(data!="")
					{
						$('#divShareCandidateToUserGrid').html(data);
					}
				}
			});
		}
	}
	
}




function shareCandidatethroughPopUp()
{
	defaultSaveDiv();
	$('#errorinvalidschooldiv').hide();
	$('#errorinvalidschooldiv').empty();
	var userIdarray =	"";
	var myUsercbx	=	document.getElementsByName("myUsercbx");
	
	var folderId = document.getElementById('txtflagSharepopover').value;
	
	if(folderId=="")
		folderId=0;

	 for (e=0;e<myUsercbx.length;e++) 
	 {
	  if (myUsercbx[e].checked==true) 
	  {
		  userIdarray+=myUsercbx[e].value+",";
	   }
	 }
	 	if(userIdarray=="")
	 	{
	 		$('#errorinvalidschooldiv').show();
	 		$('#errorinvalidschooldiv').html("&#149; "+resourceJSON.PlzSelectUsr+"");
	 	}
	 	
	 	var jobforteacheridarray	=	"";
	 /*	var inputs = document.getElementsByTagName("input"); 
		 for (var i = 0; i < inputs.length; i++) {
		        if (inputs[i].type === 'checkbox') {
		            if(inputs[i].checked){
		            	jobforteacheridarray+=	inputs[i].value+",";
		            }
		        }
		 } 
	 	*/
		 var inputs = document.getElementsByName("cgCBX"); 
		 for (var i = 0; i < inputs.length; i++) {
		        if (inputs[i].type === 'checkbox') {
		        	if(inputs[i].checked){
		        		jobforteacheridarray+=	inputs[i].value+",";
		            }
		        }
		 } 
		 if($("#txtflagSharepopover").val()==1 && $("#JFTteachetIdFromSharePoPUp").val()!="")
			{
			 jobforteacheridarray	=	$("#JFTteachetIdFromSharePoPUp").val();
			}	
		 //alert(" jobforteacheridarray  "+jobforteacheridarray);
		 //return false;
	 	//jobfor teacheridarray = $("#savecandiadetidarray").val();
	 	//alert(" userIdarray "+userIdarray+" jobforteacheridarray "+jobforteacheridarray);
	
	 
		CandidateGridAjaxNew.shareCandidatesToUserFromCG(userIdarray,jobforteacheridarray,folderId,
		{ 
			async: false,
			errorHandler:handleError,
			callback:function(data)
			{	
				//alert(" ==== data === "+data);
			if(data==1)
			{
				//$('#shareDiv').modal("hide");
				//$('#saveAndShareConfirmDiv').html("You have successfully shared the Candidates to the selected Users.");
				//$('#shareConfirm').modal("show");
				
				try{
					$('#shareDiv').modal("hide");
					$('#saveAndShareConfirmDiv').html(resourceJSON.SucceccfullySharedCandidates);
					$('#shareConfirm').modal("show");
				}catch(err)
				  {}
				uncheckAllCbx();
				//uncheckedAllCBX();
			}
		}
		});
	 
	 
}
function uncheckAllCbx()
{
	
	var inputs = document.getElementsByName("mycbx"); 
	 for (var i = 0; i < inputs.length; i++) {
	        if (inputs[i].type === 'checkbox') {
	        	if(inputs[i].checked){
	            	//savecandiadetidarray+=	inputs[i].value+",";
	        		inputs[i].checked=false;
	            }
	        }
	 } 
	
}
function refreshStatus(){
	//alert("refreshStatus");
	document.getElementById("expandremovechk").value=0;
	document.getElementById("expandincompchk").value=0;
	document.getElementById("expandinterchk").value=0;
	document.getElementById("expandvltchk").value=0;
	document.getElementById("expandwithdrchk").value=0;
	document.getElementById("expandhiredchk").value=0;
	document.getElementById("expandhiddenchk").value==0;
}
function hideStatus()
{
	//$('#myModalStatus').hide();
	$('#myModalStatus').modal('hide');
	//getCandidateGrid();
	//refreshStatus();
	try {
		$('#spanOverride').modal('hide');
	} catch (e) {}
}
var teacherId;
var noOfRowsForJob=10;
function defaultSet(){
	pageForTC = 1;
	noOfRowsForTC =100;
	noOfRowsForJob=10;
	sortOrderStrForTC="";
	sortOrderTypeForTC="";
	currentPageFlag="";
	//alert(noOfRowsForJob);
}

function getJobList(tId){
	
	hideProfilePopover();
	
	defaultSet();
	currentPageFlag='job';
	teacherId=tId;
	$('#loadingDiv').show();
	getJobOrderList();

}

function getJobOrderList(){
	$('#loadingDiv').show();
	currentPageFlag='job';
	var visitLocation=document.getElementById("teacherIdForprofileGridVisitLocation").value;
	if(visitLocation=='My Folders'){
		//TeacherInfotAjax
		CGServiceAjax.getJobListByTeacherByProfile(teacherId,noOfRowsForJob,pageJob,sortOrderStrJob,sortOrderTypeJob,false,visitLocation,0,0, { 
			async: true,
			callback: function(data)
			{
			try{
			$('#loadingDiv').hide();	
			//$('#myModalJobList').show(); 
			$('#myModalJobList').modal('show');
			document.getElementById("divJob").innerHTML=data;
			//applyScrollOnJob();
			jobEnable();
			}catch(err)
			{}
			},
			errorHandler:handleError 
		});
	}
	else
	{
		//TeacherInfotAjax
		CGServiceAjax.getJobListByTeacher(teacherId,noOfRowsForJob,pageJob,sortOrderStrJob,sortOrderTypeJob,false, { 
			async: true,
			callback: function(data)
			{
				try{
				$('#loadingDiv').hide();	
				//$('#myModalJobList').show();
				$('#myModalJobList').modal('show');
				document.getElementById("divJob").innerHTML=data;
				applyScrollOnJob();
				jobEnable();
				}catch(err){}
			},
			errorHandler:handleError 
		});
	}
}
function descriptionShow(jobId)
{
	$("#myModalJobList").css({"z-index":"1"});
	//TeacherInfotAjax
	CGServiceAjax.getJobDescription(jobId, { 
		async: true,
		callback: function(data)
		{
			//$('#myModalJobList').modal('hide');
			//$('#myModalDesc').modal('show');
			try{
				$('#myModalJobList').modal('hide');
				$('#myModalDesc').modal('show');
			}catch(err)
			  {}
			document.getElementById("description").innerHTML=data;
		},
		errorHandler:handleError 
	});
	
}

function jobEnable()
{
	var noOrRow = document.getElementById("jobTable").rows.length;
	for(var j=1;j<=noOrRow;j++)
	{
		
		$('#jobCom'+j).tooltip();
		$('#tpJSA'+j).tooltip();
		$('#hireTp'+j).tooltip();
		$('#jobCL'+j).tooltip();
		$('#jobTrans'+j).tooltip();
		$('#jobCert'+j).tooltip();
		$('#jobJobId'+j).tooltip();
		$('#jobJobTitle'+j).tooltip();
	}
}
function setZIndexJobDiv()
{
	defaultSet();
	currentPageFlag="job";
	//$('#myModalDesc').modal('hide');
	//$('#myModalCommunications').modal('hide');
	//$('#myModalCoverLetter').modal('hide');
	try{
		$('#myModalDesc').modal('hide');
		$('#myModalCommunications').modal('hide');
		$('#myModalCoverLetter').modal('hide');
	}catch(err)
	  {}
	
	$("#myModalJobList").css({"z-index":"3000"});
	if(document.getElementById("commDivFlag").value==0){
		//$('#myModalJobList').modal('show');
		try{
			$('#myModalJobList').modal('show');
		}catch(err)
		  {}
	}
}
function showCoverLetter(teacherId, jobId)
{	
	//CandidateReportAjax
	CGServiceAjax.getCoverLetter(teacherId,jobId,
	{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{	
			if(data=="")
			{
				$('#myModalMsgShow').html(resourceJSON.CandidateNotProvidedCoverLtr);
				//$('#myModalJobList').modal('hide');
				//$('#myModalMsg').modal('show');
				try{
					$('#myModalJobList').modal('hide');
					$('#myModalMsg').modal('show');
				}catch(err)
				  {}

			}
			else
			{
				document.getElementById("lblCL").innerHTML=""+data;
				//$('#myModalJobList').modal('hide');
				//$('#myModalCoverLetter').modal('show');
				try{
					$('#myModalJobList').modal('hide');
					$('#myModalCoverLetter').modal('show');
				}catch(err)
				  {}
			}		
		}
	});
}

function hideJob()
{
	$('#myModalJob').hide();
}

function showTag(status,teacherAssessmentStatusId,jobForTeacherId,teacherId)
{
	//alert("showTag ");
	//$('#myModalTag').show();
	//$('#tag'+teacherId).show();
	//document.getElementById("tag"+teacherId).click();
	//$('#errordivAct').empty();	
	CandidateGridAjaxNew.showActDiv(status,teacherAssessmentStatusId,jobForTeacherId,{ 
	async: true,
	errorHandler:handleError,
	callback:function(data)
	{	
		//document.getElementById("divAct").innerHTML=data.toString();
		//$('#myModalAct').modal('show');		
		//alert(" data "+data);
		$('#tagbody'+teacherId).html(data.toString())
		$('#tag'+teacherId).popover({ 
		    html : true,
		    placement: 'right',
		    content: $('#tagDiv'+teacherId).html(),
		  });
		
		
	}});	
	
	
	
	/*$('#tag'+teacherId).popover({ 
	    html : true,
	    placement: 'right',
	    content: $('#tagDiv'+teacherId).html(),
	  });*/
	
	/*$('.profile'+divNo).popover({ 
	 	trigger: "manual",
	    html : true,
	    placement: 'right',
	    content: function() {
	      return $('#profileDetails'+divNo).html();
	    }
	  }).on("mouseenter", function () {
	        var _this = this;
	        $(this).popover("show");
	        $(this).siblings(".popover").on("mouseleave", function () {
	            $(_this).popover('hide');
	        });
	    }).on("mouseleave", function () {
	        var _this = this;
	        setTimeout(function () {
	            if (!$(".popover:hover").length) {
	                $(_this).popover("hide")
	            }
	        }, 100);
	    });
	    */
}
function hideTag()
{
	$('#myModalTag').hide();
}
function showNorm(teacherId,jobId,normScore)
{
	/*//alert(teacherId+" "+jobId);
	var teacherDetail = {teacherId:teacherId};
	var jobOrder = {jobId:jobId};
	
	CandidateGridAjaxNew.generateNormalCurve(teacherDetail,jobOrder,normScore,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{	
			 var chart = new FusionCharts("fusioncharts/MSSplineArea.swf", "ChartId", "500", "330", "0", "1");
			   chart.setXMLUrl("fusioncharts/xmls/"+data);		
			    chart.setTransparent(true);
			   chart.render("divNorm");
			   
			//$('#myModalNorm').show();
			//$('#myModalNorm').fadeIn(500);
			$('#myModalNorm').modal('toggle');
		
		}});*/
}
function hideNorm()
{
	//$('#myModalNorm').fadeOut(500);
	//$('#myModalNorm').modal('toggle');
	try{
		$('#myModalNorm').modal('toggle');
	}catch(err)
	  {}
}

function showNormDistribution(jobId)
{
	/*var jobOrder = {jobId:jobId};
	
	CandidateGridAjaxNew.generateNormDistribution(jobOrder,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{	
			 var chart = new FusionCharts("fusioncharts/MSSplineArea.swf", "ChartId", "500", "330", "0", "1");
			   chart.setXMLUrl("fusioncharts/xmls/"+data);		
			    chart.setTransparent(true);
			   chart.render("divNorm");
			   
			//$('#myModalNorm').show();
			//$('#myModalNorm').fadeIn(500);
			$('#myModalNorm').modal('toggle');
		
		}});*/
	
}
function showProfileDetails(divNo)
{
	$('.profile'+divNo).popover({ 
	 	trigger: "manual",
	    html : true,
	    placement: 'right',
	    content: function() {
	      return $('#profileDetails'+divNo).html();
	    }
	  }).on("mouseenter", function () {
	        var _this = this;
	        $(this).popover("show");
	        $(this).siblings(".popover").on("mouseleave", function () {
	            $(_this).popover('hide');
	        });
	    }).on("mouseleave", function () {
	        var _this = this;
	        setTimeout(function () {
	            if (!$(".popover:hover").length) {
	                $(_this).popover("hide")
	            }
	        }, 100);
	    });
}
function hideProfileDetails(teacherId)
{
	$('#myModalProfileDetails'+teacherId).hide();
}

function actionMenu(name,id){
	var inputs = document.getElementsByTagName("input"); 
	var chkCount=0;
	var chkCheckedCount=0;
	 for (var i = 0; i < inputs.length; i++) {
	        if (inputs[i].type === 'checkbox') {
	        	chkCount++;
	            if(inputs[i].checked){
	            	chkCheckedCount++;
	            }
	        }
	 } 
	 if(chkCheckedCount>0){
		 document.getElementById("actionDiv").style.display="inline";
	 }else{
		 document.getElementById("actionDiv").style.display="none";
	 }
}
function displayAdvanceSearch()
{
	$('#searchLinkDiv').fadeOut();
	$('#advanceSearchDiv').slideDown('slow');
	var iFrmId="ifrmNorm";
	var name="normScoreFrm";
	var tickInterval="10";
	var max="100";
	var swidth="230";
	var svalue="0";
	var style="border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:280px;margin-top:-11px;";
	CGServiceAjax.getSlider(iFrmId,name,tickInterval,max,swidth,svalue,style,{ 
	async: false,
	callback: function(data)
	{
		$('#sliderNormScore').html(data.toString())
	},
	errorHandler:handleError
	});
	
	var iFrmId1="ifrmCGPA";
	var name1="CGPAFrm";
	var tickInterval1="1";
	var max1="5";
	var swidth1="230";
	var svalue1="0";
	var style1="border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:280px;margin-top:-11px;";
	CGServiceAjax.getSlider(iFrmId1,name1,tickInterval1,max1,swidth1,svalue1,style1,{ 
	async: false,
	callback: function(data1)
	{
		$('#sliderCGPA').html(data1.toString())
	},
	errorHandler:handleError
	});
	
	var iFrmId2="ifrmYOTE";
	var name2="YOTEFrm";
	var tickInterval2="5";
	var max2="50";
	var swidth2="230";
	var svalue2="0";
	var style2="border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:280px;margin-top:-11px;";
	CGServiceAjax.getSlider(iFrmId2,name2,tickInterval2,max2,swidth2,svalue2,style2,{ 
	async: false,
	callback: function(data)
	{
		$('#sliderYOTETP').html(data.toString())
	},
	errorHandler:handleError
	});
}

var count=0;
var index=-1;
var length=0;
var divid='';
var txtid='';
var hiddenDataArray = new Array();
var showDataArray = new Array();
var degreeTypeArray = new Array();
var hiddenId="";
function getDistrictAuto(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("districtName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getDistrictArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getDistrictArray(districtName){
	var searchArray = new Array();
	//BatchJobOrdersAjax
	CGServiceAjax.getFieldOfDistrictList(districtName,{ 
			async: false,
			callback: function(data){
			hiddenDataArray = new Array();
			showDataArray = new Array();
			for(i=0;i<data.length;i++){
				searchArray[i]=data[i].districtName;
				showDataArray[i]=data[i].districtName;
				hiddenDataArray[i]=data[i].districtId;
			}
		},
		errorHandler:handleError
		});	
	return searchArray;
}


var selectFirst="";

var fatchData= function(txtSearch,searchArray,txtId,txtdivid){
	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
				
			}
			
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}
function hideDistrictMasterDiv(dis,hiddenId,divId)
{
	document.getElementById("districtId").value="";
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			document.getElementById("districtId").value=hiddenDataArray[index];
			document.getElementById('schoolName15').readOnly=false;
			document.getElementById('schoolName15').value="";
			document.getElementById('schoolId15').value="0";
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}

count=0;
index=-1;
length=0;
divid='';
txtid='';
hiddenDataArray = new Array();
showDataArray = new Array();
degreeTypeArray = new Array();
hiddenId="";

function getUniversityAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38)
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13)
	{
		
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		document.getElementById("fieldName").focus();
		
	} 
	else if(event.keyCode==9)
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getUniversityArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getUniversityArray(universityName){
	
	var searchArray = new Array();
	//DWRAutoComplete 
	CGServiceAjax.getUniversityMasterList(universityName,{ 
		async: false,		
		callback: function(data){
		hiddenDataArray = new Array();
		showDataArray = new Array();
		for(i=0;i<data.length;i++){
			searchArray[i]=data[i].universityName;
			hiddenDataArray[i]=data[i].universityId;
			showDataArray[i]=data[i].universityName;
		}
	}
	});	

	return searchArray;
}


function hideUniversityDiv(dis,hiddenId,divId)
{
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		document.getElementById(hiddenId).value=hiddenDataArray[index];
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
		if(dis.value!="")
		{
			var cnt=0;
			var focs=0;	
			$('#errordiv').empty();	
			setDefColortoErrorMsg();
			$('#errordiv').append("&#149; "+resourceJSON.PlzEtrVldClgAttnded+"<br>");
			if(focs==0)
				$('#universityName').focus();
			
			$('#universityName').css("background-color",txtBgColor);
			cnt++;focs++;	
		}
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}

count=0;
index=-1;
length=0;
divid='';
txtid='';
hiddenDataArray = new Array();
showDataArray = new Array();
hiddenId="";

function getRegionAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38)
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13)
	{
		
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		document.getElementById("regionName").focus();
		
	} 
	else if(event.keyCode==9)
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getRegionArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getRegionArray(regionName){
	
	var searchArray = new Array();
	//DWRAutoComplete
	CGServiceAjax.getRegionMasterList(regionName,{ 
		async: false,		
		callback: function(data){
		hiddenDataArray = new Array();
		showDataArray = new Array();
		for(i=0;i<data.length;i++){
			searchArray[i]=data[i].regionName;
			hiddenDataArray[i]=data[i].regionId;
			showDataArray[i]=data[i].regionName;
		}
	}
	});	

	return searchArray;
}


function hideRegionDiv(dis,hiddenId,divId)
{
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		document.getElementById(hiddenId).value=hiddenDataArray[index];
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
		if(dis.value!="")
		{
			var cnt=0;
			var focs=0;	
			$('#errordiv').empty();	
			$('#errordiv').append("&#149; "+resourceJSON.PlzEtrVldRgnName+"<br>");
			if(focs==0)
				$('#regionName').focus();
			
			$('#regionName').css("background-color",txtBgColor);
			cnt++;focs++;	
		}
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}


count=0;
index=-1;
length=0;
divid='';
txtid='';
hiddenDataArray = new Array();
showDataArray = new Array();
degreeTypeArray = new Array();
hiddenId="";

function getDegreeMasterAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	$('#degreeName').mousedown(function(e) {
	if( (event.keyCode == 1) ){
        alert("left button");
    }if( (event.keyCode== 3) ) {
        alert("right button");
    }else if( (event.keyCode== 2) ) {
        alert("middle button"); 
    }
	});
	
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		document.getElementById("universityName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getDegreeMasterArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getDegreeMasterArray(degreeName){
	var searchArray = new Array();
	//DWRAutoComplete
	CGServiceAjax.getDegreeMasterList(degreeName,{ 
		async: false,		
		callback: function(data){
		hiddenDataArray = new Array();
		showDataArray = new Array();
		for(i=0;i<data.length;i++){
			searchArray[i]=data[i].degreeName;
			hiddenDataArray[i]=data[i].degreeId;
			showDataArray[i]=data[i].degreeName;
			degreeTypeArray[i]=data[i].degreeType;
		}
	}
	});	

	return searchArray;
}

function hideDegreeMasterDiv(dis,hiddenId,divId)
{
	document.getElementById("degreeType").value="";
	if(parseInt(length)>0)
	{
		if(index==-1)
		{
			index=0;
		}
			document.getElementById(hiddenId).value=hiddenDataArray[index];			
			var degreeName = document.getElementById("degreeName").value;
			var degreeType = document.getElementById("degreeType").value=degreeTypeArray[index];
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
			document.getElementById("degreeType").value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
		}
	}else{
		if(document.getElementById(hiddenId))
		{
			document.getElementById(hiddenId).value="";
		}
		if(dis.value!=""){
			var cnt=0;
			var focs=0;	
			$('#errordiv').empty();	
			setDefColortoErrorMsg();
			$('#errordiv').append("&#149; "+resourceJSON.PlzEtrVldDegree+"<br>");
			if(focs==0)
				$('#degreeName').focus();
			
			$('#degreeName').css("background-color",txtBgColor);
			cnt++;focs++;	
		}
			
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}



count=0;
index=-1;
length=0;
divid='';
txtid='';
hiddenDataArray = new Array();
showDataArray = new Array();
degreeTypeArray = new Array();
hiddenId="";

function getSchoolAuto(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("schoolName15").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getSchoolArray(txtSearch.value);
		fatchData2(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getSchoolArray(schoolName){
	var searchArray = new Array();
	var districtId	=	document.getElementById("districtId").value;
	
	//BatchJobOrdersAjax
		CGServiceAjax.getFieldOfSchoolList(districtId,schoolName,"all",{
			async: false,
			callback: function(data){
			hiddenDataArray = new Array();
			showDataArray = new Array();
			for(i=0;i<data.length;i++){
				
				searchArray[i]=data[i].schoolName;
				showDataArray[i]=data[i].schoolName;
				hiddenDataArray[i]=data[i].schoolMaster.schoolId;
			}
		},
		errorHandler:handleError
		});	
	return searchArray;
}


selectFirst="";

var fatchData2= function(txtSearch,searchArray,txtId,txtdivid){

	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
				
			}
			
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}
function hideSchoolMasterDiv(dis,hiddenId,divId)
{
	document.getElementById("schoolId15").value="";
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			document.getElementById("schoolId15").value=hiddenDataArray[index];
			document.getElementById("demoSchoolId").value=hiddenDataArray[index];
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}




var downArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index<length-1){
			for(var i=0;i<10;i++){
				{
					if(document.getElementById('divResult'+txtdivid+i))
						var div_id=document.getElementById('divResult'+txtdivid+i);
					if(div_id)
					{
						if(div_id.className=='over')
							index=div_id.id.split('divResult'+txtdivid)[1];
						div_id.className='normal';
					}
				}
			}
			index++;
			if(document.getElementById('divResult'+txtdivid+index))
			{
				var div_id=document.getElementById('divResult'+txtdivid+index);
				div_id.className='over';
				document.getElementById(txtid).value = div_id.innerHTML;
				selectFirst=div_id.innerHTML;
			}
		}
	}
}

var upArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index>0){
			for(var i=0;i<length;i++){

				var div_id=document.getElementById('divResult'+txtdivid+i);
				if(div_id)
				{
					if(div_id.className=='over')
						index=div_id.id.split('divResult'+txtdivid)[1];
					div_id.className='normal';
				}
			}
			index--;
			if(document.getElementById('divResult'+txtdivid+index))
				document.getElementById('divResult'+txtdivid+index).className='over';
			if(txtid && document.getElementById('divResult'+txtdivid+index))
			{
				document.getElementById(txtid).value=
					document.getElementById('divResult'+txtdivid+index).innerHTML;
				selectFirst=document.getElementById('divResult'+txtdivid+index).innerHTML;
			}
		}
	}
}

var overText=function (div_value,txtdivid) {

	for(var i=0;i<length;i++)
	{
		if(document.getElementById('divResult'+i))
		{
			var div_id=document.getElementById('divResult'+i);

			if(div_id.className=='over')
				index=div_id.id.split(txtdivid)[1];
			div_id.className='normal';
		}
	}
	div_value.className = 'over';
	document.getElementById(txtid).value= div_value.innerHTML;
}
function trim(s)
{
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}
function checkForInt(evt) {

	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==8)
		return true;

	return( (charCode >= 48 && charCode <= 57)||(charCode==13) );
}

function mouseOverChk(txtdivid,txtboxId)
{	
	for(var i=0;i<length;i++)
	{
		$('#divResult'+txtdivid+i).mouseover({param1: i,param2:txtdivid, param3:txtboxId}, fireMouseOverEvent);
	}
}


function fireMouseOverEvent(event)
{
	for(var i=0;i<length;i++)
	{	
		document.getElementById('divResult'+event.data.param2+i).className='normal';
	}
    document.getElementById('divResult'+event.data.param2+event.data.param1).className='over';	       
   	document.getElementById(event.data.param3).value= $('#divResult'+event.data.param2+event.data.param1).text();
    index=event.data.param1;
}

/*=======  searchTeacher on Press Enter Key ========= */
function chkForEnterSearchTeacher(evt)
{
	var charCode = ( evt.which ) ? evt.which : event.keyCode;	
	if(charCode==13)
	{
		searchCG();
	}	
}
/*========  display grid after Filter ===============*/
function searchCG()
{	
	getCandidateGrid();
	refreshStatus();
	
}
function activecityType(){
	document.getElementById("certType").value='';
}
function showSearchDiv()
{
	document.getElementById("firstName").value=""; 
	
	document.getElementById("lastName").value="";
	document.getElementById("emailAddress").value="";
	document.getElementById("certType").value="";
	document.getElementById("regionName").value="";
	document.getElementById("degreeName").value="";
	document.getElementById("universityName").value="";
	document.getElementById("normScoreSelectVal").value="5";
	document.getElementById("normScore").value="";
	document.getElementById("CGPA").value="";
	document.getElementById("CGPASelectVal").value="5";
	document.getElementById("contacted").value="0";
	document.getElementById("contactedRd0").checked=true;
	document.getElementById("candidateStatusRd0").checked=true;
	document.getElementById("candidateStatus").value="0";
	document.getElementById("certificateTypeMaster").value="0";
	document.getElementById("regionId").value="0";
	document.getElementById("degreeId").value="0";
	document.getElementById("stateId").value="0";
	document.getElementById("universityId").value="0";
	document.getElementById("tagsearchId").value='';
	document.getElementById("ssn").value="";
	
	$('#notReviewedFlag').attr('checked', false);
	
	try{
		document.getElementById("ifrmNorm").src="slideract.do?name=normScoreFrm&tickInterval=10&max=100&swidth=230&svalue=0";
	}catch(e){}
	try{
		document.getElementById("ifrmCGPA").src="slideract.do?name=CGPAFrm&tickInterval=1&max=5&swidth=230&svalue=0";
	}catch(e){}
	document.getElementById("stateId2").value='0';
    document.getElementById("zipCode").value='';
	document.getElementById("divCertificate").style.display='none';
	$('#divCertificate').html("");
	try{
		document.getElementById("ifrmYOTE").src="slideract.do?name=YOTEFrm&tickInterval=5&max=50&swidth=230&svalue=0";
	}catch(e){}
	//$('#myModalSearch').modal('show');
	try{
		$('#myModalSearch').modal('show');
	}catch(err)
	  {}
	$('#advanceSearchDiv').hide();
	$('#searchLinkDiv').show();
}
function hideSearchDiv()
{
	//$('#myModalSearch').modal('hide');
	try{
		$('#myModalSearch').modal('hide');
	}catch(err)
	  {}
}
function checkForCGInt(evt) {

	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==8 || charCode==46)
		return true;

	return( (charCode >= 48 && charCode <= 57)||(charCode==13) );
}
function chkLenOfTextArea(textValue,maxlength)
{
	var tlen=textValue.value.length;
	if(tlen>maxlength)
	{
		document.getElementById(textValue.id).value=document.getElementById(textValue.id).value.substr(0,maxlength)
	}
}
function hideCommunicationsDiv()
{
	//$('#myModalCommunications').modal('hide');
	try{
		$('#myModalCommunications').modal('hide');
	}catch(err)
	  {}
}
function getCommunicationsDiv(commDivFlag,jobForTeacherGId,teacherId,jobId)
{
	hideProfilePopover();
	document.getElementById("commDivFlag").value=commDivFlag;
	document.getElementById("jobForTeacherGId").value=jobForTeacherGId;
	
	//alert(" commDivFlag :"+commDivFlag+" jobForTeacherGId: "+jobForTeacherGId+" teacherId :"+teacherId+" jobId: "+jobId);
	CandidateGridAjaxNew.getCommunications(commDivFlag,jobForTeacherGId,teacherId,jobId,false,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){
		
		//document.getElementById("divCommTxt").innerHTML=data.toString();
		$("#divCommTxt").html(data);
		
		try{
			$("#saveToFolderDiv").modal("hide");
			$('#myModalCommunications').modal('show');
			$("#myModalCommunications").attr("commDivFlag",commDivFlag);
			$("#myModalCommunications").attr("jobForTeacherGId",jobForTeacherGId);
			$("#myModalCommunications").attr("teacherId",teacherId);
			$("#myModalCommunications").attr("jobId",jobId);
		}catch(e){}
		$('#commPhone').tooltip();
		$('#commMsg').tooltip();
		$('#commNotes').tooltip();
	}
	});	
}

function printCommunicationslogs()
{
	var commDivFlag = $("#myModalCommunications").attr("commDivFlag");
	var jobForTeacherGId = $("#myModalCommunications").attr("jobForTeacherGId");
	var teacherId = $("#myModalCommunications").attr("teacherId");
	var jobId = $("#myModalCommunications").attr("jobId");
	
	//alert("commDivFlag:- "+commDivFlag+"\njobForTeacherGId:- "+jobForTeacherGId+"\nteacherId:- "+teacherId+"\njobId:- "+jobId);
	CandidateGridAjaxNew.getCommunications(commDivFlag,jobForTeacherGId,teacherId,jobId,true,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){
			
			var newWindow = window.open();
			newWindow.document.write(data);	
			newWindow.print();
		}
	});
}

function showCommunicationsDiv()
{
	//$('#myModalMessage').modal('hide');
	try{
		$('#myModalMessage').modal('hide');
		$('#myModalNotes').modal('hide');
		$("#saveToFolderDiv").modal("hide");
		$('#myModalCommunications').modal('show');
	}catch(err)
	  {}
	//$('#myModalNotes').modal('hide');
	//$("#saveToFolderDiv").modal("hide");
	//$('#myModalCommunications').modal('show');
}

function showCommunicationsDivForSave()
{
	
	showProfilePopover();
	
	
	//$('#myModalCommunications').modal('hide');
	try{
		$('#myModalCommunications').modal('hide');
	}catch(err)
	  {}
	if(document.getElementById("commDivFlag").value==1){
		//$('#saveToFolderDiv').modal('show');
		try{
			$('#saveToFolderDiv').modal('show');
		}catch(err)
		  {}
	}
}
function showCommunicationsDivForMsg()
{
	//$('#myModalMessage').modal('hide');
	try{
		$('#myModalMessage').modal('hide');
	}catch(err)
	  {}
	var msgType=document.getElementById("msgType").value;
	if(msgType==1){
		//$('#myModalCommunications').modal('show');
		try{
			$('#myModalCommunications').modal('show');
		}catch(err)
		  {}
	}
}
function showCommunicationsForPhone()
{
	var phoneType=document.getElementById("phoneType").value;
	if(phoneType==1){
		//$('#myModalPhone').modal('hide');	
		//$('#myModalCommunications').modal('show');
		try{
			$('#myModalPhone').modal('hide');	
			$('#myModalCommunications').modal('show');
		}catch(err)
		  {}
	}
}
function downloadCommunication(filePath,fileName,linkId)
{
	$('#loadingDiv').show();
	CandidateGridAjaxNew.downloadCommunication(filePath,fileName,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			$('#loadingDiv').hide();
			if(deviceType)
			{
				if (data.indexOf(".doc")!=-1)
				{
					$("#docfileNotOpen").css({"z-index":"3000"});
			    	$('#docfileNotOpen').show();
				}
				else if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					$("#exelfileNotOpen").css({"z-index":"3000"});
			    	$('#exelfileNotOpen').show();
				}
				else
				{
					document.getElementById(linkId).href = data; 			
				}
			}
			else if(deviceTypeAndroid)
			{
				if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					 document.getElementById('ifrmTrans').src = ""+data+"";
				}
				else
				{
					document.getElementById(linkId).href = data; 					
				}				
			}
			else if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
			{
				  
				document.getElementById('ifrmTrans').src = ""+data+"";
			}
			else
			{
				document.getElementById(linkId).href = data; 			
			}		
			return false;			
		}
	});
}
function addNoteFileType()
{
	$('#fileNotes').empty();
	$('#fileNotes').html("<a href='javascript:void(0);' onclick='removeNotesFile();'><img src='images/can-icon.png' title='remove'/></a> <input id='fileNote' name='fileNote' size='20' title='"+resourceJSON.ChooseFile+"' type='file' style='margin-left:20px;margin-top:-15px;'><br>&nbsp;"+resourceJSON.MaxSize10Mb+".");
}
function removeNotesFile()
{
	$('#fileNotes').empty();
	$('#fileNotes').html("<a href='javascript:void(0);' onclick='addNoteFileType();'><img src='images/attach.png'/></a> <a href='javascript:void(0);' onclick='addNoteFileType();'>"+resourceJSON.AttachFile+"</a>");
}
function addPhoneFileType()
{
	$('#filePhones').empty();
	$('#filePhones').html("<a href='javascript:void(0);' onclick='removePhoneFile();'><img src='images/can-icon.png' title='remove'/></a> <input id='filePhone' name='filePhone' size='20' title='"+resourceJSON.ChooseFile+"' type='file' style='margin-left:20px;margin-top:-15px;'><br>&nbsp;"+resourceJSON.MaxSize10Mb+".");
}
function removePhoneFile()
{
	$('#filePhones').empty();
	$('#filePhones').html("<a href='javascript:void(0);' onclick='addPhoneFileType();'><img src='images/attach.png'/></a> <a href='javascript:void(0);' onclick='addPhoneFileType();'>"+resourceJSON.AttachFile+"</a>");
}
function addMessageFileType()
{
	$('#fileMessages').empty();
	$('#fileMessages').html("<a href='javascript:void(0);' onclick='removeMessagesFile();'><img src='images/can-icon.png' title='remove'/></a> <input id='fileMessage' name='fileNote' size='20' title='"+resourceJSON.ChooseFile+"' type='file' style='margin-left:20px;margin-top:-15px;'><br>&nbsp;"+resourceJSON.MaxSize10Mb+".");
}
function removeMessagesFile()
{
	$('#fileMessages').empty();
	$('#fileMessages').html("<a href='javascript:void(0);' onclick='addMessageFileType();'><img src='images/attach.png'/></a> <a href='javascript:void(0);' onclick='addMessageFileType();'>"+resourceJSON.AttachFile+"</a>");
}

function generateCGExcel()
{
	//alert("GenerateCGExcel: "+jobId);	
	$('#loadingDiv').show();
	var jobId=document.getElementById("jobId").value;
	var firstName		=	trim(document.getElementById("firstName").value); 
	var lastName		=	trim(document.getElementById("lastName").value);
	var emailAddress	=	trim(document.getElementById("emailAddress").value);
	var certType		=	trim(document.getElementById("certificateTypeMaster").value);
	var regionId		=	trim(document.getElementById("regionId").value);
	var degreeId		=	trim(document.getElementById("degreeId").value);
	var universityId		=	trim(document.getElementById("universityId").value);
	var normScoreSelectVal		=	trim(document.getElementById("normScoreSelectVal").value);
	var normScore		=	trim(document.getElementById("normScore").value);
	var CGPA		=	trim(document.getElementById("CGPA").value);
	var CGPASelectVal		=	trim(document.getElementById("CGPASelectVal").value);
	var contacted		=	trim(document.getElementById("contacted").value);
	var candidateStatus		=	trim(document.getElementById("candidateStatus").value);
	var stateId=document.getElementById("stateId").value;
	try{
		var ifrmYOTE = document.getElementById('ifrmYOTE');
		var innerYOTE = ifrmYOTE.contentDocument || ifrmYOTE.contentWindow.document;
		var inputYOTE = innerYOTE.getElementById('YOTEFrm');
		document.getElementById("YOTE").value=inputYOTE.value;
	}catch(e){}
	var YOTESelectVal		=	trim(document.getElementById("YOTESelectVal").value);
	var YOTE				=	document.getElementById("YOTE").value;
	var hiddenCertTypeIds 	=	document.getElementsByName("hiddenCertTypeId");
	var stateId2	=	trim(document.getElementById("stateId2").value);
    var zipCode		=	trim(document.getElementById("zipCode").value);
	var certIds =[];
	for(var i=0;i<hiddenCertTypeIds.length;i++)
	{
		certIds.push(hiddenCertTypeIds[i].value);
	}
	var epiFromDate = "";
	var epiToDate = "";
     try{
	var epiFromDate	=	trim(document.getElementById("epiFromDate").value);
    var epiToDate	=	trim(document.getElementById("epiToDate").value);
     }catch(err){}
	
	CandidateGridAjaxNew.generateCGExcel(jobId,firstName,lastName,emailAddress,certType,regionId,degreeId,universityId,normScoreSelectVal,
			normScore,CGPA,CGPASelectVal,contacted,candidateStatus,stateId,orderColumn, sortingOrder,totalNoOfRecord,false,null,certIds,YOTESelectVal,YOTE,stateId2,zipCode,epiFromDate,epiToDate,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{		
		$('#loadingDiv').hide();
		if(deviceType)
		{
			if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
			{
				$("#exelfileNotOpen").css({"z-index":"3000"});
		    	$('#exelfileNotOpen').show();
			}		
		}	
		else
		{
			document.getElementById('ifrmTrans').src = "candidate/"+data+"";	
		}	
		}});

}
function generateCGExcelForTeachers()
{
	//alert("GenerateCGExcel: "+jobId);
	
	$('#loadingDiv').show();
	var jobId=document.getElementById("jobId").value;
	var firstName		=	""; 
	var lastName		=	"";
	var emailAddress	=	"";
	var certType		=	"";
	var regionId		=	"";
	var degreeId		=	"";
	var universityId		=	"";
	var normScoreSelectVal		=	"";
	var normScore		=	"";
	var CGPA		=	"";
	var CGPASelectVal		=	"";
	var contacted		=	"";
	var candidateStatus		=	"";
	var stateId="0";
	var ssn = "";
	
	var savecandidatearray="";
	 
	var inputs = document.getElementsByName("cgCBX"); 
	 for (var i = 0; i < inputs.length; i++) {
	        if (inputs[i].type === 'checkbox') {
	        	if(inputs[i].checked){
	        		//alert("LLL "+inputs[i].id);
	        		var hid=inputs[i].id;
	        		var cc =hid.replace("cgCBX","");
	        		var secType=document.getElementById("sel"+inputs[i].value).value;
	        		
	        		savecandidatearray+=secType+inputs[i].value+",";
	            }
	        }
	 }
	 var hiddenCertTypeIds 	=	document.getElementsByName("hiddenCertTypeId");
	 var certIds =[];
	 	for(var i=0;i<hiddenCertTypeIds.length;i++)
	 	{
	 		certIds.push(hiddenCertTypeIds[i].value);
	 	}
	 var YOTESelectVal="",YOTE="",stateId2="",zipCode="",epiFromDate="",epiToDate="";
	// alert("pawan"+savecandidatearray);
	/* CandidateGridAjaxNew.generateCGExcel(jobId,firstName,lastName,emailAddress,certType,regionId,degreeId,universityId,normScoreSelectVal,
			normScore,CGPA,CGPASelectVal,contacted,candidateStatus,stateId,orderColumn, sortingOrder,totalNoOfRecord,true,savecandidatearray,ssn,{*/ 
	 CandidateGridAjaxNew.generateCGExcel(jobId,firstName,lastName,emailAddress,certType,regionId,degreeId,universityId,normScoreSelectVal,
				normScore,CGPA,CGPASelectVal,contacted,candidateStatus,stateId,orderColumn, sortingOrder,totalNoOfRecord,true,savecandidatearray,certIds,YOTESelectVal,YOTE,stateId2,zipCode,epiFromDate,epiToDate,ssn,{
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
		//alert(data);
		$('#loadingDiv').hide();
		document.getElementById('ifrmTrans').src = "candidate/"+data+"";
		}});
	// $('#loadingDiv').hide();
}
function generateCGPDFForTeachers()
{
	
	$('#loadingDiv').show();
	var jobId=document.getElementById("jobId").value;
	var firstName		=	""; 
	var lastName		=	"";
	var emailAddress	=	"";
	var certType		=	"";
	var regionId		=	"";
	var degreeId		=	"";
	var universityId		=	"";
	var normScoreSelectVal		=	"";
	var normScore		=	"";
	var CGPA		=	"";
	var CGPASelectVal		=	"";
	var contacted		=	"";
	var candidateStatus		=	"";
	var stateId="0";
	
	var savecandidatearray="";
	
	var ssn="";
	 
	var inputs = document.getElementsByName("cgCBX"); 
	 for (var i = 0; i < inputs.length; i++) {
	        if (inputs[i].type === 'checkbox') {
	        	if(inputs[i].checked){
	        		//alert("LLL "+inputs[i].id);
	        		var hid=inputs[i].id;
	        		var cc =hid.replace("cgCBX","");
	        		var secType=document.getElementById("sel"+inputs[i].value).value;
	        		
	        		savecandidatearray+=secType+inputs[i].value+",";
	            }
	        }
	 }
	 
	 
	 CandidateGridAjaxNew.downloadCandidateGridReport(jobId,firstName,lastName,emailAddress,certType,regionId,degreeId,universityId,normScoreSelectVal,
				normScore,CGPA,CGPASelectVal,contacted,candidateStatus,stateId,orderColumn, sortingOrder,totalNoOfRecord,true,savecandidatearray,ssn,{
				async: true,
					errorHandler:handleError,
					callback:function(data)
					{	
						$('#loadingDiv').hide();
						//window.open(data);
						//openWindow(data,'Support',650,490);
						
						$('#loadingDiv').hide();
						if(deviceType)
						{
							if (data.indexOf(".doc")!=-1)
							{
								$("#docfileNotOpen").css({"z-index":"3000"});
						    	$('#docfileNotOpen').show();
							}
							else if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
							{
								$("#exelfileNotOpen").css({"z-index":"3000"});
						    	$('#exelfileNotOpen').show();
							}
							else
							{
								$('#modalDownloadCGR').modal('hide');
								document.getElementById(hrefId).href=data;				
							}
						}
						else if(deviceTypeAndroid)
						{
							if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
							{
								  try{
								    $('#modalDownloadCGR').modal('hide');
									}catch(err)
									  {}
								    document.getElementById('ifrmCGR').src = ""+data+"";
							}
							else
							{
								$('#modalDownloadCGR').modal('hide');
								document.getElementById(hrefId).href=data;					
							}				
						}
						else if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
						{
							  try{
							    	$('#modalDownloadCGR').modal('hide');
								}catch(err)
								  {}
							    document.getElementById('ifrmCGR').src = ""+data+"";
						}
						else
						{
							$('#modalDownloadCGR').modal('hide');
							document.getElementById('ifrmCGR').src = ""+data+"";
							try{
								$('#modalDownloadCGR').modal('show');
							}catch(err)
							  {}
							
						}		
						
						return false;
					}});
}
//start from by ramesh

function showProfileContentClose(){	
	
	if(deviceType || deviceTypeAndroid)
	{
		$('#cgTeacherDivMaster').modal('hide'); 
		$('#cgTeacherDivMaster').hide();  
		var selId=$("#pnqList option:selected").val();;
		document.getElementById("teacherIdForprofileGrid").value="";
		currentPageFlag="";	
	}
	else
	{
		$('#cgTeacherDivMaster').modal('hide');  
		var selId=$("#pnqList option:selected").val();;
	    document.getElementById("teacherIdForprofileGrid").value="";
		currentPageFlag="";
	}
	//getCandidateGrid();
	//refreshStatus();
}

function showProfileContent(dis,jobforteacherId,teacherId,normscore,jobId,colorName,noOfRecordCheck,event)
{
	$("#showHideType").val("0");
	//for smart practices reset.
	spJobforteacherId = jobforteacherId;
	spTeacherId = teacherId;
	spNormscore = normscore;
	spJobId = jobId;
	spColorName = colorName;
	spNoOfRecordCheck = noOfRecordCheck;
	spEvent = event;
	
	//for assessment reset.
	assessmentJobforteacherId = jobforteacherId;
	assessmentTeacherId = teacherId;
	assessmentNormscore = normscore;
	assessmentJobId = jobId;
	assessmentColorName = colorName;
	assessmentNoOfRecordCheck = noOfRecordCheck;
	assessmentEvent = event;
	
	var scrollTopPosition = $(window).scrollTop();
	var x=event.clientX;
	var y=event.clientY;
	var z=y-180+scrollTopPosition;
	z=z+80;	
	var style = $('<style>.popover { position: absolute;top: '+z+'px !important;left: 0px;width: 770px; }</style>')
	$('html > head').append(style);	
	//showProfileContentClose();
	if(deviceType || deviceTypeAndroid)
	{
		$('#cgTeacherDivMaster').modal('hide'); 
		$('#cgTeacherDivMaster').hide();  
		var selId=$("#pnqList option:selected").val();;
		document.getElementById("teacherIdForprofileGrid").value="";
		currentPageFlag="";	
	}
	else
	{
		$('#cgTeacherDivMaster').modal('hide');  
		var selId=$("#pnqList option:selected").val();;
	    document.getElementById("teacherIdForprofileGrid").value="";
		currentPageFlag="";
	}
	refreshStatus();
	$('#loadingDiv').show();
	
	document.getElementById("teacherIdForprofileGrid").value=teacherId;		
	TeacherProfileViewInDivAjax.showProfileContent(jobforteacherId,teacherId,normscore,jobId,colorName,noOfRecordCheck,0,{ 
	async: true,
	callback: function(data)
	{  
		$("#cgTeacherDivMaster").modal('show');
		
		//var wait="<div id='removeWait' style='text-align:center;padding-top:0px;'><img src='images/loadingAnimation.gif' /> <br>Loading...</div>";
		$("#cgTeacherDivBody").html(data);
         
		TeacherProfileViewInDivAjax.showProfileContent(jobforteacherId,teacherId,normscore,jobId,colorName,noOfRecordCheck,1,{ 
			async: true,
			callback: function(redata){
			setTimeout(function () {
				$("#profile_id").after(redata);
				$('#removeWait').remove();
	        }, 100);
		}
		});	
		$('#loadingDiv').hide();
		  $('#tpResumeprofile').tooltip();
		  $('#tpPhoneprofile').tooltip();
		  $('#tpPDReportprofile').tooltip();
		  $('#tpScoreReportGE').tooltip();
		  $('#tpScoreReportSA').tooltip();
		  $('#tpJSAProfile').tooltip();
		  $('#tpTeacherProfileVisitHistory').tooltip();
		  try{
			  $('#tpDisconnect').tooltip();
		  }catch(e){}
		  $('textarea').jqte();
	},
	errorHandler:handleError  
	});
	
} 

function getPhoneDetailShow(teacherId){
	hideProfilePopover();
	
	TeacherProfileViewInDivAjax.getPhoneDetailByDiv(teacherId,
	{
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			if(data==""){
				document.getElementById("divAlertText").innerHTML="No Phone number is available for this candidate.";
				try{$('#divAlert').modal('show');}catch(err){}
			}
			else{
				document.getElementById("divPhoneByPro").innerHTML=data;			
				try{
					$('#myModalPhoneShow').modal('show');
				}catch(err){}
			}
			
		}
	});
}


function getPFEmploymentGrid_DivProfile(teacherId)
{
	var visitLocation=document.getElementById("teacherIdForprofileGridVisitLocation").value;
	
	TeacherProfileViewInDivAjax.getGridDataWorkExpEmployment(teacherId,noOfRowsworkExp,pageworkExp,sortOrderStrworkExp,sortOrderTypeworkExp,visitLocation, { 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#gridDataWorkExpEmployment').html(data);
			if(visitLocation=="CG View"){
				applyScrollOnTblWorkExp();
			}

		}});
}

function getElectronicReferencesGrid_DivProfile(teacherId)
{
	var districtMaster = null;
	var schoolMaster = null;
	
	var visitLocation=document.getElementById("teacherIdForprofileGridVisitLocation").value;
	var jobId=document.getElementById("jobId").value;
	
	TeacherProfileViewInDivAjax.getElectronicReferencesGrid(teacherId,noOfRowsReferences,pageReferences,sortOrderStrReferences,sortOrderTypeReferences,visitLocation,jobId,districtMaster,schoolMaster, { 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
		$('#gridDataReference').html(data);
		if(visitLocation=="CG View"){
			applyScrollOnTblEleRef_profile();
		}

		}});
}


function getVideoLinksGrid_DivProfile(teacherId)
{
	var visitLocation=document.getElementById("teacherIdForprofileGridVisitLocation").value;	
	TeacherProfileViewInDivAjax.getVideoLinksGrid(teacherId,noOfRowsvideoLink,pagevideoLink,sortOrderStrvideoLink,sortOrderTypevideoLink,visitLocation, { 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#gridDataVideoLink').html(data);
			if(visitLocation=="CG View"){
				applyScrollOnTblVideoLinks_profile();
			}

		}});
}
//Background Check
function getBackgroundCheckGrid_DivProfile(teacherId)
{
	TeacherProfileViewInDivAjax.getBackgroundCheckGrid_DivProfile(teacherId, { 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#gridDataBackgroundCheck').html(data);			
			applyScrollOnTblBackgroundCheck_profile();
		}});
}

function getadddocumentGrid_DivProfile(teacherId)
{
	//alert(noOfRowsvideoLink+":::::::::::::"+pagevideoLink+"::"+sortOrderStrvideoLink+"::"+sortOrderTypevideoLink+"::"+teacherId);
	var visitLocation=document.getElementById("teacherIdForprofileGridVisitLocation").value;	
	TeacherProfileViewInDivAjax.getAdditionalDocumentsGrid(noOfRowsaddDoc,pageaddDoc,sortOrderStraddDoc,sortOrderTypeaddDoc,teacherId,visitLocation, { 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#divGridAdditionalDocuments').html(data);
			if(visitLocation=="CG View"){
				applyScrollOnTbl_AdditionalDocuments();
			}
		}});
}

//For language profiency
function getlanguage_DivProfile(teacherId)
{
	//alert(noOfRowsaddDoc+":::::::::::::"+pageaddDoc+"::"+sortOrderStraddDoc+"::"+sortOrderTypeaddDoc+"::"+teacherId);
	var visitLocation=document.getElementById("teacherIdForprofileGridVisitLocation").value;
	TeacherProfileViewInDivAjax.displayTeacherLanguageByTeacherId(noOfRowsLang,pageLang,sortOrderStrLang,sortOrderTypeLang,teacherId,visitLocation, { 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#divGridlanguageProfiency').html(data);
			applyScrollOnTblLanguage();

		}});
}
//For Additional Document
function getdistrictAssessmetGrid_DivProfile(teacherId)
{
	//alert(noOfRowsaddDoc+":::::::::::::"+pageaddDoc+"::"+sortOrderStraddDoc+"::"+sortOrderTypeaddDoc+"::"+teacherId);
	var visitLocation=document.getElementById("teacherIdForprofileGridVisitLocation").value;
	TeacherProfileViewInDivAjax.getdistrictAssessmetGrid(noOfRowsASMT,pageASMT,sortOrderStrASMT,sortOrderTypeASMT,teacherId,visitLocation, { 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#divGridAssessmentDetails').html(data);
			applyScrollOnTbl_AssessmentDetails();

		}});
}
function setGridProfileVariable(currentPageFlagValue){
	currentPageFlag=currentPageFlagValue;
}

function getTeacherProfileVisitHistoryShow_FTime(teacherId){
	
	
	var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
	currentPageFlag="tpvh";
	getPagingAndSorting('1',"visitedDateTime",1);
	
	hideProfilePopover();
}

function getTeacherProfileVisitHistoryShow(teacherId){
	
	$('#loadingDiv').show();
	
	currentPageFlag="tpvh";
	
	var visitLocation=document.getElementById("teacherIdForprofileGridVisitLocation").value;
	
	TeacherProfileViewInDivAjax.getGridDataForTeacherProfileVisitHistoryByTeacher(teacherId,noOfRowstpvh,pagetpvh,sortOrderStrtpvh,sortOrderTypetpvh,visitLocation, { 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#loadingDiv').hide();
			try{
				$('#divteacherprofilevisithistory').html(data);
				if(visitLocation=="CG View"){
					applyScrollOnTblProfileVisitHistory();
				}
				$('#myModalProfileVisitHistoryShow').modal('show');
			}catch(e){}
		}});
}

function getTeacherAcademicsGrid_DivProfile(teacherId)
{
	var visitLocation=document.getElementById("teacherIdForprofileGridVisitLocation").value;
	TeacherProfileViewInDivAjax.getTeacherAcademicsGrid(teacherId,noOfRowsteacherAce,pageteacherAce,sortOrderStrteacherAce,sortOrderTypeteacherAce,visitLocation, { 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#gridDataTeacherAcademics').html(data);
			if(visitLocation=="CG View"){
				applyScrollOnTblTeacherAcademics_profile();
			}
		}});
}

function getTeacherEducationGrid_DivProfile(teacherId)
{
	
 var visitLocation=document.getElementById("teacherIdForprofileGridVisitLocation").value;
 TeacherProfileViewInDivAjax.getTeacherEducationGrid(teacherId, { 
  async: true,
  errorHandler:handleError,
  callback:function(data)
  {
	
   $('#gridDataTeacherEducations').html(data);
   //applyScrollOnTblTeacherAcademics_profile();
   applyScrollOnEducation();
  }});
}



function getDistrictSpecificQuestionData(districtId,teacherId,headQuarterId,branchId)
{	
	dId=districtId;
	tId=teacherId;
	CandidateGridAjaxNew.getDistrictSpecificQuestionData(districtId,teacherId,headQuarterId,branchId,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{	
			$('#getDistrictSpecificQuestion').html(data);
		}
	});
}



function getTeacherCertificationsGrid_DivProfile(teacherId)
{
	var visitLocation=document.getElementById("teacherIdForprofileGridVisitLocation").value;
	TeacherProfileViewInDivAjax.getTeacherCertificationsGrid(teacherId,noOfRowsteacherCerti,pageteacherCerti,sortOrderStrteacherCerti,sortOrderTypeteacherCerti,visitLocation, { 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#gridDataTeacherCertifications').html(data);
			if(visitLocation=="CG View"){
				applyScrollOnTblTeacherCertifications_profile();
			}
		}});
}


var dId=0;
var tId=0;
function printQualificationQuestion()
{	
	if(dId!=0 && tId!=0){
		CandidateGridAjaxNew.getDistrictSpecificQuestionData(dId,tId,{ 
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{	
				var teacherEmailAddress=$("#teacherEmailAddress").val();
				var statusNote=$("#statusNoteID").val();
				document.write("<html><body><table>");
				document.write("<tr><td><b>E-mail:&nbsp;&nbsp;</b><a href='mailTo:"+teacherEmailAddress+"'>"+teacherEmailAddress+"</a></td></tr>");
				document.write("<tr></tr>");
				
				if(statusNote!=""){
					document.write("<tr><td><b>Notes:&nbsp;</b>"+statusNote+"</td></tr>");
					document.write("<tr></tr>");
				}
				
				document.write("<tr>");
				document.write("<td>"+data+"</td>");
				document.write("</tr>");
				document.write("</table></body></html>");
				window.print();
			}
		});
	}
}

function getDistrictSpecificQuestion_DivProfile(jobId,teacherId,sVisitLocation)
{	
	TeacherProfileViewInDivAjax.getDistrictSpecificQuestionGrid(jobId,teacherId,sVisitLocation,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{	
			$('#getDistrictSpecificQuestion').html(data);
		}
	});
}







function getDistrictSpecificQuestion_DivProfile_FromMyFolders(districtId,teacherId,sVisitLocation)
{	
	if(sVisitLocation==null)sVisitLocation="";
	TeacherProfileViewInDivAjax.getDistrictSpecificQuestionGrid_ByDistrict(districtId,teacherId,sVisitLocation,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#getDistrictSpecificQuestion').html(data);
		}});
}

function getQuestionExplain(questionID)
{
	TeacherProfileViewInDivAjax.getQAExplaination(questionID,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){
		$("#divExplain").html(data);
		try{
				$('#myModalQAEXEditor').show();
				
				$("#draggableDivMaster").hide();	
				$("#cgTeacherDivMaster").hide();
			}catch(e){
				//alert(e);
			}
		}
		});
}

function showPreviousModel()
{
	$("#draggableDivMaster").show();
	var showType=$("#showHideType").val();
	if(showType==0){
		$("#cgTeacherDivMaster").show();
	}else{
		$("#cgTeacherMasterDiv").show();
	}
	$('#myModalQAEXEditor').hide();
}

function updateAScore(teacherId,districtId){
	
	var slider_aca_ach=0;
	var slider_lea_res=0;
	try{
		var ifrm_aca_ach = document.getElementById('ifrm_aca_ach');
		var innerNorm = ifrm_aca_ach.contentDocument || ifrm_aca_ach.contentWindow.document;
		if(innerNorm.getElementById('slider_aca_ach').value!=''){
			//$('#candidateFeedNormScore').val(innerNorm.getElementById('slider_aca_ach').value);
			slider_aca_ach=innerNorm.getElementById('slider_aca_ach').value;
		}
		}catch(e){}
		
		
	try{
		var ifrm_lea_res = document.getElementById('ifrm_lea_res');
		var innerNorm1 = ifrm_lea_res.contentDocument || ifrm_lea_res.contentWindow.document;
		if(innerNorm1.getElementById('slider_lea_res').value!=''){
			//$('#candidateFeedNormScore').val(innerNorm.getElementById('slider_aca_ach').value);
			slider_lea_res=innerNorm1.getElementById('slider_lea_res').value;
		}
		}catch(e){}
	
		var txt_sl_slider_aca_ach = document.getElementById('txt_sl_slider_aca_ach').value;
		var txt_sl_slider_lea_res = document.getElementById('txt_sl_slider_lea_res').value;
		
		//if((slider_aca_ach!=0 || slider_lea_res!=0) && ( txt_sl_slider_aca_ach!=slider_aca_ach || txt_sl_slider_lea_res!=slider_lea_res)){
		if(txt_sl_slider_aca_ach!=slider_aca_ach || txt_sl_slider_lea_res!=slider_lea_res){
			TeacherProfileViewInDivAjax.getUpdateAndSaveAScore(teacherId,districtId,slider_aca_ach,slider_lea_res,{ 
				async: true,
				errorHandler:handleError,
				callback:function(data)
				{
					if(data!="")
					{
						document.getElementById('txt_sl_slider_aca_ach').value=slider_aca_ach;
						document.getElementById('txt_sl_slider_lea_res').value=slider_lea_res;
						$('#ascore_profile').html(data[0]);
						$('#lrscore_profile').html(data[1]);
						$("#tdCGViewAScore_"+teacherId).html(data[0]);
						$("#tdCGViewlrScore_"+teacherId).html(data[1]);
					
					}
					
				}});
		}
}


function showProfileContentClose_ShareFolder(){
	defaultSaveDiv();
    //$('.profile').popover('destroy');
	document.getElementById("teacherIdForprofileGrid").value="";
    
}

function showProfileContentForTeacher(dis,teacherId,noOfRecordCheck,sVisitLocation,event)
{	
	// by alok for dynamic div width
	   try
	   {
		   var mydiv = document.getElementById("mydiv");
	       var curr_width = parseInt(mydiv.style.width);
	      
	       if(deviceType)
	       {
	    	   mydiv.style.width = 630+"px";  
	       }
	       else
		   {
	    	   mydiv.style.width = 647+"px";  
		   }      
	   }
	   catch(e){alert(e)}
	   
	var districtMaster = null;
	showProfileContentClose_ShareFolder();
	$('#loadingDiv').show();
	document.getElementById("teacherIdForprofileGrid").value=teacherId;
	TeacherProfileViewInDivAjax.showProfileContentForTeacher(teacherId,noOfRecordCheck,sVisitLocation,districtMaster,0,{ 
	async: true,
	callback: function(data)
	{
	  $('#loadingDiv').hide();
	  $("#draggableDivMaster").modal('show');
	  
	  //var wait="<div id='removeWait' style='text-align:center;padding-top:0px;'><img src='images/loadingAnimation.gif' /> <br>Loading...</div>";
		$("#draggableDiv").html(data);
       
		TeacherProfileViewInDivAjax.showProfileContentForTeacher(teacherId,noOfRecordCheck,sVisitLocation,districtMaster,1,{ 
			async: true,
			callback: function(resdata){
			setTimeout(function () {
				$("#profile_id").after(resdata);
				$('#removeWait').remove();
	        }, 100);
			
			//$("#draggableDiv").html(resdata);
		}
		});
	  
	  
	  $('#tpResumeprofile').tooltip();
	  $('#tpPhoneprofile').tooltip();
	  $('#tpPDReportprofile').tooltip();
	  $('#tpTeacherProfileVisitHistory').tooltip();
	  $('#teacherProfileTooltip').tooltip();
	  $('textarea').jqte();
	},
	errorHandler:handleError  
	});
} 

function showProfilePopover()
{
		//$("#cgTeacherDivMaster").modal('show');
	var showType=$("#showHideType").val();
		try
		{
			$("#draggableDivMaster").modal('show');
			if(showType==0){
				$("#cgTeacherDivMaster").modal('show');
			}else{
				$("#cgTeacherMasterDiv").modal('show');
			}
		}
		catch(e){}
		
		//$('#myModalJobList').hide(); // @AShish :: Hide for Job Details(myModalJobList) Div
		//$('#myModalReferenceNoteView').hide();
		
		//Refresh for Reference Notes
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		//getElectronicReferencesGrid_DivProfile(teacherIdForprofileGrid);                    //Commented By Deepak
		$('#tpResumeprofile').tooltip();
		$('#tpPhoneprofile').tooltip();
		$('#tpPDReportprofile').tooltip();
		$('#tpJSAProfile').tooltip();
		$('#tpTeacherProfileVisitHistory').tooltip();
}
function hideProfilePopover()
{
	var showType=$("#showHideType").val();
	$("#draggableDivMaster").modal('hide');
		if(showType==0){
			$("#cgTeacherDivMaster").modal('hide');
		}else{
			$("#cgTeacherMasterDiv").modal('hide');
		}
}


function closeRefNotesDivEditor()
{
	var eleRefId=document.getElementById("eleRefId").value;
	getRefNotesDiv(eleRefId);
}

function getRefNotesDiv(eleRefId)
{
	var districtMaster = null;
	var schoolMaster = null;
	
	
	hideProfilePopover();
	
	document.getElementById("eleRefId").value=eleRefId;
	
	//$('#myModalReferenceNotesEditor').modal('hide');
	try{
		$('#myModalReferenceNotesEditor').modal('hide');
		$('#myModalReferenceNotesEditor').modal('hide');
	}catch(err)
	  {}
	//$('#myModalReferenceNotesEditor').modal('hide');
	
	var jobId = document.getElementById("jobId").value;
	TeacherProfileViewInDivAjax.getRefeNotes(eleRefId,jobId,districtMaster,schoolMaster,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){
		
		$("#divRefNotesInner").html(data);
		$('#myModalReferenceNoteView').modal('show');
		//$('#myModalReferenceNoteView').show();
		$('#refeNotesId').tooltip();
	}
	});
}

function getNotesEditorDiv()
{
	//$('#myModalReferenceNoteView').modal('hide');
	try{
		$('#myModalReferenceNoteView').modal('hide');
	}catch(err)
	  {}
	
	$('#myModalReferenceNoteView').modal('hide');
	//$('#myModalReferenceNoteView').hide();
	removeRefeNotesFile();
	$('#errordivNotes_ref').empty();
	$('#divTxtNode_ref').find(".jqte_editor").html("");	
	$('#divTxtNode_ref').find(".jqte_editor").css("background-color", "");
	//$('#myModalReferenceNotesEditor').modal('show');
	try{
		$('#myModalReferenceNotesEditor').modal('show');
	}catch(err)
	  {}
	
}


function saveReferenceNotes(){
	var noteFileName="";
	var notes = $('#divTxtNode_ref').find(".jqte_editor").html();
	var teacherId = document.getElementById("teacherIdForprofileGrid").value;
	var eleRefId = document.getElementById("eleRefId").value;
	var jobId = document.getElementById("jobId").value;
	
	var fileNote=null;
	
	try{
		fileNote=document.getElementById("fileNote_ref").value;
	}catch(e){}
	
	var cnt=0;
	var focs=0;
	
	$('#errordivNotes_ref').empty();
	
	if ($('#divTxtNode_ref').find(".jqte_editor").text().trim()=="")
	{
		$('#errordivNotes_ref').append("&#149; "+resourceJSON.PlzEtrNotes+".<br>");
		if(focs==0)
			$('#divTxtNode_ref').find(".jqte_editor").focus();
		$('#divTxtNode_ref').find(".jqte_editor").css("background-color", "#F5E7E1");
		cnt++;
	}else if(fileNote==""){
		$('#errordivNotes_ref').show();
		$('#errordivNotes_ref').append("&#149; "+resourceJSON.PlzUpldNotes+".<br>");
		cnt++;
	}else if(fileNote!="" && fileNote!=null)
	{
		var ext = fileNote.substr(fileNote.lastIndexOf('.') + 1).toLowerCase();	
		noteFileName="ref_note"+noteDateTime+"."+ext;
		var fileSize = 0;
		if ($.browser.msie==true)
	 	{	
		    fileSize = 0;	   
		}
		else
		{
			if(document.getElementById("fileNote_ref").files[0]!=undefined)
			{
				fileSize = document.getElementById("fileNote_ref").files[0].size;
			}
		}
		
		if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt' || ext=='xlsx' || ext=='xls'))
		{
			$('#errordivNotes_ref').show();
			$('#errordivNotes_ref').append("&#149; "+resourceJSON.PlzSelectNoteFormat+".<br>");
			cnt++;
		}
		else if(fileSize>=10485760)
		{
			$('#errordivNotes_ref').show();
			$('#errordivNotes_ref').append("&#149; "+resourceJSON.FileSizeLessthan10Mb+".<br>");
			cnt++;
		}
	}
	if(cnt==0){	
		try{
			$('#loadingDiv').show();
			if(fileNote!="" && fileNote!=null)
			{
				document.getElementById("frmNoteUpload_ref").submit();
			}
			else
			{
				TeacherProfileViewInDivAjax.saveReferenceNotesByAjax(eleRefId,teacherId,jobId,notes,noteFileName,
				{
					async:false,
					errorHandler:handleError,
					callback:function(data)
					{
						$('#loadingDiv').hide();
						getRefNotesDiv(eleRefId);
					}
				});	
			}
		}catch(err){}
	}

	
}

function saveRefNoteFile(noteDateTime)
{	
	var notes = $('#divTxtNode_ref').find(".jqte_editor").html();
	var teacherId = document.getElementById("teacherIdForprofileGrid").value;
	var eleRefId = document.getElementById("eleRefId").value;
	var jobId = document.getElementById("jobId").value;
	
	var fileNote=null;
	try{
		fileNote=document.getElementById("fileNote_ref").value;
	}catch(e){}
	
	var noteFileName="";
	
	if(fileNote!="" && fileNote!=null)
	{
		var ext = fileNote.substr(fileNote.lastIndexOf('.') + 1).toLowerCase();	
		noteFileName="ref_note"+noteDateTime+"."+ext;
	}
	
	TeacherProfileViewInDivAjax.saveReferenceNotesByAjax(eleRefId,teacherId,jobId,notes,noteFileName,
			{
				async:false,
				errorHandler:handleError,
				callback:function(data)
				{
					$('#loadingDiv').hide();
					getRefNotesDiv(eleRefId);
				}
			});	
}

function addRefeNoteFileType()
{
	$('#fileRefNotes').empty();
	$('#fileRefNotes').html("<a href='javascript:void(0);' onclick='removeRefeNotesFile();'><img src='images/can-icon.png' title='remove'/></a> <input id='fileNote_ref' name='fileNote_ref' size='20' style='margin-left:20px;margin-top:-15px;' title='"+resourceJSON.ChooseFile+"' type='file'><br>&nbsp;"+resourceJSON.MaxSize10Mb+".");
}

function removeRefeNotesFile()
{
	$('#fileRefNotes').empty();
	$('#fileRefNotes').html("<a href='javascript:void(0);' onclick='addRefeNoteFileType();'><img src='images/attach.png'/></a> <a href='javascript:void(0);' onclick='addRefeNoteFileType();'>"+resourceJSON.AttachFile+"</a>");
}

function downloadReferenceNotes(filePath,fileName,linkId)
{
	$('#loadingDiv').show();
	TeacherProfileViewInDivAjax.downloadReferenceNotes(filePath,fileName,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			$('#loadingDiv').hide();
			if(deviceType)
			{
				if (data.indexOf(".doc")!=-1)
				{
					$("#docfileNotOpen").css({"z-index":"3000"});
			    	$('#docfileNotOpen').show();
				}
				else if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					$("#exelfileNotOpen").css({"z-index":"3000"});
			    	$('#exelfileNotOpen').show();
				}
				else
				{
					document.getElementById(linkId).href = data; 
				}
			}
			else if(deviceTypeAndroid)
			{
				if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					document.getElementById('ifrmTrans').src = ""+data+"";
				}
				else
				{
					document.getElementById(linkId).href = data; 	
				}	
			}
			else if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
			{
				document.getElementById('ifrmTrans').src = ""+data+"";
			}
			else
			{
				document.getElementById(linkId).href = data; 	
			}			 
			return false;		
		}
	});
}
function setPageFlag()
{	
	$('#myModalJobList').hide();
	var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
	if(teacherIdForprofileGrid!='')
	{
		showProfilePopover();
	}
}

function getPhoneDetailShowPro(teacherId){
	hideProfilePopover();
	TeacherProfileViewInDivAjax.getPhoneDetailByDiv(teacherId,
	{
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			if(data==""){
				document.getElementById("divAlertText").innerHTML=""+resourceJSON.MsgPhNoIsNotAvailable+"";
				try{$('#divAlert').modal('show');
								}catch(err){}
			}
			else{
				document.getElementById("divPhoneByPro").innerHTML=data;			
				try{$('#myModalPhoneShowPro').modal('show');
								}catch(err){}
			}
			
		}
	});
}



/**************************** Send Message ****************/

function sendMessage()
{
	var msgType	=	2;
	var jobForTeacherIds = new Array();
	jobForTeacherIds	=	getSendMessageTeacherIds();
	var jobId	=	$("#jobId").val();
	var emailId	=	"";
	getMessageDiv(jobForTeacherIds,emailId,jobId,msgType)
	//TeacherSendMessageAjax
	CGServiceAjax.getEmailAddress(jobForTeacherIds,
	{
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			document.getElementById("emailDiv").innerHTML=data;
		}
	});
	//TeacherSendMessageAjax
	CGServiceAjax.getTeacherIds(jobForTeacherIds,
	{
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			document.getElementById("teacherIdForMessage").value=data;	
			//document.getElementById("emailDiv").innerHTML=data;
		}
	});
	
}

function getSendMessageTeacherIds()
{
	var jobForTeacherIds = new Array();
	var inputs = document.getElementsByName("cgCBX"); 
	for (var i = 0; i < inputs.length; i++) 
	{
		if (inputs[i].type === 'checkbox') 
		{
			if(inputs[i].checked)
			{
				jobForTeacherIds[i]=inputs[i].value;
	        }
	    }
	} 
	return jobForTeacherIds;
}

function confMessageForSendMessage()
{	
	$('#lodingImage').empty();
	//$('#myModalMessage').modal('hide');
	//$('#myModalMsgShow_SendMessage').modal('show');
	try{
		$('#myModalMessage').modal('hide');
		$('#myModalMsgShow_SendMessage').modal('show');
	}catch(err)
	  {}
	//document.getElementById("message2show_SendMessage").innerHTML="Your message is successfully sent to the Candidate.";
}

/* @Start
 * @Ashish Kumar
 * @Description :: Show Qualification details Div and save Qualification Note
 * */
function getDistrictSpecificQuestionDataForCG(districtId,teacherId,jobId)
{	
	//alert('getDistrictSpecificQuestionDataForCG');
	dId=districtId;
	tId=teacherId;
	CandidateGridAjaxNew.getDistrictSpecificQuestionDataForCG(districtId,teacherId,jobId,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{	
		  //	prompt('',data);
			$('#getDistrictSpecificQuestion').html(data);
		}
	});
}
//Show Qualification Details Div
function showQualificationDiv(jobForTeacherId,teacherId,districtId)
{
		document.getElementById("jftIdforPrint").value='';
		document.getElementById("jftIdforPrint").value=jobForTeacherId;
	    CandidateGridAjaxNew.getQualificationDetails(jobForTeacherId,teacherId,districtId,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
	    	//alert(data);
			$('#qualificationDiv').modal('show');
			$('#qualificationDivBody').html(data);
			try{
			if(document.getElementById("userType").value==2 || document.getElementById("userType").value==5)
			{
				if(document.getElementById("flagForDistrictSpecificQuestions").value=="null" || document.getElementById("flagForDistrictSpecificQuestions").value=="false")
				{
					if(document.getElementById("flagForFinalize").value=="null" || document.getElementById("flagForFinalize").value=="false")
					{
						$("#qStatusSave").show();
						$("#qStatusFinalize").show();
					}
					else
					{
						$("#qStatusSave").hide();
						$("#qStatusFinalize").hide();
					}
					
				}else if(document.getElementById("flagForDistrictSpecificQuestions").value=="true"){
					$("#qStatusSave").hide();
					$("#qStatusFinalize").hide();
				}
			}
			else
			{
				$("#qStatusSave").hide();
				$("#qStatusFinalize").hide();
			}
			}catch(err){}
		}
	});
}
function printQualificationIssuse()
{
	var jobForTeacherId = document.getElementById("jftIdforPrint").value;
	//alert(" jobForTeacherId :: "+jobForTeacherId );
	
	    $('#loadingDiv').fadeIn();
	    CandidateGridAjaxNew.getQualificationDetailsForPrint(jobForTeacherId,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#loadingDiv').hide();
	    	//alert(data);
			try
			{
			    	var newWindow = window.open();
			    	newWindow.document.write(data);			
					newWindow.print();							   
			}
			catch (e) 
			{							
				$('#printmessage1').modal('show');
				//$('#printmessage').modal('show');							 							 
			}
		}
	});
}
	// show qualification Div In TeacherPool
	function showQualificationDivForTP(teacherId,districtId,headQuarterId,branchId)
	{
		     document.getElementById("teacherIdforPrint").value='';
		     document.getElementById("districtIdforPrint").value='';
		     document.getElementById("headQuarterIdforPrint").value='';
		     document.getElementById("branchIdforPrint").value='';
	         document.getElementById("teacherIdforPrint").value=teacherId;
	         document.getElementById("districtIdforPrint").value=districtId;
	         document.getElementById("headQuarterIdforPrint").value=headQuarterId;
	         document.getElementById("branchIdforPrint").value=branchId;
		    CandidateGridAjaxNew.getQualificationDetailsForTP(teacherId,districtId,headQuarterId,branchId,{ 
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{
		    	$('#qualificationDiv').modal('show');
				$('#qualificationDivBody').html(data);
				try{
				if(document.getElementById("userType").value==2 || document.getElementById("userType").value==5)
				{
					if(document.getElementById("flagForDistrictSpecificQuestions").value=="null" || document.getElementById("flagForDistrictSpecificQuestions").value=="false")
					{
						if((document.getElementById("flagForFinalize").value=="null" || document.getElementById("flagForFinalize").value=="false") && document.getElementById("singleQQFlag").value=="0")
						{						
							$("#qStatusSave").show();
							$("#qStatusFinalize").show();
						}
						else
						{
							$("#qStatusSave").hide();
							$("#qStatusFinalize").hide();
						}
						
					}else if(document.getElementById("flagForDistrictSpecificQuestions").value=="true"){
						$("#qStatusSave").hide();
						$("#qStatusFinalize").hide();
					}
				}
				else
				{
					$("#qStatusSave").hide();
					$("#qStatusFinalize").hide();
				}
				}catch(err){}
			}
		});
	}
	function printQualificationDivForTP()
	{	
		
		var teacherId = document.getElementById("teacherIdforPrint").value;
		var districtId = document.getElementById("districtIdforPrint").value;
		var headQuarterId = document.getElementById("headQuarterIdforPrint").value;
		var branchId = document.getElementById("branchIdforPrint").value;
		 $('#loadingDiv').fadeIn();
		    CandidateGridAjaxNew.getQualificationDetailsForTPPrint(teacherId,districtId,headQuarterId,branchId,{ 
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{
		     	$('#loadingDiv').hide();
				try
				{
				    	var newWindow = window.open();
				    	newWindow.document.write(data);			
						newWindow.print();							   
				}
				catch (e) 
				{							
					$('#printmessage1').modal('show');
					//$('#printmessage').modal('show');	// kelly						 							 
				}
			}
		});
	}
	//save qualification Note
	function saveQualificationNote(saveStatus)
	{
		var teacherId = document.getElementById("teacherId").value;
		var jobId = document.getElementById("jobId").value;
		
		var note = $("#statusNoteID").val();
		$('#errorQStatus').empty();
		if(note==''){
			$('#errorQStatus').show();
			$('#errorQStatus').append("&#149; "+resourceJSON.PlzEtrNotes+".<br>");
		}else{
			CandidateGridAjaxNew.saveQualificationNoteForCG(teacherId,note,saveStatus,jobId,{ 
				async: true,
				errorHandler:handleError,
				callback:function(data)
				{
					$('#qualificationDiv').modal('hide');
					window.location.reload();
				}
			});
		}
	}
	
	// Save Qualification Note For Teacher Pool
	function saveQualificationNoteForTP(saveStatus)
	{
		var teacherId = document.getElementById("teacherId").value;
		var qQSetId = document.getElementById("qQSetId").value;
		//alert(teacherId);alert(qQSetId);
		var note = $("#statusNoteID").val();		
		$('#errorQStatus').empty();	
		if(note==''){
			$('#errorQStatus').show();
			$('#errorQStatus').append("&#149; "+resourceJSON.PlzEtrNotes+".<br>");
		}
		else
		{
			//alert(teacherId+"........"+districtId+"..........."+note+"...."+saveStatus);
			CandidateGridAjaxNew.saveQualificationNote(teacherId,note,saveStatus,qQSetId,{ 
				async: true,
				errorHandler:handleError,
				callback:function(data)
				{
				    //alert(data);
					$('#qualificationDiv').modal('hide');
					applyScrollOnJobsTbl();
					displayTeacherGrid();
				}
			});
		}
	}
	function cancelQualificationIssuse()
	{
		$('#errorQStatus').empty();
	}
	
	//For Print Data(QQ, Cover Letter, Portfolio)
	/*function showPrintDataDiv(jobForTeacherId,teacherId,districtId)
	{
		CandidateGridAjaxNew.getPrintData(jobForTeacherId,teacherId,districtId,{ 
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{
				try{
					$('#printDataProfile').modal('show');
					$('#printDataProfileDiv').html(data);
				}catch(err){}
			}
		});
	}*/
	
	
	// Get Qualification Questions for Print 
	/*function getDistrictSpecificQuestionDataForPrint(districtId,teacherId,count,flag)
	{	
		dId=districtId;
		tId=teacherId;
		if(flag==0)
		{
			CandidateGridAjaxNew.getDistrictSpecificQuestionData(districtId,teacherId,{ 
				async: true,
				errorHandler:handleError,
				callback:function(data)
				{	
					$('#getDistrictSpecificQuestionForPrint'+count).html(data);
				}
			});
		}
		else if(flag==1)
		{
			CandidateGridAjaxNew.getDistrictSpecificQuestionDataForPrint(districtId,teacherId,{ 
				async: true,
				errorHandler:handleError,
				callback:function(data)
				{	
					$('#getDistrictSpecificQuestionForPrint'+count).html(data);
				}
			});
		}
	}*/
	
	function canelPrintData(){
		$('#printDataProfile').modal('hide');
		$("#portfolioItmDiv").hide();
		var elements = document.getElementsByName("chkportfolioItem");
		 for (i=0;i<elements.length;i++) 
		    elements[i].checked = false;
		 
		//var siteURL=this.location.href;
		//window.open(siteURL, '_blank'); // <- This is what makes it open in a new window.);
	}
	
	// Print All Data FOR pdf
	function printAllData()
	{	
		//var dataNotFoundFlag = document.getElementById("dataNotFoundFlag").value;
		$('#loadingDiv').fadeIn();
		var inputs = document.getElementById("jftIds").value;
		var chkcl = document.getElementById("chkcl").value;
		var chkqq = document.getElementById("chkqq").value;
		var chkportfolio=document.getElementById("chkportfolio").value;	
		
		var chkjsi=document.getElementById("chkjsi").value;	
		
		
		var pointscheck = new Array();
			
			if (document.getElementById("academics").checked==true)
			{
		      pointscheck.push("acd");
			}	
			
			if (document.getElementById("certification").checked==true) 
			{	
				pointscheck.push("cert");
			}
			
			if (document.getElementById("references").checked==true) 
			{
				
				pointscheck.push("ref");
			}
			
			if (document.getElementById("experience").checked==true) 
			{
				pointscheck.push("exp");
			}
			
			if (document.getElementById("involvement").checked==true) 
			{
				
				pointscheck.push("invol");
			}
			if (document.getElementById("honors").checked==true) 
			{
				
				pointscheck.push("honors");
			}
			if (document.getElementById("pinfo").checked==true) 
			{
				pointscheck.push("pinfo");
			}
			if (document.getElementById("resume").checked==true) 
			{
				pointscheck.push("resume");
			}
			if (document.getElementById("dspqQues").checked==true) 
			{
				pointscheck.push("dspqQues");
			}
			/*if (document.getElementById("eeocdata").checked==true) 
			{
				pointscheck.push("eeocdata");
			}*/
		
		
		
		var chkForPrint = 1;		
			if(inputs!="")
			{
				    CandidateGridAjaxNew.getPrintMultipleData(inputs,chkForPrint,chkcl,chkqq,chkportfolio, chkjsi,pointscheck,true,{ 
					async: true,
					errorHandler:handleError,
					callback:function(data)
					{
				    	$('#loadingDiv').hide();
						try
						{
								window.open(data, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0');
						}
						catch (e) 
						{							
							$('#printmessage1').modal('show');							 
						}	
						
				    	/*$('#loadingDiv').hide();
						try
						{
								//window.open(data, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0');
								var newWindow = window.open();
						    	newWindow.document.write(data);			
								newWindow.print();	
						}
						catch (e) 
						{							
							$('#printmessage1').modal('show');							 
						}*/	
					}
				});
			}
	}	
	
	//mukesh
	function chkportfolioOPtions()
	{
		 var elements = document.getElementsByName("chkportfolioItem");
		// alert(document.getElementsByName("cqp")[3].checked)
		 if(document.getElementsByName("cqp")[3].checked==true)
		 {
			 $("#portfolioItmDiv").show();
			 for (i=0;i<elements.length;i++) 
			 {
				elements[i].checked = true;
				elements[i].disabled=false;
			 }
		 }
		 else{
			 $("#portfolioItmDiv").hide();
			 for (i=0;i<elements.length;i++) 
			 {
				elements[i].checked = true;
				elements[i].disabled=true;
			 }
		 }
		 	
	}


	// Print Multiple CG Data
	function printMultipleData()
	{
		 
		$('#errForPrint').empty();
		var savecandidatearray="";
		var chkForPrint = 0 ;
		
		var inputs = document.getElementsByName("cgCBX"); 
		 for (var i = 0; i < inputs.length; i++) {
		        if (inputs[i].type === 'checkbox') {
		        	if(inputs[i].checked){
		        		savecandidatearray+=	inputs[i].value+",";
		            }
		        }
		 } 
		// alert("array length=="+savecandidatearray.length)
		 var chkcl=false;
		 var chkqq=false;
		 var chkjsi=false;
		 var chkportfolio=false;
		 var chkforPortfolios=0;
		 
		 var chkacademics=false;
		 var chkcertification=false;
		 var chkreferences=false;
		 var chkexperience=false;
		 var chkinvolvement=false;
		 
		 var pointscheck = new Array();
		
		 var cqpinputs = document.getElementsByName("cqp"); 	
		 for (var i = 0; i < cqpinputs.length; i++) {
		        if (cqpinputs[i].type === 'checkbox') {
		        	if(cqpinputs[i].checked){		        		
		        		 if(cqpinputs[i].value=="chkcl")
		        			 chkcl=true;
		        		
		        		 if(cqpinputs[i].value=="chkqq")
		        			 chkqq=true;
		        		 
		        		 if(cqpinputs[i].value=="chkjsi")
		        			 chkjsi=true;
		        		
		        		 if(cqpinputs[i].value=="chkportfolio")
		        		 {
		        			chkportfolio=true;
		        			
		        			if (document.getElementById("academics").checked==true)
		        			{
		        				chkforPortfolios=chkforPortfolios+1
		        				pointscheck.push("acd");
		        			}	
		        			
	        				if (document.getElementById("certification").checked==true) 
	        				{	
		        				chkforPortfolios=chkforPortfolios+1
		        				pointscheck.push("cert");
	        				}
		        			
	        				if (document.getElementById("references").checked==true) 
	        				{
		        				chkforPortfolios=chkforPortfolios+1
		        				pointscheck.push("ref");
	        				}
		        			
	        				if (document.getElementById("experience").checked==true) 
	        				{
		        				chkforPortfolios=chkforPortfolios+1
		        				pointscheck.push("exp");
	        				}
		        			
	        				if (document.getElementById("involvement").checked==true) 
	        				{
		        				chkforPortfolios=chkforPortfolios+1
		        				pointscheck.push("invol");
	        				}
	        				if (document.getElementById("honors").checked==true) 
	        				{
		        				chkforPortfolios=chkforPortfolios+1
		        				pointscheck.push("honors");
	        				}
	        				if (document.getElementById("pinfo").checked==true) 
	        				{
		        				chkforPortfolios=chkforPortfolios+1
		        				pointscheck.push("pinfo");
	        				}
	        				if (document.getElementById("resume").checked==true) 
	        				{
		        				chkforPortfolios=chkforPortfolios+1
		        				pointscheck.push("resume");
	        				}
	        				if (document.getElementById("dspqQues").checked==true) 
	        				{
		        				chkforPortfolios=chkforPortfolios+1
		        				pointscheck.push("dspqQues");
	        				}
	        				/*if (document.getElementById("eeocdata").checked==true) 
	        				{
		        				chkforPortfolios=chkforPortfolios+1
		        				pointscheck.push("eeocdata");
	        				}*/
						      
		        		 }
		        		
		            }
		        }
		 } 	
		 if(chkforPortfolios==0 && chkportfolio==true)
		 {
			 $('#errForPrint').show();
			 $('#errForPrint').append("&#149; "+resourceJSON.PlzSelectPortfolio+"<br>");
			 return false;
		 }
		 if(chkcl==false && chkqq==false && chkportfolio==false && chkjsi==false)
		 {
			 $('#errForPrint').show();
			 $('#errForPrint').append("&#149; "+resourceJSON.PlzSelectOneOption+"<br>");
		 }
		 else
		 {
			 var url = window.location.pathname;
			 if(url.indexOf("candidategrid.do") > -1){
			 $('#loadingDiv').fadeIn();
			 CandidateGridAjaxNew.getPrintMultipleData(savecandidatearray,chkForPrint,chkcl,chkqq,chkportfolio, chkjsi, pointscheck,false,{ 
					async: true,
					errorHandler:handleError,
					callback:function(data)
					{
				     $('#loadingDiv').hide();				 
						try{
							$('#selectedprintData').modal('hide');
							$('#printDataProfile').modal('show');
							
							for (var i = 0; i < cqpinputs.length; i++) {						      
						        	if(cqpinputs[i].checked==true){		        		
						        		cqpinputs[i].checked=false; 
						            }						      
						    } 							
							$('#printDataProfileDiv').html(data);
							 if($("#districtId").val()==1201470){
						    	 $("#printDataProfile .modal-dialog").width(1040);
						    	 $("#tblTeacherAcademics_Profile").width(950);
						    	 $("#tblTeacherCertificates_Profile").width(950);
						    	 $("#tbleleReferences_Profile").width(950);
						    	 $("#tblWorkExp_Profile").width(950);
						    	 $("#involvementGrid").width(950);
						     }
						}catch(err){}
						
					}
				});
			 }
			 else{
				 $('#loadingDiv').fadeIn();
				 CandidateGridAjaxNew.getPrintMultipleData_Opp(savecandidatearray,chkForPrint,chkcl,chkqq,chkportfolio, chkjsi, pointscheck,false,{ 
						async: true,
						errorHandler:handleError,
						callback:function(data)
						{
					     $('#loadingDiv').hide();				 
							try{
								$('#selectedprintData').modal('hide');
								$('#printDataProfile').modal('show');
								
								for (var i = 0; i < cqpinputs.length; i++) {						      
							        	if(cqpinputs[i].checked==true){		        		
							        		cqpinputs[i].checked=false; 
							            }						      
							    } 							
								$('#printDataProfileDiv').html(data);
								 if($("#districtId").val()==1201470){
							    	 $("#printDataProfile .modal-dialog").width(1040);
							    	 $("#tblTeacherAcademics_Profile").width(950);
							    	 $("#tblTeacherCertificates_Profile").width(950);
							    	 $("#tbleleReferences_Profile").width(950);
							    	 $("#tblWorkExp_Profile").width(950);
							    	 $("#involvementGrid").width(950);
							     }
							}catch(err){}
							
						}
					});
			 }
			 
			 
		 }
	}
	
	function showSelectedDataForPrint()
	{
		$('#errForPrint').empty();
		$('#errForPrint').hide();
		$('#selectedprintData').modal('show');
          var elements = document.getElementsByName("chkportfolioItem");
			 for (i=0;i<elements.length;i++) 
			 {
				elements[i].checked = true;
				elements[i].disabled=true;
			 }
		
		//$('#selectedprintDiv').html("data selection div for print");
	}
	
	function cancelMultiPrint()
	{
		$('#errForPrint').empty();
		$('#errForPrint').hide();
		$("#portfolioItmDiv").hide();
		 var cqpinputs = document.getElementsByName("cqp"); 	
		for (var i = 0; i < cqpinputs.length; i++) {						      
        	if(cqpinputs[i].checked==true){		        		
        		cqpinputs[i].checked=false; 
            }						      
        } 	
		$('#selectedprintData').modal('hide');
		
		var elements = document.getElementsByName("chkportfolioItem");
			 for (i=0;i<elements.length;i++) 
			   elements[i].checked = false;
		
	}
	
	
/* @End
 * @Ashish Kumar
 * @Description :: Show Qualification details Div and save Qualification Note
 * */
function downloadAttachInstructionFileName(filePath,fileName,secondaryStatusName)
{
	$('#loadingDiv').show();
	ManageStatusAjax.downloadAttachInstructionFile(filePath,fileName,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			$('#loadingDiv').hide();
			if(data!='')
			{
				if (data.indexOf(".doc") !=-1) 
				{
					try{$('#modalDownloadIAFN').modal('hide');}catch(err){}
				    document.getElementById('ifrmAttachInstructionFileName').src = ""+data+"";
				}
				else
				{
					document.getElementById('ifrmAttachInstructionFileName').src = ""+data+"";
					try{
						$( "#modalDownloadIAFN" ).draggable({
							handle:'#modalDownloadIAFNMove'
						});
						$('#modalDownloadIAFN').modal('hide');
						$('#myModalLabelInstructionHeader').html(""+resourceJSON.MsgInstruction+" "+secondaryStatusName);
						$('#modalDownloadIAFN').modal('show');
					}catch(err){}
				}
			}
		}
	});
}
function downloadCertificationforCG(certId, linkId,teaId)
{		
	    $("#myModalLabelText").html(resourceJSON.MsgCertification);	
	    TeacherProfileViewInDivAjax.downloadCertificationforCG(certId,teaId,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data){
			if(data=="")
			{
				data="javascript:void(0)";
			}
			else
			{
				if(deviceType)
				{
					if (data.indexOf(".doc")!=-1)
					{
						$("#docfileNotOpen").css({"z-index":"3000"});
				    	$('#docfileNotOpen').show();
					}
					else if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
					{
						$("#exelfileNotOpen").css({"z-index":"3000"});
				    	$('#exelfileNotOpen').show();
					}
					else
					{
						document.getElementById(linkId).href = data;
					}
				}
				else if(deviceTypeAndroid)
				{
					if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
					{
						document.getElementById("ifrmCert").src=data;
					}
					else
					{
						document.getElementById(linkId).href = data;	
					}	
				}
				else if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					document.getElementById("ifrmCert").src=data;
				}
				else
				{
					try{
						document.getElementById('ifrmTransCommon').src = ""+data+"";
						$('#modalDownloadsCommon').modal('show');
					}catch(err)
					  {}
				}			
			}	
			return false;
		}});
}



function getPosition(element) {
    var xPosition = 0;
    var yPosition = 0;
  
    while(element) {
        xPosition += (element.offsetLeft - element.scrollLeft + element.clientLeft);
        yPosition += (element.offsetTop - element.scrollTop + element.clientTop);
        element = element.offsetParent;
    }
    return { x: xPosition, y: yPosition };
}

//changed: by rajendra
function downloadReferenceForCandidate(referenceId,teacherId,linkId)
{	
	$("#myModalLabelText").html(resourceJSON.MsgReference);
	//PFCertifications
		CGServiceAjax.downloadReferenceForCandidate(referenceId,teacherId,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
		if(data=="")
		{
			data="javascript:void(0)";
		}
		else
		{
			if(deviceType)
			{
				if (data.indexOf(".doc")!=-1)
				{
					$("#docfileNotOpen").css({"z-index":"3000"});
			    	$('#docfileNotOpen').show();
				}
				else if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					$("#exelfileNotOpen").css({"z-index":"3000"});
			    	$('#exelfileNotOpen').show();
				}
				else
				{
					document.getElementById(linkId).href = data;
				}
			}
			else if(deviceTypeAndroid)
			{
				if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					document.getElementById("ifrmRef").src=data;
				}
				else
				{
					document.getElementById(linkId).href = data;
				}	
			}
			else if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
			{
				document.getElementById("ifrmRef").src=data;
			}
			else
			{
				try{
					document.getElementById('ifrmTransCommon').src = ""+data+"";
					$('#modalDownloadsCommon').modal('show');								
				}catch(err)
				  {
					alert(err)
				  }
			}			
		}	
		return false;
		}});
}
function downloadUploadedDocument(additionDocumentId,linkId,teacherid)
{		
	    $("#myModalLabelText").html(resourceJSON.MsgDocument);
	    //PFCertifications
	    CGServiceAjax.downloadUploadedDocumentForCg(additionDocumentId,teacherid,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
			if(data=="")
			{
				data="javascript:void(0)";
			}
			else
			{
				if(deviceType)
				{
					if (data.indexOf(".doc")!=-1)
					{
						$("#docfileNotOpen").css({"z-index":"3000"});
				    	$('#docfileNotOpen').show();
					}
					else if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
					{
						$("#exelfileNotOpen").css({"z-index":"3000"});
				    	$('#exelfileNotOpen').show();
					}
					else
					{
						document.getElementById(linkId).href = data;
					}
				}
				else if(deviceTypeAndroid)
				{
					if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
					{
						document.getElementById("ifrmRef").src=data;
					}
					else
					{
						document.getElementById(linkId).href = data;
					}	
				}
				else if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					document.getElementById("ifrmRef").src=data;
				}
				else
				{
					try{
						document.getElementById('ifrmTransCommon').src = ""+data+"";
						$('#modalDownloadsCommon').modal('show');								
					}catch(err)
					  {
						alert(err)
					  }
				}			
			}	
			return false;			
		}});
}


function viewCompleteTeacherStatusNotesClose(){
	try
	{
		$('#myModalViewCompleteTeacherStatusNotes').modal('hide');
		$("#jWTeacherStatusNotesDiv").css({"z-index":"3000"});
		$('#jWTeacherStatusNotesDiv').modal('show');
	}catch(err){}
}

function viewCompleteTeacherStatusNotes(teacherStatusNoteId){
	try
	{
		CandidateGridAjaxNew.viewCompleteTeacherStatusNotes(teacherStatusNoteId,{ 
			async: false,
			errorHandler:handleError,
			callback:function(data)
			{	if(data!="")
				{
				    $("#jWTeacherStatusNotesDiv").css({"z-index":"100"});
					$('#jWTeacherStatusNotesDiv').modal('hide');					
					document.getElementById("myModalViewCompleteTeacherStatusNotesInnerText").innerHTML=data;
					$('#myModalViewCompleteTeacherStatusNotes').modal('show');
				}
			}});
	}catch(err){}	
}

function notExternal(externalFlag){	
var jobId=document.getElementById("jobId").value;
var savecandidatearray=""; 
var inputs = document.getElementsByName("cgCBX"); 
	 for (var i = 0; i < inputs.length; i++) {
	        if (inputs[i].type === 'checkbox') {
	        	if(inputs[i].checked){
	        		savecandidatearray+=	inputs[i].value+",";
	            }
	        }
	 } 	
	 CandidateGridAjaxNew.notExternal(jobId,savecandidatearray,externalFlag,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#loadingDiv').hide();			
			if(data==0){
				document.getElementById("internalTxt").innerHTML=""+resourceJSON.PlzSelectExtCand+"" ;
			}else if(data==1){
				document.getElementById("internalTxt").innerHTML=""+resourceJSON.MsgOneOrMoreCandidateSelected+" <a href='#' onclick='showInternalTrans()'>"+resourceJSON.BtnClick+"</a> "+resourceJSON.MsgDetailOfInternalTransferCandidate+"";		
			}else if(data==2){
				document.getElementById("internalTxt").innerHTML=""+resourceJSON.MsgRegisteredAsInternalTransfer+" <a href='#' onclick='showInternalTrans()'>"+resourceJSON.BtnClick+"</a> "+resourceJSON.MsgDetailOfInternalTransferCandidate+"";	
			}else if(data==3){
				//document.getElementById("internalTxt").innerHTML="EPI invites are successfully sent to the selected candidates.";	
				document.getElementById("internalTxt").innerHTML=""+resourceJSON.MsgEPIInviteSuccessfully+"";
			}else if(data==4){
				//document.getElementById("internalTxt").innerHTML="EPI invites are successfully sent to the selected candidates. One or more Candidate(s) have already completed the EPI. Email for EPI invite is not sent to such candidate(s).";	
				document.getElementById("internalTxt").innerHTML=""+resourceJSON.MsgEPIInviteSuccessfully+"";
			}else{
				document.getElementById("internalTxt").innerHTML=""+resourceJSON.MsgStatusofExternalCandidate+"";
				for (var i = 0; i < inputs.length; i++) {
			        if (inputs[i].type === 'checkbox') {
			        	if(inputs[i].checked){
			        		inputs[i].checked=false;
			            }
			        }
			   } 
			}
			$('#internalDiv').modal('show');
		}});
	
}

function notInternal(interFlag){
	$('#loadingDiv').show();
	var jobId=document.getElementById("jobId").value;
	var savecandidatearray="";
	 
	var inputs = document.getElementsByName("cgCBX"); 
	 for (var i = 0; i < inputs.length; i++) {
	        if (inputs[i].type === 'checkbox') {
	        	if(inputs[i].checked){
	        		savecandidatearray+=	inputs[i].value+",";
	            }
	        }
	 } 
 	CandidateGridAjaxNew.notInternal(jobId,savecandidatearray,interFlag,{ 
	async: true,
	errorHandler:handleError,
	callback:function(data)
	{
		$('#loadingDiv').hide();
		if(data==0){
			document.getElementById("internalTxt").innerHTML=""+resourceJSON.PlzSelectInternalCandidates+"";
		}else if(data==1){
			document.getElementById("internalTxt").innerHTML=""+resourceJSON.MsgOneOrMoreCandidateSelected+" <a href='#' onclick='showInternalTrans()'>"+resourceJSON.BtnClick+"</a> "+resourceJSON.MsgDetailOfInternalTransferCandidate+"";	
		}else if(data==2){
			document.getElementById("internalTxt").innerHTML=""+resourceJSON.MsgRegisteredAsInternalTransfer+" <a href='#' onclick='showInternalTrans()'>"+resourceJSON.BtnClick+"</a> "+resourceJSON.MsgDetailOfInternalTransferCandidate+"";	
		}else if(data==3){
			//document.getElementById("internalTxt").innerHTML="EPI invites are successfully sent to the selected candidates.";	
			document.getElementById("internalTxt").innerHTML=""+resourceJSON.MsgEPIInviteSuccessfully+"";
		}else if(data==4){
			//document.getElementById("internalTxt").innerHTML="EPI invites are successfully sent to the selected candidates. One or more Candidate(s) have already completed the EPI. Email for EPI invite is not sent to such candidate(s).";	
			document.getElementById("internalTxt").innerHTML=""+resourceJSON.MsgEPIInviteWereSuccessfully+"";
		}else{
			document.getElementById("internalTxt").innerHTML=""+resourceJSON.MsgStatusofInternalCandidate+"";
			for (var i = 0; i < inputs.length; i++) {
		        if (inputs[i].type === 'checkbox') {
		        	if(inputs[i].checked){
		        		inputs[i].checked=false;
		            }
		        }
		   } 
		}
		$('#internalDiv').modal('show');
	}});
}
function backInternal(){
	getCandidateGrid();
	refreshStatus();
}
function showInternalTrans(){
	var jobId=document.getElementById("jobId").value;
	var savecandidatearray="";
	 
	var inputs = document.getElementsByName("cgCBX"); 
	 for (var i = 0; i < inputs.length; i++) {
	        if (inputs[i].type === 'checkbox') {
	        	if(inputs[i].checked){
	        		savecandidatearray+=	inputs[i].value+",";
	            }
	        }
	 } 
	CandidateGridAjaxNew.displayInternalTrans(jobId,savecandidatearray,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#loadingDiv').hide();
			document.getElementById("internalTxt").innerHTML=data;
			$('#internalDiv').modal('show');
			applyScrollOnInterTrans();
		}});
}

function getInternalScoreDetails()
{
	var expandchk=document.getElementById("expandchk").value;
	if(expandchk >= 1)
	{
		$( ".tdscore0_internal:first" ).animate({
		    left :0,
			 width:256
		  }, {
		    duration: 1000
		  });
		
		try { 
			document.getElementById("tdscore0tbl_internal").style.textIndent="0px";
		} catch (e) {
			// TODO: handle exception
		}
		for(var i=1;i<=sectionRecordInter;i++)
		{
			try {
				document.getElementById("tdscore_internal"+i).style.display="inline";
			} catch (e) {
				// TODO: handle exception
			}
			
		}
	}
	else
	{

		 $( ".tdscore0_internal:first" ).animate({
			    left :0,
				 width:1
			  }, {
			    duration: 1000
			  });
		 
		 try {
			 	document.getElementById("tdscore0tbl_internal").style.textIndent="-99999px";
		 } catch (e) {
				// TODO: handle exception
			}
		 
		for(var i=1;i<=sectionRecordInter;i++)
		{
			try {
				document.getElementById("tdscore_internal"+i).style.display="none";
			} catch (e) {
				// TODO: handle exception
			}
		}
	}
}


function getIncompleteScoreDetails()
{
	var expandchk=document.getElementById("expandchk").value;
	if(expandchk >= 1)
	{
		$( ".tdscore0_incomplete:first" ).animate({
		    left :0,
			 width:256
		  }, {
		    duration: 1000
		  });
		 setTimeout(function(){
				jQuery("#tblGrid .incompleteCandidate").addClass("bg");
				$("#tdscore0tbl_incomplete").show();
					
	    },1001);
		
		try {
		document.getElementById("tdscore0tbl_incomplete").style.textIndent="0px";
		} catch (e) {
			// TODO: handle exception
		}
		for(var i=1;i<=sectionRecordInComp;i++)
		{
			try {
				document.getElementById("tdscore_incomplete"+i).style.display="inline";
			} catch (e) {
				// TODO: handle exception
			}
			
		}
	}
	else
	{
		 $( ".tdscore0_incomplete:first" ).animate({
			    left :0,
				 width:1
			  }, {
			    duration: 1000
			  });
		 
		 setTimeout(function(){
				jQuery("#tblGrid .incompleteCandidate").removeClass("bg");
				$("#tdscore0tbl_incomplete").hide();
					
	    },500);
		
		 try {
			 document.getElementById("tdscore0tbl_incomplete").style.textIndent="-99999px";
		 } catch (e) {
				// TODO: handle exception
			}
		for(var i=1;i<=sectionRecordInComp;i++)
		{
			try {
				document.getElementById("tdscore_incomplete"+i).style.display="none";
			} catch (e) {
				// TODO: handle exception
			}
			
			
		}
	}
}

function getRejectScoreDetails()
{
	var expandchk=document.getElementById("expandchk").value;
	if(expandchk >= 1)
	{
		$( ".tdscore0_reject:first" ).animate({
		    left :0,
			 width:256
		  }, {
		    duration: 1000
		  });
		
		 setTimeout(function(){
				jQuery("#tblGrid .removeCandidate").addClass("bg");
				$("#tdscore0tbl_reject").show();
					
			  },1001);
		 
		try {
		document.getElementById("tdscore0tbl_reject").style.textIndent="0px";
		} catch (e) {
			// TODO: handle exception
		}
		for(var i=1;i<=sectionRecordRemove;i++)
		{
			try {
				document.getElementById("tdscore_reject"+i).style.display="inline";
			} catch (e) {
				// TODO: handle exception
			}
			
		}
	}
	else
	{
		 $( ".tdscore0_reject:first" ).animate({
			    left :0,
				 width:1
			  }, {
			    duration: 1000
			  });
		 	setTimeout(function(){
				jQuery("#tblGrid .removeCandidate").removeClass("bg");
				$("#tdscore0tbl_reject").hide();
					
			  },500);
		
		 try {
			 document.getElementById("tdscore0tbl_reject").style.textIndent="-99999px";
		 } catch (e) {
				// TODO: handle exception
			}
		for(var i=1;i<=sectionRecordRemove;i++)
		{
			try {
				document.getElementById("tdscore_reject"+i).style.display="none";
			} catch (e) {
				// TODO: handle exception
			}
			
			
		}
	}
}


function getWithdrawnScoreDetails()
{
	var expandchk=document.getElementById("expandchk").value;
	if(expandchk >= 1)
	{
		$( ".tdscore0_withdrawn:first" ).animate({
		    left :0,
			 width:256
		  }, {
		    duration: 1000
		  });
		
		
		 setTimeout(function(){
				jQuery("#tblGrid .withdrawnCandidate").addClass("bg");
				$("#tdscore0tbl_withdrawn").show();
					
			  },1001);
		 
		 
		try {
		document.getElementById("tdscore0tbl_withdrawn").style.textIndent="0px";
		} catch (e) {
			// TODO: handle exception
		}
		for(var i=1;i<=sectionRecordWithDr;i++)
		{
			try {
				document.getElementById("tdscore_withdrawn"+i).style.display="inline";
			} catch (e) {
				// TODO: handle exception
			}
			
		}
	}
	else
	{
		 $( ".tdscore0_withdrawn:first" ).animate({
			    left :0,
				 width:1
			  }, {
			    duration: 1000
			  });
		 
		 setTimeout(function(){
				jQuery("#tblGrid .withdrawnCandidate").removeClass("bg");
				$("#tdscore0tbl_withdrawn").hide();
					
			  },500);
		
		 try {
			 document.getElementById("tdscore0tbl_withdrawn").style.textIndent="-99999px";
		 } catch (e) {
				// TODO: handle exception
			}
		for(var i=1;i<=sectionRecordWithDr;i++)
		{
			try {
				document.getElementById("tdscore_withdrawn"+i).style.display="none";
			} catch (e) {
				// TODO: handle exception
			}
			
			
		}
	}
}
function getHiredScoreDetails()
{
	var expandchk=document.getElementById("expandchk").value;
	if(expandchk >= 1)
	{
		$( ".tdscore0_hired:first" ).animate({
		    left :0,
			 width:256
		  }, {
		    duration: 1000
		  });
		
		 setTimeout(function(){
				jQuery("#tblGrid .hiredCandidate").addClass("bg");
				$("#tdscore0_hired").show();
					
			  },1001);
		 
		try {
		document.getElementById("tdscore0tbl_hired").style.textIndent="0px";
		} catch (e) {
			// TODO: handle exception
		}
		for(var i=1;i<=sectionRecordHired;i++)
		{
			try {
				document.getElementById("tdscore_hired"+i).style.display="inline";
			} catch (e) {
				// TODO: handle exception
			}
			
		}
	}
	else
	{
		 $( ".tdscore0_hired:first" ).animate({
			    left :0,
				 width:1
			  }, {
			    duration: 1000
			  });
		 
		 setTimeout(function(){
				jQuery("#tblGrid .hiredCandidate").removeClass("bg");
				$("#tdscore0_hired").hide();
				jQuery(".hiredCandidate th:last").attr("colspan","2");	
			  },500);
		
		 try {
			 document.getElementById("tdscore0tbl_hired").style.textIndent="-99999px";
		 } catch (e) {
				// TODO: handle exception
			}
		for(var i=1;i<=sectionRecordHired;i++)
		{
			try {
				document.getElementById("tdscore_hired"+i).style.display="none";
			} catch (e) {
				// TODO: handle exception
			}
		}
	}
}

function getTimeoutScoreDetails()
{
	var expandchk=document.getElementById("expandchk").value;
	if(expandchk >= 1)
	{
		$( ".tdscore0_timeout:first" ).animate({
		    left :0,
			 width:256
		  }, {
		    duration: 1000
		  });
		
	
		 setTimeout(function(){
				jQuery("#tblGrid .timeoutCandidate").addClass("bg");
				$("#tdscore0tbl_timeout").show();
					
			  },1001);
		
		try {
		document.getElementById("tdscore0tbl_timeout").style.textIndent="0px";
		} catch (e) {
			// TODO: handle exception
		}
		for(var i=1;i<=sectionRecordVlt;i++)
		{
			try {
				document.getElementById("tdscore_timeout"+i).style.display="inline";
			} catch (e) {
				// TODO: handle exception
			}
			
		}
	}
	else
	{
		 $( ".tdscore0_timeout:first" ).animate({
			    left :0,
				 width:1
			  }, {
			    duration: 1000
			  });
		 
		 setTimeout(function(){
				jQuery("#tblGrid .timeoutCandidate").removeClass("bg");
				$("#tdscore0tbl_timeout").hide();
					
			  },500);
		
		 try {
			 document.getElementById("tdscore0tbl_timeout").style.textIndent="-99999px";
		 } catch (e) {
				// TODO: handle exception
			}
		for(var i=1;i<=sectionRecordVlt;i++)
		{
			try {
				document.getElementById("tdscore_timeout"+i).style.display="none";
			} catch (e) {
				// TODO: handle exception
			}
		}
	}
}


function sendEpiInVites(){
	$('#loadingDiv').show();
	var jobId=document.getElementById("jobId").value;
	var savecandidatearray="";
	 
	var inputs = document.getElementsByName("cgCBX");
	var comp=0;
	 for (var i = 0; i < inputs.length; i++) {
	        if (inputs[i].type === 'checkbox') {
	        	if(inputs[i].checked){
	        		//if(inputs[i].getAttribute("jsi")=="noJsi"){	        		
	        			savecandidatearray+=	inputs[i].value+",";
	        		/*}else if(inputs[i].getAttribute("jsi")=="comp"){
	        			comp++;
	        		}*/
	            }
	        }
	 } 
	 
	 	CandidateGridAjaxNew.sendEpiInVites(jobId,savecandidatearray,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#loadingDiv').hide();
			if(data==1){
				document.getElementById("internalTxt").innerHTML=""+resourceJSON.MsgEPIInviteCouldNotBeSent+":<p style='margin-left:18px;'>1. "+resourceJSON.MsgCompleteOrTimedOut+"<br>2. "+resourceJSON.OneOrMoreSelectedCandidates+"</p>"+resourceJSON.OtherSelectedCandidate+"";
			}else{
				document.getElementById("internalTxt").innerHTML="<p>"+resourceJSON.MsgFoundThatEPIInviteCouldNotBeSent+".</p>"+resourceJSON.OtherSelectedCandidate+"";
			}
			/*if(data==0){
				document.getElementById("internalTxt").innerHTML="Please select external Candidates only from accordion \"External Candidates\".";
			}
			else if(data==1){
				document.getElementById("internalTxt").innerHTML="We have successfully sent the JSI invite to all those Candidates who have not taken the JSI for this job. We found that one or more Candidates have already taken the JSI for this job. JSI invite is not sent to such Candidates.";
			}else if(data==0){
				document.getElementById("internalTxt").innerHTML="JSI Invites were sent successfully to the selected candidates..";
			}			
			*/
			$('#internalDiv').find('.modal-dialog').css({width:'67%'});
			$('#internalDiv').modal('show');
		}});
	 
}

function sendJsiInVites(){
	$('#loadingDiv').show();
	var jobId=document.getElementById("jobId").value;
	var savecandidatearray="";
	 
	var inputs = document.getElementsByName("cgCBX");
	var comp=0;
	 for (var i = 0; i < inputs.length; i++) {
	        if (inputs[i].type === 'checkbox') {
	        	if(inputs[i].checked){
	        		//if(inputs[i].getAttribute("jsi")=="noJsi"){	        		
	        			savecandidatearray+=	inputs[i].value+",";
	        		/*}else if(inputs[i].getAttribute("jsi")=="comp"){
	        			comp++;
	        		}*/
	            }
	        }
	 } 
	 
	 	CandidateGridAjaxNew.sendJsiInVites(jobId,savecandidatearray,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#loadingDiv').hide();
			if(data==1){
				document.getElementById("internalTxt").innerHTML=""+resourceJSON.MsgJSIInviteCouldNotBeSent+":<p style='margin-left:18px;'>1. "+resourceJSON.AlreadyTakenJSIForThisJob+"<br>2. "+resourceJSON.OneOrMoreSelectedCandidates+"</p>"+resourceJSON.MsgJSIInviteSuccessfullySent+"";
			}else if(data==3){
				document.getElementById("internalTxt").innerHTML=""+resourceJSON.JSISentSuccessfully+"";
			}else{
				document.getElementById("internalTxt").innerHTML="<p>"+resourceJSON.MsgCompleteJobForJSI+"</p>"+resourceJSON.MsgJSIInviteSuccessfullySent+"";
			}
			/*if(data==0){
				document.getElementById("internalTxt").innerHTML="Please select external Candidates only from accordion \"External Candidates\".";
			}
			else if(data==1){
				document.getElementById("internalTxt").innerHTML="We have successfully sent the JSI invite to all those Candidates who have not taken the JSI for this job. We found that one or more Candidates have already taken the JSI for this job. JSI invite is not sent to such Candidates.";
			}else if(data==0){
				document.getElementById("internalTxt").innerHTML="JSI Invites were sent successfully to the selected candidates..";
			}			
			*/
			$('#internalDiv').find('.modal-dialog').css({width:'67%'});
			$('#internalDiv').modal('show');
		}});
	 
}


/*========================== TFA  Edit By Specific users ==========================================*/
function editTFAbyUser(teacherId)
{
	$("#draggableDivMaster").show();
	$('#edittfa').hide();
	$('#savetfa').show();
	$('#canceltfa').show();
	document.getElementById("tfaList").disabled=false;
}

function saveTFAbyUser()
{
	$("#tfaMsgShow").hide();
	
	var tfa = document.getElementById("tfaList").value;
	var teacherId = document.getElementById("teacherDetailId").value;
	//TeacherInfotAjax
	CGServiceAjax.updateTFAByUser(teacherId,tfa,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
			var tfalist = document.getElementById("tfaList");
			if(data!=null)
			{
				for(var i=0; i<tfalist.options.length;i++)
				{	
					if(tfalist.options[i].value==data)
					{
						tfalist.options[i].selected = true;
					}
				}
			}
			else
			{
				tfalist.selectedIndex = 1;
			}
			
			$("#draggableDivMaster").show();
			$('#edittfa').show();
			$('#savetfa').hide();
			$('#canceltfa').hide();
			document.getElementById("tfaList").disabled=true;
		}
	});
}

function cancelTFAbyUser(teacherId)
{
		$("#draggableDivMaster").show();
		$('#edittfa').show();
		$('#savetfa').hide();
		$('#canceltfa').hide();
		document.getElementById("tfaList").disabled=true;
		var tfalist = document.getElementById("tfaList");
		//TeacherInfotAjax
		CGServiceAjax.displayselectedTFA(teacherId,{ 
			async: false,
			errorHandler:handleError,
			callback:function(data)
			{
				for(var i=0; i<tfalist.options.length;i++)
				{	
					if(tfalist.options[i].value==data)
					{
						tfalist.options[i].selected = true;
					}
				}
			}
		});
}

function displayTFAMessageBox(teacherId)
{
	document.getElementById("teacherDetailId").value=teacherId;	
	$("#tfaMsgShow").show();
}

function closeTFAMsgBox()
{
	$("#tfaMsgShow").hide();
}

//------------------Rahul:GetDocument---14/11/2014---------------------

function getDocuments()
{
	
	
	CandidateGridAjaxNew.getDocumentsByDistrictOrSchoolId(
			{
				async: true,
				errorHandler:handleError,
				callback:function(data)
				{	
				
					if(data!="" || data.length>0)
					{
						//$("#templateDiv").fadeIn();
						//$("#btnChangetemplatelink").hide();
						$("#districtDocument").html(data);
					}else
					{
						$("#documentdiv").hide();
					}
				}
			});	

	
}

function showDocumentLink()
{
var docname=document.getElementById("districtDocument").value;	

if(docname.length>1)
{
	$('#viewDoc').show();
	
}

else
{
	$('#viewDoc').hide();	
}

}




function vieDistrictFile() {
	
	var districtattachment=document.getElementById("districtDocument").value;
		
	CandidateGridAjaxNew.vieDistrictFile(districtattachment,{
	async: true,
	errorHandler:handleError,
	callback:function(data)
	{
		if(deviceType)
		{
			
			if (data.indexOf(".doc")!=-1)
			{
				$("#docfileNotOpen").css({"z-index":"3000"});
		    	$('#docfileNotOpen').show();
			}
			else if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
			{
				$("#exelfileNotOpen").css({"z-index":"3000"});
		    	$('#exelfileNotOpen').show();
			}
			else
			{
				document.getElementById(linkid).href = data;
			}
		}
		else if(deviceTypeAndroid)
		{
		
			if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
			{
				// $('#distAttachmentDiv').modal('hide');
				 document.getElementById('ifrmAttachment').src = ""+data+"";
			}
			else
			{
				document.getElementById(linkid).href = data;
			}
		}
		else if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
		{
			 $('#distAttachmentShowDiv').modal('hide');
			 document.getElementById('ifrmAttachment').src = ""+data+"";
		} else {
			//alert(data)
			$('#myModalMessage').modal('hide');
				$('.distAttachmentShowDiv').modal('show');
				document.getElementById('ifrmAttachment').src = ""+data+"";
				return false;
		}
	}});

	}


function showMessageDiv1()
{
	$('#myModalMessage').modal('show');
	$('.distAttachmentShowDiv').modal('hide');
}



function saveDistrictSpecificQues(teacherId){	
	$('#errordivspecificquestion').empty();
	var arr =[];
	var jobOrder = {jobId:document.getElementById("jobId").value};
	var dspqType=document.getElementById("dspqType").value;
	var isRequiredCount=0;
	var ansRequiredCount=0;

	var totalQuestionsList=document.getElementById("totalQuestionsList").value;

	for(i=1;i<=totalQuestionsList;i++)
	{   
		var isRequiredAns=0;
		var isRequired = dwr.util.getValue("QS"+i+"isRequired");
		if(isRequired==1){
			isRequiredCount++;
		}
		var districtSpecificQuestion = {questionId:dwr.util.getValue("QS"+i+"questionId")};
		var questionTypeShortName = dwr.util.getValue("QS"+i+"questionTypeShortName");
		var questionTypeMaster = {questionTypeId:dwr.util.getValue("QS"+i+"questionTypeId")};
		var qType = dwr.util.getValue("QS"+i+"questionTypeShortName");
		var o_maxMarks = dwr.util.getValue("o_maxMarksS");
		
		var scroreVal=$('#dstQuesSlid'+i).val();		
			if ($("#ifrmQ"+i).length > 0) {
				var iframeNorm = document.getElementById("ifrmQ"+i);
				var innerNorm = iframeNorm.contentDocument || iframeNorm.contentWindow.document;
				if(innerNorm.getElementById('sliderQ'+i).value!='')
				$('#dstQuesSlid'+i).val(innerNorm.getElementById('sliderQ'+i).value);
				scroreVal=innerNorm.getElementById('sliderQ'+i).value;		
			}
		
		if(qType=='tf' || qType=='slsel' ||  qType=='slsel')
		{
			var optId="";
			var errorFlag=1;
			if($("input[name=QS"+i+"opt]:radio:checked").length > 0 )
			{
				 errorFlag=0;
				optId=$("input[name=QS"+i+"opt]:radio:checked").val();
			}else if(isRequired==1){
				$("#errordivspecificquestion").html("&#149; "+resourceJSON.PlzProvideResponseToAllQues+".<br>");
				dSPQuestionsErrorCount=1;
				errorFlag=1;
			}
			if(errorFlag==0){
				if(isRequired==1){
					isRequiredAns=1;
				}
				arr.push({ 
					"selectedOptions"  : optId,
					"question"  : dwr.util.getValue("QS"+i+"question"),
					"questionTypeMaster" : questionTypeMaster,
					"questionType" : questionTypeShortName,
					"questionOption" : dwr.util.getValue("qOptS"+optId),
					"districtSpecificPortfolioQuestions" : districtSpecificQuestion,
					"isValidAnswer" : dwr.util.getValue("QS"+optId+"validQuestion"),
					"jobOrder" : jobOrder,
					"sliderScore" : scroreVal
				});
			}

		}else if(qType=='ml' || qType=='sl')
		{
			var insertedText = dwr.util.getValue("QS"+i+"opt");
			if((insertedText!=null && insertedText!="") || isRequired==0)
			{
				if(isRequired==1){
					isRequiredAns=1;
				}
				
				if(isRequired==1){
					isRequiredAns=1;
				}
				var schoolMaster="";
				if($(".school"+i).length>0){
					schoolMaster=$(".school"+i).val();
				}
				arr.push({ 
					"insertedText"    : insertedText,
					"question"  : dwr.util.getValue("QS"+i+"question"),
					"questionTypeMaster" : questionTypeMaster,
					"questionType" : questionTypeShortName,
					"districtSpecificPortfolioQuestions" : districtSpecificQuestion,
					"jobOrder" : jobOrder,
					"sliderScore" : scroreVal,
					"schoolIdTemp" : schoolMaster
				});
			}else
			{
				if(isRequired==1){
					$("#errordivspecificquestion").html("&#149; "+resourceJSON.PlzProvideResponseToAllQues+".<br>");
					dSPQuestionsErrorCount=1;
				}
			}
		}else if(qType=='et'){
			var optId="";
			var errorFlag=1;
			if($("input[name=QS"+i+"opt]:radio:checked").length > 0 )
			{
				errorFlag=0;
				optId=$("input[name=QS"+i+"opt]:radio:checked").val();
			}else
			{
				if(isRequired==1){
					errorFlag=1;
					$("#errordivspecificquestion").html("&#149;  "+resourceJSON.PlzProvideResponseToAllQues+".<br>");
					dSPQuestionsErrorCount=1;
				}
			}
			
			var insertedText = dwr.util.getValue("QS"+i+"optet");
			var isValidAnswer = dwr.util.getValue("QS"+optId+"validQuestion");
			if(isValidAnswer=="false" && insertedText.trim()==""){
				if(isRequired==1){
					$("#errordivspecificquestion").html("&#149;  "+resourceJSON.PlzProvideResponseToAllQues+".<br>");
					dSPQuestionsErrorCount=1;
					errorFlag=1;
				}
			}
			if(errorFlag==0){
				if(isRequired==1){
					isRequiredAns=1;
				}
				arr.push({ 
					"selectedOptions"  : optId,
					"question"  : dwr.util.getValue("QS"+i+"question"),
					"insertedText"    : insertedText,
					"questionTypeMaster" : questionTypeMaster,
					"questionType" : questionTypeShortName,
					"questionOption" : dwr.util.getValue("qOptS"+optId),
					"districtSpecificPortfolioQuestions" : districtSpecificQuestion,
					"isValidAnswer" : isValidAnswer,
					"jobOrder" : jobOrder,
					"sliderScore" : scroreVal
				});
			}
		}else if(qType=='mlsel'){
			try{
				 var multiSelectArray="";
				 var inputs = document.getElementsByName("multiSelect"+i); 
				 for (var j = 0; j < inputs.length; j++) {
				        if (inputs[j].type === 'checkbox') {
				        	if(inputs[j].checked){
				        		multiSelectArray+=	inputs[j].value+"|";
				            }
				        }
				} 
				if(multiSelectArray!=""  || isRequired==0){
					if(isRequired==1){
						isRequiredAns=1;
					} 
					arr.push({ 
						"selectedOptions"  : multiSelectArray,
						"question"  : dwr.util.getValue("QS"+i+"question"),
						"questionTypeMaster" : questionTypeMaster,
						"questionType" : questionTypeShortName,
						//"questionOption" : dwr.util.getValue("qOptS"+optId),
						"districtSpecificPortfolioQuestions" : districtSpecificQuestion,
						"isValidAnswer" : isValidAnswer,
						"jobOrder" : jobOrder,
						"sliderScore" : scroreVal
					});
				}else{
					if(isRequired==1){
						$("#errordivspecificquestion").html("&#149; "+resourceJSON.PlzProvideResponseToAllQues+".<br>");
						dSPQuestionsErrorCount=1;
					}
				}
			}catch(err){}
		}if(qType=='mloet'){
			try{
				 var multiSelectArray="";
				 var inputs = document.getElementsByName("multiSelect"+i); 
				 for (var j = 0; j < inputs.length; j++) {
				        if (inputs[j].type === 'checkbox') {
				        	if(inputs[j].checked){
				        		multiSelectArray+=	inputs[j].value+"|";
				            }
				        }
				} 
				var insertedText = dwr.util.getValue("QS"+i+"optmloet"); 
				if(multiSelectArray!="" || isRequired==0){
					if(isRequired==1){
						isRequiredAns=1;
					} 
					arr.push({ 
						"selectedOptions"  : multiSelectArray,
						"question"  : dwr.util.getValue("QS"+i+"question"),
						"insertedText"    : insertedText,
						"questionTypeMaster" : questionTypeMaster,
						"questionType" : questionTypeShortName,
						//"questionOption" : dwr.util.getValue("qOptS"+optId),
						"districtSpecificPortfolioQuestions" : districtSpecificQuestion,
						"isValidAnswer" : isValidAnswer,
						"jobOrder" : jobOrder,
						"sliderScore" : scroreVal
					});
				}else{
					if(isRequired==1){
						$("#errordivspecificquestion").html("&#149; "+resourceJSON.PlzProvideResponseToAllQues+".<br>");
						dSPQuestionsErrorCount=1;
					}
				}
			}catch(err){}
		}else if(qType=='rt'){
			var optId="";
			var score="";
			var rank="";
			var scoreRank=0;
			var opts = document.getElementsByName("optS");
			var scores = document.getElementsByName("scoreS");
			var ranks = document.getElementsByName("rankS");
			var o_ranks = document.getElementsByName("o_rankS");
			
			var tt=0;
			var uniqueflag=false;
			for(var i = 0; i < opts.length; i++) {
				optId += opts[i].value+"|";
				score += scores[i].value+"|";
				rank += ranks[i].value+"|";
				if(checkUniqueRankForPortfolio(ranks[i]))
				{
					uniqueflag=true;
					break;
				}

				if(ranks[i].value==o_ranks[i].value)
				{
					scoreRank+=parseInt(scores[i].value);
				}
				if(ranks[i].value=="")
				{
					tt++;
				}
			}
			if(uniqueflag)
				return;

			if(tt!=0)
				optId=""; 

			if(optId=="")
			{
				//totalSkippedQuestions++;
				//strike checking
			}
			var totalScore = scoreRank;
			if(isRequired==1){
				isRequiredAns=1;
			} 
			arr.push({ 
				"selectedOptions"  : optId,
				"question"  : dwr.util.getValue("QS"+i+"question"),
				"questionTypeMaster" : questionTypeMaster,
				"questionType" : questionTypeShortName,
				"questionOption" : dwr.util.getValue("qOptS"+optId),
				"districtSpecificPortfolioQuestions" : districtSpecificQuestion,
				"isValidAnswer" : isValidAnswer,
				"jobOrder" : jobOrder,
				"optionScore"      : score,
				"totalScore"       : totalScore,
				"insertedRanks"    : rank,
				"maxMarks" :o_maxMarks,
				"sliderScore" : scroreVal
			});
		}else if(qType=='OSONP'){
			//	alert("hello "+qType);
			try{
				 var multiSelectArray="";
				 var inputs = document.getElementsByClassName("OSONP"+i); 
				 for (var j = 0; j < inputs.length; j++) {
				        if (inputs[j].type === 'radio') {
				        	if(inputs[j].checked){
				        		multiSelectArray+=	inputs[j].value+"|";
				            }
				        }
				} 
				var insertedText = dwr.util.getValue("QS"+i+"OSONP");
				
				if(multiSelectArray!="" || isRequired==0){
					if(isRequired==1){
						isRequiredAns=1;
					} 
					arr.push({ 
						"selectedOptions"  : multiSelectArray,
						"question"  : dwr.util.getValue("QS"+i+"question"),
						"insertedText"    : insertedText,
						"questionTypeMaster" : questionTypeMaster,
						"questionType" : questionTypeShortName,
						//"questionOption" : dwr.util.getValue("qOptS"+optId),
						"districtSpecificPortfolioQuestions" : districtSpecificQuestion,
						"isValidAnswer" : isValidAnswer,
						"jobOrder" : jobOrder
					});
				}else{
					if(isRequired==1){
						$("#errordivspecificquestion").html("&#149; "+resourceJSON.PlzProvideResponseToAllQues+".<br>");
						dSPQuestionsErrorCount=1;
					}
				}
			}catch(err){}
		}else if(qType=='DD'){
			try{
				
				 var multiSelectArray=$("#dropdown"+i+" :selected").val() ;				
			
				if(multiSelectArray!=""  || isRequired==0){
					if(isRequired==1){
						isRequiredAns=1;
					} 
					arr.push({ 
						"selectedOptions"  : multiSelectArray,
						"question"  : dwr.util.getValue("QS"+i+"question"),
						"questionTypeMaster" : questionTypeMaster,
						"questionType" : questionTypeShortName,
						//"questionOption" : dwr.util.getValue("qOptS"+optId),
						"districtSpecificPortfolioQuestions" : districtSpecificQuestion,
						"isValidAnswer" : isValidAnswer,
						"jobOrder" : jobOrder
					});
				}else{
					if(isRequired==1){
						$("#errordivspecificquestion").html("&#149; "+resourceJSON.PlzProvideResponseToAllQues+".<br>");
						dSPQuestionsErrorCount=1;
					}
				}
			}catch(err){}
		}
		if(qType=='sscb'){
			try{
				
				var insertedText = dwr.util.getValue("QS"+i+"multiselectText");
				 var multiSelectArray="";
				 var inputs = document.getElementsByName("multiSelect"+i); 
				 for (var j = 0; j < inputs.length; j++) {
				        if (inputs[j].type === 'checkbox') {
				        	if(inputs[j].checked){
				        		multiSelectArray+=	inputs[j].value+"|";
				            }
				        }
				} 
				 
				 if($(".school"+i).length>0){
						schoolMaster=$(".school"+i).val();
					}
				if(multiSelectArray!=""){
					if(isRequired==1){
						isRequiredAns=1;
					} 
					arr.push({ 
						"selectedOptions"  : multiSelectArray,
						"question"  : dwr.util.getValue("QS"+i+"question"),
						"insertedText"    : insertedText,
						"questionTypeMaster" : questionTypeMaster,
						"questionType" : questionTypeShortName,
						"schoolIdTemp" : schoolMaster,
						"districtSpecificPortfolioQuestions" : districtSpecificQuestion,
						"isValidAnswer" : isValidAnswer,
						"jobOrder" : jobOrder
					});
				}else{
					if(isRequired==1){
						$("#errordivspecificquestion").html("&#149; "+resourceJSON.PlzProvideResponseToAllQues+".<br>");
						dSPQuestionsErrorCount=1;
					}
				}
			}catch(err){}
		}
		
		if(isRequiredAns==1){
			ansRequiredCount++;
		}
	}
	//alert('isRequiredCount:::'+isRequiredCount);
	//alert('ansRequiredCount:::'+ansRequiredCount);
	if(isRequiredCount==ansRequiredCount) 
	{
		CGServiceAjax.setDistrictQPortfoliouestions(arr,dspqType,teacherId,{ 
			async: false,
			errorHandler:handleError,
			callback: function(data){
			if(data!=null)
			{
				arr =[];
				$("#saveDataLoading").modal("show");
				if($("#teacherIdForprofileGridVisitLocation").length>0){
					var currentLocation =$("#teacherIdForprofileGridVisitLocation").val();				
					if(currentLocation=="Teacher Pool"){
						searchTeacher();
					}
				}
			}
		}
		});	
	}else
	{
		$("#errordivspecificquestion").html("&#149; "+resourceJSON.PlzProvideResponseToAllQues+".<br>");
		dSPQuestionsErrorCount=1;
	}
}

function pnqSave(teacherId){
	var pnqOption = $("#pnqList option:selected").val();
	TeacherProfileViewInDivAjax.updatePNQByUser(teacherId,pnqOption,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
			//getCandidateGrid();
			//refreshStatus();
		}
	});
}


/*====================================================================*/
var arrTagCall =[];
function showTagDetailsCG(j,jftId,tid,jid)
{
	if(jid!=null)
		$('#teacherJobId').val(jid);
	
	showTagDetails(j,jftId,tid,jid);
	
	var height=setDialogPositionNew();
	height=height+80;	
	
	var style = $('<style>.popover { position: absolute;top: '+height+'px !important;left: 0px;width: 500px;}</style>')
	$('html > head').append(style);

	if($.inArray(parseInt(j), arrTagCall)==-1)
	{
		arrTagCall.push(parseInt(j));
		$('#tag'+j).popover('show');
	}
	
/*	isClosed=false;
	$('#tag'+j).on('shown.bs.popover', function() {
		setTimeout(function() {
			
			var i=1;
			if(!isClosed)
			while(i<=300)
			{
				if ($("#popoverTrigger").next('div.popover:visible').length)
				{
					// popover is visible
				}
				else
					$('#tag'+j).popover('show');
				
				i++;
			}
		}, 50);
	});
*/
}

function showTagDetails(j,jftId,tid,jid)
{
	var showTags="true";
	try
	{
		var districtId = $("#districtId").val();
		var entityType=$("#entityTypeId").val();
		
		//var addDistrictSpecificTag=$("#schoolAddDistrictSpecificTag").val();
		//var removeDistrictSpecificTag=$("#schoolRemoveDistrictSpecificTag").val();
		//var addJobSpecificTag=$("#schoolAddJobSpecificTag").val();
		//var removeJobSpecificTag=$("#schoolRemoveJobSpecificTag").val();
		
	   //alert("entityType:- "+entityType+"\naddDistrictSpecificTag:- "+addDistrictSpecificTag+"\nremoveDistrictSpecificTag:- "+removeDistrictSpecificTag+"\naddJobSpecificTag:- "+addJobSpecificTag+"\nremoveJobSpecificTag:- "+removeJobSpecificTag);
		/*if(entityType==3)
			if( addDistrictSpecificTag=="true" | removeDistrictSpecificTag=="true" | addJobSpecificTag=="true" | removeJobSpecificTag=="true" )
				showTags="true";
			else
				showTags="false";*/
	}
	catch(err){}
	try
	{
		if(entityType==3){
			var isDistrictWideTags=$("#isDistrictWideTags").val();
			var isJobSpecificTags=$("#isJobSpecificTags").val();
			//alert("isDistrictWideTags :: "+isDistrictWideTags +" isJobSpecificTags :: "+isJobSpecificTags)
			if( isDistrictWideTags=="true" || isJobSpecificTags=="true")
				showTags="true";
			else
				showTags="false";
			
		}

	}catch(e){}
	if(showTags=="true")
	{
		$('#loadingDiv').show();
		
		CandidateGridAjaxNew.getTagDetails(jid,tid,jftId,j,{ 
			async: false,
			errorHandler:handleError,
			callback:function(data)
			{
			
			$("#disSpeDiv").show();
			$("#disSpeDiv").html(data);
			document.getElementById("tagDiv").innerHTML=data;
			
			$('#tag'+j).popover({ 
				html : true,
				placement: 'right',
				trigger: 'manual',
				content: function() {
				return $("#disSpeDiv").html();
			}
			});
			
			$("#disSpeDiv").hide();
			$('#loadingDiv').hide();
			$('#tag'+j).popover('show');
		}});
	}
}

function divToggelShow()
{
	try {
		$("#divToggel").show();
	} catch (e) {}
}

function getiFrameForTree()
{
	CGServiceAjax.getiFrameForTree({ 
	async: true,
	callback: function(data)
	{
		$('#treeiFrame').html(data)
	},
	errorHandler:handleError
	});
}

function certTextDiv(id){
	
	$("#cgTeacherDivMaster").modal('hide');
	$(".certTextContent").html($("#cerTTextContent"+id).val());
	$("#certTextDivDSPQ").modal("show");
}
function certTextDivClose(){
	 
	$("#cgTeacherDivMaster").modal('show');
	 ("#certTextDivDSPQ").modal("show");
	 $(".certTextContent").html("");
}
//---------------school search----------------
function getSchoolMasterAutoCompQuestion(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("schoolName15").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getSchoolArrayQuestion(txtSearch.value);
		fatchData2Question(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getSchoolArrayQuestion(schoolName){
	var searchArray = new Array();
	var districtId	= $("#questionDistrictId").val();
	CGServiceAjax.getFieldOfSchoolList(districtId,schoolName,{ 
			async: false,
			callback: function(data){
			hiddenDataArray = new Array();
			showDataArray = new Array();
			for(i=0;i<data.length;i++){
				searchArray[i]=data[i].schoolName;
				showDataArray[i]=data[i].schoolName;
				hiddenDataArray[i]=data[i].schoolMaster.schoolId;
			}
		},
		errorHandler:handleError
		});	
	return searchArray;
}


selectFirst="";

var fatchData2Question= function(txtSearch,searchArray,txtId,txtdivid){

	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
			}
			
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}
function hideSchoolMasterDivQuestion(dis,hiddenId,divId)
{
	document.getElementById(hiddenId).value="";
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			document.getElementById(hiddenId).value=hiddenDataArray[index];			
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}

function onSelectOpenOptions(src,cnt){
	
	if(src=="show"){
		$('.textareaOp'+cnt).show();
	}else{
		$('.textareaOp'+cnt).hide();
		
		$('.'+cnt+'OSONPtext').val("");
	}
}

function showVideo(email)
{
	CandidateGridAjaxNew.showVideo(email,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){
		$('#myModalVideoInterview').modal("show");
		//document.getElementById("iframeVideo").src="https://platform.teachermatch.org";
		document.getElementById("videoInterViewDiv").innerHTML=data;
	}
	});	
}

/*-----------------common panel ---------------*/
var arrPanel =[];
function getCommonPanelByJobWise()
{
	var jobId=$('#jobId').val();
	$('.cmPanel1').show();
	$('.cmPanel2').hide();
	
	isPanel=true;
	CandidateGridSubAjax.getPanelByJobWiseStatus(jobId,{ 
		async: true,
		callback: function(data)
		{
			if(data[0]==1)
			{	
				$('#errordivStauts').hide();
				$('#jobPanelStatusId').html(data[1]);
				$('#myJobWiseStatusDiv').modal('show');
				tId=teacherId;
				jId=jobId;
				isPanelReq=isPanel;
			}else
			{
				$('#myJobWiseStatusDiv').modal('hide');
				$('#jPanelStauts').val(data[1]);				
				getPanel(teacherId,jobId,isPanel,data[1]);
			}
		},
	});
}

function saveCommonPanel(checkedFlag){

	$('#panelDate').css("background-color", "");
	$('#panelTime').css("background-color", "");
	$('#panelMeridiem').css("background-color", "");
	$('#panelTimezone').css("background-color", "");
	$('#panelLocation').css("background-color", "");
	$('#panelUserId').css("background-color", "");

	//var panelDate 		=		new Date(trim(document.getElementById("panelDate").value));
	var panelDateVal 	=		trim(document.getElementById("panelDate").value);
	var panelTime		=		trim(document.getElementById("panelTime").value);
	var panelMeridiem		=		trim(document.getElementById("panelMeridiem").value);
	var panelLocation		=		trim(document.getElementById("panelLocation").value);
	var disSchCmnt		=		trim(document.getElementById("panelDisSchCmnt").value);
	var t				=		document.getElementById("panelTimezone");
	var panelTimezone		=		trim(t.options[t.selectedIndex].value);
	var e				=		document.getElementById("panelUserId");
	var userId			=		trim(e.options[e.selectedIndex].value);
	var panelStatus		=		document.getElementById("panelStatus");
	
	//var teacherId		=		trim(document.getElementById("pteacherId").value);
	var jobId		=		trim(document.getElementById("jobId").value);
	var panelId = $('#panelId').val();
	var counter=0;
	var focus=0;
	var saveteacherarray="";
	var saveteacherarray = new Array();
	 var inputs = $("input[name=cgCBX]");
	 for (var i = 0; i < inputs.length; i++) {
	        if (inputs[i].type === 'checkbox') {
	        	if(inputs[i].checked){	        		
	        		//saveteacherarray+=	$(inputs[i]).attr( "teacherid" )+",";
	        		saveteacherarray.push($(inputs[i]).attr( "teacherid" ));
	        		
	            }
	        }
	 } 

	$('#panelErrordiv').empty();
	
	if (panelDateVal=="" || panelDate=="Invalid Date")
	{
		$('#panelErrordiv').append("&#149; "+resourceJSON.PlzEtrPanelDate+"<br>");
		if(focus==0)
			$('#panelDate').focus();
		$('#panelDate').css("background-color", "#F5E7E1");
		counter++;
	}

	if(panelDateVal!=""){

		try{
			var  date1 = panelDateVal.split("-");
			var currentDate = "";
			var dateMsg = "Current";
			if(panelId=="")
			  currentDate = document.getElementById("currDate").value;
			else
			{
				currentDate = document.getElementById("currDate").value;
			  //currentDate = document.getElementById("panelCreatedDate").value;
			  //dateMsg = "panel Created";
			}
			
			var	date2 = currentDate.split("-");
			var sDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
			var eDate = new Date(date2[0]+"/"+date2[1]+"/"+date2[2]);
			if(sDate < eDate){
				$('#panelErrordiv').append("&#149; "+resourceJSON.MsgDateGreaterOrEqual+" "+dateMsg+" "+resourceJSON.MsgDate+"<br>");
				if(focus==0)
					$('#panelDate').focus();
				$('#panelDate').css("background-color", "#F5E7E1");
				counter++;

			} 
		}catch(err){}
	}
	if (panelTime=="Select Time")
	{
		$('#panelErrordiv').append("&#149; "+resourceJSON.PlzEtrPanelTime+"<br>");
		if(focus==0)
			$('#panelTime').focus();
		$('#panelTime').css("background-color", "#F5E7E1");
		counter++;
	}

	if (panelMeridiem=="Select AM/PM")
	{
		$('#panelErrordiv').append("&#149; "+resourceJSON.MsgSelectAmOrPm+"<br>");
		if(focus==0)
			$('#panelMeridiem').focus();
		$('#panelMeridiem').css("background-color", "#F5E7E1");
		counter++;
	}
	if (panelTimezone=="Select Timezone")
	{
		$('#panelErrordiv').append("&#149;"+resourceJSON.PlzSlctPanelTimeZone+"<br>");
		if(focus==0)
			$('#panelTimezone').focus();
		$('#panelTimezone').css("background-color", "#F5E7E1");
		counter++;
	}
	if (panelLocation=="")
	{
		$('#panelErrordiv').append("&#149; "+resourceJSON.PlzEtrPanlLocation+"<br>");
		if(focus==0)
			$('#panelLocation').focus();
		$('#panelLocation').css("background-color", "#F5E7E1");
		counter++;
	}
	//if (userId=="Select Attendee" || arrPanel.length==0)
	if (arrPanel.length==0)
	{
		$('#panelErrordiv').append("&#149; "+resourceJSON.PlzSlctAttendee+"<br>");
		if(focus==0)
			$('#userId').focus();
		$('#panelUserId').css("background-color", "#F5E7E1");
		counter++;
	}

	if(panelStatus.checked && !checkedFlag)
	{
		$('#message2showConfirm').html(resourceJSON.MsgMarkPanelCompleted);
		$('#footerbtn').html("<button class='btn btn-primary' onclick='saveCommonPanel(true)' >"+resourceJSON.btnOk+"</button> <button class='btn' data-dismiss='modal' aria-hidden='true'>"+resourceJSON.btnCancel+"</button>");
		$('#myModal3').modal('show');

		return;
	}

	if(counter==0){		
		panelId=panelId==""?"0":panelId;		
		CandidateGridSubAjax.saveCommonPanel(panelDateVal,panelTime,panelMeridiem,panelTimezone,panelLocation,disSchCmnt,
				saveteacherarray,jobId,panelStatus.checked,arrPanel,panelId,$('#jPanelStauts').val(),{
			async: true,
			callback: function(data)
			{
			$('.cmPanel1').hide();
			$('.cmPanel2').show();	
			clearPanelField();
			arrPanel = [];
			//document.getElementById("attendeeDiv").style.display='none';
			$('#myModalPanel').modal('hide');
			$('#myModal3').modal('hide');
			$('#message2show').html(resourceJSON.MsgPanelScheduled);
			$('#myModal2').modal('show');
			getCandidateGrid();
			refreshStatus();
			
			},
		});
	}
}

//add By Ram Nath

function getBreakUpRecords(table,listLength,trId,sessionLst,start) {
    var cntr = start;
    var wait="<div id='removeWait' style='text-align:center;padding-top:0px;'><img src='images/loadingAnimation.gif' /> <br>"+resourceJSON.MsgLoad+"</div>";
    /*if(start!=0)
    $('#'+table).append(wait);*/
    function next() {
    	var lasttrId;
    	if(start==1)
    	lasttrId="#"+trId+(callNoofRecords*cntr);
    	else
    		lasttrId="#"+trId+callNoofRecords;	    		
    	CGServiceAjax.getBreakUpData(cntr,sessionLst, { 
			async: true,
			cache: false,
			errorHandler:handleError,
			callback:function(record)
			{
                //console.log(record);
    			$(lasttrId).after(record); 
                ++cntr;
                tpJbIDisable(table);
        		tpJbIDisable();        		
                if (cntr <listLength) {
                	next();
                } 
            }
        });
    }
    next();    
    tpJbIDisable(table);
	tpJbIDisable();
		/*try{$('#removeWait').remove();}catch(e){}*/
}

function tpJbIDisable(table)
{
	var noOrRow = $('#'+table+' tr').length;
	for(var j=1;j<=noOrRow;j++)
	{
		$('#tpResume'+j).tooltip();
		$('#tpPortfolio'+j).tooltip();
		$('#tpJSA'+j).tooltip();
		$('#hireTp'+j).tooltip();
		$('#tpPDReport'+j).tooltip();
		$('#tpCL'+j).tooltip();
		$('#tpAct'+j).tooltip();
		$('#tpMsg'+j).tooltip();
		$('#tpNotes'+j).tooltip();
		$('#tpTrans'+j).tooltip();	
		$('#tpCert'+j).tooltip();
		$('#tpMessage'+j).tooltip();
		$('#tpInv'+j).tooltip();
		$('#tprr'+j).tooltip();	
		$('#ftsbu'+j).tooltip();
		$('#tooltipPreview'+j).tooltip()
		$('#teacherName_'+j).tooltip();
		$('#i4Ques'+j).tooltip();
		$('#teacher_PNQ'+j).tooltip();
		try { $('#alum'+j).tooltip(); } catch (e) {}
		try{ $('#tpInternalCandidate'+j).tooltip(); }catch(e){}
		try{ $('.formerEmployee').tooltip(); }catch(e){}
		try{ $('.returedEmployee').tooltip(); }catch(e){}
		try{$('#tpRefChk'+j).tooltip(); }catch(e){}
		try{$('#dtFailed'+j).tooltip(); }catch(e){}
		try{$('#tpAssessmentinvite'+j).tooltip(); }catch(e){}
		try{$('#pc'+j).tooltip(); }catch(e){}
		try{$('#cndNotReviewed'+j).tooltip(); }catch(e){}
	}
}
//end By Ram Nath

function clickedOnYes()
{
	$("#vetranOptionDiv").show();
}

function clickedOnNo()
{
	$("#vetranOptionDiv").hide();
}

function getHonorsGrid(teacherId)
{
	var visitLocation=document.getElementById("teacherIdForprofileGridVisitLocation").value;
	TeacherProfileViewInDivAjax.getHonorsGridWithoutAction(teacherId,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			document.getElementById("gridDataTeacherHonors").innerHTML=data;
			applyScrollOnHonors();
			
		}});
}

function getInvolvementGrid(teacherId)
{
	TeacherProfileViewInDivAjax.getInvolvementGrid(teacherId,dp_Involvement_Rows,dp_Involvement_page,dp_Involvement_sortOrderStr,dp_Involvement_sortOrderType,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{//alert("2");
			$('#gridDataTeacherInvolvementOrVolunteerWork').html(data);
			applyScrollOnInvl();
		}});
}
//end By Ram Nath

// start Vikas Kumar
function checkActionMenu(){
	var inputs = document.getElementsByTagName("input"); 
	var chkCount=0;
	var chkCheckedCount=0;
	 for (var i = 0; i < inputs.length; i++) {
	        if (inputs[i].type === 'checkbox') {
	        	chkCount++;
	            if(inputs[i].checked){
	            	chkCheckedCount++;
	            }
	        }
	 } 
	 if(chkCheckedCount>0){
		 document.getElementById("actionDiv").style.display="inline";
	 }else{
		 document.getElementById("actionDiv").style.display="none";
	 }

}

function checkAllCandidate(){
	var flag = document.getElementById("checkAllCandidate").checked;
   	
	 if(flag==true)
	 { 
		$('#divReportGrid input[internalCand="interDivHidden"]').each(function() {
			 $(this).attr('checked', 'checked');
			checkActionMenu();
		});
	 }
	 else{		 
		 $('#divReportGrid input[internalCand="interDivHidden"]').each(function() {
			 $(this).prop('checked', false);
			 checkActionMenu();
			 
				});
	 }
}

function checkAllCandidateForInComp(){
	var flag = document.getElementById("checkAllCandidateInComp").checked;
   	
	 if(flag==true)
	 { 
		$('#divInCompCGGrid input[name="cgCBX"]').each(function() {
			 $(this).attr('checked', 'checked');
			 checkActionMenu();
		});
	 }
	 else{
		 
		 $('#divInCompCGGrid input[name="cgCBX"]').each(function() {
			 $(this).prop('checked', false);
			 checkActionMenu();
				});
	 }
}

function checkAllCandidateForWithCand(){
	var flag = document.getElementById("checkAllCandidateWithCand").checked;
   	
	 if(flag==true)
	 { 
		$('#divWithDrCGGrid input[name="cgCBX"]').each(function() {
			 $(this).attr('checked', 'checked');
			 checkActionMenu();
		});
	 }
	 else{
		 
		 $('#divWithDrCGGrid input[name="cgCBX"]').each(function() {
			 $(this).prop('checked', false);
			 checkActionMenu();
				});
	 }
}

function checkAllForRejectedCand(){
	var flag = document.getElementById("checkAllCandidateRejected").checked;
   	
	 if(flag==true)
	 { 
		$('#divRemoveCGGrid input[name="cgCBX"]').each(function() {
			 $(this).attr('checked', 'checked');
			 checkActionMenu();
		});
	 }
	 else{
		 
		 $('#divRemoveCGGrid input[name="cgCBX"]').each(function() {
			 $(this).prop('checked', false);
			 checkActionMenu();
				});
	 }
}

function checkAllCandidateForTimedOut(){
	var flag = document.getElementById("checkAllCandidateTimedOut").checked;
   	
	 if(flag==true)
	 { 
		$('#divVltCGGrid input[name="cgCBX"]').each(function() {
			 $(this).attr('checked', 'checked');
			 checkActionMenu();
		});
	 }
	 else{
		 
		 $('#divVltCGGrid input[name="cgCBX"]').each(function() {
			 $(this).prop('checked', false);
			 checkActionMenu();
				});
	 }
}

function checkAllCandidateForHired(){
	var flag = document.getElementById("checkAllCandidateHired").checked;
   	
	 if(flag==true)
	 { 
		$('#divHiredCGGrid input[name="cgCBX"]').each(function() {
			 $(this).attr('checked', 'checked');
			 checkActionMenu();
		});
	 }
	 else{
		 
		 $('#divHiredCGGrid input[name="cgCBX"]').each(function() {
			 $(this).prop('checked', false);
			 checkActionMenu();
				});
	 }
}
//end Vikas Kumar>>>>>>> 1.51
function openDynmicDivContent(id,para){
	
	$("#cgTeacherDivMaster").modal('hide');
	$(".certTextContent").html($("#"+para+id).html());	
	$("#certTextDivDSPQ").modal("show");
}

function addCertificate(checkedFlag)
{
	var duplicateCert=false;
	var hiddenCertTypeIds 	=	document.getElementsByName("hiddenCertTypeId");
	var sb="";
	var stateId		=	trim(document.getElementById("stateId").value);
	var certType	=	trim(document.getElementById("certType").value);
	var certTypeId	=	trim(document.getElementById("certificateTypeMaster").value);
	
	if(hiddenCertTypeIds.length>0)
	{
		for(var i=0;i<hiddenCertTypeIds.length;i++)
		{
			if(hiddenCertTypeIds[i].value==certTypeId)
			{
				duplicateCert=true;
				//alert("duplicateCert=="+duplicateCert);
			}
		}
	}
	if(duplicateCert)
	{
		$('#myModalSearch').modal('hide');
		$('#myModal222').modal('show');
	}
	else
	{
		//alert("checkedFlag=="+checkedFlag+"\nstateId=="+stateId+"\ncertType=="+certType+"\ncertTypeId=="+certTypeId);
		if(stateId!="" && stateId!=0 && certType!="" && certType!=0 && certTypeId!="" && certTypeId!=0)
		{
			document.getElementById('divCertificate').style.display='block';
			if(checkedFlag==1)
			{
				//alert("checkedFlag=="+checkedFlag+"\nstateId=="+stateId+"\ncertType=="+certType+"\ncertTypeId=="+certTypeId);
				sb+="<div class='col-sm-12 col-md-12' id='div"+certTypeId+"'>"+certType;
				sb+="<a data-original-title='Remove' rel='tooltip' id='"+certTypeId+"' href='javascript:void(0);' onclick=\"removeCertificate("+certTypeId+");\"><img width='15' height='15' class='can' src='images/can-icon.png' alt=''></a>";
				sb+="<input type='hidden' name='hiddenCertTypeId' id='c"+certTypeId+"' value='"+certTypeId+"'/>";
				sb+="<script type='text/javascript'>$('#"+certTypeId+"').tooltip();</script></div>";
				
				$('#divCertificate').append(sb);
				document.getElementById("stateId").value="0";
				document.getElementById("certType").value="";
				document.getElementById("certificateTypeMaster").value="0";
			}
		}
	}
}

function removeCertificate(certTypeId)
{
	if(certTypeId!="" && certTypeId!=0)
	{
		//alert("certTypeId=="+certTypeId);
		document.getElementById('div'+certTypeId).style.display='none';
		$('#div'+certTypeId).html("");
	}
}

function showAndHideModal()
{
	$('#myModal222').modal('hide');
	$('#myModalSearch').modal('show');
	document.getElementById('certType').value="";
	document.getElementById('certType').focus();
}

//Mukesh New Code For Applying Jobs 
function getJobDetailListFromCGBySearch()
{
	pageJobDetail = 1;
	getJobDetailListFromCG();
}

function getJobDetailListFromCG()
{
	currentPageFlag="jobApplyFromCG";
	if($('input[name=cgCBX]:checkbox:checked').length == 0 ){
		$("#alertmsgToShow").html(resourceJSON.PlzSlctAtlstOneCndidate);
		$('#alertMsgForApply').modal('show');
	}else{
		$("html, body").animate({ scrollTop: 0 }, "slow");
		
		var cgjobId			=	$("#jobId").val().trim();
		var geoZoneId       =   $("#zone").val();
		var schoolId = "";
		try{
			schoolId		=	$("#schoolIdApply").val().trim();
		}catch(e){}
		var jobCategoryId	=	$("#jobCategoryIdSearch").val().trim();
		var jobId			=	$("#jobIdSearch").val().trim();
		var subjectId 		= 	document.getElementById('subjects1');
		var branchId = "";
		try{
			branchId = $("#branchSearchId").val().trim();
		}catch(e){}
		var subjectIdList = "";

		for(var i=0; i<subjectId.options.length;i++)
		{	
			if(subjectId.options[i].selected == true){
				subjectIdList = subjectIdList+subjectId.options[i].value+",";
			}
		}
		$('#loadingDiv').fadeIn();
	    CandidateGridAjaxNew.getJobDetailListOnCG(cgjobId,noOfRowsJobDetail,pageJobDetail,sortOrderStrJobDetail,sortOrderTypeJobDetail,schoolId,jobCategoryId,jobId,subjectIdList,geoZoneId,branchId,{
			async: true,
			callback: function(data)
			{ 
			 	//$('#loadingDiv').fadeOut();
			 	getAutoNotificationFlag();
			 	$("#divJobListOnCG").html(data);
				applyScrollOnJobsTbl();
				try{$('#myJobDetailListFromCG').modal('show');}catch(e){}
			},
		});
	}
}

function getAutoNotificationFlag()
{
	CandidateGridAjaxNew.getAutoNotFlag({
	async: true,
	errorHandler:handleError,
	callback:function(data)
		{
		   $('#loadingDiv').fadeOut();
		   document.getElementById("sendNotificationOnApply").checked=data;
		}
	
	});		
}

function applyJobOnCG()
{
	if($('input[name=case]:checkbox:checked').length == 0 )
	{
		$("#alertmsgToShow").html(resourceJSON.PlzSlctAtlstOneJobApply);
		$('#alertMsgForApply').modal('show');

	}else
	{
		var checkBox = document.getElementsByName("case");
		var sendnitification=0;
		if(document.getElementById("sendNotificationOnApply").checked)
		{
			sendnitification=1;
		}
		var arr =[];
		for(i=0;i<checkBox.length;i++)
		{
			if(checkBox[i].checked==true)
				arr.push(checkBox[i].value);
		}
		
		
		var teacherIds = "";
		$('input[name="cgCBX"]:checked').each(function(){
			var teacherId = $(this).attr("teacherid");//teacherid
			if(teacherIds=="")
				teacherIds=teacherIds+teacherId;
			else
				teacherIds=teacherIds+","+teacherId;
		});
		$('#loadingDiv').fadeIn();
		CandidateGridAjaxNew.applyJobsFromCG(teacherIds,arr,sendnitification,{ 
			async: true,
			callback: function(data)
			{
				try 
				{
					if(data="success"){
						$('#loadingDiv').fadeOut();
						changeonspecificrows(teacherIds);
						$("#alertmsgToShow").html(resourceJSON.JobApplySuccessfully);
						$('#alertMsgForApply').modal('show');
						try{$('#myJobDetailListFromCG').modal('hide');}catch(e){}
					}
					
				} catch (e) {}
			},
			errorHandler:handleError 
		});
	}
}


function applyScrollOnJobsTbl()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#jobTableLstOnCG').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 150,
        width: 950,
        minWidth: null,
        minWidthAuto: false,
       	colratio:[30,60,55,280,100,140,170,120],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 20,
        wrapper: false
        });
            
        });			
}


function downloadOctUpload(octId)
{
		//alert("downloadAnswer0");
	TeacherProfileViewInDivAjax.downloadOctUpload(octId,{ 
			async: false,
			errorHandler:handleError,
			callback:function(data)
			{
			if(data!="")
				if (data.indexOf(".doc") !=-1)
				{
					document.getElementById('uploadFrameReferencesID').src = ""+data+"";
				}
				else
				{
					document.getElementById("hrefOctUpload").href = data; 
					return false;
				}
			}
		});
	}

function getOctDetails(octNo){
	 
    //var octNo = "518784";//$("#").val();
	$('#loadingDiv').fadeIn();
    TeacherProfileViewInDivAjax.getMemberQualification(octNo,{ 
        async: true,
        errorHandler:handleError,
        callback:function(data)
        {
            
    	$("#changeByName").html("OCT Details for "+$("#teacherNameInPrf").val());
    	$("#octmsgToShow").html(data);    	
    	$("#octContentDiv").modal("show");
    	applyScrollOctMemDetTbl();
    	applyScrollOctMemBasicTbl();
    	applyScrollOctMemAdditionalTbl();
    	$('#cgTeacherDivMaster').modal('hide');
    	$('#loadingDiv').fadeOut();
        }
    });
 
}

function applyScrollOctMemDetTbl()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#octMemberDetail').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 150,
        width: 880,
        minWidth: null,
        minWidthAuto: false,
       	colratio:[100,180,180,210,210],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 20,
        wrapper: false
        });
            
        });			
}

function applyScrollOctMemBasicTbl()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#octMemberBasic').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 200,
        width: 880,
        minWidth: null,
        minWidthAuto: false,
       	colratio:[146,146,146,146,147,148],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 20,
        wrapper: false
        });
            
        });			
}

function applyScrollOctMemAdditionalTbl()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#octMemberAdditional').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        //height: 200,
        width: 880,
        minWidth: null,
        minWidthAuto: false,
       	colratio:[220,220,220,220],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 20,
        wrapper: false
        });
            
        });			
}

function printResume()
{
	var teacherIds="";
	$('input[name="cgCBX"]:checked').each(function() {
		
		if($(this).attr("teacherid") !=null && ($(this).attr("teacherid")!="") )
		{
			if(teacherIds=="")
				teacherIds = teacherIds + $(this).attr("teacherid");
			else
				teacherIds = teacherIds + "," + $(this).attr("teacherid");
		}
	});
	
	if(teacherIds!="")
	{
		$('#loadingDiv').show();
		CandidateGridAjaxNew.printResume(teacherIds,
		{ 
			async: false,
			errorHandler:handleError,
			callback:function(data)
			{
				$('#loadingDiv').hide();
				if(data=="")	
				{
					alert("Resume is not uploaded by the candidates.")
				}
				else
				{
					var messageAndPath = data.split("##");
					var message = messageAndPath[0];
					var path = messageAndPath[1];
					
					if(message=="")
						window.open(path, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0');
					else
						showConfirmationMessageForPrintResume(message, path);
				}	
			}
		});
	}
}

function showConfirmationMessageForPrintResume(message, path)
{
	$('#confirmationMessageForPrintResume').modal('show');
	$('#confirmationMessageForPrintResume').attr("path",path);
	$('#confirmationMessageForPrintResume .modal-body').html(message);
	$('#confirmationMessageForPrintResume').modal('show');
}

function showConfirmationMessageForPrintResumeOk()
{
	var path = $('#confirmationMessageForPrintResume').attr("path");
	if(path!="")
		window.open(path, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0');
}

function showPrevProfile(){
	$('#cgTeacherDivMaster').modal('show');
}


function getEmployeeNumberInfo(flag,table){
	var height="height:500px;";
	if(flag)
	height="height:auto;";
	var div="<div class='modal hide in' id='employeeInfo' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'><div class='modal-dialog' style='width:850px;'><div class='modal-content'><div class='modal-header'><!--<button type='button' class='close' data-dismiss='modal' aria-label='Close'><span class='confirmFalse' aria-hidden='true'>&times;</span></button>--><h4 class='modal-title' id='myModalLabel' style='color:white;'>Employee Information</h4></div><div class='modal-body' style='"+height+"overflow:auto;' id='tableData'></div><div class='modal-footer'><button type='button' class='btn btn-default confirmFalse' >Ok</button></div></div></div></div>";		try{
			$('#employeeInfo').modal('hide'); 
		}catch(e){}
		try{
			$('#employeeInfo').remove(); 
		}catch(e){}
		$('#cgTeacherDivMaster').after(div);
		$('#cgTeacherDivMaster').modal('hide');
		$('#employeeInfo').modal({show:true,backdrop: 'static',keyboard: false,});
		$('#employeeInfo #tableData').html(table); 
		$('.confirmFalse').click(function(){  	
			$('#employeeInfo').modal('hide');   
			$('#employeeInfo').remove(); 
		 	$('#cgTeacherDivMaster').modal('show');  
		});
	$('#loadingDiv').hide();
 }
function openEmployeeNumberInfo(empId){
$('#loadingDiv').show();
  try{
	  TeacherProfileViewInDivAjax.openEmployeeNumberInfo(empId,{
		  async: false,
		  cache: false,
		  errorHandler:handleError,
		  callback:function(data){
		  $('#cgTeacherDivMaster').modal('hide');
		  $('#cgTeacherDivMaster').hide();
		  //alert(data);
		  var divData=data.split('####');
		  if(divData[0].trim()=='1')
			  	getEmployeeNumberInfo(true,divData[1]);
		  		else
		  		getEmployeeNumberInfo(false,data);
		  }
	   });
  }catch(e){alert(e)}
  }

function downloadAnswer(flag, answerId)
{
	if(flag=='0')
	{
		//alert("downloadAnswer0");
		PFExperiences.downloadAnswer(answerId,{ 
			async: false,
			errorHandler:handleError,
			callback:function(data)
			{
				if (data.indexOf(".doc") !=-1)
				{
					document.getElementById('uploadFrameAnswerId').src = ""+data+"";
				}
				else
				{
					document.getElementById("hrefAnswer").href = data; 
					return false;
				}
			}
		});
	}
	/*else if(flag=='1')
	{
		//alert("downloadAnswer1");
		//alert($("#answerFilePath").val());
		document.getElementById("hrefAnswer").href = $("#answerFilePath").val();
	}*/
}

function downloadResume(teacherId,linkId)
{	
	$('#loadingDiv').show();
	CandidateGridAjaxNew.downloadResume(teacherId,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{			
			$('#loadingDiv').hide();
			if(data=="")	
			{
				alert("Resume is not uploaded by the candidate.")
			}
			else
			{
				if(deviceType)
				{
					if (data.indexOf(".doc")!=-1)
					{
						$("#docfileNotOpen").css({"z-index":"3000"});
				    	$('#docfileNotOpen').show();
					}
					else if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
					{
						$("#exelfileNotOpen").css({"z-index":"3000"});
				    	$('#exelfileNotOpen').show();
					}
					else
					{
						document.getElementById(linkId).href = data; 	
					}
				}
				else if(deviceTypeAndroid)
				{
					if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
					{
						document.getElementById('ifrmTrans').src = ""+data+"";
					}
					else
					{
						document.getElementById(linkId).href = data; 		
					}
				}
				else if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					document.getElementById('ifrmTrans').src = ""+data+"";
				}
				else
				{
					document.getElementById(linkId).href = data; 		
				}			
			}	
			return false;			
							
		}
	});
}

function downloadBackgroundCheck(teacherId,linkId)
{	
	$('#loadingDiv').show();
	CandidateGridAjaxNew.downloadBackgroundCheck(teacherId,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{			
			$('#loadingDiv').hide();
			if(data=="")	
			{
				alert("Background Check is not uploaded by the candidate.")
			}
			else
			{
				if(deviceType)
				{
					if (data.indexOf(".doc")!=-1)
					{
						$("#docfileNotOpen").css({"z-index":"3000"});
				    	$('#docfileNotOpen').show();
					}
					else if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
					{
						$("#exelfileNotOpen").css({"z-index":"3000"});
				    	$('#exelfileNotOpen').show();
					}
					else
					{
						document.getElementById(linkId).href = data; 	
					}
				}
				else if(deviceTypeAndroid)
				{
					if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
					{
						document.getElementById('ifrmTrans').src = ""+data+"";
					}
					else
					{
						document.getElementById(linkId).href = data; 		
					}
				}
				else if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					document.getElementById('ifrmTrans').src = ""+data+"";
				}
				else
				{
					document.getElementById(linkId).href = data; 		
				}			
			}	
			return false;			
							
		}
	});
}

function generateSkillPDF(teacherId)
{
	$('#loadingDiv').show();
	PrintCertificateAjax.generateSkillPDF(teacherId,{
		async: true,
		errorHandler:handleError,
		callback: function(data){
			//window.open(data);
			$('#loadingDiv').hide();
			if(deviceType)
			{
				if (data.indexOf(".doc")!=-1)
				{
					$("#docfileNotOpen").css({"z-index":"3000"});
			    	$('#docfileNotOpen').show();
				}
				else if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					$("#exelfileNotOpen").css({"z-index":"3000"});
			    	$('#exelfileNotOpen').show();
				}
				else
				{
					$('#modalDownloadSPC').modal('hide');
				}
			}
			else if(deviceTypeAndroid)
			{
				if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					try{
						$('#modalDownloadSPC').modal('hide');
					}catch(err){}
					document.getElementById('ifrmSPC').src = ""+data+"";
				}
				else
				{
					$('#modalDownloadSPC').modal('hide');
				}
			}
			else if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
			{
				try{
					$('#modalDownloadSPC').modal('hide');
				}catch(err){}
				document.getElementById('ifrmSPC').src = ""+data+"";
			}
			else
			{
				$('#modalDownloadSPC').modal('hide');
				document.getElementById('ifrmSPC').src = ""+data+"";
				try{
					$('#modalDownloadSPC').modal('show');
				}catch(err){}
			}
		}
	});
}
function getTeacherAssessmentGrid_DivProfile(teacherId)
{
	var visitLocation=document.getElementById("teacherIdForprofileGridVisitLocation").value;
	
	TeacherProfileViewInDivAjax.getTeacherAssessmentGrid(teacherId,noOfRowsEJ,pageEJ,sortOrderStrEJ,sortOrderTypeEJ, { 
  async: true,
  errorHandler:handleError,
  callback:function(data)
  {
   $('#gridTeacherAssessment').html(data);
   if(visitLocation=="CG View"){
	   applyScrollOnAssessments();
	}
   //applyScrollOnAssessments();
  }});
}

function getHiredCandidateReports()
{	
	var jobId=document.getElementById("jobId").value;
	var firstName		=	trim(document.getElementById("firstName").value); 
	var lastName		=	trim(document.getElementById("lastName").value);
	var emailAddress	=	trim(document.getElementById("emailAddress").value);
	var certType		=	trim(document.getElementById("certificateTypeMaster").value);
	var regionId		=	trim(document.getElementById("regionId").value);
	var degreeId		=	trim(document.getElementById("degreeId").value);
	var universityId		=	trim(document.getElementById("universityId").value);
	var normScoreSelectVal		=	trim(document.getElementById("normScoreSelectVal").value);
	var normScore		=	trim(document.getElementById("normScore").value);
	var CGPA		=	trim(document.getElementById("CGPA").value);
	var CGPASelectVal		=	trim(document.getElementById("CGPASelectVal").value);
	var contacted		=	trim(document.getElementById("contacted").value);
	var candidateStatus		=	trim(document.getElementById("candidateStatus").value);
	var stateId=document.getElementById("stateId").value;
	var tagsearchId		=	trim(document.getElementById("tagsearchId").value);
	try{
		var ifrmYOTE = document.getElementById('ifrmYOTE');
		var innerYOTE = ifrmYOTE.contentDocument || ifrmYOTE.contentWindow.document;
		var inputYOTE = innerYOTE.getElementById('YOTEFrm');
		document.getElementById("YOTE").value=inputYOTE.value;
	}catch(e){}
	var YOTESelectVal		=	trim(document.getElementById("YOTESelectVal").value);
	var YOTE				=	document.getElementById("YOTE").value;
	var hiddenCertTypeIds 	=	document.getElementsByName("hiddenCertTypeId");
	var stateId2	=	trim(document.getElementById("stateId2").value);
    var zipCode		=	trim(document.getElementById("zipCode").value);
	var certIds =[];
	for(var i=0;i<hiddenCertTypeIds.length;i++)
	{
		certIds.push(hiddenCertTypeIds[i].value);
	}
	var epiFromDate = "";
	var epiToDate = "";
     try{
	var epiFromDate	=	trim(document.getElementById("epiFromDate").value);
    var epiToDate	=	trim(document.getElementById("epiToDate").value);
     }catch(err){}
     //alert(' Before Call');
     CandidateReportAjax.getHiredCandidateReports(jobId,firstName,lastName,emailAddress,certType,regionId,degreeId,universityId,normScoreSelectVal,
			normScore,CGPA,CGPASelectVal,contacted,candidateStatus,stateId,orderColumn, sortingOrder,totalNoOfRecord,tagsearchId,callbreakup,callNoofRecords,
			certIds,YOTESelectVal,YOTE,stateId2,zipCode,epiFromDate,epiToDate,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
    	 try{
  			document.getElementById("noOfApp2").innerHTML=data;
  		}catch(err)
  		  {}
  		getVltCandidateReports();
		}});
}

function getInCompleteCandidateReports()
{	
	//alert('getInCompleteCandidateReports');
	var jobId=document.getElementById("jobId").value;
	var firstName		=	trim(document.getElementById("firstName").value); 
	var lastName		=	trim(document.getElementById("lastName").value);
	var emailAddress	=	trim(document.getElementById("emailAddress").value);
	var certType		=	trim(document.getElementById("certificateTypeMaster").value);
	var regionId		=	trim(document.getElementById("regionId").value);
	var degreeId		=	trim(document.getElementById("degreeId").value);
	var universityId		=	trim(document.getElementById("universityId").value);
	var normScoreSelectVal		=	trim(document.getElementById("normScoreSelectVal").value);
	var normScore		=	trim(document.getElementById("normScore").value);
	var CGPA		=	trim(document.getElementById("CGPA").value);
	var CGPASelectVal		=	trim(document.getElementById("CGPASelectVal").value);
	var contacted		=	trim(document.getElementById("contacted").value);
	var candidateStatus		=	trim(document.getElementById("candidateStatus").value);
	var stateId=document.getElementById("stateId").value;
	var tagsearchId		=	trim(document.getElementById("tagsearchId").value);
	try{
		var ifrmYOTE = document.getElementById('ifrmYOTE');
		var innerYOTE = ifrmYOTE.contentDocument || ifrmYOTE.contentWindow.document;
		var inputYOTE = innerYOTE.getElementById('YOTEFrm');
		document.getElementById("YOTE").value=inputYOTE.value;
	}catch(e){}
	var YOTESelectVal		=	trim(document.getElementById("YOTESelectVal").value);
	var YOTE				=	document.getElementById("YOTE").value;
	var hiddenCertTypeIds 	=	document.getElementsByName("hiddenCertTypeId");
	var stateId2	=	trim(document.getElementById("stateId2").value);
    var zipCode		=	trim(document.getElementById("zipCode").value);
	var certIds =[];
	for(var i=0;i<hiddenCertTypeIds.length;i++)
	{
		certIds.push(hiddenCertTypeIds[i].value);
	}
	var epiFromDate = "";
	var epiToDate = "";
     try{
	var epiFromDate	=	trim(document.getElementById("epiFromDate").value);
    var epiToDate	=	trim(document.getElementById("epiToDate").value);
     }catch(err){}
	
     CandidateReportAjax.getInCompleteCandidateReports(jobId,firstName,lastName,emailAddress,certType,regionId,degreeId,universityId,normScoreSelectVal,
			normScore,CGPA,CGPASelectVal,contacted,candidateStatus,stateId,orderColumn, sortingOrder,totalNoOfRecord,tagsearchId,callbreakup,callNoofRecords,
			certIds,YOTESelectVal,YOTE,stateId2,zipCode,epiFromDate,epiToDate,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
    	 //prompt('',data);
		 try{
	  			document.getElementById("noOfApp5").innerHTML=data;
	  		}catch(err)
	  		  {}
	  		getWithdrawnCandidateReports();
		}});
}


function getRemoveCandidateReports()
{	
	//alert(':::::::::::::::::::::getRemoveCandidateReports:::::::::::::::');
	var jobId=document.getElementById("jobId").value;
	var firstName		=	trim(document.getElementById("firstName").value); 
	var lastName		=	trim(document.getElementById("lastName").value);
	var emailAddress	=	trim(document.getElementById("emailAddress").value);
	var certType		=	trim(document.getElementById("certificateTypeMaster").value);
	var regionId		=	trim(document.getElementById("regionId").value);
	var degreeId		=	trim(document.getElementById("degreeId").value);
	var universityId		=	trim(document.getElementById("universityId").value);
	var normScoreSelectVal		=	trim(document.getElementById("normScoreSelectVal").value);
	var normScore		=	trim(document.getElementById("normScore").value);
	var CGPA		=	trim(document.getElementById("CGPA").value);
	var CGPASelectVal		=	trim(document.getElementById("CGPASelectVal").value);
	var contacted		=	trim(document.getElementById("contacted").value);
	var candidateStatus		=	trim(document.getElementById("candidateStatus").value);
	var stateId=document.getElementById("stateId").value;
	var tagsearchId		=	trim(document.getElementById("tagsearchId").value);
	try{
		var ifrmYOTE = document.getElementById('ifrmYOTE');
		var innerYOTE = ifrmYOTE.contentDocument || ifrmYOTE.contentWindow.document;
		var inputYOTE = innerYOTE.getElementById('YOTEFrm');
		document.getElementById("YOTE").value=inputYOTE.value;
	}catch(e){}
	var YOTESelectVal		=	trim(document.getElementById("YOTESelectVal").value);
	var YOTE				=	document.getElementById("YOTE").value;
	var hiddenCertTypeIds 	=	document.getElementsByName("hiddenCertTypeId");
	var stateId2	=	trim(document.getElementById("stateId2").value);
    var zipCode		=	trim(document.getElementById("zipCode").value);
	var certIds =[];
	for(var i=0;i<hiddenCertTypeIds.length;i++)
	{
		certIds.push(hiddenCertTypeIds[i].value);
	}
	var epiFromDate = "";
	var epiToDate = "";
     try{
	var epiFromDate	=	trim(document.getElementById("epiFromDate").value);
    var epiToDate	=	trim(document.getElementById("epiToDate").value);
     }catch(err){}
	
     CandidateReportAjax.getRemoveCandidateReports(jobId,firstName,lastName,emailAddress,certType,regionId,degreeId,universityId,normScoreSelectVal,
			normScore,CGPA,CGPASelectVal,contacted,candidateStatus,stateId,orderColumn,sortingOrder,totalNoOfRecord,tagsearchId,callbreakup,callNoofRecords,
			certIds,YOTESelectVal,YOTE,stateId2,zipCode,epiFromDate,epiToDate,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
    	// alert('data::::::::::::'+data);
    	 try{
	  			document.getElementById("noOfApp4").innerHTML=data;
	  		}catch(err)
	  		  {}
	  		getInCompleteCandidateReports();
		}});
}
function getVltCandidateReports()
{	
	var jobId=document.getElementById("jobId").value;
	var firstName		=	trim(document.getElementById("firstName").value); 
	var lastName		=	trim(document.getElementById("lastName").value);
	var emailAddress	=	trim(document.getElementById("emailAddress").value);
	var certType		=	trim(document.getElementById("certificateTypeMaster").value);
	var regionId		=	trim(document.getElementById("regionId").value);
	var degreeId		=	trim(document.getElementById("degreeId").value);
	var universityId		=	trim(document.getElementById("universityId").value);
	var normScoreSelectVal		=	trim(document.getElementById("normScoreSelectVal").value);
	var normScore		=	trim(document.getElementById("normScore").value);
	var CGPA		=	trim(document.getElementById("CGPA").value);
	var CGPASelectVal		=	trim(document.getElementById("CGPASelectVal").value);
	var contacted		=	trim(document.getElementById("contacted").value);
	var candidateStatus		=	trim(document.getElementById("candidateStatus").value);
	var stateId=document.getElementById("stateId").value;
	var tagsearchId		=	trim(document.getElementById("tagsearchId").value);
	try{
		var ssn	= trim(document.getElementById("ssn").value);
	}catch(err){}
	try{
		var ifrmYOTE = document.getElementById('ifrmYOTE');
		var innerYOTE = ifrmYOTE.contentDocument || ifrmYOTE.contentWindow.document;
		var inputYOTE = innerYOTE.getElementById('YOTEFrm');
		document.getElementById("YOTE").value=inputYOTE.value;
	}catch(e){}
	var YOTESelectVal		=	trim(document.getElementById("YOTESelectVal").value);
	var YOTE				=	document.getElementById("YOTE").value;
	var hiddenCertTypeIds 	=	document.getElementsByName("hiddenCertTypeId");
	var stateId2	=	trim(document.getElementById("stateId2").value);
    var zipCode		=	trim(document.getElementById("zipCode").value);
	var certIds =[];
	for(var i=0;i<hiddenCertTypeIds.length;i++)
	{
		certIds.push(hiddenCertTypeIds[i].value);
	}
	var epiFromDate = "";
	var epiToDate = "";
     try{
	var epiFromDate	=	trim(document.getElementById("epiFromDate").value);
    var epiToDate	=	trim(document.getElementById("epiToDate").value);
     }catch(err){}

	
     CandidateReportAjax.getVltCandidateReports(jobId,firstName,lastName,emailAddress,certType,regionId,degreeId,universityId,normScoreSelectVal,
			normScore,CGPA,CGPASelectVal,contacted,candidateStatus,stateId,orderColumn, sortingOrder,totalNoOfRecord,tagsearchId,callbreakup,callNoofRecords,
			certIds,YOTESelectVal,YOTE,stateId2,zipCode,epiFromDate,epiToDate,ssn,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
    	 try{
	  			document.getElementById("noOfApp3").innerHTML=data;
	  		}catch(err)
	  		  {}
	  		getRemoveCandidateReports();
		}});
}
function getWithdrawnCandidateReports()
{	
	var jobId=document.getElementById("jobId").value;
	var firstName		=	trim(document.getElementById("firstName").value); 
	var lastName		=	trim(document.getElementById("lastName").value);
	var emailAddress	=	trim(document.getElementById("emailAddress").value);
	var certType		=	trim(document.getElementById("certificateTypeMaster").value);
	var regionId		=	trim(document.getElementById("regionId").value);
	var degreeId		=	trim(document.getElementById("degreeId").value);
	var universityId		=	trim(document.getElementById("universityId").value);
	var normScoreSelectVal		=	trim(document.getElementById("normScoreSelectVal").value);
	var normScore		=	trim(document.getElementById("normScore").value);
	var CGPA		=	trim(document.getElementById("CGPA").value);
	var CGPASelectVal		=	trim(document.getElementById("CGPASelectVal").value);
	var contacted		=	trim(document.getElementById("contacted").value);
	var candidateStatus		=	trim(document.getElementById("candidateStatus").value);
	var stateId=document.getElementById("stateId").value;
	var tagsearchId		=	trim(document.getElementById("tagsearchId").value);
	try{
		var ifrmYOTE = document.getElementById('ifrmYOTE');
		var innerYOTE = ifrmYOTE.contentDocument || ifrmYOTE.contentWindow.document;
		var inputYOTE = innerYOTE.getElementById('YOTEFrm');
		document.getElementById("YOTE").value=inputYOTE.value;
	}catch(e){}
	var YOTESelectVal		=	trim(document.getElementById("YOTESelectVal").value);
	var YOTE				=	document.getElementById("YOTE").value;
	var hiddenCertTypeIds 	=	document.getElementsByName("hiddenCertTypeId");
	var stateId2	=	trim(document.getElementById("stateId2").value);
    var zipCode		=	trim(document.getElementById("zipCode").value);
	var certIds =[];
	for(var i=0;i<hiddenCertTypeIds.length;i++)
	{
		certIds.push(hiddenCertTypeIds[i].value);
	}
	var epiFromDate = "";
	var epiToDate = "";
     try{
	var epiFromDate	=	trim(document.getElementById("epiFromDate").value);
    var epiToDate	=	trim(document.getElementById("epiToDate").value);
     }catch(err){}
	
     CandidateReportAjax.getWithdrawnCandidateReports(jobId,firstName,lastName,emailAddress,certType,regionId,degreeId,universityId,normScoreSelectVal,
			normScore,CGPA,CGPASelectVal,contacted,candidateStatus,stateId,orderColumn, sortingOrder,totalNoOfRecord,tagsearchId,callbreakup,callNoofRecords,
			certIds,YOTESelectVal,YOTE,stateId2,zipCode,epiFromDate,epiToDate,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
    	 try{
	  			document.getElementById("noOfApp6").innerHTML=data;
	  		}catch(err)
	  		  {}
	  		getHiddenCandidateReports();
		}});
}

function showSchoolList(schoolIds)
{
	schoolIdsStore=schoolIds;
	DWRAutoComplete.displaySchoolList(schoolIds,noOfRowsSchool,pageforSchool,sortOrderStrSchool,sortOrderTypeSchool,{ 
		async: false,		
		callback: function(data)
		{
			$('#myModalforSchhol').modal('show');
			document.getElementById("schoolListDiv").innerHTML=data;
			applyScrollOnTb();
		}
	});	
}
function getSortFirstGrid()
{
	$('#gridNo').val("1");
}

function getSortSecondGrid()
{
	$('#gridNo').val("2");
}

function eeocDetail(teacherId){
	//alert("teacherId=="+teacherId);
	DWRAutoComplete.getEeocDetail(teacherId,{ 
		async: false,		
		callback: function(data)
		{
			$('#cgTeacherDivMaster').modal('hide');
			$('#eeocSupportDiv').html(data);
			$('#eeocSupportModal').modal('show');
		}
	});	
	
}
function showProfilePopup(){
	$('#cgTeacherDivMaster').modal('show');
	$('#eeocSupportModal').modal('hide');
}


//save qualification Note
function saveQualificationNoteForONB(saveStatus)
{
	var teacherId = document.getElementById("teacherIdforPrint").value;
	var jobId = document.getElementById("jobIdonb").value;
	
	var note = $("#statusNoteID").val();
	$('#errorQStatus').empty();
	if(note==''){
		$('#errorQStatus').show();
		$('#errorQStatus').append("&#149; "+resourceJSON.PlzEtrNotes+".<br>");
	}else{
		CandidateGridAjaxNew.saveQualificationNoteForONB(teacherId,note,saveStatus,jobId,{ 
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{
				$('#qualificationDiv').modal('hide');
				displayHiredCandidate();
			}
		});
	}
}

function checkSpViolatedForSpReset(teacherId)
{
	CGServiceAjax.checkSpCompleted(teacherId,{
		async: false,
		errorHandler:handleError,
		callback: function(data){
			if(data==1)
			{
				$('#message2show').html(resourceJSON.msgCompletedSpInventory);
				$('#myModal2').modal('show');
				return false;
			}
			else if(data==3)
			{
				makeSpIncomplete(teacherId);
				return false;
			}
			else
			{
				$('#message23show').html(resourceJSON.msgCandidateHasNotCompleteSp);
				$('#myModal23').modal('show');
				return false;
			}
		}
	});
}
var teacherIdForResetSp = '';
function makeSpIncomplete(teacherId)
{
	$("#cgTeacherDivMaster").modal('hide');
	$('#btnResetSp').attr("onclick", "makeIncompleteSp("+teacherId+")");
	document.getElementById("teacherIdForResetSp").value = teacherId;
	teacherIdForResetSp = teacherId;
	$('#errordivResetSp').empty();
	$('#resetSpDescription').find(".jqte_editor").css("background-color", "");
	$('#resetSpDescription').find(".jqte_editor").html("");
	$('#modalResetSp').modal('show');
}

function makeIncompleteSp(teacherId)
{	
	$('#errordivResetSp').empty();
	$('#resetSpDescription').find(".jqte_editor").css("background-color", "");
	var teacherDetail = {teacherId:teacherId};
	if ($('#resetSpDescription').find(".jqte_editor").text().trim()=="")
	{
		$('#errordivResetSp').append("&#149; "+resourceJSON.msgResetSp+"<br>");
		$('#resetSpDescription').find(".jqte_editor").focus();
		$('#resetSpDescription').find(".jqte_editor").css("background-color", "#F5E7E1");
		return false;
	}
	else
	{
		var spResetMsg = trim($('#resetSpDescription').find(".jqte_editor").html());
		$('#modalResetSp').modal('hide');
		$('#loadingDiv').show();
		CGServiceAjax.makeSpIncomplete(teacherDetail,spResetMsg,{ 
			async: true,
			callback: function(data){
			$('#loadingDiv').hide();
			$('#resetMsg2Show').html(resourceJSON.msgSpStatusIncomp1+" "+data+"'s "+resourceJSON.msgSpStatusIncomp2);
			$('#modalResetSpMsg').modal('show');
		},
		errorHandler:handleError  
		});
	}
}

function closeResetSp()
{
	$('#modalResetSp').modal('hide');
	$('#modalResetSpMsg').modal('hide');
	showProfileContent(this,spJobforteacherId,spTeacherId,spNormscore,spJobId,spColorName,spNoOfRecordCheck,spEvent);
}



function getTeacherLicenceGrid_DivProfile(teacherId){
	TeacherProfileViewInDivAjax.displayRecordsBySSNForDate(teacherId,{ 
		async: true,
		callback: function(data)
		{	
		//alert(data);
		if(data==null||data=="")
		{				
		ssnNotFound("No License data found for your Social Security");
		return false;
		}
		else
		{
			//	$('#licenseDivDate').show();
				$('#divLicneseGridCertificationsGridDate').html(data);			
			//    $('#loadingDiv').hide();
			   applyScrollOnTblLicenseDate();
		}
		
		},
	errorHandler:handleError
	});
}

function getLEACandidatePortfolio_DivProfile(teacherId){
	
	TeacherProfileViewInDivAjax.getLEACandidatePortfolio(teacherId,{ 
		async: true,
		callback: function(data)
		{	
		//alert(data);
		if(data==null||data=="")
		{				
		ssnNotFound("No License data found for your Social Security");
		return false;
		}
		else
		{
			//	$('#licenseDivDate').show();
				$('#divLEACandidatePotfolioDiv').html(data);			
			//    $('#loadingDiv').hide();				
				applyScrollOnLEACandidatePorfolio();
		}
		
		},
	errorHandler:handleError
	});
}
function showPopUpHideTalent()
{
	$('#hideTalentDiv').modal('show');
}
function cancelPopUpHideTalent()
{
	$('#hideTalentDiv').modal('hide');
}
function hideTheseTalents()
{
	var jobForTeacherIds = new Array();
	jobForTeacherIds	=	getSendMessageTeacherIds();
	CandidateGridAjaxNew.hideTalent(jobForTeacherIds,
			{
				async: false,
				errorHandler:handleError,
				callback:function(data)
				{
		           $('#hideTalentDiv').modal('hide');
		           $('#hideTalentSuccessDiv').modal('show');
		           setTimeout(function() {
		        	   window.location.reload();
		        	}, 2000);
				}
			});
}
function showPopUpUnhideTalent()
{
	$('#hideTalentDiv').modal('hide');
	$('#unhideTalentDiv').modal('show');
	
}
function cancelPopUpUnhideTalent()
{
	$('#unhideTalentDiv').modal('hide');
}
function cancelPopUpUnhideTalentMessage()
{
	$('#unhideTalentMessageDiv').modal('hide');
}
function unhideTheseTalents(save)
{
	var jobForTeacherIds = new Array();
	jobForTeacherIds	=	getSendMessageTeacherIds();
	CandidateGridAjaxNew.unhideTalent(jobForTeacherIds,save,
			{
				async: false,
				errorHandler:handleError,
				callback:function(data)
				{
		          if(data==1)
		          {
		        	  $('#unhideTalentMessageDiv').modal('show');  
		          }
		
		          if(data==2)
		          {	        	  
		           $('#hideTalentDiv').modal('hide');
		           $('#unhideTalentMessageDiv').modal('hide');
		           $('#unhideTalentSuccessDiv').modal('show');
		           setTimeout(function() {
		        	   window.location.reload();
		        	}, 2000);
		           
		          }
		           
				}
			});
}
function getHiddenCandidateGrid()
{	
	$('#HiddenWait').show();
	var jobId=document.getElementById("jobId").value;
	var firstName		=	trim(document.getElementById("firstName").value); 
	var lastName		=	trim(document.getElementById("lastName").value);
	var emailAddress	=	trim(document.getElementById("emailAddress").value);
	var certType		=	trim(document.getElementById("certificateTypeMaster").value);
	var regionId		=	trim(document.getElementById("regionId").value);
	var degreeId		=	trim(document.getElementById("degreeId").value);
	var universityId		=	trim(document.getElementById("universityId").value);
	var normScoreSelectVal		=	trim(document.getElementById("normScoreSelectVal").value);
	var normScore		=	trim(document.getElementById("normScore").value);
	var CGPA		=	trim(document.getElementById("CGPA").value);
	var CGPASelectVal		=	trim(document.getElementById("CGPASelectVal").value);
	var contacted		=	trim(document.getElementById("contacted").value);
	var candidateStatus		=	trim(document.getElementById("candidateStatus").value);
	var stateId=document.getElementById("stateId").value;
	var tagsearchId		=	trim(document.getElementById("tagsearchId").value);
	try{
		var ifrmYOTE = document.getElementById('ifrmYOTE');
		var innerYOTE = ifrmYOTE.contentDocument || ifrmYOTE.contentWindow.document;
		var inputYOTE = innerYOTE.getElementById('YOTEFrm');
		document.getElementById("YOTE").value=inputYOTE.value;
	}catch(e){}
	var YOTESelectVal		=	trim(document.getElementById("YOTESelectVal").value);
	var YOTE				=	document.getElementById("YOTE").value;
	var hiddenCertTypeIds 	=	document.getElementsByName("hiddenCertTypeId");
	var stateId2	=	trim(document.getElementById("stateId2").value);
    var zipCode		=	trim(document.getElementById("zipCode").value);
	var certIds =[];
	for(var i=0;i<hiddenCertTypeIds.length;i++)
	{
		certIds.push(hiddenCertTypeIds[i].value);
	}
	var epiFromDate = "";
	var epiToDate = "";
     try{
	var epiFromDate	=	trim(document.getElementById("epiFromDate").value);
    var epiToDate	=	trim(document.getElementById("epiToDate").value);
     }catch(err){}
	
	CandidateGridAjaxNew.getHiddenCandidateReportGrid(jobId,firstName,lastName,emailAddress,certType,regionId,degreeId,universityId,normScoreSelectVal,
			normScore,CGPA,CGPASelectVal,contacted,candidateStatus,stateId,orderColumn, sortingOrder,totalNoOfRecord,tagsearchId,callbreakup,callNoofRecords,
			certIds,YOTESelectVal,YOTE,stateId2,zipCode,epiFromDate,epiToDate,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
		document.getElementById("divHiddenCGGrid").innerHTML=data;
		
		
		var table="divHiddenCGGrid";
		var listLength=document.getElementById("hiddenListSize").value;
		if(callbreakup)
			if(listLength>1)
				getBreakUpRecords(table,listLength,'hiddentr_',$("[name='sessionListNameHidden"+jobId+"']").val(),1);
		
			
			displayTotalCGRecord(8,0);
			var startPoint=totalNoOfRecord-sectionRecordHidden;
			for(var j=startPoint;j<=totalNoOfRecord;j++)
			{	
				$('#tpResume'+j).tooltip();
				$('#tpPortfolio'+j).tooltip();
				$('#tpJSA'+j).tooltip();
				$('#hireTp'+j).tooltip();
				$('#tpPDReport'+j).tooltip();
				$('#tpCL'+j).tooltip();
				$('#tpAct'+j).tooltip();
				$('#tpMsg'+j).tooltip();
				$('#tpNotes'+j).tooltip();		
				$('#tpTrans'+j).tooltip();
				$('#tpCert'+j).tooltip();
				$('#tpMessage'+j).tooltip();
				$('#tpPhone'+j).tooltip();
				$('#tpInv'+j).tooltip();
				$('#tprr'+j).tooltip();	
				$('#ftsbu'+j).tooltip();
				$('#tooltipPreview'+j).tooltip()
				$('#tpref'+j).tooltip();
				$('#teacherName_'+j).tooltip();
				$('#i4Ques'+j).tooltip();
				$('#teacher_PNQ'+j).tooltip();
				try { $('#alum'+j).tooltip(); } catch (e) {}
				try{ $('#tpInternalCandidate'+j).tooltip(); }catch(e){}
				try{ $('.formerEmployee').tooltip(); }catch(e){}
				try{ $('.returedEmployee').tooltip(); }catch(e){}
				try{$('#tpRefChk'+j).tooltip(); }catch(e){}
				try{$('#dtFailed'+j).tooltip(); }catch(e){}
				try{$('#tpAssessmentinvite'+j).tooltip(); }catch(e){}
				try{$('#pc'+j).tooltip(); }catch(e){}
				try{$('#cndNotReviewed'+j).tooltip(); }catch(e){}
				var candidateFlag=document.getElementById("candidateflag").value;
				if(candidateFlag==1)
				{
					var noOfTagsFile=document.getElementById("noOfTagsFile").value;
					if(noOfTagsFile>15){
						iWidth=600;
					}
					else
					{
						iWidth=300;
					}
				}
				else
				{
					iWidth=300;
				}
				$('#tag'+j).popover({ 
				    html : true,
				    placement: 'right',
				    content: $('#tagDiv'+j).html(),
				  }).on("mouseenter", function () {
					  var height=setDialogPosition();
					  height=height+80;
					  	var style = $('<style>.popover { position: absolute;top: '+height+'px !important;left: 0px;width: '+iWidth+'px; }</style>')
						$('html > head').append(style);
					  	
				        var _this = this;
				        $(this).popover("show");
				        $(this).siblings(".popover").on("mouseleave", function () {
				            //$(_this).popover('hide');
				        });
				    }).on("mouseleave", function () {
				        var _this = this;
				        setTimeout(function () {
				            if (!$(".popover:hover").length) {
				                //$(_this).popover("hide")
				            }
				        }, 100);
				    });
			}
			
			getHiddenScoreDetails();
			
		}});
}
function hideHiddenCandidateGrid()
{		
	document.getElementById("divHiddenCGGrid").innerHTML="<div id='HiddenWait' style='display:none;text-align:center;padding-top:0px;'><img src='images/loadingAnimation.gif' /> <br>"+resourceJSON.MsgLoad+"</div>";
}
function getHiddenScoreDetails()
{
	var expandchk=document.getElementById("expandchk").value;
	var sectionRecordHidden;
	if(expandchk >= 1)
	{
		$( ".tdscore0_hidden:first" ).animate({
		    left :0,
			 width:256
		  }, {
		    duration: 1000
		  });
		try {
		document.getElementById("tdscore0tbl_hidden").style.textIndent="0px";
		} catch (e) {
			// TODO: handle exception
		}
		for(var i=1;i<=sectionRecordHidden;i++)
		{
			try {
				document.getElementById("tdscore_hidden"+i).style.display="inline";
			} catch (e) {
				// TODO: handle exception
			}
			
		}
	}
	else
	{
		 $( ".tdscore0_hidden:first" ).animate({
			    left :0,
				 width:1
			  }, {
			    duration: 1000
			  });
		
		 try {
			 document.getElementById("tdscore0tbl_hidden").style.textIndent="-99999px";
		 } catch (e) {
				// TODO: handle exception
			}
		for(var i=1;i<=sectionRecordHidden;i++)
		{
			try {
				document.getElementById("tdscore_hidden"+i).style.display="none";
			} catch (e) {
				// TODO: handle exception
			}
			
			
		}
	}
}
function checkAllCandidateHidden(){
	var flag = document.getElementById("checkAllCandidateHidden").checked;
   	
	 if(flag==true)
	 { 
		$('#divHiddenCGGrid input[name="cgCBX"]').each(function() {
			 $(this).attr('checked', 'checked');
			 checkActionMenu();
		});
	 }
	 else{
		 
		 $('#divHiddenCGGrid input[name="cgCBX"]').each(function() {
			 $(this).prop('checked', false);
			 checkActionMenu();
				});
	 }
}
function getHiddenCandidateReports()
{	
	var jobId=document.getElementById("jobId").value;
	var firstName		=	trim(document.getElementById("firstName").value); 
	var lastName		=	trim(document.getElementById("lastName").value);
	var emailAddress	=	trim(document.getElementById("emailAddress").value);
	var certType		=	trim(document.getElementById("certificateTypeMaster").value);
	var regionId		=	trim(document.getElementById("regionId").value);
	var degreeId		=	trim(document.getElementById("degreeId").value);
	var universityId		=	trim(document.getElementById("universityId").value);
	var normScoreSelectVal		=	trim(document.getElementById("normScoreSelectVal").value);
	var normScore		=	trim(document.getElementById("normScore").value);
	var CGPA		=	trim(document.getElementById("CGPA").value);
	var CGPASelectVal		=	trim(document.getElementById("CGPASelectVal").value);
	var contacted		=	trim(document.getElementById("contacted").value);
	var candidateStatus		=	trim(document.getElementById("candidateStatus").value);
	var stateId=document.getElementById("stateId").value;
	var tagsearchId		=	trim(document.getElementById("tagsearchId").value);
	try{
		var ifrmYOTE = document.getElementById('ifrmYOTE');
		var innerYOTE = ifrmYOTE.contentDocument || ifrmYOTE.contentWindow.document;
		var inputYOTE = innerYOTE.getElementById('YOTEFrm');
		document.getElementById("YOTE").value=inputYOTE.value;
	}catch(e){}
	var YOTESelectVal		=	trim(document.getElementById("YOTESelectVal").value);
	var YOTE				=	document.getElementById("YOTE").value;
	var hiddenCertTypeIds 	=	document.getElementsByName("hiddenCertTypeId");
	var stateId2	=	trim(document.getElementById("stateId2").value);
    var zipCode		=	trim(document.getElementById("zipCode").value);
	var certIds =[];
	for(var i=0;i<hiddenCertTypeIds.length;i++)
	{
		certIds.push(hiddenCertTypeIds[i].value);
	}
	var epiFromDate = "";
	var epiToDate = "";
     try{
	var epiFromDate	=	trim(document.getElementById("epiFromDate").value);
    var epiToDate	=	trim(document.getElementById("epiToDate").value);
     }catch(err){}
	
     CandidateReportAjax.getHiddenCandidateReports(jobId,firstName,lastName,emailAddress,certType,regionId,degreeId,universityId,normScoreSelectVal,
			normScore,CGPA,CGPASelectVal,contacted,candidateStatus,stateId,orderColumn, sortingOrder,totalNoOfRecord,tagsearchId,callbreakup,callNoofRecords,
			certIds,YOTESelectVal,YOTE,stateId2,zipCode,epiFromDate,epiToDate,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
    	 try{
	  			document.getElementById("hiddenTotal").innerHTML=data;
	  		}catch(err)
	  		  {}
		}});
}

//---------------------- assessment reset timed out to incomplete start ----------------------
var teacherIdForResetAssessment = '';
function checkViolatedForAssessmentReset(teacherId,assessmentType,teacherAssessmentStatusId)
{
	CGServiceAjax.checkAssessmentCompleted(teacherId,assessmentType,teacherAssessmentStatusId,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){
			if(data==1){
				if(assessmentType==1){
					$('#message2show').html(resourceJSON.msgcompletedBaseInventory);
				}else if(assessmentType==2){
					$('#message2show').html(resourceJSON.msgCompletedJsiInventory);
				}else if(assessmentType==3){
					$('#message2show').html(resourceJSON.msgCompletedSpInventory);
				}else if(assessmentType==4){
					$('#message2show').html(resourceJSON.msgCompletedIpiInventory);
				}
				$('#myModal2').modal('show');
				return false;
			}
			else if(data==3){
				$("#cgTeacherDivMaster").modal('hide');
				$('#btnResetAssessment').attr("onclick", "makeAssessmentIncomplete("+teacherId+","+assessmentType+","+teacherAssessmentStatusId+")");
				document.getElementById("teacherIdForResetAssessment").value = teacherId;
				teacherIdForResetAssessment = teacherId;
				$('#errordivResetAssessment').empty();
				$('#resetAssessmentDescription').find(".jqte_editor").css("background-color", "");
				$('#resetAssessmentDescription').find(".jqte_editor").html("");
				$('#modalResetAssessment').modal('show');
				return false;
			}
			else{
				if(assessmentType==1){
					$('#message23show').html(resourceJSON.CandidateHasNotComplete);
				}else if(assessmentType==2){
					$('#message23show').html(resourceJSON.msgCandidateHasNotCompleteJsi);
				}else if(assessmentType==3){
					$('#message23show').html(resourceJSON.msgCandidateHasNotCompleteSp);
				}else if(assessmentType==4){
					$('#message23show').html(resourceJSON.msgCandidateHasNotCompleteIpi);
				}
				$('#myModal23').modal('show');
				return false;
			}
		}
	});	

}

function makeAssessmentIncomplete(teacherId,assessmentType,teacherAssessmentStatusId)
{
	$('#errordivResetAssessment').empty();
	$('#resetAssessmentDescription').find(".jqte_editor").css("background-color", "");
	var teacherDetail = {teacherId:teacherId};
	if ($('#resetAssessmentDescription').find(".jqte_editor").text().trim()=="")
	{
		$('#errordivResetAssessment').append("&#149; "+resourceJSON.msgResetSp+"<br>");
		$('#resetAssessmentDescription').find(".jqte_editor").focus();
		$('#resetAssessmentDescription').find(".jqte_editor").css("background-color", "#F5E7E1");
		return false;
	}
	else
	{
		var resetMsg = trim($('#resetAssessmentDescription').find(".jqte_editor").html());
		$('#modalResetAssessment').modal('hide');
		$('#loadingDiv').show();
		CGServiceAjax.makeAssessmentIncomplete(teacherDetail,assessmentType,teacherAssessmentStatusId,resetMsg,{
			async: false,
			errorHandler:handleError,
			callback: function(data){
				$('#loadingDiv').hide();
				$('#resetAssessmentMsg2Show').html(resourceJSON.msgSpStatusIncomp1+" "+data+"'s "+resourceJSON.msgSpStatusIncomp2);
				$('#modalResetAssessmentMsg').modal('show');
			}
		});
	}
}

function closeAssessmentResetDiv()
{
	$('#modalResetAssessment').modal('hide');
	$('#modalResetAssessmentMsg').modal('hide');
	showProfileContent(this,assessmentJobforteacherId,assessmentTeacherId,assessmentNormscore,assessmentJobId,assessmentColorName,assessmentNoOfRecordCheck,assessmentEvent);
}

function checkPhone(){
	 $('[data-toggle="tooltip"]').tooltip();   
}
//---------------------- assessment reset timed out to incomplete end ----------------------

//Added for transcript Verification
function showVerifyTranscript(){
	$('#errorStatusVerifyTranscript').empty();
	var counter = 0;
	var transcript_data	=	document.getElementsByName("transcript_data[]");
	for(var i=0; i < transcript_data.length; i++){
		if(transcript_data[i].checked == true){			
			counter++;
		}
	}
	//alert("counter== "+counter);

	if(counter > 0){ 
		$('textarea').jqte();
		document.getElementById("academicGridAcademicNotes").style.display="block";
	} else {
		$('#academicGridAcademicNotes').hide();
	}
}

function verifyTranscript(teacherId,candidateFullName){
	document.getElementById("myModalLabelTrans").innerHTML = ""+resourceJSON.MsgTranscriptfor+" "+candidateFullName;
	TeacherProfileViewInDivAjax.verifyTranscript(teacherId,noOfRowsForTVerify,pageForTVerify,sortOrderStrForTVerify,sortOrderTypeForTVerify,currentPageFlag,{
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			$("#cgTeacherDivMaster").modal('hide');
			$("#draggableDivMaster").modal('hide');
			$('.verifyTranscriptDiv').modal('show');
			document.getElementById("divVerifyTrans").innerHTML=data;
			applyScrollOnTblVerifyTranscript();
		}});
}

function updateTranscript(status){
	$('#errorStatusVerifyTranscript').empty();
	var counter		= 	0;
	var AcademicIDs	=	"";
	var graduationCounter	=	0;
	var transcript_data	= 	document.getElementsByName("transcript_data[]");
	var teacherId 		=	document.getElementById("teacherTranscript").value
	for(var i=0; i < transcript_data.length; i++){

		if(transcript_data[i].checked == true){
			graduationDate	=	document.getElementById("graduationDate_"+transcript_data[i].value).value;
			AcademicIDs	 	=	AcademicIDs+"##"+transcript_data[i].value;
			
			if(status == "V" && (graduationDate == "" || graduationDate == null)){
				if(counter == 0){
					$('#errorStatusVerifyTranscript').append("&#149; "+resourceJSON.MsgDegreeConferredDate+".<br>");
					$("#graduationDate_"+transcript_data[i].value).focus();
					$('#errorStatusVerifyTranscript').show();
				}

				counter++;
			}else if(status == "V" && (graduationDate != "" || graduationDate != null)){
				if(counter == 0){
					saveGraduationDate(transcript_data[i].value);
					if (isDate(graduationDate)==false){
						graduationCounter	=	graduationCounter+1;
					}
				}
			}
		}
	}
	if(counter == 0 && graduationCounter == 0){
		TeacherProfileViewInDivAjax.updateTranscript(status,teacherId,AcademicIDs,{
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{
				$('.verifyTranscriptDiv').modal('hide');
				$('#messageTranscriptShow').html("Transcript Verified Successfully");
				$('#myModaTranscript').modal('show');
				
			}});
	}
}

function hideDivTranscript(id){
	$('.verifyTranscriptDiv').modal('hide');
	$("#cgTeacherDivMaster").modal('show');
	$("#draggableDivMaster").modal('show');
}

function disconnectTalent(){
	//alert(" disconnectTalent ");
	$("#errorDiv").empty();
	$("#errorDiv").hide();
	var teacherId="";
	var newksnId ="";
	newksnId = trim(document.getElementById("inputKSN").value);
	//alert("newksnId :: "+newksnId)
	teacherId = document.getElementById("disconnectTalentId").value;
	var errorCount = 0;
	if(newksnId.length==0){
		$("#inputKSN").focus();
		$("#errorDiv").show();
		$('#errorDiv').append("&#149; Please enter KSN ID<br>");
		errorCount++;
	}

	if(errorCount==0)
		//CandidateGridSubAjax.disconnectQueue(teacherId,newksnId, {
		ManageStatusAjax.disconnectQueue(teacherId,newksnId, {
		preHook:function(){$('#loadingDiv').show()},
		postHook:function(){$('#loadingDiv').hide()},
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			//alert(data);
			var result = data.split("###");
			if(result.length>0){
				if(result[0]=="error"){
					$("#inputKSN").focus();
					$("#errorDiv").show();
					$('#errorDiv').append(result[1]);
				}
				else{
					if(data.length>0){
						$("#cgTeacherDivMaster").modal("hide");
						$("#disconnectResponse").modal("show");
						$("#disconnectRespMsg").text(result[0]+": "+result[1]);
					}
				}
			}
		}
	});	

}

function hideDisconnectResponse()
{
	$("#disconnectResponse").modal("hide");
	$("#cgTeacherDivMaster").modal("show");
	//$("#tpDisconnect").hide();
	//$("#inputKSN").val("");
	showLinkToKSNResponse(document.getElementById("disconnectTalentId").value);
}

function checkForInt(evt) {
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==8)
		return true;
	return( (charCode >= 48 && charCode <= 57)||(charCode==13));
}

function changeonspecificrows(teacherIds){
	if(teacherIds.length>0){
		CandidateGridSubAjax.updateAppliedJobsonCG(teacherIds, { 
			async:true,
			errorHandler:handleError,
			callback:function(data)
			{
				if(data!=null){
					for(var i=0;i<data.length;i++){
						var subpart = data[i].split("###");
						$("#appjob"+subpart[0]).html(subpart[1]);
					}
				}
			}
		});
	}
}

function showLinkToKSNResponse(teacherId){
	ManageStatusAjax.getLinktoKSNResponse(teacherId,{ 
		async: true,
		callback: function(data){
		$("#ksndetails").empty();
		$("#ksndetails").html(data);
},
errorHandler:handleError  
});
}


function getJobInformationHeaderCG(){
	$('#loadingDiv').show();
	$('#loaddiv').hide();
	var jobId=document.getElementById("jobId").value;
	
		CandidateGridSubAjax.getJobInformationHeaderCG(jobId, { 
			async:true,
			errorHandler:handleError,
			callback:function(data)
			{
				if(data!=null){
					//console.log(data);
					$(".tableContentJobHeader").html(data);
					$('#loadingDiv').hide();
					$('#loaddiv').show();
				}
			}
		});
}

