var page = 1;
var noOfRows = 10;
var sortOrderStr="";
var sortOrderType="";
var txtBgColor="#F5E7E1";

var pageForTC = 1;
var sortOrderStrForTC="";
var sortOrderTypeForTC="";
var noOfRowsForTC = 100;
var teacherId='';
var currentPageFlag="";


var orderColumn = "candidateScore";
var sortingOrder = "1";

function getPaging(pageno)
{
	if(pageno!='')
	{
		page=pageno;	
	}
	else
	{
		page=1;
	}
	
	noOfRows = document.getElementById("pageSize3").value;
	teacherId = document.getElementById("teacherId").value;
	getNotesDiv(teacherId);
}

function getPagingAndSorting(pageno,sortOrder,sortOrderTyp){
	if(currentPageFlag=="tran" || currentPageFlag=="cert"){
		if(pageno!=''){
			pageForTC=pageno;	
		}else{
			pageForTC=1;
		}
		sortOrderStrForTC	=	sortOrder;
		sortOrderTypeForTC	=	sortOrderTyp;
		if(document.getElementById("pageSize3")!=null){
			noOfRowsTranCert = document.getElementById("pageSize3").value;
		}else{
			noOfRowsTranCert=100;
		}
		if(currentPageFlag=="tran"){
			getTranscriptGrid();
		}else{
			getCertificationGrid();
		}
	}else{
		if(pageno!=''){
			page=pageno;	
		}else{
			page=1;
		}
		sortOrderStr	=	sortOrder;
		sortOrderType	=	sortOrderTyp;
		if(document.getElementById("pageSize3")!=null){
			noOfRows = document.getElementById("pageSize3").value;
		}else{
			noOfRows=10;
		}
		teacherId = document.getElementById("teacherId").value;
		getNotesDiv(teacherId);
	}
}

function openWindow(url,windowName,width,height)
{
	windowName=windowName||"_blank";width=width||900;height=height||600;
	var left=(window.screen.availWidth-width)/2;
	var top=(window.screen.availHeight-height)/2;
	var mywindow=window.open(url,windowName,'width='+width+',height='+height+',left='+left+',top='+top+',scrollbars=1');
	var len;

	try{
		len=mywindow.length;
	}catch(err){}


	/*if (!mywindow || mywindow.closed || typeof mywindow == 'undefined' || typeof mywindow.closed == 'undefined') {
       alert("dd");
		return true;
    }*/

	//alert(len);


	if ((!mywindow) || len==undefined || mywindow.length!=0 ){
		{
			//alert("A popup blocker has been detected, please disable your popup blocker or click the link to open the new window.");
			//alert("Pop-up window is blocked on this browser. Please allow pop-ups for this site to view this report.");
			$('#blockMessage').html(""+resourceJSON.PopUpWindowBlock+" <a target = '_blank' href='"+url+"'>"+resourceJSON.BtnClick+"</a> "+resourceJSON.ToOpenNewWindow+".");
			$('#myModal').modal('show');
		}
	}else
		mywindow.focus();

	//for chrome only
	if (navigator && (navigator.userAgent.toLowerCase()).indexOf("chrome") > -1) {
		if (!mywindow)
		{
			//alert("Pop-up window is blocked on this browser. Please allow pop-ups for this site to view this report.");
			$('#blockMessage').html(""+resourceJSON.PopUpWindowBlock+" <a target = '_blank' href='"+url+"'>"+resourceJSON.BtnClick+"</a> "+resourceJSON.ToOpenNewWindow+".");
			$('#myModal').modal('show');
		}
		else {
			mywindow.onload = function() {
				setTimeout(function() {
					if (mywindow.screenX === 0) {
						//alert("Pop-up window is blocked on this browser. Please allow pop-ups for this site to view this report.");
						$('#blockMessage').html(""+resourceJSON.PopUpWindowBlock+" <a target = '_blank' href='"+url+"'>"+resourceJSON.BtnClick+"</a> "+resourceJSON.ToOpenNewWindow+".");
						$('#myModal').modal('show');
						mywindow.close();
					}
				}, 0);
			};
		}
	}
}

function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert("Oops: Your Session has expired!");  document.location = 'signin.do';}
	else{alert("Server Error: "+exception.javaClassName);}
}

function orderingCG(oColumn, sOrder){
	orderColumn = oColumn;
	sortingOrder = sOrder;
	getCandidateGrid();
}

function getCandidateGrid()
{		
	$('#loadingDiv').show();
	var jobId=document.getElementById("jobId").value;
	CandidateReportAjax.getCandidateReportGrid(jobId, orderColumn, sortingOrder,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#loadingDiv').hide();
			document.getElementById("divReportGrid").innerHTML=data.toString();
			tpJbIDisable();
			tpJbIEnable();
			displayTotalCGRecord();
		}});
}
function displayTotalCGRecord()
{
	document.getElementById('totalCGRecordDiv').innerHTML="<b>"+resourceJSON.NumOfCandidates+": </b>"+document.getElementById('totalCGRecord').value+""; 
}
function hireTeacher(assessmentStatusId, jobForTeacherId)
{	
	CandidateReportAjax.hireTeacher(assessmentStatusId, jobForTeacherId, { 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			if(data)
			{
				$('#myModalAct').modal('hide');
				getCandidateGrid();				
			}
			else
			{
				$('#myModalAct').modal('hide');
				$("#myModalAct").css({"z-index":"1000"});
				document.getElementById("divAlertText").innerHTML=""+resourceJSON.MsgNotHireTheCandidate+".";
				$('#divAlert').modal('show');
				
			}			
		}});	
}
function unHireTeacher(assessmentStatusId, jobForTeacherId)
{	
	CandidateReportAjax.unHireTeacher(assessmentStatusId, jobForTeacherId,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			if(data)
			{
				$('#myModalAct').modal('hide');
				getCandidateGrid();				
			}
			else
			{
				$('#myModalAct').modal('hide');			
				$("#myModalAct").css({"z-index":"1000"});
				document.getElementById("divAlertText").innerHTML=""+resourceJSON.MsgCannotUnHired+".";
				$('#divAlert').modal('show');
			}		
	}});
}
function removeTeacher(assessmentStatusId, jobForTeacherId)
{
	var divMsg="You have not completed";
	var divMsg1="You have not completed....";	
	$('#myModalAct').modal('hide');
	$("#myModalAct").css({"z-index":"1000"});
	$('#myConfirm').modal('show');
	
	document.getElementById("spnRemoveTeacher").innerHTML="<button class='btn btn-large btn-primary' onclick=\"removeTeacherAfterConfirm('"+assessmentStatusId+"','"+jobForTeacherId+"')\" >"+resourceJSON.btnOk+" <i class='icon'></i></button>";
}

function setZIndexActDiv()
{
	$('#myConfirm').modal('hide');
	$("#myModalAct").css({"z-index":"3000"});
	$('#myModalAct').modal('show');
}

function setZIndexActOtherDiv()
{
	$("#myModalAct").css({"z-index":"3000"});
}

function removeTeacherAfterConfirm(assessmentStatusId, jobForTeacherId){
	
	CandidateReportAjax.removeTeacher(assessmentStatusId, jobForTeacherId, { 
	async: true,
	errorHandler:handleError,
	callback:function(data)
	{
		$('#myConfirm').modal('hide');
		$('#myModalAct').modal('hide');		
		getCandidateGrid();			
		
	}});	
}
function test(){
	
}
function downloadCandidateGridReport()
{
	$('#loadingDiv').show();
	var jobId = document.getElementById("jobId").value;
	CandidateReportAjax.downloadCandidateGridReport(jobId,{ 
	async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			$('#loadingDiv').hide();
			//window.open(data);
			//openWindow(data,'Support',650,490);
			document.getElementById("hrefPDF").href = data; 
			return false;
		}});
}
function downloadResume(teacherId,linkId)
{
	$('#loadingDiv').show();
	CandidateReportAjax.downloadResume(teacherId,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			
			$('#loadingDiv').hide();
			if(data=="")			
				alert(""+resourceJSON.ResumeNotUploaded+"")
			else				
			{
				if (data.indexOf(".doc") !=-1) {
				    document.getElementById('ifrmTrans').src = ""+data+"";
				}
				else
				{
					//window.open(data);
					document.getElementById(linkId).href = data; 
					return false;
				}
			}
			
			
		}
	});
}

function downloadPortfolioReport(teacherId, linkId)
{	
	$('#loadingDiv').show();
	CandidateReportAjax.downloadPortfolioReport(teacherId,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#loadingDiv').hide();
			if(data=="")
			{
					document.getElementById(linkId).href = "javascript:void(0)";
					alert(""+resourceJSON.MsgPortfolioNotCompleted+"")
					return true;
			}
			else	
			{
				
				//window.location.href=data;
  				//window.focus();
				document.getElementById(linkId).href = data; 
				return false;
			}
				
		}
	});
}

function showCoverLetter(teacherId, jobId)
{	
	
	CandidateReportAjax.getCoverLetter(teacherId,jobId,
	{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{	
			
			if(data=="")
			{
				alert(""+resourceJSON.CandidateNotProvidedCoverLtr+"")
			}
			else
			{
				document.getElementById("lblCL").innerHTML=""+data;
				$('#myModalCoverLetter').modal('show');				
			}		
		}
	});
}



function getNotesDiv(teacherId,jobId)
{
	currentPageFlag='notes';
	document.getElementById("teacherId").value=teacherId;	
	$('#divTxtNode').find(".jqte_editor").html("");		
	$('#myModalNotes').modal('show');	
	$('#divTxtNode').find(".jqte_editor").css("background-color", "");
	document.getElementById("noteId").value="";
	$('#errordivNotes').empty();
	
	CandidateReportAjax.getNotesDetail(teacherId,page,noOfRows,sortOrderStr,sortOrderType,
	{
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{	
			document.getElementById("divNotes").innerHTML=data;
			applyScrollOnNotesTbl();
		}
	});
}
function getMessageDiv(teacherId,emailId,jobId)
{
	document.getElementById("teacherDetailId").value=teacherId;	
	document.getElementById("emailId").value=emailId;
	document.getElementById("jobId").value=jobId;
	document.getElementById("emailDiv").innerHTML=emailId;
	$('#messageSend').find(".jqte_editor").html("");
	document.getElementById("messageSubject").value="";
	$('#myModalMessage').modal('show');	
	$('#messageSubject').css("background-color", "");
	$('#messageSend').find(".jqte_editor").css("background-color", "");
	$('#messageSubject').focus();
	$('#errordivMessage').empty();
}
function validateMessage()
{
	$('#messageSubject').css("background-color", "");
	$('#messageSend').find(".jqte_editor").css("background-color", "");
	var messageSend = $('#messageSend').find(".jqte_editor").html();
	var messageSubject = document.getElementById("messageSubject").value;
	var teacherId = document.getElementById("teacherDetailId").value;
	var jobId = document.getElementById("jobId").value;
	$('#errordivMessage').empty();
	
	var cnt=0;
	var focs=0;
		if(trim(messageSubject)=="")
		{
			$('#errordivMessage').append("&#149; "+resourceJSON.PlzEtrSub+".<br>");
			if(focs==0)
				$('#messageSubject').focus();
			$('#messageSubject').css("background-color", "#F5E7E1");
			cnt++;focs++;
		}
		if ($('#messageSend').find(".jqte_editor").text().trim()==""){

			$('#errordivMessage').append("&#149; "+resourceJSON.PlzEtrMsg+".<br>");
			if(focs==0)
				$('#messageSend').find(".jqte_editor").focus();
			$('#messageSend').find(".jqte_editor").css("background-color", "#F5E7E1");
			cnt++;focs++;
		}else{
			var charCount=$('#messageSend').find(".jqte_editor").text().trim();
			var count = charCount.length;
 			if(count>1000)
 			{
				$('#errordivMessage').append("&#149; "+resourceJSON.MsgLgthNotExceed+".<br>");
				if(focs==0)
					$('#messageSend').find(".jqte_editor").focus();
				$('#messageSend').find(".jqte_editor").css("background-color", "#F5E7E1");
				cnt++;focs++;
			}
		}
		
		
	if(cnt==0){
		$('#lodingImage').append("<img src=\"images/loadingAnimation.gif\" /> "+resourceJSON.Send+"...");
		CandidateReportAjax.saveMessage(teacherId,messageSubject,messageSend,jobId,{ 
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{	
				hideMessage();
			}
		});
		return true;
	}else
	{
		$('#errordivMessage').show();
		return false;
	}
}

function hideMessage()
{	
	$('#lodingImage').empty();
	$('#myModalMessage').modal('hide');
	
	$('#message2show').html(resourceJSON.MsgSentToCandidate);
	$('#myModal2').modal('show');
		
	document.getElementById('messageSubject').value="";
	document.getElementById('messageSend').value="";
}


function cancelNotes()
{
	$('#divTxtNode').find(".jqte_editor").html("");		
	$('#myModalNotes').modal('show');
	$('#errordivNotes').empty();
	$('#divTxtNode').find(".jqte_editor").css("background-color", "");
	document.getElementById("noteId").value="";
	document.getElementById("spnBtnSave").style.display="inline";
}

function saveNotes()
{	
	var notes = $('#divTxtNode').find(".jqte_editor").html();
	var teacherId = document.getElementById("teacherId").value;
	var noteId = document.getElementById("noteId").value;
	
	var cnt=0;
	var focs=0;	
	$('#errordivNotes').empty();
	//if ($('#divTxtNode').find(".jqte_editor").text().trim()=="")
	//if (trim($('#divTxtNode').find(".jqte_editor").text())=="")
	if ($('#divTxtNode').find(".jqte_editor").text().trim()=="")
	{
		$('#errordivNotes').append("&#149; "+resourceJSON.PlzEtrNotes+".<br>");
		if(focs==0)
			$('#divTxtNode').find(".jqte_editor").focus();
		$('#divTxtNode').find(".jqte_editor").css("background-color", "#F5E7E1");
		cnt++;focs++;	
	}
	else
	{	
		CandidateReportAjax.saveNotes(teacherId,notes,noteId,
		{
			async:false,
			errorHandler:handleError,
			callback:function(data)
			{
				getNotesDiv(teacherId)					
			}
		});	
	}
}

function editNoted(notesId, viewOnly)
{	
	$('#errordivAct').empty();
	CandidateReportAjax.showEditNotes(notesId,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			document.getElementById("noteId").value=data.noteId;
			$('#divTxtNode').find(".jqte_editor").html(data.note);
			
			$('#divTxtNode').find(".jqte_editor").focus();
			if(viewOnly!=undefined)
				document.getElementById("spnBtnSave").style.display="none";
			else
				document.getElementById("spnBtnSave").style.display="inline";
		}
	});
}


function showActDiv(status,teacherAssessmentStatusId,jobForTeacherId)
{	
	$("#myModalAct").css({"z-index":"3000"});
	$('#errordivAct').empty();	
	CandidateReportAjax.showActDiv( status,teacherAssessmentStatusId,jobForTeacherId,{ 
	async: true,
	errorHandler:handleError,
	callback:function(data)
	{	
		document.getElementById("divAct").innerHTML=data.toString();
		$('#myModalAct').modal('show');		
	}});	
}

function chkActAction(){	
	var rdoSecStatus = document.getElementsByName("rdoSecStatus");
	var ids="";
	for(i=0;i<rdoSecStatus.length;i++)
	{
		if(rdoSecStatus[i].checked==true)
		{
			//alert(rdoSecStatus[i].value)
			ids = rdoSecStatus[i].value;
		}
	}
	
	var cnt=0;
	var focs=0;	
	
	$('#errordivAct').empty();
	
	if(ids=="")
	{
		$('#errordivAct').append("&#149; "+resourceJSON.PlzChooseAnAction+"<br>");		
		$('#role').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	else
	{	
		
		if(ids.split(",")[0]=="hird"){
			hireTeacher(ids.split(",")[1],ids.split(",")[2])			
		}
		else if(ids.split(",")[0]=="unhird"){
			unHireTeacher(ids.split(",")[1],ids.split(",")[2])
			
		}
		else if(ids.split(",")[0]=="remove"){
			removeTeacher(ids.split(",")[1],ids.split(",")[2])
			//$('#myModalAct').modal('hide');
		}
		else{
			var jobForTeacherId =  ids.split(",")[0];
			var secStatusId = ids.split(",")[1];
			var teacherId = ids.split(",")[2];
			CandidateReportAjax.saveOrUpdateTeacherSecStatus(jobForTeacherId,secStatusId,teacherId,
			{
				async: false,
				errorHandler:handleError,
				callback:function(data)
				{	
					$('#myModalAct').modal('hide');
					getCandidateGrid();
				}
			});	
		}
	}
}
function getPhoneDetail(teacherId){
	
	CandidateReportAjax.getPhoneDetail(teacherId,
	{
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			if(data==""){
				document.getElementById("divAlertText").innerHTML=""+resourceJSON.MsgPhNoIsNotAvailable+"";
				$('#divAlert').modal('show');
			}
			else{
				document.getElementById("divPhone").innerHTML=data;			
				$('#myModalPhone').modal('show');
			}
			
		}
	});
}
function divToggelClose(){
	if(document.getElementById("divToggel").style.display=='block'){
		document.getElementById("divToggel").style.display='none';
	}
}

function tpJbIEnable()
{
	var noOrRow = document.getElementById("tblGrid").rows.length;
	
	
	$('#hrefPDF').tooltip();
	$('#tpSearch').tooltip();
	$('#tpLegend').tooltip();
	
	$(document).ready(function(){		
		$("#tpLegend").click(function(){			
	    	$("#divToggel").slideToggle("slow");			
	  	});
	});
	
	for(var j=1;j<=noOrRow;j++)
	{	
		$('#tpResume'+j).tooltip();
		$('#tpPortfolio'+j).tooltip();
		$('#tpJSA'+j).tooltip();
		$('#tpPDReport'+j).tooltip();
		$('#tpCL'+j).tooltip();
		$('#tpAct'+j).tooltip();
		$('#tpMsg'+j).tooltip();
		$('#tpNotes'+j).tooltip();		
		$('#tpTrans'+j).tooltip();
		$('#tpCert'+j).tooltip();
		$('#tpMessage'+j).tooltip();
		$('#tpPhone'+j).tooltip();
		$('#tpInv'+j).tooltip();
		alert(2333);
	}
}
function tpJbIDisable()
{
	var noOrRow = document.getElementById("tblGrid").rows.length;
	
	for(var j=1;j<=noOrRow;j++)
	{
		$('#tpResume'+j).tooltip();
		$('#tpPortfolio'+j).tooltip();
		$('#tpJSA'+j).tooltip();
		$('#tpPDReport'+j).tooltip();
		$('#tpCL'+j).tooltip();
		$('#tpAct'+j).tooltip();
		$('#tpMsg'+j).tooltip();
		$('#tpNotes'+j).tooltip();
		$('#tpTrans'+j).tooltip();	
		$('#tpCert'+j).tooltip();
		$('#tpMessage'+j).tooltip();
		$('#tpInv'+j).tooltip();
		$('#teacherName_'+j).tooltip();
	}
}
////////// Note scroll 
function applyScrollOnNotesTbl()
{
		var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblNotes').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 100,
        width: 875,
        minWidth: null,
        minWidthAuto: false,
        colratio:[100,150,475,150], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });            
        });
}