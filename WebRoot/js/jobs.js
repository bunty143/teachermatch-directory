/* @Author: Gagan 
 * @Discription: view of edit domain js.
*/

var schoolTableFlag;
var page = 1;
var noOfRows = 50;
var sortOrderStr="";
var sortOrderType="";
var deviceTypeAndroid=$.browser.device = (/Android|webOS|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent.toLowerCase()));
var deviceType=$.browser.device = (/iPhone|iPad|iPod/i.test(navigator.userAgent.toLowerCase()));
var certificationDivCount=0;
var pageZS = 1;
var noOfRowsZS = 10;
var sortOrderStrZS="";
var sortOrderTypeZS="";
var saveUpdateflag=0;
function getPaging(pageno){		
	if(pageno!='')
		{
			page=pageno;	
		}
		else
		{
			page=1;
		}
		noOfRows = document.getElementById("pageSize").value;
		displayJobRecords();
	
	
}
function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{	
	
	  if(pageno!=''){
			page=pageno;
			//alert("page :: "+page);
		}else{
			page=1;
			//alert("default");
		}
		sortOrderStr=sortOrder;
		//alert("sortOrderStr :: "+sortOrderStr);
		sortOrderType=sortOrderTyp;
		if(document.getElementById("pageSize")!=null){
			//alert("AA")
			noOfRows = document.getElementById("pageSize").value;
		}else{
		//	alert("BB");
			noOfRows=50;
		}
		displayJobRecords();
		
	
}

function actionForward()
{
	var JobOrderType=document.getElementById("JobOrderType").value;
	if(JobOrderType==2){
		window.location.href="jobs.do";
	}
}

function disableEndDate(){
	document.getElementById('changeDateHid').value=document.getElementById('jobEndDate').value;
	document.getElementById('jobEndDate').value='';
	document.getElementById('jobEndDate').disabled=true;	
}
function unDisableEndDate(){
	document.getElementById('jobEndDate').disabled=false;
	document.getElementById('jobEndDate').value=document.getElementById('changeDateHid').value;
}
function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert(""+resourceJSON.msgServerErr+": "+exception.javaClassName);}
}
function displayJobRecordsEnterOnButton()
{
page=1;
displayJobRecords();
}

/*========  displayJobRecords ===============*/
function displayJobRecords()
{	
	$('#loadingDiv').fadeIn();
	var districtId	=	0;
	//var schoolId 	=	0;
	var schoolId		=	document.getElementById("schoolId").value;
	districtId	=	document.getElementById("districtOrSchooHiddenlId").value;
	var status	=	document.getElementById("status").value;	
	var jobId	=	document.getElementById("jobOrderId").value;
	var certType=document.getElementById("certificateTypeMaster").value;
	//alert(status+":::"+jobId+"::"+districtId+"::"+schoolId+"::"+certType)
	try
	{
	    JobsAjax.displayDistrictRecord(districtId,status,jobId,schoolId,certType,
			noOfRows,page,sortOrderStr,sortOrderType,{ 
		async: true,
		callback: function(data)
		{	    	
	    		$('#divMain').html(data);			
				applyScrollOnTbl();				
				$('#loadingDiv').hide();	    	
		},
		errorHandler:handleError
	});
	}
	catch(e){alert(e)}

}
function getCertificationTypeCheck(certificationType){
	var certificateTypeTexts =document.getElementsByName('certificateTypeText');
	var isIdExist =false;
	try{
			for(var i=0;i<certificateTypeTexts.length;i++){
				var lableText=trim(certificateTypeTexts[i].innerText);
				if(lableText===certificationType){
					isIdExist=true;
				}
			}
	}catch(err){ }
	return isIdExist;
}

/*========  activateDeactivateUser ===============*/
function activateDeactivateJob(entityID,jobid,status)
{
	JobsAjax.activateDeactivateJob(jobid,status, { 
		async: true,
		callback: function(data)
		{
			displayJobRecords();
		},
		errorHandler:handleError
	});
}

function removeCertification(val){
	document.getElementById("CertificationGrid"+val).innerHTML='';
	document.getElementById("CertificationGrid"+val).style.display='none';
}

function validateAddCertification()
{	
	var statemaster = {stateId:dwr.util.getValue("stateMaster")};
	var certType=trim(document.getElementById("certType").value);
	var certTypeId=trim(document.getElementById("certificateTypeMaster").value);
	var certNameCheck=false;
	if(certType!=""){
		certNameCheck=getCertificationTypeCheck(certType);
	}
	$('#errordiv').empty();
	if(statemaster.stateId==0){
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.msgState+"<br>");
		$('#stateMaster').focus();
	}
	if(certType==''){
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.msgentercertificatename+"<br>");
		$('#certType').focus();
	}else if(certTypeId==''){
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.msgvalidCertificationLicensureName+"<br>");
		$('#certType').focus();
	}else if(certNameCheck){
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.msgcannotaddduplicateCertification1+"<br>");
		$('#certType').focus();
	}else{
		var jobId=document.getElementById("jobId").value;
		if(certType!="" && jobId==0){
			try
			{			
			certificationDivCount++;			
			document.getElementById("showCertificationDiv").innerHTML	+=	"<div class=\"\" id=\"CertificationGrid"+certificationDivCount+"\" nowrap><input type=\"hidden\" name=\"certificateTypeIds\" id=\"certificateTypeId\" value=\""+certTypeId+"\" ><label certtypeidcl=\""+certTypeId+"\" name=\"certificateTypeText\" id=\"certificateTypeText\">"+certType+" <a href=\"javascript:void(0);\" onclick=\"removeCertification("+certificationDivCount+");\"><img width=\"15\" height=\"15\"  class=\"can\" src=\"images/can-icon.png\"></a></label></div>";
			$("#addCertificationDiv").hide();
			}
			catch(e)
			{alert(e)}
			}else if(jobId!="" && jobId!=0){			
			ManageJobOrdersAjax.addCertification(jobId,certTypeId,statemaster.stateId,{ 
				async: true,
				callback: function(data)
					{
						if(data>0){
							$('#errordiv').show();
							$('#errordiv').append("&#149; "+resourceJSON.msgcannotaddduplicateCertification1+"<br>");
							$('#certType').focus();
						}else{
							DisplayCertification();
							$("#addCertificationDiv").hide();							
							//$("#addCertificationDiv").css("display", "none");
						}						
					},
					errorHandler:handleError 
					});
			
				}
			
		document.getElementById("certType").value="";
	}
}

function validateAddSchool()
{
	$('#errordiv').empty();	
	var errorCount=0;
	var schoolId		=	document.getElementById("schoolId").value;
	var schoolName=trim(document.getElementById("schoolName").value);
	var noOfSchoolExpHires=trim(document.getElementById("noOfSchoolExpHires").value);	
	var errorCounter=0;
	var focusCounter=0; 
	var arrReqNum	=	"";
	var rdReqNoSourceSchool="";
	var arrReqNumAndSchoolId	=	"";
	$('#schoolName').css("background-color", "");
	$('#noOfSchoolExpHires').css("background-color", "");
	
	if(schoolName==''){
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.msgPleaseenterSchoolName+"<br>");
		if(focusCounter==0)
			$('#schoolName').focus();
		$('#schoolName').css("background-color", "#F5E7E1");
		errorCounter++;
		focusCounter++;
	}else if(schoolId==''){
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.msgentervalidSchoolName+"<br>");
		if(focusCounter==0)
			$('#schoolName').focus();
		$('#schoolName').css("background-color", "#F5E7E1");
		errorCounter++;
		focusCounter++;
	}
	if(schoolId!='' && saveUpdateflag==0)
	{
		if(checkAvailableSchool(schoolId)){
			$('#errordiv').show();
			$('#errordiv').append("&#149; "+resourceJSON.magcannotaddexistingSchool+"<br>");
			if(focusCounter==0)
				$('#schoolName').focus();
			$('#schoolName').css("background-color", "#F5E7E1");
			errorCounter++;
			focusCounter++;
		}
	}	
	if(noOfSchoolExpHires==''|| noOfSchoolExpHires==0){
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.msgExpectedHire+"<br>");
		if(focusCounter==0)
			$('#noOfSchoolExpHires').focus();
		$('#noOfSchoolExpHires').css("background-color", "#F5E7E1");
		focusCounter++;
		errorCounter++;
	}
	
		if(errorCounter==0)
		{	
		try
		{
		var jobId=document.getElementById("jobId").value;
		if(schoolName!="" && jobId==0)
		{			
			    var arrReqNumAndSchoolId_temp11=schoolId+"::"+noOfSchoolExpHires+"::"+saveUpdateflag;
				JobsAjax.saveJobEequisitionNumbersTemp(arrReqNumAndSchoolId_temp11,{ 
					async: true,
					callback: function(data)
					{					    
						ShowSchoolTemp();
					},
					errorHandler:handleError 
				});
			}
		else if(schoolName!="" && jobId!=0)
		{
			//alert("update");
			saveUpdateflag = 1;
			//var arrReqNumAndSchoolId_temp=schoolId+"!!"+noOfSchoolExpHires+"@@"+arrReqNum+"##";
			 //var arrReqNumAndSchoolId_temp11=schoolId+"::"+noOfSchoolExpHires+"::"+saveUpdateflag;
			var arrReqNumAndSchoolId_temp11=schoolId+"##"+noOfSchoolExpHires;
				//JobsAjax.saveJobEequisitionNumbers(jobId,arrReqNumAndSchoolId_temp,rdReqNoSourceSchool,newTextRequisitionNumbersSchool,{ 
			//alert("arrReqNumAndSchoolId_temp "+arrReqNumAndSchoolId_temp11);
				//JobsAjax.saveJobEequisitionNumbersTemp(arrReqNumAndSchoolId_temp11,{
			 ManageJobOrdersAjax.saveJobEequisitionNumbersTemp_NoReq(arrReqNumAndSchoolId_temp11,{
					async: true,
					callback: function(data)
					{
						ShowSchoolTemp();
					},
					errorHandler:handleError 
				});			
		}	
		}catch(e){alert(e)}
	document.getElementById("schoolName").value="";
	document.getElementById("noOfSchoolExpHires").value="";
	document.getElementById("schoolId").value="";	
	cancelSchool();
	}
}

function cancelSchool()
{
	document.getElementById("schoolName").value="";
	document.getElementById("noOfSchoolExpHires").value="";
	document.getElementById("schoolId").value="";
	$('#schoolName').attr("disabled", false);
	$('#errordiv').empty();
	$("#addSchoolDiv").hide();	
	
}

function getJobCategories()
{	
	JobsAjax.getJobCategories(
			{
				async: true,
				errorHandler:handleError,
				callback:function(data)
				{			
					if(data!="" || data.length>0)
					{
						$("#jobCategoryId").html(data);
					}
				}
			});
}

function checkAvailableSchool(schoolId)
{
	var res;
	createXMLHttpRequest();  
	queryString = "checkSchoolId.do?schoolId="+schoolId+"&dt="+new Date().getTime();
	xmlHttp.open("POST", queryString, false);
	xmlHttp.onreadystatechange = function()
	{
		if(xmlHttp.readyState == 4)
		{
			if(xmlHttp.status==200)	
			{	
				checkSession(xmlHttp.responseText)
				res = xmlHttp.responseText;					
			}			
			else
			{
				alert(resourceJSON.msgServerErr);
			}
		}
	}
	xmlHttp.send(null);		
	if(res==1)
		return true;
	else
		return false;
}

function checkSession(responseText)
{
	if(responseText=='100001')
	{
		alert(resourceJSON.oops)
		window.location.href="quest.do";
	}	
}

function ShowSchoolTemp()
{
	try
	{
	var JobOrderType=document.getElementById("JobOrderType").value;	
	JobsAjax.displaySchoolTemp(JobOrderType,{ 
		async: true,
		callback: function(data)
		{
		    $("#showAndHideSchoolDiv").show();
			document.getElementById("schoolRecords").innerHTML=data;
		},
		errorHandler:handleError 
	});
	}
	catch(e){alert(e)}
}
function deleteSchoolInJobOrder(jobId,schoolId)
{
	if (confirm(resourceJSON.msgsuredeleteschool)) {
		if(jobId=='')jobId=0;
		if(schoolId=='')schoolId=0;
		ManageJobOrdersAjax.deleteSchoolInJobOrder(jobId,schoolId, {
			async: true,
			callback: function(data)
			{
				if(data)
				{   displayRequisationNumber();
					ShowSchool();
					getRequistionNoByDID();
					cancelSchool();
				}
				else
				{
					var divMsg="<div>"+resourceJSON.msgdeletethisSchool+"</div>";
					document.getElementById("Msg").innerHTML=divMsg;
					$('#closeBtn').hide();
					$('#myModalMsg').modal('show');
				}
			},
			errorHandler:handleError 
		});
	}
}

function getRequistionNoByDID(){
	var districtOrSchooHiddenlId=$("#districtOrSchooHiddenlId").val();
	
	if(jobId!=""){
		ManageJobOrdersAjax.getRequistionNoByDID(districtOrSchooHiddenlId,{ 
			async: true,
			callback: function(data)
			{
				$('#avlbListSchool').html(data);
			},
			errorHandler:handleError 
		});
	}
}

function deleteReqByJobId(){
	try{
		var jobId=$("#jobId").val();
		if(jobId>0){
			ManageJobOrdersAjax.deleteByJobId(jobId,{ 
				async: true,
				callback: function(data){
					$('#avlbListSchool').append(data);
					$('#avlbList').append(data);
					sortSelectList();
				},
				errorHandler:handleError 
			});
		}
	}catch(e){}
	
	
	//document.getElementById("requiSchoolIdsForHire").value="";
	
}
function getRequisiontForJobOrder(){
	var jobId=$("#jobId").val();
	var schoolId=0;
	if(jobId!=""){
		ManageJobOrdersAjax.editSchoolInJobOrder(jobId,schoolId,{ 
			async: true,
			callback: function(data){
				$('#tempList').append(data);
				$('#avlbList').append(data);
				$('#avlbListSchool').append(data);
				$("#tempList option").remove();
				sortSelectList();
				$('#showSchoolDiv').html("");
				$('#schoolRecords').html("");
				
			},
			errorHandler:handleError 
		});
	}
}


/*function deleteSchoolInJobOrder(jobId,schoolId)
{
	if (confirm("Are you sure, you would like to delete this School? It will also delete all requisition number attached to it.")) {
		if(jobId=='')jobId=0;
		if(schoolId=='')schoolId=0;
		ManageJobOrdersAjax.deleteSchoolInJobOrder(jobId,schoolId, {
			async: true,
			callback: function(data)
			{
				if(data)
				{   //displayRequisationNumber();
					ShowSchool();
					getRequistionNoByDID();
					cancelSchool();
				}
				else
				{
					var divMsg="<div>You can not delete this School.</div>";
					document.getElementById("Msg").innerHTML=divMsg;
					$('#closeBtn').hide();
					$('#myModalMsg').modal('show');
				}
			},
			errorHandler:handleError 
		});
	}
}*/
function deleteSchoolInJobOrderTemp(schoolId)
{
	if (confirm(resourceJSON.msgsuredeleteschool)) {
		if(schoolId=='')schoolId=0;
		ManageJobOrdersAjax.deleteSchoolInJobOrderTemp(schoolId, { 
			async: true,
			callback: function(data)
			{
				if(data)
				{
					ShowSchoolTemp();					
				}
			},
			errorHandler:handleError 
		});
	}
}
function editSchoolInJobOrderTemp(schoolId){	
	try
	{
	saveUpdateflag=1;
	$("#addSchoolDiv").fadeIn();
	$('#schoolId').val("");
	$('#schoolName').val("");
	$('#noOfSchoolExpHires').val("");	
	$('#schoolName').css("background-color", "");
	$('#noOfSchoolExpHires').css("background-color", "");

	ManageJobOrdersAjax.editSchoolInJobOrderTemp(schoolId,{ 
		async: true,
		callback: function(data){
		
			var s0=data.split("###");
			var s1=s0[0].split("##");
			var s2=s1[0].split("!!");
			$('#schoolId').val(s2[0]);
			$('#schoolName').val(s2[1]);
			$('#schoolName').attr("disabled", "disabled");
			$('#noOfSchoolExpHires').val(s1[1]);		
		},
		errorHandler:handleError 
	}); 
	}
	catch(e)
	{alert(e)}
}

function addSchool(){		
	try
	{
	saveUpdateflag=0;
	$('#errordiv').empty();
	$("#addSchoolDiv").show();	
	$('#schoolId').val("");
	$('#schoolName').val("");
	$('#schoolName').focus();
	$('#schoolName').css("background-color", "");
	$('#noOfSchoolExpHires').val("");	
	$('#schoolName').attr("disabled", false);
	}
	catch(e)
	{
		alert(e);
	}
}

function checkSchoolInTempTable()
{
	var res;
	createXMLHttpRequest();  
	queryString = "checkSchoolInTable.do?dt="+new Date().getTime();
	xmlHttp.open("POST", queryString, false);
	xmlHttp.onreadystatechange = function()
	{
		if(xmlHttp.readyState == 4)
		{
			if(xmlHttp.status==200)	
			{	
				checkSession(xmlHttp.responseText)
				res = xmlHttp.responseText;					
			}			
			else
			{
				alert(resourceJSON.msgServerErr);
			}
		}
	}
	xmlHttp.send(null);		
	if(res==1)
		return true;
	else
		return false;
}
function editSchoolInJobOrder(jobId,schoolId){
	
	/*$("#rdReqNoSourceSchool1" ).prop( "disabled", true );
	$("#newTextRequisitionNumbersSchool" ).prop( "disabled", true );
	
	$("#rdReqNoSourceSchool2" ).prop( "disabled", false );
	$("#rdReqNoSourceSchool2" ).prop( "checked", true );*/
	
	updateRow=1;
	$("#addSchoolDiv").fadeIn();
	$('#schoolId').val("");
	$('#schoolName').val("");
	$('#noOfSchoolExpHires').val("");
	
	$('#schoolName').css("background-color", "");
	$('#noOfSchoolExpHires').css("background-color", "");
	$('#attachedListSchool').css("background-color", "");
	
	$('#attachedListSchool option').remove();
	ManageJobOrdersAjax.editSchoolInJobOrder(jobId,schoolId,{ 
		async: true,
		callback: function(data){
			var s0=data.split("###");
			var s1=s0[0].split("##");
			var s2=s1[0].split("!!");
			$('#schoolId').val(s2[0]);
			$('#schoolName').val(s2[1]);
			$('#schoolName').attr("disabled", "disabled");
			$('#noOfSchoolExpHires').val(s1[1]);
			$('#attachedListSchool').append(s0[1]);
			$('#deleteAvailableRow').val(schoolId);
		},
		errorHandler:handleError 
	}); 
	
}

function deleteSchoolForTempTable()
{
	var res;
	createXMLHttpRequest();  
	queryString = "deleteSchoolForTempTable.do?dt="+new Date().getTime();
	xmlHttp.open("POST", queryString, false);
	xmlHttp.onreadystatechange = function()
	{
		if(xmlHttp.readyState == 4)
		{
			if(xmlHttp.status==200)	
			{	
				checkSession(xmlHttp.responseText)
				res = xmlHttp.responseText;	
				ShowSchoolTemp();
			}			
			else
			{
				alert(resourceJSON.msgServerErr);
			}
		}
	}
	xmlHttp.send(null);		
	if(res==1)
		return true;
	else
		return false;
}
var checkFlag;
function ShowSchool()
{
	
	var jobId=0;
	if(document.getElementById("schoolId").value!=null){
		jobId=document.getElementById("jobId").value;
	}
	var JobOrderType=document.getElementById("JobOrderType").value;
	JobsAjax.displaySchool(jobId,JobOrderType, { 
		async: true,
		callback: function(data)
		{ 
		var responsedata= data.split('@@@@');
		schoolTableFlag = responsedata[1];
			document.getElementById("schoolRecords").innerHTML=responsedata[0];
		},
		errorHandler:handleError 
	});
}

function showSchoolError()
{
	$('#schoolName').css("background-color", "#F5E7E1");
	$('#noOfSchoolExpHires').css("background-color", "#F5E7E1");
	$('#schoolName').focus();
}

function validateAddEditJobOrderforquest()
{
	
	try
	{
		var subjectId = 0;	
	var errorCount=0;
	var focusCount=0;	
	$("#errordiv").empty();
	$('#jobDescriptionErrordiv').empty();
	var jobID = 0;
	var jobType=document.getElementById("jobType").value;	
	var JobOrderType	=	document.getElementById("JobOrderType").value;	
	var allSchoolGradeDistrictVal	=	document.getElementById("allSchoolGradeDistrictVal").value;		
	var districtOrSchooHiddenlId	=	document.getElementById("districtOrSchooHiddenlId").value;	
	var jobTitle		=	trim(document.getElementById("jobTitle").value);
	var jobStartDate	=	trim(document.getElementById("jobStartDate").value);
	//var jDescription	=	trim(document.getElementById("jobDescription").value);
	
	var jDescription = $('#jDescription').find(".jqte_editor").html();	
	var exitURL			=	document.getElementById("exitURL").value;
    var	noOfExpHires=trim(document.getElementById("noOfExpHires").value);
    /*var schoolName		=	document.getElementById("schoolName").value;
    var schoolId		=	document.getElementById("schoolId").value;*/
    subjectId	=	document.getElementById("subjectId").value;
    var jobCategoryId			=	document.getElementById("jobCategoryId").value;
	var jobEndDate = "";
	if ($("#endDateRadio1").prop("checked")) {
		jobEndDate	=	trim(document.getElementById("jobEndDate").value);
	}else if ($("#endDateRadio2").prop("checked")) {
		jobEndDate	=	"12-25-2099";
	}
	
	}catch(e)
	{alert(e)}
	if(jobTitle==""){
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.msgjobtitleenter+"<br>");
		if(focusCount==0)
		$('#jobTitle').focus();
		$('#jobTitle').css("background-color", "#F5E7E1");
		errorCount++;
		focusCount++;
	}
	
	if(jobCategoryId==0){
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.msgJobCategoryName+"<br>");
		if(focusCount==0)
		$('#jobCategoryId').focus();
		$('#jobCategoryId').css("background-color", "#F5E7E1");
		errorCount++;
		focusCount++;
	}
	
	if(jobStartDate==""){
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.msgPostingStartDate+"<br>");
		if(focusCount==0)
		$('#jobStartDate').focus();
		$('#jobStartDate').css("background-color", "#F5E7E1");
		errorCount++;
		focusCount++;
	}
	if(jobEndDate==""){
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.msgPostingEndDate+"<br>");
		if(focusCount==0)
		$('#jobEndDate').focus();
		$('#jobEndDate').css("background-color", "#F5E7E1");
		errorCount++;
		focusCount++;
	}
	/*if(subjectId==0){
		$('#errordiv').show();
		$('#errordiv').append("&#149; Please select Subject<br>");
		if(focusCount==0)
		$('#subjectId').focus();
		$('#subjectId').css("background-color", "#F5E7E1");
		errorCount++;
		focusCount++;
	}*/
	if(jobStartDate!="" && jobEndDate!=""){
		try{
			var date1 = jobStartDate.split("-");
			var	date2 = jobEndDate.split("-");
		    var sDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
		    var eDate = new Date(date2[0]+"/"+date2[1]+"/"+date2[2]);
			if(sDate > eDate){
				$('#errordiv').show();
				$('#errordiv').append("&#149; "+resourceJSON.msggreaterthanenddate+"<br>");
				if(focusCount==0)
				$('#jobStartDate').focus();
				$('#jobStartDate').css("background-color", "#F5E7E1");
				errorCount++;
				focusCount++;
			} 
		}catch(err){}
	}
	/*if(allSchoolGradeDistrictVal==1 && noOfExpHires=="")
	{
		$('#errordiv').show();
		$('#errordiv').append("&#149; Please enter the noOfExpHires<br>");
		if(focusCount==0)
			$('#noOfExpHires').focus();
		$('#noOfExpHires').css("background-color", "#F5E7E1");
		errorCount++;
		focusCount++;		
	}
	else if(allSchoolGradeDistrictVal==2)
	{ 	
		//find school in temp table
		var checkForSchooladdedorNotIntempTable=checkSchoolInTempTable();
		//alert("checkForSchooladdedorNotIntempTable=="+checkForSchooladdedorNotIntempTable)
		if(!checkForSchooladdedorNotIntempTable )
		{
			if(schoolTableFlag == false)
			{
				var noOfSchoolExpHires=document.getElementById("noOfSchoolExpHires").value;		
				checkForAdministration=1; 
				$("#addSchoolDiv").show();	
				if(schoolName==''){
					$('#errordiv').show();
					$('#errordiv').append("&#149; Please enter School Name<br>");
					if(focusCount==0)
					$('#schoolName').focus();
					$('#schoolName').css("background-color", "#F5E7E1");
					errorCount++;
					focusCount++;
				}else if(schoolId==''){
					$('#errordiv').show();
					$('#errordiv').append("&#149; Please enter valid School Name<br>");	
					if(focusCount==0)
					$('#schoolName').focus();
					$('#schoolName').css("background-color", "#F5E7E1");
					errorCount++;
					focusCount++;
				}
				if(noOfSchoolExpHires=="")
				{
					$('#errordiv').show();
					$('#errordiv').append("&#149; Please enter the noOfSchoolExpHires<br>");
					if(focusCount==0)			
					$('#noOfSchoolExpHires').focus();			
					$('#noOfSchoolExpHires').css("background-color", "#F5E7E1");
					errorCount++;
					focusCount++;
				}	
			}
		}			
		schoolIdCount=getTRAndTDForSchoolCount();
		if(schoolIdCount == 0 && allSchoolGradeDistrictVal==2)
		{				
			errorCount++;
			try{
				addSchool();				
				var divMsg="<div>One or more School should be attached to this Job Order </div>";
				document.getElementById("EPIMsg").innerHTML=divMsg;
				$('#EPIMessage').modal('show');
				return false;
			}catch(err){}
		}
	}*/
	if(exitURL=="")
	{
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.msgapplicantshouldberedirected+"<br>");
		if(focusCount == 0)		
		$('#exitURL').focus();
		$('#exitURL').css("background-color", "#F5E7E1");
		errorCount++;
		focusCount++;
	}
	
	/*var certificateTypeText = new Array();
	   $('input[id^="certificateTypeId"]').each( function(index){  
		   certificateTypeText[index]=$(this).val();
	   });*/
	

	   var certificationType="";
	   var certIds = document.getElementsByName("certificateTypeIds");
		for(i=0;i<certIds.length;i++)
		{
			if(i == certIds.length-1)			
				certificationType+=certIds[i].value;			
			else			
				certificationType+=certIds[i].value+",";
			
		}
		if(document.getElementById("jobId").value!="")
		{
			jobID =  document.getElementById("jobId").value;
		}
	
		//alert("SchoolGradeDistrictVal : "+allSchoolGradeDistrictVal+"Job Type : "+jobType+" Job End Date : "+jobEndDate+" Job Start Date : "+jobStartDate+" Job Category Id : "+jobCategoryId+" Job Id : "+jobID+" ExitURL : "+exitURL+" Certification Type: "+certificationType+" JobOrderType : "+JobOrderType+" DistrictOrSchoolHiddenId : "+districtOrSchooHiddenlId+" JobTitle : "+jobTitle+" Job Description : "+jDescription+" No. of exp Hires : "+noOfExpHires);
		
		if(errorCount==0)
		{
		JobsAjax.addEditJobOrder(jobCategoryId, jobID, exitURL, certificationType, JobOrderType, 
				districtOrSchooHiddenlId, jobTitle, jDescription, noOfExpHires, jobStartDate, jobEndDate, allSchoolGradeDistrictVal, jobType,subjectId,{
			async: true,
			callback: function(data)
			{	
			var dd=data.split("||");			
				if(jobID==0){					
					$('#loadingDiv').hide();
					var divMsg="<div>"+resourceJSON.msgsuccessfullycreatedajoborder+"<br/><br/>"+resourceJSON.msgTeacherMatchJobURLis+": <div id='jobOrderUrl'><a target='blank' href=\""+dd[0]+"\">"+dd[0]+"</a></div>";
					$('#jobOrderAdd').modal('show');
					document.getElementById("jobOrderText").innerHTML=divMsg;					
					return false;
				}else{
					$('#loadingDiv').hide();
					var divMsg="<div>"+resourceJSON.MsgSuccessfullyUpdateThisJobOrder+"<br/><br/>"+resourceJSON.msgTeacherMatchJobURLis+": <div id='jobOrderUrl'><a target='blank' href=\""+dd[0]+"\">"+dd[0]+"</a></div>";					
					$('#jobOrderUpdate').modal('show');
					document.getElementById("jobOrderUpdateText").innerHTML=divMsg;					
				}
			},
			errorHandler:handleError
		});
		$('#errordiv').hide();			
		}
		else{
			$('#errordiv').show();
			$('html, body').animate({scrollTop : 0},800);
			return false;
		}
}

function delay(sec){
	var starttime = new Date().getTime();
	starttime = starttime+sec;
	while(true){
		if(starttime< new Date().getTime()){
			break;
		}
	}
}

function addnewJob(url)
{
	var currentdate = new Date();
	var datetime =  currentdate.getDay()+""+currentdate.getMonth()+""+currentdate.getFullYear()+""+currentdate.getHours()+""+currentdate.getMinutes()+""+currentdate.getSeconds();
	url=url+""+datetime;
	window.location.href=url;
}

var count=0;
var index=-1;
var length=0;
var divid='';
var txtid='';
var hiddenDataArray = new Array();
var showDataArray = new Array();
var degreeTypeArray = new Array();
var hiddenId="";


/*---------------------------------School Auto Complete Js Starts ----------------------------------------------------------------*/
function getSchoolAuto(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("schoolName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getSchoolArray(txtSearch.value);		
		fatchData2(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getSchoolArray(schoolName){
	try
	{
	var searchArray = new Array();
	var districtId	=	document.getElementById("districtOrSchooHiddenlId").value;	
		BatchJobOrdersAjax.getFieldOfSchoolList(districtId,schoolName,{ 
			async: false,
			callback: function(data){
			hiddenDataArray = new Array();
			showDataArray = new Array();
			for(i=0;i<data.length;i++){
				searchArray[i]=data[i].schoolName;
				showDataArray[i]=data[i].schoolName;
				hiddenDataArray[i]=data[i].schoolMaster.schoolId;
			}
		},
		errorHandler:handleError
		});	
	}
	catch(e){alert(e)}
	return searchArray;
}


selectFirst="";

var fatchData2= function(txtSearch,searchArray,txtId,txtdivid){

	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
			}
			
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}
function hideSchoolMasterDiv(dis,hiddenId,divId)
{
	document.getElementById("schoolId").value="";
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			document.getElementById("schoolId").value=hiddenDataArray[index];
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}

/*--------------------------------- End  School Auto Complete Js  ----------------------------------------------------------------*/
var downArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index<length-1){
			for(var i=0;i<10;i++){
				{
					if(document.getElementById('divResult'+txtdivid+i))
						var div_id=document.getElementById('divResult'+txtdivid+i);
					if(div_id)
					{
						if(div_id.className=='over')
							index=div_id.id.split('divResult'+txtdivid)[1];
						div_id.className='normal';
					}
				}
			}
			index++;
			if(document.getElementById('divResult'+txtdivid+index))
			{
				var div_id=document.getElementById('divResult'+txtdivid+index);
				div_id.className='over';
				document.getElementById(txtid).value = div_id.innerHTML;
				selectFirst=div_id.innerHTML;
			}
		}
	}
}

var upArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index>0){
			for(var i=0;i<length;i++){

				var div_id=document.getElementById('divResult'+txtdivid+i);
				if(div_id)
				{
					if(div_id.className=='over')
						index=div_id.id.split('divResult'+txtdivid)[1];
					div_id.className='normal';
				}
			}
			index--;
			if(document.getElementById('divResult'+txtdivid+index))
				document.getElementById('divResult'+txtdivid+index).className='over';
			if(txtid && document.getElementById('divResult'+txtdivid+index))
			{
				document.getElementById(txtid).value=
					document.getElementById('divResult'+txtdivid+index).innerHTML;
				selectFirst=document.getElementById('divResult'+txtdivid+index).innerHTML;
			}
		}
	}
}

var overText=function (div_value,txtdivid) {

	for(var i=0;i<length;i++)
	{
		if(document.getElementById('divResult'+i))
		{
			var div_id=document.getElementById('divResult'+i);

			if(div_id.className=='over')
				index=div_id.id.split(txtdivid)[1];
			div_id.className='normal';
		}
	}
	div_value.className = 'over';
	document.getElementById(txtid).value= div_value.innerHTML;
}
function trim(s)
{
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}
function checkForInt(evt) {

	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==8)
		return true;

	return( (charCode >= 48 && charCode <= 57)||(charCode==13) );
}
function mouseOverChk(txtdivid,txtboxId)
{	
	for(var i=0;i<length;i++)
	{
		$('#divResult'+txtdivid+i).mouseover({param1: i,param2:txtdivid, param3:txtboxId}, fireMouseOverEvent);
	}
}


function fireMouseOverEvent(event)
{
	for(var i=0;i<length;i++)
	{	
		document.getElementById('divResult'+event.data.param2+i).className='normal';
	}
    document.getElementById('divResult'+event.data.param2+event.data.param1).className='over';	       
   	document.getElementById(event.data.param3).value= $('#divResult'+event.data.param2+event.data.param1).text();
    index=event.data.param1;
}

function getAllCertificateNameAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38)
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13)
	{
		
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		document.getElementById("hrefDone").focus();
		
	} 
	else if(event.keyCode==9)
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getAllCertificateNameArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getAllCertificateNameArray(certName){
	
	var searchArray = new Array();	
	DWRAutoComplete.getAllCertificateNameList(certName,{ 
		async: false,
		callback: function(data){
		hiddenDataArray = new Array();
		showDataArray = new Array();
		for(i=0;i<data.length;i++){
			searchArray[i]=data[i].certName;
			hiddenDataArray[i]=data[i].certNameId;
			showDataArray[i]=data[i].certName;
		}
	}
	});	

	return searchArray;
}
function hideAllCertificateNameDiv(dis,hiddenId,divId)
{
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
		if(dis.value!="")
		{
			var cnt=0;
			var focs=0;	
			$('#errordiv').empty();	
			setDefColortoErrorMsg();
			$('#errordiv').append("&#149; "+resourceJSON.msgcertificatenamebased+"<br>");
			if(focs==0)
				$('#certName').focus();
			
			$('#certName').css("background-color",txtBgColor);
			cnt++;focs++;	
		}
		else
		{
			$('#errordiv').empty();	
			setDefColortoErrorMsg();
		}
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}
function jobStartDateDiv(){
	 var cal = Calendar.setup({
        onSelect: function(cal) { cal.hide() },
        showTime: true
   });
	cal.manageFields("jobStartDate", "jobStartDate", "%m-%d-%Y");
}

function DisplayCertification()
{
	var jobId=0;
	var jobStartDate	=	trim(document.getElementById("jobStartDate").value);

	if(document.getElementById("jobId").value!=''){
		jobId=document.getElementById("jobId").value;
	}else if(document.getElementById("clonejobId").value){
		jobId=document.getElementById("clonejobId").value;
	}
	if(jobId==0){
		jobStartDateDiv();
	}else{
		if(jobStartDate!=""){
			var startDate= new Date(jobStartDate);   
			if(startDate < new Date()){
				document.getElementById('jobStartDate').readOnly=true;
			}else{
				jobStartDateDiv();
			}
		}else{
			jobStartDateDiv();
		}
	}
	var JobOrderType=document.getElementById("JobOrderType").value;
	JobsAjax.displayCertification(jobId,JobOrderType, { 
		async: true,
		callback: function(data)
		{	
			document.getElementById("certificationRecords").innerHTML=data;
		},
		errorHandler:handleError 
	});
}


function checkValue(val)
{			
		document.getElementById("allSchoolGradeDistrictVal").value=val;	
		if(val==2)
		{
			document.getElementById("noOfExpHires").value="";
		}
		//alert(document.getElementById("allSchoolGradeDistrictVal").value);		
}

function getTRAndTDForSchoolCount(){
	var idArr =0;
	try{
		
	var trs =document.getElementById("schoolRecords").getElementsByTagName('TR'); 
	for(var i=0;i<trs.length;i++){
		var tdchk=trs[i].getElementsByTagName('TD');
		if(trs[i].style.display!='none' && i!=0){
			idArr++;
		}
	}
	}catch(e){}
	return idArr;
}



function getCandidateListForAppliedJob(jobId)
{
	JobsAjax.fetchCandidateListOfJobApplied(jobId,{ 
		async: true,
		callback: function(data)
		{
				try{
		document.getElementById('ifrmTrans1').src = "candidatelistAppiledforjob/"+data+"";
		}catch(e){alert(e);}
		},
		errorHandler:handleError 
	});
}



