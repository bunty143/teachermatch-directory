var page = 1;
var noOfRows = 10;
var sortOrderStr="";
var sortOrderType="";
var count=0;
var index=-1;
var length=0;
var divid='';
var txtid='';
var hiddenDataArray = new Array();
var showDataArray = new Array();
var degreeTypeArray = new Array();
var hiddenId="";

function getPaging(pageno)
{
	if(pageno!='')
	{
		page=pageno;	
	}
	else
	{
		page=1;
	}
	
	noOfRows = document.getElementById("pageSize").value;
	displayTempTalent();
}

function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
	if(pageno!=''){
		page=pageno;	
	}else{
		page=1;
	}
	sortOrderStr	=	sortOrder;
	sortOrderType	=	sortOrderTyp;
	if(document.getElementById("pageSize")!=null){
		noOfRows = document.getElementById("pageSize").value;
	}else{
		noOfRows=10;
	}
	displayTempTalent();
}
/*========  For Handling session Time out Error ===============*/
function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert(resourceJSON.msgServerErr+": "+exception.javaClassName);}
}
/*========  SearchDistrictOrSchool ===============*/
function validateTalentFile()
{
	//alert('validateTalentFile::::::::::'+validateTalentFile);
	var talentfile	=	document.getElementById("talentfile").value;

	var errorCount=0;
	var branchId="";
	var aDoc="";
	var counterCheck=1;
	$('#errordiv').empty();
	//***************** Adding By Deepak  *******************************
	var entityValue=document.getElementById("entityValue").value; 
	/*{
		 branchId	=	document.getElementById("branchName").value;
		//alert(branchId);
		if(branchId=="")
		{
			$('#errordiv').show();
			$('#errordiv').append("&#149; Please enter valid Branch Name.<br>");
			errorCount++;
			aDoc="1";
		}
	}*/
	if(aDoc==1){
		$('#branchName').focus();
		$('#branchName').css("background-color", "#F5E7E1");
		counterCheck=0;
	}
	//******************************************************************************
	
	if(talentfile==""){
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.msgXlsXlsxFile+"<br>");
		errorCount++;
		aDoc="1";
	}
	else if(talentfile!="")
	{
		var ext = talentfile.substr(talentfile.lastIndexOf('.') + 1).toLowerCase();	
		
		var fileSize = 0;
		if ($.browser.msie==true)
	 	{	
		    fileSize = 0;	   
		}
		else
		{
			if(document.getElementById("talentfile").files[0]!=undefined)
			{
				fileSize = document.getElementById("talentfile").files[0].size;
			}
		}
		
		if(!(ext=='xlsx' || ext=='xls'))
		{
			$('#errordiv').show();
			$('#errordiv').append("&#149; "+resourceJSON.msgXlsXlsxFileOnly+"<br>");
			errorCount++;
			aDoc="1";
		}
		else if(fileSize>=10485760)
		{
			$('#errordiv').show();
			$('#errordiv').append("&#149; "+resourceJSON.msgfilesizelessthan+"<br>");
			errorCount++;
			aDoc="1";	
		}
	}
	//alert(aDoc=="1"+" && "+counterCheck==1);
	if(aDoc=="1" && counterCheck==1){
		$('#talentfile').focus();
		$('#talentfile').css("background-color", "#F5E7E1");
	}
	
	if(errorCount==0){
		try{
			//$('#loadingDiv').show();
			if(talentfile!=""){
					document.getElementById("talentUploadServlet").submit();
				
			}
		}catch(err){}
		
	}else{
		$('#errordiv').show();
		return false;
	}
}

function uploadDataTalent(fileName,sessionId){
	var branchId   =   "";
	var headQuarterId  =   "";
	
	//**************** Added by deepak   ********************
	
	var entityValue=	document.getElementById("entityValue").value;
	try
	{
	  if(entityValue==5 || entityValue == 6)
	  {
	    headQuarterId	=	document.getElementById("headQuarterId").value;
	  }
	  if(entityValue == 6)
	  {
	       branchId	=	document.getElementById("branchId").value;
	  }
	}catch(e)
	{
	//alert(e);	
	}
	//************* End ***************************88
	$('#loadingDiv').fadeIn();
	LinkToKsnTalentsTempAjax.saveTalentTemp(fileName,sessionId,headQuarterId,branchId,{      
		async: true,
		callback: function(data){
			if(data=='1'){
					 window.location.href="linktoksntalentslist.do";
			}else{
				$('#loadingDiv').hide();
				$('#errordiv').show();
				$('#errordiv').append("&#149; "+resourceJSON.msgField+" "+data+" "+resourceJSON.msgDoesNotMatch+"<br>");
			}
		},
		errorHandler:handleError 
	});
	
}
function displayTempTalent()
{
	LinkToKsnTalentsTempAjax.displayTempTalentRecords(noOfRows,page,sortOrderStr,sortOrderType,{ 
		async: true,
		callback: function(data)
		{
			$('#tempTalentGrid').html(data);
			applyScrollOnTbl();
		},
		errorHandler:handleError  
	});
}

function tempTeacher(){
	window.location.href="linktoksntalents.do";
}

function deleteTempTalent(sessionIdTxt){
	$('#loadingDiv').fadeIn();
	LinkToKsnTalentsTempAjax.deleteTempTalent(sessionIdTxt,{ 
		async: true,
		callback: function(data){
			$('#loadingDiv').hide();
			//window.location.href="linktoksntalents.do";
			tempTeacher();
		},
		errorHandler:handleError 
	});
}




function saveTalent(){
	$('#loadingDiv').fadeIn();
	LinkToKsnTalentsTempAjax.saveTalent({ 
		async: true,
		callback: function(data){
			$('#loadingDiv').hide();
			//document.getElementById("Msg").innerHTML="Candidate details have been imported successfully.";
			
			//********   Adding By Deepak **********************
			if(data==1)
			{
				$('#myModalMsg1').modal('show');
			}
			else
			{
				$('#myModalMsg').modal('show');
			}
			//window.location.href="linktoksntalentslist.do";
			//**************************************************
		},
		errorHandler:handleError 
	});
}
function trim(s)
{
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}

//*************** Adding By Deepak   ***********************
function hidepopup()
{
	$('#myModalMsg1').modal('hide');
}


function checkEntity()
{
	if($("#etype").val()==6)
	{
		$('#branchName').val("");
		$('#bshow').removeClass("hide");
	}
	else
	{
		$('#branchName').val("");
		$('#bshow').addClass("hide")	
	}
}


