var userIdVal=0;

var page = 1;
var noOfRows = 10;
var sortOrderStr="";
var sortOrderType="";
function getPaging(pageno)
{
	if(pageno!='')
	{
		page=pageno;	
	}
	else
	{
		page=1;
	}
	
	noOfRows = document.getElementById("pageSize").value;
	var entityID	=	document.getElementById("userSessionEntityTypeId").value;
/*	var searchTextId	=	document.getElementById("districtOrSchooHiddenlId").value;
	if(searchTextId!=null || searchTextId!="")
	{
		alert('hello');
		displayScheduleRecords();
	}
	else
	{
*/
 	displayScheduleRecords();	
	//}
}
function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
	if(pageno!=''){
		page=pageno;	
	}else{
		page=1;
	}
	sortOrderStr=sortOrder;
	sortOrderType=sortOrderTyp;
	if(document.getElementById("pageSize")!=null){
		noOfRows = document.getElementById("pageSize").value;
	}else{
		noOfRows=10;
	}
	
	displayScheduleRecords();
}

/*========  For Handling session Time out Error ===============*/
function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert(""+resourceJSON.msgServerErr+": "+exception.javaClassName);}
}
/*========  displayMyScheduleRecords ===============*/
function displayScheduleRecords()
{
	var entityID			=	document.getElementById("userMasterEntityID").value;
	var districtHiddenId	=	document.getElementById("districtHiddenId").value;
	var fromDate			=	trim(document.getElementById("fromDate").value);
	var toDate				=	trim(document.getElementById("toDate").value); 
	var b					=	document.getElementById("myScheduledCheck").checked;
	
	if(Date.parse(fromDate)>Date.parse(toDate))
	{
		endDateStartdate("Please enter end date greater than start date.");
		return false;
	}
	
	if(b){
		MyScheduleAjax.displayScheduleRecordsByEntityType(true,entityID,districtHiddenId,noOfRows,page,
				sortOrderStr,sortOrderType,fromDate,toDate,2,{ 
			async: true,
			callback: function(data)
			{
				$('#divMain').html(data);
				applyScrollOnTbl();
			},
		});
	}else{
		MyScheduleAjax.displayScheduleRecordsByEntityType(true,entityID,districtHiddenId,noOfRows,page,
				sortOrderStr,sortOrderType,fromDate,toDate,1,{ 
			async: true,
			callback: function(data)
			{	
				$('#divMain').html(data);
				applyScrollOnTbl();
			},
		});
	}
}

function updateUserLastVisit(){
UserLastVisitDemoAjax.updateUserLastVisitDemo({
	callback: function(data){
	}
});
}