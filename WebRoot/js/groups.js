var count=0;
var index=-1;
var length=0;
var divid='';
var txtid='';
var page = 1;
var noOfRows = 10;
var sortOrderStr="";
var sortOrderType="";
var deviceTypeAndroid=$.browser.device = (/Android|webOS|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent.toLowerCase()));
var deviceType=$.browser.device = (/iPhone|iPad|iPod/i.test(navigator.userAgent.toLowerCase()));

var hiddenDataArray = new Array();
var showDataArray = new Array();
var degreeTypeArray = new Array();
var hiddenId="";

function hideDiv()
{
	$('#docfileNotOpen').hide();
	$('#exelfileNotOpen').hide();
}

function getDistrictORSchoolAuto(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40) //down key
	{
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("districtORSchoolName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getDistrictORSchoolArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
	}
	else if(txtSearch.value=="")
	{
		document.getElementById(divid).style.display='none';
	}
}

function getDistrictORSchoolArray(districtOrSchoolName)
{
	var searchArray = new Array();
	ManageJobOrdersAjax.getFieldOfDistrictList(districtOrSchoolName,
	{
		async: false,
		callback: function(data)
		{
			hiddenDataArray = new Array();
			showDataArray = new Array();
			for(i=0;i<data.length;i++)
			{
				searchArray[i]=data[i].districtName;
				showDataArray[i]=data[i].districtName;
				hiddenDataArray[i]=data[i].districtId;
			}
		},
		errorHandler:handleError 
	});	
	return searchArray;
}

var selectFirst="";
var fatchData = function(txtSearch,searchArray,txtId,txtdivid)
{
	var result = document.getElementById(txtdivid);
	try
	{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			
			for(var i=0;i<len;i++)
			{
				items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
				searchArray[i].toUpperCase() + "</div>";
				count++;
				length++;
					
			if(count==10)
				break;
			}
		}
		else 
		{
			
		}
		if(count!=0)
			result.innerHTML = items;
		else
		{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		scrolButtom();
	}
	catch (err){}
}

function hideDistrictMasterDiv(dis,hiddenId,divId)
{
	document.getElementById("districtOrSchooHiddenlId").value="";
	if(parseInt(length)>0)
	{
		if(index==-1)
			index=0;
		
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId))
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		
		if(dis.value=="")
		{
			$('#schoolName').attr('readonly', true);
			document.getElementById('schoolName').value="";
			document.getElementById('schoolId').value="0";
			document.getElementById(hiddenId).value="";	
		}
		else if(showDataArray && showDataArray[index])
		{
			dis.value=showDataArray[index];
			document.getElementById("districtOrSchooHiddenlId").value=hiddenDataArray[index];
			document.getElementById('schoolName').readOnly=false;
			document.getElementById('schoolName').value="";
			document.getElementById('schoolId').value="0";
		}
	}
	else
	{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}

/*---------------------------------School Auto Complete Js Starts ----------------------------------------------------------------*/

function getSchoolAuto(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	//alert('enter');
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40)
	{
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		document.getElementById("schoolName").focus();
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getSchoolArray(txtSearch.value);
		fatchData2(txtSearch,searchArray,txtId,txtdivid);
	}
	else if(txtSearch.value=="")
	{
		document.getElementById(divid).style.display='none';
	}
}

function getSchoolArray(schoolName)
{
	//alert(schoolName);
	var searchArray = new Array();
	var districtId	=	document.getElementById("districtOrSchooHiddenlId").value;
	//alert(districtId);
	ApplicantsByCertificationsAjax.getFieldOfSchoolList(districtId,schoolName,
	{ 
		async: false,
		callback: function(data)
		{
			hiddenDataArray = new Array();
			showDataArray = new Array();
			for(i=0;i<data.length;i++)
			{
				searchArray[i]=data[i].schoolName;
				showDataArray[i]=data[i].schoolName;
				hiddenDataArray[i]=data[i].schoolMaster.schoolId;
			}
		},
		errorHandler:handleError
	});
	return searchArray;
}

selectFirst="";
var fatchData2 = function(txtSearch,searchArray,txtId,txtdivid)
{
	var result = document.getElementById(txtdivid);
	try
	{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			for(var i=0;i<len;i++)
			{
				items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
				searchArray[i].toUpperCase() + "</div>";
				count++;
				length++;
				if(count==10)
					break;
			}
		}
		else
		{
			
		}
		if(count!=0)
			result.innerHTML = items;
		else
		{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		scrolButtom();
	}
	catch (err){}
}

function hideSchoolMasterDiv(dis,hiddenId,divId)
{
	document.getElementById("schoolId").value="";
	if(parseInt(length)>0)
	{
		if(index==-1)
			index=0;
		
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId))
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		if(dis.value=="")
			document.getElementById(hiddenId).value="";
		else if(showDataArray && showDataArray[index])
		{
			dis.value=showDataArray[index];
			document.getElementById("schoolId").value=hiddenDataArray[index];
		}
	}
	else
	{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
	}
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}

var downArrowKey = function(txtdivid)
{
	if(txtdivid)
	{
		if(index<length-1)
		{
			for(var i=0;i<10;i++)
			{
				{
					if(document.getElementById('divResult'+txtdivid+i))
						var div_id=document.getElementById('divResult'+txtdivid+i);
					if(div_id)
					{
						if(div_id.className=='over')
							index=div_id.id.split('divResult'+txtdivid)[1];
						div_id.className='normal';
					}
				}
			}
			index++;
			if(document.getElementById('divResult'+txtdivid+index))
			{
				var div_id=document.getElementById('divResult'+txtdivid+index);
				div_id.className='over';
				document.getElementById(txtid).value = div_id.innerHTML;
				selectFirst=div_id.innerHTML;
			}
		}
	}
}

var upArrowKey = function(txtdivid)
{
	if(txtdivid)
	{
		if(index>0)
		{
			for(var i=0;i<length;i++)
			{
				var div_id=document.getElementById('divResult'+txtdivid+i);
				if(div_id)
				{
					if(div_id.className=='over')
						index=div_id.id.split('divResult'+txtdivid)[1];
					div_id.className='normal';
				}
			}
			index--;
			if(document.getElementById('divResult'+txtdivid+index))
				document.getElementById('divResult'+txtdivid+index).className='over';
			if(txtid && document.getElementById('divResult'+txtdivid+index))
			{
				document.getElementById(txtid).value=
					document.getElementById('divResult'+txtdivid+index).innerHTML;
				selectFirst=document.getElementById('divResult'+txtdivid+index).innerHTML;
			}
		}
	}
}

var overText=function (div_value,txtdivid)
{
	for(var i=0;i<length;i++)
	{
		if(document.getElementById('divResult'+i))
		{
			var div_id=document.getElementById('divResult'+i);

			if(div_id.className=='over')
				index=div_id.id.split(txtdivid)[1];
			div_id.className='normal';
		}
	}
	div_value.className = 'over';
	document.getElementById(txtid).value= div_value.innerHTML;
}

function trim(s)
{
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r'))
	{
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r'))
	{
		s = s.substring(0,s.length-1);
	}
	
	return s;
}

function checkForInt(evt)
{
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==8)
		return true;
	return( (charCode >= 48 && charCode <= 57)||(charCode==13) );
}

function checkForDecimalTwo(evt)
{
	var count=true;
	var str=evt.srcElement.value;
	if (str.lastIndexOf('.') == -1) str += ".";
	var decNum = str.substring(str.lastIndexOf('.')+1, str.length);
	if (decNum.length > 1)//here is the key u can just change from 2 to 3,45 etc to restict no of digits aftre decimal
	{
	//alert("Invalid more than 2 digits after decimal")
		//count=false;
	}
	//alert(count);
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==8)
		return true;
	var parts = evt.srcElement.value.split('.');
    if(parts.length > 1 && charCode==46)
        return false;
	return( ((charCode >= 48 || charCode==46)&& count && charCode <= 57)||(charCode==13) );
}

function mouseOverChk(txtdivid,txtboxId)
{	
	for(var i=0;i<length;i++)
	{
		$('#divResult'+txtdivid+i).mouseover({param1: i,param2:txtdivid, param3:txtboxId}, fireMouseOverEvent);
	}
}

function fireMouseOverEvent(event)
{
	for(var i=0;i<length;i++)
	{	
		document.getElementById('divResult'+event.data.param2+i).className='normal';
	}
    document.getElementById('divResult'+event.data.param2+event.data.param1).className='over';	       
   	document.getElementById(event.data.param3).value= $('#divResult'+event.data.param2+event.data.param1).text();
    index=event.data.param1;
}

function checkForCGInt(evt)
{
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==8 || charCode==46)
		return true;
	return( (charCode >= 48 && charCode <= 57)||(charCode==13) );
}

function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{
		alert("Oops: Your Session has expired!");  document.location = 'signin.do';
	}
	else
	{
		alert("Server Error: "+exception.javaClassName);
	}
}

// for paging and sorting
function getPaging(pageno)
{
        //alert("Paging");
		if(pageno!='')
		{
			page=pageno;	
		}
		else
		{
			page=1;
		}
		noOfRows = document.getElementById("pageSize").value;
		searchRecordsByEntityTypeApplicantsList();
}

function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
	if(pageno!='')
	{
		page=pageno;
		//alert("page :: "+page);
	}
	else
	{
		page=1;
		//alert("default");
	}
	sortOrderStr=sortOrder;
	//alert("sortOrderStr :: "+sortOrderStr);
	sortOrderType=sortOrderTyp;
	if(document.getElementById("pageSize")!=null)
	{
		//alert("AA")
		noOfRows = document.getElementById("pageSize").value;
	}
	else
	{
		//alert("BB");
		noOfRows=10;
	}
	searchRecordsByEntityTypeApplicantsList();	
}

function buildApprovalGroup()
{
	//var isChecked = $("#buildApprovalGroups").attr("checked");
	var isChecked = $("#approvalBeforeGoLive1").attr("checked");
		
	if(isChecked!=null)
	{
		$("#addGroupHyperLink").show();
				
		var districtId=document.getElementById("districtId").value;
		$("#loadingDiv").show();
		DistrictAjax.displayApprovalGroupsAndMembers(districtId,
		{
			async: true,
			errorHandler:handleError,
			callback: function(data)
			{
				$("#groupMembersList").empty();
				$("#groupMembersList").html(data);
				$("#groupMembersList").show();
				$("[data-toggle='tooltip']").tooltip();
				$("#loadingDiv").hide();
				//var ch = $("#addApprovalGroupAndMemberDiv").is(':visible');
				//alert("ch:- "+ch);
				
			}
		});
	}
	else
	{
		$(".hideGroup").hide();
		$("#addApprovalGroupAndMemberDiv").hide();
	}
}

function removeMemberConfirmation(groupId, memberId)
{
	$("#removeMemberConfirmation").modal("show");
	$("#removeMemberConfirmation").attr("removeMemberGroupId",groupId);
	$("#removeMemberConfirmation").attr("removeMemberId",memberId);
}

function removeGroupConfirmation(groupId)
{
	$("#removeGroupConfirmation").modal("show");
	$("#removeGroupConfirmation").attr("removeGroupId",groupId);
}

function removeGroup()
{
	var groupId = $("#removeGroupConfirmation").attr("removeGroupId");
	$("#removeGroupConfirmation").modal("hide");
	
	//alert("groupId:- "+groupId);
	$("#loadingDiv").show();
	//alert("groupId:- "+groupId);
	DistrictAjax.removeApprovalGroupByGroupId(groupId,
	{
		async: true,
		errorHandler:handleError,
		callback: function(data)
		{
			if(data==false){
				$("#loadingDiv").hide();
				$("#deleteConfirmation").modal("show");	
			} else{
				buildApprovalGroup();
				showAddMembers();
				displayGroups();
			}
		}
	});
}

function removeMember()
{
	var groupId = $("#removeMemberConfirmation").attr("removeMemberGroupId");
	var memberId = $("#removeMemberConfirmation").attr("removeMemberId");
	$("#removeMemberConfirmation").modal("hide");
	
	$("#loadingDiv").show();
	DistrictAjax.removeMemberFromApprovalGroup(groupId,memberId,
	{
		async: true,
		errorHandler:handleError,
		callback: function(data)
		{
			buildApprovalGroup();
			showAddMembers();
			displayGroups();
		}
	});
}

function showAddGroup()
{
	$(".hideGroup").show(300);
	$("#errorDiv").empty();
	var districtId		=	document.getElementById("districtId").value;
	var searchTextId	=	document.getElementById("districtOrSchooHiddenlId").value;
	
	if((districtId!=null && districtId!="")  || (searchTextId!=null && searchTextId!=""))
	{
		$("#addGroupName").val("");
		//$("#addGroupHyperLink").show();
		//$("#groupMembersList").show();
		showAddMembers();
		displayKeyContact();
		groupRadioButton();
		$("#addApprovalGroupAndMemberDiv").fadeIn(500);
		$('#groupRadioButtonId1').prop("disabled",false);
		$('#groupRadioButtonId1').prop("checked",true);
		$('#groupRadioButtonId2').prop("disabled",false);
		$('#addGroupName').prop("disabled",false);
		$('#addGroupName').css("background-color", "#FFFFFF");
		$('#addGroupName').focus();
	}
	else{
		$("#errorDiv").html("<label class=required>* Please select District Name </label><BR>");
		$("#districtORSchoolName").focus();
	}
}

function showAddMembers()
{
	var districtId=document.getElementById("districtId").value;
	if(districtId==null || districtId==""){
		var searchTextId=document.getElementById("districtOrSchooHiddenlId").value;
		if(searchTextId!=null && searchTextId!="")
			districtId = searchTextId;
	}
	$("#loadingDiv").show();
	DistrictAjax.getApprovalGroupsAndMembersList(districtId,
	{
		async: false,
		errorHandler:handleError,
		callback: function(data)
		{
			$.each(data, function(key, value)
			{
				if(key=="groups")
				{
					$("#groupName").empty();
					$("#groupName").append("<option value='-1' selected> Select Group </option>");
					$.each(value, function(groupKey, groupvalue)
					{
						$("#groupName").append("<option value='"+groupKey+"'> "+groupvalue+" </option>");
					});
				}
				else
				if(key=="members")
				{
					$("#groupMembers").empty();
					$.each(value, function(memberKey, membervalue)
					{
						$("#groupMembers").append("<option value='"+memberKey+"'> "+membervalue+" </option>");
					});
				}
	        });
			$("#loadingDiv").hide();
		}
	});
}

function groupRadioButton()
{
	var checked = $('input[name=groupRadioButtonId]:checked').val();
	if(checked=="radioButton1")
	{
		$("groupName").val("");
		$("#groupName").attr("disabled", "disabled");	//disable groupNameList
		$('#groupName').css("background-color", "#EEEEEE");
		$("#addGroupName").removeAttr("disabled");		//enable addGroup
		$('#addGroupName').css("background-color", "#FFF");
		// code here for add new group and members
	}
	else
	if(checked=="radioButton2")
	{
		$("#addGroupName").attr("disabled", "disabled");	//disable groupNameList
		$('#addGroupName').css("background-color", "#EEEEEE");
		$("#groupName").removeAttr("disabled");				//enable addGroup
		$('#groupName').css("background-color", "#FFF");
		//Code here for add members into group
	}
}

function addGroupORMember()
{
	var errorMessage="";
	$("#privilegeForDistrictErrorDiv").empty();
	$('#addGroupName').css("background-color", "#FFF");
	$('#groupName').css("background-color", "#FFF");
	$('#groupMembers').css("background-color", "#FFF");
	
	groupRadioButton();
	
	var checked = $('input[name=groupRadioButtonId]:checked').val();
	if(checked=="radioButton1")	//add new group
	{
		var groupName = $("#addGroupName").val();
		var memberIds=$("#groupMembers :selected").val();
		
		if(groupName=="" || groupName==null)
		{
			errorMessage = errorMessage+"<label class=required>* Please enter Group Name </label><BR>";
			$('#addGroupName').css("background-color", "#F5E7E1");
			$('#addGroupName').focus();
		}
		else if(memberIds==null || memberIds=="")
		{
			errorMessage = errorMessage+"<label class=required>* Please select Member </label><BR>";
			$('#groupMembers').css("background-color", "#F5E7E1");
			$('#groupMembers').focus();
		}
		else
		{
			addGroup();
			showAddMembers();
			$("#addApprovalGroupAndMemberDiv").fadeOut(500);
		}
	}
	else if(checked=="radioButton2")	//add member to group
	{
		var groupId = $("#groupName option:selected").val();
		var memberIds=$("#groupMembers :selected").val();
		
		if(groupId=="-1")
			errorMessage = errorMessage+"<label class=required>* Please select Group Name </label><BR>";
		if(memberIds==null || memberIds=="")
			errorMessage = errorMessage+"<label class=required>* Please select Member </label><BR>";
		
		if(memberIds==null || memberIds=="")
		{
			$('#groupMembers').css("background-color", "#F5E7E1");
			$('#groupMembers').focus();
		}
		if(groupId=="-1")
		{
			$('#groupName').css("background-color", "#F5E7E1");
			$('#groupName').focus();
		}
		
		if(groupId!="-1" && memberIds!=null && memberIds!="")
		{
			addMember();
			showAddMembers();
			$("#addApprovalGroupAndMemberDiv").fadeOut(500);
		}
	}
	
	if(errorMessage!="")
	{
		$("#privilegeForDistrictErrorDiv").html(errorMessage);
		$("#privilegeForDistrictErrorDiv").fadeIn(500);
	}
}

function addGroup()
{
	var groupName = $("#addGroupName").val();
	var noOfApproval = $("#noOfApproval").val();
	var districtId=document.getElementById("districtId").value;
	if(districtId==null || districtId==""){
		var searchTextId=document.getElementById("districtOrSchooHiddenlId").value;
		if(searchTextId!=null && searchTextId!="")
			districtId = searchTextId;
	}
	var memberIds="";
	var i=0;
	$("#groupMembers :selected").each(function(){
		if(i==0)
			memberIds = "#"+memberIds+($(this).val())+"#";
		else
			memberIds = memberIds+($(this).val())+"#";
		i=i+1;
	});
	
	$("#loadingDiv").show();
	DistrictAjax.addApprovalGroup(districtId,groupName,noOfApproval,memberIds,true,
	{
		async: false,
		errorHandler:handleError,
		callback: function(data)
		{
			if(data=="GroupExist"){
				$('#virusDivId .modal-body').html("Group already exist. Please select the Group Name from drop down to add members in this group.");
				$('#virusDivId').modal('show');
				showAddGroup();
			}
			else if(data=="EsExist"){
				$('#virusDivId .modal-body').html("Sorry! You can not create group with this name.");
				$('#virusDivId').modal('show');
				showAddGroup();
			}
			else{
				buildApprovalGroup();
				$("#addConfirmation").modal("show");
			}
		}
	});
	$("#addGroupName").val("");
}

function addMember()
{
	var groupId = $("#groupName option:selected").val();
	var memberIds="";
	var i=0;
	$("#groupMembers :selected").each(function(){
		//alert("$(this).val():- "+($(this).val()));
		if(i==0)
			memberIds = "#"+memberIds+($(this).val())+"#";
		else
			memberIds = memberIds+($(this).val())+"#";
		i=i+1;
	});
	
	$("#loadingDiv").show();
	DistrictAjax.addMemberIntoApprovalGroup(groupId,memberIds+"Remove",
	{
		async: false,
		errorHandler:handleError,
		callback: function(data)
		{
			buildApprovalGroup();
			showAddMembers();
			$("#addConfirmation").modal("show");
		}
	});
}

function hideAddApprovalGroupAndMemberDiv()
{
	$("#addApprovalGroupAndMemberDiv").fadeOut(500);
	$("#privilegeForDistrictErrorDiv").empty();
	$("#privilegeForDistrictErrorDiv").fadeOut(500);
}

function showKeyContact()
{
	keyContactIdVal=null;
	keyContactDivVal=2;
	dwr.util.setValues({ dropContactType:null});
	dwr.util.setValues({ keyContactId:null,keyContactFirstName:null,keyContactLastName:null,keyContactEmailAddress:null,keyContactPhoneNumber:null,keyContactTitle:null});
	$('#keyContactFirstName').css("background-color", "");
	$('#keyContactLastName').css("background-color", "");
	$('#keyContactEmailAddress').css("background-color", "");
	$('#errorkeydiv').empty();
	$("#addKeyContactDiv").fadeIn();
	$('#dropContactType').focus();
}

function displayKeyContact()
{
	var districtId	=	document.getElementById("districtId").value;
	DistrictAjax.displayKeyContactGrid(districtId,{ 
		async: true,
		callback: function(data)
		{
			$('#keyContactTable').html(data);
			$('#addKeyContactDiv').hide();
		},
		errorHandler:handleError 
	});
}

function displayGroups()
{
	$("#errorDiv").empty();
	$('#loadingDiv').fadeIn();
	var searchTextId		=	document.getElementById("districtOrSchooHiddenlId").value;
	var textOfDistrictName	=	document.getElementById("districtORSchoolName").value.trim();

	GroupsAjax.displayGroups(textOfDistrictName,searchTextId,noOfRows,page,sortOrderStr,sortOrderType,{
		async: false,
		callback: function(data)
		{	
			document.getElementById("divMain").innerHTML = data;
			$("[data-toggle='tooltip']").tooltip();
			$('#loadingDiv').hide();
			applyScrollOnTbl();
		},
		errorHandler:handleError
	});
}

function searchByDistrict(){
	$("#errorDiv").empty();
	var searchTextId = document.getElementById("districtOrSchooHiddenlId").value;
	if(searchTextId!=null && searchTextId!=""){
		displayGroups();
	}
	else{
		$("#errorDiv").html("<label class=required>* Please select District Name </label><BR>");
		$("#districtORSchoolName").focus();
	}
}

function editGroup(groupId,memberId){
	//$('#loadingDiv').fadeIn();
	showAddMembers();
	$("#addApprovalGroupAndMemberDiv").fadeIn(500);
	$('#addGroupName').prop("disabled",true);
	$('#groupRadioButtonId1').prop("disabled",true);
	$('#groupName').prop("disabled",true);
	$('#groupRadioButtonId2').prop("disabled",true);
	$('#groupRadioButtonId2').prop("checked",true);
	$('#groupName').val(groupId);
	
	var searchMember = memberId.split(",");
	var d = document.getElementById("groupMembers");
	if (searchMember.length > 0) {
		for (var j=0;j<searchMember.length;j++) {
			var searchvalue = searchMember[j].trim();
			for (var i=0; i<d.options.length; i++)
				if (d.options[i].value == searchvalue)
					d.options[i].selected = true;
		}
	}
}
