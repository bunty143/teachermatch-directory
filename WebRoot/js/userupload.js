

var page = 1;
var noOfRows = 10;
var sortOrderStr="";
var sortOrderType="";
var count=0;
var index=-1;
var length=0;
var divid='';
var txtid='';
var hiddenDataArray = new Array();
var showDataArray = new Array();
var degreeTypeArray = new Array();
var hiddenId="";
function getDistrictAuto(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
    hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("districtName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getDistrictArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}
function getDistrictArray(districtName){
	var searchArray = new Array();
	JobUploadTempAjax.getFieldOfDistrictList(districtName,{ 
			async: false,
			callback: function(data){
			hiddenDataArray = new Array();
			showDataArray = new Array();
			for(i=0;i<data.length;i++){
				searchArray[i]=data[i].districtName;
				showDataArray[i]=data[i].districtName;
				hiddenDataArray[i]=data[i].districtId;
			}
		},
		errorHandler:handleError
		});	
	return searchArray;
}


var selectFirst="";

var fatchData= function(txtSearch,searchArray,txtId,txtdivid){
	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
			}
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}
function hideDistrictMasterDiv(dis,hiddenId,divId)
{


	document.getElementById("districtId").value="";
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			document.getElementById("districtId").value=hiddenDataArray[index];
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}


function mouseOverChk(txtdivid,txtboxId)
{	
	
	for(var i=0;i<length;i++)
	{
		$('#divResult'+txtdivid+i).mouseover({param1: i,param2:txtdivid, param3:txtboxId}, fireMouseOverEvent);
	}
}

function fireMouseOverEvent(event)
{
	for(var i=0;i<length;i++)
	{	
		document.getElementById('divResult'+event.data.param2+i).className='normal';
	}
    document.getElementById('divResult'+event.data.param2+event.data.param1).className='over';	       
   	document.getElementById(event.data.param3).value= $('#divResult'+event.data.param2+event.data.param1).text();
    index=event.data.param1;
}

function getPaging(pageno)
{
	if(pageno!='')
	{
		page=pageno;	
	}
	else
	{
		page=1;
	}
	
	noOfRows = document.getElementById("pageSize").value;
	DisplayTempUser();
}

function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
	if(pageno!=''){
		page=pageno;	
	}else{
		page=1;
	}
	sortOrderStr	=	sortOrder;
	sortOrderType	=	sortOrderTyp;
	if(document.getElementById("pageSize")!=null){
		noOfRows = document.getElementById("pageSize").value;
	}else{
		noOfRows=10;
	}
	DisplayTempUser();
}
/*========  For Handling session Time out Error ===============*/
function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert("Oops: Your Session has expired!");  document.location = 'signin.do';}
	else{alert("Server Error: "+exception.javaClassName);}
}
/*========  SearchDistrictOrSchool ===============*/
function validateUserFile()
{
	var userfile	=	document.getElementById("userfile").value;
	var districtId	=	document.getElementById("districtId").value;
	var errorCount=0;
	var aDoc="";
	$('#errordiv').empty();
	
	if(districtId==0){
		$('#errordiv').show();
		$('#errordiv').append("&#149; Please enter District Name.<br>");
		errorCount++;
		aDoc="1";
	}
	if(userfile==""){
		$('#errordiv').show();
		$('#errordiv').append("&#149; Please select xls or xlsx file to import.<br>");
		errorCount++;
		aDoc="1";
	}
	else if(userfile!="")
	{
		var ext = userfile.substr(userfile.lastIndexOf('.') + 1).toLowerCase();	
		
		var fileSize = 0;
		if ($.browser.msie==true)
	 	{	
		    fileSize = 0;	   
		}
		else
		{
			if(document.getElementById("userfile").files[0]!=undefined)
			{
				fileSize = document.getElementById("userfile").files[0].size;
			}
		}
		
		if(!(ext=='xlsx' || ext=='xls'))
		{
			$('#errordiv').show();
			$('#errordiv').append("&#149; Please select xls or xlsx file only.<br>");
			errorCount++;
			aDoc="1";
		}
		else if(fileSize>=10485760)
		{
			$('#errordiv').show();
			$('#errordiv').append("&#149; File size must be less than 10mb.<br>");
			errorCount++;
			aDoc="1";	
		}
	}
	
	if(aDoc==1){
		$('#userfile').focus();
		$('#userfile').css("background-color", "#F5E7E1");
	}
	
	if(errorCount==0){
		try{
			if(userfile!=""){
					document.getElementById("userUploadServlet").submit();
			}
		}catch(err){}
		
	}else{
		$('#errordiv').show();
		return false;
	}
}

function uploadDataUser(fileName,sessionId){
	var districtId	=	document.getElementById("districtId").value;
	$('#loadingDiv').fadeIn();
	UserUploadTempAjax.saveUserTemp(fileName,sessionId,districtId,{ 
		async: true,
		callback: function(data){
			if(data=='1'){
					 window.location.href="usertemplist.do";
			}else{
				$('#loadingDiv').hide();
				$('#errordiv').show();
				$('#errordiv').append("&#149; Field(s) "+data+" does not match.<br>");
			}
		},
		errorHandler:handleError 
	});
	
}
function DisplayTempUser()
{
	UserUploadTempAjax.displayTempUserRecords(noOfRows,page,sortOrderStr,sortOrderType,{ 
		async: true,
		callback: function(data)
		{
			$('#tempUserGrid').html(data);
			applyScrollOnTbl();
		},
		errorHandler:handleError  
	});
}

function tempUser(){
		window.location.href="importuserdetails.do";
}
function tempUserReject(sessionIdTxt){
	$('#loadingDiv').fadeIn();
	UserUploadTempAjax.deleteTempUser(sessionIdTxt,{ 
		async: true,
		callback: function(data){
			$('#loadingDiv').hide();
			tempUser();
		},
		errorHandler:handleError 
	});
}

function saveUser(){
	$('#loadingDiv').fadeIn();
	UserUploadTempAjax.saveUser({ 
		async: true,
		callback: function(data){
			$('#loadingDiv').hide();
			$('#myModalMsg').modal('show');
		},
		errorHandler:handleError 
	});
}
function trim(s)
{
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}
