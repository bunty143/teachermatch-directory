var page = 1;
var noOfRows = 10;
var sortOrderStr="";
var sortOrderType="";
function getPaging(pageno)
{
	if(pageno!='')
	{
		page=pageno;	
	}
	else
	{
		page=1;
	}
	
	noOfRows = document.getElementById("pageSize").value;
	DisplayTempQuestion();
}

function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
	if(pageno!=''){
		page=pageno;	
	}else{
		page=1;
	}
	sortOrderStr	=	sortOrder;
	sortOrderType	=	sortOrderTyp;
	if(document.getElementById("pageSize")!=null){
		noOfRows = document.getElementById("pageSize").value;
	}else{
		noOfRows=10;
	}
	DisplayTempQuestion();
}
/*========  For Handling session Time out Error ===============*/
function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert(resourceJSON.msgServerErr+": "+exception.javaClassName);}
}
/*========  SearchDistrictOrSchool ===============*/
function validateTeacherFile()
{
	var teacherfile	=	document.getElementById("teacherfile").value;
	var errorCount=0;
	var aDoc="";
	$('#errordiv').empty();
	
	if(teacherfile==""){
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.msgXlsXlsxFile+"<br>");
		errorCount++;
		aDoc="1";
	}
	else if(teacherfile!="")
	{
		var ext = teacherfile.substr(teacherfile.lastIndexOf('.') + 1).toLowerCase();	
		
		var fileSize = 0;
		if ($.browser.msie==true)
	 	{	
		    fileSize = 0;	   
		}
		else
		{
			if(document.getElementById("teacherfile").files[0]!=undefined)
			{
				fileSize = document.getElementById("teacherfile").files[0].size;
			}
		}
		
		if(!(ext=='xlsx' || ext=='xls'))
		{
			$('#errordiv').show();
			$('#errordiv').append("&#149; "+resourceJSON.msgXlsXlsxFileOnly+"<br>");
			errorCount++;
			aDoc="1";
		}
		else if(fileSize>=10485760)
		{
			$('#errordiv').show();
			$('#errordiv').append("&#149; "+resourceJSON.msgfilesizelessthan+"<br>");
			errorCount++;
			aDoc="1";	
		}
	}
	
	if(aDoc==1){
		$('#teacherfile').focus();
		$('#teacherfile').css("background-color", "#F5E7E1");
	}

	if(errorCount==0){
		try{
			//$('#loadingDiv').show();
			if(teacherfile!=""){
					document.getElementById("questionUploadServlet").submit();

			}
		}catch(err){}
		
	}else{
		$('#errordiv').show();
		return false;
	}
}

function uploadDataQuestion(fileName,sessionId,assesmentAndSection){
	$('#loadingDiv').fadeIn();
	QuestionUploadTempAjax.saveQuestionTemp(fileName,sessionId,assesmentAndSection,{
		async: true,
		callback: function(data){
			if(data=='1'){
				window.location.href="questiontemplist.do";
				/*$('#loadingDiv').hide();
				$('#errordiv').show();
				$('#errordiv').append("&#149; Field(s) "+data+" does not match.<br>");*/
			}else{
				$('#loadingDiv').hide();
				$('#errordiv').show();
				$('#errordiv').append("&#149; "+resourceJSON.msgField+" "+data+" "+resourceJSON.msgDoesNotMatch+"<br>");
			}
		},
		errorHandler:handleError 
	});
}

function DisplayTempQuestion()
{
	$('#loadingDiv').fadeIn();
	QuestionUploadTempAjax.displayTempQuestionRecords(noOfRows,page,sortOrderStr,sortOrderType,{ 
		async: true,
		callback: function(data)
		{
			$('#tempTeacherGrid').html(data);
			applyScrollOnTbl();
			$('#loadingDiv').hide();
		},
		errorHandler:handleError  
	});
}

function tempTeacher(){ 
	var assessmentId	=	document.getElementById("assessmentId").value;
	var sectionId		=	document.getElementById("sectionId").value;
	window.location.href="importquestion.do?assessmentId="+assessmentId+"&sectionId="+sectionId;
}
function tempTeacherReject(sessionIdTxt){
	var assessmentId	=	document.getElementById("assessmentId").value;
	var sectionId		=	document.getElementById("sectionId").value;
	$('#loadingDiv').fadeIn();
	QuestionUploadTempAjax.deleteTempQuestion(sessionIdTxt,{ 
		async: true,
		callback: function(data){
			$('#loadingDiv').hide();
			window.location.href="importquestion.do?assessmentId="+assessmentId+"&sectionId="+sectionId;
		},
		errorHandler:handleError 
	});
}

function saveQuestion(){
	$('#loadingDiv').fadeIn();
	QuestionUploadTempAjax.saveQuestion({
		async: true,
		callback: function(data){
			$('#loadingDiv').hide();
			document.getElementById("Msg").innerHTML=resourceJSON.msgQuestionsImportedSuccessfully;
			$('#myModalMsg').modal('show');
		},
		errorHandler:handleError 
	});
}
function trim(s)
{
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}
