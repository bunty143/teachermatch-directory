var page = 1;
var noOfRows = 10;
var sortOrderStr="";
var sortOrderType="";

function getPaging(pageno)
{
	if(pageno!='')
	{
		page=pageno;	
	}else
	{
		page=1;
	}
		noOfRows = document.getElementById("pageSize").value;
		displayDomain();
}

function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
	if(pageno!=''){
		page=pageno;	
	}else{
		page=1;
	}
	sortOrderStr	=	sortOrder;
	sortOrderType	=	sortOrderTyp;
	if(document.getElementById("pageSize")!=null){
		noOfRows = document.getElementById("pageSize").value;
	}else{
		noOfRows=10;
	}
	displayDomain();
}

function searchi4Ques()
{
	page = 1;
	displayDomain()
}


function displayDomain()
{ 
	var refChkSetStatus 		= 	document.getElementById("refChkSetStatus").value;
	var quesSetSearchText		=	document.getElementById("quesSetSearchText").value;
	var districtIdFilter		=	"";
	var headQuarterIdFilter	=	"";
	
	if(document.getElementById("districtIdFilter"))
		districtIdFilter 	=	trim(document.getElementById("districtIdFilter").value);
	
	if(document.getElementById("headQuarterIdFilter"))
		headQuarterIdFilter 	=	trim(document.getElementById("headQuarterIdFilter").value);
		
	  
	
	ReferenceChkQuestionsSetAjax.displayQuestionSet(noOfRows,page,sortOrderStr,sortOrderType,refChkSetStatus,quesSetSearchText,districtIdFilter,headQuarterIdFilter,{ 
		async: true,
		callback: function(data){     
		document.getElementById("refChkQuesSetGrid").innerHTML=data;
		applyScrollOnTbl();
		document.getElementById("addQuesSetDiv").style.display="none";
	},
	});
}


function saveQuestion()
{
	
	
	$('#errQuesSetdiv').empty();
	$('#quesSetName').css("background-color","");
	$('#districtName').css("background-color","");
	
	var quesSetId			=	trim(document.getElementById("quesSetId").value);
	var quesSetName			=	trim(document.getElementById("quesSetName").value);
	var districtId			=	"";
	if(document.getElementById("districtId"))
	districtId	=	trim(document.getElementById("districtId").value);
	var quesSetStatus		=	document.getElementsByName("quesSetStatus");
	var headquarterId	= "";
	
	if(document.getElementById("headQuarterId"))
		headquarterId 	=	trim(document.getElementById("headQuarterId").value);
	
//	alert("-------saveQuestion------");
	var quesValue ="";	
	
	var counter				=	0;
	var focuscount			=	0;
	
	for(i=0;i<quesSetStatus.length;i++)
	{
		if(quesSetStatus[i].checked	==	true)
		{
			quesValue	=	quesSetStatus[i].value;
			break;
		}
	}
	
	
	if (districtId=="" && headquarterId =="")
	{
		$('#errQuesSetdiv').append("&#149; "+resourceJSON.PlzEtrDistrict+"<br>");
		$('#errQuesSetdiv').show();
		$('#districtName').css("background-color", "#F5E7E1");
		counter++;
		focuscount++;
	}
	
 
	
	if (quesSetName=="")
	{
		$('#errQuesSetdiv').append("&#149; "+resourceJSON.PlzEtrQuest+"");
		$('#errQuesSetdiv').show();
		$('#quesSetName').css("background-color", "#F5E7E1");
		counter++;
		focuscount++;
	}
	
	if(counter==0)
	{		 
		ReferenceChkQuestionsSetAjax.saveRefChkQuestionSet(quesSetId,quesSetName,quesValue,districtId,headquarterId,{ 
			async: true,
			callback: function(data)
			{
			 
				if(data==1)
				{
					$('#errQuesSetdiv').show();
					$('#errQuesSetdiv').append("&#149; "+resourceJSON.MsgNotInsertDuplicateQuestSet+"");
				}
				else
				{
					$('#errQuesSetdiv').hide();
					$('#errQuesSetdiv').empty();
					displayDomain();
					clearQues();
				}
			},
		});
	}
	
	
	
}

function clearQues()
{
	document.getElementById("quesSetName").value="";
	document.getElementById("addQuesSetDiv").style.display="none";
	document.getElementById("quesSetId").value="";
}

function addNewQuesSet()
{
	document.getElementById("addQuesSetDiv").style.display="block";
	document.getElementById("quesSetName").value="";
	if(document.getElementById("entityType").value==1)
	{
		document.getElementById("districtId").value="";
		document.getElementById("districtName").value="";
	}
	$('#quesSetName').css("background-color", "");
	$('#districtName').css("background-color", "");
	
	$("#errQuesSetdiv").empty();

}

function editI4Question(quesSetId)
{
	$("#errQuesSetdiv").empty();
	document.getElementById("addQuesSetDiv").style.display="block";
	var quesSetStatus		=	document.getElementsByName("quesSetStatus");
	
	ReferenceChkQuestionsSetAjax.editRefChkQuestionSet(quesSetId,{ 
		async: true,
		callback: function(data)
		{
			if(data!=null)
			{
				document.getElementById("quesSetDate").value=data.dateCreated;
				document.getElementById("quesSetId").value=data.ID;
				document.getElementById("quesSetName").value=data.questionSetText;
				
				if(data.districtMaster!=null)
					document.getElementById("districtId").value=data.districtMaster.districtId;
					
					if(data.districtMaster!=null)
					document.getElementById("districtName").value=data.districtMaster.districtName;
					
					if(data.headQuarterMaster!=null)
						document.getElementById("headQuarterId").value=data.headQuarterMaster.headQuarterId;
						
					
					if(data.headQuarterMaster!=null)
					document.getElementById("headQuarterName").value=data.headQuarterMaster.headQuarterName;
					
//				document.getElementById("districtName").value=data.districtMaster.districtName;
//				document.getElementById("districtId").value=data.districtMaster.districtId;
				
				if(data.status=='A')
					quesSetStatus[0].checked=true;
				else
					quesSetStatus[1].checked=true;
			}
		},
	});
}

function activateDeactivateQues(quesSetId,quesSetStatus)
{
	document.getElementById("addQuesSetDiv").style.display="block";
	//$( "span#i4QuesTxt" ).html("<textarea id='txtQuestion' ></textarea>");
	ReferenceChkQuestionsSetAjax.activateDeactivateQuestionSet(quesSetId,quesSetStatus, { 
		async: true,
		callback: function(data)
		{
			displayDomain();
			clearQues();
		},
	});
				
}

function trim(s)
{
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}