var count=0;
var index=-1;
var length=0;
var divid='';
var txtid='';
var page = 1;
var noOfRows = 50;
var sortOrderStr="";
var sortOrderType="";
var deviceTypeAndroid=$.browser.device = (/Android|webOS|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent.toLowerCase()));
var deviceType=$.browser.device = (/iPhone|iPad|iPod/i.test(navigator.userAgent.toLowerCase()));

var hiddenDataArray = new Array();
var showDataArray = new Array();
var degreeTypeArray = new Array();
var hiddenId="";
function hideDiv()
{
	$('#docfileNotOpen').hide();
	$('#exelfileNotOpen').hide();
}
function getDistrictORSchoolAuto(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("districtORSchoolName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getDistrictORSchoolArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getDistrictORSchoolArray(districtOrSchoolName){
	var searchArray = new Array();
	/*==== Gagan : District Auto Complete will show for each case ========*/
	ManageJobOrdersAjax.getFieldOfDistrictList(districtOrSchoolName,{ 
		async: false,
		callback: function(data){
		hiddenDataArray = new Array();
		showDataArray = new Array();
		for(i=0;i<data.length;i++){
			searchArray[i]=data[i].districtName;
			showDataArray[i]=data[i].districtName;
			hiddenDataArray[i]=data[i].districtId;
		}
	},
	errorHandler:handleError 
	});	
	
	

	return searchArray;
}


var selectFirst="";

var fatchData= function(txtSearch,searchArray,txtId,txtdivid){
	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
				
			}
			
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}

function hideDistrictMasterDiv(dis,hiddenId,divId)
{
	document.getElementById("districtOrSchooHiddenlId").value="";
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		if(dis.value==""){
			//alert(" Hi ");
			//$('#schoolName').attr('readonly', true);
			//document.getElementById('schoolName').value="";
			//document.getElementById('schoolId').value="0";
			document.getElementById(hiddenId).value="";
			
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			document.getElementById("districtOrSchooHiddenlId").value=hiddenDataArray[index];
			//document.getElementById('schoolName').readOnly=false;
			//document.getElementById('schoolName').value="";
			//document.getElementById('schoolId').value="0";
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}

/*---------------------------------School Auto Complete Js Starts ----------------------------------------------------------------*/

function getSchoolAuto(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("schoolName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		
		searchArray = getSchoolArray(txtSearch.value);
		fatchData2(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getSchoolArray(schoolName){
	//alert(schoolName);
	var searchArray = new Array();
	var districtId	=	document.getElementById("districtOrSchooHiddenlId").value;
	//alert(districtId);
	HiredEeocReportAjax.getFieldOfSchoolList(districtId,schoolName,{ 
			async: false,
			callback: function(data){
			hiddenDataArray = new Array();
			showDataArray = new Array();
			for(i=0;i<data.length;i++){
				searchArray[i]=data[i].schoolName;
				showDataArray[i]=data[i].schoolName;
				hiddenDataArray[i]=data[i].schoolMaster.schoolId;
			}
		},
		errorHandler:handleError
		});	
	return searchArray;
}


selectFirst="";

var fatchData2= function(txtSearch,searchArray,txtId,txtdivid){

	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
				
			}
			
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}
function hideSchoolMasterDiv(dis,hiddenId,divId)
{
	document.getElementById("schoolId").value="";
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			document.getElementById("schoolId").value=hiddenDataArray[index];
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}




var downArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index<length-1){
			for(var i=0;i<10;i++){
				{
					if(document.getElementById('divResult'+txtdivid+i))
						var div_id=document.getElementById('divResult'+txtdivid+i);
					if(div_id)
					{
						if(div_id.className=='over')
							index=div_id.id.split('divResult'+txtdivid)[1];
						div_id.className='normal';
					}
				}
			}
			index++;
			if(document.getElementById('divResult'+txtdivid+index))
			{
				var div_id=document.getElementById('divResult'+txtdivid+index);
				div_id.className='over';
				document.getElementById(txtid).value = div_id.innerHTML;
				selectFirst=div_id.innerHTML;
			}
		}
	}
}

var upArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index>0){
			for(var i=0;i<length;i++){

				var div_id=document.getElementById('divResult'+txtdivid+i);
				if(div_id)
				{
					if(div_id.className=='over')
						index=div_id.id.split('divResult'+txtdivid)[1];
					div_id.className='normal';
				}
			}
			index--;
			if(document.getElementById('divResult'+txtdivid+index))
				document.getElementById('divResult'+txtdivid+index).className='over';
			if(txtid && document.getElementById('divResult'+txtdivid+index))
			{
				document.getElementById(txtid).value=
					document.getElementById('divResult'+txtdivid+index).innerHTML;
				selectFirst=document.getElementById('divResult'+txtdivid+index).innerHTML;
			}
		}
	}
}

var overText=function (div_value,txtdivid) {

	for(var i=0;i<length;i++)
	{
		if(document.getElementById('divResult'+i))
		{
			var div_id=document.getElementById('divResult'+i);

			if(div_id.className=='over')
				index=div_id.id.split(txtdivid)[1];
			div_id.className='normal';
		}
	}
	div_value.className = 'over';
	document.getElementById(txtid).value= div_value.innerHTML;
}
function trim(s)
{
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}
function checkForInt1(evt) {

	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==8)
		return true;

	return( (charCode >= 48 && charCode <= 57)||(charCode==13) || (charCode==44));
}

function mouseOverChk(txtdivid,txtboxId)
{	
	for(var i=0;i<length;i++)
	{
		$('#divResult'+txtdivid+i).mouseover({param1: i,param2:txtdivid, param3:txtboxId}, fireMouseOverEvent);
	}
}


function fireMouseOverEvent(event)
{
	for(var i=0;i<length;i++)
	{	
		document.getElementById('divResult'+event.data.param2+i).className='normal';
	}
    document.getElementById('divResult'+event.data.param2+event.data.param1).className='over';	       
   	document.getElementById(event.data.param3).value= $('#divResult'+event.data.param2+event.data.param1).text();
    index=event.data.param1;
}

function checkForCGInt(evt) {

	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==8 || charCode==46)
		return true;

	return( (charCode >= 48 && charCode <= 57)||(charCode==13) );
}

function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert("Oops: Your Session has expired!");  document.location = 'signin.do';}
	else{alert("Server Error: "+exception.javaClassName);}
}



// for paging and sorting
function getPaging(pageno)
{
        //alert("Paging"+pageno);
		if(pageno!='')
		{
			page=pageno;	
		}
		else
		{
			page=1;
		}
		noOfRows = document.getElementById("pageSize").value;
		
		searchRecordsBySmartPractices();
	
}


function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{	
	  if(pageno!=''){
			page=pageno;
			//alert("page :: "+page);
		}else{
			page=1;
			//alert("default");
		}
		sortOrderStr=sortOrder;
		//alert("sortOrderStr :: "+sortOrderStr);
		sortOrderType=sortOrderTyp;
		if(document.getElementById("pageSize")!=null){
			//alert("AA")
			noOfRows = document.getElementById("pageSize").value;
		}else{
		//	alert("BB");
			noOfRows=50;
		}
		searchRecordsBySmartPractices();
		
	
}

function activecityType(){
	document.getElementById("certType").value='';
}


function tpJbIEnable()
{
	var noOrRow = document.getElementById("tblGridEEC").rows.length;
	for(var j=1;j<=noOrRow;j++)
	{	
		$('#cg'+j).tooltip();
		$('#cgn'+j).tooltip();
	}
}

//**************** global defined variable for exporting *******************
var f_JobOrderType     
var f_searchTextId	
var f_schoolId		
var f_jobOrderId	    
var	f_firstName       
var	f_lastName        
var	f_emailAddress    

//******************************************************************************
function recordsBySmartPractices()
{
	page=1;
	searchRecordsBySmartPractices()
}


function searchRecordsBySmartPractices()
{ 
	
	$('#loadingDiv').fadeIn();
		
	var fname=document.getElementById("firstName").value;
	var lname=document.getElementById("lastName").value;
	var email=document.getElementById("emailAddress").value;
	var assessmentDate=document.getElementById("assessmentDate").value;
	
	SmartPracticesAssessmentAjax.displayRecordsByEntityType(noOfRows,page,sortOrderStr,sortOrderType,fname,lname,email,assessmentDate,{ 
		async: true,
		callback: function(data)
		{	
		//alert(data);
		document.getElementById("divMainHired").innerHTML	=data;
		$("[data-toggle='tooltip']").tooltip();
		$('#loadingDiv').hide();
		applyScrollOnTblVisit();
		},
	errorHandler:handleError
	});
	
}



function generateSmartPracticesEXL()
{
	$('#loadingDiv').fadeIn();
	
	var fname=document.getElementById("firstName").value;
	var lname=document.getElementById("lastName").value;
	var email=document.getElementById("emailAddress").value;
	var assessmentDate=document.getElementById("assessmentDate").value;
	
	SmartPracticesAssessmentAjax.displayRecordsByEntityTypeEXL(noOfRows,page,sortOrderStr,sortOrderType,fname,lname,email,assessmentDate,{
		async: true,
		callback: function(data)
		{
		
		    $('#loadingDiv').hide();
			if(deviceType)
			{					
					$("#exelfileNotOpen").css({"z-index":"3000"});
			    	$('#exelfileNotOpen').show();
					
			}
			else
			{
				try
				{
					document.getElementById('ifrmTrans').src = "smartPracticesAssessment/"+data+"";
				}
				catch(e)
				{alert(e);}
			}
		},
	errorHandler:handleError
	});

}

function generateSmartPracticesPDF()
{
	$('#loadingDiv').fadeIn();
	var fname=document.getElementById("firstName").value;
	var lname=document.getElementById("lastName").value;
	var email=document.getElementById("emailAddress").value;
	var assessmentDate=document.getElementById("assessmentDate").value;
	
	SmartPracticesAssessmentAjax.displayRecordsByEntityTypePDF(noOfRows,page,sortOrderStr,sortOrderType,fname,lname,email,assessmentDate,{
		async: true,
		callback: function(data)
		{
		//alert(data);
		$('#loadingDiv').hide();	
		if(deviceType || deviceTypeAndroid)
		{
			window.open(data,'left=200,top=50,width=700,height=600,toolbar=1,resizable=0');			
		}		
		else
		{
			$('#modalDownloadHiredCandidates').modal('hide');
			document.getElementById('ifrmCJS').src = ""+data+"";
			try{
				$('#modalDownloadHiredCandidates').modal('show');
			}catch(err)
			{}		
	     }		
	     return false;
}});
		
	

}
 
function generateSmartPracticesPrintPre()
{
	//alert("inside print");
	$('#loadingDiv').fadeIn();
	var fname=document.getElementById("firstName").value;
	var lname=document.getElementById("lastName").value;
	var email=document.getElementById("emailAddress").value;
	var assessmentDate=document.getElementById("assessmentDate").value;
	SmartPracticesAssessmentAjax.displayRecordsByEntityTypePrintPreview(noOfRows,page,sortOrderStr,sortOrderType,fname,lname,email,assessmentDate,{
		async: true,
		callback: function(data)
		{	
    	
			$('#loadingDiv').hide();
			$('#printhiredApplicantsDataTableDiv').html(data);
			$("[data-toggle='tooltip']").tooltip();
			applyScrollOnPrintTable();
			$("#printHiredApplicantsDiv").modal('show');
		},
	errorHandler:handleError
	});
	
} 


var isSafari = /Safari/.test(navigator.userAgent) && /Apple Computer/.test(navigator.vendor);
function printSmartPracticesDATA()
{
	$('#loadingDiv').show();
	var fname=document.getElementById("firstName").value;
	var lname=document.getElementById("lastName").value;
	var email=document.getElementById("emailAddress").value;
	var assessmentDate=document.getElementById("assessmentDate").value;
	SmartPracticesAssessmentAjax.displayRecordsByEntityTypePrintPreview(noOfRows,page,sortOrderStr,sortOrderType,fname,lname,email,assessmentDate,{
		async: true,
		callback: function(data)
		{	
			$('#loadingDiv').hide();
			try{ 
			if (isSafari && !deviceType)
			    {
			    	window.document.write(data);
					window.print();
			    }else
			    {
			    	var newWindow = window.open();
			    	newWindow.document.write(data);	
			    	newWindow.print();
				 } 
			}catch (e) 
			{
				$('#printmessage1').modal('show');							 
			}
			
		},
	errorHandler:handleError
	});
}

 
 function canelPrint()
 {
	 $('#errDateCheckDiv').hide();
	 $('#printOfferReadyDiv').hide();
	 $('#loadingDiv').hide();
		
 }

/*function chkschoolBydistrict()
{
	var districtid = trim(document.getElementById('districtORSchoolName').value);
	if(trim(document.getElementById('entityType').value)==1)
	if(districtid=="")
	{
		//document.getElementById('schoolName').disabled=true;
	}
	else
	{
		//document.getElementById('schoolName').disabled=false;
	}
}*/

/*function displayAdvanceSearch()
{
	$('#searchLinkDiv').hide();
	$('#hidesearchLinkDiv').show();
	$('#advanceSearchDiv').slideDown('slow');
}

function hideAdvanceSearch()
{
	$('#searchLinkDiv').show();
	$('#hidesearchLinkDiv').hide();
	$('#advanceSearchDiv').slideUp('slow');
	//$('#advanceSearchDiv').toggle( "slide", { direction: "top" }, 1000);
}*/



