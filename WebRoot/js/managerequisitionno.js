var page = 1;
var noOfRows = 10;
var sortOrderStr="";
var sortOrderType="";
function getPaging(pageno)
{	
	if(pageno!='')
	{
		page=pageno;	
	}else
	{
		page=1;
	}
	
	noOfRows = document.getElementById("pageSize").value;
	var entityID	=	document.getElementById("entityID").value;
	DisplayRequisitionNos();
}
function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
	var entityID	=	document.getElementById("entityID").value;
	if(pageno!=''){
		page=pageno;	
	}else{
		page=1;
	}
	sortOrderStr	=	sortOrder;
	sortOrderType	=	sortOrderTyp;
	if(document.getElementById("pageSize")!=null){
		noOfRows = document.getElementById("pageSize").value;
	}else{
		noOfRows=10;
	}
	DisplayRequisitionNos();
}


function addNewRequisitionNo(){
	$("#reqNoDiv").show();
	$('#reqno').css("background-color", "");
	$('#reqno').val("");
	if($('#entityID').val()==1){
		$('#districtName').val("");
		$('#districtName').focus();
		$('#districtName').css("background-color", "");
		document.getElementById('districtId').value="";
		document.getElementById('districtRequisitionId').value="";
	}else{
		$('#reqno').focus();
	}
	$('#errordiv').empty();
}
function cancel(){
	$('#reqno').val("");
	$("#reqNoDiv").hide();
	$('#errordiv').empty();
}
function searchReqNoList()
{
	page = 1;
	DisplayRequisitionNos();
}
function chkForEnterSearchReqNo(evt)
{
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==13)
	{
		page = 1;
		DisplayRequisitionNos();
	}	
}
function DisplayRequisitionNos(){
	$("#loadingDiv").show();
	
	var searchTextId="";
	var requisitionId="";
	var entityID	=	document.getElementById("entityID").value;
	try{
	requisitionId= trim(document.getElementById("requisitionNoId").value);
	}catch(e){};
	
	if(entityID==1){
		searchTextId=document.getElementById('districtIdFilter').value;
	}else if(entityID==2){
		searchTextId=document.getElementById('districtId').value;
	}
	if(searchTextId==""){
		searchTextId=0;
	}
	if(requisitionId=="" || requisitionId==null)
	{
		
		requisitionId='0';	
		
	}
	
		
	//alert("searchTextId >>>>"+searchTextId+"\n"+$('#entityID').val());
	
	RequisitionAjax.DisplayRequisitionNos(entityID,searchTextId,noOfRows,page,sortOrderStr,sortOrderType,requisitionId,{ 
		async: true,
		callback: function(data){
			$("#reqNoGrid").html(data);
			applyScrollOnTbl();
			$("#loadingDiv").hide();
		},
		errorHandler:handleError 
	});
}
function chkForEnterSaveReqNo(evt){
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==13)
	{
		page = 1;
		saveRequisitionNo();
	}
}
function saveRequisitionNo(){
	var reqno=$.trim($("#reqno").val());
	var districtId=$.trim($("#districtId").val());
	var districtRequisitionId=$.trim($("#districtRequisitionId").val());
	var posType=$("#posType").val();

	var counter=0;
	var focuscount=0;
	$('#errordiv').empty();
	if(districtId==""){
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.PlzEtrDistrict+"<br/>");
		$('#districtName').css("background-color", "#F5E7E1");
		if(focuscount	==	0)
			$('#districtName').focus();
		counter++;
		focuscount++;
	}
	if(reqno==""){
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.msgEnterRequisitionNo+"<br/>");
		$('#reqno').css("background-color", "#F5E7E1");
		if(focuscount	==	0)
			$('#reqno').focus();
		counter++;
		focuscount++;
	}
	if(counter==0){
		RequisitionAjax.saveRequisitionNo(districtRequisitionId,districtId,reqno,posType,{ 
			async: true,
			callback: function(data){
				if(data==1){
					$('#errordiv').empty();
					$('#errordiv').show();
					$('#errordiv').append("&#149;"+resourceJSON.msgDuplicateRequisitionNo+"<br>");
					$('#reqno').focus();
					$('#reqno').css("background-color", "#F5E7E1");
				}else{
					$("#reqNoDiv").hide();
					DisplayRequisitionNos();
				}
				
			},
			errorHandler:handleError 
		});
	}
}
function getRequisitionNo(districtRequisitionId){
	RequisitionAjax.getRequisitionNo(districtRequisitionId,{ 
		async: true,
		callback: function(data){
			$("#districtRequisitionId").val(data.districtRequisitionId);
			$("#districtId").val(data.districtMaster.districtId);
			$("#districtName").val(data.districtMaster.districtName);			
			$("#reqno").val(data.requisitionNumber);			
			$("#posType").val(data.posType);
			$("#reqNoDiv").show();
		},
		errorHandler:handleError 
	});
}

function deleteRequisitionNo(districtRequisitionId){
	RequisitionAjax.deleteRequisitionNo(districtRequisitionId,{ 
		async: true,
		callback: function(data){
			DisplayRequisitionNos();
		},
		errorHandler:handleError 
	});
}
 
/**********************************************************************************************/
var count=0;
var index=-1;
var length=0;
var divid='';
var txtid='';
var hiddenDataArray = new Array();
var showDataArray = new Array();
var degreeTypeArray = new Array();
var hiddenId="";
var districtNameFilter=0;
function getDistrictMasterAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		if(districtNameFilter==1)
			document.getElementById("districtNameFilter").focus();
		else
			document.getElementById("districtName").focus();
		
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getDistrictMasterArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getDistrictMasterArray(districtName){
	var searchArray = new Array();
	ManageJobOrdersAjax.getFieldOfDistrictList(districtName,{ 
		async: false,
		callback: function(data){
		hiddenDataArray = new Array();
		showDataArray = new Array();
		for(i=0;i<data.length;i++){
			searchArray[i]=data[i].districtName;
			showDataArray[i]=data[i].districtName;
			hiddenDataArray[i]=data[i].districtId;
		}
	},
	errorHandler:handleError 
	});	
	return searchArray;
}


var selectFirst="";

var fatchData= function(txtSearch,searchArray,txtId,txtdivid){
	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
				
			}
			
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}

function hideDistrictMasterDiv(dis,hiddenId,divId)
{
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			
		}
		
	}else{
			if(document.getElementById(hiddenId))
				document.getElementById(hiddenId).value="";
	}
	
	if(document.getElementById(divId))
	{	
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}

var downArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index<length-1){
			for(var i=0;i<10;i++){
				{
					if(document.getElementById('divResult'+txtdivid+i))
						var div_id=document.getElementById('divResult'+txtdivid+i);
					if(div_id)
					{
						if(div_id.className=='over')
							index=div_id.id.split('divResult'+txtdivid)[1];
						div_id.className='normal';
					}
				}
			}
			index++;
			if(document.getElementById('divResult'+txtdivid+index))
			{
				var div_id=document.getElementById('divResult'+txtdivid+index);
				div_id.className='over';
				document.getElementById(txtid).value = div_id.innerHTML;
				selectFirst=div_id.innerHTML;
			}
		}
	}
}

var upArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index>0){
			for(var i=0;i<length;i++){

				var div_id=document.getElementById('divResult'+txtdivid+i);
				if(div_id)
				{
					if(div_id.className=='over')
						index=div_id.id.split('divResult'+txtdivid)[1];
					div_id.className='normal';
				}
			}
			index--;
			if(document.getElementById('divResult'+txtdivid+index))
				document.getElementById('divResult'+txtdivid+index).className='over';
			if(txtid && document.getElementById('divResult'+txtdivid+index))
			{
				document.getElementById(txtid).value=
					document.getElementById('divResult'+txtdivid+index).innerHTML;
				selectFirst=document.getElementById('divResult'+txtdivid+index).innerHTML;
			}
		}
	}
}

var overText=function (div_value,txtdivid) {

	for(var i=0;i<length;i++)
	{
		if(document.getElementById('divResult'+i))
		{
			var div_id=document.getElementById('divResult'+i);

			if(div_id.className=='over')
				index=div_id.id.split(txtdivid)[1];
			div_id.className='normal';
		}
	}
	div_value.className = 'over';
	document.getElementById(txtid).value= div_value.innerHTML;
}
function trim(s)
{
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}
function mouseOverChk(txtdivid,txtboxId)
{	
	for(var i=0;i<length;i++)
	{
		$('#divResult'+txtdivid+i).mouseover({param1: i,param2:txtdivid, param3:txtboxId}, fireMouseOverEvent);
	}
}


function fireMouseOverEvent(event)
{
	for(var i=0;i<length;i++)
	{	
		document.getElementById('divResult'+event.data.param2+i).className='normal';
	}
    document.getElementById('divResult'+event.data.param2+event.data.param1).className='over';	       
   	document.getElementById(event.data.param3).value= $('#divResult'+event.data.param2+event.data.param1).text();
    index=event.data.param1;
}

function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert(resourceJSON.msgSomeerror+": "+exception.javaClassName);}
}
function openTootip(){	
	$('#popoverData').tooltip();
}