	var clipboardNode = null;
	var pasteMode = null;

function copyPaste(action, node) 
{
		//alert("---- Gagan : action ==== "+action+" and node value  "+node);
		switch( action ) {
		case "cut":
		case "copy":
			clipboardNode = node;
			pasteMode = action;
			break;
		case "paste":
			if( !clipboardNode ) {
				alert("Clipoard is empty.");
				break;
			}
			if( pasteMode == "cut" ) {
				// Cut mode: check for recursion and remove source
				var isRecursive = false;
				var cb = clipboardNode.toDict(true, function(dict){
					// If one of the source nodes is the target, we must not move
					if( dict.key == node.data.key )
						isRecursive = true;
				});
				if( isRecursive ) {
					alert("Cannot move a node to a sub node.");
					return;
				}
				node.addChild(cb);
				clipboardNode.remove();
			} else {
				// Copy mode: prevent duplicate keys:
				var cb = clipboardNode.toDict(true, function(dict){
					dict.title = "Copy of " + dict.title;
					delete dict.key; // Remove key, so a new one will be created
				});
				node.addChild(cb);
			}
			clipboardNode = pasteMode = null;
			break;
		default:
			alert("Unhandled clipboard action '" + action + "'");
		}
	};
	
	
	// --- Contextmenu helper --------------------------------------------------
	function bindContextMenu(span) {
		//alert("------ bindContextMenu ----------");
		// Add context menu to this node:
		$(span).contextMenu({menu: "myMenu"}, function(action, el, pos) {
			// The event was bound to the <span> tag, but the node object
			// is stored in the parent <li> tag
			var node = mc$.ui.dynatree.getNode(el);
//			//alert(" action "+action+" === delete ===="+node);
			switch( action ) {
			case "cut":
			case "copy":
			case "paste":
				copyPaste(action, node);
				break;
			/*============ Gagan : For Edit Folder Code Here ===================*/
			case "edit":
			editNode(node);
			break;
			case "create":
			createUserFolder();
			/*var rootNode = mc$("#tree").dynatree("getActiveNode");
			var childNode = rootNode.addChild({
				title: "New Folder",
				tooltip: "",
				isFolder: true
			});*/
			break;
			default:
				//alert("---------------------: appply action '" + action + "' to node " + node);
			}
		});
	};
	
	function createUserFolder()
	{
		//alert(" Create Folder");
			var rootNode = mc$("#tree").dynatree("getActiveNode");
			//alert(" rootNode "+rootNode+" Key "+rootNode.data.key)
			var parentId=rootNode.data.key;
			//alert("Before Created"+parentId);
			CandidateGridAjax.createUserFolder(parentId,
			{ 
				async: false,
				errorHandler:handleError,
				callback:function(data)
				{	
					//alert("=== data ===="+data);
				}
			});
			var childNode = rootNode.addChild({
			title: "New Folder",
			tooltip: "",
			isFolder: true
		});
		//alert("Folder Created")
	}
	
	
	function editNode(node){
		//alert("Calling Edit Function")
			var prevTitle = node.data.title,
				tree = node.tree;
			// Disable dynatree mouse- and key handling
			tree.$widget.unbind();
			// Replace node with <input>
			$(".dynatree-title", node.span).html("<input id='editNode' class='span2' maxlength='50' value='" + prevTitle + "'>");
			// Focus <input> and bind keyboard handler
			$("input#editNode")
				.focus()
				.keydown(function(event){
					switch( event.which ) {
					case 27: // [esc]
						// discard changes on [esc]
						$("input#editNode").val(prevTitle);
						$(this).blur();
						break;
					case 13: // [enter]
						// simulate blur to accept new value
						$(this).blur();
						break;
					}
				}).blur(function(event){
					// Accept new value, when user leaves <input>
					var title = $("input#editNode").val();
					alert("Title "+title+" node Id "+node.data.key);
					node.setTitle(title);
					// Re-enable mouse and keyboard handlling
					tree.$widget.bind();
					node.focus();
				});
		}

	
	
	function jqueryScript()
	{
		alert("Hi");
		// --- Init dynatree during startup ----------------------------------------
		// mc$ is now an alias to the jQuery function; creating the new alias is optional.
		 var mc$ = jQuery.noConflict();
			mc$(function(){
				//alert("Treeview.js")
				mc$("#tree").dynatree({
					persist: true,
					onActivate: function(node) {
					//alert("Hi");
						$("#echoActivated").text(node.data.title + ", key=" + node.data.key);
					},
					onClick: function(node, event) {
						// Close menu on click
						if( $(".contextMenu:visible").length > 0 ){
							$(".contextMenu").hide();
//							return false;
						}
					},
					onKeydown: function(node, event) {
						// Eat keyboard events, when a menu is open
						if( $(".contextMenu:visible").length > 0 )
							return false;

						switch( event.which ) {

						// Open context menu on [Space] key (simulate right click)
						case 32: // [Space]
							$(node.span).trigger("mousedown", {
								preventDefault: true,
								button: 2
								})
							.trigger("mouseup", {
								preventDefault: true,
								pageX: node.span.offsetLeft,
								pageY: node.span.offsetTop,
								button: 2
								});
							return false;

						// Handle Ctrl-C, -X and -V
						case 67:
							if( event.ctrlKey ) { // Ctrl-C
								copyPaste("copy", node);
								return false;
							}
							break;
						case 86:
							if( event.ctrlKey ) { // Ctrl-V
								copyPaste("paste", node);
								return false;
							}
							break;
						case 88:
							if( event.ctrlKey ) { // Ctrl-X
								copyPaste("cut", node);
								return false;
							}
							break;
						}
					},
					/*Bind context menu for every node when it's DOM element is created.
					  We do it here, so we can also bind to lazy nodes, which do not
					  exist at load-time. (abeautifulsite.net menu control does not
					  support event delegation)*/
					onCreate: function(node, span){
					//alert(" onCreate Method is calling "); 
						bindContextMenu(span);
					},
					onFocus: function(node) {
						$("#echoFocused").text(node.data.title);
					},
					onBlur: function(node) {
						$("#echoFocused").text("-");
					},
					/*Load lazy content (to show that context menu will work for new items too)*/
					onLazyRead: function(node){
						node.appendAjax({
							url: "contextmenu/sample-data2.json"
						});
					},
				/* D'n'd, just to show it's compatible with a context menu.
				   See http://code.google.com/p/dynatree/issues/detail?id=174 */
				dnd: {
					preventVoidMoves: true, // Prevent dropping nodes 'before self', etc.
					onDragStart: function(node) {
						return true;
					},
					onDragEnter: function(node, sourceNode) {
						if(node.parent !== sourceNode.parent)
							return false;
						return ["before", "after"];
					},
					onDrop: function(node, sourceNode, hitMode, ui, draggable) {
						sourceNode.move(node, hitMode);
					}
				}
				});
				
				
		/* ---------------------- Create Folder Method Start here --------------- */
				$("#btnAddCode").click(function(){
					//alert("Btn Add Click ")
					// Sample: add an hierarchic branch using code.
					// This is how we would add tree nodes programatically
					//$("#echoActivated").text(node.data.title + ", key=" + node.data.key);
					//alert(node.data.title+" === key ==="+node.data.key)
					//var rootNode = mc$("#tree").dynatree("getRoot");/*---------  This Line is Used     */
					createUserFolder();
					//alert("J---------")
					/*var rootNode = mc$("#tree").dynatree("getActiveNode");
					var childNode = rootNode.addChild({
						title: "New Folder",
						tooltip: "",
						isFolder: true
					});*/
					/*
					childNode.addChild({
						title: "Document using a custom icon",
						icon: "customdoc1.gif"
					});
					*/
				});
				
				
		/*---------------- ------------- Create Folder Method End  here --------------- */

		/* ------------------Implement inline editing for a dynatree node ------------------*/

		// ----------

		mc$(function(){
			var isMac = /Mac/.test(navigator.platform);
			mc$("#tree").dynatree({
				title: "Event samples",
				onClick: function(node, event) {
					if( event.shiftKey ){
						editNode(node);
						return false;
					}
				},
				onDblClick: function(node, event) {
					editNode(node);
					return false;
				},
				onKeydown: function(node, event) {//-----------------
					if( $(".contextMenu:visible").length > 0 )
							return false;

						switch( event.which ) {
						// Open context menu on [Space] key (simulate right click)
						case 32: // [Space]
							$(node.span).trigger("mousedown", {
								preventDefault: true,
								button: 2
								})
							.trigger("mouseup", {
								preventDefault: true,
								pageX: node.span.offsetLeft,
								pageY: node.span.offsetTop,
								button: 2
								});
							return false;

						// Handle Ctrl-C, -X and -V
						case 67:
							if( event.ctrlKey ) { // Ctrl-C
								copyPaste("copy", node);
								return false;
							}
							break;
						case 86:
							if( event.ctrlKey ) { // Ctrl-V
								copyPaste("paste", node);
								return false;
							}
							break;
						case 88:
							if( event.ctrlKey ) { // Ctrl-X
								copyPaste("cut", node);
								return false;
							}
							break;
						
						case 113: // [F2]
						editNode(node);
						return false;
						case 13: // [enter]
							if( isMac ){
								editNode(node);
								return false;
							}
						}
						
					
					/*
					switch( event.which ) {
					case 113: // [F2]
						editNode(node);
						return false;
					case 13: // [enter]
						if( isMac ){
							editNode(node);
							return false;
						}
					}*/ //======== Function End ============
				}
				//---------------
				
			});
		});
				
				
				
			});


		//<%-- ------------------------------------------------------------------------------------------------------------------%>
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	