<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>

<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type='text/javascript' src="js/ac_quicktime.js?ver=${resourceMap['js/ac_quicktime.js']}"></script>
<script type="text/javascript" src="js/teacher/portfolio.js?ver=${resourceMap['js/teacher/portfolio.js']}"></script>
<script type="text/javascript" src="js/teacher/pfExperiences.js?ver=${resourceMap['js/teacher/pfExperiences.js']}"></script>
<script type="text/javascript" src="dwr/interface/TeacherProfileViewInDivAjax.js?ver=${resouceMap['TeacherProfileViewInDivAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/PFExperiences.js?ver=${resouceMap['PFExperiences.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/StateAjax.js?ver=${resourceMap['StateAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/DistrictPortfolioConfigAjax.js?ver=${resourceMap['DistrictPortfolioConfigAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/DistrictAjax.js?ver=${resourceMap['DistrictAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/interface/PFLicenseEducation.js?ver=${resourceMap['js/teacher/pfLicenseEducation.js']}"></script>


<script type="text/javascript" src="dwr/interface/HrmsLicenseAjax.js?ver=${resourceMap['HrmsLicenseAjax.ajax']}"></script>
<script type='text/javascript' src="js/teacher/dynamicPortfolio_Experiences.js?ver=${resouceMap['js/teacher/dynamicPortfolio_Experiences.js']}"></script>
<script src="calender/js/jscal2_dspq.js"></script>
<script src="calender/js/lang/en.js"></script>
<link rel="stylesheet" type="text/css" href="calender/css/jscal2.css" />
<link rel="stylesheet" type="text/css" href="calender/css/border-radius.css" />
<link rel="stylesheet" type="text/css" href="calender/css/gold/gold.css" />
<link rel="stylesheet" href="css/dialogbox.css" />
<%@ page import="tm.utility.Utility" %>

<style>
.divwidth
{
float: left;
width: 40px;
}
 .table td {
 padding-left: 5px;
 padding-right: 5px;
 
}

.miamionly{
	display :none !important;
}
.miamionly_show{
	display :block !important;
}

</style>

<script>

function applyScrollOnTblLicense()
{	
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#divLicneseGridCertifications').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 380,
        width: 955,
        minWidth: null,
        minWidthAuto: false,
       	colratio:[250,200,290,215],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
    });			
}

function applyScrollOnTblLicenseDate()
{	
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#divLicneseGridCertificationsGridD').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 380,
        width: 955,
        minWidth: null,
        minWidthAuto: false,
       	colratio:[320,360,275],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
    });			
}

function applyScrollOnTblLicenseArea()
{	
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#divLicneseGridCertificationsGridA').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 380,
        width: 955,
        minWidth: null,
        minWidthAuto: false,
       	colratio:[250,200,290,215],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
    });			
}

function applyScrollOnTblNBPTSCertification()
{	
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#divLicneseNBPTSCertifications').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 380,
        width: 955,
        minWidth: null,
        minWidthAuto: false,
       	colratio:[350,315,390],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
    });			
}


function showcertiDiv()
{
	$('#draggableDivMaster').modal('show');
}
function showAffidavitDiv()
{
	$('#affidavitDataDiv').modal('show');
}

function openPDF()
{
var left = (screen.width/2)-(600/2);
var top = (screen.height/2)-(700/2);
return window.open('http://jobs.dadeschools.net/teachers/pdf/3506.pdf','', 'width=800, height=620,top='+top+',left='+left);
}
</script>
<script type="text/javascript">//<![CDATA[
      var cal = Calendar.setup({
          onSelect: function(cal) { cal.hide() },
          showTime: true
      });
</script>
<input type="hidden" id="isJobAssessment" name="isJobAssessment" value=""/>
<input type="hidden" id="isMiami" name="isMiami" value="${isMiami}"/>
<input type="hidden" id="updateFlag" name="updateFlag" />
<input type="hidden" id="isnontj" name="isnontj" value=""/>
<input type="hidden" id="isSchoolSupportPhiladelphia" value=""/>
<input type="hidden" id="isPrinciplePhiladelphia" value="0"/>
<input type="hidden" id="jobcategoryDsp" value=""/>
<input type="hidden" id="jobTitleFeild" value=""/>
<input type="hidden" id="headQuaterIdForDspq" value=""/>
<input type="hidden" id="dspqName" value=""/>
<input type="hidden" id="staffType" name="staffType">

<input type="hidden" id="isDspqReqForKelly" value="${isDspqReqForKelly}"/>
<input type="hidden" id="isKelly" value="${isKelly}"/>


<div class="modal hide" id="topErrorMessageDSPQ" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog-for-cgcoverletter">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="showDSPQDiv();">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div class="modal-body" style="max-height: 400px;overflow-y:auto;"> 		
		<div class="control-group">
			<div class='divErrorMsg' id='divErrorMsg_dynamicPortfolio' style="display: none; padding-left: 25px;"></div>			
			<div class="row portfolio_Subheading" id='divErrorMsg_top_pInfo_header' style="display: none; padding-left: 30px;"><spring:message code="headPrsnlInfo" /></div>
			<div class='row divErrorMsg' id='divErrorMsg_top_pInfo' style="display: none; padding-left: 40px;"></div>
			
			<div class="row portfolio_Subheading" id='divErrorMsg_top_DOB_header' style="display: none; padding-left: 30px;"><spring:message code="lblDOB" /></div>
			<div class='row divErrorMsg' id='divErrorMsg_top_DOB' style="display: none; padding-left: 40px;"></div>
			
			<div class="row portfolio_Subheading" id='divErrorMsg_top_Address_pInfo_header' style="display: none; padding-left: 30px;"><spring:message code="lblAdd" /></div>
			<div class='row divErrorMsg' id='divErrorMsg_top_Address' style="display: none; padding-left: 40px;"></div>
			
			<div class="row portfolio_Subheading" id='divErrorMsg_top_preAddress_pInfo_header' style="display: none; padding-left: 30px;">Present Address</div>
			<div class='row divErrorMsg' id='divErrorMsg_top_preAddress' style="display: none; padding-left: 40px;"></div>
			
			<div class="row portfolio_Subheading" id='divErrorMsg_top_Curr_Emp_header' style="display: none; padding-left: 30px;"><spring:message code="lblCurrentEmploymnt" /></div>
			<div class='row divErrorMsg' id='divErrorMsg_top_Curr_Emp' style="display: none; padding-left: 40px;"></div>
			
			<div class="row portfolio_Subheading" id='divErrorMsg_top_aca_header' style="display: none; padding-left: 30px;"><spring:message code="headAcad" /></div>
			<div class='row divErrorMsg' id='divErrorMsg_top_aca' style="display: none; padding-left: 40px;"></div>
			
			<div class="row portfolio_Subheading" id='divErrorMsg_top_certi_header' style="display: none; padding-left: 30px;"><spring:message code="lnkCerti/Lice" /></div>
			<div class='row divErrorMsg' id='divErrorMsg_top_certi' style="display: none; padding-left: 40px;"></div>
			
			<div class="row portfolio_Subheading" id='divErrorMsg_top_gk_header' style="display: none; padding-left: 30px;"> <spring:message code="lblPFGKE" /></div>
			<div class='row divErrorMsg' id='divErrorMsg_top_gk' style="display: none; padding-left: 40px;"></div>
			
			<div class="row portfolio_Subheading" id='divErrorMsg_top_subarea_header' style="display: none; padding-left: 30px;"><spring:message code="lblPFSAE" /></div>
			<div class='row divErrorMsg' id='divErrorMsg_top_subarea' style="display: none; padding-left: 40px;"></div>
			
			<div class="row portfolio_Subheading" id='divErrorMsg_top_emphis_header' style="display: none; padding-left: 30px;"><spring:message code="lblEmplHis" /></div>
			<div class='row divErrorMsg' id='divErrorMsg_top_emphis' style="display: none; padding-left: 40px;"></div>
			
			<div class="row portfolio_Subheading" id='divErrorMsg_top_AddDoc_header' style="display: none; padding-left: 30px;"><spring:message code="lnkAddDocu" /></div>
			<div class='row divErrorMsg' id='divErrorMsg_top_AddDoc' style="display: none; padding-left: 40px;"></div>
			
			<div class="row portfolio_Subheading" id='divErrorMsg_top_Video_header' style="display: none; padding-left: 30px;"><spring:message code="lblVidLnk" /></div>
			<div class='row divErrorMsg' id='divErrorMsg_top_Video' style="display: none; padding-left: 40px;"></div>
			
			<div class="row portfolio_Subheading" id='divErrorMsg_top_ref_header' style="display: none; padding-left: 30px;"><spring:message code="lblRef" /></div>
			<div class='row divErrorMsg' id='divErrorMsg_top_ref' style="display: none; padding-left: 40px;"></div>
			
			<div class="row portfolio_Subheading" id='divErrorMsg_top_lang_header' style="display: none; padding-left: 30px;"><spring:message code="lblLanguage" /></div>
            <div class='row divErrorMsg' id='divErrorMsg_top_lang' style="display: none; padding-left: 40px;"></div>
			
			<div class="row portfolio_Subheading" id='divErrorMsg_top_invol_header' style="display: none; padding-left: 30px;"><spring:message code="lblIVWork" /></div>
			<div class='row divErrorMsg' id='divErrorMsg_top_invol' style="display: none; padding-left: 40px;"></div>
			
			<div class="row portfolio_Subheading" id='divErrorMsg_top_honors_header' style="display: none; padding-left: 30px;"><spring:message code="lblHonors" /></div>
			<div class='row divErrorMsg' id='divErrorMsg_top_honors' style="display: none; padding-left: 40px;"></div>
			
			<div class="row portfolio_Subheading" id='divErrorMsg_top_StdTchExp_header' style="display: none; padding-left: 30px;"><spring:message code="lblStudentTeachingExperience" /></div>
			<div class='row divErrorMsg' id='divErrorMsg_top_StdTchExp' style="display: none; padding-left: 40px;"></div>
			
			<div class="row portfolio_Subheading" id='divErrorMsg_top_TFA_header' style="display: none; padding-left: 30px;">Teach For America (TFA) Affiliate</div>
			<div class='row divErrorMsg' id='divErrorMsg_top_TFA' style="display: none; padding-left: 40px;"></div>
			
			<div class="row portfolio_Subheading" id='divErrorMsg_top_Residency_header' style="display: none; padding-left: 30px;">Residency</div>
			<div class='row divErrorMsg' id='divErrorMsg_top_Residency' style="display: none; padding-left: 40px;"></div>		
			
			<div class="row portfolio_Subheading" id='divErrorMsg_top_currMemIn_header' style="display: none; padding-left: 30px;"> 	<spring:message code="msgCurrentMemberInformation" /> </div>
			<div class='row divErrorMsg' id='divErrorMsg_top_currMemIn' style="display: none; padding-left: 40px;"></div>		

			<div class="row portfolio_Subheading" id='divErrorMsg_top_TFA_header' style="display: none; padding-left: 30px;"> <spring:message code="lblTFA" /> </div>
			<div class='row divErrorMsg' id='divErrorMsg_top_TFA' style="display: none; padding-left: 40px;"></div>		
			<div class="row portfolio_Subheading" id='divErrorMsg_top_SIN_header' style="display: none; padding-left: 30px;"></div>
			<div class='row divErrorMsg' id='divErrorMsg_top_SIN' style="display: none; padding-left: 40px;"></div>		
		</div>
 	</div>
 	<div class="modal-footer">
 		<span id=""><button class="btn btn-primary" data-dismiss="modal" onclick="showDSPQDiv();"><spring:message code="btnOk" /> <i class="icon"></i></button></span>&nbsp;&nbsp; 		
 	</div>
</div>
	</div>
</div><!--
//************************************************

--><div class="modal hide" id="updateAndSaveDSPQ" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="hideAllProfaileDiv();">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div id="redirectLabel" class="modal-body"> 		
		<div class="control-group">
			<spring:message code="msgProfupdSucc" />
		</div>
 	</div>
 	<div class="modal-footer">
 		<span id=""><button class="btn btn-primary" data-dismiss="modal" onclick="hideAllProfaileDiv();"><spring:message code="btnOk" /> <i class="icon"></i></button></span>&nbsp;&nbsp; 		
 	</div>
</div>
	</div>
</div><!--

*************************************************

--><!-- Start ... call Dynamic Portfolio -->
<input type="hidden" id="txtDistrictPortfolioConfig" name="txtDistrictPortfolioConfig" value="0"/>
<!-- Academic And Academic Transcript -->
<div  class="modal hide"  id="myModalDymanicPortfolio" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog" style="width:1030px;">
	<div class="modal-content">
        <div class="modal-header">
	 		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabelDynamicPortfolio"><spring:message code="msgReqAppItm" />&nbsp;${districtDName}</h3>
	</div>
	<div class="modal-body" id="modalDymanicPortfolio_modal_body" style="max-height: 450px;overflow-y: auto;overflow-x: hidden;">	
	<input type="hidden" id="gridNameFlag" name="gridNameFlag"/>
		<div class="row">
			<div class="col-sm-12 col-md-12">
				<input type="hidden" id="txtCandidateType" name="txtCandidateType" value="${isAffilated}"/>
				<div id='dynamicPortfolioInformation'><spring:message code="msgDp_commons1" />
				<!-- </BR></BR>Following application information is required to be provided:</BR> -->
				</div>
			</div>	              	
		</div>
		
		<div class="row">
			<div class="col-sm-12 col-md-12">
				<div id='dynamicPortfolioInformation_Sub' style="display: none;"><BR/><spring:message code="msgDp_commons2" />
				</div>
			</div>	              	
		</div>
		
		<div class="row">
			<div class="col-sm-12 col-md-12">
				<div id='dynamicPortfolioInformation_Inner' style="display: block;"></div>
			</div>	              	
		</div>
		
		 <!-- 
		<div class="row">
			<div class="col-sm-12 col-md-12">
				<div class='divErrorMsg' id='divErrorMsg_dynamicPortfolio' style="display: none;"></div>
			</div>	              	
		</div>
		 -->
		<div class="row">
			<div class="col-sm-12 col-md-12">
				<div class='divErrorMsg' id='divErrorMsg_customZip' style="display: none;"></div>
			</div>	              	
		</div>
		 
		 
<!-- Start Personal Information -->

<!-- End Personal Information -->
	
	<div id="peronalInformationDiv">	
		<div class="row">
			<div class="col-sm-12 col-md-12">
				<div class="span4 portfolio_Subheading"><spring:message code="headPrsnlInfo" /></div>
			</div>
		</div>
		
			<div class="row">
				<div class="col-sm-12 col-md-12">
				<div class='divErrorMsg'  id="errPersonalInfoAndSSN"></div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12 col-md-12">
				<div class='divErrorMsg'  id="errPersonalInfoAndSIN"></div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-sm-2 col-md-2">
					<label><strong>Salutation <a href="#" id="iconpophoverSolutation" rel="tooltip" data-original-title='<spring:message code="msgSalutationstrictlyoptional" />'><img src="images/qua-icon.png" width="15" height="15" alt=""></a></strong></label>					
					<select id="salutation_pi" class="form-control" >
						<option value="0"></option>   
						<option  value="4"> <spring:message code="optDr" /> </option>
						<option  value="3"><spring:message code="optMiss" /></option>
						<option  value="2"><spring:message code="optMr" /></option>
						<option  value="1"> <spring:message code="optMrs" /> </option>
						<option  value="5"> <spring:message code="optMs" /></option>											
					</select>
				</div>
				<input type="hidden" id="teacherId_pi" />
															
				<div class="col-sm-2 col-md-2">
					<label><strong><spring:message code="lblFname" /><span class="required">*</span></strong></label>
					  <c:choose>              
			  			<c:when test="${isKelly eq 'true'}">					
					 	  <input type="text" id="firstName_pi" class="form-control" maxlength="40"/>
						</c:when>
						 <c:otherwise>
				      		 <input type="text" id="firstName_pi" class="form-control" maxlength="50"/>
						 </c:otherwise>
					</c:choose>
			
				</div>
				
				<div class="col-sm-2 col-md-2">
					<label><strong><spring:message code="lblMiddleName" /></strong></label>
					<input type="text" id="middleName_pi" class="form-control" maxlength="50"/>
				</div>
							
				<div class="col-sm-2 col-md-2">
					<label><strong><spring:message code="lblLname" /><span class="required">*</span></strong></label>
					  <c:choose>              
			  			<c:when test="${isKelly eq 'true'}">					
					 	<input type="text" id="lastName_pi" class="form-control" maxlength="40"/>
						</c:when>
						 <c:otherwise>
				      		<input type="text" id="lastName_pi" class="form-control" maxlength="40"/>
						 </c:otherwise>
					</c:choose>
				</div>
			
				<div class="col-sm-4 col-md-4 hide" id="anotherNameDiv">
					<div>
						<label><strong><spring:message code="lblAname" /> <a href="#" id="iconpophoverAnotherName" rel="tooltip" data-original-title='<spring:message code="tooltipmsgDp_commons3"/>'><img src="images/qua-icon.png" width="15" height="15" alt=""></a></strong></label>
						<input type="text" id="anotherName" class="form-control" />
					</div>
				</div>          
		   </div>
	</div>
	<div class="row" class="col-sm-12 col-md-12" id="ssnDiv">
    <div class="col-sm-2 col-md-2" >
					<div class="divErrorMsg hide"  id="errSSN"></div>
				
						<c:if test="${districtMaster.districtId ne 804800 && districtMaster.districtId ne 4503810}">
					    <label  style="width: 200px;"><strong>
						<div id="4ssnlast" class="show">
						<spring:message code="lblSSNumber" />					
						<span class="required">*</span><a href="#" id="iconpophoverSSN" rel="tooltip" data-original-title="Please enter numbers only, no dashes"><img src="images/qua-icon.png" width="15" height="15" alt=""></a>
						</div>
						</strong>
						</label>
			        	<input type="text" id="ssn_pi" class="form-control" <c:if test="${districtMaster.districtId eq 4503810}">onblur='showEditSSnYORK();'</c:if>  maxlength="9" onkeypress="return checkForInt(event);" onclick="chkDistrict();" />
						</c:if>
					
		               <c:if test="${districtMaster.districtId eq 4503810}">
						<label  style="width: 200px;"><strong>
						<div id="4ssnlast" class="show">
						<spring:message code="lblSSNumber" />					
						<span class="required">*</span><a href="#" id="iconpophoverSSN" rel="tooltip" data-original-title="Please enter numbers only, no dashes"><img src="images/qua-icon.png" width="15" height="15" alt=""></a>
						</div>
						</strong>
						</label>
			        	<input type="text" id="ssn_pi" class="form-control" <c:if test="${districtMaster.districtId eq 4503810}">onblur='showEditSSnYORK();'</c:if>  maxlength="9" onkeypress="return checkForInt(event);" onclick="chkDistrict();" />
						</c:if>
						
						
						<c:if test="${districtMaster.districtId eq 804800}">
							<spring:message code="lbllastfourssn" />
							<span class="required">*</span><a href="#" id="iconpophoverSSN" rel="tooltip" data-original-title="Please enter numbers only, no dashes"><img src="images/qua-icon.png" width="15" height="15" alt=""></a></strong></label>
			        	<input type="text" id="ssn_pi" class="form-control" maxlength="4" onkeypress="return checkForInt(event);" onclick="chkDistrict();" />
						</c:if>
						
						
						
						<div id="last4snnfornc" class="hide">
                             <spring:message code="lbllastfourssn" />
                           <span class="required">*</span> <a href="#" id="iconpophoverSSNForNC" rel="tooltip" data-original-title="Last Four Digits of Social Security Number."><img src="images/qua-icon.png" width="15" height="15" alt=""></a>
                             <br>
                             <span id="ssn_pifornc"></span>                                                                  
                             <a href="#" id="hrefClick" onclick="showFullSSn();" title="Edit SSN">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="images/edit_circle.png" width="15" height="15" ></a>
                        </div>
                        
                        <div id="last4snnforyork" class="hide">
                             <spring:message code="lbllastfourssn" />
                             <a href="#" id="iconpophoverSSNForYORK" rel="tooltip" data-original-title="Last Four Digits of Social Security Number."><img src="images/qua-icon.png" width="15" height="15" alt=""></a>
                             <br>
                             <span id="ssn_piforYor"></span>                                                                  
                             <a href="#"  onclick="showFullSSnYork();" title="Edit SSN">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="images/edit_circle.png" width="15" height="15" ></a>
                        </div>
		  </div>

		

			<div id="showNCDPIOnlySsn" class="col-sm-10 col-md-10 top27 hide" ><!--

			
			      <a href="javascript:void(0);" id="licenseButtonForAll" onclick="showForm_Licence();getEducation();" class="idone">Submit SSN</a>
				  -->
				  <button  class="btn btn-large btn-primary" style="background: #0078B4;" type="button" id="submitSSN"  ><strong>Submit&nbsp;SSN <i class="icon"></i></strong></button>
				  <div  class="col-sm-10 col-md-10 " style="margin-left:145px; margin-top:-35px;">
				  <spring:message code="NCDPIssnMsg" />			         
		          </div>
		    </div>
	</div>
	

 <!-- khan Start -->
 <div class="row" class="col-sm-12 col-md-12" id="sinDiv">
    <div class="col-sm-2 col-md-2">
    
    	<div class="divErrorMsg hide"  id="errSIN"></div>	
    	
    		<label  style="width: 200px;"><strong>
						<div id="4sinlast" class="show">
						Social Insurance Number				
						<span class="required" id="4sinlastReq" >*</span>
						<img data-toggle="tooltip" title="Please enter numbers only, no dashes" src="images/qua-icon.png" width="15" height="15" alt="">
						</div>
						</strong>
						</label>
			        	<input type="text" id="sin_pi" class="form-control"/>
	   </div>
  </div>
<!-- khan Start -->  
	   
	<div>
		<div class="row">
			<!--<div class="span5" id="emailPeronalInformationDiv">
				<label><strong>Email<span class="required">*</span></strong></label>
				<input type="text" id="email_pi" class="span5" maxlength="75" readonly="readonly"/>
			</div>
			-->
			
			
			<input type="hidden" id="dob" name="dob" class="span2" maxlength="4"/>
		
		<div  id="dobDiv">
			<div class="col-sm-12 col-md-12">
				<label><strong class="portfolio_Subheading"><spring:message code="lblDOB1"/></strong></label>
			</div>
			<div class="col-sm-12 col-md-12">
				<div class='divErrorMsg' style="display: none;" id="errDOB"></div>
			</div>	
					<div class="col-sm-2 col-md-2">
					<label><strong><spring:message code="lblMonth" /><c:if test="${!(districtMaster.districtId eq 7800033 and ((jobOrder.jobCategoryMaster.jobCategoryName eq 'Student Support Services') or (jobOrder.jobCategoryMaster.jobCategoryName eq 'Instructional and SLT')))}">
					<span class="required optionalCss dobRemAst">*</span></c:if></strong></label>
					<select class="form-control" id="dobMonth" name="dobMonth">
						<option value="0"><spring:message code="optStrMonth"/></option>
						<c:forEach items="${lstMonth}" var="dobMonth" varStatus="dobStatus">
							<option value="${dobStatus.count}">${dobMonth}</option>
						</c:forEach>
					</select>
					</div>
					
					<div class="col-sm-2 col-md-2">
					<label><strong><spring:message code="lblDay1"/><c:if test="${!(districtMaster.districtId eq 7800033 and ((jobOrder.jobCategoryMaster.jobCategoryName eq 'Student Support Services') or (jobOrder.jobCategoryMaster.jobCategoryName eq 'Instructional and SLT')))}">
					<span class="required optionalCss dobRemAst">*</span></c:if></strong></label>
					<!-- <input type="text" id="dobDay" name="dobDay" class="span1" maxlength="2" onkeypress="return checkForInt(event);" /> -->
					
					<select class="form-control" id="dobDay" name="dobDay">
						<option value="0"><spring:message code="lblSelectDay"/></option>
						<c:forEach items="${lstDays}" var="dobDay" varStatus="dobStatusDays">
							<option value="${dobStatusDays.count}">${dobDay}</option>
						</c:forEach>
					</select>					
					</div>
					
					
					<div class="col-sm-2 col-md-2">
					<label><strong><spring:message code="optYear"/><c:if test="${!(districtMaster.districtId eq 7800033 and ((jobOrder.jobCategoryMaster.jobCategoryName eq 'Student Support Services') or (jobOrder.jobCategoryMaster.jobCategoryName eq 'Instructional and SLT')))}">
					<span class="required optionalCss dobRemAst">*</span></c:if></strong></label>
					<input type="text" id="dobYear" name="dobYear" class="form-control" maxlength="4" onkeypress="return checkForInt(event);" onblur="return validateDOBYearForDSPQ();" />
					</div>
			
				
				</div>
		
		</div>
	</div>
	
	<!-- present address -->
	<div class="hide" id="addressDivPresent">
			<div class="row">
				<div class="col-sm-6 col-md-6">
					<div class="portfolio_Subheading"><spring:message code="lblPresentAddr" /></div>
				</div>
			</div>
						   
			 <div class="row">
			    <div class="col-sm-6 col-md-6">
			   	<div class="divErrorMsg hide" id="errAddressPr"></div>	
			    </div>
			 </div>  
			 			   
			<div class="row">	    	
	    		<div class="col-sm-6 col-md-6">
					<label><strong><spring:message code="lblAddrL1" /><span class="required">*</span></strong></label>
					<input type="text" name="addressLinePr" id="addressLinePr" class="form-control" maxlength="50"  value="${teacherpersonalinfo.presentAddressLine1}" />
				</div>
	   		</div>    
	   
		   	<div class="row">
			  	<div class="col-sm-6 col-md-6">
					<label><strong><spring:message code="lblAddrL2" /></strong></label>
					<input type="text" name="addressLine2Pr" id="addressLine2Pr" class="form-control" maxlength="50" value="${teacherpersonalinfo.presentAddressLine2}" />
				</div>
		   </div> 
   			   
			 <div class="row">
			    <div class="col-sm-6 col-md-6">
			   	<div class="divErrorMsg hide" id="errCountryPr"></div>
			    </div>
			 </div> 
			 
			<div class="row">   		
					<div class="col-sm-3 col-md-3">
						<label><strong><spring:message code="lblCnty" /><span class="required">*</span></strong></label>
						<select id="countryIdPr" class="form-control" onchange="getStateByCountryForDspqPr('dspq')">   
							<option value=""><spring:message code="lblSelectCountry"/></option>
							<c:forEach items="${listCountryMaster}" var="ctry">  
								<c:set var="selected" value=""></c:set>
								<c:if test="${ctry.countryId eq teacherpersonalinfo.presentCountryId.countryId}">	        			
				         			<c:set var="selected" value="selected"></c:set>	         			
				        		</c:if>
				        		<c:if test="${ctry.countryId eq 223}">	        			
				         			<c:set var="selected" value="selected"></c:set>	         			
				        		</c:if>
								<option id="ctryPr${ctry.countryId}" value="${ctry.countryId}" ${selected} >${ctry.name}</option>
							</c:forEach>										
						</select>
			           		
					</div>
			</div>
		
			<div class="row">	
				<div class="hide" id="multyErrDivPr">
					<div class="divErrorMsg hide col-sm-3 col-md-3" id="errZipPr"></div>
					<div class="divErrorMsg hide col-sm-3 col-md-3" id="errStatePr"></div>
					<div class="divErrorMsg hide col-sm-3 col-md-3" id="errCityPr"></div>
				</div>
			</div>
				
   		    <div class="row">
	  			<div class="col-sm-3 col-md-3">
					<label><strong><spring:message code="lblZepCode" /><span class="required">*</span></strong></label>
					<input type="text" Class="form-control" id="zipCodePr" onblur="changeCityStateByZipForDSPQPr();" maxlength="10"  value="${teacherpersonalinfo.presentZipCode}" />
				</div>
				<div class="col-sm-3 col-md-3" id="divUSAStatePr" style="display: none;">
					<label><strong><spring:message code="lblSt" /><span class="required">*</span></strong></label>
					<select id="stateIdForDSPQPr" Class="form-control" onchange="getCityListByStateForDSPQPr()">
					
					<!-- add by khan -->
					
					<option value="" id="slstPr">Select State</option>
						<c:forEach items="${listStateMaster}" var="st">
							<c:set var="selected" value=""></c:set>					
			        		<c:if test="${st.stateId eq teacherpersonalinfo.presentStateId.stateId}">	        			
			         			<c:set var="selected" value="selected"></c:set>	         			
			        		</c:if>									
							<option id="stPr${st.stateId}" value="${st.stateId}" ${selected} >${st.stateName}</option>
						</c:forEach> 			
					
					</select>
			
				</div>
				<div class="col-sm-3 col-md-3" id="divUSACityPr" style="display: none;">
					<label><strong>City<span class="required">*</span></strong></label>
					<select id="cityIdForDSPQPr"  name="cityIdForDSPQPr" Class="form-control">   
						<option  id="slctyPr" value="">Select City</option>
						<c:forEach items="${listCityMasters}" var="city">	
							<c:set var="selected" value=""></c:set>					
			        		<c:if test="${city.cityId eq teacherpersonalinfo.presentCityId.cityId}">	        			
			         			<c:set var="selected" value="selected"></c:set>	         			
			        		</c:if>							
							<option id="ctPr${city.cityId}" value="${city.cityId}" ${selected} >${city.cityName}</option>
						</c:forEach>	
					</select>
				
				
				</div>			
				<div class="col-sm-3 col-md-3" id="divotherstatePr" style="display: none;">
					<label><strong><spring:message code="lblSt/Cnty" /><span class="required">*</span></strong></label>
					<input type="text" id="otherStatePr" name="otherStatePr" class="form-control" maxlength="50"/>
				</div>
				<div class="col-sm-3 col-md-3" id="divothercityPr" style="display: none;">
					<label><strong><spring:message code="lblCity" /><span class="required">*</span></strong></label>
					<input type="text" id="otherCityPr" name="otherCityPr" class="form-control" maxlength="50"/>
				</div>
		  </div>
		  
		  <!-- --Start  checkkbox same as present address-->
<!-- click if same as present address -->
<div class="row">			
	<div class="col-sm-3 col-md-3">
      		<label class="checkbox inline" style="margin-top: 2px;margin-bottom: 2px;">
					<input type="checkbox" id="sameAddress" onclick="sameAsAddress();"/>
           			<spring:message code="lblSameAsPresentAddr" />
          	</label>
	</div>	         			
</div>
<!-- -- End checkkbox same as present address-->
		  
	   </div>
	   
	   
	<!-- end present address -->
	

<!-- Start Address -->
	<div id="addressDiv">
			<div class="row">
				<div class="col-sm-6 col-md-6">
					<div class="portfolio_Subheading"><spring:message code="lblAdd"/></div>
				</div>
			</div>
						   
			 <div class="row">
			    <div class="col-sm-6 col-md-6">
			   	<div class="divErrorMsg hide" id="errAddress1"></div>	
			    </div>
			 </div>  
			 			   
			<div class="row">	    	
	    		<div class="col-sm-6 col-md-6">
					<label><strong><spring:message code="lblAddrL1"/><span class="required">*</span></strong></label>
					<input type="text" name="addressLine1" id="addressLine1" class="form-control" maxlength="50"  value="${teacherpersonalinfo.addressLine1}" />
				</div>
	   		</div>    
	   
		   	<div class="row">
			  	<div class="col-sm-6 col-md-6">
					<label><strong><spring:message code="lblAddrL2"/></strong></label>
					<input type="text" name="addressLine2" id="addressLine2" class="form-control" maxlength="50" value="${teacherpersonalinfo.addressLine2}" />
				</div>
		   </div> 
   			   
			 <div class="row">
			    <div class="col-sm-6 col-md-6">
			   	<div class="divErrorMsg hide" id="errCountry"></div>
			    </div>
			 </div> 
			 
			<div class="row">   		
					<div class="col-sm-3 col-md-3">
						<label><strong><spring:message code="lblCnty"/><span class="required">*</span></strong></label>
						<select id="countryId" class="form-control" onchange="getStateByCountryForDspq('dspq')">   
							<option value=""><spring:message code="lblSelectCountry"/></option>
							<c:forEach items="${listCountryMaster}" var="ctry">
								<c:set var="selected" value=""></c:set>	
								<c:if test="${ctry.countryId eq teacherpersonalinfo.countryId.countryId}">	        			
				         			<c:set var="selected" value="selected"></c:set>	         			
				        		</c:if>				
								<option id="ctry${ctry.countryId}" value="${ctry.countryId}" ${selected} >${ctry.name}</option>
							</c:forEach>				
						</select>
			           		
					</div>
			</div>
		
			<div class="row">	
				<div class="hide" id="multyErrDiv">
					<div class="divErrorMsg hide col-sm-3 col-md-3" id="errZip"></div>
					<div class="divErrorMsg hide col-sm-3 col-md-3" id="errState"></div>
					<div class="divErrorMsg hide col-sm-3 col-md-3" id="errCity"></div>
				</div>
			</div>
				
   		    <div class="row">
	  			<div class="col-sm-3 col-md-3">
					<label><strong>	<spring:message code="lblZepCode" /><span class="required">*</span></strong></label>
					<input type="text" Class="form-control" id="zipCode" onkeypress="return checkForInt(event);" onblur="changeCityStateByZipForDSPQ()" maxlength="5"  value="${teacherpersonalinfo.zipCode}" />
				</div>
				<div class="col-sm-3 col-md-3" id="divUSAState" style="display: none;">
					<label><strong><spring:message code="lblSt" /><span class="required">*</span></strong></label>
					<select id="stateIdForDSPQ" Class="form-control" onchange="getCityListByStateForDSPQ()">
					
					<!-- add by khan -->
					
					<option value="" id="slst">	<spring:message code="optSltSt" /></option>
						<c:forEach items="${listStateMaster}" var="st">
							<c:set var="selected" value=""></c:set>					
			        		<c:if test="${st.stateId eq teacherpersonalinfo.stateId.stateId}">	        			
			         			<c:set var="selected" value="selected"></c:set>	         			
			        		</c:if>									
							<option id="st${st.stateId}" value="${st.stateId}" ${selected} >${st.stateName}</option>
						</c:forEach> 			
					
					</select>
			
				</div>
				<div class="col-sm-3 col-md-3" id="divUSACity" style="display: none;">
					<label><strong>City<span class="required">*</span></strong></label>
					<select id="cityIdForDSPQ"  name="cityIdForDSPQ" Class="form-control">   
						<option  id="slcty" value="">	<spring:message code="lblSelectCity" /></option>
						<c:forEach items="${listCityMasters}" var="city">	
							<c:set var="selected" value=""></c:set>					
			        		<c:if test="${city.cityId eq teacherpersonalinfo.cityId.cityId}">	        			
			         			<c:set var="selected" value="selected"></c:set>	         			
			        		</c:if>							
							<option id="ct${city.cityId}" value="${city.cityId}" ${selected} >${city.cityName}</option>
						</c:forEach>	
					</select>
				
				
				</div>			
				<div class="col-sm-3 col-md-3" id="divotherstate" style="display: none;">
					<label><strong><spring:message code="lblSt/Cnty" /><span class="required">*</span></strong></label>
					<input type="text" id="otherState" name="otherState" class="form-control" maxlength="50"/>
				</div>
				<div class="col-sm-3 col-md-3" id="divothercity" style="display: none;">
					<label><strong>City<span class="required">*</span></strong></label>
					<input type="text" id="otherCity" name="otherCity" class="form-control" maxlength="50"/>
				</div>
		  </div>
	   </div>
<!-- End Address -->
<!-- Phone  -->
<div id="phoneDiv">	
			<div class="row">
				<div class="col-sm-3 col-md-3" style="width:950px;">
					<div class="span4 portfolio_Subheading"><spring:message code="lblPhoneNo" /></div>
				</div>
		    </div>	
	       <div class="row">	
			<div class="col-sm-6 col-md-6">
				<div class='divErrorMsg' id='errordiv_bottomPart_phone' style="display: none;"></div>
			</div>
		   </div> 
		   <div class="row">	
			   <div class="col-sm-3 col-md-3">				
					<label><strong><spring:message code="headPhone" /><span class="required">*</span> <a href="#" id="iconpophoverPhone" rel="tooltip" data-original-title="Phone number (area code first)"><img src="images/qua-icon.png" width="15" height="15" alt=""></a></strong></label>
					 <%-- 
					<input type="text" name="phoneNumber" id="phoneNumber" class="span3" maxlength="20"  value="${teacherpersonalinfo.phoneNumber}"/>
					--%>
					<div>
						<div style="float: left;"><input type="text" name="phoneNumber1" id="phoneNumber1" class="form-control" maxlength="3"  value="" onkeypress="return checkForInt(event);" style='width:56px; padding-left:10px;'/></div><div style="float: left;color: #cccccc;margin-left: 4px;margin-top: 6px">-</div>
						<div style="float: left;margin-left: 5px;"><input type="text" name="phoneNumber2" id="phoneNumber2" class="form-control" maxlength="3"  value="" onkeypress="return checkForInt(event);" style='width:56px; padding-left:10px;'/></div><div style="float: left;color: #cccccc;margin-left: 4px;margin-top: 6px">-</div>
						<div style="float: left;margin-left: 5px;"><input type="text" name="phoneNumber3" id="phoneNumber3" class="form-control" maxlength="4"  value="" style='width:66px; padding-left:10px;'' onkeypress="return checkForInt(event);"/></div>
					</div>
										
					<!--<div>
						<div style="float: left;"><input type="text" name="phoneNumber1" id="phoneNumber1" class="form-control" maxlength="3"  value="${ph1}" onkeypress="return checkForInt(event);" style='width:56px; padding-left:10px;'/></div><div style="float: left;color: #cccccc;margin-left: 4px;margin-top: 6px">-</div>
						<div style="float: left;margin-left: 5px;"><input type="text" name="phoneNumber2" id="phoneNumber2" class="form-control" maxlength="3"  value="${ph2}" onkeypress="return checkForInt(event);" style='width:56px; padding-left:10px;'/></div><div style="float: left;color: #cccccc;margin-left: 4px;margin-top: 6px">-</div>
						<div style="float: left;margin-left: 5px;"><input type="text" name="phoneNumber3" id="phoneNumber3" class="form-control" maxlength="4"  value="${ph3}" style='width:66px; padding-left:10px;' onkeypress="return checkForInt(event);"/></div>
					</div>
				
			   --></div>		
	     <input type="hidden" id="distrciIdMob" value="${districtMaster.districtId}">
	      <c:if test="${districtMaster.districtId eq 1201290}">
			   <div class="col-sm-3 col-md-3">				
					<label><strong><spring:message code="lblMobile" /><span class="required">*</span> <a href="#" id="iconpophover2" rel="tooltip" data-original-title="Mobile number (area code first)"><img src="images/qua-icon.png" width="15" height="15" alt=""></a></strong></label>
					
					 <div>
											<div style="float: left;">	<input type="text" name="mobileNumber1" id="mobileNumber1" class="form-control" maxlength="3"  value="${mb1}" onkeypress="return checkForInt(event);" style='width:56px; padding-left:10px;'/></div><div style="float: left;color: #cccccc;margin-left: 4px;margin-top: 6px">-</div>
											<div style="float: left;margin-left: 5px;"><input type="text" name="mobileNumber2" id="mobileNumber2" class="form-control" maxlength="3"  value="${mb2}" onkeypress="return checkForInt(event);" style='width:56px; padding-left:10px;'/></div><div style="float: left;color: #cccccc;margin-left: 4px;margin-top: 6px">-</div>
											<div style="float: left;margin-left: 5px;"><input type="text" name="mobileNumber3" id="mobileNumber3" class="form-control" maxlength="4"  value="${mb3}" style='width:66px; padding-left:10px;' onkeypress="return checkForInt(event);"/></div>
											</div>
				
			  </div>
			  	</c:if>
			   </div>	
	     
</div>	
<!-- End Phone -->
  <div id="veteranDiv" >
      <div class="row">
		<div class="col-sm-12 col-md-12 top5">
			<label class="span15">
				<strong><spring:message code="lblDp_commons4" /><span class="required">*</span></strong>
			</label>
		</div>
	   </div>
	   <div class="row left1 ">		
			 <div class="col-sm-1 col-md-1 radio top1">
		    	<input type="radio" id="vt1" value="1" name="veteran" onclick="showdistrictspeciFicVeteran();">
		    	<spring:message code="lblYes" />
	        </div>
	        </br>				
			<div class="col-sm-1 col-md-1 radio" style="margin-top:-18px;">
				<input type="radio" id="vt2" value="0" name="veteran" onclick="showdistrictspeciFicVeteran();"> <spring:message code="lblNo" />					
	        </div>	       
        </div>
        <div class="col-sm-12 col-md-12 top5 hide" id="vetranOptionDiv" class="hide">
        	
			<table>
				<tbody>
					<tr>
						<td style="border:0px;background-color: transparent;"><div style="display: block;min-height: 40px;padding-left: 5px;vertical-align: middle;"><input type="radio" id="veteranOptS" name="veteranOptS" value="1"></div></td>
						<td style="border:0px;background-color: transparent;padding-left:5px;"><font size="2" color="red"><spring:message code="lblDp_commonsA" /> </font> <spring:message code="msgDp_commons5" /> </td>
					</tr>
					<tr><td style="border:0px;background-color: transparent;"><div style="display: block;min-height: 40px;padding-left: 5px;vertical-align: middle;"><input type="radio" id="veteranOptS" name="veteranOptS" value="2"></div></td>
						<td style="border:0px;background-color: transparent;padding-left:5px;" ><font size="2" color="red">  <spring:message code="lblDp_commonsB" /> </font> <spring:message code="msgDp_commons6" /> </td>
					</tr>
					<tr><td style="border:0px;background-color: transparent;"><div style="display: block;min-height: 40px;padding-left: 5px;vertical-align: middle;"><input type="radio" id="veteranOptS" name="veteranOptS" value="3"></div></td>
						<td style="border:0px;background-color: transparent;padding-left:5px;" ><font size="2" color="red"> <spring:message code="lblDp_commonsC" /> </font> <spring:message code="msgDp_commons7" /> </td>
					</tr>
					<tr><td style="border:0px;background-color: transparent;"><div style="display: block;min-height: 20px;padding-left: 5px;vertical-align: middle;"><input type="radio" id="veteranOptS" name="veteranOptS" value="4"></div></td>
						<td style="border:0px;background-color: transparent; padding-left:5px;" ><font size="2" color="red"><spring:message code="lblDp_commonsD" /> </font> <spring:message code="msgDp_commons50" /> </td>
					</tr>
					<tr><td style="border:0px;background-color: transparent;"><div style="display: block;min-height: 40px;padding-left: 5px;vertical-align: middle;"><input type="radio" id="veteranOptS" name="veteranOptS" value="5"></div></td>
						<td style="border:0px;background-color: transparent;padding-left:5px;" ><font size="2" color="red"><spring:message code="lblDp_commonsE" /> </font> <spring:message code="msgDp_commons8" /> </td>
					</tr>
					<tr><td style="border:0px;background-color: transparent;"><div style="display: block;min-height: 20px;padding-left: 5px;vertical-align: middle;"><input type="radio" id="veteranOptS" name="veteranOptS" value="6"></div></td>
						<td style="border:0px;background-color: transparent;padding-left:5px;" ><font size="2" color="red"><spring:message code="lblDp_commonsF" /> </font> <spring:message code="msgDp_commons9" /> </td>
					</tr>
					<tr><td style="border:0px;background-color: transparent;"><div style="display: block;min-height: 20px;padding-left: 5px;vertical-align: middle;"><input type="radio" id="veteranOptS" name="veteranOptS" value="7"></div></td>
						<td style="border:0px;background-color: transparent;padding-left:5px;" ><font size="2" color="red"><spring:message code="lblDp_commonsG" /> </font><spring:message code="msgDp_commons10" /> </td>
					</tr>
				</tbody>
			</table>
			
			<div class="top10"><spring:message code="msgDp_commons11" /></div>
            <table>
	            <tbody>
	               <tr>
	               <td style="border:0px;background-color: transparent;"><div style="display: block;min-height: 20px;padding-left: 5px;vertical-align: middle;"><input type="radio" id="veteranPreference1" name="veteranPreference1" value="1"></div> </td>
	               <td style="border:0px;background-color: transparent;padding-left:5px;" ><spring:message code="lblYes" /> </td></tr>
	               <tr><td style="border:0px;background-color: transparent;"><div style="display: block;min-height: 20px;padding-left: 5px;vertical-align: middle;"><input type="radio" id="veteranPreference1" name="veteranPreference1" value="2"></div> 
	               </td><td style="border:0px;background-color: transparent;padding-left:5px;">No </td>
	               </tr>
	               </tbody>
	        </table>
	        <div class="top10"><spring:message code="msgDp_commons12" /></div>
			<table>
			    <tbody>
			    <tr>
			        <td style="border:0px;background-color: transparent;"><div style="display: block;min-height: 20px;padding-left: 5px;vertical-align: middle;"><input type="radio" id="veteranPreference2" name="veteranPreference2" value="1"></div> </td>
			        <td style="border:0px;background-color: transparent;padding-left:5px;" ><spring:message code="lblYes" /> </td>
			    </tr>
			    <tr><td style="border:0px;background-color: transparent;"><div style="display: block;min-height: 20px;padding-left: 5px;vertical-align: middle;"><input type="radio" id="veteranPreference2" name="veteranPreference2" value="2"></div> </td>
			        <td style="border:0px;background-color: transparent;padding-left:5px;"><spring:message code="lblNo" /> </td>
			    </tr>
			    </tbody>
			</table>
        </div>
        
   </div>
		<div  id="retirenoDiv">
	        <div class="row">
				<div class="col-sm-6 col-md-6">
					<div class="span15 portfolio_Subheading"><spring:message code="lblTeacherretir" /></div>		
				</div>	
		 	</div>	
		 	<div class="row">	
			<div class="col-sm-9 col-md-9">
				<div class='divErrorMsg' style="display: none;" id="errRetireNo"></div>
			</div>
			</div>
			
			<div class="row">			
				<div class="col-sm-3 col-md-3">
	        		<label class="checkbox inline" style="margin-top: 2px;margin-bottom: 2px;">
								<input type="checkbox" id="isretired" onclick="chkRetired();"/>
		            			<spring:message code="lblRetireForDist" />
		           	</label>
				</div>	         			
			</div>
			<div class="hide" id="isRetiredDiv">
				<div class="row">			
					<div class="col-sm-6 col-md-6">
							<div id="SearchTextboxDiv">	         		   
			             			<label id="captionDistrictOrSchool"><spring:message code="lblRetFrmDist" /><span class="required">*</span></label>
				             		<input type="hidden" id="retireddistrictId" name="retireddistrictId" value=""/>
				             		<input type="text" id="retireddistrictName" name="retireddistrictName"  class="form-control"
					             		onfocus="getDistrictMasterAutoComp(this, event, 'divTxtShowData1', 'retireddistrictName','retireddistrictId','');"
										onkeyup="getDistrictMasterAutoComp(this, event, 'divTxtShowData1', 'retireddistrictName','retireddistrictId','');"
										onblur="hideDistrictMasterDiv(this,'retireddistrictId','divTxtShowData1');"	/>
									<div id='divTxtShowData1' style='display:none;position:absolute;z-index:5000;' onmouseover="mouseOverChk('divTxtShowData1','retireddistrictName')" class='result' ></div>	
				           </div>
		        	</div>	         			
				</div>
				<div class="row">			
					<div class="col-sm-3 col-md-3">
							<label><strong><spring:message code="lblRetFrmStat" /><span class="required">*</span></strong></label>
							<select  class="form-control" id="stMForretire" onchange="getPraxis(),clearCertType();">   
								<option value=""><spring:message code="optSltSt" /></option>
								<c:forEach items="${listStateMaster}" var="st">
					        		<option id="st${st.stateId}" value="${st.stateId}" ${selected} >${st.stateName}</option>
								</c:forEach>				
							</select>	
		        	</div>	         			
				
						
					<div class="col-sm-3 col-md-3">
		        		<label><strong><spring:message code="lblTeacherRetirementNumber" /><span class="required">*</span></strong></label>	
							<input type="text" id="retireNo" class="form-control" maxlength="10"/>
					</div>	         			
				</div>
			</div>
		</div>
	<div id="formerEmployeeDiv">
	        <div class="row">
				<div class="col-sm-6 col-md-6">
					<div class="span4 portfolio_Subheading"><spring:message code="lblCurrentEmploymnt" /></div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12 col-md-12">
			    <div class='divErrorMsg' style="display: none;" id="errFormerEmployee"></div>
			    </div>
			</div>
			<div class="row">
				<div class="col-sm-9 col-md-9">
					<label class="span6 radio" style="margin-top: 2px;margin-bottom: 2px;">
			    		<input type="radio" id="fe1" name="fe" onclick="showEmpNoDiv(1);"/><spring:message code="msgDp_commons13" /> <span id='displayDistrictNameForExt1'>${districtDName}</span>
			    	</label>
			    </div>	
			</div>
		    	<div id="fe1Div" class="hide">
			    <div id="feDiv1">
			    	<div class="row left25">
				        <div class="col-sm-3 col-md-3">
							<label id="lblempnumber"><strong><spring:message code="lblEmpNum" /></strong></label>
							<input type="text" id="empfe1" class="form-control" maxlength="50" onkeypress="return checkForIntEmpNum(event);"/>
						</div>						
						<div id="positionDiv" class="col-sm-3 col-md-3" style="display: none;">
							<label><strong><spring:message code="lblLocti" /></strong></label>
							<input type="text" id="empfe5" class="form-control" maxlength="50"/>
						</div>
						
						<div id="locationDiv" class="col-sm-3 col-md-3" style="display: none;">
							<label><strong><spring:message code="lblPosition" /></strong></label>
							<input type="text" id="empfe6" class="form-control" maxlength="50"/>
						</div>
					</div>
					  <div class="row left25 ntPhiLfeild">
					      <div class="col-sm-12 col-md-12">
							<label class="checkbox inline" style="margin-top: 2px;margin-bottom: 2px;">
								<input type="checkbox" id="empchk11" onclick="showEmpDate(0);"/>
		            			<spring:message code="msgDp_commons14" />
		           				</label> 
			           		</div>	
			           		 <div class="col-sm-3 col-md-3 left25">
			           			<div class='mt5 hide' id='rtDateDiv'>
			           				<label id="lblRetirementDate1"><strong><spring:message code="lblRetirementDate" /></strong></label>
									<input type="text" id="rtDate" name="rtDate" class="form-control" maxlength="4"/>
			           			</div>
							</div>
						</div>
						<div class="row left25 ntPhiLfeild">
						    <div class="col-sm-12 col-md-12">
							<label class="checkbox inline" style="margin-top: 2px;margin-bottom: 2px;">
								<input type="checkbox" id="empchk12"  onclick="showEmpDate(1);"/>
		            			<spring:message code="msgDp_commons15" />
		            		</label>
		            		</div>	
							<div class="col-sm-3 col-md-3 left25">
		           			<div class='mt5 hide' id='wtDateDiv'>
		           				<label><strong><spring:message code="lblDateofwithdrawn" /></strong></label>
								<input type="text" id="wdDate" name="wdDate" class="form-control" maxlength="4"/>
		           			</div>
							</div>
						</div>
						
						<div class="row left25 ntPhiLfeild hide" id="empchk13Div">
						    <div class="col-sm-12 col-md-12">
							<label class="checkbox inline" style="margin-top: 2px;margin-bottom: 2px;">
								<input type="checkbox" id="empchk13"   onclick="showEmpDate(2);"/>
		            			<spring:message code="lblIColoradoPublic" />
		            		</label>
		            		</div>	
		            		<div class="col-sm-3 col-md-3 left25">
		           			<div class='mt5 hide' id='rwtDateDiv'>
		           				<label><strong><spring:message code="lblRetirementDate" /></strong></label>
								<input type="text" id="rwdDate" name="rwdDate" class="form-control" maxlength="4"/>
		           			</div>
							</div>
						</div>
						
						
						
						
						<div class="row left25 ntPhiLfeild hide" id="empfewDiv">
							<div class="col-sm-6 col-md-6 top10">
								<label><strong><spring:message code="msgDp_commons16" /></strong></label>
								<textarea name="empfew" id="empfew" class="form-control" maxlength="1000" rows="3"></textarea>
							</div>
						</div>
					</div>
           		</div>
					
			
		
		
		         <div class="row">
			
				     <div class="col-sm-9 col-md-9">
						<label class="span6 radio" style="margin-top: 2px;margin-bottom: 2px;">
							<input type="radio" id="fe2" name="fe" onclick="showEmpNoDiv(2);"/><spring:message code="msgDp_commons17" /> <span id='displayDistrictNameForExt2'>${districtDName}</span>
						</label>
					 </div>	
				</div>
				
				<div id="fe2Div" class="hide">				
					<div class="row left25">
						<div class="col-sm-3 col-md-3" id="empNoDiv">
							<label><strong><spring:message code="lblEmpNum" /><span class="required">*</span></strong></label>
							<input type="text" id="empfe2" class="form-control" maxlength="50" onkeypress="return checkForIntEmpNum(event);"/>
						</div>
						<div class="col-sm-3 col-md-3 hide" id="senNumDiv">
							<label><strong><spring:message code="msgSeniorityNumber" /><span class="required"></span></strong></label>
							<input type="text" id="seniorityNumb" class="form-control" maxlength="50"/>
						</div>

						<div class="col-sm-5 col-md-5 top20 hide" id="jeffcoSeachDiv">
							<button class="btn btn-large btn-primary" type="button" onclick="openEmployeeNumberInfo();"><strong>Employee Information<i class="icon"></i></strong></button>
						</div>
					</div>

				
				<!-- 
				<div class="span6">
					<label class="checkbox inline">
						<input type="checkbox" id="empchk2"/>
	            		I am current full time teacher
	           		</label>
           		</div>
           		 -->
           		<div class="row left25">
           		 <div class="col-sm-9 col-md-9 optionalCss">
							<label class="span6 radio">
								<input type="radio" id="rdCEmp1" name="rdCEmp" /><spring:message code="msgDp_commons18" />
							</label>
							<c:set var="showVal"  value="none"/>
							<c:if test="${isMiami}">
							<c:set var="showVal"  value="block"/>
							</c:if>
							<label class="span7 radio p0 left20" style="display: ${showVal};" id="partInsEmp">
								<input type="radio" id="rdCEmp3" name="rdCEmp" /><spring:message code="msgDp_commons19" />
							</label>
							<label class="span6 radio">
						    <input type="radio" id="rdCEmp2" name="rdCEmp" /><spring:message code="msgDp_commons20" />
					         </label>
				</div>
				</div>   		
           		
           		</div>
	
		
		
		<div class="row">
				<div class="col-sm-9 col-md-9">
					<label class="span15 radio" style="margin-top: 2px;margin-bottom: 2px;">
					<input type="radio" id="fe3" name="fe" onclick="showEmpNoDiv(3);"/><spring:message code="msgDp_commons21" /> <span id='displayDistrictNameForExt3'>${districtDName}</span>
				</label>
			</div>
		</div>
		
		<div class="row hide" id="fe5Div">
				<div class="col-sm-9 col-md-9">
					<label class="span15 radio" style="margin-top: 2px;margin-bottom: 2px;">
					<input type="radio" id="fe5" name="fe" onclick="showEmpNoDiv(5);"/>I am employed as a substitute teacher <span id='displayDistrictNameForExt5'>${districtDName}</span>
				</label>
			</div>
		</div>
		
		
		
		
	</div>
	
	<div class="row">
				<div class="col-sm-12 col-md-12 top10" id="jeFFcoEmployeeInf">				
				</div>
	</div>
	<!-- 
 	<div id="ethnicOriginDiv">
	<div class="row">
		<div class="col-sm-9 col-md-9">
			<div class="portfolio_Subheading" id="eeodForMiamiHeader"><spring:message code="msgDp_commons22" /></div>
	   </div>
	</div>
			<div class="row">
				<div class="col-sm-12 col-md-12" id="eeodForMiami" style="display: none;">
					<label><strong>
						<spring:message code="msgDp_commons23" />
						</BR><spring:message code="msgDp_commons24" />
						</BR><spring:message code="msgDp_commons25" />
						</BR><spring:message code="msgDp_commons26" /> <a href="mailTo:crc@dadeschools.net">crc@dadeschools.net</a>.
					</strong></label>
				</div>
				
				<div class="col-sm-12 col-md-12 top5" id="eeodForOthers" style="display: none;">
					<label><strong>
					  <spring:message code="msgDp_commons27" />
					</strong></label>
				</div>
				
			</div>
		<div class="row top5">
			<div class="col-sm-12 col-md-12 hide" id="eeoctext">				
		   </div>
		</div>
	
	
	
	
			<c:choose>
		<c:when test="${districtMaster.districtId == 7800038}">

			<div class="row nobleCssShow">
				<div class="col-sm-12 col-md-12">
				<spring:message code="msgDp_commons28" />
				</div>
			</div>
		</c:when>
		<c:otherwise>
		<div class="row hide nobleCssShow">
				<div class="col-sm-12 col-md-12">
				<spring:message code="msgDp_commons28" />
				</div>
			</div>
		</c:otherwise>
		</c:choose>
	-->	
	
	<div id="ethnicOriginDiv" class="miamionly">
	<div class="row">
		<div class="col-sm-9 col-md-9">
			<div class="portfolio_Subheading" id="eeodForMiamiHeader"><spring:message code="msgDp_commons22" /></div>
	   </div>
	</div>
			<div class="row">
				<div class="col-sm-12 col-md-12" id="eeodForMiami" style="display: none;">
					<label><strong>
						<spring:message code="msgDp_commons23" />
						</BR><spring:message code="msgDp_commons24" />
						</BR><spring:message code="msgDp_commons25" />
						</BR><spring:message code="msgDp_commons26" /> <a href="mailTo:crc@dadeschools.net">crc@dadeschools.net</a>.
					</strong></label>
				</div>
	</div>
		<div class="row">
			<div class="col-sm-6 col-md-6">
				<div class="span15 portfolio_Subheading"><spring:message code="lblEthOrig" /></div>		
			</div>	
		 </div>	
		 <div class="row">	
			<div class="col-sm-9 col-md-9">
				<div class='divErrorMsg' style="display: none;" id="errEthnicOrigin"></div>
			</div>
		</div>	
		<div class="row">			
				<c:forEach items="${lstethnicOriginMasters}" var="ethnicOrigin">
					<div class="col-sm-3 col-md-3">
	        		<label class="radio" style="margin-top: 2px;margin-bottom: 2px;">		
						<input type="radio" name="ethnicOriginId" id="ethnicOriginId" value="${ethnicOrigin.ethnicOriginId}">${ethnicOrigin.ethnicOriginName} 
					</label>					
					</div>	         			
				</c:forEach>						
		</div>
	 </div>
	 
	 <div id="ethinicityMasterDiv" class="miamionly">
		<div class="row">
			<div class="col-sm-6 col-md-6">
				<div class="span4 portfolio_Subheading"><spring:message code="lblEth" /></div>		
			</div>
		</div>
		<div class="row">	
			<div class="col-sm-9 col-md-9">
				<div class='divErrorMsg' style="display: none;" id="errEthinicity"></div>
		    </div>
	    </div>
		<div class="row">			
				<c:forEach items="${lstEthinicityMasters}" var="ethinicityMaster">
					<div class="col-sm-3 col-md-3">
	        		<label class="radio" style="margin-top: 2px;margin-bottom: 2px;">		
						<input type="radio" name="ethinicityId" id="ethinicityId" value="${ethinicityMaster.ethnicityId}">${ethinicityMaster.ethnicityName}</br> 
					</label>
					<!--<c:set var="checked" value=""></c:set>-->
					</div>	         			
				</c:forEach>		
		</div>
	   </div>

 	<div id="raceDiv">
 	<div class="row top5">
<div class="col-sm-12 col-md-12 top5" id="eeodForOthers" style="display: none;">
	<label><strong>
	  <spring:message code="msgDp_commons27" />
	</strong></label>
</div>
</div>
<div class="row top5">
<div class="col-sm-12 col-md-12 hide" id="eeoctext">				
</div>
</div>

<c:choose>
<c:when test="${districtMaster.districtId == 7800038}">

<div class="row nobleCssShow">
<div class="col-sm-12 col-md-12">
<spring:message code="msgDp_commons28" />
</div>
</div>
</c:when>
<c:otherwise>
<div class="row hide nobleCssShow">
<div class="col-sm-12 col-md-12">
<spring:message code="msgDp_commons28" />
</div>
</div>
</c:otherwise>
</c:choose>
 	
		<div class="row">
			<div class="col-sm-6 col-md-6" >
				<div class="portfolio_Subheading">Race/Ethnicity</div>		
			</div>
		</div>
		<div class="row">
		<div class="col-sm-9 col-md-9" >
		<div class='divErrorMsg' style="display: none;" id="errRace"></div>
		</div>
		</div>
		
		<div class="row">
		<div class="col-sm-12 col-md-12" style="margin-left:-15px;">
			<div  id="raceData"></div>
		</div>
	    </div>
	 </div>
	<div id="genderDiv">
		<div class="row">
			<div class="col-sm-6 col-md-6" >
				<div class="span4 portfolio_Subheading"><spring:message code="lblGend" /></div>		
			</div>
		</div>
		<div class='divErrorMsg' style="display: none;" id="errGender"></div>
		<div class="row">
			<div class="span15" " id="genderCheckDiv">
			 	<c:forEach items="${lstGenderMasters}" var="lstGenderMasters">
					<div class="col-sm-2 col-md-2">
		        		<label class="radio" id="displayGender${lstGenderMasters.genderId}" style="margin-top: 2px;margin-bottom: 2px;">		
							<input type="radio" name="genderId" id="genderId" value="${lstGenderMasters.genderId}">${lstGenderMasters.genderName}</br> 
						</label>
					</div>	         			
				</c:forEach>				
			</div>
		</div>
	 </div>
  <div id="drivingDiv" style="display:none">
	   <div class="row">
            <div class="col-sm-6 col-md-6" >
                <div class="span4 portfolio_Subheading">Drivers License</div>        
            </div>
        </div>
        <div class="row">
        
        <div class="col-sm-3 col-md-3" >
            <label>Drivers License Number<span class="required">*</span></label>
            <input type="text" id="drivingLicNum" class="form-control" >
        </div>
        <div class="col-sm-5 col-md-5" >
            <label>Drivers License State<span class="required">*</span></label>
            <select  class="form-control" id="drivingLicState">   
                <option value="">Select State</option>
                <c:forEach items="${listStateMaster}" var="st">
                    <option id="st${st.stateId}" value="${st.stateId}" ${selected} >${st.stateName}</option>
                </c:forEach>                
            </select>
        </div>
        </div>
	 </div>
 <!--  Start :: Expected salary Body-->
<div class="hide" id="expectedSalaryDiv">
	<div class="row">
		<div class="col-sm-6 col-md-6" >
		<div class="span4 portfolio_Subheading"><spring:message code="lblExpecSalary" /><span class="required expSalaryCss">*</span></div>		
	</div>
	</div>
	
	<div class="row">
		<div class="col-sm-3 col-md-3"> 	
			<div class='divErrorMsg' style="display: none;" id="errExpSalary"></div>
		</div>	 
	</div>

	<div class="row">	
		<div class="col-sm-3 col-md-3"> 
		<input type="text" name="expectedSalary" id="expectedSalary" onkeypress="return checkForInt(event);" class="form-control" maxlength="20"  value="${teacherpersonalinfo.expectedSalary}"/>
		</div>		
	</div>
</div>

<!-- Start :: Expected salary end -->
<!-- Start :: Residency Body -->
	<div id="residency" class="hide">
			<div class="row portfolio_Section_Gap">
				<div class="col-sm-12 col-md-12">				
					<div style="float: left" class="portfolio_Subheading">
						Residency
						<!--<span id="acaddemicHelpDiv"><a href="#" data-html='true' id="academicHelpTooltip" rel="tooltip" data-original-title=""><img src="images/qua-icon.png" width="15" height="15" alt=""></a><span>
					--></div>
					<div style="float: right" class="addPortfolio">
							<a href="#" onclick="return addResidencyForm();">+ Add Residency </a>
					</div>
					<div style="clear: both;"></div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12 col-md-12">Please list all of the places you have lived over the past 10 years.</div>
			</div>
			<div class="row" onmouseover="setGridNameFlag('residency')">
				<div class="col-sm-12 col-md-12">
					<div id="residencyGridDiv" class="residencyGridDiv"></div>
				</div>
			</div>
			<div id="residencyForm" class="hide">
				<div class="row">

				<div class="col-sm-12 col-md-12">
					<div class='divErrorMsg' id='errordivResidency' style="display: block;"></div>
				</div>

					<div class="col-sm-4 col-md-4">
						<label>Street Address<span class="required">*</span></label>
						<input type="text" id="residencyStreetAddress" name="residencyStreetAddress" class="form-control" value="">
					</div>

				
					<div class="col-sm-3 col-md-3">
						<label>City<span class="required">*</span></label>
						<input type="text" id="residencyCity" name="residencyCity" class="form-control" value="">
					</div>

					<div class="col-sm-3 col-md-3">
						<label>State<span class="required">*</span></label>
						<input type="text" id="residencyState" name="residencyState" class="form-control" value="">
					</div>

					<div class="col-sm-2 col-md-2">
						<label>Zip<span class="required">*</span></label>
						<input type="text" id="residencyZip" name="residencyZip" class="form-control" value="">
					</div>
					<div class="col-sm-4 col-md-4">
						<label>Country<span class="required">*</span></label>
						<input type="text" id="residencyCountry" name="residencyCountry" class="form-control" value="">
					</div>
					<div class="col-sm-3 col-md-3">
						<label>From Date<span class="required">*</span></label>
						<input type="text" id="residencyFromDate" name="residencyFromDate" class="form-control" value="">
					</div>
					<div class="col-sm-3 col-md-3">
						<label>To Date<span class="required">*</span></label>
						<input type="text" id="residencyToDate" name="residencyToDate" class="form-control" value="">
					</div>
				</div>
				<div class="row">
					<div class="col-sm-2 col-md-2">
					<input type="hidden" id="teacherResidencyId" name="teacherResidencyId">
						<a class="idone" style="cursor: pointer; margin-left: 0px; text-decoration:none;" onclick="insertOrUpdate_Residency()">I'm  Done</a>&nbsp;&nbsp;
						<a class="idone" style="cursor: pointer; margin-left: 0px; text-decoration:none;"	onclick="closeResidencyForm()">Cancel</a>
					</div>
				</div>
			</div>
	</div>
<!-- End :: Residency Body -->
<!-- Education div -->
	
	<div clas="hide" id="educationDiv">
	<div class="row portfolio_Section_Gap">	
		<div class="col-sm-12 col-md-12">		
		 <!--<div style="float: left" class=" hide" id="divLblEducation" >
				Academic History on file with NC licensure
			</div>
		 	
		--><!-- 	<div style="float: left" class="portfolio_Subheading">
					<font size="2px"><a href="#" id="gridEducationDSPQ" onclick="return getEducation()" >Load Education Information from NCDPI</a></font>
			</div>
		 -->	
	<!-- -		<div style="float: right" class="addPortfolio hide" id="divAddEducation" style="display: none;">
					<a href="#" id="addeducationDSPQ" onclick="return showEducation()" >+Add Education</a>
         </div>
	-->		
		</div>
	</div>
	
	
<!-- 	
	<form id="frmEducation" name="frmEducation" onsubmit="return false;">
									    <input type="hidden" id="licensureeducationId" name="licensureeducationId"/>
									    <div  id="divEducation" style="display: none;">
										    <br>
										    <div class="row col-sm-10 col-md-10">
										    <div class='divErrorMsg' id='errordivEducation'  style="display: block;"></div>
										    </div>	
										    
										    <div class="row ">	
										    
										  <div class="col-sm-3 col-md-3 "><label>School</label>
										    <input  type="hidden" id="universityIdEd" value="">
												<input  type="text" 
													class="form-control"
													maxlength="100"
													id="universityNameEd" 
													autocomplete="off"
													value=""
													name="universityNameEd" 
													onfocus="getUniversityAutoComp(this, event, 'divTxtUniversityDataEd', 'universityNameEd','universityIdEd','');"
													onkeyup="getUniversityAutoComp(this, event, 'divTxtUniversityDataEd', 'universityNameEd','universityIdEd','');" 
													onblur="hideUniversityDiv(this,'universityIdEd','divTxtUniversityDataEd');"/>
												<div id='divTxtUniversityDataEd' style=' display:none;' 
												onmouseover="mouseOverChk('divTxtUniversityDataEd','universityNameEd')" class='result' ></div>
										    
										    </div>
										    
										    <div class="col-sm-3 col-md-3 "><label>Graduation Date</label>
											 <span class="">
												<input type="text" id="graduationDate" name="graduationDate"
													class="help-inline form-control">
											</span>
										    </div>				
										    							    
										    <div class="col-sm-3 col-md-3 "><label>Degree</label>
										   <input  type="hidden" id="degreeIdEd" value="">
											<input  type="hidden" id="degreeTypeEd" value="">
											<input  type="text"
												maxlength="50" 
												class="form-control input-small"
												id="degreeNameEd" 
												autocomplete="off"
												value=""
												name="degreeNameEd" 
												onfocus="getDegreeTypeMasterAutoComp(this, event, 'divTxtShowDataEd', 'degreeNameEd','degreeIdEd','');"
												onkeyup="getDegreeTypeMasterAutoComp(this, event, 'divTxtShowDataEd', 'degreeNameEd','degreeIdEd','');" 
												onblur="hideDegreeMasterDiv(this,'degreeIdEd','divTxtShowDataEd');checkGED();"/>
											<div id='divTxtShowDataEd' style=' display:none;' class='result' 
											onmouseover="mouseOverChk('divTxtShowDataEd','degreeNameEd')"></div>
										   
										   
										   
									        </div>
										    	
										   <div class="col-sm-3 col-md-3">
											<label >
												Major <span class="required nondgrReq">*</span>
											</label>						
											<input  type="hidden" id="fieldIdEd" value="">
											<input  type="text" 
												class="form-control"
												maxlength="50"
												autocomplete="off"
												id="fieldNameEd"
												value=""
												name="fieldNameEd" 
												onfocus="getFieldOfStudyAutoComp(this, event, 'divTxtFieldOfStudyDataEd', 'fieldNameEd','fieldIdEd','');"
												onkeyup="getFieldOfStudyAutoComp(this, event, 'divTxtFieldOfStudyDataEd', 'fieldNameEd','fieldIdEd','');" 
												onblur="hideFeildOfStudyDiv(this,'fieldIdEd','divTxtFieldOfStudyDataEd');"/>
											<div id='divTxtFieldOfStudyDataEd' style=' display:none;' 
											onmouseover="mouseOverChk('divTxtFieldOfStudyDataEd','fieldNameEd')"  class='result' ></div>					
										</div>
										    	
										    	
										    	
										    <div class="row col-sm-3 col-md-3 idone top10 rowmargin" style="margin-left:4px;">
												<a href="#" onclick="return saveOrUpdateEducation()">Save Section</a>
												&nbsp;<a href="#"	onclick="return hideEducation()">
													Cancel
												</a>
											</div>	
										    
											</div>
																	
											
									    </div>
									    
									    </form>
	 -->
	</div>
	<!--  End :: Education -->




<!-- end :: sandeep-->

<!-- Start :: Academic Body -->
	<div id="academicsDiv">		
	<div class="row">
	<div class="col-sm-12 col-md-12">				
				<div style="float: left;margin-bottom: -19px;" class="portfolio_Subheading">
					<spring:message code="headAcad" /><span class="required hide" id="reqAcademicAstrick">*</span>
					<span id="acaddemicHelpDiv"><a href="#" data-html='true' id="academicHelpTooltip" rel="tooltip" data-original-title=""><img src="images/qua-icon.png" width="15" height="15" alt=""></a><span>					
				</div>
	</div>
	</div>	
	
	<div class="row">
	<div class="col-sm-12 col-md-12">		
		    <div style="float: left" class="hide" id="divLblEducation" >
				&nbsp;&nbsp;&nbsp;&nbsp;<font style="color:#007AB4;"><spring:message code="lblAcadHistoryFileNCLic" /></font>
			</div>
	</div>
	</div>
	<input type="hidden" id="divDataEducationVal" value="0">
	<div class="row" >
		<div class="col-sm-12 col-md-12" id="divDataEducation"></div>
	</div>
		
		<div class="row top10">		
			<div class="col-sm-12 col-md-12 ">				
				<div class="hide" style="float: left " id="AcadHistoryProvidedCand">
					&nbsp;&nbsp;&nbsp;&nbsp;<font style="color:#007AB4;"><spring:message code="lblAcadHistoryProvidedCand" /></font>
					
				</div>			
				
				       <div style="float: right" >
						    <a href="#" id="addSchoolIdDSPQ"  onclick="return showUniversityForm()"><spring:message code="lnkAddSch" /> </a>
						   </div>
				
				<div style="clear: both;"></div>	
				
			</div>
		</div>
		<c:if  test="${districtMaster.districtId eq 806810 && (jobOrder.jobCategoryMaster.jobCategoryName eq 'Paraprofessional')}">
		<div class="col-sm-12 col-md-12 " style="margin-left: -15px;" id="accadmicMsgForsummut">
 			<spring:message code="msgforacademic" />
 		</div>
 		</c:if>
				
		<div class="row" onmouseover="setGridNameFlag('academics')">
		   <div class="col-sm-12 col-md-12">
		      <div id="divDataGridAcademin"></div>
			</div>
		</div>

		<div class="portfolio_Section_ImputFormGap" id="divAcademicRow" style="display: none;" onkeypress="chkForEnter_Academic(event)">
		<iframe id='uploadFrameAcademicID' name='uploadFrameAcademic' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'>
		</iframe>
		<form class="textfield11" id="frmAcadamin" enctype='multipart/form-data' method='post' target='uploadFrameAcademic' action='fileuploadservlet.do'>
			<input type="hidden" value="" id="academicId" name="academicId"/>
			<div class="row">							
					<div class="col-sm-9 col-md-9">
		      			<div class='divErrorMsg' id='errordiv_AcademicForm' style="display: block;"></div>					              	
				    </div>
			</div>
			<div class="row">
					<div class="col-sm-3 col-md-3">
						<label>
							<spring:message code="lblDgr" /><span class="required nondgrReq">*</span>
							<a href="#" id="degreeTooltip" rel="tooltip" data-original-title='<spring:message code="msgDp_commons29" />'><img src="images/qua-icon.png" width="15" height="15" alt=""></a>
							<a href="#" onclick="document.getElementById('degreeName').value='Others';document.getElementById('degreeName').focus();return false;" style="margin-left: 0px;"><spring:message code="msgDp_commons30" /></a>
						</label>					
						<input  type="hidden" id="degreeId" value="">
						<input  type="hidden" id="degreeType" value="">
						<input  type="text"
							maxlength="50" 
							class="form-control input-small"
							id="degreeName" 
							autocomplete="off"
							value=""
							name="degreeName" 
							onfocus="getDegreeMasterAutoComp(this, event, 'divTxtShowData', 'degreeName','degreeId','');"
							onkeyup="getDegreeMasterAutoComp(this, event, 'divTxtShowData', 'degreeName','degreeId','');" 
							onblur="hideDegreeMasterDiv(this,'degreeId','divTxtShowData');checkGED();"/>
						<div id='divTxtShowData' style=' display:none;' class='result' 
						onmouseover="mouseOverChk('divTxtShowData','degreeName')"></div>				
					</div>
					
					<div class="col-sm-4 col-md-4">
						<label >
							<spring:message code="lblSchool" /> <span class="required nondgrReq">*</span><a href="#" onclick="document.getElementById('universityName').value='Other';document.getElementById('universityName').focus();return false;" style="margin-left: 90px;"><spring:message code="lnkMySchNotLi" /></a>
						</label>					
						<input  type="hidden" id="universityId" value="">
						<input  type="text" 
							class="form-control"
							maxlength="100"
							id="universityName" 
							autocomplete="off"
							value=""
							name="universityName" 
							onfocus="getUniversityAutoComp(this, event, 'divTxtUniversityData', 'universityName','universityId','');"
							onkeyup="getUniversityAutoComp(this, event, 'divTxtUniversityData', 'universityName','universityId','');" 
							onblur="hideUniversityDiv(this,'universityId','divTxtUniversityData');"/>
						<div id='divTxtUniversityData' style=' display:none;' 
						onmouseover="mouseOverChk('divTxtUniversityData','universityName')" class='result' ></div>
						
					</div>					
					<div class="col-sm-5 col-md-5">
						<label >
							<spring:message code="lblFildOfStudy" /> 
							<c:if test="${!(districtMaster.districtId eq 2633090 and ((jobOrder.jobCategoryMaster.jobCategoryName eq 'Drivers') or (jobOrder.jobCategoryMaster.jobCategoryName eq 'Clerical') or (jobOrder.jobCategoryMaster.jobCategoryName eq 'Other')))}">
							<span class="required nondgrReq">*</span>
							</c:if>
							<a  href="#" onclick="document.getElementById('fieldName').value='Other'; document.getElementById('fieldName').focus();return false;" style="margin-left: 180px;"><spring:message code="lnkMyFldNotLi" /></a>
						</label>						
						<input  type="hidden" id="fieldId" value="">
						<input  type="text" 
							class="form-control"
							maxlength="50"
							autocomplete="off"
							id="fieldName"
							value=""
							name="fieldName" 
							onfocus="getFieldOfStudyAutoComp(this, event, 'divTxtFieldOfStudyData', 'fieldName','fieldId','');"
							onkeyup="getFieldOfStudyAutoComp(this, event, 'divTxtFieldOfStudyData', 'fieldName','fieldId','');" 
							onblur="hideFeildOfStudyDiv(this,'fieldId','divTxtFieldOfStudyData');"/>
						<div id='divTxtFieldOfStudyData' style=' display:none;' 
						onmouseover="mouseOverChk('divTxtFieldOfStudyData','fieldName')"  class='result' ></div>					
					</div>				
			</div>
			<div class="row">				
						<div class="col-sm-12 col-md-12" id="showhide1">
							<label>
							<c:if test="${districtMaster.districtId ne 804800}">
								<spring:message code="lblDatAtt" /><span class="required nondgrReq1">*</span>
							</c:if>
							
							<c:if test="${districtMaster.districtId eq 804800}">
								<spring:message code="lblDatAtt" />
							</c:if>
							
							</label>
						 </div>
					  	
					  	<div class="col-sm-2 col-md-2" id="showhide2">
						<select class="form-control" id="attendedInYear" name="attendedInYear">
							<option id="attendedInYearSelect" value=""><spring:message code="optSlt" /></option>
							<c:forEach var="year" items="${lstLastYear}">							
								<option id="attendedInYear${year}" value="${year}">${year }</option>
							</c:forEach>					
						</select>
						</div>
						<div class="fl to" style="margin-left:-7px;" id="showhide3">
							<spring:message code="lblTo" />
						</div>
						<div class="col-sm-2 col-md-2" style="margin-left:-7px;" id="showhide4">
							<select class="span1 form-control" id="leftInYear" name="leftInYear">
								<option id="attendedInYearSelect" value=""><spring:message code="optSlt" /></option>
								<c:forEach var="year" items="${lstLastYear}">
									<option id="leftInYear${year}" value="${year}">${year }</option>
								</c:forEach>						
							</select>
					    </div>
					<input type="hidden" id="sbtsource_aca" name="sbtsource_aca" value="0"/>					
					
					<c:set var="transUDis" value=""></c:set>
					<c:if test="${districtMaster.districtId == 7800038}">					
						<c:set var="transUDis" value="hide"></c:set>
					</c:if>
			
				<c:if test="${districtMaster.districtId != 1200390}">	
						<div  id="transcriptDiv" class="nobleCssHide ${transUDis}"><BR/>
						<div  class="col-sm-3 col-md-3" style="margin-top: -23px;">
							<label>
								<spring:message code="lblTDC" /> <span class="required" id="acadTransReq">*</span><a href="#" id="transUploadTooltip" rel="tooltip" data-original-title='<spring:message code="msgUploadTranscript" />'><img src="images/qua-icon.png" width="15" height="15" alt=""></a>
							</label>							
							<input id="pathOfTranscript" name="pathOfTranscript" type="file" width="20px;">

							<a href="javascript:void(0)" onclick="clearTranscript()"><spring:message code="lnkClear" /></a>
						</div>
						<span class="col-sm-3 col-md-3" id="divResumeSection" name="divResumeSection" style="display: none;">
							<label>
								&nbsp;&nbsp;
							</label>
							<span id="divResumerName"></span>
							&nbsp;&nbsp;<a href="javascript:void(0)" onclick="removeTranscript()" class="left5"><spring:message code="lnkRmovS" /></a>
						</span>					  </div>
					</c:if>
							<input type="hidden" value="0" id="aca_file">
			</div>
			
		</form>
	</div>
	
	<div id="divGPARow" style="display: none; " onkeypress="chkForEnter_Academic(event)">
	<form  id="frmGPA">
		   <div class="row">
				<div class="col-sm-2 col-md-2">
					<spring:message code="lblGPA" />
					<a href="#" id="iconpophover1" rel="tooltip"
						data-original-title="Grade Point Average"><img
							src="images/qua-icon.png" width="15" height="15" alt="">
					</a>
				</div>
			</div>
        <div class="row">
		<div class="col-sm-2 col-md-2">
			<label>
				<spring:message code="lblFresh" />
			</label>
			<input type="text" id="gpaFreshmanYear"  name="gpaFreshmanYear"  onkeypress='return checkForDecimalTwo(event)' class="input-small form-control" id="gpaFreshman" maxlength="4">
		</div>
		<div class="col-sm-2 col-md-2">
			<label>
				<spring:message code="lblSoph" />
			</label>
			<input type="text" id="gpaSophomoreYear"  name="gpaSophomoreYear" onkeypress='return checkForDecimalTwo(event)'  class="input-small form-control" id="gpaSophomore" maxlength="4">
		</div>
		<div class="col-sm-2 col-md-2">
			<label>
				<spring:message code="lblJunior" />
			</label>
			<input type="text" id="gpaJuniorYear"  name="gpaJuniorYear" onkeypress='return checkForDecimalTwo(event)'  class="input-small form-control" id="gpaJunior" maxlength="4">
		</div>
		<div class="col-sm-2 col-md-2">
			<label>
				<spring:message code="lblSenior" />
			</label>
			<input type="text" id="gpaSeniorYear"  name="gpaSeniorYear" onkeypress='return checkForDecimalTwo(event)'  class="input-small form-control" id="gpaSenior" maxlength="4">

		</div>
		<div class="col-sm-2 col-md-2">
			<label>
				<spring:message code="lblCumulative" />
				<c:if test="${districtMaster.districtId ne 804800}">
				<span class="required nondgrReq philNT" id="crequired">*</span>
				</c:if>
			</label>
			<input type="text" id="gpaCumulative"  name="gpaCumulative" onkeypress='return checkForDecimalTwo(event)'  class="input-small form-control" id="gpaCumulative" maxlength="4">
		</div>
				
		<c:choose>
		<c:when test="${districtMaster.districtId == 7800038}">
		<div class="col-sm-2 col-md-2 nobleCssShow">
            <label class="checkbox" style="margin-top:31px;">
                  <input type="checkbox" id="international" name="international" onclick="return chkInternational();">&nbsp;&nbsp;<spring:message code="lblInternational" />
                  &nbsp;<a href="http://www.foreigncredits.com/Resources/GPA-Calculator/" class="calculatortooltip" rel="tooltip" data-original-title='<spring:message code="msgClickIntnGPA" />'><img src="images/qua-icon.png" width="15" height="15" alt=""></a>
                  </label>
                  
            </div>
		</c:when>
		<c:otherwise>
		<div class="col-sm-2 col-md-2 hide nobleCssShow">
            <label class="checkbox" style="margin-top:31px;">
                  <input type="checkbox" id="international" name="international" onclick="return chkInternational();">&nbsp;&nbsp;<spring:message code="lblInternational" /> <a href="http://www.foreigncredits.com/Resources/GPA-Calculator/" class="calculatortooltip" rel="tooltip" data-original-title='<spring:message code="msgClickIntnGPA" />'><img src="images/qua-icon.png" width="15" height="15" alt=""></a>
                  </label>
                  
            </div>
		</c:otherwise>
		</c:choose>
		</div>
	</form>
	</div>
	
	
	<div id="divGPARowOther" style="display: none;" onkeypress="chkForEnter_Academic(event)">
		<form id="frmGPA1">		
				<div class="row" >
					<div class="col-sm-2 col-md-2">
						<spring:message code="lblGPA" />
						<a href="#" id="iconpophover2" rel="tooltip"
							data-original-title="Grade Point Average"><img
								src="images/qua-icon.png" width="15" height="15" alt="">
						</a>
					</div>
				</div>
				<div class="row">		
					<div class="col-sm-2 col-md-2">
						<label>
							<spring:message code="lblCumulative" /><span class="required nondgrReq philNT" id="crequired2">*</span>
						</label>
						<input type="text" id="gpaCumulative1"  name="gpaCumulative1" onkeypress='return checkForDecimalTwo(event)'  class="input-small form-control" id="gpaCumulative" maxlength="4">
					</div>
					<c:choose>
		<c:when test="${districtMaster.districtId == 7800038}">
		<div class="col-sm-2 col-md-2">
            <label class="checkbox" style="margin-top:31px;">
                  <input type="checkbox" id="international2" class="international1" name="international" onclick="return chkInternational();">&nbsp;&nbsp;<spring:message code="lblInternational" />&nbsp; <a href="http://www.foreigncredits.com/Resources/GPA-Calculator/" class="calculatortooltip" rel="tooltip" data-original-title="Click here to change International GPA Score"><img src="images/qua-icon.png" width="15" height="15" alt=""></a>
                  </label>
            </div>
		</c:when>
		<c:otherwise>
		<div class="col-sm-2 col-md-2 hide nobleCssShow">
            <label class="checkbox" style="margin-top:31px;">
                  <input type="checkbox" id="international2" class="international1" name="international" onclick="return chkInternational();">&nbsp;&nbsp;<spring:message code="lblInternational" /> <a href="http://www.foreigncredits.com/Resources/GPA-Calculator/" class="calculatortooltip" rel="tooltip" data-original-title="Click here to change International GPA Score"><img src="images/qua-icon.png" width="15" height="15" alt=""></a>
                  </label>
            </div>
		</c:otherwise>
		</c:choose>
				</div>
		</form>
	</div>
	
	
	<div class="row">
		<div class="col-sm-3 col-md-3" id="divDone" style="display: none; ">
			<a class="idone" style="cursor: pointer; margin-left: 0px; text-decoration:none;" onclick="insertOrUpdate_Academic(0)">
				<spring:message code="lnkImD" />
			</a>&nbsp;&nbsp;
			<a class="idone" style="cursor: pointer; margin-left: 0px; text-decoration:none;"	onclick="resetUniversityForm()">
				<spring:message code="btnClr" />
			</a>
		</div>
	</div>
  </div>
	<!-- end :: academic body -->
	<!-- ----add licncse data -->
	<div id="licenseDivDate" class="hide" >	
	<div class="row portfolio_Section">
			<div class="col-sm-12 col-md-12">			
				<div style="float: left" class="portfolio_Subheading">
				<spring:message code="lblLicense" />					
				</div>				
			<div style="clear: both;"></div>	
		    </div>		   
	</div>
	<div class="row" >
	    <div class="col-sm-12 col-md-12">
			<div id="divLicneseGridCertificationsGridDate"></div>
		</div>
    	</div>
	</div>	
	<br>	
	
		<div id="licenseDivArea" class="hide" >	
	<div class="row portfolio_Section">
			<div class="col-sm-12 col-md-12">			
				<div style="float: left" >
				&nbsp;&nbsp;&nbsp;&nbsp;<font style="color:#007AB4;"><spring:message code="lblLEACandidatePortfolio" /></font>					
				</div>
				<!--<div style="float: right" class="addPortfolio">
					<a href="javascript:void(0);" id="licenseButton" onclick="showForm_Licence();" style="margin-left: 200px;"><spring:message code="licenseAddButton" /></a>
				</div>				
			--><div style="clear: both;"></div>	
		    </div>		   
	</div>
	
	<div class="row" >
	    <div class="col-sm-12 col-md-12">
			<div id="divLicneseGridCertificationsGridArea"></div>
		</div>		
    	</div>
	</div>
	
	
	<!--<div id="licenseDiv" class="hide" >	
	<div class="row portfolio_Section">
			<div class="col-sm-12 col-md-12">			
				<div style="float: left" class="portfolio_Subheading">
					Licensure <a href="#" id="iconpoplicensure" rel="tooltip" data-original-title='<spring:message code="msgLicensureNCDPI" />'><img src="images/qua-icon.png" width="15" height="15" alt=""></a></strong>
				</div>
				<div style="float: right" class="addPortfolio">
					<a href="javascript:void(0);" id="licenseButton" onclick="showForm_Licence();" style="margin-left: 200px;"><spring:message code="licenseAddButton" /></a>
				</div>
			<div style="clear: both;"></div>	
		    </div>
		   
	</div>
	<div class="row" >
	    <div class="col-sm-12 col-md-12">
			<div id="divLicneseGridCertificationsGrid"></div>
		</div>
		<div class="col-sm-12 col-md-12 top10" id="ncdpiLicensescertification">If you have an NC license that information is provided by NCDPI. If any licensure data is missing, click Add Certification/Licensure and add it below.<br>
	If any of the NC license data provided by NCDPI is inaccurate, please contact NCDPI Licensure M-F 8-5 EST at 1.800.577.7994 within North Carolina or 1.919.807.3310 outside of North Carolina.</div>
	</div>
	</div>
	--><!-- end license data -->
		
	<!-- Start :: Certifications Body -->
<div id="certificationDiv">	
	<div class="row portfolio_Section_Gap">
			<div class="col-sm-12 col-md-12">		
			
			 <%if(Utility.getValueOfPropByKey("nccheckforjspandjs").equalsIgnoreCase("yes")){%>
			<div style="float: left;" class="hide" id="ncCetrificationNC">
					&nbsp;&nbsp;&nbsp;&nbsp;<font style="color:#007AB4;"><spring:message code="lblCertiLicense" /></font><span class="required show" id="reqCerificationAstrick">*</span> <a href="#" id="iconpophoverNCDPIcertificationtooltip" rel="tooltip" data-original-title='<spring:message code="NCDPIcertificationtooltip" />'><img src="images/qua-icon.png" width="15" height="15" alt=""></a>
				</div>	
				
				<div style="float: left" class="portfolio_Subheading hide" id="ncCetrification">
								<spring:message code="lblCertiLicense" /><span class="required show" id="reqCerificationAstrick">*</span> <a href="#" id="iconpophoverNCDPIcertificationtooltip" data-toggle="tooltip" data-original-title='<spring:message code="NCDPIcertificationtooltip"/>'><img src="images/qua-icon.png" width="15" height="15" alt=""></a>
				</div>
				
				<div style="float: left" class="portfolio_Subheading show" id="normalTeacherCerti">
					<spring:message code="lnkCerti/Lice" /><span class="required hide" id="reqCerificationAstrick">*</span>
				</div>
				<%}else{%>
				 <div style="float: left" class="portfolio_Subheading show" id="normalTeacherCerti">
					<spring:message code="lnkCerti/Lice" /><span class="required hide" id="reqCerificationAstrick">*</span>
				</div>
				 <%}%>
					<div style="float: right" >
					<a href="javascript:void(0);" id="addCertificationIdDSPQ" onclick="showForm_Certification(),clearForm_Certification();" ><spring:message code="lnkAddCerti/Lice" /></a>
				</div>
				
				
				<!--<div style="margin-left: 525px;" class=" hide" id="addLicenseCerti">
					<a href="javascript:void(0);" onclick="showForm_Licence();" ><spring:message code="licenseAddButton" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
				</div>
				
				
				--><div style="clear: both;"></div>	
		    </div>
		    <div class="col-sm-12 col-md-12 hide newBerlinCss"><spring:message code="msgDp_commons31" /></div>
		    <div class="col-sm-12 col-md-12 hide philadelphiaNTCss"><spring:message code="msgDp_commons32" /></div>
		    <div class="col-sm-12 col-md-12 hide philadelphiaNTCss1"><spring:message code="msgDp_commons33" /></div>
		    <div class="col-sm-12 col-md-12 hide certiGuiText"></div>	
	</div>
		
	<div class="row" onmouseover="setGridNameFlag('certification')">
	    <div class="col-sm-12 col-md-12">
			<div id="divDataGridCertifications"></div>
		</div>
	</div>
	
	<!-- Input form -->
	<div class="portfolio_Section_ImputFormGap" id="divMainForm" style="display: none;" onkeypress="chkForEnter_Certifications(event)">
		<iframe id='uploadFrameCertificationsID' name='uploadFrameCertifications' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'></iframe>
		<form id="frmCertificate" name="frmCertificate" enctype='multipart/form-data' method='post' target='uploadFrameCertifications' action='fileuploadservletforcertification.do' >
			<input type="hidden" id="certId" name="certId"/>			
				<div class="row">
				  <div class="col-sm-12 col-md-12">
		 			<div id='divServerError' class='divErrorMsg' style="display: block;">${msgError}</div>
		 			<div class='divErrorMsg' id='errordiv_Certification' style="display: block;"></div>
		 		  </div>
				</div> 
		
		<div class="row">
				<div class="col-sm-4 col-md-4"><label><strong><spring:message code="lblCerti/LiceSt" /><span class="required">*</span> <a href="#" id="clsToolTip" class="hide" rel="tooltip" data-original-title='<spring:message code="msgDp_commons32" />'><img src="images/qua-icon.png" width="15" height="15" alt=""></a></strong></label>
			<select  class="form-control" id="certificationStatusMaster" onchange="fieldsDisable(this.value);">  
				<option value=""><spring:message code="lblSltCerti/LuceStatus" /></option>
				<c:forEach items="${lstCertificationStatusMaster}" var="csml">
	        		<option id="csml${csml.certificationStatusId}" value="${csml.certificationStatusId}" ${selected} >${csml.certificationStatusName}</option>
				</c:forEach>
			</select>
		</div>
		
		<div class="col-sm-3 col-md-3 certClass">
			<label>
				<strong id="CertTypeLabel"><spring:message code="lblCertiRp" /><span class="required">*</span>
				<a href="#" id="certificationtypeMasterTool" rel="tooltip" data-original-title="Add your administrator, teacher, and other certificates"><img src="images/qua-icon.png" width="15" height="15" alt=""></a>
				</strong>
			</label>
			<select id="certificationtypeMaster" name="certificationtypeMaster" class="form-control">
				<option value="0"><spring:message code="optSlt" /></option>
				 <c:forEach var="cvar" items="${listCertTypeMasters}">
					<option  value="${cvar.certificationTypeMasterId}">${cvar.certificationType}</option>
						</c:forEach>				
		</select>
		</div>
										 
		<div class="col-sm-4 col-md-4 certClass"><label><strong>State<span class="required">*</span></strong></label>
			<select  class="form-control" id="stateMaster" onchange="getPraxis(),clearCertType();">   
				<option value=""><spring:message code="optSltSt" /></option>
				<c:forEach items="${listStateMaster}" var="st">
	        		<option id="st${st.stateId}" value="${st.stateId}" ${selected} >${st.stateName}</option>
				</c:forEach>				
			</select>	
		</div>
		
		<div class="col-sm-2 col-md-2 certClass certDatesFlReq" style="width:14%;"><label><strong>Year Received<span class="required certiDatesReq">*</span></strong></label>
			<select id="yearReceived" name="yearReceived"  class="form-control" >
				<option id="yearReceivedSelect" value=""><spring:message code="optSlt" /></option>
				<c:forEach var="year" items="${lstLastYear}">							
					<option id="yearReceived${year}" value="${year}">${year }</option>
				</c:forEach>					
			</select>
		</div>
		
		<div class="col-sm-2 col-md-2 certClass certDatesFlReq" style="width:19.5%;">	
			<label><strong><spring:message code="lblYearExp" /><c:if test="${jobOrder.jobCategoryMaster.jobCategoryName eq 'Professional Certified'}"><span class="required">*</span></c:if>
			
			 <%if(Utility.getValueOfPropByKey("nccheckforjspandjs").equalsIgnoreCase("yes")){%>
				    	<span id="yearexp" class="required hide">*</span>
			<%}%>	
			
			</strong></label>
			<select class="form-control" name="yearexpires" id="yearexpires" style="padding-left:6px;">
				<option value="">Select Year</option>
				<option value=<spring:message code="optDoesNotExpire" />><spring:message code="optDoesNotExpire" /></option>
				<c:forEach var="year" items="${lstComingYear}">							
					<option id="yearExpires${year}" value="${year}">${year }</option>
				</c:forEach>
			</select>
		</div>
									
		<div class="col-sm-7 col-md-7 certClass">
			<c:choose>
 			<c:when test="${districtMaster.districtId eq 806810 }">
 				<c:choose>
       				<c:when test="${jobOrder.jobCategoryMaster.jobCategoryName eq 'Teacher' ||  jobOrder.jobCategoryMaster.jobCategoryName eq 'Administrator'}">
         		 		<label><strong id="cert_LicName"><spring:message code="lblCert/LiceName1" /><span class="required">*</span></strong>
				<a href="#" style="margin-left: 180px;" onclick="showcertiDiv()"><spring:message code="lnkCerti/LiceNotLi" /></a>
			</label>
       			</c:when>
       			<c:otherwise>
        			<label><strong id="cert_LicName"><spring:message code="lblCert/LiceName" /><span class="required">*</span></strong>
				<a href="#" style="margin-left: 180px;" onclick="showcertiDiv()"><spring:message code="lnkCerti/LiceNotLi" /></a>
			</label>
     			</c:otherwise>
		     	</c:choose>
     		</c:when>
     	     	<c:otherwise>
        			<label><strong id="cert_LicName"><spring:message code="lblCert/LiceName" /><span class="required">*</span></strong>
				<a href="#" style="margin-left: 180px;" onclick="showcertiDiv()"><spring:message code="lnkCerti/LiceNotLi" /></a>
			</label>
     			</c:otherwise>
  	 	</c:choose>
			
			<input  type="hidden" id="certificateTypeMaster" value="">
			<input  type="text" 
				class="form-control"
				maxlength="500"
				id="certType" 
				value=""
				name="certType" 
				onfocus="getCertificateTypeAutoComp(this, event, 'divTxtCertTypeData', 'certType','certificateTypeMaster','');"
				onkeyup="getCertificateTypeAutoComp(this, event, 'divTxtCertTypeData', 'certType','certificateTypeMaster','');" 
				onblur="hideCertificateType(this,'certificateTypeMaster','divTxtCertTypeData');"/>
			<div id='divTxtCertTypeData' style=' display:none;' class='result' ></div>
			
		</div>
		
		<!--
		
		<div class="span3"><label><strong>Certification/Licensure Status<span class="required">*</span></strong></label>
			<select  class="span3" id="certificationStatusMaster">   
				<option value="">Select Certification/Licensure Status</option>
				<c:forEach items="${lstCertificationStatusMaster}" var="csml">
	        		<option id="csml${csml.certificationStatusId}" value="${csml.certificationStatusId}" ${selected} >${csml.certificationStatusName}</option>
				</c:forEach>
				
			</select>
		</div>		
		-->
		
		<div class="col-sm-4 col-md-4 certClass dOENumberDiv" >
		
		<c:choose>
 			<c:when test="${districtMaster.districtId eq 806810 }">
 				<c:choose>
       				<c:when test="${jobOrder.jobCategoryMaster.jobCategoryName eq 'Teacher' ||  jobOrder.jobCategoryMaster.jobCategoryName eq 'Administrator'}">
         		 		<label class="show doeShow"><strong><spring:message code="lblDOENum1" /></strong></label>
       			</c:when>
       			<c:otherwise>
        			<label class="show doeShow"><strong><spring:message code="lblDOENum" /><c:if test="${jobOrder.jobCategoryMaster.jobCategoryName eq 'Professional Certified'}"><span class="required">*</span></c:if></strong></label>
     			</c:otherwise>
		     	</c:choose>
     	</c:when>
     	<c:when test="${districtMaster.districtId eq 804800 }">
     		<label class="show doeShow"><strong>CDE Number<span class="required hide doeReq">*</span> <a href="#" id="iconpophoverDSPQCDE" rel="tooltip" data-original-title="CDE: Colorado Dept of Education."><img src="images/qua-icon.png" width="15" height="15" alt=""></a></strong></label>
			<script>
				$('#iconpophoverDSPQCDE').tooltip();
			</script>
     	</c:when>
     	<c:otherwise>
     			<a href="#" id="iconpophoverDSPQCDE" rel="tooltip"></a>
        			<label class="show doeShow"><strong><spring:message code="lblDOENum" /><c:if test="${jobOrder.jobCategoryMaster.jobCategoryName eq 'Professional Certified'}"><span class="required">*</span></c:if></strong></label>
     			</c:otherwise>
  	 	</c:choose>
  	 
			<label class="hide ppdiShow"><strong><spring:message code="lblPPID" /> <a href="#" id="iconpophoverppdi" rel="tooltip" data-original-title='<spring:message code="msgDp_commons34" />'><img src="images/qua-icon.png" width="15" height="15" alt=""></a></strong></label>
			<input type="text" class="form-control" name="doenumber" id="doenumber" maxlength="25" value="" />
		</div>
		
		<div class="col-sm-7 col-md-7 certClass cluDiv">
				<label><strong><spring:message code="lblCerti/LiceUrl" /> <a href="#" id="iconpophoverCertification" rel="tooltip" data-original-title="If you are including a URL, please ensure the URL links directly to a view of your credentials and not a page that requires a log in. If a log in is required, the district will not be able to access your information."><img src="images/qua-icon.png" width="15" height="15" alt=""></a>		 
			</strong>
						
				</label>
			
				<input  type="text" 
					class="form-control"
					maxlength="500"
					id="certUrl" 
					value=""
					name="certUrl" />
				
			</div>
			
		</div>
		<div class="row" id="certiExpirationDiv">
			<div class="col-sm-12 col-md-12">
				<label><strong>Certificate Expiration</strong></label>
				<input type="text" id="certiExpiration" class="form-control" name="certiExpiration"/>
			</div>
		</div>
		<div class="row certClass gradeLvl">	
			<div class="col-sm-12 col-md-12">
			<label><strong><spring:message code="lblGradeLvl" />
				<c:if test="${jobOrder.jobCategoryMaster.jobCategoryName eq 'Professional Certified'}">
					<span class="required">*</span>
				</c:if>
				 <%if(Utility.getValueOfPropByKey("nccheckforjspandjs").equalsIgnoreCase("yes")){%>
				    	<span id="gradelevelreq" class="required hide">*</span>
			<%}%>
				</strong>
			</label>
			</div>
			</div>
			<div class="row col-sm-12 col-md-12 certClass gradeLvl">	
			<div class="divwidth">
							<label class="checkbox inline">
							<input type="checkbox" value="1" id="pkOffered" name="pkOffered" >
							<spring:message code="lblPK" /> 
							</label>
			</div>	
			<div class="divwidth">			
							<label class="checkbox inline">
							<input type="checkbox" value="1" id="kgOffered" name="kgOffered" >
							<spring:message code="lblKG" />
							</label>
			</div>	
			<div class="divwidth">	
							<label class="checkbox inline">
							<input type="checkbox" value="1" id="g01Offered" name="g01Offered" >
							1 
							</label>
			</div>	
			<div class="divwidth">					
							<label class="checkbox inline">
							<input type="checkbox" value="1" id="g02Offered" name="g02Offered" >
							2 
							</label>
			</div>	
			<div class="divwidth">					
							<label class="checkbox inline">
							<input type="checkbox" value="1" id="g03Offered" name="g03Offered" >
							3 
							</label>
			</div>	
			<div class="divwidth">					
							<label class="checkbox inline">
							<input type="checkbox" value="1" id="g04Offered" name="g04Offered" >
							4 
							</label>
			</div>	
			<div class="divwidth">					
							<label class="checkbox inline">
							<input type="checkbox" value="1" id="g05Offered" name="g05Offered" >
							5 
							</label>
			</div>	
			<div class="divwidth">					
							<label class="checkbox inline">
							<input type="checkbox" value="1" id="g06Offered" name="g06Offered" >
							6 
							</label>
			</div>	
			<div class="divwidth">					
							<label class="checkbox inline">
							<input type="checkbox" value="1" id="g07Offered" name="g07Offered" >
							7 
							</label>
			</div>	
			<div class="divwidth">					
							<label class="checkbox inline">
							<input type="checkbox" value="1" id="g08Offered" name="g08Offered" >
							8 
							</label>
			</div>	
			<div class="divwidth">					
							<label class="checkbox inline">
							<input type="checkbox" value="1" id="g09Offered" name="g09Offered" >
							9 
							</label>
			</div>	
			<div class="divwidth">					
							<label class="checkbox inline">
							<input type="checkbox" value="1" id="g10Offered" name="g10Offered" >
							10 
							</label>
			</div>	
			<div class="divwidth">					
							<label class="checkbox inline">
							<input type="checkbox" value="1" id="g11Offered" name="g11Offered" >
							11 
							</label>
			</div>	
			<div class="divwidth">					
							<label class="checkbox inline">
							<input type="checkbox" value="1" id="g12Offered" name="g12Offered" >
							12 
							</label>
							
			</div>
	     </div>
         
         
         
         
         
	         <div id='praxisArea' class="row certClass" >
	         <div class="col-sm-2 col-md-2">
	         <label ><strong><spring:message code="lblReqPrxITests" /></strong></label>
	         </div>
	         <div class="col-sm-3 col-md-3" >
	         <label ><strong id='reading'></strong></label>
	         <div class="row">
			 <div class="col-sm-8 col-md-8">
			 <label ><strong ><spring:message code="lblYurScore" /> <input type='text' maxlength="3" id='readingQualifyingScore' class="form-control" onkeypress="return checkForInt(event)"></strong></label>
			 </div>
			 </div>
	         </div>
	         <div class="col-sm-3 col-md-3">
	         <label ><strong id='writing'></strong></label>
	          <div class="row">
			 <div class="col-sm-8 col-md-8">
			 <label ><strong ><spring:message code="lblYurScore" /> <input type='text' maxlength="3" id='writingQualifyingScore' class="form-control" onkeypress="return checkForInt(event)"></strong></label>
			 </div>
			 </div>
	         </div>
	         <div class="col-sm-3 col-md-3" >
	         <label ><strong id='maths'></strong></label>
	         <div class="row">
			 <div class="col-sm-8 col-md-8">
			 <label ><strong ><spring:message code="lblYurScore" /> <input type='text' maxlength="3" id='mathematicsQualifyingScore' class="form-control" onkeypress="return checkForInt(event)"></strong></label>
			 </div>
			 </div>
	         </div>
	         
	         </div>
	    
	    <div class="row certClass certSrc">
	   		<div class="col-sm-3 col-md-3" id='ieinNumberDiv'>
	    			<label><spring:message code="lblIEINNumber" /><span class="required" style="color: red;">*</span><a href="#" id="iconpophoveriein" rel="tooltip" data-original-title="If you do not have an IEIN number, indicate n/a"><img src="images/qua-icon.png" width="15" height="15" alt=""></a></label>
	    			<input type="text" id="IEINNumber" name="IEINNumber" maxlength="50" class="form-control" >
	    	</div>
	    	
	    	<div class="col-sm-4 col-md-4">
	    		<c:choose>
 				<c:when test="${districtMaster.districtId eq 806810 }">
 				<c:choose>
       				<c:when test="${jobOrder.jobCategoryMaster.jobCategoryName eq 'Teacher' ||  jobOrder.jobCategoryMaster.jobCategoryName eq 'Administrator'}">
         		 		<label class="doeShow">
         		 		<spring:message code="msgCLLetter1" /><a href="#" id="proofCertTooltip" rel="tooltip" data-original-title="Teacher, ESA, or CTE Cert"><img src="images/qua-icon.png" width="15" height="15" alt=""></a>
				</label>
       			</c:when>
       			<c:otherwise>
        			<label class="doeShow">
        			<spring:message code="msgCLLetter" /><a href="#" id="proofCertTooltip" rel="tooltip" data-original-title="Teacher, ESA, or CTE Cert"><img src="images/qua-icon.png" width="15" height="15" alt=""></a>
				</label>
     			</c:otherwise>
		     	</c:choose>
     			</c:when>
     	     	<c:otherwise>
        			<label class="doeShow">
        			<spring:message code="msgCLLetter" /><span class="required" id="licenseceasterik">*</span><a href="#" id="proofCertTooltip" rel="tooltip" data-original-title="Teacher, ESA, or CTE Cert"><img src="images/qua-icon.png" width="15" height="15" alt=""></a>
				</label>
     			</c:otherwise>
  	 	</c:choose>
	    		
				<label class="hide ppdiShow">
					<spring:message code="msgDp_commons35" />
				</label>
				<input type="hidden" id="sbtsource_cert" name="sbtsource_cert" value="0"/>
				<input id="pathOfCertificationFile" name="pathOfCertificationFile" type="file">
				<a href="javascript:void(0)" id="clearLink" onclick="clearCertification()">Clear</a>
	    	</div>
					
		<input type=hidden id="pathOfCertification" value=""/>
			
			    <div class="col-sm-5 col-md-5">
				<span  id="removeCert" name="removeCert" style="display: none;">
					<label>
						&nbsp;&nbsp;
					</label>
					<span id="divCertName"></span>
					<a href="javascript:void(0)" onclick="removeCertificationFile()"><spring:message code="lnkRemo" /></a>&nbsp;
					<a href="#" id="iconpophover6" rel="tooltip" data-original-title="Remove certification/Licence file !">
					<img src="images/qua-icon.png" width="15" height="15" alt=""></a>
				</span>
				</div>
			</div>
			<div class="row certText">
				<div class="col-sm-9 col-md-9 top10">
				<labe><spring:message code="lblExpOfCurrPendCert" />&nbsp;<a href="javascript:void(0)" class="calculatortooltip" rel="tooltip" data-original-title='<spring:message code="msgDp_commons36" />'><img src="images/qua-icon.png" width="15" height="15" alt=""></a></label>
					<textarea id="certTextvalue" rows="" cols=""></textarea>
				</div>
			</div>			
 		</form>
 		<div class="row">								
	    <div class="col-sm-3 col-md-3 idone">
			<a id="hrefDone" href="#" style="cursor: pointer; margin-left: 0px; text-decoration:none;" onclick="return insertOrUpdate_Certification(0);" ><spring:message code="lnkImD" /></a>&nbsp;&nbsp;
			<a class="idone" style="cursor: pointer; margin-left: 0px; text-decoration:none;"	onclick="return hideForm_Certification()">
				<spring:message code="lnkCancel" />
			</a>
		</div>
		</div>
	</div>
</div>		
	<!-- End :: Certifications Body -->
<div class=" portfolio_Section_Gap" id="nationalBoardCertDiv" >
         <div class="row">
	   		<div class="col-sm12 col-md-12">
	   			<div class='divErrorMsg' style="display: none;"id="errNBCY"></div>
	   		</div>
	    	<div class="col-sm12 col-md-12">
	    	<div style="float: left" class="portfolio_Subheading">
	    			
	    			<%if(Utility.getValueOfPropByKey("nccheckforjspandjs").equalsIgnoreCase("yes")){%>					 
					<strong><spring:message code="lblNatiBoardCerti/Lice" /><span class="required">*</span> 
					<a href="#" id="iconpophover11" rel="tooltip" data-original-title="Select Yes if you have a National Board Certification."><img src="images/qua-icon.png" width="15" height="15" alt=""></a></strong>
					<%}
					else
					{
					%>	    			
	    			<c:choose>
					<c:when test="${districtMaster.districtId == 806810 && (jobOrder.jobCategoryMaster.jobCategoryName eq 'Teacher' || jobOrder.jobCategoryMaster.jobCategoryName eq 'Administrator')}">
					<strong><spring:message code="lblNatiBoardCerti" /><a href="#" id="iconpophover11" rel="tooltip" data-original-title='<spring:message code="msgYesNBC" />'><img src="images/qua-icon.png" width="15" height="15" alt=""></a></strong>
					</c:when>								    
					<c:otherwise>
					<strong><spring:message code="lblNatiBoardCerti/Lice" /><span class="required">*</span>					
					 <a href="#" id="iconpophover11" rel="tooltip" data-original-title='<spring:message code="msgYesNBC" />'><img src="images/qua-icon.png" width="15" height="15" alt=""></a>
					 </strong>
					</c:otherwise>
					</c:choose>
					<%}%>
				</div>
	    		
	    	</div>	
	    </div>		   	          
	       <%if(Utility.getValueOfPropByKey("nccheckforjspandjs").equalsIgnoreCase("yes")){%>	                                
 				<div class="col-sm-12 col-md-12" style="margin-left: -15px;">
 				Select Yes if you have a National Board Certification.
 				</div>
 	   	<%}%>
	       <div id="nbptsRadio" class="row left1 show">			
	        <table>
		        <tr >
		         <td class="radio inline ">
		         <input type="radio" value="nbc1" id="nbc1" name="nationalBoardCert" <c:if test="${teacherExperience.nationalBoardCert}">checked</c:if> onclick="chkNBCert('1')">		      
		        </td>
		        <td style="padding-left:5px;">
		        <div style=""><spring:message code="lblYes" /></div>
		        </td>
		        <td style="width: 40px;"></td>
		        <td class="radio inline">
		         <input type="radio" value="nbc2" id="nbc2" name="nationalBoardCert" <c:if test="${teacherExperience.nationalBoardCert eq false or teacherExperience.nationalBoardCert eq null}">checked</c:if>   onclick="chkNBCert('2')">
		        </td>
		        <td style="padding-left:5px;">
		         <div style=""><spring:message code="lblNo" /></div>		       
		        </td>
		        </tr>
		        		       
		     </table>
		  </div>
		  <!-- ankit -->
		 <div id="licenseDivNBPTS" class="hide" >	
	       <div class="row portfolio_Section">
			<div class="col-sm-12 col-md-12">									
			<div style="clear: both;"></div>	
		    </div>		   
	</div>
	<div class="row" >
	    <div class="col-sm-12 col-md-12">
			<div id="nbcstatus"></div>
		</div>
    	</div>
	</div>	
		  		  	
		  <div class="row top15">		
		  <div class="col-sm-12 col-md-12">
			<div class='hide' id="divNbdCert" name="divNbdCert"  <c:choose>  <c:when test="${teacherExperience.nationalBoardCert}">style='display: block;'</c:when><c:otherwise>'</c:otherwise></c:choose> />
			<div class="col-sm-4">
			<c:if test="${districtMaster.districtId == 806810 && (jobOrder.jobCategoryMaster.jobCategoryName eq 'Teacher' || jobOrder.jobCategoryMaster.jobCategoryName eq 'Administrator' || jobOrder.jobCategoryMaster.jobCategoryName eq 'Substitute')}">
			<label><spring:message code="lblYearObtained" /></label>
			</c:if>
				<select id="nationalBoardCertYear" name="nationalBoardCertYear"  class="form-control" style="max-width: 230px;" >
					<option value="" id="psl"><spring:message code="optSlt" /></option>
					<c:forEach var="year" items="${lstLastYear}">	
						<c:set var="selected" value=""></c:set>					
		        		<c:if test="${year eq teacherExperience.nationalBoardCertYear}">	        			
		         			<c:set var="selected" value="selected"></c:set>	         			
		        		</c:if>							
						<option id="nationalBoardCertYear${year}" value="${year}" ${selected} >${year }</option>
					</c:forEach>					
				</select>
				</div>
				<c:if test="${districtMaster.districtId == 806810 && (jobOrder.jobCategoryMaster.jobCategoryName eq 'Teacher' || jobOrder.jobCategoryMaster.jobCategoryName eq 'Administrator' || jobOrder.jobCategoryMaster.jobCategoryName eq 'Substitute')}">
				<div class="col-sm-4">
				<label>National Board Certification Subject question</label>
				<select id="questionmultipleoptio" name="questionmultipleoptio" multiple  class="form-control" >
				<c:forEach var="customquetion" items="${customQuestionMultipleOption}">
				<c:set var="selected" value=""></c:set>	
				<c:forEach var="custonQuestion" items="${customQuestionId}">
				<c:if test="${custonQuestion eq customquetion.customquestionid}">
				<c:set var="selected" value="selected"></c:set>	
				</c:if>
				</c:forEach>
				<option value="${customquetion.customquestionid}" ${selected}>${customquetion.question}</option>
				</c:forEach>
				 </select>
			</div>
			</c:if>
				</div>	
			</div>				
    	</div>
   </div> 

<iframe id='multifileuploadformiframe' name='multifileuploadformTarget' target='uploadFrameResume'  height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'></iframe>
<div id="generalKnowledgeDiv">
<!-- Start :: Pass/Fail General Knowledge Exam Body -->
<form:form method="post" action="multifileupload.do" id='multifileuploadform' target='multifileuploadformTarget'  modelAttribute="uploadForm" enctype="multipart/form-data">	
<input type="hidden" id="uploadFileNumber" name="uploadFileNumber" value="1"/>
	
	<div class="row portfolio_Section_Gap">
		<div class="col-sm-8 col-md-8">
			<div class="span6 portfolio_Subheading headingGkE"><spring:message code="lblPFGKE" /></div>
		</div>
	</div>
	<div class="row">
	<div class="col-sm-8 col-md-8">
		<div class='divErrorMsg' style="display: none;" id="errGeneralKnowledge"></div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-3 col-md-3">
			<label><spring:message code="lblStatus" /><span class="required">*</span></label>
			<select class="form-control" id="generalKnowledgeExamStatus" name="generalKnowledgeExamStatus">
				<option value="0"><spring:message code="sltStatis" /></option>   
				<option value="P"><spring:message code="optPass" /></option>
				<option value="F"><spring:message code="optFail" /></option>
			</select>
		</div>
		<div class="col-sm-3 col-md-3">
			<label><spring:message code="lblExamDate" /><span class="required">*</span></label>
			<input type="text" id="generalKnowledgeExamDate" name="generalKnowledgeExamDate"   maxlength="0" class="form-control">
		</div>
		<div class="col-sm-3 col-md-3">
			<label><spring:message code="lblScorereport" /><span class="required">*</span></label>
					<input type="hidden" id="generalKnowledgeScoreReportHidden" name="generalKnowledgeScoreReportHidden" value=""/>
					<input id="generalKnowledgeScoreReport" name="files[0]" type="file" width="20px;">
					<a href="javascript:void(0)" onclick="clearGeneralKnowledgeScoreReport()"><spring:message code="lnkClear" /></a>
		
		</div>
		<div class="col-sm-3 col-md-3">		
				<span class="span4" id="removeGeneralKnowledgeScoreReportSpan" name="removeGeneralKnowledgeScoreReportSpan" style="display: none;">
					<!--<label>
						&nbsp;&nbsp;
					</label>
					--><span id="divGeneralKnowledgeScoreReport">
					</span>
					<!-- <a href="javascript:void(0)" onclick="removeGeneralKnowledgeScoreReport()">Remove</a>&nbsp;
						<a href="#" id="iconpophover7" rel="tooltip" data-original-title="Remove General Knowledge Score Report"><img src="images/qua-icon.png" width="15" height="15" alt=""></a> -->
				</span>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6 col-md-6 top10 hide" id="generalExamNote">
		<textarea name="generalExamNote"></textarea>		
		</div>
	</div>
</form:form>
</div>
<!-- End :: Pass/Fail General Knowledge Exam Body -->
	
<!-- Start :: Pass/Fail Subject Area Exam Body -->
	
<div id="subjectAreaDiv">		
	<div class="row portfolio_Section_Gap">
			<div class="col-sm-12 col-md-12">				
				<div style="float: left" class="portfolio_Subheading headingSAE">
					<spring:message code="lblPFSAE" />
				</div>
				<div style="float: right" class="addPortfolio">
					<a href="#" onclick="return showSubjectAreaForm()" id="addLinkSAE"><spring:message code="lnkAddFassFailSubArea" /> </a>
				</div>
				<div style="clear: both;"></div>				
			</div>
		</div>
	
	
	<div class="row" onmouseover="setGridNameFlag('subjectAreas')">
	    <div class="col-sm-9 col-md-9">
		<div id="divGridSubjectAreas"></div>
		</div>
	</div>
	
<iframe id='uploadFrameSubjectArea' name='uploadFrameSubjectArea' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'></iframe>
<form id="frmSubjectArea" name="frmSubjectArea" enctype='multipart/form-data' method='post' target='uploadFrameSubjectArea' action='teacherSubjectAreaServlet.do' >

	<div class="mt30" style="display: none" id="divSubjectAreaInput">
		
		<div class="row">
		<input type="hidden" id="teacherSubjectAreaExamId" value="0">
		<input type="hidden" id="sbtsource_subArea" name="sbtsource_subArea" value="0"/>
		 <div class="col-sm-12 col-md-12">
		<div class='divErrorMsg' style="display: none;" id="errSubjectArea"></div>
		</div>
		
		<div class="col-sm-3 col-md-3">
			<label><spring:message code="lblStatus" /><span class="required">*</span></label>
			<select class="form-control" id="examStatus" name="examStatus">
				<option value="0"><spring:message code="sltStatis" /></option>   
				<option value="P"><spring:message code="optPass" /></option>
				<option value="F"><spring:message code="optFail" /></option>
			</select>
		</div>
		<div class="col-sm-3 col-md-3">
			<label><spring:message code="lblExamDate" /><span class="required">*</span></label>
			<input type="text" id="examDate" name="examDate"   maxlength="0" class="form-control">
		</div>
		<div class="col-sm-3 col-md-3" >
			<label><spring:message code="lblSub" /><span class="required">*</span></label>
			<div id="subjectShowDiv">
				<select class="form-control" id="subjectIdforDSPQ" name="subjectIdforDSPQ">
					<option value="0"><spring:message code="optStlSub" /></option>   
					 <c:if test="${not empty subjectList}">
					 	<c:set var="subEditId" ></c:set>
				        <c:forEach var="subjectList" items="${subjectList}" varStatus="status">
				        	<!--<c:choose>
							    <c:when test="${teacherSubjectAreaExam.subjectAreaExamMaster.subjectAreaExamId eq subjectList.subjectAreaExamId}">
							    	<c:set var="subEditId" value="selected"></c:set>
							    </c:when>
							    <c:otherwise>
							    	<c:set var="subEditId" ></c:set>
							    </c:otherwise>              
							</c:choose>-->
				        	<option value="${subjectList.subjectAreaExamId}" ${subEditId}><c:out value="${subjectList.subjectAreaExamName}" /></option>
						</c:forEach>
					</c:if>	
				</select>
			</div>
		</div>
		</div>
		<div class="row top10">
			<div class="col-sm-6 col-md-6 hide" id="subjectExamTextarea">
				<label><spring:message code="headNot" /></label>
				<textarea name="subjectExamTextarea"></textarea>
			</div>	
		<div class="col-sm-3 col-md-3">
			<label><spring:message code="lblScorereport" /><span class="required">*</span></label>
					<input type="hidden" id="scoreReportHidden" name="scoreReportHidden" value=""/>
					<input id="scoreReport" name="files[1]" type="file" width="20px;">
					<a href="javascript:void(0)" onclick="clearScoreReport()"><spring:message code="lnkClear" /></a>
		</div>
		
		<div class="col-sm-3 col-md-3">		
				<span  id="removeScoreReportSpan" name="removeScoreReportSpan" style="display: none;">
					<!--<label>
						&nbsp;&nbsp;
					</label>
					--><span id="divScoreReport">
					</span>
					<!-- <a href="javascript:void(0)" onclick="removeScoreReport()">Remove</a>&nbsp;
						<a href="#" id="iconpophover7" rel="tooltip" data-original-title="Remove Score Report"><img src="images/qua-icon.png" width="15" height="15" alt=""></a>-->
				</span>
		</div>
		</div>
		<div class="row">
		<div class="col-sm-12 col-md-12 idone" >
			<a id="hrefDone" href="#" style="cursor: pointer; margin-left: 0px; text-decoration:none;" onclick="return saveSubjectAreas(0)" ><spring:message code="lnkImD" /></a>&nbsp;&nbsp;
			<a style="cursor: pointer; margin-left: 0px; text-decoration:none;"	onclick="return cancelSubjectAreaForm()">
				<spring:message code="btnClr" />
			</a>
		</div>
		
	</div>
	
	</div>
	
</form>	
	
</div>

<!-- Involvement/Volunteer Work -->
<div clas="hide" id="involvementDiv">
	<div class="row portfolio_Section_Gap">	
		<div class="col-sm-12 col-md-12">		
			<div style="float: left" class="portfolio_Subheading">
				<spring:message code="lnkInvol/VolunWork/StuTch" />
			</div>
			<div style="float: right" class="addPortfolio">
					<a href="#" id="addInvolvemetIdDSPQ" onclick="return showInvolvementForm()" ><spring:message code="lnkAddInvol" /></a>
				</div>
		</div>
	</div>

	<div class="row" onmouseover="setGridNameFlag('involvement')">
		<div class="col-sm-12 col-md-12">
			<div id="divDataInvolvement" class="span15"></div>
		</div>
	</div>
	
						<div class="mt30" style="display: none" id="divInvolvement">
										<div class="row">
										<div class="col-sm-10 col-md-10">
											<p><spring:message code="pListOrganiz" /></p>
										</div>										
										<div class="col-sm-10 col-md-10">
										<div class='divErrorMsg' id='errordivInvolvement' style="display: block;"></div>
										</div>
										</div>
										<form id="frmInvolvement" name="frmInvolvement">
											<input type="hidden" id="involvementId"name="involvementId"	/>
											<div class="row">												
												<div class="col-sm-3 col-md-3"><label><spring:message code="lblOrga" /></label>
													<input type="text" id="organizationInv" name="organizationInv" maxlength="50" class="form-control" >
												</div>
												
												<div class="col-sm-3 col-md-3" id="orgTypDivInv"><label>Type of Organization</label>
													<select class="form-control " id="orgTypeId" name="orgTypeId">
														<option value=""><spring:message code="optSlt" /></option>
														<c:forEach items="${lstOrgTypeMasters}" var="orgType">
															<option value="${orgType.orgTypeId}">${orgType.orgType}</option>
														</c:forEach>
													</select>
												</div>
																							
												<div class="col-sm-4 col-md-4" id="rangeIdspq"><label>Number of people in this Organization</label>
													<select class="form-control " id="rangeId" name="rangeId">
														<option value="">Select</option>
														<c:forEach items="${lstPeopleRangeMasters}" var="peopleRange">
															<option value="${peopleRange.rangeId}">${peopleRange.range}</option>
														</c:forEach>
													</select>
												</div>
												
											</div>	
											<div class="row top5" id="leadPeoDspq">
											        <div class="col-sm-4 col-md-4">
											         <label><spring:message code="lblLeadPeopleInThisOrga" /></label>
											       													    
												     <div class="" id="" style="height: 40px;">
												    	<div class="radio inline col-sm-1 col-md-1" style="margin-top:0px;">
													    	<input type="radio"  id="rdo1" value="option1"  name="optionsRadios" onclick="showAndHideLeadNoOfPeople('1')"> <spring:message code="lblYes" />
													    </div>
													    </br>
													    <div class="radio inline col-sm-1 col-md-1" style="margin-left:20px;margin-top:-20px;">
													    	<input type="radio" checked="t" value="option1"  name="optionsRadios" onclick="showAndHideLeadNoOfPeople('0')"> <spring:message code="lblNo" />
													    </div>
												     </div>								    
											          </div>
											    <div class="col-sm-2 col-md-2" id="divLeadNoOfPeople" name="divLeadNoOfPeople" style="display: none;"><label><spring:message code="lblHowMnyPeople" /></label>
													<input type="text" id="leadNoOfPeople" name="leadNoOfPeople" class="form-control" onkeypress="return checkForInt(event);" maxlength="4"/>
												</div>
										    </div>	
										    <div class="row">	
											    <div class="col-sm-6 col-md-6 idone">
											    	<a href="#" onclick="return saveOrUpdateInvolvement()"><spring:message code="lnkImD" /></a>&nbsp;
											    	<a href="#"	onclick="return hideInvolvement()">
														<spring:message code="btnClr" />
													</a>
											    </div>
											</div>	
										</form>
										</div>
</div>
	<!-- End ::  Involvement/Volunteer Work -->
	
	<!-- Honors div -->
	
	<div clas="hide" id="honorsDiv">
	<div class="row portfolio_Section_Gap">	
		<div class="col-sm-12 col-md-12">		
			<div style="float: left" class="portfolio_Subheading">
				<spring:message code="lblHonors" />
			</div>
			<div style="float: right" class="addPortfolio">
					<a href="#" id="addHonorsIdDSPQ" onclick="return showHonorForm()" ><spring:message code="lnkAddHonors" /></a>
				</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-10 col-md-10" id="divDataHoner"></div>
	</div>
	
	
	<form id="frmHonor" name="frmHonor" onsubmit="return false;">
									    <input type="hidden" id="honorId" name="honorId"/>
									    <div  id="divHonor" style="display: none;">
										    <br>
										    <div class="row col-sm-10 col-md-10">
										    <div class='divErrorMsg' id='errordivHonor'  style="display: block;"></div>
										    </div>	
										    
										    <div class="row ">								    
										    <div class="col-sm-4 col-md-4 rowmargin1"><label><spring:message code="lblAward" /></label>
										    	<input type="text" id="honor" name="honor" class="form-control" placeholder="" maxlength="50">
										    </div>
										    <div class="col-sm-2 col-md-2"><label>&nbsp;</label>
												<select id="honorYear" name="honorYear" class="form-control">
													<option value="0" ><spring:message code="optYear" /></option>														
													<c:forEach items="${lstYear}" var="year">
														<option>${year}</option>
													</c:forEach>
												</select>
											</div>
											</div>								
											<div class="row col-sm-4 col-md-4 idone top10 rowmargin">
												<a href="#" onclick="return saveOrUpdateHonors()"><spring:message code="lnkImD" /></a>
												&nbsp;<a href="#"	onclick="return hideHonor()">
													<spring:message code="btnClr" />
												</a>
											</div>
									    </div>
									    
									    </form>
	</div>
	<!--  End :: Honorsd -->

	
	<%-- start ::  Employment section  --%>

<div id="employmentDiv">		
							
                   		   <div class="row portfolio_Section_Gap">
								<div class="col-sm-12 col-md-12">				
									<div style="float: left" class="portfolio_Subheading" >
										<spring:message code="lblEmplHis" /><span class="required hide" id="reqEmpAstrick">*</span>
									</div>
									<div style="float: right" class="addPortfolio">
									    <input type="hidden" id="sbtsource_emp" name="sbtsource_emp" value="0"/>
										<a href="javascript:void(0);" id="addEmploymentIdDSPQ"  onclick="return showEmploymentForm();"><spring:message code="lnkAddEmp" /></a>
									</div>
									<div style="clear: both;"></div>	
									<div class="col-sm-12 col-md-12 hide" id="empSecText"></div>			
								</div>
							</div>						
							
                    		<div class="row" onmouseover="setGridNameFlag('workExperience')">
									 <div class="col-sm-9 col-md-9">
	                    					<div id="divDataGridEmployment"></div>
	                    			</div>	
                    		</div>
                    				                      	
				<div class="top20" style="display: none" id="divEmployment">
					<div class="row">					
						<div class="col-sm-12 col-md-12">
						<spring:message code="msgEmloymentRoles" />				
						</div>
						<div class="col-sm-12 col-md-12">
						<div class='divErrorMsg' id='errordivEmployment' style="display: block;"></div>
						</div>			
					</div>
					
					<div class="row">
                        <div class="col-sm-3 col-md-3">
                            <label class="checkbox">
                            <input type="checkbox" id="idonothaveworkexp" name="idonothaveworkexp" onclick="hideworkexp()"> I do not have any work experience.
                            </label>
                        </div>
                    </div>
                        
                        	
					<form class="" id="frmEmployment" name="frmEmployment" onsubmit="return false;">
						<input type="hidden" id="roleId" name="roleId">
						<div class="row">
												
                          <div class="col-sm-3 col-md-3" id="empHisPos">
                                <label>
                                <spring:message code="lblPosition" /><span class="required hide" id="empPostionAstrik">*</span>
                                </label>
                                <select class="form-control hideWorkExp" id="empPosition">
                                <option value="0"><spring:message code="optSlt" /></option>
								<option value="1"><spring:message code="lblStudentTeaching" /></option>
								<option value="2"><spring:message code="lblFullTimeTeaching" /></option>
								<option value="3"><spring:message code="lblSubstituteTeaching" /></option>
								<option value="4"><spring:message code="lblOtherWorkExperience" /></option>
								<c:if test="${districtMaster ne null && districtMaster.districtId == 806810}">
								<option value="5"><spring:message code="lblPartTimeTeaching" /></option>
								</c:if>
                                </select>
                            </div>
                        
							<div class="col-sm-4 col-md-4">
								<label><spring:message code="lblTitDesigRol" /><span class="required">*</span></label>
								<input type="text" id="role" name="role" class="form-control hideWorkExp" placeholder="" maxlength="50">
							</div>
								<div class="col-sm-5 col-md-5" id="ndustry_FieldDspq">
								<label>Industry/Field<span class="required">*</span></label>
								<!--<input type="text" class="span4" placeholder="">-->
								<select class="form-control hideWorkExp" id="fieldId2" name="fieldId">
									<option value=""><spring:message code="optSlt" /></option>													
									<c:forEach items="${lstFieldMaster}" var="fieldMaster">
										<option  value="${fieldMaster.fieldId}">${fieldMaster.fieldName}</option>
									</c:forEach>
								</select>
							</div>
						</div>
						<!-- shriram label name by shriram -->
                        <div class="row">
                                <div class="col-sm-3 col-md-3"><label id="labelnameOforg"><spring:message code="lblNameofOrganization" /><span class="required hide" id="empOrganizationAstrik">*</span></label>
                                         <input type="text" id="empOrg" class="form-control hideWorkExp" name="empOrg"/>
                                </div>
                        
                        
								<div class="col-sm-4 col-md-4"><label id="labelnameofcity"><spring:message code="lblCity" /><span class="required hide" id="empCityAstrik">*</span></label>
									<input type="text" id="cityEmp" class="form-control hideWorkExp" name="cityEmp"/>
								</div>
								<div class="col-sm-5 col-md-5"><label id="labelnameofstate"><spring:message code="lblStateOfOrganization" /><span class="required hide" id="empStateOrganizationAstrik">*</span></label>
									<input type="text" id="stateOfOrg" class="form-control hideWorkExp" name="stateOfOrg"/>
								</div>
						</div>	
						
						<!-- shriram 7th October 2015 -->
							<div class="row hide" id="compTeleSup">
								<div class="col-sm-3 col-md-3">
									<label>
										<spring:message code="lblCompanyTelephone" />
										<span class="required">*</span>
									</label>
									<input type="text" id="compTelephone" class="form-control hideWorkExp"
										name="compTelephone" />
								</div>
								<div class="col-sm-4 col-md-4">
									<label>
										<spring:message code="lblCompanySupervisor" />
										<span class="required">*</span>
									</label>
									<input type="text" id="compSupervisor" class="form-control hideWorkExp"
										name="compSupervisor" />
								</div>

							</div>
							<!-- End by shriram -->
						<div class="row top15" id="durationReq">
                            <div class="col-sm-3 col-md-3">
                                <label><spring:message code="lblDuration" /><span class="required">*</span></label>
                            </div>
                        </div>  
                        <div class="row">
                            <div class="col-sm-3 col-md-3">
                                <label class="checkbox">
                                <input type="checkbox" class="hideWorkExp" id="currentlyWorking" name="currentlyWorking" onclick="hideToExp()"> <spring:message code="lblICurrWrkHr" />
                                </label>
                            </div>
                        </div>
						<div class="row" id="empDateDiv">
							<div id="divFromMonth" class="col-sm-2 col-md-2"><label><spring:message code="lblFrm" /><span class="required">*</span></label>
								<select id="roleStartMonth" name="roleStartMonth" class="form-control hideWorkExp">
									<option value="0"><spring:message code="optStrMonth" /></option>
									<c:forEach items="${lstMonth}" var="month" varStatus="status">
										<option value="${status.count}">${month}</option>
									</c:forEach>
									
								</select>
							</div>
							<div class="col-sm-2 col-md-2">
								<label>&nbsp;</label>
								<select id="roleStartYear" name="roleStartYear" class="form-control hideWorkExp">
									<option value="0" ><spring:message code="optYear" /></option>														
									<c:forEach items="${lstYear}" var="year" >
										<option  value="${year}">${year}</option>
									</c:forEach>
								</select>
							</div>
							<div id="divToMonth" class="col-sm-2 col-md-2 "><label>To<span class="required">*</span></label>
								<select   class="form-control hideWorkExp" id="roleEndMonth" name="roleEndMonth">
									<option value="0"><spring:message code="optStrMonth" /></option>
									<c:forEach items="${lstMonth}" var="month"  varStatus="status">
										<option value="${status.count}">${month}</option>
									</c:forEach>
								</select>
							</div>
							<div id="divToYear" class="col-sm-2 col-md-2">
								<label>&nbsp;</label>
								<select id="roleEndYear" name="roleEndYear" class="form-control hideWorkExp">
									<option value="0"><spring:message code="optYear" /></option>
									<c:forEach items="${lstYear}" var="year">
										<option value="${year}">${year}</option>
									</c:forEach>
								</select>
							</div>
						</div>
						
						
						<div class="row">
							<div id="empHisAnnSal" class="col-sm-8 col-md-8"><label><spring:message code="lblAnnualSlryAmount" /><span class="required expsalaryInwork hide">*</span></label>
						    	<input type="text" style="width: 30%;" id="amount" name="amount" class="form-control hideWorkExp"   onkeypress="return checkForInt(event);" maxlength="8">
							</div>
						
						</div>
						
						<div class="row empHisTypRol">
							<div class="col-sm-3 col-md-3"><spring:message code="lblTypOfRole" /><span class="required philNT tORR">*</span>
							</div>
						</div>								
						<div class="row empHisTypRol" id="empRoleDspq">						   
						    	<c:forEach items="${lstEmpRoleTypeMaster}" var="empRoleType">														
									 <div class="col-sm-2 col-md-2">
	        		                    <label class="radio" style="margin-top: 0px;">
								    	<input type="radio" class="hideWorkExpOPt" value="${empRoleType.empRoleTypeId}"  name="empRoleTypeId" name="optionsRadios">${empRoleType.empRoleTypeName} 
								    	</label>					
					                 </div>	
								</c:forEach>						  
						</div>	
						 							
						<div class="row hide pritr"><div class="col-sm-8 col-md-8"><br><label><spring:message code="lblRespondibinThRole" /><span class="required philNT pritreq">*</span> <a href="#" id="prrtool" rel="tooltip" data-original-title='<spring:message code="msgDp_commons37" />'><img src="images/qua-icon.png" width="15" height="15" alt=""></a></label></div>
						</div>
						<div class="row hide pritr">
						    <div class="col-sm-8 col-md-8" id='primaryRespdiv'>						   
						    <textarea id="primaryResp" name="primaryResp" class="span12" rows="4"  maxlength="250" onkeyup="return chkLenOfTextArea(this,250);" onblur="return chkLenOfTextArea(this,250);"></textarea></div>
						</div>
						<div class="row hide mscitr">
							<div class="col-sm-4 col-md-4"><br><label><spring:message code="lblSignContriInThisRole" /><span class="required hide mscitrReq">*</span></label></div>
							</div>
						<div class="row hide mscitr">
						    <div class="col-sm-8 col-md-8" id='mostSignContdiv'>
						  
						    <textarea class="span12" id="mostSignCont" name="mostSignCont" rows="4"  maxlength="250" onkeyup="return chkLenOfTextArea(this,250);" onblur="return chkLenOfTextArea(this,250);"></textarea></div>						    
						</div>
						 
						 <div class="row reasonForLeavdiv">
							<div class="col-sm-3 col-md-3"><br><label><spring:message code="lblRsnforLeaving" /></label></div>
							</div>
						<div class="row reasonForLeavdiv">
						    <div class="col-sm-8 col-md-8" id='reasonForLeadiv'>
						  
						    <textarea class="span12" id="reasonForLea" name="reasonForLea" rows="4"  maxlength="250" onkeyup="return chkLenOfTextArea(this,250);" onblur="return chkLenOfTextArea(this,250);"></textarea></div>						    
						</div>
						 
						 <div class="row">	
						    <div class="col-sm-4 col-md-4 idone">
						    	<a href="#" onclick="return insertOrUpdateEmployment(0)"><spring:message code="lnkImD" /></a>&nbsp;
						    	<a href="#"	onclick="return hideEmploymentForm()">
									<spring:message code="btnClr" />
								</a>
						    </div>
						    </div>
													
					</form>
				</div>
		

</div>
<br>
<div id="troops" class="hide">
Current or prior military? Visit <a href="http://www.ncpublicschools.org/troops" target="_blank"><b>Troops to Teachers</b></a> 
</div>


<%-- End ::  Employment section  --%>

	
<!-- I was a student teacher in The School District start -->
        <div  class="top20 hide" id="divStdTchrExp">
              <div class="row">
               <div class="col-sm-12 col-md-12">
                <div class='divErrorMsg' id='errordiv_bottomPart_divstdTch' style="display: block;"></div>
                </div>
                <div class="col-sm12 col-md-12">
                    <div class="row">
                        <div class="col-sm1 col-md-1" style="width: 20px;">
                        <input type="checkbox" id="StuTchrChk" name="StuTchrChk" onclick="showStdTchrSgrid();">
                        </div>
                        <div class="col-sm10 col-md-10"><spring:message code="msgDp_commons38" />
                        </div>
                    </div>                     
                </div>
              </div>
              
              <div class="row hide stuTchrExpDiv">
                <div class="col-sm-12 col-md-12">
                    <div style="float: left" class="portfolio_Subheading">
                    <spring:message code="lblStudentTeachingExperience" />
                </div>
                <div style="float: right" class="addPortfolio">
                    <a href="#" onclick="return showStdExpTchrForm()" ><spring:message code="lnkAddExp" /></a>
                </div>
                <div style="clear: both;"></div>
                </div>
            </div> 
                <div class="row hide stuTchrExpDiv" onmouseover="setGridNameFlag('stdTchrGrid')">
                    <div class="col-sm-12 col-md-12">
                        <div id="divDatastdTchrGrid" class="span15"></div>
                    </div>
                </div>
                 
                 <div class="portfolio_Section_ImputFormGap hide" style="display: none" id="divstdTchExp" >
            <div class="row">
                <div class="col-sm-12 col-md-12">
                <div class='divErrorMsg' id='errordivstdTch' style="display: block;"></div>
                </div>
            </div>
            
            
            <div class="row">
                <div class="col-sm-6 col-md-6">
                    <label><spring:message code="lblSchoolName" /><span class="required">*</span></label>
                    <input type="text" id="schoolNameStdTch" name="schoolNameStdTch" class="form-control" placeholder="">
                </div>
                
                <div class="col-sm-3 col-md-3">
                    <label><spring:message code="lblSub" /><span class="required ">*</span></label>
                    <input type="text" id="subjectStdTch" name="subjectStdTch" class="form-control" placeholder="" maxlength="50">
                </div>
                    
            </div>          
            
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <label><strong><spring:message code="lblGradeLvl" /><span class="required ">*</span></strong></label>
                </div>
                        <div class="col-sm-12 col-md-12">
                            <div class="divwidth">
                            <label class="checkbox inline">
                            <input type="checkbox" value="1" id="pkOfferedStdTchr" name="pkOfferedStdTchr" >
                            <spring:message code="lblPK" /> 
                            </label>
                        </div>  
                        <div class="divwidth">          
                            <label class="checkbox inline">
                            <input type="checkbox" value="1" id="kgOfferedStdTchr" name="kgOfferedStdTchr" >
                            <spring:message code="lblKG" />
                            </label>
                        </div>  
                        <div class="divwidth">  
                            <label class="checkbox inline">
                            <input type="checkbox" value="1" id="g01OfferedStdTchr" name="g01OfferedStdTchr" >
                            1 
                            </label>
                        </div>  
                        <div class="divwidth">                  
                            <label class="checkbox inline">
                            <input type="checkbox" value="1" id="g02OfferedStdTchr" name="g02OfferedStdTchr" >
                            2 
                            </label>
                        </div>  
                        <div class="divwidth">                  
                            <label class="checkbox inline">
                            <input type="checkbox" value="1" id="g03OfferedStdTchr" name="g03OfferedStdTchr" >
                            3 
                            </label>
                        </div>  
                        <div class="divwidth">                  
                            <label class="checkbox inline">
                            <input type="checkbox" value="1" id="g04OfferedStdTchr" name="g04OfferedStdTchr" >
                            4 
                            </label>
                    </div>  
                    <div class="divwidth">                  
                            <label class="checkbox inline">
                            <input type="checkbox" value="1" id="g05OfferedStdTchr" name="g05OfferedStdTchr" >
                            5 
                            </label>
                    </div>  
                    <div class="divwidth">                  
                            <label class="checkbox inline">
                            <input type="checkbox" value="1" id="g06OfferedStdTchr" name="g06OfferedStdTchr" >
                            6 
                            </label>
                    </div>  
                    <div class="divwidth">                  
                            <label class="checkbox inline">
                            <input type="checkbox" value="1" id="g07OfferedStdTchr" name="g07OfferedStdTchr" >
                            7 
                            </label>
                    </div>  
                    <div class="divwidth">                  
                            <label class="checkbox inline">
                            <input type="checkbox" value="1" id="g08OfferedStdTchr" name="g08OfferedStdTchr" >
                            8 
                            </label>
                    </div>  
                    <div class="divwidth">                  
                            <label class="checkbox inline">
                            <input type="checkbox" value="1" id="g09OfferedStdTchr" name="g09OfferedStdTchr" >
                            9 
                            </label>
                    </div>  
                    <div class="divwidth">                  
                                <label class="checkbox inline">
                            <input type="checkbox" value="1" id="g10OfferedStdTchr" name="g10OfferedStdTchr" >
                            10 
                            </label>
                    </div>  
                    <div class="divwidth">                  
                            <label class="checkbox inline">
                            <input type="checkbox" value="1" id="g11OfferedStdTchr" name="g11OfferedStdTchr" >
                            11 
                            </label>
                    </div>  
                    <div class="divwidth">                  
                            <label class="checkbox inline">
                            <input type="checkbox" value="1" id="g12OfferedStdTchr" name="g12OfferedStdTchr" >
                            12 
                            </label>
                            
                </div>
                
                </div>
            </div>
            <div class="row">
            <div class="col-sm-2 col-md-2">
                    <label><spring:message code="lblFrDate" /><span class="required ">*</span></label>
                    <input type="text" id="fromStdTch" name="fromStdTch" class="form-control" placeholder="" maxlength="0">                 
                </div>
                <div class="col-sm-2 col-md-2">
                    <label><spring:message code="lblToDate" /><span class="required ">*</span></label>
                    <input type="text" id="toStdTch" name="toStdTch" class="form-control" placeholder="" maxlength="0">
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3 col-md-3">
                <input type="hidden" id="stdTchrExpId" value="">
                    <a href="#" style="cursor: pointer; text-decoration:none;" onclick="return insertOrUpdateStdTchrExp()"><spring:message code="lnkImD" /></a>&nbsp;
                    <a href="#" style="cursor: pointer; text-decoration:none;" onclick="return hideStdTchrExp()">
                        <spring:message code="btnClr" />
                    </a>
                </div>
            </div>
                                            
        
        
    </div> 
          </div>
    <!-- I was a student teacher in The School District end -->
    


 <!-- Background Check -->	
 
 <div id="backgroundcheckMain" class="hide">
	<div class="row portfolio_Section_Gap">
		<div class="col-sm-6 col-md-6">
			<div class="span4 portfolio_Subheading">Background Check</div>
		</div>
	</div>
	<div class="row">
	<div class="col-sm-12 col-md-12">
		
				<div class="row">
				<div class="col-sm-3 col-md-3">					
				
				   	<label class="checkbox">
                                <input type="checkbox" class="hideWorkExp" onClick="ShowBackgroundCheck();"  id="BGcheck" name="BGcheck" > <label>Background Check&nbsp;<a href="#" id="iconpophover4" rel="tooltip" data-original-title="Please upload a background check. This is optional, but will d to be uploaded in order to be considered for this position in the future"><img src="images/qua-icon.png" width="15" height="15" alt=""></a></label>
                                </label> 
							
		</div></div></div></div>	
<div id="backgroundcheck" >
<iframe id='uploadFrameBackgroundCheckID' name='uploadFrameBackgroundCheck' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'></iframe>
	<form id='frmBackgroundCheckUpload' enctype='multipart/form-data' method='post' target='uploadFrameBackgroundCheck' action='backgroundCheckUploadServlet.do?fBackgroundCheck=${teacherpersonalinfo.backgroundCheckDocument}' class="form-inline">	
	<div class="row">
    	
		<br>
			<div class="col-sm-12 col-md-12">
				 <div class='divErrorMsg' id='errordiv_bottomPart_backgroundCheck' style="display: block;"></div>
			</div>
			<div>	
			<div class="col-sm-4 col-md-4"> 
				<input type="hidden" id="hdnBackgroundCheck" value="" /> 
				<label>Please upload Background document File</label><br/>
				<input name="backgroundCheck" id="backgroundCheck" type="file"><br/>
				<div id="divBackgroundCheckTxt">
					 <label id="lblBackgroundCheck">
						<c:if test="${empty teacherpersonalinfo.backgroundCheckDocument}">
   													<spring:message code="lblNone" />
   						</c:if>
						<a href="javascript:void(0)" id="hrefBackgroundCheck" onclick="downloadBackgroundCheck();if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;">${teacherpersonalinfo.backgroundCheckDocument}</a>
						<input type="hidden" id="bgcVal" value="${teacherpersonalinfo.backgroundCheckDocument}"/>
						</label>
				</div>
			</div>	
			<div class="col-sm-7 col-md-7" id="showschoollistdwr">
			</div>
			</div>
       </div>
       </form>
   </div>

   </div>
	       	
<!-- End :: Background Check -->
<!-- Start :: Video links -->


<div id="videoLinkDiv">
	<div class="row portfolio_Section_Gap">
	
		<div class="col-sm-12 col-md-12">		
				<div style="float: left" class="portfolio_Subheading">
					 <spring:message code="lnkViLnk" />
				</div>
				<div style="float: right" class="addPortfolio">
					<a href="#" id="addVideoIdDSPQ" onclick="return showVideoLinksForm()" ><spring:message code="lnkAddViLnk" /></a>
				</div>
				<div style="clear: both;"></div>
                      
 				<div class="col-sm-12 col-md-12" style="margin-left: -15px;" id="commonTextVideo">
 				<spring:message code="headPlzIncludeURLToAnyVideos" />
 				</div>	   	
 				<div class="row" onmouseover="setGridNameFlag('videolinks')"> 				
							    <div class="col-sm-10 col-md-10">
								<div id="divDataVideoLinks" ></div>
								</div>
							</div>
						<iframe id='uploadFrameVideoID' name='uploadFrameVideo' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'>
						</iframe>
				<div class="top15" style="display: none" id="divvideoLinks" onkeypress="return checkForEnter(event);">
				<div class='divErrorMsg' id='errordivvideoLinks' style="display: block;"></div>
				<form class="" id="frmvideoLinks" name="frmvideoLinks" method='post'  enctype='multipart/form-data' target='uploadFrameVideo' action='certificationVideoUploadServlet.do'>
				<input type="hidden" id="sbtsource_videoLink" name="sbtsource_videoLink" value="0"/>
					<div class="row">
						<div class="col-sm-12 col-md-12" style="max-width: 880px;">
							<label><spring:message code="lblVidLnk" /></label>
							<input type="hidden" id="videolinkAutoId" name="videolinkAutoId">
							<input type="text" id="videourl" name="videourl" class="form-control" placeholder="" maxlength="200">
						</div>
					</div>
					<div class="row">
							<div class="col-sm-6 col-md-6">
								<br/>
									<label><spring:message code="lblVid" /></label>												
									<input type="file" id="videofile" name="videofile">												
									<span style="display: none; visibility: hidden;" id="video"></span>
							</div>										
							<span class="col-sm-6 col-md-6">
							<br/><br/>												
								<spring:message code="lblSupportedvideos" />												
							</span>
					 </div>
					<div class="row top10">
					    <div class="col-sm-4 col-md-4 idone">
						<a href="javascript:void(0)" onclick="insertOrUpdatevideoLinks();"><spring:message code="lnkImD" /></a>&nbsp;
						<a href="#"	onclick="return hideVideoLinksForm()"><spring:message code="btnClr" />
							</a>
					    </div>
					</div>
				</form>
			</div>
		</div>
	</div>	
</div>

<!-- End :: Video links -->

<!-- Start :: References Body -->
<div id="referenceDiv">
	<div class="row portfolio_Section_Gap">
	
		<div class="col-sm-12 col-md-12">		
				<div style="float: left" class="portfolio_Subheading">
					
					<!--<spring:message code="lblRef" /><span class="required hide" id="reqReferenceAstrick">*</span>-->
					
					<strong>
					<c:choose>
					<c:when test="${districtMaster.districtId == 806810 or districtMaster.districtId == 7800033}">
					<spring:message code="lblRefsummitPubLic" />&nbsp;<span class="required hide" id="reqReferenceAstrick">*</span>
					</c:when>
					<c:otherwise>
					<spring:message code="lblRef" />&nbsp;<span class="required hide" id="reqReferenceAstrick">*</span>
					</c:otherwise>
					</c:choose>
					
					<c:choose>
 					<c:when test="${districtMaster.districtId eq 806810 }">
 						<c:choose>
       						<c:when test="${jobOrder.jobCategoryMaster.jobCategoryName eq 'Volunteer'}">
									<a href="#" id="iconpophover12" rel="tooltip" data-original-title='Three letters of recommendation are required. If your letters of recommendation are not from your reference, please upload the letters of recommendation to the Additional Documents section'><img src="images/qua-icon.png" width="15" height="15" alt=""></a>
       						</c:when>
       					<c:otherwise>
								<a href="#" id="iconpophover12" rel="tooltip"></a>
     					</c:otherwise>
		     		</c:choose>
     			</c:when>
     	     	<c:otherwise>     	     	
     	     	<a href="#" id="iconpophover12" rel="tooltip"></a>      	     	
     	     	   <c:if test="${districtMaster.headQuarterMaster ne null &&  districtMaster.headQuarterMaster.headQuarterId eq 2}"><a data-original-title="Please enter at least three references who may be contacted." rel="tooltip" id="referenceToolTip" href="#"><img width="15" height="15" alt="" src="images/qua-icon.png"></a></c:if>   	     	    
					<c:if test="${districtMaster.districtId == 806900}"><a data-original-title="Professional References: please add at least three (3) professional references." rel="tooltip" id="referenceToolTip" href="#"><img width="15" height="15" alt="" src="images/qua-icon.png"></a></c:if>
     			</c:otherwise>
  	 			</c:choose>
					</strong>
				</div>
				<div style="float: right" class="addPortfolio">
					<a href="#" id="addReferIdDSPQ" onclick="return showElectronicReferencesForm()" ><spring:message code="lnkAddRef" /></a>
				</div>
				
				<div style="clear: both;"></div>
               <!-- -----------ashish ------->                      
 				<div class="col-sm-12 col-md-12 hide" style="margin-left: -15px;" id="refrenceMsgForNC">
 				<spring:message code="refrenceMsg" />
 				</div>
				
				
			<!--	district-wide job-postings lable  -->
				<c:if test="${districtMaster.districtId == 601326}">
				<div style="float: left" class="" id="dynamicPortfolioInformation">
					<spring:message code="DSPQreferencetext" />
				</div>
				</c:if>
				<c:if test="${districtMaster.districtId == 806810 && (jobOrder.jobCategoryMaster.jobCategoryName ne 'Volunteer')}">
				<div style="float: left" class="" id="dynamicPortfolioInformation">
					<spring:message code="DSPQreferencetextsummit" />
				</div>
				</c:if>
				
			<!--district-wide job-postings lable   end -->
				<div style="clear: both;"></div>
				<!-- new berlin -->
				<div class="col-sm-12 col-md-12 hide newBerlinCss" style="margin-left: -15px;"><spring:message code="msgDp_commons40" /></div>				
				<!-- new berlin -->
				<div class="col-sm-12 col-md-12 hide philadelphiaCss philadelphiaCssRef" style="margin-left: -15px;"><spring:message code="msgDp_commons41" /></div>
				<div class="col-sm-12 col-md-12 hide refTextHeader" style="margin-left: -15px;"></div>
				<c:if test="${districtMaster.districtId == 1200390}">
 				<div class="col-sm-12 col-md-12" style="margin-left: -15px;">
				<label><spring:message code="msgDp_commons42" /></label>
				<label>
				<spring:message code="lblPlease" /> <a onclick="openPDF();" href="javascript:void(0);"><spring:message code="lnkClickHere" /></a>
				<spring:message code="msgDp_commons43" />
				</label>
				</div>
					</c:if>	
				<c:choose>
				<c:when test="${districtMaster.districtId == 7800038}">
					<div class="col-sm-12 col-md-12" style="margin-left: -15px;">
 				<label class="required"><spring:message code="lbl3Rq5cm" /></label><p><spring:message code="msgDp_commons44" /></p>
 				</div>
		</c:when>
		<c:otherwise>
			<div class="col-sm12 col-md-12 hide nobleCssShow" style="margin-left: -15px;">
 				<label class="required"><spring:message code="lbl3Rq5cm" /></label><p><spring:message code="msgDp_commons44" /></p>
 				</div>
		</c:otherwise>
		</c:choose>
 							   	
		</div>
	</div>	
	<div class="row" onmouseover="setGridNameFlag('reference')">
	    <div class="col-sm-12 col-md-12">
			<div id="divDataElectronicReferences" class="span15"></div>
		</div>
	</div>
	
	<!-- Input form -->	
	<div class="portfolio_Section_ImputFormGap" style="display: none;" id="divElectronicReferences" onkeypress="chkForEnterElectronicReferences(event)">
		    <div class="row">
		        <div class="col-sm-12 col-md-12">
		        <div class='divErrorMsg' id='errordivElectronicReferences' style="display: block;"></div>
		        </div>
	        </div>
			
			<iframe id='uploadFrameReferencesID' name='uploadFrameReferences' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'></iframe>
			<form id="frmElectronicReferences" name="frmElectronicReferences" enctype='multipart/form-data' method='post' target='uploadFrameReferences' action='fileuploadservletforreferences.do' >
			
			<div class="row">
				<div class="col-sm-3 col-md-3">
					<label><spring:message code="lblSalutation" /></label>
					<select class="form-control" id="salutation" name="salutation">
						<option value="0"></option>   
						<option value="4"><spring:message code="optDr" /></option>
						<option value="3"><spring:message code="optMiss" /></option>
						<option value="2"><spring:message code="optMr" /></option>
						<option value="1"><spring:message code="optMrs" /></option>
						<option value="5"><spring:message code="optMs" /></option>													
					</select>
				</div>
					
				<div class="col-sm-3 col-md-3">
					<label><spring:message code="lblFname" /><span class="required">*</span></label>
					<input type="hidden" id="elerefAutoId">
					<input type="text" id="firstName" name="firstName" class="form-control" placeholder="" maxlength="50">
				</div>
				
				<div class="col-sm-3 col-md-3">
					<label><spring:message code="lblLname" /><span class="required ">*</span></label>
					<input type="text" id="lastName" name="lastName" class="form-control" placeholder="" maxlength="50">
				</div>
					
			</div>
			
			<div class="row">
				<div class="col-sm-3 col-md-3">
					<label><spring:message code="lblTitle" />
					<span class="required hide PHLreq">*</span>&nbsp;
					<c:if test="${districtMaster.districtId eq 4800061}">
						<span class="required">*</span>
					</c:if>
					<a href="#" id="destool" rel="tooltip" data-original-title=<spring:message code="msgRecommenderTitle" />><img src="images/qua-icon.png" width="15" height="15" alt=""></a></label>
					<input type="text" id="designation" name="designation" class="form-control" placeholder="" maxlength="50">
				</div>
				
				<div class="col-sm-6 col-md-6">
					<label><spring:message code="lblOrganizationEmp" />
					<span class="required hide nobleCssShow PHLreq">*</span>
					<c:if test="${districtMaster.districtId eq 4800061}">
						<span class="required">*</span>
					</c:if>
					</label>
					<input type="text" id="organization" name="organization" class="form-control" placeholder="" maxlength="50">
				</div>
				
			</div>
			
			<div class="row">
				<div class="col-sm-3 col-md-3">
					<label><spring:message code="lblContNum" /><span class="required">*</span>&nbsp;<a href="#" id="iconpophover5" rel="tooltip" data-original-title="<spring:message code='tooltipPhoneNo' />"><img src="images/qua-icon.png" width="15" height="15" alt=""></a></label>
					<input type="text" id="contactnumber" name="contactnumber" onKeyUp="numericFilter(this);" class="form-control" placeholder="" maxlength="10">
				</div>
				
				<div class="col-sm-6 col-md-6">
					<label><spring:message code="lblEmail" /><span class="required">*</span></label>
					<input type="text" id="email" name="email" class="form-control" placeholder="" maxlength="50">
				</div>
			</div>
			
			<div id="recomendationOpt" class="row top15 nobleCssHide ${transUDis}">
				<div class="col-sm-4 col-md-4">
					<label>
						<spring:message code="msgRecommendationLetter" /><span class="required hide" id="recommLetter">*</span>
					</label>
					<input type="hidden" id="sbtsource_ref" name="sbtsource_ref" value="0"/>
					<input id="pathOfReferenceFile" name="pathOfReferenceFile" type="file" width="20px;">
				    <a href="javascript:void(0)" onclick="clearReferences()"><spring:message code="btnClr" /></a>
			    </div>
				<input type="hidden" id="pathOfReference"/>
				<div class="col-sm-3 col-md-3" id="removeref" name="removeref" style="display: none;">
					<label>
						&nbsp;&nbsp;
					</label>
					<span id="divRefName">
					</span>
					<a href="javascript:void(0)" onclick="removeReferences()"><spring:message code="lnkRemo" /></a>&nbsp;&nbsp;&nbsp;
						<a href="#" id="iconpophover7" rel="tooltip" data-original-title="Remove recommendation letter !"><img src="images/qua-icon.png" width="15" height="15" alt=""></a>
				</div>
		    </div>
			
			<div class="row top15 nobleCssHide ${transUDis}">
			    <div class="col-sm-5 col-md-5">
				    <label><spring:message code="lblPrsnDirectlyContByHiringAuth" />
				    <c:if test="${districtMaster.districtId eq 4800061}">
						<span class="required">*</span>
					</c:if>
					 <%if(Utility.getValueOfPropByKey("nccheckforjspandjs").equalsIgnoreCase("yes")){%>
				    	  <span id="cancontactastrik" class="required hide">*</span>
				    	 	<%}%>				
				    </label>
				    <div class="" id="" style="height: 40px;">
				    	<div class="radio inline col-sm-1 col-md-1">
				    	 <%if(Utility.getValueOfPropByKey("nccheckforjspandjs").equalsIgnoreCase("yes")){%>
				    	<input type="radio"  id="rdcontacted1" value="1"  name="rdcontacted" onchange="showAndHideCanContOnOffer(1)"> <spring:message code="lblYes" />
				    	 	<%}
				    	 	else
				    	 	{%>
				    	 		<input type="radio" checked="checked" id="rdcontacted1" value="1"  name="rdcontacted" onchange="showAndHideCanContOnOffer(1)"> <spring:message code="lblYes" />
				    	 	<%} %>			    	
				    	 
					    </div>
					    </br>
					    <div class="radio inline col-sm-1 col-md-1" style="margin-left:20px;margin-top:-10px;">
					    	<input type="radio" id="rdcontacted0" value="0" name="rdcontacted" onchange="showAndHideCanContOnOffer(1)"> No
					    </div>
				    </div>
			    </div>
			    
			    <div class="col-sm-5 col-md-5" id="canContOnOfferDiv" style="display: none;">
				    <label><spring:message code="msgIfNoWantOffr" /></label>
				    <div class="" id="" style="height: 40px;">
				    	<div class="radio inline col-sm-1 col-md-1">
					    	<input type="radio" id="canContOnOffer1" value="1"  name="canContOnOffer"> <spring:message code="lblYes" />
					    </div>
					    </br>
					    <div class="radio inline col-sm-1 col-md-1" style="margin-left:20px;margin-top:-10px;">
			    			<input type="radio" checked="checked" id="canContOnOffer0" value="0" name="canContOnOffer"> <spring:message code="lblNo" />
				    </div>
				    </div>
			    </div>
			    
			    <div class="col-sm-3 col-md-3 hide onlyPHL">
					<label><spring:message code="qnhowlongknwperson" /><span class="required">*</span></label>
					<input type="text" id="longHaveYouKnow" name="longHaveYouKnow" class="form-control" placeholder="">
				</div>
			</div>
			
		
				<div class="row top10">
			    <div class="col-sm-9 col-md-9" >
				    <label><spring:message code="qnhowdoknwperson" /></label>
				    <div id="referenceDetailText"><textarea rows="" cols=""></textarea></div>
				    				    
			    </div>
			</div>
			
			<div class="row">
			    <div class="col-sm-3 col-md-3 idone">
			    	<a href="#" style="cursor: pointer; text-decoration:none;" onclick="return insertOrUpdateElectronicReferences(0)"><spring:message code="lnkImD" /></a>&nbsp;
			    	<a href="#" style="cursor: pointer; text-decoration:none;" onclick="return hideElectronicReferencesForm()">
						<spring:message code="btnClr" />
					</a>
			    </div>
			</div>
											
			</form>
		
	</div>
</div>	
	<!-- End :: References Body -->

<!-- canada/French -->
<div class="hide" id="octCanadaDiv" >
<iframe id='uploadOctID' name='uploadFrameOct' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'></iframe>
	<form id='frmOctUpload' enctype='multipart/form-data' method='post' target='uploadFrameOct' action='octUploadServlet.do' class="form-inline">	
	<div class="row portfolio_Section_Gap">	
		<div class="col-sm-12 col-md-12">		
			<div style="float: left" class="portfolio_Subheading">
				<spring:message code="msgOntarioCollege" />
			</div>			
		</div>
	</div>
<div class="row">
			<div class="col-sm-12 col-md-12">
			<spring:message code="msgFrench" />
			</div>
	</div>
	<div class="row">
			<div class="col-sm-12 col-md-12">
			<div class='divErrorMsg' id='octErrors' style="display: block;"></div>
			</div>
	</div>
	<div class="row hide">
		<div class="col-sm-12 col-md-12 top5">
			<label class="span15">
				<strong><spring:message code="msgDp_commons51" /></strong>
			</label>
		</div>
	   </div>
	   <div class="row left1 hide">		
			 <div class="col-sm-1 col-md-1 radio top1">
		    	<input type="radio" id="currMemOct1" value="1" name="currMemOct">
		    	<spring:message code="btnYes" />
	        </div>
	        <br>				
			<div class="col-sm-1 col-md-1 radio" style="margin-top:-18px;">
				<input type="radio" id="currMemOct2" value="0" name="currMemOct"> <spring:message code="btnNo" />					
	        </div>	       
        </div>
		<div class="row">
			<div class="col-sm-3 col-md-3">
				<label><strong><spring:message code="msgOCTRegistrationNum" /></strong></label>
				<input type="text" id="octNumber" name="octNumber" class="form-control" maxlength="50"/>
			</div>
			<div class="col-sm-3 col-md-3">
				<button class="btn btn-primary fl top25-sm" type="button">Search <i class="icon"></i></button>
			</div>
			<div class="col-sm-2 col-md-2">
				<label><strong><spring:message code="lblOCTCardUpload" /></strong></label>
				<input type="file" id="octUpload" name="octUpload" width="20px;"/>
			</div>
			<div class="col-sm-3 col-md-3 top5" id="displayOctFile">
				<a href='javascript:void(0)' rel='tooltip' data-original-title='Click here to view Answer !' id='hrefOctUpload' onclick="downloadOctUpload();if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;"><spring:message code="lnkV" /></a>
				<input type="hidden" id="hdnOctUpload">
	
			</div>
			<input type="hidden" id="octId">
		</div>
		<div class="row">
			<div class="col-sm-6 col-md-6" id="octText">
				<label><strong> <spring:message code="msgComments" /></strong></label>
				<textarea name="" id="" class="form-control" rows="3"></textarea>
			</div>
		</div>
</div>
</form>
<!-- canada/French -->
	
<!-- Start :: Language Body -->
<div id="languageDiv" class="hide">
    <div class="row portfolio_Section_Gap">
        <div class="col-sm-12 col-md-12">
            <div style="float: left" class="portfolio_Subheading">
                    <spring:message code="lblLanguage" />
                </div><!--
                <div style="float: right" class="addPortfolio">
                    <a href="#" onclick="return showElectronicReferencesForm()" >+ Add Language</a>
                </div>
                <div style="clear: both;"></div>
                
        --></div>
        <div style="clear: both;"></div>
        <div class="col-sm-12 col-md-12">
        <label>
            <spring:message code="qnDoYouknwOtherLang" />
            </label>
            <br><INPUT TYPE="radio" NAME="knowLanguage" id="knowLanguage1"  value="1" onclick="showLanguageGrid();"> &nbsp;&nbsp;&nbsp;<spring:message code="btnYes" />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<INPUT TYPE="radio" id="knowLanguage2" NAME="knowLanguage" value="0" onclick="showLanguageGrid();" checked> &nbsp;&nbsp;&nbsp;<spring:message code="btnNo" />
        </div>  
        
            <div class="col-sm-12 col-md-12 hide langGridNform">       
                <div style="float: left" class="portfolio_Subheading">                    
                </div>
                <div style="float: right" class="addPortfolio">
                    <a href="#" onclick="return showLanguageForm()" ><spring:message code="lnkAddLang" /></a>
                </div>

                <div style="clear: both;"></div>
                </div>
         <div class="col-sm-12 col-md-12 top5 hide langGridNform" onmouseover="setGridNameFlag('language')">
            <div id="divDataLangTchrGrid" class="span15"></div>
        </div> 

        
        
        <div class="portfolio_Section_ImputFormGap hide" style="display: none" style="margin-left: 15px;margin-right: 15px;" id="divLanguageForm" >
           <div class="col-sm-12 col-md-12"> <div class="row">
        
                <div class="col-sm-12 col-md-12">
                <div class='divErrorMsg' id='errordivTchLang' style="display: block;"></div>
                </div>
            </div></div>

<div class="col-sm-12 col-md-12">

        <div class="row">
        <div class="col-sm-4 col-md-4">
                    <label><spring:message code="lblLanguage" /><span class="required">*</span></label>
                    <input type="text" id="languageText" name="languageText" class="form-control" placeholder="">
                </div>
        <div class="col-sm-4 col-md-4">
                    <label><spring:message code="lblOralSkill" /><span class="required">*</span></label>
                    <SELECT NAME='' id='oralSkills' class="form-control">
			           <OPTION VALUE='0' ><spring:message code="optSlt" /></option>
			            <OPTION VALUE='1'><spring:message code="lblPolite" /></option>
			            <OPTION VALUE='2'><spring:message code="lblLiterate" /></option>
			            <OPTION VALUE='3'><spring:message code="lblFluent" /></option>
                    </SELECT>
                </div>
        <div class="col-sm-4 col-md-4">
                    <label><spring:message code="lblWrittenSkill" /><span class="required">*</span></label>
                     <SELECT NAME='' id='writtenSkills' class="form-control">
	                    <OPTION VALUE='0' ><spring:message code="optSlt" /></option>
		                <OPTION VALUE='1'><spring:message code="lblPolite" /></option>
		                <OPTION VALUE='2'><spring:message code="lblLiterate" /></option>
		                <OPTION VALUE='3'><spring:message code="lblFluent" /></option></SELECT>
                    </SELECT>
                </div>
         </div>
          <div class="row">
                <div class="col-sm-3 col-md-3">
                <input type="hidden" id="teacherLanguageId" value="">
                    <a href="#" style="cursor: pointer; text-decoration:none;" onclick="return insertOrUpdateTchrLang()"><spring:message code="lnkImD" /></a>&nbsp;
                    <a href="#" style="cursor: pointer; text-decoration:none;" onclick="return hideTchrLang()">
                        <spring:message code="lnkCancel" />
                    </a>
                </div>
            </div>
            </div>
            </div>
</div>     
    </div>
<!-- End :: Language Body -->
	
		
	<div id="tfaTeacherDiv">
		<div class="row portfolio_Section_Gap">
			<div class="col-sm-4 col-md-4">
				<div class="span4 portfolio_Subheading"><spring:message code="msgTeachTFAAffiliate" /></div>
			</div>
		</div>
			
		<div class="row">
			<div class="col-sm-9 col-md-9">
				<div class='divErrorMsg' id='errordiv_bottomPart_TFA' style="display: none;"></div>
			</div>
		</div>	
		<div class="row">
			<div class="col-sm-4 col-md-4">
				<label>
					<strong><spring:message code="lblTFA" /><span class="required philNT tfarequired" id="tfarequired" style="display: none;">*</span>
						<a href="#" id="iconpophover4" rel="tooltip" data-original-title="Please indicate your status for the year in which this position will be hired for ex: if you are a second year corps member and the position is for next school year, please indicate 'alum'">
							<img src="images/qua-icon.png" width="15" height="15" alt="">
						</a>
					</strong>
				</label>
				<select  class="form-control" id="tfaAffiliate" onchange="hideTFAFields_DP()">   
					<option value=""><spring:message code="lblSelectTFA" /></option>
						<c:forEach items="${lstTFAAffiliateMaster}" var="lstTFAAffilate">
			        		<option id="${lstTFAAffilate.tfaAffiliateId}" value="${lstTFAAffilate.tfaAffiliateId}">${lstTFAAffilate.tfaAffiliateName}</option>
						</c:forEach>
						
				</select>		
			</div>
			<c:set var="hide" value=""></c:set>
			<c:if test="${teacherExperience.tfaAffiliateMaster.tfaAffiliateId eq 3}">
				<c:set var="hide" value="hide"></c:set>
			</c:if>
			
			<div id="tfaFieldsDiv" class="${hide}">
				<div class="col-sm-3 col-md-3">
					<label>
						<strong><spring:message code="lblCorpsYear" />
                       <span class="required tfarequired">*</span></strong>
					</label>
					<select  class="form-control" id="corpsYear">   
						<option value=""><spring:message code="lblSltCorpsYear" /></option>
							 <c:forEach var="corps" items="${lstCorpsYear}">	
								<option id="corps${corps}" value="${corps}">${corps}</option>
							</c:forEach>	
					</select>		
				</div>
				<div class="col-sm-3 col-md-3">
					<label>
						<strong><spring:message code="lblTFARegion" /><span class="required philNT tfarequired">*</span></strong>
					</label>
					<select  class="form-control"  id="tfaRegion" onchange="">  
						<option value=""><spring:message code="lblSelectTFARegion" /></option>
						 <c:forEach items="${lstTFARegionMaster}" var="lstTFARegion">
			        		<option id="${lstTFARegion.tfaRegionId}" value="${lstTFARegion.tfaRegionId}">${lstTFARegion.tfaRegionName}</option>
						</c:forEach>
						
					</select>		
				</div>
			</div>
		</div>
	</div>
	<!-- tfa option phil-->
	<div id="tfaDistSpecificoption" class="hide">	
	<div class="row portfolio_Section_Gap">
			<div class="col-sm-12 col-md-12">
				<div class=""><font class="portfolio_Subheading"><spring:message code="lblProgramParticipation" /></font><span class="required philNT">*</span></div>
			</div>
		</div>
		<div class="row">		
			<div class="col-sm-9 col-md-9">
				<div class='divErrorMsg' id='errordiv_bottomPart_tfaOptions' style="display: none;"></div>
			</div>
		</div>
		<div class="row" id="tfaDistOptData">		
		</div>
		
	</div>
	<!--end tfa option -->

	<div id="substituteTeacherDiv">
		<div class="row portfolio_Section_Gap">
			<div class="col-sm-6 col-md-6">
				<div class="span4 portfolio_Subheading"><spring:message code="lblSubstituteTeacher" /></div>
			</div>
		</div>
			
		<div class="row">		
			<div class="col-sm-9 col-md-9">
				<div class='divErrorMsg' id='errordiv_bottomPart_wst' style="display: none;"></div>
			</div>
		</div>	
		<div class="row">	
			<div class="col-sm-12 col-md-12">
				<label class="span15">
					<strong><span id="textForSubs"><spring:message code="qnWillingToserveSubTeacher" /></span><span class="required philNT" id="sSubTrequired" style="display: none;">*</span>
					<a href="#" id="ssubtTooltip" rel="tooltip" data-original-title="Would you be willing to serve as a substitute "><img src="images/qua-icon.png" width="15" height="15" alt=""></a>
					</strong>
				</label>
			</div>	
		</div>
		  
	   <div class="row left1">  	        
		        <table>
		        <tr>
		         <td class="radio inline">
		         <input type="radio" id="canServeAsSubTeacher1" value="1" name="canServeAsSubTeacher">		       
		        </td>
		        <td style="padding-left:5px;">
		         <div style=""><spring:message code="optY" /></div>		       
		        </td>
		        <td style="width: 40px;"></td>
		        <td class="radio inline">
		          <input type="radio" id="canServeAsSubTeacher0" value="0" name="canServeAsSubTeacher"> 
		        </td>
		        <td style="padding-left:5px;">
		        <div style=""><spring:message code="optN" /></div>
		        </td>
		        </tr>
		        </table>
		        		
		</div>
		
	</div>

<!-- Resume ORG -->	
<div id="resumeDiv">
<iframe id='uploadFrameResumeID' name='uploadFrameResume' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'></iframe>
	<form id='frmExpResumeUpload' enctype='multipart/form-data' method='post' target='uploadFrameResume' action='expResumeUploadServlet.do?f=${teacherExperience.resume}' class="form-inline">	
	<div class="row portfolio_Section_Gap">
		<div class="col-sm-6 col-md-6">
			<div class="span4 portfolio_Subheading"><spring:message code="lnkResume" />&nbsp;<spring:message code="msgPDFPreferred" /><span class="required" id="requiredRessumeHeader">*</span></div>
		</div>
	</div>
	
	<div class="row">
    	<div class="col-sm-12 col-md-12">
        	<spring:message code="msgAbtYurResume" />
			
		</div>
		<br>
			<div class="col-sm-12 col-md-12">
				 <div class='divErrorMsg' id='errordiv_bottomPart_resume' style="display: block;"></div>
			</div>
			<div>	
			<div class="col-sm-4 col-md-4"> 
				<input type="hidden" id="hdnResume" value="" /> 
				<label><spring:message code="lblResumePleaseUploadAFile" /><span class="required" id="requiredRessume">*</span></label><br/>
				<input name="resume" id="resume" type="file"><br/>
				<div id="divResumeTxt">
					 <label id="lblResume">
						<spring:message code="lblRecentResOnFi" />
						<c:if test="${empty teacherExperience.resume}">
   													<spring:message code="lblNone" />
   						</c:if>
						<a href="javascript:void(0)" id="hrefResume" onclick="downloadResume();if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;">${teacherExperience.resume}</a>
						</label>
				</div>
			</div>	
			<div class="col-sm-7 col-md-7" id="showschoollistdwr">
			</div>
			</div>
       </div>
       </form>
   </div>

	<div class="portfolio_Section_Gap" id="expCertTeacherTrainingDiv" >
	    <div class="row">
			<div class="col-sm12 col-md-12">
				<div class='divErrorMsg' style="display: none;" id="errExpCTT"></div>
			</div>	
		</div>
	   	<div class="row">	
	   	
	   			<div class="col-sm-12 col-md-12">
			<label>
				<strong>Are you a certified teacher?<span class="required teacherExpReq">*</span></strong>
			</label><br>
			 <form role="form">
			    <label class="radio-inline">
			      <input type="radio" id="isNonTeacherYes" value="0" name="isNonTeacher" onclick="return chkNonTeacher();">Yes
			    </label>
			    <label class="radio-inline">
			      <input type="radio" id="isNonTeacher" value="1" checked name="isNonTeacher" onclick="return chkNonTeacher();">No
			    </label>
		  </form>
		</div>
		
	   	<div class="col-sm-12 col-md-12 hide expDiv"> 	
	    		<label>
					<strong style="max-width: 230px;" id="yrOfcertTchrTxt"><spring:message code="lblCertiTeachExp" /><span class="required teacherExpReq">*  </span>
						<a href="#" id="iconpophover10" rel="tooltip" data-original-title="Years as a full-time instructor in an alternative certification program should be included. If you do not have any teaching experience, please enter 0."><img src="images/qua-icon.png" width="15" height="15" alt=""></a>
					</strong>
				</label>

	<c:choose>
			
	    <c:when test="${teacherExperience.isNonTeacher eq true}">
		<c:set var="expCertTeacherTrainingVal" value="${teacherExperience.expCertTeacherTraining }"></c:set>
	    </c:when>
	    <c:when test="${teacherExperience.isNonTeacher eq null}">
		<c:set var="expCertTeacherTrainingVal" value="0.0"></c:set>
	    </c:when>
	    <c:otherwise>			    
		<c:set var="expCertTeacherTrainingVal" value="${teacherExperience.expCertTeacherTraining }"></c:set>
		</c:otherwise>
		</c:choose>
		
		<c:set var="striveRadio1" value=""></c:set>
		<c:set var="striveRadio2" value="checked='checked'"></c:set>
		<c:choose>
		<c:when test="${teacherExperience.expCertTeacherTraining eq null || teacherExperience.expCertTeacherTraining eq 0}">
			<c:set var="striveRadio2" value="checked='checked'"></c:set>
			<c:set var="striveRadio1" value=""></c:set>
	    </c:when>
	    <c:otherwise>			    
		<c:set var="striveRadio1" value="checked='checked'"></c:set>
		<c:set var="striveRadio2" value=""></c:set>
		</c:otherwise>              
	</c:choose>
	
	<br>
			 <form role="form" id="striveExpFld" class="hide">
			    <label class="radio-inline">
			      <input type="radio" id="expTchRadioStrive" value="1" ${striveRadio1} name="expTchRadio">Yes
			    </label>
			    <label class="radio-inline">
			      <input type="radio" id="expTchRadioStrive" value="0" ${striveRadio2} name="expTchRadio">No
			    </label>
		 	 </form>
				<input type="text" id="expCertTeacherTraining" name="expCertTeacherTraining" class="form-control" onkeypress="return checkForDecimalTwo(event);" maxlength="5" value="${expCertTeacherTrainingVal}" style="width:20%">
	   	</div>

	   	<!--<div class="col-sm-4 col-md-4">
	   	
	   		<input type="checkbox" id="isNonTeacher" name="isNonTeacher" onclick="return chkNonTeacher();" style="margin-top:12%;">&nbsp;&nbsp;<spring:message code="lblIMNotACertiTech" />
	   	</div>
	   	 --></div>
	   	 
	   	 
   </div>
   
   <!--Start Q Sekhar -->
   <div id="QuestionDiv">	
	<div class="row portfolio_Section_Gap">
		<div class="col-sm-6 col-md-6">
			<div class="span4 portfolio_Subheading"><spring:message code="lblDistrictSpecificQuestions" /></div>
			 <div class='divErrorMsg' id='errordivspecificquestion' style="display: block;"></div>
			 <div class='divErrorMsg' id='errordivspecificquestionUpload' style="display: block;"></div>
		</div>
	</div>

	<div class="row">
	    <div class="col-sm-9 col-md-9">
		<div id="divGridDistrictSpecificQuestions"></div>
		</div>
	</div>
	
   </div>
   <!-- End Q Sekhar  -->
	<div  id="affidavitDiv">
	      <div class="row">
	   		<div class="col-sm12 col-md-12 top20">
   			<div class='divErrorMsg' style="display: none;" id="errAffidavit"></div>
   			</div>
   			<div class="col-sm12 col-md-12">
   			    <div class="row">
   			    	<div class="col-sm1 col-md-1" style="width: 20px;">
	   				<input type="checkbox" id="affidavit" name="affidavit">
	   				</div>
		   			<div class="col-sm10 col-md-10">					
						<spring:message code="pConfirm" />
						<a href="#" onclick="showAffidavitDiv()"><spring:message code="lnkShowAffidavitDetails" /><span class="required affidavitReq">*</span></a>
					</div>
   			    </div>	   				   
			</div>
	      </div> 	       
	  </div> 
	  
	    <!-- Start :: Additional Documents -->
<div id="additionalDocumentsDiv">		
		<div class="row top20">
			<div class="col-sm-12 col-md-12">				
				<div style="float: left" class="portfolio_Subheading">
					<spring:message code="lnkAddDocu" />
					
					<c:choose>
					<c:when test="${districtMaster.districtId eq 806810 && (jobOrder.jobCategoryMaster.jobCategoryName eq 'Support Staff' ||  jobOrder.jobCategoryMaster.jobCategoryName eq 'Coaching')}">
					<a href="#" id="iconpophover13" rel="tooltip" data-original-title='Please upload three (3) letters of reference from professional references'><img src="images/qua-icon.png" width="15" height="15" alt=""></a>
					</c:when>
					<c:otherwise>
					<a href="#" id="iconpophover13" rel="tooltip"></a>
					<spring:message code="lblRef" />&nbsp;<span class="required hide" id="reqReferenceAstrick">*</span>
					</c:otherwise>
					</c:choose>
				</div>
				<div style="float: right" class="addPortfolio">
					<a href="#" onclick="return showAdditionalDocumentsForm()"><spring:message code="lnkAddAdditionalDoc" /> </a>
				</div>
				<div style="clear: both;"></div>				
			</div>
			<div class="col-sm-12 col-md-12 hide philadelphiaCss"><spring:message code="msgDp_commons39" /></div>
			<div class="col-sm-12 col-md-12 hide additionalDocumentsHeaderText"></div>
		</div>
		 <c:if test="${districtMaster.districtId == 806810}">
    		<div style="float: left" class="" id="dynamicPortfolioInformation">
     			<spring:message code="DSPQreferencetextsummit" />
    		</div>
    	</c:if>
		
		<div class="row" onmouseover="setGridNameFlag('additionalDocuments')">
		  <div class="col-sm-9 col-md-9">
			<div id="divGridAdditionalDocuments"></div>
			</div>
		</div>

		<div  id="divAdditionalDocumentsRow" style="display: none; margin-top: 30px;" onkeypress="chkForAdditionalDocuments(event)">
				<iframe id='uploadFrameAdditionalDocumentsID' name='uploadFrameAdditionalDocuments' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'>
				</iframe>
				<form class="span15 textfield11" id="additionalDocumentsForm" name="multifileuploadformTarget2" enctype='multipart/form-data' method='post' target='uploadFrameAdditionalDocuments' action='additionalDocumentsUploadServlet.do'>
	
			            <div class="row">
							<div class="col-sm-9 col-md-9">
				      			<div class='divErrorMsg' id='errAdditionalDocuments' style="display: block;"></div>
							</div>
						</div>  
						<!-- Pavan      -->      	
					<div class="row">
						<div class="col-sm-4 col-md-4">
							<label><spring:message code="lnkAddDocu" /><span class="required">*</span></label>
							<input type="hidden" id="sbtsource_aadDoc" name="sbtsource_aadDoc" value="0"/>
							<input type="text" id="documentName" name="documentName" class="form-control" placeholder="" maxlength="250">
						</div>
						<div class="col-sm-3 col-md-3">
							<label>
								<spring:message code="lblDoc(PlzUploadAF)" /><span class="required">*</span>
							</label>
							<input type="hidden" id="uploadedDocumentHidden" name="uploadedDocumentHidden" value=""/>
							<input id="uploadedDocument" name="uploadedDocument" type="file" width="20px;">
							<a href="javascript:void(0)" onclick="clearUploadedDocument()"><spring:message code="btnClr" /></a>
						</div>
						<div class="col-sm-3 col-md-3" style="margin-top: 25px;" id="removeUploadedDocumentSpan" name="removeUploadedDocumentSpan" style="display: none;">
						<label>
							&nbsp;&nbsp;
						</label>
						<div id="divUploadedDocument">
						</div>
						</div>
					</div>			
		       </form>			
			<div class="row">
				<div class="col-sm-4 col-md-4" id="divDocumnetDone" style="display: none; ">
				<a class="idone" style="cursor: pointer;text-decoration:none;" onclick="insertOrUpdate_AdditionalDocuments(0)">
					<spring:message code="lnkImD" />
				</a>&nbsp;&nbsp;
				<a class="idone" style="cursor: pointer;text-decoration:none;"	onclick="resetAdditionalDocumentsForm()">
					<spring:message code="btnClr" />
				</a>
				</div>
			</div>
	</div>
</div>		

<!-- End :: Additional Documents -->                     		
	
	</div>
	<div class="modal-footer">
	   <button  class="btn btn-large btn-primary hide" id="editSaveDspQBtn" type="button" onclick="saveEditDspq();"  ><strong><spring:message code="btnSave" /><i class="icon"></i></strong></button>
	   
	   	 <%if(Utility.getValueOfPropByKey("nccheckforjspandjs").equalsIgnoreCase("yes")){%>
				    	  <button  class="btn btn-large btn-primary" type="button" id="dynamicSaveDspq" onclick="saveAndContinueDynamicPortfolio();showForm_Licence();"  ><strong><spring:message code="btnSv&Conti" /> <i class="icon"></i></strong></button>
				    	 	<%}						 	
						 	else {%>
				    	  <button  class="btn btn-large btn-primary" type="button" id="dynamicSaveDspq" onclick="saveAndContinueDynamicPortfolio();"  ><strong><spring:message code="btnSv&Conti" /> <i class="icon"></i></strong></button>
				    	<%}%>	
 		<button class="btn" data-dismiss="modal" id="closeButtonDsqp" aria-hidden="true" onclick="showCLOnCloseOfDP();"><spring:message code="btnClose" /></button> 		
 	</div>
</div>
</div>
</div>

<!-- Popup window -->
<div class="modal hide"  id="chgstatusRef1"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div id="deact" class="modal-body"> 		
		<div class="control-group">
			<spring:message code="msgDoUActRef" />
		</div>
 	</div>
 	<div class="modal-footer">
 	<span id=""><button class="btn  btn-primary" onclick="changeStatusElectronicReferencesConfirm()" ><spring:message code="btnOk" /> <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="lnkCancel" /></button> 		
 	</div>
</div>
	</div>
</div>

<div class="modal hide"  id="chgstatusRef2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div id="deact" class="modal-body"> 		
		<div class="control-group">
			<spring:message code="msgDoUDeActRef" />
		</div>
 	</div>
 	<div class="modal-footer">
 	<span id=""><button class="btn  btn-primary" onclick="changeStatusElectronicReferencesConfirm()" ><spring:message code="btnOk" /> <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="lnkCancel" /></button> 		
 	</div>
</div>
	</div>
</div>

<div class="modal hide"  id="deleteCertRec" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div id="deact" class="modal-body"> 		
		<div class="control-group">
			<spring:message code="msgDeleteTheRecord" />
		</div>
 	</div>
 	<div class="modal-footer">
 	<input type="hidden" id="certificateTypeID"/>
 	<span id=""><button class="btn  btn-primary" onclick="deleteCertificateRecord()" ><spring:message code="btnOk" /> <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="lnkCancel" /></button> 		
 	</div>
</div>
 	</div>
</div>
<div class="modal hide"  id="removeUploadedDocument" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div id="deact" class="modal-body"> 		
		<div class="control-group">
			<spring:message code="msgDeleteTheRecord" />
		</div>
 	</div>
 	<div class="modal-footer">
 	<input type="hidden" id="academicID"/>
 	<span id=""><button class="btn  btn-primary" onclick="removeUploadedDocument()" ><spring:message code="btnOk" /> <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="lnkCancel" /></button> 		
 	</div>
	</div>
	</div>
</div>

<div class="modal hide"  id="deleteAcademicRecord"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div id="deact" class="modal-body"> 		
		<div class="control-group">
			<spring:message code="msgDeleteTheRecord" />
		</div>
 	</div>
 	<div class="modal-footer">
 	<input type="hidden" id="academicID"/>
 	<span id=""><button class="btn  btn-primary" onclick="deleteAcademicRecord()" ><spring:message code="btnOk" /> <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="lnkCancel" /></button> 		
 	</div>
</div>
	</div>
</div>	
<div style="display:none;z-index: 99999999;" id="loadingDiv_dspq_ie">
     <table  align="center" >
 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
	</table>
</div>
<div style="display:none;" id="loadingDivWait">
     <table  align="center" >
 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
	</table>
</div>


<div  class="modal hide" id="affidavitDataDiv"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog-for-teacherprofile">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
		<h3 id="myModalLabel"><spring:message code="headAffidavit" /></h3>
	</div>
	<div class="modal-body" style="max-height: 450px;overflow-y: auto; overflow-x: hidden;">
		<div class="scrollspy-example" data-offset="0" data-target="#navbarExample" data-spy="scroll">
			
			<p style="text-indent: 0px;"><spring:message code="pUseCarefullyBeforeUsingTheTm" /></p>
			
			<div style="text-align: center; padding-bottom: 5px;">
				<spring:message code="pCopyrightTm" /><br/>
				<spring:message code="pAllRiRe" /><br/>
			</div>
			
			<p>
				<b><spring:message code="pOWNERSHIP" /></b>  <spring:message code="pEachOfItsCompIsCopyrightedPropertyOfTm" />  
			</p>
			<p>
				<b><spring:message code="pAgeAdRespo" /></b>  <spring:message code="pYuRepresntAtLeast(18)Yr" />  
			</p>
			<p>
				<b><spring:message code="pAccesingThePorAdAccSecurity" /></b>  <spring:message code="pEachTchCandMayBeProvidedWithUseraAdPss" />			
				<br/><br/><span  class="spnIndent"></span><spring:message code="pInConnWithEstablishingYurAccWithUs" /></span> 
				<br/><br/><span  class="spnIndent"></span><spring:message code="pYuAcknowledgeAdAgreeThatTmHasNoResponsibility" /> <a href="mailto:clientservices@teachermatch.net">clientservices@teachermatch.net</a>.  <spring:message code="pInAddDelAccCodeAtAnyTimeAdReason" /></span>
			</p>
			<p>
				<b><spring:message code="pSubmissions" /></b>  <spring:message code="pAllInfoOfTm" />
				<br/><br/><span  class="spnIndent"></span><spring:message code="pNoConfidentialRelEstabBySubMinssionByAnyCandTch" />  
			</p>
			<p>
				<b><spring:message code="pRulOfCon" /></b> <spring:message code="pWhileUThPortYuWillApplicableLaws" />
				<div class="spnIndent40">
				<ul>  
					<li><spring:message code="pManyInfoOfTm" /></li>
					<li><spring:message code="pCivilLiability" /></li>
					<li><spring:message code="pVirusWormTrHorsEasterEggTime" /></li>
					<li><spring:message code="pAnyPersIdentiInfoOfAthrIndividual" /></li>
					<li><spring:message code="pMateralNonPuInfoAbtCompany" /></li>
				</ul>
				</div>
				<spring:message code="pFurtherInConnWithYurUseOfPortal" />
				<div class="spnIndent40" style="padding-top: 5px;">
				<ul>
					<li><spring:message code="pUseThePorForFraudulent" /></li>
					<li><spring:message code="pImperAnyPersonOrEntity" /></li>
					<li><spring:message code="pInterfaceWiDisruptUseNet" /></li>
					<li><spring:message code="pRestOrInhibit" /></li>
					<spring:message code="pModiAdTransReveEngineer" /></ul>
				</div>
			</p>
			<p>
				<b><spring:message code="pUSEOFPORTAL" />
			</p>  
			<p>
				<b><spring:message code="pPRIVACY" /></b><br/>				
				
					<div  style="padding-left: 50px; "><spring:message code="pPRIVACY1" /><br/> <br/>
					</div>
					<div  style="padding-left: 50px; "><spring:message code="pPRIVACY2" /><br/>  
					<br/><spring:message code="pPRIVACY3" />    <br/><br/>
					</div>					
					<div  style="padding-left: 50px; "><spring:message code="pPRIVACY4" /><br/><br/>  
					</div>
					<div  style="padding-left: 50px; "><spring:message code="pPRIVACY5" /><br/>
					<ul>
						<li><spring:message code="pPRIVACYL1" /></li>
						<li><spring:message code="pPRIVACYL2" /></li>
						<li><spring:message code="pPRIVACYL3" /></li>
						<li><spring:message code="pPRIVACYL4" /></li>
						<li><spring:message code="pPRIVACYL5" /></li>
					</ul>
					</div>
				
			</p>
			<p>
				<b><spring:message code="p8USEOFDATA" /></b> <spring:message code="p8USEOFDATA1" />  
				<br/><br/><span  class="spnIndent"></span><spring:message code="p8USEOFDATA2" />  
				<br/><br/><span  class="spnIndent"></span><spring:message code="p8USEOFDATA3" />  
				<br/><br/><span  class="spnIndent"><spring:message code="p8USEOFDATA4" />  
				<br/><br/><span  class="spnIndent"><spring:message code="p8USEOFDATA5" /></span>  
			</p>
			<p>
				<b><spring:message code="head9EXCLUSIONOFWARRANTY" /></b> <spring:message code="pEXCLUSIONOFWARRANTY" />
			</p>
			<p>
				<b>
              </b>  <spring:message code="p10LIMITATIONOFLIABILITY" /> 

				<span  class="spnIndent"></span><br/><br/><spring:message code="p10LIMITATIONOFLIABILITY2" />
				
				<span  class="spnIndent"></span><br/><br/><spring:message code="p10LIMITATIONOFLIABILITY3" />
			</p>
			<p>
				<b><spring:message code="head11INDEMNIFICATION" /></b>  <spring:message code="p11INDEMNIFICATION" />
			</p>
			<p>
				<b><spring:message code="head12RELATIONSHIP" /> </b> <spring:message code="p12RELATIONSHIP" />
			</p>
			<p>
				<b><spring:message code="head13GOVERNINGLAW" /></b>  <spring:message code="p13GOVERNINGLAW" />
			</p>
			<p>
				<b>	<spring:message code="head14ASSIGNMENT" /></b>  <spring:message code="p14ASSIGNMENT" />
			</p>
			<p>
				<b>	<spring:message code="head15MODIFICATION" /></b>  <spring:message code="p15MODIFICATION" />  
			</p>
			<p>
				<b>	<spring:message code="head16SEVERABILITY" /></b>  <spring:message code="p16SEVERABILITY" />
			</p>
			<p>
				<b>	<spring:message code="head17HEADINGS" /></b>  <spring:message code="p17HEADINGS" />
			</p>
			<p>
				<b>	<spring:message code="head18ENTIREAGREEMENT" /></b>  <spring:message code="p18ENTIREAGREEMENT" />
			</p>	
			<p  style="text-indent: 0px;">
				<spring:message code="msgCommentsOrQuestion" /> <a href="mailto:clientservices@teachermatch.net">clientservices@teachermatch.net</a> <spring:message code="pWrtUsAt" />
			</p>
			
				<div style="margin-left: 260px;">			
				<spring:message code="pTmLLC" /><br/>
				<spring:message code="msgAddress207" /><br/>
				<spring:message code="pChiIL60640" /><br/>
				<spring:message code="pAttentionTe" /><br/><br/>
				</div>
				
			
			<spring:message code="msgLastreview2012" />
			<hr/>
			<p style="text-indent: 0px;" >
				<spring:message code="msgDp_commons45" />  
			</p>
			<hr/>
			<p style="text-indent: 0px;" >
				<spring:message code="msgDp_commons46" />
			</p>  
		</div>
	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnOk" /></button>
 	</div>
</div>
	</div>
</div>


<div  class="modal hide" id="draggableDivMaster" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog-for-cgdemoschedule">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel"><spring:message code="lblCert/LiceName" /></h3>
	</div>
	<div class="modal-body">
		<p><spring:message code="msgWeArSoryYuNotFindingYurCerti/Lice" /></p>
		<p><spring:message code="msgPlzTypingStringInFldCerti/Lice" /></p>
		<p><spring:message code="pNotifyEmailService" /> 
		<a href="mailto:clientservices@teachermatch.net" target="_top">clientservices@teachermatch.net</a>
		 <spring:message code="pHlpYuLoadRiLice/Certi" /></p> 
		<p><spring:message code="pThkForYurHlpInThisMtr" /></p>
	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnOk" /></button>
 	</div>
</div>
	</div>
</div>
<div class="modal hide"  id="redirectToDashboard"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="window.location.href='userdashboard.do'">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div id="redirectLabel" class="modal-body"> 		
		<div class="control-group">
			<spring:message code="msgJobAplliedSuccess" />
		</div>
 	</div>
 	<div class="modal-footer">
 	
 	<span id=""><button class="btn  btn-primary" onclick="window.location.href='userdashboard.do'" ><spring:message code="btnOk" /> <i class="icon"></i></button></span>&nbsp;&nbsp;
 	</div>
</div>
	</div>
</div>
<div class="modal hide"  id="redirectToPortfolio"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="window.location.href='personalinfo.do'">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div id="redirectLabel" class="modal-body"> 		
		<div class="control-group">
			<spring:message code="msgReviewYourPortfolio" />
		</div>
 	</div>
 	<div class="modal-footer">
 	
 	<span id=""><button class="btn  btn-primary" onclick="window.location.href='personalinfo.do'" ><spring:message code="btnOk" /> <i class="icon"></i></button></span>&nbsp;&nbsp;
 	</div>
</div>
	</div>
</div>
<div class="modal hide"  id="redirectToPreference" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="window.location.href='userpreference.do'">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div id="redirectLabel" class="modal-body"> 		
		<div class="control-group">
			<spring:message code="msgPleaseCompleteJobPreference" />
		</div>
 	</div>
 	<div class="modal-footer">
 	
 	<span id=""><button class="btn  btn-primary" onclick="window.location.href='userpreference.do'" ><spring:message code="btnOk" /> <i class="icon"></i></button></span>&nbsp;&nbsp;
 	</div>
</div>
	</div>
</div>
<div class="modal hide"  id="jobApplyOrNot" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="performAction();">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div id="jobApplyOrNotLabel" class="modal-body"> 	
	<input type="hidden" id="perform" />
	<div class="control-group" id="notApplyMsg">
			<spring:message code="msgYouCantApply" />
		</div>
 	</div>
 	<div class="modal-footer">
 	<span id=""><button class="btn  btn-primary " data-dismiss="modal" onclick="performAction();"><spring:message code="btnOk" /> <i class="icon"></i></button></span>&nbsp;&nbsp;
 	</div>
</div>
	</div>
</div>


<!-- Check Duplicate Teacher -->
<div class="modal hide"  id="chkDupCandidate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="performAction();">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div  class="modal-body"> 	
		<div class="control-group" id="chkDupCandidateBody">
			<spring:message code="msgYouCantApply" />
		</div>
 	</div>
 	<div class="modal-footer">
 		<span id=""><button class="btn  btn-primary " data-dismiss="modal" onclick="chkDupCandidate();"><spring:message code="btnOk" /> <i class="icon"></i></button></span>&nbsp;&nbsp;
 	</div>
</div>
	</div>
</div>






<div class="modal hide"  id="deleteSubjectArea"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div id="deact" class="modal-body"> 		
		<div class="control-group">
			<spring:message code="msgDeleteTheRecord" />
		</div>
 	</div>
 	<div class="modal-footer">
 	<input type="hidden" id="academicID"/>
 	<span id=""><button class="btn  btn-primary" onclick="deleteSubjectAreaConfirm()" ><spring:message code="btnOk" /> <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClr" /></button> 		
 	</div>
</div>
 	</div>
</div>


<div class="modal hide"  id="deleteEmpHistory"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div id="deact" class="modal-body"> 		
		<div class="control-group">
			<spring:message code="msgDeleteTheRecord" />
		</div>
 	</div>
 	<div class="modal-footer">
 	<input type="hidden" id="empHistoryID"/>
 	<span id=""><button class="btn  btn-primary" onclick="deleteEmploymentDSPQ()" ><spring:message code="btnOk" /> <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClr" /></button> 		
 	</div>
</div>
	</div>
</div>

<div class="modal hide" id="certTextDivDSPQ" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog-for-cgcoverletter">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="showDSPQDiv();">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div class="modal-body" style="max-height: 400px;overflow-y:auto;"> 		
		<div class="control-group certTextContent">
			
		</div>
 	</div>
 	<div class="modal-footer">
 		<span id=""><button class="btn" data-dismiss="modal" onclick="showDSPQDiv();" aria-hidden="true"><spring:message code="btnClose" /></button></span>&nbsp;&nbsp; 		
 	</div>
</div>
	</div>
</div>

<div class="modal hide"  id="delVideoLnk" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div class="modal-body"> 		
		<div class="control-group">
			<spring:message code="msgDoUDeleteVideoLnk" />
		</div>
 	</div>
 	<div class="modal-footer">
 	<span id=""><button class="btn  btn-primary" onclick="delVideoLnkConfirm()" ><spring:message code="btnOk" /> <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClr" /></button> 		
 	</div>
</div>
	</div>
</div>

<div class="modal hide"  id="affDivPHiL" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" onclick="closeAffOpenCl();" aria-hidden="true">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div class="modal-body"> 		
		<div class="control-group">
			
	<c:choose>
      <c:when test="${districtMaster.districtId == 4218990}">
      <spring:message code="msgDp_commons47" /> (<a href="http://www.philasd.org">www.philasd.org</a>) <spring:message code="msgDp_commons48" />
      </c:when>
      <c:otherwise>
      <spring:message code="msgDp_commons49" />
      </c:otherwise>
    </c:choose>
    
		</div>
 	</div>
 	<div class="modal-footer">
 	<span id=""><button class="btn  btn-primary" onclick="closeAffOpenCl();" ><spring:message code="btnOk" /> <i class="icon"></i></button></span>&nbsp;&nbsp; 		 		
 	</div>
</div>
	</div>
</div>

<div class="modal hide"  id="wrongDivPHiL" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog">
    <div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" onclick="closeDspqOPenCl();" aria-hidden="true">x</button>
        <h3 id="myModalLabel">TeacherMatch</h3>
    </div>
    <div class="modal-body">        
        <div class="control-group" id="dynamicInfoMsg"></div>
    </div>
    <div class="modal-footer">    
    <span id=""><button class="btn  btn-primary" onclick="closeDspqOPenCl();" ><spring:message code="btnOk" /> <i class="icon"></i></button></span>&nbsp;&nbsp;               
    </div>
</div>
    </div>
</div>

<%-->>>>>>>>>>>>>>>>>>>>>>mukesh --%>
<div  class="modal hide"  id="checkInvitationDiv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
	<div class="modal-content">        
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="return redirectnonInvitCandURl();" >x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div class="modal-body">
		<div class="control-group" id='InvitationErrMsgDiv'>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="return redirectnonInvitCandURl();"><spring:message code="btnOk" /></button>
 	</div>
</div> 
</div>
</div>
<!--  Display  Video Play Div -->
<div  class="modal hide" id="videovDiv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width: 40%; " >
		<div class="modal-content">		
			<div class="modal-header">
			 		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="">x</button>
				<h3 id="myModalLabel"><spring:message code="lblVid" /></h3>
			</div>
			<div class="modal-body">		
				<div class="control-group">
						<div style="display:none;" id="loadingDivWaitVideo">
						     <table  align="center" >						 		
						 		<tr><td align="center"><img src="images/loadingAnimation.gif"/></td></tr>
							</table>
						</div>	
						<div id="interVideoDiv"></div>
				</div>
			</div>	
			<div class="modal-footer">	 			
	 		<button class="btn" id="closeBtn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClose" /></button>
 			</div>		
		</div>
	</div>
</div>

<div  class="modal hide"  id="myModalDASpecificQuestionsQQ" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
		<div class="modal-dialog-for-cgdemoschedule">
		<div class="modal-content">
		<div class="modal-header">
	  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
			<h3 id="myModalLabel"><spring:message code="headMandatoryDecler" /></h3>
		</div>
		<div class="modal-body" style="max-height: 450px;overflow-y: auto;overflow-x: hidden;">
		<span id='textForDistrictSpecificQuestions' class="divlablevalins"><spring:message code="msgDistReqAllAnsOfQues" /></span>
			<div class='divErrorMsg' id='errordiv4QQquestion' style="display: block;padding-bottom: 7px;"></div>
			<table width="100%" border="0" style="border: 1px solid #cccccc;"  class="table table-bordered table-striped" id="tblQQGrid">
			</table>
	 	</div>
	 	<div class="modal-footer">
	 	<c:if test="${jobForTeacher.jobForTeacherId>0}">
			<input type="hidden" id="isApplied" name="isApplied"/>
		</c:if>
		
		<input type="hidden" id="QQjobId" name="QQjobId"/>
		<button class="btn btn-large btn-primary" type="button" onclick="setQQDistrictQuestions();" ><strong><spring:message code="btnConti" /> <i class="icon"></i></strong></button>
			
				
							
	 		<button class="btn" data-dismiss="modal" aria-hidden="true" ><spring:message code="btnClr" /></button> 		
	 	</div>
</div>
</div>
</div>
<div  class="modal hide"  id="QuestionNotAvailable" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
		<div class="modal-dialog" style="width:400px;">
		<div class="modal-content">
		<div class="modal-header">
	  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
			<h3 id="myModalLabel">TeacherMatch</h3>
		</div>
		<div class="modal-body" style="max-height: 450px;overflow-y: auto;overflow-x: hidden;">		
				
				<table width="100%" style="display: none;" id="QuestionMesageforQQ">
						<tbody>
							<tr>
								<td width="15%" valign="top" style="padding-top: 0px;">
									<div class="control-group" id="warningImg1"><img src="images/info.png" align="top"></div>
								</td>
								<td width="35%" style="padding-left: 10px;">
									<div class="control-group" id="message2showConfirm1"><spring:message code="lblNoQQrqiredforjob" /></div>
								</td>
							</tr>
						</tbody>
					</table>
					<table width="100%" style="display: none;" id="SuccessMesageforQQ">
						<tbody>
							<tr>								
								<td width="35%" style="padding-left: 10px;">
									<div class="control-group" id="message2showConfirm1"><spring:message code="msgQQUpdateSuccess" /></div>
								</td>
							</tr>
						</tbody>
					</table>	
			
			
	 	</div>
	 	<div class="modal-footer">							
	 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnOk" /></button> 		
	 	</div>
</div>
</div>
</div>
<input type="hidden" id="BlankSSNvalue" name="BlankSSNvalue"/>
<div  class="modal hide"  id="myModalDASpecificQuestionsJSI" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
		<div class="modal-dialog-for-cgdemoschedule">
		<div class="modal-content">
		<div class="modal-header">
	  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
			<h3 id="myModalLabel"><spring:message code="headMandatoryDecler" /></h3>
		</div>
		<div class="modal-body" style="max-height: 450px;overflow-y: auto;overflow-x: hidden;">
		<span id='textForDistrictSpecificQuestions' class="divlablevalins"><spring:message code="msgDistReqAllAnsOfQues" /></span>
			<div class='divErrorMsg' id='errordiv4JSIquestion' style="display: block;padding-bottom: 7px;"></div>
			<table width="100%" border="0" style="border: 1px solid #cccccc;"  class="table table-bordered table-striped" id="tblJSIGrid">
			</table>
	 	</div>
	 	<div class="modal-footer">
	 	<c:if test="${jobForTeacher.jobForTeacherId>0}">
			<input type="hidden" id="isApplied" name="isApplied"/>
		</c:if>
		
		<input type="hidden" id="JSIjobId" name="JSIjobId"/>
		<button class="btn btn-large btn-primary" type="button" onclick="setJSIDistrictQuestions();" ><strong><spring:message code="btnConti" /> <i class="icon"></i></strong></button>
			
				
							
	 		<button class="btn" data-dismiss="modal" aria-hidden="true" ><spring:message code="btnClr" /></button> 		
	 	</div>
</div>
</div>
</div>


<div  class="modal hide"  id="SuccessMessageforJSI" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
		<div class="modal-dialog" style="width:400px;">
		<div class="modal-content">
		<div class="modal-header">
	  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
			<h3 id="myModalLabel">TeacherMatch</h3>
		</div>
		<div class="modal-body" style="max-height: 450px;overflow-y: auto;overflow-x: hidden;">	
				
			<table width="100%" id="SuccessMesageforJSI">
				<tbody>
					<tr>								
						<td width="35%" style="padding-left: 10px;">
							<div class="control-group" id="message2showConfirm1JSI"><spring:message code="msgJobSpecificInventoryupdatedsuccessfully" /></div>
						</td>
					</tr>
				</tbody>
			</table>	
			
			
	 	</div>
	 	<div class="modal-footer">							
	 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnOk" /></button> 		
	 	</div>
</div>
</div>
</div>

<!-- 
<div  class="modalTrans hide"  id="myModalDASpecificQuestions" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
		<div class="modal-header">
	  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
			<h3 id="myModalLabel">Required Application Items</h3>
		</div>
		<div class="modal-body" style="height: 450px;">
		<span id='textForDistrictSpecificQuestions' class="divlablevalins">The District requires all the applicants to answer the following questions. If you have already answered them
				previously and there is no change and/or updates to the previous response,
				please feel free to continue to the Next screen. However, if the information
				has changed from the last time you responded to these questions, then please
				update it accordingly.</span>
			<div class='divErrorMsg' id='errordiv4question' style="display: block;padding-bottom: 7px;"></div>
			<table width="100%" border="0" class="table table-bordered table-striped" id="tblGrid">
			</table>
	 	</div>
	 	<div class="modal-footer">
	 		<button class="btn btn-large btn-primary" type="button" onclick="setDistrictQuestions('dashboard');" ><strong>Continue <i class="icon"></i></strong></button>
	 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="cancelDSQdiv()">Close</button> 		
	 	</div>
</div>
 -->

 <script type="text/javascript">//<![CDATA[
 
     cal.manageFields("examDate", "examDate", "%m-%d-%Y");
     cal.manageFields("generalKnowledgeExamDate", "generalKnowledgeExamDate", "%m-%d-%Y");
     cal.manageFields("rtDate", "rtDate", "%m-%d-%Y");
     cal.manageFields("rwdDate", "rwdDate", "%m-%d-%Y");
     cal.manageFields("wdDate", "wdDate", "%m-%d-%Y");
     cal.manageFields("fromStdTch", "fromStdTch", "%m-%d-%Y");
	 cal.manageFields("toStdTch", "toStdTch", "%m-%d-%Y");
	 cal.manageFields("residencyFromDate", "residencyFromDate", "%m-%d-%Y");
     cal.manageFields("residencyToDate", "residencyToDate", "%m-%d-%Y");
      
     dspqCalInstance=cal;
     
   
</script>

<script>

changeCityStateByZipForDSPQ();
changeCityStateByZipForDSPQPr();
</script>
<iframe src="" id="ifrmResume" width="100%" height="480px" style="display: none;"></iframe>
<iframe src="" id="ifrmBackgroundCheck" width="100%" height="480px" style="display: none;"></iframe>
<iframe src="" id="ifrmTrans" width="100%" height="480px" style="display: none;"></iframe>
<iframe src="" id="ifrmCert" width="100%" height="480px" style="display: none;"></iframe>
<iframe src="" id="ifrmRef" width="100%" height="480px" style="display: none;"></iframe> 	
<!-- End ... call Dynamic Portfolio -->

<input type="hidden" id="tFA_config" value="">
<input type="hidden" id="wst_config" value="">
<input type="hidden" id="resume_config" value="">
<input type="hidden" id="backgroundCheck_config" value="">
<input type="hidden" id="phone_config" value="">
<input type="hidden" id="address_config" value="">
<input type="hidden" id="exp_config" value="">
<input type="hidden" id="nbc_config" value="">
<input type="hidden" id="affidavit_config" value="">
<input type="hidden" id="personalinfo_config" value="">
<input type="hidden" id="dateOfBirth_config" value="">
<input type="hidden" id="ssn_config" value="">
<input type="hidden" id="SIN_config" value="">
<input type="hidden" id="race_config" value="">
<input type="hidden" id="formeremployee_config" value="">
<input type="hidden" id="generalKnowledge_config" value="">
<input type="hidden" id="subjectAreaExam_config" value="">
<input type="hidden" id="additionalDocuments_config" value="">
<input type="hidden" id="academicTranscriptFlag" value="">
<input type="hidden" id="norecord" value="">


<c:if test="${districtMaster ne null}">
	<input type="hidden" id="districtIdForDSPQ" value="${districtMaster.districtId}">
</c:if>
<input type="hidden" id="jobCategoryName" value="${jobOrder.jobCategoryMaster.jobCategoryName}">
<c:if test="${districtMaster eq null}">
	<input type="hidden" id="districtIdForDSPQ" > 
</c:if>
<!--<input type="hidden" id="districtIdForDSPQ" value="5513170"> -->
<input type="hidden" id="teacherIdForDSPQ" value="">
<input type="hidden" id="allowNext" value="1">
<input type="hidden" id="veteran_config" value="">
<input type="hidden" id="additionDocumentId" value="">
<input type="hidden" id="ethnicOrigin_config" value="">
<input type="hidden" id="ethinicity_config" value="">
<input type="hidden" id="employment_config" value="">
<input type="hidden" id="yaxis" value="0">
<input type="hidden" id="gender_config" value="">
<input type="hidden" id="retireNo_config" value="">
<input type="hidden" id="videoLink_config" value="">
<input type="hidden" id="involvement_config" value="">
<input type="hidden" id="honors_config" value="">
<input type="hidden" id="displayGKAndSubject" value="">

<input type="hidden" id="displayPassFailGK" value="">
<input type="hidden" id="displayPassFailSubject" value="">
<input type="hidden" id="IsSIForMiami" value="">
<input type="hidden" id="isItvtForMiami" value="">

<input type="hidden" id="countryCheck" value="">
<input type="hidden" id="countryCheckPr" value="">

<input type="hidden" id="threadCount" value="0">
<input type="hidden" id="returnThreadCount" value="0">

<input type="hidden" id="callForwardCount" value="-2">

<input type="hidden" id="threadCount_aca" value="0">
<input type="hidden" id="threadCount_cert" value="0">
<input type="hidden" id="threadCount_emp" value="0">
<input type="hidden" id="threadCount_ref" value="0">
<input type="hidden" id="threadCount_subarea" value="0">
<input type="hidden" id="threadCount_adddoc" value="0">
<input type="hidden" id="dSPQuestions_config" value="">
<input type="hidden" id="proofOfCertificationReq" value="0">

<!-- Dynamic optional fields -->
<input type="hidden" id="tfaOptional" value="">
<input type="hidden" id="nationalBoardOptional" value="">
<input type="hidden" id="certfiedTeachingExpOptional" value="">
<input type="hidden" id="substituteOptional" value="">
<input type="hidden" id="videoLinkOptional" value="">
<input type="hidden" id="gpaOptional" value="">
<input type="hidden" id="empSecSalaryOptional" value="">
<input type="hidden" id="empSecRoleOptional" value="">
<input type="hidden" id="empSecPrirOptional" value="">
<input type="hidden" id="empSecMscrOptional" value="">
<input type="hidden" id="eEocOptional" value="">
<input type="hidden" id="ressumeOptional" value="">
<input type="hidden" id="addressOptional" value="">
<input type="hidden" id="expectedSalarySection" value="">
<input type="hidden" id="certificationUrl" value="">
<input type="hidden" id="certificationDoeNumber" value="">
<input type="hidden" id="ssnOptional" value="">
<input type="hidden" id="academicsDatesOptional">
<input type="hidden" id="empDatesOptional">
<input type="hidden" id="certiDatesOptional">
<input type="hidden" id="certiGrades">
<input type="hidden" id="empSecReasonForLeavOptional">
<input type="hidden" id="transcriptOptional">
<input type="hidden" id="backgroundCheckDocumentId" value="">

<input type="hidden" id="degreeOptional">
<input type="hidden" id="schoolOptional">
<input type="hidden" id="fieldOfStudyOptional">
<input type="hidden" id="empPositionOptional">
<input type="hidden" id="empOrganizationOptional">
<input type="hidden" id="empCityOptional">
<input type="hidden" id="empStateOptional">
<input type="hidden" id="licenseLetterOptional">

<c:if test="{not empty isMiami}"> 
<script language="javascript">
document.getElementById("isMiami").value=${isMiami};
try{
<c:if test="${not empty districtMasterDSPQ.districtId}">
document.getElementById("districtIdForDSPQ").value=${districtMasterDSPQ.districtId};
</c:if>
}catch(ee){}
</script>
</c:if>
<c:if test="{not empty districtMasterDSPQ.districtId}"> 
<script language="javascript">
try{
document.getElementById("districtIdForDSPQ").value=${districtMasterDSPQ.districtId};
}catch(ee){}
</script>
</c:if>
<c:if test="{not empty teacherIdForDSPQ.teacherId}">
<script language="javascript">
try{document.getElementById("teacherIdForDSPQ").value=${teacherIdForDSPQ.teacherId};}catch(ee){}
</script>
</c:if>
<script type="text/javascript">
//getCountry();
getStateByCountryForDspq("dspq");
getStateByCountryForDspqPr("dspq");
getRacedata();
function manageRaces(dis,flg)
{
var checkBox = document.getElementsByName("raceId");
	if(dis.value=="6" && dis.checked==true)
	{
		for(i=0;i<checkBox.length;i++)
			checkBox[i].checked=flg;
			
		dis.checked=true;
	}else
	{
		 for(i=0;i<checkBox.length;i++)
		 {	
		 	if(checkBox[i].value=="6" && dis.checked==true)
		 	{
		 		checkBox[i].checked=false;
		 		break;
		 	}
		 }
	}
}
$('#transUploadTooltip').tooltip();
$('#destool').tooltip();
$('#degreeTooltip').tooltip();
$('#prrtool').tooltip();
$('#clsToolTip').tooltip();
$('#iconpophoverppdi').tooltip();
$('#iconpophoverAnotherName').tooltip();
$('.calculatortooltip').tooltip();
$("#ssubtTooltip").tooltip();
$("#iconpophoveriein").tooltip();
$("#certificationtypeMasterTool").tooltip();
$("#proofCertTooltip").tooltip();
$("#academicHelpTooltip").tooltip();
$("#hrefOctUpload").tooltip();
$("#referenceToolTip").tooltip();
</script>

<script><!--
function fileContainsVirusDiv(msg)
{
	$('#loadingDivWait').hide();
	$('#topErrorMessageDSPQ').hide();
	$('#virusDivId .modal-body').html(msg);
	$('#myModalDymanicPortfolio').modal('hide');
	$('#virusDivId').modal('show');
}

$(document).mouseup(function (e)
{
    var container = $(".DynarchCalendar-topCont");

    if (!container.is(e.target) // if the target of the click isn't the container...
        && container.has(e.target).length === 0) // ... nor a descendant of the container
    {
        container.hide();
    }
});  

--></script>
<!-- sandeep -->

<script type="text/javascript">//<![CDATA[
	  var cal = Calendar.setup({
          onSelect: function(cal) { cal.hide() },
          showTime: true
      });
     cal.manageFields("graduationDate", "graduationDate", "%m-%d-%Y");
    
</script>
<!-- sandeep -->

<div class="modal hide" id="virusDivId" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog-for-cgcoverletter" style='width: 35%;'>
		<div class="modal-content">
			<div class="modal-header">
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body" style="max-height: 400px;overflow-y:auto;">
				<div class="control-group">
				</div>
			</div>
			<div class="modal-footer">
				<span id=""><button class="btn btn-primary" data-dismiss="modal" onclick="showDSPQDiv();"><spring:message code="btnOk" /> <i class="icon"></i></button></span>&nbsp;&nbsp;
			</div>
		</div>
	</div>
</div>
<script><!--
function ssnNotFound(msg)
{
	$('#loadingDiv').hide();
	$('#ssnDivId .modal-body').html(msg);
	$('#ssnDivId').modal('show');
}

function ssnButtonNotClick(msg)
{
	$('#loadingDiv').show();
	$('#ssnDivIdRequired .modal-body').html(msg);
	$('#ssnDivIdRequired').modal('show');
}

function formerEmployeeForNc(msg)
{
	$('#loadingDiv').show();
	$('#formerEmployeeNC .modal-body').html(msg);
	$('#formerEmployeeNC').modal('show');
}
var ssnNumber="";
function showEditSSn(ssnNumber)
{
      var ssnValue = ssnNumber;
      if(ssnValue!=null && ssnValue!='')
      {     
    	$('#last4snnfornc').show(); 
		$('#4ssnlast').hide(); 
				
		var x = document.getElementById("ssn_pi");
		x.setAttribute("type", "hidden");        
        }        
        else
        { 
         var x = document.getElementById("ssn_pi");      
        $('#last4snnfornc').hide(); 
        $('#4ssnlast').show();         
        x.setAttribute("type", "text");
        }        
}

var abc="";
function showFullSSn()
{ 
ssnNumber=document.getElementById("ssn_pi").value;
document.getElementById("BlankSSNvalue").value=ssnNumber;
$('#last4snnfornc').hide(); 
$('#4ssnlast').show(); 
var x = document.getElementById("ssn_pi");
x.setAttribute("type", "text");
document.getElementById("ssn_pi").focus();
}

if(document.location.hostname=="nccloud.teachermatch.org" || document.location.hostname=="nc.teachermatch.org")
{
var ssnbuttonClick=false;
var hrefclick=true;
$(document).ready(function(){
	$("#refrenceMsgForNC").show();
$(function() {
	$('#myModalDymanicPortfolio').click(function(){
	
		$('#submitSSN').click(function(){
		var ssn = document.getElementById("ssn_pi").value;
		if(ssn.length==9)
		{
		ssnNumber=document.getElementById("ssn_pi").value;  
		}
		if(ssn=='')
		{
		ssnNumber="";
		 ssnbuttonClick=true;		 
		}
		showForm_LicenceFromSubmit();
			return true;
		});
		
		$('#dynamicSaveDspq').click(function(){	
		if(ssnNumber!=null && ssnNumber!='')
		{
		    var aftrsnn=ssnNumber.slice(-4);		
			document.getElementById("ssn_pifornc").innerHTML=aftrsnn;
			document.getElementById("ssn_pi").value=ssnNumber;			 
			}			
			return true;
		});
		$("#ssn_pi").blur(function() {            
		     abc="1";
              });
			
	   var ssnBlank=	document.getElementById("ssn_pi").value;	  
		if(ssnBlank!=null && ssnBlank!='' && ssnNumber!=null && ssnNumber!='')	
		$('#ssn_pi').val(ssnNumber);
		
		if(ssnBlank=='' && ssnbuttonClick)
		{		
		ssnbuttonClick=false;	
		}
		else if(ssnBlank=='')
		{						
		document.getElementById("ssn_pi").value=ssnNumber;	
		showEditSSn(ssnNumber);	
		ssnbuttonClick=false;	
		}
		else if(ssnBlank!=ssnNumber && ssnNumber!='')
		{		
		document.getElementById("ssn_pi").value=ssnNumber;	
		showEditSSn(ssnNumber);	
		ssnbuttonClick=false;
		}
		else
		{			
		if(abc=='1' && ssnNumber==ssnBlank)
		{			
		//alert("sdfsdfds");
		showEditSSn(ssnNumber);	
		}
		else
		{	
		$('#hrefClick').click(function(){
		hrefclick=false;		
		});			
		if(hrefclick && ssnNumber=='')
		{		
		//document.getElementById("ssn_pi").value=ssnNumber;		
		}	
		}			
		}			
		abc="";	
		
	});
});
});
}


//FOR SPAECIFIC DISTRICT YORK 01 
function showEditSSnYORK()
{
      var ssnValue = document.getElementById("ssn_pi").value;     
      if(ssnValue!=null && ssnValue!='')
      {
       var aftrsnn=ssnValue.slice(-4);
     	 document.getElementById("ssn_piforYor").innerHTML=aftrsnn;
      	 ssnNumber=document.getElementById("ssn_pi").value;
         $('#ssn_pi').val(ssnValue);
    	$('#last4snnforyork').show(); 
		$('#4ssnlast').hide(); 
				
		var x = document.getElementById("ssn_pi");
		x.setAttribute("type", "hidden");        
        }        
        else
        { 
         var x = document.getElementById("ssn_pi");      
        $('#last4snnforyork').hide(); 
        $('#4ssnlast').show();         
        x.setAttribute("type", "text");
        }        
}
//FOR SPAECIFIC DISTRICT YORK 01
function showFullSSnYork()
{ 
ssnNumber=document.getElementById("ssn_pi").value;
$('#last4snnforyork').hide(); 
$('#4ssnlast').show(); 
var x = document.getElementById("ssn_pi");
x.setAttribute("type", "text");
document.getElementById("ssn_pi").focus();
}
//FOR SPAECIFIC DISTRICT YORK 01 
if(document.getElementById("districtIdForDSPQ").value==4503810)
{
$(document).ready(function(){
$(function() {
	var ssnBlank=	document.getElementById("ssn_pi").value;	
		if(ssnBlank!=null && ssnBlank!='' && ssnNumber!=null && ssnNumber!='')	
		$('#ssn_pi').val(ssnNumber);
	});
});
}
</script>
<input type="hidden" id="tIDD" name="tIDD"  value="${teacherIdForDSPQ.teacherId}">
<div class="modal hide" id="ssnDivId" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog-for-cgcoverletter" style='width: 35%;'>
		<div class="modal-content">
			<div class="modal-header">
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body" style="max-height: 400px;overflow-y:auto;">
				<div class="control-group">
				</div>
			</div>
			<div class="modal-footer">
				<span id=""><button class="btn btn-primary" data-dismiss="modal" onclick="$('#ssn_pi').focus();">Ok <i class="icon"></i></button></span>&nbsp;&nbsp;
			</div>
		</div>
	</div>
</div>

<input type="hidden" name="continueToOkCurrentEmp" id="continueToOkCurrentEmp" value="0">

<div class="modal hide" id="ssnDivIdRequired" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog-for-cgcoverletter" style='width: 35%;'>
		<div class="modal-content">
			<div class="modal-header">
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body" style="max-height: 400px;overflow-y:auto;">
				<div class="control-group">
				</div>
			</div>
			<div class="modal-footer">
				<span id=""><button class="btn btn-primary" data-dismiss="modal" onclick="continueToOkCurrentEmp();">Ok <i class="icon"></i></button></span>&nbsp;&nbsp;
			</div>
		</div>
	</div>
</div>

<div class="modal hide" id="formerEmployeeNC" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog-for-cgcoverletter" style='width: 35%;'>
		<div class="modal-content">
			<div class="modal-header">
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body" style="max-height: 400px;overflow-y:auto;">
				<div class="control-group">
				</div>
			</div>
			<div class="modal-footer">
				<span id=""><button class="btn btn-primary" data-dismiss="modal" onclick="javascript:void(0);">Ok <i class="icon"></i></button></span>&nbsp;&nbsp;
			</div>
		</div>
	</div>
</div>
