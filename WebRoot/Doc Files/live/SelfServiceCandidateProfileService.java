package tm.service.profile;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NavigableSet;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import tm.bean.JobOrder;
import tm.dao.JobCategoryTransactionDAO;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.ReferenceNoteDAO;
import tm.dao.SchoolInJobOrderDAO;
import tm.dao.TeacherAdditionalDocumentsDAO;
import tm.dao.TeacherAchievementScoreDAO;
import tm.dao.TeacherAssessmentStatusDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.TeacherElectronicReferencesDAO;
import tm.dao.TeacherExperienceDAO;
import tm.dao.TeacherGeneralKnowledgeExamDAO;
import tm.dao.TeacherNormScoreDAO;
import tm.dao.TeacherPersonalInfoDAO;
import tm.dao.TeacherProfileVisitHistoryDAO;
import tm.dao.TeacherVideoLinksDAO;
import tm.dao.assessment.AssessmentJobRelationDAO;
import tm.dao.cgreport.JobWiseConsolidatedTeacherScoreDAO;
import tm.dao.master.CityMasterDAO;
import tm.dao.master.CountryMasterDAO;
import tm.dao.master.DemoClassScheduleDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.DistrictSpecificPortfolioAnswersDAO;
import tm.dao.master.DistrictSpecificPortfolioQuestionsDAO;
import tm.dao.master.DspqFieldMasterDAO;
import tm.dao.master.DspqGroupMasterDAO;
import tm.dao.master.DspqPortfolioNameDAO;
import tm.dao.master.DspqRouterDAO;
import tm.dao.master.DspqSectionMasterDAO;
import tm.dao.master.EthinicityMasterDAO;
import tm.dao.master.EthnicOriginMasterDAO;
import tm.dao.master.GenderMasterDAO;
import tm.dao.master.JobCategoryMasterDAO;
import tm.dao.master.RaceMasterDAO;
import tm.dao.master.StateMasterDAO;
import tm.dao.master.StatusMasterDAO;
import tm.dao.master.TFAAffiliateMasterDAO;
import tm.dao.master.TFARegionMasterDAO;
import tm.dao.menu.RoleAccessPermissionDAO;
import tm.dao.teacher.TeacherLanguagesDAO;
import tm.service.jobapplication.JobApplicationServiceAjax;
import tm.services.CommonDashboardAjax;
import tm.services.PaginationAndSorting;
import tm.services.report.CandidateGridService;
import tm.utility.Utility;
import tm.bean.JobCategoryTransaction;
import tm.bean.JobForTeacher;
import tm.bean.ReferenceNote;
import tm.bean.SchoolInJobOrder;
import tm.bean.TeacherAchievementScore;
import tm.bean.TeacherAssessmentStatus;
import tm.bean.TeacherDetail;
import tm.bean.TeacherElectronicReferences;
import tm.bean.TeacherExperience;
import tm.bean.TeacherGeneralKnowledgeExam;
import tm.bean.TeacherNormScore;
import tm.bean.TeacherPersonalInfo;
import tm.bean.TeacherProfileVisitHistory;
import tm.bean.TeacherVideoLink;
import tm.bean.assessment.AssessmentJobRelation;
import tm.bean.cgreport.JobWiseConsolidatedTeacherScore;
import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.CityMaster;
import tm.bean.master.CountryMaster;
import tm.bean.master.DemoClassSchedule;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictSpecificPortfolioAnswers;
import tm.bean.master.DistrictSpecificPortfolioOptions;
import tm.bean.master.DistrictSpecificPortfolioQuestions;
import tm.bean.master.DspqFieldMaster;
import tm.bean.master.DspqGroupMaster;
import tm.bean.master.DspqPortfolioName;
import tm.bean.master.DspqRouter;
import tm.bean.master.DspqSectionMaster;
import tm.bean.master.EthinicityMaster;
import tm.bean.master.EthnicOriginMaster;
import tm.bean.master.GenderMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.RaceMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.StateMaster;
import tm.bean.master.TFAAffiliateMaster;
import tm.bean.master.TFARegionMaster;
import tm.bean.user.UserMaster;


public class SelfServiceCandidateProfileService {
	
		String locale = Utility.getValueOfPropByKey("locale");
		String lblName  = Utility.getLocaleValuePropByKey("lblName", locale);
		String lblNA   = Utility.getLocaleValuePropByKey("lblNA ", locale);
		String lblAname  = Utility.getLocaleValuePropByKey("lblAname", locale);
		String lblAdd  = Utility.getLocaleValuePropByKey("lblAdd", locale);
		String  lblVeteran   = Utility.getLocaleValuePropByKey("lblVeteran", locale);
		String lblSSN  = Utility.getLocaleValuePropByKey("lblSSN", locale);
		String lblYes  = Utility.getLocaleValuePropByKey("lblYes", locale);
		String lblNo  = Utility.getLocaleValuePropByKey("lblNo", locale);
		String lblEmpNum  = Utility.getLocaleValuePropByKey("lblEmpNum", locale);
		String lblEmpPosition  = Utility.getLocaleValuePropByKey("lblEmpPosition", locale);
		String lblFirstJobApplied   = Utility.getLocaleValuePropByKey("lblFirstJobApplied", locale);
		String lblLastJobApplied   = Utility.getLocaleValuePropByKey("lblLastJobApplied", locale);
		String lblLastContacted   = Utility.getLocaleValuePropByKey("lblLastContacted", locale);
		String lblJobApplied   = Utility.getLocaleValuePropByKey("lblJobApplied", locale);
		String lblNatiBoardCerti  = Utility.getLocaleValuePropByKey("lblNatiBoardCerti/Lice", locale);
		String lblY   = Utility.getLocaleValuePropByKey("lblY", locale);
		String lblN   = Utility.getLocaleValuePropByKey("lblN", locale);
		String lblFitScore  = Utility.getLocaleValuePropByKey("lblFitScore", locale);
		String lblTFA  = Utility.getLocaleValuePropByKey("lblTFA", locale);
		String btnSave  = Utility.getLocaleValuePropByKey("btnSave", locale);
		String btnClr  = Utility.getLocaleValuePropByKey("btnClr", locale);
		String lblEdit  = Utility.getLocaleValuePropByKey("lblEdit", locale);
		String lblCorpsYear  = Utility.getLocaleValuePropByKey("lblCorpsYear", locale);
		String lblTFARegion   = Utility.getLocaleValuePropByKey("lblTFARegion", locale);
		String lblDemoLesson  = Utility.getLocaleValuePropByKey("lblDemoLesson", locale);
		String lblTeachingYears  = Utility.getLocaleValuePropByKey("lblTeachingYears", locale);
		String lblExpectedSalary  = Utility.getLocaleValuePropByKey("lblExpectedSalary", locale);
		String lblOfViews   = Utility.getLocaleValuePropByKey("lblOfViews", locale);
		String lblClickToViewTPVisit   = Utility.getLocaleValuePropByKey("lblClickToViewTPVisit", locale);
		String lblWillingToSubsti  = Utility.getLocaleValuePropByKey("lblWillingToSubsti", locale);
		String lnkResume  = Utility.getLocaleValuePropByKey("lnkResume", locale);
		String lblNotAvailable   = Utility.getLocaleValuePropByKey("lblNotAvailable", locale);
		String headPdRep  = Utility.getLocaleValuePropByKey("headPdRep", locale);
		String lblCandidateNotCompletedJSI  = Utility.getLocaleValuePropByKey("lblCandidateNotCompletedJSI", locale);
		String lbllblCandidateTimedOut  = Utility.getLocaleValuePropByKey("lbllblCandidateTimedOut", locale);
		String lblMobileNumber   = Utility.getLocaleValuePropByKey("lblMobileNumber", locale);
		String lblAScore  = Utility.getLocaleValuePropByKey("lblAScore", locale);
		String lblLRScore  = Utility.getLocaleValuePropByKey("lblL/RScore", locale);
		String lblAcademic  = Utility.getLocaleValuePropByKey("lblAcademic", locale);
		String lblAchievement   = Utility.getLocaleValuePropByKey("lblAchievement", locale);
		String lblLeadershipResults   = Utility.getLocaleValuePropByKey("lblLeadershipResults", locale);
		String lblSetAScore   = Utility.getLocaleValuePropByKey("lblSetAScore", locale);
		String lblPNQ   = Utility.getLocaleValuePropByKey("lblPNQ", locale);
		String headCom  = Utility.getLocaleValuePropByKey("headCom", locale);
		String lblShare  = Utility.getLocaleValuePropByKey("lblShare", locale);
		String headTag  = Utility.getLocaleValuePropByKey("headTag", locale);
		String lblGeneralKnowledgeExam   = Utility.getLocaleValuePropByKey("lblGeneralKnowledgeExam", locale);
		String lblExamStatus   = Utility.getLocaleValuePropByKey("lblExamStatus", locale);
		String lblExamDate   = Utility.getLocaleValuePropByKey("lblExamDate", locale);
		String lblScoreReport   = Utility.getLocaleValuePropByKey("lblScoreReport", locale);
		String lblPass   = Utility.getLocaleValuePropByKey("lblPass", locale);
		String lblFail   = Utility.getLocaleValuePropByKey("lblFail", locale);
		String lblSubjectAreaExam   = Utility.getLocaleValuePropByKey("lblSubjectAreaExam", locale);
		String lblCerti  = Utility.getLocaleValuePropByKey("lblCerti/Lice", locale);
		String lblAcademics  = Utility.getLocaleValuePropByKey("lblAcademics", locale);
		String lblHonors  = Utility.getLocaleValuePropByKey("lblHonors", locale);
		String lblInvolvementVolunteerWork   = Utility.getLocaleValuePropByKey("lblInvolvementVolunteerWork ", locale);
		String lblWorkExp  = Utility.getLocaleValuePropByKey("lblWorkExp", locale);
		String lblRef  = Utility.getLocaleValuePropByKey("lblRef", locale);
		String lblMrs  = Utility.getLocaleValuePropByKey("lblMrs", locale);
		String lblMr  = Utility.getLocaleValuePropByKey("lblMr", locale);
		String lblMiss   = Utility.getLocaleValuePropByKey("lblMiss", locale);
		String lblDr   = Utility.getLocaleValuePropByKey("lblDr", locale);
		String lblMs  = Utility.getLocaleValuePropByKey("lblMs", locale);
		String lblFname  = Utility.getLocaleValuePropByKey("lblFname", locale);
		String lblLname  = Utility.getLocaleValuePropByKey("lblLname", locale);
		String lblTitle  = Utility.getLocaleValuePropByKey("lblTitle", locale);
		String lblOrganizationEmp   = Utility.getLocaleValuePropByKey("lblOrganizationEmp", locale);
		String lblContNum   = Utility.getLocaleValuePropByKey("lblContNum", locale);
		String lblEmail  = Utility.getLocaleValuePropByKey("lblEmail", locale);
		String lblRecommendLetter   = Utility.getLocaleValuePropByKey("lblRecommendLetter", locale);
		String lnkClear  = Utility.getLocaleValuePropByKey("lnkClear", locale);
		String lnkRemo  = Utility.getLocaleValuePropByKey("lnkRemo", locale);
		String lblCanThisPerson   = Utility.getLocaleValuePropByKey("lblCanThisPerson ", locale);
		String lblHowLong   = Utility.getLocaleValuePropByKey("lblHowLong", locale);
		String lblReferenceDetails   = Utility.getLocaleValuePropByKey("lblReferenceDetails", locale);
		String lnkViLnk  = Utility.getLocaleValuePropByKey("lnkViLnk", locale);
		String lblVideo  = Utility.getLocaleValuePropByKey("lblVideo", locale);
		String lnkAddDocu  = Utility.getLocaleValuePropByKey("lnkAddDocu", locale);
		String lblPleaseInclude   = Utility.getLocaleValuePropByKey("lblPleaseInclude", locale);
		String lblSupportedVideoFormats   = Utility.getLocaleValuePropByKey("lblSupportedVideoFormats", locale);
		String lblLanguageProfiency   = Utility.getLocaleValuePropByKey("lblLanguageProfiency", locale);
		String lblAssessmentDetails   = Utility.getLocaleValuePropByKey("lblAssessmentDetails", locale);
		String headMngDistSpeciQues  = Utility.getLocaleValuePropByKey("headMngDistSpeciQues", locale);
		String lblSaveQuestion   = Utility.getLocaleValuePropByKey("lblSaveQuestion", locale);
		String lblNone  = Utility.getLocaleValuePropByKey("lblNone", locale);
		String lblEPINorm  = Utility.getLocaleValuePropByKey("lblEPINorm", locale);
		String lblJSI  = Utility.getLocaleValuePropByKey("lblJSI", locale);
		String lblPhone  = Utility.getLocaleValuePropByKey("lblPhone", locale);
		String lnkAddViLnk  = Utility.getLocaleValuePropByKey("lnkAddViLnk", locale);
		String lblPreviousEmployed = Utility.getLocaleValuePropByKey("lblPreviousEmployed", locale);
		String msgYrSesstionExp=Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale);
		String lblLicense=Utility.getLocaleValuePropByKey("lblLicense", locale);
		String lblLEACandidatePortfolio=Utility.getLocaleValuePropByKey("lblLEACandidatePortfolio", locale);
		String lblCertiLicense=Utility.getLocaleValuePropByKey("lblCertiLicense", locale);
		String lblNotes  = Utility.getLocaleValuePropByKey("lblNotes", locale);
		 String lblNoRecord=Utility.getLocaleValuePropByKey("lblNoRecord", locale);
		 String lblOrga=Utility.getLocaleValuePropByKey("lblOrga", locale);
		 String lblType=Utility.getLocaleValuePropByKey("lblType", locale);
		 String lblNumberofPeople=Utility.getLocaleValuePropByKey("lblNumberofPeople", locale);
		 String lblNumPeLed=Utility.getLocaleValuePropByKey("lblNumPeLed", locale);
		String lblDuration  = Utility.getLocaleValuePropByKey("lblDuration ", locale);
		String lblTypeOfRole  = Utility.getLocaleValuePropByKey("lblTypeOfRole", locale);
		String lblRole	 = Utility.getLocaleValuePropByKey("lblRole", locale);
		String lblPosition  = Utility.getLocaleValuePropByKey("lblPosition", locale);
		String lblOrgEmp  = Utility.getLocaleValuePropByKey("lblOrgEmp", locale);
		String lblCompanyName = Utility.getLocaleValuePropByKey("lblCompanyName", locale);
		String lblCompanyCity = Utility.getLocaleValuePropByKey("lblCompanyCity", locale);
		String lblCompanyState = Utility.getLocaleValuePropByKey("lblCompanyState", locale);				
		String lblAnnualSalary  = Utility.getLocaleValuePropByKey("lblAnnualSalary", locale);
		String lblPrimaryResponsibility  = Utility.getLocaleValuePropByKey("lblPrimaryResponsibility", locale);
		String lblMostSignificantContributions  = Utility.getLocaleValuePropByKey("lblMostSignificantContributions ", locale);
		String lblReasonForLeaving  = Utility.getLocaleValuePropByKey("lblReasonForLeaving ", locale);
		String lblNoData  = Utility.getLocaleValuePropByKey("lblNoData", locale);
		String lblCity = Utility.getLocaleValuePropByKey("lblCity", locale);
		String lblState = Utility.getLocaleValuePropByKey("lblState", locale);
		String lblMoreFields  = Utility.getLocaleValuePropByKey("lblMoreFields", locale);
		String lblReasons  = Utility.getLocaleValuePropByKey("lblReasons", locale);
		String lblMoreInfo  = Utility.getLocaleValuePropByKey("lblMoreInfo", locale);
		String lblStudentTeaching   = Utility.getLocaleValuePropByKey("lblStudentTeaching", locale);
		String lblFullTimeTeaching   = Utility.getLocaleValuePropByKey("lblFullTimeTeaching", locale);
		String lblSubstituteTeaching   = Utility.getLocaleValuePropByKey("lblSubstituteTeaching", locale);
		String lblOtherWorkExperience   = Utility.getLocaleValuePropByKey("lblOtherWorkExperience", locale);
		String lblToPresent   = Utility.getLocaleValuePropByKey("lblToPresent", locale);
		String lblResponsibility    = Utility.getLocaleValuePropByKey("lblResponsibility", locale);
		String lblContributions    = Utility.getLocaleValuePropByKey("lblContributions", locale);
		String lblReason    = Utility.getLocaleValuePropByKey("lblReason", locale);
		String lblActions = Utility.getLocaleValuePropByKey("lblActions", locale);
		String lblDeactivate = Utility.getLocaleValuePropByKey("lblDeactivate", locale);
        String lblActivate = Utility.getLocaleValuePropByKey("lblActivate", locale);
        String lblRefName  = Utility.getLocaleValuePropByKey("lblRefName ", locale);
        String lblRecLetter  = Utility.getLocaleValuePropByKey("lblRecLetter", locale);
        String lblContactNo  = Utility.getLocaleValuePropByKey("lblContactNo", locale);
        String lblCanContact  = Utility.getLocaleValuePropByKey("lblCanContact ", locale);
        String lblClickToViewLetter  = Utility.getLocaleValuePropByKey("lblClickToViewLetter", locale);
        String lblAddNotesToCandidate = Utility.getLocaleValuePropByKey("lblAddNotesToCandidate", locale);
        String lblSchool = Utility.getLocaleValuePropByKey("lblSchool", locale);
		String lblDatesAttended  = Utility.getLocaleValuePropByKey("lblDatesAttended", locale);
		String lblDegree  = Utility.getLocaleValuePropByKey("lblDegree", locale);
		String lblFieldOfStudy  = Utility.getLocaleValuePropByKey("lblFieldOfStudy", locale);
		String lblTranscript  = Utility.getLocaleValuePropByKey("lblTranscript", locale);
		String lblGPA  = Utility.getLocaleValuePropByKey("lblGPA", locale);
		String lblFreshman = Utility.getLocaleValuePropByKey("lblFreshman", locale);
		String lblSophomore  = Utility.getLocaleValuePropByKey("lblSophomore", locale);
		String lblJunior  = Utility.getLocaleValuePropByKey("lblJunior", locale);
		String lblSenior  = Utility.getLocaleValuePropByKey("lblSenior", locale);
		String lblCumulative  = Utility.getLocaleValuePropByKey("lblCumulative", locale);
		String LBLCandidateRefe=Utility.getLocaleValuePropByKey("LBLCandidateRefe", locale);
		String lblUserName=Utility.getLocaleValuePropByKey("lblUserName", locale);
		String lblVisitedDate1=Utility.getLocaleValuePropByKey("lblVisitedDate1", locale);
	 
	 public static int NC_HEADQUARTER=2;
	 
	@Autowired
	private JobOrderDAO jobOrderDAO;
	 
	@Autowired
	private DspqPortfolioNameDAO dspqPortfolioNameDAO;
	
	@Autowired
	private DspqRouterDAO dspqRouterDAO;
	
	@Autowired
	private DspqFieldMasterDAO dspqFieldMasterDAO;
	
	@Autowired
	private DspqSectionMasterDAO dspqSectionMasterDAO;
	
	@Autowired
	private StateMasterDAO stateMasterDAO;
	
	@Autowired
	private DspqGroupMasterDAO dspqGroupMasterDAO;
	
	@Autowired
	private CountryMasterDAO countryMasterDAO;
	
	@Autowired
	private TeacherPersonalInfoDAO teacherPersonalInfoDAO;
	
	@Autowired
	private CityMasterDAO cityMasterDAO;
	
	@Autowired
	private RaceMasterDAO raceMasterDAO;
	
	@Autowired
	private EthinicityMasterDAO ethinicityMasterDAO;
	
	@Autowired
	private EthnicOriginMasterDAO ethnicOriginMasterDAO;
	
	@Autowired
	private GenderMasterDAO genderMasterDAO;
	
	@Autowired
	private TeacherDetailDAO teacherDetailDAO;
	
	@Autowired
	private DistrictSpecificPortfolioQuestionsDAO districtSpecificPortfolioQuestionsDAO;
	
	@Autowired
	private DistrictSpecificPortfolioAnswersDAO districtSpecificPortfolioAnswersDAO;
	
	@Autowired
	private TeacherGeneralKnowledgeExamDAO teacherGeneralKnowledgeExamDAO;
	
	@Autowired
	private TeacherElectronicReferencesDAO teacherElectronicReferencesDAO;
	
	@Autowired
	private ReferenceNoteDAO referenceNoteDAO;
	
	@Autowired
	private TeacherVideoLinksDAO teacherVideoLinksDAO;
	
	@Autowired
	private TeacherLanguagesDAO teacherLanguagesDAO;
	
	@Autowired
	private SchoolInJobOrderDAO schoolInJobOrderDAO;
	
	@Autowired
	private StatusMasterDAO statusMasterDAO;
	
	@Autowired
	private DemoClassScheduleDAO demoClassScheduleDAO;
	
	@Autowired
	private TeacherProfileVisitHistoryDAO teacherProfileVisitHistoryDAO;
	
	@Autowired
	private TeacherNormScoreDAO teacherNormScoreDAO;
	
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	
	@Autowired
	private TeacherAdditionalDocumentsDAO teacherAdditionalDocumentsDAO;
	
	@Autowired
	private TeacherExperienceDAO teacherExperienceDAO;
	
	@Autowired
	private TeacherAssessmentStatusDAO teacherAssessmentStatusDAO;
	
	@Autowired
	private RoleAccessPermissionDAO roleAccessPermissionDAO;
	
	@Autowired
	private AssessmentJobRelationDAO assessmentJobRelationDAO;
	
	@Autowired
	private JobWiseConsolidatedTeacherScoreDAO jobWiseConsolidatedTeacherScoreDAO;
	
	@Autowired
	private TeacherAchievementScoreDAO teacherAchievementScoreDAO;
	
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	
	@Autowired
	private TFAAffiliateMasterDAO tfaAffiliateMasterDAO;
	
	@Autowired
	private TFARegionMasterDAO tfaRegionMasterDAO;
	
	@Autowired
	private JobCategoryMasterDAO jobCategoryMasterDAO;
	
	@Autowired
	private JobCategoryTransactionDAO jobCategoryTransactionDAO;
	
	@Autowired
	private JobApplicationServiceAjax jobApplicationServiceAjax;

	/*
	 * Create new method for candidate profile 
	 * according to the new applicant workflow of dspq	
	 */
	
	String sGridtableDisplayCSS="padding:10px;";
	String sGridLabelCss="padding:0px;padding-left:12px;padding-top:10px;color:black;";
	String allCustomQuestionIdSB="";
	public String  showProfileContentNewSSPF(Long jobForTeacherId,Integer teacherId,String jobId,Integer noOfRecordCheck,int fg,String portfoiloTypeAndIds,String displayType)
	{
		System.out.println(" >>>>>>>>>>>>>>>>>>>>>>>> showProfileContentNewSSPF  <<<<<<<<<<<<<<<<<<<<<");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		boolean editableStatus=false;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		UserMaster userMaster=null;
		  DistrictMaster districtMaster=null;
		  if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
		   throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		  }
		  else{
		   userMaster=(UserMaster)session.getAttribute("userMaster");
		   districtMaster=userMaster.getDistrictId();
		   if(districtMaster!=null){
			   if(userMaster!=null && (userMaster.getDistrictId()!=null && (userMaster.getDistrictId().getSelfServiceCustomQuestion()!=null && userMaster.getDistrictId().getSelfServiceCustomQuestion()==1))){
					editableStatus=true;
				}
		   }
		  }
		  StringBuilder sb = new StringBuilder();
			try {
			String realPath = Utility.getValueOfPropByKey("teacherRootPath");
			String webPath = Utility.getBaseURL(request);
			String sWidthFoeDivScroll="700";
			String imgPath="";
			TeacherDetail teacherDetail = null;
			try{
				teacherDetail = teacherDetailDAO.findById(teacherId,false,false);
				if(teacherDetail.getIdentityVerificationPicture()!=null){
					imgPath=CommonDashboardAjax.showPictureWeb(teacherDetail, realPath, webPath);
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			TeacherPersonalInfo teacherpersonalinfo = teacherPersonalInfoDAO.findTeacherPersonalInfoByTeacher(teacherDetail);
			Integer iSalutation=0;
			String Salution="";
			if(teacherpersonalinfo!=null && teacherpersonalinfo.getSalutation()!=null)
				iSalutation=teacherpersonalinfo.getSalutation();				
		
				if(iSalutation==4)
					Salution=Utility.getLocaleValuePropByKey("optDr", locale);						
				
				if(iSalutation==3)
					Salution=Utility.getLocaleValuePropByKey("optMiss", locale);
										
				if(iSalutation==2)
					Salution=Utility.getLocaleValuePropByKey("optMr", locale);
									
				if(iSalutation==1)
					Salution=Utility.getLocaleValuePropByKey("optMrs", locale);
										
				if(iSalutation==5)
					Salution=Utility.getLocaleValuePropByKey("optMs", locale);	
			//*******************************************************************************
			String	portfoiloTypeAndId="";
			Integer portfolioId=0;
			String globalPortfolioIds=null;
			String []portfolioTypeId=portfoiloTypeAndIds.split("#");
			String []portfolioIds = null;
			List<Integer> portfolioIdList=new ArrayList<Integer>();
			String sCandidateType=portfolioTypeId[0];
			String candidateType="";
			if(displayType.equalsIgnoreCase("cg") || displayType.equalsIgnoreCase("My Folders") || displayType.equalsIgnoreCase("Mosaic")){
				candidateType=validateApplicantType(sCandidateType);
				portfolioId=Utility.getIntValue(portfolioTypeId[1]);
				globalPortfolioIds=portfolioTypeId[1];
			}else if(displayType.equalsIgnoreCase("pool")){
				candidateType="";
				if(userMaster.getEntityType()==1){
					try{
					if(session!=null && !((session.getAttribute("multiplePortfolioIds")!=null) || (session.getAttribute("multiplePortfolioIds")!=""))){
						globalPortfolioIds=session.getAttribute("multiplePortfolioIds").toString();
					portfolioIds=globalPortfolioIds.split(",");
					for(String rec:portfolioIds){
						if(portfolioId==0){
							portfolioId=Utility.getIntValue(rec);
						portfolioIdList.add(Utility.getIntValue(rec));
						}else{
							portfolioIdList.add(Utility.getIntValue(rec));
						}
					}
				}else{
					portfolioId=Utility.getIntValue(portfolioTypeId[1]);
					globalPortfolioIds=portfolioTypeId[1];
				}
				}catch (Exception e) {
					portfolioId=Utility.getIntValue(portfolioTypeId[1]);
					globalPortfolioIds=portfolioTypeId[1];
				}
			 }
				else{
					portfolioId=Utility.getIntValue(portfolioTypeId[1]);
					globalPortfolioIds=portfolioTypeId[1];
				}
			}
			portfoiloTypeAndId=candidateType+"#"+portfolioId;
			List<DspqRouter> dspqRouterList=null;
			if(portfolioIds==null)
				dspqRouterList=dspqRouterDAO.getDspqSectionListByTypeAndId(portfolioId, candidateType);
			else
				dspqRouterList=dspqRouterDAO.getDspqSectionListByTypeAndId(portfolioIdList);
			
		    Map<Integer, DspqRouter> mapDspqRouterByFID=new HashMap<Integer, DspqRouter>();
			Map<Integer, DspqRouter> mapDspqRouterBySID=new HashMap<Integer, DspqRouter>();
			setSIDAndFIDInMapProfiler(mapDspqRouterByFID, mapDspqRouterBySID, dspqRouterList);
			List<String> listToolTips = new ArrayList<String>();
			List<String> listCalDated = new ArrayList<String>();
			//******************************************************************************
			sb.append("<input type='hidden' id='candidateProfileTeacherId' value='"+teacherId+"'/>");
			sb.append("<input type='hidden' id='candidateProfilePortfolioId' value='"+portfolioId+"'/>");
			sb.append("<input type='hidden' id='candidateProfileCandidateType' value='"+candidateType+"'/>");
			sb.append("<input type='hidden' id='gridNameFlag' name='gridNameFlag'/>");
			sb.append("<div class='modal-body scrollspy_profile' style='min-width:"+sWidthFoeDivScroll+"px;'>");
			sb.append("<div id='errorDiv' style='color:red;padding-left:60px;display:none;'></div>");
			//Candidate Name
			String sectionIds="";
			int sectionflag=0;
			sb.append("<div class='row'>");
	        String showImg="";
	        if(editableStatus)
	        	sb.append("<div class='col-md-9 col-sm-9'></div><div class='col-md-3 col-sm-3' style='text-align: right;'><button class='btn btn-primary' type='button' id='saveCustom' onclick='saveAndValidateCustomQuestion("+jobId+","+teacherId+");' style='display: inline-block;'>&nbsp;&nbsp;&nbsp;&nbsp;Save Custom Fields&nbsp;<i class='icon'></i>&nbsp;&nbsp;&nbsp;&nbsp;</button></div>");
	        
			showImg="<img src=\"images/nophoto.png\" align='left' class='circle' style='width: 48px;height: 48px;margin: 0em auto;' >";
			sb.append("<div class='col-md-1 col-sm-1' style='padding-right: 0px;'>"+showImg+"</div>");
			
			sb.append("<div class='col-md-11 col-sm-11'  style='padding-left: 0px;'>");
			sb.append("<div class='row'><div class='col-sm-12 col-md-12'><div class='span4 portfolio_Subheading'>Personal Information</div></div></div>");
			
			sb.append("<div class='row'>");			
			sb.append("<div class='col-md-2 col-sm-2 divprofile'>Name:</div>");
			sb.append("<div class='col-md-2 col-sm-2'>"+Salution+" "+ teacherDetail.getFirstName()+" "+teacherDetail.getLastName()+"</div>");
			sb.append("</div>");
			
			sb.append("</div>");
			sb.append("</div>");
			if(mapDspqRouterByFID!=null)
			{
				
				 DspqRouter dspqRouterPersonal=mapDspqRouterBySID.get(2);
				 if(dspqRouterPersonal!=null)
				{
					sb.append("<div class='hide' id='personalSection'></div>");
					sb.append("<script>personalInfoProfiler('"+jobId+"','"+portfoiloTypeAndId+"','"+teacherId+"');</script>");
				}
				//Method For Fixed Personal Information
				 //******************** Start fixed data ***************************
				 	 sb.append("<div id='personalFixedAreaDiv'></div>");
				 sb.append("<script>personalFixedArea('"+teacherId+"','"+jobId+"');</script>");
				//******************************************************************
				 	//Method For Custom Question (Personal Information)
				 sb.append("<div id='customQuestions'></div>");
				 sb.append("<script>customQuestion('"+jobId+"','"+globalPortfolioIds+"','"+candidateType+"','1','"+teacherId+"','"+displayType+"');</script>");
				 //sb.append(getCustomQuestion(jobId,portfolioId,candidateType,1,teacherDetail));
				
				//******************************************************************
				DspqRouter dspqRouterGeneralKnowledge=mapDspqRouterBySID.get(30);
				if(dspqRouterGeneralKnowledge!=null)
				{
					sb.append("<div class='hide' id='generalKnowledgeExamSection'></div>");
					sb.append("<script>generalKnowledgeExamSectionProfiler('"+teacherId+"','"+portfoiloTypeAndId+"','"+dspqRouterGeneralKnowledge.getDisplayName()+"');</script>");
				}
				
				DspqRouter dspqRouterAcademics=mapDspqRouterBySID.get(6);
				if(dspqRouterAcademics!=null)
				{
					//sb.append("<div class='pleft13 portfolio_Subheading'>"+dspqRouterAcademics.getDisplayName()+"</div>");
					sb.append("<div class='hide' id='academicsSection' onmouseover=\"setGridNameFlag('academics')\"></div>");
					sb.append("<script>academicsSectionProfiler('"+teacherId+"','"+portfoiloTypeAndId+"','"+dspqRouterAcademics.getDisplayName()+"');</script>");
				
					//Method For Custom Question (Academics)
					 sb.append("<div id='customQuestionsAcademics' class='left30'></div>");
					 sb.append("<script>customQuestionMaster('"+jobId+"','"+globalPortfolioIds+"','"+candidateType+"','2','6','"+teacherId+"','"+displayType+"');</script>");
					/*if(sectionflag==0){
						sectionIds+=6;
						sectionflag++;
					}else{
						sectionIds+="#"+6;
						sectionflag++;
					}*/
					//******************************************************************
				}
					//sb.append(getGroup3OpentextProfiler(jobId,portfolioId,candidateType,3,teacherDetail));
				sb.append("<div class='hide' id='openSectionCertification'></div>");
				sb.append("<script>openSectionCertificationLicense('"+jobId+"','"+portfolioId+"','"+candidateType+"','3','"+teacherDetail.getTeacherId()+"');</script>");
				
				DspqRouter dspqRouterCertificationLicense=mapDspqRouterBySID.get(8);
				if(dspqRouterCertificationLicense!=null){
				sb.append("<div class='hide' id='certificationLicenseDiv' onmouseover=\"setGridNameFlag('certification')\"></div>");
				sb.append("<script>certificationLicense('"+teacherId+"','"+portfoiloTypeAndId+"','"+dspqRouterCertificationLicense.getDisplayName()+"');</script>");
				
				//Method For Custom Question (Certification)
				 sb.append("<div id='customQuestionsCertification' class='left30'></div>");
				 sb.append("<script>customQuestionMaster('"+jobId+"','"+globalPortfolioIds+"','"+candidateType+"','3','8','"+teacherId+"','"+displayType+"');</script>");
				 /*if(sectionflag==0){
						sectionIds+=8;
						sectionflag++;
					}else{
						sectionIds+="#"+8;
						sectionflag++;
					}*/
				 //******************************************************************
				}
				DspqRouter dspqRouterReferenceGrid=mapDspqRouterBySID.get(9);
				if(dspqRouterReferenceGrid!=null){
				sb.append("<div class='hide' id='referenceGridDiv' onmouseover=\"setGridNameFlag('reference')\"></div>");
				sb.append("<script>referenceGrid('"+teacherId+"','"+portfoiloTypeAndId+"','"+dspqRouterReferenceGrid.getDisplayName()+"');</script>");
				
				//Method For Custom Question (Reference)
				 sb.append("<div id='customQuestionsReference' class='left30'></div>");
				 sb.append("<script>customQuestionMaster('"+jobId+"','"+globalPortfolioIds+"','"+candidateType+"','3','9','"+teacherId+"','"+displayType+"');</script>");
				 /*if(sectionflag==0){
						sectionIds+=9;
						sectionflag++;
					}else{
						sectionIds+="#"+9;
						sectionflag++;
					}*/
				 
				//***************   Add reference *************************************************
				
				String sSectionName="tooltipSection09";
				sb.append("<div class='hide left25' id='addReferenceDiv'>");
				sb.append("<div class='row'>"+
				"		        <div class='col-sm-12 col-md-12'>"+
				"		        <div class='divErrorMsg' id='errordivElectronicReferences' style='display: block;'></div>"+
				"		        </div>"+
				"	        </div>");
				
				sb.append("<div class='row'>"+
				"	    <div class='col-sm-12 col-md-12'>"+
				"			<div id='divDataElectronicReferences'></div>"+
				"		</div>"+
				"	</div>");
				
				
				sb.append("<div id='divMainForm_ref'>"+
				"		<iframe id='uploadFrameReferencesID' name='uploadFrameReferences' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'></iframe>"+
				"		<form id='frmElectronicReferences' name='frmElectronicReferences' enctype='multipart/form-data' method='post' target='uploadFrameReferences' action='fileuploadservletforreferences.do' >");
				//--------- Input Field start
				
				//Salutation
				if(mapDspqRouterByFID.get(45)!=null)
				{
					
					sb.append("<input type='hidden' id='elerefAutoId'>");
					
					DspqRouter dspqRouter=mapDspqRouterByFID.get(45);
					if(dspqRouter!=null)
					{
						String sInputControlId="salutation";
						sb.append("<div class='col-sm-3 col-md-3' style='padding-left:0px;'>"+
								"<label><strong>"+getLabelOrToolTipTagOrRequiredIconProfiler(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								"	<select class='form-control' id='"+sInputControlId+"' name='"+sInputControlId+"' "+getInputCustomeAttributeProfiler(dspqRouter)+" >"+
								"		<option value='0'></option> "+
								"		<option value='4'>"+Utility.getLocaleValuePropByKey("optDr", locale)+"</option>"+
								"		<option value='3'>"+Utility.getLocaleValuePropByKey("optMiss", locale)+"</option>"+
								"		<option value='2'>"+Utility.getLocaleValuePropByKey("optMr", locale)+"</option>"+
								"		<option value='1'>"+Utility.getLocaleValuePropByKey("optMrs", locale)+"</option>"+
								"		<option value='5'>"+Utility.getLocaleValuePropByKey("optMs", locale)+"</option>													"+
								"	</select>"+
								"</div>");
					}
					
				}
				
				if(mapDspqRouterByFID.get(46)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(46);
					if(dspqRouter!=null)
					{
						String sInputControlId="firstName";
						sb.append("<div class='col-sm-3 col-md-3'>"+
								"   <label><strong>"+getLabelOrToolTipTagOrRequiredIconProfiler(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								"	<input type='hidden' id='elerefAutoId'>"+
								"	<input type='text' id='"+sInputControlId+"' name='"+sInputControlId+"' "+getInputCustomeAttributeProfiler(dspqRouter)+" class='form-control' placeholder='' maxlength='50'>"+
								"</div>");
					}
				}
				//Last Name
				if(mapDspqRouterByFID.get(47)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(47);
					if(dspqRouter!=null)
					{
						String sInputControlId="lastName";
						sb.append("<div class='col-sm-3 col-md-3'>"+
								"   <label><strong>"+getLabelOrToolTipTagOrRequiredIconProfiler(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								"	<input type='text' id='"+sInputControlId+"' name='"+sInputControlId+"' "+getInputCustomeAttributeProfiler(dspqRouter)+" class='form-control' placeholder='' maxlength='50'>"+
								"</div>");
					}
				}
				//Tital/Designation
				if(mapDspqRouterByFID.get(48)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(48);
					if(dspqRouter!=null)
					{
						String sInputControlId="designation";
						sb.append("<div class='col-sm-3 col-md-3'>"+
								"   <label><strong>"+getLabelOrToolTipTagOrRequiredIconProfiler(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								"	<input type='text' id='"+sInputControlId+"' name='"+sInputControlId+"' "+getInputCustomeAttributeProfiler(dspqRouter)+" class='form-control' placeholder='' maxlength='50'>"+
								"</div>");
					}
					
				}
				
				//Organization/Emp
				if(mapDspqRouterByFID.get(49)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(49);
					if(dspqRouter!=null)
					{
						String sInputControlId="organization";
						sb.append("<div class='col-sm-6 col-md-6' style='padding-left:0px;'>"+
								"   <label><strong>"+getLabelOrToolTipTagOrRequiredIconProfiler(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								"	<input type='text' id='"+sInputControlId+"' name='"+sInputControlId+"' "+getInputCustomeAttributeProfiler(dspqRouter)+" class='form-control' placeholder='' maxlength='50'>"+
								"</div>");
					}
					
				}
				
				//Contact Number
				if(mapDspqRouterByFID.get(50)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(50);
					if(dspqRouter!=null)
					{
						String sInputControlId="contactnumber";
						sb.append("<div class='col-sm-3 col-md-3'>"+
								"   <label><strong>"+getLabelOrToolTipTagOrRequiredIconProfiler(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								"	<input type='text' id='"+sInputControlId+"' name='"+sInputControlId+"' "+getInputCustomeAttributeProfiler(dspqRouter)+" class='form-control' placeholder='' maxlength='50'>"+
								"</div>");
					}
					
				}
				
				//Email 
				if(mapDspqRouterByFID.get(51)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(51);
					if(dspqRouter!=null)
					{
						String sInputControlId="email";
						sb.append("<div class='col-sm-6 col-md-6' style='padding-left:0px;'>"+
								"   <label><strong>"+getLabelOrToolTipTagOrRequiredIconProfiler(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								"	<input type='text' id='"+sInputControlId+"' name='"+sInputControlId+"' "+getInputCustomeAttributeProfiler(dspqRouter)+" class='form-control' placeholder='' maxlength='50'>"+
								"</div>");
					}
					
				}
				//Recommendation Letter
				if(mapDspqRouterByFID.get(52)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(52);
					if(dspqRouter!=null)
					{
						String sInputControlId="pathOfReferenceFile";
						String sInputControlIdHidden="pathOfReference";
						sb.append("<div class='row top15'>"+
								"<div class='col-sm-4 col-md-4'>"+
								"		<label><strong>"+getLabelOrToolTipTagOrRequiredIconProfiler(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								"		<input type='hidden' name='sbtsource_ref' id='sbtsource_ref' value='0'/>"+
								"		<input id='"+sInputControlId+"' name='"+sInputControlId+"' "+getInputCustomeAttributeProfiler(dspqRouter)+" type='file' width='20px;'>"+
								"		<input type=hidden id='"+sInputControlIdHidden+"' value='0'/>"+
								"	    <a href='javascript:void(0)' onclick='clearReferences()'>"+Utility.getLocaleValuePropByKey("btnClr", locale)+"</a>"+
								" </div>"+
								"<div class='col-sm-3 col-md-3' id='removeref' name='removeref' style='display: none;'>"+
								"		<label>"+
								"			&nbsp;&nbsp;"+
								"		</label>"+
								"		<span id='divRefName'>"+
								"		</span>"+
								"		<a href='javascript:void(0)' onclick='removeReferences()'>"+Utility.getLocaleValuePropByKey("lnkRemo", locale)+"</a>&nbsp;&nbsp;&nbsp;"+
								"	</div>"+
								"</div>");
					}
				}
				
				// Can this person be directly contacted  
				if(mapDspqRouterByFID.get(53)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(53);
					if(dspqRouter!=null)
					{
						String sInputControlId="rdcontacted";
						sb.append("<input type='hidden' id='hdn_"+sInputControlId+"' "+getInputCustomeAttributeProfiler(dspqRouter)+" >");
						sb.append("<div class='nobleCssHide'>"+
								"    <div class='col-sm-5 col-md-5' style='padding-left:0px;'>"+
								"       <label><strong>"+getLabelOrToolTipTagOrRequiredIconProfiler(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								"	    <div class='' id='' style='height: 40px;'>"+
								"		   <div class='radio inline col-sm-1 col-md-1'>"+
								"			 <input type='radio' checked='checked' id='rdcontacted1' value='1'  name='"+sInputControlId+"' onchange='showAndHideCanContOnOffer(1)'> "+Utility.getLocaleValuePropByKey("lblYes", locale)+
								"		   </div>"+
								"		    </br>"+
								"		    <div class='radio inline col-sm-1 col-md-1' style='margin-left:20px;margin-top:-10px;'>"+
								"				<input type='radio' id='rdcontacted0' value='0' name='"+sInputControlId+"' onchange='showAndHideCanContOnOffer(1)'> No"+
								"		    </div>"+
								"	    </div>"+
								"    </div>"+
								"</div>");
					}
					
				}
				//How Long Do u Know
				if(mapDspqRouterByFID.get(115)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(115);
					if(dspqRouter!=null)
					{
						String sInputControlId="longHaveYouKnow";
						sb.append(" <div class='col-sm-5 col-md-5'>"+
								"       <label><strong>"+getLabelOrToolTipTagOrRequiredIconProfiler(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
								"		<input type='text' id='"+sInputControlId+"' name='"+sInputControlId+"' "+getInputCustomeAttributeProfiler(dspqRouter)+" class='form-control' placeholder=''>"+
								"	</div>");
					}
				}
				
				//New Field is not added
				if(mapDspqRouterByFID.get(116)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(116);
					if(dspqRouter!=null)
					{
						String sInputControlId="referenceDetailText";
					   sb.append("<div class='row'>"+
							"    <div class='col-sm-11 col-md-11'>"+
							"		<label><strong>"+getLabelOrToolTipTagOrRequiredIconProfiler(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
							"	    <div id='"+sInputControlId+"' "+getInputCustomeAttributeProfiler(dspqRouter)+"><textarea rows='8' style='width:100%;' id='txtArea_"+sInputControlId+"'></textarea></div>"+
							"    </div>"+
							"</div>");
					}
				}
				sb.append("<input type='hidden' id='teacherIdRefSS' value='"+teacherId+"'>");
				sb.append("</form>"+
						" 		<div class='row'>								"+
						"	    <div class='col-sm-11 col-md-11 top10 idone'>"+
						"			<a id='hrefDone' href='javascript:void(0);' style='cursor: pointer; margin-left: 0px;' onclick='return insertOrUpdateElectronicReferencesNew();' >"+Utility.getLocaleValuePropByKey("lnkImD", locale)+"</a>&nbsp;&nbsp;"+
						"			<a class='idone' style='cursor: pointer; margin-left: 0px; text-decoration:none;'	onclick='return hideElectronicReferencesFormSS()'>"+
										Utility.getLocaleValuePropByKey("lnkCancel", locale)+
						"			</a>"+
						"		</div>"+
						"		</div>"+
						"	</div>");
						
				//sb.append(getCustomFieldBySectionIdProfiler(districtMaster,candidateType,dspqGroupMaster3,dspqRouterReferenceGrid.getDspqSectionMaster(),listToolTips,sSectionName,listCalDated,dspqPortfolioName,teacherDetail));
				sb.append("</div>");
				//**********************************************************************************
				}
				DspqRouter dspqRouterVideoLinkGrid=mapDspqRouterBySID.get(10);
				if(dspqRouterVideoLinkGrid!=null){
					
				String sSectionName="tooltipSection10";
				sb.append("<div class='hide' id='videoLinkGridDiv' onmouseover=\"setGridNameFlag('videolinks')\"></div>");
				sb.append("<script>videoLinkGrid('"+teacherId+"','"+portfoiloTypeAndId+"','"+dspqRouterVideoLinkGrid.getDisplayName()+"','"+dspqRouterVideoLinkGrid.getInstructions()+"');</script>");
				
				//Method For Custom Question (VideoLink)
				 sb.append("<div id='customQuestionsVideoLink' class='left30'></div>");
				 sb.append("<script>customQuestionMaster('"+jobId+"','"+globalPortfolioIds+"','"+candidateType+"','3','10','"+teacherId+"','"+displayType+"');</script>");
				 /*if(sectionflag==0){
						sectionIds+=10;
						sectionflag++;
					}else{
						sectionIds+="#"+10;
						sectionflag++;
					}*/
				 //******************************************************************
				//*********** Add video link ***************************************************
				try{
				sb.append("<div class='left25'>");	
				sb.append("<div class='hide portfolio_Section_ImputFormGap' id='divvideoLinks'>"+
						"			<iframe id='uploadFrameVideoID' name='uploadFrameVideo' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'></iframe>"+
						"			<div class='divErrorMsg' id='errordivvideoLinks' style='display: block;'></div>"+
						"			<form id='frmvideoLinks' name='frmvideoLinks' enctype='multipart/form-data' method='post' target='uploadFrameVideo' action='certificationVideoUploadServlet.do' >");
						
						sb.append("<input type='hidden' id='sbtsource_videoLink' name='sbtsource_videoLink' value='0'/>");
						//--------- Input Field start
						
						//Video Link
						if(mapDspqRouterByFID.get(54)!=null)
						{
							DspqRouter dspqRouter=mapDspqRouterByFID.get(54);
							if(dspqRouter!=null)
							{
								String sInputControlId="videourl";
								sb.append("<div class='row' style='width:102%;'>"+
										"	<div class='col-sm-12 col-md-12' style='padding-right:0px;'>"+
										"		<label><strong>"+getLabelOrToolTipTagOrRequiredIconProfiler(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
										"		<input type='hidden' id='videolinkAutoId' name='videolinkAutoId'>"+
										"		<input type='text' id='"+sInputControlId+"' name='"+sInputControlId+"' "+getInputCustomeAttributeProfiler(dspqRouter)+" class='form-control' placeholder='' maxlength='200'>"+
										"	</div>"+
										"</div>");
							}
						}
						//Video
						if(mapDspqRouterByFID.get(55)!=null)
						{
							DspqRouter dspqRouter=mapDspqRouterByFID.get(55);
							if(dspqRouter!=null)
							{
								String sInputControlId="videofile"; 
								String sInputControlIdHidden="dbvideofile"; 
								
								sb.append("<div class=''>"+
										"	<div class='col-sm-6 col-md-6' style='padding-left:0px;'>" +
										"		<label><strong>"+getLabelOrToolTipTagOrRequiredIconProfiler(sInputControlId,dspqRouter,listToolTips)+"</strong></label>"+
										"		<input type='file' id='"+sInputControlId+"' "+getInputCustomeAttributeProfiler(dspqRouter)+" name='"+sInputControlId+"'>												"+
										"		<input type=hidden id='"+sInputControlIdHidden+"' value='0'/>"+
										"		<span style='display: none; visibility: hidden;' id='video'></span>"+
										"	</div>"+
										" </div>");
							}
						}
						
						
						//--------- Input Field end
						sb.append("<input type='hidden' id='teacherIdVideoSS' value='"+teacherId+"' >");
						sb.append("</form>"+
						" 		<div class='row'>"+
						"	    <div class='col-sm-11 col-md-11 top10 idone'>"+
						"			<a id='hrefDone' href='javascript:void(0);' style='cursor: pointer; margin-left: 0px; text-	;' onclick='return insertOrUpdatevideoLinksNew();' >"+Utility.getLocaleValuePropByKey("lnkImD", locale)+"</a>&nbsp;&nbsp;"+
						"			<a class='idone' style='cursor: pointer; margin-left: 0px; text-decoration:none;'	onclick='return hideVideoLinksFormSS();'>"+
										Utility.getLocaleValuePropByKey("lnkCancel", locale)+
						"			</a>"+
						"		</div>"+
						"		</div>"+
						"	</div>");
						//sb.append(getCustomFieldBySectionIdProfiler(districtMaster,candidateType,dspqGroupMaster3,dspqRouterReferenceGrid.getDspqSectionMaster(),listToolTips,sSectionName,listCalDated,dspqPortfolioName,teacherDetail));
						sb.append("</div>");	
						//sb.append(getCustomFieldBySectionIdProfiler(dspqPotfolioDM, sCandidateTypeDec, dspqGroupMaster, dspqRouterSection10.getDspqSectionMaster(),listToolTips,sSectionName,listCalDated,dspqPortfolioName));
				}catch (Exception e) {
					e.printStackTrace();
				}
					//***************** End add video link **********************************
				}
				DspqRouter dspqRouterAdditionalDocuments=mapDspqRouterBySID.get(11);
				if(dspqRouterAdditionalDocuments!=null){
				sb.append("<div class='hide' id='additionalDocumentsDiv' onmouseover=\"setGridNameFlag('additionalDocuments')\"></div>");
				sb.append("<script>additionalDocuments('"+teacherId+"','"+portfoiloTypeAndId+"','"+dspqRouterAdditionalDocuments.getDisplayName()+"');</script>");
				
				//Method For Custom Question (AdditionalDocuments)
				 sb.append("<div id='customQuestionsAdditionalDocuments' class='left30'></div>");
				 sb.append("<script>customQuestionMaster('"+jobId+"','"+globalPortfolioIds+"','"+candidateType+"','3','11','"+teacherId+"','"+displayType+"');</script>");
				 /*if(sectionflag==0){
						sectionIds+=11;
						sectionflag++;
					}else{
						sectionIds+="#"+11;
						sectionflag++;
					}*/
				 //******************************************************************
				}
				DspqRouter dspqRouterEmploymentGrid=mapDspqRouterBySID.get(13);
				if(dspqRouterEmploymentGrid!=null){
				sb.append("<div class='hide' id='employmentGridDiv' onmouseover=\"setGridNameFlag('workExperience')\"></div>");
				sb.append("<script>employmentGrid('"+teacherId+"','"+portfoiloTypeAndId+"','"+dspqRouterEmploymentGrid.getDisplayName()+"');</script>");
				//Method For Custom Question (Employment)
				 sb.append("<div id='customQuestionsEmployment' class='left30'></div>");
				 sb.append("<script>customQuestionMaster('"+jobId+"','"+globalPortfolioIds+"','"+candidateType+"','4','13','"+teacherId+"','"+displayType+"');</script>");
				 /*if(sectionflag==0){
						sectionIds+=13;
						sectionflag++;
					}else{
						sectionIds+="#"+13;
						sectionflag++;
					}*/
				 //******************************************************************
				}
				
				DspqRouter dspqRouterInvolvementWorkGrid=mapDspqRouterBySID.get(14);
				if(dspqRouterInvolvementWorkGrid!=null){
				sb.append("<div class='hide' id='InvolvementWorkGrid' onmouseover=\"setGridNameFlag('involvement')\"></div>");
				sb.append("<script>involvementWorkGridProfiler('"+teacherId+"','"+portfoiloTypeAndId+"','"+dspqRouterInvolvementWorkGrid.getDisplayName()+"');</script>");
				
				//Method For Custom Question (Involvement)
				 sb.append("<div id='customQuestionsInvolvement' class='left30'></div>");
				 sb.append("<script>customQuestionMaster('"+jobId+"','"+globalPortfolioIds+"','"+candidateType+"','4','14','"+teacherId+"','"+displayType+"');</script>");
				 /*if(sectionflag==0){
						sectionIds+=14;
						sectionflag++;
					}else{
						sectionIds+="#"+14;
						sectionflag++;
					}*/
				 //******************************************************************
				}
				
				DspqRouter dspqRouterHonorsGridWithoutAction=mapDspqRouterBySID.get(15);
				if(dspqRouterHonorsGridWithoutAction!=null){
				sb.append("<div class='left15 hide' id='honorsGridWithoutAction' onmouseover=\"setGridNameFlag('honors')\"></div>");
				sb.append("<script>teacherHonors('"+teacherId+"','"+portfoiloTypeAndId+"','"+dspqRouterHonorsGridWithoutAction.getDisplayName()+"');</script>");
				
				//Method For Custom Question (Honors)
				 sb.append("<div id='customQuestionsHonors' class='left30'></div>");
				 sb.append("<script>customQuestionMaster('"+jobId+"','"+globalPortfolioIds+"','"+candidateType+"','4','15','"+teacherId+"','"+displayType+"');</script>");
				 /*if(sectionflag==0){
						sectionIds+=15;
						sectionflag++;
					}else{
						sectionIds+="#"+15;
						sectionflag++;
					}*/
				 //******************************************************************
				}
				
				DspqRouter dspqRouterLanguageProfiency=mapDspqRouterBySID.get(18);
				if(dspqRouterLanguageProfiency!=null){
				sb.append("<div class='hide left15' id='languageProfiencyDiv' onmouseover=\"setGridNameFlag('language')\"></div>");
				sb.append("<script>languageProfiency('"+teacherId+"','"+portfoiloTypeAndId+"','"+dspqRouterLanguageProfiency.getDisplayName()+"');</script>");
				
				//Method For Custom Question (Language)
				 sb.append("<div id='customQuestionsLanguage' class='left30'></div>");
				 sb.append("<script>customQuestionMaster('"+jobId+"','"+globalPortfolioIds+"','"+candidateType+"','1','18','"+teacherId+"','"+displayType+"');</script>");
				 /*if(sectionflag==0){
						sectionIds+=18;
						sectionflag++;
					}else{
						sectionIds+="#"+18;
						sectionflag++;
					}*/
				 //******************************************************************
				}
				sb.append("<input type='hidden' id='customQuestionsIds' name='customQuestionsIds' value='"+sectionIds+"'/>");
			}
			else{
				//Method For Fixed Personal Information
				sb.append("<div id='personalFixedAreaDiv'></div>");
				 sb.append("<script>personalFixedArea('"+teacherId+"','"+jobId+"');</script>");
			}
			if(editableStatus)
				sb.append("<div class='col-md-9 col-sm-9 mt10'></div><div class='col-md-3 col-sm-3 mt10 pright0' style='text-align: right;'><button class='btn btn-primary' type='button' id='saveCustom' onclick='saveAndValidateCustomQuestion("+jobId+","+teacherId+");' style='display: inline-block;'>&nbsp;&nbsp;&nbsp;&nbsp;Save Custom Fields&nbsp;<i class='icon'></i>&nbsp;&nbsp;&nbsp;&nbsp;</button></div>");
			//********* End Grid section *********************************************
			sb.append("<div class='left30 hide' id='selfServiceProfileOldCustomQuestion'></div>"); 
			sb.append("</div>");
			sb.append("</div>");
			sb.append("<div class='left30 hide' id='allCusQuesForSave'><input type='hidden' id='allCustomQuestion' name='allCustomQuestion' value='"+allCustomQuestionIdSB+"'/></div>");
			//******************* Footer start ***************************
			sb.append("<div id='gridFooterDiv'></div>");
			sb.append("<script>showFooterGrid('"+jobForTeacherId+"','"+teacherId+"','"+jobId+"','"+noOfRecordCheck+"','"+displayType+"');</script>");
			//******************** End Footer ***************************		
			sb.append("</div>");
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			sb.append("<script>$('#textarea').jqte();$('.tooltipEmpNo').tooltip();</script>");
			if(fg!=0)
			return sb.toString().split("####")[1];	
			else	
			return sb.toString();
		}
	}
	
	public String getGridFooter(Long jobForTeacherId,Integer teacherId,String jobId,Integer noOfRecordCheck,String sVisitLocation){
		StringBuffer sb=new StringBuffer();
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		//String sVisitLocation="My Folders";
		Integer entityID;
		try{
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}
			DistrictMaster districtMaster=null;
			UserMaster userMaster=null;
			if (session == null || session.getAttribute("userMaster") == null) {
				//return "false";
			}else{
				userMaster=(UserMaster)session.getAttribute("userMaster");
				entityID=userMaster.getEntityType();
			}
			sb.append("<div class='custom-div-border'>");
			sb.append("<div class='modal-footer' style='text-align: center !important;'>");
			
			if(sVisitLocation!=null && (sVisitLocation.equalsIgnoreCase("Mosaic") || sVisitLocation.equalsIgnoreCase("pool") || sVisitLocation.equalsIgnoreCase("PNR Dashboard") ||sVisitLocation.equalsIgnoreCase("My Folders")))
			{	
				String btnShare=Utility.getLocaleValuePropByKey("btnShare", locale);
				sb.append("<div class='row'><div class='col-md-3 col-sm-3'><a href='javascript:void(0);' onclick=\"getCommunicationsDiv(1,0,'"+teacherId+"',0);\" ><span class='icon-dropbox icon-large iconcolor'></span>&nbsp;"+headCom+"</a></div>");
				if(userMaster.getEntityType()==1)
				{
					sb.append("<div class='col-md-3 col-sm-3'><span class='icon-save icon-large iconcolorhover'></span>&nbsp;"+btnSave+"</div>" +
							"<div class='col-md-3 col-sm-3'><span class='icon-share icon-large iconcolorhover'></span>&nbsp;"+btnShare+"</div>"+
							"<div class='col-md-3 col-sm-3'><span class='icon-tag icon-large iconcolorhover'></span></span>&nbsp;"+headTag+"</div>");
				}
				else
				{
					
					int flagpopover=1;
					sb.append("<div class='col-md-3 col-sm-3'><a href='javascript:void(0);' id='saveaction"+noOfRecordCheck+"' onclick=\"saveToFolderJFTNULL('0','"+teacherId+"',"+flagpopover+")\"><span class='icon-save icon-large iconcolor'></span>&nbsp;"+btnSave+"</a></div>" +
							"<div class='col-md-3 col-sm-3'><a href='javascript:void(0);' id='shareaction"+noOfRecordCheck+"' onclick=\"displayUsergrid('"+teacherId+"',"+flagpopover+")\"><span class='icon-share icon-large iconcolor'></span>&nbsp;"+btnShare+"</a></div>");
					
					if(userMaster.getRoleId().getRoleId()==1 || userMaster.getRoleId().getRoleId()==3){
						sb.append("<div class='col-md-3 col-sm-3'><span class='icon-tag icon-large iconcolorhover'></span></span>&nbsp;"+headTag+"</div>");
					}
					else{
						if(sVisitLocation.equalsIgnoreCase("pool") ||sVisitLocation.equalsIgnoreCase("PNR Dashboard") ||sVisitLocation.equalsIgnoreCase("My Folders"))
							sb.append("<div class='col-md-3 col-sm-3'><a href='javascript:void(0);' id='tagaction' onclick=\"closeProfileOpenTags("+teacherId+","+noOfRecordCheck+")\"> <span class='icon-tag icon-large iconcolor'></span></span>&nbsp;"+headTag+"</a></div>");
						else
							sb.append("<div class='col-md-3 col-sm-3'><span class='icon-tag icon-large iconcolorhover'></span></span>&nbsp;"+headTag+"</div>");
					}
				  }
			}
			else
			{
				JobOrder jobOrder = null;
				if(jobId.equalsIgnoreCase("0")){
				}else{
					jobOrder = jobOrderDAO.findById(new Integer(jobId), false, false);
				}

			if(userMaster.getEntityType()==2){
			if(districtMaster!=null && (districtMaster.getDisplayCommunication()==null || districtMaster.getDisplayCommunication()==false)){
				sb.append("<div class='row'><div class='col-md-3 col-sm-3'><a href='javascript:void(0);' onclick=\"getCommunicationsDiv(0,"+jobForTeacherId+",'"+teacherId+"',"+jobOrder.getJobId()+");\" ><span class='icon-dropbox icon-large iconcolor'></span>&nbsp;"+headCom+"</a></div>");
			}else if(districtMaster!=null && (districtMaster.getDisplayCommunication()==null || districtMaster.getDisplayCommunication()==true)){
				if(jobOrder!=null){
					sb.append("<div class='row'><div class='col-md-3 col-sm-3'><a href='javascript:void(0);' onclick=\"getCommunicationsDiv(0,"+jobForTeacherId+",'"+teacherId+"',"+jobOrder.getJobId()+");\" ><span class='icon-dropbox icon-large iconcolor'></span>&nbsp;"+headCom+"</a></div>");
				}else{
					sb.append("<div class='row'><div class='col-md-3 col-sm-3'><a href='javascript:void(0);' onclick=\"getCommunicationsDiv(0,"+jobForTeacherId+",'"+teacherId+"',0);\" ><span class='icon-dropbox icon-large iconcolor'></span>&nbsp;"+headCom+"</a></div>");
				}
			}else{
				if(jobOrder!=null){
					sb.append("<div class='row'><div class='col-md-3 col-sm-3'><a href='javascript:void(0);' onclick=\"getCommunicationsDiv(0,"+jobForTeacherId+",'"+teacherId+"',"+jobOrder.getJobId()+");\" ><span class='icon-dropbox icon-large iconcolor'></span>&nbsp;"+headCom+"</a></div>");
				}else{
					sb.append("<div class='row'><div class='col-md-3 col-sm-3'><a href='javascript:void(0);' onclick=\"getCommunicationsDiv(0,"+jobForTeacherId+",'"+teacherId+"',0);\" ><span class='icon-dropbox icon-large iconcolor'></span>&nbsp;"+headCom+"</a></div>");
				}
			 }
			}else{
				if(jobOrder!=null){
					sb.append("<div class='row'><div class='col-md-3 col-sm-3'><a href='javascript:void(0);' onclick=\"getCommunicationsDiv(0,"+jobForTeacherId+",'"+teacherId+"',"+jobOrder.getJobId()+");\" ><span class='icon-dropbox icon-large iconcolor'></span>&nbsp;"+headCom+"</a></div>");
				}else{
					sb.append("<div class='row'><div class='col-md-3 col-sm-3'><a href='javascript:void(0);' onclick=\"getCommunicationsDiv(0,"+jobForTeacherId+",'"+teacherId+"',0);\" ><span class='icon-dropbox icon-large iconcolor'></span>&nbsp;"+headCom+"</a></div>");
				}
			}
			if(userMaster.getEntityType()==1){
				sb.append("<div class='col-md-3 col-sm-3'><span class='icon-save icon-large iconcolorhover'></span>&nbsp;"+btnSave+"</div>");
				sb.append("<div class='col-md-3 col-sm-3'><span class='icon-share icon-large iconcolorhover'></span>&nbsp;"+lblShare+"</div>");
				sb.append("<div class='col-md-3 col-sm-3'><span class='icon-tag icon-large iconcolorhover'></span></span>&nbsp;"+headTag+"</div>");
			}else{
				sb.append("<div class='col-md-3 col-sm-3'><a href='javascript:void(0);' id='saveaction' onclick=\"saveToFolder("+jobForTeacherId+",1)\"><span class='icon-save icon-large iconcolor'></span>&nbsp;"+btnSave+"</a></div>");
				sb.append("<div class='col-md-3 col-sm-3'><a href='javascript:void(0);' id='shareaction' onclick=\"displayShareFolder("+jobForTeacherId+",1)\"><span class='icon-share icon-large iconcolor'></span>&nbsp;"+lblShare+"</a></div>");
					try{
							if(jobOrder!=null && jobOrder.getWritePrivilegeToSchool()!=null && jobOrder.getWritePrivilegeToSchool()==true && userMaster.getEntityType()==3){
								if((userMaster.getSchoolId().getSchoolId().equals(jobOrder.getSchool().get(0).getSchoolId())  || (userMaster.getDistrictId().getIsJobSpecificTags()!=null && userMaster.getDistrictId().getIsJobSpecificTags()) ||(userMaster.getDistrictId().getIsDistrictWideTags()!=null && userMaster.getDistrictId().getIsDistrictWideTags()))){
									if(jobId!=null){
										sb.append("<div class='col-md-3 col-sm-3'><a href='javascript:void(0);' id='tagaction' onclick=\"closeTagActAction("+noOfRecordCheck+","+jobForTeacherId+","+teacherId+","+jobId+")\"> <span class='icon-tag icon-large iconcolor'></span></span>&nbsp;"+headTag+"</a></div>");
									}else{
										sb.append("<div class='col-md-3 col-sm-3'><a href='javascript:void(0);' id='tagaction' onclick=\"closeTagActAction("+noOfRecordCheck+","+jobForTeacherId+","+teacherId+",0)\"> <span class='icon-tag icon-large iconcolor'></span></span>&nbsp;"+headTag+"</a></div>");
									}
								}else{
									sb.append("<div class='col-md-3 col-sm-3'><span class='icon-tag icon-large iconcolorhover'></span></span>&nbsp;"+headTag+"</div>");
								}
							}else{
								if(userMaster.getEntityType()==2){ // condition only for T_T_J_O
									if(jobId!=null){
										sb.append("<div class='col-md-3 col-sm-3'><a href='javascript:void(0);' id='tagaction' onclick=\"closeTagActAction("+noOfRecordCheck+","+jobForTeacherId+","+teacherId+","+jobId+")\"> <span class='icon-tag icon-large iconcolor'></span></span>&nbsp;"+headTag+"</a></div>");
									}else{
										sb.append("<div class='col-md-3 col-sm-3'><a href='javascript:void(0);' id='tagaction' onclick=\"closeTagActAction("+noOfRecordCheck+","+jobForTeacherId+","+teacherId+",0)\"> <span class='icon-tag icon-large iconcolor'></span></span>&nbsp;"+headTag+"</a></div>");
									}
								}else{
									if(jobOrder!=null && jobOrder.getWritePrivilegeToSchool()!=null && jobOrder.getWritePrivilegeToSchool()== false && jobOrder.getSelectedSchoolsInDistrict()==1) // condition only for Second_T_J_O (job order with school Attached)
									{
										if(userMaster.getEntityType()==2){
											sb.append("<div class='col-md-3 col-sm-3'><span class='icon-tag icon-large iconcolorhover'></span></span>&nbsp;"+headTag+"</div>");
										}else{
											if(jobId!=null){
												sb.append("<div class='col-md-3 col-sm-3'><a href='javascript:void(0);' id='tagaction' onclick=\"closeTagActAction("+noOfRecordCheck+","+jobForTeacherId+","+teacherId+","+jobId+")\"> <span class='icon-tag icon-large iconcolor'></span></span>&nbsp;"+headTag+"</a></div>");
											}else{
												sb.append("<div class='col-md-3 col-sm-3'><a href='javascript:void(0);' id='tagaction' onclick=\"closeTagActAction("+noOfRecordCheck+","+jobForTeacherId+","+teacherId+",0)\"> <span class='icon-tag icon-large iconcolor'></span></span>&nbsp;"+headTag+"</a></div>");
											}
										}
									}else{
										// First Type Job Order means Only District Job Order
										if(!jobId.equalsIgnoreCase("0")){
											sb.append("<div class='col-md-3 col-sm-3'><a href='javascript:void(0);' id='tagaction' onclick=\"closeTagActAction("+noOfRecordCheck+","+jobForTeacherId+","+teacherId+","+jobId+")\"> <span class='icon-tag icon-large iconcolor'></span></span>&nbsp;"+headTag+"</a></div>");
										}else{
											sb.append("<div class='col-md-3 col-sm-3'><a href='javascript:void(0);' id='tagaction' onclick=\"closeTagActAction("+noOfRecordCheck+","+jobForTeacherId+","+teacherId+",0)\"> <span class='icon-tag icon-large iconcolor'></span></span>&nbsp;"+headTag+"</a></div>");
										}
									}
								}
							}
					}catch (Exception e){
						e.printStackTrace();
					}
			}
		 }  //End else
		sb.append("</div></div>");
		}catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	}
	
	@SuppressWarnings("finally")
	public String  candidateGridProfilerFooter(Long jobForTeacherId,Integer teacherId,String jobId,Integer noOfRecordCheck,int fg)
	{
		StringBuffer sb=new StringBuffer();
		try{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster=null;
		DistrictMaster districtMaster=null;
		Integer roleId=0;
		
		Date lastActivityDate=null;
	
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		
		if (session != null || session.getAttribute("userMaster") != null) {			//return "false";
		
			userMaster=(UserMaster)session.getAttribute("userMaster");
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
		}
		TeacherDetail teacherDetail = null;
		try{
			teacherDetail = teacherDetailDAO.findById(teacherId,false,false);
		}catch(Exception e){
			e.printStackTrace();
		}
		JobOrder jobOrder = jobOrderDAO.findById(new Integer(jobId), false, false);
		if(userMaster!=null){
        sb.append("<div class='row'>");
			if(userMaster.getEntityType()==2){
			if(lastActivityDate!=null && (districtMaster.getDisplayCommunication()==null || districtMaster.getDisplayCommunication()==false)){
				sb.append("<div class='col-md-3 col-sm-3'><a href='javascript:void(0);' onclick=\"getCommunicationsDiv(0,"+jobForTeacherId+",'"+teacherDetail.getTeacherId()+"',"+jobOrder.getJobId()+");\" ><span class='icon-dropbox icon-large iconcolor'></span>&nbsp;"+headCom+"</a></div>");
			}else if(lastActivityDate!=null && (districtMaster.getDisplayCommunication()==null || districtMaster.getDisplayCommunication()==true)){
				if(jobOrder!=null){
					sb.append("<div class='col-md-3 col-sm-3'><a href='javascript:void(0);' onclick=\"getCommunicationsDiv(0,"+jobForTeacherId+",'"+teacherDetail.getTeacherId()+"',"+jobOrder.getJobId()+");\" ><span class='icon-dropbox icon-large iconcolor'></span>&nbsp;"+headCom+"</a></div>");
				}else{
					sb.append("<div class='col-md-3 col-sm-3'><a href='javascript:void(0);' onclick=\"getCommunicationsDiv(0,"+jobForTeacherId+",'"+teacherDetail.getTeacherId()+"',0);\" ><span class='icon-dropbox icon-large iconcolor'></span>&nbsp;"+headCom+"</a></div>");
				}
				}else if(lastActivityDate==null){
					sb.append("<div class='col-md-3 col-sm-3'><a href='javascript:void(0);' onclick=\"getCommunicationsDiv(0,"+jobForTeacherId+",'"+teacherDetail.getTeacherId()+"',"+jobOrder.getJobId()+");\" ><span class='icon-dropbox icon-large iconcolor'></span>&nbsp;"+headCom+"</a></div>");
				}
			}else{
				sb.append("<div class='col-md-3 col-sm-3'><a href='javascript:void(0);' onclick=\"getCommunicationsDiv(0,"+jobForTeacherId+",'"+teacherDetail.getTeacherId()+"',"+jobOrder.getJobId()+");\" ><span class='icon-dropbox icon-large iconcolor'></span>&nbsp;"+headCom+"</a></div>");
			}
			if(userMaster.getEntityType()==1){
				sb.append("<div class='col-md-3 col-sm-3'><span class='icon-save icon-large iconcolorhover'></span>&nbsp;"+btnSave+"</div>");
				sb.append("<div class='col-md-3 col-sm-3'><span class='icon-share icon-large iconcolorhover'></span>&nbsp;"+lblShare+"</div>");
				sb.append("<div class='col-md-3 col-sm-3'><span class='icon-tag icon-large iconcolorhover'></span></span>&nbsp;"+headTag+"</div>");
			}else{
				sb.append("<div class='col-md-3 col-sm-3'><a href='javascript:void(0);' id='saveaction' onclick=\"saveToFolder("+jobForTeacherId+",1)\"><span class='icon-save icon-large iconcolor'></span>&nbsp;"+btnSave+"</a></div>");
				sb.append("<div class='col-md-3 col-sm-3'><a href='javascript:void(0);' id='shareaction' onclick=\"displayShareFolder("+jobForTeacherId+",1)\"><span class='icon-share icon-large iconcolor'></span>&nbsp;"+lblShare+"</a></div>");
					try{
							if(jobOrder.getWritePrivilegeToSchool()!=null && jobOrder.getWritePrivilegeToSchool()==true && userMaster.getEntityType()==3){
								if((userMaster.getSchoolId().getSchoolId().equals(jobOrder.getSchool().get(0).getSchoolId())  || (userMaster.getDistrictId().getIsJobSpecificTags()!=null && userMaster.getDistrictId().getIsJobSpecificTags()) ||(userMaster.getDistrictId().getIsDistrictWideTags()!=null && userMaster.getDistrictId().getIsDistrictWideTags()))){
									sb.append("<div class='col-md-3 col-sm-3'><a href='javascript:void(0);' id='tagaction' onclick=\"closeTagActAction("+noOfRecordCheck+","+jobForTeacherId+","+teacherId+","+jobId+")\"> <span class='icon-tag icon-large iconcolor'></span></span>&nbsp;"+headTag+"</a></div>");
								}else{
									sb.append("<div class='col-md-3 col-sm-3'><span class='icon-tag icon-large iconcolorhover'></span></span>&nbsp;"+headTag+"</div>");
								}
							}else{
								if(userMaster.getEntityType()==2){ // condition only for T_T_J_O
									sb.append("<div class='col-md-3 col-sm-3'><a href='javascript:void(0);' id='tagaction' onclick=\"closeTagActAction("+noOfRecordCheck+","+jobForTeacherId+","+teacherId+","+jobId+")\"> <span class='icon-tag icon-large iconcolor'></span></span>&nbsp;"+headTag+"</a></div>");
								}else{
									if(jobOrder.getWritePrivilegeToSchool()!=null && jobOrder.getWritePrivilegeToSchool()== false && jobOrder.getSelectedSchoolsInDistrict()==1) // condition only for Second_T_J_O (job order with school Attached)
									{
										if(userMaster.getEntityType()==2){
											sb.append("<div class='col-md-3 col-sm-3'><span class='icon-tag icon-large iconcolorhover'></span></span>&nbsp;"+headTag+"</div>");
										}else{
											sb.append("<div class='col-md-3 col-sm-3'><a href='javascript:void(0);' id='tagaction' onclick=\"closeTagActAction("+noOfRecordCheck+","+jobForTeacherId+","+teacherId+","+jobId+")\"> <span class='icon-tag icon-large iconcolor'></span></span>&nbsp;"+headTag+"</a></div>");
										}
									}else{
										// First Type Job Order means Only District Job Order
										sb.append("<div class='col-md-3 col-sm-3'><a href='javascript:void(0);' id='tagaction' onclick=\"closeTagActAction("+noOfRecordCheck+","+jobForTeacherId+","+teacherId+","+jobId+")\"> <span class='icon-tag icon-large iconcolor'></span></span>&nbsp;"+headTag+"</a></div>");
									}
								}
							}
					}catch (Exception e){
						e.printStackTrace();
					}
			}
			sb.append("</div>");
		 }
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			sb.append("<script>$('textarea').jqte();$('.tooltipEmpNo').tooltip();</script>");
			if(fg!=0)
			return sb.toString().split("####")[1];	
			else	
			return sb.toString();
		}
	}
		
	@Transactional(readOnly=false)
	public String getGroupProfiler(String iJobId,Integer dspqPortfolioNameId,String sCandidateType,Integer groupId,TeacherDetail teacherDetail)
	{
		String candidateType="";
		if(sCandidateType.equalsIgnoreCase("")){
		}else{
			candidateType=validateApplicantType(sCandidateType);
		}
		/*System.out.println("iJobId " +iJobId);
		System.out.println("dspqPortfolioNameId " +dspqPortfolioNameId);
		System.out.println("candidateType " +candidateType);
		System.out.println("teacherDetail " +teacherDetail.getFirstName());*/
	//printdata("Calling DSPQServiceAjax => getGroup01");
	//TeacherDetail teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");
	
		String locale = Utility.getValueOfPropByKey("locale");
		
		StringBuffer sb=new StringBuffer();
		
		TeacherPersonalInfo teacherpersonalinfo = teacherPersonalInfoDAO.findTeacherPersonalInfoByTeacher(teacherDetail);
		
		JobOrder jobOrder=null;
		//JobCategoryMaster dspqPotfolioJC=null;
		if(iJobId!=null && !iJobId.equalsIgnoreCase("0") && !iJobId.equals(""))
		{
			int JobId=Utility.getIntValue(iJobId);
			jobOrder=jobOrderDAO.findById(JobId, false, false);
			
			/*if(jobOrder!=null && jobOrder.getJobCategoryMaster()!=null)
				dspqPotfolioJC=jobOrder.getJobCategoryMaster();*/
		}
		
		List<StateMaster> listStateMaster = null;
		List<CityMaster> listCityMasters=null;
		if(teacherpersonalinfo!=null)
		{
			if(teacherpersonalinfo.getCountryId()!=null)
				listStateMaster=stateMasterDAO.findActiveStateByCountryId(teacherpersonalinfo.getCountryId());
			if(teacherpersonalinfo.getStateId()!=null)
				listCityMasters = cityMasterDAO.findCityByState(teacherpersonalinfo.getStateId());
		}
			
		//String sCandidateTypeDec=validateCTAndDec(candidateType);
		//printdata("sCandidateTypeDec "+sCandidateTypeDec);
		
		List<RaceMaster> listRaceMasters = raceMasterDAO.findAllRaceByOrder();
		List<EthinicityMaster> lstEthinicityMasters=ethinicityMasterDAO.findByCriteria(Restrictions.eq("status", "A"));
		List<EthnicOriginMaster> lstethnicOriginMasters=ethnicOriginMasterDAO.findByCriteria(Restrictions.eq("status", "A"));
		List<GenderMaster> lstGenderMasters  = genderMasterDAO.findAllGenderByOrder();
		
		DspqGroupMaster dspqGroupMaster=dspqGroupMasterDAO.findById(groupId, false, false);
		List<DspqSectionMaster> sectionMasters=dspqSectionMasterDAO.getDspqSectionListByGroup(dspqGroupMaster);
		List<DspqFieldMaster> dspqFieldList = dspqFieldMasterDAO.getFieldListBySections(sectionMasters);
		DspqPortfolioName dspqPortfolioName = dspqPortfolioNameDAO.findById(dspqPortfolioNameId, false, false);
		List<DspqRouter> dspqRouterList=dspqRouterDAO.getControlsByPortfolioNameAndCTAndField(dspqPortfolioName, candidateType, dspqFieldList,sectionMasters);
		
		Map<Integer, DspqRouter> mapDspqRouterByFID=new HashMap<Integer, DspqRouter>();
		Map<Integer, DspqRouter> mapDspqRouterBySID=new HashMap<Integer, DspqRouter>();
		setSIDAndFIDInMapProfiler(mapDspqRouterByFID, mapDspqRouterBySID, dspqRouterList);
		
		List<String> listToolTips = new ArrayList<String>();
		List<String> listCalDated = new ArrayList<String>();
		
		if(mapDspqRouterByFID!=null)
		{		
			DistrictMaster dspqPotfolioDM=null;
			//String customQuestion="";
			if(dspqPortfolioName!=null && dspqPortfolioName.getDistrictMaster()!=null)
				dspqPotfolioDM=dspqPortfolioName.getDistrictMaster();
			
			
			DspqRouter dspqRouterSection2=mapDspqRouterBySID.get(2);
			if(dspqRouterSection2!=null)
			{
				String sSectionName="tooltipSection02";
				//sb.append("<div class='row'><div class='col-sm-12 col-md-12'><div class='span4 portfolio_Subheading'>"+dspqRouterSection2.getDisplayName()+"</div></div></div>");
				//sb.append(getInstructionForSection(dspqRouterSection2));
						
				sb.append("<div class='row'>");	
				String Salution="";
				//Salutation
				if(mapDspqRouterByFID.get(5)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(5);
					if(dspqRouter!=null)
					{
						//sb.append("<div class='col-sm-2 col-md-2'>"+dspqRouter.getDisplayName()+"</div>");
						Integer iSalutation=0;
						
						if(teacherpersonalinfo!=null && teacherpersonalinfo.getSalutation()!=null)
							iSalutation=teacherpersonalinfo.getSalutation();				
					
							if(iSalutation==4)
								Salution=Utility.getLocaleValuePropByKey("optDr", locale);						
							
							if(iSalutation==3)
								Salution=Utility.getLocaleValuePropByKey("optMiss", locale);
													
							if(iSalutation==2)
								Salution=Utility.getLocaleValuePropByKey("optMr", locale);
												
							if(iSalutation==1)
								Salution=Utility.getLocaleValuePropByKey("optMrs", locale);
													
							if(iSalutation==5)
								Salution=Utility.getLocaleValuePropByKey("optMs", locale);						
							
							
						//sb.append("<div class='col-sm-4 col-md-4'>"+Salution+"</div>");
					}
				}
				//Full Name
				if(mapDspqRouterByFID.get(6)!=null)
				{
					//sb.append("<div class='row'>");	
					DspqRouter dspqRouter=mapDspqRouterByFID.get(6);
					if(dspqRouter!=null)
					{					
						String Name="";
						if(teacherDetail!=null && teacherDetail.getFirstName()!=null)
							Name=teacherDetail.getFirstName();
						if(teacherpersonalinfo!=null && teacherpersonalinfo.getMiddleName()!=null)
							Name=Name+" "+teacherpersonalinfo.getMiddleName();
						if(teacherpersonalinfo!=null && teacherpersonalinfo.getLastName()!=null)
							Name=Name+" "+teacherpersonalinfo.getLastName();
						//sb.append("<div class='col-sm-2 col-md-2'>Name</div>");
						//sb.append("<div class='col-sm-2 col-md-2'>"+Salution+" "+Name+"</div>");
					}
					//sb.append("</div>");				
				}
				//SSN
				if(mapDspqRouterByFID.get(81)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(81);
					if(dspqRouter!=null)
					{
						//sb.append("<div class='row'>");
						String sSSN="";
						if(teacherpersonalinfo!=null && teacherpersonalinfo.getSSN()!=null && !teacherpersonalinfo.getSSN().equals(""))
							sSSN=teacherpersonalinfo.getSSN();
						if(sSSN!=null && !sSSN.equals(""))
						{
							try {
								sSSN=Utility.decodeBase64(sSSN);
							} catch (IOException e) {
								e.printStackTrace();
							}
						}
						
						sb.append("<div class='col-sm-2 col-md-2 divprofile'>"+dspqRouter.getDisplayName()+":</div>");
						sb.append("<div class='col-sm-2 col-md-2'>"+sSSN+"</div>");		
						//sb.append("</div>");
					}
				}
				//Phone
				if(mapDspqRouterByFID.get(96)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(96);
					if(dspqRouter!=null)
					{
						
						String sPhoneNumber="";
						String sPhoneNumber1="";
						String sPhoneNumber2="";
						String sPhoneNumber3="";
						//sb.append("<div class='row'>");
						sb.append("<div class='col-sm-2 col-md-2 divprofile'>"+dspqRouter.getDisplayName()+":</div>");

						if(teacherpersonalinfo!=null && teacherpersonalinfo.getPhoneNumber()!=null)
						{
							sPhoneNumber=teacherpersonalinfo.getPhoneNumber();
							if(sPhoneNumber!=null && !sPhoneNumber.equals(""))
							{
								if(sPhoneNumber.length()==10 || sPhoneNumber.length()==12)
								{
									String arrPh[]=sPhoneNumber.split("-");
									if(arrPh.length==3)
									{
										sPhoneNumber1=arrPh[0];
										sPhoneNumber2=arrPh[1];
										sPhoneNumber3=arrPh[2];
									}
									else
									{
										sPhoneNumber1=sPhoneNumber.substring(0, 3);
										sPhoneNumber2=sPhoneNumber.substring(3, 6);
										sPhoneNumber3=sPhoneNumber.substring(6, 10);
									}
								}
								
							}
						}
						//
						sb.append("<div class='col-sm-2 col-md-2'>"+sPhoneNumber1+"-"+sPhoneNumber2+"-"+sPhoneNumber2+"</div>");
						//sb.append("</div>");
					}
				}
				//Mobile
				if(mapDspqRouterByFID.get(18)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(18);
					if(dspqRouter!=null)
					{
						String sMobileNumber="";
						String sMobileNumber1="";
						String sMobileNumber2="";
						String sMobileNumber3="";
						//sb.append("<div class='row'>");
						sb.append("<div class='col-sm-2 col-md-2 divprofile'>"+dspqRouter.getDisplayName()+":</div>");
						if(teacherpersonalinfo!=null && teacherpersonalinfo.getMobileNumber()!=null)
						{
							sMobileNumber=teacherpersonalinfo.getMobileNumber();
							if(sMobileNumber!=null && !sMobileNumber.equals(""))
							{
								if(sMobileNumber.length()==10 || sMobileNumber.length()==12)
								{
									String arrMobile[]=sMobileNumber.split("-");
									if(arrMobile.length==3)
									{
										sMobileNumber1=arrMobile[0];
										sMobileNumber2=arrMobile[1];
										sMobileNumber3=arrMobile[2];
									}
									else
									{
										sMobileNumber1=sMobileNumber.substring(0, 3);
										sMobileNumber2=sMobileNumber.substring(3, 6);
										sMobileNumber3=sMobileNumber.substring(6, 10);
									}
								}
								
							}
						}
						sb.append("<div class='col-sm-2 col-md-2'>"+sMobileNumber1+"-"+sMobileNumber2+"-"+sMobileNumber3+"</div>");
						//sb.append("</div>");
					}
				}
				//Expected Salary
				if(mapDspqRouterByFID.get(19)!=null)
				{
					//sb.append("<div class='row'>");
					DspqRouter dspqRouter=mapDspqRouterByFID.get(19);
					if(dspqRouter!=null)
					{
						String iExpectedSalary=lblNA;
						if(teacherpersonalinfo!=null && teacherpersonalinfo.getExpectedSalary()!=null)
							iExpectedSalary=teacherpersonalinfo.getExpectedSalary().toString();
						
						sb.append("<div class='col-sm-2 col-md-2 divprofile'>"+dspqRouter.getDisplayName()+":</div>");
						sb.append("<div class='col-sm-2 col-md-2'>"+iExpectedSalary+"</div>");	
						
						
					}
					//sb.append("</div>");
				}
			/*	customQuestion="";
				customQuestion=getCustomFieldBySectionIdProfiler(dspqPotfolioDM, candidateType, dspqGroupMaster, dspqRouterSection2.getDspqSectionMaster(),listToolTips,sSectionName,listCalDated,dspqPortfolioName,teacherDetail);
				if(customQuestion !="" && !customQuestion.equalsIgnoreCase("")){
					sb.append("</div>");
					sb.append(customQuestion);
					sb.append("<div class='row'>");
				}*/
				
			}
				//DOB
			DspqRouter dspqRouterSection16=mapDspqRouterBySID.get(16);
			if(dspqRouterSection16!=null)
			{
				String sSectionName="tooltipSection15";
			
				sb.append("<div class='col-sm-2 col-md-2 divprofile'>Date of Birth:</div>");
				Date dDOB=null;
				int iYear =0;
				int iMonth =0;
				int iDay =0;
				    String DateOfBirth="";
				if(teacherpersonalinfo!=null && teacherpersonalinfo.getDob()!=null)
				{
					dDOB=teacherpersonalinfo.getDob();
					Calendar cal = Calendar.getInstance();
					cal.setTime(dDOB);
					iYear = cal.get(Calendar.YEAR);	
					iMonth = cal.get(Calendar.MONTH)+1;
					iDay = cal.get(Calendar.DAY_OF_MONTH);
				}
				//Month
				if(mapDspqRouterByFID.get(84)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(84);
					if(dspqRouter!=null)
					{
						/*List<String> listMonth=Utility.getMonthList();
						int i=0;
						for(String strMonthName:listMonth)
						{
							i++;						
							if(iMonth==i)
								//DateOfBirth=strMonthName;
								//sb.append("<option value='"+i+"' selected>"+strMonthName+"</option>");
							
						}*/
						
					}
				}
				if(teacherpersonalinfo!=null && teacherpersonalinfo.getDob()!=null)
					DateOfBirth=Utility.convertDateAndTimeToUSformatOnlyDate(teacherpersonalinfo.getDob());
				else
					DateOfBirth=lblNA.toString();
				//Day
				/*if(mapDspqRouterByFID.get(85)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(85);
					if(dspqRouter!=null)
					{
						List<String> listDay=Utility.getDays();
						int i=0;
						for(String strday:listDay)
						{
							i++;
							if(iDay==i)
								DateOfBirth=DateOfBirth+" "+strday;
						}
					}
				}*/
				
				//Year
				/*if(mapDspqRouterByFID.get(86)!=null)
				{
					DspqRouter dspqRouter=mapDspqRouterByFID.get(86);
					if(dspqRouter!=null)
					{
						DateOfBirth=DateOfBirth+" "+iYear;
					}
				}*/
				sb.append("<div class='col-sm-2 col-md-2'>"+DateOfBirth+"</div>");
				/*customQuestion="";
				customQuestion=getCustomFieldBySectionIdProfiler(dspqPotfolioDM,candidateType, dspqGroupMaster, dspqRouterSection16.getDspqSectionMaster(),listToolTips,sSectionName,listCalDated,dspqPortfolioName,teacherDetail);
				if(customQuestion!="" && !customQuestion.equalsIgnoreCase("")){	
					sb.append("</div>");
					sb.append(customQuestion);
					sb.append("<div class='row'>");
				}*/
				
			}
				//Address
				DspqRouter dspqRouterSection4=mapDspqRouterBySID.get(4);
				if(dspqRouterSection4!=null)
				{
					String sSectionName="tooltipSection4";		
					String Address="";			
					//sb.append("<div class='row'>");
					//AddressLine1
					if(mapDspqRouterByFID.get(12)!=null)
					{
						DspqRouter dspqRouter=mapDspqRouterByFID.get(12);
						if(dspqRouter!=null)
						{
							
							String sAddressLine1="";
							if(teacherpersonalinfo!=null && teacherpersonalinfo.getAddressLine1()!=null)
								sAddressLine1=teacherpersonalinfo.getAddressLine1();
							
							sb.append("<div class='col-sm-2 col-md-2 divprofile'>"+dspqRouterSection4.getDisplayName()+":</div>");
							Address = sAddressLine1;
							
						}
					}
					//AddressLine2
					if(mapDspqRouterByFID.get(13)!=null)
					{
						DspqRouter dspqRouter=mapDspqRouterByFID.get(13);
						if(dspqRouter!=null)
						{
							if(teacherpersonalinfo!=null && teacherpersonalinfo.getAddressLine2()!=null)
								Address=Address+", "+teacherpersonalinfo.getAddressLine2();						
						}
					}
					//State
					if(mapDspqRouterByFID.get(16)!=null)
					{
						DspqRouter dspqRouter=mapDspqRouterByFID.get(16);
						if(dspqRouter!=null)
						{						
							if(teacherpersonalinfo!=null && teacherpersonalinfo.getStateId().getStateId()!=null)
								Address=Address+", "+teacherpersonalinfo.getStateId().getStateName();
						}
					}
					//otherState
					if(mapDspqRouterByFID.get(83)!=null)
					{
						DspqRouter dspqRouter=mapDspqRouterByFID.get(83);
						if(dspqRouter!=null)
						{
							if(teacherpersonalinfo!=null && teacherpersonalinfo.getOtherState()!=null)
								Address=Address+", "+teacherpersonalinfo.getOtherState();
						}
					}
					//city
					if(mapDspqRouterByFID.get(17)!=null)
					{
						DspqRouter dspqRouter=mapDspqRouterByFID.get(17);
						if(dspqRouter!=null)
						{
							if(teacherpersonalinfo!=null && teacherpersonalinfo.getCityId()!=null)
								Address=Address+", "+teacherpersonalinfo.getCityId().getCityName();
						}
					}
					//otherCity
					if(mapDspqRouterByFID.get(82)!=null)
					{
						DspqRouter dspqRouter=mapDspqRouterByFID.get(82);
						if(dspqRouter!=null)
						{
							if(teacherpersonalinfo!=null && teacherpersonalinfo.getOtherCity()!=null)
								Address=Address+", "+teacherpersonalinfo.getOtherCity();
						}
					}
					//zipCode
					if(mapDspqRouterByFID.get(15)!=null)
					{
						DspqRouter dspqRouter=mapDspqRouterByFID.get(15);
						if(dspqRouter!=null)
						{
							if(teacherpersonalinfo!=null && teacherpersonalinfo.getZipCode()!=null)
								Address=Address+", "+teacherpersonalinfo.getZipCode();
						}
					}
					//country
					if(mapDspqRouterByFID.get(14)!=null)
					{
						List<CountryMaster> countryMasters=countryMasterDAO.findAllActiveCountry();
						
						DspqRouter dspqRouter=mapDspqRouterByFID.get(14);
						if(dspqRouter!=null)
						{
							Integer iCountryId=0;
							if(teacherpersonalinfo!=null && teacherpersonalinfo.getCountryId()!=null)
								Address=Address+", "+teacherpersonalinfo.getCountryId().getName();
						}
					}
					sb.append("<div class='col-sm-2 col-md-2'>"+Address+"</div>");
					//sb.append("</div>");
					/*customQuestion="";
					customQuestion=getCustomFieldBySectionIdProfiler(dspqPotfolioDM, candidateType, dspqGroupMaster, dspqRouterSection4.getDspqSectionMaster(),listToolTips,sSectionName,listCalDated,dspqPortfolioName,teacherDetail);
					if(customQuestion !="" && !customQuestion.equalsIgnoreCase("")){
						sb.append("</div>");
						sb.append(customQuestion);
						sb.append("<div class='row'>");
					}*/
					
			}
				
				//End Permanent Address
				
				//Start Present Address			
				
				DspqRouter dspqRouterSection17=mapDspqRouterBySID.get(17);
				if(dspqRouterSection17!=null)
				{			
					/*sb.append("<div class='row'>");
					sb.append("<div class='col-sm-12 col-md-12'><div class='span4 portfolio_Subheading'>"+dspqRouterSection17.getDisplayName()+"</div></div>");
					sb.append("</div>");*/
					String sSectionName="tooltipSection17";				
					//sb.append("<div class='row'>");
					sb.append("<div class='col-sm-2 col-md-2 divprofile'>"+dspqRouterSection17.getDisplayName()+":</div>");
					String PersentAddress="";
					//AddressLine1
					if(mapDspqRouterByFID.get(87)!=null)
					{
						DspqRouter dspqRouter=mapDspqRouterByFID.get(87);
						if(dspqRouter!=null)
						{
							
							String sAddressLinePr1="";
							if(teacherpersonalinfo!=null && teacherpersonalinfo.getPresentAddressLine1()!=null)
								sAddressLinePr1=teacherpersonalinfo.getPresentAddressLine1();
							
							PersentAddress = sAddressLinePr1;
							
							
						}
					}
					//AddressLine2
					if(mapDspqRouterByFID.get(88)!=null)
					{
						DspqRouter dspqRouter=mapDspqRouterByFID.get(88);
						if(dspqRouter!=null)
						{
							String sAddressLinePr2="";
							if(teacherpersonalinfo!=null && teacherpersonalinfo.getPresentAddressLine2()!=null){
								sAddressLinePr2=teacherpersonalinfo.getPresentAddressLine2();						
								PersentAddress=PersentAddress+","+sAddressLinePr2;
							}
						}
					}
					//State
					if(mapDspqRouterByFID.get(91)!=null)
					{
						DspqRouter dspqRouter=mapDspqRouterByFID.get(91);
						if(dspqRouter!=null)
						{
								String lPresentStateId=null;
								if(teacherpersonalinfo!=null && teacherpersonalinfo.getPresentStateId()!=null){
									lPresentStateId=teacherpersonalinfo.getPresentStateId().getStateName();							
									PersentAddress=PersentAddress+","+lPresentStateId;
									}
						}
					}
					//otherState
					if(mapDspqRouterByFID.get(94)!=null)
					{
						DspqRouter dspqRouter=mapDspqRouterByFID.get(94);
						if(dspqRouter!=null)
						{
							String sPersentOtherState="";
							if(teacherpersonalinfo!=null && teacherpersonalinfo.getPersentOtherState()!=null){
								sPersentOtherState=teacherpersonalinfo.getPersentOtherState();
								PersentAddress=PersentAddress+","+sPersentOtherState;
							}
								
						}
					}
					//city
					if(mapDspqRouterByFID.get(92)!=null)
					{
						DspqRouter dspqRouter=mapDspqRouterByFID.get(92);
						if(dspqRouter!=null)
						{
							
							String lPresentCityId=null;
							if(teacherpersonalinfo!=null && teacherpersonalinfo.getPresentCityId()!=null){
								lPresentCityId=teacherpersonalinfo.getPresentCityId().getCityName();
								PersentAddress=PersentAddress+","+lPresentCityId;
							}
									
						}
					}
					
					//otherCity
					if(mapDspqRouterByFID.get(93)!=null)
					{
						DspqRouter dspqRouter=mapDspqRouterByFID.get(93);
						if(dspqRouter!=null)
						{
							String sPersentOtherCity="";
							if(teacherpersonalinfo!=null && teacherpersonalinfo.getPersentOtherCity()!=null){
								sPersentOtherCity=teacherpersonalinfo.getPersentOtherCity();						
								PersentAddress=PersentAddress+","+sPersentOtherCity;
							}
						}
					}
					//zipCode
					if(mapDspqRouterByFID.get(90)!=null)
					{
						DspqRouter dspqRouter=mapDspqRouterByFID.get(90);
						if(dspqRouter!=null)
						{
							String sPresentZipCode="";
							if(teacherpersonalinfo!=null && teacherpersonalinfo.getPresentZipCode()!=null){
								sPresentZipCode=teacherpersonalinfo.getPresentZipCode();
								PersentAddress=PersentAddress+","+sPresentZipCode;
							}
							
						}
					}
					//country
					if(mapDspqRouterByFID.get(89)!=null)
					{
						List<CountryMaster> countryMasters=countryMasterDAO.findAllActiveCountry();
						
						DspqRouter dspqRouter=mapDspqRouterByFID.get(89);
						if(dspqRouter!=null)
						{
							String iPresentCountryId="";
							if(teacherpersonalinfo!=null && teacherpersonalinfo.getPresentCountryId()!=null){
								iPresentCountryId=teacherpersonalinfo.getPresentCountryId().getName();								
								PersentAddress=PersentAddress+","+iPresentCountryId;
							}
						}
					}
					if(PersentAddress.equalsIgnoreCase(""))
						sb.append("<div class='col-sm-2 col-md-2'>"+lblNA+"</div>");
					else
						sb.append("<div class='col-sm-2 col-md-2'>"+PersentAddress+"</div>");
					//sb.append("</div>");
					/*customQuestion="";
					customQuestion=getCustomFieldBySectionIdProfiler(dspqPotfolioDM, candidateType, dspqGroupMaster, dspqRouterSection17.getDspqSectionMaster(),listToolTips,sSectionName,listCalDated,dspqPortfolioName,teacherDetail);
					if(customQuestion !="" && !customQuestion.equalsIgnoreCase("")){
						sb.append("</div>");
						sb.append(customQuestion);
						sb.append("<div class='row'>");
					}*/
					
				}				
				// End Present Address
				
				//Start Veteran
				DspqRouter dspqRouterSection20=mapDspqRouterBySID.get(20);
				if(dspqRouterSection20!=null)
				{
					String sSectionName="tooltipSection20";	
					
					
					if(mapDspqRouterByFID.get(97)!=null)
					{
						//sb.append("<div class='row'>");
						DspqRouter dspqRouter=mapDspqRouterByFID.get(97);
						if(dspqRouter!=null)
						{
							sb.append("<div class='col-sm-2 col-md-2 divprofile'>"+dspqRouterSection20.getDisplayName()+":</div>");
							Integer isVateran=null;
							if(teacherpersonalinfo!=null && teacherpersonalinfo.getIsVateran()!=null)
								isVateran=teacherpersonalinfo.getIsVateran();						
						
							if(isVateran!=null && isVateran==1)
								sb.append("<div class='col-sm-2 col-md-2'>Yes</div>");
							else
								sb.append("<div class='col-sm-2 col-md-2'>No</div>");
							
							
							
						}
						//sb.append("</div>");
					}
					/*customQuestion="";
					customQuestion=getCustomFieldBySectionIdProfiler(dspqPotfolioDM, candidateType, dspqGroupMaster, dspqRouterSection17.getDspqSectionMaster(),listToolTips,sSectionName,listCalDated,dspqPortfolioName,teacherDetail);
					if(customQuestion!="" && customQuestion.equalsIgnoreCase("")){
						sb.append("</div>");
						sb.append(customQuestion);
						sb.append("<div class='row'>");
					}*/
				}
				//End Veteran
				
				//Start Driver Licence
				DspqRouter dspqRouterSection22=mapDspqRouterBySID.get(22);
				if(dspqRouterSection22!=null)
				{
					String sSectionName="tooltipSection22";
					
					//sb.append("<div class='row'>");
					
					if(mapDspqRouterByFID.get(100)!=null)
					{
						DspqRouter dspqRouter=mapDspqRouterByFID.get(100);
						if(dspqRouter!=null)
						{
							sb.append("<div class='col-sm-2 col-md-2 divprofile'>"+dspqRouter.getDisplayName()+":</div>");
							String sDrivingLicNum="";
							if(teacherpersonalinfo!=null && teacherpersonalinfo.getDrivingLicNum()!=null)
								sb.append("<div class='col-sm-2 col-md-2'>"+teacherpersonalinfo.getDrivingLicNum()+"</div>");
							else
								sb.append("<div class='col-sm-2 col-md-2'>"+lblNA+"</div>");
						}
					}
					
					if(mapDspqRouterByFID.get(99)!=null)
					{
						DspqRouter dspqRouter=mapDspqRouterByFID.get(99);
						if(dspqRouter!=null)
						{
							sb.append("<div class='col-sm-2 col-md-2 divprofile'>"+dspqRouter.getDisplayName()+":</div>");
							if(teacherpersonalinfo!=null && teacherpersonalinfo.getDrivingLicState()!=null && !teacherpersonalinfo.getDrivingLicState().equals("") && !teacherpersonalinfo.getDrivingLicState().equals(0))
							{
								StateMaster  stateMaster = stateMasterDAO.findById(Long.parseLong(teacherpersonalinfo.getDrivingLicState()), false, false);
								if(stateMaster!=null)
									sb.append("<div class='col-sm-2 col-md-2'>"+stateMaster.getStateName()+"</div>");
								else
									sb.append("<div class='col-sm-2 col-md-2'>"+lblNA+"</div>");
							}
							else
								sb.append("<div class='col-sm-2 col-md-2'>"+lblNA+"</div>");
						}
					}				
					
					//sb.append("</div>");
					/*customQuestion="";
					customQuestion=getCustomFieldBySectionIdProfiler(dspqPotfolioDM, candidateType, dspqGroupMaster, dspqRouterSection22.getDspqSectionMaster(),listToolTips,sSectionName,listCalDated,dspqPortfolioName,teacherDetail);
					if(customQuestion!="" && customQuestion.equalsIgnoreCase("")){
						sb.append("</div>");
						sb.append(customQuestion);
						sb.append("<div class='row'>");
					}*/
				}
				//Race Ethinicity
				int counter=0;
				DspqRouter dspqRouterSection3=mapDspqRouterBySID.get(3);
				if(dspqRouterSection3!=null)
				{
					String raceEthinicity="";
					String sSectionName="tooltipSection3";			
					//sb.append("<div class='row'>");//Start :: #5#
					sb.append("<div class='col-sm-2 col-md-2 divprofile'>"+dspqRouterSection3.getDisplayName()+":</div>");
					if(mapDspqRouterByFID.get(8)!=null)
					{
						
						DspqRouter dspqRouter=mapDspqRouterByFID.get(8);
						if(dspqRouter!=null)
						{
							
							Integer ethnicOriginId=null;
							if(teacherpersonalinfo!=null && teacherpersonalinfo.getEthnicOriginId()!=null)
								ethnicOriginId=teacherpersonalinfo.getEthnicOriginId().getEthnicOriginId();
							
							String sInputControlId="ethnicOriginId";
							for(EthnicOriginMaster ethnicOriginMaster :lstethnicOriginMasters)
							{
								
								if(ethnicOriginId!=null && ethnicOriginId.equals(ethnicOriginMaster.getEthnicOriginId())){
									if(counter==0){
										raceEthinicity = ethnicOriginMaster.getEthnicOriginName();
										counter++;
									}else{
										raceEthinicity +=", "+ethnicOriginMaster.getEthnicOriginName();
									}
								}
									
							}
						}				
					}
					
					
					if(mapDspqRouterByFID.get(9)!=null)
					{					
						DspqRouter dspqRouter=mapDspqRouterByFID.get(9);
						if(dspqRouter!=null)
						{
							
							Integer ethinicityId=null;
							if(teacherpersonalinfo!=null && teacherpersonalinfo.getEthnicityId()!=null)
								ethinicityId=teacherpersonalinfo.getEthnicityId().getEthnicityId();
							
							for(EthinicityMaster ethinicityMaster :lstEthinicityMasters)
							{
								if(ethinicityId!=null && ethinicityId.equals(ethinicityMaster.getEthnicityId())){
									if(counter==0){
										raceEthinicity = ethinicityMaster.getEthnicityName();
										counter++;
									}else{
										raceEthinicity +=", "+ethinicityMaster.getEthnicityName();
									}
									
								}
									//raceEthinicity +=ethinicityMaster.getEthnicityName()+", ";
							}
						}				
					}
					
					if(mapDspqRouterByFID.get(10)!=null)
					{
						DspqRouter dspqRouter=mapDspqRouterByFID.get(10);
						if(dspqRouter!=null)
						{
							Map<Integer, Boolean> mapRaceId=new HashMap<Integer, Boolean>();
							String raceIds=null;
							if(teacherpersonalinfo!=null && teacherpersonalinfo.getRaceId()!=null)
							{
								raceIds=teacherpersonalinfo.getRaceId();
								if(raceIds!=null && !raceIds.equals(""))
								{
									String sRaceIdArr[]=raceIds.split(",");
									if(sRaceIdArr!=null && sRaceIdArr.length>0)
									{
										for(String sRaceIsTemp:sRaceIdArr)
										{
											int IracetTemp=Utility.getIntValue(sRaceIsTemp);
											mapRaceId.put(IracetTemp, true);
										}
									}
								}
								
							}
							
							for(RaceMaster rm:listRaceMasters)
							{
								if(mapRaceId!=null && mapRaceId.get(rm.getRaceId())!=null){
									if(counter==0){
										raceEthinicity = rm.getRaceName();
										counter++;
									}else{
										raceEthinicity +=", "+rm.getRaceName();
									}
									
								}
									//raceEthinicity +=rm.getRaceName()+", ";							
							}
						}
					}
					sb.append("<div class='col-sm-2 col-md-2'>"+raceEthinicity+"</div>");
					if(mapDspqRouterByFID.get(11)!=null)
					{
						DspqRouter dspqRouter=mapDspqRouterByFID.get(11);
						if(dspqRouter!=null)
						{
							sb.append("<div class='col-sm-2 col-md-2 divprofile'>"+dspqRouter.getDisplayName()+":</div>");
							Integer genderId=null;
							if(teacherpersonalinfo!=null && teacherpersonalinfo.getGenderId()!=null)
								genderId=teacherpersonalinfo.getGenderId().getGenderId();
							
							for(GenderMaster genderMaster:lstGenderMasters)
							{
								if(genderId!=null && genderId.equals(genderMaster.getGenderId()))
									sb.append("<div class='col-sm-2 col-md-2'>"+genderMaster.getGenderName()+"</div>");
								
							}
						}
					}
					/*customQuestion="";
					customQuestion=getCustomFieldBySectionIdProfiler(dspqPotfolioDM, candidateType, dspqGroupMaster, dspqRouterSection3.getDspqSectionMaster(),listToolTips,sSectionName,listCalDated,dspqPortfolioName,teacherDetail);
					if(customQuestion!="" && customQuestion.equalsIgnoreCase("")){
						sb.append("</div>");
						sb.append(customQuestion);
						sb.append("<div class='row'>");
					}*/
					//sb.append("</div>");
				}
				/*//Employee Number
				if(mapDspqRouterByFID.get(128)!=null)
				{
					DspqRouter dspqRouter128=mapDspqRouterByFID.get(128);
					if(dspqRouter128!=null)
					{
						//sb.append("<div class='row'>");
						sb.append("<div class='col-sm-2 col-md-2 divprofile'>"+dspqRouter128.getDisplayName()+":</div>");
						String sEmployeeNumber="";
						if(teacherpersonalinfo!=null && teacherpersonalinfo.getEmployeeNumber()!=null)
							sEmployeeNumber=teacherpersonalinfo.getEmployeeNumber().trim();					
						
							sb.append("<div class='col-sm-2 col-md-2'>"+sEmployeeNumber+"</div>");
						
						//sb.append("</div>");
					}
				}*/
			
	   }
		return sb.toString();
  }	
	public void setSIDAndFIDInMapProfiler(Map<Integer, DspqRouter> mapDspqRouterByFID,Map<Integer, DspqRouter> mapDspqRouterBySID,List<DspqRouter> dspqRouterList)
	{
		if(dspqRouterList!=null && dspqRouterList.size()>0)
		{
			for(DspqRouter dspqRouter :dspqRouterList)
			{
				if(dspqRouter.getDspqFieldMaster()!=null)
					mapDspqRouterByFID.put(dspqRouter.getDspqFieldMaster().getDspqFieldId(), dspqRouter);
				else if(dspqRouter.getDspqSectionMaster()!=null)
					mapDspqRouterBySID.put(dspqRouter.getDspqSectionMaster().getSectionId(), dspqRouter);
			}
		}
	}
	@Transactional(readOnly=false)
	public String getCustomFieldBySectionIdProfiler(List<DistrictMaster> dspqPotfolioDM,String sCandidateType,DspqSectionMaster dspqSectionMaster,List<String> listToolTips,String sSectionNameCustFld,List<String> listDateCals,List<DspqPortfolioName> lstdspqPortfolioName,TeacherDetail teacherDetail,int iOldQuestionDisplaySectionId,int jobId)
	{
		StringBuffer sb=new StringBuffer();
		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		boolean editableStatus=false;
		UserMaster userMaster=null;
		int counter=0;
		String checkedValue="";
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		if(session.getAttribute("userMaster")!=null){
			userMaster=(UserMaster)session.getAttribute("userMaster");
			if(userMaster!=null && (userMaster.getDistrictId()!=null && (userMaster.getDistrictId().getSelfServiceCustomQuestion()!=null && userMaster.getDistrictId().getSelfServiceCustomQuestion()==1))){
				editableStatus=true;
			}
		}
		
		if(iOldQuestionDisplaySectionId==0 || (iOldQuestionDisplaySectionId >0 && iOldQuestionDisplaySectionId==dspqSectionMaster.getSectionId()))
		{
			
			
		
		
		JobOrder jobOrder=null;
		if(jobId!=0)
		{
			jobOrder=jobOrderDAO.findById(jobId, false, false);
		}
		List<DistrictSpecificPortfolioQuestions> lstDistrictSpecificPortfolioQuestions=null;
		String candidateType="";
		if(sCandidateType.equalsIgnoreCase("")){
		}else{
			candidateType=validateApplicantType(sCandidateType);
		}
		if(iOldQuestionDisplaySectionId ==0){
			lstDistrictSpecificPortfolioQuestions=null;
			
			lstDistrictSpecificPortfolioQuestions=districtSpecificPortfolioQuestionsDAO.getCustomFieldBySection(dspqPotfolioDM,dspqSectionMaster,candidateType,lstdspqPortfolioName);
		}
		else{
			//int dspqType=0;	
			Map<Integer,List<DistrictSpecificPortfolioQuestions>> dSPQ=districtSpecificPortfolioQuestionsDAO.getDistrictSpecificPortfolioQuestion(jobOrder,false);
			try{
				for(int i=0;i<5;i++){
					if(dSPQ.get(i)!=null)
					{
						lstDistrictSpecificPortfolioQuestions=dSPQ.get(i);
						break;
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		
		}
			
			
		Map<Integer, DistrictSpecificPortfolioAnswers> mapQA=new HashMap<Integer, DistrictSpecificPortfolioAnswers>();
		List<DistrictSpecificPortfolioAnswers> lstDistrictSpecificPortfolioAnswers = new ArrayList<DistrictSpecificPortfolioAnswers>();
		List<DistrictSpecificPortfolioQuestions> districtSpecificPortfolioQuestionsFinal = new ArrayList<DistrictSpecificPortfolioQuestions>();
		DistrictSpecificPortfolioQuestions districtSpecificPortfolioQuestions=null;
		for(DistrictSpecificPortfolioQuestions rec:lstDistrictSpecificPortfolioQuestions){
			if(rec!=null && rec.getQuestionId()!=null){
				districtSpecificPortfolioQuestions=new DistrictSpecificPortfolioQuestions();
				districtSpecificPortfolioQuestions.setQuestionId(rec.getQuestionId());
				districtSpecificPortfolioQuestionsFinal.add(districtSpecificPortfolioQuestions);
			}
		}
		if(districtSpecificPortfolioQuestionsFinal!=null && districtSpecificPortfolioQuestionsFinal.size()>0)
		{
			lstDistrictSpecificPortfolioAnswers=districtSpecificPortfolioAnswersDAO.getDSPAByTID(teacherDetail, districtSpecificPortfolioQuestionsFinal);
		
			for(DistrictSpecificPortfolioAnswers obj:lstDistrictSpecificPortfolioAnswers)
			{
				mapQA.put(obj.getDistrictSpecificPortfolioQuestions().getQuestionId(), obj);
			}
		}
		if(districtSpecificPortfolioQuestionsFinal!=null && districtSpecificPortfolioQuestionsFinal.size()>0)
		{
			//sb.append("<div class='row'><div class='col-sm-12 col-md-12'>&nbsp;</div></div>");
			
			int iTotalCountNo_CustomField=lstDistrictSpecificPortfolioQuestions.size();
			sb.append("<input type='hidden' id='customFld_"+sSectionNameCustFld+"' customFldTotalCount='"+iTotalCountNo_CustomField+"' value="+iTotalCountNo_CustomField+">");
			int iCustFldCounter=0;			
			for(DistrictSpecificPortfolioQuestions dspq:lstDistrictSpecificPortfolioQuestions)
			{
				if(allCustomQuestionIdSB!=null && !allCustomQuestionIdSB.toString().equalsIgnoreCase(""))
					allCustomQuestionIdSB+=(","+dspq.getQuestionId());
				else
					allCustomQuestionIdSB+=(dspq.getQuestionId());
					
				sb.append("<div class='row top5'>");
				//sb.append("</div></div>");
				
				iCustFldCounter++;
				List<DistrictSpecificPortfolioOptions> lstDistrictSpecificPortfolioOptions=dspq.getQuestionOptions();
				String sShortName=dspq.getQuestionTypeMaster().getQuestionTypeShortName();
				
				if(dspq!=null)
				{
					String sOptionId="_"+dspq.getQuestionId();
					
					String sTextAReaValue="";
					DistrictSpecificPortfolioAnswers Qans=null;
					if(mapQA!=null && mapQA.get(dspq.getQuestionId())!=null)
					{
						Qans=mapQA.get(dspq.getQuestionId());
						if(Qans!=null)
						{
							if(Qans.getInsertedText()!=null && !Qans.getInsertedText().equals(""))
							{
								sTextAReaValue=Qans.getInsertedText();
							}
						}
					}				
					String sQuestionText=getLabelAndCustomFieldAttributes(dspq, listToolTips,iCustFldCounter,sSectionNameCustFld,Qans);
					if(sQuestionText!=null && sQuestionText.length()>100)
					{
						sb.append("<div class='col-sm-12 col-md-12 divprofile'>");
					}
					else
					{
						sb.append("<div class='col-sm-2 col-md-2 divprofile'>");
					}
						sb.append(sQuestionText);
						sb.append(":</div>");
					sb.append("<div class='col-sm-10 col-md-10 pright20'>");
					if(sShortName.equalsIgnoreCase("ml") || sShortName.equalsIgnoreCase("et"))
					{
						if(!editableStatus){
							if(sTextAReaValue.equalsIgnoreCase(""))
								sb.append(lblNA);
							else
								sb.append(sTextAReaValue);
						}
						else{
							sb.append("<div id='divOpt"+sOptionId+"'>");
							sb.append("<textarea name='txtAreaOpt"+sOptionId+"' id='txtAreaOpt"+sOptionId+"' rows='8' style='width:100%;' class='form-control' maxlength='5000' onkeyup=\"return chkLenOfTextArea(this,5000)\" onblur=\"return chkLenOfTextArea(this,5000)\">"+sTextAReaValue+"</textarea><br/>");
							sb.append("</div>");
						}
						/*
						//sb.append("<textarea name='txtAreaOpt"+sOptionId+"' id='txtAreaOpt"+sOptionId+"' value='"+sTextAReaValue+"' class='form-control' maxlength='5000' onkeyup=\"return chkLenOfTextArea(this,5000)\" onblur=\"return chkLenOfTextArea(this,5000)\"/>"+sTextAReaValue+"</textarea><br/>");
						sb.append("<textarea "+enableDisableFields+" name='txtAreaOpt"+sOptionId+"' id='txtAreaOpt"+sOptionId+"' rows='8' style='width:100%;' class='form-control' maxlength='5000' onkeyup=\"return chkLenOfTextArea(this,5000)\" onblur=\"return chkLenOfTextArea(this,5000)\">"+sTextAReaValue+"</textarea><br/>");
						//sb.append("<div class='jqte'><div class='jqte_toolbar' role='toolbar' unselectable='on' style='-webkit-user-select: none;'><div class='jqte_tool jqte_tool_1' role='button' data-tool='0' unselectable='on' style='-webkit-user-select: none;'><a class='jqte_tool_icon' unselectable='on' style='-webkit-user-select: none;'></a><div class='jqte_fontsizes' unselectable='on' style='position: absolute; display: none; -webkit-user-select: none;'><a jqte-styleval='10' class='jqte_fontsize' style='font-size: 10px; -webkit-user-select: none;' role='menuitem' unselectable='on'>Abcdefgh...</a><a jqte-styleval='12' class='jqte_fontsize' style='font-size: 12px; -webkit-user-select: none;' role='menuitem' unselectable='on'>Abcdefgh...</a><a jqte-styleval='16' class='jqte_fontsize' style='font-size: 16px; -webkit-user-select: none;' role='menuitem' unselectable='on'>Abcdefgh...</a><a jqte-styleval='18' class='jqte_fontsize' style='font-size: 18px; -webkit-user-select: none;' role='menuitem' unselectable='on'>Abcdefgh...</a><a jqte-styleval='20' class='jqte_fontsize' style='font-size: 20px; -webkit-user-select: none;' role='menuitem' unselectable='on'>Abcdefgh...</a><a jqte-styleval='24' class='jqte_fontsize' style='font-size: 24px; -webkit-user-select: none;' role='menuitem' unselectable='on'>Abcdefgh...</a><a jqte-styleval='28' class='jqte_fontsize' style='font-size: 28px; -webkit-user-select: none;' role='menuitem' unselectable='on'>Abcdefgh...</a></div></div><div class='jqte_tool jqte_tool_2' role='button' data-tool='1' unselectable='on' style='-webkit-user-select: none;'><a class='jqte_tool_icon' unselectable='on' style='-webkit-user-select: none;'></a><div class='jqte_cpalette' unselectable='on' style='position: absolute; display: none; -webkit-user-select: none;'><a jqte-styleval='0,0,0' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(0, 0, 0);' role='gridcell' unselectable='on'></a><a jqte-styleval='68,68,68' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(68, 68, 68);' role='gridcell' unselectable='on'></a><a jqte-styleval='102,102,102' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(102, 102, 102);' role='gridcell' unselectable='on'></a><a jqte-styleval='153,153,153' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(153, 153, 153);' role='gridcell' unselectable='on'></a><a jqte-styleval='204,204,204' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(204, 204, 204);' role='gridcell' unselectable='on'></a><a jqte-styleval='238,238,238' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(238, 238, 238);' role='gridcell' unselectable='on'></a><a jqte-styleval='243,243,243' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(243, 243, 243);' role='gridcell' unselectable='on'></a><a jqte-styleval='255,255,255' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(255, 255, 255);' role='gridcell' unselectable='on'></a><a jqte-styleval='244,204,204' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(244, 204, 204);' role='gridcell' unselectable='on'></a><a jqte-styleval='252,229,205' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(252, 229, 205);' role='gridcell' unselectable='on'></a><a jqte-styleval='255,242,204' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(255, 242, 204);' role='gridcell' unselectable='on'></a><a jqte-styleval='217,234,211' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(217, 234, 211);' role='gridcell' unselectable='on'></a><a jqte-styleval='208,224,227' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(208, 224, 227);' role='gridcell' unselectable='on'></a><a jqte-styleval='207,226,243' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(207, 226, 243);' role='gridcell' unselectable='on'></a><a jqte-styleval='217,210,233' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(217, 210, 233);' role='gridcell' unselectable='on'></a><a jqte-styleval='234,209,220' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(234, 209, 220);' role='gridcell' unselectable='on'></a><a jqte-styleval='234,153,153' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(234, 153, 153);' role='gridcell' unselectable='on'></a><a jqte-styleval='249,203,156' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(249, 203, 156);' role='gridcell' unselectable='on'></a><a jqte-styleval='255,229,153' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(255, 229, 153);' role='gridcell' unselectable='on'></a><a jqte-styleval='182,215,168' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(182, 215, 168);' role='gridcell' unselectable='on'></a><a jqte-styleval='162,196,201' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(162, 196, 201);' role='gridcell' unselectable='on'></a><a jqte-styleval='159,197,232' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(159, 197, 232);' role='gridcell' unselectable='on'></a><a jqte-styleval='180,167,214' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(180, 167, 214);' role='gridcell' unselectable='on'></a><a jqte-styleval='213,166,189' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(213, 166, 189);' role='gridcell' unselectable='on'></a><a jqte-styleval='224,102,102' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(224, 102, 102);' role='gridcell' unselectable='on'></a><a jqte-styleval='246,178,107' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(246, 178, 107);' role='gridcell' unselectable='on'></a><a jqte-styleval='255,217,102' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(255, 217, 102);' role='gridcell' unselectable='on'></a><a jqte-styleval='147,196,125' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(147, 196, 125);' role='gridcell' unselectable='on'></a><a jqte-styleval='118,165,175' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(118, 165, 175);' role='gridcell' unselectable='on'></a><a jqte-styleval='111,168,220' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(111, 168, 220);' role='gridcell' unselectable='on'></a><a jqte-styleval='142,124,195' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(142, 124, 195);' role='gridcell' unselectable='on'></a><a jqte-styleval='194,123,160' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(194, 123, 160);' role='gridcell' unselectable='on'></a><a jqte-styleval='255,0,0' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(255, 0, 0);' role='gridcell' unselectable='on'></a><a jqte-styleval='255,153,0' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(255, 153, 0);' role='gridcell' unselectable='on'></a><a jqte-styleval='255,255,0' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(255, 255, 0);' role='gridcell' unselectable='on'></a><a jqte-styleval='0,255,0' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(0, 255, 0);' role='gridcell' unselectable='on'></a><a jqte-styleval='0,255,255' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(0, 255, 255);' role='gridcell' unselectable='on'></a><a jqte-styleval='0,0,255' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(0, 0, 255);' role='gridcell' unselectable='on'></a><a jqte-styleval='153,0,255' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(153, 0, 255);' role='gridcell' unselectable='on'></a><a jqte-styleval='255,0,255' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(255, 0, 255);' role='gridcell' unselectable='on'></a><a jqte-styleval='204,0,0' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(204, 0, 0);' role='gridcell' unselectable='on'></a><a jqte-styleval='230,145,56' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(230, 145, 56);' role='gridcell' unselectable='on'></a><a jqte-styleval='241,194,50' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(241, 194, 50);' role='gridcell' unselectable='on'></a><a jqte-styleval='106,168,79' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(106, 168, 79);' role='gridcell' unselectable='on'></a><a jqte-styleval='69,129,142' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(69, 129, 142);' role='gridcell' unselectable='on'></a><a jqte-styleval='61,133,198' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(61, 133, 198);' role='gridcell' unselectable='on'></a><a jqte-styleval='103,78,167' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(103, 78, 167);' role='gridcell' unselectable='on'></a><a jqte-styleval='166,77,121' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(166, 77, 121);' role='gridcell' unselectable='on'></a><a jqte-styleval='153,0,0' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(153, 0, 0);' role='gridcell' unselectable='on'></a><a jqte-styleval='180,95,6' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(180, 95, 6);' role='gridcell' unselectable='on'></a><a jqte-styleval='191,144,0' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(191, 144, 0);' role='gridcell' unselectable='on'></a><a jqte-styleval='56,118,29' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(56, 118, 29);' role='gridcell' unselectable='on'></a><a jqte-styleval='19,79,92' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(19, 79, 92);' role='gridcell' unselectable='on'></a><a jqte-styleval='11,83,148' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(11, 83, 148);' role='gridcell' unselectable='on'></a><a jqte-styleval='53,28,117' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(53, 28, 117);' role='gridcell' unselectable='on'></a><a jqte-styleval='116,27,71' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(116, 27, 71);' role='gridcell' unselectable='on'></a><a jqte-styleval='102,0,0' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(102, 0, 0);' role='gridcell' unselectable='on'></a><a jqte-styleval='120,63,4' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(120, 63, 4);' role='gridcell' unselectable='on'></a><a jqte-styleval='127,96,0' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(127, 96, 0);' role='gridcell' unselectable='on'></a><a jqte-styleval='39,78,19' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(39, 78, 19);' role='gridcell' unselectable='on'></a><a jqte-styleval='12,52,61' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(12, 52, 61);' role='gridcell' unselectable='on'></a><a jqte-styleval='7,55,99' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(7, 55, 99);' role='gridcell' unselectable='on'></a><a jqte-styleval='32,18,77' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(32, 18, 77);' role='gridcell' unselectable='on'></a><a jqte-styleval='76,17,48' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(76, 17, 48);' role='gridcell' unselectable='on'></a></div></div><div class='jqte_tool jqte_tool_3' role='button' data-tool='2' unselectable='on' style='-webkit-user-select: none;'><a class='jqte_tool_icon' unselectable='on' style='-webkit-user-select: none;'></a></div><div class='jqte_tool jqte_tool_4' role='button' data-tool='3' unselectable='on' style='-webkit-user-select: none;'><a class='jqte_tool_icon' unselectable='on' style='-webkit-user-select: none;'></a></div><div class='jqte_tool jqte_tool_5' role='button' data-tool='4' unselectable='on' style='-webkit-user-select: none;'><a class='jqte_tool_icon' unselectable='on' style='-webkit-user-select: none;'></a></div><div class='jqte_tool jqte_tool_6' role='button' data-tool='5' unselectable='on' style='-webkit-user-select: none;'><a class='jqte_tool_icon' unselectable='on' style='-webkit-user-select: none;'></a></div><div class='jqte_tool jqte_tool_7' role='button' data-tool='6' unselectable='on' style='-webkit-user-select: none;'><a class='jqte_tool_icon' unselectable='on' style='-webkit-user-select: none;'></a></div><div class='jqte_tool jqte_tool_8' role='button' data-tool='7' unselectable='on' style='-webkit-user-select: none;'><a class='jqte_tool_icon' unselectable='on' style='-webkit-user-select: none;'></a></div><div class='jqte_tool jqte_tool_9' role='button' data-tool='8' unselectable='on' style='-webkit-user-select: none;'><a class='jqte_tool_icon' unselectable='on' style='-webkit-user-select: none;'></a></div><div class='jqte_tool jqte_tool_10' role='button' data-tool='9' unselectable='on' style='-webkit-user-select: none;'><a class='jqte_tool_icon' unselectable='on' style='-webkit-user-select: none;'></a></div><div class='jqte_tool jqte_tool_11' role='button' data-tool='10' unselectable='on' style='-webkit-user-select: none;'><a class='jqte_tool_icon' unselectable='on' style='-webkit-user-select: none;'></a></div><div class='jqte_tool jqte_tool_12' role='button' data-tool='11' unselectable='on' style='-webkit-user-select: none;'><a class='jqte_tool_icon' unselectable='on' style='-webkit-user-select: none;'></a></div><div class='jqte_tool jqte_tool_13' role='button' data-tool='12' unselectable='on' style='-webkit-user-select: none;'><a class='jqte_tool_icon' unselectable='on' style='-webkit-user-select: none;'></a></div><div class='jqte_tool jqte_tool_14' role='button' data-tool='13' unselectable='on' style='-webkit-user-select: none;'><a class='jqte_tool_icon' unselectable='on' style='-webkit-user-select: none;'></a></div><div class='jqte_tool jqte_tool_15' role='button' data-tool='14' unselectable='on' style='-webkit-user-select: none;'><a class='jqte_tool_icon' unselectable='on' style='-webkit-user-select: none;'></a></div><div class='jqte_tool jqte_tool_16' role='button' data-tool='15' unselectable='on' style='-webkit-user-select: none;'><a class='jqte_tool_icon' unselectable='on' style='-webkit-user-select: none;'></a></div><div class='jqte_tool jqte_tool_17' role='button' data-tool='16' unselectable='on' style='-webkit-user-select: none;'><a class='jqte_tool_icon' unselectable='on' style='-webkit-user-select: none;'></a></div></div><div class='jqte_linkform' style='display:none' role='dialog'><div class='jqte_linktypeselect' unselectable='on' style='-webkit-user-select: none;'><div class='jqte_linktypeview' unselectable='on' style='-webkit-user-select: none;'><div class='jqte_linktypearrow' unselectable='on' style='-webkit-user-select: none;'></div><div class='jqte_linktypetext'>Web Address</div></div><div class='jqte_linktypes' role='menu' unselectable='on' style='-webkit-user-select: none;'><a jqte-linktype='0' unselectable='on' style='-webkit-user-select: none;'>Web Address</a><a jqte-linktype='1' unselectable='on' style='-webkit-user-select: none;'>E-mail Address</a><a jqte-linktype='2' unselectable='on' style='-webkit-user-select: none;'>Picture URL</a></div></div> <input class='jqte_linkinput' type='text/css' value=''> <div class='jqte_linkbutton' unselectable='on' style='-webkit-user-select: none;'>OK</div> <div style='height:1px;float:none;clear:both'></div></div><div class='jqte_editor' contenteditable='true'></div><div class='jqte_source jqte_hiddenField'></div></div>");
						sb.append("</div>");*/
					}
					else if(sShortName.equalsIgnoreCase("mlsel"))
					{
						Map<Integer,Boolean> mapOption=new HashMap<Integer, Boolean>();
						String sListValue="";
						if(Qans!=null && Qans.getSelectedOptions()!=null && !Qans.getSelectedOptions().equals(""))
						{
							sListValue=Qans.getSelectedOptions();
							String arrOp[]=sListValue.split("\\|");
							if(arrOp.length >0)
							{
								for(String sOpt :arrOp)
								{
									int iOpt=Utility.getIntValue(sOpt);
									mapOption.put(iOpt, true);
								}
							}
						}
						counter=0;
						checkedValue="";
						for (DistrictSpecificPortfolioOptions dqo : lstDistrictSpecificPortfolioOptions) 
						{
							//sb.append("<label class='checkbox inline' style='margin-top: 0px;margin-bottom: 2px;'>");
							if(!editableStatus){
								if(mapOption!=null && mapOption.get(dqo.getOptionId())!=null){
									if(counter==0){
										checkedValue=dqo.getQuestionOption();
										counter++;
									}else{
										checkedValue+=", "+dqo.getQuestionOption();
									}
									//sb.append("<input type='checkbox' "+enableDisableFields+" checked name='opt"+sOptionId+"' value='"+dqo.getOptionId()+"'>"+dqo.getQuestionOption()+"</br>");
								}
							}else{
								sb.append("<label class='checkbox inline' style='margin-top: 0px;margin-bottom: 2px;'>");
								if(mapOption!=null && mapOption.get(dqo.getOptionId())!=null)
									sb.append("<input type='checkbox' checked name='opt"+sOptionId+"' value='"+dqo.getOptionId()+"'>"+dqo.getQuestionOption()+"</br>");
								else
									sb.append("<input type='checkbox' name='opt"+sOptionId+"' value='"+dqo.getOptionId()+"'>"+dqo.getQuestionOption()+"</br>");
								sb.append("</label>");
							}
							//sb.append("</label>");
						}
						if(!editableStatus){
							if(checkedValue.equalsIgnoreCase(""))
								sb.append(lblNA);
							else
								sb.append(checkedValue);
						}
					}
					else if(sShortName.equalsIgnoreCase("mloet"))
					{
						Map<Integer,Boolean> mapOption=new HashMap<Integer, Boolean>();
						String sListValue="";
						if(Qans!=null && Qans.getSelectedOptions()!=null && !Qans.getSelectedOptions().equals(""))
						{
							sListValue=Qans.getSelectedOptions();
							String arrOp[]=sListValue.split("\\|");
							if(arrOp.length >0)
							{
								for(String sOpt :arrOp)
								{
									int iOpt=Utility.getIntValue(sOpt);
									mapOption.put(iOpt, true);
								}
							}
						}
						counter=0;
						checkedValue="";
						for (DistrictSpecificPortfolioOptions dqo : lstDistrictSpecificPortfolioOptions)
						{
							if(!editableStatus){
								if(mapOption!=null && mapOption.get(dqo.getOptionId())!=null){
									if(counter==0){
										checkedValue=dqo.getQuestionOption();
										counter++;
									}else{
										checkedValue+=", "+dqo.getQuestionOption();
									}
								}
									
							}else{
							sb.append("<label class='checkbox inline' style='margin-top: 0px;margin-bottom: 2px;'>");
							if(mapOption!=null && mapOption.get(dqo.getOptionId())!=null)
								sb.append("<input type='checkbox' checked name='opt"+sOptionId+"' value='"+dqo.getOptionId()+"'>"+dqo.getQuestionOption()+"</br>");
							else
								sb.append("<input type='checkbox' name='opt"+sOptionId+"' value='"+dqo.getOptionId()+"'>"+dqo.getQuestionOption()+"</br>");
							
							sb.append("</label>");
							}
						}
						if(!editableStatus){
							if(checkedValue.equalsIgnoreCase(""))
								sb.append(lblNA);
							else
								sb.append(checkedValue);
							
							sb.append("</br>"+sTextAReaValue);
						}else{
						
						sb.append("<div id='divOpt"+sOptionId+"'>");
						sb.append("&nbsp;&nbsp;<textarea  name='txtAreaOpt"+sOptionId+"' id='txtAreaOpt"+sOptionId+"' rows='8' style='width:100%;' class='form-control' maxlength='5000' onkeyup=\"return chkLenOfTextArea(this,5000)\" onblur=\"return chkLenOfTextArea(this,5000)\">"+sTextAReaValue+"</textarea><br/>");
						sb.append("</div>");
						}
					}
					else if(sShortName.equalsIgnoreCase("sloet"))
					{
						Map<Integer,Boolean> mapOption=new HashMap<Integer, Boolean>();
						String sListValue="";
						if(Qans!=null && Qans.getSelectedOptions()!=null && !Qans.getSelectedOptions().equals(""))
						{
							sListValue=Qans.getSelectedOptions();
							String arrOp[]=sListValue.split("\\|");
							if(arrOp.length >0)
							{
								for(String sOpt :arrOp)
								{
									int iOpt=Utility.getIntValue(sOpt);
									mapOption.put(iOpt, true);
								}
							}
						}
						counter=0;
						checkedValue="";
						for (DistrictSpecificPortfolioOptions dqo : lstDistrictSpecificPortfolioOptions)
						{
							if(!editableStatus){
								if(mapOption!=null && mapOption.get(dqo.getOptionId())!=null){
									if(counter==0){
										checkedValue=dqo.getQuestionOption();
										counter++;
									}else{
										checkedValue+=", "+dqo.getQuestionOption();
									}
								}
							}else{
								sb.append("<label class='radio' style='margin-top: 0px;margin-bottom: 2px;'>");
								if(mapOption!=null && mapOption.get(dqo.getOptionId())!=null)
									sb.append("<input checked type='radio' name='opt"+sOptionId+"' value='"+dqo.getOptionId()+"'>"+dqo.getQuestionOption()+"</br>");
								else
									sb.append("<input type='radio' name='opt"+sOptionId+"' value='"+dqo.getOptionId()+"'>"+dqo.getQuestionOption()+"</br>");
								sb.append("</label>");
							}
						}
						if(!editableStatus){
							if(checkedValue.equalsIgnoreCase(""))
								sb.append(lblNA);
							else
								sb.append(checkedValue);
							
							sb.append(sTextAReaValue);
						}else{
						sb.append("<div id='divOpt"+sOptionId+"'>");
						sb.append("&nbsp;&nbsp;<textarea  name='txtAreaOpt"+sOptionId+"' id='txtAreaOpt"+sOptionId+"' rows='8' style='width:100%;' class='form-control' maxlength='5000' onkeyup=\"return chkLenOfTextArea(this,5000)\" onblur=\"return chkLenOfTextArea(this,5000)\">"+sTextAReaValue+"</textarea><br/>");
						sb.append("</div>");
						}
					}
					else if(sShortName.equalsIgnoreCase("tf") || sShortName.equalsIgnoreCase("slsel"))
					{
						Map<Integer,Boolean> mapOption=new HashMap<Integer, Boolean>();
						String sListValue="";
						if(Qans!=null && Qans.getSelectedOptions()!=null && !Qans.getSelectedOptions().equals(""))
						{
							sListValue=Qans.getSelectedOptions();
							String arrOp[]=sListValue.split("\\|");
							if(arrOp.length >0)
							{
								for(String sOpt :arrOp)
								{
									int iOpt=Utility.getIntValue(sOpt);
									mapOption.put(iOpt, true);
								}
							}
						}
						checkedValue="";
						counter=0;
						for (DistrictSpecificPortfolioOptions dqo : lstDistrictSpecificPortfolioOptions)
						{
							if(!editableStatus){
								if(mapOption!=null && mapOption.get(dqo.getOptionId())!=null){
									if(counter==0){
										checkedValue=dqo.getQuestionOption();
										counter++;
									}else{
										checkedValue+=", "+dqo.getQuestionOption();
									}
								}
							}else{
								sb.append("<label class='radio' style='margin-top: 0px;margin-bottom: 2px;'>");
								if(mapOption!=null && mapOption.get(dqo.getOptionId())!=null)
									sb.append("<input type='radio' checked name='opt"+sOptionId+"' value='"+dqo.getOptionId()+"'>"+dqo.getQuestionOption()+"</br>");
								else
									sb.append("<input type='radio' name='opt"+sOptionId+"' value='"+dqo.getOptionId()+"'>"+dqo.getQuestionOption()+"</br>");
								sb.append("</label>");
							}
						}
						if(!editableStatus){
							if(checkedValue.equalsIgnoreCase(""))
								sb.append(lblNA);
							else
								sb.append(checkedValue);
						}
					}
					else if(sShortName.equalsIgnoreCase("drsls") || sShortName.equalsIgnoreCase("DD"))
					{
						if(lstDistrictSpecificPortfolioOptions!=null)
						{
							String sListValue="";
							if(Qans!=null && Qans.getSelectedOptions()!=null && !Qans.getSelectedOptions().equals(""))
								sListValue=Qans.getSelectedOptions();
							String answerValue="";
							if(editableStatus){
								sb.append("<select name='opt"+sOptionId+"' id='opt"+sOptionId+"' class='form-control' style='width: 325px;'>");
								sb.append("<option value='0'>Select "+dspq.getQuestion()+"</option>");
							}
							for (DistrictSpecificPortfolioOptions dqo : lstDistrictSpecificPortfolioOptions)
							{
								if(!editableStatus){									
									if(sListValue!=null && !sListValue.equals("") && sListValue.equals(dqo.getOptionId()+"")){
										answerValue=dqo.getQuestionOption();
										sb.append(answerValue);
									}
									
								}else{
									if(sListValue!=null && !sListValue.equals("") && sListValue.equals(dqo.getOptionId()+""))
										sb.append("<option selected value='"+dqo.getOptionId()+"'>"+dqo.getQuestionOption()+"</option>");
									else
										sb.append("<option value='"+dqo.getOptionId()+"'>"+dqo.getQuestionOption()+"</option>");
									
								}
									
							}
							if(!editableStatus && answerValue.equalsIgnoreCase(""))								
								sb.append(lblNA);
							else
								sb.append("</select>");
							
						}
					}
					else if(sShortName.equalsIgnoreCase("OSONP"))
					{
						Map<Integer,Boolean> mapOption=new HashMap<Integer, Boolean>();
						String sListValue="";
						if(Qans!=null && Qans.getSelectedOptions()!=null && !Qans.getSelectedOptions().equals(""))
						{
							sListValue=Qans.getSelectedOptions();							
							String arrOp[]=sListValue.split("\\|");
							if(arrOp.length >0)
							{
								for(String sOpt :arrOp)
								{
									int iOpt=Utility.getIntValue(sOpt);
									mapOption.put(iOpt, true);
								}
							}							
						}
						String answerValue="";
						for (DistrictSpecificPortfolioOptions dqo : lstDistrictSpecificPortfolioOptions)
						{
							if(!editableStatus){									
								
								if(mapOption!=null && mapOption.get(dqo.getOptionId())!=null){
									answerValue=dqo.getQuestionOption();
									sb.append(answerValue);
								}
								
							}else{
								sb.append("<label class='radio' style='margin-top: 0px;margin-bottom: 2px;'>");
								if(mapOption!=null && mapOption.get(dqo.getOptionId())!=null)
									sb.append("<input type='radio' checked name='opt"+sOptionId+"' value='"+dqo.getOptionId()+"'>"+dqo.getQuestionOption()+"</br>");
								else
									sb.append("<input type='radio' name='opt"+sOptionId+"' value='"+dqo.getOptionId()+"'>"+dqo.getQuestionOption()+"</br>");
								sb.append("</label>");								
							}
							
						}
						if(!editableStatus && answerValue.equalsIgnoreCase(""))								
							sb.append(lblNA);
						
					}
					else if(sShortName.equalsIgnoreCase("dt"))
					{
						String sDateValue="";
						if(Qans!=null && Qans.getSelectedOptions()!=null && !Qans.getSelectedOptions().equals("")){
							sDateValue=Qans.getSelectedOptions();
							Date date = null;		
							SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
							if(!editableStatus){
								if(sDateValue!=null){
							    try {
									date = df.parse(sDateValue);
								} catch (ParseException e) {
									e.printStackTrace();
								}
								sb.append(Utility.convertDateAndTimeToUSformatOnlyDate(date));
								}else
									sb.append(lblNA);
							}else{
								sb.append("<script type='text/javascript'>//<![CDATA["+
										"var cal = Calendar.setup({"+
										" onSelect: function(cal) { cal.hide() },"+
										" showTime: true});"+
										"cal.manageFields('opt"+sOptionId+"', 'opt"+sOptionId+"', '%m-%d-%Y');"+
										"//]]></script>");
								sb.append("<input type='text' value='"+sDateValue+"' id='opt"+sOptionId+"' name='opt"+sOptionId+"' class='form-control opt"+sOptionId+"' style='width: 450px;'> ");
							}
							
						}
						else
							sb.append(lblNA);
						listDateCals.add("opt"+sOptionId);
					}
					else if(sShortName.equalsIgnoreCase("sl"))
					{
						String sStringValue="";
						if(Qans!=null && Qans.getSelectedOptions()!=null && !Qans.getSelectedOptions().equals(""))
							sStringValue=Qans.getSelectedOptions();
						if(!editableStatus){
							if(!sStringValue.equalsIgnoreCase(""))
								sb.append(sStringValue);							
							else
								sb.append(lblNA);
						}else{
							sb.append("<input type='text' value='"+sStringValue+"' id='opt"+sOptionId+"' name='opt"+sOptionId+"' class='form-control' style='width: 325px;'> ");
						}
					}
					sb.append("</div></div>");
				}
			}
			//sb.append("<button class='btn btn-primary' type='button' id='saveCustom' onclick='saveCustomQuestion();' style='display: inline-block;'>&nbsp;&nbsp;&nbsp;&nbsp;Save&nbsp;<i class='icon'></i>&nbsp;&nbsp;&nbsp;&nbsp;</button>");
		}
		}
		return sb.toString();
	}
	public String getCustomFieldInstruction(DistrictSpecificPortfolioQuestions dspq)
	{
		StringBuffer sb=new StringBuffer();
		try {
			if(dspq.getQuestionInstructions()!=null && !dspq.getQuestionInstructions().equals(""))
			{
				/*sb.append("<div class='row'><div class='col-sm-12 col-md-12'>");
				sb.append("<span>"+dspq.getQuestionInstructions()+"</span>");
				sb.append("</div></div>");*/
				
				sb.append("<div class='col-sm-12 col-md-12 mt10'>");
				sb.append("<span>"+dspq.getQuestionInstructions()+"</span>");
				sb.append("</div>");
				
			}
		} catch (Exception e) {	}
		
		return sb.toString();
	}
	public String getLabelAndCustomFieldAttributes(DistrictSpecificPortfolioQuestions dspq,List<String> listToolTips,int iCustomFieldIdCounter,String sSectionNameCustFld,DistrictSpecificPortfolioAnswers Qans)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		boolean editableStatus=true;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		UserMaster userMaster=null;
		
		  if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
		   throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		  }
		  else{
		   userMaster=(UserMaster)session.getAttribute("userMaster");		 
		   if(userMaster!=null && (userMaster.getDistrictId()!=null && (userMaster.getDistrictId().getSelfServiceCustomQuestion()!=null && userMaster.getDistrictId().getSelfServiceCustomQuestion()==1))){
				editableStatus=true;
			}		   
		  }
		  String sReturnValue="";
		  int iOptionCount=0;
			if(dspq!=null && dspq.getQuestionOptions()!=null)
				iOptionCount=dspq.getQuestionOptions().size();
			
			int iAnswerId=0;
			if(Qans!=null && Qans.getAnswerId()!=null)
				iAnswerId=Qans.getAnswerId();
			
		  if(!editableStatus){		
				try {
					if(dspq.getQuestion()!=null && !dspq.getQuestion().equals(""))
						sReturnValue="<span id='lblCustomFieldId_"+dspq.getQuestionId()+"' customFieldQNo='"+dspq.getQuestionId()+"' customFieldShortCode='"+dspq.getQuestionTypeMaster().getQuestionTypeShortName()+"' customFieldRequired='"+dspq.getIsRequired()+"' customFieldOtionCount='"+iOptionCount+"' customFieldQuestionTypeId='"+dspq.getQuestionTypeMaster().getQuestionTypeId()+"' customFieldLabelAnswer='"+dspq.getQuestion()+"' customFieldAnswerID='"+iAnswerId+"'>"+dspq.getQuestion()+"</span>";
				} catch (Exception e) {
					e.printStackTrace();
				}

		  }
		  else{			
			
			try {
				if(dspq.getQuestion()!=null && !dspq.getQuestion().equals(""))
					sReturnValue="<span id='lblCustomFieldId_"+dspq.getQuestionId()+"' customFieldQNo='"+dspq.getQuestionId()+"' customFieldShortCode='"+dspq.getQuestionTypeMaster().getQuestionTypeShortName()+"' customFieldRequired='"+dspq.getIsRequired()+"' customFieldOtionCount='"+iOptionCount+"' customFieldQuestionTypeId='"+dspq.getQuestionTypeMaster().getQuestionTypeId()+"' customFieldLabelAnswer='"+dspq.getQuestion()+"' customFieldAnswerID='"+iAnswerId+"'>"+dspq.getQuestion()+"</span><span id='lblCustomFieldIdWarningBeforeSpace_"+dspq.getQuestionId()+"'></span><span id='lblCustomFieldIdWarning_"+dspq.getQuestionId()+"'></span><span id='lblCustomFieldIdWarningAfterSpace_"+dspq.getQuestionId()+"'></span>";
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			try {
				if(dspq.getIsRequired()==1)
					sReturnValue=sReturnValue+"<span class='required'>*</span>";
			} catch (Exception e) {
	
			}
			
			try {
				if(dspq.getTooltip()!=null && !dspq.getTooltip().equals(""))
				{
					sReturnValue=sReturnValue+"&nbsp;<a href='#' id='CustomFieldId_tooltip_"+dspq.getQuestionId()+"' rel='tooltip' data-original-title='"+dspq.getTooltip()+"'><img src='images/qua-icon.png' width='15' height='15' alt=''></a>";
					listToolTips.add("CustomFieldId_tooltip_"+dspq.getQuestionId());
				}
			} catch (Exception e) {}
		
		  }
		  
		return sReturnValue;
	}
	/*public String getGroupProfilerMaster(Integer groupId,String sCandidateType,Integer dspqPortfolioNameId,TeacherDetail teacherDetail){
		StringBuffer sb = new StringBuffer();	
		try{
			String candidateType="";
			if(sCandidateType.equalsIgnoreCase("")){
			}else{
				candidateType=validateApplicantType(sCandidateType);
			}
			DspqGroupMaster dspqGroupMaster=dspqGroupMasterDAO.findById(groupId, false, false);
			List<DspqSectionMaster> sectionMasters=dspqSectionMasterDAO.getDspqSectionListByGroup(dspqGroupMaster);
			List<DspqFieldMaster> dspqFieldList = dspqFieldMasterDAO.getFieldListBySections(sectionMasters);
			DspqPortfolioName dspqPortfolioName = dspqPortfolioNameDAO.findById(dspqPortfolioNameId, false, false);
			List<DspqRouter> dspqRouterList=dspqRouterDAO.getControlsByPortfolioNameAndCTAndField(dspqPortfolioName, candidateType, dspqFieldList,sectionMasters);
			DspqRouter dspqRouterSection=null;
			Map<Integer, DspqRouter> mapDspqRouterByFID=new HashMap<Integer, DspqRouter>();
			Map<Integer, DspqRouter> mapDspqRouterBySID=new HashMap<Integer, DspqRouter>();
			setSIDAndFIDInMapProfiler(mapDspqRouterByFID, mapDspqRouterBySID, dspqRouterList);
			List<DistrictSpecificPortfolioQuestions> districtSpecificPortfolioQuestions=null;
			int iOldQuestionDisplaySectionId = getSectionIdForShowOldQuestion(0, teacherDetail, candidateType, dspqPortfolioName, districtSpecificPortfolioQuestions);
			
			if(mapDspqRouterByFID!=null)
			{		
				DistrictMaster dspqPotfolioDM=null;
				if(dspqPortfolioName!=null && dspqPortfolioName.getDistrictMaster()!=null){
					dspqPotfolioDM=dspqPortfolioName.getDistrictMaster();
				 dspqRouterSection=mapDspqRouterBySID.get(6);
				 sb.append(getCustomFieldBySectionIdProfiler(dspqPotfolioDM,candidateType,dspqGroupMaster,dspqRouterSection.getDspqSectionMaster(),null,null,null,dspqPortfolioName,teacherDetail,iOldQuestionDisplaySectionId,districtSpecificPortfolioQuestions));
				}
				
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	}*/
	
	public String getPersonalInfoProfiler(String jobId,String candidateAndPortfolioId,Integer teacherId){
		StringBuffer sb=new StringBuffer();
		try{
			String candidateType="";
			String []portfolioTypeId=candidateAndPortfolioId.split("#");
			Integer portfolioId=Utility.getIntValue(portfolioTypeId[1]);
			String sCandidateType=portfolioTypeId[0];
			if(sCandidateType.equalsIgnoreCase("")){
			}else{
				candidateType=validateApplicantType(sCandidateType);
			}
			TeacherDetail teacherDetail = teacherDetailDAO.findById(teacherId, false, false);
		   sb.append("<div class='row'>");
	        String showImg="";
			//showImg="<img src=\"images/nophoto.png\" align='left' class='circle' style='width: 48px;height: 48px;margin: 0em auto;' >";
			sb.append("<div class='col-md-1 col-sm-1'>"+showImg+"</div>");
			sb.append("<div class='col-md-11 col-sm-11'  style='padding-left: 0px;'>");
			sb.append(getGroupProfiler(jobId, portfolioId, candidateType, 1 ,teacherDetail));		
			sb.append("</div>");
			sb.append("</div>");
	}catch (Exception e) {
		e.printStackTrace();
	}
	return sb.toString();
  }
	public String getAcademicsSectionProfiler(Integer teacherId,String candidateAndPortfolioId,String displayName){
		StringBuffer sb=new StringBuffer();
		try{
			String []portfolioTypeId=candidateAndPortfolioId.split("#");
			Integer portfolioId=Utility.getIntValue(portfolioTypeId[1]);
			String sCandidateType=portfolioTypeId[0];
			String candidateType="";
			if(sCandidateType.equalsIgnoreCase("")){
			}else{
				 candidateType=validateApplicantType(sCandidateType);
			}
			
			Integer sectionId=6;
			TeacherDetail teacherDetail = teacherDetailDAO.findById(teacherId, false, false);
			sb.append("<div class='left15'>");
			sb.append("<table>");
			sb.append("<tr><td colspan=5 class='pleft13 portfolio_Subheading'>"+displayName+"</td></tr>");
			sb.append("<tr><td colspan=5 width='100%'>");
				sb.append("<table border=0 width='100%' onmouseover=\"javascript:setGridProfileVariable('teacherAce')\">");
					sb.append("<tr>");
						sb.append("<td style="+sGridtableDisplayCSS+" id='gridDataTeacherAcademics'>");
						sb.append("</td>");
					sb.append("</tr>");
				sb.append("</table>"); 
			sb.append("</td></tr>");
			sb.append("<script>getTeacherAcademicsGrid_DivProfile_sspf("+teacherDetail.getTeacherId()+",'"+portfolioId+"','"+candidateType+"','"+sectionId+"');</script>");
			sb.append("</table>");
			sb.append("</div>");
		}catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	}
	public String getGeneralKnowladgeExamProfiler(Integer teacherId,String portfolioIdAndType,String displayName){
		StringBuffer sb=new StringBuffer();
		String strCss_value="";
		String strCss_lbl="";
		String windowFunc="if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
		try{
			String []portfolioTypeId=portfolioIdAndType.split("#");
			Integer portfolioId=Utility.getIntValue(portfolioTypeId[1]);
			String sCandidateType=portfolioTypeId[0];
			String candidateType=validateApplicantType(sCandidateType);
			Integer sectionId=15;
			TeacherDetail teacherDetail=teacherDetailDAO.findById(teacherId, false, false);
			// start new row   >>> General Knowledge Exam
			   int geFlag=0;
			   sb.append("<tr><td>&nbsp;<td></tr>");
			   try{
					TeacherGeneralKnowledgeExam teacherGeneralKnowledgeExam=getGKEValuesForProfile(teacherDetail);
					if(teacherGeneralKnowledgeExam!=null){
						sb.append("<tr><td colspan=5 class='pleft25 portfolio_Subheading'>&nbsp;"+displayName+":</td></tr>");
						sb.append("<tr>");
						sb.append("<td>");
							sb.append("<table border='0' cellspacing=0 cellpadding=0 class='tablecgtbl'>");
							sb.append("<tr>");
								strCss_value="padding-left:55px;";
								sb.append("<td style='"+strCss_value+"'>&nbsp;</td>");
								strCss_lbl="padding:2px;vertical-align:middle;";
								strCss_value="padding:2px;vertical-align:text-top;";
								sb.append("<td>"); 
								sb.append("<table border='0' cellspacing=0 cellpadding=0 class='tablecgtbl'>");
								sb.append("<tr>");
								strCss_lbl="padding:2px;padding-left:2px;vertical-align:middle;";
								//sb.append("<td class='divlable' style='"+strCss_lbl+"'>&nbsp;General Knowledge Exam: </td>");
								if(teacherGeneralKnowledgeExam!=null){
									geFlag=1;
									sb.append("<td class='divlable' style='"+strCss_lbl+";margin-top:-5px;'>"+lblExamStatus +"</td>");
								    sb.append("<td class='divlable' style='"+strCss_lbl+";margin-top:-5px;padding-left:60px;'>"+lblExamDate +"</td>");
								    sb.append("<td class='divlable' style='"+strCss_lbl+";margin-top:-5px;padding-left:60px;'>"+lblScoreReport+"</td>");
								    sb.append("<td class='divlable' style='"+strCss_lbl+";margin-top:-5px;padding-left:30px;'>"+lblNotes+"</td>");
								}
								sb.append("</tr>");
								sb.append("</table>");
							sb.append("</td>");
							sb.append("</tr>");
							sb.append("</table>");
						sb.append("</td>");
					   sb.append("</tr>");
					}else{
						sb.append("<tr><td colspan=5 class='pleft25 portfolio_Subheading'>&nbsp;"+displayName+":&nbsp;&nbsp;<span class='divlableval'>"+lblNA+"</span></td></tr>");
					}
			   }catch (Exception e) {
					e.printStackTrace();
				}
			 
			   if(geFlag==1){
				   sb.append("<tr>");
					sb.append("<td>");
						sb.append("<table border='0' cellspacing=0 cellpadding=0 class='tablecgtbl'>");
						sb.append("<tr>");
							strCss_value="padding-left:55px;";
							sb.append("<td style='"+strCss_value+"'>&nbsp;</td>");
							strCss_lbl="padding:2px;vertical-align:middle;";
							strCss_value="padding:2px;vertical-align:text-top;";
							sb.append("<td>"); 
							sb.append("<table border='0' cellspacing=0 cellpadding=0 class='tablecgtbl'>");
							sb.append("<tr>");
							strCss_lbl="padding:2px;padding-left:2px;vertical-align:middle;";
							try{
								TeacherGeneralKnowledgeExam teacherGeneralKnowledgeExam=getGKEValuesForProfile(teacherDetail);
								if(teacherGeneralKnowledgeExam!=null){
									if(teacherGeneralKnowledgeExam.getGeneralKnowledgeExamStatus().equals("P"))
										sb.append("<td class='divlableval' style='"+strCss_value+";margin-top:-5px;'>"+lblPass+"</td>");
									else if(teacherGeneralKnowledgeExam.getGeneralKnowledgeExamStatus().equals("F"))	
										sb.append("<td class='divlableval' style='"+strCss_value+";margin-top:-5px;'>"+lblFail+"</td>");
									
									sb.append("<td class='divlableval' style='"+strCss_value+";margin-top:-5px;padding-left:73px;'>"+Utility.convertDateAndTimeToDatabaseformatOnlyDate(teacherGeneralKnowledgeExam.getGeneralKnowledgeExamDate())+"</td>");
								    sb.append("<td  class='divlableval' style='"+strCss_value+";margin-top:-5px;padding-left:60px;'>&nbsp;<a data-original-title='Score Report' rel='tooltip' id='tpScoreReportGE' href='javascript:void(0);' onclick=\"downloadGeneralKnowledge('"+teacherGeneralKnowledgeExam.getGeneralKnowledgeExamId()+"','"+teacherGeneralKnowledgeExam.getTeacherDetail().getTeacherId()+"','tpScoreReportGE');"+windowFunc+"\">");
								    sb.append("<span class='icon-download-alt icon-large iconcolorBlue' style='padding-left:20px;'></span>");
								    sb.append("</a></td>");
								    sb.append("<td class='' style='"+strCss_value+";margin-top:-5px;padding-left:47px;'>");
								    sb.append(teacherGeneralKnowledgeExam.getGeneralExamNote());
								    sb.append("</td>");
								}
							}catch (Exception e) {
								e.printStackTrace();
							}
							sb.append("</tr>");
							sb.append("</table>");
						sb.append("</td>");
						sb.append("</tr>");
						sb.append("</table>");
					sb.append("</td>");
				   sb.append("</tr>");
					// End new row 
			   }
		}catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	}
	public TeacherGeneralKnowledgeExam getGKEValuesForProfile(TeacherDetail teacherDetail)
	{
		System.out.println("Calling getGKEValues Ajax Method  \t TeacherId "+teacherDetail.getTeacherId());
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		
		TeacherGeneralKnowledgeExam teacherGeneralKnowledgeExam=null;
		try{
			teacherGeneralKnowledgeExam=teacherGeneralKnowledgeExamDAO.findTeacherGeneralKnowledgeExam(teacherDetail);
		} 
		catch (Exception e)	{
			e.printStackTrace();
			return teacherGeneralKnowledgeExam;
		}	
		return teacherGeneralKnowledgeExam;
	}	
	public String getTeacherHonors(Integer teacherId,String portfolioIdAndType,String displayName){
		StringBuffer sb=new StringBuffer();
	try{
		String candidateType="";
		String []portfolioTypeId=portfolioIdAndType.split("#");
		Integer portfolioId=Utility.getIntValue(portfolioTypeId[1]);
		String sCandidateType=portfolioTypeId[0];
		if(sCandidateType.equalsIgnoreCase("")){
		}else{
			candidateType=validateApplicantType(sCandidateType);
		}
		Integer sectionId=15;
		TeacherDetail teacherDetail=teacherDetailDAO.findById(teacherId, false, false);
	// Start Teacher Honors Grid
		sb.append("<tr><td colspan=5 class='pleft15 portfolio_Subheading'><b>"+displayName+"</b></td></tr>");
		sb.append("<tr><th colspan=5 width='100%'>");
			sb.append("<table border=0 width='100%' onmouseover=\"javascript:setGridProfileVariable('teacherCerti')\">");
				sb.append("<tr>");
					sb.append("<td style="+sGridtableDisplayCSS+" id='gridDataTeacherHonors'>");
					sb.append("</td>");
				sb.append("</tr>");
			sb.append("</table>");
		sb.append("</th></tr>");
		sb.append("<script>honorsGridWithoutActionProfiler("+teacherDetail.getTeacherId()+",'"+portfolioId+"','"+candidateType+"','"+sectionId+"');</script>");
	// End Teacher Honors Grid
	}catch (Exception e) {
		e.printStackTrace();
	}
	return sb.toString();
}
	public String getInvolvementWorkGridProfiler(Integer teacherId,String portfolioIdAndType,String displayName){
		//Start Involvement/Volunteer Work Grid//
		StringBuffer sb=new StringBuffer();
		try{
			String candidateType="";
			String []portfolioTypeId=portfolioIdAndType.split("#");
			Integer portfolioId=Utility.getIntValue(portfolioTypeId[1]);
			String sCandidateType=portfolioTypeId[0];
			if(sCandidateType.equalsIgnoreCase("")){
			}else{
				candidateType=validateApplicantType(sCandidateType);
			}
			Integer sectionId=14;
			TeacherDetail teacherDetail = teacherDetailDAO.findById(teacherId, false, false);
			sb.append("<div class='left15'>");
			sb.append("<table>");
			sb.append("<tr><td colspan=5 class='pleft13 portfolio_Subheading'><b>"+displayName +"</b></td></tr>");
			sb.append("<tr><th colspan=5 width='100%'>");
			//sb.append("<table border=0 width='100%' onmouseover=\"javascript:setGridProfileVariable('teacherInvolve')\">");
			sb.append("<table border=0 width='100%'>");
			sb.append("<tr>");
			sb.append("<th style="+sGridtableDisplayCSS+" id='gridDataTeacherInvolvementOrVolunteerWork'>");
			sb.append("</th>");
			sb.append("</tr>");
			sb.append("</table>");
			sb.append("</th></tr>");
			sb.append("<script>involvementGridNew("+teacherDetail.getTeacherId()+",'"+portfolioId+"','"+candidateType+"','"+sectionId+"');</script>");
			sb.append("</table>");
			sb.append("</div>");
		}catch (Exception e) {
			// TODO: handle exception
		}
		//End Involvement/Volunteer Work Grid//
		return sb.toString();
	}
	
	public String getEmploymentGrid(Integer teacherId,String portfolioIdAndType,String displayName){
	// Start employment Grid
		StringBuffer sb=new StringBuffer();
		try{
			String candidateType="";
			String []portfolioTypeId=portfolioIdAndType.split("#");
			Integer portfolioId=Utility.getIntValue(portfolioTypeId[1]);
			String sCandidateType=portfolioTypeId[0];
			if(sCandidateType.equalsIgnoreCase("")){
			}else{
				candidateType=validateApplicantType(sCandidateType);
			}
			
			Integer sectionId=13;
			TeacherDetail teacherDetail = teacherDetailDAO.findById(teacherId, false, false);
			sb.append("<div class='left15'>");
			sb.append("<table>");
			sb.append("<tr><td colspan=5 class='pleft15 portfolio_Subheading'>"+displayName+"</td></tr>");
			sb.append("<tr><td colspan=5 width='100%'>");
				sb.append("<table border=0 width='100%' onmouseover=\"javascript:setGridProfileVariable('workExp')\">");
					sb.append("<tr>");
						sb.append("<td style="+sGridtableDisplayCSS+" id='gridDataWorkExpEmployment'>");
						sb.append("</td>");
					sb.append("</tr>");
				sb.append("</table>");
			sb.append("</td></tr>");
			sb.append("<script>employmentGridProfiler("+teacherDetail.getTeacherId()+",'"+portfolioId+"','"+candidateType+"','"+sectionId+"');</script>");
			sb.append("</table>");
			sb.append("</div>");
		}catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	// End employment Grid
	}
	public String getReferenceGrid(Integer teacherId,String portfolioIdAndType,String displayName){
	// Start Reference Grid
		StringBuffer sb=new StringBuffer();
		try{
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}
			UserMaster userMaster=null;
			if (session == null || session.getAttribute("userMaster") == null) {
				//return "false";
			}else{
				userMaster=(UserMaster)session.getAttribute("userMaster");
			}
			String []portfolioTypeId=portfolioIdAndType.split("#");
			Integer portfolioId=Utility.getIntValue(portfolioTypeId[1]);
			String sCandidateType=portfolioTypeId[0];
			String candidateType="";
			if(sCandidateType.equalsIgnoreCase("")){
			}else{
				validateApplicantType(sCandidateType);
			}
			Integer sectionId=9;
			TeacherDetail teacherDetail = teacherDetailDAO.findById(teacherId, false, false);
			sb.append("<div class='left15'>");
			  sb.append("<table>");
				sb.append("<tr><td colspan=5 class='pleft0'>");
				   sb.append("<div class=''>");
				    sb.append("<div class='col-sm-6 col-md-6 pleft10 portfolio_Subheading' style='margin-left: 0px !important;'>"+displayName+"</div>");
			    	  if(userMaster.getRoleId().getRoleId().equals(2) || userMaster.getRoleId().getRoleId().equals(3))
			    		  sb.append("<div class='col-sm-6 col-md-6' style='text-align:  right;' ><a href='javascript:void(0);' onclick='return showElectronicReferencesFormSS();'>+ Add Reference</a></div>");
			       
			    sb.append("</div></td></tr>");
				sb.append("<tr><td colspan=5 width='100%'>");
					sb.append("<table border=0 width='100%' onmouseover=\"javascript:setGridProfileVariable('References')\">");
						sb.append("<tr>");
							sb.append("<td style="+sGridtableDisplayCSS+" id='gridDataReference'>");
							sb.append("</td>");
						sb.append("</tr>");
					sb.append("</table>");
				sb.append("</td></tr>");
				sb.append("<script>referenceGridProfiler("+teacherDetail.getTeacherId()+",'"+portfolioId+"','"+candidateType+"','"+sectionId+"');</script>");
		    sb.append("</table>");
		   sb.append("</div>");
		}catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	// End Reference Grid
	}
	public String getVideoLinkGrid(Integer teacherId,String portfolioIdAndType,String displayName,String instructions){
	// Start Video Grid
		StringBuffer sb=new StringBuffer();
		try{
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}
			UserMaster userMaster=null;
			if (session == null || session.getAttribute("userMaster") == null) {
				//return "false";
			}else{
				userMaster=(UserMaster)session.getAttribute("userMaster");
			}
			TeacherDetail teacherDetail = teacherDetailDAO.findById(teacherId, false, false);
			String []portfolioTypeId=portfolioIdAndType.split("#");
			Integer portfolioId=Utility.getIntValue(portfolioTypeId[1]);
			String sCandidateType=portfolioTypeId[0];
			String candidateType=validateApplicantType(sCandidateType);
			Integer sectionId=10;
			sb.append("<tr><td colspan=5 class='pleft0'");
			   sb.append("<div class=''>");
			    sb.append("<div class='col-sm-6 col-md-6 pleft25 portfolio_Subheading' style='margin-left: 0px !important;'>"+displayName+"</div>");
		   
		 	 // if(userMaster.getRoleId().getRoleId().equals(2) || userMaster.getRoleId().getRoleId().equals(3))
		 	  //{
			    	  sb.append("<tr><td class='pleft25'>");
			    	  sb.append("<label class ='top10'><strong>"+instructions+"</strong></label>");
			    	  sb.append("</td></tr>");
			    	  if(userMaster.getRoleId().getRoleId().equals(2) || userMaster.getRoleId().getRoleId().equals(3))
			    		  sb.append("<tr><td><label style='padding-left: 90.3%;margin-bottom: 0px;'><a href='javascript:void(0);' onclick='return showVideoLinksFormSS();'>"+lnkAddViLnk+"</a></label></td></tr>");
			    	 // sb.append("<div class='col-sm-6 col-md-6' style='text-align:  right;' ><a href='javascript:void(0);' onclick='return showVideoLinksFormSS();'>"+lnkAddViLnk+"</a></div>");
		 	//  }
		   
		 sb.append("</div></td></tr>");
			sb.append("<tr><td colspan=5 width='100%'>");
			sb.append("<div class='left15'>");
				sb.append("<table width='100%' onmouseover=\"javascript:setGridProfileVariable('videoLink')\">");
					sb.append("<tr>");
					
						sb.append("<td style="+sGridtableDisplayCSS+"><div id='gridDataVideoLink' class=''></div>");
						sb.append("</td>");
					sb.append("</tr>");
				sb.append("</table>");
				sb.append("<div>");
			sb.append("</td></tr>");
			sb.append("<script>videoLinkGridProfiler("+teacherDetail.getTeacherId()+",'"+portfolioId+"','"+candidateType+"','"+sectionId+"');</script>");
		}catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	// End Video Grid
	}
	public String getlanguageProfiency(Integer teacherId,String portfolioIdAndType,String displayName){
	//Start language profiency
		StringBuffer sb=new StringBuffer();
		try{
			String candidateType="";
			String []portfolioTypeId=portfolioIdAndType.split("#");
			Integer portfolioId=Utility.getIntValue(portfolioTypeId[1]);
			String sCandidateType=portfolioTypeId[0];
			if(sCandidateType.equalsIgnoreCase("")){
			}else{
				candidateType=validateApplicantType(sCandidateType);
			}
			Integer sectionId=18;
			TeacherDetail teacherDetail = teacherDetailDAO.findById(teacherId, false, false);
			sb.append("<table>");
			sb.append("<tr><td colspan=5 class='pleft15 portfolio_Subheading'>"+displayName +"</td></tr>");
			sb.append("<tr><td colspan=5 width='100%'>");
			sb.append("<table width='100%' onmouseover=\"javascript:setGridProfileVariable('languageProfiency')\">");
			sb.append("<tr>");
			sb.append("<td style="+sGridtableDisplayCSS+"><div id='divGridlanguageProfiency' class=''></div>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("</table>");
			sb.append("</td></tr>");
			sb.append("</table>");
			sb.append("<script>languageProfiencyProfiler('"+teacherDetail.getTeacherId()+"','"+portfolioId+"','"+candidateType+"','"+sectionId+"');</script>");
		}catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
//end language profiency
}
	public String getPersonalSectionFixedArea(Integer teacherId,Integer jobId)
	{
		  /*SortedMap<Long,JobForTeacher> jftMap = new TreeMap<Long,JobForTeacher>();*/
		 /* SortedMap<Long,JobForTeacher> jftMapForTm = new TreeMap<Long,JobForTeacher>();*/
		/*  List<JobForTeacher> jobForTeachersList   =  new ArrayList<JobForTeacher>();*/
		  SortedMap<Integer,Integer> jftMap = new TreeMap<Integer,Integer>();
		  SortedMap<Integer,Integer> jftMapForTm = new TreeMap<Integer,Integer>();
		  List<String[]> jobForTeachersList   =  new ArrayList<String[]>();
		  List<JobOrder> lstJobOrderSLList =new ArrayList<JobOrder>();
		  SchoolMaster schoolMaster=null;
		  DistrictMaster districtMaster=null;
		  HeadQuarterMaster hqMasterobj=null;
		  BranchMaster branchMasterObj=null;
		  StringBuffer sb = new StringBuffer(); 
		  WebContext context;
		  context = WebContextFactory.get();
		  HttpServletRequest request = context.getHttpServletRequest();
		  HttpSession session = request.getSession(false);
		  int entityID=0,roleId=0;
		  boolean noEPIFlag=false;
		  try{	  
			   UserMaster userMaster=null;
			   boolean nobleflag=false;
			   if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			   {
			    throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			   }
			   else{
			    userMaster=(UserMaster)session.getAttribute("userMaster");
			    districtMaster=userMaster.getDistrictId();
			    if(districtMaster!=null)
			    {
			     if(districtMaster.getDistrictId()==7800038)
			      nobleflag=true;
			    }
			   }
			   entityID=userMaster.getEntityType();
			   boolean accessPermission=false;
			   
			    if(userMaster.getRoleId().getRoleId().equals(2))
			        accessPermission=true;
			    JobOrder jobOrder =null;
			    if(!jobId.equals(0)){
			    	jobOrder = jobOrderDAO.findById(new Integer(jobId), false, false);
			    }
			  TeacherDetail teacherDetail = teacherDetailDAO.findById(teacherId, false, false);
			  
			if(userMaster.getDistrictId()!=null){
				districtMaster =userMaster.getDistrictId();
				if(districtMaster.getNoEPI()==null ||(districtMaster.getNoEPI()!=null && districtMaster.getNoEPI()==false)){
					noEPIFlag=true;
				}
			}
			if(userMaster.getSchoolId()!=null)
				schoolMaster =userMaster.getSchoolId();
			if(entityID==3){
				lstJobOrderSLList =schoolInJobOrderDAO.jobOrderPostedBySchool(schoolMaster);
			}
			List<TeacherDetail> lstTeacherDetails	  =	 new ArrayList<TeacherDetail>();
			lstTeacherDetails.add(teacherDetail);
			List<Integer> statusIdList = new ArrayList<Integer>();
			String[] statusShrotName =  {"comp","icomp","vlt","hird","scomp","ecomp","vcomp","dcln","rem","widrw"};
			try{
				statusIdList = statusMasterDAO.findStatusIdsByStatusByShortNames(statusShrotName);
			}catch (Exception e) {
				e.printStackTrace();
			}
			/*jobForTeachersList=jobForTeacherDAO.getJobForTeacherByDSTeachersList(statusIdList,lstTeacherDetails,lstJobOrderSLList,null,null,districtMaster,entityID);
			for(JobForTeacher jobForTeacherObj : jobForTeachersList){
				jftMap.put(jobForTeacherObj.getJobForTeacherId(),jobForTeacherObj);
			}
			jobForTeachersList=jobForTeacherDAO.getJobForTeacherByDSTeachersList(statusIdList,lstTeacherDetails,lstJobOrderSLList,null,null,districtMaster,1);
			for(JobForTeacher jobForTeacherObj : jobForTeachersList){
				jftMapForTm.put(jobForTeacherObj.getJobForTeacherId(),jobForTeacherObj);
			}*/
			jobForTeachersList=jobForTeacherDAO.getJobForTeacherByJobAndTeachersListWithHQandBR(statusIdList,lstTeacherDetails,lstJobOrderSLList,hqMasterobj,branchMasterObj,districtMaster,entityID);
	           if(jobForTeachersList.size()>0){
	                 for (Iterator it = jobForTeachersList.iterator(); it.hasNext();) {
	                 Object[] row = (Object[]) it.next()       ;                                                                 
	                 jftMap.put(Integer.parseInt(row[0].toString()),Integer.parseInt(row[1].toString()));
	             }               
	            }
	              
	           
	           jobForTeachersList=jobForTeacherDAO.getJobForTeacherByJobAndTeachersListWithHQandBR(statusIdList,lstTeacherDetails,lstJobOrderSLList,hqMasterobj,branchMasterObj,districtMaster,1);
	           if(jobForTeachersList.size()>0){
	                 for (Iterator it = jobForTeachersList.iterator(); it.hasNext();) {
	                 Object[] row = (Object[]) it.next();                                                                 
	                 jftMapForTm.put(Integer.parseInt(row[0].toString()),Integer.parseInt(row[1].toString()));
	             }               
	            }
			String windowFunc="if(this.href!='javascript:void(0);')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
			TeacherExperience teacherExperience = teacherExperienceDAO.findTeacherExperienceByTeacher(teacherDetail);
			CandidateGridService cgService=new CandidateGridService();
			/*int jobList=jobListByTeacher(teacherDetail.getTeacherId(),jftMap);
			int jobListForTm=jobListByTeacher(teacherDetail.getTeacherId(),jftMapForTm);*/
			int jobList=0;
			 if(jftMap.get(teacherDetail.getTeacherId())!=null){
			 	jobList=jftMap.get(teacherDetail.getTeacherId());
			 }
			 int jobListForTm=0;
			 if(jftMapForTm.get(teacherDetail.getTeacherId())!=null){
				 jobListForTm=jftMapForTm.get(teacherDetail.getTeacherId());
			 } 
			List<JobForTeacher>  jobForTeachers=new ArrayList<JobForTeacher>();
			jobForTeachers=getJFT(teacherDetail, userMaster,"CGView");
			JobForTeacher jobForTeacher = null;
			String firstAppliedOn="";
			String lastAppliedOn="";
			Date lastActivityDate=null;
			String lastContactedOn=lblNone;
			Criterion criterion_teacherDetail = Restrictions.eq("teacherDetail",teacherDetail);
			List<TeacherNormScore> teacherNormScoresList=teacherNormScoreDAO.findByCriteria(criterion_teacherDetail);		
			List<JobWiseConsolidatedTeacherScore> lstJobWiseConsolidatedTeacherScore_new=jobWiseConsolidatedTeacherScoreDAO.getFitScore(lstTeacherDetails,jobOrder);
			int normScore=0;
			String colorName="";
			Map<Integer,TeacherAssessmentStatus> baseTakenMap = new HashMap<Integer, TeacherAssessmentStatus>();
			List<TeacherAssessmentStatus> teacherAssessmentStatus = null;
			Map<Integer, Integer> isbaseComplete	=	new HashMap<Integer, Integer>();
			
			Integer isBase = null;
			try{
				if(lstTeacherDetails!=null && lstTeacherDetails.size()>0){
					teacherAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentTakenByTeachers(lstTeacherDetails);
					for (TeacherAssessmentStatus teacherAssessmentStatus2 : teacherAssessmentStatus) {					
						baseTakenMap.put(teacherAssessmentStatus2.getTeacherDetail().getTeacherId(), teacherAssessmentStatus2);
						boolean cgflag = teacherAssessmentStatus2.getCgUpdated()==null?false:teacherAssessmentStatus2.getCgUpdated();
						if(cgflag && teacherAssessmentStatus2.getStatusMaster().getStatusShortName().equalsIgnoreCase("comp"))
							isbaseComplete.put(teacherAssessmentStatus2.getTeacherDetail().getTeacherId(), 1);
						else
							if(teacherAssessmentStatus2.getStatusMaster().getStatusShortName().equalsIgnoreCase("vlt"))
								isbaseComplete.put(teacherAssessmentStatus2.getTeacherDetail().getTeacherId(), 2);
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			if(teacherNormScoresList.size()>0)
			{
				normScore=teacherNormScoresList.get(0).getTeacherNormScore();
				colorName =teacherNormScoresList.get(0).getDecileColor();
			}
			List<TeacherProfileVisitHistory> listTeacherProfileVisitHistory=null;
			if(entityID==1)
				listTeacherProfileVisitHistory = teacherProfileVisitHistoryDAO.findByCriteria(criterion_teacherDetail);
			else
			{
				Criterion criterion_districtId = Restrictions.eq("districtId",districtMaster);
				listTeacherProfileVisitHistory = teacherProfileVisitHistoryDAO.findByCriteria(criterion_teacherDetail,criterion_districtId);
			}	
			
			String roleAccess=null;
			try{
				roleAccess = roleAccessPermissionDAO.getMenuOptionList(roleId,38,"candidatereport.do",0);
			}
			catch(Exception e){
				e.printStackTrace();
			}
			int iTeacherProfileVisitHistoryCount=0;
			if(listTeacherProfileVisitHistory.size()>0)
				iTeacherProfileVisitHistoryCount=listTeacherProfileVisitHistory.size();
			List<DemoClassSchedule> lstDemoClassSchedule_new=demoClassScheduleDAO.findDemoClassSchedules(lstTeacherDetails,jobOrder);
			if(jobForTeachers.size()>0)
			{
				jobForTeacher = jobForTeachers.get(0);
				firstAppliedOn = Utility.convertDateAndTimeToUSformatOnlyDate(jobForTeacher.getCreatedDateTime());
				jobForTeacher = jobForTeachers.get(jobForTeachers.size()-1);
				lastAppliedOn = Utility.convertDateAndTimeToUSformatOnlyDate(jobForTeacher.getCreatedDateTime());
			}
			Map<Integer, TeacherAssessmentStatus> mapJSI = new HashMap<Integer, TeacherAssessmentStatus>();
			List<TeacherAssessmentStatus> lstJSI =new ArrayList<TeacherAssessmentStatus>();
			List<AssessmentJobRelation> assessmentJobRelations1=assessmentJobRelationDAO.findRelationByJobOrderNoDate(jobOrder);
			if(assessmentJobRelations1.size()>0 && lstTeacherDetails.size()>0){
				AssessmentJobRelation assessmentJobRelation1 = assessmentJobRelations1.get(0);
				if(assessmentJobRelation1.getAssessmentId()!=null)
					lstJSI = teacherAssessmentStatusDAO.findAssessmentTakenByTeacherList(lstTeacherDetails,assessmentJobRelation1.getAssessmentId());
			}
			TeacherAssessmentStatus teacherAssessmentStatusJSI_new = null;
			for(TeacherAssessmentStatus ts : lstJSI){
				mapJSI.put(ts.getTeacherDetail().getTeacherId(), ts);
			}
				boolean achievementScore=false,tFA=false,demoClass=false,JSI=false,teachingOfYear =false,expectedSalary=false,fitScore=false,pnqDis=false;
				DistrictMaster districtMasterObj=null;
				if(jobOrder!=null)
					districtMasterObj=jobOrder.getDistrictMaster();
					
				if(districtMasterObj!=null && districtMasterObj.getDisplayAchievementScore()!=null && districtMasterObj.getDisplayAchievementScore()){
					achievementScore=true;
				}
				if(districtMasterObj!=null && districtMasterObj.getDisplayTFA()!=null && districtMasterObj.getDisplayTFA()){
					tFA=true;
				}
				if(districtMasterObj!=null && districtMasterObj.getDisplayDemoClass()!=null && districtMasterObj.getDisplayDemoClass()){
					demoClass=true;
				}
				if(districtMasterObj!=null && districtMasterObj.getDisplayJSI()!=null && districtMasterObj.getDisplayJSI()){
					JSI=true;
				}
				if(districtMasterObj!=null && districtMasterObj.getDisplayYearsTeaching()!=null && districtMasterObj.getDisplayYearsTeaching()){
					teachingOfYear=true;
				}
				if(districtMasterObj!=null && districtMasterObj.getDisplayExpectedSalary()!=null && districtMasterObj.getDisplayExpectedSalary()){
					expectedSalary=true;
				}
				if(districtMasterObj!=null && districtMasterObj.getDisplayFitScore()!=null && districtMasterObj.getDisplayFitScore()){
					fitScore=true;
				}
				if(districtMasterObj!=null && districtMasterObj.getDistrictId()!=null && districtMasterObj.getDistrictId().equals(7800038)){
					pnqDis=true;
				}
			
			
			sb.append("<div class='row'>");
			sb.append("<div class='col-md-1 col-sm-1'></div>");
			
			sb.append("<div class='col-md-11 col-sm-11' style='padding-left: 0px;'>");
			
			sb.append("<div class='row'>");	
			
			
			//Resume Class start
			sb.append("<div class='col-md-2 col-sm-2 divprofile'>"+lnkResume+":</div>");
			sb.append("<div class='col-md-2 col-sm-2'>");		
			
			if(teacherExperience!=null && teacherExperience.getResume()!=null && teacherExperience.getResume()!="")
				sb.append("<a data-original-title='"+lnkResume+"' rel='tooltip' id='tpResumeprofile' href='javascript:void(0);' onclick=\"downloadResume('"+teacherDetail.getTeacherId()+"','tpResumeprofile');"+windowFunc+"\" ><span class='icon-briefcase icon-large iconcolor'></span></a>");
			else
				sb.append("<a data-original-title='"+lblNotAvailable+"' rel='tooltip' id='tpResumeprofile'  ><span class='icon-briefcase icon-large iconcolorhover'></span></a>");
		
			sb.append("</div>");
			//Resume Class end	
			
			// PD Report start		
			sb.append("<div class='col-md-2 col-sm-2 divprofile'>"+headPdRep+":</div>");
			sb.append("<div class='col-md-2 col-sm-2'>");
			isBase = isbaseComplete.get(teacherDetail.getTeacherId());
			if(isBase!=null && isBase==1)
				sb.append("<a data-original-title='"+headPdRep+"' rel='tooltip' id='tpPDReportprofile' href='javascript:void(0);' onclick=\"generatePDReport('"+teacherDetail.getTeacherId()+"','tpPDReportprofile');"+windowFunc+"\"><span class='icon-book icon-large iconcolor'></span></a>");
			else
				sb.append("<a data-original-title='"+lblNotAvailable+"' rel='tooltip' id='tpPDReportprofile' ><span class='icon-book icon-large iconcolorhover'></span></a>");			
			sb.append("</div>");
			// PD Report end
			
			//JSI start
			
			sb.append("<div class='col-md-2 col-sm-2 divprofile'>"+lblJSI+":</div>");
			sb.append("<div class='col-md-2 col-sm-2'>");
			if(JSI)
			{
				if(roleAccess.indexOf("|4|")!=-1)
				{
					if(mapJSI.get(teacherDetail.getTeacherId())!=null){
						teacherAssessmentStatusJSI_new = mapJSI.get(teacherDetail.getTeacherId());
						if(teacherAssessmentStatusJSI_new.getStatusMaster().getStatusShortName().equalsIgnoreCase("icomp")){
							sb.append("<a data-original-title='"+lblCandidateNotCompletedJSI+"' rel='tooltip' id='tpJSAProfile'><img src='images/jsi_hover.png'  width='24px'></a>");
						}
						else if(teacherAssessmentStatusJSI_new.getStatusMaster().getStatusShortName().equalsIgnoreCase("vlt")){
							sb.append("<a data-original-title='"+lbllblCandidateTimedOut+"' rel='tooltip' id='tpJSAProfile'><img src='images/jsi_hover.png'  width='24px'></a>");
						}
						else
						{
							sb.append("<a data-original-title='"+lblJSI+"' rel='tooltip' id='tpJSAProfile' href='javascript:void(0);' onclick=\"generatePilarReport('"+teacherDetail.getTeacherId()+"','"+jobOrder.getJobId()+"','tpJSAProfile');"+windowFunc+"\"><img src='images/jsi.png'  width='24px'></a>");
						}
					}
					else
					{
						sb.append("<a data-original-title='"+lblNotAvailable+"' rel='tooltip' id='tpJSAProfile'><img src='images/jsi_hover.png'  width='24px'></a>");
					}
				}
				else
					sb.append(lblNA);
			}
			else
				sb.append(lblNA);
			sb.append("</div>");
			//JSI end
			
			//Demo Class start
			sb.append("<div class='col-md-2 col-sm-2 divprofile'>"+lblDemoLesson+":</div>");
			sb.append("<div class='col-md-2 col-sm-2'>");		
			if(demoClass){
				String demoStatus =cgService.getDemoClassScheduleStatus(lstDemoClassSchedule_new,teacherDetail);
				if(demoStatus!=null)
					sb.append(demoStatus);
				else
					sb.append(lblNA);
				
			}		
			sb.append("</div>");
			//Demo Class end
			
			sb.append("<div class='col-md-2 col-sm-2 divprofile'>"+lblFirstJobApplied+":</div>");
			sb.append("<div class='col-md-2 col-sm-2'>"+firstAppliedOn+"</div>");
			
			sb.append("<div class='col-md-2 col-sm-2 divprofile'>"+lblLastJobApplied+":</div>");
			sb.append("<div class='col-md-2 col-sm-2'>"+lastAppliedOn+"</div>");
			
			sb.append("<div class='col-md-2 col-sm-2 divprofile'>"+lblLastContacted+":</div>");
			sb.append("<div class='col-md-2 col-sm-2'>"+lastContactedOn+"</div>");
			
			sb.append("<div class='col-md-2 col-sm-2 divprofile'>"+lblJobApplied+":</div>");
			sb.append("<div class='col-md-2 col-sm-2'>");
					if(jobList>0)
						sb.append("<a href='javascript:void(0);' onclick=\"getJobListNew('"+teacherDetail.getTeacherId()+"')\" >"+jobList+"/"+(jobListForTm-jobList)+"</a>");
					else
						sb.append("&nbsp;"+jobList+"/"+(jobListForTm-jobList)+"");
					sb.append("</div>");
			
					//# of Views:
			sb.append("<div class='col-md-2 col-sm-2 divprofile'>"+lblOfViews+":</div>");
			sb.append("<div class='col-md-2 col-sm-2'>");		
			if(iTeacherProfileVisitHistoryCount>0)
				sb.append("<a data-original-title='"+lblClickToViewTPVisit+"' rel='tooltip' id='tpTeacherProfileVisitHistory' href='javascript:void(0);' onclick=\"getTeacherProfileVisitHistoryShow_FTimeNew('"+teacherDetail.getTeacherId()+"');\" >&nbsp;"+iTeacherProfileVisitHistoryCount+"</a>");	
			else
				sb.append(lblNA);
			sb.append("</div>");
			
			
			sb.append("<div class='col-md-2 col-sm-2 divprofile'>EPI Norm:</div>");
			sb.append("<div class='col-md-2 col-sm-2'>");
			
			if(noEPIFlag){
				if(normScore==0)
				{
					sb.append(lblNA);
					
				}
				else
				{
					String ccsName="";
					String normScoreLen=normScore+"";
					if(normScoreLen.length()==1){
						ccsName="nobground1";
					}else if(normScoreLen.length()==2){
						ccsName="nobground2";
					}else{
						ccsName="nobground3";
					}
					sb.append("<span class=\""+ccsName+"\" style='font-size: 11px;font-weight: bold;background-color: #"+colorName+";'>"+normScore+"</span>");
				}
			}
			else
				sb.append(lblNA);
			sb.append("</div>");
			
			
			
			
			sb.append("<div class='col-md-2 col-sm-2 divprofile'>"+lblFitScore+":</div>");
			sb.append("<div class='col-md-2 col-sm-2'>");
			
			//Fit Score
			if(fitScore){
				String fScore =cgService.getFitScore(lstJobWiseConsolidatedTeacherScore_new,teacherDetail);
				if(fScore!=null)
					sb.append(fScore);
				else
					sb.append(lblNA);
			}
			else
				sb.append(lblNA);
			sb.append("</div>");
			
			sb.append("<div class='col-md-11 col-sm-11' style='padding-left:0px; margin-left:0px;'>");
			// Calculating and Get valles of "Achievement Score","Academic Achievement" and "Leadership/Results"
			sb.append("<div id='achievementScoreDiv'></div>");
			sb.append("<script>achievementScoreProfiler('"+achievementScore+"','"+entityID+"','"+jobId+"','"+teacherId+"');</script>");
			//getAcademicAchievement(achievementScore,entityID,jobId,teacherId);
		
			sb.append("</div>");
			sb.append("</div>");
			//End New Row
			sb.append("</div>");
			sb.append("</div>");
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	}
	public String getAcademicAchievement(Boolean achievementScore,Integer entityID,Integer jobId,Integer teacherId)
	{
		StringBuilder sb=new StringBuilder();
		String strAScore="";
		String strLRScore="";
		int iAcademicAchievement=0;
		int iLeadershipResults=0;
		int itotalAchievementScore=0;
		int iachievementMaxScore=0;
		int iacademicAchievementMaxScore=0;
		int ileadershipAchievementMaxScore=0;
		
		JobOrder jobOrder=null;
		try{
			if(jobId!=0)
				jobOrder=jobOrderDAO.findById(new Integer(jobId), false, false);
			
			 TeacherDetail teacherDetail = teacherDetailDAO.findById(teacherId, false, false);
			List<TeacherDetail> lstTeacherDetails	  =	 new ArrayList<TeacherDetail>();
			lstTeacherDetails.add(teacherDetail);
			List<TeacherAchievementScore> lstTeacherAchievementScore_new=null;
			if(jobOrder!=null)
				lstTeacherAchievementScore_new=teacherAchievementScoreDAO.getAchievementScore(lstTeacherDetails,jobOrder);
			int getScrrec = 0;
			if(lstTeacherAchievementScore_new!=null && lstTeacherAchievementScore_new.size()!=0){
				getScrrec=lstTeacherAchievementScore_new.size()-1;
			}
			if(lstTeacherAchievementScore_new!=null)
				if(lstTeacherAchievementScore_new.size()>0){
					iAcademicAchievement=lstTeacherAchievementScore_new.get(getScrrec).getAcademicAchievementScore();
					iLeadershipResults=lstTeacherAchievementScore_new.get(getScrrec).getLeadershipAchievementScore();
					
					iacademicAchievementMaxScore=lstTeacherAchievementScore_new.get(getScrrec).getAcademicAchievementMaxScore();
					ileadershipAchievementMaxScore=lstTeacherAchievementScore_new.get(getScrrec).getLeadershipAchievementMaxScore();
					
					itotalAchievementScore=lstTeacherAchievementScore_new.get(getScrrec).getTotalAchievementScore();
					iachievementMaxScore=lstTeacherAchievementScore_new.get(getScrrec).getAchievementMaxScore();
					
					//strAScore=(itotalAchievementScore)+"/"+(iachievementMaxScore);
					strAScore=(iAcademicAchievement)+"/"+(iacademicAchievementMaxScore);
					strLRScore=(iLeadershipResults)+"/"+(ileadershipAchievementMaxScore);
				}
				else
				{
					strAScore=lblNA;
					strLRScore=lblNA;
				}
		if(achievementScore)
		{							
			// A Score			
			sb.append("<div class='col-md-2 col-sm-2 divprofile'>"+lblAScore+":</div>");
			sb.append("<div class='col-md-2 col-sm-2 left18'>"+strAScore+"</div>");
			
			sb.append("<div class='col-md-2 col-sm-2 divprofile left18'>"+lblLRScore+":</div>");
			sb.append("<div class='col-md-2 col-sm-2 left15'>"+strLRScore+"</div>");
		}					
		if(entityID==2)
		{
			if(achievementScore && entityID==2)
			{
				//1st Row for Label of "Achievement Score","Academic Achievement" and "Leadership/Results"
				//Academic Achievement:
				sb.append("<div class='col-md-5 col-sm-5' style='width: 360px;'>"+lblAcademic +"&nbsp;"+lblAchievement +":</div>");
				// Leadership/Results
				sb.append("<div class='col-md-4 col-sm-4'>"+lblLeadershipResults+":</div>");
				
				//2nd Row for values of "Achievement Score","Academic Achievement" and "Leadership/Results"
				
				String strSliderCss_Iframe="border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:280px;";
				sb.append("<div class='col-md-4 col-sm-4' style='padding-left:5px;'>");
				sb.append("<iframe id='ifrm_aca_ach' src='slideract.do?name=slider_aca_ach&tickInterval=5&max=20&swidth=240&svalue="+iAcademicAchievement+"' scrolling='no' frameBorder='0' style='"+strSliderCss_Iframe+"'>" +
						"</iframe>");
				sb.append("</div>");							
				sb.append("<input type='hidden' name='txt_sl_slider_aca_ach' id='txt_sl_slider_aca_ach' value='"+iAcademicAchievement+"'>");
				
				
				// Leadership/Results
				sb.append("<div class='col-md-4 col-sm-4 left25'>");
				sb.append("<iframe id='ifrm_lea_res' src='slideract.do?name=slider_lea_res&tickInterval=5&max=20&swidth=240&svalue="+iLeadershipResults+"' scrolling='no' frameBorder='0' style='"+strSliderCss_Iframe+"'>" +
							"</iframe>");
				sb.append("<input type='hidden' name='txt_sl_slider_lea_res' id='txt_sl_slider_lea_res' value='"+iLeadershipResults+"'>");
				//sb.append("<a href='javascript:void(0);' onclick=\"javascript:updateAScore("+teacherId+","+jobOrder.getDistrictMaster().getDistrictId()+")\"'>Go</A>");
				sb.append("</div>");
				
				sb.append("<div class='col-md-3 col-sm-3'>");
				sb.append("<button class='btn btn-primary' type='button' onclick=\"javascript:updateAScoreNew("+teacherDetail.getTeacherId()+","+jobOrder.getDistrictMaster().getDistrictId()+","+jobId+",'"+achievementScore+"',"+entityID+")\">"+lblSetAScore+"");
				sb.append("</div>");
			}
		 }
		}catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	}
	public List<JobForTeacher> getJFT(TeacherDetail teacherDetail,UserMaster userMaster,String Source)
	{
		System.out.println("*********** getJFT ******************");
		Criterion criterion_teacherId = Restrictions.eq("teacherId", teacherDetail);
		List<JobForTeacher>  jobForTeachers=new ArrayList<JobForTeacher>();
		int entityType=userMaster.getEntityType();
		if(entityType==2)
		{
			Criterion criterion2 = Restrictions.eq("districtMaster", userMaster.getDistrictId());
			List<JobOrder> jobOrders=jobOrderDAO.findByCriteria(criterion2);
			if(jobOrders.size() > 0)
			{
				Criterion criterion3 = Restrictions.in("jobId", jobOrders);
				jobForTeachers = jobForTeacherDAO.findByCriteria(criterion_teacherId,criterion3);
			}
		}
		else if(entityType==3)
		{			
			List<JobOrder> jobOrders=new ArrayList<JobOrder>();
			List<SchoolInJobOrder> inJobOrders=new ArrayList<SchoolInJobOrder>();
			if(userMaster.getSchoolId()!=null)
				inJobOrders=schoolInJobOrderDAO.findJobBySchool(userMaster.getSchoolId());
			if(inJobOrders.size() > 0)
			{
				for(SchoolInJobOrder inJobOrder:inJobOrders)
				{
					jobOrders.add(inJobOrder.getJobId());
				}
			}
			
			if(jobOrders.size() > 0)
			{
				Criterion criterion3 = Restrictions.in("jobId", jobOrders);
				jobForTeachers = jobForTeacherDAO.findByCriteria(criterion_teacherId,criterion3);
			}
		}
		else
		{
			jobForTeachers = jobForTeacherDAO.findJobByTeacher(teacherDetail);
		}
		return jobForTeachers;
	}
	public int jobListByTeacher(int teacherId,SortedMap<Long,JobForTeacher> jftMap)
	{
		int jobList=0;
		try{
			Iterator iterator = jftMap.keySet().iterator();
			JobForTeacher jobForTeacher=null;
			while (iterator.hasNext()) {
				Object key = iterator.next();
				jobForTeacher=jftMap.get(key);
				int teacherKey=Integer.parseInt(jobForTeacher.getTeacherId().getTeacherId()+"");
				if(teacherKey==teacherId){
					jobList++;
				}
			}
		}catch (Exception e) {
			// TODO: handle exception
		}
		return jobList;
	}
	public String getAdditionalDocuments(Integer teacherId,String portfolioIdAndType,String displayName){
		StringBuffer sb=new StringBuffer();
		//Additional Documents
		try{
			String candidateType="";
			String []portfolioTypeId=portfolioIdAndType.split("#");
			Integer portfolioId=Utility.getIntValue(portfolioTypeId[1]);
			String sCandidateType=portfolioTypeId[0];
			if(sCandidateType.equalsIgnoreCase("")){
			}else{
				validateApplicantType(sCandidateType);
			}
			Integer sectionId=11;
			TeacherDetail teacherDetail = teacherDetailDAO.findById(teacherId, false, false);
		sb.append("<tr><td colspan=5 class='pleft25 portfolio_Subheading'>"+displayName+"</td></tr>");
		sb.append("<tr><td colspan=5 width='100%'>");
		sb.append("<div class='left15'>");
			sb.append("<table width='100%' onmouseover=\"javascript:setGridProfileVariable('document')\">");
				sb.append("<tr>");
					sb.append("<td style="+sGridtableDisplayCSS+"><div id='divGridAdditionalDocuments' class=''></div>");
					sb.append("</td>");
				sb.append("</tr>");
			sb.append("</table>");
			sb.append("</div>");
		sb.append("</td></tr>");
		sb.append("<script>additionalDocumentsProfiler("+teacherDetail.getTeacherId()+",'"+portfolioId+"','"+candidateType+"','"+sectionId+"');</script>");	
		}catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
		//End Additional Documents
	}
		public String getCertificationLicense(Integer teacherId,String portfolioIdAndType,String displayName){
			// Start Teacher Certifications Grid
			StringBuffer sb=new StringBuffer();
			try{
				WebContext context;
				context = WebContextFactory.get();
				HttpServletRequest request = context.getHttpServletRequest();
				HttpSession session = request.getSession(false);
				if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
					throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
				}
				UserMaster userMaster=null;
				if (session == null || session.getAttribute("userMaster") == null) {
					//return "false";
				}else{
					userMaster=(UserMaster)session.getAttribute("userMaster");
				}
				String candidateType="";
				String []portfolioTypeId=portfolioIdAndType.split("#");
				Integer portfolioId=Utility.getIntValue(portfolioTypeId[1]);
				String sCandidateType=portfolioTypeId[0];
				if(sCandidateType.equalsIgnoreCase("")){
				}else{
					validateApplicantType(sCandidateType);
				}
				Integer sectionId=8;
				TeacherDetail teacherDetail = teacherDetailDAO.findById(teacherId, false, false);
			    // Start Teacher Certifications Grid
				sb.append("<div class='left15'>");
				sb.append("<table>");
				sb.append("<tr><td colspan=5 class='pleft13 portfolio_Subheading'>"+displayName+"</td></tr>");
				sb.append("<tr><td colspan=5 width='100%'>");
				sb.append("<table border=0 width='100%' onmouseover=\"javascript:setGridProfileVariable('teacherCerti')\">");
					sb.append("<tr>");
						sb.append("<td style="+sGridtableDisplayCSS+" id='gridDataTeacherCertifications'>");
						sb.append("</td>");
					sb.append("</tr>");
				sb.append("</table>");
			sb.append("</td></tr>");
			sb.append("<script>teacherCertificationsGrid("+teacherDetail.getTeacherId()+",'"+portfolioId+"','"+candidateType+"','"+sectionId+"');</script>");
			sb.append("</table>");
			sb.append("</div>");
			
			// End Teacher Certifications Grid
			}catch (Exception e) {
			e.printStackTrace();
			}
			return sb.toString();
		
			// End Teacher Certifications Grid
		}
//****************************************************************************************************************************************************
public String getRefeNotesNew(String eleReferenceId,String jobId,DistrictMaster districtMaster_Input,SchoolMaster schoolMaster_Input)
{
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	}
	DistrictMaster districtMaster=null;
	SchoolMaster schoolMaster=null;
	UserMaster userMaster=null;
	int entityID=0,roleId=0;
	if (session == null || session.getAttribute("userMaster") == null) {
		//return "false";
	}else{
		userMaster=(UserMaster)session.getAttribute("userMaster");
		if(userMaster.getRoleId().getRoleId()!=null){
			roleId=userMaster.getRoleId().getRoleId();
		}
		if(userMaster.getDistrictId()!=null){
			districtMaster=userMaster.getDistrictId();
		}
		if(userMaster.getSchoolId()!=null){
			schoolMaster =userMaster.getSchoolId();
		}	
		entityID=userMaster.getEntityType();
	}
	
	StringBuffer sb = new StringBuffer();		
	try 
	{
		
		String windowFunc="if(this.href!='javascript:void(0);')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
		sb.append("<table border='0' width='460' class='tablecgtbl'>");
		
			TeacherElectronicReferences electronicReferences=teacherElectronicReferencesDAO.findById(new Integer(eleReferenceId), false, false);
			//TeacherDetail teacherDetail = teacherDetailDAO.findById(new Integer(teacherId), false, false);
			JobOrder   jobOrder =null;
			if(!jobId.equals("0") && !jobId.equals("")){
				jobOrder=jobOrderDAO.findById(Integer.parseInt(jobId), false, false);
				/*if(entityID!=1){
					sb.append("<tr>"); 
					sb.append("<td style='text-align:right;'><a data-original-title='Add Notes on the Candidate Reference' rel='tooltip' id='refeNotesId' href='javascript:void(0);' onclick=\"getNotesEditorDivNew()\" ><img src=\"images/note_com.png\"></a></td>");
					sb.append("</tr>");
				}*/
			}
			
			if(entityID!=1){
				sb.append("<tr>"); 
				sb.append("<td style='text-align:right;'><a data-original-title='"+LBLCandidateRefe+"' rel='tooltip' id='refeNotesIdNew' href='javascript:void(0);' onclick=\"getNotesEditorDivNew()\" ><img src=\"images/note_com.png\"></a></td>");
				sb.append("</tr>");
			}
			
			/*List<ReferenceNote> referenceNotesList = new ArrayList<ReferenceNote>();
			Criterion criterion11 = Restrictions.eq("electronicReferences",electronicReferences);
			Criterion criterion13 = Restrictions.eq("jobId",jobOrder);
			
			if(!jobId.equals("0") && !jobId.equals(""))
				referenceNotesList = referenceNoteDAO.findByCriteria(criterion11,criterion13);
			else
				referenceNotesList = referenceNoteDAO.findByCriteria(criterion11);*/
			
			///////////////
			
			List<ReferenceNote> referenceNotesList = new ArrayList<ReferenceNote>();
			Criterion criterion_refId = Restrictions.eq("electronicReferences",electronicReferences);
			Criterion criterion_districtId = Restrictions.eq("districtMaster",districtMaster);
			//Criterion criterion_schoolId = Restrictions.eq("schoolId",schoolMaster);
			
			Order sortOrder=Order.desc("noteCreatedDate");

			/*if(jobId!=null && !jobId.equals("0") && !jobId.equals("") && jobOrder!=null){
				Criterion criterion_job = Restrictions.eq("jobId", jobOrder);
				if(entityID==1){ // For TM
					referenceNotesList=referenceNoteDAO.findByCriteria(criterion_refId,criterion_job);
				}
				else // For District
					referenceNotesList=referenceNoteDAO.findByCriteria(criterion_refId,criterion_job,criterion_districtId);
				
				else if(entityID==3){// For School
					referenceNotesList=referenceNoteDAO.findByCriteria(criterion_refId,criterion_job,criterion_schoolId,criterion_districtId);	
				}
			}
			else
			{*/
				if(entityID==1){ // For TM
					Criterion criterion_districtId_input = Restrictions.eq("districtMaster",districtMaster_Input);
					if(districtMaster_Input==null)
						referenceNotesList=referenceNoteDAO.findByCriteria(sortOrder,criterion_refId);
					else
						referenceNotesList=referenceNoteDAO.findByCriteria(sortOrder,criterion_refId,criterion_districtId_input);
				
					/*else if(schoolMaster_Input!=null && districtMaster_Input!=null)
					{
						referenceNotesList=referenceNoteDAO.findByCriteria(criterion_refId,criterion_schoolId_input,criterion_districtId_input);	
					}*/
				}
				else // For District
					referenceNotesList=referenceNoteDAO.findByCriteria(sortOrder,criterion_refId,criterion_districtId);
				
				/*else if(entityID==3){// For School
					referenceNotesList=referenceNoteDAO.findByCriteria(criterion_refId,criterion_schoolId,criterion_districtId);
					//System.out.println("entityID "+entityID+ " District Id "+districtMaster.getDistrictId()+" School ID "+schoolMaster.getSchoolId());
				}*/
			//}
			//////////////
			
			
			
			//Collections.sort(referenceNotesList,CommunicationsDetail.createdDate);
			if(referenceNotesList.size()>0){
			sb.append("<tr><td>");
				sb.append("<table border='0' style='padding:0px;' cellpadding=0 cellspacing=0  width='100%' class='tablecgtbl'>");
				
				String submitedBy = "";
				int counter=0;
				String year="";
				String preYear="";
				if(referenceNotesList !=null)
					for(ReferenceNote referenceNote : referenceNotesList){
						year= convertUSformatOnlyYear(referenceNote.getNoteCreatedDate());
						counter++;
						if(!preYear.equals(year)){
							sb.append("<tr>");
							sb.append("<td colspan=3 style='text-align:right;padding-right:8px;padding-bottom:5px;verical-align:bottom;'><span class=\"txtyearbg\">"+year+"</span></td><td>&nbsp;</td>");
							sb.append("<tr>");
							sb.append("<tr>");
							sb.append("<td width='15%' style='height:10px;'>&nbsp;</td>");
							sb.append("<td style='text-align:right;vertical-align:middle;padding-left:12px;'  height=10>");
							sb.append("<span class=\"vertical-line\"></span></td><td ></td>");
							sb.append("</tr>");
						}
						preYear=year;
						sb.append("<tr>");
						sb.append("<td width='15%' style='text-align:right;vertical-align:middle;position:relative;'>"+convertUSformatOnlyMonthDate(referenceNote.getNoteCreatedDate()));
							sb.append("<span class='note-com'><img src=\"images/note_com.png\"></span>");
						
						sb.append("</td><td style='text-align:right;vertical-align:middle;padding-left:12px;' width='8px;' height='10'>");
						sb.append("<span class=\"vertical-line\"></span></td>");
						sb.append("<td  style='text-align:right;vertical-align:middle;' width=20>");;
						sb.append("<hr style=\"color: #f00; background:#007AB4; width: 100%; height: 3px;\"/></td>");
						sb.append("<td class='commbgcolor'  style='padding:5px;vertical-align:middle;'>");
						sb.append("<span class='com-user'>From "+referenceNote.getUserMaster().getFirstName()+" "+referenceNote.getUserMaster().getLastName()+"");
						
						if(referenceNote.getJobId()!=null && referenceNote.getJobId().getJobTitle()!=null){
							sb.append(" regarding "+referenceNote.getJobId().getJobTitle()+"</span><br/><br/>");
						}else{
							sb.append("</span><br/><br/>");
						}
						sb.append(referenceNote.getRefNoteDescription()+"&nbsp;&nbsp;&nbsp;");
						
						if(referenceNote.getRefNoteAttachment()!=null){
							String filePath=null;
							String fileName=null;
							fileName=referenceNote.getRefNoteAttachment();
							filePath="ref_note/"+referenceNote.getElectronicReferences().getElerefAutoId();
							
							sb.append("<a href='javascript:void(0);' id='refNotesfileopen"+counter+"' onclick=\"downloadReferenceNotes('"+filePath+"','"+fileName+"','refNotesfileopen"+counter+"');"+windowFunc+"\"><span class='icon-paper-clip icon-large iconcolor'></span></a>");
						}
						sb.append("</td>");
						sb.append("</tr>");
						if(counter < referenceNotesList.size()){
							sb.append("<tr>");
							sb.append("<td width='15%' style='height:20px;'>&nbsp;</td>");
							sb.append("<td style='text-align:right;vertical-align:middle;padding-left:12px;'  height=20>");
							sb.append("<span class=\"vertical-line\"></span></td><td ></td>");
							sb.append("</tr>");
						}
						
				}
				sb.append("</td></tr>");
			}else{
				sb.append("<tr><td>"+lblNoRecord+"</td></tr>");
			}
		sb.append("</table>");

	} catch (Exception e) {
		e.printStackTrace();
	}
	return sb.toString();
}
public static String convertUSformatOnlyYear(Date dat)
{
	try {
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy");
		return sdf.format(dat);
	} catch (Exception e) {
		//e.printStackTrace();
		return "";
	}
}
public static String convertUSformatOnlyMonthDate(Date dat)
{
	try {
		SimpleDateFormat sdf=new SimpleDateFormat("MMMMM dd");
		return sdf.format(dat);
	} catch (Exception e) {
		return "";
	}
}
public String saveReferenceNotesByAjax(String refId,String teacherId,String jobId,String refnote,String fileName)
{	
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	}
	DistrictMaster districtMaster=null;
	SchoolMaster schoolMaster=null;
	UserMaster userMaster=null;
	int entityID=0,roleId=0;
	if (session == null || session.getAttribute("userMaster") == null) {
		//return "false";
	}else{
		userMaster=(UserMaster)session.getAttribute("userMaster");
		if(userMaster.getRoleId().getRoleId()!=null){
			roleId=userMaster.getRoleId().getRoleId();
		}
		if(userMaster.getDistrictId()!=null){
			districtMaster=userMaster.getDistrictId();
		}
		if(userMaster.getSchoolId()!=null){
			schoolMaster =userMaster.getSchoolId();
		}	
		entityID=userMaster.getEntityType();
	}
	

		TeacherDetail teacherDetail = teacherDetailDAO.findById(new Integer(teacherId), false, false);
		TeacherElectronicReferences teacherElectronicReferences=teacherElectronicReferencesDAO.findById(new Integer(refId), false, false);
		
		JobOrder   jobOrder =null;
		if(!jobId.equals("0") && !jobId.equals(""))
			jobOrder=jobOrderDAO.findById(new Integer(jobId), false, false);
		
		if(jobOrder!=null)
			districtMaster=jobOrder.getDistrictMaster();
			
		try {
			ReferenceNote referenceNote=new ReferenceNote();
			
			referenceNote.setElectronicReferences(teacherElectronicReferences);
			referenceNote.setTeacherDetail(teacherDetail);
			
			if(jobOrder!=null)
				referenceNote.setJobId(jobOrder);
			
			referenceNote.setRefNoteDescription(refnote);
			
			if(fileName!=null && !fileName.equalsIgnoreCase(""))
				referenceNote.setRefNoteAttachment(fileName);
			
			referenceNote.setDistrictMaster(districtMaster);
			referenceNote.setSchoolId(schoolMaster);
			referenceNote.setUserMaster(userMaster);
			referenceNote.setNoteCreatedDate(new Date());
			referenceNoteDAO.makePersistent(referenceNote);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	return "saveRefNotes";
}
public String getGridDataForTeacherProfileVisitHistoryByTeacher(Integer teacherId, String noOfRow, String pageNo,String sortOrder,String sortOrderType,String visitLocation)
{		
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	}
	DistrictMaster districtMaster=null;
	SchoolMaster schoolMaster=null;
	UserMaster userMaster=null;
	int entityTypeId=1;
	int entityID=0,roleId=0;
	if (session == null || session.getAttribute("userMaster") == null) {
		//return "false";
	}else{
		userMaster=(UserMaster)session.getAttribute("userMaster");
		if(userMaster.getRoleId().getRoleId()!=null){
			roleId=userMaster.getRoleId().getRoleId();
		}
		if(userMaster.getDistrictId()!=null){
			districtMaster=userMaster.getDistrictId();
		}
		if(userMaster.getSchoolId()!=null){
			schoolMaster =userMaster.getSchoolId();
		}	
		entityID=userMaster.getEntityType();
	}
	
	StringBuffer sb = new StringBuffer();		
	try 
	{
		int noOfRowInPage 	= Integer.parseInt(noOfRow);
		int pgNo 			= Integer.parseInt(pageNo);
		int start 			= ((pgNo-1)*noOfRowInPage);
		int end 			= ((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
		int totalRecord 	= 0;
		//------------------------------------

		TeacherDetail teacherDetail = teacherDetailDAO.findById(teacherId, false, false);

		List<TeacherProfileVisitHistory> listTeacherProfileVisitHistory= null;
		/** set default sorting fieldName **/
		String sortOrderFieldName	=	"visitedDateTime";
		String sortOrderNoField		=	"visitedDateTime";

		/**Start set dynamic sorting fieldName **/
		Order  sortOrderStrVal=null;

		if(sortOrder!=null){
			if(!sortOrder.equals("") && !sortOrder.equals(null) && !sortOrder.equals("userName")){
				sortOrderFieldName=sortOrder;
				sortOrderNoField=sortOrder;
			}
			if(sortOrder.equals("userName")){
				sortOrderNoField="userName";
			}
		}
		String sortOrderTypeVal="0";
		if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
			if(sortOrderType.equals("0")){
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}else{
				sortOrderTypeVal="1";
				sortOrderStrVal=Order.desc(sortOrderFieldName);
			}
		}else{
			sortOrderTypeVal="0";
			sortOrderStrVal=Order.asc(sortOrderFieldName);
		}
		/**End ------------------------------------**/
		
		Criterion criterion = Restrictions.eq("teacherDetail",teacherDetail);
		
		if(entityID==1){
			listTeacherProfileVisitHistory = teacherProfileVisitHistoryDAO.findByCriteria(criterion);
			totalRecord=listTeacherProfileVisitHistory.size();
			listTeacherProfileVisitHistory= null;
			listTeacherProfileVisitHistory = teacherProfileVisitHistoryDAO.findWithLimit(sortOrderStrVal, start, noOfRowInPage, criterion);
		}
		else
		{
			Criterion criterion_districtId = Restrictions.eq("districtId",districtMaster);
			listTeacherProfileVisitHistory = teacherProfileVisitHistoryDAO.findByCriteria(criterion,criterion_districtId);
			totalRecord=listTeacherProfileVisitHistory.size();
			listTeacherProfileVisitHistory= null;
			listTeacherProfileVisitHistory = teacherProfileVisitHistoryDAO.findWithLimit(sortOrderStrVal, start, noOfRowInPage, criterion,criterion_districtId);
		}
		List<TeacherProfileVisitHistory> sortedTeacherProfileVisitHistory		=	new ArrayList<TeacherProfileVisitHistory>();

		SortedMap<String,TeacherProfileVisitHistory>	sortedMap = new TreeMap<String,TeacherProfileVisitHistory>();
		if(sortOrderNoField.equals("userName"))
		{
			sortOrderFieldName	=	"userName";
		}
		
		int mapFlag=2;
		for (TeacherProfileVisitHistory pojo : listTeacherProfileVisitHistory){
			String orderFieldName=""+pojo.getVisitId();
			if(sortOrderFieldName.equals("userName")){
				orderFieldName=pojo.getUserMaster().getFirstName()+" "+pojo.getUserMaster().getLastName()+"||"+pojo.getVisitId();
				sortedMap.put(orderFieldName+"||",pojo);
				if(sortOrderTypeVal.equals("0")){
					mapFlag=0;
				}else{
					mapFlag=1;
				}
			}
		}
		if(mapFlag==1){
			NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet();  
			for (Iterator iter=navig.iterator();iter.hasNext();) {  
				Object key = iter.next(); 
				sortedTeacherProfileVisitHistory.add((TeacherProfileVisitHistory) sortedMap.get(key));
			} 
		}else if(mapFlag==0){
			Iterator iterator = sortedMap.keySet().iterator();
			while (iterator.hasNext()) {
				Object key = iterator.next();
				sortedTeacherProfileVisitHistory.add((TeacherProfileVisitHistory) sortedMap.get(key));
			}
		}else{
			sortedTeacherProfileVisitHistory=listTeacherProfileVisitHistory;
		}

		if(totalRecord<end)
			end=totalRecord;

		String responseText="";
		String tablePadding_Data="";
		int iRecordCount=1;
		
		if(visitLocation.equalsIgnoreCase("My Folders") || visitLocation.equalsIgnoreCase("Mosaic")){ 
			tablePadding_Data="style='padding-left:10px;vertical-align:top;text-align:left;font-weight: normal;line-height:30px;'";
			String tablePadding="style='padding-left:5px;vertical-align:top;color:#FFFFFF;line-height:33px;'";
			sb.append("<table id='tblProfileVisitHistory_Profile' border='0'  width='570' class='table-bordered' onmouseover=\"javascript:setGridProfileVariable('tpvh')\">");
			sb.append("<thead class='bg'>");
			sb.append("<tr>");
			responseText=PaginationAndSorting.responseSortingMLinkSSPF(lblUserName,sortOrderFieldName,"userName",sortOrderTypeVal,pgNo);
			sb.append("<th align=\"left\" width='460' "+tablePadding+" >"+responseText+"</th>");
			responseText=PaginationAndSorting.responseSortingMLinkSSPF(lblVisitedDate1,sortOrderFieldName,"visitedDateTime",sortOrderTypeVal,pgNo);
			sb.append("<th align=\"left\" width='110' "+tablePadding+" >"+responseText+"</th>");
		}else{
			tablePadding_Data="";
			sb.append("<table border='0' id='tblProfileVisitHistory_Profile' class='table table-striped' onmouseover=\"javascript:setGridProfileVariable('tpvh')\">");
			sb.append("<thead class='bg'>");			
			sb.append("<tr>");

			responseText=PaginationAndSorting.responseSortingMLinkSSPF(lblUserName,sortOrderFieldName,"userName",sortOrderTypeVal,pgNo);
			sb.append("<th>"+responseText+"</th>");

			responseText=PaginationAndSorting.responseSortingMLinkSSPF(lblVisitedDate1,sortOrderFieldName,"visitedDateTime",sortOrderTypeVal,pgNo);
			sb.append("<th>"+responseText+"</th>");
			
		}
		sb.append("</tr>");
		sb.append("</thead>");
		if(listTeacherProfileVisitHistory!=null)
		{
			for(TeacherProfileVisitHistory pojo:sortedTeacherProfileVisitHistory)
			{
				iRecordCount++;
				String gridColor="";
				if(iRecordCount%2==0 && ( visitLocation.equalsIgnoreCase("My Folders") || visitLocation.equalsIgnoreCase("Mosaic"))){
					gridColor="class='bggrid'";
				}
				
				String Name=pojo.getUserMaster().getFirstName()+" "+pojo.getUserMaster().getLastName();
				sb.append("<tr "+gridColor+">");
				sb.append("<td "+tablePadding_Data+">");
				sb.append(Name);
				sb.append("</td>");
				sb.append("<td "+tablePadding_Data+">");
				sb.append(Utility.convertDateAndTimeToUSformatOnlyDate(pojo.getVisitedDateTime()));
				sb.append("</td>");

				
				sb.append("</tr>");
			}
			if(totalRecord>10 && ( visitLocation.equalsIgnoreCase("My Folders") || visitLocation.equalsIgnoreCase("Mosaic"))){
				//sb.append("<tr><td colspan='2' style='border-top: 1px solid #cccccc;padding-bottom:0px;padding-top:5px;'>");
				sb.append("</table>");
				sb.append(PaginationAndSorting.getPagination_pageSizeVariableName(request,totalRecord,noOfRow, pageNo,"pageSize14","565"));
				//sb.append("</td></tr>");
			}
			
		}

		if(listTeacherProfileVisitHistory==null || listTeacherProfileVisitHistory.size()==0){
			
			if(visitLocation.equalsIgnoreCase("My Folders") || visitLocation.equalsIgnoreCase("Mosaic")){
				tablePadding_Data="style='padding-left:10px;vertical-align:top;text-align:left;font-weight: normal;line-height:30px;'";
				sb.append("<tr><td "+tablePadding_Data+" colspan='2'>"+lblNoRecord+"</td></tr>" );
			}else{
				sb.append("<tr>");
				sb.append("<td colspan='2'>");
				sb.append(lblNoRecord);
				sb.append("</td>");
				sb.append("</tr>");
			}
			
		}
		sb.append("</table>");
		if(!visitLocation.equalsIgnoreCase("My Folders") && !visitLocation.equalsIgnoreCase("Mosaic")){
			sb.append(PaginationAndSorting.getPagination_pageSizeVariableName(request,totalRecord,noOfRow, pageNo,"pageSize14","565"));
		}

	} 
	catch (Exception e){
		e.printStackTrace();
	}
	return sb.toString();	
}

public String[] getUpdateAndSaveAScore(Integer teacherId,Integer districtId,Integer slider_aca_ach,Integer slider_lea_res){
	String[] strScore=new String[2];
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	}
	UserMaster userMaster=null;
	userMaster=(UserMaster)session.getAttribute("userMaster");
	try {
		TeacherDetail teacherDetail=teacherDetailDAO.findById(teacherId, false, false);
		DistrictMaster districtMaster=districtMasterDAO.findById(districtId, false, false);
		TeacherAchievementScore teacherAchievementScore=new TeacherAchievementScore();
		List<TeacherAchievementScore> teacherAchievementScoresList=teacherAchievementScoreDAO.getAchievementScoreByTeacherAndDistrict(teacherDetail, districtMaster);
		if(teacherAchievementScoresList.size()==1)
		{
			teacherAchievementScore=teacherAchievementScoresList.get(0);
		}
		
		teacherAchievementScore.setTeacherDetail(teacherDetail);
		teacherAchievementScore.setDistrictMaster(districtMaster);
		
		teacherAchievementScore.setAcademicAchievementScore(slider_aca_ach);
		teacherAchievementScore.setLeadershipAchievementScore(slider_lea_res);
		
		teacherAchievementScore.setAcademicAchievementMaxScore(20);
		teacherAchievementScore.setLeadershipAchievementMaxScore(20);
		
		teacherAchievementScore.setTotalAchievementScore(slider_aca_ach+slider_lea_res);
		teacherAchievementScore.setAchievementMaxScore(40);
		teacherAchievementScore.setScoredBy(userMaster);
		teacherAchievementScore.setScoredDateTime(new Date());
		
		teacherAchievementScoreDAO.makePersistent(teacherAchievementScore);
		
		strScore[0]=teacherAchievementScore.getAcademicAchievementScore()+"/"+teacherAchievementScore.getAcademicAchievementMaxScore();
		strScore[1]=teacherAchievementScore.getLeadershipAchievementScore()+"/"+teacherAchievementScore.getLeadershipAchievementMaxScore();
		
	} catch (Exception e) {
		e.printStackTrace();
	}
	return strScore;
}
//*********************************************************************************************
public String getLabelOrToolTipTagOrRequiredIconProfiler(String tooltipId, DspqRouter dspqRouter,List<String> listToolTips)
{
	return getLabelOrToolTipTagOrRequiredIconProfiler(tooltipId, dspqRouter, listToolTips,"");
}

public String getLabelOrToolTipTagOrRequiredIconProfiler(String tooltipId, DspqRouter dspqRouter,List<String> listToolTips,String sPreFixText)
{
	String sReturnValue="";
	
	int iNumberRequired=0;
	try {
		if(dspqRouter.getNumberRequired()!=null && dspqRouter.getNumberRequired()>0)
		{
			iNumberRequired=dspqRouter.getNumberRequired();
		}
		//icon-warning required
	} catch (Exception e) {

	}
	
	try {
		if(dspqRouter.getDisplayName()!=null && !dspqRouter.getDisplayName().equals(""))
		{
			if(dspqRouter.getDspqFieldMaster()!=null && dspqRouter.getDspqSectionMaster()==null)
			{
				if(sPreFixText!=null && !sPreFixText.equals(""))
					sReturnValue="<span id='lblFieldId_"+tooltipId+"' numberRequired='"+iNumberRequired+"'>"+sPreFixText+""+dspqRouter.getDisplayName()+"</span><span id='lblFieldIdWarningBeforeSpace_"+tooltipId+"'></span><span id='lblFieldIdWarning_"+tooltipId+"'></span><span id='lblFieldIdWarningAfterSpace_"+tooltipId+"'></span>";
				else
					sReturnValue="<span id='lblFieldId_"+tooltipId+"' numberRequired='"+iNumberRequired+"'>"+dspqRouter.getDisplayName()+"</span><span id='lblFieldIdWarningBeforeSpace_"+tooltipId+"'></span><span id='lblFieldIdWarning_"+tooltipId+"'></span><span id='lblFieldIdWarningAfterSpace_"+tooltipId+"'></span>";
			}
			else
				sReturnValue="<span id='lblSectionId_"+tooltipId+"' numberRequired='"+iNumberRequired+"'>"+dspqRouter.getDisplayName()+"</span><span id='lblFieldIdWarningBeforeSpace_"+tooltipId+"'></span><span id='lblSectionIdWarning_"+tooltipId+"'></span><span id='lblFieldIdWarningAfterSpace_"+tooltipId+"'></span>";
		}
		//icon-warning required
	} catch (Exception e) {

	}
	
	try {
		if(dspqRouter.getIsRequired()!=null && dspqRouter.getIsRequired())
			sReturnValue=sReturnValue+"<span class='required'>*</span>";
	} catch (Exception e) {

	}
	
	try {
		if(dspqRouter.getTooltip()!=null && !dspqRouter.getTooltip().equals(""))
		{
			sReturnValue=sReturnValue+"&nbsp;<a href='javascript:void(0);' id='tooltip_"+tooltipId+"' rel='tooltip' data-original-title=\""+dspqRouter.getTooltip()+"\"><img src='images/qua-icon.png' width='15' height='15' alt=''></a>";
			listToolTips.add("tooltip_"+tooltipId);
		}
	} catch (Exception e) {

	}
	
	return sReturnValue;
}

public String getInstructionForSectionProfiler(DspqRouter dspqRouter)
{
	String sReyurnValue="";
	if(dspqRouter!=null && dspqRouter.getInstructions()!=null && !dspqRouter.getInstructions().equals(""))
	{
		sReyurnValue="<div class='row'><div class='col-sm-12 col-md-12 top5' id='instructionId_'"+dspqRouter.getDspqSectionMaster().getSectionId()+"><label><strong>"+dspqRouter.getInstructions()+"</strong></label></div></div>";
	}
	return sReyurnValue;
}
public String getAddSubSectionTextProfiler(DspqRouter dspqRouter)
{
	String sSubSection=dspqRouter.getDisplayName();
	sSubSection=sSubSection.replaceAll("s ", " ");
	sSubSection=sSubSection.replaceAll("s/", "/");
	
	if(sSubSection.endsWith("S") || sSubSection.endsWith("s"))
		sSubSection=sSubSection.substring(0, sSubSection.length()-1);
		
	sSubSection=Utility.getLocaleValuePropByKey("DSPQ_lnkAddPrefix", locale)+" "+sSubSection;
	
	return sSubSection;
}
public String getInputCustomeAttributeProfiler(DspqRouter dspqRouter)
{
	String sReturnValue="";
	
	int iIsRequired=0;
	if(dspqRouter.getIsRequired()!=null && dspqRouter.getIsRequired())
		iIsRequired=1;
	
	String sDisplayName="";
	if(dspqRouter.getDisplayName()!=null && !dspqRouter.getDisplayName().equals(""))
		sDisplayName=dspqRouter.getDisplayName();
	
	try {
		if(dspqRouter!=null)
		{
			sReturnValue=" dspqRequired='"+iIsRequired+"' dspqLabelName='"+sDisplayName+"' ";
		}
	} catch (Exception e) {
		// TODO: handle exception
	}
	return sReturnValue;
}
//************************ Save reference ************************************************************
public String saveOrUpdateElectronicReferencesProfiler(TeacherElectronicReferences teacherElectronicReferences,Integer teacherId)
{
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
		throw new IllegalStateException(msgYrSesstionExp);
	}
	int iStatus=1;
	String sFileName=null;
	TeacherDetail teacherDetail =teacherDetailDAO.findById(teacherId, false, false);
	TeacherElectronicReferences dbTER=null;
	try{
		if(teacherElectronicReferences.getElerefAutoId()!=null && teacherElectronicReferences.getElerefAutoId() >0){
			dbTER = teacherElectronicReferencesDAO.findById(teacherElectronicReferences.getElerefAutoId(), false, false);
			if(dbTER.getStatus()!=null)
				iStatus=dbTER.getStatus();
			if(dbTER.getPathOfReference()!=null)
				sFileName=dbTER.getPathOfReference();
		}
		teacherElectronicReferences.setStatus(iStatus);
		teacherElectronicReferences.setTeacherDetail(teacherDetail);
		
		if(sFileName!=null && (teacherElectronicReferences.getPathOfReference()==null || teacherElectronicReferences.getPathOfReference().equals("")))
			teacherElectronicReferences.setPathOfReference(sFileName);
		
		teacherElectronicReferencesDAO.makePersistent(teacherElectronicReferences);
	}catch (Exception e){
		e.printStackTrace();
	}
	return "OK";
}
//*********************** save Video link ****************************************************
public String saveOrUpdateVideoLinksProfiler(TeacherVideoLink teacherVideoLink,Integer videolinkAutoId,Integer teacherId)
{
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
		throw new IllegalStateException(msgYrSesstionExp);
	}
	TeacherDetail teacherDetail = teacherDetailDAO.findById(teacherId, false, false);
	teacherVideoLink.setTeacherDetail(teacherDetail);
	try{
		String sFileName="";
		TeacherVideoLink tvlTemp=null;
		if(videolinkAutoId!=null){
			tvlTemp=teacherVideoLinksDAO.findById(teacherVideoLink.getVideolinkAutoId(), false, false);
			if(tvlTemp!=null && tvlTemp.getVideo()!=null && !tvlTemp.getVideo().equals(""))
				sFileName=tvlTemp.getVideo();
		}
		if(sFileName!=null && (teacherVideoLink.getVideo()==null || teacherVideoLink.getVideo().equals("")))
			teacherVideoLink.setVideo(sFileName);
		
		if(teacherVideoLink.getCreatedDate()==null)
			teacherVideoLink.setCreatedDate(new Date());
		
		if(teacherVideoLink!=null && ( teacherVideoLink.getVideo()==null || teacherVideoLink.getVideo().equals("") ) || ( teacherVideoLink.getVideourl()==null || teacherVideoLink.getVideourl().equals("") )){
			
		}else{
			if(teacherVideoLink.getVideo().equals("0"))
				teacherVideoLink.setVideo("");
			
			teacherVideoLinksDAO.makePersistent(teacherVideoLink);
		}
	} 
	catch (Exception e){
		e.printStackTrace();
	}
	return "";
}
//*********************************************************************************************
@Transactional(readOnly=false)
public String getGroup3OpentextProfiler(String iJobId,Integer dspqPortfolioNameId,String candidateType,Integer groupId,Integer teacherId)
{
	System.out.println("Calling DSPQServiceAjax => getGroup03_21");
	
	//TeacherDetail teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");
	
	String locale = Utility.getValueOfPropByKey("locale");	
	StringBuffer sb=new StringBuffer();
	
	String sCandidateTypeDec=candidateType;
	DspqSectionMaster dspqSectionMaster=new DspqSectionMaster();
	List<DspqSectionMaster> sectionMasters=new ArrayList<DspqSectionMaster>();
	List<DspqFieldMaster> dspqFieldList=new ArrayList<DspqFieldMaster>();
	//for Cert
	dspqSectionMaster=new DspqSectionMaster();
	dspqSectionMaster.setSectionId(7);
	sectionMasters.add(dspqSectionMaster);
	
	//for Substitute Teacher 
	dspqSectionMaster=new DspqSectionMaster();
	dspqSectionMaster.setSectionId(21);
	sectionMasters.add(dspqSectionMaster);
	
	//for TFA
	dspqSectionMaster=new DspqSectionMaster();
	dspqSectionMaster.setSectionId(23);
	sectionMasters.add(dspqSectionMaster);
	
	//for Certified Teacher
	dspqSectionMaster=new DspqSectionMaster();
	dspqSectionMaster.setSectionId(29);
	sectionMasters.add(dspqSectionMaster);

	dspqFieldList = dspqFieldMasterDAO.getFieldListBySections(sectionMasters);
	DspqPortfolioName dspqPortfolioName = dspqPortfolioNameDAO.findById(dspqPortfolioNameId, false, false);
	List<DspqRouter> dspqRouterList=dspqRouterDAO.getControlsByPortfolioNameAndCTAndField(dspqPortfolioName, sCandidateTypeDec, dspqFieldList,sectionMasters);
	
	DspqGroupMaster dspqGroupMaster=dspqGroupMasterDAO.findById(groupId, false, false);
	
	Map<Integer, DspqRouter> mapDspqRouterByFID=new HashMap<Integer, DspqRouter>();
	Map<Integer, DspqRouter> mapDspqRouterBySID=new HashMap<Integer, DspqRouter>();
	setSIDAndFIDInMapProfiler(mapDspqRouterByFID, mapDspqRouterBySID, dspqRouterList);
	TeacherDetail teacherDetail=teacherDetailDAO.findById(teacherId, false, false);
	/*JobCategoryMaster dspqPotfolioJC=null;
	if(dspqPortfolioName!=null && dspqPortfolioName.getJobCategoryMaster()!=null)
		dspqPotfolioJC=dspqPortfolioName.getJobCategoryMaster();*/
	DistrictMaster dspqPotfolioDM=null;
	if(dspqPortfolioName!=null && dspqPortfolioName.getDistrictMaster()!=null){
		dspqPotfolioDM=dspqPortfolioName.getDistrictMaster();
	}
	List<String> listToolTips = new ArrayList<String>();
	List<String> listCalDated = new ArrayList<String>();	
	
	int iNewJobId=Utility.getIntValue(iJobId);
	int iOldQuestionDisplaySectionId=0;
	if(iNewJobId>0)
	{
		iOldQuestionDisplaySectionId=getSectionIdForShowOldQuestion(iNewJobId, teacherDetail, sCandidateTypeDec, dspqPortfolioName);
	}
	
	TeacherExperience teacherExperience = teacherExperienceDAO.findTeacherExperienceByTeacher(teacherDetail);
	String customQuestion="";
	if(mapDspqRouterBySID!=null)
	{
		
		//sb.append("<div class='class='col-sm-1 col-md-1'>&nbsp;</div>");
		//sb.append("<div class='class='col-sm-11 col-md-11'></div>");
		DspqRouter dspqRouterSection29=mapDspqRouterBySID.get(29);
		if(dspqRouterSection29!=null)
		{
			sb.append("<div class='row left15'><div class='col-sm-12 col-md-12'><div class='span4 portfolio_Subheading'>"+dspqRouterSection29.getDisplayName()+"</div></div>");
			String sSectionName="tooltipSection29";
			boolean isNonTeacher=false;
			if(mapDspqRouterByFID.get(142)!=null)
			{
				
				DspqRouter dspqRouter=mapDspqRouterByFID.get(142);
				if(dspqRouter!=null)
				{
					sb.append("<div class='col-sm-2 col-md-2 divprofile'>"+dspqRouter.getDisplayName()+":</div>");
					
					Boolean bIsNonTeacher=null;
					if(teacherExperience!=null && teacherExperience.getIsNonTeacher()!=null)
						bIsNonTeacher=teacherExperience.getIsNonTeacher();
					
					String sInputControlId="isNonTeacher";		
					if(bIsNonTeacher!=null && bIsNonTeacher.equals(true)){
						sb.append("<div class='col-sm-2 col-md-2'>Yes</div>");
						isNonTeacher=true;
					}
					else
						sb.append("<div class='col-sm-2 col-md-2'>No</div>");
				}
			}
			
			
			if(mapDspqRouterByFID.get(143)!=null  && isNonTeacher)
			{
				DspqRouter dspqRouter=mapDspqRouterByFID.get(143);
				if(dspqRouter!=null)
				{
					sb.append("<div class='col-sm-2 col-md-2 divprofile'>"+dspqRouter.getDisplayName()+":</div>");
					Double dExpCertTeacherTraining=0.0;
					if(teacherExperience!=null && teacherExperience.getExpCertTeacherTraining()!=null)
						dExpCertTeacherTraining=teacherExperience.getExpCertTeacherTraining();
					
					sb.append("<div class='col-sm-2 col-md-2'>"+dExpCertTeacherTraining+"</div>");
				}
			}
			/*customQuestion="";
			customQuestion=getCustomFieldBySectionIdProfiler(dspqPotfolioDM, candidateType, dspqGroupMaster, dspqRouterSection29.getDspqSectionMaster(),listToolTips,sSectionName,listCalDated,dspqPortfolioName,teacherDetail);
			if(!customQuestion.equalsIgnoreCase("")){
				sb.append("</div>");
				sb.append(customQuestion);
				sb.append("<div class='row left25'>");
			}*/
			sb.append("</div>");
		}
		//**************************************
		
		//TFA
	
		DspqRouter dspqRouterSection23=mapDspqRouterBySID.get(23);
		if(dspqRouterSection23!=null)
		{
			String sSectionName="tooltipSection23";
			sb.append("<div class='row left15'><div class='col-sm-12 col-md-12'><div class='span4 portfolio_Subheading'>"+dspqRouterSection23.getDisplayName()+"</div></div>");
			//--------- Input Field start
			//Substitute Teacher			
			if(mapDspqRouterByFID.get(101)!=null)
			{
				List<TFAAffiliateMaster> lstTFAAffiliateMaster	=	tfaAffiliateMasterDAO.findByCriteria(Order.asc("tfaAffiliateName"));
				DspqRouter dspqRouter=mapDspqRouterByFID.get(101);
				if(dspqRouter!=null)
				{
					sb.append("<div class='col-sm-2 col-md-2 divprofile'>"+dspqRouter.getDisplayName()+":</div>");
					Integer tfaAffiliate=null;
					String tfaName="";
					if(teacherExperience!=null && teacherExperience.getTfaAffiliateMaster()!=null && teacherExperience.getTfaAffiliateMaster().getTfaAffiliateId()!=null)
						tfaName=teacherExperience.getTfaAffiliateMaster().getTfaAffiliateName();
					
						if(tfaName.equalsIgnoreCase(""))
							sb.append("<div class='col-sm-2 col-md-2'>"+lblNA+"</div>");
						else
							sb.append("<div class='col-sm-2 col-md-2'>"+tfaName+"</div>");
					
				}		
			}
			
			if(mapDspqRouterByFID.get(102)!=null)
			{
				List<String> lstCorpsYear=Utility.getLasterYeartillAdvanceYear(1990);
				DspqRouter dspqRouter=mapDspqRouterByFID.get(102);
				if(dspqRouter!=null)
				{
					sb.append("<div class='col-sm-2 col-md-2 divprofile'>"+dspqRouter.getDisplayName()+":</div>");
					Integer corpsYear=null;
					if(teacherExperience!=null && teacherExperience.getCorpsYear()!=null)
						corpsYear=teacherExperience.getCorpsYear();
					
						String sInputControlId="corpsYear";
						String corpsText="";
						for(String corps : lstCorpsYear)
						{
							int iCorps=Utility.getIntValue(corps);
							if(corpsYear!=null && corpsYear==iCorps)
								corpsText=corpsYear.toString();							
						}	
						if(corpsText.equalsIgnoreCase(""))
							sb.append("<div class='col-sm-2 col-md-2'>"+lblNA+"</div>");
						else
							sb.append("<div class='col-sm-2 col-md-2'>"+corpsText+"</div>");
				}		
			}
			
			if(mapDspqRouterByFID.get(103)!=null)
			{
				List<TFARegionMaster> lstTFARegionMaster		=	tfaRegionMasterDAO.findByCriteria(Order.asc("tfaRegionName"));
				DspqRouter dspqRouter=mapDspqRouterByFID.get(103);
				if(dspqRouter!=null)
				{
					sb.append("<div class='col-sm-2 col-md-2 divprofile'>"+dspqRouter.getDisplayName()+":</div>");
					String iTFARegion="";
					if(teacherExperience!=null && teacherExperience.getTfaRegionMaster()!=null && teacherExperience.getTfaRegionMaster().getTfaRegionId()!=null)
						iTFARegion=teacherExperience.getTfaRegionMaster().getTfaRegionName();
					
						if(iTFARegion.equalsIgnoreCase(""))
							sb.append("<div class='col-sm-2 col-md-2'>"+lblNA+"</div>");
						else
							sb.append("<div class='col-sm-2 col-md-2'>"+iTFARegion+"</div>");
				}		
			}
			
			//--------- Input Field end
			/*customQuestion="";
			customQuestion=getCustomFieldBySectionIdProfiler(dspqPotfolioDM, candidateType, dspqGroupMaster, dspqRouterSection23.getDspqSectionMaster(),listToolTips,sSectionName,listCalDated,dspqPortfolioName,teacherDetail);
			if(!customQuestion.equalsIgnoreCase("")){
				sb.append("</div>");
				sb.append(customQuestion);
				sb.append("<div class='row left25'>");
			}*/
			sb.append("</div>");
		}		
		DspqRouter dspqRouterSection7=mapDspqRouterBySID.get(7);
		if(dspqRouterSection7!=null)
		{
			sb.append("<div class='row left15'><div class='col-sm-12 col-md-12'><div class='span4 portfolio_Subheading'>"+dspqRouterSection7.getDisplayName()+"</div></div>");
			String sSectionName="tooltipSection7";			
			boolean isDispalyNBCY=false;
			if(mapDspqRouterByFID.get(31)!=null)
			{
				DspqRouter dspqRouter=mapDspqRouterByFID.get(31);
				if(dspqRouter!=null)
				{
					sb.append("<div class='col-sm-3 col-md-3 divprofile'>"+dspqRouter.getDisplayName()+":</div>");
					isDispalyNBCY=true;
					Boolean bNationalBoardCert=null;
					if(teacherExperience!=null && teacherExperience.getNationalBoardCert()!=null)
						bNationalBoardCert=teacherExperience.getNationalBoardCert();
					
					
					if(bNationalBoardCert!=null && bNationalBoardCert.equals(true)){
						sb.append("<div class='col-sm-2 col-md-2'>Yes</div>");
						isDispalyNBCY=true;
					}
					else 
						sb.append("<div class='col-sm-2 col-md-2'>No</div>");
				}				
			}
			
			if(mapDspqRouterByFID.get(30)!=null && isDispalyNBCY)
			{
				List<String> lstNationalBoardCertYear=Utility.getLasterYeartillAdvanceYear(1990);
				DspqRouter dspqRouter=mapDspqRouterByFID.get(30);
				if(dspqRouter!=null)
				{		
					sb.append("<div class='col-sm-1 col-md-1 divprofile'>"+dspqRouter.getDisplayName()+":</div>");
					Integer NationalBoardCertYear=null;
					String nationalBoardYear="";
					if(teacherExperience!=null && teacherExperience.getNationalBoardCertYear()!=null)
						NationalBoardCertYear=teacherExperience.getNationalBoardCertYear();
						for(String nbsy : lstNationalBoardCertYear)
						{
							int iCorps=Utility.getIntValue(nbsy);
							if(NationalBoardCertYear!=null && NationalBoardCertYear==iCorps)
								nationalBoardYear=nbsy;								
						}
						if(nationalBoardYear.equalsIgnoreCase(""))
							sb.append("<div class='col-sm-1 col-md-1'>"+lblNA+"</div>");
						else
							sb.append("<div class='col-sm-1 col-md-1'>"+nationalBoardYear+"</div>");
				}		
			}
			/*customQuestion="";
			customQuestion=getCustomFieldBySectionIdProfiler(dspqPotfolioDM, candidateType, dspqGroupMaster, dspqRouterSection7.getDspqSectionMaster(),listToolTips,sSectionName,listCalDated,dspqPortfolioName,teacherDetail);
			if(!customQuestion.equalsIgnoreCase("")){
				sb.append("</div>");
				sb.append(customQuestion);
				sb.append("<div class='row left25'>");
			}*/
			sb.append("</div>");
		}
		//Can Sub
		DspqRouter dspqRouterSection21=mapDspqRouterBySID.get(21);
		if(dspqRouterSection21!=null)
		{
			sb.append("<div class='row left15'><div class='col-sm-12 col-md-12'><div class='span4 portfolio_Subheading'>"+dspqRouterSection21.getDisplayName()+"</div></div>");
			String sSectionName="tooltipSection21";				
			if(mapDspqRouterByFID.get(98)!=null)
			{
				
				DspqRouter dspqRouter=mapDspqRouterByFID.get(98);
				if(dspqRouter!=null)
				{
					sb.append("<div class='col-sm-5 col-md-5 divprofile'>"+dspqRouter.getDisplayName()+":</div>");	
					Integer canServeAsSubTeacher=null;
					if(teacherExperience!=null && teacherExperience.getCanServeAsSubTeacher()!=null)
						canServeAsSubTeacher=teacherExperience.getCanServeAsSubTeacher();
					
					String sInputControlId="Substitute_Teacher";
					
					if(canServeAsSubTeacher!=null && canServeAsSubTeacher==1)
						sb.append("<div class='col-sm-2 col-md-2'>Yes</div>");	
					else 
						sb.append("<div class='col-sm-2 col-md-2'>No</div>");										
				}
			}	
			sb.append("</div>");
			/*customQuestion="";
			customQuestion=getCustomFieldBySectionIdProfiler(dspqPotfolioDM, candidateType, dspqGroupMaster, dspqRouterSection21.getDspqSectionMaster(),listToolTips,sSectionName,listCalDated,dspqPortfolioName,teacherDetail);
			if(!customQuestion.equalsIgnoreCase("")){
				sb.append("</div>");
				sb.append(customQuestion);
				sb.append("<div class='row left25'>");
			}*/			
			
		}
		List<DistrictMaster> lstDistrictMaster=new ArrayList<DistrictMaster>();
		lstDistrictMaster.add(dspqPotfolioDM);
		
		List<DspqPortfolioName> lstDspqPortfolioName=new ArrayList<DspqPortfolioName>();
		lstDspqPortfolioName.add(dspqPortfolioName);
		String sSectionName="tooltipSection21";	
		///// 29
		if(dspqRouterSection29!=null){
			customQuestion="";
			customQuestion=getCustomFieldBySectionIdProfiler(lstDistrictMaster, candidateType, dspqRouterSection29.getDspqSectionMaster(),listToolTips,sSectionName,listCalDated,lstDspqPortfolioName,teacherDetail,iOldQuestionDisplaySectionId,iNewJobId);
			if(!customQuestion.equalsIgnoreCase("")){
				sb.append("<div class='row left30'>");
				sb.append(customQuestion);
				sb.append("</div>");
				//sb.append("<div class='row left25'>");
			}
		}
		
		//////////// 23
		if(dspqRouterSection23!=null){
		customQuestion="";
		customQuestion=getCustomFieldBySectionIdProfiler(lstDistrictMaster, candidateType, dspqRouterSection23.getDspqSectionMaster(),listToolTips,sSectionName,listCalDated,lstDspqPortfolioName,teacherDetail,iOldQuestionDisplaySectionId,iNewJobId);
		if(!customQuestion.equalsIgnoreCase("")){
			sb.append("<div class='row left30'>");
			sb.append(customQuestion);
			sb.append("</div>");
			//sb.append("<div class='row left25'>");
		}
		
		}
		//////////  7
		if(dspqRouterSection7!=null){
		customQuestion="";
		customQuestion=getCustomFieldBySectionIdProfiler(lstDistrictMaster, candidateType, dspqRouterSection7.getDspqSectionMaster(),listToolTips,sSectionName,listCalDated,lstDspqPortfolioName,teacherDetail,iOldQuestionDisplaySectionId,iNewJobId);
		if(!customQuestion.equalsIgnoreCase("")){
			sb.append("<div class='row left30'>");
			sb.append(customQuestion);
			sb.append("</div>");
		}
		}
		//////////  21
		if(dspqRouterSection21!=null){
		customQuestion="";
		customQuestion=getCustomFieldBySectionIdProfiler(lstDistrictMaster, candidateType, dspqRouterSection21.getDspqSectionMaster(),listToolTips,sSectionName,listCalDated,lstDspqPortfolioName,teacherDetail,iOldQuestionDisplaySectionId,iNewJobId);
		if(!customQuestion.equalsIgnoreCase("")){
			sb.append("<div class='row left30'>");
			sb.append(customQuestion);
			sb.append("</div>");
		}
		
		}
		
	}
	return sb.toString();
}
public String validateApplicantType(String candidateType){
	String defCandidateType="E";
	if(candidateType.equals("I")){
		defCandidateType="I";
	}else if(candidateType.equals("T")){
		defCandidateType="E";
	}else{
		defCandidateType="E";
	}
	return defCandidateType;
}
@Transactional(readOnly=false)
public String getCustomQuestion(String iJobId,String dspqPortfolioNameId,String sCandidateType,Integer groupId,Integer teacherId,String showType)
{
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	UserMaster userMaster=null;
	String candidateType="";
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	}
	if(session.getAttribute("userMaster")!=null){
		userMaster=(UserMaster)session.getAttribute("userMaster");
	}
	StringBuffer sb=new StringBuffer();
	JobOrder jobOrder=null;
	TeacherDetail teacherDetail=null;
	int JobId=0;
	if(iJobId!=null && !iJobId.equals("") && !iJobId.equalsIgnoreCase("0")){
		JobId=Utility.getIntValue(iJobId);
		jobOrder=jobOrderDAO.findById(JobId, false, false);
	}
	teacherDetail = teacherDetailDAO.findById(teacherId,false,false);
	candidateType=validateApplicantType(sCandidateType);
	
	/*System.out.println("iJobId " +iJobId);
	System.out.println("dspqPortfolioNameId " +dspqPortfolioNameId);
	System.out.println("candidateType " +candidateType);
	System.out.println("teacherDetail " +teacherDetail.getFirstName());
	*/
	//DspqGroupMaster dspqGroupMaster=dspqGroupMasterDAO.findById(4, false, false);
	List<DspqGroupMaster> dspqGroupMasterlst=dspqGroupMasterDAO.findAll();
	List<DspqSectionMaster> sectionMasters=dspqSectionMasterDAO.getDspqSectionListByGroupList(dspqGroupMasterlst);
	List<DspqFieldMaster> dspqFieldList = dspqFieldMasterDAO.getFieldListBySections(sectionMasters);
	List<Integer> portfolioIds=new ArrayList<Integer>();
	String[] portfolioIdNew=null;
	if(showType.equalsIgnoreCase("cg") || showType.equalsIgnoreCase("My Folders") || showType.equalsIgnoreCase("Mosaic") || showType.equalsIgnoreCase("PNR Dashboard")){
		portfolioIds.add(Utility.getIntValue(dspqPortfolioNameId));
	}else if(showType.equalsIgnoreCase("pool")){
		if(userMaster!=null && userMaster.getEntityType()==1){
			portfolioIdNew=dspqPortfolioNameId.split(",");
			for(int i=0;i<portfolioIdNew.length;i++){
					portfolioIds.add(Utility.getIntValue(portfolioIdNew[i]));
			}
		}else{
			portfolioIds.add(Utility.getIntValue(dspqPortfolioNameId));
		}
	}
	List<DspqPortfolioName> dspqPortfolioName = dspqPortfolioNameDAO.findPortfolioNameByPortfolioIds(portfolioIds);
	List<DspqRouter> dspqRouterList=dspqRouterDAO.getControlsByPortfolioNameAndCTAndField(dspqPortfolioName, candidateType, dspqFieldList,sectionMasters);
	Map<Integer, DspqRouter> mapDspqRouterByFID=new HashMap<Integer, DspqRouter>();
	Map<Integer, DspqRouter> mapDspqRouterBySID=new HashMap<Integer, DspqRouter>();
	setSIDAndFIDInMapProfiler(mapDspqRouterByFID, mapDspqRouterBySID, dspqRouterList);
	
	List<String> listToolTips = new ArrayList<String>();
	List<String> listCalDated = new ArrayList<String>();
	
	int iNewJobId=Utility.getIntValue(iJobId);
	int iOldQuestionDisplaySectionId=0;
	if(iNewJobId>0)
		iOldQuestionDisplaySectionId = getSectionIdForShowOldQuestion(iNewJobId, teacherDetail, candidateType, dspqPortfolioName.get(0));
	
	if(mapDspqRouterByFID!=null)
	{	
		sb.append("<div class='row left1 top10'>");
		sb.append("<div class='col-md-1 col-sm-1'  style='padding-left: 0px;'></div>");
		sb.append("<div class='col-md-11 col-sm-11'  style='padding-left: 0px;'>");
		List<DistrictMaster> dspqPotfolioDM=new ArrayList<DistrictMaster>();
		String customQuestion="";
		for(DspqPortfolioName rec:dspqPortfolioName){
		if(rec!=null && rec.getDistrictMaster()!=null)
			dspqPotfolioDM.add(rec.getDistrictMaster());
		}
		//Personal Information
		DspqRouter dspqRouterSection2=mapDspqRouterBySID.get(2);
		if(dspqRouterSection2!=null)
		{
			String sSectionName="tooltipSection02";		
			customQuestion="";
			customQuestion=getCustomFieldBySectionIdProfiler(dspqPotfolioDM, candidateType, dspqRouterSection2.getDspqSectionMaster(),listToolTips,sSectionName,listCalDated,dspqPortfolioName,teacherDetail,iOldQuestionDisplaySectionId,JobId);
			if(!customQuestion.equalsIgnoreCase("")){
				sb.append("<div class='row'>");
				sb.append(customQuestion);
				sb.append("</div>");
			}
			
		}
		//EEOC
		DspqRouter dspqRouterSection3=mapDspqRouterBySID.get(3);
		if(dspqRouterSection3!=null)
		{			
			String sSectionName="tooltipSection3";
			customQuestion="";
			customQuestion=getCustomFieldBySectionIdProfiler(dspqPotfolioDM, candidateType, dspqRouterSection3.getDspqSectionMaster(),listToolTips,sSectionName,listCalDated,dspqPortfolioName,teacherDetail,iOldQuestionDisplaySectionId,JobId);
			if(!customQuestion.equalsIgnoreCase("")){
				sb.append("<div class='row'>");
				sb.append(customQuestion);
				sb.append("</div>");
			}
		}
		//Permanent Address
		DspqRouter dspqRouterSection4=mapDspqRouterBySID.get(4);
		if(dspqRouterSection4!=null)
		{
			String sSectionName="tooltipSection4";	
			customQuestion="";
			customQuestion=getCustomFieldBySectionIdProfiler(dspqPotfolioDM, candidateType, dspqRouterSection4.getDspqSectionMaster(),listToolTips,sSectionName,listCalDated,dspqPortfolioName,teacherDetail,iOldQuestionDisplaySectionId,JobId);
			if(!customQuestion.equalsIgnoreCase("")){
				sb.append("<div class='row'>");
				sb.append(customQuestion);
				sb.append("</div>");
			}					
		}
		
		
		//DOB
		DspqRouter dspqRouterSection16=mapDspqRouterBySID.get(16);
		if(dspqRouterSection16!=null)
		{
			String sSectionName="tooltipSection15";
			customQuestion="";
			customQuestion=getCustomFieldBySectionIdProfiler(dspqPotfolioDM,candidateType, dspqRouterSection16.getDspqSectionMaster(),listToolTips,sSectionName,listCalDated,dspqPortfolioName,teacherDetail,iOldQuestionDisplaySectionId,JobId);
			if(!customQuestion.equalsIgnoreCase("")){	
				sb.append("<div class='row'>");
				sb.append(customQuestion);
				sb.append("</div>");
			}
		}
			//Start Present Address			
			
			DspqRouter dspqRouterSection17=mapDspqRouterBySID.get(17);
			if(dspqRouterSection17!=null)
			{	
				String sSectionName="tooltipSection17";
				customQuestion="";
				customQuestion=getCustomFieldBySectionIdProfiler(dspqPotfolioDM, candidateType, dspqRouterSection17.getDspqSectionMaster(),listToolTips,sSectionName,listCalDated,dspqPortfolioName,teacherDetail,iOldQuestionDisplaySectionId,JobId);
				if(!customQuestion.equalsIgnoreCase("")){
					sb.append("<div class='row'>");
					sb.append(customQuestion);
					sb.append("</div>");
				}					
			}	
			
			//Start Veteran
			DspqRouter dspqRouterSection20=mapDspqRouterBySID.get(20);
			if(dspqRouterSection20!=null)
			{
				String sSectionName="tooltipSection20";
				customQuestion="";
				customQuestion=getCustomFieldBySectionIdProfiler(dspqPotfolioDM, candidateType, dspqRouterSection20.getDspqSectionMaster(),listToolTips,sSectionName,listCalDated,dspqPortfolioName,teacherDetail,iOldQuestionDisplaySectionId,JobId);
				if(!customQuestion.equalsIgnoreCase("")){
					sb.append("<div class='row'>");
					sb.append(customQuestion);
					sb.append("</div>");
				}
			}
			//End Veteran
			
			//Start Driver Licence
			DspqRouter dspqRouterSection22=mapDspqRouterBySID.get(22);
			if(dspqRouterSection22!=null)
			{
				String sSectionName="tooltipSection22";			
				customQuestion="";
				customQuestion=getCustomFieldBySectionIdProfiler(dspqPotfolioDM, candidateType, dspqRouterSection22.getDspqSectionMaster(),listToolTips,sSectionName,listCalDated,dspqPortfolioName,teacherDetail,iOldQuestionDisplaySectionId,JobId);
				if(!customQuestion.equalsIgnoreCase("")){
					sb.append("<div class='row'>");
					sb.append(customQuestion);
					sb.append("</div>");
				}
			}

			//Start Resume
			DspqRouter dspqRouterSection12=mapDspqRouterBySID.get(12);
			if(dspqRouterSection12!=null)
			{
				String sSectionName="tooltipSection12";			
				customQuestion="";
				customQuestion=getCustomFieldBySectionIdProfiler(dspqPotfolioDM, candidateType, dspqRouterSection12.getDspqSectionMaster(),listToolTips,sSectionName,listCalDated,dspqPortfolioName,teacherDetail,iOldQuestionDisplaySectionId,JobId);
				if(!customQuestion.equalsIgnoreCase("")){
					sb.append("<div class='row'>");
					sb.append(customQuestion);
					sb.append("</div>");
				}
			}
			
			
			sb.append("</div>");
	}
	return sb.toString();
}

@Transactional(readOnly=false)
public String getCustomQuestionMaster(int jobId, String dspqPortfolioNameId,String sCandidateType,Integer groupId,Integer sectionId,Integer teacherId,String showType)
{
	List<DistrictMaster> dspqPotfolioDM=new ArrayList<DistrictMaster>();
	String customQuestion="";
	try{
		List<Integer> portfolioIds=new ArrayList<Integer>();
		String[] portfolioIdNew=null;
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster=null;
		String candidateType="";
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		if(session.getAttribute("userMaster")!=null){
			userMaster=(UserMaster)session.getAttribute("userMaster");
		}
		if(showType.equalsIgnoreCase("cg") || showType.equalsIgnoreCase("My Folders") || showType.equalsIgnoreCase("Mosaic") || showType.equalsIgnoreCase("PNR Dashboard")){
			portfolioIds.add(Utility.getIntValue(dspqPortfolioNameId));
		}else if(showType.equalsIgnoreCase("pool")){
			if(userMaster!=null)
			if(userMaster!=null && userMaster.getEntityType()==1){
				portfolioIdNew=dspqPortfolioNameId.split(",");
				for(int i=0;i<portfolioIdNew.length;i++){
						portfolioIds.add(Utility.getIntValue(portfolioIdNew[i]));
				}
			}else{
				portfolioIds.add(Utility.getIntValue(dspqPortfolioNameId));
			}
		}
		candidateType=validateApplicantType(sCandidateType);
		TeacherDetail teacherDetail=null;
		if(teacherId!=null && teacherId!=0)
			 teacherDetail=teacherDetailDAO.findById(teacherId, false, false);
		List<DspqPortfolioName> dspqPortfolioName = dspqPortfolioNameDAO.findPortfolioNameByPortfolioIds(portfolioIds);
		
		for(DspqPortfolioName rec:dspqPortfolioName){
		if(rec!=null && rec.getDistrictMaster()!=null)
			dspqPotfolioDM.add(rec.getDistrictMaster());
		}
		
		DspqGroupMaster dspqGroupMaster=dspqGroupMasterDAO.findById(groupId, false, false);
		List<DspqSectionMaster> sectionMasters=dspqSectionMasterDAO.getDspqSectionListByGroup(dspqGroupMaster);
		List<DspqFieldMaster> dspqFieldList = dspqFieldMasterDAO.getFieldListBySections(sectionMasters);
		List<DspqRouter> dspqRouterList=dspqRouterDAO.getControlsByPortfolioNameAndCTAndField(dspqPortfolioName, candidateType, dspqFieldList,sectionMasters);
		
		Map<Integer, DspqRouter> mapDspqRouterByFID=new HashMap<Integer, DspqRouter>();
		Map<Integer, DspqRouter> mapDspqRouterBySID=new HashMap<Integer, DspqRouter>();
		setSIDAndFIDInMapProfiler(mapDspqRouterByFID, mapDspqRouterBySID, dspqRouterList);
		
		List<String> listToolTips = new ArrayList<String>();
		List<String> listCalDated = new ArrayList<String>();
		
		int iOldQuestionDisplaySectionId=0;
		if(jobId>0)
			iOldQuestionDisplaySectionId = getSectionIdForShowOldQuestion(jobId, teacherDetail, candidateType, dspqPortfolioName.get(0));

		
		DspqRouter dspqRouterSection=mapDspqRouterBySID.get(sectionId);
		String sSectionName="tooltipSection"+sectionId;
		customQuestion=getCustomFieldBySectionIdProfiler(dspqPotfolioDM, candidateType, dspqRouterSection.getDspqSectionMaster(),listToolTips,sSectionName,listCalDated,dspqPortfolioName,teacherDetail,iOldQuestionDisplaySectionId,jobId);
	}catch (Exception e) {
		e.printStackTrace();
	}
	return customQuestion;
}
/*
 * Adding by deepak
 * For the finding portfolio configured on job
 */
public boolean checkPortfolioAvaliablity(List<TeacherDetail> lstTeacherDetail,UserMaster userMaster,DistrictMaster districtMaster,Map<Integer,String>  portfolioIdMap){
		System.out.println("::::::::::::::::::::::::::::::::::::::::::::::::: checkPortfolioAvaliablity method ::::::::::::::::::::::::::::::::::::::::::::::::::::");
	  	WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		Boolean portfolioFlag=false;
		int districtIds=0;
		String portfolioIds="";
		try{
			if(districtMaster!=null){
				boolean bServicePortfolioStatus=jobApplicationServiceAjax.getSelfServicePortfolioStatus(districtMaster);
				if(bServicePortfolioStatus){
				List<String[]>  jobForTeacherDistrictList=jobForTeacherDAO.findDistrictByTeacherIds(lstTeacherDetail);
				List<JobCategoryMaster> jobCategoryMasterList=jobCategoryMasterDAO.findAllJobCategoryNameByDistrict(districtMaster);
				List<JobCategoryTransaction> lstJobCategoryTrans= jobCategoryTransactionDAO.findListByjobCategoryId(jobCategoryMasterList);
				Set<Integer> portfolioIs = new HashSet<Integer>();
				String portfolioType="";
				Integer portfolioId=null;
				Integer teacherId=null;
				int setCounter=0;
				for(JobCategoryTransaction tr:lstJobCategoryTrans){
					if(tr.getPortfolio()!=null)
						portfolioIs.add(tr.getPortfolio());
				}
				String portfolioIdsString="";
				for (Iterator<Integer> it = portfolioIs.iterator(); it.hasNext(); ) {
					if(setCounter==0){
						portfolioIdsString+=it.next();
					}else{
						portfolioIdsString+=","+it.next();
					}
					setCounter++;
				}
				System.out.println("All portfolio Id ::::::: "+portfolioIdsString);
				for(JobCategoryTransaction  dt:lstJobCategoryTrans){
					if(dt!=null){
	
						for(TeacherDetail data:lstTeacherDetail){
							for (Iterator it = jobForTeacherDistrictList.iterator(); it.hasNext();){ 
								Object[] row = (Object[]) it.next(); 
								if(row[1]!=null){
									if(((Integer) row[1]).intValue()==data.getTeacherId()){
										portfolioIdMap.put(data.getTeacherId(), dt.getCandidateType()+"#"+dt.getPortfolio());
									}
								}
							}
						}
						portfolioFlag=true;
					}
				 }
				}
				}else {
					if(userMaster.getEntityType()==1){
						DistrictMaster districtMasterObj=null;
						List<String[]>  jobForTeacherDistrictList=null;
						if(lstTeacherDetail!=null && lstTeacherDetail.size()>0){
							jobForTeacherDistrictList=jobForTeacherDAO.findDistrictByTeacherIds(lstTeacherDetail);
						}
						List<DistrictMaster> districtMasterList=new ArrayList<DistrictMaster>();
						if(jobForTeacherDistrictList!=null){
							for (Iterator it = jobForTeacherDistrictList.iterator(); it.hasNext();){ 
								districtMasterObj=new DistrictMaster();
								Object[] row = (Object[]) it.next(); 
								if(row[0]!=null){
									districtIds=((Integer) row[0]).intValue();
									districtMasterObj.setDistrictId(districtIds);
									districtMasterList.add(districtMasterObj);
								}
							}
						}
						List<JobCategoryMaster> jobCategoryMasterList=null;
						if(districtMasterList.size()>0)
							jobCategoryMasterList=jobCategoryMasterDAO.findJobCategorysByDistricts(districtMasterList);
						Set<Integer> portfolioIs = new HashSet<Integer>();
						String portfolioType="";
						Integer portfolioId=null;
						Integer teacherId=null;
						int setCounter=0;
						List<JobCategoryTransaction> lstJobCategoryTrans=null;
						if(jobCategoryMasterList!=null){
							lstJobCategoryTrans=jobCategoryTransactionDAO.findListByjobCategoryId(jobCategoryMasterList);
						}
						if(lstJobCategoryTrans!=null)
							for(JobCategoryTransaction tr:lstJobCategoryTrans){
								if(tr.getPortfolio()!=null)
									portfolioIs.add(tr.getPortfolio());
						}
						String portfolioIdsString="";
						for (Iterator<Integer> it = portfolioIs.iterator(); it.hasNext(); ) {
							if(setCounter==0){
								portfolioIdsString+=it.next();
							}else{
								portfolioIdsString+=","+it.next();
							}
							setCounter++;
						}
						session.setAttribute("multiplePortfolioIds", portfolioIdsString);
						System.out.println("All portfolio Id ::::::: "+portfolioIdsString);
						if(lstJobCategoryTrans!=null){
							for(JobCategoryTransaction  dt:lstJobCategoryTrans){
								if(dt!=null){
									for(TeacherDetail data:lstTeacherDetail){
										for (Iterator it = jobForTeacherDistrictList.iterator(); it.hasNext();){ 
											Object[] row = (Object[]) it.next(); 
											if(row[3]!=null){
												if(((Integer) row[3]).intValue()==dt.getJobCategoryId()){
													portfolioType=dt.getCandidateType();
													portfolioId=dt.getPortfolio();
													teacherId=data.getTeacherId();
													break;
												}
											}
										}
										if(teacherId!=null)
											break;
									}
								}
								if(teacherId!=null)
									break;
							}
						}
						if(teacherId!=null){
							portfolioIdMap.put(teacherId, portfolioType+"#"+portfolioId);
							portfolioFlag=true;
						}
				}/*else if(userMaster.getEntityType()==3){
					JobCategoryMaster jobCategoryMaster=null;
					List<SchoolInJobOrder> schoolInJobList=null;
					List<JobCategoryMaster> jobCategoryMasterList=new ArrayList<JobCategoryMaster>();
					schoolInJobList=schoolInJobOrderDAO.findJobBySchool(userMaster.getSchoolId());
					System.out.println("SA JFT List :::::::: "+schoolInJobList.size());
					for(SchoolInJobOrder rec: schoolInJobList){
						jobCategoryMaster=new JobCategoryMaster();
						jobCategoryMaster.setJobCategoryId(rec.getJobId().getJobCategoryMaster().getJobCategoryId());
						jobCategoryMasterList.add(jobCategoryMaster);
					}
					List<JobCategoryTransaction> lstJobCategoryTrans= jobCategoryTransactionDAO.findListByjobCategoryId(jobCategoryMasterList);
					for(JobCategoryTransaction  dt:lstJobCategoryTrans){
						if(dt!=null){
							for(SchoolInJobOrder rec: schoolInJobList){
								if(rec.getJobId().getJobCategoryMaster().getJobCategoryId()==dt.getJobCategoryId()){
									for(TeacherDetail data:rec.getJobId().getTeacherDetail()){
										portfolioIdMap.put(data.getTeacherId(), dt.getCandidateType()+"#"+dt.getPortfolio());
									}
								}
							}
							portfolioFlag=true;
						}
					}
				  }*/
		}
		}catch (Exception e) {
			e.printStackTrace();
		}
	return portfolioFlag;
}

public int getSectionIdForShowOldQuestion(int iJobId,TeacherDetail teacherDetail,String candidateType,DspqPortfolioName dspqPortfolioName)
{
	
	int iNewQuestionSectionId=0;
	if(iJobId >0)
	{
		List<DistrictSpecificPortfolioQuestions> districtSpecificPortfolioQuestions=null;
		JobOrder jobOrder=jobOrderDAO.findById(iJobId, false, false);
		int dspqType=0;	
		Map<Integer,List<DistrictSpecificPortfolioQuestions>> dSPQ=districtSpecificPortfolioQuestionsDAO.getDistrictSpecificPortfolioQuestion(jobOrder,false);
		try{
			for(int i=0;i<5;i++){
				if(dSPQ.get(i)!=null)
				{
					districtSpecificPortfolioQuestions=dSPQ.get(i);
					dspqType=i;
					break;
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	//boolean bShowOldQuestion=false;
	List<DistrictSpecificPortfolioAnswers> lastList = districtSpecificPortfolioAnswersDAO.findSpecificPortfolioAnswersTP(teacherDetail,jobOrder,dspqType);
	if(lastList!=null && lastList.size()>0)
	{
		//bShowOldQuestion=true;
		List<Object> lstDistSpecQuestions= new ArrayList<Object>();
		lstDistSpecQuestions=districtSpecificPortfolioQuestionsDAO.getSectionForNewSS(candidateType, dspqPortfolioName);
		System.out.println("lstDistSpecQuestions "+lstDistSpecQuestions.size());
		if(lstDistSpecQuestions!=null && lstDistSpecQuestions.size()>0)
		{
			if(lstDistSpecQuestions.size()==1)
			{
				Object object=lstDistSpecQuestions.get(0);
				Object[] objArr1 = (Object[]) object;
				iNewQuestionSectionId=Utility.getIntValue(objArr1[0]+"");
			}
		}
	 }
	}
	//System.out.println("dspqType "+dspqType+" bShowOldQuestion "+bShowOldQuestion+" iNewQuestionSectionId "+iNewQuestionSectionId);
	return iNewQuestionSectionId;
}
@Transactional(readOnly=false)
public String getOldCustomQuestionByDistrict(Integer teacherId)
{
	StringBuffer sb=new StringBuffer();
	
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	boolean editableStatus=false;
	UserMaster userMaster=null;
	int counter=0;
	String checkedValue="";
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	}
	if(session.getAttribute("userMaster")!=null){
		userMaster=(UserMaster)session.getAttribute("userMaster");
		if(userMaster!=null && (userMaster.getDistrictId()!=null && (userMaster.getDistrictId().getSelfServiceCustomQuestion()!=null && userMaster.getDistrictId().getSelfServiceCustomQuestion()==1))){
			editableStatus=true;
		}
	}
	TeacherDetail teacherDetail=null;	
	if(teacherId!=null && teacherId!=0){
		teacherDetail=teacherDetailDAO.findById(teacherId, false, false);
	}
	List<DistrictMaster> districtMasterList=new ArrayList<DistrictMaster>();
	List<DistrictSpecificPortfolioQuestions> lstDistrictSpecificPortfolioQuestions=null;
	DistrictMaster districtMasterTemp=null;
	if(userMaster.getEntityType().equals(1)){
		List<JobForTeacher> jobForTeacherList=jobForTeacherDAO.findAllAppliedJobsOfTeacher(teacherId);
		if(jobForTeacherList!=null && jobForTeacherList.size()>0){
			for (JobForTeacher jobForTeacher : jobForTeacherList) {
				districtMasterTemp=new DistrictMaster();
				districtMasterTemp.setDistrictId(jobForTeacher.getDistrictId());
				districtMasterList.add(districtMasterTemp);
			}
		}
	}
	else{
		districtMasterTemp=new DistrictMaster();
		districtMasterList.add(userMaster.getDistrictId());
	}
	
	if(districtMasterList!=null && districtMasterList.size()>0)
		lstDistrictSpecificPortfolioQuestions=districtSpecificPortfolioQuestionsDAO.getCustomQuestionByDistrictList(districtMasterList);
	
	Map<Integer, DistrictSpecificPortfolioAnswers> mapQA=new HashMap<Integer, DistrictSpecificPortfolioAnswers>();
	List<DistrictSpecificPortfolioAnswers> lstDistrictSpecificPortfolioAnswers = new ArrayList<DistrictSpecificPortfolioAnswers>();
	List<DistrictSpecificPortfolioQuestions> districtSpecificPortfolioQuestionsFinal = new ArrayList<DistrictSpecificPortfolioQuestions>();
	DistrictSpecificPortfolioQuestions districtSpecificPortfolioQuestions=null;
	for(DistrictSpecificPortfolioQuestions rec:lstDistrictSpecificPortfolioQuestions){
		if(rec!=null && rec.getQuestionId()!=null){
			districtSpecificPortfolioQuestions=new DistrictSpecificPortfolioQuestions();
			districtSpecificPortfolioQuestions.setQuestionId(rec.getQuestionId());
			districtSpecificPortfolioQuestionsFinal.add(districtSpecificPortfolioQuestions);
		}
	}
	if(districtSpecificPortfolioQuestionsFinal!=null && districtSpecificPortfolioQuestionsFinal.size()>0)
	{
		lstDistrictSpecificPortfolioAnswers=districtSpecificPortfolioAnswersDAO.getDSPAByTID(teacherDetail, districtSpecificPortfolioQuestionsFinal);
	
		for(DistrictSpecificPortfolioAnswers obj:lstDistrictSpecificPortfolioAnswers)
		{
			mapQA.put(obj.getDistrictSpecificPortfolioQuestions().getQuestionId(), obj);
		}
	}
	if(districtSpecificPortfolioQuestionsFinal!=null && districtSpecificPortfolioQuestionsFinal.size()>0)
	{
		//sb.append("<div class='row'><div class='col-sm-12 col-md-12'>&nbsp;</div></div>");
		
		int iTotalCountNo_CustomField=lstDistrictSpecificPortfolioQuestions.size();
		int sSectionNameCustFld=0;
		sb.append("<input type='hidden' id='customFld_"+sSectionNameCustFld+"' customFldTotalCount='"+iTotalCountNo_CustomField+"' value="+iTotalCountNo_CustomField+">");
		int iCustFldCounter=0;			
		for(DistrictSpecificPortfolioQuestions dspq:lstDistrictSpecificPortfolioQuestions)
		{
			sb.append("<div class='row top5'>");
			//sb.append("</div></div>");
			
			iCustFldCounter++;
			List<DistrictSpecificPortfolioOptions> lstDistrictSpecificPortfolioOptions=dspq.getQuestionOptions();
			String sShortName=dspq.getQuestionTypeMaster().getQuestionTypeShortName();
			
			if(dspq!=null)
			{
				String sOptionId="_"+dspq.getQuestionId();
				
				String sTextAReaValue="";
				DistrictSpecificPortfolioAnswers Qans=null;
				if(mapQA!=null && mapQA.get(dspq.getQuestionId())!=null)
				{
					Qans=mapQA.get(dspq.getQuestionId());
					if(Qans!=null)
					{
						if(Qans.getInsertedText()!=null && !Qans.getInsertedText().equals(""))
						{
							sTextAReaValue=Qans.getInsertedText();
						}
					}
				}					
					sb.append("<div class='col-sm-12 col-md-12 divprofile'>");
					sb.append("<b>Question:</b> "+getLabelAndCustomFieldAttributes(dspq, null,iCustFldCounter,null,Qans));
					sb.append("</div>");
				sb.append("<div class='col-sm-10 col-md-10 pright20'><b>Answer:</b> ");
				if(sShortName.equalsIgnoreCase("ml") || sShortName.equalsIgnoreCase("et"))
				{
					if(!editableStatus){
						if(sTextAReaValue.equalsIgnoreCase(""))
							sb.append(lblNA);
						else
							sb.append(sTextAReaValue);
					}
					else{
						sb.append("<div id='divOpt"+sOptionId+"'>");
						sb.append("<textarea name='txtAreaOpt"+sOptionId+"' id='txtAreaOpt"+sOptionId+"' rows='8' style='width:100%;' class='form-control' maxlength='5000' onkeyup=\"return chkLenOfTextArea(this,5000)\" onblur=\"return chkLenOfTextArea(this,5000)\">"+sTextAReaValue+"</textarea><br/>");
						sb.append("</div>");
					}
					/*sb.append("<div id='divOpt"+sOptionId+"'>");
					//sb.append("<textarea name='txtAreaOpt"+sOptionId+"' id='txtAreaOpt"+sOptionId+"' value='"+sTextAReaValue+"' class='form-control' maxlength='5000' onkeyup=\"return chkLenOfTextArea(this,5000)\" onblur=\"return chkLenOfTextArea(this,5000)\"/>"+sTextAReaValue+"</textarea><br/>");
					sb.append("<textarea "+enableDisableFields+" name='txtAreaOpt"+sOptionId+"' id='txtAreaOpt"+sOptionId+"' rows='8' style='width:100%;' class='form-control' maxlength='5000' onkeyup=\"return chkLenOfTextArea(this,5000)\" onblur=\"return chkLenOfTextArea(this,5000)\">"+sTextAReaValue+"</textarea><br/>");
					//sb.append("<div class='jqte'><div class='jqte_toolbar' role='toolbar' unselectable='on' style='-webkit-user-select: none;'><div class='jqte_tool jqte_tool_1' role='button' data-tool='0' unselectable='on' style='-webkit-user-select: none;'><a class='jqte_tool_icon' unselectable='on' style='-webkit-user-select: none;'></a><div class='jqte_fontsizes' unselectable='on' style='position: absolute; display: none; -webkit-user-select: none;'><a jqte-styleval='10' class='jqte_fontsize' style='font-size: 10px; -webkit-user-select: none;' role='menuitem' unselectable='on'>Abcdefgh...</a><a jqte-styleval='12' class='jqte_fontsize' style='font-size: 12px; -webkit-user-select: none;' role='menuitem' unselectable='on'>Abcdefgh...</a><a jqte-styleval='16' class='jqte_fontsize' style='font-size: 16px; -webkit-user-select: none;' role='menuitem' unselectable='on'>Abcdefgh...</a><a jqte-styleval='18' class='jqte_fontsize' style='font-size: 18px; -webkit-user-select: none;' role='menuitem' unselectable='on'>Abcdefgh...</a><a jqte-styleval='20' class='jqte_fontsize' style='font-size: 20px; -webkit-user-select: none;' role='menuitem' unselectable='on'>Abcdefgh...</a><a jqte-styleval='24' class='jqte_fontsize' style='font-size: 24px; -webkit-user-select: none;' role='menuitem' unselectable='on'>Abcdefgh...</a><a jqte-styleval='28' class='jqte_fontsize' style='font-size: 28px; -webkit-user-select: none;' role='menuitem' unselectable='on'>Abcdefgh...</a></div></div><div class='jqte_tool jqte_tool_2' role='button' data-tool='1' unselectable='on' style='-webkit-user-select: none;'><a class='jqte_tool_icon' unselectable='on' style='-webkit-user-select: none;'></a><div class='jqte_cpalette' unselectable='on' style='position: absolute; display: none; -webkit-user-select: none;'><a jqte-styleval='0,0,0' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(0, 0, 0);' role='gridcell' unselectable='on'></a><a jqte-styleval='68,68,68' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(68, 68, 68);' role='gridcell' unselectable='on'></a><a jqte-styleval='102,102,102' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(102, 102, 102);' role='gridcell' unselectable='on'></a><a jqte-styleval='153,153,153' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(153, 153, 153);' role='gridcell' unselectable='on'></a><a jqte-styleval='204,204,204' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(204, 204, 204);' role='gridcell' unselectable='on'></a><a jqte-styleval='238,238,238' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(238, 238, 238);' role='gridcell' unselectable='on'></a><a jqte-styleval='243,243,243' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(243, 243, 243);' role='gridcell' unselectable='on'></a><a jqte-styleval='255,255,255' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(255, 255, 255);' role='gridcell' unselectable='on'></a><a jqte-styleval='244,204,204' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(244, 204, 204);' role='gridcell' unselectable='on'></a><a jqte-styleval='252,229,205' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(252, 229, 205);' role='gridcell' unselectable='on'></a><a jqte-styleval='255,242,204' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(255, 242, 204);' role='gridcell' unselectable='on'></a><a jqte-styleval='217,234,211' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(217, 234, 211);' role='gridcell' unselectable='on'></a><a jqte-styleval='208,224,227' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(208, 224, 227);' role='gridcell' unselectable='on'></a><a jqte-styleval='207,226,243' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(207, 226, 243);' role='gridcell' unselectable='on'></a><a jqte-styleval='217,210,233' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(217, 210, 233);' role='gridcell' unselectable='on'></a><a jqte-styleval='234,209,220' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(234, 209, 220);' role='gridcell' unselectable='on'></a><a jqte-styleval='234,153,153' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(234, 153, 153);' role='gridcell' unselectable='on'></a><a jqte-styleval='249,203,156' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(249, 203, 156);' role='gridcell' unselectable='on'></a><a jqte-styleval='255,229,153' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(255, 229, 153);' role='gridcell' unselectable='on'></a><a jqte-styleval='182,215,168' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(182, 215, 168);' role='gridcell' unselectable='on'></a><a jqte-styleval='162,196,201' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(162, 196, 201);' role='gridcell' unselectable='on'></a><a jqte-styleval='159,197,232' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(159, 197, 232);' role='gridcell' unselectable='on'></a><a jqte-styleval='180,167,214' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(180, 167, 214);' role='gridcell' unselectable='on'></a><a jqte-styleval='213,166,189' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(213, 166, 189);' role='gridcell' unselectable='on'></a><a jqte-styleval='224,102,102' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(224, 102, 102);' role='gridcell' unselectable='on'></a><a jqte-styleval='246,178,107' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(246, 178, 107);' role='gridcell' unselectable='on'></a><a jqte-styleval='255,217,102' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(255, 217, 102);' role='gridcell' unselectable='on'></a><a jqte-styleval='147,196,125' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(147, 196, 125);' role='gridcell' unselectable='on'></a><a jqte-styleval='118,165,175' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(118, 165, 175);' role='gridcell' unselectable='on'></a><a jqte-styleval='111,168,220' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(111, 168, 220);' role='gridcell' unselectable='on'></a><a jqte-styleval='142,124,195' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(142, 124, 195);' role='gridcell' unselectable='on'></a><a jqte-styleval='194,123,160' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(194, 123, 160);' role='gridcell' unselectable='on'></a><a jqte-styleval='255,0,0' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(255, 0, 0);' role='gridcell' unselectable='on'></a><a jqte-styleval='255,153,0' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(255, 153, 0);' role='gridcell' unselectable='on'></a><a jqte-styleval='255,255,0' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(255, 255, 0);' role='gridcell' unselectable='on'></a><a jqte-styleval='0,255,0' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(0, 255, 0);' role='gridcell' unselectable='on'></a><a jqte-styleval='0,255,255' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(0, 255, 255);' role='gridcell' unselectable='on'></a><a jqte-styleval='0,0,255' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(0, 0, 255);' role='gridcell' unselectable='on'></a><a jqte-styleval='153,0,255' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(153, 0, 255);' role='gridcell' unselectable='on'></a><a jqte-styleval='255,0,255' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(255, 0, 255);' role='gridcell' unselectable='on'></a><a jqte-styleval='204,0,0' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(204, 0, 0);' role='gridcell' unselectable='on'></a><a jqte-styleval='230,145,56' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(230, 145, 56);' role='gridcell' unselectable='on'></a><a jqte-styleval='241,194,50' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(241, 194, 50);' role='gridcell' unselectable='on'></a><a jqte-styleval='106,168,79' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(106, 168, 79);' role='gridcell' unselectable='on'></a><a jqte-styleval='69,129,142' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(69, 129, 142);' role='gridcell' unselectable='on'></a><a jqte-styleval='61,133,198' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(61, 133, 198);' role='gridcell' unselectable='on'></a><a jqte-styleval='103,78,167' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(103, 78, 167);' role='gridcell' unselectable='on'></a><a jqte-styleval='166,77,121' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(166, 77, 121);' role='gridcell' unselectable='on'></a><a jqte-styleval='153,0,0' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(153, 0, 0);' role='gridcell' unselectable='on'></a><a jqte-styleval='180,95,6' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(180, 95, 6);' role='gridcell' unselectable='on'></a><a jqte-styleval='191,144,0' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(191, 144, 0);' role='gridcell' unselectable='on'></a><a jqte-styleval='56,118,29' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(56, 118, 29);' role='gridcell' unselectable='on'></a><a jqte-styleval='19,79,92' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(19, 79, 92);' role='gridcell' unselectable='on'></a><a jqte-styleval='11,83,148' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(11, 83, 148);' role='gridcell' unselectable='on'></a><a jqte-styleval='53,28,117' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(53, 28, 117);' role='gridcell' unselectable='on'></a><a jqte-styleval='116,27,71' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(116, 27, 71);' role='gridcell' unselectable='on'></a><a jqte-styleval='102,0,0' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(102, 0, 0);' role='gridcell' unselectable='on'></a><a jqte-styleval='120,63,4' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(120, 63, 4);' role='gridcell' unselectable='on'></a><a jqte-styleval='127,96,0' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(127, 96, 0);' role='gridcell' unselectable='on'></a><a jqte-styleval='39,78,19' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(39, 78, 19);' role='gridcell' unselectable='on'></a><a jqte-styleval='12,52,61' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(12, 52, 61);' role='gridcell' unselectable='on'></a><a jqte-styleval='7,55,99' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(7, 55, 99);' role='gridcell' unselectable='on'></a><a jqte-styleval='32,18,77' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(32, 18, 77);' role='gridcell' unselectable='on'></a><a jqte-styleval='76,17,48' class='jqte_color' style='-webkit-user-select: none; background-color: rgb(76, 17, 48);' role='gridcell' unselectable='on'></a></div></div><div class='jqte_tool jqte_tool_3' role='button' data-tool='2' unselectable='on' style='-webkit-user-select: none;'><a class='jqte_tool_icon' unselectable='on' style='-webkit-user-select: none;'></a></div><div class='jqte_tool jqte_tool_4' role='button' data-tool='3' unselectable='on' style='-webkit-user-select: none;'><a class='jqte_tool_icon' unselectable='on' style='-webkit-user-select: none;'></a></div><div class='jqte_tool jqte_tool_5' role='button' data-tool='4' unselectable='on' style='-webkit-user-select: none;'><a class='jqte_tool_icon' unselectable='on' style='-webkit-user-select: none;'></a></div><div class='jqte_tool jqte_tool_6' role='button' data-tool='5' unselectable='on' style='-webkit-user-select: none;'><a class='jqte_tool_icon' unselectable='on' style='-webkit-user-select: none;'></a></div><div class='jqte_tool jqte_tool_7' role='button' data-tool='6' unselectable='on' style='-webkit-user-select: none;'><a class='jqte_tool_icon' unselectable='on' style='-webkit-user-select: none;'></a></div><div class='jqte_tool jqte_tool_8' role='button' data-tool='7' unselectable='on' style='-webkit-user-select: none;'><a class='jqte_tool_icon' unselectable='on' style='-webkit-user-select: none;'></a></div><div class='jqte_tool jqte_tool_9' role='button' data-tool='8' unselectable='on' style='-webkit-user-select: none;'><a class='jqte_tool_icon' unselectable='on' style='-webkit-user-select: none;'></a></div><div class='jqte_tool jqte_tool_10' role='button' data-tool='9' unselectable='on' style='-webkit-user-select: none;'><a class='jqte_tool_icon' unselectable='on' style='-webkit-user-select: none;'></a></div><div class='jqte_tool jqte_tool_11' role='button' data-tool='10' unselectable='on' style='-webkit-user-select: none;'><a class='jqte_tool_icon' unselectable='on' style='-webkit-user-select: none;'></a></div><div class='jqte_tool jqte_tool_12' role='button' data-tool='11' unselectable='on' style='-webkit-user-select: none;'><a class='jqte_tool_icon' unselectable='on' style='-webkit-user-select: none;'></a></div><div class='jqte_tool jqte_tool_13' role='button' data-tool='12' unselectable='on' style='-webkit-user-select: none;'><a class='jqte_tool_icon' unselectable='on' style='-webkit-user-select: none;'></a></div><div class='jqte_tool jqte_tool_14' role='button' data-tool='13' unselectable='on' style='-webkit-user-select: none;'><a class='jqte_tool_icon' unselectable='on' style='-webkit-user-select: none;'></a></div><div class='jqte_tool jqte_tool_15' role='button' data-tool='14' unselectable='on' style='-webkit-user-select: none;'><a class='jqte_tool_icon' unselectable='on' style='-webkit-user-select: none;'></a></div><div class='jqte_tool jqte_tool_16' role='button' data-tool='15' unselectable='on' style='-webkit-user-select: none;'><a class='jqte_tool_icon' unselectable='on' style='-webkit-user-select: none;'></a></div><div class='jqte_tool jqte_tool_17' role='button' data-tool='16' unselectable='on' style='-webkit-user-select: none;'><a class='jqte_tool_icon' unselectable='on' style='-webkit-user-select: none;'></a></div></div><div class='jqte_linkform' style='display:none' role='dialog'><div class='jqte_linktypeselect' unselectable='on' style='-webkit-user-select: none;'><div class='jqte_linktypeview' unselectable='on' style='-webkit-user-select: none;'><div class='jqte_linktypearrow' unselectable='on' style='-webkit-user-select: none;'></div><div class='jqte_linktypetext'>Web Address</div></div><div class='jqte_linktypes' role='menu' unselectable='on' style='-webkit-user-select: none;'><a jqte-linktype='0' unselectable='on' style='-webkit-user-select: none;'>Web Address</a><a jqte-linktype='1' unselectable='on' style='-webkit-user-select: none;'>E-mail Address</a><a jqte-linktype='2' unselectable='on' style='-webkit-user-select: none;'>Picture URL</a></div></div> <input class='jqte_linkinput' type='text/css' value=''> <div class='jqte_linkbutton' unselectable='on' style='-webkit-user-select: none;'>OK</div> <div style='height:1px;float:none;clear:both'></div></div><div class='jqte_editor' contenteditable='true'></div><div class='jqte_source jqte_hiddenField'></div></div>");
					sb.append("</div>");*/
				}
				else if(sShortName.equalsIgnoreCase("mlsel"))
				{
					Map<Integer,Boolean> mapOption=new HashMap<Integer, Boolean>();
					String sListValue="";
					if(Qans!=null && Qans.getSelectedOptions()!=null && !Qans.getSelectedOptions().equals(""))
					{
						sListValue=Qans.getSelectedOptions();
						String arrOp[]=sListValue.split(",");
						if(arrOp.length >0)
						{
							for(String sOpt :arrOp)
							{
								int iOpt=Utility.getIntValue(sOpt);
								mapOption.put(iOpt, true);
							}
						}
					}
					counter=0;
					checkedValue="";
					for (DistrictSpecificPortfolioOptions dqo : lstDistrictSpecificPortfolioOptions) 
					{
						//sb.append("<label class='checkbox inline' style='margin-top: 0px;margin-bottom: 2px;'>");
						if(!editableStatus){
							if(mapOption!=null && mapOption.get(dqo.getOptionId())!=null){
								if(counter==0){
									checkedValue=dqo.getQuestionOption();
									counter++;
								}else{
									checkedValue+=", "+dqo.getQuestionOption();
								}
								//sb.append("<input type='checkbox' "+enableDisableFields+" checked name='opt"+sOptionId+"' value='"+dqo.getOptionId()+"'>"+dqo.getQuestionOption()+"</br>");
							}
						}else{
							sb.append("<label class='checkbox inline' style='margin-top: 0px;margin-bottom: 2px;'>");
							if(mapOption!=null && mapOption.get(dqo.getOptionId())!=null)
								sb.append("<input type='checkbox' checked name='opt"+sOptionId+"' value='"+dqo.getOptionId()+"'>"+dqo.getQuestionOption()+"</br>");
							else
								sb.append("<input type='checkbox' name='opt"+sOptionId+"' value='"+dqo.getOptionId()+"'>"+dqo.getQuestionOption()+"</br>");
							sb.append("</label>");
						}
						//sb.append("</label>");
					}
					if(!editableStatus){
						if(checkedValue.equalsIgnoreCase(""))
							sb.append(lblNA);
						else
							sb.append(checkedValue);
					}
				}
				else if(sShortName.equalsIgnoreCase("mloet"))
				{
					Map<Integer,Boolean> mapOption=new HashMap<Integer, Boolean>();
					String sListValue="";
					if(Qans!=null && Qans.getSelectedOptions()!=null && !Qans.getSelectedOptions().equals(""))
					{
						sListValue=Qans.getSelectedOptions();
						String arrOp[]=sListValue.split(",");
						if(arrOp.length >0)
						{
							for(String sOpt :arrOp)
							{
								int iOpt=Utility.getIntValue(sOpt);
								mapOption.put(iOpt, true);
							}
						}
					}
					counter=0;
					checkedValue="";
					for (DistrictSpecificPortfolioOptions dqo : lstDistrictSpecificPortfolioOptions)
					{
						if(!editableStatus){
							if(mapOption!=null && mapOption.get(dqo.getOptionId())!=null){
								if(counter==0){
									checkedValue=dqo.getQuestionOption();
									counter++;
								}else{
									checkedValue+=", "+dqo.getQuestionOption();
								}
							}
								
						}else{
						sb.append("<label class='checkbox inline' style='margin-top: 0px;margin-bottom: 2px;'>");
						if(mapOption!=null && mapOption.get(dqo.getOptionId())!=null)
							sb.append("<input type='checkbox' checked name='opt"+sOptionId+"' value='"+dqo.getOptionId()+"'>"+dqo.getQuestionOption()+"</br>");
						else
							sb.append("<input type='checkbox' name='opt"+sOptionId+"' value='"+dqo.getOptionId()+"'>"+dqo.getQuestionOption()+"</br>");
						
						sb.append("</label>");
						}
					}
					if(!editableStatus){
						if(checkedValue.equalsIgnoreCase(""))
							sb.append(lblNA);
						else
							sb.append(checkedValue);
						
						sb.append("</br>"+sTextAReaValue);
					}else{
					
					sb.append("<div id='divOpt"+sOptionId+"'>");
					sb.append("&nbsp;&nbsp;<textarea  name='txtAreaOpt"+sOptionId+"' id='txtAreaOpt"+sOptionId+"' rows='8' style='width:100%;' class='form-control' maxlength='5000' onkeyup=\"return chkLenOfTextArea(this,5000)\" onblur=\"return chkLenOfTextArea(this,5000)\">"+sTextAReaValue+"</textarea><br/>");
					sb.append("</div>");
					}
				}
				else if(sShortName.equalsIgnoreCase("sloet"))
				{
					Map<Integer,Boolean> mapOption=new HashMap<Integer, Boolean>();
					String sListValue="";
					if(Qans!=null && Qans.getSelectedOptions()!=null && !Qans.getSelectedOptions().equals(""))
					{
						sListValue=Qans.getSelectedOptions();
						String arrOp[]=sListValue.split(",");
						if(arrOp.length >0)
						{
							for(String sOpt :arrOp)
							{
								int iOpt=Utility.getIntValue(sOpt);
								mapOption.put(iOpt, true);
							}
						}
					}
					counter=0;
					checkedValue="";
					for (DistrictSpecificPortfolioOptions dqo : lstDistrictSpecificPortfolioOptions)
					{
						if(!editableStatus){
							if(mapOption!=null && mapOption.get(dqo.getOptionId())!=null){
								if(counter==0){
									checkedValue=dqo.getQuestionOption();
									counter++;
								}else{
									checkedValue+=", "+dqo.getQuestionOption();
								}
							}
						}else{
							sb.append("<label class='radio' style='margin-top: 0px;margin-bottom: 2px;'>");
							if(mapOption!=null && mapOption.get(dqo.getOptionId())!=null)
								sb.append("<input checked type='radio' name='opt"+sOptionId+"' value='"+dqo.getOptionId()+"'>"+dqo.getQuestionOption()+"</br>");
							else
								sb.append("<input type='radio' name='opt"+sOptionId+"' value='"+dqo.getOptionId()+"'>"+dqo.getQuestionOption()+"</br>");
							sb.append("</label>");
						}
					}
					if(!editableStatus){
						if(checkedValue.equalsIgnoreCase(""))
							sb.append(lblNA);
						else
							sb.append(checkedValue);
						
						sb.append(sTextAReaValue);
					}else{
					sb.append("<div id='divOpt"+sOptionId+"'>");
					sb.append("&nbsp;&nbsp;<textarea  name='txtAreaOpt"+sOptionId+"' id='txtAreaOpt"+sOptionId+"' rows='8' style='width:100%;' class='form-control' maxlength='5000' onkeyup=\"return chkLenOfTextArea(this,5000)\" onblur=\"return chkLenOfTextArea(this,5000)\">"+sTextAReaValue+"</textarea><br/>");
					sb.append("</div>");
					}
				}
				else if(sShortName.equalsIgnoreCase("tf") || sShortName.equalsIgnoreCase("slsel"))
				{
					Map<Integer,Boolean> mapOption=new HashMap<Integer, Boolean>();
					String sListValue="";
					if(Qans!=null && Qans.getSelectedOptions()!=null && !Qans.getSelectedOptions().equals(""))
					{
						sListValue=Qans.getSelectedOptions();
						String arrOp[]=sListValue.split(",");
						if(arrOp.length >0)
						{
							for(String sOpt :arrOp)
							{
								int iOpt=Utility.getIntValue(sOpt);
								mapOption.put(iOpt, true);
							}
						}
					}
					checkedValue="";
					counter=0;
					for (DistrictSpecificPortfolioOptions dqo : lstDistrictSpecificPortfolioOptions)
					{
						if(!editableStatus){
							if(mapOption!=null && mapOption.get(dqo.getOptionId())!=null){
								if(counter==0){
									checkedValue=dqo.getQuestionOption();
									counter++;
								}else{
									checkedValue+=", "+dqo.getQuestionOption();
								}
							}
						}else{
							sb.append("<label class='radio' style='margin-top: 0px;margin-bottom: 2px;'>");
							if(mapOption!=null && mapOption.get(dqo.getOptionId())!=null)
								sb.append("<input type='radio' checked name='opt"+sOptionId+"' value='"+dqo.getOptionId()+"'>"+dqo.getQuestionOption()+"</br>");
							else
								sb.append("<input type='radio' name='opt"+sOptionId+"' value='"+dqo.getOptionId()+"'>"+dqo.getQuestionOption()+"</br>");
							sb.append("</label>");
						}
					}
					if(!editableStatus){
						if(checkedValue.equalsIgnoreCase(""))
							sb.append(lblNA);
						else
							sb.append(checkedValue);
					}
				}
				else if(sShortName.equalsIgnoreCase("drsls") || sShortName.equalsIgnoreCase("DD"))
				{
					if(lstDistrictSpecificPortfolioOptions!=null)
					{
						String sListValue="";
						if(Qans!=null && Qans.getSelectedOptions()!=null && !Qans.getSelectedOptions().equals(""))
							sListValue=Qans.getSelectedOptions();
						String answerValue="";
						if(editableStatus){
							sb.append("<select name='opt"+sOptionId+"' id='opt"+sOptionId+"' class='form-control' style='width: 325px;'>");
							sb.append("<option value='0'>Select "+dspq.getQuestion()+"</option>");
						}								
						for (DistrictSpecificPortfolioOptions dqo : lstDistrictSpecificPortfolioOptions)
						{
							if(!editableStatus){									
								if(sListValue!=null && !sListValue.equals("") && sListValue.equals(dqo.getOptionId()+"")){
									answerValue=dqo.getQuestionOption();
									sb.append(answerValue);
								}
								
							}else{
								if(sListValue!=null && !sListValue.equals("") && sListValue.equals(dqo.getOptionId()+""))
									sb.append("<option selected value='"+dqo.getOptionId()+"'>"+dqo.getQuestionOption()+"</option>");
								else
									sb.append("<option value='"+dqo.getOptionId()+"'>"+dqo.getQuestionOption()+"</option>");
								
							}
								
						}
						if(!editableStatus && answerValue.equalsIgnoreCase(""))								
							sb.append(lblNA);
						else
							sb.append("</select>");
						
					}
				}
				else if(sShortName.equalsIgnoreCase("OSONP"))
				{
					Map<Integer,Boolean> mapOption=new HashMap<Integer, Boolean>();
					String sListValue="";
					if(Qans!=null && Qans.getSelectedOptions()!=null && !Qans.getSelectedOptions().equals(""))
					{
						sListValue=Qans.getSelectedOptions();							
						String arrOp[]=sListValue.split("\\|");
						if(arrOp.length >0)
						{
							for(String sOpt :arrOp)
							{
								int iOpt=Utility.getIntValue(sOpt);
								mapOption.put(iOpt, true);
							}
						}							
					}
					
					for (DistrictSpecificPortfolioOptions dqo : lstDistrictSpecificPortfolioOptions)
					{
						sb.append("<label class='radio' style='margin-top: 0px;margin-bottom: 2px;'>");
						if(mapOption!=null && mapOption.get(dqo.getOptionId())!=null)
							sb.append("<input type='radio' checked name='opt"+sOptionId+"' value='"+dqo.getOptionId()+"'>"+dqo.getQuestionOption()+"</br>");
						else
							sb.append("<input type='radio' name='opt"+sOptionId+"' value='"+dqo.getOptionId()+"'>"+dqo.getQuestionOption()+"</br>");
						sb.append("</label>");
					}
				}
				else if(sShortName.equalsIgnoreCase("dt"))
				{
					String sDateValue="";
					if(Qans!=null && Qans.getSelectedOptions()!=null && !Qans.getSelectedOptions().equals("")){
						sDateValue=Qans.getSelectedOptions();
						Date date = null;		
						SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
						if(!editableStatus){
							if(sDateValue!=null){
						    try {
								date = df.parse(sDateValue);
							} catch (ParseException e) {
								e.printStackTrace();
							}
							sb.append(Utility.convertDateAndTimeToUSformatOnlyDate(date));
							}else
								sb.append(lblNA);
						}else{
							sb.append("<script type='text/javascript'>//<![CDATA["+
									"var cal = Calendar.setup({"+
									" onSelect: function(cal) { cal.hide() },"+
									" showTime: true});"+
									"cal.manageFields('opt"+sOptionId+"', 'opt"+sOptionId+"', '%m-%d-%Y');"+
									"//]]></script>");
							sb.append("<input type='text' value='"+sDateValue+"' id='opt"+sOptionId+"' name='opt"+sOptionId+"' class='form-control opt"+sOptionId+"' style='width: 450px;'> ");
						}
						
					}
					else
						sb.append(lblNA);
					//listDateCals.add("opt"+sOptionId);
				}
				else if(sShortName.equalsIgnoreCase("sl"))
				{
					String sStringValue="";
					if(Qans!=null && Qans.getSelectedOptions()!=null && !Qans.getSelectedOptions().equals(""))
						sStringValue=Qans.getSelectedOptions();
					if(!editableStatus){
						if(!sStringValue.equalsIgnoreCase(""))
							sb.append(sStringValue);							
						else
							sb.append(lblNA);
					}else{
						sb.append("<input type='text' value='"+sStringValue+"' id='opt"+sOptionId+"' name='opt"+sOptionId+"' class='form-control' style='width: 325px;'> ");
					}
				}
				sb.append("</div></div>");
			}
		}
		//sb.append("<button class='btn btn-primary' type='button' id='saveCustom' onclick='saveCustomQuestion();' style='display: inline-block;'>&nbsp;&nbsp;&nbsp;&nbsp;Save&nbsp;<i class='icon'></i>&nbsp;&nbsp;&nbsp;&nbsp;</button>");
	}
	
	return sb.toString();
}
@Transactional(readOnly=false)
public String saveProfileCustomFieldAnswer(DistrictSpecificPortfolioAnswers[] dspas,Integer teacherId)
{
	System.out.println("Calling SelfServiceCandidate => saveCustomFieldAnswer");	
	
	String sReturnValue="OK";
	TeacherDetail teacherDetail = new TeacherDetail();
	if(teacherId!=null && teacherId!=0){
		teacherDetail.setTeacherId(teacherId);
	}
	
	List<DistrictSpecificPortfolioQuestions> lstDistrictSpecificPortfolioQuestions=new ArrayList<DistrictSpecificPortfolioQuestions>();
	for (DistrictSpecificPortfolioAnswers dspa : dspas) 
		lstDistrictSpecificPortfolioQuestions.add(dspa.getDistrictSpecificPortfolioQuestions());

	Map<Integer, DistrictSpecificPortfolioAnswers> mapDSPQTemp=new HashMap<Integer, DistrictSpecificPortfolioAnswers>();
	List<DistrictSpecificPortfolioAnswers> lstDistrictSpecificPortfolioAnswers=districtSpecificPortfolioAnswersDAO.getDSPAByTID(teacherDetail, lstDistrictSpecificPortfolioQuestions);
	if(lstDistrictSpecificPortfolioAnswers!=null && lstDistrictSpecificPortfolioAnswers.size()>0)
	{
		for(DistrictSpecificPortfolioAnswers dspaTemp:lstDistrictSpecificPortfolioAnswers)
		{
			mapDSPQTemp.put(dspaTemp.getDistrictSpecificPortfolioQuestions().getQuestionId(), dspaTemp);
		}
	}
	DistrictMaster districtMaster=null;
	boolean bIsNew=true;
	for (DistrictSpecificPortfolioAnswers dspa : dspas) 
	{
		bIsNew=true;
		
		if(mapDSPQTemp!=null && mapDSPQTemp.get(dspa.getDistrictSpecificPortfolioQuestions().getQuestionId())!=null)
		{
			bIsNew=false;
			DistrictSpecificPortfolioAnswers answerIdTemp=mapDSPQTemp.get(dspa.getDistrictSpecificPortfolioQuestions().getQuestionId());
			if(dspa.getSelectedOptions()!=null && !dspa.getSelectedOptions().equals("") && dspa.getSelectedOptions().contains(","))
				answerIdTemp.setSelectedOptions(dspa.getSelectedOptions().replace(",", "|"));
			else if(dspa.getSelectedOptions()!=null && !dspa.getSelectedOptions().equals(""))
				answerIdTemp.setSelectedOptions(dspa.getSelectedOptions());
			else
				answerIdTemp.setSelectedOptions(null);
			
			answerIdTemp.setInsertedText(dspa.getInsertedText());
			answerIdTemp.setFileName(dspa.getFileName());
			
			if(answerIdTemp.getJobOrder()!=null && answerIdTemp.getJobOrder().getDistrictMaster()!=null)
				answerIdTemp.setDistrictMaster(answerIdTemp.getJobOrder().getDistrictMaster());
			else if (answerIdTemp.getDistrictMaster()!=null)
				answerIdTemp.setDistrictMaster(answerIdTemp.getDistrictMaster());
			try {
				districtSpecificPortfolioAnswersDAO.makePersistent(answerIdTemp);
			} catch (Exception e) {
				e.printStackTrace();
				sReturnValue="error";
			}
		}
		
		if(bIsNew)
		{
			if(districtMaster==null)
			{
				 JobOrder jobOrder= jobOrderDAO.findById(dspa.getJobOrder().getJobId(), false, false);
				 if(jobOrder!=null)
					 districtMaster=jobOrder.getDistrictMaster();
			}
			
			if(dspa.getSelectedOptions()!=null && !dspa.getSelectedOptions().equals("") && dspa.getSelectedOptions().contains(","))
				dspa.setSelectedOptions(dspa.getSelectedOptions().replace(",", "|"));
			else if(dspa.getSelectedOptions()!=null && !dspa.getSelectedOptions().equals(""))
				dspa.setSelectedOptions(dspa.getSelectedOptions());
			else
				dspa.setSelectedOptions(null);
			
			dspa.setAnswerId(null);
			dspa.setTeacherDetail(teacherDetail);
			dspa.setIsActive(true);
			dspa.setCreatedDateTime(new Date());
			dspa.setDistrictMaster(districtMaster);
			try {
				districtSpecificPortfolioAnswersDAO.makePersistent(dspa);
			} catch (Exception e) {
				e.printStackTrace();
				sReturnValue="error";
			}
		}
	}
	System.out.println("sReturnValue "+sReturnValue);
	return sReturnValue;
}
}