var outTeacherName=null;
var outerDoActivity=null;
var outerCgStatus=null;
var outerStatus=null;
var outerTeacherAssessmentStatusId=null;
var outerJobForTeacherId=null;
var outerTeacherId=null;
var totalOnlineActivityQuestionList=0;
var superAdminClickOnEditNote=0;//added by 11-05-2015
var clickCount=0;//added by 28-04-2015
var isClickCountOnEditForSuperUser=0;//added by 23-04-2015
var minOneScoreSliderSetFlag=2;//add variable For Jsi slider 
var minOneScoreSliderSetOrNotFlag=0;//add variable For Jsi slider 
var checkFirstClick=true;//added by 04-04-2015
var timingforDclWrdw='';
var reasonforDclWrdw='';
var dateFieldIds = {};
$(document).ready(function(){
	minOneScoreSliderSetOrNotFlag=0;
});
$("#email_candi").hide();

function showOuterStatus(teacherName,doActivity,cgStatus,status,teacherAssessmentStatusId,jobForTeacherId,teacherId)
{
	 outTeacherName=teacherName;
	 outerDoActivity=doActivity;
	 outerCgStatus=cgStatus;
	 outerStatus=status;
	 outerTeacherAssessmentStatusId=teacherAssessmentStatusId;
	 outerJobForTeacherId=jobForTeacherId;
	 outerTeacherId=teacherId;
	 document.getElementById("outer_status_l_title").innerHTML=""+resourceJSON.MsgStatusLifeCycleFor+""+teacherName;
	$('#outerStatusModal').modal('show');
}

function outerStatusClose(){
	$('#outerStatusModal').modal('hide');
	 document.getElementById("outer_status_l_title").innerHTML="";
	showStatus(outTeacherName,outerDoActivity,outerCgStatus,outerStatus,outerTeacherAssessmentStatusId,outerJobForTeacherId,outerTeacherId)
	 outTeacherName=null;
	 outerDoActivity=null;
	 outerCgStatus=null;
	 outerStatus=null;
	 outerTeacherAssessmentStatusId=null;
	 outerJobForTeacherId=null;
	 outerTeacherId=null;
}
function showStatus(teacherName,doActivity,cgStatus,status,teacherAssessmentStatusId,jobForTeacherId,teacherId,noOfRecord)
{
	document.getElementById("referenceValue").value = "";
	document.getElementById("status_l_title").innerHTML="<b>"+teacherName+"</b>&nbsp;|&nbsp;"+resourceJSON.msgCandidateLifecycle;
	$('#myModalStatus').modal('show');
	document.getElementById("statusTeacherId").value=teacherId;
	document.getElementById("jobForTeacherIdForSNote").value=jobForTeacherId;
	document.getElementById("doActivity").value=doActivity;
	
	displayStatusDashboard(0);
	finalizeOrNot(doActivity);
}
function finalizeOrNot(doActivity){
	if(doActivity=='true'){
		document.getElementById("statusSave").style.display="inline";
		document.getElementById("statusFinalize").style.display="inline";
	}else{
		document.getElementById("statusSave").style.display="none";
		document.getElementById("statusFinalize").style.display="none";
	}
	
	document.getElementById('email_da_sa').checked=false;
}
function finalizeOrNoForOfferReady(doActivity){
	document.getElementById("doActivityForOfferReady").value=doActivity;
	if(doActivity=='1'){
		document.getElementById("statusSave").style.display="inline";
		document.getElementById("statusFinalize").style.display="inline";
	}else{
		document.getElementById("statusSave").style.display="none";
		document.getElementById("statusFinalize").style.display="none";
	}
	document.getElementById('email_da_sa').checked=false;
}

function displayStatusDashboard(clickFlag){

	var jobId = document.getElementById("jobId").value;
	var teacherId = document.getElementById("statusTeacherId").value;
	$('#loadingDiv').show();
		ManageStatusAjax.displayStatusDashboardCG(teacherId,jobId,clickFlag,
		{ 
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{	$('#loadingDiv').hide();
				document.getElementById("divStatus").innerHTML=data.toString();
			}
		});
}
//----Gourav------
function displayStatusDashboardNew(clickFlag){	
	var jobId = document.getElementById("jobId").value;
	var teacherId = document.getElementById("statusTeacherId").value;
	$('#loadingDiv').show();
	ManageStatusAjax.displayStatusDashboardCGNew(teacherId,jobId,clickFlag,
	{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{	$('#loadingDiv').hide();
			document.getElementById("divStatus").innerHTML=data.toString();
			
		}
	});
}
///--------End-----------
function applyScrollOnStatusNote()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblStatusNote').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 100,
        width: 670,
        minWidth: null,
        minWidthAuto: false,
        colratio:[220,100,120,120,130], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });            
        });
}
function applyScrollOnOnlineActivity()
{
	var $j=jQuery.noConflict();
    $j(document).ready(function() {
    $j('#tblOnlineActivity').fixheadertable({ //table id 
    caption: '',
    showhide: false,
    theme: 'ui',
    height: 100,
    width: 670,
    minWidth: null,
    minWidthAuto: false,
    colratio:[200,50,70,170,120,80], // table header width
    addTitles: false,
    zebra: true,
    zebraClass: 'net-alternative-row',
    sortable: false,
    sortedColId: null,
    //sortType:[],
    dateFormat: 'd-m-y',
    pager: false,
    rowsPerPage: 10,
    resizeCol: false,
    minColWidth: 100,
    wrapper: false
    });            
    });
}
function applyScrollOnAssessmentInvite()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#assessmentInviteGridCG').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 400,
        width:670,
        minWidth: null,
        minWidthAuto: false,
        colratio:[380,170,120], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });            
        });
}
function openNoteDiv(userId){
	
	superAdminClickOnEditNote=0;
	$('#statusNotes').find(".jqte_editor").html("");
	var statusName = $('#statusName').val();
	ManageStatusAjax.statusWiseSectionList(userId,statusName,{
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{	if(data!=null){
				document.getElementById("templateDivStatus").style.display="block";
				$('#statuswisesection').html(data);
				var selectId = $("#statuswisesection option:selected").val();
				getsectionwiselist(selectId);
		}
		}
	});
	
	document.getElementById("noteMainDiv").style.display="inline";
	document.getElementById("showStatusNoteFile").innerHTML="";
	document.getElementById("showTemplateslist").style.display="none";
	removeStatusNoteFile();
	$('#errorStatusNote').hide();
}

/* ==== Gourav  : [Start  Js Method for the functinality of status wise email templates list]*/
function getsectionwiselist(value){
	document.getElementById("showTemplateslist").style.display="none";
	//document.getElementById("changeTemplateBody").style.display="none";
	$('#statusNotes').find(".jqte_editor").html("");
	if(value!=0){
		ManageStatusAjax.getSectionWiseTemaplteslist(value,{
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{
			
			document.getElementById("addTemplateslist").innerHTML=data;
			document.getElementById("showTemplateslist").style.display="block";
			}
		});
	}
}

function setTemplateByLIstId(templateId){
	$('#statusNotes').find(".jqte_editor").html("");
	if(templateId!=0){
		ManageStatusAjax.getTemplateByLIst(templateId,{
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{
			$('#statusNotes').find(".jqte_editor").html(data);
			}
		});
	}
}

function getTemplateByLIst(){
	var value=document.getElementById("templateId").value;
	if(value!=0){
		ManageStatusAjax.getTemplateByLIst(value,{
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{
			$('#statusNotes').find(".jqte_editor").html(data);
			}
		});
	}
}
/* ==== Gourav  : [Start  Js Method for the functinality of status wise email templates list end]*/

/* ==== Gagan  : [Start  Js Method for the functinality of status wise email templates ]*/

function getStatusWiseEmailForAdmin()
{
	//alert(" getStatusWiseEmailForAdmin ------------------------------");
	//alert("Hello");
	try{  
		$('#myModalEmailTeacher').modal('hide');
	}catch(e){}
	var statusName	=	$("#statusName").val();
	//alert(" statusName "+statusName);
	var statusShortName =	$("#statusShortName").val();
	var jftIdForSNote =	$("#jobForTeacherIdForSNote").val();
	var statusIdForStatusNote =	$("#statusIdForStatusNote").val();
	$('#loadingDiv').show();
	ManageStatusAjax.getStatusWiseEmailForAdmin(statusName,statusShortName,jftIdForSNote,
	{
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
		    $('#loadingDiv').hide();
			if(data!=null)
			{				
				$('#subjectLine').val(data.subjectLine);
				$('#mailBody').find(".jqte_editor").html(data.templateBody);
				try{  
					$('#jWTeacherStatusNotesDiv').modal('hide');
					document.getElementById("noteMainDiv").style.display="none";
				}catch(e){}
				
				try{  
					$('#myModalEmail').modal('show');
				}catch(e){}
			}
			else
			{
				document.getElementById("email_da_sa").disabled=true;
			}
		}
	});
}

function getFinalizeSpecificTemplateList(templateId,notifyUser)
{
	//alert("test=="+templateId);
	var statusName	=	$("#statusName").val();
	var jftIdForSNote =	$("#jobForTeacherIdForSNote").val();
	if(templateId==null)
		templateId="";
	//alert("ttttt @@@@@");//shriram
	//$('#loadingDiv').show();
	ManageStatusAjax.getFinalizeSpecificTemplateList(statusName,jftIdForSNote,templateId,notifyUser,
	{
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			//Shadab
		//alert("@@@@##########################");
		//alert(data);
			if(notifyUser=='0')	//Only For Teacher
			{
				if(data!=null && data.length>0)
				{
					$("#finelizeSpecificDivForDaSa").hide();
					$("#finelizeSpecificDiv").show();
					$("#finelizeSpecificTemplatesId").val(data[0].finalizeSpecificTemplatesId);
					$("#finelizeSpecificTemplatesName").empty();
					$('#finelizeSpecificTemplatesName').append("<option value=''>None</option>");
					for(var i=0;i<data.length;i++)
					{
						
						if(templateId!="" && data[i].status=='Selected')
						{
							$('#finelizeSpecificTemplatesName').append("<option value='"+data[i].finalizeSpecificTemplatesId+"' selected >"+data[i].templateName+"</option>")
							$('#finelizeSpecificSubjectLine').val(data[i].subjectLine);
							$('#finelizeSpecificMessage').find(".jqte_editor").html(data[i].templateBody);
							$("#finelizeSpecificTemplatesId").val(data[i].finalizeSpecificTemplatesId);
						}
						else
						{
							$('#finelizeSpecificTemplatesName').append("<option value='"+data[i].finalizeSpecificTemplatesId+"' >"+data[i].templateName+"</option>")
						}
					}
					
					if(templateId=="")
					{
						$('#finelizeSpecificSubjectLine').val("");
						$('#finelizeSpecificMessage').find(".jqte_editor").html("");
					}
				}
				else
				{
					$("#finelizeSpecificTemplatesId").val("");
					$("#finelizeSpecificTemplatesName").empty();
					$('#finelizeSpecificSubjectLine').val("");
					$('#finelizeSpecificMessage').find(".jqte_editor").html("");
					$("#finelizeSpecificDiv").hide();
				}
			}
			else
			if(notifyUser=='1')	//Only For DA/SA
			{
				if(data!=null && data.length>0)
				{
					$("#finelizeSpecificDiv").hide();
					$("#finelizeSpecificDivForDaSa").show();
					
					$("#finelizeSpecificTemplatesIdForDaSa").val(data[0].finalizeSpecificTemplatesId);
					$("#finelizeSpecificTemplatesNameForDaSa").empty();
					
					$('#finelizeSpecificTemplatesNameForDaSa').append("<option value=''>Select Template</option>");
					for(var i=0;i<data.length;i++)
					{
						
						if(templateId!="" && data[i].status=='Selected')
						{
							$('#finelizeSpecificTemplatesNameForDaSa').append("<option value='"+data[i].finalizeSpecificTemplatesId+"' selected >"+data[i].templateName+"</option>")
							$('#finelizeSpecificSubjectLineForDaSa').val(data[i].subjectLine);
							$('#finelizeSpecificMessageForDaSa').find(".jqte_editor").html(data[i].templateBody);
							$("#finelizeSpecificTemplatesIdForDaSa").val(data[i].finalizeSpecificTemplatesId);
							
							//Copy Content to Other textbox and textarea
							$('#subjectLine').val(data[i].subjectLine);
							$('#mailBody').find(".jqte_editor").html(data[i].templateBody);
							$('#isEmailTemplateChanged').val(1);
						}
						else
						{
							$('#finelizeSpecificTemplatesNameForDaSa').append("<option value='"+data[i].finalizeSpecificTemplatesId+"' >"+data[i].templateName+"</option>")
						}
					}
					
					if(templateId=="")
					{
						$('#finelizeSpecificSubjectLineForDaSa').val("");
						$('#finelizeSpecificMessageForDaSa').find(".jqte_editor").html("");
						$('#isEmailTemplateChanged').val(0);
					}
				}
				else
				{
					$("#finelizeSpecificTemplatesIdForDaSa").val("");
					$("#finelizeSpecificTemplatesNameForDaSa").empty();
					$("#finelizeSpecificDivForDaSa").hide();
				}
			}
		}
	});	
}

function sendFinelizeSpecificMailToTeacher()
{
	var finelizeSpecificTemplatesId = "";
	var statusName	=	$("#statusName").val();
	var jftIdForSNote =	$("#jobForTeacherIdForSNote").val();
	var finelizeSpecificSubjectLine = "";
	var finelizeSpecificMessage="";
	var finelizeSpecificTemplatesName = "";
	
	try{finelizeSpecificTemplatesId = $("#finelizeSpecificTemplatesId").val();}catch(err){}
	try{finelizeSpecificSubjectLine = $('#finelizeSpecificSubjectLine').val();}catch(err){}
	try{finelizeSpecificMessage = $('#finelizeSpecificMessage').find(".jqte_editor").html();}catch(err){}
	try{finelizeSpecificTemplatesName = $("#finelizeSpecificTemplatesName option:selected").val();}catch(err){}
	
	if(finelizeSpecificTemplatesId!=null && finelizeSpecificTemplatesId!="" && statusName!=null && statusName!="" && jftIdForSNote!=null && jftIdForSNote!="" && finelizeSpecificSubjectLine!=null && finelizeSpecificSubjectLine!="" && finelizeSpecificMessage!=null && finelizeSpecificMessage!="" && finelizeSpecificTemplatesName!=null && finelizeSpecificTemplatesName!="")
	{
		//$('#loadingDiv').show();
		ManageStatusAjax.sendFinelizeSpecificMailToTeacher(statusName,jftIdForSNote,finelizeSpecificSubjectLine,finelizeSpecificMessage,
		{
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{
		    	//$('#loadingDiv').hide();
		    			    
		    	$("#finelizeSpecificTemplatesId").val("");
		    	$("#finelizeSpecificTemplatesName").empty();
    			$('#finelizeSpecificSubjectLine').val("");
    			$('#finelizeSpecificMessage').find(".jqte_editor").html("");
    			$("#finelizeSpecificDiv").hide();
			}
		});
	}
}

function validateFinelizeSpecificTemplates()
{
	var finelizeSpecificTemplatesName="";
	try{finelizeSpecificTemplatesName = $("#finelizeSpecificTemplatesName option:selected").val();}catch(err){}
	
	if(finelizeSpecificTemplatesName=="")
		return true;
	
	var finelizeSpecificTemplatesId="";
	
	try{ finelizeSpecificTemplatesId = $("#finelizeSpecificTemplatesId").val(); }
	catch(err){}
	
	if(finelizeSpecificTemplatesId!="")
	{
		var msgSubject=$('#finelizeSpecificSubjectLine').val();
		var charCount	=	$('#finelizeSpecificMessage').find(".jqte_editor").text();
		
		$('#finelizeSpecificErrorDiv').empty();
		$('#finelizeSpecificSubjectLine').css("background-color", "");
		$('#finelizeSpecificMessage').find(".jqte_editor").css("background-color", "");
		
		var cnt=0;
		var focs=0;
		if(trim(msgSubject)=="")
		{
			$('#finelizeSpecificErrorDiv').append("&#149; "+resourceJSON.PlzEtrSub+"<br>");
			if(focs==0)
				$('#finelizeSpecificSubjectLine').focus();
			$('#finelizeSpecificSubjectLine').css("background-color", "#F5E7E1");
			cnt++;focs++;
		}
		if(charCount.length==0)
		{
			$('#finelizeSpecificErrorDiv').append("&#149; "+resourceJSON.PlzEtrMsg+"<br>");
			if(focs==0)
				$('#finelizeSpecificMessage').find(".jqte_editor").focus();
			$('#finelizeSpecificMessage').find(".jqte_editor").css("background-color", "#F5E7E1");
			cnt++;focs++;
		}
		
		if(cnt==0)
		{
			$('#finelizeSpecificErrorDiv').hide();
			return true;
		}
		else
		{
			$('#finelizeSpecificErrorDiv').show();
			return false;
		}
	}
	
	return true;
}

/* ==== Gagan  : [Start  Js Method for the functinality of status wise email templates ]*/

function getStatusWiseEmailForTeacher()
{
	//alert(" getStatusWiseEmailForTeacher() ");
	try{  
		$('#myModalEmail').modal('hide');
	}catch(e){}
	var statusName	=	$("#statusName").val();
	var statusShortName =	$("#statusShortName").val();
	var jftIdForSNote =	$("#jobForTeacherIdForSNote").val();
	var statusIdForStatusNote =	$("#statusIdForStatusNote").val();
	$('#loadingDiv').show();
	ManageStatusAjax.getStatusWiseEmailForTeacher(statusName,statusShortName,jftIdForSNote,
	{
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
		    $('#loadingDiv').hide();
			if(data!=null)
			{
				$('#subjectLineTeacher').val(data.subjectLine);
				$('#mailBodyTeacher').find(".jqte_editor").html(data.templateBody);
				try{  
					$('#jWTeacherStatusNotesDiv').modal('hide');
					document.getElementById("noteMainDiv").style.display="none";
				}catch(e){}
				try{  
					$('#myModalEmailTeacher').modal('show');
				}catch(e){}
				
			}
		}
	});
}
function sendChangedEmail()
{
	$('#isEmailTemplateChanged').val(1);
	var msgSubject=$('#subjectLine').val();
	var charCount	=	$('#mailBody').find(".jqte_editor").text();
			
	$('#errordivEmail').empty();
	$('#subjectLine').css("background-color", "");
	$('#mailBody').find(".jqte_editor").css("background-color", "");
	
	var cnt=0;
	var focs=0;
	if(trim(msgSubject)=="")
	{
		$('#errordivEmail').append("&#149; "+resourceJSON.PlzEtrSub+"<br>");
		if(focs==0)
			$('#subjectLine').focus();
		$('#subjectLine').css("background-color", "#F5E7E1");
		cnt++;focs++;
	}
	if(charCount.length==0)
	{
		$('#errordivEmail').append("&#149; "+resourceJSON.PlzEtrMsg+"<br>");
		if(focs==0)
			$('#mailBody').find(".jqte_editor").focus();
		$('#mailBody').find(".jqte_editor").css("background-color", "#F5E7E1");
		cnt++;focs++;
	}
	
	if(cnt==0)
	{
		emailClose();
		return false;
	
	}
	else
	{
		$('#errordivEmail').show();
		return false;
	}
}

function sendChangedEmailTeacher()
{
	
	$('#isEmailTemplateChangedTeacher').val(1);
	var msgSubjectTeacher=$('#subjectLineTeacher').val();
	var charCount	=	$('#mailBodyTeacher').find(".jqte_editor").text();
			
	$('#errordivEmailTeacher').empty();
	$('#subjectLineTeacher').css("background-color", "");
	$('#mailBodyTeacher').find(".jqte_editor").css("background-color", "");
	
	var cnt=0;
	var focs=0;
	if(trim(msgSubjectTeacher)=="")
	{
		$('#errordivEmailTeacher').append("&#149; "+resourceJSON.PlzEtrSub+"<br>");
		if(focs==0)
			$('#subjectLineTeacher').focus();
		$('#subjectLineTeacher').css("background-color", "#F5E7E1");
		cnt++;focs++;
	}
	if(charCount.length==0)
	{
		$('#errordivEmailTeacher').append("&#149; "+resourceJSON.PlzEtrMsg+"<br>");
		if(focs==0)
			$('#mailBodyTeacherTeacher').find(".jqte_editor").focus();
		$('#mailBodyTeacher').find(".jqte_editor").css("background-color", "#F5E7E1");
		cnt++;focs++;
	}
	
	if(cnt==0)
	{
		emailClose();
		return false;
	
	}
	else
	{
		$('#errordivEmailTeacher').show();
		return false;
	}
}


function requisitionNumbers()
{
	var jobId=document.getElementById("jobId").value;
	var schoolId=document.getElementById("schoolIdCG").value;
	var reqNumber=document.getElementById("reqNumber").value;
	var offerReady=0;
	try{
		offerReady=document.getElementById("offerReady").value;
	}catch(err){}
	var teacherId=document.getElementById("statusTeacherId").value;
	ManageStatusAjax.requisitionNumbers(jobId,schoolId,offerReady,reqNumber,teacherId,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
			try{
				if(data!=""){
					document.getElementById("requisitionNumbers").innerHTML=data;
					if($("#statusName").val()!="Asst Supe Interview" && ($('#isRequiredSchoolIdCheck').val()!=1 && $('#isSchoolOnJobId').val()!=1)) //added yes
					{
						document.getElementById("requisitionNumbersGrid").style.display="inline";
						try{
							document.getElementById("requisitionNumber").style.display="inline";
						}catch(err){}
					}
				}else{
					document.getElementById("requisitionNumbers").innerHTML="";
					document.getElementById("requisitionNumbersGrid").style.display="none";
				}				
			}catch(err){}
		}
	});
}

function setStatusTilteFor2ndDiv(teacherDetails,statusName,statusShortName){
//	alert("1111111"+statusName);
	//shriram
	try{
	if(statusName=="Recommend for Hire")
	{
		
	$("#email_candi").show();
	
	 $('#email_to_candi').prop("checked",false);
	
	}
	else
		$("#email_candi").hide();
	}catch(e){}
	document.getElementById("status_title").innerHTML=statusName +" Evaluation for "+teacherDetails;
	$("#statusName").val(statusName);
	$("#statusShortName").val(statusShortName);
	$('#hiredDateDiv').hide();
	//document.getElementById("setHiredDate").value = "";
	if(statusName == "Hired" || statusName == resourceJSON.msgHired2){
		$('#hiredDateDiv').show();
	}
	if(statusName=="SP Status" || statusName==resourceJSON.lblSPStatus){
		$('#spSelectDiv').show();	
	}else{
		$('#spSelectDiv').hide();
	}
	try{document.getElementById("offerReady").value=""}catch(e){}
}
/* ==== Gagan  : [END  Js Method for the functinality of status wise email templates ]*/

function checkOfferAccepted(offerAccepted){
	document.getElementById("offerAccepted").value=offerAccepted;
}
function checkReqNumber(schoolId,offerReady,reqNumber){
	document.getElementById("schoolIdCG").value=schoolId;
	document.getElementById("offerReady").value=offerReady;
	document.getElementById("reqNumber").value=reqNumber;
}

function jWTeacherStatusNotesOnOpen(fitScore,statusId,secondaryStatusId)
{
	//alert("666");//shriram
	var sUrl = window.location.pathname;
	
	dateFieldIds = new Object();
	getFinalizeSpecificTemplateList(null,0);	//get FinalizeSpecificTemplateList for Teacher added by Shadab Ansari
	getFinalizeSpecificTemplateList(null,1);	//get FinalizeSpecificTemplateList for DA/SA added by Shadab Ansari
	
	checkFirstClick=true;//added by 03-04-2015
	document.getElementById("undo").style.display="none";
	try{ document.getElementById("txtNewReqNo").value=""; }catch(err){}
	try{ $("#rdReqPre").prop("checked", true); }catch(err){}
	
	var offerReady="";
	try{
		offerReady=document.getElementById("offerReady").value;
	}catch(err){}
	if(offerReady==""){
		try{
			document.getElementById("schoolIdCG").value=0;
		}catch(err){}
		try{
			document.getElementById("schoolNameCG").value="";
		}catch(err){}
	}
	var isReqNoForHiring=0;
	try{
		isReqNoForHiring=document.getElementById("isReqNoForHiring").value;
	}catch(err){}
	if(isReqNoForHiring==0){
		try{
			document.getElementById("reqstar").style.display="none";
		}catch(err){}
	}else{
		try{
			document.getElementById("reqstar").style.display="inline";
		}catch(err){}
	}
	
	
	var jobForTeacherId=document.getElementById("jobForTeacherIdForSNote").value;
	currentPageFlag="statusNote";
	document.getElementById("statusIdForStatusNote").value=statusId;
	document.getElementById("fitScoreForStatusNote").value=fitScore;
	document.getElementById("secondaryStatusIdForStatusNote").value=secondaryStatusId;
	var teacherId = document.getElementById("statusTeacherId").value;
	var jobId = document.getElementById("jobId").value;
	$('#myModalStatus').modal('hide');
	$('#errorStatusNote').hide();
	$('#loadingDiv').css('z-index','999999');
	$('#loadingDiv').show();
	var doNotShowPanelinst=0;
	var offerReq=0;
	$('#errorStatusNoteRef').empty();
	try{
		if(document.getElementById("panelOpenFlag"))
			doNotShowPanelinst = 1;
	}catch(e){}
	
	if(sUrl.indexOf("candidategridtest") > -1){
		jWTeacherStatusNotesOnOpenOp(fitScore,statusId,secondaryStatusId)
	
	}else{   //shriram in case of stripe this method call
		//alert(jobForTeacherId+" "+fitScore +" "+teacherId +" "+ jobId+" "+statusId +" "+ secondaryStatusId+" "+ pageSNote +" "+noOfRowsSNote +" "+  sortOrderStrSNote +" "+ sortOrderTypeSNote +" "+ doNotShowPanelinst );
	ManageStatusAjax.getStatusNote(jobForTeacherId,fitScore,teacherId,jobId,statusId,secondaryStatusId,pageSNote,noOfRowsSNote,sortOrderStrSNote,sortOrderTypeSNote,doNotShowPanelinst,
	{
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			document.getElementById("jobCategoryDiv").value=2;
			$('#loadingDiv').hide();			
			if(data==1)
				document.getElementById("divStatusNoteGrid").innerHTML=""+resourceJSON.MsgNotStartedOrNotCompleted+"";
			else if(data==2)
				document.getElementById("divStatusNoteGrid").innerHTML=""+resourceJSON.MsgTimeOutTheJSI+"";
			else
				document.getElementById("divStatusNoteGrid").innerHTML=data.split("##")[0];
			
			var disFlag=0;
			if(document.getElementById("panelOpenFlag"))
			{
				
			}
			else
			$('#jWTeacherStatusNotesDiv').modal('show');
			document.getElementById("checkOptions").style.display="inline";
			finalizeOrNot(document.getElementById("doActivity").value);
			var statusName = document.getElementById("statusName").value;
			if(statusName == "Withdrew" || statusName == "Declined"){
				$("#timeAndReason").show();
				var ht=data.split("##")[11];
				var ht1=data.split("##")[12];
				if(data.split("##")[11]!=null || data.split("##")[11]!='0'){
					$("#timingforDclWrdw").val(ht);
				}
				if(data.split("##")[12]!=null || data.split("##")[12]!='0'){
					$("#reasonforDclWrdw").val(ht1);
				}
			}
			
			try{
				if(statusName == resourceJSON.msgHired2){
					document.getElementById("hiredtext").innerHTML=resourceJSON.msgHired2+" Date";
				}else if(statusName == "Hired"){
					document.getElementById("hiredtext").innerHTML="Hired Date";
				}
			}catch(e){}
			try{
				if(data.split("##")[10]==''){
					if(statusName == resourceJSON.lblLinkToKsn || statusName == resourceJSON.lblINVWF1 || statusName == resourceJSON.lblWF1COM  || statusName == resourceJSON.lblINVWF2  || statusName == resourceJSON.lblWF2COM  || statusName == resourceJSON.lblINVWF3   || statusName == resourceJSON.lblWF3COM || statusName == resourceJSON.lblScreening){
						finalizeOrNot(false);
					}
				}
			}catch(e){}
			
			try{
				var entityType=$('[name="entityType"]').val();
				
				if(statusName==resourceJSON.lblSPStatus && (entityType==5 || entityType==6)){
					document.getElementById("statusAddNote").style.display="none";
					document.getElementById("expNotePdf").style.display="none";
					document.getElementById("spSelectDiv").style.display="inline";
					document.getElementById("undo").style.display="inline";
				}else{
					document.getElementById("spSelectDiv").style.display="none";
				}
				
				if((statusName==resourceJSON.lblSPStatus || statusName==resourceJSON.lblPrescreen) && (entityType==5 || entityType==6)){
					if(data.split("##")[10]!=null && data.split("##")[10]=='waived'){
						document.getElementById("waived").style.display="inline";
					}else{
						document.getElementById("waived").style.display="none";
					}
				}else{
					document.getElementById("waived").style.display="none";
				}
			}catch(err){}
			try{
				if(statusName=='Offer Ready'){
					if(data.split("##")[7]!=null && data.split("##")[7]=='resend'){
						document.getElementById("resend").style.display="inline";
					}else{
						document.getElementById("resend").style.display="none";
					}
					var offerShowFlag="0";
					offerShowFlag=document.getElementById("doActivityForOfferReady").value;
					if(offerShowFlag=="0"){
						finalizeOrNoForOfferReady(offerShowFlag);
					}
				}else{
					document.getElementById("resend").style.display="none";
				}
			}catch(err){}
			try{
				var entityType=$('[name="entityType"]').val();
				if(statusName=='Credential Review' && entityType==3){
					var offerShowFlag="0";
					offerShowFlag=document.getElementById("doActivityForOfferReady").value;
					if(offerShowFlag=="0" || offerShowFlag==""){
						finalizeOrNoForOfferReady(offerShowFlag);
					}
				}
			}catch(err){}
			
			if(data.split("##")[1]!='null' && data.split("##")[6]!='JSI' && statusName!='Offer Ready'){				
				if(document.getElementById("statusSave").style.display=='inline'){
					if(data.split("##")[1]=='H'){
						disFlag=1;
						document.getElementById("undo").style.display="none";
						document.getElementById("unRemove").style.display="none";
						document.getElementById("unDecline").style.display="none";
						document.getElementById("unHire").style.display="inline";
						document.getElementById("unWithdraw").style.display="none";
					}else if(data.split("##")[1]=='D'){
						document.getElementById("undo").style.display="none";
						document.getElementById("unRemove").style.display="none";
						document.getElementById("unDecline").style.display="inline";
						document.getElementById("unHire").style.display="none";
						document.getElementById("unWithdraw").style.display="none";
					}else if(data.split("##")[1]=='R'){
						document.getElementById("undo").style.display="none";
						document.getElementById("unRemove").style.display="inline";
						document.getElementById("unDecline").style.display="none";
						document.getElementById("unHire").style.display="none";
						document.getElementById("unWithdraw").style.display="none";
					}else if(data.split("##")[1]=='S'){
						document.getElementById("undo").style.display="inline";
						document.getElementById("unRemove").style.display="none";
						document.getElementById("unDecline").style.display="none";
						document.getElementById("unHire").style.display="none";
						document.getElementById("unWithdraw").style.display="none";
					}else if(data.split("##")[1]=='W'){
						document.getElementById("undo").style.display="none";
						document.getElementById("unRemove").style.display="none";
						document.getElementById("unDecline").style.display="none";
						document.getElementById("unHire").style.display="none";
						document.getElementById("unWithdraw").style.display="inline";
					}
				}
			}else{				
				if(data.split("##")[6]=='JSI'){
					document.getElementById("statusFinalize").style.display="none";
					document.getElementById("statusSave").style.display="none";
					document.getElementById("checkOptions").style.display="none";
				}
				if(statusName=='Offer Ready'){
					requisitionNumbers();
					offerReq=1;
				}
				
				if(data.split("##")[1]=='S'){
					document.getElementById("undo").style.display="inline";
				}else{
					document.getElementById("undo").style.display="none";
				}
				document.getElementById("unRemove").style.display="none";
				document.getElementById("unDecline").style.display="none";
				document.getElementById("unHire").style.display="none";
				document.getElementById("unWithdraw").style.display="none";
				
			}			
			var checkOnlineActivity="0";
			try{
				checkOnlineActivity=document.getElementById("divOnlineActivityForCG").innerHTML;
			}catch(err){}
			if(checkOnlineActivity==""){
				displayOnlineActivityForCG();
			}
			scroeGrid();
			applyScrollOnStatusNote();
			applyScrollOnOnlineActivity();
			applyScrollOnTblTrans();
			applyScrollOnTblTransAcademic();
			applyScrollOnTblReferenceCheck();
			var entityType=$('[name="entityType"]').val();
			var hiredFlag=0;			
			if(document.getElementById("statusIdForStatusNote").value!=null){
				if(document.getElementById("statusIdForStatusNote").value==6){
					hiredFlag=1;
				}
			}			
			try{
				if(hiredFlag==0 && offerReq==0){
					document.getElementById("requisitionNumbers").innerHTML="";
					document.getElementById("requisitionNumbersGrid").style.display="none";
				}
			}catch(err){}
			
			document.getElementById("hireFlag").value=disFlag;
			var txtschoolCount=0;
			try { 
					txtschoolCount=document.getElementById("txtschoolCount").value;
				} catch (e) {}
				
			if(txtschoolCount > 1 && disFlag==0){
				if(hiredFlag==1)
				requisitionNumbers();
				document.getElementById("schoolAutoSuggestDivId").style.display="block";
				try{
					document.getElementById("requisitionNumber").style.display="block";
				}catch(err){}
			}else{
				if(disFlag==0){
					if(hiredFlag==1)
					requisitionNumbers();
				}else{
					if(offerReq==0){
						try{
							document.getElementById("requisitionNumbersGrid").style.display="none";
						}catch(err){}
						try{
							document.getElementById("requisitionNumber").style.display="none";
						}catch(err){}
					}
				}
				// changes display block
				if($("#statusName").val()=="Asst Supe Interview" || ($('#isRequiredSchoolIdCheck').val()==1 && $('#isSchoolOnJobId').val()==1))
				{					
					document.getElementById("schoolAutoSuggestDivId").style.display="block";
					$('#schoolNameCG').prop('disabled', false);
					if($('#alreadyExistsSchoolInStatus').val()==1){
						if(entityType== 2)
						{		
							document.getElementById("schoolNameCG").value = $('#schoolNameValue').val().replace(/!!/g , " ");
							document.getElementById("schoolIdCG").value = $('#schoolIdValue').val();
							$('#schoolNameCG').prop('disabled', true);
						}
					}
				}
				else
				{
					document.getElementById("schoolAutoSuggestDivId").style.display="none";
				}
				//Only for Jeffco added Date 10-09-2015
				try{
					try{
						$('#startDateDiv').remove();
					}catch(e){}
					if($("[name='slcStartDateOnOrOff']").val()=="ON" && $("#districtId").val()=='804800' && $("#statusName").val()=='Offer Made'){
						$('#schoolAutoSuggestDivId').after('<div class="row mt5" id="startDateDiv"><div class="span12 top5 col-md-2 form-group" style="width:100%;">'+
								'<label for="text" style="float:left;margin-right:5px;padding-top:7px;">Start Date<span class="required">*</span></label>'+
								'<div><input type="text" value="" id="startDateId" name="startDate" class="form-control" maxlength="50" onmouseup="setDateField(this);" onkeydown="return keyNotWork(event);" oncontextmenu="return false;" style="width:150px;"></div>'+
								'</div></div>');
						$('[name="startDate"]').val($('[name="isStartDate"]').val());
					}
					}catch(e){}
				//ENDed Only for Jeffco added Date 10-09-2015
			}			
			var txtOverride=0;
			try { 
				txtOverride=document.getElementById("txtOverride").value;
				} catch (e) {}
			
				try{
					if(data.split("##")[2]!=null && data.split("##")[2]!='null'){
						try{
							// changes display block
							document.getElementById("schoolAutoSuggestDivId").style.display="none";
						}catch(err){}
						/*try{
							document.getElementById("requisitionNumbersGrid").style.display="none";
						}catch(err){}
						try{
							document.getElementById("requisitionNumber").style.display="none";
						}catch(err){}*/
					}
				}catch(err){}
					
							
			if(txtOverride==1)
			{
				if(document.getElementById("panelOpenFlag"))
				{
				}
				else
				{
					if(offerReady=="")
					{
						$('#spanOverride').modal('show');
					}
					
				}
			}
			else
			{
					$('#spanOverride').modal('hide');
			
			}			
			if(document.getElementById("panelOpenFlag"))
			{			
				$('#checkOptions').hide();
				$('#statusSave').hide();
			}
			try{
				if(data.split("##")[4]!=null && data.split("##")[4]!='null'){
					try{
						document.getElementById("jobCategoryFlag").value=data.split("##")[4];
					}catch(err){}
				}
			}catch(err){}
			
			$('textarea').jqte();
			getQuestionNotesIds();
			
			//mukesh
			//alert(data.split("##")[3]);
			if(data.split("##")[3]=='true')
			{
			   document.getElementById('email_da_sa').checked=true;
			}
			
			try{
				if(data.split("##")[5]!='' && data.split("##")[5]!='null'){
					var date = new Date(data.split("##")[5]);
					document.getElementById("setHiredDate").value=(date.getMonth() + 1) + '-' + date.getDate() + '-' +  date.getFullYear();
				} else {
					try{
							var date = new Date();
							document.getElementById("setHiredDate").value=(date.getMonth() + 1) + '-' + date.getDate() + '-' +  date.getFullYear();
					} catch (err){}
				}
			}catch(err){}
			
			try{
			if($("#statusName").val()!=="Asst Supe Interview")
			{				
				document.getElementById("strengthOppDiv").style.display="none";
				document.getElementById("recomDiv").style.display="none";				
				document.getElementById("remQuesDiv").style.display="none";				
			}
			}catch(e){}
			
			try{
			if($("#statusName").val()=="Asst Supe Interview")
			{
				try
				{
					document.getElementById('candidateDetails').style.display="block";
					document.getElementById("strengthOppDiv").style.display="block";
					document.getElementById("strengthOppDiv").innerHTML = data.split("##")[8];
					
					
					if(data.split("##")[9].length > 0)
					{	
						if(entityType.value == 2)
						{
							document.getElementById("schoolAutoSuggestDivId").style.display="block";
							document.getElementById("schoolNameCG").value = data.split("##")[9].split("@@@")[0];
							document.getElementById("schoolIdCG").value = data.split("##")[9].split("@@@")[1];
							$('#schoolNameCG').prop('disabled', 'disabled');
						}
						else
						{
							document.getElementById("schoolAutoSuggestDivId").style.display="none";
						}
					}

				}
				catch(e){}
				/*for(var i=1;i<13;i++)
				{
					if(i%2==0)
					{
						$('#td'+i).html("<textarea name='growth"+i+"' id='growth"+i+"' class='form-control' maxlength='500' rows='5' style='width:240px;resize: none;' ></textarea>");
					}
					else
					{
						$('#td'+i).html("<textarea name='strength"+i+"' id='strength"+i+"' class='form-control' maxlength='500' rows='5' style='width:240px;resize: none;' ></textarea>");
					}
				}*/
			}
			else
			{
				document.getElementById("recomDiv").style.display="none";
				document.getElementById("remQuesDiv").style.display="none";
				document.getElementById('candidateDetails').style.display="none";
				document.getElementById("strengthOppDiv").style.display="none";
			}
			}catch(e){}
			
			//added key contact 
			//alert($('[name="isSuperAdministration"]').val());
			clickCount=0;
			if($('[name="isSuperAdministration"]').val()==1){
			callEnableOrDisableJqte();
			}
			//end key contact 
			
			//added by ram 21-04-2015 for if hired candidate
			var hiredFlagForClickOnStatus=0;
			//alert("hiredFlagForClickOnStatus==============="+hiredFlagForClickOnStatus);
			try{
				hiredFlagForClickOnStatus=$('[name="hiredFlagForClickOnStatus"]').val();
			}catch(e){}
			try{
				$('#jWTeacherStatusNotesDiv #checkOptions #email_da_sa_div .left16 #email_da_sa').parent().addClass('notclickable');
				$('#jWTeacherStatusNotesDiv #checkOptions #email_da_sa_div .left16 a').addClass('notclickable');
			}catch(e){}	
			try{
			$('.notclickable').css('pointer-events','');
			}catch(e){}	
			var entityType=$('[name="entityType"]').val();
			if(hiredFlagForClickOnStatus==1){
				
				var curStatus=$("#statusName").val();
				if(curStatus!=='Declined' && curStatus!=='Rejected' && curStatus!=='Hired' && curStatus!=='Withdraw'){
					//$('#jWTeacherStatusNotesDiv .modal-footer .btn').hide();
					$('#jWTeacherStatusNotesDiv #undo').hide();
					$('#jWTeacherStatusNotesDiv #statusSave').hide();
					$('#jWTeacherStatusNotesDiv #statusFinalize').hide();
					$('#jWTeacherStatusNotesDiv #cancelButtonRefChk').show();
					setTimeout(function(){$('.notclickable').css('pointer-events','none');},10);					
				}
				else if(entityType==2 && (curStatus=='Declined' || curStatus=='Rejected' || curStatus=='Hired' || curStatus=='Withdraw')){
					$('#jWTeacherStatusNotesDiv #statusSave').show();
					$('#jWTeacherStatusNotesDiv #statusFinalize').show();
					$('#jWTeacherStatusNotesDiv #cancelButtonRefChk').show();
					setTimeout(function(){$('.notclickable').css('pointer-events','');},5);
				}
			}			
			//ended by ram 21-04-2015
			setTimeout(function(){
			try{
			$('.districtTooltip').tooltip();
			}catch(e){}
			},100);
			applyScrollOnStatusNotes("tblStatusNote");
			applyScrollOnStatusNote("tblStatusNote_2");
			try{
				var entityType=$('[name="entityType"]').val();
				if(entityType==5 || entityType==6){
					document.getElementById("statusSave").style.display="none";
				}
			}catch(err){}
		}
	});
}
}
var popoverObj;
function scroeGrid(){
	$('#expNotePdf').tooltip();
	$('#noFitScore').tooltip();
	$('#attachInstructionFileName').tooltip();
	popoverObj=$('.statusscore').popover({ 
	 	trigger: "manual",
	    html : true,
	    placement: 'left',
	    template: '<div class="popover" id="popover" style="width: 400px;top: 80%;"><div class="arrow" style="top:10px;"></div><div class="popover-inner"><h3 class="popover-title"></h3><div class="popover-content"><p></p></div></div></div>',
	    content:  //$('#scoreList').html(),
	    	//added by 03-04-2015
	    	function(){
				return '<div id=\"contentEditDivScore\">'+$('#scoreList').html()+'</div>';
					},
			//ended by 03-04-2015
	  }).on("mouseenter", function () {
		  
		  var height=setDialogPosition();
		    height=height+100;
		  	var style = $('<style>.popover { position: absolute;top: '+height+'px !important;left: 0px; }</style>')
			$('html > head').append(style);
		  	
	        var _this = this;
	        $(this).popover("show");
	        $(this).siblings(".popover").on("mouseleave", function () {
	            //$(_this).popover('hide');
	        });
	    }).on("mouseleave", function () {
	        var _this = this;
	        setTimeout(function () {
	            if (!$(".popover:hover").length) {
	                //$(_this).popover("hide")
	            }
	        }, 100);
	    });
}

//add by Ram Nath
function setDialogPosition()
{
	var scrollTopPosition = $(window).scrollTop();
	var x;
	var y;
	$(function() {
		$("#statusscoreYes").hover(function(e) {
		  var offset = $(this).offset();
		  x = (e.pageX - offset.left);
		  y = (e.pageY - offset.top);
		});
		});
	var z=y-180+scrollTopPosition;	
	return z;
}
// end By Ram Nath
/*function setDialogPosition()
{
	var scrollTopPosition = $(window).scrollTop();
	var x=event.clientX; 
	var y=event.clientY;
	var z=y-180+scrollTopPosition;	
	return z;
}*/
function jWTeacherStatusNotesOnClose(){
	try{
		$('#jWTeacherStatusNotesDiv').modal('hide');
		document.getElementById("noteMainDiv").style.display="none";
		document.getElementById("scoreProvided").value="";
		$('#myModalStatus').modal('show');	
	}
	catch(err){}
	displayStatusDashboard(1);
}

function jWTeacherStatusNotesOnCloseForRef(){
	try{
		$('#jWTeacherStatusNotesDiv').modal('hide');
		document.getElementById("noteMainDiv").style.display="none";
		document.getElementById("scoreProvided").value="";
		//$('#myModalStatus').modal('show');
	}
	catch(err){}
	//displayStatusDashboard(1);
}

function jWTeacherStatusNotesOnCloseForSLC(){	
	var ksnDivCheck=0;
	try{
		document.getElementById("talentKSNDetailDataId").style.display;
		ksnDivCheck=1;
	}catch(err){}
	try{
		$('#jWTeacherStatusNotesDiv').modal('hide');
		document.getElementById("noteMainDiv").style.display="none";
		document.getElementById("scoreProvided").value="";
		$('#myModalStatus').modal('show');			
	}
	catch(err){}
	try{$('#myModalReferenceCheckConfirm1').hide();}catch(e){}
	superAdminClickOnEditNote=0;	
	if(ksnDivCheck==1){
		displayStatusDashboard(1);
	}
	try{$('#myModalStatus').modal('show');}catch(e){}
}



function sendOriginalEmail()
{
	$('#isEmailTemplateChanged').val(0);
	emailClose();
}

function sendOriginalEmailTeacher()
{
	$('#isEmailTemplateChangedTeacher').val(0);
	emailClose();
}

function emailClose(){
	try{
		$('#myModalEmail').modal('hide'); 
		$('#myModalEmailTeacher').modal('hide'); 
		$('#jWTeacherStatusNotesDiv').modal('show');	
	}
	catch(err){}
	displayStatusDashboard(1);
}
function addStatusNoteFileType()
{
	$('#statusNoteFileNames').empty();
	$('#statusNoteFileNames').html("<a href='javascript:void(0);' onclick='removeStatusNoteFile();'><img src='images/can-icon.png' title='remove'/></a><br> <input id='statusNoteFileName' name='statusNoteFileName' size='20' title='"+resourceJSON.ChooseFile+"' type='file' style='margin-left:20px;margin-top:-15px;'><br>&nbsp;"+resourceJSON.MaxSize10Mb+".");
}
function removeStatusNoteFile()
{
	$('#statusNoteFileNames').empty();
	$('#statusNoteFileNames').html("<a href='javascript:void(0);' onclick='addStatusNoteFileType();'><img src='images/attach.png'/></a> <a href='javascript:void(0);' onclick='addStatusNoteFileType();'>"+resourceJSON.AttachFile+"</a>");
}
function updateStatusHDR(flagValue){
	document.getElementById("updateStatusHDRFlag").value=flagValue;
	var entityType=document.getElementById("entityType").value;
	var statusFlag=$("#statusNotesForStatus").val()=="true"?true:false;
	var statusNotes = $('#statusNotes').find(".jqte_editor").html();
	var cnt=0;
	var focs=0;	
	var statusNoteFileName=null;
	try{  
		statusNoteFileName=document.getElementById("statusNoteFileName").value;
	}catch(e){}
	
	var noteDiv=0;
	try{
		if(document.getElementById("noteMainDiv").style.display=="inline"){
			noteDiv=1;
		}
	}catch(err){}
	var dsliderchk=1;
	try{
		dsliderchk=document.getElementById("dsliderchk").value;
	}catch(err){}
	try{
		var iframeNorm = document.getElementById('ifrmStatusNote');
		var innerNorm = iframeNorm.contentDocument || iframeNorm.contentWindow.document;
		var inputNormScore = innerNorm.getElementById('statusNoteFrm');
		document.getElementById("scoreProvided").value=inputNormScore.value;
	}catch(e){}	
	var scoreProvided=trim(document.getElementById("scoreProvided").value);
		$('#errorStatusNote').empty();
		if(statusFlag){
			if ($('#statusNotes').find(".jqte_editor").text().trim()=="" && scoreProvided==0.0 && noteDiv==0 && dsliderchk==0.0){
				$('#errorStatusNote').show();
				$('#errorStatusNote').append("&#149; "+resourceJSON.PlzEtrNoteOrScore+".<br>");
				cnt=1;
			}else if (($('#statusNotes').find(".jqte_editor").text().trim()=="" && (noteDiv==1 || (dsliderchk==-1 || dsliderchk==1))) && statusName!="Asst Supe Interview"){
				$('#errorStatusNote').show();
				$('#errorStatusNote').append("&#149; "+resourceJSON.PlzEtrNotes+".<br>");
				cnt=1;
			}else if(($('#statusNotes').find(".jqte_editor").text().trim()!="") && (statusName!="Asst Supe Interview")){
				var charCount=$('#statusNotes').find(".jqte_editor").text().trim();
				var count = charCount.length;
				if(count>4000)
				{
					$('#errorStatusNote').show();
					$('#errorStatusNote').append("&#149; "+resourceJSON.MsgLengthnotExceed+".<br>");
					cnt=1;
				}
			}
			if(statusNoteFileName=="" && cnt==0){
				$('#errorStatusNote').show();
				$('#errorStatusNote').append("&#149; "+resourceJSON.PlzSelectFileToUpload+".<br>");
				cnt++;
			}
		}
		
		
		
		var entityType=$('[name="entityType"]').val();
		var statusName = document.getElementById("statusName").value;
		var waivedstatus = "";
		if(statusName==resourceJSON.lblSPStatus && (entityType==5 || entityType==6)){
			waivedstatus = $('#waivedReasonNote option:selected').text();
			statusNotes = waivedstatus;
			if(statusNotes=='Select Reasons'){
				$('#errorStatusNote').show();
				$('#errorStatusNote').append("&#149; "+resourceJSON.PlzEtrWaivedReason+".<br>");
				$('[name="startDate"]').focus();
				$('#divTxtShowData2CG').css('display','none');
				cnt++;
			}
			
		}
		
		
		if(cnt==0){
			$('#jWTeacherStatusNotesDiv').modal('hide');
			if(flagValue==2){
				document.getElementById("updateStatusNoteMsg").innerHTML=""+resourceJSON.MsgUnhireTheCandidate+"";
			}else if(flagValue==3){
				document.getElementById("updateStatusNoteMsg").innerHTML=""+resourceJSON.MsgUndeclinedTheCandidate+"";
			}else if(flagValue==4){
				document.getElementById("updateStatusNoteMsg").innerHTML=""+resourceJSON.MsgUnRejectTheCandidate+"";
			}else if(flagValue==5){
				document.getElementById("updateStatusNoteMsg").innerHTML=""+resourceJSON.MsgUndoTheStatus+".";
			}else if(flagValue==6){
				document.getElementById("updateStatusNoteMsg").innerHTML=""+resourceJSON.MsgResendTheOfferLetter+"";
			}else if(flagValue==8){
				document.getElementById("updateStatusNoteMsg").innerHTML=""+resourceJSON.MsgUnWithdrawTheCandidate+"";
			}
			$('#modalUpdateStatusNoteMsg').modal('show');
		}
}
function closeUpdateStatusNoteMsg(flag){
	$('#modalUpdateStatusNoteMsg').modal('hide');
	if(flag==1){
		var updateStatusHDRFlag=document.getElementById("updateStatusHDRFlag").value;
		saveStatusNote(updateStatusHDRFlag);

	}else{
		$('#jWTeacherStatusNotesDiv').modal('show');
	}
}
function offerAcceptedMsg(){
	$('#jWTeacherStatusNotesDiv').modal('hide');
	$('#offerAcceptedModal').modal('show');
	
}
function offerAcceptedOK(){
	document.getElementById("offerAccepted").value=1;
	$('#offerAcceptedModal').modal('hide');
	$('#jWTeacherStatusNotesDiv').modal('show');
	saveStatusNote(1);
}
function cancelOfferAccepted(){
	$('#offerAcceptedModal').modal('hide');
	jWTeacherStatusNotesOnClose();
}
function offerReadyMsg(flag,offerMsg){
	if(flag==0){
		document.getElementById("offer_Ready_title").innerHTML="Offer Ready";
	}else{
		document.getElementById("offer_Ready_title").innerHTML="Status";
	}
	document.getElementById("offerReadyDiv").innerHTML=offerMsg;
	$('#myModalStatus').modal('hide');
	$('#offerReadyModal').modal('show');
}
function offerReadyCheck(){
	document.getElementById("offerReadyDiv").innerHTML="";
	$('#offerReadyModal').modal('hide');
	$('#myModalStatus').modal('show');
}

//add By Ram Nath
function getConfirm(header,confirmMessage,buttonNameChange,callback){
	var div="<div class='modal hide in' id='confirmbox' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'>"+
			"<div class='modal-dialog'>"+
			"<div class='modal-content'>"+
			"<div class='modal-header'>"+
	        "<!--<button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>-->"+
	        "<h4 class='modal-title' id='myModalLabel' style='color:white;'></h4>"+
	      "</div>"+
	      "<div class='modal-body' id='confirmMessage'>"+	      
	      "</div>"+
	      "<div class='modal-footer'>"+
	      "<button type='button' class='btn btn-primary' id='confirmTrue' ></button>"+
	        "<button type='button' class='btn btn-default' id='confirmFalse'>"+resourceJSON.btnCancel+"</button>"+	        
	      "</div>"+
	    "</div>"+
	  "</div>"+
	"</div>";
	$('#jWTeacherStatusNotesDiv').after(div);
    confirmMessage = confirmMessage || '';
    $('#jWTeacherStatusNotesDiv').modal('hide');
    $('#confirmbox').modal({show:true,
                            backdrop: "static",
                            keyboard: false,
    });

    $('#confirmbox #confirmMessage').html(confirmMessage);
    $('#confirmbox #myModalLabel').html(header);
    $('#confirmbox #confirmTrue').html(buttonNameChange);
    $('#confirmFalse').click(function(){
    	$('#confirmbox').modal('hide');
        $('#confirmbox').remove();
        minOneScoreSliderSetFlag=2;
        $('#jWTeacherStatusNotesDiv').modal('show');
        if (callback) callback(false);
    });
    $('#confirmTrue').click(function(){
    	$('#confirmbox').modal('hide');
        $('#confirmbox').remove();
        minOneScoreSliderSetFlag=1;
        if (callback) callback(true);
    });
} 
function getConfirmScoreSetOrNot(header,confirmMessage,buttonNameChange,buttonNameChange1,callback){
	var div="<div class='modal hide in' id='confirmbox' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'>"+
			"<div class='modal-dialog' style='width:470px;'>"+
			"<div class='modal-content'>"+
			"<div class='modal-header'>"+
	        "<!--<button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>-->"+
	        "<h4 class='modal-title' id='myModalLabel' style='color:white;'></h4>"+
	      "</div>"+
	      "<div class='modal-body' id='confirmMessage'>"+	      
	      "</div>"+
	      "<div class='modal-footer'>"+
	      "<button type='button' class='btn btn-primary' id='notScore' ></button>"+
	      "<button type='button' class='btn btn-primary' id='confirmTrue' ></button>"+
	        "<button type='button' class='btn btn-default' id='confirmFalse'>"+resourceJSON.btnCancel+"</button>"+	        
	      "</div>"+
	    "</div>"+
	  "</div>"+
	"</div>";
	$('#jWTeacherStatusNotesDiv').after(div);
    confirmMessage = confirmMessage || '';
    $('#jWTeacherStatusNotesDiv').modal('hide');
    $('#confirmbox').modal({show:true,
                            backdrop: "static",
                            keyboard: false,
    });

    $('#confirmbox #confirmMessage').html(confirmMessage);
    $('#confirmbox #myModalLabel').html(header);
    $('#confirmbox #confirmTrue').html(buttonNameChange);
    $('#confirmbox #notScore').html(buttonNameChange1);
    $('#confirmFalse').click(function(){
    	$('#confirmbox').modal('hide');
        $('#confirmbox').remove();
        minOneScoreSliderSetOrNotFlag=0;
        $('#jWTeacherStatusNotesDiv').modal('show');
        if (callback) callback(false);
    });
    $('#confirmTrue').click(function(){
    	$('#confirmbox').modal('hide');
        $('#confirmbox').remove();
        minOneScoreSliderSetOrNotFlag=1;
        if (callback) callback(true);
    });
    $('#notScore').click(function(){
    	$('#confirmbox').modal('hide');
        $('#confirmbox').remove();
        minOneScoreSliderSetOrNotFlag=2;
        if (callback) callback(true);
    });
} 
// end By Ram Nath

function saveStatusNote(finalizeStatus)
{
	$('#loadingDiv').css('z-index','999999 !important');
	$('#loadingDiv').show();
	setTimeout(function(){
		if(validateFinelizeSpecificTemplates()==true)
			sendFinelizeSpecificMailToTeacher();
		else
			return false;
		
		var isTemplateFound = false;
		if($("#finelizeSpecificTemplatesName option:selected").val()!="")
		   isTemplateFound = true;
		
		//for super administration user check click on edit button or not
		//alert("isClickCountOnEditForSuperUser==="+finalizeStatus+"==="+isClickCountOnEditForSuperUser);
		if(isClickCountOnEditForSuperUser>0){
			var messageforEditNote=""+resourceJSON.MsgOpenOneOrMoreNote+" <strong>\""+resourceJSON.BtnContinue+"\"</strong> "+resourceJSON.BtnToIgnore+" <strong>\""+resourceJSON.btnCancel+"\"</strong>"+resourceJSON.BtnToGoBack+" <strong>\""+resourceJSON.MsgSaveNote+"\"</strong> "+resourceJSON.MsgIconInTheRight+".";
			getConfirmEditOrNot($('#jWTeacherStatusNotesDiv .modal-dialog #myModalLabel #status_title').html(),messageforEditNote,"Continue",finalizeStatus);
			return false;
		}
		//ended super administration user check click on edit button or not
		
		$('#errorStatusNote').empty();
		var statusFlag=$("#statusNotesForStatus").val()=="true"?true:false;
		var txtPanel=document.getElementById("txtPanel").value;
		var txtOverride=document.getElementById("txtOverride").value;
		var txtLoggedInUserPanelAttendees=document.getElementById("txtLoggedInUserPanelAttendees").value;
		var iQdslider=document.getElementById("iQdslider").value;
		var hireFlag=document.getElementById("hireFlag").value;
		var requisitionNumberId=null;
		var chkOverride=false;
		var flagForUno=false;
		
		var onlineActivityForStatus=0;
		try{
			onlineActivityForStatus=document.getElementById("onlineActivityForStatus").value;
		}catch(err){}
		var refChk	=	document.getElementById("referenceValue").value;
		
		var rdReqSel=0;
		try {
			rdReqSel=$("input[name=rdReqSel]:checked").val(); 
			if(rdReqSel==undefined){
				rdReqSel=0;
			}
		} catch (e) {
			rdReqSel=0;
		}
		var txtNewReqNo="";
		if(rdReqSel==2)
			try { txtNewReqNo=trim(document.getElementById("txtNewReqNo").value); } catch (e) {}
		
		// Calculate Contact Detail Record
		var referenceListSize 	= 	0;
		var contactSize	  		=	0;
		try{
			referenceListSize	=	document.getElementById("referenceListSize").value;
			contactSize			=	document.getElementById("contactSize").value;
			requiredReference	=	document.getElementById("requiredReference").value;
		}catch(error){}
		
		
		if(txtOverride==1)
		{
			chkOverride=document.getElementById('chkOverride').checked;
		}
		
		var statusName = document.getElementById("statusName").value;
		var setHiredDate ="";
		var strength =  [];
		var growthOpp =  [];
		if(finalizeStatus==1 )
		{
			if(statusName == "Asst Supe Interview")
			{
				for(var i=0;i<6;i++)
				{
						if($('#growth'+i).val()!=undefined && $('#growth'+i).val()!=null)
						{
							//alert($('#growth'+i).val());
							growthOpp.push("growth"+i+"@@@"+$('#growth'+i).val());
						}
						if($('#strength'+i).val()!=undefined && $('#strength'+i).val()!=null)
						{
							//alert($('#strength'+i).val());
							strength.push("strength"+i+"@@@"+$('#strength'+i).val());
						}
				}

			}
		}
		
		if(finalizeStatus==1 ){
			if(statusName == "e-References"){
			//if(referenceListSize != contactSize){
				if(contactSize < requiredReference){
					$('#errorStatusNote').show();
					$('#errorStatusNote').append("&#149; "+resourceJSON.MsgBeforeFinalize+".<br>");
					return false;
				}
			}
		}

		if(statusName == "Hired"){
			setHiredDate = document.getElementById("setHiredDate").value;
			if(setHiredDate == "" || setHiredDate == null){
				$('#errorStatusNote').show();
				$('#errorStatusNote').append("&#149; "+resourceJSON.PlzSelectHireddate+".<br>");
				return false;
			}
		} else {
			setHiredDate = document.getElementById("setHiredDate").value;
			setHiredDate = "";
		}
		
		
		/*SWADESH*/
		if(finalizeStatus!=3){
		if(statusName == "Withdrew" || statusName == "Declined"){
			timingforDclWrdw = document.getElementById("timingforDclWrdw").value;
			reasonforDclWrdw = document.getElementById("reasonforDclWrdw").value;
			if(timingforDclWrdw == "0" || timingforDclWrdw == '' || reasonforDclWrdw == "0" || reasonforDclWrdw == ''){
				$('#errorStatusNote').show();
				if(timingforDclWrdw == "0" || timingforDclWrdw == '')
					$('#errorStatusNote').append("&#149; "+resourceJSON.PlzSelectHireddate+" 1.<br>");
				if(reasonforDclWrdw == "0" || reasonforDclWrdw == '')
					$('#errorStatusNote').append("&#149; "+resourceJSON.PlzSelectHireddate+" 2.<br>");
				return false;
			}
		}
		} 
		
		/*
		 * 		Date	:	07-08-2014
		 * 		By		:	Hanzala Subhani
		 * 		Purpose	:	Validate Status from teacheracademic 
		 */
		if(statusName == 'Ofcl Trans / Veteran Pref'){
			var teacherAcademicsStatus	 	=	document.getElementById("teacherAcademicsStatus").value;
			var split =	teacherAcademicsStatus.split(",");
			var counter = 0;
			for(var i=1; i < split.length; i++){
				if(split[i] == "" || split[i] == null || split[i] == "P"){
					counter	=	counter+1;
				}
			}
			if(counter > 0){
				$('#errorStatusNote').show();
				$('#errorStatusNote').append("&#149; "+resourceJSON.PlzSetStatusOfAllTranscript+".<br>");
				return false;
			}
		}
		/*====== Gagan [ Start ] : variable for status wise email templates ================*/
		var isEmailTemplateChanged	=	$('#isEmailTemplateChanged').val();
		var statusShortName	=	$("#statusShortName").val();
		var msgSubject	=	$('#subjectLine').val();
		//var charCount	=	$('#mailBody').find(".jqte_editor").text();
		var isEmailTemplateChangedTeacher	=	$('#isEmailTemplateChangedTeacher').val();
		var msgSubjectTeacher	 =	$('#subjectLineTeacher').val();
		var msgSubjectForTeacher =   $('#finelizeSpecificSubjectLine').val();
		var msgBodyForTeacher =   $("#finelizeSpecificMessage").find(".jqte_editor").html();

		//var charCountTeacher	=	$('#mailBodyTeacher').find(".jqte_editor").text();
		/*====== Gagan [ END ] : variable for status wise email templates ================*/
		
		// Start for save Question score
		try{
			var iframeNorm = document.getElementById('ifrmStatusNote');
			var innerNorm = iframeNorm.contentDocument || iframeNorm.contentWindow.document;
			var inputNormScore = innerNorm.getElementById('statusNoteFrm');
			document.getElementById("scoreProvided").value=inputNormScore.value;
		}catch(e){}	
		var scoreProvided=trim(document.getElementById("scoreProvided").value);
		
		var answerId_arrary = new Array();
		var answerScore_arrary = new Array();
		
		var answerFinalize=0;
		var sumAnswerScore=0;
		var sumAnswerScore=0;
		var iCountScoreSlider=document.getElementById("iCountScoreSlider").value;
		if(iCountScoreSlider>0)
		{
			for(var i=1;i<= iCountScoreSlider;i++)
			{
				var questionscore=0;
				var answerId=0;
				
				try{
					var ifrmQuestion = document.getElementById('ifrmQuestion_'+i);
					if(ifrmQuestion!=null)
					{
						var innerQuestion = ifrmQuestion.contentDocument || ifrmQuestion.contentWindow.document;
						var inputQuestionFrm = innerQuestion.getElementById('questionFrm');
						var answerIdFrm = innerQuestion.getElementById('answerId');
						questionscore=inputQuestionFrm.value;
						answerId=answerIdFrm.value;
						
						sumAnswerScore=(parseInt(sumAnswerScore)+parseInt(questionscore));
						
						answerId_arrary[i-1]=answerId;
						answerScore_arrary[i-1]=questionscore;
					}
				}catch(e){}
				
				if(questionscore==0)
				{
					answerFinalize++;
				}
	
			}
		}
		var iCountScoreSlider=document.getElementById("iCountScoreSlider").value;
		var isQuestionEnable=document.getElementById("isQuestionEnable").value;
		// End for save Question score
		
		var jobForTeacherId =document.getElementById("jobForTeacherIdForSNote").value;
		var fitScore=document.getElementById("fitScoreForStatusNote").value;
		var secondaryStatusId=document.getElementById("secondaryStatusIdForStatusNote").value;
		document.getElementById("finalizeStatus").value=finalizeStatus;
		var statusNotes = $('#statusNotes').find(".jqte_editor").html();
		var teacherId = document.getElementById("statusTeacherId").value;
		var jobId = document.getElementById("jobId").value;
		var emailSentToD =0;// document.getElementById("emailSentToD").checked;
		var emailSentToS =0;// document.getElementById("emailSentToS").checked;
		var statusId=document.getElementById("statusIdForStatusNote").value;
		var secondaryStatusId=document.getElementById("secondaryStatusIdForStatusNote").value;
		var teacherStatusNoteId=document.getElementById("teacherStatusNoteId").value;
		var bEmailSendFor_DA_SA=document.getElementById('email_da_sa').checked;
		//shriram candidate email check flag
		var bEmailSendFor_CA=false;
		try{
			bEmailSendFor_CA=$('#email_to_candi').is(":checked")?true:false;
		}catch(e){}
		//alert("bEmailSendFor_CA"+bEmailSendFor_CA);
		//alert(bEmailSendFor_CA);
		//.............@AShish :: define questionsDetails and questionAssessmentIds..........
		var questionAssessmentIds = document.getElementById('questionAssessmentIds').value;
		var qqNoteIds = document.getElementById('qqNoteIds').value;
		var qqNoteIdsvalue_arr = qqNoteIds.split("#");
		var qqNoteIdsvalue_str ="";
		
		// ........................Gaurav Kumar.............................
		var msgSubjectForTeacher =   $('#finelizeSpecificSubjectLine').val();
		var msgBodyForTeacher =   $("#finelizeSpecificMessage").find(".jqte_editor").html();
		// ............. pawan kumar ..........................
		var flagQuestionCheack=true;
		var wrongAnswers="";
		var questionidssss=$('#myQuestionId').val();
		if(questionidssss!="")
		{
			try
			{
				var questionCheacks=$('#myQuestionCheacks').val();
				var questionIddsArray=questionidssss.split(',');
				var questionCheacking=questionCheacks.split(',');
				for(var i=0;i<questionIddsArray.length;i++)
				{
					var textValues=$("[name='questionNotes"+questionIddsArray[i]+"-"+questionCheacking[i]+"']").parents().parents('.jqte').find(".jqte_editor").html().trim();
					if(questionCheacking[i]=='true')
					{
						if(textValues=='')
						{
							flagQuestionCheack=false;
							wrongAnswers+=questionIddsArray[i]+",";
						}	
					}
				}
				var p=wrongAnswers.lastIndexOf(",");
				wrongAnswers=wrongAnswers.substring(0,p);
				var wrongAnswerArray=wrongAnswers.split(",");
				for(var y=0; y<questionIddsArray.length; y++)
				{
					$('#questionerror'+questionIddsArray[y]).hide();
				}
				if(flagQuestionCheack==false)
				{
					for(var y=0; y<wrongAnswerArray.length; y++)
					{
						$('#questionerror'+wrongAnswerArray[y]).show();
						$('#questionerror'+wrongAnswerArray[y]).text("Please answer this question.");
						$("[name='questionNotes"+questionIddsArray[0]+"-"+questionCheacking[0]+"']").parents().parents('.jqte').find(".jqte_editor").focus();
					}	
					return false;
				}
			}
			catch(err){}
		}
		if(qqNoteIds!=null && qqNoteIds!="")
		{
			for(var i=0;i<qqNoteIdsvalue_arr.length;i++)
			{
				
				if(i==(qqNoteIdsvalue_arr.length-1)){
					qqNoteIdsvalue_str=qqNoteIdsvalue_str+$("#"+qqNoteIdsvalue_arr[i]).find(".jqte_editor").html();
					
				}
				else if(qqNoteIdsvalue_str!=""){					
					qqNoteIdsvalue_str=qqNoteIdsvalue_str+$("#"+qqNoteIdsvalue_arr[i]).find(".jqte_editor").html()+"#";
					
				}else{										
					qqNoteIdsvalue_str=$("#"+qqNoteIdsvalue_arr[i]).find(".jqte_editor").html()+"#";
				}
				//....for Uno district aLL GENERAL NOTES REQUIRED...
				try{
					if($('#'+qqNoteIdsvalue_arr[i]).find(".jqte_editor").html().trim()=='')
					flagForUno=true;
				}catch(e){}
				//ENDED 
				
			}
		}
		//alert(qqNoteIdsvalue_str+"    "+qqNoteIdsvalue_arr);
		
		
		var dsliderchk=1;
		try{
			dsliderchk=document.getElementById("dsliderchk").value;
		}catch(err){}
		var statusNoteFileName=null;
		try{  
			statusNoteFileName=document.getElementById("statusNoteFileName").value;
		}catch(e){}
		
		var noteDiv=0;
		try{
			if(document.getElementById("noteMainDiv").style.display=="inline"){
				noteDiv=1;
			}
		}catch(err){}
		try{  
			requisitionNumberId=document.getElementById("requisitionNumber").value;
		}catch(e){}
		
		// add By Ram Nath
		var flagforSlider=true;	
		// end By Ram Nath
		
		var cnt=0;
		var focs=0;	
		$('#errorStatusNote').empty();
		try{
			if(offerReady.value=="Offer Ready" && requisitionNumberId==null && finalizeStatus!=5){
				$('#errorStatusNote').show();
				$('#errorStatusNote').append("&#149; "+resourceJSON.MsgNoPositionNumberFound+".<br>");
				cnt=1;
			}
		}catch(e){}
		try{
			if(statusName == "Online Activity"){
				if(onlineActivityForStatus==0 && finalizeStatus==1){
					$('#errorStatusNote').show();
					$('#errorStatusNote').append("&#149; "+resourceJSON.MsgQuestionnaireRelated+".<br>");
					cnt=1;
				}else if(onlineActivityForStatus==1 && finalizeStatus==1){
					$('#errorStatusNote').show();
					$('#errorStatusNote').append("&#149; "+resourceJSON.MsgCandidateHadNotResponded+".<br>");
					cnt=1;
				}
			}
		}catch(e){}
		if(!statusFlag){
			var score=0;
			if (scoreProvided==0.0 && dsliderchk==0.0)
			{
			
			}else if(iQdslider==1 && answerFinalize>0 && iCountScoreSlider!=answerFinalize){
				score=1;
			}else if(iQdslider==1 && iCountScoreSlider!=0 && sumAnswerScore==0 && (dsliderchk==-1 || dsliderchk==1)){
				score=1;
			}
			if (((scoreProvided==0.0 && dsliderchk==0.0) || score==1 || statusName.toLowerCase()=='jsi') && statusName!="Asst Supe Interview" )//added condition || statusName.toLowerCase()=='jsi'  by Ram nath
			{
				if (finalizeStatus==1 && $('#statusNotes').find(".jqte_editor").text().trim()=="" && scoreProvided==0.0 && noteDiv==0 && dsliderchk==0.0)
				{
					$('#errorStatusNote').show();
					$('#errorStatusNote').append("&#149; "+resourceJSON.PlzEtrNoteOrScore+".<br>");
					cnt=1;
				}
				// add By Ram Nath
				else if(finalizeStatus==1 && ((statusName.toLowerCase()=='jsi' && statusName != "Asst Supe Interview" ) || (answerFinalize>0 && iCountScoreSlider!=answerFinalize )))
				{	
					//alert('enter');
					var jsiflag=true;
					if(statusName.toLowerCase()=='jsi'){ 
						jsiflag=false;
						var disableSliderForTemp=false;
						var disableSlider =$("#ifrmQuestion_1").contents().find('.ui-slider-wrapper.ui-widget.horizontal').attr('aria-disabled');
							if(disableSlider=="true"){
								disableSliderForTemp=false;
							}else{
								disableSliderForTemp=true;
							}						
						//alert("dsliderchk:::::1111======1"+dsliderchk+"   "+(disableSliderForTemp) );
						if((dsliderchk==1 || dsliderchk==-1) && disableSliderForTemp){
					var totalSetScoreCount=0;
					var totalsetScoreValue=0;					
					if(minOneScoreSliderSetFlag==2)
					$.each(answerScore_arrary,function(k,v){
						totalsetScoreValue+=parseInt(v);
						if(v>0)
							totalSetScoreCount++;
					});
					if(totalSetScoreCount>=0 && totalSetScoreCount<answerScore_arrary.length && minOneScoreSliderSetFlag==2){
						flagforSlider=false;
						var confirmMessage=""+resourceJSON.MsgOneOrMoreQuestion+".";
						getConfirm($('#jWTeacherStatusNotesDiv .modal-dialog #myModalLabel #status_title').html(),confirmMessage,resourceJSON.btnOk,function(data){
						if(data){
						// if(confirm('You only have to set '+totalSetScoreCount+'
						// Score that you want to save anyway')){
						    saveStatusNote(finalizeStatus);	
						}
						});	
					}
						}else{							
							/*if($('#statusNotes').find(".jqte_editor").text().trim()==""){
							$('#errorStatusNote').show();
							$('#errorStatusNote').append("&#149; Please enter Note.<br>");
							cnt=1;
							}else{*/
								cnt=0;
							//}
						}
					if(minOneScoreSliderSetFlag==1){
						$('#errorStatusNote').hide();
						cnt=0;
					}	
					}
					if(jsiflag){
						// end By Ram Nath
						$('#errorStatusNote').show();
						$('#errorStatusNote').append("&#149; "+resourceJSON.SetScoreForAllAttributeToFinalize+".<br>");
						cnt=1;
					}
					
				}
				// add By Ram Nath
				//comment by Ram Nath
				/*else if(finalizeStatus==1 && answerFinalize>0 && iCountScoreSlider!=answerFinalize)
				{
					$('#errorStatusNote').show();
					$('#errorStatusNote').append("&#149; Please set Score for all the attributes to Finalize.<br>");
					cnt=1;
				}*/
				else if ($('#statusNotes').find(".jqte_editor").text().trim()=="" && (noteDiv==1 || (dsliderchk==-1 || dsliderchk==1)))
				{
					if(isQuestionEnable=='false' || iCountScoreSlider==0)
					{
						if(statusName.toLowerCase()!='jsi'){//added by Ram nath
						$('#errorStatusNote').show();
						$('#errorStatusNote').append("&#149; "+resourceJSON.PlzEtrNotes+".<br>");
						cnt=1;
						}//added by Ram nath
					}
					else if(sumAnswerScore==0)
					{
						$('#errorStatusNote').show();
						$('#errorStatusNote').append("&#149; "+resourceJSON.MsgSetScoreOrEnterNoteForAttribute+".<br>");
						cnt=1;
					}
				}else if ($('#statusNotes').find(".jqte_editor").text().trim()=="" && noteDiv==0&& scoreProvided==0.0 && dsliderchk==-2)
				{
					
					
					if(isQuestionEnable=='false' || iCountScoreSlider==0)
					{
						$('#errorStatusNote').show();
						$('#errorStatusNote').append("&#149; "+resourceJSON.PlzEtrNotes+".<br>");
						cnt=1;
					}
				}
				else if($('#statusNotes').find(".jqte_editor").text().trim()!="")
				{
					var charCount=$('#statusNotes').find(".jqte_editor").text().trim();
					var count = charCount.length;
					if(count>4000)
					{
						$('#errorStatusNote').show();
						$('#errorStatusNote').append("&#149; "+resourceJSON.MsgLengthnotExceed+".<br>");
						cnt=1;
					}
				}
				if(statusNoteFileName=="" && cnt==0){
					$('#errorStatusNote').show();
					$('#errorStatusNote').append("&#149; "+resourceJSON.PlzSelectFileToUpload+".<br>");
					cnt++;
				}else if(statusNoteFileName!="" && statusNoteFileName!=null)
				{
					var ext = statusNoteFileName.substr(statusNoteFileName.lastIndexOf('.') + 1).toLowerCase();	
					var fileSize = 0;
					if ($.browser.msie==true)
				 	{	
					    fileSize = 0;	   
					}
					else
					{
						if(document.getElementById("statusNoteFileName").files[0]!=undefined)
						{
							fileSize = document.getElementById("statusNoteFileName").files[0].size;
						}
					}
					if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt'|| ext=='xlsx' || ext=='xls'))
					{
						$('#errorStatusNote').show();
						$('#errorStatusNote').append("&#149; "+resourceJSON.PlzSlctAcceptableNoteFormats+".<br>");
						cnt++;
					}
					else if(fileSize>=10485760)
					{
						$('#errorStatusNote').show();
						$('#errorStatusNote').append("&#149; "+resourceJSON.msgfilesizelessthan+".<br>");
						cnt++;
					}
				}
			}
			try{//shriram
				if ($('#statusNotes').find(".jqte_editor").text().trim()=="" && (noteDiv==1 || (dsliderchk==-1 || dsliderchk==1)) && statusName=="Offer Ready"){
					if(isQuestionEnable=='false' || iCountScoreSlider==0) {
				        $('#errorStatusNote').show();
				        $('#errorStatusNote').append("&#149; "+resourceJSON.PlzEtrNotes+".<br>");
				        cnt=1;
				    } else if(sumAnswerScore==0) {
				        $('#errorStatusNote').show();
				        $('#errorStatusNote').append("&#149; "+resourceJSON.MsgSetScoreOrEnterNoteForAttribute+".<br>");
				        cnt=1;
				    }
				}
			}catch(err){}
		} else { //End statusFlag
			if ($('#statusNotes').find(".jqte_editor").text().trim()=="" && scoreProvided==0.0 && noteDiv==0 && dsliderchk==0.0 && statusName!="Asst Supe Interview")
			{
				$('#errorStatusNote').show();
				$('#errorStatusNote').append("&#149; "+resourceJSON.PlzEtrNoteOrScore+".<br>");
				cnt=1;
			}
			// add By Ram Nath
			else if(finalizeStatus==1 && ((statusName.toLowerCase()=='jsi' )|| (answerFinalize>0 && iCountScoreSlider!=answerFinalize )))
			{								
				var jsiflag=true;
				if(statusName.toLowerCase()=='jsi'){ 
					jsiflag=false;
					var disableSliderForTemp=false;
					var disableSlider =$("#ifrmQuestion_1").contents().find('.ui-slider-wrapper.ui-widget.horizontal').attr('aria-disabled');
						if(disableSlider=="true"){
							disableSliderForTemp=false;
						}else{
							disableSliderForTemp=true;
						}
					//alert("dsliderchk:::::222222"+dsliderchk+"   "+disableSlider);
					if((dsliderchk==1 || dsliderchk==-1) && disableSliderForTemp){
					//if(dsliderchk==1 && !disableSlider){
				var totalSetScoreCount=0;
				var totalsetScoreValue=0;					
				if(minOneScoreSliderSetFlag==2)
				$.each(answerScore_arrary,function(k,v){
					totalsetScoreValue+=parseInt(v);
					if(v>0)
						totalSetScoreCount++;
				});
				if(totalSetScoreCount>=0 && totalSetScoreCount<answerScore_arrary.length && minOneScoreSliderSetFlag==2){
					flagforSlider=false;
					//alert(flagforSlider);
					var confirmMessage=""+resourceJSON.MsgOneOrMoreQuestion+".";
					getConfirm($('#jWTeacherStatusNotesDiv .modal-dialog #myModalLabel #status_title').html(),confirmMessage,resourceJSON.btnOk,function(data){
					if(data){
					// if(confirm('You only have to set '+totalSetScoreCount+'
					// Score that you want to save anyway')){
					    saveStatusNote(finalizeStatus);	
					}
					});	
				}
					}else{
						if($('#statusNotes').find(".jqte_editor").text().trim()=="" &&  statusName!="" && statusName!="Asst Supe Interview"){
						$('#errorStatusNote').show();
						$('#errorStatusNote').append("&#149; "+resourceJSON.PlzEtrNotes+".<br>");
						cnt=1;
						}else{
							cnt=0;
						}
					}
				if(minOneScoreSliderSetFlag==1){
					$('#errorStatusNote').hide();
					cnt=0;
				}	
				}
				if(jsiflag){
					// end By Ram Nath
					$('#errorStatusNote').show();
					$('#errorStatusNote').append("&#149; "+resourceJSON.SetScoreForAllAttributeToFinalize+".<br>");
					cnt=1;
				}
				
			}
			// add By Ram Nath
			//comment by Ram Nath
			/*else if(finalizeStatus==1 && answerFinalize>0 && iCountScoreSlider!=answerFinalize)
			{
				$('#errorStatusNote').show();
				$('#errorStatusNote').append("&#149; Please set Score for all the attributes to Finalize.<br>");
				cnt=1;
			}*/
			else if ($('#statusNotes').find(".jqte_editor").text().trim()=="" && (noteDiv==1 || (dsliderchk==-1 || dsliderchk==1)) && statusName!="Asst Supe Interview")
			{
				if(isQuestionEnable=='false' || iCountScoreSlider==0)
				{
					$('#errorStatusNote').show();
					$('#errorStatusNote').append("&#149; "+resourceJSON.PlzEtrNotes+".<br>");
					cnt=1;
				}
				else if(sumAnswerScore==0)
				{
					$('#errorStatusNote').show();
					$('#errorStatusNote').append("&#149; "+resourceJSON.MsgSetScoreOrEnterNoteForAttribute+".<br>");
					cnt=1;
				}
			}else if ($('#statusNotes').find(".jqte_editor").text().trim()=="" && noteDiv==0 && scoreProvided==0.0 && dsliderchk==-2 && statusName!="Asst Supe Interview")
			{
				if(isQuestionEnable=='false' || iCountScoreSlider==0)
				{
					$('#errorStatusNote').show();
					$('#errorStatusNote').append("&#149; "+resourceJSON.PlzEtrNotes+".<br>");
					cnt=1;
				}
			}
			else if($('#statusNotes').find(".jqte_editor").text().trim()!="" && statusName!="Asst Supe Interview")
			{
				var charCount=$('#statusNotes').find(".jqte_editor").text().trim();
				var count = charCount.length;
				if(count>4000)
				{
					$('#errorStatusNote').show();
					$('#errorStatusNote').append("&#149; "+resourceJSON.MsgLengthnotExceed+".<br>");
					cnt=1;
				}
			}
			if(statusNoteFileName=="" && cnt==0){
				$('#errorStatusNote').show();
				$('#errorStatusNote').append("&#149; "+resourceJSON.PlzSelectFileToUpload+".<br>");
				cnt++;
			}else if(statusNoteFileName!="" && statusNoteFileName!=null)
			{
				var ext = statusNoteFileName.substr(statusNoteFileName.lastIndexOf('.') + 1).toLowerCase();	
				var fileSize = 0;
				if ($.browser.msie==true)
			 	{	
				    fileSize = 0;	   
				}
				else
				{
					if(document.getElementById("statusNoteFileName").files[0]!=undefined)
					{
						fileSize = document.getElementById("statusNoteFileName").files[0].size;
					}
				}
				if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt'|| ext=='xlsx' || ext=='xls'))
				{
					$('#errorStatusNote').show();
					$('#errorStatusNote').append("&#149; "+resourceJSON.PlzSlctAcceptableNoteFormats+".<br>");
					cnt++;
				}
				else if(fileSize>=10485760)
				{
					$('#errorStatusNote').show();
					$('#errorStatusNote').append("&#149; "+resourceJSON.msgfilesizelessthan+".<br>");
					cnt++;
				}
			}
		}
			
		//added by 03-04-2015 for UNO District (Recomended for Hire) status
		var requiredNotesForUno=document.getElementById("requiredNotesForUno").value;		
		var nextStepFlag=true;
		if(requiredNotesForUno==1 && flagForUno){
			nextStepFlag=false;
			$('#errorStatusNote').show();
			$('#errorStatusNote').append("&#149; "+resourceJSON.MsgAllNotesAreRequired+".<br>");
		}
		//ended by 03-04-2015
		
		// add By Ram Nath for slider
		if(flagforSlider && nextStepFlag){
			// alert('enter without slider');
			// end By Ram Nath
		var txtschoolCount=0;
		var schoolId=0;
		var isReqNoForHiring=0;
		if(finalizeStatus==1){
			try{
				isReqNoForHiring=document.getElementById("isReqNoForHiring").value;
			}catch(err){}
			try{
				txtschoolCount=document.getElementById("txtschoolCount").value;
			}catch(err){}
			var schoolDivCheck=0;
			try{
				if(document.getElementById("schoolAutoSuggestDivId").style.display!="none"){
					schoolDivCheck=1;
				}
			}catch(err){}
			
			if(txtschoolCount > 1 && schoolDivCheck==1)
			{
				schoolId=document.getElementById("schoolIdCG").value;
				if(schoolId=='' || schoolId == 0 && hireFlag==0)
				{
					$('#errorStatusNote').show();
					$('#errorStatusNote').append("&#149; "+resourceJSON.PlzEtrSchool+".<br>");
					$('#schoolNameCG').focus();
					cnt++;
				}
			}
			var requisitionNumberShow=0;
			try{
				if(document.getElementById("requisitionNumbersGrid").style.display!='none'){
					requisitionNumberShow=1;
				}
			}catch(err){}
			var reqLength=0;
			try{
				reqLength = document.getElementById("requisitionNumber").length;
			}catch(err){}
			var restartWorkflowValue ="";
			try{
				if(statusName=="Restart Workflow"){
					var restartWorkflow  =	document.getElementsByName("restartWorkflow[]");
					var countFlow=0;
					for(var i=0; i < restartWorkflow.length; i++){
						if(restartWorkflow[i].checked == true){
							if(countFlow==0){
								restartWorkflowValue	 =	restartWorkflowValue+"#"+restartWorkflow[i].value+"#";	
							}else{
								restartWorkflowValue	 =	restartWorkflowValue+restartWorkflow[i].value+"#";
							}
							countFlow++;
						}
					}
				}
			}catch(err){}
			try{
				if((statusName=="Offer Ready" || statusName=="Hired") && isReqNoForHiring==1){
					if(rdReqSel==0 || rdReqSel==1)
					{
						if(isReqNoForHiring==1 && requisitionNumberId==0 && requisitionNumberShow==1 && hireFlag==0 && reqLength>1){
							$('#errorStatusNote').show();
							$('#errorStatusNote').append("&#149; "+resourceJSON.PlzSlctPosition+".<br>");
							cnt++;
						}else if(reqLength<=1 &&((isReqNoForHiring==1 && requisitionNumberShow==0 && requisitionNumberId==null)||(isReqNoForHiring==1 && requisitionNumberShow==1 && requisitionNumberId==0 ))){
							$('#errorStatusNote').show();
							if(offerReady.value=="Offer Ready"){
								$('#errorStatusNote').append("&#149; "+resourceJSON.MsgNoPositionFound+".<br>");
							}else{
								$('#errorStatusNote').append("&#149; "+resourceJSON.MsgPositionIsMandatory+".<br>");
							}
							cnt++;
						}
					}
					else if(rdReqSel==2)
					{
						if(isReqNoForHiring==1 && txtNewReqNo=="" && requisitionNumberShow==1 && hireFlag==0 && reqLength>1){
							$('#errorStatusNote').show();
							$('#errorStatusNote').append("&#149; "+resourceJSON.MsgEnterPosition+".<br>");
							cnt++;
						}else if(reqLength<=1 &&((isReqNoForHiring==1 && requisitionNumberShow==0 && txtNewReqNo=="")||(isReqNoForHiring==1 && requisitionNumberShow==1 && txtNewReqNo=="" ))){
							$('#errorStatusNote').show();
							if(offerReady.value=="Offer Ready"){
								$('#errorStatusNote').append("&#149; "+resourceJSON.MsgNoPositionFound+".<br>");
							}else{
								$('#errorStatusNote').append("&#149; "+resourceJSON.MsgEnterPosition+".<br>");
							}
							cnt++;
						}
					}
					
					
				}
			}catch(err){}
		}		
		
		var jobCategoryFlag=document.getElementById("jobCategoryFlag").value;
		var jobCategoryDiv=document.getElementById("jobCategoryDiv").value;
		var offerAccepted=document.getElementById("offerAccepted").value;
		var statusId =document.getElementById("statusIdForStatusNote").value;
		var offerFlag=0;
		try{
			if(offerAccepted==0 && statusId==18 && finalizeStatus==1 && cnt==0){
				offerAcceptedMsg();
				offerFlag=1;
			}
		}catch(err){}
		var entityType=$('[name="entityType"]').val();
		var recomreason = "";
		var remquestion = "";
		if(statusName == "Asst Supe Interview" && finalizeStatus==1)
		{
			if(entityType== 2)
			{
				schoolId=document.getElementById("schoolIdCG").value;
				if(schoolId=='' || schoolId == 0 && hireFlag==0)
				{

					$('#errorStatusNote').show();
					$('#errorStatusNote').append("&#149; "+resourceJSON.PlzEtrSchool+".<br>");
					$('#schoolNameCG').focus();
					cnt++;
				}
			}
			
			recomreason = $('#recomreason').find(".jqte_editor").html();
			//alert(" recomreason :: "+recomreason);
			remquestion = $('#remquestion').find(".jqte_editor").html();
			//	alert(" remquestion :: "+remquestion);
		}
		
		if($('#isSchoolOnJobId').val()==1 && $('#isRequiredSchoolIdCheck').val()==1 && finalizeStatus==1 && entityType == 2){
			schoolId=document.getElementById("schoolIdCG").value;
			//alert("schoolId========"+schoolId);
			if(schoolId=='' || schoolId == 0 )
			{
				$('#errorStatusNote').show();
				$('#errorStatusNote').append("&#149; "+resourceJSON.PlzEtrSchool+".<br>");
				$('#schoolNameCG').focus();
				cnt++;
			}
		}
		
		
		//added for school
		var countiuneDivShowOrNot=0;
		try{
			countiuneDivShowOrNot=$('#evaluationCompleteDiv').val();
			//alert("countiuneDivShowOrNot==============="+countiuneDivShowOrNot);
		}catch(e){}
		if(countiuneDivShowOrNot == 1 && finalizeStatus==1 && cnt==0 &&  minOneScoreSliderSetFlag==2 && $('#isSchoolOnJobId').val() == 0 && $('#isRequiredSchoolIdCheck').val() == 1 && entityType == 2 && statusName == "Evaluation Complete"){
		cnt++;
		var confirmMessage=""+resourceJSON.MsgContinueWithoutSchool+"";
		getConfirm($('#jWTeacherStatusNotesDiv .modal-dialog #myModalLabel #status_title').html(),confirmMessage,'Continue',function(data){
		if(data){
		    saveStatusNote(finalizeStatus);	
		}
		});	
		}
		//ended
		
		
		
		var disableSlider ='';	
		var disableTopSlider ='';	
		try{
			disableSlider=$("#ifrmQuestion_1").contents().find('.ui-slider-wrapper.ui-widget.horizontal').attr('aria-disabled');
		}catch(e){}
		try{
			disableTopSlider=$("#ifrmStatusNote").contents().find('.ui-slider-wrapper.ui-widget.horizontal').attr('aria-disabled');
		}catch(e){}
		var flagforstatusFlag=true;
		if(minOneScoreSliderSetOrNotFlag==2 && statusFlag && $('#statusNotes').find(".jqte_editor").text().trim()==""){
			$('#jWTeacherStatusNotesDiv').modal('show');
			minOneScoreSliderSetOrNotFlag=0;
			flagforstatusFlag=false;
			cnt++;
		} 
		else if(minOneScoreSliderSetOrNotFlag!=0){cnt=0;}
		//alert("iCountScoreSlider=="+iCountScoreSlider+" answerFinalize=="+answerFinalize);
		//alert("provided score=="+scoreProvided+" disableSlider=="+disableSlider+"    minOneScoreSliderSetOrNotFlag=="+minOneScoreSliderSetOrNotFlag +"  disableTopSlider=="+disableTopSlider);
		if(finalizeStatus==1 &&  flagforstatusFlag && statusName.toLowerCase()!='jsi' && ((scoreProvided==0.0 && disableSlider!='undefined' && disableSlider!='' && disableSlider=='false' && iCountScoreSlider>0 &&  answerFinalize>0) || (scoreProvided==0.0 && disableTopSlider!='undefined' && disableTopSlider!='' && disableTopSlider=='false' && iCountScoreSlider==0 &&  answerFinalize==0))){
			if(minOneScoreSliderSetOrNotFlag==0){
				cnt++;
			//var confirmMessage="We found that the score of one or more questions is set to 0 (zero). Click on \"Ok\" button if you have intentionally set the 0 score, it will set 0 as the score for such questions otherwise click on \"Cancel\" button to change the score.";
				var confirmMessage="<ul><p style='margin-left:-15px;'>"+resourceJSON.MsgMoreAttribute+".</p>"+
				"<li>"+resourceJSON.MsgIntentionallySet+".</li>"+
				"<li> "+resourceJSON.MsgDontWantToScore+".</li>"+
				"<li>"+resourceJSON.MsgClickOnCancelButton+".</li></ul>";
			getConfirmScoreSetOrNot($('#jWTeacherStatusNotesDiv .modal-dialog #myModalLabel #status_title').html(),confirmMessage,resourceJSON.MsgScore,resourceJSON.MsgDontScore,function(data){
			if(data){
				//if(minOneScoreSliderSetOrNotFlag==1 || minOneScoreSliderSetOrNotFlag==2){
			// if(confirm('You only have to set '+totalSetScoreCount+'
			// Score that you want to save anyway')){
				//alert("finalizeStatus==="+finalizeStatus);
			    saveStatusNote(finalizeStatus);	
					
				
			}
			});
			}
			}
		
		
		
		var jobCFlag=0;
		
		//alert('jobCategoryDiv::'+jobCategoryDiv);
		//alert("jobCategoryFlag:::"+jobCategoryFlag);
		//alert("finalizeStatus:::"+finalizeStatus);
		try{
			if(jobCategoryDiv==2 && jobCategoryFlag!=2  && cnt==0 && finalizeStatus==1){
				jobCFlag=1;
				$('#jWTeacherStatusNotesDiv').modal('hide');
				$('#updateStatusModal').modal('show');
			}
		}catch(err){}
		
		//Only for Jeffco Start Date
		var startDate="";
		try{
		if($("[name='slcStartDateOnOrOff']").val()=="ON"){
		startDate=$('[name="startDate"]').val();
		if(finalizeStatus==1 && $("#districtId").val()=='804800' && $("#statusName").val()=='Offer Made' && startDate==''){
			$('#errorStatusNote').show();
			$('#errorStatusNote').append("&#149; "+resourceJSON.PlzEtrSTDateMSG+".<br>");
			$('[name="startDate"]').focus();
			$('#divTxtShowData2CG').css('display','none');
			cnt++;
		}
		}
		}catch(e){}
		//Ended only for jeffco Start Date
		
		var entityType=$('[name="entityType"]').val();
		var statusName = document.getElementById("statusName").value;
		var waivedstatus = "";
		if(statusName==resourceJSON.lblSPStatus && (entityType==5 || entityType==6)){
			waivedstatus = $('#waivedReasonNote option:selected').text();
			statusNotes = waivedstatus;
			if(statusNotes=='Select Reasons'){
				$('#errorStatusNote').show();
				$('#errorStatusNote').append("&#149; "+resourceJSON.PlzEtrWaivedReason+".<br>");
				$('[name="startDate"]').focus();
				$('#divTxtShowData2CG').css('display','none');
				cnt++;
			}
			
		}
		
		var isOCPCChecked = true;	
		//alert(finalizeStatus);
		//alert("offerFlag=="+offerFlag+" cnt=="+cnt+" jobCFlag=="+jobCFlag);
		if(offerFlag==0 && cnt==0 && jobCFlag==0){	
		if(cnt==0){
			var minOneScoreSliderSetOrNotFlagTemp=minOneScoreSliderSetOrNotFlag;// add by Ram Nath
			//alert("minOneScoreSliderSetOrNotFlag=============================================----------------"+minOneScoreSliderSetOrNotFlag);
			try{
				if(statusName=="Offer Ready"){
					try{
						ManageStatusAjax.checkOCPCStatus(jobForTeacherId,requisitionNumberId,
						{
							async:false,
							errorHandler:handleError,
							callback:function(data)
							{
								if(data=="oc")
								{
									$('#errorStatusNote').show();
									$('#errorStatusNote').append("&#149; "+resourceJSON.MsgApplicantIsNotApplicable+" personnel@dadeschools.net.<br>");
									isOCPCChecked = false;
								}else if(data=="pc")
								{
									$('#errorStatusNote').show();
									$('#errorStatusNote').append("&#149; "+resourceJSON.MsgApplicantIsNotApplicableForAnyEmployment+" personnel@dadeschools.net.<br>");
									isOCPCChecked = false;
								}
							}
						});
					}catch(err){}
				}
				if(isOCPCChecked){
					$('#loadingDiv').show();
					if(statusNoteFileName!="" && statusNoteFileName!=null){
						document.getElementById("frmStatusNote").submit();
					}else{
						minOneScoreSliderSetFlag=2;// add by Ram Nath
						var sUrl = window.location.pathname;
						if(sUrl.indexOf("candidategridtest.do") > -1){
							ManageStatusAjax.saveStatusNoteOp(jobForTeacherId,fitScore,teacherStatusNoteId,teacherId,jobId,statusId,secondaryStatusId,setHiredDate,statusNotes,statusNoteFileName,emailSentToD,emailSentToS,scoreProvided,finalizeStatus,bEmailSendFor_DA_SA,bEmailSendFor_CA,answerId_arrary,answerScore_arrary,isEmailTemplateChanged,statusShortName,msgSubject,$('#mailBody').find(".jqte_editor").html(),txtschoolCount,schoolId,isEmailTemplateChangedTeacher,msgSubjectTeacher,$('#mailBodyTeacher').find(".jqte_editor").html(),chkOverride,requisitionNumberId,qqNoteIdsvalue_str,questionAssessmentIds,jobCategoryDiv,rdReqSel,txtNewReqNo,strength,growthOpp,recomreason,remquestion,minOneScoreSliderSetOrNotFlagTemp,superAdminClickOnEditNote,restartWorkflowValue,startDate,msgSubjectForTeacher,msgBodyForTeacher,isTemplateFound,timingforDclWrdw,reasonforDclWrdw,
									{
										async:false,
										errorHandler:handleError,
										callback:function(data)
										{
										superAdminClickOnEditNote=0;
										minOneScoreSliderSetOrNotFlag=0;// add by Ram Nath
											document.getElementById("scoreProvided").value="";
											
											var ShowTalentKSNtxtId=0;
											try { ShowTalentKSNtxtId=document.getElementById("ShowTalentKSNtxtId").value;} catch (e) {}
											if(ShowTalentKSNtxtId==1)
											{
												getTalentKSNDetail();
												jWTeacherStatusNotesOnOpen(fitScore,statusId,secondaryStatusId);
												$('#statusNotes').find(".jqte_editor").html("");
												$("#noteMainDiv").hide();
											}
											else if(data.split("##")[0]==100)
											{
												$('#loadingDiv').hide();
												
												$('#jWTeacherStatusNotesDiv').modal('hide');
												document.getElementById("noteMainDiv").style.display="none";
												document.getElementById("scoreProvided").value="";
												$('#err100msg').html(data.split("##")[1]);
												$('#modalErr100').modal('show');
												
											}else if(data.split("##")[0]==101)
											{
												$('#loadingDiv').hide();
												jWTeacherStatusNotesOnClose();
												var statusUpdateFromTP=0;
												try { statusUpdateFromTP=document.getElementById("statusUpdateFromTP").value;} catch (e) {}
												if(statusUpdateFromTP=="0")
												{
													try{
														if(data.split("##")[4]==1){
															$('#message2showConfirm').html("Would you like to invite applicants to an event? ");
															$('#footerbtn').html("<button class='btn btn-primary' onclick=\"redirectToEventPage("+jobId+","+teacherId+")\" >Yes</button> <button class='btn' data-dismiss='modal' aria-hidden='true'>No</button>");
															$('#myModal3').modal('show');
														}else{
															getCandidateGrid();
															refreshStatus();
														}
													}catch(e){getCandidateGrid();
															  refreshStatus();
													}
												}
											}else if(data.split("##")[0]==99)
											{
												$('#loadingDiv').hide();
												$('#errorStatusNote').show();
												$('#errorStatusNote').append("&#149; "+resourceJSON.MsgPositionIsAttached+"");
											}
											else if(data.split("##")[0]==3){
												
												if(document.getElementById("panelOpenFlag"))
												{
													getStatusForPanel();
												}else
												{
													var statusUpdateFromTP=0;
													try { statusUpdateFromTP=document.getElementById("statusUpdateFromTP").value;} catch (e) {}
													if(statusUpdateFromTP=="0")
													{
														try{
															if(data.split("##")[4]==1){
																$('#message2showConfirm').html("Would you like to invite applicants to an event? ");
																$('#footerbtn').html("<button class='btn btn-primary' onclick=\"redirectToEventPage("+jobId+","+teacherId+")\" >Yes</button> <button class='btn' data-dismiss='modal' aria-hidden='true'>No</button>");
																$('#myModal3').modal('show');
															}else{
																getCandidateGrid();
																refreshStatus();
															}
														}catch(e){getCandidateGrid();
																  refreshStatus();
														 }
													}
												}

												$('#loadingDiv').hide();
												if(refChk == "refChk"){
													jWTeacherStatusNotesOnCloseForRef();
												} else {
													jWTeacherStatusNotesOnClose();
												}
												document.getElementById("noteMainDiv").style.display="none";
												$('#statusNotes').find(".jqte_editor").html("");	
												document.getElementById("teacherStatusNoteId").value=0;
												document.getElementById("showStatusNoteFile").innerHTML="";
												
												if(data.split("##")[3]==1){
													jWTeacherStatusNotesOnClose();
													offerReadyMsg(1,resourceJSON.MsgSuccessfullySetTheStatus+".");
												}
											}else{
												$('#loadingDiv').hide();
												document.getElementById("noteMainDiv").style.display="none";
												$('#statusNotes').find(".jqte_editor").html("");	
												document.getElementById("teacherStatusNoteId").value=0;
												document.getElementById("showStatusNoteFile").innerHTML="";
												if(data.split("##")[0]==2){
													jWTeacherStatusNotesOnOpen(fitScore,statusId,secondaryStatusId);
													$('#errorStatusNote').show();
													$('#errorStatusNote').append("&#149;  "+resourceJSON.MsgCannotHiredNewCandidate+".");
												}else if(data.split("##")[0]==4){
													jWTeacherStatusNotesOnOpen(fitScore,statusId,secondaryStatusId);
													$('#errorStatusNote').show();
													$('#errorStatusNote').append("&#149; "+resourceJSON.MsgCannotUnHired+".");
												}else if(data.split("##")[0]==5){
													jWTeacherStatusNotesOnOpen(fitScore,statusId,secondaryStatusId);
													$('#errorStatusNote').show();
													$('#errorStatusNote').append("&#149; "+resourceJSON.PlzsetStatus+" "+data.split("##")[2]+" "+resourceJSON.MsgGreenToSetStatus+".");
												}else if(data.split("##")[1]!='null'){
													jWTeacherStatusNotesOnOpen(fitScore,statusId,secondaryStatusId);
													$('#errorStatusNote').show();
													$('#errorStatusNote').append("&#149; "+data.split("##")[1]+".<br>");
												}else{
													jWTeacherStatusNotesOnClose();
													if(data.split("##")[3]==1){
														jWTeacherStatusNotesOnClose();
														offerReadyMsg(1,resourceJSON.MsgSuccessfullySetTheStatus+".");
													}
												}
											}								
											// strength,growthOpp
											for(var i=0;i<strength.length;i++)
											{
												var str = strength.indexOf(strength[i]);
												if(str != -1) {
													strength.splice(str,1);
												}
											}
											for(var j=0;j<growthOpp.length;j++)
											{
												var st = growthOpp.indexOf(growthOpp[i]);
												if(st != -1) {
													growthOpp.splice(st,1);
												}
											}
										}	
									
									});
						} else {
							//shriram
							ManageStatusAjax.saveStatusNote(jobForTeacherId,fitScore,teacherStatusNoteId,teacherId,jobId,statusId,secondaryStatusId,setHiredDate,statusNotes,statusNoteFileName,emailSentToD,emailSentToS,scoreProvided,finalizeStatus,bEmailSendFor_DA_SA,bEmailSendFor_CA,answerId_arrary,answerScore_arrary,isEmailTemplateChanged,statusShortName,msgSubject,$('#mailBody').find(".jqte_editor").html(),txtschoolCount,schoolId,isEmailTemplateChangedTeacher,msgSubjectTeacher,$('#mailBodyTeacher').find(".jqte_editor").html(),chkOverride,requisitionNumberId,qqNoteIdsvalue_str,questionAssessmentIds,jobCategoryDiv,rdReqSel,txtNewReqNo,strength,growthOpp,recomreason,remquestion,minOneScoreSliderSetOrNotFlagTemp,superAdminClickOnEditNote,restartWorkflowValue,startDate,msgSubjectForTeacher,msgBodyForTeacher,isTemplateFound,timingforDclWrdw,reasonforDclWrdw,
									{
										async:false,
										errorHandler:handleError,
										callback:function(data)
										{
										superAdminClickOnEditNote=0;
										minOneScoreSliderSetOrNotFlag=0;// add by Ram Nath
											document.getElementById("scoreProvided").value="";
											
											var ShowTalentKSNtxtId=0;
											try { ShowTalentKSNtxtId=document.getElementById("ShowTalentKSNtxtId").value;} catch (e) {}
											if(ShowTalentKSNtxtId==1)
											{
												getTalentKSNDetail();
												jWTeacherStatusNotesOnOpen(fitScore,statusId,secondaryStatusId);
												$('#statusNotes').find(".jqte_editor").html("");
												$("#noteMainDiv").hide();
											}
											else if(data.split("##")[0]==100)
											{
												$('#loadingDiv').hide();
												
												$('#jWTeacherStatusNotesDiv').modal('hide');
												document.getElementById("noteMainDiv").style.display="none";
												document.getElementById("scoreProvided").value="";
												//$('#myModalStatus').modal('show');	
												$('#err100msg').html(data.split("##")[1]);
												$('#modalErr100').modal('show');
												
												//alert(data.split("##")[1]);
												/*jWTeacherStatusNotesOnClose();
												var statusUpdateFromTP=0;
												try { statusUpdateFromTP=document.getElementById("statusUpdateFromTP").value;} catch (e) {}
												if(statusUpdateFromTP=="0")
												{
													try{
														if(data.split("##")[4]==1){
															$('#message2showConfirm').html("Would you like to invite applicants to an event? ");
															$('#footerbtn').html("<button class='btn btn-primary' onclick=\"redirectToEventPage("+jobId+","+teacherId+")\" >Yes</button> <button class='btn' data-dismiss='modal' aria-hidden='true'>No</button>");
															$('#myModal3').modal('show');
														}else{
															getCandidateGrid();
															refreshStatus();
														}
													}catch(e){getCandidateGrid();
															  refreshStatus();
													 }
												}
													getCandidateGrid();
													refreshStatus();
												}*/
											}else if(data.split("##")[0]==101)
											{
												$('#loadingDiv').hide();
												jWTeacherStatusNotesOnClose();
												var statusUpdateFromTP=0;
												try { statusUpdateFromTP=document.getElementById("statusUpdateFromTP").value;} catch (e) {}
												if(statusUpdateFromTP=="0")
												{
													try{
														if(data.split("##")[4]==1){
															$('#message2showConfirm').html("Would you like to invite applicants to an event? ");
															$('#footerbtn').html("<button class='btn btn-primary' onclick=\"redirectToEventPage("+jobId+","+teacherId+")\" >Yes</button> <button class='btn' data-dismiss='modal' aria-hidden='true'>No</button>");
															$('#myModal3').modal('show');
														}else{
															getCandidateGrid();
															refreshStatus();
														}
													}catch(e){getCandidateGrid();
															  refreshStatus();
													}
												}
											}else if(data.split("##")[0]==99)
											{
												$('#loadingDiv').hide();
												$('#errorStatusNote').show();
												$('#errorStatusNote').append("&#149; "+resourceJSON.MsgPositionIsAttached+"");
											}
											else if(data.split("##")[0]==3){
												
												if(document.getElementById("panelOpenFlag"))
												{
													//window.location.href="";
													getStatusForPanel();
												}else
												{
													var statusUpdateFromTP=0;
													try { statusUpdateFromTP=document.getElementById("statusUpdateFromTP").value;} catch (e) {}
													if(statusUpdateFromTP=="0")
													{
														try{
															if(data.split("##")[4]==1){
																$('#message2showConfirm').html("Would you like to invite applicants to an event? ");
																$('#footerbtn').html("<button class='btn btn-primary' onclick=\"redirectToEventPage("+jobId+","+teacherId+")\" >Yes</button> <button class='btn' data-dismiss='modal' aria-hidden='true'>No</button>");
																$('#myModal3').modal('show');
															}else{
																getCandidateGrid();
																refreshStatus();
															}
														}catch(e){getCandidateGrid();
																  refreshStatus();
														 }
													}
												}

												$('#loadingDiv').hide();
												if(refChk == "refChk"){
													jWTeacherStatusNotesOnCloseForRef();
												} else {
													jWTeacherStatusNotesOnClose();
												}
												//jWTeacherStatusNotesOnOpen(fitScore,statusId,secondaryStatusId);
												document.getElementById("noteMainDiv").style.display="none";
												$('#statusNotes').find(".jqte_editor").html("");	
												document.getElementById("teacherStatusNoteId").value=0;
												document.getElementById("showStatusNoteFile").innerHTML="";
												
												if(data.split("##")[3]==1){
													jWTeacherStatusNotesOnClose();
													offerReadyMsg(1,resourceJSON.MsgSuccessfullySetTheStatus+".");
												}
											}else{
												$('#loadingDiv').hide();
												document.getElementById("noteMainDiv").style.display="none";
												$('#statusNotes').find(".jqte_editor").html("");	
												document.getElementById("teacherStatusNoteId").value=0;
												document.getElementById("showStatusNoteFile").innerHTML="";
												if(data.split("##")[0]==2){
													jWTeacherStatusNotesOnOpen(fitScore,statusId,secondaryStatusId);
													$('#errorStatusNote').show();
													$('#errorStatusNote').append("&#149;  "+resourceJSON.MsgCannotHiredNewCandidate+".");
												}else if(data.split("##")[0]==4){
													jWTeacherStatusNotesOnOpen(fitScore,statusId,secondaryStatusId);
													$('#errorStatusNote').show();
													$('#errorStatusNote').append("&#149; "+resourceJSON.MsgCannotUnHired+".");
												}else if(data.split("##")[0]==5){
													jWTeacherStatusNotesOnOpen(fitScore,statusId,secondaryStatusId);
													$('#errorStatusNote').show();
													$('#errorStatusNote').append("&#149; "+resourceJSON.PlzsetStatus+" "+data.split("##")[2]+" "+resourceJSON.MsgGreenToSetStatus+".");
												}else if(data.split("##")[1]!='null'){
													jWTeacherStatusNotesOnOpen(fitScore,statusId,secondaryStatusId);
													$('#errorStatusNote').show();
													$('#errorStatusNote').append("&#149; "+data.split("##")[1]+".<br>");
												}else{
													jWTeacherStatusNotesOnClose();
													if(data.split("##")[3]==1){
														jWTeacherStatusNotesOnClose();
														offerReadyMsg(1,resourceJSON.MsgSuccessfullySetTheStatus+".");
													}
												}
											}								
											// strength,growthOpp
											for(var i=0;i<strength.length;i++)
											{
												var str = strength.indexOf(strength[i]);
												if(str != -1) {
													strength.splice(str,1);
												}
											}
											for(var j=0;j<growthOpp.length;j++)
											{
												var st = growthOpp.indexOf(growthOpp[i]);
												if(st != -1) {
													growthOpp.splice(st,1);
												}
											}
										}	
									
									});
						}
					}
				}
			}catch(err){
				minOneScoreSliderSetFlag=2;// add by Ram Nath
				minOneScoreSliderSetOrNotFlag = 0;// add by Ram Nath
				superAdminClickOnEditNote=0;
			}
		}
	}
		}// add By Ram Nath
	},170);
	$('#loadingDiv').hide();
}
function saveStatusNoteFile(statusNoteDateTime)
{
	var jobCategoryDiv=document.getElementById("jobCategoryDiv").value;
	var txtPanel=document.getElementById("txtPanel").value;
	var txtOverride=document.getElementById("txtOverride").value;
	var txtLoggedInUserPanelAttendees=document.getElementById("txtLoggedInUserPanelAttendees").value;
	var iQdslider=document.getElementById("iQdslider").value;
	var chkOverride=false;
	if(txtOverride==1)
	{
		chkOverride=document.getElementById('chkOverride').checked;
	}
	
	var rdReqSel=0;
	try { 
		if($("input[name=rdReqSel]").length>0)
			rdReqSel=$("input[name=rdReqSel]:checked").val(); 
		
	}catch (e) {}
	var txtNewReqNo="";
	if(rdReqSel==2)
		try { txtNewReqNo=trim(document.getElementById("txtNewReqNo").value); } catch (e) {}
		
	var questionAssessmentIds = document.getElementById('questionAssessmentIds').value;
	var qqNoteIds = document.getElementById('qqNoteIds').value;
	var qqNoteIdsvalue_arr = qqNoteIds.split("#");
	var qqNoteIdsvalue_str ="";
	
	if(qqNoteIds!=null && qqNoteIds!="")
	{
		for(var i=0;i<qqNoteIdsvalue_arr.length;i++)
		{
			if(qqNoteIdsvalue_str!="")
				qqNoteIdsvalue_str = qqNoteIdsvalue_str+"#"+$('#'+qqNoteIdsvalue_arr[i]).find(".jqte_editor").html();
			else
				qqNoteIdsvalue_str = $('#'+qqNoteIdsvalue_arr[i]).find(".jqte_editor").html();
		}
	}
	/*====== Gagan [ Start ] : variable for status wise email templates ================*/
	var isEmailTemplateChanged	=	$('#isEmailTemplateChanged').val();
	var statusShortName	=	$("#statusShortName").val(statusShortName);
	var msgSubject	=	$('#subjectLine').val();
	//var charCount	=	$('#mailBody').find(".jqte_editor").text();
	var isEmailTemplateChangedTeacher	=	$('#isEmailTemplateChangedTeacher').val();
	var msgSubjectTeacher	=	$('#subjectLineTeacher').val();
	
	var isTemplateFound = false;
	if($("#finelizeSpecificTemplatesName option:selected").val()!="")
		   isTemplateFound = true;
	
	
	/*====== Gagan [ END ] : variable for status wise email templates ================*/
	// Start for save Question score
	
	try{
		var iframeNorm = document.getElementById('ifrmStatusNote');
		var innerNorm = iframeNorm.contentDocument || iframeNorm.contentWindow.document;
		var inputNormScore = innerNorm.getElementById('statusNoteFrm');
		document.getElementById("scoreProvided").value=inputNormScore.value;
	}catch(e){}	
	var scoreProvided=trim(document.getElementById("scoreProvided").value);
	
	var requisitionNumberId=null;
	try{  
		requisitionNumberId=document.getElementById("requisitionNumber").value;
	}catch(e){}
	
	var answerId_arrary = new Array();
	var answerScore_arrary = new Array();
	
	
	//@AShish :: define questionsDetails and questionAssessmentIds
	var questionAssessmentIds = document.getElementById('questionAssessmentIds').value;
	
	var answerFinalize=0;
	var sumAnswerScore=0;
	var sumAnswerScore=0;
	var iCountScoreSlider=document.getElementById("iCountScoreSlider").value;
	if(iCountScoreSlider>0)
	{
		for(var i=1;i<= iCountScoreSlider;i++)
		{
			var questionscore=0;
			var answerId=0;
			
			try{
				var ifrmQuestion = document.getElementById('ifrmQuestion_'+i);
				if(ifrmQuestion!=null)
				{
					var innerQuestion = ifrmQuestion.contentDocument || ifrmQuestion.contentWindow.document;
					var inputQuestionFrm = innerQuestion.getElementById('questionFrm');
					var answerIdFrm = innerQuestion.getElementById('answerId');
					questionscore=inputQuestionFrm.value;
					answerId=answerIdFrm.value;
					
					sumAnswerScore=(parseInt(sumAnswerScore)+parseInt(questionscore));
					
					answerId_arrary[i-1]=answerId;
					answerScore_arrary[i-1]=questionscore;
					
					if(questionscore==0)
					{
						answerFinalize++;
					}
				}
			}catch(e){}	
		}
	}
	var iCountScoreSlider=document.getElementById("iCountScoreSlider").value;
	var isQuestionEnable=document.getElementById("isQuestionEnable").value;
	// End for save Question score
	
	
	var jobForTeacherId =document.getElementById("jobForTeacherIdForSNote").value;
	var fitScore=document.getElementById("fitScoreForStatusNote").value;
	var statusNotes = $('#statusNotes').find(".jqte_editor").html();
	var teacherId = document.getElementById("statusTeacherId").value;
	var jobId = document.getElementById("jobId").value;
	var emailSentToD =0;// document.getElementById("emailSentToD").checked;
	var emailSentToS =0;// document.getElementById("emailSentToS").checked;
	var statusId=document.getElementById("statusIdForStatusNote").value;
	var secondaryStatusId=document.getElementById("secondaryStatusIdForStatusNote").value;
	var finalizeStatus=document.getElementById("finalizeStatus").value;
	var teacherStatusNoteId=document.getElementById("teacherStatusNoteId").value;
	var statusNoteFileName=null;
	try{
		statusNoteFileName=document.getElementById("statusNoteFileName").value;
	}catch(e){}
	var scoreProvided=trim(document.getElementById("scoreProvided").value);
	var sNoteFileName="";
	var bEmailSendFor_DA_SA=document.getElementById('email_da_sa').checked;
	var bEmailSendFor_CA=false;//document.getElementById('email_ca').checked;
	
	//Gaurav Kumar
	var msgSubjectForTeacher =   $('#finelizeSpecificSubjectLine').val();
	var msgBodyForTeacher =   $("#finelizeSpecificMessage").find(".jqte_editor").html();
	
	var txtschoolCount=0;
	var schoolId=0;
	if(finalizeStatus==1)
	{
		try{
			txtschoolCount=document.getElementById("txtschoolCount").value;
		}catch(err){}
		if(txtschoolCount > 1)
		{
			try{
				schoolId=document.getElementById("schoolIdCG").value;
			}catch(err){}
		}
	}
	
	var statusName = document.getElementById("statusName").value;
	var setHiredDate ="";
	if(statusName == "Hired"){
		setHiredDate = document.getElementById("setHiredDate").value;
		if(setHiredDate == "" || setHiredDate == null){
			$('#errorStatusNote').show();
			$('#errorStatusNote').append("&#149; "+resourceJSON.PlzSelectHireddate+".<br>");
			return false;
		}
	} else {
		setHiredDate = document.getElementById("setHiredDate").value;
		setHiredDate = "";
	}
	try{
		if(statusNoteFileName!="" && statusNoteFileName!=null)
		{
			var ext = statusNoteFileName.substr(statusNoteFileName.lastIndexOf('.') + 1).toLowerCase();	
			sNoteFileName="statusNote"+statusNoteDateTime+"."+ext;
		}
		
		var statusName = document.getElementById("statusName").value;
		var setHiredDate ="";
		var strength =  [];
		var growthOpp =  [];
		if(finalizeStatus==1 )
		{
			if(statusName == "Asst Supe Interview")
			{
				for(var i=0;i<6;i++)
				{
						if($('#growth'+i).val()!=undefined && $('#growth'+i).val()!=null)
						{
							//alert($('#growth'+i).val());
							growthOpp.push("growth"+i+"@@@"+$('#growth'+i).val());
						}
						if($('#strength'+i).val()!=undefined && $('#strength'+i).val()!=null)
						{
							//alert($('#strength'+i).val());
							strength.push("strength"+i+"@@@"+$('#strength'+i).val());
						}
				}
			}
		}
		var recomreason = "";
		var remquestion = "";
		if(statusName == "Asst Supe Interview" && finalizeStatus==1)
		{
			if(entityType.value == 2)
			{
				schoolId=document.getElementById("schoolIdCG").value;
				if(schoolId=='' || schoolId == 0 && hireFlag==0)
				{

					$('#errorStatusNote').show();
					$('#errorStatusNote').append("&#149; "+resourceJSON.PlzEtrSchool+".<br>");
					$('#schoolNameCG').focus();
					cnt++;
				}
			}
			
			recomreason = $('#recomreason').find(".jqte_editor").html();
			//alert(" recomreason :: "+recomreason);
			remquestion = $('#remquestion').find(".jqte_editor").html();
			//	alert(" remquestion :: "+remquestion);
		}
		
		var minOneScoreSliderSetOrNotFlagTemp = minOneScoreSliderSetOrNotFlag;// add by Ram Nath
		//Only for Jeffco Start Date
		var startDate="";
		try{
			if($("[name='slcStartDateOnOrOff']").val()=="ON"){
				startDate=$('[name="startDate"]').val();
			}
		}catch(e){}
		
		var restartWorkflowValue =""	;
		try{
			if(statusName=="Restart Workflow"){
				var restartWorkflow  =	document.getElementsByName("restartWorkflow[]");
				var countFlow=0;
				for(var i=0; i < restartWorkflow.length; i++){
					if(restartWorkflow[i].checked == true){
						if(countFlow==0){
							restartWorkflowValue	 =	restartWorkflowValue+"#"+restartWorkflow[i].value+"#";	
						}else{
							restartWorkflowValue	 =	restartWorkflowValue+restartWorkflow[i].value+"#";
						}
						countFlow++;
					}
				}
			}
		}catch(err){}

		ManageStatusAjax.saveStatusNote(jobForTeacherId,fitScore,teacherStatusNoteId,teacherId,jobId,statusId,secondaryStatusId,setHiredDate,statusNotes,sNoteFileName,emailSentToD,emailSentToS,scoreProvided,finalizeStatus,bEmailSendFor_DA_SA,bEmailSendFor_CA,answerId_arrary,answerScore_arrary,isEmailTemplateChanged,statusShortName,msgSubject,$('#mailBody').find(".jqte_editor").html(),txtschoolCount,schoolId,isEmailTemplateChangedTeacher,msgSubjectTeacher,$('#mailBodyTeacher').find(".jqte_editor").html(),chkOverride,requisitionNumberId,qqNoteIdsvalue_str,questionAssessmentIds,jobCategoryDiv,rdReqSel,txtNewReqNo,strength,growthOpp,recomreason,remquestion,minOneScoreSliderSetOrNotFlagTemp,superAdminClickOnEditNote,restartWorkflowValue,startDate,msgSubjectForTeacher,msgBodyForTeacher,isTemplateFound,timingforDclWrdw,reasonforDclWrdw,{
				async:false,
				errorHandler:handleError,
				callback:function(data)
				{
					document.getElementById("scoreProvided").value="";
					superAdminClickOnEditNote=0;
					minOneScoreSliderSetOrNotFlag = 0;// add by Ram Nath
					minOneScoreSliderSetFlag=2;// add by Ram Nath
					if(data.split("##")[0]==99)
					{
						$('#loadingDiv').hide();
						$('#errorStatusNote').show();
						$('#errorStatusNote').append("&#149; "+resourceJSON.MsgPositionIsAttached+"");
					}
					else if(data.split("##")[0]==3){
						if(document.getElementById("panelOpenFlag"))
						{
							getStatusForPanel();
						}else
						{
							var statusUpdateFromTP=0;
							try { statusUpdateFromTP=document.getElementById("statusUpdateFromTP").value;} catch (e) {}
							if(statusUpdateFromTP=="0")
							{
								getCandidateGrid();
								refreshStatus();
							}
						}
						
						$('#loadingDiv').hide();
						//jWTeacherStatusNotesOnOpen(fitScore,statusId,secondaryStatusId);
						jWTeacherStatusNotesOnClose();
						document.getElementById("noteMainDiv").style.display="none";
						document.getElementById("teacherStatusNoteId").value=0;
						$('#statusNotes').find(".jqte_editor").html("");
						document.getElementById("showStatusNoteFile").innerHTML="";
						removeStatusNoteFile();
						if(data.split("##")[3]==1){
							jWTeacherStatusNotesOnClose();
							offerReadyMsg(1,resourceJSON.MsgSuccessfullySetTheStatus+".");
						}
					}else{
						$('#loadingDiv').hide();
						document.getElementById("noteMainDiv").style.display="none";
						$('#statusNotes').find(".jqte_editor").html("");	
						document.getElementById("teacherStatusNoteId").value=0;
						document.getElementById("showStatusNoteFile").innerHTML="";
						if(data.split("##")[0]==2){
							jWTeacherStatusNotesOnOpen(fitScore,statusId,secondaryStatusId);
							$('#errorStatusNote').show();
							$('#errorStatusNote').append("&#149;  "+resourceJSON.MsgCannotHiredNewCandidate+".");
						}else if(data.split("##")[0]==4){
							jWTeacherStatusNotesOnOpen(fitScore,statusId,secondaryStatusId);
							$('#errorStatusNote').show();
							$('#errorStatusNote').append("&#149; "+resourceJSON.MsgCannotUnHired+".");
						}else if(data.split("##")[0]==5){
							jWTeacherStatusNotesOnOpen(fitScore,statusId,secondaryStatusId);
							$('#errorStatusNote').show();
							$('#errorStatusNote').append("&#149; "+resourceJSON.PlzsetStatus+" "+data.split("##")[2]+" "+resourceJSON.MsgGreenToSetStatus+".");
						}else if(data.split("##")[1]!='null'){
							jWTeacherStatusNotesOnOpen(fitScore,statusId,secondaryStatusId);
							$('#errorStatusNote').show();
							$('#errorStatusNote').append("&#149; "+data.split("##")[1]+".<br>");
						}else{
							jWTeacherStatusNotesOnClose();
							if(data.split("##")[3]==1){
								jWTeacherStatusNotesOnClose();
								offerReadyMsg(1,resourceJSON.MsgSuccessfullySetTheStatus+".");
							}
						}
					}
					
					// strength,growthOpp
					for(var i=0;i<strength.length;i++)
					{
						var str = strength.indexOf(strength[i]);
						if(str != -1) {
							strength.splice(str,1);
						}
					}
					for(var j=0;j<growthOpp.length;j++)
					{
						var st = growthOpp.indexOf(growthOpp[i]);
						if(st != -1) {
							growthOpp.splice(st,1);
						}
					}
				}
		});	
		}catch(err){
			minOneScoreSliderSetFlag=2;// add by Ram Nath
			minOneScoreSliderSetOrNotFlag = 0;// add by Ram Nath
			superAdminClickOnEditNote=0;
		}
}
function editShowStatusNote(teacherStatusNoteId)
{
	$('#errorStatusNote').empty();
	var fitScore=document.getElementById("fitScoreForStatusNote").value;
	ManageStatusAjax.editShowStatusNote(teacherStatusNoteId,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			document.getElementById("teacherStatusNoteId").value=data.teacherStatusNoteId;
			var fileName=null;
			var filePath=null;
			try
			{
				fileName=data.statusNoteFileName;
				filePath="statusNote/"+data.teacherDetail.teacherId;
				if(fileName!=null&& fileName!=""){
					document.getElementById("showStatusNoteFile").innerHTML="<a href=\"javascript:void(0)\" id=\"commfileforedit\" onclick=\"downloadCommunication('"+filePath+"','"+fileName+"','commfileforedit');if(this.href!='javascript:void(0);')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;\">"+fileName+"</a>&nbsp;<a href=\"#\" onclick=\"return deleteStatusNoteChk("+data.teacherStatusNoteId+",'attach')\"> "+resourceJSON.BtnRemove+"</a>";
				}else{
					document.getElementById("showStatusNoteFile").innerHTML="";
				}
			}catch(err){ alert(err);}
			
			$('#statusNotes').find(".jqte_editor").html(data.statusNotes);
			$('#statusNotes').find(".jqte_editor").focus();
			document.getElementById("noteMainDiv").style.display="inline";
			superAdminClickOnEditNote=1;
			
		}
	});
}
function deleteStatusNoteChk(teacherStatusNoteId,deleteType)
{
	$('#jWTeacherStatusNotesDiv').modal('hide');
	document.getElementById("statusNoteDeleteType").value=deleteType;
	document.getElementById("teacherStatusNoteId").value=teacherStatusNoteId;
	if(deleteType=="attach"){
			document.getElementById("statusNoteMsg").innerHTML=""+resourceJSON.MsgAreUSure+"";	
	}else{
		document.getElementById("statusNoteMsg").innerHTML=""+resourceJSON.MsgRemoveTheNote+"";
	}
	$('#modalStatusNoteMsg').modal('show');
}
function closeStatusNoteMsg(flag){
	$('#modalStatusNoteMsg').modal('hide');
	var deleteType=document.getElementById("statusNoteDeleteType").value;
	var teacherStatusNoteId=document.getElementById("teacherStatusNoteId").value;
	if(flag==1){
		superAdminClickOnEditNote=0;
		deleteStatusNote(teacherStatusNoteId,deleteType);
	}else{
		$('#jWTeacherStatusNotesDiv').modal('show');
	}
}
function deleteStatusNote(teacherStatusNoteId,deleteType)
{
	$('#loadingDiv').show();
	ManageStatusAjax.deleteStatusNote(teacherStatusNoteId,deleteType,{ 
		async: true,
		errorHandler:handleError,
		callback: function(data)
		{
			$('#loadingDiv').hide();
			document.getElementById("showStatusNoteFile").innerHTML="";
			var statusId=document.getElementById("statusIdForStatusNote").value;
			var secondaryStatusId=document.getElementById("secondaryStatusIdForStatusNote").value;
			var fitScore=document.getElementById("fitScoreForStatusNote").value;
			jWTeacherStatusNotesOnOpen(fitScore,statusId,secondaryStatusId);
			removeStatusNoteFile();
			var deleteTypeChk=document.getElementById("statusNoteDeleteType").value;
			if(deleteTypeChk=="attach"){
			}
			
			if(deleteTypeChk!="attach"){
				document.getElementById("noteMainDiv").style.display="none";
				document.getElementById("teacherStatusNoteId").value=0;
				$('#statusNotes').find(".jqte_editor").html("");
				document.getElementById("showStatusNoteFile").innerHTML="";
			}
		}});
	return false;
}



function updateStatusSpecificScore(answerId,score)
{
	ManageStatusAjax.updateStatusSpecificScore(answerId,score,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
		}
	});
}

function getStatusForPanel()
{
	//alert("@@@@");
		$('#loadingDiv').show();
		ManageStatusAjax.getStatusForPanel($("#panelId").val(),$("#userId").val(),
		{ 
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{
				setStatusTilteFor2ndDiv(data[0],data[1],data[2]);
				document.getElementById("jobForTeacherIdForSNote").value=data[6];
				jWTeacherStatusNotesOnOpen(data[3],data[4],data[5]);
				$('#loadingDiv').hide();
				//setStatusTilteFor2ndDiv('Ron Burgundy','Screening Complete','oth');jWTeacherStatusNotesOnOpen('15','16','1428');
			}
		});
}


//@Ashish :: for Get Qualification Question Note Ids
function getQuestionNotesIds()
{
	try{
		var qqNote = document.getElementById("qqNoteIds").value;
		if(qqNote!="")
		{
			var qqN = qqNote.split("#");
			for(var i=0;i<qqN.length;i++)
			{
				$('#'+qqN[i]).find(".jqte").width(662);
			}
		}
	}catch(err){}

}

function updateStatusOverrideSkip(param){
	document.getElementById("jobCategoryDiv").value=param;
	$('#updateStatusModal').modal('hide');
	jWTeacherStatusNotesOnClose();
	saveStatusNote(1);
}
function cancelUpdateStatus(){
	minOneScoreSliderSetOrNotFlag=0;
	minOneScoreSliderSetFlag=2;// add by Ram Nath
	superAdminClickOnEditNote=0;
	$('#updateStatusModal').modal('hide');
	jWTeacherStatusNotesOnClose();
}

/*	
 * Dated		:	30-07-2014
 * Edited By	:	Hanzala Subhani
 * Purpose		:	Open Transcript DIV
 * 
 * */
function applyScrollOnTblTrans()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#transTableHistory').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 150,
        width: 653,
        minWidth: null,
        minWidthAuto: false,
        colratio:[100,150,80,120,210],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
    });	
}

function applyScrollOnTblTransAcademic()
{
	var $j=jQuery.noConflict();
	    $j(document).ready(function() {
	    $j('#academicGridAcademic').fixheadertable({ //table id 
	    caption: '',
	    showhide: false,
	    theme: 'ui',
	    height: 150,
	    width: 670,
	    //width: 825,
	    minWidth: null,
	    minWidthAuto: false,
	    colratio:[30,133,120,150,150,87],
	    addTitles: false,
	    zebra: true,
	    zebraClass: 'net-alternative-row',
	    sortable: false,
	    sortedColId: null,
	    dateFormat: 'd-m-y',
	    pager: false,
	    rowsPerPage: 10,
	    resizeCol: false,
	    minColWidth: 100,
	    wrapper: false
	    });
    });	
}

function applyScrollOnTblReferenceCheck()
{
	var actionAdd=$('#actionAdd').val();	
	var colratioArray=new Array();
	 if(actionAdd==1){		 
		 		colratioArray=[25,120,185,100,50,50,90,50];
		    }else{
		    	colratioArray=[25,120,185,100,50,50,140];    	
		    }
	 	var $j=jQuery.noConflict();
	    $j(document).ready(function() {
	    $j('#referenceCheckGrid').fixheadertable({ //table id 
	    caption: '',
	    showhide: false,
	    theme: 'ui',
	    height: 150,
	    width: 670,
	    minWidth: null,
	    minWidthAuto: false,	   
	    colratio:colratioArray,//colratio:[30,150,200,120,50,120],	    
	    addTitles: false,
	    zebra: true,
	    zebraClass: 'net-alternative-row',
	    sortable: false,
	    sortedColId: null,
	    dateFormat: 'd-m-y',
	    pager: false,
	    rowsPerPage: 10,
	    resizeCol: false,
	    minColWidth: 100,
	    wrapper: false
	    });
    });
	setCorrectRowOfTable(actionAdd);  
	    
}
function setCorrectRowOfTable(actionAdd){
	if(actionAdd==1){
    	$('#referenceCheckGridm .td7').css('width','90px !important');
    	$('#referenceCheckGridm .td8').css('width','50px !important');
    }
    else{	    	
    	$('#referenceCheckGridm .td7').css('width','140px !important');	
    }
}

function applyScrollOnTblReferenceCheckCG()
{
	
	var actionAdd=$('#actionAdd').val();	
	var colratioArray=new Array();
	 if(actionAdd==1){		 
		 		colratioArray=[25,120,185,100,50,50,90,50];
		    }else{
		    	colratioArray=[25,120,185,100,50,50,140];    	
		    }
	 //alert("enter="+actionAdd+"="+colratioArray);
	var $j=jQuery.noConflict();
	    $j(document).ready(function() {
	    $j('#referenceCheckGridCG').fixheadertable({ //table id 
	    caption: '',
	    showhide: false,
	    theme: 'ui',
	    height: 150,
	    width: 670,
	    minWidth: null,
	    minWidthAuto: false,
	    colratio:colratioArray,//[30,130,170,120,50,50,120],
	    addTitles: false,
	    zebra: true,
	    zebraClass: 'net-alternative-row',
	    sortable: false,
	    sortedColId: null,
	    dateFormat: 'd-m-y',
	    pager: false,
	    rowsPerPage: 10,
	    resizeCol: false,
	    minColWidth: 100,
	    wrapper: false
	    });
    });	
	setCorrectRowOfTable(actionAdd);  
}

var pageForDT 			= 	1;
var noOfRowsForDT 		= 	10;
var sortOrderStrForDT	=	"";
var sortOrderTypeForDT	=	"";

function defaultSet(){
	pageForTC 			=	1;
	noOfRowsForTC 		=	100;
	noOfRowsForJob		=	50;
	sortOrderStrForTC	=	"";
	sortOrderTypeForTC	=	"";
	currentPageFlag		=	"";
}
function showAcademicQues(){
	var counter = 0;
	var transcript_data	=	document.getElementsByName("transcript_data[]");
	for(var i=0; i < transcript_data.length; i++){
		if(transcript_data[i].checked == true){			
			counter++;
		}
	}
	//alert("counter== "+counter);

	if(counter > 0){ 
		$('textarea').jqte();
		document.getElementById("academicGridAcademicNotes").style.display="block";
	} else {
		$('#academicGridAcademicNotes').hide();
	}
}

function showReferenceCheck(){
	var counter = 0;
	var referenceData	=	document.getElementsByName("referenceData[]");
	for(var i=0; i < referenceData.length; i++){
		if(referenceData[i].checked == true){			
			counter++;
		}
	}
	//alert("counter== "+counter);

	if(counter > 0){
		document.getElementById("statusSentRefChk").innerHTML="<button class=\"btn  btn-large btn-primary\" onclick='sendReferenceMailFromCG(1)'>Send <i class=\"iconlock\"></i></button>";
		/*$('textarea').jqte();
		document.getElementById("referenceGridSendEmail").style.display="block";*/
	} else {
		//$('#referenceGridSendEmail').hide();
		document.getElementById("statusSentRefChk").innerHTML="";
	}
}

function showReferenceCheckSLC(){
	var counter = 0;
	var referenceData	=	document.getElementsByName("referenceData[]");
	for(var i=0; i < referenceData.length; i++){
		if(referenceData[i].checked == true){			
			counter++;
		}
	}
	if(counter > 0){
		$('textarea').jqte();
		document.getElementById("referenceGridSendEmail").style.display="block";
	} else {
		$('#referenceGridSendEmail').hide();
	}
}


/*
 * 	Dated		:	31-07-2014
 * 	By			:	Hanzala Subhani
 * 	Purpose		:	Save Transcript Data
 */

function saveTransAcademic(id){
	$('#errorStatusNote').empty();
	var focus 		= 	0;
	var counter		= 	0;
	var AcademicIDs	=	"";
	var status 		= 	id;
	document.getElementById("formId").value = id;

	var teacherIdTrans		=	document.getElementById("teacherIdTrans").value
	var eligibilityIdTrans	=	document.getElementById("eligibilityIdTrans").value
	var jobIdTrans			=	document.getElementById("jobIdTrans").value

	var StatusMaster3		=	document.getElementById("StatusMaster3").value
	var split				=	StatusMaster3.split("#$#");
	var graduationDate		=	"";
	var transcript_data		=	document.getElementsByName("transcript_data[]");
	var counter;

	var teacherFName		=	document.getElementById("teacherFName").value;
	var teacherLName		=	document.getElementById("teacherLName").value;
	var secondaryStatus		=	document.getElementById("secondaryStatus").value;
	
	//var fitScore			=	document.getElementById("fitScore").value;

	var fitScore			=	document.getElementById("fitScoreForStatusNote").value;
	
	
	var statusId			=	document.getElementById("statusId").value;
	var secondaryStatusId	=	document.getElementById("secondaryStatusId").value;
	
	var fullTeacherName		=	teacherFName+" "+teacherLName;
	var statusName			=	'Ofcl Trans / Veteran Pref';
	var statusShortName		=	'oth';
	var graduationCounter	=	0;
	for(var i=0; i < transcript_data.length; i++){

		if(transcript_data[i].checked == true){
			graduationDate	=	document.getElementById("graduationDate_"+transcript_data[i].value).value;
			AcademicIDs	 =	AcademicIDs+"##"+transcript_data[i].value;
			if(id == 22 && (graduationDate == "" || graduationDate == null)){
				if(counter == 0){
					$('#errorStatusNote').append("&#149; "+resourceJSON.MsgDegreeConferredDate+".<br>");
					$("#graduationDate_"+transcript_data[i].value).focus();
					$('#errorStatusNote').show();
				}

				counter++;
				focus++;

			}else if(id == 22 && (graduationDate != "" || graduationDate != null)){
				if(counter == 0){
					saveGraduationDate(transcript_data[i].value);
					if (isDate(graduationDate)==false){
						graduationCounter	=	graduationCounter+1;
					}
				}
			}
		}
	}
	if(counter == 0 && graduationCounter == 0){
		ManageStatusAjax.saveNotesTrans(status,teacherIdTrans,eligibilityIdTrans,jobIdTrans,AcademicIDs,{
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{
				jWTeacherStatusNotesOnOpen(fitScore,statusId,secondaryStatusId);
			}});
	}
}

function saveGraduationDate(id){
	var graduationDate	=	document.getElementById("graduationDate_"+id).value;
	if (isDate(graduationDate)==false){
		//dt.focus()
		document.getElementById("graduationDate_"+id).focus();
		return false;
	} else {
		CGServiceAjax.saveGraduationDate(id,graduationDate,{
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{
				// $('.onrNotesShowTrans').modal('hide');
			}});
	}
}

function sendReferenceMail(){
	$('#loadingDivReference').show();
	$('#errorStatusNote').empty();
	$('#errorStatusNoteRef').empty();
	var focus 			= 	0;
	var counter			= 	0;
	var elerefAutoIds	=	"";

	var referenceTeacherId	=	document.getElementById("referenceTeacherId").value
	var referenceJobId		=	document.getElementById("referenceJobId").value
	var graduationDate		=	"";
	var referenceData		=	document.getElementsByName("referenceData[]");
	var counter;
	var fitScore			=	document.getElementById("fitScoreForStatusNote").value;
	var statusId			=	document.getElementById("statusId").value;
	var secondaryStatusId	=	document.getElementById("secondaryStatusId").value;
	var graduationCounter	=	0;
	var numberOfQuestion	=	0;

	for(var i=0; i < referenceData.length; i++){
		if(referenceData[i].checked == true){
			elerefAutoIds	 =	elerefAutoIds+"##"+referenceData[i].value;
			$('#loadingDivReference').hide();
		}
	}

	try{
		numberOfQuestion	=	document.getElementById("numberOfQuestion").value;
	} catch(err){
		
	}
	
	if(numberOfQuestion == 0){
		$('#errorStatusNoteRef').append("&#149; "+resourceJSON.MsgNoActiveQuestion+"<br>");
		return false;
	}
	

	//alert(elerefAutoIds);return false;

	if(counter == 0 && graduationCounter == 0){
		ManageStatusAjax.sendReferenceMail(referenceTeacherId,referenceJobId,elerefAutoIds,{
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{
				jWTeacherStatusNotesOnOpen(fitScore,statusId,secondaryStatusId);
			}});
	}
}

var dtCh= "-";
var minYear=1951;
var d = new Date();
var maxYear=d.getFullYear();

function isInteger(s){
	var i;
    for (i = 0; i < s.length; i++){   
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    // All characters are numbers.
    return true;
}

function stripCharsInBag(s, bag){
	var i;
    var returnString = "";
    // Search through string's characters one by one.
    // If character is not in bag, append to returnString.
    for (i = 0; i < s.length; i++){   
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }
    return returnString;
}

function daysInFebruary (year){
	// February has 29 days in any year evenly divisible by four,
    // EXCEPT for centurial years which are not also divisible by 400.
    return (((year % 4 == 0) && ( (!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28 );
}
function DaysArray(n) {
	for (var i = 1; i <= n; i++) {
		this[i] = 31
		if (i==4 || i==6 || i==9 || i==11) {this[i] = 30}
		if (i==2) {this[i] = 29}
   } 
   return this
}

function isDate(dtStr){
	$('#errorStatusNote').empty();
	var daysInMonth = 	DaysArray(12)
	var pos1		=	dtStr.indexOf(dtCh)
	var pos2		=	dtStr.indexOf(dtCh,pos1+1)
	var strMonth	=	dtStr.substring(0,pos1)
	var strDay		=	dtStr.substring(pos1+1,pos2)
	var strYear		=	dtStr.substring(pos2+1)
	strYr			=	strYear
	if (strDay.charAt(0)=="0" && strDay.length>1) strDay=strDay.substring(1)
	if (strMonth.charAt(0)=="0" && strMonth.length>1) strMonth=strMonth.substring(1)
	for (var i = 1; i <= 3; i++) {
		if (strYr.charAt(0)=="0" && strYr.length>1) strYr=strYr.substring(1)
	}
	month=parseInt(strMonth)
	day=parseInt(strDay)
	year=parseInt(strYr)
	if (pos1==-1 || pos2==-1){

		$('#errorStatusNote').append("&#149; "+resourceJSON.MsgDateFormat+".<br>");
		$('#errorStatusNote').show();
		return false
	}
	if (strMonth.length<1 || month<1 || month>12){

		$('#errorStatusNote').append("&#149; "+resourceJSON.PlzEtrValidMonth+".<br>");
		$('#errorStatusNote').show();
		return false
	}
	if (strDay.length<1 || day<1 || day>31 || (month==2 && day>daysInFebruary(year)) || day > daysInMonth[month]){
	
		$('#errorStatusNote').append("&#149; "+resourceJSON.PlzValidDate+".<br>");
		$('#errorStatusNote').show();
		return false
	}
	if (strYear.length != 4 || year==0 || year<minYear || year>maxYear){
		
		$('#errorStatusNote').append('&#149; '+resourceJSON.PlzEtrValid4DigitYear+''+minYear+' '+resourceJSON.MsgAnd+' '+maxYear+'.<br>');
		$('#errorStatusNote').show();
		return false
	}
	if (dtStr.indexOf(dtCh,pos2+1)!=-1 || isInteger(stripCharsInBag(dtStr, dtCh))==false){
		
		$('#errorStatusNote').append("&#149; "+resourceJSON.PlzValidDate+".<br>");
		$('#errorStatusNote').show();
		return false
	}
	return true
}

function canleGraduationDate(id){
	var graduationDate	=	document.getElementById("graduationDate_"+id).value;
	document.getElementById("graduationDate_"+id).value = "";
	if(graduationDate != null || graduationDate != ""){
		CGServiceAjax.canleGraduationDate(id,{
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{	
			//$('.onrNotesShowTrans').modal('hide');
			//document.getElementById("divTransAcademic").innerHTML=data;
		}});
	}
}


function getReferenceCheck(teacherDetails,teacherId){
	
	var eRefAccess=$('#eRefAccess').val();
	if(eRefAccess==1)
	{
	$('#errorStatusNote').empty();
	$('#errorStatusReferenceCheck').empty();
	$('#loadingDiv').show();
	currentPageFlag="referenceCheck";
	document.getElementById("teacherDetailsForReference").value = teacherDetails;
	document.getElementById("teacherIdForReference").value 		= teacherId;
	document.getElementById("statusTeacherTitle").innerHTML=""+resourceJSON.MsgEReferences+" "+teacherDetails;
	try{
		document.getElementById("statusSentRefChk").innerHTML="";
	} catch(e){}
	var jobId	=	"";
	try{
		jobId = document.getElementById("jobId").value;
	}catch(error){}
	var jobId	=	"";
	try{
		jobId = document.getElementById("jobId").value;
	}catch(error){}
	
	ManageStatusAjax.getReferenceCheck(teacherId,jobId,noOfRowsRefChk,pageRefChk,sortOrderStrRefChk,sortOrderTypeRefChk,
		{
			async: true,
			errorHandler:handleError,
			callback:function(data){
			var  refData = data.split("##");
			try{
				if(refData[1] > 0){
					document.getElementById("statusPrintRefChk").innerHTML="<button class=\"btn  btn-large btn-primary\" onclick='printReferenceData()'>"+resourceJSON.MsgPrint+" <i class=\"iconlock\"></i></button>";
				}else{
					document.getElementById("statusPrintRefChk").innerHTML="";
				}
			} catch(error){}
			
				$('#myModalReferenceCheck').modal('show');
				document.getElementById("referenceRecord").innerHTML=refData[0];
				applyScrollOnTblReferenceCheckCG();
				try{$('.districtTooltip').tooltip();}catch(e){}
				$('#loadingDiv').hide();
			}
			
		});
	}
  }

function showReferenceDetail(candidateFullName,techerFullName,elerefAutoId,districtId,jobId,teacherId){
	$('#loadingDiv').show();
	document.getElementById("statusTeacherTitleReferenceChk").innerHTML=""+resourceJSON.MsgReferenceFor+" "+candidateFullName+" "+resourceJSON.MsgProvidedBy+" "+techerFullName;
	document.getElementById("teacherDetailsCG").value = techerFullName;
	document.getElementById("candidateFullName").value = candidateFullName;
	document.getElementById("teacherIdCG").value = teacherId;
	var fromWhere	=	"CG";
	var topMaxScore	=	$('#topMaxScore').val();
	
	if(topMaxScore == 0){
		$('#buttonFinalizeScore').hide();
	}

	ManageStatusAjax.showReferenceDetail(elerefAutoId,districtId,jobId,teacherId,fromWhere,{
		async: true,
		errorHandler:handleError,
		callback:function(data){
				$('#myModalReferenceCheck').modal('hide');
				$('#myModalReferenceCheckFeedBack').modal('show');
				document.getElementById("referenceFeedBack").innerHTML=data;
				applyScrollOnTblReferenceCheck();
				$('#loadingDiv').hide();
			}
	});
}

function showReferenceDetailSLC(candidateFullName,techerFullName,elerefAutoId,districtId,jobId,teacherId){
	$('#loadingDiv').show();
	$('#myModalReferenceCheckConfirm1').hide();
	document.getElementById("statusTeacherTitleReferenceChkSLC").innerHTML=""+resourceJSON.MsgReferenceFor+" "+candidateFullName+" "+resourceJSON.MsgProvidedBy+" "+techerFullName;
	document.getElementById("candidateFullName").value = candidateFullName;
	var fromWhere	=	"SLC";
	var topMaxScore	=	$('#topMaxScore').val();
	if(topMaxScore == 0){
		$('#buttonFinalizeScoreSLC').hide();
	}
	
	ManageStatusAjax.showReferenceDetail(elerefAutoId,districtId,jobId,teacherId,fromWhere,{
		async: true,
		errorHandler:handleError,
		callback:function(data){
				$('#myModalStatus').modal('hide');
				$('#jWTeacherStatusNotesDiv').modal('hide');
				$('#myModalReferenceCheck').modal('hide');
				$('#myModalReferenceCheckFeedBackSLC').modal('show');
				document.getElementById("referenceFeedBackSLC").innerHTML=data;
				applyScrollOnTblReferenceCheck();
				$('#loadingDiv').hide();
			}
	});
}

function saveMaxScore(fromWhere){
	//$('#loadingDiv').show();
	
	var elerefAutoId 	= 	$("#elerefAutoId").val();
	var districtId		=	$("#districtId").val();
	var jobId			=	$("#jobId").val();
	var teacherId		=	$("#teacherId").val();
	var teacherDetails	=	$("#teacherDetailsCG").val()
	var	maxScore		=	0;
	document.getElementById("statusTeacherTitleForConfirmation").innerHTML=""+resourceJSON.MsgEreferenceConfirmation+"";
	iframeNorm 		= 	document.getElementById("ifrmDaAdminScoreCG");
	innerNorm 		= 	iframeNorm.contentDocument || iframeNorm.contentWindow.document;
	inputNormScore 	= 	innerNorm.getElementById('statusNoteFrm');
	maxScore		=	inputNormScore.value;
	
	if(fromWhere != "auto"){
		if(maxScore == 0){
			$('#showConfirmButtons').hide();
			$('#showConfirmCross').hide();
			$('#hideConfirmButtons').show();
			$('#hideConfirmCross').show();
			$('#myModalReferenceCheckFeedBack').modal('hide');
			$('#myModalReferenceCheckConfirm').modal('show');
			return false;
		}
	}

	ManageStatusAjax.saveMaxScore(elerefAutoId,districtId,jobId,teacherId,maxScore,{
		async: true,
		errorHandler:handleError,
		callback:function(data){
				$('#myModalReferenceCheckFeedBack').modal('hide');
				$('#myModalReferenceCheckConfirm').modal('hide');
				getReferenceCheck(teacherDetails,teacherId);
			}
	});
}


function saveMaxScoreSLC(fromWhere){
	//$('#loadingDiv').show();
	var elerefAutoId 	= 	$("#elerefAutoId").val();
	var districtId		=	$("#districtId").val();
	var jobId			=	$("#jobId").val();
	var teacherId		=	$("#teacherId").val();
	
	var fitScore		=	$("#fitScoreForStatusNote").val();
	var statusId		=	$("#statusIdForStatusNote").val();
	var secondaryStatusId=	$("#secondaryStatusIdForStatusNote").val();
	var	maxScore		=	0;

	document.getElementById("statusTeacherTitleForConfirmation").innerHTML=""+resourceJSON.MsgEreferenceConfirmation+"";
	iframeNorm 		= 	document.getElementById("ifrmDaAdminScoreSLC");
	innerNorm 		= 	iframeNorm.contentDocument || iframeNorm.contentWindow.document;
	inputNormScore 	= 	innerNorm.getElementById('statusNoteFrm');
	maxScore		=	inputNormScore.value;
	
	if(fromWhere != "auto"){
		if(maxScore == 0){
			$('#hideConfirmButtons').hide();
			$('#hideConfirmCross').hide();
			$('#showConfirmButtons').show();
			$('#showConfirmCross').show();
			$('#myModalReferenceCheckFeedBackSLC').modal('hide');
			$('#myModalReferenceCheckConfirm').modal('show');
			return false;
		}
	}
	
	ManageStatusAjax.saveMaxScore(elerefAutoId,districtId,jobId,teacherId,maxScore,{
		async: true,
		errorHandler:handleError,
		callback:function(data){
				$('#myModalReferenceCheckFeedBackSLC').modal('hide');
				$('#myModalReferenceCheckConfirm').modal('hide');
				jWTeacherStatusNotesOnOpen(fitScore,statusId,secondaryStatusId);
			}
	});
}

function saveMaxScorePool(fromWhere){
	//$('#loadingDiv').show();
	var elerefAutoId 	= 	$("#elerefAutoId").val();
	var districtId		=	$("#districtId").val();
	var teacherId		=	$("#teacherIdForPool").val();
	var teacherDetails	=	$("#teacherDetailsCG").val();
	var jobId			=	null
	var	maxScore		=	0;

	document.getElementById("statusTeacherTitleForConfirmation").innerHTML=""+resourceJSON.MsgEreferenceConfirmation+"";
	iframeNorm 		= 	document.getElementById("ifrmDaAdminScoreCG");
	innerNorm 		= 	iframeNorm.contentDocument || iframeNorm.contentWindow.document;
	inputNormScore 	= 	innerNorm.getElementById('statusNoteFrm');
	maxScore		=	inputNormScore.value;
	
	if(fromWhere != "auto"){
		if(maxScore == 0){
			$('#myModalReferenceCheckFeedBack').modal('hide');
			$('#myModalReferenceCheckConfirm').modal('show');
			return false;
		}
	}

	ManageStatusAjax.saveMaxScoreForPool(elerefAutoId,districtId,teacherId,maxScore,{
		async: true,
		errorHandler:handleError,
		callback:function(data){
				$('#myModalReferenceCheckFeedBack').modal('hide');
				$('#myModalReferenceCheckConfirm').modal('hide');
				getReferenceCheck(teacherDetails,teacherId);
			}
	});
}


function closeReferenceCheck(id){
	if(id == 1){
		$('#myModalReferenceCheck').modal('hide');
	} else if(id == 2){
		var candidateFullName 	= 	document.getElementById("candidateFullName").value;
		var teacherId 			=	document.getElementById("teacherIdCG").value;
		$('#myModalReferenceCheckFeedBack').modal('hide');
		getReferenceCheck(candidateFullName,teacherId);
	} else if(id == 3){

		$('#myModalReferenceCheckFeedBackSLC').modal('hide');
		$('#jWTeacherStatusNotesDiv').modal('show');
	} else if(id == 4){
		$('#myModalReferenceCheckConfirm').modal('hide');
		$('#myModalReferenceCheckFeedBack').modal('show');
	} else if(id == 5){
		$('#myModalReferenceCheckConfirm').modal('hide');
		$('#myModalReferenceCheckFeedBackSLC').modal('show');
	} else if(id == 6){
		$('#myModalReferenceCheckForPrint').modal('hide');
		$('#myModalReferenceCheck').modal('show');
	}
	try{$('.modal-backdrop.in').css('opacity','0');}catch(e){}//yy
	try{$('#cg').val('0');}catch(e){}
}

function sendReferenceMailFromCG(){
	$('#loadingDivReference').show();
	$('#errorStatusReferenceCheck').empty();
	var focus 			= 	0;
	var counter			= 	0;
	var elerefAutoIds	=	"";

	var referenceTeacherId	=	document.getElementById("referenceTeacherId").value
	var graduationDate		=	"";
	var referenceData		=	document.getElementsByName("referenceData[]");
	var counter;
	var graduationCounter	=	0;
	var numberOfQuestion	=	0;
	
	var referenceJobId		=	"";
	
	try{
		referenceJobId		=	document.getElementById("referenceJobId").value;
		numberOfQuestion	=	document.getElementById("numberOfQuestion").value;
	} catch(err){
		//alert("Error");
	}
	
	try{
		var fitScore			=	document.getElementById("fitScoreForStatusNote").value;
		var statusId			=	document.getElementById("statusId").value;
		var secondaryStatusId	=	document.getElementById("secondaryStatusId").value;
	} catch(error){}
	
	if(numberOfQuestion == 0){
		$('#errorStatusReferenceCheck').append("&#149; "+resourceJSON.MsgNoActiveQuestion+"<br>");
		return false;
	}
	
	//alert(":::::::::: Number Of Question :::::::"+numberOfQuestion);return false;
	for(var i=0; i < referenceData.length; i++){
		if(referenceData[i].checked == true){
			elerefAutoIds	 =	elerefAutoIds+"##"+referenceData[i].value;
			$('#loadingDivReference').hide();
		}
	}
	
	//return false;
	if(referenceJobId != 'null'){
		if(counter == 0 && graduationCounter == 0){
				ManageStatusAjax.sendReferenceMail(referenceTeacherId,referenceJobId,elerefAutoIds,{
				async: true,
				errorHandler:handleError,
				callback:function(data)
				{
					location.reload();
				}});
		 }
	} else {
	if(counter == 0 && graduationCounter == 0){
		ManageStatusAjax.sendReferenceMailPool(referenceTeacherId,elerefAutoIds,{
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{
				location.reload();
			}});
		}
	
	}
}

function sendOnlineActivityLink(questionSetId,flag)
{
	var teacherId = document.getElementById("statusTeacherId").value;
	var jobId = document.getElementById("jobId").value;
	
     ManageStatusAjax.sendOnlineActivityLink(teacherId,jobId,questionSetId,flag,{ 
 		async: true,
 		errorHandler:handleError,
 		callback:function(data)
 		{
 			displayOnlineActivityForCG();
 		}
     });
}
function showOnlineActivityQuestion(questionSetId){
	document.getElementById("questionSetIdForCg").value=questionSetId;
	getOnlineActivityQuestionsForCG();
	$('#jWTeacherStatusNotesDiv').modal('hide');	
	$('#onlineActivityDiv').modal('show');
}
function getOnlineActivityQuestionsForCG(){
	//added by 08-04-2015
	try{
	$('#mainTableOnlineActivity').remove();
	$('#mainonlineDiv').remove();	
	//$('#onlineActivityGrid #divMainFrame').remove();
	
	}catch(e){}
	//ended by 08-04-2015
	var questionSetIdForCg=document.getElementById("questionSetIdForCg").value;
	var teacherId=document.getElementById("statusTeacherId").value;
	var jobId = document.getElementById("jobId").value;
	var districtId=document.getElementById("districtId").value;
	CGServiceAjax.getOnlineActivityQuestionsForCG(teacherId,jobId,districtId,questionSetIdForCg,{  
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			var dataArray = data.split("@##@");
			totalOnlineActivityQuestionList = dataArray[1];
			$('#onlineActivityGrid').html(dataArray[0]);
			//added by 08-04-2015
			
			$('#onlineActivityGrid').after("<div id='mainTableOnlineActivity' style='width:670px;'><br/><div id='onlineActivityGridNoteId'><label>Note</label><br/><textarea  id='onlineActivityGridNote' name='onlineActivityGridNote' class='form-control' maxlength='100' rows='4' style='resize: none;'></textarea></div></div>");
			CGServiceAjax.displayOnlineActivityNotesForCG(teacherId,jobId,districtId,questionSetIdForCg,{ 
				async: false,
				errorHandler:handleError,
				callback:function(data)
				{	
				$('#onlineActivityGrid').after('<div id="mainonlineDiv"><br/>'+data+'</div>');
				applyTblOnlineActivityNotes();
				
				//added by 01-05-2015
				var hiredFlagForClickOnStatus=0;				
				try{
					hiredFlagForClickOnStatus=$('[name="hiredFlagForClickOnStatus"]').val();
				}catch(e){}
				if(hiredFlagForClickOnStatus==1){
				$('#onlineActivityFinalize').hide();
				$('#jWTeacherStatusNotesDiv #undo').hide();
				$('#jWTeacherStatusNotesDiv #statusSave').hide();
				$('#jWTeacherStatusNotesDiv #statusFinalize').hide();
				}else{
					$('#onlineActivityFinalize').show();
				}
				//ended by 01-05-2015
				
				}
			});		
			$('[name="onlineActivityGridNote"]').jqte();
			//ended by 08-04-2015
			$('#jWTeacherStatusNotesDiv').modal('hide');	
		}});
}

function applyTblOnlineActivityNotes()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblOnlineActivityNotes').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 100,
        width: 670,
        minWidth: null,
        minWidthAuto: false,
        colratio:[300,200,200], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: true,
        sortedColId: null,
        sortType : ['string','string','date'],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });            
        });
}
function saveOnlineActivity(param){
	
	var teacherId=document.getElementById("statusTeacherId").value;
	var jobId = document.getElementById("jobId").value;
	var districtId=document.getElementById("districtId").value;
	var questionSetIdForCg=document.getElementById("questionSetIdForCg").value;
	var arr =[];
	var isRequiredCount=0;
	var ansRequiredCount=0;
	var scoreCheck="";
	for(i=1;i<=totalOnlineActivityQuestionList;i++)
	{   
		var isRequired = dwr.util.getValue("QOA"+i+"isRequired");
		if(isRequired==1){
			isRequiredCount++;
		}	
		var score=0;
		var onlineActivityQuestionId = {questionId:dwr.util.getValue("QOA"+i+"questionId")};
		var totalScore =dwr.util.getValue("QOA"+i+"totalScore");
		try{
			var iframeNorm = document.getElementById("QOA"+i+"ifrmOnlineActivity");
			var innerNorm = iframeNorm.contentDocument || iframeNorm.contentWindow.document;
			var inputNormScore = innerNorm.getElementById('onlineActivityFrm');
			score=inputNormScore.value;
		}catch(e){}	
		
		arr.push({ 
			"onlineActivityQuestions"  : onlineActivityQuestionId,
			"totalScore"  : totalScore,
			"score"  :score
		});
		scoreCheck+="||"+score+"||";
	}
	var errorCount=true;
	if(isRequiredCount==ansRequiredCount){
		errorCount=false;
	}
	if(param==1 && scoreCheck.indexOf("||0||") > -1){
		$('#onlineActivityDiv').modal('hide');
		$('#onlineActivityMSGDiv').modal('show');
	}else{
		param=2;
	}
	if(param==2){
		var onlineActivityNote=$('#onlineActivityGridNoteId').find(".jqte_editor").html();//added by 08-04-2015 
		//var onlineActivityNote=document.getElementById("onlineActivityGridNote").value; //added by 08-04-2015 
		CGServiceAjax.saveOnlineActivity(arr,teacherId,jobId,districtId,questionSetIdForCg,errorCount,onlineActivityNote,{ 
			async: false,
			errorHandler:handleError,
			callback: function(data){
				arr =[];
				$('#onlineActivityMSGDiv').modal('hide');
				$('#onlineActivityDiv').modal('hide');
				$('#jWTeacherStatusNotesDiv').modal('show');	
				displayOnlineActivityForCG();
			}
		});	
	}
}
function onlineActivityOnClose(){
	$('#onlineActivityDiv').modal('hide');
	$('#jWTeacherStatusNotesDiv').modal('show');	
}
function onlineActivityMSGOnClose(){
	$('#onlineActivityMSGDiv').modal('hide');
	$('#onlineActivityDiv').modal('show');
}
function displayOnlineActivityForCG(){
	
	//alert('displayOnlineActivityForCG');
	var teacherId=document.getElementById("statusTeacherId").value;
	//alert('teacherId:::'+teacherId);
	var districtId=document.getElementById("districtId").value;
	//alert('districtId:::'+districtId);
	var jobId = document.getElementById("jobId").value;
	//alert('jobId:::'+jobId);
	CGServiceAjax.displayOnlineActivityForCG(teacherId,jobId,districtId,{  
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#divOnlineActivityForCG').html(data);			
			applyScrollOnOnlineActivity();
			
			//added for clickable status after hired
			var hiredFlagForClickOnStatus=0;
			try{
				hiredFlagForClickOnStatus=$('[name="hiredFlagForClickOnStatus"]').val();
			}catch(e){}
			if(hiredFlagForClickOnStatus==1)
			$('.notclickable').css('pointer-events','none');
			else
			$('.notclickable').css('pointer-events','');
			//ended for clickable status after hired
		}});
}
function applyScrollOnSchoolSelection()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblGridSelectedSchool').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 100,
        width: 670,
        minWidth: null,
        minWidthAuto: false,
        colratio:[120,320,120,130], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });            
        });
}
function getSelectedSchoolsList(jobId,teacherId)
{	
	currentPageFlag='SS';
	document.getElementById("teacherIdForSchoolSelection").value=teacherId;
	jobId=document.getElementById("jobId").value;
	
	
	CGServiceAjax.getSelectedSchoolsList(jobId,teacherId,noOfRowsSS,pageSS,sortOrderStrSS,sortOrderTypeSS,
	{ 
		async: true,
		errorHandler:handleError,
		callback: function(data)
		{
			if(data!=null && data!="")
			{
				document.getElementById("schoolSelectionDivGrid").innerHTML=data;
				$('#schoolSelectionDiv').modal('show');
				applyScrollOnSchoolSelection();				
			}
		}
	});	
}
function showLetterOfIntent(jobId,schoolId,teacherId,linkId){
	CGServiceAjax.showLetterOfIntentForCG(jobId,schoolId,teacherId,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
			if(data=="")
			{			
				//alert(""+resourceJSON.msgLetterNotUpld+"")
			}
			else
			{
				if(deviceType)
				{
					if (data.indexOf(".doc")!=-1)
					{
						$("#docfileNotOpen").css({"z-index":"3000"});
				    	$('#docfileNotOpen').show();
					}
					else if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
					{
						$("#exelfileNotOpen").css({"z-index":"3000"});
				    	$('#exelfileNotOpen').show();
					}
					else
					{
						document.getElementById(linkId).href = data; 	
					}
				}
				else if(deviceTypeAndroid)
				{
					if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
					{
						document.getElementById('ifrmletterOfIntent').src = ""+data+"";
					}
					else
					{
						document.getElementById(linkId).href = data; 	
					}	
				}
				else if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					document.getElementById('ifrmletterOfIntent').src = ""+data+"";
				}
				else
				{
					document.getElementById(linkId).href = data; 	
				}			
			}				
			return false;
		}
	});

}
function assessmentInvite(teacherFullName,jobId,teacherId)
{	
	currentPageFlag='AI';
	CGServiceAjax.getDistrictAssessmentDetails(teacherId,jobId,
	{ 
		async: true,
		errorHandler:handleError,
		callback: function(data)
		{
			if(data!=null && data!="")
			{
				document.getElementById("assessmentDivGrid").innerHTML=data;
				$('#modalAssessmentInvite').modal('show');
				applyScrollOnAssessmentInvite();			
			}
		}
	});	
}
function closeSuccessInvite()
{
	$('#modalAssessmentInvite').modal('show');
	$('#successInvite').modal('hide');	
}

function getAssessmentInviteQuestionAnswers(teacherId,jobId,districtAssessmentId){
	$('#loadingDiv').show();
	//alert('getAssessmentInviteQuestionAnswers');
	CGServiceAjax.getAssessmentInviteQuestionAnswers(teacherId,jobId,districtAssessmentId,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){
		//$('#modalAssessmentDetails').modal('hide');
		$('#saveScore').css("display","none");
		$('#loadingDiv').hide();
		$('#modalAssessmentInvite').hide();
		$('#focus').focus();
		document.getElementById("assessmentDetailsDivGrid").innerHTML=data;
		$('#modalAssessmentDetails').modal('show');
		
		}
	});	
}
function closeAssessmentInvite(){
	$('#modalAssessmentInvite').hide();
}
function closeAssessmentDetails(){
	$("html, body").animate({ scrollTop: 0 }, 'slow');
	$('#focus').focus();
	$("#assessmentDetailsDivGrid").empty();
	$('#modalAssessmentDetails').modal('hide');
	$('#modalAssessmentInvite').show();
}

function sendAssessmentInvite(teacherId,jobId,assessmentJobRelationId,flag){
	
		CGServiceAjax.sendAssessmentInvite(teacherId,jobId,assessmentJobRelationId,{ 
			async: false,
			errorHandler:handleError,
			callback: function(data){
				//
				$('#loadingdiv').hide();
				//$('#modalAssessmentDate').modal('hide');
				$('#modalAssessmentInvite').modal('hide');
				$('#successInvite').modal('show');
				assessmentInvite(data,jobId,teacherId);
			}
		});
}

function inviteAssessment()
{
	$('#multiInviteErrorDiv').empty();
	var teacherIds = "";
	$('input[name="cgCBX"]:checked').each(function(){
		var teacherId = $(this).attr("teacherid");//teacherid
		if(teacherIds=="")
			teacherIds=teacherIds+teacherId;
		else
			teacherIds=teacherIds+","+teacherId;
	});
	CGServiceAjax.getMultipleInviteAssessment(teacherIds,
			{
				async: false,
				errorHandler:handleError,
				callback:function(data)
				{	
					$('#multiInviteAssessment').modal('show');
					document.getElementById("inviteGridData").innerHTML=data;
				}
			});
}

function sendMultipleInvite(teacherIds)
{
	$('#multiInviteErrorDiv').empty();
	var errorCount = 0;
	var jobId = document.getElementById('jobId').value;
	var assessmentId = document.getElementById('assessId').value;
	if(assessmentId==-1)
	{
		$('#multiInviteErrorDiv').show();
		$('#multiInviteErrorDiv').append("&#149;  "+resourceJSON.PlzSelectAsscssment+"");
	}
	if(teacherIds.length==0)
		{
			$('#multiInviteErrorDiv').show();
			$('#multiInviteErrorDiv').append("&#149;  "+resourceJSON.PlzSelectCandidates+"<br>");
			errorCount++;
		}
	if(errorCount==0)
		{
		// $('#errordiv').show();
		CGServiceAjax.sendMultipleAssessmentInvite(teacherIds,jobId,assessmentId,
				{
					async: false,
					errorHandler:handleError,
					callback:function(data)
					{	
						$('#multiInviteAssessment').modal('hide');
						$('#successMultiInvite').modal('show');
					}
				});

	}
}

function closeSuccessMultiInvite()
{
	//
	$('#multiInviteAssessment').modal('hide');
	$('#successMultiInvite').modal('hide');
	getCandidateGrid();
}

function printReferenceData()
{
	$('#loadingDivReference').show();
	$('#errorStatusReferenceCheck').empty();
	var referenceTeacherId	=	document.getElementById("referenceTeacherId").value;
	var referenceData		=	document.getElementsByName("referenceDataForPrint[]");
	var elerefAutoIds	=	"";
	for(var i=0; i < referenceData.length; i++){
		//if(referenceData[i].checked == true){
			elerefAutoIds	 =	elerefAutoIds+"##"+referenceData[i].value;
			$('#loadingDivReference').hide();
		//}
	}
	
	try{
		referenceJobId		=	document.getElementById("referenceJobId").value;
	} catch(err){}
			
	ManageStatusAjax.printReferenceData(referenceTeacherId,referenceJobId,elerefAutoIds,{
		async: true,
		errorHandler:handleError,
		callback:function(data){
				$('#myModalReferenceCheck').modal('hide');
				$('#myModalReferenceCheckForPrint').modal('show');
				document.getElementById("referencePrintData").innerHTML=data;
				applyScrollOnTblReferenceCheck();
				$('#loadingDiv').hide();
			}
	});
}

var isSafari = /Safari/.test(navigator.userAgent) && /Apple Computer/.test(navigator.vendor);
function printReferenceDataToWindow()
{
	$('#errorStatusReferenceCheck').empty();
	var referenceTeacherId	=	document.getElementById("referenceTeacherId").value;
	var referenceData		=	document.getElementsByName("referenceDataForPrint[]");
	var elerefAutoIds	=	"";
	for(var i=0; i < referenceData.length; i++){
		//if(referenceData[i].checked == true){
			elerefAutoIds	 =	elerefAutoIds+"##"+referenceData[i].value;
			$('#loadingDivReference').hide();
		//}
	}
	
	try{
		referenceJobId = document.getElementById("referenceJobId").value;
	} catch(err){}
	
	$('#loadingDivReference').fadeIn();
	ManageStatusAjax.printReferenceData(referenceTeacherId,referenceJobId,elerefAutoIds,{
		async: true,
		errorHandler:handleError,
		callback:function(data){
			$('#loadingDiv').hide();
			
			try{
				if (isSafari && !deviceType) {
			    	window.document.write(data);
					window.print();
			    } else {
			    	var newWindow = window.open();
			    	newWindow.document.write(data);	
			    	newWindow.print();
				 }
			}catch(e){
				$('#printmessage1').modal('show');
			}
		},
		errorHandler:handleError
	});
}

/*var questionIds = [];
function setHiddenQuestion(checkedIndex,questionId)
{
	if($('#'+checkedIndex).prop('checked'))
	{
		questionIds.push(questionId);
	}
	else
	{
		var i = questionIds.indexOf(questionId);
		if(i != -1) {
			questionIds.splice(i,1);
		}
	}
}*/

var arr = [];
function setScoreForQuestion()
{	
	var errorCount = 0;
	var assessmentId = document.getElementById('assessmentId').value;
	var teacherId = document.getElementById('candidateId').value;
//	alert(" assessmentId :::  "+assessmentId+" teacherId  :::: "+teacherId);
	var scoringArray = [];
	var notesArray = [];
	var questionMarks = [];
	var inputs = document.getElementsByName("scoreSet"); 
	var splitArray;
	var inputValue;
	for (var i = 0; i < inputs.length; i++) {
	        	if(inputs[i].checked){
	        		try{
	        			 iframeNorm 		= 	document.getElementById("Q"+i+"ifrmStatusNote");
	        			 innerNorm 			= 	iframeNorm.contentDocument || iframeNorm.contentWindow.document;
	        			 inputNormScore 	= 	innerNorm.getElementById('statusNoteFrm');
	        			 questionMaxScore	=	inputNormScore.value;
	        			 dsliderchkVal 		= 	document.getElementById("Q"+i+"dsliderchk").value;
	        			 //var MaxScore		=	document.getElementById("MaxScore"+i);
	        			 var questionId= document.getElementById("Q"+i).value;
	        			// var note =  document.getElementById('notes'+i).value;
	        			 /*if(note=="")
	        			 {
	        				 if(arr.length==0)
	        				 {
	        					 arr.push(i);
	        				 }
	        				 $('#notes'+i).css("background-color", "#F5E7E1");
	        				 errorCount++;
	        			 }
	        			 else
	        			 {
	        				 $('#notes'+i).css("background-color", "white");
	        			 }*/
	        			 if(errorCount==0)
	        			 {
		        			 var teacherDistrictAssessmentQuestion = {teacherDistrictAssessmentQuestionId:questionId};
		        			 var districtAssessmentDetail = {districtAssessmentId:assessmentId};
		        			 var teacherDetail = {teacherId:teacherId};
		        			 scoringArray.push({
		        					"teacherDistrictAssessmentQuestion"  : teacherDistrictAssessmentQuestion,
		        					"districtAssessmentDetail" : districtAssessmentDetail,
		        					"totalScore"  : questionMaxScore,
		        				});
		        			/* notesArray.push({
		        				 "teacherDetail" : teacherDetail,
		        				 "districtAssessmentDetail" : districtAssessmentDetail,
		        				 "teacherDistrictAssessmentQuestion"  : teacherDistrictAssessmentQuestion,
		        				 "assessmentNotes" : note,
		        			 });*/
	        			 }	        			 
	        		}catch(err){}
	        }
	 }
	if(errorCount==0)
	 {
		CGServiceAjax.getScoreForQuestions(scoringArray,teacherId,notesArray,
				{
					async: false,
					errorHandler:handleError,
					callback:function(data)
					{	
						if(data=="update")
						{
							$('#saveScore').css("display","none");
							$('#modalAssessmentDetails').modal('hide');
							$('#modalAssessmentInvite').modal('hide');
							$('#scoreUpdateModal').modal('show');
						
						}
					}
				});
	 }
	else
	{
		$('#modalAssessmentDetails').modal('hide');
		$('#scoreUpdateErrorModal').modal('show');		
	}
}

function getFocus()
{
	$('#modalAssessmentDetails').modal('show');
	$('#notes'+arr[0]).focus();
	var i = arr.indexOf(arr[0]);
	if(i != -1) {
		arr.splice(i,1);
	}
}

function hideScoreUpdateDiv()
{
	$('#scoreUpdateModal').modal('hide');
	$('#modalAssessmentInvite').modal('show');
//	getCandidateGrid();
}

function showSlider(sliderIndex,sliderCounter,interval,topMaxScore,scoreProvided)
{
	if($('#'+sliderIndex).prop('checked'))
	{
		//document.getElementById('notesDiv'+sliderCounter).style.display="block";
		//if(document.getElementById('postedNotesDiv'+sliderCounter)!=undefined){document.getElementById('postedNotesDiv'+sliderCounter).style.display="block";}
		document.getElementById('Q'+sliderCounter+'ifrmStatusNote').src= 'slideract.do?name=statusNoteFrm&tickInterval='+interval+'&max='+topMaxScore+'&swidth=536&dslider=1&svalue='+scoreProvided;
	}
	else
	{
		//document.getElementById('notesDiv'+sliderCounter).style.display="none";
		//if(document.getElementById('postedNotesDiv'+sliderCounter)!=undefined){document.getElementById('postedNotesDiv'+sliderCounter).style.display="none";}
		document.getElementById('Q'+sliderCounter+'ifrmStatusNote').src= 'slideract.do?name=statusNoteFrm&tickInterval='+interval+'&max='+topMaxScore+'&swidth=536&dslider=0.0&svalue='+scoreProvided;
	}
}

var buttonScoreArray =[];
function showSaveScoreButton(checkedIndex,questionId)
{

	if($('#'+checkedIndex).prop('checked'))
	{
		buttonScoreArray.push(questionId);
	}
	else
	{
		var i = buttonScoreArray.indexOf(questionId);
		if(i != -1) {
			buttonScoreArray.splice(i,1);
		}
	}
	if(buttonScoreArray.length>0)
	{
		document.getElementById('saveScore').style.display="block";
	}
	else
	{
		document.getElementById('saveScore').style.display="none";
	}
}

//added by Ram Nath
function editTeacherStatusByOver(val){
	//alert('value======='+val);	
	var statusMasterId=null;
	var secondaryStatusId=null;
	var allInfo=val.split(",");
	var teacherStatusScoreId=allInfo[0].trim();
	var topMaxScore=allInfo[1].trim();
	var scoreProvided=allInfo[2].trim();
	var teacherId=allInfo[3].trim();
	var jobId=allInfo[4].trim();
	//var incOrDec="";
	
	var allStatus=allInfo[5].trim().split('##');
	if(allStatus.length>1){
		var sm=allStatus[0].split('#');
		var ss=allStatus[1].split('#');	
		statusMasterId=sm[1];
		secondaryStatusId=ss[1];
	}
	else{
		var notKnow=allStatus[0].trim().split('#');
		if(notKnow[0].trim()=='ss')
			secondaryStatusId=notKnow[1];
		else
			statusMasterId=notKnow[1];
			
	}	
	var ifrmEditSlider = document.getElementById('editIfrmTeacherStatusScore');
	if(ifrmEditSlider!=null)
	{
		var innerSliderOver = ifrmEditSlider.contentDocument || ifrmEditSlider.contentWindow.document;
		var inputQuestionFrm = innerSliderOver.getElementById('statusSliderScoreFrm');  //questionFrm
		var answerIdFrm = innerSliderOver.getElementById('answerId');
		var score=inputQuestionFrm.value;
		/*if(scoreProvided>score)
			incOrDec="dec";
		else if(scoreProvided<score)
			incOrDec="inc";
		else
			incOrDec="eq";*/
		//alert("teacherStatusScoreId=="+teacherStatusScoreId+" teacherId=="+teacherId+" jobId=="+jobId+" statusMasterId=="+statusMasterId+" secondaryStatusId=="+secondaryStatusId+" score=="+score);
		 ManageStatusAjax.setAndGetTeacherStatusSlider(teacherStatusScoreId,teacherId,jobId,statusMasterId,secondaryStatusId,score,{ 
		 		async: true,
		 		errorHandler:handleError,
		 		callback:function(data)
		 		{
		 			$('#scoreList').html(data);
		 			$('#openEditTeacherStatusScore').hide();
		 			$('#contentEditDivScore').html(data); 			
		 		}
		     });
	}	
}
function editteacherStatusScores(val){
	var allInfo=val.split(",");
	var topMaxScore=allInfo[1].trim();
	var scoreProvided=allInfo[2].trim();
	var tickInterval=1;
	if(topMaxScore%10==0)
		tickInterval=10;
	else if(topMaxScore%5==0)
		tickInterval=5;
	else if(topMaxScore%3==0)
		tickInterval=3;
	else if(topMaxScore%2==0)
		tickInterval=2;
	var iframe="<iframe id=\"editIfrmTeacherStatusScore\"  src=\"slideract.do?name=statusSliderScoreFrm&tickInterval="+tickInterval+"&max="+topMaxScore+"&swidth=360&dslider=1&svalue="+scoreProvided+"\" scrolling=\"no\" frameBorder=\"0\" style=\"padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:395px;margin-top:-10px;\"></iframe>";
	var save="<br/>&nbsp;&nbsp;&nbsp;<a href='javascript:void(0)' onclick=\"editTeacherStatusByOver('"+val+"');\">"+resourceJSON.BtnSave+"</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
	var cancel="<a href='javascript:void(0)' onclick=\"$('#openEditTeacherStatusScore').hide();\">"+resourceJSON.btnCancel+"</a>";
	$('#openEditTeacherStatusScore').html(iframe+save+cancel);
	$('#openEditTeacherStatusScore').show();
	var height=$(".popover-inner").height();
	var height2=$("#openEditTeacherStatusScore").height();
	var mainPostion=height-height2;
	mainPostion=(mainPostion/2)+2;//-
	$('#popover .arrow').css('top',mainPostion+'px');
	setTimeout(function(){
	if(checkFirstClick){
		$('#popover').show();
		$(popoverObj).popover('show');
		$('#openEditTeacherStatusScore').html(iframe+save+cancel);
		$('#openEditTeacherStatusScore').show();
		$('#popover .arrow').css('top',mainPostion+'px');
		checkFirstClick=false;
	}
	},170);
}
$(function() {
	$('#jWTeacherStatusNotesDiv').click(function(){
		$('#statusscoreYes').click(function(){
			return false;
		});
		$('#popover').click(function(){
			return false;
		});
		$(popoverObj).popover('hide');
	});
});
//ended by Ram Nath
//added by Ram for mouse over
function openInfoDivForIncomp(call_showOuterStatus_Or_showStatus,allInfo){
	$('#loadingDiv').show();
	var allval=allInfo.split(',');
	var teacherName=allval[0];
	var doActivity=allval[1];
	var cgStatus=allval[2];
	var status=allval[3];
	var  teacherAssessmentStatusId=allval[4];
	var  jftId=allval[5];
	var tdId=allval[6];	
	var noOfRec=allval[7];
	var resultData="";
	CGServiceAjax.getIncompleteInfoHtml(jftId,{ 
		async: false,
		cache: false,
		errorHandler:handleError,
		callback:function(data)	{resultData=data;}
	});
	if(resultData!=undefined && resultData!=null && resultData!=''){
	$('#loadingDiv').hide();
	getConfirmIncmpDiv(""+resourceJSON.MsgIncompleteApplication+" "+teacherName,resultData,resourceJSON.btnOk,function(resp){
		if(resp){				
			if(call_showOuterStatus_Or_showStatus==0){
				showOuterStatus(teacherName,doActivity,cgStatus,status,teacherAssessmentStatusId,jftId,tdId);
			}else if(call_showOuterStatus_Or_showStatus==1){
				showStatus(teacherName,doActivity,cgStatus,status,teacherAssessmentStatusId,jftId,tdId,noOfRec);
			}			
		}
		});
	}else if(resultData==''){		
			$('#loadingDiv').hide();
	}
}
function getConfirmIncmpDiv(header,confirmMessage,buttonNameChange,callback){
	var div="<div class='modal hide in' id='confirmbox' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'>"+
			"<div class='modal-dialog'  style='width:530px;'>"+
			"<div class='modal-content'>"+
			"<div class='modal-header'>"+
	        "<button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>"+
	        "<h4 class='modal-title' id='myModalLabel' style='color:white;'></h4>"+
	      "</div>"+
	      "<div class='modal-body' id='confirmMessage'>"+	      
	      "</div>"+
	      "<div class='modal-footer'>"+
	      "<button type='button' class='btn btn-primary' id='confirmTrue' ></button>"+
	        "<button type='button' class='btn btn-default' id='confirmFalse'>"+resourceJSON.btnCancel+"</button>"+	        
	      "</div>"+
	    "</div>"+
	  "</div>"+
	"</div>";
	$('#jWTeacherStatusNotesDiv').after(div);
    confirmMessage = confirmMessage || '';
    $('#confirmbox').modal({show:true,
                            backdrop: "static",
                            keyboard: false,
    });

    $('#confirmbox #confirmMessage').html(confirmMessage);
    $('#confirmbox #myModalLabel').html(header);
    $('#confirmbox #confirmTrue').html(buttonNameChange);
    $('#confirmFalse').click(function(){
    	$('#confirmbox').modal('hide');
        $('#confirmbox').remove();
        if (callback) callback(false);
    });
    $('#confirmTrue').click(function(){
    	$('#confirmbox').modal('hide');
        $('#confirmbox').remove();
        if (callback) callback(true);
    });
}
//ended by Ram Nath
//added by 23-04-2015
var map = new Object(); // or var map = {};
//map[myKey1] = myObj1; //map[myKey1] == get(myKey1);
function get(k) {
    return map[k];
}
//ended by 23-04-2015

function editTeacherStatusNotes(obj,notesId){
	var editNote=$('[name="questionSuperNotes'+notesId+'"]').parents().parents('.jqte').find(".jqte_editor").text().trim();
	map[notesId] = editNote;
	$('#main'+notesId).css('padding-top','35px');	
	$('[name="questionSuperNotes'+notesId+'"]').parents().parents('.jqte').css('pointer-events','');
	$('[name="questionSuperNotes'+notesId+'"]').parents().parents('.jqte').find(".jqte_editor").focus();
	isClickCountOnEditForSuperUser=isClickCountOnEditForSuperUser+1;
	$('#edit_notes'+notesId).hide();
	$('#save_notes'+notesId).show();
	$('#cancel_notes'+notesId).show();	
}
function saveTeacherStatusNotes(obj,teacherStatusNotesId){
	var editNote=$('[name="questionSuperNotes'+teacherStatusNotesId+'"]').parents().parents('.jqte').find(".jqte_editor").text().trim();
	if(editNote!=null && editNote!=undefined && editNote.trim()!=''){
	ManageStatusAjax.editAndGetTeacherStatusNote(teacherStatusNotesId,editNote,{
		async: false,
		cache: false,
		errorHandler:handleError,
		callback:function(data)	{
		if(data=='yes'){
		var successmessage="<div id='editSuccess' class='alert alert-success' style='width:80% !important;vertical-align:middle;padding:0px;padding-left:10px;padding-right:10px;margin-bottom:0px;'><a href='#' class='close' data-dismiss='alert'>&times;</a>"+
		"Note has been updated successfully.";
		$('#main'+teacherStatusNotesId).css('padding-top','65px');
		$('#edit_notes'+teacherStatusNotesId).show();
		$('#save_notes'+teacherStatusNotesId).hide();
		$('#cancel_notes'+teacherStatusNotesId).hide();
		$('[name="questionSuperNotes'+teacherStatusNotesId+'"]').parents().parents('.jqte').before(successmessage);
		setTimeout(function(){$('#editSuccess').remove();},2000*10);
		$('[name="questionSuperNotes'+teacherStatusNotesId+'"]').parents().parents('.jqte').css('pointer-events','none');
		isClickCountOnEditForSuperUser=isClickCountOnEditForSuperUser-1;
		}else{
			var error="<div id='addError' class='alert alert-danger' style='width:80% !important;vertical-align:middle;padding:0px;padding-left:10px;padding-right:10px;margin-bottom:0px;'><a href='#' class='close' data-dismiss='alert'>&times;</a>"+
			"Note could not be updated. Please try again.</div>";
			$('[name="questionNotes'+teacherStatusNotesId+'"]').parents().parents('.jqte').before(error);
			setTimeout(function(){$('#addError').remove();},2000*10);
		}
	}
	});
	}else{
		/*var error="<div class='alert alert-danger' role='alert'> <span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>"+
		  "<span class='sr-only'>Error:</span> Enter a valid note</div>";*/
		var errormessage="<div id='editSuccess' class='alert alert-danger' style='width:80% !important;vertical-align:middle;padding:0px;padding-left:10px;padding-right:10px;margin-bottom:0px;'><a href='#' class='close' data-dismiss='alert'>&times;</a>"+
		"Please enter some text to save the note</div>";
		$('[name="questionSuperNotes'+teacherStatusNotesId+'"]').parents().parents('.jqte').before(errormessage);
		setTimeout(function(){$('#editSuccess').remove();},2000*10);
	}
	
}
function cancelTeacherStatusNotes(obj,notesId){
	$('[name="questionSuperNotes'+notesId+'"]').parents().parents('.jqte').find(".jqte_editor").text(get(notesId));
	$('#main'+notesId).css('padding-top','65px');
	$('#edit_notes'+notesId).show();
	$('#save_notes'+notesId).hide();
	$('#cancel_notes'+notesId).hide();
	$('[name="questionSuperNotes'+notesId+'"]').parents().parents('.jqte').css('pointer-events','none');
	if(isClickCountOnEditForSuperUser!=0)
	isClickCountOnEditForSuperUser=isClickCountOnEditForSuperUser-1;
}
function callEnableOrDisableJqte(){	
	setTimeout(function(){
	$( ".inner_disable_jqte" ).find( ".jqte" ).css( 'pointer-events','none' );
	$('.inner_enable_jqte').find( ".jqte" ).css('pointer-events','');
	$('.tooltipforicon').tooltip();
	},5);
}

function addTeacherStatusNotes(obj,questionId,val){
	var errormessage="<div id='addError' class='alert alert-danger' style='width:80% !important;vertical-align:middle;padding:0px;padding-left:10px;padding-right:10px;margin-bottom:0px;'><a href='#' class='close' data-dismiss='alert'>&times;</a>"+
	"Please enter some text to save the note.</div>";
	var addNote=$('[name="questionNotes'+questionId+'"]').parents().parents('.jqte').find(".jqte_editor").text().trim();
	//alert('enterhhhhhhhhhh===='+addNote+"      ========"+val);	
	if(addNote!=null && addNote!=''){
		var allIntoAboutNote=val.split(",");
		var teacherId=allIntoAboutNote[0];
		var jobId=allIntoAboutNote[1];
		var statusId=allIntoAboutNote[2];
		var secondayStatusId=allIntoAboutNote[3];
		var userId=allIntoAboutNote[4];
		var districtId=allIntoAboutNote[5];
		var questionId=allIntoAboutNote[6];
		var isJSI=allIntoAboutNote[7];
	ManageStatusAjax.addTeacherStatusNote(teacherId,jobId,statusId,secondayStatusId,userId,districtId,questionId,addNote,isJSI,{
		async: false,
		cache: false,
		errorHandler:handleError,
		callback:function(data)	{
		var resData=data.split('###');
		if(resData[0]=="yes"){
			var successmessage="<div id='addSuccess' class='alert alert-success' style='width:80% !important;vertical-align:middle;padding:0px;padding-left:10px;padding-right:10px;margin-bottom:0px;'><a href='#' class='close' data-dismiss='alert'>&times;</a>"+
			"Note has been added successfully.</div>";
			$('#label'+questionId).before(resData[2]);
			$('[name="questionSuperNotes'+resData[1]+'"]').jqte();		
			$('[name="questionSuperNotes'+resData[1]+'"]').parents().parents('.jqte').css('pointer-events','none');			
			$('[name="questionSuperNotes'+resData[1]+'"]').parents().parents('.jqte').before(successmessage);
			//$('[name="questionNotes'+questionId+'"]').parents().parents('.jqte').before(successmessage);
			$('[name="questionNotes'+questionId+'"]').parents().parents('.jqte').find(".jqte_editor").text('');
			setTimeout(function(){$('#addSuccess').remove();},2000*10);
			}else{					
				var error="<div id='addError' class='alert alert-danger' style='width:80% !important;vertical-align:middle;padding:0px;padding-left:10px;padding-right:10px;margin-bottom:0px;'><a href='#' class='close' data-dismiss='alert'>&times;</a>"+
				"Note could not be added. Please try again.</div>";
				$('[name="questionNotes'+teacherStatusNotesId+'"]').parents().parents('.jqte').before(error);
				setTimeout(function(){$('#addError').remove();},2000*10);
			}
	}
	});
	}else{
		$('[name="questionNotes'+questionId+'"]').parents().parents('.jqte').before(errormessage);
		setTimeout(function(){$('#addError').remove();},2000*10);
	}
}
//add By Ram Nath
function getConfirmEditOrNot(header,confirmMessage,buttonNameChange,finalizeStatus){
	var div="<div class='modal hide in' id='confirmbox' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'>"+
			"<div class='modal-dialog' style='width:550px;'>"+
			"<div class='modal-content'>"+
			"<div class='modal-header'>"+
	        "<!--<button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>-->"+
	        "<h4 class='modal-title' id='myModalLabel' style='color:white;'></h4>"+
	      "</div>"+
	      "<div class='modal-body' id='confirmMessage'>"+	      
	      "</div>"+
	      "<div class='modal-footer'>"+
	      "<button type='button' class='btn btn-primary' id='confirmTrue' ></button>"+
	        "<button type='button' class='btn btn-default' id='confirmFalse'>"+resourceJSON.btnCancel+"</button>"+	        
	      "</div>"+
	    "</div>"+
	  "</div>"+
	"</div>";
	$('#jWTeacherStatusNotesDiv').after(div);
    confirmMessage = confirmMessage || '';
    $('#jWTeacherStatusNotesDiv').modal('hide');
    $('#confirmbox').modal({show:true,
                            backdrop: "static",
                            keyboard: false,
    });

    $('#confirmbox #confirmMessage').html(confirmMessage);
    $('#confirmbox #myModalLabel').html(header);
    $('#confirmbox #confirmTrue').html(buttonNameChange);
    $('#confirmFalse').click(function(){
    	$('#confirmbox').modal('hide');
        $('#confirmbox').remove();
        $('#jWTeacherStatusNotesDiv').modal('show');
    });
    $('#confirmTrue').click(function(){
    	$('#confirmbox').modal('hide');
        $('#confirmbox').remove();
        isClickCountOnEditForSuperUser=0;
        $('#jWTeacherStatusNotesDiv').modal('show');
        saveStatusNote(finalizeStatus);
    });
} 
function setDialogPosition1(qId)
{
	var scrollTopPosition = $(window).scrollTop();
	var x;
	var y;
	$(function() {
		$('#'+qId).hover(function(e) {
		  var offset = $(this).offset();
		  x = (e.pageX - offset.left);
		  y = (e.pageY - offset.top);
		});
		});
	var z=y-180+scrollTopPosition;	
	return z;
}
//$(function(){clickCount=0;});
var objQuestion;
var preQId;
function scroeGrid1(qId){
	try{
		if($('#popoverClick').css('display')=='block' && preQId!=qId){			
		$(objQuestion).popover("hide");
		clickCount=0;
		}
	}catch(e){}
		objQuestion=$('#'+qId).popover({		 
	 	trigger: "manual",
	    html : true,
	    placement: 'left',
	    template: '<div class="popover" id="popoverClick" style="width: 400px;top: 80%;"><div class="arrow" style="top:10px;"></div><div class="popover-inner"><h3 class="popover-title"></h3><div class="popover-content"><p></p></div></div></div>',
	    content:
	    	function(){		
				return '<div id=\"contentEditDivScore1\">'+$('#scoreTable'+qId).html()+'</div>';
					},
		});
}
function clickOnQuestionScore(obj){
		//alert('enter');
		var qId=$(obj).attr('id');		
		//alert(qId);		
		scroeGrid1(qId);
		preQId=qId;
		var height=setDialogPosition1(qId);
	    height=height+100;
	  	var style = $('<style>.popover { position: absolute;top: '+height+'px !important;left: 0px; }</style>')
		$('html > head').append(style);	  	
        var _this = obj;
        if(clickCount==0){  
        $(objQuestion).popover("show");
        clickCount=1;
        }
       else{        	
        	$(objQuestion).popover("hide");
        	clickCount=0;
        }
}
$(document).ready(function(){
	$('#jWTeacherStatusNotesDiv .modal-body').scroll(function() {
		$(objQuestion).popover("hide");
		clickCount=0;
		});
	/*$( "#jWTeacherStatusNotesDiv" ).bind('mousewheel DOMMouseScroll', function(e) {
		$(objQuestion).popover("hide");
		clickCount=0;
		var scrollTo=null;
		if(e.type=='mousewheel'){
			scroolTo=(e.originalEvent.wheelDelta*-1);
		}else if(e.type == 'DOMMouseScroll'){
			scroolTo=40*e.originalEvent.detail;
		}
		if(scrollTo){
			e.preventDefault();
			$(this).scrollTop(scrollTo+$(this).scrollTop());
		}
		
		});      */
});
$(function(){
	$('#eRef #confirmTrue').click(function(){return false;});
	try{
	$('#myModalReferenceCheckConfirm1').hide();
	}catch(e){}
});
function getDateFieldIds(k) {
	  return dateFieldIds[k];
}
function setDateField(obj){
	var currId=$(obj).attr('id');
	if(!getDateFieldIds(currId))
	Calendar.setup({
	    onSelect: function(cal) { cal.hide() },
	    showTime: true
	}).manageFields(currId, currId, "%m-%d-%Y");
	dateFieldIds[currId]=true;
}
//ended by Ram Nath
//add brajesh
function closeKSNMODal(){
	$('#modalKSNId').modal('hide');
}
function acceptedmethod(teacherId){
	var fitScore		=	$("#fitScoreForStatusNote").val();
	var statusId		=	$("#statusIdForStatusNote").val();
	var secondaryStatusId=	$("#secondaryStatusIdForStatusNote").val();
	$('#loadingDiv').show();
	var talentKSNDatailID = document.getElementsByName('teacherStatus');
	var ksnID = "";
	for (var i=0, n=talentKSNDatailID.length;i<n;i++) {
	  if (talentKSNDatailID[i].checked) 
	  {
		  ksnID =talentKSNDatailID[i].value;
	  }
	}
	//$('#jWTeacherStatusNotesDiv').hide();
	$('#jWTeacherStatusNotesDiv').modal('hide');
	
	if(ksnID==''){
		$('#loadingDiv').hide();
		$('#modalSelectKSNId').modal('show');
	}else{
		var flag = 0;
		ManageStatusAjax.changeKSNNumberOfTeacher(fitScore,statusId,secondaryStatusId,ksnID,teacherId,{ 
		 		async: true,	
		 		errorHandler:handleError,
		 		callback:function(data)
		 		{/*
				$('#modalsuccessKSNId').modal('show');
				jQuery("#oktochoseKSNNOo").click(function(){
					$('#modalsuccessKSNId').modal('hide');
					$('#jWTeacherStatusNotesDiv').modal('hide');
					jWTeacherStatusNotesOnOpen(fitScore,statusId,secondaryStatusId);
					$('#jWTeacherStatusNotesDiv').modal('show');
				});
		 		 */
			//sandeep start
					/*$('#loadingDiv').hide();
					$('#modalsuccessKSNId').modal('show');
					if(data!="SUCCESS"){
						$("#internalTxt1").html(data);
						flag =1;
						//$('#modalLinkToKSN').modal('show');
					}
					else{
						flag =0;
						$("#internalTxt1").html("Successfully Changed");
					}*/
			//sandeep start
					//reflectLinkToKsnNodeColor($("#teacherId").val(),$("#currentNode").val());
					/*jQuery("#oktochoseKSNNOo").click(function(){
						$('#loadingDiv').hide();
						if(flag==0)
							$('#modalLinkToKSN').modal('hide');
						else
							$('#modalLinkToKSN').modal('show');
		
					});*/
					
				
					$('#loadingDiv').hide();
					$('#jWTeacherStatusNotesDiv').modal('hide');
					document.getElementById("noteMainDiv").style.display="none";
					document.getElementById("scoreProvided").value="";
					$('#errContinuemsg').html(data);
					$('#modalErrContinue').modal('show');
					
					
		 		}
		     });
	}
}

function declinedKSNID(tchID,flag){
	var fitScore		=	$("#fitScoreForStatusNote").val();
	var statusId		=	$("#statusIdForStatusNote").val();
	var secondaryStatusId=	$("#secondaryStatusIdForStatusNote").val();
	
	$('#loadingDiv').show();
	
	if(tchID!=''){
		if(flag=='D'){
			$('#jWTeacherStatusNotesDiv').modal('hide');
			
			$('#loadingDiv').hide();
			$('#confrmmodalKSNId').modal('show');
			jQuery("#clnId").click(function(){
				$('#jWTeacherStatusNotesDiv').modal('show');
				});
		}else{
			
			ManageStatusAjax.inactiveKSNNumberOfTalentKSMDetail(tchID,flag,{ 
		 		async: true,	
		 		errorHandler:handleError,
		 		callback:function(data)
		 		{
					$('#loadingDiv').hide();
					$('#jWTeacherStatusNotesDiv').modal('hide');
					document.getElementById("noteMainDiv").style.display="none";
					document.getElementById("scoreProvided").value="";
					$('#errContinuemsg').html(data);
					$('#modalErrContinue').modal('show');
		 		}
		     });
		}
	}
}
function declinedKsnIds(){
	var teacherId = document.getElementById("statusTeacherId").value;
	//jWTeacherStatusNotesOnCloseForSLC();
	$('#confrmmodalKSNId').modal('hide');
	$('#jWTeacherStatusNotesDiv').modal('hide');
	$('#loadingDiv').show();
	
	ManageStatusAjax.inactiveKSNNumberOfTalentKSMDetail(teacherId,'D',{ 
 		async: true,	
 		errorHandler:handleError,
 		callback:function(data)
 		{
		
		$('#loadingDiv').hide();
		$('#jWTeacherStatusNotesDiv').modal('hide');
		document.getElementById("noteMainDiv").style.display="none";
		document.getElementById("scoreProvided").value="";
		$('#errContinuemsg').html(data);
		$('#modalErrContinue').modal('show');
		
 		}
     });
}


function showStatusFast(teacherName,doActivity,cgStatus,status,teacherAssessmentStatusId,jobForTeacherId,teacherId)
{
	document.getElementById("referenceValue").value = "";
	document.getElementById("status_l_title").innerHTML="<b>"+teacherName+"</b>&nbsp;|&nbsp;"+resourceJSON.msgCandidateLifecycle;
	$('#myModalStatus').modal('show');
	document.getElementById("statusTeacherId").value=teacherId;
	document.getElementById("jobForTeacherIdForSNote").value=jobForTeacherId;
	document.getElementById("doActivity").value=doActivity;
	//document.getElementById("noOfRecordCheck").value=noOfRecord;
	
	displayStatusDashboardCGOpenSlc(0);
	finalizeOrNot(doActivity);
}

function displayStatusDashboardCGOpenSlc(clickFlag){	
	var jobId = document.getElementById("jobId").value;
	var teacherId = document.getElementById("statusTeacherId").value;
	$('#loadingDiv').show();
	
	var url =window.location.href;
	ManageStatusAjax.displayStatusDashboardCGOpenSlc(teacherId,jobId,clickFlag,
				{ 
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{	$('#loadingDiv').hide();
				document.getElementById("divStatus").innerHTML=data.toString();
				
			}
		});

}
var countKSN=0;
function varifyTchIdANDTmtlntID(teacherid , tmtalentID)
{
	if(tmtalentID!=null && tmtalentID!=0){
		if(teacherid!=tmtalentID){
			$('#jWTeacherStatusNotesDiv').modal('hide');
			$('#confrmteacheridTmTlntId').modal('show');
			jQuery("#btnKSNAPIButtonId").click(function(){
				/*if(countKSN==0){
				countKSN=1;
				$('#loadingDiv').show();
				ManageStatusAjax.callAPIMethod(teacherid,{ 
			 		async: true,	
			 		errorHandler:handleError,
			 		callback:function(data)
			 		{

						$('#jWTeacherStatusNotesDiv').modal('show');
						$('#loadingDiv').hide();
			 		}
			     });
			}*/
				$('#loadingDiv').hide();
				$('#confrmteacheridTmTlntId').hide();	
				$('#jWTeacherStatusNotesDiv').modal('show');
			});
			/*jQuery("#clnKSNId").click(function(){
			$('#confrmteacheridTmTlntId').hide();	
			$('#jWTeacherStatusNotesDiv').modal('show');
		});*/
		}
	}
}		
function closeTalentDetailListDiv(){
	
	$("#confrmteacheridTmTlntId").modal('hide');
	$('#jWTeacherStatusNotesDiv').modal('show');
}

function closeTalentDiv(){
	$("#modalSelectKSNId").modal('hide');
	$('#jWTeacherStatusNotesDiv').modal('show');
	
}
function closeTalentSuccessDiv(){
	$("#modalsuccessKSNId").modal('hide');
	$('#jWTeacherStatusNotesDiv').modal('show');
	
}
function closeDeclineTalentDiv(){
	$("#confrmmodalKSNId").modal('hide');
	$('#jWTeacherStatusNotesDiv').modal('show');
	
}
function closeErr100(){
	
	$("#modalErr100").modal('hide');
	$('#myModalStatus').modal('show');
	displayStatusDashboard(1);
	//jWTeacherStatusNotesOnClose();
	var statusUpdateFromTP=0;
	try { statusUpdateFromTP=document.getElementById("statusUpdateFromTP").value;} catch (e) {}
	if(statusUpdateFromTP=="0")
	{
		getCandidateGrid();
		refreshStatus();
	}
	
	
}



function closeContinue(){
	
	$("#modalErrContinue").modal('hide');
	$('#myModalStatus').modal('show');
	displayStatusDashboard(1);
	//jWTeacherStatusNotesOnClose();
	var statusUpdateFromTP=0;
	try { statusUpdateFromTP=document.getElementById("statusUpdateFromTP").value;} catch (e) {}
	if(statusUpdateFromTP=="0")
	{
		getCandidateGrid();
		refreshStatus();
	}
	
	
}


function getTalentKSNDetail()
{
	//$('#talentKSNDetailDataId').html("<img src='images/loadingAnimation.gif'/>");
	var teacherId = document.getElementById("statusTeacherId").value;
	var secondaryStatusId=document.getElementById("secondaryStatusIdForStatusNote").value;
	//alert("teacherId "+teacherId+" secondaryStatusId "+secondaryStatusId)
	try{
		ManageStatusAjax.getTalentKSNDetail(teacherId,secondaryStatusId,
		{
			async:true,
			errorHandler:handleError,
			callback:function(data)
			{
				$('#talentKSNDetailDataId').html(data);
				applyScrollOnStatusNote("tblStatusNote_2");
				$('#loadingDiv').hide();
			}
		});
	}catch(err){}
	
}

function redirectToEventPage(jobId,teacherId)
{
	$("#loadingDiv").fadeIn();	
	CandidateGridSubAjax.saveTempCGEventDetail(jobId,teacherId,{
		async: false,
		callback: function(data)
		{
		    $("#loadingDiv").hide();
		   /// window.open("manageevents.do", '_blank');
		    window.location.href ="manageevents.do"
		},
		errorHandler:handleError	
	});	
	
} 

function hideUnhideAppBtn(hideUnhideflag){
	if(hideUnhideflag=="0"){
		document.getElementById("onlyDec").style.display="block";
		document.getElementById("appDec").style.display="none";
	}
	else if(hideUnhideflag=="1"){
		document.getElementById("onlyDec").style.display="none";
		document.getElementById("appDec").style.display="block";
	}
}

function closeModalsuccessKSNId(){
	$('#modalsuccessKSNId').modal('hide');
	$('#modalLinkToKSN').modal('show');
}

function keyNotWork(e) {
 	var keynum = ( e.which ) ? e.which : e.keyCode;  
 	if (event.keyCode == 8 || event.keyCode == 46) 
 	    return false;
 	else
 		return false;
}


function jWTeacherStatusNotesOnOpenOp(fitScore,statusId,secondaryStatusId)
{	
	dateFieldIds = new Object();
	//getFinalizeSpecificTemplateList(null,0);	//get FinalizeSpecificTemplateList for Teacher added by Shadab Ansari
	//getFinalizeSpecificTemplateList(null,1);	//get FinalizeSpecificTemplateList for DA/SA added by Shadab Ansari
	
	checkFirstClick=true;//added by 03-04-2015
	document.getElementById("undo").style.display="none";
	try{ document.getElementById("txtNewReqNo").value=""; }catch(err){}
	try{ $("#rdReqPre").prop("checked", true); }catch(err){}
	
	var offerReady="";
	try{
		offerReady=document.getElementById("offerReady").value;
	}catch(err){}
	if(offerReady==""){
		try{
			document.getElementById("schoolIdCG").value=0;
		}catch(err){}
		try{
			document.getElementById("schoolNameCG").value="";
		}catch(err){}
	}
	var isReqNoForHiring=0;
	try{
		isReqNoForHiring=document.getElementById("isReqNoForHiring").value;
	}catch(err){}
	if(isReqNoForHiring==0){
		try{
			document.getElementById("reqstar").style.display="none";
		}catch(err){}
	}else{
		try{
			document.getElementById("reqstar").style.display="inline";
		}catch(err){}
	}
	
	
	var jobForTeacherId=document.getElementById("jobForTeacherIdForSNote").value;
	currentPageFlag="statusNote";
	document.getElementById("statusIdForStatusNote").value=statusId;
	document.getElementById("fitScoreForStatusNote").value=fitScore;
	document.getElementById("secondaryStatusIdForStatusNote").value=secondaryStatusId;
	var teacherId = document.getElementById("statusTeacherId").value;
	var jobId = document.getElementById("jobId").value;
	$('#myModalStatus').modal('hide');
	$('#errorStatusNote').hide();
	$('#loadingDiv').css('z-index','999999');
	$('#loadingDiv').show();
	var doNotShowPanelinst=0;
	var offerReq=0;
	$('#errorStatusNoteRef').empty();
	try{
		if(document.getElementById("panelOpenFlag"))
			doNotShowPanelinst = 1;
	}catch(e){}
	ManageStatusAjax.getStatusNote_op(jobForTeacherId,fitScore,teacherId,jobId,statusId,secondaryStatusId,pageSNote,noOfRowsSNote,sortOrderStrSNote,sortOrderTypeSNote,doNotShowPanelinst,
	{
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			document.getElementById("jobCategoryDiv").value=2;
			$('#loadingDiv').hide();			
			if(data==1)
				document.getElementById("divStatusNoteGrid").innerHTML=""+resourceJSON.MsgNotStartedOrNotCompleted+"";
			else if(data==2)
				document.getElementById("divStatusNoteGrid").innerHTML=""+resourceJSON.MsgTimeOutTheJSI+"";
			else
				document.getElementById("divStatusNoteGrid").innerHTML=data.split("##")[0];
			
			var disFlag=0;
			if(document.getElementById("panelOpenFlag"))
			{
				
			}
			else
			$('#jWTeacherStatusNotesDiv').modal('show');
			document.getElementById("checkOptions").style.display="inline";
			finalizeOrNot(document.getElementById("doActivity").value);
			var statusName = document.getElementById("statusName").value;
			if(statusName == "Withdrew" || statusName == "Declined"){
				$("#timeAndReason").show();
			}
			var ht=data.split("##")[11];
			var ht1=data.split("##")[12];
			if(data.split("##")[11]!=null || data.split("##")[11]!='0'){
				$("#timingforDclWrdw").val(ht);
			}
			if(data.split("##")[12]!=null || data.split("##")[12]!='0'){
				$("#reasonforDclWrdw").val(ht1);
			}
			
			try{
				if(statusName == resourceJSON.msgHired2){
					document.getElementById("hiredtext").innerHTML=resourceJSON.msgHired2+" Date";
				}else if(statusName == "Hired"){
					document.getElementById("hiredtext").innerHTML="Hired Date";
				}
			}catch(e){}
			try{
				if(data.split("##")[10]==''){
					if(statusName == resourceJSON.lblLinkToKsn || statusName == resourceJSON.lblINVWF1 || statusName == resourceJSON.lblWF1COM  || statusName == resourceJSON.lblINVWF2  || statusName == resourceJSON.lblWF2COM  || statusName == resourceJSON.lblINVWF3   || statusName == resourceJSON.lblWF3COM || statusName == resourceJSON.lblScreening){
						finalizeOrNot(false);
					}
				}
			}catch(e){}
			
			try{
				var entityType=$('[name="entityType"]').val();
				
				if(statusName==resourceJSON.lblSPStatus && (entityType==5 || entityType==6)){
					document.getElementById("statusAddNote").style.display="none";
					document.getElementById("expNotePdf").style.display="none";
					document.getElementById("spSelectDiv").style.display="inline";
					document.getElementById("undo").style.display="inline";
				}else{
					document.getElementById("spSelectDiv").style.display="none";
				}
				
				if((statusName==resourceJSON.lblSPStatus || statusName==resourceJSON.lblPrescreen) && (entityType==5 || entityType==6)){
					if(data.split("##")[10]!=null && data.split("##")[10]=='waived'){
						document.getElementById("waived").style.display="inline";
					}else{
						document.getElementById("waived").style.display="none";
					}
				}else{
					document.getElementById("waived").style.display="none";
				}
			}catch(err){}
			try{
				if(statusName=='Offer Ready'){
					if(data.split("##")[7]!=null && data.split("##")[7]=='resend'){
						document.getElementById("resend").style.display="inline";
					}else{
						document.getElementById("resend").style.display="none";
					}
					var offerShowFlag="0";
					offerShowFlag=document.getElementById("doActivityForOfferReady").value;
					if(offerShowFlag=="0"){
						finalizeOrNoForOfferReady(offerShowFlag);
					}
				}else{
					document.getElementById("resend").style.display="none";
				}
			}catch(err){}
			try{
				var entityType=$('[name="entityType"]').val();
				if(statusName=='Credential Review' && entityType==3){
					var offerShowFlag="0";
					offerShowFlag=document.getElementById("doActivityForOfferReady").value;
					if(offerShowFlag=="0" || offerShowFlag==""){
						finalizeOrNoForOfferReady(offerShowFlag);
					}
				}
			}catch(err){}
			
			if(data.split("##")[1]!='null' && data.split("##")[6]!='JSI' && statusName!='Offer Ready'){				
				if(document.getElementById("statusSave").style.display=='inline'){
					if(data.split("##")[1]=='H'){
						disFlag=1;
						document.getElementById("undo").style.display="none";
						document.getElementById("unRemove").style.display="none";
						document.getElementById("unDecline").style.display="none";
						document.getElementById("unHire").style.display="inline";
						document.getElementById("unWithdraw").style.display="none";
					}else if(data.split("##")[1]=='D'){
						document.getElementById("undo").style.display="none";
						document.getElementById("unRemove").style.display="none";
						document.getElementById("unDecline").style.display="inline";
						document.getElementById("unHire").style.display="none";
						document.getElementById("unWithdraw").style.display="none";
					}else if(data.split("##")[1]=='R'){
						document.getElementById("undo").style.display="none";
						document.getElementById("unRemove").style.display="inline";
						document.getElementById("unDecline").style.display="none";
						document.getElementById("unHire").style.display="none";
						document.getElementById("unWithdraw").style.display="none";
					}else if(data.split("##")[1]=='S'){
						document.getElementById("undo").style.display="inline";
						document.getElementById("unRemove").style.display="none";
						document.getElementById("unDecline").style.display="none";
						document.getElementById("unHire").style.display="none";
						document.getElementById("unWithdraw").style.display="none";
					}else if(data.split("##")[1]=='W'){
						document.getElementById("undo").style.display="none";
						document.getElementById("unRemove").style.display="none";
						document.getElementById("unDecline").style.display="none";
						document.getElementById("unHire").style.display="none";
						document.getElementById("unWithdraw").style.display="inline";
					}
				}
			}else{				
				if(data.split("##")[6]=='JSI'){
					document.getElementById("statusFinalize").style.display="none";
					document.getElementById("statusSave").style.display="none";
					document.getElementById("checkOptions").style.display="none";
				}
				if(statusName=='Offer Ready'){
					requisitionNumbers();
					offerReq=1;
				}
				
				if(data.split("##")[1]=='S'){
					document.getElementById("undo").style.display="inline";
				}else{
					document.getElementById("undo").style.display="none";
				}
				document.getElementById("unRemove").style.display="none";
				document.getElementById("unDecline").style.display="none";
				document.getElementById("unHire").style.display="none";
				document.getElementById("unWithdraw").style.display="none";
				
			}			
			var checkOnlineActivity="0";
			try{
				checkOnlineActivity=document.getElementById("divOnlineActivityForCG").innerHTML;
			}catch(err){}
			if(checkOnlineActivity==""){
				displayOnlineActivityForCG();
			}
			scroeGrid();
			applyScrollOnStatusNote();
			applyScrollOnOnlineActivity();
			applyScrollOnTblTrans();
			applyScrollOnTblTransAcademic();
			applyScrollOnTblReferenceCheck();
			var entityType=$('[name="entityType"]').val();
			var hiredFlag=0;			
			if(document.getElementById("statusIdForStatusNote").value!=null){
				if(document.getElementById("statusIdForStatusNote").value==6){
					hiredFlag=1;
				}
			}			
			try{
				if(hiredFlag==0 && offerReq==0){
					document.getElementById("requisitionNumbers").innerHTML="";
					document.getElementById("requisitionNumbersGrid").style.display="none";
				}
			}catch(err){}
			
			document.getElementById("hireFlag").value=disFlag;
			var txtschoolCount=0;
			try { 
					txtschoolCount=document.getElementById("txtschoolCount").value;
				} catch (e) {}
				
			if(txtschoolCount > 1 && disFlag==0){
				if(hiredFlag==1)
				requisitionNumbers();
				document.getElementById("schoolAutoSuggestDivId").style.display="block";
				try{
					document.getElementById("requisitionNumber").style.display="block";
				}catch(err){}
			}else{
				if(disFlag==0){
					if(hiredFlag==1)
					requisitionNumbers();
				}else{
					if(offerReq==0){
						try{
							document.getElementById("requisitionNumbersGrid").style.display="none";
						}catch(err){}
						try{
							document.getElementById("requisitionNumber").style.display="none";
						}catch(err){}
					}
				}
				// changes display block
				if($("#statusName").val()=="Asst Supe Interview" || ($('#isRequiredSchoolIdCheck').val()==1 && $('#isSchoolOnJobId').val()==1))
				{					
					document.getElementById("schoolAutoSuggestDivId").style.display="block";
					$('#schoolNameCG').prop('disabled', false);
					if($('#alreadyExistsSchoolInStatus').val()==1){
						if(entityType== 2)
						{		
							document.getElementById("schoolNameCG").value = $('#schoolNameValue').val().replace(/!!/g , " ");
							document.getElementById("schoolIdCG").value = $('#schoolIdValue').val();
							$('#schoolNameCG').prop('disabled', true);
						}
					}
				}
				else
				{
					document.getElementById("schoolAutoSuggestDivId").style.display="none";
				}
				//Only for Jeffco added Date 10-09-2015
				try{
					try{
						$('#startDateDiv').remove();
					}catch(e){}
					if($("[name='slcStartDateOnOrOff']").val()=="ON" && $("#districtId").val()=='804800' && $("#statusName").val()=='Offer Made'){
						$('#schoolAutoSuggestDivId').after('<div class="row mt5" id="startDateDiv"><div class="span12 top5 col-md-2 form-group" style="width:100%;">'+
								'<label for="text" style="float:left;margin-right:5px;padding-top:7px;">Start Date<span class="required">*</span></label>'+
								'<div><input type="text" value="" id="startDateId" name="startDate" class="form-control" maxlength="50" onmouseup="setDateField(this);" onkeydown="return keyNotWork(event);" oncontextmenu="return false;" style="width:150px;"></div>'+
								'</div></div>');
						$('[name="startDate"]').val($('[name="isStartDate"]').val());
					}
					}catch(e){}
				//ENDed Only for Jeffco added Date 10-09-2015
			}			
			var txtOverride=0;
			try { 
				txtOverride=document.getElementById("txtOverride").value;
				} catch (e) {}
			
				try{
					if(data.split("##")[2]!=null && data.split("##")[2]!='null'){
						try{
							// changes display block
							document.getElementById("schoolAutoSuggestDivId").style.display="none";
						}catch(err){}
						/*try{
							document.getElementById("requisitionNumbersGrid").style.display="none";
						}catch(err){}
						try{
							document.getElementById("requisitionNumber").style.display="none";
						}catch(err){}*/
					}
				}catch(err){}
					
							
			if(txtOverride==1)
			{
				if(document.getElementById("panelOpenFlag"))
				{
				}
				else
				{
					if(offerReady=="")
					{
						$('#spanOverride').modal('show');
					}
					
				}
			}
			else
			{
					$('#spanOverride').modal('hide');
			
			}			
			if(document.getElementById("panelOpenFlag"))
			{			
				$('#checkOptions').hide();
				$('#statusSave').hide();
			}
			try{
				if(data.split("##")[4]!=null && data.split("##")[4]!='null'){
					try{
						document.getElementById("jobCategoryFlag").value=data.split("##")[4];
					}catch(err){}
				}
			}catch(err){}
			
			$('textarea').jqte();
			getQuestionNotesIds();
			
			//mukesh
			//alert(data.split("##")[3]);
			if(data.split("##")[3]=='true')
			{
			   document.getElementById('email_da_sa').checked=true;
			}
			
			try{
				if(data.split("##")[5]!='' && data.split("##")[5]!='null'){
					var date = new Date(data.split("##")[5]);
					document.getElementById("setHiredDate").value=(date.getMonth() + 1) + '-' + date.getDate() + '-' +  date.getFullYear();
				} else {
					try{
							var date = new Date();
							document.getElementById("setHiredDate").value=(date.getMonth() + 1) + '-' + date.getDate() + '-' +  date.getFullYear();
					} catch (err){}
				}
			}catch(err){}
			
			try{
			if($("#statusName").val()!=="Asst Supe Interview")
			{				
				document.getElementById("strengthOppDiv").style.display="none";
				document.getElementById("recomDiv").style.display="none";				
				document.getElementById("remQuesDiv").style.display="none";				
			}
			}catch(e){}
			
			try{
			if($("#statusName").val()=="Asst Supe Interview")
			{
				try
				{
					document.getElementById('candidateDetails').style.display="block";
					document.getElementById("strengthOppDiv").style.display="block";
					document.getElementById("strengthOppDiv").innerHTML = data.split("##")[8];
					
					
					if(data.split("##")[9].length > 0)
					{	
						if(entityType.value == 2)
						{
							document.getElementById("schoolAutoSuggestDivId").style.display="block";
							document.getElementById("schoolNameCG").value = data.split("##")[9].split("@@@")[0];
							document.getElementById("schoolIdCG").value = data.split("##")[9].split("@@@")[1];
							$('#schoolNameCG').prop('disabled', 'disabled');
						}
						else
						{
							document.getElementById("schoolAutoSuggestDivId").style.display="none";
						}
					}

				}
				catch(e){}
				/*for(var i=1;i<13;i++)
				{
					if(i%2==0)
					{
						$('#td'+i).html("<textarea name='growth"+i+"' id='growth"+i+"' class='form-control' maxlength='500' rows='5' style='width:240px;resize: none;' ></textarea>");
					}
					else
					{
						$('#td'+i).html("<textarea name='strength"+i+"' id='strength"+i+"' class='form-control' maxlength='500' rows='5' style='width:240px;resize: none;' ></textarea>");
					}
				}*/
			}
			else
			{
				document.getElementById("recomDiv").style.display="none";
				document.getElementById("remQuesDiv").style.display="none";
				document.getElementById('candidateDetails').style.display="none";
				document.getElementById("strengthOppDiv").style.display="none";
			}
			}catch(e){}
			
			//added key contact 
			//alert($('[name="isSuperAdministration"]').val());
			clickCount=0;
			if($('[name="isSuperAdministration"]').val()==1){
			callEnableOrDisableJqte();
			}
			//end key contact 
			
			//added by ram 21-04-2015 for if hired candidate
			var hiredFlagForClickOnStatus=0;
			//alert("hiredFlagForClickOnStatus==============="+hiredFlagForClickOnStatus);
			try{
				hiredFlagForClickOnStatus=$('[name="hiredFlagForClickOnStatus"]').val();
			}catch(e){}
			try{
				$('#jWTeacherStatusNotesDiv #checkOptions #email_da_sa_div .left16 #email_da_sa').parent().addClass('notclickable');
				$('#jWTeacherStatusNotesDiv #checkOptions #email_da_sa_div .left16 a').addClass('notclickable');
			}catch(e){}	
			try{
			$('.notclickable').css('pointer-events','');
			}catch(e){}	
			var entityType=$('[name="entityType"]').val();
			if(hiredFlagForClickOnStatus==1){
				
				var curStatus=$("#statusName").val();
				if(curStatus!=='Declined' && curStatus!=='Rejected' && curStatus!=='Hired' && curStatus!=='Withdraw'){
					//$('#jWTeacherStatusNotesDiv .modal-footer .btn').hide();
					$('#jWTeacherStatusNotesDiv #undo').hide();
					$('#jWTeacherStatusNotesDiv #statusSave').hide();
					$('#jWTeacherStatusNotesDiv #statusFinalize').hide();
					$('#jWTeacherStatusNotesDiv #cancelButtonRefChk').show();
					setTimeout(function(){$('.notclickable').css('pointer-events','none');},10);					
				}
				else if(entityType==2 && (curStatus=='Declined' || curStatus=='Rejected' || curStatus=='Hired' || curStatus=='Withdraw')){
					$('#jWTeacherStatusNotesDiv #statusSave').show();
					$('#jWTeacherStatusNotesDiv #statusFinalize').show();
					$('#jWTeacherStatusNotesDiv #cancelButtonRefChk').show();
					setTimeout(function(){$('.notclickable').css('pointer-events','');},5);
				}
			}			
			//ended by ram 21-04-2015
			setTimeout(function(){
			try{
			$('.districtTooltip').tooltip();
			}catch(e){}
			},100);
			applyScrollOnStatusNotes("tblStatusNote");
			applyScrollOnStatusNote("tblStatusNote_2");
			try{
				var entityType=$('[name="entityType"]').val();
				if(entityType==5 || entityType==6){
					document.getElementById("statusSave").style.display="none";
				}
			}catch(err){}
		}
	});
}