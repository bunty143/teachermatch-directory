package tm.controller.assessment;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.validation.DataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import tm.bean.JobOrder;
import tm.bean.assessment.AssessmentDetail;
import tm.bean.assessment.AssessmentQuestions;
import tm.bean.assessment.AssessmentSections;
import tm.bean.assessment.QuestionOptions;
import tm.bean.assessment.StageThreeStatus;
import tm.bean.assessment.StageTwoStatus;
import tm.bean.districtassessment.DistrictAssessmentDetail;
import tm.bean.districtassessment.DistrictAssessmentQuestionOptions;
import tm.bean.districtassessment.DistrictAssessmentQuestions;
import tm.bean.districtassessment.DistrictAssessmentSection;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictSpecificQuestions;
import tm.bean.master.OptionsForDistrictSpecificQuestions;
import tm.bean.master.QqQuestionSets;
import tm.bean.master.QuestionTypeMaster;
import tm.bean.user.UserMaster;
import tm.dao.JobOrderDAO;
import tm.dao.assessment.AssessmentDetailDAO;
import tm.dao.assessment.AssessmentQuestionsDAO;
import tm.dao.assessment.AssessmentSectionDAO;
import tm.dao.assessment.StageOneStatusDAO;
import tm.dao.assessment.StageThreeStatusDAO;
import tm.dao.assessment.StageTwoStatusDAO;
import tm.dao.districtassessment.DistrictAssessmentDetailDAO;
import tm.dao.districtassessment.DistrictAssessmentQuestionsDAO;
import tm.dao.districtassessment.DistrictAssessmentSectionDAO;
import tm.dao.hqbranchesmaster.HeadQuarterMasterDAO;
import tm.dao.master.CompetencyMasterDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.DistrictSpecificQuestionsDAO;
import tm.dao.master.DomainMasterDAO;
import tm.dao.master.ObjectiveMasterDAO;
import tm.dao.master.QqQuestionSetsDAO;
import tm.dao.master.QuestionTypeMasterDAO;
import tm.dao.menu.RoleAccessPermissionDAO;

/* @Author: Vishwanath Kumar
 * @Discription: Assessment Controller.
 */
@Controller
public class AssessmentController {

	
	@Autowired
	private DistrictAssessmentQuestionsDAO districtAssessmentQuestionsDAO;
	
	@Autowired
	private DistrictAssessmentSectionDAO districtAssessmentSectionDAO;
	
	@Autowired
	private DistrictAssessmentDetailDAO districtAssessmentDetailDAO;
	
	@Autowired
	private HeadQuarterMasterDAO headQuarterMasterDAO;
	
	@Autowired
	private AssessmentDetailDAO assessmentDetailDAO;
	public void setAssessmentDetailDAO(AssessmentDetailDAO assessmentDetailDAO) {
		this.assessmentDetailDAO = assessmentDetailDAO;
	}

	@Autowired
	private JobOrderDAO jobOrderDAO;
	public void setJobOrderDAO(JobOrderDAO jobOrderDAO) {
		this.jobOrderDAO = jobOrderDAO;
	}

	@Autowired
	private AssessmentSectionDAO assessmentSectionDAO;
	public void setAssessmentSectionDAO(
			AssessmentSectionDAO assessmentSectionDAO) {
		this.assessmentSectionDAO = assessmentSectionDAO;
	}
	
	@Autowired
	private DomainMasterDAO domainMasterDAO;
	public void setDomainMasterDAO(DomainMasterDAO domainMasterDAO) {
		this.domainMasterDAO = domainMasterDAO;
	}
	
	@Autowired
	private CompetencyMasterDAO competencyMasterDAO;
	public void setCompetencyMasterDAO(CompetencyMasterDAO competencyMasterDAO) {
		this.competencyMasterDAO = competencyMasterDAO;
	}
	
	@Autowired
	private ObjectiveMasterDAO objectiveMasterDAO;
	public void setObjectiveMasterDAO(ObjectiveMasterDAO objectiveMasterDAO) {
		this.objectiveMasterDAO = objectiveMasterDAO;
	}
	
	@Autowired
	private QuestionTypeMasterDAO questionTypeMasterDAO;
	public void setQuestionTypeMasterDAO(QuestionTypeMasterDAO questionTypeMasterDAO) {
		this.questionTypeMasterDAO = questionTypeMasterDAO;
	}
	
	@Autowired
	private AssessmentQuestionsDAO assessmentQuestionsDAO;
	public void setAssessmentQuestionsDAO(AssessmentQuestionsDAO assessmentQuestionsDAO) {
		this.assessmentQuestionsDAO = assessmentQuestionsDAO;
	}
	
	@Autowired
	private RoleAccessPermissionDAO roleAccessPermissionDAO;
	public void setRoleAccessPermissionDAO(RoleAccessPermissionDAO roleAccessPermissionDAO) {
		this.roleAccessPermissionDAO = roleAccessPermissionDAO;
	}

	@Autowired
	private DistrictSpecificQuestionsDAO districtSpecificQuestionsDAO;
	
	@Autowired
	private StageOneStatusDAO stageOneStatusDAO;
	
	@Autowired
	private StageTwoStatusDAO stageTwoStatusDAO;
	
	@Autowired
	private StageThreeStatusDAO stageThreeStatusDAO;
	@Autowired
	private QqQuestionSetsDAO qqQuestionSetsDAO;
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	@InitBinder
	public void initBinder(HttpServletRequest request,DataBinder binder) 
	{
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		dateFormat.setLenient(false);
		binder.registerCustomEditor(Date.class,new CustomDateEditor(dateFormat, true,10));
	}
	
	/* @Author: Vishwanath Kumar
	 * @Discription: Assessment View.
	 */
	@RequestMapping(value="/assessment.do", method=RequestMethod.GET)
	public String getAssessments(ModelMap map,HttpServletRequest request)
	{
		System.out.println("controller assessment.do AssessmentController getAssessments()");

		UserMaster userMaster=null;
		HttpSession session = request.getSession(false);
		int roleId=0;
		if (session == null || session.getAttribute("userMaster") == null) 
		{
			return "redirect:index.jsp";
		}else{
			userMaster=	(UserMaster) session.getAttribute("userMaster");
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
		}
		String roleAccess=null;
		try{
			roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,20,"assessment.do",0);
		}catch(Exception e){
			e.printStackTrace();
		}
		System.out.println("roleAccess="+roleAccess);
		map.addAttribute("roleAccess", roleAccess);

		List<JobOrder> jobOrders = null;
		try 
		{
			//jobOrders = jobOrderDAO.findValidJobOrders(null);
			//map.addAttribute("jobOrders", jobOrders);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return "assessment";
	}
	/* @Author: Vishwanath Kumar
	 * @Discription: Assessment Section View.
	 */
	@RequestMapping(value="/assessmentsections.do", method=RequestMethod.GET)
	public String getAssessmentSections(ModelMap map,HttpServletRequest request)
	{
		System.out.println("controller assessmentSections.do AssessmentController getAssessmentSections()");
		UserMaster userMaster=null;
		HttpSession session = request.getSession(false);
		int roleId=0;
		if (session == null || session.getAttribute("userMaster") == null) 
		{
			return "redirect:index.jsp";
		}else{
			userMaster=	(UserMaster) session.getAttribute("userMaster");
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
		}
		String roleAccess=null;
		try{
			roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,21,"assessmentsections.do",0);
		}catch(Exception e){
			e.printStackTrace();
		}
		System.out.println("roleAccess="+roleAccess);
		map.addAttribute("roleAccess", roleAccess);
		
		String assessmentId = request.getParameter("assessmentId");
		AssessmentDetail assessmentDetail = null;
		
		try 
		{
			if(assessmentId!=null && assessmentId.trim().length()>0)
			{
				assessmentDetail = assessmentDetailDAO.findById(Integer.parseInt(assessmentId), false, false);
			}
			map.addAttribute("assessmentDetail", assessmentDetail);	
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return "assessmentsections";
	}
	/* @Author: Vishwanath Kumar
	 * @Discription: Assessment Question View.
	 */
	@Transactional
	@RequestMapping(value="/assessmentquestions.do", method=RequestMethod.GET)
	public String getAssessmentQuestions(ModelMap map,HttpServletRequest request)
	{
		System.out.println("controller assessmentSections.do AssessmentController getAssessmentQuestions()");
		HttpSession session = request.getSession(false);
		UserMaster userMaster=null;
		int roleId=0;
		if (session == null || session.getAttribute("userMaster") == null) 
		{
			return "redirect:index.jsp";
		}else{
			userMaster=	(UserMaster) session.getAttribute("userMaster");
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
		}
		String roleAccess=null;
		try{
			roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,22,"assessmentquestions.do",0);
		}catch(Exception e){
			e.printStackTrace();
		}
		System.out.println("roleAccess="+roleAccess);
		map.addAttribute("roleAccess", roleAccess);
		String assessmentId = request.getParameter("assessmentId");
		String sectionId = request.getParameter("sectionId");
		List<AssessmentSections> assessmentSections = null;
		AssessmentSections assessmentSection = null;
		try 
		{
			if(sectionId!=null && sectionId.trim().length()>0)
			{
				assessmentSection = assessmentSectionDAO.findById(Integer.parseInt(sectionId), false, false);
			}
			map.addAttribute("assessmentSection", assessmentSection);	
			map.addAttribute("domainMasters", domainMasterDAO.getActiveDomains());
			assessmentSections = assessmentSectionDAO.getSectionsByAssessmentId(assessmentSection.getAssessmentDetail());
			//System.out.println("JJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJkkk");
			
		map.addAttribute("assessmentSections", assessmentSections);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return "assessmentquestions";
	}
	/* @Author: Vishwanath Kumar
	 * @Discription: Assessment Section Question View.
	 */
	@RequestMapping(value="/assessmentsectionquestions.do", method=RequestMethod.GET)
	public String getAssessmentSectionQuestions(ModelMap map,HttpServletRequest request)
	{
		System.out.println("controller assessmentSections.do AssessmentController getAssessmentSectionQuestions()");
		HttpSession session = request.getSession(false);
		UserMaster userMaster=null;
		int roleId=0;
		if (session == null || session.getAttribute("userMaster") == null) 
		{
			return "redirect:index.jsp";
		}else{
			userMaster=	(UserMaster) session.getAttribute("userMaster");
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
		}
		String roleAccess=null;
		try{
			roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,23,"assessmentsectionquestions.do",0);
		}catch(Exception e){
			e.printStackTrace();
		}
		System.out.println("roleAccess="+roleAccess);
		map.addAttribute("roleAccess", roleAccess);
		String sectionId = request.getParameter("sectionId");
		String assessmentQuestionId = request.getParameter("assessmentQuestionId");
		AssessmentSections assessmentSection = null;
		AssessmentQuestions assessmentQuestion = null;
		try 
		{
			if(sectionId!=null && sectionId.trim().length()>0)
				assessmentSection = assessmentSectionDAO.findById(Integer.parseInt(sectionId), false, false);
			
			List<QuestionTypeMaster> questionTypeMasters = questionTypeMasterDAO.getActiveQuestionTypes();
			JSONObject json = new JSONObject();
			for (QuestionTypeMaster questionTypeMaster : questionTypeMasters) {
				json.put(""+questionTypeMaster.getQuestionTypeId()+"", questionTypeMaster.getQuestionTypeShortName());
			}
			map.addAttribute("assessmentSection", assessmentSection);	
			map.addAttribute("domainMasters", domainMasterDAO.getActiveDomains());
			map.addAttribute("questionTypeMasters", questionTypeMasters);
			map.addAttribute("json", json);
			map.addAttribute("stageOneStatus", stageOneStatusDAO.getActiveStageOne());
			map.addAttribute("stageTwoStatus", stageTwoStatusDAO.getActiveStageTwo());
			map.addAttribute("stageThreeStatus", stageThreeStatusDAO.getActiveStageThree());

			if(assessmentQuestionId!=null && assessmentQuestionId.trim().length()>0)
			{
				assessmentQuestion = assessmentQuestionsDAO.findById(Integer.parseInt(assessmentQuestionId), false, false);
				List<QuestionOptions> questionOptions = assessmentQuestion.getQuestionsPool().getQuestionOptions();
				JSONArray assessmentQuestionsArray = new JSONArray();
				JSONObject jsonQuestion = new JSONObject();
				for (QuestionOptions questionOption : questionOptions) {
					jsonQuestion.put("optionId", questionOption.getOptionId());
					jsonQuestion.put("questionOption", questionOption.getQuestionOption());
					jsonQuestion.put("score", questionOption.getScore());
					jsonQuestion.put("rank", questionOption.getRank());
					assessmentQuestionsArray.add(jsonQuestion);
				}
				
				map.addAttribute("assessQues", assessmentQuestionsArray);
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return "assessmentsectionquestions";
	}
	
	/* @Author: Vishwanath Kumar
	 * @Discription: District Question View.
	 */
	@Transactional
	@RequestMapping(value="/districtquestions.do", method=RequestMethod.GET)
	public String getDistrictQuestions(ModelMap map,HttpServletRequest request)
	{
		System.out.println("controller districtquestions.do AssessmentController getDistrictQuestions()");
		try {
			HttpSession session = request.getSession(false);
			UserMaster userMaster=null;
			DistrictMaster districtMaster = null;
			Integer districtId =null;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getEntityType()==3){
					return "redirect:index.jsp";
				}
				session.setAttribute("displayType","0");
				districtMaster = userMaster.getDistrictId();
				if(districtMaster!=null)
				districtId = districtMaster.getDistrictId();
			}
			districtquestionsmethod(map, request, userMaster, districtId);
			/*try{
				String distId = request.getParameter("districtId");
				if(distId!=null && userMaster.getEntityType()==1)
				 districtId = Integer.parseInt(distId); 
			}catch(Exception e){
				e.printStackTrace();
			}
			//System.out.println("roleAccess="+roleAccess);
			map.addAttribute("userMaster", userMaster);
			map.addAttribute("districtId", districtId);
			map.addAttribute("dt",request.getParameter("dt"));
			
			System.out.println("districtId: ::: "+districtId);*/
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "districtquestions";
	}
	
	/* @Author: Vishwanath Kumar
	 * @Discription: District Question View.
	 */
	@RequestMapping(value="/districtspecificquestions.do", method=RequestMethod.GET)
	public String getDistrictQuestion(ModelMap map,HttpServletRequest request)
	{
		System.out.println("controller districtspecificquestions.do AssessmentController getDistrictQuestion()");
		HttpSession session = request.getSession(false);
		UserMaster userMaster=null;
		DistrictMaster districtMaster;
		int roleId=0;
		if (session == null || session.getAttribute("userMaster") == null) 
		{
			return "redirect:index.jsp";
		}else{
			userMaster=	(UserMaster) session.getAttribute("userMaster");
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
			districtMaster = userMaster.getDistrictId();
		}
		districtspecificquestionsmethod(map,request,roleId,districtMaster,questionTypeMasterDAO,districtSpecificQuestionsDAO,roleAccessPermissionDAO);
		/*String roleAccess=null;
		try{
			roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,23,"assessmentsectionquestions.do",0);
		}catch(Exception e){
			e.printStackTrace();
		}
		System.out.println("roleAccess="+roleAccess);
		map.addAttribute("roleAccess", roleAccess);
		
		String questionId = request.getParameter("questionId");
		DistrictSpecificQuestions districtSpecificQuestion = null;
		try 
		{
			List<QuestionTypeMaster> questionTypeMasters = questionTypeMasterDAO.getActiveQuestionTypesByShortName(new String[]{"slsel","tf","ml","et","mlsel"});
			JSONObject json = new JSONObject();
			for (QuestionTypeMaster questionTypeMaster : questionTypeMasters) {
				json.put(""+questionTypeMaster.getQuestionTypeId()+"", questionTypeMaster.getQuestionTypeShortName());
			}

			map.addAttribute("questionTypeMasters", questionTypeMasters);
			map.addAttribute("json", json);
			
			if(questionId!=null && questionId.trim().length()>0)
			{
				districtSpecificQuestion = districtSpecificQuestionsDAO.findById(Integer.parseInt(questionId), false, false);
				List<OptionsForDistrictSpecificQuestions> optionsForDistrictSpecificQuestions = districtSpecificQuestion.getQuestionOptions();
				JSONArray assessmentQuestionsArray = new JSONArray();
				JSONObject jsonQuestion = new JSONObject();
				for (OptionsForDistrictSpecificQuestions optionsForDistrictSpecificQuestion : optionsForDistrictSpecificQuestions) {
					jsonQuestion.put("optionId", optionsForDistrictSpecificQuestion.getOptionId());
					jsonQuestion.put("questionOption", optionsForDistrictSpecificQuestion.getQuestionOption());
					jsonQuestion.put("validOption", optionsForDistrictSpecificQuestion.getValidOption());
					assessmentQuestionsArray.add(jsonQuestion);
				}
				map.addAttribute("assessQues", assessmentQuestionsArray);
				map.addAttribute("districtMaster", districtMaster);
				if( request.getParameter("quesSetId")!=null){
					map.addAttribute("questSetId",Integer.parseInt(request.getParameter("quesSetId")));
				}else{
					map.addAttribute("questSetId",0);
				}
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}*/

		return "districtspecificquestions";
	}
	
	
	@RequestMapping(value="/districtassessment.do", method=RequestMethod.GET)
	public String getDistrictAssessments(ModelMap map,HttpServletRequest request)
	{
		System.out.println("controller assessment.do AssessmentController getAssessments()");

		UserMaster userMaster=null;
		HttpSession session = request.getSession(false);
		int roleId=0;
		if (session == null || session.getAttribute("userMaster") == null) 
		{
			return "redirect:index.jsp";
		}else{
			userMaster=	(UserMaster) session.getAttribute("userMaster");
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
		}
		String roleAccess=null;
		try{
			roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,20,"assessment.do",0);
		}catch(Exception e){
			e.printStackTrace();
		}
		System.out.println("roleAccess="+roleAccess);
		map.addAttribute("roleAccess", roleAccess);

		return "districtassessment";
	}
	
	
	@RequestMapping(value="/districtassessmentsections.do", method=RequestMethod.GET)
	public String getDistrictAssessmentSections(ModelMap map,HttpServletRequest request)
	{
		//System.out.println("ASSSSSSSSSSAAAAAAAAAAAAAAAAAASSSSSSSSSSSSSSSSSSSSSSSSSSSSSS");
		UserMaster userMaster=null;
		HttpSession session = request.getSession(false);
		int roleId=0;
		if (session == null || session.getAttribute("userMaster") == null) 
		{
			return "redirect:index.jsp";
		}else{
			userMaster=	(UserMaster) session.getAttribute("userMaster");
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
		}
		
		String assessmentId = request.getParameter("assessmentId");
		
		DistrictAssessmentDetail districtAssessmentDetail = null;
		try 
		{
			if(assessmentId!=null && assessmentId.trim().length()>0)
			{
				districtAssessmentDetail=districtAssessmentDetailDAO.findById(Integer.parseInt(assessmentId), false, false);
				//districtAssessmentDetail = assessmentDetailDAO.findById(Integer.parseInt(assessmentId), false, false);
			}
			map.addAttribute("districtAssessmentDetail", districtAssessmentDetail);	
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return "districtassessmentsections";
	}
	
	
	@RequestMapping(value="/districtassessmentsectionquestions.do", method=RequestMethod.GET)
	public String getDistyrAssessmentSectionQuestions(ModelMap map,HttpServletRequest request)
	{
		System.out.println("controller assessmentSections.do AssessmentController getAssessmentSectionQuestions()");
		HttpSession session = request.getSession(false);
		UserMaster userMaster=null;
		int roleId=0;
		if (session == null || session.getAttribute("userMaster") == null) 
		{
			return "redirect:index.jsp";
		}else{
			userMaster=	(UserMaster) session.getAttribute("userMaster");
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
		}
		String roleAccess=null;
		/*try{
			roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,23,"assessmentsectionquestions.do",0);
		}catch(Exception e){
			e.printStackTrace();
		}*/
		
		map.addAttribute("roleAccess", roleAccess);
		String sectionId = request.getParameter("sectionId");
		String assessmentQuestionId = request.getParameter("assessmentQuestionId");
		DistrictAssessmentSection assessmentSection = null;
		DistrictAssessmentQuestions assessmentQuestion = null;
		try 
		{
			if(sectionId!=null && sectionId.trim().length()>0)
				assessmentSection = districtAssessmentSectionDAO.findById(Integer.parseInt(sectionId), false, false);
			
			List<QuestionTypeMaster> questionTypeMasters = questionTypeMasterDAO.getActiveQuestionTypes();
			JSONObject json = new JSONObject();
			for (QuestionTypeMaster questionTypeMaster : questionTypeMasters) {
				json.put(""+questionTypeMaster.getQuestionTypeId()+"", questionTypeMaster.getQuestionTypeShortName());
			}
			map.addAttribute("assessmentSection", assessmentSection);	
			map.addAttribute("domainMasters", domainMasterDAO.getActiveDomains());
			map.addAttribute("questionTypeMasters", questionTypeMasters);
			map.addAttribute("json", json);
			/*map.addAttribute("stageOneStatus", stageOneStatusDAO.getActiveStageOne());
			map.addAttribute("stageTwoStatus", stageTwoStatusDAO.getActiveStageTwo());
			map.addAttribute("stageThreeStatus", stageThreeStatusDAO.getActiveStageThree());*/

			if(assessmentQuestionId!=null && assessmentQuestionId.trim().length()>0)
			{
				assessmentQuestion = districtAssessmentQuestionsDAO.findById(Integer.parseInt(assessmentQuestionId), false, false);
				
				map.addAttribute("districtAssessmentQuestions", assessmentQuestion);
				List<DistrictAssessmentQuestionOptions> questionOptions = assessmentQuestion.getDistrictAssessmentQuestionOptions();
				
				JSONArray assessmentQuestionsArray = new JSONArray();
				JSONObject jsonQuestion = new JSONObject();
				for (DistrictAssessmentQuestionOptions questionOption : questionOptions) {
					jsonQuestion.put("optionId", questionOption.getOptionId());
					jsonQuestion.put("questionOption", questionOption.getQuestionOption());
					jsonQuestion.put("score", questionOption.getScore());
					jsonQuestion.put("rank", questionOption.getRank());
					assessmentQuestionsArray.add(jsonQuestion);
				}
				
				map.addAttribute("assessQues", assessmentQuestionsArray);
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return "districtassessmentsectionquestions";
	}
	
	
	@Transactional
	@RequestMapping(value="/districtassessmentquestions.do", method=RequestMethod.GET)
	public String getDistrictAssessmentQuestions(ModelMap map,HttpServletRequest request)
	{
		System.out.println("controller assessmentSections.do AssessmentController getAssessmentQuestions()");
		HttpSession session = request.getSession(false);
		UserMaster userMaster=null;
		int roleId=0;
		if (session == null || session.getAttribute("userMaster") == null) 
		{
			return "redirect:index.jsp";
		}else{
			userMaster=	(UserMaster) session.getAttribute("userMaster");
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
		}
		String roleAccess=null;
		try{
			roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,22,"assessmentquestions.do",0);
		}catch(Exception e){
			e.printStackTrace();
		}
		System.out.println("roleAccess="+roleAccess);
		map.addAttribute("roleAccess", roleAccess);
		String assessmentId = request.getParameter("assessmentId");
		String sectionId = request.getParameter("sectionId");
		List<DistrictAssessmentSection> assessmentSections = null;
		DistrictAssessmentSection assessmentSection = null;
		try 
		{
			if(sectionId!=null && sectionId.trim().length()>0)
			{
				assessmentSection = districtAssessmentSectionDAO.findById(Integer.parseInt(sectionId), false, false);
			}
			map.addAttribute("assessmentSection", assessmentSection);	
			map.addAttribute("domainMasters", domainMasterDAO.getActiveDomains());
			
			assessmentSections = districtAssessmentSectionDAO.getSectionsByDistrictAssessmentId(assessmentSection.getDistrictAssessmentDetail());
			//System.out.println("JJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJkkk");
			
		map.addAttribute("assessmentSections", assessmentSections);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return "districtassessmentquestions";
	}
	
	@RequestMapping(value="/districtspecificquestionsset.do", method=RequestMethod.GET)
	public String I4QuestionsSetGET(ModelMap map,HttpServletRequest request)
	{
		System.out.println(":::::::::::::::::: districtspecificquestionsSet :::::::::::::::::::::");
		HttpSession session = request.getSession(false);
		UserMaster userSession	=null;
		if (session == null || session.getAttribute("userMaster") == null) 
		{
			return "redirect:index.jsp";
		}
		session.setAttribute("displayType","0");
		districtspecificquestionssetMethod(map,request);
		/*if (session == null || session.getAttribute("userMaster") == null) {
			return "false";
		}else{
			userSession		=	(UserMaster) session.getAttribute("userMaster");
		}
		
		String districtName = "";
		String districtId = "";
		String headQuarterId = "";
		String branchId="";
		
		if(userSession.getEntityType()==2)
		{
			districtId = userSession.getDistrictId().getDistrictId().toString();
			districtName = userSession.getDistrictId().getDistrictName();
		}

		if(userSession.getEntityType()==5)
			headQuarterId = userSession.getHeadQuarterMaster().getHeadQuarterId().toString();
			
		if(userSession.getEntityType()==6)
			branchId = userSession.getBranchMaster().getBranchId().toString();
		
		System.out.println(" userSession.getEntityType() :: "+userSession.getEntityType());
		
		map.addAttribute("entityType",userSession.getEntityType());
		map.addAttribute("districtId",districtId);
		map.addAttribute("districtName",districtName);
		map.addAttribute("headQuarterId",headQuarterId);
		map.addAttribute("branchId",branchId);
		*/
		return "districtspecificquestionsset";
	}
	
	@RequestMapping(value="/assessmentQuestionSet.do", method=RequestMethod.GET)
    public String AssessmentQuestionSetGET(ModelMap map,HttpServletRequest request){
        System.out.println("::::::::::::::: assessmentQuestionSet.do :::::::::::::::::");
        HttpSession session = request.getSession(false);
        UserMaster userSession    =null;
        if (session == null || session.getAttribute("userMaster") == null) 
        {
            return "redirect:index.jsp";
        }
        if (session == null || session.getAttribute("userMaster") == null) {
            return "false";
        }else{
            userSession        =    (UserMaster) session.getAttribute("userMaster");
        }
        
        try
        {
        	 	Integer headQuaterId=0;
        	 	Integer districtID = 0;
        	 	int quesSetId=0;
        	 	boolean addQuesFlag = true;
          
         	QqQuestionSets qqQuestionSets = new QqQuestionSets();
            DistrictMaster districtMaster = null;
            HeadQuarterMaster headQuarterMaster = null;
            
    
            if(request.getParameter("quesSetId")!=null){
                quesSetId=Integer.parseInt(request.getParameter("quesSetId"));
                qqQuestionSets = qqQuestionSetsDAO.findById(quesSetId, false, false);
            }
         
            if(request.getParameter("districtId")!=null)
            {
                districtID = Integer.parseInt(request.getParameter("districtId"));
                districtMaster = districtMasterDAO.findById(districtID, false, false);
            }
            else if(request.getParameter("headQuaterId")!=null){
            	System.out.println(" headQuaterId :: "+headQuaterId);
            	headQuaterId=Integer.parseInt(request.getParameter("headQuarterId"));
            	headQuarterMaster = headQuarterMasterDAO.findById(headQuaterId, false, false);
            }
            
            if(userSession.getHeadQuarterMaster()!=null && userSession.getHeadQuarterMaster().getHeadQuarterId()==2){
    			if(userSession.getDistrictId()!=null)
    				addQuesFlag = false;
    		}
            
            if(userSession!=null && userSession.getRoleId()!=null){
            	if(userSession.getRoleId().getRoleId()==11 || userSession.getRoleId().getRoleId()==13)
            		addQuesFlag = false;
            }
            
            
            map.addAttribute("addQuesFlag", addQuesFlag);
            map.addAttribute("quesSetId", quesSetId);
            if(districtMaster!=null)
            	map.addAttribute("distID", districtMaster.getDistrictId());
            else if(headQuarterMaster!=null)
            	map.addAttribute("headQuarterId", headQuarterMaster.getHeadQuarterId());
            	map.addAttribute("quesSetName", qqQuestionSets.getQuestionSetText());
            
        }catch(Exception e)
        {
            e.printStackTrace();
        }
        // DistrictSpecificQuestions   DistrictSpecificRefChkQuestions
        DistrictSpecificQuestions districtSpecificQuestions = null;
        try 
        {
            List<QuestionTypeMaster> questionTypeMasters = questionTypeMasterDAO.getActiveQuestionTypesByShortName(new String[]{"slsel","tf","ml","et","mlsel"});
            JSONObject json = new JSONObject();
            for (QuestionTypeMaster questionTypeMaster : questionTypeMasters) {
                json.put(""+questionTypeMaster.getQuestionTypeId()+"", questionTypeMaster.getQuestionTypeShortName());
            }
 
            map.addAttribute("questionTypeMasters", questionTypeMasters);
            map.addAttribute("json", json);
            
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        
        return "assessmentquestionset";
        
    }
	
	/**
	 * @author Amit Chaudhary
	 * @param map
	 * @param request
	 * @return inventoryinvitations
	 */
	@RequestMapping(value="/inventoryinvitations.do", method=RequestMethod.GET)
	public String inventoryInvitationsGET(ModelMap map,HttpServletRequest request)
	{
		System.out.println("::::::::: inventoryinvitations.do :::::::::");

		UserMaster userMaster=null;
		HttpSession session = request.getSession(false);
		int roleId=0;
		if (session == null || session.getAttribute("userMaster") == null) 
		{
			return "redirect:index.jsp";
		}else{
			userMaster=	(UserMaster) session.getAttribute("userMaster");
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
		}
		String roleAccess=null;
		try{
			//roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,145,"inventoryinvitations.do",6);
		}catch(Exception e){
			e.printStackTrace();
		}
		map.addAttribute("roleAccess", roleAccess);

		return "inventoryinvitations";
	}

	/**
	 * @author Amit Chaudhary
	 * @param map
	 * @param request
	 * @return invitationstemplist
	 */
	@RequestMapping(value="/invitationstemplist.do", method=RequestMethod.GET)
	public String invitationsTempListGET(ModelMap map,HttpServletRequest request)
	{
		System.out.println("::::::::: invitationstemplist.do :::::::::");
		try{
			HttpSession session = request.getSession(false);
			int roleId=0;
			UserMaster userMaster =null;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			try{
				//roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,145,"inventoryinvitations.do",6);
			}catch(Exception e){
				e.printStackTrace();
			}
			
			map.addAttribute("roleAccess", roleAccess);
			map.addAttribute("sessionIdTxt", session.getId());
		}catch (Exception e){
			e.printStackTrace();
		}
		return "invitationstemplist";
	}
	//******************* Adding By Deepak *************************************
	public synchronized static void districtquestionsmethod(ModelMap map,HttpServletRequest request,UserMaster userMaster,Integer districtId)
	{

	try{
		String distId = request.getParameter("districtId");
		if(distId!=null && userMaster.getEntityType()==1)
		 districtId = Integer.parseInt(distId); 
	}catch(Exception e){
		e.printStackTrace();
	}
	//System.out.println("roleAccess="+roleAccess);
	map.addAttribute("userMaster", userMaster);
	map.addAttribute("districtId", districtId);
	map.addAttribute("dt",request.getParameter("dt"));
	}

	public synchronized static void districtspecificquestionssetMethod(ModelMap map,HttpServletRequest request)
	{
		HttpSession session = request.getSession(false);
		UserMaster userSession	=null;
		userSession		=	(UserMaster) session.getAttribute("userMaster");
		String districtName = "";
		String districtId = "";
		String headQuarterId = "";
		String branchId="";
		boolean addQuesFlag = true;
		
		
		if(userSession.getEntityType()==2)
		{
			districtId = userSession.getDistrictId().getDistrictId().toString();
			districtName = userSession.getDistrictId().getDistrictName();
		}

		if(userSession.getEntityType()==5){
			headQuarterId = userSession.getHeadQuarterMaster().getHeadQuarterId().toString();
		}
		
		if(userSession.getHeadQuarterMaster()!=null && userSession.getHeadQuarterMaster().getHeadQuarterId()==2){
			if(userSession.getDistrictId()!=null)
				addQuesFlag = false;
		}
		
		if(userSession.getEntityType()==6){
			headQuarterId = userSession.getHeadQuarterMaster().getHeadQuarterId().toString();
			branchId = userSession.getBranchMaster().getBranchId().toString();
			addQuesFlag = false;
		}
		
		System.out.println(" userSession.getEntityType() :: "+userSession.getEntityType());
		
		map.addAttribute("entityType",userSession.getEntityType());
		map.addAttribute("districtId",districtId);
		map.addAttribute("districtName",districtName);
		map.addAttribute("headQuarterId",headQuarterId);
		map.addAttribute("branchId",branchId);
		map.addAttribute("addQuesFlag",addQuesFlag);
	}

	public synchronized static void districtspecificquestionsmethod(ModelMap map,HttpServletRequest request,int roleId,DistrictMaster districtMaster,QuestionTypeMasterDAO questionTypeMasterDAO,DistrictSpecificQuestionsDAO districtSpecificQuestionsDAO,RoleAccessPermissionDAO roleAccessPermissionDAO)
	{

		String roleAccess=null;
		try{
			roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,23,"assessmentsectionquestions.do",0);
		}catch(Exception e){
			e.printStackTrace();
		}
		System.out.println("roleAccess="+roleAccess);
		map.addAttribute("roleAccess", roleAccess);
		
		String questionId = request.getParameter("questionId");
		DistrictSpecificQuestions districtSpecificQuestion = null;
		try 
		{
			List<QuestionTypeMaster> questionTypeMasters = questionTypeMasterDAO.getActiveQuestionTypesByShortName(new String[]{"slsel","tf","ml","et","mlsel"});
			JSONObject json = new JSONObject();
			for (QuestionTypeMaster questionTypeMaster : questionTypeMasters) {
				json.put(""+questionTypeMaster.getQuestionTypeId()+"", questionTypeMaster.getQuestionTypeShortName());
			}

			map.addAttribute("questionTypeMasters", questionTypeMasters);
			map.addAttribute("json", json);
			
			if(questionId!=null && questionId.trim().length()>0)
			{
				districtSpecificQuestion = districtSpecificQuestionsDAO.findById(Integer.parseInt(questionId), false, false);
				List<OptionsForDistrictSpecificQuestions> optionsForDistrictSpecificQuestions = districtSpecificQuestion.getQuestionOptions();
				JSONArray assessmentQuestionsArray = new JSONArray();
				JSONObject jsonQuestion = new JSONObject();
				for (OptionsForDistrictSpecificQuestions optionsForDistrictSpecificQuestion : optionsForDistrictSpecificQuestions) {
					jsonQuestion.put("optionId", optionsForDistrictSpecificQuestion.getOptionId());
					jsonQuestion.put("questionOption", optionsForDistrictSpecificQuestion.getQuestionOption());
					jsonQuestion.put("validOption", optionsForDistrictSpecificQuestion.getValidOption());
					jsonQuestion.put("explanationRequired", optionsForDistrictSpecificQuestion.getRequiredExplanation());
					assessmentQuestionsArray.add(jsonQuestion);
				}
				map.addAttribute("assessQues", assessmentQuestionsArray);
				map.addAttribute("districtMaster", districtMaster);
				if( request.getParameter("quesSetId")!=null){
					map.addAttribute("questSetId",Integer.parseInt(request.getParameter("quesSetId")));
				}else{
					map.addAttribute("questSetId",0);
				}
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
	}
	
	@RequestMapping(value="/assessmentmessage.do", method=RequestMethod.GET)
	public String getAssessmentMessage(ModelMap map,HttpServletRequest request)
	{
		System.out.println("::::::::::::::::::::::::::: assessmentmessage.do :::::::::::::::::::::::::::");
		UserMaster userMaster = null;
		HttpSession session = request.getSession(false);
		int roleId = 0;
		if(session == null || session.getAttribute("userMaster") == null) 
		{
			return "redirect:index.jsp";
		}
		else
		{
			userMaster = (UserMaster)session.getAttribute("userMaster");
			if(userMaster.getRoleId()!=null && userMaster.getRoleId().getRoleId()!=null)
			{
				roleId = userMaster.getRoleId().getRoleId();
			}
		}
		String roleAccess=null;
		try{
			roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,20,"assessment.do",0);
		}catch(Exception e){
			e.printStackTrace();
		}
		System.out.println("roleAccess="+roleAccess);
		map.addAttribute("roleAccess", roleAccess);

		return "assessmentmessage";
	}

}