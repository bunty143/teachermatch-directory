package tm.services.report;

import java.io.File;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NavigableSet;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileUtils;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.tempuri.memberqualification.MemberQualificationClient;
import org.tempuri.memberqualification.MemberQualification.AdditionalQualifications;
import org.tempuri.memberqualification.MemberQualification.BasicQualification;
import org.tempuri.memberqualification.MemberQualification.Member;

import tm.api.controller.JeffcoAPIController;
import tm.bean.DistrictPortfolioConfig;
import tm.bean.DistrictSpecificTeacherTfaOptions;
import tm.bean.EmployeeMaster;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.OctDetails;
import tm.bean.ReferenceNote;
import tm.bean.SchoolInJobOrder;
import tm.bean.SchoolPreferencebyCandidate;
import tm.bean.TeacherAcademics;
import tm.bean.TeacherAchievementScore;
import tm.bean.TeacherAdditionalDocuments;
import tm.bean.TeacherAssessmentStatus;
import tm.bean.TeacherCertificate;
import tm.bean.TeacherDetail;
import tm.bean.TeacherElectronicReferences;
import tm.bean.TeacherExperience;
import tm.bean.TeacherGeneralKnowledgeExam;
import tm.bean.TeacherHonor;
import tm.bean.TeacherInvolvement;
import tm.bean.TeacherNormScore;
import tm.bean.TeacherPersonalInfo;
import tm.bean.TeacherProfileVisitHistory;
import tm.bean.TeacherRole;
import tm.bean.TeacherSubjectAreaExam;
import tm.bean.TeacherVideoLink;
import tm.bean.assessment.AssessmentDomainScore;
import tm.bean.assessment.AssessmentJobRelation;
import tm.bean.cgreport.JobWiseConsolidatedTeacherScore;
import tm.bean.cgreport.RawDataForDomain;
import tm.bean.districtassessment.ExaminationCodeDetails;
import tm.bean.districtassessment.TeacherDistrictAssessmentStatus;
import tm.bean.master.CertificateTypeMaster;
import tm.bean.master.ContactTypeMaster;
import tm.bean.master.DemoClassSchedule;
import tm.bean.master.DistrictKeyContact;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictSpecificPortfolioAnswers;
import tm.bean.master.DistrictSpecificPortfolioOptions;
import tm.bean.master.DistrictSpecificPortfolioQuestions;
import tm.bean.master.DistrictSpecificQuestions;
import tm.bean.master.EthinicityMaster;
import tm.bean.master.EthnicOriginMaster;
import tm.bean.master.RaceMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.StateMaster;
import tm.bean.master.TFAAffiliateMaster;
import tm.bean.master.TeacherAnswerDetailsForDistrictSpecificQuestions;
import tm.bean.master.UniversityMaster;
import tm.bean.mq.MQEvent;
import tm.bean.teacher.TeacherLanguages;
import tm.bean.textfile.DegreeTypeMaster;
import tm.bean.textfile.License;
import tm.bean.textfile.LicenseArea;
import tm.bean.textfile.LicenseAreaDomain;
import tm.bean.textfile.LicenseClassLevelDomain;
import tm.bean.textfile.LicenseStatusDomain;
import tm.bean.textfile.LicensureEducation;
import tm.bean.textfile.LicensureNBPTS;
import tm.bean.user.UserMaster;
import tm.dao.DistrictPortfolioConfigDAO;
import tm.dao.DistrictSpecificTeacherTfaOptionsDAO;
import tm.dao.EmployeeMasterDAO;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.OctDetailsDAO;
import tm.dao.ReferenceNoteDAO;
import tm.dao.SchoolInJobOrderDAO;
import tm.dao.SchoolPreferenceByCandidateDAO;
import tm.dao.TeacherAcademicsDAO;
import tm.dao.TeacherAchievementScoreDAO;
import tm.dao.TeacherAdditionalDocumentsDAO;
import tm.dao.TeacherAssessmentStatusDAO;
import tm.dao.TeacherCertificateDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.TeacherElectronicReferencesDAO;
import tm.dao.TeacherExperienceDAO;
import tm.dao.TeacherGeneralKnowledgeExamDAO;
import tm.dao.TeacherHonorDAO;
import tm.dao.TeacherInvolvementDAO;
import tm.dao.TeacherNormScoreDAO;
import tm.dao.TeacherPersonalInfoDAO;
import tm.dao.TeacherProfileVisitHistoryDAO;
import tm.dao.TeacherRoleDAO;
import tm.dao.TeacherSubjectAreaExamDAO;
import tm.dao.TeacherVideoLinksDAO;
import tm.dao.assessment.AssessmentDomainScoreDAO;
import tm.dao.assessment.AssessmentJobRelationDAO;
import tm.dao.cgreport.JobWiseConsolidatedTeacherScoreDAO;
import tm.dao.cgreport.RawDataForDomainDAO;
import tm.dao.districtassessment.ExaminationCodeDetailsDAO;
import tm.dao.districtassessment.TeacherDistrictAssessmentStatusDAO;
import tm.dao.master.ContactTypeMasterDAO;
import tm.dao.master.DemoClassScheduleDAO;
import tm.dao.master.DistrictKeyContactDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.DistrictSpecificPortfolioAnswersDAO;
import tm.dao.master.DistrictSpecificPortfolioQuestionsDAO;
import tm.dao.master.DistrictSpecificQuestionsDAO;
import tm.dao.master.EthinicityMasterDAO;
import tm.dao.master.EthnicOriginMasterDAO;
import tm.dao.master.RaceMasterDAO;
import tm.dao.master.StateMasterDAO;
import tm.dao.master.StatusMasterDAO;
import tm.dao.master.TFAAffiliateMasterDAO;
import tm.dao.master.TeacherAnswerDetailsForDistrictSpecificQuestionsDAO;
import tm.dao.master.UniversityMasterDAO;
import tm.dao.menu.RoleAccessPermissionDAO;
import tm.dao.mq.MQEventDAO;
import tm.dao.teacher.TeacherLanguagesDAO;
import tm.dao.textfile.DegreeTypeMasterDAO;
import tm.dao.textfile.LicenseAreaDAO;
import tm.dao.textfile.LicenseAreaDomainDAO;
import tm.dao.textfile.LicenseClassLevelDomailDAO;
import tm.dao.textfile.LicenseDAO;
import tm.dao.textfile.LicensureEducationDAO;
import tm.dao.textfile.LicensureNBPTSDao;
import tm.services.CommonDashboardAjax;
import tm.services.PaginationAndSorting;
import tm.services.district.PrintOnConsole;
import tm.services.teacher.PFLicenseEducation;
import tm.utility.IPAddressUtility;
import tm.utility.TestTool;
import tm.utility.Utility;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
public class TeacherProfileViewInDivAjax 
{	
	//sandeep	
	 public static int NC_HEADQUARTER=2;
	
	
	 String locale = Utility.getValueOfPropByKey("locale");
	
	 String lblAssessmentName=Utility.getLocaleValuePropByKey("lblAssessmentName", locale);
	 String lblDistrictName=Utility.getLocaleValuePropByKey("lblDistrictName", locale);
	 String lblAssessmentCode=Utility.getLocaleValuePropByKey("lblAssessmentCode", locale);
	 String lblAssDTime=Utility.getLocaleValuePropByKey("lblAssDTime", locale);
	 String lblAct=Utility.getLocaleValuePropByKey("lblAct", locale);
	 String lnkViLnk=Utility.getLocaleValuePropByKey("lnkViLnk", locale);
	 String lnkAddDocu=Utility.getLocaleValuePropByKey("lnkAddDocu", locale);
	 String lblVid=Utility.getLocaleValuePropByKey("lblVid", locale);
	 String lblPhoneNo=Utility.getLocaleValuePropByKey("lblPhoneNo", locale);
	 String lblMobileNumber=Utility.getLocaleValuePropByKey("lblMobileNumber", locale);
	 String lblNoRecord=Utility.getLocaleValuePropByKey("lblNoRecord", locale);
	 String lblStartAssessment1=Utility.getLocaleValuePropByKey("lblStartAssessment1", locale);
	 String lblSelectSlot1=Utility.getLocaleValuePropByKey("lblSelectSlot1", locale);
	 String lblUserName=Utility.getLocaleValuePropByKey("lblUserName", locale);
	 String lblVisitedDate1=Utility.getLocaleValuePropByKey("lblVisitedDate1", locale);
	 String lblGPA=Utility.getLocaleValuePropByKey("lblGPA", locale);
	 String lblEmpNum=Utility.getLocaleValuePropByKey("lblEmpNum", locale);
	 String lblEmpPosition=Utility.getLocaleValuePropByKey("lblEmpPosition", locale);
	 String lblFirstJobApplied=Utility.getLocaleValuePropByKey("lblFirstJobApplied", locale);
	 String lblLastJobApplied=Utility.getLocaleValuePropByKey("lblLastJobApplied", locale);
	 String lblCorpsYear=Utility.getLocaleValuePropByKey("lblCorpsYear", locale);
	 String lblTFARegion=Utility.getLocaleValuePropByKey("lblTFARegion", locale);
	 String lblTeachingYears=Utility.getLocaleValuePropByKey("lblTeachingYears", locale);
	 String lblExpectedSalary=Utility.getLocaleValuePropByKey("lblExpectedSalary", locale);
	 String lblNotAvailable=Utility.getLocaleValuePropByKey("lblNotAvailable", locale);
	// String lblPhoneNo=Utility.getLocaleValuePropByKey("lblPhoneNo", locale);
	 String msgOCTRegistrationNum=Utility.getLocaleValuePropByKey("msgOCTRegistrationNum", locale);
	 String lblLoding=Utility.getLocaleValuePropByKey("lblLoding", locale);
	 String headCom=Utility.getLocaleValuePropByKey("headCom", locale);
	 String btnSave=Utility.getLocaleValuePropByKey("btnSave", locale);
	 String btnShare=Utility.getLocaleValuePropByKey("btnShare", locale);
	 String headTag=Utility.getLocaleValuePropByKey("headTag", locale);
	 String lblLastContacted=Utility.getLocaleValuePropByKey("lblLastContacted", locale);
	 String lblGeneralKnowledgeExam=Utility.getLocaleValuePropByKey("lblGeneralKnowledgeExam", locale);
	 
	 String lblOfViews=Utility.getLocaleValuePropByKey("lblOfViews", locale);
	 String lblJobApplied=Utility.getLocaleValuePropByKey("lblJobApplied", locale);
	 String lblClickToViewTPVisit=Utility.getLocaleValuePropByKey("lblClickToViewTPVisit", locale);
	 String lblClickToViewJO=Utility.getLocaleValuePropByKey("lblClickToViewJO", locale);
	 String lblWillingToSubsti=Utility.getLocaleValuePropByKey("lblWillingToSubsti", locale);
	 String headPdRep=Utility.getLocaleValuePropByKey("headPdRep", locale);
	 String lblComments1=Utility.getLocaleValuePropByKey("lblComments1", locale);
	 String lblLRScore=Utility.getLocaleValuePropByKey("lblLRScore", locale);
	 
	 String lblName1=Utility.getLocaleValuePropByKey("lblName1", locale);
	 String lblAname=Utility.getLocaleValuePropByKey("lblAname", locale);
	 String lblAdd=Utility.getLocaleValuePropByKey("lblAdd", locale);
	 String lblEPINorm=Utility.getLocaleValuePropByKey("lblEPINorm", locale);
	 String lblSSN=Utility.getLocaleValuePropByKey("lblSSN", locale);
	 String lblNo=Utility.getLocaleValuePropByKey("lblNo", locale);
	 String lblYes=Utility.getLocaleValuePropByKey("lblYes", locale);
	 String lblNatiBoardCertiLice =Utility.getLocaleValuePropByKey("lblNatiBoardCertiLice", locale);
	 String lnkResume=Utility.getLocaleValuePropByKey("lnkResume", locale);
	 String lblAScore=Utility.getLocaleValuePropByKey("lblAScore", locale);
	 String lblExamDate=Utility.getLocaleValuePropByKey("lblExamDate", locale);
	 String lblScoreReport=Utility.getLocaleValuePropByKey("lblScoreReport", locale);
	 String lblSubjectAreaExam=Utility.getLocaleValuePropByKey("lblSubjectAreaExam", locale);
	 String optPass=Utility.getLocaleValuePropByKey("optPass", locale);
	 String optFail=Utility.getLocaleValuePropByKey("optFail", locale);
	 String lblAcademics=Utility.getLocaleValuePropByKey("lblAcademics", locale);
	 String lblCertiLice=Utility.getLocaleValuePropByKey("lblCertiLice", locale);
	 String lblHonors=Utility.getLocaleValuePropByKey("lblHonors", locale);
	 String lblIVWork=Utility.getLocaleValuePropByKey("lblIVWork", locale);
	 String lblWorkExp=Utility.getLocaleValuePropByKey("lblWorkExp", locale);
	 String lblLeadershipResults=Utility.getLocaleValuePropByKey("lblLeadershipResults", locale);
	
	 
	 String msgMarksObtained=Utility.getLocaleValuePropByKey("msgMarksObtained", locale);
	 String lnkAddRef=Utility.getLocaleValuePropByKey("lnkAddRef", locale);
	 String lblFname=Utility.getLocaleValuePropByKey("lblFname", locale);
	 String lblLname=Utility.getLocaleValuePropByKey("lblLname", locale);
	 String lblTitle=Utility.getLocaleValuePropByKey("lblTitle", locale);
	 String lblOrga=Utility.getLocaleValuePropByKey("lblOrga", locale);
	 String lblContNum=Utility.getLocaleValuePropByKey("lblContNum", locale);
	 String lblEmail=Utility.getLocaleValuePropByKey("lblEmail", locale);
	 String lblRecommendLetter=Utility.getLocaleValuePropByKey("lblRecommendLetter", locale);
	 String lnkClear=Utility.getLocaleValuePropByKey("lnkClear", locale);
	 String lnkRemo=Utility.getLocaleValuePropByKey("lnkRemo", locale);
	 String qnhowlongknwperson=Utility.getLocaleValuePropByKey("qnhowlongknwperson", locale);
	 String lblReferenceDetails=Utility.getLocaleValuePropByKey("lblReferenceDetails", locale);
	 String lnkImD=Utility.getLocaleValuePropByKey("lnkImD", locale);
	 String lnkAddViLnk=Utility.getLocaleValuePropByKey("lnkAddViLnk", locale);
	 String lblPleaseInclude=Utility.getLocaleValuePropByKey("lblPleaseInclude", locale);
	 String lnkCancel=Utility.getLocaleValuePropByKey("lnkCancel", locale);
	 String lblSupportedvideos=Utility.getLocaleValuePropByKey("lblSupportedvideos", locale);
	// String lnkAddDocu=Utility.getLocaleValuePropByKey("lnkAddDocu", locale);
	 String lblLanguageProfiency=Utility.getLocaleValuePropByKey("lblLanguageProfiency", locale);
	 String lblAssessmentDetails=Utility.getLocaleValuePropByKey("lblAssessmentDetails", locale);
	 String LBLDistrictSpecificQuestion=Utility.getLocaleValuePropByKey("LBLDistrictSpecificQuestion", locale);
	 String lblSaveQuestion=Utility.getLocaleValuePropByKey("lblSaveQuestion", locale);
	 String LBLCandidateRefe=Utility.getLocaleValuePropByKey("LBLCandidateRefe", locale);
	 String msgExplaination=Utility.getLocaleValuePropByKey("msgExplaination", locale);
	 String lblDp_commons4=Utility.getLocaleValuePropByKey("lblDp_commons4", locale);
	 String lblPrsnDirectlyContByHiringAuth=Utility.getLocaleValuePropByKey("lblPrsnDirectlyContByHiringAuth", locale);
	 
	 String MSGserviceconnected=Utility.getLocaleValuePropByKey("MSGserviceconnected", locale);
	 String msgDp_commons7=Utility.getLocaleValuePropByKey("msgDp_commons7", locale);
	 String msgDpcommo15=Utility.getLocaleValuePropByKey("msgDpcommo15", locale);
	 String msgDp_commons9=Utility.getLocaleValuePropByKey("msgDp_commons9", locale);
	 String msgDp_commons11=Utility.getLocaleValuePropByKey("msgDp_commons11", locale);
	 String msgDp_commons12=Utility.getLocaleValuePropByKey("msgDp_commons12", locale);
	 String lblEth=Utility.getLocaleValuePropByKey("lblEth", locale);
	 String lblRac=Utility.getLocaleValuePropByKey("lblRac", locale);
	 String lblType=Utility.getLocaleValuePropByKey("lblType", locale);
	 String lblNumberofPeople=Utility.getLocaleValuePropByKey("lblNumberofPeople", locale);
	 String lblNumPeLed=Utility.getLocaleValuePropByKey("lblNumPeLed", locale);
	 String LBLMemberDetail1=Utility.getLocaleValuePropByKey("LBLMemberDetail1", locale);
	 String LBLRegistrationId1=Utility.getLocaleValuePropByKey("LBLRegistrationId1", locale);
	 String LBLSurname1=Utility.getLocaleValuePropByKey("LBLSurname1", locale);
	 String LBLProfessionalDesignationEnglish=Utility.getLocaleValuePropByKey("LBLProfessionalDesignationEnglish", locale);
	 String LBLProfessionalDesignationFrench=Utility.getLocaleValuePropByKey("LBLProfessionalDesignationFrench", locale);
	 String LBLDivision1=Utility.getLocaleValuePropByKey("LBLDivision1", locale);
	 String lblSubject=Utility.getLocaleValuePropByKey("lblSubject", locale);
	 String Institution=Utility.getLocaleValuePropByKey("Institution", locale);
	 String LBLEffectiveDate=Utility.getLocaleValuePropByKey("LBLEffectiveDate", locale);
	 String LBLSubjectCode=Utility.getLocaleValuePropByKey("LBLSubjectCode", locale);
	 String lblDivisionCode1=Utility.getLocaleValuePropByKey("lblDivisionCode1", locale);
	 String lblOption1=Utility.getLocaleValuePropByKey("lblOption1", locale);
	 String lblOptionCode1=Utility.getLocaleValuePropByKey("lblOptionCode1", locale);
	 
	 String msgDp_commons6=Utility.getLocaleValuePropByKey("msgDp_commons6", locale);
	 String msgDp_commons8=Utility.getLocaleValuePropByKey("msgDp_commons8", locale);
	 String msgDp_commons10=Utility.getLocaleValuePropByKey("msgDp_commons10", locale);
	 String lblNotes  = Utility.getLocaleValuePropByKey("lblNotes", locale);
	 String lblEeocSupport  = Utility.getLocaleValuePropByKey("lblEeocSupport", locale);
	 String lblEeocLink  = Utility.getLocaleValuePropByKey("lblEeocLink", locale);
	 String lblLicense=Utility.getLocaleValuePropByKey("lblLicense", locale);
	 String lblLEACandidatePortfolio=Utility.getLocaleValuePropByKey("lblLEACandidatePortfolio", locale);
	 String lblCertiLicense=Utility.getLocaleValuePropByKey("lblCertiLicense", locale);
	 String lblDates = Utility.getLocaleValuePropByKey("lblDates", locale);
	 String lblPosition = Utility.getLocaleValuePropByKey("lblPosition", locale);
	 String lblKSNID  = Utility.getLocaleValuePropByKey("lblKSNID", locale);
	 
	 @Autowired LicensureNBPTSDao licensureNBPTSDao;
	 
	@Autowired
	private AssessmentJobRelationDAO assessmentJobRelationDAO;
	
	@Autowired
	private TeacherGeneralKnowledgeExamDAO teacherGeneralKnowledgeExamDAO;
	
	@Autowired
	private TeacherSubjectAreaExamDAO teacherSubjectAreaExamDAO;
	
	@Autowired
	private StatusMasterDAO statusMasterDAO;
	
	@Autowired
	 private SchoolPreferenceByCandidateDAO schoolPreferenceByCandidateDAO;
	 public void setSchoolPreferenceByCandidateDAO(SchoolPreferenceByCandidateDAO schoolPreferenceByCandidateDAO) 
	 {
	  this.schoolPreferenceByCandidateDAO = schoolPreferenceByCandidateDAO;
	 }
	
	@Autowired
	private DistrictSpecificPortfolioQuestionsDAO districtSpecificPortfolioQuestionsDAO;
	
	@Autowired
	private DistrictSpecificPortfolioAnswersDAO districtSpecificPortfolioAnswersDAO;
	
	@Autowired
	private EthinicityMasterDAO ethinicityMasterDAO;
	
	@Autowired
	private EthnicOriginMasterDAO ethnicOriginMasterDAO;
	
	@Autowired
	private DistrictPortfolioConfigDAO districtPortfolioConfigDAO;
	public void setDistrictPortfolioConfigDAO(DistrictPortfolioConfigDAO districtPortfolioConfigDAO) {
		this.districtPortfolioConfigDAO = districtPortfolioConfigDAO;
	}
	
	
	@Autowired
	private DistrictSpecificTeacherTfaOptionsDAO districtSpecificTeacherTfaOptionsDAO;
	public void setDistrictSpecificTeacherTfaOptionsDAO(DistrictSpecificTeacherTfaOptionsDAO districtSpecificTeacherTfaOptionsDAO) {
		this.districtSpecificTeacherTfaOptionsDAO = districtSpecificTeacherTfaOptionsDAO;
	}
	
	@Autowired
	private TeacherInvolvementDAO teacherInvolvementDAO;
	public void setTeacherInvolvementDAO(TeacherInvolvementDAO teacherInvolvementDAO) {
		this.teacherInvolvementDAO = teacherInvolvementDAO;
	}
	
	@Autowired
	private TeacherHonorDAO teacherHonorDAO;
	public void setTeacherHonorDAO(TeacherHonorDAO teacherHonorDAO) {
		this.teacherHonorDAO = teacherHonorDAO;
	}
	
	@Autowired
	private RaceMasterDAO raceMasterDAO;
	public void setRaceMasterDAO(RaceMasterDAO raceMasterDAO) {
		this.raceMasterDAO = raceMasterDAO;
	}
	
	@Autowired
	private CGReportService cGReportService;
	public void setcGReportService(CGReportService cGReportService) {
		this.cGReportService = cGReportService;
	}
	
	@Autowired
	private TeacherNormScoreDAO teacherNormScoreDAO; 
	public void setTeacherNormScoreDAO(TeacherNormScoreDAO teacherNormScoreDAO) {
		this.teacherNormScoreDAO = teacherNormScoreDAO;
	}
	
	@Autowired
	private TeacherAchievementScoreDAO teacherAchievementScoreDAO;
	
	@Autowired
	private JobWiseConsolidatedTeacherScoreDAO jobWiseConsolidatedTeacherScoreDAO; 
	
	@Autowired
	private DemoClassScheduleDAO demoClassScheduleDAO;
	
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	public void setDistrictMasterDAO(DistrictMasterDAO districtMasterDAO) {
		this.districtMasterDAO = districtMasterDAO;
	}
	
	@Autowired
	private TeacherCertificateDAO teacherCertificateDAO;
	public void setTeacherCertificateDAO(TeacherCertificateDAO teacherCertificateDAO) 
	{
		this.teacherCertificateDAO = teacherCertificateDAO;
	}
	@Autowired
	private JobOrderDAO jobOrderDAO;
	public void setJobOrderDAO(JobOrderDAO jobOrderDAO) 
	{
		this.jobOrderDAO = jobOrderDAO;
	}

	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	public void setJobForTeacherDAO(JobForTeacherDAO jobForTeacherDAO) 
	{
		this.jobForTeacherDAO = jobForTeacherDAO;
	}

	@Autowired
	private TeacherAssessmentStatusDAO teacherAssessmentStatusDAO;
	public void setTeacherAssessmentStatusDAO(TeacherAssessmentStatusDAO teacherAssessmentStatusDAO) 
	{
		this.teacherAssessmentStatusDAO = teacherAssessmentStatusDAO;
	}

	@Autowired
	private SchoolInJobOrderDAO schoolInJobOrderDAO;
	public void setSchoolInJobOrderDAO(SchoolInJobOrderDAO schoolInJobOrderDAO) 
	{
		this.schoolInJobOrderDAO = schoolInJobOrderDAO;
	}

	@Autowired
	private TeacherExperienceDAO teacherExperienceDAO;
	public void setTeacherExperienceDAO(TeacherExperienceDAO teacherExperienceDAO) 
	{
		this.teacherExperienceDAO = teacherExperienceDAO;
	}	

	@Autowired
	private TeacherDetailDAO teacherDetailDAO;
	public void setTeacherDetailDAO(TeacherDetailDAO teacherDetailDAO) {
		this.teacherDetailDAO = teacherDetailDAO;
	}

	@Autowired
	private TeacherRoleDAO teacherRoleDAO;
	public void setTeacherRoleDAO(TeacherRoleDAO teacherRoleDAO) 
	{
		this.teacherRoleDAO = teacherRoleDAO;
	}
	
	@Autowired
	private RoleAccessPermissionDAO roleAccessPermissionDAO;
	public void setRoleAccessPermissionDAO(RoleAccessPermissionDAO roleAccessPermissionDAO) {
		this.roleAccessPermissionDAO = roleAccessPermissionDAO;
	}

	@Autowired
	private TeacherPersonalInfoDAO teacherPersonalInfoDAO;
	public void setTeacherPersonalInfoDAO(TeacherPersonalInfoDAO teacherPersonalInfoDAO) {
		this.teacherPersonalInfoDAO = teacherPersonalInfoDAO;
	}

	@Autowired
	private TeacherAcademicsDAO teacherAcademicsDAO;
	public void setTeacherAcademicsDAO(TeacherAcademicsDAO teacherAcademicsDAO) {
		this.teacherAcademicsDAO = teacherAcademicsDAO;
	}
	@Autowired
	private TeacherElectronicReferencesDAO teacherElectronicReferencesDAO;
	public void setTeacherElectronicReferencesDAO(
			TeacherElectronicReferencesDAO teacherElectronicReferencesDAO) {
		this.teacherElectronicReferencesDAO = teacherElectronicReferencesDAO;
	}
	
	@Autowired
	private TeacherVideoLinksDAO teacherVideoLinksDAO;
	public void setTeacherVideoLinksDAO(
			TeacherVideoLinksDAO teacherVideoLinksDAO) {
		this.teacherVideoLinksDAO = teacherVideoLinksDAO;
	}
	
	@Autowired
	private TeacherProfileVisitHistoryDAO teacherProfileVisitHistoryDAO;
	public void setTeacherProfileVisitHistoryDAO(
			TeacherProfileVisitHistoryDAO teacherProfileVisitHistoryDAO) {
		this.teacherProfileVisitHistoryDAO = teacherProfileVisitHistoryDAO;
	}
	
	@Autowired
	private ReferenceNoteDAO referenceNoteDAO;
	public void setReferenceNoteDAO(ReferenceNoteDAO referenceNoteDAO) {
		this.referenceNoteDAO = referenceNoteDAO;
	}
	@Autowired
	private DistrictSpecificQuestionsDAO districtSpecificQuestionsDAO;
	public void setDistrictSpecificQuestionsDAO(
			DistrictSpecificQuestionsDAO districtSpecificQuestionsDAO) {
		this.districtSpecificQuestionsDAO = districtSpecificQuestionsDAO;
	}
	@Autowired
	private TeacherAnswerDetailsForDistrictSpecificQuestionsDAO teacherAnswerDetailsForDistrictSpecificQuestionsDAO;
	public void setTeacherAchievementScoreDAO(
			TeacherAchievementScoreDAO teacherAchievementScoreDAO) {
		this.teacherAchievementScoreDAO = teacherAchievementScoreDAO;
	}
	@Autowired
	private TeacherAdditionalDocumentsDAO teacherAdditionalDocumentsDAO;
	
	@Autowired
	private ContactTypeMasterDAO contactTypeMasterDAO;
	
	@Autowired
	private DistrictKeyContactDAO districtKeyContactDAO;;
	
	@Autowired
	private TFAAffiliateMasterDAO tfaAffiliateMasterDao;
	
	@Autowired
	private CandidateGridSubAjax candidateGridSubAjax;
	
	@Autowired
	private TeacherDistrictAssessmentStatusDAO teacherDistrictAssessmentStatusDAO;

	@Autowired
	private ExaminationCodeDetailsDAO examinationCodeDetailsDAO;
	@Autowired
	private TeacherLanguagesDAO teacherLanguagesDAO;
	@Autowired
	private OctDetailsDAO octDetailsDAO;
	
	@Autowired
	private LicenseDAO licenseDAO;
	
	@Autowired
	private EmployeeMasterDAO employeeMasterDAO;
	
	@Autowired
	private StateMasterDAO stateMasterDAO;
	
	@Autowired
	private LicensureEducationDAO licensureEducationDAO;
	
	@Autowired
	private DegreeTypeMasterDAO degreeTypeMasterDAO;
	
	@Autowired
	private UniversityMasterDAO universityMasterDAO;
	
	@Autowired
	private AssessmentDomainScoreDAO assessmentDomainScoreDAO;
	
	@Autowired
	private RawDataForDomainDAO rawDataForDomainDAO;
	
	@Autowired
	private LicenseClassLevelDomailDAO licenseClassLevelDomailDAO;
	
	@Autowired
	private LicenseAreaDomainDAO licenseAreaDomainDAO;
	
	@Autowired
	private LicenseAreaDAO licenseAreaDAO;
	
	
	public void setUniversityMasterDAO(UniversityMasterDAO universityMasterDAO) {
		this.universityMasterDAO = universityMasterDAO;
	}

	public void setDegreeTypeMasterDAO(DegreeTypeMasterDAO degreeTypeMasterDAO) {
		this.degreeTypeMasterDAO = degreeTypeMasterDAO;
	}
	
	@Autowired
	private MQEventDAO mqEventDAO;
	
	
	public int jobListByTeacher(int teacherId,SortedMap<Long,JobForTeacher> jftMap)
	{
		int jobList=0;
		try{
			Iterator iterator = jftMap.keySet().iterator();
			JobForTeacher jobForTeacher=null;
			while (iterator.hasNext()) {
				Object key = iterator.next();
				jobForTeacher=jftMap.get(key);
				int teacherKey=Integer.parseInt(jobForTeacher.getTeacherId().getTeacherId()+"");
				if(teacherKey==teacherId){
					jobList++;
				}
			}
		}catch (Exception e) {
			// TODO: handle exception
		}
		return jobList;
	}
	
	/**
	 * 
	 * @param teacherId
	 * @return
	 * @author Ramesh Kumar Bhartiya
	 * Date Nov 18, 2013
	 */
	
	//@Transactional(readOnly=true)
	public String getPhoneDetailByDiv(String teacherId){
		StringBuffer sb = new StringBuffer("");
		try {
			TeacherPersonalInfo teacherPersonalInfo = teacherPersonalInfoDAO.findById(new Integer(teacherId), false, false);

			String phone = null;
			String mobile = null;
			try
			{
				phone = teacherPersonalInfo.getPhoneNumber()==null?"":teacherPersonalInfo.getPhoneNumber();
				mobile = teacherPersonalInfo.getMobileNumber()==null?"":teacherPersonalInfo.getMobileNumber();
			}
			catch(NullPointerException e)
			{
				phone="";
				mobile="";
			}
			if(teacherPersonalInfo !=null && (!(phone.trim().equals("") && mobile.trim().equals("")))){			
				sb.append("<table>");

				if(!phone.trim().equals("")){
					sb.append("<tr>");
					sb.append("<td>");
					sb.append(lblPhoneNo);
					sb.append("</td>");				
					sb.append("<td>");
					sb.append(""+phone);
					sb.append("</td>");
					sb.append("</tr>");
				}

				if(!mobile.trim().equals("")){
					sb.append("<tr>");
					sb.append("<td>");
					sb.append(lblMobileNumber+":");
					sb.append("</td>");				
					sb.append("<td>");
					sb.append(""+mobile);
					sb.append("</td>");
					sb.append("</tr>");
				}

				sb.append("</table>");	
			}

		} catch (Exception e) {

			e.printStackTrace();
		}

		sb.append("");
		return sb.toString();
	}
	
	/**
	 * 
	 * @param teacherId
	 * @param noOfRow
	 * @param pageNo
	 * @param sortOrder
	 * @param sortOrderType
	 * @return
	 * @author Ramesh Kumar Bhartiya
	 * Date Nov 14, 2013
	 */
	
	
	public String getTeacherAcademicsGrid(Integer teacherId, String noOfRow, String pageNo,String sortOrder,String sortOrderType,String visitLocation)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster=null;
		DistrictMaster districtMaster=null;
		boolean nobleflag=false;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }else{
		    userMaster=(UserMaster)session.getAttribute("userMaster");
		    districtMaster=userMaster.getDistrictId();
		   if(districtMaster!=null)
			   if(districtMaster.getDistrictId()==7800038)
				   nobleflag=true;
	    }
		
		
		if(districtMaster!=null && districtMaster.getDistrictId().equals(1201470) && (visitLocation.equalsIgnoreCase("Teacher Pool") || visitLocation.equalsIgnoreCase("CG View")))
			return getTeacherAcademicsGridOsecola(teacherId, noOfRow, pageNo,sortOrder,sortOrderType,visitLocation);
		else if(districtMaster!=null && districtMaster.getDistrictId().equals(7800040) && (visitLocation.equalsIgnoreCase("Teacher Pool") || visitLocation.equalsIgnoreCase("CG View")))
			return getTeacherAcademicsGridOsecola(teacherId, noOfRow, pageNo,sortOrder,sortOrderType,visitLocation);
		else
			return getTeacherAcademicsGridCommon(teacherId, noOfRow, pageNo,sortOrder,sortOrderType,visitLocation);
	}
	public String getGridDataWorkExpEmployment(Integer teacherId, String noOfRow, String pageNo,String sortOrder,String sortOrderType,String visitLocation)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster=null;
		DistrictMaster districtMaster=null;
		boolean nobleflag=false;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }else{
		    userMaster=(UserMaster)session.getAttribute("userMaster");
		    districtMaster=userMaster.getDistrictId();
		   if(districtMaster!=null)
			   if(districtMaster.getDistrictId()==7800038)
				   nobleflag=true;
	    }
		
		if(districtMaster!=null && districtMaster.getDistrictId().equals(1201470) && (visitLocation.equalsIgnoreCase("Teacher Pool") || visitLocation.equalsIgnoreCase("CG View")))
			return getGridDataWorkExpEmploymentForOscela(teacherId, noOfRow, pageNo,sortOrder,sortOrderType,visitLocation);
		else if(districtMaster!=null && (districtMaster.getDistrictId().equals(7800040) || districtMaster.getDistrictId().equals(3904493)) && (visitLocation.equalsIgnoreCase("Teacher Pool") || visitLocation.equalsIgnoreCase("CG View")))
			return getGridDataWorkExpEmploymentForOscela(teacherId, noOfRow, pageNo,sortOrder,sortOrderType,visitLocation);
		else
			return getGridDataWorkExpEmployment1(teacherId, noOfRow, pageNo,sortOrder,sortOrderType,visitLocation);
	}
	
	public String getTeacherCertificationsGrid(Integer teacherId, String noOfRow, String pageNo,String sortOrder,String sortOrderType,String visitLocation)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster=null;
		DistrictMaster districtMaster=null;
		boolean nobleflag=false;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }else{
		    userMaster=(UserMaster)session.getAttribute("userMaster");
		    districtMaster=userMaster.getDistrictId();
		   if(districtMaster!=null)
			   if(districtMaster.getDistrictId()==7800038)
				   nobleflag=true;
	    }
		
		if(districtMaster!=null && districtMaster.getDistrictId().equals(1201470) && (visitLocation.equalsIgnoreCase("Teacher Pool") || visitLocation.equalsIgnoreCase("CG View")))
			return getTeacherCertificationsGridOsceola(teacherId, noOfRow, pageNo,sortOrder,sortOrderType,visitLocation);
		else if(districtMaster!=null && districtMaster.getDistrictId().equals(7800040) && (visitLocation.equalsIgnoreCase("Teacher Pool") || visitLocation.equalsIgnoreCase("CG View")))
			return getTeacherCertificationsGridOsceola(teacherId, noOfRow, pageNo,sortOrder,sortOrderType,visitLocation);
		else
			return getTeacherCertificationsGridCommon(teacherId, noOfRow, pageNo,sortOrder,sortOrderType,visitLocation);
	}
	// Work Experience
	//@Transactional(readOnly=true)
	public String getGridDataWorkExpEmployment1(Integer teacherId, String noOfRow, String pageNo,String sortOrder,String sortOrderType,String visitLocation)
	{		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster=null;
		DistrictMaster districtMaster=null;
		boolean nobleflag=false;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }else{
		    userMaster=(UserMaster)session.getAttribute("userMaster");
		    districtMaster=userMaster.getDistrictId();
		   if(districtMaster!=null)
			   if(districtMaster.getDistrictId()==7800038)
				   nobleflag=true;
	    }
		
		

		StringBuffer sb = new StringBuffer();		
		try 
		{
			
			String locale = Utility.getValueOfPropByKey("locale");
			String lblNoRecord = Utility.getLocaleValuePropByKey("lblNoRecord", locale);
			String lblRole	 = Utility.getLocaleValuePropByKey("lblRole", locale);
			String lblPosition  = Utility.getLocaleValuePropByKey("lblPosition", locale);
			String lblOrgEmp  = Utility.getLocaleValuePropByKey("lblOrgEmp", locale);
			String lblDuration  = Utility.getLocaleValuePropByKey("lblDuration ", locale);
			String lblAnnualSalary  = Utility.getLocaleValuePropByKey("lblAnnualSalary", locale);
			String lblTypeOfRole  = Utility.getLocaleValuePropByKey("lblTypeOfRole", locale);
			String lblPrimaryResponsibility  = Utility.getLocaleValuePropByKey("lblPrimaryResponsibility", locale);
			String lblMostSignificantContributions  = Utility.getLocaleValuePropByKey("lblMostSignificantContributions ", locale);
			String lblReasonForLeaving  = Utility.getLocaleValuePropByKey("lblReasonForLeaving ", locale);
			String lblNoData  = Utility.getLocaleValuePropByKey("lblNoData", locale);
			String lblCity = Utility.getLocaleValuePropByKey("lblCity", locale);
			String lblState = Utility.getLocaleValuePropByKey("lblState", locale);
			String lblType = Utility.getLocaleValuePropByKey("lblType", locale);
			String lblMoreFields  = Utility.getLocaleValuePropByKey("lblMoreFields", locale);
			String lblReasons  = Utility.getLocaleValuePropByKey("lblReasons", locale);
			String lblMoreInfo  = Utility.getLocaleValuePropByKey("lblMoreInfo", locale);
			String lblStudentTeaching   = Utility.getLocaleValuePropByKey("lblStudentTeaching", locale);
			String lblFullTimeTeaching   = Utility.getLocaleValuePropByKey("lblFullTimeTeaching", locale);
			String lblSubstituteTeaching   = Utility.getLocaleValuePropByKey("lblSubstituteTeaching", locale);
			String lblOtherWorkExperience   = Utility.getLocaleValuePropByKey("lblOtherWorkExperience", locale);
			String lblToPresent   = Utility.getLocaleValuePropByKey("lblToPresent", locale);
			String lblResponsibility    = Utility.getLocaleValuePropByKey("lblResponsibility", locale);
			String lblContributions    = Utility.getLocaleValuePropByKey("lblContributions", locale);
			String lblReason    = Utility.getLocaleValuePropByKey("lblReason", locale);
			
			
			int noOfRowInPage 	= Integer.parseInt(noOfRow);
			int pgNo 			= Integer.parseInt(pageNo);
			int start 			= ((pgNo-1)*noOfRowInPage);
			int end 			= ((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord 	= 0;
			//------------------------------------

			TeacherDetail teacherDetail = teacherDetailDAO.findById(teacherId, false, false);

			List<TeacherRole> listTeacherRole= null;
			/** set default sorting fieldName **/
			String sortOrderFieldName	=	"CreatedDateTime";
			String sortOrderNoField		=	"CreatedDateTime";

			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal=null;

			if(sortOrder!=null){
				if(!sortOrder.equals("") && !sortOrder.equals(null) && !sortOrder.equals("type") && !sortOrder.equals("empRoleTypeMaster")){
					sortOrderFieldName=sortOrder;
					sortOrderNoField=sortOrder;
				}
				if(sortOrder.equals("type")){
					sortOrderNoField="type";
				}
				if(sortOrder.equals("empRoleTypeMaster")){
					sortOrderNoField="empRoleTypeMaster";
				}
			}

			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
				if(sortOrderType.equals("0")){
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}else{
				sortOrderTypeVal="0";
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}
			/**End ------------------------------------**/

			listTeacherRole = teacherRoleDAO.findSortedTeacherRoleByTeacher(sortOrderStrVal,teacherDetail);

			List<TeacherRole> sortedTeacherRole		=	new ArrayList<TeacherRole>();

			SortedMap<String,TeacherRole>	sortedMap = new TreeMap<String,TeacherRole>();
			if(sortOrderNoField.equals("type"))
			{
				sortOrderFieldName	=	"type";
			}
			if(sortOrderNoField.equals("empRoleTypeMaster"))
			{
				sortOrderFieldName	=	"empRoleTypeMaster";
			}
			int mapFlag=2;
			for (TeacherRole trRole : listTeacherRole){
				String orderFieldName=trRole.getRole();
				if(sortOrderFieldName.equals("type")){
					orderFieldName=trRole.getFieldMaster().getFieldName()+"||"+trRole.getRoleId();
					sortedMap.put(orderFieldName+"||",trRole);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}
				if(sortOrderFieldName.equals("empRoleTypeMaster")){
					orderFieldName=trRole.getEmpRoleTypeMaster().getEmpRoleTypeName()+"||"+trRole.getRoleId();
					sortedMap.put(orderFieldName+"||",trRole);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}
			}
			if(mapFlag==1){
				NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet();  
				for (Iterator iter=navig.iterator();iter.hasNext();) {  
					Object key = iter.next(); 
					sortedTeacherRole.add((TeacherRole) sortedMap.get(key));
				} 
			}else if(mapFlag==0){
				Iterator iterator = sortedMap.keySet().iterator();
				while (iterator.hasNext()) {
					Object key = iterator.next();
					sortedTeacherRole.add((TeacherRole) sortedMap.get(key));
				}
			}else{
				sortedTeacherRole=listTeacherRole;
			}

			totalRecord =listTeacherRole.size();

			if(totalRecord<end)
				end=totalRecord;
			List<TeacherRole> listsortedTeacherRole		=	sortedTeacherRole.subList(start,end);

			String responseText="";
			String tablePadding_Data="";
			String tablePadding_Data_right="";
			int iRecordCount=1;
			String refDivWidth="";
			if(userMaster.getEntityType().equals(2) || userMaster.getEntityType().equals(3))
			   refDivWidth="830";
			else
			   refDivWidth="730";			
			if(visitLocation.equalsIgnoreCase("My Folders") || visitLocation.equalsIgnoreCase("Mosaic"))
			{
				tablePadding_Data="style='padding-left:10px;vertical-align:top;text-align:left;font-weight: normal;line-height:30px;'";
				tablePadding_Data_right="style='padding-right:10px;vertical-align:top;text-align:right;font-weight: normal;line-height:30px;'";
				
				String tablePadding="style='padding-left:5px;vertical-align:top;color:#FFFFFF;line-height:25px;'";
				sb.append("<table id='tblWorkExp_Profile' border='0'  width='"+refDivWidth+"' class='table-bordered' onmouseover=\"javascript:setGridProfileVariable('workExp')\">");
				sb.append("<thead class='bg'>");
				sb.append("<tr>");
				
				responseText=PaginationAndSorting.responseSortingMLink_1(lblRole,sortOrderFieldName,"role",sortOrderTypeVal,pgNo);
				sb.append("<th align=\"left\" width='148' "+tablePadding+" >"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingMLink_1(lblOrgEmp,sortOrderFieldName,"Organization",sortOrderTypeVal,pgNo);
				sb.append("<th align=\"left\" width='99' "+tablePadding+" >"+responseText+"</th>");

	
				responseText=PaginationAndSorting.responseSortingMLink_1(lblType,sortOrderFieldName,"type",sortOrderTypeVal,pgNo);
				sb.append("<th align=\"left\" width='263' "+tablePadding+" >"+responseText+"</th>");
	
				responseText=PaginationAndSorting.responseSortingMLink_1(lblDuration,sortOrderFieldName,"roleStartYear",sortOrderTypeVal,pgNo);
				sb.append("<th align=\"left\" width='90' "+tablePadding+" >"+responseText+"</th>");
	
//				responseText=PaginationAndSorting.responseSortingMLink_1("Annual&nbsp;Salary",sortOrderFieldName,"amount",sortOrderTypeVal,pgNo);
//				sb.append("<th align=\"left\" width='99' "+tablePadding+" >"+responseText+"</th>");
	
				responseText=PaginationAndSorting.responseSortingMLink_1(lblTypeOfRole,sortOrderFieldName,"empRoleTypeMaster",sortOrderTypeVal,pgNo);
				sb.append("<th align=\"left\" width='111' "+tablePadding+" >"+responseText+"</th>");
				
				
			}
			else if(visitLocation.equalsIgnoreCase("Teacher Pool"))
			{
				tablePadding_Data="";
				tablePadding_Data_right="style='padding-right:10px;text-align:right;'";
				
				sb.append("<table border='0' id='tblWorkExp_Profile' class='table table-striped' onmouseover=\"javascript:setGridProfileVariable('workExp')\">");
				sb.append("<thead class='bg'>");			
				sb.append("<tr>");
	
				responseText=PaginationAndSorting.responseSortingMLink_2_TP(lblRole,sortOrderFieldName,"role",sortOrderTypeVal,pgNo);
				sb.append("<th  valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingMLink_2_TP(lblOrgEmp,sortOrderFieldName,"organization",sortOrderTypeVal,pgNo);
				sb.append("<th  valign='top'>"+responseText+"</th>");
	
				responseText=PaginationAndSorting.responseSortingMLink_2_TP(lblType,sortOrderFieldName,"type",sortOrderTypeVal,pgNo);
				sb.append("<th  valign='top'>"+responseText+"</th>");
	
				responseText=PaginationAndSorting.responseSortingMLink_2_TP(lblDuration,sortOrderFieldName,"roleStartYear",sortOrderTypeVal,pgNo);
				sb.append("<th  valign='top'>"+responseText+"</th>");
	
//				responseText=PaginationAndSorting.responseSortingMLink_2_TP("Annual&nbsp;Salary",sortOrderFieldName,"amount",sortOrderTypeVal,pgNo);
//				sb.append("<th  valign='top'>"+responseText+"</th>");
	
				responseText=PaginationAndSorting.responseSortingMLink_2_TP(lblTypeOfRole,sortOrderFieldName,"empRoleTypeMaster",sortOrderTypeVal,pgNo);
				sb.append("<th  valign='top'>"+responseText+"</th>");
			}
			else
			{
				tablePadding_Data="";
				tablePadding_Data_right="style='padding-right:10px;text-align:right;'";
				
				sb.append("<table border='0' id='tblWorkExp_Profile' class='table table-striped' onmouseover=\"javascript:setGridProfileVariable('workExp')\">");
				sb.append("<thead class='bg'>");			
				sb.append("<tr>");
	
				responseText=PaginationAndSorting.responseSortingMLink_1(lblRole,sortOrderFieldName,"role",sortOrderTypeVal,pgNo);
				sb.append("<th  valign='top'>"+responseText+"</th>");
	
				responseText=PaginationAndSorting.responseSortingMLink_1(lblOrgEmp,sortOrderFieldName,"organization",sortOrderTypeVal,pgNo);
				sb.append("<th  valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingMLink_1(lblType,sortOrderFieldName,"type",sortOrderTypeVal,pgNo);
				sb.append("<th  valign='top'>"+responseText+"</th>");
	
				responseText=PaginationAndSorting.responseSortingMLink_1(lblDuration,sortOrderFieldName,"roleStartYear",sortOrderTypeVal,pgNo);
				sb.append("<th  valign='top'>"+responseText+"</th>");
	
/*				responseText=PaginationAndSorting.responseSortingMLink_1("Annual&nbsp;Salary",sortOrderFieldName,"amount",sortOrderTypeVal,pgNo);
				sb.append("<th valign='top'>"+responseText+"</th>");
*/	
				responseText=PaginationAndSorting.responseSortingMLink_1(lblTypeOfRole,sortOrderFieldName,"empRoleTypeMaster",sortOrderTypeVal,pgNo);
				sb.append("<th  valign='top'>"+responseText+"</th>");
			}
			
			sb.append("</tr>");
			sb.append("</thead>");
			if(listTeacherRole!=null)
			{
				for(TeacherRole tRole:listsortedTeacherRole)
				{
					iRecordCount++;
					
					if(tRole.getNoWorkExp()!=null && tRole.getNoWorkExp()==true)
					{
						sb.append("<tr>");
						sb.append("<td colspan='5'>");
						sb.append("I do not have any work experience.");
						sb.append("</td>");
						
						sb.append("<td>");

						sb.append("<a href='#' onclick=\"return showEditFormEmployment('"+tRole.getRoleId()+"')\" >"+Utility.getLocaleValuePropByKey("lblEdit", locale)+"</a>");
						sb.append("&nbsp;|&nbsp;<a href='#' onclick=\"return deleteEmployment('"+tRole.getRoleId()+"')\">"+Utility.getLocaleValuePropByKey("lblDelete", locale)+"</a>");
						sb.append("</td>");
						
						sb.append("</tr>");
					}
					else
					{
						String gridColor="";
						if(iRecordCount%2==0 && ( visitLocation.equalsIgnoreCase("My Folders") || visitLocation.equalsIgnoreCase("Mosaic"))){
							gridColor="class='bggrid'";
						}
						
						sb.append("<tr "+gridColor+">");
						
						sb.append("<td "+tablePadding_Data+">");
						sb.append(tRole.getRole());
						sb.append("</td>");
						
						sb.append("<td "+tablePadding_Data+">");
						if(tRole.getOrganization()!=null)
							sb.append(tRole.getOrganization());
						else
							sb.append("");
						sb.append("</td>");

						sb.append("<td "+tablePadding_Data+">");
						sb.append(tRole.getFieldMaster().getFieldName());
						sb.append("</td>");

						sb.append("<td "+tablePadding_Data+">");

						if(tRole.getCurrentlyWorking())
						{
							sb.append(tRole.getRoleStartYear()+" to Present");
						}	
						else
						{
							sb.append(tRole.getRoleStartYear()+" to "+tRole.getRoleEndYear());
						}
						sb.append("</td>");
					/*	String currency="";
						String amount="";
						if(tRole.getCurrencyMaster()!=null){
							currency=tRole.getCurrencyMaster().getCurrencyShortName();
						}
						if(tRole.getAmount()!=null){
							amount=tRole.getAmount()+"";
						}
						sb.append("<td "+tablePadding_Data_right+">");
						sb.append(currency+""+amount);
						sb.append("</td>");*/
						if(!nobleflag){
							sb.append("<td "+tablePadding_Data+" >");
							if(tRole.getEmpRoleTypeMaster()!=null)
							sb.append(tRole.getEmpRoleTypeMaster().getEmpRoleTypeName());	
						}else{
							sb.append("<td colspan='2' "+tablePadding_Data+" >");
							if(tRole.getEmpRoleTypeMaster()!=null)
								sb.append(tRole.getEmpRoleTypeMaster().getEmpRoleTypeName());	
						}
						
						sb.append("</td>");	

						sb.append("</tr>");
					}
					
				}
				
				
				if(totalRecord>10 && ( visitLocation.equalsIgnoreCase("My Folders") || visitLocation.equalsIgnoreCase("Mosaic"))){
					sb.append("<tr><td colspan='5' style='border-top: 1px solid #cccccc;padding-bottom:0px;padding-top:5px;'>");
					sb.append(PaginationAndSorting.getPagination_pageSizeVariableName(request,totalRecord,noOfRow, pageNo,"pageSize11","730"));
					sb.append("</td></tr>");
				}
			}

			if(listsortedTeacherRole==null || listsortedTeacherRole.size()==0)
			{
				if(visitLocation.equalsIgnoreCase("My Folders") || visitLocation.equalsIgnoreCase("Mosaic"))
				{
					tablePadding_Data="style='padding-left:10px;vertical-align:top;text-align:left;font-weight: normal;line-height:30px;'";
					sb.append("<tr><td "+tablePadding_Data+" colspan='5'>"+lblNoRecord+"</td></tr>" );
				}
				else
				{
					sb.append("<tr>");
					sb.append("<td colspan='5'>");
					sb.append(lblNoRecord+".");
					sb.append("</td>");
					sb.append("</tr>");
				}
			}
			
			sb.append("</table>");
			if(!visitLocation.equalsIgnoreCase("My Folders") && !visitLocation.equalsIgnoreCase("Mosaic"))
			{
				//sb.append(PaginationAndSorting.getPagination_pageSizeVariableName(request,totalRecord,noOfRow, pageNo,"pageSize11","800"));
				sb.append(PaginationAndSorting.getPaginationPaging(request,totalRecord,noOfRow, pageNo,"pageSize11"));
			}

		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return sb.toString();	
	}
	
	
	
	/**
	 * 
	 * @param noOfRow
	 * @param pageNo
	 * @param sortOrder
	 * @param sortOrderType
	 * @return
	 * @author Ramesh
	 */
	
	// Electronic References
	//@Transactional(readOnly=true)
	public String getElectronicReferencesGrid(Integer teacherId, String noOfRow, String pageNo,String sortOrder,String sortOrderType,String visitLocation,String jobId,DistrictMaster districtMaster_Input,SchoolMaster schoolMaster_Input)
	{	
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		DistrictMaster districtMaster=null;
		SchoolMaster schoolMaster=null;
		UserMaster userMaster=null;
		boolean nobleflag=false;
		int entityID=0,roleId=0;
		if (session == null || session.getAttribute("userMaster") == null) {
			//return "false";
		}else{
			userMaster=(UserMaster)session.getAttribute("userMaster");
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
			if(userMaster.getDistrictId()!=null){
				districtMaster=userMaster.getDistrictId();
				if(districtMaster.getDistrictId()==7800038)
					nobleflag=true;
			}
			if(userMaster.getSchoolId()!=null){
				schoolMaster =userMaster.getSchoolId();
			}	
			entityID=userMaster.getEntityType();
		}
		 
		
		StringBuffer sb = new StringBuffer();		
		try 
		{

			String locale = Utility.getValueOfPropByKey("locale");
			String lblActions = Utility.getLocaleValuePropByKey("lblActions", locale);
			String lblEmail  = Utility.getLocaleValuePropByKey("lblEmail", locale);
			String lblTitle  = Utility.getLocaleValuePropByKey("lblTitle", locale);
			String lblEdit  = Utility.getLocaleValuePropByKey("lblEdit", locale);
			String lblDeactivate = Utility.getLocaleValuePropByKey("lblDeactivate", locale);
            String lblActivate = Utility.getLocaleValuePropByKey("lblActivate", locale);
            String lblNoRecord = Utility.getLocaleValuePropByKey("lblNoRecord", locale);
            String lblOrgEmp  = Utility.getLocaleValuePropByKey("lblOrgEmp", locale);
            String lblRefName  = Utility.getLocaleValuePropByKey("lblRefName ", locale);
            String lblRecLetter  = Utility.getLocaleValuePropByKey("lblRecLetter", locale);
            String lblContactNo  = Utility.getLocaleValuePropByKey("lblContactNo", locale);
            String lblCanContact  = Utility.getLocaleValuePropByKey("lblCanContact ", locale);
            String lblClickToViewLetter  = Utility.getLocaleValuePropByKey("lblClickToViewLetter", locale);
            String lblAddNotesToCandidate = Utility.getLocaleValuePropByKey("lblAddNotesToCandidate", locale);
            String lblNo  = Utility.getLocaleValuePropByKey("lblNo", locale);
            String lblYes   = Utility.getLocaleValuePropByKey("lblYes", locale);
            String lblMrs  = Utility.getLocaleValuePropByKey("lblMrs", locale);
            String lblMr   = Utility.getLocaleValuePropByKey("lblMr", locale);
            String lblMiss  = Utility.getLocaleValuePropByKey("lblMiss", locale);
            String lblDr  = Utility.getLocaleValuePropByKey("lblDr", locale);
            String lblMs   = Utility.getLocaleValuePropByKey("lblMs", locale);
            
			
			
			int noOfRowInPage 	= Integer.parseInt(noOfRow);
			int pgNo 			= Integer.parseInt(pageNo);
			int start 			= ((pgNo-1)*noOfRowInPage);
			int end 			= ((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord 	= 0;
			//------------------------------------

			TeacherDetail teacherDetail = teacherDetailDAO.findById(teacherId, false, false);

			List<TeacherElectronicReferences> listTeacherElectronicReferences= null;
			/** set default sorting fieldName **/
			String sortOrderFieldName	=	"lastName";
			String sortOrderNoField		=	"lastName";

			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal=null;

			if(sortOrder!=null){
				if(!sortOrder.equals("") && !sortOrder.equals(null)){
					sortOrderFieldName=sortOrder;
					sortOrderNoField=sortOrder;
				}
			}

			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
				if(sortOrderType.equals("0")){
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}else{
				sortOrderTypeVal="0";
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}
			/**End ------------------------------------**/
			
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criteriastatus=Restrictions.eq("status", 1);
			
			boolean accessPermission=false;
			
				 if(userMaster.getRoleId().getRoleId().equals(2))
					 accessPermission=true;
					 
				 if(accessPermission || userMaster.getRoleId().getRoleId().equals(3) ){
						Criterion criterionDisNull = Restrictions.isNull("districtMaster");
						Criterion criterionDis = Restrictions.eq("districtMaster", districtMaster);

						Criterion criteriaDistrict   = Restrictions.or(criterionDisNull,criterionDis);
						Criterion criteriaMix   = Restrictions.and(criterion1, criteriaDistrict);
						listTeacherElectronicReferences = teacherElectronicReferencesDAO.findByCriteria(sortOrderStrVal,criteriaMix);	
					}else if(userMaster.getRoleId().getRoleId().equals(1)){
						listTeacherElectronicReferences = teacherElectronicReferencesDAO.findByCriteria(sortOrderStrVal,criterion1);
					}
					else{
						Criterion criterionDis = Restrictions.eq("districtMaster",districtMaster);
						Criterion criterionDisNull = Restrictions.isNull("districtMaster");
						
						Criterion criteriaDistrict   = Restrictions.or(criterionDisNull,criterionDis);
						Criterion criteriaMix   = Restrictions.and(criterion1, criteriaDistrict);
						listTeacherElectronicReferences = teacherElectronicReferencesDAO.findByCriteria(sortOrderStrVal,criteriaMix);		
					}
			
			//listTeacherElectronicReferences = teacherElectronicReferencesDAO.findByCriteria(sortOrderStrVal,criterion1);		
			
			List<TeacherElectronicReferences> sortedTeacherRole		=	new ArrayList<TeacherElectronicReferences>();

			SortedMap<String,TeacherElectronicReferences>	sortedMap = new TreeMap<String,TeacherElectronicReferences>();
			int mapFlag=2;
			
			if(mapFlag==1){
				NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet();  
				for (Iterator iter=navig.iterator();iter.hasNext();) {  
					Object key = iter.next(); 
					sortedTeacherRole.add((TeacherElectronicReferences) sortedMap.get(key));
				} 
			}else if(mapFlag==0){
				Iterator iterator = sortedMap.keySet().iterator();
				while (iterator.hasNext()) {
					Object key = iterator.next();
					sortedTeacherRole.add((TeacherElectronicReferences) sortedMap.get(key));
				}
			}else{
				sortedTeacherRole=listTeacherElectronicReferences;
			}

			totalRecord =listTeacherElectronicReferences.size();

			if(totalRecord<end)
				end=totalRecord;
			List<TeacherElectronicReferences> listsortedTeacherElectronicReferences		=	sortedTeacherRole.subList(start,end);

			String responseText="";
			String tablePadding_Data="";
			int iRecordCount=1;
			
			
			String refDivWidth="";
			if(userMaster.getEntityType().equals(2) || userMaster.getEntityType().equals(3))
				refDivWidth="830";
			else
				refDivWidth="727";
		
			
			
			//changed by Ashish 14-012014
			if(visitLocation.equalsIgnoreCase("My Folders") || visitLocation.equalsIgnoreCase("Mosaic"))
			{
				tablePadding_Data="style='padding-left:5px;vertical-align:middle;text-align:left;font-weight: normal;line-height:30px;'";
				if(!nobleflag){
				tablePadding_Data="style='padding-left:5px;vertical-align:middle;text-align:left;font-weight: normal;line-height:30px;'";
				
				String tablePadding="style='padding-left:0px;vertical-align:top;color:#FFFFFF;line-height:25px;'";
				sb.append("<table id='tbleleReferences_Profile' border='0'  width='"+refDivWidth+"' class='table-bordered' onmouseover=\"javascript:setGridProfileVariable('References')\">");
				sb.append("<thead class='bg'>");
				sb.append("<tr>");
				
				responseText=PaginationAndSorting.responseSortingMLink_1(lblRefName,sortOrderFieldName,"lastName",sortOrderTypeVal,pgNo);
				sb.append("<th align=\"left\" width='120px' "+tablePadding+" >"+responseText+"</th>");

				responseText=PaginationAndSorting.responseSortingMLink_1(lblTitle,sortOrderFieldName,"designation",sortOrderTypeVal,pgNo);
				sb.append("<th align=\"left\" width='85px' "+tablePadding+" >"+responseText+"</th>");

				responseText=PaginationAndSorting.responseSortingMLink_1(lblOrgEmp,sortOrderFieldName,"organization",sortOrderTypeVal,pgNo);
				sb.append("<th align=\"left\" width='100px' "+tablePadding+" >"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingMLink_1(lblEmail,sortOrderFieldName,"email",sortOrderTypeVal,pgNo);
				sb.append("<th align=\"left\" width='143px' "+tablePadding+" >"+responseText+"</th>");
				
				String tablePadding_double="style='padding-left:5px;vertical-align:top;color:#FFFFFF;line-height:15px;'";
				responseText=PaginationAndSorting.responseSortingMLink_1("Rec. Letter",sortOrderFieldName,"pathOfReference",sortOrderTypeVal,pgNo);
				sb.append("<th align=\"left\" width='90px' "+tablePadding_double+" >"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingMLink_1(lblContactNo,sortOrderFieldName,"contactnumber",sortOrderTypeVal,pgNo);
				sb.append("<th align=\"left\" width='82px' "+tablePadding+" >"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingMLink_1(lblCanContact,sortOrderFieldName,"rdcontacted",sortOrderTypeVal,pgNo);
				sb.append("<th align=\"left\" width='65px' "+tablePadding_double+" >"+responseText+"</th>");
				
				sb.append("<th align=\"left\" width='45px' "+tablePadding+" >"+lblNotes+"</th>");
				if(userMaster.getRoleId().getRoleId().equals(2) || userMaster.getRoleId().getRoleId().equals(3)){
				     sb.append("<th align=\"left\" width='45px' "+tablePadding+" >"+lblActions+"</th>");
				 }
			}else{
					String tablePadding="style='padding-left:0px;vertical-align:top;color:#FFFFFF;line-height:25px;'";
					sb.append("<table id='tbleleReferences_Profile' border='0'  width='"+refDivWidth+"' class='table-bordered' onmouseover=\"javascript:setGridProfileVariable('References')\">");
					sb.append("<thead class='bg'>");
					sb.append("<tr>");
					
					responseText=PaginationAndSorting.responseSortingMLink_1(lblRefName,sortOrderFieldName,"lastName",sortOrderTypeVal,pgNo);
					sb.append("<th align=\"left\" width='140px' "+tablePadding+" >"+responseText+"</th>");

					responseText=PaginationAndSorting.responseSortingMLink_1(lblTitle,sortOrderFieldName,"designation",sortOrderTypeVal,pgNo);
					sb.append("<th align=\"left\" width='86px' "+tablePadding+" >"+responseText+"</th>");

					responseText=PaginationAndSorting.responseSortingMLink_1(lblOrgEmp,sortOrderFieldName,"organization",sortOrderTypeVal,pgNo);
					sb.append("<th align=\"left\" width='90px' "+tablePadding+" >"+responseText+"</th>");
					
					responseText=PaginationAndSorting.responseSortingMLink_1(lblEmail,sortOrderFieldName,"email",sortOrderTypeVal,pgNo);
					sb.append("<th align=\"left\" width='142px' "+tablePadding+" >"+responseText+"</th>");
					
					String tablePadding_double="style='padding-left:5px;vertical-align:top;color:#FFFFFF;line-height:15px;'";
					
					responseText=PaginationAndSorting.responseSortingMLink_1(lblContactNo,sortOrderFieldName,"contactnumber",sortOrderTypeVal,pgNo);
					sb.append("<th align=\"left\" width='82px' "+tablePadding+" >"+responseText+"</th>");
					
					
					sb.append("<th align=\"left\" width='85px' "+tablePadding+" >&nbsp;&nbsp;"+lblNotes+"</th>");
					
					 if(userMaster.getRoleId().getRoleId().equals(2) || userMaster.getRoleId().getRoleId().equals(3)){
					     sb.append("<th align=\"left\" width='45px' "+tablePadding+" >"+lblActions+"</th>");
					 }
				  
				  
			  }
			}
			else if(visitLocation.equalsIgnoreCase("Teacher Pool"))
			{
				tablePadding_Data="";
				
				sb.append("<table border='0' id='tbleleReferences_Profile' class='table table-striped' onmouseover=\"javascript:setGridProfileVariable('References')\">");
				sb.append("<thead class='bg'>");			
				sb.append("<tr>");
	
				responseText=PaginationAndSorting.responseSortingMLink_2_TP(lblRefName,sortOrderFieldName,"lastName",sortOrderTypeVal,pgNo);
				sb.append("<th  valign='top'>"+responseText+"</th>");
	
				responseText=PaginationAndSorting.responseSortingMLink_2_TP(lblTitle,sortOrderFieldName,"designation",sortOrderTypeVal,pgNo);
				sb.append("<th  valign='top'>"+responseText+"</th>");
	
				responseText=PaginationAndSorting.responseSortingMLink_2_TP(lblOrgEmp,sortOrderFieldName,"organization",sortOrderTypeVal,pgNo);
				sb.append("<th  valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingMLink_2_TP(lblEmail,sortOrderFieldName,"email",sortOrderTypeVal,pgNo);
				sb.append("<th  valign='top'>"+responseText+"</th>");
				if(!nobleflag){
					responseText=PaginationAndSorting.responseSortingMLink_2_TP(lblRecLetter,sortOrderFieldName,"pathOfReference",sortOrderTypeVal,pgNo);
					sb.append("<th  valign='top'>"+responseText+"</th>");
				}
				
				responseText=PaginationAndSorting.responseSortingMLink_2_TP(lblContactNo,sortOrderFieldName,"contactnumber",sortOrderTypeVal,pgNo);
				sb.append("<th  valign='top'>"+responseText+"</th>");
				if(!nobleflag){
					responseText=PaginationAndSorting.responseSortingMLink_2_TP("<p style='line-height:100%;'>"+lblCanContact+"</p>",sortOrderFieldName,"rdcontacted",sortOrderTypeVal,pgNo);
					sb.append("<th  valign='top'>"+responseText+"</th>");
				}
				sb.append("<th  valign='middle' style='vertical-align:middle;'>"+lblNotes+"</th>");
				
				 if(userMaster.getRoleId().getRoleId().equals(2) || userMaster.getRoleId().getRoleId().equals(3)){
				     sb.append("<th align=\"left\" width='45px'  >"+lblActions+"</th>");
				 }
			}
			else
			{
				tablePadding_Data="";
				
				sb.append("<table border='0' id='tbleleReferences_Profile' class='table table-striped' onmouseover=\"javascript:setGridProfileVariable('References')\">");
				sb.append("<thead class='bg'>");			
				sb.append("<tr>");
	
				responseText=PaginationAndSorting.responseSortingMLink_1(lblRefName,sortOrderFieldName,"lastName",sortOrderTypeVal,pgNo);
				sb.append("<th  valign='top'>"+responseText+"</th>");
	
				responseText=PaginationAndSorting.responseSortingMLink_1(lblTitle,sortOrderFieldName,"designation",sortOrderTypeVal,pgNo);
				sb.append("<th  valign='top'>"+responseText+"</th>");
	
				responseText=PaginationAndSorting.responseSortingMLink_1(lblOrgEmp,sortOrderFieldName,"organization",sortOrderTypeVal,pgNo);
				sb.append("<th  valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingMLink_1(lblEmail,sortOrderFieldName,"email",sortOrderTypeVal,pgNo);
				sb.append("<th  valign='top'>"+responseText+"</th>");
				if(!nobleflag){
					responseText=PaginationAndSorting.responseSortingMLink_1(lblRecLetter,sortOrderFieldName,"pathOfReference",sortOrderTypeVal,pgNo);
					sb.append("<th  valign='top'>"+responseText+"</th>");
				}
				responseText=PaginationAndSorting.responseSortingMLink_1(lblContactNo,sortOrderFieldName,"contactnumber",sortOrderTypeVal,pgNo);
				sb.append("<th  valign='top'>"+responseText+"</th>");
				
				if(!nobleflag){
					responseText=PaginationAndSorting.responseSortingMLink_1("<p style='line-height:100%;'>"+lblCanContact+"</p>",sortOrderFieldName,"rdcontacted",sortOrderTypeVal,pgNo);
					sb.append("<th  valign='top'>"+responseText+"</th>");
				}
				
				sb.append("<th  valign='middle' style='vertical-align:middle;'>"+lblNotes+"</th>");
				
				 if(userMaster.getRoleId().getRoleId().equals(2) || userMaster.getRoleId().getRoleId().equals(3)){
				     sb.append("<th align=\"left\" width='45px' >"+lblActions+"</th>");
				 }
				
			}
			sb.append("</tr>");
			sb.append("</thead>");
			if(listTeacherElectronicReferences!=null)
			{
				//Criterion criterion_ref = Restrictions.in("electronicReferences", listsortedTeacherElectronicReferences);
				//Criterion criterion_user = Restrictions.eq("userMaster", userMaster);
				//List<ReferenceNote> referenceNoteList=referenceNoteDAO.findByCriteria(criterion1,criterion_user);
				
				List<ReferenceNote> referenceNoteList=null;
				
				Criterion criterion_teacher = Restrictions.eq("teacherDetail",teacherDetail);
				Criterion criterion_districtId = Restrictions.eq("districtMaster",districtMaster);
				//Criterion criterion_schoolId = Restrictions.eq("schoolId",schoolMaster);
				
				/*JobOrder   jobOrder =null;
				if(jobId!=null && !jobId.equals("0") && !jobId.equals("")){
					jobOrder=jobOrderDAO.findById(Integer.parseInt(jobId), false, false);
					Criterion criterion_job = Restrictions.eq("jobId", jobOrder);
					if(entityID==1){ // For TM
						referenceNoteList=referenceNoteDAO.findByCriteria(criterion_teacher,criterion_job);
					}
					else
					{ // For District
						referenceNoteList=referenceNoteDAO.findByCriteria(criterion_teacher,criterion_job,criterion_districtId);
					}
					else if(entityID==3){// For School
						referenceNoteList=referenceNoteDAO.findByCriteria(criterion_teacher,criterion_job,criterion_schoolId,criterion_districtId);	
					}
				}
				else
				{*/
					if(entityID==1){ // For TM
						Criterion criterion_districtId_input = Restrictions.eq("districtMaster",districtMaster_Input);
						if(districtMaster_Input==null)
							referenceNoteList=referenceNoteDAO.findByCriteria(criterion_teacher);
						else
							referenceNoteList=referenceNoteDAO.findByCriteria(criterion_teacher,criterion_districtId_input);
					}
					else // For District
						referenceNoteList=referenceNoteDAO.findByCriteria(criterion_teacher,criterion_districtId);
				//}
				
				for(TeacherElectronicReferences tRole:listsortedTeacherElectronicReferences)
				{
					//check by alok					
						iRecordCount++;
						String gridColor="";
						if(iRecordCount%2==0 && ( visitLocation.equalsIgnoreCase("My Folders") || visitLocation.equalsIgnoreCase("Mosaic"))){
							gridColor="class='bggrid'";
						}
						
						String Salutation="";
						if(tRole.getSalutation()==0)
							Salutation="";
						else if(tRole.getSalutation()==1)
							Salutation=lblMrs;
						else if(tRole.getSalutation()==2)
							Salutation=lblMr;
						else if(tRole.getSalutation()==3)
							Salutation=lblMiss;
						else if(tRole.getSalutation()==4)
							Salutation=lblDr;
						else if(tRole.getSalutation()==5)
							Salutation=lblMs;
								
						sb.append("<tr "+gridColor+">");
						
						sb.append("<td "+tablePadding_Data+">");
						sb.append(Salutation+" "+tRole.getFirstName()+" "+tRole.getLastName());
						sb.append("</td>");
						
						sb.append("<td "+tablePadding_Data+">");
						sb.append(tRole.getDesignation());
						sb.append("</td>");
						
						sb.append("<td "+tablePadding_Data+">");
						sb.append(tRole.getOrganization());
						sb.append("</td>");
						
						sb.append("<td "+tablePadding_Data+">");
						sb.append("<a href='mailto:"+tRole.getEmail()+"' rel='tooltip' data-original-title='"+tRole.getEmail()+"'  id='mail"+tRole.getElerefAutoId()+"'>"+tRole.getEmail()+"</a>");
						sb.append("<script>$('#mail"+tRole.getElerefAutoId()+"').tooltip();</script>");
						sb.append("</td>");
						
						
						String windowFunc="if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
						if(!nobleflag){
							sb.append("<td>");
							if(tRole.getPathOfReference()!=null && !tRole.getPathOfReference().equals("")){
								//sb.append("<a href='javascript:void(0)' rel='tooltip' data-original-title='"+lblClickToViewLetter+"'  id='trans"+tRole.getElerefAutoId()+"' onclick=\"downloadReferenceForCandidate('"+tRole.getElerefAutoId()+"','"+tRole.getTeacherDetail().getTeacherId()+"','trans"+tRole.getElerefAutoId()+"');"+windowFunc+"\">Yes</a>");
								sb.append("<a href='javascript:void(0)' id='trans"+tRole.getElerefAutoId()+"' onclick=\"downloadReferenceForCandidate('"+tRole.getElerefAutoId()+"','"+tRole.getTeacherDetail().getTeacherId()+"','trans"+tRole.getElerefAutoId()+"');"+windowFunc+"\">Yes</a>");
								sb.append("<script>$('#trans"+tRole.getElerefAutoId()+"').tooltip();</script>");
							}else{
								sb.append(lblNo);
							}
							
							sb.append("</td>");
						}
						sb.append("<td "+tablePadding_Data+">");
						sb.append(tRole.getContactnumber());
						sb.append("</td>");
						
						if(!nobleflag){
							sb.append("<td "+tablePadding_Data+">");
							
							if(tRole.getRdcontacted())
								sb.append(lblYes);
							else if(!tRole.getRdcontacted())
								sb.append(lblNo);
							sb.append("</td>");
						}
						String tablePadding_Data_Img="style='padding-left:10px;vertical-align:middle;text-align:left;font-weight: normal;line-height:30px;'";
						
						sb.append("<td width='40' "+tablePadding_Data_Img+">");
						
						if(TeacherProfileViewInDivAjax.isReferenceNote(referenceNoteList, tRole.getElerefAutoId())){
							sb.append("<a data-original-title='"+lblAddNotesToCandidate+"' rel='tooltip' id='RefNotesToolTips' href='javascript:void(0);' onclick=\"getRefNotesDiv("+tRole.getElerefAutoId()+")\" ><img src=\"images/note_com.png\"></a>");
						}
						else
						{
							sb.append("<a data-original-title='"+lblAddNotesToCandidate+"' rel='tooltip' id='RefNotesToolTips' href='javascript:void(0);' onclick=\"getRefNotesDiv("+tRole.getElerefAutoId()+")\" ><img src=\"images/note_com1.png\"></a>");
						}
						
						//sb.append("<a data-original-title='Add Notes on the Candidate' rel='tooltip' id='RefNotesToolTips' href='javascript:void(0);' onclick=\"getNotesDiv('"+teacherId+"','0')\" ><img src=\"images/note_com.png\"></a>");
						//sb.append("<a data-original-title='Add Notes on the Candidate' rel='tooltip' id='RefNotesToolTips' href='javascript:void(0);' onclick=\"getRefNotesDiv('"+teacherId+"','0')\" ><img src=\"images/not.png\"><img src=\"images/note_com.png\"></a>");
						
						sb.append("</td>");
						
						
							 if(accessPermission || userMaster.getRoleId().getRoleId().equals(3))
							 {
								if(tRole.getDistrictMaster()!=null){
									if(tRole.getUserMaster().getUserId().equals(userMaster.getUserId())){
										 sb.append("<td nowrap><a href='javascript:void(0);' onclick=\"editFormElectronicReferences("+tRole.getElerefAutoId()+")\">"+lblEdit+"</a>");
										if(tRole.getStatus()==0)
											sb.append("&nbsp;|&nbsp;<a href='#' onclick=\"return changeStatusElectronicReferences('"+tRole.getElerefAutoId()+"','1')\">"+lblActivate+"</a>");
										else if(tRole.getStatus()==1)
											sb.append("&nbsp;|&nbsp;<a href='#' onclick=\"return changeStatusElectronicReferences('"+tRole.getElerefAutoId()+"','0')\">"+lblDeactivate+"</a>");
									}else
										sb.append("<td></td>");
								}else{
									sb.append("<td></td>");
									
								}
							 }else
								 sb.append("<td></td>");
					    
						
						sb.append("</tr>");
					
				}
				
				if(totalRecord>10 && ( visitLocation.equalsIgnoreCase("My Folders") || visitLocation.equalsIgnoreCase("Mosaic"))){
					sb.append("<tr><td colspan='6' style='border-top: 1px solid #cccccc;padding-bottom:0px;padding-top:5px;line-height:30px;'>");
					sb.append(PaginationAndSorting.getPagination_pageSizeVariableName(request,totalRecord,noOfRow, pageNo,"pageSize12","730"));
					sb.append("</td></tr>");
				}
			}

			if(listsortedTeacherElectronicReferences==null || listsortedTeacherElectronicReferences.size()==0)
			{
				if(visitLocation.equalsIgnoreCase("My Folders") || visitLocation.equalsIgnoreCase("Mosaic"))
				{
					tablePadding_Data="style='padding-left:10px;vertical-align:top;text-align:left;font-weight: normal;line-height:30px;'";
					sb.append("<tr><td "+tablePadding_Data+" colspan='6'>"+lblNoRecord+".</td></tr>" );
				}
				else
				{
					sb.append("<tr>");
					sb.append("<td colspan='6'>");
					sb.append(lblNoRecord+".");
					sb.append("</td>");
					sb.append("</tr>");
				}
			}
			
			sb.append("</table>");
			if(!visitLocation.equalsIgnoreCase("My Folders") && !visitLocation.equalsIgnoreCase("Mosaic"))
			{
				//sb.append(PaginationAndSorting.getPagination_pageSizeVariableName(request,totalRecord,noOfRow, pageNo,"pageSize12","800"));
				sb.append(PaginationAndSorting.getPaginationPaging(request,totalRecord,noOfRow, pageNo,"pageSize12"));
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return sb.toString();	
	}
	
	
	/**
	 * 
	 * @param teacherId
	 * @param noOfRow
	 * @param pageNo
	 * @param sortOrder
	 * @param sortOrderType
	 * @return
	 * @author Ramesh Kumar Bhartiya
	 * Date Nov 14, 2013
	 */
	// Video Links
	//@Transactional(readOnly=true)
	public String getVideoLinksGrid(Integer teacherId, String noOfRow, String pageNo,String sortOrder,String sortOrderType,String visitLocation)
	{		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster=null;
		boolean nobleflag=false;
		DistrictMaster districtMaster=null;
		int roleId=0;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		else{
			userMaster=(UserMaster)session.getAttribute("userMaster");
			districtMaster=userMaster.getDistrictId();
			if(districtMaster!=null)
			{
				System.out.println("districtMaster.getDistrictId()districtMaster.getDistrictId() "+districtMaster.getDistrictId());
			 if(districtMaster.getDistrictId()==7800038)
				 nobleflag=true;
			}
		}
		boolean accessPermission=false;
		
			if(userMaster.getRoleId().getRoleId().equals(2))
			    accessPermission=true;
		/*if(userMaster.getRoleId().getRoleId()!=null)
			roleId=userMaster.getRoleId().getRoleId();
		*/
		

		StringBuffer sb = new StringBuffer();		
		try 
		{
			String locale = Utility.getValueOfPropByKey("locale");
			String lblEdit  = Utility.getLocaleValuePropByKey("lblEdit", locale);
			String lblDelete   = Utility.getLocaleValuePropByKey("lblDelete", locale);
			String lblNoRecord = Utility.getLocaleValuePropByKey("lblNoRecord", locale);
			String lblActions = Utility.getLocaleValuePropByKey("lblActions", locale);
			String lblVideoLink  = Utility.getLocaleValuePropByKey("lblVideoLink ", locale);
			String lblVideo  = Utility.getLocaleValuePropByKey("lblVideo ", locale);
			String lblUploadedDate  = Utility.getLocaleValuePropByKey("lblUploadedDate ", locale);
			
			
			int noOfRowInPage 	= Integer.parseInt(noOfRow);
			int pgNo 			= Integer.parseInt(pageNo);
			int start 			= ((pgNo-1)*noOfRowInPage);
			int end 			= ((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord 	= 0;
			//------------------------------------

			TeacherDetail teacherDetail = teacherDetailDAO.findById(teacherId, false, false);

			List<TeacherVideoLink> listTeacherVideoLink= null;
			/** set default sorting fieldName **/
			String sortOrderFieldName	=	"createdDate";
			String sortOrderNoField		=	"createdDate";

			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal=null;

			if(sortOrder!=null){
				if(!sortOrder.equals("") && !sortOrder.equals(null)){
					sortOrderFieldName=sortOrder;
					sortOrderNoField=sortOrder;
				}
			}

			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
				if(sortOrderType.equals("0")){
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}else{
				sortOrderTypeVal="0";
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}
			/**End ------------------------------------**/
			
			
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);			
			if(accessPermission || userMaster.getRoleId().getRoleId().equals(3)){
				Criterion criterionDisNull = Restrictions.isNull("districtMaster");
				Criterion criterionDis = Restrictions.eq("districtMaster", districtMaster);
				Criterion criteriaDistrict   = Restrictions.or(criterionDisNull,criterionDis);
				Criterion criteriaMix   = Restrictions.and(criterion1, criteriaDistrict);
				listTeacherVideoLink = teacherVideoLinksDAO.findByCriteria(sortOrderStrVal,criteriaMix);	
			}else if(userMaster.getRoleId().getRoleId().equals(1)){
				listTeacherVideoLink = teacherVideoLinksDAO.findByCriteria(sortOrderStrVal,criterion1);
			}
			else{
				Criterion criterionDis = Restrictions.eq("districtMaster",districtMaster);
				Criterion criterionDisNull = Restrictions.isNull("districtMaster");
				
				Criterion criteriaDistrict   = Restrictions.or(criterionDisNull,criterionDis);
				Criterion criteriaMix   = Restrictions.and(criterion1, criteriaDistrict);
				listTeacherVideoLink = teacherVideoLinksDAO.findByCriteria(sortOrderStrVal,criteriaMix);		
			}
			
			List<TeacherVideoLink> sortedTeacherRole		=	new ArrayList<TeacherVideoLink>();

			SortedMap<String,TeacherVideoLink>	sortedMap = new TreeMap<String,TeacherVideoLink>();
			/*if(sortOrderNoField.equals("type"))
			{
				sortOrderFieldName	=	"type";
			}
			if(sortOrderNoField.equals("empRoleTypeMaster"))
			{
				sortOrderFieldName	=	"empRoleTypeMaster";
			}*/
			int mapFlag=2;
			/*for (TeacherElectronicReferences trRole : listTeacherElectronicReferences){
				String orderFieldName=trRole.getRole();
				if(sortOrderFieldName.equals("type")){
					orderFieldName=trRole.getFieldMaster().getFieldName()+"||"+trRole.getRoleId();
					sortedMap.put(orderFieldName+"||",trRole);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}
				if(sortOrderFieldName.equals("empRoleTypeMaster")){
					orderFieldName=trRole.getEmpRoleTypeMaster().getEmpRoleTypeName()+"||"+trRole.getRoleId();
					sortedMap.put(orderFieldName+"||",trRole);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}
			}*/
			
			if(mapFlag==1){
				NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet();  
				for (Iterator iter=navig.iterator();iter.hasNext();) {  
					Object key = iter.next(); 
					sortedTeacherRole.add((TeacherVideoLink) sortedMap.get(key));
				} 
			}else if(mapFlag==0){
				Iterator iterator = sortedMap.keySet().iterator();
				while (iterator.hasNext()) {
					Object key = iterator.next();
					sortedTeacherRole.add((TeacherVideoLink) sortedMap.get(key));
				}
			}else{
				sortedTeacherRole=listTeacherVideoLink;
			}

			totalRecord =listTeacherVideoLink.size();

			if(totalRecord<end)
				end=totalRecord;
			List<TeacherVideoLink> listsortedTeacherVideoLink		=	sortedTeacherRole.subList(start,end);

			String responseText="";
			String tablePadding_Data="";
			int iRecordCount=1;
			String refDivWidth="";
			if(userMaster.getEntityType().equals(2) || userMaster.getEntityType().equals(3))
			  refDivWidth="830";
			else
			  refDivWidth="730";			
			if(visitLocation.equalsIgnoreCase("My Folders") || visitLocation.equalsIgnoreCase("Mosaic"))
			{
				tablePadding_Data="style='padding-left:10px;vertical-align:top;text-align:left;font-weight: normal;line-height:30px;'";
				
				String tablePadding="style='padding-left:5px;vertical-align:top;color:#FFFFFF;line-height:25px;'";
				sb.append("<table id='tblelevideoLink_Profile' border='0' width='"+refDivWidth+"' class='table-bordered' onmouseover=\"javascript:setGridProfileVariable('videoLink')\">");
				sb.append("<thead class='bg'>");
				sb.append("<tr>");
				
				if(accessPermission ||userMaster.getRoleId().getRoleId().equals(3)){
				responseText=PaginationAndSorting.responseSortingMLink_1(lblVideoLink,sortOrderFieldName,"videourl",sortOrderTypeVal,pgNo);
				sb.append("<th align=\"left\" width='630' "+tablePadding+" >"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingMLink_1(lblVideo,sortOrderFieldName,"video",sortOrderTypeVal,pgNo);
				sb.append("<th align=\"left\" width='100' "+tablePadding+" >"+responseText+"</th>");
	
				responseText=PaginationAndSorting.responseSortingMLink_1(lblUploadedDate,sortOrderFieldName,"createdDate",sortOrderTypeVal,pgNo);
				sb.append("<th align=\"left\" width='100' "+tablePadding+" >"+responseText+"</th>");
	
 				sb.append("<th  align=\"left\" width='100' "+tablePadding+">"+lblActions+"</th>");
				}else{
				   responseText=PaginationAndSorting.responseSortingMLink_1(lblVideoLink,sortOrderFieldName,"videourl",sortOrderTypeVal,pgNo);
				   sb.append("<th align=\"left\" width='530' "+tablePadding+" >"+responseText+"</th>");
		
				   responseText=PaginationAndSorting.responseSortingMLink_1(lblVideo,sortOrderFieldName,"video",sortOrderTypeVal,pgNo);
					sb.append("<th align=\"left\" width='100' "+tablePadding+" >"+responseText+"</th>");
				   
				   responseText=PaginationAndSorting.responseSortingMLink_1(lblUploadedDate,sortOrderFieldName,"createdDate",sortOrderTypeVal,pgNo);
				   sb.append("<th align=\"left\" width='100' "+tablePadding+" >"+responseText+"</th>");
				}			}
			else if(visitLocation.equalsIgnoreCase("Teacher Pool"))
			{
				tablePadding_Data="";
				
				sb.append("<table border='0' id='tblelevideoLink_Profile' class='table width='730' table-striped' onmouseover=\"javascript:setGridProfileVariable('videoLink')\">");
				sb.append("<thead class='bg'>");			
				sb.append("<tr>");
	
				responseText=PaginationAndSorting.responseSortingMLink_2_TP(lblVideoLink,sortOrderFieldName,"videourl",sortOrderTypeVal,pgNo);
				sb.append("<th>"+responseText+"</th>");
	
				responseText=PaginationAndSorting.responseSortingMLink_2_TP(lblVideo,sortOrderFieldName,"video",sortOrderTypeVal,pgNo);
				sb.append("<th>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingMLink_2_TP(lblUploadedDate,sortOrderFieldName,"createdDate",sortOrderTypeVal,pgNo);
				sb.append("<th>"+responseText+"</th>");
				if(accessPermission || userMaster.getRoleId().getRoleId().equals(3))
					  sb.append("<th>"+lblActions+"</th>");			}
			else
			{
				tablePadding_Data="";
				
				sb.append("<table border='0' id='tblelevideoLink_Profile' class='table table-striped' onmouseover=\"javascript:setGridProfileVariable('videoLink')\">");
				sb.append("<thead class='bg'>");			
				sb.append("<tr>");
	
				responseText=PaginationAndSorting.responseSortingMLink_1(lblVideoLink,sortOrderFieldName,"videourl",sortOrderTypeVal,pgNo);
				sb.append("<th>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingMLink_1(lblVideo,sortOrderFieldName,"video",sortOrderTypeVal,pgNo);
				sb.append("<th>"+responseText+"</th>");
	
				responseText=PaginationAndSorting.responseSortingMLink_1(lblUploadedDate,sortOrderFieldName,"createdDate",sortOrderTypeVal,pgNo);
				sb.append("<th>"+responseText+"</th>");
				
				//responseText=PaginationAndSorting.responseSortingMLink_1("Uploaded&nbsp;Date",sortOrderFieldName,"createdDate",sortOrderTypeVal,pgNo);
				if(accessPermission  || userMaster.getRoleId().getRoleId().equals(3))
				     sb.append("<th>"+lblActions+"</th>");
				
			}

			sb.append("</tr>");
			sb.append("</thead>");
			if(listTeacherVideoLink!=null)
			{
				for(TeacherVideoLink pojo:listsortedTeacherVideoLink)
				{
					
					iRecordCount++;
					String gridColor="";
					if(iRecordCount%2==0 && ( visitLocation.equalsIgnoreCase("My Folders"))){
						gridColor="class='bggrid'";
					}
					
					sb.append("<tr "+gridColor+">");
					
					sb.append("<td "+tablePadding_Data+">");
					sb.append("<a href='"+pojo.getVideourl()+"' target='_blank'>"+pojo.getVideourl()+"</a>");
					sb.append("</td>");
					
					sb.append("<td "+tablePadding_Data+">");					
					if(pojo.getVideo()==null)					
						sb.append("");						
					else	
					sb.append("<a onclick='return getVideoPlay("+pojo.getVideolinkAutoId()+")'  href='javascript:void(0)'>"+pojo.getVideo()+"</a>");
					sb.append("</td>");
					
					sb.append("<td "+tablePadding_Data+">");
					sb.append(Utility.convertDateAndTimeToUSformatOnlyDate(pojo.getCreatedDate()));
					sb.append("</td>");
					if(accessPermission || userMaster.getRoleId().getRoleId().equals(3)){
						if(pojo.getDistrictMaster()!=null){
							if(pojo.getUserMaster().getUserId().equals(userMaster.getUserId())){
								sb.append("<td nowrap><a href='javascript:void(0);' onclick=\"editFormVideoLink("+pojo.getVideolinkAutoId()+")\">"+lblEdit+"</a>");
								sb.append("&nbsp;|&nbsp;<a href='javascript:void(0);' onclick=\"delVideoLink("+pojo.getVideolinkAutoId()+")\">"+lblDelete+"</a>");
							}else{
								sb.append("<td></td>");
							}
						}
						else{
							sb.append("<td></td>");
						}
					}else{
						sb.append("<td></td>");
					}
					/*if(tRole.getStatus()==0)
						sb.append("&nbsp;|&nbsp;<a href='#' onclick=\"return changeStatusElectronicReferences('"+pojo.get+"','1')\">Activate</a>");
					else if(tRole.getStatus()==1)
						sb.append("&nbsp;|&nbsp;<a href='#' onclick=\"return changeStatusElectronicReferences('"+tRole.getElerefAutoId()+"','0')\">Deactivate</a>");
					*/
					
					sb.append("</tr>");
				}
				
				if(totalRecord>10 && ( visitLocation.equalsIgnoreCase("My Folders") || visitLocation.equalsIgnoreCase("Mosaic"))){
					sb.append("<tr><td colspan='2' style='border-top: 1px solid #cccccc;padding-bottom:0px;padding-top:5px;'>");
					sb.append(PaginationAndSorting.getPagination_pageSizeVariableName(request,totalRecord,noOfRow, pageNo,"pageSize13","730"));
					sb.append("</td></tr>");
				}
				
				
			}
			if(listsortedTeacherVideoLink==null || listsortedTeacherVideoLink.size()==0)
			{
				if(visitLocation.equalsIgnoreCase("My Folders") || visitLocation.equalsIgnoreCase("Mosaic"))
				{
					tablePadding_Data="style='padding-left:10px;vertical-align:top;text-align:left;font-weight: normal;line-height:30px;'";
					sb.append("<tr><td "+tablePadding_Data+" colspan='2'>"+lblNoRecord+".</td></tr>" );
				}
				else
				{
					sb.append("<tr>");
					sb.append("<td colspan='2'>");
					sb.append(lblNoRecord+".");
					sb.append("</td>");
					sb.append("</tr>");
				}
			}
			
			sb.append("</table>");
			if(!visitLocation.equalsIgnoreCase("My Folders") && !visitLocation.equalsIgnoreCase("Mosaic"))
			{
				//sb.append(PaginationAndSorting.getPaginationPaging(request,totalRecord,noOfRow, pageNo,"pageSize13"));
				sb.append(PaginationAndSorting.getPaginationPaging(request,totalRecord,noOfRow, pageNo,"pageSize13"));
			}
			
		} 
		
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return sb.toString();	
	}
	public String getAdditionalDocumentsGrid(String noOfRow, String pageNo,String sortOrder,String sortOrderType,Integer teacherId,String visitLocation)
	{
		
		
		System.out.println(" getAdditionalDocumentsGrid >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> "+visitLocation);
		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster=null;
		DistrictMaster districtMaster=null;
		int roleId=0;
		boolean nobleflag=false;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }else{
		     userMaster=(UserMaster)session.getAttribute("userMaster");
		     districtMaster=userMaster.getDistrictId();
			    if(districtMaster!=null)
			    	if(districtMaster.getDistrictId()==7800038)
			    		nobleflag=true;
	    }
		if(userMaster.getRoleId().getRoleId()!=null)
			roleId=userMaster.getRoleId().getRoleId();

		StringBuffer sb = new StringBuffer();		
		try 
		{
			String locale = Utility.getValueOfPropByKey("locale");
			String lblNoRecord = Utility.getLocaleValuePropByKey("lblNoRecord", locale);
			String lbllDocName = Utility.getLocaleValuePropByKey("lbllDocName", locale);
			String lblDoc = Utility.getLocaleValuePropByKey("lblDoc", locale);
			
			/*============= For Sorting ===========*/
			int noOfRowInPage 	= 	Integer.parseInt(noOfRow);
			int pgNo 			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord		= 	0;
			//------------------------------------
			/*====== set default sorting fieldName ====== **/
			String sortOrderFieldName	=	"createdDateTime";
			String sortOrderNoField		=	"createdDateTime";

			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal		=	null;
			if(sortOrder!=null){
				 if(!sortOrder.equals("") && !sortOrder.equals(null)){
					 sortOrderFieldName=sortOrder;
					 sortOrderNoField=sortOrder;
				 }
			}
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal		=	Order.asc(sortOrderFieldName);
				}
				else
				{
					sortOrderTypeVal	=	"1";
					sortOrderStrVal		=	Order.desc(sortOrderFieldName);
				}
			}
			else
			{
				sortOrderTypeVal		=	"0";
				sortOrderStrVal			=	Order.asc(sortOrderFieldName);
			}
			
			/*=============== End ======================*/	
			
			TeacherDetail teacherDetail= teacherDetailDAO.findById(teacherId, false, false);
			List<TeacherAdditionalDocuments> lstTeacherAdditionalDocuments=teacherAdditionalDocumentsDAO.findSortedAdditionalDocumentsByTeacher(sortOrderStrVal,teacherDetail);
			
			totalRecord =lstTeacherAdditionalDocuments.size();

			//List<TeacherAdditionalDocuments> sortedAdditionalDoc		=	new ArrayList<TeacherAdditionalDocuments>();
			
			
			if(totalRecord<end)
				end=totalRecord;
			List<TeacherAdditionalDocuments> listsortedTeacherAddtionalDoc		=	lstTeacherAdditionalDocuments.subList(start,end);
			
			
			String responseText="";
			String tablePadding_Data="";
			int iRecordCount=1;
			
			
			String refDivWidth="";
			String width1="";
			String widt2="";
			if(userMaster.getEntityType().equals(2) || userMaster.getEntityType().equals(3))
			{
				refDivWidth="830";
				width1="680";
				widt2="150";
			}
			else
			{
				refDivWidth="730";
				width1="630";
				widt2="100";
			}
			
			if(visitLocation.equalsIgnoreCase("My Folders") || visitLocation.equalsIgnoreCase("Mosaic"))
			{
				tablePadding_Data="style='padding-left:10px;vertical-align:top;text-align:left;font-weight: normal;line-height:30px;'";
				
				String tablePadding="style='padding-left:5px;vertical-align:top;color:#FFFFFF;line-height:25px;'";
				sb.append("<table id='tblelevideoLink_Profile' border='0' width='"+refDivWidth+"' class='table-bordered' onmouseover=\"javascript:setGridProfileVariable('document')\">");
				sb.append("<thead class='bg'>");
				sb.append("<tr>");
				
				responseText=PaginationAndSorting.responseSortingMLink_1(lbllDocName,sortOrderFieldName,"documentName",sortOrderTypeVal,pgNo);
				sb.append("<th align=\"left\" width='"+width1+"' "+tablePadding+" >"+responseText+"</th>");
	
				responseText=PaginationAndSorting.responseSortingMLink_1(lblDoc,sortOrderFieldName,"uploadedDocument",sortOrderTypeVal,pgNo);
				sb.append("<th align=\"left\" width='"+widt2+"' "+tablePadding+" >"+responseText+"</th>");
			
		//		responseText=PaginationAndSorting.responseSortingMLink_1("Document&nbsp;Name",sortOrderFieldName,"documentName",sortOrderTypeVal,pgNo);
		//		sb.append("<th align=\"left\" width='610' "+tablePadding+" >"+responseText+"</th>");
	
		//		responseText=PaginationAndSorting.responseSortingMLink_1("Document",sortOrderFieldName,"uploadedDocument",sortOrderTypeVal,pgNo);
		//		sb.append("<th align=\"left\" width='100' "+tablePadding+" >"+responseText+"</th>");
	
			}
			else if(visitLocation.equalsIgnoreCase("Teacher Pool"))
			{
				tablePadding_Data="";
				
				sb.append("<table border='0' id='additionalDocumentsGrid' class='table table-striped' onmouseover=\"javascript:setGridProfileVariable('document')\">");
				sb.append("<thead class='bg'>");			
				sb.append("<tr>");
	
				responseText=PaginationAndSorting.responseSortingMLink_2_TP(lbllDocName,sortOrderFieldName,"documentName",sortOrderTypeVal,pgNo);
				sb.append("<th>"+responseText+"</th>");
	
				responseText=PaginationAndSorting.responseSortingMLink_2_TP(lblDoc,sortOrderFieldName,"uploadedDocument",sortOrderTypeVal,pgNo);
				sb.append("<th>"+responseText+"</th>");
			}
			else
			{
				tablePadding_Data="";
				
				sb.append("<table border='0' id='additionalDocumentsGrid' class='table table-striped' onmouseover=\"javascript:setGridProfileVariable('document')\">");
				sb.append("<thead class='bg'>");			
				sb.append("<tr>");
	
				responseText=PaginationAndSorting.responseSortingMLink_1(lbllDocName,sortOrderFieldName,"documentName",sortOrderTypeVal,pgNo);
				sb.append("<th>"+responseText+"</th>");
	
				responseText=PaginationAndSorting.responseSortingMLink_1(lblDoc,sortOrderFieldName,"uploadedDocument",sortOrderTypeVal,pgNo);
				sb.append("<th>"+responseText+"</th>");
			}

			sb.append("</tr>");
			sb.append("</thead>");
			if(lstTeacherAdditionalDocuments!=null)
			{
				String windowFunc="if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
				
				
				for(TeacherAdditionalDocuments pojo:listsortedTeacherAddtionalDoc)
				{
					
					iRecordCount++;
					String gridColor="";
					if(iRecordCount%2==0 && ( visitLocation.equalsIgnoreCase("My Folders"))){
						gridColor="class='bggrid'";
					}
					
					sb.append("<tr "+gridColor+">");
					
					sb.append("<td "+tablePadding_Data+">");
					sb.append(pojo.getDocumentName());
					sb.append("</td>");
					
					sb.append("<td>");
					if(pojo.getUploadedDocument()!=null && !pojo.getUploadedDocument().equals("")){
						sb.append("<a href='javascript:void(0)' rel='tooltip' data-original-title='"+pojo.getUploadedDocument()+"'  id='tad"+pojo.getAdditionDocumentId()+"' onclick=\"downloadUploadedDocument('"+pojo.getAdditionDocumentId()+"','tad"+pojo.getAdditionDocumentId()+"','"+teacherId+"');"+windowFunc+"\"><span style='padding-left:20px;' class='icon-download-alt icon-large iconcolorBlue'></span></a>");
						sb.append("<script>$('#tad"+pojo.getAdditionDocumentId()+"').tooltip();</script>");
					}				
					sb.append("</td>");				
					sb.append("</tr>");
					
					
					sb.append("</tr>");
				}
				
				if(totalRecord>10 && ( visitLocation.equalsIgnoreCase("My Folders") || visitLocation.equalsIgnoreCase("Mosaic"))){
					sb.append("<tr><td colspan='2' style='border-top: 1px solid #cccccc;padding-bottom:0px;padding-top:5px;'>");
					sb.append(PaginationAndSorting.getPagination_pageSizeVariableName(request,totalRecord,noOfRow, pageNo,"pageSize15","730"));
					sb.append("</td></tr>");
				}
				
				
			}
			if(lstTeacherAdditionalDocuments==null || lstTeacherAdditionalDocuments.size()==0)
			{
				if(visitLocation.equalsIgnoreCase("My Folders") || visitLocation.equalsIgnoreCase("Mosaic"))
				{
					tablePadding_Data="style='padding-left:10px;vertical-align:top;text-align:left;font-weight: normal;line-height:30px;'";
					sb.append("<tr><td "+tablePadding_Data+" colspan='2'>"+lblNoRecord+".</td></tr>" );
				}
				else
				{
					sb.append("<tr>");
					sb.append("<td colspan='2'>");
					sb.append(lblNoRecord+".");
					sb.append("</td>");
					sb.append("</tr>");
				}
			}
			
			sb.append("</table>");
			
			if(!visitLocation.equalsIgnoreCase("My Folders") && !visitLocation.equalsIgnoreCase("Mosaic"))
			{
				//sb.append(PaginationAndSorting.getPaginationPaging(request,totalRecord,noOfRow, pageNo,"pageSize13"));
				sb.append(PaginationAndSorting.getPaginationPaging(request,totalRecord,noOfRow, pageNo,"pageSize13"));
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return sb.toString();
	}
	
	//District Assessment Details 
	public String getdistrictAssessmetGrid(String noOfRow, String pageNo,String sortOrder,String sortOrderType,Integer teacherId,String visitLocation)
	{	
		System.out.println(" getdistrictAssessmetGrid >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> "+visitLocation);

		String locale = Utility.getValueOfPropByKey("locale");
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		StringBuffer tmRecords = new StringBuffer();
		TeacherDetail teacherDetail = teacherDetailDAO.findById(teacherId, false, false);
		List<TeacherDetail> tList = new ArrayList<TeacherDetail>();
		tList.add(teacherDetail);
		List<ExaminationCodeDetails> examinationCodeDetails = new ArrayList<ExaminationCodeDetails>();
		Map<Integer,TeacherDistrictAssessmentStatus> assessmentStatus = new HashMap<Integer, TeacherDistrictAssessmentStatus>();
		List<TeacherDistrictAssessmentStatus> teacherAssessmentStatus = new ArrayList<TeacherDistrictAssessmentStatus>();
		teacherAssessmentStatus = teacherDistrictAssessmentStatusDAO.findAssessmentTakenByTeachers(tList);
 		for(TeacherDistrictAssessmentStatus tdaStatus : teacherAssessmentStatus)
 		{
 			assessmentStatus.put(tdaStatus.getTeacherDetail().getTeacherId(), tdaStatus);
 		}
 		UserMaster userMaster=null;
		DistrictMaster districtMaster=null;
		int roleId=0;
		boolean nobleflag=false;
 		
 		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		else
		{
			userMaster=(UserMaster)session.getAttribute("userMaster");
		     districtMaster=userMaster.getDistrictId();
			    if(districtMaster!=null)
			    	if(districtMaster.getDistrictId()==7800038)
			    		nobleflag=true;
			
			String sortOrderFieldName	=	"districtAssessmentName";
			String sortOrderNoField		=	"districtAssessmentName";
			int checkShortOrder=0;
			if(sortOrder.equals("") || sortOrder.equalsIgnoreCase("districtAssessmentName"))
				checkShortOrder=0;
			/*else if(sortOrder.equalsIgnoreCase("districtName")){ 
				checkShortOrder=1;
			}*/
			/*else if(sortOrder.equalsIgnoreCase("examinationCode")){ 
				checkShortOrder=2;
			}*/
			else if(sortOrder.equalsIgnoreCase("updatedDate")){
				checkShortOrder=3;
			}
			System.out.println("noofRow :: "+noOfRow+" pageNo :: "+sortOrder+" sortOrderType :: "+sortOrderType);

			Order  sortOrderStrVal=null;
			Boolean flag = false;
			if(sortOrder!=null){	
				flag = true;
				if(!sortOrder.equals("") && !sortOrder.equals(null)){
					sortOrderFieldName=sortOrder;
					sortOrderNoField=sortOrder;
				}
				if(sortOrder.equals("districtAssessmentName"))
				{
					sortOrderNoField="districtAssessmentName";
				}
				/*if(sortOrder.equals("districtName"))
				{
					sortOrderNoField="districtName";
				}*/
				/*if(sortOrder.equals("jobTitle"))
				{
					sortOrderNoField="jobTitle";
				}*/
				/*if(sortOrder.equals("examinationCode"))
				{
					sortOrderNoField="examinationCode";
				}*/
				if(sortOrder.equals("updatedDate"))
				{
					sortOrderNoField="updatedDate";
				}
			}
			
			
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("")){
				if(sortOrderType.equals("0")){
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}else{
				sortOrderTypeVal="0";
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}
			
			int noOfRowInPage = Integer.parseInt(noOfRow);
			int pgNo = Integer.parseInt(pageNo);
			int start = ((pgNo-1)*noOfRowInPage);
			int end = ((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord = 0;
			System.out.println("rows :: "+noOfRowInPage+" pgNo :: "+pgNo+" start :: "+start+" end :: "+end);	
			if(teacherDetail!=null)
			{						
					System.out.println(" Examination Code Details...");
					examinationCodeDetails = examinationCodeDetailsDAO.getExamCodeByTeacher(teacherDetail,checkShortOrder,sortOrderStrVal,start,end,flag);
					totalRecord = examinationCodeDetailsDAO.getTotalExamCodeByTeacher(teacherDetail,checkShortOrder,sortOrderStrVal,start,end);
			}
			if(totalRecord<end)
				end=totalRecord;
			
			String tablePadding_Data="";
			String responseText="";
			int iRecordCount=1;
			String refDivWidth="";
			String width1="";
			String widt2="";
			
			if(visitLocation.equalsIgnoreCase("My Folders") || visitLocation.equalsIgnoreCase("Mosaic"))
			{
				tablePadding_Data="style='padding-left:10px;vertical-align:top;text-align:left;font-weight: normal;line-height:30px;'";
				
				String tablePadding="style='padding-left:5px;vertical-align:top;color:#FFFFFF;line-height:25px;'";
				tmRecords.append("<table id='tblelevideoLink_Profile' border='0' width='"+refDivWidth+"' class='table-bordered' onmouseover=\"javascript:setGridProfileVariable('assessmentDetails')\">");
				tmRecords.append("<thead class='bg'>");
				tmRecords.append("<tr>");
				
		//		responseText=PaginationAndSorting.responseSortingMLink_1("Assessment&nbsp;Name",sortOrderFieldName,"districtAssessmentName",sortOrderTypeVal,pgNo);
		//		tmRecords.append("<th align=\"left\" width='230' "+tablePadding+" >"+responseText+"</th>");
				
				tmRecords.append("<th align=\"left\" width='230' "+tablePadding+" >"+Utility.getLocaleValuePropByKey("lblAssessmentName", locale)+"</th>");
	
				tmRecords.append("<th align=\"left\" width='200' "+tablePadding+" >"+Utility.getLocaleValuePropByKey("lblScore", locale)+"</th>");
				
			//	tmRecords.append("<th   valign='top'>Marks Obtained&nbsp;<a data-original-title='X/Y, X = Number of jobs applied at your District / Y = Number of Jobs applied at other District(s)' class='net-header-text' rel='tooltip' id='marksTool' href='javascript:void(0);'><span class='icon-question-sign'></span></a></th>");
			//	tmRecords.append("<script>$('#marksTool').tooltip();</script>");
				
//				responseText=PaginationAndSorting.responseSortingMLink_1("District&nbsp;Name",sortOrderFieldName,"districtName",sortOrderTypeVal,pgNo);
//				tmRecords.append("<th align=\"left\" width='"+widt2+"' "+tablePadding+" >"+responseText+"</th>");
			
//				responseText=PaginationAndSorting.responseSortingMLink_1("Assessment&nbsp;Code",sortOrderFieldName,"examinationCode",sortOrderTypeVal,pgNo);
//				tmRecords.append("<th align=\"left\" width='610' "+tablePadding+" >"+responseText+"</th>");
				
	//			responseText=PaginationAndSorting.responseSortingMLink_1("Invite&nbsp;Date",sortOrderFieldName,"createdDateTime",sortOrderTypeVal,pgNo);
	//			tmRecords.append("<th align=\"left\" width='100' "+tablePadding+" >"+responseText+"</th>");
				
				tmRecords.append("<th align=\"left\" width='100' "+tablePadding+" >"+Utility.getLocaleValuePropByKey("lblInviteDate", locale)+"</th>");
				
	//			responseText=PaginationAndSorting.responseSortingMLink_1("Date&nbsp;of&nbsp;Completion",sortOrderFieldName,"updatedDate",sortOrderTypeVal,pgNo);
	//			tmRecords.append("<th align=\"left\" width='200' "+tablePadding+" >"+responseText+"</th>");
				
				tmRecords.append("<th align=\"left\" width='200' "+tablePadding+" >"+Utility.getLocaleValuePropByKey("lblDateOfCompletion", locale)+"</th>");
				
	//			responseText=PaginationAndSorting.responseSortingMLink_1("Status",sortOrderFieldName,"status",sortOrderTypeVal,pgNo);
	//			tmRecords.append("<th align=\"left\" width='100' "+tablePadding+" >"+responseText+"</th>");
				
				tmRecords.append("<th align=\"left\" width='100' "+tablePadding+" >"+Utility.getLocaleValuePropByKey("lblStatus", locale)+"</th>");
	
			}
			else if(visitLocation.equalsIgnoreCase("Teacher Pool"))
			{
				tablePadding_Data="";
				
				tmRecords.append("<table border='0' id='assessmentDetails' class='table table-striped' onmouseover=\"javascript:setGridProfileVariable('assessmentDetails')\">");
				tmRecords.append("<thead class='bg'>");			
				tmRecords.append("<tr>");
	
	//			responseText=PaginationAndSorting.responseSortingMLink_2_TP("Assessment&nbsp;Name",sortOrderFieldName,"districtAssessmentName",sortOrderTypeVal,pgNo);
	//			tmRecords.append("<th width='230>"+responseText+"</th>");
				
				tmRecords.append("<th align=\"left\" width='230' >"+Utility.getLocaleValuePropByKey("lblAssessmentName", locale)+"</th>");
				
	//			tmRecords.append("<th width='230>Assessment&nbsp;Name</th>");
				
			//	responseText=PaginationAndSorting.responseSortingMLink_2_TP("Assessment&nbsp;Code",sortOrderFieldName,"examinationCode",sortOrderTypeVal,pgNo);
			//	tmRecords.append("<th>"+responseText+"</th>");
				
			//	tmRecords.append("<th   valign='top'>Marks Obtained&nbsp;<a data-original-title='X/Y, X = Number of jobs applied at your District / Y = Number of Jobs applied at other District(s)' class='net-header-text' rel='tooltip' id='marksTool' href='javascript:void(0);'><span class='icon-question-sign'></span></a></th>");
			//	tmRecords.append("<script>$('#marksTool').tooltip();</script>");
				tmRecords.append("<th   valign='top' width='200' >"+Utility.getLocaleValuePropByKey("lblScore", locale)+"</th>");
				
//				responseText=PaginationAndSorting.responseSortingMLink_1("Invite&nbsp;Date",sortOrderFieldName,"createdDateTime",sortOrderTypeVal,pgNo);
//				tmRecords.append("<th align=\"left\" width='100' >"+responseText+"</th>");

				tmRecords.append("<th align=\"left\" width='100' >"+Utility.getLocaleValuePropByKey("lblInviteDate", locale)+"</th>");
				
//				responseText=PaginationAndSorting.responseSortingMLink_2_TP("Date&nbsp;of&nbsp;Completion",sortOrderFieldName,"updatedDate",sortOrderTypeVal,pgNo);
//				tmRecords.append("<th valign='top' width='200'>"+responseText+"</th>");
				
				tmRecords.append("<th valign='top' width='200'>"+Utility.getLocaleValuePropByKey("lblDateOfCompletion", locale)+"</th>");
				
//				responseText=PaginationAndSorting.responseSortingMLink_2_TP("Status",sortOrderFieldName,"status",sortOrderTypeVal,pgNo);
//				tmRecords.append("<th valign='top' width='100'>"+responseText+"</th>");
				
				tmRecords.append("<th valign='top' width='100'>"+Utility.getLocaleValuePropByKey("lblStatus", locale)+"</th>");
				
			}
			else
			{
				tablePadding_Data="";
				
				tmRecords.append("<table border='0' id='assessmentDetails' class='table table-striped' onmouseover=\"javascript:setGridProfileVariable('assessmentDetails')\">");
				tmRecords.append("<thead class='bg'>");			
				tmRecords.append("<tr>");
	
//				responseText=PaginationAndSorting.responseSortingMLink_1("Assessment&nbsp;Name",sortOrderFieldName,"districtAssessmentName",sortOrderTypeVal,pgNo);
//				tmRecords.append("<th align=\"left\" width='230' >"+responseText+"</th>");
				
				tmRecords.append("<th align=\"left\" width='230' >"+Utility.getLocaleValuePropByKey("lblAssessmentName", locale)+"</th>");
	
				//responseText=PaginationAndSorting.responseSortingMLink_1("District&nbsp;Name",sortOrderFieldName,"districtName",sortOrderTypeVal,pgNo);
				//tmRecords.append("<th align=\"left\" width='"+widt2+"' >"+responseText+"</th>");
			
				//responseText=PaginationAndSorting.responseSortingMLink_1("Assessment&nbsp;Code",sortOrderFieldName,"examinationCode",sortOrderTypeVal,pgNo);
				//tmRecords.append("<th align=\"left\" width='610' >"+responseText+"</th>");
	
				tmRecords.append("<th   valign='top' width='200'>"+Utility.getLocaleValuePropByKey("lblScore", locale)+"</th>");
			//	tmRecords.append("<script>$('#marksTool').tooltip();</script>");
//				responseText=PaginationAndSorting.responseSortingMLink_1("Invite&nbsp;Date",sortOrderFieldName,"createdDateTime",sortOrderTypeVal,pgNo);
//				tmRecords.append("<th align=\"left\" width='100' >"+responseText+"</th>");
				
				tmRecords.append("<th align=\"left\" width='100' >"+Utility.getLocaleValuePropByKey("lblInviteDate", locale)+"</th>");
				
//				responseText=PaginationAndSorting.responseSortingMLink_1("Date&nbsp;of&nbsp;Completion",sortOrderFieldName,"updatedDate",sortOrderTypeVal,pgNo);
//				tmRecords.append("<th align=\"left\" width='200' >"+responseText+"</th>");
				
				tmRecords.append("<th align=\"left\" width='200' >"+Utility.getLocaleValuePropByKey("lblDateOfCompletion", locale)+"</th>");
				
//				responseText=PaginationAndSorting.responseSortingMLink_1("Status",sortOrderFieldName,"staus",sortOrderTypeVal,pgNo);
//				tmRecords.append("<th align=\"left\" width='100' >"+responseText+"</th>");
				
				tmRecords.append("<th align=\"left\" width='100' >"+Utility.getLocaleValuePropByKey("lblStatus", locale)+"</th>");
			}
			
			tmRecords.append("</tr>");
			tmRecords.append("</thead>");
			
			
			int counter=0 ;
			if(examinationCodeDetails!=null && examinationCodeDetails.size()>0)
			{
				String startTime="";
				String endTime="";
				for(ExaminationCodeDetails examDetails : examinationCodeDetails)
				{
					String comDate ="";
					String displayStatus = Utility.getLocaleValuePropByKey("lblNotCompleted", locale);
					
					TeacherDistrictAssessmentStatus status = assessmentStatus.get(examDetails.getTeacherDetail().getTeacherId());
					counter++;
					String gridColor="class='bggrid'";
					if(counter%2==0){
						gridColor="style='background-color:white;'";
					}
					
					int obtMarks = 0;
					int totMarks = 0;
					float marksPercentage=0;
					
					if(status.getMarksObtained()!=null && status.getMarksObtained()>0)
						obtMarks = status.getMarksObtained().intValue();
					
					if(status.getTotalMarks()!=null && status.getTotalMarks()>0)
						totMarks = status.getTotalMarks().intValue();
					
					
					if(obtMarks>0 && totMarks>0)
					{
						marksPercentage = (float)((float)(obtMarks*100)/totMarks);
					}
					
					tmRecords.append("<tr "+gridColor+">");
					tmRecords.append("<td><span style='font-weight: normal'>"+examDetails.getDistrictAssessmentDetail().getDistrictAssessmentName()+"</span></td>");
					tmRecords.append("<td><span style='font-weight: normal'>"+obtMarks+"/"+totMarks+"&nbsp;("+marksPercentage+"%)</span></td>");
					tmRecords.append("<td><span style='font-weight: normal'>"+Utility.convertDateAndTimeToUSformatOnlyDate(examDetails.getCreatedDateTime())+"</span></td>");
					
					//	tmRecords.append("<td>"+examDetails.getDistrictAssessmentDetail().getDistrictMaster().getDistrictName()+"</td>");
					//tmRecords.append("<td>"+examDetails.getJobOrder().getJobTitle()+"</td>");
				//	tmRecords.append("<td>"+examDetails.getExaminationCode()+"</td>");
					
					if(examDetails.getInviteStatus().equalsIgnoreCase("C"))
					{
					//	startTime = examDetails.getExamStartTime();
					//	endTime = examDetails.getExamEndTime();
					//	tmRecords.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(examDetails.getExamDate())+"<br/>"+startTime+" to "+endTime+"</td>");
						System.out.println(" status "+status);
						
						if(status!=null)
						{
							if(status.getStatusMaster().getStatusShortName().equalsIgnoreCase("comp"))
							{
								System.out.println("02");
								
								displayStatus=status.getStatusMaster().getStatus();
								comDate = Utility.convertDateAndTimeToUSformatOnlyDate(status.getUpdatedDate());
								//tmRecords.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(status.getUpdatedDate())+"</td>");
								//tmRecords.append("<td>Completed</td>");
							}	
							else if(status.getStatusMaster().getStatusShortName().equalsIgnoreCase("icomp"))
								displayStatus=status.getStatusMaster().getStatus();
							else if(status.getStatusMaster().getStatusShortName().equalsIgnoreCase("vlt"))
								displayStatus=status.getStatusMaster().getStatus();
							else if(status.getStatusMaster().getStatusShortName().equalsIgnoreCase("comp"))
								displayStatus=status.getStatusMaster().getStatus();
							
							
							
						}
						else
						{
							displayStatus=Utility.getLocaleValuePropByKey("lblConfirmed", locale);//tmRecords.append("<td><a href='javascript:void(0);' id='Exam"+counter+"' rel='tooltip' data-original-title='Start Assessment'>Start Assessment</a></td>");
						}
						//tmRecords.append("<td><a href='javascript:void(0);' id='Exam"+counter+"' rel='tooltip' data-original-title='Start Exam'>Start Exam</a></td>");
						//tmRecords.append("<script>$('#Exam"+counter+"').tooltip();</script>");
					}
					else
					{
						displayStatus=Utility.getLocaleValuePropByKey("lblInvited", locale);
						
						//tmRecords.append("<td> N/A </td>");
					//	tmRecords.append("<td><a href='javascript:void(0);' id='Slot"+counter+"' rel='tooltip' data-original-title='Select Slot'>Select Slot</a></td>");
					//	tmRecords.append("<script>$('#Slot"+counter+"').tooltip();</script>");
					}
					//tmRecords.append("&nbsp;&nbsp;&nbsp;&nbsp;<a href='javascript:void(0)' id='docEditToolTip"+counter+"' rel='tooltip' data-original-title='Edit Document' onclick=\"editJobDoc("+jbDoc.getJobDocsId()+","+jbDoc.getDistrictMaster().getDistrictId()+")\"><img src='images/option02.png' style='width:21px; height:21px;'></a>");
					//tmRecords.append("<script>$('#docEditToolTip"+counter+"').tooltip();</script>");
					/*tmRecords.append("&nbsp;&nbsp;&nbsp;&nbsp;<a href='javascript:void(0);' id='removeDocTooTip"+counter+"' rel='tooltip' data-original-title='Remove Document' onclick=\"openRemoveDocModal("+jbDoc.getJobDocsId()+")\"><img src='images/option03.png' style='width:21px; height:21px;'></a></td>");
					tmRecords.append("<script>$('#removeDocTooTip"+counter+"').tooltip();</script>");*/
					tmRecords.append("<td><span style='font-weight: normal'>"+comDate+"</span></td>");
					tmRecords.append("<td><span style='font-weight: normal'>"+displayStatus+"</span></td>");
					
					
					tmRecords.append("</tr>");
				}
			}
			else
			{
				tmRecords.append("<tr><td colspan='6'><span style='font-weight: normal'>"+Utility.getLocaleValuePropByKey("lblNoRecord", locale)+"</span></td></tr>" );
			}
			tmRecords.append("</table>");
			tmRecords.append(PaginationAndSorting.getPaginationPaging(request,totalRecord,noOfRow, pageNo,"pageSize15"));
			
		}
		
		return tmRecords.toString();
	}
	
	public String showAssessmentDetails(Integer teacherId,String noOfRow, String pageNo,String sortOrder,String sortOrderType)
	{
		System.out.println("================== showAssessmentDetails Grid ==========");
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		StringBuffer tmRecords = new StringBuffer();
		TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
		List<TeacherDetail> tList = new ArrayList<TeacherDetail>();
		tList.add(teacherDetail);
		List<ExaminationCodeDetails> examinationCodeDetails = new ArrayList<ExaminationCodeDetails>();
		Map<Integer,TeacherDistrictAssessmentStatus> assessmentStatus = new HashMap<Integer, TeacherDistrictAssessmentStatus>();
		List<TeacherDistrictAssessmentStatus> teacherAssessmentStatus = new ArrayList<TeacherDistrictAssessmentStatus>();
		teacherAssessmentStatus = teacherDistrictAssessmentStatusDAO.findAssessmentTakenByTeachers(tList);
 		for(TeacherDistrictAssessmentStatus tdaStatus : teacherAssessmentStatus)
 		{
 			assessmentStatus.put(tdaStatus.getTeacherDetail().getTeacherId(), tdaStatus);
 		}
		
		if(session == null || session.getAttribute("teacherDetail")==null) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		else
		{
			String sortOrderFieldName	=	"districtAssessmentName";
			String sortOrderNoField		=	"districtAssessmentName";
			int checkShortOrder=0;
			if(sortOrder.equals("") || sortOrder.equalsIgnoreCase("districtAssessmentName"))
				checkShortOrder=0;
			/*else if(sortOrder.equalsIgnoreCase("districtName")){ 
				checkShortOrder=1;
			}*/
			else if(sortOrder.equalsIgnoreCase("examinationCode")){ 
				checkShortOrder=2;
			}
			else if(sortOrder.equalsIgnoreCase("examDate")){
				checkShortOrder=3;
			}
			System.out.println("noofRow :: "+noOfRow+" pageNo :: "+sortOrder+" sortOrderType :: "+sortOrderType);

			Order  sortOrderStrVal=null;
			Boolean flag = false;
			if(sortOrder!=null){	
				flag = true;
				if(!sortOrder.equals("") && !sortOrder.equals(null)){
					sortOrderFieldName=sortOrder;
					sortOrderNoField=sortOrder;
				}
				if(sortOrder.equals("districtAssessmentName"))
				{
					sortOrderNoField="districtAssessmentName";
				}
				if(sortOrder.equals("districtName"))
				{
					sortOrderNoField="districtName";
				}
				/*if(sortOrder.equals("jobTitle"))
				{
					sortOrderNoField="jobTitle";
				}*/
				if(sortOrder.equals("examinationCode"))
				{
					sortOrderNoField="examinationCode";
				}
				if(sortOrder.equals("examDate"))
				{
					sortOrderNoField="examDate";
				}
			}
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("")){
				if(sortOrderType.equals("0")){
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}else{
				sortOrderTypeVal="0";
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}
			int noOfRowInPage = Integer.parseInt(noOfRow);
			int pgNo = Integer.parseInt(pageNo);
			int start = ((pgNo-1)*noOfRowInPage);
			int end = ((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord = 0;
			System.out.println("rows :: "+noOfRowInPage+" pgNo :: "+pgNo+" start :: "+start+" end :: "+end);	
			if(teacherDetail!=null)
			{						
					System.out.println(" Examination Code Details...");
					examinationCodeDetails = examinationCodeDetailsDAO.getExamCodeByTeacher(teacherDetail,checkShortOrder,sortOrderStrVal,start,end,flag);
					totalRecord = examinationCodeDetailsDAO.getTotalExamCodeByTeacher(teacherDetail,checkShortOrder,sortOrderStrVal,start,end);
			}
			if(totalRecord<end)
				end=totalRecord;

			tmRecords.append("<table id='examDetailsTable' width='100%' class='tablecgtbl'>");
			tmRecords.append("<thead class='bg'>");
			tmRecords.append("<tr>");
			String responseText="";
            responseText=PaginationAndSorting.responseSortingLink(lblAssessmentName,sortOrderFieldName,"districtAssessmentName",sortOrderTypeVal,pgNo);		
			tmRecords.append("<th   valign='top'>"+responseText+"</th>");
			responseText=PaginationAndSorting.responseSortingLink(lblDistrictName,sortOrderFieldName,"districtName",sortOrderTypeVal,pgNo);		
			tmRecords.append("<th   valign='top'>"+responseText+"</th>");
			/*responseText=PaginationAndSorting.responseSortingLink("Job Title",sortOrderFieldName,"jobTitle",sortOrderTypeVal,pgNo);
			tmRecords.append("<th   valign='top'>"+responseText+"</th>");*/
			responseText=PaginationAndSorting.responseSortingLink(lblAssessmentCode,sortOrderFieldName,"examinationCode",sortOrderTypeVal,pgNo);
			tmRecords.append("<th   valign='top'>"+responseText+"</th>");
			tmRecords.append("<th   valign='top'>"+msgMarksObtained+"&nbsp;<a data-original-title='X/Y, X = Number of jobs applied at your District / Y = Number of Jobs applied at other District(s)' class='net-header-text' rel='tooltip' id='marksTool' href='javascript:void(0);'><span class='icon-question-sign'></span></a></th>");
			tmRecords.append("<script>$('#marksTool').tooltip();</script>");
			responseText=PaginationAndSorting.responseSortingLink(lblAssDTime,sortOrderFieldName,"examDate",sortOrderTypeVal,pgNo);
			tmRecords.append("<th   valign='top'>"+responseText+"</th>");
			tmRecords.append("<th  valign='top'>"+lblAct+"</th>");
			tmRecords.append("</tr>");
			tmRecords.append("</thead>");
			int counter=0 ;
			if(examinationCodeDetails!=null && examinationCodeDetails.size()>0)
			{
				String startTime="";
				String endTime="";
				for(ExaminationCodeDetails examDetails : examinationCodeDetails)
				{
					counter++;
					String gridColor="class='bggrid'";
					if(counter%2==0){
						gridColor="style='background-color:white;'";
					}
					tmRecords.append("<tr "+gridColor+">");
					tmRecords.append("<td>"+examDetails.getDistrictAssessmentDetail().getDistrictAssessmentName()+"</td>");
					tmRecords.append("<td>"+examDetails.getDistrictAssessmentDetail().getDistrictMaster().getDistrictName()+"</td>");
					//tmRecords.append("<td>"+examDetails.getJobOrder().getJobTitle()+"</td>");
					tmRecords.append("<td>"+examDetails.getExaminationCode()+"</td>");
					tmRecords.append("<td>150</td>");
					if(examDetails.getInviteStatus().equalsIgnoreCase("C"))
					{
						startTime = examDetails.getExamStartTime();
						endTime = examDetails.getExamEndTime();
						tmRecords.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(examDetails.getExamDate())+"<br/>"+startTime+" to "+endTime+"</td>");
						TeacherDistrictAssessmentStatus status = assessmentStatus.get(examDetails.getTeacherDetail().getTeacherId());
						if(status!=null)
						{
							if(status.getStatusMaster().getStatusShortName().equalsIgnoreCase("icomp"))
								tmRecords.append("<td><a href='javascript:void(0);' id='Exam"+counter+"' rel='tooltip' data-original-title='"+lblStartAssessment1+"'>"+lblStartAssessment1+"</a></td>");
							else if(status.getStatusMaster().getStatusShortName().equalsIgnoreCase("vlt"))
								tmRecords.append("<td>Timed Out</a></td>");
							else if(status.getStatusMaster().getStatusShortName().equalsIgnoreCase("comp"))
								tmRecords.append("<td>Completed</a></td>");
						}
						else
						{
							tmRecords.append("<td><a href='javascript:void(0);' id='Exam"+counter+"' rel='tooltip' data-original-title='"+lblStartAssessment1+"'>"+lblStartAssessment1+"</a></td>");
						}
						//tmRecords.append("<td><a href='javascript:void(0);' id='Exam"+counter+"' rel='tooltip' data-original-title='Start Exam'>Start Exam</a></td>");
						tmRecords.append("<script>$('#Exam"+counter+"').tooltip();</script>");
					}
					else
					{
						tmRecords.append("<td> N/A </td>");
						tmRecords.append("<td><a href='javascript:void(0);' id='Slot"+counter+"' rel='tooltip' data-original-title='"+lblSelectSlot1+"'>"+lblSelectSlot1+"</a></td>");
						tmRecords.append("<script>$('#Slot"+counter+"').tooltip();</script>");
					}
					//tmRecords.append("&nbsp;&nbsp;&nbsp;&nbsp;<a href='javascript:void(0)' id='docEditToolTip"+counter+"' rel='tooltip' data-original-title='Edit Document' onclick=\"editJobDoc("+jbDoc.getJobDocsId()+","+jbDoc.getDistrictMaster().getDistrictId()+")\"><img src='images/option02.png' style='width:21px; height:21px;'></a>");
					//tmRecords.append("<script>$('#docEditToolTip"+counter+"').tooltip();</script>");
					/*tmRecords.append("&nbsp;&nbsp;&nbsp;&nbsp;<a href='javascript:void(0);' id='removeDocTooTip"+counter+"' rel='tooltip' data-original-title='Remove Document' onclick=\"openRemoveDocModal("+jbDoc.getJobDocsId()+")\"><img src='images/option03.png' style='width:21px; height:21px;'></a></td>");
					tmRecords.append("<script>$('#removeDocTooTip"+counter+"').tooltip();</script>");*/
					tmRecords.append("</tr>");
				}
			}
			else
			{
				tmRecords.append("<tr><td colspan='6'>"+lblNoRecord+"</td></tr>" );
			}
			tmRecords.append("</table>");
			tmRecords.append(PaginationAndSorting.getPaginationStringForTeacherInfoAjax(request,totalRecord,noOfRow, pageNo));
		}
		return tmRecords.toString();
	}
	
	
	//@Transactional(readOnly=true)
	public String getGridDataForTeacherProfileVisitHistoryByTeacher(Integer teacherId, String noOfRow, String pageNo,String sortOrder,String sortOrderType,String visitLocation)
	{		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		DistrictMaster districtMaster=null;
		SchoolMaster schoolMaster=null;
		UserMaster userMaster=null;
		int entityTypeId=1;
		int entityID=0,roleId=0;
		if (session == null || session.getAttribute("userMaster") == null) {
			//return "false";
		}else{
			userMaster=(UserMaster)session.getAttribute("userMaster");
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
			if(userMaster.getDistrictId()!=null){
				districtMaster=userMaster.getDistrictId();
			}
			if(userMaster.getSchoolId()!=null){
				schoolMaster =userMaster.getSchoolId();
			}	
			entityID=userMaster.getEntityType();
		}
		
		StringBuffer sb = new StringBuffer();		
		try 
		{
			int noOfRowInPage 	= Integer.parseInt(noOfRow);
			int pgNo 			= Integer.parseInt(pageNo);
			int start 			= ((pgNo-1)*noOfRowInPage);
			int end 			= ((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord 	= 0;
			//------------------------------------

			TeacherDetail teacherDetail = teacherDetailDAO.findById(teacherId, false, false);

			List<TeacherProfileVisitHistory> listTeacherProfileVisitHistory= null;
			/** set default sorting fieldName **/
			String sortOrderFieldName	=	"visitedDateTime";
			String sortOrderNoField		=	"visitedDateTime";

			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal=null;

			if(sortOrder!=null){
				if(!sortOrder.equals("") && !sortOrder.equals(null) && !sortOrder.equals("userName")){
					sortOrderFieldName=sortOrder;
					sortOrderNoField=sortOrder;
				}
				if(sortOrder.equals("userName")){
					sortOrderNoField="userName";
				}
			}
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
				if(sortOrderType.equals("0")){
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}else{
				sortOrderTypeVal="0";
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}
			/**End ------------------------------------**/
			
			Criterion criterion = Restrictions.eq("teacherDetail",teacherDetail);
			
			if(entityID==1){
				listTeacherProfileVisitHistory = teacherProfileVisitHistoryDAO.findByCriteria(criterion);
				totalRecord=listTeacherProfileVisitHistory.size();
				listTeacherProfileVisitHistory= null;
				listTeacherProfileVisitHistory = teacherProfileVisitHistoryDAO.findWithLimit(sortOrderStrVal, start, noOfRowInPage, criterion);
			}
			else
			{
				Criterion criterion_districtId = Restrictions.eq("districtId",districtMaster);
				listTeacherProfileVisitHistory = teacherProfileVisitHistoryDAO.findByCriteria(criterion,criterion_districtId);
				totalRecord=listTeacherProfileVisitHistory.size();
				listTeacherProfileVisitHistory= null;
				listTeacherProfileVisitHistory = teacherProfileVisitHistoryDAO.findWithLimit(sortOrderStrVal, start, noOfRowInPage, criterion,criterion_districtId);
			}
			/*else if(entityID==3){
				Criterion criterion_districtId = Restrictions.eq("districtId",districtMaster);
				Criterion criterion_schoolId = Restrictions.eq("schoolId",schoolMaster);
				listTeacherProfileVisitHistory = teacherProfileVisitHistoryDAO.findByCriteria(criterion,criterion_schoolId,criterion_districtId);
				totalRecord=listTeacherProfileVisitHistory.size();
				listTeacherProfileVisitHistory= null;
				listTeacherProfileVisitHistory = teacherProfileVisitHistoryDAO.findWithLimit(sortOrderStrVal, start, noOfRowInPage, criterion,criterion_schoolId,criterion_districtId);
			}*/
			

			List<TeacherProfileVisitHistory> sortedTeacherProfileVisitHistory		=	new ArrayList<TeacherProfileVisitHistory>();

			SortedMap<String,TeacherProfileVisitHistory>	sortedMap = new TreeMap<String,TeacherProfileVisitHistory>();
			if(sortOrderNoField.equals("userName"))
			{
				sortOrderFieldName	=	"userName";
			}
			
			int mapFlag=2;
			for (TeacherProfileVisitHistory pojo : listTeacherProfileVisitHistory){
				String orderFieldName=""+pojo.getVisitId();
				if(sortOrderFieldName.equals("userName")){
					orderFieldName=pojo.getUserMaster().getFirstName()+" "+pojo.getUserMaster().getLastName()+"||"+pojo.getVisitId();
					sortedMap.put(orderFieldName+"||",pojo);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}
			}
			if(mapFlag==1){
				NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet();  
				for (Iterator iter=navig.iterator();iter.hasNext();) {  
					Object key = iter.next(); 
					sortedTeacherProfileVisitHistory.add((TeacherProfileVisitHistory) sortedMap.get(key));
				} 
			}else if(mapFlag==0){
				Iterator iterator = sortedMap.keySet().iterator();
				while (iterator.hasNext()) {
					Object key = iterator.next();
					sortedTeacherProfileVisitHistory.add((TeacherProfileVisitHistory) sortedMap.get(key));
				}
			}else{
				sortedTeacherProfileVisitHistory=listTeacherProfileVisitHistory;
			}

			if(totalRecord<end)
				end=totalRecord;

			String responseText="";
			String tablePadding_Data="";
			int iRecordCount=1;
			
			if(visitLocation.equalsIgnoreCase("My Folders") || visitLocation.equalsIgnoreCase("Mosaic"))
			{ 
				tablePadding_Data="style='padding-left:10px;vertical-align:top;text-align:left;font-weight: normal;line-height:30px;'";
				
				String tablePadding="style='padding-left:5px;vertical-align:top;color:#FFFFFF;line-height:33px;'";
				sb.append("<table id='tblProfileVisitHistory_Profile' border='0'  width='570' class='table-bordered' onmouseover=\"javascript:setGridProfileVariable('tpvh')\">");
				sb.append("<thead class='bg'>");
				sb.append("<tr>");
				
				responseText=PaginationAndSorting.responseSortingMLink_1(lblUserName,sortOrderFieldName,"userName",sortOrderTypeVal,pgNo);
				sb.append("<th align=\"left\" width='460' "+tablePadding+" >"+responseText+"</th>");
	
				responseText=PaginationAndSorting.responseSortingMLink_1(lblVisitedDate1,sortOrderFieldName,"visitedDateTime",sortOrderTypeVal,pgNo);
				sb.append("<th align=\"left\" width='110' "+tablePadding+" >"+responseText+"</th>");
				
				
			}
			else
			{
			
				tablePadding_Data="";
				
				sb.append("<table border='0' id='tblProfileVisitHistory_Profile' class='table table-striped' onmouseover=\"javascript:setGridProfileVariable('tpvh')\">");
				sb.append("<thead class='bg'>");			
				sb.append("<tr>");
	
				responseText=PaginationAndSorting.responseSortingMLink_1(lblUserName,sortOrderFieldName,"userName",sortOrderTypeVal,pgNo);
				sb.append("<th>"+responseText+"</th>");
	
				responseText=PaginationAndSorting.responseSortingMLink_1(lblVisitedDate1,sortOrderFieldName,"visitedDateTime",sortOrderTypeVal,pgNo);
				sb.append("<th>"+responseText+"</th>");
				
			}

			sb.append("</tr>");
			sb.append("</thead>");
			if(listTeacherProfileVisitHistory!=null)
			{
				for(TeacherProfileVisitHistory pojo:sortedTeacherProfileVisitHistory)
				{
					iRecordCount++;
					String gridColor="";
					if(iRecordCount%2==0 && ( visitLocation.equalsIgnoreCase("My Folders") || visitLocation.equalsIgnoreCase("Mosaic"))){
						gridColor="class='bggrid'";
					}
					
					String Name=pojo.getUserMaster().getFirstName()+" "+pojo.getUserMaster().getLastName();
					
					
					sb.append("<tr "+gridColor+">");
					
					sb.append("<td "+tablePadding_Data+">");
					sb.append(Name);
					sb.append("</td>");

					sb.append("<td "+tablePadding_Data+">");
					sb.append(Utility.convertDateAndTimeToUSformatOnlyDate(pojo.getVisitedDateTime()));
					sb.append("</td>");

					
					sb.append("</tr>");
				}
				
				if(totalRecord>10 && ( visitLocation.equalsIgnoreCase("My Folders") || visitLocation.equalsIgnoreCase("Mosaic"))){
					//sb.append("<tr><td colspan='2' style='border-top: 1px solid #cccccc;padding-bottom:0px;padding-top:5px;'>");
					sb.append("</table>");
					sb.append(PaginationAndSorting.getPagination_pageSizeVariableName(request,totalRecord,noOfRow, pageNo,"pageSize14","565"));
					//sb.append("</td></tr>");
				}
				
			}

			if(listTeacherProfileVisitHistory==null || listTeacherProfileVisitHistory.size()==0)
			{
				
				if(visitLocation.equalsIgnoreCase("My Folders") || visitLocation.equalsIgnoreCase("Mosaic"))
				{
					tablePadding_Data="style='padding-left:10px;vertical-align:top;text-align:left;font-weight: normal;line-height:30px;'";
					sb.append("<tr><td "+tablePadding_Data+" colspan='2'>"+lblNoRecord+"</td></tr>" );
				}
				else
				{
					sb.append("<tr>");
					sb.append("<td colspan='2'>");
					sb.append(lblNoRecord);
					sb.append("</td>");
					sb.append("</tr>");
				}
				
			}
			
			sb.append("</table>");
			if(!visitLocation.equalsIgnoreCase("My Folders") && !visitLocation.equalsIgnoreCase("Mosaic")){
				sb.append(PaginationAndSorting.getPagination_pageSizeVariableName(request,totalRecord,noOfRow, pageNo,"pageSize14","565"));
			}

		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return sb.toString();	
	}
	
	
	
	// Teacher Academics
	//@Transactional(readOnly=true)
	public String getTeacherAcademicsGridCommon(Integer teacherId, String noOfRow, String pageNo,String sortOrder,String sortOrderType,String visitLocation)
	{		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster=null;
		DistrictMaster districtMaster=null;
		int roleId=0;
		boolean nobleflag=false;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}else{
		    userMaster=(UserMaster)session.getAttribute("userMaster");
		    districtMaster=userMaster.getDistrictId();
		    if(districtMaster!=null)
		    	if(districtMaster.getDistrictId()==7800038)
		    		nobleflag=true;
		    
		 }
		
		if(userMaster.getRoleId().getRoleId()!=null)
			roleId=userMaster.getRoleId().getRoleId();

		StringBuffer sb = new StringBuffer();		
		try 
		{
			String locale = Utility.getValueOfPropByKey("locale");
			String lblNoRecord = Utility.getLocaleValuePropByKey("lblNoRecord", locale);
			String lblSchool = Utility.getLocaleValuePropByKey("lblSchool", locale);
			String lblDatesAttended  = Utility.getLocaleValuePropByKey("lblDatesAttended", locale);
			String lblDegree  = Utility.getLocaleValuePropByKey("lblDegree", locale);
			String lblFieldOfStudy  = Utility.getLocaleValuePropByKey("lblFieldOfStudy", locale);
			String lblTranscript  = Utility.getLocaleValuePropByKey("lblTranscript", locale);
			String lblGPA  = Utility.getLocaleValuePropByKey("lblGPA", locale);
			String lblFreshman = Utility.getLocaleValuePropByKey("lblFreshman", locale);
			String lblSophomore  = Utility.getLocaleValuePropByKey("lblSophomore", locale);
			String lblJunior  = Utility.getLocaleValuePropByKey("lblJunior", locale);
			String lblSenior  = Utility.getLocaleValuePropByKey("lblSenior", locale);
			String lblCumulative  = Utility.getLocaleValuePropByKey("lblCumulative", locale);
			String lblEducation  = Utility.getLocaleValuePropByKey("lblEducation", locale);
			
			int noOfRowInPage 	= Integer.parseInt(noOfRow);
			int pgNo 			= Integer.parseInt(pageNo);
			int start 			= ((pgNo-1)*noOfRowInPage);
			int end 			= ((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord 	= 0;
			//------------------------------------

			TeacherDetail teacherDetail = teacherDetailDAO.findById(teacherId, false, false);

			//List<TeacherRole> listTeacherRole= null;
			/** set default sorting fieldName **/
			String sortOrderFieldName	=	"attendedInYear";
			String sortOrderNoField		=	"attendedInYear";

			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal=null;

			if(sortOrder!=null){
				if(!sortOrder.equals("") && !sortOrder.equals(null) && !sortOrder.equals("Education") && !sortOrder.equals("fieldOfStudy")){
					sortOrderFieldName=sortOrder;
					sortOrderNoField=sortOrder;
				}
				if(sortOrder.equals("Education")){
					sortOrderNoField="Education";
				}
				if(sortOrder.equals("fieldOfStudy")){
					sortOrderNoField="fieldOfStudy";
				}
			}

			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
				if(sortOrderType.equals("0")){
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}else{
				sortOrderTypeVal="0";
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}
			/**End ------------------------------------**/

			Criterion criterion = Restrictions.eq("teacherId",teacherDetail);
			List<TeacherAcademics>  lstTeacherAcademics = teacherAcademicsDAO.findByCriteria(sortOrderStrVal,criterion);
			
			List<TeacherAcademics> sortedTeacherAcademics		=	new ArrayList<TeacherAcademics>();

			SortedMap<String,TeacherAcademics>	sortedMap = new TreeMap<String,TeacherAcademics>();
			if(sortOrderNoField.equals("Education"))
			{
				sortOrderFieldName	=	"Education";
			}
			if(sortOrderNoField.equals("fieldOfStudy"))
			{
				sortOrderFieldName	=	"fieldOfStudy";
			}
			int mapFlag=2;
			for (TeacherAcademics pojo : lstTeacherAcademics){
				String orderFieldName=""+pojo.getAcademicId();
				if(sortOrderFieldName.equals("Education")){
					orderFieldName=pojo.getDegreeId().getDegreeShortName()+""+pojo.getUniversityId().getUniversityName()+"||"+pojo.getAcademicId();
					sortedMap.put(orderFieldName+"||",pojo);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}
				if(sortOrderFieldName.equals("fieldOfStudy")){
					orderFieldName=pojo.getFieldId().getFieldName()+"||"+pojo.getAcademicId();
					sortedMap.put(orderFieldName+"||",pojo);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}
			}
			if(mapFlag==1){
				NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet();  
				for (Iterator iter=navig.iterator();iter.hasNext();) {  
					Object key = iter.next(); 
					sortedTeacherAcademics.add((TeacherAcademics) sortedMap.get(key));
				} 
			}else if(mapFlag==0){
				Iterator iterator = sortedMap.keySet().iterator();
				while (iterator.hasNext()) {
					Object key = iterator.next();
					sortedTeacherAcademics.add((TeacherAcademics) sortedMap.get(key));
				}
			}else{
				sortedTeacherAcademics=lstTeacherAcademics;
			}

			totalRecord =lstTeacherAcademics.size();

			if(totalRecord<end)
				end=totalRecord;
			List<TeacherAcademics> listsortedTeacherAcademics		=	sortedTeacherAcademics.subList(start,end);
			
			String responseText="";
			String tablePadding_Data="";
			String tablePadding_Data_right="";
			int iRecordCount=1;
			String refDivWidth="";
			if(userMaster.getEntityType().equals(2) || userMaster.getEntityType().equals(3))
			  refDivWidth="830";
			else
			  refDivWidth="730";			
			if(visitLocation.equalsIgnoreCase("My Folders") || visitLocation.equalsIgnoreCase("Mosaic"))
			{
				tablePadding_Data="style='padding-left:10px;vertical-align:top;text-align:left;font-weight: normal;line-height:30px;'";
				tablePadding_Data_right="style='padding-right:10px;vertical-align:top;text-align:right;font-weight: normal;line-height:30px;'";
				
				String tablePadding="style='padding-left:5px;vertical-align:top;color:#FFFFFF;line-height:25px;'";
				sb.append("<table id='tblTeacherAcademics_Profile' border='0'  width='"+refDivWidth+"' class='table-bordered' onmouseover=\"javascript:setGridProfileVariable('teacherAce')\">");
				sb.append("<thead class='bg'>");
				sb.append("<tr>");
				
				responseText=PaginationAndSorting.responseSortingMLink_1(lblEducation,sortOrderFieldName,"Education",sortOrderTypeVal,pgNo);
				sb.append("<th align=\"left\" width='200' "+tablePadding+" >"+responseText+"</th>");
	
				responseText=PaginationAndSorting.responseSortingMLink_1(lblDatesAttended,sortOrderFieldName,"attendedInYear",sortOrderTypeVal,pgNo);
				sb.append("<th align=\"left\" width='50' "+tablePadding+" >"+responseText+"</th>");
				
				sb.append("<th width='100' class='net-header-text' "+tablePadding+">");
				sb.append("GPA");
				sb.append("</th>");
				
				responseText=PaginationAndSorting.responseSortingMLink_1(lblFieldOfStudy,sortOrderFieldName,"fieldOfStudy",sortOrderTypeVal,pgNo);
				sb.append("<th align=\"left\" width='150' "+tablePadding+" >"+responseText+"</th>");
				
				if(!nobleflag){
					responseText=PaginationAndSorting.responseSortingMLink_1(lblTranscript,sortOrderFieldName,"pathOfTranscript",sortOrderTypeVal,pgNo);
					sb.append("<th align=\"left\" width='100' "+tablePadding+" >"+responseText+"</th>");
				}
			}
			else if(visitLocation.equalsIgnoreCase("Teacher Pool"))
			{
				tablePadding_Data="";
				tablePadding_Data_right="style='padding-right:115px;text-align:right;'";
				
				sb.append("<table border='0' id='tblTeacherAcademics_Profile' class='table table-striped' onmouseover=\"javascript:setGridProfileVariable('teacherAce')\">");
				sb.append("<thead class='bg'>");			
				sb.append("<tr>");
	
				responseText=PaginationAndSorting.responseSortingMLink_2_TP(lblEducation,sortOrderFieldName,"Education",sortOrderTypeVal,pgNo);
				sb.append("<th>"+responseText+"</th>");
	
				responseText=PaginationAndSorting.responseSortingMLink_2_TP(lblDatesAttended,sortOrderFieldName,"attendedInYear",sortOrderTypeVal,pgNo);
				sb.append("<th>"+responseText+"</th>");
				
				sb.append("<th>");
				sb.append(lblGPA);
				sb.append("</th>");
				
				if(!nobleflag){
				responseText=PaginationAndSorting.responseSortingMLink_1(lblFieldOfStudy,sortOrderFieldName,"fieldOfStudy",sortOrderTypeVal,pgNo);
				sb.append("<th>"+responseText+"</th>");
				}else{
				responseText=PaginationAndSorting.responseSortingMLink_1(lblFieldOfStudy,sortOrderFieldName,"fieldOfStudy",sortOrderTypeVal,pgNo);
				sb.append("<th colspan='2'>"+responseText+"</th>");
				}
				if(!nobleflag){
				responseText=PaginationAndSorting.responseSortingMLink_2_TP(lblTranscript,sortOrderFieldName,"pathOfTranscript",sortOrderTypeVal,pgNo);
				sb.append("<th>"+responseText+"</th>");
				}
			}
			else
			{
				tablePadding_Data="";
				tablePadding_Data_right="style='padding-right:110px;text-align:right;'";
				
				sb.append("<table border='0' id='tblTeacherAcademics_Profile' class='table table-striped' onmouseover=\"javascript:setGridProfileVariable('teacherAce')\">");
				sb.append("<thead class='bg'>");			
				sb.append("<tr>");
	
				responseText=PaginationAndSorting.responseSortingMLink_1(lblEducation,sortOrderFieldName,"Education",sortOrderTypeVal,pgNo);
				sb.append("<th>"+responseText+"</th>");
	
				responseText=PaginationAndSorting.responseSortingMLink_1(lblDatesAttended,sortOrderFieldName,"attendedInYear",sortOrderTypeVal,pgNo);
				sb.append("<th>"+responseText+"</th>");
	
				sb.append("<th>");
				sb.append(lblGPA);
				sb.append("</th>");
				
				if(!nobleflag){
				responseText=PaginationAndSorting.responseSortingMLink_1(lblFieldOfStudy,sortOrderFieldName,"fieldOfStudy",sortOrderTypeVal,pgNo);
				sb.append("<th>"+responseText+"</th>");
				}else{
					responseText=PaginationAndSorting.responseSortingMLink_1(lblFieldOfStudy,sortOrderFieldName,"fieldOfStudy",sortOrderTypeVal,pgNo);
					sb.append("<th colspan='2'>"+responseText+"</th>");
				}
				if(!nobleflag){
				responseText=PaginationAndSorting.responseSortingMLink_1(lblTranscript,sortOrderFieldName,"pathOfTranscript",sortOrderTypeVal,pgNo);
				sb.append("<th>"+responseText+"</th>");
				}
			}
			
			sb.append("</tr>");
			sb.append("</thead>");
			
			
			if(lstTeacherAcademics!=null)
			{
				for(TeacherAcademics pojo:listsortedTeacherAcademics)
				{
					iRecordCount++;
					String gridColor="";
					if(iRecordCount%2==0 && ( visitLocation.equalsIgnoreCase("My Folders") || visitLocation.equalsIgnoreCase("Mosaic"))){
						gridColor="class='bggrid'";
					}
					
					String degree="";
					String universityName="";
					String fieldName="";
					if(pojo.getDegreeId()!=null && pojo.getDegreeId().getDegreeShortName()!=null){
						degree=pojo.getDegreeId().getDegreeShortName();
					}
					if(pojo.getUniversityId()!=null && pojo.getUniversityId().getUniversityName()!=null){
						universityName=pojo.getUniversityId().getUniversityName();
					}
					if(pojo.getFieldId()!=null && pojo.getFieldId().getFieldName()!=null){
						fieldName=pojo.getFieldId().getFieldName();	
					}
					
					sb.append("<tr "+gridColor+">");
					
					sb.append("<td "+tablePadding_Data+">");

					
					sb.append("<a href='javascript:void(0)' rel='tooltip' data-original-title='"+pojo.getDegreeId().getDegreeName()+"'  id='acadDGR"+pojo.getAcademicId()+"' >"+degree+"</a>, "+universityName);
					sb.append("<script>$('#acadDGR"+pojo.getAcademicId()+"').tooltip();</script>");
					sb.append("</td>");

					sb.append("<td "+tablePadding_Data+">");
					
					if(pojo.getAttendedInYear()!=null && !pojo.getAttendedInYear().equals(""))
						sb.append(pojo.getAttendedInYear());
					if(pojo.getAttendedInYear()!=null && !pojo.getAttendedInYear().equals("") && pojo.getLeftInYear()!=null && !pojo.getLeftInYear().equals(""))
						sb.append(" to ");
						if(pojo.getLeftInYear()!=null && !pojo.getLeftInYear().equals(""))
						sb.append(pojo.getLeftInYear());
					
					sb.append("</td>");
					
					sb.append("<td "+tablePadding_Data+">");
					if(pojo.getDegreeId().getDegreeType().trim().equalsIgnoreCase("b"))
					{
						sb.append(""+lblFreshman+": "+(pojo.getGpaFreshmanYear()==null?"":Utility.roundTwoDecimalsAsString(pojo.getGpaFreshmanYear()))+"<br> " +
							" "+lblSophomore+": "+(pojo.getGpaSophomoreYear()==null?"":Utility.roundTwoDecimalsAsString(pojo.getGpaSophomoreYear()))+"<br>" +
							" "+lblJunior+": "+(pojo.getGpaJuniorYear()==null?"":Utility.roundTwoDecimalsAsString(pojo.getGpaJuniorYear()))+"<br>" +
							" "+lblSenior+": "+(pojo.getGpaSeniorYear()==null?"":Utility.roundTwoDecimalsAsString(pojo.getGpaSeniorYear()))+"<br>" +
							" "+lblCumulative+": "+(pojo.getGpaCumulative()==null?"":Utility.roundTwoDecimalsAsString(pojo.getGpaCumulative())));
					}else{
						sb.append(""+lblCumulative+":"+(pojo.getGpaCumulative()==null?"":Utility.roundTwoDecimalsAsString(pojo.getGpaCumulative())));
					}
					sb.append("</td>");
					
					if(!nobleflag){	
						sb.append("<td "+tablePadding_Data+">");
						sb.append(fieldName);
						sb.append("</td>");
					}else{
						sb.append("<td colspan='2' "+tablePadding_Data+">");
						sb.append(fieldName);
						sb.append("</td>");
						
					}
				if(!nobleflag){	
					String windowFunc="if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
					sb.append("<td align=\"left\">");
					/*if(visitLocation.equalsIgnoreCase("Mosaic"))
						sb.append(pojo.getPathOfTranscript()==null?"":"<a href='javascript:void(0)' rel='tooltip' data-original-title='Click here to view transcript file !'  id='trans"+pojo.getAcademicId()+"' onclick=\"downloadTranscriptNew('"+pojo.getAcademicId()+"','trans"+pojo.getAcademicId()+"');"+windowFunc+"\">"+pojo.getPathOfTranscript()+"</a>");
					else*/
						sb.append(pojo.getPathOfTranscript()==null?"":"<a href='javascript:void(0)' rel='tooltip' data-original-title='"+pojo.getPathOfTranscript()+"'  id='trans"+pojo.getAcademicId()+"' onclick=\"downloadTranscript('"+pojo.getAcademicId()+"','trans"+pojo.getAcademicId()+"');"+windowFunc+"\"><span class='icon-download-alt icon-large iconcolorBlue' style='padding-left:10px;'></span></a>");
					
					sb.append("<script>$('#trans"+pojo.getAcademicId()+"').tooltip();</script>");
					sb.append("</td>");
				}
					sb.append("</tr>");
				}
				
				if(totalRecord>10 && ( visitLocation.equalsIgnoreCase("My Folders") || visitLocation.equalsIgnoreCase("Mosaic"))){
					sb.append("<tr><td colspan='4' style='border-top: 1px solid #cccccc;padding-bottom:0px;padding-top:5px;line-height:30px;'>");
					sb.append(PaginationAndSorting.getPagination_pageSizeVariableName(request,totalRecord,noOfRow, pageNo,"pageSize10","730"));
					sb.append("</td></tr>");
				}
			}

			if(listsortedTeacherAcademics==null || listsortedTeacherAcademics.size()==0)
			{
				
				if(visitLocation.equalsIgnoreCase("My Folders") || visitLocation.equalsIgnoreCase("Mosaic"))
				{
					tablePadding_Data="style='padding-left:10px;vertical-align:top;text-align:left;font-weight: normal;line-height:30px;'";
					sb.append("<tr><td "+tablePadding_Data+" colspan='4'>"+lblNoRecord+".</td></tr>" );
				}
				else
				{
					sb.append("<tr>");
					sb.append("<td colspan='4'>");
					sb.append(lblNoRecord+".");
					sb.append("</td>");
					sb.append("</tr>");
				}
			}
			
			sb.append("</table>");
			if(!visitLocation.equalsIgnoreCase("My Folders") && !visitLocation.equalsIgnoreCase("Mosaic")){
				//sb.append(PaginationAndSorting.getPagination_pageSizeVariableName(request,totalRecord,noOfRow, pageNo,"pageSize10","800"));
				sb.append(PaginationAndSorting.getPaginationPaging(request,totalRecord,noOfRow, pageNo,"pageSize10"));
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return sb.toString();		
	}
	
	
	
	// Certifications
	//@Transactional(readOnly=true)
	public String getTeacherCertificationsGridCommon(Integer teacherId, String noOfRow, String pageNo,String sortOrder,String sortOrderType,String visitLocation)
	{		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster=null;
		DistrictMaster districtMaster=null;
		int roleId=0;
		boolean nobleflag=false;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}else{
			userMaster=(UserMaster)session.getAttribute("userMaster");
			districtMaster=userMaster.getDistrictId();
		    if(districtMaster!=null)
		    	if(districtMaster.getDistrictId()==7800038)
		    		nobleflag=true;
		 }
		
		if(userMaster.getRoleId().getRoleId()!=null)
			roleId=userMaster.getRoleId().getRoleId();

		StringBuffer sb = new StringBuffer();		
		try 
		{
			String locale = Utility.getValueOfPropByKey("locale");
			String lblStatus    = Utility.getLocaleValuePropByKey("lblStatus", locale);
			String lblSource = Utility.getLocaleValuePropByKey("lblSource", locale);
			String lblName  = Utility.getLocaleValuePropByKey("lblName", locale);
            String lblNoRecord = Utility.getLocaleValuePropByKey("lblNoRecord", locale);
            String lblState  = Utility.getLocaleValuePropByKey("lblState", locale);
            String lblYearReceived   = Utility.getLocaleValuePropByKey("lblYearReceived", locale);
            String lblCertificationDetail    = Utility.getLocaleValuePropByKey("lblCertificationDetail", locale);
            String lblExpiration    = Utility.getLocaleValuePropByKey("lblExpiration", locale);
            
			
			
			int noOfRowInPage 	= Integer.parseInt(noOfRow);
			int pgNo 			= Integer.parseInt(pageNo);
			int start 			= ((pgNo-1)*noOfRowInPage);
			int end 			= ((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord 	= 0;
			//------------------------------------

			TeacherDetail teacherDetail = teacherDetailDAO.findById(teacherId, false, false);

			//List<TeacherRole> listTeacherRole= null;
			/** set default sorting fieldName **/
			String sortOrderFieldName	=	"createdDateTime";
			String sortOrderNoField		=	"createdDateTime";

			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal=null;

			if(sortOrder!=null){
				if(!sortOrder.equals("") && !sortOrder.equals(null) && !sortOrder.equals("certificateType")&& !sortOrder.equals("state")){
					sortOrderFieldName=sortOrder;
					sortOrderNoField=sortOrder;
				}
				if(sortOrder.equals("certificateType")){
					sortOrderNoField="certificateType";
				}
				if(sortOrder.equals("state")){
					sortOrderNoField="state";
				}
			}

			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
				if(sortOrderType.equals("0")){
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}else{
				sortOrderTypeVal="0";
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}
			/**End ------------------------------------**/

			Criterion criterion = Restrictions.eq("teacherDetail",teacherDetail);
			List<TeacherCertificate> lstTeacherCertificate = teacherCertificateDAO.findByCriteria(sortOrderStrVal,criterion);
			
			List<TeacherCertificate> sortedTeacherCertificates		=	new ArrayList<TeacherCertificate>();

			SortedMap<String,TeacherCertificate>	sortedMap = new TreeMap<String,TeacherCertificate>();
			if(sortOrderNoField.equals("certificateType"))
			{
				sortOrderFieldName	=	"certificateType";
			}
			if(sortOrderNoField.equals("state"))
			{
				sortOrderFieldName	=	"state";
			}
			
			int mapFlag=2;
			for (TeacherCertificate pojo : lstTeacherCertificate){
				String orderFieldName=""+pojo.getCertId();
				if(sortOrderFieldName.equals("certificateType")){
					String sortCType="";
					if(pojo.getCertificateTypeMaster()==null){
						sortCType=" ";
					}else{
						sortCType=pojo.getCertificateTypeMaster().getCertType();
					}
					orderFieldName=sortCType+"||"+pojo.getCertId();
					sortedMap.put(orderFieldName+"||",pojo);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}
				if(sortOrderFieldName.equals("state")){
					String stateName="";
					
					if(pojo.getStateMaster()==null){
						stateName= "";
					}else{
						stateName= pojo.getStateMaster().getStateName();
					}
					orderFieldName=stateName+"||"+pojo.getCertId();
					sortedMap.put(orderFieldName+"||",pojo);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}
				
			}
			if(mapFlag==1){
				NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet();  
				for (Iterator iter=navig.iterator();iter.hasNext();) {  
					Object key = iter.next(); 
					sortedTeacherCertificates.add((TeacherCertificate) sortedMap.get(key));
				} 
			}else if(mapFlag==0){
				Iterator iterator = sortedMap.keySet().iterator();
				while (iterator.hasNext()) {
					Object key = iterator.next();
					sortedTeacherCertificates.add((TeacherCertificate) sortedMap.get(key));
				}
			}else{
				sortedTeacherCertificates=lstTeacherCertificate;
			}

			totalRecord =lstTeacherCertificate.size();

			if(totalRecord<end)
				end=totalRecord;
			List<TeacherCertificate> listsortedTeacherCertificatess		=	sortedTeacherCertificates.subList(start,end);

			String refDivWidth="";
			if(userMaster.getEntityType().equals(2)  || userMaster.getEntityType().equals(3))
			  refDivWidth="830";
			else
				refDivWidth="730";							
			String responseText="";
			String tablePadding_Data="";
			if(visitLocation.equalsIgnoreCase("My Folders") || visitLocation.equalsIgnoreCase("Mosaic"))
			{
				tablePadding_Data="style='padding-left:10px;vertical-align:top;text-align:left;font-weight:normal;line-height:30px;'";
				
				String tablePadding="style='padding-left:5px;vertical-align:top;color:#FFFFFF;line-height:25px;'";
				sb.append("<table id='tblTeacherCertificates_Profile' border='0'  width='"+refDivWidth+"' class='table-bordered' onmouseover=\"javascript:setGridProfileVariable('teacherCerti')\">");
				sb.append("<thead class='bg'>");
				sb.append("<tr>");
				
				/*responseText=PaginationAndSorting.responseSortingMLink_1("State",sortOrderFieldName,"state",sortOrderTypeVal,pgNo);
				sb.append("<th align=\"left\" width='230' "+tablePadding+">"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingMLink_1("Year received",sortOrderFieldName,"yearReceived",sortOrderTypeVal,pgNo);
				sb.append("<th align=\"left\" width='250' "+tablePadding+">"+responseText+"</th>");
				responseText=PaginationAndSorting.responseSortingMLink_1("Year&nbsp;received",sortOrderFieldName,"yearReceived",sortOrderTypeVal,pgNo);
				sb.append("<th align=\"left\" width='230' "+tablePadding+">"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingMLink_1("Name",sortOrderFieldName,"certificateType",sortOrderTypeVal,pgNo);
				sb.append("<th align=\"left\" width='230' "+tablePadding+">"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingMLink_1("Certification&nbsp;Letter",sortOrderFieldName,"pathOfCertification",sortOrderTypeVal,pgNo);
				sb.append("<th align=\"left\" width='230' "+tablePadding+">"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingMLink_1("Certification&nbsp;Status",sortOrderFieldName,"certificationStatusMaster",sortOrderTypeVal,pgNo);
				sb.append("<th align=\"left\" width='230' "+tablePadding+">"+responseText+"</th>");*/
				
				if(!nobleflag){
				responseText=PaginationAndSorting.responseSortingMLink_1(lblName,sortOrderFieldName,"certificateType",sortOrderTypeVal,pgNo);
				sb.append("<th align=\"left\" width='200' "+tablePadding+">"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingMLink_1(lblState,sortOrderFieldName,"state",sortOrderTypeVal,pgNo);
				sb.append("<th align=\"left\" width='100' "+tablePadding+">"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingMLink_1(lblYearReceived,sortOrderFieldName,"yearReceived",sortOrderTypeVal,pgNo);
				sb.append("<th align=\"left\" width='100' "+tablePadding+">"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingMLink_1(lblStatus,sortOrderFieldName,"certificationStatusMaster",sortOrderTypeVal,pgNo);
				sb.append("<th align=\"left\" width='130' "+tablePadding+">"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingMLink_1(lblSource,sortOrderFieldName,"pathOfCertification",sortOrderTypeVal,pgNo);
				sb.append("<th align=\"left\" width='200' "+tablePadding+">"+responseText+"</th>");
				}else{
					//	Certification Detail
					//responseText=PaginationAndSorting.responseSortingMLink_1("Source",sortOrderFieldName,"pathOfCertification",sortOrderTypeVal,pgNo);
					responseText=PaginationAndSorting.responseSortingMLink_1(lblStatus,sortOrderFieldName,"certificationStatusMaster",sortOrderTypeVal,pgNo);
					sb.append("<th align=\"left\" width='200' "+tablePadding+">"+responseText+"</th>");
					sb.append("<th align=\"left\" width='530' "+tablePadding+">"+lblCertificationDetail+"</th>");
				}
				
			}
			else if(visitLocation.equalsIgnoreCase("Teacher Pool"))
			{
				tablePadding_Data="";
				sb.append("<table border='0' id='tblTeacherCertificates_Profile' class='table table-striped' onmouseover=\"javascript:setGridProfileVariable('teacherCerti')\">");
				sb.append("<thead class='bg'>");			
				sb.append("<tr>");
				
				/*responseText=PaginationAndSorting.responseSortingMLink_2_TP("State",sortOrderFieldName,"state",sortOrderTypeVal,pgNo);
				sb.append("<th>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingMLink_2_TP("Year&nbsp;received",sortOrderFieldName,"yearReceived",sortOrderTypeVal,pgNo);
				sb.append("<th>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingMLink_2_TP("Name",sortOrderFieldName,"certificateType",sortOrderTypeVal,pgNo);
				sb.append("<th>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingMLink_2_TP("Certification&nbsp;Letter",sortOrderFieldName,"pathOfCertification",sortOrderTypeVal,pgNo);
				sb.append("<th>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingMLink_2_TP("Certification&nbsp;Status",sortOrderFieldName,"certificationStatusMaster",sortOrderTypeVal,pgNo);
				sb.append("<th>"+responseText+"</th>");*/
				
				if(!nobleflag){
				responseText=PaginationAndSorting.responseSortingMLink_2_TP(lblName,sortOrderFieldName,"certificateType",sortOrderTypeVal,pgNo);
				sb.append("<th>"+responseText+"</th>");
					
				responseText=PaginationAndSorting.responseSortingMLink_2_TP(lblState,sortOrderFieldName,"state",sortOrderTypeVal,pgNo);
				sb.append("<th>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingMLink_2_TP(lblYearReceived,sortOrderFieldName,"yearReceived",sortOrderTypeVal,pgNo);
				sb.append("<th>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingMLink_2_TP(lblStatus,sortOrderFieldName,"certificationStatusMaster",sortOrderTypeVal,pgNo);
				sb.append("<th>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingMLink_2_TP(lblSource,sortOrderFieldName,"pathOfCertification",sortOrderTypeVal,pgNo);
				sb.append("<th>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingMLink_1(lblExpiration,sortOrderFieldName,"pathOfCertification",sortOrderTypeVal,pgNo);
				sb.append("<th>"+responseText+"</th>");
				
				}else{
					responseText=PaginationAndSorting.responseSortingMLink_2_TP(lblStatus,sortOrderFieldName,"certificationStatusMaster",sortOrderTypeVal,pgNo);
					sb.append("<th>"+responseText+"</th>");
					
					sb.append("<th>"+lblCertificationDetail+"</th>");	
				}
				
			}
			else
			{
				tablePadding_Data="";
				sb.append("<table border='0' id='tblTeacherCertificates_Profile' class='table table-striped' onmouseover=\"javascript:setGridProfileVariable('teacherCerti')\">");
				sb.append("<thead class='bg'>");			
				sb.append("<tr>");
				
				/*responseText=PaginationAndSorting.responseSortingMLink_1("State",sortOrderFieldName,"state",sortOrderTypeVal,pgNo);
				sb.append("<th>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingMLink_1("Year&nbsp;received",sortOrderFieldName,"yearReceived",sortOrderTypeVal,pgNo);
				sb.append("<th>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingMLink_1("Name",sortOrderFieldName,"certificateType",sortOrderTypeVal,pgNo);
				sb.append("<th>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingMLink_1("Certification&nbsp;Letter",sortOrderFieldName,"pathOfCertification",sortOrderTypeVal,pgNo);
				sb.append("<th>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingMLink_1("Certification&nbsp;Status",sortOrderFieldName,"certificationStatusMaster",sortOrderTypeVal,pgNo);
				sb.append("<th>"+responseText+"</th>");*/
				
				
				if(!nobleflag){
				responseText=PaginationAndSorting.responseSortingMLink_1(lblName,sortOrderFieldName,"certificateType",sortOrderTypeVal,pgNo);
				sb.append("<th>"+responseText+"</th>");
					
				responseText=PaginationAndSorting.responseSortingMLink_1(lblState,sortOrderFieldName,"state",sortOrderTypeVal,pgNo);
				sb.append("<th>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingMLink_1(lblYearReceived,sortOrderFieldName,"yearReceived",sortOrderTypeVal,pgNo);
				sb.append("<th>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingMLink_1(lblStatus,sortOrderFieldName,"certificationStatusMaster",sortOrderTypeVal,pgNo);
				sb.append("<th>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingMLink_1(lblSource,sortOrderFieldName,"pathOfCertification",sortOrderTypeVal,pgNo);
				sb.append("<th>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingMLink_1(lblExpiration,sortOrderFieldName,"pathOfCertification",sortOrderTypeVal,pgNo);
				sb.append("<th>"+responseText+"</th>");
				
				}else{
					responseText=PaginationAndSorting.responseSortingMLink_1(lblStatus,sortOrderFieldName,"certificationStatusMaster",sortOrderTypeVal,pgNo);
					sb.append("<th>"+responseText+"</th>");
					sb.append("<th>"+lblCertificationDetail+"</th>");	
				}
				
			}
			sb.append("</tr>");
			sb.append("</thead>");
			int iRecordCount=1;
			if(lstTeacherCertificate!=null)
			{  
				for(TeacherCertificate pojo:listsortedTeacherCertificatess)
				{
					//tablePadding_Data="style='padding-left:10px;vertical-align:top;text-align:left;font-weight:normal;line-height:30px;'";
					iRecordCount++;
					String gridColor="";
					if(iRecordCount%2==0 && ( visitLocation.equalsIgnoreCase("My Folders") || visitLocation.equalsIgnoreCase("Mosaic"))){
						gridColor="class='bggrid'";
					}

					
					String cityType="",stateName="",yearReceived="",ceretificationletter="",ceretificationstatus="",teaId="";
					String yearExpired = "Does Not Expire";
					if(pojo.getCertificateTypeMaster()!=null){
						cityType=pojo.getCertificateTypeMaster().getCertType();
					}
					if(pojo.getStateMaster()!=null){
						stateName=pojo.getStateMaster().getStateName();
					}
					if(pojo.getYearReceived()!=null){
						yearReceived=pojo.getYearReceived().toString();
					}
					/*if(pojo.getCertificateTypeMaster()!=null)
						ceretificationletter=pojo.getCertificateTypeMaster().getCertType();*/
					if(pojo.getCertificationStatusMaster()!=null){
						ceretificationstatus=pojo.getCertificationStatusMaster().getCertificationStatusName();
					}
					if(pojo.getCertificationStatusMaster()!=null){
						teaId=pojo.getTeacherDetail().getTeacherId().toString();
					}
					
					if(pojo.getYearExpired()!=null && pojo.getYearExpired()!=0)
						yearExpired = pojo.getYearExpired().toString();
						
						
					sb.append("<tr "+gridColor+">");
					
					
					
					if(!nobleflag){
					sb.append("<td "+tablePadding_Data+">");
					sb.append(cityType);
					sb.append("</td>");
					sb.append("<td "+tablePadding_Data+">");
					sb.append(stateName);					
					sb.append("</td>");
					
					sb.append("<td "+tablePadding_Data+">");
					sb.append(yearReceived);					
					sb.append("</td>");
					
					
					
					/*sb.append("<td "+tablePadding_Data+">");
					sb.append(ceretificationletter);					
					sb.append("</td>");*/
					
					sb.append("<td "+tablePadding_Data+">");
					sb.append(ceretificationstatus);					
					sb.append("</td>");
					
					System.out.println("  pojo.getPathOfCertification() :: "+pojo.getPathOfCertification()+" ..pojo.getCertId() :: "+pojo.getCertId());
					
					
						String windowFunc="if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
						sb.append("<td "+tablePadding_Data+">");
						
					if(pojo.getPathOfCertification()!=null)
					{	
						sb.append(pojo.getPathOfCertification()==null?"":"<a href='javascript:void(0)' rel='tooltip' data-original-title='"+pojo.getPathOfCertification()+"'  id='cert"+pojo.getCertId()+"' onclick=\"downloadCertificationforCG('"+pojo.getCertId()+"','cert"+pojo.getCertId()+"','"+teaId+"');"+windowFunc+"\"><span class='icon-download-alt icon-large iconcolorBlue' style=''></span></a><br>");
						sb.append("<script>$('#cert"+pojo.getCertId()+"').tooltip();</script>");
					}
					
					if(pojo.getCertUrl()!=null && !pojo.getCertUrl().equals(""))
					{
						if(pojo.getCertUrl().length()>5)
						{
							String url=pojo.getCertUrl().substring(0,5);
							url=url+"....";
							
							sb.append("<a href='"+pojo.getCertUrl()+"' data-original-title='"+pojo.getCertUrl()+"' id='cert1"+pojo.getCertId()+"' target='_blank'"+windowFunc+">"+url+"</a>");
							sb.append("<script>$('#cert1"+pojo.getCertId()+"').tooltip();</script>");
						}
					}
					
					sb.append("</td>");
					
					sb.append("<td "+tablePadding_Data+">");
					sb.append(yearExpired);					
					sb.append("</td>");
					
					}else{
						
						sb.append("<td "+tablePadding_Data+">");
						sb.append(ceretificationstatus);					
						sb.append("</td>");
						
						String certHtml="";
						sb.append("<td "+tablePadding_Data+">");
						if(pojo.getCertText()!=null && !pojo.getCertText().equals("")){
							sb.append("<a id='"+pojo.getCertId()+"' href='javascript:void(0)' onclick='certTextDiv("+pojo.getCertId()+");'>");					
							certHtml = html2text(pojo.getCertText());
							if(certHtml.length()>49){
								sb.append(htmlwords(certHtml)+"...");
							}else{
								sb.append(certHtml);
							}
							sb.append("</a>");
							sb.append("<input type='hidden' value='"+pojo.getCertText()+"' id='cerTTextContent"+pojo.getCertId()+"'>" );
						}
						else
							sb.append("");
						sb.append("</td>");
					}
					sb.append("</tr>");
				}
				
				if(totalRecord>10 && (visitLocation.equalsIgnoreCase("My Folders") || visitLocation.equalsIgnoreCase("Mosaic"))){
					sb.append("<tr><td colspan='2' style='border-top: 1px solid #cccccc;padding-bottom:0px;padding-top:5px;line-height:30px;'>");
					sb.append(PaginationAndSorting.getPagination_pageSizeVariableName(request,totalRecord,noOfRow, pageNo,"pageSize9","730"));
					sb.append("</td></tr>");
				}
				
			}

			if(listsortedTeacherCertificatess==null || listsortedTeacherCertificatess.size()==0)
			{
				
				if(visitLocation.equalsIgnoreCase("My Folders") || visitLocation.equalsIgnoreCase("Mosaic"))
				{
					tablePadding_Data="style='padding-left:10px;vertical-align:top;text-align:left;font-weight: normal;line-height:30px;'";
					sb.append("<tr><td "+tablePadding_Data+" colspan='2'>"+lblNoRecord+".</td></tr>" );
				}
				else
				{
					sb.append("<tr>");
					sb.append("<td colspan='2'>");
					sb.append(lblNoRecord+".");
					sb.append("</td>");
					sb.append("</tr>");
				}
			}
			
			sb.append("</table>");
			if(!visitLocation.equalsIgnoreCase("My Folders") && !visitLocation.equalsIgnoreCase("Mosaic"))
			{
				//sb.append(PaginationAndSorting.getPagination_pageSizeVariableName(request,totalRecord,noOfRow, pageNo,"pageSize9","800"));
				sb.append(PaginationAndSorting.getPaginationPaging(request,totalRecord,noOfRow, pageNo,"pageSize9"));
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return sb.toString();		
	}
	
	public String getTeacherCertificationsGridOsceola(Integer teacherId, String noOfRow, String pageNo,String sortOrder,String sortOrderType,String visitLocation)
	{		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster=null;
		DistrictMaster districtMaster=null;
		int roleId=0;
		boolean nobleflag=false;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}else{
			userMaster=(UserMaster)session.getAttribute("userMaster");
			districtMaster=userMaster.getDistrictId();
		    if(districtMaster!=null)
		    	if(districtMaster.getDistrictId()==7800038)
		    		nobleflag=true;
		 }
		
		if(userMaster.getRoleId().getRoleId()!=null)
			roleId=userMaster.getRoleId().getRoleId();

		StringBuffer sb = new StringBuffer();		
		try 
		{
			String locale = Utility.getValueOfPropByKey("locale");
			String lblStatus    = Utility.getLocaleValuePropByKey("lblStatus", locale);
			String lblSource = Utility.getLocaleValuePropByKey("lblSource", locale);
			String lblName  = Utility.getLocaleValuePropByKey("lblName", locale);
            String lblNoRecord = Utility.getLocaleValuePropByKey("lblNoRecord", locale);
            String lblState  = Utility.getLocaleValuePropByKey("lblState", locale);
            String lblYearReceived   = Utility.getLocaleValuePropByKey("lblYearReceived", locale);
            String lblCertificationDetail    = Utility.getLocaleValuePropByKey("lblCertificationDetail", locale);
            String lblType    = Utility.getLocaleValuePropByKey("lblType", locale);
            String lblIEIN    = Utility.getLocaleValuePropByKey("lblIEIN", locale);
            String lblYearExpired    = Utility.getLocaleValuePropByKey("lblYearExpired", locale);
			
			
			int noOfRowInPage 	= Integer.parseInt(noOfRow);
			int pgNo 			= Integer.parseInt(pageNo);
			int start 			= ((pgNo-1)*noOfRowInPage);
			int end 			= ((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord 	= 0;
			//------------------------------------

			TeacherDetail teacherDetail = teacherDetailDAO.findById(teacherId, false, false);

			//List<TeacherRole> listTeacherRole= null;
			/** set default sorting fieldName **/
			String sortOrderFieldName	=	"createdDateTime";
			String sortOrderNoField		=	"createdDateTime";

			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal=null;

			if(sortOrder!=null){
				if(!sortOrder.equals("") && !sortOrder.equals(null) && !sortOrder.equals("certificateType")&& !sortOrder.equals("state")){
					sortOrderFieldName=sortOrder;
					sortOrderNoField=sortOrder;
				}
				if(sortOrder.equals("certificateType")){
					sortOrderNoField="certificateType";
				}
				if(sortOrder.equals("state")){
					sortOrderNoField="state";
				}
			}

			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
				if(sortOrderType.equals("0")){
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}else{
				sortOrderTypeVal="0";
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}
			
			
			/**End ------------------------------------**/

			Criterion criterion = Restrictions.eq("teacherDetail",teacherDetail);
			List<TeacherCertificate> lstTeacherCertificate = teacherCertificateDAO.findByCriteria(sortOrderStrVal,criterion);
			
			List<TeacherCertificate> sortedTeacherCertificates		=	new ArrayList<TeacherCertificate>();

			SortedMap<String,TeacherCertificate>	sortedMap = new TreeMap<String,TeacherCertificate>();
			if(sortOrderNoField.equals("certificateType"))
			{
				sortOrderFieldName	=	"certificateType";
			}
			if(sortOrderNoField.equals("state"))
			{
				sortOrderFieldName	=	"state";
			}
			
			int mapFlag=2;
			for (TeacherCertificate pojo : lstTeacherCertificate){
				String orderFieldName=""+pojo.getCertId();
				if(sortOrderFieldName.equals("certificateType")){
					String sortCType="";
					if(pojo.getCertificateTypeMaster()==null){
						sortCType=" ";
					}else{
						sortCType=pojo.getCertificateTypeMaster().getCertType();
					}
					orderFieldName=sortCType+"||"+pojo.getCertId();
					sortedMap.put(orderFieldName+"||",pojo);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}
				if(sortOrderFieldName.equals("state")){
					String stateName="";
					
					if(pojo.getStateMaster()==null){
						stateName= "";
					}else{
						stateName= pojo.getStateMaster().getStateName();
					}
					orderFieldName=stateName+"||"+pojo.getCertId();
					sortedMap.put(orderFieldName+"||",pojo);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}
				
			}
			if(mapFlag==1){
				NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet();  
				for (Iterator iter=navig.iterator();iter.hasNext();) {  
					Object key = iter.next(); 
					sortedTeacherCertificates.add((TeacherCertificate) sortedMap.get(key));
				} 
			}else if(mapFlag==0){
				Iterator iterator = sortedMap.keySet().iterator();
				while (iterator.hasNext()) {
					Object key = iterator.next();
					sortedTeacherCertificates.add((TeacherCertificate) sortedMap.get(key));
				}
			}else{
				sortedTeacherCertificates=lstTeacherCertificate;
			}

			totalRecord =lstTeacherCertificate.size();

			if(totalRecord<end)
				end=totalRecord;
			List<TeacherCertificate> listsortedTeacherCertificatess		=	sortedTeacherCertificates.subList(start,end);

			String refDivWidth="";
			if(userMaster.getEntityType().equals(2)  || userMaster.getEntityType().equals(3))
			  refDivWidth="830";
			else
				refDivWidth="730";							
			String responseText="";
			String tablePadding_Data="";
			if(visitLocation.equalsIgnoreCase("Teacher Pool"))
			{
				tablePadding_Data="";
				sb.append("<table border='0' id='tblTeacherCertificates_Profile' class='table table-striped' onmouseover=\"javascript:setGridProfileVariable('teacherCerti')\">");
				sb.append("<thead class='bg'>");			
				sb.append("<tr>");
			
				if(!nobleflag){
				responseText=PaginationAndSorting.responseSortingMLink_2_TP(lblName,sortOrderFieldName,"certificateType",sortOrderTypeVal,pgNo);
				sb.append("<th>"+responseText+"</th>");
					
				responseText=PaginationAndSorting.responseSortingMLink_2_TP(lblState,sortOrderFieldName,"state",sortOrderTypeVal,pgNo);
				sb.append("<th>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingMLink_2_TP(lblYearReceived,sortOrderFieldName,"yearReceived",sortOrderTypeVal,pgNo);
				sb.append("<th>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingMLink_2_TP(lblYearExpired,sortOrderFieldName,"yearExpired",sortOrderTypeVal,pgNo);
				sb.append("<th>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingMLink_2_TP(lblStatus,sortOrderFieldName,"certificationStatusMaster",sortOrderTypeVal,pgNo);
				sb.append("<th>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingMLink_2_TP(lblType,sortOrderFieldName,"certType",sortOrderTypeVal,pgNo);
				sb.append("<th>"+responseText+"</th>");
				if(districtMaster!=null && districtMaster.getDistrictId().equals(7800040)){
				//IEIN number
				responseText=PaginationAndSorting.responseSortingMLink_2_TP(lblIEIN,sortOrderFieldName,"ieinNumber",sortOrderTypeVal,pgNo);
				sb.append("<th>"+responseText+"</th>");
				}else{
					responseText=PaginationAndSorting.responseSortingMLink_2_TP(lblSource,sortOrderFieldName,"pathOfCertification",sortOrderTypeVal,pgNo);
					sb.append("<th>"+responseText+"</th>");
				}
				
				}else{
					responseText=PaginationAndSorting.responseSortingMLink_2_TP(lblStatus,sortOrderFieldName,"certificationStatusMaster",sortOrderTypeVal,pgNo);
					sb.append("<th>"+responseText+"</th>");
					
					sb.append("<th>"+lblCertificationDetail+"</th>");	
				}
				
			}
			else
			{
				tablePadding_Data="";
				sb.append("<table border='0' id='tblTeacherCertificates_Profile' class='table table-striped' onmouseover=\"javascript:setGridProfileVariable('teacherCerti')\">");
				sb.append("<thead class='bg'>");			
				sb.append("<tr>");
								
				if(!nobleflag){
				responseText=PaginationAndSorting.responseSortingMLink_1(lblName,sortOrderFieldName,"certificateType",sortOrderTypeVal,pgNo);
				sb.append("<th>"+responseText+"</th>");
					
				responseText=PaginationAndSorting.responseSortingMLink_1(lblState,sortOrderFieldName,"state",sortOrderTypeVal,pgNo);
				sb.append("<th>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingMLink_1(lblYearReceived,sortOrderFieldName,"yearReceived",sortOrderTypeVal,pgNo);
				sb.append("<th>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingMLink_1(lblYearExpired,sortOrderFieldName,"yearExpired",sortOrderTypeVal,pgNo);
				sb.append("<th>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingMLink_1(lblStatus,sortOrderFieldName,"certificationStatusMaster",sortOrderTypeVal,pgNo);
				sb.append("<th>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingMLink_1(lblType,sortOrderFieldName,"certType",sortOrderTypeVal,pgNo);
				sb.append("<th>"+responseText+"</th>");
				
				if(districtMaster!=null && districtMaster.getDistrictId().equals(7800040)){
					//IEIN number
					responseText=PaginationAndSorting.responseSortingMLink_1(lblIEIN,sortOrderFieldName,"ieinNumber",sortOrderTypeVal,pgNo);
					sb.append("<th>"+responseText+"</th>");
					}else{
						responseText=PaginationAndSorting.responseSortingMLink_1(lblSource,sortOrderFieldName,"pathOfCertification",sortOrderTypeVal,pgNo);
						sb.append("<th>"+responseText+"</th>");
					}
				
				}else{
					responseText=PaginationAndSorting.responseSortingMLink_1(lblStatus,sortOrderFieldName,"certificationStatusMaster",sortOrderTypeVal,pgNo);
					sb.append("<th>"+responseText+"</th>");
					sb.append("<th>"+lblCertificationDetail+"</th>");	
				}
				
			}
			sb.append("</tr>");
			sb.append("</thead>");
			int iRecordCount=1;
			if(lstTeacherCertificate!=null)
			{  
				for(TeacherCertificate pojo:listsortedTeacherCertificatess)
				{
					//tablePadding_Data="style='padding-left:10px;vertical-align:top;text-align:left;font-weight:normal;line-height:30px;'";
					iRecordCount++;
					String gridColor="";
					if(iRecordCount%2==0 && ( visitLocation.equalsIgnoreCase("My Folders") || visitLocation.equalsIgnoreCase("Mosaic"))){
						gridColor="class='bggrid'";
					}

					
					String cityType="",stateName="",yearReceived="",ceretificationletter="",ceretificationstatus="",teaId="";
					if(pojo.getCertificateTypeMaster()!=null){
						cityType=pojo.getCertificateTypeMaster().getCertType();
					}
					if(pojo.getStateMaster()!=null){
						stateName=pojo.getStateMaster().getStateName();
					}
					if(pojo.getYearReceived()!=null){
						yearReceived=pojo.getYearReceived().toString();
					}
					/*if(pojo.getCertificateTypeMaster()!=null)
						ceretificationletter=pojo.getCertificateTypeMaster().getCertType();*/
					if(pojo.getCertificationStatusMaster()!=null){
						ceretificationstatus=pojo.getCertificationStatusMaster().getCertificationStatusName();
					}
					if(pojo.getCertificationStatusMaster()!=null){
						teaId=pojo.getTeacherDetail().getTeacherId().toString();
					}
					
					
					sb.append("<tr "+gridColor+">");
					
					
					
					if(!nobleflag){
					sb.append("<td "+tablePadding_Data+">");
					sb.append(cityType);
					sb.append("</td>");
					sb.append("<td "+tablePadding_Data+">");
					sb.append(stateName);					
					sb.append("</td>");
					
					sb.append("<td "+tablePadding_Data+">");
					sb.append(yearReceived);					
					sb.append("</td>");
					sb.append("<td "+tablePadding_Data+">");
					if(pojo.getYearExpired()!=null)
					sb.append(pojo.getYearExpired().toString());					
					sb.append("</td>");
					
					/*sb.append("<td "+tablePadding_Data+">");
					sb.append(ceretificationletter);					
					sb.append("</td>");*/
					
					sb.append("<td "+tablePadding_Data+">");
					sb.append(ceretificationstatus);					
					sb.append("</td>");
					sb.append("<td "+tablePadding_Data+">");
					sb.append(pojo.getCertType());					
					sb.append("</td>");

					
					
					
						String windowFunc="if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
						sb.append("<td "+tablePadding_Data+">");
					
						if(districtMaster.getDistrictId().equals(7800040)){
							if(pojo.getIeinNumber()!=null && !pojo.getIeinNumber().equalsIgnoreCase(""))
								sb.append(pojo.getIeinNumber());
								else{
									sb.append("");
								}
						}
						else{
							if(pojo.getPathOfCertification()!=null)
							{	
								sb.append(pojo.getPathOfCertification()==null?"":"<a href='javascript:void(0)' rel='tooltip' data-original-title='"+pojo.getPathOfCertification()+"'  id='cert"+pojo.getCertId()+"' onclick=\"downloadCertificationforCG('"+pojo.getCertId()+"','cert"+pojo.getCertId()+"','"+teaId+"');"+windowFunc+"\"><span class='icon-download-alt icon-large iconcolorBlue' style=''></span></a><br>");
								sb.append("<script>$('#cert"+pojo.getCertId()+"').tooltip();</script>");
							}
							
							if(pojo.getCertUrl()!=null && !pojo.getCertUrl().equals(""))
							{
								if(pojo.getCertUrl().length()>5)
								{
									String url=pojo.getCertUrl().substring(0,5);
									url=url+"....";
									
									sb.append("<a href='"+pojo.getCertUrl()+"' data-original-title='"+pojo.getCertUrl()+"' id='cert1"+pojo.getCertId()+"' target='_blank'"+windowFunc+">"+url+"</a>");
									sb.append("<script>$('#cert1"+pojo.getCertId()+"').tooltip();</script>");
								}
							}
						}
					sb.append("</td>");
					}else{
						
						sb.append("<td "+tablePadding_Data+">");
						sb.append(ceretificationstatus);					
						sb.append("</td>");
						
						String certHtml="";
						sb.append("<td "+tablePadding_Data+">");
						if(pojo.getCertText()!=null && !pojo.getCertText().equals("")){
							sb.append("<a id='"+pojo.getCertId()+"' href='javascript:void(0)' onclick='certTextDiv("+pojo.getCertId()+");'>");					
							certHtml = html2text(pojo.getCertText());
							if(certHtml.length()>49){
								sb.append(htmlwords(certHtml)+"...");
							}else{
								sb.append(certHtml);
							}
							sb.append("</a>");
							sb.append("<input type='hidden' value='"+pojo.getCertText()+"' id='cerTTextContent"+pojo.getCertId()+"'>" );
						}
						else
							sb.append("");
						sb.append("</td>");
					}
					sb.append("</tr>");
				}
				
				if(totalRecord>10 && (visitLocation.equalsIgnoreCase("My Folders") || visitLocation.equalsIgnoreCase("Mosaic"))){
					sb.append("<tr><td colspan='2' style='border-top: 1px solid #cccccc;padding-bottom:0px;padding-top:5px;line-height:30px;'>");
					sb.append(PaginationAndSorting.getPagination_pageSizeVariableName(request,totalRecord,noOfRow, pageNo,"pageSize9","730"));
					sb.append("</td></tr>");
				}
				
			}

			if(listsortedTeacherCertificatess==null || listsortedTeacherCertificatess.size()==0)
			{
				
				if(visitLocation.equalsIgnoreCase("My Folders") || visitLocation.equalsIgnoreCase("Mosaic"))
				{
					tablePadding_Data="style='padding-left:10px;vertical-align:top;text-align:left;font-weight: normal;line-height:30px;'";
					sb.append("<tr><td "+tablePadding_Data+" colspan='2'>"+lblNoRecord+".</td></tr>" );
				}
				else
				{
					sb.append("<tr>");
					sb.append("<td colspan='2'>");
					sb.append(lblNoRecord+".");
					sb.append("</td>");
					sb.append("</tr>");
				}
			}
			
			sb.append("</table>");
			if(!visitLocation.equalsIgnoreCase("My Folders") && !visitLocation.equalsIgnoreCase("Mosaic"))
			{
				//sb.append(PaginationAndSorting.getPagination_pageSizeVariableName(request,totalRecord,noOfRow, pageNo,"pageSize9","800"));
				sb.append(PaginationAndSorting.getPaginationPaging(request,totalRecord,noOfRow, pageNo,"pageSize9"));
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return sb.toString();		
	}
	
	//@Transactional(readOnly=false)
	public String[] getUpdateAndSaveAScore(Integer teacherId,Integer districtId,Integer slider_aca_ach,Integer slider_lea_res){
		
		String[] strScore=new String[2];
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		
		UserMaster userMaster=null;
		userMaster=(UserMaster)session.getAttribute("userMaster");
		
		try {
			
			TeacherDetail teacherDetail=teacherDetailDAO.findById(teacherId, false, false);
			DistrictMaster districtMaster=districtMasterDAO.findById(districtId, false, false);
			
			TeacherAchievementScore teacherAchievementScore=new TeacherAchievementScore();
			
			List<TeacherAchievementScore> teacherAchievementScoresList=teacherAchievementScoreDAO.getAchievementScoreByTeacherAndDistrict(teacherDetail, districtMaster);
			if(teacherAchievementScoresList.size()==1)
			{
				teacherAchievementScore=teacherAchievementScoresList.get(0);
			}
			
			teacherAchievementScore.setTeacherDetail(teacherDetail);
			teacherAchievementScore.setDistrictMaster(districtMaster);
			
			teacherAchievementScore.setAcademicAchievementScore(slider_aca_ach);
			teacherAchievementScore.setLeadershipAchievementScore(slider_lea_res);
			
			teacherAchievementScore.setAcademicAchievementMaxScore(20);
			teacherAchievementScore.setLeadershipAchievementMaxScore(20);
			
			teacherAchievementScore.setTotalAchievementScore(slider_aca_ach+slider_lea_res);
			teacherAchievementScore.setAchievementMaxScore(40);
			teacherAchievementScore.setScoredBy(userMaster);
			teacherAchievementScore.setScoredDateTime(new Date());
			
			teacherAchievementScoreDAO.makePersistent(teacherAchievementScore);
			
			strScore[0]=teacherAchievementScore.getAcademicAchievementScore()+"/"+teacherAchievementScore.getAcademicAchievementMaxScore();
			strScore[1]=teacherAchievementScore.getLeadershipAchievementScore()+"/"+teacherAchievementScore.getLeadershipAchievementMaxScore();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return strScore;
	}
	

	public String  showProfileContent(Long jobForTeacherId,Integer teacherId,String normScore,String jobId,String colorName,Integer noOfRecordCheck,int fg)
	{
		System.out.println(" >>>>>>>>>>>>>>>>>>>>>>>> showProfileContent <<<<<<<<<<<<<<<<<<<<<");
	
		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		DistrictMaster districtMaster=null;
		SchoolMaster schoolMaster=null;
		UserMaster userMaster=null;
		int entityID=0,roleId=0;
		boolean nobleFlag=false;
		boolean jeffcoFlag=false;
		if (session == null || session.getAttribute("userMaster") == null) {
			//return "false";
		}else{
			userMaster=(UserMaster)session.getAttribute("userMaster");
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
			if(userMaster.getDistrictId()!=null){
				districtMaster=userMaster.getDistrictId();
				if(districtMaster!=null)
					if(districtMaster.getDistrictId()==7800038)
						nobleFlag=true;
				
				//804800
				if(districtMaster.getDistrictId()==804800)
				jeffcoFlag=true;
			}
			if(userMaster.getSchoolId()!=null){
				schoolMaster =userMaster.getSchoolId();
			}	
			entityID=userMaster.getEntityType();
		}
		StringBuilder sb = new StringBuilder();
		
		boolean headQuarterMaster =false;
		if(userMaster!=null && userMaster.getHeadQuarterMaster()!=null){
			headQuarterMaster=true;
		}
		
		try 
		{
			String locale = Utility.getValueOfPropByKey("locale");
			String lblName  = Utility.getLocaleValuePropByKey("lblName", locale);
			System.out.println("lblName:::"+lblName);
			String lblNA   = Utility.getLocaleValuePropByKey("lblNA ", locale);
			String lblAname  = Utility.getLocaleValuePropByKey("lblAname", locale);
			String lblAdd  = Utility.getLocaleValuePropByKey("lblAdd", locale);
			String  lblVeteran   = Utility.getLocaleValuePropByKey("lblVeteran", locale);
			String lblSSN  = Utility.getLocaleValuePropByKey("lblSSN", locale);
			String lblYes  = Utility.getLocaleValuePropByKey("lblYes", locale);
			String lblNo  = Utility.getLocaleValuePropByKey("lblNo", locale);
			String lblEmpNum  = Utility.getLocaleValuePropByKey("lblEmpNum", locale);
			String lblEmpPosition  = Utility.getLocaleValuePropByKey("lblEmpPosition", locale);
			String lblFirstJobApplied   = Utility.getLocaleValuePropByKey("lblFirstJobApplied", locale);
			String lblLastJobApplied   = Utility.getLocaleValuePropByKey("lblLastJobApplied", locale);
			String lblLastContacted   = Utility.getLocaleValuePropByKey("lblLastContacted", locale);
			String lblJobApplied   = Utility.getLocaleValuePropByKey("lblJobApplied", locale);
			String lblNatiBoardCerti  = Utility.getLocaleValuePropByKey("lblNatiBoardCerti/Lice", locale);
			String lblY   = Utility.getLocaleValuePropByKey("lblY", locale);
			String lblN   = Utility.getLocaleValuePropByKey("lblN", locale);
			String lblFitScore  = Utility.getLocaleValuePropByKey("lblFitScore", locale);
			String lblTFA  = Utility.getLocaleValuePropByKey("lblTFA", locale);
			String btnSave  = Utility.getLocaleValuePropByKey("btnSave", locale);
			String btnClr  = Utility.getLocaleValuePropByKey("btnClr", locale);
			String lblEdit  = Utility.getLocaleValuePropByKey("lblEdit", locale);
			String lblCorpsYear  = Utility.getLocaleValuePropByKey("lblCorpsYear", locale);
			String lblTFARegion   = Utility.getLocaleValuePropByKey("lblTFARegion", locale);
			String lblDemoLesson  = Utility.getLocaleValuePropByKey("lblDemoLesson", locale);
			String lblTeachingYears  = Utility.getLocaleValuePropByKey("lblTeachingYears", locale);
			String lblExpectedSalary  = Utility.getLocaleValuePropByKey("lblExpectedSalary", locale);
			String lblOfViews   = Utility.getLocaleValuePropByKey("lblOfViews", locale);
			String lblClickToViewTPVisit   = Utility.getLocaleValuePropByKey("lblClickToViewTPVisit", locale);
			String lblWillingToSubsti  = Utility.getLocaleValuePropByKey("lblWillingToSubsti", locale);
			String lnkResume  = Utility.getLocaleValuePropByKey("lnkResume", locale);
			String lblNotAvailable   = Utility.getLocaleValuePropByKey("lblNotAvailable", locale);
			String headPdRep  = Utility.getLocaleValuePropByKey("headPdRep", locale);
			String lblCandidateNotCompletedJSI  = Utility.getLocaleValuePropByKey("lblCandidateNotCompletedJSI", locale);
			String lbllblCandidateTimedOut  = Utility.getLocaleValuePropByKey("lbllblCandidateTimedOut", locale);
			String lblMobileNumber   = Utility.getLocaleValuePropByKey("lblMobileNumber", locale);
			String lblAScore  = Utility.getLocaleValuePropByKey("lblAScore", locale);
			String lblLRScore  = Utility.getLocaleValuePropByKey("lblL/RScore", locale);
			String lblAcademic  = Utility.getLocaleValuePropByKey("lblAcademic", locale);
			String lblAchievement   = Utility.getLocaleValuePropByKey("lblAchievement", locale);
			String lblLeadershipResults   = Utility.getLocaleValuePropByKey("lblLeadershipResults", locale);
			String lblSetAScore   = Utility.getLocaleValuePropByKey("lblSetAScore", locale);
			String lblPNQ   = Utility.getLocaleValuePropByKey("lblPNQ", locale);
			String headCom  = Utility.getLocaleValuePropByKey("headCom", locale);
			String lblShare  = Utility.getLocaleValuePropByKey("lblShare", locale);
			String headTag  = Utility.getLocaleValuePropByKey("headTag", locale);
			String lblGeneralKnowledgeExam   = Utility.getLocaleValuePropByKey("lblGeneralKnowledgeExam", locale);
			String lblExamStatus   = Utility.getLocaleValuePropByKey("lblExamStatus", locale);
			String lblExamDate   = Utility.getLocaleValuePropByKey("lblExamDate", locale);
			String lblScoreReport   = Utility.getLocaleValuePropByKey("lblScoreReport", locale);
			String lblPass   = Utility.getLocaleValuePropByKey("lblPass", locale);
			String lblFail   = Utility.getLocaleValuePropByKey("lblFail", locale);
			String lblSubjectAreaExam   = Utility.getLocaleValuePropByKey("lblSubjectAreaExam", locale);
			String lblCerti  = Utility.getLocaleValuePropByKey("lblCerti/Lice", locale);
			String lblAcademics  = Utility.getLocaleValuePropByKey("lblAcademics", locale);
			String lblHonors  = Utility.getLocaleValuePropByKey("lblHonors", locale);
			String lblInvolvementVolunteerWork   = Utility.getLocaleValuePropByKey("lblInvolvementVolunteerWork ", locale);
			String lblWorkExp  = Utility.getLocaleValuePropByKey("lblWorkExp", locale);
			String lblRef  = Utility.getLocaleValuePropByKey("lblRef", locale);
			String lblMrs  = Utility.getLocaleValuePropByKey("lblMrs", locale);
			String lblMr  = Utility.getLocaleValuePropByKey("lblMr", locale);
			String lblMiss   = Utility.getLocaleValuePropByKey("lblMiss", locale);
			String lblDr   = Utility.getLocaleValuePropByKey("lblDr", locale);
			String lblMs  = Utility.getLocaleValuePropByKey("lblMs", locale);
			String lblFname  = Utility.getLocaleValuePropByKey("lblFname", locale);
			String lblLname  = Utility.getLocaleValuePropByKey("lblLname", locale);
			String lblTitle  = Utility.getLocaleValuePropByKey("lblTitle", locale);
			String lblOrganizationEmp   = Utility.getLocaleValuePropByKey("lblOrganizationEmp", locale);
			String lblContNum   = Utility.getLocaleValuePropByKey("lblContNum", locale);
			String lblEmail  = Utility.getLocaleValuePropByKey("lblEmail", locale);
			String lblRecommendLetter   = Utility.getLocaleValuePropByKey("lblRecommendLetter", locale);
			String lnkClear  = Utility.getLocaleValuePropByKey("lnkClear", locale);
			String lnkRemo  = Utility.getLocaleValuePropByKey("lnkRemo", locale);
			String lblCanThisPerson   = Utility.getLocaleValuePropByKey("lblCanThisPerson ", locale);
			String lblHowLong   = Utility.getLocaleValuePropByKey("lblHowLong", locale);
			String lblReferenceDetails   = Utility.getLocaleValuePropByKey("lblReferenceDetails", locale);
			String lnkViLnk  = Utility.getLocaleValuePropByKey("lnkViLnk", locale);
			String lblVideo  = Utility.getLocaleValuePropByKey("lblVideo", locale);
			String lnkAddDocu  = Utility.getLocaleValuePropByKey("lnkAddDocu", locale);
			String lblPleaseInclude   = Utility.getLocaleValuePropByKey("lblPleaseInclude", locale);
			String lblSupportedVideoFormats   = Utility.getLocaleValuePropByKey("lblSupportedVideoFormats", locale);
			String lblLanguageProfiency   = Utility.getLocaleValuePropByKey("lblLanguageProfiency", locale);
			String lblAssessmentDetails   = Utility.getLocaleValuePropByKey("lblAssessmentDetails", locale);
			String headMngDistSpeciQues  = Utility.getLocaleValuePropByKey("headMngDistSpeciQues", locale);
			String lblSaveQuestion   = Utility.getLocaleValuePropByKey("lblSaveQuestion", locale);
			String lblNone  = Utility.getLocaleValuePropByKey("lblNone", locale);
			String lblEPINorm  = Utility.getLocaleValuePropByKey("lblEPINorm", locale);
			String lblJSI  = Utility.getLocaleValuePropByKey("lblJSI", locale);
			String lblPhone  = Utility.getLocaleValuePropByKey("lblPhone", locale);
			String lnkAddViLnk  = Utility.getLocaleValuePropByKey("lnkAddViLnk", locale);
			String lblPreviousEmployed = Utility.getLocaleValuePropByKey("lblPreviousEmployed", locale);
			
			
			
			//int commDivFlag=0;
			
			String realPath = Utility.getValueOfPropByKey("teacherRootPath");
			String webPath = Utility.getBaseURL(request);
			String sWidthFoeDiv="770";
			String sWidthFoeDivScroll="765";
			/*sb.append("<div style='width:"+sWidthFoeDiv+"px;'>"+
			"<div class='modal_header_profile'>"+
	  		"<button type='button' class='close' onclick='showProfileContentClose();'>x</button>"+
	  		"<h3>Teacher Profile</h3>"+
			"</div>"+
			"<div data-spy='scroll' data-offset='50' class='scrollspy_profile' style='width:"+sWidthFoeDiv+"px;'>");
				  */  		        	
				
			
			String imgPath="";
			TeacherDetail teacherDetail = null;
			try{
				teacherDetail = teacherDetailDAO.findById(teacherId,false,false);
				if(teacherDetail.getIdentityVerificationPicture()!=null){
					imgPath=CommonDashboardAjax.showPictureWeb(teacherDetail, realPath, webPath);
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			
			/*if(!teacherDetail.getFirstName().equals("")){
				sb.append("<div style='width:"+sWidthFoeDiv+"px;cursor: default;color:black;'>"+
						"<div class='modal_header_profile' style='margin-left:-2px; margin-top:-4px;'>"+
						"<button type='button' class='close' onclick='showProfileContentClose();'>x</button>"+
				  		"<h3>"+teacherDetail.getFirstName()+" "+teacherDetail.getLastName()+"'s Profile</h3>"+
						"</div>"+
						"<div data-spy='scroll' data-offset='50' class='scrollspy_profile' style='width:"+sWidthFoeDiv+"px;'>");
			}*/
			
			
			/*

			*Start

			*Ashish Kumar

			*Description :: display teacher name 

			**/
			String width="798px";
			
			//if(tfaEditAuthUser())
			    if(userMaster.getEntityType().equals(2) || userMaster.getRoleId().getRoleId().equals(3))
				  width ="900px";
				

			if(!teacherDetail.getFirstName().equals("")){

			sb.append("<div class='modal-dialog-for-cgprofile' style='width: "+width+"'>"+
					
			"<div class='modal-content modal-border'>"+		

			"<div class='modal-header modal_header_profile' id='cgTeacherDivHeader'> "+

			"<button type='button' class='close' onclick='showProfileContentClose();'>x</button>"+

			"<h3>"+teacherDetail.getFirstName()+" "+teacherDetail.getLastName()+"'s Profile</h3>"+

			"</div>"+

			"<div class='modal-body scrollspy_profile' style='min-width:"+sWidthFoeDivScroll+"px;'>");

			}
			/*
			*End

			*Ashish Kumar

			*Description :: display teacher name 

			**/
			
			String showImg="";
			/*if(!imgPath.equals("")){
				showImg="<img src=\""+imgPath+"\" align='left' class='circle' style='width: 48px;height: 48px;margin: 0em auto;'>";
			}else{
				showImg="<img src=\"images/nophoto.png\" align='left' class='circle' style='width: 48px;height: 48px;margin: 0em auto;' >";
			}*/
			
			showImg="<img src=\"images/nophoto.png\" align='left' class='circle' style='width: 48px;height: 48px;margin: 0em auto;' >";
			
			//List<JobForTeacher>  jobForTeachers = jobForTeacherDAO.findJobByTeacher(teacherDetail);
			/*Criterion criterion_teacherId = Restrictions.eq("teacherId", teacherDetail);
			List<JobForTeacher>  jobForTeachers=new ArrayList<JobForTeacher>();
			int entityType=userMaster.getEntityType();
			if(entityType==2)
			{
				Criterion criterion2 = Restrictions.eq("districtMaster", userMaster.getDistrictId());
				List<JobOrder> jobOrders=jobOrderDAO.findByCriteria(criterion2);
				if(jobOrders.size() > 0)
				{
					Criterion criterion3 = Restrictions.in("jobId", jobOrders);
					jobForTeachers = jobForTeacherDAO.findByCriteria(criterion_teacherId,criterion3);
				}
			}
			else if(entityType==3)
			{
				System.out.println("logged In SA "+userMaster.getSchoolId().getSchoolId());
				List<JobOrder> jobOrders=new ArrayList<JobOrder>();
				List<SchoolInJobOrder> inJobOrders=new ArrayList<SchoolInJobOrder>();
				if(userMaster.getSchoolId()!=null)
					inJobOrders=schoolInJobOrderDAO.findJobBySchool(userMaster.getSchoolId());
				if(inJobOrders.size() > 0)
				{
					for(SchoolInJobOrder inJobOrder:inJobOrders)
					{
						jobOrders.add(inJobOrder.getJobId());
						System.out.println("SA School JobId "+inJobOrder.getJobId().getJobId());
					}
				}
				
				if(jobOrders.size() > 0)
				{
					Criterion criterion3 = Restrictions.in("jobId", jobOrders);
					jobForTeachers = jobForTeacherDAO.findByCriteria(criterion_teacherId,criterion3);
				}
			}
			else
				jobForTeachers = jobForTeacherDAO.findJobByTeacher(teacherDetail);*/
			
			JobOrder jobOrder = jobOrderDAO.findById(new Integer(jobId), false, false);
			
			List<JobForTeacher>  jobForTeachers=new ArrayList<JobForTeacher>();
			jobForTeachers=getJFT(teacherDetail, userMaster,"CGView");
			
			JobForTeacher jobForTeacher = null;
			String firstAppliedOn="";
			String lastAppliedOn="";
			Date lastActivityDate=null;
			String lastContactedOn=lblNone;
			if(jobForTeachers.size()>0)
			{
				jobForTeacher = jobForTeachers.get(0);
				firstAppliedOn = Utility.convertDateAndTimeToUSformatOnlyDate(jobForTeacher.getCreatedDateTime());
				jobForTeacher = jobForTeachers.get(jobForTeachers.size()-1);
				lastAppliedOn = Utility.convertDateAndTimeToUSformatOnlyDate(jobForTeacher.getCreatedDateTime());

				//Collections.reverse(jobForTeachers);
				//Collections.sort(jobForTeachers,JobForTeacher.jobForTeacherComparatorForLastActivityDateDesc);
				List<JobForTeacher>  jobForTeachersLastContactedOn=new ArrayList<JobForTeacher>();
				jobForTeachersLastContactedOn=getJFTLastContactedOn(teacherDetail, userMaster, jobOrder,"CGView");
				
				for (JobForTeacher jobForTeacher2 : jobForTeachersLastContactedOn) {
					lastActivityDate = jobForTeacher2.getLastActivityDate();
					if(userMaster.getEntityType()==2)
					{	
					if(lastActivityDate!=null && jobForTeacher2.getUserMaster()!=null && (districtMaster.getDisplayCommunication()==null || districtMaster.getDisplayCommunication()==false))
					{
						lastContactedOn ="<a href='javascript:void(0);'  onclick=\"getCommunicationsDiv(0,"+jobForTeacher.getJobForTeacherId()+",'"+teacherDetail.getTeacherId()+"',"+jobOrder.getJobId()+");\" ><span class='divlablelinkval'>"+Utility.convertDateAndTimeToUSformatOnlyDate(lastActivityDate)+" ("+jobForTeacher2.getUserMaster().getFirstName()+" "+jobForTeacher2.getUserMaster().getLastName()+")</span></a>"; 
						break;
					}else if(lastActivityDate!=null && jobForTeacher2.getUserMaster()!=null && (districtMaster.getDisplayCommunication()==null || districtMaster.getDisplayCommunication()==true))
					{
						lastContactedOn ="<a href='javascript:void(0);'  onclick=\"getCommunicationsDiv(0,"+jobForTeacher.getJobForTeacherId()+",'"+teacherDetail.getTeacherId()+"',0);\" ><span class='divlablelinkval'>"+Utility.convertDateAndTimeToUSformatOnlyDate(lastActivityDate)+" ("+jobForTeacher2.getUserMaster().getFirstName()+" "+jobForTeacher2.getUserMaster().getLastName()+")</span></a>"; 
						break;
					}
					}else if(lastActivityDate!=null && jobForTeacher2.getUserMaster()!=null)
					{
						lastContactedOn ="<a href='javascript:void(0);'  onclick=\"getCommunicationsDiv(0,"+jobForTeacher.getJobForTeacherId()+",'"+teacherDetail.getTeacherId()+"',"+jobOrder.getJobId()+");\" ><span class='divlablelinkval'>"+Utility.convertDateAndTimeToUSformatOnlyDate(lastActivityDate)+" ("+jobForTeacher2.getUserMaster().getFirstName()+" "+jobForTeacher2.getUserMaster().getLastName()+")</span></a>"; 
						break;
					}
					
					/*if(lastActivityDate!=null && userMaster.getEntityType()==2 && jobForTeacher2.getJobId().getCreatedForEntity()==2)
					{
						lastContactedOn ="<a href='javascript:void(0);'  onclick=\"getCommunicationsDiv(0,"+jobForTeacher.getJobForTeacherId()+",'"+teacherDetail.getTeacherId()+"',"+jobForTeacher.getJobId().getJobId()+");\" ><span class='divlablelinkval'>"+Utility.convertDateAndTimeToUSformatOnlyDate(lastActivityDate)+" ("+jobForTeacher2.getUserMaster().getFirstName()+" "+jobForTeacher2.getUserMaster().getLastName()+")</span></a>"; 
						break;
					}
					else if(lastActivityDate!=null && ( userMaster.getEntityType()==1 || userMaster.getEntityType()==3))
					{
						lastContactedOn ="<a href='javascript:void(0);'  onclick=\"getCommunicationsDiv(0,"+jobForTeacher.getJobForTeacherId()+",'"+teacherDetail.getTeacherId()+"',"+jobForTeacher.getJobId().getJobId()+");\" ><span class='divlablelinkval'>"+Utility.convertDateAndTimeToUSformatOnlyDate(lastActivityDate)+" ("+jobForTeacher2.getUserMaster().getFirstName()+" "+jobForTeacher2.getUserMaster().getLastName()+")</span></a>"; 
						break;
					}*/
				}
			} 
			
			SortedMap<Long,JobForTeacher> jftMap = new TreeMap<Long,JobForTeacher>();
			SortedMap<Long,JobForTeacher> jftMapForTm = new TreeMap<Long,JobForTeacher>();
			List<JobForTeacher> jobForTeachersList	  =	 new ArrayList<JobForTeacher>();
			List<JobOrder> lstJobOrderSLList =new ArrayList<JobOrder>();
			if(entityID==3){
				lstJobOrderSLList =schoolInJobOrderDAO.jobOrderPostedBySchool(schoolMaster);
			}
			List<TeacherDetail> lstTeacherDetails	  =	 new ArrayList<TeacherDetail>();
			lstTeacherDetails.add(teacherDetail);
			List<Integer> statusIdList = new ArrayList<Integer>();
			String[] statusShrotName =  {"comp","icomp","vlt","hird","scomp","ecomp","vcomp","dcln","rem","widrw"};
			try{
				statusIdList = statusMasterDAO.findStatusIdsByStatusByShortNames(statusShrotName);
			}catch (Exception e) {
				e.printStackTrace();
			}
			jobForTeachersList=jobForTeacherDAO.getJobForTeacherByDSTeachersList(statusIdList,lstTeacherDetails,lstJobOrderSLList,null,null,districtMaster,entityID);
			for(JobForTeacher jobForTeacherObj : jobForTeachersList){
				jftMap.put(jobForTeacherObj.getJobForTeacherId(),jobForTeacherObj);
			}
			jobForTeachersList=jobForTeacherDAO.getJobForTeacherByDSTeachersList(statusIdList,lstTeacherDetails,lstJobOrderSLList,null,null,districtMaster,1);
			for(JobForTeacher jobForTeacherObj : jobForTeachersList){
				jftMapForTm.put(jobForTeacherObj.getJobForTeacherId(),jobForTeacherObj);
			}
			
			int jobList=jobListByTeacher(teacherDetail.getTeacherId(),jftMap);
			int jobListForTm=jobListByTeacher(teacherDetail.getTeacherId(),jftMapForTm);
			
			CandidateGridService cgService=new CandidateGridService();
			
			DistrictPortfolioConfig districtPortfolioConfig=null;
			/*if(districtMaster!=null)
			{
				List<DistrictPortfolioConfig> lstdistrictPortfolioConfig = districtPortfolioConfigDAO.getPortfolioConfigs(districtMaster);
				if(lstdistrictPortfolioConfig!=null)
					districtPortfolioConfig = lstdistrictPortfolioConfig.get(0);
			}*/
			
			String roleAccess=null;
			try{
				roleAccess = roleAccessPermissionDAO.getMenuOptionList(roleId,38,"candidatereport.do",0);
			}
			catch(Exception e){
				e.printStackTrace();
			}
			
			boolean achievementScore=false,tFA=false,demoClass=false,JSI=false,teachingOfYear =false,expectedSalary=false,fitScore=false,pnqDis=false;
			
			//JobOrder jobOrder = jobOrderDAO.findById(new Integer(jobId), false, false);
			
			// Ramesh :: Start Insert teacherprofilevisithistory
			System.out.println("Start teacherprofilevisithistory");
			TeacherProfileVisitHistory teacherProfileVisitHistory=new TeacherProfileVisitHistory();
	      
			teacherProfileVisitHistory.setUserMaster(userMaster);

			teacherProfileVisitHistory.setTeacherDetail(teacherDetail);

			teacherProfileVisitHistory.setJobOrder(jobOrder);
			
			teacherProfileVisitHistory.setVisitLocation("CG View");
			
			teacherProfileVisitHistory.setVisitedDateTime(new Date());
			//System.out.println("ip==="+request.getRemoteAddr());
			teacherProfileVisitHistory.setIpAddress(IPAddressUtility.getIpAddress(request));
			
			if(userMaster.getDistrictId()!=null)
				teacherProfileVisitHistory.setDistrictId(userMaster.getDistrictId());
			if(userMaster.getSchoolId()!=null)
				teacherProfileVisitHistory.setSchoolId(userMaster.getSchoolId());		
			
		    try
		    {
			teacherProfileVisitHistoryDAO.makePersistent(teacherProfileVisitHistory);	
		    }
		    catch(Exception e)
		    {
		    }
			// End Insert teacherprofilevisithistory		
			
			
			try{
				DistrictMaster districtMasterObj=jobOrder.getDistrictMaster();
				if(districtMasterObj!=null && districtMasterObj.getDisplayAchievementScore()!=null && districtMasterObj.getDisplayAchievementScore()){
					achievementScore=true;
				}
				if(districtMasterObj!=null && districtMasterObj.getDisplayTFA()!=null && districtMasterObj.getDisplayTFA()){
					tFA=true;
				}
				if(districtMasterObj!=null && districtMasterObj.getDisplayDemoClass()!=null && districtMasterObj.getDisplayDemoClass()){
					demoClass=true;
				}
				if(districtMasterObj!=null && districtMasterObj.getDisplayJSI()!=null && districtMasterObj.getDisplayJSI()){
					JSI=true;
				}
				if(districtMasterObj!=null && districtMasterObj.getDisplayYearsTeaching()!=null && districtMasterObj.getDisplayYearsTeaching()){
					teachingOfYear=true;
				}
				if(districtMasterObj!=null && districtMasterObj.getDisplayExpectedSalary()!=null && districtMasterObj.getDisplayExpectedSalary()){
					expectedSalary=true;
				}
				if(districtMasterObj!=null && districtMasterObj.getDisplayFitScore()!=null && districtMasterObj.getDisplayFitScore()){
					fitScore=true;
				}
				if(districtMasterObj!=null && districtMasterObj.getDistrictId()!=null && districtMasterObj.getDistrictId().equals(7800038)){
					pnqDis=true;
				}
			}
			catch(Exception e)
			{ 
				e.printStackTrace();
			}
			
			String windowFunc="if(this.href!='javascript:void(0);')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
			
			//List<TeacherAcademics> lstTeacherAcademics_new = teacherAcademicsDAO.findAcademicsByTeacher(lstTeacherDetails);
			//List<TeacherCertificate> lstTeacherCertificate_new = teacherCertificateDAO.findCertificateByTeacher(lstTeacherDetails);
			//List<SecondaryStatusMaster>  lstSecondaryStatusMaster_new = secondaryStatusMasterDAO.findActiveSecStaus();
			List<TeacherExperience> lstTeacherExperience_new=teacherExperienceDAO.findExperienceForTeachers(lstTeacherDetails);
			List<TeacherPersonalInfo> lstTeacherPersonalInfo_new=teacherPersonalInfoDAO.findTeacherPInfoForTeacher(lstTeacherDetails);
			List<DemoClassSchedule> lstDemoClassSchedule_new=demoClassScheduleDAO.findDemoClassSchedules(lstTeacherDetails,jobOrder);
			List<TeacherAchievementScore> lstTeacherAchievementScore_new=teacherAchievementScoreDAO.getAchievementScore(lstTeacherDetails,jobOrder);
			List<JobWiseConsolidatedTeacherScore> lstJobWiseConsolidatedTeacherScore_new=jobWiseConsolidatedTeacherScoreDAO.getFitScore(lstTeacherDetails,jobOrder);	
			
			//Map<Integer,TeacherAssessmentStatus>  mapActiveJSIStatus_new = teacherAssessmentStatusDAO.findActiveJSIStatus(lstTeacherDetails,jobOrder);
			TeacherAssessmentStatus teacherAssessmentStatusJSI_new = null;
			TeacherPersonalInfo teacherPersonalInfo = teacherPersonalInfoDAO.findById(lstTeacherDetails.get(0).getTeacherId(), false, false);
			
			Map<Integer, TeacherAssessmentStatus> mapJSI = new HashMap<Integer, TeacherAssessmentStatus>();
			List<TeacherAssessmentStatus> lstJSI =new ArrayList<TeacherAssessmentStatus>();
			List<AssessmentJobRelation> assessmentJobRelations1=assessmentJobRelationDAO.findRelationByJobOrderNoDate(jobOrder);
			if(assessmentJobRelations1.size()>0 && lstTeacherDetails.size()>0){
				AssessmentJobRelation assessmentJobRelation1 = assessmentJobRelations1.get(0);
				if(assessmentJobRelation1.getAssessmentId()!=null)
					lstJSI = teacherAssessmentStatusDAO.findAssessmentTakenByTeacherList(lstTeacherDetails,assessmentJobRelation1.getAssessmentId());
			}
			
			for(TeacherAssessmentStatus ts : lstJSI){
				mapJSI.put(ts.getTeacherDetail().getTeacherId(), ts);
			}
			
			
			// New setting Start
			sb.append("<div id='errorDiv' style='color:red;padding-left:60px;display:none;'></div>");
			sb.append("<table id='profile_id' min-width="+sWidthFoeDiv+" cellspacing=0 cellpadding=0 class='tablecgtbl'>");
			
				/*sb.append("<tr>");
					sb.append("<td>");
						sb.append("<button type='button' class='close' onclick='showProfileContentClose();'>x</button>");
					sb.append("</td>");
				sb.append("</tr>");*/
			
			
				sb.append("<tr>");
					sb.append("<td style='padding-top:10px;'>");
					
						sb.append("<table border='0' cellspacing=0 cellpadding=0 class='tablecgtbl'>");
							sb.append("<tr>");
								String strCss_lbl="padding:2px;";
								String strCss_value="width=42px; vertical-algin:top;text-align:left;padding:0px;padding-left:5px;padding-right:5px;padding-top:5px;";
								sb.append("<td>"); // Photo
									sb.append("<table border='0' cellspacing=0 cellpadding=0 class='tablecgtbl'>");
										sb.append("<tr><td style='"+strCss_value+"'>"+showImg+"</td></tr>");
									sb.append("</table>");
								sb.append("</td>");
								
								sb.append("<td>");
									sb.append("<table border='0' cellspacing=0 cellpadding=0 class='tablecgtbl'>");
										strCss_lbl="padding:2px;white-space:nowrap";
										strCss_value="padding:2px;white-space:nowrap";
										//Name:

										sb.append("<tr><td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblName+":</td><td class='divlableval' style='"+strCss_value+"'>&nbsp;"+teacherDetail.getFirstName()+" "+teacherDetail.getLastName()+"</td></tr>");
																				
										sb.append("<input type='hidden' id='teacherNameInPrf' value='"+teacherDetail.getFirstName()+" "+teacherDetail.getLastName()+"'>");

										if(userMaster.getDistrictId()!=null && userMaster.getDistrictId().getDistrictId()!=null && userMaster.getDistrictId().getDistrictId()==1201470)	//OSCEOLA
											if(teacherPersonalInfo!=null)
												if(teacherPersonalInfo.getAnotherName()!=null)
													sb.append("<tr><td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblAname+":</td><td class='divlableval' style='"+strCss_value+"'>&nbsp;"+teacherPersonalInfo.getAnotherName()+"</td></tr>");
												else
													sb.append("<tr><td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblAname+":</td><td class='divlableval' style='"+strCss_value+"'>&nbsp;</td></tr>");
										
										if(userMaster.getDistrictId()!=null && userMaster.getDistrictId().getDistrictId()!=null && userMaster.getDistrictId().getDistrictId()==7800294)	//WAUKESHA 
											if(teacherPersonalInfo!=null)
												if(teacherPersonalInfo.getDistrictEmail()!=null)
													sb.append("<tr><td class='divlable' style='"+strCss_lbl+"'>&nbsp;District Email Address:</td><td class='divlableval' style='"+strCss_value+"'>&nbsp;"+teacherPersonalInfo.getDistrictEmail()+"</td></tr>");
												else
													sb.append("<tr><td class='divlable' style='"+strCss_lbl+"'>&nbsp;District Email Address:</td><td class='divlableval' style='"+strCss_value+"'>&nbsp;</td></tr>");
										
																				
										/* @Start
										 * @Ashish Kumar
										 * @Description :: Add Address of Teacher for CG
										 * */
											
										String sEmployeeNumber=lblNA;
										String sEmployeePosition=lblNA;
										if(teacherPersonalInfo!=null && teacherPersonalInfo.getEmployeeNumber()!=null && !teacherPersonalInfo.getEmployeeNumber().equalsIgnoreCase(""))
											sEmployeeNumber=teacherPersonalInfo.getEmployeeNumber();
										if(teacherPersonalInfo!=null && teacherPersonalInfo.getLastPositionWhenEmployed()!=null && !teacherPersonalInfo.getLastPositionWhenEmployed().equalsIgnoreCase(""))
											sEmployeePosition=teacherPersonalInfo.getLastPositionWhenEmployed();
										PrintOnConsole.debugPrintln("Teacher Profile1", "sEmployeeNumber "+sEmployeeNumber);
										PrintOnConsole.debugPrintln("Teacher Profile1", "sEmployeePosition "+sEmployeePosition);
										
										
										if(teacherPersonalInfo!=null)
										{
												String stateName = null;
												String country=null;
												String cityName = null;
												String zip = null;
												boolean addCHK = false;
												
												if(lstTeacherPersonalInfo_new.get(0).getStateId()!=null){
													stateName = lstTeacherPersonalInfo_new.get(0).getStateId().getStateName()+", ";
													addCHK = true;
												}else{
													stateName = "";
												}
												
												if(lstTeacherPersonalInfo_new.get(0).getCountryId()!=null){
													country=lstTeacherPersonalInfo_new.get(0).getCountryId().getName();
												}
												if(lstTeacherPersonalInfo_new.get(0).getCityId()!=null)
												{
													cityName = lstTeacherPersonalInfo_new.get(0).getCityId().getCityName()+", ";
													addCHK = true;
												}else{
													cityName = "";
												}
												
												if(lstTeacherPersonalInfo_new.get(0).getZipCode()!=null && !lstTeacherPersonalInfo_new.get(0).getZipCode().equals(""))
												{
													zip = lstTeacherPersonalInfo_new.get(0).getZipCode();
													addCHK = true;
												}else{
													zip = "";
												}
												
												
												if(lstTeacherPersonalInfo_new.get(0).getAddressLine1()!=null && !lstTeacherPersonalInfo_new.get(0).getAddressLine1().equals(""))
												{
													if(stateName=="" && cityName =="" && zip=="" && lstTeacherPersonalInfo_new.get(0).getAddressLine2().equals(""))
													{
														sb.append("<tr><td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblAdd+":</td><td class='divlableval' style='"+strCss_value+"'>&nbsp;"+lstTeacherPersonalInfo_new.get(0).getAddressLine1()+"</td></tr>");
													}
													else
													{
														sb.append("<tr><td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblAdd+":</td><td class='divlableval' style='"+strCss_value+"'>&nbsp;"+lstTeacherPersonalInfo_new.get(0).getAddressLine1()+",</td></tr>");
													}
													
													addCHK = true;
												}
												
												if(lstTeacherPersonalInfo_new.get(0).getAddressLine2()!=null && !lstTeacherPersonalInfo_new.get(0).getAddressLine2().equalsIgnoreCase(""))
												{
													if(stateName=="" && cityName =="" && zip=="")
													{
														sb.append("<tr><th class='divlable' style='"+strCss_lbl+"'>&nbsp;</th><th class='divlableval' style='"+strCss_value+"'>&nbsp;"+lstTeacherPersonalInfo_new.get(0).getAddressLine2()+"</th></tr>");
													}
													else
													{
														sb.append("<tr><th class='divlable' style='"+strCss_lbl+"'>&nbsp;</th><th class='divlableval' style='"+strCss_value+"'>&nbsp;"+lstTeacherPersonalInfo_new.get(0).getAddressLine2()+",</th></tr>");
													}
													addCHK = true;
												}
												
												
												
												if(addCHK==false)
												{
													sb.append("<tr><th class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblAdd+":</th><th class='divlableval' style='"+strCss_value+"'>&nbsp;"+lblNA+"</th></tr>");
												}
												else
												{
													sb.append("<tr><th class='divlable' style='"+strCss_lbl+"'>&nbsp;</th><th class='divlableval' style='"+strCss_value+"'>&nbsp;"+stateName+""+cityName+""+zip+"</th></tr>");
													
													if(country!=""){
														sb.append("<tr><th class='divlable' style='"+strCss_lbl+"'>&nbsp;</th><th class='divlableval' style='"+strCss_value+"'>&nbsp;"+country+"</th></tr>");	
													}
												}
											}
										/* @End
										 * @Ashish Kumar
										 * @Description :: Add Address of Teacher for CG
										 * */
										
										
										
										//EPI Norm
										
										if(normScore!=null && !normScore.equals(""))
										{
											sb.append("<tr><td class='divlable' style='"+strCss_lbl+";padding-top:10px;'>&nbsp;"+lblEPINorm+":</td><td class='divlableval' style='"+strCss_value+";padding-top:10px;padding-bottom:8px;'>&nbsp;");
											
											String ccsName="";
											if(normScore.length()==1){
												ccsName="nobground1";
											}else if(normScore.length()==2){
												ccsName="nobground2";
											}else{
												ccsName="nobground3";
											}
											
											if(teacherDetail!=null)
											{
												List<TeacherDetail> teacherDetails = new ArrayList<TeacherDetail>();
												teacherDetails.add(teacherDetail);
												List<TeacherNormScore> lstTeacherNormScores = teacherNormScoreDAO.findTeacersNormScoresList(teacherDetails);
												if(lstTeacherNormScores.size()>0)
												{
													colorName = lstTeacherNormScores.get(0).getDecileColor();
													normScore = lstTeacherNormScores.get(0).getTeacherNormScore()+"";
												}
											}
											
											sb.append("<span class=\""+ccsName+"\" style='font-size: 11px;font-weight: bold;background-color: #"+colorName+";'>"+normScore+"</span>");
											
											sb.append("</td></tr>");
										}	
										
									    //Veteran candidate
										
										 //sssnnnnnnnnnn in cgg
										if(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==NC_HEADQUARTER){
											if(userMaster.getEntityType()!=3)
											{
						                  sb.append("<tr>");
						                  sb.append("<th class='divlable' style='"+strCss_lbl+"'>&nbsp;Last 4 of SSN:</th>");
						                  
						                  if(teacherPersonalInfo!=null)
						                  if(teacherPersonalInfo.getSSN()!=null && !teacherPersonalInfo.getSSN().equals("")){
						                   String ssnFour=Utility.decodeBase64(teacherPersonalInfo.getSSN());
						                   String finalSSN="";
						                   try
						                   {
						                    finalSSN=ssnFour.substring(5, 9);
						                   System.out.println("Last four digit of ssn:"+finalSSN);
						                   }catch(Exception e){}
						                    sb.append("<th  class='divlableval' style='"+strCss_value+"'>&nbsp;"+finalSSN+"</th>");
						                   }else{
						                    sb.append("<th  class='divlableval' style='"+strCss_value+"'>&nbsp;N/A</th>");
						                   }
						                  
						                  sb.append("</tr>");
										}
										}
										else
										{
									   try{
										   
										   if(userMaster.getDistrictId()!=null && !userMaster.getDistrictId().getDistrictId().equals(804800) && !userMaster.getDistrictId().getDistrictId().equals(7800294))
											   if(teacherPersonalInfo!=null){
													if(teacherPersonalInfo.getIsVateran()!=null && teacherPersonalInfo.getIsVateran()==1){
														sb.append("<tr><td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblVeteran+":</td><td class='divlableval' style='"+strCss_value+"'>&nbsp;"+lblYes+"</td></tr>");
													}
													if(userMaster.getRoleId().getRoleId()==2  ||userMaster.getRoleId().getRoleId()==7 || userMaster.getRoleId().getRoleId()==8){
														if(teacherPersonalInfo.getSSN()!=null && !teacherPersonalInfo.getSSN().equals("")){
															if(userMaster.getEntityType()!=3)
															sb.append("<tr><td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblSSN+"</td><td class='divlableval' style='"+strCss_value+"'>&nbsp;"+Utility.decodeBase64(teacherPersonalInfo.getSSN())+"</td></tr>");
														}else{
															if(userMaster.getEntityType()!=3)
															sb.append("<tr><td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblSSN+"</td><td class='divlableval' style='"+strCss_value+"'>&nbsp;"+lblNA+"</td></tr>");
														}
													}	
												}else{
													sb.append("<tr><td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblVeteran+":</td><td class='divlableval' style='"+strCss_value+"'>&nbsp;"+lblNo+"</td></tr>");
													if(userMaster.getRoleId().getRoleId()==2 ||userMaster.getRoleId().getRoleId()==7 || userMaster.getRoleId().getRoleId()==8){
														sb.append("<tr><td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblSSN+":</td><td class='divlableval' style='"+strCss_value+"'>&nbsp;"+lblNA+"</td></tr>");
													}
												}
										}catch (Exception e) {
											e.printStackTrace();
										}
										}
										
										//EmployeeNumber
										if(jeffcoFlag && !sEmployeeNumber.trim().equalsIgnoreCase("N/A"))
											sb.append("<tr><td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblEmpNum+":</td><td class='divlableval' style='"+strCss_value+"'>&nbsp;<a class='tooltipEmpNo' data-original-title='Employee Information' href='javascript:void(0);' onclick='openEmployeeNumberInfo(\""+sEmployeeNumber+"\")'>"+sEmployeeNumber+"</a></td></tr>");
										else
											if(!sEmployeeNumber.trim().equalsIgnoreCase("N/A")&& locale.equals("fr"))
												sb.append("<tr><td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblEmpNum+":</td><td class='divlableval' style='"+strCss_value+"'>&nbsp;<a class='tooltipEmpNo' data-original-title='Employee Information' href='javascript:void(0);' onclick='openEmployeeNumberInfo(\""+sEmployeeNumber+"\")'>"+sEmployeeNumber+"</a></td></tr>");
											else
											sb.append("<tr><td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblEmpNum+":</td><td class='divlableval' style='"+strCss_value+"'>&nbsp;"+sEmployeeNumber+"</td></tr>");
										//Employee Position
										if(jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId().equals(7800294))
											sb.append("<tr><td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblEmpPosition+":</td><td class='divlableval' style='"+strCss_value+"'>&nbsp;"+sEmployeePosition+"</td></tr>");
										
									   //First Job Applied On
										sb.append("<tr><td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblFirstJobApplied +":</td><td class='divlableval' style='"+strCss_value+"'>&nbsp;"+firstAppliedOn+"</td></tr>");
										//Last Job Applied On
										sb.append("<tr><td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblLastJobApplied  +":</td><td class='divlableval' style='"+strCss_value+"'>&nbsp;"+lastAppliedOn+"</td></tr>");
										//Last Contacted On
										sb.append("<tr><td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblLastContacted  +":</td><td class='divlableval' style='"+strCss_value+"'>&nbsp;"+lastContactedOn+"</td></tr>");
										//Job Applied
										
										if(jobList>0)
											sb.append("<tr><td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblJobApplied +":</td><td class='divlableval' style='"+strCss_value+"'>&nbsp;<a href='javascript:void(0);' onclick=\"getJobList('"+teacherDetail.getTeacherId()+"')\" >"+jobList+"</a>"+"/"+(jobListForTm-jobList)+"</td></tr>");
										else
											sb.append("<tr><td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblJobApplied +":</td><td class='divlableval' style='"+strCss_value+"'>&nbsp;"+jobList+"/"+(jobListForTm-jobList)+"<td></tr>");
										
										
										//National Board Certification for ncdpi
										TeacherExperience teacherExperience = null;
										
										if(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==NC_HEADQUARTER)
										{
											System.out.println("inside headquatermaster");
											
											List<TeacherPersonalInfo> ssnTeachersList = new ArrayList<TeacherPersonalInfo>();
											ArrayList<Integer> teacherIds = new ArrayList<Integer>();
											List<String> ssnList = new ArrayList<String>();
											List<LicensureNBPTS> licensureNBPTSList = new ArrayList<LicensureNBPTS>();
											
											teacherIds.add(teacherDetail.getTeacherId());
											if(teacherIds!=null && teacherIds.size()>0)
											{
												ssnTeachersList = teacherPersonalInfoDAO.findByTeacherIds(teacherIds);
												
											}
											
											if(ssnTeachersList!=null)
											{
												System.out.println("inside ssnTeachersList");
											  for(TeacherPersonalInfo tpi:ssnTeachersList){
												if(tpi.getSSN()!=null && !tpi.getSSN().equals("")){
													System.out.println("ssssnnnn"+tpi.getSSN());
													ssnList.add(tpi.getSSN());
													System.out.println("ssnList:::::::"+ssnList);
													
												}
												//System.out.println("ssnList sizeeeeeeeee::"+ssnList.size()+"ssnnnnnnnnnnnnnnnnnnn::::::"+ssnList);
											}
											}
																					
											if(ssnList!=null && ssnList.size()>0){
												
												licensureNBPTSList = licensureNBPTSDao.getDataBySsnList(ssnList);
											}
											if(licensureNBPTSList!=null && licensureNBPTSList.size()>0)
											{
												
												Format formatter = new SimpleDateFormat("yyyy-MM-dd");
												String fullDate="";
												String finalYear="";
												if(licensureNBPTSList.get(0).getExpirationDate()!=null)
												 fullDate = formatter.format(licensureNBPTSList.get(0).getExpirationDate());
												
												if(!fullDate.equals(""))
												finalYear=fullDate.substring(0, 4);
												
												System.out.println("NBTS  cetri::::::::::YES");
												sb.append("<td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblNatiBoardCerti+":</td>");
												sb.append("<td  class='divlableval' style='"+strCss_value+"'>&nbsp;"+lblY+" ("+finalYear+")</td>");
												
											}
											else
											{
												System.out.println("NBTS  cetri::::::::::NOOOOOOOOOO");
												sb.append("<td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblNatiBoardCerti+":</td>");
												sb.append("<td  class='divlableval' style='"+strCss_value+"'>&nbsp;"+lblN+"</td>");
											}
											
											
										}
										else
										{
										
										//National Board Certification
										
										teacherExperience = teacherExperienceDAO.findTeacherExperienceByTeacher(teacherDetail);
										sb.append("<tr>");
										sb.append("<td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblNatiBoardCerti+":</td>");
										
										if(teacherExperience!=null)
										{
											if(teacherExperience.getNationalBoardCert()==null)
											{
												sb.append("<td  class='divlableval' style='"+strCss_value+"'>&nbsp;"+lblNA+"</td>");
											}
											else
											{
												if(teacherExperience.getNationalBoardCert()==true)
												{
													String sNationalBoardCertYear="";
													if(teacherExperience.getNationalBoardCertYear()!=null && !teacherExperience.getNationalBoardCertYear().equals(""))
														sNationalBoardCertYear=teacherExperience.getNationalBoardCertYear().toString();
													sb.append("<td  class='divlableval' style='"+strCss_value+"'>&nbsp;"+lblY+" ("+sNationalBoardCertYear+")</td>");
												}
												else if(teacherExperience.getNationalBoardCert()==false)
													sb.append("<td  class='divlableval' style='"+strCss_value+"'>&nbsp;"+lblN+"</td>");
											}
										}
										else
										{
											sb.append("<td  class='divlableval' style='"+strCss_value+"'>&nbsp;"+lblNA+"</td>");
										}
										
										/*if(teacherExperience!=null)
											if(teacherExperience.getNationalBoardCert()!=null && teacherExperience.getNationalBoardCert()==true)
												sb.append("<td  class='divlableval' style='"+strCss_value+"'>&nbsp;Y ("+teacherExperience.getNationalBoardCertYear()+")</td>");
										else
											sb.append("<td  class='divlableval' style='"+strCss_value+"'>&nbsp;N</td>");*/
										
										sb.append("</tr>");
										
										}
										//sb.append("<tr><td class='divlable' style='"+strCss_lbl+"'>&nbsp;Job Applied:</td><td class='divlableval' style='"+strCss_value+"'>&nbsp;<a href='javascript:void(0);' onclick=\"getJobList('"+teacherDetail.getTeacherId()+"')\" >"+jobList+"</a>"+"/"+(jobListForTm-jobList)+"<td></tr>");
										
										
									sb.append("</table>");
								sb.append("</td>");
								
								sb.append("<td>");
									sb.append("<table border='0' cellspacing=0 cellpadding=0 class='tablecgtbl'>");
									
										strCss_lbl="padding:2px;padding-left:20px;white-space:nowrap";
										strCss_value="padding:2px;padding-left:10px;white-space:nowrap";
										
										//Fit Score
										if(fitScore){
											sb.append("<tr>");
											sb.append("<td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblFitScore+":</td>");
											String fScore =cgService.getFitScore(lstJobWiseConsolidatedTeacherScore_new,teacherDetail);
											if(fScore!=null)
												sb.append("<td  class='divlableval' style='"+strCss_value+"'>&nbsp;"+fScore+"</td>");
											else
												sb.append("<td  class='divlableval' style='"+strCss_value+"'>&nbsp;"+lblNA+"</td>");
											sb.append("</tr>");
										}
										
										//TFA
										if(tFA){
											sb.append("<tr>");
											sb.append("<td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblTFA+":</td>");
											
											//String tFAVal =cgService.getTFA(lstTeacherExperience_new,teacherDetail);
											
											String tFAVal = getTFAValue(lstTeacherExperience_new,teacherDetail);
											System.out.println(tfaEditAuthUser()+ " tfaEditAuthUser() tFAValtFAValtFAValtFAValtFAValtFAValtFAValtFAValtFAValtFAValtFAVal  "+tFAVal);
											String New_tFAVal = "";
											
											try
											{
												if(tFAVal!=null)
												{
													System.out.println("1");
													if(tfaEditAuthUser())
													{
														System.out.println("2");
														New_tFAVal = tFAVal+"<span id='edittfa'>&nbsp;&nbsp;<a href='javascript:void(0);' onclick='editTFAbyUser("+lstTeacherExperience_new.get(0).getTeacherId().getTeacherId()+");'>"+lblEdit+"</a></span><span id='savetfa' style='display:none; cursor:pointer;'>&nbsp;&nbsp;<a javascript:void(0); onclick='displayTFAMessageBox("+lstTeacherExperience_new.get(0).getTeacherId().getTeacherId()+");'>"+btnSave+"</a>&nbsp;</span><span id='canceltfa' style='display:none; cursor:pointer;'><a javascript:void(0); onclick='cancelTFAbyUser("+lstTeacherExperience_new.get(0).getTeacherId().getTeacherId()+");'>"+btnClr+"</a></span>";
													}
													else
													{
														System.out.println("3");
														New_tFAVal = tFAVal;
													}
													System.out.println("4");	
												}
												else
												{
													if(tfaEditAuthUser())
														New_tFAVal = ""+lblNA+" "+"<span id='edittfa'>&nbsp;&nbsp;<a href='javascript:void(0);' onclick='editTFAbyUser("+lstTeacherExperience_new.get(0).getTeacherId().getTeacherId()+");'>Edit</a></span><span id='savetfa' style='display:none; cursor:pointer;'>&nbsp;&nbsp;<a javascript:void(0); onclick='displayTFAMessageBox("+lstTeacherExperience_new.get(0).getTeacherId().getTeacherId()+");'>"+btnSave+"</a>&nbsp;</span><span id='canceltfa' style='display:none; cursor:pointer;'><a javascript:void(0); onclick='cancelTFAbyUser("+lstTeacherExperience_new.get(0).getTeacherId().getTeacherId()+");'>"+btnClr+"</a></span>";
													else
														New_tFAVal = lblNA;
												}
											}catch(Exception e)
											{
												e.printStackTrace();
											}
											
											sb.append("<th  class='divlableval' style='"+strCss_value+"'>&nbsp;"+New_tFAVal+"</th>");
											
											
											/*if(tFAVal!=null)
												sb.append("<td  class='divlableval' style='"+strCss_value+"'>&nbsp;"+tFAVal+"</td>");
											else
												sb.append("<td  class='divlableval' style='"+strCss_value+"'>&nbsp;N/A</td>");
											sb.append("</tr>");*/
											
											
											if(tFAVal!=null && lstTeacherExperience_new!=null && lstTeacherExperience_new.size()>0 && lstTeacherExperience_new.get(0)!=null)
											{
												sb.append("<tr>");
												sb.append("<td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblCorpsYear+":</td>");
												if(lstTeacherExperience_new.get(0).getCorpsYear()!=null)
													sb.append("<td class='divlableval' style='"+strCss_value+"'>&nbsp;"+lstTeacherExperience_new.get(0).getCorpsYear()+"</td>");
												else
													sb.append("<td class='divlableval' style='"+strCss_value+"'>&nbsp;"+lblNA+"</td>");
												sb.append("</tr>");
												
												sb.append("<tr>");
												sb.append("<td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblTFARegion+":</td>");
												if(lstTeacherExperience_new.get(0).getTfaRegionMaster()!=null)
													sb.append("<td class='divlableval' style='"+strCss_value+"'>&nbsp;"+lstTeacherExperience_new.get(0).getTfaRegionMaster().getTfaRegionName()+"</td>");
												else
													sb.append("<td class='divlableval' style='"+strCss_value+"'>&nbsp;"+lblNA+"</td>");
												sb.append("</tr>");
											}
											else
											{
												sb.append("<tr>");
												sb.append("<td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblCorpsYear+":</td>");
												sb.append("<td class='divlableval' style='"+strCss_value+"'>&nbsp;"+lblNA+"</td>");
												sb.append("</tr>");
												
												sb.append("<tr>");
												sb.append("<td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblTFARegion+":</td>");
												sb.append("<td class='divlableval' style='"+strCss_value+"'>&nbsp;"+lblNA+"</td>");
												sb.append("</tr>");
											}
											
											
										}
										//Demo Class
										if(demoClass){
											sb.append("<tr>");
											sb.append("<td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblDemoLesson+":</td>");
											String demoStatus =cgService.getDemoClassScheduleStatus(lstDemoClassSchedule_new,teacherDetail);
											if(demoStatus!=null)
												sb.append("<td  class='divlableval' style='"+strCss_value+"'>&nbsp;"+demoStatus+"</td>");
											else
												sb.append("<td  class='divlableval' style='"+strCss_value+"'>&nbsp;"+lblNA+"</td>");
											sb.append("</tr>");
										}
										//Teaching Years
										if(teachingOfYear){
											sb.append("<tr>");
											sb.append("<td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblTeachingYears+":</td>");
											if(cgService.getTEYear(lstTeacherExperience_new,teacherDetail)!=0)
												sb.append("<td  class='divlableval' style='"+strCss_value+"'>&nbsp;"+cgService.getTEYear(lstTeacherExperience_new,teacherDetail)+"</td>");
											else
												sb.append("<td  class='divlableval' style='"+strCss_value+"'>&nbsp;"+lblNA+"</td>");
											sb.append("</tr>");
										}
										//Expected Salary
										if(expectedSalary){
											sb.append("<tr>");
											sb.append("<td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblExpectedSalary+":</td>");
											if(cgService.getSalary(lstTeacherPersonalInfo_new,teacherDetail)!=-1){
													sb.append("<td  class='divlableval' style='"+strCss_value+"'>&nbsp;$"+cgService.getSalary(lstTeacherPersonalInfo_new,teacherDetail)+"</td>");
											}else
												sb.append("<td  class='divlableval' style='"+strCss_value+"'>&nbsp;"+lblNA+"</td>");
											sb.append("</tr>");
										}
										
										//# of Views:
										sb.append("<tr>");
										
										Criterion criterion_teacherDetail = Restrictions.eq("teacherDetail",teacherDetail);
										List<TeacherProfileVisitHistory> listTeacherProfileVisitHistory=null;
										if(entityID==1)
											listTeacherProfileVisitHistory = teacherProfileVisitHistoryDAO.findByCriteria(criterion_teacherDetail);
										else
										{
											Criterion criterion_districtId = Restrictions.eq("districtId",districtMaster);
											listTeacherProfileVisitHistory = teacherProfileVisitHistoryDAO.findByCriteria(criterion_teacherDetail,criterion_districtId);
										}
										/*else if(entityID==3){
											Criterion criterion_districtId = Restrictions.eq("districtId",districtMaster);
											Criterion criterion_schoolId = Restrictions.eq("schoolId",schoolMaster);
											listTeacherProfileVisitHistory = teacherProfileVisitHistoryDAO.findByCriteria(criterion_teacherDetail,criterion_schoolId,criterion_districtId);
										}*/
										
										int iTeacherProfileVisitHistoryCount=0;
										if(listTeacherProfileVisitHistory.size()>0)
											iTeacherProfileVisitHistoryCount=listTeacherProfileVisitHistory.size();
										
										sb.append("<td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblOfViews+":</td>");
										if(iTeacherProfileVisitHistoryCount>0)
											sb.append("<td  class='divlableval' style='"+strCss_value+"'><a data-original-title='"+lblClickToViewTPVisit+"' rel='tooltip' id='tpTeacherProfileVisitHistory' href='javascript:void(0);' onclick=\"getTeacherProfileVisitHistoryShow_FTime('"+teacherDetail.getTeacherId()+"');\" >&nbsp;"+iTeacherProfileVisitHistoryCount+"</a></td>");	
										//else
											//sb.append("<td  class='divlableval' style='"+strCss_value+"'><a data-original-title='No Teacher Profile Visit History' rel='tooltip' id='tpTeacherProfileVisitHistoryNo' ></a></td>");
										sb.append("</tr>");
										
										//Blanl row and Cell
										//sb.append("<tr><td>&nbsp;</td><td>&nbsp;</td></tr>");
										
										//Willing to Substitute
										sb.append("<tr>");
										sb.append("<td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblWillingToSubsti+":</td>");
										if(lstTeacherExperience_new!=null && lstTeacherExperience_new.size()>0)
										{
											if(lstTeacherExperience_new.get(0).getCanServeAsSubTeacher()==2)
												sb.append("<td  class='divlableval' style='"+strCss_value+"'>&nbsp;"+lblNA+"</td>");
											if(lstTeacherExperience_new.get(0).getCanServeAsSubTeacher()==0)
												sb.append("<td  class='divlableval' style='"+strCss_value+"'>&nbsp;"+lblN+"</td>");
											if(lstTeacherExperience_new.get(0).getCanServeAsSubTeacher()==1)
												sb.append("<td  class='divlableval' style='"+strCss_value+"'>&nbsp;"+lblY+"</td>");
										}
										else
										{
											sb.append("<td  class='divlableval' style='"+strCss_value+"'>&nbsp;"+lblNA+"</td>");
										}
										sb.append("</tr>");
										
										//KSN ID
										if( (userMaster.getEntityType()==6 && userMaster.getRoleId().getRoleId()==11) || (userMaster.getEntityType()==5 && userMaster.getRoleId().getRoleId()==10)){sb.append("<tr>");
										sb.append("<td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblKSNID+":</td>");
										MQEvent mqEvent = null;
										try{
											mqEvent = mqEventDAO.findLinkToKSNIniatedByTeacher(teacherDetail);
										}catch(Exception ex){
											ex.printStackTrace();
										}
										sb.append("<td  class='divlableval' style='"+strCss_value+"'>");
										sb.append("<div id='ksndetails'>");
										sb.append("<input type='hidden' id='disconnectTalentId' name='disconnectTalentId' value='"+teacherDetail.getTeacherId()+"' />");
										if(mqEvent!=null && mqEvent.getStatus()!=null && teacherDetail.getKSNID()==null){
											if(mqEvent.getStatus().equalsIgnoreCase("R")){
												if(mqEvent.getAckStatus()!=null && mqEvent.getAckStatus().contains("Success")){
													sb.append("Link to KSN is in progress");
												}
												else if(mqEvent.getAckStatus()!=null && mqEvent.getAckStatus().contains("Fail")){
													sb.append("<table><tr>");
													sb.append("<td  class='divlableval' style='"+strCss_value+"'>");
													//sb.append("<input type='hidden' id='disconnectTalentId' name='disconnectTalentId' value='"+teacherDetail.getTeacherId()+"' />");
													sb.append("<input type='text' id='inputKSN' class='form-control' value='' style='height:30px;' maxlength=10 onkeypress='return checkForInt(event);'/>");
													sb.append("</td>");
													sb.append("<td>");
													sb.append("<a data-original-title='Link to KSN' rel='tooltip' id='tpDisconnect' class='tpDisconnect' style='padding-left:8px;' href='javascript:void(0);' onclick='disconnectTalent();'><i class='icon-unlock icon-large'></i></a>");
													sb.append("</td>");
													sb.append("<script>$('#tpDisconnect').tooltip();</script>");
													sb.append("</tr><table>");
												}
												else{
													sb.append("Link to KSN is in progress");
												}
											}
										}
										else if(teacherDetail.getKSNID()==null){
											sb.append("<table><tr><td>");
											//sb.append("<input type='hidden' id='disconnectTalentId' name='disconnectTalentId' value='"+teacherDetail.getTeacherId()+"' />");
											sb.append("<input type='text' id='inputKSN' class='form-control' value='' style='height:30px;' maxlength=10 onkeypress='return checkForInt(event);'/>");
											sb.append("</td>");
											sb.append("<td>");
											sb.append("<a data-original-title='Link to KSN' rel='tooltip' id='tpDisconnect' class='tpDisconnect' style='padding-left:8px;' href='javascript:void(0);' onclick='disconnectTalent();'><i class='icon-unlock icon-large'></i></a>");
											sb.append("</td>");
											sb.append("<script>$('#tpDisconnect').tooltip();</script>");
											sb.append("</tr></table>");
										}
										else if(teacherDetail.getKSNID()!=null){
											sb.append("<table><tr>");
											sb.append("<td>"+teacherDetail.getKSNID()+"</td>");
											sb.append("<td>");
											sb.append("<a data-original-title='Linked to KSN' rel='tooltip' id='tpDisconnect' style='padding-left:8px;' href='javascript:void(0);'><i class='icon-lock icon-large'></i></a>");
											sb.append("</td>");
											sb.append("<script>$('#tpDisconnect').tooltip();</script>");
											sb.append("</tr></table>");
										}
										sb.append("</div>");
										sb.append("</td>");
										sb.append("</tr>");
										}
										
										//System.out.println("District Id:"+jobOrder.getDistrictMaster().getDistrictId());
										if(jobOrder!=null && jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getEeocRequired()){
											sb.append("<tr>");
											sb.append("<td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblEeocSupport+":</td>");
											sb.append("<td  class='divlableval' style='"+strCss_value+"'><a href='javascript:void(0);' onclick='eeocDetail("+teacherDetail.getTeacherId()+");'>"+lblEeocLink+"</a></td>");
											sb.append("</tr>");
										}
										if(jobOrder!=null && jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId()==3904380){
											List<DistrictSpecificPortfolioQuestions> listQuestions= districtSpecificPortfolioQuestionsDAO.findByCriteria(Restrictions.eq("question", "Have you ever worked for Columbus City Schools?"));
											if(listQuestions!=null && listQuestions.size()>0){
												DistrictSpecificPortfolioQuestions question = listQuestions.get(0);
												Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
												Criterion criterion2 = Restrictions.eq("districtSpecificPortfolioQuestions",question);
												List<DistrictSpecificPortfolioAnswers> listdistrictSpecificPortfolioAnswers = districtSpecificPortfolioAnswersDAO.findByCriteria(criterion1, criterion2);
												
												if(listdistrictSpecificPortfolioAnswers!=null && listdistrictSpecificPortfolioAnswers.size()>0){
													sb.append("<tr>");
													sb.append("<td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblPreviousEmployed+":</td>");
													DistrictSpecificPortfolioAnswers districtSpecificPortfolioAnswer = listdistrictSpecificPortfolioAnswers.get(0);
													System.out.println("districtSpecificPortfolioAnswer="+districtSpecificPortfolioAnswer+"::::::::::::districtSpecificPortfolioAnswer.getQuestionOption():"+districtSpecificPortfolioAnswer.getQuestionOption());
														if(districtSpecificPortfolioAnswer!=null && districtSpecificPortfolioAnswer.getQuestionOption().trim().equalsIgnoreCase("Yes")){
													sb.append("<td  class='divlableval' style='"+strCss_value+"'>"+lblYes+"</td>");
														}
													else if(districtSpecificPortfolioAnswer!=null && districtSpecificPortfolioAnswer.getQuestionOption().trim().equalsIgnoreCase("No")){
													sb.append("<td  class='divlableval' style='"+strCss_value+"'>"+lblNo+"</td>");
													}
													else
													sb.append("<td  class='divlableval' style='"+strCss_value+"'>&nbsp;N/A</td>");
														
													sb.append("</tr>");
													
													if(districtSpecificPortfolioAnswer.getQuestionOption().trim().equalsIgnoreCase("Yes")){
														String insertedText = districtSpecificPortfolioAnswer.getInsertedText();
														if(!insertedText.isEmpty()){
															System.out.println("::::::::insertedText"+insertedText);
															String insertedTextArray[] = insertedText.split("\\$"); 
															System.out.println("::::::::insertedTextArray Length"+insertedTextArray.length);
															String fromDateStr 		   = insertedTextArray[0].replaceAll("-", "/");;
															//fromDateStr=fromDateStr.replaceAll("-", "/");
															System.out.println("date  ::::::::::;===="+fromDateStr);
															String toDateStr 		  	   = insertedTextArray[2].replaceAll("-", "/");
															String position 		   = insertedTextArray[4];
															//Format formatter = new SimpleDateFormat("yyyy/MM/dd");
															
															
															
															SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yy");
															Date fromDate = dateFormat.parse(fromDateStr);
															Date toDate = dateFormat.parse(toDateStr);
															
															/*String fromDate = dateFormat.format(dateFormat.parse(fromDateStr.toString()));
															String toDate = dateFormat.format(dateFormat.parse(toDateStr.toString()));*/
															Calendar cal = Calendar.getInstance();
															cal.setTime(fromDate);

															int month = cal.get(Calendar.MONTH);
															int day = cal.get(Calendar.DAY_OF_MONTH);
															int year = cal.get(Calendar.YEAR);
															dateFormat = new SimpleDateFormat("MMM");
															String month_name1 = dateFormat.format(cal.getTime());
															//System.out.println("Month name ::::"+month_name1+"Month ::::::"+month+" Day :::"+day+" Year :::"+year);
															
															Calendar cal2 = Calendar.getInstance();
															cal2.setTime(toDate);
															int month2 = cal2.get(Calendar.MONTH);
															int day2 = cal2.get(Calendar.DAY_OF_MONTH);
															int year2 = cal2.get(Calendar.YEAR);
															String month_name2 = dateFormat.format(cal.getTime());
															String finalFromString = month_name1+" "+day+", "+year;
															String finalToString = month_name2+" "+day2+", "+year2;
															
															//System.out.println("Month name ::::"+month_name2+"Month ::::::"+month2+" Day :::"+day2+" Year :::"+year2);
															
														sb.append("<tr>");
														sb.append("<td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblDates+":</td>");
														sb.append("<td  class='divlableval' style='"+strCss_value+"'>"+finalFromString+"-"+finalToString+"</td>");
														sb.append("</tr>");
														sb.append("<tr>");
														sb.append("<td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblPosition+":</td>");
														sb.append("<td  class='divlableval' style='"+strCss_value+"'>"+position+"</td>");
														sb.append("</tr>");
													  }
													}
												}
												
												//sb.append("<td  class='divlableval' style='"+strCss_value+"'><a href='javascript:void(0);' onclick='eeocDetail("+teacherDetail.getTeacherId()+");'>"+lblEeocLink+"</a></td>");
												
											}
										}
										
									sb.append("</table>");
								sb.append("</td>");
								
							sb.append("</tr>");
						sb.append("</table>");
						
					sb.append("</td>");
				sb.append("</tr>");
				
				// start new row
				
				sb.append("<tr>");
					sb.append("<td>");
						sb.append("<table border='0' cellspacing=0 cellpadding=0 class='tablecgtbl'>");
						sb.append("<tr>");
							strCss_value="padding-left:55px;";
							// Blank Cell
							sb.append("<td style='"+strCss_value+"'>&nbsp;</td>");
							
							strCss_lbl="padding:2px;vertical-align:middle;";
							strCss_value="padding:2px;vertical-align:text-top;";
							sb.append("<td>"); 
							sb.append("<table border='0' cellspacing=0 cellpadding=0 class='tablecgtbl'>");
							
							// Resume
							sb.append("<tr>");
							
							sb.append("<td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lnkResume+":</td>");
							if(teacherExperience!=null && teacherExperience.getResume()!=null && teacherExperience.getResume()!="")
								sb.append("<td  class='divlableval' style='"+strCss_value+"'>&nbsp;<a data-original-title='"+lnkResume+"' rel='tooltip' id='tpResumeprofile' href='javascript:void(0);' onclick=\"downloadResume('"+teacherDetail.getTeacherId()+"','tpResumeprofile');"+windowFunc+"\" ><span class='icon-briefcase icon-large iconcolor'></span></a></td>");
							else
								sb.append("<td  class='divlableval' style='"+strCss_value+"'>&nbsp;<a data-original-title='"+lblNotAvailable+"' rel='tooltip' id='tpResumeprofile'  ><span class='icon-briefcase icon-large iconcolorhover'></span></a></td>");
							
							/*strCss_lbl="padding:2px;padding-left:126px;vertical-align:middle;";
							
							//Phone:
							List<TeacherPersonalInfo> lstTeacherPersonalInfo1	  =	 new ArrayList<TeacherPersonalInfo>();
							lstTeacherPersonalInfo1=teacherPersonalInfoDAO.findTeacherPInfoForTeacher(lstTeacherDetails);
							sb.append("<td class='divlable' style='"+strCss_lbl+"'>&nbsp;Phone: </td>");
							if(lstTeacherPersonalInfo1.size()==1){
								if( (lstTeacherPersonalInfo1.get(0).getPhoneNumber()!=null && !lstTeacherPersonalInfo1.get(0).getPhoneNumber().equalsIgnoreCase("")) || (lstTeacherPersonalInfo1.get(0).getMobileNumber()!=null && !lstTeacherPersonalInfo1.get(0).getMobileNumber().equalsIgnoreCase("")))
									sb.append("<td  class='divlableval' style='"+strCss_value+";margin-top:-5px;padding-left:5px;'><a data-original-title='Phone' rel='tooltip' id='tpPhoneprofile' href='javascript:void(0);' onclick=\"getPhoneDetailShow('"+teacherDetail.getTeacherId()+"');\" ><span class='icon-mobile-phone icon-large iconcolor icon-large2';></span></a></td>");	
								else
									sb.append("<td  class='divlableval' style='"+strCss_value+";margin-top:-5px;padding-left:5px;'><a data-original-title='Not Available' rel='tooltip' id='tpPhoneprofile' ><span class='icon-mobile-phone icon-large iconcolorhover icon-large2'></span></a></td>");
							}
							else
								sb.append("<td  class='divlableval' style='"+strCss_value+";margin-top:-5px;padding-left:5px;'><a data-original-title='Not Available' rel='tooltip' id='tpPhoneprofile' ><span class='icon-mobile-phone icon-large iconcolorhover icon-large2'></span></a></td>");
							*/
							// PD Report
							strCss_lbl="padding:2px;padding-left:295px;vertical-align:middle;";
							
							Map<Integer,TeacherAssessmentStatus> baseTakenMap = new HashMap<Integer, TeacherAssessmentStatus>();
							List<TeacherAssessmentStatus> teacherAssessmentStatus = null;
							Map<Integer, Integer> isbaseComplete	=	new HashMap<Integer, Integer>();
							
							Integer isBase = null;
							try{
								if(lstTeacherDetails!=null && lstTeacherDetails.size()>0){
									teacherAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentTakenByTeachers(lstTeacherDetails);
									for (TeacherAssessmentStatus teacherAssessmentStatus2 : teacherAssessmentStatus) {					
										baseTakenMap.put(teacherAssessmentStatus2.getTeacherDetail().getTeacherId(), teacherAssessmentStatus2);
										boolean cgflag = teacherAssessmentStatus2.getCgUpdated()==null?false:teacherAssessmentStatus2.getCgUpdated();
										if(cgflag && teacherAssessmentStatus2.getStatusMaster().getStatusShortName().equalsIgnoreCase("comp"))
											isbaseComplete.put(teacherAssessmentStatus2.getTeacherDetail().getTeacherId(), 1);
										else
											if(teacherAssessmentStatus2.getStatusMaster().getStatusShortName().equalsIgnoreCase("vlt"))
												isbaseComplete.put(teacherAssessmentStatus2.getTeacherDetail().getTeacherId(), 2);
									}
								}
							}catch(Exception e){
								e.printStackTrace();
							}
							isBase = isbaseComplete.get(teacherDetail.getTeacherId());
							sb.append("<td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+headPdRep+": </td>");
							if(isBase!=null && isBase==1)
							{
								sb.append("<td  class='divlableval' style='"+strCss_value+"'>&nbsp;<a data-original-title='"+headPdRep+"' rel='tooltip' id='tpPDReportprofile' href='javascript:void(0);' onclick=\"generatePDReport('"+teacherDetail.getTeacherId()+"','tpPDReportprofile');"+windowFunc+"\"><span class='icon-book icon-large iconcolor'></span></a></td>");
							}
							else
							{
								sb.append("<td  class='divlableval' style='"+strCss_value+"'>&nbsp;<a data-original-title='"+lblNotAvailable+"' rel='tooltip' id='tpPDReportprofile' ><span class='icon-book icon-large iconcolorhover'></span></a></td>");			
								
							}					
							
							//JSI
							if(JSI)
							{
								if(roleAccess.indexOf("|4|")!=-1)
								{
									sb.append("<td class='divlable' style='padding:2px;padding-left:105px;vertical-align:middle;'>&nbsp;"+lblJSI+":</td>");
									if(mapJSI.get(teacherDetail.getTeacherId())!=null){
										teacherAssessmentStatusJSI_new = mapJSI.get(teacherDetail.getTeacherId());
										if(teacherAssessmentStatusJSI_new.getStatusMaster().getStatusShortName().equalsIgnoreCase("icomp")){
											sb.append("<td  class='divlableval' style='"+strCss_value+"'>");
											sb.append("<a data-original-title='"+lblCandidateNotCompletedJSI+"' rel='tooltip' id='tpJSAProfile'><img src='images/jsi_hover.png'  width='24px'></a>");
											sb.append("</td>");
										}
										else if(teacherAssessmentStatusJSI_new.getStatusMaster().getStatusShortName().equalsIgnoreCase("vlt")){
											sb.append("<td  class='divlableval' style='"+strCss_value+"'>");
											sb.append("<a data-original-title='"+lbllblCandidateTimedOut+"' rel='tooltip' id='tpJSAProfile'><img src='images/jsi_hover.png'  width='24px'></a>");
											sb.append("</td>");
										}
										else
										{
											sb.append("<td  class='divlableval' style='"+strCss_value+";margin-bottom:-10px;'>");
											sb.append("<a data-original-title='"+lblJSI+"' rel='tooltip' id='tpJSAProfile' href='javascript:void(0);' onclick=\"generatePilarReport('"+teacherDetail.getTeacherId()+"','"+jobOrder.getJobId()+"','tpJSAProfile');"+windowFunc+"\"><img src='images/jsi.png'  width='24px'></a>");
											sb.append("</td>");
										}
									}
									else
									{
										sb.append("<td  class='divlableval' style='"+strCss_value+"'>");
										sb.append("<a data-original-title='"+lblNotAvailable+"' rel='tooltip' id='tpJSAProfile'><img src='images/jsi_hover.png'  width='24px'></a>");
										sb.append("</td>");
									}
								}
							}
							
							sb.append("</tr>");
							
							sb.append("</table>");
							
						sb.append("</td>");
						sb.append("</tr>");
						sb.append("</table>");
					
					sb.append("</td>");
				sb.append("</tr>");
				// End new row
			
				// start new row
				sb.append("<tr>");
				sb.append("<td>");
					sb.append("<table border='0' cellspacing=0 cellpadding=0 class='tablecgtbl'>");
					sb.append("<tr>");
						strCss_value="padding-left:55px;";
						// Blank Cell
						sb.append("<td style='"+strCss_value+"'>&nbsp;</td>");
						
						strCss_lbl="padding:2px;vertical-align:middle;";
						strCss_value="padding:2px;vertical-align:text-top;";
						sb.append("<td>"); 
						sb.append("<table border='0' cellspacing=0 cellpadding=0 class='tablecgtbl'>");
						
						// Resume
						sb.append("<tr>");
						
						strCss_lbl="padding:2px;padding-left:2px;vertical-align:middle;";
						
						//Phone:
						List<TeacherPersonalInfo> lstTeacherPersonalInfo1	  =	 new ArrayList<TeacherPersonalInfo>();
						lstTeacherPersonalInfo1=teacherPersonalInfoDAO.findTeacherPInfoForTeacher(lstTeacherDetails);
						sb.append("<td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblPhone+": </td>");
						if(lstTeacherPersonalInfo1.size()==1){
							if( (lstTeacherPersonalInfo1.get(0).getPhoneNumber()!=null && !lstTeacherPersonalInfo1.get(0).getPhoneNumber().equalsIgnoreCase("")))
								sb.append("<td  class='divlableval' style='"+strCss_value+";margin-top:-5px;padding-left:116px;'>"+lstTeacherPersonalInfo1.get(0).getPhoneNumber()+"</td>");	
							else
								sb.append("<td  class='divlableval' style='"+strCss_value+";margin-top:-5px;padding-left:5px;'>&nbsp;"+lblNA+"</td>");
						}
						else
							sb.append("<td  class='divlableval' style='"+strCss_value+";margin-top:-5px;padding-left:5px;'>&nbsp;"+lblNA+"</td>");
						
						// PD Report
						strCss_lbl="padding:2px;padding-left:83px;vertical-align:middle;";
						
						
						isBase = isbaseComplete.get(teacherDetail.getTeacherId());
						sb.append("<td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblMobileNumber+": </td>");
						if(((lstTeacherPersonalInfo1.size()==1 && lstTeacherPersonalInfo1.get(0).getMobileNumber()!=null && !lstTeacherPersonalInfo1.get(0).getMobileNumber().equalsIgnoreCase(""))))
						{
							sb.append("<td  class='divlableval' style='"+strCss_value+";padding-left:30px;'>&nbsp;"+lstTeacherPersonalInfo1.get(0).getMobileNumber()+"</td>");
						}
						else
						{
							sb.append("<td  class='divlableval' style='"+strCss_value+"'>&nbsp;"+lblNA+"</td>");			
							
						}	
						sb.append("</tr>");
						
						sb.append("</table>");
						
						sb.append("</td>");
						sb.append("</tr>");
					
						if(userMaster.getDistrictId()!=null && (userMaster.getDistrictId().getDistrictId().equals(7800049) || userMaster.getDistrictId().getDistrictId().equals(7800048) || userMaster.getDistrictId().getDistrictId().equals(7800050) || userMaster.getDistrictId().getDistrictId().equals(7800051) || userMaster.getDistrictId().getDistrictId().equals(7800053))){
						sb.append("<tr>");
						strCss_value="padding-left:55px;";
						// Blank Cell
						sb.append("<td style='"+strCss_value+"'>&nbsp;</td>");
						
						strCss_lbl="padding:2px;vertical-align:middle;";
						strCss_value="padding:2px;vertical-align:text-top;";
						sb.append("<td>"); 
						sb.append("<table border='0' cellspacing=0 cellpadding=0 class='tablecgtbl'>");					
						
						sb.append("<tr><td colspan=5 class='net-header-text' style='padding:0px;padding-left:4px;padding-top:10px;padding-bottom:5px;color:black;'>"+Utility.getLocaleValuePropByKey("lbloctdetails", locale)+"</td></tr>");
						
						
						strCss_lbl="padding:2px;padding-left:2px;vertical-align:middle;";
						
						OctDetails octDetails = getOctDetails(teacherDetail);
						
						String octNumber ="";
						String octFileUpload ="";
						if(octDetails!=null && octDetails.getOctNumber()!=null){
							octFileUpload=octDetails.getOctFileName();
						}
						if(octDetails!=null && octDetails.getOctNumber()!=null){
							octNumber="<a href='#' onclick=\"getOctDetails('"+octDetails.getOctNumber()+"')\">"+octDetails.getOctNumber()+"</a>";
						}else{
							octNumber="N/A";
						}
						sb.append("<td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+Utility.getLocaleValuePropByKey("lbloctRgNo", locale)+": </td>");					
						sb.append("<td  class='divlableval' style='"+strCss_value+";margin-top:-5px;padding-left:5px;'>&nbsp;&nbsp;&nbsp;"+octNumber+"</td>");
						
						// PD Report
						strCss_lbl="padding:2px;padding-left:83px;vertical-align:middle;";
						sb.append("<td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+Utility.getLocaleValuePropByKey("lbloctcard", locale)+": </td>");					
							sb.append("<td  class='divlableval' style='"+strCss_value+"'>&nbsp;");
							if(octFileUpload!=""){
								sb.append("<a data-original-title='"+Utility.getLocaleValuePropByKey("lbloctcard", locale)+"' rel='tooltip' id='hrefOctUpload' href='javascript:void(0);' onclick=\"downloadOctUpload('"+octDetails.getTeacherId().getTeacherId()+"');"+windowFunc+"\">View</a>");
							}else{
								sb.append("N/A");
							}
							sb.append("</td>");						
						sb.append("</tr>");
						
						sb.append("<tr><td class='divlable' style='"+strCss_lbl+"padding-left:0px;'>&nbsp;"+Utility.getLocaleValuePropByKey("msgComments", locale)+"</td></tr>");
						sb.append("<tr>");
						sb.append("<td colspan='3'><textarea>");
						if(octDetails!=null && octDetails.getOctText()!=null){
							sb.append(octDetails.getOctText());
						}
						sb.append("</textarea></td>");
						sb.append("</tr>");
						sb.append("</table>");
						sb.append("</td>");
						sb.append("</tr>");
						}
			
					sb.append("</table>");
				
				sb.append("</td>");
			sb.append("</tr>");
				// End new row
				
				// start new row
				
				// Calculating and Get valles of "Achievement Score","Academic Achievement" and "Leadership/Results"
				String strAScore="";
				String strLRScore="";
				int iAcademicAchievement=0;
				int iLeadershipResults=0;
				int itotalAchievementScore=0;
				int iachievementMaxScore=0;
				
				int iacademicAchievementMaxScore=0;
				int ileadershipAchievementMaxScore=0;
				
				int getScrrec = 0;
				if(lstTeacherAchievementScore_new!=null && lstTeacherAchievementScore_new.size()!=0){
					getScrrec=lstTeacherAchievementScore_new.size()-1;
				}
				if(lstTeacherAchievementScore_new.size()>0){
					iAcademicAchievement=lstTeacherAchievementScore_new.get(getScrrec).getAcademicAchievementScore();
					iLeadershipResults=lstTeacherAchievementScore_new.get(getScrrec).getLeadershipAchievementScore();
					
					iacademicAchievementMaxScore=lstTeacherAchievementScore_new.get(getScrrec).getAcademicAchievementMaxScore();
					ileadershipAchievementMaxScore=lstTeacherAchievementScore_new.get(getScrrec).getLeadershipAchievementMaxScore();
					
					itotalAchievementScore=lstTeacherAchievementScore_new.get(getScrrec).getTotalAchievementScore();
					iachievementMaxScore=lstTeacherAchievementScore_new.get(getScrrec).getAchievementMaxScore();
					
					//strAScore=(itotalAchievementScore)+"/"+(iachievementMaxScore);
					strAScore=(iAcademicAchievement)+"/"+(iacademicAchievementMaxScore);
					strLRScore=(iLeadershipResults)+"/"+(ileadershipAchievementMaxScore);
				}
				else
				{
					strAScore=lblNA;
					strLRScore=lblNA;
				}
				
				strCss_lbl="padding-left:10px;";
				strCss_value="padding-left:105px;";
				if(entityID==2 || entityID==3)
				{
					if(entityID==3)
						strCss_lbl="padding-left:51px;";
					
					sb.append("<tr>");
						sb.append("<td>");
							sb.append("<table border='0' min-width=750 cellspacing=0 cellpadding=0 class='tablecgtbl'>");
							if(achievementScore)
							{
									sb.append("<tr><td>&nbsp;</td>" +
											"<td>" +
												"<table border='0' cellspacing=0 cellpadding=0 class='tablecgtbl'>" +
													"<TR>");
														// A Score
														if(entityID==3)
															strCss_lbl="padding-left:55px;";
														else
															strCss_lbl="padding-left:5px;width:50px;";
														
														strCss_value="padding-left:5px;width:50px;";	
														
														sb.append("<td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblAScore+":</td>");
														sb.append("<td class='divlableval' style='"+strCss_value+";vertical-align:middle;' id='ascore_profile'>&nbsp;"+strAScore+"</td>");
														
														sb.append("<td style='width:190px;'>&nbsp;</td>");
														
														strCss_lbl="padding-left:2px;width:55px;";
														sb.append("<td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblLRScore+":</td>");
														sb.append("<td class='divlableval' style='"+strCss_value+";vertical-align:middle;' id='lrscore_profile'>&nbsp;"+strLRScore+"</td>");
									
									sb.append("</TR>" +
												"</table>"+
											"</td></tr>");
							}
							
							if(entityID==2)
							{
								
							
							sb.append("<tr>");
								strCss_value="padding-left:55px;";
								// Blank Cell
								sb.append("<td style='width:55px;'>&nbsp;</td>");
								
								strCss_lbl="padding:2px;padding-left:5px;padding-top:5px;";
								sb.append("<td>"); 
								
								
		
								if(achievementScore && entityID==2)
								{
									sb.append("<table border='0' min-width=700 cellspacing=0 cellpadding=0 class='tablecgtbl'>");
									//1st Row for Label of "Achievement Score","Academic Achievement" and "Leadership/Results"
									sb.append("<tr>");
									//Academic Achievement:
										sb.append("<td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblAcademic +"&nbsp;"+lblAchievement +":</td>");
									// Leadership/Results
										sb.append("<td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+ lblLeadershipResults +":</td>");
										
										sb.append("<td style='width:100px;'>&nbsp;</td>");
									sb.append("</tr>");
									
									strCss_lbl="padding:2px;padding-left:8px;padding-top:-20px;";
									//2nd Row for values of "Achievement Score","Academic Achievement" and "Leadership/Results"
									String strSliderCss_Iframe="border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:280px;";
									sb.append("<tr>");
										
									sb.append("<td style='"+strCss_lbl+"'>&nbsp;");
									sb.append("<div style='margin-left: -5px; margin-top: -18px;'><label>" +
											"<iframe id='ifrm_aca_ach' src='slideract.do?name=slider_aca_ach&tickInterval=5&max=20&swidth=240&svalue="+iAcademicAchievement+"' scrolling='no' frameBorder='0' style='"+strSliderCss_Iframe+"'>" +
											"</iframe></label>" +
											"</div>");
										
									sb.append("<input type='hidden' name='txt_sl_slider_aca_ach' id='txt_sl_slider_aca_ach' value='"+iAcademicAchievement+"'>");
									sb.append("</td>");
									
									
									// Leadership/Results
									sb.append("<td style='"+strCss_lbl+"'>&nbsp;");
									
									sb.append("<div style='margin-left: -5px; margin-top: -18px;'><label>" +
												"<iframe id='ifrm_lea_res' src='slideract.do?name=slider_lea_res&tickInterval=5&max=20&swidth=240&svalue="+iLeadershipResults+"' scrolling='no' frameBorder='0' style='"+strSliderCss_Iframe+"'>" +
												"</iframe></label>" +
												"</div>");
									sb.append("<input type='hidden' name='txt_sl_slider_lea_res' id='txt_sl_slider_lea_res' value='"+iLeadershipResults+"'>");
									//sb.append("<a href='javascript:void(0);' onclick=\"javascript:updateAScore("+teacherId+","+jobOrder.getDistrictMaster().getDistrictId()+")\"'>Go</A>");
									sb.append("</td>");
									
									//sb.append("<td style='width:20px;vertical-align:middle;'>&nbsp;<a href='javascript:void(0);' onclick=\"javascript:updateAScore("+teacherId+","+jobOrder.getDistrictMaster().getDistrictId()+")\"'>Go</A></td>");
									//sb.append("<td style='width:20px;vertical-align:middle;'>&nbsp;<a href='javascript:void(0);' onclick=\"javascript:updateAScore("+teacherId+","+jobOrder.getDistrictMaster().getDistrictId()+")\"'>Go</A></td>");
									//sb.append("<td style='width:40px;vertical-align:middle;'>&nbsp;<button class='btn btn-primary' type='button' onclick=\"javascript:updateAScore("+teacherId+","+jobOrder.getDistrictMaster().getDistrictId()+")\">Set A Score <i class='icon'></i></button></td>");
									sb.append("<td style='width:170px;vertical-align:middle;'>&nbsp;<button style='margin-top:-18px;' class='btn btn-primary' type='button' onclick=\"javascript:updateAScore("+teacherId+","+jobOrder.getDistrictMaster().getDistrictId()+")\">"+lblSetAScore+"</td>");
									
									sb.append("</tr>");
									
									sb.append("</table>");
								}
								
							sb.append("</td>");
							sb.append("</tr>");
							
							}
							sb.append("</table>");
						
						sb.append("</td>");
					sb.append("</tr>");
				
				}
				// End new row
			
					if(pnqDis){
						sb.append("<tr>");
						sb.append("<td style='padding:0px 0px 0px 53px'>");
					
						sb.append("<table border='0' cellspacing=0 cellpadding=0 class='tablecgtbl'>");
						sb.append("<tr>");				
						sb.append("<td class='divlable' style='padding:2px;padding-left:8px;padding-top:20px;'>&nbsp;"+lblPNQ+":</td>");				
						String pNQVal = getPNQValue(lstTeacherExperience_new,teacherDetail);
						String New_pNQVal = "";
						System.out.println();
						sb.append("<th >&nbsp;"+pNQVal+"</th>");				
						sb.append("</td>");
						sb.append("</tr>");
						sb.append("</table>");
					
						sb.append("</td>");
						sb.append("</tr>");
					}
			sb.append("</table>");
			// New setting end
			
			/*sb.append("</TD></TR>"); // main table tblShowProDetails
			sb.append("<TR><TD>"); // main table tblShowProDetails
			 */
			sb.append("<div id='removeWait' style='text-align:center;padding-top:0px;'><img src='images/loadingAnimation.gif' /> <br>Loading...</div>");
			
			sb.append("</div>");
			sb.append("<div class='custom-div-border'>");
			//sb.append("<table border=0 width='100%'><tr bgcolor='#F2FAEF'><td style='text-align:center;vertical-align:middle;padding-top:10px;padding-bottom:10px;'width='35%'><a href='javascript:void(0);' onclick=\"getCommunicationsDiv(0,"+jobForTeacher.getJobForTeacherId()+",'"+teacherDetail.getTeacherId()+"',"+jobOrder.getJobId()+");\" ><span class='icon-dropbox icon-large iconcolor'></span>&nbsp;Communications</a></td>");
			sb.append("<DIV>");
			if(userMaster.getEntityType()==2)
			{
			if(lastActivityDate!=null && (districtMaster.getDisplayCommunication()==null || districtMaster.getDisplayCommunication()==false)){
			sb.append("<table border=0 width='100%'><tr bgcolor='#F2FAEF'><td style='text-align:center;vertical-align:middle;padding-top:10px;padding-bottom:10px;'width='35%'><a href='javascript:void(0);' onclick=\"getCommunicationsDiv(0,"+jobForTeacherId+",'"+teacherDetail.getTeacherId()+"',"+jobOrder.getJobId()+");\" ><span class='icon-dropbox icon-large iconcolor'></span>&nbsp;"+headCom+"</a></td>");
			}else if(lastActivityDate!=null && (districtMaster.getDisplayCommunication()==null || districtMaster.getDisplayCommunication()==true)){
				if(jobOrder!=null)
					sb.append("<table border=0 width='100%'><tr bgcolor='#F2FAEF'><td style='text-align:center;vertical-align:middle;padding-top:10px;padding-bottom:10px;'width='35%'><a href='javascript:void(0);' onclick=\"getCommunicationsDiv(0,"+jobForTeacherId+",'"+teacherDetail.getTeacherId()+"',"+jobOrder.getJobId()+");\" ><span class='icon-dropbox icon-large iconcolor'></span>&nbsp;"+headCom+"</a></td>");
				else
					sb.append("<table border=0 width='100%'><tr bgcolor='#F2FAEF'><td style='text-align:center;vertical-align:middle;padding-top:10px;padding-bottom:10px;'width='35%'><a href='javascript:void(0);' onclick=\"getCommunicationsDiv(0,"+jobForTeacherId+",'"+teacherDetail.getTeacherId()+"',0);\" ><span class='icon-dropbox icon-large iconcolor'></span>&nbsp;"+headCom+"</a></td>");	
			}else if(lastActivityDate==null){
				sb.append("<table border=0 width='100%'><tr bgcolor='#F2FAEF'><td style='text-align:center;vertical-align:middle;padding-top:10px;padding-bottom:10px;'width='35%'><a href='javascript:void(0);' onclick=\"getCommunicationsDiv(0,"+jobForTeacherId+",'"+teacherDetail.getTeacherId()+"',"+jobOrder.getJobId()+");\" ><span class='icon-dropbox icon-large iconcolor'></span>&nbsp;"+headCom+"</a></td>");
			}
			}else{
				sb.append("<table border=0 width='100%'><tr bgcolor='#F2FAEF'><td style='text-align:center;vertical-align:middle;padding-top:10px;padding-bottom:10px;'width='35%'><a href='javascript:void(0);' onclick=\"getCommunicationsDiv(0,"+jobForTeacherId+",'"+teacherDetail.getTeacherId()+"',"+jobOrder.getJobId()+");\" ><span class='icon-dropbox icon-large iconcolor'></span>&nbsp;"+headCom+"</a></td>");
			}
			
			if(userMaster.getEntityType()==1)
			{
				sb.append("<td style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><span class='icon-save icon-large iconcolorhover'></span>&nbsp;"+btnSave+"</td>" +
						"<td style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><span class='icon-share icon-large iconcolorhover'></span>&nbsp;"+lblShare+"</td>"+
						"<td style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><span class='icon-tag icon-large iconcolorhover'></span></span>&nbsp;"+headTag+"</td>");
			}
			else
			{
				sb.append("<td style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><a href='javascript:void(0);' id='saveaction' onclick=\"saveToFolder("+jobForTeacherId+",1)\"><span class='icon-save icon-large iconcolor'></span>&nbsp;"+btnSave+"</a></td>");
				sb.append("<td style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><a href='javascript:void(0);' id='shareaction' onclick=\"displayShareFolder("+jobForTeacherId+",1)\"><span class='icon-share icon-large iconcolor'></span>&nbsp;"+lblShare+"</a></td>");

				//System.out.println(" Gagan : Condition for writePrivilegeToSchool only District and School  ");
				/*	if(jobOrder.getWritePrivilegeToSchool()==false)
				{
					sb.append("<td style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><a href='javascript:void(0);' id='tagaction' onclick=\"closeTagActAction("+noOfRecordCheck+")\"> <span class='icon-tag icon-large iconcolor'></span></span>&nbsp;Tag</a></td>");
				}
				else
				{*/
					try
					{
						//System.out.println("Privilege  : "+jobOrder.getWritePrivilegeToSchool()+" EntityType : "+userMaster.getEntityType());
						//System.out.println(" Size  "+jobOrder.getSchool().size()+" Else Block "+(userMaster.getSchoolId().getSchoolId()+" == "+jobOrder.getSchool().get(0).getSchoolId())+"");
							if(jobOrder.getWritePrivilegeToSchool()!=null && jobOrder.getWritePrivilegeToSchool()==true && userMaster.getEntityType()==3)
							{
								if((userMaster.getSchoolId().getSchoolId().equals(jobOrder.getSchool().get(0).getSchoolId())  || (userMaster.getDistrictId().getIsJobSpecificTags()!=null && userMaster.getDistrictId().getIsJobSpecificTags()) ||(userMaster.getDistrictId().getIsDistrictWideTags()!=null && userMaster.getDistrictId().getIsDistrictWideTags()))  )
								{
									System.out.println(" Third Type Job Order "+jobId);
									//sb.append("<td style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><a href='javascript:void(0);' id='tagaction' onclick=\"closeTagActAction("+noOfRecordCheck+")\"> <span class='icon-tag icon-large iconcolor'></span></span>&nbsp;Tag</a></td>");
									sb.append("<td style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><a href='javascript:void(0);' id='tagaction' onclick=\"closeTagActAction("+noOfRecordCheck+","+jobForTeacherId+","+teacherId+","+jobId+")\"> <span class='icon-tag icon-large iconcolor'></span></span>&nbsp;"+headTag+"</a></td>");
								}
								else
									sb.append("<td style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><span class='icon-tag icon-large iconcolorhover'></span></span>&nbsp;"+headTag+"</td>");
							}
							else
							{
								//sb.append("<td style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><a href='javascript:void(0);' id='tagaction' onclick=\"closeTagActAction("+noOfRecordCheck+")\"> <span class='icon-tag icon-large iconcolor'></span></span>&nbsp;Tag</a></td>");
								if(/*jobOrder.getWritePrivilegeToSchool()==true &&*/ userMaster.getEntityType()==2) // condition only for T_T_J_O
								{
									//sb.append("<td style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><a href='javascript:void(0);' id='tagaction' onclick=\"closeTagActAction("+noOfRecordCheck+")\"> <span class='icon-tag icon-large iconcolor'></span></span>&nbsp;Tag</a></td>");
									sb.append("<td style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><a href='javascript:void(0);' id='tagaction' onclick=\"closeTagActAction("+noOfRecordCheck+","+jobForTeacherId+","+teacherId+","+jobId+")\"> <span class='icon-tag icon-large iconcolor'></span></span>&nbsp;"+headTag+"</a></td>");
								}
								else
								{
									if(jobOrder.getWritePrivilegeToSchool()!=null && jobOrder.getWritePrivilegeToSchool()== false && jobOrder.getSelectedSchoolsInDistrict()==1) // condition only for Second_T_J_O (job order with school Attached)
									{
										if(userMaster.getEntityType()==2)
											sb.append("<td style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><span class='icon-tag icon-large iconcolorhover'></span></span>&nbsp;"+headTag+"</td>");
										else
										{
											//sb.append("<td style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><a href='javascript:void(0);' id='tagaction' onclick=\"closeTagActAction("+noOfRecordCheck+")\"> <span class='icon-tag icon-large iconcolor'></span></span>&nbsp;Tag</a></td>");
											sb.append("<td style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><a href='javascript:void(0);' id='tagaction' onclick=\"closeTagActAction("+noOfRecordCheck+","+jobForTeacherId+","+teacherId+","+jobId+")\"> <span class='icon-tag icon-large iconcolor'></span></span>&nbsp;"+headTag+"</a></td>");
										}
									}
									else
									{
										// First Type Job Order means Only District Job Order
										//sb.append("<td style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><a href='javascript:void(0);' id='tagaction' onclick=\"closeTagActAction("+noOfRecordCheck+")\"> <span class='icon-tag icon-large iconcolor'></span></span>&nbsp;Tag</a></td>");
										sb.append("<td style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><a href='javascript:void(0);' id='tagaction' onclick=\"closeTagActAction("+noOfRecordCheck+","+jobForTeacherId+","+teacherId+","+jobId+")\"> <span class='icon-tag icon-large iconcolor'></span></span>&nbsp;"+headTag+"</a></td>");
									}
								}
							}
					}
					catch (Exception e) 
					{
						e.printStackTrace();
					}
					
				//}
				
			}
			
			
			
			//sb.append(pfcontent);
			sb.append("</table>");
			sb.append("</div>");	
			/*sb.append("<div class='modal-footer'>"+
			 		"<button class='btn' onclick='showProfileContentClose();'>Close</button>"+ 		
			 	"</div>"+
			"</div>");*/
			
			sb.append("</div>");
			sb.append("</div>");
			sb.append("</div>");
			
			if(fg!=0){                //By Ravindra
				sb.append("####");
			String sGridtableDisplayCSS="padding:10px;";
			String sGridtableDisplayCSS1="padding:0px;";
			String sGridLabelCss="padding:0px;padding-left:12px;padding-top:10px;color:black;";
			sb.append("<table min-width="+sWidthFoeDiv+" border='0' cellspacing=0 cellpadding=0 class='tablecgtbl'>");
			
			if(userMaster.getEntityType()!=1){
				Criterion criterion2=Restrictions.eq("jobOrder",jobOrder);
				Criterion criterion3=Restrictions.eq("teacherDetail",teacherDetail);
				int lst=teacherAnswerDetailsForDistrictSpecificQuestionsDAO.findByCriteria(criterion2,criterion3).size();
				if(lst>0){
				//sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'>District Specific Question</td></tr>");
				sb.append("<tr><td colspan=5 width='100%'>");
				sb.append("<div style='margin-left:10px;'><table border=0 width='100%' onmouseover=\"javascript:setGridProfileVariable('teacherCerti')\">");
					sb.append("<tr>");
				//		sb.append("<td style="+sGridtableDisplayCSS+" id='getDistrictSpecificQuestion'>");
				//		sb.append("</td>");
					sb.append("</tr>");
					sb.append("</table></div>");
				sb.append("</td></tr>");
			//	sb.append("<script>getDistrictSpecificQuestion_DivProfile("+jobOrder.getJobId()+","+teacherId+");</script>");
				}
			}
			
			if(userMaster!=null && userMaster.getDistrictId()!=null && userMaster.getDistrictId().getDistrictId().equals(1302010))
			if(teacherPersonalInfo!=null && teacherPersonalInfo.getEmployeeType()!=null && teacherPersonalInfo.getEmployeeType().equals(0)){
				sb.append("<tr><td style='"+sGridLabelCss+"' >");
				sb.append("<table ><tr><td colspan='2' class='divlable' style='padding:2px 2px 2px 24px;white-space:nowrap'>Why are you no longer employed with us?</td></tr>");
				sb.append("<tr><td style='padding:2px 0px 2px 24px;white-space:nowrap'>Ans: &nbsp;&nbsp;</td>");
				if(teacherPersonalInfo!=null && teacherPersonalInfo.getNoLongerEmployed()!=null && !teacherPersonalInfo.getNoLongerEmployed().equalsIgnoreCase(""))
					sb.append("<td>"+teacherPersonalInfo.getNoLongerEmployed()+"</td>");
				else{
					sb.append("<td class='divlableval'>N/A</td>");
				}
				sb.append("</tr>");
				sb.append("</table></td></tr>");
			}
			System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>Teacher Profile>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
			//************************************Start of Veteran Origin**************************//
			try
			{
				if(districtMaster!=null && !districtMaster.getDistrictId().equals(7800294))
				if( (districtPortfolioConfig==null || (districtPortfolioConfig.getVeteran()!=null && districtPortfolioConfig.getVeteran())))
				{
				String veteranOrigin = getVeteranOrigin(districtMaster, teacherDetail,teacherPersonalInfo);
				sb.append("<tr><td class='net-header-text' style='"+sGridLabelCss+"' >");
				sb.append(veteranOrigin);
				sb.append("</td> </tr>");
				}
			}
			catch(Exception e){e.printStackTrace();}
			//************************************End of Veteran Origin****************************//
			
			//************************************Start of Ethnic Origin**************************//
			try
			{
				/*if( (districtPortfolioConfig==null || (districtPortfolioConfig.getEthnicorigin()!=null && districtPortfolioConfig.getEthnicorigin())) && (districtMaster!=null && districtMaster.getDistrictId()!=null && districtMaster.getDistrictId().equals(1201470)) )
				{
				String ethnicOrigin = getEthnicOrigin(teacherPersonalInfo);
				sb.append("<tr><td class='net-header-text' style='"+sGridLabelCss+"' >");
				sb.append(ethnicOrigin);
				sb.append("</td> </tr>");
				}*/
			}
			catch(Exception e){e.printStackTrace();}
			//************************************End of Ethnic Origin**************************//
			
			//************************************Start of Ethnicity***************************//
			try
			{
				/*if( (districtPortfolioConfig==null || (districtPortfolioConfig.getEthinicity()!=null && districtPortfolioConfig.getEthinicity())) && (districtMaster!=null && districtMaster.getDistrictId()!=null && districtMaster.getDistrictId().equals(1201470)) )
				{
				String ethinicity = getEthnicity(teacherPersonalInfo);
				sb.append("<tr><td class='net-header-text' style='"+sGridLabelCss+"' >");
				sb.append(ethinicity.toString());
				sb.append("</td> </tr>");
				}*/
			}
			catch(Exception e){e.printStackTrace();}
			
			//************************************End of Ethnicity****************************//
			
			//************************************Start of Race*************************//
			try
			{
				/*if( (districtPortfolioConfig==null || (districtPortfolioConfig.getRace()!=null && districtPortfolioConfig.getRace())) && (districtMaster!=null && districtMaster.getDistrictId()!=null && districtMaster.getDistrictId().equals(1201470)) )
				{
				String race = getRace(teacherPersonalInfo);
				sb.append("<tr><td class='net-header-text' style='"+sGridLabelCss+"' >");
				sb.append(race);
				sb.append("</td> </tr>");
				}*/
			}
			catch(Exception e){e.printStackTrace();}
			//************************************End of Race***************************//
			
			
			//************************ Special Note ****************************************//
			if(userMaster!=null && userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==NC_HEADQUARTER){
				getSpecialNoteForTeacher(teacherDetail);
			}
			
			
			
			
			
			
			if(districtMaster!=null && !districtMaster.getDistrictId().equals(1201470) && !districtMaster.getDistrictId().equals(7800040) && !districtMaster.getDistrictId().equals(804800) && !districtMaster.getDistrictId().equals(7800294))
			if(districtPortfolioConfig==null || (districtPortfolioConfig.getGeneralKnowledgeExam()!=null && districtPortfolioConfig.getGeneralKnowledgeExam()) )
			{
			// start new row   >>> General Knowledge Exam
			   int geFlag=0;
			   sb.append("<tr><td>&nbsp;<td></tr>");
			   try{
					TeacherGeneralKnowledgeExam teacherGeneralKnowledgeExam=getGKEValuesForProfile(teacherDetail);
					if(teacherGeneralKnowledgeExam!=null){
						sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'>&nbsp;"+lblGeneralKnowledgeExam+":</td></tr>");
						sb.append("<tr>");
						sb.append("<td>");
							sb.append("<table border='0' cellspacing=0 cellpadding=0 class='tablecgtbl'>");
							sb.append("<tr>");
								strCss_value="padding-left:55px;";
								sb.append("<td style='"+strCss_value+"'>&nbsp;</td>");
								strCss_lbl="padding:2px;vertical-align:middle;";
								strCss_value="padding:2px;vertical-align:text-top;";
								sb.append("<td>"); 
								sb.append("<table border='0' cellspacing=0 cellpadding=0 class='tablecgtbl'>");
								sb.append("<tr>");
								strCss_lbl="padding:2px;padding-left:2px;vertical-align:middle;";
								//sb.append("<td class='divlable' style='"+strCss_lbl+"'>&nbsp;General Knowledge Exam: </td>");
								if(teacherGeneralKnowledgeExam!=null){
									geFlag=1;
									sb.append("<td class='divlable' style='"+strCss_lbl+";margin-top:-5px;'>"+lblExamStatus +"</td>");
								    sb.append("<td class='divlable' style='"+strCss_lbl+";margin-top:-5px;padding-left:60px;'>"+lblExamDate +"</td>");
								    sb.append("<td class='divlable' style='"+strCss_lbl+";margin-top:-5px;padding-left:60px;'>"+lblScoreReport+"</td>");
								    sb.append("<td class='divlable' style='"+strCss_lbl+";margin-top:-5px;padding-left:30px;'>"+lblNotes+"</td>");
								}
								sb.append("</tr>");
								sb.append("</table>");
							sb.append("</td>");
							sb.append("</tr>");
							sb.append("</table>");
						sb.append("</td>");
					   sb.append("</tr>");
					}else{
						sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'>&nbsp;"+lblGeneralKnowledgeExam+":&nbsp;&nbsp;<span class='divlableval'>"+lblNA+"</span></td></tr>");
					}
			   }catch (Exception e) {
					e.printStackTrace();
				}
			 
			   if(geFlag==1){
				   sb.append("<tr>");
					sb.append("<td>");
						sb.append("<table border='0' cellspacing=0 cellpadding=0 class='tablecgtbl'>");
						sb.append("<tr>");
							strCss_value="padding-left:55px;";
							sb.append("<td style='"+strCss_value+"'>&nbsp;</td>");
							strCss_lbl="padding:2px;vertical-align:middle;";
							strCss_value="padding:2px;vertical-align:text-top;";
							sb.append("<td>"); 
							sb.append("<table border='0' cellspacing=0 cellpadding=0 class='tablecgtbl'>");
							sb.append("<tr>");
							strCss_lbl="padding:2px;padding-left:2px;vertical-align:middle;";
							//sb.append("<td class='divlable' style='"+strCss_lbl+"padding-left:140px;'>&nbsp;</td>");
							try{
								TeacherGeneralKnowledgeExam teacherGeneralKnowledgeExam=getGKEValuesForProfile(teacherDetail);
								if(teacherGeneralKnowledgeExam!=null){
									if(teacherGeneralKnowledgeExam.getGeneralKnowledgeExamStatus().equals("P"))
										sb.append("<td class='divlableval' style='"+strCss_value+";margin-top:-5px;'>"+lblPass+"</td>");
									else if(teacherGeneralKnowledgeExam.getGeneralKnowledgeExamStatus().equals("F"))	
										sb.append("<td class='divlableval' style='"+strCss_value+";margin-top:-5px;'>"+lblFail+"</td>");
									
									sb.append("<td class='divlableval' style='"+strCss_value+";margin-top:-5px;padding-left:100px;'>"+Utility.convertDateAndTimeToDatabaseformatOnlyDate(teacherGeneralKnowledgeExam.getGeneralKnowledgeExamDate())+"</td>");
								    sb.append("<td  class='divlableval' style='"+strCss_value+";margin-top:-5px;padding-left:60px;'>&nbsp;<a data-original-title='Score Report' rel='tooltip' id='tpScoreReportGE' href='javascript:void(0);' onclick=\"downloadGeneralKnowledge('"+teacherGeneralKnowledgeExam.getGeneralKnowledgeExamId()+"','"+teacherGeneralKnowledgeExam.getTeacherDetail().getTeacherId()+"','tpScoreReportGE');"+windowFunc+"\">");
								    sb.append("<span class='icon-download-alt icon-large iconcolorBlue' style='padding-left:20px;'></span>");
								    sb.append("</a></td>");
								    sb.append("<td class='' style='"+strCss_value+";margin-top:-5px;padding-left:47px;'>");
								    sb.append(teacherGeneralKnowledgeExam.getGeneralExamNote());
								    sb.append("</td>");
								}
							}catch (Exception e) {
								e.printStackTrace();
							}
							sb.append("</tr>");
							sb.append("</table>");
						sb.append("</td>");
						sb.append("</tr>");
						sb.append("</table>");
					sb.append("</td>");
				   sb.append("</tr>");
					// End new row 
			   }
			}
			
			/*********************SWADESH******************************/
			
			   if(districtMaster!=null&& districtMaster.getDistrictId()==7800292)
			   {
				   
				   sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'>School Site Preference</td></tr>");
					sb.append("<tr><td colspan=5 width='100%'>");
						sb.append("<table border=0 width='100%'>");
							sb.append("<tr>");
								sb.append("<td style="+sGridtableDisplayCSS+">");
								sb.append(schoolpref(teacherDetail,jobOrder,districtMaster));
								sb.append("</td>");
							sb.append("</tr>");
						sb.append("</table>");
					sb.append("</td></tr>");
					//sb.append("<script>applyScrollOnTblSubjectAreaExam();</script>");
				   
			   }
			   
			   /************************SWADESH******************************/
			//************************In CG  Special Note ****************************************//
			System.out.println("In CG  Special Note .................");
			
			//&& userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==2
			//************************ Special Note ****************************************//
			//if(userMaster!=null && userMaster.getEntityType()==2&& userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==2){
				if(userMaster!=null &&  userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==NC_HEADQUARTER){		   
						   sb.append("<tr>");
						   sb.append("<td>");
							sb.append(getSpecialNoteForTeacher(teacherDetail));
							sb.append("</td>");
							sb.append("</tr>");
						   
				 }
								
			//************************ End CG Special Note ****************************************//
			
			if(districtMaster!=null && !districtMaster.getDistrictId().equals(1201470) && !districtMaster.getDistrictId().equals(7800040) && !districtMaster.getDistrictId().equals(804800) && !districtMaster.getDistrictId().equals(7800294))
			if(districtPortfolioConfig==null || (districtPortfolioConfig.getSubjectAreaExam()!=null && districtPortfolioConfig.getSubjectAreaExam()) )
			{/*
			   // subject Area Exam---- 
			   int saFlag=0;
			   try{
				TeacherSubjectAreaExam teacherSubjectAreaExam=getSAEValuesForProfile(teacherDetail);
				if(teacherSubjectAreaExam!=null){
					 sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'>&nbsp;"+lblSubjectAreaExam+":</td></tr>");
					 sb.append("<tr>");
						sb.append("<td>");
							sb.append("<table border='0' cellspacing=0 cellpadding=0 class='tablecgtbl'>");
							sb.append("<tr>");
								strCss_value="padding-left:55px;";
								sb.append("<td style='"+strCss_value+"'>&nbsp;</td>");
								strCss_lbl="padding:2px;vertical-align:middle;";
								strCss_value="padding:2px;vertical-align:text-top;";
								sb.append("<td>"); 
								sb.append("<table border='0' cellspacing=0 cellpadding=0 class='tablecgtbl'>");
								sb.append("<tr>");
								strCss_lbl="padding:2px;padding-left:2px;vertical-align:middle;";
								//sb.append("<td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblSubjectAreaExam+": </td>");
								if(teacherSubjectAreaExam!=null){
									saFlag=1;
									sb.append("<td class='divlable' style='"+strCss_lbl+";margin-top:-5px;'>"+lblExamStatus+"</td>");
								    sb.append("<td class='divlable' style='"+strCss_lbl+";margin-top:-5px;padding-left:60px;'>"+lblExamDate+"</td>");
								    sb.append("<td class='divlable' style='"+strCss_lbl+";margin-top:-5px;padding-left:60px;'>"+lblScoreReport+"</td>");
								}
								sb.append("</tr>");
								sb.append("</table>");
							sb.append("</td>");
							sb.append("</tr>");
							sb.append("</table>");
						sb.append("</td>");
					   sb.append("</tr>");
				}else{
					 sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'>&nbsp;"+lblSubjectAreaExam+": &nbsp;<span class='divlableval'>"+lblNA+"</sapn></td></tr>");
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
			
			if(saFlag==1){
				   sb.append("<tr>");
					sb.append("<td>");
						sb.append("<table border='0' cellspacing=0 cellpadding=0 class='tablecgtbl'>");
						sb.append("<tr>");
							strCss_value="padding-left:55px;";
							sb.append("<td style='"+strCss_value+"'>&nbsp;</td>");
							strCss_lbl="padding:2px;vertical-align:middle;";
							strCss_value="padding:2px;vertical-align:text-top;";
							sb.append("<td>"); 
							sb.append("<table border='0' cellspacing=0 cellpadding=0 class='tablecgtbl'>");
							sb.append("<tr>");
							strCss_lbl="padding:2px;padding-left:2px;vertical-align:middle;";
							//sb.append("<td class='divlable' style='"+strCss_lbl+"padding-left:140px;'>&nbsp;</td>");
							try{
								TeacherSubjectAreaExam teacherSubjectAreaExam=getSAEValuesForProfile(teacherDetail);
								if(teacherSubjectAreaExam!=null){
									if(teacherSubjectAreaExam.getExamStatus().equals("P"))
										sb.append("<td  class='divlableval' style='"+strCss_value+";margin-top:-5px;'>"+lblPass+"</td>");
									else if(teacherSubjectAreaExam.getExamStatus().equals("F"))	
										sb.append("<td  class='divlableval' style='"+strCss_value+";margin-top:-5px;'>"+lblFail+"</td>");
									
									sb.append("<td  class='divlableval' style='"+strCss_value+";margin-top:-5px;padding-left:100px;'>"+Utility.convertDateAndTimeToDatabaseformatOnlyDate(teacherSubjectAreaExam.getExamDate())+"</td>");
								    sb.append("<td  class='divlableval' style='"+strCss_value+";margin-top:-5px;padding-left:57px;'>&nbsp;<a data-original-title='Score Report' rel='tooltip' id='tpScoreReportSA' href='javascript:void(0);' onclick=\"downloadSubjectAreaExam('"+teacherSubjectAreaExam.getTeacherSubjectAreaExamId()+"','"+teacherSubjectAreaExam.getTeacherDetail().getTeacherId()+"','tpScoreReportSA');"+windowFunc+"\">");
								    sb.append(teacherSubjectAreaExam.getScoreReport());
								    sb.append("</a></td>");
								}
							}catch (Exception e) {
								e.printStackTrace();
							}
							sb.append("</tr>");
							sb.append("</table>");
						sb.append("</td>");
						sb.append("</tr>");
						sb.append("</table>");
					sb.append("</td>");
				   sb.append("</tr>");
					// End new row 
			   }
			*/}
			
			
			if(districtMaster!=null && !districtMaster.getDistrictId().equals(1201470) && !districtMaster.getDistrictId().equals(7800040) && !districtMaster.getDistrictId().equals(804800) && !districtMaster.getDistrictId().equals(7800294)){
				//subject==========================================			
					sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'>"+lblSubjectAreaExam+"</td></tr>");
					sb.append("<tr><td colspan=5 width='100%'>");
						sb.append("<table border=0 width='100%'>");
							sb.append("<tr>");
								sb.append("<td style="+sGridtableDisplayCSS+">");
								sb.append(getSubjectAreasGrid(teacherDetail));
								sb.append("</td>");
							sb.append("</tr>");
						sb.append("</table>");
					sb.append("</td></tr>");
					sb.append("<script>applyScrollOnTblSubjectAreaExam();</script>");
				//subject==========================================
			}
			///////////////////////////////////Starting of showing grid///////////////////////////////		
			System.out.println("0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000");
			
			if(districtPortfolioConfig==null || (districtPortfolioConfig.getCertification()!=null && !districtPortfolioConfig.getCertification().equals(0)) )
			{
			// Start Teacher Certifications Grid
			sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'>"+lblCerti+"</td></tr>");
			
			if(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==NC_HEADQUARTER){
				sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'>"+lblLicense+"</td></tr>");
				sb.append("<tr><td colspan=5 width='100%'>");
				sb.append("<table border=0 width='100%' onmouseover=\"javascript:setGridProfileVariable('teacherCerti')\">");
					sb.append("<tr>");
						sb.append("<td style="+sGridtableDisplayCSS+" id='divLicneseGridCertificationsGridDate'>");
						sb.append("</td>");
					sb.append("</tr>");
				sb.append("</table>");
			sb.append("</td></tr>");
			sb.append("<script>getTeacherLicenceGrid_DivProfile("+teacherDetail.getTeacherId()+");</script>");
			
			
			sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'>"+lblLEACandidatePortfolio+"</td></tr>");
			sb.append("<tr><td colspan=5 width='100%'>");
			sb.append("<table border=0 width='100%' onmouseover=\"javascript:setGridProfileVariable('teacherCerti')\">");
				sb.append("<tr>");
					sb.append("<td style="+sGridtableDisplayCSS+" id='divLEACandidatePotfolioDiv'>");
					sb.append("</td>");
				sb.append("</tr>");
				sb.append("</table>");
			sb.append("</td></tr>");
			sb.append("<script>getLEACandidatePortfolio_DivProfile("+teacherDetail.getTeacherId()+");</script>");
			sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'>"+lblCertiLicense+"</td></tr>");
		}
		
			sb.append("<tr><td colspan=5 width='100%'>");
				sb.append("<table border=0 width='100%' onmouseover=\"javascript:setGridProfileVariable('teacherCerti')\">");
					sb.append("<tr>");
						sb.append("<td style="+sGridtableDisplayCSS+" id='gridDataTeacherCertifications'>");
						sb.append("</td>");
					sb.append("</tr>");
				sb.append("</table>");
			sb.append("</td></tr>");
			sb.append("<script>getTeacherCertificationsGrid_DivProfile("+teacherDetail.getTeacherId()+");</script>");
			// End Teacher Certifications Grid
			}
			
			if(districtPortfolioConfig==null || (districtPortfolioConfig.getAcademic()!=null && !districtPortfolioConfig.getAcademic().equals(0)) )
			{
			// Start Teacher Academics Grid
			sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'>"+lblAcademics+"</td></tr>");
			sb.append("<tr><td colspan=5 width='100%'>");
				sb.append("<table border=0 width='100%' onmouseover=\"javascript:setGridProfileVariable('teacherAce')\">");
					sb.append("<tr>");
						sb.append("<td style="+sGridtableDisplayCSS+" id='gridDataTeacherAcademics'>");
						sb.append("</td>");
					sb.append("</tr>");
				sb.append("</table>"); 
			sb.append("</td></tr>");
			sb.append("<script>getTeacherAcademicsGrid_DivProfile("+teacherDetail.getTeacherId()+");</script>");
			// End Teacher Academics Grid
			}
			
			
			//sandeep Start Teacher education Grid
			  
			if(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==NC_HEADQUARTER){
				  
			       sb.append("<tr><td colspan=4 class='net-header-text' style='"+sGridLabelCss+"'><b>Education</b></td></tr>");
			      sb.append("<tr><th colspan=4 width='100%'>");
			       sb.append("<table border=0 width='100%' style='margin-left:10px;'>");
			        sb.append("<tr>");
			         sb.append("<th style="+sGridtableDisplayCSS1+" id='gridDataTeacherEducations'>");
			         sb.append("</th>");
			        sb.append("</tr>");
			       sb.append("</table>");
			      sb.append("</th></tr>");
			      sb.append("<script>getTeacherEducationGrid_DivProfile("+teacherDetail.getTeacherId()+");</script>");
			}     
		      // End Teacher Education Grid
			
			
			
			
			if(districtMaster!=null && districtMaster.getDistrictId().equals(1201470)){
			if(districtPortfolioConfig==null || (districtPortfolioConfig.getHonors()!=null && districtPortfolioConfig.getHonors()) )
			{
			// Start Teacher Honors Grid
			sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'><b>"+lblHonors+"</b></td></tr>");
			sb.append("<tr><th colspan=5 width='100%'>");
				sb.append("<table border=0 width='100%' onmouseover=\"javascript:setGridProfileVariable('teacherCerti')\">");
					sb.append("<tr>");
						sb.append("<td style="+sGridtableDisplayCSS+" id='gridDataTeacherHonors'>");
						sb.append("</td>");
					sb.append("</tr>");
				sb.append("</table>");
			sb.append("</th></tr>");
			sb.append("<script>getHonorsGrid("+teacherDetail.getTeacherId()+");</script>");
			// End Teacher Honors Grid
			}
			
			if(districtPortfolioConfig==null || (districtPortfolioConfig.getInvolvement()!=null && districtPortfolioConfig.getInvolvement()) )
			{
			//Start Involvement/Volunteer Work Grid//
			sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'><b>"+lblInvolvementVolunteerWork +"</b></td></tr>");
			sb.append("<tr><th colspan=5 width='100%'>");
			sb.append("<table border=0 width='100%' onmouseover=\"javascript:setGridProfileVariable('teacherInvolve')\">");
			sb.append("<tr>");
			sb.append("<th style="+sGridtableDisplayCSS+" id='gridDataTeacherInvolvementOrVolunteerWork'>");
			sb.append("</th>");
			sb.append("</tr>");
			sb.append("</table>");
			sb.append("</th></tr>");
			sb.append("<script>getInvolvementGrid("+teacherDetail.getTeacherId()+");</script>");
			//End Involvement/Volunteer Work Grid//
			}
			}
			if(districtPortfolioConfig==null || (districtPortfolioConfig.getEmployment()!=null && districtPortfolioConfig.getEmployment()) )
			{
			// Start employment Grid
			sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'>"+lblWorkExp+"</td></tr>");
			sb.append("<tr><td colspan=5 width='100%'>");
				sb.append("<table border=0 width='100%' onmouseover=\"javascript:setGridProfileVariable('workExp')\">");
					sb.append("<tr>");
						sb.append("<td style="+sGridtableDisplayCSS+" id='gridDataWorkExpEmployment'>");
						sb.append("</td>");
					sb.append("</tr>");
				sb.append("</table>");
			sb.append("</td></tr>");
			sb.append("<script>getPFEmploymentGrid_DivProfile("+teacherDetail.getTeacherId()+");</script>");
			// End employment Grid
			}
			
			// Start teacher assessment Grid
			sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'>"+lblAssessmentDetails+"</td></tr>");
			sb.append("<tr><td colspan=5 width='100%'>");
				sb.append("<table border=0 width='100%' onmouseover=\"javascript:setGridProfileVariable('teacherAssessment')\">");
					sb.append("<tr>");
						sb.append("<td style="+sGridtableDisplayCSS+" id='gridTeacherAssessment'>");
						sb.append("</td>");
					sb.append("</tr>");
				sb.append("</table>");
			sb.append("</td></tr>");
			sb.append("<script>getTeacherAssessmentGrid_DivProfile("+teacherDetail.getTeacherId()+");</script>");
			
			// End teacher assessment Grid
			
			if(districtPortfolioConfig==null || (districtPortfolioConfig.getReference()!=null && !districtPortfolioConfig.getReference().equals(0)) )
			{
			// Start Reference Grid
			sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+";padding-left: 0px;'>");
			   sb.append("<div class=''>");
			    sb.append("<div class='col-sm-6 col-md-6'>"+lblRef+"</div>");
		    	  if(userMaster.getRoleId().getRoleId().equals(2) || userMaster.getRoleId().getRoleId().equals(3))
		    		  sb.append("<div class='col-sm-6 col-md-6' style='text-align:  right;' ><a href='#' onclick='return showReferencesForm();'>+ Add Reference</a></div>");
		       
		    sb.append("</div></td></tr>");
		    	 
		    	 
		    	 
			//sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'>References</td></tr>");
			sb.append("<tr><td colspan=5 width='100%'>");
				sb.append("<table border=0 width='100%' onmouseover=\"javascript:setGridProfileVariable('References')\">");
					sb.append("<tr>");
						sb.append("<td style="+sGridtableDisplayCSS+" id='gridDataReference'>");
						sb.append("</td>");
					sb.append("</tr>");
				sb.append("</table>");
			sb.append("</td></tr>");
			sb.append("<script>getElectronicReferencesGrid_DivProfile("+teacherDetail.getTeacherId()+");</script>");
			// End Reference Grid
			
			sb.append("<tr><td style='padding-left: 15px;'>");
				sb.append("<div class='portfolio_Section_ImputFormGap' style='display: none;' id='divElectronicReferences'>");
					sb.append("<div class='row'>");
						sb.append(" <div class='col-sm-12 col-md-12'>");
							sb.append(" <div class='divErrorMsg' id='errordivElectronicReferences' style='display: block;''></div>");
						sb.append(" </div>");
					sb.append(" </div>");
			
			   sb.append("<iframe id='uploadFrameReferencesID' name='uploadFrameReferences' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'></iframe>");
			   sb.append("<form id='frmElectronicReferences' name='frmElectronicReferences' enctype='multipart/form-data' method='post' target='uploadFrameReferences' action='fileuploadservletforreferencesnoble.do' >");
			   sb.append("<input type='hidden' id='teacherId' name='teacherId' value="+teacherId+" >");	
			   sb.append("<div class='row'>");
						
					sb.append("<div class='col-sm-3 col-md-3'>");
						 sb.append("<label>Salutation</label>");
						   sb.append("<select class='form-control' id='salutation' name='salutation'>");
							    sb.append("<option value='0'></option>");
								sb.append("<option value='4'>"+lblDr+"</option>");
								sb.append("<option value='3'>"+lblMiss+"</option>");
								sb.append("<option value='2'>"+lblMr+"</option>");
								sb.append("<option value='1'>"+lblMrs+"</option>");
								sb.append("<option value='5'>"+lblMs+"</option>");													
							 sb.append("</select>");
							sb.append("</div>");

					sb.append("<div class='col-sm-3 col-md-3'>");
					  sb.append("<label>"+lblFname+"<span class='required'>*</span></label>");
					   sb.append("<input type='hidden' id='elerefAutoId'>");
					   sb.append("<input type='text' id='firstName' name='firstName' class='form-control' placeholder='' maxlength='50'>");
					sb.append("</div>");
					
					sb.append("<div class='col-sm-3 col-md-3'>");
					  sb.append("<label>"+lblLname+"<span class='required'>*</span></label>");
					sb.append("<input type='text' id='lastName' name='lastName' class='form-control' placeholder='' maxlength='50'>");
					sb.append("</div>");
		        sb.append("</div>");
		        
		        sb.append("<div class='row'>");
		          sb.append("<div class='col-sm-3 col-md-3'>");
		          if(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==NC_HEADQUARTER)
		          {
		        	  sb.append("<label>"+lblTitle+"<span class='required'>*</span></label>"); 
		          }
		          else
		          {
		        	  sb.append("<label>"+lblTitle+"</label>");
		          }
		        		 sb.append("<input type='text' id='designation' name='designation' class='form-control' placeholder='' maxlength='50'>");
		          sb.append("</div>");
				
		          sb.append("<div class=col-sm-6 col-md-6'>");
                  sb.append("<label>"+lblOrganizationEmp +"<span class='required'>*</span></label>");
		          sb.append("<input type='text' id='organization' name='organization' class='form-control' placeholder='' maxlength='50'>");
		         sb.append("</div>");
		       sb.append("</div>");
			
		       
		       sb.append("<div class='row '>");
		        sb.append("<div class='col-sm-3 col-md-3'>");
		    	 sb.append("<label>"+lblContNum +"<span class='required'>*</span>&nbsp;<a href='#' id='iconpophover7' rel='tooltip' data-original-title='Phone number (area code first)'><img src='images/qua-icon.png' width='15' height='15' alt=''></a></label>");
		    	 sb.append("<script type='text/javascript'>$('#iconpophover7').tooltip();</script>");  
		    	 sb.append("<input type='text' id='contactnumber' name='contactnumber' onKeyUp='numericFilter(this)'  class='form-control' placeholder='' maxlength='10'>");
		    	 sb.append("</div>");
			
		    	 sb.append("<div class='col-sm-6 col-md-6'>");
		    		sb.append("<label>"+lblEmail+"<span class='required'>*</span></label>");
		    			sb.append("<input type='text' id='email' name='email' class='form-control' placeholder='' maxlength='50'>");
		    	 sb.append("</div>");
		       sb.append("</div>");
		       
		       
		       sb.append(" <div class='row top15 hide' >");
		         sb.append("<div class='col-sm-3 col-md-3'>");
		    	    sb.append("<label>");
		    		 sb.append(lblRecommendLetter);
		    		 sb.append("</label>");
		    		 sb.append("<input type='hidden' id='sbtsource_ref' name='sbtsource_ref' value='0'/>");
		    		 sb.append("<input id='pathOfReferenceFile' name='pathOfReferenceFile' type='file' width='20px;'>");
		    			sb.append("<a href='javascript:void(0)' onclick='clearReferences()>"+lnkClear+"</a>");
		    	 sb.append("</div>");
		    		sb.append("<input type='text' id='pathOfReference'/>");
		    	 sb.append("<div class='col-sm-3 col-md-3' id='removeref' name='removeref' style='display: none;'>");
		    		sb.append("<label>&nbsp;&nbsp;");
		    	    sb.append("</label>");
		    	    sb.append("</div>");
		    	    sb.append("<span id='divRefName'>");
		    	    sb.append("</span>");
	                sb.append("&nbsp;&nbsp;&nbsp;<a href='javascript:void(0)' onclick='removeReferences()'>"+lnkRemo+"</a>&nbsp;&nbsp;&nbsp;");
	                //sb.append(" <a href='#' id='iconpophover7' rel='tooltip' data-original-title='Remove recommendation letter !'><img src='images/qua-icon.png' width='15' height='15' alt=''></a>");
	               
		         sb.append("</div>");
	          sb.append("</div>");
	          
	          String cntTP="show";
		       if(userMaster.getDistrictId()!=null && userMaster.getDistrictId().getDistrictId().equals(7800038)){
		    	   cntTP="hide";
		       }
	          sb.append(" <div class='row top15 "+cntTP+"'>");
	        	sb.append("<div class='col-sm-12 col-md-12'>");
	        	 sb.append("<label>"+lblCanThisPerson +"</label>");
	        	  sb.append("<div class='' id='' style='height: 40px;'>");
	        		  sb.append("<div class='radio inline col-sm-1 col-md-1'>");
	        		  sb.append("<input type='radio' checked='checked' id='rdcontacted1' value='1'  name='rdcontacted'> Yes");
	        		  sb.append("</div></br>");
					    sb.append("<div class='radio inline col-sm-1 col-md-1' style='margin-left:20px;margin-top:-0px;'>");
					    sb.append("<input type='radio' id='rdcontacted0' value='0' name='rdcontacted'> No");
					  sb.append(" </div>");
					sb.append("  </div>");
				sb.append("</div>");
			sb.append("</div>");
			
			String hlhyk="hide";
		       if(userMaster.getDistrictId()!=null && userMaster.getDistrictId().getDistrictId().equals(4218990)){
		    	   hlhyk="show";
		       }
			sb.append(" <div class='row top10 "+hlhyk+"'>");
       	 	sb.append("<div class='col-sm-5 col-md-5'>");
	        sb.append("<label>"+lblHowLong +"</label><input type='text' id='longHaveYouKnow' name='longHaveYouKnow' class='form-control' placeholder=''>");
	        sb.append("</div>");
	        sb.append("</div>");
			
			sb.append(" <div class='row top10'>");
        	 sb.append("<div class='col-sm-12 col-md-12'>");
	        	sb.append("<label>"+lblReferenceDetails +"</label>");
	        	sb.append("<div id='referenceDetailstext' >");
	        	  sb.append("<textarea id='referenceDetails' name='referenceDetails'></textarea>");
        	     sb.append("</div>");
        	 sb.append("</div>");
        	sb.append("</div>");
			
       sb.append("<div class='row top5'>");
    	sb.append("<div class='col-sm-3 col-md-3 idone'>");
    		sb.append("<a href='#' style='cursor: pointer; text-decoration:none;' onclick='return insertOrUpdateElectronicReferencesM(0)'>"+lnkImD +"</a>&nbsp;");
    		sb.append(" &nbsp;&nbsp;<a href='#' style='cursor: pointer; text-decoration:none;' onclick='return hideElectronicReferencesForm()'>");
    		sb.append("Cancel");
    		 sb.append("</a>");
    		sb.append("</div>");
    	sb.append("</div>");
											
		    sb.append("</form>");
		
		    sb.append("</div>");
		    sb.append("</div>");
		       
		   sb.append("</td></tr>");		
			}
			//Ajay Jain
			if(userMaster.getDistrictId()!=null && userMaster.getDistrictId().getDistrictId()==3904374 && jobOrder.getJobCategoryMaster().getJobCategoryName().equals("Substitute Teachers"))
			{
				sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+";padding-left: 0px;'>");
				sb.append("<div class=''>");
				sb.append("<div class='col-sm-6 col-md-6'>Background Check</div>");				  
				sb.append("</div></td></tr>");
				sb.append("<tr><td colspan=5 width='100%'>");
				sb.append("<table width='100%'>");
				sb.append("<tr>");
				sb.append("<td style="+sGridtableDisplayCSS+"><div id='gridDataBackgroundCheck' class=''></div>");
				sb.append("</td>");
				sb.append("</tr>");
				sb.append("</table>");
				sb.append("</td></tr>");
				sb.append("<script>getBackgroundCheckGrid_DivProfile("+teacherDetail.getTeacherId()+");</script>");
			}	
			if(districtPortfolioConfig==null || (districtPortfolioConfig.getVideoLink()!=null && districtPortfolioConfig.getVideoLink()) )
			{
			// Start Video Grid
		   
			sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+";padding-left: 0px;'>");
			   sb.append("<div class=''>");
			    sb.append("<div class='col-sm-6 col-md-6'>Video Links</div>");
		      
		    	  if(userMaster.getRoleId().getRoleId().equals(2) || userMaster.getRoleId().getRoleId().equals(3))
		    	  {
		    		  sb.append("<div class='col-sm-6 col-md-6' style='text-align:  right;' ><a href='#' onclick='return showVideoLinksForm();'>"+lnkAddViLnk+"</a></div>");
			    	  sb.append("<tr><td style='padding-left: 15px;'>");
			    	  sb.append("<label class ='top10'><strong>"+lblPleaseInclude+"</strong></label>");
			    	  sb.append("</tr></td>");
		    	  }
		      
		    sb.append("</div></td></tr>");
			//sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'>Video Links</td></tr>");
			sb.append("<tr><td colspan=5 width='100%'>");
				sb.append("<table width='100%' onmouseover=\"javascript:setGridProfileVariable('videoLink')\">");
					sb.append("<tr>");
						//sb.append("<td style="+sGridtableDisplayCSS+" id='gridDataVideoLink'>");
					
					//System.out.println(" pppppppppppppppppppppppppppppppppppppppppppppp ");	
					
						sb.append("<td style="+sGridtableDisplayCSS+"><div id='gridDataVideoLink' class=''></div>");
						sb.append("</td>");
					sb.append("</tr>");
				sb.append("</table>");
			sb.append("</td></tr>");
			sb.append("<script>getVideoLinksGrid_DivProfile("+teacherDetail.getTeacherId()+");</script>");
			// End Video Grid
			
//VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVvide formVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV
			sb.append("<iframe id='uploadFrameVideoID' name='uploadFrameVideo' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'></iframe>");
			sb.append("<tr><td	style='padding-left: 15px;'>");
			sb.append("<div class='top15' style='display: none' id='divvideoLinks' onkeypress='return checkForEnter(event);'>");
			 sb.append("<div class='divErrorMsg' id='errordivvideoLinks' style='display: block;'></div>");
			 sb.append("<form class='' id='frmvideoLinks' name='frmvideoLinks' onsubmit='return false;' method='post'  enctype='multipart/form-data' target='uploadFrameVideo' action='certificationVideoUploadServlet.do?teacherIdForVideo="+teacherDetail.getTeacherId()+"'>");
				 sb.append("<div class='row'>");
					 sb.append("<div class='col-sm-10 col-md-10' style='max-width: 800px;'>");
						 sb.append("<label>"+lnkViLnk+"</label>");
						 sb.append("<input type='hidden' id='videolinkAutoId' name='videolinkAutoId'>");
						 sb.append("<input type='text' id='videourl' name='videourl' class='form-control' placeholder='' maxlength='200'>");
					 sb.append("</div>");
					 sb.append("</div>");
					 
					 sb.append("<div class='row'>");
					 sb.append("<div class='col-sm-5 col-md-5' style='max-width: 800px;'><br/>");
						 sb.append("<label>"+lblVideo+"</label>");
						 sb.append("<input type='file' id='videofile' name='videofile'>");
						 sb.append("<span style='display: none; visibility: hidden;' id='video'></span>");
					 sb.append("</div>");
					 sb.append("<span class='col-sm-6 col-md-6'> <br/><br/>");
					 sb.append(lblSupportedVideoFormats );
					 sb.append("</span>");
				 sb.append("</div>");
						
					 sb.append("<div class='row top10'>");
				     sb.append("<div class='col-sm-4 col-md-4 idone'>");
				    	 sb.append("<a href='javascript:void(0)' onclick='insertOrUpdatevideoLinks();'>"+lnkImD+"</a>&nbsp;&nbsp;&nbsp;");
				    	 sb.append("<a href='#'	onclick='return hideVideoLinksForm()'>Cancel");
						 sb.append("</a>");
				     sb.append("</div>");
				 sb.append("</div>");
			 sb.append("</form>");
		 sb.append("</div>");
	 sb.append("</div>");
	 sb.append("</td></tr>");
			}
//VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV  EENNNNEEEDD   VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV
			
			if(districtPortfolioConfig==null || (districtPortfolioConfig.getAdditionalDocuments()!=null && districtPortfolioConfig.getAdditionalDocuments()) )
			{
			//Additional Documents
			sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'>"+lnkAddDocu+"</td></tr>");
			sb.append("<tr><td colspan=5 width='100%'>");
				sb.append("<table width='100%' onmouseover=\"javascript:setGridProfileVariable('document')\">");
					sb.append("<tr>");
						sb.append("<td style="+sGridtableDisplayCSS+"><div id='divGridAdditionalDocuments' class=''></div>");
						sb.append("</td>");
					sb.append("</tr>");
				sb.append("</table>");
			sb.append("</td></tr>");
			sb.append("<script>getadddocumentGrid_DivProfile("+teacherDetail.getTeacherId()+");</script>");		
			//End Additional Documents
			}
			if(userMaster.getDistrictId()!=null && (userMaster.getDistrictId().getDistrictId().equals(4218990) || userMaster.getDistrictId().getDistrictId().equals(7800040))){
			//Start language profiency
				sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'>"+lblLanguageProfiency +"</td></tr>");
				sb.append("<tr><td colspan=5 width='100%'>");
				sb.append("<table width='100%' onmouseover=\"javascript:setGridProfileVariable('languageProfiency')\">");
				sb.append("<tr>");
				sb.append("<td style="+sGridtableDisplayCSS+"><div id='divGridlanguageProfiency' class=''></div>");
				sb.append("</td>");
				sb.append("</tr>");
				sb.append("</table>");
				sb.append("</td></tr>");
				sb.append("</table>");
				sb.append("<script>getlanguage_DivProfile("+teacherDetail.getTeacherId()+");</script>");
			
			//end language profiency
			}
		//	System.out.println(" getDistrictAssessmentFlag(teacherDetail) >>>>>>>>>>> "+getDistrictAssessmentFlag(teacherDetail));
			if(districtMaster!=null && districtMaster.getDistrictId().equals(4218990))
			if(getDistrictAssessmentFlag(teacherDetail))
			{
				//Start District Assessment Details
				sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'>"+lblAssessmentDetails +"</td></tr>");
				sb.append("<tr><td colspan=5 width='100%'>");
					sb.append("<table width='100%' onmouseover=\"javascript:setGridProfileVariable('assessmentDetails')\">");
						sb.append("<tr>");
							sb.append("<td style="+sGridtableDisplayCSS+"><div id='divGridAssessmentDetails' class=''></div>");
							sb.append("</td>");
						sb.append("</tr>");
					sb.append("</table>");
				sb.append("</td></tr>");
				sb.append("</table>");
				sb.append("<script>getdistrictAssessmetGrid_DivProfile("+teacherDetail.getTeacherId()+");</script>");
				//End District Assessment Details
			}
			
			/*
             ************************************************************
             				Start Transcript Verification
             ************************************************************ 
             * */
            String candidateFname 	 = "";
            String candidateLname 	 = "";
            String candidateFullName = "";
            if(teacherDetail!=null){
            	candidateFname = teacherDetail.getFirstName()==null?"":teacherDetail.getFirstName();
            	candidateLname = teacherDetail.getLastName()==null?"":teacherDetail.getLastName();
            	candidateFullName 	= 	candidateFname+" "+candidateLname;
            	candidateFullName	=	candidateFullName.replace("'","\\'");
            }
            if(userMaster!=null && userMaster.getDistrictId()!=null && userMaster.getDistrictId().getDistrictId().equals(1200390)){
            sb.append("<table>");
            sb.append("<tr>");
            	sb.append("<td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'>Transcript Verification : </td>");
            	sb.append("<td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'><a href='javascript:void:(0);' onclick=\"return verifyTranscript('"+teacherId+"','"+candidateFullName+"')\">Transcript Verification<a></td>");
            sb.append("</tr>");
            sb.append("</table>");
            }
            /*
             ************************************************************
             				End Transcript Verification
             ************************************************************ 
             * */
			
            //Start district specific question                                                                 
            Integer chJobId = Integer.parseInt(jobId);
            String pfcontent = candidateGridSubAjax.getDistrictSpecificPortfolioQuestions(chJobId,teacherId);           
        	   
            if(pfcontent!=null && !pfcontent.equals("")){
                            sb.append("<table><tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'>"+headMngDistSpeciQues+"</td></tr></table>");
                            if(districtMaster!=null && districtMaster.getDistrictId().equals(7800038))
                            sb.append("<div class='row '>");
                            else
                            	//sb.append("<div class='row'>");
                            sb.append("<div class='row ' style='pointer-events: none;'>");	
                            sb.append("<div class='col-sm-12 col-md-12'><div id='errordivspecificquestion' class='required' style='margin-left:10px;'></div><table style='margin-left:10px;' width='90%'>"+pfcontent+"</table>");
                            sb.append("</div></div>");
                            if(userMaster.getDistrictId()!=null && userMaster.getDistrictId().getDistrictId().equals(7800038))
                            if(userMaster.getRoleId().getRoleId().equals(2)){
                            	sb.append("<div class='col-sm-12 col-md-12'><button class='btn btn-primary' type='button' onclick=\"javascript:saveDistrictSpecificQues('"+teacherId+"')\">Save Question</button></div>");
                            }
            }
            
//End district specific question
                

//			sb.append("</div>");
//			sb.append("<div class='custom-div-border'>");
//			//sb.append("<table border=0 width='100%'><tr bgcolor='#F2FAEF'><td style='text-align:center;vertical-align:middle;padding-top:10px;padding-bottom:10px;'width='35%'><a href='javascript:void(0);' onclick=\"getCommunicationsDiv(0,"+jobForTeacher.getJobForTeacherId()+",'"+teacherDetail.getTeacherId()+"',"+jobOrder.getJobId()+");\" ><span class='icon-dropbox icon-large iconcolor'></span>&nbsp;Communications</a></td>");
//			sb.append("<DIV>");
//			sb.append("<table border=0 width='100%'><tr bgcolor='#F2FAEF'><td style='text-align:center;vertical-align:middle;padding-top:10px;padding-bottom:10px;'width='35%'><a href='javascript:void(0);' onclick=\"getCommunicationsDiv(0,"+jobForTeacherId+",'"+teacherDetail.getTeacherId()+"',"+jobOrder.getJobId()+");\" ><span class='icon-dropbox icon-large iconcolor'></span>&nbsp;Communications</a></td>");
//			if(userMaster.getEntityType()==1)
//			{
//				sb.append("<td style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><span class='icon-save icon-large iconcolorhover'></span>&nbsp;Save</td>" +
//						"<td style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><span class='icon-share icon-large iconcolorhover'></span>&nbsp;Share</td>"+
//						"<td style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><span class='icon-tag icon-large iconcolorhover'></span></span>&nbsp;Tag</td>");
//			}
//			else
//			{
//				sb.append("<td style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><a href='javascript:void(0);' id='saveaction' onclick=\"saveToFolder("+jobForTeacherId+",1)\"><span class='icon-save icon-large iconcolor'></span>&nbsp;Save</a></td>");
//				sb.append("<td style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><a href='javascript:void(0);' id='shareaction' onclick=\"displayShareFolder("+jobForTeacherId+",1)\"><span class='icon-share icon-large iconcolor'></span>&nbsp;Share</a></td>");
//
//				//System.out.println(" Gagan : Condition for writePrivilegeToSchool only District and School  ");
//				/*	if(jobOrder.getWritePrivilegeToSchool()==false)
//				{
//					sb.append("<td style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><a href='javascript:void(0);' id='tagaction' onclick=\"closeTagActAction("+noOfRecordCheck+")\"> <span class='icon-tag icon-large iconcolor'></span></span>&nbsp;Tag</a></td>");
//				}
//				else
//				{*/
//					try
//					{
//						//System.out.println("Privilege  : "+jobOrder.getWritePrivilegeToSchool()+" EntityType : "+userMaster.getEntityType());
//						//System.out.println(" Size  "+jobOrder.getSchool().size()+" Else Block "+(userMaster.getSchoolId().getSchoolId()+" == "+jobOrder.getSchool().get(0).getSchoolId())+"");
//							if(jobOrder.getWritePrivilegeToSchool()==true && userMaster.getEntityType()==3)
//							{
//								if((userMaster.getSchoolId().getSchoolId().equals(jobOrder.getSchool().get(0).getSchoolId()) )  )
//								{
//									System.out.println(" Third Type Job Order "+jobId);
//									//sb.append("<td style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><a href='javascript:void(0);' id='tagaction' onclick=\"closeTagActAction("+noOfRecordCheck+")\"> <span class='icon-tag icon-large iconcolor'></span></span>&nbsp;Tag</a></td>");
//									sb.append("<td style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><a href='javascript:void(0);' id='tagaction' onclick=\"closeTagActAction("+noOfRecordCheck+","+jobForTeacherId+","+teacherId+","+jobId+")\"> <span class='icon-tag icon-large iconcolor'></span></span>&nbsp;Tag</a></td>");
//								}
//								else
//									sb.append("<td style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><span class='icon-tag icon-large iconcolorhover'></span></span>&nbsp;Tag</td>");
//							}
//							else
//							{
//								//sb.append("<td style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><a href='javascript:void(0);' id='tagaction' onclick=\"closeTagActAction("+noOfRecordCheck+")\"> <span class='icon-tag icon-large iconcolor'></span></span>&nbsp;Tag</a></td>");
//								if(jobOrder.getWritePrivilegeToSchool()==true && userMaster.getEntityType()==2) // condition only for T_T_J_O
//								{
//									//sb.append("<td style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><a href='javascript:void(0);' id='tagaction' onclick=\"closeTagActAction("+noOfRecordCheck+")\"> <span class='icon-tag icon-large iconcolor'></span></span>&nbsp;Tag</a></td>");
//									sb.append("<td style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><a href='javascript:void(0);' id='tagaction' onclick=\"closeTagActAction("+noOfRecordCheck+","+jobForTeacherId+","+teacherId+","+jobId+")\"> <span class='icon-tag icon-large iconcolor'></span></span>&nbsp;Tag</a></td>");
//								}
//								else
//								{
//									if(jobOrder.getWritePrivilegeToSchool()== false && jobOrder.getSelectedSchoolsInDistrict()==1) // condition only for Second_T_J_O (job order with school Attached)
//									{
//										if(userMaster.getEntityType()==2)
//											sb.append("<td style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><span class='icon-tag icon-large iconcolorhover'></span></span>&nbsp;Tag</td>");
//										else
//										{
//											//sb.append("<td style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><a href='javascript:void(0);' id='tagaction' onclick=\"closeTagActAction("+noOfRecordCheck+")\"> <span class='icon-tag icon-large iconcolor'></span></span>&nbsp;Tag</a></td>");
//											sb.append("<td style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><a href='javascript:void(0);' id='tagaction' onclick=\"closeTagActAction("+noOfRecordCheck+","+jobForTeacherId+","+teacherId+","+jobId+")\"> <span class='icon-tag icon-large iconcolor'></span></span>&nbsp;Tag</a></td>");
//										}
//									}
//									else
//									{
//										// First Type Job Order means Only District Job Order
//										//sb.append("<td style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><a href='javascript:void(0);' id='tagaction' onclick=\"closeTagActAction("+noOfRecordCheck+")\"> <span class='icon-tag icon-large iconcolor'></span></span>&nbsp;Tag</a></td>");
//										sb.append("<td style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><a href='javascript:void(0);' id='tagaction' onclick=\"closeTagActAction("+noOfRecordCheck+","+jobForTeacherId+","+teacherId+","+jobId+")\"> <span class='icon-tag icon-large iconcolor'></span></span>&nbsp;Tag</a></td>");
//									}
//								}
//							}
//					}
//					catch (Exception e) 
//					{
//						e.printStackTrace();
//					}
//					
//				//}
//				
//			}
//			
//			
//			
//			//sb.append(pfcontent);
//			sb.append("</table>");
//			sb.append("</div>");	
//			/*sb.append("<div class='modal-footer'>"+
//			 		"<button class='btn' onclick='showProfileContentClose();'>Close</button>"+ 		
//			 	"</div>"+
//			"</div>");*/
//			
//			sb.append("</div>");
//			sb.append("</div>");
//			sb.append("</div>"); 
			
		}
		}catch (Exception e) {
			e.printStackTrace();
		}
		finally
		{
			sb.append("<script>$('textarea').jqte();$('.tooltipEmpNo').tooltip();</script>");
			if(fg!=0)
			return sb.toString().split("####")[1];	
			else	
			return sb.toString();
		}
		
	}
	
	private Object schoolpref(TeacherDetail teacherDetail,JobOrder jobOrder,DistrictMaster districtMaster) {

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		StringBuffer sb = new StringBuffer();		
		try 
		{
			 List<SchoolPreferencebyCandidate> schoolPreferencebyCandidate=new ArrayList<SchoolPreferencebyCandidate>();
			 schoolPreferencebyCandidate=schoolPreferenceByCandidateDAO.getschoolpreferencebyTeacher(teacherDetail, jobOrder, districtMaster);

			sb.append("<table border='0' id=''  width='100%' class='table table-striped'  >");
			sb.append("<thead class='bg'>");
			sb.append("<tr height='30px;'>");
			String responseText="";
			
			sb.append("<th valign='top'>School Name</th>");
			
			sb.append("<th valign='top'>School Preference</th>");
			
			sb.append("</tr>");
			sb.append("</thead>");
			
			if(schoolPreferencebyCandidate!=null && schoolPreferencebyCandidate.size()>0)
			for(SchoolPreferencebyCandidate objTAD:schoolPreferencebyCandidate)
			{
				sb.append("<tr height='30px;'>");
				
				sb.append("<td>");
				sb.append(objTAD.getSchoolMaster().getSchoolName());
				sb.append("</td>");
				
				sb.append("<td>");
				sb.append(objTAD.getPreference());
				sb.append("</td>");
				
				sb.append("</tr>");
			}
			
			if(schoolPreferencebyCandidate==null || schoolPreferencebyCandidate.size()==0)
			{
				sb.append("<tr>");
				sb.append("<td colspan='2'>");
				sb.append(Utility.getLocaleValuePropByKey("msgNorecordfound", locale));
				sb.append("</td>");
				sb.append("</tr>");
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return sb.toString();
	}

	/*private String getFunction(){
		System.out.println("::::::::::::::::::::::::::::::::::::::::::Call my methoid:::::::::::::::::::::::::::::::::::::::::");
		StringBuffer sb=new StringBuffer();
		sb.append("function getEmployeeNumberInfo(table){var div=\"<div class='modal hide in' id='employeeInfo' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'><div class='modal-dialog' style='width:650px;'><div class='modal-content'><div class='modal-header'><button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button><h4 class='modal-title' id='myModalLabel' style='color:white;'>Employee Information</h4></div><div class='modal-body' id='tableData'></div><div class='modal-footer'><button type='button' class='btn btn-default' id='confirmFalse'>Ok</button></div></div></div></div>\";"+
				"$('#cgTeacherDivMaster').after(div);$('#cgTeacherDivMaster').modal('hide');$('#employeeInfo').modal({show:true,backdrop: 'static',keyboard: false,});"+
				"$('#employeeInfo #tableData').html(table); $('#confirmFalse').click(function(){  	$('#employeeInfo').modal('hide');    $('#employeeInfo').remove(); $('#cgTeacherDivMaster').modal('show');  });"+
				"}");
		sb.append("function openEmployeeNumberInfo(empId){	TeacherProfileViewInDivAjax.openEmployeeNumberInfo(empId,{async: false,cache: false,errorHandler:handleError,callback:function(data);{getEmployeeNumberInfo(data);}});}");
		
		System.out.println("sb============="+sb.toString());
		return sb.toString();
	}*/
	public String openEmployeeNumberInfo(String empId){
		StringBuffer sb=new StringBuffer();
		try {
			sb.append(JeffcoAPIController.getEmployeeInfoByEmployeeNumber(empId));
		} catch (Exception e) {
			e.printStackTrace();
			sb.append("1####Information could not be found. Please try again.");
		}
		return sb.toString();
	}
	
      public String openEmployeeNumberInfoOther(Integer empId){
    	   	
    	  System.out.println("Locale value :"+locale);
    	  WebContext context;
  		  context = WebContextFactory.get();
  		  HttpServletRequest request = context.getHttpServletRequest();
  		  HttpSession session = request.getSession(false);
  		  if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
  		  {
  			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
  		  }
    	  
    	    System.out.println("openEmployeeNumberInfoOther(empid):"+empId);
    	    String strCss_value = "padding-left:55px;";
    	    String  strCss_lbl="padding:2px;padding-left:8px;padding-top:-20px;";
    	    List<EmployeeMaster>	employeeNumberInfo = employeeMasterDAO.getEmployeeInfoByEmpId(empId);
    		StringBuffer sb=new StringBuffer();
		    try {
		    	 if(employeeNumberInfo!=null && !employeeNumberInfo.equals(""))
		         {
		        	 
		            sb.append("<tr><th class='divlable' style='"+strCss_lbl+"'>Employee&nbsp;Name:</th><th class='divlableval' style='"+strCss_value+"'>&nbsp;"+employeeNumberInfo.get(0).getFirstName()+"&nbsp;"+employeeNumberInfo.get(0).getMiddleName()+"&nbsp;"+employeeNumberInfo.get(0).getLastName()+"</th></tr>");
		           if(employeeNumberInfo.get(0).getOMN()!=null && !employeeNumberInfo.get(0).getOMN().equals("")){
		    	       sb.append("<tr><th class='divlable' style='"+strCss_lbl+"'>OME&nbsp;:</th><th class='divlableval' style='"+strCss_value+"'>&nbsp;"+employeeNumberInfo.get(0).getOMN()+"</th></tr>");
		           }
		    	   else{
		        	   sb.append("<tr><th class='divlable' style='"+strCss_lbl+"'>OME&nbsp;:</th><th class='divlableval' style='"+strCss_value+"'>&nbsp;N/A</th></tr>");
		    	   }
		           sb.append("<tr><th class='divlable' style='"+strCss_lbl+"'><Strong>Employee Number&nbsp;:</Strong></th><th class='divlableval' style='"+strCss_value+"'>&nbsp;"+empId+"</th></tr>");
		           if(employeeNumberInfo.get(0).getSeniority_number()!=null&&!employeeNumberInfo.get(0).getSeniority_number().equals("")){
		    	   sb.append("<tr><th class='divlable' style='"+strCss_lbl+"'>Seniority Number&nbsp;:</th><th class='divlableval' style='"+strCss_value+"'>&nbsp;"+employeeNumberInfo.get(0).getSeniority_number()+"</th></tr>");
		           }else{
		        	   sb.append("<tr><th class='divlable' style='"+strCss_lbl+"'>Seniority Number&nbsp;:</th><th class='divlableval' style='"+strCss_value+"'>&nbsp;N/A</th></tr>");
		           }
		           if(employeeNumberInfo.get(0).getFTE()!=null&&!employeeNumberInfo.get(0).getFTE().equals("")){
		        	   sb.append("<tr><th class='divlable' style='"+strCss_lbl+"'>FTE&nbsp;:</th><th class='divlableval' style='"+strCss_value+"'>&nbsp;"+employeeNumberInfo.get(0).getFTE()+"</th></tr>"); 
		           }else{
		        	   sb.append("<tr><th class='divlable' style='"+strCss_lbl+"'>FTE&nbsp;:</th><th class='divlableval' style='"+strCss_value+"'>&nbsp;N/A</th></tr>");
		           }
		            if(employeeNumberInfo.get(0).getPosition()!=null&&!employeeNumberInfo.get(0).getPosition().equals("")){
		    	      sb.append("<tr><th class='divlable' style='"+strCss_lbl+"'>Position Name&nbsp;:</th><th class='divlableval' style='"+strCss_value+"'>&nbsp;"+employeeNumberInfo.get(0).getPosition()+"</th></tr>");
		    	   }
		    	   else{
		    		   sb.append("<tr><th class='divlable' style='"+strCss_lbl+"'>Position Name&nbsp;:</th><th class='divlableval' style='"+strCss_value+"'>&nbsp;N/A</th></tr>");
		    	   }
		         }else{
		        	 sb.append("1####Information could not be found. Please try again.");
		         }
		   
		   
		        } catch (Exception e) {
			     e.printStackTrace();
			 sb.append("1####Information could not be found. Please try again.");
		     }
		return sb.toString();
	}
	
	public String showProfileContentForTeacher(Integer teacherId,Integer noOfRecordCheck,String sVisitLocation,DistrictMaster districtMaster_Input,int fg)
	{
		System.out.println(" >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> showProfileContentForTeacher <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
		String sGridtableDisplayCSS1="padding:0px;font-weight: normal;";
		//TestTool.getTraceTimeTPAP("TPAP 200");
		//System.out.println(sVisitLocation+"aaaaaaaaaaaa");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		DistrictMaster districtMaster=null;
		SchoolMaster schoolMaster=null;
		HeadQuarterMaster hqMaster=null;
		UserMaster userMaster=null;
		boolean nobleflag=false;
		int entityID=0,roleId=0;
		boolean noEPIFlag=false;
		boolean jeffcoFlag=false;
		if (session == null || session.getAttribute("userMaster") == null) {
			//return "false";
		}else{
			userMaster=(UserMaster)session.getAttribute("userMaster");
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
			if(userMaster.getDistrictId()!=null){
				districtMaster=userMaster.getDistrictId();
				if(districtMaster.getNoEPI()==null ||(districtMaster.getNoEPI()!=null && districtMaster.getNoEPI()==false)){
					noEPIFlag=true;
				}

				if(districtMaster.getDistrictId()==7800038)
					nobleflag=true;
				//804800
				if(districtMaster.getDistrictId()==804800)
				jeffcoFlag=true;
			}
			else if(userMaster.getHeadQuarterMaster()!=null)
			{
				hqMaster=userMaster.getHeadQuarterMaster();
				if(hqMaster.getNoEPI()==null ||(hqMaster.getNoEPI()!=null && hqMaster.getNoEPI()==false)){
					noEPIFlag=true;
				}
			}
			if(userMaster.getSchoolId()!=null){
				schoolMaster =userMaster.getSchoolId();
			}	
			entityID=userMaster.getEntityType();
		}
		
		//TestTool.getTraceTimeTPAP("TPAP 201");
		boolean achievementScore=false,tFA=false,demoClass=false,JSI=false,teachingOfYear =false,expectedSalary=false,fitScore=false,pnqDis=false;
		if(entityID==1)
		{
			achievementScore=true;tFA=true;demoClass=true;JSI=true;teachingOfYear =true;expectedSalary=true;fitScore=true;
		}else
		{
			try{
				DistrictMaster districtMasterObj=districtMaster;
				if(districtMasterObj!=null && districtMasterObj.getDisplayAchievementScore()!=null && districtMasterObj.getDisplayAchievementScore()){
					achievementScore=true;
				}
				else if(hqMaster!=null && hqMaster.getDisplayAchievementScore()!=null && hqMaster.getDisplayAchievementScore()){
					achievementScore=true;
				}
				if(districtMasterObj!=null && districtMasterObj.getDisplayTFA()!=null && districtMasterObj.getDisplayTFA()){
					tFA=true;
				}
				else if(hqMaster!=null && hqMaster.getDisplayTFA()!=null && hqMaster.getDisplayTFA()){
					tFA=true;
				}
				if(districtMasterObj!=null && districtMasterObj.getDisplayDemoClass()!=null && districtMasterObj.getDisplayDemoClass()){
					demoClass=true;
				}
				else if(hqMaster!=null && hqMaster.getDisplayDemoClass()!=null && hqMaster.getDisplayDemoClass()){
					demoClass=true;
				}
				if(districtMasterObj!=null && districtMasterObj.getDisplayJSI()!=null && districtMasterObj.getDisplayJSI()){
					JSI=true;
				}
				else if(hqMaster!=null && hqMaster.getDisplayJSI()!=null && hqMaster.getDisplayJSI()){
					JSI=true;
				}
				if(districtMasterObj!=null && districtMasterObj.getDisplayYearsTeaching()!=null && districtMasterObj.getDisplayYearsTeaching()){
					teachingOfYear=true;
				}
				else if(hqMaster!=null && hqMaster.getDisplayYearsTeaching()!=null && hqMaster.getDisplayYearsTeaching()){
					teachingOfYear=true;
				}
				if(districtMasterObj!=null && districtMasterObj.getDisplayExpectedSalary()!=null && districtMasterObj.getDisplayExpectedSalary()){
					expectedSalary=true;
				}
				else if(hqMaster!=null && hqMaster.getDisplayExpectedSalary()!=null && hqMaster.getDisplayExpectedSalary()){
					expectedSalary=true;
				}
				if(districtMasterObj!=null && districtMasterObj.getDisplayFitScore()!=null && districtMasterObj.getDisplayFitScore()){
					fitScore=true;
				}
				else if(hqMaster!=null && hqMaster.getDisplayFitScore()!=null && hqMaster.getDisplayFitScore()){
					fitScore=true;
				}
				if(districtMasterObj!=null && districtMasterObj.getDistrictId()!=null && districtMasterObj.getDistrictId().equals(7800038)){
					pnqDis=true;
				}
			}catch(Exception e){ e.printStackTrace();}
		}
		//TestTool.getTraceTimeTPAP("TPAP 202");
		/*String roleAccess=null;
		try{
			roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,43,"teacherinfo.do",1);
		}catch(Exception e){
			e.printStackTrace();
		}*/
		
		StringBuilder sb = new StringBuilder();
		
		
		try
		{
			//int commDivFlag=0;
			
			String realPath = Utility.getValueOfPropByKey("teacherRootPath");
			String webPath = Utility.getBaseURL(request);
			String sWidthFoeDiv="765";
			//System.out.println("<--realPath-->"+realPath+"<--webPath-->"+webPath);
			String sDivCloseJsFunctionName="showProfileContentClose";
			if(sVisitLocation.equalsIgnoreCase("My Folders"))
				sDivCloseJsFunctionName="showProfileContentClose_ShareFolder";
			
			
			String imgPath="";
			TeacherDetail teacherDetail = null;
			try{
				teacherDetail = teacherDetailDAO.findById(teacherId,false,false);
				if(teacherDetail.getIdentityVerificationPicture()!=null){
					imgPath=CommonDashboardAjax.showPictureWeb(teacherDetail, realPath, webPath);
					//System.out.println("<--imgPath-->"+imgPath);
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			
			/*@Start
			@Ashish
			@Description :: For Teacher Profile (draggable Div) and Content*/
		/*	if(!teacherDetail.getFirstName().equals("")){
			sb.append("<div style='width:"+sWidthFoeDiv+"px;color:black;'>"+ //**@AShish :: Change cursor default to move ****** 
			"<div class='modal_header_profile' style='margin-left:-2px; margin-top:-4px; cursor: move;' id='teacherDiv'>"+
			"<button type='button' class='close' data-dismiss='modal' onclick='"+sDivCloseJsFunctionName+"();'>x</button>"+
			"<h3>"+teacherDetail.getFirstName()+" "+teacherDetail.getLastName()+"'s Profile</h3>"+
			"</div>"+
			"<div data-spy='scroll' data-offset='50' class='scrollspy_profile' style='width:"+sWidthFoeDiv+"px; height:500px;'>");
			}
			*/
			
                        String width="798px";
				if(userMaster.getEntityType().equals(2) || userMaster.getRoleId().getRoleId().equals(3))
				  width ="900px";
			
			if(!teacherDetail.getFirstName().equals("")){
				//System.out.println("<--teacherDetail.getFirstName()-->"+teacherDetail.getFirstName());
				sb.append("<div class='modal-dialog-for-cgprofile' style='width: "+width+"'>"+
						
				"<div class='modal-content modal-border'>"+		

				"<div class='modal-header modal_header_profile' id='teacherDiv'> "+

				"<button type='button' class='close' data-dismiss='modal' onclick='"+sDivCloseJsFunctionName+"();'>x</button>"+

				"<h3>"+teacherDetail.getFirstName()+" "+teacherDetail.getLastName()+"'s Profile</h3>"+

				"</div>"+

				"<div class='modal-body scrollspy_profile' style='min-width:"+sWidthFoeDiv+"px;'>");

				}
			/*@End
			@Ashish
			@Description :: For Teacher Profile (draggable Div)*/

			
			
			
			String showImg="";
			/*if(!imgPath.equals("")){
				showImg="<img src=\""+imgPath+"\" align='left' class='circle' style='width: 48px;height: 48px;margin: 0em auto;'>";
			}else{
				showImg="<img src=\"images/nophoto.png\" align='left' class='circle' style='width: 48px;height: 48px;margin: 0em auto;' >";
			}*/
			
			showImg="<img src=\"images/nophoto.png\" align='left' class='circle' style='width: 48px;height: 48px;margin: 0em auto;' >";
			//List<JobForTeacher>  jobForTeachers = jobForTeacherDAO.findJobByTeacher(teacherDetail);
			/*Criterion criterion_teacherId = Restrictions.eq("teacherId", teacherDetail);
			List<JobForTeacher>  jobForTeachers=new ArrayList<JobForTeacher>();
			int entityType=userMaster.getEntityType();
			if(entityType==2)
			{
				System.out.println("DA");
				Criterion criterion2 = Restrictions.eq("districtMaster", userMaster.getDistrictId());
				List<JobOrder> jobOrders=jobOrderDAO.findByCriteria(criterion2);
				if(jobOrders.size() > 0)
				{
					Criterion criterion3 = Restrictions.in("jobId", jobOrders);
					jobForTeachers = jobForTeacherDAO.findByCriteria(criterion_teacherId,criterion3);
				}
			}
			else if(entityType==3)
			{
				System.out.println("SA");
				System.out.println("logged In SA "+userMaster.getSchoolId().getSchoolId());
				List<JobOrder> jobOrders=new ArrayList<JobOrder>();
				List<SchoolInJobOrder> inJobOrders=new ArrayList<SchoolInJobOrder>();
				if(userMaster.getSchoolId()!=null)
					inJobOrders=schoolInJobOrderDAO.findJobBySchool(userMaster.getSchoolId());
				if(inJobOrders.size() > 0)
				{
					for(SchoolInJobOrder inJobOrder:inJobOrders)
					{
						jobOrders.add(inJobOrder.getJobId());
						System.out.println("SA School JobId "+inJobOrder.getJobId().getJobId());
					}
				}
				
				if(jobOrders.size() > 0)
				{
					Criterion criterion3 = Restrictions.in("jobId", jobOrders);
					jobForTeachers = jobForTeacherDAO.findByCriteria(criterion_teacherId,criterion3);
				}
			}
			else
			{
				System.out.println("TM");
				jobForTeachers = jobForTeacherDAO.findJobByTeacher(teacherDetail);
			}*/
			//TestTool.getTraceTimeTPAP("TPAP 203");
			List<JobForTeacher>  jobForTeachers=new ArrayList<JobForTeacher>();
			jobForTeachers=getJFT(teacherDetail, userMaster,"Others");
			
			JobForTeacher jobForTeacher = null;
			String firstAppliedOn="";
			String lastAppliedOn="";
			Date lastActivityDate=null;
			String lastContactedOn="None";
			if(jobForTeachers.size()>0)
			{
				jobForTeacher = jobForTeachers.get(0);
				firstAppliedOn = Utility.convertDateAndTimeToUSformatOnlyDate(jobForTeacher.getCreatedDateTime());
				jobForTeacher = jobForTeachers.get(jobForTeachers.size()-1);
				lastAppliedOn = Utility.convertDateAndTimeToUSformatOnlyDate(jobForTeacher.getCreatedDateTime());
				//System.out.println("<--firstAppliedOn-->"+firstAppliedOn+"<--lastAppliedOn-->"+lastAppliedOn);
				//Collections.reverse(jobForTeachers);
				//Collections.sort(jobForTeachers,JobForTeacher.jobForTeacherComparatorForLastActivityDateDesc);
				List<JobForTeacher>  jobForTeachersLastContactedOn=new ArrayList<JobForTeacher>();
				jobForTeachersLastContactedOn=getJFTLastContactedOn(teacherDetail, userMaster,null, "Others");
				for (JobForTeacher jobForTeacher2 : jobForTeachersLastContactedOn) {
					lastActivityDate = jobForTeacher2.getLastActivityDate();
					if(lastActivityDate!=null && jobForTeacher2.getUserMaster()!=null)
					{
						lastContactedOn ="<a href='javascript:void(0);'  onclick=\"getCommunicationsDiv(1,0,'"+teacherDetail.getTeacherId()+"',0);\" ><span class='divlablelinkval'>"+Utility.convertDateAndTimeToUSformatOnlyDate(lastActivityDate)+" ("+jobForTeacher2.getUserMaster().getFirstName()+" "+jobForTeacher2.getUserMaster().getLastName()+")</span></a>"; 
						break;
					}
					
					/*if(lastActivityDate!=null && userMaster.getEntityType()==2 && jobForTeacher2.getJobId().getCreatedForEntity()==2 && userMaster.getDistrictId().getDistrictId().equals(jobForTeacher2.getJobId().getDistrictMaster().getDistrictId()))
					{
						lastContactedOn ="<a href='javascript:void(0);'  onclick=\"getCommunicationsDiv(1,0,'"+teacherDetail.getTeacherId()+"',0);\" ><span class='divlablelinkval'>"+Utility.convertDateAndTimeToUSformatOnlyDate(lastActivityDate)+" ("+jobForTeacher2.getUserMaster().getFirstName()+" "+jobForTeacher2.getUserMaster().getLastName()+")</span></a>"; 
						break;
					}
					else if(lastActivityDate!=null && ( userMaster.getEntityType()==1 || userMaster.getEntityType()==3))
					{
						lastContactedOn ="<a href='javascript:void(0);'  onclick=\"getCommunicationsDiv(1,0,'"+teacherDetail.getTeacherId()+"',0);\" ><span class='divlablelinkval'>"+Utility.convertDateAndTimeToUSformatOnlyDate(lastActivityDate)+" ("+jobForTeacher2.getUserMaster().getFirstName()+" "+jobForTeacher2.getUserMaster().getLastName()+")</span></a>"; 
						break;
					}*/
				}
				//System.out.println("<--lastActivityDate-->"+lastActivityDate+"<--lastContactedOn-->"+lastContactedOn);
			} 
			//TestTool.getTraceTimeTPAP("TPAP 204");
			List<TeacherDetail> lstTeacherDetails	  =	 new ArrayList<TeacherDetail>();
			lstTeacherDetails.add(teacherDetail);
			
			CandidateGridService cgService=new CandidateGridService();
			
			DistrictPortfolioConfig districtPortfolioConfig=null;
			/*if(districtMaster!=null)
			{
				List<DistrictPortfolioConfig> lstdistrictPortfolioConfig = districtPortfolioConfigDAO.getPortfolioConfigs(districtMaster);
				if(lstdistrictPortfolioConfig!=null)
					districtPortfolioConfig = lstdistrictPortfolioConfig.get(0);
			}*/
			
			// Ramesh :: Start Insert teacherprofilevisithistory
			//System.out.println("Start teacherprofilevisithistory<<<<<<<<<<<<<<<<<<<<<<<<<<");
			TeacherProfileVisitHistory teacherProfileVisitHistory=new TeacherProfileVisitHistory();
			teacherProfileVisitHistory.setUserMaster(userMaster);
			teacherProfileVisitHistory.setTeacherDetail(teacherDetail);
			teacherProfileVisitHistory.setJobOrder(null);
			teacherProfileVisitHistory.setVisitLocation(sVisitLocation);
			teacherProfileVisitHistory.setVisitedDateTime(new Date());
			teacherProfileVisitHistory.setIpAddress(IPAddressUtility.getIpAddress(request));
			
			if(userMaster.getDistrictId()!=null)
				teacherProfileVisitHistory.setDistrictId(userMaster.getDistrictId());
			if(userMaster.getSchoolId()!=null)
				teacherProfileVisitHistory.setSchoolId(userMaster.getSchoolId());
			
			teacherProfileVisitHistoryDAO.makePersistent(teacherProfileVisitHistory);
			// End Insert teacherprofilevisithistory
			
			String windowFunc="if(this.href!='javascript:void(0);')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
			
			//List for Willing to Substitute
			Criterion criterion = Restrictions.eq("teacherDetail",teacherDetail);
			//List<TeacherCertificate> lstTeacherCertificate_new = teacherCertificateDAO.findByCriteria(criterion);
			//List for TFA
			List<TeacherExperience> lstTeacherExperience_new=teacherExperienceDAO.findByCriteria(criterion);
			//List for Expected Salary
			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail.getTeacherId());
			List<TeacherPersonalInfo> lstTeacherPersonalInfo_new=teacherPersonalInfoDAO.findByCriteria(criterion1);
			
			//System.out.println("New setting Start");
			boolean headQuarterMaster =false;
			if(userMaster!=null && userMaster.getHeadQuarterMaster()!=null){
				headQuarterMaster=true;
			}
			// New setting Start
			sb.append("<div id='errorDiv' style='color:red;display:none;padding-left:60px;'></div>");
			sb.append("<table id='profile_id' width="+sWidthFoeDiv+" cellspacing=0 cellpadding=0 class='tablecgtbl'>");
			
				sb.append("<tr >");
					sb.append("<th style='padding-top:10px;text-align:left;'>");
					
						sb.append("<table border='0' cellspacing=0 cellpadding=0 class='tablecgtbl'>");
							sb.append("<tr>");
								String strCss_lbl="padding:2px;text-align:left;";
								String strCss_value="width=42px; vertical-algin:top;text-align:left;padding:0px;padding-left:5px;padding-right:5px;padding-top:5px;";
								sb.append("<th width=50 valign='top'>"); // Photo
									sb.append("<table border='0' cellspacing=0 cellpadding=0 class='tablecgtbl'>");
										sb.append("<tr><th style='"+strCss_value+"'>"+showImg+"</th></tr>");
									sb.append("</table>");
								sb.append("</th>");
								
								sb.append("<th style='text-align:left;'>");
									sb.append("<table border='0' cellspacing=0 cellpadding=0 class='tablecgtbl' style='border-spacing:0;'>");
									
									if(sVisitLocation.equalsIgnoreCase("My Folders"))
									{
										strCss_lbl="padding:0px;text-align:left;";
										strCss_value="padding:0px;text-align:left;";
									}else{
										strCss_lbl="padding:2px;text-align:left;";//OK
										strCss_value="padding:2px;text-align:left;";//OK
									}
									
										//Name:
										sb.append("<tr><th class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblName1+":</th><th class='divlableval' style='"+strCss_value+"'>&nbsp;"+teacherDetail.getFirstName()+" "+teacherDetail.getLastName()+"</th></tr>");
										
										/* @Start
										 * @Ashish Kumar
										 * @Description :: Add Address of Teacher
										 * */
										//TestTool.getTraceTimeTPAP("TPAP 205");	
										TeacherPersonalInfo teacherPersonalInfo = teacherPersonalInfoDAO.findById(lstTeacherDetails.get(0).getTeacherId(), false, false);
										
										if(userMaster.getDistrictId()!=null && userMaster.getDistrictId().getDistrictId()!=null && userMaster.getDistrictId().getDistrictId()==1201470)	//OSCEOLA
											if(teacherPersonalInfo!=null)
												if(teacherPersonalInfo.getAnotherName()!=null)
													sb.append("<tr><th class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblAname+":</th><th class='divlableval' style='"+strCss_value+"'>&nbsp;"+teacherPersonalInfo.getAnotherName()+"</th></tr>");
												else
													sb.append("<tr><th class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblAname+":</th><th class='divlableval' style='"+strCss_value+"'>&nbsp;</th></tr>");
										if(userMaster.getDistrictId()!=null && userMaster.getDistrictId().getDistrictId()!=null && userMaster.getDistrictId().getDistrictId()==7800294)	//WAUKESHA 
											if(teacherPersonalInfo!=null)
												if(teacherPersonalInfo.getDistrictEmail()!=null)
													sb.append("<tr><td class='divlable' style='"+strCss_lbl+"'>&nbsp;District Email Address:</td><td class='divlableval' style='"+strCss_value+"'>&nbsp;"+teacherPersonalInfo.getDistrictEmail()+"</td></tr>");
												else
													sb.append("<tr><td class='divlable' style='"+strCss_lbl+"'>&nbsp;District Email Address:</td><td class='divlableval' style='"+strCss_value+"'>&nbsp;</td></tr>");
										
										String sEmployeeNumber="N/A";
										String sEmployeePosition="N/A";
										if(teacherPersonalInfo!=null && teacherPersonalInfo.getEmployeeNumber()!=null && !teacherPersonalInfo.getEmployeeNumber().equalsIgnoreCase(""))
											sEmployeeNumber=teacherPersonalInfo.getEmployeeNumber();
										if(teacherPersonalInfo!=null && teacherPersonalInfo.getLastPositionWhenEmployed()!=null && !teacherPersonalInfo.getLastPositionWhenEmployed().equalsIgnoreCase(""))
											sEmployeePosition=teacherPersonalInfo.getLastPositionWhenEmployed();
										PrintOnConsole.debugPrintln("Teacher Profile2", "sEmployeeNumber "+sEmployeeNumber);

										try{
											if(teacherPersonalInfo!=null)
											{
												String stateName = null;
												String country = null;
												String cityName = null;
												String zip = null;
												boolean addCHK = false;
												
												if(lstTeacherPersonalInfo_new.get(0).getCountryId()!=null){
													country=lstTeacherPersonalInfo_new.get(0).getCountryId().getName();
												}
												if(lstTeacherPersonalInfo_new.get(0).getStateId()!=null){
													stateName = lstTeacherPersonalInfo_new.get(0).getStateId().getStateName()+", ";
													//System.out.println("<--stateName-->"+stateName);
													addCHK = true;
												}else{
													stateName = "";
												}
												
												if(lstTeacherPersonalInfo_new.get(0).getCityId()!=null)
												{
													cityName = lstTeacherPersonalInfo_new.get(0).getCityId().getCityName()+", ";
													//System.out.println("<--cityName-->"+cityName);
													addCHK = true;
												}else{
													cityName = "";
												}
												
												if(lstTeacherPersonalInfo_new.get(0).getZipCode()!=null && !lstTeacherPersonalInfo_new.get(0).getZipCode().equals(""))
												{
													zip = lstTeacherPersonalInfo_new.get(0).getZipCode();
													//System.out.println("<--zip-->"+zip);
													addCHK = true;
												}else{
													zip = "";
												}
												
												if(lstTeacherPersonalInfo_new.get(0).getAddressLine1()!=null && !lstTeacherPersonalInfo_new.get(0).getAddressLine1().equals(""))
												{
													if(stateName=="" && cityName =="" && zip=="" && lstTeacherPersonalInfo_new.get(0).getAddressLine2().equals(""))
													{
														sb.append("<tr><th class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblAdd+":</th><th class='divlableval' style='"+strCss_value+"'>&nbsp;"+lstTeacherPersonalInfo_new.get(0).getAddressLine1()+"</th></tr>");
														//System.out.println("<--AddressLine1-->"+lstTeacherPersonalInfo_new.get(0).getAddressLine1());
													}
													else
													{
														sb.append("<tr><th class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblAdd+":</th><th class='divlableval' style='"+strCss_value+"'>&nbsp;"+lstTeacherPersonalInfo_new.get(0).getAddressLine1()+",</th></tr>");
													}
													
													addCHK = true;
												}
												
												if(lstTeacherPersonalInfo_new.get(0).getAddressLine2()!=null && !lstTeacherPersonalInfo_new.get(0).getAddressLine2().equals(""))
												{
													if(stateName=="" && cityName =="" && zip=="")
													{
														sb.append("<tr><th class='divlable' style='"+strCss_lbl+"'>&nbsp;</th><th class='divlableval' style='"+strCss_value+"'>&nbsp;"+lstTeacherPersonalInfo_new.get(0).getAddressLine2()+"</th></tr>");
														//System.out.println("<--AddressLine2-->"+lstTeacherPersonalInfo_new.get(0).getAddressLine2());
													}
													else
													{
														sb.append("<tr><th class='divlable' style='"+strCss_lbl+"'>&nbsp;</th><th class='divlableval' style='"+strCss_value+"'>&nbsp;"+lstTeacherPersonalInfo_new.get(0).getAddressLine2()+",</th></tr>");
													}
													addCHK = true;
												}
												
												if(addCHK==false)
												{
													sb.append("<tr><th class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblAdd+":</th><th class='divlableval' style='"+strCss_value+"'>&nbsp;N/A</th></tr>");
												}
												else
												{
													sb.append("<tr><th class='divlable' style='"+strCss_lbl+"'>&nbsp;</th><th class='divlableval' style='"+strCss_value+"'>&nbsp;"+stateName+""+cityName+""+zip+"</th></tr>");
													
													if(country!=""){
														sb.append("<tr><th class='divlable' style='"+strCss_lbl+"'>&nbsp;</th><th class='divlableval' style='"+strCss_value+"'>&nbsp;"+country+"</th></tr>");	
													}
												}
											}
										}catch(Exception e)
										{
											e.printStackTrace();
										}
											
										/* @End
										 * @Ashish Kumar
										 * @Description :: Add Address of Teacher
										 * */
											//System.out.println("EPI Norm");
										//EPI Norm
											//TestTool.getTraceTimeTPAP("TPAP 206");
										//Criterion criterionNS = Restrictions.eq("teacherId",teacherDetail);
										List<TeacherNormScore> teacherNormScoresList=teacherNormScoreDAO.findByCriteria(criterion);
										
										
										int normScore=0;
										String colorName="";
										if(teacherNormScoresList.size()>0)
										{
											normScore=teacherNormScoresList.get(0).getTeacherNormScore();
											//double percentile=teacherNormScoresList.get(0).getPercentile();
											colorName =teacherNormScoresList.get(0).getDecileColor();
											//colorName =cGReportService.getColorName(percentile);
											//System.out.println("<--normScore--> "+normScore+"<--colorName--> "+colorName);
										}
										if(noEPIFlag){
											sb.append("<tr><th class='divlable' style='"+strCss_lbl+";padding-top:10px;'>&nbsp;"+lblEPINorm+":</th><th class='divlableval' style='"+strCss_value+";padding-top:10px;padding-bottom:8px;'>&nbsp;");	
											if(normScore==0)
											{
												sb.append("N/A</th>");
												
											}
											else
											{
												String ccsName="";
												String normScoreLen=normScore+"";
												if(normScoreLen.length()==1){
													ccsName="nobground1";
												}else if(normScoreLen.length()==2){
													ccsName="nobground2";
												}else{
													ccsName="nobground3";
												}
												sb.append("<span class=\""+ccsName+"\" style='font-size: 11px;font-weight: bold;background-color: #"+colorName+";'>"+normScore+"</span>");
												//System.out.println(">>>>  "+sb+" <<<");
												sb.append("</th></tr>");
											}
										}
										//Veteran candidate
										
										boolean ssnCheck=true;
										if(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==NC_HEADQUARTER){
											ssnCheck=false;
										}
										
										if(ssnCheck)
										 try{
										   String  lblVeteran   = Utility.getLocaleValuePropByKey("lblVeteran", locale);
											   if(userMaster.getDistrictId()!=null && !userMaster.getDistrictId().getDistrictId().equals(804800) && !userMaster.getDistrictId().getDistrictId().equals(7800294))
											      if(teacherPersonalInfo!=null){
														if(teacherPersonalInfo.getIsVateran()!=null && teacherPersonalInfo.getIsVateran()==1){
														sb.append("<tr><td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblVeteran+":</td><td class='divlableval' style='"+strCss_value+"'>&nbsp;Yes</td></tr>");
														}
														
														if(userMaster.getRoleId().getRoleId()==2 ||userMaster.getRoleId().getRoleId()==7 || userMaster.getRoleId().getRoleId()==8){
															if(teacherPersonalInfo.getSSN()!=null && !teacherPersonalInfo.getSSN().equals("")){
																if(userMaster.getEntityType()!=3)
																sb.append("<tr><td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblSSN+":</td><td class='divlableval' style='"+strCss_value+"'>&nbsp;"+Utility.decodeBase64(teacherPersonalInfo.getSSN())+"</td></tr>");
															}else{
																if(userMaster.getEntityType()!=3)
																sb.append("<tr><td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblSSN+":</td><td class='divlableval' style='"+strCss_value+"'>&nbsp;N/A</td></tr>");
															}
														}
													}else{
														sb.append("<tr><td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblVeteran+":</td><td class='divlableval' style='"+strCss_value+"'>&nbsp;"+lblNo+"</td></tr>");
														if(userMaster.getRoleId().getRoleId()==2 ||userMaster.getRoleId().getRoleId()==7 || userMaster.getRoleId().getRoleId()==8){
															sb.append("<tr><td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblSSN+":</td><td class='divlableval' style='"+strCss_value+"'>&nbsp;N/A</td></tr>");
														}
													}
											}catch (Exception e) {
												e.printStackTrace();
											}
											//System.out.println("<--IsVateran--> "+teacherPersonalInfo.getIsVateran());
										//EmployeeNumber
										if(jeffcoFlag && !sEmployeeNumber.trim().equalsIgnoreCase("N/A"))
										        sb.append("<tr><td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblEmpNum+"A1"+":</td><td class='divlableval' style='"+strCss_value+"'>&nbsp;<a class='tooltipEmpNo' data-original-title='Employee Information' href='javascript:void(0);' onclick='openEmployeeNumberInfo(\""+sEmployeeNumber+"\")'>"+sEmployeeNumber+"</a></td></tr>");
										else
											 if(!sEmployeeNumber.trim().equalsIgnoreCase("N/A")&&locale.equals("fr"))
												 sb.append("<tr><td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblEmpNum+"A1"+":</td><td class='divlableval' style='"+strCss_value+"'>&nbsp;<a class='tooltipEmpNo' data-original-title='Employee Information' href='javascript:void(0);' onclick='openEmployeeNumberInfoOther(\""+sEmployeeNumber+"\")'>"+sEmployeeNumber+"</a></td></tr>");	
											 else
												 sb.append("<tr><td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblEmpNum+":</td><td class='divlableval' style='"+strCss_value+"'>&nbsp;"+sEmployeeNumber+"</td></tr>");
										//Employee Position
										if(districtMaster!=null && districtMaster.getDistrictId().equals(7800294))
											sb.append("<tr><td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblEmpPosition+":</td><td class='divlableval' style='"+strCss_value+"'>&nbsp;"+sEmployeePosition+"</td></tr>");	
										
										//First Job Applied On
										sb.append("<tr><th class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblFirstJobApplied+":</th><th class='divlableval' style='"+strCss_value+"'>&nbsp;"+firstAppliedOn+"</th></tr>");
										//Last Job Applied On
										sb.append("<tr><th class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblLastJobApplied+":</th><th class='divlableval' style='"+strCss_value+"'>&nbsp;"+lastAppliedOn+"</th></tr>");
										//Last Contacted On
										sb.append("<tr><th class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblLastContacted+":</th><th class='divlableval' style='"+strCss_value+"'>&nbsp;"+lastContactedOn+"</th></tr>");
										
										//Job Applied calculation
										//TestTool.getTraceTimeTPAP("TPAP 207");
										SortedMap<Long,JobForTeacher> jftMap = new TreeMap<Long,JobForTeacher>();
										SortedMap<Long,JobForTeacher> jftMapForTm = new TreeMap<Long,JobForTeacher>();
										List<JobForTeacher> jobForTeachersList	  =	 new ArrayList<JobForTeacher>();
										List<JobOrder> lstJobOrderSLList =new ArrayList<JobOrder>();
										if(entityID==3){
											lstJobOrderSLList =schoolInJobOrderDAO.jobOrderPostedBySchool(schoolMaster);
										}
										List<Integer> statusIdList = new ArrayList<Integer>();
										String[] statusShrotName = {"comp","icomp","vlt","hird","scomp","ecomp","vcomp","dcln","rem","widrw"};
										try{
											statusIdList = statusMasterDAO.findStatusIdsByStatusByShortNames(statusShrotName);
										}catch (Exception e) {
											e.printStackTrace();
										}
										jobForTeachersList=jobForTeacherDAO.getJobForTeacherByDSTeachersList(statusIdList,lstTeacherDetails,lstJobOrderSLList,null,null,districtMaster,entityID);
										for(JobForTeacher jobForTeacherObj : jobForTeachersList){
											jftMap.put(jobForTeacherObj.getJobForTeacherId(),jobForTeacherObj);
										}
										jobForTeachersList=jobForTeacherDAO.getJobForTeacherByDSTeachersList(statusIdList,lstTeacherDetails,lstJobOrderSLList,null,null,districtMaster,1);
										for(JobForTeacher jobForTeacherObj : jobForTeachersList){
											jftMapForTm.put(jobForTeacherObj.getJobForTeacherId(),jobForTeacherObj);
										}
										
										int jobList=jobListByTeacher(teacherDetail.getTeacherId(),jftMap);
										int jobListForTm=jobListByTeacher(teacherDetail.getTeacherId(),jftMapForTm);
										
										//sb.append("<tr><th class='divlable' style='"+strCss_lbl+"'>&nbsp;Job Applied:</th><th class='divlableval' style='"+strCss_value+"'>&nbsp;<a href='javascript:void(0);' onclick=\"getJobList('"+teacherDetail.getTeacherId()+"')\" >"+jobList+"</a>"+"/"+(jobListForTm-jobList)+"<th></tr>");
										
										
										//National Board Certification
										//TestTool.getTraceTimeTPAP("TPAP 208");
										
                                        TeacherExperience teacherExperience = null;
										
										if(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==NC_HEADQUARTER )
										{
											System.out.println("inside headquatermaster");
											
											List<TeacherPersonalInfo> ssnTeachersList = new ArrayList<TeacherPersonalInfo>();
											ArrayList<Integer> teacherIds = new ArrayList<Integer>();
											List<String> ssnList = new ArrayList<String>();
											List<LicensureNBPTS> licensureNBPTSList = new ArrayList<LicensureNBPTS>();
											
											teacherIds.add(teacherDetail.getTeacherId());
											if(teacherIds!=null && teacherIds.size()>0)
											{
												ssnTeachersList = teacherPersonalInfoDAO.findByTeacherIds(teacherIds);
											
											}
											if(ssnTeachersList!=null && ssnTeachersList.size()>0)
											{
												System.out.println("inside ssnTeachersList");
											  for(TeacherPersonalInfo tpi:ssnTeachersList){
												if(tpi.getSSN()!=null && !tpi.getSSN().equals("")){
													System.out.println("ssssnnnn"+tpi.getSSN());
													ssnList.add(tpi.getSSN());
													System.out.println("ssnList:::::::"+ssnList);
													
												}
												//System.out.println("ssnList sizeeeeeeeee::"+ssnList.size()+"ssnnnnnnnnnnnnnnnnnnn::::::"+ssnList);
											}
											}
																					
											if(ssnList!=null && ssnList.size()>0){
												
												licensureNBPTSList = licensureNBPTSDao.getDataBySsnList(ssnList);
											}
											if(licensureNBPTSList!=null && licensureNBPTSList.size()>0)
											{
												String fullDate ="";
												String finalYear="";
												Format formatter = new SimpleDateFormat("yyyy-MM-dd");
												
												if(licensureNBPTSList.get(0).getExpirationDate()!=null)
												 fullDate = formatter.format(licensureNBPTSList.get(0).getExpirationDate());
												if(!fullDate.equals(""))
												 finalYear=fullDate.substring(0, 4);
												
												System.out.println("NBTS  cetri::::::::::YES");
												sb.append("<th class='divlable' style='"+strCss_lbl+"'>&nbsp;"+Utility.getLocaleValuePropByKey("lblNatiBoardCertiLice", locale)+":</th>");
												sb.append("<th  class='divlableval' style='"+strCss_value+"'>&nbsp;Y ("+finalYear+")</th>");
												
											}
											else
											{
												System.out.println("NBTS  cetri::::::::::NOOOOOOOOOO");
												sb.append("<th class='divlable' style='"+strCss_lbl+"'>&nbsp;"+Utility.getLocaleValuePropByKey("lblNatiBoardCertiLice", locale)+":</th>");
												sb.append("<th  class='divlableval' style='"+strCss_value+"'>&nbsp;N</th>");
											}
											
											
										}else{
											teacherExperience = teacherExperienceDAO.findTeacherExperienceByTeacher(teacherDetail);
											sb.append("<tr>");
											
											
											sb.append("<th class='divlable' style='"+strCss_lbl+"'>&nbsp;"+Utility.getLocaleValuePropByKey("lblNatiBoardCertiLice", locale)+"</th>");
											
											if(teacherExperience!=null)
											{
												if(teacherExperience.getNationalBoardCert()==null)
												{
													sb.append("<th  class='divlableval' style='"+strCss_value+"'>&nbsp;N/A</th>");
												}
												else
												{
													if(teacherExperience.getNationalBoardCert()==true)
													{
														String sNationalBoardCertYear="";
														if(teacherExperience.getNationalBoardCertYear()!=null && !teacherExperience.getNationalBoardCertYear().equals(""))
															sNationalBoardCertYear=teacherExperience.getNationalBoardCertYear().toString();
														sb.append("<th  class='divlableval' style='"+strCss_value+"'>&nbsp;Y ("+sNationalBoardCertYear+")</th>");
													}
													else if(teacherExperience.getNationalBoardCert()==false)
														sb.append("<th  class='divlableval' style='"+strCss_value+"'>&nbsp;N</th>");
												}
											}
											else
											{
												sb.append("<th  class='divlableval' style='"+strCss_value+"'>&nbsp;N/A</th>");
											}
												
											sb.append("</tr>");
										}
										
									
										
										
										
										//sb.append("<tr><th class='divlable' style='"+strCss_lbl+"'>&nbsp;Job Applied:</th><th class='divlableval' style='"+strCss_value+"'>&nbsp;<a href='javascript:void(0);' onclick=\"getJobList('"+teacherDetail.getTeacherId()+"')\" >"+jobList+"</a>"+"/"+(jobListForTm-jobList)+"<th></tr>");
										//System.out.println("<--NationalBoardCert--> "+teacherExperience.getNationalBoardCert());
										
									sb.append("</table>");
								sb.append("</th>");
								
								sb.append("<th>");
									sb.append("<table border='0' cellspacing=0 cellpadding=0 class='tablecgtbl' style='border-spacing:0;'>");
									
									if(sVisitLocation.equalsIgnoreCase("My Folders"))
									{
										strCss_lbl="padding-left:20px;text-align:left;white-space:nowrap";
										strCss_value="padding-left:10px;text-align:left;white-space:nowrap";
									}
									else
									{
										strCss_lbl="padding:2px;padding-left:20px;text-align:left;white-space:nowrap";
										strCss_value="padding:2px;padding-left:10px;text-align:left;white-space:nowrap";
									}
									
										
										//TFA
										if(tFA)
										{
											sb.append("<tr>");
											sb.append("<th class='divlable' style='"+strCss_lbl+"'>&nbsp;TFA:</th>");
											//String tFAVal =cgService.getTFA(lstTeacherExperience_new,teacherDetail);
											
											String tFAVal = getTFAValue(lstTeacherExperience_new,teacherDetail);
											
											//System.out.println(tfaEditAuthUser()+ " tfaEditAuthUser() tFAValtFAValtFAValtFAValtFAValtFAValtFAValtFAValtFAValtFAValtFAVal  "+tFAVal);
											
											String New_tFAVal = "";
											
											try
											{
												if(tFAVal!=null)
												{
													if(tfaEditAuthUser())
													{
														if(lstTeacherExperience_new.size()>0)
															New_tFAVal = tFAVal+"<span id='edittfa'>&nbsp;&nbsp;<a href='javascript:void(0);' onclick='editTFAbyUser("+lstTeacherExperience_new.get(0).getTeacherId().getTeacherId()+");'>Edit</a></span><span id='savetfa' style='display:none; cursor:pointer;'>&nbsp;&nbsp;<a javascript:void(0); onclick='displayTFAMessageBox("+lstTeacherExperience_new.get(0).getTeacherId().getTeacherId()+");'>Save</a>&nbsp;</span><span id='canceltfa' style='display:none; cursor:pointer;'><a javascript:void(0); onclick='cancelTFAbyUser("+lstTeacherExperience_new.get(0).getTeacherId().getTeacherId()+");'>Cancel</a></span>";	
														else
															New_tFAVal = tFAVal;
													}
													else
														New_tFAVal = tFAVal;
												}
												else
												{
													if(tfaEditAuthUser())
													{
														if(lstTeacherExperience_new.size()>0)
															New_tFAVal = "N/A "+"<span id='edittfa'>&nbsp;&nbsp;<a href='javascript:void(0);' onclick='editTFAbyUser("+lstTeacherExperience_new.get(0).getTeacherId().getTeacherId()+");'>Edit</a></span><span id='savetfa' style='display:none;cursor:pointer;'>&nbsp;&nbsp;<a javascript:void(0); onclick='displayTFAMessageBox("+lstTeacherExperience_new.get(0).getTeacherId().getTeacherId()+");'>Save</a>&nbsp;</span><span id='canceltfa' style='display:none; cursor:pointer;'><a javascript:void(0); onclick='cancelTFAbyUser("+lstTeacherExperience_new.get(0).getTeacherId().getTeacherId()+");'>Cancel</a></span>";
														else
															New_tFAVal = "N/A";
													}
													else
														New_tFAVal = "N/A";
												}
											}catch(Exception e)
											{
												e.printStackTrace();
											}
											
												sb.append("<th  class='divlableval' style='"+strCss_value+"'>&nbsp;"+New_tFAVal+"</th>");
											
											/*if(tFAVal!=null)
												sb.append("<th  class='divlableval' style='"+strCss_value+"'>&nbsp;"+tFAVal+"</th>");
											else
												sb.append("<th  class='divlableval' style='"+strCss_value+"'>&nbsp;N/A</th>");*/
											
											sb.append("</tr>");
											
											
											if(tFAVal!=null && lstTeacherExperience_new!=null && lstTeacherExperience_new.size()>0 && lstTeacherExperience_new.get(0)!=null)
											{
												sb.append("<tr>");
												sb.append("<th class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblCorpsYear+":</th>");
												if(lstTeacherExperience_new.get(0).getCorpsYear()!=null)
													sb.append("<th class='divlableval' style='"+strCss_value+"'>&nbsp;"+lstTeacherExperience_new.get(0).getCorpsYear()+"</th>");
												else
													sb.append("<th class='divlableval' style='"+strCss_value+"'>&nbsp;N/A</th>");
												sb.append("</tr>");
												
												sb.append("<tr>");
												sb.append("<th class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblTFARegion+":</th>");
												if(lstTeacherExperience_new.get(0).getTfaRegionMaster()!=null)
													sb.append("<th class='divlableval' style='"+strCss_value+"'>&nbsp;"+lstTeacherExperience_new.get(0).getTfaRegionMaster().getTfaRegionName()+"</th>");
												else
													sb.append("<th class='divlableval' style='"+strCss_value+"'>&nbsp;N/A</th>");
												sb.append("</tr>");
											}
											else
											{
												sb.append("<tr>");
												sb.append("<th class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblCorpsYear+":</th>");
												sb.append("<th class='divlableval' style='"+strCss_value+"'>&nbsp;N/A</th>");
												sb.append("</tr>");
												
												sb.append("<tr>");
												sb.append("<th class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblTFARegion+":</th>");
												sb.append("<th class='divlableval' style='"+strCss_value+"'>&nbsp;N/A</th>");
												sb.append("</tr>");
											}
										}
										//System.out.println("<--CorpsYear--> "+lstTeacherExperience_new.get(0).getCorpsYear());
										//TestTool.getTraceTimeTPAP("TPAP 209");
										//Teaching Years
										if(teachingOfYear){
											sb.append("<tr>");
											sb.append("<th class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblTeachingYears+":</th>");
											if(cgService.getTEYear(lstTeacherExperience_new,teacherDetail)!=0)
												sb.append("<th  class='divlableval' style='"+strCss_value+"'>&nbsp;"+cgService.getTEYear(lstTeacherExperience_new,teacherDetail)+"</th>");
											else
												sb.append("<th  class='divlableval' style='"+strCss_value+"'>&nbsp;N/A</th>");
											sb.append("</tr>");
										}
										//Expected Salary
										if(expectedSalary){
											sb.append("<tr>");
											sb.append("<th class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblExpectedSalary+":</th>");
											if(cgService.getSalary(lstTeacherPersonalInfo_new,teacherDetail)!=-1){
													sb.append("<th  class='divlableval' style='"+strCss_value+"'>&nbsp;$"+cgService.getSalary(lstTeacherPersonalInfo_new,teacherDetail)+"</th>");
											}else
												sb.append("<th  class='divlableval' style='"+strCss_value+"'>&nbsp;N/A</th>");
											sb.append("</tr>");
										}
										
										//# of Views:
										sb.append("<tr>");									
										Criterion criterion_teacherDetail = Restrictions.eq("teacherDetail",teacherDetail);
										List<TeacherProfileVisitHistory> listTeacherProfileVisitHistory=null;
										if(entityID==1)
											listTeacherProfileVisitHistory = teacherProfileVisitHistoryDAO.findByCriteria(criterion_teacherDetail);
										else 
										{
											Criterion criterion_districtId = Restrictions.eq("districtId",districtMaster);
											listTeacherProfileVisitHistory = teacherProfileVisitHistoryDAO.findByCriteria(criterion_teacherDetail,criterion_districtId);
										}
										/*else if(entityID==3){
											Criterion criterion_districtId = Restrictions.eq("districtId",districtMaster);
											Criterion criterion_schoolId = Restrictions.eq("schoolId",schoolMaster);
											listTeacherProfileVisitHistory = teacherProfileVisitHistoryDAO.findByCriteria(criterion_teacherDetail,criterion_schoolId,criterion_districtId);
										}*/
										
										int iTeacherProfileVisitHistoryCount=0;
										if(listTeacherProfileVisitHistory.size()>0)
											iTeacherProfileVisitHistoryCount=listTeacherProfileVisitHistory.size();
										
										sb.append("<th class='divlable1' style='"+strCss_lbl+"'>&nbsp;"+lblOfViews+":</th>");
										if(iTeacherProfileVisitHistoryCount>0)
											sb.append("<th  class='divlableval1' style='"+strCss_lbl+"'><a data-original-title='"+lblClickToViewTPVisit+"' rel='tooltip' id='tpTeacherProfileVisitHistory' href='javascript:void(0);' onclick=\"getTeacherProfileVisitHistoryShow_FTime('"+teacherDetail.getTeacherId()+"');\" ><span class='divlablelinkval1'>&nbsp;"+iTeacherProfileVisitHistoryCount+"</span></a></th>");
										
										sb.append("</tr>");
										//System.out.println("<--TeacherProfileVisitHistoryCount--> "+iTeacherProfileVisitHistoryCount);
										//Blank row and Cell
										//sb.append("<tr><th>&nbsp;</th><th>&nbsp;</th></tr>");
										//Job Applied View
										sb.append("<tr>");
										sb.append("<th class='divlable1' style='"+strCss_lbl+"'>&nbsp;"+lblJobApplied+":</th>");
										
										sb.append("<th class='divlableval1' style='"+strCss_lbl+"'>");
										if(jobList!=0){
											sb.append("<a data-original-title='"+lblClickToViewJO+"' rel='tooltip' id='teacherProfileTooltip' href='javascript:void(0);' onclick=\"getJobList('"+teacherDetail.getTeacherId()+"')\" ><span class='divlablelinkval1'>&nbsp;"+jobList+"</span></a>");
										}else{
											sb.append("&nbsp;0");
										}
										sb.append("/"+(jobListForTm-jobList)+"</th>");
										
										sb.append("</tr>");
										
										//Willing to Substitute
										sb.append("<tr>");
										sb.append("<th class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblWillingToSubsti+":</th>");
										if(lstTeacherExperience_new!=null && lstTeacherExperience_new.size()>0)
										{
											if(lstTeacherExperience_new.get(0).getCanServeAsSubTeacher()==2)
												sb.append("<th  class='divlableval' style='"+strCss_value+"'>&nbsp;N/A</th>");
											if(lstTeacherExperience_new.get(0).getCanServeAsSubTeacher()==0)
												sb.append("<th  class='divlableval' style='"+strCss_value+"'>&nbsp;N</th>");
											if(lstTeacherExperience_new.get(0).getCanServeAsSubTeacher()==1)
												sb.append("<th  class='divlableval' style='"+strCss_value+"'>&nbsp;Y</th>");
										}
										else
										{
											sb.append("<th  class='divlableval' style='"+strCss_value+"'>&nbsp;N/A</th>");
										}
										sb.append("</tr>");
										
										//KSN ID
										if( (userMaster.getEntityType()==6 && userMaster.getRoleId().getRoleId()==11) || (userMaster.getEntityType()==5 && userMaster.getRoleId().getRoleId()==10)){sb.append("<tr>");
										sb.append("<td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblKSNID+":</td>");
										MQEvent mqEvent = null;
										try{
											mqEvent = mqEventDAO.findLinkToKSNIniatedByTeacher(teacherDetail);
										}catch(Exception ex){
											ex.printStackTrace();
										}
										sb.append("<td  class='divlableval' style='"+strCss_value+"'>");
										sb.append("<div id='ksndetails'>");
										sb.append("<input type='hidden' id='disconnectTalentId' name='disconnectTalentId' value='"+teacherDetail.getTeacherId()+"' />");
										if(mqEvent!=null && mqEvent.getStatus()!=null && teacherDetail.getKSNID()==null){
											if(mqEvent.getStatus()!=null && mqEvent.getStatus().equalsIgnoreCase("R")){
												if(mqEvent.getAckStatus()!=null && mqEvent.getAckStatus().contains("Success")){
													sb.append("Link to KSN is in progress");
												}
												else if(mqEvent.getAckStatus()!=null && mqEvent.getAckStatus().contains("Fail")){
													sb.append("<table><tr>");
													sb.append("<td  class='divlableval' style='"+strCss_value+"'>");
												//	sb.append("<input type='hidden' id='disconnectTalentId' name='disconnectTalentId' value='"+teacherDetail.getTeacherId()+"' />");
													sb.append("<input type='text' id='inputKSN' class='form-control' value='' style='height:30px;' maxlength=10 onkeypress='return checkForInt(event);'/>");
													sb.append("</td>");
													sb.append("<td>");
													sb.append("<a data-original-title='Link to KSN' rel='tooltip' id='tpDisconnect' class='tpDisconnect' style='padding-left:8px;' href='javascript:void(0);' onclick='disconnectTalent();'><i class='icon-unlock icon-large'></i></a>");
													sb.append("</td>");
													sb.append("<script>$('#tpDisconnect').tooltip();</script>");
													sb.append("</tr><table>");
												}
												else{
													sb.append("Link to KSN is in progress");
												}
											}
										}
										else if(teacherDetail.getKSNID()==null){
											sb.append("<table><tr><td>");
											//sb.append("<input type='hidden' id='disconnectTalentId' name='disconnectTalentId' value='"+teacherDetail.getTeacherId()+"' />");
											sb.append("<input type='text' id='inputKSN' class='form-control' value='' style='height:30px;' maxlength=10 onkeypress='return checkForInt(event);'/>");
											sb.append("</td>");
											sb.append("<td>");
											sb.append("<a data-original-title='Link to KSN' rel='tooltip' id='tpDisconnect' class='tpDisconnect' style='padding-left:8px;' href='javascript:void(0);' onclick='disconnectTalent();'><i class='icon-unlock icon-large'></i></a>");
											sb.append("</td>");
											sb.append("<script>$('#tpDisconnect').tooltip();</script>");
											sb.append("</tr></table>");
										}
										else if(teacherDetail.getKSNID()!=null){
											sb.append("<table><tr>");
											sb.append("<td>"+teacherDetail.getKSNID()+"</td>");
											sb.append("<td>");
											sb.append("<a data-original-title='Linked to KSN' rel='tooltip' id='tpDisconnect' style='padding-left:8px;' href='javascript:void(0);'><i class='icon-lock icon-large'></i></a>");
											sb.append("</td>");
											sb.append("<script>$('#tpDisconnect').tooltip();</script>");
											sb.append("</tr></table>");
										}
										sb.append("</div>");
										sb.append("</td>");
										sb.append("</tr>");
										}
										
										//sssnnnnnnnnnn in applicant pool
										if(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==NC_HEADQUARTER){
										sb.append("<tr>");
										if(userMaster.getEntityType()!=3)
										sb.append("<th class='divlable' style='"+strCss_lbl+"'>&nbsp;Last 4 of SSN:</th>");
										
										if(teacherPersonalInfo!=null)
										if(teacherPersonalInfo.getSSN()!=null && !teacherPersonalInfo.getSSN().equals("")){
											String ssnFour=Utility.decodeBase64(teacherPersonalInfo.getSSN());
											String finalSSN="";
											try
											{
											 finalSSN=ssnFour.substring(5, 9);
											System.out.println("Last four digit of ssn:::"+finalSSN);
											}catch(Exception e){}
												sb.append("<th  class='divlableval' style='"+strCss_value+"'>&nbsp;"+finalSSN+"</th>");
											}else{
												sb.append("<th  class='divlableval' style='"+strCss_value+"'>&nbsp;N/A</th>");
											}
										
										sb.append("</tr>");
										}
										
										
									sb.append("</table>");
								sb.append("</th>");
								
							sb.append("</tr>");
						sb.append("</table>");
						
						
						
					sb.append("</th>");
				sb.append("</tr>");
				
				// start new row
				
				sb.append("<tr>");
					sb.append("<th>");
						sb.append("<table border='0' cellspacing=0 cellpadding=0>");
						sb.append("<tr>");
							// Blank Cell
							sb.append("<th width=50>&nbsp;</th>");
							
							strCss_lbl="padding:4px;vertical-align:middle;";
							strCss_value="padding:2px;text-align:left;vertical-align:text-top;";
							
							String Resume_column_width="";
							String Phone_column_width="";
							String PDReport_column_width="";
							if(sVisitLocation.equalsIgnoreCase("Teacher Pool")||sVisitLocation.equalsIgnoreCase("PNR Dashboard"))
							{
								sb.append("<th style='text-align:left;'>");//OK
								Resume_column_width=";width:44px;";
								Phone_column_width=";width:46px;";
								PDReport_column_width=";width:340px;";
							}
							else if(sVisitLocation.equalsIgnoreCase("Mosaic"))
							{
								sb.append("<th style='text-align:left;'>"); //OK
							}
							else if(sVisitLocation.equalsIgnoreCase("My Folders"))
							{
								sb.append("<th style='text-align:left;'>"); //)OK
							}
							
							
							sb.append("<table border='0' cellspacing=0 cellpadding=0");
							
							// Resume
							sb.append("<tr>");
							
							sb.append("<th class='divlable' style='"+strCss_lbl+""+Resume_column_width+"'>&nbsp;&nbsp;&nbsp;"+lnkResume+":</th>");
							if(teacherExperience!=null && teacherExperience.getResume()!=null && teacherExperience.getResume()!="")
								sb.append("<th  class='divlableval' style='"+strCss_value+"'>&nbsp;<a data-original-title='"+lnkResume+"' rel='tooltip' id='tpResumeprofile' href='javascript:void(0);' onclick=\"downloadResume('"+teacherDetail.getTeacherId()+"','tpResumeprofile');"+windowFunc+"\" ><span class='icon-briefcase icon-large iconcolor'></span></a></th>");
							else
								sb.append("<th  class='divlableval' style='"+strCss_value+"'>&nbsp;<a data-original-title='"+lblNotAvailable+"' rel='tooltip' id='tpResumeprofile'><span class='icon-briefcase icon-large iconcolorhover'></span></a></th>");
							
							if(sVisitLocation.equalsIgnoreCase("Teacher Pool")||sVisitLocation.equalsIgnoreCase("PNR Dashboard"))
								strCss_lbl="padding:2px;padding-left:118px;vertical-align:middle;text-align:right;";//OK // @AShish :: change padding-left from 200 to 110
							else if(sVisitLocation.equalsIgnoreCase("Mosaic"))
								strCss_lbl="padding:2px;padding-left:118px;vertical-align:middle;text-align:right;"; //OK // @Ashish :: change padding-left from 200 to 110
							else if(sVisitLocation.equalsIgnoreCase("My Folders"))
								strCss_lbl="padding:2px;padding-left:190px;vertical-align:middle;text-align:right;"; // OK
							
							
							//Phone:
							/*List<TeacherPersonalInfo> lstTeacherPersonalInfo1	  =	 new ArrayList<TeacherPersonalInfo>();
							lstTeacherPersonalInfo1=teacherPersonalInfoDAO.findTeacherPInfoForTeacher(lstTeacherDetails);
							sb.append("<th class='divlable' style='"+strCss_lbl+""+Phone_column_width+"'>&nbsp;Phone: </th>");
							if(lstTeacherPersonalInfo1.size()==1)
							{
								if((lstTeacherPersonalInfo1.get(0).getPhoneNumber()!=null && !lstTeacherPersonalInfo1.get(0).getPhoneNumber().equalsIgnoreCase("")) || (lstTeacherPersonalInfo1.get(0).getMobileNumber()!=null && !lstTeacherPersonalInfo1.get(0).getMobileNumber().equalsIgnoreCase("")))
									sb.append("<th  class='divlableval' style='"+strCss_value+";margin-top:-5px;padding-left:5px;'><a data-original-title='Phone' rel='tooltip' id='tpPhoneprofile' href='javascript:void(0);' onclick=\"getPhoneDetailShowPro('"+teacherDetail.getTeacherId()+"');\" ><span class='icon-mobile-phone icon-large iconcolor icon-large2';></span></a></th>");	
								else
									sb.append("<th  class='divlableval' style='"+strCss_value+";margin-top:-5px;padding-left:5px;'><a data-original-title='Not Available' rel='tooltip' id='tpPhoneprofile' ><span class='icon-mobile-phone icon-large iconcolorhover icon-large2'></span></a></th>");
							}
							else
								sb.append("<th  class='divlableval' style='"+strCss_value+";margin-top:-5px;padding-left:5px;'><a data-original-title='Not Available' rel='tooltip' id='tpPhoneprofile' ><span class='icon-mobile-phone icon-large iconcolorhover icon-large2'></span></a></th>");
							*/
							// PD Report
							//TestTool.getTraceTimeTPAP("TPAP 210");
							Map<Integer,TeacherAssessmentStatus> baseTakenMap = new HashMap<Integer, TeacherAssessmentStatus>();
							List<TeacherAssessmentStatus> teacherAssessmentStatus = null;
							Map<Integer, Integer> isbaseComplete	=	new HashMap<Integer, Integer>();
							
							Integer isBase = null;
							try{
								if(lstTeacherDetails!=null && lstTeacherDetails.size()>0){
									teacherAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentTakenByTeachers(lstTeacherDetails);
									for (TeacherAssessmentStatus teacherAssessmentStatus2 : teacherAssessmentStatus) {					
										baseTakenMap.put(teacherAssessmentStatus2.getTeacherDetail().getTeacherId(), teacherAssessmentStatus2);
										boolean cgflag = teacherAssessmentStatus2.getCgUpdated()==null?false:teacherAssessmentStatus2.getCgUpdated();
										if(cgflag && teacherAssessmentStatus2.getStatusMaster().getStatusShortName().equalsIgnoreCase("comp"))
											isbaseComplete.put(teacherAssessmentStatus2.getTeacherDetail().getTeacherId(), 1);
										else
											if(teacherAssessmentStatus2.getStatusMaster().getStatusShortName().equalsIgnoreCase("vlt"))
												isbaseComplete.put(teacherAssessmentStatus2.getTeacherDetail().getTeacherId(), 2);
									}
								}
							}catch(Exception e){
								e.printStackTrace();
							}
							
							isBase = isbaseComplete.get(teacherDetail.getTeacherId());
							sb.append("<th class='divlable' style='"+strCss_lbl+""+PDReport_column_width+"'>&nbsp;"+headPdRep+": </th>");
							if(isBase!=null && isBase==1)
								sb.append("<th  class='divlableval' style='"+strCss_value+"'>&nbsp;<a data-original-title='"+headPdRep+"' rel='tooltip' id='tpPDReportprofile' href='javascript:void(0);' onclick=\"generatePDReport('"+teacherDetail.getTeacherId()+"','tpPDReportprofile');"+windowFunc+"\"><span class='icon-book icon-large iconcolor'></span></a></th>");
							else
								sb.append("<th  class='divlableval' style='"+strCss_value+"'>&nbsp;<a data-original-title='"+lblNotAvailable+"' rel='tooltip' id='tpPDReportprofile' ><span class='icon-book icon-large iconcolorhover'></span></a></th>");
							
							sb.append("</tr>");
								
							sb.append("</table>");
							
						sb.append("</th>");
						sb.append("</tr>");
						sb.append("</table>");
					
					sb.append("</th>");
				sb.append("</tr>");
				// End new row
				
				/////////////////
				sb.append("<tr>");
				sb.append("<th>");
					sb.append("<table border='0' cellspacing=0 cellpadding=0 style='margin-top:-10px;'>");
					sb.append("<tr>");
						// Blank Cell
						sb.append("<th width=50>&nbsp;</th>");
						
						strCss_lbl="padding:10px;vertical-align:middle;padding-right:70px;";
						if(sVisitLocation.equalsIgnoreCase("Teacher Pool") ||sVisitLocation.equalsIgnoreCase("PNR Dashboard"))
						{
							sb.append("<th style='text-align:left;'>");//OK
							Phone_column_width=";width:189px;";
							PDReport_column_width=";width:100px;";
						}
						else if(sVisitLocation.equalsIgnoreCase("Mosaic"))
						{
							sb.append("<th style='text-align:left;'>"); //OK
						}
						else if(sVisitLocation.equalsIgnoreCase("My Folders"))
						{
							sb.append("<th style='text-align:left;'>"); //)OK
						}
						
						
						sb.append("<table border='0' cellspacing=0 cellpadding=0");
						
						// Resume
						sb.append("<tr>");
						
						//Phone:MyFolder
						//TestTool.getTraceTimeTPAP("TPAP 211");
						List<TeacherPersonalInfo> lstTeacherPersonalInfo1	  =	 new ArrayList<TeacherPersonalInfo>();
						lstTeacherPersonalInfo1=teacherPersonalInfoDAO.findTeacherPInfoForTeacher(lstTeacherDetails);
						if(sVisitLocation.equalsIgnoreCase("Teacher Pool") ||sVisitLocation.equalsIgnoreCase("PNR Dashboard"))
						{
							strCss_value="padding:10px;text-align:left;vertical-align:text-top;padding-right:73px;";
							sb.append("<th class='divlable' style='"+strCss_lbl+""+Phone_column_width+"'>&nbsp;"+lblPhoneNo+":</th>");
							if(lstTeacherPersonalInfo1.size()==1)
							{
								if((lstTeacherPersonalInfo1.get(0).getPhoneNumber()!=null && !lstTeacherPersonalInfo1.get(0).getPhoneNumber().equalsIgnoreCase("")))
									sb.append("<th  class='divlableval' style='"+strCss_value+";margin-top:-5px;padding-left:12px;'>"+lstTeacherPersonalInfo1.get(0).getPhoneNumber()+"</th>");	
								else
									sb.append("<th  class='divlableval' style='"+strCss_value+";margin-top:-5px;padding-left:5px;'><span class='span2'>&nbsp;N/A</th>");
							}
							else
								sb.append("<th  class='divlableval' style='"+strCss_value+";margin-top:-5px;padding-left:5px;'>&nbsp;N/A</th>");
							
							// PD Report
							
							isBase = isbaseComplete.get(teacherDetail.getTeacherId());
							sb.append("<th class='divlable' style='"+strCss_lbl+""+PDReport_column_width+"'>&nbsp;"+lblMobileNumber+": </th>");
							if((lstTeacherPersonalInfo1.size()==1 && lstTeacherPersonalInfo1.get(0).getMobileNumber()!=null && !lstTeacherPersonalInfo1.get(0).getMobileNumber().equalsIgnoreCase("")))
								sb.append("<th  class='divlableval' style='"+strCss_value+";margin-top:-5px;padding-left:30px;'>"+lstTeacherPersonalInfo1.get(0).getMobileNumber()+"</th>");
							else
								sb.append("<th  class='divlableval' style='"+strCss_value+";margin-top:-5px;padding-left:30px;'>&nbsp;N/A</th>");
							
							sb.append("</tr>");
						}else if(sVisitLocation.equalsIgnoreCase("My Folders"))
						{
							strCss_value="padding:10px;text-align:left;vertical-align:text-top;";
							sb.append("<th class='divlable' style='"+strCss_lbl+""+Phone_column_width+"'>&nbsp;"+lblPhoneNo+": </th>");
								if(lstTeacherPersonalInfo1.size()==1)
								{
									if((lstTeacherPersonalInfo1.get(0).getPhoneNumber()!=null && !lstTeacherPersonalInfo1.get(0).getPhoneNumber().equalsIgnoreCase("")))
										sb.append("<th  class='divlableval' style='"+strCss_value+";margin-top:-5px;padding-left:12px;'><span class='span2'>"+lstTeacherPersonalInfo1.get(0).getPhoneNumber()+"</span></th>");	
									else
										sb.append("<th  class='divlableval' style='"+strCss_value+";margin-top:-5px;padding-left:5px;'><span class='span2'>&nbsp;N/A</span></th>");
								}
								else
									sb.append("<th  class='divlableval' style='"+strCss_value+";margin-top:-5px;padding-left:5px;'><span class='span2'>&nbsp;N/A</span></th>");
								
								// PD Report
								
								isBase = isbaseComplete.get(teacherDetail.getTeacherId());
								sb.append("<th class='divlable' style='"+strCss_lbl+""+PDReport_column_width+"'>&nbsp;"+lblMobileNumber+": </th>");
								if((lstTeacherPersonalInfo1.size()==1 && lstTeacherPersonalInfo1.get(0).getMobileNumber()!=null && !lstTeacherPersonalInfo1.get(0).getMobileNumber().equalsIgnoreCase("")))
									sb.append("<th  class='divlableval' style='"+strCss_value+";padding-right:70px;margin-top:-5px;padding-left:5px;'><span style='width:40px;'>"+lstTeacherPersonalInfo1.get(0).getMobileNumber()+"</span></th>");
								else
									sb.append("<th  class='divlableval' style='"+strCss_value+";padding-right:70px;margin-top:-5px;padding-left:30px;'><span style='width:40px;'>&nbsp;N/A</span></th>");
								
								sb.append("</tr>");
						}else if(sVisitLocation.equalsIgnoreCase("Mosaic"))
						{
							strCss_value="padding:10px;text-align:left;vertical-align:text-top;";
							sb.append("<th class='divlable' style='"+strCss_lbl+""+Phone_column_width+"'>&nbsp;Phone Number: </th>");
								if(lstTeacherPersonalInfo1.size()==1)
								{
									if((lstTeacherPersonalInfo1.get(0).getPhoneNumber()!=null && !lstTeacherPersonalInfo1.get(0).getPhoneNumber().equalsIgnoreCase("")))
										sb.append("<th  class='divlableval' style='"+strCss_value+";margin-top:-5px;padding-left:12px;'><span class='span2'>"+lstTeacherPersonalInfo1.get(0).getPhoneNumber()+"</span></th>");	
									else
										sb.append("<th  class='divlableval' style='"+strCss_value+";margin-top:-5px;padding-left:5px;'><span class='span2'>&nbsp;N/A</span></th>");
								}
								else
									sb.append("<th  class='divlableval' style='"+strCss_value+";margin-top:-5px;padding-left:5px;'><span class='span2'>&nbsp;N/A</span></th>");
								
								// PD Report
								
								isBase = isbaseComplete.get(teacherDetail.getTeacherId());
								sb.append("<th class='divlable' style='"+strCss_lbl+""+PDReport_column_width+"'>&nbsp;"+lblMobileNumber+": </th>");
								if((lstTeacherPersonalInfo1.size()==1 &&  lstTeacherPersonalInfo1.get(0).getMobileNumber()!=null && !lstTeacherPersonalInfo1.get(0).getMobileNumber().equalsIgnoreCase("")))
									sb.append("<th  class='divlableval' style='"+strCss_value+";padding-right:70px;margin-top:-5px;padding-left:5px;'><span style='width:40px;'>"+lstTeacherPersonalInfo1.get(0).getMobileNumber()+"</span></th>");
								else
									sb.append("<th  class='divlableval' style='"+strCss_value+";padding-right:70px;margin-top:-5px;padding-left:30px;'><span style='width:40px;'>&nbsp;N/A</span></th>");
								
								sb.append("</tr>");
						}
						if(sVisitLocation.equalsIgnoreCase("Teacher Pool") && userMaster.getDistrictId()!=null && (userMaster.getDistrictId().getDistrictId().equals(7800049) || userMaster.getDistrictId().getDistrictId().equals(7800048) || userMaster.getDistrictId().getDistrictId().equals(7800050) || userMaster.getDistrictId().getDistrictId().equals(7800051) || userMaster.getDistrictId().getDistrictId().equals(7800053))){
						sb.append("<tr>");
						OctDetails octDetails = getOctDetails(teacherDetail);
						String octNumber ="";
						String octFileUpload ="";
						if(octDetails!=null && octDetails.getOctNumber()!=null){
							octFileUpload=octDetails.getOctFileName();
						}
						if(octDetails!=null && octDetails.getOctNumber()!=null){
							octNumber="<a href='#' onclick=\"getOctDetails('"+octDetails.getOctNumber()+"')\">"+octDetails.getOctNumber()+"</a>";
						}else{
							octNumber="N/A";
						}
							sb.append("<th class='divlable' style='"+strCss_lbl+""+Phone_column_width+"'>&nbsp;"+msgOCTRegistrationNum+": </th>");
							sb.append("<th  class='divlableval' style='"+strCss_value+";margin-top:-5px;padding-left:5px;'><span class='span2'>&nbsp;"+octNumber+"</span></th>");
							//sb.append("<th class='divlable' style='"+strCss_lbl+""+PDReport_column_width+"'>&nbsp;OCT&nbsp;Card: </th>");
							sb.append("<th class='divlable' style='"+strCss_lbl+""+PDReport_column_width+"'>&nbsp;"+Utility.getLocaleValuePropByKey("lbloctcard", locale)+": </th>");
							sb.append("<th  class='divlableval' style='"+strCss_value+";padding-right:70px;margin-top:-5px;padding-left:30px;'><span style='width:40px;'>&nbsp;");
							if(octFileUpload!=""){
								sb.append("<a data-original-title='"+Utility.getLocaleValuePropByKey("lbloctcard", locale)+"' rel='tooltip' id='hrefOctUpload' href='javascript:void(0);' onclick=\"downloadOctUpload('"+octDetails.getTeacherId().getTeacherId()+"');"+windowFunc+"\">View</a>");
							}else{
								sb.append("N/A");							
								}
							sb.append("</span></th>");						
							sb.append("</tr>");
							sb.append("<tr>");
							sb.append("<th class='divlable' style='"+strCss_lbl+""+Phone_column_width+"padding:0px;'>&nbsp;"+lblComments1+":</th>");
							sb.append("</tr>");
							sb.append("<tr>");
							sb.append("<td colspan='3' class='divlable' style='"+strCss_lbl+""+Phone_column_width+"'><textarea>");
							if(octDetails!=null && octDetails.getOctText()!=null){
								sb.append(octDetails.getOctText());
							}
						sb.append("</textarea></td>");
						sb.append("</tr>");
						}
						sb.append("</table>");
						
					sb.append("</th>");
					sb.append("</tr>");
					sb.append("</table>");
				
				sb.append("</th>");
			sb.append("</tr>");
				/////////////////
				
			//System.out.println("<--Phone--> "+lstTeacherPersonalInfo1.get(0).getPhoneNumber()+"<--Mobile--> "+lstTeacherPersonalInfo1.get(0).getMobileNumber());	
				// start new row
			//TestTool.getTraceTimeTPAP("TPAP 212");
				// Calculating and Get valles of "Achievement Score","Academic Achievement" and "Leadership/Results"
				String strAScore="";
				String strLRScore="";
				int iAcademicAchievement=0;
				int iLeadershipResults=0;
				int itotalAchievementScore=0;
				int iachievementMaxScore=0;
				
				int iacademicAchievementMaxScore=0;
				int ileadershipAchievementMaxScore=0;
				
				//A Score and L/R Score
				List<TeacherAchievementScore> lstTeacherAchievementScore_new=new ArrayList<TeacherAchievementScore>();
				DistrictMaster districtMasterObjForScore=null;
				if(entityID==1){ // For TM and For Mosaic
					if(sVisitLocation.equalsIgnoreCase("Mosaic") && districtMaster_Input!=null){
						districtMasterObjForScore=districtMaster_Input;
						Criterion criterion_district = Restrictions.eq("districtMaster", districtMasterObjForScore);
						lstTeacherAchievementScore_new =teacherAchievementScoreDAO.findByCriteria(criterion,criterion_district);
					}
				}
				else if(entityID==2 || entityID==3){ // For District OR SA
					districtMasterObjForScore=districtMaster;
					Criterion criterion_district = Restrictions.eq("districtMaster", districtMasterObjForScore);
					lstTeacherAchievementScore_new =teacherAchievementScoreDAO.findByCriteria(criterion,criterion_district);
				}
				
				int getScrrec = 0;
				if(lstTeacherAchievementScore_new!=null && lstTeacherAchievementScore_new.size()!=0){
					getScrrec=lstTeacherAchievementScore_new.size()-1;
				}
				if(lstTeacherAchievementScore_new!=null && lstTeacherAchievementScore_new.size()>0){
					iAcademicAchievement=lstTeacherAchievementScore_new.get(getScrrec).getAcademicAchievementScore();
					iLeadershipResults=lstTeacherAchievementScore_new.get(getScrrec).getLeadershipAchievementScore();
					
					iacademicAchievementMaxScore=lstTeacherAchievementScore_new.get(getScrrec).getAcademicAchievementMaxScore();
					ileadershipAchievementMaxScore=lstTeacherAchievementScore_new.get(getScrrec).getLeadershipAchievementMaxScore();
					
					itotalAchievementScore=lstTeacherAchievementScore_new.get(getScrrec).getTotalAchievementScore();
					iachievementMaxScore=lstTeacherAchievementScore_new.get(getScrrec).getAchievementMaxScore();
					
					strAScore=(iAcademicAchievement)+"/"+(iacademicAchievementMaxScore);
					strLRScore=(iLeadershipResults)+"/"+(ileadershipAchievementMaxScore);
				}
				else
				{
					strAScore="N/A";
					strLRScore="N/A";
				}
				
				strCss_lbl="padding-left:10px;";
				strCss_value="padding-left:105px;";
				if((sVisitLocation.equalsIgnoreCase("Mosaic") && entityID==1) || entityID==2 || entityID==3)
				{
					if(entityID==3)
						strCss_lbl="padding-left:51px;";
					
					sb.append("<tr>");
						sb.append("<td valign='top'>");
							sb.append("<table border='0' width=750 cellspacing=0 cellpadding=0 class='tablecgtbl'>");
							if(achievementScore)
							{
								if(sVisitLocation.equalsIgnoreCase("Teacher Pool") ||sVisitLocation.equalsIgnoreCase("PNR Dashboard"))
									sb.append("<tr><td style='width:30px;'>&nbsp;</td>");
								else
									sb.append("<tr><td style='width:50px;'>&nbsp;</td>");
									sb.append("<td valign='top'>" +
										"<table border='0' cellspacing=0 cellpadding=0 class='tablecgtbl'>" +
										"<TR>");
											// A Score and L/R Score
											if(sVisitLocation.equalsIgnoreCase("Teacher Pool") ||sVisitLocation.equalsIgnoreCase("PNR Dashboard")){
												strCss_lbl="padding:0px;padding-left:0px;width:55px;"; // Change padding-left 20 to 0 
												strCss_value="padding:0px;padding-left:0px;width:50px;";
											}
											else
											{
												strCss_lbl="padding:0px;padding-left:4px;width:55px;";
												strCss_value="padding:0px;padding-left:5px;width:50px;";
											}
											sb.append("<td class='divlable' style='"+strCss_lbl+"'>&nbsp;&nbsp;&nbsp;"+lblAScore+":</td>");
											sb.append("<td class='divlableval' style='"+strCss_value+";vertical-align:top;' id='ascore_profile'>&nbsp;"+strAScore+"</td>");

											sb.append("<td style='width:154px;'>&nbsp;</td>");
											if(sVisitLocation.equalsIgnoreCase("Teacher Pool") ||sVisitLocation.equalsIgnoreCase("PNR Dashboard")|| sVisitLocation.equalsIgnoreCase("My Folders") ){
												strCss_lbl="padding:0px;padding-left:0px;width:60px;"; // Change padding-left 20 to 0 
											}else{
												strCss_lbl="padding:0px;padding-left:2px;width:60px;";
											}
											sb.append("<td class='divlable' style='"+strCss_lbl+"'>&nbsp;&nbsp;&nbsp;"+lblLRScore+":</td>");
											sb.append("<td class='divlableval' style='"+strCss_value+";vertical-align:top;' id='lrscore_profile'>&nbsp;"+strLRScore+"</td>");
											sb.append("</TR>"+"</table>"+"</td></tr>");
											
											/* @Start
											 * @Ashish Kumar
											 * @Description :: District Admin can change achievement score.
											 * */
											if( (sVisitLocation.equalsIgnoreCase("Teacher Pool") || sVisitLocation.equalsIgnoreCase("PNR Dashboard")) && entityID==2){
												//System.out.println("******************2*******************");
												sb.append("<tr>");
												strCss_value="padding-left:55px;";
												// Blank Cell
												sb.append("<td style='width:55px;'>&nbsp;</td>");
												
												strCss_lbl="padding:2px;padding-left:4px;padding-top:5px;";
												sb.append("<td>"); 
												
												

												if(achievementScore && entityID==2)
												{
													sb.append("<table border='0' width=200 cellspacing=0 cellpadding=0 class='tablecgtbl'>");
													//1st Row for Label of "Achievement Score","Academic Achievement" and "Leadership/Results"
													sb.append("<tr>");
													//Academic Achievement:
														sb.append("<td class='divlable' style='"+strCss_lbl+"'>&nbsp;&nbsp;Academic&nbsp;Achievement:</td>");
													// Leadership/Results
														sb.append("<td class='divlable' style='"+strCss_lbl+"'>&nbsp;&nbsp;"+lblLeadershipResults+":</td>");
														
														sb.append("<td style='width:100px;'>&nbsp;</td>");
													sb.append("</tr>");
													
													strCss_lbl="padding:2px;padding-left:0px;padding-top:-20px;";
													//2nd Row for values of "Achievement Score","Academic Achievement" and "Leadership/Results"
													String strSliderCss_Iframe="border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:280px;";
													sb.append("<tr>");
														
													sb.append("<td style='"+strCss_lbl+"'>&nbsp;");
													sb.append("<div style='margin-left: 0px; margin-top: -18px;'><label>" +
															"<iframe id='ifrm_aca_ach' src='slideract.do?name=slider_aca_ach&tickInterval=5&max=20&swidth=240&svalue="+iAcademicAchievement+"' scrolling='no' frameBorder='0' style='"+strSliderCss_Iframe+"'>" +
															"</iframe></label>" +
															"</div>");
														
													sb.append("<input type='hidden' name='txt_sl_slider_aca_ach' id='txt_sl_slider_aca_ach' value='"+iAcademicAchievement+"'>");
													sb.append("</td>");
													
													
													// Leadership/Results
													sb.append("<td style='"+strCss_lbl+"'>&nbsp;");
													
													sb.append("<div style='margin-left: 0px; margin-top: -18px;'><label>" +
																"<iframe id='ifrm_lea_res' src='slideract.do?name=slider_lea_res&tickInterval=5&max=20&swidth=240&svalue="+iLeadershipResults+"' scrolling='no' frameBorder='0' style='"+strSliderCss_Iframe+"'>" +
																"</iframe></label>" +
																"</div>");
													sb.append("<input type='hidden' name='txt_sl_slider_lea_res' id='txt_sl_slider_lea_res' value='"+iLeadershipResults+"'>");
													//sb.append("<a href='javascript:void(0);' onclick=\"javascript:updateAScore("+teacherId+","+jobOrder.getDistrictMaster().getDistrictId()+")\"'>Go</A>");
													sb.append("</td>");
													
													//sb.append("<td style='width:20px;vertical-align:middle;'>&nbsp;<a href='javascript:void(0);' onclick=\"javascript:updateAScore("+teacherId+","+jobOrder.getDistrictMaster().getDistrictId()+")\"'>Go</A></td>");
													//sb.append("<td style='width:20px;vertical-align:middle;'>&nbsp;<a href='javascript:void(0);' onclick=\"javascript:updateAScore("+teacherId+","+jobOrder.getDistrictMaster().getDistrictId()+")\"'>Go</A></td>");
													//sb.append("<td style='width:40px;vertical-align:middle;'>&nbsp;<button class='btn btn-primary' type='button' onclick=\"javascript:updateAScore("+teacherId+","+jobOrder.getDistrictMaster().getDistrictId()+")\">Set A Score <i class='icon'></i></button></td>");
													
													//System.out.println(" Testing :: userMaster.getDistrictId() :: "+userMaster.getDistrictId().getDistrictId());
													
													sb.append("<td style='width:170px;vertical-align:middle;'>&nbsp;<button class='btn btn-primary' style='margin-top:-20px; width:106px;' type='button' onclick='javascript:updateAScoreFromTeacherPool("+teacherId+","+userMaster.getDistrictId().getDistrictId()+")'>Set A Score</td>");
													
													sb.append("</tr>");
													
													sb.append("</table>");
												}
												
											sb.append("</td>");
											sb.append("</tr>");
											}
											/* @End
											 * @Ashish Kumar
											 * @Description :: District Admin can change achievement score.
											 * */
							}
							sb.append("</table>");
						sb.append("</td>");
					sb.append("</tr>");
				
				}if(pnqDis){
					sb.append("<tr>");
					sb.append("<td style='padding:0px 0px 0px 53px'>");
				
					sb.append("<table border='0' cellspacing=0 cellpadding=0 class='tablecgtbl'>");
					sb.append("<tr>");				
					sb.append("<td class='divlable' style='padding:2px;padding-left:8px;padding-top:20px;'>&nbsp;PNQ:</td>");				
					String pNQVal = getPNQValue(lstTeacherExperience_new,teacherDetail);
					String New_pNQVal = "";
					sb.append("<th >&nbsp;"+pNQVal+"</th>");				
					sb.append("</td>");
					sb.append("</tr>");
					sb.append("</table>");
					
					sb.append("</td>");
					sb.append("</tr>");
				}
			sb.append("</table>");
			
			sb.append("<div id='removeWait' style='text-align:center;padding-top:0px;'><img src='images/loadingAnimation.gif' /> <br>"+lblLoding+"</div>");
			
			sb.append("</div>");
			sb.append("<DIV>");
			sb.append("<table border=0 width='100%'><tr bgcolor='#F2FAEF' class='custom-div-border'><th style='text-align:center;vertical-align:middle;padding-top:10px;padding-bottom:10px;'width='35%'><a href='javascript:void(0);' onclick=\"getCommunicationsDiv(1,0,'"+teacherDetail.getTeacherId()+"',0);\" ><span class='icon-dropbox icon-large iconcolor'></span>&nbsp;"+headCom+"</a></th>");
			if(userMaster.getEntityType()==1)
			{
				sb.append("<th style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><span class='icon-save icon-large iconcolorhover'></span>&nbsp;"+btnSave+"</th>" +
						"<th style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><span class='icon-share icon-large iconcolorhover'></span>&nbsp;"+btnShare+"</th>"+
						"<th style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><span class='icon-tag icon-large iconcolorhover'></span></span>&nbsp;"+headTag+"</th>");
			}
			else
			{
				
				int flagpopover=1;
				sb.append("<th style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><a href='javascript:void(0);' id='saveaction"+noOfRecordCheck+"' onclick=\"saveToFolderJFTNULL('0','"+teacherDetail.getTeacherId()+"',"+flagpopover+")\"><span class='icon-save icon-large iconcolor'></span>&nbsp;"+btnSave+"</a></th>" +
						"<th style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><a href='javascript:void(0);' id='shareaction"+noOfRecordCheck+"' onclick=\"displayUsergrid('"+teacherDetail.getTeacherId()+"',"+flagpopover+")\"><span class='icon-share icon-large iconcolor'></span>&nbsp;"+btnShare+"</a></th>");
				//sb.append("<th style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><span class='icon-tag icon-large iconcolor'></span></th>");
				
				//sb.append("<th style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><a href='javascript:void(0);' id='tagaction' onclick=\"closeProfileOpenTags("+noOfRecordCheck+")\"> <span class='icon-tag icon-large iconcolor'></span></span>&nbsp;Tag</a></th>");
				
				if(userMaster.getRoleId().getRoleId()==1 || userMaster.getRoleId().getRoleId()==3){
					sb.append("<th style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><span class='icon-tag icon-large iconcolorhover'></span></span>&nbsp;"+headTag+"</th>");
				}
				else{
					if(sVisitLocation.equalsIgnoreCase("Teacher Pool") ||sVisitLocation.equalsIgnoreCase("PNR Dashboard") ||sVisitLocation.equalsIgnoreCase("My Folders"))
						sb.append("<th style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><a href='javascript:void(0);' id='tagaction' onclick=\"closeProfileOpenTags("+teacherId+","+noOfRecordCheck+")\"> <span class='icon-tag icon-large iconcolor'></span></span>&nbsp;"+headTag+"</a></th>");
					else
						sb.append("<th style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><span class='icon-tag icon-large iconcolorhover'></span></span>&nbsp;"+headTag+"</th>");
				}
			}
			sb.append("</tr>");
			sb.append("</table>");
			sb.append("</DIV>");	
			/*sb.append("<div class='modal-footer'>"+
			 		"<button class='btn' onclick='showProfileContentClose();'>Close</button>"+ 		
			 	"</div>"+
			"</div>");*/
			sb.append("</div>");
			
			
			if(fg!=0){                //By Ravindra
				sb.append("####");
			String sGridLabelCss="padding:0px;padding-left:12px;padding-top:10px;color:black;";
			String sGridtableDisplayCSS="padding:10px;font-weight: normal;";
			sb.append("<table width="+sWidthFoeDiv+" border='0' cellspacing=0 cellpadding=0 >");
			//TestTool.getTraceTimeTPAP("TPAP 213");
			if(userMaster.getEntityType()!=1){
				//getDistrictSpecificQuestionGrid_ByDistrict(districtMaster.getDistrictId(),teacherId,sVisitLocation);
				Criterion criterion2=Restrictions.eq("districtMaster",districtMaster);
				Criterion criterion3=Restrictions.eq("teacherDetail",teacherDetail);
				int lst=teacherAnswerDetailsForDistrictSpecificQuestionsDAO.findByCriteria(criterion2,criterion3).size();
				if(lst>0){
				if(sVisitLocation.equalsIgnoreCase("Mosaic")){
				//	sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'>District Specific Question</td></tr>");
				}else if(sVisitLocation.equalsIgnoreCase("My Folders")){
				//	sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'><b>District Specific Question</b></td></tr>");
				}else{
				//	sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'><b>District Specific Question</b></td></tr>");
				}
				
				sb.append("<tr><td colspan=5 width='100%'>");
					if(sVisitLocation.equalsIgnoreCase("Mosaic")){
						sb.append("<div style='margin-left:20px;margin-top:8px;'><table  style='margin-right:45px;padding-left:12px;' border='0' width='100%' onmouseover=\"javascript:setGridProfileVariable('teacherCerti')\">");
					//	sb.append("<tr>");
					//	sb.append("<td id='getDistrictSpecificQuestion'></td>");
					//	sb.append("</tr>");
						sb.append("</table></div>");
					}else if(sVisitLocation.equalsIgnoreCase("My Folders")){
						sb.append("<div style='margin-left:20px;margin-top:8px;'><table  style='margin-right:45px;padding-left:12px;' border='0' width='100%' onmouseover=\"javascript:setGridProfileVariable('teacherCerti')\">");
					//	sb.append("<tr>");
					//	sb.append("<td id='getDistrictSpecificQuestion'></td>");
					//	sb.append("</tr>");
						sb.append("</table></div>");
					}
					else{
						sb.append("<div style='margin-left:20px;'><table style='margin-right:45px;padding-left:12px;color:#484848;' border='0' width='100%' onmouseover=\"javascript:setGridProfileVariable('teacherCerti')\">");
					//	sb.append("<tr>");
					//	sb.append("<td id='getDistrictSpecificQuestion'></td>");
					//	sb.append("</tr>");
						sb.append("</table></div>");
					}
				sb.append("</td></tr>");
					if(sVisitLocation.equalsIgnoreCase("Mosaic")){
					//	sb.append("<script>getDistrictSpecificQuestion_DivProfile("+districtMaster.getDistrictId()+","+teacherId+",'"+sVisitLocation+"');</script>");
					}else if(sVisitLocation.equalsIgnoreCase("My Folders")){
					//	sb.append("<script>getDistrictSpecificQuestion_DivProfile_FromMyFolders("+districtMaster.getDistrictId()+","+teacherId+",'"+sVisitLocation+"');</script>");
					}else{
					//	sb.append("<script>getDistrictSpecificQuestion_DivProfile("+districtMaster.getDistrictId()+","+teacherId+");</script>");
					}
				}
			}
			
			
			/*******************************************Code Start Here********************************************/
			if(sVisitLocation.equalsIgnoreCase("Teacher Pool") ||sVisitLocation.equalsIgnoreCase("PNR Dashboard") ||sVisitLocation.equalsIgnoreCase("My Folders"))
			{
				if(sVisitLocation.equalsIgnoreCase("Teacher Pool") && userMaster!=null && userMaster.getDistrictId()!=null && userMaster.getDistrictId().getDistrictId().equals(1302010))
					if(teacherPersonalInfo!=null && teacherPersonalInfo.getEmployeeType()!=null && teacherPersonalInfo.getEmployeeType().equals(0)){
						sb.append("<tr><td style='"+sGridLabelCss+"' >");
						sb.append("<table ><tr><td colspan='2' class='divlable' style='padding:2px 2px 2px 24px;white-space:nowrap'>Why are you no longer employed with us?</td></tr>");
						sb.append("<tr><td style='padding:2px 0px 2px 24px;white-space:nowrap'>Ans: &nbsp;&nbsp;</td>");
						if(teacherPersonalInfo!=null && teacherPersonalInfo.getNoLongerEmployed()!=null && !teacherPersonalInfo.getNoLongerEmployed().equalsIgnoreCase(""))
							sb.append("<td>"+teacherPersonalInfo.getNoLongerEmployed()+"</td>");
						else{
							sb.append("<td class='divlableval'>N/A</td>");
						}
						sb.append("</tr>");
						sb.append("</table></td></tr>");
					}
			//************************************Start of Veteran Origin**************************//
			try
			{
				if( (districtPortfolioConfig==null || (districtPortfolioConfig.getVeteran()!=null && districtPortfolioConfig.getVeteran()) ) && (districtMaster!=null && districtMaster.getDistrictId()!=null && districtMaster.getDistrictId().equals(1201470)) )
				{
					String veteranOrigin = getVeteranOrigin(districtMaster, teacherDetail,teacherPersonalInfo);
					sb.append("<tr><td class='net-header-text' style='"+sGridLabelCss+"' >");
					sb.append(veteranOrigin);
					sb.append("</td> </tr>");
				}
			}
			catch(Exception e){e.printStackTrace();}
			//************************************End of Veteran Origin****************************//
			
			//************************************Start of Ethnic Origin**************************//
			try
			{
				/*if( (districtPortfolioConfig==null || (districtPortfolioConfig.getEthnicorigin()!=null && districtPortfolioConfig.getEthnicorigin())) && (districtMaster!=null && districtMaster.getDistrictId()!=null && districtMaster.getDistrictId().equals(1201470)) )
				{
					String ethnicOrigin = getEthnicOrigin(teacherPersonalInfo);
					sb.append("<tr><td class='net-header-text' style='"+sGridLabelCss+"' >");
					sb.append(ethnicOrigin);
					sb.append("</td> </tr>");
				}*/
			}
			catch(Exception e){e.printStackTrace();}
			//************************************End of Ethnic Origin**************************//
						
			//************************************Start of Ethnicity***************************//
			try
			{
				/*if( (districtPortfolioConfig==null || (districtPortfolioConfig.getEthinicity()!=null && districtPortfolioConfig.getEthinicity())) && (districtMaster!=null && districtMaster.getDistrictId()!=null && districtMaster.getDistrictId().equals(1201470)) )
				{
					String ethinicity = getEthnicity(teacherPersonalInfo);
					sb.append("<tr><td class='net-header-text' style='"+sGridLabelCss+"' >");
					sb.append(ethinicity.toString());
					sb.append("</td> </tr>");
				}*/
			}
			catch(Exception e){e.printStackTrace();}
			
			//************************************End of Ethnicity****************************//
			
			//************************************Start of Race*************************//
			try
			{
				/*if( (districtPortfolioConfig==null || (districtPortfolioConfig.getRace()!=null && districtPortfolioConfig.getRace())) && (districtMaster!=null && districtMaster.getDistrictId()!=null && districtMaster.getDistrictId().equals(1201470)) )
				{
					String race = getRace(teacherPersonalInfo);
					sb.append("<tr><td class='net-header-text' style='"+sGridLabelCss+"' >");
					sb.append(race);
					sb.append("</td> </tr>");
				}*/
			}
			catch(Exception e){e.printStackTrace();}
			//************************************End of Race***************************//
			}	//End Of check
			
			
			/*************   GE SA Exam ******************/
			if(districtMaster!=null && !districtMaster.getDistrictId().equals(1201470) && !districtMaster.getDistrictId().equals(7800040) && !districtMaster.getDistrictId().equals(7800294))
			if(districtPortfolioConfig==null || (districtPortfolioConfig.getGeneralKnowledgeExam()!=null && districtPortfolioConfig.getGeneralKnowledgeExam()) )
			{
			// start new row   >>> General Knowledge Exam
			   int geFlag=0;
			   try{
					TeacherGeneralKnowledgeExam teacherGeneralKnowledgeExam=getGKEValuesForProfile(teacherDetail);
					if(teacherGeneralKnowledgeExam!=null){
						sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'>&nbsp;<b>"+lblGeneralKnowledgeExam+":</b></td></tr>");
						sb.append("<tr>");
						sb.append("<td>");
							sb.append("<table border='0' cellspacing=0 cellpadding=0 class='tablecgtbl'>");
							sb.append("<tr>");
								strCss_value="padding-left:55px;";
								sb.append("<td style='"+strCss_value+"'>&nbsp;</td>");
								strCss_lbl="padding:2px;vertical-align:middle;";
								strCss_value="padding:2px;vertical-align:text-top;";
								sb.append("<td>"); 
								sb.append("<table border='0' cellspacing=0 cellpadding=0 class='tablecgtbl'>");
								sb.append("<tr>");
								strCss_lbl="padding:2px;padding-left:2px;vertical-align:middle;";
								//sb.append("<td class='divlable' style='"+strCss_lbl+"'>&nbsp;General Knowledge Exam: </td>");
								if(teacherGeneralKnowledgeExam!=null){
									geFlag=1;
									sb.append("<td class='divlable' style='"+strCss_lbl+";margin-top:-5px;'>Exam Status</td>");
								    sb.append("<td class='divlable' style='"+strCss_lbl+";margin-top:-5px;padding-left:60px;'>"+lblExamDate+"</td>");
								    sb.append("<td class='divlable' style='"+strCss_lbl+";margin-top:-5px;padding-left:60px;'>"+lblScoreReport+"</td>");
								    								    sb.append("<td class='divlable' style='"+strCss_lbl+";margin-top:-5px;padding-left:30px;'>"+lblNotes+"</td>");
								}
								sb.append("</tr>");
								sb.append("</table>");
							sb.append("</td>");
							sb.append("</tr>");
							sb.append("</table>");
						sb.append("</td>");
					   sb.append("</tr>");
					}else{
						sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'>&nbsp;<b>"+lblGeneralKnowledgeExam+"</b>:&nbsp;&nbsp;<span class='divlableval'>N/A</span></td></tr>");
					}
			   }catch (Exception e) {
					e.printStackTrace();
				}
			   //TestTool.getTraceTimeTPAP("TPAP 214");
			   if(geFlag==1){
				   sb.append("<tr>");
					sb.append("<td>");
						sb.append("<table border='0' cellspacing=0 cellpadding=0 class='tablecgtbl'>");
						sb.append("<tr>");
							strCss_value="padding-left:55px;";
							sb.append("<td style='"+strCss_value+"'>&nbsp;</td>");
							strCss_lbl="padding:2px;vertical-align:middle;";
							strCss_value="padding:2px;vertical-align:text-top;";
							sb.append("<td>"); 
							sb.append("<table border='0' cellspacing=0 cellpadding=0 class='tablecgtbl'>");
							sb.append("<tr>");
							strCss_lbl="padding:2px;padding-left:2px;vertical-align:middle;";
							//sb.append("<td class='divlable' style='"+strCss_lbl+"padding-left:140px;'>&nbsp;</td>");
							try{
								TeacherGeneralKnowledgeExam teacherGeneralKnowledgeExam=getGKEValuesForProfile(teacherDetail);
								if(teacherGeneralKnowledgeExam!=null){
									if(teacherGeneralKnowledgeExam.getGeneralKnowledgeExamStatus().equals("P"))
										sb.append("<td class='divlableval' style='"+strCss_value+";margin-top:-5px;'>Pass</td>");
									else if(teacherGeneralKnowledgeExam.getGeneralKnowledgeExamStatus().equals("F"))	
										sb.append("<td class='divlableval' style='"+strCss_value+";margin-top:-5px;'>Fail</td>");
									
									sb.append("<td class='divlableval' style='"+strCss_value+";margin-top:-5px;padding-left:100px;'>"+Utility.convertDateAndTimeToDatabaseformatOnlyDate(teacherGeneralKnowledgeExam.getGeneralKnowledgeExamDate())+"</td>");
								    sb.append("<td  class='divlableval' style='"+strCss_value+";margin-top:-5px;padding-left:60px;'>&nbsp;<a data-original-title='"+lblScoreReport+"' rel='tooltip' id='tpScoreReportGE' href='javascript:void(0);' onclick=\"downloadGeneralKnowledge('"+teacherGeneralKnowledgeExam.getGeneralKnowledgeExamId()+"','"+teacherGeneralKnowledgeExam.getTeacherDetail().getTeacherId()+"','tpScoreReportGE');"+windowFunc+"\">");
								    sb.append("<span class='icon-download-alt icon-large iconcolorBlue' style='padding-left:20px;'></span>");
								    sb.append("</a></td>");
								    sb.append("<td class='' style='"+strCss_value+";margin-top:-5px;padding-left:47px;'>");
								    sb.append(teacherGeneralKnowledgeExam.getGeneralExamNote());
								    sb.append("</td>");
								}
							}catch (Exception e) {
								e.printStackTrace();
							}
							sb.append("</tr>");
							sb.append("</table>");
						sb.append("</td>");
						sb.append("</tr>");
						sb.append("</table>");
					sb.append("</td>");
				   sb.append("</tr>");
					// End new row 
			   }
			}
			
			System.out.println(" >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Special Note >>>>>>>>>>>>>>>>>>>>>>>>>>>>");
			//******************************** Special Note ****************************//NC
			//&& userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==2
			//if(userMaster!=null && userMaster.getEntityType()==2 && userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==2){
				if(userMaster!=null  && userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==NC_HEADQUARTER){
				   
				   sb.append("<tr>");
					sb.append("<td>");
					sb.append(getSpecialNoteForTeacher(teacherDetail));
					sb.append("</td>");
					sb.append("</tr>");
				   
			}
			
			
			if(districtMaster!=null && !districtMaster.getDistrictId().equals(1201470) && !districtMaster.getDistrictId().equals(7800040) && !districtMaster.getDistrictId().equals(7800294))
				if(sVisitLocation.equalsIgnoreCase("Teacher Pool")){
					//subject==========================================			
						sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'>"+lblSubjectAreaExam+"</td></tr>");
						sb.append("<tr><td colspan=5 width='100%'>");
							sb.append("<table border=0 width='100%'>");
								sb.append("<tr>");
									sb.append("<td style="+sGridtableDisplayCSS+">");
									sb.append(getSubjectAreasGrid(teacherDetail));
									sb.append("</td>");
								sb.append("</tr>");
							sb.append("</table>");
						sb.append("</td></tr>");
						sb.append("<script>applyScrollOnTblSubjectAreaExam();</script>");
					//subject==========================================
				}
			
			if(districtPortfolioConfig==null || (districtPortfolioConfig.getSubjectAreaExam()!=null && districtPortfolioConfig.getSubjectAreaExam()) )
			{/*			   
			   // subject ---- 
			   int saFlag=0;
			   try{
				TeacherSubjectAreaExam teacherSubjectAreaExam=getSAEValuesForProfile(teacherDetail);
				if(teacherSubjectAreaExam!=null){
					 sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'>&nbsp;<b>"+lblSubjectAreaExam+":</b></td></tr>");
					 sb.append("<tr>");
						sb.append("<td>");
							sb.append("<table border='0' cellspacing=0 cellpadding=0 class='tablecgtbl'>");
							sb.append("<tr>");
								strCss_value="padding-left:55px;";
								sb.append("<td style='"+strCss_value+"'>&nbsp;</td>");
								strCss_lbl="padding:2px;vertical-align:middle;";
								strCss_value="padding:2px;vertical-align:text-top;";
								sb.append("<td>"); 
								sb.append("<table border='0' cellspacing=0 cellpadding=0 class='tablecgtbl'>");
								sb.append("<tr>");
								strCss_lbl="padding:2px;padding-left:2px;vertical-align:middle;";
								//sb.append("<td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblSubjectAreaExam+": </td>");
								if(teacherSubjectAreaExam!=null){
									saFlag=1;
									sb.append("<td class='divlable' style='"+strCss_lbl+";margin-top:-5px;'>Exam Status</td>");
								    sb.append("<td class='divlable' style='"+strCss_lbl+";margin-top:-5px;padding-left:60px;'>"+lblExamDate+"</td>");
								    sb.append("<td class='divlable' style='"+strCss_lbl+";margin-top:-5px;padding-left:60px;'>"+lblScoreReport+"</td>");
								}
								sb.append("</tr>");
								sb.append("</table>");
							sb.append("</td>");
							sb.append("</tr>");
							sb.append("</table>");
						sb.append("</td>");
					   sb.append("</tr>");
				}else{
					 sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'>&nbsp;<b>"+lblSubjectAreaExam+":</b> &nbsp;<span class='divlableval'>N/A</sapn></td></tr>");
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
			
			if(saFlag==1){
				   sb.append("<tr>");
					sb.append("<td>");
						sb.append("<table border='0' cellspacing=0 cellpadding=0 class='tablecgtbl'>");
						sb.append("<tr>");
							strCss_value="padding-left:55px;";
							sb.append("<td style='"+strCss_value+"'>&nbsp;</td>");
							strCss_lbl="padding:2px;vertical-align:middle;";
							strCss_value="padding:2px;vertical-align:text-top;";
							sb.append("<td>"); 
							sb.append("<table border='0' cellspacing=0 cellpadding=0 class='tablecgtbl'>");
							sb.append("<tr>");
							strCss_lbl="padding:2px;padding-left:2px;vertical-align:middle;";
							//sb.append("<td class='divlable' style='"+strCss_lbl+"padding-left:140px;'>&nbsp;</td>");
							try{
								TeacherSubjectAreaExam teacherSubjectAreaExam=getSAEValuesForProfile(teacherDetail);
								if(teacherSubjectAreaExam!=null){
									if(teacherSubjectAreaExam.getExamStatus().equals("P"))
										sb.append("<td  class='divlableval' style='"+strCss_value+";margin-top:-5px;'>"+optPass+"</td>");
									else if(teacherSubjectAreaExam.getExamStatus().equals("F"))	
										sb.append("<td  class='divlableval' style='"+strCss_value+";margin-top:-5px;'>"+optFail+"</td>");
									
									sb.append("<td  class='divlableval' style='"+strCss_value+";margin-top:-5px;padding-left:100px;'>"+Utility.convertDateAndTimeToDatabaseformatOnlyDate(teacherSubjectAreaExam.getExamDate())+"</td>");
								    sb.append("<td  class='divlableval' style='"+strCss_value+";margin-top:-5px;padding-left:57px;'>&nbsp;<a data-original-title='"+lblScoreReport+"' rel='tooltip' id='tpScoreReportSA' href='javascript:void(0);' onclick=\"downloadSubjectAreaExam('"+teacherSubjectAreaExam.getTeacherSubjectAreaExamId()+"','"+teacherSubjectAreaExam.getTeacherDetail().getTeacherId()+"','tpScoreReportSA');"+windowFunc+"\">");
								    sb.append(teacherSubjectAreaExam.getScoreReport());
								    sb.append("</a></td>");
								}
							}catch (Exception e) {
								e.printStackTrace();
							}
							sb.append("</tr>");
							sb.append("</table>");
						sb.append("</td>");
						sb.append("</tr>");
						sb.append("</table>");
					sb.append("</td>");
				   sb.append("</tr>");
					// End new row 
			   }
			*/}
			   /********************************************************/
			
			//TestTool.getTraceTimeTPAP("TPAP 215");
			if(!headQuarterMaster ||(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==NC_HEADQUARTER))
			if(districtPortfolioConfig==null || (districtPortfolioConfig.getAcademic()!=null && !districtPortfolioConfig.getAcademic().equals(0)) )
			{
			// Start Teacher Academics Grid
			if(sVisitLocation.equalsIgnoreCase("Mosaic")){
				sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'>"+lblAcademics+"</td></tr>");
			}else if(sVisitLocation.equalsIgnoreCase("My Folders")){
				sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'><b>"+lblAcademics+"</b></td></tr>");
			}else{
				sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'><b>"+lblAcademics+"</b></td></tr>");
			}
			
			sb.append("<tr><th colspan=5 width='100%'>");
				sb.append("<table border=0 width='100%' onmouseover=\"javascript:setGridProfileVariable('teacherAce')\">");
					sb.append("<tr>");
						sb.append("<th style="+sGridtableDisplayCSS+" id='gridDataTeacherAcademics'>");
						sb.append("</th>");
					sb.append("</tr>");
				sb.append("</table>");
			sb.append("</th></tr>");
			sb.append("<script>getTeacherAcademicsGrid_DivProfile("+teacherDetail.getTeacherId()+");</script>");
			// End Teacher Academics Grid
			}
			//sandeep Start Teacher education Grid
			  
			if(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==NC_HEADQUARTER){
				  
			      if(sVisitLocation.equalsIgnoreCase("Mosaic")){
			       sb.append("<tr><td colspan=4 class='net-header-text' style='"+sGridLabelCss+"'>Education</td></tr>");
			      }else if(sVisitLocation.equalsIgnoreCase("My Folders")){
			       sb.append("<tr><td colspan=4 class='net-header-text' style='"+sGridLabelCss+"'><b>Education</b></td></tr>");
			      }else{
			       sb.append("<tr><td colspan=4 class='net-header-text' style='"+sGridLabelCss+"'><b>Education</b></td></tr>");
			      }
			      
			      sb.append("<tr><th colspan=4 width='100%'>");
			       sb.append("<table border=0 width='100%' style='margin-left:10px;'>");
			        sb.append("<tr>");
			         sb.append("<th style="+sGridtableDisplayCSS1+" id='gridDataTeacherEducations'>");
			         sb.append("</th>");
			        sb.append("</tr>");
			       sb.append("</table>");
			      sb.append("</th></tr>");
			      sb.append("<script>getTeacherEducationGrid_DivProfile("+teacherDetail.getTeacherId()+");</script>");
			}     
		      // End Teacher Education Grid
			
			System.out.println("11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111");
			if(!headQuarterMaster ||(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==NC_HEADQUARTER))
			if(districtPortfolioConfig==null || (districtPortfolioConfig.getCertification()!=null && !districtPortfolioConfig.getCertification().equals(0)) )
			{
			// Start Teacher Certifications Grid
			if(sVisitLocation.equalsIgnoreCase("Mosaic")){
				sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'>"+lblCertiLice+"</td></tr>");
			}else if(sVisitLocation.equalsIgnoreCase("My Folders")){
				sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'><b>"+lblCertiLice+"</b></td></tr>");
			}else{
				sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'><b>"+lblCertiLice+"</b></td></tr>");
			}
			if(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==NC_HEADQUARTER){
				sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'><b>"+lblLicense+"</b></td></tr>");
				sb.append("<tr><th colspan=5 width='100%'>");
				sb.append("<table border=0 width='100%' onmouseover=\"javascript:setGridProfileVariable('teacherCerti')\">");
					sb.append("<tr>");
						sb.append("<th style="+sGridtableDisplayCSS+" id='divLicneseGridCertificationsGridDate'>");
						sb.append("</th>");
					sb.append("</tr>");
				sb.append("</table>");
				sb.append("</th></tr>");
				sb.append("<script>getTeacherLicenceGrid_DivProfile("+teacherDetail.getTeacherId()+");</script>");
				
				
				sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'><b>"+lblLEACandidatePortfolio+"</b></td></tr>");
				sb.append("<tr><th colspan=5 width='100%'>");
				sb.append("<table border=0 width='100%' onmouseover=\"javascript:setGridProfileVariable('teacherCerti')\">");
					sb.append("<tr>");
						sb.append("<th style="+sGridtableDisplayCSS+" id='divLEACandidatePotfolioDiv'>");
						sb.append("</th>");
					sb.append("</tr>");
				sb.append("</table>");
				sb.append("</th></tr>");
				sb.append("<script>getLEACandidatePortfolio_DivProfile("+teacherDetail.getTeacherId()+");</script>");
				
				sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'><b>"+lblCertiLicense+"</b></td></tr>");
			}
			sb.append("<tr><th colspan=5 width='100%'>");
				sb.append("<table border=0 width='100%' onmouseover=\"javascript:setGridProfileVariable('teacherCerti')\">");
					sb.append("<tr>");
						sb.append("<th style="+sGridtableDisplayCSS+" id='gridDataTeacherCertifications'>");
						sb.append("</th>");
					sb.append("</tr>");
				sb.append("</table>");
			sb.append("</th></tr>");
			sb.append("<script>getTeacherCertificationsGrid_DivProfile("+teacherDetail.getTeacherId()+");</script>");
			// End Teacher Certifications Grid
			}
			
			if(districtMaster!=null && districtMaster.getDistrictId().equals(1201470))
			if(sVisitLocation.equalsIgnoreCase("Teacher Pool") ||sVisitLocation.equalsIgnoreCase("PNR Dashboard")||sVisitLocation.equalsIgnoreCase("My Folders"))
			{
			
			if(districtPortfolioConfig==null || (districtPortfolioConfig.getHonors()!=null && districtPortfolioConfig.getHonors()) )
			{
			// Start Teacher Honors Grid
			if(sVisitLocation.equalsIgnoreCase("Mosaic")){
				sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'>"+lblHonors+"</td></tr>");
			}else if(sVisitLocation.equalsIgnoreCase("My Folders")){
				sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'><b>"+lblHonors+"</b></td></tr>");
			}else{
				sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'><b>"+lblHonors+"</b></td></tr>");
			}
			sb.append("<tr><th colspan=5 width='100%'>");
				sb.append("<table border=0 width='100%' onmouseover=\"javascript:setGridProfileVariable('teacherCerti')\">");
					sb.append("<tr>");
						sb.append("<td style="+sGridtableDisplayCSS+" id='gridDataTeacherHonors'>");
						sb.append("</td>");
					sb.append("</tr>");
				sb.append("</table>");
			sb.append("</th></tr>");
			sb.append("<script>getHonorsGrid("+teacherDetail.getTeacherId()+");</script>");
			// End Teacher Honors Grid
			}
			
			if(districtPortfolioConfig==null || (districtPortfolioConfig.getInvolvement()!=null && districtPortfolioConfig.getInvolvement()) )
			{
			//Start Involvement/Volunteer Work Grid//
			if(sVisitLocation.equalsIgnoreCase("Mosaic")){
				sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'>"+lblIVWork+"</td></tr>");
			}else if(sVisitLocation.equalsIgnoreCase("My Folders")){
				sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'><b>"+lblIVWork+"</b></td></tr>");
			}else{
				sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'><b>"+lblIVWork+"</b></td></tr>");
			}
			
			sb.append("<tr><th colspan=5 width='100%'>");
			sb.append("<table border=0 width='100%' onmouseover=\"javascript:setGridProfileVariable('teacherInvolve')\">");
			sb.append("<tr>");
			sb.append("<th style="+sGridtableDisplayCSS+" id='gridDataTeacherInvolvementOrVolunteerWork'>");
			sb.append("</th>");
			sb.append("</tr>");
			sb.append("</table>");
			sb.append("</th></tr>");
			sb.append("<script>getInvolvementGrid("+teacherDetail.getTeacherId()+");</script>");
			//End Involvement/Volunteer Work Grid//
			}
			
			}//End of teacher pool check
			if(!headQuarterMaster ||(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==NC_HEADQUARTER))
			if(districtPortfolioConfig==null || (districtPortfolioConfig.getEmployment()!=null && districtPortfolioConfig.getEmployment()) )
			{
			// Start Work Experience
			if(sVisitLocation.equalsIgnoreCase("Mosaic")){
				sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'>"+lblWorkExp+"</td></tr>");
			}else if(sVisitLocation.equalsIgnoreCase("My Folders")){
				sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'><b>"+lblWorkExp+"</b></td></tr>");
			}else{
				sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'><b>"+lblWorkExp+"</b></td></tr>");
			}
			sb.append("<tr><th colspan=5 width='100%'>");
				sb.append("<table border=0 width='100%' onmouseover=\"javascript:setGridProfileVariable('workExp')\">");
					sb.append("<tr>");
						sb.append("<th style="+sGridtableDisplayCSS+" id='gridDataWorkExpEmployment'>");
						sb.append("</th>");
					sb.append("</tr>");
				sb.append("</table>");
			sb.append("</th></tr>");
			sb.append("<script>getPFEmploymentGrid_DivProfile("+teacherDetail.getTeacherId()+");</script>");
			// End employment Grid
			}
			
			//Ravindra Teacher Assessment Start
			if(!sVisitLocation.equalsIgnoreCase("Mosaic") && !sVisitLocation.equalsIgnoreCase("My Folders")){			
			if(sVisitLocation.equalsIgnoreCase("Mosaic")){
				sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'>"+lblAssessmentDetails+"</td></tr>");
			}else if(sVisitLocation.equalsIgnoreCase("My Folders")){
				sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'><b>"+lblAssessmentDetails+"</b></td></tr>");
			}else{
				sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'><b>"+lblAssessmentDetails+"</b></td></tr>");
			}
			sb.append("<tr><th colspan=5 width='100%'>");
				sb.append("<table border=0 width='100%' onmouseover=\"javascript:setGridProfileVariable('teacherAssessment')\">");
					sb.append("<tr>");
						sb.append("<th style="+sGridtableDisplayCSS+" id='gridTeacherAssessment'>");
						sb.append("</th>");
					sb.append("</tr>");
				sb.append("</table>");
			sb.append("</th></tr>");
			sb.append("<script>getTeacherAssessmentGrid_DivProfile("+teacherDetail.getTeacherId()+");</script>");
			}
			// Ravindra Teacher Assessment End
			
			
			//if(!headQuarterMaster)
			if(!headQuarterMaster ||(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==NC_HEADQUARTER))
			if(districtPortfolioConfig==null || (districtPortfolioConfig.getReference()!=null && !districtPortfolioConfig.getReference().equals(0)) )
			{
			// Start Reference Grid
			if(sVisitLocation.equalsIgnoreCase("Mosaic")){
				//sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'>"+Utility.getLocaleValuePropByKey("lblRef", locale)+"</td></tr>");

				sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+";padding-left: 0px;'>");
				   sb.append("<div class=''>");
				    sb.append("<div class='col-sm-6 col-md-6'>"+Utility.getLocaleValuePropByKey("lblRef", locale)+"</div>");
			      if(userMaster.getRoleId().getRoleId().equals(2) || userMaster.getRoleId().getRoleId().equals(3))
			    		  sb.append("<div class='col-sm-6 col-md-6' style='text-align:  right;' ><a href='#' onclick='return showReferencesForm();'>"+lnkAddRef+"</a></div>");
			      
			    sb.append("</div></td></tr>");
			
			}else if(sVisitLocation.equalsIgnoreCase("My Folders")){
				//sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'><b>"+Utility.getLocaleValuePropByKey("lblRef", locale)+"</b></td></tr>");
				sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+";padding-left: 0px;'>");
				   sb.append("<div class=''>");
				    sb.append("<div class='col-sm-6 col-md-6'>"+Utility.getLocaleValuePropByKey("lblRef", locale)+"</div>");
			      
			    	  if(userMaster.getRoleId().getRoleId().equals(2) || userMaster.getRoleId().getRoleId().equals(3))
			    		  sb.append("<div class='col-sm-6 col-md-6' style='text-align:  right;' ><a href='#' onclick='return showReferencesForm();'>"+lnkAddRef+"</a></div>");
			      
			    sb.append("</div></td></tr>");
				//sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'><b>"+Utility.getLocaleValuePropByKey("lblRef", locale)+"</b></td></tr>");
			}else{
				//sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'><b>"+Utility.getLocaleValuePropByKey("lblRef", locale)+"</b></td></tr>");
				
				sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+";padding-left: 0px;'>");
				   sb.append("<div class=''>");
				    sb.append("<div class='col-sm-6 col-md-6'>"+Utility.getLocaleValuePropByKey("lblRef", locale)+"</div>");
			      if(userMaster.getRoleId().getRoleId().equals(2) || userMaster.getRoleId().getRoleId().equals(3))
			       {
			    	
			    		  sb.append("<div class='col-sm-6 col-md-6' style='text-align:  right;' ><a href='#' onclick='return showReferencesForm();'>"+lnkAddRef+"</a></div>");
			       }
			    sb.append("</div></td></tr>");
				//sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'><b>"+Utility.getLocaleValuePropByKey("lblRef", locale)+"</b></td></tr>");
			}
			sb.append("<tr><th colspan=5 width='100%'>");
				sb.append("<table border=0 width='100%' onmouseover=\"javascript:setGridProfileVariable('References')\">");
					sb.append("<tr>");
						sb.append("<th style="+sGridtableDisplayCSS+" id='gridDataReference'>");
						sb.append("</th>");
					sb.append("</tr>");
				sb.append("</table>");
			sb.append("</th></tr>");
			sb.append("<script>getElectronicReferencesGrid_DivProfile("+teacherDetail.getTeacherId()+");</script>");
			// End Reference Grid
			
			//TestTool.getTraceTimeTPAP("TPAP 216");
//RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
			
			sb.append("<tr><td style='padding-left: 15px;'>");
			sb.append("<div class='portfolio_Section_ImputFormGap' style='display: none;' id='divElectronicReferences'>");
			sb.append("<div class='row'>");
			sb.append(" <div class='col-sm-12 col-md-12'>");
				sb.append(" <div class='divErrorMsg' id='errordivElectronicReferences' style='display: block;''></div>");
			sb.append(" </div>");
			sb.append(" </div>");
			
			   sb.append("<iframe id='uploadFrameReferencesID' name='uploadFrameReferences' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'></iframe>");
			   sb.append("<form id='frmElectronicReferences' name='frmElectronicReferences' enctype='multipart/form-data' method='post' target='uploadFrameReferences' action='fileuploadservletforreferencesnoble.do' >");
			   sb.append("<input type='hidden' id='teacherId' name='teacherId' value="+teacherId+" >");	
			   sb.append("<div class='row'>");
						
					sb.append("<div class='col-sm-3 col-md-3'>");
						 sb.append("<label>Salutation</label>");
						   sb.append("<select class='form-control' id='salutation' name='salutation'>");
							    sb.append("<option value='0'></option>");
								sb.append("<option value='4'>Dr.</option>");
								sb.append("<option value='3'>Miss</option>");
								sb.append("<option value='2'>Mr.</option>");
								sb.append("<option value='1'>Mrs.</option>");
								sb.append("<option value='5'>Ms.</option>");													
							 sb.append("</select>");
							sb.append("</div>");

					sb.append("<div class='col-sm-3 col-md-3'>");
					  sb.append("<label>"+lblFname+"<span class='required'>*</span></label>");
					   sb.append("<input type='hidden' id='elerefAutoId'>");
					   sb.append("<input type='text' id='firstName1' name='firstName1' class='form-control' placeholder='' maxlength='50'>");
					sb.append("</div>");
					
					sb.append("<div class='col-sm-3 col-md-3'>");
					  sb.append("<label>"+lblLname+"<span class='required'>*</span></label>");
					sb.append("<input type='text' id='lastName1' name='lastName1' class='form-control' placeholder='' maxlength='50'>");
					sb.append("</div>");
		        sb.append("</div>");
		        
		        sb.append("<div class='row'>");
		          sb.append("<div class='col-sm-3 col-md-3'>");
		          if(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==NC_HEADQUARTER)
		          {
		        	  sb.append("<label>"+lblTitle+"<span class='required'>*</span></label>"); 
		          }
		          else
		          {
		        	  sb.append("<label>"+lblTitle+"</label>");
		          }
		        		 sb.append("<input type='text' id='designation' name='designation' class='form-control' placeholder='' maxlength='50'>");
		          sb.append("</div>");
				
		          sb.append("<div class=col-sm-6 col-md-6'>");
                  sb.append("<label>"+lblOrga+"/Emp.<span class='required'>*</span></label>");
		          sb.append("<input type='text' id='organization' name='organization' class='form-control' placeholder='' maxlength='50'>");
		         sb.append("</div>");
		       sb.append("</div>");
			
		       
		       sb.append("<div class='row '>");
		        sb.append("<div class='col-sm-3 col-md-3'>");
		    	 sb.append("<label>"+lblContNum+"<span class='required'>*</span>&nbsp;<a href='#' id='iconpophover7' rel='tooltip' data-original-title='Phone number (area code first)'><img src='images/qua-icon.png' width='15' height='15' alt=''></a></label>");
		    	 sb.append("<script type='text/javascript'>$('#iconpophover7').tooltip();</script>");  
		    	 sb.append("<input type='text' id='contactnumber' name='contactnumber' onKeyUp='numericFilter(this)' class='form-control' placeholder='' maxlength='10'>");
		    	 sb.append("</div>");
			
		    	 sb.append("<div class='col-sm-6 col-md-6'>");
		    		sb.append("<label>"+lblEmail+"<span class='required'>*</span></label>");
		    			sb.append("<input type='text' id='email' name='email' class='form-control' placeholder='' maxlength='50'>");
		    	 sb.append("</div>");
		       sb.append("</div>");
		       
		       
		       sb.append(" <div class='row top15 hide'>");
		         sb.append("<div class='col-sm-3 col-md-3'>");
		    	    sb.append("<label>");
		    		 sb.append(lblRecommendLetter);
		    		 sb.append("</label>");
		    		 sb.append("<input type='hidden' id='sbtsource_ref' name='sbtsource_ref' value='0'/>");
		    		 sb.append("<input id='pathOfReferenceFile' name='pathOfReferenceFile' type='file' width='20px;'>");
		    			sb.append("<a href='javascript:void(0)' onclick='clearReferences()>"+lnkClear+"</a>");
		    	 sb.append("</div>");
		    		sb.append("<input type='text' id='pathOfReference'/>");
		    	 sb.append("<div class='col-sm-3 col-md-3' id='removeref' name='removeref' style='display: none;'>");
		    		sb.append("<label>&nbsp;&nbsp;");
		    	    sb.append("</label>");
		    	    sb.append("</div>");
		    	    sb.append("<span id='divRefName'>");
		    	    sb.append("</span>");
	                sb.append("&nbsp;&nbsp;&nbsp;<a href='javascript:void(0)' onclick='removeReferences()'>"+lnkRemo+"</a>&nbsp;&nbsp;&nbsp;");
	                //sb.append(" <a href='#' id='iconpophover7' rel='tooltip' data-original-title='Remove recommendation letter !'><img src='images/qua-icon.png' width='15' height='15' alt=''></a>");
	               
		         sb.append("</div>");
	          sb.append("</div>");
	          
	          String cntTP="show";
		       if(userMaster.getDistrictId()!=null && userMaster.getDistrictId().getDistrictId().equals(7800038)){
		    	   cntTP="hide";
		       }
		       
	          sb.append(" <div class='row top10 "+cntTP+"'>");
	        	sb.append("<div class='col-sm-12 col-md-12'>");
	        	 sb.append("<label>"+lblPrsnDirectlyContByHiringAuth+"</label>");
	        	  sb.append("<div class='' id='' style='height: 40px;'>");
	        		  sb.append("<div class='radio inline col-sm-1 col-md-1'>");
	        		  sb.append("<input type='radio' checked='checked' id='rdcontacted1' value='1'  name='rdcontacted'> Yes");
	        		  sb.append("</div></br>");
					    sb.append("<div class='radio inline col-sm-1 col-md-1' style='margin-left:20px;margin-top:-10px;'>");
					    sb.append("<input type='radio' id='rdcontacted0' value='0' name='rdcontacted'> No");
					  sb.append(" </div>");
					sb.append("  </div>");
				sb.append("</div>");
			sb.append("</div>");
			
			String hlhyk="hide";
		       if(userMaster.getDistrictId()!=null && userMaster.getDistrictId().getDistrictId().equals(4218990)){
		    	   hlhyk="show";
		       }
			sb.append(" <div class='row top10 "+hlhyk+"'>");
       	 	sb.append("<div class='col-sm-5 col-md-5'>");
	        sb.append("<label>"+qnhowlongknwperson+"</label><input type='text' id='longHaveYouKnow' name='longHaveYouKnow' class='form-control' placeholder=''>");
	        sb.append("</div>");
	        sb.append("</div>");
	        
		 sb.append(" <div class='row'>");
        	 sb.append("<div class='col-sm-12 col-md-12'>");
	        	sb.append("<label>"+lblReferenceDetails+"</label>");
	        	sb.append("<div id='referenceDetailstext'>");
	        	  sb.append("<textarea id='referenceDetails' name='referenceDetails' ></textarea>");
        	     sb.append("</div>");
        	 sb.append("</div>");
         sb.append("</div>");
			
       sb.append("<div class='row top5'>");
    	sb.append("<div class='col-sm-3 col-md-3 idone'>");
    		sb.append("<a href='#' style='cursor: pointer; text-decoration:none;' onclick='return insertOrUpdateElectronicReferencesM(0)'>"+lnkImD+"</a>&nbsp;");
    		sb.append(" &nbsp;&nbsp;<a href='#' style='cursor: pointer; text-decoration:none;' onclick='return hideElectronicReferencesForm()'>");
    		sb.append(lnkCancel);
    		 sb.append("</a>");
    		sb.append("</div>");
    	sb.append("</div>");
											
		    sb.append("</form>");
		
		    sb.append("</div>");
		    sb.append("</div>");
		       
		// END RRREFERENCESRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
		   sb.append("</td></tr>");
		}
		if(userMaster.getDistrictId()!=null && userMaster.getDistrictId().getDistrictId()==3904374)
		{
			sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+";padding-left: 0px;'>");
			sb.append("<div class=''>");
			sb.append("<div class='col-sm-6 col-md-6'>Background Check</div>");				  
			sb.append("</div></td></tr>");
			sb.append("<tr><td colspan=5 width='100%'>");
			sb.append("<table width='100%'>");
			sb.append("<tr>");
			sb.append("<td style="+sGridtableDisplayCSS+"><div id='gridDataBackgroundCheck' class=''></div>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("</table>");
			sb.append("</td></tr>");
			sb.append("<script>getBackgroundCheckGrid_DivProfile("+teacherDetail.getTeacherId()+");</script>");
		}		
		if(!headQuarterMaster ||(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==NC_HEADQUARTER))
		if(districtPortfolioConfig==null || (districtPortfolioConfig.getVideoLink()!=null && districtPortfolioConfig.getVideoLink()) )
		{
			// Start Video Grid
			if(sVisitLocation.equalsIgnoreCase("Mosaic")){
				//sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'>"+Utility.getLocaleValuePropByKey("lnkViLnk", locale)+"</td></tr>");
				sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+";padding-left: 0px;'>");
				    sb.append("<div class='col-sm-6 col-md-6'>"+Utility.getLocaleValuePropByKey("lnkViLnk", locale)+"</div>");
				    sb.append("<div class=''>");
			    	  if(userMaster.getRoleId().getRoleId().equals(2) || userMaster.getRoleId().getRoleId().equals(3))
			    	  {
			    		  sb.append("<div class='col-sm-6 col-md-6' style='text-align: right;' ><a href='#' onclick='return showVideoLinksForm();'>"+lnkAddViLnk+"</a></div>");
				    	  sb.append("<tr><td style='padding-left: 15px;'>");
				    	  sb.append("<label class ='top10'><strong>"+lblPleaseInclude+"</strong></label>");
				    	  sb.append("</td></tr>");
			    	  }			      
			    sb.append("</div></td></tr>");
			
			    //TestTool.getTraceTimeTPAP("TPAP 217");
			// Start Video Grid
			//if(sVisitLocation.equalsIgnoreCase("Mosaic")){
				//sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'>"+Utility.getLocaleValuePropByKey("lnkViLnk", locale)+"</td></tr>");
			}else if(sVisitLocation.equalsIgnoreCase("My Folders")){
				//sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'><b>"+Utility.getLocaleValuePropByKey("lnkViLnk", locale)+"</b></td></tr>");

				sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+";padding-left: 0px;'>");
				     sb.append("<div class='col-sm-6 col-md-6'>"+Utility.getLocaleValuePropByKey("lnkViLnk", locale)+"</div>");
				     sb.append("<div class=''>");
			    	  if(userMaster.getRoleId().getRoleId().equals(2) || userMaster.getRoleId().getRoleId().equals(3))
			    	  {
			    		  sb.append("<div class='col-sm-6 col-md-6' style='text-align: right; margin-left:-12px;' ><a href='#' onclick='return showVideoLinksForm();'>"+lnkAddViLnk+"</a></div>");
				    	  sb.append("<tr><td style='padding-left: 15px;'>");
				    	  sb.append("<label class ='top10'><strong>"+lblPleaseInclude+"</strong></label>");
				    	  sb.append("</td></tr>");  
			    	  }
			    sb.append("</div></td></tr>");
			
			//	sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'><b>"+Utility.getLocaleValuePropByKey("lnkViLnk", locale)+"</b></td></tr>");
			}else{
				//sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'><b>"+Utility.getLocaleValuePropByKey("lnkViLnk", locale)+"</b></td></tr>");
				
				sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+";padding-left: 0px;'>");
				   sb.append("<div class=''>");
				    sb.append("<div class='col-sm-6 col-md-6'>"+Utility.getLocaleValuePropByKey("lnkViLnk", locale)+"</div>");
			      
			    	  if(userMaster.getRoleId().getRoleId().equals(2) || userMaster.getRoleId().getRoleId().equals(3))
			    	  {
			    		  sb.append("<div class='col-sm-6 col-md-6' style='text-align: right; margin-left:-12px;' ><a href='#' onclick='return showVideoLinksForm();'>"+lnkAddViLnk+"</a></div>");
				    	  sb.append("<tr><td style='padding-left: 15px;'>");
				    	  sb.append("<label class ='top10'><strong>"+lblPleaseInclude+"</strong></label>");
				    	  sb.append("</div></td></tr>");
			    	  }
			      
			    sb.append("</td></tr>");
				//sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'><b>"+Utility.getLocaleValuePropByKey("lnkViLnk", locale)+"</b></td></tr>");
			}
			sb.append("<tr><th colspan=5 width='100%'>");
			sb.append("<table border=0 width='100%' onmouseover=\"javascript:setGridProfileVariable('videoLink')\">");
					sb.append("<tr>");
					//System.out.println(" ooooooooooooooooooooooooo ");
						sb.append("<th style="+sGridtableDisplayCSS+" id='gridDataVideoLink'>");
						//sb.append("<th style="+sGridtableDisplayCSS+"><div id='gridDataVideoLink' class=''></div>");
						sb.append("</th>");
					sb.append("</tr>");
				sb.append("</table>");
			sb.append("</th></tr>");
			sb.append("<script>getVideoLinksGrid_DivProfile("+teacherDetail.getTeacherId()+");</script>");
			// End Video Grid
			
			sb.append("<iframe id='uploadFrameVideoID' name='uploadFrameVideo' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'></iframe>");
sb.append("<tr><td	style='padding-left: 15px;'>");
			sb.append("<div class='top15' style='display: none' id='divvideoLinks' onkeypress='return checkForEnter(event);'>");
			 sb.append("<div class='divErrorMsg' id='errordivvideoLinks' style='display: block;'></div>");
			 sb.append("<form class='' id='frmvideoLinks' name='frmvideoLinks' onsubmit='return false;' method='post'  enctype='multipart/form-data' target='uploadFrameVideo' action='certificationVideoUploadServlet.do?teacherIdForVideo="+teacherDetail.getTeacherId()+"'>");
				 sb.append("<div class='row'>");
					 sb.append("<div class='col-sm-10 col-md-10' style='max-width: 800px;'>");
						 sb.append("<label>Video Link</label>");
						 sb.append("<input type='hidden' id='videolinkAutoId' name='videolinkAutoId'>");
						 sb.append("<input type='text' id='videourl' name='videourl' class='form-control' placeholder='' maxlength='200'>");
					 sb.append("</div>");
				 sb.append("</div>");
				 
				 sb.append("<div class='row'>");
				 sb.append("<div class='col-sm-5 col-md-5' style='max-width: 800px;'><br/>");
					 sb.append("<label>Video</label>");
					 sb.append("<input type='file' id='videofile' name='videofile'>");
					 sb.append("<span style='display: none; visibility: hidden;' id='video'></span>");
				 sb.append("</div>");
				 sb.append("<span class='col-sm-6 col-md-6'> <br/><br/>");
				 sb.append(lblSupportedvideos);
				 sb.append("</span>");
			 sb.append("</div>");
					
				 sb.append("<div class='row top10'>");
				     sb.append("<div class='col-sm-4 col-md-4 idone'>");
				    	 sb.append("<a href='javascript:void(0)' onclick='insertOrUpdatevideoLinks();'>"+lnkImD+"</a>&nbsp;&nbsp;&nbsp;");
				    	 sb.append("<a href='#'	onclick='return hideVideoLinksForm()'>Cancel");
						 sb.append("</a>");
				     sb.append("</div>");
				 sb.append("</div>");
			 sb.append("</form>");
		    sb.append("</div>");
	      sb.append("</div>");
	 sb.append("</td></tr>");	
	}
	
	if(!headQuarterMaster ||(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==NC_HEADQUARTER))	
	if(districtPortfolioConfig==null || (districtPortfolioConfig.getAdditionalDocuments()!=null && districtPortfolioConfig.getAdditionalDocuments()) )
	{
			//TestTool.getTraceTimeTPAP("TPAP 218");
			// Start Additional Document
			if(sVisitLocation.equalsIgnoreCase("Mosaic")){
				sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'>"+lnkAddDocu+"</td></tr>");
			}else if(sVisitLocation.equalsIgnoreCase("My Folders")){
				sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'><b>"+lnkAddDocu+"</b></td></tr>");
			}else{
				sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'><b>"+lnkAddDocu+"</b></td></tr>");
			}
			sb.append("<tr><th colspan=5 width='100%'>");
			sb.append("<table border=0 width='100%' onmouseover=\"javascript:setGridProfileVariable('document')\">");
					sb.append("<tr>");
						sb.append("<th style="+sGridtableDisplayCSS+" id='divGridAdditionalDocuments'>");
						sb.append("</th>");
					sb.append("</tr>");
				sb.append("</table>");
			sb.append("</th></tr>");
			sb.append("<script>getadddocumentGrid_DivProfile("+teacherDetail.getTeacherId()+");</script>");
	}	
			if(userMaster.getDistrictId()!=null && (userMaster.getDistrictId().getDistrictId().equals(4218990)|| userMaster.getDistrictId().getDistrictId().equals(7800040))){
			//Start language profiency
			sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'>"+lblLanguageProfiency+"</td></tr>");			
			sb.append("<table width='100%' onmouseover=\"javascript:setGridProfileVariable('languageProfiency')\">");
			sb.append("<tr>");
			sb.append("<td style="+sGridtableDisplayCSS+"><div id='divGridlanguageProfiency' class=''></div>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("</table>");
			sb.append("<script>getlanguage_DivProfile("+teacherDetail.getTeacherId()+");</script>");
		
			//end language profiency
			}
			if(districtMaster!=null && districtMaster.getDistrictId().equals(4218990))
			if(getDistrictAssessmentFlag(teacherDetail))
			{
				// Start Assessment Details
				if(sVisitLocation.equalsIgnoreCase("Mosaic")){
					sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'>"+lblAssessmentDetails+"</td></tr>");
				}else if(sVisitLocation.equalsIgnoreCase("My Folders")){
					sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'><b>"+lblAssessmentDetails+"</b></td></tr>");
				}else{
					sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'><b>"+lblAssessmentDetails+"</b></td></tr>");
				}
				sb.append("<tr><th colspan=5 width='100%'>");
				sb.append("<table border=0 width='100%' onmouseover=\"javascript:setGridProfileVariable('assessmentDetails')\">");
						sb.append("<tr>");
							sb.append("<th style="+sGridtableDisplayCSS+" id='divGridAssessmentDetails'>");
							sb.append("</th>");
						sb.append("</tr>");
					sb.append("</table>");
				sb.append("</th></tr>");
				sb.append("<script>getdistrictAssessmetGrid_DivProfile("+teacherDetail.getTeacherId()+");</script>");
				// End Assessment Details
			}
			
			/*
             ************************************************************
             				Start Transcript Verification
             ************************************************************ 
             * */
			
			if(userMaster!=null && userMaster.getDistrictId()!=null && userMaster.getDistrictId().getDistrictId().equals(1200390)){
            String candidateFname 	 = "";
            String candidateLname 	 = "";
            String candidateFullName = "";
            if(teacherDetail!=null){
            	candidateFname = teacherDetail.getFirstName()==null?"":teacherDetail.getFirstName();
            	candidateLname = teacherDetail.getLastName()==null?"":teacherDetail.getLastName();
            	candidateFullName 	= 	candidateFname+" "+candidateLname;
            	candidateFullName	=	candidateFullName.replace("'","\\'");
            }
            
            sb.append("<table>");
            	sb.append("<tr>");
            	sb.append("<td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'>Transcript Verification : </td>");
            	sb.append("<td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'><a href='javascript:void:(0);' onclick=\"return verifyTranscript('"+teacherId+"','"+candidateFullName+"')\">Transcript Verification<a></td>");
            	sb.append("</tr>");
            sb.append("</table>");
			}
            /*
             ************************************************************
             				End Transcript Verification
             ************************************************************ 
             * */
			
            String pfcontent = candidateGridSubAjax.getDistrictSpecificPortfolioQuestions(0,teacherId);
            if(pfcontent!=null && !pfcontent.equals("")){
                            sb.append("<table><tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'>"+LBLDistrictSpecificQuestion+"</td></tr></table>");
                            if(districtMaster!=null && districtMaster.getDistrictId().equals(7800038))
                                sb.append("<div class='row '>");
                                else
                             //sb.append("<div class='row' >");
                            sb.append("<div class='row ' style='pointer-events: none;'>");
                            sb.append("<div class='col-sm-12 col-md-12'><div id='errordivspecificquestion' class='required' style='margin-left:10px;'></div><table style='margin-left:10px;' width='90%'>"+pfcontent+"</table>");
                            sb.append("</div></div>");
                            if(userMaster.getDistrictId()!=null && userMaster.getDistrictId().getDistrictId().equals(7800038))
                            if(userMaster.getRoleId().getRoleId().equals(2)){
                                            sb.append("<div class='col-sm-12 col-md-12'><button class='btn btn-primary' type='button' onclick=\"javascript:saveDistrictSpecificQues('"+teacherId+"')\">"+lblSaveQuestion+"</button></div>");
                            }
            }

		//End district specific question
			//Start Div Footer
			//End Div Footer
			
			sb.append("</table>");
			
			
//			sb.append("</div>");
//			sb.append("<DIV>");
//			sb.append("<table border=0 width='100%'><tr bgcolor='#F2FAEF' class='custom-div-border'><th style='text-align:center;vertical-align:middle;padding-top:10px;padding-bottom:10px;'width='35%'><a href='javascript:void(0);' onclick=\"getCommunicationsDiv(1,0,'"+teacherDetail.getTeacherId()+"',0);\" ><span class='icon-dropbox icon-large iconcolor'></span>&nbsp;Communications</a></th>");
//			if(userMaster.getEntityType()==1)
//			{
//				sb.append("<th style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><span class='icon-save icon-large iconcolorhover'></span>&nbsp;Save</th>" +
//						"<th style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><span class='icon-share icon-large iconcolorhover'></span>&nbsp;Share</th>"+
//						"<th style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><span class='icon-tag icon-large iconcolorhover'></span></span>&nbsp;Tag</th>");
//			}
//			else
//			{
//				
//				int flagpopover=1;
//				sb.append("<th style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><a href='javascript:void(0);' id='saveaction"+noOfRecordCheck+"' onclick=\"saveToFolderJFTNULL('0','"+teacherDetail.getTeacherId()+"',"+flagpopover+")\"><span class='icon-save icon-large iconcolor'></span>&nbsp;Save</a></th>" +
//						"<th style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><a href='javascript:void(0);' id='shareaction"+noOfRecordCheck+"' onclick=\"displayUsergrid('"+teacherDetail.getTeacherId()+"',"+flagpopover+")\"><span class='icon-share icon-large iconcolor'></span>&nbsp;Share</a></th>");
//				//sb.append("<th style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><span class='icon-tag icon-large iconcolor'></span></th>");
//				
//				//sb.append("<th style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><a href='javascript:void(0);' id='tagaction' onclick=\"closeProfileOpenTags("+noOfRecordCheck+")\"> <span class='icon-tag icon-large iconcolor'></span></span>&nbsp;Tag</a></th>");
//				
//				if(userMaster.getRoleId().getRoleId()==1 || userMaster.getRoleId().getRoleId()==3){
//					sb.append("<th style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><span class='icon-tag icon-large iconcolorhover'></span></span>&nbsp;Tag</th>");
//				}
//				else{
//					if(sVisitLocation.equalsIgnoreCase("Teacher Pool"))
//						sb.append("<th style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><a href='javascript:void(0);' id='tagaction' onclick=\"closeProfileOpenTags("+teacherId+","+noOfRecordCheck+")\"> <span class='icon-tag icon-large iconcolor'></span></span>&nbsp;Tag</a></th>");
//					else
//						sb.append("<th style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><span class='icon-tag icon-large iconcolorhover'></span></span>&nbsp;Tag</th>");
//				}
//			}
//			sb.append("</tr>");
//			sb.append("</table>");
//			sb.append("</DIV>");	
//			/*sb.append("<div class='modal-footer'>"+
//			 		"<button class='btn' onclick='showProfileContentClose();'>Close</button>"+ 		
//			 	"</div>"+
//			"</div>");*/
//			sb.append("</div>");
			
			}//added
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		finally
		{
			//TestTool.getTraceTimeTPAP("TPAP 219");
			sb.append("<script>$('textarea').jqte();$('.tooltipEmpNo').tooltip();</script>");
			if(fg!=0)
				return sb.toString().split("####")[1];
			else
				return sb.toString();
		}
	//}
	//catch(Exception e)
	//{e.printStackTrace();}
	}
	
	
	public static boolean isReferenceNote(List<ReferenceNote> referenceNotesList,Integer eleReferenceId){
		for(ReferenceNote referenceNote:referenceNotesList)
			if(referenceNote.getElectronicReferences().getElerefAutoId().equals(eleReferenceId))
				return true;
		return false;
	}
	
	//@Transactional(readOnly=true)
	public String getRefeNotes(String eleReferenceId,String jobId,DistrictMaster districtMaster_Input,SchoolMaster schoolMaster_Input)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		DistrictMaster districtMaster=null;
		SchoolMaster schoolMaster=null;
		UserMaster userMaster=null;
		int entityID=0,roleId=0;
		if (session == null || session.getAttribute("userMaster") == null) {
			//return "false";
		}else{
			userMaster=(UserMaster)session.getAttribute("userMaster");
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
			if(userMaster.getDistrictId()!=null){
				districtMaster=userMaster.getDistrictId();
			}
			if(userMaster.getSchoolId()!=null){
				schoolMaster =userMaster.getSchoolId();
			}	
			entityID=userMaster.getEntityType();
		}
		
		StringBuffer sb = new StringBuffer();		
		try 
		{
			
			String windowFunc="if(this.href!='javascript:void(0);')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
			sb.append("<table border='0' width='460' class='tablecgtbl'>");
			
				TeacherElectronicReferences electronicReferences=teacherElectronicReferencesDAO.findById(new Integer(eleReferenceId), false, false);
				//TeacherDetail teacherDetail = teacherDetailDAO.findById(new Integer(teacherId), false, false);
				JobOrder   jobOrder =null;
				if(!jobId.equals("0") && !jobId.equals("")){
					jobOrder=jobOrderDAO.findById(Integer.parseInt(jobId), false, false);
					/*if(entityID!=1){
						sb.append("<tr>"); 
						sb.append("<td style='text-align:right;'><a data-original-title='Add Notes on the Candidate Reference' rel='tooltip' id='refeNotesId' href='javascript:void(0);' onclick=\"getNotesEditorDiv()\" ><img src=\"images/note_com.png\"></a></td>");
						sb.append("</tr>");
					}*/
				}
				
				if(entityID!=1){
					sb.append("<tr>"); 
					sb.append("<td style='text-align:right;'><a data-original-title='"+LBLCandidateRefe+"' rel='tooltip' id='refeNotesId' href='javascript:void(0);' onclick=\"getNotesEditorDiv()\" ><img src=\"images/note_com.png\"></a></td>");
					sb.append("</tr>");
				}
				
				/*List<ReferenceNote> referenceNotesList = new ArrayList<ReferenceNote>();
				Criterion criterion11 = Restrictions.eq("electronicReferences",electronicReferences);
				Criterion criterion13 = Restrictions.eq("jobId",jobOrder);
				
				if(!jobId.equals("0") && !jobId.equals(""))
					referenceNotesList = referenceNoteDAO.findByCriteria(criterion11,criterion13);
				else
					referenceNotesList = referenceNoteDAO.findByCriteria(criterion11);*/
				
				///////////////
				
				List<ReferenceNote> referenceNotesList = new ArrayList<ReferenceNote>();
				Criterion criterion_refId = Restrictions.eq("electronicReferences",electronicReferences);
				Criterion criterion_districtId = Restrictions.eq("districtMaster",districtMaster);
				//Criterion criterion_schoolId = Restrictions.eq("schoolId",schoolMaster);
				
				Order sortOrder=Order.desc("noteCreatedDate");

				/*if(jobId!=null && !jobId.equals("0") && !jobId.equals("") && jobOrder!=null){
					Criterion criterion_job = Restrictions.eq("jobId", jobOrder);
					if(entityID==1){ // For TM
						referenceNotesList=referenceNoteDAO.findByCriteria(criterion_refId,criterion_job);
					}
					else // For District
						referenceNotesList=referenceNoteDAO.findByCriteria(criterion_refId,criterion_job,criterion_districtId);
					
					else if(entityID==3){// For School
						referenceNotesList=referenceNoteDAO.findByCriteria(criterion_refId,criterion_job,criterion_schoolId,criterion_districtId);	
					}
				}
				else
				{*/
					if(entityID==1){ // For TM
						Criterion criterion_districtId_input = Restrictions.eq("districtMaster",districtMaster_Input);
						if(districtMaster_Input==null)
							referenceNotesList=referenceNoteDAO.findByCriteria(sortOrder,criterion_refId);
						else
							referenceNotesList=referenceNoteDAO.findByCriteria(sortOrder,criterion_refId,criterion_districtId_input);
					
						/*else if(schoolMaster_Input!=null && districtMaster_Input!=null)
						{
							referenceNotesList=referenceNoteDAO.findByCriteria(criterion_refId,criterion_schoolId_input,criterion_districtId_input);	
						}*/
					}
					else // For District
						referenceNotesList=referenceNoteDAO.findByCriteria(sortOrder,criterion_refId,criterion_districtId);
					
					/*else if(entityID==3){// For School
						referenceNotesList=referenceNoteDAO.findByCriteria(criterion_refId,criterion_schoolId,criterion_districtId);
						//System.out.println("entityID "+entityID+ " District Id "+districtMaster.getDistrictId()+" School ID "+schoolMaster.getSchoolId());
					}*/
				//}
				//////////////
				
				
				
				//Collections.sort(referenceNotesList,CommunicationsDetail.createdDate);
				if(referenceNotesList.size()>0){
				sb.append("<tr><td>");
					sb.append("<table border='0' style='padding:0px;' cellpadding=0 cellspacing=0  width='100%' class='tablecgtbl'>");
					
					String submitedBy = "";
					int counter=0;
					String year="";
					String preYear="";
					if(referenceNotesList !=null)
						for(ReferenceNote referenceNote : referenceNotesList){
							year= convertUSformatOnlyYear(referenceNote.getNoteCreatedDate());
							counter++;
							if(!preYear.equals(year)){
								sb.append("<tr>");
								sb.append("<td colspan=3 style='text-align:right;padding-right:8px;padding-bottom:5px;verical-align:bottom;'><span class=\"txtyearbg\">"+year+"</span></td><td>&nbsp;</td>");
								sb.append("<tr>");
								sb.append("<tr>");
								sb.append("<td width='15%' style='height:10px;'>&nbsp;</td>");
								sb.append("<td style='text-align:right;vertical-align:middle;padding-left:12px;'  height=10>");
								sb.append("<span class=\"vertical-line\"></span></td><td ></td>");
								sb.append("</tr>");
							}
							preYear=year;
							sb.append("<tr>");
							sb.append("<td width='15%' style='text-align:right;vertical-align:middle;position:relative;'>"+convertUSformatOnlyMonthDate(referenceNote.getNoteCreatedDate()));
								sb.append("<span class='note-com'><img src=\"images/note_com.png\"></span>");
							
							sb.append("</td><td style='text-align:right;vertical-align:middle;padding-left:12px;' width='8px;' height='10'>");
							sb.append("<span class=\"vertical-line\"></span></td>");
							sb.append("<td  style='text-align:right;vertical-align:middle;' width=20>");;
							sb.append("<hr style=\"color: #f00; background:#007AB4; width: 100%; height: 3px;\"/></td>");
							sb.append("<td class='commbgcolor'  style='padding:5px;vertical-align:middle;'>");
							sb.append("<span class='com-user'>From "+referenceNote.getUserMaster().getFirstName()+" "+referenceNote.getUserMaster().getLastName()+"");
							
							if(referenceNote.getJobId()!=null && referenceNote.getJobId().getJobTitle()!=null){
								sb.append(" regarding "+referenceNote.getJobId().getJobTitle()+"</span><br/><br/>");
							}else{
								sb.append("</span><br/><br/>");
							}
							sb.append(referenceNote.getRefNoteDescription()+"&nbsp;&nbsp;&nbsp;");
							
							if(referenceNote.getRefNoteAttachment()!=null){
								String filePath=null;
								String fileName=null;
								fileName=referenceNote.getRefNoteAttachment();
								filePath="ref_note/"+referenceNote.getElectronicReferences().getElerefAutoId();
								
								sb.append("<a href='javascript:void(0);' id='refNotesfileopen"+counter+"' onclick=\"downloadReferenceNotes('"+filePath+"','"+fileName+"','refNotesfileopen"+counter+"');"+windowFunc+"\"><span class='icon-paper-clip icon-large iconcolor'></span></a>");
							}
							sb.append("</td>");
							sb.append("</tr>");
							if(counter < referenceNotesList.size()){
								sb.append("<tr>");
								sb.append("<td width='15%' style='height:20px;'>&nbsp;</td>");
								sb.append("<td style='text-align:right;vertical-align:middle;padding-left:12px;'  height=20>");
								sb.append("<span class=\"vertical-line\"></span></td><td ></td>");
								sb.append("</tr>");
							}
							
					}
					sb.append("</td></tr>");
				}else{
					sb.append("<tr><td>"+lblNoRecord+"</td></tr>");
				}
			sb.append("</table>");

		} catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	}
	
	public static String convertUSformatOnlyYear(Date dat)
	{
		try {
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy");
			return sdf.format(dat);
		} catch (Exception e) {
			//e.printStackTrace();
			return "";
		}
	}

	
	//@Transactional(readOnly=false)
	public String saveReferenceNotesByAjax(String refId,String teacherId,String jobId,String refnote,String fileName)
	{	
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		DistrictMaster districtMaster=null;
		SchoolMaster schoolMaster=null;
		UserMaster userMaster=null;
		int entityID=0,roleId=0;
		if (session == null || session.getAttribute("userMaster") == null) {
			//return "false";
		}else{
			userMaster=(UserMaster)session.getAttribute("userMaster");
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
			if(userMaster.getDistrictId()!=null){
				districtMaster=userMaster.getDistrictId();
			}
			if(userMaster.getSchoolId()!=null){
				schoolMaster =userMaster.getSchoolId();
			}	
			entityID=userMaster.getEntityType();
		}
		

			TeacherDetail teacherDetail = teacherDetailDAO.findById(new Integer(teacherId), false, false);
			TeacherElectronicReferences teacherElectronicReferences=teacherElectronicReferencesDAO.findById(new Integer(refId), false, false);
			
			JobOrder   jobOrder =null;
			if(!jobId.equals("0") && !jobId.equals(""))
				jobOrder=jobOrderDAO.findById(new Integer(jobId), false, false);
			
			if(jobOrder!=null)
				districtMaster=jobOrder.getDistrictMaster();
				
			try {
				ReferenceNote referenceNote=new ReferenceNote();
				
				referenceNote.setElectronicReferences(teacherElectronicReferences);
				referenceNote.setTeacherDetail(teacherDetail);
				
				if(jobOrder!=null)
					referenceNote.setJobId(jobOrder);
				
				referenceNote.setRefNoteDescription(refnote);
				
				if(fileName!=null && !fileName.equalsIgnoreCase(""))
					referenceNote.setRefNoteAttachment(fileName);
				
				referenceNote.setDistrictMaster(districtMaster);
				referenceNote.setSchoolId(schoolMaster);
				referenceNote.setUserMaster(userMaster);
				referenceNote.setNoteCreatedDate(new Date());
				referenceNoteDAO.makePersistent(referenceNote);
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		return "saveRefNotes";
	}

	
	public static String convertUSformatOnlyMonthDate(Date dat)
	{
		try {
			SimpleDateFormat sdf=new SimpleDateFormat("MMMMM dd");
			return sdf.format(dat);
		} catch (Exception e) {
			//e.printStackTrace();
			return "";
		}
	}

	public String downloadReferenceNotes(String filePath,String fileName)
	{
		
		System.out.println("Calling DWR downloadReferenceNotes ....");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		String path="";

		try 
		{
			String source = Utility.getValueOfPropByKey("referenceNotesRootPath")+filePath+"/"+fileName;
			String target = context.getServletContext().getRealPath("/")+"/referenceNotes/"+filePath+"/";
			
			System.out.println("Test "+source+" :: "+target);
			
			File sourceFile = new File(source);
			File targetDir = new File(target);
			if(!targetDir.exists())
				targetDir.mkdirs();

			File targetFile = new File(targetDir+"/"+sourceFile.getName());
			FileUtils.copyFile(sourceFile, targetFile);
			path = Utility.getValueOfPropByKey("contextBasePath")+"/referenceNotes/"+filePath+"/"+sourceFile.getName();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			path="";
		}

		return path;
	}
	
	public String getDistrictSpecificQuestionGrid(Integer jobId,Integer teacherId,String sVisitLocation)
	{
		//System.out.println(" inside getDistrictSpecificQuestionGrid..........");
		
		StringBuffer dq=new StringBuffer();
		try{
			JobOrder jobOrder=jobOrderDAO.findById(jobId, false, false);
			TeacherDetail teacherDetail=teacherDetailDAO.findById(teacherId, false, false);
			List<TeacherAnswerDetailsForDistrictSpecificQuestions>list=new ArrayList<TeacherAnswerDetailsForDistrictSpecificQuestions>();
			
			list=teacherAnswerDetailsForDistrictSpecificQuestionsDAO.findTeacherAnswersByDistrictAndJob(teacherDetail,jobOrder);
			int qno=1;
			
			if(list!=null && list.size()>0 ){
				for(TeacherAnswerDetailsForDistrictSpecificQuestions dsq:list){
					dq.append("<tr><td style='font-size: 11px;font-weight: normal; line-height: 16px;'><b>Q"+qno+".&nbsp;</b>"+dsq.getQuestion()+"</td></tr>");
					if((dsq.getSelectedOptions()!=null && !dsq.getSelectedOptions().equals("")) ||(dsq.getQuestionOption()!=null && !dsq.getQuestionOption().equals("")))
					{	
						dq.append("<tr><td style='font-size: 11px;font-weight: normal; line-height: 16px;'><b>Ans.&nbsp;</b>"+dsq.getQuestionOption());
						if(dsq.getQuestionTypeMaster().getQuestionTypeId()==8 && !dsq.getQuestionOption().equals("") && !dsq.getInsertedText().equals("")){
							//dq.append("<br><a href='javascript:void(0);' onclick=\"getQuestionExplain(\'"+dsq.getAnswerId()+"\');\"><span style='font-size: 11px;'>Explaination</span></a>&nbsp;");
							//dq.append("<a href='#' id='iconpophoverQE' rel='tooltip' data-original-title='Click here to view the explaination'><img src='images/qua-icon.png' width='15' height='15' alt=''></a><script>$('#iconpophoverQE').tooltip();</script></td></tr>");
							dq.append("</td></tr><tr><td style='font-size: 11px;font-weight: normal; line-height: 16px;'><b>"+msgExplaination+".&nbsp;</b>"+dsq.getInsertedText()+"</td></tr>");	
						}else{
							dq.append("</td></tr>");
						}
						
						
					}else if(dsq.getInsertedText()!=null && !dsq.getInsertedText().equals(""))
					{	
						dq.append("<tr><td style='font-size: 11px;font-weight: normal; line-height: 16px;'><b>Ans.&nbsp;</b>"+dsq.getInsertedText()+"</td></tr>");
					}
					if(qno<=list.size()-1)
					{
						dq.append("<tr><td style='line-height:4px;'>&nbsp;</td></tr>"); // Ashish :: change line-height 6 to 4
					}	
					qno++;
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return dq.toString();
	}
	
	public String getDistrictSpecificQuestionGrid_ByDistrict(Integer districtId,Integer teacherId,String sVisitLocation)
	{	
		//System.out.println(" inside getDistrictSpecificQuestionGrid_ByDistrict..........");
		
		StringBuffer dq=new StringBuffer();
		try{
			DistrictMaster districtMaster=districtMasterDAO.findById(districtId, false, false);
			TeacherDetail teacherDetail=teacherDetailDAO.findById(teacherId, false, false);
			List<TeacherAnswerDetailsForDistrictSpecificQuestions>list=new ArrayList<TeacherAnswerDetailsForDistrictSpecificQuestions>();
			list=teacherAnswerDetailsForDistrictSpecificQuestionsDAO.getDistrictSpecificQuestionBYDistrict(districtMaster,teacherDetail);
			int qno=1;
			
			if(list!=null && list.size()>0 ){
				if(sVisitLocation.equals("Mosaic")){
					for(TeacherAnswerDetailsForDistrictSpecificQuestions dsq:list){
						dq.append("<tr><td style='font-size: 11px;font-weight: normal;'><b>Q"+qno+".&nbsp;</b>"+dsq.getQuestion()+"</td></tr>");
						if((dsq.getSelectedOptions()!=null && !dsq.getSelectedOptions().equals("")) ||(dsq.getQuestionOption()!=null && !dsq.getQuestionOption().equals("")))
						{	
							dq.append("<tr><td style='font-size: 11px;font-weight: normal;'><b>Ans.&nbsp;</b>"+dsq.getQuestionOption());
							if(dsq.getQuestionTypeMaster().getQuestionTypeId()==8 && !dsq.getQuestionOption().equals("") && !dsq.getInsertedText().equals("")){
								//dq.append("<BR><a href='javascript:void(0);' onclick=\"getQuestionExplain(\'"+dsq.getAnswerId()+"\');\"><span style='font-size: 11px;'>Explaination</span></a>&nbsp;");
								//dq.append("<a href='#' id='iconpophoverQE' rel='tooltip' data-original-title='Click here to view the explaination'><img src='images/qua-icon.png' width='15' height='15' alt=''></a><script>$('#iconpophoverQE').tooltip();</script></td></tr>");
								dq.append("</td></tr><tr><td style='font-size: 11px;font-weight: normal; line-height: 16px;'><b>"+msgExplaination+".&nbsp;</b>"+dsq.getInsertedText()+"</td></tr>");
							}else{
								dq.append("</td></tr>");
							}
						}else if(dsq.getInsertedText()!=null && !dsq.getInsertedText().equals(""))
						{	
							dq.append("<tr><td style='font-size: 11px;font-weight: normal;'><b>Ans.&nbsp;</b>"+dsq.getInsertedText()+"</td></tr>");
						}
						if(qno<=list.size()-1)
						{
							dq.append("<tr><td style='line-height:8px;'>&nbsp;</td></tr>");
						}	
						qno++;
					}
				}else if(sVisitLocation.equals("My Folders")){
					
					for(TeacherAnswerDetailsForDistrictSpecificQuestions dsq:list){
							dq.append("<table><tr><td style='font-size: 11px;'><b>Q"+qno+".&nbsp;</b>"+dsq.getQuestion()+"</td></tr>");
						if((dsq.getSelectedOptions()!=null && (!dsq.getSelectedOptions().equals("")) ||(dsq.getQuestionOption()!=null) && !dsq.getQuestionOption().equals(""))){
							dq.append("<tr><td style='font-size: 11px;'><b>Ans.&nbsp;</b>"+dsq.getQuestionOption());
							if(dsq.getQuestionTypeMaster().getQuestionTypeId()==8 && !dsq.getQuestionOption().equals("") && !dsq.getInsertedText().equals("")){
								/*dq.append("<br><a href='javascript:void(0);' onclick=\"getQuestionExplain(\'"+dsq.getAnswerId()+"\');\"><span style='font-size: 11px;'>Explaination</span></a>&nbsp;");
								dq.append("<a href='#' id='iconpophoverQE' rel='tooltip' data-original-title='Click here to view the explaination'>" +
								"<img src='images/qua-icon.png' width='15' height='15' alt=''></a><script>$('#iconpophoverQE').tooltip();</script>" +
								"</td></tr>");*/
								dq.append("</td></tr><tr><td style='font-size: 11px;font-weight: normal; line-height: 16px;'><b>"+msgExplaination+".&nbsp;</b>"+dsq.getInsertedText()+"</td></tr>");
							}else{
								dq.append("</td></tr>");
							}
							
						}else if(dsq.getInsertedText()!=null && !dsq.getInsertedText().equals("")){
							dq.append("<tr><td style='font-size: 11px;'><b>Ans.&nbsp;</b>"+dsq.getInsertedText()+"</td></tr>");
						}
						
						if(qno<=list.size()-1){
							dq.append("<tr><td style='line-height:8px;'>&nbsp;</td></tr></table>");
						}	
						qno++;
						
					}
				}else{
					for(TeacherAnswerDetailsForDistrictSpecificQuestions dsq:list){
						dq.append("<tr><td style='font-size: 11px;'><b>Q"+qno+".&nbsp;</b>"+dsq.getQuestion()+"</td></tr>");
					if((dsq.getSelectedOptions()!=null && !dsq.getSelectedOptions().equals("")) ||(dsq.getQuestionOption()!=null && !dsq.getQuestionOption().equals(""))){
						dq.append("<tr><td style='font-size: 11px;'><b>Ans.&nbsp;</b>"+dsq.getQuestionOption());
						if(dsq.getQuestionTypeMaster().getQuestionTypeId()==8 && !dsq.getQuestionOption().equals("") && !dsq.getInsertedText().equals("")){
							//dq.append("<BR><a href='javascript:void(0);' onclick=\"getQuestionExplain(\'"+dsq.getAnswerId()+"\');\"><span style='font-size: 11px;'>Explaination</span></a>&nbsp;");
							//dq.append("<a href='#' id='iconpophoverQE' rel='tooltip' data-original-title='Click here to view the explaination'><img src='images/qua-icon.png' width='15' height='15' alt=''></a><script>$('#iconpophoverQE').tooltip();</script></td></tr>");
							dq.append("</td></tr><tr><td style='font-size: 11px;font-weight: normal; line-height: 16px;'><b>"+msgExplaination+".&nbsp;</b>"+dsq.getInsertedText()+"</td></tr>");
						}else{
							dq.append("</td></tr>");
						}
						
					}else if(dsq.getInsertedText()!=null && !dsq.getInsertedText().equals("")){
						dq.append("<tr><td style='font-size: 11px;'><b>Ans.&nbsp;</b>"+dsq.getInsertedText()+"</td></tr>");
					}
					
					if(qno<=list.size()-1){
						dq.append("<tr><td style='line-height:8px;'>&nbsp;</td></tr>");
					}	
					qno++;
					}
				}
				
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return dq.toString();
	}
	/*public String getQAExplaination(Integer QAId)
	{	
		TeacherAnswerDetailsForDistrictSpecificQuestions tqa=teacherAnswerDetailsForDistrictSpecificQuestionsDAO.findById(QAId, false, false);
		return tqa.getInsertedText();
	}*/

	/*@Start
	 *@Ashish 
	 *@Description :: Get Update and Save Score from Teacher Pool
	 **/
		public String[] getUpdateAndSaveAScoreFromTeacherPool(Integer teacherId,Integer districtId,Integer slider_aca_ach,Integer slider_lea_res)
		{
			String[] strScore=new String[2];
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}
			
			UserMaster userMaster=null;
			userMaster=(UserMaster)session.getAttribute("userMaster");
			
			try {
				
				TeacherDetail teacherDetail=teacherDetailDAO.findById(teacherId, false, false);
				DistrictMaster districtMaster=districtMasterDAO.findById(districtId, false, false);
				
				TeacherAchievementScore teacherAchievementScore=new TeacherAchievementScore();
				
				List<TeacherAchievementScore> teacherAchievementScoresList=teacherAchievementScoreDAO.getAchievementScoreByTeacherAndDistrict(teacherDetail, districtMaster);
				if(teacherAchievementScoresList.size()==1)
				{
					teacherAchievementScore=teacherAchievementScoresList.get(0);
				}
				
				teacherAchievementScore.setTeacherDetail(teacherDetail);
				teacherAchievementScore.setDistrictMaster(districtMaster);
				
				teacherAchievementScore.setAcademicAchievementScore(slider_aca_ach);
				teacherAchievementScore.setLeadershipAchievementScore(slider_lea_res);
				
				teacherAchievementScore.setAcademicAchievementMaxScore(20);
				teacherAchievementScore.setLeadershipAchievementMaxScore(20);
				
				teacherAchievementScore.setTotalAchievementScore(slider_aca_ach+slider_lea_res);
				teacherAchievementScore.setAchievementMaxScore(40);
				teacherAchievementScore.setScoredBy(userMaster);
				teacherAchievementScore.setScoredDateTime(new Date());
				
				teacherAchievementScoreDAO.makePersistent(teacherAchievementScore);
				
				strScore[0]=teacherAchievementScore.getAcademicAchievementScore()+"/"+teacherAchievementScore.getAcademicAchievementMaxScore();
				strScore[1]=teacherAchievementScore.getLeadershipAchievementScore()+"/"+teacherAchievementScore.getLeadershipAchievementMaxScore();
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			return strScore;
		}
	/*@End
	 *@Ashish 
	 *@Description :: Get Update and Save Score from Teacher Pool
	 **/

		public String downloadCertificationforCG(Integer referenceId,String teaId)
		{
			
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		    }
			String path="";
			try 
			{
				TeacherCertificate tele = teacherCertificateDAO.findById(referenceId, false, false);
				String fileName= tele.getPathOfCertification();
		        String source = Utility.getValueOfPropByKey("teacherRootPath")+""+Integer.parseInt(teaId)+"/"+fileName;
		        String target = context.getServletContext().getRealPath("/")+"/"+"/teacher/"+teaId+"/";
		        File sourceFile = new File(source);
		        File targetDir = new File(target);
		        if(!targetDir.exists())
		        	targetDir.mkdirs();
		        
		        File targetFile = new File(targetDir+"/"+sourceFile.getName());
		        if(sourceFile.exists()){
		        	FileUtils.copyFile(sourceFile, targetFile);
		        	path = Utility.getValueOfPropByKey("contextBasePath")+"/teacher/"+Integer.parseInt(teaId)+"/"+sourceFile.getName();
		        }	
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			return path;
		
		}
		
		
		public List<JobForTeacher> getJFT(TeacherDetail teacherDetail,UserMaster userMaster,String Source)
		{
			System.out.println("*********** getJFT ******************");
			System.out.println("Source "+Source +" TID "+teacherDetail.getTeacherId());
			Criterion criterion_teacherId = Restrictions.eq("teacherId", teacherDetail);
			List<JobForTeacher>  jobForTeachers=new ArrayList<JobForTeacher>();
			int entityType=userMaster.getEntityType();
			System.out.println("entityType "+entityType);
			if(entityType==2)
			{
				System.out.println("logged In DA "+userMaster.getDistrictId().getDistrictId());
				Criterion criterion2 = Restrictions.eq("districtMaster", userMaster.getDistrictId());
				List<JobOrder> jobOrders=jobOrderDAO.findByCriteria(criterion2);
				if(jobOrders.size() > 0)
				{
					Criterion criterion3 = Restrictions.in("jobId", jobOrders);
					jobForTeachers = jobForTeacherDAO.findByCriteria(criterion_teacherId,criterion3);
				}
			}
			else if(entityType==3)
			{
				System.out.println("logged In SA "+userMaster.getSchoolId().getSchoolId() +" DID "+userMaster.getDistrictId().getDistrictId());
				List<JobOrder> jobOrders=new ArrayList<JobOrder>();
				List<SchoolInJobOrder> inJobOrders=new ArrayList<SchoolInJobOrder>();
				if(userMaster.getSchoolId()!=null)
					inJobOrders=schoolInJobOrderDAO.findJobBySchool(userMaster.getSchoolId());
				if(inJobOrders.size() > 0)
				{
					for(SchoolInJobOrder inJobOrder:inJobOrders)
					{
						jobOrders.add(inJobOrder.getJobId());
						System.out.println("SA School JobId "+inJobOrder.getJobId().getJobId());
					}
				}
				
				if(jobOrders.size() > 0)
				{
					Criterion criterion3 = Restrictions.in("jobId", jobOrders);
					jobForTeachers = jobForTeacherDAO.findByCriteria(criterion_teacherId,criterion3);
				}
			}
			else
			{
				System.out.println("logged In TM ");
				jobForTeachers = jobForTeacherDAO.findJobByTeacher(teacherDetail);
			}
			System.out.println("jobForTeachers Size "+jobForTeachers.size());
			
			return jobForTeachers;
		}
		
		
		
		public List<JobForTeacher> getJFTLastContactedOn(TeacherDetail teacherDetail,UserMaster userMaster,JobOrder jobOrder,String Source)
		{
			System.out.println("*********** getJFTLastContactedOn ******************");
			System.out.println("Source "+Source +" TID "+teacherDetail.getTeacherId());
			
			Order sortOrder=Order.desc("lastActivityDate");
			
			Criterion criterion_teacherId = Restrictions.eq("teacherId", teacherDetail);
			List<JobForTeacher>  jobForTeachers=new ArrayList<JobForTeacher>();
			int entityType=userMaster.getEntityType();
			System.out.println("entityType "+entityType);
			if(entityType==2)
			{
				System.out.println("logged In DA "+userMaster.getDistrictId().getDistrictId());
				
				if(jobOrder==null)
				{
					Criterion criterion2 = Restrictions.eq("districtMaster", userMaster.getDistrictId());
					List<JobOrder> jobOrders=jobOrderDAO.findByCriteria(criterion2);
					if(jobOrders.size() > 0)
					{
						Criterion criterion3 = Restrictions.in("jobId", jobOrders);
						jobForTeachers = jobForTeacherDAO.findByCriteria(sortOrder,criterion_teacherId,criterion3);
					}
				}
				else
				{
					Criterion criterion_jobId = Restrictions.eq("jobId", jobOrder);
					jobForTeachers = jobForTeacherDAO.findByCriteria(sortOrder,criterion_teacherId,criterion_jobId);
				}
				
			}
			else if(entityType==3)
			{
				System.out.println("logged In SA "+userMaster.getSchoolId().getSchoolId() +" DID "+userMaster.getDistrictId().getDistrictId());
				
				if(jobOrder==null)
				{
					List<JobOrder> jobOrders=new ArrayList<JobOrder>();
					List<SchoolInJobOrder> inJobOrders=new ArrayList<SchoolInJobOrder>();
					if(userMaster.getSchoolId()!=null)
						inJobOrders=schoolInJobOrderDAO.findJobBySchool(userMaster.getSchoolId());
					if(inJobOrders.size() > 0)
					{
						for(SchoolInJobOrder inJobOrder:inJobOrders)
						{
							jobOrders.add(inJobOrder.getJobId());
							System.out.println("SA School JobId "+inJobOrder.getJobId().getJobId());
						}
					}
					
					if(jobOrders.size() > 0)
					{
						Criterion criterion3 = Restrictions.in("jobId", jobOrders);
						jobForTeachers = jobForTeacherDAO.findByCriteria(sortOrder,criterion_teacherId,criterion3);
					}
				}
				else
				{
					Criterion criterion_jobId = Restrictions.eq("jobId", jobOrder);
					jobForTeachers = jobForTeacherDAO.findByCriteria(sortOrder,criterion_teacherId,criterion_jobId);
				}
				
			}
			else
			{
				System.out.println("logged In TM ");
				if(jobOrder==null)
				{
					jobForTeachers = jobForTeacherDAO.findByCriteria(sortOrder,criterion_teacherId);
				}
				else
				{
					Criterion criterion_jobId = Restrictions.eq("jobId", jobOrder);
					jobForTeachers = jobForTeacherDAO.findByCriteria(sortOrder,criterion_teacherId,criterion_jobId);	
				}
			}
			System.out.println("jobForTeachers Size "+jobForTeachers.size());
			
			return jobForTeachers;
		}
		
		public TeacherGeneralKnowledgeExam getGKEValuesForProfile(TeacherDetail teacherDetail)
		{
			System.out.println("Calling getGKEValues Ajax Method  \t TeacherId "+teacherDetail.getTeacherId());
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		    }
			
			TeacherGeneralKnowledgeExam teacherGeneralKnowledgeExam=null;
			try{
				teacherGeneralKnowledgeExam=teacherGeneralKnowledgeExamDAO.findTeacherGeneralKnowledgeExam(teacherDetail);
			} 
			catch (Exception e)	{
				e.printStackTrace();
				return teacherGeneralKnowledgeExam;
			}	
			return teacherGeneralKnowledgeExam;
		}
		public TeacherSubjectAreaExam getSAEValuesForProfile(TeacherDetail teacherDetail)
		{
			System.out.println("Calling getSAEValues Ajax Method \t TeacherId "+teacherDetail.getTeacherId());
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		    }
			
			TeacherSubjectAreaExam teacherSubjectAreaExam=null;
			try{
				 teacherSubjectAreaExam = teacherSubjectAreaExamDAO.findTeacherSubjectAreaExam(teacherDetail);
			} 
			catch (Exception e)	{
				e.printStackTrace();
				return teacherSubjectAreaExam;
			}	
			return teacherSubjectAreaExam;
		}
		
		
		public boolean tfaEditAuthUser()
		{
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		    }
			
			boolean chkAuthUsers = false;
			List<DistrictKeyContact> lstdistrictKeyContacts = new ArrayList<DistrictKeyContact>();
			
			
			try
			{
				UserMaster userMaster=null;
				userMaster=(UserMaster)session.getAttribute("userMaster");
				
				if(userMaster!=null)
				{
					ContactTypeMaster contactTypeMaster = contactTypeMasterDAO.findById(13, false, false);
					lstdistrictKeyContacts = districtKeyContactDAO.findByContactTypeAndDistrict(userMaster, contactTypeMaster);
				
					//System.out.println(" lstdistrictKeyContacts :: "+lstdistrictKeyContacts.size());
				
					if(lstdistrictKeyContacts!=null && lstdistrictKeyContacts.size()>0)
					{
						chkAuthUsers = true;
					}
				}
			}catch(Exception e)
			{
				e.printStackTrace();
			}
			
			System.out.println("  chkAuthUsers :: "+chkAuthUsers);
			return chkAuthUsers;
		}
		
		
		public String getTFAValue(List<TeacherExperience> teacherExperienceList,TeacherDetail teacherDetail)
		{
			StringBuilder tFA = new StringBuilder();
			
			System.out.println("  teacherExperienceList>>>>>>>>>>>>>>>>> "+teacherExperienceList.size());
			
			try 
			{
				List<TFAAffiliateMaster> tfaAffiliateMasterList = tfaAffiliateMasterDao.findAll(); 
				tFA.append("<select id='tfaList' disabled='disabled'>");
				tFA.append("<option value='0'>Select</option>");
				
				if(teacherExperienceList!=null && teacherExperienceList.size()>0)
				{
					if(teacherExperienceList.get(0).getTfaAffiliateMaster()!=null )
					{
						tFA.append("<option value=''>N/A</option>");
						for(TFAAffiliateMaster tfalist:tfaAffiliateMasterList)
						{
								if(teacherExperienceList.get(0).getTfaAffiliateMaster().getTfaAffiliateId().equals(tfalist.getTfaAffiliateId()))
									tFA.append("<option id='"+tfalist.getTfaAffiliateId()+"' selected='selected' value='"+tfalist.getTfaAffiliateId()+"'>"+tfalist.getTfaAffiliateName()+"</option>");
								else
									tFA.append("<option id='"+tfalist.getTfaAffiliateId()+"' value='"+tfalist.getTfaAffiliateId()+"'>"+tfalist.getTfaAffiliateName()+"</option>");
						}
					}
					else
					{
						tFA.append("<option value='' selected='selected'>N/A</option>");
						for(TFAAffiliateMaster tfalist:tfaAffiliateMasterList)
						{
								tFA.append("<option id='"+tfalist.getTfaAffiliateId()+"' value='"+tfalist.getTfaAffiliateId()+"'>"+tfalist.getTfaAffiliateName()+"</option>");
						}
					}
					tFA.append("</select>");
				}else
				{
					return null;
				}
				
				
				
				} catch (Exception e) {
				e.printStackTrace();
			}
			return tFA.toString();
		}
		
		public String getDistrictSpecificPortfolioQuestions(Integer jobId,Integer teacherId)
		{
				System.out.println(teacherId+"*****:::::::::::::::||getDistrictSpecificPortfolioQuestions|11|:::::::::::::::::::;; "+jobId);
				WebContext context;
				context = WebContextFactory.get();
				HttpServletRequest request = context.getHttpServletRequest();
				HttpSession session = request.getSession(false);
				if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
				{
					throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
				}
				StringBuffer sb =new StringBuffer();
				try{
					TeacherDetail teacherDetail = null;
		
					if (session == null || session.getAttribute("teacherDetail") == null) {
						//return "false";
					}else
						teacherDetail=(TeacherDetail)session.getAttribute("teacherDetail");
		
					if(teacherId!=null && teacherId!=0){
						teacherDetail = teacherDetailDAO.findById(teacherId, false, false);
					}
					
					if(jobId!=0)
					{
						
						JobOrder jobOrder=jobOrderDAO.findById(jobId, false, false);
						
						List<DistrictSpecificPortfolioQuestions> districtSpecificPortfolioQuestionsList=districtSpecificPortfolioQuestionsDAO.getDistrictSpecificPortfolioQuestionBYDistrict(jobOrder.getDistrictMaster()); 
						
						int totalQuestions = districtSpecificPortfolioQuestionsList.size();
						int cnt = 0;
						if(districtSpecificPortfolioQuestionsList.size()>0){
							List<JobOrder> jobOrders = districtSpecificPortfolioAnswersDAO.findLastTeacherAnswersByDistrictAndJob(teacherDetail,jobOrder);
							List<DistrictSpecificPortfolioAnswers> lastList = new ArrayList<DistrictSpecificPortfolioAnswers>();
		
							if(jobOrders.size()>0)
								lastList = districtSpecificPortfolioAnswersDAO.findTeacherAnswersByDistrictAndJob(teacherDetail,jobOrders.get(0));
		
							Map<Integer, DistrictSpecificPortfolioAnswers> map = new HashMap<Integer, DistrictSpecificPortfolioAnswers>();
		
							for(DistrictSpecificPortfolioAnswers districtSpecificPortfolioAnswers : lastList)
								map.put(districtSpecificPortfolioAnswers.getDistrictSpecificPortfolioQuestions().getQuestionId(), districtSpecificPortfolioAnswers);
		
							DistrictSpecificPortfolioAnswers districtSpecificPortfolioAnswers = null;
							List<DistrictSpecificPortfolioOptions> questionOptionsList = null;
							String questionInstruction = null;
							String shortName = "";
						
							String selectedOptions = "",insertedRanks="";
								for (DistrictSpecificPortfolioQuestions districtSpecificPortfolioQuestions : districtSpecificPortfolioQuestionsList) 
								{
									shortName = districtSpecificPortfolioQuestions.getQuestionTypeMaster().getQuestionTypeShortName();
		
									sb.append("<tr>");
									sb.append("<td width='90%'>");
									sb.append("<div><b>Question "+(++cnt)+" of "+totalQuestions+"</b></div>");
									sb.append("<input type='hidden' name='o_maxMarksS' value='"+(districtSpecificPortfolioQuestions.getMaxMarks()==null?0:districtSpecificPortfolioQuestions.getMaxMarks())+"'  />");
									questionInstruction = districtSpecificPortfolioQuestions.getQuestionInstructions()==null?"":districtSpecificPortfolioQuestions.getQuestionInstructions();
									if(!questionInstruction.equals(""))
										sb.append(""+Utility.getUTFToHTML(questionInstruction)+"");
		
									sb.append("<div id='QS"+cnt+"question'>"+Utility.getUTFToHTML(districtSpecificPortfolioQuestions.getQuestion())+"</div>");
		
									questionOptionsList = districtSpecificPortfolioQuestions.getQuestionOptions();
									districtSpecificPortfolioAnswers = map.get(districtSpecificPortfolioQuestions.getQuestionId());
		
									String checked = "";
									Integer optId = 0;
									String insertedText = "";
									if(districtSpecificPortfolioAnswers!=null)
									{
										if(!shortName.equalsIgnoreCase("rt") && shortName.equalsIgnoreCase(districtSpecificPortfolioAnswers.getQuestionType()))
										{
											if(districtSpecificPortfolioAnswers.getSelectedOptions()!=null)
												optId = Integer.parseInt(districtSpecificPortfolioAnswers.getSelectedOptions());
		
											insertedText = districtSpecificPortfolioAnswers.getInsertedText();
										}
									}
									if(shortName.equalsIgnoreCase("tf") || shortName.equalsIgnoreCase("slsel")|| shortName.equalsIgnoreCase("lkts"))
									{
										sb.append("<table>");
										for (DistrictSpecificPortfolioOptions questionOptions : questionOptionsList) {
											if(optId.equals(questionOptions.getOptionId()))
												checked = "checked";
											else
												checked = "";
		
											sb.append("<tr><td style='border:0px;background-color: transparent;'><div  style='display: block;min-height: 20px;padding-left: 5px;vertical-align: middle;'><input type='radio' name='QS"+cnt+"opt' value='"+questionOptions.getOptionId()+"' "+checked+" /></div>" +
													" <input type='hidden' id='QS"+questionOptions.getOptionId()+"validQuestion' value='"+questionOptions.getValidOption()+"' ></td>" +
													"<td style='border:0px;background-color: transparent;padding-left:5px;' id='qOptS"+questionOptions.getOptionId()+"'>"+questionOptions.getQuestionOption()+" ");
											sb.append("</td></tr>");
										}
										sb.append("</table>");
									}
									if(shortName.equalsIgnoreCase("rt"))
										{	
											if(districtSpecificPortfolioAnswers!=null && districtSpecificPortfolioAnswers.getInsertedRanks()!=null){
												insertedRanks = districtSpecificPortfolioAnswers.getInsertedRanks()==null?"":districtSpecificPortfolioAnswers.getInsertedRanks();
											}
											String[] ranks = insertedRanks.split("\\|");
											int rank=1;
											int count=0;
											String ans = "";
											sb.append("<table>");
											Map<Integer,DistrictSpecificPortfolioOptions> optionMap=new HashMap<Integer, DistrictSpecificPortfolioOptions>();
											for (DistrictSpecificPortfolioOptions teacherQuestionOption : questionOptionsList) {
												optionMap.put(teacherQuestionOption.getOptionId(),teacherQuestionOption);
											}
											for (DistrictSpecificPortfolioOptions teacherQuestionOption : questionOptionsList) {
												try{ans =ranks[count];}catch(Exception e){}
												sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'><input type='text' id='rnkS"+rank+"' name='rankS' class='span1' maxlength='1' onkeypress='return checkForIntUpto6(event)' onblur='checkUniqueRankForPortfolio(this);' value='"+ans+"'/></td><td style='border:0px;background-color: transparent;padding: 6px 3px;'>"+teacherQuestionOption.getQuestionOption()+"");
												sb.append("<input type='hidden' name='scoreS' value='"+(teacherQuestionOption.getScore()==null?0:teacherQuestionOption.getScore())+"' /><input type='hidden' name='optS' value='"+teacherQuestionOption.getOptionId()+"' />");
												sb.append("<input type='hidden' name='o_rankS' value='"+(teacherQuestionOption.getRank()==null?0:teacherQuestionOption.getRank())+"' />");
												sb.append("<script type=\"text/javascript\" language=\"javascript\">");
												sb.append("document.getElementById('rnkS1').focus();");
												sb.append("</script></td></tr>");
												rank++;
												count++;
											}
											sb.append("</table >");
									}
									if(shortName.equalsIgnoreCase("it"))
									{
										sb.append("<table >");
										for (DistrictSpecificPortfolioOptions teacherQuestionOption : questionOptionsList) {
											sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;vertical-align:middle;'><input type='radio' name='optS' value='"+teacherQuestionOption.getOptionId()+"' /></td><td style='border:0px;background-color: transparent;vertical-align:middle;padding-top:5px;'><img src='showImage?image=ques_images/"+teacherQuestionOption.getQuestionOption()+"'> ");
											sb.append("<input type='hidden' name='scoreS' value='"+(teacherQuestionOption.getScore()==null?0:teacherQuestionOption.getScore())+"'  /></td></tr>");
										}
										sb.append("</table>");
									}
									if(shortName.equalsIgnoreCase("et"))
									{
										sb.append("<table>");
										for (DistrictSpecificPortfolioOptions questionOptions : questionOptionsList) {
											if(optId.equals(questionOptions.getOptionId()))
												checked = "checked";
											else
												checked = "";
		
											sb.append("<tr><td style='border:0px;background-color: transparent;'><div  style='display: block;min-height: 20px;padding-left: 5px;vertical-align: middle;'><input type='radio' name='QS"+cnt+"opt' value='"+questionOptions.getOptionId()+"' "+checked+" /></div>" +
													" <input type='hidden' id='QS"+questionOptions.getOptionId()+"validQuestion' value='"+questionOptions.getValidOption()+"' ></td>" +
													"<td style='border:0px;background-color: transparent;padding-left:5px;'  id='qOptS"+questionOptions.getOptionId()+"'>"+questionOptions.getQuestionOption()+" ");
											sb.append("</td></tr>");
										}
										sb.append("</table>");
		
										sb.append("&nbsp;&nbsp;<span style=''><textarea  name='QS"+cnt+"opt' id='QS"+cnt+"optet' class='form-control' maxlength='5000' rows='8' style='width:98%;margin:5px;'>"+insertedText+"</textarea><span><br/>");
										sb.append("<script type=\"text/javascript\" language=\"javascript\">");
										sb.append("document.getElementById('QS"+cnt+"optet').focus();");
										sb.append("</script>");
									}
									if(shortName.equalsIgnoreCase("ml") || shortName.equalsIgnoreCase("sl"))
									{
										sb.append("&nbsp;&nbsp;<textarea name='QS"+cnt+"opt' id='QS"+cnt+"opt' class='form-control' maxlength='5000' rows='8' style='width:98%;margin:5px;'>"+insertedText+"</textarea><br/>");					
										sb.append("<script type=\"text/javascript\" language=\"javascript\">");
										sb.append("document.getElementById('QS"+cnt+"opt').focus();");
										sb.append("</script>");
									}
									sb.append("<input type='hidden' id='QS"+cnt+"questionId' value='"+districtSpecificPortfolioQuestions.getQuestionId()+"'/>" );
									sb.append("<input type='hidden' id='QS"+cnt+"questionTypeId' value='"+districtSpecificPortfolioQuestions.getQuestionTypeMaster().getQuestionTypeId()+"'/>" );
									sb.append("<input type='hidden' id='QS"+cnt+"questionTypeShortName' value='"+shortName+"'/>" );
		
									sb.append("</td>");
									sb.append("</tr>");
									insertedText = "";
								}
						    }
						System.out.println("jobOrder.getDistrictMaster().getTextForDistrictSpecificQuestions()   "+jobOrder.getDistrictMaster().getTextForDistrictSpecificQuestions());
						//sb.append("@##@"+cnt+"@##@"+jobOrder.getDistrictMaster().getTextForDistrictSpecificQuestions());
		
						
					}
					//////////////////////	
				}catch (Exception e) {
					e.printStackTrace();
				}
				return sb.toString();
		}
		
		public String getPNQValue(List<TeacherExperience> teacherExperienceList,TeacherDetail teacherDetail)
		{
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}
			
			UserMaster userMaster = (UserMaster)session.getAttribute("userMaster");
			StringBuilder pNQ = new StringBuilder();
			try 
			{
					Map<Integer, String> options = new HashMap<Integer, String>();
						options.put(0, "Select PNQ");
						options.put(1, "Yes");
						options.put(2, "No");
				
				if(teacherExperienceList!=null && teacherExperienceList.size()>0)
				{		
					if(userMaster.getRoleId().getRoleId().equals(2)){
						 pNQ.append("<select id='pnqList' class='form-control pnqDisabled' onchange='pnqSave("+teacherDetail.getTeacherId()+");'>");
					 }
					 else{
						 pNQ.append("<select id='pnqList' class='form-control pnqDisabled' disabled='disabled'>");
					 }
					for (Integer i = 0; i < options.size(); i++) {
						if(i.equals(teacherExperienceList.get(0).getPotentialQuality()))
							pNQ.append("<option id='"+i+"' selected='selected' value='"+i+"'>"+options.get(i)+"</option>");
						else
							pNQ.append("<option id='"+i+"' value='"+i+"'>"+options.get(i)+"</option>");
					}
					pNQ.append("</select>");
					pNQ.append("<input type='hidden' value='"+teacherExperienceList.get(0).getPotentialQuality()+"'  id='pnqSelected'>");
				}else
				{
					
					if(userMaster.getRoleId().getRoleId().equals(2)){
						 pNQ.append("<select id='pnqList' class='form-control pnqDisabled' onchange='pnqSave("+teacherDetail.getTeacherId()+");'>");
					 }
					 else{
						 pNQ.append("<select id='pnqList' class='form-control pnqDisabled' disabled='disabled'>");
					 }
					for (Integer i = 0; i < options.size(); i++) {						
							pNQ.append("<option id='"+i+"' value='"+i+"'>"+options.get(i)+"</option>");
					}
					pNQ.append("</select>");
					pNQ.append("<input type='hidden' value='0'  id='pnqSelected'>");
				
				}
				
				
				
				} catch (Exception e) {
				e.printStackTrace();
			}
			return pNQ.toString();
		}
		//updatePNQByUser
				
		public Integer updatePNQByUser(Integer teacherId,Integer pnqId)
		{

			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}
			
			UserMaster userMaster = (UserMaster)session.getAttribute("userMaster");
			
			try
			{
				TeacherDetail teacherDetail = teacherDetailDAO.findById(teacherId, false, false);
				
				
				if(pnqId!=null && !pnqId.equals(""))
				{
					if(teacherDetail!=null)
					{
							TeacherExperience teacherExperience = teacherExperienceDAO.findTeacherExperienceByTeacher(teacherDetail);
							
							if(teacherExperience==null){
								System.out.println("teacherDetail   "+teacherDetail.getTeacherId());
								teacherExperience = new TeacherExperience();
								teacherExperience.setTeacherId(teacherDetail);
								teacherExperience.setCanServeAsSubTeacher(2);
								teacherExperience.setIsNonTeacher(false);
							}
							
							teacherExperience.setPotentialQuality(pnqId);
							teacherExperienceDAO.makePersistent(teacherExperience);
							return 1;
					}
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			return 2;
		
		}
		public  String html2text(String html) {
	        String text = html.replaceAll("\\<.*?\\>", "");
			return text;
		}
		public  String htmlwords(String html) {
			String htmlReturn = html.substring(0, Math.min(20, html.length()));
			return htmlReturn;
		//String truncated = explanation.subString(0, Math.min(49, explanation.length()));
		}
		
		public boolean getDistrictAssessmentFlag(TeacherDetail teacherDetail)
		{
			try
			{
				List<ExaminationCodeDetails> examCodeDetailsList = new ArrayList<ExaminationCodeDetails>();
				examCodeDetailsList = examinationCodeDetailsDAO.getDistrictAssessmentByTeacher(teacherDetail);
				
				if(examCodeDetailsList.size()>0)
				{
					return true;
				}
				
			}catch(Exception e)
			{
				e.printStackTrace();
			}
			
			return false;
		}
		
		public String displayTeacherLanguageByTeacherId(String noOfRow, String pageNo,String sortOrder,String sortOrderType,Integer teacherId,String visiLocation)
		{		
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}
			StringBuffer sb = new StringBuffer();		
			try 
			{
				int noOfRowInPage 	= Integer.parseInt(noOfRow);
				int pgNo 			= Integer.parseInt(pageNo);
				int start 			= ((pgNo-1)*noOfRowInPage);
				int end 			= ((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
				int totalRecord 	= 0;
				//------------------------------------
				System.out.println(start+" start "+end);
				//TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");

				List<TeacherLanguages> listTeacherExp= null;
				/** set default sorting fieldName **/
				String sortOrderFieldName	=	"createdDateTime";
				String sortOrderNoField		=	"createdDateTime";

				/**Start set dynamic sorting fieldName **/
				Order  sortOrderStrVal=null;

				if(sortOrder!=null){
					if(!sortOrder.equals("") && !sortOrder.equals(null)){
						sortOrderFieldName=sortOrder;
						sortOrderNoField=sortOrder;
					}
				}

				String sortOrderTypeVal="0";
				if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
					if(sortOrderType.equals("0")){
						sortOrderStrVal=Order.asc(sortOrderFieldName);
					}else{
						sortOrderTypeVal="1";
						sortOrderStrVal=Order.desc(sortOrderFieldName);
					}
				}else{
					sortOrderTypeVal="0";
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}
				/**End ------------------------------------**/
				
				//teacherId
				TeacherDetail teacherDetail = teacherDetailDAO.findById(teacherId, false, false);
				Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);				
				listTeacherExp = teacherLanguagesDAO.findByCriteria(sortOrderStrVal,criterion1);		

				List<TeacherLanguages> sortedTeacherRole		=	new ArrayList<TeacherLanguages>();

				SortedMap<String,TeacherLanguages>	sortedMap = new TreeMap<String,TeacherLanguages>();
				int mapFlag=2;
				if(mapFlag==1){
					NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet();  
					for (Iterator iter=navig.iterator();iter.hasNext();) {  
						Object key = iter.next(); 
						sortedTeacherRole.add((TeacherLanguages) sortedMap.get(key));
					} 
				}else if(mapFlag==0){
					Iterator iterator = sortedMap.keySet().iterator();
					while (iterator.hasNext()) {
						Object key = iterator.next();
						sortedTeacherRole.add((TeacherLanguages) sortedMap.get(key));
					}
				}else{
					sortedTeacherRole=listTeacherExp;
				}

				totalRecord =listTeacherExp.size();
				if(totalRecord<end)
					end=totalRecord;
				List<TeacherLanguages> listsortedTeacherRole		=	sortedTeacherRole.subList(start,end);

				String responseText="";
				sb.append("<table border='0' id='teacherLanguageGrid' class='table table-striped'>");
				sb.append("<thead class='bg'>");			
				sb.append("<tr>");

				responseText=PaginationAndSorting.responseSortingMLink_1("Language",sortOrderFieldName,"language",sortOrderTypeVal,pgNo);
				sb.append("<th valign='top'>"+responseText+"</th>");

				responseText=PaginationAndSorting.responseSortingMLink_1("Oral&nbsp;Skill",sortOrderFieldName,"oralSkills",sortOrderTypeVal,pgNo);
				sb.append("<th valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingMLink_1("Written&nbsp;Skill",sortOrderFieldName,"writtenSkills",sortOrderTypeVal,pgNo);
				sb.append("<th valign='top'>"+responseText+"</th>");

				/*sb.append("<th  width='20%' valign='top'>");
				sb.append("Actions");
				sb.append("</th>");*/	

				sb.append("</tr>");
				sb.append("</thead>");
				Map<Integer,String> optionMap = new HashMap<Integer, String>();
				optionMap.put(1, "Polite");
				optionMap.put(2, "Literate");
				optionMap.put(3, "Fluent");
				if(listTeacherExp!=null)
					
					for(TeacherLanguages pojo:listsortedTeacherRole)
					{
						sb.append("<tr>");
						sb.append("<td>");
						sb.append(pojo.getLanguage());
						sb.append("</td>");
						
						sb.append("<td>");
						sb.append(optionMap.get(pojo.getOralSkills()));
						sb.append("</td>");
						
						sb.append("<td>");
						sb.append(optionMap.get(pojo.getWrittenSkills()));
						sb.append("</td>");
										
						sb.append("</tr>");
					}

				if(listsortedTeacherRole==null || listsortedTeacherRole.size()==0)
				{
					sb.append("<tr>");
					sb.append("<td colspan='3'>");
					sb.append(lblNoRecord);
					sb.append("</td>");
					sb.append("</tr>");
				}
				sb.append("</table>");
				sb.append(PaginationAndSorting.getPaginationPaging(request,totalRecord,noOfRow, pageNo,"pageLangProf"));
				sb.append("<input type='hidden' id='ttlTeacherLanguage' value='"+listsortedTeacherRole.size()+"'>");
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			return sb.toString();	
		}
		
		public String getVeteranOrigin(DistrictMaster districtMaster, TeacherDetail teacherDetail,TeacherPersonalInfo teacherPersonalInfo)
		{
			if(districtMaster!=null && districtMaster.getDistrictId()!=null && teacherDetail!=null && teacherDetail.getTeacherId()!=null)
			{
			String verteranVal = getTeacherVeteran(districtMaster, teacherDetail);
			String[] verteranRadioButtonValues = {""};
			if(verteranVal!=null)
				verteranRadioButtonValues = verteranVal.split("#");
			
			for(String val : verteranRadioButtonValues)
				System.out.println(" "+val);
			String checked=null;
			int vetTan = 0;
			if(teacherPersonalInfo!=null && teacherPersonalInfo.getIsVateran()!=null){
				vetTan = teacherPersonalInfo.getIsVateran();
			}
			String checkedRadio="";
			StringBuffer veteranBuffer = new StringBuffer();
			
			if(vetTan==1)
				veteranBuffer.append("<script>clickedOnYes();</script>");
			if(districtMaster!=null && districtMaster.getDistrictId().equals(806810)){
			veteranBuffer.append("<div id='veteranDiv' class='profileContent hide' style='font-weight: normal'>");
			veteranBuffer.append("<div id='veteranDiv' class='profileContent hide'>");}
			else{
				veteranBuffer.append("<div id='veteranDiv' class='profileContent' style='font-weight: normal'>");
				veteranBuffer.append("<div id='veteranDiv' class='profileContent'>");
			}
				
			
			veteranBuffer.append("<div class='row'> <div class='col-sm-12 col-md-12 top5'>");
			veteranBuffer.append("<label class='span15'> 	<strong>"+lblDp_commons4+"<span class='required'>*</span></strong> </label>");
			veteranBuffer.append("</div> </div>");
			
			veteranBuffer.append("<table style='width:13%;pointer-events: none;'>");
			veteranBuffer.append("<tbody><tr>");
			if(vetTan==1)
				veteranBuffer.append("<td> <input type='radio' id='vt1' value='1' checked name='veteran' onclick='clickedOnYes();'> </td>");
			else
				veteranBuffer.append("<td> <input type='radio' id='vt1' value='1' checked name='veteran' onclick='clickedOnYes();'> </td>");
			veteranBuffer.append("<td style='padding-left:4px;padding-top:4px'> Yes </td>");
			if(vetTan==0)
				veteranBuffer.append("<td> <input type='radio' checked id='vt2' value='0' name='veteran' onclick='clickedOnNo();' </td>");
			else
				veteranBuffer.append("<td> <input type='radio' id='vt2' value='0' name='veteran' onclick='clickedOnNo();' </td>");
			
			veteranBuffer.append("<td style='padding-left:4px;padding-top:4px'> No</td>");
			veteranBuffer.append("</tr>");
			veteranBuffer.append("<tr></tr>");
			veteranBuffer.append("</tbody></table>");

			
			if(districtMaster.getDistrictId().equals(1201470))
				veteranBuffer.append("<div class='col-sm-12 col-md-12 top5 hide' id='vetranOptionDiv' class='hide'>");
			else
				veteranBuffer.append("<div class='col-sm-12 col-md-12 top5 hide' id='' class='hide'>");
		        	
			veteranBuffer.append("<table style='pointer-events: none;'>");
			veteranBuffer.append("<tbody>");
			veteranBuffer.append("<tr>");
			checked = (verteranRadioButtonValues.length>0 && verteranRadioButtonValues[0].equals("1"))? "checked" : "";
			veteranBuffer.append("<td style='border:0px;background-color: transparent;'><div style='display: block;min-height: 40px;padding-left: 5px;vertical-align: middle;'><input type='radio' "+checked+" id='veteranOptS' name='veteranOptS' value='1'></div></td>");
			veteranBuffer.append("<td style='border:0px;background-color: transparent;padding-left:5px;padding-top: 4px;'><font size='2' color='red'>A. </font> "+MSGserviceconnected+"</td>");
			veteranBuffer.append("</tr>");
			checked = (verteranRadioButtonValues.length>0 && verteranRadioButtonValues[0].equals("2"))? "checked" : "";
			veteranBuffer.append("<tr><td style='border:0px;background-color: transparent;'><div style='display: block;min-height: 40px;padding-left: 5px;vertical-align: middle;'><input type='radio' "+checked+" id='veteranOptS' name='veteranOptS' value='2'></div></td>");
			veteranBuffer.append("<td style='border:0px;background-color: transparent;padding-left:5px;padding-top: 4px;' ><font size='2' color='red'>B. </font> "+msgDp_commons6+" </td>");
			veteranBuffer.append("</tr>");
			checked = (verteranRadioButtonValues.length>0 && verteranRadioButtonValues[0].equals("3"))? "checked" : "";
			veteranBuffer.append("<tr><td style='border:0px;background-color: transparent;'><div style='display: block;min-height: 40px;padding-left: 5px;vertical-align: middle;'><input type='radio' "+checked+" id='veteranOptS' name='veteranOptS' value='3'></div></td>");
			veteranBuffer.append("<td style='border:0px;background-color: transparent;padding-left:5px;padding-top: 4px;' ><font size='2' color='red'>C. </font> "+msgDp_commons7+" </td>");
			veteranBuffer.append("</tr>");
			checked = (verteranRadioButtonValues.length>0 && verteranRadioButtonValues[0].equals("4"))? "checked" : "";
			veteranBuffer.append("<tr><td style='border:0px;background-color: transparent;'><div style='display: block;min-height: 20px;padding-left: 5px;vertical-align: middle;'><input type='radio' "+checked+" id='veteranOptS' name='veteranOptS' value='4'></div></td>");
			veteranBuffer.append("<td style='border:0px;background-color: transparent;padding-left:5px;padding-top: 4px;' ><font size='2' color='red'>D. </font> "+msgDpcommo15	+" </td>");
			veteranBuffer.append("</tr>");
			checked = (verteranRadioButtonValues.length>0 && verteranRadioButtonValues[0].equals("5"))? "checked" : "";
			veteranBuffer.append("<tr><td style='border:0px;background-color: transparent;'><div style='display: block;min-height: 40px;padding-left: 5px;vertical-align: middle;'><input type='radio' "+checked+" id='veteranOptS' name='veteranOptS' value='5'></div></td>");
			veteranBuffer.append("<td style='border:0px;background-color: transparent;padding-left:5px;padding-top: 4px;' ><font size='2' color='red'>E. </font> "+msgDp_commons8+" </td>");
			veteranBuffer.append("</tr>");
			checked = (verteranRadioButtonValues.length>0 && verteranRadioButtonValues[0].equals("6"))? "checked" : "";
			veteranBuffer.append("<tr><td style='border:0px;background-color: transparent;'><div style='display: block;min-height: 20px;padding-left: 5px;vertical-align: middle;'><input type='radio' "+checked+" id='veteranOptS' name='veteranOptS' value='6'></div></td>");
			veteranBuffer.append("<td style='border:0px;background-color: transparent;padding-left:5px;padding-top: 4px;' ><font size='2' color='red'>F. </font> "+msgDp_commons9+" </td>");
			veteranBuffer.append("</tr>");
			checked = (verteranRadioButtonValues.length>0 && verteranRadioButtonValues[0].equals("7"))? "checked" : "";
			veteranBuffer.append("<tr><td style='border:0px;background-color: transparent;'><div style='display: block;min-height: 20px;padding-left: 5px;vertical-align: middle;'><input type='radio' "+checked+" id='veteranOptS' name='veteranOptS' value='7'></div></td>");
			veteranBuffer.append("<td style='border:0px;background-color: transparent;padding-left:5px;padding-top: 4px;' ><font size='2' color='red'>G. </font> "+msgDp_commons10+" </td>");
			veteranBuffer.append("</tr>");
			veteranBuffer.append("</tbody>");
			veteranBuffer.append("</table>");
					
			veteranBuffer.append("<div class='top10'>"+msgDp_commons11+"</div>");
					
			veteranBuffer.append("<table style='pointer-events: none;'>");
			veteranBuffer.append("<tbody>");
			veteranBuffer.append("<tr>");
			checked = (verteranRadioButtonValues.length>1 && verteranRadioButtonValues[1].equals("1"))? "checked" : "";
			veteranBuffer.append("<td style='border:0px;background-color: transparent;'><div style='display: block;min-height: 20px;padding-left: 5px;vertical-align: middle;'><input type='radio' "+checked+" id='veteranPreference1' name='veteranPreference1' value='1'></div> </td>");
			veteranBuffer.append("<td style='border:0px;background-color: transparent;padding-left:5px;padding-top: 4px;' >Yes </td></tr>");
			checked = (verteranRadioButtonValues.length>1 && verteranRadioButtonValues[1].equals("2"))? "checked" : "";
			veteranBuffer.append("<tr><td style='border:0px;background-color: transparent;'><div style='display: block;min-height: 20px;padding-left: 5px;vertical-align: middle;'><input type='radio' "+checked+" id='veteranPreference1' name='veteranPreference1' value='2'></div>"); 
			veteranBuffer.append("</td><td style='border:0px;background-color: transparent;padding-left:5px;padding-top: 4px;'>No </td>");
			veteranBuffer.append("</tr>");
			veteranBuffer.append("</tbody>");
			veteranBuffer.append("</table>");
					
			veteranBuffer.append("<div class='top10'>"+msgDp_commons12+"</div>");
			
			veteranBuffer.append("<table style='pointer-events: none;'>");
			veteranBuffer.append("<tbody>");
			veteranBuffer.append("<tr>");
			checked = (verteranRadioButtonValues.length>2 && verteranRadioButtonValues[2].equals("1"))? "checked" : "";
			veteranBuffer.append("<td style='border:0px;background-color: transparent;'><div style='display: block;min-height: 20px;padding-left: 5px;vertical-align: middle;'><input type='radio' "+checked+" id='veteranPreference2' name='veteranPreference2' value='1'></div> </td>");
			veteranBuffer.append("<td style='border:0px;background-color: transparent;padding-left:5px;padding-top: 4px;' >Yes </td>");
			veteranBuffer.append("</tr>");
			checked = (verteranRadioButtonValues.length>2 && verteranRadioButtonValues[2].equals("2"))? "checked" : "";
			veteranBuffer.append("<tr><td style='border:0px;background-color: transparent;'><div style='display: block;min-height: 20px;padding-left: 5px;vertical-align: middle;'><input type='radio' "+checked+" id='veteranPreference2' name='veteranPreference2' value='2'></div> </td>");
			veteranBuffer.append("<td style='border:0px;background-color: transparent;padding-left:5px;padding-top: 4px;'>No </td>");
			veteranBuffer.append("</tr>");
			veteranBuffer.append("</tbody>");
			veteranBuffer.append("</table>");
			
			veteranBuffer.append("</div>");
			veteranBuffer.append("</div>");
			
			return veteranBuffer.toString();
			}
			return "";
		}
		
		public String getEthnicOrigin(TeacherPersonalInfo teacherPersonalInfo)
		{
			List<EthnicOriginMaster> lstethnicOriginMasters = null;
			lstethnicOriginMasters=ethnicOriginMasterDAO.findByCriteria(Restrictions.eq("status", "A"));
			
			StringBuffer ethnicOriginBuffer = new StringBuffer();
			ethnicOriginBuffer.append("<table style='width:85%;pointer-events: none;'> <tr> <td colspan='4'>");
			ethnicOriginBuffer.append("Ethnic Origin");		
			ethnicOriginBuffer.append("</td> </tr>");
			
			int k=0;
			int l=0;
			Integer teacherEthnicOriginId = null;
			if(teacherPersonalInfo!=null && teacherPersonalInfo.getEthnicOriginId()!=null && teacherPersonalInfo.getEthnicOriginId().getEthnicOriginId()!=null)
				teacherEthnicOriginId = teacherPersonalInfo.getEthnicOriginId().getEthnicOriginId();
			
			for(EthnicOriginMaster ethnicOriginMaster :lstethnicOriginMasters)
			{
				if(k==l && k!=0)
				{
					ethnicOriginBuffer.append("</tr>");
				}
				
				if(k%4==0)
				{
					ethnicOriginBuffer.append("<tr class='profileContent' style='font-weight: normal'>");
					l=k+4;
				}
				
				if(teacherEthnicOriginId!=null && teacherEthnicOriginId.equals(ethnicOriginMaster.getEthnicOriginId()))
					ethnicOriginBuffer.append("<td> <input type='radio' checked name='ethnicOriginId' id='ethnicOriginId' value='"+ethnicOriginMaster.getEthnicOriginId()+"'> </td>");
				else
					ethnicOriginBuffer.append("<td> <input type='radio' name='ethnicOriginId' id='ethnicOriginId' value='"+ethnicOriginMaster.getEthnicOriginId()+"'> </td>");
				ethnicOriginBuffer.append("<td style='width:25%;padding-top:4px;padding-left:4px;'>"+ethnicOriginMaster.getEthnicOriginName()+"</td>");
				
				k++;
			}
			ethnicOriginBuffer.append("</table>");
			
			return ethnicOriginBuffer.toString();
		}
		
		public String getEthnicity(TeacherPersonalInfo teacherPersonalInfo)
		{
		
			List<EthinicityMaster> lstEthinicityMasters = null;
			lstEthinicityMasters=ethinicityMasterDAO.findByCriteria(Restrictions.eq("status", "A"));
			
			StringBuffer ethinicityBuffer = new StringBuffer();
			ethinicityBuffer.append("<table style='width:85%;pointer-events: none;'> <tr> <td colspan='6'>");
			ethinicityBuffer.append(lblEth);		
			ethinicityBuffer.append("</td> </tr>");
			
			int k=0;
			int l=0;
			Integer teacherEthnicityId = null;
			if(teacherPersonalInfo!=null && teacherPersonalInfo.getEthnicityId()!=null && teacherPersonalInfo.getEthnicityId().getEthnicityId()!=null)
				teacherEthnicityId = teacherPersonalInfo.getEthnicityId().getEthnicityId();
			
			for(EthinicityMaster ethinicityMaster : lstEthinicityMasters)
			{
				if(k==l && k!=0)
				{
					ethinicityBuffer.append("</tr>");
				}
				
				if(k%4==0)
				{
					ethinicityBuffer.append("<tr class='profileContent' style='font-weight: normal'>");
					l=k+4;
				}
				
				if(teacherEthnicityId!=null && teacherEthnicityId.equals(ethinicityMaster.getEthnicityId()))
					ethinicityBuffer.append("<td> <input type='radio' checked name='ethinicityId' id='ethinicityId' value='"+ethinicityMaster.getEthnicityId()+"'> </td>");
				else
					ethinicityBuffer.append("<td> <input type='radio' name='ethinicityId' id='ethinicityId' value='"+ethinicityMaster.getEthnicityId()+"'> </td>");
				ethinicityBuffer.append("<td style='width:25%;padding-top:4px;padding-left:4px;' >"+ethinicityMaster.getEthnicityName()+"</td>");
						
				k++;
			}
			
			k--;
			while(k%4!=0)
			{
				ethinicityBuffer.append("<td></td>");
				ethinicityBuffer.append("<td style='width:25%' class='divlableval'></td>");
				k++;
			}
			
			ethinicityBuffer.append("</table>");
			
			return ethinicityBuffer.toString();
		}
		
		public String getRace(TeacherPersonalInfo teacherPersonalInfo)
		{
			List<RaceMaster> lstraceList = null;
			lstraceList = raceMasterDAO.findAllRaceByOrder();
			
			StringBuffer raceBuffer = new StringBuffer();
			raceBuffer.append("<table style='width:85%;pointer-events: none;'> <tr> <td colspan='4'>");
			raceBuffer.append(lblRac);		
			raceBuffer.append("</td> </tr>");
			
			int k=0;
			int l=0;
			List<String> raceList = new ArrayList<String>();
			if(teacherPersonalInfo!=null && teacherPersonalInfo.getRaceId()!=null)
				raceList = Arrays.asList(teacherPersonalInfo.getRaceId().split(","));
			
			for(RaceMaster raceMaster : lstraceList)
			{
				if(k==l && k!=0)
				{
					raceBuffer.append("</tr>");
				}
				
				if(k%4==0)
				{
					raceBuffer.append("<tr class='profileContent' style='font-weight: normal'>");
					l=k+4;
				}
				
				if(raceList.contains(""+raceMaster.getRaceId()))
					raceBuffer.append("<td> <input type='checkbox' checked name='raceId' id='raceId' value='"+raceMaster.getRaceId()+"'> </td>");
				else
					raceBuffer.append("<td> <input type='checkbox' name='raceId' id='raceId' value='"+raceMaster.getRaceId()+"'> </td>");
				
				raceBuffer.append("<td style='width:34%;padding-top:4px;padding-left:4px;'>"+raceMaster.getRaceName()+"</td>");
						
				k++;
			}
			
			k--;
			while(k%4!=0)
			{
				raceBuffer.append("<td></td>");
				raceBuffer.append("<td style='width:25%' class='divlableval'></td>");
				k++;
			}
			
			raceBuffer.append("</table>");
			
			return raceBuffer.toString();
		}
		
		public String getTeacherVeteran(DistrictMaster districtId, TeacherDetail teacherDetail)
	    {
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}	
				
			List<DistrictSpecificTeacherTfaOptions> districtSpecificTeacherTfaOptions=null;
			
			String selectedOptions = "";
			try{
				Criterion criterion = Restrictions.eq("districtMaster", districtId);
				Criterion criterion2 =Restrictions.eq("teacherDetail", teacherDetail);
				districtSpecificTeacherTfaOptions = districtSpecificTeacherTfaOptionsDAO.findByCriteria(criterion,criterion2);
				
				if(districtSpecificTeacherTfaOptions!=null && districtSpecificTeacherTfaOptions.size()>0 && districtSpecificTeacherTfaOptions.get(0).getTfaOptions()!=null){
					selectedOptions = districtSpecificTeacherTfaOptions.get(0).getTfaOptions()==null?"":districtSpecificTeacherTfaOptions.get(0).getTfaOptions();
				}
			} 
			catch (Exception e)  {
				e.printStackTrace();
				return selectedOptions;
			}    
			return selectedOptions;
	    }
		
		public String getInvolvementGrid(Integer teacherId, String noOfRow, String pageNo,String sortOrder,String sortOrderType)
		{
			System.out.println("getInvolvementGrid........");
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}

			StringBuffer sb = new StringBuffer();		
			try 
			{
				int noOfRowInPage = Integer.parseInt(noOfRow);
				int pgNo = Integer.parseInt(pageNo);
				int start = ((pgNo-1)*noOfRowInPage);
				int end = ((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
				int totalRecord = 0;
				//------------------------------------

				TeacherDetail teacherDetail = teacherDetailDAO.findById(teacherId, false, false);
				
				List<TeacherInvolvement> listTeacherInvolvements = null;
				/** set default sorting fieldName **/
				String sortOrderFieldName	=	"organization";
				String sortOrderNoField		=	"organization";

				/**Start set dynamic sorting fieldName **/
				Order  sortOrderStrVal=null;

				if(sortOrder!=null){
					if(!sortOrder.equals("") && !sortOrder.equals(null) && !sortOrder.equals("orgtype")){
						sortOrderFieldName=sortOrder;
						sortOrderNoField=sortOrder;
					}
					if(sortOrder.equals("orgtype")){
						sortOrderNoField="orgtype";
					}
				}

				String sortOrderTypeVal="0";
				if(sortOrderType!=null && !sortOrderType.equals("") && !sortOrderType.equals(null)){
					if(sortOrderType.equals("0")){
						sortOrderStrVal=Order.asc(sortOrderFieldName);
					}else{
						sortOrderTypeVal="1";
						sortOrderStrVal=Order.desc(sortOrderFieldName);
					}
				}else{
					sortOrderTypeVal="0";
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}
				/**End ------------------------------------**/


				listTeacherInvolvements = teacherInvolvementDAO.findSortedTeacherInvolvementByTeacher(sortOrderStrVal,teacherDetail);


				List<TeacherInvolvement> sortedlistTeacherInvolvement		=	new ArrayList<TeacherInvolvement>();

				SortedMap<String,TeacherInvolvement>	sortedMap = new TreeMap<String,TeacherInvolvement>();
				if(sortOrderNoField.equals("orgtype"))
				{
					sortOrderFieldName	=	"orgtype";
				}
				int mapFlag=2;
				for (TeacherInvolvement trInvolvement : listTeacherInvolvements){
					String orderFieldName=trInvolvement.getOrganization();
					if(sortOrderFieldName.equals("orgtype")){
						orderFieldName=trInvolvement.getOrgTypeMaster().getOrgType()+"||"+trInvolvement.getInvolvementId();
						sortedMap.put(orderFieldName+"||",trInvolvement);
						if(sortOrderTypeVal.equals("0")){
							mapFlag=0;
						}else{
							mapFlag=1;
						}
					}

				}
				if(mapFlag==1){
					NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet();  
					for (Iterator iter=navig.iterator();iter.hasNext();) {  
						Object key = iter.next(); 
						sortedlistTeacherInvolvement.add((TeacherInvolvement) sortedMap.get(key));
					} 
				}else if(mapFlag==0){
					Iterator iterator = sortedMap.keySet().iterator();
					while (iterator.hasNext()) {
						Object key = iterator.next();
						sortedlistTeacherInvolvement.add((TeacherInvolvement) sortedMap.get(key));
					}
				}else{
					sortedlistTeacherInvolvement=listTeacherInvolvements;
				}

				totalRecord =sortedlistTeacherInvolvement.size();

				if(totalRecord<end)
					end=totalRecord;
				List<TeacherInvolvement> listsortedTeacherCertificates		=	sortedlistTeacherInvolvement.subList(start,end);
				String responseText="";

				sb.append("<table border='0' id='involvementGrid' class='table table-striped' >");
				sb.append("<thead class='bg'>");			
				sb.append("<tr>");

				responseText=PaginationAndSorting.responseSortingLink(lblOrga,sortOrderFieldName,"organization",sortOrderTypeVal,pgNo);
				sb.append("<th width='20%' valign='top'>"+responseText+"</th>");

				responseText=PaginationAndSorting.responseSortingLink(lblType,sortOrderFieldName,"orgtype",sortOrderTypeVal,pgNo);
				sb.append("<th width='15%' valign='top'>"+responseText+"</th>");

				responseText=PaginationAndSorting.responseSortingLink(lblNumberofPeople,sortOrderFieldName,"peopleRangeMaster",sortOrderTypeVal,pgNo);
				sb.append("<th width='25%' valign='top'>"+responseText+"</th>");

				responseText=PaginationAndSorting.responseSortingLink(lblNumPeLed,sortOrderFieldName,"leadNoOfPeople",sortOrderTypeVal,pgNo);
				sb.append("<th width='25%' valign='top'>"+responseText+"</th>");

				sb.append("</tr>");
				sb.append("</thead>");
				if(listTeacherInvolvements!=null)
				{
					for(TeacherInvolvement tInv:listsortedTeacherCertificates)
					{
						sb.append("<tr>");
						sb.append("<td>");
						sb.append(tInv.getOrganization());
						sb.append("</td>");

						sb.append("<td>");
						sb.append(tInv.getOrgTypeMaster().getOrgType());
						sb.append("</td>");

						sb.append("<td>");
						sb.append(tInv.getPeopleRangeMaster().getRange());	
						sb.append("</td>");

						sb.append("<td>");
						sb.append(tInv.getLeadNoOfPeople()==null?"":tInv.getLeadNoOfPeople());	
						sb.append("</td>");
						
						
					}
					sb.append(PaginationAndSorting.getPaginationPaging(request,totalRecord,noOfRow, pageNo,"pageSize16"));
				}

				if(listTeacherInvolvements==null || listTeacherInvolvements.size()==0)
				{
					sb.append("<tr>");
					sb.append("<td colspan='7'>");
					sb.append(lblNoRecord);
					sb.append("</td>");
					sb.append("</tr>");
				}
				sb.append("</table>");
				
				
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			return sb.toString();	
		}
		
		public String getHonorsGridWithoutAction(Integer teacherId)
		{		
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}

			StringBuffer sb = new StringBuffer();		
			try 
			{
				TeacherDetail teacherDetail = teacherDetailDAO.findById(teacherId, false, false);
				
				List<TeacherHonor> listTeacherHonors = null;
				listTeacherHonors = teacherHonorDAO.findTeacherHonersByTeacher(teacherDetail);
				
				System.out.println("-----------------------------Start Honors Grid--------------------------------");
				System.out.println("listTeacherHonors:- "+listTeacherHonors+",teacherId:-  "+teacherId+", teacherDetail:- "+teacherDetail);
				System.out.println("-----------------------------End Honors Grid--------------------------------");
				
				String responseText="";

				sb.append("<table border='0' id='honorsGrid' class='table table-striped' >");
				sb.append("<thead class='bg'>");			
				sb.append("<tr>");

				//responseText=PaginationAndSorting.responseSortingLink("Organization",sortOrderFieldName,"organization",sortOrderTypeVal,pgNo);
				sb.append("<th valign='top'>"+lblHonors+"</th>");

				sb.append("</tr>");
				sb.append("</thead>");

				if(listTeacherHonors!=null)
					for(TeacherHonor tHon:listTeacherHonors)
					{
						sb.append("<tr>");
						sb.append("<td>"); 
							sb.append(tHon.getHonor()+" - "+tHon.getHonorYear());
						sb.append("</td>");
						sb.append("</tr>");
					}

				if(listTeacherHonors==null || listTeacherHonors.size()==0)
				{
					sb.append("<tr>");
					sb.append("<td>"+lblNoRecord+"</td>");
					sb.append("</tr>");
				}
				sb.append("</table>");
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			return sb.toString();	
		}
		
		public String getGridDataWorkExpEmploymentForOscela(Integer teacherId, String noOfRow, String pageNo,String sortOrder,String sortOrderType,String visitLocation)
		{		
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			UserMaster userMaster=null;
			DistrictMaster districtMaster=null;
			boolean nobleflag=false;
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		    }else{
			    userMaster=(UserMaster)session.getAttribute("userMaster");
			    districtMaster=userMaster.getDistrictId();
			   if(districtMaster!=null)
				   if(districtMaster.getDistrictId()==7800038)
					   nobleflag=true;
		    }
			
			

			StringBuffer sb = new StringBuffer();		
			try 
			{
				String locale = Utility.getValueOfPropByKey("locale");
				String lblNoRecord = Utility.getLocaleValuePropByKey("lblNoRecord", locale);
				String lblRole	 = Utility.getLocaleValuePropByKey("lblRole", locale);
				String lblPosition  = Utility.getLocaleValuePropByKey("lblPosition", locale);
				String lblOrgEmp  = Utility.getLocaleValuePropByKey("lblOrgEmp", locale);
				String lblCompanyName = Utility.getLocaleValuePropByKey("lblCompanyName", locale);
				String lblCompanyCity = Utility.getLocaleValuePropByKey("lblCompanyCity", locale);
				String lblCompanyState = Utility.getLocaleValuePropByKey("lblCompanyState", locale);				
				String lblDuration  = Utility.getLocaleValuePropByKey("lblDuration ", locale);
				String lblAnnualSalary  = Utility.getLocaleValuePropByKey("lblAnnualSalary", locale);
				String lblTypeOfRole  = Utility.getLocaleValuePropByKey("lblTypeOfRole", locale);
				String lblPrimaryResponsibility  = Utility.getLocaleValuePropByKey("lblPrimaryResponsibility", locale);
				String lblMostSignificantContributions  = Utility.getLocaleValuePropByKey("lblMostSignificantContributions ", locale);
				String lblReasonForLeaving  = Utility.getLocaleValuePropByKey("lblReasonForLeaving ", locale);
				String lblNoData  = Utility.getLocaleValuePropByKey("lblNoData", locale);
				String lblCity = Utility.getLocaleValuePropByKey("lblCity", locale);
				String lblState = Utility.getLocaleValuePropByKey("lblState", locale);
				String lblType = Utility.getLocaleValuePropByKey("lblType", locale);
				String lblMoreFields  = Utility.getLocaleValuePropByKey("lblMoreFields", locale);
				String lblReasons  = Utility.getLocaleValuePropByKey("lblReasons", locale);
				String lblMoreInfo  = Utility.getLocaleValuePropByKey("lblMoreInfo", locale);
				String lblStudentTeaching   = Utility.getLocaleValuePropByKey("lblStudentTeaching", locale);
				String lblFullTimeTeaching   = Utility.getLocaleValuePropByKey("lblFullTimeTeaching", locale);
				String lblSubstituteTeaching   = Utility.getLocaleValuePropByKey("lblSubstituteTeaching", locale);
				String lblOtherWorkExperience   = Utility.getLocaleValuePropByKey("lblOtherWorkExperience", locale);
				String lblToPresent   = Utility.getLocaleValuePropByKey("lblToPresent", locale);
				String lblResponsibility    = Utility.getLocaleValuePropByKey("lblResponsibility", locale);
				String lblContributions    = Utility.getLocaleValuePropByKey("lblContributions", locale);
				String lblReason    = Utility.getLocaleValuePropByKey("lblReason", locale);
				
				int noOfRowInPage 	= Integer.parseInt(noOfRow);
				int pgNo 			= Integer.parseInt(pageNo);
				int start 			= ((pgNo-1)*noOfRowInPage);
				int end 			= ((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
				int totalRecord 	= 0;
				//------------------------------------

				TeacherDetail teacherDetail = teacherDetailDAO.findById(teacherId, false, false);

				List<TeacherRole> listTeacherRole= null;
				/** set default sorting fieldName **/
				String sortOrderFieldName	=	"CreatedDateTime";
				String sortOrderNoField		=	"CreatedDateTime";

				/**Start set dynamic sorting fieldName **/
				Order  sortOrderStrVal=null;

				if(sortOrder!=null){
					if(!sortOrder.equals("") && !sortOrder.equals(null) && !sortOrder.equals("type") && !sortOrder.equals("empRoleTypeMaster") && !sortOrder.equals("position")){
						sortOrderFieldName=sortOrder;
						sortOrderNoField=sortOrder;
					}
					if(sortOrder.equals("type")){
						sortOrderNoField="type";
					}
					if(sortOrder.equals("empRoleTypeMaster")){
						sortOrderNoField="empRoleTypeMaster";
					}
				}

				String sortOrderTypeVal="0";
				if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
					if(sortOrderType.equals("0")){
						sortOrderStrVal=Order.asc(sortOrderFieldName);
					}else{
						sortOrderTypeVal="1";
						sortOrderStrVal=Order.desc(sortOrderFieldName);
					}
				}else{
					sortOrderTypeVal="0";
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}
				/**End ------------------------------------**/

				listTeacherRole = teacherRoleDAO.findSortedTeacherRoleByTeacher(sortOrderStrVal,teacherDetail);

				List<TeacherRole> sortedTeacherRole		=	new ArrayList<TeacherRole>();

				SortedMap<String,TeacherRole>	sortedMap = new TreeMap<String,TeacherRole>();
				if(sortOrderNoField.equals("type"))
				{
					sortOrderFieldName	=	"type";
				}
				if(sortOrderNoField.equals("empRoleTypeMaster"))
				{
					sortOrderFieldName	=	"empRoleTypeMaster";
				}
				if(sortOrderNoField.equals("empRoleTypeMaster"))
				{
					sortOrderFieldName	=	"empRoleTypeMaster";
				}
				int mapFlag=2;
				for (TeacherRole trRole : listTeacherRole){
					String orderFieldName=trRole.getRole();
					if(sortOrderFieldName.equals("type")){
						orderFieldName=trRole.getFieldMaster().getFieldName()+"||"+trRole.getRoleId();
						sortedMap.put(orderFieldName+"||",trRole);
						if(sortOrderTypeVal.equals("0")){
							mapFlag=0;
						}else{
							mapFlag=1;
						}
					}
					if(sortOrderFieldName.equals("empRoleTypeMaster")){
						orderFieldName=trRole.getEmpRoleTypeMaster().getEmpRoleTypeName()+"||"+trRole.getRoleId();
						sortedMap.put(orderFieldName+"||",trRole);
						if(sortOrderTypeVal.equals("0")){
							mapFlag=0;
						}else{
							mapFlag=1;
						}
					}
				}
				if(mapFlag==1){
					NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet();  
					for (Iterator iter=navig.iterator();iter.hasNext();) {  
						Object key = iter.next(); 
						sortedTeacherRole.add((TeacherRole) sortedMap.get(key));
					} 
				}else if(mapFlag==0){
					Iterator iterator = sortedMap.keySet().iterator();
					while (iterator.hasNext()) {
						Object key = iterator.next();
						sortedTeacherRole.add((TeacherRole) sortedMap.get(key));
					}
				}else{
					sortedTeacherRole=listTeacherRole;
				}

				totalRecord =listTeacherRole.size();

				if(totalRecord<end)
					end=totalRecord;
				List<TeacherRole> listsortedTeacherRole		=	sortedTeacherRole.subList(start,end);

				String responseText="";
				String tablePadding_Data="";
				String tablePadding_Data_right="";
				int iRecordCount=1;
				String refDivWidth="";
				if(userMaster.getEntityType().equals(2) || userMaster.getEntityType().equals(3))
				   refDivWidth="830";
				else
				   refDivWidth="730";			
				
				System.out.println("visitLocation   visitLocation  visitLocation   "+visitLocation);
				if(visitLocation.equalsIgnoreCase("Teacher Pool"))
				{
					tablePadding_Data="";
					tablePadding_Data_right="style='padding-right:10px;text-align:right;'";
					
					sb.append("<table border='0' id='tblWorkExp_Profile' class='table table-striped' onmouseover=\"javascript:setGridProfileVariable('workExp')\">");
					sb.append("<thead class='bg'>");			
					sb.append("<tr>");
		
					responseText=PaginationAndSorting.responseSortingMLink_2_TP(lblPosition,sortOrderFieldName,"position",sortOrderTypeVal,pgNo);
					sb.append("<th  valign='top'>"+responseText+"</th>");
					
					responseText=PaginationAndSorting.responseSortingMLink_2_TP(lblRole,sortOrderFieldName,"role",sortOrderTypeVal,pgNo);
					sb.append("<th  valign='top'>"+responseText+"</th>");
					
					if(districtMaster.getDistrictId()==3904493 )
					{
						responseText=PaginationAndSorting.responseSortingMLink_2_TP(lblCompanyName,sortOrderFieldName,"role",sortOrderTypeVal,pgNo);
						sb.append("<th  valign='top'>"+responseText+"</th>");
					}else{
						responseText=PaginationAndSorting.responseSortingMLink_2_TP(lblOrgEmp,sortOrderFieldName,"organization",sortOrderTypeVal,pgNo);
						sb.append("<th  valign='top'>"+responseText+"</th>");
					}
		
					responseText=PaginationAndSorting.responseSortingMLink_2_TP(lblType,sortOrderFieldName,"type",sortOrderTypeVal,pgNo);
					sb.append("<th  valign='top'>"+responseText+"</th>");
		
					responseText=PaginationAndSorting.responseSortingMLink_2_TP(lblDuration,sortOrderFieldName,"roleStartYear",sortOrderTypeVal,pgNo);
					sb.append("<th  valign='top'>"+responseText+"</th>");
					
					if(districtMaster!=null && !districtMaster.getDistrictId().equals(1201470)){
						responseText=PaginationAndSorting.responseSortingMLink_2_TP(lblAnnualSalary,sortOrderFieldName,"amount",sortOrderTypeVal,pgNo);
						sb.append("<th  valign='top'>"+responseText+"</th>");
					}
					responseText=PaginationAndSorting.responseSortingMLink_2_TP(lblTypeOfRole,sortOrderFieldName,"empRoleTypeMaster",sortOrderTypeVal,pgNo);
					sb.append("<th  valign='top'>"+responseText+"</th>");
					
					if(districtMaster.getDistrictId()==3904493 )
					{
						responseText=PaginationAndSorting.responseSortingMLink_2_TP(lblCompanyCity,sortOrderFieldName,"role",sortOrderTypeVal,pgNo);
						sb.append("<th  valign='top'>"+responseText+"</th>");
						
						responseText=PaginationAndSorting.responseSortingMLink_2_TP(lblCompanyState,sortOrderFieldName,"state",sortOrderTypeVal,pgNo);
						sb.append("<th  valign='top'>"+responseText+"</th>");
					}else{
					responseText=PaginationAndSorting.responseSortingMLink_2_TP(lblCity,sortOrderFieldName,"city",sortOrderTypeVal,pgNo);
					sb.append("<th  valign='top'>"+responseText+"</th>");
					
					responseText=PaginationAndSorting.responseSortingMLink_2_TP(lblState,sortOrderFieldName,"state",sortOrderTypeVal,pgNo);
					sb.append("<th  valign='top'>"+responseText+"</th>");
					}
					
					
					
					
					if(districtMaster!=null && !districtMaster.getDistrictId().equals(7800040)){
						/*sb.append("<th  valign='top'>Responsibility<a href='#' id='respTooltipleave' class='net-header-text ' rel='tooltip' data-original-title='Primary Responsibility in this Role'><span class='icon-question-sign'></span></a></th>");
						sb.append("<script>$('#respTooltipleave').tooltip();</script>");
						
						sb.append("<th  valign='top'>Responsibility<a href='#' id='respTooltipleave' class='net-header-text ' rel='tooltip' data-original-title='Primary Responsibility in this Role'><span class='icon-question-sign'></span></a></th>");
						sb.append("<script>$('#respTooltipleave').tooltip();</script>");*/
						
						sb.append("<th  valign='top'>"+lblMoreFields+"<a href='#' id='mostTooltipleave' class='net-header-text ' rel='tooltip' data-original-title='Most significant contributions in this role'></a></th>");
						//sb.append("<script>$('#mostTooltipleave').tooltip();</script>");
					}else{
						sb.append("<th  valign='top'>"+lblReasons+"<a href='#' id='reasonTooltipleave' class='net-header-text ' rel='tooltip' data-original-title='Reason For Leaving'><span class='icon-question-sign'></span></a></th>");
						sb.append("<script>$('#reasonTooltipleave').tooltip();</script>");
					}
				}
				else
				{
					tablePadding_Data="";
					tablePadding_Data_right="style='padding-right:10px;text-align:right;'";
					
					sb.append("<table border='0' id='tblWorkExp_Profile' class='table table-striped' onmouseover=\"javascript:setGridProfileVariable('workExp')\">");
					sb.append("<thead class='bg'>");			
					sb.append("<tr>");
					
					responseText=PaginationAndSorting.responseSortingMLink_1(lblPosition,sortOrderFieldName,"position",sortOrderTypeVal,pgNo);
					sb.append("<th  valign='top'>"+responseText+"</th>");
					
					responseText=PaginationAndSorting.responseSortingMLink_1(lblRole,sortOrderFieldName,"role",sortOrderTypeVal,pgNo);
					sb.append("<th  valign='top'>"+responseText+"</th>");
		
					responseText=PaginationAndSorting.responseSortingMLink_1(lblOrgEmp,sortOrderFieldName,"organization",sortOrderTypeVal,pgNo);
					sb.append("<th  valign='top'>"+responseText+"</th>");
					
					responseText=PaginationAndSorting.responseSortingMLink_1(lblType,sortOrderFieldName,"type",sortOrderTypeVal,pgNo);
					sb.append("<th  valign='top'>"+responseText+"</th>");
		
					responseText=PaginationAndSorting.responseSortingMLink_1(lblDuration,sortOrderFieldName,"roleStartYear",sortOrderTypeVal,pgNo);
					sb.append("<th  valign='top'>"+responseText+"</th>");
		
					if(districtMaster!=null && !districtMaster.getDistrictId().equals(1201470)){
						responseText=PaginationAndSorting.responseSortingMLink_1(lblAnnualSalary,sortOrderFieldName,"amount",sortOrderTypeVal,pgNo);
						sb.append("<th valign='top'>"+responseText+"</th>");
					}
		
					responseText=PaginationAndSorting.responseSortingMLink_1(lblTypeOfRole,sortOrderFieldName,"empRoleTypeMaster",sortOrderTypeVal,pgNo);
					sb.append("<th  valign='top'>"+responseText+"</th>");
					
					responseText=PaginationAndSorting.responseSortingMLink_1(lblCity,sortOrderFieldName,"city",sortOrderTypeVal,pgNo);
					sb.append("<th  valign='top'>"+responseText+"</th>");
					
					responseText=PaginationAndSorting.responseSortingMLink_1(lblState,sortOrderFieldName,"state",sortOrderTypeVal,pgNo);
					sb.append("<th  valign='top'>"+responseText+"</th>");
					
					if(districtMaster!=null && districtMaster.getDistrictId().equals(1201470)){
						/*sb.append("<th  valign='top'>Responsibility<a href='#' id='respTooltipleave' class='net-header-text ' rel='tooltip' data-original-title='Primary Responsibility in this Role'><span class='icon-question-sign'></span></a></th>");
						sb.append("<script>$('#respTooltipleave').tooltip();</script>");
						
						sb.append("<th  valign='top'>Responsibility<a href='#' id='respTooltipleave' class='net-header-text ' rel='tooltip' data-original-title='Primary Responsibility in this Role'><span class='icon-question-sign'></span></a></th>");
						sb.append("<script>$('#respTooltipleave').tooltip();</script>");*/
						
						sb.append("<th  valign='top'>"+lblMoreInfo+".<a href='#' id='mostTooltipleave' class='net-header-text ' rel='tooltip' data-original-title='"+lblMostSignificantContributions+"'></a></th>");
						//sb.append("<script>$('#mostTooltipleave').tooltip();</script>");
					}else{
						sb.append("<th  valign='top'>"+lblReasons+"<a href='#' id='reasonTooltipleave' class='net-header-text ' rel='tooltip' data-original-title='"+lblReasonForLeaving+"'><span class='icon-question-sign'></span></a></th>");
						sb.append("<script>$('#reasonTooltipleave').tooltip();</script>");
					}
				}
				
				sb.append("</tr>");
				sb.append("</thead>");
				if(listTeacherRole!=null)
				{
					for(TeacherRole tRole:listsortedTeacherRole)
					{
						iRecordCount++;
						
						if(tRole.getNoWorkExp()!=null && tRole.getNoWorkExp()==true)
						{
							sb.append("<tr>");
							sb.append("<td colspan='5'>");
							sb.append("I do not have any work experience.");
							sb.append("</td>");
							
							sb.append("<td>");

							sb.append("<a href='#' onclick=\"return showEditFormEmployment('"+tRole.getRoleId()+"')\" >Edit</a>");
							sb.append("&nbsp;|&nbsp;<a href='#' onclick=\"return deleteEmployment('"+tRole.getRoleId()+"')\">Delete</a>");
							sb.append("</td>");
							
							sb.append("</tr>");
						}
						else
						{
							String gridColor="";
							if(iRecordCount%2==0 && ( visitLocation.equalsIgnoreCase("My Folders") || visitLocation.equalsIgnoreCase("Mosaic"))){
								gridColor="class='bggrid'";
							}
							
							sb.append("<tr "+gridColor+">");
							
							sb.append("<td "+tablePadding_Data+">");
							if(tRole.getPosition()!=null){	
								if(tRole.getPosition().equals(1))
									sb.append("Student Teaching");
								else if(tRole.getPosition().equals(2))
									sb.append("Full-Time Teaching");
								else if(tRole.getPosition().equals(3))
									sb.append("Substitute Teaching");
								else if(tRole.getPosition().equals(4))
									sb.append("Other Work Experience");
							}
							sb.append("</td>");
							
							sb.append("<td "+tablePadding_Data+">");
							sb.append(tRole.getRole());
							sb.append("</td>");
							
							sb.append("<td "+tablePadding_Data+">");
							if(tRole.getOrganization()!=null)
								sb.append(tRole.getOrganization());
							else
								sb.append("");
							sb.append("</td>");

							sb.append("<td "+tablePadding_Data+">");
							sb.append(tRole.getFieldMaster().getFieldName());
							sb.append("</td>");

							sb.append("<td "+tablePadding_Data+">");

							if(tRole.getCurrentlyWorking())
							{
								sb.append(tRole.getRoleStartYear()+" to Present");
							}	
							else
							{
								sb.append(tRole.getRoleStartYear()+" to "+tRole.getRoleEndYear());
							}
							sb.append("</td>");
							String currency="";
							String amount="";
							if(tRole.getCurrencyMaster()!=null){
								currency=tRole.getCurrencyMaster().getCurrencyShortName();
							}
							if(tRole.getAmount()!=null){
								amount=tRole.getAmount()+"";
							}
							if(districtMaster!=null && !districtMaster.getDistrictId().equals(1201470)){
								sb.append("<td "+tablePadding_Data_right+">");
								sb.append(currency+""+amount);
								sb.append("</td>");
							}
							if(!nobleflag){
								sb.append("<td "+tablePadding_Data+" >");
								if(tRole.getEmpRoleTypeMaster()!=null)
								sb.append(tRole.getEmpRoleTypeMaster().getEmpRoleTypeName());	
							}else{
								sb.append("<td colspan='2' "+tablePadding_Data+" >");
								if(tRole.getEmpRoleTypeMaster()!=null)
								sb.append(tRole.getEmpRoleTypeMaster().getEmpRoleTypeName());	
							}
							sb.append("</td>");
							
							sb.append("<td>");
							if(tRole.getCity()!=null)
							sb.append(tRole.getCity());
							sb.append("</td>");
							
							sb.append("<td>");
							if(tRole.getState()!=null)
							sb.append(tRole.getState());
							sb.append("</td>");
							
							sb.append("<td>");
							if(districtMaster!=null && districtMaster.getDistrictId().equals(1201470)){
								sb.append("<a id='"+tRole.getRoleId()+"' rel='tooltip' data-original-title='Primary Responsibility in this Role' class='respTooltipleave' href='javascript:void(0)' onclick=\"return openDynmicDivContent("+tRole.getRoleId()+",'responsiTextContent');\">Responsibility</a><br>");
								sb.append("<a id='"+tRole.getRoleId()+"' rel='tooltip' data-original-title='Most significant contributions in this' class='mostTooltipleave' href='javascript:void(0)' onclick=\"return openDynmicDivContent("+tRole.getRoleId()+",'contributesTextContent');\">Contributions</a><br>");
								sb.append("<a id='"+tRole.getRoleId()+"' class='resoTooltipleave' rel='tooltip' data-original-title='Reason for leaving' href='javascript:void(0)' onclick=\"return openDynmicDivContent("+tRole.getRoleId()+",'reasonTextContent');\">Reason</a>");
								sb.append("<script>$('.mostTooltipleave').tooltip();</script>");
								sb.append("<script>$('.respTooltipleave').tooltip();</script>");
								sb.append("<script>$('.resoTooltipleave').tooltip();</script>");
							}else{
								String certHtml="";
								if(tRole.getReasonForLeaving()!=null && !tRole.getReasonForLeaving().equalsIgnoreCase("")){
									certHtml = html2text(tRole.getReasonForLeaving());
									String[] wordArray = certHtml.split("\\s+");
									int wordCount = wordArray.length;
									
									if(wordCount>2){
									sb.append("<a id='"+tRole.getRoleId()+"' href='javascript:void(0)' onclick=\"return openDynmicDivContent("+tRole.getRoleId()+",'reasonTextContent');\">");					
									
									if(certHtml.length()>20){
										sb.append(htmlwords(certHtml)+"...");
									}
									sb.append("</a>");
									}else{
										sb.append(certHtml);
									}
									/*sb.append("<input type='hidden' value='"+tRole.getReasonForLeaving()+"' id='reasonTextContent"+tRole.getRoleId()+"'>" );*/
									sb.append("<div class='hide' id='reasonTextContent"+tRole.getRoleId()+"'>"+tRole.getReasonForLeaving()+"</div>");
								}else{
									sb.append("");							
								}
							}
							String msc=lblNoData+".";
							String resp=lblNoData+".";
							String rea=lblNoData+".";
							if(tRole.getMostSignCont()!=null && !tRole.getMostSignCont().equalsIgnoreCase("")){
								msc=tRole.getMostSignCont();
								System.out.println();
							}
							if(tRole.getReasonForLeaving()!=null && !tRole.getReasonForLeaving().equalsIgnoreCase("")){
								rea=tRole.getReasonForLeaving();
							}
							if(tRole.getPrimaryResp()!=null && !tRole.getPrimaryResp().equalsIgnoreCase("")){
								resp=tRole.getPrimaryResp();
							}
							sb.append("<div id='contributesTextContent"+tRole.getRoleId()+"' class='hide'>"+msc+"</div>");
							sb.append("<div class='hide' id='reasonTextContent"+tRole.getRoleId()+"'>"+rea+"</div>");
							sb.append("<div class='hide' id='responsiTextContent"+tRole.getRoleId()+"'>"+resp+"</div>");
							sb.append("</td>");
							
							

							sb.append("</tr>");
						}
						
						
					}
					
					
					if(totalRecord>10 && ( visitLocation.equalsIgnoreCase("My Folders") || visitLocation.equalsIgnoreCase("Mosaic"))){
						sb.append("<tr><td colspan='5' style='border-top: 1px solid #cccccc;padding-bottom:0px;padding-top:5px;'>");
						sb.append(PaginationAndSorting.getPagination_pageSizeVariableName(request,totalRecord,noOfRow, pageNo,"pageSize11","730"));
						sb.append("</td></tr>");
					}
				}

				if(listsortedTeacherRole==null || listsortedTeacherRole.size()==0)
				{
					if(visitLocation.equalsIgnoreCase("My Folders") || visitLocation.equalsIgnoreCase("Mosaic"))
					{
						tablePadding_Data="style='padding-left:10px;vertical-align:top;text-align:left;font-weight: normal;line-height:30px;'";
						sb.append("<tr><td "+tablePadding_Data+" colspan='5'>"+lblNoRecord+"</td></tr>" );
					}
					else
					{
						sb.append("<tr>");
						sb.append("<td colspan='5'>");
						sb.append(lblNoRecord);
						sb.append("</td>");
						sb.append("</tr>");
					}
				}
				
				sb.append("</table>");
				if(!visitLocation.equalsIgnoreCase("My Folders") && !visitLocation.equalsIgnoreCase("Mosaic"))
				{
					//sb.append(PaginationAndSorting.getPagination_pageSizeVariableName(request,totalRecord,noOfRow, pageNo,"pageSize11","800"));
					sb.append(PaginationAndSorting.getPaginationPaging(request,totalRecord,noOfRow, pageNo,"pageSize11"));
				}

			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			return sb.toString();	
		}
		
		// Teacher Academics for Oscela
		//@Transactional(readOnly=true)
		public String getTeacherAcademicsGridOsecola(Integer teacherId, String noOfRow, String pageNo,String sortOrder,String sortOrderType,String visitLocation)
		{		
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			UserMaster userMaster=null;
			DistrictMaster districtMaster=null;
			int roleId=0;
			boolean nobleflag=false;
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}else{
			    userMaster=(UserMaster)session.getAttribute("userMaster");
			    districtMaster=userMaster.getDistrictId();
			    if(districtMaster!=null)
			    	if(districtMaster.getDistrictId()==7800038)
			    		nobleflag=true;
			    
			 }
			
			if(userMaster.getRoleId().getRoleId()!=null)
				roleId=userMaster.getRoleId().getRoleId();

			StringBuffer sb = new StringBuffer();		
			try 
			{
				String locale = Utility.getValueOfPropByKey("locale");
				String lblNoRecord = Utility.getLocaleValuePropByKey("lblNoRecord", locale);
				String lblSchool = Utility.getLocaleValuePropByKey("lblSchool", locale);
				String lblDatesAttended  = Utility.getLocaleValuePropByKey("lblDatesAttended", locale);
				String lblDegree  = Utility.getLocaleValuePropByKey("lblDegree", locale);
				String lblFieldOfStudy  = Utility.getLocaleValuePropByKey("lblFieldOfStudy", locale);
				String lblTranscript  = Utility.getLocaleValuePropByKey("lblTranscript", locale);
				String lblGPA  = Utility.getLocaleValuePropByKey("lblGPA", locale);
				String lblFreshman = Utility.getLocaleValuePropByKey("lblFreshman", locale);
				String lblSophomore  = Utility.getLocaleValuePropByKey("lblSophomore", locale);
				String lblJunior  = Utility.getLocaleValuePropByKey("lblJunior", locale);
				String lblSenior  = Utility.getLocaleValuePropByKey("lblSenior", locale);
				String lblCumulative  = Utility.getLocaleValuePropByKey("lblCumulative", locale);
				//String lblDegree = Utility.getLocaleValuePropByKey("lblSchool", locale);
				
				
				int noOfRowInPage 	= Integer.parseInt(noOfRow);
				int pgNo 			= Integer.parseInt(pageNo);
				int start 			= ((pgNo-1)*noOfRowInPage);
				int end 			= ((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
				int totalRecord 	= 0;
				//------------------------------------

				TeacherDetail teacherDetail = teacherDetailDAO.findById(teacherId, false, false);

				//List<TeacherRole> listTeacherRole= null;
				/** set default sorting fieldName **/
				String sortOrderFieldName	=	"attendedInYear";
				String sortOrderNoField		=	"attendedInYear";

				/**Start set dynamic sorting fieldName **/
				Order  sortOrderStrVal=null;

				if(sortOrder!=null){
					if(!sortOrder.equals("") && !sortOrder.equals(null) && !sortOrder.equals("Education") && !sortOrder.equals("fieldOfStudy")){
						sortOrderFieldName=sortOrder;
						sortOrderNoField=sortOrder;
					}
					if(sortOrder.equals("degree")){
						sortOrderNoField="degree";
					}
					if(sortOrder.equals("fieldOfStudy")){
						sortOrderNoField="fieldOfStudy";
					}
				}

				String sortOrderTypeVal="0";
				if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
					if(sortOrderType.equals("0")){
						sortOrderStrVal=Order.asc(sortOrderFieldName);
					}else{
						sortOrderTypeVal="1";
						sortOrderStrVal=Order.desc(sortOrderFieldName);
					}
				}else{
					sortOrderTypeVal="0";
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}
				/**End ------------------------------------**/

				Criterion criterion = Restrictions.eq("teacherId",teacherDetail);
				List<TeacherAcademics>  lstTeacherAcademics = teacherAcademicsDAO.findByCriteria(sortOrderStrVal,criterion);
				
				List<TeacherAcademics> sortedTeacherAcademics		=	new ArrayList<TeacherAcademics>();

				SortedMap<String,TeacherAcademics>	sortedMap = new TreeMap<String,TeacherAcademics>();
				if(sortOrderNoField.equals("degree"))
				{
					sortOrderFieldName	=	"degree";
				}
				if(sortOrderNoField.equals("fieldOfStudy"))
				{
					sortOrderFieldName	=	"fieldOfStudy";
				}
				if(sortOrderNoField.equals("university"))
				{
					sortOrderFieldName	=	"university";
				}
				int mapFlag=2;
				for (TeacherAcademics pojo : lstTeacherAcademics){
					String orderFieldName=""+pojo.getAcademicId();
					if(sortOrderFieldName.equals("degree")){
						orderFieldName=pojo.getDegreeId().getDegreeName()+"||"+pojo.getAcademicId();
						sortedMap.put(orderFieldName+"||",pojo);
						if(sortOrderTypeVal.equals("0")){
							mapFlag=0;
						}else{
							mapFlag=1;
						}
					}
					if(sortOrderFieldName.equals("fieldOfStudy")){
						orderFieldName=pojo.getFieldId().getFieldName()+"||"+pojo.getAcademicId();
						sortedMap.put(orderFieldName+"||",pojo);
						if(sortOrderTypeVal.equals("0")){
							mapFlag=0;
						}else{
							mapFlag=1;
						}
					}
					if(sortOrderFieldName.equals("university")){
						orderFieldName=pojo.getUniversityId().getUniversityName()+"||"+pojo.getAcademicId();
						sortedMap.put(orderFieldName+"||",pojo);
						if(sortOrderTypeVal.equals("0")){
							mapFlag=0;
						}else{
							mapFlag=1;
						}
					}
				}
				if(mapFlag==1){
					NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet();  
					for (Iterator iter=navig.iterator();iter.hasNext();) {  
						Object key = iter.next(); 
						sortedTeacherAcademics.add((TeacherAcademics) sortedMap.get(key));
					} 
				}else if(mapFlag==0){
					Iterator iterator = sortedMap.keySet().iterator();
					while (iterator.hasNext()) {
						Object key = iterator.next();
						sortedTeacherAcademics.add((TeacherAcademics) sortedMap.get(key));
					}
				}else{
					sortedTeacherAcademics=lstTeacherAcademics;
				}

				totalRecord =lstTeacherAcademics.size();

				if(totalRecord<end)
					end=totalRecord;
				List<TeacherAcademics> listsortedTeacherAcademics		=	sortedTeacherAcademics.subList(start,end);
				
				String responseText="";
				String tablePadding_Data="";
				String tablePadding_Data_right="";
				int iRecordCount=1;
				String refDivWidth="";
				if(userMaster.getEntityType().equals(2) || userMaster.getEntityType().equals(3))
				  refDivWidth="925";
				else
				  refDivWidth="730";			
				if(visitLocation.equalsIgnoreCase("My Folders") || visitLocation.equalsIgnoreCase("Mosaic"))
				{
					tablePadding_Data="style='padding-left:10px;vertical-align:top;text-align:left;font-weight: normal;line-height:30px;'";
					tablePadding_Data_right="style='padding-right:10px;vertical-align:top;text-align:right;font-weight: normal;line-height:30px;'";
					
					String tablePadding="style='padding-left:5px;vertical-align:top;color:#FFFFFF;line-height:25px;'";
					sb.append("<table id='tblTeacherAcademics_Profile' border='0'  width='"+refDivWidth+"' class='table-bordered' onmouseover=\"javascript:setGridProfileVariable('teacherAce')\">");
					sb.append("<thead class='bg'>");
					sb.append("<tr>");
					
					responseText=PaginationAndSorting.responseSortingMLink_1(lblSchool,sortOrderFieldName,"universityId",sortOrderTypeVal,pgNo);
					sb.append("<th align=\"left\" width='200' "+tablePadding+" >"+responseText+"</th>");
					
					responseText=PaginationAndSorting.responseSortingMLink_1(lblDatesAttended,sortOrderFieldName,"attendedInYear",sortOrderTypeVal,pgNo);
					sb.append("<th align=\"left\" width='50' "+tablePadding+" >"+responseText+"</th>");
					
					responseText=PaginationAndSorting.responseSortingMLink_1(lblDegree,sortOrderFieldName,"degreeId",sortOrderTypeVal,pgNo);
					sb.append("<th align=\"left\" width='200' "+tablePadding+" >"+responseText+"</th>");
		
					
					
					sb.append("<th width='100' class='net-header-text' "+tablePadding+">");
					sb.append(lblGPA);
					sb.append("</th>");
					
					responseText=PaginationAndSorting.responseSortingMLink_1(lblFieldOfStudy,sortOrderFieldName,"fieldOfStudy",sortOrderTypeVal,pgNo);
					sb.append("<th align=\"left\" width='150' "+tablePadding+" >"+responseText+"</th>");
					
					if(!nobleflag){
						responseText=PaginationAndSorting.responseSortingMLink_1(lblTranscript,sortOrderFieldName,"pathOfTranscript",sortOrderTypeVal,pgNo);
						sb.append("<th align=\"left\" width='100' "+tablePadding+" >"+responseText+"</th>");
					}
				}
				else if(visitLocation.equalsIgnoreCase("Teacher Pool"))
				{
					tablePadding_Data="";
					tablePadding_Data_right="style='padding-right:115px;text-align:right;'";
					
					sb.append("<table border='0' id='tblTeacherAcademics_Profile' class='table table-striped' onmouseover=\"javascript:setGridProfileVariable('teacherAce')\">");
					sb.append("<thead class='bg'>");			
					sb.append("<tr>");
		
					responseText=PaginationAndSorting.responseSortingMLink_2_TP(lblSchool,sortOrderFieldName,"universityId",sortOrderTypeVal,pgNo);
					sb.append("<th>"+responseText+"</th>");				
		
					responseText=PaginationAndSorting.responseSortingMLink_2_TP(lblDatesAttended,sortOrderFieldName,"attendedInYear",sortOrderTypeVal,pgNo);
					sb.append("<th>"+responseText+"</th>");
					
					responseText=PaginationAndSorting.responseSortingMLink_2_TP(lblDegree,sortOrderFieldName,"degreeId",sortOrderTypeVal,pgNo);
					sb.append("<th>"+responseText+"</th>");
					
					sb.append("<th>");
					sb.append(lblGPA);
					sb.append("</th>");
					
					if(!nobleflag){
					responseText=PaginationAndSorting.responseSortingMLink_1("Field&nbsp;of&nbsp;Study",sortOrderFieldName,"fieldOfStudy",sortOrderTypeVal,pgNo);
					sb.append("<th>"+responseText+"</th>");
					}else{
					responseText=PaginationAndSorting.responseSortingMLink_1(lblFieldOfStudy,sortOrderFieldName,"fieldOfStudy",sortOrderTypeVal,pgNo);
					sb.append("<th colspan='2'>"+responseText+"</th>");
					}
					if(!nobleflag){
					responseText=PaginationAndSorting.responseSortingMLink_2_TP(lblTranscript,sortOrderFieldName,"pathOfTranscript",sortOrderTypeVal,pgNo);
					sb.append("<th>"+responseText+"</th>");
					}
				}
				else
				{
					tablePadding_Data="";
					tablePadding_Data_right="style='padding-right:110px;text-align:right;'";
					
					sb.append("<table border='0' id='tblTeacherAcademics_Profile' class='table table-striped' onmouseover=\"javascript:setGridProfileVariable('teacherAce')\">");
					sb.append("<thead class='bg'>");			
					sb.append("<tr>");
					responseText=PaginationAndSorting.responseSortingMLink_1(lblSchool,sortOrderFieldName,"universityId",sortOrderTypeVal,pgNo);
					sb.append("<th>"+responseText+"</th>");				
		
					responseText=PaginationAndSorting.responseSortingMLink_1(lblDatesAttended,sortOrderFieldName,"attendedInYear",sortOrderTypeVal,pgNo);
					sb.append("<th>"+responseText+"</th>");
					responseText=PaginationAndSorting.responseSortingMLink_1(lblDegree,sortOrderFieldName,"degreeId",sortOrderTypeVal,pgNo);
					sb.append("<th>"+responseText+"</th>");
		
					sb.append("<th>");
					sb.append(lblGPA);
					sb.append("</th>");
					
					if(!nobleflag){
					responseText=PaginationAndSorting.responseSortingMLink_1(lblFieldOfStudy,sortOrderFieldName,"fieldOfStudy",sortOrderTypeVal,pgNo);
					sb.append("<th>"+responseText+"</th>");
					}else{
						responseText=PaginationAndSorting.responseSortingMLink_1(lblFieldOfStudy,sortOrderFieldName,"fieldOfStudy",sortOrderTypeVal,pgNo);
						sb.append("<th colspan='2'>"+responseText+"</th>");
					}
					if(!nobleflag){
					responseText=PaginationAndSorting.responseSortingMLink_1(lblTranscript,sortOrderFieldName,"pathOfTranscript",sortOrderTypeVal,pgNo);
					sb.append("<th>"+responseText+"</th>");
					}
				}
				
				sb.append("</tr>");
				sb.append("</thead>");
				
				
				if(lstTeacherAcademics!=null)
				{
					for(TeacherAcademics pojo:listsortedTeacherAcademics)
					{
						iRecordCount++;
						String gridColor="";
						if(iRecordCount%2==0 && ( visitLocation.equalsIgnoreCase("My Folders") || visitLocation.equalsIgnoreCase("Mosaic"))){
							gridColor="class='bggrid'";
						}
						
						String degree="";
						String universityName="";
						String fieldName="";
						if(pojo.getDegreeId()!=null && pojo.getDegreeId().getDegreeShortName()!=null){
							degree=pojo.getDegreeId().getDegreeName();
						}
						if(pojo.getUniversityId()!=null && pojo.getUniversityId().getUniversityName()!=null){
							universityName=pojo.getUniversityId().getUniversityName();
						}
						if(pojo.getFieldId()!=null && pojo.getFieldId().getFieldName()!=null){
							fieldName=pojo.getFieldId().getFieldName();	
						}
						
						sb.append("<tr "+gridColor+">");
						
						sb.append("<td "+tablePadding_Data+">");
						sb.append(universityName);
						sb.append("</td>");

						sb.append("<td "+tablePadding_Data+">");
						
						if(pojo.getAttendedInYear()!=null && !pojo.getAttendedInYear().equals(""))
							sb.append(pojo.getAttendedInYear());
						if(pojo.getAttendedInYear()!=null && !pojo.getAttendedInYear().equals("") && pojo.getLeftInYear()!=null && !pojo.getLeftInYear().equals(""))
							sb.append(" to ");
							if(pojo.getLeftInYear()!=null && !pojo.getLeftInYear().equals(""))
							sb.append(pojo.getLeftInYear());
						
						sb.append("</td>");
						
						sb.append("<td "+tablePadding_Data+">");
						sb.append(degree);
						sb.append("</td>");
						
						sb.append("<td "+tablePadding_Data+">");
						if(pojo.getDegreeId().getDegreeType().trim().equalsIgnoreCase("b"))
						{
							sb.append(""+lblFreshman+": "+(pojo.getGpaFreshmanYear()==null?"":Utility.roundTwoDecimalsAsString(pojo.getGpaFreshmanYear()))+"<br> " +
								" "+lblSophomore+": "+(pojo.getGpaSophomoreYear()==null?"":Utility.roundTwoDecimalsAsString(pojo.getGpaSophomoreYear()))+"<br>" +
								" "+lblJunior+": "+(pojo.getGpaJuniorYear()==null?"":Utility.roundTwoDecimalsAsString(pojo.getGpaJuniorYear()))+"<br>" +
								" "+lblSenior+": "+(pojo.getGpaSeniorYear()==null?"":Utility.roundTwoDecimalsAsString(pojo.getGpaSeniorYear()))+"<br>" +
								" "+lblCumulative+": "+(pojo.getGpaCumulative()==null?"":Utility.roundTwoDecimalsAsString(pojo.getGpaCumulative())));
						}else{
							sb.append(""+lblCumulative+":"+(pojo.getGpaCumulative()==null?"":Utility.roundTwoDecimalsAsString(pojo.getGpaCumulative())));
						}
						sb.append("</td>");
						
						if(!nobleflag){	
							sb.append("<td "+tablePadding_Data+">");
							sb.append(fieldName);
							sb.append("</td>");
						}else{
							sb.append("<td colspan='2' "+tablePadding_Data+">");
							sb.append(fieldName);
							sb.append("</td>");
							
						}
					if(!nobleflag){	
						String windowFunc="if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
						sb.append("<td align=\"left\">");
						/*if(visitLocation.equalsIgnoreCase("Mosaic"))
							sb.append(pojo.getPathOfTranscript()==null?"":"<a href='javascript:void(0)' rel='tooltip' data-original-title='Click here to view transcript file !'  id='trans"+pojo.getAcademicId()+"' onclick=\"downloadTranscriptNew('"+pojo.getAcademicId()+"','trans"+pojo.getAcademicId()+"');"+windowFunc+"\">"+pojo.getPathOfTranscript()+"</a>");
						else*/
							sb.append(pojo.getPathOfTranscript()==null?"":"<a href='javascript:void(0)' rel='tooltip' data-original-title='"+pojo.getPathOfTranscript()+"'  id='trans"+pojo.getAcademicId()+"' onclick=\"downloadTranscript('"+pojo.getAcademicId()+"','trans"+pojo.getAcademicId()+"');"+windowFunc+"\"><span class='icon-download-alt icon-large iconcolorBlue' style='padding-left:10px;'></span></a>");
						
						sb.append("<script>$('#trans"+pojo.getAcademicId()+"').tooltip();</script>");
						sb.append("</td>");
					}
						sb.append("</tr>");
					}
					
					if(totalRecord>10 && ( visitLocation.equalsIgnoreCase("My Folders") || visitLocation.equalsIgnoreCase("Mosaic"))){
						sb.append("<tr><td colspan='4' style='border-top: 1px solid #cccccc;padding-bottom:0px;padding-top:5px;line-height:30px;'>");
						sb.append(PaginationAndSorting.getPagination_pageSizeVariableName(request,totalRecord,noOfRow, pageNo,"pageSize10","730"));
						sb.append("</td></tr>");
					}
				}

				if(listsortedTeacherAcademics==null || listsortedTeacherAcademics.size()==0)
				{
					
					if(visitLocation.equalsIgnoreCase("My Folders") || visitLocation.equalsIgnoreCase("Mosaic"))
					{
						tablePadding_Data="style='padding-left:10px;vertical-align:top;text-align:left;font-weight: normal;line-height:30px;'";
						sb.append("<tr><td "+tablePadding_Data+" colspan='4'>"+lblNoRecord+".</td></tr>" );
					}
					else
					{
						sb.append("<tr>");
						sb.append("<td colspan='4'>");
						sb.append(lblNoRecord+".");
						sb.append("</td>");
						sb.append("</tr>");
					}
				}
				
				sb.append("</table>");
				if(!visitLocation.equalsIgnoreCase("My Folders") && !visitLocation.equalsIgnoreCase("Mosaic")){
					//sb.append(PaginationAndSorting.getPagination_pageSizeVariableName(request,totalRecord,noOfRow, pageNo,"pageSize10","800"));
					sb.append(PaginationAndSorting.getPaginationPaging(request,totalRecord,noOfRow, pageNo,"pageSize10"));
				}
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			return sb.toString();		
		}
		
		public OctDetails getOctDetails(TeacherDetail teacherDetail){
			OctDetails octDetails = null;//new OctDetails();
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}
			try {
				//TeacherDetail teacherDetail = teacherDetail;
					Criterion criterion = Restrictions.eq("teacherId", teacherDetail);				
					octDetails = octDetailsDAO.findByCriteria(criterion).get(0);
			} catch (Exception e) {
			}
			return octDetails;
		}
		
	public String downloadOctUpload(Integer teacherId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		String path="";

		try 
		{
			TeacherDetail teacherDetail = null;//(TeacherDetail) session.getAttribute("teacherDetail");
			
			if(teacherId!=null && teacherId!=0){
				teacherDetail = teacherDetailDAO.findById(teacherId, false, false);
			}
			Criterion criterion = Restrictions.eq("teacherId", teacherDetail);				
			OctDetails octDetails = octDetailsDAO.findByCriteria(criterion).get(0);
			System.out.println("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
			if(octDetails!=null)
			{

				 String source = Utility.getValueOfPropByKey("teacherRootPath")+""+teacherDetail.getTeacherId()+"/"+octDetails.getOctFileName();
			     String target = context.getServletContext().getRealPath("/")+"/"+"/teacher/"+teacherDetail.getTeacherId()+"/";
			     
			     File sourceFile = new File(source);
			     File targetDir = new File(target);
			     if(!targetDir.exists())
			    	 targetDir.mkdirs();
			     
			     File targetFile = new File(targetDir+"/"+sourceFile.getName());
			     
			     if(sourceFile.exists()){
			       	FileUtils.copyFile(sourceFile, targetFile);
			       	path =  Utility.getValueOfPropByKey("contextBasePath")+"/teacher/"+teacherDetail.getTeacherId()+"/"+sourceFile.getName();
			     }
			     System.out.println("source=="+source);
			     System.out.println("target=="+target);
			     System.out.println("path=="+path);
			
			}
			else
				System.out.println("oct is null or Blank");
		}
		catch (Exception e) 
		{
			System.out.println("Error in Answer");
			e.printStackTrace();
		}
		return path;
	}
		
		public String getMemberQualification(String RegistrationId)
		{
			try {
				Object[] returnObject =  new MemberQualificationClient().getMemberQualification(RegistrationId);
				StringBuffer sb = new StringBuffer();
				List<Member> memberList = (ArrayList<Member>)returnObject[0];
				List<BasicQualification> basicQualificationList = (ArrayList<BasicQualification>)returnObject[1];
				List<AdditionalQualifications> additionalQualificationList = (ArrayList<AdditionalQualifications>)returnObject[2];
				try {
					
					String sGridLabelCss="padding:0px;padding-top:10px;color:black;";
					sb.append("<table><tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'>"+LBLMemberDetail1+"</td></tr></table>");
					
					sb.append("<table border='0' id='octMemberDetail' width='850' class='table-bordered'>");
					sb.append("<thead class='bg'>");
					sb.append("<tr>");
					sb.append("<th align=\"left\" >");
					sb.append(LBLRegistrationId1);

					sb.append("</th>");
					sb.append("<th align=\"left\" >");
					sb.append(lblFname);
					sb.append("</th>");
					sb.append("<th align=\"left\" >");
	
					sb.append(LBLSurname1);
					sb.append("</th>");
					sb.append("<th align=\"left\" >");
					sb.append(LBLProfessionalDesignationEnglish);
					sb.append("</th>");
					sb.append("<th align=\"left\" >");
					sb.append(LBLProfessionalDesignationFrench);
					sb.append("</th>");
					sb.append("</tr>");
					sb.append("</thead>");
					for (Member member : memberList) {
						sb.append("<tr>");
						sb.append("<td>");
						sb.append(member.getRegistrationId());
						sb.append("</td>");
						sb.append("<td>");
						sb.append(member.getFirstName()==null?"":member.getFirstName());
						sb.append("</td>");
						sb.append("<td>");
						sb.append(member.getSurname()==null?"":member.getSurname());
						sb.append("</td>");
						sb.append("<td>");
						sb.append(member.getProfessionalDesignationEnglish()==null?"":member.getProfessionalDesignationEnglish());
						sb.append("</td>");
						sb.append("<td>");
						sb.append(member.getProfessionalDesignationFrench()==null?"":member.getProfessionalDesignationFrench());
						sb.append("</td>");
						sb.append("</tr>");	
					}
					sb.append("</table>");
					
					sb.append("<table><tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'>"+ Utility.getLocaleValuePropByKey("LBLbasicQualifaction", locale)+"</td></tr></table>");
					sb.append("<table border='0' id='octMemberBasic' width='850' class='table-bordered'>");
					sb.append("<thead class='bg'>");
					sb.append("<tr>");
					sb.append("<th align=\"left\" >");
					sb.append(LBLDivision1);
					sb.append("</th>");
					sb.append("<th align=\"left\" >");
					sb.append(lblSubject);
					sb.append("</th>");
					sb.append("<th align=\"left\" >");
					sb.append(Institution);
					sb.append("</th>");
					sb.append("<th align=\"left\" >");
					sb.append(LBLEffectiveDate);
					sb.append("</th>");
					sb.append("<th align=\"left\" >");
					sb.append(LBLSubjectCode);
					sb.append("</th>");
					sb.append("<th align=\"left\" >");
					sb.append(lblDivisionCode1);
					sb.append("</th>");
					sb.append("</tr>");	
					sb.append("</thead>");
					if(basicQualificationList.size()>0){
					for (BasicQualification basicQualification : basicQualificationList) {
						sb.append("<tr>");
						sb.append("<td>");
						sb.append(basicQualification.getDivision()==null?"":basicQualification.getDivision());
						sb.append("</td>");
						sb.append("<td>");
						sb.append(basicQualification.getSubject()==null?"":basicQualification.getSubject());
						sb.append("</td>");
						sb.append("<td>");
						sb.append(basicQualification.getInstitution()==null?"":basicQualification.getInstitution());
						sb.append("</td>");
						sb.append("<td>");
						sb.append(basicQualification.getEffectiveDate()==null?"":basicQualification.getEffectiveDate());
						sb.append("</td>");
						sb.append("<td>");
						sb.append(basicQualification.getSubjectCode()==null?"":basicQualification.getSubjectCode());
						sb.append("</td>");
						sb.append("<td>");
						sb.append(basicQualification.getDivisionCode()==null?"":basicQualification.getDivisionCode());
						sb.append("</td>");
						sb.append("</tr>");
						
						//lblNoRecord
					}
					}else{
						sb.append("</tr>");
						sb.append("<td>");
						sb.append(Utility.getLocaleValuePropByKey("msgNorecordfound", locale));
						sb.append("</td>");
						sb.append("</tr>");
					}
					sb.append("</table>");
					
					
					sb.append("<table><tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'>"+Utility.getLocaleValuePropByKey("LBLAddQualifaction", locale)+"</td></tr></table>");
					
					sb.append("<table border='0' id='octMemberAdditional' width='850' class='table-bordered'>");
					sb.append("<thead class='bg'>");
					sb.append("<tr>");
					sb.append("<th align=\"left\" >");
					sb.append(lblSubject);
					sb.append("</th>");
					sb.append("<th align=\"left\" >");
					sb.append(lblOption1);
					sb.append("</th>");
					sb.append("<th align=\"left\" >");
					sb.append(Institution);
					sb.append("</th>");
					sb.append("<th align=\"left\" >");
					sb.append(LBLEffectiveDate);
					sb.append("</th>");
					sb.append("<th align=\"left\" >");
					sb.append(LBLSubjectCode);
					sb.append("</th>");
					sb.append("<th align=\"left\" >");
					sb.append(lblOptionCode1);
					sb.append("</th>");
					sb.append("</tr>");
					sb.append("</thead>");
					if(additionalQualificationList.size()>0){
					for (AdditionalQualifications additionalQualifications : additionalQualificationList) {
						
						sb.append("<tr>");
						sb.append("<td>");
						sb.append(additionalQualifications.getSubject()==null?"":additionalQualifications.getSubject());
						sb.append("</td>");
						sb.append("<td>");
						sb.append(additionalQualifications.getOption()==null?"":additionalQualifications.getOption());
						sb.append("</td>");
						sb.append("<td>");
						sb.append(additionalQualifications.getInstitution()==null?"":additionalQualifications.getInstitution());
						sb.append("</td>");
						sb.append("<td>");
						sb.append(additionalQualifications.getEffectiveDate()==null?"":additionalQualifications.getEffectiveDate());
						sb.append("</td>");
						sb.append("<td>");
						sb.append(additionalQualifications.getSubjectCode()==null?"":additionalQualifications.getSubjectCode());
						sb.append("</td>");
						sb.append("<td>");
						sb.append(additionalQualifications.getOptionCode()==null?"":additionalQualifications.getOptionCode());
						sb.append("</td>");
						sb.append("</tr>");	
					}
				}else{
					sb.append("</tr>");
					sb.append("<td>");
					sb.append(Utility.getLocaleValuePropByKey("msgNorecordfound", locale));
					sb.append("</td>");
					sb.append("</tr>");
				}
					sb.append("</table>");
					System.out.println(sb.toString());
					return sb.toString();
				} catch (Exception e) {
					e.printStackTrace();
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				StringBuffer sb = new StringBuffer();
				
				sb.append("<table border='0' id='octMemberDetail' width='850' class='table-bordered'>");
				sb.append("<tr>");
				sb.append("<td>");
				sb.append(Utility.getLocaleValuePropByKey("msgNorecordfound", locale));
				sb.append("</td>");
				sb.append("</tr>");
				sb.append("</table>");
				return sb.toString();
				//e.printStackTrace();
				
			}
			
			return null;
		}
		
		public String getSubjectAreasGrid(TeacherDetail teacherDetail)
		{		
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		    }
			StringBuffer sb = new StringBuffer();		
			try 
			{
				List<TeacherSubjectAreaExam> teacherSubjectAreaExamsList=teacherSubjectAreaExamDAO.findTeacherSubjectAreaExamLst(teacherDetail);

				sb.append("<table border='0' id='subjectAreasGrid'  width='100%' class='table table-striped'  >");
				sb.append("<thead class='bg'>");
				sb.append("<tr>");
				String responseText="";
				
				sb.append("<th valign='top'>"+Utility.getLocaleValuePropByKey("lblStatus", locale)+"</th>");
				
				sb.append("<th valign='top'>"+Utility.getLocaleValuePropByKey("lblExamDate", locale)+"</th>");
				
				sb.append("<th valign='top'>"+Utility.getLocaleValuePropByKey("lblSub", locale)+"</th>");
				
				//responseText=PaginationAndSorting.responseSortingLink("+Utility.getLocaleValuePropByKey("lblScoreReport", locale)+",sortOrderFieldName,"scoreReport",sortOrderTypeVal,pgNo);
				sb.append("<th valign='top'>"+Utility.getLocaleValuePropByKey("lblScoreReport", locale)+"</th>");
				
				sb.append("<th class='net-header-text'>");
				sb.append(""+lblNotes+"");
				sb.append("</th>");			
				sb.append("</tr>");
				sb.append("</thead>");
				
				String sExamStatus="";
				if(teacherSubjectAreaExamsList!=null && teacherSubjectAreaExamsList.size()>0)
				for(TeacherSubjectAreaExam objTAD:teacherSubjectAreaExamsList)
				{
					if(objTAD.getExamStatus()!=null)
					{
						if(objTAD.getExamStatus().equalsIgnoreCase("P"))
							sExamStatus="Pass";
						else if(objTAD.getExamStatus().equalsIgnoreCase("F"))
							sExamStatus="Fail";
					}
					
					sb.append("<tr>");
					
					sb.append("<td>");
					sb.append(sExamStatus);	
					sb.append("</td>");
					
					sb.append("<td>");
					sb.append(Utility.convertDateAndTimeToUSformatOnlyDate(objTAD.getExamDate()));
					sb.append("</td>");
					
					sb.append("<td>");
					sb.append(objTAD.getSubjectAreaExamMaster().getSubjectAreaExamName());
					sb.append("</td>");
					
					
					String windowFunc="if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
					
					sb.append("<td>");
					
					if(objTAD.getScoreReport()!=null)
					{
						sb.append(objTAD.getScoreReport()==null?"":"<a data-original-title='"+Utility.getLocaleValuePropByKey("lblScoreReport", locale)+"' rel='tooltip' id='certSubject"+objTAD.getTeacherSubjectAreaExamId()+"' href='javascript:void(0);' onclick=\"downloadSubjectAreaExam('"+objTAD.getTeacherSubjectAreaExamId()+"','"+objTAD.getTeacherDetail().getTeacherId()+"','certSubject"+objTAD.getTeacherSubjectAreaExamId()+"');"+windowFunc+"\"><span class='icon-download-alt icon-large iconcolorBlue' style='padding-left:20px;'></span></a><br>");
						sb.append("<script>$('#certSubject"+objTAD.getTeacherSubjectAreaExamId()+"').tooltip();</script>");
					}
					
					sb.append("</td>");
					sb.append("<td>");
					if(objTAD.getExamNote()!=null && !objTAD.getExamNote().equalsIgnoreCase("")){
						sb.append(objTAD.getExamNote());
					}
					sb.append("</td>");
					/*sb.append("<td>");
					
					sb.append("	<a href='#' onclick=\"return showEditSubject('"+objTAD.getTeacherSubjectAreaExamId()+"')\" >Edit</a>");
					sb.append("&nbsp;|&nbsp;<a href='#' onclick=\"return delRow_subjectArea('"+objTAD.getTeacherSubjectAreaExamId()+"')\">Delete</a>");
					sb.append("</td>");*/
					sb.append("</tr>");
				}
				
				if(teacherSubjectAreaExamsList==null || teacherSubjectAreaExamsList.size()==0)
				{
					sb.append("<tr>");
					sb.append("<td colspan='5'>");
					sb.append(Utility.getLocaleValuePropByKey("msgNorecordfound", locale));
					sb.append("</td>");
					sb.append("</tr>");
				}
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			return sb.toString();
		
		}
		
		public String getSpecialNoteForTeacher(TeacherDetail teacherDetail){
			
			System.out.println(" >>>>>>>> getSpecialNoteForTeacher <<<<<<<<<<");
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		    }
			StringBuffer sb = new StringBuffer();	
			
			try
			{
				List<License> revokedLicenseList = new ArrayList<License>();
				List<String> revokedSSNList	= new ArrayList<String>();
				
				List<EmployeeMaster> dismissedTeacherList = new ArrayList<EmployeeMaster>();
				List<String> dismissedTeacherSSNList	= new ArrayList<String>();
				
				List<EmployeeMaster> doNotHireList = new ArrayList<EmployeeMaster>();
				List<String> doNotHireSSNList	= new ArrayList<String>();
				
				List<TeacherPersonalInfo> ssnTeachersList = new ArrayList<TeacherPersonalInfo>();
				List<String> ssnList = new ArrayList<String>();
				
				Map<String,String> mapTeacherSSN = new HashMap<String, String>();
				ArrayList<Integer> teacherIds = new ArrayList<Integer>();
				if(teacherDetail!=null){
					teacherIds.add(teacherDetail.getTeacherId());
				}
				if(teacherIds.size()>0){
					ssnTeachersList = teacherPersonalInfoDAO.findByTeacherIds(teacherIds);
					
					if(ssnTeachersList!=null && ssnTeachersList.size()>0){
						
						for(TeacherPersonalInfo tpi:ssnTeachersList){
							if(tpi.getSSN()!=null && !tpi.getSSN().equals(""))
								ssnList.add(Utility.decodeBase64(tpi.getSSN()));
						}
						  revokedLicenseList = licenseDAO.findListBySsn(ssnList); //Get SSN List from applicantlicensedetails(License) table
						  dismissedTeacherList = employeeMasterDAO.getDismissedTeacherListBySsn(ssnList); //Get Dismissed Teacher List from EmployeeMaster Table
						  doNotHireList	=	employeeMasterDAO.getDoNotHireTeacherListBySsnTeacher(ssnList); //Get Do Not Hire Teacher List Fro Employee master Table
						
						for(TeacherPersonalInfo tpi:ssnTeachersList){
							mapTeacherSSN.put(tpi.getTeacherId()+"", Utility.decodeBase64(tpi.getSSN()));
						}
					}
				}
				
				String dismissDate = "";
				String RevokedState = "";
				String RrevokeDate = "";
				String doNotHireDate = "";
				String doNotHireComment = "";
				
				if(dismissedTeacherList!=null && dismissedTeacherList.size()>0){
					if(dismissedTeacherList.get(0).getDismissedDate()!=null && !dismissedTeacherList.get(0).getDismissedDate().equals(""))
						dismissDate	=	dismissedTeacherList.get(0).getDismissedDate().toString();
				}
				
				if(revokedLicenseList!=null && revokedLicenseList.size()>0){
					if(revokedLicenseList.get(0).getLicenseRevokeRescindDate()!=null && !revokedLicenseList.get(0).getLicenseRevokeRescindDate().equals("") )
						RrevokeDate = revokedLicenseList.get(0).getLicenseRevokeRescindDate().toString();
					
					if(revokedLicenseList.get(0).getStateCode()!=null && !revokedLicenseList.get(0).getStateCode().equals("")){
						StateMaster stateMaster = stateMasterDAO.findByStateCode(revokedLicenseList.get(0).getStateCode());
						RevokedState = stateMaster.getStateName();
					}
				}
				
				if(doNotHireList!=null && doNotHireList.size()>0){
					if(doNotHireList.get(0).getDoNotHireDate()!=null && !doNotHireList.get(0).getDoNotHireDate().equals(""))
						doNotHireDate = doNotHireList.get(0).getDoNotHireDate().toString();
					
					if(doNotHireList.get(0).getDoNotHireComment()!=null && !doNotHireList.get(0).getDoNotHireComment().equals(""))
						doNotHireComment = doNotHireList.get(0).getDoNotHireComment();
				}
				
				
				/*if(revokedLicenseList!=null && revokedLicenseList.size()>0){
					for(License l:revokedLicenseList){
						revokedSSNList.add(l.getSSN());
					}
				}
				
				if(dismissedTeacherSSNList!=null && dismissedTeacherSSNList.size()>0){
					for(EmployeeMaster l:dismissedTeacherList){
						dismissedTeacherSSNList.add(l.getSSN());
					}		
				}
				
				if(doNotHireSSNList!=null && doNotHireSSNList.size()>0){
					for(EmployeeMaster l:doNotHireList){
						doNotHireSSNList.add(l.getSSN());
					}
				}*/
				sb.append("<div class='row left10'>");
				sb.append("<div class='mt10'>");
				sb.append("&nbsp;&nbsp;<lable><strong>Special Note</strong></lable>");
				sb.append("<ul>");
				if((dismissedTeacherList!=null && dismissedTeacherList.size()>0) || (revokedLicenseList!=null && revokedLicenseList.size()>0) || (doNotHireList!=null && doNotHireList.size()>0)){
					//dismissDate..............
					
					if(dismissDate!=null && !dismissDate.equals("")){
						String  dismissDateF = Utility.getCalenderDateFormart(dismissDate);
						sb.append("<li>");
						sb.append("This teacher was dismissed effective "+dismissDateF);
						sb.append("</li>");
					}
				/*	if(RevokedState!=null && !RevokedState.equals("") && RrevokeDate!=null && !RrevokeDate.equals("")){
						String  RrevokeDateF = Utility.getCalenderDateFormart(RrevokeDate);
						sb.append("<li>");
						sb.append("License revoked by the state of "+RevokedState+" effective "+RrevokeDateF);
						sb.append("</li>");
					}
					*/
					//    RevokedState  and RrevokeDate  .............
					
					if(RevokedState!=null && !RevokedState.equals("") || RrevokeDate!=null && !RrevokeDate.equals("")||revokedLicenseList!=null && !revokedLicenseList.equals("")){
						
						if(RevokedState!=null && !RevokedState.equals("") && RrevokeDate!=null && !RrevokeDate.equals("")){
							String  RrevokeDateF = Utility.getCalenderDateFormart(RrevokeDate);
							sb.append("<li>");
							sb.append("License revoked by the state of "+RevokedState+" effective "+RrevokeDateF);
							sb.append("</li>");
						}else{
						      if(RevokedState!=null && !RevokedState.equals("")){
						        
						            sb.append("<li>");
						            sb.append("License revoked by the state of "+RevokedState);
						            sb.append("</li>");
						      }else{if(RrevokeDate!=null && !RrevokeDate.equals("")){
						    	    String  RrevokeDateF = Utility.getCalenderDateFormart(RrevokeDate);
						    	    sb.append("<li>");
									sb.append("License revoked by effective "+RrevokeDateF);
									sb.append("</li>");
						      }else{
						    	  sb.append("<li>");
									sb.append("License revoked ");
									sb.append("</li>");
						      }
						      }
						}
					}
					
					//doNotHireList..............
					
					if(doNotHireList!=null && doNotHireList.size()>0)
					{
						if((doNotHireDate!=null && !doNotHireDate.equals("")) && (doNotHireDate!=null && !doNotHireDate.equals(""))){
							String  doNotHireDateF = Utility.getCalenderDateFormart(doNotHireDate);
							sb.append("<li>");
							sb.append("Candidate appears on your LEA Do Not Hire List as of "+doNotHireDateF+". Comment: "+doNotHireComment);
							sb.append("</li>");
						}else{
							if(doNotHireComment!=null && !doNotHireComment.equals(""))
							{
								sb.append("<li>");
						     	sb.append("Candidate appears on your LEA Do Not Hire List. Comment: "+doNotHireComment);
							    sb.append("</li>");
							}else{
								   if(doNotHireDate!=null && !doNotHireDate.equals(""))
								   {
									    String  doNotHireDateF = Utility.getCalenderDateFormart(doNotHireDate);
									    sb.append("<li>");
										sb.append("Candidate appears on your LEA Do Not Hire List as of "+doNotHireDateF);
										sb.append("</li>");
								   }else{
							              sb.append("<li>");
						     	          sb.append("Candidate appears on your LEA Do Not Hire List.");
							              sb.append("</li>");
								   }
							}
						}
					}
					
				}else{
					sb.append("<li>N/A</li>");
				}
				
				sb.append("</ul>");
				
				sb.append("</div>");
				sb.append("</div>");
			//////////////////////////////////  test
				/*sb.append("<div class='row left10'>");
					sb.append("<div class='mt10'>");
					
				sb.append("<lable>Special Note</lable>");
				sb.append("<ul>");
				//if((dismissDate!=null && !dismissDate.equals("")) || (RevokedState!=null && !RevokedState.equals("") && RrevokeDate!=null && !RrevokeDate.equals("")) || (doNotHireDate!=null && !doNotHireDate.equals(""))){
			    //if(dismissDate!=null && !dismissDate.equals("")){
						sb.append("<li>");
						sb.append("This teacher was dismissed effective "+"dismissDate");
						sb.append("</li>");
					
				//	if(RevokedState!=null && !RevokedState.equals("") && RrevokeDate!=null && !RrevokeDate.equals("")){
						sb.append("<li>");
						sb.append("License revoked by the state of "+"RevokedState"+" effective "+"RrevokeDate");
						sb.append("</li>");
					
				//	if(doNotHireDate!=null && !doNotHireDate.equals("")){
						sb.append("<li>");
						sb.append("Candidate appears on your LEA Do Not Hire List as of "+"doNotHireDate"+". Comment: "+"doNotHireComment");
						sb.append("</li>");
					
				
				
				sb.append("</ul>");
		
				sb.append("</div>");
			sb.append("</div>");*/
			}catch(Exception e){
				e.printStackTrace();
			}
			
			
			
			return sb.toString();
		}
		
		//////////////  End of the  specail Note 
		
		 public String getTeacherEducationGrid(Integer teacherId, String noOfRow, String pageNo,String sortOrder,String sortOrderType,String visitLocation)
	       {
	        WebContext context;
	        context = WebContextFactory.get();
	        HttpServletRequest request = context.getHttpServletRequest();
	        HttpSession session = request.getSession(false);
	        UserMaster userMaster=null;
	        DistrictMaster districtMaster=null;
	        boolean nobleflag=false;
	        if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	        {
	         throw new IllegalStateException("Your session has expired!");
	           }else{
	            userMaster=(UserMaster)session.getAttribute("userMaster");
	            districtMaster=userMaster.getDistrictId();
	           if(districtMaster!=null)
	            if(districtMaster.getDistrictId()==7800038)
	             nobleflag=true;
	           }
	        
	        return getTeacherEducationLicenseGrid(teacherId);
	      
	       }
	       
		 public String getTeacherEducationLicenseGrid(int teacherId)
	       {
	        
	        WebContext context;
	        context = WebContextFactory.get();
	        HttpServletRequest request = context.getHttpServletRequest();
	        HttpSession session = request.getSession(false);
	        if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	        {
	         throw new IllegalStateException("Your session has expired!");
	        }

	        StringBuffer sb = new StringBuffer(); 
	        
	        
	        List<LicensureEducation> licensureEducationList = new ArrayList<LicensureEducation>();
	        String ssn="";
	        
	        try {
	         
	         TeacherPersonalInfo teacherPersonalInfo = teacherPersonalInfoDAO.findById(teacherId, false, false);
	         
	         if(teacherPersonalInfo!=null)
	         {
	         // ssn = Utility.decodeBase64(teacherPersonalInfo.getSSN());
	        	 ssn = teacherPersonalInfo.getSSN();
	         Criterion criteria = Restrictions.eq("SSN", ssn);
	         licensureEducationList = licensureEducationDAO.findByCriteria(criteria);
	         System.out.println("licensureEducationList size----------------------"+licensureEducationList.size());
	         }
	         
	         

	            sb.append("<table border='0' id='gridDataTeacherEducationsForLablel' class='table table-striped' width='600px;' >");
				sb.append("<thead class='bg'>");			
				sb.append("<tr>");
				
				sb.append("<th width='30%' >School</th>");
				sb.append("<th width='30%' >Graduation Date</th>");
				sb.append("<th width='20%' >Degree</th>");
				sb.append("<th width='20%' >Major</th>");
				

				
				sb.append("</tr>");
				sb.append("</thead>");

				if(licensureEducationList!=null)
					for(LicensureEducation licensureEducation:licensureEducationList)
					{
						System.out.println("inside licensureEducation");
						/*if(licensureEducation.getEducationFlag()){
							continue;
					    }*/
						
						List<DegreeTypeMaster> degreeTypeMasters = new ArrayList<DegreeTypeMaster>();
						List<UniversityMaster> universityMaster = new ArrayList<UniversityMaster>();
						sb.append("<tr>");
						sb.append("<td>"); 
						Criterion iheCriteria = Restrictions.eq("iheCode", licensureEducation.getIHECode());
						universityMaster = universityMasterDAO.findByCriteria(iheCriteria);
						if(universityMaster.size() != 0 && universityMaster != null)
						sb.append(universityMaster.get(0).getUniversityName());
						
					    sb.append("</td>");
					
						sb.append("<td>");
						String date= PFLicenseEducation.getIndiaFormatDateMonthFirst(licensureEducation.getGraduationDate());
						sb.append(date);
					    sb.append("</td>");

					    sb.append("<td>"); 
					    Criterion degreeCriteria = Restrictions.eq("degreeTypeCode", licensureEducation.getDegreeType());
						degreeTypeMasters = degreeTypeMasterDAO.findByCriteria(degreeCriteria);
						if(degreeTypeMasters.size() != 0 && degreeTypeMasters != null)
						sb.append(degreeTypeMasters.get(0).getDegreeName());
					    
					    sb.append("</td>");				
			          
					    sb.append("<td>"); 
					    sb.append(licensureEducation.getMajor());
					    sb.append("</td>");
						
					    sb.append("</tr>");
						
					}

				if(licensureEducationList==null || licensureEducationList.size()==0)
				{			
					sb.append("<tr>");
					sb.append("<td colspan='4'>");
					sb.append(Utility.getLocaleValuePropByKey("msgNorecordfound", locale));
					sb.append("</td>");
					sb.append("</tr>");
								
				}
				sb.append("</table>");
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			return sb.toString();	
		}
	      
	    public String getTeacherAssessmentGrid(int teacherId,String noOfRow, String pageNo,String sortOrder,String sortOrderType)
	   	{
	    	
	    	WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			String locale = Utility.getValueOfPropByKey("locale");
			UserMaster userMaster=null;
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
			}
	   		int noOfRowInPage 	= 	Integer.parseInt(noOfRow);
	   		int pgNo 			= 	Integer.parseInt(pageNo);
	   		int start 			= 	((pgNo-1)*noOfRowInPage);
	   		int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
	   		int totalRecord		= 	0;
	   		String sortOrderFieldName	=	"assessmentType";
	   		Order  sortOrderStrVal		=	null;		
	   		if(!sortOrder.equals("") && !sortOrder.equals(null))
	   		{
	   			sortOrderFieldName		=	sortOrder;
	   		}
	   		
	   		String sortOrderTypeVal="0";
	   		if(!sortOrderType.equals("") && !sortOrderType.equals(null))
	   		{
	   			if(sortOrderType.equals("0"))
	   			{
	   				sortOrderStrVal		=	Order.asc(sortOrderFieldName);
	   			}
	   			else
	   			{
	   				sortOrderTypeVal	=	"1";
	   				sortOrderStrVal		=	Order.desc(sortOrderFieldName);
	   			}
	   		}
	   		else
	   		{
	   			sortOrderTypeVal		=	"0";
	   			sortOrderStrVal			=	Order.asc(sortOrderFieldName);
	   		}
	   		System.out.println("............."+sortOrderStrVal);
	   		List<TeacherAssessmentStatus> teacherAssessmentJsiEpi = null;
	   		List<TeacherNormScore> teacherNormScore = null;
	   		List<AssessmentDomainScore> assessmentDomainScore = null;
	   		List<RawDataForDomain> rawDataForDomain = null;
	   		StringBuffer sb = new StringBuffer();	
	   		String responseText="";
	   		try
	   		{		TeacherDetail teacherDetail		=	teacherDetailDAO.findById(teacherId, false, false);		
	   				teacherAssessmentJsiEpi 		= 	teacherAssessmentStatusDAO.getAllAssessmentByTeacher(teacherDetail,sortOrderStrVal,userMaster);
	   				teacherNormScore 		        = 	teacherNormScoreDAO.findNormScoreByTeacher(teacherDetail);
	   				assessmentDomainScore 		    = 	assessmentDomainScoreDAO.findByTeacher(teacherDetail);
	   				rawDataForDomain 		        =   rawDataForDomainDAO.findByTeacher(teacherDetail);

	   		/*		List<TeacherAssessmentStatusDetail> teacherDetailList	=	new ArrayList<TeacherAssessmentStatusDetail>();
	   				for(TeacherAssessmentStatus teacherAssessmentStatus : teacherAssessmentJsiEpi){
	   					TeacherAssessmentStatusDetail tecaherObj = new TeacherAssessmentStatusDetail();
	   					tecaherObj.setAssessmentName(teacherAssessmentStatus.getAssessmentDetail().getAssessmentName());
	   					tecaherObj.setAssessmentType(teacherAssessmentStatus.getAssessmentType());
	   					tecaherObj.setStatusMaster(teacherAssessmentStatus.getStatusMaster());
	   					tecaherObj.setCreatedDateTime(teacherAssessmentStatus.getCreatedDateTime());
	   					teacherDetailList.add(tecaherObj);

	   				}*/
	   				Map<Integer,TeacherNormScore> normScoreMap = new HashMap<Integer, TeacherNormScore>();
	   				
	   				
	   				
	   				for(TeacherNormScore tns : teacherNormScore){
	   					if(tns!=null && tns.getTeacherAssessmentdetail()!=null)
	   					normScoreMap.put(tns.getTeacherAssessmentdetail().getTeacherAssessmentId(),tns);
	   				}
	   				
	   				
	   				
	   				
	   				sb.append("<table id='assessmentTable' width='100%'>");
	   				sb.append("<thead class='bg'>");
	   				sb.append("<tr>");

	   				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("msgAssessmentType", locale),sortOrderFieldName,"assessmentType",sortOrderTypeVal,pgNo);
	   				sb.append("<th valign='top'>"+responseText+"</th>");

	   				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("assessmentCompletionDate", locale),sortOrderFieldName,"createdDateTime",sortOrderTypeVal,pgNo);
	   				sb.append("<th valign='top'>"+responseText+"</th>");
	   				
	   				responseText=Utility.getLocaleValuePropByKey("lblScr", locale);
	   				sb.append("<th valign='top'>"+responseText+"</th>");
	   				
	   				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblStatus", locale),sortOrderFieldName,"pass",sortOrderTypeVal,pgNo);
	   				sb.append("<th valign='top'>"+responseText+"</th>");
	   				
	   				responseText=Utility.getLocaleValuePropByKey("lblTeachingSkillscore", locale);
	   				sb.append("<th valign='top'>"+responseText+"</th>");
	   				
	   				responseText=Utility.getLocaleValuePropByKey("lblAttitudinalFactorsscore", locale);
	   				sb.append("<th valign='top'>"+responseText+"</th>");
	   				
	   				responseText=Utility.getLocaleValuePropByKey("lblCognitiveAbilityscore", locale);
	   				sb.append("<th valign='top'>"+responseText+"</th>");

	   				sb.append("</tr>");
	   			    sb.append("</thead>");
	   			    
	   			    int count = 1;
	   			    List<TeacherAssessmentStatus> teacherAssessmentStatusListForSp = new ArrayList<TeacherAssessmentStatus>();
	   			    if(teacherAssessmentJsiEpi!=null && teacherAssessmentJsiEpi.size()>0){
	   			    	for(TeacherAssessmentStatus teacherAssessmentStatus : teacherAssessmentJsiEpi){
	   			    		if(teacherAssessmentStatus.getAssessmentType()==3){
	   			    			if(teacherAssessmentStatus.getStatusMaster()!=null && (teacherAssessmentStatus.getStatusMaster().getStatusShortName().equals("comp") || teacherAssessmentStatus.getStatusMaster().getStatusShortName().equals("vlt"))){
	   			    				teacherAssessmentStatusListForSp.add(teacherAssessmentStatus);
	   			    			}
	   			    		}
	   			    	}
	   			    }
	   			    
	   			    if(teacherAssessmentJsiEpi.size()>0)
	   			    {
	   			    	//for(TeacherAssessmentStatus assessmentStatus:teacherAssessmentJsiEpi){
	   			    	for(TeacherAssessmentStatus assessmentStatus : teacherAssessmentJsiEpi){
	   			    		Map<Integer,Double> aDSMap = new HashMap<Integer, Double>();
	   		   				Map<Integer,Integer> rDMap = new HashMap<Integer, Integer>();
	   			    		
	   			    		if(assessmentStatus.getAssessmentType()!=2)
	   			    		{
	   			    		 for(RawDataForDomain rDD : rawDataForDomain){
	   			    			 if(assessmentStatus.getTeacherAssessmentdetail()!= null && rDD.getTeacherAssessmentdetail()!=null){
		   			    			 if(assessmentStatus.getTeacherAssessmentdetail().getTeacherAssessmentId().equals(rDD.getTeacherAssessmentdetail().getTeacherAssessmentId())){
		   			    				rDMap.put(rDD.getRawDataId(), rDD.getRawDataId());
		   						     }
	   			    			 }
	   			    		  }
	   			    		  for(AssessmentDomainScore aDs : assessmentDomainScore){
	   				    			if(aDs.getRawDataForDomain().getRawDataId().equals(rDMap.get(aDs.getRawDataForDomain().getRawDataId()))){
	   				    				aDSMap.put(aDs.getDomainMaster().getDomainId() ,  aDs.getNormscore());
	   				    			}
	   							   
	   				    		  }
	   			    		}
	   			    		
	   			    			sb.append("<tr>");
	   //assessment type
	   						    sb.append("<td>");
	   						    if(assessmentStatus.getAssessmentType()==1) {
	   						    	sb.append(Utility.getLocaleValuePropByKey("lblEPI", locale));
	   						    } else if(assessmentStatus.getAssessmentType()==2) {
	   						    	 sb.append(Utility.getLocaleValuePropByKey("lblJSI", locale));
	   						    }else if(assessmentStatus.getAssessmentType()==3) {
	   						    	 sb.append(Utility.getLocaleValuePropByKey("lblSmartP", locale));
	   						    }else if(assessmentStatus.getAssessmentType()==4) {
	   						    	 sb.append(Utility.getLocaleValuePropByKey("lblIPI", locale));
	   						    }
	   						    sb.append("</td>");
	   //date
	   						    sb.append("<td>");
   						    	if(assessmentStatus.getAssessmentCompletedDateTime()!=null)
   						    		sb.append(Utility.convertDateAndTimeFormatForEpiAndJsi(assessmentStatus.getAssessmentCompletedDateTime()));
   						    	else if(assessmentStatus.getCreatedDateTimeOld()!=null)
						    		sb.append(Utility.convertDateAndTimeFormatForEpiAndJsi(assessmentStatus.getCreatedDateTimeOld()));
   						    	else if(assessmentStatus.getCreatedDateTime()!=null)
						    		sb.append(Utility.convertDateAndTimeFormatForEpiAndJsi(assessmentStatus.getCreatedDateTime()));
   						    	else
   						    		sb.append(Utility.getLocaleValuePropByKey("optN/A", locale));
	   						    sb.append("</td>");
	   //score					    
	   						    sb.append("<td>");
	   						    if(assessmentStatus!=null && assessmentStatus.getAssessmentType()==1 && assessmentStatus.getStatusMaster().getStatusShortName().equals("comp") && assessmentStatus.getTeacherAssessmentdetail()!=null ){
	   						    	if(normScoreMap.get(assessmentStatus.getTeacherAssessmentdetail().getTeacherAssessmentId())!=null){
	   						    	  sb.append(normScoreMap.get(assessmentStatus.getTeacherAssessmentdetail().getTeacherAssessmentId()).getTeacherNormScore());
	   						    	}
	   						    	else
	   						    		sb.append(Utility.getLocaleValuePropByKey("optN/A", locale));
	   						    		
	   						    }else{
	   						    	 sb.append(Utility.getLocaleValuePropByKey("optN/A", locale));
	   						    }
	   						    sb.append("</td>");
	   //status
	   						    sb.append("<td>");
	   						    if(assessmentStatus.getAssessmentType()!=3){
	   						    	if(assessmentStatus.getStatusMaster()!=null)
	   						    	{
		   						    	if(assessmentStatus.getStatusMaster().getStatusShortName().equals("comp"))
		   						    	    sb.append(Utility.getLocaleValuePropByKey("lblCompleted", locale));
		   						    	else if(assessmentStatus.getStatusMaster().getStatusShortName().equals("vlt")){
		   						    		if(userMaster.getEntityType()==1){
			   						    		if(assessmentStatus.getAssessmentType()==1){
			   						    			sb.append("<a data-original-title='Click to Reset EPI' rel='tooltip' id='resetEPI' href='javascript:void(0)' onclick=\"checkViolatedForAssessmentReset("+teacherDetail.getTeacherId()+","+assessmentStatus.getAssessmentType()+","+assessmentStatus.getTeacherAssessmentStatusId()+");\">");
			   						    			sb.append(Utility.getLocaleValuePropByKey("optTOut", locale));
			   										sb.append("</a>");
			   										sb.append("<script>$('#resetEPI').tooltip();</script>");
			   						    		}else if(assessmentStatus.getAssessmentType()==2){
			   						    			sb.append("<a data-original-title='Click to Reset JSI' rel='tooltip' id='resetJSI' href='javascript:void(0)' onclick=\"checkViolatedForAssessmentReset("+teacherDetail.getTeacherId()+","+assessmentStatus.getAssessmentType()+","+assessmentStatus.getTeacherAssessmentStatusId()+");\">");
			   						    			sb.append(Utility.getLocaleValuePropByKey("optTOut", locale));
			   										sb.append("</a>");
			   										sb.append("<script>$('#resetJSI').tooltip();</script>");
			   						    		}else if(assessmentStatus.getAssessmentType()==4){
			   						    			sb.append("<a data-original-title='Click to Reset IPI' rel='tooltip' id='resetIPI' href='javascript:void(0)' onclick=\"checkViolatedForAssessmentReset("+teacherDetail.getTeacherId()+","+assessmentStatus.getAssessmentType()+","+assessmentStatus.getTeacherAssessmentStatusId()+");\">");
			   						    			sb.append(Utility.getLocaleValuePropByKey("optTOut", locale));
			   										sb.append("</a>");
			   										sb.append("<script>$('#resetIPI').tooltip();</script>");
			   						    		}
		   						    		}else{
		   						    			sb.append(Utility.getLocaleValuePropByKey("optTOut", locale));
	   						    			}
		   						    	}
	   						    	}else{
	   						    		sb.append(Utility.getLocaleValuePropByKey("optN/A", locale));
	   						    	}
	   						    }else{
	   						    	if(assessmentStatus.getStatusMaster()!=null && assessmentStatus.getStatusMaster().getStatusShortName().equals("vlt")){
	   						    		if(userMaster.getEntityType()==1 || userMaster.getEntityType()==5){
	   						    			if(teacherAssessmentStatusListForSp!=null && ((teacherAssessmentStatusListForSp.size()==1 && count==1) || (teacherAssessmentStatusListForSp.size()==2 && count==2))){
		   						    			sb.append("<a data-original-title='Click to Reset Smart Practices Assessment' rel='tooltip' id='resetSP' href='javascript:void(0)' onclick=\"checkViolatedForAssessmentReset("+teacherDetail.getTeacherId()+","+assessmentStatus.getAssessmentType()+","+assessmentStatus.getTeacherAssessmentStatusId()+");\">");
		   						    			sb.append(Utility.getLocaleValuePropByKey("optTOut", locale));
		   										sb.append("</a>");
		   										sb.append("<script>$('#resetSP').tooltip();</script>");
	   										}
	   						    			else{
	   						    				count++;
		   						    			sb.append(Utility.getLocaleValuePropByKey("optTOut", locale));
	   						    			}
	   						    		}else{
	   						    			sb.append(Utility.getLocaleValuePropByKey("optTOut", locale));
	   						    		}
	   						    	}
	   						    	else if(assessmentStatus.getPass()!=null)
	   						    	{
	   						    		count++;
	   						    		if(assessmentStatus.getPass().equals("P"))
	   						    		    sb.append(Utility.getLocaleValuePropByKey("optPass", locale));
	   						    		else
	   						    			sb.append(Utility.getLocaleValuePropByKey("optFail", locale));
	   						    	}
	   						    }					    
	   						    sb.append("</td>");
	   						    
	   //3Teaching Skills score
	   						    sb.append("<td>");
	   						    if(assessmentStatus.getAssessmentType()==2)
	   						    sb.append(Utility.getLocaleValuePropByKey("optN/A", locale));
	   						    else{
	   						    	if(aDSMap.get(3)!=null)
	   						    	sb.append(aDSMap.get(3));
	   						    	else
	   						    		sb.append(Utility.getLocaleValuePropByKey("optN/A", locale));
	   						    }
	   						    sb.append("</td>");
	   //1Attitudinal Factors score
	   						    sb.append("<td>");
	   						    if(assessmentStatus.getAssessmentType()==2)
	   							    sb.append(Utility.getLocaleValuePropByKey("optN/A", locale));
	   							    else{
	   							    	
	   							    	if(aDSMap.get(1)!=null)
	   								    	sb.append(aDSMap.get(1));
	   								    	else
	   								    		sb.append(Utility.getLocaleValuePropByKey("optN/A", locale));
	   							    }
	   						    sb.append("</td>");
	   //2Cognitive Ability score
	   						    sb.append("<td>");
	   						    if(assessmentStatus.getAssessmentType()==2)
	   							    sb.append(Utility.getLocaleValuePropByKey("optN/A", locale));
	   							    else{
	   							    	
	   							    	if(aDSMap.get(2)!=null)
	   								    	sb.append(aDSMap.get(2));
	   								    	else
	   								    		sb.append(Utility.getLocaleValuePropByKey("optN/A", locale));	
	   							    }
	   						    sb.append("</td>");

	   						    sb.append("</tr>");
	   						    rDMap=null;
	   						    aDSMap=null;
	   			    	}
	   			    	
	   			    }else{
	   			    	sb.append("<tr>");
	   			    	sb.append("<td>");
	   			    	sb.append(Utility.getLocaleValuePropByKey("msgNorecordfound", locale));
	   			    	sb.append("<td>");
	   			    	sb.append("</tr>");
	   			    }
	   			    sb.append("</table>");		
	   		}	
	   		catch(Exception e)
	   		{e.printStackTrace();}
	   		
	   		return sb.toString();		
	   	}
	    
	  //Display Licence Grid
	    public String displayRecordsBySSNForDate(Integer teacherId)
		{
			
			StringBuffer sb=new StringBuffer();		

			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException("Your session has expired!");
			}

			List<License> ssnList=new ArrayList<License>();
			List<StateMaster> stateFullName = null;
			List<License> licenseList=null;
			List<LicenseAreaDomain> licenseAreaDomainList=null;
			List<LicenseStatusDomain> licenseStatusDomainList=null;
			List<CertificateTypeMaster> lstcertificatetypemaster=null;
			
			String ssn="";
			try{	
				
				System.out.println(" teacherId "+teacherId);

				TeacherPersonalInfo teacherPersonalInfo = teacherPersonalInfoDAO.findById(teacherId, false, false);
				if(teacherPersonalInfo!=null)
					ssn = Utility.decodeBase64(teacherPersonalInfo.getSSN());
				
				String stateName="";
				String issueDate="";
				String effectiveDate="";
				String expirationDate="";			

				sb.append("<table border='0'  id='divLicneseGridCertificationsGridD' class='table  table-striped' width='600px;' >");
				sb.append("<thead class='bg'>");			
				sb.append("<tr>");

				
				sb.append("<th width='50%'  valign='top'>State</th>");
			//	sb.append("<th width='25%'  valign='top'>Issue Date</th>");
				sb.append("<th width='25%'  valign='top'>Effective Date</th>");
				sb.append("<th width='25%'  valign='top'>Expiration Date</th>");


				sb.append("</tr>");
				sb.append("</thead>");

					
				Criterion statusArea = Restrictions.eq("status", "A");
				Criterion ssnArea = Restrictions.eq("SSN", ssn);
				licenseList=licenseDAO.findByCriteria(statusArea,ssnArea);

			
				if((licenseList!=null)&&(licenseList.size()>0))
				{				
					//get state full name
					Criterion active = Restrictions.eq("status", "A");
					Criterion name = Restrictions.eq("stateShortName", "NC");
					stateFullName = stateMasterDAO.findByCriteria(active,name);
					stateName=stateFullName.get(0).getStateName();	
					
					if(licenseList.get(0).getLicenseLastIssueDate()!=null)
					{
					issueDate=licenseList.get(0).getLicenseLastIssueDate().toString();
					}
					else
					{
						issueDate="N/A";
					}
					
					if(licenseList.get(0).getLicenseEffectiveDate()!=null)
					{
					effectiveDate=Utility.convertDateAndTimeToDatabaseformatOnlyDate(licenseList.get(0).getLicenseEffectiveDate()).toString();
					}
					else
					{
						effectiveDate="N/A";
					}
					
					if(licenseList.get(0).getLicenseExpireDate()!=null)
					{
					expirationDate=Utility.convertDateAndTimeToDatabaseformatOnlyDate(licenseList.get(0).getLicenseExpireDate()).toString();
					}
					else
					{
						expirationDate="N/A";
					}
				sb.append("<tr >");
				sb.append("<td>");
				sb.append(stateName);
				sb.append("</td>");

				/*sb.append("<td>");
				sb.append(issueDate);
				sb.append("</td>");*/

				sb.append("<td>");
				sb.append(effectiveDate);
				sb.append("</td>");

				sb.append("<td>");
				sb.append(expirationDate);
				sb.append("</td>");

				sb.append("</tr>");
				}
			
			if((licenseList==null)||(licenseList.size()==0))
			{
				sb.append("<tr>");
				sb.append("<td colspan='4'>");
				sb.append(Utility.getLocaleValuePropByKey("msgNorecordfound", locale));
				sb.append("</td>");
				sb.append("</tr>");
				
			}
			}catch(Exception e){
				e.printStackTrace();
			}
			return sb.toString();	
		}
	    
	    public String getLEACandidatePortfolio(Integer teacherId)
		{
			
			StringBuffer sb=new StringBuffer();		

			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException("Your session has expired!");
			}

			List<License> ssnList=new ArrayList<License>();
			List<StateMaster> stateFullName = null;
			
			
			List<LicenseArea> licenseAreaList = new ArrayList<LicenseArea>();
			List<LicenseClassLevelDomain> licenseClassCodeList = new ArrayList<LicenseClassLevelDomain>();
			List<LicenseAreaDomain> licenseAreaDomainList = new ArrayList<LicenseAreaDomain>();
			List<String> classCodeList = new ArrayList<String>();
			List<String> laCodeList = new ArrayList<String>();
			Map<String, String> licenseAreaMap = new HashMap<String, String>();
			Map<String, String> classCodeMap = new HashMap<String, String>();
			String ssn="";
			
			try{	
				
				System.out.println(" teacherId "+teacherId);

				TeacherPersonalInfo teacherPersonalInfo = teacherPersonalInfoDAO.findById(teacherId, false, false);
				if(teacherPersonalInfo!=null)
					ssn = Utility.decodeBase64(teacherPersonalInfo.getSSN());
			
				sb.append("<table border='0'  id='divLEACandidatePorfolioGridD' class='table  table-striped' width='600px;' >");
				sb.append("<thead class='bg'>");			
				sb.append("<tr>");

				sb.append("<th valign='top'>Name</th>");
				sb.append("<th valign='top'>Effective Date</th>");
				sb.append("<th valign='top'>Class</th>");
				sb.append("<th valign='top'>Experience(Years)</th>");
				sb.append("<th valign='top'>Program</th>");
				sb.append("<th valign='top'>HQ</th>");

				sb.append("</tr>");
				sb.append("</thead>");

					
				Criterion statusArea = Restrictions.eq("status", "A");
				Criterion ssnArea = Restrictions.eq("SSN", ssn);
				
				licenseAreaList=licenseAreaDAO.findByCriteria(statusArea,ssnArea);

			
				if((licenseAreaList!=null)&&(licenseAreaList.size()>0))
				{				
					//get state full name
					/*Criterion active = Restrictions.eq("status", "A");
					Criterion name = Restrictions.eq("stateShortName", "NC");
					stateFullName = stateMasterDAO.findByCriteria(active,name);*/
					
					for(LicenseArea llist:licenseAreaList){
						classCodeList.add(llist.getLicenseClassLevelCode());
						laCodeList.add(llist.getLicenseArea());
					}
					
					licenseClassCodeList = licenseClassLevelDomailDAO.getListByClassCodeList(classCodeList);
					licenseAreaDomainList = licenseAreaDomainDAO.getListByClassCodeList(laCodeList);
					
					for(LicenseClassLevelDomain lcld:licenseClassCodeList){
						classCodeMap.put(lcld.getClassCode()+"",lcld.getClassCodeDescription()+"");
					}
					
					for(LicenseAreaDomain lad:licenseAreaDomainList){
						licenseAreaMap.put(lad.getLicenseAreaCode()+"", lad.getLicenseAreaDescription()+"");
					}
					
					
					//stateName=stateFullName.get(0).getStateName();	
					
					/*if(licenseList.get(0).getLicenseLastIssueDate()!=null)
					{
						issueDate=licenseList.get(0).getLicenseLastIssueDate().toString();
					}
					else
					{
						issueDate="N/A";
					}*/
					
					/*if(licenseList.get(0).getLicenseEffectiveDate()!=null)
					{
						effectiveDate=Utility.convertDateAndTimeToDatabaseformatOnlyDate(licenseList.get(0).getLicenseEffectiveDate()).toString();
					}
					else
					{
						effectiveDate="N/A";
					}*/
					
					/*if(licenseList.get(0).getLicenseExpireDate()!=null)
					{
					expirationDate=Utility.convertDateAndTimeToDatabaseformatOnlyDate(licenseList.get(0).getLicenseExpireDate()).toString();
					}
					else
					{
						expirationDate="N/A";
					}*/
					for(LicenseArea la:licenseAreaList){
						sb.append("<tr >");
						sb.append("<td>");
							sb.append(licenseAreaMap.get(la.getLicenseArea())!=null?licenseAreaMap.get(la.getLicenseArea()):"");
						sb.append("</td>");
	
						sb.append("<td>");
							sb.append(Utility.convertDateAndTimeToDatabaseformatOnlyDate(la.getLicenseAreaEffectiveDate()));
						sb.append("</td>");
	
						sb.append("<td>");
							sb.append(classCodeMap.get(la.getLicenseClassLevelCode())!=null?classCodeMap.get(la.getLicenseClassLevelCode()):"");
						sb.append("</td>");
						
						sb.append("<td>");
							sb.append(la.getYearsOfExperience()!=null?la.getYearsOfExperience():"");
						sb.append("</td>");
	
						sb.append("<td>");
							sb.append(la.getLicenseProgramStatusCode()+" "+la.getLicenseProgramBasisCode());
						sb.append("</td>");
						
						sb.append("<td>");
							sb.append(la.getLicenseAreaHQCode());
						sb.append("</td>");
	
						sb.append("</tr>");
					}	
				}
			
			if((licenseAreaList==null)||(licenseAreaList.size()==0))
			{
				sb.append("<tr>");
				sb.append("<td colspan='4'>");
				sb.append(Utility.getLocaleValuePropByKey("msgNorecordfound", locale));
				sb.append("</td>");
				sb.append("</tr>");
				
			}
			}catch(Exception e){
				e.printStackTrace();
			}
			return sb.toString();	
		}
	    
	    
	    public String verifyTranscript(Integer teacherId, String noOfRow, String pageNo,String sortOrder,String sortOrderType,String visitLocation)
		{
	    	System.out.println(":::::::::::::::::::::::::::: VerifyTranscript ::::::::::::::::::::::::::::");
	    	System.out.println(":::::::::: teacherId ::::::::::"+teacherId);
	    	WebContext context;
	        context = WebContextFactory.get();
	        HttpServletRequest request = context.getHttpServletRequest();
	        HttpSession session1 = request.getSession(false);
	        if(session1 == null ||(session1.getAttribute("teacherDetail")==null && session1.getAttribute("userMaster")==null)) {
	              throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	        	}
	        StringBuffer sb = new StringBuffer();
	    	
	    	try{
	    		int totalTranscript = 0;
	    		/*============= For Sorting ===========*/
	    		int noOfRowInPage 	= 	Integer.parseInt(noOfRow);
	    		int pgNo 			= 	Integer.parseInt(pageNo);
	    		int start 			= 	((pgNo-1)*noOfRowInPage);
	    		int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
	    		int totalRecord		= 	0;
	    		
	    		/*====== set default sorting fieldName ====== **/
	    		String sortOrderFieldName	=	"attendedInYear";
	    		String sortOrderNoField		=	"attendedInYear";
	    		
	    		/**Start set dynamic sorting fieldName **/
	    		Order sortOrderStrVal		=	null;
	    		if(sortOrder!=null){
	    			 if(!sortOrder.equals("") && !sortOrder.equals(null) && !sortOrder.equals("degree") && !sortOrder.equals("fieldOfStudy") && !sortOrder.equals("university")){
	    				 sortOrderFieldName=sortOrder;
	    				 sortOrderNoField=sortOrder;
	    			 }
	    			 if(sortOrder.equals("degree")){
	    				 sortOrderNoField="degree";
	    			 }
	    			 if(sortOrder.equals("fieldOfStudy")){
	    				 sortOrderNoField="fieldOfStudy";
	    			 }
	    			 if(sortOrder.equals("university")){
	    				 sortOrderNoField="university";
	    			 }
	    		}
	    		String sortOrderTypeVal="0";
	    		if(!sortOrderType.equals("") && !sortOrderType.equals(null))
	    		{
	    			if(sortOrderType.equals("0"))
	    			{
	    				sortOrderStrVal		=	Order.asc(sortOrderFieldName);
	    			}
	    			else
	    			{
	    				sortOrderTypeVal	=	"1";
	    				sortOrderStrVal		=	Order.desc(sortOrderFieldName);
	    			}
	    		}
	    		else
	    		{
	    			sortOrderTypeVal		=	"0";
	    			sortOrderStrVal			=	Order.asc(sortOrderFieldName);
	    		}
	    		/*=============== End ======================*/	
	    		
	    		TeacherDetail teacherDetail = teacherDetailDAO.findById(teacherId, false, false);
	    		
	    		List<TeacherAcademics> listTeacherAcademics = null;
	    		//listTeacherAcademics = teacherAcademicsDAO.findAcadamicDetailByTeacher(teacherDetail);
	    		listTeacherAcademics = teacherAcademicsDAO.findSortedAcadamicDetailByTeacher(sortOrderStrVal,teacherDetail);
	    		
	    		List<TeacherAcademics> sortedlistTeacherAcademics	=	new ArrayList<TeacherAcademics>();
	    		
	    		SortedMap<String,TeacherAcademics>	sortedMap = new TreeMap<String,TeacherAcademics>();
	    		if(sortOrderNoField.equals("degree"))
	    		{
	    			sortOrderFieldName	=	"degree";
	    		}
	    		if(sortOrderNoField.equals("fieldOfStudy"))
	    		{
	    			sortOrderFieldName	=	"fieldOfStudy";
	    		}
	    		if(sortOrderNoField.equals("university"))
	    		{
	    			sortOrderFieldName	=	"university";
	    		}
	    		int mapFlag=2;
	    		for (TeacherAcademics trAcademics : listTeacherAcademics){
	    			String orderFieldName=trAcademics.getAttendedInYear()+"";
	    			if(sortOrderFieldName.equals("degree")){
	    				orderFieldName=trAcademics.getDegreeId().getDegreeName()+"||"+trAcademics.getAcademicId();
	    				sortedMap.put(orderFieldName+"||",trAcademics);
	    				if(sortOrderTypeVal.equals("0")){
	    					mapFlag=0;
	    				}else{
	    					mapFlag=1;
	    				}
	    			}
	    			if(sortOrderFieldName.equals("fieldOfStudy")){
	    				orderFieldName=trAcademics.getFieldId().getFieldName()+"||"+trAcademics.getAcademicId();
	    				sortedMap.put(orderFieldName+"||",trAcademics);
	    				if(sortOrderTypeVal.equals("0")){
	    					mapFlag=0;
	    				}else{
	    					mapFlag=1;
	    				}
	    			}
	    			if(sortOrderFieldName.equals("university")){
	    				orderFieldName=trAcademics.getUniversityId().getUniversityName()+"||"+trAcademics.getAcademicId();
	    				sortedMap.put(orderFieldName+"||",trAcademics);
	    				if(sortOrderTypeVal.equals("0")){
	    					mapFlag=0;
	    				}else{
	    					mapFlag=1;
	    				}
	    			}
	    		}
	    		if(mapFlag==1){
	    			NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet();  
	    			for (Iterator iter=navig.iterator();iter.hasNext();) {  
	    				Object key = iter.next(); 
	    				sortedlistTeacherAcademics.add((TeacherAcademics) sortedMap.get(key));
	    			} 
	    		}else if(mapFlag==0){
	    			Iterator iterator = sortedMap.keySet().iterator();
	    			while (iterator.hasNext()) {
	    				Object key = iterator.next();
	    				sortedlistTeacherAcademics.add((TeacherAcademics) sortedMap.get(key));
	    			}
	    		}else{
	    			sortedlistTeacherAcademics=listTeacherAcademics;
	    		}
	    		
	    		totalRecord =sortedlistTeacherAcademics.size();

	    		if(totalRecord<end)
	    			end=totalRecord;
	    		List<TeacherAcademics> listsortedTeacherCertificates =	sortedlistTeacherAcademics.subList(start,end);
	    		
	    		sb.append("<table border='0' id='transcriptVerificationTab'>");
	    		sb.append("<thead class='bg'>");
	    		sb.append("<tr>");
	    		String responseText="";

	    		sb.append("<th width='20%' valign='top'>&nbsp;</th>");

	    		sb.append("<th width='20%' valign='top'>"+Utility.getLocaleValuePropByKey("optSchool", locale)+"</th>");

	    		sb.append("<th width='14%' valign='top'>"+Utility.getLocaleValuePropByKey("lblDegreeConferredDate", locale)+"</th>");

	    		sb.append("<th width='15%' valign='top'>"+Utility.getLocaleValuePropByKey("lblDgr", locale)+"</th>");

	    		sb.append("<th width='12%' valign='top'>"+Utility.getLocaleValuePropByKey("lblFildOfStudy", locale)+"</th>");

	    		sb.append("<th width='10%' class='net-header-text'>");
	    		sb.append(Utility.getLocaleValuePropByKey("lblStatus", locale));
	    		sb.append("</th>");			
	    		sb.append("</tr>");
	    		sb.append("</thead>");
	    		String fieldName = "";
	    		String degreeName ="";
	    		if(listTeacherAcademics!=null)
	    		for(TeacherAcademics ta:listsortedTeacherCertificates)
	    		{
	    			
	    			Long academicId	=	ta.getAcademicId();
	    			if(ta.getFieldId()!=null && ta.getFieldId().getFieldName() != null && ta.getFieldId().getFieldName() !=""){
	    				fieldName 	= 	ta.getFieldId().getFieldName();
	    			}
	    			if(ta.getDegreeId()!= null && ta.getDegreeId().getDegreeName() != null && ta.getDegreeId().getDegreeName() !=""){
	    				degreeName	=	ta.getDegreeId().getDegreeName();
	    			}
	    			
	    			sb.append("<tr>");

	    			sb.append("<td>");
	    			sb.append("<input type=\"checkbox\" name=\"transcript_data[]\" onclick='return showVerifyTranscript()' value="+academicId+">");	
	    			sb.append("</td>");			

	    			sb.append("<td>");
	    			if(ta.getUniversityId() != null){
	    				sb.append(ta.getUniversityId().getUniversityName());
	    			} else {
	    				sb.append("");
	    			}
	    			sb.append("</td>");

	    			sb.append("<td>");
	    			String status	=	ta.getStatus()==null?"":ta.getStatus();
	    			String dateShow	=	Utility.convertDateAndTimeToDatabaseformatOnlyDate(ta.getDegreeConferredDate());
	    			dateShow		=	dateShow==null?"":dateShow;
	    			if(ta.getDegreeConferredDate() != null){
	    				
	    				if(status.equals("V") && ta.getDegreeConferredDate() != null){
	    					sb.append("<input style=\"width:92px;\" type=\"text\" placeholder='mm-dd-yyyy' id=\"graduationDate_"+ta.getAcademicId()+"\" name=\"graduationDate_"+ta.getAcademicId()+"\"  maxlength=\"10\" value="+dateShow+" readonly=\"readonly\" class=\"help-inline form-control_1\">");
	    				} else {
	    					sb.append("<input style=\"width:92px;\" type=\"text\" placeholder='mm-dd-yyyy' id=\"graduationDate_"+ta.getAcademicId()+"\" name=\"graduationDate_"+ta.getAcademicId()+"\"  maxlength=\"10\" value="+dateShow+" class=\"help-inline form-control_1\">");
	    				}
	    			} else {
	    				if(status.equals("V") && ta.getDegreeConferredDate() != null){
	    					sb.append("<input style=\"width:92px;\" type=\"text\" placeholder='mm-dd-yyyy' id=\"graduationDate_"+ta.getAcademicId()+"\" name=\"graduationDate_"+ta.getAcademicId()+"\"  maxlength=\"10\" readonly=\"readonly\" class=\"help-inline form-control_1\">");
	    				} else {
	    					sb.append("<input style=\"width:92px;\" type=\"text\" placeholder='mm-dd-yyyy' id=\"graduationDate_"+ta.getAcademicId()+"\" name=\"graduationDate_"+ta.getAcademicId()+"\"  maxlength=\"10\" class=\"help-inline form-control_1\">");
	    				}
	    			}
	    			if(!status.equals("V")){
	    				sb.append("&nbsp;&nbsp;<a href=\"#\" onClick=\"canleGraduationDate("+ta.getAcademicId()+")\"><img src=\"images/red_cancel.png\"></a>");
	    			}
	    				
	    			sb.append("</td>");

	    			sb.append("<td>");
	    			sb.append(degreeName);
	    			sb.append("</td>");

	    			sb.append("<td>");
	    			sb.append(fieldName);	
	    			sb.append("</td>");

	    			sb.append("<td>");

	    			if(status == null || status.equals("") || status.equals("P")){
	    				status = "Pending";
	    			} else if(status.equals("V")){
	    				status = Utility.getLocaleValuePropByKey("lblVerified", locale);
	    			} else if(status.equals("F")){
	    				status = Utility.getLocaleValuePropByKey("lblFailed", locale);
	    			} else if(status.equals("W")){
	    				status = Utility.getLocaleValuePropByKey("lblWaived", locale);
	    			}
	    			sb.append(status);
	    			sb.append("</td>");
	    			sb.append("</tr>");
	    		}
	    		if(listTeacherAcademics==null || listTeacherAcademics.size()==0) {
	    			sb.append("<tr>");
	    			sb.append("<td colspan='7'>");
	    			sb.append(Utility.getLocaleValuePropByKey("msgNorecordfound", locale));
	    			sb.append("</td>");
	    			sb.append("</tr>");
	    		}
	    		sb.append("</table>");

	    		sb.append("<table border='0' id='academicGridAcademicNotes' cellspacing='3' cellpadding='3' style='display:none;'>");
	    		sb.append("<tr><td colspan=\"2\">&nbsp;</td></tr>");
	    		sb.append("<tr>");
	    		sb.append("<td>&nbsp;</td>");
	    		sb.append("<td>");
				sb.append("<input type=\"hidden\" id=\"teacherTranscript\" name=\"teacherTranscript\" value='"+teacherId+"'/>");
	    		sb.append("<button class='btn btn-primary' type='button' onclick=\"return updateTranscript('V')\">Verified</button>&nbsp;&nbsp;");
	    		sb.append("<button class='btn btn-primary' type='button' onclick=\"return updateTranscript('F')\">Failed</button>&nbsp;&nbsp;");
	    		sb.append("<button class='btn btn-primary' type='button' onclick=\"return updateTranscript('W')\">Waived</button>&nbsp;&nbsp;");
	    		sb.append("<a href='javascript:void(0);' onclick='cancelDiv()'>Cancel</a>");
	    		sb.append("</td>");
	    		sb.append("</tr>");
	    		sb.append("</table>");
	    		
	    	}catch(Exception exception){
	    		exception.printStackTrace();
	    	}
	    	
	    	
	    	System.out.println("::::::::::::::::::::::::::::::::::::::::::::");
	    	return sb.toString();
		}
	    
		public boolean updateTranscript(String status,Integer teacherId,String AcademicIDs) {
			System.out.println(":::::::::::::: updateTranscript() ::::::::::::::"+teacherId);
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session1 = request.getSession(false);
			if(session1 == null ||(session1.getAttribute("teacherDetail")==null && session1.getAttribute("userMaster")==null)) {
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}
			
			String arr[]=AcademicIDs.split("##");
			try{
				TeacherDetail teacherDetail = teacherDetailDAO.findById(teacherId, false, false);
				List<TeacherAcademics> listTeacherAcademics = null;
				listTeacherAcademics = teacherAcademicsDAO.findAcadamicDetailByTeacher(teacherDetail);
				String  academicStatus	=	"";
				
				if(listTeacherAcademics!=null && listTeacherAcademics.size()>0){
					for (TeacherAcademics trAcademics : listTeacherAcademics){
						try{
							if(trAcademics.getStatus()!=null){
								academicStatus = trAcademics.getStatus();
							}else{
								academicStatus = "P";
							}
						}catch(Exception e){
							academicStatus = "P";
						}
					}
				}
				
				if(arr.length>0) {
					for(int i=1;i<arr.length;i++) {
						TeacherAcademics academicDetail = teacherAcademicsDAO.findById(Long.valueOf(arr[i]), false, false);
						academicDetail.setStatus(status);
						teacherAcademicsDAO.makePersistent(academicDetail);
					}
				}
				
			} catch(Exception exception){
				exception.printStackTrace();
			}
			
			
			
			return true;
		}
		public String showProfileContentForTeacherNew(Integer teacherId,Integer noOfRecordCheck,String sVisitLocation,DistrictMaster districtMaster_Input,int fg)
		{
				System.out.println(" >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> showProfileContentForTeacher <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
				
				WebContext context;
				context = WebContextFactory.get();
				HttpServletRequest request = context.getHttpServletRequest();
				HttpSession session = request.getSession(false);
				if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
				{
					throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
				}
				DistrictMaster districtMaster=null;
				SchoolMaster schoolMaster=null;
				UserMaster userMaster=null;
				boolean nobleflag=false;
				int entityID=0,roleId=0;
				boolean noEPIFlag=false;
				boolean jeffcoFlag=false;
				if (session == null || session.getAttribute("userMaster") == null) {				
				}else{
					userMaster=(UserMaster)session.getAttribute("userMaster");
					if(userMaster.getRoleId().getRoleId()!=null){
						roleId=userMaster.getRoleId().getRoleId();
					}
					if(userMaster.getDistrictId()!=null){
						districtMaster=userMaster.getDistrictId();
						if(districtMaster.getNoEPI()==null ||(districtMaster.getNoEPI()!=null && districtMaster.getNoEPI()==false)){
							noEPIFlag=true;
						}

						if(districtMaster.getDistrictId()==7800038)
							nobleflag=true;
						//804800
						if(districtMaster.getDistrictId()==804800)
						jeffcoFlag=true;
					}
					if(userMaster.getSchoolId()!=null){
						schoolMaster =userMaster.getSchoolId();
					}	
					entityID=userMaster.getEntityType();
				}
				
				boolean achievementScore=false,tFA=false,demoClass=false,JSI=false,teachingOfYear =false,expectedSalary=false,fitScore=false,pnqDis=false;
				if(entityID==1)
				{
					achievementScore=true;tFA=true;demoClass=true;JSI=true;teachingOfYear =true;expectedSalary=true;fitScore=true;
				}else
				{
					try{
						DistrictMaster districtMasterObj=districtMaster;
						if(districtMasterObj!=null && districtMasterObj.getDisplayAchievementScore()!=null && districtMasterObj.getDisplayAchievementScore()){
							achievementScore=true;
						}
						if(districtMasterObj!=null && districtMasterObj.getDisplayTFA()!=null && districtMasterObj.getDisplayTFA()){
							tFA=true;
						}
						if(districtMasterObj!=null && districtMasterObj.getDisplayDemoClass()!=null && districtMasterObj.getDisplayDemoClass()){
							demoClass=true;
						}
						if(districtMasterObj!=null && districtMasterObj.getDisplayJSI()!=null && districtMasterObj.getDisplayJSI()){
							JSI=true;
						}
						if(districtMasterObj!=null && districtMasterObj.getDisplayYearsTeaching()!=null && districtMasterObj.getDisplayYearsTeaching()){
							teachingOfYear=true;
						}
						if(districtMasterObj!=null && districtMasterObj.getDisplayExpectedSalary()!=null && districtMasterObj.getDisplayExpectedSalary()){
							expectedSalary=true;
						}
						if(districtMasterObj!=null && districtMasterObj.getDisplayFitScore()!=null && districtMasterObj.getDisplayFitScore()){
							fitScore=true;
						}
						if(districtMasterObj!=null && districtMasterObj.getDistrictId()!=null && districtMasterObj.getDistrictId().equals(7800038)){
							pnqDis=true;
						}
					}catch(Exception e){ e.printStackTrace();}
				}

				StringBuilder sb = new StringBuilder();
				
				boolean headQuarterMaster =false;
				if(userMaster!=null && userMaster.getHeadQuarterMaster()!=null){
					headQuarterMaster=true;
				}
				String windowFunc="if(this.href!='javascript:void(0);')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
				try
				{
					String realPath = Utility.getValueOfPropByKey("teacherRootPath");
					String webPath = Utility.getBaseURL(request);
					String sWidthFoeDiv="765";

					String sDivCloseJsFunctionName="showProfileContentClose";
					if(sVisitLocation.equalsIgnoreCase("My Folders"))
						sDivCloseJsFunctionName="showProfileContentClose_ShareFolder";
					
					String imgPath="";
					TeacherDetail teacherDetail = null;
					try{
						teacherDetail = teacherDetailDAO.findById(teacherId,false,false);
						if(teacherDetail.getIdentityVerificationPicture()!=null){
							imgPath=CommonDashboardAjax.showPictureWeb(teacherDetail, realPath, webPath);
						}
					}catch(Exception e){
						e.printStackTrace();
					}
					
					String width="798px";
						if(userMaster.getEntityType().equals(2) || userMaster.getRoleId().getRoleId().equals(3))
						  width ="900px";
					
					if(!teacherDetail.getFirstName().equals("")){
						sb.append("<div class='modal-dialog-for-cgprofile' style='width: "+width+"'>"+
						"<div class='modal-content modal-border'>"+		
						"<div class='modal-header modal_header_profile' id='teacherDiv'> "+
						"<button type='button' class='close' data-dismiss='modal' onclick='"+sDivCloseJsFunctionName+"();'>x</button>"+
						"<h3>"+teacherDetail.getFirstName()+" "+teacherDetail.getLastName()+"'s Profile</h3>"+
						"</div>"+
						"<div class='modal-body scrollspy_profile' style='min-width:"+sWidthFoeDiv+"px;'>");
						}
					
					
					
					String showImg="";
					
					showImg="<img src=\"images/nophoto.png\" align='left' class='circle' style='width: 48px;height: 48px;margin: 0em auto;' >";
				
					if(fg==0){
						TestTool.getTraceSecond("start");
					SimpleDateFormat sdf=new SimpleDateFormat("MM-dd-yyyy");
					Date endStartDate=null,endToDate=null;
					
						List<String[]> minEndStartDateFromJobOrder=jobForTeacherDAO.findMinJobEndDateFromJobForteacher(teacherDetail, userMaster);
						for (Iterator it = minEndStartDateFromJobOrder.iterator(); it.hasNext();){
							 Object[] row = (Object[]) it.next();
							endStartDate = (Date) row[1];
							endToDate 	= (Date) row[0];
						}
					
					JobForTeacher jobForTeacher = null;
					String firstAppliedOn="";
					String lastAppliedOn="";
					Date lastActivityDate=null;
					String lastContactedOn="None";
					/*if(minEndStartDateFromJobOrder.size()>0)
					{
						
						
						firstAppliedOn = Utility.convertDateAndTimeToUSformatOnlyDate(endStartDate);
						lastAppliedOn = Utility.convertDateAndTimeToUSformatOnlyDate(endToDate);
						
						TestTool.getTraceSecond("lastcontactedon new start");
						try {
						
						List<String[]> lastConTacted=jobForTeacherDAO.findLastContactedOnJobForteacher(teacherDetail, userMaster,null);
						
						Long jftId =0L;
						for (Iterator it = lastConTacted.iterator(); it.hasNext();){
							 Object[] row = (Object[]) it.next();
							 jftId=Long.parseLong(row[1].toString());
						
						}
						
						JobForTeacher jobForTeacher2 =null;
						
						if(jftId!=0){
							jobForTeacher2 = jobForTeacherDAO.findById(jftId, false, false);
							lastActivityDate = jobForTeacher2.getLastActivityDate();
						}
						if(lastActivityDate!=null)
						{
							lastContactedOn ="<a href='javascript:void(0);'  onclick=\"getCommunicationsDiv(1,0,'"+teacherDetail.getTeacherId()+"',0);\" ><span class='divlablelinkval'>"+Utility.convertDateAndTimeToUSformatOnlyDate(lastActivityDate)+" ("+jobForTeacher2.getUserMaster().getFirstName()+" "+jobForTeacher2.getUserMaster().getLastName()+")</span></a>"; 
						}
						
						} catch (Exception e) {
							// TODO: handle exception
						}
						TestTool.getTraceSecond("lastcontactedonchange  new end");
						//List<JobForTeacher>  jobForTeachersLastContactedOn=new ArrayList<JobForTeacher>();						
						//jobForTeachersLastContactedOn=getJFTLastContactedOn(teacherDetail, userMaster,null, "Others");
						for (JobForTeacher jobForTeacher2 : jobForTeachersLastContactedOn) {
							lastActivityDate = jobForTeacher2.getLastActivityDate();
							if(lastActivityDate!=null)
							{
								lastContactedOn ="<a href='javascript:void(0);'  onclick=\"getCommunicationsDiv(1,0,'"+teacherDetail.getTeacherId()+"',0);\" ><span class='divlablelinkval'>"+Utility.convertDateAndTimeToUSformatOnlyDate(lastActivityDate)+" ("+jobForTeacher2.getUserMaster().getFirstName()+" "+jobForTeacher2.getUserMaster().getLastName()+")</span></a>"; 
								break;
							}
						}
					}*/ 
					List<TeacherDetail> lstTeacherDetails	  =	 new ArrayList<TeacherDetail>();
					lstTeacherDetails.add(teacherDetail);
					
					CandidateGridService cgService=new CandidateGridService();
					
					DistrictPortfolioConfig districtPortfolioConfig=null;
/*
					TeacherProfileVisitHistory teacherProfileVisitHistory=new TeacherProfileVisitHistory();
					teacherProfileVisitHistory.setUserMaster(userMaster);
					teacherProfileVisitHistory.setTeacherDetail(teacherDetail);
					teacherProfileVisitHistory.setJobOrder(null);
					teacherProfileVisitHistory.setVisitLocation(sVisitLocation);
					teacherProfileVisitHistory.setVisitedDateTime(new Date());
					teacherProfileVisitHistory.setIpAddress(IPAddressUtility.getIpAddress(request));
					
					if(userMaster.getDistrictId()!=null)
						teacherProfileVisitHistory.setDistrictId(userMaster.getDistrictId());
					if(userMaster.getSchoolId()!=null)
						teacherProfileVisitHistory.setSchoolId(userMaster.getSchoolId());
					
					teacherProfileVisitHistoryDAO.makePersistent(teacherProfileVisitHistory);
					// End Insert teacherprofilevisithistory
*/				
					//List for Willing to Substitute
					Criterion criterion = Restrictions.eq("teacherDetail",teacherDetail);
					List<TeacherExperience> lstTeacherExperience_new=teacherExperienceDAO.findByCriteria(criterion);

					Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail.getTeacherId());
					List<TeacherPersonalInfo> lstTeacherPersonalInfo_new=teacherPersonalInfoDAO.findByCriteria(criterion1);
					
					//System.out.println("New setting Start");
					
					// New setting Start
					sb.append("<div id='errorDiv' style='color:red;display:none;padding-left:60px;'></div>");
					sb.append("<table id='profile_id' width="+sWidthFoeDiv+" cellspacing=0 cellpadding=0 class='tablecgtbl'>");
					
						sb.append("<tr >");
							sb.append("<th style='padding-top:10px;text-align:left;'>");
							
								sb.append("<table border='0' cellspacing=0 cellpadding=0 class='tablecgtbl'>");
									sb.append("<tr>");
										String strCss_lbl="padding:2px;text-align:left;";
										String strCss_value="width=42px; vertical-algin:top;text-align:left;padding:0px;padding-left:5px;padding-right:5px;padding-top:5px;";
										sb.append("<th width=50 valign='top'>"); // Photo
											sb.append("<table border='0' cellspacing=0 cellpadding=0 class='tablecgtbl'>");
												sb.append("<tr><th style='"+strCss_value+"'>"+showImg+"</th></tr>");
											sb.append("</table>");
										sb.append("</th>");
										
										sb.append("<th style='text-align:left;'>");
											sb.append("<table border='0' cellspacing=0 cellpadding=0 class='tablecgtbl' style='border-spacing:0;'>");
											
											if(sVisitLocation.equalsIgnoreCase("My Folders"))
											{
												strCss_lbl="padding:0px;text-align:left;";
												strCss_value="padding:0px;text-align:left;";
											}else{
												strCss_lbl="padding:2px;text-align:left;";//OK
												strCss_value="padding:2px;text-align:left;";//OK
											}
											
												//Name:
												sb.append("<tr><th class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblName1+":</th><th class='divlableval' style='"+strCss_value+"'>&nbsp;"+teacherDetail.getFirstName()+" "+teacherDetail.getLastName()+"</th></tr>");
												
												/* @Start
												 * @Ashish Kumar
												 * @Description :: Add Address of Teacher
												 * */
												//TestTool.getTraceTimeTPAP("TPAP 205");
												
												TeacherPersonalInfo teacherPersonalInfo = null;
												if(lstTeacherPersonalInfo_new!=null && lstTeacherPersonalInfo_new.size()>0)
													teacherPersonalInfo = lstTeacherPersonalInfo_new.get(0);
												
												if(userMaster.getDistrictId()!=null && userMaster.getDistrictId().getDistrictId()!=null && userMaster.getDistrictId().getDistrictId()==1201470)	//OSCEOLA
													if(teacherPersonalInfo!=null)
														if(teacherPersonalInfo.getAnotherName()!=null)
															sb.append("<tr><th class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblAname+":</th><th class='divlableval' style='"+strCss_value+"'>&nbsp;"+teacherPersonalInfo.getAnotherName()+"</th></tr>");
														else
															sb.append("<tr><th class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblAname+":</th><th class='divlableval' style='"+strCss_value+"'>&nbsp;</th></tr>");
												if(userMaster.getDistrictId()!=null && userMaster.getDistrictId().getDistrictId()!=null && userMaster.getDistrictId().getDistrictId()==7800294)	//WAUKESHA 
													if(teacherPersonalInfo!=null)
														if(teacherPersonalInfo.getDistrictEmail()!=null)
															sb.append("<tr><td class='divlable' style='"+strCss_lbl+"'>&nbsp;District Email Address:</td><td class='divlableval' style='"+strCss_value+"'>&nbsp;"+teacherPersonalInfo.getDistrictEmail()+"</td></tr>");
														else
															sb.append("<tr><td class='divlable' style='"+strCss_lbl+"'>&nbsp;District Email Address:</td><td class='divlableval' style='"+strCss_value+"'>&nbsp;</td></tr>");												
												String sEmployeeNumber="N/A";
												String sEmployeePosition="N/A";
												if(teacherPersonalInfo!=null && teacherPersonalInfo.getEmployeeNumber()!=null && !teacherPersonalInfo.getEmployeeNumber().equalsIgnoreCase(""))
													sEmployeeNumber=teacherPersonalInfo.getEmployeeNumber();
												if(teacherPersonalInfo!=null && teacherPersonalInfo.getLastPositionWhenEmployed()!=null && !teacherPersonalInfo.getLastPositionWhenEmployed().equalsIgnoreCase(""))
													sEmployeePosition=teacherPersonalInfo.getLastPositionWhenEmployed();
												PrintOnConsole.debugPrintln("Teacher Profile2", "sEmployeeNumber "+sEmployeeNumber);
												PrintOnConsole.debugPrintln("Teacher Profile2", "sEmployeePosition "+sEmployeePosition);

												try{
													if(teacherPersonalInfo!=null)
													{
														String stateName = null;
														String country = null;
														String cityName = null;
														String zip = null;
														boolean addCHK = false;
														
														if(lstTeacherPersonalInfo_new.get(0).getCountryId()!=null){
															country=lstTeacherPersonalInfo_new.get(0).getCountryId().getName();
														}
														if(lstTeacherPersonalInfo_new.get(0).getStateId()!=null){
															stateName = lstTeacherPersonalInfo_new.get(0).getStateId().getStateName()+", ";
															//System.out.println("<--stateName-->"+stateName);
															addCHK = true;
														}else{
															stateName = "";
														}
														
														if(lstTeacherPersonalInfo_new.get(0).getCityId()!=null)
														{
															cityName = lstTeacherPersonalInfo_new.get(0).getCityId().getCityName()+", ";
															//System.out.println("<--cityName-->"+cityName);
															addCHK = true;
														}else{
															cityName = "";
														}
														
														if(lstTeacherPersonalInfo_new.get(0).getZipCode()!=null && !lstTeacherPersonalInfo_new.get(0).getZipCode().equals(""))
														{
															zip = lstTeacherPersonalInfo_new.get(0).getZipCode();
															//System.out.println("<--zip-->"+zip);
															addCHK = true;
														}else{
															zip = "";
														}
														
														if(lstTeacherPersonalInfo_new.get(0).getAddressLine1()!=null && !lstTeacherPersonalInfo_new.get(0).getAddressLine1().equals(""))
														{
															if(stateName=="" && cityName =="" && zip=="" && lstTeacherPersonalInfo_new.get(0).getAddressLine2().equals(""))
															{
																sb.append("<tr><th class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblAdd+":</th><th class='divlableval' style='"+strCss_value+"'>&nbsp;"+lstTeacherPersonalInfo_new.get(0).getAddressLine1()+"</th></tr>");
																//System.out.println("<--AddressLine1-->"+lstTeacherPersonalInfo_new.get(0).getAddressLine1());
															}
															else
															{
																sb.append("<tr><th class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblAdd+":</th><th class='divlableval' style='"+strCss_value+"'>&nbsp;"+lstTeacherPersonalInfo_new.get(0).getAddressLine1()+",</th></tr>");
															}
															
															addCHK = true;
														}
														
														if(lstTeacherPersonalInfo_new.get(0).getAddressLine2()!=null && !lstTeacherPersonalInfo_new.get(0).getAddressLine2().equals(""))
														{
															if(stateName=="" && cityName =="" && zip=="")
															{
																sb.append("<tr><th class='divlable' style='"+strCss_lbl+"'>&nbsp;</th><th class='divlableval' style='"+strCss_value+"'>&nbsp;"+lstTeacherPersonalInfo_new.get(0).getAddressLine2()+"</th></tr>");
																//System.out.println("<--AddressLine2-->"+lstTeacherPersonalInfo_new.get(0).getAddressLine2());
															}
															else
															{
																sb.append("<tr><th class='divlable' style='"+strCss_lbl+"'>&nbsp;</th><th class='divlableval' style='"+strCss_value+"'>&nbsp;"+lstTeacherPersonalInfo_new.get(0).getAddressLine2()+",</th></tr>");
															}
															addCHK = true;
														}
														
														if(addCHK==false)
														{
															sb.append("<tr><th class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblAdd+":</th><th class='divlableval' style='"+strCss_value+"'>&nbsp;N/A</th></tr>");
														}
														else
														{
															sb.append("<tr><th class='divlable' style='"+strCss_lbl+"'>&nbsp;</th><th class='divlableval' style='"+strCss_value+"'>&nbsp;"+stateName+""+cityName+""+zip+"</th></tr>");
															
															if(country!=""){
																sb.append("<tr><th class='divlable' style='"+strCss_lbl+"'>&nbsp;</th><th class='divlableval' style='"+strCss_value+"'>&nbsp;"+country+"</th></tr>");	
															}
														}
													}
												}catch(Exception e)
												{
													e.printStackTrace();
												}
													
												/* @End
												 * @Ashish Kumar
												 * @Description :: Add Address of Teacher
												 * */
													//System.out.println("EPI Norm");
												//EPI Norm
													//TestTool.getTraceTimeTPAP("TPAP 206");
												//Criterion criterionNS = Restrictions.eq("teacherId",teacherDetail);
												TestTool.getTraceSecond("Norm Score Start");
												//List<TeacherNormScore> teacherNormScoresList=teacherNormScoreDAO.findTeacersNormScoresList(lstTeacherDetails);
												int normScore=0;
												String colorName="";
												/*if(teacherNormScoresList.size()>0)
												{
													normScore=teacherNormScoresList.get(0).getTeacherNormScore();
													//double percentile=teacherNormScoresList.get(0).getPercentile();
													colorName =teacherNormScoresList.get(0).getDecileColor();
													//colorName =cGReportService.getColorName(percentile);
													//System.out.println("<--normScore--> "+normScore+"<--colorName--> "+colorName);
												}*/
												
												TestTool.getTraceSecond("Norm Score End");
												if(noEPIFlag){
													sb.append("<tr><th class='divlable' style='"+strCss_lbl+";padding-top:10px;'>&nbsp;"+lblEPINorm+":</th><th class='divlableval' style='"+strCss_value+";padding-top:10px;padding-bottom:8px;' id='epiScoreInPrf"+teacherId+"'>&nbsp;");	
													if(normScore==0)
													{
														sb.append("N/A</th>");
														
													}
													else
													{
														String ccsName="";
														String normScoreLen=normScore+"";
														if(normScoreLen.length()==1){
															ccsName="nobground1";
														}else if(normScoreLen.length()==2){
															ccsName="nobground2";
														}else{
															ccsName="nobground3";
														}
														sb.append("<span class=\""+ccsName+"\" style='font-size: 11px;font-weight: bold;background-color: #"+colorName+";'>"+normScore+"</span>");
														//System.out.println(">>>>  "+sb+" <<<");
														sb.append("</th></tr>");
													}
												}
												//Veteran candidate
												
												boolean ssnCheck=true;
												if(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==NC_HEADQUARTER){
													ssnCheck=false;
												}
												
												if(ssnCheck)
												 try{
												   String  lblVeteran   = Utility.getLocaleValuePropByKey("lblVeteran", locale);
													   if(userMaster.getDistrictId()!=null && !userMaster.getDistrictId().getDistrictId().equals(804800) && !userMaster.getDistrictId().getDistrictId().equals(7800294))
													      if(teacherPersonalInfo!=null){
																if(teacherPersonalInfo.getIsVateran()!=null && teacherPersonalInfo.getIsVateran()==1){
																sb.append("<tr><td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblVeteran+":</td><td class='divlableval' style='"+strCss_value+"'>&nbsp;Yes</td></tr>");
																}
																
																if(userMaster.getRoleId().getRoleId()==2 ||userMaster.getRoleId().getRoleId()==7 || userMaster.getRoleId().getRoleId()==8){
																	if(teacherPersonalInfo.getSSN()!=null && !teacherPersonalInfo.getSSN().equals("")){
																		if(userMaster.getEntityType()!=3)
																		sb.append("<tr><td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblSSN+":</td><td class='divlableval' style='"+strCss_value+"'>&nbsp;"+Utility.decodeBase64(teacherPersonalInfo.getSSN())+"</td></tr>");
																	}else{
																		if(userMaster.getEntityType()!=3)
																		sb.append("<tr><td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblSSN+":</td><td class='divlableval' style='"+strCss_value+"'>&nbsp;N/A</td></tr>");
																	}
																}
															}else{
																sb.append("<tr><td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblVeteran+":</td><td class='divlableval' style='"+strCss_value+"'>&nbsp;"+lblNo+"</td></tr>");
																if(userMaster.getRoleId().getRoleId()==2 ||userMaster.getRoleId().getRoleId()==7 || userMaster.getRoleId().getRoleId()==8){
																	sb.append("<tr><td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblSSN+":</td><td class='divlableval' style='"+strCss_value+"'>&nbsp;N/A</td></tr>");
																}
															}
													}catch (Exception e) {
														e.printStackTrace();
													}
													//System.out.println("<--IsVateran--> "+teacherPersonalInfo.getIsVateran());
												//EmployeeNumber
												if(jeffcoFlag && !sEmployeeNumber.trim().equalsIgnoreCase("N/A"))
												        sb.append("<tr><td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblEmpNum+"A1"+":</td><td class='divlableval' style='"+strCss_value+"'>&nbsp;<a class='tooltipEmpNo' data-original-title='Employee Information' href='javascript:void(0);' onclick='openEmployeeNumberInfo(\""+sEmployeeNumber+"\")'>"+sEmployeeNumber+"</a></td></tr>");
												else
													 if(!sEmployeeNumber.trim().equalsIgnoreCase("N/A")&&locale.equals("fr"))
														 sb.append("<tr><td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblEmpNum+"A1"+":</td><td class='divlableval' style='"+strCss_value+"'>&nbsp;<a class='tooltipEmpNo' data-original-title='Employee Information' href='javascript:void(0);' onclick='openEmployeeNumberInfoOther(\""+sEmployeeNumber+"\")'>"+sEmployeeNumber+"</a></td></tr>");	
													 else
														 sb.append("<tr><td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblEmpNum+":</td><td class='divlableval' style='"+strCss_value+"'>&nbsp;"+sEmployeeNumber+"</td></tr>");
												
												//Employee Position
												if(districtMaster!=null && districtMaster.getDistrictId().equals(7800294))
													sb.append("<tr><td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblEmpPosition+":</td><td class='divlableval' style='"+strCss_value+"'>&nbsp;"+sEmployeePosition+"</td></tr>");
												//First Job Applied On
												sb.append("<tr><th class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblFirstJobApplied+":</th><th class='divlableval' style='"+strCss_value+"' id='firstJobApplied"+teacherId+"'>&nbsp;"+firstAppliedOn+"</th></tr>");
												//Last Job Applied On
												sb.append("<tr><th class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblLastJobApplied+":</th><th class='divlableval' style='"+strCss_value+"' id='lastJobApplied"+teacherId+"'>&nbsp;"+lastAppliedOn+"</th></tr>");
												//Last Contacted On
												sb.append("<tr><th class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblLastContacted+":</th><th class='divlableval' style='"+strCss_value+"' id='lastContacted"+teacherId+"'>&nbsp;"+lastContactedOn+"</th></tr>");
												
												
												//Job Applied calculation
												//TestTool.getTraceTimeTPAP("TPAP 207");
												/*SortedMap<Long,JobForTeacher> jftMap = new TreeMap<Long,JobForTeacher>();
												SortedMap<Long,JobForTeacher> jftMapForTm = new TreeMap<Long,JobForTeacher>();*/
												
												/*SortedMap<Integer,Integer> jftMap = new TreeMap<Integer,Integer>();
												SortedMap<Integer,Integer> jftMapForTm = new TreeMap<Integer,Integer>();
												List<JobForTeacher> jobForTeachersList	  =	 new ArrayList<JobForTeacher>();
												List<JobOrder> lstJobOrderSLList =new ArrayList<JobOrder>();
												if(entityID==3){
													lstJobOrderSLList =schoolInJobOrderDAO.jobOrderPostedBySchool(schoolMaster);
												}
												List<Integer> statusIdList = new ArrayList<Integer>();
												String[] statusShrotName = {"comp","icomp","vlt","hird","scomp","ecomp","vcomp","dcln","rem","widrw"};
												try{
													statusIdList = statusMasterDAO.findStatusIdsByStatusByShortNames(statusShrotName);
												}catch (Exception e) {
													e.printStackTrace();
												}
												List<String[]> lstJobForTeachers =new ArrayList<String[]>();
												lstJobForTeachers=jobForTeacherDAO.getJobForTeacherByJobAndTeachersList(statusIdList,lstTeacherDetails,lstJobOrderSLList,districtMaster,entityID);
												if(lstJobForTeachers.size()>0){
													for (Iterator it = lstJobForTeachers.iterator(); it.hasNext();) {
														Object[] row = (Object[]) it.next()       ;                                                                 
														jftMap.put(Integer.parseInt(row[0].toString()),Integer.parseInt(row[1].toString()));
													}               
												}

												lstJobForTeachers=jobForTeacherDAO.getJobForTeacherByJobAndTeachersList(statusIdList,lstTeacherDetails,lstJobOrderSLList,districtMaster,1);
												if(lstJobForTeachers.size()>0){
													for (Iterator it = lstJobForTeachers.iterator(); it.hasNext();) {
														Object[] row = (Object[]) it.next()       ;                                                                 
														jftMapForTm.put(Integer.parseInt(row[0].toString()),Integer.parseInt(row[1].toString()));
													}               
												}
												
												int jobList=jftMap.get(teacherDetail.getTeacherId());
												int jobListForTm=jftMapForTm.get(teacherDetail.getTeacherId());*/
												
												//sb.append("<tr><th class='divlable' style='"+strCss_lbl+"'>&nbsp;Job Applied:</th><th class='divlableval' style='"+strCss_value+"'>&nbsp;<a href='javascript:void(0);' onclick=\"getJobList('"+teacherDetail.getTeacherId()+"')\" >"+jobList+"</a>"+"/"+(jobListForTm-jobList)+"<th></tr>");
												
												
												//National Board Certification
												//TestTool.getTraceTimeTPAP("TPAP 208");
												
		                                        TeacherExperience teacherExperience = null;
												
												if(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==NC_HEADQUARTER )
												{
													System.out.println("inside headquatermaster");
													
													List<TeacherPersonalInfo> ssnTeachersList = new ArrayList<TeacherPersonalInfo>();
													ArrayList<Integer> teacherIds = new ArrayList<Integer>();
													List<String> ssnList = new ArrayList<String>();
													List<LicensureNBPTS> licensureNBPTSList = new ArrayList<LicensureNBPTS>();
													
													teacherIds.add(teacherDetail.getTeacherId());
													if(teacherIds!=null && teacherIds.size()>0)
													{
														ssnTeachersList.add((lstTeacherPersonalInfo_new.get(0)));//teacherPersonalInfoDAO.findByTeacherIds(teacherIds);
													
													}
													
													if(ssnTeachersList!=null && ssnTeachersList.size()>0)
													{
														System.out.println("inside ssnTeachersList");
													  for(TeacherPersonalInfo tpi:ssnTeachersList){
														if(tpi.getSSN()!=null && !tpi.getSSN().equals("")){
															System.out.println("ssssnnnn"+tpi.getSSN());
															ssnList.add(tpi.getSSN());
															System.out.println("ssnList:::::::"+ssnList);
															
														}
														//System.out.println("ssnList sizeeeeeeeee::"+ssnList.size()+"ssnnnnnnnnnnnnnnnnnnn::::::"+ssnList);
													}
													}
																							
													if(ssnList!=null && ssnList.size()>0){
														
														licensureNBPTSList = licensureNBPTSDao.getDataBySsnList(ssnList);
													}
													if(licensureNBPTSList!=null && licensureNBPTSList.size()>0)
													{
														String fullDate ="";
														String finalYear="";
														Format formatter = new SimpleDateFormat("yyyy-MM-dd");
														
														if(licensureNBPTSList.get(0).getExpirationDate()!=null)
														 fullDate = formatter.format(licensureNBPTSList.get(0).getExpirationDate());
														if(!fullDate.equals(""))
														 finalYear=fullDate.substring(0, 4);
														
														System.out.println("NBTS  cetri::::::::::YES");
														sb.append("<th class='divlable' style='"+strCss_lbl+"'>&nbsp;"+Utility.getLocaleValuePropByKey("lblNatiBoardCertiLice", locale)+":</th>");
														sb.append("<th  class='divlableval' style='"+strCss_value+"'>&nbsp;Y ("+finalYear+")</th>");
														
													}
													else
													{
														System.out.println("NBTS  cetri::::::::::NOOOOOOOOOO");
														sb.append("<th class='divlable' style='"+strCss_lbl+"'>&nbsp;"+Utility.getLocaleValuePropByKey("lblNatiBoardCertiLice", locale)+":</th>");
														sb.append("<th  class='divlableval' style='"+strCss_value+"'>&nbsp;N</th>");
													}
													
													
												}else{
													if(lstTeacherExperience_new!=null && lstTeacherExperience_new.size()>0)
														teacherExperience = lstTeacherExperience_new.get(0);//teacherExperienceDAO.findTeacherExperienceByTeacher(teacherDetail);
													sb.append("<tr>");
													
													
													sb.append("<th class='divlable' style='"+strCss_lbl+"'>&nbsp;"+Utility.getLocaleValuePropByKey("lblNatiBoardCertiLice", locale)+"</th>");
													
													
													if(teacherExperience!=null)
													{
														if(teacherExperience.getNationalBoardCert()==null)
														{
															sb.append("<th  class='divlableval' style='"+strCss_value+"'>&nbsp;N/A</th>");
														}
														else
														{
															if(teacherExperience.getNationalBoardCert()==true)
															{
																String sNationalBoardCertYear="";
																if(teacherExperience.getNationalBoardCertYear()!=null && !teacherExperience.getNationalBoardCertYear().equals(""))
																	sNationalBoardCertYear=teacherExperience.getNationalBoardCertYear().toString();
																sb.append("<th  class='divlableval' style='"+strCss_value+"'>&nbsp;Y ("+sNationalBoardCertYear+")</th>");
															}
															else if(teacherExperience.getNationalBoardCert()==false)
																sb.append("<th  class='divlableval' style='"+strCss_value+"'>&nbsp;N</th>");
														}
													}
													else
													{
														sb.append("<th  class='divlableval' style='"+strCss_value+"'>&nbsp;N/A</th>");
													}
														
													sb.append("</tr>");
												}
												
											sb.append("</table>");
										sb.append("</th>");
										
										sb.append("<th>");
											sb.append("<table border='0' cellspacing=0 cellpadding=0 class='tablecgtbl' style='border-spacing:0;'>");
											
											if(sVisitLocation.equalsIgnoreCase("My Folders"))
											{
												strCss_lbl="padding-left:20px;text-align:left;white-space:nowrap";
												strCss_value="padding-left:10px;text-align:left;white-space:nowrap";
											}
											else
											{
												strCss_lbl="padding:2px;padding-left:20px;text-align:left;white-space:nowrap";
												strCss_value="padding:2px;padding-left:10px;text-align:left;white-space:nowrap";
											}
												//TFA
												if(tFA)
												{
													sb.append("<tr>");
													sb.append("<th class='divlable' style='"+strCss_lbl+"'>&nbsp;TFA:</th>");
													String tFAVal = getTFAValue(lstTeacherExperience_new,teacherDetail);
													
													String New_tFAVal = "";
													try
													{
														if(tFAVal!=null)
														{
															if(tfaEditAuthUser())
															{
																if(lstTeacherExperience_new.size()>0)
																	New_tFAVal = tFAVal+"<span id='edittfa'>&nbsp;&nbsp;<a href='javascript:void(0);' onclick='editTFAbyUser("+lstTeacherExperience_new.get(0).getTeacherId().getTeacherId()+");'>Edit</a></span><span id='savetfa' style='display:none; cursor:pointer;'>&nbsp;&nbsp;<a javascript:void(0); onclick='displayTFAMessageBox("+lstTeacherExperience_new.get(0).getTeacherId().getTeacherId()+");'>Save</a>&nbsp;</span><span id='canceltfa' style='display:none; cursor:pointer;'><a javascript:void(0); onclick='cancelTFAbyUser("+lstTeacherExperience_new.get(0).getTeacherId().getTeacherId()+");'>Cancel</a></span>";	
																else
																	New_tFAVal = tFAVal;
															}
															else
																New_tFAVal = tFAVal;
														}
														else
														{
															if(tfaEditAuthUser())
															{
																if(lstTeacherExperience_new.size()>0)
																	New_tFAVal = "N/A "+"<span id='edittfa'>&nbsp;&nbsp;<a href='javascript:void(0);' onclick='editTFAbyUser("+lstTeacherExperience_new.get(0).getTeacherId().getTeacherId()+");'>Edit</a></span><span id='savetfa' style='display:none;cursor:pointer;'>&nbsp;&nbsp;<a javascript:void(0); onclick='displayTFAMessageBox("+lstTeacherExperience_new.get(0).getTeacherId().getTeacherId()+");'>Save</a>&nbsp;</span><span id='canceltfa' style='display:none; cursor:pointer;'><a javascript:void(0); onclick='cancelTFAbyUser("+lstTeacherExperience_new.get(0).getTeacherId().getTeacherId()+");'>Cancel</a></span>";
																else
																	New_tFAVal = "N/A";
															}
															else
																New_tFAVal = "N/A";
														}
													}catch(Exception e)
													{
														e.printStackTrace();
													}
													sb.append("<th  class='divlableval' style='"+strCss_value+"'>&nbsp;"+New_tFAVal+"</th>");
													sb.append("</tr>");
													if(tFAVal!=null && lstTeacherExperience_new!=null && lstTeacherExperience_new.size()>0 && lstTeacherExperience_new.get(0)!=null)
													{
														sb.append("<tr>");
														sb.append("<th class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblCorpsYear+":</th>");
														if(lstTeacherExperience_new.get(0).getCorpsYear()!=null)
															sb.append("<th class='divlableval' style='"+strCss_value+"'>&nbsp;"+lstTeacherExperience_new.get(0).getCorpsYear()+"</th>");
														else
															sb.append("<th class='divlableval' style='"+strCss_value+"'>&nbsp;N/A</th>");
														sb.append("</tr>");
														
														sb.append("<tr>");
														sb.append("<th class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblTFARegion+":</th>");
														if(lstTeacherExperience_new.get(0).getTfaRegionMaster()!=null)
															sb.append("<th class='divlableval' style='"+strCss_value+"'>&nbsp;"+lstTeacherExperience_new.get(0).getTfaRegionMaster().getTfaRegionName()+"</th>");
														else
															sb.append("<th class='divlableval' style='"+strCss_value+"'>&nbsp;N/A</th>");
														sb.append("</tr>");
													}
													else
													{
														sb.append("<tr>");
														sb.append("<th class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblCorpsYear+":</th>");
														sb.append("<th class='divlableval' style='"+strCss_value+"'>&nbsp;N/A</th>");
														sb.append("</tr>");
														
														sb.append("<tr>");
														sb.append("<th class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblTFARegion+":</th>");
														sb.append("<th class='divlableval' style='"+strCss_value+"'>&nbsp;N/A</th>");
														sb.append("</tr>");
													}
												}
												//System.out.println("<--CorpsYear--> "+lstTeacherExperience_new.get(0).getCorpsYear());
												//TestTool.getTraceTimeTPAP("TPAP 209");
												//Teaching Years
												if(teachingOfYear){
													sb.append("<tr>");
													sb.append("<th class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblTeachingYears+":</th>");
													if(cgService.getTEYear(lstTeacherExperience_new,teacherDetail)!=0)
														sb.append("<th  class='divlableval' style='"+strCss_value+"'>&nbsp;"+cgService.getTEYear(lstTeacherExperience_new,teacherDetail)+"</th>");
													else
														sb.append("<th  class='divlableval' style='"+strCss_value+"'>&nbsp;N/A</th>");
													sb.append("</tr>");
												}
												//Expected Salary
												if(expectedSalary){
													sb.append("<tr>");
													sb.append("<th class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblExpectedSalary+":</th>");
													if(cgService.getSalary(lstTeacherPersonalInfo_new,teacherDetail)!=-1){
															sb.append("<th  class='divlableval' style='"+strCss_value+"'>&nbsp;$"+cgService.getSalary(lstTeacherPersonalInfo_new,teacherDetail)+"</th>");
													}else
														sb.append("<th  class='divlableval' style='"+strCss_value+"'>&nbsp;N/A</th>");
													sb.append("</tr>");
												}
												
												//# of Views:
												sb.append("<tr>");									
												Criterion criterion_teacherDetail = Restrictions.eq("teacherDetail",teacherDetail);
												/*List<TeacherProfileVisitHistory> listTeacherProfileVisitHistory=null;
												if(entityID==1)
													listTeacherProfileVisitHistory = teacherProfileVisitHistoryDAO.findByCriteria(criterion_teacherDetail);
												else 
												{
													Criterion criterion_districtId = Restrictions.eq("districtId",districtMaster);
													listTeacherProfileVisitHistory = teacherProfileVisitHistoryDAO.findByCriteria(criterion_teacherDetail,criterion_districtId);
												}
																								
												int iTeacherProfileVisitHistoryCount=0;
												if(listTeacherProfileVisitHistory.size()>0)
													iTeacherProfileVisitHistoryCount=listTeacherProfileVisitHistory.size();
												
												sb.append("<th class='divlable1' style='"+strCss_lbl+"'>&nbsp;"+lblOfViews+":</th>");
												if(iTeacherProfileVisitHistoryCount>0)
													sb.append("<th  class='divlableval1' style='"+strCss_lbl+"'><a data-original-title='"+lblClickToViewTPVisit+"' rel='tooltip' id='tpTeacherProfileVisitHistory' href='javascript:void(0);' onclick=\"getTeacherProfileVisitHistoryShow_FTime('"+teacherDetail.getTeacherId()+"');\" ><span class='divlablelinkval1'>&nbsp;"+iTeacherProfileVisitHistoryCount+"</span></a></th>");
												*/
												
												int iTeacherProfileVisitHistoryCount=0;
												sb.append("<th class='divlable1' style='"+strCss_lbl+"'>&nbsp;"+lblOfViews+":</th>");
												sb.append("<th  class='divlableval1' style='"+strCss_lbl+"'><a data-original-title='"+lblClickToViewTPVisit+"' style='pointer-events:none;' class='nOofViews"+teacherId+"' rel='tooltip' id='tpTeacherProfileVisitHistory' href='javascript:void(0);' onclick=\"getTeacherProfileVisitHistoryShow_FTime('"+teacherDetail.getTeacherId()+"');\" ><span class='divlablelinkval1'>&nbsp;"+iTeacherProfileVisitHistoryCount+"</span></a></th>");
												sb.append("</tr>");
												
												//Job Applied View
												sb.append("<tr>");
												sb.append("<th class='divlable1' style='"+strCss_lbl+"'>&nbsp;"+lblJobApplied+":</th>");
												
												sb.append("<th class='divlableval1' style='"+strCss_lbl+"' id='jobListByTProfile"+teacherDetail.getTeacherId()+"'>0/0</th>");
												/*if(jobList!=0){
													sb.append("<a data-original-title='"+lblClickToViewJO+"' rel='tooltip' id='teacherProfileTooltip' href='javascript:void(0);' onclick=\"getJobList('"+teacherDetail.getTeacherId()+"')\" ><span class='divlablelinkval1'>&nbsp;"+jobList+"</span></a>");
												}else{
													sb.append("&nbsp;0");
												}
												sb.append("/"+(jobListForTm-jobList)+"</th>");*/
												
												sb.append("</tr>");
												
												//Willing to Substitute
												sb.append("<tr>");
												sb.append("<th class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblWillingToSubsti+":</th>");
												if(lstTeacherExperience_new!=null && lstTeacherExperience_new.size()>0)
												{
													if(lstTeacherExperience_new.get(0).getCanServeAsSubTeacher()==2)
														sb.append("<th  class='divlableval' style='"+strCss_value+"'>&nbsp;N/A</th>");
													if(lstTeacherExperience_new.get(0).getCanServeAsSubTeacher()==0)
														sb.append("<th  class='divlableval' style='"+strCss_value+"'>&nbsp;N</th>");
													if(lstTeacherExperience_new.get(0).getCanServeAsSubTeacher()==1)
														sb.append("<th  class='divlableval' style='"+strCss_value+"'>&nbsp;Y</th>");
												}
												else
												{
													sb.append("<th  class='divlableval' style='"+strCss_value+"'>&nbsp;N/A</th>");
												}
												sb.append("</tr>");
												
												//KSN ID
												if( (userMaster.getEntityType()==6 && userMaster.getRoleId().getRoleId()==11) || (userMaster.getEntityType()==5 && userMaster.getRoleId().getRoleId()==10)){sb.append("<tr>");
												sb.append("<td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblKSNID+":</td>");
												MQEvent mqEvent = null;
												try{
													mqEvent = mqEventDAO.findLinkToKSNIniatedByTeacher(teacherDetail);
												}catch(Exception ex){
													ex.printStackTrace();
												}
												sb.append("<td  class='divlableval' style='"+strCss_value+"'>");
												sb.append("<div id='ksndetails'>");
												sb.append("<input type='hidden' id='disconnectTalentId' name='disconnectTalentId' value='"+teacherDetail.getTeacherId()+"' />");
												if(mqEvent!=null && mqEvent.getStatus()!=null && teacherDetail.getKSNID()==null){
													if(mqEvent.getStatus().equalsIgnoreCase("R")){
														if(mqEvent.getAckStatus()!=null && mqEvent.getAckStatus().contains("Success")){
															sb.append("Link to KSN is in progress");
														}
														else if(mqEvent.getAckStatus()!=null && mqEvent.getAckStatus().contains("Fail")){
															sb.append("<table><tr>");
															sb.append("<td  class='divlableval' style='"+strCss_value+"'>");
															sb.append("<input type='text' id='inputKSN' class='form-control' value='' style='height:30px;' maxlength=10 onkeypress='return checkForInt(event);'/>");
															sb.append("</td>");
															sb.append("<td>");
															sb.append("<a data-original-title='Link to KSN' rel='tooltip' id='tpDisconnect' class='tpDisconnect' style='padding-left:8px;' href='javascript:void(0);' onclick='disconnectTalent();'><i class='icon-unlock icon-large'></i></a>");
															sb.append("</td>");
															sb.append("<script>$('#tpDisconnect').tooltip();</script>");
															sb.append("</tr><table>");
														}
														else{
															sb.append("Link to KSN is in progress");
														}
													}
												}
												else if(teacherDetail.getKSNID()==null){
													sb.append("<table><tr><td>");
													//sb.append("<input type='hidden' id='disconnectTalentId' name='disconnectTalentId' value='"+teacherDetail.getTeacherId()+"' />");
													sb.append("<input type='text' id='inputKSN' class='form-control' value='' style='height:30px;' maxlength=10 onkeypress='return checkForInt(event);'/>");
													sb.append("</td>");
													sb.append("<td>");
													sb.append("<a data-original-title='Link to KSN' rel='tooltip' id='tpDisconnect' class='tpDisconnect' style='padding-left:8px;' href='javascript:void(0);' onclick='disconnectTalent();'><i class='icon-unlock icon-large'></i></a>");
													sb.append("</td>");
													sb.append("<script>$('#tpDisconnect').tooltip();</script>");
													sb.append("</tr></table>");
												}
												else if(teacherDetail.getKSNID()!=null){
													sb.append("<table><tr>");
													sb.append("<td>"+teacherDetail.getKSNID()+"</td>");
													sb.append("<td>");
													sb.append("<a data-original-title='Linked to KSN' rel='tooltip' id='tpDisconnect' style='padding-left:8px;' href='javascript:void(0);'><i class='icon-lock icon-large'></i></a>");
													sb.append("</td>");
													sb.append("<script>$('#tpDisconnect').tooltip();</script>");
													sb.append("</tr></table>");
												}
												sb.append("</div>");
												sb.append("</td>");
												sb.append("</tr>");
												}
												
												//sssnnnnnnnnnn in applicant pool
												if(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==NC_HEADQUARTER){
												sb.append("<tr>");
												if(userMaster.getEntityType()!=3)
												sb.append("<th class='divlable' style='"+strCss_lbl+"'>&nbsp;Last 4 of SSN:</th>");
												
												if(teacherPersonalInfo!=null)
												if(teacherPersonalInfo.getSSN()!=null && !teacherPersonalInfo.getSSN().equals("")){
													String ssnFour=Utility.decodeBase64(teacherPersonalInfo.getSSN());
													String finalSSN="";
													try
													{
													 finalSSN=ssnFour.substring(5, 9);
													System.out.println("Last four digit of ssn:"+finalSSN);
													}catch(Exception e){}
														sb.append("<th  class='divlableval' style='"+strCss_value+"'>&nbsp;"+finalSSN+"</th>");
													}else{
														sb.append("<th  class='divlableval' style='"+strCss_value+"'>&nbsp;N/A</th>");
													}
												
												sb.append("</tr>");
												}
												
												
											sb.append("</table>");
										sb.append("</th>");
										
									sb.append("</tr>");
								sb.append("</table>");
								
								
								
							sb.append("</th>");
						sb.append("</tr>");
						
						// start new row
						
						sb.append("<tr>");
							sb.append("<th>");
								sb.append("<table border='0' cellspacing=0 cellpadding=0>");
								sb.append("<tr>");
									// Blank Cell
									sb.append("<th width=50>&nbsp;</th>");
									
									strCss_lbl="padding:4px;vertical-align:middle;";
									strCss_value="padding:2px;text-align:left;vertical-align:text-top;";
									
									String Resume_column_width="";
									String Phone_column_width="";
									String PDReport_column_width="";
									
									if(sVisitLocation.equalsIgnoreCase("Teacher Pool")||sVisitLocation.equalsIgnoreCase("PNR Dashboard"))
									{
										sb.append("<th style='text-align:left;'>");//OK
										Resume_column_width=";width:44px;";
										Phone_column_width=";width:46px;";
										PDReport_column_width=";width:340px;";
									}
									else if(sVisitLocation.equalsIgnoreCase("Mosaic"))
									{
										sb.append("<th style='text-align:left;'>"); //OK
									}
									else if(sVisitLocation.equalsIgnoreCase("My Folders"))
									{
										sb.append("<th style='text-align:left;'>"); //)OK
									}
									
									
									sb.append("<table border='0' cellspacing=0 cellpadding=0");
									
									// Resume
									sb.append("<tr>");
									
									sb.append("<th class='divlable' style='"+strCss_lbl+""+Resume_column_width+"'>&nbsp;&nbsp;&nbsp;"+lnkResume+":</th>");
									if(teacherExperience!=null && teacherExperience.getResume()!=null && teacherExperience.getResume()!="")
										sb.append("<th  class='divlableval' style='"+strCss_value+"'>&nbsp;<a data-original-title='"+lnkResume+"' rel='tooltip' id='tpResumeprofile' href='javascript:void(0);' onclick=\"downloadResume('"+teacherDetail.getTeacherId()+"','tpResumeprofile');"+windowFunc+"\" ><span class='icon-briefcase icon-large iconcolor'></span></a></th>");
									else
										sb.append("<th  class='divlableval' style='"+strCss_value+"'>&nbsp;<a data-original-title='"+lblNotAvailable+"' rel='tooltip' id='tpResumeprofile'><span class='icon-briefcase icon-large iconcolorhover'></span></a></th>");
									
									if(sVisitLocation.equalsIgnoreCase("Teacher Pool")||sVisitLocation.equalsIgnoreCase("PNR Dashboard"))
										strCss_lbl="padding:2px;padding-left:118px;vertical-align:middle;text-align:right;";//OK // @AShish :: change padding-left from 200 to 110
									else if(sVisitLocation.equalsIgnoreCase("Mosaic"))
										strCss_lbl="padding:2px;padding-left:118px;vertical-align:middle;text-align:right;"; //OK // @Ashish :: change padding-left from 200 to 110
									else if(sVisitLocation.equalsIgnoreCase("My Folders"))
										strCss_lbl="padding:2px;padding-left:190px;vertical-align:middle;text-align:right;"; // OK
									
									TestTool.getTraceSecond("Assessment Start");
									Map<Integer,TeacherAssessmentStatus> baseTakenMap = new HashMap<Integer, TeacherAssessmentStatus>();
									List<TeacherAssessmentStatus> teacherAssessmentStatus = null;
									Map<Integer, Integer> isbaseComplete	=	new HashMap<Integer, Integer>();
									
									Integer isBase = null;
									try{
										if(lstTeacherDetails!=null && lstTeacherDetails.size()>0){
											teacherAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentTakenByTeachers(lstTeacherDetails);
											for (TeacherAssessmentStatus teacherAssessmentStatus2 : teacherAssessmentStatus) {					
												baseTakenMap.put(teacherAssessmentStatus2.getTeacherDetail().getTeacherId(), teacherAssessmentStatus2);
												boolean cgflag = teacherAssessmentStatus2.getCgUpdated()==null?false:teacherAssessmentStatus2.getCgUpdated();
												if(cgflag && teacherAssessmentStatus2.getStatusMaster().getStatusShortName().equalsIgnoreCase("comp"))
													isbaseComplete.put(teacherAssessmentStatus2.getTeacherDetail().getTeacherId(), 1);
												else
													if(teacherAssessmentStatus2.getStatusMaster().getStatusShortName().equalsIgnoreCase("vlt"))
														isbaseComplete.put(teacherAssessmentStatus2.getTeacherDetail().getTeacherId(), 2);
											}
										}
									}catch(Exception e){
										e.printStackTrace();
									}
									TestTool.getTraceSecond("Assessment End");
									isBase = isbaseComplete.get(teacherDetail.getTeacherId());
									sb.append("<th class='divlable' style='"+strCss_lbl+""+PDReport_column_width+"'>&nbsp;"+headPdRep+": </th>");
									if(isBase!=null && isBase==1)
										sb.append("<th  class='divlableval' style='"+strCss_value+"'>&nbsp;<a data-original-title='"+headPdRep+"' rel='tooltip' id='tpPDReportprofile' href='javascript:void(0);' onclick=\"generatePDReport('"+teacherDetail.getTeacherId()+"','tpPDReportprofile');"+windowFunc+"\"><span class='icon-book icon-large iconcolor'></span></a></th>");
									else
										sb.append("<th  class='divlableval' style='"+strCss_value+"'>&nbsp;<a data-original-title='"+lblNotAvailable+"' rel='tooltip' id='tpPDReportprofile' ><span class='icon-book icon-large iconcolorhover'></span></a></th>");
									
									sb.append("</tr>");
							
									sb.append("</table>");
									
								sb.append("</th>");
								sb.append("</tr>");
								sb.append("</table>");
							
							sb.append("</th>");
						sb.append("</tr>");
						// End new row
						
						/////////////////
						
						sb.append("<tr>");
						sb.append("<th>");
							sb.append("<table border='0' cellspacing=0 cellpadding=0 style='margin-top:-10px;'>");
							sb.append("<tr>");
								// Blank Cell
								sb.append("<th width=50>&nbsp;</th>");
								
								strCss_lbl="padding:10px;vertical-align:middle;padding-right:70px;";
								if(sVisitLocation.equalsIgnoreCase("Teacher Pool") ||sVisitLocation.equalsIgnoreCase("PNR Dashboard"))
								{
									sb.append("<th style='text-align:left;'>");//OK
									Phone_column_width=";width:189px;";
									PDReport_column_width=";width:100px;";
								}
								else if(sVisitLocation.equalsIgnoreCase("Mosaic"))
								{
									sb.append("<th style='text-align:left;'>"); //OK
								}
								else if(sVisitLocation.equalsIgnoreCase("My Folders"))
								{
									sb.append("<th style='text-align:left;'>"); //)OK
								}
								
								
								sb.append("<table border='0' cellspacing=0 cellpadding=0");
								
								// Resume
								sb.append("<tr>");
							
								//Phone:MyFolder
								//TestTool.getTraceTimeTPAP("TPAP 211");
								List<TeacherPersonalInfo> lstTeacherPersonalInfo1	  =	 new ArrayList<TeacherPersonalInfo>();
								if(lstTeacherPersonalInfo_new!=null && lstTeacherPersonalInfo_new.size()>0)
								lstTeacherPersonalInfo1.add(lstTeacherPersonalInfo_new.get(0));
								if(sVisitLocation.equalsIgnoreCase("Teacher Pool") ||sVisitLocation.equalsIgnoreCase("PNR Dashboard"))
								{
									strCss_value="padding:10px;text-align:left;vertical-align:text-top;padding-right:73px;";
									sb.append("<th class='divlable' style='"+strCss_lbl+""+Phone_column_width+"'>&nbsp;"+lblPhoneNo+":</th>");
									if(lstTeacherPersonalInfo1.size()==1)
									{
										if((lstTeacherPersonalInfo1.get(0).getPhoneNumber()!=null && !lstTeacherPersonalInfo1.get(0).getPhoneNumber().equalsIgnoreCase("")))
											sb.append("<th  class='divlableval' style='"+strCss_value+";margin-top:-5px;padding-left:12px;'>"+lstTeacherPersonalInfo1.get(0).getPhoneNumber()+"</th>");	
										else
											sb.append("<th  class='divlableval' style='"+strCss_value+";margin-top:-5px;padding-left:5px;'><span class='span2'>&nbsp;N/A</th>");
									}
									else
										sb.append("<th  class='divlableval' style='"+strCss_value+";margin-top:-5px;padding-left:5px;'>&nbsp;N/A</th>");
									
									// PD Report
									
									isBase = isbaseComplete.get(teacherDetail.getTeacherId());
									sb.append("<th class='divlable' style='"+strCss_lbl+""+PDReport_column_width+"'>&nbsp;"+lblMobileNumber+": </th>");
									if((lstTeacherPersonalInfo1.size()==1 && lstTeacherPersonalInfo1.get(0).getMobileNumber()!=null && !lstTeacherPersonalInfo1.get(0).getMobileNumber().equalsIgnoreCase("")))
										sb.append("<th  class='divlableval' style='"+strCss_value+";margin-top:-5px;padding-left:30px;'>"+lstTeacherPersonalInfo1.get(0).getMobileNumber()+"</th>");
									else
										sb.append("<th  class='divlableval' style='"+strCss_value+";margin-top:-5px;padding-left:30px;'>&nbsp;N/A</th>");
									
									sb.append("</tr>");
								}else if(sVisitLocation.equalsIgnoreCase("My Folders"))
								{
									strCss_value="padding:10px;text-align:left;vertical-align:text-top;";
									sb.append("<th class='divlable' style='"+strCss_lbl+""+Phone_column_width+"'>&nbsp;"+lblPhoneNo+": </th>");
										if(lstTeacherPersonalInfo1.size()==1)
										{
											if((lstTeacherPersonalInfo1.get(0).getPhoneNumber()!=null && !lstTeacherPersonalInfo1.get(0).getPhoneNumber().equalsIgnoreCase("")))
												sb.append("<th  class='divlableval' style='"+strCss_value+";margin-top:-5px;padding-left:12px;'><span class='span2'>"+lstTeacherPersonalInfo1.get(0).getPhoneNumber()+"</span></th>");	
											else
												sb.append("<th  class='divlableval' style='"+strCss_value+";margin-top:-5px;padding-left:5px;'><span class='span2'>&nbsp;N/A</span></th>");
										}
										else
											sb.append("<th  class='divlableval' style='"+strCss_value+";margin-top:-5px;padding-left:5px;'><span class='span2'>&nbsp;N/A</span></th>");
										
										// PD Report
										
										isBase = isbaseComplete.get(teacherDetail.getTeacherId());
										sb.append("<th class='divlable' style='"+strCss_lbl+""+PDReport_column_width+"'>&nbsp;"+lblMobileNumber+": </th>");
										if((lstTeacherPersonalInfo1.size()==1 && lstTeacherPersonalInfo1.get(0).getMobileNumber()!=null && !lstTeacherPersonalInfo1.get(0).getMobileNumber().equalsIgnoreCase("")))
											sb.append("<th  class='divlableval' style='"+strCss_value+";padding-right:70px;margin-top:-5px;padding-left:5px;'><span style='width:40px;'>"+lstTeacherPersonalInfo1.get(0).getMobileNumber()+"</span></th>");
										else
											sb.append("<th  class='divlableval' style='"+strCss_value+";padding-right:70px;margin-top:-5px;padding-left:30px;'><span style='width:40px;'>&nbsp;N/A</span></th>");
										
										sb.append("</tr>");
								}else if(sVisitLocation.equalsIgnoreCase("Mosaic"))
								{
									strCss_value="padding:10px;text-align:left;vertical-align:text-top;";
									sb.append("<th class='divlable' style='"+strCss_lbl+""+Phone_column_width+"'>&nbsp;Phone Number: </th>");
										if(lstTeacherPersonalInfo1.size()==1)
										{
											if((lstTeacherPersonalInfo1.get(0).getPhoneNumber()!=null && !lstTeacherPersonalInfo1.get(0).getPhoneNumber().equalsIgnoreCase("")))
												sb.append("<th  class='divlableval' style='"+strCss_value+";margin-top:-5px;padding-left:12px;'><span class='span2'>"+lstTeacherPersonalInfo1.get(0).getPhoneNumber()+"</span></th>");	
											else
												sb.append("<th  class='divlableval' style='"+strCss_value+";margin-top:-5px;padding-left:5px;'><span class='span2'>&nbsp;N/A</span></th>");
										}
										else
											sb.append("<th  class='divlableval' style='"+strCss_value+";margin-top:-5px;padding-left:5px;'><span class='span2'>&nbsp;N/A</span></th>");
										
										// PD Report
										
										isBase = isbaseComplete.get(teacherDetail.getTeacherId());
										sb.append("<th class='divlable' style='"+strCss_lbl+""+PDReport_column_width+"'>&nbsp;"+lblMobileNumber+": </th>");
										if((lstTeacherPersonalInfo1.size()==1 &&  lstTeacherPersonalInfo1.get(0).getMobileNumber()!=null && !lstTeacherPersonalInfo1.get(0).getMobileNumber().equalsIgnoreCase("")))
											sb.append("<th  class='divlableval' style='"+strCss_value+";padding-right:70px;margin-top:-5px;padding-left:5px;'><span style='width:40px;'>"+lstTeacherPersonalInfo1.get(0).getMobileNumber()+"</span></th>");
										else
											sb.append("<th  class='divlableval' style='"+strCss_value+";padding-right:70px;margin-top:-5px;padding-left:30px;'><span style='width:40px;'>&nbsp;N/A</span></th>");
										
										sb.append("</tr>");
								}
								
								if(sVisitLocation.equalsIgnoreCase("Teacher Pool") && userMaster.getDistrictId()!=null && (userMaster.getDistrictId().getDistrictId().equals(7800049) || userMaster.getDistrictId().getDistrictId().equals(7800048) || userMaster.getDistrictId().getDistrictId().equals(7800050) || userMaster.getDistrictId().getDistrictId().equals(7800051) || userMaster.getDistrictId().getDistrictId().equals(7800053))){
								sb.append("<tr>");
								OctDetails octDetails = getOctDetails(teacherDetail);
								String octNumber ="";
								String octFileUpload ="";
								if(octDetails!=null && octDetails.getOctNumber()!=null){
									octFileUpload=octDetails.getOctFileName();
								}
								if(octDetails!=null && octDetails.getOctNumber()!=null){
									octNumber="<a href='#' onclick=\"getOctDetails('"+octDetails.getOctNumber()+"')\">"+octDetails.getOctNumber()+"</a>";
								}else{
									octNumber="N/A";
								}
									sb.append("<th class='divlable' style='"+strCss_lbl+""+Phone_column_width+"'>&nbsp;"+msgOCTRegistrationNum+": </th>");
									sb.append("<th  class='divlableval' style='"+strCss_value+";margin-top:-5px;padding-left:5px;'><span class='span2'>&nbsp;"+octNumber+"</span></th>");
									//sb.append("<th class='divlable' style='"+strCss_lbl+""+PDReport_column_width+"'>&nbsp;OCT&nbsp;Card: </th>");
									sb.append("<th class='divlable' style='"+strCss_lbl+""+PDReport_column_width+"'>&nbsp;"+Utility.getLocaleValuePropByKey("lbloctcard", locale)+": </th>");
									sb.append("<th  class='divlableval' style='"+strCss_value+";padding-right:70px;margin-top:-5px;padding-left:30px;'><span style='width:40px;'>&nbsp;");
									if(octFileUpload!=""){
										sb.append("<a data-original-title='"+Utility.getLocaleValuePropByKey("lbloctcard", locale)+"' rel='tooltip' id='hrefOctUpload' href='javascript:void(0);' onclick=\"downloadOctUpload('"+octDetails.getTeacherId().getTeacherId()+"');"+windowFunc+"\">View</a>");
									}else{
										sb.append("N/A");							
										}
									sb.append("</span></th>");						
									sb.append("</tr>");
									sb.append("<tr>");
									sb.append("<th class='divlable' style='"+strCss_lbl+""+Phone_column_width+"padding:0px;'>&nbsp;"+lblComments1+":</th>");
									sb.append("</tr>");
									sb.append("<tr>");
									sb.append("<td colspan='3' class='divlable' style='"+strCss_lbl+""+Phone_column_width+"'><textarea>");
									if(octDetails!=null && octDetails.getOctText()!=null){
										sb.append(octDetails.getOctText());
									}
								sb.append("</textarea></td>");
								sb.append("</tr>");
								}
								sb.append("</table>");
								
							sb.append("</th>");
							sb.append("</tr>");
							sb.append("</table>");
						
						sb.append("</th>");
					sb.append("</tr>");
						
						/////////////////
						
					//System.out.println("<--Phone--> "+lstTeacherPersonalInfo1.get(0).getPhoneNumber()+"<--Mobile--> "+lstTeacherPersonalInfo1.get(0).getMobileNumber());	
						// start new row
					TestTool.getTraceTimeTPAP("AchievementScor start");
						// Calculating and Get valles of "Achievement Score","Academic Achievement" and "Leadership/Results"
						String strAScore="";
						String strLRScore="";
						int iAcademicAchievement=0;
						int iLeadershipResults=0;
						int itotalAchievementScore=0;
						int iachievementMaxScore=0;
						
						int iacademicAchievementMaxScore=0;
						int ileadershipAchievementMaxScore=0;
						
						//A Score and L/R Score
						List<TeacherAchievementScore> lstTeacherAchievementScore_new=new ArrayList<TeacherAchievementScore>();
						DistrictMaster districtMasterObjForScore=null;
						if(entityID==1 && achievementScore){ // For TM and For Mosaic
							if(sVisitLocation.equalsIgnoreCase("Mosaic") && districtMaster_Input!=null){
								districtMasterObjForScore=districtMaster_Input;
								Criterion criterion_district = Restrictions.eq("districtMaster", districtMasterObjForScore);
								lstTeacherAchievementScore_new =teacherAchievementScoreDAO.findByCriteria(criterion,criterion_district);
							}
						}
						else if((entityID==2 || entityID==3) && achievementScore){ // For District OR SA
							districtMasterObjForScore=districtMaster;
							Criterion criterion_district = Restrictions.eq("districtMaster", districtMasterObjForScore);
							lstTeacherAchievementScore_new =teacherAchievementScoreDAO.findByCriteria(criterion,criterion_district);
						}
						
						int getScrrec = 0;
						if(lstTeacherAchievementScore_new!=null && lstTeacherAchievementScore_new.size()!=0){
							getScrrec=lstTeacherAchievementScore_new.size()-1;
						}
						if(lstTeacherAchievementScore_new!=null && lstTeacherAchievementScore_new.size()>0){
							iAcademicAchievement=lstTeacherAchievementScore_new.get(getScrrec).getAcademicAchievementScore();
							iLeadershipResults=lstTeacherAchievementScore_new.get(getScrrec).getLeadershipAchievementScore();
							
							iacademicAchievementMaxScore=lstTeacherAchievementScore_new.get(getScrrec).getAcademicAchievementMaxScore();
							ileadershipAchievementMaxScore=lstTeacherAchievementScore_new.get(getScrrec).getLeadershipAchievementMaxScore();
							
							itotalAchievementScore=lstTeacherAchievementScore_new.get(getScrrec).getTotalAchievementScore();
							iachievementMaxScore=lstTeacherAchievementScore_new.get(getScrrec).getAchievementMaxScore();
							
							strAScore=(iAcademicAchievement)+"/"+(iacademicAchievementMaxScore);
							strLRScore=(iLeadershipResults)+"/"+(ileadershipAchievementMaxScore);
						}
						else
						{
							strAScore="N/A";
							strLRScore="N/A";
						}
						TestTool.getTraceTimeTPAP("AchievementScor end");
						strCss_lbl="padding-left:10px;";
						strCss_value="padding-left:105px;";
						if((sVisitLocation.equalsIgnoreCase("Mosaic") && entityID==1) || entityID==2 || entityID==3)
						{
							if(entityID==3)
								strCss_lbl="padding-left:51px;";
							
							sb.append("<tr>");
								sb.append("<td valign='top'>");
									sb.append("<table border='0' width=750 cellspacing=0 cellpadding=0 class='tablecgtbl'>");
									if(achievementScore)
									{
										if(sVisitLocation.equalsIgnoreCase("Teacher Pool") ||sVisitLocation.equalsIgnoreCase("PNR Dashboard"))
											sb.append("<tr><td style='width:30px;'>&nbsp;</td>");
										else
											sb.append("<tr><td style='width:50px;'>&nbsp;</td>");
											sb.append("<td valign='top'>" +
												"<table border='0' cellspacing=0 cellpadding=0 class='tablecgtbl'>" +
												"<TR>");
													// A Score and L/R Score
													if(sVisitLocation.equalsIgnoreCase("Teacher Pool") ||sVisitLocation.equalsIgnoreCase("PNR Dashboard")){
														strCss_lbl="padding:0px;padding-left:0px;width:55px;"; // Change padding-left 20 to 0 
														strCss_value="padding:0px;padding-left:0px;width:50px;";
													}
													else
													{
														strCss_lbl="padding:0px;padding-left:4px;width:55px;";
														strCss_value="padding:0px;padding-left:5px;width:50px;";
													}
													sb.append("<td class='divlable' style='"+strCss_lbl+"'>&nbsp;&nbsp;&nbsp;"+lblAScore+":</td>");
													sb.append("<td class='divlableval' style='"+strCss_value+";vertical-align:top;' id='ascore_profile'>&nbsp;"+strAScore+"</td>");

													sb.append("<td style='width:154px;'>&nbsp;</td>");
													if(sVisitLocation.equalsIgnoreCase("Teacher Pool") ||sVisitLocation.equalsIgnoreCase("PNR Dashboard")|| sVisitLocation.equalsIgnoreCase("My Folders") ){
														strCss_lbl="padding:0px;padding-left:0px;width:60px;"; // Change padding-left 20 to 0 
													}else{
														strCss_lbl="padding:0px;padding-left:2px;width:60px;";
													}
													sb.append("<td class='divlable' style='"+strCss_lbl+"'>&nbsp;&nbsp;&nbsp;"+lblLRScore+":</td>");
													sb.append("<td class='divlableval' style='"+strCss_value+";vertical-align:top;' id='lrscore_profile'>&nbsp;"+strLRScore+"</td>");
													sb.append("</TR>"+"</table>"+"</td></tr>");
													
													/* @Start
													 * @Ashish Kumar
													 * @Description :: District Admin can change achievement score.
													 * */
													if( (sVisitLocation.equalsIgnoreCase("Teacher Pool") || sVisitLocation.equalsIgnoreCase("PNR Dashboard")) && entityID==2){
														//System.out.println("******************2*******************");
														sb.append("<tr>");
														strCss_value="padding-left:55px;";
														// Blank Cell
														sb.append("<td style='width:55px;'>&nbsp;</td>");
														
														strCss_lbl="padding:2px;padding-left:4px;padding-top:5px;";
														sb.append("<td>"); 
														
														

														if(achievementScore && entityID==2)
														{
															sb.append("<table border='0' width=200 cellspacing=0 cellpadding=0 class='tablecgtbl'>");
															//1st Row for Label of "Achievement Score","Academic Achievement" and "Leadership/Results"
															sb.append("<tr>");
															//Academic Achievement:
																sb.append("<td class='divlable' style='"+strCss_lbl+"'>&nbsp;&nbsp;Academic&nbsp;Achievement:</td>");
															// Leadership/Results
																sb.append("<td class='divlable' style='"+strCss_lbl+"'>&nbsp;&nbsp;"+lblLeadershipResults+":</td>");
																
																sb.append("<td style='width:100px;'>&nbsp;</td>");
															sb.append("</tr>");
															
															strCss_lbl="padding:2px;padding-left:0px;padding-top:-20px;";
															//2nd Row for values of "Achievement Score","Academic Achievement" and "Leadership/Results"
															String strSliderCss_Iframe="border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:280px;";
															sb.append("<tr>");
																
															sb.append("<td style='"+strCss_lbl+"'>&nbsp;");
															sb.append("<div style='margin-left: 0px; margin-top: -18px;'><label id='achievementSlider'></label></div>");
																
															sb.append("<input type='hidden' name='txt_sl_slider_aca_ach' id='txt_sl_slider_aca_ach' value='"+iAcademicAchievement+"'>");
															sb.append("</td>");
															
															
															// Leadership/Results
															sb.append("<td style='"+strCss_lbl+"'>&nbsp;");
															
															sb.append("<div style='margin-left: 0px; margin-top: -18px;'><label id='leadershipSlider'></label>" +
																		"</div>");
															sb.append("<input type='hidden' name='txt_sl_slider_lea_res' id='txt_sl_slider_lea_res' value='"+iLeadershipResults+"'>");
															//sb.append("<a href='javascript:void(0);' onclick=\"javascript:updateAScore("+teacherId+","+jobOrder.getDistrictMaster().getDistrictId()+")\"'>Go</A>");
															sb.append("</td>");
															
															//sb.append("<td style='width:20px;vertical-align:middle;'>&nbsp;<a href='javascript:void(0);' onclick=\"javascript:updateAScore("+teacherId+","+jobOrder.getDistrictMaster().getDistrictId()+")\"'>Go</A></td>");
															//sb.append("<td style='width:20px;vertical-align:middle;'>&nbsp;<a href='javascript:void(0);' onclick=\"javascript:updateAScore("+teacherId+","+jobOrder.getDistrictMaster().getDistrictId()+")\"'>Go</A></td>");
															//sb.append("<td style='width:40px;vertical-align:middle;'>&nbsp;<button class='btn btn-primary' type='button' onclick=\"javascript:updateAScore("+teacherId+","+jobOrder.getDistrictMaster().getDistrictId()+")\">Set A Score <i class='icon'></i></button></td>");
															
															//System.out.println(" Testing :: userMaster.getDistrictId() :: "+userMaster.getDistrictId().getDistrictId());
															
															sb.append("<td style='width:170px;vertical-align:middle;'>&nbsp;<button class='btn btn-primary' style='margin-top:-20px; width:106px;' type='button' onclick='javascript:updateAScoreFromTeacherPool("+teacherId+","+userMaster.getDistrictId().getDistrictId()+")'>Set A Score</td>");
															
															sb.append("</tr>");
															
															sb.append("</table>");
														}
														
													sb.append("</td>");
													sb.append("</tr>");
													}
													/* @End
													 * @Ashish Kumar
													 * @Description :: District Admin can change achievement score.
													 * */
									}
									sb.append("</table>");
								sb.append("</td>");
							sb.append("</tr>");
						
						}if(pnqDis){
							TestTool.getTraceTimeTPAP("panq start");
							sb.append("<tr>");
							sb.append("<td style='padding:0px 0px 0px 53px'>");
						
							sb.append("<table border='0' cellspacing=0 cellpadding=0 class='tablecgtbl'>");
							sb.append("<tr>");				
							sb.append("<td class='divlable' style='padding:2px;padding-left:8px;padding-top:20px;'>&nbsp;PNQ:</td>");				
							String pNQVal = getPNQValue(lstTeacherExperience_new,teacherDetail);
							String New_pNQVal = "";
							sb.append("<th >&nbsp;"+pNQVal+"</th>");				
							sb.append("</td>");
							sb.append("</tr>");
							sb.append("</table>");
							
							sb.append("</td>");
							sb.append("</tr>");
							TestTool.getTraceTimeTPAP("panq start");
						}
					sb.append("</table>");
					
					sb.append("<div id='removeWait' style='text-align:center;padding-top:0px;'><img src='images/loadingAnimation.gif' /> <br>"+lblLoding+"</div>");
					
					sb.append("</div>");
					sb.append("<DIV>");
					sb.append("<table border=0 width='100%'><tr bgcolor='#F2FAEF' class='custom-div-border'><th style='text-align:center;vertical-align:middle;padding-top:10px;padding-bottom:10px;'width='35%'><a href='javascript:void(0);' onclick=\"getCommunicationsDiv(1,0,'"+teacherDetail.getTeacherId()+"',0);\" ><span class='icon-dropbox icon-large iconcolor'></span>&nbsp;"+headCom+"</a></th>");
					if(userMaster.getEntityType()==1)
					{
						sb.append("<th style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><span class='icon-save icon-large iconcolorhover'></span>&nbsp;"+btnSave+"</th>" +
								"<th style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><span class='icon-share icon-large iconcolorhover'></span>&nbsp;"+btnShare+"</th>"+
								"<th style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><span class='icon-tag icon-large iconcolorhover'></span></span>&nbsp;"+headTag+"</th>");
					}
					else
					{
						
						int flagpopover=1;
						sb.append("<th style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><a href='javascript:void(0);' id='saveaction"+noOfRecordCheck+"' onclick=\"saveToFolderJFTNULL('0','"+teacherDetail.getTeacherId()+"',"+flagpopover+")\"><span class='icon-save icon-large iconcolor'></span>&nbsp;"+btnSave+"</a></th>" +
								"<th style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><a href='javascript:void(0);' id='shareaction"+noOfRecordCheck+"' onclick=\"displayUsergrid('"+teacherDetail.getTeacherId()+"',"+flagpopover+")\"><span class='icon-share icon-large iconcolor'></span>&nbsp;"+btnShare+"</a></th>");
						//sb.append("<th style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><span class='icon-tag icon-large iconcolor'></span></th>");
						
						//sb.append("<th style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><a href='javascript:void(0);' id='tagaction' onclick=\"closeProfileOpenTags("+noOfRecordCheck+")\"> <span class='icon-tag icon-large iconcolor'></span></span>&nbsp;Tag</a></th>");
						
						if(userMaster.getRoleId().getRoleId()==1 || userMaster.getRoleId().getRoleId()==3){
							sb.append("<th style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><span class='icon-tag icon-large iconcolorhover'></span></span>&nbsp;"+headTag+"</th>");
						}
						else{
							if(sVisitLocation.equalsIgnoreCase("Teacher Pool") ||sVisitLocation.equalsIgnoreCase("PNR Dashboard") ||sVisitLocation.equalsIgnoreCase("My Folders"))
								sb.append("<th style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><a href='javascript:void(0);' id='tagaction' onclick=\"closeProfileOpenTags("+teacherId+","+noOfRecordCheck+")\"> <span class='icon-tag icon-large iconcolor'></span></span>&nbsp;"+headTag+"</a></th>");
							else
								sb.append("<th style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><span class='icon-tag icon-large iconcolorhover'></span></span>&nbsp;"+headTag+"</th>");
						}
					}
					sb.append("</tr>");
					sb.append("</table>");
					sb.append("</DIV>");	
					/*sb.append("<div class='modal-footer'>"+
					 		"<button class='btn' onclick='showProfileContentClose();'>Close</button>"+ 		
					 	"</div>"+
					"</div>");*/
					sb.append("</div>");
					TestTool.getTraceSecond("end");
					}
					
					if(fg!=0){
						
						DistrictPortfolioConfig districtPortfolioConfig=null;
						TeacherPersonalInfo teacherPersonalInfo = teacherPersonalInfoDAO.findById(teacherId, false, false);
						
						String strCss_lbl="padding:2px;text-align:left;";
						String strCss_value="width=42px; vertical-algin:top;text-align:left;padding:0px;padding-left:5px;padding-right:5px;padding-top:5px;";

						if(sVisitLocation.equalsIgnoreCase("My Folders"))
						{
							strCss_lbl="padding:0px;text-align:left;";
							strCss_value="padding:0px;text-align:left;";
						}else{
							strCss_lbl="padding:2px;text-align:left;";//OK
							strCss_value="padding:2px;text-align:left;";//OK
						}

		                //By Ravindra
						sb.append("####");
					String sGridLabelCss="padding:0px;padding-left:12px;padding-top:10px;color:black;";
					String sGridtableDisplayCSS="padding:10px;font-weight: normal;";
					String sGridtableDisplayCSS1="padding:0px;font-weight: normal;";
					sb.append("<table width="+sWidthFoeDiv+" border='0' cellspacing=0 cellpadding=0 >");
					//TestTool.getTraceTimeTPAP("TPAP 213");
					
					/*******************************************Code Start Here********************************************/
					if(sVisitLocation.equalsIgnoreCase("Teacher Pool") ||sVisitLocation.equalsIgnoreCase("PNR Dashboard") ||sVisitLocation.equalsIgnoreCase("My Folders"))
					{
						if(sVisitLocation.equalsIgnoreCase("Teacher Pool") && userMaster!=null && userMaster.getDistrictId()!=null && userMaster.getDistrictId().getDistrictId().equals(1302010))
							if(teacherPersonalInfo!=null && teacherPersonalInfo.getEmployeeType()!=null && teacherPersonalInfo.getEmployeeType().equals(0)){
								sb.append("<tr><td style='"+sGridLabelCss+"' >");
								sb.append("<table ><tr><td colspan='2' class='divlable' style='padding:2px 2px 2px 24px;white-space:nowrap'>Why are you no longer employed with us?</td></tr>");
								sb.append("<tr><td style='padding:2px 0px 2px 24px;white-space:nowrap'>Ans: &nbsp;&nbsp;</td>");
								if(teacherPersonalInfo!=null && teacherPersonalInfo.getNoLongerEmployed()!=null && !teacherPersonalInfo.getNoLongerEmployed().equalsIgnoreCase(""))
									sb.append("<td>"+teacherPersonalInfo.getNoLongerEmployed()+"</td>");
								else{
									sb.append("<td class='divlableval'>N/A</td>");
								}
								sb.append("</tr>");
								sb.append("</table></td></tr>");
							}
					//************************************Start of Veteran Origin**************************//
					try
					{
						if(districtMaster!=null && districtMaster.getDistrictId()!=null && districtMaster.getDistrictId().equals(1201470))
						{
							String veteranOrigin = getVeteranOrigin(districtMaster, teacherDetail,teacherPersonalInfo);
							sb.append("<tr><td class='net-header-text' style='"+sGridLabelCss+"' >");
							sb.append(veteranOrigin);
							sb.append("</td> </tr>");
						}
					}
					catch(Exception e){e.printStackTrace();}
					//************************************End of Veteran Origin****************************//
					
					
					//************************************Start of Race*************************//
					try
					{
						/*if( (districtPortfolioConfig==null || (districtPortfolioConfig.getRace()!=null && districtPortfolioConfig.getRace())) && (districtMaster!=null && districtMaster.getDistrictId()!=null && districtMaster.getDistrictId().equals(1201470)) )
						{
							String race = getRace(teacherPersonalInfo);
							sb.append("<tr><td class='net-header-text' style='"+sGridLabelCss+"' >");
							sb.append(race);
							sb.append("</td> </tr>");
						}*/
					}
					catch(Exception e){e.printStackTrace();}
					//************************************End of Race***************************//
					}	//End Of check
					
					
					/*************   GE SA Exam ******************/
					if(districtMaster!=null && !districtMaster.getDistrictId().equals(1201470) && !districtMaster.getDistrictId().equals(7800040) && !districtMaster.getDistrictId().equals(7800038) && !districtMaster.getDistrictId().equals(7800294))
					if(districtPortfolioConfig==null || (districtPortfolioConfig.getGeneralKnowledgeExam()!=null && districtPortfolioConfig.getGeneralKnowledgeExam()) )
					{
					// start new row   >>> General Knowledge Exam
					   int geFlag=0;
					   try{
							TeacherGeneralKnowledgeExam teacherGeneralKnowledgeExam=getGKEValuesForProfile(teacherDetail);
							if(teacherGeneralKnowledgeExam!=null){
								sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'>&nbsp;<b>"+lblGeneralKnowledgeExam+":</b></td></tr>");
								sb.append("<tr>");
								sb.append("<td>");
									sb.append("<table border='0' cellspacing=0 cellpadding=0 class='tablecgtbl'>");
									sb.append("<tr>");
										strCss_value="padding-left:55px;";
										sb.append("<td style='"+strCss_value+"'>&nbsp;</td>");
										strCss_lbl="padding:2px;vertical-align:middle;";
										strCss_value="padding:2px;vertical-align:text-top;";
										sb.append("<td>"); 
										sb.append("<table border='0' cellspacing=0 cellpadding=0 class='tablecgtbl'>");
										sb.append("<tr>");
										strCss_lbl="padding:2px;padding-left:2px;vertical-align:middle;";
										//sb.append("<td class='divlable' style='"+strCss_lbl+"'>&nbsp;General Knowledge Exam: </td>");
										if(teacherGeneralKnowledgeExam!=null){
											geFlag=1;
											sb.append("<td class='divlable' style='"+strCss_lbl+";margin-top:-5px;'>Exam Status</td>");
										    sb.append("<td class='divlable' style='"+strCss_lbl+";margin-top:-5px;padding-left:60px;'>"+lblExamDate+"</td>");
										    sb.append("<td class='divlable' style='"+strCss_lbl+";margin-top:-5px;padding-left:60px;'>"+lblScoreReport+"</td>");
										    								    sb.append("<td class='divlable' style='"+strCss_lbl+";margin-top:-5px;padding-left:30px;'>"+lblNotes+"</td>");
										}
										sb.append("</tr>");
										sb.append("</table>");
									sb.append("</td>");
									sb.append("</tr>");
									sb.append("</table>");
								sb.append("</td>");
							   sb.append("</tr>");
							}else{
								sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'>&nbsp;<b>"+lblGeneralKnowledgeExam+"</b>:&nbsp;&nbsp;<span class='divlableval'>N/A</span></td></tr>");
							}
					   }catch (Exception e) {
							e.printStackTrace();
						}
					   //TestTool.getTraceTimeTPAP("TPAP 214");
					   if(geFlag==1){
						   sb.append("<tr>");
							sb.append("<td>");
								sb.append("<table border='0' cellspacing=0 cellpadding=0 class='tablecgtbl'>");
								sb.append("<tr>");
									strCss_value="padding-left:55px;";
									sb.append("<td style='"+strCss_value+"'>&nbsp;</td>");
									strCss_lbl="padding:2px;vertical-align:middle;";
									strCss_value="padding:2px;vertical-align:text-top;";
									sb.append("<td>"); 
									sb.append("<table border='0' cellspacing=0 cellpadding=0 class='tablecgtbl'>");
									sb.append("<tr>");
									strCss_lbl="padding:2px;padding-left:2px;vertical-align:middle;";
									//sb.append("<td class='divlable' style='"+strCss_lbl+"padding-left:140px;'>&nbsp;</td>");
									try{
										TeacherGeneralKnowledgeExam teacherGeneralKnowledgeExam=getGKEValuesForProfile(teacherDetail);
										if(teacherGeneralKnowledgeExam!=null){
											if(teacherGeneralKnowledgeExam.getGeneralKnowledgeExamStatus().equals("P"))
												sb.append("<td class='divlableval' style='"+strCss_value+";margin-top:-5px;'>Pass</td>");
											else if(teacherGeneralKnowledgeExam.getGeneralKnowledgeExamStatus().equals("F"))	
												sb.append("<td class='divlableval' style='"+strCss_value+";margin-top:-5px;'>Fail</td>");
											
											sb.append("<td class='divlableval' style='"+strCss_value+";margin-top:-5px;padding-left:100px;'>"+Utility.convertDateAndTimeToDatabaseformatOnlyDate(teacherGeneralKnowledgeExam.getGeneralKnowledgeExamDate())+"</td>");
										    sb.append("<td  class='divlableval' style='"+strCss_value+";margin-top:-5px;padding-left:60px;'>&nbsp;<a data-original-title='"+lblScoreReport+"' rel='tooltip' id='tpScoreReportGE' href='javascript:void(0);' onclick=\"downloadGeneralKnowledge('"+teacherGeneralKnowledgeExam.getGeneralKnowledgeExamId()+"','"+teacherGeneralKnowledgeExam.getTeacherDetail().getTeacherId()+"','tpScoreReportGE');"+windowFunc+"\">");
										    sb.append("<span class='icon-download-alt icon-large iconcolorBlue' style='padding-left:20px;'></span>");
										    sb.append("</a></td>");
										    sb.append("<td class='' style='"+strCss_value+";margin-top:-5px;padding-left:47px;'>");
										    sb.append(teacherGeneralKnowledgeExam.getGeneralExamNote());
										    sb.append("</td>");
										}
									}catch (Exception e) {
										e.printStackTrace();
									}
									sb.append("</tr>");
									sb.append("</table>");
								sb.append("</td>");
								sb.append("</tr>");
								sb.append("</table>");
							sb.append("</td>");
						   sb.append("</tr>");
							// End new row 
					   }
					}
					
					System.out.println(" >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Special Note >>>>>>>>>>>>>>>>>>>>>>>>>>>>");
					//******************************** Special Note ****************************//NC
					//&& userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==2
					//if(userMaster!=null && userMaster.getEntityType()==2 && userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==2){
						if(userMaster!=null  && userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==NC_HEADQUARTER){
						   
						   sb.append("<tr>");
							sb.append("<td>");
							sb.append(getSpecialNoteForTeacher(teacherDetail));
							sb.append("</td>");
							sb.append("</tr>");
						   
					}
					
					
					if(districtMaster!=null && !districtMaster.getDistrictId().equals(1201470) && !districtMaster.getDistrictId().equals(7800040) && !districtMaster.getDistrictId().equals(7800038) && !districtMaster.getDistrictId().equals(7800294))
						if(sVisitLocation.equalsIgnoreCase("Teacher Pool")){
							//subject==========================================			
								sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'>"+lblSubjectAreaExam+"</td></tr>");
								sb.append("<tr><td colspan=5 width='100%'>");
									sb.append("<table border=0 width='100%'>");
										sb.append("<tr>");
											sb.append("<td style="+sGridtableDisplayCSS+">");
											sb.append(getSubjectAreasGrid(teacherDetail));
											sb.append("</td>");
										sb.append("</tr>");
									sb.append("</table>");
								sb.append("</td></tr>");
								sb.append("<script>applyScrollOnTblSubjectAreaExam();</script>");
							//subject==========================================
						}
					
					if(districtPortfolioConfig==null || (districtPortfolioConfig.getSubjectAreaExam()!=null && districtPortfolioConfig.getSubjectAreaExam()) )
					{/*			   
					   // subject ---- 
					   int saFlag=0;
					   try{
						TeacherSubjectAreaExam teacherSubjectAreaExam=getSAEValuesForProfile(teacherDetail);
						if(teacherSubjectAreaExam!=null){
							 sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'>&nbsp;<b>"+lblSubjectAreaExam+":</b></td></tr>");
							 sb.append("<tr>");
								sb.append("<td>");
									sb.append("<table border='0' cellspacing=0 cellpadding=0 class='tablecgtbl'>");
									sb.append("<tr>");
										strCss_value="padding-left:55px;";
										sb.append("<td style='"+strCss_value+"'>&nbsp;</td>");
										strCss_lbl="padding:2px;vertical-align:middle;";
										strCss_value="padding:2px;vertical-align:text-top;";
										sb.append("<td>"); 
										sb.append("<table border='0' cellspacing=0 cellpadding=0 class='tablecgtbl'>");
										sb.append("<tr>");
										strCss_lbl="padding:2px;padding-left:2px;vertical-align:middle;";
										//sb.append("<td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblSubjectAreaExam+": </td>");
										if(teacherSubjectAreaExam!=null){
											saFlag=1;
											sb.append("<td class='divlable' style='"+strCss_lbl+";margin-top:-5px;'>Exam Status</td>");
										    sb.append("<td class='divlable' style='"+strCss_lbl+";margin-top:-5px;padding-left:60px;'>"+lblExamDate+"</td>");
										    sb.append("<td class='divlable' style='"+strCss_lbl+";margin-top:-5px;padding-left:60px;'>"+lblScoreReport+"</td>");
										}
										sb.append("</tr>");
										sb.append("</table>");
									sb.append("</td>");
									sb.append("</tr>");
									sb.append("</table>");
								sb.append("</td>");
							   sb.append("</tr>");
						}else{
							 sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'>&nbsp;<b>"+lblSubjectAreaExam+":</b> &nbsp;<span class='divlableval'>N/A</sapn></td></tr>");
						}
					}catch (Exception e) {
						e.printStackTrace();
					}
					
					if(saFlag==1){
						   sb.append("<tr>");
							sb.append("<td>");
								sb.append("<table border='0' cellspacing=0 cellpadding=0 class='tablecgtbl'>");
								sb.append("<tr>");
									strCss_value="padding-left:55px;";
									sb.append("<td style='"+strCss_value+"'>&nbsp;</td>");
									strCss_lbl="padding:2px;vertical-align:middle;";
									strCss_value="padding:2px;vertical-align:text-top;";
									sb.append("<td>"); 
									sb.append("<table border='0' cellspacing=0 cellpadding=0 class='tablecgtbl'>");
									sb.append("<tr>");
									strCss_lbl="padding:2px;padding-left:2px;vertical-align:middle;";
									//sb.append("<td class='divlable' style='"+strCss_lbl+"padding-left:140px;'>&nbsp;</td>");
									try{
										TeacherSubjectAreaExam teacherSubjectAreaExam=getSAEValuesForProfile(teacherDetail);
										if(teacherSubjectAreaExam!=null){
											if(teacherSubjectAreaExam.getExamStatus().equals("P"))
												sb.append("<td  class='divlableval' style='"+strCss_value+";margin-top:-5px;'>"+optPass+"</td>");
											else if(teacherSubjectAreaExam.getExamStatus().equals("F"))	
												sb.append("<td  class='divlableval' style='"+strCss_value+";margin-top:-5px;'>"+optFail+"</td>");
											
											sb.append("<td  class='divlableval' style='"+strCss_value+";margin-top:-5px;padding-left:100px;'>"+Utility.convertDateAndTimeToDatabaseformatOnlyDate(teacherSubjectAreaExam.getExamDate())+"</td>");
										    sb.append("<td  class='divlableval' style='"+strCss_value+";margin-top:-5px;padding-left:57px;'>&nbsp;<a data-original-title='"+lblScoreReport+"' rel='tooltip' id='tpScoreReportSA' href='javascript:void(0);' onclick=\"downloadSubjectAreaExam('"+teacherSubjectAreaExam.getTeacherSubjectAreaExamId()+"','"+teacherSubjectAreaExam.getTeacherDetail().getTeacherId()+"','tpScoreReportSA');"+windowFunc+"\">");
										    sb.append(teacherSubjectAreaExam.getScoreReport());
										    sb.append("</a></td>");
										}
									}catch (Exception e) {
										e.printStackTrace();
									}
									sb.append("</tr>");
									sb.append("</table>");
								sb.append("</td>");
								sb.append("</tr>");
								sb.append("</table>");
							sb.append("</td>");
						   sb.append("</tr>");
							// End new row 
					   }
					*/}
					   /********************************************************/
					
					//TestTool.getTraceTimeTPAP("TPAP 215");
					if(!headQuarterMaster ||(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==NC_HEADQUARTER) || userMaster.getDistrictId()!=null)
					if(districtPortfolioConfig==null || (districtPortfolioConfig.getAcademic()!=null && !districtPortfolioConfig.getAcademic().equals(0)) )
					{
					// Start Teacher Academics Grid
					if(sVisitLocation.equalsIgnoreCase("Mosaic")){
						sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'>"+lblAcademics+"</td></tr>");
					}else if(sVisitLocation.equalsIgnoreCase("My Folders")){
						sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'><b>"+lblAcademics+"</b></td></tr>");
					}else{
						sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'><b>"+lblAcademics+"</b></td></tr>");
					}
					
					sb.append("<tr><th colspan=5 width='100%'>");
						sb.append("<table border=0 width='100%' onmouseover=\"javascript:setGridProfileVariable('teacherAce')\">");
							sb.append("<tr>");
								sb.append("<th style="+sGridtableDisplayCSS+" id='gridDataTeacherAcademics'>");
								sb.append("</th>");
							sb.append("</tr>");
						sb.append("</table>");
					sb.append("</th></tr>");
					sb.append("<script>getTeacherAcademicsGrid_DivProfile("+teacherDetail.getTeacherId()+");</script>");
					// End Teacher Academics Grid
					}
					//sandeep Start Teacher education Grid
					  
					if(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==NC_HEADQUARTER){
						  
					      if(sVisitLocation.equalsIgnoreCase("Mosaic")){
					       sb.append("<tr><td colspan=4 class='net-header-text' style='"+sGridLabelCss+"'>Education</td></tr>");
					      }else if(sVisitLocation.equalsIgnoreCase("My Folders")){
					       sb.append("<tr><td colspan=4 class='net-header-text' style='"+sGridLabelCss+"'><b>Education</b></td></tr>");
					      }else{
					       sb.append("<tr><td colspan=4 class='net-header-text' style='"+sGridLabelCss+"'><b>Education</b></td></tr>");
					      }
					      
					      sb.append("<tr><th colspan=4 width='100%'>");
					       sb.append("<table border=0 width='100%' style='margin-left:10px;'>");
					        sb.append("<tr>");
					         sb.append("<th style="+sGridtableDisplayCSS1+" id='gridDataTeacherEducations'>");
					         sb.append("</th>");
					        sb.append("</tr>");
					       sb.append("</table>");
					      sb.append("</th></tr>");
					      sb.append("<script>getTeacherEducationGrid_DivProfile("+teacherDetail.getTeacherId()+");</script>");
					}     
				      // End Teacher Education Grid
					
					System.out.println("11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111");
					if(!headQuarterMaster ||(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==NC_HEADQUARTER) || userMaster.getDistrictId()!=null)
					if(districtPortfolioConfig==null || (districtPortfolioConfig.getCertification()!=null && !districtPortfolioConfig.getCertification().equals(0)) )
					{
					// Start Teacher Certifications Grid
					if(sVisitLocation.equalsIgnoreCase("Mosaic")){
						sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'>"+lblCertiLice+"</td></tr>");
					}else if(sVisitLocation.equalsIgnoreCase("My Folders")){
						sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'><b>"+lblCertiLice+"</b></td></tr>");
					}else{
						sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'><b>"+lblCertiLice+"</b></td></tr>");
					}
					if(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==NC_HEADQUARTER){
						sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'><b>"+lblLicense+"</b></td></tr>");
						sb.append("<tr><th colspan=5 width='100%'>");
						sb.append("<table border=0 width='100%' onmouseover=\"javascript:setGridProfileVariable('teacherCerti')\">");
							sb.append("<tr>");
								sb.append("<th style="+sGridtableDisplayCSS+" id='divLicneseGridCertificationsGridDate'>");
								sb.append("</th>");
							sb.append("</tr>");
						sb.append("</table>");
						sb.append("</th></tr>");
						sb.append("<script>getTeacherLicenceGrid_DivProfile("+teacherDetail.getTeacherId()+");</script>");
						
						
						sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'><b>"+lblLEACandidatePortfolio+"</b></td></tr>");
						sb.append("<tr><th colspan=5 width='100%'>");
						sb.append("<table border=0 width='100%' onmouseover=\"javascript:setGridProfileVariable('teacherCerti')\">");
							sb.append("<tr>");
								sb.append("<th style="+sGridtableDisplayCSS+" id='divLEACandidatePotfolioDiv'>");
								sb.append("</th>");
							sb.append("</tr>");
						sb.append("</table>");
						sb.append("</th></tr>");
						sb.append("<script>getLEACandidatePortfolio_DivProfile("+teacherDetail.getTeacherId()+");</script>");
						
						sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'><b>"+lblCertiLicense+"</b></td></tr>");
					}
					sb.append("<tr><th colspan=5 width='100%'>");
						sb.append("<table border=0 width='100%' onmouseover=\"javascript:setGridProfileVariable('teacherCerti')\">");
							sb.append("<tr>");
								sb.append("<th style="+sGridtableDisplayCSS+" id='gridDataTeacherCertifications'>");
								sb.append("</th>");
							sb.append("</tr>");
						sb.append("</table>");
					sb.append("</th></tr>");
					sb.append("<script>getTeacherCertificationsGrid_DivProfile("+teacherDetail.getTeacherId()+");</script>");
					// End Teacher Certifications Grid
					}
					
					if(districtMaster!=null && districtMaster.getDistrictId().equals(1201470))
					if(sVisitLocation.equalsIgnoreCase("Teacher Pool") ||sVisitLocation.equalsIgnoreCase("PNR Dashboard")||sVisitLocation.equalsIgnoreCase("My Folders"))
					{
					
					if(districtPortfolioConfig==null || (districtPortfolioConfig.getHonors()!=null && districtPortfolioConfig.getHonors()) )
					{
					// Start Teacher Honors Grid
					if(sVisitLocation.equalsIgnoreCase("Mosaic")){
						sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'>"+lblHonors+"</td></tr>");
					}else if(sVisitLocation.equalsIgnoreCase("My Folders")){
						sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'><b>"+lblHonors+"</b></td></tr>");
					}else{
						sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'><b>"+lblHonors+"</b></td></tr>");
					}
					sb.append("<tr><th colspan=5 width='100%'>");
						sb.append("<table border=0 width='100%' onmouseover=\"javascript:setGridProfileVariable('teacherCerti')\">");
							sb.append("<tr>");
								sb.append("<td style="+sGridtableDisplayCSS+" id='gridDataTeacherHonors'>");
								sb.append("</td>");
							sb.append("</tr>");
						sb.append("</table>");
					sb.append("</th></tr>");
					sb.append("<script>getHonorsGrid("+teacherDetail.getTeacherId()+");</script>");
					// End Teacher Honors Grid
					}
					
					if(districtPortfolioConfig==null || (districtPortfolioConfig.getInvolvement()!=null && districtPortfolioConfig.getInvolvement()) )
					{
					//Start Involvement/Volunteer Work Grid//
					if(sVisitLocation.equalsIgnoreCase("Mosaic")){
						sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'>"+lblIVWork+"</td></tr>");
					}else if(sVisitLocation.equalsIgnoreCase("My Folders")){
						sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'><b>"+lblIVWork+"</b></td></tr>");
					}else{
						sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'><b>"+lblIVWork+"</b></td></tr>");
					}
					
					sb.append("<tr><th colspan=5 width='100%'>");
					sb.append("<table border=0 width='100%' onmouseover=\"javascript:setGridProfileVariable('teacherInvolve')\">");
					sb.append("<tr>");
					sb.append("<th style="+sGridtableDisplayCSS+" id='gridDataTeacherInvolvementOrVolunteerWork'>");
					sb.append("</th>");
					sb.append("</tr>");
					sb.append("</table>");
					sb.append("</th></tr>");
					sb.append("<script>getInvolvementGrid("+teacherDetail.getTeacherId()+");</script>");
					//End Involvement/Volunteer Work Grid//
					}
					
					}//End of teacher pool check
					if(!headQuarterMaster ||(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==NC_HEADQUARTER) || userMaster.getDistrictId()!=null)
					if(districtPortfolioConfig==null || (districtPortfolioConfig.getEmployment()!=null && districtPortfolioConfig.getEmployment()) )
					{
					// Start Work Experience
					if(sVisitLocation.equalsIgnoreCase("Mosaic")){
						sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'>"+lblWorkExp+"</td></tr>");
					}else if(sVisitLocation.equalsIgnoreCase("My Folders")){
						sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'><b>"+lblWorkExp+"</b></td></tr>");
					}else{
						sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'><b>"+lblWorkExp+"</b></td></tr>");
					}
					sb.append("<tr><th colspan=5 width='100%'>");
						sb.append("<table border=0 width='100%' onmouseover=\"javascript:setGridProfileVariable('workExp')\">");
							sb.append("<tr>");
								sb.append("<th style="+sGridtableDisplayCSS+" id='gridDataWorkExpEmployment'>");
								sb.append("</th>");
							sb.append("</tr>");
						sb.append("</table>");
					sb.append("</th></tr>");
					sb.append("<script>getPFEmploymentGrid_DivProfile("+teacherDetail.getTeacherId()+");</script>");
					// End employment Grid
					}
					
					//Ravindra Teacher Assessment Start
					if(!sVisitLocation.equalsIgnoreCase("Mosaic") && !sVisitLocation.equalsIgnoreCase("My Folders")){			
					if(sVisitLocation.equalsIgnoreCase("Mosaic")){
						sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'>"+lblAssessmentDetails+"</td></tr>");
					}else if(sVisitLocation.equalsIgnoreCase("My Folders")){
						sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'><b>"+lblAssessmentDetails+"</b></td></tr>");
					}else{
						sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'><b>"+lblAssessmentDetails+"</b></td></tr>");
					}
					sb.append("<tr><th colspan=5 width='100%'>");
						sb.append("<table border=0 width='100%' onmouseover=\"javascript:setGridProfileVariable('teacherAssessment')\">");
							sb.append("<tr>");
								sb.append("<th style="+sGridtableDisplayCSS+" id='gridTeacherAssessment'>");
								sb.append("</th>");
							sb.append("</tr>");
						sb.append("</table>");
					sb.append("</th></tr>");
					sb.append("<script>getTeacherAssessmentGrid_DivProfile("+teacherDetail.getTeacherId()+");</script>");
					}
					// Ravindra Teacher Assessment End
					
					
					//if(!headQuarterMaster)
					if(!headQuarterMaster ||(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==NC_HEADQUARTER) || userMaster.getDistrictId()!=null)
					if(districtPortfolioConfig==null || (districtPortfolioConfig.getReference()!=null && !districtPortfolioConfig.getReference().equals(0)) )
					{
					// Start Reference Grid
					if(sVisitLocation.equalsIgnoreCase("Mosaic")){
						//sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'>"+Utility.getLocaleValuePropByKey("lblRef", locale)+"</td></tr>");

						sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+";padding-left: 0px;'>");
						   sb.append("<div class=''>");
						    sb.append("<div class='col-sm-6 col-md-6'>"+Utility.getLocaleValuePropByKey("lblRef", locale)+"</div>");
					      if(userMaster.getRoleId().getRoleId().equals(2) || userMaster.getRoleId().getRoleId().equals(3))
					    		  sb.append("<div class='col-sm-6 col-md-6' style='text-align:  right;' ><a href='#' onclick='return showReferencesForm();'>"+lnkAddRef+"</a></div>");
					      
					    sb.append("</div></td></tr>");
					
					}else if(sVisitLocation.equalsIgnoreCase("My Folders")){
						//sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'><b>"+Utility.getLocaleValuePropByKey("lblRef", locale)+"</b></td></tr>");
						sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+";padding-left: 0px;'>");
						   sb.append("<div class=''>");
						    sb.append("<div class='col-sm-6 col-md-6'>"+Utility.getLocaleValuePropByKey("lblRef", locale)+"</div>");
					      
					    	  if(userMaster.getRoleId().getRoleId().equals(2) || userMaster.getRoleId().getRoleId().equals(3))
					    		  sb.append("<div class='col-sm-6 col-md-6' style='text-align:  right;' ><a href='#' onclick='return showReferencesForm();'>"+lnkAddRef+"</a></div>");
					      
					    sb.append("</div></td></tr>");
						//sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'><b>"+Utility.getLocaleValuePropByKey("lblRef", locale)+"</b></td></tr>");
					}else{
						//sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'><b>"+Utility.getLocaleValuePropByKey("lblRef", locale)+"</b></td></tr>");
						
						sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+";padding-left: 0px;'>");
						   sb.append("<div class=''>");
						    sb.append("<div class='col-sm-6 col-md-6'>"+Utility.getLocaleValuePropByKey("lblRef", locale)+"</div>");
					      if(userMaster.getRoleId().getRoleId().equals(2) || userMaster.getRoleId().getRoleId().equals(3))
					       {
					    	
					    		  sb.append("<div class='col-sm-6 col-md-6' style='text-align:  right;' ><a href='#' onclick='return showReferencesForm();'>"+lnkAddRef+"</a></div>");
					       }
					    sb.append("</div></td></tr>");
						//sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'><b>"+Utility.getLocaleValuePropByKey("lblRef", locale)+"</b></td></tr>");
					}
					sb.append("<tr><th colspan=5 width='100%'>");
						sb.append("<table border=0 width='100%' onmouseover=\"javascript:setGridProfileVariable('References')\">");
							sb.append("<tr>");
								sb.append("<th style="+sGridtableDisplayCSS+" id='gridDataReference'>");
								sb.append("</th>");
							sb.append("</tr>");
						sb.append("</table>");
					sb.append("</th></tr>");
					sb.append("<script>getElectronicReferencesGrid_DivProfile("+teacherDetail.getTeacherId()+");</script>");
					// End Reference Grid
					
					//TestTool.getTraceTimeTPAP("TPAP 216");
		//RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
					
					sb.append("<tr><td style='padding-left: 15px;'>");
					sb.append("<div class='portfolio_Section_ImputFormGap' style='display: none;' id='divElectronicReferences'>");
					sb.append("<div class='row'>");
					sb.append(" <div class='col-sm-12 col-md-12'>");
						sb.append(" <div class='divErrorMsg' id='errordivElectronicReferences' style='display: block;''></div>");
					sb.append(" </div>");
					sb.append(" </div>");
					
					   sb.append("<iframe id='uploadFrameReferencesID' name='uploadFrameReferences' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'></iframe>");
					   sb.append("<form id='frmElectronicReferences' name='frmElectronicReferences' enctype='multipart/form-data' method='post' target='uploadFrameReferences' action='fileuploadservletforreferencesnoble.do' >");
					   sb.append("<input type='hidden' id='teacherId' name='teacherId' value="+teacherId+" >");	
					   sb.append("<div class='row'>");
								
							sb.append("<div class='col-sm-3 col-md-3'>");
								 sb.append("<label>Salutation</label>");
								   sb.append("<select class='form-control' id='salutation' name='salutation'>");
									    sb.append("<option value='0'></option>");
										sb.append("<option value='4'>Dr.</option>");
										sb.append("<option value='3'>Miss</option>");
										sb.append("<option value='2'>Mr.</option>");
										sb.append("<option value='1'>Mrs.</option>");
										sb.append("<option value='5'>Ms.</option>");													
									 sb.append("</select>");
									sb.append("</div>");

							sb.append("<div class='col-sm-3 col-md-3'>");
							  sb.append("<label>"+lblFname+"<span class='required'>*</span></label>");
							   sb.append("<input type='hidden' id='elerefAutoId'>");
							   sb.append("<input type='text' id='firstName1' name='firstName1' class='form-control' placeholder='' maxlength='50'>");
							sb.append("</div>");
							
							sb.append("<div class='col-sm-3 col-md-3'>");
							  sb.append("<label>"+lblLname+"<span class='required'>*</span></label>");
							sb.append("<input type='text' id='lastName1' name='lastName1' class='form-control' placeholder='' maxlength='50'>");
							sb.append("</div>");
				        sb.append("</div>");
				        
				        sb.append("<div class='row'>");
				          sb.append("<div class='col-sm-3 col-md-3'>");
				          
				          if(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==NC_HEADQUARTER)
				          {
				        	  sb.append("<label>"+lblTitle+"<span class='required'>*</span></label>"); 
				          }
				          else
				          {
				        	  sb.append("<label>"+lblTitle+"</label>");
				          }
				        	  
				        		 sb.append("<input type='text' id='designation' name='designation' class='form-control' placeholder='' maxlength='50'>");
				          sb.append("</div>");
						
				          sb.append("<div class=col-sm-6 col-md-6'>");
		                  sb.append("<label>"+lblOrga+"/Emp.<span class='required'>*</span></label>");
				          sb.append("<input type='text' id='organization' name='organization' class='form-control' placeholder='' maxlength='50'>");
				         sb.append("</div>");
				       sb.append("</div>");
					
				       
				       sb.append("<div class='row '>");
				        sb.append("<div class='col-sm-3 col-md-3'>");
				    	 sb.append("<label>"+lblContNum+"<span class='required'>*</span>&nbsp;<a href='#' id='iconpophover7' rel='tooltip' data-original-title='Phone number (area code first)'><img src='images/qua-icon.png' width='15' height='15' alt=''></a></label>");
				    	 sb.append("<script type='text/javascript'>$('#iconpophover7').tooltip();</script>");  
				    	 //sb.append("<input type='text' id='contactnumber' name='contactnumber' class='form-control' placeholder='' maxlength='10'>");
				    	 sb.append("<input type='text' id='contactnumber' name='contactnumber' onKeyUp='numericFilter(this)'  class='form-control' placeholder='' maxlength='10'>");
				    	 sb.append("</div>");
					
				    	 sb.append("<div class='col-sm-6 col-md-6'>");
				    		sb.append("<label>"+lblEmail+"<span class='required'>*</span></label>");
				    			sb.append("<input type='text' id='email' name='email' class='form-control' placeholder='' maxlength='50'>");
				    	 sb.append("</div>");
				       sb.append("</div>");
				       
				       
				       sb.append(" <div class='row top15 hide'>");
				         sb.append("<div class='col-sm-3 col-md-3'>");
				    	    sb.append("<label>");
				    		 sb.append(lblRecommendLetter);
				    		 sb.append("</label>");
				    		 sb.append("<input type='hidden' id='sbtsource_ref' name='sbtsource_ref' value='0'/>");
				    		 sb.append("<input id='pathOfReferenceFile' name='pathOfReferenceFile' type='file' width='20px;'>");
				    			sb.append("<a href='javascript:void(0)' onclick='clearReferences()>"+lnkClear+"</a>");
				    	 sb.append("</div>");
				    		sb.append("<input type='text' id='pathOfReference'/>");
				    	 sb.append("<div class='col-sm-3 col-md-3' id='removeref' name='removeref' style='display: none;'>");
				    		sb.append("<label>&nbsp;&nbsp;");
				    	    sb.append("</label>");
				    	    sb.append("</div>");
				    	    sb.append("<span id='divRefName'>");
				    	    sb.append("</span>");
			                sb.append("&nbsp;&nbsp;&nbsp;<a href='javascript:void(0)' onclick='removeReferences()'>"+lnkRemo+"</a>&nbsp;&nbsp;&nbsp;");
			                //sb.append(" <a href='#' id='iconpophover7' rel='tooltip' data-original-title='Remove recommendation letter !'><img src='images/qua-icon.png' width='15' height='15' alt=''></a>");
			               
				         sb.append("</div>");
			          sb.append("</div>");
			          
			          String cntTP="show";
				       if(userMaster.getDistrictId()!=null && userMaster.getDistrictId().getDistrictId().equals(7800038)){
				    	   cntTP="hide";
				       }
				       
			          sb.append(" <div class='row top10 "+cntTP+"'>");
			        	sb.append("<div class='col-sm-12 col-md-12'>");
			        	 sb.append("<label>"+lblPrsnDirectlyContByHiringAuth+"</label>");
			        	  sb.append("<div class='' id='' style='height: 40px;'>");
			        		  sb.append("<div class='radio inline col-sm-1 col-md-1'>");
			        		  sb.append("<input type='radio' checked='checked' id='rdcontacted1' value='1'  name='rdcontacted'> Yes");
			        		  sb.append("</div></br>");
							    sb.append("<div class='radio inline col-sm-1 col-md-1' style='margin-left:20px;margin-top:-10px;'>");
							    sb.append("<input type='radio' id='rdcontacted0' value='0' name='rdcontacted'> No");
							  sb.append(" </div>");
							sb.append("  </div>");
						sb.append("</div>");
					sb.append("</div>");
					
					String hlhyk="hide";
				       if(userMaster.getDistrictId()!=null && userMaster.getDistrictId().getDistrictId().equals(4218990)){
				    	   hlhyk="show";
				       }
					sb.append(" <div class='row top10 "+hlhyk+"'>");
		       	 	sb.append("<div class='col-sm-5 col-md-5'>");
			        sb.append("<label>"+qnhowlongknwperson+"</label><input type='text' id='longHaveYouKnow' name='longHaveYouKnow' class='form-control' placeholder=''>");
			        sb.append("</div>");
			        sb.append("</div>");
			        
				 sb.append(" <div class='row'>");
		        	 sb.append("<div class='col-sm-12 col-md-12'>");
			        	sb.append("<label>"+lblReferenceDetails+"</label>");
			        	sb.append("<div id='referenceDetailstext'>");
			        	  sb.append("<textarea id='referenceDetails' name='referenceDetails' ></textarea>");
		        	     sb.append("</div>");
		        	 sb.append("</div>");
		         sb.append("</div>");
					
		       sb.append("<div class='row top5'>");
		    	sb.append("<div class='col-sm-3 col-md-3 idone'>");
		    		sb.append("<a href='#' style='cursor: pointer; text-decoration:none;' onclick='return insertOrUpdateElectronicReferencesM(0)'>"+lnkImD+"</a>&nbsp;");
		    		sb.append(" &nbsp;&nbsp;<a href='#' style='cursor: pointer; text-decoration:none;' onclick='return hideElectronicReferencesForm()'>");
		    		sb.append(lnkCancel);
		    		 sb.append("</a>");
		    		sb.append("</div>");
		    	sb.append("</div>");
													
				    sb.append("</form>");
				
				    sb.append("</div>");
				    sb.append("</div>");
				       
				// END RRREFERENCESRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
				   sb.append("</td></tr>");
				}
				if(userMaster.getDistrictId()!=null && userMaster.getDistrictId().getDistrictId()==3904374)
				{
					sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+";padding-left: 0px;'>");
					sb.append("<div class=''>");
					sb.append("<div class='col-sm-6 col-md-6'>Background Check</div>");				  
					sb.append("</div></td></tr>");
					sb.append("<tr><td colspan=5 width='100%'>");
					sb.append("<table width='100%'>");
					sb.append("<tr>");
					sb.append("<td style="+sGridtableDisplayCSS+"><div id='gridDataBackgroundCheck' class=''></div>");
					sb.append("</td>");
					sb.append("</tr>");
					sb.append("</table>");
					sb.append("</td></tr>");
					sb.append("<script>getBackgroundCheckGrid_DivProfile("+teacherDetail.getTeacherId()+");</script>");
				}		
				if(!headQuarterMaster ||(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==NC_HEADQUARTER) || userMaster.getDistrictId()!=null)
				if(districtPortfolioConfig==null || (districtPortfolioConfig.getVideoLink()!=null && districtPortfolioConfig.getVideoLink()) )
				{
					// Start Video Grid
					if(sVisitLocation.equalsIgnoreCase("Mosaic")){
						//sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'>"+Utility.getLocaleValuePropByKey("lnkViLnk", locale)+"</td></tr>");
						sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+";padding-left: 0px;'>");
						    sb.append("<div class='col-sm-6 col-md-6'>"+Utility.getLocaleValuePropByKey("lnkViLnk", locale)+"</div>");
						    sb.append("<div class=''>");
					    	  if(userMaster.getRoleId().getRoleId().equals(2) || userMaster.getRoleId().getRoleId().equals(3))
					    	  {
					    		  sb.append("<div class='col-sm-6 col-md-6' style='text-align: right;' ><a href='#' onclick='return showVideoLinksForm();'>"+lnkAddViLnk+"</a></div>");
						    	  sb.append("<tr><td style='padding-left: 15px;'>");
						    	  sb.append("<label class ='top10'><strong>"+lblPleaseInclude+"</strong></label>");
						    	  sb.append("</td></tr>");
					    	  }			      
					    sb.append("</div></td></tr>");
					
					    //TestTool.getTraceTimeTPAP("TPAP 217");
					// Start Video Grid
					//if(sVisitLocation.equalsIgnoreCase("Mosaic")){
						//sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'>"+Utility.getLocaleValuePropByKey("lnkViLnk", locale)+"</td></tr>");
					}else if(sVisitLocation.equalsIgnoreCase("My Folders")){
						//sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'><b>"+Utility.getLocaleValuePropByKey("lnkViLnk", locale)+"</b></td></tr>");

						sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+";padding-left: 0px;'>");
						     sb.append("<div class='col-sm-6 col-md-6'>"+Utility.getLocaleValuePropByKey("lnkViLnk", locale)+"</div>");
						     sb.append("<div class=''>");
					    	  if(userMaster.getRoleId().getRoleId().equals(2) || userMaster.getRoleId().getRoleId().equals(3))
					    	  {
					    		  sb.append("<div class='col-sm-6 col-md-6' style='text-align: right; margin-left:-12px;' ><a href='#' onclick='return showVideoLinksForm();'>"+lnkAddViLnk+"</a></div>");
						    	  sb.append("<tr><td style='padding-left: 15px;'>");
						    	  sb.append("<label class ='top10'><strong>"+lblPleaseInclude+"</strong></label>");
						    	  sb.append("</td></tr>");  
					    	  }
					    sb.append("</div></td></tr>");
					
					//	sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'><b>"+Utility.getLocaleValuePropByKey("lnkViLnk", locale)+"</b></td></tr>");
					}else{
						//sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'><b>"+Utility.getLocaleValuePropByKey("lnkViLnk", locale)+"</b></td></tr>");
						
						sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+";padding-left: 0px;'>");
						   sb.append("<div class=''>");
						    sb.append("<div class='col-sm-6 col-md-6'>"+Utility.getLocaleValuePropByKey("lnkViLnk", locale)+"</div>");
					      
					    	  if(userMaster.getRoleId().getRoleId().equals(2) || userMaster.getRoleId().getRoleId().equals(3))
					    	  {
					    		  sb.append("<div class='col-sm-6 col-md-6' style='text-align: right; margin-left:-12px;' ><a href='#' onclick='return showVideoLinksForm();'>"+lnkAddViLnk+"</a></div>");
						    	  sb.append("<tr><td style='padding-left: 15px;'>");
						    	  sb.append("<label class ='top10'><strong>"+lblPleaseInclude+"</strong></label>");
						    	  sb.append("</div></td></tr>");
					    	  }
					      
					    sb.append("</td></tr>");
						//sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'><b>"+Utility.getLocaleValuePropByKey("lnkViLnk", locale)+"</b></td></tr>");
					}
					sb.append("<tr><th colspan=5 width='100%'>");
					sb.append("<table border=0 width='100%' onmouseover=\"javascript:setGridProfileVariable('videoLink')\">");
							sb.append("<tr>");
							//System.out.println(" ooooooooooooooooooooooooo ");
								sb.append("<th style="+sGridtableDisplayCSS+" id='gridDataVideoLink'>");
								//sb.append("<th style="+sGridtableDisplayCSS+"><div id='gridDataVideoLink' class=''></div>");
								sb.append("</th>");
							sb.append("</tr>");
						sb.append("</table>");
					sb.append("</th></tr>");
					sb.append("<script>getVideoLinksGrid_DivProfile("+teacherDetail.getTeacherId()+");</script>");
					// End Video Grid
					
					sb.append("<iframe id='uploadFrameVideoID' name='uploadFrameVideo' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'></iframe>");
		sb.append("<tr><td	style='padding-left: 15px;'>");
					sb.append("<div class='top15' style='display: none' id='divvideoLinks' onkeypress='return checkForEnter(event);'>");
					 sb.append("<div class='divErrorMsg' id='errordivvideoLinks' style='display: block;'></div>");
					 sb.append("<form class='' id='frmvideoLinks' name='frmvideoLinks' onsubmit='return false;' method='post'  enctype='multipart/form-data' target='uploadFrameVideo' action='certificationVideoUploadServlet.do?teacherIdForVideo="+teacherDetail.getTeacherId()+"'>");
						 sb.append("<div class='row'>");
							 sb.append("<div class='col-sm-10 col-md-10' style='max-width: 800px;'>");
								 sb.append("<label>Video Link</label>");
								 sb.append("<input type='hidden' id='videolinkAutoId' name='videolinkAutoId'>");
								 sb.append("<input type='text' id='videourl' name='videourl' class='form-control' placeholder='' maxlength='200'>");
							 sb.append("</div>");
						 sb.append("</div>");
						 
						 sb.append("<div class='row'>");
						 sb.append("<div class='col-sm-5 col-md-5' style='max-width: 800px;'><br/>");
							 sb.append("<label>Video</label>");
							 sb.append("<input type='file' id='videofile' name='videofile'>");
							 sb.append("<span style='display: none; visibility: hidden;' id='video'></span>");
						 sb.append("</div>");
						 sb.append("<span class='col-sm-6 col-md-6'> <br/><br/>");
						 sb.append(lblSupportedvideos);
						 sb.append("</span>");
					 sb.append("</div>");
							
						 sb.append("<div class='row top10'>");
						     sb.append("<div class='col-sm-4 col-md-4 idone'>");
						    	 sb.append("<a href='javascript:void(0)' onclick='insertOrUpdatevideoLinks();'>"+lnkImD+"</a>&nbsp;&nbsp;&nbsp;");
						    	 sb.append("<a href='#'	onclick='return hideVideoLinksForm()'>Cancel");
								 sb.append("</a>");
						     sb.append("</div>");
						 sb.append("</div>");
					 sb.append("</form>");
				    sb.append("</div>");
			      sb.append("</div>");
			 sb.append("</td></tr>");	
			}
			
			if(!headQuarterMaster ||(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==NC_HEADQUARTER) || userMaster.getDistrictId()!=null)	
			if(districtPortfolioConfig==null || (districtPortfolioConfig.getAdditionalDocuments()!=null && districtPortfolioConfig.getAdditionalDocuments()) )
			{
					//TestTool.getTraceTimeTPAP("TPAP 218");
					// Start Additional Document
					if(sVisitLocation.equalsIgnoreCase("Mosaic")){
						sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'>"+lnkAddDocu+"</td></tr>");
					}else if(sVisitLocation.equalsIgnoreCase("My Folders")){
						sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'><b>"+lnkAddDocu+"</b></td></tr>");
					}else{
						sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'><b>"+lnkAddDocu+"</b></td></tr>");
					}
					sb.append("<tr><th colspan=5 width='100%'>");
					sb.append("<table border=0 width='100%' onmouseover=\"javascript:setGridProfileVariable('document')\">");
							sb.append("<tr>");
								sb.append("<th style="+sGridtableDisplayCSS+" id='divGridAdditionalDocuments'>");
								sb.append("</th>");
							sb.append("</tr>");
						sb.append("</table>");
					sb.append("</th></tr>");
					sb.append("<script>getadddocumentGrid_DivProfile("+teacherDetail.getTeacherId()+");</script>");
			}	
					if(userMaster.getDistrictId()!=null && (userMaster.getDistrictId().getDistrictId().equals(4218990)|| userMaster.getDistrictId().getDistrictId().equals(7800040))){
					//Start language profiency
					sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'>"+lblLanguageProfiency+"</td></tr>");			
					sb.append("<table width='100%' onmouseover=\"javascript:setGridProfileVariable('languageProfiency')\">");
					sb.append("<tr>");
					sb.append("<td style="+sGridtableDisplayCSS+"><div id='divGridlanguageProfiency' class=''></div>");
					sb.append("</td>");
					sb.append("</tr>");
					sb.append("</table>");
					sb.append("<script>getlanguage_DivProfile("+teacherDetail.getTeacherId()+");</script>");
				
					//end language profiency
					}
					if(districtMaster!=null && districtMaster.getDistrictId().equals(4218990))
					if(getDistrictAssessmentFlag(teacherDetail))
					{
						// Start Assessment Details
						if(sVisitLocation.equalsIgnoreCase("Mosaic")){
							sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'>"+lblAssessmentDetails+"</td></tr>");
						}else if(sVisitLocation.equalsIgnoreCase("My Folders")){
							sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'><b>"+lblAssessmentDetails+"</b></td></tr>");
						}else{
							sb.append("<tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'><b>"+lblAssessmentDetails+"</b></td></tr>");
						}
						sb.append("<tr><th colspan=5 width='100%'>");
						sb.append("<table border=0 width='100%' onmouseover=\"javascript:setGridProfileVariable('assessmentDetails')\">");
								sb.append("<tr>");
									sb.append("<th style="+sGridtableDisplayCSS+" id='divGridAssessmentDetails'>");
									sb.append("</th>");
								sb.append("</tr>");
							sb.append("</table>");
						sb.append("</th></tr>");
						sb.append("<script>getdistrictAssessmetGrid_DivProfile("+teacherDetail.getTeacherId()+");</script>");
						// End Assessment Details
					}
					
					/*
		             ************************************************************
		             				Start Transcript Verification
		             ************************************************************ 
		             * */
					
					if(userMaster!=null && userMaster.getDistrictId()!=null && userMaster.getDistrictId().getDistrictId().equals(1200390)){
		            String candidateFname 	 = "";
		            String candidateLname 	 = "";
		            String candidateFullName = "";
		            if(teacherDetail!=null){
		            	candidateFname = teacherDetail.getFirstName()==null?"":teacherDetail.getFirstName();
		            	candidateLname = teacherDetail.getLastName()==null?"":teacherDetail.getLastName();
		            	candidateFullName 	= 	candidateFname+" "+candidateLname;
		            	candidateFullName	=	candidateFullName.replace("'","\\'");
		            }
		            
		            if(userMaster!=null && userMaster.getDistrictId()!=null && userMaster.getDistrictId().getDistrictId().equals(1200390)){
			            sb.append("<table>");
			            	sb.append("<tr>");
			            	sb.append("<td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'>Transcript Verification : </td>");
			            	sb.append("<td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'><a href='javascript:void:(0);' onclick=\"return verifyTranscript('"+teacherId+"','"+candidateFullName+"')\">Transcript Verification<a></td>");
			            	sb.append("</tr>");
			            sb.append("</table>");
		            }
		            /*
		             ************************************************************
		             				End Transcript Verification
		             ************************************************************ 
		             * */
					}
		            String pfcontent = candidateGridSubAjax.getDistrictSpecificPortfolioQuestions(0,teacherId);
		            if(pfcontent!=null && !pfcontent.equals("")){
		                            sb.append("<table><tr><td colspan=5 class='net-header-text' style='"+sGridLabelCss+"'>"+LBLDistrictSpecificQuestion+"</td></tr></table>");
		                            if(districtMaster!=null && districtMaster.getDistrictId().equals(7800038))
		                                sb.append("<div class='row '>");
		                                else
		                                	//sb.append("<div class='row' >");
		                                sb.append("<div class='row ' style='pointer-events: none;'>");
		                            sb.append("<div class='col-sm-12 col-md-12'><div id='errordivspecificquestion' class='required' style='margin-left:10px;'></div><table style='margin-left:10px;' width='90%'>"+pfcontent+"</table>");
		                            sb.append("</div></div>");
		                            if(userMaster.getDistrictId()!=null && userMaster.getDistrictId().getDistrictId().equals(7800038))
		                            if(userMaster.getRoleId().getRoleId().equals(2)){
		                                            sb.append("<div class='col-sm-12 col-md-12'><button class='btn btn-primary' type='button' onclick=\"javascript:saveDistrictSpecificQues('"+teacherId+"')\">"+lblSaveQuestion+"</button></div>");
		                            }
		            }

				//End district specific question
					//Start Div Footer
					//End Div Footer
					
					sb.append("</table>");
					}//added
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
				
				finally
				{
					//TestTool.getTraceTimeTPAP("TPAP 219");
					sb.append("<script>$('textarea').jqte();$('.tooltipEmpNo').tooltip();</script>");
					
					TestTool.getTraceSecond("end return");
					if(fg!=0)
						return sb.toString().split("####")[1];
					else
						return sb.toString();
				}
			//}
			//catch(Exception e)
			//{e.printStackTrace();}
			

		}
		
		public String showProfilePartialData(Integer teacherId,Integer noOfRecordCheck,String sVisitLocation,DistrictMaster districtMaster_Input,int fg)
		{
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}
			DistrictMaster districtMaster=null;
			SchoolMaster schoolMaster=null;
			UserMaster userMaster=null;
			Integer roleId=0;Integer entityID=0;
			if (session == null || session.getAttribute("userMaster") == null) {				
			}else{
				userMaster=(UserMaster)session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
				if(userMaster.getDistrictId()!=null){
					districtMaster=userMaster.getDistrictId();
				}
				if(userMaster.getSchoolId()!=null){
					schoolMaster =userMaster.getSchoolId();
				}	
				entityID=userMaster.getEntityType();
			}
			StringBuilder sb = new StringBuilder();
		
			SimpleDateFormat sdf=new SimpleDateFormat("MM-dd-yyyy");
			Date endStartDate=null,endToDate=null;

			TeacherDetail teacherDetail = null;
			try{
				teacherDetail = teacherDetailDAO.findById(teacherId,false,false);
			}catch(Exception e){
				e.printStackTrace();
			}
			String firstAppliedOn="";
			String lastAppliedOn="";
			Date lastActivityDate=null;
			String lastContactedOn="None";
			String noofviews="0";
			
			boolean noEPIFlag=false;
			if(userMaster.getDistrictId()!=null){
			districtMaster=userMaster.getDistrictId();
				if(districtMaster.getNoEPI()==null ||(districtMaster.getNoEPI()!=null && districtMaster.getNoEPI()==false)){
					noEPIFlag=true;
				}
			}
			try {
		

				TeacherProfileVisitHistory teacherProfileVisitHistory=new TeacherProfileVisitHistory();
				teacherProfileVisitHistory.setUserMaster(userMaster);
				teacherProfileVisitHistory.setTeacherDetail(teacherDetail);
				teacherProfileVisitHistory.setJobOrder(null);
				teacherProfileVisitHistory.setVisitLocation(sVisitLocation);
				teacherProfileVisitHistory.setVisitedDateTime(new Date());
				teacherProfileVisitHistory.setIpAddress(IPAddressUtility.getIpAddress(request));
				
				if(userMaster.getDistrictId()!=null)
					teacherProfileVisitHistory.setDistrictId(userMaster.getDistrictId());
				if(userMaster.getSchoolId()!=null)
					teacherProfileVisitHistory.setSchoolId(userMaster.getSchoolId());
				
				teacherProfileVisitHistoryDAO.makePersistent(teacherProfileVisitHistory);
				// End Insert teacherprofilevisithistory
				
			
				List<String[]> minEndStartDateFromJobOrder=jobForTeacherDAO.findMinJobEndDateFromJobForteacher(teacherDetail, userMaster);
				for (Iterator it = minEndStartDateFromJobOrder.iterator(); it.hasNext();){
					 Object[] row = (Object[]) it.next();
					endStartDate = (Date) row[1];
					endToDate 	= (Date) row[0];
				}
				
				JobForTeacher jobForTeacher = null;
				
				if(minEndStartDateFromJobOrder.size()>0)
				{	
					firstAppliedOn = Utility.convertDateAndTimeToUSformatOnlyDate(endStartDate);
					lastAppliedOn = Utility.convertDateAndTimeToUSformatOnlyDate(endToDate);
					
					try {				
					List<String[]> lastConTacted=jobForTeacherDAO.findLastContactedOnJobForteacher(teacherDetail, userMaster,null);
					
					Long jftId =0L;
					for (Iterator it = lastConTacted.iterator(); it.hasNext();){
						 Object[] row = (Object[]) it.next();
						 jftId=Long.parseLong(row[1].toString());
					
					}
					
					JobForTeacher jobForTeacher2 =null;
					
					if(jftId!=0){
						jobForTeacher2 = jobForTeacherDAO.findById(jftId, false, false);
						lastActivityDate = jobForTeacher2.getLastActivityDate();
					}
					if(lastActivityDate!=null)
					{
						lastContactedOn ="<a href=\"javascript:void(0);\" onclick=\"getCommunicationsDiv(1,0,"+teacherDetail.getTeacherId()+",0);\"><span class=\"divlablelinkval\">"+Utility.convertDateAndTimeToUSformatOnlyDate(lastActivityDate)+" ("+jobForTeacher2.getUserMaster().getFirstName()+" "+jobForTeacher2.getUserMaster().getLastName()+")</span></a>";
						//onclick=\"getCommunicationsDiv(1,0,'"+teacherDetail.getTeacherId()+"',0);\" 
					}
					
					} catch (Exception e) {
						// TODO: handle exception
					}
					
					Criterion criterion_teacherDetail = Restrictions.eq("teacherDetail",teacherDetail);
					List<TeacherProfileVisitHistory> listTeacherProfileVisitHistory=null;
					if(entityID==1)
						listTeacherProfileVisitHistory = teacherProfileVisitHistoryDAO.findByCriteria(criterion_teacherDetail);
					else 
					{
						Criterion criterion_districtId = Restrictions.eq("districtId",districtMaster);
						listTeacherProfileVisitHistory = teacherProfileVisitHistoryDAO.findByCriteria(criterion_teacherDetail,criterion_districtId);
					}
																	
					int iTeacherProfileVisitHistoryCount=0;
					if(listTeacherProfileVisitHistory.size()>0)
						iTeacherProfileVisitHistoryCount=listTeacherProfileVisitHistory.size();

					if(iTeacherProfileVisitHistoryCount>0){
						noofviews = iTeacherProfileVisitHistoryCount+"";
						sb.append("<script type='text/javascript'>$('.nOofViews"+teacherId+"').html('&nbsp;"+noofviews+"');$('.nOofViews"+teacherId+"').css('pointer-events','');</script>");
						
					}
				}
				sb.append("<script type='text/javascript'>$('#firstJobApplied"+teacherId+"').html('&nbsp;"+firstAppliedOn+"');</script>");
				sb.append("<script type='text/javascript'>$('#lastJobApplied"+teacherId+"').html('&nbsp;"+lastAppliedOn+"');</script>");
				sb.append("<script type='text/javascript'>$('#lastContacted"+teacherId+"').html('&nbsp;"+lastContactedOn+"');</script>");
				
				if(noEPIFlag){
					sb.append("<script type='text/javascript'>$('#epiScoreInPrf"+teacherId+"').html('&nbsp;'+$('#normScoreByT"+teacherId+"').html());</script>");				
				}
				sb.append("<script type='text/javascript'>$('#jobListByTProfile"+teacherId+"').html('&nbsp;'+$('#jobListByT"+teacherId+"').html());</script>");
				//sb.append(b)normScoreByT
				//jobListByT
		
				
			}catch (Exception e) {
				// TODO: handle exception
			}
			return sb.toString();
		}
		
		public String showProfileContentForTeacherNewMul(String teacherIdRec,String sVisitLocation)
		{
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}
			DistrictMaster districtMaster=null;
			SchoolMaster schoolMaster=null;
			UserMaster userMaster=null;
			boolean nobleflag=false;
			int entityID=0,roleId=0;
			boolean noEPIFlag=false;
			boolean jeffcoFlag=false;
			if (session == null || session.getAttribute("userMaster") == null) {				
			}else{
				userMaster=(UserMaster)session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
				if(userMaster.getDistrictId()!=null){
					districtMaster=userMaster.getDistrictId();
					if(districtMaster.getNoEPI()==null ||(districtMaster.getNoEPI()!=null && districtMaster.getNoEPI()==false)){
						noEPIFlag=true;
					}

					if(districtMaster.getDistrictId()==7800038)
						nobleflag=true;
					//804800
					if(districtMaster.getDistrictId()==804800)
					jeffcoFlag=true;
				}
				if(userMaster.getSchoolId()!=null){
					schoolMaster =userMaster.getSchoolId();
				}	
				entityID=userMaster.getEntityType();
			}
			
			boolean achievementScore=false,tFA=false,demoClass=false,JSI=false,teachingOfYear =false,expectedSalary=false,fitScore=false,pnqDis=false;
			if(entityID==1)
			{
				achievementScore=true;tFA=true;demoClass=true;JSI=true;teachingOfYear =true;expectedSalary=true;fitScore=true;
			}else
			{
				try{
					DistrictMaster districtMasterObj=districtMaster;
					if(districtMasterObj!=null && districtMasterObj.getDisplayAchievementScore()!=null && districtMasterObj.getDisplayAchievementScore()){
						achievementScore=true;
					}
					if(districtMasterObj!=null && districtMasterObj.getDisplayTFA()!=null && districtMasterObj.getDisplayTFA()){
						tFA=true;
					}
					if(districtMasterObj!=null && districtMasterObj.getDisplayDemoClass()!=null && districtMasterObj.getDisplayDemoClass()){
						demoClass=true;
					}
					if(districtMasterObj!=null && districtMasterObj.getDisplayJSI()!=null && districtMasterObj.getDisplayJSI()){
						JSI=true;
					}
					if(districtMasterObj!=null && districtMasterObj.getDisplayYearsTeaching()!=null && districtMasterObj.getDisplayYearsTeaching()){
						teachingOfYear=true;
					}
					if(districtMasterObj!=null && districtMasterObj.getDisplayExpectedSalary()!=null && districtMasterObj.getDisplayExpectedSalary()){
						expectedSalary=true;
					}
					if(districtMasterObj!=null && districtMasterObj.getDisplayFitScore()!=null && districtMasterObj.getDisplayFitScore()){
						fitScore=true;
					}
					if(districtMasterObj!=null && districtMasterObj.getDistrictId()!=null && districtMasterObj.getDistrictId().equals(7800038)){
						pnqDis=true;
					}
				}catch(Exception e){ e.printStackTrace();}
			}

			StringBuilder sb = new StringBuilder();
			
			boolean headQuarterMaster =false;
			if(userMaster!=null && userMaster.getHeadQuarterMaster()!=null){
				headQuarterMaster=true;
			}
			String windowFunc="if(this.href!='javascript:void(0);')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
			
			try
			{
				
				String realPath = Utility.getValueOfPropByKey("teacherRootPath");
				String webPath = Utility.getBaseURL(request);
				String sWidthFoeDiv="765";

				String sDivCloseJsFunctionName="showProfileContentClose";
				Map<Integer, Integer> teacherAndChk = new HashMap<Integer, Integer>();
				if(sVisitLocation.equalsIgnoreCase("My Folders"))
					sDivCloseJsFunctionName="showProfileContentClose_ShareFolder";
				
		
				
				try{
					List<Integer> teacherIdsList = new ArrayList<Integer>();
					String[] teacherArr = teacherIdRec.split(",");
					
					if(teacherArr.length>0){
						 for(String tNrec:teacherArr){
							 String[] teacherArrN = tNrec.split("##");							 
							 if(teacherArrN.length>0){
								 teacherIdsList.add(Integer.parseInt(teacherArrN[0]));
								 teacherAndChk.put(Integer.parseInt(teacherArrN[0]), Integer.parseInt(teacherArrN[1]));
							 }
						 }
							 
					 }
					List<TeacherDetail> listOfTeachers = teacherDetailDAO.findByTeacherIds(teacherIdsList);
					List<TFAAffiliateMaster> tfaAffiliateMasterList = tfaAffiliateMasterDao.findAll();
					Criterion criterion = Restrictions.in("teacherDetail",listOfTeachers);
					List<TeacherExperience> lstTeacherExperienceLst=teacherExperienceDAO.findByCriteria(criterion);
					Criterion criterion1 = Restrictions.in("teacherId",teacherIdsList);
					List<TeacherPersonalInfo> lstTeacherPersonalInfoList=teacherPersonalInfoDAO.findByCriteria(criterion1);
					
					Map<Integer, TeacherPersonalInfo> mapTpInfo = new HashMap<Integer, TeacherPersonalInfo>();
					Map<Integer, TeacherExperience> mapExpInfo = new HashMap<Integer, TeacherExperience>();
					for (TeacherPersonalInfo teacherPersonalInfo : lstTeacherPersonalInfoList) {
						mapTpInfo.put(teacherPersonalInfo.getTeacherId(), teacherPersonalInfo);
					}
					
					for (TeacherExperience teacherExp : lstTeacherExperienceLst) {
						mapExpInfo.put(teacherExp.getTeacherId().getTeacherId(), teacherExp);				
					}
		
					boolean tfaEditAuthUser = tfaEditAuthUser();
					
					//forloop start
					for (TeacherDetail teacherDetail : listOfTeachers) {
						/*====================================================*/
						sb.append("<div id='profileDivUtId"+teacherDetail.getTeacherId()+"'>");
						List<TeacherExperience> lstTeacherExperience_new = new ArrayList<TeacherExperience>();
						if(mapExpInfo.get(teacherDetail.getTeacherId())!=null){
							lstTeacherExperience_new.add(mapExpInfo.get(teacherDetail.getTeacherId()));
						}
						List<TeacherPersonalInfo> lstTeacherPersonalInfo_new = new ArrayList<TeacherPersonalInfo>();
						if(mapTpInfo.get(teacherDetail.getTeacherId())!=null){
							lstTeacherPersonalInfo_new.add(mapTpInfo.get(teacherDetail.getTeacherId()));
						}
						String imgPath="";
						try{
							if(teacherDetail.getIdentityVerificationPicture()!=null){
								imgPath=CommonDashboardAjax.showPictureWeb(teacherDetail, realPath, webPath);
							}
						}catch(Exception e){
							e.printStackTrace();
						}
						////////////done///////////////////					
						String width="798px";
							if(userMaster.getEntityType().equals(2) || userMaster.getRoleId().getRoleId().equals(3))
							  width ="900px";
							
						if(!teacherDetail.getFirstName().equals("")){
							sb.append("<div class='modal-dialog-for-cgprofile' style='width: "+width+"'>"+
							"<div class='modal-content modal-border'>"+		
							"<div class='modal-header modal_header_profile' id='teacherDiv'> "+
							"<button type='button' class='close' data-dismiss='modal' onclick='"+sDivCloseJsFunctionName+"();'>x</button>"+
							"<h3>"+teacherDetail.getFirstName()+" "+teacherDetail.getLastName()+"'s Profile</h3>"+
							"</div>"+
							"<div class='modal-body scrollspy_profile' style='min-width:"+sWidthFoeDiv+"px;'>");
							}



						String showImg="";

						showImg="<img src=\"images/nophoto.png\" align='left' class='circle' style='width: 48px;height: 48px;margin: 0em auto;' >";


						TestTool.getTraceSecond("start");
					JobForTeacher jobForTeacher = null;
					String firstAppliedOn="";
					String lastAppliedOn="";
					Date lastActivityDate=null;
					String lastContactedOn="None";

					List<TeacherDetail> lstTeacherDetails	  =	 new ArrayList<TeacherDetail>();
					lstTeacherDetails.add(teacherDetail);

					CandidateGridService cgService=new CandidateGridService();

					DistrictPortfolioConfig districtPortfolioConfig=null;

					//System.out.println("New setting Start");

					// New setting Start
					sb.append("<div id='errorDiv' style='color:red;display:none;padding-left:60px;'></div>");
					sb.append("<table id='profile_id' width="+sWidthFoeDiv+" cellspacing=0 cellpadding=0 class='tablecgtbl'>");

						sb.append("<tr >");
							sb.append("<th style='padding-top:10px;text-align:left;'>");
							
								sb.append("<table border='0' cellspacing=0 cellpadding=0 class='tablecgtbl'>");
									sb.append("<tr>");
										String strCss_lbl="padding:2px;text-align:left;";
										String strCss_value="width=42px; vertical-algin:top;text-align:left;padding:0px;padding-left:5px;padding-right:5px;padding-top:5px;";
										sb.append("<th width=50 valign='top'>"); // Photo
											sb.append("<table border='0' cellspacing=0 cellpadding=0 class='tablecgtbl'>");
												sb.append("<tr><th style='"+strCss_value+"'>"+showImg+"</th></tr>");
											sb.append("</table>");
										sb.append("</th>");
										
										sb.append("<th style='text-align:left;'>");
											sb.append("<table border='0' cellspacing=0 cellpadding=0 class='tablecgtbl' style='border-spacing:0;'>");
											
											if(sVisitLocation.equalsIgnoreCase("My Folders"))
											{
												strCss_lbl="padding:0px;text-align:left;";
												strCss_value="padding:0px;text-align:left;";
											}else{
												strCss_lbl="padding:2px;text-align:left;";//OK
												strCss_value="padding:2px;text-align:left;";//OK
											}
											
												//Name:
												sb.append("<tr><th class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblName1+":</th><th class='divlableval' style='"+strCss_value+"'>&nbsp;"+teacherDetail.getFirstName()+" "+teacherDetail.getLastName()+"</th></tr>");
												
												/* @Start
												 * @Ashish Kumar
												 * @Description :: Add Address of Teacher
												 * */
												//TestTool.getTraceTimeTPAP("TPAP 205");
												
												TeacherPersonalInfo teacherPersonalInfo = null;
												if(lstTeacherPersonalInfo_new!=null && lstTeacherPersonalInfo_new.size()>0)
													teacherPersonalInfo = lstTeacherPersonalInfo_new.get(0);
												
												if(userMaster.getDistrictId()!=null && userMaster.getDistrictId().getDistrictId()!=null && userMaster.getDistrictId().getDistrictId()==1201470)	//OSCEOLA
													if(teacherPersonalInfo!=null)
														if(teacherPersonalInfo.getAnotherName()!=null)
															sb.append("<tr><th class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblAname+":</th><th class='divlableval' style='"+strCss_value+"'>&nbsp;"+teacherPersonalInfo.getAnotherName()+"</th></tr>");
														else
															sb.append("<tr><th class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblAname+":</th><th class='divlableval' style='"+strCss_value+"'>&nbsp;</th></tr>");
												if(userMaster.getDistrictId()!=null && userMaster.getDistrictId().getDistrictId()!=null && userMaster.getDistrictId().getDistrictId()==7800294)	//WAUKESHA 
													if(teacherPersonalInfo!=null)
														if(teacherPersonalInfo.getDistrictEmail()!=null)
															sb.append("<tr><td class='divlable' style='"+strCss_lbl+"'>&nbsp;District Email Address:</td><td class='divlableval' style='"+strCss_value+"'>&nbsp;"+teacherPersonalInfo.getDistrictEmail()+"</td></tr>");
														else
															sb.append("<tr><td class='divlable' style='"+strCss_lbl+"'>&nbsp;District Email Address:</td><td class='divlableval' style='"+strCss_value+"'>&nbsp;</td></tr>");
												
												String sEmployeeNumber="N/A";
												String sEmployeePosition="N/A";
												if(teacherPersonalInfo!=null && teacherPersonalInfo.getEmployeeNumber()!=null && !teacherPersonalInfo.getEmployeeNumber().equalsIgnoreCase(""))
													sEmployeeNumber=teacherPersonalInfo.getEmployeeNumber();
												if(teacherPersonalInfo!=null && teacherPersonalInfo.getLastPositionWhenEmployed()!=null && !teacherPersonalInfo.getLastPositionWhenEmployed().equalsIgnoreCase(""))
													sEmployeePosition=teacherPersonalInfo.getLastPositionWhenEmployed();
												PrintOnConsole.debugPrintln("Teacher Profile2", "sEmployeeNumber "+sEmployeeNumber);
												PrintOnConsole.debugPrintln("Teacher Profile2", "sEmployeePosition "+sEmployeePosition);

												try{
													if(teacherPersonalInfo!=null)
													{
														String stateName = null;
														String country = null;
														String cityName = null;
														String zip = null;
														boolean addCHK = false;
														
														if(lstTeacherPersonalInfo_new.get(0).getCountryId()!=null){
															country=lstTeacherPersonalInfo_new.get(0).getCountryId().getName();
														}
														if(lstTeacherPersonalInfo_new.get(0).getStateId()!=null){
															stateName = lstTeacherPersonalInfo_new.get(0).getStateId().getStateName()+", ";
															//System.out.println("<--stateName-->"+stateName);
															addCHK = true;
														}else{
															stateName = "";
														}
														
														if(lstTeacherPersonalInfo_new.get(0).getCityId()!=null)
														{
															cityName = lstTeacherPersonalInfo_new.get(0).getCityId().getCityName()+", ";
															//System.out.println("<--cityName-->"+cityName);
															addCHK = true;
														}else{
															cityName = "";
														}
														
														if(lstTeacherPersonalInfo_new.get(0).getZipCode()!=null && !lstTeacherPersonalInfo_new.get(0).getZipCode().equals(""))
														{
															zip = lstTeacherPersonalInfo_new.get(0).getZipCode();
															//System.out.println("<--zip-->"+zip);
															addCHK = true;
														}else{
															zip = "";
														}
														
														if(lstTeacherPersonalInfo_new.get(0).getAddressLine1()!=null && !lstTeacherPersonalInfo_new.get(0).getAddressLine1().equals(""))
														{
															if(stateName=="" && cityName =="" && zip=="" && lstTeacherPersonalInfo_new.get(0).getAddressLine2().equals(""))
															{
																sb.append("<tr><th class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblAdd+":</th><th class='divlableval' style='"+strCss_value+"'>&nbsp;"+lstTeacherPersonalInfo_new.get(0).getAddressLine1()+"</th></tr>");
																//System.out.println("<--AddressLine1-->"+lstTeacherPersonalInfo_new.get(0).getAddressLine1());
															}
															else
															{
																sb.append("<tr><th class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblAdd+":</th><th class='divlableval' style='"+strCss_value+"'>&nbsp;"+lstTeacherPersonalInfo_new.get(0).getAddressLine1()+",</th></tr>");
															}
															
															addCHK = true;
														}
														
														if(lstTeacherPersonalInfo_new.get(0).getAddressLine2()!=null && !lstTeacherPersonalInfo_new.get(0).getAddressLine2().equals(""))
														{
															if(stateName=="" && cityName =="" && zip=="")
															{
																sb.append("<tr><th class='divlable' style='"+strCss_lbl+"'>&nbsp;</th><th class='divlableval' style='"+strCss_value+"'>&nbsp;"+lstTeacherPersonalInfo_new.get(0).getAddressLine2()+"</th></tr>");
																//System.out.println("<--AddressLine2-->"+lstTeacherPersonalInfo_new.get(0).getAddressLine2());
															}
															else
															{
																sb.append("<tr><th class='divlable' style='"+strCss_lbl+"'>&nbsp;</th><th class='divlableval' style='"+strCss_value+"'>&nbsp;"+lstTeacherPersonalInfo_new.get(0).getAddressLine2()+",</th></tr>");
															}
															addCHK = true;
														}
														
														if(addCHK==false)
														{
															sb.append("<tr><th class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblAdd+":</th><th class='divlableval' style='"+strCss_value+"'>&nbsp;N/A</th></tr>");
														}
														else
														{
															sb.append("<tr><th class='divlable' style='"+strCss_lbl+"'>&nbsp;</th><th class='divlableval' style='"+strCss_value+"'>&nbsp;"+stateName+""+cityName+""+zip+"</th></tr>");
															
															if(country!=""){
																sb.append("<tr><th class='divlable' style='"+strCss_lbl+"'>&nbsp;</th><th class='divlableval' style='"+strCss_value+"'>&nbsp;"+country+"</th></tr>");	
															}
														}
													}
												}catch(Exception e)
												{
													e.printStackTrace();
												}
													
												TestTool.getTraceSecond("Norm Score Start");
												int normScore=0;
												String colorName="";
												TestTool.getTraceSecond("Norm Score End");
												if(noEPIFlag){
													sb.append("<tr><th class='divlable' style='"+strCss_lbl+";padding-top:10px;'>&nbsp;"+lblEPINorm+":</th><th class='divlableval' style='"+strCss_value+";padding-top:10px;padding-bottom:8px;' id='epiScoreInPrf"+teacherDetail.getTeacherId()+"'>&nbsp;");	
													if(normScore==0)
													{
														sb.append("N/A</th>");
														
													}
													else
													{
														String ccsName="";
														String normScoreLen=normScore+"";
														if(normScoreLen.length()==1){
															ccsName="nobground1";
														}else if(normScoreLen.length()==2){
															ccsName="nobground2";
														}else{
															ccsName="nobground3";
														}
														sb.append("<span class=\""+ccsName+"\" style='font-size: 11px;font-weight: bold;background-color: #"+colorName+";'>"+normScore+"</span>");
														//System.out.println(">>>>  "+sb+" <<<");
														sb.append("</th></tr>");
													}
												}
												//Veteran candidate
												
												boolean ssnCheck=true;
												if(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==NC_HEADQUARTER){
													ssnCheck=false;
												}
												
												if(ssnCheck)
												 try{
												   String  lblVeteran   = Utility.getLocaleValuePropByKey("lblVeteran", locale);
													   if(userMaster.getDistrictId()!=null && !userMaster.getDistrictId().getDistrictId().equals(804800) && !userMaster.getDistrictId().getDistrictId().equals(7800294))
													      if(teacherPersonalInfo!=null){
																if(teacherPersonalInfo.getIsVateran()!=null && teacherPersonalInfo.getIsVateran()==1){
																sb.append("<tr><td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblVeteran+":</td><td class='divlableval' style='"+strCss_value+"'>&nbsp;Yes</td></tr>");
																}
																
																if(userMaster.getRoleId().getRoleId()==2 ||userMaster.getRoleId().getRoleId()==7 || userMaster.getRoleId().getRoleId()==8){
																	if(teacherPersonalInfo.getSSN()!=null && !teacherPersonalInfo.getSSN().equals("")){
																		if(userMaster.getEntityType()!=3)
																		sb.append("<tr><td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblSSN+":</td><td class='divlableval' style='"+strCss_value+"'>&nbsp;"+Utility.decodeBase64(teacherPersonalInfo.getSSN())+"</td></tr>");
																	}else{
																		if(userMaster.getEntityType()!=3)
																		sb.append("<tr><td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblSSN+":</td><td class='divlableval' style='"+strCss_value+"'>&nbsp;N/A</td></tr>");
																	}
																}
															}else{
																sb.append("<tr><td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblVeteran+":</td><td class='divlableval' style='"+strCss_value+"'>&nbsp;"+lblNo+"</td></tr>");
																if(userMaster.getRoleId().getRoleId()==2 ||userMaster.getRoleId().getRoleId()==7 || userMaster.getRoleId().getRoleId()==8){
																	sb.append("<tr><td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblSSN+":</td><td class='divlableval' style='"+strCss_value+"'>&nbsp;N/A</td></tr>");
																}
															}
													}catch (Exception e) {
														e.printStackTrace();
													}
													//System.out.println("<--IsVateran--> "+teacherPersonalInfo.getIsVateran());
												//EmployeeNumber
												if(jeffcoFlag && !sEmployeeNumber.trim().equalsIgnoreCase("N/A"))
													sb.append("<tr><td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblEmpNum+"A1"+":</td><td class='divlableval' style='"+strCss_value+"'>&nbsp;<a class='tooltipEmpNo' data-original-title='Employee Information' href='javascript:void(0);' onclick='openEmployeeNumberInfo(\""+sEmployeeNumber+"\")'>"+sEmployeeNumber+"</a></td></tr>");
												else
													 if(!sEmployeeNumber.trim().equalsIgnoreCase("N/A")&&locale.equals("fr"))
														 sb.append("<tr><td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblEmpNum+"A1"+":</td><td class='divlableval' style='"+strCss_value+"'>&nbsp;<a class='tooltipEmpNo' data-original-title='Employee Information' href='javascript:void(0);' onclick='openEmployeeNumberInfoOther(\""+sEmployeeNumber+"\")'>"+sEmployeeNumber+"</a></td></tr>");	
													 else
														 sb.append("<tr><td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblEmpNum+":</td><td class='divlableval' style='"+strCss_value+"'>&nbsp;"+sEmployeeNumber+"</td></tr>");
												
												//Employee Position
												if(districtMaster!=null && districtMaster.getDistrictId().equals(7800294))
													sb.append("<tr><td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblEmpPosition+":</td><td class='divlableval' style='"+strCss_value+"'>&nbsp;"+sEmployeePosition+"</td></tr>");
													
												//First Job Applied On
												sb.append("<tr><th class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblFirstJobApplied+":</th><th class='divlableval' style='"+strCss_value+"' id='firstJobApplied"+teacherDetail.getTeacherId()+"'>&nbsp;"+firstAppliedOn+"</th></tr>");
												//Last Job Applied On
												sb.append("<tr><th class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblLastJobApplied+":</th><th class='divlableval' style='"+strCss_value+"' id='lastJobApplied"+teacherDetail.getTeacherId()+"'>&nbsp;"+lastAppliedOn+"</th></tr>");
												//Last Contacted On
												sb.append("<tr><th class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblLastContacted+":</th><th class='divlableval' style='"+strCss_value+"' id='lastContacted"+teacherDetail.getTeacherId()+"'>&nbsp;"+lastContactedOn+"</th></tr>");
												
												TeacherExperience teacherExperience = null;
												
												if(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==NC_HEADQUARTER )
												{
													System.out.println("inside headquatermaster");
													teacherExperience = mapExpInfo.get(teacherDetail.getTeacherId());
													List<TeacherPersonalInfo> ssnTeachersList = new ArrayList<TeacherPersonalInfo>();
													ArrayList<Integer> teacherIds = new ArrayList<Integer>();
													List<String> ssnList = new ArrayList<String>();
													List<LicensureNBPTS> licensureNBPTSList = new ArrayList<LicensureNBPTS>();
													
													teacherIds.add(teacherDetail.getTeacherId());													
													if(lstTeacherPersonalInfo_new!=null && lstTeacherPersonalInfo_new.size()>0 && teacherIds!=null && teacherIds.size()>0)
													{
														ssnTeachersList.add((lstTeacherPersonalInfo_new.get(0)));													
													}
													
													if(ssnTeachersList!=null && ssnTeachersList.size()>0)
													{
														System.out.println("inside ssnTeachersList");
													  for(TeacherPersonalInfo tpi:ssnTeachersList){
														if(tpi.getSSN()!=null && !tpi.getSSN().equals("")){
															System.out.println("ssssnnnn"+tpi.getSSN());
															ssnList.add(tpi.getSSN());
															System.out.println("ssnList:::::::"+ssnList);
															
														}
														//System.out.println("ssnList sizeeeeeeeee::"+ssnList.size()+"ssnnnnnnnnnnnnnnnnnnn::::::"+ssnList);
													}
													}
																							
													if(ssnList!=null && ssnList.size()>0){
														System.out.println("yhi dao 1");
														licensureNBPTSList = licensureNBPTSDao.getDataBySsnList(ssnList);
													}
													if(licensureNBPTSList!=null && licensureNBPTSList.size()>0)
													{
														String fullDate ="";
														String finalYear="";
														Format formatter = new SimpleDateFormat("yyyy-MM-dd");
														
														if(licensureNBPTSList.get(0).getExpirationDate()!=null)
														 fullDate = formatter.format(licensureNBPTSList.get(0).getExpirationDate());
														if(!fullDate.equals(""))
														 finalYear=fullDate.substring(0, 4);
														
														System.out.println("NBTS  cetri::::::::::YES");
														sb.append("<th class='divlable' style='"+strCss_lbl+"'>&nbsp;"+Utility.getLocaleValuePropByKey("lblNatiBoardCertiLice", locale)+":</th>");
														sb.append("<th  class='divlableval' style='"+strCss_value+"'>&nbsp;Y ("+finalYear+")</th>");
														
													}
													else
													{
														System.out.println("NBTS  cetri::::::::::NOOOOOOOOOO");
														sb.append("<th class='divlable' style='"+strCss_lbl+"'>&nbsp;"+Utility.getLocaleValuePropByKey("lblNatiBoardCertiLice", locale)+":</th>");
														sb.append("<th  class='divlableval' style='"+strCss_value+"'>&nbsp;N</th>");
													}
													
													
												}else{
													
														teacherExperience = mapExpInfo.get(teacherDetail.getTeacherId());
													sb.append("<tr>");
													
													
													sb.append("<th class='divlable' style='"+strCss_lbl+"'>&nbsp;"+Utility.getLocaleValuePropByKey("lblNatiBoardCertiLice", locale)+"</th>");
													
													
													if(teacherExperience!=null)
													{
														if(teacherExperience.getNationalBoardCert()==null)
														{
															sb.append("<th  class='divlableval' style='"+strCss_value+"'>&nbsp;N/A</th>");
														}
														else
														{
															if(teacherExperience.getNationalBoardCert()==true)
															{
																String sNationalBoardCertYear="";
																if(teacherExperience.getNationalBoardCertYear()!=null && !teacherExperience.getNationalBoardCertYear().equals(""))
																	sNationalBoardCertYear=teacherExperience.getNationalBoardCertYear().toString();
																sb.append("<th  class='divlableval' style='"+strCss_value+"'>&nbsp;Y ("+sNationalBoardCertYear+")</th>");
															}
															else if(teacherExperience.getNationalBoardCert()==false)
																sb.append("<th  class='divlableval' style='"+strCss_value+"'>&nbsp;N</th>");
														}
													}
													else
													{
														sb.append("<th  class='divlableval' style='"+strCss_value+"'>&nbsp;N/A</th>");
													}
														
													sb.append("</tr>");
												}
												
											sb.append("</table>");
										sb.append("</th>");
										
										sb.append("<th>");
											sb.append("<table border='0' cellspacing=0 cellpadding=0 class='tablecgtbl' style='border-spacing:0;'>");
											
											if(sVisitLocation.equalsIgnoreCase("My Folders"))
											{
												strCss_lbl="padding-left:20px;text-align:left;white-space:nowrap";
												strCss_value="padding-left:10px;text-align:left;white-space:nowrap";
											}
											else
											{
												strCss_lbl="padding:2px;padding-left:20px;text-align:left;white-space:nowrap";
												strCss_value="padding:2px;padding-left:10px;text-align:left;white-space:nowrap";
											}
												//TFA
												if(tFA)
												{
													sb.append("<tr>");
													sb.append("<th class='divlable' style='"+strCss_lbl+"'>&nbsp;TFA:</th>");
													String tFAVal = getTFAValueNew(lstTeacherExperience_new, teacherDetail, tfaAffiliateMasterList);
													
													String New_tFAVal = "";
													try
													{
														if(tFAVal!=null)
														{
															if(tfaEditAuthUser)
															{
																if(lstTeacherExperience_new.size()>0)
																	New_tFAVal = tFAVal+"<span id='edittfa'>&nbsp;&nbsp;<a href='javascript:void(0);' onclick='editTFAbyUser("+lstTeacherExperience_new.get(0).getTeacherId().getTeacherId()+");'>Edit</a></span><span id='savetfa' style='display:none; cursor:pointer;'>&nbsp;&nbsp;<a javascript:void(0); onclick='displayTFAMessageBox("+lstTeacherExperience_new.get(0).getTeacherId().getTeacherId()+");'>Save</a>&nbsp;</span><span id='canceltfa' style='display:none; cursor:pointer;'><a javascript:void(0); onclick='cancelTFAbyUser("+lstTeacherExperience_new.get(0).getTeacherId().getTeacherId()+");'>Cancel</a></span>";	
																else
																	New_tFAVal = tFAVal;
															}
															else
																New_tFAVal = tFAVal;
														}
														else
														{
															if(tfaEditAuthUser)
															{
																if(lstTeacherExperience_new.size()>0)
																	New_tFAVal = "N/A "+"<span id='edittfa'>&nbsp;&nbsp;<a href='javascript:void(0);' onclick='editTFAbyUser("+lstTeacherExperience_new.get(0).getTeacherId().getTeacherId()+");'>Edit</a></span><span id='savetfa' style='display:none;cursor:pointer;'>&nbsp;&nbsp;<a javascript:void(0); onclick='displayTFAMessageBox("+lstTeacherExperience_new.get(0).getTeacherId().getTeacherId()+");'>Save</a>&nbsp;</span><span id='canceltfa' style='display:none; cursor:pointer;'><a javascript:void(0); onclick='cancelTFAbyUser("+lstTeacherExperience_new.get(0).getTeacherId().getTeacherId()+");'>Cancel</a></span>";
																else
																	New_tFAVal = "N/A";
															}
															else
																New_tFAVal = "N/A";
														}
													}catch(Exception e)
													{
														e.printStackTrace();
													}
													sb.append("<th  class='divlableval' style='"+strCss_value+"'>&nbsp;"+New_tFAVal+"</th>");
													sb.append("</tr>");
													if(tFAVal!=null && lstTeacherExperience_new!=null && lstTeacherExperience_new.size()>0 && lstTeacherExperience_new.get(0)!=null)
													{
														sb.append("<tr>");
														sb.append("<th class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblCorpsYear+":</th>");
														if(lstTeacherExperience_new.get(0).getCorpsYear()!=null)
															sb.append("<th class='divlableval' style='"+strCss_value+"'>&nbsp;"+lstTeacherExperience_new.get(0).getCorpsYear()+"</th>");
														else
															sb.append("<th class='divlableval' style='"+strCss_value+"'>&nbsp;N/A</th>");
														sb.append("</tr>");
														
														sb.append("<tr>");
														sb.append("<th class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblTFARegion+":</th>");
														if(lstTeacherExperience_new.get(0).getTfaRegionMaster()!=null)
															sb.append("<th class='divlableval' style='"+strCss_value+"'>&nbsp;"+lstTeacherExperience_new.get(0).getTfaRegionMaster().getTfaRegionName()+"</th>");
														else
															sb.append("<th class='divlableval' style='"+strCss_value+"'>&nbsp;N/A</th>");
														sb.append("</tr>");
													}
													else
													{
														sb.append("<tr>");
														sb.append("<th class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblCorpsYear+":</th>");
														sb.append("<th class='divlableval' style='"+strCss_value+"'>&nbsp;N/A</th>");
														sb.append("</tr>");
														
														sb.append("<tr>");
														sb.append("<th class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblTFARegion+":</th>");
														sb.append("<th class='divlableval' style='"+strCss_value+"'>&nbsp;N/A</th>");
														sb.append("</tr>");
													}
												}
												//System.out.println("<--CorpsYear--> "+lstTeacherExperience_new.get(0).getCorpsYear());
												//TestTool.getTraceTimeTPAP("TPAP 209");
												//Teaching Years
												if(teachingOfYear){
													sb.append("<tr>");
													sb.append("<th class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblTeachingYears+":</th>");
													if(cgService.getTEYear(lstTeacherExperience_new,teacherDetail)!=0)
														sb.append("<th  class='divlableval' style='"+strCss_value+"'>&nbsp;"+cgService.getTEYear(lstTeacherExperience_new,teacherDetail)+"</th>");
													else
														sb.append("<th  class='divlableval' style='"+strCss_value+"'>&nbsp;N/A</th>");
													sb.append("</tr>");
												}
												//Expected Salary
												if(expectedSalary){
													sb.append("<tr>");
													sb.append("<th class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblExpectedSalary+":</th>");
													if(cgService.getSalary(lstTeacherPersonalInfo_new,teacherDetail)!=-1){
															sb.append("<th  class='divlableval' style='"+strCss_value+"'>&nbsp;$"+cgService.getSalary(lstTeacherPersonalInfo_new,teacherDetail)+"</th>");
													}else
														sb.append("<th  class='divlableval' style='"+strCss_value+"'>&nbsp;N/A</th>");
													sb.append("</tr>");
												}
												
												//# of Views:
												sb.append("<tr>");									
												Criterion criterion_teacherDetail = Restrictions.eq("teacherDetail",teacherDetail);
												
												int iTeacherProfileVisitHistoryCount=0;
												sb.append("<th class='divlable1' style='"+strCss_lbl+"'>&nbsp;"+lblOfViews+":</th>");
												sb.append("<th  class='divlableval1' style='"+strCss_lbl+"'><a data-original-title='"+lblClickToViewTPVisit+"' style='pointer-events:none;' class='nOofViews"+teacherDetail.getTeacherId()+"' rel='tooltip' id='tpTeacherProfileVisitHistory' href='javascript:void(0);' onclick=\"getTeacherProfileVisitHistoryShow_FTime('"+teacherDetail.getTeacherId()+"');\" ><span class='divlablelinkval1'>&nbsp;"+iTeacherProfileVisitHistoryCount+"</span></a></th>");
												sb.append("</tr>");
												
												//Job Applied View
												sb.append("<tr>");
												sb.append("<th class='divlable1' style='"+strCss_lbl+"'>&nbsp;"+lblJobApplied+":</th>");
												
												sb.append("<th class='divlableval1' style='"+strCss_lbl+"' id='jobListByTProfile"+teacherDetail.getTeacherId()+"'>0/0</th>");
												/*if(jobList!=0){
													sb.append("<a data-original-title='"+lblClickToViewJO+"' rel='tooltip' id='teacherProfileTooltip' href='javascript:void(0);' onclick=\"getJobList('"+teacherDetail.getTeacherId()+"')\" ><span class='divlablelinkval1'>&nbsp;"+jobList+"</span></a>");
												}else{
													sb.append("&nbsp;0");
												}
												sb.append("/"+(jobListForTm-jobList)+"</th>");*/
												
												sb.append("</tr>");
												
												//Willing to Substitute
												sb.append("<tr>");
												sb.append("<th class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblWillingToSubsti+":</th>");
												if(lstTeacherExperience_new!=null && lstTeacherExperience_new.size()>0)
												{
													System.out.println("teacherdetail 111111111ggggggggggggg   "+teacherDetail.getTeacherId());
													if(lstTeacherExperience_new.get(0).getCanServeAsSubTeacher()==2)
														sb.append("<th  class='divlableval' style='"+strCss_value+"'>&nbsp;N/A</th>");
													if(lstTeacherExperience_new.get(0).getCanServeAsSubTeacher()==0)
														sb.append("<th  class='divlableval' style='"+strCss_value+"'>&nbsp;N</th>");
													if(lstTeacherExperience_new.get(0).getCanServeAsSubTeacher()==1)
														sb.append("<th  class='divlableval' style='"+strCss_value+"'>&nbsp;Y</th>");
												}
												else
												{
													sb.append("<th  class='divlableval' style='"+strCss_value+"'>&nbsp;N/A</th>");
												}
												sb.append("</tr>");
												
												//KSN ID
												if( (userMaster.getEntityType()==6 && userMaster.getRoleId().getRoleId()==11) || (userMaster.getEntityType()==5 && userMaster.getRoleId().getRoleId()==10)){sb.append("<tr>");
												sb.append("<td class='divlable' style='"+strCss_lbl+"'>&nbsp;"+lblKSNID+":</td>");
												MQEvent mqEvent = null;
												try{
													mqEvent = mqEventDAO.findLinkToKSNIniatedByTeacher(teacherDetail);
												}catch(Exception ex){
													ex.printStackTrace();
												}
												sb.append("<td  class='divlableval' style='"+strCss_value+"'>");
												sb.append("<div id='ksndetails'>");
												sb.append("<input type='hidden' id='disconnectTalentId' name='disconnectTalentId' value='"+teacherDetail.getTeacherId()+"' />");
												if(mqEvent!=null && teacherDetail.getKSNID()==null){
													if(mqEvent.getStatus()!=null && mqEvent.getStatus().equalsIgnoreCase("R")){
														if(mqEvent.getAckStatus()!=null && mqEvent.getAckStatus().contains("Success")){
															sb.append("Link to KSN is in progress");
														}
														else if(mqEvent.getAckStatus()!=null && mqEvent.getAckStatus().contains("Fail")){
															sb.append("<table><tr>");
															sb.append("<td  class='divlableval' style='"+strCss_value+"'>");
															//sb.append("<input type='hidden' id='disconnectTalentId' name='disconnectTalentId' value='"+teacherDetail.getTeacherId()+"' />");
															sb.append("<input type='text' id='inputKSN' class='form-control' value='' style='height:30px;' maxlength=10 onkeypress='return checkForInt(event);'/>");
															sb.append("</td>");
															sb.append("<td>");
															sb.append("<a data-original-title='Link to KSN' rel='tooltip' id='tpDisconnect' class='tpDisconnect' style='padding-left:8px;' href='javascript:void(0);' onclick='disconnectTalent();'><i class='icon-unlock icon-large'></i></a>");
															sb.append("</td>");
															sb.append("<script>$('#tpDisconnect').tooltip();</script>");
															sb.append("</tr><table>");
														}
														else{
															sb.append("Link to KSN is in progress");
														}
													}
												}
												else if(teacherDetail.getKSNID()==null){
													sb.append("<table><tr><td>");
													//sb.append("<input type='hidden' id='disconnectTalentId' name='disconnectTalentId' value='"+teacherDetail.getTeacherId()+"' />");
													sb.append("<input type='text' id='inputKSN' class='form-control' value='' style='height:30px;' maxlength=10 onkeypress='return checkForInt(event);'/>");
													sb.append("</td>");
													sb.append("<td>");
													sb.append("<a data-original-title='Link to KSN' rel='tooltip' id='tpDisconnect' class='tpDisconnect' style='padding-left:8px;' href='javascript:void(0);' onclick='disconnectTalent();'><i class='icon-unlock icon-large'></i></a>");
													sb.append("</td>");
													sb.append("<script>$('#tpDisconnect').tooltip();</script>");
													sb.append("</tr></table>");
												}
												else if(teacherDetail.getKSNID()!=null){
													sb.append("<table><tr>");
													sb.append("<td>"+teacherDetail.getKSNID()+"</td>");
													sb.append("<td>");
													sb.append("<a data-original-title='Linked to KSN' rel='tooltip' id='tpDisconnect' style='padding-left:8px;' href='javascript:void(0);'><i class='icon-lock icon-large'></i></a>");
													sb.append("</td>");
													sb.append("<script>$('#tpDisconnect').tooltip();</script>");
													sb.append("</tr></table>");
												}
												sb.append("</div>");
												sb.append("</td>");
												sb.append("</tr>");
												}
												
												//sssnnnnnnnnnn in applicant pool
												if(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==NC_HEADQUARTER){
												sb.append("<tr>");
												if(userMaster.getEntityType()!=3)
												sb.append("<th class='divlable' style='"+strCss_lbl+"'>&nbsp;Last 4 of SSN:</th>");
												
												if(teacherPersonalInfo!=null)
												if(teacherPersonalInfo.getSSN()!=null && !teacherPersonalInfo.getSSN().equals("")){
													String ssnFour=Utility.decodeBase64(teacherPersonalInfo.getSSN());
													String finalSSN="";
													try
													{
													 finalSSN=ssnFour.substring(5, 9);
													System.out.println("Last four digit of ssn:::"+finalSSN);
													}catch(Exception e){}
														sb.append("<th  class='divlableval' style='"+strCss_value+"'>&nbsp;"+finalSSN+"</th>");
													}else{
														sb.append("<th  class='divlableval' style='"+strCss_value+"'>&nbsp;N/A</th>");
													}
												
												sb.append("</tr>");
												}
												
												
											sb.append("</table>");
										sb.append("</th>");
										
									sb.append("</tr>");
								sb.append("</table>");
								
								
								
							sb.append("</th>");
						sb.append("</tr>");
						
						// start new row
						
						sb.append("<tr>");
							sb.append("<th>");
								sb.append("<table border='0' cellspacing=0 cellpadding=0>");
								sb.append("<tr>");
									// Blank Cell
									sb.append("<th width=50>&nbsp;</th>");
									
									strCss_lbl="padding:4px;vertical-align:middle;";
									strCss_value="padding:2px;text-align:left;vertical-align:text-top;";
									
									String Resume_column_width="";
									String Phone_column_width="";
									String PDReport_column_width="";
									
									if(sVisitLocation.equalsIgnoreCase("Teacher Pool")||sVisitLocation.equalsIgnoreCase("PNR Dashboard"))
									{
										sb.append("<th style='text-align:left;'>");//OK
										Resume_column_width=";width:44px;";
										Phone_column_width=";width:46px;";
										PDReport_column_width=";width:340px;";
									}
									else if(sVisitLocation.equalsIgnoreCase("Mosaic"))
									{
										sb.append("<th style='text-align:left;'>"); //OK
									}
									else if(sVisitLocation.equalsIgnoreCase("My Folders"))
									{
										sb.append("<th style='text-align:left;'>"); //)OK
									}
									
									
									sb.append("<table border='0' cellspacing=0 cellpadding=0");
									
									// Resume
									sb.append("<tr>");
									
									sb.append("<th class='divlable' style='"+strCss_lbl+""+Resume_column_width+"'>&nbsp;&nbsp;&nbsp;"+lnkResume+":</th>");
									if(teacherExperience!=null && teacherExperience.getResume()!=null && teacherExperience.getResume()!=""){
										sb.append("<th  class='divlableval' style='"+strCss_value+"'>&nbsp;<a data-original-title='"+lnkResume+"' rel='tooltip' class='newTpRessime' id='tpResumeprofile"+teacherDetail.getTeacherId()+"' href='javascript:void(0);' onclick=\"downloadResume('"+teacherDetail.getTeacherId()+"','tpResumeprofile');"+windowFunc+"\" ><span class='icon-briefcase icon-large iconcolor'></span></a></th>");									
									}else
										sb.append("<th  class='divlableval' style='"+strCss_value+"'>&nbsp;<a data-original-title='"+lblNotAvailable+"' rel='tooltip' id='tpResumeprofile'><span class='icon-briefcase icon-large iconcolorhover'></span></a></th>");
					
									if(sVisitLocation.equalsIgnoreCase("Teacher Pool")||sVisitLocation.equalsIgnoreCase("PNR Dashboard"))
										strCss_lbl="padding:2px;padding-left:118px;vertical-align:middle;text-align:right;";//OK // @AShish :: change padding-left from 200 to 110
									else if(sVisitLocation.equalsIgnoreCase("Mosaic"))
										strCss_lbl="padding:2px;padding-left:118px;vertical-align:middle;text-align:right;"; //OK // @Ashish :: change padding-left from 200 to 110
									else if(sVisitLocation.equalsIgnoreCase("My Folders"))
										strCss_lbl="padding:2px;padding-left:190px;vertical-align:middle;text-align:right;"; // OK
									
									TestTool.getTraceSecond("Assessment Start");
									/*Map<Integer,TeacherAssessmentStatus> baseTakenMap = new HashMap<Integer, TeacherAssessmentStatus>();*/
									List<TeacherAssessmentStatus> teacherAssessmentStatus = null;
									Map<Integer, Integer> isbaseComplete	=	new HashMap<Integer, Integer>();
									
									Integer isBase = null;
									try{/*
										if(lstTeacherDetails!=null && lstTeacherDetails.size()>0){
											teacherAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentTakenByTeachers(lstTeacherDetails);
											for (TeacherAssessmentStatus teacherAssessmentStatus2 : teacherAssessmentStatus) {					
												//baseTakenMap.put(teacherAssessmentStatus2.getTeacherDetail().getTeacherId(), teacherAssessmentStatus2);
												boolean cgflag = teacherAssessmentStatus2.getCgUpdated()==null?false:teacherAssessmentStatus2.getCgUpdated();
												if(cgflag && teacherAssessmentStatus2.getStatusMaster().getStatusShortName().equalsIgnoreCase("comp"))
													isbaseComplete.put(teacherAssessmentStatus2.getTeacherDetail().getTeacherId(), 1);
												else
													if(teacherAssessmentStatus2.getStatusMaster().getStatusShortName().equalsIgnoreCase("vlt"))
														isbaseComplete.put(teacherAssessmentStatus2.getTeacherDetail().getTeacherId(), 2);
											}
										}
									*/}catch(Exception e){
										e.printStackTrace();
									}
									TestTool.getTraceSecond("Assessment End");
									isBase = isbaseComplete.get(teacherDetail.getTeacherId());
									sb.append("<th class='divlable' style='"+strCss_lbl+""+PDReport_column_width+"'>&nbsp;"+headPdRep+": </th>");
									//if(isBase!=null && isBase==1)else
									sb.append("<th  class='divlableval hide' id='baseFirst"+teacherDetail.getTeacherId()+"' style='"+strCss_value+"'>&nbsp;<a data-original-title='"+headPdRep+"' rel='tooltip' id='tpPDReportprofile' href='javascript:void(0);' onclick=\"generatePDReport('"+teacherDetail.getTeacherId()+"','tpPDReportprofile');"+windowFunc+"\"><span class='icon-book icon-large iconcolor'></span></a></th>");
									sb.append("<th  class='divlableval' id='baseSecond"+teacherDetail.getTeacherId()+"' style='"+strCss_value+"'>&nbsp;<a data-original-title='"+lblNotAvailable+"' rel='tooltip' id='tpPDReportprofile' ><span class='icon-book icon-large iconcolorhover'></span></a></th>");
									
									sb.append("</tr>");										
									sb.append("</table>");
									
								sb.append("</th>");
								sb.append("</tr>");
								sb.append("</table>");
							
							sb.append("</th>");
						sb.append("</tr>");
						// End new row
						
						
						sb.append("<tr>");
						sb.append("<th>");
							sb.append("<table border='0' cellspacing=0 cellpadding=0 style='margin-top:-10px;'>");
							sb.append("<tr>");
								// Blank Cell
								sb.append("<th width=50>&nbsp;</th>");
								
								strCss_lbl="padding:10px;vertical-align:middle;padding-right:70px;";
								if(sVisitLocation.equalsIgnoreCase("Teacher Pool") ||sVisitLocation.equalsIgnoreCase("PNR Dashboard"))
								{
									sb.append("<th style='text-align:left;'>");//OK
									Phone_column_width=";width:189px;";
									PDReport_column_width=";width:100px;";
								}
								else if(sVisitLocation.equalsIgnoreCase("Mosaic"))
								{
									sb.append("<th style='text-align:left;'>"); //OK
								}
								else if(sVisitLocation.equalsIgnoreCase("My Folders"))
								{
									sb.append("<th style='text-align:left;'>"); //)OK
								}
								
								
								sb.append("<table border='0' cellspacing=0 cellpadding=0");
								
								// Resume
								sb.append("<tr>");

								
								//Phone:MyFolder
								//TestTool.getTraceTimeTPAP("TPAP 211");
								List<TeacherPersonalInfo> lstTeacherPersonalInfo1	  =	 new ArrayList<TeacherPersonalInfo>();
								if(lstTeacherPersonalInfo_new!=null && lstTeacherPersonalInfo_new.size()>0)
								lstTeacherPersonalInfo1.add(lstTeacherPersonalInfo_new.get(0));
								if(sVisitLocation.equalsIgnoreCase("Teacher Pool") ||sVisitLocation.equalsIgnoreCase("PNR Dashboard"))
								{
									strCss_value="padding:10px;text-align:left;vertical-align:text-top;padding-right:73px;";
									sb.append("<th class='divlable' style='"+strCss_lbl+""+Phone_column_width+"'>&nbsp;"+lblPhoneNo+":</th>");
									if(lstTeacherPersonalInfo1.size()==1)
									{
										if((lstTeacherPersonalInfo1.get(0).getPhoneNumber()!=null && !lstTeacherPersonalInfo1.get(0).getPhoneNumber().equalsIgnoreCase("")))
											sb.append("<th  class='divlableval' style='"+strCss_value+";margin-top:-5px;padding-left:12px;'>"+lstTeacherPersonalInfo1.get(0).getPhoneNumber()+"</th>");	
										else
											sb.append("<th  class='divlableval' style='"+strCss_value+";margin-top:-5px;padding-left:5px;'><span class='span2'>&nbsp;N/A</th>");
									}
									else
										sb.append("<th  class='divlableval' style='"+strCss_value+";margin-top:-5px;padding-left:5px;'>&nbsp;N/A</th>");
									
									// PD Report
									
									sb.append("<th class='divlable' style='"+strCss_lbl+""+PDReport_column_width+"'>&nbsp;"+lblMobileNumber+": </th>");
									if((lstTeacherPersonalInfo1.size()==1 && lstTeacherPersonalInfo1.get(0).getMobileNumber()!=null && !lstTeacherPersonalInfo1.get(0).getMobileNumber().equalsIgnoreCase("")))
										sb.append("<th  class='divlableval' style='"+strCss_value+";margin-top:-5px;padding-left:30px;'>"+lstTeacherPersonalInfo1.get(0).getMobileNumber()+"</th>");
									else
										sb.append("<th  class='divlableval' style='"+strCss_value+";margin-top:-5px;padding-left:30px;'>&nbsp;N/A</th>");
									
									sb.append("</tr>");
									
								}else if(sVisitLocation.equalsIgnoreCase("My Folders"))
								{
									strCss_value="padding:10px;text-align:left;vertical-align:text-top;";
									sb.append("<th class='divlable' style='"+strCss_lbl+""+Phone_column_width+"'>&nbsp;"+lblPhoneNo+": </th>");
										if(lstTeacherPersonalInfo1.size()==1)
										{
											if((lstTeacherPersonalInfo1.get(0).getPhoneNumber()!=null && !lstTeacherPersonalInfo1.get(0).getPhoneNumber().equalsIgnoreCase("")))
												sb.append("<th  class='divlableval' style='"+strCss_value+";margin-top:-5px;padding-left:12px;'><span class='span2'>"+lstTeacherPersonalInfo1.get(0).getPhoneNumber()+"</span></th>");	
											else
												sb.append("<th  class='divlableval' style='"+strCss_value+";margin-top:-5px;padding-left:5px;'><span class='span2'>&nbsp;N/A</span></th>");
										}
										else
											sb.append("<th  class='divlableval' style='"+strCss_value+";margin-top:-5px;padding-left:5px;'><span class='span2'>&nbsp;N/A</span></th>");
										
										// PD Report
										
										sb.append("<th class='divlable' style='"+strCss_lbl+""+PDReport_column_width+"'>&nbsp;"+lblMobileNumber+": </th>");
										if((lstTeacherPersonalInfo1.size()==1 && lstTeacherPersonalInfo1.get(0).getMobileNumber()!=null && !lstTeacherPersonalInfo1.get(0).getMobileNumber().equalsIgnoreCase("")))
											sb.append("<th  class='divlableval' style='"+strCss_value+";padding-right:70px;margin-top:-5px;padding-left:5px;'><span style='width:40px;'>"+lstTeacherPersonalInfo1.get(0).getMobileNumber()+"</span></th>");
										else
											sb.append("<th  class='divlableval' style='"+strCss_value+";padding-right:70px;margin-top:-5px;padding-left:30px;'><span style='width:40px;'>&nbsp;N/A</span></th>");
										
										sb.append("</tr>");
								}else if(sVisitLocation.equalsIgnoreCase("Mosaic"))
								{
									strCss_value="padding:10px;text-align:left;vertical-align:text-top;";
									sb.append("<th class='divlable' style='"+strCss_lbl+""+Phone_column_width+"'>&nbsp;Phone Number: </th>");
										if(lstTeacherPersonalInfo1.size()==1)
										{
											if((lstTeacherPersonalInfo1.get(0).getPhoneNumber()!=null && !lstTeacherPersonalInfo1.get(0).getPhoneNumber().equalsIgnoreCase("")))
												sb.append("<th  class='divlableval' style='"+strCss_value+";margin-top:-5px;padding-left:12px;'><span class='span2'>"+lstTeacherPersonalInfo1.get(0).getPhoneNumber()+"</span></th>");	
											else
												sb.append("<th  class='divlableval' style='"+strCss_value+";margin-top:-5px;padding-left:5px;'><span class='span2'>&nbsp;N/A</span></th>");
										}
										else
											sb.append("<th  class='divlableval' style='"+strCss_value+";margin-top:-5px;padding-left:5px;'><span class='span2'>&nbsp;N/A</span></th>");
										
										// PD Report
										
										sb.append("<th class='divlable' style='"+strCss_lbl+""+PDReport_column_width+"'>&nbsp;"+lblMobileNumber+": </th>");
										if((lstTeacherPersonalInfo1.size()==1 &&  lstTeacherPersonalInfo1.get(0).getMobileNumber()!=null && !lstTeacherPersonalInfo1.get(0).getMobileNumber().equalsIgnoreCase("")))
											sb.append("<th  class='divlableval' style='"+strCss_value+";padding-right:70px;margin-top:-5px;padding-left:5px;'><span style='width:40px;'>"+lstTeacherPersonalInfo1.get(0).getMobileNumber()+"</span></th>");
										else
											sb.append("<th  class='divlableval' style='"+strCss_value+";padding-right:70px;margin-top:-5px;padding-left:30px;'><span style='width:40px;'>&nbsp;N/A</span></th>");
										
										sb.append("</tr>");
								}
								
								if(sVisitLocation.equalsIgnoreCase("Teacher Pool") && userMaster.getDistrictId()!=null && (userMaster.getDistrictId().getDistrictId().equals(7800049) || userMaster.getDistrictId().getDistrictId().equals(7800048) || userMaster.getDistrictId().getDistrictId().equals(7800050) || userMaster.getDistrictId().getDistrictId().equals(7800051) || userMaster.getDistrictId().getDistrictId().equals(7800053))){
								sb.append("<tr>");
								OctDetails octDetails = getOctDetails(teacherDetail);
								String octNumber ="";
								String octFileUpload ="";
								if(octDetails!=null && octDetails.getOctNumber()!=null){
									octFileUpload=octDetails.getOctFileName();
								}
								if(octDetails!=null && octDetails.getOctNumber()!=null){
									octNumber="<a href='#' onclick=\"getOctDetails('"+octDetails.getOctNumber()+"')\">"+octDetails.getOctNumber()+"</a>";
								}else{
									octNumber="N/A";
								}
									sb.append("<th class='divlable' style='"+strCss_lbl+""+Phone_column_width+"'>&nbsp;"+msgOCTRegistrationNum+": </th>");
									sb.append("<th  class='divlableval' style='"+strCss_value+";margin-top:-5px;padding-left:5px;'><span class='span2'>&nbsp;"+octNumber+"</span></th>");
									//sb.append("<th class='divlable' style='"+strCss_lbl+""+PDReport_column_width+"'>&nbsp;OCT&nbsp;Card: </th>");
									sb.append("<th class='divlable' style='"+strCss_lbl+""+PDReport_column_width+"'>&nbsp;"+Utility.getLocaleValuePropByKey("lbloctcard", locale)+": </th>");
									sb.append("<th  class='divlableval' style='"+strCss_value+";padding-right:70px;margin-top:-5px;padding-left:30px;'><span style='width:40px;'>&nbsp;");
									if(octFileUpload!=""){
										sb.append("<a data-original-title='"+Utility.getLocaleValuePropByKey("lbloctcard", locale)+"' rel='tooltip' id='hrefOctUpload' href='javascript:void(0);' onclick=\"downloadOctUpload('"+octDetails.getTeacherId().getTeacherId()+"');"+windowFunc+"\">View</a>");
									}else{
										sb.append("N/A");							
										}
									sb.append("</span></th>");						
									sb.append("</tr>");
									sb.append("<tr>");
									sb.append("<th class='divlable' style='"+strCss_lbl+""+Phone_column_width+"padding:0px;'>&nbsp;"+lblComments1+":</th>");
									sb.append("</tr>");
									sb.append("<tr>");
									sb.append("<td colspan='3' class='divlable' style='"+strCss_lbl+""+Phone_column_width+"'><textarea>");
									if(octDetails!=null && octDetails.getOctText()!=null){
										sb.append(octDetails.getOctText());
									}
								sb.append("</textarea></td>");
								sb.append("</tr>");
								}
								sb.append("</table>");
								
							sb.append("</th>");
							sb.append("</tr>");
							sb.append("</table>");
						
						sb.append("</th>");
					sb.append("</tr>");
						
						/////////////////
						
					//System.out.println("<--Phone--> "+lstTeacherPersonalInfo1.get(0).getPhoneNumber()+"<--Mobile--> "+lstTeacherPersonalInfo1.get(0).getMobileNumber());	
						// start new row
					TestTool.getTraceTimeTPAP("AchievementScor start");
						// Calculating and Get valles of "Achievement Score","Academic Achievement" and "Leadership/Results"
						String strAScore="N/A";
						String strLRScore="N/A";
						
						TestTool.getTraceTimeTPAP("AchievementScor end");
						strCss_lbl="padding-left:10px;";
						strCss_value="padding-left:105px;";
						if((sVisitLocation.equalsIgnoreCase("Mosaic") && entityID==1) || entityID==2 || entityID==3)
						{
							if(entityID==3)
								strCss_lbl="padding-left:51px;";
							
							sb.append("<tr>");
								sb.append("<td valign='top'>");
									sb.append("<table border='0' width=750 cellspacing=0 cellpadding=0 class='tablecgtbl'>");
									if(achievementScore)
									{
										if(sVisitLocation.equalsIgnoreCase("Teacher Pool") ||sVisitLocation.equalsIgnoreCase("PNR Dashboard"))
											sb.append("<tr><td style='width:30px;'>&nbsp;</td>");
										else
											sb.append("<tr><td style='width:50px;'>&nbsp;</td>");
											sb.append("<td valign='top'>" +
												"<table border='0' cellspacing=0 cellpadding=0 class='tablecgtbl'>" +
												"<TR>");
													// A Score and L/R Score
													if(sVisitLocation.equalsIgnoreCase("Teacher Pool") ||sVisitLocation.equalsIgnoreCase("PNR Dashboard")){
														strCss_lbl="padding:0px;padding-left:0px;width:55px;"; // Change padding-left 20 to 0 
														strCss_value="padding:0px;padding-left:0px;width:50px;";
													}
													else
													{
														strCss_lbl="padding:0px;padding-left:4px;width:55px;";
														strCss_value="padding:0px;padding-left:5px;width:50px;";
													}
													sb.append("<td class='divlable' style='"+strCss_lbl+"'>&nbsp;&nbsp;&nbsp;"+lblAScore+":</td>");
													sb.append("<td class='divlableval' style='"+strCss_value+";vertical-align:top;' id='ascore_profile'>&nbsp;"+strAScore+"</td>");

													sb.append("<td style='width:154px;'>&nbsp;</td>");
													if(sVisitLocation.equalsIgnoreCase("Teacher Pool") ||sVisitLocation.equalsIgnoreCase("PNR Dashboard")|| sVisitLocation.equalsIgnoreCase("My Folders") ){
														strCss_lbl="padding:0px;padding-left:0px;width:60px;"; // Change padding-left 20 to 0 
													}else{
														strCss_lbl="padding:0px;padding-left:2px;width:60px;";
													}
													sb.append("<td class='divlable' style='"+strCss_lbl+"'>&nbsp;&nbsp;&nbsp;"+lblLRScore+":</td>");
													sb.append("<td class='divlableval' style='"+strCss_value+";vertical-align:top;' id='lrscore_profile'>&nbsp;"+strLRScore+"</td>");
													sb.append("</TR>"+"</table>"+"</td></tr>");
													
													/* @Start
													 * @Ashish Kumar
													 * @Description :: District Admin can change achievement score.
													 * */
													if( (sVisitLocation.equalsIgnoreCase("Teacher Pool") || sVisitLocation.equalsIgnoreCase("PNR Dashboard")) && entityID==2){
														//System.out.println("******************2*******************");
														sb.append("<tr>");
														strCss_value="padding-left:55px;";
														// Blank Cell
														sb.append("<td style='width:55px;'>&nbsp;</td>");
														
														strCss_lbl="padding:2px;padding-left:4px;padding-top:5px;";
														sb.append("<td>"); 
														
														

														if(achievementScore && entityID==2)
														{
															sb.append("<table border='0' width=200 cellspacing=0 cellpadding=0 class='tablecgtbl'>");
															//1st Row for Label of "Achievement Score","Academic Achievement" and "Leadership/Results"
															sb.append("<tr>");
															//Academic Achievement:
																sb.append("<td class='divlable' style='"+strCss_lbl+"'>&nbsp;&nbsp;Academic&nbsp;Achievement:</td>");
															// Leadership/Results
																sb.append("<td class='divlable' style='"+strCss_lbl+"'>&nbsp;&nbsp;"+lblLeadershipResults+":</td>");
																
																sb.append("<td style='width:100px;'>&nbsp;</td>");
															sb.append("</tr>");
															
															strCss_lbl="padding:2px;padding-left:0px;padding-top:-20px;";
															//2nd Row for values of "Achievement Score","Academic Achievement" and "Leadership/Results"
															String strSliderCss_Iframe="border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:280px;";
															sb.append("<tr>");
																
															sb.append("<td style='"+strCss_lbl+"'>&nbsp;");
															sb.append("<div style='margin-left: 0px; margin-top: -18px;'><label id='achievementSlider'></label></div>");
																
															sb.append("<input type='hidden' name='txt_sl_slider_aca_ach' id='txt_sl_slider_aca_ach' value='0'>");
															sb.append("</td>");
															
															
															// Leadership/Results
															sb.append("<td style='"+strCss_lbl+"'>&nbsp;");
															
															sb.append("<div style='margin-left: 0px; margin-top: -18px;'><label id='leadershipSlider'></label>" +
																		"</div>");
															sb.append("<input type='hidden' name='txt_sl_slider_lea_res' id='txt_sl_slider_lea_res' value='0'>");
															//sb.append("<a href='javascript:void(0);' onclick=\"javascript:updateAScore("+teacherId+","+jobOrder.getDistrictMaster().getDistrictId()+")\"'>Go</A>");
															sb.append("</td>");
															
															//sb.append("<td style='width:20px;vertical-align:middle;'>&nbsp;<a href='javascript:void(0);' onclick=\"javascript:updateAScore("+teacherId+","+jobOrder.getDistrictMaster().getDistrictId()+")\"'>Go</A></td>");
															//sb.append("<td style='width:20px;vertical-align:middle;'>&nbsp;<a href='javascript:void(0);' onclick=\"javascript:updateAScore("+teacherId+","+jobOrder.getDistrictMaster().getDistrictId()+")\"'>Go</A></td>");
															//sb.append("<td style='width:40px;vertical-align:middle;'>&nbsp;<button class='btn btn-primary' type='button' onclick=\"javascript:updateAScore("+teacherId+","+jobOrder.getDistrictMaster().getDistrictId()+")\">Set A Score <i class='icon'></i></button></td>");
															
															//System.out.println(" Testing :: userMaster.getDistrictId() :: "+userMaster.getDistrictId().getDistrictId());
															
															sb.append("<td style='width:170px;vertical-align:middle;'>&nbsp;<button class='btn btn-primary' style='margin-top:-20px; width:106px;' type='button' onclick='javascript:updateAScoreFromTeacherPool("+teacherDetail.getTeacherId()+","+userMaster.getDistrictId().getDistrictId()+")'>Set A Score</td>");
															
															sb.append("</tr>");
															
															sb.append("</table>");
														}
														
													sb.append("</td>");
													sb.append("</tr>");
													}
													/* @End
													 * @Ashish Kumar
													 * @Description :: District Admin can change achievement score.
													 * */
									}
									sb.append("</table>");
								sb.append("</td>");
							sb.append("</tr>");
						
						}if(pnqDis){
							TestTool.getTraceTimeTPAP("panq start");
							sb.append("<tr>");
							sb.append("<td style='padding:0px 0px 0px 53px'>");
						
							sb.append("<table border='0' cellspacing=0 cellpadding=0 class='tablecgtbl'>");
							sb.append("<tr>");				
							sb.append("<td class='divlable' style='padding:2px;padding-left:8px;padding-top:20px;'>&nbsp;PNQ:</td>");				
							String pNQVal = getPNQValue(lstTeacherExperience_new,teacherDetail);
							String New_pNQVal = "";
							sb.append("<th >&nbsp;"+pNQVal+"</th>");				
							sb.append("</td>");
							sb.append("</tr>");
							sb.append("</table>");
							
							sb.append("</td>");
							sb.append("</tr>");
							TestTool.getTraceTimeTPAP("panq start");
						}
					sb.append("</table>");

					sb.append("<div id='removeWait' style='text-align:center;padding-top:0px;'><img src='images/loadingAnimation.gif' /> <br>"+lblLoding+"</div>");

					sb.append("</div>");
					sb.append("<DIV>");
					sb.append("<table border=0 width='100%'><tr bgcolor='#F2FAEF' class='custom-div-border'><th style='text-align:center;vertical-align:middle;padding-top:10px;padding-bottom:10px;'width='35%'><a href='javascript:void(0);' onclick=\"getCommunicationsDiv(1,0,'"+teacherDetail.getTeacherId()+"',0);\" ><span class='icon-dropbox icon-large iconcolor'></span>&nbsp;"+headCom+"</a></th>");
					if(userMaster.getEntityType()==1)
					{
						sb.append("<th style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><span class='icon-save icon-large iconcolorhover'></span>&nbsp;"+btnSave+"</th>" +
								"<th style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><span class='icon-share icon-large iconcolorhover'></span>&nbsp;"+btnShare+"</th>"+
								"<th style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><span class='icon-tag icon-large iconcolorhover'></span></span>&nbsp;"+headTag+"</th>");
					}
					else
					{
						
						int flagpopover=1;
						sb.append("<th style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><a href='javascript:void(0);' id='saveaction"+teacherAndChk.get(teacherDetail.getTeacherId())+"' onclick=\"saveToFolderJFTNULL('0','"+teacherDetail.getTeacherId()+"',"+flagpopover+")\"><span class='icon-save icon-large iconcolor'></span>&nbsp;"+btnSave+"</a></th>" +
								"<th style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><a href='javascript:void(0);' id='shareaction"+teacherAndChk.get(teacherDetail.getTeacherId())+"' onclick=\"displayUsergrid('"+teacherDetail.getTeacherId()+"',"+flagpopover+")\"><span class='icon-share icon-large iconcolor'></span>&nbsp;"+btnShare+"</a></th>");
						//sb.append("<th style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><span class='icon-tag icon-large iconcolor'></span></th>");
						
						//sb.append("<th style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><a href='javascript:void(0);' id='tagaction' onclick=\"closeProfileOpenTags("+noOfRecordCheck+")\"> <span class='icon-tag icon-large iconcolor'></span></span>&nbsp;Tag</a></th>");
						
						if(userMaster.getRoleId().getRoleId()==1 || (userMaster.getRoleId().getRoleId()==3 && (userMaster.getDistrictId().getIsDistrictWideTags()==null || !userMaster.getDistrictId().getIsDistrictWideTags()))){
							sb.append("<th style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><span class='icon-tag icon-large iconcolorhover'></span></span>&nbsp;"+headTag+"</th>");
						}
						else{
							if(sVisitLocation.equalsIgnoreCase("Teacher Pool") ||sVisitLocation.equalsIgnoreCase("PNR Dashboard") ||sVisitLocation.equalsIgnoreCase("My Folders"))
								sb.append("<th style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><a href='javascript:void(0);' id='tagaction' onclick=\"closeProfileOpenTags("+teacherDetail.getTeacherId()+","+teacherAndChk.get(teacherDetail.getTeacherId())+")\"> <span class='icon-tag icon-large iconcolor'></span></span>&nbsp;"+headTag+"</a></th>");
							else
								sb.append("<th style='text-align:center;vertical-align:middle;border-left: 1px solid #CCCCCC;padding-top:10px;padding-bottom:10px;'><span class='icon-tag icon-large iconcolorhover'></span></span>&nbsp;"+headTag+"</th>");
						}
					}
					sb.append("</tr>");
					sb.append("</table>");
					sb.append("</DIV>");	
					/*sb.append("<div class='modal-footer'>"+
							"<button class='btn' onclick='showProfileContentClose();'>Close</button>"+ 		
						"</div>"+
					"</div>");*/
					sb.append("</div>");
					sb.append("</div>");
					sb.append("</div>");
					TestTool.getTraceSecond("end");
						/*====================================================*/
					}
					
					sb.append("<script type='text/javascript'>$('.tpDiscwwwonnect').tooltip();</script>");
					//for loop end
				}catch(Exception e){
					e.printStackTrace();
				}
				
			}catch (Exception e) {
				e.printStackTrace();
			}
			return sb.toString();
		}
		
		
		public String getTFAValueNew(List<TeacherExperience> teacherExperienceList,TeacherDetail teacherDetail,List<TFAAffiliateMaster>  tfaAffiliateMasterList)
		{
			StringBuilder tFA = new StringBuilder();
			
			System.out.println("  teacherExperienceList>>>>>>>>>>>>>>>>> "+teacherExperienceList.size());
			
			try 
			{
				//List<TFAAffiliateMaster> tfaAffiliateMasterList = tfaAffiliateMasterDao.findAll(); 
				tFA.append("<select id='tfaList' disabled='disabled'>");
				tFA.append("<option value='0'>Select</option>");
				
				if(teacherExperienceList!=null && teacherExperienceList.size()>0)
				{
					if(teacherExperienceList.get(0).getTfaAffiliateMaster()!=null )
					{
						tFA.append("<option value=''>N/A</option>");
						for(TFAAffiliateMaster tfalist:tfaAffiliateMasterList)
						{
								if(teacherExperienceList.get(0).getTfaAffiliateMaster().getTfaAffiliateId().equals(tfalist.getTfaAffiliateId()))
									tFA.append("<option id='"+tfalist.getTfaAffiliateId()+"' selected='selected' value='"+tfalist.getTfaAffiliateId()+"'>"+tfalist.getTfaAffiliateName()+"</option>");
								else
									tFA.append("<option id='"+tfalist.getTfaAffiliateId()+"' value='"+tfalist.getTfaAffiliateId()+"'>"+tfalist.getTfaAffiliateName()+"</option>");
						}
					}
					else
					{
						tFA.append("<option value='' selected='selected'>N/A</option>");
						for(TFAAffiliateMaster tfalist:tfaAffiliateMasterList)
						{
								tFA.append("<option id='"+tfalist.getTfaAffiliateId()+"' value='"+tfalist.getTfaAffiliateId()+"'>"+tfalist.getTfaAffiliateName()+"</option>");
						}
					}
					tFA.append("</select>");
				}else
				{
					return null;
				}
				
				
				
				} catch (Exception e) {
				e.printStackTrace();
			}
			return tFA.toString();
		}
		
	public String schoolprefrence()
	
	{
		return null;
		
	}
	
	public String getBackgroundCheckGrid_DivProfile(Integer teacherId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		StringBuffer sb = new StringBuffer();		
		try 
		{
			String locale = Utility.getValueOfPropByKey("locale");
			String lblNoRecord = Utility.getLocaleValuePropByKey("lblNoRecord", locale);
			String lblFileName = Utility.getLocaleValuePropByKey("lblFileName1", locale);
			String lblUploadedDate = Utility.getLocaleValuePropByKey("lblUploadedDate", locale);
			
			
			TeacherPersonalInfo teacherPersonalInfo=teacherPersonalInfoDAO.findById(teacherId, false, false);
			String tablePadding_Data="style='padding-left:10px;vertical-align:top;text-align:left;font-weight: normal;line-height:30px;'";
			String tablePadding="style='padding-left:5px;vertical-align:top;color:#FFFFFF;line-height:25px;'";
			sb.append("<table id='tbleBgCheck_Profile' border='0' class='table-bordered' onmouseover=\"javascript:setGridProfileVariable('document')\">");
			sb.append("<thead class='bg'>");
			sb.append("<tr>");				
			sb.append("<th align=\"left\" "+tablePadding+" >"+lblFileName+"</th>");
			sb.append("<th align=\"left\" "+tablePadding+" >File</th>");
			sb.append("<th align=\"left\" "+tablePadding+" >"+lblUploadedDate+"</th>");
			sb.append("</tr>");
			sb.append("</thead>");
			if(teacherPersonalInfo.getBackgroundCheckDocument()!=null && teacherPersonalInfo.getBackgroundCheckDocument()!="")
			{
				String windowFunc="if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
				String gridColor="class='bggrid'";
				sb.append("<tr "+gridColor+">");
				sb.append("<td "+tablePadding_Data+">");
				sb.append(teacherPersonalInfo.getBackgroundCheckDocument());
				sb.append("</td>");
				sb.append("<td>");
				sb.append("<a data-original-title='Background Check' rel='tooltip' id='tpBackgroundCheckprofile' href='javascript:void(0);' onclick=\"downloadBackgroundCheck('"+teacherId+"','tpBackgroundCheckprofile');"+windowFunc+"\" ><span class='icon-download-alt icon-large iconcolorBlue'></span></a>");
				sb.append("</td>");
				sb.append("<td>");
				sb.append(teacherPersonalInfo.getBgCheckUploadedDate());
				sb.append("</td>");
				sb.append("</tr>");
			}
			else
			{
				sb.append("<tr><td "+tablePadding_Data+" colspan='3'>"+lblNoRecord+".</td></tr>" );
			}			
			sb.append("</table>");		
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return sb.toString();
	}	
}