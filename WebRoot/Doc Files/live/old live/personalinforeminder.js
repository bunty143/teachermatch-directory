var xmlHttp=null;
var txtBgColor="#F5E7E1";
var dp_Academics_page = 1;
var dp_Academics_noOfRows = 100;
var dp_Academics_sortOrderStr="";
var dp_Academics_sortOrderType="";

var dp_Certification_page = 1;
var dp_Certification_noOfRows = 10;
var dp_Certification_sortOrderStr="";
var dp_Certification_sortOrderType="";

var dp_Reference_page = 1;
var dp_Reference_noOfRows = 10;
var dp_Reference_sortOrderStr="";
var dp_Reference_sortOrderType="";

var dp_AdditionalDocuments_page = 1;
var dp_AdditionalDocuments_noOfRows = 10;
var dp_AdditionalDocuments_sortOrderStr="";
var dp_AdditionalDocuments_sortOrderType="";

var dp_Employment_page=1;
var dp_Employment_noOfRows=100;
var dp_Employment_sortOrderStr="";
var dp_Employment_sortOrderType="";

var dp_SubjectArea_page = 1;
var dp_SubjectArea_noOfRows = 10;
var dp_SubjectArea_sortOrderStr="";
var dp_SubjectArea_sortOrderType="";

var dp_VideoLink_Rows=10
var dp_VideoLink_page=1
var dp_VideoLink_sortOrderStr=""
var dp_VideoLink_sortOrderType=""

var dp_StdTchrExp_Rows=10;
var dp_StdTchrExp_page=1;
var dp_StdTchrExp_sortOrderStr="";
var dp_StdTchrExp_sortOrderType="";

var dp_Involvement_Rows=10;
var dp_Involvement_page=1;
var dp_Involvement_sortOrderStr="";
var dp_Involvement_sortOrderType="";

var dp_TchrLang_Rows=10;
var dp_TchrLang_page=1;
var dp_TchrLang_sortOrderStr="";
var dp_TchrLang_sortOrderType="";
var txtBgColor="#F5E7E1";

var count=0;
var index=-1;
var length=0;
var divid='';
var txtid='';
var hiddenDataArray = new Array();
var showDataArray = new Array();
var degreeTypeArray = new Array();
var dSPQuestionsErrorCount=0;

function createXMLHttpRequest()
{
	if (window.ActiveXObject)
    	xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
   	else if (window.XMLHttpRequest)
		xmlHttp = new XMLHttpRequest();
        else
        	alert(resourceJSON.msgAjaxNotSupported);
}

var dp_moduleName="";
function setFocus(id)
{
	if(id!="")
		document.getElementById(id).focus();
}


function showPortfolioReminder(){
	var iErrorCount=0;
	// Set Academic Transcript value
	//var personalinfo_config	= document.getElementById("personalinfo").value;
	var ssn_config			= document.getElementById("ssn_config").value;
	var dateOfBirth_config	= document.getElementById("dateOfBirth").value;
	var address_config		= document.getElementById("address").value;
	var phoneNumber_config	= document.getElementById("phoneNumber").value;
	var veteran_config		= document.getElementById("veteran").value;
	var retireNo_config		= document.getElementById("retirementnumber").value;
	var formeremployee_config	= document.getElementById("formeremployee").value;
	var honors_conf		= document.getElementById("honors_conf").value;
	var videoLink_conf		= document.getElementById("videoLink_conf").value;
	var certification_conf		= document.getElementById("certification_conf").value;
	var tfaAffiliate_conf		= document.getElementById("tfaAffiliate_conf").value;
	var involvement_conf		= document.getElementById("involvement_conf").value;
	var race		= document.getElementById("race").value;
	var academicsStatus		= document.getElementById("academicsStatus").value;
	var firstname_conf		= document.getElementById("firstname_conf").value;
	var lastname_conf		= document.getElementById("lastname_conf").value;
	var genderId		= document.getElementById("genderId").value;
	var ethnicorigin		= document.getElementById("ethnicorigin").value;
	var ethinicity		= document.getElementById("ethinicity").value;
	
	var wst_config = document.getElementById("wst_config").value;
	var resume_config = document.getElementById("resume_config").value;
	var exp_config = document.getElementById("exp_config").value;
	var nbc_config = document.getElementById("nbc_config").value;
	var affidavit_config = document.getElementById("affidavit_config").value;
	var generalKnowledge_config = document.getElementById("generalKnowledge_config").value;
	var subjectAreaExam_config = document.getElementById("subjectAreaExam_config").value;
	var additionalDocuments_conf = document.getElementById("additionalDocuments_conf").value;
	var references_conf = document.getElementById("references_conf").value;
	var acedminTranscript_conf=document.getElementById("acedminTranscript_conf").value;
	var isMiamiChk = document.getElementById("isMiami").value;
	var dspq_conf = document.getElementById("dspq_conf").value;
	
	var references0_conf = document.getElementById("references0_conf").value;
	var honors_conf = document.getElementById("honors_conf").value;
	var certification0_conf = document.getElementById("certification0_conf").value;
	var employment0_conf = document.getElementById("employment0_conf").value;
	var tfaAffiliate0_conf = document.getElementById("tfaAffiliate0_conf").value;
	var wst0_config = document.getElementById("wst0_config").value;
	var academicDspq_config = document.getElementById("academicDspq_config").value;
	var employment_conf=document.getElementById("employment_conf").value;
	
	 getStateByCountryForDspq("dspq");
	 showGridAcademics();
	 showGridCertifications();
	 getInvolvementGrid();
	 getHonorsGrid();
	 getPFEmploymentDataGrid();
	 showGridAdditionalDocuments();
	 getVideoLinksGrid();
	 getElectronicReferencesGrid();
	 showDSPQForPortRemind();
	 showPortfolioInstruction();
	 
	if(isMiamiChk=="true")
	{
		$('#eeodForMiamiHeader').show();
		$('#eeodForMiami').show();
		$('#eeodForOthers').hide();
	}
	else
	{
		$('#eeodForMiamiHeader').hide();
		$('#eeodForMiami').hide();
		$('#eeodForOthers').show();
	}
	//alert("dspq_conf:"+dspq_conf);
	

	if(academicDspq_config=="1" || academicsStatus=="1")
	{
		if(acedminTranscript_conf=="1"){
		    $('#transcriptDiv').show();
		}else
		{
			document.getElementById("dspq_conf").value=0;
			$('#transcriptDiv').hide();			
		}
	}
	else
	{
		$('#transcriptDiv').hide();			
	} 
	
	if(dspq_conf=="1")
	{ 
		$('#dynamicPortfolioInformation_Inner').append("&#149; "+resourceJSON.MsgDetailsOfDSPQ+"</BR>");
		document.getElementById("dspq_conf").value=1;
		$('#QuestionDiv').show();
	}
	else
	{
		document.getElementById("dspq_conf").value=0;
		$('#QuestionDiv').hide();			
	} 
	
	
	if(firstname_conf=="1")
	{ 
		$('#dynamicPortfolioInformation_Inner').append("&#149; "+resourceJSON.msgDetailsPI+"</BR>");
		document.getElementById("firstname_conf").value=1;
		$('#firstNameDiv').show();
	}
	else
	{
		document.getElementById("firstname_conf").value=0;
		$('#firstNameDiv').hide();			
	}
	if(lastname_conf=="1")
	{
		$('#dynamicPortfolioInformation_Inner').append("&#149; "+resourceJSON.msgDetailsPI+"</BR>");
		document.getElementById("lastname_conf").value=1;
		$('#lastNameDiv').show();
	}
	else
	{
		document.getElementById("lastname_conf").value=0;
		$('#lastNameDiv').hide();			
	}
	
	if(ssn_config=="1")
	{
		$('#dynamicPortfolioInformation_Inner').append("&#149; "+resourceJSON.msgDetailsSSN+"</BR>");
		document.getElementById("ssn_config").value=1;
		$('#ssnDiv').show();
	}
	else
	{
		document.getElementById("ssn_config").value=0;
		$('#ssnDiv').hide();
	}
	
	if(dateOfBirth_config=="1"){ 
	try{
		document.getElementById("dateOfBirth_config").value=1;
		
		$('#dobDiv').show();
	}catch(ee){}
	}else{
		document.getElementById("dateOfBirth_config").value=0;
		$('#dobDiv').hide();
	}
	 
	if(address_config=="1"){
		$('#dynamicPortfolioInformation_Inner').append("&#149; "+resourceJSON.msgDetailsAddress+"</BR>");
		document.getElementById("address_config").value=1;
		$('#addressDiv').show();
	}
	else
	{
		document.getElementById("address_config").value=0;
		$('#addressDiv').hide();
	}
	
	if(phoneNumber_config=="1"){
		$('#dynamicPortfolioInformation_Inner').append("&#149; "+resourceJSON.msgDetailsofPhoneNumber+"</BR>");
		document.getElementById("phone_config").value=1;
		$('#phoneDiv').show();
	}
	else
	{
		document.getElementById("phone_config").value=0;
		$('#phoneDiv').hide();
	}
	
	if(veteran_config=="1")
	{
		$('#dynamicPortfolioInformation_Inner').append("&#149; "+resourceJSON.msgDetailsVeteran+"</BR>");
		document.getElementById("veteran_config").value=1;
		$('#veteranDiv').show();
	}
	else
	{
		document.getElementById("veteran_config").value=0;
		$('#veteranDiv').hide();
	}
	
	if(retireNo_config=="1")
	{
		$('#dynamicPortfolioInformation_Inner').append("&#149; "+resourceJSON.msgRetirementNumber+"</BR>");
		document.getElementById("retireNo_config").value=1;
		$('#retirenoDiv').show();
	}
	else
	{
		document.getElementById("retireNo_config").value=0;
		$('#retirenoDiv').hide();
	}
	
	if(formeremployee_config=="1")
	{
		if(document.getElementById("districtIdForDSPQ").value==4218990){
			$(".ntPhiLfeild").hide();
		}
		
		$('#dynamicPortfolioInformation_Inner').append("&#149; "+resourceJSON.msgDetailsFormerEmployee+"</BR>");
		document.getElementById("formeremployee_config").value=1;
		$('#formerEmployeeDiv').show();
	}
	else
	{
		document.getElementById("formeremployee_config").value=0;
		$('#formerEmployeeDiv').hide();
	}
	
	if(honors_conf=="1")
	{
		$('#dynamicPortfolioInformation_Inner').append("&#149; "+resourceJSON.MsgDetailsOfHonours+"</BR>");
		document.getElementById("honors_conf").value=1;
		$('#honorsDiv').show();
	}
	else
	{
		document.getElementById("honors_conf").value=0;
		$('#honorsDiv').hide();
	}
   
	if(videoLink_conf=="1")
	{
		$('#dynamicPortfolioInformation_Inner').append("&#149; "+resourceJSON.msgVideoLink+"</BR>");
		document.getElementById("videoLink_conf").value=1;
		$('#videoLinkDiv').show();
	}
	else
	{
		document.getElementById("videoLink_conf").value=0;
		$('#videoLinkDiv').hide();
	}
	
	if(certification_conf=="1" || certification0_conf=="1")
	{
		$('#dynamicPortfolioInformation_Inner').append("&#149; "+resourceJSON.MsgDetailsOfCertification+"</BR>");
		document.getElementById("certification_conf").value=1;
		$('#certificationDiv').show();
	}
	else
	{
		document.getElementById("certification_conf").value=0;
		$('#certificationDiv').hide();
	}
	
	if(tfaAffiliate_conf=="1" || tfaAffiliate0_conf=="1")
	{
		$('#dynamicPortfolioInformation_Inner').append("&#149; "+resourceJSON.MsgDetailsOftfaAffiliate+"</BR>");
		document.getElementById("tFA_config").value=1;
		$('#tfaTeacherDiv').show();
		$('#tfarequired').show();
	}
	else
	{
		document.getElementById("tFA_config").value=0;
		$('#tfaTeacherDiv').hide();
		$('#tfarequired').hide();
	}
	
	if(involvement_conf=="1")
	{
		$('#dynamicPortfolioInformation_Inner').append("&#149; "+resourceJSON.MsgDetailsOfInvolvement+"</BR>");
		document.getElementById("involvement_conf").value=1;
		$('#involvementDiv').show();
	}
	else
	{
		document.getElementById("involvement_conf").value=0;
		$('#involvementDiv').hide();
	}
	if(race=="1")
	{
		$('#dynamicPortfolioInformation_Inner').append("&#149; "+resourceJSON.msgDetailsRace+"</BR>");
		document.getElementById("race_config").value=1;
		$('#raceDiv').show();
	}
	else
	{
		document.getElementById("race_config").value=0;
		$('#raceDiv').hide();
	}
	if(academicsStatus=="1" || acedminTranscript_conf=='1')
	{
		
		$('#dynamicPortfolioInformation_Inner').append("&#149; "+resourceJSON.MsgDetailsofAcademics+"</BR>");
		document.getElementById("academicsStatus").value=1;
		$('#academicsDiv').show();
	}
	else
	{
		document.getElementById("academicsStatus").value=0;
		$('#academicsDiv').hide();
	}
	if(genderId=="1")
	{
		$('#dynamicPortfolioInformation_Inner').append("&#149; "+resourceJSON.msgDetailsGender+"</BR>");
		document.getElementById("gender_config").value=1;
		$('#genderDiv').show();
	}
	else
	{
		document.getElementById("gender_config").value=0;
		$('#genderDiv').hide();
	}
	
	if(ethnicorigin=="1")
	{
		$('#dynamicPortfolioInformation_Inner').append("&#149; "+resourceJSON.MsgDetailsOfEthenicOrigin+"</BR>");
		document.getElementById("ethnicOrigin_config").value=1;
		$('#ethnicOriginDiv').show();
	}
	else
	{
		document.getElementById("ethnicOrigin_config").value=0;
		$('#ethnicOriginDiv').hide();
	}
	
	if(ethinicity=="1")
	{
		$('#dynamicPortfolioInformation_Inner').append("&#149; "+resourceJSON.MsgDetailsOfEthenicity+"</BR>");
		document.getElementById("ethinicity_config").value=1;
		$('#ethinicityMasterDiv').show();
	}
	else
	{
		document.getElementById("ethinicity_config").value=0;
		$('#ethinicityMasterDiv').hide();
	}
	
	if(wst_config=="1" || wst0_config=="1")
	{
		$('#dynamicPortfolioInformation_Inner').append("&#149; "+resourceJSON.MsgDetailsOfWillingtoSubstitute+"</BR>");
		document.getElementById("wst_config").value=1;
		$('#substituteTeacherDiv').show();
	}
	else
	{
		document.getElementById("wst_config").value=0;
		$('#substituteTeacherDiv').hide();
	}
	
	if(resume_config=="1")
	{
		$('#dynamicPortfolioInformation_Inner').append("&#149; "+resourceJSON.MsgDetailsOfResume+"</BR>");
		document.getElementById("resume_config").value=1;
		$('#resumeDiv').show();
	}
	else
	{
		document.getElementById("resume_config").value=0;
		$('#resumeDiv').hide();
	}
	
	if(exp_config=="1")
	{
		$('#dynamicPortfolioInformation_Inner').append("&#149; "+resourceJSON.MsgDetailsOfTeachingExperience+"</BR>");
		document.getElementById("exp_config").value=1;
		$('#expCertTeacherTrainingDiv').show();
	}
	else
	{
		document.getElementById("exp_config").value=0;
		$('#expCertTeacherTrainingDiv').hide();
	}
	
	if(nbc_config=="1")
	{
		$('#dynamicPortfolioInformation_Inner').append("&#149; "+resourceJSON.MsgDetailsOfNational+"</BR>");
		document.getElementById("nbc_config").value=1;
		$('#nationalBoardCertDiv').show();
	}
	else
	{
		document.getElementById("nbc_config").value=0;
		$('#nationalBoardCertDiv').hide();
	}
	
	if(affidavit_config=="1")
	{
		$('#dynamicPortfolioInformation_Inner').append("&#149; "+resourceJSON.mafDetailsofAffidavit+"</BR>");
		document.getElementById("affidavit_config").value=1;
		$('#affidavitDiv').show();
	}
	else
	{
		document.getElementById("affidavit_config").value=0;
		$('#affidavitDiv').hide();
	}
	if(generalKnowledge_config=="1")
	{
		$('#dynamicPortfolioInformation_Inner').append("&#149; "+resourceJSON.MsgDetailsOfGeneral+"</BR>");
		document.getElementById("generalKnowledge_config").value=1;
		$('#generalKnowledgeDiv').show();
	}
	else
	{
		document.getElementById("generalKnowledge_config").value=0;
		$('#generalKnowledgeDiv').hide();
	}
	
	if(subjectAreaExam_config=="1")
	{
		$('#dynamicPortfolioInformation_Inner').append("&#149; "+resourceJSON.MsgSubjectAreaExam+"</BR>");
		document.getElementById("subjectAreaExam_config").value=1;
		$('#subjectAreaDiv').show();
	}
	else
	{
		document.getElementById("subjectAreaExam_config").value=0;
		$('#subjectAreaDiv').hide();
	}
	
	if(additionalDocuments_conf=="1")
	{ 
		$('#dynamicPortfolioInformation_Inner').append("&#149; "+resourceJSON.msgAdditionalDocuments+"</BR>");
		document.getElementById("additionalDocuments_conf").value=1;
		$('#additionalDocumentsDiv').show();
	}
	else
	{
		document.getElementById("additionalDocuments_conf").value=0;
		$('#additionalDocumentsDiv').hide();			
	}
	
	if(references_conf=="1" || references0_conf=="1")
	{ 
		$('#dynamicPortfolioInformation_Inner').append("&#149; "+resourceJSON.MsgDetailsOfReferences+"</BR>");
		document.getElementById("references_conf").value=1;
		$('#referenceDiv').show();
	}
	else
	{
		document.getElementById("references_conf").value=0;
		$('#referenceDiv').hide();			
	}
	
	if(employment_conf=="1" || employment0_conf=="1")
	{ 
		$('#dynamicPortfolioInformation_Inner').append("&#149; "+resourceJSON.MsgDetailsOfEmployement+"</BR>");
		document.getElementById("employment_config").value=1;
		$('#employmentDiv').show();
	}
	else
	{
		document.getElementById("employment_config").value=0;
		$('#employmentDiv').hide();			
	}
}

function getPersonalInfoValues(){
	PFExperiences.getTeacherPersonalInfoValues(
	{
		async: false,
		errorHandler:handleError,
		callback: function(data)
		{
			if(data!=null)
			{
				
				if(data.phoneNumber!=null && data.phoneNumber!="")
				{
					document.getElementById("phoneNumber1").value=data.phoneNumber.substring(0,3);
					document.getElementById("phoneNumber2").value=data.phoneNumber.substring(3,6);
					document.getElementById("phoneNumber3").value=data.phoneNumber.substring(6,10);
				}
				else
				{
					document.getElementById("phoneNumber1").value="";
					document.getElementById("phoneNumber2").value="";
					document.getElementById("phoneNumber3").value="";
				}
				
				
				if(data.firstName!=null && data.firstName!="")
					document.getElementById("firstName_pi").value=data.firstName;
				else
					document.getElementById("firstName_pi").value="";
				
				if(data.middleName!=null && data.middleName!="")
					document.getElementById("middleName_pi").value=data.middleName;
				else
					document.getElementById("middleName_pi").value="";
				
				if(data.lastName!=null && data.lastName!="")
					document.getElementById("lastName_pi").value=data.lastName;
				else
					document.getElementById("lastName_pi").value="";
				
				if(data.anotherName!=null && data.anotherName!="")
					document.getElementById("anotherName").value=data.anotherName;
				else
					document.getElementById("anotherName").value="";
				
				if(data.SSN!=null && data.SSN!="")
				{
					if(document.getElementById("districtIdForDSPQ").value==3680340)
					{
						if(data.SSN.length==4)
							document.getElementById("ssn_pi").value=data.SSN;
						else
						{
							var s = data.SSN.substring(5);
							document.getElementById("ssn_pi").value=s;	
						}
					}else if(document.getElementById("districtIdForDSPQ").value==1200390)
					{
						document.getElementById("ssn_pi").value=data.SSN;	
					}else if(document.getElementById("districtIdForDSPQ").value==4218990)
					{
						document.getElementById("ssn_pi").value=data.SSN;	
					}else
					{
						document.getElementById("ssn_pi").value=data.SSN;	
					}
				}
				else
					document.getElementById("ssn_pi").value="";
				
				if(data.dob!=null && data.dob!="")
				{
					//document.getElementById("dob").value=(new Date(data.dob).getMonth()+1)+"-"+(new Date(data.dob).getDate())+"-"+(new Date(data.dob).getFullYear());
					
					document.getElementById("dobMonth").value=new Date(data.dob).getMonth()+1;
					document.getElementById("dobDay").value=new Date(data.dob).getDate();
					document.getElementById("dobYear").value=new Date(data.dob).getFullYear();
				}
				else
				{
					//document.getElementById("dob").value="";
					document.getElementById("dobMonth").value="0";
					document.getElementById("dobDay").value="0";
					document.getElementById("dobYear").value="";
				}
				
				if(data.isVateran==1)
				{
					document.getElementById("vt1").checked=true;
				}
				else if(data.isVateran==0 || data.isVateran==null)
				{
					document.getElementById("vt2").checked=true;
				}
				
				
				if(data.addressLine1!=null && data.addressLine1!="")
					document.getElementById("addressLine1").value=data.addressLine1;
				else
					document.getElementById("addressLine1").value="";
				
				if(data.addressLine2!=null && data.addressLine2!="")
					document.getElementById("addressLine2").value=data.addressLine2;
				else
					document.getElementById("addressLine2").value="";
				if(data.zipCode!=null && data.zipCode!="")
					document.getElementById("zipCode").value=data.zipCode;
				else
					document.getElementById("zipCode").value="";
				
				//try{
					//----- Start :: Country, State, City------------
					if(data.countryId!=null)
					{
						document.getElementById("ctry"+data.countryId.countryId).selected=true;
						
						if(data.countryId.countryId==223)
						{
							if(data.stateId!=null)
							{	
								document.getElementById("st_"+data.stateId.stateId).selected=true;
								//getCityListByStateSetForDSPQ(data);
							}
							else
								document.getElementById("slst").selected=true;
						}
						else
						{
							if(document.getElementById("countryCheck").value==1)
							{
								if(data.stateId!=null)
								{
									//document.getElementById("st_"+data.stateId.stateId).selected=true;
									//getCityListByStateSetForDSPQ(data);
									if(	data.otherCity!=null)
									{
										document.getElementById("otherCity").value=data.otherCity;
									}
								}
								else
									document.getElementById("slst").selected=true;
							}
							else
							{
								if((data.otherState!=null && data.otherState!="") || (data.otherCity!=null && data.otherCity!=""))
								{
									if(data.otherState!=null)
										document.getElementById("otherState").value=data.otherState;
									if(data.otherCity!=null)
										document.getElementById("otherCity").value=data.otherCity;
									
									$('#divotherstate').show();
									$('#divothercity').show();

									$('#divUSAState').hide();
									$('#divUSACity').hide();
								}
							}
						}
					}
					
					try
					{
						var chkIsRetired = 0;
						
						if(data.retirementnumber!=null)
						{
							chkIsRetired++;
							document.getElementById("retireNo").value=data.retirementnumber;
						}
						if(data.stateMaster!=null && data.stateMaster.stateId>0)
						{
							chkIsRetired++;
							document.getElementById("stMForretire").value=data.stateMaster.stateId;
						}
						if(data.districtmaster!=null)
						{
							chkIsRetired++;
							document.getElementById("retireddistrictName").value=data.districtmaster.districtName;
							document.getElementById("retireddistrictId").value=data.districtmaster.districtId;
						}
						
						if(chkIsRetired==3)
						{
							document.getElementById("isretired").checked=true;
							$("#isRetiredDiv").show();
						}
						else
						{
							document.getElementById("isretired").checked=false;
							$("#isRetiredDiv").hide();
						}
						
					}catch(err){}
				
				// Former Employee
					
				var flagForEmp = false;
				if(data.employeeType!=null && data.employeeType==1) {
					
					
				} else {
					
					
				}
					
					
			}
			//hideTFAFields_DP();
		}
	});

}

function getStateByCountryForDspq(stateModuleFlag){
	var countryId = document.getElementById("countryId").value;
	if(countryId!='')
	{
		StateAjax.getStateByCountry(countryId,stateModuleFlag,{ 
			async: true,
			callback: function(data)
			{
				if(data[0]!="")
				{
					document.getElementById("countryCheck").value=1;
					if(stateModuleFlag=="dspq")
					{
						$("#stateIdForDSPQ").html(data[0]);
						if(!(countryId==data[3]))
						{
							document.getElementById("slst").selected=true;
							document.getElementById("slcty").selected=true;
						}
							
					}
					else if(stateModuleFlag=="portfolio")
					{
						$("#stateId").html(data[0]);
						if(!(countryId==data[3]))
						{
							document.getElementById("slst").selected=true;
							document.getElementById("slcty").selected=true;
						}
							
					}
					document.getElementById("divUSAState").style.display="inline";
					document.getElementById("divotherstate").style.display="none";
					if(countryId!=223)
					{
						document.getElementById("divUSACity").style.display="none";
						document.getElementById("divothercity").style.display="inline";
						
						if(countryId==data[3])
							document.getElementById("otherCity").value=data[2];
						else
							document.getElementById("otherCity").value="";
					}
					else
					{
						document.getElementById("divUSACity").style.display="inline";
						document.getElementById("divothercity").style.display="none";
						//getCityListByStateSetForDSPQ(data[0]);
						
						var diststatId= document.getElementById("DiststatId").value;
						var infostatId=document.getElementById("InfostatId").value;
						if(diststatId==infostatId){
							if(countryId==data[3])
							{
								if(data[4]!=null && data[4]!="")
									document.getElementById("ct"+data[4]).selected=true;
								else
									document.getElementById("slcty").selected=true;
							}
						}
					}
				}
				else
				{
					document.getElementById("countryCheck").value=0;
					document.getElementById("divUSAState").style.display="none";
					document.getElementById("divUSACity").style.display="none";
					
					if(countryId==data[3])
					{
						document.getElementById("otherState").value=data[1];
						document.getElementById("otherCity").value=data[2];
					}
					else
					{
						document.getElementById("otherState").value="";
						document.getElementById("otherCity").value="";
					}
					document.getElementById("divotherstate").style.display="inline";
					document.getElementById("divothercity").style.display="inline";
				}
			},
		});
	}
	
	if(countryId=='')
	{
		if(resourceJSON.locale=="fr")
		{
			countryId='38';
			document.getElementById("ctry38").selected=true;
		}else
		{
			countryId='223';
			document.getElementById("ctry223").selected=true;
		}
	
		
		StateAjax.getStateByCountry(countryId,stateModuleFlag,{ 
			async: true,
			callback: function(data)
			{
				if(data[0]!="")
				{
					document.getElementById("countryCheck").value=1;
					if(stateModuleFlag=="dspq")
					{
						$("#stateIdForDSPQ").html(data[0]);
			
						if(!(countryId==data[3]))
							document.getElementById("slst").selected=true;
					}
					else if(stateModuleFlag=="portfolio")
					{
						$("#stateId").html(data[0]);
						if(!(countryId==data[3]))
							document.getElementById("slcty").selected=true;
					}
					document.getElementById("divUSAState").style.display="inline";
					document.getElementById("divUSACity").style.display="inline";
					document.getElementById("divotherstate").style.display="none";
					document.getElementById("divothercity").style.display="none";
				}
				else
				{
					document.getElementById("countryCheck").value=0;
					document.getElementById("divUSAState").style.display="none";
					document.getElementById("divUSACity").style.display="none";
					
					if(countryId==data[3])
					{
						document.getElementById("otherState").value=data[1];
						document.getElementById("otherCity").value=data[2];
					}
					else
					{
						document.getElementById("otherState").value="";
						document.getElementById("otherCity").value="";
					}
					document.getElementById("divotherstate").style.display="inline";
					document.getElementById("divothercity").style.display="inline";
				}
			},
		});
	}
	
}

function changeCityStateByZipForDSPQ(){
	
	if($("#divUSAState").is(':visible') && $("#divUSACity").is(':visible'))
	{
		var zipCode = document.getElementById("zipCode");
		
		$('#loadingDivWait').show();
		delay(1000);
		var res;
		createXMLHttpRequest();  
		queryString = "findcitybyzip.do?zipCode="+zipCode.value+"&dt="+new Date().getTime();
		xmlHttp.open("POST", queryString, true);
		xmlHttp.onreadystatechange = function()
		{
			if(xmlHttp.readyState == 4)
			{
				if(xmlHttp.status==200)	
				{	
					var res = xmlHttp.responseText.split("|");	
					if(res.length!=1)
					{
						var cityId = trim(res[0].toString());
						var stateId = trim(res[1].toString());
						try
						{
						document.getElementById("st_"+stateId).selected=true;					
						document.getElementById('stateIdForDSPQ').onchange();			
						document.getElementById("ct"+cityId).selected=true;
						}
						catch(e)
						{}
					}
					$('#loadingDivWait').hide();
				}			
				else
				{
					alert(resourceJSON.msgServerErr);
				}
			}
		}
		xmlHttp.send(null);	
	}
}

function getCityListByStateForDSPQMain(){
	
	$('#loadingDiv').show();
	try{
		var stateId = "";
		stateId = document.getElementById("stateIdForDSPQ");
		if(stateId!=null && stateId!=""){
			var res;
			createXMLHttpRequest();
			queryString = "findcityhtmlbystate.do?stateId="+stateId.value+"&dt="+new Date().getTime();
			xmlHttp.open("POST", queryString, true);
			xmlHttp.onreadystatechange = function()
			{
				if(xmlHttp.readyState == 4)
				{
					if(xmlHttp.status==200)	
					{
						while (document.getElementById("cityIdForDSPQ").childNodes.length >= 1 )
						{
						    document.getElementById("cityIdForDSPQ").removeChild(document.getElementById("cityIdForDSPQ").firstChild);       
						}
						var optAr = xmlHttp.responseText.split("##");
						var newOption;
						
						newOption = document.createElement('option');
					    newOption.text="Select City";
					    newOption.value="";
					    newOption.id="slcty";
					   	document.getElementById('cityIdForDSPQ').options.add(newOption);
						for(var i=0;i<optAr.length-1;i++)
						{
						    newOption = document.createElement('option');
						    newOption.id=optAr[i].split("$$")[0];
						    newOption.value=optAr[i].split("$$")[1];
						    newOption.text=optAr[i].split("$$")[2];
						    document.getElementById('cityIdForDSPQ').options.add(newOption);
						}
						$('#loadingDiv').hide();
					}else{
						alert(resourceJSON.msgServerErr);
					}
				}
			}
			xmlHttp.send(null);	
		}
		
	}catch(e){}
}

function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert(""+resourceJSON.msgServerErr+": "+exception.javaClassName);}
}
function showEmpNoDiv(radioFlag)
{
	var nextProcess=true;	
	if($("#districtIdForDSPQ").length > 0 && document.getElementById("districtIdForDSPQ").value==4218990 && $("input:checkbox[name=isAffilated]").is(":checked")){
		if($('input:radio[name=staffType]').is(":checked") && radioFlag!=2){
			//callDynamicPortfolio();
			$("#myModalDymanicPortfolio").hide();
			
			$("#affDivPHiL").hide();
			$("#dynamicInfoMsg").html(""+resourceJSON.MsgCurrentInternalCandidate+"");
			$("#wrongDivPHiL").show();
			nextProcess=false;
		}else if(!$('input:radio[name=staffType]').is(":checked") && radioFlag==2){
			$("#myModalDymanicPortfolio").hide();
			$("#affDivPHiL").hide();
			$("#dynamicInfoMsg").html(""+resourceJSON.MsgExternalCandidate+"");
			$("#wrongDivPHiL").show();
			nextProcess=false;
		}
		
	}
	if(nextProcess==true)
	if(radioFlag==1)
	{
		$("#empfewDiv").hide();
		$("#fe1Div").show();
		$("#fe2Div").hide();
		$("#position6Div").hide();
		if($("#districtIdForDSPQ").length > 0 && document.getElementById("districtIdForDSPQ").value==1302010){
			$("#empfewDiv").show();
		}
	}
	else if(radioFlag==2)
	{
		$("#fe1Div").hide();
		$("#fe2Div").show();
		$("#position6Div").hide();
		if($("#districtIdForDSPQ").length > 0 && document.getElementById("districtIdForDSPQ").value==4218990){
			$("#myModalDymanicPortfolio").hide();
			$("#affDivPHiL").show();
			$("#rdCEmp1").hide();
			$("#rdCEmp2").hide();
		}		
	}else if(radioFlag==6)
	{
		$("#fe1Div").hide();
		$("#fe2Div").hide();
		$("#position6Div").show();
		if($("#districtIdForDSPQ").length > 0 && document.getElementById("districtIdForDSPQ").value==4218990){
			$("#myModalDymanicPortfolio").hide();
			$("#affDivPHiL").hide();
			$("#rdCEmp1").hide();
			$("#rdCEmp2").hide();
			
			
		}		
	}
	else
	{
		$("#fe1Div").hide();
		$("#fe2Div").hide();
		$("#position6Div").hide();
	}
}
function showUniversityForm()
{
	document.getElementById("degreeName").disabled=false;
	document.getElementById("degreeId").disabled=false;
	document.getElementById("degreeType").disabled=false;
	
	document.getElementById("universityId").disabled=false;
	document.getElementById("universityName").disabled=false;
	
	document.getElementById("fieldId").disabled=false;
	document.getElementById("fieldName").disabled=false;
	document.getElementById("attendedInYear").disabled=false;
	document.getElementById("leftInYear").disabled=false;
	
	document.getElementById("gpaFreshmanYear").disabled=false;
	document.getElementById("gpaJuniorYear").disabled=false;     
	document.getElementById("gpaSophomoreYear").disabled=false;     
	document.getElementById("gpaSeniorYear").disabled=false;     
	document.getElementById("gpaCumulative").disabled=false;
	
	document.getElementById("divGPARowOther").disabled=false;
	document.getElementById("gpaCumulative1").disabled=false;
	document.getElementById("divAcademicRow").style.display="block";
	
	
	document.getElementById("divDone").style.display="block";
	document.getElementById("divGPARow").style.display="none";
	document.getElementById("divGPARowOther").style.display="none";
	try{document.getElementById("divResumeSection").style.display="none";}catch (e){}
	document.getElementById("academicId").value="";
	document.getElementById("frmAcadamin").reset();
	document.getElementById("frmGPA").reset();
	document.getElementById("frmGPA1").reset();	
	document.getElementById("degreeName").focus();
	setDefColortoErrorMsg_Academic();
	if(document.getElementById("districtIdForDSPQ").value==3904380 || document.getElementById("districtIdForDSPQ").value==1201470 || document.getElementById("districtIdForDSPQ").value==7800040 || document.getElementById("districtIdForDSPQ").value==7800047){
		$("#crequired").css("color", "white");
		$("#crequired2").css("color", "white");
	}
	if(document.getElementById("districtIdForDSPQ").value==5304860){
		$("#transUploadTooltip").attr("data-original-title", resourceJSON.Msgofficialcollegetranscript);
	}else if(document.getElementById("districtIdForDSPQ").value==1302010 && ($("#jobcategoryDsp").val()=="Classified" || $("#jobcategoryDsp").val()=="Substitutes")){
		$("#transUploadTooltip").attr("data-original-title", resourceJSON.MsgParaprofessionalApplicants);
	}else{
		$("#transUploadTooltip").attr("data-original-title", resourceJSON.msguploadyourtranscript);
	}
	$('#errordiv_AcademicForm').empty();
	return false;
}
function showGridAcademics()
{ 

	var isMiami=document.getElementById("isMiami").value;
	var districtIdForDSPQ="";	
	if ($('#districtIdForDSPQ').length > 0) {
		districtIdForDSPQ=document.getElementById("districtIdForDSPQ").value;
	}
	
	var transcriptFlag=trim(document.getElementById("academicTranscriptFlag").value);
	
	if(districtIdForDSPQ==7800038)
	{
		//WOT : without transcript
		PFAcademics.getPFAcademinGridForDspqWOT(dp_Academics_noOfRows,dp_Academics_page,dp_Academics_sortOrderStr,dp_Academics_sortOrderType,isMiami,transcriptFlag,{ 
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{
				$('#divDataGridAcademin').html(data);
				applyScrollOnTbl_Acad();
			}});
	}else{
	PFAcademics.getPFAcademinGridForDspq(dp_Academics_noOfRows,dp_Academics_page,dp_Academics_sortOrderStr,dp_Academics_sortOrderType,isMiami,transcriptFlag,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#divDataGridAcademin').html(data);
			applyScrollOnTbl_Acad();
			
		}});
	}
}

function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
	var gridNameFlag = document.getElementById("gridNameFlag").value;
	if(gridNameFlag=="subjectAreas")
	{
		if(pageno!=''){
			dp_SubjectArea_page=pageno;	
		}else{
			dp_SubjectArea_page=1;
		}
		dp_SubjectArea_sortOrderStr	=	sortOrder;
		dp_SubjectArea_sortOrderType	=	sortOrderTyp;
		if(document.getElementById("pageSize1")!=null){
			dp_SubjectArea_noOfRows = document.getElementById("pageSize1").value;
		}else{
			dp_SubjectArea_noOfRows=10;
		}
		showGridSubjectAreasGrid();
	}
	else if(gridNameFlag=="additionalDocuments"){
		if(pageno!=''){
			dp_AdditionalDocuments_page=pageno;	
		}else{
			dp_AdditionalDocuments_page=1;
		}
		dp_AdditionalDocuments_sortOrderStr	=	sortOrder;
		dp_AdditionalDocuments_sortOrderType	=	sortOrderTyp;
		if(document.getElementById("pageSize1")!=null){
			dp_AdditionalDocuments_noOfRows = document.getElementById("pageSize1").value;
		}else{
			dp_AdditionalDocuments_noOfRows=10;
		}
		showGridAdditionalDocuments();
	}else if(gridNameFlag=="academics"){
		if(pageno!=''){
			dp_Academics_page=pageno;	
		}else{
			dp_Academics_page=1;
		}
		dp_Academics_sortOrderStr	=	sortOrder;
		dp_Academics_sortOrderType	=	sortOrderTyp;
		if(document.getElementById("pageSize1")!=null){
			dp_Academics_noOfRows = document.getElementById("pageSize1").value;
		}else{
			dp_Academics_noOfRows=10;
		}
		showGridAcademics();
	}else if(gridNameFlag=="certification"){
		if(pageno!=''){
			dp_Certification_page=pageno;	
		}else{
			dp_Certification_page=1;
		}
		dp_Certification_sortOrderStr	=	sortOrder;
		dp_Certification_sortOrderType	=	sortOrderTyp;
		if(document.getElementById("pageSize3")!=null){
			dp_Certification_noOfRows = document.getElementById("pageSize3").value;
		}else{
			dp_Certification_noOfRows=10;
		}
		showGridCertifications();
	}else if(gridNameFlag=="reference"){
		if(pageno!=''){
			dp_Reference_page=pageno;	
		}else{
			dp_Reference_page=1;
		}
		dp_Reference_sortOrderStr	=	sortOrder;
		dp_Reference_sortOrderType	=	sortOrderTyp;
		if(document.getElementById("pageSize1")!=null){
			dp_Reference_noOfRows = document.getElementById("pageSize1").value;
		}else{
			dp_Reference_noOfRows=10;
		}
		getElectronicReferencesGrid();
	}else if(gridNameFlag=="workExperience"){
		if(pageno!=''){
			dp_Employment_page=pageno;	
		}else{
			dp_Employment_page=1;
		}
		dp_Employment_sortOrderStr	=	sortOrder;
		dp_Employment_sortOrderType	=	sortOrderTyp;
		if(document.getElementById("pageSize1")!=null){
			dp_Employment_noOfRows = document.getElementById("pageSize1").value;
		}else{
			dp_Employment_noOfRows=10;
		}
		getPFEmploymentDataGrid();
	}else if(gridNameFlag=="videolinks"){

		if(pageno!=''){
			dp_VideoLink_page=pageno;	
		}else{
			dp_VideoLink_page=1;
		}
		dp_VideoLink_sortOrderStr	=	sortOrder;
		dp_VideoLink_sortOrderType	=	sortOrderTyp;
		if(document.getElementById("pageSize1")!=null){
			dp_VideoLink_Rows = document.getElementById("pageSize1").value;
		}else{
			dp_VideoLink_Rows=10;
		}
		getVideoLinksGrid();
	
	}else if(gridNameFlag=="stdTchrGrid"){

		if(pageno!=''){
			dp_StdTchrExp_page=pageno;	
		}else{
			dp_StdTchrExp_page=1;
		}
		dp_StdTchrExp_sortOrderStr	=	sortOrder;
		dp_StdTchrExp_sortOrderType	=	sortOrderTyp;
		if(document.getElementById("pageSize1")!=null){
			dp_StdTchrExp_Rows = document.getElementById("pageSize1").value;
		}else{
			dp_StdTchrExp_Rows=10;
		}
		displayStdTchrExp();
	
	}
	else if(gridNameFlag=="involvement"){
		if(pageno!=''){
			dp_Involvement_page=pageno;	
		}else{
			dp_Involvement_page=1;
		}
		dp_Involvement_sortOrderStr	=	sortOrder;
		dp_Involvement_sortOrderType	=	sortOrderTyp;
		if(document.getElementById("pageSize1")!=null){
			dp_Involvement_Rows = document.getElementById("pageSize1").value;
		}else{
			dp_Involvement_Rows=10;
		}
		getInvolvementGrid();	
	}else if(gridNameFlag=="language"){

		if(pageno!=''){
			dp_TchrLang_page=pageno;	
		}else{
			dp_TchrLang_page=1;
		}
		dp_TchrLang_sortOrderStr	=	sortOrder;
		dp_TchrLang_sortOrderType	=	sortOrderTyp;
		if(document.getElementById("pageSize1")!=null){
			dp_TchrLang_Rows = document.getElementById("pageSize1").value;
		}else{
			dp_TchrLang_Rows=10;
		}
		displayTchrLanguage();
	
	}
}
function getRacedata()
{
	
	var rId=document.getElementById("rId").value;
	document.getElementById("raceData").innerHTML="";
	DistrictPortfolioConfigAjax.getDSPRaceData(rId,{ 
		async: false,
		callback: function(data)
		{
			document.getElementById("raceData").innerHTML=data;
			
		},
	});
	}
function resetUniversityForm()
{
	document.getElementById("divAcademicRow").style.display="none";
	document.getElementById("divDone").style.display="none";
	document.getElementById("divGPARow").style.display="none";
	document.getElementById("divGPARowOther").style.display="none";
	document.getElementById("frmAcadamin").reset();
	return false;
}
function showForm_Certification()
{
	document.getElementById("frmCertificate").reset();
	document.getElementById("certId").value="";	
	document.getElementById("divMainForm").style.display="block";
	document.getElementById("removeCert").style.display="none";
	document.getElementById("certificationStatusMaster").focus();
	for (var i = 4; i <= 11; i++) { 
		$("#certificationtypeMaster option[value='"+i+"']").hide();		
	}
	$(".certText").hide();
	var districtIdForDSPQ="";	
	if ($('#districtIdForDSPQ').length > 0) {
		districtIdForDSPQ=document.getElementById("districtIdForDSPQ").value;
	}
	$(".certText").find(".jqte_editor").html("");
	document.getElementById("pathOfCertification").value="";
	if(districtIdForDSPQ==7800038){
		$(".certClass").hide();
		$(".certText").show();
	}else if(districtIdForDSPQ==3904380 || districtIdForDSPQ==7800040){
		$(".dOENumberDiv").hide();
		$(".cluDiv").hide();
		$(".gradeLvl").hide();
	}
	else{
		$(".certText").hide();
		$(".certClass").show();
		
	}
	if(districtIdForDSPQ==804800){
		$(".cluDiv").hide();
		$(".doeReq").show();
	}
	
	if(districtIdForDSPQ==4218990){
		$(".doeShow").hide();
		$(".ppdiShow").show();
		$(".gradeLvl").hide();
		$('#clsToolTip').show();
		if($('#isPrinciplePhiladelphia').val()=="1"){
			$(".certSrc").hide();
		}
		for (var i = 4; i <= 11; i++) { 
			$("#certificationtypeMaster option[value='"+i+"']").show();
		}
	}else{
		$(".ppdiShow").hide();
		$(".doeShow").show();
		$(".certSrc").show();
	}
	$("#certiExpiration").val("");
	/*if(districtIdForDSPQ==1200390)
		$('#certiExpirationDiv').show();
	else*/
		$('#certiExpirationDiv').show();
	
	if(districtIdForDSPQ==7800040)
		$('#ieinNumberDiv').show();
	else
		$('#ieinNumberDiv').hide();
	
	$("#certificationtypeMasterTool").hide();
	if(districtIdForDSPQ==5304860  && ($("#jobcategoryDsp").val()=="Administrator" || $("#jobcategoryDsp").val()=="Principal / Asst Principal")){
		$("#certificationtypeMasterTool").show();
	}else if(districtIdForDSPQ==1302010 && ($("#jobcategoryDsp").val()=="Classified" || $("#jobcategoryDsp").val()=="Substitutes")){
		$('#certificationtypeMasterTool').attr('data-original-title', resourceJSON.msgallcertificationsheld);
		$("#certificationtypeMasterTool").show();
	}
	
	$('#errordiv_Certification').empty();
	setDefColortoErrorMsg_Certification();
	fieldsDisable(0);
	return false;
}

function clearForm_Certification()
{
	$("#praxisArea").hide();
	$("#reading").html("");
	$("#writing").html("");
	$("#maths").html("");
}
function hideForm_Certification()
{
	
	document.getElementById("frmCertificate").reset();
	document.getElementById("certId").value="";	
	document.getElementById("divMainForm").style.display="none";
	$('#errordiv_Certification').empty();
	setDefColortoErrorMsg_Certification();
	return false;
}
function showGridCertifications()
{ 
	var districtIdForDSPQ="";	
	if ($('#districtIdForDSPQ').length > 0) {
		districtIdForDSPQ=document.getElementById("districtIdForDSPQ").value;
	}
	//alert(document.getElementById("districtIdForDSPQ").value);
	if(districtIdForDSPQ==7800038){
	PFCertifications.getPFCertificationsGridDspqNoble(dp_Certification_noOfRows,dp_Certification_page,dp_Certification_sortOrderStr,dp_Certification_sortOrderType,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
		document.getElementById("divDataCertificate").innerHTML=data;
		applyScrollOnTbl_Certificat();
	}})
	}
	else {
		PFCertifications.getPFCertificationsGrid(dp_Certification_noOfRows,dp_Certification_page,dp_Certification_sortOrderStr,dp_Certification_sortOrderType,{ 
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{
				document.getElementById("divDataCertificate").innerHTML=data;
				applyScrollOnTbl_Certificat();
			
		if(districtIdForDSPQ==4218990 && $('#isPrinciplePhiladelphia').val()=="1"){	
			$("#certSRCHeader").html("");
			$(".certSRCRec").html("");
		}
		}})
	}
}

function setGridNameFlag(gridName)
{
	//alert("gridName->"+gridName);
	document.getElementById("gridNameFlag").value=gridName;
}
function getInvolvementGrid()
{
	PFExperiences.getInvolvementGrid(dp_Involvement_Rows,dp_Involvement_page,dp_Involvement_sortOrderStr,dp_Involvement_sortOrderType,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
		document.getElementById("divDataInvolve").innerHTML=data;
			//$('#divDataInvolve').html(data);
			applyScrollOnInvlvement();
		}});
}
function getHonorsGrid()
{
	PFExperiences.getHonorsGrid({ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			document.getElementById("divDataHon").innerHTML=data;
			applyScrollOnHon();
		}});
}
function getPFEmploymentDataGrid()
{
	PFExperiences.getPFEmploymentGrid(dp_Employment_noOfRows,dp_Employment_page,dp_Employment_sortOrderStr,dp_Employment_sortOrderType, { 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			//$('#divDataGridEmploy').html(data);
			document.getElementById("divDataGridEmploy").innerHTML=data;
			applyScrollOnTbl_EmployeementHistry();
			if(document.getElementById("districtIdForDSPQ").value==7800036 || document.getElementById("districtIdForDSPQ").value==1201470){
				$('#empAnnSal').html("");
				$('.empAnnSalRec').html("");
			}
			
			if($('#districtIdForDSPQ').val()==4218990 && $('#isPrinciplePhiladelphia').val()=="1"){
				$('#empAnnSal').html("");
				$('.empAnnSalRec').html("");
				$("#empDurationHeader").html("");
				$(".empDurationRec").html("");
			}
		}});
}
function showInvolvementForm()
{
	document.getElementById("frmInvolvement").reset();
	document.getElementById("involvementId").value="";	
	document.getElementById("divInvolvement").style.display="block";
	document.getElementById("organizationInv").focus();
	document.getElementById("divLeadNoOfPeople").style.display="none";
	setDefColortoErrorMsgToInvolvement();
	$('#errordivInvolvement').empty();
	return false;
}
function setDefColortoErrorMsgToInvolvement()
{
	$('#organizationInv').css("background-color","");
	$('#orgTypeId').css("background-color","");
	$('#rangeId').css("background-color","");
	$('#leadNoOfPeople').css("background-color","");

}
function hideInvolvement()
{
	document.getElementById("frmInvolvement").reset();
	document.getElementById("involvementId").value="";	
	document.getElementById("divInvolvement").style.display="none";
	return false;
}
function hideHonor()
{
	document.getElementById("frmHonor").reset();
	document.getElementById("honorId").value="";	
	document.getElementById("divHonor").style.display="none";
	return false;
}
function showHonorForm()
{
	document.getElementById("frmHonor").reset();
	document.getElementById("honorId").value="";	
	document.getElementById("divHonor").style.display="block";
	document.getElementById("honor").focus();
	setDefColortoErrorMsgToHoner();
	$('#errordivHonor').empty();
	return false;
}
function setDefColortoErrorMsgToHoner()
{
	$('#honor').css("background-color","");
	$('#honorYear').css("background-color","");

}
function showEmploymentForm()
{   
//	$("#empDateDiv").show();
//	//document.getElementById("empDateDiv").style.display="block";
// 	document.getElementById('empHisAnnSal').style.display="block";	alert("A");
//	if(parseInt(document.getElementById("districtIdForDSPQ").value) > 0 && (document.getElementById("districtIdForDSPQ").value==7800036 || document.getElementById("districtIdForDSPQ").value==1201470)){
//		document.getElementById("empHisAnnSal").style.display="none";
//	}alert("b");
//	 //document.getElementsByClassName("pritr").style.visibility="hidden";
//	 //document.getElementsById("pritr").style.display="none";
//	 
//	 
//	 $(".pritr").hide( );
//	 alert("d");
//	$(".mscitr").hide();
//	if($("#districtIdForDSPQ").length > 0 && (document.getElementById("districtIdForDSPQ").value==4218990 || document.getElementById("districtIdForDSPQ").value==1201470 || document.getElementById("districtIdForDSPQ").value==804800)){
//		$(".pritr").show();
//		$(".mscitr").show();
//		if(document.getElementById("districtIdForDSPQ").value==1201470){			
//			$(".mscitrReq").show();
//			$(".pritreq").hide();
//			$(".mscitrReq").hide();
//		}
//		
//		if(document.getElementById("districtIdForDSPQ").value==804800){		
//			$(".reasonForLeavdiv").hide();
//		}
//	}else if($("#districtIdForDSPQ").length > 0 && document.getElementById("districtIdForDSPQ").value==3904380){
//		$(".pritr").show();
//		$(".mscitr").show();
//		$(".tORR").hide();
//	}alert("B");
//	if(document.getElementById("districtIdForDSPQ").value==4218990 && $('#isPrinciplePhiladelphia').val()=="1"){
//		$("#empDateDiv").hide();
//		document.getElementById('empHisAnnSal').style.display="none";
//	}
//	$(".expsalaryInwork").hide();
//	if(document.getElementById("districtIdForDSPQ").value==7800040){
//		$(".expsalaryInwork").show();
//	}
//	
//	/*$("#teachingPosChk").hide();
//	if(document.getElementById("districtIdForDSPQ").value==1201470){
//		$("#teachingPosChk").show();
//	}*/
//	
//	document.getElementById("empPosition").value="0"	
//	document.getElementById("cityEmp").value="";
//	document.getElementById("stateOfOrg").value="";
//	document.getElementById("role").value="";
//	document.getElementById("fieldId2").value="";
//	document.getElementById("empOrg").value="";
//	document.getElementById("roleStartMonth").value="0";
//	document.getElementById("roleStartYear").value="0";
//	document.getElementById("roleEndMonth").value="0";
//	document.getElementById("roleEndYear").value="0";
//	document.getElementById("amount").value="";
//	//document.getElementById("primaryRespdiv").value="";
//	//document.getElementById("mostSignContdiv").value="";
//
//	var empRoleTypeId=document.getElementsByName("empRoleTypeId");
//	for(i=0;i<empRoleTypeId.length;i++)
//	{
//			empRoleTypeId[i].checked=false
//	}
//
//	document.getElementById("roleId").value="";	
//	document.getElementById("divEmployment").style.display="block";
//	document.getElementById("role").focus();
//	document.getElementById("currentlyWorking").checked = false;
//	document.getElementById("divToMonth").style.display="block";
//	document.getElementById("divToYear").style.display="block";
//	setDefColortoErrorMsgToEmployment();	
//	$('#primaryRespdiv').find(".jqte_editor").html("");
//	$('#mostSignContdiv').find(".jqte_editor").html("");
//	$('#reasonForLeadiv').find(".jqte_editor").html("");
//	$('#errordivEmployment').empty();
//	return false;
}
function hideEmploymentForm()
{
	//document.getElementById("frmEmployment").reset();
	document.getElementById("roleId").value="";	
	document.getElementById("divEmployment").style.display="none";
	return false;
}
function showAddDocumentsForm()
{
	document.getElementById("additionDocumentId").value="";
	document.getElementById("divAdditionalDocumentsRow").style.display="block";
	document.getElementById("divDocumnetDone").style.display="block";
	document.getElementById("documentName").value="";
	document.getElementById("uploadedDocument").value="";
	document.getElementById("uploadedDocumentHidden").value="";
	document.getElementById("divUploadedDocument").style.display="none";
	document.getElementById("removeUploadedDocumentSpan").style.display="none";
	document.getElementById("uploadedDocument").value="";
	document.getElementById("documentName").focus();
	$('#documentName').css("background-color","");
	$('#uploadedDocument').css("background-color","");
	$('#errAdditionalDocuments').empty();
	return false;
}
function resetAdditionalDocumentsForm()
{
	document.getElementById("divAdditionalDocumentsRow").style.display="none";
	document.getElementById("divDocumnetDone").style.display="none";
	document.getElementById("additionDocumentId").value="";
	document.getElementById("documentName").value="";
	document.getElementById("uploadedDocument").value="";
	document.getElementById("uploadedDocumentHidden").value="";
	document.getElementById("divUploadedDocument").style.display="none";
	document.getElementById("removeUploadedDocumentSpan").style.display="none";
	document.getElementById("additionalDocumentsForm").reset();
	return false;
}

function showGridAdditionalDocuments()
{
	PFCertifications.getAdditionalDocumentsGrid(dp_AdditionalDocuments_noOfRows,dp_AdditionalDocuments_page,dp_AdditionalDocuments_sortOrderStr,dp_AdditionalDocuments_sortOrderType,{  
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			//$('#divGridAdditionalDocuments').html(data);
			document.getElementById("divGridAdditionalDoc").innerHTML=data;
			applyScrollOnTbl_AdditionalDocuments();
		}});
}
function clearUploadedDocument()
{
	document.getElementById("uploadedDocument").value=null;
	document.getElementById("uploadedDocument").reset();
	$('#uploadedDocument').css("background-color","");	
}
function showVideoLinksForm()
{
	document.getElementById("frmvideoLinks").reset();
	document.getElementById("videolinkAutoId").value="";
	document.getElementById("videofile").value="";
	document.getElementById("video").style.display="block";
	document.getElementById("divvideoLinks").style.display="block";
	document.getElementById("videourl").focus();
	setDefColortoErrorMsgToVideoLinksForm();
	$('#errordivvideoLinks').empty();
	$('#video').empty();
	return false;
}

function setDefColortoErrorMsgToVideoLinksForm()
{
	$('#videourl').css("background-color","");
}


function hideVideoLinksForm()
{
	document.getElementById("frmvideoLinks").reset();
	document.getElementById("videolinkAutoId").value="";
	document.getElementById("divvideoLinks").style.display="none";
	return false;
}
function getVideoLinksGrid()
{
	PFCertifications.getVideoLinksGrid(dp_VideoLink_Rows,dp_VideoLink_page,dp_VideoLink_sortOrderStr,dp_VideoLink_sortOrderType, { 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			//$('#divDataVideoLinks').html(data);
			document.getElementById("divDataVideoLink").innerHTML=data;
			applyScrollOnTblVideoLinks();
		}});
}
function showElectronicReferencesForm()
{
	document.getElementById("frmElectronicReferences").reset();
	document.getElementById("elerefAutoId").value="";
	document.getElementById("divElectronicReferences").style.display="block";
	document.getElementById("removeref").style.display="none";
	document.getElementById("salutation").focus();
	setDefColortoErrorMsgToElectronicReferences();
	$('#errordivElectronicReferences').empty();
	$("#referenceDetailText").find(".jqte_editor").html("");
	var districtIdForDSPQ="";	
	if ($('#districtIdForDSPQ').length > 0) {
		districtIdForDSPQ=document.getElementById("districtIdForDSPQ").value;
	}
	if(districtIdForDSPQ==7800038){
		$('.nobleCssShow').show();
	}
	if(districtIdForDSPQ==4218990){
		$('.PHLreq').show();
		$('.onlyPHL').show();
	}
	if(districtIdForDSPQ==1201470 || districtIdForDSPQ==5510470){
		$('.PHLreq').show();
	}
	return false;
}
function setDefColortoErrorMsgToElectronicReferences()
{
	$('#salutation').css("background-color","");
	$('#firstName').css("background-color","");
	$('#lastName').css("background-color","");
	$('#designation').css("background-color","");
	$('#organization').css("background-color","");
	$('#email').css("background-color","");
	$('#contactnumber').css("background-color","");
	$('#pathOfReferences').css("background-color","");
}
function hideElectronicReferencesForm()
{
	document.getElementById("frmElectronicReferences").reset();
	document.getElementById("elerefAutoId").value="";
	document.getElementById("divElectronicReferences").style.display="none";
	return false;
}
function clearReferences()
{
	document.getElementById("pathOfReferenceFile").value=null;
	document.getElementById("pathOfReferenceFile").reset();
	$('#pathOfReferenceFile').css("background-color",""); 
}
function getElectronicReferencesGrid()
{
	var districtIdForDSPQ="";	
	if ($('#districtIdForDSPQ').length > 0) {
		districtIdForDSPQ=document.getElementById("districtIdForDSPQ").value;
	}
	
	if(districtIdForDSPQ==7800038)
	PFCertifications.getElectronicReferencesGridNoble(dp_Reference_noOfRows,dp_Reference_page,dp_Reference_sortOrderStr,dp_Reference_sortOrderType, { 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
		//$('#divDataElectronicReferences').html(data);
		document.getElementById("divDataElectronicReferenc").innerHTML=data;
		applyScrollOnTblEleRef();
		}});
	else
		PFCertifications.getElectronicReferencesGrid(dp_Reference_noOfRows,dp_Reference_page,dp_Reference_sortOrderStr,dp_Reference_sortOrderType, { 
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{
			//$('#divDataElectronicReferences').html(data);
			document.getElementById("divDataElectronicReferenc").innerHTML=data;
			applyScrollOnTblEleRef();
			}});
}
var hiddenId="";
function getDegreeMasterAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	$('#degreeName').mousedown(function(e) {
	if( (event.keyCode == 1) ){
        alert(resourceJSON.msgleftbutton);
    }if( (event.keyCode== 3) ) {
        alert(resourceJSON.msgrightbutton);
    }else if( (event.keyCode== 2) ) {
        alert(resourceJSON.msgmiddlebutton); 
    }
	});
	
	var gedflag=false;
	if($('#degreeName').val()=="High School or GED"){
		gedflag=true;
	}
		
		if($('#degreeName').val()=="No Degree" || gedflag==true){
			$('.nondgrReq').hide();
			if(gedflag==true){
				$('.nondgrReq1').show();
				$('.nondgrReq2').show();
			}else{
				$('.nondgrReq1').hide();
				$('.nondgrReq2').hide();
			}
		}else{
			$('.nondgrReq').show();
			$('.nondgrReq1').show();
			$('.nondgrReq2').hide();
		}	
	
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		//searchArray = getDegreeMasterArray(txtSearch.value);
		//fatchData(txtSearch,searchArray,txtId,txtdivid);
		document.getElementById("universityName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getDegreeMasterArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getDegreeMasterArray(degreeName){
	var searchArray = new Array();
	DWRAutoComplete.getDegreeMasterList(degreeName,{ 
		async: false,		
		callback: function(data){
		hiddenDataArray = new Array();
		showDataArray = new Array();
		for(i=0;i<data.length;i++){
			searchArray[i]=data[i].degreeName;
			hiddenDataArray[i]=data[i].degreeId;
			showDataArray[i]=data[i].degreeName;
			degreeTypeArray[i]=data[i].degreeType;
		}
	}
	});	

	return searchArray;
}
var fatchData= function(txtSearch,searchArray,txtId,txtdivid){
	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i] + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
				
			}
			
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}
function hideDegreeMasterDiv(dis,hiddenId,divId)
{
	//setDefColortoErrorMsg_Academic();
	//$('#errordiv').empty();
	document.getElementById("degreeType").value="";
	document.getElementById("divGPARow").style.display="none";
	
	
	//alert(hiddenDataArray);
	//alert(degreeTypeArray);
	//alert(index)
	//alert(degreeTypeArray[index]);
	//alert(hiddenDataArray[index])
	if(parseInt(length)>0)
	{
		if(index==-1)
		{
			index=0;
		}
		//alert(hiddenId);
		//alert(hiddenDataArray[index]);
		//if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId))
		//{
			//alert("Enter in ...")
			document.getElementById(hiddenId).value=hiddenDataArray[index];			
			var degreeName = document.getElementById("degreeName").value;
			var degreeType = document.getElementById("degreeType").value=degreeTypeArray[index];
			
			if(trim(degreeName)!="" && degreeType=="B")
			{
				document.getElementById("divGPARow").style.display="block";
				document.getElementById("divGPARowOther").style.display="none";
				
			}
			else
			{
				document.getElementById("divGPARow").style.display="none";
				document.getElementById("divGPARowOther").style.display="block";
			}
			//alert("degreeType"+degreeType)
			
			
		//}
		
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
			document.getElementById("degreeType").value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			
		}
		
	}
	else
	{
		if(document.getElementById(hiddenId))
		{
			document.getElementById(hiddenId).value="";
		}
		//dis.value="";
		if(dis.value!="")
		{
			var cnt=0;
			var focs=0;	
			$('#errordiv_AcademicForm').empty();	
			setDefColortoErrorMsg_Academic();
			$('#errordiv_AcademicForm').append("&#149; "+resourceJSON.PlzEtrVldDegree+"<br>");
			if(focs==0)
				$('#degreeName').focus();
			
			$('#degreeName').css("background-color",txtBgColor);
			cnt++;focs++;	
		}
			
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	
		if($('#degreeName').val()=="No Degree"|| $('#degreeName').val()=="High School or GED"){
			$('.nondgrReq').hide();
			if($('#degreeName').val()=="High School or GED"){
				$('.nondgrReq1').show();
				$('.nondgrReq2').show();
			}else{
				$('.nondgrReq1').hide();
				$('.nondgrReq2').hide();
			}
		}else{
			$('.nondgrReq').show();
			$('.nondgrReq1').show();
			$('.nondgrReq2').hide();
		}
	
	index = -1;
	length = 0;
}

function mouseOverChk(txtdivid,txtboxId)
{	
	for(var i=0;i<length;i++)
	{
		$('#divResult'+txtdivid+i).mouseover({param1: i,param2:txtdivid, param3:txtboxId}, fireMouseOverEvent);
	}
}
function getUniversityAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38)
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13)
	{
		
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		//searchArray = getUniversityArray(txtSearch.value);
		//fatchData(txtSearch,searchArray,txtId,txtdivid);
		document.getElementById("fieldName").focus();
		
	} 
	else if(event.keyCode==9)
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getUniversityArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}
function getUniversityArray(universityName){
	
	var searchArray = new Array();
	DWRAutoComplete.getUniversityMasterList(universityName,{ 
		async: false,		
		callback: function(data){
		hiddenDataArray = new Array();
		showDataArray = new Array();
		for(i=0;i<data.length;i++){
			searchArray[i]=data[i].universityName;
			hiddenDataArray[i]=data[i].universityId;
			showDataArray[i]=data[i].universityName;
		}
	}
	});	

	return searchArray;
}
function hideUniversityDiv(dis,hiddenId,divId)
{
	//setDefColortoErrorMsg_Academic();
	//$('#errordiv').empty();	
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		//alert(hiddenId);
		//alert(hiddenDataArray[index]);
		//if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		//}
		
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
		//dis.value="";
		if(dis.value!="")
		{
			var cnt=0;
			var focs=0;	
			$('#errordiv_AcademicForm').empty();	
			setDefColortoErrorMsg_Academic();
			$('#errordiv_AcademicForm').append("&#149; "+resourceJSON.msgvalidschool+"<br>");
			if(focs==0)
				$('#universityName').focus();
			
			$('#universityName').css("background-color",txtBgColor);
			cnt++;focs++;	
		}
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}


function getFieldOfStudyAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38)
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13)
	{
		
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		//searchArray = getFieldOfStudyArray(txtSearch.value);
		//fatchData(txtSearch,searchArray,txtId,txtdivid);
		document.getElementById("attendedInYear").focus();
		
	} 
	else if(event.keyCode==9)
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getFieldOfStudyArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getFieldOfStudyArray(fieldName){
	
	var searchArray = new Array();
	DWRAutoComplete.getFieldOfStudyList(fieldName,{ 
		async: false,		
		callback: function(data){
		hiddenDataArray = new Array();
		showDataArray = new Array();
		for(i=0;i<data.length;i++){
			searchArray[i]=data[i].fieldName;
			hiddenDataArray[i]=data[i].fieldId;
			showDataArray[i]=data[i].fieldName;
		}
	}
	});	

	return searchArray;
}
function checkGED(){
	if($('#degreeName').val()=="High School or GED"){
		//document.getElementById('universityName').disabled=true;
		document.getElementById('fieldName').disabled=true;
		//document.getElementById('gpaCumulative1').disabled=true;
	}else{
		document.getElementById('universityName').disabled=false;
		document.getElementById('fieldName').disabled=false;
		document.getElementById('gpaCumulative1').disabled=false;
	}
}
function clearTranscript()
{
	document.getElementById("pathOfTranscript").value=null;
	$('#pathOfTranscript').css("background-color","");	
}
function chkForEnter_Academic(evt)
{	
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==13)
	{
		insertOrUpdate_Academic();
	}	
}
function chkInternational()
{
      if (document.getElementById('international').checked)
      {
             document.getElementById("crequired").innerHTML = ""; 
             document.getElementById("crequired2").innerHTML = "";
       }
      else
      {
            document.getElementById("crequired").innerHTML ="*";
            document.getElementById("crequired2").innerHTML ="*";
      }
}
function removeTranscript()
{
	
	var academicId = document.getElementById("academicId").value;
	if(window.confirm(resourceJSON.msgconfirmdeletescript))
	{
		PFAcademics.removeTranscript(academicId,
		{ 
			async: false,
			errorHandler:handleError,
			callback: function(data)
			{	
				//alert(data)
				showGridAcademics();
				document.getElementById("divResumeSection").style.display="none";
				
			}
		});
	}
}
function setDefColortoErrorMsg_Academic()
{
	//$('#divServerError').css("background-color","");	
	$('#degreeName').css("background-color","");
	$('#universityName').css("background-color","");
	$('#fieldName').css("background-color","");
	$('#attendedInYear').css("background-color","");
	$('#leftInYear').css("background-color","");
	$('#pathOfTranscript').css("background-color","");	
	$('#gpaFreshmanYear').css("background-color","");
	$('#gpaJuniorYear').css("background-color","");
	$('#gpaSophomoreYear').css("background-color","");
	$('#gpaSeniorYear').css("background-color","");
	$('#gpaCumulative').css("background-color","");
	$('#gpaCumulative1').css("background-color","");
	
}


function showRecordToForm(academicId,statusCheck)
{

	document.getElementById("frmAcadamin").reset();	
	document.getElementById("divGPARowOther").style.display="none";
	document.getElementById("international2").checked=false;
	document.getElementById("international").checked=false;
	PFAcademics.showEditAcademics(academicId,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
	{
		//alert(data.leftInYear)
		//alert(data.degreeId.degreeId)
		//alert(data.degreeId.degreeName)
	
		document.getElementById("divAcademicRow").style.display="block";
		document.getElementById("divDone").style.display="block";
			
		document.getElementById("academicId").value=data.academicId;
		document.getElementById("degreeName").value=data.degreeId.degreeName;
		document.getElementById("degreeId").value=data.degreeId.degreeId;
		document.getElementById("degreeType").value=data.degreeId.degreeType;
		
		if(data.universityId!=null){
		document.getElementById("universityId").value=data.universityId.universityId;		
		document.getElementById("universityName").value=data.universityId.universityName;
		}
		
		if(data.fieldId!=null){
		document.getElementById("fieldId").value=data.fieldId.fieldId;
		document.getElementById("fieldName").value=data.fieldId.fieldName;
		}
		
		
		if(data.degreeId.degreeType=="M" && data.international){
			document.getElementById("international2").checked=data.international;
		}else if(data.degreeId.degreeType=="A" && data.international){
			document.getElementById("international2").checked=data.international;
		}else if(data.degreeId.degreeType=="D" && data.international){
			document.getElementById("international2").checked=data.international;
		}else{
			document.getElementById("international").checked=data.international;
		}
		
		var isMiamiChk=document.getElementById("isMiami").value;
		
		//alert(data.pathOfTranscript)
		
		//alert(" statusCheck :: "+statusCheck);
		
		if(isMiamiChk=='false')
		{
			if(statusCheck=='V')
			{
				document.getElementById("degreeName").disabled=true;
				document.getElementById("degreeId").disabled=true;
				document.getElementById("degreeType").disabled=true;
				document.getElementById("universityId").disabled=true;
				document.getElementById("universityName").disabled=true;
				document.getElementById("fieldId").disabled=true;
				document.getElementById("fieldName").disabled=true;
				document.getElementById("attendedInYear").disabled=true;
				document.getElementById("leftInYear").disabled=true;
				
				//alert(data.pathOfTranscript);
				if(data.pathOfTranscript!=null && data.pathOfTranscript!="")
				{
					document.getElementById("divResumeSection").style.display="block";
					document.getElementById("divResumerName").innerHTML="<a href='javascript:void(0)' id='hrefEditTrans' onclick=\"downloadTranscript('"+data.academicId+"','hrefEditTrans');if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;\">"+data.pathOfTranscript+"</a>";
				}
				else
				{
					document.getElementById("divResumeSection").style.display="none";
					//document.getElementById("divResumerName").innerHTML = "";
				}
			}
			else
			{
				document.getElementById("degreeName").disabled=false;
				document.getElementById("degreeId").disabled=false;
				document.getElementById("degreeType").disabled=false;
				
				document.getElementById("universityId").disabled=false;
				document.getElementById("universityName").disabled=false;
				
				document.getElementById("fieldId").disabled=false;
				document.getElementById("fieldName").disabled=false;
				document.getElementById("attendedInYear").disabled=false;
				document.getElementById("leftInYear").disabled=false;
				
				if(data.pathOfTranscript!=null && data.pathOfTranscript!="")
				{
					document.getElementById("divResumeSection").style.display="block";
					document.getElementById("divResumerName").innerHTML="<a href='javascript:void(0)' id='hrefEditTrans' onclick=\"downloadTranscript('"+data.academicId+"','hrefEditTrans');if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;\">"+data.pathOfTranscript+"</a>";
				}
				else
				{
					document.getElementById("divResumeSection").style.display="none";
					//document.getElementById("divResumerName").innerHTML = "";
				}
			}
			
		}
		
		if(data.attendedInYear!=null)
		document.getElementById("attendedInYear".toString()+(data.attendedInYear)).selected=true;
		if(data.leftInYear!=null)
		document.getElementById("leftInYear".toString()+(data.leftInYear)).selected=true;
		
		if(document.getElementById("degreeType").value=="B" || document.getElementById("degreeType").value=="b")
		{
			document.getElementById("divGPARow").style.display="block";
			document.getElementById("gpaFreshmanYear").value =  data.gpaFreshmanYear;
			document.getElementById("gpaJuniorYear").value =  data.gpaJuniorYear;     
			document.getElementById("gpaSophomoreYear").value =  data.gpaSophomoreYear;     
			document.getElementById("gpaSeniorYear").value =  data.gpaSeniorYear;     
			document.getElementById("gpaCumulative").value =  data.gpaCumulative;
			
			if(isMiamiChk=='false' && statusCheck=='V')
			{
				document.getElementById("gpaFreshmanYear").disabled=true;
				document.getElementById("gpaJuniorYear").disabled=true;     
				document.getElementById("gpaSophomoreYear").disabled=true;     
				document.getElementById("gpaSeniorYear").disabled=true;     
				document.getElementById("gpaCumulative").disabled=true;
			}
			else
			{
				document.getElementById("gpaFreshmanYear").disabled=false;
				document.getElementById("gpaJuniorYear").disabled=false;     
				document.getElementById("gpaSophomoreYear").disabled=false;     
				document.getElementById("gpaSeniorYear").disabled=false;     
				document.getElementById("gpaCumulative").disabled=false;
			}
		}else if(document.getElementById("degreeType").value=="ND"){
			document.getElementById("divGPARow").style.display="none";
			document.getElementById("divGPARowOther").style.display="block";
			document.getElementById("gpaCumulative1").value =  data.gpaCumulative;
		}
		else
		{
			document.getElementById("divGPARow").style.display="none";
			document.getElementById("divGPARowOther").style.display="block";
			document.getElementById("gpaCumulative1").value =  data.gpaCumulative;
			
			if(isMiamiChk=='false' && statusCheck=='V')
			{
				document.getElementById("divGPARowOther").disabled=true;
				document.getElementById("gpaCumulative1").disabled=true;
			}
			else
			{
				document.getElementById("divGPARowOther").disabled=false;
				document.getElementById("gpaCumulative1").disabled=false;
			}
		}
		if(data.degreeId.degreeName=="High School or GED"){
			document.getElementById('universityName').disabled=true;
			document.getElementById('fieldName').disabled=true;
			document.getElementById('gpaCumulative1').disabled=true;
		}else{
			document.getElementById('universityName').disabled=false;
			document.getElementById('fieldName').disabled=false;
			document.getElementById('gpaCumulative1').disabled=false;
		}
		
		document.getElementById("degreeName").focus();
	
	}})
	return false;
}
function downloadTranscript(academicId, linkId)
{	
		PFAcademics.downloadTranscript(academicId,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	if(data=="")
			{
				data="javascript:void(0)";
			}
			else if(data.indexOf(".doc")!=-1)
			{
				document.getElementById("ifrmTrans").src=data;
			}
			else
			{
				document.getElementById(linkId).href = data;
			}
			return false;
		}});
}
var upArrowKey = function(txtdivid){
	
	if(txtdivid)
	{
		if(index>0){
			for(var i=0;i<length;i++){

				var div_id=document.getElementById('divResult'+txtdivid+i);
				if(div_id)
				{
					if(div_id.className=='over')
						index=div_id.id.split('divResult'+txtdivid)[1];
					div_id.className='normal';
				}
			}
			index--;
			if(document.getElementById('divResult'+txtdivid+index))
				document.getElementById('divResult'+txtdivid+index).className='over';
			if(txtid && document.getElementById('divResult'+txtdivid+index))
			{
				document.getElementById(txtid).value=
					document.getElementById('divResult'+txtdivid+index).innerHTML;
				selectFirst=document.getElementById('divResult'+txtdivid+index).innerHTML;
			}
		}
	}
}
function fireMouseOverEvent(event)
{
	for(var i=0;i<length;i++)
	{	
		document.getElementById('divResult'+event.data.param2+i).className='normal';
	}
    document.getElementById('divResult'+event.data.param2+event.data.param1).className='over';	       
   	document.getElementById(event.data.param3).value= $('#divResult'+event.data.param2+event.data.param1).text();
    index=event.data.param1;
}
var downArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index<length-1){
			for(var i=0;i<10;i++){
				{
					if(document.getElementById('divResult'+txtdivid+i))
						var div_id=document.getElementById('divResult'+txtdivid+i);
					if(div_id)
					{
						if(div_id.className=='over')
							index=div_id.id.split('divResult'+txtdivid)[1];
						div_id.className='normal';
					}
				}
			}
			index++;
			if(document.getElementById('divResult'+txtdivid+index))
			{
				var div_id=document.getElementById('divResult'+txtdivid+index);
				div_id.className='over';
				document.getElementById(txtid).value = div_id.innerHTML;
				selectFirst=div_id.innerHTML;
			}
		}
	}
}

var upArrowKey = function(txtdivid){
	
	if(txtdivid)
	{
		if(index>0){
			for(var i=0;i<length;i++){

				var div_id=document.getElementById('divResult'+txtdivid+i);
				if(div_id)
				{
					if(div_id.className=='over')
						index=div_id.id.split('divResult'+txtdivid)[1];
					div_id.className='normal';
				}
			}
			index--;
			if(document.getElementById('divResult'+txtdivid+index))
				document.getElementById('divResult'+txtdivid+index).className='over';
			if(txtid && document.getElementById('divResult'+txtdivid+index))
			{
				document.getElementById(txtid).value=
					document.getElementById('divResult'+txtdivid+index).innerHTML;
				selectFirst=document.getElementById('divResult'+txtdivid+index).innerHTML;
			}
		}
	}
}
function hideFeildOfStudyDiv(dis,hiddenId,divId)
{
	//setDefColortoErrorMsg_Academic();
	//$('#errordiv').empty();
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		//alert(hiddenId);
		//alert(hiddenDataArray[index]);
		//if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		//}
		
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
		//dis.value="";
		if(dis.value!="")
		{
			var cnt=0;
			var focs=0;	
			$('#errordiv_AcademicForm').empty();	
			setDefColortoErrorMsg_Academic();
			$('#errordiv_AcademicForm').append("&#149; "+resourceJSON.msgvalidstudy+"<br>");
			if(focs==0)
				$('#fieldName').focus();
			
			$('#fieldName').css("background-color",txtBgColor);
			cnt++;focs++;	
		}
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}
function clearCertification(){
	document.getElementById("pathOfCertificationFile").value=null;
	document.getElementById("pathOfCertificationFile").reset();
	$('#pathOfCertificationFile').css("background-color","");
	}

function showEditForm(id)
{
	var certificate = {certId:null, certificationStatusMaster:null,stateMaster:null,yearReceived:null,certType:null,pkOffered:null,kgOffered:null,g01Offered:null,g02Offered:null,g03Offered:null,g04Offered:null,g05Offered:null,g06Offered:null,g07Offered:null,g08Offered:null,g09Offered:null,g10Offered:null,g11Offered:null,g12Offered:null,canServeAsSubTeacher:null};
	
	PFCertifications.showEditForm(id,
	{ 
		async: false,
		errorHandler:handleError,
		callback: function(data)
		{
			showForm_Certification();
						
			document.getElementById("certId").value=data.certId;
			if(data.stateMaster!=null && data.stateMaster.stateId!=null){
				document.getElementById("stateMaster").value=data.stateMaster.stateId;
			}
			
			var districtIdForDSPQ="";	
			if ($('#districtIdForDSPQ').length > 0) {
				districtIdForDSPQ=document.getElementById("districtIdForDSPQ").value;
			}
			if(districtIdForDSPQ==7800038){
				$(".certText").find(".jqte_editor").html(data.certText);
			}
			
			if(districtIdForDSPQ==7800040)
				$('#IEINNumber').val(data.ieinNumber);
			
			if(data.certificationStatusMaster!=null)
				document.getElementById("certificationStatusMaster").value=data.certificationStatusMaster.certificationStatusId;
			else
				document.getElementById("certificationStatusMaster").value="";
			
			if(data.certificationTypeMaster!=null)
				document.getElementById("certificationtypeMaster").value=data.certificationTypeMaster.certificationTypeMasterId;
			else
				document.getElementById("certificationtypeMaster").value="0";
			
			if(data.doeNumber==0)
			{
				document.getElementById("doenumber").value="";
			}	
			else
			{
				document.getElementById("doenumber").value=data.doeNumber;
			}
			
			if(data.yearExpired==0 || data.yearExpired==null || data.yearExpired=='')
			{
				document.getElementById("yearexpires").text="";
			}
			else
			{
				document.getElementById("yearexpires").value=data.yearExpired;
			}	
			
			
			document.getElementById("yearReceived").value=data.yearReceived;
			document.getElementById("certUrl").value=data.certUrl;
			dwr.util.setValues(data);
			if(data.certificateTypeMaster!=null)
			{
				document.getElementById("certType").value=data.certificateTypeMaster.certType;
				document.getElementById("certificateTypeMaster").value=data.certificateTypeMaster.certTypeId;
			}
			else
			{
				document.getElementById("certType").value="";
				document.getElementById("certificateTypeMaster").value="";
			}
			
			if(data.pathOfCertification!=null && data.pathOfCertification!="")
			{	
				//alert("yes");
				document.getElementById("removeCert").style.display="block";
				document.getElementById("divCertName").style.display="block";
				document.getElementById("pathOfCertification").value=data.pathOfCertification;
				
				document.getElementById("divCertName").innerHTML="<a href='javascript:void(0)' id='hrefEditRef' onclick=\"downloadCertification('"+data.certId+"','hrefEditRef');" +
				"if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0');" +
				"return false;\">"+data.pathOfCertification+"</a>";
			}else{
				//alert("No");
				document.getElementById("divCertName").style.display="none";
				document.getElementById("removeCert").style.display="none";
				document.getElementById("pathOfCertificationFile").value="";
				document.getElementById("pathOfCertification").value="";
			}
			fieldsDisable(data.certificationStatusMaster.certificationStatusId);
			getPraxis();
			return false;
		}
	});
	return false;
}
function fieldsDisable(value){
	
	if(value==5){
		$('#errordiv_Certification').empty();
		$( "#certificationDiv" ).find( "span.required" ).css( "color", "white" );
		setDefColortoErrorMsg_Certification();
		document.getElementById("certificationtypeMaster").value=0;
		document.getElementById("stateMaster").value="";
		document.getElementById("yearReceived").value="";
		document.getElementById("yearexpires").value="";
		document.getElementById("certType").value="";
		document.getElementById("doenumber").value="";
		document.getElementById("certUrl").value="";
		document.getElementById("pathOfCertificationFile").value="";
		document.getElementById("pkOffered").checked=false;
		document.getElementById("kgOffered").checked=false;
		document.getElementById("g01Offered").checked=false;
		document.getElementById("g02Offered").checked=false;
		document.getElementById("g03Offered").checked=false;
		document.getElementById("g04Offered").checked=false;
		document.getElementById("g05Offered").checked=false;
		document.getElementById("g06Offered").checked=false;
		document.getElementById("g07Offered").checked=false;
		document.getElementById("g08Offered").checked=false;
		document.getElementById("g09Offered").checked=false;
		document.getElementById("g10Offered").checked=false;
		document.getElementById("g11Offered").checked=false;
		document.getElementById("g12Offered").checked=false;
		document.getElementById("clearLink").style.display="none";
		getPraxis();
		
		document.getElementById("certificationtypeMaster").disabled=true;
		document.getElementById("stateMaster").disabled=true;		
		document.getElementById("yearReceived").disabled=true;
		document.getElementById("yearexpires").disabled=true;
		document.getElementById("certType").disabled=true;
		document.getElementById("doenumber").disabled=true;
		document.getElementById("certUrl").disabled=true;
		document.getElementById("pathOfCertificationFile").disabled=true;
		document.getElementById("pkOffered").disabled=true;
		document.getElementById("kgOffered").disabled=true;
		document.getElementById("g01Offered").disabled=true;
		document.getElementById("g02Offered").disabled=true;
		document.getElementById("g03Offered").disabled=true;
		document.getElementById("g04Offered").disabled=true;
		document.getElementById("g05Offered").disabled=true;
		document.getElementById("g06Offered").disabled=true;
		document.getElementById("g07Offered").disabled=true;
		document.getElementById("g08Offered").disabled=true;
		document.getElementById("g09Offered").disabled=true;
		document.getElementById("g10Offered").disabled=true;
		document.getElementById("g11Offered").disabled=true;
		document.getElementById("g12Offered").disabled=true;
		document.getElementById("clearLink").style.display="none";
		
	}else{
		$( "#certificationDiv" ).find( "span.required" ).css( "color", "red" );
		document.getElementById("clearLink").style.display="block";
		document.getElementById("certificationtypeMaster").disabled=false;
		document.getElementById("stateMaster").disabled=false;		
		document.getElementById("yearReceived").disabled=false;
		document.getElementById("yearexpires").disabled=false;
		document.getElementById("certType").disabled=false;
		document.getElementById("doenumber").disabled=false;
		document.getElementById("certUrl").disabled=false;
		document.getElementById("pathOfCertificationFile").disabled=false;
		document.getElementById("pkOffered").disabled=false;
		document.getElementById("kgOffered").disabled=false;
		document.getElementById("g01Offered").disabled=false;
		document.getElementById("g02Offered").disabled=false;
		document.getElementById("g03Offered").disabled=false;
		document.getElementById("g04Offered").disabled=false;
		document.getElementById("g05Offered").disabled=false;
		document.getElementById("g06Offered").disabled=false;
		document.getElementById("g07Offered").disabled=false;
		document.getElementById("g08Offered").disabled=false;
		document.getElementById("g09Offered").disabled=false;
		document.getElementById("g10Offered").disabled=false;
		document.getElementById("g11Offered").disabled=false;
		document.getElementById("g12Offered").disabled=false;
	}	
}
function getPraxis()
{
	var districtIdForDSPQ="";
	if ($('#districtIdForDSPQ').length > 0) {
		districtIdForDSPQ=document.getElementById("districtIdForDSPQ").value;
	}

	if(districtIdForDSPQ!=4218990){
	var statemaster = {stateId:dwr.util.getValue("stateMaster")};
	PFCertifications.getPraxis(statemaster,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			if(data!=null)
			{
				$("#praxisArea").show();
				$("#reading").html(data.praxisIReading);
				$("#writing").html(data.praxisIWriting);
				$("#maths").html(data.praxisIMathematics);
			}else
			{
				$("#praxisArea").hide();
				$("#reading").html("");
				$("#writing").html("");
				$("#maths").html("");
			}
	}});
	}
}
function setDefColortoErrorMsg_Certification()
{
	$('#certificationtypeMaster').css("background-color","");
	$('#stateMaster').css("background-color","");
	$('#yearReceived').css("background-color","");
	$('#yearexpires').css("background-color","");
	$('#certType').css("background-color","");
	$('#certName').css("background-color","");
	$('#certificationStatusMaster').css("background-color","");
	$('#certUrl').css("background-color","");
	var districtIdForDSPQ="";	
	if ($('#districtIdForDSPQ').length > 0) {
		districtIdForDSPQ=document.getElementById("districtIdForDSPQ").value;
	}
	
	if(districtIdForDSPQ==7800040)
	{
		$('#IEINNumber').css("background-color","");
	}
	
}
function showEditFormInvolvement(id)
{
	PFExperiences.showEditFormInvolvement(id,
			{ 
		async: false,
		errorHandler:handleError,
		callback: function(data)
		{		
		showInvolvementForm();
		//alert(data.leadNoOfPeople)

		if(data.leadNoOfPeople!=null)
		{
			document.getElementById("rdo1").click();
		}
		else
		{
			document.getElementById("divLeadNoOfPeople").style.display="none";
			document.getElementById("leadNoOfPeople").value="";		
		}
		document.getElementById("involvementId").value=id;
		document.getElementById("organizationInv").value=data.organization;
		document.getElementById("orgTypeId").value=data.orgTypeMaster.orgTypeId;
		document.getElementById("rangeId").value=data.peopleRangeMaster.rangeId;
		document.getElementById("leadNoOfPeople").value=data.leadNoOfPeople;


		}
			});

	return false;
}
function showEditHonors(id)
{
	PFExperiences.showEditHonors(id,
			{ 
		async: false,
		errorHandler:handleError,
		callback: function(data)
		{		
		showHonorForm();


		document.getElementById("honorId").value=id;
		document.getElementById("honor").value=data.honor;
		document.getElementById("honorYear").value=data.honorYear;

		}
			});

	return false;
}
function showEmploymentForm()
{
	$("#empDateDiv").show();
	document.getElementById('empHisAnnSal').style.display="block";	
	if($("#districtIdForDSPQ").length > 0 && (document.getElementById("districtIdForDSPQ").value==7800036 || document.getElementById("districtIdForDSPQ").value==1201470)){
		document.getElementById('empHisAnnSal').style.display="none";
	}
	$(".pritr").hide();
	$(".mscitr").hide();
	if($("#districtIdForDSPQ").length > 0 && (document.getElementById("districtIdForDSPQ").value==4218990 || document.getElementById("districtIdForDSPQ").value==1201470 || document.getElementById("districtIdForDSPQ").value==804800)){
		$(".pritr").show();
		$(".mscitr").show();
		if(document.getElementById("districtIdForDSPQ").value==1201470){			
			$(".mscitrReq").show();
			$(".pritreq").hide();
			$(".mscitrReq").hide();
		}
		
		if(document.getElementById("districtIdForDSPQ").value==804800){		
			$(".reasonForLeavdiv").hide();
		}
	}else if($("#districtIdForDSPQ").length > 0 && document.getElementById("districtIdForDSPQ").value==3904380){
		$(".pritr").show();
		$(".mscitr").show();
		$(".tORR").hide();
	}
	if(document.getElementById("districtIdForDSPQ").value==4218990 && $('#isPrinciplePhiladelphia').val()=="1"){
		$("#empDateDiv").hide();
		document.getElementById('empHisAnnSal').style.display="none";
	}
	$(".expsalaryInwork").hide();
	if(document.getElementById("districtIdForDSPQ").value==7800040){
		$(".expsalaryInwork").show();
	}
	
	/*$("#teachingPosChk").hide();
	if(document.getElementById("districtIdForDSPQ").value==1201470){
		$("#teachingPosChk").show();
	}*/
	
	document.getElementById("empPosition").value="0"	
	document.getElementById("cityEmp").value="";
	document.getElementById("stateOfOrg").value="";
	document.getElementById("role").value="";
	document.getElementById("fieldId2").value="";
	document.getElementById("empOrg").value="";
	document.getElementById("roleStartMonth").value="0";
	document.getElementById("roleStartYear").value="0";
	document.getElementById("roleEndMonth").value="0";
	document.getElementById("roleEndYear").value="0";
	document.getElementById("amount").value="";
	//document.getElementById("primaryRespdiv").value="";
	//document.getElementById("mostSignContdiv").value="";

	var empRoleTypeId=document.getElementsByName("empRoleTypeId");
	for(i=0;i<empRoleTypeId.length;i++)
	{
			empRoleTypeId[i].checked=false
	}

	document.getElementById("roleId").value="";	
	document.getElementById("divEmployment").style.display="block";
	document.getElementById("role").focus();
	document.getElementById("currentlyWorking").checked = false;
	document.getElementById("divToMonth").style.display="block";
	document.getElementById("divToYear").style.display="block";
	setDefColortoErrorMsgToEmployment();	
	$('#primaryRespdiv').find(".jqte_editor").html("");
	$('#mostSignContdiv').find(".jqte_editor").html("");
	$('#reasonForLeadiv').find(".jqte_editor").html("");
	$('#errordivEmployment').empty();
	return false;
}

function hideEmploymentForm()
{
	//document.getElementById("frmEmployment").reset();
	document.getElementById("roleId").value="";	
	document.getElementById("divEmployment").style.display="none";
	return false;
}

function setDefColortoErrorMsgToEmployment()
{
	$('#role').css("background-color","");
	$('#empPosition').css("background-color", "")
	$('#fieldId2').css("background-color","");
	$('#empOrg').css("background-color","");
	$('#roleStartMonth').css("background-color","");
	$('#roleStartYear').css("background-color","");
	$('#roleEndMonth').css("background-color","");
	$('#roleEndYear').css("background-color","");
//	$('#currencyId').css("background-color","");
	$('#amount').css("background-color","");	
	$('#primaryRespdiv').find(".jqte_editor").css("background-color","");
	$('#mostSignContdiv').find(".jqte_editor").css("background-color","");
	$('#reasonForLeadiv').find(".jqte_editor").css("background-color","");
	//shadab
	
	$('#cityEmp').css("background-color","");
	$('#stateOfOrg').css("background-color","");
}
function showEditFormEmployment(id)
{
	PFExperiences.showEditFormEmployment(id,
		{ 
			async: false,
			errorHandler:handleError,
			callback: function(data)
			{
					showEmploymentForm();
					document.getElementById("roleId").value=id;	
					
					document.getElementById("empPosition").value=data.position;
				  
					document.getElementById("role").value=data.role;
					document.getElementById("fieldId2").value=data.fieldMaster.fieldId;
					document.getElementById("amount").value=data.amount;
					document.getElementById("empOrg").value=data.organization;
					
					document.getElementById("cityEmp").value=data.city;
					document.getElementById("stateOfOrg").value=data.state;
					//alert(data.organization);
					if(data.currentlyWorking)
					{
						document.getElementById("currentlyWorking").click();
						document.getElementById("roleStartMonth").value=data.roleStartMonth;
						document.getElementById("roleStartYear").value=data.roleStartYear;
					}
					else
					{
						document.getElementById("currentlyWorking").checked=false;
						document.getElementById("divToMonth").style.display="block";
						document.getElementById("divToYear").style.display="block";
						document.getElementById("roleStartMonth").value=data.roleStartMonth;
						document.getElementById("roleStartYear").value=data.roleStartYear;
						
			
						document.getElementById("roleEndMonth").value=data.roleEndMonth;
						document.getElementById("roleEndYear").value=data.roleEndYear;
					}
			try {
				var empRoleTypeId=document.getElementsByName("empRoleTypeId");
				
				if(data.empRoleTypeMaster.empRoleTypeId!=null)
				for(i=0;i<empRoleTypeId.length;i++)
				{
					if(empRoleTypeId[i].value==data.empRoleTypeMaster.empRoleTypeId)
						empRoleTypeId[i].checked=true
		
				}
			} catch (e) {
				// TODO: handle exception
			}
					
			
					//document.getElementById("primaryResp").value=data.primaryResp;
					//document.getElementById("mostSignCont").value=data.mostSignCont;
					$('#primaryRespdiv').find(".jqte_editor").html(data.primaryResp);
					$('#mostSignContdiv').find(".jqte_editor").html(data.mostSignCont);
					$('#reasonForLeadiv').find(".jqte_editor").html(data.reasonForLeaving);
					/*document.getElementById("certificateNameMaster").value=data.certificateNameMaster.certNameId;
						document.getElementById("certName").value=data.certificateNameMaster.certName;
					 */
					return false;
			
					//document.getElementById("stateMaster").value=data.stateMaster.stateId;
					//document.getElementById("").value="";
					//document.getElementById("").value="";
					//document.getElementById("").value="";
					//document.getElementById("").value="";
					//document.getElementById("").value="";
			
					}
			});

	return false;

}

function showEditTeacherAdditionalDocuments(additionDocumentId)
{
	$('#errAdditionalDocuments').empty();
	$('#documentName').css("background-color","");
	$('#uploadedDocument').css("background-color","");
	
	document.getElementById("additionalDocumentsForm").reset();	
	PFCertifications.showEditTeacherAdditionalDocuments(additionDocumentId,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
	{
		document.getElementById("divAdditionalDocumentsRow").style.display="block";
		document.getElementById("divDocumnetDone").style.display="block";
		
		document.getElementById("additionDocumentId").value=data.additionDocumentId;
		document.getElementById("documentName").value=data.documentName;
		/*try{
			document.getElementById("uploadedDocument").value=data.uploadedDocument;
		}catch(err){}*/
		try{
			document.getElementById("uploadedDocumentHidden").value=data.uploadedDocument;
		}catch(err){}
		
		
		//alert(data.pathOfTranscript)
		if(data.uploadedDocument!=null && data.uploadedDocument!="")
		{	
			document.getElementById("removeUploadedDocumentSpan").style.display="inline";
			document.getElementById("divUploadedDocument").style.display="inline";
			document.getElementById("divUploadedDocument").innerHTML="<a href='javascript:void(0)' id='hrefAdditionalDocuments' onclick=\"downloadUploadedDocument('"+data.additionDocumentId+"','hrefAdditionalDocuments');" +
			"if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0');" +
			"return false;\">"+data.uploadedDocument+"</a>";
		}
		else
		{
			document.getElementById("divUploadedDocument").style.display="none";
			document.getElementById("removeUploadedDocumentSpan").style.display="none";
			document.getElementById("uploadedDocument").value="";
		}
		document.getElementById("documentName").focus();
		
	}})
	return false;
}

function editFormVideoLink(id)
{
	PFCertifications.editVideoLink(id,
			{ 
				async: false,
				errorHandler:handleError,
				callback: function(data)
				{
					showVideoLinksForm();
					dwr.util.setValues(data);
					return false;
				}
			});
		return false;
}
function checkForEnter(evt)
{
	var charCode = (evt.which) ? evt.which : event.keyCode;
	if(charCode==13){
		insertOrUpdatevideoLinks();
		return true;
	}
}

function checkForDecimalTwo(evt) {
	var count=true;
	var str=evt.srcElement.value;
	if (str.lastIndexOf('.') == -1) str += ".";
	var decNum = str.substring(str.lastIndexOf('.')+1, str.length);
	if (decNum.length > 1)//here is the key u can just change from 2 to 3,45 etc to restict no of digits aftre decimal
	{
		//alert("Invalid more than 2 digits after decimal")
		//count=false;
	}
	//alert(count);
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==8)
		return true;

	var parts = evt.srcElement.value.split('.');
	if(parts.length > 1 && charCode==46)
		return false;

	return( ((charCode >= 48 || charCode==46)&& count && charCode <= 57)||(charCode==13) );
}
function editFormElectronicReferences(id)
{	
	//document.getElementById('referenceDetails').value="";alert("A");
	PFCertifications.editElectronicReferences(id,{ 
			async: false,
			errorHandler:handleError,
			callback: function(data){
				showElectronicReferencesForm();
				dwr.util.setValues(data);
				if(data.pathOfReference!=null && data.pathOfReference!="")
				{	
					document.getElementById("removeref").style.display="block";
					document.getElementById("pathOfReference").value=data.pathOfReference;
					document.getElementById("divRefName").style.display="block";
					document.getElementById("divRefName").innerHTML="<a href='javascript:void(0)' id='hrefEditRef' onclick=\"downloadReference('"+data.elerefAutoId+"','hrefEditRef');" +
					"if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0');" +
					"return false;\">"+data.pathOfReference+"</a>";
				}else{
					document.getElementById("divRefName").style.display="none";
					document.getElementById("removeref").style.display="none";
					document.getElementById("pathOfReference").value="";
				}
				return false;
			}
		});
	return false;
}
function delRow_academic(id)
{	
	document.getElementById("academicID").value=id;
	$('#deleteAcademicRecord').modal('show');
}
function deleteAcademicRecord(){
	var id=document.getElementById("academicID").value;
	PFAcademics.deletePFAcademinGrid(id,{
	async: true,
	errorHandler:handleError,
	callback:function(data)
	{
		$('#deleteAcademicRecord').modal('hide');
		showGridAcademics();
		resetUniversityForm();
		return false;
	}});
}
function deleteRecord_Certificate(id)
{	
	document.getElementById("certificateTypeID").value=id;
	$('#deleteCertRec').modal('show');
}
function deleteCertificateRecord(){
	var id=document.getElementById("certificateTypeID").value;
	PFCertifications.deleteRecord(id,{ 
	async: true,
	errorHandler:handleError,
	callback:function(data)
	{
		$('#deleteCertRec').modal('hide');
		showGridCertifications();
		hideForm_Certification();
		
		displayGKAndSubject();
		
		return false;
	}});
}
function deleteRecordInvolvment(id)
{
	if(window.confirm(resourceJSON.msgsuredelete))
	{
		PFExperiences.deleteRecordInvolvment(id, { 
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{
			hideInvolvement();
			getInvolvementGrid();

			return false;
			}});
	}
	return false;
}

function deleteRecordHonors(id)
{
	if(window.confirm(resourceJSON.msgsuredelete))
	{
		PFExperiences.deleteRecordHonors(id,{ 
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{
			hideHonor();
			getHonorsGrid();

			return false;
			}});
	}
	return false;
}

function deleteEmployment(id)
{	
	document.getElementById("empHistoryID").value=id;
	$('#deleteEmpHistory').modal('show');
}

function deleteEmploymentDSPQ()
{	
	$('#deleteEmpHistory').modal('hide');
	var id=document.getElementById("empHistoryID").value;
	PFExperiences.deleteEmployment(id, { 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			getPFEmploymentDataGrid();
			hideEmploymentForm();
			return false;
		}});
}

function delRow_Document(id)
{	
	document.getElementById("additionDocumentId").value=id;
	$('#removeUploadedDocument').modal('show');
}

function removeUploadedDocument(){
	
	var additionDocumentId=document.getElementById("additionDocumentId").value;
	PFCertifications.removeUploadedDocument(additionDocumentId,{
	async: true,
	errorHandler:handleError,
	callback:function(data)
	{
		$('#removeUploadedDocument').modal('hide');
		showGridAdditionalDocuments();
		resetAdditionalDocumentsForm();
		return false;
	}});
}

var videolnkIDForTeacher=0;
function delVideoLink(id)
{	
	videolnkIDForTeacher=id;
	$('#delVideoLnk').modal('show');
}

function delVideoLnkConfirm()
{	
		$('#delVideoLnk').modal('hide');
		PFCertifications.deleteVIdeoLink(videolnkIDForTeacher, { 
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{
				getVideoLinksGrid();
				hideVideoLinksForm();
				return false;
			}});
		
		videolnkIDForTeacher=0;
}

var eleRefIDForTeacher=0;
var status_eref=0;
function changeStatusElectronicReferences(id,status)
{	
	eleRefIDForTeacher=id;
	status_eref=status;
	
	if(status==1)
	$('#chgstatusRef1').modal('show');
	else if(status==0)
	$('#chgstatusRef2').modal('show');	
}	

function changeStatusElectronicReferencesConfirm()
{	
	$('#chgstatusRef1').modal('hide');
	$('#chgstatusRef2').modal('hide');

		PFCertifications.changeStatusElectronicReferences(eleRefIDForTeacher,status_eref, { 
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{
				getElectronicReferencesGrid();
				hideElectronicReferencesForm();
				return false;
			}});
		
		eleRefIDForTeacher=0;
}



function getVideoLinksGrid_DivProfile(teacherId)
{
	var visitLocation=document.getElementById("teacherIdForprofileGridVisitLocation").value;	
	TeacherProfileViewInDivAjax.getVideoLinksGrid(teacherId,noOfRowsvideoLink,pagevideoLink,sortOrderStrvideoLink,sortOrderTypevideoLink,visitLocation, { 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#gridDataVideoLink').html(data);
			if(visitLocation=="CG View"){
				applyScrollOnTblVideoLinks_profile();
			}

		}});
}

function displayGKAndSubject()
{
	//alert("Calling displayGKAndSubject.....1");
	DistrictPortfolioConfigAjax.getCurrentStatusCertificate({ 
		async: false,
		errorHandler:handleError,
		callback: function(data)
		{
			if(data!=null)
			{
				var res = data.split("#");				
				document.getElementById("displayGKAndSubject").value=res[0];
				document.getElementById("displayPassFailGK").value=res[1];
				showHideGK_SubjectArea();
			}
		}
		});
}
function insertOrUpdate_Academic(sbtsource)
{ 
	
	//$('#testcall').append("&#149; insertOrUpdate_Academic<br>");
	if(sbtsource=="0")
	{
		resetSBTNSource();
	}
	
	var threadCount_aca=document.getElementById("threadCount_aca").value;
	if(threadCount_aca=="0")
	{
		document.getElementById("threadCount_aca").value=1;
		
		updateThreadCount("aca");
		
		var academicId = document.getElementById("academicId");
		var degreeId = document.getElementById("degreeId");
		var degreeName = document.getElementById("degreeName");
		var universityId = document.getElementById("universityId");
		var universityName = document.getElementById("universityName");
		var fieldId = document.getElementById("fieldId");     
		var fieldName = document.getElementById("fieldName");     
		var attendedInYear = document.getElementById("attendedInYear"); 
		var leftInYear = document.getElementById("leftInYear");     
		var gpaFreshmanYear = document.getElementById("gpaFreshmanYear");     
		var gpaJuniorYear = document.getElementById("gpaJuniorYear");     
		var gpaSophomoreYear = document.getElementById("gpaSophomoreYear");     
		var gpaSeniorYear = document.getElementById("gpaSeniorYear");     
		var gpaCumulative = document.getElementById("gpaCumulative"); 
		var degreeType = document.getElementById("degreeType");
		var isMiamiChk=document.getElementById("isMiami").value;
		var international=0;
		
		//alert(degreeType.value+"   "+document.document.getElementById('international2').checked);
        if(degreeType.value=="B" && document.getElementById('international').checked){
            international=1;
        }else if(degreeType.value=="M" && document.getElementById('international2').checked){
        	international=1;        	
        }else if(degreeType.value=="A" && document.getElementById('international2').checked){
        	international=1;        	
        }else if(degreeType.value=="D" && document.getElementById('international2').checked){
        	international=1;        	
        }
		var pathOfTranscript="";
		var ext="";
		var fileName="";

		if(isMiamiChk=='false')
		{
			pathOfTranscript = document.getElementById("pathOfTranscript");
			fileName = pathOfTranscript.value;
			
			ext = fileName.substr(fileName.lastIndexOf('.') + 1).toLowerCase();
			var fileSize=0;		
			if ($.browser.msie==true)
		 	{	
			    fileSize = 0;	   
			}
			else
			{		
				if(pathOfTranscript.files[0]!=undefined)
					fileSize = pathOfTranscript.files[0].size;
			}
		}
			
		
		var cnt=0;
		var focs=0;	
		
		$('#errordiv_AcademicForm').empty();
		setDefColortoErrorMsg_Academic();	
		if(trim(degreeName.value)=="")
		{
			$('#errordiv_AcademicForm').append("&#149; "+resourceJSON.magPleaseenterDegree+"<br>");
			if(focs==0)
				$('#degreeName').focus();
			
			$('#degreeName').css("background-color",txtBgColor);
			cnt++;focs++;
		}
		var GEDFlag=false;
		
		if($('#degreeName').val()=="High School or GED"){
			GEDFlag=true;
		}
		
		if($('#degreeName').val()!="No Degree"){
					if(trim(universityName.value)=="" && GEDFlag==false)
					{
						$('#errordiv_AcademicForm').append("&#149; "+resourceJSON.msgPleaseenterSchool+"<br>");
						if(focs==0)
							$('#universityName').focus();
						
						$('#universityName').css("background-color",txtBgColor);
						cnt++;focs++;
					}
					if(trim(fieldName.value)=="" && GEDFlag==false)
					{
						$('#errordiv_AcademicForm').append("&#149; "+resourceJSON.msgFieldofStudy+"<br>");
						if(focs==0)
							$('#fieldName').focus();
						
						$('#fieldName').css("background-color",txtBgColor);
						cnt++;focs++;
					}
					if(trim(attendedInYear.value)=="")
					{
						$('#errordiv_AcademicForm').append("&#149; "+resourceJSON.msgdateattended+"<br>");
						if(focs==0)
							$('#attendedInYear').focus();
						
						$('#attendedInYear').css("background-color",txtBgColor);
						cnt++;focs++;
					}
						
					if(trim(leftInYear.value)=="")
					{
						$('#errordiv_AcademicForm').append("&#149; "+resourceJSON.msgdatetoattended+"<br>");
						if(focs==0)
							$('#leftInYear').focus();
						
						$('#leftInYear').css("background-color",txtBgColor);
						cnt++;focs++;
					}
					
					if((trim(attendedInYear.value)!="") && (trim(leftInYear.value)!="") &&(trim(attendedInYear.value) > trim(leftInYear.value)))
					{
						$('#errordiv_AcademicForm').append("&#149; "+resourceJSON.msgdateattendedgreaterthan+"<br>");
						if(focs==0)
							$('#attendedInYear').focus();
						
						$('#attendedInYear').css("background-color",txtBgColor);
						$('#leftInYear').css("background-color",txtBgColor);
						cnt++;focs++;
					}
					
					if(GEDFlag==true){	
						if(trim(pathOfTranscript.value)=="")
						{
							
							$('#errordiv_AcademicForm').append("&#149; "+resourceJSON.msguploadtrascript+"<br>");
							if(focs==0)
								$('#pathOfTranscript').focus();
							
							$('#pathOfTranscript').css("background-color",txtBgColor);
							cnt++;focs++;
						}
						/*else if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt'))
						{
							$('#errordiv_AcademicForm').append("&#149; Please select Acceptable resume formats include PDF, MS-Word, GIF, PNG, and JPEG  files.<br>");
								if(focs==0)
									$('#pathOfTranscript').focus();
								
								$('#pathOfTranscript').css("background-color",txtBgColor);
								cnt++;focs++;	
								return false;
						}*/
					}
					
		}

		/*if(document.getElementById("districtIdForDSPQ").value==4218990){
			if($('#isSchoolSupportPhiladelphia').val()!="")
			if(document.getElementById("pathOfTranscript").value=="" && $('#divResumerName').html()==""){		
				$('#errordiv_AcademicForm').append("&#149;  Please upload Transcript<br>");
				if(focs==0)
					$('#pathOfTranscript').focus();
				
				$('#pathOfTranscript').css("background-color",txtBgColor);
				cnt++;focs++;
			}
		}
		*/
		if(ext!="")
		if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt'))
		{
			$('#errordiv_AcademicForm').append("&#149; "+resourceJSON.msgaacptfileformate+"<br>");
				if(focs==0)
					$('#pathOfTranscript').focus();
				
				$('#pathOfTranscript').css("background-color",txtBgColor);
				cnt++;focs++;	
				return false;
		}	
		else if(fileSize>=10485760)
		{
			$('#errordiv_AcademicForm').append("&#149; "+resourceJSON.FileSizeLessthan10Mb+"<br>");
				if(focs==0)
					$('#pathOfTranscript').focus();
				
				$('#pathOfTranscript').css("background-color",txtBgColor);
				cnt++;focs++;	
				return false;
		}
		
		if(trim(degreeType.value)=="B")
		{	
			if(trim(gpaFreshmanYear.value)=="" || trim(gpaFreshmanYear.value)==".")
			{
				/*$('#errordiv').append("&#149; Please enter valid Freshman GPA<br>");
				if(focs==0)
					$('#gpaFreshmanYear').focus();
				
				$('#gpaFreshmanYear').css("background-color",txtBgColor);
				cnt++;focs++;*/
			}
			else if(parseFloat(trim(gpaFreshmanYear.value))>5.00)
			{
				$('#errordiv_AcademicForm').append("&#149; "+resourceJSON.msgfreshmangpa+"<br>");
				if(focs==0)
					$('#gpaFreshmanYear').focus();
				
				$('#gpaFreshmanYear').css("background-color",txtBgColor);
				cnt++;focs++;
			}
			
			if(trim(gpaSophomoreYear.value)=="" || trim(gpaSophomoreYear.value)==".")
			{
				/*$('#errordiv').append("&#149; Please enter valid Sophomore GPA<br>");
				if(focs==0)
					$('#gpaSophomoreYear').focus();
				
				$('#gpaSophomoreYear').css("background-color",txtBgColor);
				cnt++;focs++;*/
			}
			else if(parseFloat(trim(gpaSophomoreYear.value))>5.00)
			{
				$('#errordiv_AcademicForm').append("&#149; "+resourceJSON.msgSophomoreGPA+"<br>");
				if(focs==0)
					$('#gpaSophomoreYear').focus();
				
				$('#gpaSophomoreYear').css("background-color",txtBgColor);
				cnt++;focs++;
			}
			
			if(trim(gpaJuniorYear.value)=="" || trim(gpaJuniorYear.value)==".")
			{
				/*$('#errordiv').append("&#149; Please enter valid Junior GPA<br>");
				if(focs==0)
					$('#gpaJuniorYear').focus();
				
				$('#gpaJuniorYear').css("background-color",txtBgColor);
				cnt++;focs++;*/
			}
			else if(parseFloat(trim(gpaJuniorYear.value))>5.00)
			{
				$('#errordiv_AcademicForm').append("&#149; "+resourceJSON.msgJuniorGPA+"<br>");
				if(focs==0)
					$('#gpaJuniorYear').focus();
				
				$('#gpaJuniorYear').css("background-color",txtBgColor);
				cnt++;focs++;
			}
				
			if(trim(gpaSeniorYear.value)=="" || trim(gpaSeniorYear.value)==".")
			{
				/*$('#errordiv').append("&#149; Please enter valid Senior GPA<br>");
				if(focs==0)
					$('#gpaSeniorYear').focus();
				
				$('#gpaSeniorYear').css("background-color",txtBgColor);
				cnt++;focs++;*/
			}
			else if(parseFloat(trim(gpaSeniorYear.value))>5.00)
			{
				$('#errordiv_AcademicForm').append("&#149; "+resourceJSON.msgSeniorGPA+"<br>");
				if(focs==0)
					$('#gpaSeniorYear').focus();
				
				$('#gpaSeniorYear').css("background-color",txtBgColor);
				cnt++;focs++;
			}
			gpaCumulative = document.getElementById("gpaCumulative").value;
			if(document.getElementById("districtIdForDSPQ").value==4218990){
				var nonteacherFlag=$("#isnontj").val();
				if($('#isSchoolSupportPhiladelphia').val()!=""){}
				else if(nonteacherFlag=="" || nonteacherFlag!="true"){
					if(trim(gpaCumulative)=="" || trim(gpaCumulative)==".")
					{
						if(!document.getElementById('international').checked){
						$('#errordiv_AcademicForm').append("&#149; "+resourceJSON.msgvalidCumulativeGPA+"<br>");
						if(focs==0)
						$('#gpaCumulative').focus();

						$('#gpaCumulative').css("background-color",txtBgColor);
						cnt++;focs++;
						}
					}
					else if(parseFloat(trim(gpaCumulative))>5.00)
					{
						$('#errordiv_AcademicForm').append("&#149; "+resourceJSON.msgCumulativeGPA+"<br>");
						if(focs==0)
						$('#gpaCumulative').focus();

						$('#gpaCumulative').css("background-color",txtBgColor);
						cnt++;focs++;
					}

				}
			}else if(document.getElementById("districtIdForDSPQ").value==3904380 || document.getElementById("districtIdForDSPQ").value==1201470 || document.getElementById("districtIdForDSPQ").value==7800040 || document.getElementById("districtIdForDSPQ").value==7800047
					 || document.getElementById("districtIdForDSPQ").value==804800){
				if(parseFloat(trim(gpaCumulative))>5.00)
				{
					$('#errordiv_AcademicForm').append("&#149; "+resourceJSON.msgCumulativeGPA+"<br>");
					if(focs==0)
					$('#gpaCumulative').focus();

					$('#gpaCumulative').css("background-color",txtBgColor);
					cnt++;focs++;
				}

			}else{
				if(trim(gpaCumulative)=="" || trim(gpaCumulative)==".")
				{
					if(!document.getElementById('international').checked){
					$('#errordiv_AcademicForm').append("&#149; "+resourceJSON.msgvalidCumulativeGPA+"<br>");
					if(focs==0)
					$('#gpaCumulative').focus();

					$('#gpaCumulative').css("background-color",txtBgColor);
					cnt++;focs++;
					}
				}
				else if(parseFloat(trim(gpaCumulative))>5.00)
				{
					$('#errordiv_AcademicForm').append("&#149; "+resourceJSON.msgCumulativeGPA+"<br>");
					if(focs==0)
					$('#gpaCumulative').focus();

					$('#gpaCumulative').css("background-color",txtBgColor);
					cnt++;focs++;
				}
			}
		}
		else if(GEDFlag==false)			
		{	
			gpaCumulative = document.getElementById("gpaCumulative1").value;
			if($('#degreeName').val()!="No Degree")
				if(document.getElementById("districtIdForDSPQ").value==3904380 || document.getElementById("districtIdForDSPQ").value==1201470 || document.getElementById("districtIdForDSPQ").value==7800040 || document.getElementById("districtIdForDSPQ").value==7800047
						 || document.getElementById("districtIdForDSPQ").value==804800){
					if(trim(degreeName.value)!="" && parseFloat(trim(gpaCumulative))>5.00)
					{
						$('#errordiv_AcademicForm').append("&#149; "+resourceJSON.msgCumulativeGPA+"<br>");
								if(focs==0)
									$('#gpaCumulative1').focus();
								
								$('#gpaCumulative1').css("background-color",txtBgColor);
								cnt++;focs++;
					}
				}
			else if(document.getElementById("districtIdForDSPQ").value==4218990){
			var nonteacherFlag=$("#isnontj").val();
			if($('#isSchoolSupportPhiladelphia').val()!=""){}
			else if(nonteacherFlag=="" || nonteacherFlag!="true"){

				if((trim(degreeName.value)!="") && (trim(gpaCumulative)=="" || trim(gpaCumulative)=="."))
				{
					if(!document.getElementById('international2').checked){
								$('#errordiv_AcademicForm').append("&#149; "+resourceJSON.msgvalidCumulativeGPA+"<br>");
								if(focs==0)
									$('#gpaCumulative1').focus();
								
								$('#gpaCumulative1').css("background-color",txtBgColor);
								cnt++;focs++;
							}
				}
				else if(trim(degreeName.value)!="" && parseFloat(trim(gpaCumulative))>5.00)
				{
					$('#errordiv_AcademicForm').append("&#149; "+resourceJSON.msgCumulativeGPA+"<br>");
							if(focs==0)
								$('#gpaCumulative1').focus();
							
							$('#gpaCumulative1').css("background-color",txtBgColor);
							cnt++;focs++;
				}
			}
			}else{
				if((trim(degreeName.value)!="") && (trim(gpaCumulative)=="" || trim(gpaCumulative)=="."))
				{
					if(!document.getElementById('international2').checked){
						$('#errordiv_AcademicForm').append("&#149; "+resourceJSON.msgvalidCumulativeGPA+"<br>");
						if(focs==0)
							$('#gpaCumulative1').focus();
						
						$('#gpaCumulative1').css("background-color",txtBgColor);
						cnt++;focs++;
					}
				}
				else if(trim(degreeName.value)!="" && parseFloat(trim(gpaCumulative))>5.00)
				{
					$('#errordiv_AcademicForm').append("&#149; "+resourceJSON.msgCumulativeGPA+"<br>");
					if(focs==0)
						$('#gpaCumulative1').focus();
					
					$('#gpaCumulative1').css("background-color",txtBgColor);
					cnt++;focs++;
				}
			}
		}
		
		
		if(cnt!=0)		
		{
			updateReturnThreadCount("aca");
			$('#errordiv_AcademicForm').show();
			return false;
		}
		else
		{	
			if(fileName!="")
			{
				//$('#loadingDiv').show();
				document.getElementById("frmAcadamin").submit();
			}
			else
			{
		
				PFAcademics.saveOrUpdateAcademics(academicId.value,degreeId.value,universityId.value,fieldId.value,attendedInYear.value,leftInYear.value,gpaFreshmanYear.value,gpaJuniorYear.value,gpaSophomoreYear.value,gpaSeniorYear.value,gpaCumulative,degreeType.value,ext,international,
				{ 
				async: false,
				errorHandler:handleError,
				callback: function(data)
					{	
						showGridAcademics();
						/*if(ext!="")
						{
							$('#loadingDiv').show();
							document.getElementById("frmAcadamin").submit();
						}*/				
						$("#hrefAcademics").addClass("stepactive");
						resetUniversityForm();
						if(sbtsource==1)
						{
							saveAllDSPQ();
							updateReturnThreadCount("aca");
//							validatePortfolioErrorMessageAndGridData('level2');
						}
					}
				});
			}
		}
	}
	
}

function hideLoadingDiv_DSPQ()
{
	try
	{
		$('#loadingDiv_dspq_ie').fadeOut();
	}
	catch(e){}
}
function saveAllDSPQ(){
	
	var conti=false;
	var sbtsource=1;
	if($("#degreeName").is(':visible'))
	{
		document.getElementById("sbtsource_aca").value=1;
		conti=true;
		insertOrUpdate_Academic(sbtsource);
	}
	if($("#stateMaster").is(':visible'))
	{
		document.getElementById("sbtsource_cert").value=1;
		conti=true;
		insertOrUpdate_Certification(sbtsource);
	}
	else
	{
		displayGKAndSubject();
	}
	if($("#salutation").is(':visible'))
	{
		document.getElementById("sbtsource_ref").value=1;
		conti=true;
		insertOrUpdateElectronicReferences(sbtsource);
	}
	if($("#role").is(':visible'))
	{
		document.getElementById("sbtsource_emp").value=1;
		conti=true;
		insertOrUpdateEmployment(sbtsource);
	}
	if($("#examStatus").is(':visible'))
	{
		document.getElementById("sbtsource_subArea").value=1;
		conti=true;
		saveSubjectAreas(sbtsource);
	}
	if($("#documentName").is(':visible'))
	{
		document.getElementById("sbtsource_aadDoc").value=1;
		conti=true;
		insertOrUpdate_AdditionalDocuments(sbtsource);
	}
	
	if(conti)
	{
//		validatePortfolioErrorMessageAndGridData('level2');
	}
}

function resetSBTNSource()
{
	
	try
	{
		document.getElementById("threadCount").value=0;
	}
	catch(e){}
	
	try
	{
		document.getElementById("returnThreadCount").value=0;
	}
	catch(e){}
	
	try
	{
		document.getElementById("callForwardCount").value=-2;
	}
	catch(e){}
	
	
	/////////////////
	
	try
	{
		document.getElementById("sbtsource_aca").value=0;
	}
	catch(e){}
	
	
	try
	{
		document.getElementById("sbtsource_cert").value=0;
		
	}
	catch(e){}
	
	try
	{
		document.getElementById("sbtsource_ref").value=0;
		
	}
	catch(e){}
	
	try
	{
		document.getElementById("sbtsource_emp").value=0;
		
	}
	catch(e){}
	
	try
	{
		document.getElementById("sbtsource_subArea").value=0;
		
	}
	catch(e){}
	
	try
	{
		document.getElementById("sbtsource_aadDoc").value=0;
	}
	catch(e){}
	
	///////////////////////////////////////////////
	
	try
	{
		document.getElementById("threadCount_aca").value=0;
		//alert("aca :: "+document.getElementById("threadCount_aca").value);
		
	}
	catch(e){}
	
	try
	{
		document.getElementById("threadCount_cert").value=0;
		
	}
	catch(e){}
	
	try
	{
		document.getElementById("threadCount_emp").value=0;
		
	}
	catch(e){}
	
	try
	{
		document.getElementById("threadCount_ref").value=0;
		
	}
	catch(e){}
	
	try
	{
		document.getElementById("threadCount_subarea").value=0;
		
	}
	catch(e){}
	
	try
	{
		document.getElementById("threadCount_adddoc").value=0;
	}
	catch(e){}
	
	
}

function updateThreadCount(section)
{
	try
	{
		var threadCount=document.getElementById("threadCount").value;
		var iThreadCount = parseInt(threadCount);
		iThreadCount=iThreadCount+1;
		document.getElementById("threadCount").value=iThreadCount;
		//alert(section+" iThreadCount "+iThreadCount);
	}
	catch(e){}
}

function updateReturnThreadCount(section)
{
	try
	{
		var returnThreadCount=document.getElementById("returnThreadCount").value;
		var iReturnThreadCount = parseInt(returnThreadCount);
		iReturnThreadCount=iReturnThreadCount+1;
		document.getElementById("returnThreadCount").value=iReturnThreadCount;
		//alert(section+" iReturnThreadCount "+iReturnThreadCount);
	}
	catch(e){}
}



function getDashboardJobsofIntrest()
{
	DashboardAjax.getJbofIntresGrid({ 
		async: true,
		errorHandler:handleError,
		callback: function(data){
		$('#divJbInterest').html(data);
		tpJbIntrestDashboardEnable();
	}
	});	

}
function tpJbIntrestDashboardEnable()
{
	$('#iconpophoverRe').tooltip();
	$('#iconpophoverBase').tooltip({
	        toggle: 'toolip',
	        placement: 'right'
	    });

	$('#iconpophover1').tooltip();
	$('#iconpophover2').tooltip();
	$('#iconpophover3').tooltip();
	$('#iconpophover4').tooltip();
	$('#iconpophover5').tooltip();
	$('#iconpophover6').tooltip();

	$('#tpApply1').tooltip();
	$('#tpApply2').tooltip();
	$('#tpApply3').tooltip();
	$('#tpApply4').tooltip();

	$('#tpWithdraw1').tooltip();
	$('#tpWithdraw2').tooltip();
	$('#tpWithdraw3').tooltip();
	$('#tpWithdraw4').tooltip();

	$('#tpCoverLetter1').tooltip();
	$('#tpCoverLetter2').tooltip();
	$('#tpCoverLetter3').tooltip();
	$('#tpCoverLetter4').tooltip();
	
	
	$('#tpJbStatus1').tooltip();
	$('#tpJbStatus2').tooltip();
	$('#tpJbStatus3').tooltip();
	$('#tpJbStatus4').tooltip();


	$('#tpShare1').tooltip();
	$('#tpShare2').tooltip();
	$('#tpShare3').tooltip();
	$('#tpShare4').tooltip();
	$('#tpShare5').tooltip();
	$('#tpShare6').tooltip();
	$('#tpShare7').tooltip();
	$('#tpShare8').tooltip();

	$('#tpHide1').tooltip();
	$('#tpHide2').tooltip();
	$('#tpHide3').tooltip();
	$('#tpHide4').tooltip();
	
	$('#QQ1').tooltip();
	$('#QQ2').tooltip();
	$('#QQ3').tooltip();
	$('#QQ4').tooltip();
	$('#QQ5').tooltip();
	$('#QQ6').tooltip();
	$('#QQ7').tooltip();
	$('#QQ8').tooltip();
	
	$('#JSI1').tooltip();
	$('#JSI2').tooltip();
	$('#JSI3').tooltip();
	$('#JSI4').tooltip();
	$('#JSI5').tooltip();
	$('#JSI6').tooltip();
	$('#JSI7').tooltip();
	$('#JSI8').tooltip();

}

function hideAllPortfolioModel()
{
	try { $('#myModalCL').modal('hide'); } catch (e) {}
	try { $('#myModalDymanicPortfolio').modal('hide'); } catch (e) {}
	try { $('#topErrorMessageDSPQ').modal('hide'); } catch (e) {}
}

function hideTFAFields_DP()
{
	$('#errordivExp').empty();
	$('#expCertTeacherTraining').css("background-color","");
	$('#tfaAffiliate').css("background-color","");
	$('#corpsYear').css("background-color","");
	$('#tfaRegion').css("background-color","");
	
	var tfaAffiliate 	= 	document.getElementById("tfaAffiliate").value;
	if(tfaAffiliate=="3")//if(tfaAffiliate=="3" || tfaAffiliate=='')
	{
		try { document.getElementById("corpsYear").value=""; } catch (e) {}
		try { document.getElementById("tfaRegion").value=""; } catch (e) {}
		$("#tfaFieldsDiv").fadeOut();
	}
	else
	{
		$("#tfaFieldsDiv").fadeIn();
	}
}

function getDistrictSpecificTFAValues(districtId){
	PFExperiences.getDistrictSpecificTFAValues(districtId,
			{ 
				async: false,
				errorHandler:handleError,
				callback: function(data)
				{
				//alert(data);
					$("#tfaDistOptData").html(data);
				}
			});	
}

function showHideGK_SubjectArea()
{
	//alert("Calling showHideGK_SubjectArea");
	var isMiamiChk=document.getElementById("isMiami").value;
	var displayGKAndSubject1=document.getElementById("displayGKAndSubject").value;
	
	var generalKnowledge_config=document.getElementById("generalKnowledge_config").value;
	var subjectAreaExam_config=document.getElementById("subjectAreaExam_config").value;
	
	var IsSIForMiami=document.getElementById("IsSIForMiami").value;
	var displayPassFailGK1=document.getElementById("displayPassFailGK").value;
	
	var isItvtForMiami = document.getElementById("isItvtForMiami").value;
	var districtIdForDSPQ = $("#districtIdForDSPQ").val();
	
	if(document.getElementById("districtIdForDSPQ").value=="614730" || document.getElementById("districtIdForDSPQ").value=="1302010"){
		if(generalKnowledge_config==1)
		{
			document.getElementById("generalKnowledgeDiv").style.display="block";
		}
		else
		{
			document.getElementById("generalKnowledgeDiv").style.display="none";
		}
		if(subjectAreaExam_config==1)
		{
			document.getElementById("subjectAreaDiv").style.display="block";
		}
		else
		{
			document.getElementById("subjectAreaDiv").style.display="none";
		}
	}	
	else if(isItvtForMiami=='false')
	{
		if(displayGKAndSubject1=="true" && isMiamiChk=="true" && IsSIForMiami=="false")
		{
			//alert("1");
			if(generalKnowledge_config==1)
			{
				document.getElementById("generalKnowledgeDiv").style.display="block";
			}
			else
			{
				document.getElementById("generalKnowledgeDiv").style.display="none";
			}
			if(subjectAreaExam_config==1)
			{
				document.getElementById("subjectAreaDiv").style.display="block";
			}
			else
			{
				document.getElementById("subjectAreaDiv").style.display="none";
			}
		}
		else if(isMiamiChk=="true" && IsSIForMiami=="true" && displayPassFailGK1=="false")
		{
			//alert("2");
			if(generalKnowledge_config==1)
			{
				document.getElementById("generalKnowledgeDiv").style.display="block";
			}
			else
			{
				document.getElementById("generalKnowledgeDiv").style.display="none";
			}
			document.getElementById("subjectAreaDiv").style.display="none";
		}
		else
		{
			//alert("3");
			document.getElementById("generalKnowledgeDiv").style.display="none";
			document.getElementById("subjectAreaDiv").style.display="none";
		}
	}
	else
	{
		try
		{
			if(isMiamiChk=="true")
			{
				document.getElementById("generalKnowledgeDiv").style.display="none";
				document.getElementById("subjectAreaDiv").style.display="none";
			}
		}catch(e){}
	}
	
}

function getSAEValues()
{
	PFCertifications.getSAEValues({ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
		try{
			document.getElementById("examStatus").value=data.examStatus;
			try{
				document.getElementById("examDate").value=(new Date(data.examDate).getMonth()+1)+"-"+(new Date(data.examDate).getDate())+"-"+(new Date(data.examDate).getFullYear()); 
			}catch(err){}
			try{
				document.getElementById("subjectIdforDSPQ").value=data.subjectMaster.subjectId;
			}catch(err){}
			try{
				document.getElementById("scoreReport").value=data.scoreReport; 
			}catch(err){}
			try{
				document.getElementById("scoreReportHidden").value=data.scoreReport;
			}catch(err){}
			try{
				$("#subjectExamTextarea").find(".jqte_editor").html(data.examNote);
				$('[name="subjectExamTextarea"]').text(data.examNote);
			}catch(err){}
			if(data.scoreReport!=null && data.scoreReport!="")
			{	
				document.getElementById("removeScoreReportSpan").style.display="inline";
				document.getElementById("divScoreReport").style.display="inline";
				//
				document.getElementById("divScoreReport").innerHTML=""+resourceJSON.msgRecentscorereportonfile+": <a href='javascript:void(0)' id='hrefSubjectArea' onclick=\"downloadSubjectAreaExam('"+data.teacherSubjectAreaExamId+"','hrefSubjectArea');" +
				"if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0');" +
				"return false;\">"+data.scoreReport+"</a>";
				
				//$('#hrefSubjectArea').tooltip();
				
			}else{
				document.getElementById("removeScoreReportSpan").style.display="none";
				document.getElementById("divScoreReport").style.display="none";
				document.getElementById("scoreReport").value="";
			}
		}catch(err){}
		}});
}

function clearGeneralKnowledgeScoreReport()
{
	document.getElementById("generalKnowledgeScoreReport").value=null;
	document.getElementById("generalKnowledgeScoreReport").reset();
	$('#generalKnowledgeScoreReport').css("background-color","");	
}

function clearScoreReport()
{
	document.getElementById("scoreReport").value=null;
	document.getElementById("scoreReport").reset();
	$('#scoreReport').css("background-color","");	
}

function cancelSubjectAreaForm()
{
	resetSubjectAreaForm();
	document.getElementById("divSubjectAreaInput").style.display="none";
	closeDSPQCal();
	return false;
}


function getGKEValues()
{
	PFCertifications.getGKEValues({ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
		try{
			document.getElementById("generalKnowledgeExamStatus").value=data.generalKnowledgeExamStatus;
			try{
				document.getElementById("generalKnowledgeExamDate").value=(new Date(data.generalKnowledgeExamDate).getMonth()+1)+"-"+(new Date(data.generalKnowledgeExamDate).getDate())+"-"+(new Date(data.generalKnowledgeExamDate).getFullYear()); 
			}catch(err){}
			try{
				document.getElementById("generalKnowledgeScoreReport").value=data.generalKnowledgeScoreReport;
			}catch(err){}
			try{
				document.getElementById("generalKnowledgeScoreReportHidden").value=data.generalKnowledgeScoreReport;
			}catch(err){}
			try{
				$("#generalExamNote").find(".jqte_editor").html(data.generalExamNote);
				$('[name="generalExamNote"]').text(data.generalExamNote);
			}catch(err){}
			
			if(data.generalKnowledgeScoreReport!=null && data.generalKnowledgeScoreReport!="")
			{	
				document.getElementById("removeGeneralKnowledgeScoreReportSpan").style.display="inline";
				document.getElementById("divGeneralKnowledgeScoreRep").style.display="inline";
				document.getElementById("divGeneralKnowledgeScoreRep").innerHTML=""+resourceJSON.msgRecentscorereportonfile+": <a href='javascript:void(0)' id='hrefGeneralKnowledge' onclick=\"downloadGeneralKnowledge('"+data.generalKnowledgeExamId+"','hrefGeneralKnowledge');" +
				"if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0');" +
				"return false;\">"+data.generalKnowledgeScoreReport+"</a>";
				
				//$('#hrefGeneralKnowledge').tooltip();
				
			}else{
				document.getElementById("divGeneralKnowledgeScoreReport").style.display="none";
				document.getElementById("removeGeneralKnowledgeScoreReportSpan").style.display="none";
				document.getElementById("generalKnowledgeScoreReport").value="";
			}
		}catch(err){}
		}});
}
function showSubjectAreaForm()
{
	document.getElementById("divSubjectAreaInput").style.display="block";
	document.getElementById("examStatus").focus();
	
	$('#examStatus').css("background-color","");
	$('#examDate').css("background-color","");
	$('#subjectIdforDSPQ').css("background-color","");
	$('#scoreReport').css("background-color","");
	resetSubjectAreaForm();
	
	
	document.getElementById("removeScoreReportSpan").style.display="none";
	document.getElementById("divScoreReport").style.display="none";
	document.getElementById("scoreReport").value="";
	$("#subjectExamTextarea").find(".jqte_editor").html("");	
	$('[name="subjectExamTextarea"]').text("");
	
	$('#errSubjectArea').empty();
	return false;
}
function resetSubjectAreaForm()
{
	document.getElementById("examStatus").value="0";
	document.getElementById("examDate").value="";
	document.getElementById("subjectIdforDSPQ").value="0";
	document.getElementById("scoreReport").value="";
	document.getElementById("scoreReportHidden").value="";
	
	document.getElementById("teacherSubjectAreaExamId").value=0;
	//document.getElementById("additionalDocumentsForm").reset();
	return false;
}

function cancelSubjectAreaForm()
{
	resetSubjectAreaForm();
	document.getElementById("divSubjectAreaInput").style.display="none";
	closeDSPQCal();
	return false;
}
function showGridSubjectAreasGrid()
{
	PFCertifications.getSubjectAreasGrid(dp_SubjectArea_noOfRows,dp_SubjectArea_page,dp_SubjectArea_sortOrderStr,dp_SubjectArea_sortOrderType,{  
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			//$('#divGridSubjectAre').html(data);
			document.getElementById("divGridSubjectAre").innerHTML=data;
			applyScrollOnTbl_SubjectAreas();
		}});
}



function showDSPQDiv()
{
	try { $('#topErrorMessageDSPQ').modal('hide'); } catch (e) {}
	try { $('#myModalDymanicPortfolio').modal('show'); } catch (e) {}
}

function insertOrUpdate_Certification(sbtsource)
{
	if(sbtsource=="0")
	{
		resetSBTNSource();
	}
	
	var threadCount_cert=document.getElementById("threadCount_cert").value;
	if(threadCount_cert=="0")
	{
		document.getElementById("threadCount_cert").value=1;
		
		updateThreadCount("cert");
		
		//$('#testcall').append("&#149; insertOrUpdate_Certification<br>");
		
		var certId = document.getElementById("certId");		
		var certText = $(".certText").find(".jqte_editor").html();	
		var stateMaster = document.getElementById("stateMaster");
		var yearReceived = document.getElementById("yearReceived");
		var certType = document.getElementById("certType");
		var certUrl = trim(document.getElementById("certUrl").value);
		var certiExpiration =$("#certiExpiration").val();
		var certificationtypeMasterObj = {certificationTypeMasterId:dwr.util.getValue("certificationtypeMaster")};
		var certificationTypeMaster = document.getElementById("certificationtypeMaster").value;
		var yearExpires = document.getElementById("yearexpires").value;
		var doenumber = trim(document.getElementById("doenumber").value);
		
		var IEINNumber = "";
		
		
		var pathOfCertificationFile = document.getElementById("pathOfCertificationFile");
		var cnt=0;
		var focs=0;	
		
		var fileName = pathOfCertificationFile.value;
		//alert("fileName "+fileName);
		var ext = fileName.substr(fileName.lastIndexOf('.') + 1).toLowerCase();
		var fileSize=0;		
		if ($.browser.msie==true){	
		    fileSize = 0;	   
		}else{		
			if(pathOfCertificationFile.files[0]!=undefined)
			fileSize = pathOfCertificationFile.files[0].size;
		}
		
		if(doenumber=="")
		{
			doenumber=0;
		}
		/*if(yearExpires=="")
		{
			yearExpires=0;
		}*/
		var districtIdForDSPQ="";	
		if ($('#districtIdForDSPQ').length > 0) {
			districtIdForDSPQ=document.getElementById("districtIdForDSPQ").value;
		}
		
		
		
		
		
		$('#errordiv_Certification').empty();
		setDefColortoErrorMsg_Certification();
		
		var certificationStatusMaster = document.getElementById("certificationStatusMaster").value;
		if(certificationStatusMaster=="")
		{
			$('#errordiv_Certification').append("&#149; "+resourceJSON.msgcertificate+"<br>");
				if(focs==0)
					$('#certificationStatusMaster').focus();
				
			$('#certificationStatusMaster').css("background-color",txtBgColor);
				cnt++;focs++;	
				//return false;
		}
		//districtIdForDSPQ noble
		
		if(districtIdForDSPQ==7800040)
		{
			IEINNumber = trim(document.getElementById("IEINNumber").value);
			
			if(IEINNumber=="" && certificationStatusMaster!="5")
			{
				$('#errordiv_Certification').append("&#149; "+resourceJSON.msgPleaseIEINNumber+"<br>");
					if(focs==0)
						$('#IEINNumber').focus();
					
				$('#IEINNumber').css("background-color",txtBgColor);
					cnt++;focs++;	
					//return false;
			}
		}
		
		if(document.getElementById("districtIdForDSPQ").value==804800)
		{				
			if(doenumber=="" && certificationStatusMaster!=5)
			{

				$('#errordiv_Certification').append("&#149; "+resourceJSON.msgDOENumber+"<br>");
					if(focs==0)
						$('#doenumber').focus();
					
				$('#doenumber').css("background-color",txtBgColor);
					cnt++;focs++;	
					//return false;
			
			}
			var selectedGrades = $( ".gradeLvl input:checked" ).length;
			if(selectedGrades==0){
				$('#errordiv_Certification').append("&#149; "+resourceJSON.msgGradeLevel+"<br>");
				if(focs==0)
					$('#doenumber').focus();			
			}
		}
		
		if(districtIdForDSPQ!=7800038){
		if(certificationStatusMaster!=5){
		var certificationtypeMasterObj = {certificationTypeMasterId:dwr.util.getValue("certificationtypeMaster")};
		if(trim(certificationtypeMasterObj.certificationTypeMasterId)==0)
		{
			$('#errordiv_Certification').append("&#149; "+resourceJSON.msgceritificationtype+"<br>");
			if(focs==0)
				$('#certificationtypeMaster').focus();
			
			$('#certificationtypeMaster').css("background-color",txtBgColor);
				cnt++;focs++;	
		}
		
		if(trim(stateMaster.value)=="")
		{
			$('#errordiv_Certification').append("&#149; "+resourceJSON.msgState+"<br>");
			if(focs==0)
				$('#stateMaster').focus();
			
			$('#stateMaster').css("background-color",txtBgColor);
			cnt++;focs++;
		}
		
		if(certificationStatusMaster!=3)
		{
			if(trim(yearReceived.value)=="")
			{
				$('#errordiv_Certification').append("&#149; "+resourceJSON.msgyearreciverd+"<br>");
				if(focs==0)
					$('#yearReceived').focus();
				
				$('#yearReceived').css("background-color",txtBgColor);
				cnt++;focs++;
			}
			
			
			/*if(trim(yearExpires)=="")
			{
				$('#errordiv_Certification').append("&#149; Please select Year Expires<br>");
				if(focs==0)
					$('#yearexpires').focus();
				
				$('#yearexpires').css("background-color",txtBgColor);
				cnt++;focs++;
			}*/
		}
		
		
		if(trim(certType.value)=="")
		{
			$('#errordiv_Certification').append("&#149; "+resourceJSON.msgentercertificatename+"<br>");
			if(focs==0)
				$('#certType').focus();
			
			$('#certType').css("background-color",txtBgColor);
			cnt++;focs++;
		}
		var certTypeObj = {certTypeId:dwr.util.getValue("certificateTypeMaster")};
		//alert((trim(certTypeObj.certTypeId)=="" && trim(certType.value)!=""));
		if(trim(certTypeObj.certTypeId)=="" && trim(certType.value)!="")
		{
			$('#errordiv_Certification').append("&#149; "+resourceJSON.msgnotexistselecstate+"<br>");
			if(focs==0)
				$('#certType').focus();
			$('#certType').css("background-color",txtBgColor);
			cnt++;focs++;
		}
		
		if(districtIdForDSPQ=4218990 && (certificationStatusMaster==1 || certificationStatusMaster==2)){}
		else{
		if(certificationStatusMaster==1 || certificationStatusMaster==2)
		{
			var pathOfCertification = document.getElementById("pathOfCertification").value;
			if(certUrl=="" && pathOfCertification=="" && pathOfCertificationFile.value=="")
			{
				$('#errordiv_Certification').append("&#149; "+resourceJSON.msgCertificationLicensureUrl+"<br>");
					if(focs==0)
						$('#certUrl').focus();
					$('#certUrl').css("background-color",txtBgColor);
					cnt++;focs++;	
					return false;
			}
		}
		}
		
		if(ext!=""){
			if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt'))
			{
				$('#errordiv').append("&#149; "+resourceJSON.msgaacptfileformate+"<br>");
					if(focs==0)
						$('#pathOfCertification').focus();
					
					$('#pathOfCertification').css("background-color",txtBgColor);
					cnt++;focs++;	
					return false;
			}	
			else if(fileSize>=10485760)
			{
				$('#errordiv').append("&#149; "+resourceJSON.msgfilesizelessthan+"<br>");
					if(focs==0)
						$('#pathOfCertification').focus();
					
					$('#pathOfCertification').css("background-color",txtBgColor);
					cnt++;focs++;	
					return false;
			}
		}
	}
	
 }	//		val end
		if(cnt!=0)		
		{
			updateReturnThreadCount("cert");
			$('#errordiv_Certification').show();
			return false;
		}else if(districtIdForDSPQ==7800038){
		
			/*var certificationStatusMasterObj= {certificationStatusId:dwr.util.getValue("certificationStatusMaster")};
			certificate = {certId:certId.value, stateMaster:null,yearReceived:null,certType:null,certUrl:null,
					certificationStatusMaster:certificationStatusMasterObj,pkOffered:null,kgOffered:null,g01Offered:null,g02Offered:null,
					g03Offered:null,g04Offered:null,g05Offered:null,g06Offered:null,g07Offered:null,g08Offered:null,
					g09Offered:null,g10Offered:null,g11Offered:null,g12Offered:null,readingQualifyingScore:null,
					writingQualifyingScore:null,mathematicsQualifyingScore:null,certificationTypeMaster:null,
					yearExpired:null,doeNumber:null,certText:certText};*/
			
			PFCertifications.saveOrUpdateCertificationsDSPQNoble(certId.value,certificationStatusMaster,certText,{
				async: true,
				errorHandler:handleError,
				callback:function(data)
					{
				
				if(data==0){
					errorHandler:handleError
					   }

					hideForm_Certification();
					showGridCertifications();

					if(sbtsource==1)
									{
										/*if(certificationTypeMaster==2 || certificationTypeMaster==3)
										{
											try{ document.getElementById("displayGKAndSubject").value=true; } catch(e){}
											showHideGK_SubjectArea();
											saveAllDSPQ();
											saveAndContinueBottomPart();
											updateReturnThreadCount("cert");
											validatePortfolioErrorMessageAndGridData('level2');
										}
										else
										{
											try{ document.getElementById("displayGKAndSubject").value=false; } catch(e){}
											showHideGK_SubjectArea();
											saveAllDSPQ();
											saveAndContinueBottomPart();
											updateReturnThreadCount("cert");
											validatePortfolioErrorMessageAndGridData('level2');
										}*/
										
										displayGKAndSubject();
										saveAllDSPQ();
										saveAndContinueBottomPart();
										updateReturnThreadCount("cert");
//										validatePortfolioErrorMessageAndGridData('level2');
										
									}
									else
									{
										/*if(certificationTypeMaster==2 || certificationTypeMaster==3)
										{
											try{ document.getElementById("displayGKAndSubject").value=true; } catch(e){}
											showHideGK_SubjectArea();
										}
										else
										{
											try{ document.getElementById("displayGKAndSubject").value=false; } catch(e){}
											showHideGK_SubjectArea();
										}*/
										displayGKAndSubject();
									}
					}
			});
		}
		else
		{
			var stateObj = {stateId:dwr.util.getValue("stateMaster")};
			var certificationStatusMasterObj= {certificationStatusId:dwr.util.getValue("certificationStatusMaster")};
			//var certTypeObj = {certTypeId:dwr.util.getValue("certificateTypeMaster")};
			
			var certificate =null;
			
			if(dwr.util.getValue("certificationStatusMaster")=="")
			{
				certificate = {certId:null, stateMaster:stateObj,yearReceived:trim(yearReceived.value),certType:null,certUrl:null,
						certificateTypeMaster:certTypeObj,pkOffered:null,kgOffered:null,g01Offered:null,g02Offered:null,
						g03Offered:null,g04Offered:null,g05Offered:null,g06Offered:null,g07Offered:null,g08Offered:null,
						g09Offered:null,g10Offered:null,g11Offered:null,g12Offered:null,readingQualifyingScore:null,
						writingQualifyingScore:null,mathematicsQualifyingScore:null,certText:null,ieinNumber:IEINNumber,certiExpiration:certiExpiration};
			}
			else
			{
				certificate = {certId:null,certificationStatusMaster:certificationStatusMasterObj,certUrl:null,
						stateMaster:stateObj,yearReceived:trim(yearReceived.value),certType:trim(certType.value),certificateTypeMaster:certTypeObj,
						pkOffered:null,kgOffered:null,g01Offered:null,g02Offered:null,g03Offered:null,
						g04Offered:null,g05Offered:null,g06Offered:null,g07Offered:null,g08Offered:null,
						g09Offered:null,g10Offered:null,g11Offered:null,g12Offered:null,readingQualifyingScore:null,
						writingQualifyingScore:null,mathematicsQualifyingScore:null,certificationTypeMaster:certificationtypeMasterObj,
						yearExpired:yearExpires,doeNumber:doenumber,certText:certText,ieinNumber:IEINNumber,certiExpiration:certiExpiration};
			}
		
			dwr.util.getValues(certificate);
			dwr.engine.beginBatch();
			
			var certiFile = document.getElementById("pathOfCertification").value;
			//alert("certiFile "+certiFile);
			if(certiFile!=""){
				certificate.pathOfCertification=certiFile;
			}
			if(fileName!="")
			{	
				//$('#loadingDiv').show();	
				//document.getElementById("frmCertificate").submit();
				
				PFCertifications.findDuplicateCertificate(certificate,{
					async: true,
					errorHandler:handleError,
					callback:function(data)
						{
							if(data)
							{
								$('#errordiv').append("&#149; "+resourceJSON.msgCertificationLicensure+"<br>");
								$('#certType').focus();
								$('#certType').css("background-color",txtBgColor);
								updateReturnThreadCount("cert");
								$('#errordiv').show();
								return false;
							}else{
								//$('#loadingDiv').show();
								document.getElementById("frmCertificate").submit();
							}
						}
					});
				
			}
			else
			{
				//alert(" IEINNumber "+IEINNumber+" certificationStatusMaster "+certificationStatusMaster);
				if(certificationStatusMaster==5){
					certificate = {certId:certId.value, stateMaster:null,yearReceived:null,certType:null,certUrl:null,
							certificationStatusMaster:certificationStatusMasterObj,pkOffered:null,kgOffered:null,g01Offered:null,g02Offered:null,
							g03Offered:null,g04Offered:null,g05Offered:null,g06Offered:null,g07Offered:null,g08Offered:null,
							g09Offered:null,g10Offered:null,g11Offered:null,g12Offered:null,readingQualifyingScore:null,
							writingQualifyingScore:null,mathematicsQualifyingScore:null,certificationTypeMaster:null,
							yearExpired:null,doeNumber:null,certText:certText,ieinNumber:IEINNumber,certiExpiration:certiExpiration};
				}

				PFCertifications.saveOrUpdate(certificate,{
					async: true,
					errorHandler:handleError,
					callback:function(data)
						{
							if(data==1)
							{
								updateReturnThreadCount("cert");
								$('#errordiv_Certification').append("&#149; "+resourceJSON.msgCertificationLicensure+"<br>");
								$('#certType').focus();
								$('#certType').css("background-color",txtBgColor);
								$('#errordiv_Certification').show();
								return false;
							}else if(data==0){
								errorHandler:handleError
							}
							//$('#loadingDiv').hide();
							hideForm_Certification();
							showGridCertifications();
							if(sbtsource==1)
							{
								/*if(certificationTypeMaster==2 || certificationTypeMaster==3)
								{
									try{ document.getElementById("displayGKAndSubject").value=true; } catch(e){}
									showHideGK_SubjectArea();
									saveAllDSPQ();
									saveAndContinueBottomPart();
									updateReturnThreadCount("cert");
									validatePortfolioErrorMessageAndGridData('level2');
								}
								else
								{
									try{ document.getElementById("displayGKAndSubject").value=false; } catch(e){}
									showHideGK_SubjectArea();
									saveAllDSPQ();
									saveAndContinueBottomPart();
									updateReturnThreadCount("cert");
									validatePortfolioErrorMessageAndGridData('level2');
								}*/
								
								displayGKAndSubject();
								saveAllDSPQ();
								saveAndContinueBottomPart();
								updateReturnThreadCount("cert");
//								validatePortfolioErrorMessageAndGridData('level2');
								
							}
							else
							{
								/*if(certificationTypeMaster==2 || certificationTypeMaster==3)
								{
									try{ document.getElementById("displayGKAndSubject").value=true; } catch(e){}
									showHideGK_SubjectArea();
								}
								else
								{
									try{ document.getElementById("displayGKAndSubject").value=false; } catch(e){}
									showHideGK_SubjectArea();
								}*/
								displayGKAndSubject();
							}
							
						}
					});
			}
			/*PFCertifications.saveOrUpdate(certificate,certUrl,
			{ 
				async: true,
				errorHandler:handleError,
				callback:function(data)
				{
					if(data==0)
					{
						
						$('#errordiv_Certification').append("&#149; This Certification Name is already added, Please select another Certification Name<br>");
						$('#certType').focus();
						$('#certType').css("background-color",txtBgColor);
						$('#errordiv_Certification').show();
						return false;
					}
					showGridCertifications();
					//$("#hrefCertifications").addClass("stepactive");
					hideForm_Certification();
					validatePortfolioErrorMessageAndGridData('level2');
					return true;
				}
			});*/
			dwr.engine.endBatch();
			return true;
		}
	}
	
		
}



function saveAndContinueBottomPart()
{
	//alert('saveAndContinueBottomPart');//sekhar
	$('#errordivspecificquestion').empty();
	$('#errordiv_bottomPart_TFA').empty();
	if($('#errordiv_bottomPart_tfaOptions').length>0){
		$('#errordiv_bottomPart_tfaOptions').empty();
	}
	$('#errordiv_bottomPart_divstdTch').empty();
	$('#errordiv_bottomPart_wst').empty();
	$('#errordiv_bottomPart_resume').empty();
	$('#errordiv_bottomPart_phone').empty();
	$('#errAddress1').empty();
//	$('#multyErrDiv').empty();
	$('#errCountry').empty();
	$('#errZip').empty();
	$('#errState').empty();
	$('#errCity').empty();
	$('#errExpCTT').empty();
	$('#errNBCY').empty();
	$('#errAffidavit').empty();
	$('#errPersonalInfoAndSSN').empty();
	$('#errFormerEmployee').empty();
	$('#errRace').empty();
	$('#errGeneralKnowledge').empty();
	//$('#errSubjectArea').empty();
	$('#errAdditionalDocuments').empty();
	$('#errEthnicOrigin').empty();
	$('#errEthinicity').empty();
	$('#errGender').empty();
	$('#errDOB').empty();
	$('#errRetireNo').empty();
	$('#errordivvideoLinks').empty();
	$('#errExpSalary').empty();
	
	
	
	
	var tFA_config=document.getElementById("tFA_config").value;
	var ssn_config=document.getElementById("ssn_config").value;
	var dateOfBirth_config=document.getElementById("dateOfBirth_config").value;
	var address_config=document.getElementById("address_config").value;
	var phone_config=document.getElementById("phone_config").value;
	var veteran_config=document.getElementById("veteran_config").value;
	var retireNo_config=document.getElementById("retireNo_config").value;
	var formeremployee_config=document.getElementById("formeremployee_config").value;
	var race_config=document.getElementById("race_config").value;
	var firstname_conf		= document.getElementById("firstname_conf").value;
	var lastname_conf		= document.getElementById("lastname_conf").value;
	var gender_config=document.getElementById("gender_config").value;
	var ethnicOrigin_config=document.getElementById("ethnicOrigin_config").value;
	var ethinicity_config=document.getElementById("ethinicity_config").value;
	var wst_config=document.getElementById("wst_config").value;
	var resume_config=document.getElementById("resume_config").value;
	var exp_config=document.getElementById("exp_config").value;
	var nbc_config=document.getElementById("nbc_config").value;
	var affidavit_config=document.getElementById("affidavit_config").value;
	var generalKnowledge_config=document.getElementById("generalKnowledge_config").value;
	var subjectAreaExam_config=document.getElementById("subjectAreaExam_config").value;
	
	
	
	var personalinfo_config=document.getElementById("personalinfo_config").value;
	var additionalDocuments_config=document.getElementById("additionalDocuments_config").value;
	var employment_config=document.getElementById("employment_config").value;
	var videoLink_config=document.getElementById("videoLink_config").value;
	var dSPQuestions_config=document.getElementById("dSPQuestions_config").value;
	var dSPQuestions_config=document.getElementById("dspq_conf").value;
	
	var affflag='';
	try{
	if (document.getElementById('affidavit').checked){
	var affidavit=document.getElementsByName("affidavit");
	if(document.getElementById('affidavit').checked)
		affflag=true;
	else 
		affflag=false;
	}
	}
	catch(ee){}
	
	var tfaAffiliate 	= 	document.getElementById("tfaAffiliate").value;
	var corpsYear 		= 	document.getElementById("corpsYear").value;
	var tfaRegion 		= 	document.getElementById("tfaRegion").value;
	
	var phoneNumber=""; //document.getElementById("phoneNumber").value;
	
	var phoneNumber1=document.getElementById("phoneNumber1").value;
	var phoneNumber2=document.getElementById("phoneNumber2").value;
	var phoneNumber3=document.getElementById("phoneNumber3").value;
	
	if(phoneNumber1!="" && phoneNumber2!="" && phoneNumber3!="")
		if(phoneNumber1.length==3 && phoneNumber2.length==3 && phoneNumber3.length==4)
			phoneNumber=phoneNumber1+phoneNumber2+phoneNumber3;
	
	var hdnResume = document.getElementById("hdnResume").value;
	var resumeFile = document.getElementById("resume");
	var addressLine1 = document.getElementById("addressLine1");
	var addressLine2 = document.getElementById("addressLine2");
	var zipCode = document.getElementById("zipCode");
	
	var stateIdForDSPQ = "";
	var cityIdForDSPQ = "";
	var countryId = document.getElementById("countryId").value;

	var districtIdForDSPQ = $("#districtIdForDSPQ").val();
	
	/*if(countryId==223)
	{
		stateIdForDSPQ = document.getElementById("stateIdForDSPQ").value;
		cityIdForDSPQ = document.getElementById("cityIdForDSPQ").value;
	}
	else
	{
		stateIdForDSPQ = document.getElementById("otherState").value;
		cityIdForDSPQ = document.getElementById("otherCity").value;
	}*/

	if(countryId!="")
	{
		if(countryId==223)
		{
			stateIdForDSPQ = document.getElementById("stateIdForDSPQ").value;
			cityIdForDSPQ = document.getElementById("cityIdForDSPQ").value;
		}
		else
		{
			if(document.getElementById("countryCheck").value==1)
			{
				stateIdForDSPQ = document.getElementById("stateIdForDSPQ").value;
				cityIdForDSPQ = document.getElementById("otherCity").value;
			}
			else
			{
				stateIdForDSPQ = document.getElementById("otherState").value;
				cityIdForDSPQ = document.getElementById("otherCity").value;
			}
		}
	}
	
	var expCertTeacherTraining = document.getElementById("expCertTeacherTraining").value;
	var nbc1 = document.getElementById("nbc1");
	var nbc2 = document.getElementById("nbc2");
	var nationalBoardCertYear = document.getElementById("nationalBoardCertYear").value;
	
	var isNonTeacher = document.getElementById("isNonTeacher").checked;
	var salutation_pi=$("#salutation_pi").val();
	var firstName_pi=$("#firstName_pi").val();
	var middleName_pi=$("#middleName_pi").val();
	var lastName_pi=$("#lastName_pi").val();
	var anotherName_pi=$("#anotherName").val();
	var ssn_pi=$("#ssn_pi").val();
	var expectedSalary_pi=$("#expectedSalary").val();
	var dobMonth=$("#dobMonth").val();
	var dobDay=$("#dobDay").val();
	var dobYear=$("#dobYear").val();
	
	var dob="";
	
	var vt1=$("#vt1").val();
	var vt2=$("#vt2").val();
	var veteranValue="";
	
	if (document.getElementById('vt1').checked) {
		veteranValue = document.getElementById('vt1').value;
	}else if (document.getElementById('vt2').checked) {
		veteranValue = document.getElementById('vt2').value;
	}
	
	
	
	var rtDate=$("#rtDate").val();
	var wdDate=$("#wdDate").val();
	var retireNo=$("#retireNo").val();
	var stMForretire=$("#stMForretire").val();
	var distForRetire=$("#retireddistrictId").val();
	
	var employeeType="";
	var formerEmployeeNo="";;
	var currentEmployeeNo="";
	var noLongerEmployed="";
	
	var isCurrentFullTimeTeacher="";
	
	var fileName = resumeFile.value;
	var ext = fileName.substr(fileName.lastIndexOf('.') + 1).toLowerCase();
	var fileSize=0;		
	if ($.browser.msie==true){	
	    fileSize = 0;	   
	}else{		
		if(resumeFile.files[0]!=undefined)
		fileSize = resumeFile.files[0].size;
	}
	
	
	var canServeAsSubTeacher=2;
	try{
		if (document.getElementById('canServeAsSubTeacher0').checked) {
			canServeAsSubTeacher = document.getElementById('canServeAsSubTeacher0').value;
		}else if (document.getElementById('canServeAsSubTeacher1').checked) {
			canServeAsSubTeacher = document.getElementById('canServeAsSubTeacher1').value;
		}
	}catch(err){alert(err);}
	
	var cnt_resume=0;
	var cnt_tfa=0;
	var cnt_wst=0;
	var cnt_ph=0;
	var cnt_address1=0;
	var cnt_zip=0;
	var cnt_state=0;
	var cnt_city=0;
	var cnt_Country=0;
	var cnt_ectt=0;
	var cnt_nbcy=0;
	var cnt_affdt=0;
	var focs=0;	

	var cnt_PersonalInfo=0;
	var cnt_SSN=0;
	var cnt_Race=0;
	var cnt_EthnicOrigin=0;
	var cnt_Ethinicity=0;
	var cnt_FormerEmployee=0;
	var cnt_Veteran=0;
	var cnt_Gender=0;
	var cnt_ExpSalary=0;
	
	
	
	var cnt_GeneralKnowledge=0;
	var cnt_SubjectAreaExam=0;
	var cnt_AdditionalDocuments=0;
	var cnt_RetireNo=0;
	var cnt_videoLink=0;
	var cnt_dspq=0;

	if(dSPQuestions_config==1)
	{
  //alert("dSPQuestions_config::"+dSPQuestions_config);
		
		$('#errordivspecificquestion').empty();
		var arr =[];
		var jobOrder = {jobId:document.getElementById("jobId").value};
		var dspqType="";
		try{
			dspqType=document.getElementById("dspqType").value;
		} catch(e){}
		var isRequiredCount=0;
		var ansRequiredCount=0;
		for(i=1;i<=totalQuestionsList;i++)
		{   
			var schoolMaster="";
			var isRequiredAns=0;
			var isRequired = dwr.util.getValue("QS"+i+"isRequired");
			if(isRequired==1){
				isRequiredCount++;
			}			
			var districtSpecificQuestion = {questionId:dwr.util.getValue("QS"+i+"questionId")};
			var questionTypeShortName = dwr.util.getValue("QS"+i+"questionTypeShortName");
			var questionTypeMaster = {questionTypeId:dwr.util.getValue("QS"+i+"questionTypeId")};			
			var qType = dwr.util.getValue("QS"+i+"questionTypeShortName");
			var o_maxMarks = dwr.util.getValue("o_maxMarksS");
			if(qType=='tf' || qType=='slsel' ||  qType=='slsel')
			{
				var optId="";
				var errorFlag=1;
				var isValidAnswer=false;
				if($("input[name=QS"+i+"opt]:radio:checked").length > 0 )
				{
					 errorFlag=0;
					optId=$("input[name=QS"+i+"opt]:radio:checked").val();
				}else if(isRequired==1){
					$("#errordivspecificquestion").html("&#149; "+resourceJSON.msgProvideResToAllQ+"<br>");
					dSPQuestionsErrorCount=1;
					errorFlag=1;
					cnt_dspq=1;
				}

				isValidAnswer =dwr.util.getValue("QS"+optId+"validQuestion")
				if(dwr.util.getValue("QS"+i+"question").indexOf(""+resourceJSON.msgPartTimeTeachingPos+"") >= 0 && document.getElementById("districtIdForDSPQ").value==614730){
					var nextQ = i+1;
					document.getElementById("QS"+nextQ+"isRequired").value=0;
					if(isValidAnswer==true){
						document.getElementById("QS"+nextQ+"isRequired").value=1;
					}
				}
				if(errorFlag==0){
					if(isRequired==1){
						isRequiredAns=1;
					}
					arr.push({ 
						"selectedOptions"  : optId,
						"question"  : dwr.util.getValue("QS"+i+"question"),
						"questionTypeMaster" : questionTypeMaster,
						"questionType" : questionTypeShortName,
						"questionOption" : dwr.util.getValue("qOptS"+optId),
						"districtSpecificPortfolioQuestions" : districtSpecificQuestion,
						"isValidAnswer" : dwr.util.getValue("QS"+optId+"validQuestion"),
						"jobOrder" : jobOrder						
					});
				}
	
			}else if(qType=='ml' || qType=='sl')
			{
				var insertedText = dwr.util.getValue("QS"+i+"opt");
				if((insertedText!=null && insertedText!="") || isRequired==0)
				{
					if(isRequired==1){
						isRequiredAns=1;
					}
					
					if($(".school"+i).length>0){
						schoolMaster=$(".school"+i).val();
					}
					
					arr.push({ 
						"insertedText"    : insertedText,
						"question"  : dwr.util.getValue("QS"+i+"question"),
						"questionTypeMaster" : questionTypeMaster,
						"questionType" : questionTypeShortName,
						"districtSpecificPortfolioQuestions" : districtSpecificQuestion,
						"jobOrder" : jobOrder,
						"schoolIdTemp" : schoolMaster
					});
				}else
				{
					if(isRequired==1){
						$("#errordivspecificquestion").html("&#149; "+resourceJSON.msgProvideResToAllQ+"<br>");
						dSPQuestionsErrorCount=1;
						cnt_dspq=1;
					}
				}
			}else if(qType=='et' || qType=='sswc'){
				var optId="";
				var errorFlag=1;
				if($("input[name=QS"+i+"opt]:radio:checked").length > 0 )
				{
					errorFlag=0;
					optId=$("input[name=QS"+i+"opt]:radio:checked").val();
				}else
				{
					if(isRequired==1){
						errorFlag=1;
						$("#errordivspecificquestion").html("&#149; "+resourceJSON.msgProvideResToAllQ+"<br>");
						dSPQuestionsErrorCount=1;
						cnt_dspq=1;
					}
				}
				if($("input[name=QS"+i+"opt]:radio").length==0)
					errorFlag=0;
				
				var insertedText = "";
				var isValidAnswer = dwr.util.getValue("QS"+optId+"validQuestion");
				
				if(dwr.util.getValue("QS"+i+"question")=="Have you previously worked for UNO?"){
					if(isValidAnswer==true && (dwr.util.getValue("QS"+i+"optet1").trim()=="" || dwr.util.getValue("QS"+i+"optet2").trim()=="")){						
						if(isRequired==1){
							$("#errordivspecificquestion").html("&#149; "+resourceJSON.msgProvideResToAllQ+"<br>");
							dSPQuestionsErrorCount=1;
							errorFlag=1;
							cnt_dspq=1;
						}
					}
					insertedText=dwr.util.getValue("QS"+i+"optet1")+"##"+dwr.util.getValue("QS"+i+"optet2");
					
				}else{
					insertedText=dwr.util.getValue("QS"+i+"optet");
					if(isValidAnswer==true && insertedText.trim()==""){
						if(isRequired==1){
							$("#errordivspecificquestion").html("&#149;"+resourceJSON.msgProvideResToAllQ+"<br>");
							dSPQuestionsErrorCount=1;
							errorFlag=1;
							cnt_dspq=1;
						}
					}
				}
				if(errorFlag==0){
					if(isRequired==1){
						isRequiredAns=1;
					}
					arr.push({ 
						"selectedOptions"  : optId,
						"question"  : dwr.util.getValue("QS"+i+"question"),
						"insertedText"    : insertedText,
						"questionTypeMaster" : questionTypeMaster,
						"questionType" : questionTypeShortName,
						"questionOption" : dwr.util.getValue("qOptS"+optId),
						"districtSpecificPortfolioQuestions" : districtSpecificQuestion,
						"isValidAnswer" : isValidAnswer,
						"jobOrder" : jobOrder
					});
				}
			}else if(qType=='mlsel'){
				try{
					 var multiSelectArray="";
					 var inputs = document.getElementsByName("multiSelect"+i); 
					 for (var j = 0; j < inputs.length; j++) {
					        if (inputs[j].type === 'checkbox') {
					        	if(inputs[j].checked){
					        		multiSelectArray+=	inputs[j].value+"|";
					            }
					        }
					} 
					if(multiSelectArray!=""  || isRequired==0){
						if(isRequired==1){
							isRequiredAns=1;
						} 
						arr.push({ 
							"selectedOptions"  : multiSelectArray,
							"question"  : dwr.util.getValue("QS"+i+"question"),
							"questionTypeMaster" : questionTypeMaster,
							"questionType" : questionTypeShortName,
							//"questionOption" : dwr.util.getValue("qOptS"+optId),
							"districtSpecificPortfolioQuestions" : districtSpecificQuestion,
							"isValidAnswer" : isValidAnswer,
							"jobOrder" : jobOrder
						});
					}else{
						if(isRequired==1){
							$("#errordivspecificquestion").html("&#149;"+resourceJSON.msgProvideResToAllQ+"<br>");
							dSPQuestionsErrorCount=1;
							cnt_dspq=1;
						}
					}
				}catch(err){}
			}if(qType=='mloet'){
				try{
					 var multiSelectArray="";
					 var inputs = document.getElementsByName("multiSelect"+i); 
					 for (var j = 0; j < inputs.length; j++) {
					        if (inputs[j].type === 'checkbox') {
					        	if(inputs[j].checked){
					        		multiSelectArray+=	inputs[j].value+"|";
					            }
					        }
					} 
					var insertedText = dwr.util.getValue("QS"+i+"optmloet"); 
					if(multiSelectArray!="" || isRequired==0){
						if(isRequired==1){
							isRequiredAns=1;
						} 
						arr.push({ 
							"selectedOptions"  : multiSelectArray,
							"question"  : dwr.util.getValue("QS"+i+"question"),
							"insertedText"    : insertedText,
							"questionTypeMaster" : questionTypeMaster,
							"questionType" : questionTypeShortName,
							//"questionOption" : dwr.util.getValue("qOptS"+optId),
							"districtSpecificPortfolioQuestions" : districtSpecificQuestion,
							"isValidAnswer" : isValidAnswer,
							"jobOrder" : jobOrder
						});
					}else{
						if(isRequired==1){
							$("#errordivspecificquestion").html("&#149;"+resourceJSON.msgProvideResToAllQ+"<br>");
							dSPQuestionsErrorCount=1;
							cnt_dspq=1;
						}
					}
				}catch(err){}
			}else if(qType=='rt'){
				var optId="";
				var score="";
				var rank="";
				var scoreRank=0;
				var opts = document.getElementsByName("optS");
				var scores = document.getElementsByName("scoreS");
				var ranks = document.getElementsByName("rankS");
				var o_ranks = document.getElementsByName("o_rankS");
				
				var tt=0;
				var uniqueflag=false;
				for(var i = 0; i < opts.length; i++) {
					optId += opts[i].value+"|";
					score += scores[i].value+"|";
					rank += ranks[i].value+"|";
					if(checkUniqueRankForPortfolio(ranks[i]))
					{
						uniqueflag=true;
						break;
					}
	
					if(ranks[i].value==o_ranks[i].value)
					{
						scoreRank+=parseInt(scores[i].value);
					}
					if(ranks[i].value=="")
					{
						tt++;
					}
				}
				if(uniqueflag)
					return;
	
				if(tt!=0)
					optId=""; 
	
				if(optId=="")
				{
					//totalSkippedQuestions++;
					//strike checking
				}
				var totalScore = scoreRank;
				if(isRequired==1){
					isRequiredAns=1;
				} 
				arr.push({ 
					"selectedOptions"  : optId,
					"question"  : dwr.util.getValue("QS"+i+"question"),
					"questionTypeMaster" : questionTypeMaster,
					"questionType" : questionTypeShortName,
					"questionOption" : dwr.util.getValue("qOptS"+optId),
					"districtSpecificPortfolioQuestions" : districtSpecificQuestion,
					"isValidAnswer" : isValidAnswer,
					"jobOrder" : jobOrder,
					"optionScore"      : score,
					"totalScore"       : totalScore,
					"insertedRanks"    : rank,
					"maxMarks" :o_maxMarks
				});
			}else if(qType=='OSONP'){
			//	alert("hello "+qType);
				try{
					 var multiSelectArray="";
					 var inputs = document.getElementsByClassName("OSONP"+i); 
					 for (var j = 0; j < inputs.length; j++) {
					        if (inputs[j].type === 'radio') {
					        	if(inputs[j].checked){
					        		multiSelectArray+=	inputs[j].value+"|";
					            }
					        }
					} 
					var insertedText = dwr.util.getValue("QS"+i+"OSONP");
					var isValidAnswer = dwr.util.getValue("QS"+multiSelectArray+"validQuestion");							
					if(isValidAnswer==true && insertedText.trim()==""){
						if(isRequired==1){
							$("#errordivspecificquestion").html("&#149;"+resourceJSON.msgProvideResToAllQ+"<br>");
							cnt_dspq=1;
							if(errorFlagCheck==0){
								errorFlagCheck=1;
								$("#divErrorMsg_dynamicPortfolio").append("&#149;"+resourceJSON.msgProvideResToAllQ+"<br>");
								cnt_dspq=1;
							}
							iErrorCount++;
							document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
							dSPQuestionsErrorCount=1;
						}
					}
					
					if(multiSelectArray!="" || isRequired==0){
						if(isRequired==1){
							isRequiredAns=1;
						} 
						arr.push({ 
							"selectedOptions"  : multiSelectArray,
							"question"  : dwr.util.getValue("QS"+i+"question"),
							"insertedText"    : insertedText,
							"questionTypeMaster" : questionTypeMaster,
							"questionType" : questionTypeShortName,
							//"questionOption" : dwr.util.getValue("qOptS"+optId),
							"districtSpecificPortfolioQuestions" : districtSpecificQuestion,
							"isValidAnswer" : isValidAnswer,
							"jobOrder" : jobOrder
						});
					}else{
						if(isRequired==1){
							$("#errordivspecificquestion").html("&#149;"+resourceJSON.msgProvideResToAllQ+"<br>");
							dSPQuestionsErrorCount=1;
							cnt_dspq=1;
						}
					}
				}catch(err){}
			}else if(qType=='DD'){
				try{
					
					 var multiSelectArray=$("#dropdown"+i+" :selected").val() ;				
				
					if(multiSelectArray!=""  || isRequired==0){
						if(isRequired==1){
							isRequiredAns=1;
						} 
						arr.push({ 
							"selectedOptions"  : multiSelectArray,
							"question"  : dwr.util.getValue("QS"+i+"question"),
							"questionTypeMaster" : questionTypeMaster,
							"questionType" : questionTypeShortName,
							//"questionOption" : dwr.util.getValue("qOptS"+optId),
							"districtSpecificPortfolioQuestions" : districtSpecificQuestion,
							"isValidAnswer" : isValidAnswer,
							"jobOrder" : jobOrder
						});
					}else{
						if(isRequired==1){
							$("#errordivspecificquestion").html("&#149;"+resourceJSON.msgProvideResToAllQ+"<br>");
							dSPQuestionsErrorCount=1;
							cnt_dspq=1;
						}
					}
				}catch(err){}
			}
			if(qType=='sscb'){
				try{
					var isValidAnswer = false;
					var insertedText = dwr.util.getValue("QS"+i+"multiselectText");
					 var multiSelectArray="";
					 var inputs = document.getElementsByName("multiSelect"+i); 
					 for (var j = 0; j < inputs.length; j++) {
					        if (inputs[j].type === 'checkbox') {
					        	if(inputs[j].checked){
					        		multiSelectArray+=	inputs[j].value+"|";
					        		if(isValidAnswer==false)
					        			isValidAnswer = dwr.util.getValue("QS"+inputs[j].value+"validQuestion")
					            }
					        }
					} 
					 
					 if($(".school"+i).length>0){
							schoolMaster=$(".school"+i).val();
						}
					 
				
					 if(isValidAnswer==true && insertedText.trim()==""){							
								$("#errordivspecificquestion").html("&#149;"+resourceJSON.msgProvideResToAllQ+"<br>");								
								dSPQuestionsErrorCount=1;
								errorFlag=1;
								cnt_dspq=1;
								
								
						}else{
							var sectCnt = $(".secHeading").length;
							var showsecerror=false;
								for ( var int = 0; int < sectCnt; int++) {			
									var cntSec= int+1;
									//alert($(".sectionCnt"+cntSec+":checked").length);
									var atLeastOneIsCheckedInsec = $(".sectionCnt"+cntSec+":checked").length;
									if(atLeastOneIsCheckedInsec==0){
										showsecerror=true;
									}
								}
								if(showsecerror){/*
									$("#divErrorMsg_dynamicPortfolio").append("&#149; Please provide response for each section in Question "+i);
									iErrorCount++;
									document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
									dSPQuestionsErrorCount=1;
									//$("#errordivspecificquestionUpload").show();
								*/}
							}
					 
				if(errorFlag==0){
					if(multiSelectArray!=""){
						if(isRequired==1){
							isRequiredAns=1;
						} 
						arr.push({ 
							"selectedOptions"  : multiSelectArray,
							"question"  : dwr.util.getValue("QS"+i+"question"),
							"insertedText"    : insertedText,
							"questionTypeMaster" : questionTypeMaster,
							"questionType" : questionTypeShortName,
							"schoolIdTemp" : schoolMaster,
							"districtSpecificPortfolioQuestions" : districtSpecificQuestion,
							"isValidAnswer" : isValidAnswer,
							"jobOrder" : jobOrder
						});
					}else{
						if(isRequired==1){
							$("#errordivspecificquestion").html("&#149;"+resourceJSON.msgProvideResToAllQ+"<br>");
							cnt_dspq=1;
							dSPQuestionsErrorCount=1;
						}
					}
				}
				}catch(err){}
			}
			if(qType=='UAT'){
							
				try{
					var errorFlag=0;
					
					var cntErr=0;
					var fileNameQuestion = dwr.util.getValue("QS"+i+"File").value;
					var ext = fileNameQuestion.substr(fileNameQuestion.lastIndexOf('.') + 1).toLowerCase();

					if(isRequired==1 && fileNameQuestion=="")
					{
						$('#errordivspecificquestionUpload').html("&#149; "+resourceJSON.msgPleaseupload+" "+dwr.util.getValue("QS"+i+"question")+".</BR>");
						errorFlag=1;
						cntErr++;
						cnt_dspq=1;
					}
					if(ext!="")
					{
						if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt'))
						{
							$('#errordivspecificquestionUpload').html("&#149; "+resourceJSON.msgselectAcceptable+" "+dwr.util.getValue("QS"+i+"question")+" formats which include PDF, MS-Word, GIF, PNG, and JPEG  files</BR>");
							errorFlag=1;
							cntErr++;
							cnt_dspq=1;
						}
					}
					
					if(cntErr==0 && fileNameQuestion!=""){
						var newName="proof_of_highly_qualified_"+fileNameQuestion.substr(fileNameQuestion.lastIndexOf('\\') + 1).toLowerCase()						
						$('#answerFileName').val(newName);
						document.getElementById('frmAnswerUpload').submit();						
					}else{
						$("#errordivspecificquestionUpload").show();
					}
					
					var isValidAnswer = false;
					var insertedText = dwr.util.getValue("QS"+i+"Text");
					var file = dwr.util.getValue("answerFileName");
					var multiSelectArray=$("#dropdown"+i+" :selected").val() ;

					if(cntErr==0 && errorFlag==0){
						if(multiSelectArray!=""){
							if(isRequired==1){
								isRequiredAns=1;
							} 
							if(fileNameQuestion==""){
								file=dwr.util.getValue("editCaseFile");
							}

							arr.push({ 
								"selectedOptions"  : multiSelectArray,
								"question"  : dwr.util.getValue("QS"+i+"question"),
								"insertedText"    : insertedText,
								"fileName"    : file,
								"questionTypeMaster" : questionTypeMaster,
								"questionType" : questionTypeShortName,
								//"schoolIdTemp" : schoolMaster,
								"districtSpecificPortfolioQuestions" : districtSpecificQuestion,
								"isValidAnswer" : isValidAnswer,
								"jobOrder" : jobOrder
							});
						}else{
							if(isRequired==1){
								$("#errordivspecificquestion").html("&#149;"+resourceJSON.msgProvideResToAllQ+"<br>");
								dSPQuestionsErrorCount=1;
								cnt_dspq=1;
							}
						}
					}
				}catch(err){}
			}
			if(isRequiredAns==1){
				ansRequiredCount++;
			}
		}
		
		//alert('isRequiredCount:::'+isRequiredCount);
		//alert('ansRequiredCount:::'+ansRequiredCount);
		/*if(isRequiredCount==ansRequiredCount) 
		{*/
		var disId=document.getElementById("districtIdForDSPQ").value;
			PFCertifications.setDistrictPortfolioAnswr(arr,dspqType,disId,{ 
				async: false,
				errorHandler:handleError,
				callback: function(data){
				if(data!=null)
				{
					arr =[];
				}
			}
			});	
		/*}else
		{
			$("#errordivspecificquestion").html("&#149; Please provide responses to all the questions since they are required.<br>");
			dSPQuestionsErrorCount=1;
		}*/
	


}
	
	$('#tfaAffiliate').css("background-color","");
	$('#corpsYear').css("background-color","");
	$('#tfaRegion').css("background-color","");
	$('.tfaOptId').css("background-color","");
	
	$('#resume').css("background-color","");
	
	//$('#phoneNumber').css("background-color","");
	$('#phoneNumber1').css("background-color","");
	$('#phoneNumber2').css("background-color","");
	$('#phoneNumber3').css("background-color","");
	$('#addressLine1').css("background-color","");
	$('#zipCode').css("background-color","");
	$('#stateIdForDSPQ').css("background-color","");
	$('#cityIdForDSPQ').css("background-color","");
	$('#otherState').css("background-color","");
	$('#otherCity').css("background-color","");
	$('#countryId').css("background-color","");
	
	$('#salutation_pi').css("background-color","");
	$('#firstName_pi').css("background-color","");
	$('#middleName_pi').css("background-color","");
	$('#lastName_pi').css("background-color","");
	$('#ssn_pi').css("background-color","");
	
	$('#dobMonth').css("background-color","");
	$('#dobDay').css("background-color","");
	$('#dobYear').css("background-color","");
	
	$('#rtDate').css("background-color","");
	$('#wdDate').css("background-color","");
	$('#retireNo').css("background-color","");
	
	try{
		$('#generalKnowledgeExamStatus').css("background-color","");
		$('#generalKnowledgeExamDate').css("background-color","");
		$('#generalKnowledgeScoreReport').css("background-color","");
	}catch(err){}
	try{
		$('#examStatus').css("background-color","");
		$('#examDate').css("background-color","");
		$('#subjectIdforDSPQ').css("background-color","");
		$('#scoreReport').css("background-color","");
	}catch(err){}
	
	if(firstname_conf==1)
	{
		if(trim(firstName_pi)=="")
		{
			$('#errPersonalInfoAndSSN').append("&#149; "+resourceJSON.PlzEtrFirstName+"<br>");
			$('#firstName_pi').css("background-color",txtBgColor);
			cnt_PersonalInfo++;focs++;
		}
	}
	if(lastname_conf==1){
		
		if(trim(lastName_pi)=="")
		{
			$('#errPersonalInfoAndSSN').append("&#149; "+resourceJSON.PlzEtrLastName+"<br>");
			$('#lastName_pi').css("background-color",txtBgColor);
			cnt_PersonalInfo++;focs++;
		}			
		
	}
	
	$('#errDOB').hide();
	$('#errDOB').empty();
	
	if(document.getElementById("districtIdForDSPQ").value==4218990 || document.getElementById("districtIdForDSPQ").value==1302010){
		var idobYear = new String(parseInt(trim(dobYear)));
		var currentFullYear = new Date().getFullYear();
		currentFullYear=currentFullYear-1;
		
		if(trim(dobMonth)!="0"){
			if(trim(dobDay)=="0")
			{
				$('#errDOB').append("&#149; "+resourceJSON.msgDayofBirth+"<br>");
				$('#dobDay').css("background-color",txtBgColor);
				cnt_PersonalInfo++;focs++;
			}
			
			if((dobYear=="") || ( dobYear==0 || idobYear=="NaN") || (idobYear > currentFullYear || idobYear < 1931))
			{
				$('#errDOB').append("&#149; "+resourceJSON.msgValidYearBirth+"<br>");
				$('#dobYear').css("background-color",txtBgColor);
				$('#errDOB').show();
				cnt_PersonalInfo++;focs++;
			}
		
		}else if(trim(dobDay)!="0" ){	
			if(trim(dobMonth)=="0")
			{
				$('#errDOB').append("&#149; "+resourceJSON.msgMonthofBirth+"<br>");
				$('#dobMonth').css("background-color",txtBgColor);
				cnt_PersonalInfo++;focs++;
			}
			
			if((dobYear=="") || ( dobYear==0 || idobYear=="NaN") || (idobYear > currentFullYear || idobYear < 1931))
			{
				$('#errDOB').append("&#149; "+resourceJSON.msgValidYearBirth+"<br>");
				$('#dobYear').css("background-color",txtBgColor);
				$('#errDOB').show();
				cnt_PersonalInfo++;focs++;
			}
		}else if((dobYear!="") || ( dobYear!=0 || idobYear!="NaN") || (idobYear < currentFullYear || idobYear > 1931)){
			if(trim(dobMonth)=="0")
			{
				$('#errDOB').append("&#149; "+resourceJSON.msgMonthofBirth+"<br>");
				$('#dobMonth').css("background-color",txtBgColor);
				cnt_PersonalInfo++;focs++;
			}
			
			if(trim(dobDay)=="0")
			{
				$('#errDOB').append("&#149; "+resourceJSON.msgDayofBirth+"<br>");
				$('#dobDay').css("background-color",txtBgColor);
				cnt_PersonalInfo++;focs++;
			}
		}

		if(trim(dobMonth)> 0 && trim(dobDay) > 0 && trim(dobYear) > 0)
	{
		dob=trim(dobMonth)+"-"+trim(dobDay)+"-"+trim(dobYear);
		document.getElementById("dob").value=dob;
	}
	$('#errDOB').show();
	}
	else if(dateOfBirth_config==1){
		if(trim(dobMonth)=="0")
		{
			$('#errDOB').append("&#149; "+resourceJSON.msgMonthofBirth+"<br>");
			$('#dobMonth').css("background-color",txtBgColor);
			cnt_PersonalInfo++;focs++;
		}
		
		if(trim(dobDay)=="0")
		{
			$('#errDOB').append("&#149; "+resourceJSON.msgDayofBirth+"<br>");
			$('#dobDay').css("background-color",txtBgColor);
			cnt_PersonalInfo++;focs++;
		}
		
		var idobYear = new String(parseInt(trim(dobYear)));
		var currentFullYear = new Date().getFullYear();
		currentFullYear=currentFullYear-1;
		
		if((dobYear=="") || ( dobYear==0 || idobYear=="NaN") || (idobYear > currentFullYear || idobYear < 1931))
		{
			$('#errDOB').append("&#149; "+resourceJSON.msgValidYearBirth+"<br>");
			$('#dobYear').css("background-color",txtBgColor);
			$('#errDOB').show();
			cnt_PersonalInfo++;focs++;
		}

		if(trim(dobMonth)> 0 && trim(dobDay) > 0 && trim(dobYear) > 0)
	{
		dob=trim(dobMonth)+"-"+trim(dobDay)+"-"+trim(dobYear);
		document.getElementById("dob").value=dob;
	}
	$('#errDOB').show();
	}
	
	if(ssn_config==1)
	{
		if(trim(ssn_pi)=="")
		{
			$('#errPersonalInfoAndSSN').append("&#149; "+resourceJSON.msgSocialSecurityNumber+"<br>");
			$('#ssn_pi').css("background-color",txtBgColor);
			cnt_SSN++;focs++;
		}
		/*else if(trim(ssn_pi).length < 4)
		{
			$('#errPersonalInfoAndSSN').append("&#149; Please enter 4 digits SSN.<br>");
			$('#ssn_pi').css("background-color",txtBgColor);
			cnt_SSN++;focs++;
		}*/
	}
	
	if(veteran_config==1)
	{
		if(trim(veteranValue)=="")
		{
			$('#errPersonalInfoAndSSN').append("&#149; "+resourceJSON.msgVeteran+"<br>");			
			cnt_Veteran++;focs++;
		}
		
		if(districtIdForDSPQ==1201470 && trim(veteranValue)=="1"){
			if($("#veteranOptS:checked").length==0){
				$('#errPersonalInfoAndSSN').append("&#149; "+resourceJSON.msgPleaseVeteranPrefrence+"<br>");			
				cnt_Veteran++;focs++;
				
			}else{
				setDistrictSpecificVeteranValues();
			}

		}
	}
	
	
	
	if(formeremployee_config==1)
	{
		if($('#fe2').is(':checked') || $('#fe1').is(':checked') || $('#fe3').is(':checked'))
		{
			
		}
		else
		{
			$('#errFormerEmployee').append("&#149; "+resourceJSON.msgCurrentEmployment1+"<br>");
			cnt_FormerEmployee++;focs++;
		}
		
		if($('#fe2').is(':checked'))
		{
			employeeType="1";
			currentEmployeeNo=$("#empfe2").val();
			if($('#rdCEmp1').is(':checked'))
				isCurrentFullTimeTeacher="1";
			else if($('#rdCEmp2').is(':checked'))
				isCurrentFullTimeTeacher="0";
			else if($('#rdCEmp3').is(':checked'))
				isCurrentFullTimeTeacher="2";
			
			if(currentEmployeeNo==null || trim(currentEmployeeNo)=='')
			{
				$('#errFormerEmployee').append("&#149; "+resourceJSON.msgEnterEmployeeNo+"</BR>");
				$('#empfe2').css("background-color","#F5E7E1");
				cnt_FormerEmployee++;focs++;
			}
			else
			{
				if(document.getElementById("districtIdForDSPQ").value!=4218990){
				$('#empfe2').css("background-color","");
				if(!$('input[name=rdCEmp]').is(":checked"))
				{
					$('#errFormerEmployee').append("&#149; "+resourceJSON.msgStaffMemtype+"</BR>");
					cnt_FormerEmployee++;focs++;
				}
				}
			}
			
		}
		else if($('#fe1').is(':checked'))
		{
			employeeType="0";
			formerEmployeeNo=$("#empfe1").val();
			if(districtIdForDSPQ==1302010)
				noLongerEmployed=$('#empfewDiv').find(".jqte_editor").text().trim();
				
			
			/*if($('#empchk11').is(':checked'))
			{
				if(trim(rtDate)=="")
				{
					$('#errFormerEmployee').append("&#149; Please enter Retirement Date<br>");
					$('#rtDate').css("background-color",txtBgColor);
					cnt_FormerEmployee++;focs++;
				}
				
			}	
			
			if($('#empchk12').is(':checked'))
			{
				if(trim(wdDate)=="")
				{
					$('#errFormerEmployee').append("&#149; Please enter money Withdrawl Date<br>");
					$('#wdDate').css("background-color",txtBgColor);
					cnt_FormerEmployee++;focs++;
				}
			}*/
		}
		else if($('#fe3').is(':checked'))	
		{
			employeeType="2";
		}
	}
	
	
	if(retireNo_config==1)
	{
		if(document.getElementById("isretired").checked==true)
		{
			if(trim(retireNo)=="")
			{
				$('#errRetireNo').append("&#149; "+resourceJSON.msgTeacherRetirementNumber+"<br>");
				cnt_RetireNo++;focs++;
			}
			if(distForRetire=="")
			{
				$('#errRetireNo').append("&#149; "+resourceJSON.PlzEtrRetiredFromDistrict+"<br>");
				cnt_RetireNo++;focs++;
			}
			if(stMForretire=="")
			{
				$('#errRetireNo').append("&#149; "+resourceJSON.msgRetiredFromState+"<br>");
				cnt_RetireNo++;focs++;
			}
		}
	}
	
	
	var isMiamiChk=document.getElementById("isMiami").value;
	var displayGKAndSubject=document.getElementById("displayGKAndSubject").value;
	var displayPassFailGK1=document.getElementById("displayPassFailGK").value;
	var IsSIForMiami1=document.getElementById("IsSIForMiami").value;
	var jobTitle=document.getElementById("jobTitleFeild").value;
	var isItvtForMiami=document.getElementById("isItvtForMiami").value;
	
	//if(generalKnowledge_config==1 && displayGKAndSubject=="true" && isMiamiChk=="true")
	var generalKnowledgeExamNote = $("#generalExamNote").find(".jqte_editor").html();	
	$('[name="generalExamNote"]').text(generalKnowledgeExamNote);
	
	var subjectExamTextarea = $("#subjectExamTextarea").find(".jqte_editor").html();	
	$('[name="subjectExamTextarea"]').text(subjectExamTextarea);
	//subjectExamTextarea
	
	if(isItvtForMiami=='false') // && generalKnowledge_source==0
	{
		if((generalKnowledge_config==1 && displayGKAndSubject=='true' && isMiamiChk=="true" && IsSIForMiami1=="false") || (generalKnowledge_config==1 && isMiamiChk=="true" && IsSIForMiami1=="true" && displayPassFailGK1=="false") || (displayGKAndSubject=='true' && document.getElementById("districtIdForDSPQ").value=="614730") || (displayGKAndSubject=='true' && document.getElementById("districtIdForDSPQ").value=="1302010")) // && generalKnowledge_source==0
		{
			var generalKnowledgeExamStatus = document.getElementById("generalKnowledgeExamStatus").value;
			var generalKnowledgeExamDate = document.getElementById("generalKnowledgeExamDate").value;
			var generalKnowledgeScoreReport = document.getElementById("generalKnowledgeScoreReport").value;
			var generalKnowledgeScoreReportHidden = document.getElementById("generalKnowledgeScoreReportHidden").value;
			
			
			if(jobTitle=="Teach For America 2015-2016"){

				if(generalKnowledgeExamStatus!=0 || generalKnowledgeExamDate!="" || trim(generalKnowledgeScoreReport)!="" && trim(generalKnowledgeScoreReportHidden)!=""){
				if(trim(generalKnowledgeExamStatus)=="0"){
					$('#errGeneralKnowledge').append("&#149; "+resourceJSON.msgExamStatus+"</BR>");
					$('#generalKnowledgeExamStatus').css("background-color",txtBgColor);
					cnt_GeneralKnowledge++;focs++;
				}
				
				
				if(trim(generalKnowledgeExamDate)==""){
					$('#errGeneralKnowledge').append("&#149; "+resourceJSON.msgExamDate+"</BR>");
					$('#generalKnowledgeExamDate').css("background-color",txtBgColor);
					cnt_GeneralKnowledge++;focs++;
				}
				
				if(trim(generalKnowledgeScoreReport)=="" && trim(generalKnowledgeScoreReportHidden)==""){
					$('#errGeneralKnowledge').append("&#149; Please upload Score Report</BR>");
					$('#generalKnowledgeScoreReport').css("background-color",txtBgColor);
					cnt_GeneralKnowledge++;focs++;
				}else{
					if(trim(generalKnowledgeScoreReport)!=""){
						var ext = generalKnowledgeScoreReport.substr(generalKnowledgeScoreReport.lastIndexOf('.') + 1).toLowerCase();	
						
						var fileSize = 0;
						if ($.browser.msie==true)
					 	{	
						    fileSize = 0;	   
						}
						else
						{
							if(document.getElementById("generalKnowledgeScoreReport").files[0]!=undefined)
							{
								fileSize = document.getElementById("generalKnowledgeScoreReport").files[0].size;
							}
						}
						
						if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt'))
						{
							$('#errGeneralKnowledge').append("&#149; "+resourceJSON.msgAcceptableScoreReportformats+"</BR>");
							$('#generalKnowledgeScoreReport').css("background-color",txtBgColor);
							cnt_GeneralKnowledge++;focs++;
						}else if(fileSize>=10485760){
							$('#errGeneralKnowledge').append("&#149; "+resourceJSON.msgScoreReportFilesize+"<br>");
							$('#generalKnowledgeScoreReport').css("background-color",txtBgColor);
							cnt_GeneralKnowledge++;focs++;
						}
					}
				}
			
				}
				
				
				
			}
			else{
			if(trim(generalKnowledgeExamStatus)=="0"){
				$('#errGeneralKnowledge').append("&#149; "+resourceJSON.msgExamStatus+"</BR>");
				$('#generalKnowledgeExamStatus').css("background-color",txtBgColor);
				cnt_GeneralKnowledge++;focs++;
			}
			
			
			if(trim(generalKnowledgeExamDate)==""){
				$('#errGeneralKnowledge').append("&#149; "+resourceJSON.msgExamDate+"</BR>");
				$('#generalKnowledgeExamDate').css("background-color",txtBgColor);
				cnt_GeneralKnowledge++;focs++;
			}
			
			if(trim(generalKnowledgeScoreReport)=="" && trim(generalKnowledgeScoreReportHidden)==""){
				$('#errGeneralKnowledge').append("&#149; Please upload Score Report</BR>");
				$('#generalKnowledgeScoreReport').css("background-color",txtBgColor);
				cnt_GeneralKnowledge++;focs++;
			}else{
				if(trim(generalKnowledgeScoreReport)!=""){
					var ext = generalKnowledgeScoreReport.substr(generalKnowledgeScoreReport.lastIndexOf('.') + 1).toLowerCase();	
					
					var fileSize = 0;
					if ($.browser.msie==true)
				 	{	
					    fileSize = 0;	   
					}
					else
					{
						if(document.getElementById("generalKnowledgeScoreReport").files[0]!=undefined)
						{
							fileSize = document.getElementById("generalKnowledgeScoreReport").files[0].size;
						}
					}
					
					if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt'))
					{
						$('#errGeneralKnowledge').append("&#149; "+resourceJSON.msgAcceptableScoreReportformats+"</BR>");
						$('#generalKnowledgeScoreReport').css("background-color",txtBgColor);
						cnt_GeneralKnowledge++;focs++;
					}else if(fileSize>=10485760){
						$('#errGeneralKnowledge').append("&#149; "+resourceJSON.msgScoreReportFilesize+"<br>");
						$('#generalKnowledgeScoreReport').css("background-color",txtBgColor);
						cnt_GeneralKnowledge++;focs++;
					}
				}
			}
		}
		}
	}
	
	if(subjectAreaExam_config==1)
	{/*
		var examStatus = document.getElementById("examStatus").value;
		var examDate = document.getElementById("examDate").value;
		var subjectIdforDSPQ = document.getElementById("subjectIdforDSPQ").value;
		var scoreReport = document.getElementById("scoreReport").value;
		var scoreReportHidden = document.getElementById("scoreReportHidden").value;
		
		if(trim(examStatus)=="0" && trim(examDate)=="" && trim(subjectIdforDSPQ)==0 && trim(scoreReport)=="" && trim(scoreReportHidden)=="" && displayGKAndSubject=='true')
		{
			//alert("T SB");
		}
		else
		{
			//alert("SB");
			if(trim(examStatus)=="0"){
				$('#errSubjectArea').append("&#149; Please select Exam Status.</BR>");
				$('#examStatus').css("background-color",txtBgColor);
				cnt_SubjectAreaExam++;focs++;
			}
			
			
			if(trim(examDate)==""){
				$('#errSubjectArea').append("&#149; Please enter Exam Date.</BR>");
				$('#examDate').css("background-color",txtBgColor);
				cnt_SubjectAreaExam++;focs++;
				
			}
			
			if(trim(subjectIdforDSPQ)==0){
				$('#errSubjectArea').append("&#149; Please select Subject.</BR>");
				$('#subjectIdforDSPQ').css("background-color",txtBgColor);
				cnt_SubjectAreaExam++;focs++;
			}
			
			
			
			if(trim(scoreReport)=="" && trim(scoreReportHidden)==""){
				$('#errSubjectArea').append("&#149; Please upload Score Report.</BR>");
				$('#scoreReport').css("background-color",txtBgColor);
				cnt_SubjectAreaExam++;focs++;
			}else{
				if(trim(scoreReport)!=""){
					var ext = scoreReport.substr(scoreReport.lastIndexOf('.') + 1).toLowerCase();	
					
					var fileSize = 0;
					if ($.browser.msie==true)
				 	{	
					    fileSize = 0;	   
					}
					else
					{
						if(document.getElementById("scoreReport").files[0]!=undefined)
						{
							fileSize = document.getElementById("scoreReport").files[0].size;
						}
					}
					
					if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt'))
					{
						$('#errSubjectArea').append("&#149; Please select Acceptable Score Report formats which include PDF, MS-Word, GIF, PNG, and JPEG  files.</BR>");
						$('#scoreReport').css("background-color",txtBgColor);
						cnt_SubjectAreaExam++;focs++;
					}else if(fileSize>=10485760){
						$('#errSubjectArea').append("&#149; Score Report File size must be less than 10mb.<br>");
						$('#scoreReport').css("background-color",txtBgColor);
						cnt_SubjectAreaExam++;focs++;
					}
				}
			}
		}
		
		
	*/}
	
	
		/*if(additionalDocuments_config==1){
		var documentName = document.getElementById("documentName").value;
		if(trim(documentName)==""){
			$('#errAdditionalDocuments').append("&#149; Please enter Document Name.</BR>");
			$('#documentName').css("background-color",txtBgColor);
			cnt_AdditionalDocuments++;focs++;
		}
		
		var uploadedDocument = document.getElementById("uploadedDocument").value;
		var uploadedDocumentHidden = document.getElementById("uploadedDocumentHidden").value;
		
		if(trim(uploadedDocument)=="" && trim(uploadedDocumentHidden)==""){
			$('#errAdditionalDocuments').append("&#149; Please upload Document.</BR>");
			$('#uploadedDocument').css("background-color",txtBgColor);
			cnt_AdditionalDocuments++;focs++;
		}else{
			if(trim(scoreReport)!=""){
				var ext = uploadedDocument.substr(uploadedDocument.lastIndexOf('.') + 1).toLowerCase();	
				
				var fileSize = 0;
				if ($.browser.msie==true)
			 	{	
				    fileSize = 0;	   
				}
				else
				{
					if(document.getElementById("uploadedDocument").files[0]!=undefined)
					{
						fileSize = document.getElementById("uploadedDocument").files[0].size;
					}
				}
				if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt'))
				{
					$('#errAdditionalDocuments').append("&#149; Please select Acceptable Document formats which include PDF, MS-Word, GIF, PNG, and JPEG  files.</BR>");
					$('#uploadedDocument').css("background-color",txtBgColor);
					cnt_AdditionalDocuments++;focs++;
				}else if(fileSize>=10485760){
					$('#errAdditionalDocuments').append("&#149; Document File size must be less than 10mb.<br>");
					$('#uploadedDocument').css("background-color",txtBgColor);
					cnt_AdditionalDocuments++;focs++;
				}
			}
		}
	}*/
	
	// For EthnicOrigin value
	var ethnicOriginValue=-1;
//	if(ethnicOrigin_config==1)
//	{
		var elements = document.getElementsByName('ethnicOriginId');
		for (i=0;i<elements.length;i++) 
		{
		  if(elements[i].checked) 
		  {
			  ethnicOriginValue=elements[i].value;
		  }
		}
		
		if(districtIdForDSPQ==7800038){}
		else if(districtIdForDSPQ==4218990){}
		else if(districtIdForDSPQ==3904380){}
		else if(districtIdForDSPQ==1201470){}
		else if(districtIdForDSPQ==7800040){}
		else if(districtIdForDSPQ==7800047){}
		else if(districtIdForDSPQ==614730){}
		else if(districtIdForDSPQ==5304860){}
		else if(districtIdForDSPQ==804800){}
		else if(districtIdForDSPQ==1302010){}
		else{
			if(ethnicOriginValue==-1)
			{
				//$('#errEthnicOrigin').append("&#149; "+resourceJSON.PlzSelectEhnicOrigin+"<br>");
				//$('#phoneNumber').css("background-color",txtBgColor);
				//cnt_EthnicOrigin++;focs++;
			}
		}
		
		//alert(" ethnicOriginValue "+ethnicOriginValue);
//	}
	
	//For Ethinicity value
	var ethinicityValue=-1;
//	if(ethinicity_config==1)
//	{
		var elements = document.getElementsByName('ethinicityId');
		for (i=0;i<elements.length;i++) 
		{
		  if(elements[i].checked) 
		  {
			  ethinicityValue=elements[i].value;
		  }
		}
		
		//alert(" ethinicityValue "+ethinicityValue);
		if(districtIdForDSPQ==7800038){}
		else if(districtIdForDSPQ==4218990){}
		else if(districtIdForDSPQ==3904380){}
		else if(districtIdForDSPQ==1201470){}
		else if(districtIdForDSPQ==7800040){}
		else if(districtIdForDSPQ==7800047){}
		else if(districtIdForDSPQ==614730){}
		else if(districtIdForDSPQ==5304860){}
		else if(districtIdForDSPQ==804800){}
		else if(districtIdForDSPQ==1302010){}
		else{
			if(ethinicityValue==-1)
			{
				//$('#errEthinicity').append("&#149; "+resourceJSON.msgSelectEthnicity1+"<br>");
				//$('#phoneNumber').css("background-color",txtBgColor);
				//cnt_Ethinicity++;focs++;
			}
		}
//	}
	
		
		//For Employment
		/*if(employment_config==1)
		{
			
		}*/
	
	//For Race value
	
	var raceValue="";
	if(race_config==1)
	{
		var elements = document.getElementsByName('raceId');
		
		for (i=0;i<elements.length;i++) 
		{
		  if(elements[i].checked) 
		  {
			  raceValue+=elements[i].value+",";
		  }
		}
		/*var elements = document.getElementsByName('raceId');
		for (i=0;i<elements.length;i++) 
		{
		  if(elements[i].checked) 
		  {
			  raceValue=elements[i].value;
		  }
		}*/
		
		if(districtIdForDSPQ==7800038){}
		else if(districtIdForDSPQ==4218990){}
		else if(districtIdForDSPQ==3904380){}
		else if(districtIdForDSPQ==1201470){}
		else if(districtIdForDSPQ==7800040){}
		else if(districtIdForDSPQ==7800047){}
		else if(districtIdForDSPQ==614730){}
		else if(districtIdForDSPQ==5304860){}
		else if(districtIdForDSPQ==804800){}
		else if(districtIdForDSPQ==1302010){}
		else{
			if(raceValue=="")
			{
				$('#errRace').append("&#149; "+resourceJSON.msgSelectRace+"<br>");
				//$('#phoneNumber').css("background-color",txtBgColor);
				cnt_Race++;focs++;
			}
		}
	}
	
	
	
	var miami = document.getElementById("isMiami").value;
	var genderValue=-1;
	if(districtIdForDSPQ==614730){}
	else if(districtIdForDSPQ==5304860){}
	else if(districtIdForDSPQ==1302010){}
	else if(gender_config==1)
	{
		var genderElements = document.getElementsByName('genderId');
		
		for (i=0;i<genderElements.length;i++) 
		{
			/*if(miami==false)
			{
				if(genderElements[i].value==1 || genderElements[i].value==2)
					if(genderElements[i].checked) 
						  genderValue=genderElements[i].value;
			}
			else 
			{*/
				if(genderElements[i].checked) 
					  genderValue=genderElements[i].value;
			/*}*/
		}
		
		if(genderValue==-1)
		{
			$('#errGender').append("&#149; "+resourceJSON.msgSelectGender+"<br>");
			//$('#phoneNumber').css("background-color",txtBgColor);
			cnt_Gender++;focs++;
		}
	}
	
	if(document.getElementById("districtIdForDSPQ").value==7800040)
	{
		if(expectedSalary_pi=="")
		{
			$('#errExpSalary').append("&#149; "+resourceJSON.msgExpectedSalary+"<br>");
			cnt_ExpSalary++;focs++;
		}
	}
	
	if(document.getElementById("districtIdForDSPQ").value==804800 && ($('#jobcategoryDsp').val()=="Licensed Teacher" || $('#jobcategoryDsp').val()=="Digital Teacher Librarian" || $('#jobcategoryDsp').val()=="Special Education Teacher")){
		wst_config=0;
		$("#sSubTrequired").hide();
	}
	
	if(document.getElementById("districtIdForDSPQ").value==4218990){
		var nonteacherFlag=$("#isnontj").val();
		if(nonteacherFlag=="" || nonteacherFlag!="true"){
			if(wst_config==1 && canServeAsSubTeacher==2)
			{
				$('#errordiv_bottomPart_wst').append("&#149; "+resourceJSON.msgSubstituteTeacher+"<br>");
				cnt_wst++;focs++;
			}
		}
	}
	else{
		if(wst_config==1 && canServeAsSubTeacher==2)
		{
			$('#errordiv_bottomPart_wst').append("&#149; "+resourceJSON.msgSubstituteTeacher+"<br>");
			cnt_wst++;focs++;
		}
	}
	
	if(phone_config==1 && trim(phoneNumber)=="")
	{
		if(phoneNumber1=="" && phoneNumber2=="" && phoneNumber3=="")
			$('#errordiv_bottomPart_phone').append("&#149; "+resourceJSON.msgPhoneNumber+"<br>");
		else if(phoneNumber1.length!=3 || phoneNumber2.length!=3 || phoneNumber3.length!=4)
			$('#errordiv_bottomPart_phone').append("&#149; "+resourceJSON.msgValidPhoneNumber+"<br>");
		
		$('#phoneNumber1').css("background-color",txtBgColor);
		$('#phoneNumber2').css("background-color",txtBgColor);
		$('#phoneNumber3').css("background-color",txtBgColor);
		cnt_ph++;focs++;
	}
	
	
	if(document.getElementById("districtIdForDSPQ").value==804800 && $('#jobcategoryDsp').val()=="Substitute Teacher"){
		exp_config=0;
	} else if(document.getElementById("districtIdForDSPQ").value==1302010 && ($('#jobcategoryDsp').val()=="Classified" || $('#jobcategoryDsp').val()=="Substitutes")){
		exp_config=0;
	}
	if(exp_config==1)
	{
		if(trim(expCertTeacherTraining)=="")
		{
			$('#errExpCTT').append("&#149; "+resourceJSON.msgteachingcertificate+"<br>");
			$('#expCertTeacherTraining').css("background-color",txtBgColor);
			cnt_ectt++;focs++;
		}
	}
	
	if(nbc_config==1)
	{
		if(nbc1.checked && trim(nationalBoardCertYear)=="")
		{
			$('#errNBCY').append("&#149; "+resourceJSON.msgnationalboard+"<br>");
			$('#nationalBoardCertYear').css("background-color",txtBgColor);
			cnt_nbcy++;focs++;
		}
	}
	
	if(affidavit_config==1)
	{
		if(affflag==false)
		{
			$('#errAffidavit').append("&#149; "+resourceJSON.msgSelectAffidavit+"<br>");
			$('#affidavit').css("background-color",txtBgColor);
			cnt_affdt++;focs++;
		}
	}
	if(districtIdForDSPQ==1201470 || districtIdForDSPQ==614730 || districtIdForDSPQ==1302010){}
	else if(districtIdForDSPQ==4218990){
		var nonteacherFlag=$("#isnontj").val();
		if(nonteacherFlag=="" || nonteacherFlag!="true"){
			
			if(tfaAffiliate!="3" && tfaAffiliate!="")
			{
				if(trim(corpsYear)=="")
				{
					$('#errordiv_bottomPart_TFA').append("&#149; "+resourceJSON.msgcorpsyear+"<br>");
					if(focs==0)
						$('#corpsYear').focus();
					
					$('#corpsYear').css("background-color",txtBgColor);
					cnt_tfa++;focs++;
				}
				
				if(trim(tfaRegion)=="")
				{
					$('#errordiv_bottomPart_TFA').append("&#149; "+resourceJSON.msgtfaregion+"<br>");
					if(focs==0)
						$('#tfaRegion').focus();
					
					$('#tfaRegion').css("background-color",txtBgColor);
					cnt_tfa++;focs++;
				}
			}
			
		}
	}
	else{		
	if(trim(tfaAffiliate)=="")
	{
		$('#errordiv_bottomPart_TFA').append("&#149; "+resourceJSON.msgTeachForAmericaAffiliate+"<br>");
		if(focs==0)
			$('#tfaAffiliate').focus();
		
		$('#tfaAffiliate').css("background-color",txtBgColor);
		cnt_tfa++;focs++;
	}
	
	if(tfaAffiliate!="3" && tfaAffiliate!="")
	{
		if(trim(corpsYear)=="")
		{
			$('#errordiv_bottomPart_TFA').append("&#149; "+resourceJSON.msgcorpsyear+"<br>");
			if(focs==0)
				$('#corpsYear').focus();
			
			$('#corpsYear').css("background-color",txtBgColor);
			cnt_tfa++;focs++;
		}
		
		if(trim(tfaRegion)=="")
		{
			$('#errordiv_bottomPart_TFA').append("&#149; "+resourceJSON.msgtfaregion+"<br>");
			if(focs==0)
				$('#tfaRegion').focus();
			
			$('#tfaRegion').css("background-color",txtBgColor);
			cnt_tfa++;focs++;
		}
	}
	}
	
	if(districtIdForDSPQ==4218990){/*		
		
		var nonteacherFlag=$("#isnontj").val();
		var isSchoolsupport=$("#isSchoolSupportPhiladelphia").val();		
		var atLeastOneIsChecked = $('.tfaOptId:checked').length > 0;
		
		if((isSchoolsupport=="" || isSchoolsupport=="0") && (nonteacherFlag=="" || nonteacherFlag=="false")){
			if(document.getElementById('StuTchrChk').checked==true && $('#ttlRecStdExp').val()==0){
				$('#errordiv_bottomPart_divstdTch').html("&#149; Please provide Student Teaching Experience.");
			}
		}
		
		if(nonteacherFlag=="" || nonteacherFlag!="true"){
			if(atLeastOneIsChecked==false){
				$('#errordiv_bottomPart_divstdTch').html("&#149; PLEASE SELECT ONE OR PROGRAM YOU'VE BEEN INVOLVED IN");
				$('#errordiv_bottomPart_tfaOptions').show();
				if(focs==0)
					$('#errordiv_bottomPart_tfaOptions').focus();
				
				$('.tfaOptId').css("background-color",txtBgColor);
				cnt_tfa++;focs++;
			}else{
				//setDistrictSpecificTFAValues();				
				setDistrictSpecificTFAValues();
			}
		}else{
			if(atLeastOneIsChecked==true){
				
				setDistrictSpecificTFAValues();
			//}
		}
	*/}
	if(document.getElementById("districtIdForDSPQ").value=="804800"){
		resume_config=0;
		$("#requiredRessume").hide();
	}
	if(ext!="")
	{
		if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt'))
		{
			$('#errordiv_bottomPart_resume').append("&#149; "+resourceJSON.msgacceptableresume+"<br>");
			if(focs==0)
				$('#resume').focus();

			$('#resume').css("background-color",txtBgColor);
			cnt_resume++;focs++;	

		}
		else if(fileSize>=10485760)
		{		
			$('#errordiv_bottomPart_resume').append("&#149; "+resourceJSON.msgfilesize+"<br>");
			if(focs==0)
				$('#resume').focus();

			$('#resume').css("background-color",txtBgColor);
			cnt_resume++;focs++;	

		}
		else
		{	
			if(cnt_resume > 0)
				$('#errordiv_bottomPart_resume').show();
			//document.getElementById("frmExpResumeUpload").submit();
		}
	}
//	else if(resume_config==1 && ext=="" && hdnResume=="")
//	{
//		$('#errordiv_bottomPart_resume').append("&#149; Please upload resume<br>");
//		cnt_resume++;
//		$('#errordiv_bottomPart_resume').show();
//	}
	
	if(address_config==1)
	{
		if(trim(addressLine1.value)=="")
		{
			$('#errAddress1').append("&#149; "+resourceJSON.msgaddresslinefirst+"<br>");
			$('#addressLine1').css("background-color",txtBgColor);
			cnt_address1++;focs++;
		}	
		if(trim(zipCode.value)=="")
		{
			$('#errZip').append("&#149; "+resourceJSON.msgenterzipcode+"<br>");
			$('#zipCode').css("background-color",txtBgColor);
			cnt_zip++;focs++;
		}
		
		//------ Start:: Country ,State, City----------
		if(countryId=="")
		{
			$('#errCountry').append("&#149; "+resourceJSON.msgPleaseaCountry+"<br>");
			$('#cnt_Country').css("background-color",txtBgColor);
			cnt_Country++;focs++;
		}
		else
		{
			if(trim(stateIdForDSPQ)=="")
			{
				$('#errState').append("&#149; "+resourceJSON.msgPleaseselectaState+"<br>");
				
				if(countryId==223)
				{
					$('#stateIdForDSPQ').css("background-color",txtBgColor);
					cnt_state++;focs++;
				}
				else
				{
					if(document.getElementById("countryCheck").value==1)
					{
						$('#stateIdForDSPQ').css("background-color",txtBgColor);
						cnt_state++;focs++;
					}
					else if(document.getElementById("countryCheck").value==0)
					{
						$('#otherState').css("background-color",txtBgColor);
						cnt_state++;focs++;
					}
				}
			}
			
			if(trim(cityIdForDSPQ)=="")
			{
				$('#errCity').append("&#149; "+resourceJSON.msgPleaseselectaCity+"<br>");
				
				if(countryId==223)
				{
					$('#cityIdForDSPQ').css("background-color",txtBgColor);
					cnt_city++;focs++;
				}
				else
				{
					if(document.getElementById("countryCheck").value==1)
					{
						$('#otherCity').css("background-color",txtBgColor);
						cnt_city++;focs++;
					}
					else if(document.getElementById("countryCheck").value==0)
					{
						$('#otherCity').css("background-color",txtBgColor);
						cnt_city++;focs++;
					}
				}
			}
			
			
			
			/*if(document.getElementById("countryCheck").value==1)
			{
				if(trim(stateIdForDSPQ)=="")
				{
					$('#errState').append("&#149; Please select a State<br>");
					$('#stateIdForDSPQ').css("background-color",txtBgColor);
					cnt_state++;focs++;
				}
				if(trim(cityIdForDSPQ)=="")
				{
					$('#errCity').append("&#149; Please select a City<br>");
					$('#cityIdForDSPQ').css("background-color",txtBgColor);
					cnt_city++;focs++;
				}
			}
			else if(document.getElementById("countryCheck").value==0)
			{
				if(trim(stateIdForDSPQ)=="")
				{
					$('#errState').append("&#149; Please select a State<br>");
					$('#otherState').css("background-color",txtBgColor);
					cnt_state++;focs++;
				}
				if(trim(cityIdForDSPQ)=="")
				{
					$('#errCity').append("&#149; Please select a City<br>");
					$('#otherCity').css("background-color",txtBgColor);
					cnt_city++;focs++;
				}
			}*/
		}
		//------ End----------
	}
	
	if(cnt_PersonalInfo >= 1 || cnt_SSN >=1)
	{
		$('#errPersonalInfoAndSSN').show();
	}
	
	if(cnt_PersonalInfo >= 1 || cnt_Veteran >=1)
	{
		$('#errPersonalInfoAndSSN').show();
	}
	
	if(cnt_FormerEmployee >= 1)
	{
		$('#errFormerEmployee').show();
	}
	
	if(cnt_GeneralKnowledge >= 1){
		$('#errGeneralKnowledge').show();
	}
	else
	{
		$('#errGeneralKnowledge').hide();
	}
	
	if(cnt_SubjectAreaExam >= 1){
		//$('#errSubjectArea').show();
	}
	
	if(cnt_Race >=1)
	{
		$('#errRace').show();
	}
	if(cnt_Gender >=1)
	{
		$('#errGender').show();
	}
	if(cnt_ExpSalary >=1)
	{
		$('#errExpSalary').show();
	}
	if(cnt_EthnicOrigin >=1)
	{
		$('#errEthnicOrigin').show();
	}
	if(cnt_Ethinicity >=1)
	{
		$('#errEthinicity').show();
	}
	
	if(cnt_tfa!=0 && tFA_config==1)		
	{
		$('#errordiv_bottomPart_TFA').show();
	}
	else if(cnt_tfa!=0 && tfaAffiliate!="3" && tfaAffiliate!="")		
	{
		$('#errordiv_bottomPart_TFA').show();
	}
	
	if(cnt_wst==1)
	{
		$('#errordiv_bottomPart_wst').show();
	}
	
	if(cnt_ph==1)
	{
		$('#errordiv_bottomPart_phone').show();
	}
	
	if(cnt_address1==1)
	{
		$('#errAddress1').show();
	}
	if(cnt_zip==1)
	{
		$('#multyErrDiv').show();
		$('#errZip').show();
	}
	if(cnt_state==1)
	{
		$('#multyErrDiv').show();
		$('#errZip').show();
		$('#errState').show();
	}
	if(cnt_city==1)
	{
		$('#multyErrDiv').show();
		$('#errZip').show();
		$('#errState').show();
		$('#errCity').show();
	}
	if(cnt_Country==1)
	{
		$('#errCountry').show();
	}
	if(cnt_ectt==1)
	{
		$('#errExpCTT').show();
	}
	if(cnt_nbcy==1)
	{
		$('#errNBCY').show();
	}
	if(cnt_affdt==1)
	{
		$('#errAffidavit').show();
	}
	if(cnt_RetireNo>0)
	{
		$('#errRetireNo').show();
	}
	
	if(((generalKnowledge_config==1 || subjectAreaExam_config || additionalDocuments_config) && cnt_AdditionalDocuments==0 && cnt_GeneralKnowledge==0 && cnt_SubjectAreaExam==0)){
		try{
			document.getElementById("multifileuploadform").submit();
		}catch(err){}
	}
	//if(cnt_tfa==0 || cnt_wst==0 || cnt_resume==0 || cnt_ph==0 || cnt_address1==0 || cnt_zip==0 || cnt_state==0 || cnt_city==0 || cnt_ectt==0 ||	cnt_nbcy==0 || cnt_affdt==0 || cnt_PersonalInfo==0 || cnt_SSN==0 || cnt_FormerEmployee==0 || cnt_Race==0 || cnt_GeneralKnowledge==0 || cnt_SubjectAreaExam==0|| cnt_AdditionalDocuments==0 || cnt_Veteran==0 || cnt_EthnicOrigin==0 || cnt_Ethinicity==0 ||cnt_Country==0 || cnt_Gender || cnt_RetireNo==0 || cnt_ExpSalary==0)		
	if(cnt_dspq==0 && cnt_ectt==0 && cnt_RetireNo==0 && cnt_tfa==0 && cnt_ph==0 && cnt_address1==0 && cnt_zip==0 && cnt_state==0 && cnt_city==0 &&	cnt_nbcy==0 && cnt_PersonalInfo==0 && cnt_SSN==0 && cnt_FormerEmployee==0 && cnt_Race==0 && cnt_Veteran==0 && cnt_EthnicOrigin==0 && cnt_Ethinicity==0 && cnt_Country==0 && cnt_Gender==0)
	//if(cnt_PersonalInfo<1 && cnt_address1<1 && cnt_city<1 && cnt_FormerEmployee<1 && cnt_ph<1 && cnt_zip<1 && cnt_state<1 && cnt_SSN<1 && cnt_Veteran<1 && cnt_EthnicOrigin<1 && cnt_Ethinicity<1 && cnt_Country<1 && cnt_Gender <1)
	{
		if(tfaAffiliate==null || tfaAffiliate=='')
			tfaAffiliate =0;
		if(corpsYear==null || corpsYear=='')
			corpsYear =0;
		if(tfaRegion==null || tfaRegion=='')
			tfaRegion =0;
		
		
		//------------------------Testing for flag----------------------
		var isAffilated=0;
		var candidateType="";
		
		try{
			if(document.getElementById("isAffilated")!=null && document.getElementById("isAffilated").checked)
				isAffilated=1;
			else
				isAffilated=document.getElementById("txtCandidateType").value;
		}catch(err){}
		
		if(isAffilated==1)
			candidateType="I";
		else
			candidateType="E";
		
		//alert(middleName_pi);
		//--------------------------------------------------------------
		//var jobId=document.getElementById("jobId").value;
		var jobId= "";
		var portfolioStatus = document.getElementById("portfolioStatus").value;
		$('#loadingDiv').show();
		PFExperiences.insertOrUpdateTeacherPersonaLinForRem(tfaAffiliate,corpsYear,tfaRegion,canServeAsSubTeacher,phoneNumber,addressLine1.value,addressLine2.value,zipCode.value,stateIdForDSPQ,cityIdForDSPQ,expCertTeacherTraining,nationalBoardCertYear,affflag,salutation_pi,firstName_pi,middleName_pi,lastName_pi,ssn_pi,dob,employeeType,formerEmployeeNo,currentEmployeeNo,isCurrentFullTimeTeacher,raceValue,rtDate,wdDate,veteranValue,districtIdForDSPQ,ethnicOriginValue,ethinicityValue,countryId,genderValue,candidateType,portfolioStatus,jobId,isNonTeacher,retireNo,distForRetire,stMForretire,expectedSalary_pi,anotherName_pi,noLongerEmployed,
		{ 
			async: false,
			errorHandler:handleError,
			callback: function(data)
			{
			if(fileName!="")
			{
				document.getElementById("frmExpResumeUpload").submit();
			}
				showPortfolioReminder();
				//$('#updateAndSaveDSPQ').modal('show');
				window.location="portfoliosubmit.do";
				$('#loadingDiv').hide();
			
			}
		});
	}
	else
	{
		//$('#topErrorMessageDSPQ').modal('show');
		return false;
	}
		
}
function showcertiDiv()
{
	$('#draggableDivMaster').modal('show');
}
function showAffidavitDiv()
{
	$('#affidavitDataDiv').modal('show');
}


function saveOrUpdateInvolvement()
{

	var organization = document.getElementById("organizationInv");
	var orgTypeId = document.getElementById("orgTypeId");
	var rangeId = document.getElementById("rangeId");
	var leadNoOfPeople =  document.getElementById("leadNoOfPeople");

	var rdo1 =  document.getElementById("rdo1");

	var cnt=0;
	var focs=0;	

	$('#errordivInvolvement').empty();
	setDefColortoErrorMsgToInvolvement();


	if(trim(organization.value)=="")
	{
		$('#errordivInvolvement').append("&#149; "+resourceJSON.PlzEtrOrg+"<br>");
		if(focs==0)
			$('#organizationInv').focus();

		$('#organizationInv').css("background-color",txtBgColor);
		cnt++;focs++;
	}

	if(trim(orgTypeId.value)=="")
	{
		$('#errordivInvolvement').append("&#149; "+resourceJSON.msgorgtype+"<br>");
		if(focs==0)
			$('#orgTypeId').focus();

		$('#orgTypeId').css("background-color",txtBgColor);
		cnt++;focs++;
	}

	if(trim(rangeId.value)=="")
	{
		$('#errordivInvolvement').append("&#149; "+resourceJSON.msgnumberofpeople+"<br>");
		if(focs==0)
			$('#rangeId').focus();

		$('#rangeId').css("background-color",txtBgColor);
		cnt++;focs++;
	}

	if(rdo1.checked && trim(leadNoOfPeople.value)=="")
	{
		$('#errordivInvolvement').append("&#149; "+resourceJSON.msgmanypeoplelead+"<br>");
		if(focs==0)
			$('#leadNoOfPeople').focus();

		$('#leadNoOfPeople').css("background-color",txtBgColor);
		cnt++;focs++;
	}


	if(cnt!=0)		
	{
		$('#errordivInvolvement').show();
		return false;
	}
	else
	{
		var orgObj = {orgTypeId:dwr.util.getValue("orgTypeId")};
		var peopleRangeObj = {rangeId:dwr.util.getValue("rangeId")};		
		var involvement = {involvementId:null, orgTypeMaster:orgObj, peopleRangeMaster:peopleRangeObj, leadNoOfPeople:null};

		dwr.engine.beginBatch();	
		dwr.util.getValues(involvement);	
		involvement.organization= organization.value;
		PFExperiences.saveOrUpdateInvolvement(involvement,
				{ 
			async: false,
			errorHandler:handleError,
			callback: function(data)
			{

			hideInvolvement();
			getInvolvementGrid();

			return false;
			}
				});
		dwr.engine.endBatch();
	}
	return true;


}

function saveOrUpdateHonors()
{

	var honor = document.getElementById("honor");
	var honorYear = document.getElementById("honorYear");


	var cnt=0;
	var focs=0;	

	$('#errordivHonor').empty();
	setDefColortoErrorMsgToHoner();


	if(trim(honor.value)=="")
	{
		$('#errordivHonor').append("&#149; "+resourceJSON.msgaward+"<br>");
		if(focs==0)
			$('#honor').focus();

		$('#honor').css("background-color",txtBgColor);
		cnt++;focs++;
	}

	if(trim(honorYear.value)=="0")
	{
		$('#errordivHonor').append("&#149; "+resourceJSON.msgawardyear+"<br>");
		if(focs==0)
			$('#honorYear').focus();

		$('#honorYear').css("background-color",txtBgColor);
		cnt++;focs++;
	}


	if(cnt!=0)		
	{
		$('#errordivHonor').show();
		return false;
	}
	else
	{
		var honer = {honorId:null, honor:null, honorYear:null};
		dwr.engine.beginBatch();	
		dwr.util.getValues(honer);
		PFExperiences.saveOrUpdateHonors(honer,
				{ 
			async: false,
			errorHandler:handleError,
			callback: function(data)
			{

			hideHonor();
			getHonorsGrid();

			return false;
			}
				});
		dwr.engine.endBatch();
	}
	return true;

}


function insertOrUpdateEmployment(sbtsource)
{
	if(sbtsource=="0")
	{
		resetSBTNSource();
	}
	
	var threadCount_emp=document.getElementById("threadCount_emp").value;
	if(threadCount_emp=="0")
	{
		
		document.getElementById("threadCount_emp").value=1;
		
		updateThreadCount("emp");
		
		var position = document.getElementById("empPosition");
		
		var role = document.getElementById("role");
		var empOrg = document.getElementById("empOrg");
		var fieldId = document.getElementById("fieldId2");
		var currentlyWorking = document.getElementById("currentlyWorking");
		var roleStartMonth = document.getElementById("roleStartMonth");
		var roleStartYear = document.getElementById("roleStartYear");	
		var roleEndMonth = document.getElementById("roleEndMonth");
		var roleEndYear = document.getElementById("roleEndYear");
		var empRoleTypeId=document.getElementsByName("empRoleTypeId");
		var amount=document.getElementById("amount");
		var reasonForLea = $('#reasonForLeadiv').find(".jqte_editor").text().trim();
		
		var cityEmp = document.getElementById("cityEmp");
		var stateOfOrg = document.getElementById("stateOfOrg");
		
		var cnt=0;
		var focs=0;	
		var amountFlag=false;
		if(amount!=""){
			amountFlag=isNaN(amount.value);
		}
		
		if(document.getElementById("districtIdForDSPQ").value==4218990 || document.getElementById("districtIdForDSPQ").value==3904380 || document.getElementById("districtIdForDSPQ").value==1201470 || document.getElementById("districtIdForDSPQ").value==804800){
			var charCount=trim($('#primaryRespdiv').find(".jqte_editor").text());
			var countPrimaryResp = charCount.trim().length;
			charCount=trim($('#mostSignContdiv').find(".jqte_editor").text());
			var countmostSignCont = charCount.trim().length;
		}
		
		$('#errordivEmployment').empty();
		setDefColortoErrorMsgToEmployment();
		if(document.getElementById("districtIdForDSPQ").value!=804800){	
			if(amount.value=="" && $("#amount").is(':visible'))
			{
				$('#errordivEmployment').append("&#149; "+resourceJSON.msgEneterAmount+"<br>");
				if(focs==0)
					$('#amount').focus();
	
				$('#amount').css("background-color",txtBgColor);
				cnt++;focs++;
			}
		}
		if(amountFlag)
		{
			$('#errordivEmployment').append("&#149; "+resourceJSON.msgnumericamount+"<br>");
			if(focs==0)
				$('#amount').focus();

			$('#amount').css("background-color",txtBgColor);
			cnt++;focs++;
		}
		
		
	if(trim(position.value)=="0")
		{
			
				$('#errordivEmployment').append("&#149; "+resourceJSON.msgposition+"<br>");
			
			if(focs==0)
				$('#empPosition').focus();

			$('#empPosition').css("background-color",txtBgColor);
			cnt++;focs++;
		}
		
		if(trim(role.value)=="")
		{
			
				$('#errordivEmployment').append("&#149; "+resourceJSON.msgTitleDesigRole+"<br>");
			
			if(focs==0)
				$('#role').focus();

			$('#role').css("background-color",txtBgColor);
			cnt++;focs++;
		}
		
		
		if(trim(empOrg.value)=="")
		{
				$('#errordivEmployment').append("&#149; "+resourceJSON.msgorganization+"<br>");
			
			if(focs==0)
				$('#empOrg').focus();

			$('#empOrg').css("background-color",txtBgColor);
			cnt++;focs++;
		}
		//shadab
		
		if(trim(cityEmp.value)=="")
		{
			
				$('#errordivEmployment').append("&#149; "+resourceJSON.msgentercity+"<br>");
			
			if(focs==0)
				$('#cityEmp').focus();

			$('#cityEmp').css("background-color",txtBgColor);
			cnt++;focs++;
		}
		if(trim(stateOfOrg.value)=="")
		{
			
				$('#errordivEmployment').append("&#149; "+resourceJSON.msgStateOfOrganisation+"<br>");
			
			if(focs==0)
				$('#stateOfOrg').focus();

			$('#stateOfOrg').css("background-color",txtBgColor);
			cnt++;focs++;
		}
		//shadab end
		if(trim(fieldId.value)=="")
		{
			$('#errordivEmployment').append("&#149; "+resourceJSON.msgPleaseselectField+"<br>");
			if(focs==0)
				$('#fieldId2').focus();

			$('#fieldId2').css("background-color",txtBgColor);
			cnt++;focs++;
		}
		if($("#empDateDiv").is(':visible')){
		if(trim(roleStartMonth.value)=="0")
		{
			$('#errordivEmployment').append("&#149; "+resourceJSON.msgPleaseselectFromMonth+"<br>");
			if(focs==0)
				$('#roleStartMonth').focus();

			$('#roleStartMonth').css("background-color",txtBgColor);
			cnt++;focs++;
		}
		if(trim(roleStartYear.value)=="0")
		{
			$('#errordivEmployment').append("&#149; "+resourceJSON.msgPleaseselectFromYear+"<br>");
			if(focs==0)
				$('#roleStartYear').focus();

			$('#roleStartYear').css("background-color",txtBgColor);
			cnt++;focs++;
		}
		}
		if(currentlyWorking.checked==false)
		{
			if($("#empDateDiv").is(':visible')){
			if(trim(roleEndMonth.value)=="0")
			{
				$('#errordivEmployment').append("&#149; "+resourceJSON.msgPleaseselectToMonth+"<br>");
				if(focs==0)
					$('#roleEndMonth').focus();

				$('#roleEndMonth').css("background-color",txtBgColor);
				cnt++;focs++;
			}
			if(trim(roleEndYear.value)=="0")
			{
				$('#errordivEmployment').append("&#149; "+resourceJSON.msgPleaseselectToYear+"<br>");
				if(focs==0)
					$('#roleEndYear').focus();

				$('#roleEndYear').css("background-color",txtBgColor);
				cnt++;focs++;
			}

			if(trim(roleStartMonth.value)!="0" && trim(roleStartYear.value)!="0" && trim(roleEndMonth.value)!="0" && trim(roleEndYear.value)!="0")
			{
				if(trim(roleStartYear.value)>trim(roleEndYear.value))
				{
					$('#errordivEmployment').append("&#149; "+resourceJSON.msgFromgreaterthan+"<br>");
					if(focs==0)
						$('#roleStartMonth').focus();

					$('#roleStartMonth').css("background-color",txtBgColor);
					$('#roleStartYear').css("background-color",txtBgColor);
					$('#roleEndMonth').css("background-color",txtBgColor);
					$('#roleEndYear').css("background-color",txtBgColor);
					cnt++;focs++;
				}
				else if(parseInt(trim(roleStartMonth.value))>parseInt(trim(roleEndMonth.value)) && (trim(roleStartYear.value)==trim(roleEndYear.value)))
				{
					$('#errordivEmployment').append("&#149; "+resourceJSON.msgFromgreaterthan+"<br>");
					if(focs==0)
						$('#roleStartMonth').focus();

					$('#roleStartMonth').css("background-color",txtBgColor);
					$('#roleStartYear').css("background-color",txtBgColor);
					$('#roleEndMonth').css("background-color",txtBgColor);
					$('#roleEndYear').css("background-color",txtBgColor);
					cnt++;focs++;
				}
			}
		}
		}
		var chkRole=true;
		for(i=0;i<empRoleTypeId.length;i++)
		{
			if(empRoleTypeId[i].checked==true)
			{
				chkRole=false;
				break;
			}
		}
		if(document.getElementById("districtIdForDSPQ").value==4218990 || document.getElementById("districtIdForDSPQ").value==3904380){
			chkRole=false;
		}
		if(chkRole)
		{
			$('#errordivEmployment').append("&#149; "+resourceJSON.msgtypeRole+"<br>");
			if(focs==0)
				$('#empRoleTypeId').focus();		
			cnt++;focs++;
		}
		
		
		if(document.getElementById("districtIdForDSPQ").value==4218990){
			var nonteacherFlag=$("#isnontj").val();
			
			if($('#isSchoolSupportPhiladelphia').val()!=""){}
			else if(nonteacherFlag=="" || nonteacherFlag!="true"){
				if(countPrimaryResp==0)
				{
					$('#errordivEmployment').append("&#149; "+resourceJSON.msgprimaryresponsibilities+"<br>");
					if(focs==0)
						$('#primaryRespdiv').find(".jqte_editor").focus();
					$('#primaryRespdiv').find(".jqte_editor").css("background-color", "#F5E7E1");
					cnt++;focs++;
				}
				else if(countPrimaryResp>5000)
				{
					$('#errordivEmployment').append("&#149; "+resourceJSON.MsgMaxLengthOfField+"<br>");
					if(focs==0)
						$('#primaryRespdiv').find(".jqte_editor").focus();
					$('#primaryRespdiv').find(".jqte_editor").css("background-color", "#F5E7E1");
					cnt++;focs++;
				}
			}			
			else{
				if(countPrimaryResp>5000)
				{
					$('#errordivEmployment').append("&#149; "+resourceJSON.MsgMaxLengthOfField+"<br>");
					if(focs==0)
						$('#primaryRespdiv').find(".jqte_editor").focus();
					$('#primaryRespdiv').find(".jqte_editor").css("background-color", "#F5E7E1");
					cnt++;focs++;
				}
			}

			if(countmostSignCont>5000)
			{
				$('#errordivEmployment').append("&#149; "+resourceJSON.MsgForMostSignificantContributions+"<br>");
				if(focs==0)
					$('#mostSignContdiv').find(".jqte_editor").focus();
				$('#mostSignContdiv').find(".jqte_editor").css("background-color", "#F5E7E1");
				cnt++;focs++;
			}
		}
		
		if(document.getElementById("districtIdForDSPQ").value==804800){
			if(countPrimaryResp==0)
			{
				$('#errordivEmployment').append("&#149; "+resourceJSON.msgprimaryresponsibilities+"<br>");
				if(focs==0)
					$('#primaryRespdiv').find(".jqte_editor").focus();
				$('#primaryRespdiv').find(".jqte_editor").css("background-color", "#F5E7E1");
				cnt++;focs++;
			}else if(countPrimaryResp>5000)
			{
				$('#errordivEmployment').append("&#149; "+resourceJSON.MsgMaxLengthOfField+"<br>");
				if(focs==0)
					$('#primaryRespdiv').find(".jqte_editor").focus();
				$('#primaryRespdiv').find(".jqte_editor").css("background-color", "#F5E7E1");
				cnt++;focs++;
			}
			
			if(countmostSignCont==0 && $('#jobcategoryDsp').val()!="Hourly Staff")
			{
				$('#errordivEmployment').append("&#149; "+resourceJSON.msgmostsignificantcontributions+"<br>");
				if(focs==0)
					$('#mostSignContdiv').find(".jqte_editor").focus();
				$('#mostSignContdiv').find(".jqte_editor").css("background-color", "#F5E7E1");
				cnt++;focs++;
			}else if(countmostSignCont>5000)
			{
				$('#errordivEmployment').append("&#149; "+resourceJSON.MsgForMostSignificantContributions+"<br>");
				if(focs==0)
					$('#mostSignContdiv').find(".jqte_editor").focus();
				$('#mostSignContdiv').find(".jqte_editor").css("background-color", "#F5E7E1");
				cnt++;focs++;
			}
		}
		if(document.getElementById("districtIdForDSPQ").value==1201470){
			/*if(countPrimaryResp==0)
			{
				$('#errordivEmployment').append("&#149; Please provide primary responsibilities in this role<br>");
				if(focs==0)
					$('#primaryRespdiv').find(".jqte_editor").focus();
				$('#primaryRespdiv').find(".jqte_editor").css("background-color", "#F5E7E1");
				cnt++;focs++;
			}
			else */if(countPrimaryResp>5000)
			{
				$('#errordivEmployment').append("&#149; "+resourceJSON.MsgMaxLengthOfField+"<br>");
				if(focs==0)
					$('#primaryRespdiv').find(".jqte_editor").focus();
				$('#primaryRespdiv').find(".jqte_editor").css("background-color", "#F5E7E1");
				cnt++;focs++;
			}
			
			/*if(countPrimaryResp==0)
			{
				$('#errordivEmployment').append("&#149; Please provide most significant contributions in this role<br>");
				if(focs==0)
					$('#mostSignContdiv').find(".jqte_editor").focus();
				$('#mostSignContdiv').find(".jqte_editor").css("background-color", "#F5E7E1");
				cnt++;focs++;
			}else */if(countmostSignCont>5000)
			{
				$('#errordivEmployment').append("&#149; "+resourceJSON.MsgForMostSignificantContributions+"<br>");
				if(focs==0)
					$('#mostSignContdiv').find(".jqte_editor").focus();
				$('#mostSignContdiv').find(".jqte_editor").css("background-color", "#F5E7E1");
				cnt++;focs++;
			}
		}
		if($(".reasonForLeavdiv").is(':visible') && roleEndYear.value!=0 && roleEndMonth.value!=0){
			if(reasonForLea.trim().length==0)
			{
				$('#errordivEmployment').append("&#149; "+resourceJSON.msgReasonforleaving+"<br>");
				if(focs==0)
					$('#reasonForLeadiv').find(".jqte_editor").focus();
					$('#reasonForLeadiv').find(".jqte_editor").css("background-color", "#F5E7E1");
				cnt++;focs++;
			}
		}
		
		if(reasonForLea.trim().length>5000)
		{
			$('#errordivEmployment').append("&#149; "+resourceJSON.MsgMaxLengthOffieldReasonForLeaving+"<br>");
			if(focs==0)
				$('#reasonForLeadiv').find(".jqte_editor").focus();
			$('#reasonForLeadiv').find(".jqte_editor").css("background-color", "#F5E7E1");
			cnt++;focs++;
		}
		
		
		if(cnt!=0)		
		{
			updateReturnThreadCount("emp");
			$('#errordivEmployment').show();
			return false;
		}
		else
		{
			var roleType;
			for(i=0;i<empRoleTypeId.length;i++)
			{
				if(empRoleTypeId[i].checked==true)
				{
					roleType=empRoleTypeId[i].value;
					break;
				}
			}
			var roleTypeObj=null;
			
			if(roleType==1 || roleType==2 || roleType==3){
				roleTypeObj = {empRoleTypeId:roleType};
			}else{
				roleTypeObj=null;
			}
			
			var fieldObj = {fieldId:dwr.util.getValue("fieldId2")};
			
			var employment = {position:null,roleId:null,fieldMaster:fieldObj,empRoleTypeMaster:roleTypeObj,role:null, roleStartMonth:null, roleStartYear:null, roleEndMonth:null, roleEndYear:null,amount:null,primaryResp:null,mostSignCont:null,reasonForLeaving:null};
			dwr.engine.beginBatch();
			
			dwr.util.getValues(employment);
			employment.currentlyWorking=document.getElementById("currentlyWorking").checked;
			employment.reasonForLeaving=reasonForLea;
			employment.position=position.value;
			employment.organization=empOrg.value;
			
			employment.city=cityEmp.value;
			employment.state=stateOfOrg.value;
			//employeerName.value
			PFExperiences.saveOrUpdateEmployment(employment,{ 
				async: false,
				errorHandler:handleError,
				callback: function(data)
				{
					hideEmploymentForm();
					getPFEmploymentDataGrid();
					if(sbtsource==1)
					{
						saveAllDSPQ();
						updateReturnThreadCount("emp");
//						validatePortfolioErrorMessageAndGridData('level2');
					}
				//$("#hrefCertifications").addClass("stepactive");
				//hideForm();
				}
			});
			dwr.engine.endBatch();
			return true;
		}
	}
}
function hideToExp()
{
	var chk = document.getElementById("currentlyWorking").checked;
	if(chk)
	{
		document.getElementById("divToMonth").style.display="none";
		document.getElementById("divToYear").style.display="none";
	}
	else
	{
		document.getElementById("divToMonth").style.display="block";
		document.getElementById("divToYear").style.display="block";
	}
}

function insertOrUpdate_AdditionalDocuments(sbtsource)
{
	
	if(sbtsource=="0")
	{
		resetSBTNSource();
	}
	
	var threadCount_adddoc=document.getElementById("threadCount_adddoc").value;
	if(threadCount_adddoc=="0")
	{
		document.getElementById("threadCount_adddoc").value=1;
		
		updateThreadCount("adddoc");
		
		var additionDocumentId=document.getElementById("additionDocumentId").value;
		$('#errAdditionalDocuments').empty();
		var cnt=0;
		var documentName = document.getElementById("documentName").value;
		if(trim(documentName)==""){
			$('#errAdditionalDocuments').append("&#149; "+resourceJSON.msgdocumentname+"</BR>");
			$('#documentName').css("background-color",txtBgColor);
			cnt++;
		}
		var uploadedDocument = document.getElementById("uploadedDocument").value;
		var uploadedDocumentHidden = document.getElementById("uploadedDocumentHidden").value;
		if(trim(uploadedDocument)=="" && trim(uploadedDocumentHidden)==""){
			$('#errAdditionalDocuments').append("&#149; Please upload Document</BR>");
			$('#uploadedDocument').css("background-color",txtBgColor);
			cnt++;
		}else{
			if(trim(uploadedDocument)!=""){
				var ext = uploadedDocument.substr(uploadedDocument.lastIndexOf('.') + 1).toLowerCase();	
				
				var fileSize = 0;
				if ($.browser.msie==true)
			 	{	
				    fileSize = 0;	   
				}
				else
				{
					if(document.getElementById("uploadedDocument").files[0]!=undefined)
					{
						fileSize = document.getElementById("uploadedDocument").files[0].size;
					}
				}
				if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt'))
				{
					$('#errAdditionalDocuments').append("&#149; "+resourceJSON.msgacceptabledocformate+"</BR>");
					$('#uploadedDocument').css("background-color",txtBgColor);
					cnt++;
				}else if(fileSize>=10485760){
					$('#errAdditionalDocuments').append("&#149; "+resourceJSON.msgDocSize10mb+"<br>");
					$('#uploadedDocument').css("background-color",txtBgColor);
					cnt++;
				}
			}
		}
		if(cnt!=0)		
		{
			updateReturnThreadCount("adddoc");
			$('#errAdditionalDocuments').show();
			return false;
		}else if(cnt==0){
			if(trim(uploadedDocument)!=""){
				try{
					document.getElementById("additionalDocumentsForm").submit();
				}catch(err){}
			}else
			{    
				PFCertifications.saveOrUpdateAdditionalDocuments(additionDocumentId,documentName,null,uploadedDocumentHidden,
				{ 
				async: false,
				errorHandler:handleError,
				callback: function(data)
					{	
						showGridAdditionalDocuments()		
						resetAdditionalDocumentsForm();
						if(sbtsource=="1")
						{
							//saveAllDSPQ();
							updateReturnThreadCount("adddoc");
							//validatePortfolioErrorMessageAndGridData('level2');
						}
						
					}
				});
			}
		}
		
	}

}

function insertOrUpdatevideoLinks()
{
	var elerefAutoId = document.getElementById("videolinkAutoId");
	var videourl = trim(document.getElementById("videourl").value);
	var video = trim(document.getElementById("videofile").value);	
	var videoSize = document.getElementById("videofile");
	var oldvideo = document.getElementById("video").innerHTML;
	var cnt=0;
	var focs=0;	
	
	$('#errordivvideoLinks').empty();
	setDefColortoErrorMsgToVideoLinksForm();
	
	if(videourl=="" && video =="" && oldvideo =="")
	{
		$('#errordivvideoLinks').append("&#149; "+resourceJSON.msgvideolink+"<br>");
		if(videourl==0)
			$('#videourl').focus();
		
		$('#videourl').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	if(video !="")
	{
		var ext = video.substr(video.lastIndexOf('.') + 1).toLowerCase();		
		if(!(ext=="mp4"||ext=="MP4"||ext=="ogv"||ext=="ogg"||ext=="webm"))
		{        	
		 $('#errordivvideoLinks').append("&#149; "+resourceJSON.msgacceptablevideo+"");
		 if(video==0)
			$('#videourl').focus();
		
		$('#videourl').css("background-color",txtBgColor);
		cnt++;focs++;
		}
		else if(videoSize.files[0].size>10485760)
		{        	
		 $('#errordivvideoLinks').append("&#149; "+resourceJSON.msgvideosize+"<br>");
		 if(video==0)
			$('#videourl').focus();
		
		$('#videourl').css("background-color",txtBgColor);
		cnt++;focs++;
		}
		else
		{
			video=video.replace(/^.*[\\\/]/, '');	
		}
	}
	else
	{
		video= document.getElementById('video').innerHTML;
	}
	if(cnt!=0 )		
	{
		$('#errordivvideoLinks').show();
		return false;
	}
	else
	{	
		$('#loadingDiv').show();		
		if(videoSize.value !="")
		{			
			document.getElementById("frmvideoLinks").submit();
		}	
		else
		{
		var teacherVideoLink = {videolinkAutoId:null,videourl:null, video:null, createdDate:null};
		dwr.engine.beginBatch();
		dwr.util.getValues(teacherVideoLink);
		teacherVideoLink.videourl=videourl;		
		teacherVideoLink.video=	oldvideo;	
		PFCertifications.saveOrUpdateVideoLinks(teacherVideoLink,{ 
			async: false,
			errorHandler:handleError,
			callback: function(data)
			{			
				$('#loadingDiv').hide();					
				hideVideoLinksForm();
				getVideoLinksGrid();
			}
		});
		dwr.engine.endBatch();
		return true;
	}   
  }
}

function insertOrUpdateElectronicReferences(sbtsource)
{ 
	
	if(sbtsource=="0")
	{
		resetSBTNSource();
	}
	
	var threadCount_ref=document.getElementById("threadCount_ref").value;
	{
		document.getElementById("threadCount_ref").value=1;
		updateThreadCount("ref");
		//$('#testcall').append("&#149; insertOrUpdateElectronicReferences<br>");
		
		var elerefAutoId	=	document.getElementById("elerefAutoId");
		var salutation		=	document.getElementById("salutation");
		
		var firstName		=	trim(document.getElementById("firstName").value);
		var lastName		= 	trim(document.getElementById("lastName").value);;
		var designation		= 	trim(document.getElementById("designation").value);		
		var organization	=	trim(document.getElementById("organization").value);
		var email			=	trim(document.getElementById("email").value);
		
		var contactnumber	=	trim(document.getElementById("contactnumber").value);
		var rdcontacted0	=   document.getElementById("rdcontacted0");
		var rdcontacted1	=   document.getElementById("rdcontacted1");
		var pathOfReferencesFile = document.getElementById("pathOfReferenceFile");
		//var referenceDetails 	= trim($("#referenceDetailText").find(".jqte_editor").html());alert("cc");
		var referenceDetails 	=document.getElementById("referenceDetailText").value;
		var longHaveYouKnow	=   trim(document.getElementById("longHaveYouKnow").value);
		
		var cnt=0;
		var focs=0;	
		
		var fileName = pathOfReferencesFile.value;
		var ext = fileName.substr(fileName.lastIndexOf('.') + 1).toLowerCase();
		var fileSize=0;		
		if ($.browser.msie==true){	
		    fileSize = 0;	   
		}else{		
			if(pathOfReferencesFile.files[0]!=undefined)
			fileSize = pathOfReferencesFile.files[0].size;
		}
		
		$('#errordivElectronicReferences').empty();
		setDefColortoErrorMsgToElectronicReferences();

		var districtIdForDSPQ="";	
		if ($('#districtIdForDSPQ').length > 0) {
			districtIdForDSPQ=document.getElementById("districtIdForDSPQ").value;
		}
		
		if(firstName=="")
		{
			$('#errordivElectronicReferences').append("&#149; "+resourceJSON.PlzEtrFirstName+"<br>");
			if(focs==0)
				$('#firstName').focus();

			$('#firstName').css("background-color",txtBgColor);
			cnt++;focs++;
		}
		if(lastName=="")
		{
			$('#errordivElectronicReferences').append("&#149; "+resourceJSON.PlzEtrLastName+"<br>");
			if(focs==0)
				$('#lastName').focus();

			$('#lastName').css("background-color",txtBgColor);
			cnt++;focs++;
		}
		
		if(designation=="" && (districtIdForDSPQ==4218990 || districtIdForDSPQ==3904380 || districtIdForDSPQ==1201470 || districtIdForDSPQ==5510470))
		{
			$('#errordivElectronicReferences').append("&#149; "+resourceJSON.msgTitle+"<br>");
			if(focs==0)
				$('#designation').focus();

			$('#designation').css("background-color",txtBgColor);
			cnt++;focs++;
		}
		
		if(organization=="" && (districtIdForDSPQ==7800038 || districtIdForDSPQ==4218990 || districtIdForDSPQ==3904380  || districtIdForDSPQ==1201470 || districtIdForDSPQ==5510470))
		{
			$('#errordivElectronicReferences').append("&#149; "+resourceJSON.PlzEtrOrg+"<br>");
			if(focs==0)
				$('#organization').focus();

			$('#organization').css("background-color",txtBgColor);
			cnt++;focs++;
		}		
		
		if(email=="")
		{
			$('#errordivElectronicReferences').append("&#149; "+resourceJSON.PlzEtrEmail+"<br>");
			if(focs==0)
				$('#email').focus();
			
			$('#email').css("background-color",txtBgColor);
			cnt++;focs++;
		}
		else if(!isEmailAddress(email))
		{		
			$('#errordivElectronicReferences').append("&#149; "+resourceJSON.PlzEtrVldEmail+"<br>");
			if(focs==0)
				$('#email').focus();
			
			$('#email').css("background-color",txtBgColor);
			cnt++;focs++;
		}if(contactnumber=="")
		{
			$('#errordivElectronicReferences').append("&#149; "+resourceJSON.PlzEtrCtctNum+"<br>");
			if(focs==0)
				$('#contactnumber').focus();
			
			$('#contactnumber').css("background-color",txtBgColor);
			cnt++;focs++;
		}
		
		if(longHaveYouKnow=="" && districtIdForDSPQ==4218990)
		{
			$('#errordivElectronicReferences').append("&#149; "+resourceJSON.msgYouKnowThisPerson+"?<br>");
			if(focs==0)
				$('#longHaveYouKnow').focus();

			$('#longHaveYouKnow').css("background-color",txtBgColor);
			cnt++;focs++;
		}
		
		var rdcontacted_value;
		
		if (rdcontacted0.checked) {
			rdcontacted_value = false;
		}
		else if (rdcontacted1.checked) {
			rdcontacted_value = true;
		}
		
		if($("#recommLetter").is(':visible')){
			if(fileName==""){
				$('#errordivElectronicReferences').append("&#149; "+resourceJSON.msgUploadRecommendation+"</BR>");
				if(focs==0)
					$('#pathOfReferenceFile').focus();
				
				$('#pathOfReferenceFile').css("background-color",txtBgColor);
				cnt++;focs++;	
			}
		}
		
		if(ext!=""){
			if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt'))
			{
				$('#errordivElectronicReferences').append("&#149; "+resourceJSON.msgaacptfileformate+"<br>");
					if(focs==0)
						$('#pathOfReferenceFile').focus();
					
					$('#pathOfReferenceFile').css("background-color",txtBgColor);
					cnt++;focs++;	
					return false;
			}	
			else if(fileSize>=10485760)
			{
				$('#errordivElectronicReferences').append("&#149; "+resourceJSON.msgfilesizelessthan+"<br>");
					if(focs==0)
						$('#pathOfReferenceFile').focus();
					
					$('#pathOfReferenceFile').css("background-color",txtBgColor);
					cnt++;focs++;	
					return false;
			}
		}
		
		/*else if(isEmailAddress(trim(email.value)))
		{
			PFCertifications.checkEmailForEleRef(email.value,
			{ 
				async: true,
				errorHandler:handleError,
				callback:function(data)
				{
					if(data==true)
					{
						$('#errordivElectronicReferences').append("&#149; A Electronic References has already registered with the email.<br>");
						if(focs==0)
							$('#email').focus();
						$('#email').css("background-color",txtBgColor);
							cnt++;focs++;
					}
				}
			});
		}*/
		if(cnt!=0)		
		{
			updateReturnThreadCount("ref");
			$('#errordivElectronicReferences').show();
			return false;
		}
		else
		{
			var teacherElectronicReferences = {elerefAutoId:null,salutation:null, firstName:null, lastName:null, designation:null, organization:null,email:null,contactnumber:null,rdcontacted:null,referenceDetails:null,longHaveYouKnow:null};
			dwr.engine.beginBatch();
			
			
			dwr.util.getValues(teacherElectronicReferences);
			teacherElectronicReferences.rdcontacted=rdcontacted_value;
			teacherElectronicReferences.salutation=salutation.value;
			teacherElectronicReferences.firstName=firstName;
			teacherElectronicReferences.lastName=lastName;;
			teacherElectronicReferences.designation=designation;
			teacherElectronicReferences.organization=organization;
			teacherElectronicReferences.email=email;
			teacherElectronicReferences.referenceDetails=referenceDetails;
			teacherElectronicReferences.longHaveYouKnow=longHaveYouKnow;
			//referenceDetails
			
			var refFile = document.getElementById("pathOfReference").value;
			if(refFile!=""){
				teacherElectronicReferences.pathOfReference=refFile;
			}
			if(fileName!="")
			{	
				//$('#loadingDiv').show();
				//document.getElementById("frmElectronicReferences").submit();
				PFCertifications.findDuplicateEleReferences(teacherElectronicReferences,{ 
					async: false,
					errorHandler:handleError,
					callback: function(data)
					{
					if(data=="isDuplicate")
					{
						updateReturnThreadCount("ref");
						$('#errordivElectronicReferences').append("&#149; "+resourceJSON.ElectronicRefAlreadyReg+"<br>");
						if(focs==0)
							$('#email').focus();
						$('#email').css("background-color",txtBgColor);
						cnt++;focs++;
						$('#errordivElectronicReferences').show();
						return false;
					}else{
						//$('#loadingDiv').show();
						document.getElementById("frmElectronicReferences").submit();
					}
					}
				});
				
			}else{
				PFCertifications.saveOrUpdateElectronicReferences(teacherElectronicReferences,{ 
					async: true,
					errorHandler:handleError,
					callback: function(data)
					{
						if(data=="isDuplicate")
						{
							updateReturnThreadCount("ref");
							$('#errordivElectronicReferences').append("&#149; "+resourceJSON.ElectronicRefAlreadyRegWithProvidedEmail+"<br>");
							if(focs==0)
								$('#email').focus();
							$('#email').css("background-color",txtBgColor);
								cnt++;focs++;
							$('#errordivElectronicReferences').show();
							return false;
						}
						hideElectronicReferencesForm();
						getElectronicReferencesGrid();
						if(sbtsource==1)
						{
							saveAllDSPQ();
							updateReturnThreadCount("ref");
//							validatePortfolioErrorMessageAndGridData('level2');
						}
					}
				});
			}
			
			/*PFCertifications.saveOrUpdateElectronicReferences(teacherElectronicReferences,{ 
				async: false,
				errorHandler:handleError,
				callback: function(data)
				{
					if(data=="isDuplicate")
					{
						$('#errordivElectronicReferences').append("&#149; An Electronic Reference has already registered with the provided Email.<br>");
						if(focs==0)
							$('#email').focus();
						$('#email').css("background-color",txtBgColor);
							cnt++;focs++;
						$('#errordivElectronicReferences').show();
						return false;
					}
					else
					{
						hideElectronicReferencesForm();
						getElectronicReferencesGrid();
						
						validatePortfolioErrorMessageAndGridData('level2');
					}
				}
			});*/
			dwr.engine.endBatch();
			return true;
		}
	}
	
	
		
}

function saveAcademics(fileName,sbtsource)
{
	//$('#testcall').append("&#149; saveAcademics<br>");
	var academicId = document.getElementById("academicId");
	
	var degreeId = document.getElementById("degreeId");
	var universityId = document.getElementById("universityId");
	var fieldId = document.getElementById("fieldId");   
	
	var attendedInYear = document.getElementById("attendedInYear"); 
	var leftInYear = document.getElementById("leftInYear");     
	var gpaFreshmanYear = document.getElementById("gpaFreshmanYear");     
	var gpaJuniorYear = document.getElementById("gpaJuniorYear");     
	var gpaSophomoreYear = document.getElementById("gpaSophomoreYear");     
	var gpaSeniorYear = document.getElementById("gpaSeniorYear");     
	var gpaCumulative = ""; 
	var degreeType = document.getElementById("degreeType");
	if(trim(degreeType.value)=="B"){
		gpaCumulative = document.getElementById("gpaCumulative");
	}else{
		gpaCumulative = document.getElementById("gpaCumulative1")
	}
	var international=0;
	if(degreeType.value=="B" && document.getElementById('international').checked){
        international=1;
    }else if(degreeType.value=="M" && document.getElementById('international2').checked){
    	international=1;        	
    }else if(degreeType.value=="A" && document.getElementById('international2').checked){
    	international=1;        	
    }else if(degreeType.value=="D" && document.getElementById('international2').checked){
    	international=1;        	
    }
	
	PFAcademics.saveOrUpdateAcademics(academicId.value,degreeId.value,universityId.value,fieldId.value,
			attendedInYear.value,leftInYear.value,gpaFreshmanYear.value,gpaJuniorYear.value,
			gpaSophomoreYear.value,gpaSeniorYear.value,gpaCumulative.value,degreeType.value,fileName,international,
	{ 
	async: false,
	errorHandler:handleError,
	callback: function(data)
		{	
			//$('#loadingDiv').hide();
			showGridAcademics();
			$("#hrefAcademics").addClass("stepactive");
			resetUniversityForm();
			if(sbtsource==1)
			{
				saveAllDSPQ();
				updateReturnThreadCount("aca");
//				validatePortfolioErrorMessageAndGridData('level2');
			}
		}
	});
	
	/*if(finalcallStatus==1)
	{
		sendToOtherPager();
	}*/
}
function saveAdditionalDocuments(fileName,sbtsource)
{
	//alert("fileName "+fileName+" sbtsource "+sbtsource);
	var documentName = document.getElementById("documentName").value;
	var uploadedDocumentHidden = document.getElementById("uploadedDocumentHidden").value;
	var additionDocumentId=document.getElementById("additionDocumentId").value;
	
	PFCertifications.saveOrUpdateAdditionalDocuments(additionDocumentId,documentName,fileName,uploadedDocumentHidden,
	{ 
	async: false,
	errorHandler:handleError,
	callback: function(data)
		{	
			showGridAdditionalDocuments()		
			resetAdditionalDocumentsForm();
			if(sbtsource==1)
			{
				saveAllDSPQ();
				updateReturnThreadCount("adddoc");
//				validatePortfolioErrorMessageAndGridData('level2');
			}
		}
	});
	
}

var saveRefFlag=0;
function saveReference(fileName,sbtsource){
	//$('#testcall').append("&#149; saveReference<br>");
	
	var elerefAutoId	=	document.getElementById("elerefAutoId");
	var salutation		=	document.getElementById("salutation");
	
	var firstName		=	trim(document.getElementById("firstName").value);
	var lastName		= 	trim(document.getElementById("lastName").value);;
	var designation		= 	trim(document.getElementById("designation").value);
	
	var organization	=	trim(document.getElementById("organization").value);
	var email			=	trim(document.getElementById("email").value);
	
	var contactnumber	=	trim(document.getElementById("contactnumber").value);
	var rdcontacted0	=   document.getElementById("rdcontacted0");
	var rdcontacted1	=   document.getElementById("rdcontacted1");
	var longHaveYouKnow	=   trim(document.getElementById("longHaveYouKnow").value);
	var referenceDetails 	= trim($("#referenceDetailText").find(".jqte_editor").html());
	var cnt=0;
	var focs=0;	
	
	var rdcontacted_value;
	if (rdcontacted0.checked) {
		rdcontacted_value = false;
	}
	else if (rdcontacted1.checked) {
		rdcontacted_value = true;
	}
	
	var teacherElectronicReferences = {elerefAutoId:null,salutation:null, firstName:null, lastName:null, designation:null, organization:null,email:null,contactnumber:null,rdcontacted:null,pathOfReference:null,referenceDetails:null,longHaveYouKnow:null};
	dwr.engine.beginBatch();
	dwr.util.getValues(teacherElectronicReferences);
	
	teacherElectronicReferences.salutation=salutation.value;
	teacherElectronicReferences.rdcontacted=rdcontacted_value;
	teacherElectronicReferences.firstName=firstName;
	teacherElectronicReferences.lastName=lastName;;
	teacherElectronicReferences.designation=designation;
	teacherElectronicReferences.organization=organization;
	teacherElectronicReferences.email=email;
	teacherElectronicReferences.pathOfReference=fileName;
	teacherElectronicReferences.referenceDetails=referenceDetails;
	teacherElectronicReferences.longHaveYouKnow=longHaveYouKnow;
	PFCertifications.saveOrUpdateElectronicReferences(teacherElectronicReferences,{ 
		async: true,
		errorHandler:handleError,
		callback: function(data)
		{
			if(data=="isDuplicate")
			{
				updateReturnThreadCount("ref");
				$('#errordivElectronicReferences').append("&#149; "+resourceJSON.ElectronicRefAlreadyRegWithProvidedEmail+"<br>");
				if(focs==0)
					$('#email').focus();
				$('#email').css("background-color",txtBgColor);
					cnt++;focs++;
				$('#errordivElectronicReferences').show();
				return false;
			}
			//$('#loadingDiv').hide();
			saveRefFlag=1;
			getElectronicReferencesGrid();
			hideElectronicReferencesForm();
			if(sbtsource==1)
			{
				saveAllDSPQ();
				updateReturnThreadCount("ref");
//				validatePortfolioErrorMessageAndGridData('level2');
			}
		}
	});
	dwr.engine.endBatch();
	return true;
}

function chkForEnterElectronicReferences(evt)
{
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	//alert(">?>"+evt.srcElement.id)
	if(charCode==13 && evt.srcElement.id!="")
	{
		insertOrUpdateElectronicReferences();
	}	
}
function isEmailAddress(str) 
{
	var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
	return emailPattern.test(str);
}
function closeDSPQCal()
{
	try
	{
		if(dspqCalInstance!=null)
		{
			dspqCalInstance.hide();
		}
	}
	catch(e){}
}

function saveAndContinueDynamicPortfolio(){
	resetSBTNSource();
	closeDSPQCal();
	$('#loadingDiv_dspq_ie').show();	
	var sbtsource=1;
	var isValidate=true;
	var count = 0;
		
	if($("#degreeName").is(':visible'))
	{
		document.getElementById("sbtsource_aca").value=1;
		isValidate=false;
		insertOrUpdate_Academic(sbtsource);
	}	
	if($("#stateMaster").is(':visible'))
	{
		document.getElementById("sbtsource_cert").value=1;
		isValidate=false;
		insertOrUpdate_Certification(sbtsource);
	}
	else
	{
		displayGKAndSubject();
	}
	
	if($("#salutation").is(':visible'))
	{
		document.getElementById("sbtsource_ref").value=1;
		isValidate=false;
		insertOrUpdateElectronicReferences(sbtsource);
	}
	
	if($("#role").is(':visible'))
	{
		document.getElementById("sbtsource_emp").value=1;
		isValidate=false;
		insertOrUpdateEmployment(sbtsource);
	}
	
	
	if($("#examStatus").is(':visible'))
	{
		document.getElementById("sbtsource_subArea").value=1;
		isValidate=false;
		saveSubjectAreas(sbtsource);
	}
	
	
	if($("#documentName").is(':visible'))
	{
		document.getElementById("sbtsource_aadDoc").value=1;
		isValidate=false;
		insertOrUpdate_AdditionalDocuments(sbtsource);
	}
	
	if($("#videourl").is(':visible'))
	{
		document.getElementById("sbtsource_videoLink").value=1;
		isValidate=false;
		insertOrUpdatevideoLinks();
	}
	else
	{
		getVideoLinksGrid();
	}
	
	if($("#schoolNameStdTch").is(':visible'))
	{		
		isValidate=false;
		insertOrUpdateStdTchrExp();
	}
	if($("#languageText").is(':visible'))
	{		
		isValidate=false;
		insertOrUpdateTchrLang();
	}
	if($("#organizationInv").is(':visible'))
	{	isValidate=false;	
		saveOrUpdateInvolvement();
	}
	if($("#honor").is(':visible'))
	{	isValidate=false;	
		saveOrUpdateHonors();
	}
		
	if($("#languageDiv").is(':visible'))
	{
		try {
			updateLangeuage();
		} catch (e) {
			// TODO: handle exception
		}
		
	}
	
	saveAndContinueBottomPart();
	
	// validate Dynamic Portfolio
	if(isValidate)
	{
//		validatePortfolioErrorMessageAndGridData('level2');
	}

}
function chkNonTeacher(){
	if(document.getElementById("isNonTeacher").checked){
		document.getElementById("expCertTeacherTraining").value="0.0";		
		document.getElementById("expCertTeacherTraining").disabled=true;
	}else{
		document.getElementById("expCertTeacherTraining").disabled=false;
	}
}

function chkDistrict()
{
var districtIdForDSPQ = $("#districtIdForDSPQ").val();
	
	if(districtIdForDSPQ==1200390)
		document.getElementById('ssn_pi').setAttribute('maxlength','9');
	else if(districtIdForDSPQ==4218990)
		document.getElementById('ssn_pi').setAttribute('maxlength','9');
	else if(districtIdForDSPQ==3680340 || districtIdForDSPQ==806900)
		document.getElementById('ssn_pi').setAttribute('maxlength','4');
}

function chkRetired()
{
	var isretired = document.getElementById("isretired").checked;
	
	if(isretired==true)
		$('#isRetiredDiv').show();
	else
		$('#isRetiredDiv').hide();
}

function validateDOBYearForDSPQ()
{
	$('#errDOB').hide();
	$('#errDOB').empty();
	
	$('#dobYear').css("background-color","");
	$('#dobMonth').css("background-color","");
	$('#dobDay').css("background-color","");
	
	var dobYear=$("#dobYear").val();
	dobYear=trim(dobYear);
	var idobYear = new String(parseInt(dobYear));
	var currentFullYear = new Date().getFullYear();
	currentFullYear=currentFullYear-1;
	
	var dobMonth=$("#dobMonth").val();
	var dobDay=$("#dobDay").val();
	if(document.getElementById("districtIdForDSPQ").value==4218990){		
		
		if(trim(dobMonth)!="0"){
			if(trim(dobDay)=="0")
			{
				$('#errDOB').append("&#149; "+resourceJSON.msgDayofBirth+"<br>");
				$('#dobDay').css("background-color",txtBgColor);
				$('#errDOB').show();
			}
			
			if((dobYear=="") || ( dobYear==0 || idobYear=="NaN") || (idobYear > currentFullYear || idobYear < 1931))
			{
				$('#errDOB').append("&#149; "+resourceJSON.msgValidYearBirth+"<br>");
				$('#dobYear').css("background-color",txtBgColor);
				$('#errDOB').show();
				return false;
			}
		
		}else if(trim(dobDay)!="0" ){	
			if(trim(dobMonth)=="0")
			{
				$('#errDOB').append("&#149; "+resourceJSON.msgMonthofBirth+"<br>");
				$('#dobMonth').css("background-color",txtBgColor);
				$('#errDOB').show();
			}
			
			if((dobYear=="") || ( dobYear==0 || idobYear=="NaN") || (idobYear > currentFullYear || idobYear < 1931))
			{
				$('#errDOB').append("&#149; "+resourceJSON.msgValidYearBirth+"<br>");
				$('#dobYear').css("background-color",txtBgColor);
				$('#errDOB').show();
				return false;
			}
		}else if((dobYear!="") || ( dobYear!=0 || idobYear!="NaN") || (idobYear < currentFullYear || idobYear > 1931)){
			if(trim(dobMonth)=="0")
			{
				$('#errDOB').append("&#149; "+resourceJSON.msgMonthofBirth+"<br>");
				$('#dobMonth').css("background-color",txtBgColor);
				$('#errDOB').show();
			}
			
			if(trim(dobDay)=="0")
			{
				$('#errDOB').append("&#149; "+resourceJSON.msgDayofBirth+"<br>");
				$('#dobDay').css("background-color",txtBgColor);
				$('#errDOB').show();
			}
		}
	}
	else{
	if(trim(dobMonth)=="0")
	{
		$('#errDOB').append("&#149; "+resourceJSON.msgMonthofBirth+"<br>");
		$('#dobMonth').css("background-color",txtBgColor);
		$('#errDOB').show();
		//return false;
	}
	
	if(trim(dobDay)=="0")
	{
		$('#errDOB').append("&#149; "+resourceJSON.msgDayofBirth+"<br>");
		$('#dobDay').css("background-color",txtBgColor);
		$('#errDOB').show();
		//return false;
	}
	
	if((dobYear=="") || ( dobYear==0 || idobYear=="NaN") || (idobYear > currentFullYear || idobYear < 1931))
	{
		$('#errDOB').append("&#149; "+resourceJSON.msgValidYearBirth+"<br>");
		$('#dobYear').css("background-color",txtBgColor);
		$('#errDOB').show();
		return false;
	}
	
}
	/*else if(dobYear==0 || idobYear=="NaN")
	{
		$('#errDOB').append("&#149; Please enter a valid year of birth.<br>");
		$('#dobYear').css("background-color",txtBgColor);
		$('#errDOB').show();
		return false;
	}
	else if(idobYear > currentFullYear || idobYear < 1931)
	{
		$('#errDOB').append("&#149; Please enter a valid year of birth.<br>");
		$('#dobYear').css("background-color",txtBgColor);
		$('#errDOB').show();
		return false;
	}*/
	return true;
}

function clearCertType()
{
	$("#certificateTypeMaster").val("");
	$("#certType").val("");
}

function showdistrictspeciFicVeteran(){		
	$("#vetranOptionDiv").hide();
	
	if(document.getElementById("districtIdForDSPQ").value==1200390 && $("#vt1").is(':checked')){
		$("#vetranOptionDiv").html("Please <a href='javascript:void(0);' onclick=\"window.open('https://platform.teachermatch.org/pdf/6888VeteranPreference.pdf','_blank', 'width = 1050px, height = 600px');\" download>download</a>, complete and submit this form.");
		$("#vetranOptionDiv").show();

		//$("#message2show").html("Please <a href='javascript:void(0);' onclick=\"window.open('https://platform.teachermatch.org/6888VeteranPreference.pdf','_blank', 'width = 1050px, height = 600px');\" download>download</a>, complete and submit this form.");
		//$('#myModal2').modal('show');
	}else if(document.getElementById("districtIdForDSPQ").value==1201470 && $("#vt1").is(':checked')){
	var districtMaster = {districtId:dwr.util.getValue("districtIdForDSPQ")};
	
	PFExperiences.getTeacherVeteran(districtMaster,
			{ 
				async: false,
				errorHandler:handleError,
				callback: function(data)
				{					
					var res = data.split("#");
					$("input[id=veteranOptS][value='"+res[0]+"']").prop("checked",true);
					$("input[id=veteranPreference1][value='"+res[1]+"']").prop("checked",true);
					$("input[id=veteranPreference2][value='"+res[2]+"']").prop("checked",true);
					$("#vetranOptionDiv").show();
				}
			});	
	
}
}

function showEmpDate(chkflag)
{
	if(chkflag==0)
	{
		if($('#empchk11').is(':checked'))
			$('#rtDateDiv').show();
		else
			$('#rtDateDiv').hide();
	}
	else if(chkflag==1)
	{
		if($('#empchk12').is(':checked'))
			$("#wtDateDiv").show();
		else
			$("#wtDateDiv").hide();
	}
}
function checkEmployeeCode(){
	var districtIdForDSPQ = $("#districtIdForDSPQ").val();	
	
	//if($('#empfe2').val().length==10 && districtIdForDSPQ=="4218990" && $('#jobcategoryDsp').val()=="Principal"){
	if(districtIdForDSPQ=="4218990" && $('#jobcategoryDsp').val()=="Principal"){		
		var text="";
		var empCode= $('#empfe2').val();
		var countl =10-empCode.length;
		for (i = 0; i < countl; i++) {
			text+="0";
		}
		var finalempcode = text+empCode;

		DistrictPortfolioConfigAjax.checkEmployee(finalempcode,districtIdForDSPQ,{  
			async: false,
			callback: function(data){
			//alert(data);
			if(data[0]!=null){
				//if($('input:radio[name=staffType]').is(":checked") || $("#staffTypeSession").val()=="I"){
				if($("#isPrinciplePhiladelphia").val()=="1" && data[1]=="false" && data[0]==1 || data[0]==3){
					$("#myModalDymanicPortfolio").hide();				
					$("#affDivPHiL").hide();
					$("#dynamicInfoMsg").html(""+resourceJSON.msgCoverLetterdetails+"");
					$("#wrongDivPHiL").show();
				}else if(data[1]=="true" && data[0]==0){
					$("#myModalDymanicPortfolio").hide();
					
					$("#affDivPHiL").hide();
					$("#dynamicInfoMsg").html(""+resourceJSON.msgNonPrincipalCoverLetter+"");
					$("#wrongDivPHiL").show();
				}
				}else{
					$("#myModalDymanicPortfolio").hide();
					
					$("#affDivPHiL").hide();
					$("#dynamicInfoMsg").html(""+resourceJSON.msgNonPrincipalCoverLetter+"");
					$("#wrongDivPHiL").show();
				}
		},
		errorHandler:handleError
		});	
	}
}
function chkNBCert(chk)
{
	if(chk=="1")
	{
		document.getElementById("divNbdCert").style.display="block";
	}
	else
	{
		document.getElementById("divNbdCert").style.display="none";
	}
}

function downloadUploadedDocument(additionDocumentId, linkId)
{		
		PFCertifications.downloadUploadedDocument(additionDocumentId,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
			if(data=="")
			{
				data="javascript:void(0)";
			}
			else if(data.indexOf(".doc")!=-1)
			{
				document.getElementById("ifrmRef").src=data;
			}else
			{
				document.getElementById(linkId).href = data;
			}
			return false;
		}});
}
function downloadReference(referenceId, linkId)
{		
		PFCertifications.downloadReference(referenceId,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	if(data=="")
			{
				data="javascript:void(0)";
			}
			else if(data.indexOf(".doc")!=-1)
			{
				document.getElementById("ifrmRef").src=data;
			}else
			{
				document.getElementById(linkId).href = data;
			}
			return false;
		}});
}

function downloadResume()
{
	PFExperiences.downloadResume(
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
			if (data.indexOf(".doc") !=-1) {
			    document.getElementById('ifrmResume').src = ""+data+"";
			}
			else
			{
				document.getElementById("hrefResume").href = data;
				return false;
			}
			
			if(deviceType)
			{
				if (data.indexOf(".doc")!=-1)
				{
					$("#docfileNotOpen").css({"z-index":"3000"});
			    	$('#docfileNotOpen').show();
				}
				else if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					$("#exelfileNotOpen").css({"z-index":"3000"});
			    	$('#exelfileNotOpen').show();
				}
				else
				{
					document.getElementById("hrefResume").href = data;
				}
			}
			else if(deviceTypeAndroid)
			{
				if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					document.getElementById('ifrmResume').src = ""+data+"";
				}
				else
				{
					document.getElementById("hrefResume").href = data;	
				}	
			}
			else if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
			{
				document.getElementById('ifrmResume').src = ""+data+"";
			}
			else
			{
				document.getElementById("hrefResume").href = data;	
			}			 
			return false;
			
		}
	});
}
function saveResume(ext)
{
	//alert("ext::"+ext);
	PFExperiences.addOrEditResume(ext, { 
	async: true,
	errorHandler:handleError,
	callback:function(data)
	{
		document.getElementById("frmExpResumeUpload").reset();
		$("a#hrefResume").html(ext);
		getTFAValues();
		validatePortfolioErrorMessageAndGridData('level2');
		
	}});
}
//Pavan Gupta
function saveBackgroundCheck(ext)
{

	PFExperiences.addOrEditResume(ext, { 
	async: true,
	errorHandler:handleError,
	callback:function(data)
	{
		document.getElementById("frmBackgroundCheckUpload").reset();
		$("a#hrefBackgroundCheck").html(ext);
		
	}});
}


function showDSPQForPortRemind()
{	var disId=document.getElementById("districtIdForDSPQ").value;
	PFCertifications.getDSPQForPortfolioReminder(disId,{  
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			var dataArray = data.split("@##@");
			totalQuestionsList = dataArray[1];
			var textmsg = dataArray[2];
			$('#divGridDistrictSpecificQuestions').html(dataArray[0]);
		}});
}

/*===========Start For Resolving IE TextArea Maxlength Problem ================*/
function chkLenOfTextArea(textValue,maxlength)
{
	var tlen=textValue.value.length;
	if(tlen>maxlength)
	{
		document.getElementById(textValue.id).value=document.getElementById(textValue.id).value.substr(0,maxlength)
	}
}

function checkForInt(evt) {

	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==8)
		return true;

	return( (charCode >= 48 && charCode <= 57)||(charCode==13) );
}

function setFocus(id)
{
	if(id!="")
		document.getElementById(id).focus();
}

function showPortfolioInstruction(){
	
	var districtId = $('#districtIdForDSPQ').val();
	
	PortfolioReminderAjax.getPortfolioInstruction(districtId,{
		async: true,
		errorHandler:handleError,
		callback: function(data){
			$('#messageApplicantManagement').html(data);
		},
		errorHandler:handleError  
	});
}

/*===========End For Resolving IE TextArea Maxlength Problem ================*/

