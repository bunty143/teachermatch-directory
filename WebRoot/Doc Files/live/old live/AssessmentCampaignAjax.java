package tm.services;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.DistrictPortfolioConfig;
import tm.bean.EpiTakenList;
import tm.bean.EventParticipantsList;
import tm.bean.InternalTransferCandidates;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.TeacherAnswerDetail;
import tm.bean.TeacherAssessmentAttempt;
import tm.bean.TeacherAssessmentOption;
import tm.bean.TeacherAssessmentQuestion;
import tm.bean.TeacherAssessmentStatus;
import tm.bean.TeacherAssessmentdetail;
import tm.bean.TeacherDetail;
import tm.bean.TeacherPersonalInfo;
import tm.bean.TeacherPortfolioStatus;
import tm.bean.TeacherSectionDetail;
import tm.bean.TeacherStrikeLog;
import tm.bean.assessment.AssessmentDetail;
import tm.bean.assessment.AssessmentGroupDetails;
import tm.bean.assessment.AssessmentJobRelation;
import tm.bean.assessment.AssessmentQuestions;
import tm.bean.assessment.AssessmentSections;
import tm.bean.assessment.QuestionOptions;
import tm.bean.assessment.QuestionsPool;
import tm.bean.assessment.TeacherWiseAssessmentTime;
import tm.bean.cgreport.PercentileZscoreTscore;
import tm.bean.cgreport.TmpPercentileWiseZScore;
import tm.bean.i4.I4InterviewInvites;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictSpecificQuestions;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.OptionsForDistrictSpecificQuestions;
import tm.bean.master.QqQuestionSets;
import tm.bean.master.QqQuestionsetQuestions;
import tm.bean.master.QuestionTypeMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.StatusMaster;
import tm.bean.master.TeacherAnswerDetailsForDistrictSpecificQuestions;
import tm.bean.master.TeacherAnswerHistory;
import tm.bean.user.UserMaster;
import tm.dao.CandidateRawScoreDAO;
import tm.dao.EpiTakenListDAO;
import tm.dao.EventParticipantsListDAO;
import tm.dao.InternalTransferCandidatesDAO;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.TeacherAnswerDetailDAO;
import tm.dao.TeacherAssessmentAttemptDAO;
import tm.dao.TeacherAssessmentDetailDAO;
import tm.dao.TeacherAssessmentOptionDAO;
import tm.dao.TeacherAssessmentQuestionDAO;
import tm.dao.TeacherAssessmentStatusDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.TeacherPersonalInfoDAO;
import tm.dao.TeacherPortfolioStatusDAO;
import tm.dao.TeacherSectionDetailDAO;
import tm.dao.TeacherStrikeLogDAO;
import tm.dao.assessment.AssessmentDetailDAO;
import tm.dao.assessment.AssessmentDomainScoreDAO;
import tm.dao.assessment.AssessmentGroupDetailsDAO;
import tm.dao.assessment.AssessmentJobRelationDAO;
import tm.dao.assessment.AssessmentQuestionsDAO;
import tm.dao.assessment.AssessmentSectionDAO;
import tm.dao.assessment.QuestionsPoolDAO;
import tm.dao.assessment.TeacherWiseAssessmentTimeDAO;
import tm.dao.master.ContactTypeMasterDAO;
import tm.dao.master.DistrictKeyContactDAO;
import tm.dao.master.DistrictSpecificQuestionsDAO;
import tm.dao.master.OptionsForDistrictSpecificQuestionsDAO;
import tm.dao.master.QqQuestionsetQuestionsDAO;
import tm.dao.master.SchoolKeyContactDAO;
import tm.dao.master.StatusMasterDAO;
import tm.dao.master.TeacherAnswerDetailsForDistrictSpecificQuestionsDAO;
import tm.dao.master.TeacherAnswerHistoryDAO;
import tm.dao.master.UserEmailNotificationsDAO;
import tm.dao.user.UserMasterDAO;
import tm.services.district.PrintOnConsole;
import tm.services.report.CGReportService;
import tm.services.teacher.DistrictPortfolioConfigAjax;
import tm.servlet.WorkThreadServlet;
import tm.utility.Utility;
public class AssessmentCampaignAjax {

	
	String locale = Utility.getValueOfPropByKey("locale");
	
	@Autowired
	private InternalTransferCandidatesDAO internalTransferCandidatesDAO;
	
	@Autowired
	private TeacherDetailDAO teacherDetailDAO;

	@Autowired
	private AssessmentDetailDAO assessmentDetailDAO;
	public void setAssessmentDetailDAO(AssessmentDetailDAO assessmentDetailDAO) {
		this.assessmentDetailDAO = assessmentDetailDAO;
	}

	@Autowired
	private AssessmentJobRelationDAO assessmentJobRelationDAO;
	public void setAssessmentJobRelationDAO(AssessmentJobRelationDAO assessmentJobRelationDAO) {
		this.assessmentJobRelationDAO = assessmentJobRelationDAO;
	}

	@Autowired 
	private AssessmentSectionDAO assessmentSectionDAO;
	public void setAssessmentSectionDAO(AssessmentSectionDAO assessmentSectionDAO) {
		this.assessmentSectionDAO = assessmentSectionDAO;
	}

	@Autowired
	private AssessmentQuestionsDAO assessmentQuestionsDAO;
	public void setAssessmentQuestionsDAO(AssessmentQuestionsDAO assessmentQuestionsDAO) {
		this.assessmentQuestionsDAO = assessmentQuestionsDAO;
	}

	@Autowired
	private TeacherAssessmentStatusDAO teacherAssessmentStatusDAO;
	public void setTeacherAssessmentStatusDAO(TeacherAssessmentStatusDAO teacherAssessmentStatusDAO) {
		this.teacherAssessmentStatusDAO = teacherAssessmentStatusDAO;
	}

	@Autowired
	private TeacherAssessmentDetailDAO teacherAssessmentDetailDAO;
	public void setTeacherAssessmentDetailDAO(TeacherAssessmentDetailDAO teacherAssessmentDetailDAO) {
		this.teacherAssessmentDetailDAO = teacherAssessmentDetailDAO;
	}

	@Autowired
	private TeacherSectionDetailDAO teacherSectionDetailDAO;
	public void setTeacherSectionDetailDAO(TeacherSectionDetailDAO teacherSectionDetailDAO) {
		this.teacherSectionDetailDAO = teacherSectionDetailDAO;
	}

	@Autowired
	private TeacherAssessmentQuestionDAO teacherAssessmentQuestionDAO;
	public void setTeacherAssessmentQuestionDAO(TeacherAssessmentQuestionDAO teacherAssessmentQuestionDAO) {
		this.teacherAssessmentQuestionDAO = teacherAssessmentQuestionDAO;
	}

	@Autowired
	private TeacherAssessmentOptionDAO teacherAssessmentOptionDAO;
	public void setTeacherAssessmentOptionDAO(TeacherAssessmentOptionDAO teacherAssessmentOptionDAO) {
		this.teacherAssessmentOptionDAO = teacherAssessmentOptionDAO;
	}

	@Autowired
	private TeacherAssessmentAttemptDAO teacherAssessmentAttemptDAO;
	public void setTeacherAssessmentAttemptDAO(TeacherAssessmentAttemptDAO teacherAssessmentAttemptDAO) {
		this.teacherAssessmentAttemptDAO = teacherAssessmentAttemptDAO;
	}

	@Autowired
	private StatusMasterDAO statusMasterDAO;
	public void setStatusMasterDAO(StatusMasterDAO statusMasterDAO) {
		this.statusMasterDAO = statusMasterDAO;
	}

	@Autowired
	private TeacherAnswerDetailDAO teacherAnswerDetailDAO;
	public void setTeacherAnswerDetailDAO(TeacherAnswerDetailDAO teacherAnswerDetailDAO) {
		this.teacherAnswerDetailDAO = teacherAnswerDetailDAO;
	}

	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	public void setJobForTeacherDAO(JobForTeacherDAO jobForTeacherDAO) {
		this.jobForTeacherDAO = jobForTeacherDAO;
	}

	@Autowired
	private DistrictSpecificQuestionsDAO districtSpecificQuestionsDAO;
	public void setDistrictSpecificQuestionsDAO(
			DistrictSpecificQuestionsDAO districtSpecificQuestionsDAO) {
		this.districtSpecificQuestionsDAO = districtSpecificQuestionsDAO;
	}

	@Autowired
	private JobOrderDAO jobOrderDAO;
	public void setJobOrderDAO(JobOrderDAO jobOrderDAO) {
		this.jobOrderDAO = jobOrderDAO;
	}

	@Autowired
	private TeacherStrikeLogDAO teacherStrikeLogDAO;
	public void setTeacherStrikeLogDAO(TeacherStrikeLogDAO teacherStrikeLogDAO) {
		this.teacherStrikeLogDAO = teacherStrikeLogDAO;
	}

	@Autowired
	private CandidateRawScoreDAO candidateRawScoreDAO;
	public void setCandidateRawScoreDAO(CandidateRawScoreDAO candidateRawScoreDAO) {
		this.candidateRawScoreDAO = candidateRawScoreDAO;
	}

	@Autowired
	private EpiTakenListDAO epiTakenListDAO;
	public void setEpiTakenListDAO(EpiTakenListDAO epiTakenListDAO) {
		this.epiTakenListDAO = epiTakenListDAO;
	}

	@Autowired
	private TeacherPortfolioStatusDAO teacherPortfolioStatusDAO;
	public void setTeacherPortfolioStatusDAO(TeacherPortfolioStatusDAO teacherPortfolioStatusDAO) {
		this.teacherPortfolioStatusDAO = teacherPortfolioStatusDAO;
	}

	@Autowired
	private ContactTypeMasterDAO contactTypeMasterDAO;
	public void setContactTypeMasterDAO(ContactTypeMasterDAO contactTypeMasterDAO) {
		this.contactTypeMasterDAO = contactTypeMasterDAO;
	}

	@Autowired
	private DistrictKeyContactDAO districtKeyContactDAO;
	public void setDistrictKeyContactDAO(DistrictKeyContactDAO districtKeyContactDAO) {
		this.districtKeyContactDAO = districtKeyContactDAO;
	}

	@Autowired
	private SchoolKeyContactDAO schoolKeyContactDAO;
	public void setSchoolKeyContactDAO(SchoolKeyContactDAO schoolKeyContactDAO) {
		this.schoolKeyContactDAO = schoolKeyContactDAO;
	}

	@Autowired
	private UserEmailNotificationsDAO userEmailNotificationsDAO;
	public void setUserEmailNotificationsDAO(
			UserEmailNotificationsDAO userEmailNotificationsDAO) {
		this.userEmailNotificationsDAO = userEmailNotificationsDAO;
	}

	@Autowired
	private CommonService commonService;

	@Autowired
	private TeacherAnswerDetailsForDistrictSpecificQuestionsDAO teacherAnswerDetailsForDistrictSpecificQuestionsDAO;
	
	@Autowired
	private TeacherAnswerHistoryDAO teacherAnswerHistoryDAO;

	@Autowired
	private UserMasterDAO userMasterDAO;

	@Autowired
	private DistrictPortfolioConfigAjax districtPortfolioConfigAjax;

	@Autowired
	private EmailerService emailerService;
	public void setEmailerService(EmailerService emailerService) 
	{
		this.emailerService = emailerService;
	}

	@Autowired
	private TeacherPersonalInfoDAO teacherPersonalInfoDAO;

	@Autowired
	private TeacherWiseAssessmentTimeDAO teacherWiseAssessmentTimeDAO;

	@Autowired
	private QuestionsPoolDAO questionsPoolDAO;
	
	@Autowired
	private AssessmentDomainScoreDAO assessmentDomainScoreDAO;
	
	@Autowired
	private CGReportService reportService;
	@Autowired
	private QqQuestionsetQuestionsDAO qqQuestionsetQuestionsDAO;
	
	@Autowired
	private OptionsForDistrictSpecificQuestionsDAO optionsForDistrictSpecificQuestionsDAO;
	
	List<TmpPercentileWiseZScore> tmpPercentileWiseZScoreList = null;
	public void setTmpPercentileWiseZScoreList(
			List<TmpPercentileWiseZScore> tmpPercentileWiseZScoreList) {
		this.tmpPercentileWiseZScoreList = tmpPercentileWiseZScoreList;
	}
	
	List<PercentileZscoreTscore> pztList = null;
	public void setPztList(List<PercentileZscoreTscore> pztList) {
		this.pztList = pztList;
	}
	
	HttpServletRequest httpServletRequest = null;
	public void setHttpServletRequest(HttpServletRequest httpServletRequest){
		this.httpServletRequest = httpServletRequest;
	}
	
	@Autowired
	private AssessmentGroupDetailsDAO assessmentGroupDetailsDAO;
	
	@Autowired
	private EventParticipantsListDAO eventParticipantsListDAO;
	
	
	/* @Author: Vishwanath Kumar
	 * @Discription: check Inventory.
	 */
	public AssessmentDetail checkInventory(JobOrder jobOrder, Integer epiJobId)
	{
		//System.out.println(":::::::::::::::Assessment checkInventory:::::::::::::::::::>>> ");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = null;
		HttpSession session = null;
		if(httpServletRequest!=null && httpServletRequest.getSession()!=null){
			request = httpServletRequest;
			session = request.getSession(false);
		}
		else{
			request = context.getHttpServletRequest();
			session = request.getSession(false);
		}
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}

		TeacherDetail teacherDetail = null;

		if (session == null || session.getAttribute("teacherDetail") == null) {
			//return "false";
		}else
			teacherDetail=(TeacherDetail)session.getAttribute("teacherDetail");

		int newJobId = jobOrder.getJobId();
		if(jobOrder.getJobId()!=null && jobOrder.getJobId()>0)
			jobOrder = jobOrderDAO.findById(newJobId, false, false);

		AssessmentDetail assessmentDetail = new AssessmentDetail();
		List<TeacherAssessmentStatus> teacherAssessmentStatusList = null;
		TeacherAssessmentStatus teacherAssessmentStatus = null;
		Integer assessmentTakenCount = null;
		teacherAssessmentStatusList=teacherAssessmentStatusDAO.findAssessmentTaken(teacherDetail,jobOrder);
		//////////////////////////////////////
		if(teacherAssessmentStatusList.size()==0 && newJobId!=0)
		{
			List<AssessmentJobRelation>	assessmentJobRelations1=assessmentJobRelationDAO.findRelationByJobOrder(jobOrder);
			if(assessmentJobRelations1.size()>0)
			{
				AssessmentJobRelation assessmentJobRelation1 = assessmentJobRelations1.get(0);
				teacherAssessmentStatusList = teacherAssessmentStatusDAO.findAssessmentTakenByTeacher(teacherDetail,assessmentJobRelation1.getAssessmentId());
			}
		}
		/////////////////////////////////////
		int oldJobId = 0;
		if(teacherAssessmentStatusList.size()!=0)
		{
			teacherAssessmentStatus = teacherAssessmentStatusList.get(0);
			assessmentDetail = teacherAssessmentStatus.getAssessmentDetail();
			assessmentTakenCount = teacherAssessmentStatus.getAssessmentTakenCount();
			String statusShortName=teacherAssessmentStatus.getStatusMaster().getStatusShortName();

			List<TeacherAssessmentStatus> teacherAssessmentStatusListIfexist = null;

			if(teacherAssessmentStatus.getAssessmentType()==2)
			{
				teacherAssessmentStatusListIfexist=teacherAssessmentStatusDAO.findAssessmentTakenByTeacher(teacherDetail,assessmentDetail);
				TeacherAssessmentStatus teacherAssessmentStatus1 = teacherAssessmentStatusListIfexist.get(0);
				oldJobId = teacherAssessmentStatus1.getJobOrder().getJobId();
				if(newJobId!=oldJobId)
				{
					jobOrder = teacherAssessmentStatus1.getJobOrder();
					//oldJobId = jobOrder.getJobId();
				}/*else
					oldJobId=0;*/
			}

			if(statusShortName.equalsIgnoreCase("comp"))
			{
				//shadab
				System.out.println("(((((((((((((((((((( "+Utility.checkDateBetweenTwoDate(jobOrder.getJobStartDate(), jobOrder.getJobEndDate()));
				if(!Utility.checkDateBetweenTwoDate(jobOrder.getJobStartDate(), jobOrder.getJobEndDate()))
				{
					//job expired
					assessmentDetail.setAssessmentName(null);
					assessmentDetail.setStatus("7");
					return assessmentDetail;
				}
				//shadab end
				assessmentDetail.setAssessmentName(null);
				assessmentDetail.setStatus("3");
				return assessmentDetail;
			}else if(statusShortName.equalsIgnoreCase("exp"))
			{
				assessmentDetail.setAssessmentName(null);
				assessmentDetail.setStatus("4");
				return assessmentDetail;
			}
			else if(statusShortName.equalsIgnoreCase("vlt"))
			{
				assessmentDetail.setAssessmentName(null);
				assessmentDetail.setStatus("5");
				return assessmentDetail;
			}
			else if(statusShortName.equalsIgnoreCase("icomp"))
			{
				List<TeacherAssessmentAttempt> teacherAssessmentAttempts = null;
				teacherAssessmentAttempts = teacherAssessmentAttemptDAO.findAssessmentAttempts(teacherDetail,jobOrder);
				int attempts = 0;
				for (TeacherAssessmentAttempt teacherAssessmentAttempt1 : teacherAssessmentAttempts) {
					if(!teacherAssessmentAttempt1.getIsForced())
						attempts++;
				}
				////////////////////////

				int strikeLogs=0; 
				try{
					if(teacherAssessmentAttempts.size()>0)
					{
						List<TeacherAssessmentStatus> teacherAssessmentStatusList2 = null;
						teacherAssessmentStatusList2 = teacherAssessmentStatusDAO.getTeacherAssessmentStatus(teacherAssessmentAttempts.get(0).getAssessmentDetail(),teacherDetail);
						List<TeacherStrikeLog> teacherStrikeLog = null;
						teacherStrikeLog = teacherStrikeLogDAO.getTotalStrikesByTeacher(teacherDetail, teacherAssessmentStatusList2.get(0).getTeacherAssessmentdetail());
						strikeLogs=teacherStrikeLog.size();
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				int attemptsStrike=attempts+strikeLogs;

				if(attemptsStrike==3)
				{
					StatusMaster statusMaster = WorkThreadServlet.statusMap.get("vlt");
					TeacherAssessmentdetail teacherAssessmentdetail = teacherAssessmentStatus.getTeacherAssessmentdetail();

					List<TeacherAssessmentStatus> lstTeacherAssessmentStatus = null;
					lstTeacherAssessmentStatus =teacherAssessmentStatusDAO.findAssessmentStatusByTeacherAssessmentdetail(teacherAssessmentdetail);
					teacherAssessmentStatus=lstTeacherAssessmentStatus.get(0);

					List<JobOrder> jobs = new ArrayList<JobOrder>();

					if(lstTeacherAssessmentStatus.size()!=0)
						if(teacherAssessmentStatus!=null)
						{
							for(TeacherAssessmentStatus tas: lstTeacherAssessmentStatus)
							{
								jobs.add(tas.getJobOrder());

								tas.setStatusMaster(statusMaster);
								teacherAssessmentStatusDAO.makePersistent(tas);
							}
						}
					//teacherAssessmentStatus

					// JobForTeacher update after 3 attempts
					if(jobOrder.getJobId()!=null && jobOrder.getJobId()>0)
					{
						List<AssessmentJobRelation>	assessmentJobRelations1=assessmentJobRelationDAO.findRelationByAssessment(teacherAssessmentStatus.getAssessmentDetail());

						for (AssessmentJobRelation assessmentJobRelation : assessmentJobRelations1) {
							jobs.add(assessmentJobRelation.getJobId());
						}

						List<JobForTeacher> jobForTeachers = jobForTeacherDAO.findJobByTeacherAndSatusFormJobIds(teacherDetail, jobs);

						String jftStatus = "";
						for(JobForTeacher jft: jobForTeachers)
						{
							jftStatus = jft.getStatus().getStatusShortName();
							if(!(jftStatus.equalsIgnoreCase("hird") || jftStatus.equalsIgnoreCase("widrw") || jftStatus.equalsIgnoreCase("hide")))
							{
								if(jft.getInternalSecondaryStatus()!=null){
									jft.setStatus(jft.getInternalStatus());
									jft.setStatusMaster(null);
									jft.setSecondaryStatus(jft.getInternalSecondaryStatus());
								}else if(jft.getInternalStatus()!=null){
									jft.setStatus(jft.getInternalStatus());
									jft.setStatusMaster(jft.getInternalStatus());
								}else{
									if(jftStatus.equalsIgnoreCase("icomp")|| jftStatus.equalsIgnoreCase("comp") || jftStatus.equalsIgnoreCase("vlt")){
										jft.setStatus(statusMaster);
										try{
											if(jft.getSecondaryStatus()!=null){
												jft.setStatusMaster(null);
											}else{
												jft.setStatusMaster(statusMaster);
											}
										}catch(Exception e){
											jft.setStatusMaster(statusMaster);
											e.printStackTrace();
										}
									}
								}
								// New Check JSI optional
								if(jft.getJobId().getJobAssessmentStatus()==1)
									jobForTeacherDAO.makePersistent(jft);
							}
						}
					}else
					{
						// base taken status 
						StatusMaster epiStatus = null;
						JobOrder jjj = new JobOrder();
						jjj.setJobId(0);
						List<TeacherAssessmentStatus> baseStatusList = teacherAssessmentStatusDAO.findAssessmentTaken(teacherDetail,jjj);
						if(baseStatusList.size()>0)
							epiStatus = baseStatusList.get(0).getStatusMaster();

						StatusMaster stautsIcomp= WorkThreadServlet.statusMap.get("icomp");
						//////////////////////////////////////////////
							
						///////////////////////// for external user /////////////////////////
						List<JobForTeacher> lstJobForTeacherIcomp = new ArrayList<JobForTeacher>();
						List<JobOrder> lstJBOrder = new ArrayList<JobOrder>();

						List<JobForTeacher> lstJobForTeacher= null;
						lstJobForTeacher = jobForTeacherDAO.findJobByTeacherAndSatusDuringCurrentYear(teacherDetail,stautsIcomp);
						for(JobForTeacher jft: lstJobForTeacher)
						{
							if(jft.getJobId().getIsJobAssessment()==false)
							{
								String jftStatus = jft.getStatus().getStatusShortName();
								if(jft.getInternalSecondaryStatus()!=null){
									jft.setStatus(jft.getInternalStatus());
									jft.setStatusMaster(null);
									jft.setSecondaryStatus(jft.getInternalSecondaryStatus());
								}else if(jft.getInternalStatus()!=null){
									jft.setStatus(jft.getInternalStatus());
									jft.setStatusMaster(jft.getInternalStatus());
								}else{
									if(jftStatus.equalsIgnoreCase("icomp")|| jftStatus.equalsIgnoreCase("comp") || jftStatus.equalsIgnoreCase("vlt")){
										jft.setStatus(statusMaster);
										try{
											if(jft.getSecondaryStatus()!=null){
												jft.setStatusMaster(null);
											}else{
												jft.setStatusMaster(statusMaster);
											}
										}catch(Exception e){
											jft.setStatusMaster(statusMaster);
											e.printStackTrace();
										}
									}
								}
								jobForTeacherDAO.makePersistent(jft);
							}
							else if(jft.getJobId().getJobCategoryMaster().getBaseStatus()){//**##**
								lstJobForTeacherIcomp.add(jft);
								lstJBOrder.add(jft.getJobId());
							}
						}

						TeacherAssessmentStatus tAStatus = null;
						if(lstJobForTeacherIcomp.size()>0){
							List<TeacherAssessmentStatus> lstTAStatus = teacherAssessmentStatusDAO.findByTeacherAndListJob(teacherDetail, lstJBOrder);
							Map<Integer, TeacherAssessmentStatus> mapAssessStatus = new HashMap<Integer, TeacherAssessmentStatus>();
							for(TeacherAssessmentStatus tas: lstTAStatus){
								mapAssessStatus.put(tas.getJobOrder().getJobId(), tas);
							}

							for(JobForTeacher jft: lstJobForTeacherIcomp){
								tAStatus = mapAssessStatus.get(jft.getJobId().getJobId());
								if(tAStatus!=null){
									String jftStatus = jft.getStatus().getStatusShortName();
									if(jft.getInternalSecondaryStatus()!=null){
										jft.setStatus(jft.getInternalStatus());
										jft.setStatusMaster(null);
										jft.setSecondaryStatus(jft.getInternalSecondaryStatus());
									}else if(jft.getInternalStatus()!=null){
										jft.setStatus(jft.getInternalStatus());
										jft.setStatusMaster(jft.getInternalStatus());
									}else{
										if(jftStatus.equalsIgnoreCase("icomp")|| jftStatus.equalsIgnoreCase("comp") || jftStatus.equalsIgnoreCase("vlt")){
											jft.setStatus(tAStatus.getStatusMaster());
											try{
												if(jft.getSecondaryStatus()!=null){
													jft.setStatusMaster(null);
												}else{
													jft.setStatusMaster(tAStatus.getStatusMaster());
												}
											}catch(Exception e){
												jft.setStatusMaster(tAStatus.getStatusMaster());
												e.printStackTrace();
											}	
										}
									}
									// New Check JSI optional
									if(jft.getJobId().getJobAssessmentStatus()==1)
										jobForTeacherDAO.makePersistent(jft);
								}else{
			                        try{
			                        	if(jft.getJobId().getJobCategoryMaster().getBaseStatus()){
				                            if(epiStatus!=null)
				                            {
					                             jft.setStatusMaster(epiStatus);
					                             jft.setStatus(epiStatus);
					                             jobForTeacherDAO.makePersistent(jft);
				                            }else
				                            {
				                            	 jft.setStatusMaster(stautsIcomp);
					                             jft.setStatus(stautsIcomp);
					                             jobForTeacherDAO.makePersistent(jft);
				                            	
				                            }
			                             }
			                         }catch(Exception e){
			                        	 e.printStackTrace();
			                         }
		                        }
							}

						}
						/////////////////////////////////////////////////////////////////////
					}

				}

				//ssessmentDetail.setStatus(assessmentDetail.getStatus()+"#"+attemptsStrike+"#"+oldJobId);
				assessmentDetail.setStatus(assessmentDetail.getStatus()+"#"+attemptsStrike+"#"+assessmentTakenCount+"#"+oldJobId);
				if(assessmentDetail.getAssessmentType()==1)
				{
					boolean isKelly = false;
					if(request.getServerName().contains("kelly") || request.getServerName().toLowerCase().contains("smartpractice") || request.getRequestURL().toString().contains("smartpractice")){
						isKelly = true;
					}
					boolean isKellyJobApply = false;
					try {
						isKellyJobApply = jobForTeacherDAO.getJFTByTeachers(teacherDetail);
					} catch (Exception e) {
						e.printStackTrace();
					}
					System.out.println("isKellyJobApply : "+isKellyJobApply);
					if(isKellyJobApply || isKelly)
					assessmentDetail.setIpaddress("kelly");
				}
				return assessmentDetail;
			}

		}
		List<AssessmentDetail> assessmentDetails = null;
		List<AssessmentJobRelation> assessmentJobRelations = null;

		AssessmentJobRelation assessmentJobRelation = null;
		// if assessment is not taken
		try {//for base
			if(jobOrder!=null && jobOrder.getJobId()!=null && jobOrder.getJobId()==0)
			{
				Criterion criterion = Restrictions.eq("status", "A");
				Criterion criterion1 = Restrictions.eq("assessmentType", 1);
				Criterion criterion2 = Restrictions.eq("isDefault", true);
				Criterion criterion3 = null;
				
				List<AssessmentGroupDetails> assessmentGroupDetailsList = null;
				assessmentGroupDetailsList = assessmentGroupDetailsDAO.findByCriteria(criterion,criterion1,criterion2);
				if(assessmentGroupDetailsList!=null && assessmentGroupDetailsList.size()>0)
					criterion3 = Restrictions.eq("assessmentGroupDetails", assessmentGroupDetailsList.get(0));
				
				AssessmentGroupDetails assessmentGroupDetails = null;
				if(epiJobId!=null && epiJobId>0){
					System.out.println("******************************* 1 ******************************* : epiJobId : "+epiJobId);
					JobOrder epiJobOrder  = jobOrderDAO.findById(epiJobId, false, false);
					if(epiJobOrder!=null){
						System.out.println("******************************* 2 ******************************* : epiJobOrder.getJobCategoryMaster().getJobCategoryId() : "+epiJobOrder.getJobCategoryMaster().getJobCategoryId());
						Integer assessmentGroupId = null;
						assessmentGroupId = epiJobOrder.getJobCategoryMaster().getAssessmentGroupId();
						if(assessmentGroupId!=null && assessmentGroupId>0){
							System.out.println("******************************* 3 ******************************* : assessmentGroupId : "+assessmentGroupId);
							assessmentGroupDetails = assessmentGroupDetailsDAO.findById(assessmentGroupId, false, false);
							if(assessmentGroupDetails!=null)
								assessmentDetails = assessmentDetailDAO.getAssessmentDetailListByGroup(assessmentGroupDetails);
							else
								assessmentDetails = assessmentDetailDAO.findByCriteria(criterion,criterion1,criterion3);
						}
						else
							assessmentDetails = assessmentDetailDAO.findByCriteria(criterion,criterion1,criterion3);
					}
					else
						assessmentDetails = assessmentDetailDAO.findByCriteria(criterion,criterion1,criterion3);
				}
				else
					assessmentDetails = assessmentDetailDAO.findByCriteria(criterion,criterion1,criterion3);
				
				if(assessmentDetails==null)
					assessmentDetails = assessmentDetailDAO.findByCriteria(criterion,criterion1,criterion3);
				
				System.out.println("******************************* 4 ******************************* : assessmentDetails.size() : "+assessmentDetails.size());
				
				List<AssessmentDetail> normalAssessmentDetail = new ArrayList<AssessmentDetail>();
				List<AssessmentDetail> researchAssessmentDetail = new ArrayList<AssessmentDetail>();

				for(AssessmentDetail assessmentDetail2 : assessmentDetails)
				{
					if(assessmentDetail2.getIsResearchEPI())
						researchAssessmentDetail.add(assessmentDetail2);
					else
						normalAssessmentDetail.add(assessmentDetail2);
				}
				Map<Boolean,EpiTakenList> epiTakenMap = new HashMap<Boolean, EpiTakenList>();
				EpiTakenList epiTakenList = null;
				List<EpiTakenList> epiTakenLists = null;
				epiTakenLists = epiTakenListDAO.getEpiTakenList(assessmentGroupDetails);
				if(epiTakenLists!=null && epiTakenLists.size()>0){
					for (EpiTakenList epiTakenList2 : epiTakenLists) {
						if(epiTakenList2.getIsResearchEPI())
							epiTakenMap.put(true, epiTakenList2);
						else
							epiTakenMap.put(false, epiTakenList2);
					}
				}
				int nadSize = normalAssessmentDetail.size();
				int radSize = researchAssessmentDetail.size();
				if(teacherDetail.getIsResearchTeacher()) //Research EPI
				{
					if(radSize==1)
					{
						assessmentDetail = researchAssessmentDetail.get(0);
						return assessmentDetail;
					}else if(radSize>1)
					{
						epiTakenList = epiTakenMap.get(true);
						if(epiTakenList!=null)
						{
							int indexId = 0;
							for (AssessmentDetail assessmentDetail2 : researchAssessmentDetail) {
								if(assessmentDetail2.getAssessmentId().equals(epiTakenList.getAssessmentId()))
									break;

								indexId++;
							}

							if((indexId+1)==radSize)
								assessmentDetail = researchAssessmentDetail.get(0);
							else
								assessmentDetail = researchAssessmentDetail.get(indexId+1);
						}else
							assessmentDetail = researchAssessmentDetail.get(0);

						return assessmentDetail;
					}else
					{
						assessmentDetail.setAssessmentName(null);
						assessmentDetail.setStatus("1");
						return assessmentDetail;
					}

				}else //normal EPI
				{
					if(nadSize==1)
					{
						assessmentDetail = normalAssessmentDetail.get(0);
						if(assessmentDetail.getAssessmentType()==1)
						{
							boolean isKelly = false;
							if(request.getServerName().contains("kelly") || request.getServerName().toLowerCase().contains("smartpractice") || request.getRequestURL().toString().contains("smartpractice")){
								isKelly = true;
							}
							
							boolean isKellyJobApply = false;
							try {
								isKellyJobApply = jobForTeacherDAO.getJFTByTeachers(teacherDetail);
							} catch (Exception e) {
								e.printStackTrace();
							}
							System.out.println("isKellyJobApply : "+isKellyJobApply);
							if(isKellyJobApply || isKelly)
							assessmentDetail.setIpaddress("kelly");
						}
						return assessmentDetail;
					}else if(nadSize>1)
					{
						epiTakenList = epiTakenMap.get(false);
						if(epiTakenList!=null)
						{
							int indexId = 0;
							for (AssessmentDetail assessmentDetail2 : normalAssessmentDetail) {
								if(assessmentDetail2.getAssessmentId().equals(epiTakenList.getAssessmentId()))
									break;

								indexId++;
							}

							if((indexId+1)==nadSize)
								assessmentDetail = normalAssessmentDetail.get(0);
							else
								assessmentDetail = normalAssessmentDetail.get(indexId+1);
							
							/*Map<Integer, AssessmentDetail> nadMap = new HashMap<Integer, AssessmentDetail>();
							for (AssessmentDetail assessmentDetail2 : normalAssessmentDetail) {
								nadMap.put(assessmentDetail2.getAssessmentId(), assessmentDetail2);
							}
							if(nadMap.containsKey(epiTakenList.getAssessmentId())){
								nadMap.remove(epiTakenList.getAssessmentId());
							}
							assessmentDetail = nadMap.get(nadMap.keySet().toArray()[0]);*/
							
						}else
							assessmentDetail = normalAssessmentDetail.get(0);

						if(assessmentDetail.getAssessmentType()==1)
						{
							boolean isKelly = false;
							if(request.getServerName().contains("kelly") || request.getServerName().toLowerCase().contains("smartpractice") || request.getRequestURL().toString().contains("smartpractice")){
								isKelly = true;
							}
							boolean isKellyJobApply = false;
							try {
								isKellyJobApply = jobForTeacherDAO.getJFTByTeachers(teacherDetail);
							} catch (Exception e) {
								e.printStackTrace();
							}
							System.out.println("isKellyJobApply : "+isKellyJobApply);
							if(isKellyJobApply || isKelly)
							assessmentDetail.setIpaddress("kelly");
						}
						return assessmentDetail;
					}else
					{
						assessmentDetail.setAssessmentName(null);
						assessmentDetail.setStatus("1");
						return assessmentDetail;
					}

				}

			}else if(jobOrder!=null && jobOrder.getJobId()!=null && jobOrder.getJobId()>0)
			{
				String jobStatus = jobOrder.getStatus();
				//System.out.println("---------------- jobStatus::::: "+jobStatus);
				// checking job stauts
				/*Date dateWithoutTime = Utility.getDateWithoutTime();
				Date a = jobOrder.getJobStartDate();
				Date b =jobOrder.getJobEndDate();   // assume these are set to something

				System.out.println("=====+++====+++======++====");
				System.out.println(a.compareTo(dateWithoutTime) * dateWithoutTime.compareTo(b) > 0);
				System.out.println("=====+++====+++======++====");*/

				if(jobStatus!=null && !jobStatus.equalsIgnoreCase("A"))
				{
					//job deativated
					assessmentDetail.setAssessmentName(null);
					assessmentDetail.setStatus("6");
					return assessmentDetail;
				}/*else
				{
					//System.out.println("(((((((((((((((((((( "+Utility.checkDateBetweenTwoDate(jobOrder.getJobStartDate(), jobOrder.getJobEndDate()));
					if(!Utility.checkDateBetweenTwoDate(jobOrder.getJobStartDate(), jobOrder.getJobEndDate()))
					{
						//job expired
						assessmentDetail.setAssessmentName(null);
						assessmentDetail.setStatus("7");
						return assessmentDetail;
					}

				}*/
				//assessmentJobRelations=assessmentJobRelationDAO.findRelationByJobOrder(jobOrder);
				assessmentJobRelations=assessmentJobRelationDAO.findRelationByJobOrderForJSI(jobOrder);
				if(assessmentJobRelations.size()!=0)
				{
					assessmentJobRelation = assessmentJobRelations.get(0);
					assessmentDetail = assessmentJobRelation.getAssessmentId();
					if(assessmentDetail.getStatus().equalsIgnoreCase("A"))
					{
						List<AssessmentQuestions> assessmentQuestions = assessmentQuestionsDAO.getAssessmentQuestions(assessmentDetail);
						if(assessmentQuestions!=null && assessmentQuestions.size()>0)
						{
							return assessmentDetail;
						}
						else
						{
							assessmentDetail.getAssessmentDescription();
							assessmentDetail.setAssessmentName(null);
							assessmentDetail.setStatus("2"); // JSI has no questions
							return assessmentDetail;
						}
					}
					else{
						assessmentDetail.setAssessmentName(null);
						assessmentDetail.setStatus("2");// JSI not active
						return assessmentDetail;
					}

				}else
				{
					assessmentDetail.setAssessmentName(null);
					assessmentDetail.setStatus("1");
					return assessmentDetail;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}


	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to load Assessment Questions.
	 */
	@Transactional(readOnly=false)
	public String loadAssessmentQuestions(AssessmentDetail assessmentDetail,JobOrder jobOrder,Integer epiJobId)
	{
		System.out.println(":::::::::::::::::::AssessmentCampaignAjax : loadAssessmentQuestions:::::::::::::::::::");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}

		Integer jId = 0;
		if(jobOrder!=null && jobOrder.getJobId()==0)
			jId = null;

		TeacherDetail teacherDetail = null;
		List<TeacherAssessmentStatus> teacherAssessmentStatusList =null;
		List<TeacherAssessmentStatus> teacherAssessmentStatusList1 =null;
		try{

			String ipAddress = request.getRemoteAddr();

			Calendar cal = Calendar.getInstance();

			cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
			Date sDate=cal.getTime();
			cal.set(cal.get(Calendar.YEAR), 11,31,0,0,0);
			Date eDate=cal.getTime();


			if (session == null || session.getAttribute("teacherDetail") == null) {
				//return "false";
			}else
				teacherDetail=(TeacherDetail)session.getAttribute("teacherDetail");

			Boolean questionRandomization = assessmentDetail.getQuestionRandomization();
			Boolean optionRandomization = assessmentDetail.getOptionRandomization();
			Criterion criterion = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion1 = Restrictions.eq("assessmentDetail", assessmentDetail);
			Criterion criterion2 = Restrictions.between("createdDateTime", sDate, eDate);
			int assessmentTakenCount = 0;

			if(jId==null)//for base
			{
				teacherAssessmentStatusList=teacherAssessmentStatusDAO.findByCriteria(criterion,criterion1,criterion2);
				teacherAssessmentStatusList1=teacherAssessmentStatusDAO.findByCriteria(criterion,criterion1);
				assessmentTakenCount = teacherAssessmentStatusList1.size();
			}
			else
			{
				teacherAssessmentStatusList=teacherAssessmentStatusDAO.findByCriteria(criterion,criterion1);
				assessmentTakenCount = teacherAssessmentStatusList.size();
			}
			
			if(assessmentTakenCount==0)
				assessmentTakenCount=1;
			else if(assessmentTakenCount>0)
				assessmentTakenCount=assessmentTakenCount+1;

			if(teacherAssessmentStatusList.size()==0)
			{
				try {

					/////////////////////////////////////////////////////////////////////////////////
					TeacherAssessmentdetail teacherAssessmentdetail = new TeacherAssessmentdetail();
					teacherAssessmentdetail.setAssessmentDetail(assessmentDetail);
					teacherAssessmentdetail.setAssessmentDescription(assessmentDetail.getAssessmentDescription());
					teacherAssessmentdetail.setAssessmentName(assessmentDetail.getAssessmentName());
					teacherAssessmentdetail.setAssessmentType(assessmentDetail.getAssessmentType());
					teacherAssessmentdetail.setStatus(assessmentDetail.getStatus());
					teacherAssessmentdetail.setAssosiatedJobOrderType(assessmentDetail.getAssessmentType());
					teacherAssessmentdetail.setCreatedDateTime(new Date());
					teacherAssessmentdetail.setDistrictMaster(assessmentDetail.getDistrictMaster());
					teacherAssessmentdetail.setIpaddress(ipAddress);
					teacherAssessmentdetail.setMaxQuesToDisplay(assessmentDetail.getMaxQuesToDisplay());
					teacherAssessmentdetail.setOptionRandomization(assessmentDetail.getOptionRandomization());
					teacherAssessmentdetail.setQuestionRandomization(assessmentDetail.getQuestionRandomization());
					teacherAssessmentdetail.setSchoolMaster(assessmentDetail.getSchoolMaster());
					teacherAssessmentdetail.setTeacherDetail(teacherDetail);
					UserMaster userMaster = userMasterDAO.findById(assessmentDetail.getUserMaster().getUserId(), false, false);
					System.out.println("userMaster : "+userMaster);
					teacherAssessmentdetail.setUserMaster(userMaster);
					//teacherAssessmentdetail.setUserMaster(assessmentDetail.getUserMaster());
					//System.out.println("assessmentDetail.getScoreLookupMaster():: "+assessmentDetail.getScoreLookupMaster());
				
					teacherAssessmentdetail.setScoreLookupMaster(assessmentDetail.getScoreLookupMaster());
					//System.out.println("assessmentDetail.getScoreLookupMaster():: "+assessmentDetail.getScoreLookupMaster());
					teacherAssessmentdetail.setIsDone(false);
					teacherAssessmentdetail.setAssessmentTakenCount(assessmentTakenCount);

					List<TeacherWiseAssessmentTime> teacherWiseAssessmentTimeList = new ArrayList<TeacherWiseAssessmentTime>();
					teacherWiseAssessmentTimeList = teacherWiseAssessmentTimeDAO.findTeacherWiseAssessmentTime(teacherDetail);
					if(teacherWiseAssessmentTimeList.size()>0) // Time reset
					{
						TeacherWiseAssessmentTime teacherWiseAssessmentTime = teacherWiseAssessmentTimeList.get(0);
						if(teacherWiseAssessmentTime.getQuestionSessionTime()!=null)
							teacherAssessmentdetail.setQuestionSessionTime(teacherWiseAssessmentTime.getQuestionSessionTime());
						else
							teacherAssessmentdetail.setQuestionSessionTime(assessmentDetail.getQuestionSessionTime());

						if(teacherWiseAssessmentTime.getAssessmentSessionTime()!=null)
							teacherAssessmentdetail.setAssessmentSessionTime(teacherWiseAssessmentTime.getAssessmentSessionTime());
						else
							teacherAssessmentdetail.setAssessmentSessionTime(assessmentDetail.getAssessmentSessionTime());

					}else
					{
						teacherAssessmentdetail.setQuestionSessionTime(assessmentDetail.getQuestionSessionTime());
						teacherAssessmentdetail.setAssessmentSessionTime(assessmentDetail.getAssessmentSessionTime());
					}

					teacherAssessmentDetailDAO.makePersistent(teacherAssessmentdetail);

					//System.out.println("hhhhhhhhhhhhhhhhhhhhhhhhhh "+assessmentDetail.getAssessmentId());
					////////////// section 
					List<AssessmentSections> assessmentSectionsList = null;
					if(questionRandomization!=null && questionRandomization==true)
						assessmentSectionsList = assessmentSectionDAO.getRNDSectionsByAssessmentId(assessmentDetail);
					else
						assessmentSectionsList = assessmentSectionDAO.getSectionsByAssessmentId(assessmentDetail);
					int noOfExperimentQuestions = 0;

					if(assessmentSectionsList.size()>0)
					{
						noOfExperimentQuestions = assessmentSectionsList.get(0).getAssessmentDetail().getNoOfExperimentQuestions()==null?0:assessmentSectionsList.get(0).getAssessmentDetail().getNoOfExperimentQuestions();
					}
					//System.out.println("assessmentSectionsList.size() : "+assessmentSectionsList.size());
					//Collections.shuffle(assessmentSectionsList);
					
					boolean isKelly = false;
					if(request.getServerName().contains("kelly") || request.getServerName().toLowerCase().contains("smartpractice") || request.getRequestURL().toString().contains("smartpractice")){
						isKelly = true;
					}
					//Check for teacher apply any job of Kelly or not
					boolean isKellyJobApply = false;
					isKellyJobApply = jobForTeacherDAO.getJFTByTeachers(teacherDetail);
					System.out.println("isKellyJobApply : "+isKellyJobApply);
					if(isKellyJobApply || isKelly)
						noOfExperimentQuestions=0;

					Map<Integer,List<AssessmentQuestions>> sMap = new HashMap<Integer, List<AssessmentQuestions>>();

					if(assessmentDetail.getAssessmentType()==1 && noOfExperimentQuestions>0)
					{
						List<AssessmentQuestions> assessmentQuestions = new ArrayList<AssessmentQuestions>();
						/*List<AssessmentQuestions> assessmentQuestionsStage2 = null;
						assessmentQuestionsStage2 = assessmentQuestionsDAO.getSectionQuestionsWithoutWeightageExp(assessmentSectionsList,noOfExperimentQuestions);
						int Stage2Size =  assessmentQuestionsStage2.size();

						System.out.println("Stage2Size:: "+Stage2Size);

						if(Stage2Size<noOfExperimentQuestions)
							assessmentQuestions = assessmentQuestionsDAO.getSectionsQuestionListWithoutWeightage(assessmentSectionsList,noOfExperimentQuestions-Stage2Size);

						System.out.println("assessmentQuestions1:: "+assessmentQuestions.size());

						assessmentQuestions.addAll(assessmentQuestionsStage2);*/

						assessmentQuestions = assessmentQuestionsDAO.getSectionsQuestionListWithoutWeightage(assessmentSectionsList,noOfExperimentQuestions);
						
						System.out.println("assessmentQuestions : ExperimentQuestions : "+assessmentQuestions.size());

						for (AssessmentQuestions assessmentQuestions2 : assessmentQuestions) {
							Integer secId = assessmentQuestions2.getAssessmentSections().getSectionId();
							List<AssessmentQuestions> aqList = sMap.get(secId);
							if(aqList==null)
							{
								aqList = new ArrayList<AssessmentQuestions>();
								aqList.add(assessmentQuestions2);
							}else
								aqList.add(assessmentQuestions2);
							sMap.put(secId, aqList);
						}
					}
					if(assessmentSectionsList.size()!=0)
					{
						List<Integer> questionPools = new ArrayList<Integer>();
						for (AssessmentSections assessmentSection : assessmentSectionsList) 
						{
							//System.out.println("sectionName: "+assessmentSection.getSectionName());
							TeacherSectionDetail teacherSectionDetail = new TeacherSectionDetail(); 

							teacherSectionDetail.setTeacherAssessmentdetail(teacherAssessmentdetail);
							teacherSectionDetail.setCreatedDateTime(new Date());
							teacherSectionDetail.setIpAddress(ipAddress);
							teacherSectionDetail.setSectionDescription(assessmentSection.getSectionDescription());
							teacherSectionDetail.setSectionInstructions(assessmentSection.getSectionInstructions());
							teacherSectionDetail.setSectionName(assessmentSection.getSectionName());
							teacherSectionDetail.setSectionVideoUrl(assessmentSection.getSectionVideoUrl());
							teacherSectionDetail.setStatus(assessmentSection.getStatus());
							teacherSectionDetail.setUserMaster(assessmentSection.getUserMaster());
							teacherSectionDetail.setTeacherDetail(teacherDetail);

							teacherSectionDetailDAO.makePersistent(teacherSectionDetail);

							List<AssessmentQuestions> assessmentQuestions = new ArrayList<AssessmentQuestions>();
							if(questionRandomization!=null && questionRandomization==true)
								assessmentQuestions = assessmentQuestionsDAO.getRNDSectionQuestions(assessmentSection);
							else
								assessmentQuestions = assessmentQuestionsDAO.getSectionQuestionsWithWeightage(assessmentSection);
							//assessmentQuestions = assessmentQuestionsDAO.getSectionQuestions(assessmentSection);

							//	System.out.println("assessmentQuestions.size() : "+assessmentQuestions.size());
							if(assessmentDetail.getAssessmentType()==1)
							{
								List<AssessmentQuestions> assessmentQuestionsExp = sMap.get(assessmentSection.getSectionId());
								if(assessmentQuestionsExp!=null)
									assessmentQuestions.addAll(assessmentQuestionsExp);

								if(questionRandomization!=null && questionRandomization==true)
									Collections.shuffle(assessmentQuestions);
							}

							List<QuestionOptions> questionOptions = null;
							if(assessmentQuestions.size()!=0)
							{
								QuestionsPool questionsPool = null;

								for (AssessmentQuestions assessmentQuestion : assessmentQuestions) 
								{
									questionsPool=assessmentQuestion.getQuestionsPool();

									questionPools.add(questionsPool.getQuestionId());

									TeacherAssessmentQuestion teacherAssessmentQuestion = new TeacherAssessmentQuestion();
									teacherAssessmentQuestion.setTeacherAssessmentdetail(teacherAssessmentdetail);
									teacherAssessmentQuestion.setTeacherSectionDetail(teacherSectionDetail);

									teacherAssessmentQuestion.setCompetencyMaster(questionsPool.getCompetencyMaster());
									teacherAssessmentQuestion.setCreatedDateTime(new Date());
									teacherAssessmentQuestion.setDomainMaster(questionsPool.getDomainMaster());
									teacherAssessmentQuestion.setFieldLength(questionsPool.getFieldLength());
									teacherAssessmentQuestion.setIsMandatory(assessmentQuestion.getIsMandatory());
									teacherAssessmentQuestion.setObjectiveMaster(questionsPool.getObjectiveMaster());
									//System.out.println(questionsPool.getQuestionId()+" questionsPool.getQuestionUId(): "+questionsPool.getQuestionUId());
									teacherAssessmentQuestion.setQuestionUId(questionsPool.getQuestionUId());
									teacherAssessmentQuestion.setQuestion(questionsPool.getQuestion());
									teacherAssessmentQuestion.setQuestionInstruction(questionsPool.getQuestionInstruction());
									teacherAssessmentQuestion.setQuestionPosition(assessmentQuestion.getQuestionPosition());
									teacherAssessmentQuestion.setQuestionTypeMaster(questionsPool.getQuestionTypeMaster());
									//teacherAssessmentQuestion.setQuestionWeightage(questionsPool.getQuestionWeightage());
									teacherAssessmentQuestion.setQuestionWeightage(assessmentQuestion.getQuestionWeightage());
									teacherAssessmentQuestion.setMaxMarks(questionsPool.getMaxMarks());
									teacherAssessmentQuestion.setTeacherDetail(teacherDetail);
									teacherAssessmentQuestion.setValidationType(questionsPool.getValidationType());
									teacherAssessmentQuestion.setIsAttempted(false);
									teacherAssessmentQuestionDAO.makePersistent(teacherAssessmentQuestion);
									//System.out.println(assessmentQuestion.getQuestionsPool().getQuestion());
									QuestionTypeMaster questionTypeMaster= questionsPool.getQuestionTypeMaster();
									// Likert Scale check
									questionOptions=questionsPool.getQuestionOptions();
									if(optionRandomization!=null && optionRandomization==true && !questionTypeMaster.getQuestionTypeShortName().equalsIgnoreCase("lkts"))
										Collections.shuffle(questionOptions);

									if(questionOptions.size()!=0)
									{
										for (QuestionOptions questionOption : questionOptions) {
											//System.out.println("LL "+questionOption.getQuestionOption());
											TeacherAssessmentOption teacherAssessmentOption = new TeacherAssessmentOption();

											teacherAssessmentOption.setTeacherDetail(teacherDetail);
											teacherAssessmentOption.setTeacherAssessmentQuestion(teacherAssessmentQuestion);
											teacherAssessmentOption.setQuestionOption(questionOption.getQuestionOption());
											teacherAssessmentOption.setQuestionOptionId(questionOption.getOptionId());
											teacherAssessmentOption.setQuestionOptionTag(questionOption.getQuestionOptionTag());
											teacherAssessmentOption.setRank(questionOption.getRank());
											teacherAssessmentOption.setScore(questionOption.getScore());
											teacherAssessmentOption.setCreatedDateTime(new Date());
											teacherAssessmentOptionDAO.makePersistent(teacherAssessmentOption);
										}
									}

								}

							}

						}

						/// updating questionsPool
						/*try {
							questionsPoolDAO.updateQuestionsCount(questionPools);
						} catch (Exception e) {
							e.printStackTrace();
						}*/
					}
					////////////////////// 

					// teacherassessmentattempt
					TeacherAssessmentAttempt teacherAssessmentAttempt = new TeacherAssessmentAttempt();
					teacherAssessmentAttempt.setAssessmentDetail(assessmentDetail);
					teacherAssessmentAttempt.setIpAddress(ipAddress);
					if(jobOrder!=null && jobOrder.getJobId()!=null && jobOrder.getJobId()==0)
						teacherAssessmentAttempt.setJobOrder(null);
					else
						teacherAssessmentAttempt.setJobOrder(jobOrder);

					teacherAssessmentAttempt.setTeacherDetail(teacherDetail);
					teacherAssessmentAttempt.setAssessmentSessionTime(0);
					teacherAssessmentAttempt.setAssessmentStartTime(new Date());
					teacherAssessmentAttempt.setIsForced(false);
					teacherAssessmentAttempt.setAssessmentTakenCount(assessmentTakenCount);
					teacherAssessmentAttempt.setTeacherAssessmentdetail(teacherAssessmentdetail);
					teacherAssessmentAttemptDAO.makePersistent(teacherAssessmentAttempt);

					/////// TeacherAssessmentStatus
					TeacherAssessmentStatus teacherAssessmentStatus = new TeacherAssessmentStatus();
					teacherAssessmentStatus.setAssessmentDetail(assessmentDetail);
					teacherAssessmentStatus.setTeacherDetail(teacherDetail);
					teacherAssessmentStatus.setTeacherAssessmentdetail(teacherAssessmentdetail);
					teacherAssessmentStatus.setAssessmentType(assessmentDetail.getAssessmentType());
					teacherAssessmentStatus.setCreatedDateTime(new Date());
					StatusMaster statusMaster=WorkThreadServlet.statusMap.get("icomp");
					teacherAssessmentStatus.setStatusMaster(statusMaster);
					teacherAssessmentStatus.setAssessmentTakenCount(assessmentTakenCount);

					if(jobOrder!=null && jobOrder.getJobId()!=null && jobOrder.getJobId()==0)
					{
						teacherAssessmentStatus.setJobOrder(null);
						teacherAssessmentStatusDAO.makePersistent(teacherAssessmentStatus);
					}
					else
					{
						teacherAssessmentStatus = new TeacherAssessmentStatus();
						teacherAssessmentStatus.setAssessmentDetail(assessmentDetail);
						teacherAssessmentStatus.setTeacherDetail(teacherDetail);
						teacherAssessmentStatus.setTeacherAssessmentdetail(teacherAssessmentdetail);
						teacherAssessmentStatus.setAssessmentType(assessmentDetail.getAssessmentType());
						teacherAssessmentStatus.setCreatedDateTime(new Date());
						teacherAssessmentStatus.setJobOrder(jobOrder);
						teacherAssessmentStatus.setStatusMaster(statusMaster);
						teacherAssessmentStatus.setAssessmentTakenCount(assessmentTakenCount);
						teacherAssessmentStatusDAO.makePersistent(teacherAssessmentStatus);

					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				//EPI taken Updation 
				if(jobOrder.getJobId()==0)
				{
					Map<Boolean,EpiTakenList> epiTakenMap = new HashMap<Boolean, EpiTakenList>();
					EpiTakenList epiTakenList = null;
					List<EpiTakenList> epiTakenLists = null;
					epiTakenLists = epiTakenListDAO.getEpiTakenList(assessmentDetail.getAssessmentGroupDetails());
					if(epiTakenLists!=null && epiTakenLists.size()>0){
						for (EpiTakenList epiTakenList2 : epiTakenLists) {
							if(epiTakenList2.getIsResearchEPI())
								epiTakenMap.put(true, epiTakenList2);
							else
								epiTakenMap.put(false, epiTakenList2);
						}
					}

					epiTakenList  = epiTakenMap.get(assessmentDetail.getIsResearchEPI());
					if(epiTakenList==null)
					{
						epiTakenList = new EpiTakenList();
						epiTakenList.setAssessmentId(assessmentDetail.getAssessmentId());
						epiTakenList.setIsResearchEPI(assessmentDetail.getIsResearchEPI());
						epiTakenList.setAssessmentGroupDetails(assessmentDetail.getAssessmentGroupDetails());
					}else
						epiTakenList.setAssessmentId(assessmentDetail.getAssessmentId());

					epiTakenListDAO.makePersistent(epiTakenList);
				}
				//send mail to candidate on start of EPI and JSI according to district settings for EPI and JSI.
				sendMailToCandidate(assessmentDetail.getAssessmentType(),"strt",jobOrder.getJobId(),epiJobId);
			}else
			{
				return teacherAssessmentStatusList.get(0).getStatusMaster().getStatusShortName();
			}


		}catch (Exception e) {
			e.printStackTrace();
		}
		return "1";
	}
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to get Assessment Section Questions.
	 */
	@Transactional(readOnly=false)
	public String getAssessmentSectionCampaignQuestions(AssessmentDetail assessmentDetail,TeacherAssessmentdetail teacherAssessmentdetail,TeacherAssessmentQuestion teacherAssessmentQues,TeacherSectionDetail teacherSectionDetail,TeacherAnswerDetail[] teacherAnswerDetails,int attemptId,int newJobId,Integer epiJobId)
	{
		//System.out.println("::::::::::::::::getAssessmentSectionCampaignQuestions::::::::::::::::::");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		int assessmentType=1;
		Boolean epiStandalone = session.getAttribute("epiStandalone")==null?false:(Boolean)session.getAttribute("epiStandalone");
		StringBuffer sb =new StringBuffer();
		JobOrder jobOrderForUpdate=null;
		try{

			try{
				if(newJobId>0)
					jobOrderForUpdate = jobOrderDAO.findById(newJobId, false, false);
			}catch(Exception e){
				e.printStackTrace();
			}

			TeacherDetail teacherDetail = null;
			if (session == null || session.getAttribute("teacherDetail") == null) {
				//return "false";
			}else
				teacherDetail=(TeacherDetail)session.getAttribute("teacherDetail");
			try{
				System.out.print("Assesment:"+new Date()+", T:"+teacherDetail.getTeacherId() +", E:"+teacherDetail.getEmailAddress());
			}catch(Exception e){
				e.printStackTrace();
			}

			if(attemptId!=0)
			{
				teacherAssessmentAttemptDAO.updateAssessmentAttemptSessionTime(attemptId,false);
			}

			if(teacherAssessmentQues!=null && teacherAssessmentQues.getTeacherAssessmentQuestionId()!=null)
			{

				TeacherAnswerDetail teacherAnswerDetail = teacherAnswerDetailDAO.findTADetailbyQuestion(teacherAssessmentQues);
                if(teacherAnswerDetails!=null && teacherAnswerDetails.length>0)
                {
                    if(teacherAnswerDetail==null)
                        teacherAnswerDetail = teacherAnswerDetails[0];
					if(teacherAnswerDetail.getJobOrder().getJobId()==null)
						teacherAnswerDetail.setJobOrder(null);
					
					teacherAnswerDetail.setAssessmentDetail(assessmentDetail);
					teacherAnswerDetail.setTeacherAssessmentdetail(teacherAssessmentdetail);
					teacherAnswerDetail.setTeacherAssessmentQuestion(teacherAssessmentQues);
					teacherAnswerDetail.setCreatedDateTime(new Date());
					teacherAnswerDetail.setTeacherDetail(teacherDetail);
					teacherAnswerDetailDAO.makePersistent(teacherAnswerDetail);
				}
				/////////////////////////////////////////////////////////////////////////////////////////////
				teacherAssessmentQues=teacherAssessmentQuestionDAO.findById(teacherAssessmentQues.getTeacherAssessmentQuestionId(), false, false);
				teacherAssessmentQues.setIsAttempted(true);
				teacherAssessmentQuestionDAO.makePersistent(teacherAssessmentQues);
			}
			int teacherSectionId = 0;
			if(teacherSectionDetail!=null && teacherSectionDetail.getTeacherSectionId()!=null)
			{
				teacherSectionId = teacherSectionDetail.getTeacherSectionId();
			}
			List<TeacherAssessmentQuestion> teacherAssessmentQuestions =null;
			Criterion criterion = Restrictions.eq("teacherAssessmentdetail", teacherAssessmentdetail);
			Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion2 = Restrictions.eq("isAttempted", false);
			Criterion criterion3 = Restrictions.eq("isAttempted", true);
			int totalQuestions = teacherAssessmentQuestionDAO.getRowCount(criterion,criterion1);
			int totalQuestionsAttempted = teacherAssessmentQuestionDAO.getRowCount(criterion,criterion1,criterion3);
			teacherAssessmentQuestions = teacherAssessmentQuestionDAO.findWithLimit(null,0,1,criterion,criterion1,criterion2);
			int i=0;
			List<TeacherAssessmentOption> teacherQuestionOptionsList = null;
			String questionInstruction = null;
			int teacherSectionIDD = 0;
			String shortName = "";
			String sectionInstructions="";
			for (TeacherAssessmentQuestion teacherAssessmentQuestion : teacherAssessmentQuestions) 
			{
				teacherAssessmentdetail = teacherAssessmentQuestion.getTeacherAssessmentdetail();

				teacherSectionIDD=teacherAssessmentQuestion.getTeacherSectionDetail().getTeacherSectionId();
				if(teacherSectionId==0 || (teacherSectionIDD!=teacherSectionId))
				{
					sb.append("<tr>");
					sb.append("<td width='90%'>");
					sb.append("<h3>Section: "+teacherAssessmentQuestion.getTeacherSectionDetail().getSectionName()+"</h3><br>");
					sb.append("<b>"+Utility.getLocaleValuePropByKey("epiInstructionssection", locale)+"</b><br><br>");

					sectionInstructions = teacherAssessmentQuestion.getTeacherSectionDetail().getSectionInstructions();

					sb.append(Utility.getUTFToHTML(sectionInstructions)+"<br>");
					String sectionVideoUrl = teacherAssessmentQuestion.getTeacherSectionDetail().getSectionVideoUrl();

					if(sectionVideoUrl!=null && !sectionVideoUrl.equals(""))
						sb.append("<div align='center'>"+Utility.getYouTubeCode(sectionVideoUrl,450,350)+"</div><br><br>");

					if(teacherSectionId!=0)
						sb.append("<input type='hidden' id='teacherAssessmentQuestionId' value=''/>" );
					sb.append("<input type='hidden' id='teacherSectionId' value='"+teacherSectionIDD+"'/>" );
					sb.append("<script type=\"text/javascript\" language=\"javascript\">");
					sb.append("document.getElementById('sectionName').innerHTML='<b>Section: "+teacherAssessmentQuestion.getTeacherSectionDetail().getSectionName()+"</b>';");
					sb.append("document.getElementById('sectionName').style.display='none';");
					sb.append("document.getElementById('questionTimer').style.display='none';");
					sb.append("</script>");
					sb.append("</td>");
					sb.append("</tr>");

					return sb.toString();

				}
				sb.append("<tr>");
				sb.append("<td width='90%'>");

			sb.append("<div style='text-align:right;'>"+Utility.genrateHTMLspace(198)+"<b>"+Utility.getLocaleValuePropByKey("headQues", locale)+" "+(++totalQuestionsAttempted)+" "+Utility.getLocaleValuePropByKey("epiof", locale)+ " "+totalQuestions+"</b></div>");
				try{
					assessmentType=teacherAssessmentdetail.getAssessmentType();
					System.out.println(", Type:"+teacherAssessmentdetail.getAssessmentType()+", Q:"+(totalQuestionsAttempted)+" of "+totalQuestions);
				}catch(Exception e){}
				questionInstruction = teacherAssessmentQuestion.getQuestionInstruction()==null?"":teacherAssessmentQuestion.getQuestionInstruction();
				if(!questionInstruction.equals(""))
					sb.append(""+Utility.getUTFToHTML(questionInstruction)+"<br/>");

				sb.append(""+Utility.getUTFToHTML(teacherAssessmentQuestion.getQuestion())+"<br/>");
				sb.append("<input type='hidden' name='o_maxMarks' value='"+(teacherAssessmentQuestion.getMaxMarks()==null?0:teacherAssessmentQuestion.getMaxMarks())+"'  />");

				teacherQuestionOptionsList = teacherAssessmentQuestion.getTeacherAssessmentOptions();
				shortName=teacherAssessmentQuestion.getQuestionTypeMaster().getQuestionTypeShortName();

				if(shortName.equalsIgnoreCase("tf") || shortName.equalsIgnoreCase("slsel") || shortName.equalsIgnoreCase("lkts"))
				{
					sb.append("<table >");
					for (TeacherAssessmentOption teacherQuestionOption : teacherQuestionOptionsList) {
						sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'><input type='radio' name='opt' value='"+teacherQuestionOption.getTeacherAssessmentOptionId()+"' /></td><td style='border:0px;background-color: transparent;padding: 5px 3px;' >"+teacherQuestionOption.getQuestionOption()+" ");
						sb.append("<input type='hidden' name='score' value='"+(teacherQuestionOption.getScore()==null?0:teacherQuestionOption.getScore())+"'/>");
						sb.append("<input type='hidden' name='questionOptionId' value='"+(teacherQuestionOption.getQuestionOptionId()==null?null:teacherQuestionOption.getQuestionOptionId())+"'/>");
						sb.append("<input type='hidden' name='questionOptionTag' value='"+(teacherQuestionOption.getQuestionOptionTag()==null?null:teacherQuestionOption.getQuestionOptionTag())+"'/></td></tr>");
					}
					sb.append("</table>");
				}
				if(shortName.equalsIgnoreCase("it"))
				{
					sb.append("<table >");
					for (TeacherAssessmentOption teacherQuestionOption : teacherQuestionOptionsList) {
						sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;vertical-align:middle;'><input type='radio' name='opt' value='"+teacherQuestionOption.getTeacherAssessmentOptionId()+"' /></td><td style='border:0px;background-color: transparent;vertical-align:middle;padding-top:5px;'><img src='showImage?image=ques_images/"+teacherQuestionOption.getQuestionOption()+"'> ");
						sb.append("<input type='hidden' name='score' value='"+(teacherQuestionOption.getScore()==null?0:teacherQuestionOption.getScore())+"'/>");
						sb.append("<input type='hidden' name='questionOptionId' value='"+(teacherQuestionOption.getQuestionOptionId()==null?null:teacherQuestionOption.getQuestionOptionId())+"'/>");
						sb.append("<input type='hidden' name='questionOptionTag' value='"+(teacherQuestionOption.getQuestionOptionTag()==null?null:teacherQuestionOption.getQuestionOptionTag())+"'/></td></tr>");
					}
					sb.append("</table>");
				}
				else if(shortName.equalsIgnoreCase("rt"))
				{
					int rank=1;
					sb.append("<table>");
					for (TeacherAssessmentOption teacherQuestionOption : teacherQuestionOptionsList) {
						sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'><input type='text' id='rnk"+rank+"' name='rank' class='span1' maxlength='1' onkeypress='return checkForIntUpto6(event)' onblur='checkUniqueRank(this);'/></td><td style='border:0px;background-color: transparent;padding: 6px 3px;'>"+teacherQuestionOption.getQuestionOption()+"");
						sb.append("<input type='hidden' name='score' value='"+(teacherQuestionOption.getScore()==null?0:teacherQuestionOption.getScore())+"' /><input type='hidden' name='opt' value='"+teacherQuestionOption.getTeacherAssessmentOptionId()+"' />");
						sb.append("<input type='hidden' name='o_rank' value='"+(teacherQuestionOption.getRank()==null?0:teacherQuestionOption.getRank())+"' />");
						sb.append("<input type='hidden' name='questionOptionId' value='"+(teacherQuestionOption.getQuestionOptionId()==null?null:teacherQuestionOption.getQuestionOptionId())+"'/>");
						sb.append("<input type='hidden' name='questionOptionTag' value='"+(teacherQuestionOption.getQuestionOptionTag()==null?null:teacherQuestionOption.getQuestionOptionTag())+"'/>");
						sb.append("<script type=\"text/javascript\" language=\"javascript\">");
						sb.append("document.getElementById('rnk1').focus();");
						sb.append("</script></td></tr>");
						rank++;
					}
					sb.append("</table >");
				}else if(shortName.equalsIgnoreCase("sl"))
				{
					sb.append("&nbsp;&nbsp;<input type='text' name='opt' id='opt' class='span15' maxlength='75'/><br/>");
					//sb.append(Utility.genrateHTMLspace(190)+"Max Length: 75 Characters<br/>");
					sb.append("<script type=\"text/javascript\" language=\"javascript\">");
					sb.append("document.getElementById('opt').focus();");
					sb.append("</script>");
				}
				else if(shortName.equalsIgnoreCase("ml"))
				{
					sb.append("&nbsp;&nbsp;<textarea name='opt' id='opt' class='form-control' maxlength='5000' rows='15' style='width:98%;margin:5px;' /></textarea><br/>");
					//sb.append(Utility.genrateHTMLspace(187)+"Max Length: 5000 Characters<br/>");
					sb.append("<script type=\"text/javascript\" language=\"javascript\">");
					sb.append("document.getElementById('opt').focus();");
					sb.append("</script>");
				}
				else if(shortName.equalsIgnoreCase("mlsel"))
				{
					sb.append("<table>");
					for (TeacherAssessmentOption teacherQuestionOption : teacherQuestionOptionsList) {
						sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'><input type='checkbox' name='opt' value='"+teacherQuestionOption.getTeacherAssessmentOptionId()+"' /></td><td style='border:0px;background-color: transparent;padding: 5px 3px;' >"+teacherQuestionOption.getQuestionOption()+" ");
						sb.append("<input type='hidden' name='score' value='"+(teacherQuestionOption.getScore()==null?0:teacherQuestionOption.getScore())+"'/>");
						sb.append("<input type='hidden' name='questionOptionId' value='"+(teacherQuestionOption.getQuestionOptionId()==null?null:teacherQuestionOption.getQuestionOptionId())+"'/>");
						sb.append("<input type='hidden' name='questionOptionTag' value='"+(teacherQuestionOption.getQuestionOptionTag()==null?null:teacherQuestionOption.getQuestionOptionTag())+"'/></td></tr>");
					}
					sb.append("</table>");
				}
				else if(shortName.equalsIgnoreCase("mloet"))
				{
					sb.append("<table>");
					for (TeacherAssessmentOption teacherQuestionOption : teacherQuestionOptionsList) {
						sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'><input type='checkbox' name='opt' value='"+teacherQuestionOption.getTeacherAssessmentOptionId()+"' /></td><td style='border:0px;background-color: transparent;padding: 5px 3px;' >"+teacherQuestionOption.getQuestionOption()+" ");
						sb.append("<input type='hidden' name='score' value='"+(teacherQuestionOption.getScore()==null?0:teacherQuestionOption.getScore())+"'/>");
						sb.append("<input type='hidden' name='questionOptionId' value='"+(teacherQuestionOption.getQuestionOptionId()==null?null:teacherQuestionOption.getQuestionOptionId())+"'/>");
						sb.append("<input type='hidden' name='questionOptionTag' value='"+(teacherQuestionOption.getQuestionOptionTag()==null?null:teacherQuestionOption.getQuestionOptionTag())+"'/></td></tr>");
					}
					sb.append("</table>");
					
					sb.append("<table>");
					sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'><textarea name='insertedText' id='insertedText' class='form-control' maxlength='5000' rows='15' cols='100' style='width:98%;margin:5px;' /></textarea></td></tr>");
					sb.append("</table>");
					
					
				}
				else if(shortName.equalsIgnoreCase("sloet"))
				{
					sb.append("<table>");
					for (TeacherAssessmentOption teacherQuestionOption : teacherQuestionOptionsList) {
						sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'><input type='radio' name='opt' value='"+teacherQuestionOption.getTeacherAssessmentOptionId()+"' /></td><td style='border:0px;background-color: transparent;padding: 5px 3px;' >"+teacherQuestionOption.getQuestionOption()+" ");
						sb.append("<input type='hidden' name='score' value='"+(teacherQuestionOption.getScore()==null?0:teacherQuestionOption.getScore())+"'/>");
						sb.append("<input type='hidden' name='questionOptionId' value='"+(teacherQuestionOption.getQuestionOptionId()==null?null:teacherQuestionOption.getQuestionOptionId())+"'/>");
						sb.append("<input type='hidden' name='questionOptionTag' value='"+(teacherQuestionOption.getQuestionOptionTag()==null?null:teacherQuestionOption.getQuestionOptionTag())+"'/></td></tr>");
					}
					sb.append("</table>");
					if(shortName.equalsIgnoreCase("sloet"))
					{
						sb.append("<table >");
						sb.append("<tr><td style='border:0px;background-color: transparent;padding: 2px 2px;'><textarea name='insertedText' id='insertedText' class='form-control' maxlength='5000' rows='15' cols='100' style='width:98%;margin:5px;' /></textarea></td></tr>");
						sb.append("</table>");
					}
					
				}
				sb.append("<input type='hidden' id='teacherAssessmentQuestionId' value='"+teacherAssessmentQuestion.getTeacherAssessmentQuestionId()+"'/>" );
				sb.append("<input type='hidden' id='teacherSectionId' value='"+teacherSectionIDD+"'/>" );
				sb.append("<input type='hidden' id='questionWeightage' value='"+(teacherAssessmentQuestion.getQuestionWeightage()==null?0:teacherAssessmentQuestion.getQuestionWeightage())+"'/>" );
				sb.append("<input type='hidden' id='questionTypeId' value='"+teacherAssessmentQuestion.getQuestionTypeMaster().getQuestionTypeId()+"'/>" );
				sb.append("<input type='hidden' id='questionTypeShortName' value='"+teacherAssessmentQuestion.getQuestionTypeMaster().getQuestionTypeShortName()+"'/>" );

				if(teacherAssessmentdetail.getQuestionSessionTime()!=null && teacherAssessmentdetail.getQuestionSessionTime()!=0)
				{
					sb.append("<script type=\"text/javascript\" language=\"javascript\">tmTimer("+teacherAssessmentdetail.getQuestionSessionTime()+",3);" );
					//sb.append("document.getElementById('timer').style.display='block';");
					sb.append("</script>");
				}
				sb.append("<input type='hidden' id='questionSessionTime' value='"+(teacherAssessmentdetail.getQuestionSessionTime()==null?0:teacherAssessmentdetail.getQuestionSessionTime())+"'/>" );
				sb.append("</td>");
				sb.append("</tr>");

			}
			if(teacherAssessmentQuestions.size()==0 && teacherAssessmentQues!=null)
			{
				sb.append("<tr id='tr1'><td id='tr1'>");
				if(totalQuestions!=0)
				{
					JobOrder jobOrder = new JobOrder();

					// TeacherAssessmentStatus update after completion
					List<TeacherAssessmentStatus> lstTeacherAssessmentStatus = null;
					int jId=0;
					//System.out.println("teacherId: "+teacherDetail.getTeacherId());
					//System.out.println("teacherAssessmentId: "+teacherAssessmentdetail.getTeacherAssessmentId());
					lstTeacherAssessmentStatus =teacherAssessmentStatusDAO.findAssessmentStatusByTeacherAssessmentdetail(teacherAssessmentdetail);

					TeacherAssessmentStatus teacherAssessmentStatus=lstTeacherAssessmentStatus.get(0);
					
					if(lstTeacherAssessmentStatus.size()!=0)
						if(teacherAssessmentStatus!=null)
						{
							StatusMaster statusMasterIcomp = WorkThreadServlet.statusMap.get("icomp");
							
							teacherAssessmentdetail = teacherAssessmentStatus.getTeacherAssessmentdetail();

							StatusMaster statusMaster = WorkThreadServlet.statusMap.get("comp");
							List<JobOrder> jobs = new ArrayList<JobOrder>();
							for(TeacherAssessmentStatus tas: lstTeacherAssessmentStatus)
							{
								jobs.add(tas.getJobOrder());

								tas.setStatusMaster(statusMaster);
								
								if(statusMaster.getStatusShortName().equalsIgnoreCase("comp"))
									tas.setAssessmentCompletedDateTime(new Date());
								
								teacherAssessmentStatusDAO.makePersistent(tas);
							}

							//						System.out.println("done.......................");
							boolean onDashboard = false;
							//sb.append("Assessment completed");
							List<JobForTeacher> lstJobForTeacherIcomp = new ArrayList<JobForTeacher>();
							List<JobOrder> lstJBOrder = new ArrayList<JobOrder>();
							sb.append("<script type=\"text/javascript\" language=\"javascript\">");
							sb.append("window.onbeforeunload = function() {};");
							if(teacherAssessmentdetail.getAssessmentType()==1)
							{
								///////////////////////// candidate data coping ///////////////////
								String status = teacherAssessmentStatus.getStatusMaster().getStatusShortName();
								if(status.equalsIgnoreCase("comp"))
								{
									CadidateRawScoreThread crt = new CadidateRawScoreThread();
									crt.setCandidateRawScoreDAO(candidateRawScoreDAO);
									crt.setTeacherAnswerDetailDAO(teacherAnswerDetailDAO);
									crt.setTeacherAssessmentQuestionDAO(teacherAssessmentQuestionDAO);
									crt.setTeacherDetail(teacherDetail);
									crt.setReportService(reportService);
									crt.setTeacherAssessmentdetail(teacherAssessmentdetail);
									crt.setTeacherAssessmentStatus(teacherAssessmentStatus);
									crt.start();

									//send mail to candidate on completion of EPI according to district settings for EPI.
									sendMailToCandidate(teacherAssessmentdetail.getAssessmentType(),"comp",jobOrder.getJobId(),epiJobId);
									
									DistrictMaster districtMaster = null;
									if(epiJobId!=null && epiJobId>0){
										JobOrder epiJobOrder  = jobOrderDAO.findById(epiJobId, false, false);
										if(epiJobOrder!=null){
											if(epiJobOrder.getDistrictMaster()!=null){
												districtMaster = epiJobOrder.getDistrictMaster();
											}
											//Send Mail to all DAs after EPI completion
											try{
												if(districtMaster!=null){
													if(districtMaster.getDistrictId()==7800292){
														sendMailToAllDAAfterEpiComplition(userMasterDAO,districtMaster,teacherDetail,epiJobOrder,"EPI");
													}
												}
											}catch(Exception e){e.printStackTrace();};
										}
									}
								}
								///////////////////////////////////////////////////////////////////

								///////////////////////// for external user /////////////////////////
								List<JobForTeacher> lstJobForTeacher = null;
								JobForTeacher jobForTeacherObj=null;

								List<StatusMaster> statusList =  Utility.getStaticMasters(new String[]{"icomp","vlt"});
								Map<Integer,Boolean> qqRequiredMap = new HashMap<Integer, Boolean>();
								//Map<Integer,Boolean> qqTakenMap = new HashMap<Integer, Boolean>();
								//for completed 
								lstJobForTeacher = jobForTeacherDAO.findJobByTeacherAndSatusDuringCurrentYear(teacherDetail,statusList);
								List<DistrictMaster> districts = new ArrayList<DistrictMaster>();
								for(JobForTeacher jft: lstJobForTeacher){
									districts.add(jft.getJobId().getDistrictMaster());
								}
								if(districts.size()>0)
								{
									districts = districtSpecificQuestionsDAO.getDistrictSpecificQuestionBYDistricts(districts);
									
									for (DistrictMaster districtMaster : districts) {
										qqRequiredMap.put(districtMaster.getDistrictId(), false);
									}
									
									if(districts.size()>0)
									{
										districts = teacherAnswerDetailsForDistrictSpecificQuestionsDAO.findByQQTakenByTeacher(districts,teacherDetail);
										for (DistrictMaster districtMaster : districts) {
											//qqTakenMap.put(districtMaster.getDistrictId(), true);
											qqRequiredMap.put(districtMaster.getDistrictId(), true);
										}
									}
								}
								
								for(JobForTeacher jft: lstJobForTeacher){
								
									Integer districtId = null;
									if(jft.getJobId().getDistrictMaster()!=null)
										districtId	 = jft.getJobId().getDistrictMaster().getDistrictId();
								Boolean teacherQQTaken	= qqRequiredMap.get(districtId)==null?true:qqRequiredMap.get(districtId);
								if(jft.getJobId().getJobCategoryMaster().getOfferQualificationItems()==false)
									teacherQQTaken=true;
								else if(jft.getJobId().getDistrictMaster()==null)
									teacherQQTaken=true;
								JobOrder jO = jft.getJobId();
								if(jO.getIsJobAssessment()==false){
									//if(jO.getIsJobAssessment()==false || jO.getJobCategoryMaster().getOfferJSI()==2){
										boolean completeFlag=false;
										try{
											if(!jft.getStatus().getStatusShortName().equalsIgnoreCase("comp") && statusMaster.getStatusShortName().equalsIgnoreCase("comp")){
												completeFlag=true;
												jobForTeacherObj=jft;
											}
										}catch(Exception e){
											e.printStackTrace();
										}
										if(jft.getInternalSecondaryStatus()!=null){
											jft.setStatus(jft.getInternalStatus());
											jft.setStatusMaster(null);
											jft.setSecondaryStatus(jft.getInternalSecondaryStatus());
										}else if(jft.getInternalStatus()!=null){
											jft.setStatus(jft.getInternalStatus());
											jft.setStatusMaster(jft.getInternalStatus());
										}else{
											String jftStatus = jft.getStatus().getStatusShortName();
											if(jftStatus.equalsIgnoreCase("icomp")|| jftStatus.equalsIgnoreCase("comp") || jftStatus.equalsIgnoreCase("vlt")){
												if(teacherQQTaken)
												{
													jft.setStatus(statusMaster);
													try{
														if(jft.getSecondaryStatus()!=null){
															jft.setStatusMaster(null);
														}else{
															jft.setStatusMaster(statusMaster);
														}
													}catch(Exception e){
														jft.setStatusMaster(statusMaster);
														e.printStackTrace();
													}
												}else
												{
													jft.setStatus(statusMasterIcomp);
													try{
														if(jft.getSecondaryStatus()!=null){
															jft.setStatusMaster(null);
														}else{
															jft.setStatusMaster(statusMasterIcomp);
														}
													}catch(Exception e){
														jft.setStatusMaster(statusMasterIcomp);
														e.printStackTrace();
													}
												}
											}
										}
										
										jobForTeacherDAO.makePersistent(jft);
										
										if(teacherQQTaken)
										{
											try{
												if(completeFlag && jobForTeacherObj!=null && jobForTeacherObj.getStatus()!=null && jobForTeacherObj.getStatus().getStatusShortName().equalsIgnoreCase("comp")){
													System.out.println("::::::::::::::Update Status 23-::::::::::::::::");
													commonService.futureJobStatus(jobForTeacherObj.getJobId(), jobForTeacherObj.getTeacherId(),jobForTeacherObj);
												}
											}catch(Exception e){
												e.printStackTrace();
											}
										}
										
									}else if(jft.getJobId().getJobCategoryMaster().getBaseStatus()){//**##**
										lstJobForTeacherIcomp.add(jft);
										lstJBOrder.add(jft.getJobId());
									}
								}
								TeacherAssessmentStatus tAStatus = null;
								if(lstJobForTeacherIcomp.size()>0){
									List<TeacherAssessmentStatus> lstTAStatus = teacherAssessmentStatusDAO.findByTeacherAndListJob(teacherDetail, lstJBOrder);
									Map<Integer, TeacherAssessmentStatus> mapAssessStatus = new HashMap<Integer, TeacherAssessmentStatus>();
									for(TeacherAssessmentStatus tas: lstTAStatus){
										mapAssessStatus.put(tas.getJobOrder().getJobId(), tas);
									}
									districts = new ArrayList<DistrictMaster>();
									for(JobForTeacher jft: lstJobForTeacherIcomp){
										districts.add(jft.getJobId().getDistrictMaster());
									}
									if(districts.size()>0)
									{
										districts = districtSpecificQuestionsDAO.getDistrictSpecificQuestionBYDistricts(districts);
										
										for (DistrictMaster districtMaster : districts) {
											qqRequiredMap.put(districtMaster.getDistrictId(), false);
										}
										
										if(districts.size()>0)
										{
											districts = teacherAnswerDetailsForDistrictSpecificQuestionsDAO.findByQQTakenByTeacher(districts,teacherDetail);
											for (DistrictMaster districtMaster : districts) {
												//qqTakenMap.put(districtMaster.getDistrictId(), true);
												qqRequiredMap.put(districtMaster.getDistrictId(), true);
											}
										}
									}
									
									for(JobForTeacher jft: lstJobForTeacherIcomp){
										
										Integer districtId = jft.getJobId().getDistrictMaster().getDistrictId();
										Boolean teacherQQTaken	= qqRequiredMap.get(districtId)==null?true:qqRequiredMap.get(districtId);
										if(jft.getJobId().getJobCategoryMaster().getOfferQualificationItems()==false)
											teacherQQTaken=true;
										
										boolean completeFlag=false;
										tAStatus = mapAssessStatus.get(jft.getJobId().getJobId());
										if(tAStatus==null && jft.getJobId().getJobCategoryMaster().getOfferJSI()==2)
										{
											tAStatus = new TeacherAssessmentStatus();
											tAStatus.setStatusMaster(statusMaster);
										}
										try{
											if(!jft.getStatus().getStatusShortName().equalsIgnoreCase("comp") && tAStatus!=null &&  tAStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("comp")){
												completeFlag=true;
												jobForTeacherObj=jft;
											}
										}catch(Exception e){
											e.printStackTrace();
										}
										if(tAStatus!=null){
											if(jft.getInternalSecondaryStatus()!=null){
												jft.setStatus(jft.getInternalStatus());
												jft.setStatusMaster(null);
												jft.setSecondaryStatus(jft.getInternalSecondaryStatus());
											}else if(jft.getInternalStatus()!=null){
												jft.setStatus(jft.getInternalStatus());
												jft.setStatusMaster(jft.getInternalStatus());
											}else{
												String jftStatus = jft.getStatus().getStatusShortName();
												if(jftStatus.equalsIgnoreCase("icomp")|| jftStatus.equalsIgnoreCase("comp") || jftStatus.equalsIgnoreCase("vlt")){
													if(teacherQQTaken)
													{
														jft.setStatus(tAStatus.getStatusMaster());
														try{
															if(jft.getSecondaryStatus()!=null){
																jft.setStatusMaster(null);
															}else{
																jft.setStatusMaster(tAStatus.getStatusMaster());
															}
														}catch(Exception e){
															jft.setStatusMaster(tAStatus.getStatusMaster());
															e.printStackTrace();
														}
													}else
													{
														jft.setStatus(statusMasterIcomp);
														try{
															if(jft.getSecondaryStatus()!=null){
																jft.setStatusMaster(null);
															}else{
																jft.setStatusMaster(statusMasterIcomp);
															}
														}catch(Exception e){
															jft.setStatusMaster(statusMasterIcomp);
															e.printStackTrace();
														}
													}
													
												}
											}
											jobForTeacherDAO.makePersistent(jft);
											if(teacherQQTaken)
											{
												try{
													if(completeFlag && jobForTeacherObj!=null && jobForTeacherObj.getStatus()!=null && jobForTeacherObj.getStatus().getStatusShortName().equalsIgnoreCase("comp")){
														commonService.futureJobStatus(jobForTeacherObj.getJobId(), jobForTeacherObj.getTeacherId(),jobForTeacherObj);
													}
												}catch(Exception e){
													e.printStackTrace();
												}
											}
										}else
										{
											jft.setStatus(statusMasterIcomp);
											try{
												if(jft.getSecondaryStatus()!=null){
													jft.setStatusMaster(null);
												}else{
													jft.setStatusMaster(statusMasterIcomp);
												}
											}catch(Exception e){
												jft.setStatusMaster(statusMasterIcomp);
												e.printStackTrace();
											}
											jobForTeacherDAO.makePersistent(jft);
										}
									}
								}
								/////////////////////////////////////////////////////////////////////
								onDashboard = true; 
							}else if(teacherAssessmentdetail.getAssessmentType()==2)
							{
								jobOrder=teacherAssessmentStatus.getJobOrder();
								String status = teacherAssessmentStatus.getStatusMaster().getStatusShortName();
								if(status.equalsIgnoreCase("comp"))
								{
									//send mail to candidate on completion of JSI according to district settings for JSI.
									sendMailToCandidate(teacherAssessmentdetail.getAssessmentType(),"comp",jobOrder.getJobId(),epiJobId);
								}
								try{
									if(jobOrderForUpdate==null){
										jobOrderForUpdate=jobOrder;
									}
								}catch(Exception e){
									e.printStackTrace();
								}
								System.out.println("jobOrderForUpdate::::::"+jobOrderForUpdate);
								jId=jobOrder.getJobId();
								List<AssessmentJobRelation>	assessmentJobRelations1=assessmentJobRelationDAO.findRelationByAssessment(teacherAssessmentStatus.getAssessmentDetail());

								for (AssessmentJobRelation assessmentJobRelation : assessmentJobRelations1) {
									jobs.add(assessmentJobRelation.getJobId());
								}

								List<JobForTeacher> jobForTeachers = jobForTeacherDAO.findJobByTeacherAndSatusFormJobIds(teacherDetail, jobs);
								String jftStatus = "";
								boolean completeFlag=false;
								JobForTeacher jobForTeacherObj=null;
								
								//==================
								Map<Integer,Boolean> qqRequiredMap = new HashMap<Integer, Boolean>();
								
								List<DistrictMaster> districts = new ArrayList<DistrictMaster>();
								for(JobForTeacher jft: jobForTeachers){
									districts.add(jft.getJobId().getDistrictMaster());
								}
								if(districts.size()>0)
								{
									districts = districtSpecificQuestionsDAO.getDistrictSpecificQuestionBYDistricts(districts);
									
									for (DistrictMaster districtMaster : districts) {
										qqRequiredMap.put(districtMaster.getDistrictId(), false);
									}
									
									if(districts.size()>0)
									{
										districts = teacherAnswerDetailsForDistrictSpecificQuestionsDAO.findByQQTakenByTeacher(districts,teacherDetail);
										for (DistrictMaster districtMaster : districts) {
											//qqTakenMap.put(districtMaster.getDistrictId(), true);
											qqRequiredMap.put(districtMaster.getDistrictId(), true);
										}
									}
								}
								//==========================================================
								
								boolean isBaseVlt = false;
								StatusMaster statusMasterVlt = WorkThreadServlet.statusMap.get("vlt");
								JobOrder jobOrder2 = new JobOrder();
								jobOrder2.setJobId(0);
								List<TeacherAssessmentStatus> teacherAssessmentStatusList = null;
								teacherAssessmentStatusList = teacherAssessmentStatusDAO.findAssessmentTaken(teacherDetail, jobOrder2);
								if(teacherAssessmentStatusList!=null && teacherAssessmentStatusList.size()>0 && teacherAssessmentStatusList.get(0).getStatusMaster()!=null && teacherAssessmentStatusList.get(0).getStatusMaster().getStatusShortName().equalsIgnoreCase("vlt")){
									isBaseVlt = true;
								}
								
								for(JobForTeacher jft: jobForTeachers)
								{
									jftStatus = jft.getStatus().getStatusShortName();
									Integer districtId = jft.getJobId().getDistrictMaster().getDistrictId();
									Boolean teacherQQTaken	= qqRequiredMap.get(districtId)==null?true:qqRequiredMap.get(districtId);
									if(jft.getJobId().getJobCategoryMaster().getOfferQualificationItems()==false)
										teacherQQTaken=true;
									
									try{
										if(jobOrderForUpdate!=null && jobOrderForUpdate.getJobId().equals(jft.getJobId().getJobId())){
											if(!jftStatus.equalsIgnoreCase("comp") && statusMaster.getStatusShortName().equalsIgnoreCase("comp")){
												completeFlag=true;
												jobForTeacherObj=jft;
											}
										}
									}catch(Exception e){
										e.printStackTrace();
									}
									if(!(jftStatus.equalsIgnoreCase("hird") || jftStatus.equalsIgnoreCase("widrw") || jftStatus.equalsIgnoreCase("hide")))
									{
										if(jft.getInternalSecondaryStatus()!=null){
											jft.setStatus(jft.getInternalStatus());
											jft.setStatusMaster(null);
											jft.setSecondaryStatus(jft.getInternalSecondaryStatus());
										}else if(jft.getInternalStatus()!=null){
											jft.setStatus(jft.getInternalStatus());
											jft.setStatusMaster(jft.getInternalStatus());
										}else{
											if(jftStatus.equalsIgnoreCase("icomp")|| jftStatus.equalsIgnoreCase("comp") || jftStatus.equalsIgnoreCase("vlt")){
												
												if(teacherQQTaken)
												{
													jft.setStatus(statusMaster);
													try{
														if(jft.getSecondaryStatus()!=null){
															jft.setStatusMaster(null);
														}else{
															jft.setStatusMaster(statusMaster);
														}
													}catch(Exception e){
														jft.setStatusMaster(statusMaster);
														e.printStackTrace();
													}
												}else
												{
													jft.setStatus(statusMasterIcomp);
													try{
														if(jft.getSecondaryStatus()!=null){
															jft.setStatusMaster(null);
														}else{
															jft.setStatusMaster(statusMasterIcomp);
														}
													}catch(Exception e){
														jft.setStatusMaster(statusMasterIcomp);
														e.printStackTrace();
													}
												}
											}
										}
									
										// New Check for Base timed out or not
										if(isBaseVlt && jft.getJobId().getJobCategoryMaster().getBaseStatus() && jft.getJobId().getDistrictMaster()!=null){
											DistrictMaster districtMaster = jft.getJobId().getDistrictMaster();
											List<InternalTransferCandidates> itcList = internalTransferCandidatesDAO.getInternalCandidateByDistrict(teacherDetail,districtMaster);
											int internalFlag=0;
											int offerEPI=1;
											if(itcList.size()>0){
												internalFlag=1;
											}
											if(internalFlag==1){
												if(districtMaster.getOfferEPI()){
													offerEPI=1;
												}else{
													offerEPI=0;
												}
											}
											try{
												if(jft.getJobId().getJobCategoryMaster()!=null){
													if(jft.getJobId().getJobCategoryMaster().getEpiForFullTimeTeachers()!=null){
														int isAffilated=0;
														if(jft.getIsAffilated()!=null){
															 isAffilated=jft.getIsAffilated();
														}
														if(internalFlag==0 && isAffilated==1){
															if(jft.getJobId().getJobCategoryMaster().getEpiForFullTimeTeachers()){
																offerEPI=1;
															}else{
																offerEPI=0;
															}
														}
													}
												}
											}catch(Exception e){
												e.printStackTrace();
											}
											System.out.println("offerEPI:::::::::::::::::::::"+offerEPI);
											if(offerEPI==1){
												jft.setStatus(statusMasterVlt);
												jft.setStatusMaster(statusMasterVlt);
											}
										}
										// New Check JSI optional
										if(jft.getJobId().getJobAssessmentStatus()==1)
											jobForTeacherDAO.makePersistent(jft);
									}
								}
								try{
									if(completeFlag && jobForTeacherObj!=null && jobForTeacherObj.getStatus()!=null && jobForTeacherObj.getStatus().getStatusShortName().equalsIgnoreCase("comp")){
										commonService.futureJobStatus(jobForTeacherObj.getJobId(), jobForTeacherObj.getTeacherId(),jobForTeacherObj);
									}
								}catch(Exception e){
									e.printStackTrace();
								}

								String exitMessage = null;
								String exitURL = null;
								if(jobOrder!=null)
								{
									if(newJobId>0)
										jobOrder = jobOrderDAO.findById(newJobId, false, false); 

									int flagForURL = jobOrder.getFlagForURL();
									int flagForMessage  = jobOrder.getFlagForMessage();
									exitMessage = jobOrder.getExitMessage();
									exitURL = jobOrder.getExitURL();
									if(exitMessage!=null && !exitMessage.trim().equals("") && exitURL!=null && !exitURL.trim().equals(""))
									{
										onDashboard = true;
									}else if(flagForMessage==1)
									{
										onDashboard = false;
										sb.append("window.location.href='thankyoumessage.do?jobId="+jobOrder.getJobId()+"'");

									}else if(flagForURL==1)
									{
										sb.append("document.getElementById('mainDiv').style.display='none';");
										if(epiStandalone)
											sb.append("window.location.href='epiboard.do?jobId="+jobOrder.getJobId()+"';");
										else
											sb.append("window.location.href='userdashboard.do?jobId="+jobOrder.getJobId()+"';");
									}else
										onDashboard = true;
								}

							}

							if(onDashboard)
							{
								String redirectURL = "userdashboard.do";
								sb.append("document.getElementById('mainDiv').style.display='none';");
								String msg= Utility.getLocaleValuePropByKey("msgTeacherMatchBaseInventory", locale);
								if(jId>0)
								{
									//msg= "You have completed this Job Specific Inventory.";
									msg= Utility.getLocaleValuePropByKey("msgDistAssCamp3", locale);
									redirectURL="jobsofinterest.do";
								}
								//sb.append("alert('"+msg+"');");
								//sb.append("window.location.href='"+redirectURL+"'");
								//showInfoAndRedirect(sts,msg,nextmsg,url)
								if(epiStandalone)
									redirectURL="epiboard.do";

								sb.append("showInfoAndRedirect(0,'"+msg+"','','"+redirectURL+"',"+assessmentType+");");
							}
							sb.append("</script>");
						}

					teacherAssessmentdetail.setIsDone(true);
					teacherAssessmentDetailDAO.makePersistent(teacherAssessmentdetail);

				}else
				{
					sb.append("No Questions found");
					sb.append("<script type=\"text/javascript\" language=\"javascript\">");
					sb.append("window.onbeforeunload = function() {};");
					sb.append("document.getElementById('mainDiv').style.display='none';");
					//sb.append("alert('No Questions found');");
					if(epiStandalone)
						sb.append("window.location.href='epiboard.do'");
					else
						sb.append("window.location.href='userdashboard.do'");

					sb.append("</script>");
				}

				sb.append("</td></tr>");
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	}
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to insert TeacherAssessmentAttempt.
	 */
	@Transactional(readOnly=false)
	public int insertTeacherAssessmentAttempt(AssessmentDetail assessmentDetail,JobOrder jobOrder)
	{
		//System.out.println(":::::::::::::::Assessment insertTeacherAssessmentAttempt:::::::::::::::::::;; ");

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		try{
			String ipAddress = request.getRemoteAddr();
			TeacherDetail teacherDetail = null;
			if (session == null || session.getAttribute("teacherDetail") == null) {
				//return "false";
			}else
				teacherDetail=(TeacherDetail)session.getAttribute("teacherDetail");

			List<TeacherAssessmentStatus> teacherAssessmentStatusList = null;
			TeacherAssessmentdetail teacherAssessmentdetail = null;
			//amit
			if(assessmentDetail!=null)
			{
				teacherAssessmentStatusList = teacherAssessmentStatusDAO.getTeacherAssessmentStatus(assessmentDetail,teacherDetail);
				if(teacherAssessmentStatusList!=null && teacherAssessmentStatusList.size()!=0)
					teacherAssessmentdetail = teacherAssessmentStatusList.get(0).getTeacherAssessmentdetail();
			}
			
			// teacherassessmentattempt
			TeacherAssessmentAttempt teacherAssessmentAttempt = new TeacherAssessmentAttempt();
			teacherAssessmentAttempt.setAssessmentDetail(assessmentDetail);
			teacherAssessmentAttempt.setIpAddress(ipAddress);
			if(jobOrder!=null && jobOrder.getJobId()!=null && jobOrder.getJobId()==0)
				teacherAssessmentAttempt.setJobOrder(null);
			else
				teacherAssessmentAttempt.setJobOrder(jobOrder);

			teacherAssessmentAttempt.setTeacherDetail(teacherDetail);
			teacherAssessmentAttempt.setAssessmentSessionTime(0);
			teacherAssessmentAttempt.setAssessmentStartTime(new Date());
			teacherAssessmentAttempt.setIsForced(false);
			teacherAssessmentAttempt.setAssessmentTakenCount(teacherAssessmentdetail.getAssessmentTakenCount());
			teacherAssessmentAttempt.setTeacherAssessmentdetail(teacherAssessmentdetail);
			teacherAssessmentAttemptDAO.makePersistent(teacherAssessmentAttempt);
			return 1;
		}catch (Exception e) {
			e.printStackTrace();
			return 0;
		}

	}
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to set TeacherAssessmentAttempt forced quit.
	 */
	@Transactional(readOnly=false)
	public int setAssessmentForced(int attemptId)
	{
		//System.out.println(":::::::::::::::Assessment setAssessmentForced:::::::::::::::::::;; ");

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		try{
			if(attemptId!=0)
			{
				teacherAssessmentAttemptDAO.updateAssessmentAttemptSessionTime(attemptId,true);
			}
			return 1;
		}catch (Exception e) {
			e.printStackTrace();
			return 0;
		}

	}

	/* @Author: Vishwanath Kumar
	 * @Discription: .
	 */
	public String insertLastAttempt(AssessmentDetail assessmentDetail, Integer assessmentTakenCount, JobOrder jobOrder)
	{
		//System.out.println(":::::::::::::::Assessment insertLastAttempt:::::::::::::::::::;; ");

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		String ipAddress = request.getRemoteAddr();
		TeacherDetail teacherDetail = null;

		if (session == null || session.getAttribute("teacherDetail") == null) {
			//return "false";
		}else
			teacherDetail=(TeacherDetail)session.getAttribute("teacherDetail");

		List<TeacherAssessmentStatus> teacherAssessmentStatusList = null;
		TeacherAssessmentStatus teacherAssessmentStatus = null;
		//teacherAssessmentStatusList=teacherAssessmentStatusDAO.findAssessmentTaken(teacherDetail,jobOrder);
		teacherAssessmentStatusList=teacherAssessmentStatusDAO.findAssessmentTaken(assessmentDetail,assessmentTakenCount,teacherDetail,jobOrder);
		TeacherAssessmentdetail teacherAssessmentdetail = null;
		if(teacherAssessmentStatusList.size()!=0)
		{
			teacherAssessmentStatus = teacherAssessmentStatusList.get(0);
		}

		List<TeacherAssessmentAttempt> teacherAssessmentAttempts = null;
		//teacherAssessmentAttempts = teacherAssessmentAttemptDAO.findAssessmentAttempts(teacherDetail,jobOrder);
		teacherAssessmentAttempts = teacherAssessmentAttemptDAO.findAssessmentAttempts(teacherDetail,assessmentDetail,assessmentTakenCount,jobOrder);

		int attempts = 0;
		for (TeacherAssessmentAttempt teacherAssessmentAttempt1 : teacherAssessmentAttempts) {
			if(!teacherAssessmentAttempt1.getIsForced())
				attempts++;
		}
		////////////////////////

		/******** Sekhar add for count strikeLogs ( Start )*******/
		int strikeLogs=0; 
		try{
			if(teacherAssessmentAttempts.size()>0)
			{
				List<TeacherAssessmentStatus> teacherAssessmentStatusList2 = null;
				teacherAssessmentStatusList2 = teacherAssessmentStatusDAO.getTeacherAssessmentStatus(teacherAssessmentAttempts.get(0).getAssessmentDetail(), teacherDetail);
				List<TeacherStrikeLog> teacherStrikeLog = null;
				teacherStrikeLog = teacherStrikeLogDAO.getTotalStrikesByTeacher(teacherDetail, teacherAssessmentStatusList2.get(0).getTeacherAssessmentdetail());
				strikeLogs=teacherStrikeLog.size();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		int attemptsStrike=attempts+strikeLogs;
		/******** Sekhar add for count strikeLogs ( End )*******/

		if(attemptsStrike==3)
		{
			StatusMaster statusMaster = WorkThreadServlet.statusMap.get("vlt");
			teacherAssessmentdetail = teacherAssessmentStatus.getTeacherAssessmentdetail();

			List<TeacherAssessmentStatus> lstTeacherAssessmentStatus = null;
			lstTeacherAssessmentStatus =teacherAssessmentStatusDAO.findAssessmentStatusByTeacherAssessmentdetail(teacherAssessmentdetail);
			teacherAssessmentStatus=lstTeacherAssessmentStatus.get(0);

			List<JobOrder> jobs = new ArrayList<JobOrder>();

			if(lstTeacherAssessmentStatus.size()!=0)
				if(teacherAssessmentStatus!=null)
				{
					for(TeacherAssessmentStatus tas: lstTeacherAssessmentStatus)
					{
						jobs.add(tas.getJobOrder());

						tas.setStatusMaster(statusMaster);
						teacherAssessmentStatusDAO.makePersistent(tas);
					}
				}
			// JobForTeacher update after 3 attempts
			if(jobOrder.getJobId()!=null && jobOrder.getJobId()>0)
			{
				
				// New Changes 
				List<AssessmentJobRelation>	assessmentJobRelations1=assessmentJobRelationDAO.findRelationByAssessment(teacherAssessmentStatus.getAssessmentDetail());
				for (AssessmentJobRelation assessmentJobRelation : assessmentJobRelations1) {
					jobs.add(assessmentJobRelation.getJobId());
				}

				List<JobForTeacher> jobForTeachers = jobForTeacherDAO.findJobByTeacherAndSatusFormJobIds(teacherDetail, jobs);
				///////////////
				String jftStatus = "";
				for(JobForTeacher jft: jobForTeachers)
				{
					jftStatus = jft.getStatus().getStatusShortName();
					if(!(jftStatus.equalsIgnoreCase("hird") || jftStatus.equalsIgnoreCase("widrw") || jftStatus.equalsIgnoreCase("hide")))
					{
						if(jft.getInternalSecondaryStatus()!=null){
							jft.setStatus(jft.getInternalStatus());
							jft.setStatusMaster(null);
							jft.setSecondaryStatus(jft.getInternalSecondaryStatus());
						}else if(jft.getInternalStatus()!=null){
							jft.setStatus(jft.getInternalStatus());
							jft.setStatusMaster(jft.getInternalStatus());
						}else{
							if(jftStatus.equalsIgnoreCase("icomp")|| jftStatus.equalsIgnoreCase("comp") || jftStatus.equalsIgnoreCase("vlt")){
								jft.setStatus(statusMaster);
								try{
									if(jft.getSecondaryStatus()!=null){
										jft.setStatusMaster(null);
									}else{
										jft.setStatusMaster(statusMaster);
									}
								}catch(Exception e){
									jft.setStatusMaster(statusMaster);
									e.printStackTrace();
								}
							}
						}
						// New Check JSI optional
						if(jft.getJobId().getJobAssessmentStatus()==1)
							jobForTeacherDAO.makePersistent(jft);
					}
				}
			}else
			{
				// base taken status 
				StatusMaster epiStatus = null;
				JobOrder jjj = new JobOrder();
				jjj.setJobId(0);
				List<TeacherAssessmentStatus> baseStatusList = teacherAssessmentStatusDAO.findAssessmentTaken(teacherDetail,jjj);
				if(baseStatusList.size()>0)
					epiStatus = baseStatusList.get(0).getStatusMaster();
				//////////////////////////////////////////////
				
				///////////////////////// for external user /////////////////////////
				List<JobForTeacher> lstJobForTeacherIcomp = new ArrayList<JobForTeacher>();
				List<JobOrder> lstJBOrder = new ArrayList<JobOrder>();
				StatusMaster stautsIcomp = WorkThreadServlet.statusMap.get("icomp");
				List<JobForTeacher> lstJobForTeacher= null;
				lstJobForTeacher = jobForTeacherDAO.findJobByTeacherAndSatusDuringCurrentYear(teacherDetail,stautsIcomp);
				for(JobForTeacher jft: lstJobForTeacher)
				{
					if(jft.getJobId().getIsJobAssessment()==false)
					{
						if(jft.getInternalSecondaryStatus()!=null){
							jft.setStatus(jft.getInternalStatus());
							jft.setStatusMaster(null);
							jft.setSecondaryStatus(jft.getInternalSecondaryStatus());
						}else if(jft.getInternalStatus()!=null){
							jft.setStatus(jft.getInternalStatus());
							jft.setStatusMaster(jft.getInternalStatus());
						}else{
							String jftStatus = jft.getStatus().getStatusShortName();
							if(jftStatus.equalsIgnoreCase("icomp")|| jftStatus.equalsIgnoreCase("comp") || jftStatus.equalsIgnoreCase("vlt")){
								jft.setStatus(statusMaster);
								try{
									if(jft.getSecondaryStatus()!=null){
										jft.setStatusMaster(null);
									}else{
										jft.setStatusMaster(statusMaster);
									}
								}catch(Exception e){
									jft.setStatusMaster(statusMaster);
									e.printStackTrace();
								}
							}
						}
						jobForTeacherDAO.makePersistent(jft);
					}else if(jft.getJobId().getJobCategoryMaster().getBaseStatus()){ //**##**
						lstJobForTeacherIcomp.add(jft);
						lstJBOrder.add(jft.getJobId());
					}
				}

				TeacherAssessmentStatus tAStatus = null;
				if(lstJobForTeacherIcomp.size()>0){
					List<TeacherAssessmentStatus> lstTAStatus = teacherAssessmentStatusDAO.findByTeacherAndListJob(teacherDetail, lstJBOrder);
					Map<Integer, TeacherAssessmentStatus> mapAssessStatus = new HashMap<Integer, TeacherAssessmentStatus>();
					for(TeacherAssessmentStatus tas: lstTAStatus){
						mapAssessStatus.put(tas.getJobOrder().getJobId(), tas);
					}
					for(JobForTeacher jft: lstJobForTeacherIcomp){
						tAStatus = mapAssessStatus.get(jft.getJobId().getJobId());
						if(tAStatus!=null){
							if(jft.getInternalSecondaryStatus()!=null){
								jft.setStatus(jft.getInternalStatus());
								jft.setStatusMaster(null);
								jft.setSecondaryStatus(jft.getInternalSecondaryStatus());
							}else if(jft.getInternalStatus()!=null){
								jft.setStatus(jft.getInternalStatus());
								jft.setStatusMaster(jft.getInternalStatus());
							}else{
								String jftStatus = jft.getStatus().getStatusShortName();
								if(jftStatus.equalsIgnoreCase("icomp")|| jftStatus.equalsIgnoreCase("comp") || jftStatus.equalsIgnoreCase("vlt")){
									jft.setStatus(tAStatus.getStatusMaster());
									try{
										if(jft.getSecondaryStatus()!=null){
											jft.setStatusMaster(null);
										}else{
											jft.setStatusMaster(tAStatus.getStatusMaster());
										}
									}catch(Exception e){
										jft.setStatusMaster(tAStatus.getStatusMaster());
										e.printStackTrace();
									}	
								}
							}
							// New Check JSI optional
							if(jft.getJobId().getJobAssessmentStatus()==1)
								jobForTeacherDAO.makePersistent(jft);
						}else{
	                        try{
	                            if(jft.getJobId().getJobCategoryMaster().getBaseStatus()){
	                            	System.out.println("--{{{{{{{{{{{{{{{{{{{{{{{{{{{------------------- "+epiStatus);
		                            if(epiStatus!=null)
		                            {
			                             jft.setStatusMaster(epiStatus);
			                             jft.setStatus(epiStatus);
			                             jobForTeacherDAO.makePersistent(jft);
		                            }else
		                            {
		                            	 jft.setStatusMaster(stautsIcomp);
			                             jft.setStatus(stautsIcomp);
			                             jobForTeacherDAO.makePersistent(jft);
		                            	
		                            }
	                             }
	                         }catch(Exception e){
	                        	 e.printStackTrace();
	                         }
                        }
					}							
				}
				/////////////////////////////////////////////////////////////////////
			}
		}

		return ""+attempts;
	}

	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to finish assessment whether completed or not.
	 */
	@Transactional(readOnly=false)
	public String finishAssessment(TeacherAssessmentdetail teacherAssessmentdetail,JobOrder jobOrder,int newJobId,int sessionCheck,Integer epiJobId)
	{
		System.out.println(":::::::::::::::AssessmentCampaignAjax : finishAssessment:::::::::::::::");

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		try{
			Boolean epiStandalone = session.getAttribute("epiStandalone")==null?false:(Boolean)session.getAttribute("epiStandalone");

			String ipAddress = request.getRemoteAddr();
			TeacherDetail teacherDetail = null;
			if (session == null || session.getAttribute("teacherDetail") == null) {
				//return "false";
			}else
				teacherDetail=(TeacherDetail)session.getAttribute("teacherDetail");

			////////////////////////////////////////////////////////////////////////////////
			StringBuffer sb =new StringBuffer();
			int jId=0;
			List<TeacherAssessmentQuestion> teacherAssessmentQuestions =null;
			Criterion criterion = Restrictions.eq("teacherAssessmentdetail", teacherAssessmentdetail);
			Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion2 = Restrictions.eq("isAttempted", false);
			teacherAssessmentQuestions = teacherAssessmentQuestionDAO.findByCriteria(criterion,criterion1,criterion2);
			int unattemptedQuestions = teacherAssessmentQuestions.size();

			// TeacherAssessmentStatus update after completion
			List<TeacherAssessmentStatus> lstTeacherAssessmentStatus = null;

			lstTeacherAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentStatusByTeacherAssessmentdetail(teacherAssessmentdetail);
			TeacherAssessmentStatus teacherAssessmentStatus=lstTeacherAssessmentStatus.get(0);

			int assessmentType=1;
			if(teacherAssessmentStatus!=null){
				if(teacherAssessmentStatus.getAssessmentType()!=null){
					assessmentType=teacherAssessmentStatus.getAssessmentType();
				}
			}

			if(lstTeacherAssessmentStatus.size()!=0)
				if(teacherAssessmentStatus!=null)
				{
					StatusMaster statusMaster = null;
					String appendMsg="not ";
					String status="";
					if(unattemptedQuestions==0)
					{
						status="comp";
						appendMsg="";
					}
					else
						status="vlt";

					statusMaster = WorkThreadServlet.statusMap.get(status);

					List<JobOrder> jobs = new ArrayList<JobOrder>();
					for(TeacherAssessmentStatus tas: lstTeacherAssessmentStatus)
					{
						jobs.add(tas.getJobOrder());

						tas.setStatusMaster(statusMaster);
						if(statusMaster.getStatusShortName().equalsIgnoreCase("comp"))
							tas.setAssessmentCompletedDateTime(new Date());
						teacherAssessmentStatusDAO.makePersistent(tas);
					}

					boolean onDashboard = false;
					List<JobForTeacher> lstJobForTeacherIcomp = new ArrayList<JobForTeacher>();
					List<JobOrder> lstJBOrder = new ArrayList<JobOrder>();
					sb.append("<script type=\"text/javascript\" language=\"javascript\">");
					sb.append("window.onbeforeunload = function() {};");

					if(teacherAssessmentdetail.getAssessmentType()==1)
					{
						///////////////////////// candidate data coping ///////////////////
						if(status.equalsIgnoreCase("comp"))
						{
							CadidateRawScoreThread crt = new CadidateRawScoreThread();
							crt.setCandidateRawScoreDAO(candidateRawScoreDAO);
							crt.setTeacherAnswerDetailDAO(teacherAnswerDetailDAO);
							crt.setTeacherAssessmentQuestionDAO(teacherAssessmentQuestionDAO);
							crt.setTeacherDetail(teacherDetail);
							crt.setReportService(reportService);
							crt.setTeacherAssessmentdetail(teacherAssessmentdetail);
							crt.setTeacherAssessmentStatus(teacherAssessmentStatus);
							crt.start();
							
							//send mail to candidate on completion of EPI according to district settings for EPI.
							sendMailToCandidate(teacherAssessmentdetail.getAssessmentType(),"comp",jobOrder.getJobId(),epiJobId);
							
							DistrictMaster districtMaster = null;
							if(epiJobId!=null && epiJobId>0){
								JobOrder epiJobOrder  = jobOrderDAO.findById(epiJobId, false, false);
								if(epiJobOrder!=null){
									if(epiJobOrder.getDistrictMaster()!=null){
										districtMaster = epiJobOrder.getDistrictMaster();
									}
									//Send Mail to all DAs after EPI completion
									try{
										if(districtMaster!=null){
											if(districtMaster.getDistrictId()==7800292){
												sendMailToAllDAAfterEpiComplition(userMasterDAO,districtMaster,teacherDetail,epiJobOrder,"EPI");
											}
										}
									}catch(Exception e){e.printStackTrace();};
								}
							}
						}
						else if(status.equalsIgnoreCase("vlt"))
						{
							//send mail to candidate on timed out of EPI according to district settings for EPI.
							sendMailToCandidate(teacherAssessmentdetail.getAssessmentType(),"vlt",jobOrder.getJobId(),epiJobId);
						}
						///////////////////////////////////////////////////////////////////
						///////////////////////// for external user /////////////////////////
						List<JobForTeacher> lstJobForTeacher= null;
						lstJobForTeacher = jobForTeacherDAO.findJobByTeacherAndSatusDuringCurrentYear(teacherDetail,WorkThreadServlet.statusMap.get("icomp"));
						for(JobForTeacher jft: lstJobForTeacher)
						{
							if(jft.getJobId().getIsJobAssessment()==false)
							{
								if(jft.getInternalSecondaryStatus()!=null){
									jft.setStatus(jft.getInternalStatus());
									jft.setStatusMaster(null);
									jft.setSecondaryStatus(jft.getInternalSecondaryStatus());
								}else if(jft.getInternalStatus()!=null){
									jft.setStatus(jft.getInternalStatus());
									jft.setStatusMaster(jft.getInternalStatus());
								}else{
									String jftStatus = jft.getStatus().getStatusShortName();
									if(jftStatus.equalsIgnoreCase("icomp")|| jftStatus.equalsIgnoreCase("comp") || jftStatus.equalsIgnoreCase("vlt")){
										jft.setStatus(statusMaster);
										try{
											if(jft.getSecondaryStatus()!=null){
												jft.setStatusMaster(null);
											}else{
												jft.setStatusMaster(statusMaster);
											}
										}catch(Exception e){
											jft.setStatusMaster(statusMaster);
											e.printStackTrace();
										}
									}
								}

								boolean isInviteFlag = false;
								if(jft.getJobId().getIsInviteOnly()!=null && jft.getJobId().getIsInviteOnly())
								{
									isInviteFlag=true;
								}
								if(isInviteFlag==false)	{
									jobForTeacherDAO.makePersistent(jft);
								}
							}
							else if(jft.getJobId().getJobCategoryMaster().getBaseStatus()){ //**##**
								lstJobForTeacherIcomp.add(jft);
								lstJBOrder.add(jft.getJobId());
							}
						}

						TeacherAssessmentStatus tAStatus = null;
						if(lstJobForTeacherIcomp.size()>0){
							List<TeacherAssessmentStatus> lstTAStatus = teacherAssessmentStatusDAO.findByTeacherAndListJob(teacherDetail, lstJBOrder);
							Map<Integer, TeacherAssessmentStatus> mapAssessStatus = new HashMap<Integer, TeacherAssessmentStatus>();
							for(TeacherAssessmentStatus tas: lstTAStatus){
								mapAssessStatus.put(tas.getJobOrder().getJobId(), tas);
							}

							for(JobForTeacher jft: lstJobForTeacherIcomp){
								tAStatus = mapAssessStatus.get(jft.getJobId().getJobId());
								if(tAStatus!=null){
									if(jft.getInternalSecondaryStatus()!=null){
										jft.setStatus(jft.getInternalStatus());
										jft.setStatusMaster(null);
										jft.setSecondaryStatus(jft.getInternalSecondaryStatus());
									}else if(jft.getInternalStatus()!=null){
										jft.setStatus(jft.getInternalStatus());
										jft.setStatusMaster(jft.getInternalStatus());
									}else{
										String jftStatus = jft.getStatus().getStatusShortName();
										if(jftStatus.equalsIgnoreCase("icomp")|| jftStatus.equalsIgnoreCase("comp") || jftStatus.equalsIgnoreCase("vlt")){
											jft.setStatus(tAStatus.getStatusMaster());
											try{
												if(jft.getSecondaryStatus()!=null){
													jft.setStatusMaster(null);
												}else{
													jft.setStatusMaster(tAStatus.getStatusMaster());
												}
											}catch(Exception e){
												jft.setStatusMaster(tAStatus.getStatusMaster());
												e.printStackTrace();
											}	
										}
									}
									boolean isInviteFlag = false;
									if(jft.getJobId().getIsInviteOnly()!=null && jft.getJobId().getIsInviteOnly())
									{
										isInviteFlag=true;
									}
									if(isInviteFlag==false)	{
										// New Check JSI optional
										if(jft.getJobId().getJobAssessmentStatus()==1)
											jobForTeacherDAO.makePersistent(jft);
									}
									
								}
							}							
						}
						/////////////////////////////////////////////////////////////////////
						onDashboard = true; 
					}else if(teacherAssessmentdetail.getAssessmentType()==2)
					{
						jobOrder=teacherAssessmentStatus.getJobOrder();
						if(status.equalsIgnoreCase("comp") || status.equalsIgnoreCase("vlt"))
						{
							//send mail to candidate on completion or timed out of JSI according to district settings for JSI.
							sendMailToCandidate(teacherAssessmentdetail.getAssessmentType(),status,jobOrder.getJobId(),epiJobId);
						}
						List<AssessmentJobRelation>	assessmentJobRelations1=assessmentJobRelationDAO.findRelationByAssessment(teacherAssessmentStatus.getAssessmentDetail());

						for (AssessmentJobRelation assessmentJobRelation : assessmentJobRelations1) {
							jobs.add(assessmentJobRelation.getJobId());
						}

						List<JobForTeacher> jobForTeachers = jobForTeacherDAO.findJobByTeacherAndSatusFormJobIds(teacherDetail, jobs);

						String jftStatus = "";
						for(JobForTeacher jft: jobForTeachers)
						{
							jftStatus = jft.getStatus().getStatusShortName();
							if(!(jftStatus.equalsIgnoreCase("hird") || jftStatus.equalsIgnoreCase("widrw") || jftStatus.equalsIgnoreCase("hide")))
							{
								if(jft.getInternalSecondaryStatus()!=null){
									jft.setStatus(jft.getInternalStatus());
									jft.setStatusMaster(null);
									jft.setSecondaryStatus(jft.getInternalSecondaryStatus());
								}else if(jft.getInternalStatus()!=null){
									jft.setStatus(jft.getInternalStatus());
									jft.setStatusMaster(jft.getInternalStatus());
								}else{
									if(jftStatus.equalsIgnoreCase("icomp")|| jftStatus.equalsIgnoreCase("comp") || jftStatus.equalsIgnoreCase("vlt")){
										jft.setStatus(statusMaster);
										try{
											if(jft.getSecondaryStatus()!=null){
												jft.setStatusMaster(null);
											}else{
												jft.setStatusMaster(statusMaster);
											}
										}catch(Exception e){
											jft.setStatusMaster(statusMaster);
											e.printStackTrace();
										}
									}
								}
								// New Check JSI optional
								if(jft.getJobId().getJobAssessmentStatus()==1)
									jobForTeacherDAO.makePersistent(jft);
							}
						}

						String exitMessage = null;
						String exitURL = null;
						if(jobOrder!=null)
						{
							jId=jobOrder.getJobId();

							if(newJobId>0)
								jobOrder = jobOrderDAO.findById(newJobId, false, false); 

							int flagForURL = jobOrder.getFlagForURL();
							int flagForMessage  = jobOrder.getFlagForMessage();
							exitMessage = jobOrder.getExitMessage();
							exitURL = jobOrder.getExitURL();
							if(exitMessage!=null && !exitMessage.trim().equals("") && exitURL!=null && !exitURL.trim().equals(""))
							{
								onDashboard = true;
							}else if(flagForMessage==1)
							{
								onDashboard = false;
								boolean isVlt = false;
								if(status.equalsIgnoreCase("vlt"))
								{
									String msg1="";
									/*if(sessionCheck==1)
										msg1= Utility.getLocaleValuePropByKey("msgAssesmentCall1", locale)+"(888) 312 7231 "+Utility.getLocaleValuePropByKey("msgoremailusat", locale)+" <a href=\"mailto:clientservices@teachermatch.net\">clientservices@teachermatch.net</a>.";
									else
										msg1=Utility.getLocaleValuePropByKey("msgAssesmentCall2", locale)+" <a href=\"mailto:clientservices@teachermatch.net\">clientservices@teachermatch.net</a>.";*/
									
									if(sessionCheck==1)
										msg1="You have exceeded the Job Specific Inventory time limit and you will no longer be able to continue with the Job Specific Inventory. As you have timed out, you are no longer eligible to respond to Job Specific Inventory questions. Your status will remain \"Timed Out\" for 12 months. If you have any further questions regarding this matter, please call us at (888) 312 7231 or email us at <a href=\"mailto:clientservices@teachermatch.net\">clientservices@teachermatch.net</a>.";
									else
										msg1="You have exceeded the time limit for your 3rd and final attempt and have timed out of the Job Specific Inventory. As you have timed out, you are no longer eligible to respond to Job Specific Inventory questions. Your status will remain \"Timed Out\" for 12 months. If you have any further questions regarding this matter, please call us at (888) 312 7231 or email us at <a href=\"mailto:clientservices@teachermatch.net\">clientservices@teachermatch.net</a>.";
									
									
									sb.append("document.getElementById('mainDiv').style.display='none';");
									sb.append("showInfoAndRedirect(2,'"+msg1+"','','thankyoumessage.do?jobId="+jobOrder.getJobId()+"',"+assessmentType+");");
									isVlt = true;
								}
								if(!isVlt)
									sb.append("window.location.href='thankyoumessage.do?jobId="+jobOrder.getJobId()+"'");
							}else if(flagForURL==1)
							{
								sb.append("document.getElementById('mainDiv').style.display='none';");
								String m="";
								if(appendMsg.equals(""))
									m="y";
								else
									m="n";
								if(epiStandalone)
									sb.append("window.location.href='epiboard.do?jobId="+jobOrder.getJobId()+"&mf="+m+"';");
								else
									sb.append("window.location.href='userdashboard.do?jobId="+jobOrder.getJobId()+"&mf="+m+"';");
							}else
								onDashboard = true;
						}
					}

					if(onDashboard)
					{
						String redirectURL = "userdashboard.do";
						sb.append("document.getElementById('mainDiv').style.display='none';");
						String msg="";
						/*if(sessionCheck==1)
							msg= Utility.getLocaleValuePropByKey("msgAssesmentCall3", locale)+" (888) 312 7231 "+Utility.getLocaleValuePropByKey("msgoremailusat", locale)+" <a href=\"mailto:clientservices@teachermatch.net\">clientservices@teachermatch.net</a>.";
						else
							msg=Utility.getLocaleValuePropByKey("msgAssesmentCall4", locale)+" (888) 312 7231 "+Utility.getLocaleValuePropByKey("msgoremailusat", locale)+" <a href=\"mailto:clientservices@teachermatch.net\">clientservices@teachermatch.net</a>.";*/
						
						boolean isKellyJobApply = false;
						isKellyJobApply = jobForTeacherDAO.getJFTByTeachers(teacherDetail);
						System.out.println("isKellyJobApply : "+isKellyJobApply);
						boolean isKelly = false;
						if(request.getServerName().contains("kelly") || request.getServerName().toLowerCase().contains("smartpractice") || request.getRequestURL().toString().contains("smartpractice")){
							isKelly = true;
						}

						if(sessionCheck==1)
							msg="You have exceeded the Base Inventory time limit and you will no longer be able to continue with the Base Inventory. As you have timed out, you are no longer eligible to respond to Base Inventory questions. Your status will remain \"Timed Out\" for 12 months. If you have any further questions regarding this matter, please call us at (888) 312 7231 or email us at <a href=\"mailto:clientservices@teachermatch.net\">clientservices@teachermatch.net</a>.";
						else{
							//msg="You have exceeded the time limit for your 3rd and final attempt and have timed out of the Base Inventory. As you have timed out, you are no longer eligible to respond to Base Inventory questions. Your status will remain \"Timed Out\" for 12 months. If you have any further questions regarding this matter, please call us at (888) 312 7231 or email us at <a href=\"mailto:clientservices@teachermatch.net\">clientservices@teachermatch.net</a>.";
							msg="thirdVlt";
						}
						
						if((isKellyJobApply || isKelly) && assessmentType==1)
						{
							System.out.println(":::::: kelly timedout msg ::::::");
							msg="<p>Dear valuable Kelly Educational Staffing&reg; (KES&reg;) talent, unfortunately you have been timed out from the EPI. As a result, you will not be required to respond to additional more EPI items.</p>";
							msg+="<p>If you have any further questions regarding your EPI status, please call us at 885-535-5915 or email us at <a href=\"KESTMSP@kellyservices.com\">KESTMSP@kellyservices.com</a>. If you do not reach to the Kelly support team, your EPI status will remain \"Timed Out\" for 12 months.</p>";
							msg+="<p>You may still apply to any position you are interested in, and your EPI status will NOT prevent you from being considered by Kelly Educational Staffing&reg; (KES&reg;).</p>";
							msg+="<p>If you would like to review TeacherMatch&#39;s policy regarding this issue in greater depth please click <a target=\"_blank\" href=\"policiesandprocedures.do\">here</a>.</p>";
						}
						int sts = 2;
						if(jId>0)
						{
							if(appendMsg.equals(""))
							{
								msg= Utility.getLocaleValuePropByKey("msgDistAssCamp3", locale);
								sts = 0;
							}
							else
								msg= Utility.getLocaleValuePropByKey("msgNotCompTMInvent", locale);

							redirectURL="jobsofinterest.do";
						}

						if(epiStandalone)
							redirectURL="epiboard.do";

						sb.append("showInfoAndRedirect("+sts+",'"+msg+"','','"+redirectURL+"',"+assessmentType+");");
					}
					sb.append("</script>");

					teacherAssessmentdetail = teacherAssessmentDetailDAO.findById(teacherAssessmentdetail.getTeacherAssessmentId(), false, false);
					teacherAssessmentdetail.setIsDone(true);
					teacherAssessmentDetailDAO.makePersistent(teacherAssessmentdetail);
				}

			////////////////////////////////////////////////////////////////////////////////

			return sb.toString();
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to insert Teacher Assessment Strike.
	 */
	@Transactional(readOnly=false)
	public int saveStrike(TeacherAssessmentdetail teacherAssessmentdetail,TeacherAssessmentQuestion teacherAssessmentQuestion)
	{

		//System.out.println(":::::::::::::::Assessment saveStrike:::::::::::::::::::;; ");

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		try{
			String ipAddress = request.getRemoteAddr();
			TeacherDetail teacherDetail = null;
			if (session == null || session.getAttribute("teacherDetail") == null) {
				//return "false";
			}else
				teacherDetail=(TeacherDetail)session.getAttribute("teacherDetail");

			// teacherassessmentattempt
			TeacherStrikeLog teacherStrikeLog = new TeacherStrikeLog();
			teacherStrikeLog.setTeacherDetail(teacherDetail);
			teacherStrikeLog.setTeacherAssessmentdetail(teacherAssessmentdetail);
			teacherStrikeLog.setTeacherAssessmentQuestion(teacherAssessmentQuestion);
			teacherStrikeLog.setIpAddress(ipAddress);
			teacherStrikeLog.setCreatedDateTime(new Date());

			teacherStrikeLogDAO.makePersistent(teacherStrikeLog);

			return 1;
		}catch (Exception e) {
			e.printStackTrace();
			return 0;
		}

	}
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to checkAssessmentDone.
	 */
	@Transactional(readOnly=false)
	public int checkAssessmentDone(TeacherAssessmentdetail teacherAssessmentdetail)
	{
		//System.out.println(":::::::::::::::Assessment checkAssessmentDone:::::::::::::::::::;; ");

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		try{
			TeacherDetail teacherDetail = null;
			if (session == null || session.getAttribute("teacherDetail") == null) {
				//return "false";
			}else
				teacherDetail=(TeacherDetail)session.getAttribute("teacherDetail");

			teacherAssessmentdetail = teacherAssessmentDetailDAO.findById(teacherAssessmentdetail.getTeacherAssessmentId(), false, false);

			if(teacherAssessmentdetail.getIsDone())
				return 1;
			else
				return 0;

		}catch (Exception e) {
			e.printStackTrace();
			return 0;
		}

	}

	/* @Author: Gagan 
	 * @Discription: It is used to check isAffilated check.
	 */
	@Transactional(readOnly=false)
	public int checkIsAffilated(Integer jobId,Integer isAffilated)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		try{
			TeacherDetail teacherDetail = null;
			if (session == null || session.getAttribute("teacherDetail") == null) {
				//return "false";
			}else
				teacherDetail=(TeacherDetail)session.getAttribute("teacherDetail");

			JobOrder jobOrder=jobOrderDAO.findById(jobId, false, false);
			List<StatusMaster> lstStatusMasters =  Utility.getStaticMasters(new String[]{"comp","icomp","vlt"});

			List<JobForTeacher> lstjobForTeacher=jobForTeacherDAO.findDistrictOrSchoolLatestAppliedJobByTeacher(teacherDetail,jobOrder.getDistrictMaster(),lstStatusMasters); 

			if(lstjobForTeacher!=null && lstjobForTeacher.size()>0)
			{
				/* ========= checking Last Latest Job Applied ==  isAffilated value should be different       ===================*/
				if((lstjobForTeacher.get((lstjobForTeacher.size()-1)).getIsAffilated()!=null) && (lstjobForTeacher.get((lstjobForTeacher.size()-1)).getIsAffilated()-isAffilated)!=0)
				{
					return 1;
				}
				else
				{
					return 0;
				}
			}
			else
			{
				return 0;
			}

		}catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
		//return jobId;
		//return 0;

	}


	/* @Author: Gagan 
	 * @Discription: It is used to check isAffilated check.
	 */
	@Transactional(readOnly=false)
	public int resetjftIsAffilated(Integer jobId,Integer isAffilated)
	{
		//	System.out.println(":::::::::::::::Assessment resetjftIsAffilated:::::::::::::::::::;; ");

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		try{
			TeacherDetail teacherDetail = null;
			StatusMaster statusMaster = null;
			TeacherAssessmentStatus teacherAssessmentStatus = null;
			AssessmentDetail assessmentDetail = null;
			if (session == null || session.getAttribute("teacherDetail") == null) {
				//return "false";
			}else
				teacherDetail=(TeacherDetail)session.getAttribute("teacherDetail");

			TeacherPortfolioStatus teacherPortfolioStatus = teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);

			JobOrder jobOrder1=jobOrderDAO.findById(jobId, false, false);
			Map<String,StatusMaster> statusMap = new HashMap<String, StatusMaster>();
			List<StatusMaster> lstStatusMasters = new ArrayList<StatusMaster>();
			String[] statuss = {"comp","icomp","vlt"};
			try{
				lstStatusMasters = Utility.getStaticMasters(statuss);
				for (StatusMaster statusmaster : lstStatusMasters) {
					statusMap.put(statusmaster.getStatusShortName(), statusmaster);
				}
			}catch (Exception e) {
				e.printStackTrace();
			}


			List<JobForTeacher> lstjobForTeacher=jobForTeacherDAO.findDistrictOrSchoolLatestAppliedJobByTeacher(teacherDetail,jobOrder1.getDistrictMaster(),lstStatusMasters); 

			/*==================================== Resetiing Status Here ==========================*/
			boolean completeFlag=false;
			if(lstjobForTeacher.size()>0)
			{
				for(JobForTeacher jb:lstjobForTeacher)
				{
					int[] flagList=assessmentDetailDAO.getInternalTeacherFalgs(jb) ;
					statusMaster = assessmentDetailDAO.findByTeacherIdJobStaus(jb,flagList);
					try{
						String jftStatus=jb.getStatus().getStatusShortName();
						if(!jftStatus.equalsIgnoreCase("comp") && statusMaster.getStatusShortName().equalsIgnoreCase("comp")){
							completeFlag=true;
						}
					}catch(Exception e){
						e.printStackTrace();
					}
					/*----------------------------------------------------------------------------------------------------------*/
					jb.setJobForTeacherId(jb.getJobForTeacherId());
					jb.setIsAffilated(isAffilated);
					if(jb.getInternalSecondaryStatus()!=null){
						jb.setStatus(jb.getInternalStatus());
						jb.setStatusMaster(null);
						jb.setSecondaryStatus(jb.getInternalSecondaryStatus());
					}else if(jb.getInternalStatus()!=null){
						jb.setStatus(jb.getInternalStatus());
						jb.setStatusMaster(jb.getInternalStatus());
					}else{
						jb.setStatus(statusMaster);
						jb.setStatusMaster(statusMaster);
					}
					jobForTeacherDAO.makePersistent(jb);
					try{
						if(completeFlag && jb.getStatus()!=null && jb.getStatus().getStatusShortName().equalsIgnoreCase("comp")){
							commonService.futureJobStatus(jb.getJobId(), teacherDetail,jb);
						}
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			}
			else
			{
				return 0;
			}

		}catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
		//return jobId;
		return 1;

	}

	// Gagan : For getting latest cover letter by Teacher
	@Transactional(readOnly=false)
	public JobForTeacher getLatestCoverLetter()
	{

		//System.out.println(":::::::::::::::Assessment getLatestCoverLetter:::::::::::::::::::;; ");

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		try{
			//System.out.println("calling... getLatestCoverLetter Method");
			TeacherDetail teacherDetail = null;
			if (session == null || session.getAttribute("teacherDetail") == null) {
				//return "false";
			}else
				teacherDetail=(TeacherDetail)session.getAttribute("teacherDetail");
			JobForTeacher jFTCover		= jobForTeacherDAO.findJobByTeacherAndCoverLetter(teacherDetail);

			if(jFTCover==null)
				return null;
			else
				return jFTCover;	

		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	/* @Author: Gagan 
	 * @Discription: It is used to check isAffilated check.
	 */
	@Transactional(readOnly=false)
	public String getDistrictSpecificQuestion(Integer jobId,String isAffilated)
	{
		System.out.println("*****:::::::::::::::Assessment getDistrictSpecificQuestion:::::::::::::::::::;; getDistrictSpecificQuestion");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		StringBuffer sb =new StringBuffer();
		try{
			List<DistrictSpecificQuestions> districtSpecificQuestionList	= new ArrayList<DistrictSpecificQuestions>(); 
			TeacherDetail teacherDetail = null;

			if (session == null || session.getAttribute("teacherDetail") == null) {
				//return "false";
			}else
				teacherDetail=(TeacherDetail)session.getAttribute("teacherDetail");

			if(jobId!=0)
			{
				JobOrder jobOrder=jobOrderDAO.findById(jobId, false, false);
				JobCategoryMaster jobCategoryMaster = jobOrder.getJobCategoryMaster();
				QqQuestionSets qqQuestionSets = new QqQuestionSets();
				List<QqQuestionsetQuestions> qqQuestionsetQuestions=null;
				if(jobCategoryMaster!=null && jobCategoryMaster.getQuestionSets()!=null){
					qqQuestionSets = jobCategoryMaster.getQuestionSets();
					qqQuestionsetQuestions = qqQuestionsetQuestionsDAO.findByQuestionSet(jobCategoryMaster.getQuestionSets()); 
				}
				List<Integer> tempDistSpecQues = new ArrayList<Integer>();
				if(qqQuestionsetQuestions!=null && qqQuestionsetQuestions.size()>0){
					for (QqQuestionsetQuestions qqQuestionsetQuestions2 : qqQuestionsetQuestions) {
						tempDistSpecQues.add(qqQuestionsetQuestions2.getDistrictSpecificQuestions().getQuestionId());
					}
				}
				JobForTeacher jft = jobForTeacherDAO.findJFTByTeacherAndJob(teacherDetail, jobOrder);
				//districtSpecificQuestionList=districtSpecificQuestionsDAO.getDistrictSpecificQuestionBYDistrict(jobOrder.getDistrictMaster());
				System.out.println(">.>.> 1  metthod 1");
				if(tempDistSpecQues.size()>0)
					districtSpecificQuestionList=districtSpecificQuestionsDAO.getDistrictSpecificQuestionBYQues(tempDistSpecQues);
				System.out.println("districtSpecificQuestionList-------->"+districtSpecificQuestionList.size());
				System.out.println(">.>.> 2 metthod 1");
				List<JobOrder> jobOrders = teacherAnswerDetailsForDistrictSpecificQuestionsDAO.findLastTeacherAnswersByDistrictAndJob(teacherDetail,jobOrder);
				System.out.println(">.>.> 3 methhod 1");
				List<TeacherAnswerDetailsForDistrictSpecificQuestions> lastList = new ArrayList<TeacherAnswerDetailsForDistrictSpecificQuestions>();
				System.out.println(">.>.> 4 method 1");
				if(jobOrders.size()>0)
					lastList = teacherAnswerDetailsForDistrictSpecificQuestionsDAO.findTeacherAnswersBySetQues(teacherDetail,qqQuestionSets);

				Map<Integer, TeacherAnswerDetailsForDistrictSpecificQuestions> map = new HashMap<Integer, TeacherAnswerDetailsForDistrictSpecificQuestions>();

				if(lastList!=null)
				for(TeacherAnswerDetailsForDistrictSpecificQuestions teacherAnswerDetailsForDistrictSpecificQuestion : lastList)
					map.put(teacherAnswerDetailsForDistrictSpecificQuestion.getDistrictSpecificQuestions().getQuestionId(), teacherAnswerDetailsForDistrictSpecificQuestion);

				TeacherAnswerDetailsForDistrictSpecificQuestions teacherAnswerDetailsForDistrictSpecificQuestion = null;
				List<OptionsForDistrictSpecificQuestions> questionOptionsList = null;
				String questionInstruction = null;
				String shortName = "";
				int totalQuestions =0;
				if(tempDistSpecQues.size()>0)
					totalQuestions = districtSpecificQuestionList.size();
				int cnt = 0;


				///***** Sekhar Add TranseferCandidate ******///
				DistrictMaster districtMaster=null;
				boolean isMiami = false;
				if(jobOrder.getDistrictMaster()!=null){
					districtMaster=jobOrder.getDistrictMaster();
					if(districtMaster.getDistrictId().equals(1200390))
						isMiami = true;
				}
				List<InternalTransferCandidates> itcList=internalTransferCandidatesDAO.getInternalCandidateByDistrict(teacherDetail,districtMaster);
				boolean internalFlag=false;
				boolean getQQ=true;
				if(itcList.size()>0){
					internalFlag=true;
				}
				if(districtMaster!=null && internalFlag){
					if(districtMaster.getOfferQualificationItems()){
						getQQ=true;
					}else{
						getQQ=false;
					}
				}
				
				try{
					if(jft!=null){
						if(jft.getIsAffilated()!=null && jft.getIsAffilated()==1 && internalFlag==false){
							if(jobOrder.getJobCategoryMaster()!=null && jobOrder.getJobCategoryMaster().getOfferQualificationItems()){
								getQQ=true;
							}else{
								getQQ=false;
							}
						}
					}else{
						if(isAffilated!=null && isAffilated.equalsIgnoreCase("1") && internalFlag==false){
							if(jobOrder.getJobCategoryMaster()!=null && jobOrder.getJobCategoryMaster().getOfferQualificationItems()){
								getQQ=true;
							}else{
								getQQ=false;
							}
						}
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				
				try {					
				
				if(jobOrder!= null && jobOrder.getJobCategoryMaster()!=null && jobOrder.getJobCategoryMaster().getJobCategoryName().equalsIgnoreCase("Aspiring Assistant Principal")){
					//System.out.println("inside jobcategroyr");
					TeacherPersonalInfo teacherPersonalInfo = teacherPersonalInfoDAO.findTeacherPersonalInfoByTeacher(teacherDetail);
					if(teacherPersonalInfo!=null && teacherPersonalInfo.getEmployeeType().equals(1)){
						isAffilated=1+"";
				//		System.out.println("inside jobcategroyr  "+isAffilated);
					}
					
					if(isAffilated!=null && isAffilated.equalsIgnoreCase("1") && isMiami && jobOrder.getJobCategoryMaster()!=null && jobOrder.getJobCategoryMaster().getJobCategoryName().equalsIgnoreCase("Aspiring Assistant Principal")){
						getQQ=false;
					}
					
				}
				} catch (Exception e) {}
				
				try {
					System.out.println("get QQQQQQQQ   "+getQQ+"   "+isAffilated);
				} catch (Exception e) {}
				
				// change 
				boolean focus = false;
				if(getQQ && totalQuestions>0){					
					for (Integer quesId : tempDistSpecQues) {
						for (DistrictSpecificQuestions districtSpecificQuestion : districtSpecificQuestionList) 
						{
							if(quesId.equals(districtSpecificQuestion.getQuestionId())){

								shortName = districtSpecificQuestion.getQuestionTypeMaster().getQuestionTypeShortName();

								sb.append("<tr>");
								sb.append("<td width='90%'>");
								sb.append("<div><b>Question "+(++cnt)+" of "+totalQuestions+"</b></div>");

								questionInstruction = districtSpecificQuestion.getQuestionInstructions()==null?"":districtSpecificQuestion.getQuestionInstructions();
								if(!questionInstruction.equals(""))
									sb.append(""+Utility.getUTFToHTML(questionInstruction)+"");

								sb.append("<div id='Q"+cnt+"question'>"+Utility.getUTFToHTML(districtSpecificQuestion.getQuestion())+"</div>");

								questionOptionsList = districtSpecificQuestion.getQuestionOptions();

								teacherAnswerDetailsForDistrictSpecificQuestion = map.get(districtSpecificQuestion.getQuestionId());

								String checked = "";
								Integer optId = 0;
								String insertedText = "";
								if(teacherAnswerDetailsForDistrictSpecificQuestion!=null)
								{
									if(shortName.equalsIgnoreCase(teacherAnswerDetailsForDistrictSpecificQuestion.getQuestionType()))
									{
										if(teacherAnswerDetailsForDistrictSpecificQuestion.getSelectedOptions()!=null)
											optId = Integer.parseInt(teacherAnswerDetailsForDistrictSpecificQuestion.getSelectedOptions());
										insertedText = teacherAnswerDetailsForDistrictSpecificQuestion.getInsertedText();
									}
								}
								if(shortName.equalsIgnoreCase("tf") || shortName.equalsIgnoreCase("slsel"))
								{
									sb.append("<table>");
									for (OptionsForDistrictSpecificQuestions questionOptions : questionOptionsList) {
										if(optId.equals(questionOptions.getOptionId()))
											checked = "checked";
										else
											checked = "";

										sb.append("<tr><td style='border:0px;background-color: transparent;'><div  style='display: block;min-height: 20px;padding-left: 5px;vertical-align: middle;'><input type='radio' name='Q"+cnt+"opt' value='"+questionOptions.getOptionId()+"' "+checked+" /></div>" +
												" <input type='hidden' id='Q"+questionOptions.getOptionId()+"validQuestion' value='"+questionOptions.getValidOption()+"' ></td>" +
												"<td style='border:0px;background-color: transparent;padding-left:5px;' id='qOpt"+questionOptions.getOptionId()+"'>"+questionOptions.getQuestionOption()+" ");
										sb.append("</td></tr>");
									}
									sb.append("</table>");
								}
								if(shortName.equalsIgnoreCase("et"))
								{
									String display="none";
									sb.append("<table>");
									for (OptionsForDistrictSpecificQuestions questionOptions : questionOptionsList) {
										if(optId.equals(questionOptions.getOptionId()))
											checked = "checked";
										else
											checked = "";
										
										sb.append("<tr><td style='border:0px;background-color: transparent;'><div  style='display: block;min-height: 20px;padding-left: 5px;vertical-align: middle;'><input type='radio' name='Q"+cnt+"opt' value='"+questionOptions.getOptionId()+"' "+checked+" onclick=getQuestionEvent("+"\"Q"+cnt+"optet\","+"\"Q"+questionOptions.getOptionId()+"requiredExplanation\");"+" /></div>" +
												" <input type='hidden' id='Q"+questionOptions.getOptionId()+"validQuestion' value='"+questionOptions.getValidOption()+"' ></td>" +
												"<td style='border:0px;background-color: transparent;padding-left:5px;'  id='qOpt"+questionOptions.getOptionId()+"'>"+questionOptions.getQuestionOption()+" ");
										sb.append("</td></tr>");
										sb.append(" <input type='hidden' id='Q"+questionOptions.getOptionId()+"requiredExplanation' value='"+questionOptions.getRequiredExplanation()+"' >");
										if(questionOptions.getRequiredExplanation()!=null){
											if(questionOptions.getRequiredExplanation()==true){
												if(checked.equals("checked")){
											display="block";
															}
												}
										}
									}
									sb.append("</table>");

									if(districtSpecificQuestion.getQuestionExplanation()!=null)
									sb.append("<div>"+Utility.getUTFToHTML(districtSpecificQuestion.getQuestionExplanation())+"</div>");
									sb.append("&nbsp;&nbsp;<span style=''><textarea  name='Q"+cnt+"opt' id='Q"+cnt+"optet' class='form-control' maxlength='5000' rows='8' style='width:98%;margin:5px;display:"+display+";'>"+insertedText+"</textarea><span><br/>");
									if(focus==false)
									{
										sb.append("<script type=\"text/javascript\" language=\"javascript\">");
										//sb.append("document.getElementById('Q"+cnt+"optet').focus();");
										sb.append("</script>");
										focus = true;
									}
									sb.append("<script type=\"text/javascript\" language=\"javascript\">");
									//sb.append("document.getElementById('Q"+cnt+"optet').focus();");
									sb.append("</script>");
								}
								if(shortName.equalsIgnoreCase("ml"))
								{
									sb.append("&nbsp;&nbsp;<textarea name='Q"+cnt+"opt' id='Q"+cnt+"opt' class='form-control' maxlength='5000' rows='8' style='width:98%;margin:5px;'>"+insertedText+"</textarea><br/>");					
									if(focus==false)
									{
										sb.append("<script type=\"text/javascript\" language=\"javascript\">");
										//sb.append("document.getElementById('Q"+cnt+"opt').focus();");
										sb.append("</script>");
										focus = true;
									}
									sb.append("<script type=\"text/javascript\" language=\"javascript\">");
									//sb.append("document.getElementById('Q"+cnt+"opt').focus();");
									sb.append("</script>");
								}
								sb.append("<input type='hidden' id='Q"+cnt+"questionId' value='"+districtSpecificQuestion.getQuestionId()+"'/>" );
								sb.append("<input type='hidden' id='Q"+cnt+"questionTypeId' value='"+districtSpecificQuestion.getQuestionTypeMaster().getQuestionTypeId()+"'/>" );
								sb.append("<input type='hidden' id='Q"+cnt+"questionTypeShortName' value='"+shortName+"'/>" );
								

								sb.append("</td>");
								sb.append("</tr>");
								insertedText = "";
							
							}
						}
					}
			}
					
				if(jobCategoryMaster.getQuestionSets()!=null && !jobCategoryMaster.getQuestionSets().getID().equals(0))
					sb.append("<input type='hidden' id='qqQuestionSetID' value='"+jobCategoryMaster.getQuestionSets().getID()+"'/>" );
				else
					sb.append("<input type='hidden' id='qqQuestionSetID' value=''/>" );
				if(jobOrder.getDistrictMaster()!=null)
				sb.append("@##@"+cnt+"@##@"+jobOrder.getDistrictMaster().getTextForDistrictSpecificQuestions());
				else
					sb.append("@##@"+cnt+"@##@"+"");

				List<JobForTeacher> lstJobForTeacher= null;
				JobForTeacher jobForTeacher = null;
				lstJobForTeacher = jobForTeacherDAO.findJobByTeacherAndSatusFormJobId(teacherDetail,jobOrder);
				boolean completeFlag=false;
				if(lstJobForTeacher==null || lstJobForTeacher.size()==0)
				{
					PrintOnConsole.debugPrintln("getDistrictSpecificQuestion ... Not save in JFT");
				}else
				{
					PrintOnConsole.debugPrintln("getDistrictSpecificQuestion ... already saved in JFT");
					////////////////////Checking status to be set///////////////////////
					jobForTeacher  = lstJobForTeacher.get(0);
					StatusMaster statusMaster = null;
					if(districtSpecificQuestionList.size()>0 && jobForTeacher.getFlagForDistrictSpecificQuestions()==null)
					{
						statusMaster = WorkThreadServlet.statusMap.get("icomp");
						if(jobForTeacher.getInternalSecondaryStatus()!=null){
							jobForTeacher.setStatus(jobForTeacher.getInternalStatus());
							jobForTeacher.setStatusMaster(null);
							jobForTeacher.setSecondaryStatus(jobForTeacher.getInternalSecondaryStatus());
						}else if(jobForTeacher.getInternalStatus()!=null){
							try{
								if(jobForTeacher.getStatus()!=null && (jobForTeacher.getStatus().getStatusShortName().equals("icomp")|| jobForTeacher.getStatus().getStatusShortName().equals("comp") || jobForTeacher.getStatus().getStatusShortName().equals("vlt"))){
									jobForTeacher.setStatus(jobForTeacher.getInternalStatus());
									jobForTeacher.setStatusMaster(jobForTeacher.getInternalStatus());
								}
							}catch(Exception e){}
						}else{
							try{
								if(jobForTeacher.getStatus()!=null && (jobForTeacher.getStatus().getStatusShortName().equals("icomp")|| jobForTeacher.getStatus().getStatusShortName().equals("comp") || jobForTeacher.getStatus().getStatusShortName().equals("vlt"))){
									jobForTeacher.setStatus(statusMaster);
									if(jobForTeacher.getSecondaryStatus()!=null){
										jobForTeacher.setStatusMaster(null);
									}else{
										jobForTeacher.setStatusMaster(statusMaster);
									}
								}
							}catch(Exception e){}
						}
					}else
					{

						/*String isAffilated = "0";
					if(jobForTeacher.getIsAffilated()!=null)
						isAffilated = ""+jobForTeacher.getIsAffilated();

					statusMaster = assessmentDetailDAO.findByTeacherIdJobStaus(jobForTeacher, isAffilated,internalFlag,offerEPI,offerJSI,isPortfolio);*/

						int[] flagList=assessmentDetailDAO.getInternalTeacherFalgs(jobForTeacher) ;
						statusMaster = assessmentDetailDAO.findByTeacherIdJobStaus(jobForTeacher,flagList);


						if(jobForTeacher.getInternalSecondaryStatus()!=null){
							jobForTeacher.setStatus(jobForTeacher.getInternalStatus());
							jobForTeacher.setStatusMaster(null);
							jobForTeacher.setSecondaryStatus(jobForTeacher.getInternalSecondaryStatus());
						}else if(jobForTeacher.getInternalStatus()!=null){
							try{
								String jftStatus=jobForTeacher.getStatus().getStatusShortName();
								if(!jftStatus.equalsIgnoreCase("comp") && statusMaster.getStatusShortName().equalsIgnoreCase("comp")){
									completeFlag=true;
								}
							}catch(Exception e){
								e.printStackTrace();
							}
							try{
								if(jobForTeacher.getStatus()!=null && (jobForTeacher.getStatus().getStatusShortName().equals("icomp")|| jobForTeacher.getStatus().getStatusShortName().equals("comp") || jobForTeacher.getStatus().getStatusShortName().equals("vlt"))){
									jobForTeacher.setStatus(jobForTeacher.getInternalStatus());
									jobForTeacher.setStatusMaster(jobForTeacher.getInternalStatus());
								}
							}catch(Exception e){}
						}else{
							try{
								String jftStatus=jobForTeacher.getStatus().getStatusShortName();
								if(!jftStatus.equalsIgnoreCase("comp") && statusMaster.getStatusShortName().equalsIgnoreCase("comp")){
									completeFlag=true;
								}
							}catch(Exception e){
								e.printStackTrace();
							}
							try{
								if(jobForTeacher.getStatus()!=null && (jobForTeacher.getStatus().getStatusShortName().equals("icomp")|| jobForTeacher.getStatus().getStatusShortName().equals("comp") || jobForTeacher.getStatus().getStatusShortName().equals("vlt"))){
									jobForTeacher.setStatus(statusMaster);
									if(jobForTeacher.getSecondaryStatus()!=null){
										jobForTeacher.setStatusMaster(null);
									}else{
										jobForTeacher.setStatusMaster(statusMaster);
									}
								}
							}catch(Exception e){}
						}
					}
					try{
						commonService.autoRejectCandidate(jobOrder,teacherDetail,jobForTeacher);
					 }catch(Exception e){
						 e.printStackTrace();
					 }
					jobForTeacherDAO.makePersistent(jobForTeacher);

					try{
						if(completeFlag && jobForTeacher.getStatus()!=null && jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("comp")){
							commonService.futureJobStatus(jobOrder, teacherDetail,jobForTeacher);
						}
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			}
			//////////////////////	
		}catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	}

	@Transactional(readOnly=false)
	public String setDistrictQuestions(TeacherAnswerDetailsForDistrictSpecificQuestions[] answerDetailsForDistrictSpecificQuestions)
	{
		//System.out.println(" *** ::::::::::::::::::::setDistrictQuestions:::::::::::::::::::");
		//System.out.println(":::::::::::::::Assessment setDistrictQuestions:::::::::::::::::::;; ");

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}

		TeacherDetail teacherDetail = null;

		if (session == null || session.getAttribute("teacherDetail") == null) {
			//return "false";
		}else
			teacherDetail=(TeacherDetail)session.getAttribute("teacherDetail");

		try {

			int inValidCount = 0;
			List<TeacherAnswerDetailsForDistrictSpecificQuestions> wrongAnswerDetailsForDistrictSpecificQuestionList = new ArrayList<TeacherAnswerDetailsForDistrictSpecificQuestions>();
			JobOrder jobOrder = answerDetailsForDistrictSpecificQuestions[0].getJobOrder();
			jobOrder = jobOrderDAO.findById(jobOrder.getJobId(), false, false);

			//List<TeacherAnswerDetailsForDistrictSpecificQuestions> lastList = new ArrayList<TeacherAnswerDetailsForDistrictSpecificQuestions>();
			Map<Integer,Integer> map = new HashMap<Integer, Integer>();

			try {
				map = teacherAnswerDetailsForDistrictSpecificQuestionsDAO.findTeacherAnswersForQQByDistrict(teacherDetail,jobOrder.getDistrictMaster());
			} catch (Exception e) {
				e.printStackTrace();
			}

			List<Integer> answers = new ArrayList<Integer>();
			Integer ansId = null;
			for (TeacherAnswerDetailsForDistrictSpecificQuestions answerDetailsForDistrictSpecificQuestion : answerDetailsForDistrictSpecificQuestions) {
				answerDetailsForDistrictSpecificQuestion.setTeacherDetail(teacherDetail);
				answerDetailsForDistrictSpecificQuestion.setDistrictMaster(jobOrder.getDistrictMaster());
				answerDetailsForDistrictSpecificQuestion.setCreatedDateTime(new Date());

				ansId = map.get(answerDetailsForDistrictSpecificQuestion.getDistrictSpecificQuestions().getQuestionId());
				if(ansId!=null)
				{
					answerDetailsForDistrictSpecificQuestion.setAnswerId(ansId);
				}
				if(!answerDetailsForDistrictSpecificQuestion.getQuestionType().equalsIgnoreCase("ml"))
				{
					if(answerDetailsForDistrictSpecificQuestion.getIsValidAnswer()==false)
					{
						inValidCount++;
						wrongAnswerDetailsForDistrictSpecificQuestionList.add(answerDetailsForDistrictSpecificQuestion);
					}
				}

				answerDetailsForDistrictSpecificQuestion.setIsActive(true);
				teacherAnswerDetailsForDistrictSpecificQuestionsDAO.makePersistent(answerDetailsForDistrictSpecificQuestion);
				answers.add(answerDetailsForDistrictSpecificQuestion.getAnswerId());
			}

			if(answers.size()>0)
				teacherAnswerDetailsForDistrictSpecificQuestionsDAO.updateOtherQQ(teacherDetail,jobOrder.getDistrictMaster(),answers);

			List<JobForTeacher> lstJobForTeacher= null;
			JobForTeacher jobForTeacher = null;
			lstJobForTeacher = jobForTeacherDAO.findJobByTeacherAndSatusFormJobId(teacherDetail,jobOrder);
			boolean validFlag = false;

			if(inValidCount>0)
				validFlag=false;
			else
				validFlag=true;

			if(lstJobForTeacher==null || lstJobForTeacher.size()==0)
			{
				session.setAttribute("jobValid"+jobOrder.getJobId(),validFlag);
				PrintOnConsole.debugPrintln("setDistrictQuestions ... Not save in JFT");
			}else
			{
				PrintOnConsole.debugPrintln("setDistrictQuestions ... already saved in JFT");

				jobForTeacher  = lstJobForTeacher.get(0);
				jobForTeacher.setFlagForDistrictSpecificQuestions(validFlag);
				////////////////////Checking status to be set///////////////////////
				StatusMaster statusMaster = null;
				boolean baseRequired = jobOrder.getJobCategoryMaster().getBaseStatus();
				boolean isJobAssessment = jobOrder.getIsJobAssessment();
				boolean portfolioRequired = jobOrder.getIsPortfolioNeeded(); 
				boolean completeFlag=false;
				//statusMaster = assessmentDetailDAO.findByTeacherIdJobStaus(jobForTeacher, isAffilated,internalFlag,offerEPI,offerJSI,isPortfolio);
				int[] flagList=assessmentDetailDAO.getInternalTeacherFalgs(jobForTeacher) ;
				statusMaster = assessmentDetailDAO.findByTeacherIdJobStaus(jobForTeacher,flagList);
				try{
					String jftStatus=jobForTeacher.getStatus().getStatusShortName();
					if(!jftStatus.equalsIgnoreCase("comp") && statusMaster.getStatusShortName().equalsIgnoreCase("comp")){
						completeFlag=true;
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				if(jobForTeacher.getInternalSecondaryStatus()!=null){
					jobForTeacher.setStatus(jobForTeacher.getInternalStatus());
					jobForTeacher.setStatusMaster(null);
					jobForTeacher.setSecondaryStatus(jobForTeacher.getInternalSecondaryStatus());
				}else if(jobForTeacher.getInternalStatus()!=null){
					jobForTeacher.setStatus(jobForTeacher.getInternalStatus());
					jobForTeacher.setStatusMaster(jobForTeacher.getInternalStatus());
				}else{
					try{
						if(jobForTeacher.getStatus()!=null && (jobForTeacher.getStatus().getStatusShortName().equals("icomp")|| jobForTeacher.getStatus().getStatusShortName().equals("comp") || jobForTeacher.getStatus().getStatusShortName().equals("vlt"))){
							jobForTeacher.setStatus(statusMaster);
							if(jobForTeacher.getSecondaryStatus()!=null){
								jobForTeacher.setStatusMaster(null);
							}else{
								jobForTeacher.setStatusMaster(statusMaster);
							}
						}
					}catch(Exception e){}
				}

				PrintOnConsole.debugPrintln("try to set setIsAffilated flag in setDistrictQuestions");
				if (session != null && session.getAttribute("isCurrentEmploymentNeeded") != null) 
				{
					boolean isCurrentEmploymentNeeded=false;
					isCurrentEmploymentNeeded=(Boolean)session.getAttribute("isCurrentEmploymentNeeded");
					PrintOnConsole.debugPrintln("setDistrictQuestions isCurrentEmploymentNeeded "+isCurrentEmploymentNeeded);
					if(isCurrentEmploymentNeeded)
					{
						TeacherPersonalInfo teacherPersonalInfo=teacherPersonalInfoDAO.findTeacherPersonalInfoByTeacher(teacherDetail);
						if(teacherPersonalInfo!=null && teacherPersonalInfo.getEmployeeType()!=null && teacherPersonalInfo.getEmployeeType()==1)
							jobForTeacher.setIsAffilated(1);
						else
							jobForTeacher.setIsAffilated(0);
					}
					session.removeAttribute("isCurrentEmploymentNeeded");
				}

				jobForTeacherDAO.makePersistent(jobForTeacher);
				////////////////////////////////////
				try{
					if(completeFlag && jobForTeacher.getStatus()!=null && jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("comp")){
						commonService.futureJobStatus(jobOrder, teacherDetail,jobForTeacher);
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			}

			lstJobForTeacher=new ArrayList<JobForTeacher>();
			lstJobForTeacher = jobForTeacherDAO.findJobAppliedByTeacherAndDistrict(teacherDetail, jobOrder.getDistrictMaster(), true);

			if(lstJobForTeacher.size()==0)
			{
				List<JobForTeacher> lstJobForTeacherUpdate = jobForTeacherDAO.findJobAppliedByTeacherAndDistrict(teacherDetail, jobOrder.getDistrictMaster(), null);
				if(lstJobForTeacherUpdate.size()>0)
				{
					for(JobForTeacher jft:lstJobForTeacherUpdate)
					{
						jft.setFlagForDistrictSpecificQuestions(validFlag);
						jobForTeacherDAO.makePersistent(jft);
					}
				}
			}else
			{
				List<JobForTeacher> lstJobForTeacherUpdate = jobForTeacherDAO.findJobAppliedByTeacherAndDistrict(teacherDetail, jobOrder.getDistrictMaster(), null);
				lstJobForTeacherUpdate.removeAll(lstJobForTeacher);
				if(lstJobForTeacherUpdate.size()>0)
				{
					for(JobForTeacher jft:lstJobForTeacherUpdate)
					{
						jft.setFlagForDistrictSpecificQuestions(validFlag);
						jobForTeacherDAO.makePersistent(jft);
					}
				}
			}
			
			System.out.println("-----------------------------------Valid Flag:- "+validFlag+" and inValidCount:- "+inValidCount+"-------------------------------------------------------");
			if(!validFlag || inValidCount>0)
				sendMailOnNegativeQQ(jobOrder, wrongAnswerDetailsForDistrictSpecificQuestionList);
			
			return "1";

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;

	}
	
	public void sendMailOnNegativeQQ(JobOrder jobOrder, List<TeacherAnswerDetailsForDistrictSpecificQuestions> wrongAnswerDetailsForDistrictSpecificQuestionList)
	{
		JobOrder jb = jobOrderDAO.findById(jobOrder.getJobId(), false, false);
		DistrictMaster districtMaster = jb.getDistrictMaster();
		List<UserMaster> userMasterList=null;
		
		List<Integer> userIds = new ArrayList<Integer>();
		if(districtMaster!=null && districtMaster.getSendNotificationOnNegativeQQ()!=null)
			for(String id : districtMaster.getSendNotificationOnNegativeQQ().split("#"))
				if(!id.equals(""))
					userIds.add(Integer.parseInt(id));
		
		if(userIds.size()>0)
			userMasterList = userMasterDAO.getActiveDAUserByUserIds(userIds);
		
		
		//System.out.println("districtMaster.getSendNotificationOnNegativeQQ():- "+districtMaster.getSendNotificationOnNegativeQQ()+",districtId:- "+districtMaster.getDistrictId()+", userMasterList:- "+userMasterList);
		if(userMasterList!=null && userMasterList.size()>0)
		{
			for(UserMaster userMaster : userMasterList)
			{
				try
				{
					System.out.println("Start Negative Question Qualification Response");
					
					String sEmailBodyText = MailText.getNegativeQQMailText(userMaster,jobOrder,districtMaster,wrongAnswerDetailsForDistrictSpecificQuestionList);
					String sEmailSubject="Negative Question Qualification Response";
					
					DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
					dsmt.setEmailerService(emailerService);
					dsmt.setMailfrom("shadab.ansari@netsutra.com");
					dsmt.setMailto(userMaster.getEmailAddress());
					dsmt.setMailsubject(sEmailSubject);
					dsmt.setMailcontent(sEmailBodyText);
					
					System.out.println("sEmailSubject:- "+sEmailSubject);
					System.out.println("sEmailBodyText:- "+sEmailBodyText);
					
					try {
						dsmt.start();	
					} catch (Exception e) {}
					
		     	 	System.out.println("End Negative Question Qualification Response");
		     	 	//emailerService.sendMailAsHTMLText(userMaster.getEmailAddress(),"Negative Question Qualification Response", sEmailBodyText);
				}
				catch(Exception e){e.printStackTrace();}
			}
		}
	}
	
	/* ====== Gagan : Save Job For teacher using ajax============ */
	@Transactional
	public int saveJobForTeacher(String jobId)
	{
		System.out.println("***:::::::::::::::Assessment saveJobForTeacher:::::::::::::::::::;; ");

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);

		TeacherDetail teacherDetail = null;

		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		if (session == null || session.getAttribute("teacherDetail") == null) {
			//return "false";
		}else
			teacherDetail=(TeacherDetail)session.getAttribute("teacherDetail");

		Map<String, String> referer = (HashMap<String, String>) session.getAttribute("referer");
		String jobBoardReferralURL = session.getAttribute("jobBoardReferralURL")==null?"":(String)session.getAttribute("jobBoardReferralURL");
		if(referer!=null)
		{			
			jobId = referer.get("jobId");
			String coverLetter = (String)(session.getAttribute("coverLetter")==null?"":session.getAttribute("coverLetter"));
			String isAffilated =(String)(session.getAttribute("isAffilated")==null?"0":session.getAttribute("isAffilated")); 
			String staffType =(String)(session.getAttribute("staffType")==null?"N":session.getAttribute("staffType"));
			JobOrder jobOrder = jobOrderDAO.findById(new Integer(jobId), false, false); 

			JobForTeacher jobForTeacherObj = jobForTeacherDAO.findJobByTeacherAndJob(teacherDetail, jobOrder);

			try{
				DistrictMaster districtMaster = jobOrder.getDistrictMaster();
				List<InternalTransferCandidates> itcList=internalTransferCandidatesDAO.getInternalCandidateByDistrict(teacherDetail,districtMaster);
				if(itcList.size()>0){
					isAffilated="1";
				}
			}catch(Exception e){
				e.printStackTrace();
			}

			boolean completeFlag=false;
			if(jobForTeacherObj==null)
			{
				//TeacherPortfolioStatus teacherPortfolioStatus = teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);

				JobForTeacher jobForTeacher = new JobForTeacher();
				jobForTeacher.setTeacherId(teacherDetail);
				jobForTeacher.setJobId(jobOrder);
				
				if(jobOrder.getDistrictMaster()!=null)
				jobForTeacher.setDistrictId(jobOrder.getDistrictMaster().getDistrictId());
				
				jobForTeacher.setCoverLetter(coverLetter);
				jobForTeacher.setIsAffilated(Integer.parseInt(isAffilated));
				jobForTeacher.setCreatedDateTime(new Date());
				jobForTeacher.setStaffType(staffType);
				StatusMaster statusMaster = null;
				statusMaster = WorkThreadServlet.statusMap.get("icomp");
				int[] flagList=assessmentDetailDAO.getInternalTeacherFalgs(jobForTeacher) ;
				statusMaster = assessmentDetailDAO.findByTeacherIdJobStaus(jobForTeacher,flagList);
				TeacherAssessmentStatus teacherAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentStatusByTeacherForBase(jobForTeacher.getTeacherId());
				try{
					if(statusMaster.getStatusShortName().equalsIgnoreCase("comp")){
						completeFlag=true;
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				try
				{
					String re = referer.get("refererURL");
					jobForTeacher.setRedirectedFromURL(re);
					jobForTeacher.setJobBoardReferralURL(jobBoardReferralURL);
					//jobForTeacher.setRedirectedFromURL("refererURL");//referer.get("refererURL")
					if(jobForTeacher.getInternalSecondaryStatus()!=null){
						jobForTeacher.setStatus(jobForTeacher.getInternalStatus());
						jobForTeacher.setStatusMaster(null);
						jobForTeacher.setSecondaryStatus(jobForTeacher.getInternalSecondaryStatus());
					}else if(jobForTeacher.getInternalStatus()!=null){
						jobForTeacher.setStatus(jobForTeacher.getInternalStatus());
						jobForTeacher.setStatusMaster(jobForTeacher.getInternalStatus());
					}else{
						jobForTeacher.setStatus(statusMaster);
						if(jobForTeacher.getSecondaryStatus()!=null){
							jobForTeacher.setStatusMaster(null);
						}else{
							jobForTeacher.setStatusMaster(statusMaster);
						}
					}
					if(session.getAttribute("jobValid"+jobOrder.getJobId())!=null)
					{
						if((Boolean)session.getAttribute("jobValid"+jobOrder.getJobId())==true)
							jobForTeacher.setFlagForDistrictSpecificQuestions(true);
						else
							jobForTeacher.setFlagForDistrictSpecificQuestions(false);
						session.removeAttribute("jobValid"+jobOrder.getJobId());
					}

					PrintOnConsole.debugPrintln("saveJobForTeacher ... Saved JFT");
					if (session != null && session.getAttribute("isCurrentEmploymentNeeded") != null) 
					{
						boolean isCurrentEmploymentNeeded=false;
						isCurrentEmploymentNeeded=(Boolean)session.getAttribute("isCurrentEmploymentNeeded");
						PrintOnConsole.debugPrintln("saveJobForTeacher isCurrentEmploymentNeeded "+isCurrentEmploymentNeeded);
						if(isCurrentEmploymentNeeded)
						{
							TeacherPersonalInfo teacherPersonalInfo=teacherPersonalInfoDAO.findTeacherPersonalInfoByTeacher(teacherDetail);
							if(teacherPersonalInfo!=null && teacherPersonalInfo.getEmployeeType()!=null && teacherPersonalInfo.getEmployeeType()==1)
								jobForTeacher.setIsAffilated(1);
							else
								jobForTeacher.setIsAffilated(0);
						}
						session.removeAttribute("isCurrentEmploymentNeeded");
					}
					/*SessionFactory sessionFactory = jobForTeacherDAO.getSessionFactory();
					StatelessSession statelesSsession = sessionFactory.openStatelessSession();
					Transaction txOpen =statelesSsession.beginTransaction();*/
					jobForTeacherDAO.makePersistent(jobForTeacher);
					/*statelesSsession.insert(jobForTeacher);
					txOpen.commit();
					statelesSsession.close();*/
					try{
						Boolean spPass = false;
						if(jobOrder.getHeadQuarterMaster()!=null && jobOrder.getJobCategoryMaster().getPreHireSmartPractices()!=null && jobOrder.getJobCategoryMaster().getPreHireSmartPractices()){
							try {
								List<TeacherAssessmentStatus> teacherStatus = teacherAssessmentStatusDAO.findAssessmentStatusByTeacher(teacherDetail);
								if(teacherStatus!=null && teacherStatus.size()>0){
									for(TeacherAssessmentStatus st : teacherStatus){
										if(st.getAssessmentType()==3 && st.getPass().equalsIgnoreCase("p")){
											spPass = true;
											break;
										}
									}
								}
							} catch (Exception e) {
								e.printStackTrace();
							}
							if(spPass){
								commonService.autoStatusForSPJobs(jobOrder,teacherDetail,jobForTeacher);
							}
						}
						else{
							if(completeFlag && jobForTeacher.getStatus()!=null && jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("comp")){
								commonService.futureJobStatus(jobOrder, teacherDetail,jobForTeacher);
							}
						}
					}catch(Exception e){
						e.printStackTrace();
					}
					/* ======== Gagan : [ Start ] Functinality for to Send Email to all active DA\SA Admins acording to Job order, who has checked job apply CHECKBOX in notification [ Available ] checkbox ============== */
					List<UserMaster> lstusermaster	=	new ArrayList<UserMaster>();

					try{
						/* ===== Gagan : statusJobApply is used to get status short name against Apply which is used in Email sending to all active DA\SA Active Admins who has checked thier job apply CHECKBOX in notification [ Available ]======= */
						StatusMaster statusJobApply = WorkThreadServlet.statusMap.get("apl");
						String statusShortName = statusJobApply.getStatusShortName();
						boolean isSchool=false;
						if(jobOrder.getSelectedSchoolsInDistrict()!=null){
							if(jobOrder.getSelectedSchoolsInDistrict()==1){
								isSchool=true;
							}
						}
						if(jobOrder.getCreatedForEntity()==2 && isSchool==false){
							lstusermaster=userEmailNotificationsDAO.getUserByDistrict(jobOrder.getDistrictMaster(),statusShortName);
						}else if(jobOrder.getCreatedForEntity()==2 && isSchool){
							List<SchoolMaster>  lstschoolMaster= new ArrayList<SchoolMaster>();
							if(jobOrder.getSchool()!=null){
								if(jobOrder.getSchool().size()!=0){
									for(SchoolMaster sMaster : jobOrder.getSchool()){
										lstschoolMaster.add(sMaster);
									}
								}
							}
							lstusermaster=userEmailNotificationsDAO.getUserByDistrictAndSchoolList(jobOrder.getDistrictMaster(),lstschoolMaster,jobOrder,statusShortName);
						}else if(jobOrder.getCreatedForEntity()==3){
							lstusermaster=userEmailNotificationsDAO.getUserByOnlySchool(jobOrder.getSchool().get(0),statusShortName);
						}
					}catch(Exception e){
						e.printStackTrace();
					}



					TeacherAssessmentStatus _teacherAssessmentStatus =null;
					StatusMaster _statusMaster =	null;

					if(jobForTeacher.getJobId().getJobCategoryMaster().getBaseStatus())
					{
						List<TeacherAssessmentStatus> _teacherAssessmentStatusList = null;
						JobOrder _jOrder = new JobOrder();
						_jOrder.setJobId(0);
						_teacherAssessmentStatusList = teacherAssessmentStatusDAO.findAssessmentTaken(jobForTeacher.getTeacherId(),_jOrder);
						if(_teacherAssessmentStatusList.size()>0){
							_teacherAssessmentStatus = _teacherAssessmentStatusList.get(0);
							_statusMaster = teacherAssessmentStatus.getStatusMaster();
						}else{
							_statusMaster = WorkThreadServlet.statusMap.get("icomp");
						}
					}

					/*======= Gagan[ END ] : Check for  Base status for that job which against EPI required ==================*/

					String[] arrHrDetail = commonService.getHrDetailToTeacher(request,jobForTeacher.getJobId());
					arrHrDetail[10]	=	Utility.getBaseURL(request)+"signin.do";
					arrHrDetail[11] =	Utility.getValueOfPropByKey("basePath")+"/candidategrid.do?jobId="+jobForTeacher.getJobId().getJobId()+"&JobOrderType="+jobForTeacher.getJobId().getCreatedForEntity();
					if(jobForTeacher.getJobId().getDistrictMaster()!=null)
						arrHrDetail[12] =	Utility.getShortURL(Utility.getBaseURL(request)+"jobsboard.do?districtId="+Utility.encryptNo(jobForTeacher.getJobId().getDistrictMaster().getDistrictId()));
					else
						arrHrDetail[12] ="";
					arrHrDetail[13] =	Utility.getValueOfPropByKey("basePath")+"/forgotpassword.do";
					try {
						if(_statusMaster.getStatusShortName()!=null)
							arrHrDetail[14] =	_statusMaster.getStatusShortName();
					} catch (NullPointerException e) {
						//e.printStackTrace();
					}
					String flagforEmail	="applyJob";
					if(jobForTeacher!=null && jobForTeacher.getJobId().getDistrictMaster()!=null 
							&& jobForTeacher.getJobId().getDistrictMaster().getHeadQuarterMaster()!=null 
							&& jobForTeacher.getJobId().getDistrictMaster().getHeadQuarterMaster().getHeadQuarterId()!=null 
							&& jobForTeacher.getJobId().getDistrictMaster().getHeadQuarterMaster().getHeadQuarterId().equals(2))
					{
						if(jobForTeacher.getJobId().getDistrictMaster().getJobCompletionEmail() 
								&& jobForTeacher.getStatus()!=null && jobForTeacher.getStatus().getStatusId()!=null
								&& jobForTeacher.getStatus().getStatusId()==4 && (jobForTeacher.getSendMail()==null || !jobForTeacher.getSendMail()))
						{
							System.out.println("-----------------------AssessmentCampaignAjax------------------");
							MailSendToTeacher mst =new MailSendToTeacher(teacherDetail,jobOrder,request,arrHrDetail,flagforEmail,lstusermaster,emailerService,jobForTeacher);
							Thread currentThread = new Thread(mst);
							currentThread.start();
							jobForTeacher.setSendMail(true);
							jobForTeacherDAO.makePersistent(jobForTeacher);
						}
					}
					
					else
					{
						MailSendToTeacher mst =new MailSendToTeacher(teacherDetail,jobOrder,request,arrHrDetail,flagforEmail,lstusermaster,emailerService,jobForTeacher);
						Thread currentThread = new Thread(mst);
						currentThread.start(); 

					}
					
				}catch(Exception ex)
				{
					ex.printStackTrace();
					return 0;
				}
				//return 1;

			}
		}
		return 1;
	}

	@Transactional(readOnly=false)
	public String[] checkTeacherCriteria(JobOrder jobOrder)
	{
		//System.out.println(":::::::::::::::Assessment checkTeacherCriteria:::::::::::::::::::;; ");

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		try {

			TeacherDetail teacherDetail=(TeacherDetail)session.getAttribute("teacherDetail");
			JobForTeacher jft = jobForTeacherDAO.findJFTByTeacherAndJob(teacherDetail, jobOrder);
			String candidateType="E";
			if(jft.getIsAffilated()!=null && jft.getIsAffilated()==1)
				candidateType="I";
			boolean getQQ=false;
			boolean dspqFlag=false;
			DistrictPortfolioConfig districtPortfolioConfig = districtPortfolioConfigAjax.getPortfolioConfigByJobId(jobOrder.getJobId(),candidateType);
			if(districtPortfolioConfig!=null){
				dspqFlag = true;
			}

			try{
				jobOrder = jobOrderDAO.findById(jobOrder.getJobId(), false, false); 
				List<DistrictSpecificQuestions> districtSpecificQuestionList=districtSpecificQuestionsDAO.getDistrictSpecificQuestionBYDistrict(jobOrder.getDistrictMaster());
				if(districtSpecificQuestionList.size()>0)
				{
					getQQ=true;
				}
			}catch(Exception e){
				e.printStackTrace();
			}

			jobOrder = jft.getJobId();
			boolean isMiami = false;
			if(jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId().equals(1200390))
				isMiami=true;

			List<JobOrder> jobOrders = teacherAnswerDetailsForDistrictSpecificQuestionsDAO.findLastTeacherAnswersByDistrictAndJob(teacherDetail, jobOrder);
			String [] qqStr = new String[5]; 
			boolean qqFlag=false;
			if(jobOrders.size()==0)
				qqFlag = true;

			///***** Sekhar Add TranseferCandidate ******///
			DistrictMaster districtMaster=null;
			if(jobOrder.getDistrictMaster()!=null){
				districtMaster=jobOrder.getDistrictMaster();
			}
			List<InternalTransferCandidates> itcList=internalTransferCandidatesDAO.getInternalCandidateByDistrict(teacherDetail,districtMaster);
			boolean internalFlag=false;
			if(itcList.size()>0){
				internalFlag=true;
			}
			if(districtMaster!=null && internalFlag){
				if(districtMaster.getOfferDistrictSpecificItems()){
					dspqFlag=true;
				}else{
					dspqFlag=false;
				}
				if(districtMaster.getOfferQualificationItems()){
					getQQ=true;
				}else{
					getQQ=false;
				}
			}
			if(candidateType.equalsIgnoreCase("I") && internalFlag==false){
				if(jobOrder.getJobCategoryMaster()!=null && jobOrder.getJobCategoryMaster().getOfferDistrictSpecificItems()){
					dspqFlag=true;
				}else{
					dspqFlag=false;
				}
				if(jobOrder.getJobCategoryMaster()!=null && jobOrder.getJobCategoryMaster().getOfferQualificationItems()){
					getQQ=true;
				}else{
					getQQ=false;
				}
			}
			///////////***** Sekhar End *********//////////
			qqStr[0]=""+qqFlag;
			qqStr[1]=""+dspqFlag;
			qqStr[2]=""+jft.getIsAffilated();
			qqStr[3]=""+getQQ;
			qqStr[4]=""+isMiami;
			return qqStr;

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	// Author Sonu Gupta	
	public String setQQDistrictQuestions(TeacherAnswerDetailsForDistrictSpecificQuestions[] answerDetailsForDistrictSpecificQuestions)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		List<UserMaster> userMastersList=new ArrayList<UserMaster>();
		TeacherDetail teacherDetail = null;
		if (session == null || session.getAttribute("teacherDetail") == null) {
			//return "false";
		}else
			teacherDetail=(TeacherDetail)session.getAttribute("teacherDetail");

		try {
			StringBuilder sb=new StringBuilder();
			int inValidCount = 0;
			List<TeacherAnswerDetailsForDistrictSpecificQuestions> wrongAnswerDetailsForDistrictSpecificQuestionList = new ArrayList<TeacherAnswerDetailsForDistrictSpecificQuestions>();
			JobOrder jobOrder = answerDetailsForDistrictSpecificQuestions[0].getJobOrder();
			jobOrder = jobOrderDAO.findById(jobOrder.getJobId(), false, false);
			System.out.println("jobOrder.getJobId()::"+jobOrder.getJobId());
			JobCategoryMaster jobCategoryMaster = jobOrder.getJobCategoryMaster();
			QqQuestionSets qqQuestionSets = new QqQuestionSets();
			if(jobCategoryMaster!=null && jobCategoryMaster.getQuestionSets()!=null){
				qqQuestionSets = jobCategoryMaster.getQuestionSets();
			 
			}
			List<TeacherAnswerDetailsForDistrictSpecificQuestions> teacherAnswerDetailsForDistrictSpecificQuestionsLists = null;
			 teacherAnswerDetailsForDistrictSpecificQuestionsLists=teacherAnswerDetailsForDistrictSpecificQuestionsDAO.findTeacherAnswersByDistrict(teacherDetail,jobOrder.getDistrictMaster());
			 Map<Integer, TeacherAnswerDetailsForDistrictSpecificQuestions> mapAnswerCompair = new HashMap<Integer,TeacherAnswerDetailsForDistrictSpecificQuestions>();
				
			 for (TeacherAnswerDetailsForDistrictSpecificQuestions teacherAnswerDetailsForDistrictSpecificQuestions : teacherAnswerDetailsForDistrictSpecificQuestionsLists) {
				 mapAnswerCompair.put(teacherAnswerDetailsForDistrictSpecificQuestions.getDistrictSpecificQuestions().getQuestionId(), teacherAnswerDetailsForDistrictSpecificQuestions);
			}
			 TeacherAnswerHistory teacherAnswerHistory=new TeacherAnswerHistory();

			 Map<Integer,Integer> map = new HashMap<Integer, Integer>();

			try {
				map = teacherAnswerDetailsForDistrictSpecificQuestionsDAO.findTeacherAnswersForQQByDistrictSet(teacherDetail,qqQuestionSets);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			List<Integer> answers = new ArrayList<Integer>();
			Integer ansId = null;
			Boolean sendMailFlag=false;
			Boolean QQFlag=false;
			try {
				SessionFactory sessionFactory = teacherAnswerHistoryDAO.getSessionFactory();
				StatelessSession statelessSession=sessionFactory.openStatelessSession();
				Transaction transaction=statelessSession.beginTransaction();
				
				List<TeacherAnswerDetailsForDistrictSpecificQuestions> wrongAnsListInFinalizedQQ = new ArrayList<TeacherAnswerDetailsForDistrictSpecificQuestions>();
				List<JobForTeacher> lstJobForTeacher= null;
				lstJobForTeacher=new ArrayList<JobForTeacher>();
				//lstJobForTeacher = jobForTeacherDAO.findByTeacherDistrictAndJobCategory(teacherDetail, jobOrder.getDistrictMaster(), jobOrder.getJobCategoryMaster());
				lstJobForTeacher = jobForTeacherDAO.findByTeacherAndQuestionSet(teacherDetail,jobOrder.getJobCategoryMaster().getQuestionSets());
                System.out.println("lstJobForTeacher Size Ctaegory Wise"+lstJobForTeacher.size());
                try{
	                if(lstJobForTeacher.size()>0){
	                	if(lstJobForTeacher.get(0).getIsDistrictSpecificNoteFinalize()!=null && lstJobForTeacher.get(0).getIsDistrictSpecificNoteFinalize()==true){
	                		List<TeacherAnswerDetailsForDistrictSpecificQuestions> list=new ArrayList<TeacherAnswerDetailsForDistrictSpecificQuestions>();        				
	        				list=teacherAnswerDetailsForDistrictSpecificQuestionsDAO.findTeacherAnswersByDistrictTeacherAndQuestionSet(teacherDetail,jobOrder.getDistrictMaster(),jobOrder.getJobCategoryMaster().getQuestionSets());
	        				System.out.println("AllQuestionListSize-->"+list.size());
	        				if(list.size()>0){
	        				for(TeacherAnswerDetailsForDistrictSpecificQuestions tadfdsq : list){
	        					if(tadfdsq.getIsValidAnswer()!=null && tadfdsq.getIsValidAnswer()==false){
	        						wrongAnsListInFinalizedQQ.add(tadfdsq);
	        						QQFlag=true;
	        					}
	        				}
	        			  }
	                	}
	                }
                }catch(Exception e){
                	e.printStackTrace();
                }
                System.out.println("wrongAnsListInFinalizedQQ Size:"+wrongAnsListInFinalizedQQ.size());
				
				for (TeacherAnswerDetailsForDistrictSpecificQuestions answerDetailsForDistrictSpecificQuestion : answerDetailsForDistrictSpecificQuestions) {
					answerDetailsForDistrictSpecificQuestion.setTeacherDetail(teacherDetail);
					answerDetailsForDistrictSpecificQuestion.setDistrictMaster(jobOrder.getDistrictMaster());
					answerDetailsForDistrictSpecificQuestion.setCreatedDateTime(new Date());						

					ansId = map.get(answerDetailsForDistrictSpecificQuestion.getDistrictSpecificQuestions().getQuestionId());					
					if(ansId!=null)
					{
						answerDetailsForDistrictSpecificQuestion.setAnswerId(ansId);
						teacherAnswerHistory.setAnswerId(ansId);
					}
					
					if(!answerDetailsForDistrictSpecificQuestion.getQuestionType().equalsIgnoreCase("ml"))
					{
						if(answerDetailsForDistrictSpecificQuestion.getIsValidAnswer()==false)
						{
							inValidCount++;
							wrongAnswerDetailsForDistrictSpecificQuestionList.add(answerDetailsForDistrictSpecificQuestion);
						}
					}
					
					answerDetailsForDistrictSpecificQuestion.setIsActive(true);					
					teacherAnswerDetailsForDistrictSpecificQuestionsDAO.makePersistent(answerDetailsForDistrictSpecificQuestion);
					answers.add(answerDetailsForDistrictSpecificQuestion.getAnswerId());
					
					TeacherAnswerDetailsForDistrictSpecificQuestions oldAns =mapAnswerCompair.get(answerDetailsForDistrictSpecificQuestion.getDistrictSpecificQuestions().getQuestionId());
					if(oldAns!=null)
					{
						if(!answerDetailsForDistrictSpecificQuestion.getQuestionOption().equalsIgnoreCase(oldAns.getQuestionOption()) || (answerDetailsForDistrictSpecificQuestion!=null && oldAns!=null &&  answerDetailsForDistrictSpecificQuestion.getInsertedText()!=null && oldAns.getInsertedText()!=null && !answerDetailsForDistrictSpecificQuestion.getInsertedText().equalsIgnoreCase(oldAns.getInsertedText()) ))
						{
							System.out.println("Question Not Match "+oldAns);
							
							teacherAnswerHistory.setIsValidAnswer(answerDetailsForDistrictSpecificQuestion.getIsValidAnswer());
							teacherAnswerHistory.setInsertedText(answerDetailsForDistrictSpecificQuestion.getInsertedText());
							teacherAnswerHistory.setQuestionOption(answerDetailsForDistrictSpecificQuestion.getQuestionOption());
							teacherAnswerHistory.setSelectedOptions(answerDetailsForDistrictSpecificQuestion.getSelectedOptions());
							teacherAnswerHistory.setQuestionType(answerDetailsForDistrictSpecificQuestion.getQuestionType());
							teacherAnswerHistory.setQuestionTypeMaster(answerDetailsForDistrictSpecificQuestion.getQuestionTypeMaster());
							teacherAnswerHistory.setQuestion(answerDetailsForDistrictSpecificQuestion.getQuestion());
							teacherAnswerHistory.setDistrictSpecificQuestions(answerDetailsForDistrictSpecificQuestion.getDistrictSpecificQuestions());
							teacherAnswerHistory.setJobOrder(jobOrder);
							teacherAnswerHistory.setTeacherDetail(teacherDetail);
							teacherAnswerHistory.setDistrictMaster(jobOrder.getDistrictMaster());
							teacherAnswerHistory.setTypeId(1);
							teacherAnswerHistory.setCreatedDateTime(new Date());
							teacherAnswerHistory.setIsActive(true);							
							statelessSession.insert(teacherAnswerHistory);
							transaction=statelessSession.beginTransaction();
							transaction.commit();
							sendMailFlag=true;
							String  oldText="";
							String  newText="";
							if(oldAns.getInsertedText()==null)
								oldText="";
							else
								oldText=oldAns.getInsertedText();
							if(answerDetailsForDistrictSpecificQuestion.getInsertedText()==null)
								newText="";
							else
								newText=answerDetailsForDistrictSpecificQuestion.getInsertedText();
							sb.append("Changed the answer for Question "+'"'+""+answerDetailsForDistrictSpecificQuestion.getQuestion()+""+'"'+" from "+oldAns.getQuestionOption()+" "+ oldText +" to "+answerDetailsForDistrictSpecificQuestion.getQuestionOption()+ " "+newText+ ".<br><br>");
							
						}
					}
					
				}
				//ravindra qq update
				List<TeacherAnswerDetailsForDistrictSpecificQuestions> wrongAnswerDetailsForDistrictSpecificQuestionList1 = null; 
				wrongAnswerDetailsForDistrictSpecificQuestionList1 = new ArrayList<TeacherAnswerDetailsForDistrictSpecificQuestions>();				
				if(wrongAnswerDetailsForDistrictSpecificQuestionList.size()>0 && wrongAnsListInFinalizedQQ.size()>0){
					wrongAnswerDetailsForDistrictSpecificQuestionList1.addAll(wrongAnswerDetailsForDistrictSpecificQuestionList);
					System.out.println("wrongAnswerDetailsForDistrictSpecificQuestionList1 after addAll-->"+wrongAnswerDetailsForDistrictSpecificQuestionList1.size());
					wrongAnswerDetailsForDistrictSpecificQuestionList1.removeAll(wrongAnsListInFinalizedQQ);
				}
				System.out.println("wrongAnswerDetailsForDistrictSpecificQuestionList1 after Remove-->"+wrongAnswerDetailsForDistrictSpecificQuestionList1.size());
				boolean validFlag = false;
				if(inValidCount==0)
					validFlag=true;
				
				if(lstJobForTeacher.size()>0)
				{			
						for(JobForTeacher jft:lstJobForTeacher)
						{
							if(jft.getIsDistrictSpecificNoteFinalize()!=null && jft.getIsDistrictSpecificNoteFinalize()){

								if(validFlag==true)
								{
									jft.setFlagForDistrictSpecificQuestions(validFlag);
									jft.setIsDistrictSpecificNoteFinalize(false);
								}
								else
								{																	
									if(QQFlag==true && wrongAnswerDetailsForDistrictSpecificQuestionList1.size()==0)
									{
										jft.setFlagForDistrictSpecificQuestions(true);	
									}
									else
									{
										jft.setFlagForDistrictSpecificQuestions(validFlag);
										jft.setIsDistrictSpecificNoteFinalize(validFlag);
									}

								}
							}
							else
							{
								jft.setFlagForDistrictSpecificQuestions(validFlag);								
							}
							jobForTeacherDAO.makePersistent(jft);
						}				
				}			
//				transaction=statelessSession.beginTransaction();
//				transaction.commit();
				statelessSession.close();
				sendMailFlag=false; //Temporary Add for stop mail
				if(sendMailFlag==true)					
				{
					MailText mail=new MailText();
					String subject = "Applicant "+teacherDetail.getFirstName()+" "+teacherDetail.getLastName()+ "has updated the Qualification Question";
					userMastersList=userMasterDAO.getUserByDistrict(jobOrder.getDistrictMaster());
					for(UserMaster userMaster: userMastersList)
					{
						String contents=mail.getUpdateQQMailForDistrict(userMaster, teacherDetail,sb.toString());
						System.out.println(">>>>>>>>>>>>>>>>>>>>>>>userMastersList size  :: "+ userMastersList.size());
						DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
						dsmt.setEmailerService(emailerService);
						dsmt.setMailfrom(Utility.getValueOfSmtpPropByKey("smtphost.adminusername"));					
						dsmt.setMailto(userMaster.getEmailAddress());						
						dsmt.setMailsubject(subject);
						dsmt.setMailcontent(contents);
						System.out.println("content  "+contents);
						try {
							dsmt.start();	
						} catch (Exception e) {}
					}
				}
				System.out.println("<----------------------------------inValidCount:- "+inValidCount+"------------------------------------------------------->");
				if(inValidCount>0)
					sendMailOnNegativeQQ(jobOrder, wrongAnswerDetailsForDistrictSpecificQuestionList);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			/*if(answers.size()>0)
				teacherAnswerDetailsForDistrictSpecificQuestionsDAO.updateOtherQQ(teacherDetail,jobOrder.getDistrictMaster(),answers);*/

		return "1";
		}
		catch (Exception e) {		
		}
		return null;
	}
		// Author Sonu Gupta
	@Transactional(readOnly=false)
	public String getQQDistrictSpecificQuestion(Integer jobId)
	{
		//System.out.println("*****:::::::::::::::Assessment getDistrictSpecificQuestion:::::::::::::::::::;; ");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		StringBuffer sb =new StringBuffer();
		try{
			List<DistrictSpecificQuestions> districtSpecificQuestionList	=	null; 
			TeacherDetail teacherDetail = null;

			if (session == null || session.getAttribute("teacherDetail") == null) {
				//return "false";
			}else
				teacherDetail=(TeacherDetail)session.getAttribute("teacherDetail");

			if(jobId!=0)
			{
				JobOrder jobOrder=jobOrderDAO.findById(jobId, false, false);
				JobCategoryMaster jobCategoryMaster = jobOrder.getJobCategoryMaster();
				QqQuestionSets qqQuestionSets = new QqQuestionSets();
				List<QqQuestionsetQuestions> qqQuestionsetQuestions=null;
				if(jobCategoryMaster!=null && jobCategoryMaster.getQuestionSets()!=null){
					qqQuestionSets = jobCategoryMaster.getQuestionSets();
					qqQuestionsetQuestions = qqQuestionsetQuestionsDAO.findByQuestionSet(jobCategoryMaster.getQuestionSets()); 
				}
				List<Integer> tempDistSpecQues = new ArrayList<Integer>();
				if(qqQuestionsetQuestions!=null && qqQuestionsetQuestions.size()>0){
					for (QqQuestionsetQuestions qqQuestionsetQuestions2 : qqQuestionsetQuestions) {
						tempDistSpecQues.add(qqQuestionsetQuestions2.getDistrictSpecificQuestions().getQuestionId());
					}
				}
				JobForTeacher jft = jobForTeacherDAO.findJFTByTeacherAndJob(teacherDetail, jobOrder);
				//districtSpecificQuestionList=districtSpecificQuestionsDAO.getDistrictSpecificQuestionBYDistrict(jobOrder.getDistrictMaster());
				System.out.println(">.>.> 1");
				if(tempDistSpecQues.size()>0)
				districtSpecificQuestionList=districtSpecificQuestionsDAO.getDistrictSpecificQuestionBYQues(tempDistSpecQues);
				System.out.println(">.>.> 2");
				List<JobOrder> jobOrders = teacherAnswerDetailsForDistrictSpecificQuestionsDAO.findLastTeacherAnswersByDistrictAndJob(teacherDetail,jobOrder);
				System.out.println(">.>.> 3");
				List<TeacherAnswerDetailsForDistrictSpecificQuestions> lastList = new ArrayList<TeacherAnswerDetailsForDistrictSpecificQuestions>();
				System.out.println(">.>.> 4");
				if(jobOrders.size()>0)
					lastList = teacherAnswerDetailsForDistrictSpecificQuestionsDAO.findTeacherAnswersBySetQues(teacherDetail,qqQuestionSets);

				Map<Integer, TeacherAnswerDetailsForDistrictSpecificQuestions> map = new HashMap<Integer, TeacherAnswerDetailsForDistrictSpecificQuestions>();

				for(TeacherAnswerDetailsForDistrictSpecificQuestions teacherAnswerDetailsForDistrictSpecificQuestion : lastList)
					map.put(teacherAnswerDetailsForDistrictSpecificQuestion.getDistrictSpecificQuestions().getQuestionId(), teacherAnswerDetailsForDistrictSpecificQuestion);

				TeacherAnswerDetailsForDistrictSpecificQuestions teacherAnswerDetailsForDistrictSpecificQuestion = null;
				List<OptionsForDistrictSpecificQuestions> questionOptionsList = null;
				String questionInstruction = null;
				String shortName = "";
				int totalQuestions =0;
				if(tempDistSpecQues.size()>0)
					totalQuestions = districtSpecificQuestionList.size();
				int cnt = 0;

				DistrictMaster districtMaster=null;
				if(jobOrder.getDistrictMaster()!=null){
					districtMaster=jobOrder.getDistrictMaster();
				}
				List<InternalTransferCandidates> itcList=internalTransferCandidatesDAO.getInternalCandidateByDistrict(teacherDetail,districtMaster);
				boolean internalFlag=false;
				boolean getQQ=true;
				if(itcList.size()>0){
					internalFlag=true;
				}
				if(districtMaster!=null && internalFlag){
					if(districtMaster.getOfferQualificationItems()){
						getQQ=true;
					}else{
						getQQ=false;
					}
				}				
				boolean focus = false;
				if(getQQ)
					for (DistrictSpecificQuestions districtSpecificQuestion : districtSpecificQuestionList) 
					{
						shortName = districtSpecificQuestion.getQuestionTypeMaster().getQuestionTypeShortName();

						sb.append("<tr>");
						sb.append("<td width='90%'>");
						sb.append("<div><b>Question "+(++cnt)+" of "+totalQuestions+"</b></div>");

						questionInstruction = districtSpecificQuestion.getQuestionInstructions()==null?"":districtSpecificQuestion.getQuestionInstructions();
						if(!questionInstruction.equals(""))
							sb.append(""+Utility.getUTFToHTML(questionInstruction)+"");

						sb.append("<div id='Q"+cnt+"question'>"+Utility.getUTFToHTML(districtSpecificQuestion.getQuestion())+"</div>");

						questionOptionsList = districtSpecificQuestion.getQuestionOptions();

						teacherAnswerDetailsForDistrictSpecificQuestion = map.get(districtSpecificQuestion.getQuestionId());

						String checked = "";
						Integer optId = 0;
						String insertedText = "";
						if(teacherAnswerDetailsForDistrictSpecificQuestion!=null)
						{
							if(shortName.equalsIgnoreCase(teacherAnswerDetailsForDistrictSpecificQuestion.getQuestionType()))
							{
								if(teacherAnswerDetailsForDistrictSpecificQuestion.getSelectedOptions()!=null)
									optId = Integer.parseInt(teacherAnswerDetailsForDistrictSpecificQuestion.getSelectedOptions());

								insertedText = teacherAnswerDetailsForDistrictSpecificQuestion.getInsertedText();
							}
						}
						if(shortName.equalsIgnoreCase("tf") || shortName.equalsIgnoreCase("slsel"))
						{
							sb.append("<table>");
							for (OptionsForDistrictSpecificQuestions questionOptions : questionOptionsList) {
								if(optId.equals(questionOptions.getOptionId()))
									checked = "checked";
								else
									checked = "";

								sb.append("<tr><td style='border:0px;background-color: transparent;'><div  style='display: block;min-height: 20px;padding-left: 5px;vertical-align: middle;'><input type='radio' name='Q"+cnt+"opt' value='"+questionOptions.getOptionId()+"' "+checked+" /></div>" +
										" <input type='hidden' id='Q"+questionOptions.getOptionId()+"validQuestion' value='"+questionOptions.getValidOption()+"' ></td>" +
										"<td style='border:0px;background-color: transparent;padding-left:5px;' id='qOpt"+questionOptions.getOptionId()+"'>"+questionOptions.getQuestionOption()+" ");
								sb.append("</td></tr>");
							}
							sb.append("</table>");
						}
						if(shortName.equalsIgnoreCase("et"))
						{
							sb.append("<table>");
							for (OptionsForDistrictSpecificQuestions questionOptions : questionOptionsList) {
								if(optId.equals(questionOptions.getOptionId()))
									checked = "checked";
								else
									checked = "";

								sb.append("<tr><td style='border:0px;background-color: transparent;'><div  style='display: block;min-height: 20px;padding-left: 5px;vertical-align: middle;'><input type='radio' name='Q"+cnt+"opt' value='"+questionOptions.getOptionId()+"' "+checked+" /></div>" +
										" <input type='hidden' id='Q"+questionOptions.getOptionId()+"validQuestion' value='"+questionOptions.getValidOption()+"' ></td>" +
										"<td style='border:0px;background-color: transparent;padding-left:5px;'  id='qOpt"+questionOptions.getOptionId()+"'>"+questionOptions.getQuestionOption()+" ");
								sb.append("</td></tr>");
							}
							sb.append("</table>");

							if(districtSpecificQuestion.getQuestionExplanation()!=null)
								sb.append("<div>"+Utility.getUTFToHTML(districtSpecificQuestion.getQuestionExplanation())+"</div>");

							sb.append("&nbsp;&nbsp;<span style=''><textarea  name='Q"+cnt+"opt' id='Q"+cnt+"optet' class='form-control' maxlength='5000' rows='8' style='width:98%;margin:5px;'>"+insertedText+"</textarea><span><br/>");
							if(focus==false)
							{
								sb.append("<script type=\"text/javascript\" language=\"javascript\">");
								//sb.append("document.getElementById('Q"+cnt+"optet').focus();");
								sb.append("</script>");
								focus = true;
							}
							sb.append("<script type=\"text/javascript\" language=\"javascript\">");
							//sb.append("document.getElementById('Q"+cnt+"optet').focus();");
							sb.append("</script>");
						}
						if(shortName.equalsIgnoreCase("ml"))
						{
							sb.append("&nbsp;&nbsp;<textarea name='Q"+cnt+"opt' id='Q"+cnt+"opt' class='form-control' maxlength='5000' rows='8' style='width:98%;margin:5px;'>"+insertedText+"</textarea><br/>");					
							if(focus==false)
							{
								sb.append("<script type=\"text/javascript\" language=\"javascript\">");
								//sb.append("document.getElementById('Q"+cnt+"opt').focus();");
								sb.append("</script>");
								focus = true;
							}
							sb.append("<script type=\"text/javascript\" language=\"javascript\">");
							//sb.append("document.getElementById('Q"+cnt+"opt').focus();");
							sb.append("</script>");
						}
						sb.append("<input type='hidden' id='Q"+cnt+"questionId' value='"+districtSpecificQuestion.getQuestionId()+"'/>" );
						sb.append("<input type='hidden' id='Q"+cnt+"questionTypeId' value='"+districtSpecificQuestion.getQuestionTypeMaster().getQuestionTypeId()+"'/>" );
						sb.append("<input type='hidden' id='Q"+cnt+"questionTypeShortName' value='"+shortName+"'/>" );

						sb.append("</td>");
						sb.append("</tr>");
						insertedText = "";
					}
				if(jobCategoryMaster.getQuestionSets()!=null && !jobCategoryMaster.getQuestionSets().getID().equals(0))
					sb.append("<input type='hidden' id='qqQuestionSetID' value='"+jobCategoryMaster.getQuestionSets().getID()+"'/>" );
				else
					sb.append("<input type='hidden' id='qqQuestionSetID' value=''/>" );
				sb.append("@##@"+cnt+"@##@"+jobOrder.getDistrictMaster().getTextForDistrictSpecificQuestions());
			}
		}
		catch (Exception e) {
			// TODO: handle exception
		}
		return sb.toString();
	}
	public AssessmentDetail CheckJSI(JobOrder jobOrder)
	{		
	//System.out.println(":::::::::::::::Assessment checkInventory:::::::::::::::::::>>> ");
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	}

	TeacherDetail teacherDetail = null;

	if (session == null || session.getAttribute("teacherDetail") == null) {
		//return "false";
	}else
		teacherDetail=(TeacherDetail)session.getAttribute("teacherDetail");

	int newJobId = jobOrder.getJobId();
	if(jobOrder.getJobId()!=null && jobOrder.getJobId()>0)
		jobOrder = jobOrderDAO.findById(newJobId, false, false);

	AssessmentDetail assessmentDetail = new AssessmentDetail();
	List<TeacherAssessmentStatus> teacherAssessmentStatusList = null;
	TeacherAssessmentStatus teacherAssessmentStatus = null;
	teacherAssessmentStatusList=teacherAssessmentStatusDAO.findAssessmentTaken(teacherDetail,jobOrder);
	//////////////////////////////////////
	if(teacherAssessmentStatusList.size()==0 && newJobId!=0)
	{
		List<AssessmentJobRelation>	assessmentJobRelations1=assessmentJobRelationDAO.findRelationByJobOrder(jobOrder);
		if(assessmentJobRelations1.size()>0)
		{
			AssessmentJobRelation assessmentJobRelation1 = assessmentJobRelations1.get(0);
			teacherAssessmentStatusList = teacherAssessmentStatusDAO.findAssessmentTakenByTeacher(teacherDetail,assessmentJobRelation1.getAssessmentId());
		}
	}
	/////////////////////////////////////
	int oldJobId = 0;
	if(teacherAssessmentStatusList.size()!=0)
	{
		teacherAssessmentStatus = teacherAssessmentStatusList.get(0);
		assessmentDetail = teacherAssessmentStatus.getAssessmentDetail();
		String statusShortName=teacherAssessmentStatus.getStatusMaster().getStatusShortName();

		List<TeacherAssessmentStatus> teacherAssessmentStatusListIfexist = null;

		if(teacherAssessmentStatus.getAssessmentType()==2)
		{
			teacherAssessmentStatusListIfexist=teacherAssessmentStatusDAO.findAssessmentTakenByTeacher(teacherDetail,assessmentDetail);
			TeacherAssessmentStatus teacherAssessmentStatus1 = teacherAssessmentStatusListIfexist.get(0);
			oldJobId = teacherAssessmentStatus1.getJobOrder().getJobId();
			if(newJobId!=oldJobId)
			{
				jobOrder = teacherAssessmentStatus1.getJobOrder();
				//oldJobId = jobOrder.getJobId();
			}else
				oldJobId=0;

		}
		System.out.println("statusShortName "+ statusShortName);
		if(statusShortName.equalsIgnoreCase("comp"))
		{
			assessmentDetail.setAssessmentName(null);
			assessmentDetail.setStatus("3");
			return assessmentDetail;
		}else if(statusShortName.equalsIgnoreCase("exp"))
		{
			assessmentDetail.setAssessmentName(null);
			assessmentDetail.setStatus("4");
			return assessmentDetail;
		}
		else if(statusShortName.equalsIgnoreCase("vlt"))
		{
			assessmentDetail.setAssessmentName(null);
			assessmentDetail.setStatus("5");
			return assessmentDetail;
		}
		else if(statusShortName.equalsIgnoreCase("icomp"))
		{
			List<TeacherAssessmentAttempt> teacherAssessmentAttempts = null;
			teacherAssessmentAttempts = teacherAssessmentAttemptDAO.findAssessmentAttempts(teacherDetail,jobOrder);
			int attempts = 0;
			for (TeacherAssessmentAttempt teacherAssessmentAttempt1 : teacherAssessmentAttempts) {
				if(!teacherAssessmentAttempt1.getIsForced())
					attempts++;
			}
			////////////////////////

			int strikeLogs=0; 
			try{
				if(teacherAssessmentAttempts.size()>0)
				{
					List<TeacherAssessmentStatus> teacherAssessmentStatusList2 = null;
					teacherAssessmentStatusList2 = teacherAssessmentStatusDAO.getTeacherAssessmentStatus(teacherAssessmentAttempts.get(0).getAssessmentDetail(),teacherDetail);
					List<TeacherStrikeLog> teacherStrikeLog = null;
					teacherStrikeLog = teacherStrikeLogDAO.getTotalStrikesByTeacher(teacherDetail, teacherAssessmentStatusList2.get(0).getTeacherAssessmentdetail());
					strikeLogs=teacherStrikeLog.size();
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			int attemptsStrike=attempts+strikeLogs;

			if(attemptsStrike==3)
			{
				StatusMaster statusMaster = WorkThreadServlet.statusMap.get("vlt");
				TeacherAssessmentdetail teacherAssessmentdetail = teacherAssessmentStatus.getTeacherAssessmentdetail();

				List<TeacherAssessmentStatus> lstTeacherAssessmentStatus = null;
				lstTeacherAssessmentStatus =teacherAssessmentStatusDAO.findAssessmentStatusByTeacherAssessmentdetail(teacherAssessmentdetail);
				teacherAssessmentStatus=lstTeacherAssessmentStatus.get(0);

				List<JobOrder> jobs = new ArrayList<JobOrder>();

				if(lstTeacherAssessmentStatus.size()!=0)
					if(teacherAssessmentStatus!=null)
					{
						for(TeacherAssessmentStatus tas: lstTeacherAssessmentStatus)
						{
							jobs.add(tas.getJobOrder());

							tas.setStatusMaster(statusMaster);
							teacherAssessmentStatusDAO.makePersistent(tas);
						}
					}
				//teacherAssessmentStatus

				// JobForTeacher update after 3 attempts
				if(jobOrder.getJobId()!=null && jobOrder.getJobId()>0)
				{
					List<AssessmentJobRelation>	assessmentJobRelations1=assessmentJobRelationDAO.findRelationByAssessment(teacherAssessmentStatus.getAssessmentDetail());

					for (AssessmentJobRelation assessmentJobRelation : assessmentJobRelations1) {
						jobs.add(assessmentJobRelation.getJobId());
					}

					List<JobForTeacher> jobForTeachers = jobForTeacherDAO.findJobByTeacherAndSatusFormJobIds(teacherDetail, jobs);

					String jftStatus = "";
					for(JobForTeacher jft: jobForTeachers)
					{
						jftStatus = jft.getStatus().getStatusShortName();
						if(!(jftStatus.equalsIgnoreCase("hird") || jftStatus.equalsIgnoreCase("widrw") || jftStatus.equalsIgnoreCase("hide")))
						{
							if(jft.getInternalSecondaryStatus()!=null){
								jft.setStatus(jft.getInternalStatus());
								jft.setStatusMaster(null);
								jft.setSecondaryStatus(jft.getInternalSecondaryStatus());
							}else if(jft.getInternalStatus()!=null){
								jft.setStatus(jft.getInternalStatus());
								jft.setStatusMaster(jft.getInternalStatus());
							}else{
								if(jftStatus.equalsIgnoreCase("icomp")|| jftStatus.equalsIgnoreCase("comp") || jftStatus.equalsIgnoreCase("vlt")){
									jft.setStatus(statusMaster);
									try{
										if(jft.getSecondaryStatus()!=null){
											jft.setStatusMaster(null);
										}else{
											jft.setStatusMaster(statusMaster);
										}
									}catch(Exception e){
										jft.setStatusMaster(statusMaster);
										e.printStackTrace();
									}
								}
							}
							// New Check JSI optional
							if(jft.getJobId().getJobAssessmentStatus()==1)
								jobForTeacherDAO.makePersistent(jft);
						}
					}
				}else
				{
					// base taken status 
					StatusMaster epiStatus = null;
					JobOrder jjj = new JobOrder();
					jjj.setJobId(0);
					List<TeacherAssessmentStatus> baseStatusList = teacherAssessmentStatusDAO.findAssessmentTaken(teacherDetail,jjj);
					if(baseStatusList.size()>0)
						epiStatus = baseStatusList.get(0).getStatusMaster();
					StatusMaster stautsIcomp= WorkThreadServlet.statusMap.get("icomp");
					//////////////////////////////////////////////
					///////////////////////// for external user /////////////////////////
					List<JobForTeacher> lstJobForTeacherIcomp = new ArrayList<JobForTeacher>();
					List<JobOrder> lstJBOrder = new ArrayList<JobOrder>();

					List<JobForTeacher> lstJobForTeacher= null;
					lstJobForTeacher = jobForTeacherDAO.findJobByTeacherAndSatusDuringCurrentYear(teacherDetail,stautsIcomp);
					for(JobForTeacher jft: lstJobForTeacher)
					{
						if(jft.getJobId().getIsJobAssessment()==false)
						{
							String jftStatus = jft.getStatus().getStatusShortName();
							if(jft.getInternalSecondaryStatus()!=null){
								jft.setStatus(jft.getInternalStatus());
								jft.setStatusMaster(null);
								jft.setSecondaryStatus(jft.getInternalSecondaryStatus());
							}else if(jft.getInternalStatus()!=null){
								jft.setStatus(jft.getInternalStatus());
								jft.setStatusMaster(jft.getInternalStatus());
							}else{
								if(jftStatus.equalsIgnoreCase("icomp")|| jftStatus.equalsIgnoreCase("comp") || jftStatus.equalsIgnoreCase("vlt")){
									jft.setStatus(statusMaster);
									try{
										if(jft.getSecondaryStatus()!=null){
											jft.setStatusMaster(null);
										}else{
											jft.setStatusMaster(statusMaster);
										}
									}catch(Exception e){
										jft.setStatusMaster(statusMaster);
										e.printStackTrace();
									}
								}
							}
							jobForTeacherDAO.makePersistent(jft);
						}
						else if(jft.getJobId().getJobCategoryMaster().getBaseStatus()){//**##**
							lstJobForTeacherIcomp.add(jft);
							lstJBOrder.add(jft.getJobId());
						}
					}

					TeacherAssessmentStatus tAStatus = null;
					if(lstJobForTeacherIcomp.size()>0){
						List<TeacherAssessmentStatus> lstTAStatus = teacherAssessmentStatusDAO.findByTeacherAndListJob(teacherDetail, lstJBOrder);
						Map<Integer, TeacherAssessmentStatus> mapAssessStatus = new HashMap<Integer, TeacherAssessmentStatus>();
						for(TeacherAssessmentStatus tas: lstTAStatus){
							mapAssessStatus.put(tas.getJobOrder().getJobId(), tas);
						}

						for(JobForTeacher jft: lstJobForTeacherIcomp){
							tAStatus = mapAssessStatus.get(jft.getJobId().getJobId());
							if(tAStatus!=null){
								String jftStatus = jft.getStatus().getStatusShortName();
								if(jft.getInternalSecondaryStatus()!=null){
									jft.setStatus(jft.getInternalStatus());
									jft.setStatusMaster(null);
									jft.setSecondaryStatus(jft.getInternalSecondaryStatus());
								}else if(jft.getInternalStatus()!=null){
									jft.setStatus(jft.getInternalStatus());
									jft.setStatusMaster(jft.getInternalStatus());
								}else{
									if(jftStatus.equalsIgnoreCase("icomp")|| jftStatus.equalsIgnoreCase("comp") || jftStatus.equalsIgnoreCase("vlt")){
										jft.setStatus(tAStatus.getStatusMaster());
										try{
											if(jft.getSecondaryStatus()!=null){
												jft.setStatusMaster(null);
											}else{
												jft.setStatusMaster(tAStatus.getStatusMaster());
											}
										}catch(Exception e){
											jft.setStatusMaster(tAStatus.getStatusMaster());
											e.printStackTrace();
										}	
									}
								}
								// New Check JSI optional
								if(jft.getJobId().getJobAssessmentStatus()==1)
									jobForTeacherDAO.makePersistent(jft);
							}else{
		                        try{
		                        	if(jft.getJobId().getJobCategoryMaster().getBaseStatus()){
			                            if(epiStatus!=null)
			                            {
				                             jft.setStatusMaster(epiStatus);
				                             jft.setStatus(epiStatus);
				                             jobForTeacherDAO.makePersistent(jft);
			                            }else
			                            {
			                            	 jft.setStatusMaster(stautsIcomp);
				                             jft.setStatus(stautsIcomp);
				                             jobForTeacherDAO.makePersistent(jft);
			                            	
			                            }
		                             }
		                         }catch(Exception e){
		                        	 e.printStackTrace();
		                         }
	                        }
						}

					}
					/////////////////////////////////////////////////////////////////////
				}

			}

			assessmentDetail.setStatus(assessmentDetail.getStatus()+"#"+attemptsStrike+"#"+oldJobId);

			return assessmentDetail;
		}

	}
	List<AssessmentDetail> assessmentDetails = null;
	List<AssessmentJobRelation> assessmentJobRelations = null;

	AssessmentJobRelation assessmentJobRelation = null;
	// if assessment is not taken
	try {//for base
		if(jobOrder!=null && jobOrder.getJobId()!=null && jobOrder.getJobId()==0)
		{

			Criterion criterion = Restrictions.eq("status", "A");
			Criterion criterion1 = Restrictions.eq("assessmentType", 1);

			assessmentDetails = assessmentDetailDAO.findByCriteria(criterion,criterion1);
			List<AssessmentDetail> normalAssessmentDetail = new ArrayList<AssessmentDetail>();
			List<AssessmentDetail> researchAssessmentDetail = new ArrayList<AssessmentDetail>();

			for(AssessmentDetail assessmentDetail2 : assessmentDetails)
			{
				if(assessmentDetail2.getIsResearchEPI())
					researchAssessmentDetail.add(assessmentDetail2);
				else
					normalAssessmentDetail.add(assessmentDetail2);
			}
			Map<Boolean,EpiTakenList> epiTakenMap = epiTakenListDAO.getEPITakenMap();
			EpiTakenList epiTakenList = null;
			int nadSize = normalAssessmentDetail.size();
			int radSize = researchAssessmentDetail.size();
			if(teacherDetail.getIsResearchTeacher()) //Research EPI
			{
				if(radSize==1)
				{
					assessmentDetail = researchAssessmentDetail.get(0);
					return assessmentDetail;
				}else if(radSize>1)
				{
					epiTakenList = epiTakenMap.get(true);
					if(epiTakenList!=null)
					{
						int indexId = 0;
						for (AssessmentDetail assessmentDetail2 : researchAssessmentDetail) {
							if(assessmentDetail2.getAssessmentId().equals(epiTakenList.getAssessmentId()))
								break;

							indexId++;
						}

						if((indexId+1)==radSize)
							assessmentDetail = researchAssessmentDetail.get(0);
						else
							assessmentDetail = researchAssessmentDetail.get(indexId+1);
					}else
						assessmentDetail = researchAssessmentDetail.get(0);

					return assessmentDetail;
				}else
				{
					assessmentDetail.setAssessmentName(null);
					assessmentDetail.setStatus("1");
					return assessmentDetail;
				}

			}else //normal EPI
			{
				if(nadSize==1)
				{
					assessmentDetail = normalAssessmentDetail.get(0);
					return assessmentDetail;
				}else if(nadSize>1)
				{
					epiTakenList = epiTakenMap.get(false);
					if(epiTakenList!=null)
					{
						int indexId = 0;
						for (AssessmentDetail assessmentDetail2 : normalAssessmentDetail) {
							if(assessmentDetail2.getAssessmentId().equals(epiTakenList.getAssessmentId()))
								break;

							indexId++;
						}

						if((indexId+1)==nadSize)
							assessmentDetail = normalAssessmentDetail.get(0);
						else
							assessmentDetail = normalAssessmentDetail.get(indexId+1);
					}else
						assessmentDetail = normalAssessmentDetail.get(0);

					return assessmentDetail;
				}else
				{
					assessmentDetail.setAssessmentName(null);
					assessmentDetail.setStatus("1");
					return assessmentDetail;
				}

			}

		}else if(jobOrder!=null && jobOrder.getJobId()!=null && jobOrder.getJobId()>0)
		{
			String jobStatus = jobOrder.getStatus();
			//System.out.println("---------------- jobStatus::::: "+jobStatus);
			// checking job stauts
			Date dateWithoutTime = Utility.getDateWithoutTime();
			Date a = jobOrder.getJobStartDate();
			Date b =jobOrder.getJobEndDate();   // assume these are set to something

			System.out.println("=====+++====+++======++====");
			System.out.println(a.compareTo(dateWithoutTime) * dateWithoutTime.compareTo(b) > 0);
			System.out.println("=====+++====+++======++====");

			if(jobStatus!=null && !jobStatus.equalsIgnoreCase("A"))
			{
				//job deativated
				assessmentDetail.setAssessmentName(null);
				assessmentDetail.setStatus("6");
				return assessmentDetail;
			}else
			{
				//System.out.println("(((((((((((((((((((( "+Utility.checkDateBetweenTwoDate(jobOrder.getJobStartDate(), jobOrder.getJobEndDate()));
				if(!Utility.checkDateBetweenTwoDate(jobOrder.getJobStartDate(), jobOrder.getJobEndDate()))
				{
					//job expired
					assessmentDetail.setAssessmentName(null);
					assessmentDetail.setStatus("7");
					return assessmentDetail;
				}

			}
			assessmentJobRelations=assessmentJobRelationDAO.findRelationByJobOrder(jobOrder);
			if(assessmentJobRelations.size()!=0)
			{
				assessmentJobRelation = assessmentJobRelations.get(0);
				assessmentDetail = assessmentJobRelation.getAssessmentId();
				if(assessmentDetail.getStatus().equalsIgnoreCase("A"))
				{
					List<AssessmentQuestions> assessmentQuestions = assessmentQuestionsDAO.getAssessmentQuestions(assessmentDetail);
					if(assessmentQuestions.size()>0)
					{
						return assessmentDetail;
					}
					else
					{
						System.out.println("Sonu ssssssssss");
						assessmentDetail.getAssessmentDescription();
						assessmentDetail.setAssessmentName(null);
						assessmentDetail.setStatus("2"); // JSI has no questions
						return assessmentDetail;
					}
				}
				else{
					System.out.println("Sonu ssssssssssyyyyyyyyyyyyyy");
					assessmentDetail.setAssessmentName(null);
					assessmentDetail.setStatus("2");// JSI not active
					return assessmentDetail;
				}

			}else
			{
				System.out.println("Sonu ssssssssssyyyyyyyyyyyyyfffffffffy");
				assessmentDetail.setAssessmentName(null);
				assessmentDetail.setStatus("1");
				return assessmentDetail;
				
			}
		}
	} catch (Exception e) {
		e.printStackTrace();
	}
	return null;
}
	// Author Sonu Gupta
	//@Transactional(readOnly=false)
	public String getJSIDistrictSpecificQuestion(Integer jobId)
	{
		System.out.println("*****:::::::::::::::Assessment getJSIDistrictSpecificQuestion:::::::::::::::::::;; ");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		StringBuffer sb =new StringBuffer();
		try{			
			List<TeacherAssessmentQuestion> teacherAssessmentQuestionList	=	null;
			List<TeacherAnswerDetail> teacherAnswerDetailList	=	null; 
			TeacherDetail teacherDetail = null;

			if (session == null || session.getAttribute("teacherDetail") == null) {
				//return "false";
			}else
				teacherDetail=(TeacherDetail)session.getAttribute("teacherDetail");

			if(jobId!=0)
			{				
				JobOrder jobOrder=jobOrderDAO.findById(jobId, false, false);
				JobForTeacher jft = jobForTeacherDAO.findJFTByTeacherAndJob(teacherDetail, jobOrder);
				teacherAnswerDetailList=teacherAnswerDetailDAO.findAssessmentQuestionAnswers(teacherDetail, jobOrder);				
				
							
				List<JobOrder> jobOrders = teacherAnswerDetailsForDistrictSpecificQuestionsDAO.findLastTeacherAnswersByDistrictAndJob(teacherDetail,jobOrder);
				List<TeacherAssessmentQuestion> lastList = new ArrayList<TeacherAssessmentQuestion>();

				if(jobOrders.size()>0)
					lastList = teacherAssessmentQuestionDAO.findTAQbyDomainAndTeacher(null, teacherDetail);

				Map<Integer, TeacherAssessmentQuestion> map = new HashMap<Integer, TeacherAssessmentQuestion>();

				for(TeacherAssessmentQuestion teacherAssessmentQuestion : lastList)
					map.put(teacherAssessmentQuestion.getTeacherAssessmentdetail().getTeacherAssessmentId(), teacherAssessmentQuestion);

				TeacherAssessmentQuestion teacherAssessmentQuestion  = null;
				List<TeacherAssessmentOption> questionOptionsList = null;
				String questionInstruction = null;
				String shortName = "";
				int totalQuestions = teacherAnswerDetailList.size();
				int cnt = 0;
				DistrictMaster districtMaster=null;
				if(jobOrder.getDistrictMaster()!=null){
					districtMaster=jobOrder.getDistrictMaster();
				}			
				
							
					boolean focus = false;
				
					for (TeacherAnswerDetail teacherAnswerDetail : teacherAnswerDetailList) 
					{						
						shortName = teacherAnswerDetail.getQuestionTypeMaster().getQuestionTypeShortName();

						sb.append("<tr>");
						sb.append("<td width='90%'>");
						sb.append("<div><b>Question "+(++cnt)+" of "+totalQuestions+"</b></div>");					
																		
						sb.append("<div id='Q"+cnt+"question'>"+Utility.getUTFToHTML(teacherAnswerDetail.getTeacherAssessmentQuestion().getQuestion())+"</div>");

						questionOptionsList = teacherAnswerDetail.getTeacherAssessmentQuestion().getTeacherAssessmentOptions();
						teacherAssessmentQuestion = map.get(teacherAnswerDetail.getTeacherAssessmentQuestion().getTeacherAssessmentQuestionId());

						String checked = "";
						Integer optId = 0;
						String insertedText = "";
						if(teacherAnswerDetail!=null)
						{							
								if(teacherAnswerDetail.getSelectedOptions()!=null)
									optId = Integer.parseInt(teacherAnswerDetail.getSelectedOptions());

								insertedText = teacherAnswerDetail.getInsertedText();
							
						}										
						if(shortName.equalsIgnoreCase("tf") || shortName.equalsIgnoreCase("slsel"))
						{
							sb.append("<table>");
							for (TeacherAssessmentOption questionOptions : questionOptionsList) {								
								if((optId+"").equals(questionOptions.getTeacherAssessmentOptionId()+""))
									checked = "checked";
								else
									checked = "";

								sb.append("<tr><td style='border:0px;background-color: transparent;'><div  style='display: block;min-height: 20px;padding-left: 5px;vertical-align: middle;'><input type='radio' name='Q"+cnt+"opt' value='"+questionOptions.getTeacherAssessmentOptionId()+"' "+checked+" /></div>" +
										" <input type='hidden' id='Q"+questionOptions.getTeacherAssessmentOptionId()+"validQuestion' value='"+questionOptions.getQuestionOption()+"' ></td>" +
										"<td style='border:0px;background-color: transparent;padding-left:5px;' id='qOpt"+questionOptions.getTeacherAssessmentOptionId()+"'>"+questionOptions.getQuestionOption()+" ");
								sb.append("</td></tr>");
							}
							sb.append("</table>");
						}
						if(shortName.equalsIgnoreCase("et"))
						{
							sb.append("<table>");
							for (TeacherAssessmentOption questionOptions : questionOptionsList) {
								if((optId+"").equals(questionOptions.getTeacherAssessmentOptionId()+""))
									checked = "checked";
								else
									checked = "";

								sb.append("<tr><td style='border:0px;background-color: transparent;'><div  style='display: block;min-height: 20px;padding-left: 5px;vertical-align: middle;'><input type='radio' name='Q"+cnt+"opt' value='"+questionOptions.getTeacherAssessmentOptionId()+"' "+checked+" /></div>" +
										" <input type='hidden' id='Q"+questionOptions.getTeacherAssessmentOptionId()+"validQuestion' value='"+questionOptions.getTeacherAssessmentOptionId()+"' ></td>" +
										"<td style='border:0px;background-color: transparent;padding-left:5px;'  id='qOpt"+questionOptions.getTeacherAssessmentOptionId()+"'>"+questionOptions.getQuestionOption()+" ");
								sb.append("</td></tr>");
							}
							sb.append("</table>");

							if(teacherAnswerDetail.getInsertedText()!=null)
								sb.append("<div>"+Utility.getUTFToHTML(teacherAnswerDetail.getInsertedText())+"</div>");

							sb.append("&nbsp;&nbsp;<span style=''><textarea  name='Q"+cnt+"opt' id='Q"+cnt+"optet' class='form-control' maxlength='5000' rows='8' style='width:98%;margin:5px;'>"+insertedText+"</textarea><span><br/>");
							if(focus==false)
							{
								sb.append("<script type=\"text/javascript\" language=\"javascript\">");
								//sb.append("document.getElementById('Q"+cnt+"optet').focus();");
								sb.append("</script>");
								focus = true;
							}
							sb.append("<script type=\"text/javascript\" language=\"javascript\">");
							//sb.append("document.getElementById('Q"+cnt+"optet').focus();");
							sb.append("</script>");
						}
						if(shortName.equalsIgnoreCase("ml"))
						{
							sb.append("&nbsp;&nbsp;<textarea name='Q"+cnt+"opt' id='Q"+cnt+"opt' class='form-control' maxlength='5000' rows='8' style='width:98%;margin:5px;'>"+insertedText+"</textarea><br/>");					
							if(focus==false)
							{
								sb.append("<script type=\"text/javascript\" language=\"javascript\">");
								//sb.append("document.getElementById('Q"+cnt+"opt').focus();");
								sb.append("</script>");
								focus = true;
							}
							sb.append("<script type=\"text/javascript\" language=\"javascript\">");
							//sb.append("document.getElementById('Q"+cnt+"opt').focus();");
							sb.append("</script>");
						}
						sb.append("<input type='hidden' id='Q"+cnt+"questionId' value='"+teacherAnswerDetail.getTeacherAssessmentQuestion().getTeacherAssessmentQuestionId()+"'/>" );
						sb.append("<input type='hidden' id='Q"+cnt+"questionTypeId' value='"+teacherAnswerDetail.getQuestionTypeMaster().getQuestionTypeId()+"'/>" );
						sb.append("<input type='hidden' id='Q"+cnt+"questionTypeShortName' value='"+shortName+"'/>" );

						sb.append("</td>");
						sb.append("</tr>");
						insertedText = "";
					}					
				sb.append("@##@"+cnt+"@##@"+jobOrder.getDistrictMaster().getTextForDistrictSpecificQuestions());
			}
		}
		catch (Exception e) {
			// TODO: handle exception
		}
		return sb.toString();
	
	}
	public String setJSIDistrictQuestions(TeacherAnswerDetail[] answerDetail)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}		
		TeacherDetail teacherDetail = null;
		List<UserMaster> userMastersList=new ArrayList<UserMaster>();
		if (session == null || session.getAttribute("teacherDetail") == null) {
			//return "false";
		}else
			teacherDetail=(TeacherDetail)session.getAttribute("teacherDetail");

		try {					
			JobOrder jobOrder = answerDetail[0].getJobOrder();			
			jobOrder = jobOrderDAO.findById(jobOrder.getJobId(), false, false);
			
			StringBuilder sb=new StringBuilder();
			List<TeacherAnswerDetail> teacherAnswerDetailLists = null;
			teacherAnswerDetailLists=teacherAnswerDetailDAO.findAssessmentQuestionAnswers(teacherDetail, jobOrder);
			TeacherAnswerHistory teacherAnswerHistory=new TeacherAnswerHistory();
			Map<Long, TeacherAnswerDetail> teacherAnswerDetailOldMap = new HashMap<Long, TeacherAnswerDetail>();
			System.out.println(teacherDetail.getTeacherId());
			Criterion criterion=Restrictions.eq("teacherDetail", teacherDetail);
			Map<Integer,String> questionOptionById = new HashMap<Integer, String>();		
			List<TeacherAssessmentOption> teacherAssessmentOptions = teacherAssessmentOptionDAO.findByCriteria(criterion);
			if(teacherAssessmentOptions!=null && teacherAssessmentOptions.size()>0)
			{
				for(TeacherAssessmentOption top : teacherAssessmentOptions)
				{
					questionOptionById.put(top.getTeacherAssessmentOptionId().intValue(), top.getQuestionOption());
				}
			}
			for (TeacherAnswerDetail teacherAnswerDetail : teacherAnswerDetailLists) {						
				 teacherAnswerDetailOldMap.put(teacherAnswerDetail.getTeacherAssessmentQuestion().getTeacherAssessmentQuestionId(), teacherAnswerDetail);
			}			 

			 Map<BigInteger,Integer> map = new HashMap<BigInteger, Integer>();
			try {
				map = teacherAnswerDetailDAO.findTeacherQuestionAnswersForJSI(teacherDetail, jobOrder);				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			List<Integer> answers = new ArrayList<Integer>();
			Integer ansId = null;
			Boolean sendMailFlag=false;			
			try {
				SessionFactory sessionFactory = teacherAnswerHistoryDAO.getSessionFactory();
				StatelessSession statelessSession=sessionFactory.openStatelessSession();
				Transaction transaction=statelessSession.beginTransaction();
				
				for (TeacherAnswerDetail teacherAnswerDetail : answerDetail) {
					
					TeacherAnswerDetail oldAns =new TeacherAnswerDetail();
					
					oldAns = teacherAnswerDetailOldMap.get(teacherAnswerDetail.getTeacherAssessmentQuestion().getTeacherAssessmentQuestionId());
					
					teacherAnswerDetail.setTeacherDetail(teacherDetail);
					teacherAnswerDetail.setJobOrder(jobOrder);
					teacherAnswerDetail.setCreatedDateTime(new Date());
					if(oldAns!=null)
					teacherAnswerDetail.setSelectedOptions(teacherAnswerDetail.getSelectedOptions());
					else
						teacherAnswerDetail.setSelectedOptions(oldAns.getSelectedOptions());
					
					ansId = map.get(teacherAnswerDetail.getTeacherAssessmentQuestion().getTeacherAssessmentQuestionId());					
					if(ansId!=null)
					{
						teacherAnswerDetail.setAnswerId(ansId);
					}	
					
					teacherAnswerDetail.setAnswerId(oldAns.getAnswerId());
					teacherAnswerDetail.setAssessmentDetail(oldAns.getAssessmentDetail());
					teacherAnswerDetail.setQuestionWeightage(oldAns.getQuestionWeightage());
					teacherAnswerDetail.setTeacherAssessmentdetail(oldAns.getTeacherAssessmentdetail());
					teacherAnswerDetail.setTotalScore(oldAns.getTotalScore());					
					teacherAnswerDetailDAO.makePersistent(teacherAnswerDetail);									
					
					if(oldAns!=null)
					{
						String opt1="";
						String opt2="";
						if(teacherAnswerDetail.getSelectedOptions()==null)
							opt1="";
						else
							opt1=teacherAnswerDetail.getSelectedOptions();
						if(oldAns.getSelectedOptions()==null)
							opt2="";
						else
							opt2=oldAns.getSelectedOptions();
						
						if(!opt1.equalsIgnoreCase(opt2) || (teacherAnswerDetail!=null &&  teacherAnswerDetail.getInsertedText()!=null && oldAns.getInsertedText()!=null && !teacherAnswerDetail.getInsertedText().equalsIgnoreCase(oldAns.getInsertedText())))
						{
							System.out.println("Question Not Match "+oldAns);
							
							teacherAnswerHistory.setTeacherDetail(teacherDetail);
							teacherAnswerHistory.setQuestion(oldAns.getTeacherAssessmentQuestion().getQuestion());
							teacherAnswerHistory.setJobOrder(jobOrder);
							teacherAnswerHistory.setQuestion(oldAns.getTeacherAssessmentQuestion().getQuestion());
							teacherAnswerHistory.setQuestionTypeMaster(oldAns.getQuestionTypeMaster());
							teacherAnswerHistory.setQuestionType(oldAns.getQuestionTypeMaster().getQuestionTypeShortName());
							teacherAnswerHistory.setSelectedOptions(teacherAnswerDetail.getSelectedOptions());
							teacherAnswerHistory.setSelectedOptions(teacherAnswerDetail.getSelectedOptions());
							teacherAnswerHistory.setInsertedText(teacherAnswerDetail.getInsertedText());							
							teacherAnswerHistory.setTeacherAssessmentQuestion(oldAns.getTeacherAssessmentQuestion());
							teacherAnswerHistory.setAssessmentDetail(oldAns.getAssessmentDetail());
							teacherAnswerHistory.setTeacherAssessmentdetail(oldAns.getTeacherAssessmentdetail());
							teacherAnswerHistory.setTypeId(2);
							teacherAnswerHistory.setCreatedDateTime(new Date());										
							statelessSession.insert(teacherAnswerHistory);
							sendMailFlag=true;
							String  oldText="";
							String  newText="";
							String  oldSelOpt="";
							String  newSelOpt="";
							if(oldAns.getInsertedText()==null)
								oldText="";
							else
								oldText=oldAns.getInsertedText();
							if(teacherAnswerDetail.getInsertedText()==null)
								newText="";
							else
								newText=teacherAnswerDetail.getInsertedText();
							
							if(oldAns.getSelectedOptions()==null)
								oldSelOpt="";
							else
								oldSelOpt=questionOptionById.get(Integer.parseInt(oldAns.getSelectedOptions()));
							if(teacherAnswerDetail.getSelectedOptions()==null)
								newSelOpt="";
							else
								newSelOpt=questionOptionById.get(Integer.parseInt(teacherAnswerDetail.getSelectedOptions()));
							
							sb.append("Changed the answer for Question "+'"'+""+oldAns.getTeacherAssessmentQuestion().getQuestion()+""+'"'+" from "+oldSelOpt+" "+ oldText +" to "+newSelOpt+ " "+newText+ ".<br><br>");
							
						}
					}
				}
				System.out.println("ffffffffffffff "+sb.toString());
				transaction.commit();
				statelessSession.close();
				sendMailFlag=false; //Temporary Add for stop mail
				/*if(sendMailFlag==true)					
				{
					MailText mail=new MailText();
					String subject = "Applicant "+teacherDetail.getFirstName()+" "+teacherDetail.getLastName()+ "has updated the Job  Specific Inventory";
					userMastersList=userMasterDAO.getUserByDistrict(jobOrder.getDistrictMaster());
					for(UserMaster userMaster: userMastersList)
					{
						String contents=mail.getUpdateQQMailForDistrict(userMaster, teacherDetail,sb.toString());
						System.out.println(">>>>>>>>>>>>>>>>>>>>>>>userMastersList size  :: "+ userMastersList.size());
						DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
						dsmt.setEmailerService(emailerService);
						dsmt.setMailfrom(Utility.getValueOfSmtpPropByKey("smtphost.adminusername"));					
						dsmt.setMailto(userMaster.getEmailAddress());						
						dsmt.setMailsubject(subject);
						dsmt.setMailcontent(contents);
						System.out.println("content  "+contents);
						try {
							dsmt.start();	
						} catch (Exception e) {}
					}
				}*/
			} catch (Exception e) {
				e.printStackTrace();
			}
			String ss="";
			if(answers.size()>0)
				 ss="";
				//teacherAnswerDetailsForDistrictSpecificQuestionsDAO.updateOtherQQ(teacherDetail,jobOrder.getDistrictMaster(),answers);

		return "1";
		}
		catch (Exception e) {		
		}
		return null;
	}
	
	@Transactional(readOnly=false)
	public String getDistrictSpecificQuestionBySet(Integer jobId,String isAffilated)
	{
		System.out.println("*****:::::::::::::::Assessment getDistrictSpecificQuestion:::::::::::::::::::;; ");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		StringBuffer sb =new StringBuffer();
		try{
			List<DistrictSpecificQuestions> districtSpecificQuestionList	= new ArrayList<DistrictSpecificQuestions>(); 
			TeacherDetail teacherDetail = null;

			if (session == null || session.getAttribute("teacherDetail") == null) {
				//return "false";
			}else
				teacherDetail=(TeacherDetail)session.getAttribute("teacherDetail");

			if(jobId!=0)
			{
				JobOrder jobOrder=jobOrderDAO.findById(jobId, false, false);
				JobCategoryMaster jobCategoryMaster = jobOrder.getJobCategoryMaster();
				QqQuestionSets qqQuestionSets = new QqQuestionSets();
				List<QqQuestionsetQuestions> qqQuestionsetQuestions=null;
				if(jobCategoryMaster!=null && jobCategoryMaster.getQuestionSets()!=null){
					qqQuestionSets = jobCategoryMaster.getQuestionSets();
					qqQuestionsetQuestions = qqQuestionsetQuestionsDAO.findByQuestionSet(jobCategoryMaster.getQuestionSets()); 
				}
				List<Integer> tempDistSpecQues = new ArrayList<Integer>();
				if(qqQuestionsetQuestions!=null && qqQuestionsetQuestions.size()>0){
					for (QqQuestionsetQuestions qqQuestionsetQuestions2 : qqQuestionsetQuestions) {
						tempDistSpecQues.add(qqQuestionsetQuestions2.getDistrictSpecificQuestions().getQuestionId());
					}
				}
				JobForTeacher jft = jobForTeacherDAO.findJFTByTeacherAndJob(teacherDetail, jobOrder);
				//districtSpecificQuestionList=districtSpecificQuestionsDAO.getDistrictSpecificQuestionBYDistrict(jobOrder.getDistrictMaster());
				System.out.println(">.>.> 1");
				if(tempDistSpecQues.size()>0)
				districtSpecificQuestionList=districtSpecificQuestionsDAO.getDistrictSpecificQuestionBYQues(tempDistSpecQues);
				System.out.println(">.>.> 2");
				List<JobOrder> jobOrders = teacherAnswerDetailsForDistrictSpecificQuestionsDAO.findLastTeacherAnswersByDistrictAndJob(teacherDetail,jobOrder);
				System.out.println(">.>.> 3");
				List<TeacherAnswerDetailsForDistrictSpecificQuestions> lastList = new ArrayList<TeacherAnswerDetailsForDistrictSpecificQuestions>();
				System.out.println(">.>.> 4");
				if(jobOrders.size()>0)
					lastList = teacherAnswerDetailsForDistrictSpecificQuestionsDAO.findTeacherAnswersBySetQues(teacherDetail,qqQuestionSets);

				Map<Integer, TeacherAnswerDetailsForDistrictSpecificQuestions> map = new HashMap<Integer, TeacherAnswerDetailsForDistrictSpecificQuestions>();

				for(TeacherAnswerDetailsForDistrictSpecificQuestions teacherAnswerDetailsForDistrictSpecificQuestion : lastList)
					map.put(teacherAnswerDetailsForDistrictSpecificQuestion.getDistrictSpecificQuestions().getQuestionId(), teacherAnswerDetailsForDistrictSpecificQuestion);

				TeacherAnswerDetailsForDistrictSpecificQuestions teacherAnswerDetailsForDistrictSpecificQuestion = null;
				List<OptionsForDistrictSpecificQuestions> questionOptionsList = null;
				String questionInstruction = null;
				String shortName = "";
				int totalQuestions =0;
				if(tempDistSpecQues.size()>0)
					totalQuestions = districtSpecificQuestionList.size();
				int cnt = 0;


				///***** Sekhar Add TranseferCandidate ******///
				DistrictMaster districtMaster=null;
				boolean isMiami=false;
				if(jobOrder.getDistrictMaster()!=null){
					districtMaster=jobOrder.getDistrictMaster();
					if(districtMaster.getDistrictId().equals(1200390))
						isMiami = true;
				}
				List<InternalTransferCandidates> itcList=internalTransferCandidatesDAO.getInternalCandidateByDistrict(teacherDetail,districtMaster);
				boolean internalFlag=false;
				boolean getQQ=true;
				if(itcList.size()>0){
					internalFlag=true;
				}
				if(districtMaster!=null && internalFlag){
					if(districtMaster.getOfferQualificationItems()){
						getQQ=true;
					}else{
						getQQ=false;
					}
				}
				try{
					if(jft!=null){
						if(jft.getIsAffilated()!=null && jft.getIsAffilated()==1 && internalFlag==false){
							if(jobOrder.getJobCategoryMaster()!=null && jobOrder.getJobCategoryMaster().getOfferQualificationItems()){
								getQQ=true;
							}else{
								getQQ=false;
							}
						}
					}else{
						if(isAffilated!=null && isAffilated.equalsIgnoreCase("1") && internalFlag==false){
							if(jobOrder.getJobCategoryMaster()!=null && jobOrder.getJobCategoryMaster().getOfferQualificationItems()){
								getQQ=true;
							}else{
								getQQ=false;
							}
						}
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				
				
				if(jobOrder!= null && jobOrder.getJobCategoryMaster()!=null && jobOrder.getJobCategoryMaster().getJobCategoryName().equalsIgnoreCase("Aspiring Assistant Principal")){
					TeacherPersonalInfo teacherPersonalInfo = teacherPersonalInfoDAO.findTeacherPersonalInfoByTeacher(teacherDetail);
					if(teacherPersonalInfo!=null && teacherPersonalInfo.getEmployeeType().equals(1)){
						isAffilated=1+"";
					}
					
					if(isAffilated!=null && isAffilated.equalsIgnoreCase("1") && districtMaster.getDistrictId().equals(1200390) &&  jobOrder.getJobCategoryMaster()!=null && jobOrder.getJobCategoryMaster().getJobCategoryName().equalsIgnoreCase("Aspiring Assistant Principal")){
						getQQ=false;
					}
					
				}
					
				System.out.println("get QQQQQQQQ   "+getQQ+"   "+isAffilated);
				boolean focus = false;
				if(getQQ && totalQuestions>0)
					for (DistrictSpecificQuestions districtSpecificQuestion : districtSpecificQuestionList) 
					{
						shortName = districtSpecificQuestion.getQuestionTypeMaster().getQuestionTypeShortName();

						sb.append("<tr>");
						sb.append("<td width='90%'>");
						sb.append("<div><b>"+Utility.getLocaleValuePropByKey("headQues", locale)+" "+(++cnt)+" of "+totalQuestions+"</b></div>");

						questionInstruction = districtSpecificQuestion.getQuestionInstructions()==null?"":districtSpecificQuestion.getQuestionInstructions();
						if(!questionInstruction.equals(""))
							sb.append(""+Utility.getUTFToHTML(questionInstruction)+"");

						sb.append("<div id='Q"+cnt+"question'>"+Utility.getUTFToHTML(districtSpecificQuestion.getQuestion())+"</div>");

						questionOptionsList = districtSpecificQuestion.getQuestionOptions();

						teacherAnswerDetailsForDistrictSpecificQuestion = map.get(districtSpecificQuestion.getQuestionId());

						String checked = "";
						Integer optId = 0;
						String insertedText = "";
						if(teacherAnswerDetailsForDistrictSpecificQuestion!=null)
						{
							if(shortName.equalsIgnoreCase(teacherAnswerDetailsForDistrictSpecificQuestion.getQuestionType()))
							{
								if(teacherAnswerDetailsForDistrictSpecificQuestion.getSelectedOptions()!=null)
									optId = Integer.parseInt(teacherAnswerDetailsForDistrictSpecificQuestion.getSelectedOptions());

								insertedText = teacherAnswerDetailsForDistrictSpecificQuestion.getInsertedText();
							}
						}
						if(shortName.equalsIgnoreCase("tf") || shortName.equalsIgnoreCase("slsel"))
						{
							sb.append("<table>");
							for (OptionsForDistrictSpecificQuestions questionOptions : questionOptionsList) {
								if(optId.equals(questionOptions.getOptionId()))
									checked = "checked";
								else
									checked = "";

								sb.append("<tr><td style='border:0px;background-color: transparent;'><div  style='display: block;min-height: 20px;padding-left: 5px;vertical-align: middle;'><input type='radio' name='Q"+cnt+"opt' value='"+questionOptions.getOptionId()+"' "+checked+" /></div>" +
										" <input type='hidden' id='Q"+questionOptions.getOptionId()+"validQuestion' value='"+questionOptions.getValidOption()+"' ></td>" +
										"<td style='border:0px;background-color: transparent;padding-left:5px;' id='qOpt"+questionOptions.getOptionId()+"'>"+questionOptions.getQuestionOption()+" ");
								sb.append("</td></tr>");
							}
							sb.append("</table>");
						}
						if(shortName.equalsIgnoreCase("et"))
						{
							sb.append("<table>");
							for (OptionsForDistrictSpecificQuestions questionOptions : questionOptionsList) {
								if(optId.equals(questionOptions.getOptionId()))
									checked = "checked";
								else
									checked = "";

								sb.append("<tr><td style='border:0px;background-color: transparent;'><div  style='display: block;min-height: 20px;padding-left: 5px;vertical-align: middle;'><input type='radio' name='Q"+cnt+"opt' value='"+questionOptions.getOptionId()+"' "+checked+" /></div>" +
										" <input type='hidden' id='Q"+questionOptions.getOptionId()+"validQuestion' value='"+questionOptions.getValidOption()+"' ></td>" +
										"<td style='border:0px;background-color: transparent;padding-left:5px;'  id='qOpt"+questionOptions.getOptionId()+"'>"+questionOptions.getQuestionOption()+" ");
								sb.append("</td></tr>");
							}
							sb.append("</table>");

							if(districtSpecificQuestion.getQuestionExplanation()!=null)
								sb.append("<div>"+Utility.getUTFToHTML(districtSpecificQuestion.getQuestionExplanation())+"</div>");

							sb.append("&nbsp;&nbsp;<span style=''><textarea  name='Q"+cnt+"opt' id='Q"+cnt+"optet' class='form-control' maxlength='5000' rows='8' style='width:98%;margin:5px;'>"+insertedText+"</textarea><span><br/>");
							if(focus==false)
							{
								sb.append("<script type=\"text/javascript\" language=\"javascript\">");
								//sb.append("document.getElementById('Q"+cnt+"optet').focus();");
								sb.append("</script>");
								focus = true;
							}
							sb.append("<script type=\"text/javascript\" language=\"javascript\">");
							//sb.append("document.getElementById('Q"+cnt+"optet').focus();");
							sb.append("</script>");
						}
						if(shortName.equalsIgnoreCase("ml"))
						{
							sb.append("&nbsp;&nbsp;<textarea name='Q"+cnt+"opt' id='Q"+cnt+"opt' class='form-control' maxlength='5000' rows='8' style='width:98%;margin:5px;'>"+insertedText+"</textarea><br/>");					
							if(focus==false)
							{
								sb.append("<script type=\"text/javascript\" language=\"javascript\">");
								//sb.append("document.getElementById('Q"+cnt+"opt').focus();");
								sb.append("</script>");
								focus = true;
							}
							sb.append("<script type=\"text/javascript\" language=\"javascript\">");
							//sb.append("document.getElementById('Q"+cnt+"opt').focus();");
							sb.append("</script>");
						}
						sb.append("<input type='hidden' id='Q"+cnt+"questionId' value='"+districtSpecificQuestion.getQuestionId()+"'/>" );
						sb.append("<input type='hidden' id='Q"+cnt+"questionTypeId' value='"+districtSpecificQuestion.getQuestionTypeMaster().getQuestionTypeId()+"'/>" );
						sb.append("<input type='hidden' id='Q"+cnt+"questionTypeShortName' value='"+shortName+"'/>" );
						

						sb.append("</td>");
						sb.append("</tr>");
						insertedText = "";
					}
				if(jobCategoryMaster.getQuestionSets()!=null && !jobCategoryMaster.getQuestionSets().getID().equals(0))
					sb.append("<input type='hidden' id='qqQuestionSetID' value='"+jobCategoryMaster.getQuestionSets().getID()+"'/>" );
				else
					sb.append("<input type='hidden' id='qqQuestionSetID' value=''/>" );
				sb.append("@##@"+cnt+"@##@"+jobOrder.getDistrictMaster().getTextForDistrictSpecificQuestions());

				List<JobForTeacher> lstJobForTeacher= null;
				JobForTeacher jobForTeacher = null;
				lstJobForTeacher = jobForTeacherDAO.findJobByTeacherAndSatusFormJobId(teacherDetail,jobOrder);
				boolean completeFlag=false;
				if(lstJobForTeacher==null || lstJobForTeacher.size()==0)
				{
					PrintOnConsole.debugPrintln("getDistrictSpecificQuestion ... Not save in JFT");
				}else
				{
					PrintOnConsole.debugPrintln("getDistrictSpecificQuestion ... already saved in JFT");
					////////////////////Checking status to be set///////////////////////
					jobForTeacher  = lstJobForTeacher.get(0);
					StatusMaster statusMaster = null;
					if(districtSpecificQuestionList.size()>0 && jobForTeacher.getFlagForDistrictSpecificQuestions()==null)
					{
						statusMaster = WorkThreadServlet.statusMap.get("icomp");
						if(jobForTeacher.getInternalSecondaryStatus()!=null){
							jobForTeacher.setStatus(jobForTeacher.getInternalStatus());
							jobForTeacher.setStatusMaster(null);
							jobForTeacher.setSecondaryStatus(jobForTeacher.getInternalSecondaryStatus());
						}else if(jobForTeacher.getInternalStatus()!=null){
							try{
								if(jobForTeacher.getStatus()!=null && (jobForTeacher.getStatus().getStatusShortName().equals("icomp")|| jobForTeacher.getStatus().getStatusShortName().equals("comp") || jobForTeacher.getStatus().getStatusShortName().equals("vlt"))){
									jobForTeacher.setStatus(jobForTeacher.getInternalStatus());
									jobForTeacher.setStatusMaster(jobForTeacher.getInternalStatus());
								}
							}catch(Exception e){}
						}else{
							try{
								if(jobForTeacher.getStatus()!=null && (jobForTeacher.getStatus().getStatusShortName().equals("icomp")|| jobForTeacher.getStatus().getStatusShortName().equals("comp") || jobForTeacher.getStatus().getStatusShortName().equals("vlt"))){
									jobForTeacher.setStatus(statusMaster);
									if(jobForTeacher.getSecondaryStatus()!=null){
										jobForTeacher.setStatusMaster(null);
									}else{
										jobForTeacher.setStatusMaster(statusMaster);
									}
								}
							}catch(Exception e){}
						}
					}else
					{

						/*String isAffilated = "0";
					if(jobForTeacher.getIsAffilated()!=null)
						isAffilated = ""+jobForTeacher.getIsAffilated();

					statusMaster = assessmentDetailDAO.findByTeacherIdJobStaus(jobForTeacher, isAffilated,internalFlag,offerEPI,offerJSI,isPortfolio);*/

						int[] flagList=assessmentDetailDAO.getInternalTeacherFalgs(jobForTeacher) ;
						statusMaster = assessmentDetailDAO.findByTeacherIdJobStaus(jobForTeacher,flagList);


						if(jobForTeacher.getInternalSecondaryStatus()!=null){
							jobForTeacher.setStatus(jobForTeacher.getInternalStatus());
							jobForTeacher.setStatusMaster(null);
							jobForTeacher.setSecondaryStatus(jobForTeacher.getInternalSecondaryStatus());
						}else if(jobForTeacher.getInternalStatus()!=null){
							try{
								String jftStatus=jobForTeacher.getStatus().getStatusShortName();
								if(!jftStatus.equalsIgnoreCase("comp") && statusMaster.getStatusShortName().equalsIgnoreCase("comp")){
									completeFlag=true;
								}
							}catch(Exception e){
								e.printStackTrace();
							}
							try{
								if(jobForTeacher.getStatus()!=null && (jobForTeacher.getStatus().getStatusShortName().equals("icomp")|| jobForTeacher.getStatus().getStatusShortName().equals("comp") || jobForTeacher.getStatus().getStatusShortName().equals("vlt"))){
									jobForTeacher.setStatus(jobForTeacher.getInternalStatus());
									jobForTeacher.setStatusMaster(jobForTeacher.getInternalStatus());
								}
							}catch(Exception e){}
						}else{
							try{
								String jftStatus=jobForTeacher.getStatus().getStatusShortName();
								if(!jftStatus.equalsIgnoreCase("comp") && statusMaster.getStatusShortName().equalsIgnoreCase("comp")){
									completeFlag=true;
								}
							}catch(Exception e){
								e.printStackTrace();
							}
							try{
								if(jobForTeacher.getStatus()!=null && (jobForTeacher.getStatus().getStatusShortName().equals("icomp")|| jobForTeacher.getStatus().getStatusShortName().equals("comp") || jobForTeacher.getStatus().getStatusShortName().equals("vlt"))){
									jobForTeacher.setStatus(statusMaster);
									if(jobForTeacher.getSecondaryStatus()!=null){
										jobForTeacher.setStatusMaster(null);
									}else{
										jobForTeacher.setStatusMaster(statusMaster);
									}
								}
							}catch(Exception e){}
						}
					}
					jobForTeacherDAO.makePersistent(jobForTeacher);

					try{
						if(completeFlag && jobForTeacher.getStatus()!=null && jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("comp")){
							commonService.futureJobStatus(jobOrder, teacherDetail,jobForTeacher);
						}
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			}
			//////////////////////	
		}catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	}
	
	@Transactional(readOnly=false)
	public String setDistrictQuestionsBySet(TeacherAnswerDetailsForDistrictSpecificQuestions[] answerDetailsForDistrictSpecificQuestions,QqQuestionSets qqQuestionSets)
	{
		//System.out.println(" *** ::::::::::::::::::::setDistrictQuestions:::::::::::::::::::");
		//System.out.println(":::::::::::::::Assessment setDistrictQuestions:::::::::::::::::::;; ");

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}

		TeacherDetail teacherDetail = null;

		if (session == null || session.getAttribute("teacherDetail") == null) {
			//return "false";
		}else
			teacherDetail=(TeacherDetail)session.getAttribute("teacherDetail");

		try {

			int inValidCount = 0;
			List<TeacherAnswerDetailsForDistrictSpecificQuestions> wrongAnswerDetailsForDistrictSpecificQuestionList = new ArrayList<TeacherAnswerDetailsForDistrictSpecificQuestions>();
			JobOrder jobOrder = answerDetailsForDistrictSpecificQuestions[0].getJobOrder();
			jobOrder = jobOrderDAO.findById(jobOrder.getJobId(), false, false);

			QqQuestionSets qqQuestionSets2 = new QqQuestionSets();			
			
			if(jobOrder.getJobCategoryMaster()!=null && jobOrder.getJobCategoryMaster().getQuestionSets()!=null && !jobOrder.getJobCategoryMaster().getQuestionSets().equals(0)){
				qqQuestionSets2 = jobOrder.getJobCategoryMaster().getQuestionSets();
			}
			//List<TeacherAnswerDetailsForDistrictSpecificQuestions> lastList = new ArrayList<TeacherAnswerDetailsForDistrictSpecificQuestions>();
			Map<Integer,Integer> map = new HashMap<Integer, Integer>();

			try {
				map = teacherAnswerDetailsForDistrictSpecificQuestionsDAO.findTeacherAnswersForQQByDistrictSet(teacherDetail,qqQuestionSets);
			} catch (Exception e) {
				e.printStackTrace();
			}
			List<Integer> answers = new ArrayList<Integer>();
			Integer ansId = null;
			Boolean QQFlag=false;
			
			List<TeacherAnswerDetailsForDistrictSpecificQuestions> wrongAnsListInFinalizedQQ = new ArrayList<TeacherAnswerDetailsForDistrictSpecificQuestions>();
			List<JobForTeacher> lstJobForTeacher1= null;
			lstJobForTeacher1=new ArrayList<JobForTeacher>();
			//lstJobForTeacher1 = jobForTeacherDAO.findByTeacherDistrictAndJobCategory(teacherDetail, jobOrder.getDistrictMaster(), jobOrder.getJobCategoryMaster());
			lstJobForTeacher1 = jobForTeacherDAO.findByTeacherAndQuestionSet(teacherDetail,jobOrder.getJobCategoryMaster().getQuestionSets());
            System.out.println("lstJobForTeacher Size Ctaegory Wise"+lstJobForTeacher1.size());
            
            try{
	            if(lstJobForTeacher1.size()>0){
	            	if(lstJobForTeacher1.get(0).getIsDistrictSpecificNoteFinalize()!=null && lstJobForTeacher1.get(0).getIsDistrictSpecificNoteFinalize()==true){
	            		List<TeacherAnswerDetailsForDistrictSpecificQuestions> list=new ArrayList<TeacherAnswerDetailsForDistrictSpecificQuestions>();        				
	    				list=teacherAnswerDetailsForDistrictSpecificQuestionsDAO.findTeacherAnswersByDistrictTeacherAndQuestionSet(teacherDetail,jobOrder.getDistrictMaster(),jobOrder.getJobCategoryMaster().getQuestionSets());
	    				System.out.println("AllQuestionListSize-->"+list.size());
	    				if(list.size()>0){
	    				for(TeacherAnswerDetailsForDistrictSpecificQuestions tadfdsq : list){
	    					if(tadfdsq.getIsValidAnswer()!=null && tadfdsq.getIsValidAnswer()==false){
	    						wrongAnsListInFinalizedQQ.add(tadfdsq);
	    						QQFlag=true;
	    					}
	    				}
	    			  }
	            	}
	            }
	            System.out.println("wrongAnsListInFinalizedQQ Size:"+wrongAnsListInFinalizedQQ.size());
            }catch(Exception e){
            	e.printStackTrace();
            }
 			
            for (TeacherAnswerDetailsForDistrictSpecificQuestions answerDetailsForDistrictSpecificQuestion : answerDetailsForDistrictSpecificQuestions) {
				answerDetailsForDistrictSpecificQuestion.setTeacherDetail(teacherDetail);
				answerDetailsForDistrictSpecificQuestion.setDistrictMaster(jobOrder.getDistrictMaster());
				answerDetailsForDistrictSpecificQuestion.setCreatedDateTime(new Date());

				ansId = map.get(answerDetailsForDistrictSpecificQuestion.getDistrictSpecificQuestions().getQuestionId());
				
				if(ansId!=null)
				{
					System.out.println("ansId ||||  "+ansId);
					answerDetailsForDistrictSpecificQuestion.setAnswerId(ansId);
				}
				if(!answerDetailsForDistrictSpecificQuestion.getQuestionType().equalsIgnoreCase("ml"))
				{
					if(answerDetailsForDistrictSpecificQuestion.getIsValidAnswer()==false)
					{
						inValidCount++;
						wrongAnswerDetailsForDistrictSpecificQuestionList.add(answerDetailsForDistrictSpecificQuestion);
					}
					Integer selectedOption=null;
					if(answerDetailsForDistrictSpecificQuestion.getSelectedOptions()!=null && answerDetailsForDistrictSpecificQuestion.getSelectedOptions()!="")
						try{
							selectedOption = Integer.parseInt(answerDetailsForDistrictSpecificQuestion.getSelectedOptions());
						}
					catch(Exception ee)
					{
						ee.printStackTrace();
					}
					if(selectedOption!=null)
					{
						OptionsForDistrictSpecificQuestions	optionsForDistrictSpecificQuestions=optionsForDistrictSpecificQuestionsDAO.findById(selectedOption, false, false);
						if(optionsForDistrictSpecificQuestions.getRequiredExplanation()!=null)
						{
						if(optionsForDistrictSpecificQuestions.getRequiredExplanation()==false){
							answerDetailsForDistrictSpecificQuestion.setInsertedText("");
						}
					}
					}
				}
				answerDetailsForDistrictSpecificQuestion.setIsActive(true);
				//System.out.println("@@@@@@@@@ hafeez"+answerDetailsForDistrictSpecificQuestion.getSelectedOptions());
				
				//teacherAnswerDetailsForDistrictSpecificQuestionsDAO.makePersistent(answerDetailsForDistrictSpecificQuestion);
				teacherAnswerDetailsForDistrictSpecificQuestionsDAO.getSession().merge(answerDetailsForDistrictSpecificQuestion);
				answers.add(answerDetailsForDistrictSpecificQuestion.getAnswerId());
			}

			if(answers.size()>0)
				teacherAnswerDetailsForDistrictSpecificQuestionsDAO.updateOtherQQ(teacherDetail,jobOrder.getDistrictMaster(),answers);

			List<JobForTeacher> lstJobForTeacher= null;
			JobForTeacher jobForTeacher = null;
			lstJobForTeacher = jobForTeacherDAO.findJobByTeacherAndSatusFormJobId(teacherDetail,jobOrder);
			boolean validFlag = false;

			if(inValidCount>0)
				validFlag=false;
			else
				validFlag=true;

			if(lstJobForTeacher==null || lstJobForTeacher.size()==0)
			{
				session.setAttribute("jobValid"+jobOrder.getJobId(),validFlag);
				PrintOnConsole.debugPrintln("setDistrictQuestions ... Not save in JFT");
			}else
			{
				PrintOnConsole.debugPrintln("setDistrictQuestions ... already saved in JFT");

				jobForTeacher  = lstJobForTeacher.get(0);
				jobForTeacher.setFlagForDistrictSpecificQuestions(validFlag);
				////////////////////Checking status to be set///////////////////////
				StatusMaster statusMaster = null;
				boolean baseRequired = jobOrder.getJobCategoryMaster().getBaseStatus();
				boolean isJobAssessment = jobOrder.getIsJobAssessment();
				boolean portfolioRequired = jobOrder.getIsPortfolioNeeded(); 
				boolean completeFlag=false;
				//statusMaster = assessmentDetailDAO.findByTeacherIdJobStaus(jobForTeacher, isAffilated,internalFlag,offerEPI,offerJSI,isPortfolio);
				int[] flagList=assessmentDetailDAO.getInternalTeacherFalgs(jobForTeacher) ;
				statusMaster = assessmentDetailDAO.findByTeacherIdJobStaus(jobForTeacher,flagList);
				try{
					String jftStatus=jobForTeacher.getStatus().getStatusShortName();
					if(!jftStatus.equalsIgnoreCase("comp") && statusMaster.getStatusShortName().equalsIgnoreCase("comp")){
						completeFlag=true;
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				if(jobForTeacher.getInternalSecondaryStatus()!=null){
					jobForTeacher.setStatus(jobForTeacher.getInternalStatus());
					jobForTeacher.setStatusMaster(null);
					jobForTeacher.setSecondaryStatus(jobForTeacher.getInternalSecondaryStatus());
				}else if(jobForTeacher.getInternalStatus()!=null){
					jobForTeacher.setStatus(jobForTeacher.getInternalStatus());
					jobForTeacher.setStatusMaster(jobForTeacher.getInternalStatus());
				}else{
					try{
						if(jobForTeacher.getStatus()!=null && (jobForTeacher.getStatus().getStatusShortName().equals("icomp")|| jobForTeacher.getStatus().getStatusShortName().equals("comp") || jobForTeacher.getStatus().getStatusShortName().equals("vlt"))){
							jobForTeacher.setStatus(statusMaster);
							if(jobForTeacher.getSecondaryStatus()!=null){
								jobForTeacher.setStatusMaster(null);
							}else{
								jobForTeacher.setStatusMaster(statusMaster);
							}
						}
					}catch(Exception e){}
				}

				PrintOnConsole.debugPrintln("try to set setIsAffilated flag in setDistrictQuestions");
				if (session != null && session.getAttribute("isCurrentEmploymentNeeded") != null) 
				{
					boolean isCurrentEmploymentNeeded=false;
					isCurrentEmploymentNeeded=(Boolean)session.getAttribute("isCurrentEmploymentNeeded");
					PrintOnConsole.debugPrintln("setDistrictQuestions isCurrentEmploymentNeeded "+isCurrentEmploymentNeeded);
					if(isCurrentEmploymentNeeded)
					{
						TeacherPersonalInfo teacherPersonalInfo=teacherPersonalInfoDAO.findTeacherPersonalInfoByTeacher(teacherDetail);
						if(teacherPersonalInfo!=null && teacherPersonalInfo.getEmployeeType()!=null && teacherPersonalInfo.getEmployeeType()==1)
							jobForTeacher.setIsAffilated(1);
						else
							jobForTeacher.setIsAffilated(0);
					}
					session.removeAttribute("isCurrentEmploymentNeeded");
				}

				jobForTeacherDAO.makePersistent(jobForTeacher);
				////////////////////////////////////
				try{
					if(completeFlag && jobForTeacher.getStatus()!=null && jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("comp")){
						commonService.futureJobStatus(jobOrder, teacherDetail,jobForTeacher);
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			}
            //ravindra qq update
			List<TeacherAnswerDetailsForDistrictSpecificQuestions> wrongAnswerDetailsForDistrictSpecificQuestionList1 = null; 
			wrongAnswerDetailsForDistrictSpecificQuestionList1 = new ArrayList<TeacherAnswerDetailsForDistrictSpecificQuestions>();				
			if(wrongAnswerDetailsForDistrictSpecificQuestionList.size()>0 && wrongAnsListInFinalizedQQ.size()>0){
				wrongAnswerDetailsForDistrictSpecificQuestionList1.addAll(wrongAnswerDetailsForDistrictSpecificQuestionList);
				System.out.println("wrongAnswerDetailsForDistrictSpecificQuestionList1 after addAll-->"+wrongAnswerDetailsForDistrictSpecificQuestionList1.size());
				wrongAnswerDetailsForDistrictSpecificQuestionList1.removeAll(wrongAnsListInFinalizedQQ);
			}
			System.out.println("wrongAnswerDetailsForDistrictSpecificQuestionList1 after Remove-->"+wrongAnswerDetailsForDistrictSpecificQuestionList1.size());
			
			if(lstJobForTeacher1.size()>0)
			{			
					for(JobForTeacher jft:lstJobForTeacher1)
					{
						if(jft.getIsDistrictSpecificNoteFinalize()!=null && jft.getIsDistrictSpecificNoteFinalize()){

							if(validFlag==true)
							{
								jft.setFlagForDistrictSpecificQuestions(validFlag);
								jft.setIsDistrictSpecificNoteFinalize(false);
							}
							else
							{																

								if(QQFlag==true && wrongAnswerDetailsForDistrictSpecificQuestionList1.size()==0)
								{
									jft.setFlagForDistrictSpecificQuestions(true);	
								}
								else
								{
									jft.setFlagForDistrictSpecificQuestions(validFlag);
									jft.setIsDistrictSpecificNoteFinalize(validFlag);
								}

							}
						}
						else
						{
							jft.setFlagForDistrictSpecificQuestions(validFlag);								
						}
						jobForTeacherDAO.makePersistent(jft);
					}				
			}
			
			System.out.println("-----------------------------------Valid Flag:- "+validFlag+" and inValidCount:- "+inValidCount+"-------------------------------------------------------");
			if(!validFlag || inValidCount>0)
				sendMailOnNegativeQQ(jobOrder, wrongAnswerDetailsForDistrictSpecificQuestionList);
			
			return "1";

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;

	}
	
	@Transactional(readOnly=false)
	public String setDistrictQuestionsBySetONB(TeacherAnswerDetailsForDistrictSpecificQuestions[] answerDetailsForDistrictSpecificQuestions,QqQuestionSets qqQuestionSets, Integer teacherId)
	{
		System.out.println(" *** ::::::::::::::::::::setDistrictQuestions:::::::::::::::::::"+answerDetailsForDistrictSpecificQuestions.length);
		//System.out.println(":::::::::::::::Assessment setDistrictQuestions:::::::::::::::::::;; ");

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession();
		TeacherDetail teacherDetail =teacherDetailDAO.findById(teacherId,false,false);
		TeacherPersonalInfo teacherPersonalInfo =null;
		boolean updatePersonalInfo=false;
		try {
			int inValidCount = 0;
			List<TeacherAnswerDetailsForDistrictSpecificQuestions> wrongAnswerDetailsForDistrictSpecificQuestionList = new ArrayList<TeacherAnswerDetailsForDistrictSpecificQuestions>();
			JobOrder jobOrder = answerDetailsForDistrictSpecificQuestions[0].getJobOrder();
			jobOrder = jobOrderDAO.findById(jobOrder.getJobId(), false, false);
			QqQuestionSets qqQuestionSets2 = new QqQuestionSets();			
			if(jobOrder.getJobCategoryMaster()!=null && jobOrder.getJobCategoryMaster().getQuestionSetsForOnboarding()!=null && !jobOrder.getJobCategoryMaster().getQuestionSetsForOnboarding().equals(0)){
				qqQuestionSets2 = jobOrder.getJobCategoryMaster().getQuestionSetsForOnboarding();
			}
			Map<Integer,Integer> map = new HashMap<Integer, Integer>();
			try {
				map = teacherAnswerDetailsForDistrictSpecificQuestionsDAO.findTeacherAnswersForQQByDistrictSetONB(teacherDetail,qqQuestionSets);
			} catch (Exception e) {
				e.printStackTrace();
			}
			List<Integer> answers = new ArrayList<Integer>();
			Integer ansId = null;
			Boolean QQFlag=false;
			if(jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId().equals(806900)){
			 teacherPersonalInfo = teacherPersonalInfoDAO.findById(teacherId, false, false);
			}
			
			List<TeacherAnswerDetailsForDistrictSpecificQuestions> wrongAnsListInFinalizedQQ = new ArrayList<TeacherAnswerDetailsForDistrictSpecificQuestions>();
			List<JobForTeacher> lstJobForTeacher1= null;
			lstJobForTeacher1=new ArrayList<JobForTeacher>();
			//lstJobForTeacher1 = jobForTeacherDAO.findByTeacherDistrictAndJobCategory(teacherDetail, jobOrder.getDistrictMaster(), jobOrder.getJobCategoryMaster());
			lstJobForTeacher1 = jobForTeacherDAO.findByTeacherAndQuestionSet(teacherDetail,jobOrder.getJobCategoryMaster().getQuestionSets());
            System.out.println("lstJobForTeacher Size Ctaegory Wise"+lstJobForTeacher1.size());
            
 			
			for (TeacherAnswerDetailsForDistrictSpecificQuestions answerDetailsForDistrictSpecificQuestion : answerDetailsForDistrictSpecificQuestions) {
				answerDetailsForDistrictSpecificQuestion.setTeacherDetail(teacherDetail);
				answerDetailsForDistrictSpecificQuestion.setDistrictMaster(jobOrder.getDistrictMaster());
				answerDetailsForDistrictSpecificQuestion.setCreatedDateTime(new Date());

				ansId = map.get(answerDetailsForDistrictSpecificQuestion.getDistrictSpecificQuestions().getQuestionId());
				
				if(ansId!=null)
				{
					System.out.println("ansId ||||  "+ansId);
					answerDetailsForDistrictSpecificQuestion.setAnswerId(ansId);
				}
				if(!answerDetailsForDistrictSpecificQuestion.getQuestionType().equalsIgnoreCase("ml"))
				{
					if(answerDetailsForDistrictSpecificQuestion.getIsValidAnswer()==false)
					{
						inValidCount++;
						wrongAnswerDetailsForDistrictSpecificQuestionList.add(answerDetailsForDistrictSpecificQuestion);
					}
				}
				if(jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId().equals(806900)){
					String questionFromAccepOrDecline=	answerDetailsForDistrictSpecificQuestion.getQuestion().toLowerCase();
					if(questionFromAccepOrDecline!=null)
						if(questionFromAccepOrDecline.contains("what is your social security number?")){
							String encryptedText=	Utility.encodeInBase64(answerDetailsForDistrictSpecificQuestion.getInsertedText());
								if(teacherPersonalInfo!=null){
									teacherPersonalInfo.setSSN(encryptedText);
									updatePersonalInfo=true;
									}
								answerDetailsForDistrictSpecificQuestion.setInsertedText(encryptedText);
						}
				}
				answerDetailsForDistrictSpecificQuestion.setIsActive(true);
				answerDetailsForDistrictSpecificQuestion.setIsonboarding(true);
			
				teacherAnswerDetailsForDistrictSpecificQuestionsDAO.getSession().merge(answerDetailsForDistrictSpecificQuestion);
				answers.add(answerDetailsForDistrictSpecificQuestion.getAnswerId());
			}

			if(answers.size()>0)
				teacherAnswerDetailsForDistrictSpecificQuestionsDAO.updateOtherQQ(teacherDetail,jobOrder.getDistrictMaster(),answers);

			List<JobForTeacher> lstJobForTeacher= null;
			JobForTeacher jobForTeacher = null;
			lstJobForTeacher = jobForTeacherDAO.findJobByTeacherAndSatusFormJobId(teacherDetail,jobOrder);
			boolean validFlag = false;
			if(updatePersonalInfo){
				teacherPersonalInfoDAO.updatePersistent(teacherPersonalInfo);
			}
			if(inValidCount>0)
				validFlag=false;
			else
				validFlag=true;

            //ravindra qq update
			List<TeacherAnswerDetailsForDistrictSpecificQuestions> wrongAnswerDetailsForDistrictSpecificQuestionList1 = null; 
			wrongAnswerDetailsForDistrictSpecificQuestionList1 = new ArrayList<TeacherAnswerDetailsForDistrictSpecificQuestions>();				
			if(wrongAnswerDetailsForDistrictSpecificQuestionList.size()>0 && wrongAnsListInFinalizedQQ.size()>0){
				wrongAnswerDetailsForDistrictSpecificQuestionList1.addAll(wrongAnswerDetailsForDistrictSpecificQuestionList);
				System.out.println("wrongAnswerDetailsForDistrictSpecificQuestionList1 after addAll-->"+wrongAnswerDetailsForDistrictSpecificQuestionList1.size());
				wrongAnswerDetailsForDistrictSpecificQuestionList1.removeAll(wrongAnsListInFinalizedQQ);
			}
			System.out.println("wrongAnswerDetailsForDistrictSpecificQuestionList1 after Remove-->"+wrongAnswerDetailsForDistrictSpecificQuestionList1.size());
			
			if(lstJobForTeacher1.size()>0)
			{			
					for(JobForTeacher jft:lstJobForTeacher1)
					{
						if(jft.getIsDistrictSpecificNoteFinalize()!=null && jft.getIsDistrictSpecificNoteFinalize()){

							if(validFlag==true)
							{
								jft.setFlagForDistrictSpecificQuestionsONB(validFlag);
								jft.setIsDistrictSpecificNoteFinalizeONB(false);
							}
							else
							{																

								if(QQFlag==true && wrongAnswerDetailsForDistrictSpecificQuestionList1.size()==0)
								{
									jft.setFlagForDistrictSpecificQuestionsONB(true);	
								}
								else
								{
									jft.setFlagForDistrictSpecificQuestionsONB(validFlag);
									jft.setIsDistrictSpecificNoteFinalizeONB(validFlag);
								}

							}
						}
						else
						{
							jft.setFlagForDistrictSpecificQuestionsONB(validFlag);								
						}
						jobForTeacherDAO.makePersistent(jft);
					}				
			}
			
			System.out.println("-----------------------------------Valid Flag:- "+validFlag+" and inValidCount:- "+inValidCount+"-------------------------------------------------------");
			return "1";

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;

	}


//added By Ram Nath
	@Transactional(readOnly=false)
	public String getQQDistrictSpecificQuestionONB(Integer jobId,Integer teacherId)
	{
		System.out.println("*****:::::::::::::::Assessment getQQDistrictSpecificQuestionONB:::::::::::::::::::;; ");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		StringBuffer sb =new StringBuffer();
		try{
			List<DistrictSpecificQuestions> districtSpecificQuestionList	=	null; 
			TeacherDetail teacherDetail = null;
			teacherDetail =teacherDetailDAO.findById(teacherId,false,false);

			if(jobId!=0)
			{
				JobOrder jobOrder=jobOrderDAO.findById(jobId, false, false);
				JobCategoryMaster jobCategoryMaster = jobOrder.getJobCategoryMaster();
				QqQuestionSets qqQuestionSets = new QqQuestionSets();
				List<QqQuestionsetQuestions> qqQuestionsetQuestions=null;
				if(jobCategoryMaster!=null && jobCategoryMaster.getQuestionSets()!=null){
					qqQuestionSets = jobCategoryMaster.getQuestionSetsForOnboarding();
					qqQuestionsetQuestions = qqQuestionsetQuestionsDAO.findByQuestionSet(jobCategoryMaster.getQuestionSetsForOnboarding()); 
				}
				List<Integer> tempDistSpecQues = new ArrayList<Integer>();
				if(qqQuestionsetQuestions!=null && qqQuestionsetQuestions.size()>0){
					for (QqQuestionsetQuestions qqQuestionsetQuestions2 : qqQuestionsetQuestions) {
						tempDistSpecQues.add(qqQuestionsetQuestions2.getDistrictSpecificQuestions().getQuestionId());
					}
				}
				JobForTeacher jft = jobForTeacherDAO.findJFTByTeacherAndJob(teacherDetail, jobOrder);
				//districtSpecificQuestionList=districtSpecificQuestionsDAO.getDistrictSpecificQuestionBYDistrict(jobOrder.getDistrictMaster());
				System.out.println(">.>.> 1");
				if(tempDistSpecQues.size()>0)
				districtSpecificQuestionList=districtSpecificQuestionsDAO.getDistrictSpecificQuestionBYQues(tempDistSpecQues);
				System.out.println(">.>.> 2");
				List<JobOrder> jobOrders = teacherAnswerDetailsForDistrictSpecificQuestionsDAO.findLastTeacherAnswersByDistrictAndJobONB(teacherDetail,jobOrder,true);
				System.out.println(">.>.> 3");
				List<TeacherAnswerDetailsForDistrictSpecificQuestions> lastList = new ArrayList<TeacherAnswerDetailsForDistrictSpecificQuestions>();
				System.out.println(">.>.> 4");
				
				System.out.println("jobOrders=="+jobOrders.size()+"   districtSpecificQuestionList=="+districtSpecificQuestionList.size()+"  lastList=="+lastList.size());
				if(jobOrders.size()>0)
					lastList = teacherAnswerDetailsForDistrictSpecificQuestionsDAO.findTeacherAnswersBySetQuesONB(teacherDetail,qqQuestionSets,true);

				Map<Integer, TeacherAnswerDetailsForDistrictSpecificQuestions> map = new HashMap<Integer, TeacherAnswerDetailsForDistrictSpecificQuestions>();

				for(TeacherAnswerDetailsForDistrictSpecificQuestions teacherAnswerDetailsForDistrictSpecificQuestion : lastList)
					map.put(teacherAnswerDetailsForDistrictSpecificQuestion.getDistrictSpecificQuestions().getQuestionId(), teacherAnswerDetailsForDistrictSpecificQuestion);

				TeacherAnswerDetailsForDistrictSpecificQuestions teacherAnswerDetailsForDistrictSpecificQuestion = null;
				List<OptionsForDistrictSpecificQuestions> questionOptionsList = null;
				String questionInstruction = null;
				String shortName = "";
				int totalQuestions =0;
				if(tempDistSpecQues.size()>0)
					totalQuestions = districtSpecificQuestionList.size();
				int cnt = 0;

				DistrictMaster districtMaster=null;
				if(jobOrder.getDistrictMaster()!=null){
					districtMaster=jobOrder.getDistrictMaster();
				}
				List<InternalTransferCandidates> itcList=internalTransferCandidatesDAO.getInternalCandidateByDistrict(teacherDetail,districtMaster);
				boolean internalFlag=false;
				boolean getQQ=true;
				if(itcList.size()>0){
					internalFlag=true;
				}
				if(districtMaster!=null && internalFlag){
					if(districtMaster.getOfferQualificationItems()){
						getQQ=true;
					}else{
						getQQ=false;
					}
				}				
				boolean focus = false;
				if(getQQ){
					sb.append("<input type='hidden' name='totalQuestions' value='"+totalQuestions+"'/>" );
					for (DistrictSpecificQuestions districtSpecificQuestion : districtSpecificQuestionList) 
					{
						shortName = districtSpecificQuestion.getQuestionTypeMaster().getQuestionTypeShortName();

						sb.append("<tr>");
						sb.append("<td width='90%'>");
						sb.append("<div><b>Question "+(++cnt)+" of "+totalQuestions+"</b></div>");

						questionInstruction = districtSpecificQuestion.getQuestionInstructions()==null?"":districtSpecificQuestion.getQuestionInstructions();
//						if(!questionInstruction.equals(""))
//							sb.append(""+Utility.getUTFToHTML(questionInstruction)+"");
						sb.append("<div id='Q"+cnt+"question'>"+Utility.getUTFToHTML(districtSpecificQuestion.getQuestion())+"</div>");

						questionOptionsList = districtSpecificQuestion.getQuestionOptions();

						teacherAnswerDetailsForDistrictSpecificQuestion = map.get(districtSpecificQuestion.getQuestionId());

						String checked = "";
						Integer optId = 0;
						String insertedText = "";
						if(teacherAnswerDetailsForDistrictSpecificQuestion!=null)
						{
							if(shortName.equalsIgnoreCase(teacherAnswerDetailsForDistrictSpecificQuestion.getQuestionType()))
							{
								if(teacherAnswerDetailsForDistrictSpecificQuestion.getSelectedOptions()!=null)
									optId = Integer.parseInt(teacherAnswerDetailsForDistrictSpecificQuestion.getSelectedOptions());

								insertedText = teacherAnswerDetailsForDistrictSpecificQuestion.getInsertedText();
							}
						}
						if(shortName.equalsIgnoreCase("tf") || shortName.equalsIgnoreCase("slsel"))
						{
							sb.append("<table>");
							for (OptionsForDistrictSpecificQuestions questionOptions : questionOptionsList) {
								if(optId.equals(questionOptions.getOptionId()))
									checked = "checked";
								else
									checked = "";

								sb.append("<tr><td style='border:0px;background-color: transparent;'><div  style='display: block;min-height: 20px;padding-left: 5px;vertical-align: middle;'><input type='radio' name='Q"+cnt+"opt' value='"+questionOptions.getOptionId()+"' "+checked+" /></div>" +
										" <input type='hidden' id='Q"+questionOptions.getOptionId()+"validQuestion' value='"+questionOptions.getValidOption()+"' ></td>" +
										"<td style='border:0px;background-color: transparent;padding-left:5px;' id='qOpt"+questionOptions.getOptionId()+"'>"+questionOptions.getQuestionOption()+" ");
								sb.append("</td></tr>");
							}
							sb.append("</table>");
						}
						if(shortName.equalsIgnoreCase("et"))
						{
							String display="none";
							sb.append("<table>");
							for (OptionsForDistrictSpecificQuestions questionOptions : questionOptionsList) {
								if(optId.equals(questionOptions.getOptionId()))
									checked = "checked";
								else
									checked = "";

								sb.append("<tr><td style='border:0px;background-color: transparent;'><div  style='display: block;min-height: 20px;padding-left: 5px;vertical-align: middle;'><input type='radio' name='Q"+cnt+"opt' value='"+questionOptions.getOptionId()+"' "+checked+" onclick='getQuestionEvent("+"\"Q"+cnt+"optet\","+"\"Q"+questionOptions.getOptionId()+"requiredExplanation\");'"+"/></div>" +
										" <input type='hidden' id='Q"+questionOptions.getOptionId()+"validQuestion' value='"+questionOptions.getValidOption()+"' ></td>" +
										"<td style='border:0px;background-color: transparent;padding-left:5px;'  id='qOpt"+questionOptions.getOptionId()+"'>"+questionOptions.getQuestionOption()+" ");
								sb.append("</td></tr>");
								sb.append(" <input type='hidden' id='Q"+questionOptions.getOptionId()+"requiredExplanation' value='"+questionOptions.getRequiredExplanation()+"' >");
								if(questionOptions.getRequiredExplanation()!=null){
									if(questionOptions.getRequiredExplanation()==true){
										if(checked.equals("checked")){
											display="block";
											}
										}
									}
							}
							sb.append("</table>");

							if(districtSpecificQuestion.getQuestionExplanation()!=null)
								sb.append("<div>"+Utility.getUTFToHTML(districtSpecificQuestion.getQuestionExplanation())+"</div>");

							sb.append("&nbsp;&nbsp;<span style=''><textarea  name='Q"+cnt+"opt' id='Q"+cnt+"optet' class='form-control' maxlength='5000' rows='8' style='width:98%;margin:5px;display:"+display+";'>"+insertedText+"</textarea><span><br/>");
							if(focus==false)
							{
								sb.append("<script type=\"text/javascript\" language=\"javascript\">");
								//sb.append("document.getElementById('Q"+cnt+"optet').focus();");
								sb.append("</script>");
								focus = true;
							}
							sb.append("<script type=\"text/javascript\" language=\"javascript\">");
							//sb.append("document.getElementById('Q"+cnt+"optet').focus();");
							sb.append("</script>");
						}
						if(shortName.equalsIgnoreCase("ml"))
						{
							if(districtMaster!=null && districtMaster.getDistrictId().equals(806900)){
								if(districtSpecificQuestion.getQuestion().toLowerCase().contains("what is your social security number?")){
									insertedText=Utility.decodeBase64(insertedText);
									sb.append("&nbsp;&nbsp;<input type='text' name='Q"+cnt+"opt' id='Q"+cnt+"opt' class='form-control' maxlength='9' r style='width:20%;margin:5px;' onkeypress='return checkForInt(event);'/>"+insertedText+"<br/>");					
									} else{
										sb.append("&nbsp;&nbsp;<textarea name='Q"+cnt+"opt' id='Q"+cnt+"opt' class='form-control' maxlength='5000' rows='8' style='width:98%;margin:5px;'>"+insertedText+"</textarea><br/>");						
									}
							} else{
								sb.append("&nbsp;&nbsp;<textarea name='Q"+cnt+"opt' id='Q"+cnt+"opt' class='form-control' maxlength='5000' rows='8' style='width:98%;margin:5px;'>"+insertedText+"</textarea><br/>");						
							}
							if(focus==false)
							{
								sb.append("<script type=\"text/javascript\" language=\"javascript\">");
								//sb.append("document.getElementById('Q"+cnt+"opt').focus();");
								sb.append("</script>");
								focus = true;
							}
							sb.append("<script type=\"text/javascript\" language=\"javascript\">");
							//sb.append("document.getElementById('Q"+cnt+"opt').focus();");
							sb.append("</script>");
						}
						sb.append("<input type='hidden' id='Q"+cnt+"questionId' value='"+districtSpecificQuestion.getQuestionId()+"'/>" );
						sb.append("<input type='hidden' id='Q"+cnt+"questionTypeId' value='"+districtSpecificQuestion.getQuestionTypeMaster().getQuestionTypeId()+"'/>" );
						sb.append("<input type='hidden' id='Q"+cnt+"questionTypeShortName' value='"+shortName+"'/>" );
						
						

						sb.append("</td>");
						sb.append("</tr>");
						insertedText = "";
					}
				}
				if(jobCategoryMaster.getQuestionSets()!=null && !jobCategoryMaster.getQuestionSets().getID().equals(0))
					sb.append("<input type='hidden' id='qqQuestionSetID' value='"+jobCategoryMaster.getQuestionSets().getID()+"'/>" );
				else
					sb.append("<input type='hidden' id='qqQuestionSetID' value=''/>" );
				sb.append("@##@"+cnt+"@##@"+jobOrder.getDistrictMaster().getTextForDistrictSpecificQuestions());
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	}
	
	public void sendMailToAllDAAfterEpiComplition(UserMasterDAO userMasterDAO,DistrictMaster districtMaster,TeacherDetail teacherDetail,JobOrder epiJobOrder,String EpiVvi)
	{
		System.out.println("000001");
		List<UserMaster> userMasterList = new ArrayList<UserMaster>(); 
		try{
			if(districtMaster!=null){
				userMasterList = userMasterDAO.getUserByDistrict(districtMaster);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		if(userMasterList.size()>0){
			emailerService = userMasterDAO.getemEmailerService();
			for(UserMaster userMaster : userMasterList)
			{
				String emailSubjectString = teacherDetail.getFirstName()+" "+teacherDetail.getLastName()+" has completed "+EpiVvi;
				String emailBodyTextString = MailText.getEpiVviCompletionDAMailText(teacherDetail,userMaster,EpiVvi,epiJobOrder.getJobId(),epiJobOrder.getJobTitle());
				System.out.println("Before DemoScheduleMailThred");
				DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
				System.out.println("After DemoScheduleMailThred");
				dsmt.setEmailerService(emailerService);
				System.out.println("after DemoScheduleMailThred");
				dsmt.setMailto(userMaster.getEmailAddress());
				dsmt.setMailsubject(emailSubjectString);
				dsmt.setMailcontent(emailBodyTextString);
				try {
					System.out.println(" thread start :: 111111111111111111111111111111");
					dsmt.start();
					System.out.println(" thread end :: 222222222222222222222222222222");
				} catch (Exception e) {e.printStackTrace();}
			}
		}
	}
	
	
	public void sendMailToAllDAAfterVVIComplitionForEvent(UserMasterDAO userMasterDAO,DistrictMaster districtMaster,TeacherDetail teacherDetail,I4InterviewInvites i4InterviewInvites,String EpiVvi)
	{
		System.out.println("000001");
		List<UserMaster> userMasterList = new ArrayList<UserMaster>(); 
		List<EventParticipantsList> eventParticipantsList  = null;
		String eventName = "";
		
		try{
			if(districtMaster!=null){
				userMasterList = userMasterDAO.getUserByDistrict(districtMaster);
			}
			
			eventParticipantsList = eventParticipantsListDAO.findeventParticipantsListByI4InterviewId(i4InterviewInvites);
			if(eventParticipantsList!=null && eventParticipantsList.get(0).getEventDetails()!=null)
				eventName = eventParticipantsList.get(0).getEventDetails().getEventName();
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		if(userMasterList.size()>0){
			emailerService = userMasterDAO.getemEmailerService();
			for(UserMaster userMaster : userMasterList)
			{
				String emailSubjectString = teacherDetail.getFirstName()+" "+teacherDetail.getLastName()+" has completed "+EpiVvi;
				String emailBodyTextString = MailText.getEpiVviCompletionDAMailText(teacherDetail,userMaster,EpiVvi,0,eventName);
				System.out.println("Before DemoScheduleMailThred");
				DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
				System.out.println("After DemoScheduleMailThred");
				dsmt.setEmailerService(emailerService);
				System.out.println("after DemoScheduleMailThred");
				dsmt.setMailto(userMaster.getEmailAddress());
				dsmt.setMailsubject(emailSubjectString);
				dsmt.setMailcontent(emailBodyTextString);
				try {
					System.out.println(" thread start :: 111111111111111111111111111111");
					dsmt.start();
					System.out.println(" thread end :: 222222222222222222222222222222");
				} catch (Exception e) {e.printStackTrace();}
			}
		}
	}
	
	
	
	public void sendMailToCandidate(Integer assessmentType,String status,Integer jobId,Integer epiJobId)
	{
		System.out.println(":::::::::::::::::::AssessmentCampaignAjax : sendMailToCandidate:::::::::::::::::::");
		WebContext context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		TeacherDetail teacherDetail = null;
		if(session == null || session.getAttribute("teacherDetail") == null)
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		else
		{
			teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");
			String inventoryName = "EPI";
			JobOrder jobOrder = null;
			DistrictMaster districtMaster = null;
			String epiSetting = null;
			String jsiSetting = null;
			boolean isMailSend = false;
			String emailBodyText = null;
			
			if(assessmentType==1){
				if(epiJobId!=null && epiJobId>0){
					jobOrder = jobOrderDAO.findById(epiJobId, false, false);
				}
			}
			else if(assessmentType==2){
				inventoryName = "JSI";
				if(jobId!=null && jobId>0){
					jobOrder = jobOrderDAO.findById(jobId, false, false);
				}
			}
			if(jobOrder!=null){
				if(jobOrder.getDistrictMaster()!=null){
					districtMaster = jobOrder.getDistrictMaster();
				}
				if(districtMaster!=null){
					if(districtMaster.getEpiSetting()!=null && districtMaster.getEpiSetting()!=""){
						epiSetting = districtMaster.getEpiSetting();
					}
					if(districtMaster.getJsiSetting()!=null && districtMaster.getJsiSetting()!=""){
						jsiSetting = districtMaster.getJsiSetting();
					}
				}
			}
			if(assessmentType==1){
				if(epiSetting!=null && epiSetting!=""){
					if(epiSetting.equalsIgnoreCase("2") && (status.equalsIgnoreCase("strt") || status.equalsIgnoreCase("comp") || status.equalsIgnoreCase("vlt1") || status.equalsIgnoreCase("vlt2") || status.equalsIgnoreCase("vlt"))){
						isMailSend = true;
						emailBodyText = MailText.getAssessmentNotificationsMailText(teacherDetail,assessmentType,status);
					}
					else if(status.equalsIgnoreCase("comp") && (epiSetting.equalsIgnoreCase("1") || epiSetting.equalsIgnoreCase("2"))){
						isMailSend = true;
						emailBodyText = MailText.getAssessmentNotificationsMailText(teacherDetail,assessmentType,status);
					}
					else if(epiSetting.equalsIgnoreCase("0")){
						isMailSend = false;
						emailBodyText = null;
					}
				}
			}
			else if(assessmentType==2){
				if(jsiSetting!=null && jsiSetting!=""){
					if(jsiSetting.equalsIgnoreCase("2") && (status.equalsIgnoreCase("strt") || status.equalsIgnoreCase("comp") || status.equalsIgnoreCase("vlt1") || status.equalsIgnoreCase("vlt2") || status.equalsIgnoreCase("vlt"))){
						isMailSend = true;
						emailBodyText = MailText.getAssessmentNotificationsMailText(teacherDetail,assessmentType,status);
					}
					else if(status.equalsIgnoreCase("comp") && (jsiSetting.equalsIgnoreCase("1") || jsiSetting.equalsIgnoreCase("2"))){
						isMailSend = true;
						emailBodyText = MailText.getAssessmentNotificationsMailText(teacherDetail,assessmentType,status);
					}
					else if(jsiSetting.equalsIgnoreCase("0")){
						isMailSend = false;
						emailBodyText = null;
					}
				}
			}
			if(isMailSend && emailBodyText!=null){
				String emailSubject = inventoryName+" Notification";
				DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
				dsmt.setEmailerService(emailerService);
				dsmt.setMailto(teacherDetail.getEmailAddress());
				dsmt.setMailsubject(emailSubject);
				dsmt.setMailcontent(emailBodyText);
				try {
					dsmt.start();
				} catch (Exception e) {e.printStackTrace();}
			}
		}
	}
}
