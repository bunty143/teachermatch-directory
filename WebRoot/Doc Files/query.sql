------------------------------------------------------------------------------------------------------
------------------------------ Updated SQL Query on Platform Server 14-12-2015 -----------------------
------------------------------------------------------------------------------------------------------
------------------------- Ankit Sharma(05-12-2015)-------------------
 ALTER TABLE `statusnodecolorhistory` ADD FOREIGN KEY ( `mqEventId` ) REFERENCES `mqevent` ( `eventId` ) 
 
 ------------------------- Sonu Gupta(05-12-2015)-------------------
 ALTER TABLE `dspqportfolioname` DROP COLUMN `jobCategoryId`, DROP FOREIGN KEY `dspqportfolioname _ibfk_2`;
 ALTER TABLE `dspqportfolioname` DROP `jobCategory`,DROP `isDeleted`;
DROP TABLE `dspqportfolionamejc`;

ALTER TABLE  `dspqfieldmaster` ADD  `fieldByOrder` VARCHAR( 4 ) NULL AFTER  `fieldDataType` ;
------------------------- Deepak(05-12-2015)-------------------
INSERT INTO `readonlycheckpointmaster` (`checkPointId`, `checkPointName`, `createdDate`, `status`) VALUES
(1, 'Candidate Name', '2015-12-05', 'A'),
(2, 'Candidate Email', '2015-12-05', 'A'),
(3, 'SSN', '2015-12-05', 'A'),
(4, 'Position Number', '2015-12-05', 'A'),
(5, 'Hiring For', '2015-12-05', 'A'),
(6, 'Job Type', '2015-12-05', 'A');
ALTER TABLE `readonlycheckpointtransaction`
	CHANGE COLUMN `onBoardingId` `onBoardingId` INT(11) NULL DEFAULT '0' AFTER `checkPointMasterId`,
	ADD COLUMN `districtId` INT(11) NULL DEFAULT '0' AFTER `onBoardingId`,
	CHANGE COLUMN `checkPointName` `checkPointName` VARCHAR(100) NULL DEFAULT '0' AFTER `districtId`,
	CHANGE COLUMN `createdByUser` `createdByUser` VARCHAR(2) NULL DEFAULT '0' AFTER `createdDate`,
	CHANGE COLUMN `ipAddress` `ipAddress` VARCHAR(20) NULL DEFAULT '0' AFTER `modifiedByUser`;
	
------------------------------------Ravindra(07-12-2015)-------------------------------------------------
ALTER TABLE  `statusnodecolorhistory` ADD  `milestoneName` VARCHAR( 20 ) NULL DEFAULT NULL AFTER  `currentColor` ;
ALTER TABLE  `statusnodecolorhistory` CHANGE  `mqEventId`  `mqEventId` INT( 10 ) NULL DEFAULT NULL


	
-----------------------------------------SANDEEP 08-12-15--------------------------------


ALTER TABLE `mqevent` ADD `checkEmailAPI` INT( 1 ) NULL AFTER `eventType` ;


-----------------------------------------Sekhar 12-8-15--------------------------
INSERT INTO `menumaster` (`menuName`, `parentMenuId`, `subMenuId`, `districtId`, `menuArrow`, `pageName`, `pageNameWithSubPage`, `orderBy`, `status`, `description`) VALUES
('Onboarding Dashboard', 149, NULL, NULL, NULL, 'pnrdashboard.do', '|pnrdashboard.do|', 2, 'A', NULL);
INSERT INTO `teachermatch`.`menusectionmaster` (`menuSectionId`, `menuId`, `menuSectionName`) VALUES ('160', '168', 'Onboarding Dashboard');
INSERT INTO `roleaccesspermission` (`roleId`, `menuId`, `menuSectionId`, `optionId`, `status`) VALUES
(11, 168, 160, '|1|2|4|5|7|', 'A');
INSERT INTO `roleaccesspermission` (`roleId`, `menuId`, `menuSectionId`, `optionId`, `status`) VALUES
(13, 168, 160, '|1|2|4|5|7|', 'A');

---------------------------- Amit Chaudhary 09-12-15 (updated on Platform)------------------------------------------------
ALTER TABLE `epitakenlist` ADD `assessmentGroupId` INT( 5 ) NULL DEFAULT NULL COMMENT 'Foreign Key to "assessmentGroupId" in table "assessmentgroupdetails"' AFTER `assessmentId` ;
ALTER TABLE `epitakenlist` ADD FOREIGN KEY (assessmentGroupId) REFERENCES assessmentgroupdetails(assessmentGroupId);




------------------------------------Sandeep 09-12-15-------------------------------

ALTER TABLE `statusnodecolorhistory` CHANGE `ipAddress` `ipAddress` VARCHAR(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL;


--------------------------------SWADESH--------------------------------------
ALTER TABLE `jobcategorymaster` ADD `autoRejectScore` int( 10 ) NULL ;

---------vishwanath 10-12-2015 ----------------------------------------------
ALTER TABLE `districtportfolioconfig` ADD `transcriptUpload` TINYINT( 1 ) NULL DEFAULT '0' AFTER `tfaOptional` ;
--------------------------------------------------


---------------------------- Ram Nath 11-12-15------------------------------------------------
ALTER TABLE employeemaster ADD COLUMN DT VARCHAR(1) DEFAULT NULL AFTER employmentStatus;
----------------------------------------------------------------------------------------------

---------------------------- Amit Chaudhary 11-12-15 (updated on Platform)------------------------------------------------
ALTER TABLE  `teacherassessmentdetails` CHANGE  `assessmentName`  `assessmentName` VARCHAR( 200 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ;
--------------------------------------------------------------------------------------------------------------------------

------------------------------ Ankit Sharma (14-12-2015) -----------------
ALTER TABLE `mqevent` ADD `eRegStatus` VARCHAR( 20 ) NULL AFTER `msgStatus` ;

--------------------------------Ravindra (16-12-2015)------------------------------------
CREATE TABLE `reasonsforwaived` (
	`reasonId` INT NOT NULL AUTO_INCREMENT,
	`reasonName` VARCHAR(75) NOT NULL DEFAULT '0',
	`createdDate` DATE NULL DEFAULT NULL,
	`createdByUser` INT NULL DEFAULT '0',
	`status` VARCHAR(2) NOT NULL DEFAULT '0',
	PRIMARY KEY (`reasonId`)
);

INSERT INTO `reasonsforwaived` (
`reasonId` ,
`reasonName` ,
`createdDate` ,
`createdByUser` ,
`status`
)
VALUES (
NULL ,  'Branch Decision',  '2015-12-16',  '0',  'A'
);

INSERT INTO `reasonsforwaived` (
`reasonId` ,
`reasonName` ,
`createdDate` ,
`createdByUser` ,
`status`
)
VALUES (
NULL ,  'Consider for Non-Instructional Position',  '2015-12-16',  '0',  'A'
);

INSERT INTO `reasonsforwaived` (
`reasonId` ,
`reasonName` ,
`createdDate` ,
`createdByUser` ,
`status`
)
VALUES (
NULL ,  'Customer Request',  '2015-12-16',  '0',  'A'
);

--------------------------------Mukesh (17-12-2015)------------------------------------

INSERT INTO `roleaccesspermission` (`accessId`, `roleId`, `menuId`, `menuSectionId`, `optionId`, `status`) VALUES (NULL, '3', '59', '57', '|2|4|5|7|', 'A');

UPDATE `joborder` SET `writePrivilegeToSchool` = '1' WHERE `districtId` =804800;

------------------------------ Ankit Sharma (17-12-2015) -----------------
 ALTER TABLE `statuswiseautoemailsend` CHANGE `districtId` `districtId` INT( 5 ) NULL 
 
  -----------------------------Ankit Sharma (18-12-2015) ------------------
 ALTER TABLE `statusnodecolorhistory` ADD `updateDateTime` TIMESTAMP NULL AFTER `createdDateTime` ;
 
  ------------------------- Sonu Gupta(21-12-2015)-------------------
 INSERT INTO `dspqsectionmaster` (`sectionId`, `groupId`, `sectionName`, `status`, `sectionByOrder`, `createdDateTime`) VALUES 
 (NULL, '3', 'Certified Teacher', 'A', NULL, NOW());
 
 INSERT INTO `dspqfieldmaster` (`dspqFieldId`, `sectionId`, `dspqFieldName`, `isRequired`, `numberRequired`, `isAdditionalField`, `isDependantField`, `fieldDataType`, `fieldByOrder`, `status`, `createdDateTime`) VALUES 
 (NULL, '29', 'Are you a certified teacher?', '0', '0', '0', NULL, 'Radio Button', NULL, 'A', NOW()), (NULL, '29', 'Years of Certified Teaching Experience', '0', '0', '0', NULL, 'Text Box', NULL, 'A', NOW());
 
 ---------------------------- Ashish choudhary (21-12-2015) -----------------

 UPDATE `menumaster` SET `pageName`="onboardingdashboard.do" ,`pageNameWithSubPage`="|onboardingdashboard.do|" WHERE `menuName`="Onboarding Dashboard";
 
 
 ---------------------------- Ram Nath 22-12-15------------------------------------------------
ALTER TABLE employeemaster CHANGE COLUMN DT DT VARCHAR(1) AFTER PCCode;
----------------------------------------------------------------------------------------------

-----------------------------------Ajay Jain (23-12-2015)--------------------------------
ALTER TABLE  `teachersecondarystatus` ADD  `headQuarterId` INT( 10 ) NULL DEFAULT NULL COMMENT  'Foreign Key to "headQuarterId" in table "headquartermaster"' AFTER  `districtId` ,
ADD  `branchId` INT( 10 ) NULL DEFAULT NULL COMMENT  'Foreign Key to "branchId" in table "branchmaster"' AFTER  `headQuarterId` ;

ALTER TABLE  `teachersecondarystatus` CHANGE  `districtId`  `districtId` INT( 11 ) NULL DEFAULT NULL;

ALTER TABLE  `teachersavedbycandidates` CHANGE  `districtId`  `districtId` INT( 5 ) NULL DEFAULT NULL COMMENT  'Foreign Key to "districtId" in table "districtMaster"';

ALTER TABLE  `teachersavedbycandidates` ADD  `headQuarterId` INT( 10 ) NULL DEFAULT NULL COMMENT  'Foreign Key to "headQuarterId" in table "headquartermaster"' AFTER  `districtId` ,
ADD  `branchId` INT( 10 ) NULL DEFAULT NULL COMMENT  'Foreign Key to "branchId" in table "branchmaster"' AFTER  `headQuarterId` ;

  ------------------------- Sonu Gupta(21-12-2015)-------------------
INSERT INTO `dspqfieldmaster` (`dspqFieldId`, `sectionId`, `dspqFieldName`, `isRequired`, `numberRequired`, `isAdditionalField`, `isDependantField`, `fieldDataType`, `fieldByOrder`, `status`, `createdDateTime`) 
VALUES (NULL, '13', 'I do not have any work experience.', '0', '0', '0', NULL, 'Check Box', '0', 'A', NOW());

ALTER TABLE  `districtspecificportfolioquestions` CHANGE  `fieldDisplayLabel`  `fieldDisplayLabel` VARCHAR( 500 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL

------------------- Gourav 24-12-2015 -------------------------------------------------------
ALTER TABLE `teacherpersonalinfo` ADD districtEmail varchar( 100 ) NULL DEFAULT NULL COMMENT  'District Email Address' AFTER anotherName;

-----------------------------------Ajay Jain (28-12-2015)--------------------------------
ALTER TABLE  `teacherpersonalinfo` ADD  `lastPositionWhenEmployed` VARCHAR( 100 ) NULL DEFAULT NULL AFTER  `employeeNumber` ;

-------------------------Gourav Pal 29-12-2015 --------------------------------------------------------
drop PROCEDURE if exists getChildsFromSecondaryStatus;
DELIMITER //
CREATE PROCEDURE `getChildsFromSecondaryStatus`(IN jobOrderId INT(11), IN statusMaster INT(11))
 BEGIN
  DECLARE lstStatusCount INT(11) DEFAULT 0;
  DECLARE districtId_jobOrder INT(11) DEFAULT 0;
  DECLARE jobCategoryId_jobOrder INT(11) DEFAULT 0;
  DECLARE statusNodeMaster_statusmaster INT(11) DEFAULT 0;
   DECLARE secondaryStatusId_call INT(11) DEFAULT 0;    
  SELECT districtId INTO districtId_jobOrder FROM joborder WHERE jobId=jobOrderId;
  SELECT jobCategoryId INTO jobCategoryId_jobOrder FROM joborder WHERE jobId=jobOrderId;
  SELECT sm.statusNodeId INTO statusNodeMaster_statusmaster FROM statusmaster sm WHERE sm.statusId=statusMaster;  
  SELECT COUNT(*) INTO lstStatusCount FROM `secondarystatus` sm WHERE sm.districtId=districtId_jobOrder AND sm.status='A' AND sm.`jobCategoryId`=jobCategoryId_jobOrder AND sm.jobCategoryId IS NOT NULL AND sm.statusNodeId=statusNodeMaster_statusmaster ORDER BY orderNumber ASC;  
   IF lstStatusCount > 0 THEN 
    SELECT distinct secondaryStatusId into secondaryStatusId_call  FROM `secondarystatus` sm WHERE sm.districtId=districtId_jobOrder AND sm.status='A' AND sm.`jobCategoryId`=jobCategoryId_jobOrder AND sm.jobCategoryId IS NOT NULL AND sm.statusNodeId=statusNodeMaster_statusmaster ORDER BY orderNumber ASC limit 0,1;
   ELSEIF lstStatusCount=0  THEN
    SELECT distinct secondaryStatusId into secondaryStatusId_call  FROM `secondarystatus` sm WHERE sm.districtId=districtId_jobOrder AND sm.status='A' AND sm.jobCategoryId IS NULL AND sm.statusNodeId=statusNodeMaster_statusmaster ORDER BY orderNumber ASC limit 0,1;    
   END IF; 
   select * from secondarystatus where parentSecondaryStatusId=secondaryStatusId_call and status="A" and parentStatusId is null;
END;

---------------------------- Ram Nath 30-12-15------------------------------------------------
ALTER TABLE `employeemaster` ADD `updatedDateTime` TIMESTAMP NULL AFTER `createdDateTime` ;
------------------------------------------- Deepak 30-12-2015 ---------------------------------------------------
 ALTER TABLE `readonlycheckpointtransaction` CHANGE `checkPointName` `checkPointName` VARCHAR( 30 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0';
  ALTER TABLE `readonlycheckpointmaster` CHANGE `checkPointName` `checkPointName` VARCHAR( 30 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '0';
  
---------------------------- Amit Chaudhary 31-12-15 (updated on cloud)------------------------------------------------
ALTER TABLE `assessmentquestions` ADD `isExperimental` TINYINT( 1 ) NULL DEFAULT NULL AFTER `status` ;
ALTER TABLE `questionuploadtemp` ADD `isExperimental` VARCHAR( 10 ) NULL DEFAULT NULL AFTER `question` ;
---------------------------Vishwanath 31-12-2015--------------------------------------------------------------------------
ALTER TABLE `districtmaster` ADD `isApplitrackIntegration` INT( 1 ) NULL AFTER `displayCNFGColumn` ;
ALTER TABLE `applitrackdistricts` ADD `clientcode` VARCHAR( 20 ) NULL AFTER `password` ;

---------------------------- Sonu Gupta 02-01-16------------------------------------------------
UPDATE  `dspqfieldmaster` SET  `status` =  'A',
`createdDateTime` = NOW( ) WHERE  `dspqFieldId` =133 ;
------------------- Gourav 4-1-2016 ------------------
drop PROCEDURE if exists getChildsFromSecondaryStatus;
DELIMITER //
CREATE PROCEDURE `getChildsFromSecondaryStatus`(IN jobOrderId INT(11), IN statusMaster INT(11))
 BEGIN
  DECLARE lstStatusCount INT(11) DEFAULT 0;
  DECLARE districtId_jobOrder INT(11) DEFAULT 0;
  DECLARE jobCategoryId_jobOrder INT(11) DEFAULT 0;
  DECLARE statusNodeMaster_statusmaster INT(11) DEFAULT 0;
   DECLARE secondaryStatusId_call INT(11) DEFAULT 0;    
  SELECT districtId INTO districtId_jobOrder FROM joborder WHERE jobId=jobOrderId;
  SELECT jobCategoryId INTO jobCategoryId_jobOrder FROM joborder WHERE jobId=jobOrderId;
  SELECT sm.statusNodeId INTO statusNodeMaster_statusmaster FROM statusmaster sm WHERE sm.statusId=statusMaster;  
  SELECT COUNT(*) INTO lstStatusCount FROM `secondarystatus` sm WHERE sm.districtId=districtId_jobOrder AND sm.status='A' AND sm.`jobCategoryId`=jobCategoryId_jobOrder AND sm.jobCategoryId IS NOT NULL AND sm.statusNodeId=statusNodeMaster_statusmaster ORDER BY orderNumber ASC;  
   IF lstStatusCount > 0 THEN 
    SELECT distinct secondaryStatusId into secondaryStatusId_call  FROM `secondarystatus` sm WHERE sm.districtId=districtId_jobOrder AND sm.status='A' AND sm.`jobCategoryId`=jobCategoryId_jobOrder AND sm.jobCategoryId IS NOT NULL AND sm.statusNodeId=statusNodeMaster_statusmaster ORDER BY orderNumber ASC limit 0,1;
   ELSEIF lstStatusCount=0  THEN
    SELECT distinct secondaryStatusId into secondaryStatusId_call  FROM `secondarystatus` sm WHERE sm.districtId=districtId_jobOrder AND sm.status='A' AND sm.jobCategoryId IS NULL AND sm.statusNodeId=statusNodeMaster_statusmaster ORDER BY orderNumber ASC limit 0,1;    
   END IF; 
   select * from secondarystatus where parentSecondaryStatusId=secondaryStatusId_call and status="A" and parentStatusId is null;
END;

---------------------------- Hafeez 04-01-16------------------------------------------------
ALTER TABLE  `optionsfordistrictspecificquestions` ADD  `requiredExplanation` TINYINT( 1 ) NULL DEFAULT  '0' COMMENT  ' 0 = Not Required Explanation (Default) 1 = Required Explanation';
---------------------------- Deepak -5-01-2016 ---------------------------------------
ALTER TABLE `checkpointtransaction` CHANGE `ipAddress` `ipAddress` VARCHAR( 20 ) NULL DEFAULT NULL;
ALTER TABLE `onboardingdashboard` CHANGE `districtId` `districtId` INT( 11 ) NULL DEFAULT '0',
CHANGE `headQuarterId` `headQuarterId` INT( 11 ) NULL DEFAULT '0',
CHANGE `branchId` `branchId` INT( 11 ) NULL DEFAULT '0',
CHANGE `onboardingName` `onboardingName` VARCHAR( 75 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0',
CHANGE `createdByUser` `createdByUser` INT( 11 ) NULL DEFAULT '0',
CHANGE `ipAddress` `ipAddress` VARCHAR( 20 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0',
CHANGE `status` `status` VARCHAR( 2 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0' ;

---------------------------- Shadab -06-01-2016 ---------------------------------------
ALTER TABLE  `jobforteacher` ADD  `sendMail` TINYINT( 1 )  DEFAULT  '0';
ALTER TABLE districtmaster ADD jobCompletionEmail TINYINT( 1 )  DEFAULT  '0';

---------------------------- Sonu Gupta -06-01-2016 ---------------------------------------
UPDATE  `dspqfieldmaster` SET  `sectionId` =  '2',
`isAdditionalField` =  '1',
`createdDateTime` = NOW( ) WHERE  `dspqfieldmaster`.`dspqFieldId` =19 LIMIT 1 ;

-------------------------SWADESH 07-01-2016-------------------------
ALTER TABLE `teacherstatushistoryforjob` ADD `timingforDclWrdw` VARCHAR( 100 ) NULL  ,
ADD `reasonforDclWrdw` VARCHAR( 100 ) NULL AFTER `timingforDclWrdw` ;

------------------------Sekhar 7-1-2016-----------------------------
INSERT INTO `menumaster` (`menuId`, `menuName`, `description`, `parentMenuId`, `subMenuId`, `districtId`, `menuArrow`, `pageName`, `pageNameWithSubPage`, `orderBy`, `status`) VALUES
(NULL, 'Hire Time by School Job', 'Hire Time by School Job', 46, 92, NULL, NULL, 'hireTimeBySchool.do', '|hireTimeBySchool.do|', 35, 'A');

INSERT INTO `roleaccesspermission` (`accessId`, `roleId`, `menuId`, `menuSectionId`, `optionId`, `status`) VALUES
(NULL, '1', '174', '164', '|2|4|5|7|', 'A'),
(NULL, '2', '174', '164', '|2|4|5|7|', 'A')
-------------------- Vishwanath 13-1-2016 -----------------------------
 ALTER TABLE `districtmaster` CHANGE `dmPhoneNumber` `dmPhoneNumber` VARCHAR( 50 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL 
 
-------------------------Hafeez 13-01-2016-------------------------
ALTER TABLE  `joborder` ADD  `jobCode` VARCHAR( 50 ) NULL ;
------------------------ vishwanath 13-1-16 -----------------
ALTER TABLE `districtspecificjobcode` CHANGE `jobDescription` `jobDescription` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;

----------------------------SWADESH 16-01-2016-------------------------------------------------
CREATE TABLE `districtspecificreason` (
`reasonId` INT( 5 ) NOT NULL AUTO_INCREMENT ,
`districtId` INT( 5 ) NULL COMMENT 'ditrict Id from "districtmaster"',
`reason` VARCHAR( 50 ) NOT NULL ,
`createdBy` INT( 5 ) NOT NULL ,
`createdDate` DATETIME NOT NULL ,
`status` VARCHAR( 1 ) NOT NULL ,
PRIMARY KEY ( `reasonId` )
) ENGINE = InnoDB 


  CREATE TABLE `districtspecifictiming` (
`timingId` INT( 5 ) NOT NULL AUTO_INCREMENT ,
`districtId` INT( 5 ) NULL COMMENT 'ditrict Id from "districtmaster"',
`timing` VARCHAR( 50 ) NOT NULL ,
`createdBy` INT( 5 ) NOT NULL ,
`createdDate` DATETIME NOT NULL ,
`status` VARCHAR( 1 ) NOT NULL ,
PRIMARY KEY ( `timingId` )
) ENGINE = InnoDB 

	INSERT INTO `districtspecificreason` (`districtId`, `reason`, `createdBy`, `createdDate`, `status`) VALUES
(NULL, 'Other Offer', 13, '2016-01-07 17:44:29', 'A'),
(NULL, 'Pay/Compensation', 13, '2016-01-07 17:44:32', 'A'),
(NULL, 'Untimely Communication', 13, '2016-01-07 17:44:35', 'A'),
(NULL, 'Personal', 13, '2016-01-07 17:44:38', 'A'),
(NULL, 'Location', 13, '2016-01-07 17:44:41', 'A'),
(NULL, 'Unknown', 13, '2016-01-07 17:44:44', 'A');

	INSERT INTO `districtspecifictiming` (`districtId`, `timing`, `createdBy`, `createdDate`, `status`) VALUES
(NULL, 'Before Interview', 13, '2016-01-07 17:40:25', 'A'),
(NULL, 'After Interview', 13, '2016-01-07 17:40:28', 'A'),
(NULL, 'After Offer', 13, '2016-01-07 17:40:31', 'A'),
(NULL, 'After Acceptance', 13, '2016-01-07 17:40:35', 'A');


________________________________________________________________Ram Nath For Base64 (18-01-2016)___________________________________________________________________
DROP TABLE IF EXISTS base64_data;
CREATE TABLE base64_data (c CHAR(1) BINARY, val TINYINT);
INSERT INTO base64_data VALUES 
	('A',0), ('B',1), ('C',2), ('D',3), ('E',4), ('F',5), ('G',6), ('H',7), ('I',8), ('J',9),
	('K',10), ('L',11), ('M',12), ('N',13), ('O',14), ('P',15), ('Q',16), ('R',17), ('S',18), ('T',19),
	('U',20), ('V',21), ('W',22), ('X',23), ('Y',24), ('Z',25), ('a',26), ('b',27), ('c',28), ('d',29),
	('e',30), ('f',31), ('g',32), ('h',33), ('i',34), ('j',35), ('k',36), ('l',37), ('m',38), ('n',39),
	('o',40), ('p',41), ('q',42), ('r',43), ('s',44), ('t',45), ('u',46), ('v',47), ('w',48), ('x',49),
	('y',50), ('z',51), ('0',52), ('1',53), ('2',54), ('3',55), ('4',56), ('5',57), ('6',58), ('7',59),
	('8',60), ('9',61), ('+',62), ('/',63), ('=',0) ;
 
 
DROP FUNCTION IF EXISTS BASE64_DECODE;
DELIMITER //
CREATE FUNCTION BASE64_DECODE (input BLOB)
	RETURNS BLOB
	CONTAINS SQL
	DETERMINISTIC
	SQL SECURITY INVOKER
BEGIN
	DECLARE ret BLOB DEFAULT '';
	DECLARE done TINYINT DEFAULT 0;
 
	IF input IS NULL THEN
		RETURN NULL;
	END IF;
 
each_block:
	WHILE NOT done DO BEGIN
		DECLARE accum_value BIGINT UNSIGNED DEFAULT 0;
		DECLARE in_count TINYINT DEFAULT 0;
		DECLARE out_count TINYINT DEFAULT 3;
 
each_input_char:
		WHILE in_count < 4 DO BEGIN
			DECLARE first_char CHAR(1);
 
			IF LENGTH(input) = 0 THEN
				RETURN ret;
			END IF;
 
			SET first_char = SUBSTRING(input,1,1);
			SET input = SUBSTRING(input,2);
 
			BEGIN
				DECLARE tempval TINYINT UNSIGNED;
				DECLARE error TINYINT DEFAULT 0;
				DECLARE base64_getval CURSOR FOR SELECT val FROM base64_data WHERE c = first_char;
				DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET error = 1;
 
				OPEN base64_getval;
				FETCH base64_getval INTO tempval;
				CLOSE base64_getval;
 
				IF error THEN
					ITERATE each_input_char;
				END IF;
 
				SET accum_value = (accum_value << 6) + tempval;
			END;
 
			SET in_count = in_count + 1;
 
			IF first_char = '=' THEN
				SET done = 1;
				SET out_count = out_count - 1;
			END IF;
		END; END WHILE;
 
		-- We've now accumulated 24 bits; deaccumulate into bytes
 
		-- We have to work from the left, so use the third byte position and shift left
		WHILE out_count > 0 DO BEGIN
			SET ret = CONCAT(ret,CHAR((accum_value & 0xff0000) >> 16));
			SET out_count = out_count - 1;
			SET accum_value = (accum_value << 8) & 0xffffff;
		END; END WHILE;
 
	END; END WHILE;
 
	RETURN ret;
END ;
 
DROP FUNCTION IF EXISTS BASE64_ENCODE;
DELIMITER //
CREATE FUNCTION BASE64_ENCODE (input BLOB)
	RETURNS BLOB
	CONTAINS SQL
	DETERMINISTIC
	SQL SECURITY INVOKER
BEGIN
	DECLARE ret BLOB DEFAULT '';
	DECLARE done TINYINT DEFAULT 0;
 
	IF input IS NULL THEN
		RETURN NULL;
	END IF;
 
each_block:
	WHILE NOT done DO BEGIN
		DECLARE accum_value BIGINT UNSIGNED DEFAULT 0;
		DECLARE in_count TINYINT DEFAULT 0;
		DECLARE out_count TINYINT;
 
each_input_char:
		WHILE in_count < 3 DO BEGIN
			DECLARE first_char CHAR(1);
 
			IF LENGTH(input) = 0 THEN
				SET done = 1;
				SET accum_value = accum_value << (8 * (3 - in_count));
				LEAVE each_input_char;
			END IF;
 
			SET first_char = SUBSTRING(input,1,1);
			SET input = SUBSTRING(input,2);
 
			SET accum_value = (accum_value << 8) + ASCII(first_char);
 
			SET in_count = in_count + 1;
		END; END WHILE;
 
		-- We've now accumulated 24 bits; deaccumulate into base64 characters
 
		-- We have to work from the left, so use the third byte position and shift left
		CASE
			WHEN in_count = 3 THEN SET out_count = 4;
			WHEN in_count = 2 THEN SET out_count = 3;
			WHEN in_count = 1 THEN SET out_count = 2;
			ELSE RETURN ret;
		END CASE;
 
		WHILE out_count > 0 DO BEGIN
			BEGIN
				DECLARE out_char CHAR(1);
				DECLARE base64_getval CURSOR FOR SELECT c FROM base64_data WHERE val = (accum_value >> 18);
 
				OPEN base64_getval;
				FETCH base64_getval INTO out_char;
				CLOSE base64_getval;
 
				SET ret = CONCAT(ret,out_char);
				SET out_count = out_count - 1;
				SET accum_value = accum_value << 6 & 0xffffff;
			END;
		END; END WHILE;
 
		CASE
			WHEN in_count = 2 THEN SET ret = CONCAT(ret,'=');
			WHEN in_count = 1 THEN SET ret = CONCAT(ret,'==');
			ELSE BEGIN END;
		END CASE;
 
	END; END WHILE;
 
	RETURN ret;
END;
______________________________________________________________________________________________________________________________________
--------------------Gourav -------------------------------------
ALTER TABLE  `districtportfolioconfig` ADD  `generalKnowledgeExamOptional` TINYINT( 1 ) NOT NULL DEFAULT  '1' AFTER  `transcriptUpload` ;

-------------------pankaj sewalia(19-01-2016)--------------------
ALTER TABLE  `districtmaster` ADD  `timeZone` VARCHAR( 2 ) NULL ;
ALTER TABLE  `districtmaster` ADD  `isHireVueIntegration` INT( 1 ) NULL , ADD  `hireVueAPIKey` VARCHAR( 100 ) NULL ;
ALTER TABLE  `districtmaster` ADD  `PostingStartTime` VARCHAR( 20 ) NULL ,ADD  `PostingEndTime` VARCHAR( 20 ) NULL ;

------------------------------------Ajay Jain 20-01-2016-------------------------------------
ALTER TABLE  `districtattachment` ADD  `documentName` VARCHAR( 100 ) NULL DEFAULT NULL AFTER  `createdBy` ;

------------------------------------Sonu Gupta 21-01-2016-------------------------------------

UPDATE  `dspqsectionmaster` SET  `groupId` =  '3',
`sectionName` =  'Pass/Fail General Knowledge Exam',
`status` =  'A',
`createdDateTime` = NOW( ) WHERE  `dspqsectionmaster`.`sectionId` =27 LIMIT 1 ;

INSERT INTO  `dspqfieldmaster` (
`dspqFieldId` ,
`sectionId` ,
`dspqFieldName` ,
`isRequired` ,
`numberRequired` ,
`isAdditionalField` ,
`isDependantField` ,
`fieldDataType` ,
`fieldByOrder` ,
`status` ,
`createdDateTime`
)
VALUES (
NULL ,  '27',  'Status',  '0',  '0',  '0', NULL ,  'Dropdown', NULL ,  'A', NOW( )
), (
NULL ,  '27',  'Exam Date',  '0',  '0',  '0', NULL ,  'Date', NULL ,  'A', NOW( )
);

INSERT INTO  `dspqfieldmaster` (
`dspqFieldId` ,
`sectionId` ,
`dspqFieldName` ,
`isRequired` ,
`numberRequired` ,
`isAdditionalField` ,
`isDependantField` ,
`fieldDataType` ,
`fieldByOrder` ,
`status` ,
`createdDateTime`
)
VALUES (
NULL ,  '27',  'Score Report',  '1',  '0',  '0', NULL ,  'File Upload', NULL ,  'A', NOW( )
);


UPDATE  `dspqsectionmaster` SET  `groupId` =  '3',
`sectionName` =  'Pass/Fail Subject Area Exam',
`status` =  'A',
`createdDateTime` = NOW( ) WHERE  `dspqsectionmaster`.`sectionId` =28 LIMIT 1 ;

INSERT INTO `dspqfieldmaster` (
`dspqFieldId` ,
`sectionId` ,
`dspqFieldName` ,
`isRequired` ,
`numberRequired` ,
`isAdditionalField` ,
`isDependantField` ,
`fieldDataType` ,
`fieldByOrder` ,
`status` ,
`createdDateTime`
)
VALUES (
NULL ,  '28',  'Status',  '0',  '0',  '0', NULL ,  'Dropdown', NULL ,  'A', NOW( )
), (
NULL ,  '28',  'Exam Date',  '0',  '0',  '0', NULL ,  'Date', NULL ,  'A', NOW( )
);


INSERT INTO  `dspqfieldmaster` (
`dspqFieldId` ,
`sectionId` ,
`dspqFieldName` ,
`isRequired` ,
`numberRequired` ,
`isAdditionalField` ,
`isDependantField` ,
`fieldDataType` ,
`fieldByOrder` ,
`status` ,
`createdDateTime`
)
VALUES (
NULL ,  '28',  'Subject',  '0',  '0',  '0', NULL ,  'Dropdown', NULL ,  'A', NOW( )
), (
NULL ,  '28',  'Score Report',  '1',  '0',  '0', NULL ,  'File Upload', NULL ,  'A', NOW( )
);

INSERT INTO `dspqfieldmaster` (`dspqFieldId`, `sectionId`, `dspqFieldName`, `isRequired`, `numberRequired`, `isAdditionalField`, `isDependantField`, `fieldDataType`, `fieldByOrder`, `status`, `createdDateTime`) VALUES (NULL, '26', 'Why are you no longer employed with us?', '0', '0', '0', NULL, 'Textarea', NULL, 'A', NOW());

------------------------------------Brajesh 21-01-2016-------------------------------------
CREATE TABLE `districtwisecandidatename` (
`nameId` INT( 5 ) NOT NULL AUTO_INCREMENT ,
`districtId` INT( 11 ) NOT NULL ,
`teacherId` INT( 10 ) NOT NULL ,
`status` VARCHAR( 1 ) NULL ,
`createdDateTime` TIMESTAMP NULL ,
PRIMARY KEY ( `nameId` ) ,
UNIQUE (
`nameId`
)
) ENGINE = InnoDB 

ALTER TABLE `districtwisecandidatename` ADD `nameWhileEmployed` VARCHAR( 200 ) NULL AFTER `nameId` ;

----------------------Sekhar 22-1-2016-------------------
UPDATE  `teachermatch`.`workflowstatus` SET  `workFlowStatus` =  'PEND',`updatedDateTime` = NOW( ) WHERE  `workflowstatus`.`workFlowStatusId` =12 LIMIT 1 ;
----------------------Sonu Gupta 22-1-2016-------------------
INSERT INTO `dspqfieldmaster` (`dspqFieldId`, `sectionId`, `dspqFieldName`, `isRequired`, `numberRequired`, `isAdditionalField`, `isDependantField`, `fieldDataType`, `fieldByOrder`, `status`, `createdDateTime`) VALUES (NULL, '27', 'General Exam Note ', '0', '0', '0', NULL, 'Textarea', NULL, 'A', NOW());

---------------------------SWADESH----------------------------------
 ALTER TABLE `teacherstatushistoryforjob` CHANGE `withdrawnByCandidate` `ActiontakenByCandidate` INT( 1 ) NULL DEFAULT NULL 
 
 ---------------------------pawan kumar  23-01-16----------------------------------
 ALTER TABLE `statusspecificquestions` ADD `isQuestionRequired` TINYINT NOT NULL ;
------------------------------ Vishwanath 24-1-16 -----------------------------------
DROP PROCEDURE IF EXISTS `ADMIN_DASHBOARD_NATIVE_REFERAL`;

DELIMITER //
CREATE PROCEDURE `ADMIN_DASHBOARD_NATIVE_REFERAL`()
BEGIN /* ADDED BY RAM NATH*/
  DECLARE total_teacher INT(10);
  DECLARE native_teacher INT(10);
  DECLARE authentication_native INT(10);
  DECLARE unauthentication_native INT(10) DEFAULT 0;
  DECLARE referal_teacher INT(10) DEFAULT 0;
  DECLARE authentication_referal INT(10) DEFAULT 0;
  DECLARE unauthentication_referal INT(10) DEFAULT 0;
  DECLARE emp_comp_native INT(10) DEFAULT 0;
  DECLARE emp_Incomp_native INT(10) DEFAULT 0;
  DECLARE emp_comp_referal INT(10) DEFAULT 0;
  DECLARE emp_Incomp_referal INT(10) DEFAULT 0;
  DECLARE time_out_native INT(10) DEFAULT 0;
  DECLARE time_out_referal INT(10) DEFAULT 0;
  DECLARE emp_comp_native_apply INT(10) DEFAULT 0;
  DECLARE emp_comp_referal_apply INT(10) DEFAULT 0;
  DECLARE done INT DEFAULT FALSE;

  DECLARE cursor_i CURSOR FOR (SELECT COUNT(*) /*total_teacher*/ , COUNT( IF( td.userType='N', 1, NULL ) ) /*native_teacher*/, COUNT( IF( (td.userType='N' AND td.verificationStatus=1), 1, NULL ) ) /*authentication_native*/, COUNT( IF( (td.userType='N' AND  td.verificationStatus=0), 1, NULL ) )  /*unauthentication_native*/, COUNT( IF( td.userType='R', 1, NULL ) ) /*referal_teacher*/ ,  COUNT( IF( (td.userType='R' AND td.verificationStatus=1), 1, NULL ) )  /*authentication_referal*/, COUNT( IF( (td.userType='R' AND td.verificationStatus=0), 1, NULL ) )  /* unauthentication_referal*/ FROM teacherdetail td); 
  DECLARE cursor_j CURSOR FOR (SELECT COUNT(IF( (techAss.statusId=4 AND td.userType='N' AND techAss.assessmentType=1 AND techAss.jobId IS NULL),1,NULL)) AS c1 /*emp_comp_native*/, COUNT(IF( (techAss.statusId=9 AND td.userType='N' AND techAss.assessmentType=1 AND techAss.jobId IS NULL),1,NULL)) AS c2 /*emp_Incomp_native*/, COUNT(IF( (techAss.statusId=4 AND td.userType='R' AND techAss.assessmentType=1 AND techAss.jobId IS NULL),1,NULL)) AS c3 /*emp_comp_referal*/, COUNT(IF( (techAss.statusId=9 AND td.userType='R' AND techAss.assessmentType=1 AND techAss.jobId IS NULL),1,NULL)) AS c4 /*emp_Incomp_referal*/ FROM teacherdetail td INNER JOIN teacherassessmentstatus techAss ON td.teacherId=techAss.teacherId WHERE YEAR(techAss.createdDateTime) =(YEAR(NOW())) );
  DECLARE cursor_k CURSOR FOR (SELECT COUNT(DISTINCT(teacherId)) FROM jobforteacher WHERE teacherId IN (SELECT  td.teacherId AS c1 /*emp_comp_native_apply*/ FROM  teacherdetail td INNER JOIN teacherassessmentstatus techAss ON td.teacherId=techAss.teacherId WHERE YEAR(techAss.createdDateTime) =(YEAR(NOW())) AND techAss.statusId=4 AND td.userType='N' AND techAss.assessmentType=1 AND techAss.jobId IS NULL ));
  DECLARE cursor_l CURSOR FOR (SELECT COUNT(DISTINCT(teacherId)) FROM jobforteacher WHERE teacherId IN (SELECT td.teacherId AS c1 /*emp_comp_referal_apply*/ FROM  teacherdetail td INNER JOIN teacherassessmentstatus techAss ON td.teacherId=techAss.teacherId WHERE YEAR(techAss.createdDateTime) =(YEAR(NOW())) AND techAss.statusId=4 AND td.userType='R' AND techAss.assessmentType=1 AND techAss.jobId IS NULL ));
  OPEN cursor_i;
    FETCH cursor_i INTO total_teacher,native_teacher,authentication_native,unauthentication_native,referal_teacher,authentication_referal,unauthentication_referal;        
   
 
  OPEN cursor_j;
    FETCH cursor_j INTO emp_comp_native,emp_Incomp_native,emp_comp_referal,emp_Incomp_referal;  
    
  OPEN cursor_k;
    FETCH cursor_k INTO emp_comp_native_apply;  
    
  OPEN cursor_l;
    FETCH cursor_l INTO emp_comp_referal_apply;        
  CLOSE cursor_j;
  CLOSE cursor_i; 
  CLOSE cursor_k;
  CLOSE cursor_l; 
  
  SELECT total_teacher AS total,
  native_teacher,
    authentication_native,                           
                                emp_comp_native,
                                emp_comp_native_apply,(emp_comp_native-emp_comp_native_apply) AS no_apply_anyJob_native ,
                                emp_Incomp_native,
                                (authentication_native-(emp_comp_native+emp_Incomp_native)) AS time_out_native,unauthentication_native,
                                referal_teacher,authentication_referal,
                                emp_comp_referal,
                                emp_comp_referal_apply,
                                (emp_comp_referal-emp_comp_referal_apply) AS no_apply_anyJob_referal,
                                emp_Incomp_referal,
                                (authentication_referal-(emp_comp_referal+emp_Incomp_referal)),
                                unauthentication_referal;
END;
--------------------------------------------------------------------------------------------------------------------------------------

--------------------------------------------------------Ram Nath(25-01-16)-------------------------------------------------------------
CREATE FUNCTION SPLIT_STR(X VARCHAR(255), delim VARCHAR(12),  pos INT) RETURNS VARCHAR(255)
RETURN REPLACE(SUBSTRING(SUBSTRING_INDEX(X, delim, pos),LENGTH(SUBSTRING_INDEX(X, delim, pos -1)) + 1),delim, '');
--------------------------------------------------------------------------------------------------------------------------------------

----------------------------------------Gaurav Kumar(25-01-2016)----------------------------------------------
UPDATE `teachermatch`.`certificationtypemaster` SET `certificationType` = 'Endorsement' WHERE `certificationtypemaster`.`certificationTypeMasterId` = 3;

----------------------------------------HAfeez(25-01-2016)----------------------------------------------
ALTER TABLE  `districtmaster` ADD  `skipApprovalProcess` VARCHAR( 50 ) NULL ;
UPDATE  `teachermatch`.`districtmaster` SET  `skipApprovalProcess` =  '1' WHERE  `districtmaster`.`districtId` =806900 LIMIT 1 ;

----------------------------------------Pavan(25-01-2016)----------------------------------------------
ALTER TABLE `teacherpersonalinfo` ADD `backgroundCheckDocument` VARCHAR( 150 ) NULL AFTER `retirementdatePERA` ;

--------------------------------------------------------Ram Nath(27-01-16)-------------------------------------------------------------
INSERT INTO `citymaster` (`cityId`, `zipcode`, `zipcodetype`, `cityName`, `stateId`, `lats`, `longs`, `decommisioned`, `status`)
 VALUES
 (NULL, '80126', 'STANDARD', 'HIGHLANDS RANCH', '6', NULL, NULL, 'FALSE', 'A'),
  (NULL, '80129', 'STANDARD', 'HIGHLANDS RANCH', '6', NULL, NULL, 'FALSE', 'A'),
   (NULL, '80130', 'STANDARD', 'HIGHLANDS RANCH', '6', NULL, NULL, 'FALSE', 'A'),
     (NULL, '80163', 'PO BOX', 'HIGHLANDS RANCH', '6', NULL, NULL, 'FALSE', 'A');
--------------------------------------------------------------------------------------------------------------------------------------
UPDATE mqevent SET eventType='Linked to KSN' WHERE eventType='Link to KSN';
UPDATE secondarystatus SET secondaryStatusName='Linked to KSN' WHERE secondaryStatusName='Link to KSN';
---------------------------------Ajay Jain 29-01-2016------------------------------
ALTER TABLE `teacherpersonalinfo` ADD `bgCheckUploadedDate` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP;

---------------------------------Shadab 30-01-2016-----------------------
CREATE TABLE `hrmspositionstextdetail` (
  `hrmsPositionsTextDetailId` int(10) unsigned NOT NULL auto_increment,
  `fileName` varchar(100) default NULL,
  `recordReceived` int(7) default NULL,
  `recordInserted` int(7) default NULL,
  `recordUpdated` int(7) default NULL,
  `exceptionCout` int(7) default NULL,
  `status` varchar(2) default NULL,
  `createdDateTime` datetime default NULL,
  `updatedDateTime` datetime default NULL,
  `folderPath` varchar(100) default NULL,
  `uploaded` tinyint(4) default NULL,
  `uploadedDateTime` datetime default NULL,
  `fileCreatedDateTime` datetime default NULL,
  `fileModifiedDateTime` datetime default NULL,
  `fileSize` int(11) default NULL,
  `lineNumber` int(11) default NULL,
  `message` varchar(500) default NULL,
  PRIMARY KEY  (`hrmsPositionsTextDetailId`)
)

---------------------------------Sonu Gupta 30-01-2016-----------------------
INSERT INTO `dspqfieldmaster` (`dspqFieldId`, `sectionId`, `dspqFieldName`, `isRequired`, `numberRequired`, `isAdditionalField`, `isDependantField`, `fieldDataType`, `fieldByOrder`, `status`, `createdDateTime`)
VALUES (NULL, '1', 'I am a former employee of', '0', '0', '1', NULL, 'Radio Button', NULL, 'A', NOW()), (NULL, '1', 'Name While Employed', '0', '0', '1', NULL, 'Text Box', NULL, 'A', NOW());

INSERT INTO `dspqfieldmaster` (`dspqFieldId`, `sectionId`, `dspqFieldName`, `isRequired`, `numberRequired`, `isAdditionalField`, `isDependantField`, `fieldDataType`, `fieldByOrder`, `status`, `createdDateTime`) VALUES (NULL, '1', 'Location', '0', '0', '1', NULL, 'Text Box', NULL, 'A', NOW()), (NULL, '1', 'Position', '0', '0', '1', NULL, 'Text Box', NULL, 'A', NOW());

INSERT INTO `dspqfieldmaster` (`dspqFieldId`, `sectionId`, `dspqFieldName`, `isRequired`, `numberRequired`, `isAdditionalField`, `isDependantField`, `fieldDataType`, `fieldByOrder`, `status`, `createdDateTime`) VALUES (NULL, '1', 'I am a retired employee of  Colorado''s Public Employers Retirement Association (PERA)', '0', '0', '1', NULL, 'Radio Button', NULL, 'A', NOW()), (NULL, '1', 'Approximate Retirement Date', '0', '0', '1', NULL, 'Date Picker', NULL, 'A', NOW());

INSERT INTO `dspqfieldmaster` (`dspqFieldId`, `sectionId`, `dspqFieldName`, `isRequired`, `numberRequired`, `isAdditionalField`, `isDependantField`, `fieldDataType`, `fieldByOrder`, `status`, `createdDateTime`) VALUES (NULL, '1', 'I have never been employed by ', '0', '0', '1', NULL, 'Radio Button', NULL, 'A', NOW()), (NULL, '1', 'I am employed as a substitute teacher for ', '0', '0', '1', NULL, 'Radio Button', NULL, 'A', NOW());


--------------------------------------------------------Ram Nath(01-02-16)-------------------------------------------------------------
CREATE TABLE `districtcandidate` (
  `districtCandidateId` int(10) NOT NULL auto_increment COMMENT 'when a candidate applies a job in a district for the first time. Do not create a record if the job is a branch job without a districtId (one and only one record per candidate per districtId)',
  `teacherId` int(10) NOT NULL COMMENT 'Foreign Key to "teacherId" in table "teacherdetail"',
  `districtId` int(10) NOT NULL COMMENT 'Foreign Key to "districtId" in table "districtMaster"',
  `numberOfapplications` int(10) NOT NULL default '1' COMMENT 'number of applicants',
  `firstApplicationDate` datetime NOT NULL COMMENT 'the date part of the createddatetime of the first application in the district',
  `mostRecentApplicationDate` datetime NOT NULL COMMENT 'default value is same as the firstApplicationDate',
  `hiringStatusInDistrict` tinyint(1) NOT NULL default '0' COMMENT 'default 0',
  `hiringStatusInDistrictUpdatedDateTime` datetime default NULL COMMENT 'Primary Employement Service Technician',
  `positionsHiredForInDistrict` int(10) NOT NULL default '0' COMMENT 'default is 0',
  `statusInDistrict` varchar(1) NOT NULL default 'A' COMMENT 'default active',
  `statusInDistrictUpdatedDateTime` datetime default NULL COMMENT 'default null',
  `createdDateTime` datetime NOT NULL COMMENT 'date and time when the record is created',
  PRIMARY KEY  (`districtCandidateId`),
  UNIQUE KEY `districtcandidate_unquie_1` (`districtId`,`teacherId`),
  KEY `districtId` (`districtId`),
  KEY `teacherId` (`teacherId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

ALTER TABLE `districtcandidate`
  ADD CONSTRAINT `districtcandidate_fk_key_1` FOREIGN KEY (`districtId`) REFERENCES `districtmaster` (`districtId`),
  ADD CONSTRAINT `districtcandidate_fk_key_2` FOREIGN KEY (`teacherId`) REFERENCES `teacherdetail` (`teacherId`);
--------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------SWADESH(01-02-2015-------------------------------------------------------------------
CREATE TABLE `districtspecificcustomquestion` (
  `customquestionid` int(10) NOT NULL auto_increment,
  `headQuarterId` int(11) default NULL,
  `branchId` int(11) default NULL,
  `districtId` int(5) default NULL COMMENT 'Foreign Key to "districtId" in table "districtMaster"',
  `jobCategoryId` int(10) default NULL COMMENT 'Foreign Key to "jobCategoryId" in table "jobCategoryMaster"',
  `question` text NOT NULL,
  `questionExplanation` text,
  `createdBy` int(5) NOT NULL COMMENT 'Foreign Key to "userId" in table "userMaster"',
  `status` varchar(1) NOT NULL COMMENT '"I" - Inactive, "A" - Active By default "A"',
  `createdDateTime` datetime NOT NULL,
  PRIMARY KEY  (`customquestionid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

ALTER TABLE `teacherexperience` ADD `customquestion` varchar(500) default NULL

ALTER TABLE `districtspecificcustomquestion` DROP `questionExplanation`;
 ALTER TABLE `districtspecificcustomquestion` CHANGE `question` `question` VARCHAR( 50 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL 
RENAME TABLE `districtspecificcustomquestion` TO `nationalboardcertificationMaster` ;


------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------Ram Nath(04-02-16)-------------------------------------------------------------
alter table districtmaster add column displaySubmenuAsaParentMenu int(1) default null;
---------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------Ankit saini(05-02-2016)------------------------------------
ALTER TABLE `districtportfolioconfig` ADD `degreeOptional` TINYINT( 1 ) NOT NULL DEFAULT '1' AFTER `generalKnowledgeExamOptional` ,
ADD `schoolOptional` TINYINT( 1 ) NOT NULL DEFAULT '1' AFTER `degreeOptional` ,
ADD `fieldOfStudyOptional` TINYINT( 1 ) NOT NULL DEFAULT '1' AFTER `schoolOptional` ;

ALTER TABLE `districtportfolioconfig` ADD `empPositionOptional` TINYINT( 1 ) NOT NULL DEFAULT '1' AFTER `fieldOfStudyOptional` ,
ADD `empOrganizationOptional` TINYINT( 1 ) NOT NULL DEFAULT '1' AFTER `empPositionOptional` ,
ADD `empCityOptional` TINYINT( 1 ) NOT NULL DEFAULT '1' AFTER `empOrganizationOptional` ,
ADD `empStateOptional` TINYINT( 1 ) NOT NULL DEFAULT '1' AFTER `empCityOptional` ;

--------------------------------------------------------Hafeez(05-02-16)-------------------------------------------------------------
INSERT INTO `eligibilitymaster` (`eligibilityId`, `headQuarterId`, `branchId`, `districtId`, `eligibilityName`, `eligibilityCode`, `status`, `orderBy`) VALUES
(20, NULL, NULL, 806900, 'New Employee Form', 'NEF', 'A', NULL),
(21, NULL, NULL, 806900, 'PERA', 'PERA', 'A', NULL),
(22, NULL, NULL, 806900, 'Social Security', 'SSF', 'A', NULL),
(23, NULL, NULL, 806900, 'Colorado Affirmation', 'CAF', 'A', NULL),
(24, NULL, NULL, 806900, 'Multi-Form', 'MF', 'A', NULL),
(25, NULL, NULL, 806900, 'CDE License', 'CDE', 'A', NULL);

INSERT INTO `eligibilitystatusmaster` (`eligibilityStatusId`, `districtId`, `statusName`, `statusCode`, `statusColorCode`, `eligibilityId`, `formId`, `formCode`, `status`, `validateStatus`) VALUES
(58, 806900, 'yes', 'y', '00882B', NULL, '', 'NEF', 'A', '1'),
(59, 806900, 'no', 'n', 'FF0000', NULL, '', 'NEF', 'A', '1'),
(60, 806900, 'Waived', 'W', 'FFFF00', NULL, '', 'NEF', 'A', '1'),
(61, 806900, 'yes', 'y', '00882B', NULL, '', 'PERA', 'A', '1'),
(62, 806900, 'no', 'n', 'FF0000', NULL, '', 'PERA', 'A', '1'),
(63, 806900, 'Waived', 'W', 'FFFF00', NULL, '', 'PERA', 'A', '1'),
(64, 806900, 'yes', 'y', '00882B', NULL, '', 'SSF', 'A', '1'),
(65, 806900, 'no', 'n', 'FF0000', NULL, '', 'SSF', 'A', '1'),
(66, 806900, 'Waived', 'W', 'FFFF00', NULL, '', 'SSF', 'A', '1'),
(67, 806900, 'yes', 'y', '00882B', NULL, '', 'CAF', 'A', '1'),
(68, 806900, 'no', 'n', 'FF0000', NULL, '', 'CAF', 'A', '1'),
(69, 806900, 'Waived', 'W', 'FFFF00', NULL, '', 'CAF', 'A', '1'),
(70, 806900, 'yes', 'y', '00882B', NULL, '', 'MF', 'A', '1'),
(71, 806900, 'no', 'n', 'FF0000', NULL, '', 'MF', 'A', '1'),
(72, 806900, 'Waived', 'W', 'FFFF00', NULL, '', 'MF', 'A', '1'),
(73, 806900, 'yes', 'y', '00882B', NULL, '', 'CDE', 'A', '1'),
(74, 806900, 'no', 'n', 'FF0000', NULL, '', 'CDE', 'A', '1'),
(75, 806900, 'Waived', 'W', 'FFFF00', NULL, '', 'CDE', 'A', '1');
---------------------------------------------------------------------------------------------------------------------------------------
-----

--------------------------------------------------------Sonu Gupta(05-02-16)-------------------------------------------------------------
alter table dspqgroupmaster add UNIQUE KEY `dspqgroupmaster_unquie_1` (`groupName`);
alter table dspqsectionmaster add UNIQUE KEY `dspqsectionmaster_unquie_1` (`groupId`,`sectionName`);
alter table dspqfieldmaster add UNIQUE KEY `dspqfieldmaster_unquie_1` (`sectionId`,`dspqFieldName`,`status`,`isRequired`);

--------------------------------------------------------Gourav Pal(06-02-16)-------------------------------------------------------------
CREATE TABLE `districtdetails` (
 districtDetailsId INT(11) AUTO_INCREMENT primary key NOT NULL,
  `districtId` int(11) NOT NULL default '0',
  `fipst` int(11) default NULL,
  `mstree` varchar(255) default NULL,
  `mcity` varchar(255) default NULL,
  `mstate` varchar(5) default NULL,
  `mzip` varchar(6) default NULL,
  `lcity` varchar(100) default NULL,
  `lstate` varchar(5) default NULL,
  `unions` varchar(5) default NULL,
  `conum` varchar(10) default NULL,
  `coname` varchar(255) default NULL,
  `ulocal` varchar(5) default NULL,
  `latcod` varchar(20) default NULL,
  `loncod` varchar(20) default NULL,
  `gslo` varchar(5) default NULL,
  `gshi` varchar(5) default NULL,
  `agchrt` varchar(50) default NULL,
  `speced` varchar(10) default NULL,
  `ell` varchar(10) default NULL,
  `pktch` varchar(10) default NULL,
  `kgtch` varchar(10) default NULL,
  `elmtch` varchar(20) default NULL,
  `sectch` varchar(20) default NULL,
  `aides` varchar(20) default NULL,
  `racecat` varchar(20) default NULL
);
INSERT `districtdetails` (districtId,fipst,mstree,mcity,mstate,mzip,lcity,lstate,unions,conum,coname,ulocal,latcod,loncod,gslo,gshi,agchrt,speced,ell,pktch,kgtch,elmtch,sectch,aides,racecat )
SELECT districtId,fipst,mstree,mcity,mstate,mzip,lcity,lstate,unions,conum,coname,ulocal,latcod,loncod,gslo,gshi,agchrt,speced,ell,pktch,kgtch,elmtch,sectch,aides,racecat 
FROM districtmaster;

ALTER TABLE districtmaster
DROP fipst,DROP mstree,DROP mcity,DROP mstate,DROP mzip,DROP lcity,DROP lstate,DROP unions,DROP conum,DROP coname,DROP ulocal,DROP latcod,DROP loncod,DROP gslo,DROP gshi,DROP agchrt,DROP speced,DROP ell,DROP pktch,DROP kgtch,DROP elmtch,DROP sectch,DROP aides,DROP racecat;
--------------sekhar 8-2-2016-------------------
RENAME TABLE `teachermatch`.`nationalboardcertificationMaster` TO `teachermatch`.`nationalboardcertificationmaster` ;

---------------------------- Amit Chaudhary 08-02-16 ---------------------------------------------------------------------------------
ALTER TABLE `teachernormscore` ADD `assessmentDateTime` DATETIME NULL DEFAULT NULL COMMENT 'the createdDatetime of the last answered question in teacheranswerdetails table' AFTER `createdDateTime` ,
ADD `updateDateTime` DATETIME NULL DEFAULT NULL COMMENT 'the value in this field is assigned by the system when the record was updated' AFTER `assessmentDateTime` ;
ALTER TABLE `assessmentdomainscore` ADD `assessmentDateTime` DATETIME NULL DEFAULT NULL COMMENT 'the createdDatetime of the last answered question in teacheranswerdetails table' AFTER `createdDateTime` ,
ADD `updateDateTime` DATETIME NULL DEFAULT NULL COMMENT 'the value in this field is assigned by the system when the record was updated' AFTER `assessmentDateTime` ;
ALTER TABLE `assessmentcompetencyscore` ADD `assessmentDateTime` DATETIME NULL DEFAULT NULL COMMENT 'the createdDatetime of the last answered question in teacheranswerdetails table' AFTER `createdDateTime` ,
ADD `updateDateTime` DATETIME NULL DEFAULT NULL COMMENT 'the value in this field is assigned by the system when the record was updated' AFTER `assessmentDateTime` ;
ALTER TABLE `rawdatafordomain` ADD `assessmentDateTime` DATETIME NULL DEFAULT NULL COMMENT 'the createdDatetime of the last answered question in teacheranswerdetails table' AFTER `createdDateTime` ,
ADD `updateDateTime` DATETIME NULL DEFAULT NULL COMMENT 'the value in this field is assigned by the system when the record was updated' AFTER `assessmentDateTime` ;
ALTER TABLE `rawdataforcompetency` ADD `assessmentDateTime` DATETIME NULL DEFAULT NULL COMMENT 'the createdDatetime of the last answered question in teacheranswerdetails table' AFTER `createdDateTime` ,
ADD `updateDateTime` DATETIME NULL DEFAULT NULL COMMENT 'the value in this field is assigned by the system when the record was updated' AFTER `assessmentDateTime` ;
ALTER TABLE `rawdataforobjective` ADD `assessmentDateTime` DATETIME NULL DEFAULT NULL COMMENT 'the createdDatetime of the last answered question in teacheranswerdetails table' AFTER `createdDateTime` ,
ADD `updateDateTime` DATETIME NULL DEFAULT NULL COMMENT 'the value in this field is assigned by the system when the record was updated' AFTER `assessmentDateTime` ;
--------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE  `districtmaster` ADD  `candidatePoolRefresh` INT( 1 ) NULL ;

  
  ---------------------------8-2-2016-----------------Sekhar-----------
--------------sekhar 8-2-2016-------------------
INSERT INTO `teachermatch`.`menumaster` (`menuId`, `menuName`, `parentMenuId`, `subMenuId`, `districtId`, `menuArrow`, `pageName`, `pageNameWithSubPage`, `orderBy`, `status`, `description`) VALUES (NULL, 'Candidate Pool Refresh', '2', NULL, NULL, NULL, 'candidatepoolrefresh.do', '|candidatepoolrefresh.do|', '24', 'A', NULL);
INSERT INTO `teachermatch`.`menusectionmaster` (`menuSectionId`, `menuId`, `menuSectionName`) VALUES (NULL, '176', 'Candidate Pool Refresh');
INSERT INTO `teachermatch`.`roleaccesspermission` (`accessId`, `roleId`, `menuId`, `menuSectionId`, `optionId`, `status`) VALUES (NULL, '2', '176', '166', '|1|2|4|5|7|', 'A');

-------------------- Hanzala ---------------------------
CREATE TABLE `withdrawnreasonmaster` (
  `withdrawnreasonmasterId` int(11) NOT NULL auto_increment,
  `districtId` int(11) default NULL COMMENT 'Foreign Key to "districtId" in table "districtMaster"',
  `reason` varchar(100) NOT NULL,
  `status` varchar(1) NOT NULL,
  `createdDateTime` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`withdrawnreasonmasterId`),
  KEY `districtId` (`districtId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

ALTER TABLE withdrawnreasonmaster ADD FOREIGN KEY (districtId) REFERENCES districtmaster(districtId);

INSERT INTO `withdrawnreasonmaster` (`withdrawnreasonmasterId`, `districtId`, `reason`, `status`, `createdDateTime`) VALUES
(1, NULL, 'Found another position', 'A', '2015-12-19 12:20:41'),
(2, NULL, 'Selection process too long', 'A', '2015-12-19 12:20:41'),
(3, NULL, 'No longer interested', 'A', '2015-12-19 12:20:57'),
(4, NULL, 'Other', 'A', '2015-12-19 12:20:57'),
(5, 1200390, 'Found another position', 'A', '2015-12-19 12:20:41'),
(6, 1200390, 'Selection process too long', 'A', '2015-12-19 12:20:41'),
(7, 1200390, 'No longer interested', 'A', '2015-12-19 12:20:57'),
(8, 1200390, 'Other', 'A', '2015-12-19 12:20:57');

ALTER TABLE  `jobforteacher` ADD  `withdrawnreasonmasterId` INT( 11 ) NULL AFTER  `notReviewedFlag`;

ALTER TABLE  `teacherstatushistoryforjob` ADD  `withdrawnreasonmasterId` INT( 10 ) NULL AFTER  `ActiontakenByCandidate`;
ALTER TABLE  `jobforteacher` DROP  `withdrawnreasonmasterId`;

-------------------------shadab ahmad-10-02-2016-------------------------------
ALTER TABLE `districtportfolioconfig` ADD `licenseLetterOptional` TINYINT( 1 ) NOT NULL DEFAULT '0' AFTER `empStateOptional` ;


-------------------------2-10-2016-sekhar----------------------------------
CREATE TABLE `linktoksntalentstemp` (
  `talenttempid` int(10) NOT NULL AUTO_INCREMENT,
  `talentId` varchar(10) DEFAULT NULL,
  `talent_first_name` varchar(100) DEFAULT NULL,
  `talent_last_name` varchar(100) DEFAULT NULL,
  `talent_email` varchar(100) DEFAULT NULL,
  `jobId` varchar(100) DEFAULT NULL,
  `ksnId` varchar(100) DEFAULT NULL,
  `createdDateTime` varchar(100) DEFAULT NULL,
  `errortext` varchar(400) DEFAULT NULL,
  `exist_teacherId` int(10) DEFAULT '0',
  `sessionid` varchar(200) DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`talenttempid`)
) ENGINE=InnoDB

----------------------------------(10-02-2016) - Ankit Sharma-----------------------------
CREATE INDEX KSNID ON `teacherdetail` (KSNID); 
CREATE INDEX teacheremail ON `teacherdetail` (emailAddress); 

-------------- Ramesh 11-02-2016 --------------
ALTER TABLE  `districtmaster` ADD  `selfServicePortfolioStatus` TINYINT NULL ;

----------------Dhananjay Verma-------------------------
CREATE TABLE `invitecandidatetoposition` (
  `inviteCandidateId` INT(11) NOT NULL AUTO_INCREMENT,
  `teacherId`  INT(10)   NOT NULL  COMMENT 'Foreign Key to "teacherId" in table "teacherdetail" ',
  `districtId` INT(11)   NOT NULL       COMMENT 'Foreign Key to "districtId" in table "districtMaster" ',
  `jobId`      INT(6)   NOT NULL  COMMENT 'Foreign Key to "jobId" in table "joborder" ',
  `positionId`      VARCHAR(20)  NOT NULL,
  `serviceProvider` VARCHAR(100) NOT NULL ,
  `createdDateTime` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Current Date Time ',
  `createdBy` INT(5) NOT NULL COMMENT 'Foreign Key to "userId" in table "usermaster" ',
  `status` VARCHAR(1)  NULL ,
  `ipAddress` VARCHAR(50)  NULL , 

  PRIMARY KEY  (`inviteCandidateId`),
   KEY `invitecandidatetoposition_teacherId` (`teacherId`),
   KEY `invitecandidatetoposition_jobId` (`jobId`),
   KEY `invitecandidatetoposition_districtId` (`districtId`),
   KEY `invitecandidatetoposition_createdBy` (`createdBy`),
   
  CONSTRAINT `invitecandidatetoposition_teacherId` FOREIGN KEY (`teacherId`) REFERENCES `teacherdetail` (`teacherId`),
  CONSTRAINT `invitecandidatetoposition_jobId` FOREIGN KEY (`jobId`) REFERENCES `joborder` (`jobId`),  
  CONSTRAINT `invitecandidatetoposition_districtId` FOREIGN KEY (`districtId`) REFERENCES `districtmaster` (`districtId`),
  CONSTRAINT `invitecandidatetoposition_createdBy` FOREIGN KEY (`createdBy`) REFERENCES `usermaster` (`userId`)
  
) ENGINE=INNODB DEFAULT CHARSET=latin1;


----------------------------------(12-02-2016) - Brajesh kumar-----------------------------

CREATE TABLE IF NOT EXISTS `communicationlog` (
  `autoId` int(10) NOT NULL AUTO_INCREMENT,
  `teacherId` int(10) NOT NULL,
  `headQuarterId` int(10) DEFAULT NULL,
  `branchId` int(10) DEFAULT NULL,
  `districtId` int(10) DEFAULT NULL,
  `schoolId` int(10) DEFAULT NULL,
  `modulename` varchar(200) DEFAULT NULL,
  `subject` text,
  `message` text,
  `jobId` int(10) DEFAULT NULL,
  `icanName` varchar(200) DEFAULT NULL,
  `attachmentname` varchar(200) DEFAULT NULL,
  `attachmentPath` varchar(200) DEFAULT NULL,
  `communicationType` varchar(20) DEFAULT NULL,
  `addedBy` varchar(100) DEFAULT NULL,
  `ipAddress` varchar(200) DEFAULT NULL,
  `createdDateTime` date DEFAULT NULL,
  `changedatetime` date DEFAULT NULL,
  PRIMARY KEY (`autoId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

ALTER TABLE `communicationlog` ADD `filename` VARCHAR( 300 ) NULL ;

----------------------------------(12-02-2016) - Sonu Gupta-----------------------------
ALTER TABLE  `dspqrouter` ADD  `sectionOrder` INT( 3 ) NULL AFTER  `othersAttribute` ;
------------------------------------(12-02-2016)----------------------------------------
ALTER TABLE  `joborder` ADD  `jobZoneId` INT( 2 ) NULL AFTER  `jobEndDate` ;
ALTER TABLE  `joborder` ADD  `jobStartTime` VARCHAR( 11 ) NULL AFTER  `jobZoneId` ;
ALTER TABLE  `joborder` ADD  `jobEndTime` VARCHAR( 11 ) NULL AFTER  `jobStartTime` ;
ALTER TABLE `joborder` CHANGE `jobZoneId` `jobTimeZoneId` INT( 2 ) NULL DEFAULT NULL;
-----------------------------Hafeez (15-02-2016)----------------------------------------
ALTER TABLE  `districtmaster` ADD  `showApprovalGropus` INT( 1 ) NULL;

----------------------------------Sujit Kumar 16-02-2016----------------------------
ALTER TABLE  `invitecandidatetoposition` ADD  `interviewId` varchar(150) NOT NULL;
ALTER TABLE  `invitecandidatetoposition` ADD `interviewCode` varchar(150) NOT NULL;

------------------------------------(16-02-2016) Sonu Gupta----------------------------------------
INSERT INTO `dspqfieldmaster` (`dspqFieldId`, `sectionId`, `dspqFieldName`, `isRequired`, `numberRequired`, `isAdditionalField`, `isDependantField`, `fieldDataType`, `fieldByOrder`, `status`, `createdDateTime`)
 VALUES (162, '28', 'Notes', '0', '0', '0', NULL, 'Textarea', NULL, 'A', NOW());
 
 ---------------------------------- (16-02-2016) Ashish -------------------------------------
ALTER TABLE  `teacheranswerdetailsfordistrictspecificquestions` ADD  `selectedoptionmlsel` VARCHAR( 50 ) NULL AFTER  `selectedOptions` ;
 
 ---------------------------------- (17-02-2016) Indra Jeet -------------------------------------
 ALTER TABLE `districtmaster` ADD `sAEditUpdateJob` TINYINT( 2 ) NULL  AFTER `sACreateDistJOb` ;
 
  ---------------------------------- (19-02-2016) Dhananjay Verma -------------------------------------
 
 ALTER TABLE  `eventdetails` ADD  `schoolId`  INT( 11 ) NULL DEFAULT NULL;
 
 ------------------------------------(20-02-2016) Deepak -----------------------------------------------
 ALTER TABLE `checkpointtransaction` ADD `conmmunicationRequired` TINYINT NULL AFTER `actionNeeded` ,
ADD `actionPerformedBy` TINYINT NULL AFTER `conmmunicationRequired` ;
ALTER TABLE `checkpointtransaction` DROP `actionNeeded`;
CREATE TABLE `onboardingcheckpointquestion` (
	`checkPointQuestionId` INT NOT NULL AUTO_INCREMENT,
	`onBoardingId` INT NOT NULL,
	`districtId` INT NOT NULL,
	`checkPointId` INT NOT NULL,
	`checkPointTypeId` INT NULL,
	`question` TEXT NULL,
	`candidateType` CHAR(2) NULL,
	`createdDate` DATE NULL,
	`createdBy` TINYINT NULL,
	`modifiedDate` DATE NULL,
	`modifiedBy` TINYINT NULL,
	`ipAddress` VARCHAR(12) NULL,
	`status` CHAR(2) NULL,
	PRIMARY KEY (`checkPointQuestionId`),
	CONSTRAINT `FK__onboardingdashboard` FOREIGN KEY (`onBoardingId`) REFERENCES `onboardingdashboard` (`onboardingId`),
	CONSTRAINT `FK__checkpointtransaction` FOREIGN KEY (`checkPointId`) REFERENCES `checkpointtransaction` (`checkPointTransactionId`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB;

CREATE TABLE `onboardingcheckpointoptions` (
	`checkPointOptionsId` INT NOT NULL AUTO_INCREMENT,
	`checkPointQuestionId` INT NOT NULL,
	`validOption` INT NULL,
	`options` TEXT NULL,
	`createdDate` DATE NULL,
	`createdBy` TINYINT NULL,
	`modifiedDate` DATE NULL,
	`modifiedBy` TINYINT NULL,
	`ipAddress` VARCHAR(12) NULL,
	`status` VARCHAR(2) NULL,
	PRIMARY KEY (`checkPointOptionsId`),
	CONSTRAINT `FK__onboardingcheckpointquestion` FOREIGN KEY (`checkPointQuestionId`) REFERENCES `onboardingcheckpointquestion` (`checkPointQuestionId`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB;


------------------------------------Dhananjay Verma 23-02-2016 -----------------------------------------------

ALTER TABLE  `teacherpersonalinfo` ADD  `currentempjobtitle` VARCHAR( 100 ) NULL DEFAULT NULL AFTER  `employeeNumber` ;
ALTER TABLE  `teacherpersonalinfo` ADD  `currentemplocation` VARCHAR( 100 ) NULL DEFAULT NULL AFTER  `currentempjobtitle` ;
ALTER TABLE  `teacherpersonalinfo` ADD  `currentempyearinlocation` VARCHAR( 100 ) NULL DEFAULT NULL AFTER  `currentemplocation` ;
ALTER TABLE  `teacherpersonalinfo` ADD  `currentempsupervisor` VARCHAR( 100 ) NULL DEFAULT NULL AFTER  `currentempyearinlocation` ;
ALTER TABLE  `teacherpersonalinfo` ADD  `fromalempjobtitle` VARCHAR( 100 ) NULL DEFAULT NULL AFTER  `currentempsupervisor` ;
ALTER TABLE  `teacherpersonalinfo` ADD  `formalemplocation` VARCHAR( 100 ) NULL DEFAULT NULL AFTER  `fromalempjobtitle` ;
ALTER TABLE  `teacherpersonalinfo` ADD  `formalempyearlocation` VARCHAR( 100 ) NULL DEFAULT NULL AFTER  `formalemplocation` ;
ALTER TABLE  `teacherpersonalinfo` ADD  `formalempsupervisor` VARCHAR( 100 ) NULL DEFAULT NULL AFTER  `formalempyearlocation` ;
ALTER TABLE  `teacherpersonalinfo` ADD  `retiredempjobtitle` VARCHAR( 100 ) NULL DEFAULT NULL AFTER  `formalempsupervisor` ;
ALTER TABLE  `teacherpersonalinfo` ADD  `retiredemplocation` VARCHAR( 100 ) NULL DEFAULT NULL AFTER  `retiredempjobtitle` ;
ALTER TABLE  `teacherpersonalinfo` ADD  `retiredempyearlocation` VARCHAR( 100 ) NULL DEFAULT NULL AFTER  `retiredemplocation` ;
ALTER TABLE  `teacherpersonalinfo` ADD  `retiredempsupervisor` VARCHAR( 100 ) NULL DEFAULT NULL AFTER  `retiredempyearlocation` ;
ALTER TABLE  `teacherpersonalinfo` ADD  `currentsubstitutejobtitle` VARCHAR( 100 ) NULL DEFAULT NULL AFTER  `retiredempsupervisor` ;
ALTER TABLE  `teacherpersonalinfo` ADD  `currentsubstitutelocation` VARCHAR( 100 ) NULL DEFAULT NULL AFTER  `currentsubstitutejobtitle` ;
ALTER TABLE  `teacherpersonalinfo` ADD  `currentsubstituteyearlocation` VARCHAR( 100 ) NULL DEFAULT NULL AFTER  `currentsubstitutelocation` ;
ALTER TABLE  `teacherpersonalinfo` ADD  `currentsubstitutesupervisor` VARCHAR( 100 ) NULL DEFAULT NULL AFTER  `currentsubstituteyearlocation` ;

------------------------------------Deepak 26-02-2016 -----------------------------------------------
ALTER TABLE `onboardingcheckpointquestion` CHANGE `candidateType` `candidateType` VARCHAR(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;
ALTER TABLE `checkpointtransaction` ADD `actionNeeded` TINYINT NULL AFTER `checkPointStatus` ;
ALTER TABLE `checkpointtransaction` CHANGE `actionPerformedBy` `actionPerformedBy` VARCHAR( 6 ) NULL DEFAULT NULL ;
ALTER TABLE `districtmaster` ADD `selfServiceCustomQuestion` TINYINT NULL AFTER `selfServicePortfolioStatus` ;


----------------------------------------------Pavan Gupta 27-02-2016 -----------------------------------------------
INSERT INTO `jobcategorymaster` (`jobCategoryId`, `jobCategoryName`, `headQuarterId`, `branchId`, `districtId`, `parentJobCategoryId`, `assessmentGroupId`, `baseStatus`, `offerVVIForInternalCandidates`, `offerVirtualVideoInterview`, `vviQuestionSet`, `maxScoreForVVI`, `sendAutoVVILink`, `statusIdForAutoVVILink`, `secondaryStatusIdForAutoVVILink`, `timeAllowedPerQuestion`, `VVIExpiresInDays`, `jobCompletedVVILink`, `offerAssessmentInviteOnly`, `districtAssessmentId`, `epiForFullTimeTeachers`, `offerDistrictSpecificItems`, `offerQualificationItems`, `offerJSI`, `offerPortfolioNeeded`, `assessmentDocument`, `epiForIMCandidates`, `portfolioForIMCandidates`, `districtSpecificItemsForIMCandidates`, `qualificationItemsForIMCandidates`, `jsiForIMCandidates`, `status`, `schoolSelection`, `questionSetID`, `attachDSPQFromJC`, `attachSLCFromJC`, `QuestionSetIDForOnboard`, `minDaysJobWillDisplay`, `approvalBeforeGoLive`, `noOfApprovalNeeded`, `buildApprovalGroup`, `approvalByPredefinedGroups`, `jobInviteOnly`, `hiddenJob`, `primaryJobCode`, `preHireSmartPractices`, `spAssessmentId`, `qualificationQuestion`, `offerDSPQ`, `autoRejectScore`) VALUES
(347, 'Non-Instructional', NULL, NULL, 7800185, NULL, NULL, 1, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 'A', 1, NULL, 0, 0, NULL, 8, 0, 0, 0, NULL, 0, NULL, NULL, 0, NULL, 0, 0, '0');

INSERT INTO `districtportfolioconfig` (`districtPortfolioConfigId`, `headQuarterId`, `branchId`, `districtId`, `jobCategoryId`, `dspqName`, `coverLetter`, `academic`, `academicTranscript`, `certification`, `proofOfCertification`, `reference`, `referenceLettersOfRecommendation`, `resume`, `tfaAffiliate`, `willingAsSubstituteTeacher`, `candidateType`, `address`, `expCertTeacherTraining`, `nationalBoardCert`, `affidavit`, `phoneNumber`, `personalinfo`, `dateOfBirth`, `ssn`, `race`, `genderId`, `ethnicorigin`, `ethinicity`, `employment`, `formeremployee`, `generalKnowledgeExam`, `subjectAreaExam`, `additionalDocuments`, `retirementnumber`, `veteran`, `videoLink`, `involvement`, `honors`, `ssnOptional`, `certiGrades`, `certiDatesOptional`, `empDatesOptional`, `academicsDatesOptional`, `certificationDoeNumber`, `certificationUrl`, `expectedSalary`, `addressOptional`, `residency`, `ressumeOptional`, `coverLetterOptional`, `empSecMscrOptional`, `empSecPrirOptional`, `empSecReasonForLeavOptional`, `empSecRoleOptional`, `empSecSalaryOptional`, `gpaOptional`, `videoSecOptional`, `academicsOptional`, `certificationptional`, `employeementOptional`, `referenceOptional`, `eEOCOptional`, `substituteOptional`, `certfiedTeachingExpOptional`, `nationalBoardOptional`, `tfaOptional`, `transcriptUpload`, `generalKnowledgeExamOptional`, `degreeOptional`, `schoolOptional`, `fieldOfStudyOptional`, `empPositionOptional`, `empOrganizationOptional`, `empCityOptional`, `empStateOptional`, `licenseLetterOptional`, `userId`, `createdDateTime`, `lastUpdatedDateTime`) VALUES
(285, NULL, NULL, 7800185, 347, NULL, 1, 1, 0, 1, 0, 0, 0, 1, 0, 0, 'I', 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '0', 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, '2014-02-10 00:00:02', '2016-02-19 15:04:42'),
(286, NULL, NULL, 7800185, 347, NULL, 1, 1, 0, 1, 1, 0, 0, 1, 0, 0, 'E', 1, 0, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, '0', 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, '2014-11-05 00:00:02', '2016-02-19 15:05:19');

---------------------------- Amit Chaudhary 27-02-16 (updated on cloud)------------------------------------------------
CREATE TABLE `assessmentstepmaster` (
`assessmentStepId` INT( 5 ) NOT NULL AUTO_INCREMENT ,
`stepName` VARCHAR( 200 ) NOT NULL ,
`stepShortName` VARCHAR( 10 ) NOT NULL ,
`status` VARCHAR( 1 ) NOT NULL ,
`createdBy` INT( 10 ) NOT NULL ,
`createdDateTime` DATETIME NOT NULL COMMENT 'Current Date and Time',
`ipaddress` VARCHAR( 200 ) NULL DEFAULT NULL ,
PRIMARY KEY ( `assessmentStepId` ) ,
UNIQUE (`stepName`) ,
UNIQUE (`stepShortName`)
) ENGINE = InnoDB ;

CREATE TABLE `assessmenttemplatemaster` (
`assessmentTemplateId` INT( 5 ) NOT NULL AUTO_INCREMENT ,
`templateName` VARCHAR( 200 ) NOT NULL ,
`assessmentType` INT( 1 ) NOT NULL COMMENT '1-Base, 2-JSI, 3-SP, 4-IPI',
`isDefault` TINYINT( 1 ) NULL DEFAULT NULL ,
`status` VARCHAR( 1 ) NOT NULL ,
`createdBy` INT( 10 ) NOT NULL ,
`createdDateTime` DATETIME NOT NULL COMMENT 'Current Date and Time',
`updatedBy` int(10) default NULL,
`updatedDateTime` datetime default NULL,
`ipaddress` VARCHAR( 200 ) NULL DEFAULT NULL ,
PRIMARY KEY ( `assessmentTemplateId` ) ,
UNIQUE (`templateName`)
) ENGINE = InnoDB ;

CREATE TABLE `templatewisestepmessage` (
`templateWiseStepMessageId` INT( 5 ) NOT NULL AUTO_INCREMENT ,
`assessmentTemplateId` INT( 5 ) NOT NULL ,
`assessmentStepId` INT( 5 ) NOT NULL ,
`stepMessage` TEXT NOT NULL ,
`status` VARCHAR( 1 ) NOT NULL ,
`createdBy` INT( 10 ) NOT NULL ,
`createdDateTime` DATETIME NOT NULL COMMENT 'Current Date and Time',
`updatedBy` int(10) default NULL,
`updatedDateTime` datetime default NULL,
`ipaddress` VARCHAR( 200 ) NULL DEFAULT NULL ,
PRIMARY KEY ( `templateWiseStepMessageId` ) 
) ENGINE = InnoDB ;

CREATE TABLE `assessmentwisestepmessage` (
`assessmentWiseStepMessageId` INT( 5 ) NOT NULL AUTO_INCREMENT ,
`assessmentId` INT( 5 ) NOT NULL ,
`useDefault` TINYINT( 1 ) NULL DEFAULT 1 ,
`assessmentTemplateId` int(5) NULL default NULL,
`assessmentStepId` int(5) NULL default NULL,
`stepMessage` TEXT NULL default NULL,
`status` VARCHAR( 1 ) NOT NULL ,
`createdBy` INT( 10 ) NOT NULL ,
`createdDateTime` DATETIME NOT NULL COMMENT 'Current Date and Time',
`updatedBy` int(10) default NULL,
`updatedDateTime` datetime default NULL,
`ipaddress` VARCHAR( 200 ) NULL DEFAULT NULL ,
PRIMARY KEY ( `assessmentWiseStepMessageId` )
) ENGINE = InnoDB ;

ALTER TABLE assessmenttemplatemaster ADD FOREIGN KEY (createdBy) REFERENCES usermaster(userId);
ALTER TABLE assessmentstepmaster ADD FOREIGN KEY (createdBy) REFERENCES usermaster(userId);
ALTER TABLE templatewisestepmessage ADD FOREIGN KEY (createdBy) REFERENCES usermaster(userId);
ALTER TABLE templatewisestepmessage ADD FOREIGN KEY (assessmentTemplateId) REFERENCES assessmenttemplatemaster(assessmentTemplateId);
ALTER TABLE templatewisestepmessage ADD FOREIGN KEY (assessmentStepId) REFERENCES assessmentstepmaster(assessmentStepId);
ALTER TABLE assessmentwisestepmessage ADD FOREIGN KEY (createdBy) REFERENCES usermaster(userId);
ALTER TABLE assessmentwisestepmessage ADD FOREIGN KEY (assessmentId) REFERENCES assessmentdetail(assessmentId);
ALTER TABLE assessmentwisestepmessage ADD FOREIGN KEY (assessmentTemplateId) REFERENCES assessmenttemplatemaster(assessmentTemplateId);
ALTER TABLE assessmentwisestepmessage ADD FOREIGN KEY (assessmentStepId) REFERENCES assessmentstepmaster(assessmentStepId);

INSERT INTO `assessmentstepmaster` (`assessmentStepId`, `stepName`, `stepShortName`, `status`, `createdBy`, `createdDateTime`, `ipaddress`) VALUES
(1, 'Before click start', 'step1', 'I', 1, '2016-02-27 15:30:30', '0:0:0:0:0:0:0:1'),
(2, 'Assessment Instruction Page', 'step2', 'A', 1, '2016-02-27 15:30:30', '0:0:0:0:0:0:0:1'),
(3, 'Assessment Instruction Popup', 'step3', 'A', 1, '2016-02-27 15:30:30', '0:0:0:0:0:0:0:1'),
(4, 'Section Instruction', 'step4', 'I', 1, '2016-02-27 15:30:30', '0:0:0:0:0:0:0:1'),
(5, 'Advance to next item without an answer', 'step5', 'A', 1, '2016-02-27 15:30:30', '0:0:0:0:0:0:0:1'),
(6, '1st Timeout on item', 'step6', 'A', 1, '2016-02-27 15:30:30', '0:0:0:0:0:0:0:1'),
(7, '2nd Timeout on item', 'step7', 'A', 1, '2016-02-27 15:30:30', '0:0:0:0:0:0:0:1'),
(8, '3rd Timeout on item', 'step8', 'A', 1, '2016-02-27 15:30:30', '0:0:0:0:0:0:0:1'),
(9, 'Close browser', 'step9', 'A', 1, '2016-02-27 15:30:30', '0:0:0:0:0:0:0:1'),
(10, '2nd attempt warning', 'step10', 'A', 1, '2016-02-27 15:30:30', '0:0:0:0:0:0:0:1'),
(11, '3rd attempt warning', 'step11', 'A', 1, '2016-02-27 15:30:30', '0:0:0:0:0:0:0:1'),
(12, 'Timeout on assessment without timeout on an item', 'step12', 'A', 1, '2016-02-27 15:30:30', '0:0:0:0:0:0:0:1'),
(13, '1st assessment timeout', 'step13', 'I', 1, '2016-02-27 15:30:30', '0:0:0:0:0:0:0:1'),
(14, 'Resume after reset', 'step14', 'I', 1, '2016-02-27 15:30:30', '0:0:0:0:0:0:0:1'),
(15, '2nd assessment timeout', 'step15', 'I', 1, '2016-02-27 15:30:30', '0:0:0:0:0:0:0:1');

------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------ khan 27-02
ALTER TABLE `districtportfolioconfig` ADD `SINOptional` TINYINT( 1 ) NULL DEFAULT '0' AFTER `ssnOptional` ; 
ALTER TABLE teacherpersonalinfo ADD COLUMN SIN VARCHAR(50) DEFAULT NULL AFTER SSN;
-----------------------------------------

-------Gaurav Kumar(27-2-2016)----------------
ALTER TABLE  `sapcandidatedetails` ADD  `districtId` INT  NULL AFTER  `jobForTeacherId` ;
ALTER TABLE  `sapcandidatedetails` CHANGE  `position`  `position` VARCHAR( 8 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL COMMENT  'Position/Reference Code as per SAP'

------------3-1------Sekhar-------
ALTER TABLE `mqevent` ADD `tmInitiate` TINYINT( 1 ) NULL AFTER `ksnActionType` ;

-------Indra Jeet(01-03-2016)----------------
ALTER TABLE  districtmaster ADD  defaultPDP TINYINT(1) NULL comment 'EPI-1 , IPI-2 , SP-3' ;
ALTER TABLE  assessmentdetail ADD  pdpreporttype INT(2) NULL comment 'EPIPDP-1 , IPIPDP-2 , SPPDP-3' ;

-------Brajesh(01-03-2016)----------------
ALTER TABLE `teacherpersonalinfo` ADD `phoneTypeUSNonUS` INT( 2 ) NULL DEFAULT '1';

-----------------------Ajay Jain (02-03-2016)------------------------------
ALTER TABLE `linktoksntalentstemp` ADD `tmksnId` VARCHAR(100) NULL DEFAULT NULL AFTER `ksnId`;

------------------------------------Dhananjay Verma 03-03-2016 -----------------------------------------------

ALTER TABLE  `teacherpersonalinfo` DROP  `currentempjobtitle` ,
DROP  `currentemplocation` ,
DROP  `currentempyearinlocation` ,
DROP  `currentempsupervisor` ,
DROP  `fromalempjobtitle` ,
DROP  `formalemplocation` ,
DROP  `formalempyearlocation` ,
DROP  `formalempsupervisor` ,
DROP  `retiredempjobtitle` ,
DROP  `retiredemplocation` ,
DROP  `retiredempyearlocation` ,
DROP  `retiredempsupervisor` ,
DROP  `currentsubstitutejobtitle` ,
DROP  `currentsubstitutelocation` ,
DROP  `currentsubstituteyearlocation` ,
DROP  `currentsubstitutesupervisor` ;


------------------------------------Dhananjay Verma 02-03-2016 -----------------------------------------------

ALTER TABLE  `districtwisecandidatename` ADD  `currentempjobtitle` VARCHAR( 100 ) NULL DEFAULT NULL AFTER  `createdDateTime` ;
ALTER TABLE  `districtwisecandidatename` ADD  `currentemplocation` VARCHAR( 100 ) NULL DEFAULT NULL AFTER  `currentempjobtitle` ;
ALTER TABLE  `districtwisecandidatename` ADD  `currentempyearinlocation` VARCHAR( 100 ) NULL DEFAULT NULL AFTER  `currentemplocation` ;
ALTER TABLE  `districtwisecandidatename` ADD  `currentempsupervisor` VARCHAR( 100 ) NULL DEFAULT NULL AFTER  `currentempyearinlocation` ;
ALTER TABLE  `districtwisecandidatename` ADD  `fromalempjobtitle` VARCHAR( 100 ) NULL DEFAULT NULL AFTER  `currentempsupervisor` ;
ALTER TABLE  `districtwisecandidatename` ADD  `formalemplocation` VARCHAR( 100 ) NULL DEFAULT NULL AFTER  `fromalempjobtitle` ;
ALTER TABLE  `districtwisecandidatename` ADD  `formalempyearlocation` VARCHAR( 100 ) NULL DEFAULT NULL AFTER  `formalemplocation` ;
ALTER TABLE  `districtwisecandidatename` ADD  `formalempsupervisor` VARCHAR( 100 ) NULL DEFAULT NULL AFTER  `formalempyearlocation` ;
ALTER TABLE  `districtwisecandidatename` ADD  `retiredempjobtitle` VARCHAR( 100 ) NULL DEFAULT NULL AFTER  `formalempsupervisor` ;
ALTER TABLE  `districtwisecandidatename` ADD  `retiredemplocation` VARCHAR( 100 ) NULL DEFAULT NULL AFTER  `retiredempjobtitle` ;
ALTER TABLE  `districtwisecandidatename` ADD  `retiredempyearlocation` VARCHAR( 100 ) NULL DEFAULT NULL AFTER  `retiredemplocation` ;
ALTER TABLE  `districtwisecandidatename` ADD  `retiredempsupervisor` VARCHAR( 100 ) NULL DEFAULT NULL AFTER  `retiredempyearlocation` ;
ALTER TABLE  `districtwisecandidatename` ADD  `currentsubstitutejobtitle` VARCHAR( 100 ) NULL DEFAULT NULL AFTER  `retiredempsupervisor` ;
ALTER TABLE  `districtwisecandidatename` ADD  `currentsubstitutelocation` VARCHAR( 100 ) NULL DEFAULT NULL AFTER  `currentsubstitutejobtitle` ;
ALTER TABLE  `districtwisecandidatename` ADD  `currentsubstituteyearlocation` VARCHAR( 100 ) NULL DEFAULT NULL AFTER  `currentsubstitutelocation` ;
ALTER TABLE  `districtwisecandidatename` ADD  `currentsubstitutesupervisor` VARCHAR( 100 ) NULL DEFAULT NULL AFTER  `currentsubstituteyearlocation` ;



------------------------------------Indra Jeet 03-03-2016 -----------------------------------------------
CREATE TABLE `pdpmaster` (
`pdpdId` INT( 5 ) NOT NULL AUTO_INCREMENT ,
`pdpname` VARCHAR( 100 ) NOT NULL ,
`status` VARCHAR( 1 ) NOT NULL COMMENT 'Active A, Inactive I',
`createdDateTime` DATETIME NOT NULL COMMENT 'Current Date and Time',
`createdBy` INT( 10 ) NOT NULL,
PRIMARY KEY ( `pdpdId` ) 
) ENGINE = InnoDB ;

INSERT INTO `pdpmaster` (`pdpname`, `status`,`createdDateTime`, `createdBy`) 
VALUES( 'EPI PDP','A','2016-03-03 15:30:31', 1),
		('IPI PDP','A','2016-03-03 15:30:33', 1),
		('SP PDP' ,'A','2016-03-03 15:30:39', 1);
		
------------------------------------Deepak 02-03-2016 -----------------------------------------------
ALTER TABLE `districtmaster` ADD `selfServiceAddGridData` TINYINT NULL AFTER `selfServiceCustomQuestion` ;
------------------------------------Shiva 02-03-2016 -----------------------------------------------
ALTER TABLE  `onboardingcheckpointquestion` ADD  `documentFileName` text NULL AFTER  `question` ;

---------------------------- Amit Chaudhary 04-03-16 -------------------------------
CREATE TABLE `spinboundapicallrecordlog` (
`spInboundAPICallRecordId` INT( 11 ) NOT NULL AUTO_INCREMENT ,
 `teacherId` INT( 11 ) DEFAULT NULL ,
 `currentLessonNo` INT( 11 ) DEFAULT NULL ,
 `totalLessons` INT( 11 ) DEFAULT NULL ,
 `lpm` VARCHAR( 50 ) DEFAULT NULL ,
 `IPAddress` VARCHAR( 200 ) DEFAULT NULL ,
 `createdDateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
PRIMARY KEY (  `spInboundAPICallRecordId` )
) ENGINE = INNODB;

ALTER TABLE `spinboundapicallrecord` CHANGE `lastUpdateDate` `lastUpdateDate` DATETIME NOT NULL ;

---------------------------------------------------------------- khan-04-03-2016
ALTER TABLE `districtportfolioconfig` ADD `dateOfBirthOptional` TINYINT( 1 ) NOT NULL DEFAULT '0' AFTER `empStateOptional` ;

-----------------------Pankaj Sewalia (2016-03-05)-------------------------------------------
ALTER TABLE `districtmaster` ADD COLUMN candidatesNotReviewed VARCHAR(100) NULL

-----------------5-3-2016--sekhar ---------------------------
ALTER TABLE  `jobforteacher` ADD  `applicationStatus` INT( 2 ) NULL AFTER  `jobBoardReferralURL`;




------------------------------------Dhananjay Verma 08-03-2016 -----------------------------------------------

ALTER TABLE  `districtwisecandidatename` 
DROP  `formalemplocation` ,
DROP  `formalempyearlocation` ,
DROP  `formalempsupervisor` ,
DROP  `retiredempjobtitle` ,
DROP  `retiredemplocation` ,
DROP  `retiredempyearlocation` ,
DROP  `retiredempsupervisor` ,
DROP  `currentsubstitutelocation` ,
DROP  `currentsubstituteyearlocation` ,
DROP  `currentsubstitutesupervisor` ;


  ------------------------------------Dhananjay Verma 08-03-2016 -----------------------------------------------
  
  ALTER TABLE  `districtwisecandidatename` ADD  `DatesofEmployment` timestamp  NULL ;
  ALTER TABLE  `districtwisecandidatename` ADD  `DateofRetirement` timestamp  NULL;
  ALTER TABLE  `districtwisecandidatename` ADD  `employeeType` INT( 11 ) NOT NULL ;
					
ALTER TABLE `districtwisecandidatename` CHANGE `fromalempjobtitle` `formalempjobtitle` VARCHAR( 100 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL
------------------------------ Vishwanath 08-03-2016 ------------------------------------
ALTER TABLE `questionspool` ADD INDEX `stage3IdIndex` (`stage3Id`);
ALTER TABLE `questionspool` ADD INDEX (`status`);
ALTER TABLE `assessmentquestions` ADD INDEX (`status`);			

------------------------------------------Sujit Kumar 08-03-2016-------------------------------------------	
ALTER TABLE `districtmaster_06072015` RENAME futuredistrictmaster; ( please check table name for diff server )

DELIMITER $$
DROP PROCEDURE IF EXISTS `insertDistrictMaster`$$
CREATE PROCEDURE `insertDistrictMaster`(IN  district_id INT)
BEGIN       
    INSERT `districtmaster` (districtId,`areAllSchoolsInContract`, `postingOnDistrictWall`, `postingOnTMWall`, `exitURL`, `flagForMessage`, `flagForURL`, `exitMessage`, `assessmentUploadURL`, `canTMApproach`, `allowMessageTeacher`, `emailForTeacher`, `createdDateTime`, `stateId`, `ncesStateId`, `districtName`, `phoneNumber`,  `address`, `zipCode`, `type`, `totalNoOfSchools`, `totalNoOfTeachers`, `totalNoOfStudents`,`cityname`, `alternatePhone`, `faxNumber`, `website`, `displayName`, `logoPath`, `description`, `dmName`, `dmEmailAddress`, `dmPhoneNumber`, `dmPassword`, `acName`, `acEmailAddress`, `acPhoneNumber`, `amName`, `amEmailAddress`, `amPhoneNumber`, `initiatedOnDate`, `contractStartDate`, `contractEndDate`, `schoolsUnderContract`, `teachersUnderContract`, `studentsUnderContract`, `annualSubsciptionAmount`, `noSchoolUnderContract`, `allSchoolsUnderContract`, `allGradeUnderContract`, `selectedSchoolsUnderContract`, `pkOffered`, `kgOffered`, `g01Offered`, `g02Offered`, `g03Offered`, `g04Offered`, `g05Offered`, `g06Offered`, `g07Offered`, `g08Offered`, `g09Offered`, `g10Offered`, `g11Offered`, `g12Offered`, `logoPathFile`, `assessmentUploadURLFile`, `status`,hiringAuthority,statusNotes,displayTMDefaultJobCategory,setAssociatedStatusToSetDPoints,resetQualificationIssuesPrivilegeToSchool,statusPrivilegeForSchools)
    SELECT districtId,`areAllSchoolsInContract`, `postingOnDistrictWall`, `postingOnTMWall`, `exitURL`, `flagForMessage`, `flagForURL`, `exitMessage`, `assessmentUploadURL`, `canTMApproach`, `allowMessageTeacher`, `emailForTeacher`, `createdDateTime`, `stateId`, `ncesStateId`, `districtName`, `phoneNumber`,  `address`, `zipCode`, `type`, `totalNoOfSchools`, `totalNoOfTeachers`, `totalNoOfStudents`,`cityname`, `alternatePhone`, `faxNumber`, `website`, `displayName`, `logoPath`, `description`, `dmName`, `dmEmailAddress`, `dmPhoneNumber`, `dmPassword`, `acName`, `acEmailAddress`, `acPhoneNumber`, `amName`, `amEmailAddress`, `amPhoneNumber`, `initiatedOnDate`, `contractStartDate`, `contractEndDate`, `schoolsUnderContract`, `teachersUnderContract`, `studentsUnderContract`, `annualSubsciptionAmount`, `noSchoolUnderContract`, `allSchoolsUnderContract`, `allGradeUnderContract`, `selectedSchoolsUnderContract`, `pkOffered`, `kgOffered`, `g01Offered`, `g02Offered`, `g03Offered`, `g04Offered`, `g05Offered`, `g06Offered`, `g07Offered`, `g08Offered`, `g09Offered`, `g10Offered`, `g11Offered`, `g12Offered`, `logoPathFile`, `assessmentUploadURLFile`,
     CASE WHEN `status`='I'  THEN 'A' END, 
     hiringAuthority,statusNotes,displayTMDefaultJobCategory,setAssociatedStatusToSetDPoints,resetQualificationIssuesPrivilegeToSchool,statusPrivilegeForSchools
     FROM futuredistrictmaster WHERE districtId= district_id;
END$$
DELIMITER ;		
	
-----------------------------SWADESH 10-03-16---------------------------
ALTER TABLE `teacherstatusnotes` ADD `offeraccepbycandidate` TINYINT( 1 ) NOT NULL DEFAULT '0';
---------------------------10-3-2016 Sekhar-------------------
--ALTER TABLE  `sapcandidatedetailshistory` ADD  `districtId` INT NULL AFTER  `sapCandidateId` ; already live


--------------------------------------------RAM Nath(11-03-2016) ----------------------------------------------------
CREATE TABLE `affidavitmaster` (
	  `affidavitId` int(10) NOT NULL auto_increment,
	  `headQuarterId` int(10) NOT NULL COMMENT 'Foreign Key to "headQuarterId" in table "headquartermaster"',
	  `content` text NOT NULL,
	  `affidavitheading` varchar(255) NOT NULL,
	  `status` varchar(1) NOT NULL default 'A',
	  `createdDateTime` timestamp NOT NULL default CURRENT_TIMESTAMP,
	  PRIMARY KEY  (`affidavitId`),
	  KEY `headQuarterId` (`headQuarterId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

	ALTER TABLE `affidavitmaster`
	  ADD CONSTRAINT `affidavitmaster_fk_key_1` FOREIGN KEY (`headQuarterId`) REFERENCES `headquartermaster` (`headQuarterId`);
-----------------------------------------------------------------------------------------------------------------------
--------------------------------------------Saurav Kumar(11-03-2016) ----------------------------------------------------
INSERT INTO `subjectareaexammaster` (`subjectAreaExamId`, `districtId`, `subjectAreaExamCode`, `subjectAreaExamName`, `status`, `createdDateTime`) VALUES (NULL, '806900', '', 'Instructional Technology', 'A', CURRENT_TIMESTAMP), (NULL, '806900', '', 'Agriculture and Renewable Natural Resources', 'A', CURRENT_TIMESTAMP);

INSERT INTO `subjectareaexammaster` (`subjectAreaExamId`, `districtId`, `subjectAreaExamCode`, `subjectAreaExamName`, `status`, `createdDateTime`) VALUES (NULL, '806900', '', 'Art', 'A', CURRENT_TIMESTAMP), (NULL, '806900', '', 'Business Education', 'A', CURRENT_TIMESTAMP);

INSERT INTO `subjectareaexammaster` (`subjectAreaExamId`, `districtId`, `subjectAreaExamCode`, `subjectAreaExamName`, `status`, `createdDateTime`) VALUES (NULL, '806900', '', 'Drama', 'A', CURRENT_TIMESTAMP), (NULL, '806900', '', 'Early Childhood Education', 'A', CURRENT_TIMESTAMP), (NULL, '806900', '', 'Early Childhood Special Education', 'A', CURRENT_TIMESTAMP), (NULL, '806900', '', 'Elementary Education', 'A', CURRENT_TIMESTAMP), (NULL, '806900', '', 'English', 'A', CURRENT_TIMESTAMP), (NULL, '806900', '', 'Family and Consumer Studies', 'A', CURRENT_TIMESTAMP), (NULL, '806900', '', 'French', 'A', CURRENT_TIMESTAMP), (NULL, '806900', '', 'German', 'A', CURRENT_TIMESTAMP), (NULL, '806900', '', 'Gifted and Talented', 'A', CURRENT_TIMESTAMP), (NULL, '806900', '', 'Health', 'A', CURRENT_TIMESTAMP), (NULL, '806900', '', 'Instructional Technology', 'A', CURRENT_TIMESTAMP), (NULL, '806900', '', 'Italian', 'A', CURRENT_TIMESTAMP), (NULL, '806900', '', 'Japanese', 'A', CURRENT_TIMESTAMP), (NULL, '806900', '', 'Latin', 'A', CURRENT_TIMESTAMP), (NULL, '806900', '', 'Linguistically Diverse Education', 'A', CURRENT_TIMESTAMP), (NULL, '806900', '', 'Linguistically Diverse Education Specialist: Bilingual Education', 'A', CURRENT_TIMESTAMP), (NULL, '806900', '', 'Marketing Education', 'A', CURRENT_TIMESTAMP), (NULL, '806900', '', 'Mathematics', 'A', CURRENT_TIMESTAMP), (NULL, '806900', '', 'Music', 'A', CURRENT_TIMESTAMP), (NULL, '806900', '', 'Physical Education', 'A', CURRENT_TIMESTAMP);

INSERT INTO `subjectareaexammaster` (`subjectAreaExamId`, `districtId`, `subjectAreaExamCode`, `subjectAreaExamName`, `status`, `createdDateTime`) VALUES (NULL, '806900', '', 'Principal', 'A', CURRENT_TIMESTAMP), (NULL, '806900', '', 'Reading Specialist', 'A', CURRENT_TIMESTAMP), (NULL, '806900', '', 'Reading Teacher', 'A', CURRENT_TIMESTAMP), (NULL, '806900', '', 'Russian', 'A', CURRENT_TIMESTAMP), (NULL, '806900', '', 'School Counselor', 'A', CURRENT_TIMESTAMP), (NULL, '806900', '', 'School Librarian', 'A', CURRENT_TIMESTAMP), (NULL, '806900', '', 'School Social Worker', 'A', CURRENT_TIMESTAMP), (NULL, '806900', '', 'Science', 'A', CURRENT_TIMESTAMP), (NULL, '806900', '', 'Social Studies', 'A', CURRENT_TIMESTAMP), (NULL, '806900', '', 'Spanish', 'A', CURRENT_TIMESTAMP), (NULL, '806900', '', 'Special Education Generalist', 'A', CURRENT_TIMESTAMP), (NULL, '806900', '', 'Special Education Specialist: Deaf/Hard of Hearing', 'A', CURRENT_TIMESTAMP), (NULL, '806900', '', 'Special Education Specialist: Visually Impaired', 'A', CURRENT_TIMESTAMP), (NULL, '806900', '', 'Speech', 'A', CURRENT_TIMESTAMP), (NULL, '806900', '', 'Technology Education', 'A', CURRENT_TIMESTAMP);

---------------------------- Amit Chaudhary 14-03-16 -------------------------------
ALTER TABLE `assessmentwisestepmessage` ADD `assessmentType` INT( 1 ) NOT NULL COMMENT '1-Base, 2-JSI, 3-SP, 4-IPI' AFTER `assessmentId` ;
---------------------------------------------------Ram Nath(15-03-2016)----------------------------------------------------------
alter table jobforteacher add column jftId int(10) default NULL COMMENT 'Foreign Key to "jobForTeacherId" in table "jftwisedetails"' AFTER displaySecondaryStatusId;
ALTER TABLE `jobforteacher`  ADD CONSTRAINT `jobforteacherId_fk_key` FOREIGN KEY (`jftId`) REFERENCES `jftwisedetails` (`jobForTeacherId`);

Drop table if exists jftwisedetails;
CREATE TABLE `jftwisedetails` (
  `jobForTeacherId` int(10) NOT NULL,
  `affidavit` tinyint(1) default NULL,
  `complete` tinyint(1) default NULL,
  `affidavitAcceptedDate` datetime default NULL,
  PRIMARY KEY  (`jobForTeacherId`)
) ENGINE=InnoDB;
----------------------------------------------------------------------------------------------------------------------------------

-----------------------------shadab(17-03-2016)------------------------------------------
ALTER TABLE `teacheracademics` ADD COLUMN `otherUniversityName` VARCHAR(100) NULL AFTER `degreeConferredDate`; 

--------------------------------------------------------------------------Ram Nath(17-03-2016)-----------------------------------------------------------------------------------
insert into `affidavitmaster`(`affidavitId`,`headQuarterId`,`affidavitheading`,`content`,`status`,`createdDateTime`) values (1,2,'Applicant''s Certification & Release of Liability','I, the applicant/employee, by submission of this online application certify that I hereby expressly authorize the Board of Education, its agents, and its employees to make any investigation of my personal or employment history, expressly including, but not limited to, federal and/or state criminal, law enforcement, or traffic records, which may include confirmation by fingerprint identification. I further authorize any former employer, person, firm, corporation, credit agency, administration body, or governmental agency to give the Board of Education, or its agents, or its employees any information they may have regarding me. In consideration of the review of my employment application by the Board of Education, its members, officers, agents, or its employees, I hereby release the Board of Education to which this application is submitted and any and all providers of information to whom this release is sent, from any liability as a result of furnishing or receiving this information. If employed, I further authorize this Board of Education or its agents to provide information about my employment in this school system to future employers or prospective employers. I authorize persons to whom an exact copy of this release is presented to rely on the copy as if it were a signed original. I have read the information contained in the application carefully and certify that the information I have given is correct and complete. I understand that if I am employed, false statements on this application shall be considered sufficient cause for dismissal. I also understand that the application, references, and other employment-related information become property of the local school system and are classified as confidential information.  I understand that the employment application has been developed, reviewed and approved by the human resource staff for the individual school districts. Both the website and the application are a resource to assist applicants in applying for employment with local boards of education across North Carolina. Neither the NC State Board of Education nor the NC Department of Public Instruction has approved the contents of the website or the employment application. I understand that in compliance with the Immigration Reform and Control Act of 1986, the Board of Education will employ only United States citizens and aliens lawfully authorized to work in the United States. Upon employment, acceptable authorization and identification documents may be required. I understand that any offer of employment is conditional upon the receipt by the Board of Education of an acceptable criminal history check pursuant to authorization above. In addition, I understand that in accordance with NC General Statues, I do not have to disclose any arrest, charge or conviction that has been expunged. In compliance with federal law, including the provisions of Title IX of the Education Amendments of 1972, public schools in North Carolina do not discriminate on the basis of race, sex, religion, color, national or ethnic origin, age, disability, or military service in its policies, programs, activities, admissions or employment. If you feel that you have been discriminated against based upon any of the aforementioned criteria, please contact the Title IX Coordinator with the employing school system, or the Office of Civil Rights of the U.S. Department of Education (OCR).','A','2016-03-17 00:00:00');
---------------------------------------------------------------Deepak(17-03-2016)-------------------------------------------------
 CREATE TABLE `checkpointaction` (
`actionId` INT NOT NULL AUTO_INCREMENT ,
`districtId` INT NOT NULL ,
`onBoardingId` INT NOT NULL ,
`checkPointId` INT NOT NULL ,
`action` INT NOT NULL ,
`candidateType` VARCHAR( 2 ) NULL ,
`createdDate` DATE NULL ,
`createdBy` TINYINT NULL ,
`modifiedDate` DATE NULL ,
`modifiedBy` TINYINT NULL ,
`ipAddress` VARCHAR( 15 ) NULL ,
`status` VARCHAR( 2 ) NOT NULL ,
PRIMARY KEY ( `actionId` )
) ENGINE = InnoDB ;
ALTER TABLE `checkpointaction`
	ADD CONSTRAINT `FK_checkpointaction_onboardingdashboard` FOREIGN KEY (`onBoardingId`) REFERENCES `onboardingdashboard` (`onboardingId`),
	ADD CONSTRAINT `FK_checkpointaction_checkpointtransaction` FOREIGN KEY (`checkPointId`) REFERENCES `checkpointtransaction` (`checkPointTransactionId`);
ALTER TABLE `checkpointaction` ADD COLUMN `checkPointStatus` INT(11) NOT NULL AFTER `action`;

----------------------------------Sujit Kumar 18-03-2016----------------------------
ALTER TABLE  `userloginhistory` ADD  `serverName` VARCHAR(100) NOT NULL;
ALTER TABLE  `userloginhistory` ADD  `loginPasswordType` VARCHAR(100) NOT NULL;

ALTER TABLE  `teacherloginhistory` ADD  `serverName` VARCHAR(100) NOT NULL;
ALTER TABLE  `teacherloginhistory` ADD  `loginPasswordType` VARCHAR(100) NOT NULL;
ALTER TABLE  `teacherloginhistory` ADD  `logType` VARCHAR(100) NOT NULL;

--------------------------------------------RAM Nath(11-03-2016) ----------------------------------------------------
alter table jftwisedetails add column applyJobWithSignUp tinyint(1) default null Comment 'this flag true if job apply with sign up' after affidavitAcceptedDate ;
alter table jftwisedetails add column applyJobWithSignUpFirstTime tinyint(1) default null Comment 'this flag true if job apply successfully with sign up' after applyJobWithSignUp ;

---------------------------------------------------------------------------------------------------------------------