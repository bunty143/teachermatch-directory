DROP PROCEDURE IF EXISTS `JOB_DEACTIVE_AUTO_IF_HIREEXPECTED_FULL`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `JOB_DEACTIVE_AUTO_IF_HIREEXPECTED_FULL`(IN job_Id INT(11))
BEGIN /* ADDED BY RAM NATH*/
    DECLARE schoolExistsorNot INT(11) DEFAULT 0;
    DECLARE noOfExpHiresSchoolCount INT(11) DEFAULT NULL;
    DECLARE noOfExpHiresDistrictCount INT(11) DEFAULT NULL;
    DECLARE noOfHireCount INT(11) DEFAULT 0;
    SELECT COUNT(*) INTO schoolExistsorNot FROM schoolinjoborder WHERE jobId=job_Id;
    select count(*) INTO noOfHireCount FROM jobforteacher jft WHERE jft.status=6 and jft.jobId=job_Id;
    IF schoolExistsorNot > 0 THEN 
    select sum(noOfSchoolExpHires) INTO noOfExpHiresSchoolCount FROM schoolinjoborder WHERE jobId=job_Id;
       IF (noOfExpHiresSchoolCount IS NOT NULL) && (noOfExpHiresSchoolCount > 0) THEN
         IF  noOfExpHiresSchoolCount = noOfHireCount THEN
          UPDATE joborder AS jo SET status='I' WHERE jo.jobId=job_Id and status='A';
          /*INSERT INTO checkingtrigger (districtId,jobId,jobDescription) value(districtId,job_Id,'One');*/
          ELSEIF noOfExpHiresSchoolCount > noOfHireCount THEN
          UPDATE joborder AS jo SET status='A' WHERE jo.jobId=job_Id and status='I';
          /*INSERT INTO checkingtrigger (districtId,jobId,jobDescription) value(districtId,job_Id,'two');*/
        END IF;
      END IF;
    ELSEIF schoolExistsorNot=0  THEN
      select noOfExpHires INTO noOfExpHiresDistrictCount FROM joborder where jobId=job_Id;
       IF  (noOfExpHiresDistrictCount IS NOT NULL) && (noOfExpHiresDistrictCount > 0) then
         IF  noOfExpHiresDistrictCount = noOfHireCount THEN 
          UPDATE joborder AS jo SET status='I' WHERE jo.jobId=job_Id and status='A';
          /*INSERT INTO checkingtrigger (districtId,jobId,jobDescription) value(districtId,job_Id,'three');*/
          ELSEIF noOfExpHiresDistrictCount > noOfHireCount THEN
          UPDATE joborder AS jo SET status='A' WHERE jo.jobId=job_Id and status='I';
          /*INSERT INTO checkingtrigger (districtId,jobId,jobDescription) value(districtId,job_Id,'four');*/
        END IF;
      END IF;
    END IF;
END//






DROP PROCEDURE IF EXISTS `JOBREQUITSITIONNUMBER_UPDATE`;
DELIMITER //

CREATE DEFINER=`root`@`localhost` PROCEDURE `JOBREQUITSITIONNUMBER_UPDATE`()
BEGIN /* ADDED BY RAM NATH*/
  DECLARE cursor_jobId INT;
  DECLARE cursor_requisitionNumber VARCHAR(255);
  DECLARE cursor_schoolId_d INT(11) DEFAULT 0;
  DECLARE cursor_districtRequisitionId_d INT(11) DEFAULT 0;
  DECLARE done INT DEFAULT FALSE;
  /*DECLARE cursor_i CURSOR FOR (select jobId,requisitionNumber from jobOrder jom where jobId not in(select jrn.jobId FROM jobrequisitionnumbers jrn, joborder jo, districtrequisitionnumbers drn where jrn.jobId =jo.jobId and jo.districtId=804800 and jrn.districtRequisitionId = drn.districtRequisitionId) and jom.districtId=804800);*/
  DECLARE cursor_i CURSOR FOR (select jobId,requisitionNumber,(select districtRequisitionId from districtrequisitionnumbers where requisitionNumber=jom.requisitionNumber and districtId=804800 and isUsed=1 limit 0,1) as drnumber,(select schoolId FROM schoolinjoborder where jobId=jom.jobId limit 0,1) as schoolId from joborder jom where jobId not in(select jrn.jobId FROM jobrequisitionnumbers jrn, joborder jo, districtrequisitionnumbers drn where jrn.jobId =jo.jobId and jo.districtId=804800 and jrn.districtRequisitionId = drn.districtRequisitionId) and jom.districtId=804800);
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
  OPEN cursor_i;
  read_loop: LOOP
    FETCH cursor_i INTO cursor_jobId,cursor_requisitionNumber,cursor_districtRequisitionId_d,cursor_schoolId_d;        
    IF done THEN
      LEAVE read_loop;
    END IF; 
    /*select requisitionNumber into cursor_requisitionNumber from joborder where jobId=cursor_jobId;   
   insert into checkingtrigger(jobId,jobDescription) values(cursor_jobId,cursor_requisitionNumber);*/
    /*set schoolId_d := 0;
    set districtRequisitionId_d := 0;*/
    /*select districtRequisitionId into districtRequisitionId_d from districtrequisitionnumbers where requisitionNumber=cursor_requisitionNumber and districtId=804800 limit 0,1;
     /*select schoolId into schoolId_d FROM schoolinjoborder where jobId=cursor_jobId limit 0,1;
   insert into checkingtrigger(id, districtId,jobId) values(cursor_jobId,schoolId_d,districtRequisitionId_d); */
      IF cursor_districtRequisitionId_d IS NOT NULL && cursor_schoolId_d IS NOT NULL THEN
       INSERT INTO jobrequisitionnumbers (jobRequisitionId,jobId,districtRequisitionId,schoolId,status) VALUES(null,cursor_jobId,cursor_districtRequisitionId_d,cursor_schoolId_d,0);
      END IF;
  END LOOP;
  CLOSE cursor_i;
END//









DROP PROCEDURE IF EXISTS `getChildsFromSecondaryStatus`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `getChildsFromSecondaryStatus`(IN jobOrderId INT(11), IN statusMaster INT(11))
BEGIN /* ADDED BY RAM NATH*/
  DECLARE lstStatusCount INT(11) DEFAULT 0;
  DECLARE districtId_jobOrder INT(11) DEFAULT 0;
  DECLARE jobCategoryId_jobOrder INT(11) DEFAULT 0;
  DECLARE statusNodeMaster_statusmaster INT(11) DEFAULT 0;
   DECLARE secondaryStatusId_call INT(11) DEFAULT 0;    
  SELECT districtId INTO districtId_jobOrder FROM joborder WHERE jobId=jobOrderId;
  SELECT jobCategoryId INTO jobCategoryId_jobOrder FROM joborder WHERE jobId=jobOrderId;
  SELECT sm.statusNodeId INTO statusNodeMaster_statusmaster FROM statusmaster sm WHERE sm.statusId=statusMaster;  
  SELECT COUNT(*) INTO lstStatusCount FROM `secondarystatus` sm WHERE sm.districtId=districtId_jobOrder AND sm.status='A' AND sm.`jobCategoryId`=jobCategoryId_jobOrder AND sm.jobCategoryId IS NOT NULL AND sm.statusNodeId=statusNodeMaster_statusmaster ORDER BY orderNumber ASC;  
   IF lstStatusCount > 0 THEN 
    SELECT distinct secondaryStatusId into secondaryStatusId_call  FROM `secondarystatus` sm WHERE sm.districtId=districtId_jobOrder AND sm.status='A' AND sm.`jobCategoryId`=jobCategoryId_jobOrder AND sm.jobCategoryId IS NOT NULL AND sm.statusNodeId=statusNodeMaster_statusmaster ORDER BY orderNumber ASC limit 0,1;
   ELSEIF lstStatusCount=0  THEN
    SELECT distinct secondaryStatusId into secondaryStatusId_call  FROM `secondarystatus` sm WHERE sm.districtId=districtId_jobOrder AND sm.status='A' AND sm.jobCategoryId IS NULL AND sm.statusNodeId=statusNodeMaster_statusmaster ORDER BY orderNumber ASC limit 0,1;    
   END IF; 
   select * from secondarystatus where parentSecondaryStatusId=secondaryStatusId_call and status="A" and parentStatusId is null;
END//







DROP PROCEDURE IF EXISTS `DELETE_DUBLICATE_TEACHERSTATUSHISTORYFORJOB_SECOND_SS`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `DELETE_DUBLICATE_TEACHERSTATUSHISTORYFORJOB_SECOND_SS`(IN teacherIdIN INT(11),IN jobIdIN INT(11),IN secondaryStatusIdIN INT(11),IN stausIN VARCHAR(1),IN rowCount NVARCHAR(11))
BEGIN /* ADDED BY RAM NATH*/ 
  DECLARE i INT DEFAULT 0;
  DECLARE cursor_teacherStatusHistoryForJobId INT(11);  
  DECLARE done INT DEFAULT FALSE;  
  DECLARE cursor_i CURSOR FOR (select teacherStatusHistoryForJobId from teacherstatushistoryforjob where teacherId=teacherIdIN and jobId=jobIdIN and secondaryStatusId=secondaryStatusIdIN and status=stausIN limit 1,999999);
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
  OPEN cursor_i;
  read_loop: LOOP
    FETCH cursor_i INTO cursor_teacherStatusHistoryForJobId;        
    IF done THEN
      LEAVE read_loop;
    END IF;    
      IF i!=0 then
      CALL DELETE_DUBLICATE_TEACHERSTATUSHISTORYFORJOB_FINAL_DELETE(cursor_teacherStatusHistoryForJobId);
      END IF;
      SET i = i+1;
  END LOOP;
  CLOSE cursor_i;
END//





DROP PROCEDURE IF EXISTS `DELETE_DUBLICATE_TEACHERSTATUSHISTORYFORJOB_SECOND_SM`;
DELIMITER //

CREATE DEFINER=`root`@`localhost` PROCEDURE `DELETE_DUBLICATE_TEACHERSTATUSHISTORYFORJOB_SECOND_SM`(IN teacherIdIN INT(11),IN jobIdIN INT(11),IN stausIdIN INT(11),IN stausIN VARCHAR(1),IN rowCount NVARCHAR(11))
BEGIN /* ADDED BY RAM NATH*/  
  DECLARE i INT DEFAULT 0;
 
  DECLARE cursor_teacherStatusHistoryForJobId INT(11);
  DECLARE done INT DEFAULT FALSE;  
  DECLARE cursor_i CURSOR FOR (select teacherStatusHistoryForJobId from teacherstatushistoryforjob where teacherId=teacherIdIN and jobId=jobIdIN and statusId=stausIdIN and status=stausIN limit 0,999999);
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
  OPEN cursor_i;
   SET i = 0;
  read_loop: LOOP
    FETCH cursor_i INTO cursor_teacherStatusHistoryForJobId;        
    IF done THEN
      LEAVE read_loop;
    END IF;  
       IF i!=0 then
        CALL DELETE_DUBLICATE_TEACHERSTATUSHISTORYFORJOB_FINAL_DELETE(cursor_teacherStatusHistoryForJobId);
       END IF;
      SET i = i+1;
  END LOOP;
  CLOSE cursor_i;
END//





DROP PROCEDURE IF EXISTS `DELETE_DUBLICATE_TEACHERSTATUSHISTORYFORJOB_FIRST_SS`;
DELIMITER //

CREATE DEFINER=`root`@`localhost` PROCEDURE `DELETE_DUBLICATE_TEACHERSTATUSHISTORYFORJOB_FIRST_SS`()
BEGIN /* ADDED BY RAM NATH*/  
  DECLARE cursor_count INT;
  DECLARE cursor_teacherId INT(11);
  DECLARE cursor_jobId INT(11) DEFAULT 0;
  DECLARE cursor_secondaryStatusId INT(11) DEFAULT 0;
  DECLARE cursor_staus VARCHAR(1) DEFAULT NULL; 
  DECLARE done INT DEFAULT FALSE;
  DECLARE cursor_i CURSOR FOR (select tshj.teacherId,tshj.jobId,tshj.secondaryStatusId,tshj.status,count(*) as cnt from teacherstatushistoryforjob tshj where tshj.status='S' AND tshj.secondaryStatusId is not null  group by tshj.teacherId,tshj.jobId,tshj.secondaryStatusId,tshj.status having count(*)>1);    
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
  OPEN cursor_i;
  read_loop: LOOP
     FETCH cursor_i INTO cursor_teacherId,cursor_jobId,cursor_secondaryStatusId,cursor_staus,cursor_count;        
    IF done THEN
      LEAVE read_loop;
    END IF;    
      CALL DELETE_DUBLICATE_TEACHERSTATUSHISTORYFORJOB_SECOND_SS(cursor_teacherId,cursor_jobId,cursor_secondaryStatusId,cursor_staus,cursor_count);
  END LOOP;
  CLOSE cursor_i;
END//









DROP PROCEDURE IF EXISTS `DELETE_DUBLICATE_TEACHERSTATUSHISTORYFORJOB_FIRST_SM`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `DELETE_DUBLICATE_TEACHERSTATUSHISTORYFORJOB_FIRST_SM`()
BEGIN /* ADDED BY RAM NATH*/  
  DECLARE cursor_count INT;
  DECLARE cursor_teacherId INT(11);
  DECLARE cursor_jobId INT(11) DEFAULT 0;
  DECLARE cursor_stausId INT(11) DEFAULT 0;
  DECLARE cursor_staus VARCHAR(1) DEFAULT NULL; 
  DECLARE done INT DEFAULT FALSE;
  DECLARE cursor_i CURSOR FOR (select tshj.teacherId,tshj.jobId,tshj.statusId,tshj.status,count(*) as cnt from teacherstatushistoryforjob tshj where tshj.status='A' and tshj.statusId is not null  group by tshj.teacherId,tshj.jobId,tshj.statusId,tshj.status having count(*)>1);    
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
  OPEN cursor_i;
  read_loop: LOOP
     FETCH cursor_i INTO cursor_teacherId,cursor_jobId,cursor_stausId,cursor_staus,cursor_count;        
    IF done THEN
      LEAVE read_loop;
    END IF;    
      CALL DELETE_DUBLICATE_TEACHERSTATUSHISTORYFORJOB_SECOND_SM(cursor_teacherId,cursor_jobId,cursor_stausId,cursor_staus,cursor_count);
  END LOOP;
  CLOSE cursor_i;
END//










DROP PROCEDURE IF EXISTS `DELETE_DUBLICATE_TEACHERSTATUSHISTORYFORJOB_FIRST`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `DELETE_DUBLICATE_TEACHERSTATUSHISTORYFORJOB_FIRST`()
BEGIN /* ADDED BY RAM NATH*/
  CALL DELETE_DUBLICATE_TEACHERSTATUSHISTORYFORJOB_FIRST_SM();
  /*CALL DELETE_DUBLICATE_TEACHERSTATUSHISTORYFORJOB_FIRST_SS();*/
END//









DROP PROCEDURE IF EXISTS `DELETE_DUBLICATE_TEACHERSTATUSHISTORYFORJOB_FINAL_DELETE`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `DELETE_DUBLICATE_TEACHERSTATUSHISTORYFORJOB_FINAL_DELETE`(IN teacherStatusHistoryForJobId_delte INT(11))
BEGIN /* ADDED BY RAM NATH*/
  DECLARE id_new INT(11) DEFAULT 0;
  DECLARE teacherId_new INT(11) DEFAULT 0;
  DECLARE jobId_new INT(11) DEFAULT 0;
  DECLARE statusId_new INT(11) DEFAULT 0;
  DECLARE secondaryId_new INT(11) DEFAULT 0;
  DECLARE status_new varchar(1) DEFAULT 0;
  DECLARE createdDateTime_new date DEFAULT null;  
  DECLARE createdBy_new INT(11) DEFAULT 0;
  DECLARE done INT DEFAULT FALSE;  
  DECLARE cursor_i CURSOR FOR (select jhfj.teacherStatusHistoryForJobId,teacherId, jobId , statusId , secondaryStatusId , status , createdDateTime, createdBy  from teacherstatushistoryforjob as jhfj where jhfj.teacherStatusHistoryForJobId=teacherStatusHistoryForJobId_delte);
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
  OPEN cursor_i;
  read_loop: LOOP
    FETCH cursor_i INTO id_new,teacherId_new,jobId_new,statusId_new,secondaryId_new,status_new,createdDateTime_new,createdBy_new;        
    IF done THEN
      LEAVE read_loop;
    END IF;      
  insert into deletethfjstatus(id,teacherId,jobId,statusId,secondaryId,status,createdDateTime,createdBy) values(id_new,teacherId_new,jobId_new,statusId_new,secondaryId_new,status_new,createdDateTime_new,createdBy_new);
  delete from teacherstatushistoryforjob where teacherStatusHistoryForJobId=teacherStatusHistoryForJobId_delte;
  END LOOP;
  CLOSE cursor_i;  
END//













DROP FUNCTION IF EXISTS `BASE64_ENCODE`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `BASE64_ENCODE`(input BLOB) RETURNS blob
    DETERMINISTIC
    SQL SECURITY INVOKER
BEGIN /* ADDED BY RAM NATH*/
	DECLARE ret BLOB DEFAULT '';
	DECLARE done TINYINT DEFAULT 0;
 
	IF input IS NULL THEN
		RETURN NULL;
	END IF;
 
each_block:
	WHILE NOT done DO BEGIN
		DECLARE accum_value BIGINT UNSIGNED DEFAULT 0;
		DECLARE in_count TINYINT DEFAULT 0;
		DECLARE out_count TINYINT;
 
each_input_char:
		WHILE in_count < 3 DO BEGIN
			DECLARE first_char CHAR(1);
 
			IF LENGTH(input) = 0 THEN
				SET done = 1;
				SET accum_value = accum_value << (8 * (3 - in_count));
				LEAVE each_input_char;
			END IF;
 
			SET first_char = SUBSTRING(input,1,1);
			SET input = SUBSTRING(input,2);
 
			SET accum_value = (accum_value << 8) + ASCII(first_char);
 
			SET in_count = in_count + 1;
		END; END WHILE;
 
		-- We've now accumulated 24 bits; deaccumulate into base64 characters
 
		-- We have to work from the left, so use the third byte position and shift left
		CASE
			WHEN in_count = 3 THEN SET out_count = 4;
			WHEN in_count = 2 THEN SET out_count = 3;
			WHEN in_count = 1 THEN SET out_count = 2;
			ELSE RETURN ret;
		END CASE;
 
		WHILE out_count > 0 DO BEGIN
			BEGIN
				DECLARE out_char CHAR(1);
				DECLARE base64_getval CURSOR FOR SELECT c FROM base64_data WHERE val = (accum_value >> 18);
 
				OPEN base64_getval;
				FETCH base64_getval INTO out_char;
				CLOSE base64_getval;
 
				SET ret = CONCAT(ret,out_char);
				SET out_count = out_count - 1;
				SET accum_value = accum_value << 6 & 0xffffff;
			END;
		END; END WHILE;
 
		CASE
			WHEN in_count = 2 THEN SET ret = CONCAT(ret,'=');
			WHEN in_count = 1 THEN SET ret = CONCAT(ret,'==');
			ELSE BEGIN END;
		END CASE;
 
	END; END WHILE;
 
	RETURN ret;
END//








DROP FUNCTION IF EXISTS `BASE64_DECODE`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `BASE64_DECODE`(input BLOB) RETURNS blob
    DETERMINISTIC
    SQL SECURITY INVOKER
BEGIN /* ADDED BY RAM NATH*/
	DECLARE ret BLOB DEFAULT '';
	DECLARE done TINYINT DEFAULT 0;
 
	IF input IS NULL THEN
		RETURN NULL;
	END IF;
 
each_block:
	WHILE NOT done DO BEGIN
		DECLARE accum_value BIGINT UNSIGNED DEFAULT 0;
		DECLARE in_count TINYINT DEFAULT 0;
		DECLARE out_count TINYINT DEFAULT 3;
 
each_input_char:
		WHILE in_count < 4 DO BEGIN
			DECLARE first_char CHAR(1);
 
			IF LENGTH(input) = 0 THEN
				RETURN ret;
			END IF;
 
			SET first_char = SUBSTRING(input,1,1);
			SET input = SUBSTRING(input,2);
 
			BEGIN
				DECLARE tempval TINYINT UNSIGNED;
				DECLARE error TINYINT DEFAULT 0;
				DECLARE base64_getval CURSOR FOR SELECT val FROM base64_data WHERE c = first_char;
				DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET error = 1;
 
				OPEN base64_getval;
				FETCH base64_getval INTO tempval;
				CLOSE base64_getval;
 
				IF error THEN
					ITERATE each_input_char;
				END IF;
 
				SET accum_value = (accum_value << 6) + tempval;
			END;
 
			SET in_count = in_count + 1;
 
			IF first_char = '=' THEN
				SET done = 1;
				SET out_count = out_count - 1;
			END IF;
		END; END WHILE;
 
		-- We've now accumulated 24 bits; deaccumulate into bytes
 
		-- We have to work from the left, so use the third byte position and shift left
		WHILE out_count > 0 DO BEGIN
			SET ret = CONCAT(ret,CHAR((accum_value & 0xff0000) >> 16));
			SET out_count = out_count - 1;
			SET accum_value = (accum_value << 8) & 0xffffff;
		END; END WHILE;
 
	END; END WHILE;
 
	RETURN ret;
END//




DROP FUNCTION IF EXISTS `SPLIT_STR`;
DELIMITER//
CREATE DEFINER=`root`@`localhost` FUNCTION `SPLIT_STR`(
  X VARCHAR(255),
  delim VARCHAR(12),
  pos INT
) RETURNS VARCHAR(255) CHARSET latin1 /* ADDED BY RAM NATH*/
RETURN REPLACE(SUBSTRING(SUBSTRING_INDEX(X, delim, pos),
       LENGTH(SUBSTRING_INDEX(X, delim, pos -1)) + 1),
       delim, '')//









DROP PROCEDURE IF EXISTS `ADMIN_DASHBOARD_NATIVE_REFERAL`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `ADMIN_DASHBOARD_NATIVE_REFERAL`()
BEGIN /* ADDED BY RAM NATH*/
  DECLARE total_teacher INT(10);
  DECLARE native_teacher INT(10);
  DECLARE authentication_native INT(10);
  DECLARE unauthentication_native INT(10) DEFAULT 0;
  DECLARE referal_teacher INT(10) DEFAULT 0;
  DECLARE authentication_referal INT(10) DEFAULT 0;
  DECLARE unauthentication_referal INT(10) DEFAULT 0;
  DECLARE emp_comp_native INT(10) DEFAULT 0;
  DECLARE emp_Incomp_native INT(10) DEFAULT 0;
  DECLARE emp_comp_referal INT(10) DEFAULT 0;
  DECLARE emp_Incomp_referal INT(10) DEFAULT 0;
  DECLARE time_out_native INT(10) DEFAULT 0;
  DECLARE time_out_referal INT(10) DEFAULT 0;
  DECLARE emp_comp_native_apply INT(10) DEFAULT 0;
  DECLARE emp_comp_referal_apply INT(10) DEFAULT 0;
  DECLARE done INT DEFAULT FALSE;

  DECLARE cursor_i CURSOR FOR (SELECT COUNT(*) /*total_teacher*/ , COUNT( IF( td.userType='N', 1, NULL ) ) /*native_teacher*/, COUNT( IF( (td.userType='N' AND td.verificationStatus=1), 1, NULL ) ) /*authentication_native*/, COUNT( IF( (td.userType='N' AND  td.verificationStatus=0), 1, NULL ) )  /*unauthentication_native*/, COUNT( IF( td.userType='R', 1, NULL ) ) /*referal_teacher*/ ,  COUNT( IF( (td.userType='R' AND td.verificationStatus=1), 1, NULL ) )  /*authentication_referal*/, COUNT( IF( (td.userType='R' AND td.verificationStatus=0), 1, NULL ) )  /* unauthentication_referal*/ FROM teacherdetail td); 
  DECLARE cursor_j CURSOR FOR (SELECT COUNT(IF( (techAss.statusId=4 AND td.userType='N' AND techAss.assessmentType=1 AND techAss.jobId IS NULL),1,NULL)) AS c1 /*emp_comp_native*/, COUNT(IF( (techAss.statusId=9 AND td.userType='N' AND techAss.assessmentType=1 AND techAss.jobId IS NULL),1,NULL)) AS c2 /*emp_Incomp_native*/, COUNT(IF( (techAss.statusId=4 AND td.userType='R' AND techAss.assessmentType=1 AND techAss.jobId IS NULL),1,NULL)) AS c3 /*emp_comp_referal*/, COUNT(IF( (techAss.statusId=9 AND td.userType='R' AND techAss.assessmentType=1 AND techAss.jobId IS NULL),1,NULL)) AS c4 /*emp_Incomp_referal*/ FROM teacherdetail td INNER JOIN teacherassessmentstatus techAss ON td.teacherId=techAss.teacherId WHERE YEAR(techAss.createdDateTime) =(YEAR(NOW())) );
  DECLARE cursor_k CURSOR FOR (SELECT COUNT(DISTINCT(teacherId)) FROM jobforteacher WHERE teacherId IN (SELECT  td.teacherId AS c1 /*emp_comp_native_apply*/ FROM  teacherdetail td INNER JOIN teacherassessmentstatus techAss ON td.teacherId=techAss.teacherId WHERE YEAR(techAss.createdDateTime) =(YEAR(NOW())) AND techAss.statusId=4 AND td.userType='N' AND techAss.assessmentType=1 AND techAss.jobId IS NULL ));
  DECLARE cursor_l CURSOR FOR (SELECT COUNT(DISTINCT(teacherId)) FROM jobforteacher WHERE teacherId IN (SELECT td.teacherId AS c1 /*emp_comp_referal_apply*/ FROM  teacherdetail td INNER JOIN teacherassessmentstatus techAss ON td.teacherId=techAss.teacherId WHERE YEAR(techAss.createdDateTime) =(YEAR(NOW())) AND techAss.statusId=4 AND td.userType='R' AND techAss.assessmentType=1 AND techAss.jobId IS NULL ));
  OPEN cursor_i;
    FETCH cursor_i INTO total_teacher,native_teacher,authentication_native,unauthentication_native,referal_teacher,authentication_referal,unauthentication_referal;        
   
 
  OPEN cursor_j;
    FETCH cursor_j INTO emp_comp_native,emp_Incomp_native,emp_comp_referal,emp_Incomp_referal;  
    
  OPEN cursor_k;
    FETCH cursor_k INTO emp_comp_native_apply;  
    
  OPEN cursor_l;
    FETCH cursor_l INTO emp_comp_referal_apply;        
  CLOSE cursor_j;
  CLOSE cursor_i; 
  CLOSE cursor_k;
  CLOSE cursor_l; 
  
  SELECT total_teacher AS total,
  native_teacher,
    authentication_native,                           
                                emp_comp_native,
                                emp_comp_native_apply,(emp_comp_native-emp_comp_native_apply) AS no_apply_anyJob_native ,
                                emp_Incomp_native,
                                (authentication_native-(emp_comp_native+emp_Incomp_native)) AS time_out_native,unauthentication_native,
                                referal_teacher,authentication_referal,
                                emp_comp_referal,
                                emp_comp_referal_apply,
                                (emp_comp_referal-emp_comp_referal_apply) AS no_apply_anyJob_referal,
                                emp_Incomp_referal,
                                (authentication_referal-(emp_comp_referal+emp_Incomp_referal)),
                                unauthentication_referal;
END//









DROP TRIGGER IF EXISTS `INSERT_DISTRICTREQUESTIONNUMBER_FOR_HISTORY`;
DELIMITER //
CREATE TRIGGER `INSERT_DISTRICTREQUESTIONNUMBER_FOR_HISTORY` AFTER INSERT ON `districtrequisitionnumbers`
 FOR EACH ROW begin /* ADDED BY RAM NATH*/
  if NEW.districtId=804800
  then
    INSERT INTO districtrequisitionnumbershistory (districtRequisitionHistoryId,districtRequisitionId,districtId,requisitionNumber,schoolId,jobCode,createdBy,jobTitle,status) VALUES(NULL,NEW.districtRequisitionId,NEW.districtId,NEW.requisitionNumber,NEW.schoolId,NEW.jobCode,NEW.createdBy,NEW.jobTitle,NEW.status);
  end if;
end//







DROP TRIGGER IF EXISTS `INSERT_IN_JOBFORTEACHER_FOR_DISTRICTID`;
DELIMITER //
CREATE TRIGGER `INSERT_IN_JOBFORTEACHER_FOR_DISTRICTID` BEFORE INSERT ON `jobforteacher`
 FOR EACH ROW begin/* ADDED BY RAM NATH*/
  if NEW.`districtId` IS NULL
  then
   SET NEW.`districtId` = (SELECT districtId FROM joborder AS JO WHERE JO.`jobId` = NEW.`jobId`);   
  end if;
end//







DROP TRIGGER IF EXISTS `INSERT_IN_TEACHERSTATUSHISTORYFORJOB_FOR_HIREDATE`;
DELIMITER //
CREATE TRIGGER `INSERT_IN_TEACHERSTATUSHISTORYFORJOB_FOR_HIREDATE` BEFORE INSERT ON `teacherstatushistoryforjob`
 FOR EACH ROW begin/* ADDED BY RAM NATH*/
  if (NEW.statusId=6 && NEW.hiredByDate IS NULL)
  then
    set NEW.hiredByDate=NEW.createdDateTime;
  end if;
end//










DROP TRIGGER IF EXISTS `UPDATE_JobStart_JobEnd_DATE`;
DELIMITER //
CREATE TRIGGER `UPDATE_JobStart_JobEnd_DATE` BEFORE UPDATE ON `joborder`
 FOR EACH ROW BEGIN /* ADDED BY RAM NATH*/
  IF New.approvalBeforeGoLive=1 && (Old.approvalBeforeGoLive=2 || Old.approvalBeforeGoLive=0) && Old.districtId=804800
  THEN
    IF  (DATE(NEW.jobEndDate)-DATE(SYSDATE())) < 7
      THEN
       SET NEW.jobStartDate=NOW();
       SET NEW.jobEndDate=DATE_ADD(NEW.jobStartDate, INTERVAL 7 DAY);
    END IF;
  END IF;
END//






--------------------------------------------------------Ram Nath(01-02-16)-------------------------------------------------------------
DROP PROCEDURE IF EXISTS INSERT_OR_UPDATE_DISERTCANDIDATE;
DELIMITER //

CREATE PROCEDURE `INSERT_OR_UPDATE_DISERTCANDIDATE`(in_teacherId INT(10), in_districtId INT(10), in_createdDateTime DATETIME, in_status INT(10), in_secondaryStatus INT(10)) 
BEGIN /* ADDED BY RAM NATH*/
  DECLARE fetchDistrictCandidateId INT(10);
  DECLARE COND_districtId INT(10);
  DECLARE COND_teacherId INT(10);
  DECLARE COUNT_ROW INT(10);
  SET @COUNT_ROW = 0;
  SET @fetchDistrictCandidateId=0;
  
  SELECT districtCandidateId INTO fetchDistrictCandidateId FROM districtcandidate WHERE teacherId=in_teacherId AND districtId=in_districtId LIMIT 0,1; 
  IF in_districtId IS NOT NULL AND (fetchDistrictCandidateId IS NULL OR fetchDistrictCandidateId=0) THEN
	IF in_status=6 THEN
			INSERT INTO districtcandidate 
			(districtCandidateId,teacherId,districtId,numberOfapplications,firstApplicationDate,mostRecentApplicationDate,hiringStatusInDistrict,
			hiringStatusInDistrictUpdatedDateTime,positionsHiredForInDistrict,statusInDistrict,statusInDistrictUpdatedDateTime,createdDateTime)
			VALUES 
			(NULL,in_teacherId,in_districtId,1,in_createdDateTime,in_createdDateTime,1,
			in_createdDateTime,1,'A',NOW(),now());
		ELSE		
			INSERT INTO districtcandidate 
			(districtCandidateId,teacherId,districtId,numberOfapplications,firstApplicationDate,mostRecentApplicationDate,hiringStatusInDistrict,
			hiringStatusInDistrictUpdatedDateTime,positionsHiredForInDistrict,statusInDistrict,statusInDistrictUpdatedDateTime,createdDateTime)
			VALUES 
			(NULL,in_teacherId,in_districtId,1,in_createdDateTime,in_createdDateTime,0,
			NULL,0,'A',NOW(),NOW());
		END IF;
  ELSEIF in_districtId IS NOT NULL && (fetchDistrictCandidateId IS NOT NULL || fetchDistrictCandidateId>0) THEN
  	IF in_status=6 THEN
			UPDATE districtcandidate  SET  numberOfapplications=(numberOfapplications+1), mostRecentApplicationDate=in_createdDateTime, hiringStatusInDistrict=1, hiringStatusInDistrictUpdatedDateTime=NOW(), positionsHiredForInDistrict=(positionsHiredForInDistrict+1), statusInDistrict='A', statusInDistrictUpdatedDateTime=NOW()	WHERE districtCandidateId=fetchDistrictCandidateId;
			ELSE		
			UPDATE districtcandidate  SET  numberOfapplications=(numberOfapplications+1), mostRecentApplicationDate=in_createdDateTime, statusInDistrict='A', statusInDistrictUpdatedDateTime=NOW() WHERE districtCandidateId=fetchDistrictCandidateId;	
	END IF;
  END IF;
END//


DROP TRIGGER IF EXISTS `INSERT_IN_JOBFORTEACHER_FOR_DISTRICTID`;
DELIMITER //
CREATE TRIGGER `INSERT_IN_JOBFORTEACHER_FOR_DISTRICTID` BEFORE INSERT ON `jobforteacher` 
    FOR EACH ROW BEGIN/* ADDED BY RAM NATH*/
  IF NEW.`districtId` IS NULL
  THEN
   SET NEW.`districtId` = (SELECT `districtId` FROM `joborder` AS JO WHERE JO.`jobId` = NEW.`jobId`);   
  END IF;
  CALL `INSERT_OR_UPDATE_DISERTCANDIDATE`(NEW.`teacherId` , NEW.`districtId`, NEW.`createdDateTime`, NEW.`status`,NEW.`displaySecondaryStatusId`);
END//





DROP PROCEDURE IF EXISTS `UPDATE_DISERTCANDIDATE_WHEN_JOBFORTEACHER_UPDATE`;
DELIMITER //

CREATE PROCEDURE `UPDATE_DISERTCANDIDATE_WHEN_JOBFORTEACHER_UPDATE`(in_jobId INT(10), in_teacherId INT(10), in_districtId INT(10), in_createdDateTime DATETIME, in_status_old INT(10), in_status INT(10), in_secondaryStatus INT(10)) 
BEGIN /* ADDED BY RAM NATH*/
  DECLARE fetchDistrictCandidateId INT(10);
  DECLARE fetchStatusInDistrict VARCHAR(1);
  DECLARE fetchStatusInDistrictUpdatedDateTime DATETIME;
  DECLARE setStatusInDistrictUpdatedDateTime DATETIME;
  DECLARE setStatusInDistrict VARCHAR(1);
  DECLARE COND_districtId INT(10);
  DECLARE COND_teacherId INT(10);
  DECLARE COUNT_ROW INT(10);
  DECLARE COUNT_FOR_DISTRICT_STATUS INT(10);
  DECLARE UPDATE_STAUTS_DATE_FLAG INT(1) DEFAULT 0;
  DECLARE done INT DEFAULT FALSE;
  DECLARE cursor_i CURSOR FOR (SELECT districtCandidateId,statusInDistrict,statusInDistrictUpdatedDateTime FROM districtcandidate WHERE teacherId=in_teacherId AND districtId=in_districtId LIMIT 0,1);
  SET @COUNT_ROW = 0;
  SET @fetchDistrictCandidateId=0;
  SET @COUNT_FOR_DISTRICT_STATUS=0;

  
  OPEN cursor_i;
  FETCH cursor_i INTO fetchDistrictCandidateId,fetchStatusInDistrict,fetchStatusInDistrictUpdatedDateTime;        
  CLOSE cursor_i;

  SELECT COUNT(jobforteacherId) INTO COUNT_FOR_DISTRICT_STATUS FROM jobforteacher WHERE teacherId=in_teacherId AND districtId=in_districtId AND STATUS NOT IN (7,8); 
 

  SELECT fetchStatusInDistrictUpdatedDateTime INTO setStatusInDistrictUpdatedDateTime;
  SELECT fetchStatusInDistrict INTO setStatusInDistrict;/*insert into checkPro (printMessage,districtstatus,statusDAte) values( concat('1 ',COUNT_FOR_DISTRICT_STATUS),fetchStatusInDistrict,fetchStatusInDistrictUpdatedDateTime); INSERT INTO checkPro (printMessage,districtstatus,statusDAte) VALUES('5',setStatusInDistrict,setStatusInDistrictUpdatedDateTime);*/  
  IF in_districtId IS NOT NULL && COUNT_FOR_DISTRICT_STATUS IS NOT NULL && COUNT_FOR_DISTRICT_STATUS = 0 && fetchStatusInDistrict='A' THEN
	SELECT NOW() INTO setStatusInDistrictUpdatedDateTime;
	SELECT 'I' INTO setStatusInDistrict;/*insert into checkPro (printMessage,districtstatus,statusDAte) values('2',setStatusInDistrict,setStatusInDistrictUpdatedDateTime);*/	
  END IF;
  IF in_districtId IS NOT NULL && COUNT_FOR_DISTRICT_STATUS IS NOT NULL && COUNT_FOR_DISTRICT_STATUS > 0 && fetchStatusInDistrict='I' THEN
	SELECT NOW() INTO setStatusInDistrictUpdatedDateTime;
	SELECT 'A' INTO setStatusInDistrict;/*insert into checkPro (printMessage,districtstatus,statusDAte) values('3',setStatusInDistrict,setStatusInDistrictUpdatedDateTime);*/	
  END IF;

  IF in_districtId IS NOT NULL && (fetchDistrictCandidateId IS NOT NULL || fetchDistrictCandidateId>0) THEN
  	IF in_status_old!=6 && in_status=6 THEN
			UPDATE districtcandidate  SET hiringStatusInDistrict=1, hiringStatusInDistrictUpdatedDateTime=NOW(), positionsHiredForInDistrict=(positionsHiredForInDistrict+1), statusInDistrict=setStatusInDistrict, statusInDistrictUpdatedDateTime=setStatusInDistrictUpdatedDateTime WHERE districtCandidateId=fetchDistrictCandidateId;
        ELSEIF in_status_old=6 && in_status!=6 THEN
			UPDATE districtcandidate  SET hiringStatusInDistrict=(CASE WHEN positionsHiredForInDistrict>1 THEN 1 ELSE 0 END), hiringStatusInDistrictUpdatedDateTime=(CASE WHEN positionsHiredForInDistrict>1 THEN (SELECT IFNULL(hiredByDate,createdDateTime) FROM teacherstatushistoryforjob WHERE jobId!=in_jobId AND teacherId=in_teacherId AND statusId=6 AND STATUS='A' LIMIT 0,1) ELSE NULL END), positionsHiredForInDistrict=(CASE WHEN positionsHiredForInDistrict>1 THEN (positionsHiredForInDistrict-1) ELSE 0 END), statusInDistrict=setStatusInDistrict, statusInDistrictUpdatedDateTime=setStatusInDistrictUpdatedDateTime WHERE districtCandidateId=fetchDistrictCandidateId;
			ELSE		
			UPDATE districtcandidate  SET  statusInDistrict=setStatusInDistrict, statusInDistrictUpdatedDateTime=setStatusInDistrictUpdatedDateTime WHERE districtCandidateId=fetchDistrictCandidateId;	
	END IF;
  END IF;
END//


DROP TRIGGER IF EXISTS `UPDATE_DISERTCANDIDATE_IFHIRE_FOR_DISTRICTID_OR_SCHOOL`;
DELIMITER //
CREATE TRIGGER `UPDATE_DISERTCANDIDATE_IFHIRE_FOR_DISTRICTID_OR_SCHOOL` AFTER UPDATE ON `jobforteacher`
 FOR EACH ROW BEGIN/* ADDED BY RAM NATH*/
DECLARE IS_EXISTS_CANDIDATE INT(10) default 0;
SELECT count(*) into IS_EXISTS_CANDIDATE FROM districtcandidate WHERE teacherId=NEW.`teacherId` AND districtId=NEW.`districtId` LIMIT 0,1;
 IF IS_EXISTS_CANDIDATE>0 && NEW.`districtId` IS NOT NULL && ((OLD.status!=6 && NEW.status=6) || (OLD.status=6 && NEW.status!=6) || NEW.status=7 || NEW.status=8 || ((OLD.status=7 && NEW.status!=7) || (OLD.status=8 && NEW.status!=8))) THEN
   CALL `UPDATE_DISERTCANDIDATE_WHEN_JOBFORTEACHER_UPDATE`(NEW.jobId, NEW.`teacherId` , NEW.`districtId`, NEW.`createdDateTime`, OLD.`status`,NEW.`status`,NEW.`displaySecondaryStatusId`);   
  END IF; 
END//





DROP PROCEDURE IF EXISTS UPDATE_DISERTCANDIDATE_WHEN_DELETE_JOBFORTEACHER;
DELIMITER //

CREATE PROCEDURE `UPDATE_DISERTCANDIDATE_WHEN_DELETE_JOBFORTEACHER`(in_jobforteacherId INT(10), in_jobId INT(10), in_teacherId INT(10), in_districtId INT(10), in_createdDateTime DATETIME, in_status_old INT(10), in_status INT(10), in_secondaryStatus INT(10)) 
BEGIN /* ADDED BY RAM NATH*/
  DECLARE fetchDistrictCandidateId INT(10);
  DECLARE fetchPositionsHiredForInDistrict INT(10);
  DECLARE fetchStatusInDistrict VARCHAR(10);
  DECLARE fetchStatusInDistrictUpdatedDateTime DATETIME;
  DECLARE fetchNumberOfapplications INT(10);
  DECLARE setStatusInDistrictUpdatedDateTime DATETIME;
  DECLARE setStatusInDistrict VARCHAR(1);
  DECLARE COND_districtId INT(10);
  DECLARE COND_teacherId INT(10);
  DECLARE COUNT_ROW INT(10);
  DECLARE COUNT_FOR_DISTRICT_STATUS INT(10);
  DECLARE done INT DEFAULT FALSE;
  DECLARE cursor_i CURSOR FOR (SELECT districtCandidateId,positionsHiredForInDistrict,statusInDistrict,statusInDistrictUpdatedDateTime,numberOfapplications FROM districtcandidate WHERE teacherId=in_teacherId AND districtId=in_districtId LIMIT 0,1);
  SET @COUNT_FOR_DISTRICT_STATUS=0;
  SET @COUNT_ROW = 0;
  SET @fetchDistrictCandidateId=0;
  SET @fetchPositionsHiredForInDistrict=0;

  SELECT COUNT(jobforteacherId) INTO COUNT_FOR_DISTRICT_STATUS FROM jobforteacher WHERE teacherId=in_teacherId AND districtId=in_districtId AND STATUS NOT IN (7,8) AND jobforteacherId!=in_jobforteacherId; 
  SELECT COUNT(jobforteacherId) INTO COUNT_ROW FROM jobforteacher WHERE teacherId=in_teacherId AND districtId=in_districtId;
  
  OPEN cursor_i;
  FETCH cursor_i INTO fetchDistrictCandidateId,fetchPositionsHiredForInDistrict,fetchStatusInDistrict,fetchStatusInDistrictUpdatedDateTime,fetchNumberOfapplications;        
  CLOSE cursor_i;
  
  SELECT fetchStatusInDistrictUpdatedDateTime INTO setStatusInDistrictUpdatedDateTime;
  SELECT fetchStatusInDistrict INTO setStatusInDistrict;/*INSERT INTO checkPro (printMessage,districtstatus,statusDAte) VALUES( CONCAT('1 ',COUNT_FOR_DISTRICT_STATUS),fetchStatusInDistrict,fetchStatusInDistrictUpdatedDateTime);  INSERT INTO checkPro (printMessage,districtstatus,statusDAte) VALUES('5',setStatusInDistrict,setStatusInDistrictUpdatedDateTime);*/
  IF COUNT_ROW>0 &&  in_districtId IS NOT NULL && COUNT_FOR_DISTRICT_STATUS IS NOT NULL && COUNT_FOR_DISTRICT_STATUS = 0 && fetchStatusInDistrict='A' THEN
	SELECT NOW() INTO setStatusInDistrictUpdatedDateTime;
	SELECT 'I' INTO setStatusInDistrict;/*INSERT INTO checkPro (printMessage,districtstatus,statusDAte) VALUES('2',setStatusInDistrict,setStatusInDistrictUpdatedDateTime);*/
  END IF;

  IF COUNT_ROW>0 && in_districtId IS NOT NULL && COUNT_FOR_DISTRICT_STATUS IS NOT NULL && COUNT_FOR_DISTRICT_STATUS > 0 && fetchStatusInDistrict='I' THEN
	SELECT NOW() INTO setStatusInDistrictUpdatedDateTime;
	SELECT 'A' INTO setStatusInDistrict;/*INSERT INTO checkPro (printMessage,districtstatus,statusDAte) VALUES('3',setStatusInDistrict,setStatusInDistrictUpdatedDateTime);	*/
  END IF;

  IF COUNT_ROW>0 && in_districtId IS NOT NULL && (fetchDistrictCandidateId IS NOT NULL || fetchDistrictCandidateId>0) THEN   
  	IF in_status=6 THEN
			UPDATE districtcandidate  SET 
			numberOfapplications=(CASE WHEN numberOfapplications>1 THEN (numberOfapplications-1) ELSE 0 END),
			mostRecentApplicationDate=(SELECT createdDateTime FROM jobforteacher WHERE teacherId=in_teacherId AND districtId=in_districtId  ORDER BY jobforteacherId DESC LIMIT 0,1),
			hiringStatusInDistrict=(CASE WHEN fetchPositionsHiredForInDistrict>1 THEN 1 ELSE 0 END), 
			hiringStatusInDistrictUpdatedDateTime=(CASE WHEN fetchPositionsHiredForInDistrict>1 THEN (SELECT IFNULL(hiredByDate,createdDateTime) FROM teacherstatushistoryforjob WHERE jobId!=in_jobId AND teacherId=in_teacherId AND statusId=6 AND STATUS='A' LIMIT 0,1) ELSE NULL END), 
			positionsHiredForInDistrict=(CASE WHEN fetchPositionsHiredForInDistrict>0 THEN (positionsHiredForInDistrict-1) ELSE 0 END), 
			statusInDistrict=setStatusInDistrict, 
			statusInDistrictUpdatedDateTime=setStatusInDistrictUpdatedDateTime	
			WHERE districtCandidateId=fetchDistrictCandidateId;
			ELSE	
			UPDATE districtcandidate  SET 
			numberOfapplications=(CASE WHEN numberOfapplications>1 THEN (numberOfapplications-1) ELSE 0 END), 
			mostRecentApplicationDate=(SELECT createdDateTime FROM jobforteacher WHERE teacherId=in_teacherId AND districtId=in_districtId ORDER BY jobforteacherId DESC LIMIT 0,1), 
			statusInDistrict=setStatusInDistrict, 
			statusInDistrictUpdatedDateTime=setStatusInDistrictUpdatedDateTime 
			WHERE districtCandidateId=fetchDistrictCandidateId;	
	END IF;
  END IF; /*INSERT INTO checkPro (printMessage) VALUES(CONCAT('call delete ',COUNT_ROW, '   ',fetchNumberOfapplications));  */
  IF COUNT_ROW = 0 && fetchNumberOfapplications=1 THEN
	DELETE FROM districtcandidate WHERE  teacherId=in_teacherId AND districtId=in_districtId;
  END IF;
END//




DROP TRIGGER IF EXISTS `UPDATE_DISERTCANDIDATE_IFDELETE_JOBFORTEACHER`;
DELIMITER //
CREATE  TRIGGER `UPDATE_DISERTCANDIDATE_IFDELETE_JOBFORTEACHER` AFTER DELETE ON `jobforteacher` 
  FOR EACH ROW BEGIN/* ADDED BY RAM NATH*/
  DECLARE IS_EXISTS_CANDIDATE INT(10) default 0;
  SELECT count(*) into IS_EXISTS_CANDIDATE FROM districtcandidate WHERE teacherId=OLD.`teacherId` AND districtId=OLD.`districtId` LIMIT 0,1;
  IF IS_EXISTS_CANDIDATE>0 && OLD.`districtId` IS NOT NULL THEN /*INSERT INTO checkPro (printMessage) VALUES('call delete');  */
   CALL `UPDATE_DISERTCANDIDATE_WHEN_DELETE_JOBFORTEACHER`(OLD.jobforteacherId, OLD.jobId, OLD.`teacherId` , OLD.`districtId`, OLD.`createdDateTime`, OLD.`status`,OLD.`status`,OLD.`displaySecondaryStatusId`);   
  END IF;  
END//
--------------------------------------------------------------------------------------------------------------------------------------
