var deviceTypeAndroid=$.browser.device = (/Android|webOS|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent.toLowerCase()));
var deviceType=$.browser.device = (/iPhone|iPad|iPod/i.test(navigator.userAgent.toLowerCase()));
var xmlHttp=null;
var txtBgColor="#F5E7E1";
function createXMLHttpRequest()
{
	if (window.ActiveXObject)
    	xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
   	else if (window.XMLHttpRequest)
		xmlHttp = new XMLHttpRequest();
        else
        	alert(resourceJSON.msgAjaxNotSupported);
}
function removeResume()
{	
	if(window.confirm(resourceJSON.msgsureremoveresum))
	{
		$('#resume').css("background-color","");
		$('#errordiv').empty();
		PFExperiences.deleteResume({ 
		async: true,
		errorHandler:handleError,
		callback: function(data)
		{
			document.getElementById("hrefExperiences").className = "";
			document.getElementById("divResumeTxt").innerHTML="";
			document.getElementById("hdnResume").value="";
			document.getElementById("frmExpResumeUpload").reset();
		}});
	}
	return false;
}
function hideDiv()
{
	$('#docfileNotOpen').hide();
	$('#exelfileNotOpen').hide();
}
function downloadResume()
{
	PFExperiences.downloadResume(
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
			if (data.indexOf(".doc") !=-1) {
			    document.getElementById('ifrmResume').src = ""+data+"";
			}
			else
			{
				document.getElementById("hrefResume").href = data;
				return false;
			}
			
			if(deviceType)
			{
				if (data.indexOf(".doc")!=-1)
				{
					$("#docfileNotOpen").css({"z-index":"3000"});
			    	$('#docfileNotOpen').show();
				}
				else if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					$("#exelfileNotOpen").css({"z-index":"3000"});
			    	$('#exelfileNotOpen').show();
				}
				else
				{
					document.getElementById("hrefResume").href = data;
				}
			}
			else if(deviceTypeAndroid)
			{
				if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					document.getElementById('ifrmResume').src = ""+data+"";
				}
				else
				{
					document.getElementById("hrefResume").href = data;	
				}	
			}
			else if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
			{
				document.getElementById('ifrmResume').src = ""+data+"";
			}
			else
			{
				document.getElementById("hrefResume").href = data;	
			}			 
			return false;
			
		}
	});
}

/*function saveAndContinueResume()
{
	var fileName=document.getElementById("resume").value;
	var hdnResume = document.getElementById("hdnResume").value;
	var cnt=0;
	var focs=0;	

	$('#resume').css("background-color","");
	$('#errorDivResume').empty();

	if(fileName!="")
	{
		var ext = fileName.substr(fileName.lastIndexOf('.') + 1).toLowerCase();

		var fileSize=0;		
		if ($.browser.msie==true)
		{	
			fileSize = 0;	   
		}
		else
		{	
			if(document.getElementById("resume").files[0]!=undefined)
				fileSize = document.getElementById("resume").files[0].size;
		}

		if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt'))
		{
			$('#errorDivResume').append("&#149; Please select Acceptable resume formats include PDF, MS-Word, GIF, PNG, and JPEG  files<br>");
			if(focs==0)
				$('#resume').focus();

			$('#resume').css("background-color",txtBgColor);
			cnt++;focs++;	

		}
		else if(fileSize>=10485760)
		{		
			$('#errorDivResume').append("&#149; File size must be less then 10mb.<br>");
			if(focs==0)
				$('#resume').focus();

			$('#resume').css("background-color",txtBgColor);
			cnt++;focs++;	

		}
		else
		{		
			document.getElementById("frmExpResumeUpload").submit();
			$('#loadingDiv').show();

		}
	}

	return true;
}*/

function saveResume(ext)
{
	PFExperiences.addOrEditResume(ext, { 
	async: true,
	errorHandler:handleError,
	callback:function(data)
	{
		document.getElementById("frmExpResumeUpload").reset();
		$("a#hrefResume").html(ext);
		getTFAValues();
		validatePortfolioErrorMessageAndGridData('level2');
		
	}});
}

function hideTFAFields_DP()
{
	$('#errordivExp').empty();
	$('#expCertTeacherTraining').css("background-color","");
	$('#tfaAffiliate').css("background-color","");
	$('#corpsYear').css("background-color","");
	$('#tfaRegion').css("background-color","");
	
	var tfaAffiliate 	= 	document.getElementById("tfaAffiliate").value;
	if(tfaAffiliate=="3")//if(tfaAffiliate=="3" || tfaAffiliate=='')
	{
		try { document.getElementById("corpsYear").value=""; } catch (e) {}
		try { document.getElementById("tfaRegion").value=""; } catch (e) {}
		$("#tfaFieldsDiv").fadeOut();
	}
	else
	{
		$("#tfaFieldsDiv").fadeIn();
	}
}

/*function saveAndContinueTFA(tFA_config)
{
	var tfaAffiliate 	= 	document.getElementById("tfaAffiliate").value;
	var corpsYear 		= 	document.getElementById("corpsYear").value;
	var tfaRegion 		= 	document.getElementById("tfaRegion").value;
	
	var cnt=0;
	var focs=0;	
	
	$('#errordiv_TFA').empty();
	//setDefColortoErrorMsgToExp();
	
	$('#expCertTeacherTraining').css("background-color","");
	$('#tfaAffiliate').css("background-color","");
	$('#corpsYear').css("background-color","");
	$('#tfaRegion').css("background-color","");
	
	if(trim(tfaAffiliate)=="")
	{
		$('#errordiv_TFA').append("&#149; Please select TFA Affiliate<br>");
		if(focs==0)
			$('#tfaAffiliate').focus();
		
		$('#tfaAffiliate').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	
	if(tfaAffiliate!="3" && tfaAffiliate!="")
	{
		if(trim(corpsYear)=="")
		{
			$('#errordiv_TFA').append("&#149; Please select Corps Year<br>");
			if(focs==0)
				$('#corpsYear').focus();
			
			$('#corpsYear').css("background-color",txtBgColor);
			cnt++;focs++;
		}
		
		if(trim(tfaRegion)=="")
		{
			$('#errordiv_TFA').append("&#149; Please select TFA Region<br>");
			if(focs==0)
				$('#tfaRegion').focus();
			
			$('#tfaRegion').css("background-color",txtBgColor);
			cnt++;focs++;
		}
	}
	if(cnt!=0 && tFA_config==1)		
	{
		$('#errordiv_TFA').show();
		return false;
		
	}
	else if(cnt!=0 && tfaAffiliate!="3" && tfaAffiliate!="")		
	{
		$('#errordiv_TFA').show();
		return false;
	}
	else
	{
		if(tfaAffiliate==null || tfaAffiliate=='')
			tfaAffiliate =0;
		if(corpsYear==null || corpsYear=='')
			corpsYear =0;
		if(tfaRegion==null || tfaRegion=='')
			tfaRegion =0;
		
		PFExperiences.insertOrUpdateTeacherTFA(tfaAffiliate,corpsYear,tfaRegion,
		{ 
			async: false,
			errorHandler:handleError,
			callback: function(data)
			{	
				validatePortfolioErrorMessageAndGridData('level2');
				return data;
			}
		});
		
	}
	
}*/

function getTFAValues()
{
	PFExperiences.getTFAValues(
	{ 
		async: false,
		errorHandler:handleError,
		callback: function(data)
		{	
			if(data!=null)
			{
				if(data.tfaAffiliateMaster!=null && data.tfaAffiliateMaster.tfaAffiliateId > 0)
					document.getElementById("tfaAffiliate").value=data.tfaAffiliateMaster.tfaAffiliateId;
				else
					document.getElementById("tfaAffiliate").value="";
				
				if(data.corpsYear!=null && data.corpsYear > 0)
					document.getElementById("corpsYear").value=data.corpsYear;
				else
					document.getElementById("corpsYear").value="";
				
				if(data.tfaRegionMaster!=null && data.tfaRegionMaster.tfaRegionId > 0)
					document.getElementById("tfaRegion").value=data.tfaRegionMaster.tfaRegionId;
				else
					document.getElementById("tfaRegion").value="";
				
				if(data.canServeAsSubTeacher==0)
					document.getElementById('canServeAsSubTeacher0').checked=true;
				else if(data.canServeAsSubTeacher==1)
					document.getElementById('canServeAsSubTeacher1').checked=true;
				
				if(data.resume!=null)
					document.getElementById("hdnResume").value=data.resume;
				
				if(data.expCertTeacherTraining!=null && data.expCertTeacherTraining >= 0)
					document.getElementById("expCertTeacherTraining").value=data.expCertTeacherTraining;
				else
					document.getElementById("expCertTeacherTraining").value="";
				
				if(data.nationalBoardCert!=null && data.nationalBoardCert==false)
				{
					document.getElementById("nbc2").checked=true;
					document.getElementById("nbc1").checked=false;
					document.getElementById("divNbdCert").style.display="none";
				}
				else
				{
					document.getElementById("nbc1").checked=true;
					document.getElementById("nbc2").checked=false;
					document.getElementById("divNbdCert").style.display="block";
					
					if(data.nationalBoardCertYear!=null)
					{
						try
						{
							document.getElementById("nationalBoardCertYear"+data.nationalBoardCertYear).selected=true;
						}
						catch(e){}
					}
					else
						document.getElementById("psl").selected=true;
				}
				
				if(data.isNonTeacher)
				{
					document.getElementById("isNonTeacher").checked=true;
					document.getElementById("isNonTeacherYes").checked=false;
					//document.getElementById("expCertTeacherTraining").disabled=true;
					document.getElementById("expCertTeacherTraining").value="0.0";
					$(".expDiv").hide();
				}else if(data.isNonTeacher==null)
				{
					document.getElementById("isNonTeacher").checked=true;
					document.getElementById("isNonTeacherYes").checked=false;
					//document.getElementById("expCertTeacherTraining").disabled=true;
					document.getElementById("expCertTeacherTraining").value="0.0";
					$(".expDiv").hide();
				}
				else
				{
					document.getElementById("isNonTeacher").checked=false;
					document.getElementById("isNonTeacherYes").checked=true;
					//document.getElementById("expCertTeacherTraining").disabled=false;
					$(".expDiv").show();
				}
			}
			hideTFAFields_DP();
			getPersonalInfoValues();
		}
	});
}

function getAffidavitValue()
{
	PFExperiences.getAffidavitValue(
	{ 
		async: false,
		errorHandler:handleError,
		callback: function(data)
		{	
			if(data!=null)
			{
				var affidavit = document.getElementsByName("affidavit");
				
				if(data.isAffidavitCompleted)
				{
					affidavit[0].checked=true;
					$('#affidavitDiv').hide();
				}
				else
				{
					affidavit[0].checked=false;
					$('#affidavitDiv').show();
				}
					
			}
		}
	});
}



function getPersonalInfoValues()
{
	PFExperiences.getTeacherPersonalInfoValues(
	{ 
		async: false,
		errorHandler:handleError,
		callback: function(data)
		{	
			if(data!=null)
			{
				if(data.phoneNumber!=null && data.phoneNumber!="")
				{
					document.getElementById("phoneNumber1").value=data.phoneNumber.substring(0,3);
					document.getElementById("phoneNumber2").value=data.phoneNumber.substring(3,6);
					document.getElementById("phoneNumber3").value=data.phoneNumber.substring(6,10);
				}
				else
				{
					document.getElementById("phoneNumber1").value="";
					document.getElementById("phoneNumber2").value="";
					document.getElementById("phoneNumber3").value="";
				}
				if(data.expectedSalary!=null && data.expectedSalary!=""){
					$("#expectedSalary").val(data.expectedSalary);
				}
				
				if(data.addressLine1!=null && data.addressLine1!="")
					document.getElementById("addressLine1").value=data.addressLine1;
				else
					document.getElementById("addressLine1").value="";
				
				if(data.addressLine2!=null && data.addressLine2!="")
					document.getElementById("addressLine2").value=data.addressLine2;
				else
					document.getElementById("addressLine2").value="";
				if(data.zipCode!=null && data.zipCode!="")
					document.getElementById("zipCode").value=data.zipCode;
				else
					document.getElementById("zipCode").value="";
				
				
				if(data.presentAddressLine1!=null && data.presentAddressLine1!="")
					document.getElementById("addressLinePr").value=data.presentAddressLine1;
				else
					document.getElementById("addressLinePr").value="";
				
				if(data.presentAddressLine2!=null && data.presentAddressLine2!="")
					document.getElementById("addressLine2Pr").value=data.presentAddressLine2;
				else
					document.getElementById("addressLine2Pr").value="";
				
				if(data.presentZipCode!=null && data.presentZipCodepresentZipCode!="")
					document.getElementById("zipCodePr").value=data.presentZipCode;
				else
					document.getElementById("zipCodePr").value="";
				
				
				//try{
					//----- Start :: Country, State, City------------
					if(data.countryId!=null)
					{
						document.getElementById("ctry"+data.countryId.countryId).selected=true;
						
						if(data.countryId.countryId==223)
						{
							if(data.stateId!=null)
							{	
								document.getElementById("st_"+data.stateId.stateId).selected=true;
								getCityListByStateSetForDSPQ(data);
							}
							else
								document.getElementById("slst").selected=true;
						}
						else
						{
							if(document.getElementById("countryCheck").value==1)
							{
								if(data.stateId!=null)
								{
									//document.getElementById("st_"+data.stateId.stateId).selected=true;
									//getCityListByStateSetForDSPQ(data);
									if(	data.otherCity!=null)
									{
										document.getElementById("otherCity").value=data.otherCity;
									}
								}
								else
									document.getElementById("slst").selected=true;
							}
							else
							{
								if((data.otherState!=null && data.otherState!="") || (data.otherCity!=null && data.otherCity!=""))
								{
									if(data.otherState!=null)
										document.getElementById("otherState").value=data.otherState;
									if(data.otherCity!=null)
										document.getElementById("otherCity").value=data.otherCity;
									
									$('#divotherstate').show();
									$('#divothercity').show();

									$('#divUSAState').hide();
									$('#divUSACity').hide();
								}
							}
						}
					}
										
				
					//try{
						//----- Start :: Country, State, City------------
						if(data.presentCountryId!=null)
						{
							try{document.getElementById("ctryPr"+data.presentCountryId.countryId).selected=true;}catch(e){}
							
							if(data.presentCountryId.countryId==223)
							{
								if(data.presentStateId!=null)
								{	
									try{document.getElementById("st_Pr"+data.presentStateId.stateId).selected=true;}catch(e){}
									getCityListByStateSetForDSPQPr(data);
								}
								else
									document.getElementById("slstPr").selected=true;
							}
							else
							{
								if(document.getElementById("countryCheckPr").value==1)
								{
									if(data.presentStateId!=null)
									{
										//document.getElementById("st_"+data.stateId.stateId).selected=true;
										//getCityListByStateSetForDSPQ(data);
										if(	data.persentOtherCity!=null)
										{
											document.getElementById("otherCityPr").value=data.persentOtherCity;
										}
									}
									else
										document.getElementById("slstPr").selected=true;
								}
								else
								{
									if((data.persentOtherState!=null && data.persentOtherState!="") || (data.persentOtherCity!=null && data.persentOtherCity!=""))
									{
										if(data.persentOtherState!=null)
											document.getElementById("otherStatePr").value=data.persentOtherState;
										if(data.persentOtherCity!=null)
											document.getElementById("otherCityPr").value=data.persentOtherCity;
										
										$('#divotherstatePr').show();
										$('#divothercityPr').show();

										$('#divUSAStatePr').hide();
										$('#divUSACityPr').hide();
									}
								}
							}
						}
					//---------- End---------
					
				//}catch(err){alert("error");}
					if(data.drivingLicNum!=null && data.drivingLicNum!="")
						document.getElementById("drivingLicNum").value=data.drivingLicNum;
					
					if(data.drivingLicState!=null && data.drivingLicState!="" && data.drivingLicState!=0)
						document.getElementById("drivingLicState").value=data.drivingLicState;
				if(data.firstName!=null && data.firstName!="")
					document.getElementById("firstName_pi").value=data.firstName;
				else
					document.getElementById("firstName_pi").value="";
				
				if(data.middleName!=null && data.middleName!="")
					document.getElementById("middleName_pi").value=data.middleName;
				else
					document.getElementById("middleName_pi").value="";
				
				if(data.lastName!=null && data.lastName!="")
					document.getElementById("lastName_pi").value=data.lastName;
				else
					document.getElementById("lastName_pi").value="";
				
				if(data.anotherName!=null && data.anotherName!="")
					document.getElementById("anotherName").value=data.anotherName;
				else
					document.getElementById("anotherName").value="";
				
				if(data.SSN!=null && data.SSN!="")
				{
					if(document.getElementById("districtIdForDSPQ").value==3680340)
					{
						if(data.SSN.length==4)
							document.getElementById("ssn_pi").value=data.SSN;
						else
						{
							var s = data.SSN.substring(5);
							document.getElementById("ssn_pi").value=s;	
						}
					}else if(document.getElementById("districtIdForDSPQ").value==1200390)
					{
						document.getElementById("ssn_pi").value=data.SSN;	
					}else if(document.getElementById("districtIdForDSPQ").value==4218990)
					{
						document.getElementById("ssn_pi").value=data.SSN;	
					}
					else if((document.location.hostname=="nccloud.teachermatch.org" || document.location.hostname=="nccloud.teachermatch.org"))						
					{
												
						var last4ssn=data.SSN;
						
						if(last4ssn!=null && last4ssn!='')
						{
			          	var aftrsnn=last4ssn.slice(-4);				
			        	$("#last4snnfornc").show();				     	
			           	document.getElementById("ssn_pifornc").innerHTML=aftrsnn;				           	
			           	$("#4ssnlast").hide();			           	
			        	var x = document.getElementById("ssn_pi");
			        	x.setAttribute("type", "hidden");	
						}
			           	
			        	document.getElementById("ssn_pi").value=data.SSN;
					
					}
					else
					{	
						document.getElementById("ssn_pi").value=data.SSN;
					}
				}
				else
					document.getElementById("ssn_pi").value="";
				
				
				if(data.dob!=null && data.dob!="")
				{
					//document.getElementById("dob").value=(new Date(data.dob).getMonth()+1)+"-"+(new Date(data.dob).getDate())+"-"+(new Date(data.dob).getFullYear());
					
					document.getElementById("dobMonth").value=new Date(data.dob).getMonth()+1;
					document.getElementById("dobDay").value=new Date(data.dob).getDate();
					document.getElementById("dobYear").value=new Date(data.dob).getFullYear();
				}
				else
				{
					//document.getElementById("dob").value="";
					document.getElementById("dobMonth").value="0";
					document.getElementById("dobDay").value="0";
					document.getElementById("dobYear").value="";
				}
				
				
			//	if(data.isVateran!=null && data.isVateran!="")
			//	{
				
					if(data.isVateran==1)
					{
						document.getElementById("vt1").checked=true;
					}
					else if(data.isVateran==0 || data.isVateran==null)
					{
						document.getElementById("vt2").checked=true;
					}
			//	}
				
				if(data.salutation!=null && data.salutation!="")
				{
					var saluation=document.getElementById("salutation_pi");
					for(var i=0; i<saluation.options.length;i++)
					{	
						if(saluation.options[i].value==data.salutation){
							saluation.options[i].selected=true;
							break;
						}
					}
				}
				else
					document.getElementById("salutation_pi").value="";
				
				 var flagForEmp = false;
				
				if(data.employeeType!=null && data.employeeType==1)
				{
					document.getElementById("fe2").checked=true;
					if(data.isCurrentFullTimeTeacher==1)
					    document.getElementById("rdCEmp1").checked=true;
					else
						document.getElementById("rdCEmp1").checked=false;
					
					if(data.isCurrentFullTimeTeacher==0)
						document.getElementById("rdCEmp2").checked=true;
					else
						document.getElementById("rdCEmp2").checked=false;
					
					if(data.isCurrentFullTimeTeacher==2)
						document.getElementById("rdCEmp3").checked=true;
					else
						document.getElementById("rdCEmp3").checked=false;
					
					$("#empfe2").val(data.employeeNumber);
					
					if(data.employeeNumber!=null && data.employeeNumber!=""){
						$("#empfe2").val(data.employeeNumber);
						if(document.getElementById("districtIdForDSPQ").value==804800){
							var tempZip="";
							if(data.zipCode!=null && data.zipCode!=""){
								tempZip=data.zipCode;
							}
							
							/*try{
							    DistrictPortfolioConfigAjax.doNotHiredForJeffco(data.employeeNumber,{
					                  async: false,
					                  errorHandler:handleError,
					                  callback:function(data){
					                  //alert("success");
					            }
					                  
					            });
	
							}catch(err){}*/
							
							openEmployeeNumberInfo(data.employeeNumber,tempZip);
						}
					}
					/*if(data.employeeNumber!=null && data.employeeNumber!="")
						document.getElementById("empfe2").disabled=true;
					
					if(document.getElementById("empchk2").checked)
						document.getElementById("empchk2").disabled=true;*/
					
					$("#fe2Div").show();
					$("#fe1Div").hide();
					
					if(data.employeeNumber!="")
						flagForEmp=true;
				}
				else if(data.employeeType!=null && data.employeeType==0)
				{	
					document.getElementById("fe1").checked=true;
					//document.getElementById("empchk1").checked=true;
					$("#empfe1").val(data.employeeNumber)
					
					$("#empfewDiv").hide();
					if(document.getElementById("districtIdForDSPQ").value==1302010)
					{
						$("#empfewDiv").show();
						if(data.noLongerEmployed!=null && data.noLongerEmployed!="")
							$('#empfewDiv').find(".jqte_editor").html(data.noLongerEmployed);
					}
				
					/*if(data.employeeNumber!="")
						document.getElementById("empfe1").disabled=true;
					*/
					if(data.retirementdate!=null && data.retirementdate!="")
					{
						$("#rtDateDiv").show();
						document.getElementById("empchk11").checked=true;
						$("#rtDate").val((new Date(data.retirementdate).getMonth()+1)+"-"+(new Date(data.retirementdate).getDate())+"-"+(new Date(data.retirementdate).getFullYear()));
						
						/*document.getElementById("empchk11").disabled=true;
						document.getElementById("rtDate").disabled=true;*/
					}
					else{
						document.getElementById("empchk11").checked=false;
						$("#rtDateDiv").hide();
						$("#divErrorMsg").empty();
					}
					
					if(data.moneywithdrawaldate!=null &&  data.moneywithdrawaldate!="")
					{
						$("#wtDateDiv").show();
						document.getElementById("empchk12").checked=true;
						$("#wdDate").val((new Date(data.moneywithdrawaldate).getMonth()+1)+"-"+(new Date(data.moneywithdrawaldate).getDate())+"-"+(new Date(data.moneywithdrawaldate).getFullYear()));
						
						/*document.getElementById("empchk12").disabled=true;
						document.getElementById("wdDate").disabled=true;*/
					}
					else{
						document.getElementById("empchk12").checked=false;
						$("#wtDateDiv").hide();
						$("#divErrorMsg").empty();
					}
				//	if(document.getElementById("empchk1").checked)
				//		document.getElementById("empchk1").disabled=true;
					
					$("#fe1Div").show();
					$("#fe2Div").hide();
					
					if(data.employeeNumber!="" || document.getElementById("empchk11").checked)
						flagForEmp=true;
				}
				else if(data.employeeType!=null && data.employeeType==2)
				{
					document.getElementById("fe1").checked=false;
					document.getElementById("fe2").checked=false;
					document.getElementById("fe3").checked=true;
					$("#fe1Div").hide();
					$("#fe2Div").hide();
					flagForEmp=true;
				}
				
				/*if(flagForEmp)
				{
					document.getElementById("fe1").disabled=true;
					document.getElementById("fe2").disabled=true;
					document.getElementById("fe3").disabled=true;
				}*/
				
				if(data.raceId!=null)
				{
					var selectedRaceId = data.raceId.split(",");
					var elements = document.getElementsByName('raceId');
					
					for (i=0;i<elements.length;i++) 
					{
						for(var j=0;j<selectedRaceId.length;j++)
						{
							if(elements[i].value == selectedRaceId[j]) 
							  {
							    elements[i].checked = true;
							  }
						}
					}
				}
				if(data.genderId!=null)
				{
					var miami = document.getElementById("isMiami").value;
					
					var genderElements = document.getElementsByName('genderId');
					
					/*if(miami=='true')
					{
						document.getElementById("displayGender3").style.display="none";
					}
					else 
					{
						document.getElementById("displayGender3").style.display="block";
					}*/
					
					for (i=0;i<genderElements.length;i++) 
					{
						if(miami==true)
						{
							if(genderElements[i].value==1 || genderElements[i].value==2)
								if(genderElements[i].value == data.genderId.genderId) 
									genderElements[i].checked = true;
						}
						else 
						{
							if(genderElements[i].value == data.genderId.genderId) 
								genderElements[i].checked = true;
						}
					}
					
					
				}
				else
				{
					/*var miami = document.getElementById("isMiami").value;
					if(miami=='true')
					{
						document.getElementById("displayGender3").style.display="none";
					}
					else 
					{*/
						document.getElementById("displayGender3").style.display="block";
					/*}*/
				}
				
				try
				{
					var chkIsRetired = 0;
					
					if(data.retirementnumber!=null)
					{
						chkIsRetired++;
						document.getElementById("retireNo").value=data.retirementnumber;
					}
					if(data.stateMaster!=null && data.stateMaster.stateId>0)
					{
						chkIsRetired++;
						document.getElementById("stMForretire").value=data.stateMaster.stateId;
					}
					if(data.districtmaster!=null)
					{
						chkIsRetired++;
						document.getElementById("retireddistrictName").value=data.districtmaster.districtName;
						document.getElementById("retireddistrictId").value=data.districtmaster.districtId;
					}
					
					if(chkIsRetired==3)
					{
						document.getElementById("isretired").checked=true;
						$("#isRetiredDiv").show();
					}
					else
					{
						document.getElementById("isretired").checked=false;
						$("#isRetiredDiv").hide();
					}
					
				}catch(err){}
				
				if(data.ethnicOriginId!=null)
				{
					var elements = document.getElementsByName('ethnicOriginId');
					for (i=0;i<elements.length;i++) 
					{
					  if(elements[i].value == data.ethnicOriginId.ethnicOriginId) 
					  {
					    elements[i].checked = true;
					  }
					}
				}
				
				if(data.ethnicityId!=null)
				{
					var elements = document.getElementsByName('ethinicityId');
					for (i=0;i<elements.length;i++) 
					{
					  if(elements[i].value == data.ethnicityId.ethnicityId) 
					  {
					    elements[i].checked = true;
					  }
					}
				}
				
			}
			//hideTFAFields_DP();
		}
	});
}

/* @Start
 * @Ashish Kumar
 * @Description :: Get Employment Details
 * */
/*var dp_Employment_page=1;
var dp_Employment_noOfRows=100;
var dp_Employment_sortOrderStr="";
var dp_Employment_sortOrderType="";*/

function getPFEmploymentDataGrid()
{
	PFExperiences.getPFEmploymentGrid(dp_Employment_noOfRows,dp_Employment_page,dp_Employment_sortOrderStr,dp_Employment_sortOrderType, { 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#divDataGridEmployment').html(data);
			applyScrollOnTbl_EmployeementHistory();
			if(document.getElementById("districtIdForDSPQ").value==7800036 || document.getElementById("districtIdForDSPQ").value==1201470){
				$('#empAnnSal').html("");
				$('.empAnnSalRec').html("");
			}
			
			if($('#districtIdForDSPQ').val()==4218990 && $('#isPrinciplePhiladelphia').val()=="1"){
				$('#empAnnSal').html("");
				$('.empAnnSalRec').html("");
				$("#empDurationHeader").html("");
				$(".empDurationRec").html("");
			}
			if($('#districtIdForDSPQ').val()==3904493){
				$('#lblCompanyName').html("Company Name");
			}
			
			if($('#districtIdForDSPQ').val()==804800){				
				//$('.empRoleTypegrd').replace("General Member", "Employee");
				for (i = 0; i < $('.empRoleTypegrd').length; i++) { 
				   if($('.empRoleTypegrd').html()=="General Member"){
					   //$('.empRoleTypegrd').html("Employee");
					   var chngId=$('.empRoleTypegrd').attr("id");
					   $("#"+chngId).html("Employee");
					   
				   } 
				}
			} 
			//empRoleTypegrd
		}});
}


function insertOrUpdateEmployment(sbtsource)
{
	
	var idonothaveworkexp=false;
	try {
		idonothaveworkexp=document.getElementById("idonothaveworkexp").checked;	
	} catch (e) {
		// TODO: handle exception
	}
	
	if(sbtsource=="0")
	{
		resetSBTNSource();
	}
	var cnt=0;
	var focs=0;	
	
	var threadCount_emp=document.getElementById("threadCount_emp").value;
	if(threadCount_emp=="0")
	{
		
		document.getElementById("threadCount_emp").value=1;
		
		updateThreadCount("emp");
		
		if(idonothaveworkexp==false)
		{
		var position = document.getElementById("empPosition");
		
		var role = document.getElementById("role");
		var empOrg = document.getElementById("empOrg");
		var fieldId = document.getElementById("fieldId2");
		var currentlyWorking = document.getElementById("currentlyWorking");
		var roleStartMonth = document.getElementById("roleStartMonth");
		var roleStartYear = document.getElementById("roleStartYear");	
		var roleEndMonth = document.getElementById("roleEndMonth");
		var roleEndYear = document.getElementById("roleEndYear");
		var empRoleTypeId=document.getElementsByName("empRoleTypeId");
		var amount=document.getElementById("amount");
		var reasonForLea = $('#reasonForLeadiv').find(".jqte_editor").text().trim();
		
		var cityEmp = document.getElementById("cityEmp");
		var stateOfOrg = document.getElementById("stateOfOrg");
		//shriram///
		if(document.getElementById("districtIdForDSPQ").value==3904493 )
		{
		var compTelephone = $("#compTelephone").val().trim();
		var compSupervisor = $("#compSupervisor").val().trim();
		var validTelephone=false;
		var validCompSupervisor=true;
		if(compTelephone!="")
			validTelephone=isNaN(compTelephone);
		}
		//////////////
		
		
		
		
		
		var amountFlag=false;
		if(amount!=""){
			amountFlag=isNaN(amount.value);
		}
		
		//if(document.getElementById("districtIdForDSPQ").value==4218990 || document.getElementById("districtIdForDSPQ").value==3904380 || document.getElementById("districtIdForDSPQ").value==1201470 || document.getElementById("districtIdForDSPQ").value==804800 || document.getElementById("districtIdForDSPQ").value==3703120 || document.getElementById("districtIdForDSPQ").value==3700690 || document.getElementById("districtIdForDSPQ").value==3702640 || $("#headQuaterIdForDspq").val()==2){
			var charCount=trim($('#primaryRespdiv').find(".jqte_editor").text());
			var countPrimaryResp = charCount.trim().length;
			charCount=trim($('#mostSignContdiv').find(".jqte_editor").text());
			var countmostSignCont = charCount.trim().length;
		//}
		
		$('#errordivEmployment').empty();
		setDefColortoErrorMsgToEmployment();
		var amountValid=true;
		
		if(document.getElementById("districtIdForDSPQ").value==804800 && document.getElementById("districtIdForDSPQ").value==3703120 && document.getElementById("districtIdForDSPQ").value==3700690){	
			amountValid=false;
		}
	
		
		
	if($("#empHisPos").is(':visible') && trim(position.value)=="0"  && $("#empPositionOptional").val()=="true")
		{
			
				$('#errordivEmployment').append("&#149; "+resourceJSON.msgposition+"<br>");
			
			if(focs==0)
				$('#empPosition').focus();

			$('#empPosition').css("background-color",txtBgColor);
			cnt++;focs++;
		}
		
		if(trim(role.value)=="")
		{
			
				$('#errordivEmployment').append("&#149; "+resourceJSON.msgTitleDesigRole+"<br>");
			
			if(focs==0)
				$('#role').focus();

			$('#role').css("background-color",txtBgColor);
			cnt++;focs++;
		}
		//place here////
		
		if($("#ndustry_FieldDspq").is(':visible') && trim(fieldId.value)=="")
		{
			$('#errordivEmployment').append("&#149; "+resourceJSON.msgPleaseselectField+"<br>");
			if(focs==0)
				$('#fieldId2').focus();

			$('#fieldId2').css("background-color",txtBgColor);
			cnt++;focs++;
		}
		
		
		//////////////////////shriram
		if(document.getElementById("districtIdForDSPQ").value==3904493 )
		{
	
		if(trim(empOrg.value)=="")
		{
				$('#errordivEmployment').append("&#149; "+resourceJSON.errmsgCompanyName+"<br>");
			
			if(focs==0)
				$('#empOrg').focus();

			$('#empOrg').css("background-color",txtBgColor);
			cnt++;focs++;
		}
		//shadab
		
		if(trim(cityEmp.value)=="")
		{
			
				$('#errordivEmployment').append("&#149; "+resourceJSON.errmsgCompanyCity+"<br>");
			
			if(focs==0)
				$('#cityEmp').focus();

			$('#cityEmp').css("background-color",txtBgColor);
			cnt++;focs++;
		}
		if(trim(stateOfOrg.value)=="")
		{
			
				$('#errordivEmployment').append("&#149; "+resourceJSON.errmsgCompanyState+"<br>");
			
			if(focs==0)
				$('#stateOfOrg').focus();

			$('#stateOfOrg').css("background-color",txtBgColor);
			cnt++;focs++;
		}
		}
		else
		{		
			if(trim(empOrg.value)==""  && $("#empOrganizationOptional").val()=="true" )
		{
				$('#errordivEmployment').append("&#149; "+resourceJSON.msgorganization+"<br>");
			
			if(focs==0)
				$('#empOrg').focus();

			$('#empOrg').css("background-color",txtBgColor);
			cnt++;focs++;
		}
		//shadab
		
		if(trim(cityEmp.value)==""  && $("#empCityOptional").val()=="true")
		{
			
				$('#errordivEmployment').append("&#149; "+resourceJSON.msgentercity+"<br>");
			
			if(focs==0)
				$('#cityEmp').focus();

			$('#cityEmp').css("background-color",txtBgColor);
			cnt++;focs++;
		}
		if(trim(stateOfOrg.value)==""  && $("#empStateOptional").val()=="true")
		{
			
				$('#errordivEmployment').append("&#149; "+resourceJSON.msgStateOfOrganisation+"<br>");
			
			if(focs==0)
				$('#stateOfOrg').focus();

			$('#stateOfOrg').css("background-color",txtBgColor);
			cnt++;focs++;
		}
		}
///////////////shriram//////////////////////
		//shriram/////////////////////////////////////
		if(document.getElementById("districtIdForDSPQ").value==3904493 )
		{
		$('#compTelephone').val( compTelephone.replace(/^\s\s*/, '').replace(/\s\s*$/, ''));
		if(compTelephone=="" )
		{ 
			$('#errordivEmployment').append("&#149; "+resourceJSON.msgCompanyTelephone+"<br>");
			if(focs==0)
				$('#compTelephone').focus();

			$('#compTelephone').css("background-color",txtBgColor);
			cnt++;focs++;
		}

	
	
	/*if(validTelephone)
	{		
		
		$('#errordivEmployment').append("&#149; "+resourceJSON.msgNumericTelephone+"<br>");
		if(focs==0)
			$('#compTelephone').focus();

		$('#compTelephone').css("background-color",txtBgColor);
		cnt++;focs++;
	}*/
	
	////////////////////////Shriram Company Supervisor//////////
	$('#compSupervisor').val( compSupervisor.replace(/^\s\s*/, '').replace(/\s\s*$/, ''));
	if(compSupervisor=="" )
	{ 
	
		$('#errordivEmployment').append("&#149; "+resourceJSON.msgCompanySupervisor+"<br>");
		if(focs==0)
			$('#compSupervisor').focus();

		$('#compSupervisor').css("background-color",txtBgColor);
		cnt++;focs++;
	}

		}
///////////////////Ended by shriram//////////////////
		
		
		
		
		
		
		//shadab end
		
		if($("#empDatesOptional").val()=="true")
		if($("#empDateDiv").is(':visible')){
		if(trim(roleStartMonth.value)=="0")
		{
			$('#errordivEmployment').append("&#149; "+resourceJSON.msgPleaseselectFromMonth+"<br>");
			if(focs==0)
				$('#roleStartMonth').focus();

			$('#roleStartMonth').css("background-color",txtBgColor);
			cnt++;focs++;
		}
		if(trim(roleStartYear.value)=="0")
		{
			$('#errordivEmployment').append("&#149; "+resourceJSON.msgPleaseselectFromYear+"<br>");
			if(focs==0)
				$('#roleStartYear').focus();

			$('#roleStartYear').css("background-color",txtBgColor);
			cnt++;focs++;
		}
		}
		
		if(currentlyWorking.checked==false)
		{
			if($("#empDatesOptional").val()=="true")
			if($("#empDateDiv").is(':visible')){
			if(trim(roleEndMonth.value)=="0")
			{
				$('#errordivEmployment').append("&#149; "+resourceJSON.msgPleaseselectToMonth+"<br>");
				if(focs==0)
					$('#roleEndMonth').focus();

				$('#roleEndMonth').css("background-color",txtBgColor);
				cnt++;focs++;
			}
			if(trim(roleEndYear.value)=="0")
			{
				$('#errordivEmployment').append("&#149; "+resourceJSON.msgPleaseselectToYear+"<br>");
				if(focs==0)
					$('#roleEndYear').focus();

				$('#roleEndYear').css("background-color",txtBgColor);
				cnt++;focs++;
			}

			if(trim(roleStartMonth.value)!="0" && trim(roleStartYear.value)!="0" && trim(roleEndMonth.value)!="0" && trim(roleEndYear.value)!="0")
			{
				if(trim(roleStartYear.value)>trim(roleEndYear.value))
				{
					$('#errordivEmployment').append("&#149; "+resourceJSON.msgFromgreaterthan+"<br>");
					if(focs==0)
						$('#roleStartMonth').focus();

					$('#roleStartMonth').css("background-color",txtBgColor);
					$('#roleStartYear').css("background-color",txtBgColor);
					$('#roleEndMonth').css("background-color",txtBgColor);
					$('#roleEndYear').css("background-color",txtBgColor);
					cnt++;focs++;
				}
				else if(parseInt(trim(roleStartMonth.value))>parseInt(trim(roleEndMonth.value)) && (trim(roleStartYear.value)==trim(roleEndYear.value)))
				{
					$('#errordivEmployment').append("&#149; "+resourceJSON.msgFromgreaterthan+"<br>");
					if(focs==0)
						$('#roleStartMonth').focus();

					$('#roleStartMonth').css("background-color",txtBgColor);
					$('#roleStartYear').css("background-color",txtBgColor);
					$('#roleEndMonth').css("background-color",txtBgColor);
					$('#roleEndYear').css("background-color",txtBgColor);
					cnt++;focs++;
				}
			}
		}
		}
		var chkRole=true;
		//alert("empRoleTypeId.length "+empRoleTypeId.length);
		for(i=0;i<empRoleTypeId.length;i++)
		{
			if(empRoleTypeId[i].checked==true)
			{
				chkRole=false;
				break;
			}
		}
		if(document.getElementById("districtIdForDSPQ").value==4218990 || document.getElementById("districtIdForDSPQ").value==3904380 || document.getElementById("districtIdForDSPQ").value==3700690 || document.getElementById("districtIdForDSPQ").value==3700112 || ($("#headQuaterIdForDspq").val()==2 && ($("#dspqName").val()=="Option A" || $("#dspqName").val()=="Option B"))){
			chkRole=false;
		}
		if($("#empSecRoleOptional").val()=="false"){
			chkRole=false;
		}
		
		//PUT AMOUNT HERE
		if($("#empSecSalaryOptional").val()=="false"){
			amountValid=false;
		}
		if(amountValid){
			if(amount.value=="" && $("#amount").is(':visible'))
			{
				$('#errordivEmployment').append("&#149; "+resourceJSON.msgEneterAmount+"<br>");
				if(focs==0)
					$('#amount').focus();
	
				$('#amount').css("background-color",txtBgColor);
				cnt++;focs++;
			}
		}
		if(amountFlag)
		{
			$('#errordivEmployment').append("&#149; "+resourceJSON.msgnumericamount+"<br>");
			if(focs==0)
				$('#amount').focus();

			$('#amount').css("background-color",txtBgColor);
			cnt++;focs++;
		}
		
		if(chkRole)
		{
			$('#errordivEmployment').append("&#149; "+resourceJSON.msgtypeRole+"<br>");
			if(focs==0)
				$('#empRoleTypeId').focus();		
			cnt++;focs++;
		}
		

				if($("#empSecPrirOptional").val()=="2"){
			if(countPrimaryResp==0)
			{
				$('#errordivEmployment').append("&#149; "+resourceJSON.msgprimaryresponsibilities+"<br>");
				if(focs==0)
					$('#primaryRespdiv').find(".jqte_editor").focus();
				$('#primaryRespdiv').find(".jqte_editor").css("background-color", "#F5E7E1");
				cnt++;focs++;
			}			
		}
		if(countPrimaryResp>5000)
		{
			$('#errordivEmployment').append("&#149; "+resourceJSON.msgMAxLenPrimaryResponsibilities+"<br>");
			if(focs==0)
				$('#primaryRespdiv').find(".jqte_editor").focus();
			$('#primaryRespdiv').find(".jqte_editor").css("background-color", "#F5E7E1");
			cnt++;focs++;
		}
		if($("#empSecMscrOptional").val()=="2"){
			 if(countmostSignCont==0)
				{
					$('#errordivEmployment').append("&#149; "+resourceJSON.msgmostsignificantcontributions+"<br>");
					if(focs==0)
						$('#mostSignContdiv').find(".jqte_editor").focus();
					$('#mostSignContdiv').find(".jqte_editor").css("background-color", "#F5E7E1");
					cnt++;focs++;
				}			
		}
		 if(countmostSignCont>5000)
		 {
		 	$('#errordivEmployment').append("&#149; "+resourceJSON.msgMAxLenSignificantContributions+"<br>");
		 	if(focs==0)
		 		$('#mostSignContdiv').find(".jqte_editor").focus();
		 	$('#mostSignContdiv').find(".jqte_editor").css("background-color", "#F5E7E1");
		 	cnt++;focs++;
		 }

		
		if(document.getElementById("districtIdForDSPQ").value==4218990){
			var nonteacherFlag=$("#isnontj").val();
			
			if($('#isSchoolSupportPhiladelphia').val()!=""){}
			else if(nonteacherFlag=="" || nonteacherFlag!="true"){
				if(countPrimaryResp==0)
				{
					$('#errordivEmployment').append("&#149; "+resourceJSON.msgprimaryresponsibilities+"<br>");
					if(focs==0)
						$('#primaryRespdiv').find(".jqte_editor").focus();
					$('#primaryRespdiv').find(".jqte_editor").css("background-color", "#F5E7E1");
					cnt++;focs++;
				}
				else if(countPrimaryResp>5000)
				{
					$('#errordivEmployment').append("&#149; "+resourceJSON.msgMAxLenPrimaryResponsibilities+"<br>");
					if(focs==0)
						$('#primaryRespdiv').find(".jqte_editor").focus();
					$('#primaryRespdiv').find(".jqte_editor").css("background-color", "#F5E7E1");
					cnt++;focs++;
				}
			}			
			else{
				if(countPrimaryResp>5000)
				{
					$('#errordivEmployment').append("&#149; "+resourceJSON.msgMAxLenPrimaryResponsibilities+"<br>");
					if(focs==0)
						$('#primaryRespdiv').find(".jqte_editor").focus();
					$('#primaryRespdiv').find(".jqte_editor").css("background-color", "#F5E7E1");
					cnt++;focs++;
				}
			}

			if(countmostSignCont>5000)
			{
				$('#errordivEmployment').append("&#149; "+resourceJSON.msgMAxLenSignificantContributions+"<br>");
				if(focs==0)
					$('#mostSignContdiv').find(".jqte_editor").focus();
				$('#mostSignContdiv').find(".jqte_editor").css("background-color", "#F5E7E1");
				cnt++;focs++;
			}
		}
		
		if(document.getElementById("districtIdForDSPQ").value==3703120 || document.getElementById("districtIdForDSPQ").value==3700112 || document.getElementById("districtIdForDSPQ").value==3702040 || document.getElementById("districtIdForDSPQ").value==3702640 || ($("#headQuaterIdForDspq").val()==2 && ($("#dspqName").val()!="Option A" && $("#dspqName").val()!="Option B"))){
			if(countPrimaryResp==0)
			{
				$('#errordivEmployment').append("&#149; Please provide primary responsibilities in this role<br>");
				if(focs==0)
					$('#primaryRespdiv').find(".jqte_editor").focus();
				$('#primaryRespdiv').find(".jqte_editor").css("background-color", "#F5E7E1");
				cnt++;focs++;
			}
		}

			 if(countmostSignCont==0 && (document.getElementById("districtIdForDSPQ").value==3700112 || document.getElementById("districtIdForDSPQ").value==3702040 || document.getElementById("districtIdForDSPQ").value==3702640))
			{
				$('#errordivEmployment').append("&#149; Please provide most significant contributions in this role<br>");
				if(focs==0)
					$('#mostSignContdiv').find(".jqte_editor").focus();
				$('#mostSignContdiv').find(".jqte_editor").css("background-color", "#F5E7E1");
				cnt++;focs++;
			}
		if(document.getElementById("districtIdForDSPQ").value==804800){
			if(countPrimaryResp==0)
			{
				$('#errordivEmployment').append("&#149; "+resourceJSON.msgprimaryresponsibilities+"<br>");
				if(focs==0)
					$('#primaryRespdiv').find(".jqte_editor").focus();
				$('#primaryRespdiv').find(".jqte_editor").css("background-color", "#F5E7E1");
				cnt++;focs++;
			}else if(countPrimaryResp>5000)
			{
				$('#errordivEmployment').append("&#149; "+resourceJSON.msgMAxLenPrimaryResponsibilities+"<br>");
				if(focs==0)
					$('#primaryRespdiv').find(".jqte_editor").focus();
				$('#primaryRespdiv').find(".jqte_editor").css("background-color", "#F5E7E1");
				cnt++;focs++;
			}
			
			if(countmostSignCont==0 && $('#jobcategoryDsp').val().trim().indexOf("Hourly") ==-1)
			{
				$('#errordivEmployment').append("&#149; "+resourceJSON.msgmostsignificantcontributions+"<br>");
				if(focs==0)
					$('#mostSignContdiv').find(".jqte_editor").focus();
				$('#mostSignContdiv').find(".jqte_editor").css("background-color", "#F5E7E1");
				cnt++;focs++;
			}else if(countmostSignCont>5000 && $('#jobcategoryDsp').val().trim().indexOf("Hourly")==-1)
			{
				$('#errordivEmployment').append("&#149; "+resourceJSON.msgMAxLenSignificantContributions+"<br>");
				if(focs==0)
					$('#mostSignContdiv').find(".jqte_editor").focus();
				$('#mostSignContdiv').find(".jqte_editor").css("background-color", "#F5E7E1");
				cnt++;focs++;
			}
		}
		if(document.getElementById("districtIdForDSPQ").value==1201470){
			/*if(countPrimaryResp==0)
			{
				$('#errordivEmployment').append("&#149; Please provide primary responsibilities in this role<br>");
				if(focs==0)
					$('#primaryRespdiv').find(".jqte_editor").focus();
				$('#primaryRespdiv').find(".jqte_editor").css("background-color", "#F5E7E1");
				cnt++;focs++;
			}
			else */if(countPrimaryResp>5000)
			{
				$('#errordivEmployment').append("&#149; "+resourceJSON.msgMAxLenPrimaryResponsibilities+"<br>");
				if(focs==0)
					$('#primaryRespdiv').find(".jqte_editor").focus();
				$('#primaryRespdiv').find(".jqte_editor").css("background-color", "#F5E7E1");
				cnt++;focs++;
			}
			
			/*if(countPrimaryResp==0)
			{
				$('#errordivEmployment').append("&#149; Please provide most significant contributions in this role<br>");
				if(focs==0)
					$('#mostSignContdiv').find(".jqte_editor").focus();
				$('#mostSignContdiv').find(".jqte_editor").css("background-color", "#F5E7E1");
				cnt++;focs++;
			}else */if(countmostSignCont>5000)
			{
				$('#errordivEmployment').append("&#149; "+resourceJSON.msgMAxLenSignificantContributions+"<br>");
				if(focs==0)
					$('#mostSignContdiv').find(".jqte_editor").focus();
				$('#mostSignContdiv').find(".jqte_editor").css("background-color", "#F5E7E1");
				cnt++;focs++;
			}
		}
		if($(".reasonForLeavdiv").is(':visible') && roleEndYear.value!=0 && roleEndMonth.value!=0){
			if(reasonForLea.trim().length==0)
			{
				$('#errordivEmployment').append("&#149; "+resourceJSON.msgReasonforleaving+"<br>");
				if(focs==0)
					$('#reasonForLeadiv').find(".jqte_editor").focus();
					$('#reasonForLeadiv').find(".jqte_editor").css("background-color", "#F5E7E1");
				cnt++;focs++;
			}
		}
		
		if(reasonForLea.trim().length>5000)
		{
			$('#errordivEmployment').append("&#149; "+resourceJSON.msgmaximumlengthfiled+"<br>");
			if(focs==0)
				$('#reasonForLeadiv').find(".jqte_editor").focus();
			$('#reasonForLeadiv').find(".jqte_editor").css("background-color", "#F5E7E1");
			cnt++;focs++;
		}
	}  // nowork
		else
		{
			cnt=0;
		}
		
		if(cnt!=0)		
		{
			updateReturnThreadCount("emp");
			$('#errordivEmployment').show();
			return false;
		}
		else
		{
			var roleType;
			//alert("empRoleTypeId "+empRoleTypeId);
			
			//if(empRoleTypeId!=null)
			//{
			
			//alert("idonothaveworkexp "+idonothaveworkexp)
			
			if(idonothaveworkexp==false)
			{
				for(i=0;i<empRoleTypeId.length;i++)
				{
					if(empRoleTypeId[i].checked==true)
					{
						roleType=empRoleTypeId[i].value;
						break;
					}
				}
			}
				
			//}
			
			//alert("roleType "+roleType);
			
			var roleTypeObj=null;
			
			if(roleType==1 || roleType==2 || roleType==3){
				roleTypeObj = {empRoleTypeId:roleType};
			}else{
				roleTypeObj=null;
			}
			
			var fieldObj = {fieldId:dwr.util.getValue("fieldId2")};
			
			var employment = {position:null,roleId:null,fieldMaster:fieldObj,empRoleTypeMaster:roleTypeObj,role:null, roleStartMonth:null, roleStartYear:null, roleEndMonth:null, roleEndYear:null,amount:null,primaryResp:null,mostSignCont:null,reasonForLeaving:null,compSupervisor:null,compTelephone:null};
			dwr.engine.beginBatch();
			
			dwr.util.getValues(employment);
			employment.currentlyWorking=document.getElementById("currentlyWorking").checked;
			employment.noWorkExp=idonothaveworkexp;
			employment.reasonForLeaving=reasonForLea;
			if(document.getElementById("districtIdForDSPQ").value==3904493 )
			{
			employment.compTelephone=compTelephone;
			employment.compSupervisor=compSupervisor;
			}
			if(idonothaveworkexp==false)
			{
				employment.position=position.value;
				employment.organization=empOrg.value;
				employment.city=cityEmp.value;
				employment.state=stateOfOrg.value;
			}
			else
			{
				employment.position=null;
				employment.organization=null;
				employment.city=null;
				employment.state=null;
			}
			
			if(dwr.util.getValue("fieldId2")==""){
				employment.fieldMaster=null;
			}
			
			
			//employeerName.value
			
			PFExperiences.saveOrUpdateEmployment(employment,{ 
				async: false,
				errorHandler:handleError,
				callback: function(data)
				{
					hideEmploymentForm();
					getPFEmploymentDataGrid();
					if(sbtsource==1)
					{
						saveAllDSPQ();
						updateReturnThreadCount("emp");
						validatePortfolioErrorMessageAndGridData('level2');
					}
				//$("#hrefCertifications").addClass("stepactive");
				//hideForm();
				}
			});
			dwr.engine.endBatch();
			return true;
		}
	}
}


function showEditFormEmployment(id)
{
	PFExperiences.showEditFormEmployment(id,
		{ 
			async: false,
			errorHandler:handleError,
			callback: function(data)
			{
					showEmploymentForm();
					document.getElementById("roleId").value=id;	
					if(data.noWorkExp==null || data.noWorkExp==0)
					{
						document.getElementById("empPosition").value=data.position;
						  
						document.getElementById("role").value=data.role;
						document.getElementById("fieldId2").value=data.fieldMaster.fieldId;
						document.getElementById("amount").value=data.amount;
						document.getElementById("empOrg").value=data.organization;
						
						document.getElementById("cityEmp").value=data.city;
						document.getElementById("stateOfOrg").value=data.state;
						//shriram
						document.getElementById("compTelephone").value=data.compTelephone;
						document.getElementById("compSupervisor").value=data.compSupervisor;
						//alert(data.organization);
						if(data.currentlyWorking)
						{
							document.getElementById("currentlyWorking").click();
							document.getElementById("roleStartMonth").value=data.roleStartMonth;
							document.getElementById("roleStartYear").value=data.roleStartYear;
						}
						else
						{
							document.getElementById("currentlyWorking").checked=false;
							document.getElementById("divToMonth").style.display="block";
							document.getElementById("divToYear").style.display="block";
							document.getElementById("roleStartMonth").value=data.roleStartMonth;
							document.getElementById("roleStartYear").value=data.roleStartYear;
							
				
							document.getElementById("roleEndMonth").value=data.roleEndMonth;
							document.getElementById("roleEndYear").value=data.roleEndYear;
						}
						
						try {
							
							/*var empRoleTypeId=document.getElementsByName("empRoleTypeId");

							if(data.empRoleTypeMaster.empRoleTypeId!=null)
							for(i=0;i<empRoleTypeId.length;i++)
							{
								alert("3 empRoleTypeId[i].value "+empRoleTypeId[i].value +" data.empRoleTypeMaster.empRoleTypeId "+data.empRoleTypeMaster.empRoleTypeId);
								if(empRoleTypeId[i].value==data.empRoleTypeMaster.empRoleTypeId)
								{
									alert("4");
									empRoleTypeId[i].checked=true
								}
								
					
							}*/
							document.getElementById("empRoleTypeId_"+data.empRoleTypeMaster.empRoleTypeId).checked = true;
							//$("#empRoleTypeId_"+data.empRoleTypeMaster.empRoleTypeId).prop("checked", true);
							
							
						} catch (e) {
							// TODO: handle exception
						}
						
						//document.getElementById("primaryResp").value=data.primaryResp;
						//document.getElementById("mostSignCont").value=data.mostSignCont;
						$('#primaryRespdiv').find(".jqte_editor").html(data.primaryResp);
						$('#mostSignContdiv').find(".jqte_editor").html(data.mostSignCont);
						$('#reasonForLeadiv').find(".jqte_editor").html(data.reasonForLeaving);
						/*document.getElementById("certificateNameMaster").value=data.certificateNameMaster.certNameId;
							document.getElementById("certName").value=data.certificateNameMaster.certName;
						 */
						return false;
				
						//document.getElementById("stateMaster").value=data.stateMaster.stateId;
						//document.getElementById("").value="";
						//document.getElementById("").value="";
						//document.getElementById("").value="";
						//document.getElementById("").value="";
						//document.getElementById("").value="";
						
					}
					else
					{
						document.getElementById("idonothaveworkexp").checked=true;
						
						$('#primaryRespdiv').find(".jqte_editor").html("");
						$('#mostSignContdiv').find(".jqte_editor").html("");
						$('#reasonForLeadiv').find(".jqte_editor").html("");
						
						hideworkexp();
					}
					
					}
			});

	return false;

}

function deleteEmployment(id)
{	
	document.getElementById("empHistoryID").value=id;
	$('#deleteEmpHistory').modal('show');
}

function deleteEmploymentDSPQ()
{	
	$('#deleteEmpHistory').modal('hide');
	var id=document.getElementById("empHistoryID").value;
	PFExperiences.deleteEmployment(id, { 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			getPFEmploymentDataGrid();
			hideEmploymentForm();
			return false;
		}});
}


function hideToExp()
{
	var chk = document.getElementById("currentlyWorking").checked;
	if(chk)
	{
		document.getElementById("divToMonth").style.display="none";
		document.getElementById("divToYear").style.display="none";
	}
	else
	{
		document.getElementById("divToMonth").style.display="block";
		document.getElementById("divToYear").style.display="block";
	}
}

function hideworkexp()
{
	var idonothaveworkexp=false;
	try {
		idonothaveworkexp=document.getElementById("idonothaveworkexp").checked;	
	} catch (e) {
		// TODO: handle exception
	}
	if(idonothaveworkexp)
	{
		$(".hideWorkExp").prop('disabled', true);
		$(".hideWorkExp").val("");
		$(".hideWorkExpOPt").prop('disabled', true);
		
		
		//$(".avoid-clicks").prop('pointer-events', none);
		//$(".avoid-clicks").val("");
	}
	else
	{
		$(".hideWorkExp").prop('disabled', false);
		$(".hideWorkExpOPt").prop('disabled', false);
		//$(".hideWorkExp").val("");
		//document.getElementById("primaryResp").disabled = disabled;
	}
}
//
function showEmploymentForm()
{
	$("#empDateDiv").show();
	document.getElementById('empHisAnnSal').style.display="block";	
	if($("#districtIdForDSPQ").length > 0 && (document.getElementById("districtIdForDSPQ").value==7800036 || document.getElementById("districtIdForDSPQ").value==1201470 || document.getElementById("districtIdForDSPQ").value==3904493)){
		document.getElementById('empHisAnnSal').style.display="none";
	}
	$(".pritr").hide();
	$(".mscitr").hide();
	//$(".reasonForLeavdiv").hide();
	if($("#empPositionOptional").val()=="true")
		{
			$("#empPostionAstrik").show();
		}
		if($("#empOrganizationOptional").val()=="true")
		{
			$("#empOrganizationAstrik").show();
		}
		if($("#empCityOptional").val()=="true")
		{
			$("#empCityAstrik").show();
		}
		if($("#empStateOptional").val()=="true")
		{
			$("#empStateOrganizationAstrik").show();
		}

	if($("#empSecReasonForLeavOptional").val()=="false"){
			$(".reasonForLeavdiv").hide();
	}
	if($("#empSecPrirOptional").val()=="1"){
		$(".pritr").show();		
		$(".pritreq").hide();		
	}else if($("#empSecPrirOptional").val()=="2"){
		$(".pritr").show();		
		$(".pritreq").show();
	}
	
	if($("#empSecMscrOptional").val()=="1"){
		$(".mscitr").show();		
		$(".mscitrReq").hide();	
	}else if($("#empSecMscrOptional").val()=="2"){
		$(".mscitr").show();		
		$(".mscitrReq").show();
	}
	
	if($("#empSecRoleOptional").val()=="false"){
		$(".tORR").hide();
	}
	//empSecMscrOptional

	if($("#districtIdForDSPQ").length > 0 && (document.getElementById("districtIdForDSPQ").value==4218990 || document.getElementById("districtIdForDSPQ").value==1201470 || document.getElementById("districtIdForDSPQ").value==804800 || document.getElementById("districtIdForDSPQ").value==3702040 || document.getElementById("districtIdForDSPQ").value==3702640)){
		$(".pritr").show();
		$(".mscitr").show();
		if(document.getElementById("districtIdForDSPQ").value==1201470){			
			$(".mscitrReq").show();
			$(".pritreq").hide();
			$(".mscitrReq").hide();
		}		
		
	}else if($("#districtIdForDSPQ").length > 0 && document.getElementById("districtIdForDSPQ").value==3904380){
		$(".pritr").show();
		$(".mscitr").show();
		$(".tORR").hide();
	}
	if(document.getElementById("districtIdForDSPQ").value==4218990 && $('#isPrinciplePhiladelphia').val()=="1"){
		$("#empDateDiv").hide();
		document.getElementById('empHisAnnSal').style.display="none";
	}
	$(".expsalaryInwork").hide();
		if(document.getElementById("districtIdForDSPQ").value==7800040 || $("#headQuaterIdForDspq").val()==2 && ($("#dspqName").val()=="Option C" || $("#dspqName").val()=="Option D")){
		$(".expsalaryInwork").show();
	}
	if(document.getElementById("districtIdForDSPQ").value==3702340){
		$(".expsalaryInwork").hide();
	}
	if(document.getElementById("districtIdForDSPQ").value==3703120){
		$(".pritr").show();
		$(".mscitr").show();
	}
	
	if(document.getElementById("districtIdForDSPQ").value==3700690){
		$(".pritr").show();
		$(".mscitr").show();
		$(".tORR").hide();
		$(".pritreq").hide();
	}
		if(document.getElementById("districtIdForDSPQ").value==3700112){
		$(".pritr").show();
		$(".mscitr").show();	
		$(".pritreq").show();
		$(".mscitrReq").show();
		$(".empHisTypRol").hide();	
	}
	
		$("#compTeleSup").hide();
		if(document.getElementById("districtIdForDSPQ").value==3904493)
		{	$(".empHisTypRol").hide();
			$("#empHisPos").hide();
			$("#compTeleSup").show();
			$("#labelnameOforg").html("Company Name<span class='required'>*</span>");
			$("#labelnameofcity").html("Company City<span class='required'>*</span>");
			$("#labelnameofstate").html("Company State<span class='required'>*</span>");
		}

	//ndustry_FieldDspq
	if(document.getElementById("districtIdForDSPQ").value==3703120 && $('#jobcategoryDsp').val().trim().indexOf("Bus Driver") !=-1){
		//$("#ndustry_FieldDspq").hide();
		$(".mscitr").hide();
	}
	/*$("#teachingPosChk").hide();
	if(document.getElementById("districtIdForDSPQ").value==1201470){
		$("#teachingPosChk").show();
	}*/
	if(document.getElementById("districtIdForDSPQ").value==804800){		
		var tempStr=$("#empRoleDspq").html();
		var chngdd=tempStr.replace("General Member", "Employee");
		$("#empRoleDspq").html(chngdd);
					
		if($('#jobcategoryDsp').val().trim().indexOf("Licensed") !=-1){
			$("#empHisPos").show();
		}else{
			$("#empHisPos").hide();
		}
	}	
	/*if(document.getElementById("districtIdForDSPQ").value==3904493){
		$("#empHisPos").hide();
	}*/
	$("#divToMonth .required").show();
	if($("#headQuaterIdForDspq").val()==2 && ($("#dspqName").val()=="Option A" || $("#dspqName").val()=="Option B")){
		$("#divToMonth .required").hide();
		$(".pritr").show();
		$(".mscitr").show();
		$(".reasonForLeavdiv").hide();
		$(".pritreq").hide();$(".tORR").hide();

if(document.getElementById("districtIdForDSPQ").value==3702340)
			$("#pritreq").show();

		if($("#dspqName").val()=="Option B"){
			$("#divToMonth .required").show();
		}
	}else if($("#headQuaterIdForDspq").val()==2 && ($("#dspqName").val()=="Option C" || $("#dspqName").val()=="Option D")){
		$(".pritr").show();
		$(".mscitr").show();
		$(".reasonForLeavdiv").hide();
		$(".pritreq").show();$(".tORR").show();$(".mscitrReq").show();
		if(document.getElementById("districtIdForDSPQ").value==3702340)
		{
			$(".tORR").hide();
			$(".mscitrReq").hide();
		}
	}
	if($("#empDatesOptional").val()=="true"){
		$("#divToMonth .required").show();
	}
	document.getElementById("empPosition").value="0"	
	document.getElementById("cityEmp").value="";
	document.getElementById("stateOfOrg").value="";
	document.getElementById("role").value="";
	document.getElementById("fieldId2").value="";
	document.getElementById("empOrg").value="";
	document.getElementById("roleStartMonth").value="0";
	document.getElementById("roleStartYear").value="0";
	document.getElementById("roleEndMonth").value="0";
	document.getElementById("roleEndYear").value="0";
	document.getElementById("amount").value="";
	//document.getElementById("primaryRespdiv").value="";
	//document.getElementById("mostSignContdiv").value="";
	//shriram
	if(document.getElementById("districtIdForDSPQ").value==3904493 )
	{
	document.getElementById("compTelephone").value="";
	document.getElementById("compSupervisor").value="";
	$("#compTelephone").css("background-color", "#fff");
	$("#compSupervisor").css("background-color", "#fff");
	}
	////////
	var empRoleTypeId=document.getElementsByName("empRoleTypeId");
	for(i=0;i<empRoleTypeId.length;i++)
	{
			empRoleTypeId[i].checked=false
	}

	document.getElementById("roleId").value="";	
	document.getElementById("divEmployment").style.display="block";
	document.getElementById("role").focus();
	document.getElementById("currentlyWorking").checked = false;
	document.getElementById("divToMonth").style.display="block";
	document.getElementById("divToYear").style.display="block";
	setDefColortoErrorMsgToEmployment();	
	$('#primaryRespdiv').find(".jqte_editor").html("");
	$('#mostSignContdiv').find(".jqte_editor").html("");
	$('#reasonForLeadiv').find(".jqte_editor").html("");
	$('#errordivEmployment').empty();
	
	document.getElementById("idonothaveworkexp").checked=false;
	hideworkexp();
	
	return false;
}

function hideEmploymentForm()
{
	//document.getElementById("frmEmployment").reset();
	document.getElementById("roleId").value="";	
	document.getElementById("divEmployment").style.display="none";
	window.location.hash = '#addEmploymentIdDSPQ';
	return false;
}

function setDefColortoErrorMsgToEmployment()
{
	$('#role').css("background-color","");
	$('#empPosition').css("background-color", "")
	$('#fieldId2').css("background-color","");
	$('#empOrg').css("background-color","");
	$('#roleStartMonth').css("background-color","");
	$('#roleStartYear').css("background-color","");
	$('#roleEndMonth').css("background-color","");
	$('#roleEndYear').css("background-color","");
//	$('#currencyId').css("background-color","");
	$('#amount').css("background-color","");	
	$('#primaryRespdiv').find(".jqte_editor").css("background-color","");
	$('#mostSignContdiv').find(".jqte_editor").css("background-color","");
	$('#reasonForLeadiv').find(".jqte_editor").css("background-color","");
	$('#compTelephone').css("background-color","");
	$('#compSupervisor').css("background-color","");
	
	//shadab
	
	$('#cityEmp').css("background-color","");
	$('#stateOfOrg').css("background-color","");
}

/* @End
 * @Ashish Kumar
 * @Description :: Get Employment Details
 * */

function getCityListByStateSetForDSPQ(data1)
{
	$('#loadingDiv').show();	
	var stateIdForDSPQ = document.getElementById("stateIdForDSPQ");
	var res;
	createXMLHttpRequest();
	queryString = "findcityhtmlbystate.do?stateId="+stateIdForDSPQ.value+"&dt="+new Date().getTime();
	xmlHttp.open("POST", queryString, false);
	xmlHttp.onreadystatechange = function()
	{
		if(xmlHttp.readyState == 4)
		{
			if(xmlHttp.status==200)	
			{
				
				while (document.getElementById("cityIdForDSPQ").childNodes.length >= 1 )
				{
				    document.getElementById("cityIdForDSPQ").removeChild(document.getElementById("cityIdForDSPQ").firstChild);       
				}
				var optAr = xmlHttp.responseText.split("##");
				var newOption;
				
				newOption = document.createElement('option');
			    newOption.text=resourceJSON.msgSelectCity;
			    newOption.value="";
			    newOption.id="slcty";
			   	document.getElementById('cityIdForDSPQ').options.add(newOption);
				for(var i=0;i<optAr.length-1;i++)
				{
				    newOption = document.createElement('option');
				    newOption.id="ct"+optAr[i].split("$$")[1]
				    newOption.value=optAr[i].split("$$")[1];
				    newOption.text=optAr[i].split("$$")[2];
				    document.getElementById('cityIdForDSPQ').options.add(newOption);
				}
				$('#loadingDiv').hide();
				
				if(data1.cityId!=null)
				{
					try{document.getElementById("ct"+data1.cityId.cityId).selected=true;}catch(e){}
					//document.getElementById("ct"+data1.cityId.cityId).selected=true;
				}
				else
				{
					//try{document.getElementById("slct").selected=true;}catch(e){}
					if(document.getElementById("slcty"))
						document.getElementById("slcty").selected=true;
				}
				
			}else{
				alert(resourceJSON.msgServerErr);
			}
		}
	}
	xmlHttp.send(null);	
}

function getCityListByStateSetForDSPQPr(data1)
{
	$('#loadingDiv').show();	
	var stateIdForDSPQ = document.getElementById("stateIdForDSPQPr");
	var res;
	createXMLHttpRequest();
	queryString = "findcityhtmlbystate.do?stateId="+stateIdForDSPQ.value+"&dt="+new Date().getTime();
	xmlHttp.open("POST", queryString, false);
	xmlHttp.onreadystatechange = function()
	{
		if(xmlHttp.readyState == 4)
		{
			if(xmlHttp.status==200)	
			{
				
				while (document.getElementById("cityIdForDSPQPr").childNodes.length >= 1 )
				{
				    document.getElementById("cityIdForDSPQPr").removeChild(document.getElementById("cityIdForDSPQPr").firstChild);       
				}
				var optAr = xmlHttp.responseText.split("##");
				var newOption;
				
				newOption = document.createElement('option');
			    newOption.text="Select City";
			    newOption.value="";
			    newOption.id="slctyPr";
			   	document.getElementById('cityIdForDSPQPr').options.add(newOption);
				for(var i=0;i<optAr.length-1;i++)
				{
				    newOption = document.createElement('option');
				    newOption.id="ctPr"+optAr[i].split("$$")[1]
				    newOption.value=optAr[i].split("$$")[1];
				    newOption.text=optAr[i].split("$$")[2];
				    document.getElementById('cityIdForDSPQPr').options.add(newOption);
				}
				$('#loadingDiv').hide();
				
				if(data1.cityId!=null)
				{
					try{document.getElementById("ctPr"+data1.cityId.cityId).selected=true;}catch(e){}
					//document.getElementById("ct"+data1.cityId.cityId).selected=true;
				}
				else
				{
					//try{document.getElementById("slct").selected=true;}catch(e){}
					if(document.getElementById("slctyPr"))
						document.getElementById("slctyPr").selected=true;
				}
				
			}else{
				alert("server Error");
			}
		}
	}
	xmlHttp.send(null);	
}

function getDistrictSpecificTFAValues(districtId){
	PFExperiences.getDistrictSpecificTFAValues(districtId,
			{ 
				async: false,
				errorHandler:handleError,
				callback: function(data)
				{
				//alert(data);
					$("#tfaDistOptData").html(data);
				}
			});	
}

function setDistrictSpecificTFAValues(){
	var multiSelectOptArray="";
	 var inputs = $('.tfaOptId:checked'); 
	 for (var j = 0; j < inputs.length; j++) {
	        if (inputs[j].type === 'checkbox') {
	        	if(inputs[j].checked){
	        		multiSelectOptArray+=	inputs[j].value+"|";
	            }
	        }
	}	
	var othText = trim($('#tfaOthText').find(".jqte_editor").text());
	 var districtMaster = {districtId:dwr.util.getValue("districtIdForDSPQ")};
	PFExperiences.setDistrictSpecificTFAValues(districtMaster,multiSelectOptArray,othText,
			{ 
				async: false,
				errorHandler:handleError,
				callback: function(data)
				{
				//alert(data);
					//$("#tfaDistOptData").html(data);
				}
			});	

}

function setDistrictSpecificVeteranValues(){
	var multiSelectOptArray="";
	 var inputs = $('#veteranOptS:checked').val();
	 var input2 =$('#veteranPreference1:checked').val();
	 var input3 =$('#veteranPreference2:checked').val();
	 
	var othText = "Veteran Value Oscelo";
	var districtMaster = {districtId:dwr.util.getValue("districtIdForDSPQ")};
	var finalInputs= inputs+"#"+input2+"#"+input3;
	PFExperiences.setDistrictSpecificTFAValues(districtMaster,finalInputs,othText,
			{ 
				async: false,
				errorHandler:handleError,
				callback: function(data)
				{}
			});	

}

function showdistrictspeciFicVeteran(){		
	$("#vetranOptionDiv").hide();
	if(document.getElementById("districtIdForDSPQ").value==1200390 && $("#vt1").is(':checked')){
		$("#vetranOptionDiv").html("Please <a href='javascript:void(0);' onclick=\"window.open('https://platform.teachermatch.org/pdf/6888VeteranPreference.pdf','_blank', 'width = 1050px, height = 600px');\" download>download</a>, complete and submit this form.");
		$("#vetranOptionDiv").show();

		//$("#message2show").html("Please <a href='javascript:void(0);' onclick=\"window.open('https://platform.teachermatch.org/pdf/6888VeteranPreference.pdf','_blank', 'width = 1050px, height = 600px');\" download>download</a>, complete and submit this form.");
		//$('#myModal2').modal('show');
	}else if(document.getElementById("districtIdForDSPQ").value==1201470 && $("#vt1").is(':checked')){
	var districtMaster = {districtId:dwr.util.getValue("districtIdForDSPQ")};
	
	PFExperiences.getTeacherVeteran(districtMaster,
			{ 
				async: false,
				errorHandler:handleError,
				callback: function(data)
				{					
					var res = data.split("#");
					$("input[id=veteranOptS][value='"+res[0]+"']").prop("checked",true);
					$("input[id=veteranPreference1][value='"+res[1]+"']").prop("checked",true);
					$("input[id=veteranPreference2][value='"+res[2]+"']").prop("checked",true);
					$("#vetranOptionDiv").show();
				}
			});	
	
}
}
function insertOrUpdateStdTchrExp(){
	$('#errordivstdTch').empty();
	$('#toStdTch').css("background-color","");
	$('#fromStdTch').css("background-color","");
	$('#subjectStdTch').css("background-color","");
	$('#schoolNameStdTch').css("background-color","");
	$("#compTelephone").css("background-color", "");
	$("#compSupervisor").css("background-color", "");
	var schoolNamestdTch  = trim(document.getElementById('schoolNameStdTch').value);
	var subjectStdTch 	  = trim(document.getElementById('subjectStdTch').value);
	var pkOfferedStdTchr=0;
	var kgOfferedStdTchr=0;
	var g01OfferedStdTchr=0;
	var g02OfferedStdTchr=0;
	var g03OfferedStdTchr=0;
	var g04OfferedStdTchr=0;
	var g05OfferedStdTchr=0;
	var g06OfferedStdTchr=0;
	var g07OfferedStdTchr=0;
	var g08OfferedStdTchr=0;
	var g09OfferedStdTchr=0;
	var g10OfferedStdTchr=0;
	var g11OfferedStdTchr=0;
	var g12OfferedStdTchr=0;
	
	var cnt=0;
	var focs=0;	

	
	if(document.getElementById('pkOfferedStdTchr').checked==true){
		pkOfferedStdTchr  = true;
	}
	if(document.getElementById('kgOfferedStdTchr').checked==true){
		kgOfferedStdTchr  = true;
	}
	if(document.getElementById('g01OfferedStdTchr').checked==true){
		g01OfferedStdTchr  = true;
	}
	if(document.getElementById('g02OfferedStdTchr').checked==true){
		g02OfferedStdTchr  = true;
	}
	if(document.getElementById('g03OfferedStdTchr').checked==true){
		g03OfferedStdTchr  = true;
	}
	if(document.getElementById('g04OfferedStdTchr').checked==true){
		g04OfferedStdTchr  = true;
	}
	if(document.getElementById('g05OfferedStdTchr').checked==true){
		g05OfferedStdTchr  = true;
	}
	if(document.getElementById('g06OfferedStdTchr').checked==true){
		g06OfferedStdTchr  = true;
	}
	if(document.getElementById('g07OfferedStdTchr').checked==true){
		g07OfferedStdTchr  = true;
	}
	if(document.getElementById('g08OfferedStdTchr').checked==true){
		g08OfferedStdTchr  = true;
	}
	if(document.getElementById('g09OfferedStdTchr').checked==true){
		g09OfferedStdTchr  = true;
	}
	if(document.getElementById('g10OfferedStdTchr').checked==true){
		g10OfferedStdTchr  = true;
	}
	if(document.getElementById('g11OfferedStdTchr').checked==true){
		g11OfferedStdTchr  = true;
	}
	if(document.getElementById('g12OfferedStdTchr').checked==true){
		g12OfferedStdTchr  = true;
	}
	var fromDate 		  = trim(document.getElementById('fromStdTch').value);
	var toDate 		  = trim(document.getElementById('toStdTch').value);
	var stdTchrExpId = trim(document.getElementById('stdTchrExpId').value);
		
	
	if(schoolNamestdTch==""){
		$('#errordivstdTch').append("&#149; "+resourceJSON.msgfillSchoolName+"<br>");
		if(focs==0)
			$('#schoolNameStdTch').focus();

		$('#schoolNameStdTch').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	
	if(subjectStdTch==""){
		$('#errordivstdTch').append("&#149; "+resourceJSON.msgFillSubj+"<br>");
		if(focs==0)
			$('#subjectStdTch').focus();

		$('#subjectStdTch').css("background-color",txtBgColor);
		cnt++;focs++;
		
	}
	
	if(fromDate==""){
		$('#errordivstdTch').append("&#149; "+resourceJSON.msgfillfromDate+"<br>");
		if(focs==0)
			$('#fromStdTch').focus();

		$('#fromStdTch').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	if(toDate==""){
		$('#errordivstdTch').append("&#149; "+resourceJSON.msgfillToDate+"<br>");
		if(focs==0)
			$('#toStdTch').focus();

		$('#toStdTch').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	
	if(cnt!=0){
		$('#errordivstdTch').show();
	}
	else
	PFExperiences.insertOrUpdateStdTchrExp(stdTchrExpId,schoolNamestdTch,subjectStdTch,pkOfferedStdTchr, kgOfferedStdTchr, g01OfferedStdTchr, g02OfferedStdTchr, g03OfferedStdTchr, g04OfferedStdTchr, g05OfferedStdTchr, g06OfferedStdTchr, g07OfferedStdTchr, g08OfferedStdTchr, g09OfferedStdTchr, g10OfferedStdTchr, g11OfferedStdTchr, g12OfferedStdTchr, fromDate, toDate,
		{ 
		async: false,
		errorHandler:handleError,
		callback: function(data)
		{
		displayStdTchrExp();
		$("#divstdTchExp").hide();
			//$("#tfaDistOptData").html(data);
		}
	});	
}

function displayStdTchrExp(){
	
	PFExperiences.displayStdTchrExp(dp_StdTchrExp_Rows,dp_StdTchrExp_page,dp_StdTchrExp_sortOrderStr,dp_StdTchrExp_sortOrderType,{ 
			async: false,
			errorHandler:handleError,
			callback: function(data)
			{
			//alert(data);
				$("#divDatastdTchrGrid").html(data);
				if($('#ttlRecStdExp').val()>0){					
					document.getElementById('StuTchrChk').checked=true;
					$(".stuTchrExpDiv").show();
				}
				applyScrollOnTblStdTchr();
			}
		});
}

function showStdExpTchrForm(){
	$('#toStdTch').css("background-color","");
	$('#fromStdTch').css("background-color","");
	$('#subjectStdTch').css("background-color","");
	$('#schoolNameStdTch').css("background-color","");
	document.getElementById('schoolNameStdTch').value="";
	document.getElementById('subjectStdTch').value="";
	document.getElementById('toStdTch').value="";
	document.getElementById('fromStdTch').value="";
	document.getElementById('stdTchrExpId').value="";
	
	
	document.getElementById('kgOfferedStdTchr').checked=false;
	document.getElementById('g01OfferedStdTchr').checked=false;
	document.getElementById('g02OfferedStdTchr').checked=false;
	document.getElementById('g03OfferedStdTchr').checked=false;
	document.getElementById('g04OfferedStdTchr').checked=false;
	document.getElementById('g05OfferedStdTchr').checked=false;
	document.getElementById('g06OfferedStdTchr').checked=false;
	document.getElementById('g07OfferedStdTchr').checked=false;
	document.getElementById('g08OfferedStdTchr').checked=false;
	document.getElementById('g09OfferedStdTchr').checked=false;
	document.getElementById('g10OfferedStdTchr').checked=false;
	document.getElementById('g11OfferedStdTchr').checked=false;
	document.getElementById('g12OfferedStdTchr').checked=false;
	$("#divstdTchExp").show();
}
function delStdTchrExp(id){
	PFExperiences.delStdTchrExp(id, { 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
		displayStdTchrExp();
		hideStdTchrExp();
			return false;
		}});
}

function editFormStdTchrExp(id){
	PFExperiences.editFormStdTchrExp(id,
			{ 
				async: false,
				errorHandler:handleError,
				callback: function(data)
				{
		
					showStdExpTchrForm();
					document.getElementById('schoolNameStdTch').value=data.schoolName;
					document.getElementById('subjectStdTch').value=data.subject;
					document.getElementById('toStdTch').value=data.toDate;
					document.getElementById('fromStdTch').value=data.fromDate;
					document.getElementById('stdTchrExpId').value=data.studentTeacherExperienceId;
					
					document.getElementById('pkOfferedStdTchr').checked=data.pkOffered;
					document.getElementById('kgOfferedStdTchr').checked=data.kgOffered;
					document.getElementById('g01OfferedStdTchr').checked=data.g01Offered;
					document.getElementById('g02OfferedStdTchr').checked=data.g02Offered;
					document.getElementById('g03OfferedStdTchr').checked=data.g03Offered;
					document.getElementById('g04OfferedStdTchr').checked=data.g04Offered;
					document.getElementById('g05OfferedStdTchr').checked=data.g05Offered;
					document.getElementById('g06OfferedStdTchr').checked=data.g06Offered;
					document.getElementById('g07OfferedStdTchr').checked=data.g07Offered;
					document.getElementById('g08OfferedStdTchr').checked=data.g08Offered;
					document.getElementById('g09OfferedStdTchr').checked=data.g09Offered;
					document.getElementById('g10OfferedStdTchr').checked=data.g10Offered;
					document.getElementById('g11OfferedStdTchr').checked=data.g11Offered;
					document.getElementById('g12OfferedStdTchr').checked=data.g12Offered;
					
				//	dwr.util.setValues(data);
					return false;
				}
			});
		return false;
}

function hideStdTchrExp(){
	$("#divstdTchExp").hide();
}

function getInvolvementGrid()
{
	
	//getInvolvementGridDistrictSpecific
	var districtId = document.getElementById("districtIdForDSPQ").value;
	var jobCat=$('#jobcategoryDsp').val().trim();
	if(districtId!="" && districtId==3703120){
		PFExperiences.getInvolvementGridDistrictSpecific(dp_Involvement_Rows,dp_Involvement_page,dp_Involvement_sortOrderStr,dp_Involvement_sortOrderType,districtId,jobCat,{ 
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{
			//document.getElementById("divDataInvolvement").innerHTML=data;
				$('#divDataInvolvement').html(data);
				applyScrollOnInvl();
				if(document.getElementById("districtIdForDSPQ").value==3703120 && $('#jobcategoryDsp').val().trim().indexOf("Bus Driver") !=-1){
					$(".pplRang").html("");
					$(".yNldPPl").html("");
				}
				
			}});
	}else{
		PFExperiences.getInvolvementGrid(dp_Involvement_Rows,dp_Involvement_page,dp_Involvement_sortOrderStr,dp_Involvement_sortOrderType,{ 
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{
			//document.getElementById("divDataInvolvement").innerHTML=data;
				$('#divDataInvolvement').html(data);
				applyScrollOnInvl();
			}});
	}

}

function showInvolvementForm()
{
	document.getElementById("frmInvolvement").reset();
	document.getElementById("involvementId").value="";	
	document.getElementById("divInvolvement").style.display="block";
	document.getElementById("organizationInv").focus();
	document.getElementById("divLeadNoOfPeople").style.display="none";
	
	//orgTypDivInv
	if(document.getElementById("districtIdForDSPQ").value==3703120){
		$("#orgTypDivInv").hide();
	}
	
	
	if(document.getElementById("districtIdForDSPQ").value==3703120 && $('#jobcategoryDsp').val().trim().indexOf("Bus Driver") !=-1){
		$("#rangeIdspq").hide();
		$("#leadPeoDspq").hide();
	}
	setDefColortoErrorMsgToInvolvement();
	$('#errordivInvolvement').empty();
	return false;
}
function hideInvolvement()
{
	document.getElementById("frmInvolvement").reset();
	document.getElementById("involvementId").value="";	
	document.getElementById("divInvolvement").style.display="none";
	window.location.hash = '#addInvolvemetIdDSPQ';
	return false;
}
function saveOrUpdateInvolvement()
{

	var organization = document.getElementById("organizationInv");
	var orgTypeId = document.getElementById("orgTypeId");
	var rangeId = document.getElementById("rangeId");
	var leadNoOfPeople =  document.getElementById("leadNoOfPeople");

	var rdo1 =  document.getElementById("rdo1");

	var cnt=0;
	var focs=0;	

	$('#errordivInvolvement').empty();
	setDefColortoErrorMsgToInvolvement();


	if(trim(organization.value)=="")
	{
		$('#errordivInvolvement').append("&#149; "+resourceJSON.PlzEtrOrg+"<br>");
		if(focs==0)
			$('#organizationInv').focus();

		$('#organizationInv').css("background-color",txtBgColor);
		cnt++;focs++;
	}

	if($("#orgTypDivInv").is(':visible') && trim(orgTypeId.value)=="")
	{
		$('#errordivInvolvement').append("&#149; "+resourceJSON.msgorgtype+"<br>");
		if(focs==0)
			$('#orgTypeId').focus();

		$('#orgTypeId').css("background-color",txtBgColor);
		cnt++;focs++;
	}

	if(document.getElementById("districtIdForDSPQ").value!=3703120)	
	if($("#rangeIdspq").is(':visible') && trim(rangeId.value)=="")
	{
		$('#errordivInvolvement').append("&#149; "+resourceJSON.msgnumberofpeople+"<br>");
		if(focs==0)
			$('#rangeId').focus();

		$('#rangeId').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	
	if(rdo1.checked && trim(leadNoOfPeople.value)=="")
	{
		$('#errordivInvolvement').append("&#149; "+resourceJSON.msgmanypeoplelead+"<br>");
		if(focs==0)
			$('#leadNoOfPeople').focus();

		$('#leadNoOfPeople').css("background-color",txtBgColor);
		cnt++;focs++;
	}


	if(cnt!=0)		
	{
		$('#errordivInvolvement').show();
		return false;
	}
	else
	{
		var orgObj = {orgTypeId:dwr.util.getValue("orgTypeId")};
		var peopleRangeObj = {rangeId:dwr.util.getValue("rangeId")};		
		var involvement = {involvementId:null, orgTypeMaster:orgObj, peopleRangeMaster:peopleRangeObj, leadNoOfPeople:null};
		
		dwr.engine.beginBatch();	
		dwr.util.getValues(involvement);	
		involvement.organization= organization.value;
		if(dwr.util.getValue("rangeId")==""){
			involvement.peopleRangeMaster=null;
		}
		if(!$("#orgTypDivInv").is(':visible') && dwr.util.getValue("orgTypeId")==""){
			involvement.orgTypeMaster=null;
		}
		PFExperiences.saveOrUpdateInvolvement(involvement,
				{ 
			async: false,
			errorHandler:handleError,
			callback: function(data)
			{

			hideInvolvement();
			getInvolvementGrid();

			return false;
			}
				});
		dwr.engine.endBatch();
	}
	return true;


}
function showEditFormInvolvement(id)
{
	PFExperiences.showEditFormInvolvement(id,
			{ 
		async: false,
		errorHandler:handleError,
		callback: function(data)
		{		
		showInvolvementForm();
		//alert(data.leadNoOfPeople)

		if(data.leadNoOfPeople!=null)
		{
			document.getElementById("rdo1").click();
		}
		else
		{
			document.getElementById("divLeadNoOfPeople").style.display="none";
			document.getElementById("leadNoOfPeople").value="";		
		}
		document.getElementById("involvementId").value=id;
		document.getElementById("organizationInv").value=data.organization;
		document.getElementById("orgTypeId").value=data.orgTypeMaster.orgTypeId;
		document.getElementById("rangeId").value=data.peopleRangeMaster.rangeId;
		document.getElementById("leadNoOfPeople").value=data.leadNoOfPeople;


		}
			});

	return false;
}
function showAndHideLeadNoOfPeople(opt)
{
	if(opt==1)
	{
		document.getElementById("divLeadNoOfPeople").style.display="block";
	}
	else
	{
		document.getElementById("divLeadNoOfPeople").style.display="none";
		document.getElementById("leadNoOfPeople").value="";		
	}

}
function deleteRecordInvolvment(id)
{
	if(window.confirm(""+resourceJSON.msgsuredelete+""))
	{
		PFExperiences.deleteRecordInvolvment(id, { 
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{
			hideInvolvement();
			getInvolvementGrid();

			return false;
			}});
	}
	return false;
}

function setDefColortoErrorMsgToInvolvement()
{
	$('#organizationInv').css("background-color","");
	$('#orgTypeId').css("background-color","");
	$('#rangeId').css("background-color","");
	$('#leadNoOfPeople').css("background-color","");

}

function getHonorsGrid()
{
	PFExperiences.getHonorsGrid({ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			document.getElementById("divDataHoner").innerHTML=data;
			applyScrollOnHonors();
		}});
}

function saveOrUpdateHonors()
{

	var honor = document.getElementById("honor");
	var honorYear = document.getElementById("honorYear");


	var cnt=0;
	var focs=0;	

	$('#errordivHonor').empty();
	setDefColortoErrorMsgToHoner();


	if(trim(honor.value)=="")
	{
		$('#errordivHonor').append("&#149; "+resourceJSON.msgaward+"<br>");
		if(focs==0)
			$('#honor').focus();

		$('#honor').css("background-color",txtBgColor);
		cnt++;focs++;
	}

	if(trim(honorYear.value)=="0")
	{
		$('#errordivHonor').append("&#149; "+resourceJSON.msgawardyear+"<br>");
		if(focs==0)
			$('#honorYear').focus();

		$('#honorYear').css("background-color",txtBgColor);
		cnt++;focs++;
	}


	if(cnt!=0)		
	{
		$('#errordivHonor').show();
		return false;
	}
	else
	{
		var honer = {honorId:null, honor:null, honorYear:null};

		dwr.engine.beginBatch();	
		dwr.util.getValues(honer);	
		PFExperiences.saveOrUpdateHonors(honer,
				{ 
			async: false,
			errorHandler:handleError,
			callback: function(data)
			{

			hideHonor();
			getHonorsGrid();

			return false;
			}
				});
		dwr.engine.endBatch();
	}
	return true;

}

function showEditHonors(id)
{
	PFExperiences.showEditHonors(id,
			{ 
		async: false,
		errorHandler:handleError,
		callback: function(data)
		{		
		showHonorForm();


		document.getElementById("honorId").value=id;
		document.getElementById("honor").value=data.honor;
		document.getElementById("honorYear").value=data.honorYear;

		}
			});

	return false;
}




function deleteRecordHonors(id)
{
	if(window.confirm(""+resourceJSON.msgsuredelete+""))
	{
		PFExperiences.deleteRecordHonors(id,{ 
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{
			hideHonor();
			getHonorsGrid();

			return false;
			}});
	}
	return false;
}
function setDefColortoErrorMsgToHoner()
{
	$('#honor').css("background-color","");
	$('#honorYear').css("background-color","");

}
function hideHonor()
{
	document.getElementById("frmHonor").reset();
	document.getElementById("honorId").value="";	
	document.getElementById("divHonor").style.display="none";
	window.location.hash = '#addHonorsIdDSPQ';
	return false;
}
function showHonorForm()
{
	document.getElementById("frmHonor").reset();
	document.getElementById("honorId").value="";	
	document.getElementById("divHonor").style.display="block";
	document.getElementById("honor").focus();
	setDefColortoErrorMsgToHoner();
	$('#errordivHonor').empty();
	return false;
}

function displayTchrLanguage(){
	
	
	PFExperiences.displayTeacherLanguage(dp_TchrLang_Rows,dp_TchrLang_page,dp_TchrLang_sortOrderStr,dp_TchrLang_sortOrderType,{ 
			async: false,
			errorHandler:handleError,
			callback: function(data)
			{
			//alert(data);
		$("#divDataLangTchrGrid").html(data);
		applyScrollOnTblteacherLang()
				/*$("#divDatastdTchrGrid").html(data);
				if($('#ttlRecStdExp').val()>0){					
					document.getElementById('StuTchrChk').checked=true;
					$(".stuTchrExpDiv").show();
				}
				applyScrollOnTblStdTchr();*/
			}
		});
}
function showLanguageGrid(){
	var checkedVal = $('input[name=knowLanguage]:checked').val();
	if(checkedVal=="1"){
		$('.langGridNform').show();
		displayTchrLanguage();
	}else{
		$(".langGridNform").hide();
		hideTchrLang();
	}
}
function showLanguageForm(){
	$("#languageText").val("");
	$("#oralSkill").val("");
	$("#writtenSkill").val("");
	setDefColortoErrorMsgToTchLang();
	$("#errordivTchLang").html("");
	$("#errordivTchLang").hide();
	$("#divLanguageForm").show();
}
function setDefColortoErrorMsgToTchLang()
{		
	$("#languageText").css("background-color","");
	$("#oralSkills").css("background-color","");
	$("#writtenSkills").css("background-color","");
}
function hideTchrLang()
{
	//document.getElementById("divLanguageForm").reset();		
	document.getElementById("divLanguageForm").style.display="none";
	return false;
}
function insertOrUpdateTchrLang(){
	var language = $("#languageText").val();
	var oralSkill=$("#oralSkills").val();
	var writtenSkill=$("#writtenSkills").val();
	var teacherLanguageId=$("#teacherLanguageId").val();
	
	var cnt=0;
	var focs=0;
	
	$('#errordivTchLang').empty();
	setDefColortoErrorMsgToTchLang();
	
	if(trim(language)=="")
	{
		$('#errordivTchLang').append("&#149; "+resourceJSON.msgEnterLang+"<br>");
		if(focs==0)
			$('#languageText').focus();

		$('#languageText').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	
	if(trim(oralSkill)=="0")
	{
		$('#errordivTchLang').append("&#149; "+resourceJSON.msgOralskill+"<br>");
		if(focs==0)
			$('#oralSkills').focus();

		$('#oralSkills').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	
	if(trim(writtenSkill)=="0")
	{
		$('#errordivTchLang').append("&#149; "+resourceJSON.msgWrittenSkill+"<br>");
		if(focs==0)
			$('#writtenSkills').focus();

		$('#writtenSkills').css("background-color",txtBgColor);
		cnt++;focs++;
	}

	if(cnt!=0)		
	{
		$('#errordivTchLang').show();
		return false;
	}else{

		/*var teacherLanguage = {teacherLanguageId:null, teacherId:null, language:null,oralSkills:null,writtenSkills:null,createdDateTime:null};

		dwr.engine.beginBatch();	
		dwr.util.getValues(teacherLanguage);
		teacherLanguage.language=language;*/
		PFExperiences.insertOrUpdateTchrLang(language,oralSkill,writtenSkill,teacherLanguageId,
				{ 
			async: false,
			errorHandler:handleError,
			callback: function(data)
			{
			hideTchrLang();			
			displayTchrLanguage();

			return false;
			}
		});
	}

}

function delTeacherLanguage(id)
{
	if(window.confirm(""+resourceJSON.msgsuredelete+""))
	{
		PFExperiences.delTeacherLanguage(id,{ 
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{
			hideTchrLang();			
			displayTchrLanguage();

			return false;
			}});
	}
	return false;
}
function editTeacherLanguage(id){
	
		PFExperiences.showEditLang(id,
		{ 
			async: false,
			errorHandler:handleError,
			callback: function(data)
			{		
				showLanguageForm();
				$("#languageText").val(data.language);
				$("#oralSkills").val(data.oralSkills);
				$("#writtenSkills").val(data.writtenSkills);
				$("#teacherLanguageId").val(data.teacherLanguageId);
			}
		});

		return false;
		
}
function updateLangeuage(){
	
	var checkedVal = $('input[name=knowLanguage]:checked').val();
	
	PFExperiences.updateLangeuage(checkedVal,
			{ 
				async: false,
				errorHandler:handleError,
				callback: function(data)
				{

				}
		});	
}

function checkKnowLanguage(){
PFExperiences.checkKnowLanguage({ 
	async: false,
	errorHandler:handleError,
	callback: function(data)
	{		
	
		if(data==true){
			$('#knowLanguage2').prop('checked', false);
			$('#knowLanguage1').prop('checked', true);
			showLanguageGrid();
		}else{
			$('#knowLanguage2').prop('checked', true);
			$('#knowLanguage1').prop('checked', false);
			showLanguageGrid();
		}
	}
});	
}

function addResidencyForm(){
	resetResidencyForm();
	$("#residencyForm").show();	
	$('#residencyStreetAddress').focus();
}
function resetResidencyForm(){	
	$("#residencyStreetAddress").val("");
	$("#streetAddress").val("");
	$("#residencyCity").val("");
	$("#residencyState").val("");
	$("#residencyZip").val("");
	$("#residencyCountry").val("");
	$("#residencyFromDate").val("");
	$("#residencyToDate").val("");
	setDefColortoErrorMsgToResidency();	
}

function closeResidencyForm(){
	resetResidencyForm();
	$("#residencyForm").hide();
}

function editFormResidency(residencyId){
	addResidencyForm();
	PFExperiences.editResidency(residencyId,
			{ 
				async: false,
				errorHandler:handleError,
				callback: function(data)
				{
					dwr.util.setValues(data);
				}
		});
	//dwr.util.setValues(data);
}

function delResidency(residencyId){
	if(window.confirm("Are you sure, you would like to delete Residency?"))
	{
	PFExperiences.deleteResidency(residencyId,
			{ 
				async: false,
				errorHandler:handleError,
				callback: function(data)
				{
					getResidencyGrid();
				}
		});
	}
	return false;
	//dwr.util.setValues(data);
}
function insertOrUpdate_Residency(){
	var streetAddress		=	$("#residencyStreetAddress").val().trim();
	var residencyCity		=	$("#residencyCity").val().trim();
	var residencyState		=	$("#residencyState").val().trim();
	var residencyZip		=	$("#residencyZip").val().trim();
	var residencyCountry	=	$("#residencyCountry").val().trim();
	var residencyFromDate	=	$("#residencyFromDate").val().trim();
	var residencyToDate		=	$("#residencyToDate").val().trim();
	
	var cnt=0;
	var focs=0;
	$('#errordivResidency').empty();
	$('#errordivResidency').hide();
	setDefColortoErrorMsgToResidency();
	
	if(streetAddress==""){
		$('#errordivResidency').append("&#149; Please enter Street Address<br>");
		if(focs==0)
			$('#residencyStreetAddress').focus();
		$('#residencyStreetAddress').css("background-color",txtBgColor);
		cnt++;focs++;
	}	
	if(residencyCity==""){
		$('#errordivResidency').append("&#149; Please enter City<br>");
		if(focs==0)
			$('#residencyCity').focus();
		$('#residencyCity').css("background-color",txtBgColor);
		cnt++;focs++;
	}	
	if(residencyState==""){
		$('#errordivResidency').append("&#149; Please enter State<br>");
		if(focs==0)
			$('#residencyState').focus();
		$('#residencyState').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	if(residencyZip==""){
		$('#errordivResidency').append("&#149; Please enter Zip<br>");
		if(focs==0)
			$('#residencyZip').focus();
		$('#residencyZip').css("background-color",txtBgColor);
		cnt++;focs++;
	}	
	if(residencyCountry==""){
		$('#errordivResidency').append("&#149; Please enter Country<br>");
		if(focs==0)
			$('#residencyCountry').focus();
		$('#residencyCountry').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	if(residencyFromDate==""){
		$('#errordivResidency').append("&#149; Please enter From Date<br>");
		if(focs==0)
			$('#residencyFromDate').focus();
		$('#residencyFromDate').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	if(residencyToDate==""){
		$('#errordivResidency').append("&#149; Please enter To Date<br>");
		if(focs==0)
			$('#residencyToDate').focus();
		$('#residencyToDate').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	var startDate	=	new Date(trim(residencyFromDate));
	var endDate	=	new Date(trim(residencyToDate));

	if(startDate!="" && endDate && (startDate>endDate))
	{
		$('#errordivResidency').append("&#149; Dates Residency from cannot be greater than Dates Residency to<br>");
		if(focs==0)
			$('#residencyFromDate').focus();
		
		$('#residencyFromDate').css("background-color",txtBgColor);
		$('#residencyToDate').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	if(cnt!=0){
		$('#errordivResidency').show();
	}else{
		//alert("No Error ....");
		var teacherresidencydetails = {teacherResidencyId:null,residencyStreetAddress:null,residencyCity:null,residencyState:null,residencyZip:null,residencyCountry:null,residencyFromDate:null,residencyToDate:null}
		dwr.engine.beginBatch();
		dwr.util.getValues(teacherresidencydetails);
		
		PFExperiences.insertOrUpdateResidency(teacherresidencydetails,
				{ 
					async: false,
					errorHandler:handleError,
					callback: function(data)
					{
						closeResidencyForm();
						getResidencyGrid();
					}
			});
		
		dwr.engine.endBatch();
		
	}
	//streetAddress,residencyCity,residencyState,residencyZip,residencyCountry,residencyFromDate,residencyToDate
/*	var teacherresidencydetails = {teacherResidencyId:null,residencyStreetAddress:null,residencyCity:null,residencyState:null,residencyZip:null,residencyCountry:null,residencyFromDate:null,residencyToDate:null}
	dwr.engine.beginBatch();
	dwr.util.getValues(teacherresidencydetails);*/
	
}

function setDefColortoErrorMsgToResidency(){
	$("#residencyStreetAddress").css("background-color","");
	$("#residencyCity").css("background-color","");
	$("#residencyState").css("background-color","");
	$("#residencyZip").css("background-color","");
	$("#residencyCountry").css("background-color","");
	$("#residencyFromDate").css("background-color","");
	$("#residencyToDate").css("background-color","");	
}

//
function getResidencyGrid(){
	
	PFExperiences.getResidencyGrid(dp_Residency_Rows,dp_Residency_page,dp_Residency_sortOrderStr,dp_Residency_sortOrderType,{ 
			async: false,
			errorHandler:handleError,
			callback: function(data)
			{
				$("#residencyGridDiv").html(data);				
				applyScrollOnTblResidencyDiv();
			}
		});
}