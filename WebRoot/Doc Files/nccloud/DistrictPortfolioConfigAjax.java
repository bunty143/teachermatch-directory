package tm.services.teacher;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.net.ssl.HttpsURLConnection;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

import org.apache.commons.io.FileUtils;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.api.UtilityAPI;
import tm.bean.DistrictPortfolioConfig;
import tm.bean.EmployeeMaster;
import tm.bean.InternalTransferCandidates;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.TeacherAcademics;
import tm.bean.TeacherAdditionalDocuments;
import tm.bean.TeacherAffidavit;
import tm.bean.TeacherCertificate;
import tm.bean.TeacherDetail;
import tm.bean.TeacherElectronicReferences;
import tm.bean.TeacherExperience;
import tm.bean.TeacherGeneralKnowledgeExam;
import tm.bean.TeacherHonor;
import tm.bean.TeacherInvolvement;
import tm.bean.TeacherPersonalInfo;
import tm.bean.TeacherPortfolioStatus;
import tm.bean.TeacherRole;
import tm.bean.TeacherSubjectAreaExam;
import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.CertificationStatusMaster;
import tm.bean.master.CountryMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictSchools;
import tm.bean.master.DistrictSpecificPortfolioQuestions;
import tm.bean.master.GenderMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.PortfolioItem;
import tm.bean.master.RaceMaster;
import tm.bean.master.StateMaster;
import tm.bean.master.StatusMaster;
import tm.bean.user.UserMaster;
import tm.dao.DistrictPortfolioConfigDAO;
import tm.dao.EmployeeMasterDAO;
import tm.dao.InternalTransferCandidatesDAO;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.TeacherAcademicsDAO;
import tm.dao.TeacherAdditionalDocumentsDAO;
import tm.dao.TeacherAffidavitDAO;
import tm.dao.TeacherCertificateDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.TeacherElectronicReferencesDAO;
import tm.dao.TeacherExperienceDAO;
import tm.dao.TeacherGeneralKnowledgeExamDAO;
import tm.dao.TeacherHonorDAO;
import tm.dao.TeacherInvolvementDAO;
import tm.dao.TeacherPersonalInfoDAO;
import tm.dao.TeacherPortfolioStatusDAO;
import tm.dao.TeacherRoleDAO;
import tm.dao.TeacherSubjectAreaExamDAO;
import tm.dao.TeacherVideoLinksDAO;
import tm.dao.master.CertificationStatusMasterDAO;
import tm.dao.master.CountryMasterDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.DistrictSchoolsDAO;
import tm.dao.master.DistrictSpecificPortfolioQuestionsDAO;
import tm.dao.master.GenderMasterDAO;
import tm.dao.master.JobCategoryMasterDAO;
import tm.dao.master.PortfolioitemDAO;
import tm.dao.master.RaceMasterDAO;
import tm.dao.master.StateMasterDAO;
import tm.dao.master.StatusMasterDAO;
import tm.dao.user.UserMasterDAO;
import tm.services.DemoScheduleMailThread;
import tm.services.EmailerService;
import tm.services.MailText;
import tm.services.PaginationAndSorting;
import tm.services.district.GlobalServices;
import tm.services.district.PrintOnConsole;
import tm.services.hqbranches.BranchesAjax;
import tm.utility.Utility;

public class DistrictPortfolioConfigAjax 
{
	 String locale = Utility.getValueOfPropByKey("locale");
	 String msgYrSesstionExp=Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale);
	 String lblAct=Utility.getLocaleValuePropByKey("lblAct", locale);
	 String lblAcademic=Utility.getLocaleValuePropByKey("lblAcademic", locale);
	 String lblEdit=Utility.getLocaleValuePropByKey("lblEdit", locale);
	 String lnkDlt=Utility.getLocaleValuePropByKey("lnkDlt", locale);
	 String msgNorecordfound=Utility.getLocaleValuePropByKey("msgNorecordfound", locale);
	 String lblAcademicTranscript=Utility.getLocaleValuePropByKey("lblAcademicTranscript", locale);
	 String headCoverLetr=Utility.getLocaleValuePropByKey("headCoverLetr", locale);
	 String lblCertification=Utility.getLocaleValuePropByKey("lblCertification", locale);
	 String lblProofOfCertification=Utility.getLocaleValuePropByKey("lblProofOfCertification", locale);
	 String lblReference=Utility.getLocaleValuePropByKey("lblReference", locale);
	 String lblReferenceLettersOfRecommentation=Utility.getLocaleValuePropByKey("lblReferenceLettersOfRecommentation", locale);
	 
	 
	@Autowired
	private DistrictSpecificPortfolioQuestionsDAO districtSpecificPortfolioQuestionsDAO;
	
	@Autowired
	private TeacherSubjectAreaExamDAO teacherSubjectAreaExamDAO;

	@Autowired
	private TeacherGeneralKnowledgeExamDAO teacherGeneralKnowledgeExamDAO;

	@Autowired
	private TeacherAdditionalDocumentsDAO teacherAdditionalDocumentsDAO;  

	@Autowired
	private InternalTransferCandidatesDAO internalTransferCandidatesDAO;

	@Autowired
	private RaceMasterDAO raceMasterDAO;
	
	@Autowired
	private JobCategoryMasterDAO jobCategoryMasterDAO;

	@Autowired
	private JobOrderDAO jobOrderDAO;
	public void setJobOrderDAO(JobOrderDAO jobOrderDAO) {
		this.jobOrderDAO = jobOrderDAO;
	}

	@Autowired
	private DistrictPortfolioConfigDAO districtPortfolioConfigDAO;
	public void setDistrictPortfolioConfigDAO(DistrictPortfolioConfigDAO districtPortfolioConfigDAO) {
		this.districtPortfolioConfigDAO = districtPortfolioConfigDAO;
	}

	@Autowired
	private TeacherExperienceDAO teacherExperienceDAO;
	public void setTeacherExperienceDAO(TeacherExperienceDAO teacherExperienceDAO){
		this.teacherExperienceDAO = teacherExperienceDAO;
	}

	@Autowired
	private TeacherElectronicReferencesDAO teacherElectronicReferencesDAO;
	public void setTeacherElectronicReferencesDAO(TeacherElectronicReferencesDAO teacherElectronicReferencesDAO) {
		this.teacherElectronicReferencesDAO = teacherElectronicReferencesDAO;
	}

	@Autowired
	private TeacherAcademicsDAO teacherAcademicsDAO;
	public void setTeacherAcademicsDAO(TeacherAcademicsDAO teacherAcademicsDAO){
		this.teacherAcademicsDAO = teacherAcademicsDAO;
	}

	@Autowired
	private TeacherCertificateDAO teacherCertificateDAO;
	public void setTeacherCertificateDAO(TeacherCertificateDAO teacherCertificateDAO){
		this.teacherCertificateDAO = teacherCertificateDAO;
	}

	@Autowired
	private TeacherInvolvementDAO teacherInvolvementDAO;
	@Autowired
	private TeacherHonorDAO teacherHonorDAO;
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	public void setJobForTeacherDAO(JobForTeacherDAO jobForTeacherDAO) {
		this.jobForTeacherDAO = jobForTeacherDAO;
	}

	@Autowired
	private TeacherPersonalInfoDAO teacherPersonalInfoDAO;
	public void setTeacherPersonalInfoDAO(
			TeacherPersonalInfoDAO teacherPersonalInfoDAO) {
		this.teacherPersonalInfoDAO = teacherPersonalInfoDAO;
	}
	@Autowired
	private PortfolioitemDAO portfolioitemDAO;
	public void setPortfolioitemDAO(PortfolioitemDAO portfolioitemDAO) {
		this.portfolioitemDAO = portfolioitemDAO;
	}

	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	public void setDistrictMasterDAO(DistrictMasterDAO districtMasterDAO) {
		this.districtMasterDAO = districtMasterDAO;
	}

	@Autowired
	private TeacherRoleDAO teacherRoleDAO;
	public void setTeacherRoleDAO(TeacherRoleDAO teacherRoleDAO) {
		this.teacherRoleDAO = teacherRoleDAO;
	}

	@Autowired
	private TeacherPortfolioStatusDAO teacherPortfolioStatusDAO;
	public void setTeacherPortfolioStatusDAO(
			TeacherPortfolioStatusDAO teacherPortfolioStatusDAO) {
		this.teacherPortfolioStatusDAO = teacherPortfolioStatusDAO;
	}

	@Autowired
	private TeacherAffidavitDAO teacherAffidavitDAO;
	public void setTeacherAffidavitDAO(TeacherAffidavitDAO teacherAffidavitDAO) {
		this.teacherAffidavitDAO = teacherAffidavitDAO;
	}

	@Autowired
	private TeacherPersonalInfoDAO personalInfoDAO;
	public void setPersonalInfoDAO(TeacherPersonalInfoDAO personalInfoDAO) {
		this.personalInfoDAO = personalInfoDAO;
	}
	@Autowired
	private UserMasterDAO userMasterDAO;

	@Autowired
	private CertificationStatusMasterDAO certificationStatusMasterDAO;

	@Autowired
	private TeacherDetailDAO teacherDetailDAO;
	@Autowired
	private EmployeeMasterDAO employeeMasterDAO;

	@Autowired
	private EmailerService emailerService;

	@Autowired
	private GenderMasterDAO genderMasterDAO;

	@Autowired
	private CountryMasterDAO countryMasterDAO;
	
	@Autowired
	private StateMasterDAO stateMasterDAO;
	
	@Autowired
	private TeacherVideoLinksDAO teacherVideoLinksDAO;
	@Autowired
	private DistrictSchoolsDAO districtSchoolsDAO;
	
	@Autowired
	private StatusMasterDAO statusMasterDAO;
	public void setStatusMasterDAO(StatusMasterDAO statusMasterDAO) {
		this.statusMasterDAO = statusMasterDAO;
	}
	
	@Transactional(readOnly=false)
	public DistrictPortfolioConfig getPortfolioConfigByJobId(Integer jobId,String isAffilated){
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		DistrictPortfolioConfig districtPortfolioConfig=null;
		JobCategoryMaster parentJobCategory = null;
		try {
			boolean internalFlag=false;
			boolean dspqFlag=true;
			 boolean isNonTeacher = false;	
			 boolean isAlsoSA=false;
			 boolean jobSubCateFlag = false;
			 boolean jobSubCatedspqFlag = false;
			 
			DistrictMaster districtMaster=null;
			 boolean isSchoolSupportPhiladelphia=false;
			JobOrder jobOrder=null;
			if(jobId!=null){
				jobOrder=jobOrderDAO.findById(jobId, false, false);
				if(jobOrder!=null){
					if(jobOrder.getDistrictMaster()!=null)
						districtMaster=jobOrder.getDistrictMaster();
					try{
						TeacherDetail teacherDetail =(TeacherDetail) session.getAttribute("teacherDetail");
						
						try{
							//if((session!=null || (session.getAttribute("isAlsoSA")!=null)) && session.getAttribute("isAlsoSA").equals(true))
							
							if(session != null && (session.getAttribute("isAlsoSA")!=null))
							{
								isAlsoSA=(Boolean)session.getAttribute("isAlsoSA");
							}
						} catch(Exception exception){
							exception.printStackTrace();
						}

						if(teacherDetail!=null){
							List<InternalTransferCandidates> itcList=internalTransferCandidatesDAO.getInternalCandidateByDistrict(teacherDetail,districtMaster);
							if(itcList.size()>0){
								internalFlag=true;
								if(districtMaster.getOfferDistrictSpecificItems()){
									dspqFlag=true;
								}else{
									dspqFlag=false;
								}
							}
						}
					}catch(Exception e){
						e.printStackTrace();
					}
					try{
						if(jobOrder!=null){
							if(jobOrder.getJobCategoryMaster().getParentJobCategoryId()!=null && jobOrder.getJobCategoryMaster().getParentJobCategoryId().getJobCategoryId()>0){
								System.out.println("00001");
								jobSubCateFlag = true;
								if(jobOrder.getJobCategoryMaster().getAttachDSPQFromJC()!=null && jobOrder.getJobCategoryMaster().getAttachDSPQFromJC()){
									System.out.println("00002");
									jobSubCatedspqFlag = true; //DSPQ display By Parent Job category
									parentJobCategory = jobOrder.getJobCategoryMaster().getParentJobCategoryId();
								}else{
									System.out.println("00003");
									jobSubCatedspqFlag = false; //DSPQ display By Sub Job category
								}
							}
						}
						if(jobOrder!=null && isAffilated.equalsIgnoreCase("I") && internalFlag==false){
							System.out.println("00004");
								if(jobSubCateFlag && jobSubCatedspqFlag){
									System.out.println("00005");
										if(parentJobCategory!=null && parentJobCategory.getOfferDistrictSpecificItems()){
											System.out.println("00006");
											dspqFlag=true;
										}else{
											System.out.println("00007");
											dspqFlag=false;
										}
								}else{
									System.out.println("00008");
										if(jobOrder.getJobCategoryMaster()!=null && jobOrder.getJobCategoryMaster().getOfferDistrictSpecificItems()){
											System.out.println("00009");
											dspqFlag=true;
										}else{
											System.out.println("00010");
											dspqFlag=false;
										}
								}
							}else{
								System.out.println("00011");
									if(jobSubCateFlag && jobSubCatedspqFlag){
										System.out.println("00012");
										if(parentJobCategory!=null && parentJobCategory.getOfferDistrictSpecificItems()){
											System.out.println("00013");
											dspqFlag=true;
										}else{
											System.out.println("00014");
											dspqFlag=false;
										}
								}else{
									System.out.println("00015");
										if(jobOrder.getJobCategoryMaster()!=null && jobOrder.getJobCategoryMaster().getOfferDistrictSpecificItems()){
											System.out.println("00016");
											dspqFlag=true;
										}else{
											System.out.println("00017");
											dspqFlag=false;
										}
								}
							}
							
						
					}catch(Exception e){
						e.printStackTrace();

					}
					
					HeadQuarterMaster headQuarterMaster=null;
					if(jobOrder.getHeadQuarterMaster()!=null)
						headQuarterMaster=jobOrder.getHeadQuarterMaster();
					
					BranchMaster branchMaster=null;
					if(jobOrder.getBranchMaster() !=null)
						branchMaster=jobOrder.getBranchMaster();
					
					
					JobCategoryMaster jobCategoryMaster=null;
					if(jobOrder.getJobCategoryMaster()!=null)
						jobCategoryMaster=jobOrder.getJobCategoryMaster();
					
			          if(districtMaster!=null || branchMaster!=null || headQuarterMaster!=null)
			          {
			        	  //getPortfolioConfigByJobCategory
			        	/*districtPortfolioConfig=districtPortfolioConfigDAO.getPortfolioConfigByJobCategory(districtMaster,isAffilated,jobOrder.getJobCategoryMaster());
			        	
			        	if(districtPortfolioConfig==null){*/
			        	  
			        		//districtPortfolioConfig=districtPortfolioConfigDAO.getPortfolioConfig(districtMaster,isAffilated,jobOrder);
			        	  
			        	  Map<Integer, DistrictPortfolioConfig> mapHQ=new HashMap<Integer, DistrictPortfolioConfig>();
			        	  Map<Integer, DistrictPortfolioConfig> mapBM=new HashMap<Integer, DistrictPortfolioConfig>();
			        	  Map<Integer, DistrictPortfolioConfig> mapDM=new HashMap<Integer, DistrictPortfolioConfig>();
			        	  Map<Integer, DistrictPortfolioConfig> mapJC=new HashMap<Integer, DistrictPortfolioConfig>();
			        	  
			        	  List<DistrictPortfolioConfig> districtPortfolioConfigs=new ArrayList<DistrictPortfolioConfig>();
			        	  
			        	//  System.out.println("isAffilated :: "+isAffilated +" tDistrictId() :: "+jobOrder.getDistrictMaster().getDistrictId());
			        	  
			        	  districtPortfolioConfigs=districtPortfolioConfigDAO.getPortfolioConfig(isAffilated,jobOrder);
			        	  System.out.println("districtPortfolioConfigs Size "+districtPortfolioConfigs.size());
			        		if(districtPortfolioConfigs!=null && districtPortfolioConfigs.size()>0)
			        		{
			        			for(DistrictPortfolioConfig dPC:districtPortfolioConfigs)
			        			{
			        				if(dPC!=null)
			        				{
			        					if(dPC.getHeadQuarterMaster()!=null && dPC.getBranchMaster()!=null && dPC.getDistrictMaster()!=null && dPC.getJobCategoryMaster()!=null)
			        					{
			        						if(jobSubCateFlag && jobSubCatedspqFlag){
			        							if(jobOrder.getJobCategoryMaster().getParentJobCategoryId().getJobCategoryId()==dPC.getJobCategoryMaster().getJobCategoryId()){
			        								mapJC.put(dPC.getJobCategoryMaster().getJobCategoryId(), dPC);
			        							}
			        						}else{
			        							mapJC.put(dPC.getJobCategoryMaster().getJobCategoryId(), dPC);
			        						}
			        						System.out.println("Put JC");
			        					}
			        					else if(dPC.getHeadQuarterMaster()!=null && dPC.getBranchMaster()!=null && dPC.getDistrictMaster()!=null && dPC.getJobCategoryMaster()==null)
			        					{
			        						mapDM.put(dPC.getDistrictMaster().getDistrictId(), dPC);
			        						System.out.println("Put DM");
			        					}
			        					else if(dPC.getHeadQuarterMaster()!=null && dPC.getBranchMaster()!=null && dPC.getDistrictMaster()==null && dPC.getJobCategoryMaster()==null)
			        					{
			        						mapBM.put(dPC.getBranchMaster().getBranchId(), dPC);
			        						System.out.println("Put BM");
			        					}
			        					else if(dPC.getHeadQuarterMaster()!=null	 && dPC.getBranchMaster()==null && dPC.getDistrictMaster()==null && dPC.getJobCategoryMaster()==null)
			        					{
			        						mapHQ.put(dPC.getHeadQuarterMaster().getHeadQuarterId(), dPC);
			        						System.out.println("Put HQ");
			        					} // Start for old business rule
			        					else if(dPC.getHeadQuarterMaster()==null && dPC.getBranchMaster()==null && dPC.getDistrictMaster()!=null && dPC.getJobCategoryMaster()==null)
			        					{
			        						mapDM.put(dPC.getDistrictMaster().getDistrictId(), dPC);
			        						System.out.println("Put Old DM");
			        					}
			        					else if(dPC.getHeadQuarterMaster()==null && dPC.getBranchMaster()==null && dPC.getDistrictMaster()!=null && dPC.getJobCategoryMaster()!=null)
			        					{
			        						System.out.println(" 0045 01");
			        						if(jobSubCateFlag && jobSubCatedspqFlag){
			        							if(jobOrder.getJobCategoryMaster().getParentJobCategoryId().getJobCategoryId()==dPC.getJobCategoryMaster().getJobCategoryId()){
			        								mapJC.put(dPC.getJobCategoryMaster().getJobCategoryId(), dPC);
			        							}	
			        						}else{
			        							System.out.println(" 0045 03");
			        							mapJC.put(dPC.getJobCategoryMaster().getJobCategoryId(), dPC);
			        						}
			        						//mapJC.put(dPC.getJobCategoryMaster().getJobCategoryId(), dPC);
			        						System.out.println("Put Old JC"+mapJC);
			        					}
			        				}
			        			}
			        		}
			        		
			        		for(Map.Entry<Integer, DistrictPortfolioConfig> entry : mapJC.entrySet())
							{
			        		System.out.println(entry.getValue().getDistrictPortfolioConfigId() +" :: >>>>>>>>>>>>>> :: "+entry.getKey());	
							}
			        		
			        		if(jobCategoryMaster!=null)
			        		{
			        			System.out.println("get DSPQ Jc");
			        			if(jobSubCateFlag && jobSubCatedspqFlag){
			        				if(mapJC!=null && mapJC.get(jobOrder.getJobCategoryMaster().getParentJobCategoryId().getJobCategoryId())!=null){
				        				districtPortfolioConfig=mapJC.get(jobOrder.getJobCategoryMaster().getParentJobCategoryId().getJobCategoryId());
			        				}
			        			}else{
			        				System.out.println("00019");
			        				if(mapJC!=null && mapJC.get(jobOrder.getJobCategoryMaster().getJobCategoryId())!=null)
				        				districtPortfolioConfig=mapJC.get(jobOrder.getJobCategoryMaster().getJobCategoryId());
			        			}
			        		}
			        		if(districtPortfolioConfig==null && districtMaster!=null)
			        		{
			        			System.out.println("get DSPQ DM");
			        			if(mapDM!=null && mapDM.get(districtMaster.getDistrictId())!=null)
			        				districtPortfolioConfig=mapDM.get(districtMaster.getDistrictId());
			        		}
			        		if(districtPortfolioConfig==null && branchMaster!=null)
			        		{
			        			System.out.println("get DSPQ BM");
			        			if(mapBM!=null && mapBM.get(branchMaster.getBranchId())!=null)
			        				districtPortfolioConfig=mapBM.get(branchMaster.getBranchId());
			        			else
			        				System.out.println("Not in BM");
			        		}
			        		if(districtPortfolioConfig==null && headQuarterMaster!=null)
			        		{
			        			System.out.println("get DSPQ HQ");
			        			if(mapHQ!=null && mapHQ.get(headQuarterMaster.getHeadQuarterId())!=null)
			        				districtPortfolioConfig=mapHQ.get(headQuarterMaster.getHeadQuarterId());
			        			else
			        				System.out.println("Not in HQ");
			        		}
			        	//}
			        	
			        	boolean dspqExist=true;
						if(districtPortfolioConfig==null){
							dspqExist=false;
							districtPortfolioConfig=new DistrictPortfolioConfig();
							districtPortfolioConfig.setCoverLetter(false);
							districtPortfolioConfig.setReference(0);
							districtPortfolioConfig.setAcademic(0);
							districtPortfolioConfig.setAcademicTranscript(0);
							districtPortfolioConfig.setCertification(0);
							districtPortfolioConfig.setProofOfCertification(0);
							districtPortfolioConfig.setReferenceLettersOfRecommendation(0);
							districtPortfolioConfig.setDistrictPortfolioConfigId(1);
							districtPortfolioConfig.setDistrictMaster(districtMaster);
						}
						districtPortfolioConfig.setIsPrinciplePhiladelphia(false);
						districtPortfolioConfig.setJobTitle(jobOrder.getJobTitle());
						//System.out.println(">>>>>>>>>>>>>................      "+districtPortfolioConfig.getVeteran());
						try
				          {
							if ((districtPortfolioConfig!= null) && (districtPortfolioConfig.getJobCategoryMaster()!= null)) {
			            		districtPortfolioConfig.setJobCategoryName(districtPortfolioConfig.getJobCategoryMaster().getJobCategoryName());
							}
				            if ((jobOrder != null) && (districtMaster!= null && districtMaster.getDistrictId().equals(Integer.valueOf(4218990)))) {
				            	
				            	if ((jobOrder.getJobCategoryMaster()!= null) && (jobOrder.getJobCategoryMaster().getJobCategoryName().contains("School Support"))) {
					            	  isSchoolSupportPhiladelphia = true;					            	  
					              }else if ((jobOrder.getJobCategoryMaster()!= null) && (!jobOrder.getJobCategoryMaster().getJobCategoryName().equals("Teacher"))) {
					            	  isNonTeacher = true;
				              }else {
				                isNonTeacher = false;
				                isSchoolSupportPhiladelphia = false;
				              }			              
				              
				            	if ((jobOrder.getJobCategoryMaster()!= null) && (jobOrder.getJobCategoryMaster().getJobCategoryName().contains("Principal"))) {
				            		districtPortfolioConfig.setJobCategoryName(jobOrder.getJobCategoryMaster().getJobCategoryName());
				            		districtPortfolioConfig.setIsPrinciplePhiladelphia(isAlsoSA);
				            		System.out.println("isAffilated  isAffilated   "+isAffilated);
				            		if(isAffilated.equalsIgnoreCase("I")){
				            			districtPortfolioConfig.setIsPrinciplePhiladelphia(true);
				            		}
					              }
				            }else  if ((jobOrder != null) && (districtMaster!= null && districtMaster.getDistrictId().equals(Integer.valueOf(3904380)))) {
				            	
				            if ((jobOrder.getJobCategoryMaster()!= null) && (!jobOrder.getJobCategoryMaster().getJobCategoryName().equals("Certified Teacher"))) {				            	
					            	  isNonTeacher = true;				              }		              
				            }
				          }
				          catch (Exception e)
				          {
				            e.printStackTrace();
				          }
						 districtPortfolioConfig.setIsNonTeacher(Boolean.valueOf(isNonTeacher));
						 districtPortfolioConfig.setIsSchoolSupportPhiladelphia(Boolean.valueOf(isSchoolSupportPhiladelphia));
						List<DistrictSpecificPortfolioQuestions> districtSpecificPortfolioQuestionsList=districtSpecificPortfolioQuestionsDAO.getDistrictSpecificPortfolioQuestionBYJOb(jobOrder);
						System.out.println("*** districtSpecificPortfolioQuestionsList "+districtSpecificPortfolioQuestionsList.size());
						if(districtSpecificPortfolioQuestionsList.size()>0){
							districtPortfolioConfig.setDistrictSpecificPortfolioQuestions(true);
						}else{
							if(dspqExist==false){
								districtPortfolioConfig=null;
							}else{
								districtPortfolioConfig.setDistrictSpecificPortfolioQuestions(false);
							}
						}
						if(districtMaster!=null && districtMaster.getDistrictId()==7800040 && isAffilated.equalsIgnoreCase("I")){
							districtPortfolioConfig.setDistrictSpecificPortfolioQuestions(false);
						}
						if(dspqFlag && districtPortfolioConfig!=null && dspqExist){
							boolean formeremployee=false;
							if(districtPortfolioConfig!=null)
							{
								if(districtPortfolioConfig.getFormeremployee()!=null && districtPortfolioConfig.getFormeremployee())
									formeremployee=true;
								else
									formeremployee=false;
							}
							session.setAttribute("isCurrentEmploymentNeeded",formeremployee);
							if(districtPortfolioConfig!=null)
							{
								try
								{
									if(districtMaster!= null && districtMaster.getDistrictId()==1200390 && jobOrder!=null && jobOrder.getJobCategoryMaster()!=null && jobOrder.getJobCategoryMaster().getJobCategoryName().equalsIgnoreCase("Interventionists"))
									{
										System.out.println("1");
										districtPortfolioConfig.setIsInterventionistsForMiami(true);
									}
									else
									{
										System.out.println("2");
										districtPortfolioConfig.setIsInterventionistsForMiami(false);
										if(districtMaster!= null && districtMaster.getDistrictId()==1200390 && jobOrder!=null && jobOrder.getJobCategoryMaster()!=null && (jobOrder.getJobCategoryMaster().getJobCategoryName().equalsIgnoreCase("Substitute Instructional Positions")))
										{
											System.out.println("3");
											districtPortfolioConfig.setIsSubstituteInstructionalForMiami(true);
											PrintOnConsole.debugPrintln("IsSubstituteInstructionalForMiami is true");
											PrintOnConsole.debugPrintln("jobOrder.getJobCategoryMaster().getJobCategoryName() "+jobOrder.getJobCategoryMaster().getJobCategoryName());
										}
										else
										{
											System.out.println("4");
											districtPortfolioConfig.setIsSubstituteInstructionalForMiami(false);
											PrintOnConsole.debugPrintln("IsSubstituteInstructionalForMiami is false");
										}
									}
								}catch(Exception e)
								{
									e.printStackTrace();
								}
							}
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally
		{
			System.out.println(" districtPortfolioConfig 01 "+districtPortfolioConfig);
			return districtPortfolioConfig;
		}
	}

	public int countAcademic()
	{
		int iReturnValue=0;
		TeacherDetail teacherDetail=null;

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		else
			teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");

		try 
		{
			if(teacherDetail!=null)
			{
				List<TeacherAcademics> teacherAcademics=new ArrayList<TeacherAcademics>();
				teacherAcademics =teacherAcademicsDAO.findAcadamicDetailByTeacher(teacherDetail);
				iReturnValue=teacherAcademics.size();

			}

		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		finally
		{
			return iReturnValue;
		}

	}


	public int countAcademicTranscript()
	{
		int iReturnValue=0;
		TeacherDetail teacherDetail=null;

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		else
			teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");

		try 
		{
			int iCountAcademic=countAcademic();
			if(iCountAcademic > 0)
			{
				if(teacherDetail!=null)
				{
					List<TeacherAcademics> teacherAcademics=new ArrayList<TeacherAcademics>();
					teacherAcademics =teacherAcademicsDAO.findTranscript_Null(teacherDetail);
					iReturnValue=teacherAcademics.size();

				}
			}

		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		finally
		{
			return iReturnValue;
		}

	}


	public int countCertification()
	{
		int iReturnValue=0;
		TeacherDetail teacherDetail=null;

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		else
			teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");

		try 
		{
			if(teacherDetail!=null)
			{
				List<TeacherCertificate> lstCertificate=new ArrayList<TeacherCertificate>();
				lstCertificate =teacherCertificateDAO.findCertificateByTeacher(teacherDetail);
				iReturnValue=lstCertificate.size();

			}

		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		finally
		{
			return iReturnValue;
		}

	}


	public int countProofOfCertification()
	{
		int iReturnValue=0;
		TeacherDetail teacherDetail=null;

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		else
			teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");

		try 
		{
			if(teacherDetail!=null)
			{
				List<TeacherCertificate> teacherCertificates=new ArrayList<TeacherCertificate>();
				teacherCertificates =teacherCertificateDAO.findAttachPathOfCertification(teacherDetail);
				iReturnValue=teacherCertificates.size();
			}

		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		finally
		{
			return iReturnValue;
		}

	}

	public int countReference()
	{
		int iReturnValue=0;
		TeacherDetail teacherDetail=null;

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		else
			teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");

		try 
		{
			if(teacherDetail!=null)
			{
				List<TeacherElectronicReferences> lstTeacherElectronicReferences = new ArrayList<TeacherElectronicReferences>();
				lstTeacherElectronicReferences =teacherElectronicReferencesDAO.findActiveReferencesByTeacher(teacherDetail);
				iReturnValue=lstTeacherElectronicReferences.size();

			}

		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		finally
		{
			return iReturnValue;
		}

	}

	public int countReferenceLettersOfRecommendation()
	{
		int iReturnValue=0;
		TeacherDetail teacherDetail=null;

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		else
			teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");

		try 
		{
			if(teacherDetail!=null)
			{
				List<TeacherElectronicReferences> lstTeacherElectronicReferences = new ArrayList<TeacherElectronicReferences>();
				lstTeacherElectronicReferences =teacherElectronicReferencesDAO.findActiveReferencesByPathOfReference(teacherDetail);
				iReturnValue=lstTeacherElectronicReferences.size();

			}

		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		finally
		{
			return iReturnValue;
		}

	}

	public boolean callResume()
	{
		boolean bReturnValue=false;
		TeacherDetail teacherDetail=null;

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		else
			teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");

		try 
		{
			if(teacherDetail!=null)
			{
				TeacherExperience teacherExperience = teacherExperienceDAO.findTeacherExperienceByTeacher(teacherDetail);
				if(teacherExperience!=null)
					if(teacherExperience.getResume()!=null && !teacherExperience.getResume().equalsIgnoreCase(""))
						bReturnValue=true;
			}

		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		finally
		{
			return bReturnValue;
		}

	}


	@Transactional(readOnly=false)
	public boolean isJobApplied(Integer jobId)
	{
		boolean bReturnValue=false;
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}

		try
		{
			TeacherDetail teacherDetail = null;
			if (session!= null && session.getAttribute("teacherDetail") != null)
				teacherDetail=(TeacherDetail)session.getAttribute("teacherDetail");

			JobOrder jobOrder=null;
			if(jobId!=null)
				jobOrder=jobOrderDAO.findById(jobId, false, false);

			JobForTeacher jobForTeacher=null;
			if(teacherDetail!=null && jobOrder!=null)
				jobForTeacher = jobForTeacherDAO.findJFTByTeacherAndJob(teacherDetail, jobOrder);

			if(jobForTeacher!=null)
				bReturnValue=true;

		}catch (Exception e) {
			e.printStackTrace();

		}
		finally
		{
			return bReturnValue;
		}
	}



	@Transactional(readOnly=false)
	public DistrictPortfolioConfig getPortfolioConfigByJobIdForExternal(Integer jobId,String isAffilated){
		System.out.println("::::::::::::::::getPortfolioConfigByJobIdForExternal::::::::::Candidate Type "+isAffilated);
		DistrictPortfolioConfig districtPortfolioConfig=null;
		
		try {
			DistrictMaster districtMaster=null;
			JobOrder jobOrder=null;
			boolean isNonTeacher = false;
			boolean jobSubCateFlag = false;
			 boolean jobSubCatedspqFlag = false;
			boolean isSchoolSupportPhiladelphia=false;
			 boolean isAlsoSA=false;
			if(jobId!=null){
				jobOrder=jobOrderDAO.findById(jobId, false, false);
				if(jobOrder!=null){
					districtMaster=jobOrder.getDistrictMaster();
					
					
					
					HeadQuarterMaster headQuarterMaster=null;
					if(jobOrder.getHeadQuarterMaster()!=null)
						headQuarterMaster=jobOrder.getHeadQuarterMaster();
					
					BranchMaster branchMaster=null;
					if(jobOrder.getBranchMaster() !=null)
						branchMaster=jobOrder.getBranchMaster();
					
					
					JobCategoryMaster jobCategoryMaster=null;
					if(jobOrder.getJobCategoryMaster()!=null)
						jobCategoryMaster=jobOrder.getJobCategoryMaster();
					
			          if(districtMaster!=null || branchMaster!=null || headQuarterMaster!=null)
			          {
			        	  //getPortfolioConfigByJobCategory
			        	/*districtPortfolioConfig=districtPortfolioConfigDAO.getPortfolioConfigByJobCategory(districtMaster,isAffilated,jobOrder.getJobCategoryMaster());
			        	
			        	if(districtPortfolioConfig==null){*/
			        	  
			        		//districtPortfolioConfig=districtPortfolioConfigDAO.getPortfolioConfig(districtMaster,isAffilated,jobOrder);
			        	  
			        	  Map<Integer, DistrictPortfolioConfig> mapHQ=new HashMap<Integer, DistrictPortfolioConfig>();
			        	  Map<Integer, DistrictPortfolioConfig> mapBM=new HashMap<Integer, DistrictPortfolioConfig>();
			        	  Map<Integer, DistrictPortfolioConfig> mapDM=new HashMap<Integer, DistrictPortfolioConfig>();
			        	  Map<Integer, DistrictPortfolioConfig> mapJC=new HashMap<Integer, DistrictPortfolioConfig>();
			        	  
			        	  List<DistrictPortfolioConfig> districtPortfolioConfigs=new ArrayList<DistrictPortfolioConfig>();
			        	  
			        	  districtPortfolioConfigs=districtPortfolioConfigDAO.getPortfolioConfig(isAffilated,jobOrder);
			        	  System.out.println("districtPortfolioConfigs Size "+districtPortfolioConfigs.size());
			        		if(districtPortfolioConfigs!=null && districtPortfolioConfigs.size()>0)
			        		{
			        			for(DistrictPortfolioConfig dPC:districtPortfolioConfigs)
			        			{
			        				if(dPC!=null)
			        				{
			        					if(dPC.getHeadQuarterMaster()!=null && dPC.getBranchMaster()!=null && dPC.getJobCategoryMaster()!=null)
			        					{
			        						if(jobSubCateFlag && jobSubCatedspqFlag){
			        							if(jobOrder.getJobCategoryMaster().getParentJobCategoryId().getJobCategoryId()==dPC.getJobCategoryMaster().getJobCategoryId()){
			        								mapJC.put(dPC.getJobCategoryMaster().getJobCategoryId(), dPC);
			        							}
			        						}else{
			        							mapJC.put(dPC.getJobCategoryMaster().getJobCategoryId(), dPC);
			        						}
			        						System.out.println("Put JC");
			        					}
			        					else if(dPC.getHeadQuarterMaster()!=null && dPC.getBranchMaster()!=null && dPC.getDistrictMaster()!=null && dPC.getJobCategoryMaster()==null)
			        					{
			        						mapDM.put(dPC.getDistrictMaster().getDistrictId(), dPC);
			        						System.out.println("Put DM");
			        					}
			        					else if(dPC.getHeadQuarterMaster()!=null && dPC.getBranchMaster()!=null && dPC.getDistrictMaster()==null && dPC.getJobCategoryMaster()==null)
			        					{
			        						mapBM.put(dPC.getBranchMaster().getBranchId(), dPC);
			        						System.out.println("Put BM");
			        					}
			        					else if(dPC.getHeadQuarterMaster()!=null	 && dPC.getBranchMaster()==null && dPC.getDistrictMaster()==null && dPC.getJobCategoryMaster()==null)
			        					{
			        						mapHQ.put(dPC.getHeadQuarterMaster().getHeadQuarterId(), dPC);
			        						System.out.println("Put HQ");
			        					} // Start for old business rule
			        					else if(dPC.getHeadQuarterMaster()==null && dPC.getBranchMaster()==null && dPC.getDistrictMaster()!=null && dPC.getJobCategoryMaster()==null)
			        					{
			        						mapDM.put(dPC.getDistrictMaster().getDistrictId(), dPC);
			        						System.out.println("Put Old DM");
			        					}
			        					else if(dPC.getHeadQuarterMaster()==null && dPC.getBranchMaster()==null && dPC.getDistrictMaster()!=null && dPC.getJobCategoryMaster()!=null)
			        					{
			        						System.out.println(" 0045 01");
			        						if(jobSubCateFlag && jobSubCatedspqFlag){
			        							if(jobOrder.getJobCategoryMaster().getParentJobCategoryId().getJobCategoryId()==dPC.getJobCategoryMaster().getJobCategoryId()){
			        								mapJC.put(dPC.getJobCategoryMaster().getJobCategoryId(), dPC);
			        							}	
			        						}else{
			        							System.out.println(" 0045 03");
			        							mapJC.put(dPC.getJobCategoryMaster().getJobCategoryId(), dPC);
			        						}
			        						//mapJC.put(dPC.getJobCategoryMaster().getJobCategoryId(), dPC);
			        						System.out.println("Put Old JC"+mapJC);
			        					}
			        				}
			        			}
			        		}
			        		
			        		for(Map.Entry<Integer, DistrictPortfolioConfig> entry : mapJC.entrySet())
							{
			        		System.out.println(entry.getValue().getDistrictPortfolioConfigId() +" :: >>>>>>>>>>>>>> :: "+entry.getKey());	
							}
			        		
			        		if(jobCategoryMaster!=null)
			        		{
			        			System.out.println("get DSPQ Jc");
			        			if(jobSubCateFlag && jobSubCatedspqFlag){
			        				if(mapJC!=null && mapJC.get(jobOrder.getJobCategoryMaster().getParentJobCategoryId().getJobCategoryId())!=null){
				        				districtPortfolioConfig=mapJC.get(jobOrder.getJobCategoryMaster().getParentJobCategoryId().getJobCategoryId());
			        				}
			        			}else{
			        				System.out.println("00019");
			        				if(mapJC!=null && mapJC.get(jobOrder.getJobCategoryMaster().getJobCategoryId())!=null)
				        				districtPortfolioConfig=mapJC.get(jobOrder.getJobCategoryMaster().getJobCategoryId());
			        			}
			        		}
			        		if(districtPortfolioConfig==null && districtMaster!=null)
			        		{
			        			System.out.println("get DSPQ DM");
			        			if(mapDM!=null && mapDM.get(districtMaster.getDistrictId())!=null)
			        				districtPortfolioConfig=mapDM.get(districtMaster.getDistrictId());
			        		}
			        		if(districtPortfolioConfig==null && branchMaster!=null)
			        		{
			        			System.out.println("get DSPQ BM");
			        			if(mapBM!=null && mapBM.get(branchMaster.getBranchId())!=null)
			        				districtPortfolioConfig=mapBM.get(branchMaster.getBranchId());
			        			else
			        				System.out.println("Not in BM");
			        		}
			        		if(districtPortfolioConfig==null && headQuarterMaster!=null)
			        		{
			        			System.out.println("get DSPQ HQ");
			        			if(mapHQ!=null && mapHQ.get(headQuarterMaster.getHeadQuarterId())!=null)
			        				districtPortfolioConfig=mapHQ.get(headQuarterMaster.getHeadQuarterId());
			        			else
			        				System.out.println("Not in HQ");
			        		}
			        	/*}*/
						
						WebContext context;
						context = WebContextFactory.get();
						HttpServletRequest request = context.getHttpServletRequest();
						HttpSession session = request.getSession(false);
						
						boolean formeremployee=false;

						if(session != null && (session.getAttribute("isAlsoSA")!=null))
						{
							isAlsoSA=(Boolean)session.getAttribute("isAlsoSA");
						}
						if(districtPortfolioConfig!=null)
						{
							districtPortfolioConfig.setIsPrinciplePhiladelphia(false);
							districtPortfolioConfig.setJobTitle(jobOrder.getJobTitle());

							if(districtPortfolioConfig.getFormeremployee()!=null && districtPortfolioConfig.getFormeremployee())
								formeremployee=true;
							else
								formeremployee=false;
							
							try
					          {
								
								if ((districtPortfolioConfig != null) && (districtPortfolioConfig.getJobCategoryMaster()!= null)) {
				            		districtPortfolioConfig.setJobCategoryName(districtPortfolioConfig.getJobCategoryMaster().getJobCategoryName());
								}
								
					            if ((jobOrder != null) && (districtMaster!=null && districtMaster.getDistrictId().equals(Integer.valueOf(4218990)))) {
					            	
					            	if ((jobOrder.getJobCategoryMaster()!= null) && (jobOrder.getJobCategoryMaster().getJobCategoryName().contains("School Support"))) {
						            	  isSchoolSupportPhiladelphia = true;						            	 
						              }else if ((jobOrder.getJobCategoryMaster()!= null) && (!jobOrder.getJobCategoryMaster().getJobCategoryName().equals("Teacher"))) {
						            	  isNonTeacher = true;
					              }else  if ((jobOrder != null) && (districtMaster!= null && districtMaster.getDistrictId().equals(Integer.valueOf(3904380)))) {				      
						            	if ((jobOrder.getJobCategoryMaster()!= null) && (!jobOrder.getJobCategoryMaster().getJobCategoryName().equals("Certified Teacher"))) {
							            	  isNonTeacher = true;
						              }		              
						              
						            }else {
					                isNonTeacher = false;
					                isSchoolSupportPhiladelphia = false;
					              }			              
					            	if ((jobOrder.getJobCategoryMaster()!= null) && (jobOrder.getJobCategoryMaster().getJobCategoryName().contains("Principal"))) {
					            		districtPortfolioConfig.setJobCategoryName(jobOrder.getJobCategoryMaster().getJobCategoryName());
					            		districtPortfolioConfig.setIsPrinciplePhiladelphia(isAlsoSA);
					            		System.out.println("isAffilated    "+isAffilated);
					            		if(isAffilated.equalsIgnoreCase("I")){
					            			districtPortfolioConfig.setIsPrinciplePhiladelphia(true);
					            		}
						              }
					            }
					          }
					          catch (Exception e)
					          {
					            e.printStackTrace();
					          }
					          if(districtMaster!=null && districtMaster.getDistrictId()==7800040  && isAffilated.equalsIgnoreCase("I")){
					        	  districtPortfolioConfig.setDistrictSpecificPortfolioQuestions(false);					        	  
					          }
							districtPortfolioConfig.setIsNonTeacher(Boolean.valueOf(isNonTeacher));
							districtPortfolioConfig.setIsSchoolSupportPhiladelphia(Boolean.valueOf(isSchoolSupportPhiladelphia));
						}
						session.setAttribute("isCurrentEmploymentNeeded",formeremployee);
						PrintOnConsole.debugPrintln("External isCurrentEmploymentNeeded "+formeremployee);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally
		{
			return districtPortfolioConfig;
		}
	}

	@Transactional(readOnly=false)
	public void savePortfolioItem(Integer districtId,Integer itemId,Integer quantity){
		try{
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(msgYrSesstionExp);
			}

			UserMaster userMaster = (UserMaster) session.getAttribute("userMaster");
			PortfolioItem portfolioItem=portfolioitemDAO.findById(itemId, false, false);
			String portfolioItemName=null;
			if(portfolioItem!=null){
				portfolioItemName=portfolioItem.getPortfolioItemName();
			}

			DistrictMaster districtMaster=districtMasterDAO.findById(districtId, false, false);
			if(districtMaster!=null){
				Criterion criterion=Restrictions.eq("districtMaster",districtMaster);
				List<DistrictPortfolioConfig>districtPortfolioConfigs=null;
				districtPortfolioConfigs=districtPortfolioConfigDAO.findByCriteria(criterion);
				DistrictPortfolioConfig districtPortfolioConfig=null;
				if(districtPortfolioConfigs!=null&& districtPortfolioConfigs.size()>0){
					districtPortfolioConfig=districtPortfolioConfigs.get(0);
					if(portfolioItemName.equals("Cover Letter")){
						districtPortfolioConfig.setCoverLetter(true);	
					}
					if(portfolioItemName.equals("Academic")){
						districtPortfolioConfig.setAcademic(quantity);	
					}
					if(portfolioItemName.equals("Academic Transcript")){
						districtPortfolioConfig.setAcademicTranscript(quantity);	
					}
					if(portfolioItemName.equals("Certification")){
						districtPortfolioConfig.setCertification(quantity);;	
					}
					if(portfolioItemName.equals("ProofOfCertification")){
						districtPortfolioConfig.setProofOfCertification(quantity);
					}
					if(portfolioItemName.equals("Reference")){
						districtPortfolioConfig.setReference(quantity);
					}
					if(portfolioItemName.equals("ReferenceLettersOfRecommentation")){
						districtPortfolioConfig.setReferenceLettersOfRecommendation(quantity);
					}

					if(portfolioItemName.equals("Resume")){
						districtPortfolioConfig.setResume(true);	
					}
				}else{
					districtPortfolioConfig=new DistrictPortfolioConfig();
					districtPortfolioConfig.setDistrictMaster(districtMaster);
					if(portfolioItemName.equals("Cover Letter")){
						districtPortfolioConfig.setCoverLetter(true);	
					}else{
						districtPortfolioConfig.setCoverLetter(false);
					}

					if(portfolioItemName.equals("Academic")){
						districtPortfolioConfig.setAcademic(quantity);	
					}else{
						districtPortfolioConfig.setAcademic(0);
					}

					if(portfolioItemName.equals("Academic Transcript")){
						districtPortfolioConfig.setAcademicTranscript(quantity);	
					}else{
						districtPortfolioConfig.setAcademicTranscript(0);
					}
					if(portfolioItemName.equals("Certification")){
						districtPortfolioConfig.setCertification(quantity);;	
					}else{
						districtPortfolioConfig.setCertification(0);;
					}
					if(portfolioItemName.equals("ProofOfCertification")){
						districtPortfolioConfig.setProofOfCertification(quantity);
					}else{
						districtPortfolioConfig.setProofOfCertification(0);
					}
					if(portfolioItemName.equals("Reference")){
						districtPortfolioConfig.setReference(quantity);
					}else{
						districtPortfolioConfig.setReference(0);
					}
					if(portfolioItemName.equals("ReferenceLettersOfRecommentation")){
						districtPortfolioConfig.setReferenceLettersOfRecommendation(quantity);
					}else{
						districtPortfolioConfig.setReferenceLettersOfRecommendation(quantity);
					}

					if(portfolioItemName.equals("Resume")){
						districtPortfolioConfig.setResume(true);	
					}else{
						districtPortfolioConfig.setResume(false);
					}
					districtPortfolioConfig.setCreatedDateTime(new Date());
					districtPortfolioConfig.setUserMaster(userMaster);
				}
				districtPortfolioConfigDAO.makePersistent(districtPortfolioConfig);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getPortfolioItemList(Integer districtId){
		StringBuffer dmRecords=new StringBuffer();
		try{
			DistrictMaster districtMaster=null;
			if(districtId!=null){
				districtMaster=districtMasterDAO.findById(districtId, false, false);
			}
			dmRecords.append("<table  id='itemTable' width='100%' border='0' >");
			dmRecords.append("<thead class='bg'>");
			dmRecords.append("<tr>");
			String responseText="";
			responseText=PaginationAndSorting.responseLink("Item");
			dmRecords.append("<th width='20%' valign='top'>"+responseText+"</th>");

			responseText=PaginationAndSorting.responseLink("Quantity");
			dmRecords.append("<th width='20%' valign='top'>"+responseText+"</th>");

			dmRecords.append("<th width='15%' valign='center'>"+lblAct+"</th>");
			dmRecords.append("</tr>");
			dmRecords.append("</thead>");
			List<DistrictPortfolioConfig>districtPortfolioConfigs=null;
			if(districtMaster!=null){
				Criterion criterion=Restrictions.eq("districtMaster", districtMaster);
				districtPortfolioConfigs=districtPortfolioConfigDAO.findByCriteria(criterion);
			}
			DistrictPortfolioConfig districtPortfolioConfig=null;
			if(districtPortfolioConfigs!=null && districtPortfolioConfigs.size()>0){
				districtPortfolioConfig=districtPortfolioConfigs.get(0);
			}
			if(districtPortfolioConfig!=null){

				if(districtPortfolioConfig.getAcademic()!=0){
					dmRecords.append("<tr><td>"+lblAcademic+"</td><td>"+districtPortfolioConfig.getAcademic()+"</td>");
					dmRecords.append("<td>");
					dmRecords.append("<a href='javascript:void(0);' onclick=\"return editPortfolioItem('"+districtPortfolioConfig.getDistrictPortfolioConfigId()+"','Academic')\">"+lblEdit+"</a>&nbsp;|&nbsp;");
					dmRecords.append("<a href='javascript:void(0);' onclick=\"return deletePortfolioItem('"+districtPortfolioConfig.getDistrictPortfolioConfigId()+"','Academic')\">"+lnkDlt+"");
					dmRecords.append("</td>");
					dmRecords.append("<tr>");
				}

				if(districtPortfolioConfig.getAcademicTranscript()!=0){
					dmRecords.append("<tr><td>"+lblAcademicTranscript+"</td><td>"+districtPortfolioConfig.getAcademicTranscript()+"</td>");
					dmRecords.append("<td>");
					dmRecords.append("<a href='javascript:void(0);' onclick=\"return editPortfolioItem('"+districtPortfolioConfig.getDistrictPortfolioConfigId()+"','Academic Transcript')\">"+lblEdit+"</a>&nbsp;|&nbsp;");
					dmRecords.append("<a href='javascript:void(0);' onclick=\"return deletePortfolioItem('"+districtPortfolioConfig.getDistrictPortfolioConfigId()+"','Academic Transcript')\">"+lnkDlt+"");
					dmRecords.append("</td>");
					dmRecords.append("<tr>");
				}

				int cvl;
				if(districtPortfolioConfig.getCoverLetter()){
					cvl=1;
				}else{
					cvl=0;
				}

				if(cvl!=0){
					dmRecords.append("<tr><td>"+headCoverLetr+"</td><td>"+cvl+"</td>");
					dmRecords.append("<td>");
					dmRecords.append("<a href='javascript:void(0);' onclick=\"return editPortfolioItem('"+districtPortfolioConfig.getDistrictPortfolioConfigId()+"','Cover Letter')\">"+lblEdit+"</a>&nbsp;|&nbsp;");
					dmRecords.append("<a href='javascript:void(0);' onclick=\"return deletePortfolioItem('"+districtPortfolioConfig.getDistrictPortfolioConfigId()+"','Cover Letter')\">"+lnkDlt+"");
					dmRecords.append("</td>");
					dmRecords.append("<tr>");
				}

				if(districtPortfolioConfig.getCertification()!=0){
					dmRecords.append("<tr><td>"+lblCertification+"</td><td>"+districtPortfolioConfig.getCertification()+"</td>");
					dmRecords.append("<td>");
					dmRecords.append("<a href='javascript:void(0);' onclick=\"return editPortfolioItem('"+districtPortfolioConfig.getDistrictPortfolioConfigId()+"','Certification')\">"+lblEdit+"</a>&nbsp;|&nbsp;");
					dmRecords.append("<a href='javascript:void(0);' onclick=\"return deletePortfolioItem('"+districtPortfolioConfig.getDistrictPortfolioConfigId()+"','Certification')\">"+lnkDlt+"");
					dmRecords.append("</td>");
					dmRecords.append("<tr>");
				}

				if(districtPortfolioConfig.getProofOfCertification()!=0){
					dmRecords.append("<tr><td>"+lblProofOfCertification+"</td><td>"+districtPortfolioConfig.getProofOfCertification()+"</td>");
					dmRecords.append("<td>");
					dmRecords.append("<a href='javascript:void(0);' onclick=\"return editPortfolioItem('"+districtPortfolioConfig.getDistrictPortfolioConfigId()+"','ProofOfCertification')\">"+lblEdit+"</a>&nbsp;|&nbsp;");
					dmRecords.append("<a href='javascript:void(0);' onclick=\"return deletePortfolioItem('"+districtPortfolioConfig.getDistrictPortfolioConfigId()+"','ProofOfCertification')\">"+lnkDlt+"");
					dmRecords.append("</td>");
					dmRecords.append("<tr>");
				}

				if(districtPortfolioConfig.getReference()!=0){
					dmRecords.append("<tr><td>"+lblReference+"</td><td>"+districtPortfolioConfig.getReference()+"</td>");
					dmRecords.append("<td>");
					dmRecords.append("<a href='javascript:void(0);' onclick=\"return editPortfolioItem('"+districtPortfolioConfig.getDistrictPortfolioConfigId()+"','Reference')\">"+lblEdit+"</a>&nbsp;|&nbsp;");
					dmRecords.append("<a href='javascript:void(0);' onclick=\"return deletePortfolioItem('"+districtPortfolioConfig.getDistrictPortfolioConfigId()+"','Reference')\">"+lnkDlt+"");
					dmRecords.append("</td>");
					dmRecords.append("<tr>");
				}

				if(districtPortfolioConfig.getReferenceLettersOfRecommendation()!=0){
					dmRecords.append("<tr><td>"+lblReferenceLettersOfRecommentation+"</td><td>"+districtPortfolioConfig.getReferenceLettersOfRecommendation()+"</td>");
					dmRecords.append("<td>");
					dmRecords.append("<a href='javascript:void(0);' onclick=\"return editPortfolioItem('"+districtPortfolioConfig.getDistrictPortfolioConfigId()+"','ReferenceLettersOfRecommentation')\">"+lblEdit+"</a>&nbsp;|&nbsp;");
					dmRecords.append("<a href='javascript:void(0);' onclick=\"return deletePortfolioItem('"+districtPortfolioConfig.getDistrictPortfolioConfigId()+"','ReferenceLettersOfRecommentation')\">"+lnkDlt+"");
					dmRecords.append("</td>");
					dmRecords.append("<tr>");
				}

				int rsm;
				if(districtPortfolioConfig.getResume()){
					rsm=1;
				}else{
					rsm=0;
				}
				if(rsm!=0){
					dmRecords.append("<tr><td>Resume</td><td>"+rsm+"</td>");
					dmRecords.append("<td>");
					dmRecords.append("<a href='javascript:void(0);' onclick=\"return editPortfolioItem('"+districtPortfolioConfig.getDistrictPortfolioConfigId()+"','Resume')\">"+lblEdit+"</a>&nbsp;|&nbsp;");
					dmRecords.append("<a href='javascript:void(0);' onclick=\"return deletePortfolioItem('"+districtPortfolioConfig.getDistrictPortfolioConfigId()+"','Resume')\">"+lnkDlt+"");
					dmRecords.append("</td>");
					dmRecords.append("<tr>");
				}

			}else{
				dmRecords.append("<tr><td colspan='6'>"+msgNorecordfound+"</td></tr>" );
			}
			dmRecords.append("</table>");

		}catch (Exception e) {
			e.printStackTrace();
		}
		return dmRecords.toString();
	}

	@Transactional(readOnly=false)
	public DistrictPortfolioConfig getPortfolioItemStatus(Integer districtportfolioconfigId){
		DistrictPortfolioConfig districtPortfolioConfig=null;
		try{
			if(districtportfolioconfigId!=null){
				districtPortfolioConfig=districtPortfolioConfigDAO.findById(districtportfolioconfigId, false, false);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return districtPortfolioConfig;
	}
	@Transactional(readOnly=false)
	public Integer deletePortfolioItem(Integer districtportfolioconfigId,String portfolioItemName){
		DistrictPortfolioConfig districtPortfolioConfig=null;
		try{
			if(districtportfolioconfigId!=null){
				districtPortfolioConfig=districtPortfolioConfigDAO.findById(districtportfolioconfigId, false, false);
			}
			if(districtPortfolioConfig!=null){
				if(portfolioItemName.equals("Cover Letter")){
					districtPortfolioConfig.setCoverLetter(false);
				}

				if(portfolioItemName.equals("Academic")){
					districtPortfolioConfig.setAcademic(0);
				}

				if(portfolioItemName.equals("Academic Transcript")){
					districtPortfolioConfig.setAcademicTranscript(0);
				}
				if(portfolioItemName.equals("Certification")){
					districtPortfolioConfig.setCertification(0);;
				}
				if(portfolioItemName.equals("ProofOfCertification")){
					districtPortfolioConfig.setProofOfCertification(0);
				}
				if(portfolioItemName.equals("Reference")){
					districtPortfolioConfig.setReference(0);
				}
				if(portfolioItemName.equals("ReferenceLettersOfRecommentation")){
					districtPortfolioConfig.setReferenceLettersOfRecommendation(0);
				}
				if(portfolioItemName.equals("Resume")){
					districtPortfolioConfig.setResume(false);
				}
				return 1;
			}else{
				return 0;
			}

		}catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

	public boolean callTFA()
	{
		boolean bReturnValue=false;
		TeacherDetail teacherDetail=null;

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		else
			teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");

		try 
		{
			if(teacherDetail!=null)
			{
				TeacherExperience teacherExperience = teacherExperienceDAO.findTeacherExperienceByTeacher(teacherDetail);
				if(teacherExperience!=null)
					if(teacherExperience.getTfaAffiliateMaster()!=null && teacherExperience.getTfaAffiliateMaster().getTfaAffiliateId() > 0)
						bReturnValue=true;
			}

		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		finally
		{
			return bReturnValue;
		}

	}



	public int[] validatePortfolio()
	{
		System.out.println(" inside validatePortfolio +++++++++++++++++++++++================");

		int[] intReturnArray = new int[34];
		TeacherDetail teacherDetail=null;

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		else
			teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");

		try 
		{
			if(teacherDetail!=null)
			{
				//********************* 01 countAcademic *********************
				int countAcademic=0;
				List<TeacherAcademics> teacherAcademics=new ArrayList<TeacherAcademics>();
				teacherAcademics =teacherAcademicsDAO.findAcadamicDetailByTeacher(teacherDetail);
				if(teacherAcademics!=null && teacherAcademics.size() >0)
					countAcademic=teacherAcademics.size();
				intReturnArray[0]=countAcademic;

				//********************* 02 count AcademicTranscript *********************
				int countAcademicTranscript=0;
				List<TeacherAcademics> teacherAcademicsAttachment=new ArrayList<TeacherAcademics>();
				teacherAcademicsAttachment =teacherAcademicsDAO.findTranscript_Null(teacherDetail);
				countAcademicTranscript=teacherAcademicsAttachment.size();
				intReturnArray[1]=countAcademicTranscript;

				List<TeacherAcademics> teacherAcademicsBType=teacherAcademicsDAO.findAcadamicDetailByTeacherWithBType(teacherDetail);
				if(teacherAcademicsBType.size()>0){
					intReturnArray[26]=1;
				}else{
					intReturnArray[26]=0;
				}

				//********************* 03 count Certification *********************
				int countCertification=0;
				List<TeacherCertificate> lstCertificate=new ArrayList<TeacherCertificate>();
				lstCertificate =teacherCertificateDAO.findCertificateByTeacher(teacherDetail);
				countCertification=lstCertificate.size();
				intReturnArray[2]=countCertification;
				intReturnArray[31] = teacherCertificateDAO.findIEINByTeacher(teacherDetail);
				//********************* 04 count ProofOfCertification *********************
				
				
				CertificationStatusMaster certificationStatusMaster1=certificationStatusMasterDAO.findById(3, false, false);
				CertificationStatusMaster certificationStatusMaster2=certificationStatusMasterDAO.findById(5, false, false);
				
				List<CertificationStatusMaster> lstCertificationStatusMaster=new ArrayList<CertificationStatusMaster>();
				lstCertificationStatusMaster.add(certificationStatusMaster1);
				lstCertificationStatusMaster.add(certificationStatusMaster2);
				
				int countProofOfCertification=0;
				List<TeacherCertificate> teacherCertificates=new ArrayList<TeacherCertificate>();
				teacherCertificates =teacherCertificateDAO.findAttachPathOfCertificationWithNoPlanning(teacherDetail,lstCertificationStatusMaster);
				countProofOfCertification=teacherCertificates.size();
				intReturnArray[3]=countProofOfCertification;

				//********************* 05 count Reference *********************
				int countReference=0;
				List<TeacherElectronicReferences> lstTeacherElectronicReferences = new ArrayList<TeacherElectronicReferences>();
			 
				if(Utility.isNC())
				lstTeacherElectronicReferences =teacherElectronicReferencesDAO.findActiveRdcontactedReferencesByTeacher(teacherDetail);
                 else
                	 lstTeacherElectronicReferences =teacherElectronicReferencesDAO.findActiveReferencesByTeacher(teacherDetail);
				countReference=lstTeacherElectronicReferences.size();
				intReturnArray[4]=countReference;

				//********************* 06 count ReferenceLettersOfRecommendation *********************
				int countReferenceLettersOfRecommendation=0;
				List<TeacherElectronicReferences> lstTeacherElectronicReferencesAttachment = new ArrayList<TeacherElectronicReferences>();
				lstTeacherElectronicReferencesAttachment =teacherElectronicReferencesDAO.findActiveReferencesByPathOfReference(teacherDetail);
				countReferenceLettersOfRecommendation=lstTeacherElectronicReferencesAttachment.size();
				intReturnArray[5]=countReferenceLettersOfRecommendation;

				//********************* 07 call Resume and 08 call TFA and 09 Call willingAsSubstituteTeacher *********************
				int countResume=0;
				int countTFA=0;
				int countWillingAsSubstituteTeacher=0;
				TeacherExperience teacherExperience = teacherExperienceDAO.findTeacherExperienceByTeacher(teacherDetail);
				if(teacherExperience!=null)
				{
					if(teacherExperience.getResume()!=null && !teacherExperience.getResume().equalsIgnoreCase(""))
					{
						countResume=1;
						intReturnArray[6]=countResume;
					}
					if(teacherExperience.getTfaAffiliateMaster()!=null && teacherExperience.getTfaAffiliateMaster().getTfaAffiliateId() > 0)
					{
						countTFA=1;
						intReturnArray[7]=countTFA;
					}
					if(teacherExperience.getCanServeAsSubTeacher()!=null &&  ( teacherExperience.getCanServeAsSubTeacher()==0 || teacherExperience.getCanServeAsSubTeacher() == 1))
					{
						countWillingAsSubstituteTeacher=1;
						intReturnArray[8]=countWillingAsSubstituteTeacher;
					}
				}


				//********************* 10 Phone Number *********************
				/*int phoneNumber=0;
				if(teacherDetail.getPhoneNumber()!=null && teacherDetail.getPhoneNumber().trim().length() > 0)
				{
					phoneNumber=1;
					intReturnArray[9]=phoneNumber;
				}*/

				//************************** 11 Reference Contact No. Check
				boolean contactCheck = false;
				for(TeacherElectronicReferences teacherElectronicReferences: lstTeacherElectronicReferences)
				{
					if(teacherElectronicReferences.getContactnumber()==null || teacherElectronicReferences.getContactnumber().trim().length()==0)
						contactCheck = true;
				}
				if(contactCheck)
					intReturnArray[10]=1;


				//********************* 12 Address *********************
				TeacherPersonalInfo teacherPersonalInfo = teacherPersonalInfoDAO.findById(teacherDetail.getTeacherId(), false, false);
				TeacherPortfolioStatus teacherPortfolioStatus = teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);

				TeacherGeneralKnowledgeExam teacherGeneralKnowledgeExam = teacherGeneralKnowledgeExamDAO.findTeacherGeneralKnowledgeExam(teacherDetail);
				List<TeacherSubjectAreaExam> teacherSubjectAreaExamLst = teacherSubjectAreaExamDAO.findTeacherSubjectAreaExamLst(teacherDetail);
				TeacherAdditionalDocuments teacherAdditionalDocuments=teacherAdditionalDocumentsDAO.findTeacherAdditionalDocuments(teacherDetail);


				if(teacherPersonalInfo!=null)
				{
					boolean addressFlag=true;
					boolean personalInfoFlag=true;

					if(teacherPersonalInfo.getAddressLine1()!=null && teacherPersonalInfo.getAddressLine1().trim().length() > 0)
						addressFlag=false;
					if(teacherPersonalInfo.getZipCode()!=null && teacherPersonalInfo.getZipCode().trim().length() > 0)
						addressFlag=false;

					if(teacherPersonalInfo.getCountryId()!=null)
					{
						CountryMaster countryMaster2 = countryMasterDAO.findById(teacherPersonalInfo.getCountryId().getCountryId(), false, false);
						List<StateMaster> lstState = stateMasterDAO.findActiveStateByCountryId(countryMaster2);
						
						if(teacherPersonalInfo.getCountryId().getCountryId()==223)
						{
							if(teacherPersonalInfo.getStateId()!=null)
								addressFlag=false;
							if(teacherPersonalInfo.getCityId()!=null)
								addressFlag=false;
						}
						else
						{
							if(lstState!=null && lstState.size()>0)
							{
								if(teacherPersonalInfo.getStateId()!=null)
									addressFlag=false;
								if(teacherPersonalInfo.getOtherCity()!=null)
									addressFlag=false;
							}
							else
							{
								if(teacherPersonalInfo.getOtherState()!=null)
									addressFlag=false;
								if(teacherPersonalInfo.getOtherCity()!=null)
									addressFlag=false;
							}
						}
					}
					else
					{
						addressFlag=false;
					}


					if(addressFlag)
					{
						intReturnArray[11]=1;
					}

					if(teacherPersonalInfo.getFirstName()!=null && !teacherPersonalInfo.getFirstName().equalsIgnoreCase(""))
						personalInfoFlag=false;
					if(teacherPersonalInfo.getLastName()!=null && !teacherPersonalInfo.getLastName().equalsIgnoreCase(""))	
						personalInfoFlag=false;
					if(teacherPersonalInfo.getDob()!=null && !teacherPersonalInfo.getDob().equals(""))
						personalInfoFlag=false;

					if(personalInfoFlag)
						intReturnArray[15]=1;
					else
						intReturnArray[15]=0;

					/*if(teacherPersonalInfo.getFirstName()!=null && !teacherPersonalInfo.getFirstName().equalsIgnoreCase("") && teacherPersonalInfo.getLastName()!=null && !teacherPersonalInfo.getLastName().equalsIgnoreCase("") && teacherPersonalInfo.getDob()!=null)
					{
						intReturnArray[15]=1;
					}
					else
					{
						intReturnArray[15]=0;
					}*/

					if(teacherPersonalInfo.getSSN()!=null && !teacherPersonalInfo.getSSN().equalsIgnoreCase(""))
					{
						String sTempSSN=Utility.decodeBase64(teacherPersonalInfo.getSSN());
						if(sTempSSN!=null && !sTempSSN.equalsIgnoreCase("") && sTempSSN.length()==9)
							intReturnArray[16]=0;
						else
							intReturnArray[16]=1;
						//PrintOnConsole.debugPrintln("Yes SSN "+teacherPersonalInfo.getSSN() +" Length "+teacherPersonalInfo.getSSN().length());
					}
					else
					{
						intReturnArray[16]=1;
						//PrintOnConsole.debugPrintln("No SSN "+teacherPersonalInfo.getSSN() +" Length "+teacherPersonalInfo.getSSN().length());
					}

					if(teacherPersonalInfo.getIsVateran()!=null && !teacherPersonalInfo.getIsVateran().equals(""))
					{
						intReturnArray[22]=1;
					}
					else
					{
						intReturnArray[22]=0;
					}

					try{
						if(teacherPersonalInfo.getEthnicOriginId()!=null && teacherPersonalInfo.getEthnicOriginId().getEthnicOriginId()>0)
						{
							intReturnArray[23]=1;
						}
						else
						{
							intReturnArray[23]=0;
						}
					}catch(Exception e){
						intReturnArray[23]=0;
					}

					try{
						if(teacherPersonalInfo.getEthnicityId()!=null && teacherPersonalInfo.getEthnicityId().getEthnicityId()>0)
						{
							intReturnArray[24]=1;
						}
						else
						{
							intReturnArray[24]=0;
						}
					}catch(Exception e){
						intReturnArray[24]=0;
					}

					//********************************WOrk Experience***********************
					int countEmploymwnt=0;
					List<TeacherRole> lstTeacherRole = new ArrayList<TeacherRole>();
					lstTeacherRole = teacherRoleDAO.findTeacherRoleByTeacher(teacherDetail);
					countEmploymwnt=lstTeacherRole.size();
					intReturnArray[25]=countEmploymwnt;

					//***********   Race  *************	

					if(teacherPersonalInfo.getRaceId()!=null && !teacherPersonalInfo.getRaceId().equals(""))
						intReturnArray[17]=1;
					else
						intReturnArray[17]=0;


					//*************** Gender ************************
					if(teacherPersonalInfo.getGenderId()!=null && teacherPersonalInfo.getGenderId().getGenderId()> 0)
					{
						intReturnArray[27]=1;
					}
					else
					{
						intReturnArray[27]=0;
					}

					//*************** Retirement Number ************************
					if(teacherPersonalInfo.getRetirementnumber()!=null && !teacherPersonalInfo.equals(""))
					{
						intReturnArray[28]=1;
					}
					else
					{
						intReturnArray[28]=0;
					}
					
					if(teacherPersonalInfo.getEmployeeType()==null)
					{
						intReturnArray[18]=1;
					}
					else if(teacherPersonalInfo.getEmployeeType()==1 &&  ( teacherPersonalInfo.getEmployeeNumber()!=null  || !teacherPersonalInfo.getEmployeeNumber().equalsIgnoreCase("")) )
					{
						intReturnArray[18]=1;
					}
					else
					{
						intReturnArray[18]=0;
					}

					// ************* Phone Number ************************
					int phoneNumber=0;
					if(teacherPersonalInfo.getPhoneNumber()!=null && teacherPersonalInfo.getPhoneNumber().trim().length() > 0)
					{
						phoneNumber=1;
						intReturnArray[9]=phoneNumber;
					}
				}

				// Experience
				if(teacherExperience!=null)
				{
					// For experience Teacher training
					if(teacherExperience.getExpCertTeacherTraining()==null)
					{
						intReturnArray[12]=1;
					}
					// National Bord Certifications
					if(teacherExperience.getNationalBoardCert()==null)
					{
						intReturnArray[13]=1;
					}
				}

				// For  Affidavit
				if(teacherPortfolioStatus!=null)
				{
					if(teacherPortfolioStatus.getIsAffidavitCompleted()!=null)
						if(teacherPortfolioStatus.getIsAffidavitCompleted()==true)
							intReturnArray[14]=1;
				}

				if(teacherGeneralKnowledgeExam!=null){
					intReturnArray[19]=1;
				}else{
					intReturnArray[19]=0;
				}

				System.out.println("teacherSubjectAreaExamLst.size() "+teacherSubjectAreaExamLst.size());

				if(teacherSubjectAreaExamLst.size() > 0){
					intReturnArray[20]=1;
				}else{
					intReturnArray[20]=0;
				}

				if(teacherAdditionalDocuments!=null){
					intReturnArray[21]=1;
				}else{
					intReturnArray[21]=0;
				}
				//*****************end ************
				//***********video count start *************
				
				intReturnArray[29]  = 	teacherVideoLinksDAO.findTeachByVideoLink(teacherDetail).size();
				float totalYear		=	0;
				Date currentDate 	= 	null;
				Date drugDate 		= 	null;
				intReturnArray[30]	=	0;
				try{
					currentDate	=	new Date();
					drugDate	=	teacherDetail.getDrugtestRejectDate();
					if(drugDate!=null)
						totalYear			=	yearBetween(currentDate,drugDate);
				} catch(Exception exception){
					exception.printStackTrace();
				}
				
				if(drugDate!= null && totalYear < 3)
					intReturnArray[30]=1;
					
				//********************* End *********************
				
				//********************* 32 count Involvment *********************
				int countInvolvment=0;
				List<TeacherInvolvement> lstInvolvment=new ArrayList<TeacherInvolvement>();
				lstInvolvment =teacherInvolvementDAO.findTeacherInvolvementByTeacher(teacherDetail);
				countInvolvment=lstInvolvment.size();
				intReturnArray[32]=countInvolvment;
				//********************* 32 count Involvment *********************
				
				//********************* 32 count Honor *********************
				int countHonor=0;
				List<TeacherHonor> lstHonor=new ArrayList<TeacherHonor>();
				lstHonor =teacherHonorDAO.findTeacherHonersByTeacher(teacherDetail);
				countHonor=lstInvolvment.size();
				intReturnArray[33]=countHonor;
				//********************* 32 count Honor *********************
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		finally
		{
			return intReturnArray;
		}
	}
	public String downloadJobDescription(Integer jobId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		String path="";

		try 
		{
			JobOrder jobOrder=null;
			if(jobId!=0){
				jobOrder=jobOrderDAO.findById(jobId, false, false);
			}
			String fileName= jobOrder.getPathOfJobDescription();

			String source=Utility.getValueOfPropByKey("districtRootPath")+jobOrder.getDistrictMaster().getDistrictId()+"/JobDescription/"+fileName;
			String target = context.getServletContext().getRealPath("/")+"/"+jobOrder.getDistrictMaster().getDistrictId()+"/JobDescription/";

			File sourceFile = new File(source);
			File targetDir = new File(target);
			if(!targetDir.exists())
				targetDir.mkdirs();

			File targetFile = new File(targetDir+"/"+sourceFile.getName());

			FileUtils.copyFile(sourceFile, targetFile);

			path = Utility.getValueOfPropByKey("contextBasePath")+jobOrder.getDistrictMaster().getDistrictId()+"/JobDescription/"+fileName;


		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return path;
	}

	public String updateCredentialsFlag(String name)
	{
		TeacherDetail teacherDetail = null;

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		else
			teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
		
		boolean bTeacherPortfolioStatus=false;
		TeacherPortfolioStatus teacherPortfolioStatus = null;
		try
		{
			teacherPortfolioStatus = teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);
			
			if(teacherPortfolioStatus==null)
			{
				bTeacherPortfolioStatus=true;
				teacherPortfolioStatus = new TeacherPortfolioStatus();
				teacherPortfolioStatus.setTeacherId(teacherDetail);
			}


			if(name!=null && name.equals("personal"))
			{
				TeacherPersonalInfo personalInfo = teacherPersonalInfoDAO.findTeacherPersonalInfoByTeacher(teacherDetail);
				if(personalInfo==null)
				{
					personalInfo.setFirstName(teacherDetail.getFirstName());
					personalInfo.setLastName(teacherDetail.getLastName());
					personalInfoDAO.makePersistent(personalInfo);
				}

				teacherPortfolioStatus.setIsPersonalInfoCompleted(true);

			}
			else if(name!=null && name.equals("credentials"))
			{
				teacherPortfolioStatus.setIsCertificationsCompleted(true);
			}
			else if(name!=null && name.equals("experiences"))
			{
				TeacherExperience	teacherExperience = teacherExperienceDAO.findTeacherExperienceByTeacher(teacherDetail);

				if(teacherExperience.getResume()!=null && !teacherExperience.getResume().equals(""))
				{
					teacherPortfolioStatus.setIsExperiencesCompleted(true);
				}
			}
			else if(name!=null && name.equals("affidavit"))
			{
				TeacherAffidavit teacherAffidavit = null;
				teacherAffidavit = teacherAffidavitDAO.findAffidavitByTeacher(teacherDetail);

				if(teacherAffidavit==null)
				{
					teacherAffidavit = new TeacherAffidavit();
					teacherAffidavit.setTeacherId(teacherDetail);
				}

				teacherAffidavit.setAffidavitAccepted(true);
				teacherAffidavit.setIsDone(true);
				teacherAffidavitDAO.makePersistent(teacherAffidavit);


				teacherPortfolioStatus.setIsAffidavitCompleted(true);
			}
			try{
				if(teacherPortfolioStatus.getIsPersonalInfoCompleted()==null){
					teacherPortfolioStatus.setIsPersonalInfoCompleted(false);
				}
			}catch(Exception e){
				teacherPortfolioStatus.setIsPersonalInfoCompleted(false);
				e.printStackTrace();
			}
			try{
				if(teacherPortfolioStatus.getIsCertificationsCompleted()==null){
					teacherPortfolioStatus.setIsCertificationsCompleted(false);
				}
			}catch(Exception e){
				teacherPortfolioStatus.setIsCertificationsCompleted(false);
				e.printStackTrace();
			}
			try{
				if(teacherPortfolioStatus.getIsExperiencesCompleted()==null){
					teacherPortfolioStatus.setIsExperiencesCompleted(false);
				}
			}catch(Exception e){
				teacherPortfolioStatus.setIsExperiencesCompleted(false);
				e.printStackTrace();
			}
			try{
				if(teacherPortfolioStatus.getIsAffidavitCompleted()==null){
					teacherPortfolioStatus.setIsAffidavitCompleted(false);
				}
			}catch(Exception e){
				teacherPortfolioStatus.setIsAffidavitCompleted(false);
				e.printStackTrace();
			}
			teacherPortfolioStatus.setIsAcademicsCompleted(true);
			
			TeacherPortfolioStatus tpsTemp=teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);
			if(bTeacherPortfolioStatus && tpsTemp==null)
				teacherPortfolioStatusDAO.makePersistent(teacherPortfolioStatus);
			else
			{
				teacherPortfolioStatus.setId(tpsTemp.getId());
				teacherPortfolioStatusDAO.makePersistent(teacherPortfolioStatus);
			}

		}catch(Exception e){
			e.printStackTrace();
		}

		return "success";
	}


	/*public TeacherDetail getTeacherDetails()
	{
		TeacherDetail teacherDetail=null;

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		else
			teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");

		try 
		{
			if(teacherDetail!=null)
			{
				teacherDetail=teacherDetailDAO.findById(teacherDetail.getTeacherId(), false, false);
			}

		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		finally
		{
			return teacherDetail;
		}

	}*/

	public String[] checkEmployeeDetail(Integer jobId)
	{
		TeacherDetail teacherDetail = null;

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		else
			teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
		String arr[] = new String[2];
		try
		{
			System.out.println("teacherDetail.getTeacherId(): "+teacherDetail.getTeacherId());
			JobOrder jobOrder = jobOrderDAO.findById(jobId, false, false);
			System.out.println("KKKKKKKKKKKKKKKKKKKKKKKKKKKK: "+jobId);
			if(!(jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId().equals(1200390)))
			{
				arr[0]="1";
				arr[1]="A";
				return arr;
			}
			System.out.println("jobId: "+jobOrder.getJobId());
			TeacherPersonalInfo teacherPersonalInfo = teacherPersonalInfoDAO.findTeacherPersonalInfoByTeacher(teacherDetail);
			String firstName = "";
			String lastName = "";
			String ssn = "";
			String employeeCode="";
			if(teacherPersonalInfo!=null)
			{
				ssn=teacherPersonalInfo.getSSN();
				employeeCode= teacherPersonalInfo.getEmployeeNumber();
				firstName = teacherPersonalInfo.getFirstName();
				lastName = teacherPersonalInfo.getLastName();

				//System.out.println("1 firstName "+firstName);

				//System.out.println("encoded: "+ssn);
				if(ssn!=null)
				{
					ssn = ssn.trim();
					ssn = Utility.decodeBase64(ssn);
					if(ssn.length()>4)
						ssn = ssn.substring(ssn.length() - 4, ssn.length());
				}
				//System.out.println("decoded: "+ssn);
			}else
			{
				ssn="";
				employeeCode="";
			}
			System.out.println("teacherPersonalInfo: "+teacherPersonalInfo);
			List<EmployeeMaster> employeeMasters =	employeeMasterDAO.checkEmployee(ssn, employeeCode,firstName,lastName,jobOrder.getDistrictMaster());

			String FECode=null; //Former Employee
			String RRCode=null; //Part Time
			String OCCode=null; //Full Time
			String PCCode=null; //Do not apply
			String staffType=null; // N - Non-Instructional Internal Employee, I - Instructional Internal Employee
			String jobType=jobOrder.getJobType()==null?"":jobOrder.getJobType().trim();
			System.out.println("jobType: "+jobOrder.getJobType());
			System.out.println("employeeMaster: "+employeeMasters.size());
			
			if(employeeMasters.size()>0)
			{
				//System.out.println("employeeMaster: "+employeeMasters.size());
				EmployeeMaster employeeMaster = employeeMasters.get(0);
				FECode=employeeMaster.getFECode()==null?"":employeeMaster.getFECode().trim();
				RRCode=employeeMaster.getRRCode()==null?"":employeeMaster.getRRCode().trim();
				OCCode=employeeMaster.getOCCode()==null?"":employeeMaster.getOCCode().trim();
				PCCode=employeeMaster.getPCCode()==null?"":employeeMaster.getPCCode().trim();
				staffType=employeeMaster.getStaffType()==null?"":employeeMaster.getStaffType().trim();
				Integer isCurrentFullTime = teacherPersonalInfo.getIsCurrentFullTimeTeacher()==null?0:teacherPersonalInfo.getIsCurrentFullTimeTeacher();
				// System.out.println("Pccod:::: "+PCCode);
				JobForTeacher jobForTeacher = null;
				jobForTeacher = jobForTeacherDAO.findJFTByTeacherAndJob(teacherDetail, jobOrder);
				System.out.println("jobForTeacher:::: "+jobForTeacher);
				String jobCategoryName =jobOrder.getJobCategoryMaster().getJobCategoryName();
				if(PCCode.equalsIgnoreCase("y"))
				{
					arr[0]="0";
					arr[1]="R";
					if(jobForTeacher!=null)
						jobForTeacherDAO.makeTransient(jobForTeacher);
					if(session.getAttribute("referer")!=null)
						session.removeAttribute("referer");
					return arr;
				}else if(!FECode.equalsIgnoreCase("0"))
				{
					System.out.println("Current Employee ============================FECode>>>"+FECode);
					if(staffType.equalsIgnoreCase("I") && isCurrentFullTime==1)
					{
						arr[0]="0";
						arr[1]="RD";
						if(jobForTeacher!=null && !jobCategoryName.equalsIgnoreCase("Aspiring Assistant Principal"))
							jobForTeacherDAO.makeTransient(jobForTeacher);
						if(session.getAttribute("referer")!=null && !jobCategoryName.equalsIgnoreCase("Aspiring Assistant Principal"))
							session.removeAttribute("referer");
					}else
					{
						System.out.println("Current Employee Non Institutional ================isCurrentFullTime:"+isCurrentFullTime);
						/*arr[0]="1";
						arr[1]="A";*/
						/////////For current employee/////////////////////////////////
						if(jobType.equalsIgnoreCase("p") || jobType.equalsIgnoreCase("f"))
						{
							if(RRCode.equals("") && OCCode.equals(""))
							{
								arr[0]="1";
								arr[1]="A";
								return arr;
							}else if(jobType.equalsIgnoreCase("p"))
							{
								if(OCCode.equalsIgnoreCase("y"))
								{
									arr[0]="0";
									arr[1]="P";
									if(jobForTeacher!=null)
										jobForTeacherDAO.makeTransient(jobForTeacher);

									if(session.getAttribute("referer")!=null)
										session.removeAttribute("referer");
								}else
								{
									arr[0]="1";
									arr[1]="A";
								}
								return arr;
							}else if(jobType.equalsIgnoreCase("f"))
							{
								if(OCCode.equalsIgnoreCase("y"))
								{
									arr[0]="1";
									arr[1]="A";
								}
								else
								{
									// RR allowed
									if(RRCode.equalsIgnoreCase("y"))
									{
										arr[0]="1";
										arr[1]="A";
									}else 
									{
										arr[0]="1";
										arr[1]="A";
									}/*else
									{
										arr[0]="0";
										arr[1]="F";
										if(jobForTeacher!=null)
											jobForTeacherDAO.makeTransient(jobForTeacher);

										if(session.getAttribute("referer")!=null)
											session.removeAttribute("referer");
									}*/
								}
								return arr;
							}
						}
					   ///For curenent Employee end//////////////////////////////////////////////////////
						
						
					}
					return arr;
				}
				else if(RRCode.equals("") && OCCode.equals(""))
				{
					arr[0]="1";
					arr[1]="A";
					return arr;
				}					 
				else if(jobType.equalsIgnoreCase("p") || jobType.equalsIgnoreCase("f"))
				{

					if(jobType.equalsIgnoreCase("p"))
					{
						if(OCCode.equalsIgnoreCase("y"))
						{
							arr[0]="0";
							arr[1]="P";
							if(jobForTeacher!=null)
								jobForTeacherDAO.makeTransient(jobForTeacher);

							if(session.getAttribute("referer")!=null)
								session.removeAttribute("referer");
						}else
						{
							arr[0]="1";
							arr[1]="A";
						}
						return arr;
					}else if(jobType.equalsIgnoreCase("f"))
					{
						if(OCCode.equalsIgnoreCase("y"))
						{
							arr[0]="1";
							arr[1]="A";
						}
						else
						{
							// RR allowed
							if(RRCode.equalsIgnoreCase("y"))
							{
								arr[0]="1";
								arr[1]="A";
							}else 
							{
								arr[0]="1";
								arr[1]="A";
							}/*else
							{
								arr[0]="0";
								arr[1]="F";
								if(jobForTeacher!=null)
									jobForTeacherDAO.makeTransient(jobForTeacher);

								if(session.getAttribute("referer")!=null)
									session.removeAttribute("referer");
							}*/
						}
						return arr;
					}
				}
			}

		}catch(Exception e){
			e.printStackTrace();
		}

		return arr;
	}

	public String notifyUsers(Integer jobId,String retVal)
	{
		TeacherDetail teacherDetail = null;

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		else
			teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
		System.out.println("retVal::::: "+retVal);
		//R Restricted (PC)
		if(retVal.equalsIgnoreCase("R"))
		{
			JobOrder jobOrder = jobOrderDAO.findById(jobId, false, false);
			String mailContent = MailText.getPCEmailBody(teacherDetail);
			DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
			System.out.println("mailContent: "+mailContent);
			dsmt.setEmailerService(emailerService);
			dsmt.setMailfrom("vishwanath@netsutra.com");
			dsmt.setMailto(teacherDetail.getEmailAddress());
			dsmt.setMailsubject("You are not allowed to apply job \""+jobOrder.getJobTitle()+"\"");
			dsmt.setMailcontent(mailContent);
			dsmt.start();
		}/*else if(retVal.equalsIgnoreCase("F"))
		{
			JobOrder jobOrder = jobOrderDAO.findById(jobId, false, false);
			String mailContent = MailText.getRestrictedEmailBody(teacherDetail);
			DemoScheduleMailThread dsmt = new DemoScheduleMailThread();

			dsmt.setEmailerService(emailerService);
			dsmt.setMailfrom("vishwanath@netsutra.com");
			dsmt.setMailto(teacherDetail.getEmailAddress());
			dsmt.setMailsubject(teacherDetail.getFirstName()+" "+teacherDetail.getLastName()+" cannot apply job \""+jobOrder.getJobTitle()+"\"");
			dsmt.setMailcontent(mailContent);
			dsmt.start();
		}*/
		return null;
	}
	public String donotApplyJob()
	{

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session.getAttribute("referer")!=null)
			session.removeAttribute("referer");
		System.out.println("Do Not Apply");
		return null;
	}

	public String getCurrentStatusCertificate()
	{

		boolean bReturnValue=true;
		boolean bCertificateStatus=false;
		
		TeacherDetail teacherDetail=null;

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		else
			teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");

		List<TeacherCertificate> teacherCertificates=new ArrayList<TeacherCertificate>();
		try 
		{
			if(teacherDetail!=null)
			{
				teacherCertificates = teacherCertificateDAO.findCertificateByTeacher(teacherDetail);
				if(teacherCertificates!=null && teacherCertificates.size() > 0)
				{
					for(TeacherCertificate pojo:teacherCertificates)
					{
						if(pojo.getCertificationTypeMaster()==null || pojo.getCertificationTypeMaster().getCertificationTypeMasterId()==1)
						{
							bReturnValue=false;
							break;
						}
					}
					
					for(TeacherCertificate pojo:teacherCertificates)
					{
						if(pojo!=null && pojo.getCertificationStatusMaster()!=null && pojo.getCertificationStatusMaster().getCertificationStatusId()==1)
						{
							bCertificateStatus=true;
							break;
						}
					}
				}
				else
				{
					bReturnValue=false;
				}

			}

		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		finally
		{
			System.out.println(bReturnValue+"#"+bCertificateStatus);
			return bReturnValue+"#"+bCertificateStatus;
		}

	}

	// Get Gender By District
	public String getGenderByDistrict(boolean isMiami)
	{
		StringBuffer genderdata = new StringBuffer();
		List<GenderMaster> lstGenderMasters = genderMasterDAO.findAllGenderByOrder();

		if(lstGenderMasters!=null && lstGenderMasters.size()>0)
		{
			for(GenderMaster gm:lstGenderMasters)
			{
				genderdata.append("<div class='span3'>");
				genderdata.append("<label class='radio'>");
				if(isMiami)
				{
					if(gm.getGenderId()==1 || gm.getGenderId()==2)
						genderdata.append("<input type='radio' name='genderId' id='genderId' value='"+gm.getGenderId()+"'>"+gm.getGenderName()+"</br>");
				}
				else
				{
					genderdata.append("<input type='radio' name='genderId' id='genderId' value='"+gm.getGenderId()+"'>"+gm.getGenderName()+"</br>");
				}
				genderdata.append("</label>");
				genderdata.append("</div>");	
			}
		}
		return genderdata.toString();
	}

	// Get Race Data
	public String getRaceData()
	 {
	  StringBuffer racedata = new StringBuffer();
	  List<RaceMaster> lstraceList = raceMasterDAO.findAllRaceByOrder();

	  if(lstraceList!=null && lstraceList.size()>0)
	  {
	   for(RaceMaster rm:lstraceList)
	   {
	    racedata.append("<div class='col-sm-3 col-md-3' style='width:245px;'>");
	    racedata.append("<label class='checkbox inline' style='margin-top: 2px;margin-bottom: 2px;'>");
	      racedata.append("<input type='checkbox' name='raceId' id='raceId' value='"+rm.getRaceId()+"' onclick='manageRaces(this,false);'>"+rm.getRaceName()+"</br>");
	    racedata.append("</label>");
	       racedata.append("</div>");
	   }
	  }
	  return racedata.toString();
	 }
	public String getDSPRaceData(String rId)
	{
		StringBuffer racedata = new StringBuffer();
		List<RaceMaster> lstraceList = raceMasterDAO.findAllRaceByOrder();
		int flg=0;
		String[] ra=null;
		if(rId!=null){
		ra=rId.split(",");	
		}

		if(lstraceList!=null && lstraceList.size()>0)
		{
			for(RaceMaster rm:lstraceList)
			{
				racedata.append("<div class='col-sm-3 col-md-3' style='width:245px;'>");
				racedata.append("<label class='checkbox inline' style='margin-top: 2px;margin-bottom: 2px;'>");
				if(ra!=null){
				for(String s : ra){
					if(Integer.parseInt(s)==rm.getRaceId())
					{
						racedata.append("<input type='checkbox' name='raceId' id='raceId' checked value='"+rm.getRaceId()+"'>"+rm.getRaceName()+"</br>");
						flg=1;
						break;
					}
				}
					if(flg!=1){
						racedata.append("<input type='checkbox' name='raceId' id='raceId' value='"+rm.getRaceId()+"'>"+rm.getRaceName()+"</br>");
					}flg=0	;
				
				}else{
					racedata.append("<input type='checkbox' name='raceId' id='raceId' value='"+rm.getRaceId()+"'>"+rm.getRaceName()+"</br>");
				}
				racedata.append("</label>");
			    racedata.append("</div>");
			}
		}
		return racedata.toString();
	}
	
	public List<DistrictSchools> getFieldOfSchoolList(int districtIdForSchool,String SchoolName){

		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
	    }
		List<DistrictSchools> schoolMasterList =  new ArrayList<DistrictSchools>();
		List<DistrictSchools> fieldOfSchoolList1 = null;
		List<DistrictSchools> fieldOfSchoolList2 = null;
		try 
		{
			Criterion criterion = Restrictions.like("schoolName", SchoolName,MatchMode.START);
			if(SchoolName.length()>0){
				if(districtIdForSchool==0){
					if(SchoolName.trim()!=null && SchoolName.trim().length()>0){
						fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(null, 0, 10, criterion);
						Criterion criterion2 = Restrictions.ilike("schoolName","% "+SchoolName.trim()+"%" );
						fieldOfSchoolList2 = districtSchoolsDAO.findWithLimit(null, 0, 10, criterion2);
						schoolMasterList.addAll(fieldOfSchoolList1);
						schoolMasterList.addAll(fieldOfSchoolList2);
						Set<DistrictSchools> setSchool = new LinkedHashSet<DistrictSchools>(schoolMasterList);
						schoolMasterList = new ArrayList<DistrictSchools>(new LinkedHashSet<DistrictSchools>(setSchool));
					}else{
						fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(null, 0, 10);
						schoolMasterList.addAll(fieldOfSchoolList1);
					}
				}else{
				
					DistrictMaster districtMaster = districtMasterDAO.findById(districtIdForSchool, false, false);
					Criterion criterion2 = Restrictions.eq("districtMaster",districtMaster);
				
					
					if(SchoolName.trim()!=null && SchoolName.trim().length()>0){
						fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(Order.asc("schoolName"), 0, 25, criterion,criterion2);
						Criterion criterion3 = Restrictions.ilike("schoolName","% "+SchoolName.trim()+"%" );
						fieldOfSchoolList2 = districtSchoolsDAO.findWithLimit(Order.asc("schoolName"), 0, 25,criterion3,criterion2);
						
						schoolMasterList.addAll(fieldOfSchoolList1);
						schoolMasterList.addAll(fieldOfSchoolList2);
						Set<DistrictSchools> setSchool = new LinkedHashSet<DistrictSchools>(schoolMasterList);
						schoolMasterList = new ArrayList<DistrictSchools>(new LinkedHashSet<DistrictSchools>(setSchool));
					}else{
						fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(Order.asc("schoolName"), 0, 25,criterion2);
						schoolMasterList.addAll(fieldOfSchoolList1);
					}
				}
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return schoolMasterList;
	}
	
	public float yearBetween(Date currentDate,Date drugDate){
		SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
		String date1	= 	DATE_FORMAT.format(currentDate);
		String date2	= 	DATE_FORMAT.format(drugDate);
		String[] splitDate1 = date1.split("-");
		String[] splitDate2 = date2.split("-");
		float year 		= 	Integer.parseInt(splitDate1[0]) - Integer.parseInt(splitDate2[0]);
		float month 	= 	Integer.parseInt(splitDate1[1]) - Integer.parseInt(splitDate2[1]);
		float day 		= 	Integer.parseInt(splitDate1[2]) - Integer.parseInt(splitDate2[2]);
		float totalYear	=	year+(month/12)+(day/(12*30));
		return totalYear;
	}
	
	public String[] checkEmployee(String employeeCode,String district)
	{
		String arr[] = new String[2];
		try {
		System.out.println(">>>"+employeeCode+"<<<");
		DistrictMaster districtMaster = districtMasterDAO.findByDistrictId(district);
		EmployeeMaster employeMaster = employeeMasterDAO.checkEmployeeByEmployeeCode(employeeCode, districtMaster);
		boolean checkAlsoSa = userMasterDAO.checkTeacherAlsoSA(employeMaster);
		//System.out.println("checkAlsoSa    "+checkAlsoSa);
			/*if(checkAlsoSa && employeMaster.getFECode().equals("1") || employeMaster.getFECode().equals("3")){
				//res=1;
			}else{
				//res=0;
			}*/
			arr[0]=employeMaster.getFECode();
			arr[1]=checkAlsoSa+"";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return arr;
	}
	
	@SuppressWarnings("unchecked")
	public String setDspqFlag(Integer jobId)
	{
		synchronized(this){
			System.out.println("::: setDspqFlag in ::::::::::::::::::::::::::::: jobId=="+jobId);
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
				throw new IllegalStateException(msgYrSesstionExp);
	
			TeacherDetail teacherDetail = null;
	
			if (session == null || session.getAttribute("teacherDetail") == null)
				throw new IllegalStateException(msgYrSesstionExp);
			else
				teacherDetail=(TeacherDetail)session.getAttribute("teacherDetail");
			try
			{
				JobOrder jobOrder = new JobOrder();
				jobOrder.setJobId(jobId);
				
				JobOrder jobOrderForNC = new JobOrder();
				if(jobId!=null)
				{
					jobOrderForNC=jobOrderDAO.findById(jobId, false, false);	
				}
				
				JobForTeacher jobForTeacher = null;
				List<JobForTeacher> jobForTeacherList = new ArrayList<JobForTeacher>();
				jobForTeacherList = jobForTeacherDAO.findByCriteria(Restrictions.eq("teacherId", teacherDetail),Restrictions.eq("jobId", jobOrder));
				if(jobForTeacherList!=null && jobForTeacherList.size()>0 && jobForTeacherList.size()==1)
				{
					jobForTeacher = jobForTeacherList.get(0);
					jobForTeacher.setFlagForDspq(true);
					jobForTeacherDAO.makePersistent(jobForTeacher);
					System.out.println("setDspqFlag true in jobForTeacher ::::::::::::::::::::::::::::: for jobId=="+jobId);
				}
				else if(jobForTeacherList.size()==0)
				{
					System.out.println("::: setDspqFlag : insert jobForTeacher after dspq complete : jobId : "+jobId);
					StatusMaster statusMaster = statusMasterDAO.findStatusByShortName("icomp");
					String coverLetter = (String)(session.getAttribute("coverLetter")==null?"":session.getAttribute("coverLetter"));
					String isAffilated =(String)(session.getAttribute("isAffilated")==null?"0":session.getAttribute("isAffilated")); 
					String staffType =(String)(session.getAttribute("staffType")==null?"N":session.getAttribute("staffType"));
					
					try{
						DistrictMaster districtMaster = jobOrder.getDistrictMaster();
						List<InternalTransferCandidates> itcList=internalTransferCandidatesDAO.getInternalCandidateByDistrict(teacherDetail,districtMaster);
						if(itcList.size()>0){
							isAffilated="1";
						}
					}catch(Exception e){
						e.printStackTrace();
					}
					jobForTeacher = new JobForTeacher();
					jobForTeacher.setTeacherId(teacherDetail);
					jobForTeacher.setJobId(jobOrder);
					if(jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId()!=null)
						jobForTeacher.setDistrictId(jobOrder.getDistrictMaster().getDistrictId());
					jobForTeacher.setStatus(statusMaster);
					jobForTeacher.setStatusMaster(statusMaster);
					jobForTeacher.setFlagForDspq(true);
					jobForTeacher.setCoverLetter(coverLetter);
					if(jobOrderForNC!=null && jobOrderForNC.getDistrictMaster()!=null && jobOrderForNC.getDistrictMaster().getHeadQuarterMaster()!=null && jobOrderForNC.getDistrictMaster().getHeadQuarterMaster().getHeadQuarterId()==2)
					{	
						String teacherStatus="";
						if(session.getAttribute("Currnrt")!=null)
							teacherStatus=session.getAttribute("Currnrt").toString();
						
						System.out.println("teacherStatus=======>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+teacherStatus);
						if(teacherStatus!=null && teacherStatus!="" && teacherStatus.equalsIgnoreCase("c"))
						{
							jobForTeacher.setIsAffilated(1);
							session.setAttribute("Currnrt","");
						}
						else if(teacherStatus!=null && teacherStatus!="" && teacherStatus.equalsIgnoreCase("x"))
						{
							jobForTeacher.setIsAffilated(0);
							session.setAttribute("Currnrt","");
						}
						else
						{
							jobForTeacher.setIsAffilated(0);
						}
					}
					else
					{
						jobForTeacher.setIsAffilated(Utility.getIntValue(isAffilated));
					}
					jobForTeacher.setStaffType(staffType);
					jobForTeacher.setCreatedDateTime(new Date());
					Map<String, String> referer = (HashMap<String, String>) session.getAttribute("referer");
					if(referer!=null){
						String re = referer.get("refererURL");
						jobForTeacher.setRedirectedFromURL(re);
					}
					String jobBoardReferralURL = session.getAttribute("jobBoardReferralURL")==null?"":(String)session.getAttribute("jobBoardReferralURL");
					jobForTeacher.setJobBoardReferralURL(jobBoardReferralURL);
					jobForTeacherDAO.makePersistent(jobForTeacher);
					try
					{
						session.setAttribute("dspqComplete"+jobId,true);
						System.out.println("setDspqFlag true in session ::::::::::::::::::::::::::::: for jobId=="+jobId);
					}
					catch(Exception e)
					{
						e.printStackTrace();
					}
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	
	
	public String getDetailOnJObLevel(Integer jobId){
		
		StringBuffer tmRecords =	new StringBuffer();
		try {
			JobOrder jobOrder = jobOrderDAO.findById(jobId, false, false);
			BranchesAjax branchesAjax=new BranchesAjax();
			if(jobOrder!=null){
				HeadQuarterMaster headQuarterMaster=null;
				if(jobOrder.getBranchMaster() !=null){
					tmRecords.append("<TABLE align='center'>");
						tmRecords.append("<TR>");
						tmRecords.append("<TD>Please contact the following Kelly Branch</TD>");
						tmRecords.append("</TR>");
						tmRecords.append("<TR>");
						tmRecords.append("<TD>"+branchesAjax.getBranchDetails(jobOrder)+"</TD>");
						tmRecords.append("</TR>");	
					tmRecords.append("</TABLE>");
					
				}else if(jobOrder.getHeadQuarterMaster()!=null){
					headQuarterMaster=jobOrder.getHeadQuarterMaster();
					
					tmRecords.append("<TABLE align='center'>");
					tmRecords.append("<TR>");
						tmRecords.append("<TD colspan='2' align='center'>Please contact the following Head Quarter</TD>");
					tmRecords.append("</TR>");
					tmRecords.append("<TR>");
						tmRecords.append("<TD>Head Quarter Name: </TD>");
						tmRecords.append("<TD>"+headQuarterMaster.getHeadQuarterName()+"</TD>");
					tmRecords.append("</TR>");				
					
					tmRecords.append("<TR>");
						tmRecords.append("<TD>Phone Number: </TD>");
						tmRecords.append("<TD>"+headQuarterMaster.getPhoneNumber()+"</TD>");
					tmRecords.append("</TR>");
				tmRecords.append("</TABLE>");
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return tmRecords.toString();
	}
	
	public String getDoNotHireStatusByEmpoloyeeId(String employeeCode, Integer districtId,String empDOB,String empSSN){
		GlobalServices.println("<<<<<<<<<<<<<<<<<<::::::::"+employeeCode+"::::::::::getDoNotHireStatusByEmpoloyeeId:::::::"+districtId+"::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		WebContext context;
        context = WebContextFactory.get();
        HttpServletRequest request = context.getHttpServletRequest();
        HttpSession session = request.getSession(false);
        TeacherDetail td=null;
        if(session == null ||(session.getAttribute("teacherDetail")==null)) 
              throw new IllegalStateException(msgYrSesstionExp);
        else if(session.getAttribute("teacherDetail")!=null)
        	td=(TeacherDetail)session.getAttribute("teacherDetail");	
	        
		String DNSStatus="0";
		try{
		String contactNo="303-982-6515";
		DistrictMaster districtMaster=districtMasterDAO.findById(districtId, false, false);
		List<Criterion> listCriterions=new ArrayList<Criterion>();
		listCriterions.add(Restrictions.eq("districtMaster", districtMaster));
		System.out.println("td.getEmailAddress()==="+td.getEmailAddress()+" td.id=="+td.getTeacherId());
		if(employeeCode!=null && !employeeCode.trim().equalsIgnoreCase("")){
			listCriterions.add(Restrictions.eq("employeeCode", employeeCode));
			//listCriterions.add(Restrictions.eq("districtMaster", dm));
			//listCriterions.add(Restrictions.eq("emailAddress",td.getEmailAddress()));
		}else{
			Date dob=new SimpleDateFormat("MMddyyyy").parse(empDOB);
			listCriterions.add(Restrictions.eq("SSN", empSSN));
			listCriterions.add(Restrictions.eq("dob", dob));
			listCriterions.add(Restrictions.eq("emailAddress",td.getEmailAddress()));
		}
		
			if(employeeCode!=null && districtId!=null){
				EmployeeMaster employeeMaster=null;
				listCriterions.add(Restrictions.eq("districtMaster", districtMaster));
				List<EmployeeMaster> empList=employeeMasterDAO.findByCriteria(listCriterions.toArray(new Criterion[listCriterions.size()]));
				if(empList!=null && empList.size()>0){
					employeeMaster=empList.get(0);
				}
				if(employeeMaster!=null && employeeMaster.getPCCode()!=null && employeeMaster.getPCCode().trim().equalsIgnoreCase("Y")){
					String message=GlobalServices.getConvertedString(Utility.getLocaleValuePropByKey("doNotHireMessage", locale),"#brk#",new String[]{districtMaster.getDisplayName(),contactNo});
					DNSStatus="1##"+districtMaster.getDisplayName()+"##"+message;
				}
			}
		}catch(Throwable t){
			t.printStackTrace();
		}
			GlobalServices.println("DNSStatus==================="+DNSStatus);	
		return DNSStatus;
	}
	public String[] getDoNotHireResponseForJeffco(String empDOB,String empSSN){
		StringBuffer response = new StringBuffer();
		int responseCode =0;
		String JC_TM_DNHR_RESP=null;
        JSONObject jsonRequest = null;
        JSONObject faultOBJ = null;
        String[] doNotHireInfo=null;
		try{
			//String url = "https://jcspa.jeffco.k12.co.us/PSIGW/RESTListeningConnector/PSFT_HR/DontHire.v1/";
		    String url = Utility.getValueOfPropByKey("productionUrlOrTest")+"/PSIGW/RESTListeningConnector/PSFT_HR/DontHire.v1";
			URL obj = new URL(url);
	        HttpsURLConnection urlConnection = (HttpsURLConnection) obj.openConnection();
	        String method = "POST";
	        urlConnection.setRequestMethod(method); 
	        urlConnection.setRequestProperty("User-Agent", "");
	        urlConnection.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
	        //urlConnection.setRequestProperty("Authorization", "Basic " + "VEVBQ0hNQVQ6QzBsZFN0MG4z");
	        urlConnection.setRequestProperty("Authorization", "Basic " + Utility.getValueOfPropByKey("passwordAPIURL"));
	        
	        //String urlParameters = "{\"JC_TM_DNHR_RQST\": {\"SSN\": \""+empSSN+"\",\"DOB\": \""+dob+"\"}}";
	        //String urlParameters = "{\"JC_TM_DNHR_RQST\": {\"SSN\": \"8168\",\"DOB\": \"04291971\"}}";
	        
	        String urlParameters = "{\"JC_TM_DNHR_RQST\": {\"SSN\": \""+empSSN.substring((empSSN.length()-4),empSSN.length())+"\",\"DOB\": \""+empDOB+"\"}}";
	        
	        System.out.println("Input parameter suplied: "+urlParameters);
	        
	        urlConnection.setDoOutput(true);
	        DataOutputStream dos = new DataOutputStream(urlConnection.getOutputStream());
	        dos.writeBytes(urlParameters);
	        dos.flush();
	        dos.close();
	        
	        responseCode = urlConnection.getResponseCode();
	        System.out.println("\nSending '" + method + "' request to URL : " + url);
	        System.out.println("Post parameters : " + urlParameters);
	        System.out.println("Response Code : " + responseCode);
	        if(responseCode==200){
	        doNotHireInfo=new String[5];
	        BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
	        String inputLine;
	        while ((inputLine = in.readLine()) != null)
	        {
	              response.append(inputLine); 
	        }
	        in.close();
	        String finalResult=response.toString();
	        if(finalResult!=null && !finalResult.trim().equals("")){
	        	 jsonRequest = (JSONObject) JSONSerializer.toJSON(finalResult); 
	
	              if(finalResult.contains("JC_TM_DNHR_RESP")){
	              JC_TM_DNHR_RESP =jsonRequest.get("JC_TM_DNHR_RESP")==null?"":UtilityAPI.parseString(jsonRequest.getString("JC_TM_DNHR_RESP")).trim();
	              
	              if(JC_TM_DNHR_RESP!=null && !JC_TM_DNHR_RESP.trim().equalsIgnoreCase(""))
	              faultOBJ = jsonRequest.getJSONObject("JC_TM_DNHR_RESP"); 
	              
	              if(faultOBJ!=null){
	            	  doNotHireInfo[0] = faultOBJ.get("PC")==null?"":UtilityAPI.parseString(faultOBJ.getString("PC")).trim();
	            	  doNotHireInfo[1] = faultOBJ.get("RR")==null?"":UtilityAPI.parseString(faultOBJ.getString("RR")).trim();
	            	  doNotHireInfo[2] = faultOBJ.get("OC")==null?"":UtilityAPI.parseString(faultOBJ.getString("OC")).trim();
	            	  doNotHireInfo[3] = faultOBJ.get("Emplid")==null?"":UtilityAPI.parseString(faultOBJ.getString("Emplid")).trim();
	            	  doNotHireInfo[4] = faultOBJ.get("EmplStatus")==null?"":UtilityAPI.parseString(faultOBJ.getString("EmplStatus")).trim();
	              }
	          }   
	        }
	        return doNotHireInfo;
	        }else if(responseCode==400){
	        	doNotHireInfo=new String[5];
	        	doNotHireInfo[0] =null;
	        		doNotHireInfo[1] =null;
	        			doNotHireInfo[2] =null;
	        				doNotHireInfo[3] =null;
	        					doNotHireInfo[4] =null;
	        	return doNotHireInfo;
	        }
	        
		}catch(Exception e){e.printStackTrace();
		return doNotHireInfo;
		}
		return doNotHireInfo;
		
	}
	public String doNotHiredForJeffco(String empDOB,String empSSN)throws Exception{
	      
        WebContext context;
        context = WebContextFactory.get();
        HttpServletRequest request = context.getHttpServletRequest();
        HttpSession session = request.getSession(false);
        TeacherDetail td=null;
        if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
              throw new IllegalStateException(msgYrSesstionExp);
        else if(session.getAttribute("teacherDetail")!=null)
        	td=(TeacherDetail)session.getAttribute("teacherDetail");	
	    System.out.println("inside****************doNotHiredForJeffco******************::::::::::::::::empSSN: ");
	    try{
	    	TeacherPersonalInfo tpi=teacherPersonalInfoDAO.findById(td.getTeacherId(), false, false);
			if(empSSN!=null && !empSSN.trim().equals(""))
			tpi.setSSN(Utility.encodeInBase64(empSSN));
			tpi.setDob(empDOB.trim().equalsIgnoreCase("")?null:new SimpleDateFormat("MMddyyyy").parse(empDOB));
			teacherPersonalInfoDAO.updatePersistent(tpi);
	        DistrictMaster dist = new DistrictMaster();
	        dist.setDistrictId(804800);
	        String[] result = getDoNotHireResponseForJeffco(empDOB,empSSN);
	        
	        System.out.println("Final response : "+result);
	        if(result!=null){
	        String PC=result[0];
	        String RR=result[1];
	        String OC=result[2];
	        String Emplid=result[3];
	        String EmplStatus=result[4];
	        System.out.println("*******PC: "+PC+" || "+RR+" || "+" || "+OC+" || "+Emplid+" || "+EmplStatus);
            insertOrUpdateEmployeeMaster(td,Emplid,dist,empDOB,empSSN,PC,RR,OC,Emplid,EmplStatus);
            return Emplid+"##"+EmplStatus;
            //return "12456##A";
	        }
	    }catch(Exception e){
           e.printStackTrace();
           return "##";
     }
	 return "##";
}
	
	@Transactional(readOnly=false)
	public boolean insertOrUpdateEmployeeMaster(TeacherDetail td, String employeeCode, DistrictMaster districtId,String dob,String sSN, String pCCode,String rRCode,String oCCode,String Emplid, String EmplStatus)throws Exception{
    	List<EmployeeMaster> listEM=new ArrayList<EmployeeMaster>();
    	SimpleDateFormat sdf=new SimpleDateFormat("MMddyyyy");
    	boolean flagPrevious=false;
    	if(employeeCode==null)employeeCode="";
    		if(!employeeCode.trim().equals("") && !employeeCode.trim().equals("0")){
    		listEM=employeeMasterDAO.findByCriteria(Restrictions.eq("employeeCode", employeeCode.trim()),Restrictions.eq("districtMaster", districtId));
    		if(listEM.size()<1 && !sSN.trim().equalsIgnoreCase("") && !dob.trim().equalsIgnoreCase("")){
    			listEM=employeeMasterDAO.findByCriteria(Restrictions.eq("SSN", sSN),Restrictions.eq("dob",sdf.parse(dob)),Restrictions.eq("emailAddress", td.getEmailAddress()),Restrictions.eq("districtMaster", districtId));
    			if(listEM.size()>0){
    				flagPrevious=true;
    			}else{
    				listEM=employeeMasterDAO.findByCriteria(Restrictions.eq("emailAddress", td.getEmailAddress()),Restrictions.eq("districtMaster", districtId));
    				if(listEM.size()>0)
        				flagPrevious=true;
    			}
    		}
    		}
    		else if(!flagPrevious){
    			if(!sSN.trim().equalsIgnoreCase("") && !dob.trim().equalsIgnoreCase(""))
    				listEM=employeeMasterDAO.findByCriteria(Restrictions.eq("SSN", sSN),Restrictions.eq("dob",sdf.parse(dob)),Restrictions.eq("emailAddress", td.getEmailAddress()),Restrictions.eq("districtMaster", districtId));
	    		if(listEM.size()<1)
		    		listEM=employeeMasterDAO.findByCriteria(Restrictions.eq("emailAddress", td.getEmailAddress()),Restrictions.eq("districtMaster", districtId));
    		}
    		if(listEM.size()>0){
    			EmployeeMaster em=listEM.get(0);
    			em.setDistrictMaster(districtId);
    			em.setDob(dob.trim().equalsIgnoreCase("")?null:sdf.parse(dob));
    			em.setRRCode(rRCode!=null?(rRCode.trim().equalsIgnoreCase("")?em.getRRCode():rRCode):em.getRRCode());
    			em.setPCCode(pCCode!=null?(pCCode.trim().equalsIgnoreCase("")?em.getPCCode():pCCode):em.getPCCode());
    			em.setOCCode(oCCode!=null?(oCCode.trim().equalsIgnoreCase("")?em.getOCCode():oCCode):em.getOCCode());
    			em.setEmployeeCode(Emplid!=null?(Emplid.trim().equalsIgnoreCase("")?em.getEmployeeCode():Emplid):em.getEmployeeCode());
    			em.setEmploymentStatus(EmplStatus!=null?(EmplStatus.trim().equalsIgnoreCase("")?em.getEmploymentStatus():EmplStatus):em.getEmploymentStatus());
    			/*if(!employeeCode.trim().equals("") && !employeeCode.trim().equals("0"))
        		em.setEmployeeCode(employeeCode.trim());*/
    			em.setEmailAddress(td.getEmailAddress());
    			em.setSSN(sSN);
    			em.setCreatedDateTime(new Date());
    			em.setDoNotHireStatus(pCCode!=null?(pCCode.trim().equals("Y")?"1":null):null);
    			em.setStatus(td.getStatus());
    			employeeMasterDAO.makePersistent(em);
    			return true;
    		}else{
    			EmployeeMaster em=new EmployeeMaster();
    			em.setFirstName(td.getFirstName());
    			em.setLastName(td.getLastName());
    			em.setMiddleName("");
    			em.setEmailAddress(td.getEmailAddress());
    			/*if(!employeeCode.trim().equals("") && !employeeCode.trim().equals("0"))
    			em.setEmployeeCode(employeeCode.trim());*/
    			em.setDistrictMaster(districtId);
    			em.setDob(dob.trim().equalsIgnoreCase("")?null:sdf.parse(dob));
    			em.setRRCode(rRCode);
    			em.setPCCode(pCCode);
    			em.setOCCode(oCCode);
    			em.setEmployeeCode(Emplid);
    			em.setEmploymentStatus(EmplStatus);
    			em.setSSN(sSN);
    			em.setStatus(td.getStatus());
    			em.setCreatedDateTime(new Date());
    			em.setDoNotHireStatus(pCCode!=null?(pCCode.trim().equals("Y")?"1":null):null);
    			employeeMasterDAO.makePersistent(em);
    			return true;
    		}
    }

	

}




