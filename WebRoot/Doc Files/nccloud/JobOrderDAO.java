package tm.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.connection.ConnectionProvider;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.hibernate.engine.SessionFactoryImplementor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

//import com.sun.xml.fastinfoset.sax.Properties;

import tm.bean.JobOrder;
import tm.bean.JobRequisitionNumbers;
import tm.bean.SchoolInJobOrder;
import tm.bean.TeacherDetail;
import tm.bean.assessment.AssessmentJobRelation;
import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.GeoZoneMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.SubjectMaster;
import tm.bean.user.UserMaster;
import tm.dao.assessment.AssessmentJobRelationDAO;
import tm.dao.generic.GenericHibernateDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.JobCategoryMasterDAO;
import tm.dao.master.SchoolMasterDAO;
import tm.utility.ElasticSearchConfig;
import tm.utility.Utility;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import org.hibernate.FetchMode;
import org.hibernate.transform.Transformers;

public class JobOrderDAO extends GenericHibernateDAO<JobOrder, Integer> 
{
	public JobOrderDAO() 
	{
		super(JobOrder.class);
	}
	@Autowired
	private SchoolMasterDAO schoolMasterDAO;
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	@Autowired
	private JobCategoryMasterDAO jobCategoryMasterDAO;
	@Autowired
	AssessmentJobRelationDAO assessmentJobRelationDAO;
	public void setAssessmentJobRelationDAO(
			AssessmentJobRelationDAO assessmentJobRelationDAO) {
		this.assessmentJobRelationDAO = assessmentJobRelationDAO;
	}
	@Autowired
	private SchoolInJobOrderDAO schoolInJobOrderDAO;
	public void setSchoolInJobOrderDAO(SchoolInJobOrderDAO schoolInJobOrderDAO) {
		this.schoolInJobOrderDAO = schoolInJobOrderDAO;
	}
	/*
	 * @author : Gourav Pal
	 * @discription : It is used to get valid Job Orders By District 
	 */
	
		@Transactional(readOnly=false)
		public List<JobOrder> findJobOrderListByDistrict(DistrictMaster districtMaster){
			List<JobOrder> jobOrderList =  new ArrayList<JobOrder>();
			
			try{
				
			//	Criterion criterion1 = Restrictions.isNotEmpty("locationCode");
				Criterion criterion3 = Restrictions.isNotNull("apiJobId");
				Criterion criterion2 = Restrictions.eq("districtMaster",districtMaster);
				//Criterion criterion4 = Restrictions.or(criterion1, criterion3);
				jobOrderList = findByCriteria(criterion2,criterion3);
			}catch(Exception e){
				e.printStackTrace();
			}			
				return jobOrderList;	
		}
		
		/*
		 * @author : Gourav Pal
		 * @discription : It is used to get valid Job Orders By District only Pool Jobs 
		 */ 
		
			@Transactional(readOnly=false)
			public List<JobOrder> findJobOrderListByDistrictAndPoolJob(DistrictMaster districtMaster){
				List<JobOrder> jobOrderList =  new ArrayList<JobOrder>();
				
				try{
					
				//	Criterion criterion1 = Restrictions.isNotEmpty("locationCode");
					Criterion criterion1 = Restrictions.eq("status","A");
					Criterion criterion2 = Restrictions.eq("districtMaster",districtMaster);
					/*Criterion criterion3 = Restrictions.isNotNull("apiJobId");
					Criterion criterion4 = Restrictions.isNotNull("geoZoneMaster");*/
					
					//Criterion criterion4 = Restrictions.or(criterion1, criterion3);
					jobOrderList = findByCriteria(criterion1,criterion2);
				}catch(Exception e){
					e.printStackTrace();
				}			
					return jobOrderList;	
			}
	
	/*
	 * @author : Vishwanath Kumar
	 * @discription : It is used to get valid Job Orders
	 */

	@Transactional(readOnly=false)
	public List<JobOrder> findValidJobOrders(Map<Integer,JobOrder> map,DistrictMaster districtMaster,SchoolMaster schoolMaster)
	{
		List<JobOrder> jobOrders =null;
		List<Integer> schoolJobIds = new ArrayList<Integer>();
		System.out.println("time" + Calendar.getInstance().get(Calendar.SECOND));
		if(schoolMaster!=null)
		{
			jobOrders = schoolInJobOrderDAO.jobOrderPostedBySchool(schoolMaster);
			System.out.println("Inside school master???????????????????????????????????????????????????");
			for (JobOrder jobOrder : jobOrders) {
				schoolJobIds.add(jobOrder.getJobId());
			}
		}
		
		try 
		{
			List<Integer> jobIds = new ArrayList<Integer>();
			List list=null;
			JobOrder joborder = null;
			list=assessmentJobRelationDAO.findUniqueJobOrders();
			System.out.println("time" + Calendar.getInstance().get(Calendar.SECOND));
			Iterator itr = list.iterator();

			while(itr.hasNext())
			{
				AssessmentJobRelation obj = (AssessmentJobRelation) itr.next();
				joborder=obj.getJobId();
				
				if(map!=null)
				{
					if(map.get(joborder.getJobId())==null)
						jobIds.add(joborder.getJobId());
				}else
					jobIds.add(joborder.getJobId());
			}


			Session session = getSession();

			if(jobIds.size()!=0)
			{
				if(schoolMaster==null)
				{
					List result = session.createCriteria(getPersistentClass())       
					.add(Restrictions.eq("isJobAssessment",true)) 
					.add(Restrictions.eq("status","a")) 
					.add(Restrictions.eq("jobStatus","o"))
					.add(Restrictions.eq("districtMaster",districtMaster))
					.add(Restrictions.eq("createdForEntity",2))
					.add(Restrictions.not(Restrictions.in("jobId",jobIds))) 
					//.add(Restrictions.sqlRestriction(" now() BETWEEN jobstartdate and jobenddate ")) 
					//.add(Restrictions.le("jobStartDate",new Date())) 
					//.add( Restrictions.ge("jobEndDate",new Date())) 
					.addOrder(Order.asc("jobTitle"))
					.list();
					System.out.println("time1:" + Calendar.getInstance().get(Calendar.SECOND));
					return result;
				}else
				{
					List result = session.createCriteria(getPersistentClass())       
					.add(Restrictions.eq("isJobAssessment",true)) 
					.add(Restrictions.eq("status","a")) 
					.add(Restrictions.eq("jobStatus","o"))
					.add(Restrictions.eq("districtMaster",districtMaster))
					.add(Restrictions.eq("createdForEntity",3))
					.add(Restrictions.not(Restrictions.in("jobId",jobIds))) 
					.add(Restrictions.in("jobId",schoolJobIds))
					//.add(Restrictions.sqlRestriction(" now() BETWEEN jobstartdate and jobenddate ")) 
					//.add(Restrictions.le("jobStartDate",new Date())) 
					//.add( Restrictions.ge("jobEndDate",new Date())) 
					.addOrder(Order.asc("jobTitle"))
					.list();
					System.out.println("time2:" + Calendar.getInstance().get(Calendar.SECOND));
					return result;

				}
			}
			else
			{
				if(schoolMaster==null)
				{
					List result = session.createCriteria(getPersistentClass())       
					.add(Restrictions.eq("isJobAssessment",true)) 
					.add(Restrictions.eq("jobStatus","o"))
					.add(Restrictions.eq("status","a")) 
					.add(Restrictions.eq("districtMaster",districtMaster)) 
					.add(Restrictions.eq("createdForEntity",2))
					//.add(Restrictions.sqlRestriction(" now() BETWEEN jobstartdate and jobenddate ")) 
					//.add(Restrictions.le("jobStartDate",new Date())) 
					//.add( Restrictions.ge("jobEndDate",new Date())) 
					.addOrder(Order.asc("jobTitle"))
					.list();
					System.out.println("time3:" + Calendar.getInstance().get(Calendar.SECOND));
					return result;
				}else
				{
					List result = session.createCriteria(getPersistentClass())       
					.add(Restrictions.eq("isJobAssessment",true)) 
					.add(Restrictions.eq("jobStatus","o"))
					.add(Restrictions.eq("status","a")) 
					.add(Restrictions.eq("districtMaster",districtMaster)) 
					.add(Restrictions.eq("createdForEntity",3))
					.add(Restrictions.in("jobId",schoolJobIds))
					//.add(Restrictions.sqlRestriction(" now() BETWEEN jobstartdate and jobenddate ")) 
					//.add(Restrictions.le("jobStartDate",new Date())) 
					//.add( Restrictions.ge("jobEndDate",new Date())) 
					.addOrder(Order.asc("jobTitle"))
					.list();
					System.out.println("time4:" + Calendar.getInstance().get(Calendar.SECOND));
					return result;
				}
			}
			

		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return null;
	}

	/*
	 * @author : Vishwanath Kumar
	 * @discription : It is used to get valid District involved in Job Orders
	 */

	@Transactional(readOnly=false)
	public List<DistrictMaster> getDistrictsByJobOrders(int createdForEntity)
	{
		
			
		List<DistrictMaster> districtMasters = new ArrayList<DistrictMaster>();
		DistrictMaster districtMaster = null;
		try{
			Session session = getSession();
			List result = session.createCriteria(getPersistentClass()) 
			.add(Restrictions.eq("status","a")) 
			.add(Restrictions.eq("jobStatus","o")) 
			.add(Restrictions.eq("isJobAssessment",true)) 
			.add(Restrictions.eq("createdForEntity",createdForEntity)) 
			//.add(Restrictions.sqlRestriction(" now() BETWEEN jobstartdate and jobenddate ")) 
			//.add(Restrictions.le("jobStartDate",new Date())) 
			//.add( Restrictions.ge("jobEndDate",new Date())) 
			.setProjection(Projections.projectionList()
					.add(Projections.groupProperty("districtMaster"))
					.add(Projections.property("districtMaster"))  
			).list();

			Iterator itr = result.iterator();
			while(itr.hasNext())
			{
				Object[] obj = (Object[]) itr.next();
				districtMaster=(DistrictMaster)obj[0];
				districtMasters.add(districtMaster);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return districtMasters;
	}
	/*
	 * @author : Vishwanath Kumar
	 * @discription : It is used to get valid School involved in Job Orders
	 */

	@Transactional(readOnly=false)
	public List<SchoolMaster> getSchoolsByDistrictInJobOrders(DistrictMaster districtMaster)
	{
			
		List<JobOrder> jobOrders = null;
		List<SchoolMaster> schoolMasters = new ArrayList<SchoolMaster>();
		Collection<SchoolMaster> schoolCollection = new HashSet<SchoolMaster>();
		//SortedSet<SchoolMaster> schoolCollection=new TreeSet<SchoolMaster>();
		try{
			Session session = getSession();
			jobOrders = session.createCriteria(getPersistentClass()) 
			.add(Restrictions.eq("status","a")) 
			.add(Restrictions.eq("jobStatus","o")) 
			.add(Restrictions.eq("isJobAssessment",true)) 
			.add(Restrictions.eq("createdForEntity",3)) 
			.add(Restrictions.eq("districtMaster",districtMaster)) 
			.list();
			for (Iterator iterator = jobOrders.iterator(); iterator.hasNext();) {
				JobOrder jobOrder = (JobOrder) iterator.next();
				schoolCollection.addAll(jobOrder.getSchool());

			}
			SortedMap<String,SchoolMaster> map = new TreeMap<String,SchoolMaster>();
			schoolMasters.addAll(schoolCollection);
			for (Iterator iterator  = schoolCollection.iterator(); iterator.hasNext();) {
				SchoolMaster schoolMaster = (SchoolMaster) iterator .next();
				map.put(schoolMaster.getSchoolName(), schoolMaster);
			}

			if(jobOrders.size()>0)
				schoolMasters = new ArrayList<SchoolMaster>(map.values());
		}catch (Exception e) {
			e.printStackTrace();
		}
		return schoolMasters;
	}

	/*
	 * Get all the job from database that is active 
	 */
	@Transactional(readOnly=false)
	public List<JobOrder> findJobtoShow()
	{
		List<JobOrder> jobOrders = null;
		try 
		{
			Date dateWithoutTime = Utility.getDateWithoutTime();
			
			Criterion criterion1 = Restrictions.eq("status","a");
			Criterion criterion2 = Restrictions.le("jobStartDate",dateWithoutTime);
			Criterion criterion3 = Restrictions.ge("jobEndDate",dateWithoutTime);
			Criterion criterion4 = Restrictions.eq("approvalBeforeGoLive",1);
			Criterion criterion5 = Restrictions.ne("isInviteOnly",true);
			jobOrders = findByCriteria(Order.desc("createdDateTime"),criterion1,criterion2,criterion3,criterion4,criterion5);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}		
		return jobOrders;
	}
	@Transactional(readOnly=false)
	public List<JobOrder> findJobtoShowNew(Set<Integer> set)
	{
		List<JobOrder> jobOrders = null;
		try 
		{
			Date dateWithoutTime = Utility.getDateWithoutTime();
			
			Criterion criterion1 = Restrictions.eq("status","a");
			Criterion criterion2 = Restrictions.le("jobStartDate",dateWithoutTime);
			Criterion criterion3 = Restrictions.ge("jobEndDate",dateWithoutTime);
			Criterion criterion4 = Restrictions.eq("approvalBeforeGoLive",1);
			Criterion criterion5 = Restrictions.ne("isInviteOnly",true);
			Criterion criterion6 = Restrictions.not(Restrictions.in("jobId",set));
			jobOrders = findByCriteria(Order.desc("createdDateTime"),criterion1,criterion2,criterion3,criterion4,criterion5,criterion6);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}		
		return jobOrders;
	}
	@Transactional(readOnly=false)
	public List<JobOrder> findJobtoShowNew1(Set<Integer> set)
	{
		List<JobOrder> jobOrders = null;
		Session session = getSession();
		List<Integer> list=new ArrayList<Integer>();
		list.addAll(set);
		try 
		{
			Date dateWithoutTime = Utility.getDateWithoutTime();
			String hql = " FROM JobOrder where jobId not in (:jobId) and status=:status and jobStartDate<=:jobStartDate and jobEndDate>=:jobEndDate and isInviteOnly!=:isInviteOnly and approvalBeforeGoLive=:approvalBeforeGoLive";
			Query query = session.createQuery(hql);
			//Query query = session.createSQLQuery(hql);
			query.setParameter("status","a");
			query.setParameter("jobStartDate",dateWithoutTime);
			query.setParameter("approvalBeforeGoLive",1);
			query.setParameter("jobEndDate",dateWithoutTime);
			query.setParameter("isInviteOnly",true);
			query.setParameterList("jobId",list);
			jobOrders = query.list();
			System.out.println("Job Order Size"+jobOrders.size());
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}		
		return jobOrders;
	}
	/*
	 * Get all the sorted job from database that is active 
	 */
	@Transactional(readOnly=false)
	public List<JobOrder> findSortedJobtoShow(Order sortOrderStrVal)
	{
		List<JobOrder> jobOrders = null;
		try 
		{
			Date dateWithoutTime = Utility.getDateWithoutTime();
			
			Criterion criterion1 = Restrictions.eq("status","a");
			Criterion criterion2 = Restrictions.le("jobStartDate",dateWithoutTime);
			Criterion criterion3 = Restrictions.ge("jobEndDate",dateWithoutTime);
			Criterion criterion4 = Restrictions.eq("approvalBeforeGoLive",1);
			Criterion criterion5 = Restrictions.ne("isInviteOnly",true);
			jobOrders = findByCriteria(sortOrderStrVal,criterion1,criterion2,criterion3,criterion4,criterion5);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}		
		return jobOrders;
	}
	@Transactional(readOnly=false)
	public List<JobOrder> findSortedJobtoShowNew(Order sortOrderStrVal,Set<Integer> set)
	{
		List<JobOrder> jobOrders = null;
		try 
		{
			Date dateWithoutTime = Utility.getDateWithoutTime();
			
			Criterion criterion1 = Restrictions.eq("status","a");
			Criterion criterion2 = Restrictions.le("jobStartDate",dateWithoutTime);
			Criterion criterion3 = Restrictions.ge("jobEndDate",dateWithoutTime);
			Criterion criterion4 = Restrictions.eq("approvalBeforeGoLive",1);
			Criterion criterion5 = Restrictions.ne("isInviteOnly",true);
			Criterion criterion6 = Restrictions.not(Restrictions.in("jobId",set));
			jobOrders = findByCriteria(sortOrderStrVal,criterion1,criterion2,criterion3,criterion4,criterion5,criterion6);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}		
		return jobOrders;
	}
	
	@Transactional(readOnly=false)
	public List<JobOrder> findJobOrderbyJobCategory(JobCategoryMaster jobCategoryMaster)
	{
		List<JobOrder> jobOrders = null;
		try 
		{
			Date dateWithoutTime = Utility.getDateWithoutTime();
			
			Criterion criterion1 = Restrictions.eq("status","a");
			Criterion criterion2 = Restrictions.le("jobStartDate",dateWithoutTime);
			Criterion criterion3 = Restrictions.ge("jobEndDate",dateWithoutTime);			
			Criterion criterion4 = Restrictions.eq("jobCategoryMaster",jobCategoryMaster);
			
			jobOrders = findByCriteria(Order.desc("createdDateTime"),criterion1,criterion2,criterion3,criterion4);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}		
		return jobOrders;
	}
	
	@Transactional(readOnly=false)
	public List<JobOrder> findSortedJobOrderbyJobCategory(Order sortOrderStrVal,List<JobCategoryMaster> jobCategoryMasterList)
	{
		List<JobOrder> jobOrders = null;
		try 
		{
			Date dateWithoutTime = Utility.getDateWithoutTime();
			
			Criterion criterion1 = Restrictions.eq("status","a");
			Criterion criterion2 = Restrictions.le("jobStartDate",dateWithoutTime);
			Criterion criterion3 = Restrictions.ge("jobEndDate",dateWithoutTime);			
			Criterion criterion4 = Restrictions.in("jobCategoryMaster",jobCategoryMasterList);
			
			jobOrders = findByCriteria(sortOrderStrVal,criterion1,criterion2,criterion3,criterion4);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}		
		return jobOrders;
	}
	@Transactional(readOnly=false)
	public List<JobOrder> findSortedJobbyJobCategory(Order sortOrderStrVal,JobCategoryMaster jobCategoryMaster)
	{
		List<JobOrder> jobOrders = null;
		try 
		{
			Date dateWithoutTime = Utility.getDateWithoutTime();
			
			Criterion criterion1 = Restrictions.eq("status","a");
			Criterion criterion2 = Restrictions.le("jobStartDate",dateWithoutTime);
			Criterion criterion3 = Restrictions.ge("jobEndDate",dateWithoutTime);			
			Criterion criterion4 = Restrictions.eq("jobCategoryMaster",jobCategoryMaster);
			
			jobOrders = findByCriteria(sortOrderStrVal,criterion1,criterion2,criterion3,criterion4);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}		
		return jobOrders;
	}
	
	
	@Transactional(readOnly=false)
	public List<JobOrder> findJobOrderbyDistrict(DistrictMaster districtMaster)
	{
		List<JobOrder> jobOrders = null;
		try 
		{

			Date dateWithoutTime = Utility.getDateWithoutTime();
			
			Criterion criterion1 = Restrictions.eq("status","a");
			Criterion criterion2 = Restrictions.le("jobStartDate",dateWithoutTime);
			Criterion criterion3 = Restrictions.ge("jobEndDate",dateWithoutTime);
			Criterion criterion4 = Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion5 = Restrictions.eq("approvalBeforeGoLive",1);

			jobOrders = findByCriteria(Order.desc("createdDateTime"),criterion1,criterion2,criterion3,criterion4,criterion5);

		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return jobOrders;
	}	
	@Transactional(readOnly=false)
	public List<JobOrder> findAllJobOrdersbyDistrict(DistrictMaster districtMaster)
	{
		List<JobOrder> jobOrders = null;
		try 
		{
			Date dateWithoutTime = Utility.getDateWithoutTime();
			
			Criterion criterion1 = Restrictions.eq("status","a");
			Criterion criterion2 = Restrictions.le("jobStartDate",dateWithoutTime);
			Criterion criterion3 = Restrictions.ge("jobEndDate",dateWithoutTime);
			Criterion criterion4 = Restrictions.eq("districtMaster",districtMaster);

			//jobOrders = findByCriteria(Order.desc("createdDateTime"),criterion1,criterion2,criterion3,criterion4);
			jobOrders = findByCriteria(Order.desc("createdDateTime"),criterion1,criterion4);

		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return jobOrders;
	}	
	/*============= Find Sorted Job by district =================*/
	@Transactional(readOnly=false)
	public List<JobOrder> findSortedJobOrderbyDistrict(DistrictMaster districtMaster,Order sortOrderStrVal)
	{
		
		List<JobOrder> jobOrders = null;
		try 
		{
		    
			Date dateWithoutTime = Utility.getDateWithoutTime();

			Criterion criterion1 = Restrictions.eq("status","a");
			Criterion criterion2 = Restrictions.le("jobStartDate",dateWithoutTime);
			Criterion criterion3 = Restrictions.ge("jobEndDate",dateWithoutTime);
			Criterion criterion4 = Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion5 = Restrictions.ne("isInviteOnly",true);
			//jobOrders = findByCriteria(Order.desc("createdDateTime"),criterion1,criterion2,criterion3,criterion4);
			jobOrders = findByCriteria(sortOrderStrVal,criterion1,criterion2,criterion3,criterion4,criterion5);

		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return jobOrders;
	}	
	
	@Transactional(readOnly=false)
	public List<JobOrder> findJobOrderbyDistrict(DistrictMaster districtMaster, JobCategoryMaster jobCategoryMaster)
	{
		List<JobOrder> jobOrders = null;
		try 
		{
			Date dateWithoutTime = Utility.getDateWithoutTime();

			Criterion criterion1 = Restrictions.eq("status","a");
			Criterion criterion2 = Restrictions.le("jobStartDate",dateWithoutTime);
			Criterion criterion3 = Restrictions.ge("jobEndDate",dateWithoutTime);
			Criterion criterion4 = Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion5 = Restrictions.eq("jobCategoryMaster",jobCategoryMaster);
			

			jobOrders = findByCriteria(Order.desc("createdDateTime"),criterion1,criterion2,criterion3,criterion4,criterion5);

		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return jobOrders;
	}
	/*========== Sorted Job Order by District =========*/
	@Transactional(readOnly=false)
	public List<JobOrder> findSortedJobOrderbyDistrict(Order  sortOrderStrVal,DistrictMaster districtMaster,List<SubjectMaster> ObjSubjectList)
	{
		
		List<JobOrder> jobOrders = null;
		try 
		{
			Date dateWithoutTime = Utility.getDateWithoutTime();
			Criterion criterion1 = Restrictions.eq("status","A");
			Criterion criterion2 = Restrictions.le("jobStartDate",dateWithoutTime);
			Criterion criterion3 = Restrictions.ge("jobEndDate",dateWithoutTime);
			Criterion criterion4 = Restrictions.eq("districtMaster",districtMaster);
			/* @Start
			 * @Ashish
			 * @Description :: Check null condition and get List according ObjSubjectList
			 * */
				if(ObjSubjectList!=null && ObjSubjectList.size()>0){
					Criterion criterion5 = Restrictions.in("subjectMaster",ObjSubjectList );
					jobOrders = findByCriteria(sortOrderStrVal,criterion1,criterion2,criterion3,criterion4,criterion5);
				}else{
					jobOrders = findByCriteria(sortOrderStrVal,criterion1,criterion2,criterion3,criterion4);
				}
			/* @End
			 * @Ashish
			 * @Description :: Check null condition and get List according ObjSubjectList
			 * */
				
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return jobOrders;
	}
	
	@Transactional(readOnly=false)
	public List<JobOrder> findSortedJobOrderbyDistrict(Order  sortOrderStrVal,DistrictMaster districtMaster, List<JobCategoryMaster> jobCategoryMasterList,List<SubjectMaster> ObjSubjectList)
	{
		
		List<JobOrder> jobOrders = null;
		try 
		{

			Date dateWithoutTime = Utility.getDateWithoutTime();
			
		    Criterion criterion1 = Restrictions.eq("status","a");
			Criterion criterion2 = Restrictions.le("jobStartDate",dateWithoutTime);
			Criterion criterion3 = Restrictions.ge("jobEndDate",dateWithoutTime);
			Criterion criterion4 = Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion5 = Restrictions.in("jobCategoryMaster",jobCategoryMasterList);
			
			/* @Start
			 * @Ashish
			 * @Description :: Check null condition and get List according ObjSubjectList
			 * */
				if(ObjSubjectList!=null && ObjSubjectList.size()>0){
					Criterion criterion6 = Restrictions.in("subjectMaster",ObjSubjectList );
					jobOrders = findByCriteria(sortOrderStrVal,criterion1,criterion2,criterion3,criterion4,criterion5,criterion6);
				}else{
					if(jobCategoryMasterList.size()>0)
						jobOrders = findByCriteria(sortOrderStrVal,criterion1,criterion2,criterion3,criterion4,criterion5);
				}
			/* @End
			 * @Ashish
			 * @Description :: Check null condition and get List according ObjSubjectList
			 * */
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return jobOrders;
	}
	
	/*
	 * Get all the job from database that is active 
	 */
	@Transactional(readOnly=false)
	public List<JobOrder> findJobfilterbyPref(TeacherDetail teacherDetail)
	{
		return null;

	}
	/* @Author: Gagan 
	* @Discription: Get Total no of Joborder Table from SchoolInJobOrder Table(JobId).
	*/
	/*======= Find findJFTApplicantbyJobOrder  Method ========*/
	@Transactional(readOnly=false)
	public List<JobOrder> findJobOrderlistFromJobId(List<JobOrder> jobOrder)
	{
		List<JobOrder> listjobOrder= null;
		try 
		{
			Calendar cal = Calendar.getInstance();

			cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
			Date sDate=cal.getTime();
			cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
			Date eDate=cal.getTime();
			
			Criterion criterion						=	Restrictions.eq("createdForEntity",3);
			Criterion criterion1					=	Restrictions.eq("status","A");
			Criterion criterion2					=	Restrictions.between("createdDateTime", sDate, eDate);
			
			List<Integer> jobs = new ArrayList<Integer>();
			for(JobOrder jobOrderDetail:jobOrder)
			{
				jobs.add(jobOrderDetail.getJobId());
			}
			
			Criterion criterion3					=	Restrictions.in("jobId",jobs);
			listjobOrder = findByCriteria(criterion1,criterion2,criterion3);		
			for(JobOrder jobOrderDetail1:listjobOrder)
			{
				System.out.println("\n ======After Applying  Criteria: Job Id "+jobOrderDetail1.getJobId());
				
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return listjobOrder;
	}
	
	/* @Author: Gagan 
	* @Discription: Get Total no of Joborder Table from SchoolInJobOrder Table(JobId).
	*/
	/*======= Find findJFTApplicantbyJobOrder  Method ========*/
	@Transactional(readOnly=false)
	public List<JobOrder> getCurrentlyOpenJobOrders(List<JobOrder> listJobOrder)
	{
		List<JobOrder> listjobOrder= null;
		try 
		{
			Calendar cal = Calendar.getInstance();

			cal.set(cal.get(Calendar.YEAR), 0,1,0,0,0);
			Date sDate=cal.getTime();
			cal.set(cal.get(Calendar.YEAR), 11,31,23,59,59);
			Date eDate=cal.getTime();
			
			
			Date dateWithoutTime = Utility.getDateWithoutTime();
			
			Criterion criterion						=	Restrictions.eq("createdForEntity",3);
			Criterion criterion1					=	Restrictions.eq("status","A");
			Criterion criterion2					=	Restrictions.between("createdDateTime", sDate, eDate);
			Criterion criterion4					=	Restrictions.eq("jobStatus","O");
			Criterion criterion5					=	Restrictions.le("jobStartDate",dateWithoutTime);
			Criterion criterion6					=	Restrictions.ge("jobEndDate",dateWithoutTime);
			
			List<Integer> jobs = new ArrayList<Integer>();
			for(JobOrder jobOrderDetail:listJobOrder)
			{
				jobs.add(jobOrderDetail.getJobId());
			}
			
			Criterion criterion3					=	Restrictions.in("jobId",jobs);
			
			//listjobOrder = findByCriteria(criterion,criterion1,criterion2,criterion3,criterion4,criterion5,criterion6);
			listjobOrder = findByCriteria(criterion1,criterion2,criterion3,criterion4,criterion5,criterion6);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return listjobOrder;
	}
	
	/* @Author: Vishwanath 
	* @Discription: Get all jobOrders for district .
	*/
	@Transactional(readOnly=false)
	public List<JobOrder> getAllJobOrdersOfTheYear(DistrictMaster districtMaster)
	{
		List<JobOrder> listjobOrder= null;
		try 
		{
			Calendar cal = Calendar.getInstance();

			cal.set(cal.get(cal.YEAR), 0,1,0,0,0);
			Date sDate=cal.getTime();
			cal.set(cal.get(cal.YEAR), 11,31,0,0,0);
			Date eDate=cal.getTime();
			
			Criterion criterion1 =	Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion2 =	Restrictions.between("createdDateTime", sDate, eDate);
			
			listjobOrder = findByCriteria(criterion1,criterion2);	
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return listjobOrder;
	}
	
	/* @Author: Sekhar 
	* @Discription: Checking default District and School 
	*/
	@Transactional(readOnly=false)
	public int checkDefaultDistrictAndSchool(DistrictMaster districtMaster,SchoolMaster schoolMaster,int defaultType)
	{
		List<JobOrder> listjobOrder= null;
		int jobOrderSize=0;
		try{
			if(defaultType==1){
				Criterion criterion1 =	Restrictions.eq("districtMaster",districtMaster);
				Criterion criterion2 =	Restrictions.eq("attachDefaultDistrictPillar",1);
				listjobOrder = findByCriteria(criterion1,criterion2);
				if(listjobOrder.size()>0){
					jobOrderSize=1;
				}
			}else{
				List<SchoolInJobOrder> lstAllSJO = schoolInJobOrderDAO.findJobBySchool(schoolMaster);
				for(SchoolInJobOrder sjo: lstAllSJO){		
					JobOrder jobOrder = sjo.getJobId();
					if(jobOrder.getAttachDefaultSchoolPillar()==1){
						jobOrderSize=1;
						break;
					}
				}
			}
		}catch (Exception e){
			e.printStackTrace();
		}
		return jobOrderSize;
	}

	/* @Author: Vishwanath 
	* @Discription: Get all jobOrders for district .
	*/
	@Transactional(readOnly=false)
	public List<JobOrder> getAllJobOrdersOfTheYearForSchool(List<Integer> jobs)
	{
		List<JobOrder> listjobOrder= null;
		
		try 
		{
			Calendar cal = Calendar.getInstance();

			cal.set(cal.get(cal.YEAR), 0,1,0,0,0);
			Date sDate=cal.getTime();
			cal.set(cal.get(cal.YEAR), 11,31,0,0,0);
			Date eDate=cal.getTime();
			
			Criterion criterion1 =	Restrictions.in("jobId",jobs);
			Criterion criterion2 =	Restrictions.between("createdDateTime", sDate, eDate);
			
			listjobOrder = findByCriteria(criterion1,criterion2);	
		
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return listjobOrder;
	}
	
	@Transactional(readOnly=false)
	public List<JobOrder> getTodayAciveJob()
	{
		List<JobOrder> lstJobOrders = null;
		try 
		{
			Date dateWithoutTime = Utility.getDateWithoutTime();
			
			Criterion criterion1 = Restrictions.eq("status","a");
			Criterion criterion2 = Restrictions.ge("jobStartDate",dateWithoutTime);
			lstJobOrders = findByCriteria(Order.desc("createdDateTime"),criterion1,criterion2);
			//lstJobOrders = findByCriteria(Order.desc("createdDateTime"),criterion1);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobOrders;
	}	
	@Transactional(readOnly=false)
	public List<JobOrder> getOnlyTodayAciveJob()
	{
		List<JobOrder> lstJobOrders = null;
		try 
		{
			Date dateWithoutTime = Utility.getDateWithoutTime();
			
			Criterion criterion1 = Restrictions.eq("status","a");
			Criterion criterion2 = Restrictions.eq("jobStartDate",dateWithoutTime);
			lstJobOrders = findByCriteria(Order.desc("createdDateTime"),criterion1,criterion2);
			//lstJobOrders = findByCriteria(Order.desc("createdDateTime"),criterion1);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobOrders;
	}
	@Transactional(readOnly=true)
	public JobOrder findJobByApiJobId(String apiJobId,DistrictMaster districtMaster)
	{
	     List <JobOrder> lstJobOrder = null;
		 Criterion criterion = Restrictions.eq( "apiJobId",apiJobId);
		 Criterion criterion1 = Restrictions.eq("districtMaster",districtMaster);
		 lstJobOrder = findByCriteria(criterion,criterion1);  
		 if(lstJobOrder==null || lstJobOrder.size()==0)
			 return null;
		 else
			 return lstJobOrder.get(0);
	}
	
	@Transactional(readOnly=true)
	public JobOrder findJobByApiJobId(String apiJobId)
	{
	     List <JobOrder> lstJobOrder = null;
		 Criterion criterion = Restrictions.eq( "apiJobId",apiJobId);
		 lstJobOrder = findByCriteria(criterion);  
		 if(lstJobOrder==null || lstJobOrder.size()==0)
			 return null;
		 else
			 return lstJobOrder.get(0);
	}
	
	@Transactional(readOnly=false)
	public List<JobOrder> findAllJobOrderbyDistrict(DistrictMaster districtMaster)
	{
		List<JobOrder> jobOrders = null;
		try 
		{
			/*
			7 days check
			Calendar cal1 = Calendar.getInstance();
			cal1.add(Calendar.DATE, -7);
			cal1.set(Calendar.HOUR_OF_DAY, 0);
			cal1.set(Calendar.MINUTE, 0);
			cal1.set(Calendar.SECOND, 0);
			cal1.set(Calendar.MILLISECOND, 0);
			System.out.println(cal1.getTime());*/

			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.MILLISECOND, 0);
			
			Criterion criterion1 = Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion2 = Restrictions.eq("createdForEntity",2);
			Criterion criterion3 = Restrictions.ge("jobEndDate",cal.getTime());
			Criterion criterion4 = Restrictions.eq("status","A");
			jobOrders = findByCriteria(criterion1,criterion2,criterion3,criterion4);

		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return jobOrders;
	}
	
	@Transactional(readOnly=true)
	public boolean findPortfolioNeededByApiJobId(String apiJobId)
	{	
		try{
		     List <JobOrder> lstJobOrder = null;
			 Criterion criterion = Restrictions.eq( "apiJobId",apiJobId);
			 lstJobOrder = findByCriteria(criterion);  
			 if(lstJobOrder==null || lstJobOrder.size()==0)
				 return true;
			 else{
				 if(lstJobOrder.get(0).getCreatedForEntity()==2){
					 return lstJobOrder.get(0).getDistrictMaster().getIsPortfolioNeeded();
				 }else{
					 return lstJobOrder.get(0).getSchool().get(0).getIsPortfolioNeeded(); 
				 }
			 }
		}catch(Exception e){
			return true;
		}
	}
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public Map<Integer,JobOrder> findByAllJobOrder() 
	{
		Session session = getSession();
		Criterion criterion1 = Restrictions.eq("status","a");
		List result = session.createCriteria(getPersistentClass())
		.add(criterion1)
		.list();
		Map<Integer,JobOrder> jobOrderMap = new HashMap<Integer, JobOrder>();
		JobOrder jobOrder = null;
		int i=0;
		for (Object object : result) {
			jobOrder=((JobOrder)object);
			jobOrderMap.put(new Integer(""+i),jobOrder);
			i++;
		}
		return jobOrderMap;
	}
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public Map<Integer,JobOrder> findByAllJob(List jobs) 
	{
		Session session = getSession();
		Criterion criterion1 = Restrictions.eq("status","a");
		Criterion criterion2 = Restrictions.in("apiJobId",jobs);	
		List result = session.createCriteria(getPersistentClass())
		.add(criterion1)
		.add(criterion2)
		.list();
		Map<Integer,JobOrder> jobOrderMap = new HashMap<Integer, JobOrder>();
		JobOrder jobOrder = null;
		int i=0;
		for (Object object : result) {
			jobOrder=((JobOrder)object);
			jobOrderMap.put(new Integer(""+i),jobOrder);
			i++;
		}
		return jobOrderMap;
	}
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public Map<Integer,JobOrder> findByAllJobOrders(List<Integer> jobIds) 
	{
		Session session = getSession();
		Criterion criterion2 = Restrictions.in("jobId",jobIds);
		List result = session.createCriteria(getPersistentClass())
		.add(criterion2)
		.list();
		Map<Integer,JobOrder> jobOrderMap = new HashMap<Integer, JobOrder>();
		JobOrder jobOrder = null;
		int i=0;
		for (Object object : result) {
			jobOrder=((JobOrder)object);
			jobOrderMap.put(new Integer(""+i),jobOrder);
			i++;
		}
		return jobOrderMap;
	}
	
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public List findByJobOrders() 
	{
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		
		Session session = getSession();
		
		Query query = session.createSQLQuery(
		" select jo.jobId,jo.districtId,sij.schoolId,DATEDIFF(now(),jo.jobStartDate) as jobActiveDays,jo.noOfExpHires," +
		" sij.noOfSchoolExpHires "+
		" from joborder jo " +
		" left join schoolinjoborder sij on jo.jobId=sij.jobId join districtmaster dm on jo.districtId = dm.districtId " +
		" where jo.status=:statusCode and (jo.jobStartDate <= :from and jo.jobEndDate >= :to) ");
		
		query.setParameter("statusCode", "A");
		query.setParameter("from", cal.getTime());
		query.setParameter("to",cal.getTime());
            
		List<Object[]> rows = query.list();
		return rows;
		
	}
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public Map<Integer,JobOrder> findByAllJobOrderWithInActive() 
	{
		Session session = getSession();
		List result = session.createCriteria(getPersistentClass())
		.list();
		Map<Integer,JobOrder> jobOrderMap = new HashMap<Integer, JobOrder>();
		JobOrder jobOrder = null;
		int i=0;
		for (Object object : result) {
			jobOrder=((JobOrder)object);
			jobOrderMap.put(new Integer(""+i),jobOrder);
			i++;
		}
		return jobOrderMap;
	}
	@Transactional(readOnly=false)
	public List<JobOrder> findJobBySubject(SubjectMaster subjectMaster)
	{
		List<JobOrder> lstJobOrder= null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("subjectMaster",subjectMaster);
			lstJobOrder = findByCriteria(criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		
		return lstJobOrder;
	}
	
	@Transactional(readOnly=false)
	public List<JobOrder> findJobByDistrict(DistrictMaster  districtMaster)
	{
		List<JobOrder> lstJobOrder= null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("districtMaster",districtMaster);
			lstJobOrder = findByCriteria(criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		
		return lstJobOrder;
	}
	@Transactional(readOnly=false)
	public boolean findExistJobId(DistrictMaster  districtMaster,Integer jobId, String refNo)
	{
		List<JobOrder> lstJobOrder= null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion2 = Restrictions.eq("apiJobId",refNo);
			if(jobId!=null && jobId!=0){
				Criterion criterion3=Restrictions.ne("jobId", jobId);
				lstJobOrder = findByCriteria(criterion1,criterion2,criterion3);
			}else{
				lstJobOrder = findByCriteria(criterion1,criterion2);	
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(lstJobOrder.size()>0)
			return true;
		else
			return false;
	}
	
	@Transactional(readOnly=false)
	public List<JobOrder> findAllActiveJobOrderbyDistrict(Order sortOrderStrVal,int start,int end,HeadQuarterMaster headQuarterMaster,BranchMaster branchMaster,DistrictMaster districtMaster, List<Integer> lstJobs,SchoolMaster schoolMaster, List<Integer> schoolJob,boolean isAll)
	{
		List<JobOrder> jobOrders = null;
		try 
		{
			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.MILLISECOND, 0);
			
			Criterion criterion1 = Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion3 = Restrictions.le("jobStartDate",cal.getTime());
			Criterion criterion4 = Restrictions.ge("jobEndDate",cal.getTime());
			Criterion criterion5 = Restrictions.eq("status","A");
			Criterion criterion6 = null;

			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());

			
				criteria.add(criterion3);
				criteria.add(criterion4);
				criteria.add(criterion5);
			if(headQuarterMaster!=null)
				criteria.add(Restrictions.eq("headQuarterMaster",headQuarterMaster));
			if(branchMaster!=null)
				criteria.add(Restrictions.eq("branchMaster",branchMaster));
			if(districtMaster!=null && schoolMaster==null)
				criteria.add(criterion1);
			if(lstJobs.size()>0)
			{
				criterion6 = Restrictions.not(Restrictions.in("jobId",lstJobs));
				criteria.add(criterion6);
			}
			if(districtMaster!=null && schoolMaster!=null && schoolJob.size()>0)
			{
				criteria.add(criterion1);
				criteria.add(Restrictions.in("jobId",schoolJob));
			}

			//criteria.addOrder(sortOrderStrVal);
			if(!isAll)
			{
				criteria.setFirstResult(start);
				criteria.setMaxResults(end);
			}

			if(sortOrderStrVal!=null)
				criteria.addOrder(sortOrderStrVal);

			jobOrders = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return jobOrders;
	}
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public List<JobOrder> findByAllJobOrders(Integer[] jobIds) 
	{
		Session session = getSession();
		List<JobOrder> result = new ArrayList<JobOrder>();
		if(jobIds.length>0)
		{
			Criterion criterion2 = Restrictions.in("jobId",jobIds);
			result = session.createCriteria(getPersistentClass())
			.add(criterion2)
			.list();
		}
		return result;
	}
	
	
	/* @Start
	 * @Ashish Kumar
	 * @Description :: find JobOrder By Job Category and Subject List
	 * */
		
		// Search By Subject List
		@Transactional(readOnly=false)
		public List<JobOrder> findJobOrderBySubjectList(List<SubjectMaster> ObjSubjectList)
		{
			List<JobOrder> jobOrders = null;
			try 
			{
				Criterion criterion5 = Restrictions.in("subjectMaster",ObjSubjectList); 	
				Criterion criterion6 = Restrictions.eq("status","A");
	
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
	
				criteria.add(criterion5);
				criteria.add(criterion6);
				jobOrders = criteria.list();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}		
			return jobOrders;
		}
		
		// Search By Job category
		@Transactional(readOnly=false)
		public List<JobOrder> findJobOrderByJobCategoryMaster(JobCategoryMaster jobCategoryMaster)
		{
			List<JobOrder> jobOrders = null;
			try 
			{
				Criterion criterion5 = Restrictions.eq("jobCategoryMaster",jobCategoryMaster); 	
				Criterion criterion6 = Restrictions.eq("status","A");
	
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
	
				criteria.add(criterion5);
				criteria.add(criterion6);
				jobOrders = criteria.list();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}		
			return jobOrders;
		}
		
		
		// Search By District MAster List
		@Transactional(readOnly=false)
		public List<JobOrder> findJobOrderByDistrictList(List<DistrictMaster> dmLst)
		{
			List<JobOrder> jobOrders = null;
			try 
			{
				Date dateWithoutTime = Utility.getDateWithoutTime();
				
				Criterion criterion2 = Restrictions.le("jobStartDate",dateWithoutTime);
				Criterion criterion3 = Restrictions.ge("jobEndDate",dateWithoutTime);
				Criterion criterion5 = Restrictions.in("districtMaster",dmLst); 	
				Criterion criterion6 = Restrictions.eq("status","A");
				jobOrders =findByCriteria(criterion2,criterion3,criterion5,criterion6); 
			} 
			catch (Exception e) {
				e.printStackTrace();
			}		
			return jobOrders;
		}
		
		
		@Transactional(readOnly=false)
		public List<JobOrder> findJobOrderByDistrict(DistrictMaster districtMaster)
		{
			List<JobOrder> jobOrders = null;
			try 
			{
				Criterion criterion5 = Restrictions.eq("districtMaster",districtMaster); 	
				Criterion criterion6 = Restrictions.eq("status","A");
				Criterion criterion7 = Restrictions.ne("isInviteOnly",true);
				jobOrders = findByCriteria(criterion5,criterion6,criterion6); 
			} 
			catch (Exception e) {
				e.printStackTrace();
			}		
			return jobOrders;
		}
	
		@Transactional(readOnly=false)
		public List<JobOrder> findAllActiveJobForJobboard(Order sortOrderStrVal,int start,int end,Criterion criterion1,Criterion criterion2,Criterion criterion3,Criterion criterion4,boolean isAll)
		{
			List<JobOrder> jobOrders = null;
			try 
			{
				
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				Criterion criterion5 = Restrictions.ne("isInviteOnly",true);
				Criterion criterion6 = Restrictions.ne("isVacancyJob",true);
				
				criteria.add(criterion1);
				criteria.add(criterion2);
				criteria.add(criterion3);
				criteria.add(criterion4);
				criteria.add(criterion5);
				criteria.add(criterion6);
				
				if(!isAll)
				{
					criteria.setFirstResult(start);
					criteria.setMaxResults(end);
				}

				if(sortOrderStrVal!=null)
					criteria.addOrder(sortOrderStrVal);

				jobOrders = criteria.list();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}		
			return jobOrders;
		}
		
		/**
		 * Ankit Sharma (31-12-2014)
		 * @param criterion1
		 * @param criterion2
		 * @param criterion3
		 * @param criterion4
		 * @return
		 */
		@Transactional(readOnly=false)
		public List<JobOrder> findAllActiveJobForLocations(Criterion criterion1,Criterion criterion2,Criterion criterion3,Criterion criterion4)
		{
			List<JobOrder> jobOrders = null;
			try 
			{				
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				Criterion criterion5 = Restrictions.ne("isInviteOnly",true);
				
				criteria.add(criterion1);
				criteria.add(criterion2);
				criteria.add(criterion3);
				criteria.add(criterion4);
				criteria.add(criterion5);
				
				jobOrders = criteria.list();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}		
			return jobOrders;
		}
		
		@Transactional(readOnly=false)
		public List<JobOrder> findAllActiveJobForJobboardWithDays(Order sortOrderStrVal,int start,int end,Criterion criterion1,Criterion criterion2,Criterion criterion3,Criterion criterion4,boolean isAll)
		{
			List<JobOrder> jobOrders = null;
			try 
			{			
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				Criterion criterion5 = Restrictions.ne("isInviteOnly",true);
				criteria.add(criterion1);
				criteria.add(criterion2);
				criteria.add(criterion3);
				criteria.add(criterion4);
				criteria.add(criterion5);
				
				if(!isAll)
				{
					criteria.setFirstResult(start);
					criteria.setMaxResults(end);
				}

				if(sortOrderStrVal!=null)
					criteria.addOrder(sortOrderStrVal);

				jobOrders = criteria.list();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}		
			return jobOrders;
		}
		
		
	/* @End
	 * @Ashish Kumar
	 * @Description :: find JobOrder By Job Category and Subject List
	 * */
		
		@Transactional(readOnly=false)
		public List<JobOrder> findJobOrdersbyDistrict(DistrictMaster districtMaster)
		{
			List<JobOrder> jobOrders = null;
			try 
			{
				Criterion criterion1 = Restrictions.eq("districtMaster",districtMaster);
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				criteria.add(criterion1);
				jobOrders = criteria.list();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}		
			return jobOrders;
		}
		
		/*
		 * @author : Vishwanath Kumar
		 * @discription : 
		 */

		@Transactional(readOnly=false)
		public List<DistrictMaster> getUniqueDistrictsByJobOrderStartDate()
		{
			
				
			List<DistrictMaster> districtMasters = new ArrayList<DistrictMaster>();
			DistrictMaster districtMaster = null;
			try{
				
				Date dateWithoutTime = Utility.getDateWithoutTime();
			    
				Session session = getSession();
				List result = session.createCriteria(getPersistentClass()) 
				.add(Restrictions.eq("status","a")) 
				.add(Restrictions.eq("jobStartDate",dateWithoutTime)) 
				.setProjection(Projections.projectionList()
						.add(Projections.groupProperty("districtMaster"))
						//.add(Projections.property("districtMaster"))  
				).list();

				districtMasters = result;
			}catch (Exception e) {
				e.printStackTrace();
			}
			return districtMasters;
		}
		@Transactional(readOnly=false)
		public List<JobOrder> getJobOrdersByStartDate()
		{
			List<JobOrder> jobOrders = new ArrayList<JobOrder>();
			try{
				
				Date dateWithoutTime = Utility.getDateWithoutTime();
			    
				Session session = getSession();
				List result = session.createCriteria(getPersistentClass()) 
				.add(Restrictions.eq("status","a")) 
				.add(Restrictions.eq("jobStartDate",dateWithoutTime)) 
				.list();
				jobOrders = result;
			}catch (Exception e) {
				e.printStackTrace();
			}
			return jobOrders;
		}
		
		
		@Transactional(readOnly=false)
		public List<JobOrder> getJobOrdersByJobIds_msu(List<Integer> jobOrdersLst)
		{
			List<JobOrder> listjobOrder=new ArrayList<JobOrder>();
			try 
			{
				Criterion criterion =	Restrictions.in("jobId",jobOrdersLst);
				listjobOrder = findByCriteria(criterion);	
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			
			return listjobOrder;
		}
		
		
		@Transactional(readOnly=false)
		public List<JobOrder> getJobOrderWithSubject()
		{
			List<JobOrder> listjobOrder=new ArrayList<JobOrder>();
			try 
			{
				Criterion criterion =	Restrictions.isNotNull("subjectMaster");
				listjobOrder = findByCriteria(criterion);	
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			
			return listjobOrder;
		}
		
		@Transactional(readOnly=false)
		public List<JobOrder> getJobOrderBygeoZone(GeoZoneMaster geoZoneMaster)
		{
			List<JobOrder> listjobOrder=new ArrayList<JobOrder>();
			try 
			{
				Criterion criterion =	Restrictions.eq("geoZoneMaster",geoZoneMaster);
				listjobOrder = findByCriteria(criterion);	
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			
			return listjobOrder;
		}

		@Transactional(readOnly=false)
		public HashMap<Integer, Boolean> getUsedZoneInJobOrder()
		{
			List<JobOrder> jobOrders=null;
			HashMap<Integer, Boolean>map=null;
			try{
				Criterion criterion1=Restrictions.isNotNull("geoZoneMaster");
				jobOrders=findByCriteria(criterion1);
				map=new HashMap<Integer, Boolean>();
				for(JobOrder pojo:jobOrders )
				{
					map.put(pojo.getGeoZoneMaster().getGeoZoneId(), false);
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
			return map;
		}
		

		@Transactional(readOnly=false)
		public List<JobOrder> getListByTitle(List<String> jobTitle)
		{
			List<JobOrder> listjobOrder=new ArrayList<JobOrder>();
			
			System.out.println(" jobTitle  :::: "+jobTitle.size());
			
			if(jobTitle.size()>0)
		     {
				try 
				{
					Criterion criterion1 =	Restrictions.in("jobTitle",jobTitle);
					//Criterion criterion2 =  Restrictions.eq("isPoolJob", 1); 
					//Criterion criterion3 =  Restrictions.ge("isPoolJob", 0);
					listjobOrder = findByCriteria(criterion1);	
				} 
				catch (Exception e) 
				{
					e.printStackTrace();
				}
		}
			return listjobOrder;
		}

		@Transactional(readOnly=false)
		public List<JobOrder> findAllJobWithPoolCondition(DistrictMaster districtMaster)
		{
			System.out.println("districtMaster :: "+districtMaster.getDistrictId());
			
			List<JobOrder> listjobOrder=new ArrayList<JobOrder>();
			try 
			{
				Date dateWithoutTime = Utility.getDateWithoutTime();
				
				Criterion criterion1 = Restrictions.le("jobStartDate",dateWithoutTime);
				Criterion criterion2 = Restrictions.ge("jobEndDate",dateWithoutTime);
				Criterion criterion3 = Restrictions.ne("isPoolJob",1); 	
				Criterion criterion4 = Restrictions.eq("status","A");
				Criterion criterion5 = Restrictions.eq("approvalBeforeGoLive",1);
				Criterion criterion6 = Restrictions.ne("isInviteOnly",true);
				Criterion criterion7 = Restrictions.ne("isVacancyJob",true);
				Criterion criterion8 = Restrictions.eq("districtMaster",districtMaster);
				listjobOrder = findByCriteria(criterion1,criterion2,criterion3,criterion4,criterion5,criterion6,criterion7,criterion8);
				
				/*Criterion criterion1 = Restrictions.le("jobStartDate",dateWithoutTime);
				Criterion criterion2 = Restrictions.ge("jobEndDate",dateWithoutTime);
				Criterion criterion3 = Restrictions.ne("isPoolJob",1); 	
				Criterion criterion4 = Restrictions.eq("status","A");
				Criterion criterion5 = Restrictions.eq("districtMaster",districtMaster);
				
				listjobOrder = findByCriteria(criterion1,criterion2,criterion3,criterion4,criterion5);*/
				
				System.out.println(" listjobOrder with isPoolJob Not Eq 1 :: "+listjobOrder.size());
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			
			return listjobOrder;
		}
		
		@Transactional(readOnly=false)
		public List<JobOrder> findAllJobWithPoolCondition()
		{
			List<JobOrder> listjobOrder=new ArrayList<JobOrder>();
			try 
			{
				Date dateWithoutTime = Utility.getDateWithoutTime();
				
				Criterion criterion1 = Restrictions.le("jobStartDate",dateWithoutTime);
				Criterion criterion2 = Restrictions.ge("jobEndDate",dateWithoutTime);
				Criterion criterion3 = Restrictions.ne("isPoolJob",1); 	
				Criterion criterion4 = Restrictions.eq("status","A");
				Criterion criterion5 = Restrictions.eq("approvalBeforeGoLive",1);
				Criterion criterion6 = Restrictions.ne("isInviteOnly",true);
				Criterion criterion7 = Restrictions.ne("isVacancyJob",true);
				
				listjobOrder = findByCriteria(criterion1,criterion2,criterion3,criterion4,criterion5,criterion6,criterion7);
				
				System.out.println(" listjobOrder with isPoolJob Not Eq 1 :: "+listjobOrder.size());
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			
			return listjobOrder;
		}
		
		@Transactional(readOnly=false)
		public List<JobOrder> findAllJobWithPoolCondition(Order sortOrderStrVal)
		{
			List<JobOrder> listjobOrder=new ArrayList<JobOrder>();
			try 
			{
				Date dateWithoutTime = Utility.getDateWithoutTime();
				
				Criterion criterion1 = Restrictions.le("jobStartDate",dateWithoutTime);
				Criterion criterion2 = Restrictions.ge("jobEndDate",dateWithoutTime);
				Criterion criterion3 = Restrictions.ne("isPoolJob",1); 	
				Criterion criterion4 = Restrictions.eq("status","A");
				Criterion criterion5 = Restrictions.eq("approvalBeforeGoLive",1);
				Criterion criterion6 = Restrictions.ne("isInviteOnly",true);
				Criterion criterion7 = Restrictions.ne("isVacancyJob",true);
				
				if(sortOrderStrVal!=null)
				{
				listjobOrder = findByCriteria(sortOrderStrVal,criterion1,criterion2,criterion3,criterion4,criterion5,criterion6,criterion7);
				}else{
				listjobOrder = findByCriteria(Order.desc("createdDateTime"),criterion1,criterion2,criterion3,criterion4,criterion5,criterion6,criterion7);
			    }
				System.out.println(" listjobOrder with isPoolJob Not Eq 1 :: "+listjobOrder.size());
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			
			return listjobOrder;
		}
		
		@Transactional(readOnly=false)
		public List<JobOrder> findJobWithPoolConditionByJobId(Integer jobId)
		{
			List<JobOrder> listjobOrder=new ArrayList<JobOrder>();
			try 
			{
				Date dateWithoutTime = Utility.getDateWithoutTime();
				
				Criterion criterion1 = Restrictions.le("jobStartDate",dateWithoutTime);
				Criterion criterion2 = Restrictions.ge("jobEndDate",dateWithoutTime);
				Criterion criterion3 = Restrictions.eq("status","A");
				Criterion criterion4 = Restrictions.eq("jobId",jobId);
				listjobOrder = findByCriteria(criterion1,criterion2,criterion3,criterion4);
				
				System.out.println(" listjobOrder with isPoolJob Not Eq 1 :: "+listjobOrder.size());
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			
			return listjobOrder;
		}
		
		@Transactional(readOnly=false)
		public List<JobOrder> findAllJobByDistrict(DistrictMaster districtMaster,List<JobOrder> jobOrders)
		{
			List<JobOrder> listjobOrder=new ArrayList<JobOrder>();
			try 
			{
				Criterion criterion1 = Restrictions.eq("districtMaster",districtMaster);
				if(districtMaster!=null)
					listjobOrder = findByCriteria(criterion1);
				else
					listjobOrder = findByCriteria();
				
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			
			return listjobOrder;
		}
		
		
		
		@Transactional(readOnly=false)
		public List<JobOrder> findByJobIDS(List<Integer> jobOrdersLst)
		{
			List<JobOrder> listjobOrder=new ArrayList<JobOrder>();
			try 
			{
				Criterion criterion =	Restrictions.in("jobId",jobOrdersLst);
				listjobOrder = findByCriteria(criterion);	
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			
			return listjobOrder;
		}
		
		@Transactional(readOnly=false)
		public List<JobOrder> findByJobCategery(List<Integer> jobCategoryIds,Integer districtId)
		{
			List<JobOrder> jobOrders = new ArrayList<JobOrder>();
			try 
			{
				DistrictMaster dm = districtMasterDAO.findById(districtId, false, false);
				Criterion criterion5=null;
				List<JobCategoryMaster> lstjoCategoryMasters = jobCategoryMasterDAO.findByCategoryID(jobCategoryIds); 
				if(lstjoCategoryMasters!=null && lstjoCategoryMasters.size()>0)
					criterion5	=Restrictions.in("jobCategoryMaster",lstjoCategoryMasters); 	
				
				Criterion criterion6 = Restrictions.eq("districtMaster",dm);
	
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
	
				if(criterion5!=null)
				criteria.add(criterion5);
				
				criteria.add(criterion6);
				jobOrders = criteria.list();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}		
			
			return jobOrders;
		}
		
		@Transactional(readOnly=false)
		public List<JobOrder> findByJobCategeryWithSubCate(List<Integer> jobCategoryIds,Integer districtId)
		{
			List<JobOrder> jobOrders = new ArrayList<JobOrder>();
			try 
			{
				DistrictMaster dm = districtMasterDAO.findById(districtId, false, false);
				List<JobCategoryMaster> lstjoCategoryMasters = jobCategoryMasterDAO.findByCategoryID(jobCategoryIds); 
				List<JobCategoryMaster> lstSubJobcategory = jobCategoryMasterDAO.findAllJobSubCategoryByJobCateIds(lstjoCategoryMasters,dm); 
				lstjoCategoryMasters.addAll(lstSubJobcategory);
				
				Criterion criterion5	= Restrictions.in("jobCategoryMaster",lstjoCategoryMasters); 	
				Criterion criterion6 =  Restrictions.eq("districtMaster",dm);
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
	
				criteria.add(criterion5);
				criteria.add(criterion6);
				jobOrders = criteria.list();
				
				
			} 
			catch (Exception e) {
				e.printStackTrace();
			}		
			return jobOrders;
		}
		
		
		
		
		@Transactional(readOnly=false)
		@SuppressWarnings("unchecked")
		public List<JobOrder> findJobOrderbyWithAndWithoutDistrict(DistrictMaster districtMaster, int sortingcheck, Order sortOrderStrVal,int startPos,int limit,String endfromDate, String endtoDate,String sfromDate, String stoDate,List<Integer> lstJobIds, boolean nbyAflag,List<Integer> nbyAJobIds,boolean exportcheck)
		{
			List<JobOrder> jobOrders = new ArrayList<JobOrder>();
			try 
			{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				Date fDate=null;
				Date tDate=null;
				Date endfDate=null;
				Date endtDate =null;
				
				if(!sfromDate.equals(""))
				   fDate = Utility.getCurrentDateFormart(sfromDate);
				if(!stoDate.equals(""))
				   tDate = Utility.getCurrentDateFormart(stoDate);
				
				if(!endfromDate.equals(""))
				  endfDate = Utility.getCurrentDateFormart(endfromDate);
				if(!endtoDate.equals(""))
				  endtDate= Utility.getCurrentDateFormart(endtoDate);
				
				if(districtMaster!=null)
				 criteria.add( Restrictions.eq("districtMaster",districtMaster));
				
				if(sortingcheck==0)
				 criteria.addOrder(sortOrderStrVal);
				
				Criteria c1= null;
				if(sortingcheck==1){
					c1 = criteria.createCriteria("districtMaster");
					c1.addOrder(sortOrderStrVal);
				}
				
				if(lstJobIds!=null && lstJobIds.size()>0 && nbyAflag==false){
					criteria.add(Restrictions.in("jobId",lstJobIds));
				}else if( nbyAflag==true){
					criteria.add(Restrictions.not(Restrictions.in("jobId",nbyAJobIds)));
					if(lstJobIds!=null && lstJobIds.size()>0)
					 criteria.add(Restrictions.in("jobId",lstJobIds));
				}
				
				
				if(fDate!=null && tDate!=null){
					criteria.add(Restrictions.ge("jobStartDate",fDate)).add(Restrictions.le("jobStartDate",tDate));
				}else if(fDate!=null && tDate==null){
					criteria.add(Restrictions.ge("jobStartDate",fDate));
				}else if(fDate==null && tDate!=null){
					criteria.add(Restrictions.le("jobStartDate",tDate));
				}
				if(endfDate!=null && endtDate!=null){
					criteria.add(Restrictions.ge("jobEndDate",endfDate)).add(Restrictions.le("jobEndDate",endtDate));
				}else if(endfDate!=null && endtDate==null){
					criteria.add(Restrictions.ge("jobEndDate",endfDate));
				}else if(endfDate==null && endtDate!=null){
					criteria.add(Restrictions.le("jobEndDate",endtDate));
				}
				if(!exportcheck){
					criteria.setFirstResult(startPos);
					criteria.setMaxResults(limit);
				}
				jobOrders = criteria.list();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}		
			return jobOrders;
		}
		
		@Transactional(readOnly=false)
		@SuppressWarnings("unchecked")
		public int findTotalJobOrderbyWithAndWithoutDistrict(DistrictMaster districtMaster, int sortingcheck, Order sortOrderStrVal,int startPos,int limit,String endfromDate, String endtoDate,String sfromDate, String stoDate,List<Integer> lstJobIds, boolean nbyAflag,List<Integer> nbyAJobIds)
		{
			List<JobOrder> jobOrders = new ArrayList<JobOrder>();
			try 
			{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				Date fDate=null;
				Date tDate=null;
				Date endfDate=null;
				Date endtDate =null;
				
				if(!sfromDate.equals(""))
				   fDate = Utility.getCurrentDateFormart(sfromDate);
				if(!stoDate.equals(""))
				   tDate = Utility.getCurrentDateFormart(stoDate);
				
				if(!endfromDate.equals(""))
				  endfDate = Utility.getCurrentDateFormart(endfromDate);
				if(!endtoDate.equals(""))
				  endtDate= Utility.getCurrentDateFormart(endtoDate);
				
				if(districtMaster!=null)
				 criteria.add( Restrictions.eq("districtMaster",districtMaster));
				
				if(sortingcheck==0)
				 criteria.addOrder(sortOrderStrVal);
				
				Criteria c1= null;
				if(sortingcheck==1){
					c1 = criteria.createCriteria("districtMaster");
					c1.addOrder(sortOrderStrVal);
				}
				
				if(lstJobIds!=null && lstJobIds.size()>0 && nbyAflag==false){
					criteria.add(Restrictions.in("jobId",lstJobIds));
				}else if( nbyAflag==true){
					criteria.add(Restrictions.not(Restrictions.in("jobId",nbyAJobIds)));
					if(lstJobIds!=null && lstJobIds.size()>0)
					 criteria.add(Restrictions.in("jobId",lstJobIds));
				}
				
				
				if(fDate!=null && tDate!=null){
					criteria.add(Restrictions.ge("jobStartDate",fDate)).add(Restrictions.le("jobStartDate",tDate));
				}else if(fDate!=null && tDate==null){
					criteria.add(Restrictions.ge("jobStartDate",fDate));
				}else if(fDate==null && tDate!=null){
					criteria.add(Restrictions.le("jobStartDate",tDate));
				}
				
				if(endfDate!=null && endtDate!=null){
					criteria.add(Restrictions.ge("jobEndDate",endfDate)).add(Restrictions.le("jobEndDate",endtDate));
				}else if(endfDate!=null && endtDate==null){
					criteria.add(Restrictions.ge("jobEndDate",endfDate));
				}else if(endfDate==null && endtDate!=null){
					criteria.add(Restrictions.le("jobEndDate",endtDate));
				}
               criteria.setProjection(Projections.rowCount());
				
				List results = criteria.list();
				int rowCount = (Integer) results.get(0);
				return rowCount;
			} 
			catch (Exception e) {
				e.printStackTrace();
			}		
			return 0;
		}

		@Transactional(readOnly=false)
		public List<JobOrder> getJobOrderBygeoZoneRole(GeoZoneMaster geoZoneMaster,UserMaster userMaster)
		{
			List<JobOrder> listjobOrder=new ArrayList<JobOrder>();
			try 
			{
				
				Criterion criterion =	Restrictions.eq("geoZoneMaster",geoZoneMaster);
				Criterion criterion2 =	Restrictions.or(criterion, Restrictions.isNull("geoZoneMaster"));
				listjobOrder = findByCriteria(criterion2);	
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			
			return listjobOrder;
		}
		
		//----------- Deepak :::22-05-2015 ----------------
		@Transactional(readOnly=false)
		public List<JobOrder> getJobApplicationStatusRole(Integer jobStatusId)
		{
			List<JobOrder> listjobOrder=new ArrayList<JobOrder>();
			try 
			{
				
				Criterion criterion =	Restrictions.eq("jobApplicationStatus",jobStatusId);

				listjobOrder = findByCriteria(criterion);	
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			
			return listjobOrder;
		}
		
//----------Rahul Tyagi:22/11/2014-----------------
@Transactional(readOnly=false)
@SuppressWarnings("unchecked")		
public List<JobOrder> getJobOrderByPool(Criterion crt[],Order ord[],int start,int max)
{
	System.out.println("getSession");
	
	Criteria criteria=null;;
	try {
		Session session = getSession();
		System.out.println("no session");
		criteria = session.createCriteria(getPersistentClass());
		   for(Criterion cr:crt)
		   {
				   criteria.add(cr);
		   }
		   for(Order or:ord)
		   {
				criteria.addOrder(or);   
		   }
		   criteria.setFirstResult(start);
				criteria.setMaxResults(max);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
   return criteria.list();
}

@Transactional(readOnly=false)
public List<JobOrder> getJOByDID_APIJID(DistrictMaster districtMaster,String apiJobId)
{
	List<JobOrder> lstJobOrder= new ArrayList<JobOrder>();
	try 
	{
		Criterion criterion1 = Restrictions.eq("districtMaster",districtMaster);
		Criterion criterion2 = Restrictions.eq("apiJobId",apiJobId);
		lstJobOrder = findByCriteria(criterion1,criterion2);	
	} 
	catch (Exception e) {
		e.printStackTrace();
	}		
	return lstJobOrder;
}

@Transactional(readOnly=false)
public List<JobOrder> getActiveJobs(List<DistrictMaster> districtMasters)
{
	List<JobOrder> lstJobOrder= new ArrayList<JobOrder>();
	try 
	{
		
		Date dateWithoutTime = Utility.getDateWithoutTime();
		
		Criterion criterion1 = Restrictions.eq("status","a");
		Criterion criterion2 = Restrictions.le("jobStartDate",dateWithoutTime);
		Criterion criterion3 = Restrictions.ge("jobEndDate",dateWithoutTime);
		Criterion criterion4 = Restrictions.eq("approvalBeforeGoLive",1);
		Criterion criterion5 = Restrictions.in("districtMaster",districtMasters);
		
		lstJobOrder = findByCriteria(criterion1,criterion2,criterion3,criterion4,criterion5);	
	} 
	catch (Exception e) {
		e.printStackTrace();
	}		
	return lstJobOrder;
}

/*
 * @author : Ram Nath
 * @discription : It is used to get Only Job Id By districtId 
 */
@Transactional(readOnly=false)
public List<String[]> findAllJobOrderByDistrict(Integer districtId){
	List<String[]> jobOrderList =  new ArrayList<String[]>();
	try{
		Session session = getSession();	
		Criteria criteria = session.createCriteria(getPersistentClass());			
		criteria.createAlias("districtMaster", "dm")
	    .setProjection( Projections.projectionList()
	        .add( Projections.property("jobId"), "jobId" )		        
	    );
		criteria.add(Restrictions.eq("dm.districtId",districtId));
		criteria.add(Restrictions.eq("status","A"));
		jobOrderList=criteria.list();			
	
	}catch(Exception e){
		e.printStackTrace();
	}			
		return jobOrderList;	
}
@Transactional(readOnly=false)
public List<String[]> findMinJobEndDateFromJobOrderByDistrict(Integer districtId,Integer jobId){
	List<String[]> jobOrderList =  new ArrayList<String[]>();
	//JobOrder
	try{
		Session session = getSession();	
		Criteria criteria = session.createCriteria(getPersistentClass());			
		criteria.createAlias("districtMaster", "dm")
	    .setProjection( Projections.projectionList()
	        .add( Projections.max("jobEndDate"), "jobEndDateMax" )
	        .add( Projections.min("jobEndDate"), "jobEndDateMin" )
	    );
		criteria.add(Restrictions.eq("dm.districtId",districtId));
		if(jobId!=null && jobId!=0)
			criteria.add(Restrictions.eq("jobId",jobId));
		jobOrderList=criteria.list();			
	
	}catch(Exception e){
		e.printStackTrace();
	}			
		return jobOrderList;	
}	
@Transactional(readOnly=false)
public List<JobOrder> findJobOrderByDistrict(Integer districtId,Integer jobId,Date endStartDate, Date endToDate, int schoolId){
	List<JobOrder> jobOrderList =  new ArrayList<JobOrder>();
	List<JobOrder > lstJobOrderBySchoolId=new ArrayList<JobOrder>();
	try{
		Session session=getSession();			
		Criteria criteria=session.createCriteria(getPersistentClass());	
		//by school id
		if(schoolId!=0){
			Criteria criteria1=session.createCriteria(SchoolInJobOrder.class);	
			criteria1.add(Restrictions.eq("schoolId", schoolMasterDAO.findById(new Long(schoolId), false, false)));
			criteria1.createCriteria("jobId").add(Restrictions.eq("status", "A"));
			List<SchoolInJobOrder> lstSchInJOrder=criteria1.list();
			for(SchoolInJobOrder sijo:lstSchInJOrder)
				if(sijo.getJobId()!=null)
					lstJobOrderBySchoolId.add(sijo.getJobId());						
		}	
		
		if(districtId!=0){
			criteria.add(Restrictions.eq("districtMaster",districtMasterDAO.findById(districtId, false, false)));
			criteria.add(Restrictions.eq("status", "A"));
		}
		
		if(jobId!=0)
			criteria.add(Restrictions.eq("jobId",jobId));
		
		criteria.add(Restrictions.ge("jobEndDate",endStartDate)).add(Restrictions.le("jobEndDate",endToDate));
		jobOrderList=criteria.list();
		//System.out.println("lstJobOrderBySchoolId=="+lstJobOrderBySchoolId);	
		//System.out.println("jobOrderList=="+jobOrderList);	
		if(schoolId!=0)
			jobOrderList.retainAll(lstJobOrderBySchoolId);
		//System.out.println("jobOrderList1111=="+jobOrderList);
	}catch (Exception e) {
		e.printStackTrace();
	}
	return jobOrderList;
}
/*end by Ram nath*/
@Transactional(readOnly=false)
public List<JobOrder> findJobOrderListByApiJobIdList(DistrictMaster districtMaster,List<String> apiJobIdLst){
	List<JobOrder> jobOrderList =  new ArrayList<JobOrder>();
	
	try{
		
		Criterion criterion3 = Restrictions.in("apiJobId",apiJobIdLst);
		if(districtMaster!=null)
		{
			Criterion criterion2 = Restrictions.eq("districtMaster",districtMaster);
			jobOrderList = findByCriteria(criterion2,criterion3);
		}
		else
		{
			jobOrderList = findByCriteria(criterion3);
		}
	}catch(Exception e){
		e.printStackTrace();
	}			
		return jobOrderList;	
}

@Transactional(readOnly=false)
public List<JobOrder> getListByCateApiJobCatDist(List<String> apiJobCodes,DistrictMaster districtMaster,List<JobCategoryMaster> jobCategoryMaster)
{
	List<JobOrder> listjobOrder=new ArrayList<JobOrder>();
	
	if(jobCategoryMaster.size()>0)
     {
		try 
		{
			//Criterion criterion1 =	Restrictions.in("apiJobId",apiJobCodes);
			Criterion criterion2 =  Restrictions.eq("districtMaster", districtMaster); 
			Criterion criterion3 = Restrictions.in("jobCategoryMaster",jobCategoryMaster);
			listjobOrder = findByCriteria(criterion2,criterion3);	
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
}
	return listjobOrder;
}

@Transactional(readOnly=false)
public List<JobOrder> findAllJobForInternalJobBoard()
{
	List<JobOrder> listjobOrder=new ArrayList<JobOrder>();
	try 
	{
		Date dateWithoutTime = Utility.getDateWithoutTime();
		
		Criterion criterion1 = Restrictions.le("jobStartDate",dateWithoutTime);
		Criterion criterion2 = Restrictions.ge("jobEndDate",dateWithoutTime);
		Criterion criterion3 = Restrictions.ne("isPoolJob",1); 	
		Criterion criterion4 = Restrictions.eq("status","A");
		Criterion criterion5 = Restrictions.eq("approvalBeforeGoLive",1);	
		
		listjobOrder = findByCriteria(criterion1,criterion2,criterion3,criterion4,criterion5);
		
		System.out.println(" listjobOrder with isPoolJob Not Eq 1 :: "+listjobOrder.size());
	} 
	catch (Exception e) 
	{
		e.printStackTrace();
	}
	
	return listjobOrder;
}

@Transactional(readOnly=false)
public List<JobOrder> findActiveJobOrderbyDistrict(DistrictMaster districtMaster)
{
	
	List<JobOrder> jobOrders = new ArrayList<JobOrder>();
	try 
	{
	    
		Date dateWithoutTime = Utility.getDateWithoutTime();

		Criterion criterion1 = Restrictions.eq("status","a");
		Criterion criterion2 = Restrictions.le("jobStartDate",dateWithoutTime);
		Criterion criterion3 = Restrictions.ge("jobEndDate",dateWithoutTime);
		Criterion criterion4 = Restrictions.eq("districtMaster",districtMaster);
		Criterion criterion5 = Restrictions.ne("isInviteOnly",true);
		jobOrders = findByCriteria(criterion1,criterion2,criterion3,criterion4,criterion5);

	} 
	catch (Exception e) {
		e.printStackTrace();
	}		
	return jobOrders;
}
@Transactional(readOnly=false)
@SuppressWarnings("unchecked")
public Map<Integer,JobOrder> findByAllJobOrderByDistrict(DistrictMaster districtMaster) 
{
	Session session = getSession();
	Criterion criterion1 = Restrictions.eq("status","a");
	Criterion criterion2 = Restrictions.eq("districtMaster",districtMaster);
	List result = session.createCriteria(getPersistentClass())
	.add(criterion1)
	.add(criterion2)
	.list();
	Map<Integer,JobOrder> jobOrderMap = new HashMap<Integer, JobOrder>();
	JobOrder jobOrder = null;
	int i=0;
	for (Object object : result) {
		jobOrder=((JobOrder)object);
		jobOrderMap.put(new Integer(""+i),jobOrder);
		i++;
	}
	return jobOrderMap;
}

@Transactional(readOnly=false)
@SuppressWarnings("unchecked")
public Map<Integer,JobOrder> findByAllActiveAndInactiveJobs(List jobs) 
{
	Session session = getSession();
	Criterion criterion2 = Restrictions.in("apiJobId",jobs);	
	List result = session.createCriteria(getPersistentClass())
	.add(criterion2)
	.list();
	Map<Integer,JobOrder> jobOrderMap = new HashMap<Integer, JobOrder>();
	JobOrder jobOrder = null;
	int i=0;
	for (Object object : result) {
		jobOrder=((JobOrder)object);
		jobOrderMap.put(new Integer(""+i),jobOrder);
		i++;
	}
	return jobOrderMap;
}


//Search By Job category
@Transactional(readOnly=false)
public List<JobOrder> findJobOrderByJobCategoryMasterAll(JobCategoryMaster jobCategoryMaster)
{
	List<JobOrder> jobOrders = null;
	try 
	{
		Criterion criterion5 = Restrictions.eq("jobCategoryMaster",jobCategoryMaster); 	
		//Criterion criterion6 = Restrictions.eq("status","A");

		Session session = getSession();
		Criteria criteria = session.createCriteria(getPersistentClass());

		criteria.add(criterion5);
		//criteria.add(criterion6);
		jobOrders = criteria.list();
	} 
	catch (Exception e) {
		e.printStackTrace();
	}		
	return jobOrders;
}

@Transactional(readOnly=false)
public List<JobOrder> findAllActivJobOrder(){

	List<JobOrder> lstJobOrder = null;
	try{
		
		Date dateWithoutTime = Utility.getDateWithoutTime();
        
		Criterion criterion1 = Restrictions.eq("hiddenJob",false);
		Criterion criterion11 = Restrictions.isNull("hiddenJob");
		Criterion criterion12 = Restrictions.or(criterion1, criterion11);
		
		Criterion criterion3 = Restrictions.eq("isInviteOnly",false);
		Criterion criterion13 = Restrictions.isNull("isInviteOnly");
		Criterion criterion14 = Restrictions.or(criterion3, criterion13);
		
		
		Criterion criterion2 = Restrictions.eq("status","A");
		Criterion criterion4 = Restrictions.le("jobStartDate",dateWithoutTime);
		Criterion criterion5 = Restrictions.ge("jobEndDate",dateWithoutTime);
		Criterion criterion6 = Restrictions.eq("approvalBeforeGoLive",1);
		
		
		
		
		lstJobOrder = findByCriteria(criterion12,criterion14,criterion2,criterion3,criterion4,criterion5,criterion6);
		
	}catch (Exception e) {
		// TODO: handle exception
	   e.printStackTrace();
	}
	
	
	return lstJobOrder;
}
@Transactional(readOnly=false)
public List<JobCategoryMaster> findUniqueCateogryByJobOrder(DistrictMaster districtMaster,boolean flag)
{
	List<JobCategoryMaster> jobCategoryMasterList= new ArrayList<JobCategoryMaster>();
	try 
	{
		
		Date dateWithoutTime = Utility.getDateWithoutTime();
		
		if(districtMaster!=null){
			Session session = getSession();
			Criteria  criteria = session.createCriteria(getPersistentClass()) ;
			
			Criteria  criteria1 = criteria.createCriteria("jobCategoryMaster");
			
			
			
			criteria.setProjection(Projections.distinct(Projections.property("jobCategoryMaster")));
			criteria.add(Restrictions.eq( "status","A"));
			
			criteria.add(Restrictions.eq("districtMaster",districtMaster));
			criteria.add(Restrictions.le("jobStartDate",dateWithoutTime));
			criteria.add(Restrictions.ge("jobEndDate",dateWithoutTime));
		    criteria.add(Restrictions.ne("isInviteOnly",true));
			//criteria.add(Restrictions.ne("hiddenJob",true));
			if(flag)
				criteria1.add(Restrictions.isNull("parentJobCategoryId"));
			criteria1.add(Restrictions.eq( "status","A"));
			criteria1.addOrder(Order.asc("jobCategoryName"));
			criteria1.add(Restrictions.eq("districtMaster",districtMaster));
			
			jobCategoryMasterList = criteria.list();
		}
	} 
	catch (Exception e) 
	{
		e.printStackTrace();
	}
	return jobCategoryMasterList;
}


@Transactional(readOnly=false)
public List<JobCategoryMaster> findUniqueCateogryByHeadQuarter(HeadQuarterMaster headQuarterMaster,BranchMaster branchMaster )
{
	List<JobCategoryMaster> jobCategoryMasterList= new ArrayList<JobCategoryMaster>();
	try 
	{
		
		Date dateWithoutTime = Utility.getDateWithoutTime();
		
		if(headQuarterMaster!=null){
			Session session = getSession();
			Criteria  criteria = session.createCriteria(getPersistentClass()) ;
			
			Criteria  criteria1 = criteria.createCriteria("jobCategoryMaster");
			
			criteria.setProjection(Projections.distinct(Projections.property("jobCategoryMaster")));
			criteria.add(Restrictions.eq( "status","A"));
			criteria.add(Restrictions.eq("headQuarterMaster",headQuarterMaster));
			criteria.add(Restrictions.le("jobStartDate",dateWithoutTime));
			criteria.add(Restrictions.ge("jobEndDate",dateWithoutTime));
		    criteria.add(Restrictions.or(Restrictions.ne("isInviteOnly",true), Restrictions.isNull("isInviteOnly")));
			criteria.add(Restrictions.or(Restrictions.ne("hiddenJob",true),Restrictions.isNull("hiddenJob")));
			criteria.add(Restrictions.ne("isPoolJob",1)); 	
			criteria.add(Restrictions.eq("approvalBeforeGoLive",1));
			if(branchMaster!=null)
				criteria.add(Restrictions.eq("branchMaster",branchMaster));
			criteria1.add(Restrictions.eq( "status","A"));
			criteria1.addOrder(Order.asc("jobCategoryName"));
			criteria1.add(Restrictions.eq("headQuarterMaster",headQuarterMaster))
			.createCriteria("parentJobCategoryId")
			.add(Restrictions.isNull("parentJobCategoryId"));
			
			jobCategoryMasterList = criteria1.list();
		}
	} 
	catch (Exception e) 
	{
		e.printStackTrace();
	}
	return jobCategoryMasterList;
}



@Transactional(readOnly=false)
public List<JobCategoryMaster> findJobCategoryByHQAandBranch(HeadQuarterMaster headQuarterMaster,BranchMaster branchMaster )
{
	List<JobCategoryMaster> jobCategoryMasterList= new ArrayList<JobCategoryMaster>();
	try 
	{
		
		Date dateWithoutTime = Utility.getDateWithoutTime();
		
		if(headQuarterMaster!=null){
			Session session = getSession();
			Criteria  criteria = session.createCriteria(getPersistentClass()) ;
			
			Criteria  criteria1 = criteria.createCriteria("jobCategoryMaster");
			
			criteria.setProjection(Projections.distinct(Projections.property("jobCategoryMaster")));
			criteria.add(Restrictions.eq( "status","A"));
			
			criteria.add(Restrictions.eq("headQuarterMaster",headQuarterMaster));
			criteria.add(Restrictions.eq("branchMaster",branchMaster));
			criteria.add(Restrictions.le("jobStartDate",dateWithoutTime));
			criteria.add(Restrictions.ge("jobEndDate",dateWithoutTime));
		    criteria.add(Restrictions.ne("isInviteOnly",true));
			//criteria.add(Restrictions.ne("hiddenJob",true));
			
			criteria1.add(Restrictions.isNull("parentJobCategoryId"));
			criteria1.add(Restrictions.eq( "status","A"));
			criteria1.addOrder(Order.asc("jobCategoryName"));
			criteria1.add(Restrictions.eq("headQuarterMaster",headQuarterMaster));
			criteria1.add(Restrictions.eq("branchMaster",branchMaster));
			
			jobCategoryMasterList = criteria.list();
		}
	} 
	catch (Exception e) 
	{
		e.printStackTrace();
	}
	return jobCategoryMasterList;
}




@Transactional(readOnly=false)
@SuppressWarnings("unchecked")
public  List<String[]> noblestjobs(int sortingcheck, String sortOrderStrVal ,int start, int noOfRow,boolean report,String sortColomnName) 
{
	List<String[]> lst=new ArrayList<String[]>(); 
	Session session = getSession();
	String sql = "";
	 Connection connection =null;
	try {
	    SessionFactoryImplementor sessionFactoryImplementation = (SessionFactoryImplementor) session.getSessionFactory();
	    ConnectionProvider connectionProvider = sessionFactoryImplementation.getConnectionProvider();
	      connection = connectionProvider.getConnection();
		
		
        System.out.println(" sortOrderStrVal :: "+sortOrderStrVal+" sortColomnName "+sortColomnName);
      
		
       if(sortColomnName.equalsIgnoreCase("jobid"))
			sortColomnName = "Internal_Job_ID";
		else if(sortColomnName.equalsIgnoreCase("jobtitle"))
			sortColomnName = "Job_Title";
		else if(sortColomnName.equalsIgnoreCase("jobstartdate"))
			sortColomnName = "Job_Posted_Date";
		else if(sortColomnName.equalsIgnoreCase("jobenddate"))
			sortColomnName = "Job_Post_End_Date";
		else if(sortColomnName.equalsIgnoreCase("immediatehiring"))
			sortColomnName = "Immediate_Hiring";
		else if(sortColomnName.equalsIgnoreCase("hiringseason"))
			sortColomnName = "Hiring_Season";
		else if(sortColomnName.equalsIgnoreCase("jobCategoryName"))
			sortColomnName = "Job_Category";
		else if(sortColomnName.equalsIgnoreCase("subjectname"))
			sortColomnName = "Job_Subject";
		else if(sortColomnName.equalsIgnoreCase("jobstatus"))
			sortColomnName = "Job_Status";
		else if(sortColomnName.equalsIgnoreCase("jobtype"))
			sortColomnName = "Job_Type";
		else if(sortColomnName.equalsIgnoreCase("jobcreatedby"))
			sortColomnName = "Job_Created_By";
		else if(sortColomnName.equalsIgnoreCase("jobrecorddate"))
			sortColomnName = "Job_Record_Date";
		else if(sortColomnName.equalsIgnoreCase("noofpoition"))
			sortColomnName = "Number_of_Positions";
		else if(sortColomnName.equalsIgnoreCase("noofpositionfilled"))
			sortColomnName = "Number_of_Positions_Filled";
		else if(sortColomnName.equalsIgnoreCase("percentfilled"))
			sortColomnName = "Percent_Number_of_Positions_Filled";
		else if(sortColomnName.equalsIgnoreCase("noschooljob"))
			sortColomnName = "Number_Of_Schools_for_The_Job";
		
      
		String orderby = " order by "+sortColomnName+" "+sortOrderStrVal ;
		
		 sql=" select "+
		 "	jobs.Internal_Job_ID, Job_Title, Job_Posted_Date,  "+
		 "	Job_Post_End_Date, Immediate_Hiring, Hiring_Season, "+
		 "	Job_Category, Job_Subject,Job_Status,Job_Type, "+
		 "	Job_Created_By,Job_Record_Date, "+
		 "	case  "+
		 "		when countbyschool.numOfSchoolHires is null then ifnull(jobs.noOfExpHires,0)  "+
		 "		else countbyschool.numOfSchoolHires "+
		 "	end as `Number_of_Positions`, "+
		 "	Number_of_Positions_Filled, "+
		 "	round((Number_of_Positions_Filled/ "+
		 "			case  "+
		 "				when countbyschool.numOfSchoolHires is null then ifnull(jobs.noOfExpHires,0)  "+
		 "				else countbyschool.numOfSchoolHires end) *100,0) as `Percent_Number_of_Positions_Filled`, "+
		 "	Number_Of_Schools_for_The_Job "+
		 " from "+
		 "	( "+
		 "		select distinct jo.jobid as `Internal_Job_ID`,  "+
		 "			jo.jobtitle as `Job_Title`, jo.jobstartdate as `Job_Posted_Date`,  "+
		 "			case when jo.jobtitle like '%Immediate%' then 'Yes' else 'No' end as `Immediate_Hiring`, "+
		 "			case when  "+
		 "				case when jo.jobtitle like '%Immediate%' then 'Yes' else 'No' end ='Yes'  "+
		 "				then "+
		 "					case  "+
		 "					when month(jo.jobendDate)<=11 and year(jo.jobendDate)<=year(curdate()) then concat(left(jo.jobendDate,4),' - Immediate')  "+
		 "					when month(jo.jobendDate)=12 and year(jo.jobendDate)<=year(curdate()) then concat(year(jo.jobendDate)+1,' - Immediate')  "+
		 "					else 'Immediate' end "+
		 "				else case when month(jo.jobendDate)<=11 then year(jo.jobendDate) else year(jo.jobendDate)+1 end "+
		 "			end as `Hiring_Season`, "+
		 "			jo.jobenddate as `Job_Post_End_Date`, jcm.jobCategoryName as `Job_Category`,  "+
		 "			sjm.subjectname as `Job_Subject`, "+
		 "			case when jo.status='I' then 'Inactive' else 'Active' end as `Job_Status`, "+
		 "			case when jo.jobType='F' then 'Full-Time' else 'Part-Time' end as `Job_Type`, "+
		 "			Concat(um.lastname, ', ', um.firstname, ' ',um.title) as `Job_Created_By`, "+
		 "			jo.createddatetime as `Job_Record_Date`, "+
		 "			jo.noOfExpHires, "+
		 "			jo.noSchoolAttach as `Number_of_Schools_Have_Position`, "+
		 "			jo.selectedSchoolsInDistrict "+
		 "		from joborder jo "+
		 "			left join jobcategorymaster jcm on jcm.jobCategoryId=jo.jobCategoryId "+
		 "			left join subjectmaster sjm on sjm.subjectid=jo.subjectId "+
		 "			left join usermaster um on um.userid=jo.createdBy "+
		 "		where jo.districtid='7800038' "+
		 "	) as jobs "+
		 " left join "+
		 "	( "+
		 "		select  "+
		 "			sij.jobid, sum(nullif(sij.noOfSchoolExpHires,0)) as `numOfSchoolHires` "+
		 "		from schoolinjoborder sij "+
		 "		group by sij.jobid  "+
		 "	) as countbyschool  "+
		 " on jobs.Internal_Job_ID=countbyschool.jobid "+

		 " left join "+
		 "	( "+
		 "		select  "+
		 "			sij.jobid, count(sij.schoolid) as `Number_Of_Schools_for_The_Job` "+
		 "		from schoolinjoborder sij "+
		 "		group by sij.jobid  "+
		 "	) as countschoolbyjob  "+
		 " on jobs.Internal_Job_ID=countschoolbyjob.jobid "+

		 " left join "+
		 "	( "+
		 "		select  "+
		 "			jobid, nullif(count(*),0) as `Number_of_Positions_Filled` "+
		 "		from jobforteacher  "+
		 "		where status=6  "+
		 "		group by jobid "+
		 "	) as hired  "+
		 " on hired.jobid=jobs.Internal_Job_ID "+
	""+orderby;
		 
	 
	 
	PreparedStatement ps=connection.prepareStatement(sql,ResultSet.CONCUR_READ_ONLY,ResultSet.CONCUR_UPDATABLE);		

	ResultSet rs=ps.executeQuery();
		
		if(rs.next()){				
			do{
				final String[] allInfo=new String[rs.getMetaData().getColumnCount()];
				for(Integer i=1;i<=rs.getMetaData().getColumnCount();i++)
				   {
					allInfo[i-1]=rs.getString(i);
				   }
				lst.add(allInfo);
				
			}while(rs.next());
		}
		else{
			System.out.println("Record not found.");
		}
		
		return lst;
	} catch (Exception e) {
		e.printStackTrace();
	}finally{
		if(connection!=null)
			try {
				connection.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	return null;
}


@Transactional(readOnly=false)
@SuppressWarnings("unchecked")
public  List<String[]> noblestjobsRun() 
{
	List<String[]> lst=new ArrayList<String[]>(); 
	Session session = getSession();
	String sql = "";
	 Connection connection =null;
	try {
	    SessionFactoryImplementor sessionFactoryImplementation = (SessionFactoryImplementor) session.getSessionFactory();
	    ConnectionProvider connectionProvider = sessionFactoryImplementation.getConnectionProvider();
	      connection = connectionProvider.getConnection();
		      
		String orderby = " order by Internal_Job_ID" ;
		
		 sql=" select "+
		 "	jobs.Internal_Job_ID, Job_Title, Job_Posted_Date,  "+
		 "	Job_Post_End_Date, Immediate_Hiring, Hiring_Season, "+
		 "	Job_Category, Job_Subject,Job_Status,Job_Type, "+
		 "	Job_Created_By,Job_Record_Date, "+
		 "	case  "+
		 "		when countbyschool.numOfSchoolHires is null then ifnull(jobs.noOfExpHires,0)  "+
		 "		else countbyschool.numOfSchoolHires "+
		 "	end as `Number_of_Positions`, "+
		 "	Number_of_Positions_Filled, "+
		 "	round((Number_of_Positions_Filled/ "+
		 "			case  "+
		 "				when countbyschool.numOfSchoolHires is null then ifnull(jobs.noOfExpHires,0)  "+
		 "				else countbyschool.numOfSchoolHires end) *100,0) as `Percent_Number_of_Positions_Filled`, "+
		 "	Number_Of_Schools_for_The_Job "+
		 " from "+
		 "	( "+
		 "		select distinct jo.jobid as `Internal_Job_ID`,  "+
		 "			jo.jobtitle as `Job_Title`, jo.jobstartdate as `Job_Posted_Date`,  "+
		 "			case when jo.jobtitle like '%Immediate%' then 'Yes' else 'No' end as `Immediate_Hiring`, "+
		 "			case when  "+
		 "				case when jo.jobtitle like '%Immediate%' then 'Yes' else 'No' end ='Yes'  "+
		 "				then "+
		 "					case  "+
		 "					when month(jo.jobendDate)<=11 and year(jo.jobendDate)<=year(curdate()) then concat(left(jo.jobendDate,4),' - Immediate')  "+
		 "					when month(jo.jobendDate)=12 and year(jo.jobendDate)<=year(curdate()) then concat(year(jo.jobendDate)+1,' - Immediate')  "+
		 "					else 'Immediate' end "+
		 "				else case when month(jo.jobendDate)<=11 then year(jo.jobendDate) else year(jo.jobendDate)+1 end "+
		 "			end as `Hiring_Season`, "+
		 "			jo.jobenddate as `Job_Post_End_Date`, jcm.jobCategoryName as `Job_Category`,  "+
		 "			sjm.subjectname as `Job_Subject`, "+
		 "			case when jo.status='I' then 'Inactive' else 'Active' end as `Job_Status`, "+
		 "			case when jo.jobType='F' then 'Full-Time' else 'Part-Time' end as `Job_Type`, "+
		 "			Concat(um.lastname, ', ', um.firstname, ' ',um.title) as `Job_Created_By`, "+
		 "			jo.createddatetime as `Job_Record_Date`, "+
		 "			jo.noOfExpHires, "+
		 "			jo.noSchoolAttach as `Number_of_Schools_Have_Position`, "+
		 "			jo.selectedSchoolsInDistrict "+
		 "		from joborder jo "+
		 "			left join jobcategorymaster jcm on jcm.jobCategoryId=jo.jobCategoryId "+
		 "			left join subjectmaster sjm on sjm.subjectid=jo.subjectId "+
		 "			left join usermaster um on um.userid=jo.createdBy "+
		 "		where jo.districtid='7800038' "+
		 "	) as jobs "+
		 " left join "+
		 "	( "+
		 "		select  "+
		 "			sij.jobid, sum(nullif(sij.noOfSchoolExpHires,0)) as `numOfSchoolHires` "+
		 "		from schoolinjoborder sij "+
		 "		group by sij.jobid  "+
		 "	) as countbyschool  "+
		 " on jobs.Internal_Job_ID=countbyschool.jobid "+

		 " left join "+
		 "	( "+
		 "		select  "+
		 "			sij.jobid, count(sij.schoolid) as `Number_Of_Schools_for_The_Job` "+
		 "		from schoolinjoborder sij "+
		 "		group by sij.jobid  "+
		 "	) as countschoolbyjob  "+
		 " on jobs.Internal_Job_ID=countschoolbyjob.jobid "+

		 " left join "+
		 "	( "+
		 "		select  "+
		 "			jobid, nullif(count(*),0) as `Number_of_Positions_Filled` "+
		 "		from jobforteacher  "+
		 "		where status=6  "+
		 "		group by jobid "+
		 "	) as hired  "+
		 " on hired.jobid=jobs.Internal_Job_ID "+
	""+orderby;
		 
	 
	 
	PreparedStatement ps=connection.prepareStatement(sql,ResultSet.CONCUR_READ_ONLY,ResultSet.CONCUR_UPDATABLE);		

	ResultSet rs=ps.executeQuery();
		
		if(rs.next()){				
			do{
				final String[] allInfo=new String[rs.getMetaData().getColumnCount()];
				for(Integer i=1;i<=rs.getMetaData().getColumnCount();i++)
				   {
					allInfo[i-1]=rs.getString(i);
				   }
				lst.add(allInfo);
				
			}while(rs.next());
		}
		else{
			System.out.println("Record not found.");
		}
		
		return lst;
	} catch (Exception e) {
		e.printStackTrace();
	}finally{
		if(connection!=null)
			try {
				connection.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	return null;
}



/*****************************sandeep**********************************************/
@Transactional(readOnly=false)
public void totalNoOfHires(JobOrder jobOrder)
{
	List<SchoolInJobOrder> lstSchoolInJoborders =new ArrayList<SchoolInJobOrder>();
	Integer noofHiresinJobOrder=0;  		
	try{
	if(jobOrder!=null){

		Criterion criterion1 = Restrictions.eq("jobId", jobOrder);
		lstSchoolInJoborders = schoolInJobOrderDAO.findByCriteria(criterion1);
		System.out.println("lstSchoolInJoborders:   : "+lstSchoolInJoborders.size()+" ::::jobOrder:::"+jobOrder.getJobId());
		if(lstSchoolInJoborders!=null && lstSchoolInJoborders.size()>0){
				try{
					Integer sumHired =0;
					for(SchoolInJobOrder sch : lstSchoolInJoborders){
						sumHired =sumHired+sch.getNoOfSchoolExpHires();
					}
					noofHiresinJobOrder = sumHired;
				}catch(Exception e){
					e.printStackTrace();
				}
				System.out.println(":: noofHiresinJobOrder ="+noofHiresinJobOrder);
				if(noofHiresinJobOrder!=null && noofHiresinJobOrder >0){
					try{
						jobOrder.setNoOfHires(noofHiresinJobOrder);
						updatePersistent(jobOrder);
					}catch(Exception e){
						e.printStackTrace();
					}   
				}

		}else{
			noofHiresinJobOrder=jobOrder.getNoOfExpHires();
			System.out.println("Else noofHiresinJobOrder ="+noofHiresinJobOrder);
			try{
				jobOrder.setNoOfHires(noofHiresinJobOrder);
				updatePersistent(jobOrder);
			}catch(Exception e){
				e.printStackTrace();
			}   
		}
	}	
	}catch(Throwable e){
		e.printStackTrace();
	}
	//return;
}

//shadab start
@Transactional(readOnly=false)
public List<JobOrder> findAllActiveJobForLocationsES(Criterion criterion1,Criterion criterion2,Criterion criterion3,Criterion criterion4,Criterion criterion6)
{
	List<JobOrder> jobOrders = null;
	try 
	{				
		Session session = getSession();
		Criteria criteria = session.createCriteria(getPersistentClass());
		Criterion criterion5 = Restrictions.ne("isInviteOnly",true);
		
		criteria.add(criterion1);
		criteria.add(criterion2);
		criteria.add(criterion3);
		criteria.add(criterion4);
		criteria.add(criterion5);
		criteria.add(criterion6);
		
		jobOrders = criteria.list();
	} 
	catch (Exception e) {
		e.printStackTrace();
	}		
	return jobOrders;
}

@Transactional(readOnly=false)
public List<JobOrder> findActiveJobOrderbyDistrictES()
{
	
	List<JobOrder> jobOrders = new ArrayList<JobOrder>();
	Map<Integer,String> map=new HashMap<Integer, String>();
	try 
	{
	    
		Date dateWithoutTime = Utility.getDateWithoutTime();

		Criterion criterion1 = Restrictions.eq("status","a");
		Criterion criterion2 = Restrictions.le("jobStartDate",dateWithoutTime);
		Criterion criterion3 = Restrictions.ge("jobEndDate",dateWithoutTime);
		//Criterion criterion5 = Restrictions.ne("isInviteOnly",true);
		//jobOrders = findByCriteria(criterion1,criterion2,criterion3,criterion5);
		jobOrders = findByCriteria(criterion1,criterion2,criterion3);
		
		
		System.out.println("getDBDataForJobsOfInterestForCandidate size==============="+jobOrders.size());
		
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		
		for(JobOrder jobOrderDetails:jobOrders)
		{
			JSONObject obj=new JSONObject();
			obj.put("jobId", jobOrderDetails.getJobId());
			if(jobOrderDetails.getIsPoolJob()!=null)
				obj.put("isPoolJob", jobOrderDetails.getIsPoolJob());
			else
				obj.put("isPoolJob", 0);
			if(jobOrderDetails.getJobTitle()!=null)
				obj.put("jobTitle", jobOrderDetails.getJobTitle());
			else
				obj.put("jobTitle", "");
			obj.put("createdForEntity", jobOrderDetails.getCreatedForEntity());
			if(jobOrderDetails.getDistrictMaster()!=null)
			{
				obj.put("districtId", jobOrderDetails.getDistrictMaster().getDistrictId());
				obj.put("districtName", jobOrderDetails.getDistrictMaster().getDistrictName());
				obj.put("zipCode", jobOrderDetails.getDistrictMaster().getZipCode());
			}
				
			else
			{
				obj.put("districtId", "");
				obj.put("districtName", "");
				obj.put("zipCode", "");
			}
			
			//tommorow if null check
			if(jobOrderDetails.getDistrictMaster()!=null && jobOrderDetails.getDistrictMaster().getCityName()!=null)
				obj.put("disCityName", jobOrderDetails.getDistrictMaster().getCityName());
			else
				obj.put("disCityName", "");
			if(jobOrderDetails.getDistrictMaster()!=null && jobOrderDetails.getDistrictMaster().getAddress()!=null)
				obj.put("distaddress", jobOrderDetails.getDistrictMaster().getAddress());
			else
				obj.put("distaddress", "");
			obj.put("status", jobOrderDetails.getStatus());
			if(jobOrderDetails.getIpAddress()!=null)
				obj.put("ipAddress", jobOrderDetails.getIpAddress());
			else
				obj.put("ipAddress", "");
			obj.put("jobEndDate", sdf.format(jobOrderDetails.getJobEndDate()));
			if(jobOrderDetails.getDistrictMaster()!=null && jobOrderDetails.getDistrictMaster().getDisplayName()!=null)
				obj.put("displayName", jobOrderDetails.getDistrictMaster().getDisplayName());
			else
				obj.put("displayName", "");
			if(jobOrderDetails.getDistrictMaster()!=null && jobOrderDetails.getDistrictMaster().getStateId()!=null )
				obj.put("disStateName", jobOrderDetails.getDistrictMaster().getStateId().getStateName());
			else
				obj.put("disStateName", "");
			
			
			if(jobOrderDetails.getSubjectMaster()!=null)
				obj.put("subjectName", jobOrderDetails.getSubjectMaster().getSubjectName());
			else
				obj.put("subjectName", "");
			if(jobOrderDetails.getGeoZoneMaster()!=null)
			{
				obj.put("geoZoneDistrictId", jobOrderDetails.getGeoZoneMaster().getDistrictMaster().getDistrictId());
				obj.put("geoZoneName", jobOrderDetails.getGeoZoneMaster().getGeoZoneName());
				obj.put("geoZoneId", jobOrderDetails.getGeoZoneMaster().getGeoZoneId());
			}
				
			else
			{
				obj.put("geoZoneDistrictId", 0);
				obj.put("geoZoneName", "");
				obj.put("geoZoneId", 0);
			}
			
			JSONArray schoolName = new JSONArray();
			JSONArray schoolId = new JSONArray();
			JSONArray schoolAddress = new JSONArray();
			JSONArray schoolZip = new JSONArray();
			JSONArray schoolCityName = new JSONArray();
			JSONArray schoolStateName = new JSONArray();
			JSONArray schoolGeoMapId = new JSONArray();
			JSONArray schoolSchoolTypeId = new JSONArray();
			JSONArray schoolRegionId = new JSONArray();
			if(jobOrderDetails.getSchool()!=null)
			{
				for(SchoolMaster school:jobOrderDetails.getSchool())
				{
					schoolGeoMapId.add(school.getGeoMapping().getGeoId().getGeoId()+"");
					schoolSchoolTypeId.add(school.getSchoolTypeId().getSchoolTypeId()+"");
					schoolRegionId.add(school.getRegionId().getRegionId()+"");
					
					schoolName.add(school.getSchoolName());
					schoolId.add(school.getSchoolId());
					if(school.getAddress()!=null)
						schoolAddress.add(school.getAddress());
					else
						schoolAddress.add("");
					if(school.getZip()!=null)
						schoolZip.add(school.getZip());
					else
						schoolZip.add(0);
					if(schoolCityName!=null)
						schoolCityName.add(school.getCityName());
					else
						schoolCityName.add("");
					if(school.getStateMaster()!=null)
						schoolStateName.add(school.getStateMaster().getStateName());
					else
						schoolStateName.add("");
				}
				
			}
			obj.put("schoolName", schoolName);
			obj.put("schoolId", schoolId);
			obj.put("schoolAddress", schoolAddress);
			obj.put("schoolZip", schoolZip);
			obj.put("schoolCityName", schoolCityName);
			obj.put("schoolStateName", schoolStateName);
			obj.put("schoolGeoMapId", schoolGeoMapId);
			obj.put("schoolSchoolTypeId", schoolSchoolTypeId);
			obj.put("schoolRegionId", schoolRegionId);
			
				
			
			//System.out.println(obj.toString());
			map.put(jobOrderDetails.getJobId(), obj.toString());
		}
		
		//return map;
		

	} 
	catch (Exception e) {
		e.printStackTrace();
	}		
	//return map;
	return jobOrders;
}

@Transactional(readOnly=false)
public Map<Integer,String> findAllJobWithPoolConditionDup()
{
	List<JobOrder> listjobOrder=new ArrayList<JobOrder>();
	try 
	{
		Date dateWithoutTime = Utility.getDateWithoutTime();
		
		Criterion criterion1 = Restrictions.le("jobStartDate",dateWithoutTime);
		Criterion criterion2 = Restrictions.ge("jobEndDate",dateWithoutTime);
		//Criterion criterion3 = Restrictions.ne("isPoolJob",1); 	
		Criterion criterion4 = Restrictions.eq("status","A");
		//Criterion criterion5 = Restrictions.eq("approvalBeforeGoLive",1);
		//Criterion criterion6 = Restrictions.ne("isInviteOnly",true);
		//Criterion criterion7 = Restrictions.ne("isVacancyJob",true);
		
		//listjobOrder = findByCriteria(criterion1,criterion2,criterion3,criterion4,criterion5,criterion6,criterion7);
		listjobOrder = findByCriteria(criterion1,criterion2,criterion4);
		
		System.out.println(" listjobOrder with isPoolJob Not Eq 1 :: "+listjobOrder.size());
		
		
		System.out.println("jobwithpoolcondition size==============="+listjobOrder.size());
		Map<Integer,String> map=new HashMap<Integer, String>();
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		try
		{
		
		for(JobOrder jobOrderDetails:listjobOrder)
		{
			JSONObject obj=new JSONObject();
			obj.put("jobId", jobOrderDetails.getJobId());
			if(jobOrderDetails.getIsPoolJob()!=null)
				obj.put("isPoolJob", jobOrderDetails.getIsPoolJob());
			else
				obj.put("isPoolJob", 0);
			if(jobOrderDetails.getJobTitle()!=null)
				obj.put("jobTitle", jobOrderDetails.getJobTitle());
			else
				obj.put("jobTitle", "");
			obj.put("createdForEntity", jobOrderDetails.getCreatedForEntity());
			if(jobOrderDetails.getDistrictMaster()!=null)
			{
				obj.put("districtId", jobOrderDetails.getDistrictMaster().getDistrictId());
				obj.put("districtName", jobOrderDetails.getDistrictMaster().getDistrictName());
				obj.put("zipCode", jobOrderDetails.getDistrictMaster().getZipCode());
			}
				
			else
			{
				obj.put("districtId", 0);
				obj.put("districtName", "");
				obj.put("zipCode", "");
			}
				
			
			
			
			if(jobOrderDetails.getIsInviteOnly()!=null)
				obj.put("isInviteOnly", jobOrderDetails.getIsInviteOnly());
			else
				obj.put("isInviteOnly", false);
			
			if(jobOrderDetails.getIsVacancyJob()!=null)
				obj.put("isVacancyJob", jobOrderDetails.getIsVacancyJob());
			else
				obj.put("isVacancyJob", false);
			
			if(jobOrderDetails.getApprovalBeforeGoLive()!=null)
				obj.put("approvalBeforeGoLive", jobOrderDetails.getApprovalBeforeGoLive());
			else
				obj.put("approvalBeforeGoLive", 0);
			
			
			if(jobOrderDetails.getDistrictMaster()!= null && jobOrderDetails.getDistrictMaster().getCityName()!=null)
				obj.put("disCityName", jobOrderDetails.getDistrictMaster().getCityName());
			else
				obj.put("disCityName", "");
			if(jobOrderDetails.getDistrictMaster()!= null && jobOrderDetails.getDistrictMaster().getAddress()!=null)
				obj.put("distaddress", jobOrderDetails.getDistrictMaster().getAddress());
			else
				obj.put("distaddress", "");
			obj.put("status", jobOrderDetails.getStatus());
			if(jobOrderDetails.getIpAddress()!=null)
				obj.put("ipAddress", jobOrderDetails.getIpAddress());
			else
				obj.put("ipAddress", "");
			obj.put("jobEndDate", sdf.format(jobOrderDetails.getJobEndDate()));
			if(jobOrderDetails.getDistrictMaster()!= null && jobOrderDetails.getDistrictMaster().getDisplayName()!=null)
				obj.put("displayName", jobOrderDetails.getDistrictMaster().getDisplayName());
			else
				obj.put("displayName", "");
			if(jobOrderDetails.getDistrictMaster()!= null && jobOrderDetails.getDistrictMaster().getStateId()!=null )
				obj.put("disStateName", jobOrderDetails.getDistrictMaster().getStateId().getStateName());
			else
				obj.put("disStateName", "");
			
			
			if(jobOrderDetails.getSubjectMaster()!=null)
				obj.put("subjectName", jobOrderDetails.getSubjectMaster().getSubjectName());
			else
				obj.put("subjectName", "");
			if(jobOrderDetails.getSubjectMaster()!=null)
				obj.put("subjectId", jobOrderDetails.getSubjectMaster().getSubjectId().toString());
			else
				obj.put("subjectId", "");
			
			
			if(jobOrderDetails.getGeoZoneMaster()!=null)
			{
				obj.put("geoZoneDistrictId", jobOrderDetails.getGeoZoneMaster().getDistrictMaster().getDistrictId());
				obj.put("geoZoneName", jobOrderDetails.getGeoZoneMaster().getGeoZoneName());
				obj.put("geoZoneId", jobOrderDetails.getGeoZoneMaster().getGeoZoneId());
			}
				
			else
			{
				obj.put("geoZoneDistrictId", 0);
				obj.put("geoZoneName", "");
				obj.put("geoZoneId", 0);
			}
		
			JSONArray schoolName = new JSONArray();
			JSONArray schoolId = new JSONArray();
			JSONArray schoolAddress = new JSONArray();
			JSONArray schoolZip = new JSONArray();
			JSONArray schoolCityName = new JSONArray();
			JSONArray schoolStateName = new JSONArray();
			JSONArray schoolGeoMapId = new JSONArray();
			JSONArray schoolSchoolTypeId = new JSONArray();
			JSONArray schoolRegionId = new JSONArray();
			if(jobOrderDetails.getSchool()!=null)
			{
				
				for(SchoolMaster school:jobOrderDetails.getSchool())
				{
					schoolGeoMapId.add(school.getGeoMapping().getGeoId().getGeoId()+"");
					schoolSchoolTypeId.add(school.getSchoolTypeId().getSchoolTypeId()+"");
					schoolRegionId.add(school.getRegionId().getRegionId()+"");
					
					schoolName.add(school.getSchoolName());
					schoolId.add(school.getSchoolId());
					if(school.getAddress()!=null)
						schoolAddress.add(school.getAddress());
					else
						schoolAddress.add("");
					if(school.getZip()!=null)
						schoolZip.add(school.getZip());
					else
						schoolZip.add(0);
					if(schoolCityName!=null)
						schoolCityName.add(school.getCityName());
					else
						schoolCityName.add("");
					if(school.getStateMaster()!=null)
						schoolStateName.add(school.getStateMaster().getStateName());
					else
						schoolStateName.add("");
				}
				
			}
			obj.put("schoolName", schoolName);
			obj.put("schoolId", schoolId);
			obj.put("schoolAddress", schoolAddress);
			obj.put("schoolZip", schoolZip);
			obj.put("schoolCityName", schoolCityName);
			obj.put("schoolStateName", schoolStateName);
			obj.put("schoolGeoMapId", schoolGeoMapId);
			obj.put("schoolSchoolTypeId", schoolSchoolTypeId);
			obj.put("schoolRegionId", schoolRegionId);
			
				
			
			//System.out.println(obj.toString());
			map.put(jobOrderDetails.getJobId(), obj.toString());
		}
		
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		System.out.println("jobsofinterest map size============================="+map.size());
		
		return map;
		
		
		
		
	} 
	catch (Exception e) 
	{
		e.printStackTrace();
	}
	
	//return listjobOrder;
	return null;
}

@Transactional(readOnly=false)
//public Map<Integer,String> findAllJob(Map<Integer, JobRequisitionNumbers> hrStatusMap)
public Map<Integer,String> findAllJob(Map<Integer, Object> hrStatusMap,int start,JobRequisitionNumbersDAO jobRequisitionNumbersDAO)
{
	List<JobOrder> listjobOrder=new ArrayList<JobOrder>();
	System.out.println("findAllJob method is called");
	long l=new Date().getTime();
	try 
	{
		//SessionFactory sessionFactory=schoolMasterDAO.getSessionFactory();
		//StatelessSession statelesSsession = sessionFactory.openStatelessSession();
		
		
		Date dateWithoutTime = Utility.getDateWithoutTime();
		//listjobOrder = findByCriteria(Order.asc("jobId"));
		listjobOrder = getSession().createCriteria(getPersistentClass())
		.setFetchMode("headQuarterMaster", FetchMode.SELECT)
		.setFetchMode("branchMaster", FetchMode.SELECT)
		.setFetchMode("i4QuestionSets", FetchMode.SELECT)
		.setFetchMode("statusMaster", FetchMode.SELECT)
		.setFetchMode("secondaryStatus", FetchMode.SELECT)
		.setFetchMode("employmentServicesTechnician", FetchMode.SELECT)
		.setFetchMode("teacherDetail", FetchMode.SELECT)
		.setFetchMode("districtMaster", FetchMode.SELECT)
		.setFetchMode("jobCategoryMaster", FetchMode.SELECT)
		.setFetchMode("subjectMaster", FetchMode.SELECT)
		.setFetchMode("geoZoneMaster", FetchMode.SELECT)
		.setFetchMode("jobCategoryMaster", FetchMode.SELECT)
		.setFirstResult(start)
		.setMaxResults(ElasticSearchConfig.rowSize)
		.addOrder(Order.asc("jobId"))
		.list();
		System.out.println("Fetch time For fetching job"+(new Date().getTime()-l)/1000);
		
		List<Object[]> jobRequisitionNumbers=new ArrayList<Object[]>();
		hrStatusMap = new HashMap<Integer, Object>();
		//if(Utility.isNC())
		{
			jobRequisitionNumbers=jobRequisitionNumbersDAO.getJobIdAndRequisitionNo(listjobOrder);
			if(jobRequisitionNumbers!=null && jobRequisitionNumbers.size()>0)
			{
				for(Object[] hrstatus: jobRequisitionNumbers)
				{
					hrStatusMap.put((Integer)hrstatus[0], hrstatus[1]);
				}
			}
		}	
		
		/*listjobOrder=statelesSsession.createCriteria(getPersistentClass(),"jo")
		.createCriteria("jo.districtMaster", "dm",Criteria.LEFT_JOIN)
		.createCriteria("jo.jobCategoryMaster", "jcm",Criteria.LEFT_JOIN)
		.createCriteria("jo.subjectMaster", "sm",Criteria.LEFT_JOIN)
		.createCriteria("jo.geoZoneMaster", "gzm",Criteria.LEFT_JOIN)
		.createCriteria("jo.school", "sch",Criteria.LEFT_JOIN)
		.setFetchMode("headQuarterMaster", FetchMode.SELECT)
		.setFetchMode("branchMaster", FetchMode.SELECT)
		.setFetchMode("i4QuestionSets", FetchMode.SELECT)
		.setFetchMode("statusMaster", FetchMode.SELECT)
		.setFetchMode("secondaryStatus", FetchMode.SELECT)
		.setFetchMode("employmentServicesTechnician", FetchMode.SELECT)
		.setFetchMode("teacherDetail", FetchMode.SELECT)
		.setFetchMode("districtMaster", FetchMode.SELECT)
		.setFetchMode("jobCategoryMaster", FetchMode.SELECT)
		.setFetchMode("subjectMaster", FetchMode.SELECT)
		.setFetchMode("geoZoneMaster", FetchMode.SELECT)
		.setFetchMode("jobCategoryMaster", FetchMode.SELECT)
		.setProjection(Projections.projectionList()
				.add(Projections.property("jo.jobId"))
				.add(Projections.property("jo.createdForEntity"))
				.add(Projections.property("jo.isPoolJob"))
				.add(Projections.property("jo.jobTitle"))
				.add(Projections.property("dm.districtName"))
				.add(Projections.property("dm.districtId"))
				.add(Projections.property("jo.status"))
				.add(Projections.property("jo.approvalBeforeGoLive"))
				.add(Projections.property("jo.jobEndDate"))
				.add(Projections.property("jo.districtAttachment"))
				.add(Projections.property("jo.isInviteOnly"))
				.add(Projections.property("jo.requisitionNumber"))
				.add(Projections.property("sm.subjectName"))
				.add(Projections.property("gzm.districtMaster.districtId"))
				.add(Projections.property("gzm.geoZoneName"))
				.add(Projections.property("gzm.geoZoneId"))
				.add(Projections.property("jcm.jobCategoryName"))
				.add(Projections.property("sch.schoolId"))
				//.add(Projections.property("sch.schoolId.schoolName"))
				//obj.put("schoolName", schoolName);
				//obj.put("schoolId", schoolId);
				//obj.put("districtRequisitionNo", hrStatusMap.get(jobOrderDetails.getJobId()));
				
		)
		
		
		.setFirstResult(start)
		.setMaxResults(5000)
		.list();
		statelesSsession.close();*/
		//System.out.println("All job size==============="+listjobOrder.size());
		//System.out.println("Fetch time findAllJob"+(new Date().getTime()-l)/1000);
		/*if(true)
			return null;*/
		
		Map<Integer,String> map=new HashMap<Integer, String>();
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		try
		{
		int x=0;
		//System.out.println("**********************loop start**************************");
		long l1=new Date().getTime();
		for(JobOrder jobOrderDetails:listjobOrder)
		{
			/*x++;
			if(x>=1000)
			{
				x=0;
				System.out.println("Time after 1000 record="+(new Date().getTime()-l)/1000);
			}*/
			JSONObject obj=new JSONObject();
			obj.put("jobIdOrder", jobOrderDetails.getJobId());
			obj.put("jobId", jobOrderDetails.getJobId().toString());
			obj.put("createdForEntity", jobOrderDetails.getCreatedForEntity());
			if(jobOrderDetails.getIsPoolJob()!=null)
				obj.put("isPoolJob", jobOrderDetails.getIsPoolJob());
			else
				obj.put("isPoolJob", 0);
			if(jobOrderDetails.getJobTitle()!=null)
				obj.put("jobTitle", jobOrderDetails.getJobTitle());
			else
				obj.put("jobTitle", "");
			if(jobOrderDetails.getDistrictMaster()!=null)
			{
				obj.put("districtName", jobOrderDetails.getDistrictMaster().getDistrictName());
				obj.put("districtId", jobOrderDetails.getDistrictMaster().getDistrictId());
			}
			else
			{
				obj.put("districtId", 0);
				obj.put("districtName", "");
			}
			
			obj.put("status", jobOrderDetails.getStatus());
			if(jobOrderDetails.getApprovalBeforeGoLive()!=null)
			{
				obj.put("approvalBeforeGoLive", jobOrderDetails.getApprovalBeforeGoLive());
			}
			else
				obj.put("approvalBeforeGoLive", 0);
			obj.put("jobEndDate", sdf.format(jobOrderDetails.getJobEndDate()));
			if(jobOrderDetails.getDistrictAttachment()!=null)
				obj.put("districtAttachment", jobOrderDetails.getDistrictAttachment());
			else
				obj.put("districtAttachment", "");
			if(jobOrderDetails.getIsInviteOnly()!=null)
				obj.put("IsInviteOnly", jobOrderDetails.getIsInviteOnly());
			else
				obj.put("IsInviteOnly", false);
			if(jobOrderDetails.getRequisitionNumber()!=null)
			{
				obj.put("requisitionNumber", jobOrderDetails.getRequisitionNumber());
			}
			else
			{
				obj.put("requisitionNumber", "");
			}
			
			if(jobOrderDetails.getSubjectMaster()!=null)
				obj.put("subjectName", jobOrderDetails.getSubjectMaster().getSubjectName());
			else
				obj.put("subjectName", "");
			if(jobOrderDetails.getGeoZoneMaster()!=null)
			{
				obj.put("geoZoneDistrictId", jobOrderDetails.getGeoZoneMaster().getDistrictMaster().getDistrictId());
				obj.put("geoZoneName", jobOrderDetails.getGeoZoneMaster().getGeoZoneName());
				obj.put("geoZoneId", jobOrderDetails.getGeoZoneMaster().getGeoZoneId());
			}
				
			else
			{
				obj.put("geoZoneDistrictId", 0);
				obj.put("geoZoneName", "");
				obj.put("geoZoneId", 0);
			}
			if(jobOrderDetails.getJobCategoryMaster()!=null)
			{
				obj.put("jobCategory", jobOrderDetails.getJobCategoryMaster().getJobCategoryName());
			}
			else
			{
				obj.put("jobCategory", "");
			}
			//System.out.println(jobOrderDetails.getJobId()+"\t"+hrStatusMap.containsKey(jobOrderDetails.getJobId()));
			
			if(hrStatusMap.containsKey(jobOrderDetails.getJobId()))
			{
				//obj.put("districtRequisitionNo", hrStatusMap.get(jobOrderDetails.getJobId()).getDistrictRequisitionNumbers().getRequisitionNumber());
				obj.put("districtRequisitionNo", hrStatusMap.get(jobOrderDetails.getJobId()));
			}
			else
			{
				obj.put("districtRequisitionNo", "");
			}
				
			JSONArray schoolName = new JSONArray();
			JSONArray schoolId = new JSONArray();
			if(jobOrderDetails.getSchool()!=null)
			{
				for(SchoolMaster school:jobOrderDetails.getSchool())
				{
					schoolName.add(school.getSchoolName());
					schoolId.add(school.getSchoolId());
				}
				
			}
			obj.put("schoolName", schoolName);
			obj.put("schoolId", schoolId);
				
			
			//System.out.println(obj.toString());
			map.put(jobOrderDetails.getJobId(), obj.toString());
		}
		System.out.println("Loop timing "+(new Date().getTime()-l1)/1000);
		//System.out.println("******************loop end***************************");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		System.out.println("Time after 1000 record="+(new Date().getTime()-l)/1000);
		System.out.println("schooljoborder map size============================="+map.size());
		
		return map;
		
		
		
		
	} 
	catch (Exception e) 
	{
		e.printStackTrace();
	}
	
	//return listjobOrder;
	return null;
}
@Transactional(readOnly=true)
public List<JobOrder> findAllJob(HeadQuarterMaster headQuarterMaster)
{
	List<JobOrder> listjobOrder=new ArrayList<JobOrder>();
	try 
	{
		System.out.println(getPersistentClass());
		DetachedCriteria query = DetachedCriteria.forClass(DistrictMaster.class)
		.add(Restrictions.eq("headQuarterMaster", headQuarterMaster)
		).setProjection(Projections.projectionList().add(Projections.property("districtId")));
		
		//listjobOrder = findByCriteria(Restrictions.eq("headQuarterMaster", headQuarterMaster));
		//criteria.add(Property.forName("tad.teacherAssessmentId").eq(subQuery));
		/*Session session=getSession();
		System.out.println("session==="+session);
		System.out.println("persistance==="+getPersistentClass());
		
		Criteria criteria=session.createCriteria(getPersistentClass());
		System.out.println(criteria);
		//criteria=criteria.add(Subqueries.in("districtMaster", query));
		criteria=criteria.add(Property.forName("districtMaster").in(query));
		System.out.println(criteria);
		listjobOrder=criteria.list();*/
		listjobOrder=getSession().createCriteria(JobOrder.class)
		.add(Property.forName("districtMaster").in(query)).list();
		//listjobOrder = findByCriteria(Subqueries.in("districtMaster", query));
		System.out.println("All job size==============="+listjobOrder.size());
		return listjobOrder;
	} 
	catch (Exception e) 
	{
		e.printStackTrace();
	}
	
	//return listjobOrder;
	return null;
}


///shadab end


	
	@Transactional(readOnly=false)
	public List<JobOrder> getJobByDates(DistrictMaster districtMaster,boolean dateFlag,Date fDate,Date tDate,int entityID) 
	{
		List<JobOrder> jobOrderList=new ArrayList<JobOrder>();
		try
		{
			boolean dateDistrictFlag=false;
			
			Criterion criterion1 = Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion2 = Restrictions.ge("createdDateTime",fDate);
			Criterion criterion3 = Restrictions.le("createdDateTime",tDate);
			
			boolean flag=false;
			if((entityID==3 || entityID==4))
			{
				flag=true;
				if(entityID==4 && dateFlag)
				{
					dateDistrictFlag=true;
					if(fDate!=null && tDate!=null)
					{
						jobOrderList=findByCriteria(criterion1,criterion2,criterion3);
						//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
					}
					else if(fDate!=null && tDate==null)
					{
						jobOrderList=findByCriteria(criterion1,criterion2);
						//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.ge("createdDateTime",fDate));
					}
					else if(fDate==null && tDate!=null)
					{
						jobOrderList=findByCriteria(criterion1,criterion3);
						//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.le("createdDateTime",tDate));
					}
				}
			}
			else if(dateFlag==false && districtMaster!=null &&  entityID==2)
			{
				flag=true;
			}
			else if(dateFlag && districtMaster!=null &&  entityID==2)
			{
				flag=true;
				if(fDate!=null && tDate!=null)
				{
					jobOrderList=findByCriteria(criterion1,criterion2,criterion3);
					//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
				}
				else if(fDate!=null && tDate==null)
				{
					jobOrderList=findByCriteria(criterion1,criterion2);
					//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.ge("createdDateTime",fDate));
				}
				else if(fDate==null && tDate!=null)
				{
					jobOrderList=findByCriteria(criterion1,criterion3);
					//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.le("createdDateTime",tDate));
				}
				dateDistrictFlag=true;
			}
			if(dateDistrictFlag==false && dateFlag)
			{
				if(fDate!=null && tDate!=null)
				{
					jobOrderList=findByCriteria(criterion2,criterion3);
					//criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
				}
				else if(fDate!=null && tDate==null)
				{
					jobOrderList=findByCriteria(criterion2);
					//criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate));
				}
				else if(fDate==null && tDate!=null)
				{
					jobOrderList=findByCriteria(criterion3);
					//criteria.createCriteria("jobId").add(Restrictions.le("createdDateTime",tDate));
				}
				flag=true;
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return jobOrderList;
	}



@Transactional(readOnly=false)
public List<JobOrder> getActiveHQJobs(HeadQuarterMaster headQuarterMaster)
{
	List<JobOrder> lstJobOrder= new ArrayList<JobOrder>();
	try 
	{
		
		Date dateWithoutTime = Utility.getDateWithoutTime();
		
		Criterion criterion1 = Restrictions.eq("status","a");
		Criterion criterion2 = Restrictions.le("jobStartDate",dateWithoutTime);
		Criterion criterion3 = Restrictions.ge("jobEndDate",dateWithoutTime);
		Criterion criterion4 = Restrictions.eq("approvalBeforeGoLive",1);
		Criterion criterion5 = Restrictions.eq("headQuarterMaster",headQuarterMaster);
		
		lstJobOrder = findByCriteria(criterion1,criterion2,criterion3,criterion4,criterion5);	
	} 
	catch (Exception e) {
		e.printStackTrace();
	}		
	return lstJobOrder;
}

@Transactional(readOnly=false)
public List<JobOrder> getActiveBranchJobs(BranchMaster branchMaster,boolean orderByFlag)
{
	List<JobOrder> lstJobOrder= new ArrayList<JobOrder>();
	try 
	{
		Session session = getSession();
		Criteria  criteria = session.createCriteria(getPersistentClass()) ;
		
		Date dateWithoutTime = Utility.getDateWithoutTime();
		
		Criterion criterion1 = Restrictions.eq("status","a");
		Criterion criterion2 = Restrictions.le("jobStartDate",dateWithoutTime);
		Criterion criterion3 = Restrictions.ge("jobEndDate",dateWithoutTime);
		Criterion criterion4 = Restrictions.eq("approvalBeforeGoLive",1);
		Criterion criterion5 = Restrictions.eq("branchMaster",branchMaster);
		if(orderByFlag){
			criteria.add(criterion1);
			criteria.add(criterion2);
			criteria.add(criterion3);
			criteria.add(criterion4);
			criteria.add(criterion5);
			Criteria  criteria1 = criteria.createCriteria("jobCategoryMaster");
			criteria1.add(Restrictions.eq("preHireSmartPractices", true));
			lstJobOrder = criteria.list();
			//Criterion criterion6 = Restrictions.eq("jobTitle","Smart Practices Non-Pilot Job");
			//lstJobOrder = findByCriteria(criterion1,criterion2,criterion3,criterion4,criterion5,criterion6);
		}
		else
			lstJobOrder = findByCriteria(criterion1,criterion2,criterion3,criterion4,criterion5);
	} 
	catch (Exception e) {
		e.printStackTrace();
	}		
	return lstJobOrder;
}
@Transactional(readOnly=false)
public List<JobOrder> findJobOrderbyHBD(HeadQuarterMaster headQuarterMaster)
{
	List<JobOrder> jobOrders = null;
	try 
	{

		Date dateWithoutTime = Utility.getDateWithoutTime();
		
		Criterion criterion1 = Restrictions.eq("status","a");
		Criterion criterion2 = Restrictions.le("jobStartDate",dateWithoutTime);
		Criterion criterion3 = Restrictions.ge("jobEndDate",dateWithoutTime);
		Criterion criterion4 = Restrictions.eq("headQuarterMaster",headQuarterMaster);
		Criterion criterion5 = Restrictions.eq("approvalBeforeGoLive",1);
		jobOrders = findByCriteria(Order.desc("createdDateTime"),criterion1,criterion2,criterion3,criterion4,criterion5);

	} 
	catch (Exception e) {
		e.printStackTrace();
	}		
	return jobOrders;
}
@Transactional(readOnly=false)
public List<JobOrder> findSortedJobOrderbyHBD(HeadQuarterMaster headQuarterMaster,Order sortOrderStrVal)
{
	
	List<JobOrder> jobOrders = null;
	try 
	{
		Date dateWithoutTime = Utility.getDateWithoutTime();

		Criterion criterion1 = Restrictions.eq("status","a");
		Criterion criterion2 = Restrictions.le("jobStartDate",dateWithoutTime);
		Criterion criterion3 = Restrictions.ge("jobEndDate",dateWithoutTime);
		Criterion criterion4 = Restrictions.eq("headQuarterMaster",headQuarterMaster);
		Criterion criterion5 = Restrictions.ne("isInviteOnly",true);
		jobOrders = findByCriteria(sortOrderStrVal,criterion1,criterion2,criterion3,criterion4,criterion5);

	} 
	catch (Exception e) {
		e.printStackTrace();
	}		
	return jobOrders;
}	

	@Transactional(readOnly=false)
	public List<JobOrder> getAllBranchesJobs()
	{
		List<JobOrder> jobOrderList = new ArrayList<JobOrder>();
		try
		{
			Date dateWithoutTime = Utility.getDateWithoutTime();
			Criterion criterion1 = Restrictions.eq("status","A");
			Criterion criterion2 = Restrictions.le("jobStartDate",dateWithoutTime);
			Criterion criterion3 = Restrictions.ge("jobEndDate",dateWithoutTime);
			Criterion criterion4 = Restrictions.eq("approvalBeforeGoLive",1);
			Criterion criterion5 = Restrictions.isNotNull("branchMaster");
			Criterion criterion6 = Restrictions.ne("isInviteOnly",true);
			jobOrderList = findByCriteria(Order.desc("jobId"),criterion1,criterion2,criterion3,criterion4,criterion5,criterion6);
		}
		catch (Exception e){
			e.printStackTrace();
		}
		return jobOrderList;
	}
	
	
	//Anurag for get all active job for headquarter based

	@Transactional(readOnly=false)
	public List<JobOrder> getActiveHQJobs(List<HeadQuarterMaster> headQuarterMaster)
	{
		List<JobOrder> lstJobOrder= new ArrayList<JobOrder>();
		try 
		{			
			Date dateWithoutTime = Utility.getDateWithoutTime();
			
			Criterion status = Restrictions.eq("status","a");
			Criterion jobStartDate = Restrictions.le("jobStartDate",dateWithoutTime);
			Criterion jobEndDate = Restrictions.ge("jobEndDate",dateWithoutTime);
			Criterion approvalBeforeGoLive = Restrictions.eq("approvalBeforeGoLive",1);
			Criterion hqMsater = Restrictions.in("headQuarterMaster",headQuarterMaster);
			
			lstJobOrder = findByCriteria(status,jobStartDate,jobEndDate,approvalBeforeGoLive,hqMsater);	
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobOrder;
	}

	//-------------------------------------------------		
	
	@Transactional(readOnly=false)
	public List<JobOrder> getJobListForInterest(Order order,int startPos,int limit, List<Criterion> criterions) 
	{
		List<JobOrder> jobOrderList=new ArrayList<JobOrder>();
		try {
			Criteria crit = getSession().createCriteria(getPersistentClass());
			for (Criterion c : criterions) {
				crit.add(c);
			}
			crit.setFirstResult(startPos);
			crit.setMaxResults(limit);

			if(order != null)
				crit.addOrder(order);

			jobOrderList=crit.list();
		} catch (HibernateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return jobOrderList;
	}
	
	
	@Transactional(readOnly=false)
	public int getJobListForInterestCount(List<Criterion> criterions) 
	{
		int rowCount=0;
		try {
			Criteria crit = getSession().createCriteria(getPersistentClass());
			for (Criterion c : criterions) {
				crit.add(c);
			}
			crit.setProjection(Projections.rowCount());
			List results = crit.list();
			rowCount = (Integer) results.get(0);
		} catch (HibernateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return rowCount;
	}
	@Transactional(readOnly=false)
	public JobOrder getListByTitle(String jobTitle,DistrictMaster districtMaster)
	{
		List<JobOrder> listjobOrder=new ArrayList<JobOrder>();
		
		if(jobTitle!=null && !jobTitle.equals(""))
	     {
			try 
			{
				Criterion criterion1 =	Restrictions.eq("jobTitle",jobTitle);
				Criterion criterion2 =  Restrictions.eq("districtMaster", districtMaster); 
				listjobOrder = findByCriteria(criterion1,criterion2);	
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
	     }
		if(listjobOrder!=null && listjobOrder.size()>0)
			return listjobOrder.get(0);
		else
			return null;
	}
	
	@Transactional(readOnly=false)
	public List<JobOrder> findJobByJobCategery(List<Integer> jobCategoryIds,Integer districtId)
	{
		List<JobOrder> jobOrders = new ArrayList<JobOrder>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			DistrictMaster dm=null;

			if(districtId!=0 && districtId>0)
		    dm = districtMasterDAO.findById(districtId, false, false);
			Criterion criterion5=null;
			
			List<JobCategoryMaster> lstjoCategoryMasters = jobCategoryMasterDAO.getMasterAndSubByCategoryID(jobCategoryIds); 
			if(lstjoCategoryMasters!=null && lstjoCategoryMasters.size()>0)
				criterion5	=Restrictions.in("jobCategoryMaster",lstjoCategoryMasters); 	
			
			if(districtId!=0 && districtId>0){
				criteria.add(Restrictions.eq("districtMaster",dm));
			}

			
			if(criterion5!=null)
			criteria.add(criterion5);
			jobOrders = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		
		return jobOrders;
	}
	
	@Transactional(readOnly=false)
	public List<JobOrder> getJobByJobCategoryHQAndBranch(List<Integer> jobCategoryIds,HeadQuarterMaster headQuarterMaster,BranchMaster branchMaster)
	{
		List<JobOrder> jobOrders = new ArrayList<JobOrder>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			
			Criterion criterion5=null;
			List<JobCategoryMaster> lstjoCategoryMasters = jobCategoryMasterDAO.findByCategoryID(jobCategoryIds); 
			if(lstjoCategoryMasters!=null && lstjoCategoryMasters.size()>0)
				criterion5	=Restrictions.in("jobCategoryMaster",lstjoCategoryMasters); 	
			
			if(headQuarterMaster!=null)
				criteria.add(Restrictions.eq("headQuarterMaster",headQuarterMaster));
			if(branchMaster!=null)
				criteria.add(Restrictions.eq("branchMaster",branchMaster));

			if(criterion5!=null)
			criteria.add(criterion5);
			jobOrders = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		
		return jobOrders;
	}
	@Transactional(readOnly=false)
	public List<JobOrder> findByJobCategeryAndDistrict(JobCategoryMaster jobCategoryMaster,DistrictMaster districtMaster)
	{
		List<JobOrder> jobOrders = new ArrayList<JobOrder>();
		try 
		{	
			Criterion criterion1	= Restrictions.eq("jobCategoryMaster",jobCategoryMaster); 	
			Criterion criterion2 =  Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion3 =  Restrictions.eq("status","A");
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());

			criteria.add(criterion1);
			criteria.add(criterion2);
			criteria.add(criterion3);
			jobOrders = criteria.list();			
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return jobOrders;
	}
	
	
	@Transactional(readOnly=false)
	public List<JobOrder> findJobByDistricts(List<DistrictMaster> districtMasters){
		List<JobOrder> lstJobOrder= null;
		try 
		{
			Criterion criterionDistrict = Restrictions.in("districtMaster",districtMasters);
			lstJobOrder = findByCriteria(criterionDistrict);
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return lstJobOrder;
	}
	
	// Search By District MAster List
	@Transactional(readOnly=false)
	public List<Integer> findJobIdsByDistrictList(List<DistrictMaster> dmLst)
	{
		List<Integer> jobOrders = null;
		try 
		{
			Date dateWithoutTime = Utility.getDateWithoutTime();
			
			/*Criterion criterion2 = Restrictions.le("jobStartDate",dateWithoutTime);
			Criterion criterion3 = Restrictions.ge("jobEndDate",dateWithoutTime);
			Criterion criterion5 = Restrictions.in("districtMaster",dmLst); 	
			Criterion criterion6 = Restrictions.eq("status","A");
			jobOrders =findByCriteria(criterion2,criterion3,criterion5,criterion6); */		
			

			Session session = getSession();
			Criteria criteria = session.createCriteria(JobOrder.class)						
		    .setProjection( Projections.projectionList()
		        .add( Projections.property("jobId"), "jobId" ));
			criteria.add(Restrictions.in("districtMaster",dmLst));
			criteria.add(Restrictions.eq("status", "A"));

			jobOrders = criteria.list();
			
		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return jobOrders;
	}

	// Search job ids By Subject List
	@Transactional(readOnly=false)
	public List<Integer> findJobIdsBySubjectList(List<SubjectMaster> ObjSubjectList)
	{
		List<Integer> jobOrders = null;
		try 
		{	
			Session session = getSession();
			Criteria criteria = session.createCriteria(JobOrder.class)						
		    .setProjection( Projections.projectionList()
		        .add( Projections.property("jobId"), "jobId" ));
			criteria.add(Restrictions.in("subjectMaster",ObjSubjectList));
			criteria.add(Restrictions.eq("status", "A"));

			jobOrders = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return jobOrders;
	}

	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public Map<Integer,JobOrder> findByAllJobsWithBranch(List jobs,BranchMaster branchMaster) 
	{
		Session session = getSession();
		Criterion criterion2 = Restrictions.in("apiJobId",jobs);
		Criterion criterion3 = Restrictions.eq("branchMaster",branchMaster);
		List result = session.createCriteria(getPersistentClass())
		.add(criterion2)
		.add(criterion3)
		.list();
		Map<Integer,JobOrder> jobOrderMap = new HashMap<Integer, JobOrder>();
		JobOrder jobOrder = null;
		int i=0;
		for (Object object : result) {
			jobOrder=((JobOrder)object);
			jobOrderMap.put(new Integer(""+i),jobOrder);
			i++;
		}
		return jobOrderMap;
	}
	
	
	@Transactional(readOnly=false)
	public List<Integer> getJobIdsBygeoZone(GeoZoneMaster geoZoneMaster)
	{
		List<Integer> listjobOrder=new ArrayList<Integer>();
		try 
		{
			
			Session session = getSession();
			Criteria criteria = session.createCriteria(JobOrder.class)						
		    .setProjection( Projections.projectionList()
		        .add( Projections.property("jobId"), "jobId" ));
			criteria.add(Restrictions.eq("geoZoneMaster",geoZoneMaster));
			criteria.add(Restrictions.eq("status", "A"));
			listjobOrder = criteria.list();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return listjobOrder;
	}
	
	@Transactional(readOnly=false)
	public List<JobOrder> getByDistrictID(DistrictMaster distId,String jobTitlemsg,String expDate,String postStart1,String endPost1)
	{
		
		List<JobOrder> listjobOrder=new ArrayList<JobOrder>();
		List<JobOrder> listjobOrderForStartDate=new ArrayList<JobOrder>();
		try 
		{
			
			Date jobendDate=null;
			Date postStart=null;
			Date endPost=null;
			if(expDate!=null && !expDate.equals(""))
			{
				jobendDate=Utility.getCurrentDateFormart(expDate);
			}
			if(postStart1!=null && !postStart1.equals(""))
			{
				postStart=Utility.getCurrentDateFormart(postStart1);
			}
			
			if(endPost1!=null && !endPost1.equals(""))
			{
				endPost=Utility.getCurrentDateFormart(endPost1);
			}
			
			
			if(distId!=null && (jobTitlemsg!=null && !jobTitlemsg.equals("")) && (jobendDate!=null && !jobendDate.equals("")) )
			{
				Criterion criterion			=	Restrictions.eq("districtMaster", distId);
				Criterion criterion1			=	Restrictions.like("jobTitle", jobTitlemsg, MatchMode.ANYWHERE);
				Criterion criterion2			=	Restrictions.eq("jobEndDate", jobendDate);
				listjobOrder = findByCriteria(criterion,criterion1,criterion2);
			}
			else if(distId!=null && (jobTitlemsg!=null && !jobTitlemsg.equals("")) )
			{
				Criterion criterion			=	Restrictions.eq("districtMaster", distId);
				Criterion criterion1			=	Restrictions.like("jobTitle", jobTitlemsg, MatchMode.ANYWHERE);
				listjobOrder = findByCriteria(criterion,criterion1);
			}
			else if(distId!=null && (jobendDate!=null && !jobendDate.equals("")))
			{
				Criterion criterion			=	Restrictions.eq("districtMaster", distId);
				Criterion criterion2			=	Restrictions.eq("jobEndDate", jobendDate);
				listjobOrder = findByCriteria(criterion,criterion2);				
			}				
			else
			{			
			Criterion criterion			=	Restrictions.eq("districtMaster", distId);
			listjobOrder = findByCriteria(criterion);			
			}
			if(distId!=null && (postStart!=null && !postStart.equals("")) && (endPost!=null && !endPost.equals("")))
			{
				Criterion criterion			=	Restrictions.eq("districtMaster", distId);
				Criterion criterion2			=	Restrictions.between("jobStartDate", postStart, endPost);
				listjobOrderForStartDate = findByCriteria(criterion,criterion2);
				listjobOrder.retainAll(listjobOrderForStartDate);
				
			}
			else if(distId!=null && (postStart!=null && !postStart.equals("")))
			{
				Criterion criterion			=	Restrictions.eq("districtMaster", distId);
				Criterion criterion2			=	Restrictions.ge("jobStartDate", postStart);
				listjobOrderForStartDate = findByCriteria(criterion,criterion2);
				listjobOrder.retainAll(listjobOrderForStartDate);
			}
			else if(distId!=null && (endPost!=null && !endPost.equals("")))
			{
				Criterion criterion			=	Restrictions.eq("districtMaster", distId);
				Criterion criterion2			=	Restrictions.le("jobStartDate", endPost);
				listjobOrderForStartDate = findByCriteria(criterion,criterion2);
				listjobOrder.retainAll(listjobOrderForStartDate);
			}
				
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return listjobOrder;
	}
	
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public Map<Integer,JobOrder> findByAllJobsWithDistrict(List jobs,DistrictMaster districtMaster) 
	{
		Session session = getSession();
		List result = null;
		Criterion criterion2 = Restrictions.in("apiJobId",jobs);
		if(districtMaster!=null){
			Criterion criterion			=	Restrictions.eq("districtMaster", districtMaster);
			 result = session.createCriteria(getPersistentClass())
			.add(criterion2)
			.add(criterion)
			.list();
		}else{
			 result = session.createCriteria(getPersistentClass())
			.add(criterion2)
			.list();
		}
		Map<Integer,JobOrder> jobOrderMap = new HashMap<Integer, JobOrder>();
		JobOrder jobOrder = null;
		int i=0;
		for (Object object : result) {
			jobOrder=((JobOrder)object);
			jobOrderMap.put(new Integer(""+i),jobOrder);
			i++;
		}
		return jobOrderMap;
	}

	@Transactional(readOnly=false)
	public List<JobOrder> findJobOrderbyDistrictList(List<DistrictMaster> listDistrictMaster)
	{
		List<JobOrder> jobOrders = null;
		try 
		{

			Date dateWithoutTime = Utility.getDateWithoutTime();
			
			Criterion criterion1 = Restrictions.eq("status","a");
			Criterion criterion2 = Restrictions.le("jobStartDate",dateWithoutTime);
			Criterion criterion3 = Restrictions.ge("jobEndDate",dateWithoutTime);
			Criterion criterion4 = Restrictions.in("districtMaster",listDistrictMaster);
			Criterion criterion5 = Restrictions.eq("approvalBeforeGoLive",1);

			jobOrders = findByCriteria(Order.desc("createdDateTime"),criterion1,criterion2,criterion3,criterion4,criterion5);

		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return jobOrders;
	}
	
	@Transactional(readOnly=false)
	public List<JobOrder> findJobOrderByJobCategories(List<JobCategoryMaster> jobCategoryMaster)
	{
		List<JobOrder> jobOrders = new ArrayList<JobOrder>();
		try 
		{
			if(jobCategoryMaster!=null && jobCategoryMaster.size()>0){
				Criterion criterion5 = Restrictions.in("jobCategoryMaster",jobCategoryMaster); 	
				Criterion criterion6 = Restrictions.eq("status","A");

				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());

				criteria.add(criterion5);
				criteria.add(criterion6);
				jobOrders = criteria.list();
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return jobOrders;
	}
	
	@Transactional(readOnly=false)
	public List<JobOrder> findJobOrderForReportByDistrict(Integer districtId,Integer jobId,Date endStartDate, Date endToDate, int schoolId){
		List<JobOrder> jobOrderList =  new ArrayList<JobOrder>();
		List<JobOrder > lstJobOrderBySchoolId=new ArrayList<JobOrder>();
		try{
			Session session=getSession();			
			Criteria criteria=session.createCriteria(getPersistentClass());	
			//by school id
			if(schoolId!=0){
				Criteria criteria1=session.createCriteria(SchoolInJobOrder.class);	
				criteria1.add(Restrictions.eq("schoolId", schoolMasterDAO.findById(new Long(schoolId), false, false)));
				//criteria1.createCriteria("jobId").add(Restrictions.eq("status", "A"));
				List<SchoolInJobOrder> lstSchInJOrder=criteria1.list();
				for(SchoolInJobOrder sijo:lstSchInJOrder)
					if(sijo.getJobId()!=null)
						lstJobOrderBySchoolId.add(sijo.getJobId());						
			}	
			
			if(districtId!=0){
				criteria.add(Restrictions.eq("districtMaster",districtMasterDAO.findById(districtId, false, false)));
				//criteria.add(Restrictions.eq("status", "A"));
			}
			
			if(jobId!=0)
				criteria.add(Restrictions.eq("jobId",jobId));
			
			criteria.add(Restrictions.ge("jobEndDate",endStartDate)).add(Restrictions.le("jobEndDate",endToDate));
			jobOrderList=criteria.list();
			if(schoolId!=0)
				jobOrderList.retainAll(lstJobOrderBySchoolId);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return jobOrderList;
	}
	
	@Transactional(readOnly=false)
	public List<String[]> findActiveAndInactiveJobOrderByDistrict(Integer districtId){
		List<String[]> jobOrderList =  new ArrayList<String[]>();
		try{
			Session session = getSession();	
			Criteria criteria = session.createCriteria(getPersistentClass());			
			criteria.createAlias("districtMaster", "dm")
		    .setProjection( Projections.projectionList()
		        .add( Projections.property("jobId"), "jobId" )		        
		    );
			criteria.add(Restrictions.eq("dm.districtId",districtId));
			//criteria.add(Restrictions.eq("status","A"));
			jobOrderList=criteria.list();			
		
		}catch(Exception e){
			e.printStackTrace();
		}			
			return jobOrderList;	
	}
	
	
	// Anurag get list of jobid only
	
	 
	@Transactional(readOnly=false)
	public List<JobOrder> findJobIdByJobCategoryMasterAll(JobCategoryMaster jobCategoryMaster)
	{
		List<JobOrder> jobOrders = null;
		try 
		{
			 long start = new Date().getTime();
			Criterion criterion5 = Restrictions.eq("jobCategoryMaster",jobCategoryMaster); 	
			 
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());

			criteria.add(criterion5);
			criteria.setProjection(Projections.groupProperty("jobId"));
			jobOrders = criteria.list();
			
			
			  long end = new Date().getTime();
			     System.out.println("total time taken ==="+(end-start)+"ms");
			     
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return jobOrders;
	}
	
	@Transactional(readOnly=false)
	public List<Integer> findJobIdsByDistrict(DistrictMaster  districtMaster)
	{
		List<Integer> listjobOrder=new ArrayList<Integer>();
		try 
		{
			
			Session session = getSession();
			Criteria criteria = session.createCriteria(JobOrder.class)						
		    .setProjection( Projections.projectionList()
		        .add( Projections.property("jobId"), "jobId" ));
			criteria.add(Restrictions.eq("districtMaster",districtMaster));
			listjobOrder = criteria.list();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return listjobOrder;
	}
	
	@Transactional(readOnly=false)
	public List<Integer> getJobApplicationIdsStatusRole(Integer jobStatusId)
	{
		List<Integer> listjobOrder=new ArrayList<Integer>();
		try 
		{
			/*Criterion criterion =	Restrictions.eq("jobApplicationStatus",jobStatusId);
			listjobOrder = findByCriteria(criterion);	*/
			
			Session session = getSession();
			Criteria criteria = session.createCriteria(JobOrder.class)						
		    .setProjection( Projections.projectionList()
		        .add( Projections.property("jobId"), "jobId" ));
			criteria.add(Restrictions.eq("jobApplicationStatus",jobStatusId));
			listjobOrder = criteria.list();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return listjobOrder;
	}

	@Transactional(readOnly=false)
	public List<Integer> getJobOrderIdsBygeoZoneRole(GeoZoneMaster geoZoneMaster,UserMaster userMaster)
	{
		List<Integer> listjobOrder=new ArrayList<Integer>();
		try 
		{
			
			Criterion criterion =	Restrictions.eq("geoZoneMaster",geoZoneMaster);
			Criterion criterion2 =	Restrictions.or(criterion, Restrictions.isNull("geoZoneMaster"));
			/*listjobOrder = findByCriteria(criterion2);	*/
			
			Session session = getSession();
			Criteria criteria = session.createCriteria(JobOrder.class)						
		    .setProjection( Projections.projectionList()
		        .add( Projections.property("jobId"), "jobId" ));
			if(userMaster!=null)
				criteria.add(criterion2);
			else
				criteria.add(criterion);
			
			listjobOrder = criteria.list();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return listjobOrder;
	}
	
	@Transactional(readOnly=false)
	public List<Integer> findJobIdsByJobCategery(List<Integer> jobCategoryIds,Integer districtId)
	{
		List<Integer> jobOrders = new ArrayList<Integer>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(JobOrder.class)						
		    .setProjection( Projections.projectionList()
		        .add( Projections.property("jobId"), "jobId" ));
			DistrictMaster dm= new DistrictMaster();

			if(districtId!=0 && districtId>0)
				dm.setDistrictId(districtId);//districtMasterDAO.findById(districtId, false, false);
			Criterion criterion5=null;
			
			List<JobCategoryMaster> lstjoCategoryMasters = jobCategoryMasterDAO.getMasterAndSubByCategoryID(jobCategoryIds); 
			if(jobCategoryIds!=null && jobCategoryIds.size()>0)
				criterion5	=Restrictions.in("jobCategoryMaster",lstjoCategoryMasters); 	
			
			if(districtId!=0 && districtId>0){
				criteria.add(Restrictions.eq("districtMaster",dm));
			}

			
			if(criterion5!=null)
			criteria.add(criterion5);
			jobOrders = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		
		return jobOrders;
	}
	@Transactional(readOnly=false)
	public List<JobOrder> findNoArchieveJobByDistricts(List<DistrictMaster> districtMasters){
		List<JobOrder> lstJobOrder= null;
		try 
		{
			Criterion criterion1 = Restrictions.in("districtMaster",districtMasters);
			Criterion criterion2 = Restrictions.ne("status","R");
			lstJobOrder = findByCriteria(criterion1,criterion2);
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return lstJobOrder;
	}
	@Transactional(readOnly=false)
	public List<JobOrder> findJobOrderByJobCategoriesWithoutStatus(List<JobCategoryMaster> jobCategoryMaster)
	{
		List<JobOrder> jobOrders = new ArrayList<JobOrder>();
		try 
		{
			if(jobCategoryMaster!=null && jobCategoryMaster.size()>0){
				Criterion criterion1 = Restrictions.in("jobCategoryMaster",jobCategoryMaster); 				
				jobOrders = findByCriteria(criterion1);				
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return jobOrders;
	}
}