package tm.api.rest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.URLDecoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

import org.apache.activemq.command.ActiveMQQueue;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import tm.api.JSONUtility;
import tm.api.UtilityAPI;
import tm.bean.DistrictSpecificVideoInterviewDetails;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.MessageToTeacher;
import tm.bean.TalentKSNDetail;
import tm.bean.TeacherDetail;
import tm.bean.TeacherStatusHistoryForJob;
import tm.bean.cgreport.TeacherStatusNotes;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.hqbranchesmaster.StatusNodeColorHistory;
import tm.bean.hqbranchesmaster.WorkFlowStatus;
import tm.bean.i4.I4InterviewInvites;
import tm.bean.i4.VVIResponse;
import tm.bean.master.CertificateTypeMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictSchools;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.SecondaryStatus;
import tm.bean.master.StatusMaster;
import tm.bean.master.SubjectMaster;
import tm.bean.mq.MQEvent;
import tm.bean.mq.MQEventHistory;
import tm.bean.user.UserMaster;
import tm.dao.DistrictSpecificVideoInterviewDetailsDAO;
import tm.dao.JobForTeacherDAO;
import tm.dao.MessageToTeacherDAO;
import tm.dao.TalentKSNDetailDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.TeacherStatusHistoryForJobDAO;
import tm.dao.assessment.AssessmentDetailDAO;
import tm.dao.cgreport.TeacherStatusNotesDAO;
import tm.dao.hqbranchesmaster.HeadQuarterMasterDAO;
import tm.dao.hqbranchesmaster.StatusNodeColorHistoryDAO;
import tm.dao.hqbranchesmaster.WorkFlowStatusDAO;
import tm.dao.i4.I4InterviewInvitesDAO;
import tm.dao.i4.VVIResponseDAO;
import tm.dao.master.CertificateTypeMasterDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.DistrictSchoolsDAO;
import tm.dao.master.SecondaryStatusDAO;
import tm.dao.master.StatusMasterDAO;
import tm.dao.master.SubjectMasterDAO;
import tm.dao.mq.MQEventDAO;
import tm.dao.mq.MQEventHistoryDAO;
import tm.dao.user.UserMasterDAO;
import tm.services.AssessmentCampaignAjax;
import tm.services.CommonService;
import tm.services.EmailerService;
import tm.services.district.ManageStatusAjax;
import tm.services.hqbranches.KellyONRAjax;
import tm.services.mq.MQService;
import tm.services.report.CandidateGridService;
import tm.services.teacher.DWRAutoComplete;
import tm.servlet.WorkThreadServlet;
import tm.utility.IPAddressUtility;
import tm.utility.Utility;

import com.sun.jersey.api.client.ClientResponse.Status;
import com.sun.jersey.api.core.InjectParam;
import com.sun.jersey.spi.inject.Inject;



@Component
@Path("/callback")
public class ClientCallbackServices {
	@InjectParam
	private DistrictSchoolsDAO districtSchoolsDAO;
	@InjectParam
	private CertificateTypeMasterDAO certificateTypeMasterDAO;
	@InjectParam
	private SubjectMasterDAO subjectMasterDAO;

	String locale = Utility.getValueOfPropByKey("locale");
	 
	@InjectParam
	private DistrictSpecificVideoInterviewDetailsDAO districtSpecificVideoInterviewDetailsDAO;
	
	@InjectParam
	private DistrictMasterDAO districtMasterDAO;
	
	@InjectParam
	private TeacherDetailDAO teacherDetailDAO;
	
	@SuppressWarnings("deprecation")
	@Inject
	private I4InterviewInvitesDAO i4InterviewInvitesDAO;
	
	@InjectParam
	private VVIResponseDAO vviResponseDAO;
	
	@InjectParam
	private HeadQuarterMasterDAO headQuarterMasterDAO;
	
	@InjectParam
	private MQEventDAO mqEventDAO;
	
	@InjectParam
	private MQEventHistoryDAO mqEventHistoryDAO;
	
	@InjectParam
	private UserMasterDAO userMasterDAO;
	
	@InjectParam
	private JobForTeacherDAO jobForTeacherDAO;
	
	@InjectParam
	private SecondaryStatusDAO secondaryStatusDAO;
	
	@InjectParam
	private CommonService commonService;
	
	@InjectParam
	private EmailerService emailerService;
	
	@InjectParam
	private TeacherStatusHistoryForJobDAO teacherStatusHistoryForJobDAO;
	
	@InjectParam
	private ManageStatusAjax manageStatusAjax; 
	
	@InjectParam
	private MQService mqService;
	
	@InjectParam
	private TalentKSNDetailDAO talentKSNDetailDAO;
	
	@InjectParam
	private StatusMasterDAO statusMasterDAO;
	
	@InjectParam
	private WorkFlowStatusDAO workFlowStatusDAO;
	
	@InjectParam
	private TeacherStatusNotesDAO teacherStatusNotesDAO;
	
	@InjectParam
	private AssessmentDetailDAO assessmentDetailDAO;
	
	@InjectParam
	private StatusNodeColorHistoryDAO statusNodeColorHistoryDAO;
	
	@InjectParam
	private CandidateGridService candidateGridService;
	
	@InjectParam
	private AssessmentCampaignAjax assessmentCompaignAjax;
	
	@InjectParam
	private MessageToTeacherDAO messageToTeacherDAO; 
	
	@POST
	@Produces(MediaType.TEXT_XML)
	@Consumes(MediaType.TEXT_XML)
	@Path("/postCandidateVideoDataToTM")
	public Response postCandidateVideoDataToTM(InputStream incomingData,@Context HttpServletRequest request, @Context HttpServletResponse res) 
	{

		Document doc = null;
		Element root = null;	
		String returnVal="";
		try {			
			System.out.println("Enter in postCandidateVideoDataToTM XML");

			try {
				String payloadRequest = "";

				StringBuilder sb = new StringBuilder();
				try {
					BufferedReader in = new BufferedReader(new InputStreamReader(incomingData));
					String line = null;
					while ((line = in.readLine()) != null) {
						sb.append(line);
					}
				} catch (Exception e) {
					System.out.println("Error Parsing: - ");
				}
				// System.out.println("Data Received: " + crunchifyBuilder.toString());
				payloadRequest = sb.toString();
				System.err.println("payloadRequest::= "+payloadRequest);

				DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();

				try {
					//Using factory get an instance of document builder
					DocumentBuilder docBuilder = null;
					
					docBuilder = builderFactory.newDocumentBuilder();
					//parse using builder to get DOM representation of the XML file
					InputSource is = new InputSource();
				    is.setCharacterStream(new StringReader(payloadRequest));
					doc = docBuilder.parse(is);
					NodeList CandidateVideoDetails = doc.getElementsByTagName("CandidateVideoData");
				    if (CandidateVideoDetails.getLength() > 0) {
				        Element element = (Element)CandidateVideoDetails.item(0);
				       String AuthKey = element.getElementsByTagName("authKey").item(0).getTextContent();
				        System.out.println("AuthKey: "+AuthKey);
				        String CandidateFirstName = element.getElementsByTagName("candidateFirstName").item(0).getTextContent();
				        System.out.println("CandidateFirstName: "+CandidateFirstName);
				        String CandidateLastName = element.getElementsByTagName("candidateLastName").item(0).getTextContent();
				        System.out.println("CandidateLastName: "+CandidateLastName);
				        String CandidateEmailAddress = element.getElementsByTagName("candidateEmailAddress").item(0).getTextContent();
				        System.out.println("CandidateEmailAddress: "+CandidateEmailAddress);
				        String videoURL = element.getElementsByTagName("videoURL").item(0).getTextContent();
				        System.out.println("videoURL: "+videoURL);
				        String CompletedDateTime = element.getElementsByTagName("completedDateTime").item(0).getTextContent();
				        System.out.println("CompletedDateTime: "+CompletedDateTime);
				        
				        ServletContext context = request.getSession().getServletContext();
						ApplicationContext context0 = WebApplicationContextUtils.getWebApplicationContext(context);
						districtMasterDAO = (DistrictMasterDAO)context0.getBean("districtMasterDAO");
						districtSpecificVideoInterviewDetailsDAO = (DistrictSpecificVideoInterviewDetailsDAO)context0.getBean("districtSpecificVideoInterviewDetailsDAO");
						
						
						
				        DistrictMaster dm =  districtMasterDAO.validateDistrictByAuthkey(AuthKey);
				        System.out.println("districtMaster:: "+dm);
				        doc = docBuilder.newDocument();
						root = doc.createElement("message");
						doc.appendChild(root);
						
				        Element eleStatus = doc.createElement("status");
						root.appendChild(eleStatus);
						Text textStatus = doc.createTextNode("true");
				        if(dm==null)
				        {
				        	textStatus = doc.createTextNode("false");
				        }
				        
				        eleStatus.appendChild(textStatus);
				        
				        Element timeStamp = doc.createElement("timeStamp");
						root.appendChild(timeStamp);
						Text timeStampResult = doc.createTextNode(UtilityAPI.getCurrentTimeStamp());
						timeStamp.appendChild(timeStampResult);
						
						
						TransformerFactory factory = TransformerFactory.newInstance();
						Transformer transformer = factory.newTransformer();
						
						StringWriter sw = new StringWriter();
						StreamResult result = new StreamResult(sw);
						DOMSource source = new DOMSource(doc);
						transformer.transform(source, result);
						returnVal = sw.toString();
				       // dm.setDistrictId(7800038);
				        DistrictSpecificVideoInterviewDetails specificVideoInterviewDetails = new DistrictSpecificVideoInterviewDetails();
				        specificVideoInterviewDetails.setDistrictMaster(dm);
				        try {
							CandidateEmailAddress = Utility.decodeBase64(CandidateEmailAddress);
						} catch (Exception e2) {
						}
						
				        List<DistrictSpecificVideoInterviewDetails> districtSpecificVideoInterviewDetails = districtSpecificVideoInterviewDetailsDAO.checkCandidateDataSubmittedByEmail(CandidateEmailAddress,dm);
				        System.out.println("districtSpecificVideoInterviewDetails:: "+districtSpecificVideoInterviewDetails.size());
				        if(districtSpecificVideoInterviewDetails.size()>0)
				        {
				        	specificVideoInterviewDetails.setCandidateId(districtSpecificVideoInterviewDetails.get(0).getCandidateId());	
				        }
				        specificVideoInterviewDetails.setCreatedDateTime(new Date());
				        specificVideoInterviewDetails.setFirstName(CandidateFirstName);
				        specificVideoInterviewDetails.setLastName(CandidateLastName);
				        specificVideoInterviewDetails.setEmailAddress(CandidateEmailAddress);
				        specificVideoInterviewDetails.setVideoURL(URLDecoder.decode(videoURL, "UTF-8"));
				       // specificVideoInterviewDetails.setVideoURL(videoURL);
				       
				        //DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
				        // New format
				        DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
				        Date completedDateTime = null;
				        try{					
				        	completedDateTime = (Date)formatter.parse(CompletedDateTime);
							System.out.println("jobStartDate> "+completedDateTime);

						} 
						catch (Exception e) {
							try {
								formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
								completedDateTime = (Date)formatter.parse(CompletedDateTime);
								System.out.println("jobStartDate> "+completedDateTime);
							} catch (Exception e1) {
								completedDateTime = null;	
							}
						}
						
						specificVideoInterviewDetails.setCompletedDateTime(completedDateTime);
						specificVideoInterviewDetails.setStatus("A");
				        districtSpecificVideoInterviewDetailsDAO.makePersistent(specificVideoInterviewDetails);
				        
				    } else { 
				        // success
				    }

				}catch(ParserConfigurationException pce) {
					pce.printStackTrace();
				}catch(SAXException se) {
					se.printStackTrace();
				}catch(IOException ioe) {
					ioe.printStackTrace();
				}
			} 
			catch (Exception e) {
				e.printStackTrace();
				UtilityAPI.writeXMLErrors(100011,"",null, doc, root);

			}
			//out.print(">>>>"+jsonRequest);
			//out.print(jsonResponse);
			//returnVal = jsonResponse.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Response.status(200).entity(returnVal.toString()).build();
	}
	public static String getCharacterDataFromElement(Element e) {
	    Node child = e.getFirstChild();
	    if (child instanceof CharacterData) {
	      CharacterData cd = (CharacterData) child;
	      return cd.getData();
	    }
	    return "";
	  }
	
	
	/*@POST
	@Path("/postCandidateVideoDataFromI4ToTM")
	public Response postCandidateVideoDataFromI4ToTM(InputStream incomingData,@Context HttpServletRequest request, @Context HttpServletResponse res) 
	{
		System.out.println(" ============= postCandidateVideoDataFromI4ToTM ========= ");
	    ServletContext context = request.getSession().getServletContext();
		ApplicationContext context0 = WebApplicationContextUtils.getWebApplicationContext(context);
		i4InterviewInvitesDAO = (I4InterviewInvitesDAO)context0.getBean("i4InterviewInvitesDAO");
		String returnVal="";
		try {			
			System.out.println("Enter in postCandidateVideoDataToTM JSON called");

			JSONObject jsonResponse = new JSONObject();

			try {
				String payloadRequest = "";

				StringBuilder sb = new StringBuilder();
				try {
					BufferedReader in = new BufferedReader(new InputStreamReader(incomingData));
					String line = null;
					while ((line = in.readLine()) != null) {
						sb.append(line);
					}
				} catch (Exception e) {
					System.out.println("Error Parsing: - ");
				}
				payloadRequest = sb.toString();
				JSONObject jsonRequest = (JSONObject) JSONSerializer.toJSON( payloadRequest ); 
				
				JSONObject jsonRequestCan = jsonRequest.getJSONObject(Utility.getLocaleValuePropByKey("lblCANDIDATE", locale));
				
				JSONObject jsonRequestInterview = jsonRequestCan.getJSONObject("interview");
				
				System.out.println("jsonRequestInterview : "+jsonRequestInterview.toString());
				
				
				
				String InterViewInviteId = jsonRequestInterview.getString("id");
				String completeddate = jsonRequestInterview.getString("completed");
				String player_url = jsonRequestInterview.getString("player_url");
				
				System.out.println(" InterViewInviteId : "+InterViewInviteId+" completeddate : "+completeddate+" player_url : "+player_url);
				
				player_url = player_url.replace("http", "https");

				System.out.println(">> Old player_url >> "+player_url);
				
				player_url = player_url.replace("video_interview_viewer.php", "embed_interview_viewer.php");
				
				System.out.println(">> New player_url >> "+player_url);
				
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				
				Date date = formatter.parse(completeddate);
				
			
				System.out.println(" ddddddddddate :: "+date);
				
				List<I4InterviewInvites> i4InterviewInvites = new ArrayList<I4InterviewInvites>();
				System.out.println("i4InterviewInvitesDAO:: "+i4InterviewInvitesDAO);
				i4InterviewInvites = i4InterviewInvitesDAO.findByInviteId(InterViewInviteId);
				
				
				i4InterviewInvites.get(0).setVideoUrl(player_url);
				i4InterviewInvites.get(0).setCompletedDateTime(date);
				i4InterviewInvites.get(0).setStatus(3);
				
				i4InterviewInvitesDAO.makePersistent(i4InterviewInvites.get(0));
				
				
				String errors = "";
				jsonResponse = new JSONObject();
				if(errors.equals(""))
					jsonResponse.put("status", true);
				else
					jsonResponse.put("status", false);
				jsonResponse.put("timeStamp", UtilityAPI.getCurrentTimeStamp());
				if(!errors.equals(""))
					jsonResponse.put("errorMessage", errors);
				else
					jsonResponse.put("errorMessage", "Data recieved.");


			} 
			catch (Exception e) {
				jsonResponse = UtilityAPI.writeJSONErrors(10017, "Server Error.", e);

			}
			returnVal = jsonResponse.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Response.status(200).entity(returnVal.toString()).build();
	}*/
	
	@POST
	@Path("/postCandidateVideoDataFromI4ToTM")
	public Response postCandidateVideoDataFromI4ToTM(InputStream incomingData,@Context HttpServletRequest request, @Context HttpServletResponse res) 
	{
		System.out.println(" ============= postCandidateVideoDataFromI4ToTM 11111111 ========= ");
	    ServletContext context = request.getSession().getServletContext();
		ApplicationContext context0 = WebApplicationContextUtils.getWebApplicationContext(context);
		i4InterviewInvitesDAO = (I4InterviewInvitesDAO)context0.getBean("i4InterviewInvitesDAO");
		vviResponseDAO = (VVIResponseDAO)context0.getBean("vVIResponseDAO");
		districtMasterDAO = (DistrictMasterDAO)context0.getBean("districtMasterDAO");
		userMasterDAO = (UserMasterDAO)context0.getBean("userMasterDAO");
		
		String returnVal="";
		try {			
			System.out.println("Enter in postCandidateVideoDataToTM JSON called >>>>>>>>>>>>>>");

			JSONObject jsonResponse = new JSONObject();

			try {
				String payloadRequest = "";

				System.out.println("01");
				
				StringBuilder sb = new StringBuilder();
				System.out.println("02");
				
				try {
					System.out.println("03");
					BufferedReader in = new BufferedReader(new InputStreamReader(incomingData));
					String line = null;
					while ((line = in.readLine()) != null) {
						sb.append(line);
					}
					System.out.println("04");
				} catch (Exception e) {
					System.out.println("Error Parsing: - ");
				}
				
				
				System.out.println("05");
				
				payloadRequest = sb.toString();
				
				System.out.println("06");
				System.out.println(" payloadRequest "+payloadRequest);
				
				JSONObject jsonRequestInterview = (JSONObject) JSONSerializer.toJSON( payloadRequest ); 
				
				System.out.println("07");
				
			   JSONObject jsonRequest = jsonRequestInterview.getJSONObject("candidate");
				
				String Id = jsonRequest.getString("id");
				JSONObject jsonInterview = jsonRequest.getJSONObject("interview");
				String completeddate = jsonInterview.getString("completed");
				
				
				
				System.out.println(" 08 ");
				
				
				/*
				JSONObject jsonRequestCan = jsonRequest.getJSONObject(Utility.getLocaleValuePropByKey("lblCANDIDATE", locale));
				
				System.out.println("08");
				
				JSONObject jsonRequestInterview = jsonRequestCan.getJSONObject("interview");
				
				System.out.println("jsonRequestInterview : "+jsonRequestInterview.toString());
				
				String InterViewInviteId = jsonRequestInterview.getString("id");
				String completeddate = jsonRequestInterview.getString("completed");
				String player_url = jsonRequestInterview.getString("player_url");*/
				String userAssesmentId = "";
				
				System.out.println(" 09 ");
				
				
				///////////////////////////////////////////////////////////////////
				//JSONArray jsonArrayResponse = jsonRequest.getJSONArray("responses");
				JSONArray jsonArrayResponse = jsonInterview.getJSONArray("responses");
				String InterViewInviteId = "";//jsonArrayResponse.getString("user_assessment_id");
				
				System.out.println(" 10 ");
				
				JSONObject jobectData = null;
				VVIResponse vviResponse = null;
				
				System.out.println(" 11 ");

				System.out.println(" jsonArrayResponse "+jsonArrayResponse);
				
				System.out.println("  vviResponseDAO :::::: "+vviResponseDAO);
				
				
 				if(jsonArrayResponse!=null)
				{
 					System.out.println(" 11.11 ");
					for(int i=0;i<jsonArrayResponse.size();i++)
					{
						System.out.println(" 11.12 ");
						jobectData = jsonArrayResponse.getJSONObject(i);
						if(jobectData!=null)
						{
							String answerFile = jobectData.getString("answer_file");
							String answerUrl = jobectData.getString("answer_url");
							String mp4Link = jobectData.getString("mp4_link");
							String webLink = jobectData.getString("webm_link");
							
							/*try{
								if(answerFile.contains("%27"))
									answerFile.replace("%27", "");
								
								if(answerUrl.contains("%27"))
									answerUrl.replace("%27", "");
								
								if(mp4Link.contains("%27"))
									mp4Link.replace("%27", "");
								
								if(webLink.contains("%27"))
									webLink.replace("%27", "");
							
								System.out.println(" answerFile :: "+answerFile);
								System.out.println(" answerUrl :: "+answerUrl);
								System.out.println(" mp4Link :: "+mp4Link);
								System.out.println(" webLink :: "+webLink);
								
							
							}catch(Exception e){
								e.printStackTrace();
							}*/
							
							
							System.out.println(" 11.13 ");
							vviResponse = new  VVIResponse();
							userAssesmentId = jobectData.getString("user_assessment_id");
							InterViewInviteId = jobectData.getString("user_assessment_id");
							vviResponse.setResponseId(jobectData.getString("id"));
							vviResponse.setI4InviteId(jobectData.getString("user_assessment_id"));
							vviResponse.setInterviewQuestionId(jobectData.getString("interview_question_id"));
							vviResponse.setAnswerFile(answerFile);
							vviResponse.setAnswerUrl(answerUrl);
							vviResponse.setMp4Link(mp4Link);
							vviResponse.setWebmLink(webLink);
							vviResponse.setDuration(jobectData.getString("duration"));
							vviResponseDAO.makePersistent(vviResponse);
						}
					}
				}
				
 				System.out.println(" 12 ");
 				
				///////////////////////////////////////////////////////////////////
				
				
 				
				System.out.println(" InterViewInviteId : "+InterViewInviteId+" completeddate : "+completeddate);
				
				/*player_url = player_url.replace("http", "https");

				System.out.println(">> Old player_url >> "+player_url);
				
				player_url = player_url.replace("video_interview_viewer.php", "embed_interview_viewer.php");
				
				System.out.println(">> New player_url >> "+player_url);*/
				
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				
				Date date = formatter.parse(completeddate);
				
			
				System.out.println(" ddddddddddate :: "+date);
				
				List<I4InterviewInvites> i4InterviewInvites = new ArrayList<I4InterviewInvites>();
				System.out.println("i4InterviewInvitesDAO:: "+i4InterviewInvitesDAO);
				i4InterviewInvites = i4InterviewInvitesDAO.findByInviteId(InterViewInviteId);
				
				
				//i4InterviewInvites.get(0).setVideoUrl(player_url); 
				i4InterviewInvites.get(0).setUserAssessmentId(userAssesmentId);
				i4InterviewInvites.get(0).setCompletedDateTime(date);
				i4InterviewInvites.get(0).setStatus(3);
				i4InterviewInvitesDAO.makePersistent(i4InterviewInvites.get(0));
				
				//Send Mail to all DAs after VVI completion
				DistrictMaster districtMaster =null;
				System.out.println("==================== Start mail send after VVI completion =============================");
				try
				{
					TeacherDetail teacherDetail = null;
					//DistrictMaster districtMaster = districtMasterDAO.findByDistrictId(Integer.toString(i4InterviewInvites.get(0).getDistrictId()));
					if(i4InterviewInvites!=null && i4InterviewInvites.get(0)!=null && i4InterviewInvites.get(0).getDistrictId()!=null)
						districtMaster = districtMasterDAO.findById(i4InterviewInvites.get(0).getDistrictId(),false,false);
					
					JobOrder jobOrder = i4InterviewInvites.get(0).getJobOrder();
					teacherDetail = i4InterviewInvites.get(0).getTeacherDetail();
					if(districtMaster!=null && jobOrder!=null ){
						if(districtMaster.getDistrictId()==7800292){
							System.out.println("07");
							assessmentCompaignAjax.sendMailToAllDAAfterEpiComplition(userMasterDAO,districtMaster,teacherDetail,jobOrder,"VVI");
							System.out.println("08");
						}
					}else{
						if(districtMaster!=null && districtMaster.getDistrictId()==7800292)
							assessmentCompaignAjax.sendMailToAllDAAfterVVIComplitionForEvent(userMasterDAO,districtMaster,teacherDetail,i4InterviewInvites.get(0),"VVI");
					}
				System.out.println("==================== End mail send after VVI completion =============================");
				}
				catch(Exception e){e.printStackTrace();}
				
				String errors = "";
				jsonResponse = new JSONObject();
				if(errors.equals(""))
					jsonResponse.put("status", true);
				else
					jsonResponse.put("status", false);
				jsonResponse.put("timeStamp", UtilityAPI.getCurrentTimeStamp());
				if(!errors.equals(""))
					jsonResponse.put("errorMessage", errors);
				else
					jsonResponse.put("errorMessage", "Data recieved.");


			} 
			catch (Exception e) {
				jsonResponse = UtilityAPI.writeJSONErrors(10017, "Server Error.", e);
				e.printStackTrace();

			}
			returnVal = jsonResponse.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Response.status(200).entity(returnVal.toString()).build();
	}
	
	
	@GET
	@Path("/postCandidateVideoDataFromI4ToTM")
	public Response getCandidateVideoDataFromI4ToTM(InputStream incomingData,@Context HttpServletRequest request, @Context HttpServletResponse res) 
	{
		System.out.println(" ============= postCandidateVideoDataFromI4ToTM ========= ");
	    ServletContext context = request.getSession().getServletContext();
		ApplicationContext context0 = WebApplicationContextUtils.getWebApplicationContext(context);
		i4InterviewInvitesDAO = (I4InterviewInvitesDAO)context0.getBean("i4InterviewInvitesDAO");
		vviResponseDAO = (VVIResponseDAO)context0.getBean("vviResponseDAO");
		String returnVal="";
		try {			
			System.out.println("Enter in postCandidateVideoDataToTM JSON called ............");

			JSONObject jsonResponse = new JSONObject();

			System.out.println("01");
			
			
			try {
				String payloadRequest = "";

				StringBuilder sb = new StringBuilder();
				System.out.println("02");
				
				try {
					
					System.out.println("03");
					BufferedReader in = new BufferedReader(new InputStreamReader(incomingData));
					String line = null;
					while ((line = in.readLine()) != null) {
						sb.append(line);
					}
				} catch (Exception e) {
					System.out.println("Error Parsing: - ");
				}
				
				System.out.println("04");
				
				payloadRequest = sb.toString();
				
				System.out.println("05");
				JSONObject jsonRequest = (JSONObject) JSONSerializer.toJSON( payloadRequest ); 
				
				System.out.println("06");
				
				JSONObject jsonRequestCan = jsonRequest.getJSONObject("candidate");
				
				JSONObject jsonRequestInterview = jsonRequestCan.getJSONObject("interview");
				
				System.out.println("jsonRequestInterview : "+jsonRequestInterview.toString());
				
				String InterViewInviteId = jsonRequestInterview.getString("id");
				String completeddate = jsonRequestInterview.getString("completed");
				String player_url = jsonRequestInterview.getString("player_url");
				String userAssesmentId = "";
				
				///////////////////////////////////////////////////////////////////
				JSONArray jsonArrayResponse = jsonRequestInterview.getJSONArray("responses");
				JSONObject jobectData = null;
				VVIResponse vviResponse = null;
 				if(jsonArrayResponse!=null)
				{
					for(int i=0;i<jsonArrayResponse.size();i++)
					{
						jobectData = jsonArrayResponse.getJSONObject(i);
						if(jobectData!=null)
						{
							vviResponse = new  VVIResponse();
							userAssesmentId = jobectData.getString("user_assessment_id");
							vviResponse.setResponseId(jobectData.getString("id"));
							vviResponse.setI4InviteId(jobectData.getString("user_assessment_id"));
							vviResponse.setInterviewQuestionId(jobectData.getString("interview_ question_id"));
							vviResponse.setAnswerFile(jobectData.getString("answer_file"));
							vviResponse.setAnswerUrl(jobectData.getString("answer_url"));
							vviResponse.setDuration(jobectData.getString("duration"));
							vviResponse.setMp4Link(jobectData.getString("mp4_link"));
							vviResponse.setWebmLink(jobectData.getString("webm_link"));
							vviResponseDAO.makePersistent(vviResponse);
						}
					}
				}
				
				///////////////////////////////////////////////////////////////////
				
				
				
				
				
				System.out.println(" InterViewInviteId : "+InterViewInviteId+" completeddate : "+completeddate+" player_url : "+player_url);
				
				player_url = player_url.replace("http", "https");

				System.out.println(">> Old player_url >> "+player_url);
				
				player_url = player_url.replace("video_interview_viewer.php", "embed_interview_viewer.php");
				
				System.out.println(">> New player_url >> "+player_url);
				
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				
				Date date = formatter.parse(completeddate);
				
			
				System.out.println(" ddddddddddate :: "+date);
				
				List<I4InterviewInvites> i4InterviewInvites = new ArrayList<I4InterviewInvites>();
				System.out.println("i4InterviewInvitesDAO:: "+i4InterviewInvitesDAO);
				i4InterviewInvites = i4InterviewInvitesDAO.findByInviteId(InterViewInviteId);
				
				
				//i4InterviewInvites.get(0).setVideoUrl(player_url); 
				i4InterviewInvites.get(0).setUserAssessmentId(userAssesmentId);
				i4InterviewInvites.get(0).setCompletedDateTime(date);
				i4InterviewInvites.get(0).setStatus(3);
				i4InterviewInvitesDAO.makePersistent(i4InterviewInvites.get(0));
				
				
				
				
				
				
				String errors = "";
				jsonResponse = new JSONObject();
				if(errors.equals(""))
					jsonResponse.put("status", true);
				else
					jsonResponse.put("status", false);
				jsonResponse.put("timeStamp", UtilityAPI.getCurrentTimeStamp());
				if(!errors.equals(""))
					jsonResponse.put("errorMessage", errors);
				else
					jsonResponse.put("errorMessage", "Data recieved.");


			} 
			catch (Exception e) {
				jsonResponse = UtilityAPI.writeJSONErrors(10017, "Server Error.", e);

			}
			returnVal = jsonResponse.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Response.status(200).entity(returnVal.toString()).build();
	}
	//Mukesh API for KES
	@POST
	@Produces(MediaType.TEXT_XML)
	@Consumes(MediaType.TEXT_XML)
	@Path("/postKESTalent")
	public Response postCandidateKellyDataToTM(InputStream incomingData,@Context HttpServletRequest request, @Context HttpServletResponse res) 
	{
		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		Document doc = null;
		Element root = null;	
		String returnVal="";
		try {			
			System.out.println("Enter in postCandidateKellyDataToTM XML");

			try {
				String payloadRequest = "";

				StringBuilder sb = new StringBuilder();
				try {
					BufferedReader in = new BufferedReader(new InputStreamReader(incomingData));
					String line = null;
					while ((line = in.readLine()) != null) {
						sb.append(line);
					}
				} catch (Exception e) {
					System.out.println("Error Parsing: - ");
				}
				// System.out.println("Data Received: " + crunchifyBuilder.toString());
				payloadRequest = sb.toString();
				System.err.println("payloadRequest::= "+payloadRequest);

				DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();

				try {
					//Using factory get an instance of document builder
					DocumentBuilder docBuilder = null;
					
					docBuilder = builderFactory.newDocumentBuilder();
					//parse using builder to get DOM representation of the XML file
					InputSource inputSource = new InputSource();
					inputSource.setCharacterStream(new StringReader(payloadRequest));
					doc = docBuilder.parse(inputSource);

					NodeList CandidateDetailsTM = doc.getElementsByTagName("Request");
					System.out.println(""+CandidateDetailsTM.getLength());
				    if (CandidateDetailsTM.getLength() > 0) {
				        //Element element = (Element)CandidateDetailsTM.item(0);
				    	String CandidateFirstName = null;
				        Element element = (Element) CandidateDetailsTM.item(0);
				        if(element.getElementsByTagName("FirstName").item(0) != null)
				        CandidateFirstName = element.getElementsByTagName("FirstName").item(0).getTextContent();
				        System.out.println("CandidateFirstName: "+CandidateFirstName);
				        
				        
				        
				        
				        //Writing response
				        doc = docBuilder.newDocument();
						root = doc.createElement("TeacherMatch");
						doc.appendChild(root);
						root.setAttribute("method", "AddUpdate");
						
				        Element eleStatus = doc.createElement("ResponseStatus");
						Text textStatus = doc.createTextNode("Success");
				        eleStatus.appendChild(textStatus);
				        
				        Element timeStamp = doc.createElement("MessageDescription");
						Text timeStampResult = doc.createTextNode("Data sent to teachermatch");
						timeStamp.appendChild(timeStampResult);
						
						
						Element eleWebResponse = doc.createElement("WebResponse");
						eleWebResponse.appendChild(eleStatus);
						eleWebResponse.appendChild(timeStamp);
						root.appendChild(eleWebResponse);
						
						TransformerFactory factory = TransformerFactory.newInstance();
						Transformer transformer = factory.newTransformer();
						
						StringWriter sw = new StringWriter();
						StreamResult result = new StreamResult(sw);
						DOMSource source = new DOMSource(doc);
						transformer.transform(source, result);
						returnVal = sw.toString();
						System.out.println("KKKKKKKKKKKKKKKKKKK:: "+returnVal);
				        
				    }else{
				    	
				    }
				}catch(ParserConfigurationException pce) {
					pce.printStackTrace();
				}catch(SAXException se) {
					se.printStackTrace();
				}catch(IOException ioe) {
					ioe.printStackTrace();
				}
			} 
			catch (Exception e) {
				e.printStackTrace();
				//UtilityAPI.writeXMLErrors(100011,"",null, doc, root);

			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return Response.status(200).entity(returnVal.toString()).build();
	}
	
	
	@POST
	@Produces(MediaType.TEXT_XML)
	@Consumes(MediaType.TEXT_XML)
	@Path("/addUpdateKESTalent")
	public Response postResponseCandidateKellyDataToTM(InputStream incomingData,@Context HttpServletRequest request, @Context HttpServletResponse res) 
	{
		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>postResponseCandidateKellyDataToTM>>>>>>>>>>>>>>>>>>>>>>>>");
		Document doc = null;
		Element root = null;	
		String returnVal="";
		boolean isValid=true;
		int statusCode =200;
				
		/**
	        * add to track node color history
	        */
	       StatusNodeColorHistory statusNodeColorHistory = null;
	       Map<String,MQEvent> mapNodeStatus= new HashMap<String, MQEvent>();
	       Map<String,Date> latestNodeColorDateMap = new HashMap<String, Date>();
	       /**
	        * end
	        */
		try {
			
			String successORFailure="";
	        String Msg="";
	        ServletContext context = request.getSession().getServletContext();
			ApplicationContext context0 = WebApplicationContextUtils.getWebApplicationContext(context);
			
			String payloadRequest = "";
			StringBuilder sb = new StringBuilder();
			TeacherDetail teacherDetail = null;
			BufferedReader in = new BufferedReader(new InputStreamReader(incomingData));
			String line = null;
			while ((line = in.readLine()) != null) {
				sb.append(line);
			}
			
			payloadRequest = sb.toString();
			System.err.println("Link To KSN payloadRequest::= "+payloadRequest);
			DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();

			DocumentBuilder docBuilder = null;
			docBuilder = builderFactory.newDocumentBuilder();
			InputSource inputSource = new InputSource();
			inputSource.setCharacterStream(new StringReader(payloadRequest));
			doc = docBuilder.parse(inputSource);
			NodeList CandidateDetailsTM = doc.getElementsByTagName("TeacherMatch");
			
		    if (CandidateDetailsTM != null && CandidateDetailsTM.getLength() > 0) {
				        Element element = (Element) CandidateDetailsTM.item(0);
				        String CandidateFirstName = null;
				        String CandidateLastName = null;
				        String CandidateEmailAddress = null;
				        String TalentID= null;
				        String KSNID = null;
				        String ManagedByBranch = null;
				        String JobCode = null;
				        String kellyTransactionID = null;
				        String status = null;
				        String message = null;
				        String authKey = null;
				        
				        if(element.getElementsByTagName("AuthKey").item(0) != null)
					    authKey = element.getElementsByTagName("AuthKey").item(0).getTextContent();
						System.out.println("AuthKey: "+authKey);
				        
				        if(element.getElementsByTagName("KellyTransactionID").item(0) != null)
					    kellyTransactionID = element.getElementsByTagName("KellyTransactionID").item(0).getTextContent();
					    System.out.println("KellyTransactionID: "+kellyTransactionID);
				        
				        
				        if(element.getElementsByTagName("FirstName").item(0) != null)
				        CandidateFirstName = element.getElementsByTagName("FirstName").item(0).getTextContent();
				        System.out.println("CandidateFirstName: "+CandidateFirstName);
				        
					    if(element.getElementsByTagName("LastName").item(0) != null)    
				        CandidateLastName = element.getElementsByTagName("LastName").item(0).getTextContent();
				        System.out.println("CandidateLastName: "+CandidateLastName);
	
				        if(element.getElementsByTagName("EmailID").item(0) != null) 
				        CandidateEmailAddress = element.getElementsByTagName("EmailID").item(0).getTextContent();
				        System.out.println("CandidateEmailAddress: "+CandidateEmailAddress);
				        
				        if(element.getElementsByTagName("TMTalentID").item(0) != null) 
				        TalentID = element.getElementsByTagName("TMTalentID").item(0).getTextContent();
				        System.out.println("TMTalentID: "+TalentID);
				        
				        if(element.getElementsByTagName("KSNTalentID").item(0) != null) 
				        KSNID = element.getElementsByTagName("KSNTalentID").item(0).getTextContent();
				        System.out.println("KSNTalentID: "+KSNID);
				        
				        if(element.getElementsByTagName("ManagedByBranch").item(0) != null) 
				        ManagedByBranch = element.getElementsByTagName("ManagedByBranch").item(0).getTextContent();
				        System.out.println("ManagedByBranch: "+ManagedByBranch);
				        
				        if(element.getElementsByTagName("JobCode").item(0) != null) 
				        JobCode = element.getElementsByTagName("JobCode").item(0).getTextContent();
				        System.out.println("JobCode: "+JobCode);
				        
				        if(element.getElementsByTagName("Status").item(0) != null) 
				        status = element.getElementsByTagName("Status").item(0).getTextContent();
					    System.out.println("Status: "+status);
				        
				        if(element.getElementsByTagName("Message").item(0) != null) 
				        message = element.getElementsByTagName("Message").item(0).getTextContent();
						System.out.println("Message: "+message);
					       
						
						if(authKey == null){
							isValid=false;
							Msg=Utility.getLocaleValuePropByKey("msgRequestAuthKey", locale);
						}else{
							if(authKey.isEmpty()){
								isValid=false;
								Msg=Utility.getLocaleValuePropByKey("msgRequestAuthKey", locale);
							}else if(!authKey.equalsIgnoreCase(WorkThreadServlet.authKey)){
								isValid=false;
								Msg=Utility.getLocaleValuePropByKey("msgRequestAuthKey", locale);
							}
						}
						
						if(isValid)
						if(kellyTransactionID == null){
				        	isValid=false;
				        	Msg=Utility.getLocaleValuePropByKey("msgValidKellyTrationID", locale);
				        }else{
				        	if(kellyTransactionID.isEmpty()){
				        		isValid=false;
				        		Msg=Utility.getLocaleValuePropByKey("msgValidKellyTrationID", locale);
					    	}
				        }
						
						 if(isValid)
						 if(TalentID!=null){
				        	 teacherDetailDAO = (TeacherDetailDAO)context0.getBean("teacherDetailDAO");
				        	 TeacherDetail teachers =teacherDetailDAO.findById(Utility.getIntValue(TalentID), false, false);
				        	 if( teachers== null){
				        		 	isValid=false;
						        	Msg=Utility.getLocaleValuePropByKey("msgInvalidTMTalentID", locale);
						     }else if(teachers!=null){
						    	 teacherDetail= teachers;
						     }else if(TalentID.isEmpty()){
					        		isValid=false;
						        	Msg=Utility.getLocaleValuePropByKey("msgInvalidTMTalentID", locale);
						    	}
				        }else {
				        		isValid=false;
					        	Msg=Utility.getLocaleValuePropByKey("msgInvalidTMTalentID", locale);
					    }
				        
						if(isValid) 
				        if(CandidateEmailAddress == null){
				        	isValid=false;
				        	Msg=Utility.getLocaleValuePropByKey("msgInvalidEmail", locale);
				        }else{
				        	if(CandidateEmailAddress.isEmpty()){
				        	isValid=false;
				        	Msg=Utility.getLocaleValuePropByKey("msgInvalidEmail", locale);
				        	}
				        	
				        	if(! Utility.validEmailForTeacher(CandidateEmailAddress)){
					        	isValid=false;
					        	Msg=Utility.getLocaleValuePropByKey("msgInvalidEmail", locale);
					        }
				        	
				        }
				        
				        if(status!=null && status.equalsIgnoreCase("Success")){
					       
				        	if(isValid)
				        	if(KSNID == null){
					        	isValid=false;
					        	Msg=Utility.getLocaleValuePropByKey("msgKSNTalentID", locale);
					        }else{
					        	if(KSNID.isEmpty()){
					        		isValid=false;
					        		Msg=Utility.getLocaleValuePropByKey("msgKSNTalentID", locale);
						        }else{
						        	if(KSNID.length() >10){
						        		isValid=false;
						        		Msg=Utility.getLocaleValuePropByKey("msgKSNTalentID", locale);
							    	}else{
							        	List<TeacherDetail> teachersKSNID =teacherDetailDAO.findByKSN_OP(BigInteger.valueOf(Long.valueOf((KSNID))));
							        	if(teachersKSNID != null && teachersKSNID.size() >0){
							        		isValid=false;
							        		Msg=Utility.getLocaleValuePropByKey("msgKSNIDExist", locale);
							        	}
						        	}
						        }
					            
					        	
				              }
				        
				        	if(isValid)
					        if(CandidateFirstName == null){
					        	isValid=false;
					        	Msg=Utility.getLocaleValuePropByKey("msgValidFirstName", locale);
					        }else{
					        	if(CandidateFirstName.isEmpty()){
					        		isValid=false;
					        		Msg=Utility.getLocaleValuePropByKey("msgValidFirstName", locale);
						    	}
					        }
					        
				        	if(isValid)
					        if(CandidateLastName == null){
					        	isValid=false;
					        	Msg=Utility.getLocaleValuePropByKey("msgValidLastName", locale);
					        }else{
					        	if(CandidateLastName.isEmpty()){
					        		isValid=false;
					        		Msg=Utility.getLocaleValuePropByKey("msgValidLastName", locale);
						    	}
					        }
					        
					       /* if(JobCode == null){
					        	isValid=false;
					        	Msg="JobCode is Required";
					        }else{
					        	if(JobCode.isEmpty()){
					        		isValid=false;
						        	Msg="JobCode is Required";
						    	}
					        }*/
				        	if(isValid)
					        if(ManagedByBranch == null){
					        	isValid=false;
					        	Msg=Utility.getLocaleValuePropByKey("msgValidManagedByBranch", locale);
					        	//errorCodeVal="10001";
					        }else{
					        	if(ManagedByBranch.isEmpty()){
					        		isValid=false;
					        		Msg=Utility.getLocaleValuePropByKey("msgValidManagedByBranch", locale);
						        	//errorCodeVal="10001";
					        	}
					        }
				           if(isValid){ 
						       talentKSNDetailDAO = (TalentKSNDetailDAO)context0.getBean("talentKSNDetailDAO"); 
						       mqEventDAO = (MQEventDAO)context0.getBean("mqEventDAO"); 
						       mqEventHistoryDAO = (MQEventHistoryDAO)context0.getBean("mqEventHistoryDAO");
						       statusNodeColorHistoryDAO = (StatusNodeColorHistoryDAO)context0.getBean("statusNodeColorHistoryDAO");
						       /**
						        * add to track node color history
						        */
						       
						       List<StatusNodeColorHistory> listStatusClr = new ArrayList<StatusNodeColorHistory>();
						       listStatusClr = statusNodeColorHistoryDAO.getStatusColorHistoryByTeacher_OP(teacherDetail);
						       System.out.println(" listStatusClr :: "+listStatusClr.size());
						       if(listStatusClr.size()>0){
						    	   for(StatusNodeColorHistory s :listStatusClr)
						    		   if(s.getMqEvent()!=null)
						    		   latestNodeColorDateMap.put(s.getTeacherDetail().getTeacherId()+"##"+s.getMqEvent().getEventType(), s.getUpdateDateTime());
						       }
						       
						      List<MQEvent> mqEventsList=new ArrayList<MQEvent>();
								 mqEventsList=mqEventDAO.findMQEventByTeacher_Op(teacherDetail);
								 System.out.println(" mqEventsList :: "+mqEventsList.size());
								 if(mqEventsList.size()>0){
									 for (MQEvent mqEv : mqEventsList) {
										 mapNodeStatus.put(mqEv.getTeacherdetail()+"-"+mqEv.getEventType() , mqEv);
									 }
								 }
						      
					           List<MQEvent> mqEvent =  mqEventDAO.findMQEventByTeacher(teacherDetail); 
					           MQEvent tempMqEvent = null;
					           if(mqEvent != null && mqEvent.size() >0){
					        	   mqEvent.get(0).setStatus("C");
					        	   mqEvent.get(0).setKsnTransId(kellyTransactionID);
					        	   mqEvent.get(0).setUpdateDateTime(new Date());
					        	   tempMqEvent= mqEvent.get(0);
					        	   
					        	   
					        	   mqEventDAO.makePersistent(mqEvent.get(0));
					        	   try{
						        	   MQEventHistory mqEventHistory = new MQEventHistory();
						        	   mqEventHistory.setMqEvent(mqEvent.get(0));
						        	   mqEventHistory.setCreatedBy(1);
						        	   mqEventHistory.setCreatedDateTime(new Date());
						        	   mqEventHistory.setIpAddress(request.getRemoteAddr());
						        	   mqEventHistory.setXmlObject(payloadRequest);
						        	   mqEventHistoryDAO.makePersistent(mqEventHistory);
					        	   }catch(Exception e){
					        		   e.printStackTrace();
					        	   }
					        	   
					        	   if(teacherDetail!=null){
						        		teacherDetail.setKSNID(BigInteger.valueOf(Long.valueOf((KSNID))));
						        		teacherDetailDAO.makePersistent(teacherDetail);
						        	}
					        	   HeadQuarterMaster headQuarterMaster=new HeadQuarterMaster();
					 		       headQuarterMaster.setHeadQuarterId(1);
					 		       jobForTeacherDAO = (JobForTeacherDAO)context0.getBean("jobForTeacherDAO"); 
					        	   List<JobForTeacher> jobForTeacherList = jobForTeacherDAO.findByTeacherIdHQAndBranch_OP(teacherDetail,headQuarterMaster, null);
					        	   System.out.println(" jobForTeacherList :: "+jobForTeacherList.size());
					        	   try{
										if(jobForTeacherList.size()>0){
											 userMasterDAO = (UserMasterDAO)context0.getBean("userMasterDAO");
											 UserMaster userMaster = null;
											 Integer uId = 0;
											 if(mqEvent.get(0).getCreatedBy()!=null)
												 uId = mqEvent.get(0).getCreatedBy();
											 else if(jobForTeacherList.get(0).getJobId().getCreatedBy()!=null)
												 uId = jobForTeacherList.get(0).getJobId().getCreatedBy();
											 
											 userMaster=userMasterDAO.findById(uId,false,false);
											 
											 for (JobForTeacher jobForTeacher : jobForTeacherList) {
												 
											 	try {
													mqService.statusUpdate(jobForTeacher,null,mqEvent.get(0).getSecondaryStatus(),userMaster,context0,null,true);
												} catch (Exception e) {
													e.printStackTrace();
												}	
											 }
											 
											 try {
												 statusNodeColorHistory = candidateGridService.saveStatusNodeColor(jobForTeacherList.get(0),mqEvent.get(0),mapNodeStatus,latestNodeColorDateMap);
											 } catch (Exception e) {
												 e.printStackTrace();
											 }
											 
												 mqEventsList=mqEventDAO.findMQEventByTeacher_Op(teacherDetail);
												 if(mqEventsList.size()>0){
													 for (MQEvent mqEv : mqEventsList) {
														 mapNodeStatus.put(mqEv.getTeacherdetail()+"-"+mqEv.getEventType() , mqEv);
													 }
												 }
											 String newColor = candidateGridService.getStatusNodeColor(mqEvent.get(0).getEventType(),teacherDetail,mapNodeStatus);
											   System.out.println(" newColor :: "+newColor);
											   statusNodeColorHistory.setCurrentColor(newColor);
											   statusNodeColorHistory.setUpdateDateTime(new Date());
											   statusNodeColorHistoryDAO.updatePersistent(statusNodeColorHistory);
										}
								   }catch(Exception e){
									   e.printStackTrace();
								   }
					           }else{
					        	   	System.out.println(":::::Invalid Request:::::");
					        	    isValid=false;
						        	Msg=Utility.getLocaleValuePropByKey("msgInvalidRequest", locale);
					           }
				           
				       }    
				             
				    }else{
				    	
				    	//Sandeep updated notes table by message fail cases..............................here
				    	   mqEventDAO = (MQEventDAO)context0.getBean("mqEventDAO"); 
					       mqEventHistoryDAO = (MQEventHistoryDAO)context0.getBean("mqEventHistoryDAO"); 
						   List<MQEvent> mqEvent = mqEventDAO.findMQEventByTeacher(teacherDetail); 
						   
						   if(mqEvent != null && mqEvent.size() >0){
							   try{
								   jobForTeacherDAO = (JobForTeacherDAO)context0.getBean("jobForTeacherDAO");
			        			   secondaryStatusDAO = (SecondaryStatusDAO)context0.getBean("secondaryStatusDAO"); 
			        			   userMasterDAO = (UserMasterDAO)context0.getBean("userMasterDAO");
			        			   statusNodeColorHistoryDAO = (StatusNodeColorHistoryDAO)context0.getBean("statusNodeColorHistoryDAO");
				        		   HeadQuarterMaster headQuarterMaster=new HeadQuarterMaster();
					 		       headQuarterMaster.setHeadQuarterId(1);
					        	   List<JobForTeacher> jobForTeacherList = jobForTeacherDAO.findByTeacherIdHQAndBranch_OP(mqEvent.get(0).getTeacherdetail(),headQuarterMaster, null);
					        	   
				        		   UserMaster userMaster=userMasterDAO.findById(mqEvent.get(0).getCreatedBy(),false,false);
					        	   if(isValid){
					        		   /************Start Node Green*************/
					        		   try{
					        				if(jobForTeacherList.size()>0){
						        				mqService.statusUpdate(jobForTeacherList.get(0),null,mqEvent.get(0).getSecondaryStatus(),userMaster,context0,message,true);
						        			}
					        		   }catch(Exception e){
					        			   e.printStackTrace();
					        		   }
					        	   }else{
					        		   mqService.statusUpdate(jobForTeacherList.get(0),null,mqEvent.get(0).getSecondaryStatus(),userMaster,context0,message,false);
					        	   }
					        	   
					        	   try{
					        		   List<MQEvent> mqEventForRedStatus = mqEventDAO.findEventByEventType(teacherDetail, Utility.getLocaleValuePropByKey("lblLinkToKsn", locale));
					        		   if(mqEventForRedStatus!=null && mqEventForRedStatus.size()>0){
					        			   MQEvent mq = mqEventForRedStatus.get(0);
					        			   if(mq!=null){
					        				   String color = "Grey";
					        				   color = candidateGridService.getNodeColorWithMqEvent(mq, null);
					        				   mq.setAckStatus("Failed");
						        			   mq.setStatus("R");
						        			   mqEventDAO.makePersistent(mq);
						        			   try{
						        				   statusNodeColorHistory = null;
						        				   statusNodeColorHistory = new StatusNodeColorHistory();
						        				   statusNodeColorHistory.setHeadQuarterMaster(headQuarterMaster);
						        				   statusNodeColorHistory.setCreatedDateTime(new Date());
						        				   statusNodeColorHistory.setTeacherDetail(mq.getTeacherdetail());
						        				   statusNodeColorHistory.setPrevColor(color);  
						        				   statusNodeColorHistory.setMqEvent(mq);
						        				   statusNodeColorHistory.setCurrentColor("Red");
						        				   statusNodeColorHistory.setUserMaster(userMaster);
						        				   statusNodeColorHistoryDAO.makePersistent(statusNodeColorHistory);
						        			  }catch(Exception e){
						        				  e.printStackTrace();
						        			  } 
						        		   }
					        		   }
					        	   }catch(Exception e){
					        		   e.printStackTrace();
					        	   }
					        	   
							   }catch(Exception e){
								   e.printStackTrace();
							   }
						   } 	   
				    }
				        if(isValid){
				        	Msg=Utility.getLocaleValuePropByKey("msgDataSentToTeacherMatch", locale);
			        	    successORFailure="Success";
				        }else{
				        	successORFailure="Failure";
				        }
				     	
		    	      			        
				        doc = docBuilder.newDocument();
						root = doc.createElement("TeacherMatch");
						doc.appendChild(root);
						root.setAttribute("method", "AddUpdate");
						
				        Element eleStatus = doc.createElement("ResponseStatus");
						Text textStatus = doc.createTextNode(successORFailure);//"Success"
				        eleStatus.appendChild(textStatus);
				        
				        Element timeStamp = doc.createElement("MessageDescription");
						Text timeStampResult = doc.createTextNode(Msg);//"Data sent to Kelly"
						timeStamp.appendChild(timeStampResult);
						
						
						Element eleWebResponse = doc.createElement("WebResponse");
						eleWebResponse.appendChild(eleStatus);
						//if(!isValid)
						 // eleWebResponse.appendChild(eleErrorCode);
						eleWebResponse.appendChild(timeStamp);
						root.appendChild(eleWebResponse);
						
						TransformerFactory factory = TransformerFactory.newInstance();
						Transformer transformer = factory.newTransformer();
						
						StringWriter sw = new StringWriter();
						StreamResult result = new StreamResult(sw);
						DOMSource source = new DOMSource(doc);
						transformer.transform(source, result);
						returnVal = sw.toString();
						System.out.println("ReturnValue Link To KSN :: "+returnVal);
				        
				     			    
//						if(isValid){
				        	statusCode = 200;
//				        }else{
//				        	statusCode= 400;
//				        }
				      	        
				        
				    }else{
				    	statusCode= 200;
				    	Msg=Utility.getLocaleValuePropByKey("msgInvalidAction", locale);
				    	successORFailure="Failure";
				    	doc = docBuilder.newDocument();
						root = doc.createElement("TeacherMatch");
						doc.appendChild(root);
						root.setAttribute("method", "AddUpdate");
						
				        Element eleStatus = doc.createElement("ResponseStatus");
						Text textStatus = doc.createTextNode(successORFailure);//"Success"
				        eleStatus.appendChild(textStatus);
				         Element timeStamp = doc.createElement("MessageDescription");
						Text timeStampResult = doc.createTextNode(Msg);//"Data sent to Kelly"
						timeStamp.appendChild(timeStampResult);
						
						
						Element eleWebResponse = doc.createElement("WebResponse");
						eleWebResponse.appendChild(eleStatus);
						eleWebResponse.appendChild(timeStamp);
						root.appendChild(eleWebResponse);
						
						TransformerFactory factory = TransformerFactory.newInstance();
						Transformer transformer = factory.newTransformer();
						
						StringWriter sw = new StringWriter();
						StreamResult result = new StreamResult(sw);
						DOMSource source = new DOMSource(doc);
						transformer.transform(source, result);
						returnVal = sw.toString();
						System.out.println("Failed ReturnValue Link To KSN :: "+returnVal);
				    	
				    	
				    }
					
				} 
				catch (Exception e) {
					e.printStackTrace();
					statusCode = 500;
					returnVal=Utility.getLocaleValuePropByKey("msgHTTPCode500", locale);
			}								
			return Response.status(statusCode).entity(returnVal.toString()).build();
		}





//sandeep API for KES
@POST
@Produces(MediaType.TEXT_XML)
@Consumes(MediaType.TEXT_XML)
@Path("/requestEReg")
public Response postKellyrEquestERegToTM(InputStream incomingData,@Context HttpServletRequest request, @Context HttpServletResponse res) 
{
	System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
	Document doc = null;
	Element root = null;	
	String returnVal="";
	try {			
		System.out.println("Enter in postKellyrEquestERegToTM XML");

		try {
			String payloadRequest = "";

			StringBuilder sb = new StringBuilder();
			try {
				BufferedReader in = new BufferedReader(new InputStreamReader(incomingData));
				String line = null;
				while ((line = in.readLine()) != null) {
					sb.append(line);
				}
			} catch (Exception e) {
				System.out.println("Error Parsing: - ");
			}
			// System.out.println("Data Received: " + crunchifyBuilder.toString());
			payloadRequest = sb.toString();
			System.err.println("payloadRequest::= "+payloadRequest);

			DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();

			try {
				//Using factory get an instance of document builder
				DocumentBuilder docBuilder = null;
				
				docBuilder = builderFactory.newDocumentBuilder();
				//parse using builder to get DOM representation of the XML file
				InputSource inputSource = new InputSource();
				inputSource.setCharacterStream(new StringReader(payloadRequest));
				doc = docBuilder.parse(inputSource);

				NodeList CandidateDetailsTM = doc.getElementsByTagName("Request");
				System.out.println(""+CandidateDetailsTM.getLength());
			    if (CandidateDetailsTM.getLength() > 0) {
			        //Element element = (Element)CandidateDetailsTM.item(0);
			        Element element = (Element) CandidateDetailsTM.item(0);
			        
			       /* String CandidateFirstName = element.getElementsByTagName("FirstName").item(0).getTextContent();
			        System.out.println("CandidateFirstName: "+CandidateFirstName);
			        
			        String CandidateLastName = element.getElementsByTagName("LastName").item(0).getTextContent();
			        System.out.println("CandidateLastName: "+CandidateLastName);
			        
			        String CandidateEmailAddress = element.getElementsByTagName("EmailID").item(0).getTextContent();
			        System.out.println("CandidateEmailAddress: "+CandidateEmailAddress);
			        
			        
			        String KSNID = element.getElementsByTagName("KSNID").item(0).getTextContent();
			        System.out.println("KSNID: "+KSNID);*/
			        
			        
			        //Writing response
			        doc = docBuilder.newDocument();
					root = doc.createElement("TeacherMatch");
					doc.appendChild(root);
					root.setAttribute("method", "requestEReg");
					
			        Element eleStatus = doc.createElement("Status");
					Text textStatus = doc.createTextNode("success");
			        eleStatus.appendChild(textStatus);
			        
			        Element timeStamp = doc.createElement("Message");
					Text timeStampResult = doc.createTextNode("Data sent to Kelly");
					timeStamp.appendChild(timeStampResult);
					
					
					Element eleWebResponse = doc.createElement("Request");
					eleWebResponse.appendChild(eleStatus);
					eleWebResponse.appendChild(timeStamp);
					root.appendChild(eleWebResponse);
					
					TransformerFactory factory = TransformerFactory.newInstance();
					Transformer transformer = factory.newTransformer();
					
					StringWriter sw = new StringWriter();
					StreamResult result = new StreamResult(sw);
					DOMSource source = new DOMSource(doc);
					transformer.transform(source, result);
					returnVal = sw.toString();
					System.out.println("ReturnVal:: "+returnVal);
			        
			    }else{
			    	
			    }
			}catch(ParserConfigurationException pce) {
				pce.printStackTrace();
			}catch(SAXException se) {
				se.printStackTrace();
			}catch(IOException ioe) {
				ioe.printStackTrace();
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
			//UtilityAPI.writeXMLErrors(100011,"",null, doc, root);

		}
		
	} catch (Exception e) {
		e.printStackTrace();
	}
	return Response.status(200).entity(returnVal.toString()).build();
}






public boolean autoCallQueue(StatusMaster statusMaster,SecondaryStatus secondaryStatus,JobForTeacher jobForTeacher,UserMaster userMaster , ApplicationContext APPLICATIONCONTEXT ){
	  
	  
     boolean queueFlag=false; 
     System.out.println("Actime MQ Process Start with jobforteacherid -autoCallQueue::  "+ jobForTeacher.getJobForTeacherId());
     List<MQEvent> mqEventsList = new ArrayList<MQEvent>();
     StatusNodeColorHistory statusNodeColorHistory = null;
     Map<String,MQEvent> mapNodeStatus= new HashMap<String, MQEvent>();
     Map<String,Date> latestNodeColorDateMap = new HashMap<String, Date>();
 try{
  
     mqEventDAO = (MQEventDAO)APPLICATIONCONTEXT.getBean("mqEventDAO");
     mqEventHistoryDAO = (MQEventHistoryDAO)APPLICATIONCONTEXT.getBean("mqEventHistoryDAO");
  
     Map<Object, Object> mapData = new HashMap<Object, Object>();
     mapData.put("jobForTeacherId", jobForTeacher.getJobForTeacherId());
     mapData.put("secondaryStatusId",secondaryStatus.getSecondaryStatusId());
     mapData.put("userId",userMaster.getUserId());
     String TMTransactionID="TM-"+(Utility.randomString(8)+Utility.getDateTime());
     mapData.put("TMTransactionID", TMTransactionID);
    
     MQEvent mqEvent = null;
     List<MQEvent> mqEventList = new ArrayList<MQEvent>();
     /*List<MQEvent> mqEventList = mqEventDAO.findMQEventByTeacher(jobForTeacher.getTeacherId());*/ 
     if(secondaryStatus!=null)
  	   mqEventList = mqEventDAO.findEventByEventType(jobForTeacher.getTeacherId(),secondaryStatus.getSecondaryStatusName());
     else
  	   mqEventList = mqEventDAO.findLinkToKSNByTeacher(jobForTeacher.getTeacherId());
     
     List<StatusNodeColorHistory> listStatusClr = new ArrayList<StatusNodeColorHistory>();
     listStatusClr = statusNodeColorHistoryDAO.getStatusColorHistoryByTeacher(jobForTeacher.getTeacherId());
     if(listStatusClr.size()>0){
  	   for(StatusNodeColorHistory s :listStatusClr)
  		 if(s.getMqEvent()!=null)
  		   latestNodeColorDateMap.put(s.getTeacherDetail().getTeacherId()+"##"+s.getMqEvent().getEventType(), s.getUpdateDateTime());
     }
     
     if(mqEventList != null && mqEventList.size() >0){
    	 
      mqEvent = mqEventList.get(0);
      mapData.put("TMTransactionID", mqEvent.getTmTransId());
      
      mqEventsList=mqEventDAO.findMQEventByTeacher(mqEvent.getTeacherdetail());
		 if(mqEventsList.size()>0){
			 for (MQEvent mqEv : mqEventsList) {
				 mapNodeStatus.put(mqEv.getTeacherdetail()+"-"+mqEv.getEventType() , mqEv);
			 }
		 }
		 
	    try {
			 statusNodeColorHistory = candidateGridService.saveStatusNodeColor(jobForTeacher,mqEvent,mapNodeStatus,latestNodeColorDateMap);
		 } catch (Exception e) {
			 e.printStackTrace();
		 }
		 
     }else{
    	 
	      mqEvent = new MQEvent();
	      mqEvent.setTeacherdetail(jobForTeacher.getTeacherId());
	      if(secondaryStatus==null){
	       mqEvent.setStatusMaster(statusMaster);
	       mqEvent.setEventType(statusMaster.getStatus());
	      }else{
	       mqEvent.setSecondaryStatus(secondaryStatus);
	       mqEvent.setEventType(secondaryStatus.getSecondaryStatusName());
	      }
	     
	      mqEvent.setTmTransId(TMTransactionID);
	      mqEvent.setStatus("R");
	      mqEvent.setCreatedBy(userMaster.getUserId());
	      mqEvent.setCreatedDateTime(new Date());
	      mqEventDAO.makePersistent(mqEvent);
	      
 	    try {
				 statusNodeColorHistory = candidateGridService.saveStatusNodeColor(jobForTeacher,mqEvent,new HashMap<String, MQEvent>(),latestNodeColorDateMap);
			 } catch (Exception e) {
				 e.printStackTrace();
			 }
			 
      }
     if(mqEvent != null)
	     mapData.put("MQEventID", mqEvent.getEventId());
	      
	     MQEventHistory mqhEventHistory = new MQEventHistory();
	     mqhEventHistory.setMqEvent(mqEvent);
	     mqhEventHistory.setCreatedBy(userMaster.getUserId());
	     mqhEventHistory.setCreatedDateTime(new Date());
	     mqEventHistoryDAO.makePersistent(mqhEventHistory);
     
	     if(mqhEventHistory != null)
	     mapData.put("MQEventHistoryID", mqhEventHistory.getEventHistoryId());
	     
	     if(statusNodeColorHistory!=null)
	     mapData.put("statusNodeColorHistoryId", statusNodeColorHistory.getStatusNodeColorHistoryId());
	     
	     JmsTemplate jmsTemplate =  (JmsTemplate) APPLICATIONCONTEXT.getBean("JMSTemplate");
	     String activeMQ=null;
	     if(secondaryStatus!=null && secondaryStatus.getSecondaryStatusName().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblLinkToKsn", locale))){
			   activeMQ="ActiveMQDestination";
			   mapData.put("WorkFlowID",null);
		   }else if(secondaryStatus!=null && secondaryStatus.getSecondaryStatusName().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINVWF1", locale))){
			   activeMQ="ERegistration";
			   mapData.put("WorkFlowID","INITIALAPP");
		   }else if(secondaryStatus!=null && secondaryStatus.getSecondaryStatusName().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINVWF2", locale))){
			   activeMQ="ERegistration";
			   mapData.put("WorkFlowID","CONDOFF");
		   }else if(secondaryStatus!=null && secondaryStatus.getSecondaryStatusName().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINVWF3", locale))){
			   activeMQ="ERegistration";
			   mapData.put("WorkFlowID","CONDFORMS");
		 }
	     ActiveMQQueue eventSend =  (ActiveMQQueue)  APPLICATIONCONTEXT.getBean(activeMQ);
	     System.out.println("-----------Before sending active mq of "+activeMQ+"-------------");
	     jmsTemplate.convertAndSend(eventSend,mapData);
	     System.out.println("-----------After sending active mq  "+activeMQ+"-------------");
	     
	    } catch (Exception e) {
	    e.printStackTrace();
	   }
       return queueFlag;
}



	//shadab
	
	@GET
	@Consumes(MediaType.TEXT_HTML)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("getDistrictMasterList/{districtName}")
	public String getDistrictMasterList(@PathParam("districtName") String districtName,@Context HttpServletRequest request)
	{
		try {
			//System.out.println("districtname=="+URLDecoder.decode(districtName,"UTF-8"));
			//System.out.println("districtname=="+districtName);
			districtName=URLDecoder.decode(districtName,"UTF-8");
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		DWRAutoComplete dwrAutoComplete=new DWRAutoComplete();
		ServletContext context = request.getSession().getServletContext();
		ApplicationContext context0 = WebApplicationContextUtils.getWebApplicationContext(context);
		districtMasterDAO = (DistrictMasterDAO)context0.getBean("districtMasterDAO");
		//List<DistrictMaster> list=dwrAutoComplete.getDistrictMasterList(districtName,districtMasterDAO);
		//new start
		List<DistrictMaster> districtMasterList =  new ArrayList<DistrictMaster>();
		List<DistrictMaster> fieldOfDistrictList1 = null;
		List<DistrictMaster> fieldOfDistrictList2 = null;
		try{
			
			Criterion criterion = Restrictions.like("districtName", districtName.trim(),MatchMode.START);
			Criterion criterion1 = Restrictions.eq("status", "A");
			if(districtName.trim()!=null && districtName.trim().length()>0){
				//System.out.println(districtMasterDAO);
				fieldOfDistrictList1 = districtMasterDAO.findWithLimit(Order.asc("districtName"), 0, 25, criterion,criterion1);
				
				Criterion criterion2 = Restrictions.ilike("districtName","% "+districtName.trim()+"%" );
				fieldOfDistrictList2 = districtMasterDAO.findWithLimit(Order.asc("districtName"), 0, 25, criterion2,criterion1);
				
				districtMasterList.addAll(fieldOfDistrictList1);
				districtMasterList.addAll(fieldOfDistrictList2);
				Set<DistrictMaster> setDistrict = new LinkedHashSet<DistrictMaster>(districtMasterList);
				districtMasterList = new ArrayList<DistrictMaster>(new LinkedHashSet<DistrictMaster>(setDistrict));
								
			}else{
				fieldOfDistrictList1 = districtMasterDAO.findWithLimit(Order.asc("districtName"), 0, 25,criterion1);
				districtMasterList.addAll(fieldOfDistrictList1);
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		//return districtMasterList;
	
		
		
		
		//new end
		
		
		//System.out.println(districtMasterList.size());
		return JSONUtility.districtListToJson(districtMasterList).toString();
		/*DistrictMaster districtMaster=new DistrictMaster();
		districtMaster.setDistrictId(1);
		districtMaster.setDistrictName("test");
		Gson gson=new Gson();
		return gson.toJson(list,DistrictMaster.class);*/
		
	}
	
	@GET
	@Consumes(MediaType.TEXT_HTML)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("getSchoolMasterList/{districtId}/{schoolName}")
	public String getSchoolMasterList(@PathParam("districtId") String districtId,@PathParam("schoolName") String schoolName,@Context HttpServletRequest request)
	{
		try {
			//System.out.println("schoolName=="+URLDecoder.decode(schoolName,"UTF-8"));
			//System.out.println("schoolName=="+schoolName);
			schoolName=URLDecoder.decode(schoolName,"UTF-8");
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		DWRAutoComplete dwrAutoComplete=new DWRAutoComplete();
		ServletContext context = request.getSession().getServletContext();
		ApplicationContext context0 = WebApplicationContextUtils.getWebApplicationContext(context);
		districtMasterDAO = (DistrictMasterDAO)context0.getBean("districtMasterDAO");
		districtSchoolsDAO=(DistrictSchoolsDAO)context0.getBean("districtSchoolsDAO");
		//List<DistrictMaster> list=dwrAutoComplete.getDistrictMasterList(districtName,districtMasterDAO);
		//new start
		List<DistrictSchools> schoolMasterList =  new ArrayList<DistrictSchools>();
		List<DistrictSchools> fieldOfSchoolList1 = null;
		List<DistrictSchools> fieldOfSchoolList2 = null;
		try 
		{
			Criterion criterion = Restrictions.like("schoolName", schoolName,MatchMode.START);
			
			// Get School List, this code using when districtId.equals("0") || districtId.equals("") in JobsBoard {Not Tested}
			if(districtId.equals("0") || districtId.equals("")){
				if(schoolName.trim()!=null && schoolName.trim().length()>0){
					fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(null, 0, 10, criterion);
					Collections.sort(fieldOfSchoolList1,DistrictSchools.compSchoolMaster);
					Criterion criterion2 = Restrictions.ilike("schoolName","% "+schoolName.trim()+"%" );
					fieldOfSchoolList2 = districtSchoolsDAO.findWithLimit(null, 0, 10, criterion2);
					Collections.sort(fieldOfSchoolList2,DistrictSchools.compSchoolMaster);
					schoolMasterList.addAll(fieldOfSchoolList1);
					schoolMasterList.addAll(fieldOfSchoolList2);
					Set<DistrictSchools> setSchool = new LinkedHashSet<DistrictSchools>(schoolMasterList);
					schoolMasterList = new ArrayList<DistrictSchools>(new LinkedHashSet<DistrictSchools>(setSchool));
				}
				else{
					fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(null, 0, 10);
					Collections.sort(fieldOfSchoolList1,DistrictSchools.compSchoolMaster);
					schoolMasterList.addAll(fieldOfSchoolList1);
				}
			}
			// Get School List By District ID
			else{
				
				//System.out.println(" inside DWRAutoComplete getSchoolMasterList ELSE  -----::-----> ");
				
				DistrictMaster districtMaster = districtMasterDAO.findById(new Integer(districtId), false, false);
				Criterion criterion2 = Restrictions.eq("districtMaster",districtMaster);
				
				if(schoolName.trim()!=null && schoolName.trim().length()>0){
					fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(Order.asc("schoolName"), 0, 25, criterion,criterion2);
					Criterion criterion3 = Restrictions.ilike("schoolName","% "+schoolName.trim()+"%" );
					fieldOfSchoolList2 = districtSchoolsDAO.findWithLimit(Order.asc("schoolName"), 0, 25,criterion3,criterion2);
					
					schoolMasterList.addAll(fieldOfSchoolList1);
					schoolMasterList.addAll(fieldOfSchoolList2);
					Set<DistrictSchools> setSchool = new LinkedHashSet<DistrictSchools>(schoolMasterList);
					schoolMasterList = new ArrayList<DistrictSchools>(new LinkedHashSet<DistrictSchools>(setSchool));
					
				}else{
					fieldOfSchoolList1 = districtSchoolsDAO.findWithLimit(Order.asc("schoolName"), 0, 25,criterion2);
					schoolMasterList.addAll(fieldOfSchoolList1);
				}
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		//return schoolMasterList;
	
		
		
		
		//new end
		
		
		//System.out.println(schoolMasterList.size());
		return JSONUtility.schoolListToJson(schoolMasterList).toString();
		/*DistrictMaster districtMaster=new DistrictMaster();
		districtMaster.setDistrictId(1);
		districtMaster.setDistrictName("test");
		Gson gson=new Gson();
		return gson.toJson(list,DistrictMaster.class);*/
		
	}
	@GET
	@Consumes(MediaType.TEXT_HTML)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("getAllCertificateTypeList/{certType}")
	public String getAllCertificateTypeList(@PathParam("certType") String certType,@Context HttpServletRequest request)
	{
		try {
			//System.out.println("schoolName=="+URLDecoder.decode(certType,"UTF-8"));
			//System.out.println("schoolName=="+certType);
			certType=URLDecoder.decode(certType,"UTF-8");
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		DWRAutoComplete dwrAutoComplete=new DWRAutoComplete();
		ServletContext context = request.getSession().getServletContext();
		ApplicationContext context0 = WebApplicationContextUtils.getWebApplicationContext(context);
		certificateTypeMasterDAO=(CertificateTypeMasterDAO)context0.getBean("certificateTypeMasterDAO");
		List<CertificateTypeMaster> certList =  new ArrayList<CertificateTypeMaster>();
		List<CertificateTypeMaster> certList1 = null;
		List<CertificateTypeMaster> certList2 = null;
		try 
		{
			Criterion criterionMode1 = Restrictions.like("certType", certType,MatchMode.START);
			Criterion statusCriteria = Restrictions.eq("status", "A");
			certList1 = certificateTypeMasterDAO.findWithLimit(Order.asc("certType"), 0, 10, criterionMode1,statusCriteria);
			
			Criterion criterionLike1 = Restrictions.ilike("certType","% "+certType.trim()+"%" );
			certList2 = certificateTypeMasterDAO.findWithLimit(Order.asc("certType"), 0, 10, criterionLike1,statusCriteria);
			
			certList.addAll(certList1);
			certList.addAll(certList2);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}		
		//return certList;
	
		
		
		
		//new end
		
		
		//System.out.println(certList.size());
		return JSONUtility.certListToJson(certList).toString();
		/*DistrictMaster districtMaster=new DistrictMaster();
		districtMaster.setDistrictId(1);
		districtMaster.setDistrictName("test");
		Gson gson=new Gson();
		return gson.toJson(list,DistrictMaster.class);*/
		
	}
	
	@GET
	@Consumes(MediaType.TEXT_HTML)
	@Produces(MediaType.TEXT_PLAIN)
	@Path("getSubjectList/{districtId}")
	public String getSubjectList(@PathParam("districtId") String districtId,@Context HttpServletRequest request)
	{
		
		DWRAutoComplete dwrAutoComplete=new DWRAutoComplete();
		ServletContext context = request.getSession().getServletContext();
		ApplicationContext context0 = WebApplicationContextUtils.getWebApplicationContext(context);
		subjectMasterDAO=(SubjectMasterDAO)context0.getBean("subjectMasterDAO");
		districtMasterDAO=(DistrictMasterDAO)context0.getBean("districtMasterDAO");

		StringBuffer sb = new StringBuffer();
		DistrictMaster districtMaster = null;
		if(!districtId.equalsIgnoreCase("0"))
		{
			districtMaster = districtMasterDAO.findByDistrictId(districtId);
		}
		List<SubjectMaster> subjectMasters = new ArrayList<SubjectMaster>();
		//System.out.println(" districtId :: "+districtId);
		if(districtMaster!=null)
		{
			//System.out.println(" District Name :: "+districtMaster.getDistrictName()+"  &  District Id ::  " +districtMaster.getDistrictId());
			Criterion criterionD = Restrictions.eq("districtMaster", districtMaster);
			subjectMasters = subjectMasterDAO.findByCriteria(criterionD);
			if(subjectMasters!=null && subjectMasters.size()>0)
			{
				for(SubjectMaster subject : subjectMasters)
				{
					sb.append("<option value="+subject.getSubjectId()+">"+subject.getSubjectName()+"</option>");
				}
			}
		}
		else
		{/*
			//Criterion criterionD = Restrictions.eq("status", "A");
			List<MasterSubjectMaster> lstMasterSubjectMasters = masterSubjectMasterDAO.findAllActiveSuject();
			sb.append("<option value='0'>All</option>");
				if(lstMasterSubjectMasters!=null && lstMasterSubjectMasters.size()>0)
				{
					for(MasterSubjectMaster subject : lstMasterSubjectMasters)
					{
						sb.append("<option value="+subject.getMasterSubjectId()+">"+subject.getMasterSubjectName()+"</option>");
					}
				}
		*/}
		return sb.toString();
	
		
	}



	
	
	
	@POST
	@Produces(MediaType.TEXT_XML)
	@Consumes(MediaType.TEXT_XML)
	@Path("/candidateOnboarding")
	public Response candidateOnboarding(InputStream incomingData,@Context HttpServletRequest request, @Context HttpServletResponse res) 
	{
		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>candidateOnboarding>>>>>>>>>>>>>>>>>>>>>>>>");
		Document doc = null;
		Element root = null;	
		String returnVal="";
		boolean isValid=true;
		int statusCode = 200;
		String Msg="";
		String KSNID ="";
		String tID ="";
		 Map<String,Date> latestNodeColorDateMap = new HashMap<String, Date>();
			try {
				
				String successORFailure="";
		        
		        ServletContext context = request.getSession().getServletContext();
				ApplicationContext context0 = WebApplicationContextUtils.getWebApplicationContext(context);
				
		        TeacherDetail teacherDetail=null;
				String payloadRequest = "";
				StringBuilder sb = new StringBuilder();
				
				BufferedReader in = new BufferedReader(new InputStreamReader(incomingData));
				String line = null;
				while ((line = in.readLine()) != null) {
					sb.append(line);
				}
				
				payloadRequest = sb.toString();
				System.err.println("Date ::::::::: "+new Date().toString()+"  >>>>>>> candidateOnboarding of Request::= "+payloadRequest);

				DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();

				DocumentBuilder docBuilder = null;
				docBuilder = builderFactory.newDocumentBuilder();
				InputSource inputSource = new InputSource();
				inputSource.setCharacterStream(new StringReader(payloadRequest));
				doc = docBuilder.parse(inputSource);
				
				NodeList CandidateDetailsTM = doc.getElementsByTagName("TeacherMatch");
				
			    if (CandidateDetailsTM != null && CandidateDetailsTM.getLength() > 0) {
			       
			        Element element = (Element) CandidateDetailsTM.item(0);
			        String kellyTransactionID = null;
			        String authKey = null;
			        List<TeacherDetail> teachers = new ArrayList<TeacherDetail>();
			        String KSNTalentID = null;
			        String tmTalentID = null;
			        String ODTM_WF1_STATUS= null;
			        String ODTM_EREG_WF1_READY = null;
			        String ODTM_WF2_STATUS = null;
			        String ODTM_EREG_WF2_READY = null;
			        String ODTM_WF3_STATUS = null;
			        String ODTM_EREG_WF3_READY = null;
			        String ODTM_BACKGROUND_DRUG_CHECK = null;
			        String ODTM_TALENT_TYPE = null;
			        String ODTM_TALENT_STATUS = null;
			        String ODTM_ACTION = null;
			        String ODTM_CREATED_DATE = null;
			        String ODTM_CREATED_BY = null;
			        String ODTM_MODIFIED_DATE = null;
			        String ODTM_MODIFIED_BY = null;
			        String ODTM_PROCESSED_STATUS = null;
			        
			        
			        
			        if(element.getElementsByTagName("AuthKey").item(0) != null)
			        authKey = element.getElementsByTagName("AuthKey").item(0).getTextContent();
					System.out.println("AuthKey: "+authKey);
			        
			        if(element.getElementsByTagName("KellyTransactionID").item(0) != null)
				    kellyTransactionID = element.getElementsByTagName("KellyTransactionID").item(0).getTextContent();
				    System.out.println("KellyTransactionID: "+kellyTransactionID);
			        
				    if(element.getElementsByTagName("KSNTalentID").item(0) != null)    
				    KSNTalentID = element.getElementsByTagName("KSNTalentID").item(0).getTextContent();
			        System.out.println("KSNTalentID: "+KSNTalentID);

			        if(element.getElementsByTagName("TMTalentID").item(0) != null) 
			        tmTalentID = element.getElementsByTagName("TMTalentID").item(0).getTextContent();
			        System.out.println("TMTalentID: "+tmTalentID);
			        
			        if(element.getElementsByTagName("ODTM_WF1_STATUS").item(0) != null) 
			        ODTM_WF1_STATUS = element.getElementsByTagName("ODTM_WF1_STATUS").item(0).getTextContent();
			        System.out.println("ODTM_WF1_STATUS: "+ODTM_WF1_STATUS);
			        
			        if(element.getElementsByTagName("ODTM_EREG_WF1_READY").item(0) != null) 
			        ODTM_EREG_WF1_READY = element.getElementsByTagName("ODTM_EREG_WF1_READY").item(0).getTextContent();
			        System.out.println("ODTM_EREG_WF1_READY: "+ODTM_EREG_WF1_READY);
			        
			        if(element.getElementsByTagName("ODTM_WF2_STATUS").item(0) != null) 
			        ODTM_WF2_STATUS = element.getElementsByTagName("ODTM_WF2_STATUS").item(0).getTextContent();
				    System.out.println("ODTM_WF2_STATUS: "+ODTM_WF2_STATUS);
				    
				    if(element.getElementsByTagName("ODTM_EREG_WF2_READY").item(0) != null) 
				    ODTM_EREG_WF2_READY = element.getElementsByTagName("ODTM_EREG_WF2_READY").item(0).getTextContent();
					System.out.println("ODTM_EREG_WF2_READY: "+ODTM_EREG_WF2_READY);
			        
					if(element.getElementsByTagName("ODTM_WF3_STATUS").item(0) != null) 
					ODTM_WF3_STATUS = element.getElementsByTagName("ODTM_WF3_STATUS").item(0).getTextContent();
					System.out.println("ODTM_WF3_STATUS: "+ODTM_WF3_STATUS);
					
					if(element.getElementsByTagName("ODTM_EREG_WF3_READY").item(0) != null)
					ODTM_EREG_WF3_READY = element.getElementsByTagName("ODTM_EREG_WF3_READY").item(0).getTextContent();
				    System.out.println("ODTM_EREG_WF3_READY: "+ODTM_EREG_WF3_READY);
				        
			        if(element.getElementsByTagName("ODTM_BACKGROUND_DRUG_CHECK").item(0) != null)
			        ODTM_BACKGROUND_DRUG_CHECK = element.getElementsByTagName("ODTM_BACKGROUND_DRUG_CHECK").item(0).getTextContent();
			        System.out.println("ODTM_BACKGROUND_DRUG_CHECK: "+ODTM_BACKGROUND_DRUG_CHECK);
				        
			        if(element.getElementsByTagName("ODTM_TALENT_TYPE").item(0) != null)
			        ODTM_TALENT_TYPE = element.getElementsByTagName("ODTM_TALENT_TYPE").item(0).getTextContent();
				    System.out.println("ODTM_TALENT_TYPE: "+ODTM_TALENT_TYPE);
					        
			        if(element.getElementsByTagName("ODTM_TALENT_STATUS").item(0) != null)
			        ODTM_TALENT_STATUS = element.getElementsByTagName("ODTM_TALENT_STATUS").item(0).getTextContent();
				    System.out.println("ODTM_TALENT_STATUS: "+ODTM_TALENT_STATUS);
						        
			        if(element.getElementsByTagName("ODTM_ACTION").item(0) != null)
			        ODTM_ACTION = element.getElementsByTagName("ODTM_ACTION").item(0).getTextContent();
				    System.out.println("ODTM_ACTION: "+ODTM_ACTION);
							        
			        if(element.getElementsByTagName("ODTM_CREATED_DATE").item(0) != null)
			        ODTM_CREATED_DATE = element.getElementsByTagName("ODTM_CREATED_DATE").item(0).getTextContent();
				    System.out.println("ODTM_CREATED_DATE: "+ODTM_CREATED_DATE);
								        
			        if(element.getElementsByTagName("ODTM_CREATED_BY").item(0) != null)
			        ODTM_CREATED_BY = element.getElementsByTagName("ODTM_CREATED_BY").item(0).getTextContent();
				    System.out.println("ODTM_CREATED_BY: "+ODTM_CREATED_BY);
				    
				    if(element.getElementsByTagName("ODTM_MODIFIED_DATE").item(0) != null)
				    ODTM_MODIFIED_DATE = element.getElementsByTagName("ODTM_MODIFIED_DATE").item(0).getTextContent();
				    System.out.println("ODTM_MODIFIED_DATE: "+ODTM_MODIFIED_DATE);
				        
				    if(element.getElementsByTagName("ODTM_MODIFIED_BY").item(0) != null)
				    ODTM_MODIFIED_BY = element.getElementsByTagName("ODTM_MODIFIED_BY").item(0).getTextContent();
					System.out.println("ODTM_MODIFIED_BY: "+ODTM_MODIFIED_BY);    
				       
					if(element.getElementsByTagName("ODTM_PROCESSED_STATUS").item(0) != null)
					ODTM_PROCESSED_STATUS = element.getElementsByTagName("ODTM_PROCESSED_STATUS").item(0).getTextContent();
					System.out.println("ODTM_PROCESSED_STATUS: "+ODTM_PROCESSED_STATUS);
					
					
					
					if(authKey == null){
			        	isValid=false;
			        	Msg=Utility.getLocaleValuePropByKey("msgRequestAuthKey", locale);
			        }else{
			        	if(authKey.isEmpty()){
			        		isValid=false;
			        		Msg=Utility.getLocaleValuePropByKey("msgRequestAuthKey", locale);
				    	}else if(!authKey.equalsIgnoreCase(WorkThreadServlet.authKey)){
			        		isValid=false;
			        		Msg=Utility.getLocaleValuePropByKey("msgRequestAuthKey", locale);
				    	}
			        }
					
					if(isValid)
					if(kellyTransactionID == null){
			        	isValid=false;
			        	Msg=Utility.getLocaleValuePropByKey("msgValidKellyTrationID", locale);
			        }else{
			        	if(kellyTransactionID.isEmpty()){
			        		isValid=false;
			        		Msg=Utility.getLocaleValuePropByKey("msgValidKellyTrationID", locale);
				    	}
			        }
					
					
					if(isValid){
				        if(KSNTalentID!=null && KSNTalentID.length()>0){
				        	 teacherDetailDAO = (TeacherDetailDAO)context0.getBean("teacherDetailDAO");
				        	 teachers =teacherDetailDAO.findByKSN(BigInteger.valueOf(Long.valueOf((KSNTalentID))));
				        	 if(teachers.isEmpty()){
				        		 	isValid=false;
				        		 	Msg=Utility.getLocaleValuePropByKey("msgKSNTalentID", locale);
						     }else if(teachers!=null && teachers.size()>0){
						    	 teacherDetail= teachers.get(0);
						    	 KSNID = teacherDetail.getKSNID().toString();
						    	 tID = teacherDetail.getTeacherId().toString();
						     }
				        }else{
				        	isValid=false;
				        	Msg=Utility.getLocaleValuePropByKey("msgKSNTalentID", locale);
				        }
					 } 
					
					if(isValid)
					if(tmTalentID == null){
			        	isValid=false;
			        	Msg=Utility.getLocaleValuePropByKey("msgInvalidTMTalentID", locale);
			        }else{
			        	if(tmTalentID.isEmpty()){
			        		isValid=false;
			        		Msg=Utility.getLocaleValuePropByKey("msgInvalidTMTalentID", locale);
				    	}else if(teacherDetail.getTeacherId() != Utility.getIntValue(tmTalentID)){
			        		isValid=false;
			        		Msg=Utility.getLocaleValuePropByKey("msgInvalidTMTalentID", locale);
				    	}
			        }
					
					
					Boolean dissConnection = false;
			        Boolean intializeFlag = false;
			        Boolean updateFlag = false;
			        if(ODTM_ACTION != null && ODTM_ACTION.equalsIgnoreCase(Utility.getLocaleValuePropByKey("msgDISCONNECT", locale))){
			        	dissConnection = true;
			        }else if(ODTM_ACTION != null && ODTM_ACTION.equalsIgnoreCase(Utility.getLocaleValuePropByKey("msgINITIALIZE", locale))){
			        	intializeFlag = true;
			        }else if(ODTM_ACTION != null && ODTM_ACTION.equalsIgnoreCase(Utility.getLocaleValuePropByKey("msgUPDATE", locale))){
			        	updateFlag = true;
			        }
					
			        if(isValid)
			        if(!(dissConnection || intializeFlag || updateFlag)){
			        	isValid=false;
			        	Msg=Utility.getLocaleValuePropByKey("msgInvalidAction", locale);
			        }
					
			        
			        
			        workFlowStatusDAO = (WorkFlowStatusDAO)context0.getBean("workFlowStatusDAO");
			        SessionFactory sessionFactory=workFlowStatusDAO.getSessionFactory();
				    StatelessSession statelesSsession = sessionFactory.openStatelessSession();
			        
				    
				    statusNodeColorHistoryDAO = (StatusNodeColorHistoryDAO)context0.getBean("statusNodeColorHistoryDAO"); 
				    
  		            List<MQEvent> mqEventList = new ArrayList<MQEvent>();
				    mqEventDAO = (MQEventDAO)context0.getBean("mqEventDAO"); 
				    mqEventHistoryDAO = (MQEventHistoryDAO)context0.getBean("mqEventHistoryDAO");
				    userMasterDAO = (UserMasterDAO)context0.getBean("userMasterDAO");
			        
			        List<MQEvent>  mqEList = new ArrayList<MQEvent>();
			        /*if(isValid){
				        mqEList = mqEventDAO.findMQEventExceptWorkFlowId(teacherDetail);
				        if(mqEList != null && mqEList.size() == 0 && updateFlag){
				        	isValid=false;
				        	Msg=Utility.getLocaleValuePropByKey("msgNoRecordsTalentUpdate", locale);
				        }
			        }*/
			        
					 if(isValid){
						        
						 /**
						  * add to track node color history
						  */
							 StatusNodeColorHistory statusNodeColorHistory = null;
							 Map<String,MQEvent> mapNodeStatus= new HashMap<String, MQEvent>();
							 List<StatusNodeColorHistory> listStatusClr = new ArrayList<StatusNodeColorHistory>();
						       listStatusClr = statusNodeColorHistoryDAO.getStatusColorHistoryByTeacher(teacherDetail);
						       if(listStatusClr.size()>0){
						    	   for(StatusNodeColorHistory s :listStatusClr)
						    		   if(s.getMqEvent()!=null)
						    				   latestNodeColorDateMap.put(s.getTeacherDetail().getTeacherId()+"##"+s.getMqEvent().getEventType(), s.getUpdateDateTime());
						       }
						      List<MQEvent> mqEventsList=new ArrayList<MQEvent>();
								 /*mqEventsList=mqEventDAO.findMQEventByTeacher(teacherDetail);
								 if(mqEventsList.size()>0){
									 for (MQEvent mqEv : mqEventsList) {
										 mapNodeStatus.put(mqEv.getTeacherdetail()+"-"+mqEv.getEventType() , mqEv);
									 }
								 }*/
						      
								 /**
							       * end
							       */
						        
						        List<WorkFlowStatus> workFlowStatusList =  workFlowStatusDAO.findAll();
						    	 Map<String, WorkFlowStatus> workFlowMap = new HashMap<String, WorkFlowStatus>();
						    	 if(workFlowStatusList!=null && workFlowStatusList.size()>0)
						    	 for (WorkFlowStatus workFlow : workFlowStatusList) {
						    		 workFlowMap.put(workFlow.getWorkFlowStatus(), workFlow);
								}
						        
						    	 String WF1_STATUS = null;
							     String WF2_STATUS = null;
							     String WF3_STATUS = null;
						        if(ODTM_WF1_STATUS != null && !ODTM_WF1_STATUS.isEmpty() && (ODTM_WF1_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINIT", locale))||ODTM_WF1_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))||ODTM_WF1_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale))||ODTM_WF1_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCANC", locale)))){
						        	WF1_STATUS = ODTM_WF1_STATUS;
						        }
						        if(ODTM_WF2_STATUS != null && !ODTM_WF2_STATUS.isEmpty()&& (ODTM_WF2_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINIT", locale))||ODTM_WF2_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))||ODTM_WF2_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale))||ODTM_WF2_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCANC", locale))||ODTM_WF2_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblAWAIT", locale)))){
						        	WF2_STATUS = ODTM_WF2_STATUS;
						        }
						        if(ODTM_WF3_STATUS != null && !ODTM_WF3_STATUS.isEmpty()&& (ODTM_WF3_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINIT", locale))||ODTM_WF3_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))||ODTM_WF3_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale))||ODTM_WF3_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCANC", locale)))){
						        	WF3_STATUS = ODTM_WF3_STATUS;
						        }
						        
						        
						        String WF1_READY = null;
						        String WF2_READY = null;
						        String WF3_READY = null;
						        if(ODTM_EREG_WF1_READY != null && !ODTM_EREG_WF1_READY.isEmpty() && (ODTM_EREG_WF1_READY.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))||ODTM_EREG_WF1_READY.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblN", locale)))){
						        	WF1_READY = ODTM_EREG_WF1_READY;
						        }
						        if(ODTM_EREG_WF2_READY != null && !ODTM_EREG_WF2_READY.isEmpty()&& (ODTM_EREG_WF2_READY.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))||ODTM_EREG_WF2_READY.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblN", locale)))){
						        	WF2_READY = ODTM_EREG_WF2_READY;
						        }
						        if(ODTM_EREG_WF3_READY != null && !ODTM_EREG_WF3_READY.isEmpty()&& (ODTM_EREG_WF3_READY.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))||ODTM_EREG_WF3_READY.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblN", locale)))){
						        	WF3_READY = ODTM_EREG_WF3_READY;
						        }
						        
						       
						         
							    mqEventList = mqEventDAO.findMQEventExceptLTKNdDiss(teacherDetail);
							    Map<String, MQEvent> mqEventMap = new HashMap<String, MQEvent>();
							    List<Integer> userIds=new  ArrayList<Integer>();
						    	 if(mqEventList!=null && mqEventList.size()>0)
						    	 for (MQEvent mqEvent : mqEventList) {
						    		 mqEventMap.put(mqEvent.getEventType(), mqEvent);
						    		 userIds.add(mqEvent.getCreatedBy());
								}
						    	List<UserMaster> userMasterList = userMasterDAO.getUserlistByUserIdArray(userIds);
						    	Map<Integer,UserMaster> userMap=new HashMap<Integer, UserMaster>();
						    	for (UserMaster userMaster : userMasterList) {
						    		userMap.put(userMaster.getUserId(),userMaster);
								}
						    	MQEventHistory mqEventHistory = new MQEventHistory();
						    	MQEvent mqEvent = new MQEvent();
						    	
						    	JobForTeacher jobForTeacher=new JobForTeacher();
						      
						    	
						    	headQuarterMasterDAO = (HeadQuarterMasterDAO)context0.getBean("headQuarterMasterDAO");
								jobForTeacherDAO = (JobForTeacherDAO)context0.getBean("jobForTeacherDAO");
								
		        				 HeadQuarterMaster headQuarterMaster1 = headQuarterMasterDAO.findById(1, false, false);
		        				 List<JobForTeacher> kellyAppliedJobs1 = new ArrayList<JobForTeacher>();
		        				 kellyAppliedJobs1 = jobForTeacherDAO.findByTeacherIdHQAndBranch(teacherDetail, headQuarterMaster1, null);
		        				 jobForTeacher = kellyAppliedJobs1.get(0);
						    	
						    	
						    	
						    	
						    	
						    	
						    	
						    	//update sandeep 10-11-15	
						        String backGroundCheck = null;
							    String talentType = null;
							    String talentStatus = null;
						        if(ODTM_BACKGROUND_DRUG_CHECK  != null && !ODTM_BACKGROUND_DRUG_CHECK.isEmpty() && (ODTM_BACKGROUND_DRUG_CHECK.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblNONE", locale))||ODTM_BACKGROUND_DRUG_CHECK.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINPR", locale))||ODTM_BACKGROUND_DRUG_CHECK.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblFAVO", locale))||ODTM_BACKGROUND_DRUG_CHECK.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblUNFA", locale))||ODTM_BACKGROUND_DRUG_CHECK.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblRERE", locale)))){
						        	backGroundCheck = ODTM_BACKGROUND_DRUG_CHECK;
						        }
						        if(ODTM_TALENT_TYPE != null && !ODTM_TALENT_TYPE.isEmpty()&& (ODTM_TALENT_TYPE.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCANDIDATE", locale))||ODTM_TALENT_TYPE.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblHIRED", locale))||ODTM_TALENT_TYPE.equalsIgnoreCase("OFFER"))){
						        	talentType = ODTM_TALENT_TYPE;
						        }
						        if(ODTM_TALENT_STATUS != null && !ODTM_TALENT_STATUS.isEmpty()&& (ODTM_TALENT_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblFlowACTIVE", locale))||ODTM_TALENT_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINACTIVEELIG", locale))||ODTM_TALENT_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINACTIVENELIG", locale)))){
						        	talentStatus = ODTM_TALENT_STATUS;
						        }
						        Boolean mailSend = true;
						    	if(intializeFlag){
						        	
						    		if(WF1_READY != null){
						        		
						        		mqEvent = mqEventMap.get(Utility.getLocaleValuePropByKey("lblWF1COM", locale));
						        		if(mqEvent!=null){
						        			if(mqEvent.getWorkFlowStatusId()!=null && mqEvent.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale)))
						        				mailSend = false;
						        			else
						        				mailSend = true;
						        			UserMaster userMaster=userMap.get(mqEvent.getCreatedBy());
							        	    mqEvent.setWorkFlowStatusId(workFlowMap.get(WF1_READY.toUpperCase()));
							        	    mqEvent.setKsnActionType(ODTM_ACTION);
							        	    mqEvent.setKsnTransId(kellyTransactionID);
							        	    if(WF1_READY.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
							        	    	mqEvent.setStatus("R");
							        	    	mqEvent.setAckStatus("Success");
							        	    	if(mailSend)
							        	    		mqService.sendERegMailToCand(request,userMaster,jobForTeacher,mqEvent,"1");
							        	    }else if(WF1_READY.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblN", locale))){
							        	    	mqEvent.setStatus("R");
							        	    	mqEvent.setAckStatus("Failure");
							        	    }else if(WF1_READY == null){
							        	    	mqEvent.setStatus("R");
							        	    	mqEvent.setAckStatus("Failure");
							        	    } 
							        	    
											 mqEventsList=mqEventDAO.findMQEventByTeacher(teacherDetail);
											 if(mqEventsList.size()>0){
												 for (MQEvent mqEv : mqEventsList) {
													 mapNodeStatus.put(mqEv.getTeacherdetail()+"-"+mqEv.getEventType() , mqEv);
												 }
											 }
							        	    
							        	    try {
												 statusNodeColorHistory = candidateGridService.saveStatusNodeColor(jobForTeacher,mqEvent,mapNodeStatus,latestNodeColorDateMap);
											 } catch (Exception e) {
												 e.printStackTrace();
											 }
											 
							        	    mqEventDAO.makePersistent(mqEvent);
							        	    
							        	    try{
							        	    	String newColor = candidateGridService.getNodeColorWithMqEvent(mqEvent,WF1_STATUS);
							        	    	System.out.println(" newColor :: "+newColor);
							        	    	statusNodeColorHistory.setCurrentColor(newColor);
							        	    	 statusNodeColorHistory.setUpdateDateTime(new Date());
							        	    	statusNodeColorHistoryDAO.updatePersistent(statusNodeColorHistory);

							        	    }catch(Exception ex){ 
							        	    	ex.printStackTrace();
							        	    }
						        		}else{
						        			mqEvent = new MQEvent();
							        	    mqEvent.setWorkFlowStatusId(workFlowMap.get(WF1_READY.toUpperCase()));
							        	    mqEvent.setKsnActionType(ODTM_ACTION);
							        	    mqEvent.setKsnTransId(kellyTransactionID);
							        	    mqEvent.setTeacherdetail(teacherDetail);
							        	    mqEvent.setEventType(Utility.getLocaleValuePropByKey("lblWF1COM", locale));
							        	    if(WF1_READY.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
							        	    	mqEvent.setStatus("C");
							        	    	mqEvent.setAckStatus("Success");
							        	    	mqService.sendERegMailToCand(request,null,jobForTeacher,mqEvent,"1");
							        	    }else if(WF1_READY.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblN", locale))){
							        	    	mqEvent.setStatus("R");
							        	    	mqEvent.setAckStatus("Failure");
							        	    }else if(WF1_READY == null){
							        	    	mqEvent.setStatus("R");
							        	    	mqEvent.setAckStatus("Failure");
							        	    } 
							        	    mqEvent.setCreatedDateTime(new Date());
							        	    
							        	    mqEventDAO.makePersistent(mqEvent);
							        	    
							        	    try {
												 statusNodeColorHistory = candidateGridService.saveStatusNodeColor(jobForTeacher,mqEvent,new HashMap<String, MQEvent>(),latestNodeColorDateMap);
											 } catch (Exception e) {
												 e.printStackTrace();
											 }
							        	    
							        	    try{
							        	    	
							        	    	String newColor = candidateGridService.getNodeColorWithMqEvent(mqEvent,WF1_STATUS);
												   System.out.println(" newColor :: "+newColor);
												   statusNodeColorHistory.setCurrentColor(newColor);
												   statusNodeColorHistory.setUpdateDateTime(new Date());
												   statusNodeColorHistoryDAO.updatePersistent(statusNodeColorHistory);
							        	    	
							        	    }catch(Exception ex){
							        	    	
							        	    }
						        		}
						        		
						        		if(WF1_READY.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
						        			Map<String,MQEvent> tempMap = new HashMap<String, MQEvent>();
						        			mqEvent = mqEventMap.get(Utility.getLocaleValuePropByKey("lblINVWF1", locale));
						        			if(mqEvent!=null){
						        				mqEvent.setStatus("R");
							        	    	mqEvent.setAckStatus("Success");
							        	    	
							        	    	mqEventsList=mqEventDAO.findMQEventByTeacher(teacherDetail);
												 if(mqEventsList.size()>0){
													 for (MQEvent mqEv : mqEventsList) {
														 if(mqEv.getEventType().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINVWF1", locale)))
															 tempMap.put(mqEv.getTeacherdetail()+"-"+mqEv.getEventType() , mqEv);
													 }
												 }
							        	    	
							        	    	try {
													 statusNodeColorHistory = candidateGridService.saveStatusNodeColor(jobForTeacher,mqEvent,tempMap,latestNodeColorDateMap);
												 } catch (Exception e) {
													 e.printStackTrace();
												 }
												 
								        	    mqEventDAO.makePersistent(mqEvent);
								        	    
								        	    try{
								        	    	String newColor = candidateGridService.getNodeColorWithMqEvent(mqEvent,WF1_STATUS);
								        	    	System.out.println(" newColor :: "+newColor);
								        	    	statusNodeColorHistory.setCurrentColor(newColor);
								        	    	 statusNodeColorHistory.setUpdateDateTime(new Date());
								        	    	statusNodeColorHistoryDAO.updatePersistent(statusNodeColorHistory);

								        	    }catch(Exception ex){ 
								        	    	ex.printStackTrace();
								        	    }
						        			}
						        			
						        			try {
												mqService.statusUpdateForKellyWorkFlowNodes(Utility.getLocaleValuePropByKey("lblINVWF1", locale),teacherDetail);
											} catch (Exception e) {
												e.printStackTrace();
											}
						        		}
			                         }
						        	
						        	if(WF1_STATUS != null){
						        		
							        		mqEvent = mqEventMap.get(Utility.getLocaleValuePropByKey("lblINVWF1", locale));
							        		MQEvent mqEventTemp = mqEventMap.get(Utility.getLocaleValuePropByKey("lblWF1COM", locale));
							        		
							        		if(WF1_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale)) 
							        				&& ((mqEvent==null && mqEventTemp==null) || (mqEvent==null && !(mqEventTemp!=null && mqEventTemp.getWorkFlowStatusId()!=null && mqEventTemp.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale)))) || !((mqEvent!=null && mqEvent.getWorkFlowStatusId()!=null && (mqEvent.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINIT", locale)) || mqEvent.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale)))))) && !(mqEventTemp!=null && mqEventTemp.getWorkFlowStatusId()!=null && mqEventTemp.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))))
							        		{
							        			try {
													triggerWorkFlowCOM(Utility.getLocaleValuePropByKey("lblWF1COM", locale), mqEventTemp, mqEventMap, userMap, workFlowMap, ODTM_ACTION, kellyTransactionID, teacherDetail, jobForTeacher, latestNodeColorDateMap,request,payloadRequest);
												} catch (Exception e) {
													e.printStackTrace();
												}
							        		}else{
							        			if(mqEvent!=null){
							        				mqEvent.setWorkFlowStatusId(workFlowMap.get(WF1_STATUS.toUpperCase()));
							        				mqEvent.setKsnActionType(ODTM_ACTION);
							        				mqEvent.setKsnTransId(kellyTransactionID);
							        				if(candidateGridService.getNodeColorWithMqEvent(mqEvent,null).equalsIgnoreCase("Yellow")){
							        					mqEvent.setTmInitiate(true);
							        				}else{
							        					mqEvent.setTmInitiate(false);
							        				}
							        				if(WF1_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINIT", locale))){
							        					mqEvent.setStatus("R");
							        					if(WF1_READY!=null && WF1_READY.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale)))
							        						mqEvent.setAckStatus("Success");
							        					else if(mqEventTemp!=null && mqEventTemp.getWorkFlowStatusId()!=null && mqEventTemp.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale)))
							        						mqEvent.setAckStatus("Success");
							        					else
							        						mqEvent.setAckStatus("Success");
							        				}else if(WF1_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))){
							        					mqEvent.setStatus("C");
							        					mqEvent.setAckStatus("Success");
							        				}else if(WF1_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale)) || WF1_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCANC", locale))){
							        					mqEvent.setStatus("R");
							        					mqEvent.setAckStatus("Failure");
							        				}
							        				
							        				if(!(WF1_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINIT", locale)))){

							        					mqEventsList=mqEventDAO.findMQEventByTeacher(teacherDetail);
							        					if(mqEventsList.size()>0){
							        						for (MQEvent mqEv : mqEventsList) {
							        							mapNodeStatus.put(mqEv.getTeacherdetail()+"-"+mqEv.getEventType() , mqEv);
							        						}
							        					}
							        				}
							        				
							        				
							        				
							        				if(!(WF1_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale))) && !(WF1_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINIT", locale)))){
							        					try {
							        						statusNodeColorHistory = candidateGridService.saveStatusNodeColor(jobForTeacher,mqEvent,mapNodeStatus,latestNodeColorDateMap);
							        					} catch (Exception e) {
							        						e.printStackTrace();
							        					}
							        				}
							        					
							        				mqEventDAO.makePersistent(mqEvent);
							        					
							        					if(!(WF1_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale))) && !(WF1_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINIT", locale)))){
							        						try{

							        							String newColor = candidateGridService.getNodeColorWithMqEvent(mqEvent,null);
							        							System.out.println(" newColor :: "+newColor);
							        							statusNodeColorHistory.setCurrentColor(newColor);
							        							statusNodeColorHistory.setUpdateDateTime(new Date());
							        							statusNodeColorHistoryDAO.updatePersistent(statusNodeColorHistory);

							        						}catch(Exception ex){
							        							ex.printStackTrace();
							        						}
							        					}
							        			}else{
							        				mqEvent = new MQEvent();
							        				mqEvent.setWorkFlowStatusId(workFlowMap.get(WF1_STATUS.toUpperCase()));
							        				mqEvent.setKsnActionType(ODTM_ACTION);
							        				mqEvent.setKsnTransId(kellyTransactionID);
							        				mqEvent.setTeacherdetail(teacherDetail);
							        				mqEvent.setTmInitiate(false);
							        				mqEvent.setEventType(Utility.getLocaleValuePropByKey("lblINVWF1", locale));
							        				if(WF1_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINIT", locale))){
							        					mqEvent.setStatus("R");
							        					if(WF1_READY!=null && WF1_READY.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale)))
							        						mqEvent.setAckStatus("Success");
							        					else if(mqEventTemp!=null && mqEventTemp.getWorkFlowStatusId()!=null && mqEventTemp.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale)))
							        						mqEvent.setAckStatus("Success");
							        					else
							        						mqEvent.setAckStatus("Success");
							        				}else if(WF1_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))){
							        					mqEvent.setStatus("C");
							        					mqEvent.setAckStatus("Success");
							        				}else if(WF1_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale)) || WF1_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCANC", locale))){
							        					mqEvent.setStatus("R");
							        					mqEvent.setAckStatus("Failure");
							        				} 
							        				mqEvent.setCreatedDateTime(new Date());

							        				mqEventDAO.makePersistent(mqEvent);
							        				
							        				if(!(WF1_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale))) && !(WF1_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINIT", locale)))){

							        					try {
							        						statusNodeColorHistory = candidateGridService.saveStatusNodeColor(jobForTeacher,mqEvent,new HashMap<String, MQEvent>(),latestNodeColorDateMap);
							        					} catch (Exception e) {
							        						e.printStackTrace();
							        					}

							        					try{

							        						String newColor = candidateGridService.getNodeColorWithMqEvent(mqEvent,null);
							        						System.out.println(" newColor :: "+newColor);
							        						statusNodeColorHistory.setCurrentColor(newColor);
							        						statusNodeColorHistory.setUpdateDateTime(new Date());
							        						statusNodeColorHistoryDAO.updatePersistent(statusNodeColorHistory);

							        					}catch(Exception ex){

							        					}
							        				}
							        			}
							        		}
							        		
							        		if(WF1_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINIT", locale))){
							        			try{
							        				resetWorkFlow(mqEventTemp);
							        			}catch(Exception ex){
							        				ex.printStackTrace();
							        			}
							        		}
							        		
							        		if(WF1_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale)))
							        			createHistoryForErrorWfxComNodes(jobForTeacher,mqEventTemp,mapNodeStatus,latestNodeColorDateMap,workFlowMap,ODTM_ACTION,kellyTransactionID,teacherDetail,Utility.getLocaleValuePropByKey("lblWF1COM", locale));
							        		
							        		if(WF1_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))){
							        				
							        			MQEvent mqETemp = mqEventMap.get(Utility.getLocaleValuePropByKey("lblWF1COM", locale));
							        			if(mqETemp!=null){
							        				try {
							        					//checkAndCreateWorkFlows(jobForTeacher,workFlowMap,mqEventMap, Utility.getLocaleValuePropByKey("lblWF1COM", locale),ODTM_ACTION,kellyTransactionID,teacherDetail,latestNodeColorDateMap);
							        				} catch (Exception e) {
							        					e.printStackTrace();
							        				}
							        			}
							        			try {
													mqService.statusUpdateForKellyWorkFlowNodes(Utility.getLocaleValuePropByKey("lblWF1COM", locale),teacherDetail);
												} catch (Exception e) {
													e.printStackTrace();
												}
							        		}
							        		else if(WF1_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINIT", locale))){
							        			if(mqEventTemp!=null && mqEventTemp.getWorkFlowStatusId()!=null && mqEventTemp.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))){
							        				mqEventTemp.setWorkFlowStatusId(null);
							        				mqEventDAO.makePersistent(mqEventTemp);
							        			}
							        		}
						        	}
						        	
						        	if(WF2_READY != null){
						        		
						        		mqEvent = mqEventMap.get(Utility.getLocaleValuePropByKey("lblWF2COM", locale));
						        		if(mqEvent!=null){
						        			
						        			if(mqEvent.getWorkFlowStatusId()!=null && mqEvent.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale)))
						        				mailSend = false;
						        			else
						        				mailSend = true;
						        			
						        			UserMaster userMaster=userMap.get(mqEvent.getCreatedBy());
							        	    mqEvent.setWorkFlowStatusId(workFlowMap.get(WF2_READY.toUpperCase()));
							        	    mqEvent.setKsnActionType(ODTM_ACTION);
							        	    mqEvent.setKsnTransId(kellyTransactionID);
							        	    if(WF2_READY.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
							        	    	mqEvent.setStatus("C");
							        	    	mqEvent.setAckStatus("Success");
							        	    	if(mailSend)
							        	    		mqService.sendERegMailToCand(request,userMaster,jobForTeacher,mqEvent,"2");
							        	    }else if(WF2_READY.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblN", locale))){
							        	    	mqEvent.setStatus("R");
							        	    	mqEvent.setAckStatus("Failure");
							        	    }else if(WF2_READY == null){
							        	    	mqEvent.setStatus("R");
							        	    	mqEvent.setAckStatus("Failure");
							        	    } 
							        	   
							        	    mqEventsList=mqEventDAO.findMQEventByTeacher(teacherDetail);
											 if(mqEventsList.size()>0){
												 for (MQEvent mqEv : mqEventsList) {
														 mapNodeStatus.put(mqEv.getTeacherdetail()+"-"+mqEv.getEventType() , mqEv);
												 }
											 }
							        	    
							        	    try {
												 statusNodeColorHistory = candidateGridService.saveStatusNodeColor(jobForTeacher,mqEvent,mapNodeStatus,latestNodeColorDateMap);
											 } catch (Exception e) {
												 e.printStackTrace();
											 }
							        	    
							        	    mqEventDAO.makePersistent(mqEvent);
							        	    
							        	    try{
							        	    	
							        	    	String newColor = candidateGridService.getNodeColorWithMqEvent(mqEvent,WF2_STATUS);
												   System.out.println(" newColor :: "+newColor);
												   statusNodeColorHistory.setCurrentColor(newColor);
												   statusNodeColorHistory.setUpdateDateTime(new Date());
												   statusNodeColorHistoryDAO.updatePersistent(statusNodeColorHistory);
							        	    	
							        	    }catch(Exception ex){
							        	    	
							        	    }
							        	    
						        		}else{
						        			mqEvent = new MQEvent();
							        	    mqEvent.setWorkFlowStatusId(workFlowMap.get(WF2_READY.toUpperCase()));
							        	    mqEvent.setKsnActionType(ODTM_ACTION);
							        	    mqEvent.setTeacherdetail(teacherDetail);
							        	    mqEvent.setEventType(Utility.getLocaleValuePropByKey("lblWF2COM", locale));
							        	    if(WF2_READY.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
							        	    	mqEvent.setStatus("C");
							        	    	mqEvent.setAckStatus("Success");
							        	    	mqService.sendERegMailToCand(request,null,jobForTeacher,mqEvent,"2");
							        	    }else if(WF2_READY.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblN", locale))){
							        	    	mqEvent.setStatus("R");
							        	    	mqEvent.setAckStatus("Failure");
							        	    }else if(WF2_READY == null){
							        	    	mqEvent.setStatus("R");
							        	    	mqEvent.setAckStatus("Failure");
							        	    } 
							        	    mqEvent.setCreatedDateTime(new Date());
							        	    
							        	    mqEventDAO.makePersistent(mqEvent);
							        	    
							        	    try {
												 statusNodeColorHistory = candidateGridService.saveStatusNodeColor(jobForTeacher,mqEvent,new HashMap<String, MQEvent>(),latestNodeColorDateMap);
											 } catch (Exception e) {
												 e.printStackTrace();
											 }
							        	    
							        	    try{
							        	    	
							        	    	String newColor = candidateGridService.getNodeColorWithMqEvent(mqEvent,WF2_STATUS);
												   System.out.println(" newColor :: "+newColor);
												   statusNodeColorHistory.setCurrentColor(newColor);
												   statusNodeColorHistory.setUpdateDateTime(new Date());
												   statusNodeColorHistoryDAO.updatePersistent(statusNodeColorHistory);
							        	    	
							        	    }catch(Exception ex){
							        	    	
							        	    }
							        	    
						        		}
						        		
						        		if(WF2_READY.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
						        			Map<String,MQEvent> tempMap = new HashMap<String, MQEvent>();
						        			mqEvent = mqEventMap.get(Utility.getLocaleValuePropByKey("lblINVWF2", locale));
						        			if(mqEvent!=null){
						        				mqEvent.setStatus("R");
							        	    	mqEvent.setAckStatus("Success");
							        	    	
							        	    	mqEventsList=mqEventDAO.findMQEventByTeacher(teacherDetail);
												 if(mqEventsList.size()>0){
													 for (MQEvent mqEv : mqEventsList) {
														 if(mqEv.getEventType().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINVWF2", locale)))
															tempMap.put(mqEv.getTeacherdetail()+"-"+mqEv.getEventType() , mqEv);
													 }
												 }
							        	    	try {
													 statusNodeColorHistory = candidateGridService.saveStatusNodeColor(jobForTeacher,mqEvent,tempMap,latestNodeColorDateMap);
												 } catch (Exception e) {
													 e.printStackTrace();
												 }
												 
								        	    mqEventDAO.makePersistent(mqEvent);
								        	    
								        	    try{
								        	    	String newColor = candidateGridService.getNodeColorWithMqEvent(mqEvent,WF1_STATUS);
								        	    	System.out.println(" newColor :: "+newColor);
								        	    	statusNodeColorHistory.setCurrentColor(newColor);
								        	    	 statusNodeColorHistory.setUpdateDateTime(new Date());
								        	    	statusNodeColorHistoryDAO.updatePersistent(statusNodeColorHistory);

								        	    }catch(Exception ex){ 
								        	    	ex.printStackTrace();
								        	    }
						        			}
						        			
						        			try {
												mqService.statusUpdateForKellyWorkFlowNodes(Utility.getLocaleValuePropByKey("lblINVWF2", locale),teacherDetail);
											} catch (Exception e) {
												e.printStackTrace();
											}
						        		}
			                         }
						        	
						        	if(WF2_STATUS != null){
						        		
						        		mqEvent = mqEventMap.get(Utility.getLocaleValuePropByKey("lblINVWF2", locale));
						        		MQEvent mqEventTemp = mqEventMap.get(Utility.getLocaleValuePropByKey("lblWF2COM", locale));
						        		if(WF2_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale)) 
						        				&& ((mqEvent==null && mqEventTemp==null) || (mqEvent==null && !(mqEventTemp!=null && mqEventTemp.getWorkFlowStatusId()!=null && mqEventTemp.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale)))) || !((mqEvent!=null && mqEvent.getWorkFlowStatusId()!=null && (mqEvent.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINIT", locale)) || mqEvent.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale)))))) && !(mqEventTemp!=null && mqEventTemp.getWorkFlowStatusId()!=null && mqEventTemp.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))))
						        		{
						        			try {
												triggerWorkFlowCOM(Utility.getLocaleValuePropByKey("lblWF2COM", locale), mqEventTemp, mqEventMap, userMap, workFlowMap, ODTM_ACTION, kellyTransactionID, teacherDetail, jobForTeacher, latestNodeColorDateMap,request,payloadRequest);
											} catch (Exception e) {
												e.printStackTrace();
											}
						        		}
						        		else{
						        			if(mqEvent!=null){
						        				mqEvent.setWorkFlowStatusId(workFlowMap.get(WF2_STATUS.toUpperCase()));
						        				mqEvent.setKsnActionType(ODTM_ACTION);
						        				mqEvent.setKsnTransId(kellyTransactionID);
						        				if(candidateGridService.getNodeColorWithMqEvent(mqEvent,null).equalsIgnoreCase("Yellow")){
						        					mqEvent.setTmInitiate(true);
						        				}else{
						        					mqEvent.setTmInitiate(false);
						        				}
						        				if(WF2_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINIT", locale))){
						        					mqEvent.setStatus("R");
						        					if(WF2_READY!=null && WF2_READY.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale)))
						        						mqEvent.setAckStatus("Success");
						        					else if(mqEventTemp!=null && mqEventTemp.getWorkFlowStatusId()!=null && mqEventTemp.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale)))
						        						mqEvent.setAckStatus("Success");
						        					else
						        						mqEvent.setAckStatus("Success");
						        				}else if(WF2_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))){
						        					mqEvent.setStatus("C");
						        					mqEvent.setAckStatus("Success");
						        				}else if(WF2_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale)) || WF2_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCANC", locale))){
						        					mqEvent.setStatus("R");
						        					mqEvent.setAckStatus("Failure");
						        				}else if(WF2_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblAWAIT", locale))){
						        					mqEvent.setStatus("R");
						        					mqEvent.setAckStatus("Success");
						        				} 
						        				mqEventsList=mqEventDAO.findMQEventByTeacher(teacherDetail);
						        				if(!(WF2_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINIT", locale)))){
						        					if(mqEventsList.size()>0){
						        						for (MQEvent mqEv : mqEventsList) {
						        							mapNodeStatus.put(mqEv.getTeacherdetail()+"-"+mqEv.getEventType() , mqEv);
						        						}
						        					}
						        					if(!(WF2_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale)))){
						        						try {
						        							statusNodeColorHistory = candidateGridService.saveStatusNodeColor(jobForTeacher,mqEvent,mapNodeStatus,latestNodeColorDateMap);
						        						} catch (Exception e) {
						        							e.printStackTrace();
						        						}
						        					}
						        				}
						        				mqEventDAO.makePersistent(mqEvent);
						        				if(!(WF2_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale))) && !(WF2_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINIT", locale)))){
						        					try{

						        						String newColor = candidateGridService.getNodeColorWithMqEvent(mqEvent,null);
						        						System.out.println(" newColor :: "+newColor);
						        						statusNodeColorHistory.setCurrentColor(newColor);
						        						statusNodeColorHistory.setUpdateDateTime(new Date());
						        						statusNodeColorHistoryDAO.updatePersistent(statusNodeColorHistory);

						        					}catch(Exception ex){

						        					}
						        				}
						        				
						        			}else{
						        				mqEvent = new MQEvent();
						        				mqEvent.setWorkFlowStatusId(workFlowMap.get(WF2_STATUS.toUpperCase()));
						        				mqEvent.setKsnActionType(ODTM_ACTION);
						        				mqEvent.setTmInitiate(false);
						        				mqEvent.setKsnTransId(kellyTransactionID);
						        				mqEvent.setTeacherdetail(teacherDetail);
						        				mqEvent.setEventType(Utility.getLocaleValuePropByKey("lblINVWF2", locale));
						        				if(WF2_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINIT", locale))){
						        					mqEvent.setStatus("R");
						        					if(WF2_READY!=null && WF2_READY.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale)))
						        						mqEvent.setAckStatus("Success");
						        					else if(mqEventTemp!=null && mqEventTemp.getWorkFlowStatusId()!=null && mqEventTemp.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale)))
						        						mqEvent.setAckStatus("Success");
						        					else
						        						mqEvent.setAckStatus("Success");
						        				}else if(WF2_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))){
						        					mqEvent.setStatus("C");
						        					mqEvent.setAckStatus("Success");

						        				}else if(WF2_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale)) || WF2_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCANC", locale))){
						        					mqEvent.setStatus("R");
						        					mqEvent.setAckStatus("Failure");
						        				}else if(WF2_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblAWAIT", locale))){
						        					mqEvent.setStatus("R");
						        					mqEvent.setAckStatus("Success");
						        				}
						        				mqEvent.setCreatedDateTime(new Date());

						        				mqEventDAO.makePersistent(mqEvent);
						        				
						        				
						        				if(!(WF2_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale))) && !(WF1_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINIT", locale)))){
						        					try {
						        						statusNodeColorHistory = candidateGridService.saveStatusNodeColor(jobForTeacher,mqEvent,new HashMap<String, MQEvent>(),latestNodeColorDateMap);
						        					} catch (Exception e) {
						        						e.printStackTrace();
						        					}

						        					try{

						        						String newColor = candidateGridService.getNodeColorWithMqEvent(mqEvent,null);
						        						System.out.println(" newColor :: "+newColor);
						        						statusNodeColorHistory.setCurrentColor(newColor);
						        						statusNodeColorHistory.setUpdateDateTime(new Date());
						        						statusNodeColorHistoryDAO.updatePersistent(statusNodeColorHistory);

						        					}catch(Exception ex){

						        					}
						        				}

						        			}
						        		}
						        		
						        		
						        		if(WF2_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINIT", locale))){
						        			try{
						        				resetWorkFlow(mqEventTemp);
						        			}catch(Exception ex){
						        				ex.printStackTrace();
						        			}
						        		}
						        		
						        		if(WF2_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale)))
						        			createHistoryForErrorWfxComNodes(jobForTeacher,mqEventTemp,mapNodeStatus,latestNodeColorDateMap,workFlowMap,ODTM_ACTION,kellyTransactionID,teacherDetail,Utility.getLocaleValuePropByKey("lblWF1COM", locale));

						        		
						        		if(WF2_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))){
						        			MQEvent mqETemp = mqEventMap.get(Utility.getLocaleValuePropByKey("lblWF2COM", locale));
						        			if(mqETemp!=null){
						        				try {
						        					//checkAndCreateWorkFlows(jobForTeacher,workFlowMap,mqEventMap, Utility.getLocaleValuePropByKey("lblWF2COM", locale),ODTM_ACTION,kellyTransactionID,teacherDetail,latestNodeColorDateMap);
						        				} catch (Exception e) {
						        					e.printStackTrace();
						        				}
						        			}
						        			try {
												mqService.statusUpdateForKellyWorkFlowNodes(Utility.getLocaleValuePropByKey("lblWF2COM", locale),teacherDetail);
											} catch (Exception e) {
												e.printStackTrace();
											}
											
											try {
												/*headQuarterMasterDAO = (HeadQuarterMasterDAO)context0.getBean("headQuarterMasterDAO");
												jobForTeacherDAO = (JobForTeacherDAO)context0.getBean("jobForTeacherDAO");
												userMasterDAO = (UserMasterDAO)context0.getBean("userMasterDAO");
												secondaryStatusDAO = (SecondaryStatusDAO)context0.getBean("secondaryStatusDAO");
						        				 System.out.println(":::::::::Inside WorkFlow >>autoCallQueue=>>:::::::::::::::");
						        				 HeadQuarterMaster headQuarterMaster = headQuarterMasterDAO.findById(1, false, false);
						        				 UserMaster userMaster = userMasterDAO.findById(1, false, false);
						        				 List<JobForTeacher> kellyAppliedJobs = new ArrayList<JobForTeacher>();
						        				 kellyAppliedJobs = jobForTeacherDAO.findByTeacherIdHQAndBranch(teacherDetail, headQuarterMaster, null);
						        				 JobForTeacher jft = kellyAppliedJobs.get(0);
						                         SecondaryStatus secondaryStatusWF3=secondaryStatusDAO.findSecondaryStatusObjByJobCategoryHeadAndBranch(jft.getJobId(),Utility.getLocaleValuePropByKey("lblINVWF3", locale));
						                         autoCallQueue(null,secondaryStatusWF3,jft,userMaster,context0);*/
											} catch (Exception e) {
												e.printStackTrace();
											}
											
						        		}
						        		else if(WF2_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINIT", locale))){
						        			if(mqEventTemp!=null && mqEventTemp.getWorkFlowStatusId()!=null && mqEventTemp.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))){
						        				mqEventTemp.setWorkFlowStatusId(null);
						        				mqEventDAO.makePersistent(mqEventTemp);
						        			}
						        		}
						        	}
						        	if(WF3_READY != null){
						        		
						        		mqEvent = mqEventMap.get(Utility.getLocaleValuePropByKey("lblWF3COM", locale));
						        		if(mqEvent!=null){
						        			
						        			if(mqEvent.getWorkFlowStatusId()!=null && mqEvent.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale)))
						        				mailSend = false;
						        			else
						        				mailSend = true;
						        			UserMaster userMaster=userMap.get(mqEvent.getCreatedBy());
							        	    mqEvent.setWorkFlowStatusId(workFlowMap.get(WF3_READY.toUpperCase()));
							        	    mqEvent.setKsnActionType(ODTM_ACTION);
							        	    mqEvent.setKsnTransId(kellyTransactionID);
							        	    if(WF3_READY.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
							        	    	mqEvent.setStatus("C");
							        	    	mqEvent.setAckStatus("Success");
							        	    	if(mailSend)
							        	    		mqService.sendERegMailToCand(request,userMaster,jobForTeacher,mqEvent,"3");
							        	    }else if(WF3_READY.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblN", locale))){
							        	    	mqEvent.setStatus("R");
							        	    	mqEvent.setAckStatus("Failure");
							        	    }else if(WF3_READY == null){
							        	    	mqEvent.setStatus("R");
							        	    	mqEvent.setAckStatus("Failure");
							        	    } 
							        	   
							        	    mqEventsList=mqEventDAO.findMQEventByTeacher(teacherDetail);
											 if(mqEventsList.size()>0){
												 for (MQEvent mqEv : mqEventsList) {
													 mapNodeStatus.put(mqEv.getTeacherdetail()+"-"+mqEv.getEventType() , mqEv);
												 }
											 }
							        	    
							        	    try {
												 statusNodeColorHistory = candidateGridService.saveStatusNodeColor(jobForTeacher,mqEvent,mapNodeStatus,latestNodeColorDateMap);
											 } catch (Exception e) {
												 e.printStackTrace();
											 }
							        	    
							        	    mqEventDAO.makePersistent(mqEvent);
							        	    
							        	    try{
							        	    	
							        	    	String newColor = candidateGridService.getNodeColorWithMqEvent(mqEvent,WF3_STATUS);
												   System.out.println(" newColor :: "+newColor);
												   statusNodeColorHistory.setCurrentColor(newColor);
												   statusNodeColorHistory.setUpdateDateTime(new Date());
												   statusNodeColorHistoryDAO.updatePersistent(statusNodeColorHistory);
							        	    	
							        	    }catch(Exception ex){
							        	    	
							        	    }
							        	    
							        	    
						        		}else{
						        			mqEvent = new MQEvent();
							        	    mqEvent.setWorkFlowStatusId(workFlowMap.get(WF3_READY.toUpperCase()));
							        	    mqEvent.setKsnActionType(ODTM_ACTION);
							        	    mqEvent.setKsnTransId(kellyTransactionID);
							        	    mqEvent.setTeacherdetail(teacherDetail);
							        	    mqEvent.setEventType(Utility.getLocaleValuePropByKey("lblWF3COM", locale));
							        	    if(WF3_READY.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
							        	    	mqEvent.setStatus("C");
							        	    	mqEvent.setAckStatus("Success");
							        	    	mqService.sendERegMailToCand(request,null,jobForTeacher,mqEvent,"3");
							        	    }else if(WF3_READY.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblN", locale))){
							        	    	mqEvent.setStatus("R");
							        	    	mqEvent.setAckStatus("Failure");
							        	    }else if(WF3_READY == null){
							        	    	mqEvent.setStatus("R");
							        	    	mqEvent.setAckStatus("Failure");
							        	    } 
							        	    mqEvent.setCreatedDateTime(new Date());
							        	    
											 mqEventDAO.makePersistent(mqEvent);
							        	    
							        	    try {
												 statusNodeColorHistory = candidateGridService.saveStatusNodeColor(jobForTeacher,mqEvent,new HashMap<String, MQEvent>(),latestNodeColorDateMap);
											 } catch (Exception e) {
												 e.printStackTrace();
											 }
							        	    
							        	    try{
							        	    	String newColor = candidateGridService.getNodeColorWithMqEvent(mqEvent,WF3_STATUS);
												   System.out.println(" newColor :: "+newColor);
												   statusNodeColorHistory.setCurrentColor(newColor);
												   statusNodeColorHistory.setUpdateDateTime(new Date());
												   statusNodeColorHistoryDAO.updatePersistent(statusNodeColorHistory);
							        	    	
							        	    }catch(Exception ex){
							        	    	
							        	    }
							        	    
						        		}
						        		
						        		if(WF3_READY.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
						        			Map<String,MQEvent> tempMap = new HashMap<String, MQEvent>();
						        			mqEvent = mqEventMap.get(Utility.getLocaleValuePropByKey("lblINVWF3", locale));
						        			if(mqEvent!=null){
						        				mqEvent.setStatus("R");
							        	    	mqEvent.setAckStatus("Success");
							        	    	
							        	    	mqEventsList=mqEventDAO.findMQEventByTeacher(teacherDetail);
												 if(mqEventsList.size()>0){
													 for (MQEvent mqEv : mqEventsList) {
														 if(mqEv.getEventType().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINVWF3", locale)))
															 tempMap.put(mqEv.getTeacherdetail()+"-"+mqEv.getEventType() , mqEv);
													 }
												 }
							        	    	
							        	    	try {
													 statusNodeColorHistory = candidateGridService.saveStatusNodeColor(jobForTeacher,mqEvent,tempMap,latestNodeColorDateMap);
												 } catch (Exception e) {
													 e.printStackTrace();
												 }
												 
								        	    mqEventDAO.makePersistent(mqEvent);
								        	    
								        	    try{
								        	    	String newColor = candidateGridService.getNodeColorWithMqEvent(mqEvent,WF1_STATUS);
								        	    	System.out.println(" newColor :: "+newColor);
								        	    	statusNodeColorHistory.setCurrentColor(newColor);
								        	    	 statusNodeColorHistory.setUpdateDateTime(new Date());
								        	    	statusNodeColorHistoryDAO.updatePersistent(statusNodeColorHistory);

								        	    }catch(Exception ex){ 
								        	    	ex.printStackTrace();
								        	    }
						        			}
						        			
						        			try {
												mqService.statusUpdateForKellyWorkFlowNodes(Utility.getLocaleValuePropByKey("lblINVWF3", locale),teacherDetail);
											} catch (Exception e) {
												e.printStackTrace();
											}
						        		}
			                         }
						        	
						        	if(WF3_STATUS != null){
						        		
						        		mqEvent = mqEventMap.get(Utility.getLocaleValuePropByKey("lblINVWF3", locale));
						        		MQEvent mqEventTemp = mqEventMap.get(Utility.getLocaleValuePropByKey("lblWF3COM", locale));
						        		if(WF3_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale)) 
						        				&& ((mqEvent==null && mqEventTemp==null) || (mqEvent==null && !(mqEventTemp!=null && mqEventTemp.getWorkFlowStatusId()!=null && mqEventTemp.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale)))) || !((mqEvent!=null && mqEvent.getWorkFlowStatusId()!=null && (mqEvent.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINIT", locale)) || mqEvent.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale)))))) && !(mqEventTemp!=null && mqEventTemp.getWorkFlowStatusId()!=null && mqEventTemp.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))))
						        		{
						        			try {
												triggerWorkFlowCOM(Utility.getLocaleValuePropByKey("lblWF3COM", locale), mqEventTemp, mqEventMap, userMap, workFlowMap, ODTM_ACTION, kellyTransactionID, teacherDetail, jobForTeacher, latestNodeColorDateMap,request,payloadRequest);
											} catch (Exception e) {
												e.printStackTrace();
											}
						        		}
						        		else{
						        			if(mqEvent!=null){
						        				mqEvent.setWorkFlowStatusId(workFlowMap.get(WF3_STATUS.toUpperCase()));
						        				mqEvent.setKsnActionType(ODTM_ACTION);
						        				mqEvent.setKsnTransId(kellyTransactionID);
						        				if(candidateGridService.getNodeColorWithMqEvent(mqEvent,null).equalsIgnoreCase("Yellow")){
						        					mqEvent.setTmInitiate(true);
						        				}else{
						        					mqEvent.setTmInitiate(false);
						        				}
						        				if(WF3_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINIT", locale))){
						        					mqEvent.setStatus("R");
						        					if(WF3_READY!=null && WF3_READY.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale)))
						        						mqEvent.setAckStatus("Success");
						        					else if(mqEventTemp!=null && mqEventTemp.getWorkFlowStatusId()!=null && mqEventTemp.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale)))
						        						mqEvent.setAckStatus("Success");
						        					else
						        						mqEvent.setAckStatus("Success");
						        				}else if(WF3_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))){
						        					mqEvent.setStatus("C");
						        					mqEvent.setAckStatus("Success");
						        				}else if(WF3_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale)) || WF3_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCANC", locale))){
						        					mqEvent.setStatus("R");
						        					mqEvent.setAckStatus("Failure");
						        				} 
						        				
						        				if(!(WF3_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINIT", locale)))){
						        					mqEventsList=mqEventDAO.findMQEventByTeacher(teacherDetail);
						        					if(mqEventsList.size()>0){
						        						for (MQEvent mqEv : mqEventsList) {
						        							mapNodeStatus.put(mqEv.getTeacherdetail()+"-"+mqEv.getEventType() , mqEv);
						        						}
						        					}

						        					if(!(WF3_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale)))){
						        						try {
						        							statusNodeColorHistory = candidateGridService.saveStatusNodeColor(jobForTeacher,mqEvent,mapNodeStatus,latestNodeColorDateMap);
						        						} catch (Exception e) {
						        							e.printStackTrace();
						        						}
						        					}
						        				}
						        				mqEventDAO.makePersistent(mqEvent);
						        				
						        				if(!(WF3_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale))) && !(WF3_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINIT", locale)))){
						        					try{

						        						String newColor = candidateGridService.getNodeColorWithMqEvent(mqEvent,null);
						        						System.out.println(" newColor :: "+newColor);
						        						statusNodeColorHistory.setCurrentColor(newColor);
						        						statusNodeColorHistory.setUpdateDateTime(new Date());
						        						statusNodeColorHistoryDAO.updatePersistent(statusNodeColorHistory);

						        					}catch(Exception ex){

						        					}
						        				}

						        			}else{
						        				mqEvent = new MQEvent();
						        				mqEvent.setWorkFlowStatusId(workFlowMap.get(WF3_STATUS.toUpperCase()));
						        				mqEvent.setKsnActionType(ODTM_ACTION);
						        				mqEvent.setKsnTransId(kellyTransactionID);
						        				mqEvent.setTeacherdetail(teacherDetail);
						        				mqEvent.setTmInitiate(false);
						        				mqEvent.setEventType(Utility.getLocaleValuePropByKey("lblINVWF3", locale));
						        				if(WF3_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINIT", locale))){
						        					mqEvent.setStatus("R");
						        					if(WF3_READY!=null && WF3_READY.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale)))
						        						mqEvent.setAckStatus("Success");
						        					else if(mqEventTemp!=null && mqEventTemp.getWorkFlowStatusId()!=null && mqEventTemp.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale)))
						        						mqEvent.setAckStatus("Success");
						        					else
						        						mqEvent.setAckStatus("Success");
						        				}else if(WF3_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))){
						        					mqEvent.setStatus("C");
						        					mqEvent.setAckStatus("Success");
						        				}else if(WF3_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale)) || WF3_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCANC", locale))){
						        					mqEvent.setStatus("R");
						        					mqEvent.setAckStatus("Failure");
						        				} 
						        				mqEvent.setCreatedDateTime(new Date());

						        				mqEventDAO.makePersistent(mqEvent);
						        				
						        				if(!(WF3_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale))) && !(WF3_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINIT", locale)))){
						        					try {
						        						statusNodeColorHistory = candidateGridService.saveStatusNodeColor(jobForTeacher,mqEvent,new HashMap<String, MQEvent>(),latestNodeColorDateMap);
						        					} catch (Exception e) {
						        						e.printStackTrace();
						        					}

						        					try{
						        						String newColor = candidateGridService.getNodeColorWithMqEvent(mqEvent,null);
						        						System.out.println(" newColor :: "+newColor);
						        						statusNodeColorHistory.setCurrentColor(newColor);
						        						statusNodeColorHistory.setUpdateDateTime(new Date());
						        						statusNodeColorHistoryDAO.updatePersistent(statusNodeColorHistory);

						        					}catch(Exception ex){

						        					}
						        				}
						        			}
						        		}
						        		
						        		
						        		if(WF3_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINIT", locale))){
						        			try{
						        				resetWorkFlow(mqEventTemp);
						        			}catch(Exception ex){
						        				ex.printStackTrace();
						        			}
						        		}
						        		
						        		if(WF3_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale)))
						        			createHistoryForErrorWfxComNodes(jobForTeacher,mqEventTemp,mapNodeStatus,latestNodeColorDateMap,workFlowMap,ODTM_ACTION,kellyTransactionID,teacherDetail,Utility.getLocaleValuePropByKey("lblWF1COM", locale));

						        		
						        		if(WF3_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))){
						        			MQEvent mqETemp = mqEventMap.get(Utility.getLocaleValuePropByKey("lblWF3COM", locale));
						        			if(mqETemp!=null){
						        				try {
						        					//checkAndCreateWorkFlows(jobForTeacher,workFlowMap,mqEventMap, Utility.getLocaleValuePropByKey("lblWF3COM", locale),ODTM_ACTION,kellyTransactionID,teacherDetail,latestNodeColorDateMap);
						        				} catch (Exception e) {
						        					e.printStackTrace();
						        				}
						        			}
						        			try {
												mqService.statusUpdateForKellyWorkFlowNodes(Utility.getLocaleValuePropByKey("lblWF3COM", locale),teacherDetail);
											} catch (Exception e) {
												e.printStackTrace();
											}
						        		}
						        		else if(WF3_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINIT", locale))){
						        			if(mqEventTemp!=null && mqEventTemp.getWorkFlowStatusId()!=null && mqEventTemp.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))){
						        				mqEventTemp.setWorkFlowStatusId(null);
						        				mqEventDAO.makePersistent(mqEventTemp);
						        			}
						        		}
			                         }
						        	if(backGroundCheck != null){
						    			mqEvent = mqEventMap.get(Utility.getLocaleValuePropByKey("lblScreening", locale));
						        		if(mqEvent!=null){
							        	    mqEvent.setWorkFlowStatusId(workFlowMap.get(backGroundCheck.toUpperCase()));
							        	    mqEvent.setKsnActionType(ODTM_ACTION);
							        	    mqEvent.setKsnTransId(kellyTransactionID);
							        		StatusMaster statusMaster= WorkThreadServlet.statusMap.get("vcomp");
						        	    	mqEvent.setStatusMaster(statusMaster);
							        	    if(backGroundCheck.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblFAVO", locale))){
							        	    	mqService.statusUpdateForKellyWorkFlowNodes(Utility.getLocaleValuePropByKey("lblScreening", locale),teacherDetail);
							        	    	mqEvent.setStatus("C");
							        	    }else{
							        	    	try {
													commonService.resetStatusForJobs(null,teacherDetail,null,Utility.getLocaleValuePropByKey("lblScreening", locale));
												} catch (Exception e) {
													e.printStackTrace();
												}
												
							        	    	mqEvent.setStatus("R");
							        	    }
							        	    if(backGroundCheck.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblUNFA", locale))){
							        	    	 mqEvent.setAckStatus("Failure");
							        	    }else{
							        	    	 mqEvent.setAckStatus("Success");
							        	    }
							        	   
							        	    mqEventsList=mqEventDAO.findMQEventByTeacher(teacherDetail);
											 if(mqEventsList.size()>0){
												 for (MQEvent mqEv : mqEventsList) {
													 mapNodeStatus.put(mqEv.getTeacherdetail()+"-"+mqEv.getEventType() , mqEv);
												 }
											 }
							        	    
							        	    try {
												 statusNodeColorHistory = candidateGridService.saveStatusNodeColor(jobForTeacher,mqEvent,mapNodeStatus,latestNodeColorDateMap);
											 } catch (Exception e) {
												 e.printStackTrace();
											 }
							        	    
							        	    mqEventDAO.makePersistent(mqEvent);
							        	    
							        	    try{
							        	    	
							        	    	String newColor = candidateGridService.getNodeColorWithMqEvent(mqEvent,null);
												   System.out.println(" newColor :: "+newColor);
												   statusNodeColorHistory.setCurrentColor(newColor);
												   statusNodeColorHistory.setUpdateDateTime(new Date());
												   statusNodeColorHistoryDAO.updatePersistent(statusNodeColorHistory);
							        	    	
							        	    }catch(Exception ex){
							        	    	
							        	    }
							        	    
							        	    
						        		}else{
						        			mqEvent = new MQEvent();
							        	    mqEvent.setWorkFlowStatusId(workFlowMap.get(backGroundCheck.toUpperCase()));
							        	    mqEvent.setKsnActionType(ODTM_ACTION);
							        	    mqEvent.setKsnTransId(kellyTransactionID);
							        	    mqEvent.setTeacherdetail(teacherDetail);
							        	    mqEvent.setEventType(Utility.getLocaleValuePropByKey("lblScreening", locale));
							        		StatusMaster statusMaster= WorkThreadServlet.statusMap.get("vcomp");
						        	    	mqEvent.setStatusMaster(statusMaster);
							        	    if(backGroundCheck.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblFAVO", locale))){
							        	    	mqService.statusUpdateForKellyWorkFlowNodes(Utility.getLocaleValuePropByKey("lblScreening", locale),teacherDetail);
							        	    	mqEvent.setStatus("C");
							        	    }else{
							        	    	mqEvent.setStatus("R");
							        	    }
							        	    if(backGroundCheck.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblUNFA", locale))){
							        	    	 mqEvent.setAckStatus("Failure");
							        	    }else{
							        	    	 mqEvent.setAckStatus("Success");
							        	    }
							        	    mqEvent.setCreatedDateTime(new Date());
							        	   
							        	    mqEventDAO.makePersistent(mqEvent);
							        	    
							        	    try {
												 statusNodeColorHistory = candidateGridService.saveStatusNodeColor(jobForTeacher,mqEvent,new HashMap<String, MQEvent>(),latestNodeColorDateMap);
											 } catch (Exception e) {
												 e.printStackTrace();
											 }
							        	    
							        	    try{
							        	    	
							        	    	String newColor = candidateGridService.getNodeColorWithMqEvent(mqEvent,null);
												   System.out.println(" newColor :: "+newColor);
												   statusNodeColorHistory.setCurrentColor(newColor);
												   statusNodeColorHistory.setUpdateDateTime(new Date());
												   statusNodeColorHistoryDAO.updatePersistent(statusNodeColorHistory);
							        	    	
							        	    }catch(Exception ex){
							        	    	
							        	    }
							        	    
						        		}
						    		}
									if(talentType != null){
						    			mqEvent = mqEventMap.get(Utility.getLocaleValuePropByKey("msgHired2", locale));
						    			StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hird");
						        		if(mqEvent!=null){
							        	    mqEvent.setWorkFlowStatusId(workFlowMap.get(talentType.toUpperCase()));
						        			mqEvent.setStatusMaster(statusMaster);
							        	    mqEvent.setKsnActionType(ODTM_ACTION);
							        	    mqEvent.setKsnTransId(kellyTransactionID);
							        	    if(talentType.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblHIRED", locale))){
							        	    	mqEvent.setStatus("C");
							        	    	mqEvent.setAckStatus("Success");
							        	    }else{
							        	    	mqEvent.setStatus("R");
							        	    	mqEvent.setAckStatus("Success");
							        	    	try {
													commonService.resetStatusForJobs(null,teacherDetail,null,null);
												} catch (Exception e) {
													// TODO Auto-generated catch block
													e.printStackTrace();
												}
							        	    }
							        	     
							        	    mqEventsList=mqEventDAO.findMQEventByTeacher(teacherDetail);
											 if(mqEventsList.size()>0){
												 for (MQEvent mqEv : mqEventsList) {
													 mapNodeStatus.put(mqEv.getTeacherdetail()+"-"+mqEv.getEventType() , mqEv);
												 }
											 }
							        	    
							        	    try {
												 statusNodeColorHistory = candidateGridService.saveStatusNodeColor(jobForTeacher,mqEvent,mapNodeStatus,latestNodeColorDateMap);
											 } catch (Exception e) {
												 e.printStackTrace();
											 }
							        	    
							        	    mqEventDAO.makePersistent(mqEvent);
							        	    
							        	    try{
							        	    	
							        	    	String newColor = candidateGridService.getNodeColorWithMqEvent(mqEvent,null);
												   System.out.println(" newColor :: "+newColor);
												   statusNodeColorHistory.setCurrentColor(newColor);
												   statusNodeColorHistory.setUpdateDateTime(new Date());
												   statusNodeColorHistoryDAO.updatePersistent(statusNodeColorHistory);
							        	    	
							        	    }catch(Exception ex){
							        	    	
							        	    }
							        	    
							        	    
						        		}else{
						        			mqEvent = new MQEvent();
							        	    mqEvent.setWorkFlowStatusId(workFlowMap.get(talentType.toUpperCase()));
							        	    mqEvent.setKsnActionType(ODTM_ACTION);
							        	    mqEvent.setKsnTransId(kellyTransactionID);
							        	    mqEvent.setTeacherdetail(teacherDetail);
							        	    mqEvent.setEventType(Utility.getLocaleValuePropByKey("msgHired2", locale));
						        			mqEvent.setStatusMaster(statusMaster);
							        	    if(talentType.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblHIRED", locale))){
							        	    	mqEvent.setStatus("C");
							        	    	mqEvent.setAckStatus("Success");
							        	    }else{
							        	    	mqEvent.setStatus("R");
							        	    	mqEvent.setAckStatus("Success");
							        	    }
							        	    mqEvent.setCreatedDateTime(new Date()); 
							        	    
							        	    mqEventDAO.makePersistent(mqEvent);
							        	    
							        	    try {
												 statusNodeColorHistory = candidateGridService.saveStatusNodeColor(jobForTeacher,mqEvent,new HashMap<String, MQEvent>(),latestNodeColorDateMap);
											 } catch (Exception e) {
												 e.printStackTrace();
											 }
							        	    
							        	    try{
							        	    	
							        	    	String newColor = candidateGridService.getNodeColorWithMqEvent(mqEvent,null);
												   System.out.println(" newColor :: "+newColor);
												   statusNodeColorHistory.setCurrentColor(newColor);
												   statusNodeColorHistory.setUpdateDateTime(new Date());
												   statusNodeColorHistoryDAO.updatePersistent(statusNodeColorHistory);
							        	    	
							        	    }catch(Exception ex){
							        	    	
							        	    }
							        	    
						        		}
						        		if(talentType.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblHIRED", locale))){
						        			try {
												mqService.statusUpdateForKellyWorkFlowNodes(Utility.getLocaleValuePropByKey("msgHired2", locale),teacherDetail);
											} catch (Exception e) {
												e.printStackTrace();
											}
						        		}
						    		}
						    		
						    		if(talentStatus != null){
						    			mqEvent = mqEventMap.get(Utility.getLocaleValuePropByKey("msgTalentStatus", locale));
						        		if(mqEvent!=null){
							        	    mqEvent.setWorkFlowStatusId(workFlowMap.get(talentStatus.toUpperCase()));
							        	    mqEvent.setKsnActionType(ODTM_ACTION);
							        	    mqEvent.setKsnTransId(kellyTransactionID);
							        	    if(talentStatus.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINACTIVEELIG", locale))){
							        	    	StatusMaster statusMaster= WorkThreadServlet.statusMap.get("ielig");
							        	    	mqEvent.setStatus("C");
							        	    	mqEvent.setStatusMaster(statusMaster);
							        	    	mqService.statusUpdateForKellyWorkFlowNodes(Utility.getLocaleValuePropByKey("lblInElig", locale),teacherDetail);
							        	    }else  if(talentStatus.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINACTIVENELIG", locale))){
							        	    	StatusMaster statusMaster= WorkThreadServlet.statusMap.get("iielig");
							        	    	mqEvent.setStatus("C");
							        	    	mqEvent.setStatusMaster(statusMaster);
							        	    	mqService.statusUpdateForKellyWorkFlowNodes(Utility.getLocaleValuePropByKey("lblInInlig", locale),teacherDetail);
							        	    }else if(talentStatus.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblFlowACTIVE", locale))){
							        	    	mqEvent.setStatus("R");
							        	    	try {
													commonService.resetStatusForJobs(null,teacherDetail,null,null);
												} catch (Exception e) {
													// TODO Auto-generated catch block
													e.printStackTrace();
												}
							        	    }else{
							        	    	mqEvent.setStatus(null);
							        	    	mqEvent.setStatusMaster(null);
							        	    }
							        	    mqEvent.setAckStatus("Success");
							        	    
							        	    
							        	    mqEventsList=mqEventDAO.findMQEventByTeacher(teacherDetail);
											 if(mqEventsList.size()>0){
												 for (MQEvent mqEv : mqEventsList) {
													 mapNodeStatus.put(mqEv.getTeacherdetail()+"-"+mqEv.getEventType() , mqEv);
												 }
											 }
							        	    
							        	    try {
												 statusNodeColorHistory = candidateGridService.saveStatusNodeColor(jobForTeacher,mqEvent,mapNodeStatus,latestNodeColorDateMap);
											 } catch (Exception e) {
												 e.printStackTrace();
											 }
							        	    
							        	    mqEventDAO.makePersistent(mqEvent);
							        	    
							        	    try{
							        	    	
							        	    	String newColor = candidateGridService.getNodeColorWithMqEvent(mqEvent,null);
												   System.out.println(" newColor :: "+newColor);
												   statusNodeColorHistory.setCurrentColor(newColor);
												   statusNodeColorHistory.setUpdateDateTime(new Date());
												   statusNodeColorHistoryDAO.updatePersistent(statusNodeColorHistory);
							        	    	
							        	    }catch(Exception ex){
							        	    	
							        	    }
							        	    
							        	    
						        		}else{
						        			mqEvent = new MQEvent();
							        	    mqEvent.setWorkFlowStatusId(workFlowMap.get(talentStatus.toUpperCase()));
							        	    mqEvent.setKsnActionType(ODTM_ACTION);
							        	    mqEvent.setKsnTransId(kellyTransactionID);
							        	    mqEvent.setTeacherdetail(teacherDetail);
							        	    mqEvent.setEventType(Utility.getLocaleValuePropByKey("msgTalentStatus", locale));
							        	    if(talentStatus.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINACTIVEELIG", locale))){
							        	    	StatusMaster statusMaster= WorkThreadServlet.statusMap.get("ielig");
							        	    	mqEvent.setStatus("C");
							        	    	mqEvent.setStatusMaster(statusMaster);
							        	    	mqService.statusUpdateForKellyWorkFlowNodes(Utility.getLocaleValuePropByKey("lblInElig", locale),teacherDetail);
							        	    }else  if(talentStatus.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINACTIVENELIG", locale))){
							        	    	StatusMaster statusMaster= WorkThreadServlet.statusMap.get("iielig");
							        	    	mqEvent.setStatus("C");
							        	    	mqEvent.setStatusMaster(statusMaster);
							        	    	mqService.statusUpdateForKellyWorkFlowNodes(Utility.getLocaleValuePropByKey("lblInInlig", locale),teacherDetail);
							        	    }else if(talentStatus.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblFlowACTIVE", locale))){
							        	    	mqEvent.setStatus("R");
							        	    	try {
													commonService.resetStatusForJobs(null,teacherDetail,null,null);
												} catch (Exception e) {
													// TODO Auto-generated catch block
													e.printStackTrace();
												}
							        	    }else{
							        	    	mqEvent.setStatus(null);
							        	    	mqEvent.setStatusMaster(null);
							        	    }
							        	    mqEvent.setAckStatus("Success");
							        	    mqEvent.setCreatedDateTime(new Date());
							        	    
							        	    mqEventDAO.makePersistent(mqEvent);
							        	    
							        	    try {
												 statusNodeColorHistory = candidateGridService.saveStatusNodeColor(jobForTeacher,mqEvent,new HashMap<String, MQEvent>(),latestNodeColorDateMap);
											 } catch (Exception e) {
												 e.printStackTrace();
											 }
							        	    
							        	    try{
							        	    	
							        	    	String newColor = candidateGridService.getNodeColorWithMqEvent(mqEvent,null);
												   System.out.println(" newColor :: "+newColor);
												   statusNodeColorHistory.setCurrentColor(newColor);
												   statusNodeColorHistory.setUpdateDateTime(new Date());
												   statusNodeColorHistoryDAO.updatePersistent(statusNodeColorHistory);
							        	    	
							        	    }catch(Exception ex){
							        	    	
							        	    }
							        	    
						        		}
						    		}
						    		
						    		System.out.println("-------------mqEvent----------"+mqEvent);
						    		if(mqEvent != null && mqEvent.getEventType() != null){
							    		mqEventHistory = new MQEventHistory();
										mqEventHistory.setMqEvent(mqEvent);
										mqEventHistory.setCreatedBy(1);
										mqEventHistory.setCreatedDateTime(new Date());
										mqEventHistory.setIpAddress(request.getRemoteAddr());
										mqEventHistory.setXmlObject(payloadRequest);
										mqEventHistoryDAO.makePersistent(mqEventHistory);
						    		}	
						        }
						        if(updateFlag){
						        	
						        	Boolean updateMQEvent = false;
						        	/*if(mqEList != null && mqEList.size() > 0){
						        		updateMQEvent = true;
						        	}*/
						        	
						        	updateMQEvent = true;
						        	
						        	if(WF1_READY != null){
						        		
						        		mqEvent = mqEventMap.get(Utility.getLocaleValuePropByKey("lblWF1COM", locale));
						        		if(mqEvent!=null){
						        			if(mqEvent.getWorkFlowStatusId()!=null && mqEvent.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale)))
						        				mailSend = false;
						        			else
						        				mailSend = true;
						        			UserMaster userMaster=userMap.get(mqEvent.getCreatedBy());
							        	    mqEvent.setWorkFlowStatusId(workFlowMap.get(WF1_READY.toUpperCase()));
							        	    mqEvent.setKsnActionType(ODTM_ACTION);
							        	    mqEvent.setKsnTransId(kellyTransactionID);
							        	    if(WF1_READY.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
							        	    	mqEvent.setStatus("C");
							        	    	mqEvent.setAckStatus("Success");
							        	    	if(mailSend)
							        	    		mqService.sendERegMailToCand(request,userMaster,jobForTeacher,mqEvent,"1");
							        	    }else if(WF1_READY.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblN", locale))){
							        	    	mqEvent.setStatus("R");
							        	    	mqEvent.setAckStatus("Failure");
							        	    }else if(WF1_READY == null){
							        	    	mqEvent.setStatus("R");
							        	    	mqEvent.setAckStatus("Failure");
							        	    } 
							        	    
							        	    mqEventsList=mqEventDAO.findMQEventByTeacher(teacherDetail);
											 if(mqEventsList.size()>0){
												 for (MQEvent mqEv : mqEventsList) {
													 mapNodeStatus.put(mqEv.getTeacherdetail()+"-"+mqEv.getEventType() , mqEv);
												 }
											 }
							        	    
							        	    try {
												 statusNodeColorHistory = candidateGridService.saveStatusNodeColor(jobForTeacher,mqEvent,mapNodeStatus,latestNodeColorDateMap);
											 } catch (Exception e) {
												 e.printStackTrace();
											 }
							        	    
							        	    mqEventDAO.makePersistent(mqEvent);
							        	    
							        	    try{
							        	    	
							        	    	String newColor = candidateGridService.getNodeColorWithMqEvent(mqEvent,WF1_STATUS);
												   System.out.println(" newColor :: "+newColor);
												   statusNodeColorHistory.setCurrentColor(newColor);
												   statusNodeColorHistory.setUpdateDateTime(new Date());
												   statusNodeColorHistoryDAO.updatePersistent(statusNodeColorHistory);
							        	    	
							        	    }catch(Exception ex){
							        	    	
							        	    }
							        	    
						        		}else{
						        			if(updateMQEvent){
						        				mqEvent = new MQEvent();
								        	    mqEvent.setWorkFlowStatusId(workFlowMap.get(WF1_READY.toUpperCase()));
								        	    mqEvent.setKsnActionType(ODTM_ACTION);
								        	    mqEvent.setKsnTransId(kellyTransactionID);
								        	    mqEvent.setTeacherdetail(teacherDetail);
								        	    mqEvent.setEventType(Utility.getLocaleValuePropByKey("lblWF1COM", locale));
								        	    if(WF1_READY.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
								        	    	mqEvent.setStatus("C");
								        	    	mqEvent.setAckStatus("Success");
								        	    	mqService.sendERegMailToCand(request,null,jobForTeacher,mqEvent,"1");
								        	    }else if(WF1_READY.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblN", locale))){
								        	    	mqEvent.setStatus("R");
								        	    	mqEvent.setAckStatus("Failure");
								        	    }else if(WF1_READY == null){
								        	    	mqEvent.setStatus("R");
								        	    	mqEvent.setAckStatus("Failure");
								        	    } 
								        	    mqEvent.setCreatedDateTime(new Date());
								        	  
								        	    mqEventDAO.makePersistent(mqEvent);
								        	    
								        	    try {
													 statusNodeColorHistory = candidateGridService.saveStatusNodeColor(jobForTeacher,mqEvent,new HashMap<String, MQEvent>(),latestNodeColorDateMap);
												 } catch (Exception e) {
													 e.printStackTrace();
												 }
								        	    
								        	    try{
								        	    	
								        	    	String newColor = candidateGridService.getNodeColorWithMqEvent(mqEvent,WF1_STATUS);
													   System.out.println(" newColor :: "+newColor);
													   statusNodeColorHistory.setCurrentColor(newColor);
													   statusNodeColorHistory.setUpdateDateTime(new Date());
													   statusNodeColorHistoryDAO.updatePersistent(statusNodeColorHistory);
								        	    	
								        	    }catch(Exception ex){
								        	    	
								        	    }
								        	    
						        				
						        			}
						        		}
						        		
						        		if(WF1_READY.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
						        			Map<String,MQEvent> tempMap = new HashMap<String, MQEvent>();
						        			mqEvent = mqEventMap.get(Utility.getLocaleValuePropByKey("lblINVWF1", locale));
						        			if(mqEvent!=null){
						        				mqEvent.setStatus("R");
							        	    	mqEvent.setAckStatus("Success");
							        	    	
							        	    	mqEventsList=mqEventDAO.findMQEventByTeacher(teacherDetail);
												 if(mqEventsList.size()>0){
													 for (MQEvent mqEv : mqEventsList) {
														 if(mqEv.getEventType().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINVWF1", locale)))
															 tempMap.put(mqEv.getTeacherdetail()+"-"+mqEv.getEventType() , mqEv);
													 }
												 }
							        	    	
							        	    	try {
													 statusNodeColorHistory = candidateGridService.saveStatusNodeColor(jobForTeacher,mqEvent,tempMap,latestNodeColorDateMap);
												 } catch (Exception e) {
													 e.printStackTrace();
												 }
												 
								        	    mqEventDAO.makePersistent(mqEvent);
								        	    
								        	    try{
								        	    	String newColor = candidateGridService.getNodeColorWithMqEvent(mqEvent,WF1_STATUS);
								        	    	System.out.println(" newColor :: "+newColor);
								        	    	statusNodeColorHistory.setCurrentColor(newColor);
								        	    	 statusNodeColorHistory.setUpdateDateTime(new Date());
								        	    	statusNodeColorHistoryDAO.updatePersistent(statusNodeColorHistory);

								        	    }catch(Exception ex){ 
								        	    	ex.printStackTrace();
								        	    }
						        			}
						        			
						        			try {
												mqService.statusUpdateForKellyWorkFlowNodes(Utility.getLocaleValuePropByKey("lblINVWF1", locale),teacherDetail);
											} catch (Exception e) {
												e.printStackTrace();
											}
						        		}
			                         }
						        	
						        	if(WF1_STATUS != null){
							        		mqEvent = mqEventMap.get(Utility.getLocaleValuePropByKey("lblINVWF1", locale));
							        		MQEvent mqEventTemp = mqEventMap.get(Utility.getLocaleValuePropByKey("lblWF1COM", locale));
							        		if(WF1_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale)) 
							        				&& ((mqEvent==null && mqEventTemp==null) || (mqEvent==null && !(mqEventTemp!=null && mqEventTemp.getWorkFlowStatusId()!=null && mqEventTemp.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale)))) || !((mqEvent!=null && mqEvent.getWorkFlowStatusId()!=null && (mqEvent.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINIT", locale)) || mqEvent.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale)))))) && !(mqEventTemp!=null && mqEventTemp.getWorkFlowStatusId()!=null && mqEventTemp.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))))
							        		{
							        			try {
													triggerWorkFlowCOM(Utility.getLocaleValuePropByKey("lblWF1COM", locale), mqEventTemp, mqEventMap, userMap, workFlowMap, ODTM_ACTION, kellyTransactionID, teacherDetail, jobForTeacher, latestNodeColorDateMap,request,payloadRequest);
												} catch (Exception e) {
													e.printStackTrace();
												}
							        		}
							        		else{
							        			if(mqEvent!=null){
							        				mqEvent.setWorkFlowStatusId(workFlowMap.get(WF1_STATUS.toUpperCase()));
							        				mqEvent.setKsnActionType(ODTM_ACTION);
							        				mqEvent.setKsnTransId(kellyTransactionID);
							        				if(candidateGridService.getNodeColorWithMqEvent(mqEvent,null).equalsIgnoreCase("Yellow")){
							        					mqEvent.setTmInitiate(true);
							        				}else{
							        					mqEvent.setTmInitiate(false);
							        				}
							        				if(WF1_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINIT", locale))){
							        					mqEvent.setStatus("R");
							        					if(WF1_READY!=null && WF1_READY.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale)))
							        						mqEvent.setAckStatus("Success");
							        					else if(mqEventTemp!=null && mqEventTemp.getWorkFlowStatusId()!=null && mqEventTemp.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale)))
							        						mqEvent.setAckStatus("Success");
							        					else
							        						mqEvent.setAckStatus("Success");
							        				}else if(WF1_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))){
							        					mqEvent.setStatus("C");
							        					mqEvent.setAckStatus("Success");
							        				}else if(WF1_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale)) || WF1_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCANC", locale))){
							        					mqEvent.setStatus("R");
							        					mqEvent.setAckStatus("Failure");
							        				} 
							        				
							        				if(!(WF1_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINIT", locale)))){
							        					mqEventsList=mqEventDAO.findMQEventByTeacher(teacherDetail);
							        					if(mqEventsList.size()>0){
							        						for (MQEvent mqEv : mqEventsList) {
							        							mapNodeStatus.put(mqEv.getTeacherdetail()+"-"+mqEv.getEventType() , mqEv);
							        						}
							        					}

							        					if(!(WF1_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale)))){
							        						try {
							        							statusNodeColorHistory = candidateGridService.saveStatusNodeColor(jobForTeacher,mqEvent,mapNodeStatus,latestNodeColorDateMap);
							        						} catch (Exception e) {
							        							e.printStackTrace();
							        						}
							        					}
							        				}

							        				mqEventDAO.makePersistent(mqEvent);
							        				if(!(WF1_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale))) && !(WF1_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINIT", locale)))){
							        					try{

							        						String newColor = candidateGridService.getNodeColorWithMqEvent(mqEvent,null);
							        						System.out.println(" newColor :: "+newColor);
							        						statusNodeColorHistory.setCurrentColor(newColor);
							        						statusNodeColorHistory.setUpdateDateTime(new Date());
							        						statusNodeColorHistoryDAO.updatePersistent(statusNodeColorHistory);

							        					}catch(Exception ex){

							        					}
							        				}
							        			}else{
							        				if(updateMQEvent){
							        					mqEvent = new MQEvent();
							        					mqEvent.setWorkFlowStatusId(workFlowMap.get(WF1_STATUS.toUpperCase()));
							        					mqEvent.setKsnActionType(ODTM_ACTION);
							        					mqEvent.setKsnTransId(kellyTransactionID);
							        					mqEvent.setTmInitiate(false);
							        					mqEvent.setTeacherdetail(teacherDetail);
							        					mqEvent.setEventType(Utility.getLocaleValuePropByKey("lblINVWF1", locale));
							        					if(WF1_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINIT", locale))){
							        						mqEvent.setStatus("R");
							        						if(WF1_READY!=null && WF1_READY.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale)))
							        							mqEvent.setAckStatus("Success");
							        						else if(mqEventTemp!=null && mqEventTemp.getWorkFlowStatusId()!=null && mqEventTemp.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale)))
							        							mqEvent.setAckStatus("Success");
							        						else
							        							mqEvent.setAckStatus("Success");
							        					}else if(WF1_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))){
							        						mqEvent.setStatus("C");
							        						mqEvent.setAckStatus("Success");
							        					}else if(WF1_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale)) || WF1_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCANC", locale))){
							        						mqEvent.setStatus("R");
							        						mqEvent.setAckStatus("Failure");
							        					} 
							        					mqEvent.setCreatedDateTime(new Date());

							        					mqEventDAO.makePersistent(mqEvent);
							        					
							        					if(!(WF1_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale))) && !(WF1_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINIT", locale)))){
							        						try {
							        							statusNodeColorHistory = candidateGridService.saveStatusNodeColor(jobForTeacher,mqEvent,new HashMap<String, MQEvent>(),latestNodeColorDateMap);
							        						} catch (Exception e) {
							        							e.printStackTrace();
							        						}

							        						try{

							        							String newColor = candidateGridService.getNodeColorWithMqEvent(mqEvent,null);
							        							System.out.println(" newColor :: "+newColor);
							        							statusNodeColorHistory.setCurrentColor(newColor);
							        							statusNodeColorHistory.setUpdateDateTime(new Date());
							        							statusNodeColorHistoryDAO.updatePersistent(statusNodeColorHistory);

							        						}catch(Exception ex){

							        						}
							        					}
							        				}
							        			}
							        		}
							        		
							        		
							        		if(WF1_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINIT", locale))){
							        			try{
							        				resetWorkFlow(mqEventTemp);
							        			}catch(Exception ex){
							        				ex.printStackTrace();
							        			}
							        		}
							        		
							        		if(WF1_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale)))
							        			createHistoryForErrorWfxComNodes(jobForTeacher,mqEventTemp,mapNodeStatus,latestNodeColorDateMap,workFlowMap,ODTM_ACTION,kellyTransactionID,teacherDetail,Utility.getLocaleValuePropByKey("lblWF1COM", locale));

							        		
							        		if(WF1_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))){
							        			MQEvent mqETemp = mqEventMap.get(Utility.getLocaleValuePropByKey("lblWF1COM", locale));
							        			if(mqETemp!=null){
							        				try {
							        					//checkAndCreateWorkFlows(jobForTeacher,workFlowMap,mqEventMap, Utility.getLocaleValuePropByKey("lblWF1COM", locale),ODTM_ACTION,kellyTransactionID,teacherDetail,latestNodeColorDateMap);
							        				} catch (Exception e) {
							        					e.printStackTrace();
							        				}
							        			}
							        			try {
													mqService.statusUpdateForKellyWorkFlowNodes(Utility.getLocaleValuePropByKey("lblWF1COM", locale),teacherDetail);
												} catch (Exception e) {
													e.printStackTrace();
												}
							        		}
							        		else if(WF1_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINIT", locale))){
							        			if(mqEventTemp!=null && mqEventTemp.getWorkFlowStatusId()!=null && mqEventTemp.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))){
							        				mqEventTemp.setWorkFlowStatusId(null);
							        				mqEventDAO.makePersistent(mqEventTemp);
							        			}
							        		}
						        	}
						        	
						        	if(WF2_READY != null){
						        		
						        		mqEvent = mqEventMap.get(Utility.getLocaleValuePropByKey("lblWF2COM", locale));
						        		if(mqEvent!=null){
						        			if(mqEvent.getWorkFlowStatusId()!=null && mqEvent.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale)))
						        				mailSend = false;
						        			else
						        				mailSend = true;
						        			UserMaster userMaster=userMap.get(mqEvent.getCreatedBy());
							        	    mqEvent.setWorkFlowStatusId(workFlowMap.get(WF2_READY.toUpperCase()));
							        	    mqEvent.setKsnActionType(ODTM_ACTION);
							        	    mqEvent.setKsnTransId(kellyTransactionID);
							        	    if(WF2_READY.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
							        	    	mqEvent.setStatus("C");
							        	    	mqEvent.setAckStatus("Success");
							        	    	if(mailSend)
							        	    		mqService.sendERegMailToCand(request,userMaster,jobForTeacher,mqEvent,"2");
							        	    }else if(WF2_READY.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblN", locale))){
							        	    	mqEvent.setStatus("R");
							        	    	mqEvent.setAckStatus("Failure");
							        	    }else if(WF2_READY == null){
							        	    	mqEvent.setStatus("R");
							        	    	mqEvent.setAckStatus("Failure");
							        	    } 
							        	   
							        	    mqEventsList=mqEventDAO.findMQEventByTeacher(teacherDetail);
											 if(mqEventsList.size()>0){
												 for (MQEvent mqEv : mqEventsList) {
													 mapNodeStatus.put(mqEv.getTeacherdetail()+"-"+mqEv.getEventType() , mqEv);
												 }
											 }
							        	    
							        	    try {
												 statusNodeColorHistory = candidateGridService.saveStatusNodeColor(jobForTeacher,mqEvent,mapNodeStatus,latestNodeColorDateMap);
											 } catch (Exception e) {
												 e.printStackTrace();
											 }
							        	    
							        	    mqEventDAO.makePersistent(mqEvent);
							        	    
							        	    try{
							        	    	
							        	    	String newColor = candidateGridService.getNodeColorWithMqEvent(mqEvent,WF2_STATUS);
												   System.out.println(" newColor :: "+newColor);
												   statusNodeColorHistory.setCurrentColor(newColor);
												   statusNodeColorHistory.setUpdateDateTime(new Date());
												   statusNodeColorHistoryDAO.updatePersistent(statusNodeColorHistory);
							        	    	
							        	    }catch(Exception ex){
							        	    	
							        	    }
							        	    
						        		}else{
						        			if(updateMQEvent){
						        				mqEvent = new MQEvent();
								        	    mqEvent.setWorkFlowStatusId(workFlowMap.get(WF2_READY.toUpperCase()));
								        	    mqEvent.setKsnActionType(ODTM_ACTION);
								        	    mqEvent.setTeacherdetail(teacherDetail);
								        	    mqEvent.setEventType(Utility.getLocaleValuePropByKey("lblWF2COM", locale));
								        	    if(WF2_READY.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
								        	    	mqEvent.setStatus("C");
								        	    	mqEvent.setAckStatus("Success");
								        	    	mqService.sendERegMailToCand(request,null,jobForTeacher,mqEvent,"2");
								        	    }else if(WF2_READY.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblN", locale))){
								        	    	mqEvent.setStatus("R");
								        	    	mqEvent.setAckStatus("Failure");
								        	    }else if(WF2_READY == null){
								        	    	mqEvent.setStatus("R");
								        	    	mqEvent.setAckStatus("Failure");
								        	    } 
								        	    mqEvent.setCreatedDateTime(new Date());
								        	    
								        	    mqEventDAO.makePersistent(mqEvent);
								        	    
								        	    try {
													 statusNodeColorHistory = candidateGridService.saveStatusNodeColor(jobForTeacher,mqEvent,new HashMap<String, MQEvent>(),latestNodeColorDateMap);
												 } catch (Exception e) {
													 e.printStackTrace();
												 }
								        	    
								        	    
								        	    try{
								        	    	
								        	    	String newColor = candidateGridService.getNodeColorWithMqEvent(mqEvent,WF2_STATUS);
													   System.out.println(" newColor :: "+newColor);
													   statusNodeColorHistory.setCurrentColor(newColor);
													   statusNodeColorHistory.setUpdateDateTime(new Date());
													   statusNodeColorHistoryDAO.updatePersistent(statusNodeColorHistory);
								        	    	
								        	    }catch(Exception ex){
								        	    	
								        	    }
								        	    
						        			}
						        		}
						        		
						        		if(WF2_READY.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
						        			Map<String,MQEvent> tempMap = new HashMap<String, MQEvent>();
						        			mqEvent = mqEventMap.get(Utility.getLocaleValuePropByKey("lblINVWF2", locale));
						        			if(mqEvent!=null){
						        				mqEvent.setStatus("R");
							        	    	mqEvent.setAckStatus("Success");
							        	    	
							        	    	mqEventsList=mqEventDAO.findMQEventByTeacher(teacherDetail);
												 if(mqEventsList.size()>0){
													 for (MQEvent mqEv : mqEventsList) {
														 if(mqEv.getEventType().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINVWF2", locale)))
															 tempMap.put(mqEv.getTeacherdetail()+"-"+mqEv.getEventType() , mqEv);
													 }
												 }
							        	    	
							        	    	try {
													 statusNodeColorHistory = candidateGridService.saveStatusNodeColor(jobForTeacher,mqEvent,tempMap,latestNodeColorDateMap);
												 } catch (Exception e) {
													 e.printStackTrace();
												 }
												 
								        	    mqEventDAO.makePersistent(mqEvent);
								        	    
								        	    try{
								        	    	String newColor = candidateGridService.getNodeColorWithMqEvent(mqEvent,WF1_STATUS);
								        	    	System.out.println(" newColor :: "+newColor);
								        	    	statusNodeColorHistory.setCurrentColor(newColor);
								        	    	 statusNodeColorHistory.setUpdateDateTime(new Date());
								        	    	statusNodeColorHistoryDAO.updatePersistent(statusNodeColorHistory);

								        	    }catch(Exception ex){ 
								        	    	ex.printStackTrace();
								        	    }
						        			}
						        			try {
												mqService.statusUpdateForKellyWorkFlowNodes(Utility.getLocaleValuePropByKey("lblINVWF2", locale),teacherDetail);
											} catch (Exception e) {
												e.printStackTrace();
											}
						        		}
			                         }
						        	
						        	if(WF2_STATUS != null){
						        		
						        		mqEvent = mqEventMap.get(Utility.getLocaleValuePropByKey("lblINVWF2", locale));
						        		MQEvent mqEventTemp = mqEventMap.get(Utility.getLocaleValuePropByKey("lblWF2COM", locale));
						        		if(WF2_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale)) 
						        				&& ((mqEvent==null && mqEventTemp==null) || (mqEvent==null && !(mqEventTemp!=null && mqEventTemp.getWorkFlowStatusId()!=null && mqEventTemp.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale)))) || !((mqEvent!=null && mqEvent.getWorkFlowStatusId()!=null && (mqEvent.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINIT", locale)) || mqEvent.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale)))))) && !(mqEventTemp!=null && mqEventTemp.getWorkFlowStatusId()!=null && mqEventTemp.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))))
						        		{
						        			try {
												triggerWorkFlowCOM(Utility.getLocaleValuePropByKey("lblWF2COM", locale), mqEventTemp, mqEventMap, userMap, workFlowMap, ODTM_ACTION, kellyTransactionID, teacherDetail, jobForTeacher, latestNodeColorDateMap,request,payloadRequest);
											} catch (Exception e) {
												e.printStackTrace();
											}
						        		}
						        		else{
						        			if(mqEvent!=null){
						        				mqEvent.setWorkFlowStatusId(workFlowMap.get(WF2_STATUS.toUpperCase()));
						        				mqEvent.setKsnActionType(ODTM_ACTION);
						        				mqEvent.setKsnTransId(kellyTransactionID);
						        				if(candidateGridService.getNodeColorWithMqEvent(mqEvent,null).equalsIgnoreCase("Yellow")){
						        					mqEvent.setTmInitiate(true);
						        				}else{
						        					mqEvent.setTmInitiate(false);
						        				}
						        				if(WF2_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINIT", locale))){
						        					mqEvent.setStatus("R");
						        					if(WF2_READY!=null && WF2_READY.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale)))
						        						mqEvent.setAckStatus("Success");
						        					else if(mqEventTemp!=null && mqEventTemp.getWorkFlowStatusId()!=null && mqEventTemp.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale)))
						        						mqEvent.setAckStatus("Success");
						        					else
						        						mqEvent.setAckStatus("Success");
						        				}else if(WF2_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))){
						        					mqEvent.setStatus("C");
						        					mqEvent.setAckStatus("Success");
						        				}else if(WF2_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale)) || WF2_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCANC", locale))){
						        					mqEvent.setStatus("R");
						        					mqEvent.setAckStatus("Failure");
						        				}else if(WF2_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblAWAIT", locale))){
						        					mqEvent.setStatus("R");
						        					mqEvent.setAckStatus("Success");
						        				} 

						        				mqEventsList=mqEventDAO.findMQEventByTeacher(teacherDetail);
						        				if(mqEventsList.size()>0){
						        					for (MQEvent mqEv : mqEventsList) {
						        						mapNodeStatus.put(mqEv.getTeacherdetail()+"-"+mqEv.getEventType() , mqEv);
						        					}
						        				}
						        				
						        				if(!(WF2_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale))) && !(WF2_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINIT", locale)))){
						        					try {
						        						statusNodeColorHistory = candidateGridService.saveStatusNodeColor(jobForTeacher,mqEvent,mapNodeStatus,latestNodeColorDateMap);
						        					} catch (Exception e) {
						        						e.printStackTrace();
						        					}
						        				}
						        				mqEventDAO.makePersistent(mqEvent);
						        				
						        				if(!(WF2_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale))) && !(WF2_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINIT", locale)))){
						        					try{

						        						String newColor = candidateGridService.getNodeColorWithMqEvent(mqEvent,null);
						        						System.out.println(" newColor :: "+newColor);
						        						statusNodeColorHistory.setCurrentColor(newColor);
						        						statusNodeColorHistory.setUpdateDateTime(new Date());
						        						statusNodeColorHistoryDAO.updatePersistent(statusNodeColorHistory);

						        					}catch(Exception ex){

						        					}
						        				}
						        			}else{
						        				if(updateMQEvent){
						        					mqEvent = new MQEvent();
						        					mqEvent.setWorkFlowStatusId(workFlowMap.get(WF2_STATUS.toUpperCase()));
						        					mqEvent.setKsnActionType(ODTM_ACTION);
						        					mqEvent.setKsnTransId(kellyTransactionID);
						        					mqEvent.setTeacherdetail(teacherDetail);
						        					mqEvent.setTmInitiate(false);
						        					mqEvent.setEventType(Utility.getLocaleValuePropByKey("lblINVWF2", locale));
						        					if(WF2_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINIT", locale))){
						        						mqEvent.setStatus("R");
						        						if(WF2_READY!=null && WF2_READY.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale)))
						        							mqEvent.setAckStatus("Success");
						        						else if(mqEventTemp!=null && mqEventTemp.getWorkFlowStatusId()!=null && mqEventTemp.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale)))
						        							mqEvent.setAckStatus("Success");
						        						else
						        							mqEvent.setAckStatus("Success");
						        					}else if(WF2_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))){
						        						mqEvent.setStatus("C");
						        						mqEvent.setAckStatus("Success");
						        					}else if(WF2_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale)) || WF2_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCANC", locale))){
						        						mqEvent.setStatus("R");
						        						mqEvent.setAckStatus("Failure");
						        					}else if(WF2_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblAWAIT", locale))){
						        						mqEvent.setStatus("R");
						        						mqEvent.setAckStatus("Success");
						        					}
						        					mqEvent.setCreatedDateTime(new Date());

						        					mqEventDAO.makePersistent(mqEvent);
						        					
						        					if(!(WF2_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale))) && !(WF2_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINIT", locale)))){
						        						try {
						        							statusNodeColorHistory = candidateGridService.saveStatusNodeColor(jobForTeacher,mqEvent,new HashMap<String, MQEvent>(),latestNodeColorDateMap);
						        						} catch (Exception e) {
						        							e.printStackTrace();
						        						}

						        						try{

						        							String newColor = candidateGridService.getNodeColorWithMqEvent(mqEvent,null);
						        							System.out.println(" newColor :: "+newColor);
						        							statusNodeColorHistory.setCurrentColor(newColor);
						        							statusNodeColorHistory.setUpdateDateTime(new Date());
						        							statusNodeColorHistoryDAO.updatePersistent(statusNodeColorHistory);

						        						}catch(Exception ex){

						        						}
						        					}
						        				}
						        			}
						        		}
						        		
						        		if(WF2_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINIT", locale))){
						        			try{
						        				resetWorkFlow(mqEventTemp);
						        			}catch(Exception ex){
						        				ex.printStackTrace();
						        			}
						        		}
						        		
						        		if(WF2_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale)))
						        			createHistoryForErrorWfxComNodes(jobForTeacher,mqEventTemp,mapNodeStatus,latestNodeColorDateMap,workFlowMap,ODTM_ACTION,kellyTransactionID,teacherDetail,Utility.getLocaleValuePropByKey("lblWF1COM", locale));

						        		
						        		if(WF2_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))){
						        			
						        			MQEvent mqETemp = mqEventMap.get(Utility.getLocaleValuePropByKey("lblWF2COM", locale));
						        			if(mqETemp!=null){
						        				try {
						        					//checkAndCreateWorkFlows(jobForTeacher,workFlowMap,mqEventMap, Utility.getLocaleValuePropByKey("lblWF2COM", locale),ODTM_ACTION,kellyTransactionID,teacherDetail,latestNodeColorDateMap);
						        				} catch (Exception e) {
						        					e.printStackTrace();
						        				}
						        			}
						        			try {
												mqService.statusUpdateForKellyWorkFlowNodes(Utility.getLocaleValuePropByKey("lblWF2COM", locale),teacherDetail);
											} catch (Exception e) {
												e.printStackTrace();
											}
											
											try {
												/*headQuarterMasterDAO = (HeadQuarterMasterDAO)context0.getBean("headQuarterMasterDAO");
												jobForTeacherDAO = (JobForTeacherDAO)context0.getBean("jobForTeacherDAO");
												userMasterDAO = (UserMasterDAO)context0.getBean("userMasterDAO");
												secondaryStatusDAO = (SecondaryStatusDAO)context0.getBean("secondaryStatusDAO");
												
						        				 System.out.println(":::::::::Inside WorkFlow >>autoCallQueue=>>:::::::::::::::");
						        				 HeadQuarterMaster headQuarterMaster = headQuarterMasterDAO.findById(1, false, false);
						        				 UserMaster userMaster = userMasterDAO.findById(1, false, false);
						        				 List<JobForTeacher> kellyAppliedJobs = new ArrayList<JobForTeacher>();
						        				 kellyAppliedJobs = jobForTeacherDAO.findByTeacherIdHQAndBranch(teacherDetail, headQuarterMaster, null);
						        				 JobForTeacher jft = kellyAppliedJobs.get(0);
						                         SecondaryStatus secondaryStatusWF3=secondaryStatusDAO.findSecondaryStatusObjByJobCategoryHeadAndBranch(jft.getJobId(),Utility.getLocaleValuePropByKey("lblINVWF3", locale));
						                         autoCallQueue(null,secondaryStatusWF3,jft,userMaster,context0);*/
											} catch (Exception e) {
												e.printStackTrace();
											}
											
						        		}
						        		else if(WF2_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINIT", locale))){
						        			if(mqEventTemp!=null && mqEventTemp.getWorkFlowStatusId()!=null && mqEventTemp.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))){
						        				mqEventTemp.setWorkFlowStatusId(null);
						        				mqEventDAO.makePersistent(mqEventTemp);
						        			}
						        		}
						        	}
						        	 
						        	if(WF3_READY != null){
						        		
						        		mqEvent = mqEventMap.get(Utility.getLocaleValuePropByKey("lblWF3COM", locale));
						        		if(mqEvent!=null){
						        			if(mqEvent.getWorkFlowStatusId()!=null && mqEvent.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale)))
						        				mailSend = false;
						        			else
						        				mailSend = true;
						        			UserMaster userMaster=userMap.get(mqEvent.getCreatedBy());
							        	    mqEvent.setWorkFlowStatusId(workFlowMap.get(WF3_READY.toUpperCase()));
							        	    mqEvent.setKsnActionType(ODTM_ACTION);
							        	    mqEvent.setKsnTransId(kellyTransactionID);
							        	    if(WF3_READY.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
							        	    	mqEvent.setStatus("C");
							        	    	mqEvent.setAckStatus("Success");
							        	    	if(mailSend)
							        	    		mqService.sendERegMailToCand(request,userMaster,jobForTeacher,mqEvent,"3");
							        	    }else if(WF3_READY.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblN", locale))){
							        	    	mqEvent.setStatus("R");
							        	    	mqEvent.setAckStatus("Failure");
							        	    }else if(WF3_READY == null){
							        	    	mqEvent.setStatus("R");
							        	    	mqEvent.setAckStatus("Failure");
							        	    } 
							        	    
							        	    
							        	    mqEventsList=mqEventDAO.findMQEventByTeacher(teacherDetail);
											 if(mqEventsList.size()>0){
												 for (MQEvent mqEv : mqEventsList) {
													 mapNodeStatus.put(mqEv.getTeacherdetail()+"-"+mqEv.getEventType() , mqEv);
												 }
											 }
							        	    
							        	    try {
												 statusNodeColorHistory = candidateGridService.saveStatusNodeColor(jobForTeacher,mqEvent,mapNodeStatus,latestNodeColorDateMap);
											 } catch (Exception e) {
												 e.printStackTrace();
											 }
							        	    
							        	    mqEventDAO.makePersistent(mqEvent);
							        	    
							        	    try{
							        	    	
							        	    	String newColor = candidateGridService.getNodeColorWithMqEvent(mqEvent,WF3_STATUS);
												   System.out.println(" newColor :: "+newColor);
												   statusNodeColorHistory.setCurrentColor(newColor);
												   statusNodeColorHistory.setUpdateDateTime(new Date());
												   statusNodeColorHistoryDAO.updatePersistent(statusNodeColorHistory);
							        	    	
							        	    }catch(Exception ex){
							        	    	
							        	    }
							        	    
						        		}else{
						        			if(updateMQEvent){
						        				mqEvent = new MQEvent();
								        	    mqEvent.setWorkFlowStatusId(workFlowMap.get(WF3_READY.toUpperCase()));
								        	    mqEvent.setKsnActionType(ODTM_ACTION);
								        	    mqEvent.setKsnTransId(kellyTransactionID);
								        	    mqEvent.setTeacherdetail(teacherDetail);
								        	    mqEvent.setEventType(Utility.getLocaleValuePropByKey("lblWF3COM", locale));
								        	    if(WF3_READY.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
								        	    	mqEvent.setStatus("C");
								        	    	mqEvent.setAckStatus("Success");
								        	    	mqService.sendERegMailToCand(request,null,jobForTeacher,mqEvent,"3");
								        	    }else if(WF3_READY.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblN", locale))){
								        	    	mqEvent.setStatus("R");
								        	    	mqEvent.setAckStatus("Failure");
								        	    }else if(WF3_READY == null){
								        	    	mqEvent.setStatus("R");
								        	    	mqEvent.setAckStatus("Failure");
								        	    } 
								        	    mqEvent.setCreatedDateTime(new Date());
								        	    
								        	    mqEventDAO.makePersistent(mqEvent);
								        	    
								        	    try {
													 statusNodeColorHistory = candidateGridService.saveStatusNodeColor(jobForTeacher,mqEvent,new HashMap<String, MQEvent>(),latestNodeColorDateMap);
												 } catch (Exception e) {
													 e.printStackTrace();
												 }
								        	    
								        	    try{
								        	    	
								        	    	String newColor = candidateGridService.getNodeColorWithMqEvent(mqEvent,WF3_STATUS);
													   System.out.println(" newColor :: "+newColor);
													   statusNodeColorHistory.setCurrentColor(newColor);
													   statusNodeColorHistory.setUpdateDateTime(new Date());
													   statusNodeColorHistoryDAO.updatePersistent(statusNodeColorHistory);
								        	    	
								        	    }catch(Exception ex){
								        	    	
								        	    }
								        	    
						        			}
						        		}
						        		
						        		if(WF3_READY.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
						        			Map<String,MQEvent> tempMap = new HashMap<String, MQEvent>();
						        			mqEvent = mqEventMap.get(Utility.getLocaleValuePropByKey("lblINVWF3", locale));
						        			if(mqEvent!=null){
						        				mqEvent.setStatus("R");
							        	    	mqEvent.setAckStatus("Success");
							        	    	
							        	    	mqEventsList=mqEventDAO.findMQEventByTeacher(teacherDetail);
												 if(mqEventsList.size()>0){
													 for (MQEvent mqEv : mqEventsList) {
														 if(mqEv.getEventType().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINVWF3", locale)))
															 tempMap.put(mqEv.getTeacherdetail()+"-"+mqEv.getEventType() , mqEv);
													 }
												 }
							        	    	
							        	    	try {
													 statusNodeColorHistory = candidateGridService.saveStatusNodeColor(jobForTeacher,mqEvent,tempMap,latestNodeColorDateMap);
												 } catch (Exception e) {
													 e.printStackTrace();
												 }
												 
								        	    mqEventDAO.makePersistent(mqEvent);
								        	    
								        	    try{
								        	    	String newColor = candidateGridService.getNodeColorWithMqEvent(mqEvent,WF1_STATUS);
								        	    	System.out.println(" newColor :: "+newColor);
								        	    	statusNodeColorHistory.setCurrentColor(newColor);
								        	    	 statusNodeColorHistory.setUpdateDateTime(new Date());
								        	    	statusNodeColorHistoryDAO.updatePersistent(statusNodeColorHistory);

								        	    }catch(Exception ex){ 
								        	    	ex.printStackTrace();
								        	    }
						        			}
						        			
						        			try {
												mqService.statusUpdateForKellyWorkFlowNodes(Utility.getLocaleValuePropByKey("lblINVWF3", locale),teacherDetail);
											} catch (Exception e) {
												e.printStackTrace();
											}
						        		}
			                         }
						        	
						        	if(WF3_STATUS != null){
						        		mqEvent = mqEventMap.get(Utility.getLocaleValuePropByKey("lblINVWF3", locale));
						        		MQEvent mqEventTemp = mqEventMap.get(Utility.getLocaleValuePropByKey("lblWF3COM", locale));
						        		if(WF3_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale)) 
						        				&& ((mqEvent==null && mqEventTemp==null) || (mqEvent==null && !(mqEventTemp!=null && mqEventTemp.getWorkFlowStatusId()!=null && mqEventTemp.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale)))) || !((mqEvent!=null && mqEvent.getWorkFlowStatusId()!=null && (mqEvent.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINIT", locale)) || mqEvent.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale)))))) && !(mqEventTemp!=null && mqEventTemp.getWorkFlowStatusId()!=null && mqEventTemp.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))))
						        		{
						        			try {
												triggerWorkFlowCOM(Utility.getLocaleValuePropByKey("lblWF3COM", locale), mqEventTemp, mqEventMap, userMap, workFlowMap, ODTM_ACTION, kellyTransactionID, teacherDetail, jobForTeacher, latestNodeColorDateMap,request,payloadRequest);
											} catch (Exception e) {
												e.printStackTrace();
											}
						        		}
						        		else{
						        			if(mqEvent!=null){
						        				mqEvent.setWorkFlowStatusId(workFlowMap.get(WF3_STATUS.toUpperCase()));
						        				mqEvent.setKsnActionType(ODTM_ACTION);
						        				mqEvent.setKsnTransId(kellyTransactionID);
						        				if(candidateGridService.getNodeColorWithMqEvent(mqEvent,null).equalsIgnoreCase("Yellow")){
						        					mqEvent.setTmInitiate(true);
						        				}else{
						        					mqEvent.setTmInitiate(false);
						        				}
						        				
						        				if(WF3_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINIT", locale))){
						        					mqEvent.setStatus("R");
						        					if(WF3_READY!=null && WF3_READY.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale)))
						        						mqEvent.setAckStatus("Success");
						        					else if(mqEventTemp!=null && mqEventTemp.getWorkFlowStatusId()!=null && mqEventTemp.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale)))
						        						mqEvent.setAckStatus("Success");
						        					else
						        						mqEvent.setAckStatus("Success");
						        				}else if(WF3_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))){
						        					mqEvent.setStatus("C");
						        					mqEvent.setAckStatus("Success");
						        				}else if(WF3_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale)) || WF3_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCANC", locale))){
						        					mqEvent.setStatus("R");
						        					mqEvent.setAckStatus("Failure");
						        				} 

						        				mqEventsList=mqEventDAO.findMQEventByTeacher(teacherDetail);
						        				if(mqEventsList.size()>0){
						        					for (MQEvent mqEv : mqEventsList) {
						        						mapNodeStatus.put(mqEv.getTeacherdetail()+"-"+mqEv.getEventType() , mqEv);
						        					}
						        				}
						        				
						        				if(!(WF3_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale))) && !(WF3_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINIT", locale)))){
						        					try {
						        						statusNodeColorHistory = candidateGridService.saveStatusNodeColor(jobForTeacher,mqEvent,mapNodeStatus,latestNodeColorDateMap);
						        					} catch (Exception e) {
						        						e.printStackTrace();
						        					}
						        				}

						        				mqEventDAO.makePersistent(mqEvent);
						        				
						        				if(!(WF3_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale))) && !(WF3_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINIT", locale)))){
						        					try{

						        						String newColor = candidateGridService.getNodeColorWithMqEvent(mqEvent,null);
						        						System.out.println(" newColor :: "+newColor);
						        						statusNodeColorHistory.setCurrentColor(newColor);
						        						statusNodeColorHistory.setUpdateDateTime(new Date());
						        						statusNodeColorHistoryDAO.updatePersistent(statusNodeColorHistory);

						        					}catch(Exception ex){

						        					}
						        				}
						        			}else{
						        				if(updateMQEvent){
						        					mqEvent = new MQEvent();
						        					mqEvent.setWorkFlowStatusId(workFlowMap.get(WF3_STATUS.toUpperCase()));
						        					mqEvent.setKsnActionType(ODTM_ACTION);
						        					mqEvent.setKsnTransId(kellyTransactionID);
						        					mqEvent.setTmInitiate(false);
						        					mqEvent.setTeacherdetail(teacherDetail);
						        					mqEvent.setEventType(Utility.getLocaleValuePropByKey("lblINVWF3", locale));
						        					if(WF3_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINIT", locale))){
						        						mqEvent.setStatus("R");
						        						if(WF3_READY!=null && WF3_READY.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale)))
						        							mqEvent.setAckStatus("Success");
						        						else if(mqEventTemp!=null && mqEventTemp.getWorkFlowStatusId()!=null && mqEventTemp.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale)))
						        							mqEvent.setAckStatus("Success");
						        						else
						        							mqEvent.setAckStatus("Success");
						        					}else if(WF3_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))){
						        						mqEvent.setStatus("C");
						        						mqEvent.setAckStatus("Success");
						        					}else if(WF3_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale)) || WF3_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCANC", locale))){
						        						mqEvent.setStatus("R");
						        						mqEvent.setAckStatus("Failure");
						        					} 
						        					mqEvent.setCreatedDateTime(new Date());

						        					mqEventDAO.makePersistent(mqEvent);

						        					try {
						        						statusNodeColorHistory = candidateGridService.saveStatusNodeColor(jobForTeacher,mqEvent,new HashMap<String, MQEvent>(),latestNodeColorDateMap);
						        					} catch (Exception e) {
						        						e.printStackTrace();
						        					}

						        					try{

						        						String newColor = candidateGridService.getNodeColorWithMqEvent(mqEvent,null);
						        						System.out.println(" newColor :: "+newColor);
						        						statusNodeColorHistory.setCurrentColor(newColor);
						        						statusNodeColorHistory.setUpdateDateTime(new Date());
						        						statusNodeColorHistoryDAO.updatePersistent(statusNodeColorHistory);

						        					}catch(Exception ex){

						        					}

						        				}
						        			}
						        		}
						        		
						        		if(WF3_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINIT", locale))){
						        			try{
						        				resetWorkFlow(mqEventTemp);
						        			}catch(Exception ex){
						        				ex.printStackTrace();
						        			}
						        		}						        		
						        		
						        		if(WF3_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale)))
						        			createHistoryForErrorWfxComNodes(jobForTeacher,mqEventTemp,mapNodeStatus,latestNodeColorDateMap,workFlowMap,ODTM_ACTION,kellyTransactionID,teacherDetail,Utility.getLocaleValuePropByKey("lblWF1COM", locale));

						        		
						        		if(WF3_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))){
						        			MQEvent mqETemp = mqEventMap.get(Utility.getLocaleValuePropByKey("lblWF3COM", locale));
						        			if(mqETemp!=null){
						        				try {
						        						//checkAndCreateWorkFlows(jobForTeacher,workFlowMap,mqEventMap, Utility.getLocaleValuePropByKey("lblWF3COM", locale),ODTM_ACTION,kellyTransactionID,teacherDetail,latestNodeColorDateMap);
						        				} catch (Exception e) {
						        					e.printStackTrace();
						        				}
						        			}
						        			try {
												mqService.statusUpdateForKellyWorkFlowNodes(Utility.getLocaleValuePropByKey("lblWF3COM", locale),teacherDetail);
											} catch (Exception e) {
												e.printStackTrace();
											}
						        		}
						        		else if(WF3_STATUS.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINIT", locale))){
						        			if(mqEventTemp!=null && mqEventTemp.getWorkFlowStatusId()!=null && mqEventTemp.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))){
						        				mqEventTemp.setWorkFlowStatusId(null);
						        				mqEventDAO.makePersistent(mqEventTemp);
						        			}
						        		}
			                         }
						        	if(backGroundCheck != null){
						    			mqEvent = mqEventMap.get(Utility.getLocaleValuePropByKey("lblScreening", locale));
						        		if(mqEvent!=null){
							        	    mqEvent.setWorkFlowStatusId(workFlowMap.get(backGroundCheck.toUpperCase()));
							        	    mqEvent.setKsnActionType(ODTM_ACTION);
							        	    mqEvent.setKsnTransId(kellyTransactionID);
							        		StatusMaster statusMaster= WorkThreadServlet.statusMap.get("vcomp");
						        	    	mqEvent.setStatusMaster(statusMaster);
							        	    if(backGroundCheck.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblFAVO", locale))){
							        	    	mqService.statusUpdateForKellyWorkFlowNodes(Utility.getLocaleValuePropByKey("lblScreening", locale),teacherDetail);
							        	    	mqEvent.setStatus("C");
							        	    }else{
							        	    	try {
													commonService.resetStatusForJobs(null,teacherDetail,null,Utility.getLocaleValuePropByKey("lblScreening", locale));
												} catch (Exception e) {
													// TODO Auto-generated catch block
													e.printStackTrace();
												}
							        	    	mqEvent.setStatus("R");
							        	    }
							        	    if(backGroundCheck.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblUNFA", locale))){
							        	    	 mqEvent.setAckStatus("Failure");
							        	    }else{
							        	    	 mqEvent.setAckStatus("Success");
							        	    }
							        	   
							        	    mqEventsList=mqEventDAO.findMQEventByTeacher(teacherDetail);
											 if(mqEventsList.size()>0){
												 for (MQEvent mqEv : mqEventsList) {
													 mapNodeStatus.put(mqEv.getTeacherdetail()+"-"+mqEv.getEventType() , mqEv);
												 }
											 }
							        	    
							        	    try {
												 statusNodeColorHistory = candidateGridService.saveStatusNodeColor(jobForTeacher,mqEvent,mapNodeStatus,latestNodeColorDateMap);
											 } catch (Exception e) {
												 e.printStackTrace();
											 }
							        	    
							        	    mqEventDAO.makePersistent(mqEvent);
							        	    
							        	    try{
							        	    	
							        	    	String newColor = candidateGridService.getNodeColorWithMqEvent(mqEvent,null);
												   System.out.println(" newColor :: "+newColor);
												   statusNodeColorHistory.setCurrentColor(newColor);
												   statusNodeColorHistory.setUpdateDateTime(new Date());
												   statusNodeColorHistoryDAO.updatePersistent(statusNodeColorHistory);
							        	    	
							        	    }catch(Exception ex){
							        	    	
							        	    }
							        	    
						        		}else{
						        			if(updateMQEvent){
						        				mqEvent = new MQEvent();
								        	    mqEvent.setWorkFlowStatusId(workFlowMap.get(backGroundCheck.toUpperCase()));
								        	    mqEvent.setKsnActionType(ODTM_ACTION);
								        	    mqEvent.setKsnTransId(kellyTransactionID);
								        	    mqEvent.setTeacherdetail(teacherDetail);
								        	    mqEvent.setEventType(Utility.getLocaleValuePropByKey("lblScreening", locale));
								        		StatusMaster statusMaster= WorkThreadServlet.statusMap.get("vcomp");
							        	    	mqEvent.setStatusMaster(statusMaster);
								        	    if(backGroundCheck.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblFAVO", locale))){
								        	    	mqService.statusUpdateForKellyWorkFlowNodes(Utility.getLocaleValuePropByKey("lblScreening", locale),teacherDetail);
								        	    	mqEvent.setStatus("C");
								        	    }else{
								        	    	mqEvent.setStatus("R");
								        	    }
								        	    if(backGroundCheck.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblUNFA", locale))){
								        	    	 mqEvent.setAckStatus("Failure");
								        	    }else{
								        	    	 mqEvent.setAckStatus("Success");
								        	    }
								        	    mqEvent.setCreatedDateTime(new Date());
								        	    
								        	    mqEventDAO.makePersistent(mqEvent);
								        	    
								        	    try {
													 statusNodeColorHistory = candidateGridService.saveStatusNodeColor(jobForTeacher,mqEvent,new HashMap<String, MQEvent>(),latestNodeColorDateMap);
												 } catch (Exception e) {
													 e.printStackTrace();
												 }
								        	    
								        	    try{
								        	    	
								        	    	String newColor = candidateGridService.getNodeColorWithMqEvent(mqEvent,null);
													   System.out.println(" newColor :: "+newColor);
													   statusNodeColorHistory.setCurrentColor(newColor);
													   statusNodeColorHistory.setUpdateDateTime(new Date());
													   statusNodeColorHistoryDAO.updatePersistent(statusNodeColorHistory);
								        	    	
								        	    }catch(Exception ex){
								        	    	
								        	    }
								        	    
						        			}
						        		}
						    		}
					        		
					        		
									System.out.println("talentType:::::::::::::::::::::::::::"+talentType);
						        	if(talentType != null){
						    			mqEvent = mqEventMap.get(Utility.getLocaleValuePropByKey("msgHired2", locale));
						    			StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hird");
						        		if(mqEvent!=null){
						        			mqEvent.setStatusMaster(statusMaster);
							        	    mqEvent.setWorkFlowStatusId(workFlowMap.get(talentType.toUpperCase()));
							        	    mqEvent.setKsnActionType(ODTM_ACTION);
							        	    mqEvent.setKsnTransId(kellyTransactionID);
							        	    if(talentType.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblHIRED", locale))){
							        	    	mqEvent.setStatus("C");
							        	    	mqEvent.setAckStatus("Success");
							        	    }else{
							        	    	mqEvent.setStatus("R");
							        	    	mqEvent.setAckStatus("Success");
							        	    	try {
													commonService.resetStatusForJobs(null,teacherDetail,null,null);
												} catch (Exception e) {
													// TODO Auto-generated catch block
													e.printStackTrace();
												}
							        	    }
							        	   
							        	    
							        	    mqEventsList=mqEventDAO.findMQEventByTeacher(teacherDetail);
											 if(mqEventsList.size()>0){
												 for (MQEvent mqEv : mqEventsList) {
													 mapNodeStatus.put(mqEv.getTeacherdetail()+"-"+mqEv.getEventType() , mqEv);
												 }
											 }
							        	    
							        	    try {
												 statusNodeColorHistory = candidateGridService.saveStatusNodeColor(jobForTeacher,mqEvent,mapNodeStatus,latestNodeColorDateMap);
											 } catch (Exception e) {
												 e.printStackTrace();
											 }
							        	    
							        	    mqEventDAO.makePersistent(mqEvent);
							        	    
							        	    try{
							        	    	
							        	    	String newColor = candidateGridService.getNodeColorWithMqEvent(mqEvent,null);
												   System.out.println(" newColor :: "+newColor);
												   statusNodeColorHistory.setCurrentColor(newColor);
												   statusNodeColorHistory.setUpdateDateTime(new Date());
												   statusNodeColorHistoryDAO.updatePersistent(statusNodeColorHistory);
							        	    	
							        	    }catch(Exception ex){
							        	    	
							        	    }
							        	    
						        		}else{
						        			if(updateMQEvent){
						        				mqEvent = new MQEvent();
								        	    mqEvent.setWorkFlowStatusId(workFlowMap.get(talentType.toUpperCase()));
								        	    mqEvent.setKsnActionType(ODTM_ACTION);
								        	    mqEvent.setKsnTransId(kellyTransactionID);
								        	    mqEvent.setTeacherdetail(teacherDetail);
								        	    mqEvent.setEventType(Utility.getLocaleValuePropByKey("msgHired2", locale));
							        			mqEvent.setStatusMaster(statusMaster);
								        	    if(talentType.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblHIRED", locale))){
								        	    	mqEvent.setStatus("C");
								        	    	mqEvent.setAckStatus("Success");
								        	    }else{
								        	    	mqEvent.setStatus("R");
								        	    	mqEvent.setAckStatus("Success");
								        	    }
								        	    mqEvent.setCreatedDateTime(new Date()); 
								        	    
								        	    mqEventDAO.makePersistent(mqEvent);
								        	    
								        	    try {
													 statusNodeColorHistory = candidateGridService.saveStatusNodeColor(jobForTeacher,mqEvent,new HashMap<String, MQEvent>(),latestNodeColorDateMap);
												 } catch (Exception e) {
													 e.printStackTrace();
												 }
								        	    
								        	    try{
								        	    	
								        	    	String newColor = candidateGridService.getNodeColorWithMqEvent(mqEvent,null);
													   System.out.println(" newColor :: "+newColor);
													   statusNodeColorHistory.setCurrentColor(newColor);
													   statusNodeColorHistory.setUpdateDateTime(new Date());
													   statusNodeColorHistoryDAO.updatePersistent(statusNodeColorHistory);
								        	    	
								        	    }catch(Exception ex){
								        	    	
								        	    }
								        	    
						        			}
						        		}
						        		
						        		if(talentType.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblHIRED", locale))){
						        			try {
												mqService.statusUpdateForKellyWorkFlowNodes(Utility.getLocaleValuePropByKey("msgHired2", locale),teacherDetail);
											} catch (Exception e) {
												e.printStackTrace();
											}
						        		}
						    		}
						    		if(talentStatus != null){
						    			mqEvent = mqEventMap.get(Utility.getLocaleValuePropByKey("msgTalentStatus", locale));
						        		if(mqEvent!=null){
							        	    mqEvent.setWorkFlowStatusId(workFlowMap.get(talentStatus.toUpperCase()));
							        	    mqEvent.setKsnActionType(ODTM_ACTION);
							        	    mqEvent.setKsnTransId(kellyTransactionID);
							        	    if(talentStatus.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINACTIVEELIG", locale))){
							        	    	StatusMaster statusMaster= WorkThreadServlet.statusMap.get("ielig");
							        	    	mqEvent.setStatus("C");
							        	    	mqEvent.setStatusMaster(statusMaster);
							        	    	mqService.statusUpdateForKellyWorkFlowNodes(Utility.getLocaleValuePropByKey("lblInElig", locale),teacherDetail);
							        	    }else  if(talentStatus.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINACTIVENELIG", locale))){
							        	    	StatusMaster statusMaster= WorkThreadServlet.statusMap.get("iielig");
							        	    	mqEvent.setStatus("C");
							        	    	mqEvent.setStatusMaster(statusMaster);
							        	    	mqService.statusUpdateForKellyWorkFlowNodes(Utility.getLocaleValuePropByKey("lblInInlig", locale),teacherDetail);
							        	    }else if(talentStatus.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblFlowACTIVE", locale))){
							        	    	mqEvent.setStatus("R");
							        	    	try {
													commonService.resetStatusForJobs(null,teacherDetail,null,null);
												} catch (Exception e) {
													// TODO Auto-generated catch block
													e.printStackTrace();
												}
							        	    }else{
							        	    	mqEvent.setStatus(null);
							        	    	mqEvent.setStatusMaster(null);
							        	    }
							        	    mqEvent.setAckStatus("Success");
							         	   
							        	    mqEventsList=mqEventDAO.findMQEventByTeacher(teacherDetail);
											 if(mqEventsList.size()>0){
												 for (MQEvent mqEv : mqEventsList) {
													 mapNodeStatus.put(mqEv.getTeacherdetail()+"-"+mqEv.getEventType() , mqEv);
												 }
											 }
							        	    
							        	    try {
												 statusNodeColorHistory = candidateGridService.saveStatusNodeColor(jobForTeacher,mqEvent,mapNodeStatus,latestNodeColorDateMap);
											 } catch (Exception e) {
												 e.printStackTrace();
											 }
							        	    
							        	    mqEventDAO.makePersistent(mqEvent);
							        	    
							        	    try{
							        	    	
							        	    	String newColor = candidateGridService.getNodeColorWithMqEvent(mqEvent,null);
												   System.out.println(" newColor :: "+newColor);
												   statusNodeColorHistory.setCurrentColor(newColor);
												   statusNodeColorHistory.setUpdateDateTime(new Date());
												   statusNodeColorHistoryDAO.updatePersistent(statusNodeColorHistory);
							        	    	
							        	    }catch(Exception ex){
							        	    	
							        	    }
							        	    
						        	    }else{
						        	    	if(updateMQEvent){
						        	    		mqEvent = new MQEvent();
								        	    mqEvent.setWorkFlowStatusId(workFlowMap.get(talentStatus.toUpperCase()));
								        	    mqEvent.setKsnActionType(ODTM_ACTION);
								        	    mqEvent.setKsnTransId(kellyTransactionID);
								        	    mqEvent.setTeacherdetail(teacherDetail);
								        	    mqEvent.setEventType(Utility.getLocaleValuePropByKey("msgTalentStatus", locale));
								        	    if(talentStatus.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINACTIVEELIG", locale))){
								        	    	StatusMaster statusMaster= WorkThreadServlet.statusMap.get("ielig");
								        	    	mqEvent.setStatus("C");
								        	    	mqEvent.setStatusMaster(statusMaster);
								        	    }else  if(talentStatus.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINACTIVENELIG", locale))){
								        	    	StatusMaster statusMaster= WorkThreadServlet.statusMap.get("iielig");
								        	    	mqEvent.setStatus("C");
								        	    	mqEvent.setStatusMaster(statusMaster);
								        	    }else{
								        	    	mqEvent.setStatus(null);
								        	    	mqEvent.setStatusMaster(null);
								        	    }
								        	    mqEvent.setAckStatus("Success");
								        	    mqEvent.setCreatedDateTime(new Date());
								        	   
								        	    mqEventDAO.makePersistent(mqEvent);
								        	    
								        	    try {
													 statusNodeColorHistory = candidateGridService.saveStatusNodeColor(jobForTeacher,mqEvent,new HashMap<String, MQEvent>(),latestNodeColorDateMap);
												 } catch (Exception e) {
													 e.printStackTrace();
												 }
								        	    
								        	    try{
								        	    	
								        	    	String newColor = candidateGridService.getNodeColorWithMqEvent(mqEvent,null);
													   System.out.println(" newColor :: "+newColor);
													   statusNodeColorHistory.setCurrentColor(newColor);
													   statusNodeColorHistory.setUpdateDateTime(new Date());
													   statusNodeColorHistoryDAO.updatePersistent(statusNodeColorHistory);
								        	    	
								        	    }catch(Exception ex){
								        	    	
								        	    }
								        	    
						        	    	}
						        	    }
						        	}
						    		
					        		System.out.println("-------------mqEvent----Just check object------"+mqEvent);
					        		if(mqEvent != null && mqEvent.getEventType() != null){
							        	mqEventHistory = new MQEventHistory();
										mqEventHistory.setMqEvent(mqEvent);
										mqEventHistory.setCreatedBy(1);
										mqEventHistory.setCreatedDateTime(new Date());
										mqEventHistory.setIpAddress(request.getRemoteAddr());
										mqEventHistory.setXmlObject(payloadRequest);
										mqEventHistoryDAO.makePersistent(mqEventHistory);
						        	}
					    		
						        }
						        if(dissConnection){
						        	List<TeacherStatusHistoryForJob> lstHist = null;
						        	List<TeacherStatusHistoryForJob> lstExHist = null;
						        	Map<String,List<TeacherStatusHistoryForJob>> jobWiseExcludeStatus = new HashMap<String,List<TeacherStatusHistoryForJob>>();
						        	Map<String,List<TeacherStatusHistoryForJob>> teacherJobWiseStatus = new HashMap<String, List<TeacherStatusHistoryForJob>>();
						        	List<TeacherStatusHistoryForJob> listHistJobs = null;
						        	Map<Integer,StatusMaster> parentStatus = new HashMap<Integer,StatusMaster>();
						        	List<MQEvent> mqEvents = null;
						        	try {
						        		System.out.println("dissConnection--------"+dissConnection);
						        		headQuarterMasterDAO = (HeadQuarterMasterDAO)context0.getBean("headQuarterMasterDAO");
						        		jobForTeacherDAO = (JobForTeacherDAO)context0.getBean("jobForTeacherDAO");
						        		statusMasterDAO = (StatusMasterDAO)context0.getBean("statusMasterDAO");
						        		teacherStatusNotesDAO = (TeacherStatusNotesDAO)context0.getBean("teacherStatusNotesDAO");
						        		teacherDetailDAO = (TeacherDetailDAO)context0.getBean("teacherDetailDAO");
						        		assessmentDetailDAO = (AssessmentDetailDAO)context0.getBean("assessmentDetailDAO");
						        		statusNodeColorHistoryDAO = (StatusNodeColorHistoryDAO)context0.getBean("statusNodeColorHistoryDAO");
						        		teacherStatusHistoryForJobDAO = (TeacherStatusHistoryForJobDAO)context0.getBean("teacherStatusHistoryForJobDAO");
						        		messageToTeacherDAO = (MessageToTeacherDAO)context0.getBean("messageToTeacherDAO");
						        		
						        		HeadQuarterMaster headQuarterMaster = headQuarterMasterDAO.findById(1, false, false);
						        		
						        		//mqEvents = mqEventDAO.findMQEventForWFStatusByTeacher(teacherDetail);
						        		List<MQEvent> teacherDat = mqEventDAO.findMQEventByTeacher_Op(teachers.get(0));
						        		List<MQEvent> mqEventDt = mqEventDAO.findEventByEventType(teachers.get(0), Utility.getLocaleValuePropByKey("msgDisconnectTalent", locale));
						        		
						        		Transaction txOpen =statelesSsession.beginTransaction();
						        		List<JobForTeacher> kellyAppliedJobs = new ArrayList<JobForTeacher>();


						        		// Available, Smart Practices, Withdrawn
						        		List<JobOrder> listJobs = new ArrayList<JobOrder>();
						        		List<JobCategoryMaster> jobCategories = new ArrayList<JobCategoryMaster>();
						        		kellyAppliedJobs = jobForTeacherDAO.findByTeacherIdHQAndBranch(teacherDetail, headQuarterMaster, null);

						        		UserMaster userMaster = new UserMaster();
						        		userMaster.setUserId(1);
						        		
						        		Boolean anyStatusReset = false; 
						        		
						        		if(kellyAppliedJobs.size()>0){

						        			// get all parent category of all applied jobs
						        			for(JobForTeacher jft : kellyAppliedJobs){
						        				listJobs.add(jft.getJobId());
						        				if(jft.getJobId().getHeadQuarterMaster()!=null && jft.getJobId().getJobCategoryMaster().getParentJobCategoryId()!=null){
						        					jobCategories.add(jft.getJobId().getJobCategoryMaster().getParentJobCategoryId());
						        					System.out.println(" jft.getJobId().getJobCategoryMaster().getParentJobCategoryId() :: "+jft.getJobId().getJobCategoryMaster().getParentJobCategoryId().getJobCategoryId());
						        				}
						        			}

						        			secondaryStatusDAO = (SecondaryStatusDAO)context0.getBean("secondaryStatusDAO");
						        			List<SecondaryStatus> secondaryStatuses =null;
						        			if(jobCategories!=null && jobCategories.size()>0)
						        				secondaryStatuses = secondaryStatusDAO.findByNameAndJobCats(Utility.getLocaleValuePropByKey("lblSPStatus", locale),jobCategories);

						        			if(secondaryStatuses!=null && secondaryStatuses.size()>0)
						        				for(SecondaryStatus st : secondaryStatuses){
						        					if(st.getStatusMaster()!=null){
						        						if(st.getSecondaryStatusName().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblSPStatus", locale))){
						        							parentStatus.put(st.getJobCategoryMaster().getJobCategoryId(),st.getStatusMaster());
						        						}
						        					}
						        				}

						        			// get statuses of all jobs for witdraw node
						        			StatusMaster statusMaster = WorkThreadServlet.statusMap.get("widrw");
						        			listHistJobs = teacherStatusHistoryForJobDAO.findJobByTeachersWithHeadQuarter(teacherDetail, listJobs);

						        			String keyVal ="";
						        			Integer parentCatId = 0;
						        			if(listHistJobs!=null && listHistJobs.size()>0)
						        				for(TeacherStatusHistoryForJob ts : listHistJobs){
						        					keyVal = ts.getTeacherDetail().getTeacherId()+"###"+ts.getJobOrder().getJobId();
						        					lstHist = teacherJobWiseStatus.get(keyVal);
						        					lstExHist = jobWiseExcludeStatus.get(keyVal);

						        					if(ts.getJobOrder().getJobCategoryMaster().getParentJobCategoryId()!=null)
						        						parentCatId = ts.getJobOrder().getJobCategoryMaster().getParentJobCategoryId().getJobCategoryId();


						        					if(lstHist==null){
						        						lstHist = new ArrayList<TeacherStatusHistoryForJob>();
						        						lstHist.add(ts);
						        						teacherJobWiseStatus.put(keyVal, lstHist);							    			
						        					}
						        					else{
						        						lstHist.add(ts);
						        						teacherJobWiseStatus.put(keyVal, lstHist);
						        					}

						        					if(lstExHist==null){
						        						lstExHist = new ArrayList<TeacherStatusHistoryForJob>();
						        						if(ts.getSecondaryStatus()!=null && ts.getSecondaryStatus().getSecondaryStatusName().equals(Utility.getLocaleValuePropByKey("lblSPStatus", locale))){
						        							lstExHist.add(ts);
						        							jobWiseExcludeStatus.put(keyVal, lstExHist);
						        						}
						        						else if(parentStatus.get(parentCatId)!=null){
						        							lstExHist.add(ts);
						        							jobWiseExcludeStatus.put(keyVal, lstExHist);
						        						}
						        						else if(ts.getStatusMaster()!=null && ts.getStatusMaster().getStatusId().equals(statusMaster.getStatusId())){
						        							lstExHist.add(ts);
						        							jobWiseExcludeStatus.put(keyVal, lstExHist);
						        						}
						        					}
						        					else{
						        						if(ts.getSecondaryStatus()!=null && ts.getSecondaryStatus().getSecondaryStatusName().equals(Utility.getLocaleValuePropByKey("lblSPStatus", locale))){
						        							lstExHist.add(ts);
						        							jobWiseExcludeStatus.put(keyVal, lstExHist);
						        						}
						        						else if(parentStatus.get(parentCatId)!=null){
						        							lstExHist.add(ts);
						        							jobWiseExcludeStatus.put(keyVal, lstExHist);
						        						}
						        						else if(ts.getStatusMaster()!=null && ts.getStatusMaster().getStatusId().equals(statusMaster.getStatusId())){
						        							lstExHist.add(ts);
						        							jobWiseExcludeStatus.put(keyVal, lstExHist);
						        						}
						        					}
						        				}


						        			if(kellyAppliedJobs.size()>0){
						        				String key = "";
						        				
						        				for(JobForTeacher jft : kellyAppliedJobs){
						        					SecondaryStatus sec = null;
						        					StatusMaster status = null;
						        					boolean statusReset = false;
						        					List<TeacherStatusHistoryForJob> tshj = null;
						        					key = jft.getTeacherId().getTeacherId()+"###"+jft.getJobId().getJobId();
						        					if(jft.getJobId().getJobCategoryMaster().getParentJobCategoryId()!=null)
						        						parentCatId = jft.getJobId().getJobCategoryMaster().getParentJobCategoryId().getJobCategoryId();
						        					// jobforteacher status reset
						        					tshj = teacherJobWiseStatus.get(key);
						        					if(tshj!=null && tshj.size()>0){
						        						Integer statusId = 0;
						        						for(TeacherStatusHistoryForJob ts : tshj){
						        							statusId = 0;
						        							if(parentStatus.get(parentCatId)!=null)
						        								statusId = parentStatus.get(parentCatId).getStatusId();

						        							if(ts.getSecondaryStatus()!=null  &&  (ts.getSecondaryStatus().getSecondaryStatusName().equals(Utility.getLocaleValuePropByKey("lblSPStatus", locale)))){
						        								sec = ts.getSecondaryStatus();
						        								break;
						        							}
						        							else if(ts.getStatusMaster()!=null && (ts.getStatusMaster().getStatusId().equals(statusId) || (ts.getStatusMaster().getStatusShortName().equals("widrw") || ts.getStatusMaster().getStatusShortName().equals("hired")))){
						        								status = ts.getStatusMaster();
						        								break;
						        							}
						        						}
						        					}
						        					if(jft.getStatus().getStatusShortName().equalsIgnoreCase("widrw")){
						        						jft.setStatusMaster(jft.getStatus());
						        						jft.setSecondaryStatus(null);
						        						statusReset = true;
						        					}
						        					else if(status!=null){
						        						jft.setStatus(status);
						        						jft.setStatusMaster(status);
						        						jft.setSecondaryStatus(null);
						        						statusReset = true;
						        					}
						        					else if(sec!=null){
						        						jft.setStatus(statusMaster);
						        						jft.setStatusMaster(null);
						        						jft.setSecondaryStatus(sec);
						        						statusReset = true;
						        					}
						        					else if(sec==null && status==null){
						        						try{
						        							int[] flagList=assessmentDetailDAO.getInternalTeacherFalgs(jft) ;
						        							statusMaster = assessmentDetailDAO.findByTeacherIdJobStaus(jft,flagList);
						        						}catch(Exception ex){
						        							statusMaster = WorkThreadServlet.statusMap.get("icomp");
						        						}
						        						jft.setStatus(statusMaster);
						        						jft.setStatusMaster(statusMaster);
						        						jft.setSecondaryStatus(null);
						        						statusReset = true;
						        					}

						        					tshj = teacherJobWiseStatus.get(key);
						        					if(tshj!=null && tshj.size()>0){
						        						Integer statusId = 0;
						        						for(TeacherStatusHistoryForJob ts : tshj){
						        							statusId = 0;
						        							parentCatId = ts.getJobOrder().getJobCategoryMaster().getParentJobCategoryId().getJobCategoryId();
						        							if(parentStatus.get(parentCatId)!=null)
						        								statusId = parentStatus.get(parentCatId).getStatusId();

						        							if(ts.getSecondaryStatus()!=null  &&  !(ts.getSecondaryStatus().getSecondaryStatusName().equals(Utility.getLocaleValuePropByKey("lblSPStatus", locale)))){
						        								ts.setStatus("I");
						        								System.out.println(" Sec Status Inactive :: "+ts.getSecondaryStatus().getSecondaryStatusId());
						        							}
						        							else if((ts.getStatusMaster()!=null && !ts.getStatusMaster().getStatusId().equals(statusId)) && !(ts.getStatusMaster().getStatusShortName().equals("widrw"))){
						        								ts.setStatus("I");
						        								System.out.println(" Status Inactive :: "+ts.getStatusMaster().getStatusId());
						        							}
						        							//teacherStatusHistoryForJobDAO.makePersistent(ts);
						        							statelesSsession.update(ts);
						        						}
						        					}



						        					if(statusReset){
						        						anyStatusReset = true;
						        						TeacherStatusNotes teacherStatusNoteObj=new TeacherStatusNotes();
						        						teacherStatusNoteObj.setTeacherDetail(jft.getTeacherId());
						        						teacherStatusNoteObj.setJobOrder(jft.getJobId());
						        						if(status!=null)
						        							teacherStatusNoteObj.setStatusMaster(status);
						        						if(sec!=null){
						        							teacherStatusNoteObj.setSecondaryStatus(sec);
						        						}
						        						teacherStatusNoteObj.setUserMaster(userMaster);
						        						teacherStatusNoteObj.setStatusNotes(Utility.getLocaleValuePropByKey("msgDisconnectTalent", locale));
						        						teacherStatusNoteObj.setEmailSentTo(0);
						        						teacherStatusNoteObj.setFinalizeStatus(true);
						        						statelesSsession.insert(teacherStatusNoteObj);
						        					}

						        					//jobForTeacherDAO.updatePersistent(jft);
						        					statelesSsession.update(jft);
						        				}
						        			}
						        		}
						        		
						        		teacherDetail.setKSNID(null);
						        		statelesSsession.update(teacherDetail);
						        		//teacherDetailDAO.makePersistent(teacherDetail);
						        		txOpen.commit();
				        				statelesSsession.close();
						        		
						        		// reset all mqevent data for teacher
										MQEvent tempLinkToKSNEvent =null;
										try {
											if(teacherDat!=null && teacherDat.size()>0){
												for(MQEvent m : teacherDat){
													if(!m.getEventType().equalsIgnoreCase(Utility.getLocaleValuePropByKey("msgDisconnectTalent", locale))){
														m.setAckStatus(null);
														m.setStatus(null);
														m.setWorkFlowStatusId(null);
														mqEventDAO.updatePersistent(m);
														//statelesSsession.update(m);
													}
													if(m.getEventType().equalsIgnoreCase(Utility.getLocaleValuePropByKey("msgDisconnectTalent", locale)))
														tempLinkToKSNEvent = m;
												}
											}
											// end reset
										} catch (Exception e1) {
											e1.printStackTrace();
										}
										
						        		if(mqEventDt==null || mqEventDt.size()==0){
						        			mqEvent = new MQEvent();
						        			mqEvent.setStatus("C");
						        			mqEvent.setKsnTransId(kellyTransactionID);
						        			mqEvent.setEventType(Utility.getLocaleValuePropByKey("msgDisconnectTalent", locale));
						        			mqEvent.setTeacherdetail(teachers.get(0));
						        			mqEvent.setCreatedDateTime(new Date());
						        			//statelesSsession.insert(mqEvent);
						        			mqEventDAO.makePersistent(mqEvent);
						        			
						        			mqEventHistory = new MQEventHistory();
			        						mqEventHistory.setMqEvent(mqEvent);
			        						mqEventHistory.setCreatedBy(1);
			        						mqEventHistory.setCreatedDateTime(new Date());
			        						mqEventHistory.setIpAddress(request.getRemoteAddr());
			        						mqEventHistory.setXmlObject(payloadRequest);
			        						//statelesSsession.insert(mqEventHistory);
			        						mqEventHistoryDAO.makePersistent(mqEventHistory);
						        		}
		        						
		        						try {
											StatusNodeColorHistory staColorHistory = new StatusNodeColorHistory();
											staColorHistory.setCreatedDateTime(new Date());
											staColorHistory.setHeadQuarterMaster(headQuarterMaster);
											staColorHistory.setIpAddress(IPAddressUtility.getIpAddress(request));
											staColorHistory.setCurrentColor("Grey");
											staColorHistory.setPrevColor("Grey");
											staColorHistory.setTeacherDetail(teachers.get(0));
											if(tempLinkToKSNEvent!=null)
												staColorHistory.setMqEvent(tempLinkToKSNEvent);
											else
												staColorHistory.setMqEvent(mqEvent);
												
											statusNodeColorHistoryDAO.makePersistent(staColorHistory);
										} catch (Exception e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}

		        						if(anyStatusReset){
		        							MessageToTeacher messageToTeacher = new MessageToTeacher();
		        							messageToTeacher.setEntityType(5);
		        							messageToTeacher.setTeacherId(teacherDetail);
		        							messageToTeacher.setTeacherEmailAddress(teacherDetail.getEmailAddress());
		        							messageToTeacher.setCreatedDateTime(new Date());
		        							messageToTeacher.setMessageSubject(Utility.getLocaleValuePropByKey("msgDisconnectTalent", locale));
		        							messageToTeacher.setSenderId(userMaster);
		        							messageToTeacher.setSenderEmailAddress("");
		        							messageToTeacher.setMessageSend(Utility.getLocaleValuePropByKey("msgDisconnectTalent", locale));
		        							//statelesSsession.insert(messageToTeacher);
		        							messageToTeacherDAO.makePersistent(messageToTeacher);
		        						}
		        						
						        		
						        		//statelesSsession.update(teacherDetail);
				        				
				        				/*if(mqEvents!=null && mqEvents.size()>0){
				        					for(MQEvent mq : mqEvents){
				        						mq.setStatus(null);
				        						mq.setWorkFlowStatusId(null);
				        						statelesSsession.update(mq);
				        					}
				        				}*/

				        				
				        				
						        	} catch (Exception e) {
						        		e.printStackTrace();

						        	}
						        }
					 }
			          
					 System.out.println(" >>>>>>>  teacherId ::::: "+tID);
					 System.out.println(" >>>>>>>  KSNID     ::::: "+KSNID);
					 
			        if(isValid){
			        	Msg=Utility.getLocaleValuePropByKey("msgDataSentToTeacherMatch", locale);
		        	    successORFailure="Success";
			        	
			        }else{
			        	//Msg="";
			        	successORFailure="Failure";
			        }
			        //Writing response
				    doc = docBuilder.newDocument();
					root = doc.createElement("TeacherMatch");
					doc.appendChild(root);
					root.setAttribute("method", "candidateOnboarding");
					
			        Element eleStatus = doc.createElement("ResponseStatus");
					Text textStatus = doc.createTextNode(successORFailure);//"Success"
			        eleStatus.appendChild(textStatus);
			        
			        /*Element eleErrorCode = doc.createElement("ErrorCode");
			        if(!isValid){
			    	    eleErrorCode = doc.createElement("ErrorCode");
						Text timeStampResult = doc.createTextNode(errorCodeVal);//"Data sent to Kelly"
						eleErrorCode.appendChild(timeStampResult);
			        }*/
			       
			        Element timeStamp = doc.createElement("MessageDescription");
					Text timeStampResult = doc.createTextNode(Msg);//"Data sent to Kelly"
					timeStamp.appendChild(timeStampResult);
					
					
					Element eleWebResponse = doc.createElement("WebResponse");
					eleWebResponse.appendChild(eleStatus);
					/*if(!isValid)
					  eleWebResponse.appendChild(eleErrorCode);*/
					eleWebResponse.appendChild(timeStamp);
					root.appendChild(eleWebResponse);
					
					TransformerFactory factory = TransformerFactory.newInstance();
					Transformer transformer = factory.newTransformer();
					
					StringWriter sw = new StringWriter();
					StreamResult result = new StreamResult(sw);
					DOMSource source = new DOMSource(doc);
					transformer.transform(source, result);
					returnVal = sw.toString();
					System.out.println("Date :::::: "+new Date().toString()+" >>>>>>>>>>>>> candidateOnboarding response::::::::::::: :: "+returnVal);
			        
					
			       /* if(isValid){*/
			        	statusCode = 200;
			        /*}else{
			        	 
			        	statusCode= 400;
			        }*/
			      	        
			    }else{
			    	statusCode= 200;
			    	Msg=Utility.getLocaleValuePropByKey("msgInvalidAction", locale);
			    	successORFailure="Failure";
			    	doc = docBuilder.newDocument();
					root = doc.createElement("TeacherMatch");
					doc.appendChild(root);
					root.setAttribute("method", "candidateOnboarding");
					
			        Element eleStatus = doc.createElement("ResponseStatus");
					Text textStatus = doc.createTextNode(successORFailure);//"Success"
			        eleStatus.appendChild(textStatus);
			        Element timeStamp = doc.createElement("MessageDescription");
					Text timeStampResult = doc.createTextNode(Msg);//"Data sent to Kelly"
					timeStamp.appendChild(timeStampResult);
					
					Element eleWebResponse = doc.createElement("WebResponse");
					eleWebResponse.appendChild(eleStatus);
					eleWebResponse.appendChild(timeStamp);
					root.appendChild(eleWebResponse);
					
					TransformerFactory factory = TransformerFactory.newInstance();
					Transformer transformer = factory.newTransformer();
					
					StringWriter sw = new StringWriter();
					StreamResult result = new StreamResult(sw);
					DOMSource source = new DOMSource(doc);
					transformer.transform(source, result);
					returnVal = sw.toString();
					System.out.println("Failed response::::::::::::: :: "+returnVal);
			    	
			    	
			    	
			    	
			    	
			    }
		    }
			catch (Exception e) {
				e.printStackTrace();
				statusCode = 500;
				returnVal=Utility.getLocaleValuePropByKey("msgHTTPCode500", locale);
		}
			System.out.println("statuscode::::::::::::"+statusCode+"--------req"+returnVal.toString() );
		return Response.status(statusCode).entity(returnVal.toString()).build();
	}

	
	
	
//Test API
	
	
	//sandeep API for KES
	@POST
	@Produces(MediaType.TEXT_XML)
	@Consumes(MediaType.TEXT_XML)
	@Path("/requestURL")
	public Response postKellyrURL(InputStream incomingData,@Context HttpServletRequest request, @Context HttpServletResponse res) 
	{
		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		Document doc = null;
		Element root = null;	
		String returnVal="";
		int statusCode=200;
		String successORFailure =null;
		try {			
			System.out.println("Enter in postKellyrURL XML");
			ServletContext context = request.getSession().getServletContext();
			ApplicationContext context0 = WebApplicationContextUtils.getWebApplicationContext(context);
			teacherDetailDAO = (TeacherDetailDAO)context0.getBean("teacherDetailDAO");
			//try {
				String payloadRequest = "";

				StringBuilder sb = new StringBuilder();
				try {
					BufferedReader in = new BufferedReader(new InputStreamReader(incomingData));
					String line = null;
					while ((line = in.readLine()) != null) {
						sb.append(line);
					}
				} catch (Exception e) {
					System.out.println("Error Parsing: - ");
				}
				// System.out.println("Data Received: " + crunchifyBuilder.toString());
				payloadRequest = sb.toString();
				System.err.println("payloadRequest::= "+payloadRequest);

				DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();

				//try {
					//Using factory get an instance of document builder
					DocumentBuilder docBuilder = null;
					
					docBuilder = builderFactory.newDocumentBuilder();
					//parse using builder to get DOM representation of the XML file
					InputSource inputSource = new InputSource();
					inputSource.setCharacterStream(new StringReader(payloadRequest));
					doc = docBuilder.parse(inputSource);

					NodeList CandidateDetailsTM = doc.getElementsByTagName("Request");
					System.out.println(""+CandidateDetailsTM.getLength());
				    if (CandidateDetailsTM.getLength() > 0) {
				        //Element element = (Element)CandidateDetailsTM.item(0);
				        Element element = (Element) CandidateDetailsTM.item(0);
				        
				        String TrustedTokenID = element.getElementsByTagName("TrustedTokenID").item(0).getTextContent();
				        System.out.println("TrustedTokenID: "+TrustedTokenID);
				        
				        String KSNTalentID = element.getElementsByTagName("KSNTalentID").item(0).getTextContent();
				        System.out.println("KSNTalentID: "+KSNTalentID);
				        
				        String Msg="";
				        Boolean isValid = true;
				        
				        if(KSNTalentID == null){
				        	isValid=false;
				        	Msg="KSNID is Required";
				        }else{
				        	if(KSNTalentID.isEmpty()){
				        		isValid=false;
					        	Msg="KSNID is Required";
					    	}
				        	
				        	if(KSNTalentID.length() >10){
				        		isValid=false;
					        	Msg="KSNID Length should be 10";
					    	}else{
					        	List<TeacherDetail> teachersKSNID =teacherDetailDAO.findByKSN(BigInteger.valueOf(Long.valueOf((KSNTalentID))));
					        	if(teachersKSNID != null && teachersKSNID.size() ==0){
					        		isValid=false;
						        	Msg="KSNID is Already Exist";
					        	}
				        	}
				        	
			              }
			        
				        
				        if(TrustedTokenID == null){
				        	isValid=false;
				        	Msg="TrustedTokenID is Required";
				        }else{
				        	if(TrustedTokenID.isEmpty()){
				        		isValid=false;
					        	Msg="TrustedTokenID is Required";
					    	}
				        }
				        
				        
				        
				        
				        Element RedirectURL = null;
				        //Writing response
				        if(isValid){
			        	    Msg="Data sent to TeacherMatch";
			        	    successORFailure="Success";
			        	     RedirectURL = doc.createElement("RedirectURL");
							Text RedirectURLTest = doc.createTextNode("https://eonbdev.kellyservices.com/AuthenticateTeacherMatchUser.aspx?username={0}&ksnid={1}&tokenid={2}");//"Data sent to Kelly"
							RedirectURL.appendChild(RedirectURLTest);
			        	    
			        	    
			        	    
				        }else{
				        	successORFailure="Failed";
				        }
				     	
		    	      			        
				        doc = docBuilder.newDocument();
						root = doc.createElement("TeacherMatch");
						doc.appendChild(root);
						root.setAttribute("method", "eRegSSO");
						
				        Element eleStatus = doc.createElement("ResponseStatus");
						Text textStatus = doc.createTextNode(successORFailure);//"Success"
				        eleStatus.appendChild(textStatus);
				        
				       if(isValid){
				    	   RedirectURL = doc.createElement("RedirectURL");
				    	   Text RedirectURLTest = doc.createTextNode("https://eonbdev.kellyservices.com/AuthenticateTeacherMatchUser.aspx?username={0}&ksnid={1}&tokenid={2}");
				    	   //Text RedirectURLTest = doc.createTextNode("https://www.google.com");
				    	   RedirectURL.appendChild(RedirectURLTest);
				       }
				       
				        Element timeStamp = doc.createElement("MessageDescription");
						Text timeStampResult = doc.createTextNode(Msg);//"Data sent to Kelly"
						timeStamp.appendChild(timeStampResult);
						
						
						Element eleWebResponse = doc.createElement("WebResponse");
						eleWebResponse.appendChild(eleStatus);
						eleWebResponse.appendChild(timeStamp);
						if(isValid)
						eleWebResponse.appendChild(RedirectURL);
						
						root.appendChild(eleWebResponse);
						
						TransformerFactory factory = TransformerFactory.newInstance();
						Transformer transformer = factory.newTransformer();
						
						StringWriter sw = new StringWriter();
						StreamResult result = new StreamResult(sw);
						DOMSource source = new DOMSource(doc);
						transformer.transform(source, result);
						returnVal = sw.toString();
						System.out.println("ReturnValue Link To KSN :: "+returnVal);
				        
				     			    
						if(isValid){
				        	statusCode = 200;
				        }else{
				        	statusCode= 400;
				        }
				      	        
				        
				    }
					
				
			}catch (Exception e) {
					e.printStackTrace();
					statusCode = 500;
					returnVal=Utility.getLocaleValuePropByKey("msgHTTPCode500", locale);
			}
				
			return Response.status(statusCode).entity(returnVal.toString()).build();
		}
	
	
//testing Ashish choudhary 	
	
	@POST
	 @Path("/sendjobdetailsto")
	 public Response sendjobdetailsto(InputStream incomingData,@Context HttpServletRequest request, @Context HttpServletResponse res) 
	 {
	  
		System.out.println(" ============= sendjobdetailsto 11111111 ========= ");
	     ServletContext context = request.getSession().getServletContext();
	  ApplicationContext context0 = WebApplicationContextUtils.getWebApplicationContext(context);
	  String returnVal="";
	  JSONObject jsonResponse = new JSONObject();
	  
	  try {   
	   
	   
	    
	    String errors = "";
	    
	    
	    jsonResponse = new JSONObject();
	    if(errors.equals(""))
	     jsonResponse.put("status", true);
	    else
	     jsonResponse.put("status", false);
	    jsonResponse.put("timeStamp", UtilityAPI.getCurrentTimeStamp());
	    if(!errors.equals(""))
	     jsonResponse.put("errorMessage", errors);
	    else
	     jsonResponse.put("errorMessage", "Data recieved.");


	   } 
	   catch (Exception e) {
	    jsonResponse = UtilityAPI.writeJSONErrors(10017, "Server Error.", e);
	    e.printStackTrace();

	   }
	   returnVal = jsonResponse.toString();
	  
	  return Response.status(200).entity(returnVal.toString()).build();
	 }
	
	public void checkAndCreateWorkFlows(JobForTeacher jobForTeacher,Map<String, WorkFlowStatus> workFlowMap,Map<String,MQEvent> mqEventMap,String workFlow,String ODTM_ACTION,String kellyTransactionID,TeacherDetail teacherDetail,Map<String,Date> latestNodeColorDateMap){
		
		MQEvent mqEvent = mqEventMap.get(workFlow);
		StatusNodeColorHistory statusNodeColorHistory = null;
		List<MQEvent> mqEventsList  = null;
		Map<String,MQEvent> mapNodeStatus = new HashMap<String, MQEvent>();
		if(mqEvent==null){
			mqEvent = new MQEvent();
    	    mqEvent.setWorkFlowStatusId(workFlowMap.get(Utility.getLocaleValuePropByKey("lblY", locale)));
    	    mqEvent.setKsnActionType(ODTM_ACTION);
    	    mqEvent.setKsnTransId(kellyTransactionID);
    	    mqEvent.setTeacherdetail(teacherDetail);
    	    mqEvent.setEventType(workFlow);
   	    	mqEvent.setStatus("C");
   	    	mqEvent.setAckStatus("Success");
    	    mqEvent.setCreatedDateTime(new Date());
    	    mqEventDAO.makePersistent(mqEvent);
    	    
    	    try {
   			 statusNodeColorHistory = candidateGridService.saveStatusNodeColor(jobForTeacher,mqEvent,new HashMap<String, MQEvent>(),latestNodeColorDateMap);
   		 } catch (Exception e) {
   			 e.printStackTrace();
   		 }
		}
		else{
			
			try {
				mqEventsList=mqEventDAO.findMQEventByTeacher_Op(teacherDetail);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			 if(mqEventsList.size()>0){
				 for (MQEvent mqEv : mqEventsList) {
					 mapNodeStatus.put(mqEv.getTeacherdetail()+"-"+mqEv.getEventType() , mqEv);
				 }
			 }
			 
			 try {
				 statusNodeColorHistory = candidateGridService.saveStatusNodeColor(jobForTeacher,mqEvent,mapNodeStatus,latestNodeColorDateMap);
			 } catch (Exception e) {
				 e.printStackTrace();
			 }
			mqEvent.setWorkFlowStatusId(workFlowMap.get(Utility.getLocaleValuePropByKey("lblY", locale)));
			mqEvent.setStatus("C");
   	    	mqEvent.setAckStatus("Success");
    	    mqEventDAO.updatePersistent(mqEvent);
		}
		
		 
	    
		 try{
			 String newColor = candidateGridService.getNodeColorWithMqEvent(mqEvent,Utility.getLocaleValuePropByKey("lblCOM", locale));
			 System.out.println(" newColor :: "+newColor);
			 statusNodeColorHistory.setCurrentColor(newColor);
			 statusNodeColorHistory.setUpdateDateTime(new Date());
			 statusNodeColorHistoryDAO.updatePersistent(statusNodeColorHistory);

		 }catch(Exception ex){
	    }
	}
	
	public void triggerWorkFlowCOM(String node,MQEvent mqEvent,Map<String,MQEvent> mqEventMap,Map<Integer,UserMaster> userMap,Map<String, WorkFlowStatus> workFlowMap,String ODTM_ACTION,String kellyTransactionID,TeacherDetail teacherDetail,JobForTeacher jobForTeacher, Map<String,Date> latestNodeColorDateMap,HttpServletRequest request,String payloadRequest) throws Exception
	{ 
		
		StatusNodeColorHistory statusNodeColorHistory = new StatusNodeColorHistory();
		mqEvent = mqEventMap.get(node);
		List<MQEvent> mqEventsList = null;
		Map<String,MQEvent> mapNodeStatus = new HashMap<String, MQEvent>();
		if(mqEvent!=null){
			UserMaster userMaster=userMap.get(mqEvent.getCreatedBy());
    	    mqEvent.setWorkFlowStatusId(workFlowMap.get(Utility.getLocaleValuePropByKey("lblCOM", locale)));
    	    mqEvent.setKsnActionType(ODTM_ACTION);
    	    mqEvent.setKsnTransId(kellyTransactionID);
    	    mqEvent.setStatus("C");
    	    mqEvent.setAckStatus("Success");
    	    
			 mqEventsList=mqEventDAO.findMQEventByTeacher(teacherDetail);
			 if(mqEventsList.size()>0){
				 for (MQEvent mqEv : mqEventsList) {
					 mapNodeStatus.put(mqEv.getTeacherdetail()+"-"+mqEv.getEventType() , mqEv);
				 }
			 }
    	    
    	    try {
				 statusNodeColorHistory = candidateGridService.saveStatusNodeColor(jobForTeacher,mqEvent,mapNodeStatus,latestNodeColorDateMap);
			 } catch (Exception e) {
				 e.printStackTrace();
			 }
			 
    	    mqEventDAO.makePersistent(mqEvent);
    	    
		}else{
			mqEvent = new MQEvent();
    	    mqEvent.setWorkFlowStatusId(workFlowMap.get(Utility.getLocaleValuePropByKey("lblCOM", locale)));
    	    mqEvent.setKsnActionType(ODTM_ACTION);
    	    mqEvent.setKsnTransId(kellyTransactionID);
    	    mqEvent.setTeacherdetail(teacherDetail);
    	    mqEvent.setEventType(node);
    	    mqEvent.setStatus("C");
    	    mqEvent.setAckStatus("Success");
    	    mqEvent.setCreatedDateTime(new Date());
    	    mqEventDAO.makePersistent(mqEvent);
    	    
    	    try {
				 statusNodeColorHistory = candidateGridService.saveStatusNodeColor(jobForTeacher,mqEvent,new HashMap<String, MQEvent>(),latestNodeColorDateMap);
			 } catch (Exception e) {
				 e.printStackTrace();
			 }
		}
		
		if(mqEvent != null && mqEvent.getEventType() != null){
			MQEventHistory mqEventHistory = new MQEventHistory();
			mqEventHistory.setMqEvent(mqEvent);
			mqEventHistory.setCreatedBy(1);
			mqEventHistory.setCreatedDateTime(new Date());
			mqEventHistory.setIpAddress(request.getRemoteAddr());
			mqEventHistory.setXmlObject(payloadRequest);
			mqEventHistoryDAO.makePersistent(mqEventHistory);
		}
		
		 try{
 	    	String newColor = candidateGridService.getNodeColorWithMqEvent(mqEvent,null);
 	    	System.out.println(" newColor :: "+newColor);
 	    	statusNodeColorHistory.setCurrentColor(newColor);
 	    	 statusNodeColorHistory.setUpdateDateTime(new Date());
 	    	statusNodeColorHistoryDAO.updatePersistent(statusNodeColorHistory);

 	    }catch(Exception ex){ 
 	    	ex.printStackTrace();
 	    }
 	    
		try {
			mqService.statusUpdateForKellyWorkFlowNodes(node,teacherDetail);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	
	public void createHistoryForErrorWfxComNodes(JobForTeacher jobForTeacher,MQEvent mqEvent,Map<String,MQEvent> mapNodeStatus,Map<String,Date> latestNodeColorDateMap,Map<String, WorkFlowStatus> workFlowMap,String ODTM_ACTION,String kellyTransactionID,TeacherDetail teacherDetail,String node) throws Exception
	{
		if(mqEvent==null){
			mqEvent = new MQEvent();
    	    mqEvent.setWorkFlowStatusId(null);
    	    mqEvent.setKsnActionType(ODTM_ACTION);
    	    mqEvent.setKsnTransId(kellyTransactionID);
    	    mqEvent.setTeacherdetail(teacherDetail);
    	    mqEvent.setEventType(node);
    	    mqEvent.setStatus("R");
    	    mqEvent.setAckStatus("Success");
    	    mqEvent.setCreatedDateTime(new Date());
    	    mqEventDAO.makePersistent(mqEvent);
		}
		
		try {
			StatusNodeColorHistory statusNodeColorHistory = candidateGridService.saveStatusNodeColor(jobForTeacher,mqEvent,mapNodeStatus,latestNodeColorDateMap);
			statusNodeColorHistory.setCurrentColor("Red");
			statusNodeColorHistory.setUpdateDateTime(new Date());
			statusNodeColorHistoryDAO.updatePersistent(statusNodeColorHistory);
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	public void resetWorkFlow(MQEvent mqEvent){
		if(mqEvent!=null){
			mqEvent.setWorkFlowStatusId(null);
			mqEvent.setAckStatus(null);
			mqEvent.setStatus(null);
			mqEventDAO.makePersistent(mqEvent);
		}
	}
}