package tm.services.teacher;

import java.util.Date;
import static tm.services.district.GlobalServices.CHECK_BEFORE_GREEN_MAP;
import static tm.services.district.GlobalServices.println;
import static tm.services.teacher.AcceptOrDeclineMailHtml.getTemplete;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import tm.bean.MessageToTeacher;
import javax.servlet.http.HttpServletRequest;
import tm.utility.IPAddressUtility;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import tm.dao.MessageToTeacherDAO;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.SchoolInJobOrder;
import tm.bean.TeacherDetail;
import tm.bean.TeacherStatusHistoryForJob;
import tm.bean.master.DistrictMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.SecondaryStatus;
import tm.bean.master.StatusMaster;
import tm.bean.user.UserMaster;
import tm.dao.JobForTeacherDAO;
import tm.dao.SchoolInJobOrderDAO;
import tm.dao.TeacherStatusHistoryForJobDAO;
import tm.dao.master.SecondaryStatusDAO;
import tm.services.EmailerService;
import tm.services.report.CandidateGridService;
import tm.utility.Utility;
import static tm.services.teacher.AcceptOrDeclineMailHtml.*;
import static tm.services.district.GlobalServices.*;
/*
*@Author: Ram Nath
*/
public class AcceptOrDeclineAjax {
	
	@Autowired
	private EmailerService emailerService;
	public void setEmailerService(EmailerService emailerService) 
	{
		this.emailerService = emailerService;
	}
	@Autowired
	private MessageToTeacherDAO messageToTeacherDAO;
	
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	
	
	
		
	 public String sendMailWithTemplate(JobOrder jobOrder, SchoolMaster schoolId, TeacherDetail teacherDetail, String callingControllerStatus, String subject, String emailIds){
		 String templatetoSend=getTemplate(jobOrder, schoolId, teacherDetail, callingControllerStatus);
		 sendMail(subject,templatetoSend,emailIds);
		 return "success";
	 }
	 
	 public String sendAcceptOrDecline(TeacherDetail teacherDetail,JobOrder jobOrder,DistrictMaster districtId,UserMaster userMaster, SchoolMaster schoolId,Integer callbytemplete,String statusFlagAndStatusId,String callingControllerStatus,String subject,String emailId,String flowOptionNames,HttpServletRequest request, String[] ccEmailId, Session statelessSession)throws Exception{
		 println(":::::::::sendAcceptOrDecline=:::::::::::::");
		 String acceptUrl=getAcceptedUrl(teacherDetail,jobOrder,districtId,userMaster,schoolId,callingControllerStatus,statusFlagAndStatusId);
		 String declineUrl=getDeclinedUrl(teacherDetail,jobOrder,districtId,userMaster,schoolId,callingControllerStatus,statusFlagAndStatusId);		 
		 String templatetoSend=AcceptOrDeclineMailHtml.getTemplete(teacherDetail,jobOrder,districtId,userMaster,schoolId,callbytemplete,statusFlagAndStatusId,callingControllerStatus,request,acceptUrl,declineUrl,flowOptionNames);
		 sendMailByStatusWise( teacherDetail, jobOrder, districtId, userMaster,schoolId, callbytemplete, statusFlagAndStatusId, callingControllerStatus, subject,emailId, request, templatetoSend, ccEmailId, statelessSession);
		 return "success";
	 }
	 
	 
	 public String sendNotification(TeacherDetail teacherDetail,JobOrder jobOrder,DistrictMaster districtId,UserMaster userMaster, SchoolMaster schoolId,Integer callbytemplete,String statusFlagAndStatusId,String callingControllerStatus,String subject,String emailId,HttpServletRequest request, String[] ccEmailId, Session statelessSession)throws Exception{
		 println(":::::::::sendNotification=:::::::::::::");
		 String templatetoSend=AcceptOrDeclineMailHtml.getNotificationTemplete(teacherDetail,jobOrder,districtId,userMaster,schoolId,callbytemplete,statusFlagAndStatusId,callingControllerStatus,request);
		 System.out.println("Template ============>"+templatetoSend);
		 sendNotificationMailByStatusWise(callingControllerStatus, subject,emailId, templatetoSend);
		 return "success";
	 }
	 
	 
	@SuppressWarnings("unchecked")
	private boolean sendMailByStatusWise(TeacherDetail teacherDetail,JobOrder jobOrder,DistrictMaster districtId,UserMaster userMaster,SchoolMaster schoolMaster,Integer callbytemplete,String statusFlagAndStatusId,String callingControllerStatus,String subject,String emailIds, HttpServletRequest request,String templete,String[] ccEmailId, Session statelessSession){
		try{
		List<JobForTeacher> lstJFT=jobForTeacherDAO.findJobByTeacherAndSatusFormJobId(teacherDetail,jobOrder); //statelessSession.createCriteria(JobForTeacher.class).add(Restrictions.eq("teacherId", teacherDetail)).add(Restrictions.eq("jobId", jobOrder)).list();		
		 if(callingControllerStatus.trim().equalsIgnoreCase("HR Review/Offer")){			 
				if(lstJFT.size()>0){
				Transaction transaction=statelessSession.beginTransaction();
				JobForTeacher jft=lstJFT.get(0);
				if(schoolMaster!=null){
					print("================="+schoolMaster.getSchoolId());
					jft.setSchoolMaster(schoolMaster);
					println("jobForTeacher==========="+jft.getSchoolMaster().getSchoolName());
				}
				else if(userMaster.getEntityType()==3 && userMaster.getSchoolId()!=null){
					print("====111111============="+userMaster.getSchoolId().getSchoolId());
					jft.setSchoolMaster(userMaster.getSchoolId());					
					println("jobForTeacher===11111111========"+jft.getSchoolMaster().getSchoolName());
				}
				jft.setOfferReady(true);
				jft.setOfferAccepted(null);
				jft.setOfferMadeDate(new Date());				
				statelessSession.update(jft);
				transaction.commit();
				sendMail(subject,templete,emailIds); 
				}
		 }else  if(callingControllerStatus.trim().equalsIgnoreCase("Evaluation Complete")){			
				if(lstJFT.size()>0){
				Transaction transaction=statelessSession.beginTransaction();
				JobForTeacher jft=lstJFT.get(0);
				if(schoolMaster!=null){
					print("================="+schoolMaster.getSchoolId());
					jft.setSchoolMaster(schoolMaster);
					println("jobForTeacher==========="+jft.getSchoolMaster().getSchoolName());
				}
				else if(userMaster.getEntityType()==3 && userMaster.getSchoolId()!=null){
					print("====111111============="+userMaster.getSchoolId().getSchoolId());
					jft.setSchoolMaster(userMaster.getSchoolId());					
					println("jobForTeacher===11111111========"+jft.getSchoolMaster().getSchoolName());
				}
				jft.setOfferReady(true);
				jft.setOfferAccepted(null);
				jft.setOfferMadeDate(new Date());
				statelessSession.update(jft);
				transaction.commit();
				sendMail(subject,templete,emailIds); 
				}
		 }else if(jobOrder!=null && jobOrder.getDistrictMaster().getDistrictId().equals(1201470) && callingControllerStatus.trim().equalsIgnoreCase("Offer Made")){
			 if(lstJFT.size()>0){
				 	Transaction transaction=statelessSession.beginTransaction();
					JobForTeacher jft=lstJFT.get(0);
					if(schoolMaster!=null){
						print("================="+schoolMaster.getSchoolId());
						jft.setSchoolMaster(schoolMaster);
						println("jobForTeacher==========="+jft.getSchoolMaster().getSchoolName());
					}
					else if(userMaster.getEntityType()==3 && userMaster.getSchoolId()!=null){
						print("====111111============="+userMaster.getSchoolId().getSchoolId());
						jft.setSchoolMaster(userMaster.getSchoolId());					
						println("jobForTeacher===11111111========"+jft.getSchoolMaster().getSchoolName());
					}
					jft.setOfferReady(true);
					jft.setOfferAccepted(null);
					jft.setOfferMadeDate(new Date());
					statelessSession.update(jft);
					transaction.commit();
					sendMail(subject,templete,emailIds); 
					
				}
		 }else if(jobOrder!=null && jobOrder.getDistrictMaster().getDistrictId().equals(804800) && callingControllerStatus.trim().equalsIgnoreCase("Offer Made")){
			 if(lstJFT.size()>0){
				 	Transaction transaction=statelessSession.beginTransaction();
					JobForTeacher jft=lstJFT.get(0);
					if(schoolMaster!=null){
						print("================="+schoolMaster.getSchoolId());
						jft.setSchoolMaster(schoolMaster);
						println("jobForTeacher==========="+jft.getSchoolMaster().getSchoolName());
					}
					else if(userMaster.getEntityType()==3 && userMaster.getSchoolId()!=null){
						print("====111111============="+userMaster.getSchoolId().getSchoolId());
						jft.setSchoolMaster(userMaster.getSchoolId());					
						println("jobForTeacher===11111111========"+jft.getSchoolMaster().getSchoolName());
					}
					jft.setOfferReady(true);
					jft.setOfferAccepted(null);
					jft.setOfferMadeDate(new Date());
					statelessSession.update(jft);
					transaction.commit();
					println("yessssssssssssssssssssssssssssssssssssssssssssssssssssss");
					sendMailCC(subject,templete,emailIds,new String[]{jobOrder.getEmploymentServicesTechnician().getPrimaryEmailAddress(),jobOrder.getEmploymentServicesTechnician().getBackupEmailAddress()}); 
					
				}
		 }else if(jobOrder!=null && jobOrder.getDistrictMaster().getDistrictId().equals(806900) && callingControllerStatus.trim().equalsIgnoreCase("Recommend for Hire")){
			 if(lstJFT.size()>0){
				 	Transaction transaction=statelessSession.beginTransaction();
					JobForTeacher jft=lstJFT.get(0);
					if(schoolMaster!=null){
						print("================="+schoolMaster.getSchoolId());
						jft.setSchoolMaster(schoolMaster);
						println("jobForTeacher==========="+jft.getSchoolMaster().getSchoolName());
					}
					else if(userMaster.getEntityType()==3 && userMaster.getSchoolId()!=null){
						print("====111111============="+userMaster.getSchoolId().getSchoolId());
						jft.setSchoolMaster(userMaster.getSchoolId());					
						println("jobForTeacher===11111111========"+jft.getSchoolMaster().getSchoolName());
					}
					jft.setOfferReady(true);
					jft.setOfferAccepted(null);
					jft.setOfferMadeDate(new Date());
					statelessSession.update(jft);
					transaction.commit();
					println("yessssssssssssssssssssssssssssssssssssssssssssssssssssss->>>-------Recommend for Hire");
					sendMailCC(subject,templete,emailIds,null); 
					
				}
		 }else if(callingControllerStatus.trim().equalsIgnoreCase("teacherAcceptedMail")){
			 sendMail(subject,templete,emailIds); 		
		 }else if(callingControllerStatus.trim().equalsIgnoreCase("teacherDeclinedMail")){
			 sendMail(subject,templete,emailIds);
		 }else if(callingControllerStatus.trim().equalsIgnoreCase("allSchoolUserNotificationMail")){
			 sendMail(subject,templete,emailIds);
		 }else if(callingControllerStatus.trim().equalsIgnoreCase("teacherAcceptedOfferMadeMail")){
			 sendMailCC(subject,templete,emailIds,ccEmailId);
		 }else if(callingControllerStatus.trim().equalsIgnoreCase("teacherDeclinedOfferMadeMail")){
			 sendMailCC(subject,templete,emailIds,ccEmailId);
		 }else if(callingControllerStatus.trim().equalsIgnoreCase("teacherAcceptedOfferMadeJeffcoMail") || callingControllerStatus.trim().equalsIgnoreCase("teacherDeclinedOfferMadeJeffcoMail")){
			 sendMailCC(subject,templete,emailIds,new String[]{});
		 }else if(callingControllerStatus.trim().equalsIgnoreCase("Restart Workflow")){
			 try{
				 	MessageToTeacher  messageToTeacher= new MessageToTeacher();
					messageToTeacher.setTeacherId(teacherDetail);
					messageToTeacher.setTeacherEmailAddress(teacherDetail.getEmailAddress());
					messageToTeacher.setMessageSubject(subject);
					messageToTeacher.setMessageSend(templete);
					messageToTeacher.setSenderId(userMaster);
					messageToTeacher.setSenderEmailAddress(userMaster.getEmailAddress());
					messageToTeacher.setEntityType(userMaster.getEntityType());
					messageToTeacher.setJobId(jobOrder);
					try{
						messageToTeacher.setIpAddress(IPAddressUtility.getIpAddress(request));
					}catch(Exception e){
						e.printStackTrace();
					}
					messageToTeacherDAO.makePersistent(messageToTeacher);
				 }catch(Exception e){
					 e.printStackTrace();
				 }
				 sendMail(subject,templete,emailIds);
			 }
		// transaction.commit();
		 return true;
		}catch(Exception e){
			e.printStackTrace();
			 return false;
		}
	 }
	 
	
	@SuppressWarnings("unchecked")
	private boolean sendNotificationMailByStatusWise(String callingControllerStatus,String subject,String emailIds, String templete){
		try{
		 if(callingControllerStatus.trim().equalsIgnoreCase("JEFFCO_NF_Offer Made")){	
			 String[] ccEId=new String[0];
			 	sendMailCC(subject,templete,emailIds,ccEId); 
				}
		 return true;
		}catch(Exception e){
			e.printStackTrace();
			 return false;
		}
	 }
	
	 public boolean sendMail(String subject,String templete, String emails){
		 	println(":::::::::sendMail=:::::::::::::");				 	
			println("subject:::"+subject);
			println("email:::::"+emails);
			try{
				System.out.println(":"+subject);
				System.out.println(":"+templete);
				 String eamilIds[]=emails.split(",");
				 if(eamilIds.length>0){
					 for(String eId:eamilIds)
						 emailerService.sendMailAsHTMLText(eId,subject,templete);
				 }					
				println("Mail set successfully");
				return true;
			}catch (Exception e) {
				e.printStackTrace();
				return false;
			}		 
	 }
	 public boolean sendMailCC(String subject,String templete, String emails, String[] ccEId){
		 //INSERT INTO `teachermatch3`.`contacttypemaster` (`contactTypeId`, `contactType`, `status`) VALUES (NULL, 'HR Staff', 'A');
		 	println(":::::::::sendMail=:::::::::::::");				 	
			println("subject:::"+subject);
			println("email:::::"+emails);
			try{	
				System.out.println(":"+subject);
				System.out.println("Mail template:"+templete+"\nEmailId:"+emails);
				try{println("ccEmailId:"+Arrays.asList(ccEId));}catch(Exception e){}
				 String eamilIds[]=emails.split(",");
				 if(eamilIds.length>0){
					 //for(String eId:eamilIds)
						 emailerService.sendMailAsHTMLTextCC(eamilIds,subject,templete,ccEId);
				 }					
				println("Mail set successfully");
				return true;
			}catch (Exception e) {
				e.printStackTrace();
				return false;
			}		 
	 }
	 
	 
	 private String getAcceptedUrl(TeacherDetail teacherDetail,JobOrder jobOrder,DistrictMaster districtId,UserMaster userMaster, SchoolMaster schoolId,String callingControllerStatus,String statusFlagAndStatusId){
		 //teacherId##JobId##userId##statusFlagAndStatus##AcceptOrDecline##callingControllerText##SchoolId
		 String acceptParameters = Utility.encodeInBase64(teacherDetail.getTeacherId()+"##"+jobOrder.getJobId()+"##"+userMaster.getUserId()+"##"+statusFlagAndStatusId+"##1##"+callingControllerStatus+"##"+(schoolId!=null?schoolId.getSchoolId():null));			
		 String acceptURL=Utility.getValueOfPropByKey("basePath")+"/acceptordecline.do?key="+acceptParameters;	
		 return acceptURL;
	 }
	 private String getDeclinedUrl(TeacherDetail teacherDetail,JobOrder jobOrder,DistrictMaster districtId,UserMaster userMaster, SchoolMaster schoolId,String callingControllerStatus,String statusFlagAndStatusId){
		 //teacherId##JobId##userId##statusFlagAndStatus##AcceptOrDecline##callingControllerText##SchoolId
		 String declineParameters = Utility.encodeInBase64(teacherDetail.getTeacherId()+"##"+jobOrder.getJobId()+"##"+userMaster.getUserId()+"##"+statusFlagAndStatusId+"##0##"+callingControllerStatus+"##"+(schoolId!=null?schoolId.getSchoolId():null));
		 String declineURL=Utility.getValueOfPropByKey("basePath")+"/acceptordecline.do?key="+declineParameters;	
		return  declineURL;
	 }
	 
	 public static boolean checkStatusGreen(JobForTeacher jobForTeacher, TeacherStatusHistoryForJobDAO teacherStatusHistoryForJobDAO, SecondaryStatusDAO secondaryStatusDAO, String... checkStatus){
		 System.out.println("::::::::::::::::::::::::::::::::checkStatusGreen::Enter:::::::::::::::::::::::::::"+Arrays.asList(checkStatus)+":::::::::::::::");
			try{
				List<String> checkStatus1=new ArrayList<String>();
				if(checkStatus!=null){
					checkStatus1=Arrays.asList(checkStatus);
				}
				int count=0;
				List<TeacherStatusHistoryForJob> historyList=teacherStatusHistoryForJobDAO.findByTeacherAndJob(jobForTeacher.getTeacherId(),jobForTeacher.getJobId());
				Map<String,Boolean> cmpMap = new HashMap<String, Boolean>();
				println("historyList============="+historyList.size());
				for (TeacherStatusHistoryForJob teacherStatusHistoryForJob : historyList) {
					if(teacherStatusHistoryForJob.getSecondaryStatus()!=null)
					{		
						println("SecondaryStatus: "+teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusName());
						cmpMap.put(teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusName(),true);
					}
					if(teacherStatusHistoryForJob.getStatusMaster()!=null)
					{						
						List<SecondaryStatus>  lstSecondaryStatus_temp=secondaryStatusDAO.getSecondaryStatusForPanel(jobForTeacher.getJobId().getDistrictMaster(), jobForTeacher.getJobId().getJobCategoryMaster(), teacherStatusHistoryForJob.getStatusMaster());
						if(lstSecondaryStatus_temp.size() ==1)
						{
							SecondaryStatus ss=lstSecondaryStatus_temp.get(0);
							cmpMap.put(ss.getSecondaryStatusName(),true);//added	
							println("Status: "+ss.getSecondaryStatusName());
						}else{
							println("Status: "+teacherStatusHistoryForJob.getStatusMaster().getStatus());
							cmpMap.put(teacherStatusHistoryForJob.getStatusMaster().getStatus(),true);//added
						}
					}
				}
				for(String status:checkStatus1){
					Boolean flagCheck=cmpMap.get(status);
					System.out.println("flagCheck=========="+status+"============"+flagCheck);
					if(flagCheck!=null && flagCheck){
						count++;
					}
				}
				System.out.println("count======================"+count);
				
					if(checkStatus!=null && checkStatus.length==count){
					println("================================"+checkStatus.length+"==================================="+count+"==================================================================");
					return true;
					}else {
					return false;
					}
			}catch(Exception e){e.printStackTrace();return false;}
	 }
	 
	 public static boolean checkAllGreenBeforeCurrentStatus(CandidateGridService cgService,JobForTeacher jobForTeacher, JobOrder jobOrder,SecondaryStatusDAO secondaryStatusDAO,TeacherStatusHistoryForJobDAO teacherStatusHistoryForJobDAO, String checkStauts){		
		// String checkValueInMap=jobForTeacher.getJobId().getDistrictMaster().getDistrictId()+"##"+checkStauts;
		 String checkValueInMap=null;
		 if(jobForTeacher.getJobId().getHeadQuarterMaster()!=null){
			 
		 }else{
			 checkValueInMap=jobForTeacher.getJobId().getDistrictMaster().getDistrictId()+"##"+checkStauts;
		 }
		//println("=====================================================================================================================================");
		 if(CHECK_BEFORE_GREEN_MAP.get(checkValueInMap)!=null && CHECK_BEFORE_GREEN_MAP.get(checkValueInMap)){
			try{
				Map<String,StatusMaster> statusMasterMap=new HashMap<String,StatusMaster>();
				Map<String,SecondaryStatus> secondaryStatusMap=new HashMap<String,SecondaryStatus>();
				List<TeacherStatusHistoryForJob> historyList=teacherStatusHistoryForJobDAO.findByTeacherAndJob(jobForTeacher.getTeacherId(),jobForTeacher.getJobId());
				Map<String,String> cmpMap = new HashMap<String, String>();
				Map<String,String> allStsMap = new HashMap<String, String>();
				println("historyList============="+historyList.size());
				for (TeacherStatusHistoryForJob teacherStatusHistoryForJob : historyList) {
					if(teacherStatusHistoryForJob.getSecondaryStatus()!=null)
					{		
						println("SecondaryStatus: "+teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusName()+"==========0##"+teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusId());
						cmpMap.put("0##"+teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusId(),teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusName());
					}
					if(teacherStatusHistoryForJob.getStatusMaster()!=null && !teacherStatusHistoryForJob.getStatusMaster().getStatusShortName().equalsIgnoreCase("vcomp"))
					{
						println("Status: "+teacherStatusHistoryForJob.getStatusMaster().getStatus()+"==============="+teacherStatusHistoryForJob.getStatusMaster().getStatusId()+"##0");
						cmpMap.put(teacherStatusHistoryForJob.getStatusMaster().getStatusId()+"##0",teacherStatusHistoryForJob.getStatusMaster().getStatus());
					}
				}			
				List<SecondaryStatus> lstTreeStructure = secondaryStatusDAO.findSecondaryStatusByJobCategoryHeadAndBranch(jobOrder);
				LinkedHashMap<String,String> statusNameMap=new LinkedHashMap<String,String>();
				Map<String,Integer> statusHistMap=new HashMap<String, Integer>();				
				try{
					int counter=0; 
					for(SecondaryStatus tree1: lstTreeStructure)
					{
						if(tree1.getSecondaryStatus()==null)
						{
							if(tree1.getChildren().size()>0){
								counter=cgService.getAllStatusForHistory(statusMasterMap,secondaryStatusMap,tree1.getChildren(),statusHistMap,statusNameMap,counter);
							}
						}
					}												
					for(Map.Entry<String,String> entry:statusNameMap.entrySet()){												
						String id[]=entry.getKey().toString().split("##");
						if(entry.getValue().toString().equalsIgnoreCase(checkStauts))
							break;
						if(!entry.getValue().toString().equalsIgnoreCase("Available")){
						println("Key======"+entry.getKey()+"   Value==="+entry.getValue());
						allStsMap.put(entry.getKey().toString(),entry.getValue().toString());
						}						
					}
				}catch(Exception e){}				
				for(Map.Entry<String,String> entry:cmpMap.entrySet()){											
					allStsMap.remove(entry.getKey());
				}
				//println("=====================================================================================================================================");
				if(allStsMap.size()==0)
					return false;
				else
					return true;								
			}catch(Exception e){e.printStackTrace();return false;}
		 }else{
			// println("=====================================================================================================================================");
			 return false;
		 }		 
	 }
	 public static boolean checkAlreadyOfferOrNot(JobForTeacherDAO jobForTeacherDAO, JobForTeacher jobForTeacher, UserMaster userMaster, String checkStauts){
		 String checkValueInMap=null;
		 if(jobForTeacher.getJobId().getHeadQuarterMaster()!=null){
			 
		 }else{
			 checkValueInMap=jobForTeacher.getJobId().getDistrictMaster().getDistrictId()+"##"+checkStauts;
		 }
		//println("========================================================================checkAlreadyOfferOrNot=============================================================");
		if(IS_ALREADY_OFFERED.get(checkValueInMap)!=null && IS_ALREADY_OFFERED.get(checkValueInMap)){
		 try{
			List<JobForTeacher> lstJFT=jobForTeacherDAO.findAllTeacherByDistrict(jobForTeacher.getTeacherId(),jobForTeacher.getJobId().getDistrictMaster());											
			if(lstJFT!=null && lstJFT.size()>0)
			{
				for(JobForTeacher jft:lstJFT){
					if(jft.getUpdatedBy()!=null){
						println("jobForTeacher.getJobForTeacherId()=="+jobForTeacher.getJobForTeacherId()+"==jft.getJobForTeacherId()="+jft.getJobForTeacherId()+"      userMaster.getUserId()=="+userMaster.getUserId()+"  jft.getUpdatedBy().getUserId()=="+jft.getUpdatedBy().getUserId());
						println("userMaster.getDistrictId().getDistrictId()====="+userMaster.getDistrictId().getDistrictId().toString()+"  jft.getUpdatedBy().getDistrictId().getDistrictId()==="+jft.getUpdatedBy().getDistrictId().getDistrictId().toString());
					if(jft.getJobForTeacherId().toString().equals(jobForTeacher.getJobForTeacherId().toString()) && ((userMaster.getUserId().toString().equalsIgnoreCase(jft.getUpdatedBy().getUserId().toString())) || (userMaster.getEntityType()==2 && userMaster.getDistrictId().getDistrictId().toString().equalsIgnoreCase(jft.getUpdatedBy().getDistrictId().getDistrictId().toString())) ))
						return false;
						else
						return true;
					}
					else
					return false;
				}
			}
			return false;
		 }catch (Exception e) {
			// TODO: handle exception
			 e.printStackTrace();
			 return false;
		}
	 }else{
		 return false;
	 }
	 }
	 	 
	 public static String getAllEmailId(List<UserMaster> umList){
		 String allEmailId="";					
			if(umList.size()>0){
				for(UserMaster um:umList){
					if(allEmailId.trim().equalsIgnoreCase(""))
						allEmailId+=um.getEmailAddress();
					else
						allEmailId+=","+um.getEmailAddress();
				}				
			}
			return allEmailId;
	 }
	 
	 
	 public static boolean checkAllGreenBeforeCurrentStatusNew(List<TeacherStatusHistoryForJob> historyList,List<SecondaryStatus> lstTreeStructure,CandidateGridService cgService,JobForTeacher jobForTeacher, JobOrder jobOrder,SecondaryStatusDAO secondaryStatusDAO,TeacherStatusHistoryForJobDAO teacherStatusHistoryForJobDAO, String checkStauts){		
			// String checkValueInMap=jobForTeacher.getJobId().getDistrictMaster().getDistrictId()+"##"+checkStauts;
			 String checkValueInMap=null;
			 if(jobForTeacher.getJobId().getHeadQuarterMaster()!=null){
				 
			 }else{
				 checkValueInMap=jobForTeacher.getJobId().getDistrictMaster().getDistrictId()+"##"+checkStauts;
			 }
			//println("=====================================================================================================================================");
			 if(CHECK_BEFORE_GREEN_MAP.get(checkValueInMap)!=null && CHECK_BEFORE_GREEN_MAP.get(checkValueInMap)){
				try{
					Map<String,StatusMaster> statusMasterMap=new HashMap<String,StatusMaster>();
					Map<String,SecondaryStatus> secondaryStatusMap=new HashMap<String,SecondaryStatus>();
					//List<TeacherStatusHistoryForJob> historyList=teacherStatusHistoryForJobDAO.findByTeacherAndJob(jobForTeacher.getTeacherId(),jobForTeacher.getJobId());
					
					Map<String,String> cmpMap = new HashMap<String, String>();
					Map<String,String> allStsMap = new HashMap<String, String>();
					println("historyList============="+historyList.size());
					for (TeacherStatusHistoryForJob teacherStatusHistoryForJob : historyList) {
						if(teacherStatusHistoryForJob.getSecondaryStatus()!=null)
						{		
							println("SecondaryStatus: "+teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusName()+"==========0##"+teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusId());
							cmpMap.put("0##"+teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusId(),teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusName());
						}
						if(teacherStatusHistoryForJob.getStatusMaster()!=null && !teacherStatusHistoryForJob.getStatusMaster().getStatusShortName().equalsIgnoreCase("vcomp"))
						{
							println("Status: "+teacherStatusHistoryForJob.getStatusMaster().getStatus()+"==============="+teacherStatusHistoryForJob.getStatusMaster().getStatusId()+"##0");
							cmpMap.put(teacherStatusHistoryForJob.getStatusMaster().getStatusId()+"##0",teacherStatusHistoryForJob.getStatusMaster().getStatus());
						}
					}			
					//List<SecondaryStatus> lstTreeStructure = secondaryStatusDAO.findSecondaryStatusByJobCategoryHeadAndBranch(jobOrder);
					LinkedHashMap<String,String> statusNameMap=new LinkedHashMap<String,String>();
					Map<String,Integer> statusHistMap=new HashMap<String, Integer>();				
					try{
						int counter=0; 
						for(SecondaryStatus tree1: lstTreeStructure)
						{
							if(tree1.getSecondaryStatus()==null)
							{
								if(tree1.getChildren().size()>0){
									counter=cgService.getAllStatusForHistory(statusMasterMap,secondaryStatusMap,tree1.getChildren(),statusHistMap,statusNameMap,counter);
								}
							}
						}												
						for(Map.Entry<String,String> entry:statusNameMap.entrySet()){												
							String id[]=entry.getKey().toString().split("##");
							if(entry.getValue().toString().equalsIgnoreCase(checkStauts))
								break;
							if(!entry.getValue().toString().equalsIgnoreCase("Available")){
							println("Key======"+entry.getKey()+"   Value==="+entry.getValue());
							allStsMap.put(entry.getKey().toString(),entry.getValue().toString());
							}						
						}
					}catch(Exception e){}				
					for(Map.Entry<String,String> entry:cmpMap.entrySet()){											
						allStsMap.remove(entry.getKey());
					}
					//println("=====================================================================================================================================");
					if(allStsMap.size()==0)
						return false;
					else
						return true;								
				}catch(Exception e){e.printStackTrace();return false;}
			 }else{
				// println("=====================================================================================================================================");
				 return false;
			 }		 
		 }
	 
	 public static boolean checkAlreadyOfferOrNotNew(List<JobForTeacher> lstJFT,JobForTeacherDAO jobForTeacherDAO, JobForTeacher jobForTeacher, UserMaster userMaster, String checkStauts){
		 String checkValueInMap=null;
		 if(jobForTeacher.getJobId().getHeadQuarterMaster()!=null){
			 
		 }else{
			 checkValueInMap=jobForTeacher.getJobId().getDistrictMaster().getDistrictId()+"##"+checkStauts;
		 }
		//println("========================================================================checkAlreadyOfferOrNot=============================================================");
		if(IS_ALREADY_OFFERED.get(checkValueInMap)!=null && IS_ALREADY_OFFERED.get(checkValueInMap)){
		 try{
			 System.out.println("lstJFTlstJFTlstJFTlstJFTlstJFT    "+lstJFT.size());
			List<JobForTeacher> lstJFT1=jobForTeacherDAO.findAllTeacherByDistrict(jobForTeacher.getTeacherId(),jobForTeacher.getJobId().getDistrictMaster());
			System.out.println("lstJFT1    "+lstJFT1.size());
			if(lstJFT!=null && lstJFT.size()>0)
			{
				for(JobForTeacher jft:lstJFT){
					if(jft.getUpdatedBy()!=null){
						println("jobForTeacher.getJobForTeacherId()=="+jobForTeacher.getJobForTeacherId()+"==jft.getJobForTeacherId()="+jft.getJobForTeacherId()+"      userMaster.getUserId()=="+userMaster.getUserId()+"  jft.getUpdatedBy().getUserId()=="+jft.getUpdatedBy().getUserId());
						println("userMaster.getDistrictId().getDistrictId()====="+userMaster.getDistrictId().getDistrictId().toString()+"  jft.getUpdatedBy().getDistrictId().getDistrictId()==="+jft.getUpdatedBy().getDistrictId().getDistrictId().toString());
					if(jft.getJobForTeacherId().toString().equals(jobForTeacher.getJobForTeacherId().toString()) && ((userMaster.getUserId().toString().equalsIgnoreCase(jft.getUpdatedBy().getUserId().toString())) || (userMaster.getEntityType()==2 && userMaster.getDistrictId().getDistrictId().toString().equalsIgnoreCase(jft.getUpdatedBy().getDistrictId().getDistrictId().toString())) ))
						return false;
						else
						return true;
					}
					else
					return false;
				}
			}
			return false;
		 }catch (Exception e) {
			// TODO: handle exception
			 e.printStackTrace();
			 return false;
		}
	 }else{
		 return false;
	 }
	 }
	 
	 public static boolean jeffcoCheckAlreadyOfferMadeInSameJobOrNot(JobForTeacherDAO jobForTeacherDAO, JobForTeacher jobForTeacher, UserMaster userMaster, String checkStauts){
		 //println("========================================================================checkAlreadyOfferOrNot=============================================================");
		 String checkValueInMap=null;
		 int noOfOpening=0;
		 if(jobForTeacher.getJobId().getHeadQuarterMaster()!=null){
			 
		 }else{
			 checkValueInMap=jobForTeacher.getJobId().getDistrictMaster().getDistrictId()+"##"+checkStauts;
		 }
		if(IS_ALREADY_OFFERMADE_ONLYONE_IN_SAMEJOB.get(checkValueInMap)!=null && IS_ALREADY_OFFERMADE_ONLYONE_IN_SAMEJOB.get(checkValueInMap)){
			
			 SchoolInJobOrderDAO schoolInJobOrderDAO=jobForTeacherDAO.getSchoolInJobOrderDAO();
				List<SchoolInJobOrder> listSchoolInJobOrders=schoolInJobOrderDAO.findJobOrder(jobForTeacher.getJobId());
				if(listSchoolInJobOrders!=null && listSchoolInJobOrders.size()>0){
					for(SchoolInJobOrder schoolJob : listSchoolInJobOrders){
						if(schoolJob.getNoOfSchoolExpHires()!=null){
							noOfOpening+=schoolJob.getNoOfSchoolExpHires();
						}
					}
				}else{ 
					if(jobForTeacher.getJobId().getNoOfExpHires()!=null){
						noOfOpening=jobForTeacher.getJobId().getNoOfExpHires();
					}
				}
			
		 try{
			List<TeacherStatusHistoryForJob> lstTSHJ=jobForTeacherDAO.findAllTeacherByDistrictAndJobId(jobForTeacher.getJobId(),jobForTeacher.getTeacherId());
			println("lstTSHJ==============="+lstTSHJ.size()+" jobForTeacher.=="+jobForTeacher.getTeacherId().getTeacherId()+" jobId=="+jobForTeacher.getJobId().getJobId() );
			if(lstTSHJ!=null && lstTSHJ.size()>=noOfOpening)
			{
				/*for(TeacherStatusHistoryForJob tshj:lstTSHJ)
					if(tshj.getTeacherDetail().getTeacherId().equals(jobForTeacher.getTeacherId().getTeacherId()))
						return false;
				if(lstTSHJ.size()>0)*/
					return true;
				/*else
					return false;*/
			}
			return false;
		 }catch (Exception e) {
			// TODO: handle exception
			 e.printStackTrace();
			 return false;
		}
	 }else{
		 return false;
	 }
	 }
}
