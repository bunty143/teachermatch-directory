package tm.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobApprovalProcess;
import tm.bean.master.DistrictMaster;
import tm.dao.generic.GenericHibernateDAO;

public class JobApprovalProcessDAO extends GenericHibernateDAO<JobApprovalProcess, Integer> {
	public JobApprovalProcessDAO() {
		super(JobApprovalProcess.class);
	}
	@Transactional(readOnly=false)
	public List<JobApprovalProcess> getJobApprovalProcessList(){
		Criterion criterion1 = Restrictions.eq("status",1);
		List<JobApprovalProcess> list = findByCriteria(criterion1);
		return list;
	}
	
	@Transactional(readOnly=true)
	public List<JobApprovalProcess> findAllDistrictJobApproval(DistrictMaster districtMaster)
	{
		List <JobApprovalProcess> jobApprovalProcessesList=null;
		try
		{
			Criterion criterion1 = Restrictions.eq("districtId",districtMaster);			
			Criterion criterion2 = Restrictions.eq("status",1);
			Order order = Order.asc("jobApprovalProcessId");
			
			Criterion criterion = Restrictions.and(criterion1, criterion2);
			jobApprovalProcessesList = findByCriteria(order, criterion);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return jobApprovalProcessesList;
	}
	@Transactional(readOnly=true)
	public boolean isProcessExistByDistrict(Integer districtId, String approvalName)
	{
		boolean isGroupExist=false;
		try
		{
			Criterion criterion1 = Restrictions.eq("districtId.districtId",districtId);
			Criterion criterion2 = Restrictions.eq("jobApprovalProcess",approvalName);
			
			List<JobApprovalProcess> jobApprovalProcessesList = findByCriteria(criterion1, criterion2);
			
			if(jobApprovalProcessesList!=null && jobApprovalProcessesList.size()>0)
				isGroupExist=true;
			
			System.out.println("districtApprovalGroupsList:- "+jobApprovalProcessesList+", isGroupExist:- "+isGroupExist);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return isGroupExist;
	}
	@Transactional(readOnly=true)
	public List<JobApprovalProcess> findAllJobApprovalProcessByDistrict(DistrictMaster districtMaster)
	{
		List <JobApprovalProcess> jobApprovalProcessList=null;
		try
		{
			Criterion criterion1 = Restrictions.eq("districtId",districtMaster);
			Criterion criterion2 = Restrictions.eq("status",1);
			Order order = Order.asc("jobApprovalProcessId");			
			Criterion criterion = Restrictions.and(criterion1, criterion2);
			jobApprovalProcessList = findByCriteria(order, criterion);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return jobApprovalProcessList;
	}
	@Transactional(readOnly=true)
	public List<JobApprovalProcess> findApprovalProcessByDistrictAndName(DistrictMaster districtMaster, String processName)
	{
		List<JobApprovalProcess> jobApprovalProcessList = new ArrayList<JobApprovalProcess>();
		
		try{
			Criterion criterion1 = Restrictions.eq("districtId",districtMaster);
			Criterion criterion2 = Restrictions.eq("groupName",processName);
			Criterion criterion= Restrictions.and(criterion1, criterion2);
			
			jobApprovalProcessList = findByCriteria(criterion);
			
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		return jobApprovalProcessList;
	}
	@Transactional(readOnly=true)
	public List<JobApprovalProcess> findActiveInactiveJobApprovalProcessByDistrict(DistrictMaster districtMaster)
	{
		List <JobApprovalProcess> jobApprovalProcessList=null;
		try
		{
			Criterion criterion1 = Restrictions.eq("districtId",districtMaster);
			Order order = Order.asc("jobApprovalProcessId");	
			jobApprovalProcessList = findByCriteria(order,criterion1);	
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return jobApprovalProcessList;
	}
	@Transactional(readOnly=true)
	public Integer findNumberOfRowsWhichContainGroupIds(Integer groupId){
		Integer totalRow=null;
		try{
			Criteria criteria = getSession().createCriteria(JobApprovalProcess.class);
			Criterion criterion=Restrictions.like("jobApprovalGroups","%|"+groupId+"|%");
			Criterion criterion3 = Restrictions.eq("status",1);
			criteria.add(criterion);
			criteria.add(criterion3);
			criteria.setProjection(Projections.rowCount());
			totalRow = (Integer) criteria.uniqueResult();
		
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return totalRow;
	}
}
