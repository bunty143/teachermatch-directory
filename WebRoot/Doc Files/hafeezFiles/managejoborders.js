/* @Author: Gagan 
 * @Discription: view of edit domain js.
*/
var page = 1;
var noOfRows = 50;
var sortOrderStr="";
var sortOrderType="";
var deviceTypeAndroid=$.browser.device = (/Android|webOS|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent.toLowerCase()));
var deviceType=$.browser.device = (/iPhone|iPad|iPod/i.test(navigator.userAgent.toLowerCase()));

var schoolIdsStore="";
var pageforSchool		=	1;
var noOfRowsSchool 		=	50;
var sortOrderStrSchool	=	"";
var sortOrderTypeSchool	=	"";

var pageZS = 1;
var noOfRowsZS = 10;
var sortOrderStrZS="";
var sortOrderTypeZS="";

function getPaging(pageno)
{
	
	if($("#gridNo").val()==2)
	{
		if(pageno!='')
		{
			pageforSchool=pageno;	
		}
		else
		{
			pageforSchool=1;
		}
		
		
		noOfRowsSchool = document.getElementById("pageSize1").value;
		showSchoolList(schoolIdsStore);
		return;
	}	
	
	
	var pageFlag=$('#geozoneschoolFlag').val();
	if(pageFlag==1){
		if(pageno!='')
		{
			pageZS=pageno;	
		}
		else
		{
			pageZS=1;
		}
		noOfRowsZS = document.getElementById("pageSizeZoneSchool").value;
		displayZoneSchoolList();
		
	}else{
		if(pageno!='')
		{
			page=pageno;	
		}
		else
		{
			page=1;
		}
		noOfRows = document.getElementById("pageSize").value;
		//shadab
		//searchRecordsByEntityType();
		searchRecordsByEntityTypeNewES();
	}
	
}

//shadab start

function runScript(characterCode){
	//alert(characterCode.keyCode);
	if(characterCode.keyCode == 13)
	{
		//searchRecordsByEntityTypeES();
		//searchBatchJobOrderES();
		searchBatchJobOrderNewES();
	}
	
}
function getESPaging(pageno)
{
	var pageFlag=$('#geozoneschoolFlag').val();
	if(pageFlag==1){
		if(pageno!='')
		{
			pageZS=pageno;	
		}
		else
		{
			pageZS=1;
		}
		noOfRowsZS = document.getElementById("pageSizeZoneSchool").value;
		displayZoneSchoolList();
		
	}else{
		if(pageno!='')
		{
			page=pageno;	
		}
		else
		{
			page=1;
		}
		noOfRows = document.getElementById("pageSize").value;
		searchRecordsByEntityTypeES();
	}
	
}

function getESPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
	var pageFlag=$('#geozoneschoolFlag').val();
	if(pageFlag==1)
	{	
		if(pageno!=''){
			pageZS=pageno;	
		}else{
			pageZS=1;
		}
		sortOrderStrZS=sortOrder;
		sortOrderTypeZS=sortOrderTyp;
		if(document.getElementById("pageSizeZoneSchool")!=null){
			noOfRowsZS = document.getElementById("pageSizeZoneSchool").value;
		}else{
			noOfRowsZS=100;
		}
		displayZoneSchoolList();
	}else
	{	
		//alert(pageFlag);
		if(pageno!=''){
			page=pageno;	
		}else{
			page=1;
		}
		sortOrderStr=sortOrder;
		sortOrderType=sortOrderTyp;
		if(document.getElementById("pageSize")!=null){
			noOfRows = document.getElementById("pageSize").value;
		}else{
			noOfRows=20;
		}
		searchRecordsByEntityTypeES();
	}	
}

//shadab end

function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
	
	if($("#gridNo").val()==2)
	{
		if(pageno!=''){
			pageforSchool=pageno;	
		}else{
			pageforSchool=1;
		}
		sortOrderStrSchool	=	sortOrder;
		sortOrderTypeSchool	=	sortOrderTyp;
		if(document.getElementById("pageSize1")!=null){
			noOfRowsSchool = document.getElementById("pageSize1").value;
		}else{
			noOfRowsSchool=50;
		}
		showSchoolList(schoolIdsStore);
		return;
	}	
	var pageFlag=$('#geozoneschoolFlag').val();
	if(pageFlag==1)
	{	
		if(pageno!=''){
			pageZS=pageno;	
		}else{
			pageZS=1;
		}
		sortOrderStrZS=sortOrder;
		sortOrderTypeZS=sortOrderTyp;
		if(document.getElementById("pageSizeZoneSchool")!=null){
			noOfRowsZS = document.getElementById("pageSizeZoneSchool").value;
		}else{
			noOfRowsZS=100;
		}
		displayZoneSchoolList();
	}else
	{	
		//alert(pageFlag);
		if(pageno!=''){
			page=pageno;	
		}else{
			page=1;
		}
		sortOrderStr=sortOrder;
		sortOrderType=sortOrderTyp;
		if(document.getElementById("pageSize")!=null){
			noOfRows = document.getElementById("pageSize").value;
		}else{
			noOfRows=20;
		}
		//shadab
		//searchRecordsByEntityType();
		searchRecordsByEntityTypeNewES();
	}	
}

function showMiamiPdf()
{
	  var left = (screen.width/2)-(600/2);
	  var top = (screen.height/2)-(700/2);
	  return window.open("http://jobs.dadeschools.net/teachers/pdf/Geo_Zones_With_Schools.pdf", "Miami Geo Zone", "width=600, height=600,top="+top+",left="+left);
}

function showPhiladelphiaPdf()
{
	  var left = (screen.width/2)-(600/2);
	  var top = (screen.height/2)-(700/2);
	  return window.open("images/District_Map_LN_8x11_2014_09_05%5B1%5D%20copy.pdf", "Miami Geo Zone", "width=600, height=600,top="+top+",left="+left);
}

function searchBatchJobOrder()
{
	page = 1;
	$('#searchLinkDiv').show();
	$('#hidesearchLinkDiv').hide();
	$("#advanceSearchDiv").hide()
	searchRecordsByEntityType();
}
/*========  For Handling session Time out Error ===============*/
function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert(resourceJSON.msgSomeerror+": "+exception.javaClassName);}
}
/*======= SearchJobOrder on Press Enter Key ========= */
function chkForEnterSearchJobOrder(evt)
{
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==13)
	{
		//shadab change this line
		//searchBatchJobOrder();
		searchBatchJobOrderNewES();
	}	
}
function showFile(districtORSchoool,districtORSchooolId,docFileName,linkId)
{
	ManageJobOrdersAjax.showFile(districtORSchoool,districtORSchooolId,docFileName,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			if(data=="")			
				alert(resourceJSON.msgJSIisnotuploaded)
			else if(data.indexOf(".doc")!=-1)
			{
				document.getElementById("iframeJSI").src=data;
			}
			else
			{
				document.getElementById(linkId).href = data; 
				return false;
			}
		}
	});
}
function addnewJob(url)
{
	var currentdate = new Date();
	var datetime =  currentdate.getDay()+""+currentdate.getMonth()+""+currentdate.getFullYear()+""+currentdate.getHours()+""+currentdate.getMinutes()+""+currentdate.getSeconds();
	url=url+""+datetime;
	window.location.href=url;
}
//******************* Global Variable for Exporting*************************
var f_geoZoneID
var f_JobOrderType
var f_districtId
var f_schoolId
//var f_subjectId		
//var f_certType
//var f_certificationsId
var f_jobOrderId
var f_status
var f_jobReqNoId
var f_resultFlag
var f_jobCategoryIds
var f_jobSubCategoryIds
var f_jobApplicationStatus
//**************************************************************************
/*========  SearchDistrictOrSchool ===============*/
function searchRecordsByEntityType()
{
	var sUrl = window.location.pathname;
    var geoZoneID=document.getElementById("zone").value;
	$('#loadingDiv').fadeIn();
	var JobOrderType    =   document.getElementById("JobOrderType").value;
	var searchTextId	=	document.getElementById("districtOrSchooHiddenlId").value;
	var schoolId		=	document.getElementById("schoolId").value;
//	var subjectId		=	document.getElementById("subjectId").value;
//	var certType			=	trim(document.getElementById("certType").value);
	//var certificationsId	=	trim(document.getElementById("certificateTypeMaster").value);
	var jobOrderId	=	document.getElementById("jobOrderId").value;
	var status	=	document.getElementById("status").value;
	var jobReqNoId=document.getElementById("reqno").value;
	
	
	//*******************   Deepak   ********************************
	
	var jobApplicationStatus =	document.getElementById("jobApplicationStatus");
	jobApplicationStatus=jobApplicationStatus.options[jobApplicationStatus.selectedIndex].value;
	//alert(jobApplicationStatus);
	var jobCategoryIds='';
    var 	jobCategoryId   = document.getElementById("jobCateSelect");
    
    for (var i = 0; i < jobCategoryId.options.length; i++) {
        if(jobCategoryId.options[i].selected ==true){
      	  if(jobCategoryIds==''){
      		  jobCategoryIds=  jobCategoryId.options[i].value;
      	  }
      	  else{
      		  jobCategoryIds=jobCategoryIds+ ","+jobCategoryId.options[i].value;
      	  }
        }
     }
	
    var 	jobSubCategoryId   = document.getElementById("jobSubCateSelect");
    var jobSubCategoryIds='';

	 for (var i = 0; i < jobSubCategoryId.options.length; i++) {
	        if(jobSubCategoryId.options[i].selected ==true){
	      	  if(jobSubCategoryIds==''){
	      		  jobSubCategoryIds=  jobSubCategoryId.options[i].value;
	      	  }
	      	  else{
	      		  jobSubCategoryIds=jobSubCategoryIds+ ","+jobSubCategoryId.options[i].value;
	      	  }
	        }
	     }
    
	 
	var resultFlag=true;
	
	if(document.getElementById("districtORSchoolName").value!="" && searchTextId==0){
		resultFlag=false;
	}
	delay(1000);
	f_geoZoneID		=	geoZoneID;
	f_JobOrderType	=   JobOrderType;
	f_districtId	=	searchTextId
	f_schoolId		=	schoolId;
//	f_subjectId		=   subjectId
//	f_certificationsId=certificationsId;
	f_jobOrderId	= 	jobOrderId;
	f_status		=	status;
	f_jobReqNoId    =	jobReqNoId;
	f_resultFlag	=	resultFlag;
	f_jobCategoryIds=	jobCategoryIds;
	f_jobSubCategoryIds = jobSubCategoryIds;
	f_jobApplicationStatus=jobApplicationStatus;
	
	
	
	
	
	
	if(sUrl.indexOf("managejobordersnew") > -1 || sUrl.indexOf("schooljobordersnew") > -1){
		//alert('enter999');
		ManageJobOrdersAjax.displayRecordsByEntityType(resultFlag,JobOrderType,searchTextId,schoolId,/*subjectId,
				certificationsId,*/jobOrderId,status,noOfRows,page,sortOrderStr,sortOrderType,jobReqNoId,geoZoneID,jobCategoryIds,jobApplicationStatus,jobSubCategoryIds,{ 
			async: true,
			callback: function(data)
			{	
				$('#divMain').html(data);
				tpJbIEnable();
				applyScrollOnTbl();	
				$('#loadingDiv').hide();
				$('#iconpophover1').tooltip();
			},
		errorHandler:handleError
		});
 }else{
	 //alert('enter99911');
	 ManageJobOrdersAjax.displayRecordsByEntityTypeNew(resultFlag,JobOrderType,searchTextId,schoolId,/*subjectId,
				certificationsId,*/jobOrderId,status,noOfRows,page,sortOrderStr,sortOrderType,jobReqNoId,geoZoneID,jobCategoryIds,jobApplicationStatus,jobSubCategoryIds,{    //  Adding jobApplicationStatus paramater by deepak 
			async: true,
			callback: function(data)
			{	
				$('#divMain').html(data);
				tpJbIEnable();
				applyScrollOnTbl();	
				$('#loadingDiv').hide();
				$('#iconpophover1').tooltip();

				//added by 27-05-2015   //804800
				//alert("searchTextId==="+searchTextId+"==="+804800);
				if(searchTextId=='804800'){
				try{
						var obj=document.getElementById("jobApplicationStatus");											
						for(var i=0;i<obj.length;i++){
							//alert(obj.options[i].value+"=="+jobApplicationStatus);
							if(obj.options[i].value==jobApplicationStatus){	
								$(obj.options[i]).attr('selected','selected');							
							}
						}						
					}catch(e){}
				}
			    //ended by 27-05-2015
					
					
			},
		errorHandler:handleError
		});
 }
}


	
/*========  displayJobRecords ===============*/
function displayJobRecords()
{
	var sUrl = window.location.pathname;
	$('#loadingDiv').fadeIn();
	var districtId	=	0;
	var schoolId 	=	0;
	var entityType=document.getElementById("entityType").value;
	if(entityType==1)
	{
		$('#schoolName').attr('readonly', true);
		document.getElementById('schoolName').value="";
		document.getElementById('schoolId').value="0";
	}
	if(entityType==2){
		districtId	=	document.getElementById("districtOrSchooHiddenlId").value;
		// jeffco added by 27-05-2015
		try{
			if(districtId=='804800'){
				$('.jobStatusJeffco').show();
				$('#zone_id').attr('class','col-sm-3 col-md-3');
			}else{
				$('#zone_id').attr('class','col-sm-6 col-md-6');
			}
		}catch(e){}
		//jeffco ended by 27-05-2015
	}
	if(entityType==3)
	{
		districtId		=	document.getElementById("districtOrSchooHiddenlId").value;
		schoolId		=	document.getElementById("schoolId").value;
	}	
	var JobOrderType	=null;	
	try{
		JobOrderType=document.getElementById("JobOrderType").value;
	}catch(err){
		JobOrderType=0;
	}
	if(JobOrderType	==	"" || JobOrderType	==	null)
	{
		JobOrderType	=0;
	}
	if(JobOrderType==2){
		document.getElementById("captionDistrictOrSchool").innerHTML	=	resourceJSON.msgDistrictName1;
		document.getElementById("districtOrSchoolTitle").innerHTML		=	resourceJSON.msgDistrict;
	}
	
	var status	=	document.getElementById("status").value;
	var jobReqNoId=trim(document.getElementById("reqno").value);
	var geoZoneID=document.getElementById("zone").value;
	
	//**********************                Deepak  ***************************             
	var jobApplicationStatus =	document.getElementById("jobApplicationStatus");
	jobApplicationStatus=jobApplicationStatus.options[jobApplicationStatus.selectedIndex].value;
	//alert(jobApplicationStatus);
	var jobCategoryIds='';
    var 	jobCategoryId   = document.getElementById("jobCateSelect");
    
   // alert(" jobCategoryId :: "+jobCategoryId);
    
    for (var i = 0; i < jobCategoryId.options.length; i++) {
    	
    	
    	
        if(jobCategoryId.options[i].selected ==true){
      	  if(jobCategoryIds==''){
     // 		alert(" jobCategoryId.options[i].value "+jobCategoryIds);
      		  jobCategoryIds=  jobCategoryId.options[i].value;
      	  }
      	  else{
      		  jobCategoryIds=jobCategoryIds+ ","+jobCategoryId.options[i].value;
      	  }
        }
     }

	
	
	
	f_geoZoneID		=	geoZoneID;
	f_JobOrderType	=   JobOrderType;
	f_districtId	=	districtId
	f_schoolId		=	schoolId;
//	f_subjectId		=   "";
//	f_certificationsId="";
	f_jobOrderId	= 	0;
	f_status		=	status;
	f_jobReqNoId    =	jobReqNoId;
	f_resultFlag	=	true;
	f_jobCategoryIds=	jobCategoryIds;
	f_jobApplicationStatus=jobApplicationStatus;
	
	delay(1000);
	if(sUrl.indexOf("managejobordersnew") > -1 || sUrl.indexOf("schooljobordersnew") > -1){
		//alert("new call");
		ManageJobOrdersAjax.displayRecordsByEntityType(true,JobOrderType,districtId,schoolId/*,"",""*/,0,status,
				noOfRows,page,sortOrderStr,sortOrderType,jobReqNoId,geoZoneID,jobApplicationStatus,jobCategoryIds,{           //Adding jobApplicationStatus parameter by Deepak
			async: true,
			callback: function(data)
			{
				$('#geozoneschoolFlag').val(0);
				$('#divMain').html(data);
				tpJbIEnable();
				applyScrollOnTbl();	
				$('#loadingDiv').hide();
				$('#iconpophover1').tooltip();
			},
			errorHandler:handleError
		});
		
	}else{
		//alert("old call");
	ManageJobOrdersAjax.displayRecordsByEntityTypeNew(true,JobOrderType,districtId,schoolId/*,"",""*/,0,status,
			noOfRows,page,sortOrderStr,sortOrderType,jobReqNoId,geoZoneID,jobApplicationStatus,jobCategoryIds,{              //Adding jobApplicationStatus parameter by Deepak
		async: true,
		callback: function(data)
		{
			$('#geozoneschoolFlag').val(0);
			$('#divMain').html(data);
			tpJbIEnable();
			applyScrollOnTbl();	
			
			$('#loadingDiv').hide();
			$('#iconpophover1').tooltip();
		},
		errorHandler:handleError
	});
}
	getJobCategoryByDistrict();
}
function tpJbIEnable()
{
	var noOrRow = document.getElementById("tblGrid").rows.length;
	for(var j=1;j<=noOrRow;j++)
	{	
		$('#cg'+j).tooltip();
		$('#cgn'+j).tooltip();
		$('.attchToolTip'+j).tooltip();
		$('#edit'+j).tooltip();
	}
}
function delay(sec){
	var starttime = new Date().getTime();
	starttime = starttime+sec;
	while(true){
		if(starttime< new Date().getTime()){
			break;
		}
	}
}
/*========  activateDeactivateUser ===============*/
function activateDeactivateJob(entityID,jobid,status)
{
	ManageJobOrdersAjax.activateDeactivateJob(jobid,status, { 
		async: true,
		callback: function(data)
		{
			//displayJobRecords(entityID);
		searchBatchJobOrderNewES();
		},
		errorHandler:handleError
	});
}
var DistrictId;
function displayZoneSchoolList()
{
	var geozoneId=$('#geozoneId').val();
	delay(1000);
	
	ManageJobOrdersAjax.displayGeoZoneSchoolGrid(geozoneId,DistrictId,noOfRowsZS,pageZS,sortOrderStrZS,sortOrderTypeZS,{ 
		async: true,
		callback: function(data){
		$('#geozoneId').val(geozoneId);
		$('#geozoneschoolFlag').val(1);
		$("#geozoneschoolDiv").modal('show');
		$("#geozoneschoolGrid").html(data);
		applyScrollOnTblGeoZoneSchool();
	},
	errorHandler:handleError 
	});
}

function showZoneSchoolList(geozoneId,geozoneName,districtId)
{  DistrictId=districtId
	pageZS=1;
	noOfRowsZS = 10;
	delay(1000);
	$('#loadingDiv').show();
	$("#myModalLabelGeo").html(geozoneName);
	ManageJobOrdersAjax.displayGeoZoneSchoolGrid(geozoneId,districtId,noOfRowsZS,pageZS,sortOrderStrZS,sortOrderTypeZS,{ 
		async: true,
		callback: function(data){
		$('#geozoneId').val(geozoneId);
		$('#geozoneschoolFlag').val(1);
		$("#geozoneschoolDiv").modal('show');
		$("#geozoneschoolGrid").html(data);
		applyScrollOnTblGeoZoneSchool();
		$('#loadingDiv').hide();
	},
	errorHandler:handleError 
	});
}

/**************************************************************************************************************************************/

var count=0;
var index=-1;
var length=0;
var divid='';
var txtid='';
var hiddenDataArray = new Array();
var showDataArray = new Array();
var degreeTypeArray = new Array();
var hiddenId="";
function getDistrictORSchoolAuto(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("districtORSchoolName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getDistrictORSchoolArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getDistrictORSchoolArray(districtOrSchoolName){
	var searchArray = new Array();
	/*==== Gagan : District Auto Complete will show for each case ========*/
	ManageJobOrdersAjax.getFieldOfDistrictList(districtOrSchoolName,{ 
		async: false,
		callback: function(data){
		hiddenDataArray = new Array();
		showDataArray = new Array();
		for(i=0;i<data.length;i++){
			searchArray[i]=data[i].districtName;
			showDataArray[i]=data[i].districtName;
			hiddenDataArray[i]=data[i].districtId;
		}
	},
	errorHandler:handleError 
	});	
	
	return searchArray;
}


var selectFirst="";

var fatchData= function(txtSearch,searchArray,txtId,txtdivid){
	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="") {

			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;

				if(count==10)
					break;
			}
		} else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}

		scrolButtom();
	}catch (err){}
}

function hideDistrictMasterDiv(dis,hiddenId,divId)
{
	document.getElementById("districtOrSchooHiddenlId").value="";
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		if(dis.value==""){
			$('#schoolName').attr('readonly', true);
			document.getElementById('schoolName').value="";
			document.getElementById('schoolId').value="0";
			document.getElementById(hiddenId).value="";
		} else if(showDataArray && showDataArray[index]) {
			dis.value=showDataArray[index];
			document.getElementById("districtOrSchooHiddenlId").value=hiddenDataArray[index];
			document.getElementById('schoolName').readOnly=false;
			document.getElementById('schoolName').value="";
			document.getElementById('schoolId').value="0";
		}
		
	} else {
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
	}

	if(document.getElementById(divId)) {
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}

/*---------------------------------School Auto Complete Js Starts ----------------------------------------------------------------*/
function getSchoolAuto(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("schoolName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getSchoolArray(txtSearch.value);
		fatchData2(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getSchoolArray(schoolName){
	var searchArray = new Array();
	var JobOrderType	=	document.getElementById("JobOrderType").value;
	var districtId	=	document.getElementById("districtOrSchooHiddenlId").value;
		BatchJobOrdersAjax.getFieldOfSchoolList(districtId,schoolName,{ 
			async: false,
			callback: function(data){
			hiddenDataArray = new Array();
			showDataArray = new Array();
			for(i=0;i<data.length;i++){
				searchArray[i]=data[i].schoolName;
				showDataArray[i]=data[i].schoolName;
				hiddenDataArray[i]=data[i].schoolMaster.schoolId;
			}
		},
		errorHandler:handleError
		});	
	return searchArray;
}


selectFirst="";

var fatchData2= function(txtSearch,searchArray,txtId,txtdivid){

	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
			}
			
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}
function hideSchoolMasterDiv(dis,hiddenId,divId)
{
	document.getElementById("schoolId").value="";
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			document.getElementById("schoolId").value=hiddenDataArray[index];
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}

/*--------------------------------- End  School Auto Complete Js  ----------------------------------------------------------------*/
var downArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index<length-1){
			for(var i=0;i<10;i++){
				{
					if(document.getElementById('divResult'+txtdivid+i))
						var div_id=document.getElementById('divResult'+txtdivid+i);
					if(div_id)
					{
						if(div_id.className=='over')
							index=div_id.id.split('divResult'+txtdivid)[1];
						div_id.className='normal';
					}
				}
			}
			index++;
			if(document.getElementById('divResult'+txtdivid+index))
			{
				var div_id=document.getElementById('divResult'+txtdivid+index);
				div_id.className='over';
				document.getElementById(txtid).value = div_id.innerHTML;
				selectFirst=div_id.innerHTML;
			}
		}
	}
}

var upArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index>0){
			for(var i=0;i<length;i++){

				var div_id=document.getElementById('divResult'+txtdivid+i);
				if(div_id)
				{
					if(div_id.className=='over')
						index=div_id.id.split('divResult'+txtdivid)[1];
					div_id.className='normal';
				}
			}
			index--;
			if(document.getElementById('divResult'+txtdivid+index))
				document.getElementById('divResult'+txtdivid+index).className='over';
			if(txtid && document.getElementById('divResult'+txtdivid+index))
			{
				document.getElementById(txtid).value=
					document.getElementById('divResult'+txtdivid+index).innerHTML;
				selectFirst=document.getElementById('divResult'+txtdivid+index).innerHTML;
			}
		}
	}
}

var overText=function (div_value,txtdivid) {

	for(var i=0;i<length;i++)
	{
		if(document.getElementById('divResult'+i))
		{
			var div_id=document.getElementById('divResult'+i);

			if(div_id.className=='over')
				index=div_id.id.split(txtdivid)[1];
			div_id.className='normal';
		}
	}
	div_value.className = 'over';
	document.getElementById(txtid).value= div_value.innerHTML;
}
function trim(s)
{
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}
function checkForInt(evt) {

	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==8)
		return true;

	return( (charCode >= 48 && charCode <= 57)||(charCode==13) );
}
function mouseOverChk(txtdivid,txtboxId)
{	
	for(var i=0;i<length;i++)
	{
		$('#divResult'+txtdivid+i).mouseover({param1: i,param2:txtdivid, param3:txtboxId}, fireMouseOverEvent);
	}
}


function fireMouseOverEvent(event)
{
	for(var i=0;i<length;i++)
	{	
		document.getElementById('divResult'+event.data.param2+i).className='normal';
	}
    document.getElementById('divResult'+event.data.param2+event.data.param1).className='over';	       
   	document.getElementById(event.data.param3).value= $('#divResult'+event.data.param2+event.data.param1).text();
    index=event.data.param1;
}

function getAllCertificateNameAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38)
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13)
	{
		
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		document.getElementById("hrefDone").focus();
		
	} 
	else if(event.keyCode==9)
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getAllCertificateNameArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getAllCertificateNameArray(certName){
	
	var searchArray = new Array();	
	DWRAutoComplete.getAllCertificateNameList(certName,{ 
		async: false,
		callback: function(data){
		hiddenDataArray = new Array();
		showDataArray = new Array();
		for(i=0;i<data.length;i++){
			searchArray[i]=data[i].certName;
			hiddenDataArray[i]=data[i].certNameId;
			showDataArray[i]=data[i].certName;
		}
	}
	});	

	return searchArray;
}
function hideAllCertificateNameDiv(dis,hiddenId,divId)
{
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
		if(dis.value!="")
		{
			var cnt=0;
			var focs=0;	
			$('#errordiv').empty();	
			setDefColortoErrorMsg();
			$('#errordiv').append("&#149; "+resourceJSON.msgcertificatenamebased+"<br>");
			if(focs==0)
				$('#certName').focus();
			
			$('#certName').css("background-color",txtBgColor);
			cnt++;focs++;	
		}
		else
		{
			$('#errordiv').empty();	
			setDefColortoErrorMsg();
		}
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}
function showJobStatus(){
	//added by 27-05-2015
	try{
		districtId	=	document.getElementById("districtOrSchooHiddenlId").value;
			if(districtId=='804800'){
				$('.jobStatusJeffco').show();
				$('#zone_id').attr('class','col-sm-3 col-md-3');
			}else{
				$('.jobStatusJeffco').hide();
				$('#zone_id').attr('class','col-sm-6 col-md-6');
			}
	}catch(e){}
	//ended by 27-05-2015
}
function getZoneList()
{
	showJobStatus();
	var districtOrSchooHiddenlId=document.getElementById("districtOrSchooHiddenlId").value
	ManageJobOrdersAjax.getZoneListAll(districtOrSchooHiddenlId,{ 
		async: false,		
		callback: function(data)
		{
			if(data!=null && data!="")
			{			
				$("#zone").html(data);
				$("#zone").prop('disabled', false);
			}
			else{
				$("#zone").html(data);
				$("#zone").prop('disabled', true);
			}
		
		}
	});
}
function getList(districtOrSchooHiddenlId,entityType)
{	
	ManageJobOrdersAjax.getZoneListAll(districtOrSchooHiddenlId,{ 
		async: false,		
		callback: function(data)
		{
			if(data!=null && data!="")
			{			
				$("#zone").html(data);
				$("#zone").prop('disabled', false);
			}
			else{
				$("#zone").html(data);
				$("#zone").prop('disabled', true);
			}
			if(entityType!=1){
				//shadab commenting below lines
				//displayJobRecords();
				//searchBatchJobOrderES();
				searchBatchJobOrderNewES();
			}
		}
	});
}


var isSafari = /Safari/.test(navigator.userAgent) && /Apple Computer/.test(navigator.vendor);
function printAllJobs()
{	
	$('#loadingDiv').fadeIn();
	ManageJobOrdersAjax.displayRecordsByEntityTypeforPrinting(f_resultFlag,f_JobOrderType,f_districtId,f_schoolId,/*f_subjectId,
			f_certificationsId,*/f_jobOrderId,f_status,noOfRows,page,sortOrderStr,sortOrderType,f_jobReqNoId,f_geoZoneID,f_jobCategoryIds,f_jobApplicationStatus,f_jobSubCategoryIds,{ 
		async: true,
		callback: function(data)
		{
		$('#loadingDiv').hide();
			 try{
		       if (isSafari && !deviceType)
			    {
			    	window.document.write(data);
					window.print();
			    }else
			    {
			    	var newWindow = window.open();
			    	newWindow.document.write(data);	
			    	newWindow.print();
				 } 
			 }catch (e) 
				{
					$('#printmessage').modal('show');							 
				}
			},
			errorHandler:handleError
		});
}

function canelPrintData(){
	$('#printAllJobDiv').hide();
	}
function canelPrintDataforDistrict(){
	$('#districtAllJobReportModelDiv').hide();
	}

function districtAlljobsReport()
{
	$('#loadingDiv').fadeIn();
	$('#loadingDiv').show();
	districtId	=	document.getElementById("districtOrSchooHiddenlId").value;
	ManageJobOrdersAjax.generateCompleteDistrictReport(districtId,{ 
	async: true,		
	callback: function(data)
	{
		$('#districtAllJobReportTableDiv').html(data);
		$("#districtAllJobReportModelDiv").modal('show');
		$('#loadingDiv').hide();
	}
});
}

function downloadAttachment(filePath,fileName) {
	ManageJobOrdersAjax.downloadAttachment(filePath,fileName,{ 
	async: true,
	errorHandler:handleError,
	callback:function(data)
	{
		if(deviceType)
		{
			if (data.indexOf(".doc")!=-1)
			{
				$("#docfileNotOpen").css({"z-index":"3000"});
		    	$('#docfileNotOpen').show();
			}
			else if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
			{
				$("#exelfileNotOpen").css({"z-index":"3000"});
		    	$('#exelfileNotOpen').show();
			}
			else
			{
				document.getElementById(linkid).href = data;
			}
		}
		else if(deviceTypeAndroid)
		{
			if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
			{
				 $('#modalDownloadsAttachment').modal('hide');
				 document.getElementById('ifrmAttachment').src = ""+data+"";
			}
			else
			{
				document.getElementById(linkid).href = data;
			}
		}
		else if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
		{
			 $('#modalDownloadsAttachment').modal('hide');
			 document.getElementById('ifrmAttachment').src = ""+data+"";
		} else {
				$('.distAttachment').modal('show');
				document.getElementById('ifrmAttachment').src = ""+data+"";
		}
	}});
}

function downloadJobOrderExcelReport()
{
	//alert('f_jobApplicationStatus==========='+f_jobApplicationStatus);
 $('#loadingDiv').fadeIn();
 ManageJobOrdersAjax.displayRecordsByEntityTypeExcelExport(f_resultFlag,f_JobOrderType,f_districtId,f_schoolId,/*f_subjectId,
		f_certificationsId,*/f_jobOrderId,f_status,noOfRows,page,sortOrderStr,sortOrderType,f_jobReqNoId,f_geoZoneID,f_jobCategoryIds,f_jobApplicationStatus,f_jobSubCategoryIds,{ 
	async: true,
	callback: function(data)
	{	
	    $('#loadingDiv').hide();
		if(deviceType)
		{					
				$("#exelfileNotOpen").css({"z-index":"3000"});
		    	$('#exelfileNotOpen').show();
				
		}
		else
		{
			try
			{
				document.getElementById('ifrmTrans').src = "managejoborder/"+data+"";
			}
			catch(e)
			{alert(e);}
		}
	
	},
errorHandler:handleError
});
}

function downloadJobOrderPDFReport()
{
 $('#loadingDiv').fadeIn();
 if(f_JobOrderType==2)
	 document.getElementById("pdfDistrictOrSchoolTitle").innerHTML		=	resourceJSON.msgDistrict;
 else if(f_JobOrderType==3)
	 document.getElementById("pdfDistrictOrSchoolTitle").innerHTML		=	resourceJSON.msgSchool;
	 
 ManageJobOrdersAjax.displayRecordsByEntityTypePDFExport(f_resultFlag,f_JobOrderType,f_districtId,f_schoolId,/*f_subjectId,
		f_certificationsId,*/f_jobOrderId,f_status,noOfRows,page,sortOrderStr,sortOrderType,f_jobReqNoId,f_geoZoneID,f_jobCategoryIds,f_jobApplicationStatus,f_jobSubCategoryIds,{ 
	async: true,
	callback: function(data)
	{	
		$('#loadingDiv').hide();	
		if(deviceType || deviceTypeAndroid)
		{
			window.open(data,'left=200,top=50,width=700,height=600,toolbar=1,resizable=0');			
		}		
		else
		{
			$('#modalDownloadJobOrder').modal('hide');
			document.getElementById('ifrmCJS').src = ""+data+"";
			try{
				$('#modalDownloadJobOrder').modal('show');
			}catch(err){}
	     }		
	     return false;
},
errorHandler:handleError
});
}

function approveJob(entityID,jobid,userId)
{
	ManageJobOrdersAjax.approveJob(entityID,jobid,userId, { 
		async: true,
		callback: function(data)
		{
			/*
			 * return "2";// already approved
				return "4";//Approve job
				return "3";// no need to approve this job
				return "0";
			 */
		
			var jobTital = data[0];
			var dData = data[1];
			//alert(data);
			if(dData=="1")
			{
				$('#message2show').html(resourceJSON.msgAuthorizedApproveJob+" \""+jobTital+"\"");
				$('#myModal2').modal('show');
			}else if(dData=="2")
			{
				$('#message2show').html(""+resourceJSON.msgThejob+" \""+jobTital+"\" "+resourceJSON.msgIsAlreadyApproved);
				$('#myModal2').modal('show');
			}else if(dData=="3")
			{
				$('#message2show').html(resourceJSON.msgNoNeedApproveJob+" \""+jobTital+"\"");
				$('#myModal2').modal('show');
			}else if(dData=="4")
			{
				/*$('#message2show').html("You can approve this job.");
				$('#myModal2').modal('show');*/
				
				$('#warningImg1k').html("");
				$('#message2showConfirm1k').html("");
				$('#nextMsgk').html(""+resourceJSON.msgReallyWantApproveJob+" \""+jobTital+"\"?");
				$('#footerbtn1k').html("<button class='btn btn-primary' data-dismiss='modal' aria-hidden='true' onclick=\"approveJobOk("+entityID+","+jobid+","+userId+");\">Approve</button>&nbsp;<button class='btn' data-dismiss='modal' aria-hidden='true' >"+resourceJSON.btnCancel+"</button>");
				$('#vcloseBtnk').html("<span>x</span>");
				$('#myModalvk').modal('show');
			}
			else if(dData=="5")
			{
				$('#message2show').html(resourceJSON.msgThisjob+" \""+jobTital+"\" "+resourceJSON.msgIsAlreadyApprovedbyyou);
				$('#myModal2').modal('show');
			}else if(dData=="6")
			{
				$('#message2show').html(resourceJSON.deactivateJobApprovalProcess);
				$('#myModal2').modal('show');
			}
			else
			{
				$('#message2show').html(resourceJSON.msgApprovethisjob);
				$('#myModal2').modal('show');
			}
				
		},
		errorHandler:handleError
	});
}
function approveJobOk(entityID,jobid,userId)
{
	ManageJobOrdersAjax.approveJobOk(entityID,jobid,userId, { 
		async: true,
		callback: function(data)
		{
//		   displayJobRecords();
		    searchBatchJobOrderNewES();
			$('#message2show').html(resourceJSON.msgJobApprovedSuccessfully);
			$('#myModal2').modal('show');
		},
		errorHandler:handleError
	});
}

function displayAdvanceSearch()
{
	$('#searchLinkDiv').hide();
	$('#hidesearchLinkDiv').show();
	$('#advanceSearchDiv').slideDown('slow');
	//$('#jobApplicationStatus option[value=0]').prop('selected', true);              // Adding By Deepak
}

function hideAdvanceSearch()
{
	$('#searchLinkDiv').show();
	$('#hidesearchLinkDiv').hide();
	$('#advanceSearchDiv').slideUp('slow');
}

function getJobCategoryByDistrict()
{
	var districtId = document.getElementById("districtOrSchooHiddenlId").value;
	
	if(districtId!=0)
	{
		//DWRAutoComplete
		DWRAutoComplete.getJobCategoryByDistrict(districtId,{ 
			async: false,		
			callback: function(data)
			{
				document.getElementById("jobCateSelect").innerHTML=data;
			}
		});
	}
	else
	{
		document.getElementById("jobCateSelect").innerHTML="<option value='0'>"+resourceJSON.msgDistrictJobsCategorylist+"</option>";
	}
	
}

function getJobSubcategory()
{
	var jobSubCategoryId=document.getElementById("jobCateSelect");
	var jobId=0;
	var districtId = trim(document.getElementById("districtOrSchooHiddenlId").value);
	var clonejobId = 0;
	
	var jobSubCategoryIds='';

	 for (var i = 0; i < jobSubCategoryId.options.length; i++) {
	        if(jobSubCategoryId.options[i].selected ==true){
	      	  if(jobSubCategoryIds==''){
	      		  jobSubCategoryIds=  jobSubCategoryId.options[i].value;
	      	  }
	      	  else{
	      		  jobSubCategoryIds=jobSubCategoryIds+ ","+jobSubCategoryId.options[i].value;
	      	  }
	        }
	     }
	 
	if(districtId!="")
	{
			ManageJobOrdersAjax.getJobSubCategoryForSeach(jobId,jobSubCategoryIds,districtId,clonejobId,
			{ 
				async: true,
				errorHandler:handleError,
				callback: function(data)
				{
					if(data!='')
					{
						document.getElementById("jobSubCateSelect").innerHTML=data;
					}
				}
			});
	}
}


//shadab start for schooljoborder
function searchRecordsByEntityTypeNewES()
{
	//alert("inside searchRecordsByEntityTypeNewES");
	//try { getJobCategoryByDistrict(); } catch (e) {}
	
	
	var hiddenLabel="";
	var hiddedSearch=false;
	var sUrl = window.location.pathname;
    var geoZoneID=document.getElementById("zone").value;
    if(geoZoneID.trim()!="" && geoZoneID!="0" && geoZoneID!="-1")
    {
    	hiddenLabel+=resourceJSON.lblZone+" || ";
    	hiddedSearch=true;
    }	
	$('#loadingDiv').fadeIn();
	var districtId	=	0;
	var schoolId 	=	0;
	var entityType=document.getElementById("entityType").value;
	if(entityType==1)
	{
		$('#schoolName').attr('readonly', true);
		//document.getElementById('schoolName').value="";
		//document.getElementById('schoolId').value="0";
		districtId	=	document.getElementById("districtOrSchooHiddenlId").value;
		schoolId		=	document.getElementById("schoolId").value;
	}
	if(entityType==2){
		districtId	=	document.getElementById("districtOrSchooHiddenlId").value;
		schoolId		=	document.getElementById("schoolId").value;
		// jeffco added by 27-05-2015
		try{
			if(districtId=='804800'){
				$('.jobStatusJeffco').show();
				$('#zone_id').attr('class','col-sm-3 col-md-3');
			}else{
				$('#zone_id').attr('class','col-sm-6 col-md-6');
			}
		}catch(e){}
		//jeffco ended by 27-05-2015
	}
	if(entityType==3)
	{
		districtId		=	document.getElementById("districtOrSchooHiddenlId").value;
		schoolId		=	document.getElementById("schoolId").value;
	}	
	var JobOrderType	=null;	
	try{
		JobOrderType=document.getElementById("JobOrderType").value;
	}catch(err){
		JobOrderType=0;
	}
	if(JobOrderType	==	"" || JobOrderType	==	null)
	{
		JobOrderType	=0;
	}
	if(JobOrderType==2){
		document.getElementById("captionDistrictOrSchool").innerHTML	=	resourceJSON.msgDistrictName1;
		document.getElementById("districtOrSchoolTitle").innerHTML		=	resourceJSON.msgDistrict;
	}
	
	/*if(districtId=='804800'){
		$('#jeffcoButtonSearchShow').show();
		$('#jeffcoButtonSearchHide').hide();
		displayJobRecords();
		return false;
	}*/
	$('#jeffcoButtonSearchShow').hide();
	$('#jeffcoButtonSearchHide').show();
	
	
	
	
	
	//var JobOrderType    =   document.getElementById("JobOrderType").value;
	//var searchTextId	=	document.getElementById("districtOrSchooHiddenlId").value;
	//var schoolId		=	document.getElementById("schoolId").value;
//	var subjectId		=	document.getElementById("subjectId").value;
//	var certType			=	trim(document.getElementById("certType").value);
	//var certificationsId	=	trim(document.getElementById("certificateTypeMaster").value);
	var jobOrderId	=	document.getElementById("jobOrderId").value;
	var status	=	document.getElementById("status").value;
	var jobReqNoId=document.getElementById("reqno").value;
	
	
	
	//**********************                Deepak  ***************************             
	var jobApplicationStatus =	document.getElementById("jobApplicationStatus");
	jobApplicationStatus=jobApplicationStatus.options[jobApplicationStatus.selectedIndex].value;
	//alert(jobApplicationStatus);
	
	
	
	var jobCategoryIds='';
    var 	jobCategoryId   = document.getElementById("jobCateSelect");
        
    for (var i = 0; i < jobCategoryId.options.length; i++) {
        if(jobCategoryId.options[i].selected ==true){
        	  if(jobCategoryIds==''){
      		  jobCategoryIds=  jobCategoryId.options[i].value;
      	  }
      	  else{
      		  jobCategoryIds=jobCategoryIds+ ","+jobCategoryId.options[i].value;
      	  }
        }
     }
    
    if(jobCategoryIds.trim()!="" && jobCategoryIds!="0")
    {
    	hiddenLabel+=resourceJSON.lblJobCategory+" || ";
    	hiddedSearch=true;
    }
    var jobSubCateSelect="";
    try
    {
    	jobSubCateSelect=document.getElementById("jobSubCateSelect").value;
    }
    catch(err){}
    if(jobSubCateSelect!="" && jobSubCateSelect!="0" && jobSubCateSelect!="-1")
    {
    	hiddenLabel+=resourceJSON.lblJobSubCategory+"   ";
    	hiddedSearch=true;
    }
    if(hiddedSearch)
    {
    	$("#advLabel").show();
    	$("#filterActiveLbl").show();
 		$("#advsearch").hide();
    }
    else
    {
    	$("#advLabel").hide();
    	$("#filterActiveLbl").hide();
    	$("#advsearch").show();
    }	
    $("#advanceCheckTooltip").text(hiddenLabel.substring(0, hiddenLabel.length - 3));
    
	var resultFlag=true;		
	
	
	
	if(document.getElementById("districtORSchoolName").value!="" && districtId==0){
		resultFlag=false;
	}
	delay(1000);
	f_geoZoneID		=	geoZoneID;
	f_JobOrderType	=   JobOrderType;
	f_districtId	=	districtId
	f_schoolId		=	schoolId;
//	f_subjectId		=   subjectId
//	f_certificationsId=certificationsId;
	f_jobOrderId	= 	jobOrderId;
	f_status		=	status;
	f_jobReqNoId    =	jobReqNoId;
	f_resultFlag	=	resultFlag;
	f_jobCategoryIds=	jobCategoryIds;
	f_jobApplicationStatus=jobApplicationStatus;
	
	//alert("jobCategoryIds====="+jobCategoryIds);
	 ManageJobOrdersAjax.displayRecordsByEntityTypeNewESNew(resultFlag,JobOrderType,districtId,schoolId,jobOrderId,status,noOfRows,page,sortOrderStr,sortOrderType,jobReqNoId,geoZoneID,jobCategoryIds,$("#searchTerm").val(),jobApplicationStatus,{ 
			async: true,
			callback: function(data)
			{	
				$('#divMain').html(data);
				tpJbIEnable();
				applyScrollOnTbl();	
				$('#loadingDiv').hide();
				$('#iconpophover1').tooltip();
			},
		errorHandler:handleError
		});
}
//shadab end for schooljoborder

//shadab start for districtjoborder
function searchBatchJobOrderES()
{//alert("searchBatchJobOrderES")
	page = 1;
	sortOrderStr='jobId';
	
	$('#searchLinkDiv').show();
	$('#hidesearchLinkDiv').hide();
	$("#advanceSearchDiv").hide()
	searchRecordsByEntityTypeES();
}
function searchBatchJobOrderNewES()
{
	page = 1;
	sortOrderStr='jobId';
	
	$('#searchLinkDiv').show();
	$('#hidesearchLinkDiv').hide();
	$("#advanceSearchDiv").hide()
	searchRecordsByEntityTypeNewES();
}

function searchRecordsByEntityTypeES()
{
	try { getJobCategoryByDistrict(); } catch (e) {}
	
	var sUrl = window.location.pathname;
    var geoZoneID=document.getElementById("zone").value;
	$('#loadingDiv').fadeIn();
	var JobOrderType    =   document.getElementById("JobOrderType").value;
	var searchTextId	=	document.getElementById("districtOrSchooHiddenlId").value;
	var schoolId		=	document.getElementById("schoolId").value;
//	var subjectId		=	document.getElementById("subjectId").value;
//	var certType			=	trim(document.getElementById("certType").value);
	//var certificationsId	=	trim(document.getElementById("certificateTypeMaster").value);
	var jobOrderId	=	document.getElementById("jobOrderId").value;
	var status	=	document.getElementById("status").value;
	var jobReqNoId=document.getElementById("reqno").value;
	
	var jobCategoryIds='';
	var jobCategoryName='';
    var 	jobCategoryId   = document.getElementById("jobCateSelect");
    
    for (var i = 0; i < jobCategoryId.options.length; i++) {
        if(jobCategoryId.options[i].selected ==true){
      	  if(jobCategoryIds==''){
      		  jobCategoryIds=  jobCategoryId.options[i].value;
      		jobCategoryName=jobCategoryName+jobCategoryId.options[i].text;
      	  }
      	  else{
      		  jobCategoryIds=jobCategoryIds+ ","+jobCategoryId.options[i].value;
      		jobCategoryName=jobCategoryName+","+jobCategoryId.options[i].text;
      	  }
        }
     }
	
	var resultFlag=true;		
	if(document.getElementById("districtORSchoolName").value!="" && searchTextId==0){
		resultFlag=false;
	}
	delay(1000);
	f_geoZoneID		=	geoZoneID;
	f_JobOrderType	=   JobOrderType;
	f_districtId	=	searchTextId
	f_schoolId		=	schoolId;
//	f_subjectId		=   subjectId
//	f_certificationsId=certificationsId;
	f_jobOrderId	= 	jobOrderId;
	f_status		=	status;
	f_jobReqNoId    =	jobReqNoId;
	f_resultFlag	=	resultFlag;
	f_jobCategoryIds=	jobCategoryIds;
	
	
	
	
	
	
	if(sUrl.indexOf("managejobordersnew") > -1 || sUrl.indexOf("schooljobordersnew") > -1){
		ManageJobOrdersAjax.displayRecordsByEntityType(resultFlag,JobOrderType,searchTextId,schoolId,/*subjectId,
				certificationsId,*/jobOrderId,status,noOfRows,page,sortOrderStr,sortOrderType,jobReqNoId,geoZoneID,jobCategoryIds,{ 
			async: true,
			callback: function(data)
			{	
				$('#divMain').html(data);
				tpJbIEnable();
				applyScrollOnTbl();	
				$('#loadingDiv').hide();
				$('#iconpophover1').tooltip();
			},
		errorHandler:handleError
		});
 }else{//alert($("#searchTerm").val());
	
	
	 if(JobOrderType==2)
	 {
		 ManageJobOrdersAjax.displayRecordsByEntityTypeNewES(resultFlag,JobOrderType,searchTextId,schoolId,/*subjectId,
					certificationsId,*/jobOrderId,status,noOfRows,page,sortOrderStr,sortOrderType,jobReqNoId,geoZoneID,jobCategoryIds,jobCategoryName,$("#searchTerm").val(),{ 
				async: true,
				callback: function(data)
				{	
					$('#divMain').html(data);
					tpJbIEnable();
					applyScrollOnTbl();	
					$('#loadingDiv').hide();
					$('#iconpophover1').tooltip();
				},
			errorHandler:handleError
			});
	 }
	 else
	{
		 searchBatchJobOrder();
	}
	 
	 
 }
}


function displayJobRecordsES()
{
	try { getJobCategoryByDistrict(); } catch (e) {}
	var sUrl = window.location.pathname;
    var geoZoneID=document.getElementById("zone").value;
	$('#loadingDiv').fadeIn();
	var JobOrderType    =   document.getElementById("JobOrderType").value;
	var searchTextId	=	document.getElementById("districtOrSchooHiddenlId").value;
	var schoolId		=	document.getElementById("schoolId").value;
//	var subjectId		=	document.getElementById("subjectId").value;
//	var certType			=	trim(document.getElementById("certType").value);
	//var certificationsId	=	trim(document.getElementById("certificateTypeMaster").value);
	var jobOrderId	=	document.getElementById("jobOrderId").value;
	var status	=	document.getElementById("status").value;
	var jobReqNoId=document.getElementById("reqno").value;
	
	var jobCategoryIds='';
    var 	jobCategoryId   = document.getElementById("jobCateSelect");
    
    for (var i = 0; i < jobCategoryId.options.length; i++) {
        if(jobCategoryId.options[i].selected ==true){
      	  if(jobCategoryIds==''){
      		  jobCategoryIds=  jobCategoryId.options[i].value;
      	  }
      	  else{
      		  jobCategoryIds=jobCategoryIds+ ","+jobCategoryId.options[i].value;
      	  }
        }
     }
	
	var resultFlag=true;		
	if(document.getElementById("districtORSchoolName").value!="" && searchTextId==0){
		resultFlag=false;
	}
	delay(1000);
	f_geoZoneID		=	geoZoneID;
	f_JobOrderType	=   JobOrderType;
	f_districtId	=	searchTextId
	f_schoolId		=	schoolId;
//	f_subjectId		=   subjectId
//	f_certificationsId=certificationsId;
	f_jobOrderId	= 	jobOrderId;
	f_status		=	status;
	f_jobReqNoId    =	jobReqNoId;
	f_resultFlag	=	resultFlag;
	f_jobCategoryIds=	jobCategoryIds;
	
	
	
	
	
	
	if(sUrl.indexOf("managejobordersnew") > -1 || sUrl.indexOf("schooljobordersnew") > -1){
		ManageJobOrdersAjax.displayRecordsByEntityType(resultFlag,JobOrderType,searchTextId,schoolId,/*subjectId,
				certificationsId,*/jobOrderId,status,noOfRows,page,sortOrderStr,sortOrderType,jobReqNoId,geoZoneID,jobCategoryIds,{ 
			async: true,
			callback: function(data)
			{	
				$('#divMain').html(data);
				tpJbIEnable();
				applyScrollOnTbl();	
				$('#loadingDiv').hide();
				$('#iconpophover1').tooltip();
			},
		errorHandler:handleError
		});
 }else{//alert("searchRecordsByEntityTypeES");
	 ManageJobOrdersAjax.displayRecordsByEntityTypeNewES(resultFlag,JobOrderType,searchTextId,schoolId,/*subjectId,
				certificationsId,*/jobOrderId,status,noOfRows,page,sortOrderStr,sortOrderType,jobReqNoId,geoZoneID,jobCategoryIds,{ 
			async: true,
			callback: function(data)
			{	
				$('#divMain').html(data);
				tpJbIEnable();
				applyScrollOnTbl();	
				$('#loadingDiv').hide();
				$('#iconpophover1').tooltip();
			},
		errorHandler:handleError
		});
 }
}

//shadab end for districtjoborder

function getSortFirstGrid()
{
	$('#gridNo').val("1");
}

function getSortSecondGrid()
{
	$('#gridNo').val("2");
}

function showSchoolList(schoolIds)
{
	schoolIdsStore=schoolIds;
	DWRAutoComplete.displaySchoolList(schoolIds,noOfRowsSchool,pageforSchool,sortOrderStrSchool,sortOrderTypeSchool,{ 
		async: false,		
		callback: function(data)
		{
			$('#myModalforSchhol').modal('show');
			document.getElementById("schoolListDiv").innerHTML=data;
			applyScrollOnTb();
		}
	});	
}



function showEditJobHistory(jobId){
	$("#jobOrderHistoryModalId").modal("show");
	//alert("111111");
	document.getElementById("divJobHisTxt").innerHTML= '<img src="images/loadingAnimation.gif" style="margin-left:45%;">';
	
	try{
		ManageJobOrdersAjax.getDataFromJobOrderHistory(jobId,{
		async:false,
		callback:function(data){
		if(data!=null){
				
			document.getElementById("divJobHisTxt").innerHTML=data;
		}

	},errorHandler:handleError
	});
	}catch(e){}
}


function hideById(itemId){
	$("#"+itemId).hide();
	
	$("#"+itemId).modal("hide");
	
}


function denyJob(jobid)
{	
	
			var div="<div class='modal hide in' id='confirmbox' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'>"+
			"<div class='modal-dialog' style='width:550px;'>"+
			"<div class='modal-content'>"+
			"<div class='modal-header'>"+
		    "<!--<button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>-->"+
		    "<h4 class='modal-title' id='myModalLabel' style='color:white;'></h4>"+
			"</div>"+
			"<div class='modal-body' id='confirmMessage'>"+	      
			"</div>"+
			"<div class='modal-footer'>"+
			"<button type='button' class='btn btn-primary' id='confirmTrue' ></button>"+
			"<button type='button' class='btn btn-default' id='confirmFalse'>"+resourceJSON.btnCancel+"</button>"+	        
			"</div>"+
			"</div>"+
			"</div>"+
			"</div>"; 
			
		$('#confirmMessage').after(div);
		//confirmMessage = confirmMessage || '';
		$('#confirmMessage').modal('hide');
		$('#confirmbox').modal({show:true,
		                    backdrop: "static",
		                    keyboard: false,
		});
		$('#confirmbox #confirmMessage').html("<div class='divErrorMsg' id='errorDiv' style=\"display: block;\"></div><div id='denyDiv' class='col-sm-12 col-md-12'>" +
				"<label>Comments<span class='required'>*</span></label>:" +
				"<textarea class=\"span12\" rows=\"2\" id=\"comments\" name=\"comments\" maxlength=\"500\"></textarea>" +
				"</div>");
		$('#confirmbox #myModalLabel').html("TeacherMatch");
		$('#confirmbox #confirmTrue').html("Send to Posting Creator");
		$('#confirmFalse').click(function(){
		$('#confirmbox').modal('hide');
		$('#confirmbox').remove();
		//$('#jWTeacherStatusNotesDiv').modal('show');
		});
		$('#confirmTrue').click(function(){
		if(denyJobConfirm(jobid)){
			/*$('#confirmbox').modal('hide');
			$('#confirmbox').remove();*/
			//$('#jWTeacherStatusNotesDiv').modal('show');
		}
		});
		$('#comments').jqte();
}
//shriram
function denyJobConfirm(jobId){
	
	$('#errorDiv').hide();
	//var jobId=document.getElementById("jobId").value;
	
	var comment=$("[name='comments']").parents().parents('.jqte').find(".jqte_editor").html();
	
	if(comment.trim()==''){
		$('#errorDiv').html('');
		$('#errorDiv').append("&nbsp;&nbsp;&nbsp;&nbsp;&#149; Please enter Comment<br>");
		$("[name='comments']").parents().parents('.jqte').find(".jqte_editor").focus();
		$('#errorDiv').show();
		
		return false;
	}
	
	ApproveJobAjax.denyJobApprovalJobOrderWithComment(jobId,comment,{ 
		async: false,
		callback: function(data)
		{
	
			var checkReturn=""+data;
			var message="<p>This job is denied successfully.</p>";
			if(checkReturn==0){
				message="<p>You have been already denied this job.</p>";
			}
			$('#confirmShowMessage .modal-body').html(message);
			$('#confirmShowMessage').modal('show');	
			$('#denyButton').hide();
			$('#confirmbox').modal('hide');
			$('#confirmbox').remove();
			return true;
			
		},
		errorHandler:handleError
	});
	return false;
}
//Pavan Gupta
function getapprovalgroupdetails(jobordertype,jobid,districtid){	
	ApproveJobAjax.getApprovalGroupDetails(jobordertype,jobid,districtid,{ 
		async: false,
		callback: function(data)
		{	
		$('#approvalgroupdetailsBody').html(data);		
		$('#myModalforapprovalgroupdetails').modal('show');				
		},
		errorHandler:handleError
	});	
	

	
	
}


