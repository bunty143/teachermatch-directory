package tm.services;


import static tm.services.district.GlobalServices.println;
import static tm.services.report.CandidateGridReportAjax.convertUSformatOnlyYear;

import java.awt.Color;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringReader;
import java.net.URL;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HttpsURLConnection;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import jxl.CellView;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import tm.bean.AdminActivityDetail;
import tm.bean.BatchJobOrder;
import tm.bean.DistrictAccountingCode;
import tm.bean.DistrictApprovalGroups;
import tm.bean.DistrictJobDescriptionLibrary;
import tm.bean.DistrictRequisitionNumbers;
import tm.bean.DistrictSpecificApprovalFlow;
import tm.bean.DistrictSpecificJobcode;
import tm.bean.DistrictWiseApprovalGroup;
import tm.bean.EmploymentServicesTechnician;
import tm.bean.InternalTransferCandidates;
import tm.bean.JobApprovalHistory;
import tm.bean.JobApprovalProcess;
import tm.bean.JobCertification;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.JobRequisitionNumbers;
import tm.bean.JobWiseApprovalProcess;
import tm.bean.Jobrequisitionnumberstemp;
import tm.bean.SchoolInJobOrder;
import tm.bean.TeacherAcademics;
import tm.bean.TeacherAchievementScore;
import tm.bean.TeacherAssessmentStatus;
import tm.bean.TeacherDetail;
import tm.bean.TeacherElectronicReferences;
import tm.bean.TeacherExperience;
import tm.bean.TeacherNormScore;
import tm.bean.TeacherPersonalInfo;
import tm.bean.TeacherPortfolioStatus;
import tm.bean.TeacherStatusHistoryForJob;
import tm.bean.TempJobRequisitionNumbers;
import tm.bean.assessment.AssessmentDetail;
import tm.bean.assessment.AssessmentJobRelation;
import tm.bean.assessment.AssessmentSections;
import tm.bean.cgreport.JobCategoryWiseStatusPrivilege;
import tm.bean.cgreport.JobWiseConsolidatedTeacherScore;
import tm.bean.cgreport.PercentileScoreByJob;
import tm.bean.cgreport.RawDataForDomain;
import tm.bean.districtassessment.DistrictAssessmentDetail;
import tm.bean.districtassessment.DistrictAssessmentJobRelation;
import tm.bean.i4.I4QuestionSets;
import tm.bean.master.CertificateTypeMaster;
import tm.bean.master.ContactTypeMaster;
import tm.bean.master.DistrictKeyContact;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictSchools;
import tm.bean.master.DomainMaster;
import tm.bean.master.GeoZoneMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.JobTitleWithDescription;
import tm.bean.master.JobWisePanelStatus;
import tm.bean.master.RaceMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.SecondaryStatus;
import tm.bean.master.StateMaster;
import tm.bean.master.StatusMaster;
import tm.bean.master.SubjectMaster;
import tm.bean.master.TimeZoneMaster;
import tm.bean.user.UserMaster;
import tm.dao.AdminActivityDetailDAO;
import tm.dao.DistrictAccountingCodeDAO;
import tm.dao.DistrictApprovalGroupsDAO;
import tm.dao.DistrictJobDescriptionLibraryDAO;
import tm.dao.DistrictRequisitionNumbersDAO;
import tm.dao.DistrictSpecificApprovalFlowDAO;
import tm.dao.DistrictSpecificJobcodeDAO;
import tm.dao.DistrictWiseApprovalGroupDAO;
import tm.dao.EmployeeMasterDAO;
import tm.dao.EmploymentServicesTechnicianDAO;
import tm.dao.InternalTransferCandidatesDAO;
import tm.dao.JobApprovalHistoryDAO;
import tm.dao.JobApprovalProcessDAO;
import tm.dao.JobCertificationDAO;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.JobRequisitionNumbersDAO;
import tm.dao.JobRequisitionNumbersTempDAO;
import tm.dao.JobWiseApprovalProcessDAO;
import tm.dao.SchoolInJobOrderDAO;
import tm.dao.TeacherAcademicsDAO;
import tm.dao.TeacherAchievementScoreDAO;
import tm.dao.TeacherAssessmentAttemptDAO;
import tm.dao.TeacherAssessmentStatusDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.TeacherElectronicReferencesDAO;
import tm.dao.TeacherExperienceDAO;
import tm.dao.TeacherNormScoreDAO;
import tm.dao.TeacherPersonalInfoDAO;
import tm.dao.TeacherPortfolioStatusDAO;
import tm.dao.TeacherStatusHistoryForJobDAO;
import tm.dao.TempJobRequisitionNumbersDAO;
import tm.dao.UserLoginHistoryDAO;
import tm.dao.assessment.AssessmentDetailDAO;
import tm.dao.assessment.AssessmentJobRelationDAO;
import tm.dao.assessment.AssessmentSectionDAO;
import tm.dao.cgreport.JobCategoryWiseStatusPrivilegeDAO;
import tm.dao.cgreport.JobWiseConsolidatedTeacherScoreDAO;
import tm.dao.cgreport.PercentileScoreByJobDAO;
import tm.dao.cgreport.RawDataForDomainDAO;
import tm.dao.districtassessment.DistrictAssessmentDetailDAO;
import tm.dao.districtassessment.DistrictAssessmentJobRelationDAO;
import tm.dao.i4.I4QuestionSetsDAO;
import tm.dao.master.ContactTypeMasterDAO;
import tm.dao.master.DistrictKeyContactDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.DistrictSchoolsDAO;
import tm.dao.master.DomainMasterDAO;
import tm.dao.master.GeoZoneMasterDAO;
import tm.dao.master.JobCategoryMasterDAO;
import tm.dao.master.JobMasterDAO;
import tm.dao.master.JobTitleWithDescriptionDAO;
import tm.dao.master.JobWisePanelStatusDAO;
import tm.dao.master.RaceMasterDAO;
import tm.dao.master.SchoolMasterDAO;
import tm.dao.master.SecondaryStatusDAO;
import tm.dao.master.StateMasterDAO;
import tm.dao.master.StatusMasterDAO;
import tm.dao.master.SubjectMasterDAO;
import tm.dao.master.TimeZoneMasterDAO;
import tm.dao.menu.RoleAccessPermissionDAO;
import tm.dao.user.RoleMasterDAO;
import tm.dao.user.UserMasterDAO;
import tm.logs.CreateLog;
import tm.services.es.ElasticSearchService;
import tm.services.report.CandidateGridAjax;
import tm.services.social.SocialService;
import tm.services.teacher.DashboardAjax;
import tm.servlet.WorkThreadServlet;
import tm.utility.ElasticSearchConfig;
import tm.utility.IPAddressUtility;
import tm.utility.TMCommonUtil;
import tm.utility.TestTool;
import tm.utility.Utility;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

import edu.emory.mathcs.backport.java.util.Arrays;


public class ManageJobOrdersAjax 
{	
	
	String locale = Utility.getValueOfPropByKey("locale");
	int totalShowCol=0;
	

	@Autowired
	private DashboardAjax dashboardAjax;
	
	@Autowired
	private GeoZoneMasterDAO geoZoneMasterDAO;
	
	@Autowired
	private DistrictJobDescriptionLibraryDAO districtJobDescriptionLibraryDAO;
	
	@Autowired
	private JobWiseConsolidatedTeacherScoreDAO jobWiseConsolidatedTeacherScoreDAO;
	
	public void setDistrictJobDescriptionLibraryDAO(
			DistrictJobDescriptionLibraryDAO districtJobDescriptionLibraryDAO) {
		this.districtJobDescriptionLibraryDAO = districtJobDescriptionLibraryDAO;
	}
	@Autowired
	private TeacherAcademicsDAO teacherAcademicsDAO;
	
	@Autowired
	private TeacherElectronicReferencesDAO teacherElectronicReferencesDAO;

	
	@Autowired
	private TeacherAchievementScoreDAO teacherAchievementScoreDAO;
	
	@Autowired
	private DomainMasterDAO domainMasterDAO;
	
	@Autowired
	private RawDataForDomainDAO rawDataForDomainDAO;
	
	@Autowired
	private PercentileScoreByJobDAO percentileScoreByJobDAO;
	
	@Autowired
	private JobMasterDAO jobMasterDAO;

	@Autowired
	private TeacherPersonalInfoDAO teacherPersonalInfoDAO;
	
	@Autowired
	private TeacherExperienceDAO teacherExperienceDAO;
	
	@Autowired
	private InternalTransferCandidatesDAO internalTransferCandidatesDAO;
	
	
	@Autowired
	private TeacherDetailDAO teacherDetailDAO;
	
	@Autowired
	private DistrictRequisitionNumbersDAO districtRequisitionNumbersDAO;
	
	@Autowired
	private JobRequisitionNumbersDAO jobRequisitionNumbersDAO;
	
	@Autowired
	private JobRequisitionNumbersTempDAO jobRequisitionNumbersTempDAO;
	
	@Autowired
	private TempJobRequisitionNumbersDAO tempJobRequisitionNumbersDAO;
	
	@Autowired
	private TeacherStatusHistoryForJobDAO teacherStatusHistoryForJobDAO;

	@Autowired
	private UserLoginHistoryDAO userLoginHistoryDAO;
	
	@Autowired
	private JobCategoryWiseStatusPrivilegeDAO jobCategoryWiseStatusPrivilegeDAO;

	@Autowired
	private RaceMasterDAO raceMasterDAO;
	
	@Autowired
	private ApproveJobAjax approveJobAjax;
	
	@Autowired
	private EmploymentServicesTechnicianDAO employmentServicesTechnicianDAO;
	
	@Autowired
	private CandidateGridAjax candidateGridAjax;
	
	@Autowired
	private AdminActivityDetailDAO adminActivityDetailDAO;
	
	
	@Autowired
	private TimeZoneMasterDAO timeZoneMasterDAO;
	/*@Autowired
 	private JobOrderLogDAO jobOrderLogDAO;
 	
 	
	public void setJobOrderLogDAO(JobOrderLogDAO jobOrderLogDAO) {
		this.jobOrderLogDAO = jobOrderLogDAO;
	}*/
	
	@Autowired
	private JobApprovalProcessDAO jobApprovalProcessDAO;
	
	public JobApprovalProcessDAO getJobApprovalProcessDAO() {
		return jobApprovalProcessDAO;
	}


	public void setJobApprovalProcessDAO(JobApprovalProcessDAO jobApprovalProcessDAO) {
		this.jobApprovalProcessDAO = jobApprovalProcessDAO;
	}


	@Autowired
	private JobTitleWithDescriptionDAO jobTitleWithDescriptionDAO;
	
	public void setCandidateGridAjax(CandidateGridAjax candidateGridAjax) {
		this.candidateGridAjax = candidateGridAjax;
	}


	@Autowired
	private DistrictApprovalGroupsDAO districtApprovalGroupsDAO;
	public void setDistrictApprovalGroupsDAO(DistrictApprovalGroupsDAO districtApprovalGroupsDAO) {
		this.districtApprovalGroupsDAO = districtApprovalGroupsDAO;
	}

	
	@Autowired
	private AssessmentDetailDAO assessmentDetailDAO;
	public void setAssessmentDetailDAO(AssessmentDetailDAO assessmentDetailDAO) {
		this.assessmentDetailDAO = assessmentDetailDAO;
	}

	@Autowired
	private SchoolMasterDAO schoolMasterDAO;
	public void setSchoolMasterDAO(SchoolMasterDAO schoolMasterDAO) {
		this.schoolMasterDAO = schoolMasterDAO;
	}

	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	public void setDistrictMasterDAO(DistrictMasterDAO districtMasterDAO) {
		this.districtMasterDAO = districtMasterDAO;
	}

	@Autowired
	private JobOrderDAO jobOrderDAO;
	public void setJobOrderDAO(JobOrderDAO jobOrderDAO) {
		this.jobOrderDAO = jobOrderDAO;
	}

	@Autowired
	private SchoolInJobOrderDAO schoolInJobOrderDAO;
	public void setSchoolInJobOrderDAO(SchoolInJobOrderDAO schoolInJobOrderDAO) {
		this.schoolInJobOrderDAO = schoolInJobOrderDAO;
	}

	@Autowired
	private TeacherAssessmentAttemptDAO teacherAssessmentAttemptDAO;
	public void setTeacherAssessmentAttemptDAO(
			TeacherAssessmentAttemptDAO teacherAssessmentAttemptDAO) {
		this.teacherAssessmentAttemptDAO = teacherAssessmentAttemptDAO;
	}
	@Autowired
	private JobCertificationDAO jobCertificationDAO;
	public void setJobCertificationDAO(JobCertificationDAO jobCertificationDAO) {
		this.jobCertificationDAO = jobCertificationDAO;
	}
	@Autowired
	private AssessmentJobRelationDAO assessmentJobRelationDAO;
	public void setAssessmentJobRelationDAO(AssessmentJobRelationDAO assessmentJobRelationDAO) {
		this.assessmentJobRelationDAO = assessmentJobRelationDAO;
	}
	@Autowired
	private EmailerService emailerService;
	public void setEmailerService(EmailerService emailerService) 
	{
		this.emailerService = emailerService;
	}
	@Autowired
	private UserMasterDAO userMasterDAO;
	public void setUserMasterDAO(UserMasterDAO userMasterDAO) {
		this.userMasterDAO = userMasterDAO;
	}
	@Autowired
	private TeacherAssessmentStatusDAO teacherAssessmentStatusDAO;
	public void setTeacherAssessmentStatusDAO(TeacherAssessmentStatusDAO teacherAssessmentStatusDAO) {
		this.teacherAssessmentStatusDAO = teacherAssessmentStatusDAO;
	}
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	public void setJobForTeacherDAO(JobForTeacherDAO jobForTeacherDAO) 
	{
		this.jobForTeacherDAO = jobForTeacherDAO;
	}
	@Autowired
	private DistrictSchoolsDAO districtSchoolsDAO;
	public void setDistrictSchoolsDAO(DistrictSchoolsDAO districtSchoolsDAO) {
		this.districtSchoolsDAO = districtSchoolsDAO;
	}
	@Autowired
	private RoleAccessPermissionDAO roleAccessPermissionDAO;
	public void setRoleAccessPermissionDAO(RoleAccessPermissionDAO roleAccessPermissionDAO) {
		this.roleAccessPermissionDAO = roleAccessPermissionDAO;
	}

	@Autowired
	private StatusMasterDAO statusMasterDAO;

	public void setStatusMasterDAO(StatusMasterDAO statusMasterDAO) 
	{
		this.statusMasterDAO = statusMasterDAO;
	}
	@Autowired
	private JobCategoryMasterDAO jobCategoryMasterDAO;
	public void setJobCategoryMasterDAO(JobCategoryMasterDAO jobCategoryMasterDAO) {
		this.jobCategoryMasterDAO = jobCategoryMasterDAO;
	}

	@Autowired
	private SocialService socialService;
	public void setSocialService(SocialService socialService) {
		this.socialService= socialService;
	}
	@Autowired
	private TeacherPortfolioStatusDAO teacherPortfolioStatusDAO;
	public void setTeacherPortfolioStatusDAO(TeacherPortfolioStatusDAO teacherPortfolioStatusDAO) {
		this.teacherPortfolioStatusDAO = teacherPortfolioStatusDAO;
	}

	@Autowired
	private SubjectMasterDAO subjectMasterDAO;
	public void setSubjectMasterDAO(SubjectMasterDAO subjectMasterDAO) {
		this.subjectMasterDAO = subjectMasterDAO;
	}
	@Autowired
	private StateMasterDAO stateMasterDAO;
	public void setStateMasterDAO(StateMasterDAO stateMasterDAO) {
		this.stateMasterDAO = stateMasterDAO;
	}
	@Autowired
	private SecondaryStatusDAO secondaryStatusDAO;
	public void setSecondaryStatusDAO(SecondaryStatusDAO secondaryStatusDAO) {
		this.secondaryStatusDAO = secondaryStatusDAO;
	}

	@Autowired
	private TeacherNormScoreDAO teacherNormScoreDAO;
	
	@Autowired
	private AssessmentSectionDAO assessmentSectionDAO;
	
	@Autowired
	private EmployeeMasterDAO employeeMasterDAO;
	
	@Autowired
	private JobWisePanelStatusDAO jobWisePanelStatusDAO;
	public void setJobWisePanelStatusDAO(
			JobWisePanelStatusDAO jobWisePanelStatusDAO) {
		this.jobWisePanelStatusDAO = jobWisePanelStatusDAO;
	}
	@Autowired
	private DistrictKeyContactDAO districtKeyContactDAO;

	@Autowired
	private JobApprovalHistoryDAO jobApprovalHistoryDAO;
	
	@Autowired
	private I4QuestionSetsDAO i4QuestionSetsDAO;
	
	@Autowired
	private DistrictAssessmentDetailDAO districtAssessmentDetailDAO;
	
	
	@Autowired
	private DistrictAssessmentJobRelationDAO districtAssessmentJobRelationDAO;

	@Autowired
	private DistrictSpecificApprovalFlowDAO districtSpecificApprovalFlowDAO;
	
	
	@Autowired
	private DistrictWiseApprovalGroupDAO districtWiseApprovalGroupDAO;
	
	@Autowired
	private RoleMasterDAO roleMasterDAO;
	
	@Autowired
	private DistrictSpecificJobcodeDAO districtSpecificJobcodeDAO;
	
	public void setDistrictSpecificJobcodeDAO(
			DistrictSpecificJobcodeDAO districtSpecificJobcodeDAO) {
		this.districtSpecificJobcodeDAO = districtSpecificJobcodeDAO;
	}


	@Autowired
	private CreateLog createLog;
	
	@Autowired
	private DistrictAccountingCodeDAO districtAccountingCodeDAO;
	
	public void setDistrictAccountingCodeDAO(
		DistrictAccountingCodeDAO districtAccountingCodeDAO) {
		this.districtAccountingCodeDAO = districtAccountingCodeDAO;
	}
	@Autowired
	private JobWiseApprovalProcessDAO jobWiseApprovalProcessDAO;
	
	public void setJobWiseApprovalProcessDAO(
			JobWiseApprovalProcessDAO jobWiseApprovalProcessDAO) {
		this.jobWiseApprovalProcessDAO = jobWiseApprovalProcessDAO;
	}
	@Autowired
	private ContactTypeMasterDAO contactTypeMasterDAO;
	
	public void setContactTypeMasterDAO(ContactTypeMasterDAO contactTypeMasterDAO) {
		this.contactTypeMasterDAO = contactTypeMasterDAO;
	}


	String lblNoRecord=Utility.getLocaleValuePropByKey("lblNoRecord", locale);
	
	
	public String  displaySchool(int jobOrderId,int JobOrderType){
		/* ========  For Session time Out Error =========*/
		//System.out.println("calling displaySchool");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		String locale = Utility.getValueOfPropByKey("locale");
		StringBuffer tmRecords =	new StringBuffer();
		try{

			UserMaster userMaster = null;
			SchoolMaster schoolMaster=null;
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) {
				return "false";
			}else{
				userMaster=(UserMaster)session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			try{
				if(JobOrderType==2){
					roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,27,"addeditjoborder.do",1);
				}else{
					roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,28,"addeditjoborder.do",2);
				}
			}catch(Exception e){
				e.printStackTrace();
			}

			JobOrder jobOrder =jobOrderDAO.findById(jobOrderId,false,false);
			List<SchoolInJobOrder> listSchoolInJobOrder	= schoolInJobOrderDAO.findJobOrder(jobOrder);

			Criterion criterion1 = Restrictions.eq("jobOrder", jobOrder);
			tmRecords.append("<input type='hidden' id='avlSchoolInJobOrder' value='"+listSchoolInJobOrder.size()+"'>");
			if(listSchoolInJobOrder.size()!=0){
				//if(jobOrder.getDistrictMaster().getIsReqNoRequired())
				if(jobOrder.getDistrictMaster().getDistrictId().equals(804800))
					tmRecords.append("<table id=\"schoolTable\" width=\"100%\" border=\"0\" class=\"table table-bordered span11 mt10\"><thead class=\"bg\"><tr><th>"+Utility.getLocaleValuePropByKey("lblSchoolName", locale)+"</th><th>"+Utility.getLocaleValuePropByKey("lblAddedOn", locale)+"</th><th>"+Utility.getLocaleValuePropByKey("lblOfExpHi", locale)+"</th><th>"+Utility.getLocaleValuePropByKey("lblRequisitionNumber", locale)+"</th></tr></thead><tbody>");
				else
					tmRecords.append("<table id=\"schoolTable\" width=\"100%\" border=\"0\" class=\"table table-bordered span11 mt10\"><thead class=\"bg\"><tr><th>"+Utility.getLocaleValuePropByKey("lblSchoolName", locale)+"</th><th>"+Utility.getLocaleValuePropByKey("lblAddedOn", locale)+"</th><th>"+Utility.getLocaleValuePropByKey("lblOfExpHi", locale)+"</th><th>"+Utility.getLocaleValuePropByKey("lblRequisitionNumber", locale)+"<th>"+Utility.getLocaleValuePropByKey("lblActions", locale)+"</th></tr></thead><tbody>");				//else
				//else
					//tmRecords.append("<table id=\"schoolTable\" width=\"100%\" border=\"0\" class=\"table table-bordered span11 mt10\"><thead class=\"bg\"><tr><th>School Name</th><th>Added on</th><th> # of Expected Hire(s)</th><th>Actions</th></tr></thead><tbody>");
			}
			int noOfRecordCheck = 0;
			
			
			/*================= Checking If Record Not Found ======================*/
			List<SchoolMaster> schoolMasters= new ArrayList<SchoolMaster>();
			for (SchoolInJobOrder schoolJobOrder : listSchoolInJobOrder){
				schoolMasters.add(schoolJobOrder.getSchoolId());
			}
			Map<Long,List<JobRequisitionNumbers>> jobReqMap= new HashMap<Long, List<JobRequisitionNumbers>>();
			List<JobRequisitionNumbers> jobRequisitionNumbers = jobRequisitionNumbersDAO.findJobRequisitionNumbersBySchoolInJobOrder(jobOrder,schoolMasters);
			//System.out.println("jobRequisitionNumbers::::::::::::::>>>"+jobRequisitionNumbers.size());
			try{
				for (JobRequisitionNumbers jobReqObj : jobRequisitionNumbers) {
					Long schoolId=jobReqObj.getSchoolMaster().getSchoolId();
					List<JobRequisitionNumbers> tAlist = jobReqMap.get(schoolId);
					if(tAlist==null){
						List<JobRequisitionNumbers> jobs = new ArrayList<JobRequisitionNumbers>();
						jobs.add(jobReqObj);
						jobReqMap.put(schoolId, jobs);
					}else{
						tAlist.add(jobReqObj);
						jobReqMap.put(schoolId, tAlist);
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			//System.out.println("jobReqMap::::::::::::::>>>"+jobReqMap.size());
			List<TeacherAssessmentStatus> teacherAssessmentStatusList=teacherAssessmentStatusDAO.findHireStatusByJobAndSchools(jobOrder,schoolMasters);
			Map<Long,List<TeacherAssessmentStatus>> assMap= new HashMap<Long, List<TeacherAssessmentStatus>>();
			try{
				for (TeacherAssessmentStatus assObj : teacherAssessmentStatusList) {
					Long schoolId=assObj.getUpdatedBy().getSchoolId().getSchoolId();
					List<TeacherAssessmentStatus> tAlist = assMap.get(schoolId);
					if(tAlist==null){
						List<TeacherAssessmentStatus> assList = new ArrayList<TeacherAssessmentStatus>();
						assList.add(assObj);
						assMap.put(schoolId, assList);
					}else{
						tAlist.add(assObj);
						assMap.put(schoolId, tAlist);
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			String location = "";
			for (SchoolInJobOrder schoolJobOrder : listSchoolInJobOrder){
					if(schoolJobOrder.getSchoolId().getLocationCode()!=null && schoolJobOrder.getSchoolId().getLocationCode()!="")
						location = " ("+schoolJobOrder.getSchoolId().getLocationCode()+")";
		
				noOfRecordCheck++;
				int countOfReqNo =0;
				try{
					if(jobReqMap.get(schoolJobOrder.getSchoolId().getSchoolId())!=null){
						countOfReqNo = jobReqMap.get(schoolJobOrder.getSchoolId().getSchoolId()).size();
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				tmRecords.append("<tr id='"+schoolJobOrder.getSchoolId().getSchoolId()+"'>" );
				tmRecords.append("<td nowrap >"+schoolJobOrder.getSchoolId().getSchoolName()+location+"</td>");
				tmRecords.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(schoolJobOrder.getCreatedDateTime())+"</td>");
				tmRecords.append("<td>"+schoolJobOrder.getNoOfSchoolExpHires()+"</td>");
				//if(jobOrder.getDistrictMaster().getIsReqNoRequired())
				tmRecords.append("<td>"+countOfReqNo+"</td>");
//indra jeet	
				if(jobOrder.getDistrictMaster().getDistrictId()!=804800 && jobOrder.getDistrictMaster().getDistrictId()!=806900&&userMaster.getEntityType()==3){
					if(userMaster.getSchoolId().getSchoolId().equals(schoolJobOrder.getSchoolId().getSchoolId()))
					tmRecords.append("<td nowrap><a href='javascript:void(0);' onclick='return editSchoolInJobOrderTemp("+schoolJobOrder.getSchoolId().getSchoolId()+")'><span class='fa-pencil-square-o  icon-large  iconcolor'></span></a>&nbsp;&nbsp;&nbsp;&nbsp;");
					else
					tmRecords.append("<td nowrap><a href='javascript:void(0);' style='pointer-events: none;' onclick='return editSchoolInJobOrderTemp("+schoolJobOrder.getSchoolId().getSchoolId()+")'><span class='fa-pencil-square-o  icon-large  iconcolor'></span></a>&nbsp;&nbsp;&nbsp;&nbsp;");
				}
				
				int val=0;
				try{
					if(assMap.get(schoolJobOrder.getSchoolId().getSchoolId())!=null){
						val=assMap.get(schoolJobOrder.getSchoolId().getSchoolId()).size();
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				if(!jobOrder.getDistrictMaster().getDistrictId().equals(804800))
				if(val==0){
					if(userMaster.getEntityType()==3 && JobOrderType==2){
					//	tmRecords.append("<td nowrap>&nbsp;</td>");
					    if(jobOrder.getDistrictMaster().getDistrictId().equals(806900))
						tmRecords.append("<td nowrap><a href='javascript:void(0);' onclick='return editSchoolInJobOrder("+schoolJobOrder.getJobId().getJobId()+","+schoolJobOrder.getSchoolId().getSchoolId()+")'><span class='fa-pencil-square-o  icon-large  iconcolor'></span></a>&nbsp;&nbsp;&nbsp;&nbsp;");
					}else{
						if(roleAccess.indexOf("|3|")!=-1)
						{
							String sReqNumbers="";
							for(JobRequisitionNumbers pojo :jobRequisitionNumbers)
							{
								sReqNumbers=sReqNumbers+pojo.getDistrictRequisitionNumbers().getDistrictRequisitionId()+"#";
							}
							if(sReqNumbers.length() >1)
							{
								sReqNumbers=sReqNumbers.substring(0,sReqNumbers.length()-1);
							}
							//System.out.println("Edit sReqNumbers "+sReqNumbers);
						
							//Start sandeep 31-08-15
							
							
							 if(jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getHeadQuarterMaster() != null && jobOrder.getDistrictMaster().getHeadQuarterMaster().getHeadQuarterId() ==DashboardAjax.NC_HEADQUARTER){
									
								  Criterion criterionRequestion  = Restrictions.eq("jobOrder", jobOrder);
								  List<JobRequisitionNumbers> jobRequNumber = jobRequisitionNumbersDAO.findByCriteria(criterionRequestion);    
								  if(jobRequNumber != null && jobRequNumber.size() >0)
									  if(jobRequNumber.get(0).getDistrictRequisitionNumbers() != null)
										  tmRecords.append("<td></td>");
									  
							 }else{
								tmRecords.append("<td nowrap><a href='javascript:void(0);' onclick='return editSchoolInJobOrder("+schoolJobOrder.getJobId().getJobId()+","+schoolJobOrder.getSchoolId().getSchoolId()+")'><span class='fa-pencil-square-o  icon-large  iconcolor'></span></a>&nbsp;&nbsp;&nbsp;&nbsp;");
								tmRecords.append("<a href='javascript:void(0);' onclick='return deleteSchoolInJobOrder("+schoolJobOrder.getJobId().getJobId()+","+schoolJobOrder.getSchoolId().getSchoolId()+")'><span class='fa-trash-o  icon-large  iconcolor'></span></a></td>");
							 }	
							tmRecords.append("<td style='display:none;'>"+sReqNumbers+"</td>");
							tmRecords.append("<td style='display:none;'>"+schoolJobOrder.getSchoolId().getSchoolId()+"</td>");
							
						}
						else
						{
							tmRecords.append("<td nowrap>&nbsp;</td>");
						}
					}
				}else{
					tmRecords.append("<td nowrap>&nbsp;</td>");
				}
			}
			if(listSchoolInJobOrder.size()!=0)
				tmRecords.append("</table>");
		}catch (Exception e) 
		{
			e.printStackTrace();
		}

		return tmRecords.toString();
	}
	
	
	public String isReqNoEqualToNoOfExpHired(int jobId){
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		StringBuffer tmRecords =	new StringBuffer();
		String arraySchoolGrid = "";
		try{

		//	JobOrder jobOrder =jobOrderDAO.findById(jobId,false,false);
			Criterion jobidCriterion = Restrictions.eq("jobId.jobId",jobId);
			Order orderJobid =  Order.asc("jobId.jobId");
			
			//Anurag   List<SchoolInJobOrder> listSchoolInJobOrder	= schoolInJobOrderDAO.findJobOrder(jobOrder);
			List<SchoolInJobOrder> listSchoolInJobOrder	= schoolInJobOrderDAO.findWithAll(orderJobid, jobidCriterion);
			Criterion criterion1 = Restrictions.eq("jobOrder.jobId", jobId);
			int countOfReqNo =0;

			/*================= Checking If Record Not Found ======================*/
			for (SchoolInJobOrder schoolJobOrder : listSchoolInJobOrder) 
			{
				Criterion criterion2 = Restrictions.eq("schoolMaster", schoolJobOrder.getSchoolId());
				//  countOfReqNo = jobRequisitionNumbersDAO.findByCriteria(criterion1,criterion2).size();
				countOfReqNo = jobRequisitionNumbersDAO.countRowByCriterion(criterion1,criterion2);   // Optimized by Anurag
				System.out.println("countOfReqNo ::::::"+countOfReqNo);
				if(schoolJobOrder.getNoOfSchoolExpHires()!=countOfReqNo)
				{
					arraySchoolGrid = schoolJobOrder.getSchoolId().getSchoolId()+","+schoolJobOrder.getSchoolId().getSchoolName()+","+schoolJobOrder.getNoOfSchoolExpHires()+","+countOfReqNo;
					//System.out.println(" arraySchoolGrid : "+arraySchoolGrid);
 					return arraySchoolGrid;
				}
			}
		}catch (Exception e) 
		{
			e.printStackTrace();
		}

		return "";
	}

	public String isTempReqNoEqualToNoOfExpHired(String jobAuthKey){
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		StringBuffer tmRecords =	new StringBuffer();
		String arraySchoolGrid = "";
		try{

			//System.out.println(" ==================== isTempReqNoEqualToNoOfExpHired Method ============");
			
			Criterion criterion1 = Restrictions.eq("jobAuthKey", jobAuthKey);
			//JobOrder jobOrder =jobOrderDAO.findById(jobId,false,false);
			List<TempJobRequisitionNumbers> listTempJobRequisitionNumbers	= tempJobRequisitionNumbersDAO.findByCriteria(criterion1);

			//Criterion criterion1 = Restrictions.eq("jobOrder", jobOrder);
			int countOfReqNo =0;

			/*================= Checking If Record Not Found ======================*/
			for (TempJobRequisitionNumbers tjbrn : listTempJobRequisitionNumbers) 
			{
				Criterion criterion2 = Restrictions.eq("schoolMaster", tjbrn.getSchoolMaster());
				countOfReqNo = tempJobRequisitionNumbersDAO.findByCriteria(criterion2).size();
				
				if(tjbrn.getNoOfSchoolExpHires()!=countOfReqNo)
				{
					arraySchoolGrid = tjbrn.getSchoolMaster().getSchoolId()+","+tjbrn.getSchoolMaster().getSchoolName()+","+tjbrn.getNoOfSchoolExpHires()+","+countOfReqNo;
					//System.out.println(" arraySchoolGrid : "+arraySchoolGrid);
					return arraySchoolGrid;
				}
			}
		}catch (Exception e) 
		{
			e.printStackTrace();
		}

		return "";
	}
	
	public String  displayCertification(int jobOrderId,int JobOrderType){
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		StringBuffer tmRecords =	new StringBuffer();
		try{
			UserMaster userMaster = null;
			SchoolMaster schoolMaster=null;
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) {
				return "false";
			}else{
				userMaster=(UserMaster)session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			try{
				if(JobOrderType==2){
					roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,27,"addeditjoborder.do",1);
				}else{
					roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,28,"addeditjoborder.do",2);
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			JobOrder jobOrder =jobOrderDAO.findById(jobOrderId,false,false);
			List<JobCertification> lstJobCertification= null;
			Criterion criterion1 = Restrictions.eq("jobId",jobOrder);
			lstJobCertification = jobCertificationDAO.findByCriteria(criterion1);		
			int i=1;
			for (JobCertification jobCertification : lstJobCertification) 
			{
				if(userMaster.getEntityType()==3 && JobOrderType==2){
					tmRecords.append("<div style='margin-top:-20px;' class=\"\" id=\"CertificationGrid"+i+"\" nowrap><input type=\"hidden\" name=\"certificateTypeIds\" id=\"certificateTypeId"+i+"\" value=\""+jobCertification.getJobCertificationId()+"\" >" +
							"<label certtypeidcl=\""+jobCertification.getCertificateTypeMaster().getCertTypeId()+"\" name=\"certificateTypeText\">"+jobCertification.getCertificateTypeMaster().getCertType()+"</label></div>" );
				}else{
					tmRecords.append("<div style='margin-top:-20px;' class=\"\" id=\"CertificationGrid"+i+"\" nowrap>" +
							"<label certtypeidcl=\""+jobCertification.getCertificateTypeMaster().getCertTypeId()+"\" name=\"certificateTypeText\">"+jobCertification.getCertificateTypeMaster().getCertType()+" ");
					if(roleAccess.indexOf("|3|")!=-1){
						tmRecords.append("<a href=\"javascript:void(0);\" onclick=\"deleteCertification("+jobCertification.getJobCertificationId()+");\"><img width=\"15\" height=\"15\"  class=\"can\" src=\"images/can-icon.png\"></a>" );
					}
					tmRecords.append("</label></div></br>");
				}
				i++;
			}
		}catch (Exception e) 
		{
			e.printStackTrace();
		}

		return tmRecords.toString();
	}
	//@ mukesh 
	
	public String displayRecordsByEntityType(boolean resultFlag,int JobOrderType,int districtOrSchoolId,int 
			schoolId,/*String subjectId,String certifications,*/String jobOrderIds,String status,String noOfRow,
			String pageNo,String sortOrder,String sortOrderType,String  disJobReqNo,String geoZonId,String jobCategoryIds,String jobApplicationStatus,String jobSubCategoryIds)  //Adding jobApplicationStatus parameter by Deepak
	{
		/* ========  For Session time Out Error =========*/
		//System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>11>>>>>>>>>>>>>>>>>>>>>>>>>jobApplicationStatus :"+jobApplicationStatus);
		int	geoZoneId=0;
		if(geoZonId!=null && geoZonId!="" &&!geoZonId.equals(""))
		  geoZoneId =  Integer.parseInt(geoZonId);
		
		int	jobApplicationStatusId=0;
		if(jobApplicationStatus!=null && jobApplicationStatus!="" &&!jobApplicationStatus.equals(""))
			jobApplicationStatusId =  Integer.parseInt(jobApplicationStatus);
		
		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		StringBuffer tmRecords =	new StringBuffer();
		try{
			UserMaster userMaster = null;
			Integer entityID=null;
			DistrictMaster districtMaster=null;
			SchoolMaster schoolMaster=null;
			SubjectMaster subjectMaster	=	null;
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) {
				return "false";
			}else{
				userMaster=(UserMaster)session.getAttribute("userMaster");

				if(userMaster.getSchoolId()!=null)
					schoolMaster =userMaster.getSchoolId();
				

				if(userMaster.getDistrictId()!=null){
					districtMaster=userMaster.getDistrictId();
				}

				entityID=userMaster.getEntityType();
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			try{
				if(JobOrderType==3){
					roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,25,"schooljoborders.do",0);
				}else{
					roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,24,"managejoborders.do",0);
				}
			}catch(Exception e){
				e.printStackTrace();
			}

			//-- get no of record in grid,
			//-- set start and end position

			int noOfRowInPage = Integer.parseInt(noOfRow);
			int pgNo = Integer.parseInt(pageNo);
			int start =((pgNo-1)*noOfRowInPage);
			int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord = 0;
			//------------------------------------

			/** set default sorting fieldName **/
			String sortOrderFieldName="createdDateTime";
			String sortOrderNoField="jobId";

			boolean deafultFlag=false;
			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal=null;
			if(sortOrder!=null){
				if(!sortOrder.equals("") && !sortOrder.equals(null) && !sortOrder.equals("ofApplicant") && !sortOrder.equals("ofHire")){
					sortOrderFieldName=sortOrder;
					sortOrderNoField=sortOrder;
					deafultFlag=false;
				}else{
					deafultFlag=true;
				}
				if(sortOrder.equals("ofApplicant")){
					sortOrderNoField="ofApplicant";
				}
				if(sortOrder.equals("ofHire")){
					sortOrderNoField="ofHire";
				}
			}
			String sortOrderTypeVal="0";
			sortOrderStrVal=Order.asc(sortOrderFieldName);
			if(sortOrderType!=null){
				if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
					if(sortOrderType.equals("1")){
						sortOrderTypeVal="1";
						sortOrderStrVal=Order.desc(sortOrderFieldName);
					}
				}
			}
			if(sortOrderType.equals("")){
				sortOrderStrVal=Order.desc(sortOrderFieldName);
			}

			/**End ------------------------------------**/
			List<JobOrder> lstJobOrder	  =	 new ArrayList<JobOrder>();
			boolean jobOrderIdFlag=false;
			boolean statusFlag=false;

			Criterion criterionStatus=null;
			Criterion criterionJobFilter=null;
			if(!(jobOrderIds.equals("0") || jobOrderIds.equalsIgnoreCase("0"))){
				String [] jobsArr =  jobOrderIds.split(",");
				List<Integer> jobs = new ArrayList<Integer>();
				for(int i=0;i<jobsArr.length;i++)
				{	
					String jobId = jobsArr[i].trim();
					try{if(!jobId.equals(""))
						jobs.add(Integer.parseInt(jobId));
					}catch(Exception e){}
				}
				if(jobs.size()>0)
				{
					jobOrderIdFlag=true;
					criterionJobFilter	= Restrictions.in("jobId",jobs);
				}
			}
			/*if(jobOrderId!=0){
				jobOrderIdFlag=true;
				criterionJobFilter	= Restrictions.eq("jobId",jobOrderId);
			}*/
			if(status!=null){
				if(!status.equalsIgnoreCase("") && !status.equalsIgnoreCase(null)){
					statusFlag=true;
					criterionStatus=Restrictions.eq("status",status);
				}
			}else{
				statusFlag=false;
			}

			/* ============== Gagan : For Adding Subject Filter ===============*/

			/*Criterion criterionSubject=null;
			if(!subjectId.equalsIgnoreCase(""))
			{
				subjectMaster=	subjectMasterDAO.findById(Integer.parseInt(subjectId), false, false);
				criterionSubject=Restrictions.eq("subjectMaster",subjectMaster);
			}*/
			//String windowFunc="if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
			int noOfRecordCheck = 0;
			int totalRecords=0;
			/*************Start *************/
			List<JobOrder> filterJobOrderList = new ArrayList<JobOrder>();

			boolean jobFlag=false;
			
			/*List<JobOrder> certJobOrderList	  =	 new ArrayList<JobOrder>();
			boolean certTypeFlag=false;
			if(certifications!=null && !certifications.equals("") && !certifications.equals("0")){
				certTypeFlag=true;
				certJobOrderList=jobCertificationDAO.findCertificationByJob(certifications);
			}
			if(certTypeFlag && certJobOrderList.size()>0){
				if(jobFlag){
					filterJobOrderList.retainAll(certJobOrderList);
				}else{
					filterJobOrderList.addAll(certJobOrderList);
				}
			}
			if(certTypeFlag){
				jobFlag=true;
			}*/
			
			/*List<JobOrder> subjectJList	  =	 new ArrayList<JobOrder>();
			boolean subjectFlag=false;
			if(subjectId!=null && !subjectId.equals("") && !subjectId.equals("0")){
				SubjectMaster sMaster=null;
				if(subjectId!=null){
					sMaster=subjectMasterDAO.findById(Integer.parseInt(subjectId),false,false);
				}
				subjectFlag=true;
				subjectJList=jobOrderDAO.findJobBySubject(sMaster);
			}
			if(subjectFlag && subjectJList.size()>0){
				if(jobFlag){
					filterJobOrderList.retainAll(subjectJList);
				}else{
					filterJobOrderList.addAll(subjectJList);
				}
			}
			if(subjectFlag){
				jobFlag=true;
			}*/
			
			
			DistrictMaster  distMaster=null;
			List<JobOrder> districtJList	  =	 new ArrayList<JobOrder>();
			boolean districtFlag=false;
			if(districtOrSchoolId!=0){
				if(districtOrSchoolId!=0){
					distMaster=districtMasterDAO.findById(districtOrSchoolId,false,false);
				}
				districtFlag=true;
				districtJList=jobOrderDAO.findJobByDistrict(distMaster);
			}
			if(districtFlag && districtJList.size()>0){
				if(jobFlag){
					filterJobOrderList.retainAll(districtJList);
				}else{
					filterJobOrderList.addAll(districtJList);
				}
			}
			if(districtFlag){
				jobFlag=true;
			}

			
			
			// Job category
			/*List<JobOrder> listjJobOrders = new ArrayList<JobOrder>();
			boolean jobCateFlag=false;
			if(jobCategoryIds!=null && !jobCategoryIds.equals("") && !jobCategoryIds.equals("0"))
			{
				List<Integer> jobcatIds = new ArrayList<Integer>();
				
				String jobCategoryIdStr[] =jobCategoryIds.split(",");
			  if(!ArrayUtils.contains(jobCategoryIdStr, "0"))
			  {		  
				for(String str : jobCategoryIdStr){
				 if(!str.equals(""))
					 jobcatIds.add(Integer.parseInt(str));
				 } 
				 if(jobcatIds.size()>0){
					 listjJobOrders = jobOrderDAO.findByJobCategery(jobcatIds,districtOrSchoolId);
					 jobCateFlag=true;
				 }
				 if(listjJobOrders!=null && listjJobOrders.size()>0){
						if(jobCateFlag){
							filterJobOrderList.retainAll(listjJobOrders);
						}else{
							filterJobOrderList.addAll(listjJobOrders);
						}
					 }
				 }
			  }
			
			if(jobCateFlag){
				jobFlag=true;
			}*/
			
			
			
			if(jobSubCategoryIds!=null && !jobSubCategoryIds.equals("") && !jobSubCategoryIds.equals("0"))
			  {
				//Job Sub Category
					List<JobOrder> listJobOrdersBySubCatgry = new ArrayList<JobOrder>();
					boolean jobSubCateFlag=false;
					
					if(jobSubCategoryIds!=null && !jobSubCategoryIds.equals("") && !jobSubCategoryIds.equals("0"))
					{
						List<Integer> jobSubcatIds = new ArrayList<Integer>();
						
						String jobSubCategoryIdStr[] = jobSubCategoryIds.split(",");
					  if(!ArrayUtils.contains(jobSubCategoryIdStr, "0"))
					  {		  
						for(String str : jobSubCategoryIdStr){
						 if(!str.equals(""))
							 jobSubcatIds.add(Integer.parseInt(str));
						 } 
						 if(jobSubcatIds.size()>0){
							 listJobOrdersBySubCatgry = jobOrderDAO.findByJobCategery(jobSubcatIds,districtOrSchoolId);
							 System.out.println("  job list by jobcategory :: "+listJobOrdersBySubCatgry.size());
							 jobSubCateFlag=true;
						 }
						 if(jobSubCateFlag){
							filterJobOrderList.retainAll(listJobOrdersBySubCatgry);
						 }else{
							filterJobOrderList.addAll(listJobOrdersBySubCatgry);
						 }
						}
					  }

					if(jobSubCateFlag){
						jobFlag=true;
					}
			  }
			  else
			  {
				// Job category
					List<JobOrder> listjJobOrders = new ArrayList<JobOrder>();
					boolean jobCateFlag=false;
					if(jobCategoryIds!=null && !jobCategoryIds.equals("") && !jobCategoryIds.equals("0"))
					{
						List<Integer> jobcatIds = new ArrayList<Integer>();
						
						String jobCategoryIdStr[] =jobCategoryIds.split(",");
					  if(!ArrayUtils.contains(jobCategoryIdStr, "0"))
					  {		  
						for(String str : jobCategoryIdStr){
						 if(!str.equals(""))
							 jobcatIds.add(Integer.parseInt(str));
						 } 
						 if(jobcatIds.size()>0){
							 listjJobOrders = jobOrderDAO.findByJobCategery(jobcatIds,districtOrSchoolId);
							 System.out.println("  job list by jobcategory :: "+listjJobOrders.size());
							 jobCateFlag=true;
						 }
						 if(jobCateFlag){
							filterJobOrderList.retainAll(listjJobOrders);
						 }else{
							filterJobOrderList.addAll(listjJobOrders);
						 }
						}
					  }

					if(jobCateFlag){
						jobFlag=true;
					}
				  
			  }
			
			
			
			Criterion criterionJobOrderType = Restrictions.eq("createdForEntity",JobOrderType);
			
			List<JobOrder> schoolJList	  =	 new ArrayList<JobOrder>();
			boolean schoolFlag=false;
			int searchschoolflag=2;
			if(schoolId!=0){
				SchoolMaster  sclMaster=null;
				if(districtOrSchoolId!=0){
					sclMaster=schoolMasterDAO.findById(Long.parseLong(schoolId+""),false,false);
				}
				schoolFlag=true;
				searchschoolflag=1;
				if(distMaster!=null)
					schoolJList=schoolInJobOrderDAO.findJobListBySchool(sclMaster,distMaster,JobOrderType,entityID,searchschoolflag,userMaster);
			}
			else
			{// Only for SA for Nobletype district
				if(userMaster.getEntityType()==3 && JobOrderType==2 && userMaster.getDistrictId()!=null && distMaster.getWritePrivilegeToSchool().equals(true))
				{
					searchschoolflag=0; // below [ schoolMaster ] is sending from school id by getting  session 
					schoolJList=schoolInJobOrderDAO.findJobListBySchool(schoolMaster,distMaster,JobOrderType,entityID,searchschoolflag,userMaster);
					schoolFlag=true;
				}
			}
			if(schoolFlag && schoolJList.size()>0){
				if(jobFlag){
					filterJobOrderList.retainAll(schoolJList);
				}else{
					filterJobOrderList.addAll(schoolJList);
				}
			}else if(schoolFlag && schoolJList.size()==0){
				filterJobOrderList = new ArrayList<JobOrder>();
			}

			if(schoolFlag){
				jobFlag=true;
			}
			
			/**********rajendra: search by jobrequisition no**********************/
			boolean jobReqnoFlag=false;
			List<JobOrder> jobOrderOfReqNoList	  =	 new ArrayList<JobOrder>();
			if(disJobReqNo!=null && !disJobReqNo.equals("") && !disJobReqNo.equals("0")){
				jobReqnoFlag=true;
				List<DistrictRequisitionNumbers> districtRequisitionNumbers=null;
				Criterion crDrq=Restrictions.eq("requisitionNumber", disJobReqNo);
				Criterion crDSTrq=null;
				if(districtOrSchoolId!=0){
					crDSTrq=Restrictions.eq("districtMaster", distMaster);
					districtRequisitionNumbers=districtRequisitionNumbersDAO.findByCriteria(crDSTrq,crDrq);
				}else{
					districtRequisitionNumbers=districtRequisitionNumbersDAO.findByCriteria(crDrq);
				}
				if(districtRequisitionNumbers!=null && districtRequisitionNumbers.size()>0){
					
					Criterion crJrq=Restrictions.in("districtRequisitionNumbers", districtRequisitionNumbers);
					List<JobRequisitionNumbers> jobRequisitionNumbers=jobRequisitionNumbersDAO.findByCriteria(crJrq);
					for(JobRequisitionNumbers jqr:jobRequisitionNumbers){
						jobOrderOfReqNoList.add(jqr.getJobOrder());
					}
				}
			}
			
			if(jobReqnoFlag && jobOrderOfReqNoList.size()>0){
				if(jobFlag){
					filterJobOrderList.retainAll(jobOrderOfReqNoList);
				}else{
					filterJobOrderList.addAll(jobOrderOfReqNoList);
				}
			}else if(jobReqnoFlag && jobOrderOfReqNoList.size()==0){
				
				filterJobOrderList = new ArrayList<JobOrder>();
			}
			
			if(jobReqnoFlag){
				jobFlag=true;
			}

			
			/**********rajendra: search by jobrequisition no**********************/
			
			// =============  Deepak filtering basis on job Status  added by 27-05-2015
				if(jobApplicationStatusId>0 && distMaster!=null && distMaster.getDistrictId().toString().equalsIgnoreCase("804800"))
				{
					boolean jobApplicationStatusFlag = false;
						List<JobOrder> jobApplicationStatusList = new ArrayList<JobOrder>();
							if (disJobReqNo != null && jobApplicationStatusId > 0) {
								jobApplicationStatusFlag = true;
								jobApplicationStatusList = jobOrderDAO.getJobApplicationStatusRole(jobApplicationStatusId);
								System.out.println("jobApplicationStatusList========"+ jobApplicationStatusList.size());
			
							}
							if (jobApplicationStatusFlag
									&& jobApplicationStatusList.size() > 0) {
								if (jobFlag == true) {
									filterJobOrderList.retainAll(jobApplicationStatusList);
								} else {
									filterJobOrderList.addAll(jobApplicationStatusList);
								}
							} else {
								filterJobOrderList = new ArrayList<JobOrder>();
							}
					if (jobApplicationStatusFlag) {
						jobFlag = true;
					}
				}
			
			
			 //     Mukesh filtering on the basis of zone
				List<JobOrder> listjobOrdersgeoZone =new ArrayList<JobOrder>();
			
			  if(geoZoneId>0)
				{
					GeoZoneMaster geoZoneMaster=geoZoneMasterDAO.findById(geoZoneId, false, false);
					if(userMaster.getRoleId().getRoleId().equals(3))
						listjobOrdersgeoZone = jobOrderDAO.getJobOrderBygeoZoneRole(geoZoneMaster,userMaster);
					else
						listjobOrdersgeoZone = jobOrderDAO.getJobOrderBygeoZone(geoZoneMaster);
				}
			 
			  if(listjobOrdersgeoZone!=null && listjobOrdersgeoZone.size()>0)
			  {
				  if(jobFlag==true){
					  filterJobOrderList.retainAll(listjobOrdersgeoZone);
					}else{
						filterJobOrderList.addAll(listjobOrdersgeoZone);
					}
			  }	
			  if(listjobOrdersgeoZone.size()==0 && geoZoneId==-1 ||geoZoneId==0)
			  {
				  filterJobOrderList.addAll(listjobOrdersgeoZone);
			  
			  }
			  else if(listjobOrdersgeoZone.size()==0 &&geoZoneId!=-1)
			  {
				  filterJobOrderList.retainAll(listjobOrdersgeoZone);
			  }
				
			  
		      if(jobFlag){
				if(filterJobOrderList.size()>0){
					List jobIds = new ArrayList();
					for(int e=0; e<filterJobOrderList.size(); e++) {
						jobIds.add(filterJobOrderList.get(e).getJobId());
					}
					Criterion criterionJobList = Restrictions.in("jobId",jobIds);

					Criterion criterionMix=criterionJobOrderType;
					if(statusFlag && jobOrderIdFlag){
						criterionMix=Restrictions.and(criterionStatus,criterionJobFilter);
					}else if(statusFlag){
						criterionMix=criterionStatus;
					}else if(jobOrderIdFlag){
						criterionMix=criterionJobFilter;
					}
					lstJobOrder=jobOrderDAO.findWithLimit(sortOrderStrVal,start,noOfRowInPage,criterionJobList,criterionJobOrderType,criterionMix);
					totalRecords=jobOrderDAO.getRowCountWithSort(sortOrderStrVal,criterionJobList,criterionJobOrderType,criterionMix);
				}
			}else{
				Criterion criterionMix=criterionJobOrderType;
				if(statusFlag && jobOrderIdFlag){
					criterionMix=Restrictions.and(criterionStatus,criterionJobFilter);
				}else if(statusFlag){
					criterionMix=criterionStatus;
				}else if(jobOrderIdFlag){
					criterionMix=criterionJobFilter;
				}
				lstJobOrder=jobOrderDAO.findWithLimit(sortOrderStrVal,start,noOfRowInPage,criterionJobOrderType,criterionMix);
				totalRecords=jobOrderDAO.getRowCountWithSort(sortOrderStrVal,criterionJobOrderType,criterionMix);
				
			}
			/***************************** End Code **************************************/

			Map<Integer,JobForTeacher> jobForTeacherList = new HashMap<Integer, JobForTeacher>();
			Map<Integer,JobCertification> jobCertificationList = new HashMap<Integer, JobCertification>();
			Map<Integer,JobOrder> jobOrderList =new HashMap<Integer, JobOrder>();
			List<TeacherDetail> teacherDetails = new ArrayList<TeacherDetail>();
			if(lstJobOrder.size()>0){
				jobForTeacherList = jobForTeacherDAO.findJFTApplicantbyJobOrders(lstJobOrder);
				List<Integer> joborderIds=new ArrayList<Integer>();
				for(JobOrder job: lstJobOrder){
					joborderIds.add(job.getJobId());
				}
				jobOrderList = jobOrderDAO.findByAllJobOrders(joborderIds); 

				try{
					for(int t=0;t<jobForTeacherList.size();t++){
						teacherDetails.add(jobForTeacherList.get(t).getTeacherId());
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			}

			List<JobCategoryMaster> jobCategoryMasterList= jobCategoryMasterDAO.findAllJobCategoryNameByOrder();
			List<TeacherAssessmentStatus> teacherAssessmentStatusList=teacherAssessmentStatusDAO.findByAllTeacherAssessStatusByTeacher(teacherDetails);
			List <StatusMaster> statusMasterList=WorkThreadServlet.statusMasters;
			StatusMaster statusMaster =findStatusByShortName(statusMasterList,"hide");
			StatusMaster statusMasterHird = findStatusByShortName(statusMasterList,"hird");

			List<TeacherStatusHistoryForJob> teacherSHFJobList=teacherStatusHistoryForJobDAO.findStatusAndSecStatusByTeacherList(teacherDetails);
			Map<String,TeacherStatusHistoryForJob> mapTSHFJob = new HashMap<String, TeacherStatusHistoryForJob>();
			for(TeacherStatusHistoryForJob forJob:teacherSHFJobList){
				if(forJob.getStatusMaster()!=null){
					mapTSHFJob.put(forJob.getJobOrder().getJobId()+"##"+forJob.getTeacherDetail().getTeacherId()+"##"+forJob.getStatusMaster().getStatusId()+"##0",forJob);
				}else{
					if(forJob.getSecondaryStatus()!=null){
						if(forJob.getSecondaryStatus().getSecondaryStatus_copy()!=null){
							mapTSHFJob.put(forJob.getJobOrder().getJobId()+"##"+forJob.getTeacherDetail().getTeacherId()+"##"+forJob.getSecondaryStatus().getSecondaryStatus_copy().getSecondaryStatusId()+"##1",forJob);
						}else{
							mapTSHFJob.put(forJob.getJobOrder().getJobId()+"##"+forJob.getTeacherDetail().getTeacherId()+"##"+forJob.getSecondaryStatus().getSecondaryStatusId()+"##1",forJob);
						}
					}
				}
			}
			totalRecord =totalRecords;

			if(totalRecord<end)
				end=totalRecord;

			tmRecords.append("<table  id='tblGrid' width='100%' border='0'>");
			tmRecords.append("<thead class='bg'>");
			tmRecords.append("<tr>");
			String responseText="";
			if(deafultFlag){
				sortOrderTypeVal="1";
			}
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblJobId", locale),sortOrderNoField,"jobId",sortOrderTypeVal,pgNo);
			tmRecords.append("<th valign='top'>"+responseText+"</th>");

			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblTitle", locale),sortOrderFieldName,"jobTitle",sortOrderTypeVal,pgNo);
			tmRecords.append("<th  valign='top'>"+responseText+"</th>");
			// zone added by mukesh
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblZone", locale),sortOrderFieldName,"geoZoneMaster",sortOrderTypeVal,pgNo);
			tmRecords.append("<th  valign='top'>"+responseText+"</th>");

			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblSub", locale),sortOrderFieldName,"subjectMaster",sortOrderTypeVal,pgNo);
			tmRecords.append("<th  valign='top'>"+responseText+"</th>");

			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblStatus", locale),sortOrderFieldName,"status",sortOrderTypeVal,pgNo);
			tmRecords.append("<th valign='top'>"+responseText+"</th>");

			//responseText=PaginationAndSorting.responseSortingLink("Activation Date",sortOrderFieldName,"jobStartDate",sortOrderTypeVal,pgNo);
			//tmRecords.append("<th width='14%' valign='top'>"+responseText+"</th>");

			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblPostedUntil", locale),sortOrderFieldName,"jobEndDate",sortOrderTypeVal,pgNo);
			tmRecords.append("<th  valign='top'>"+responseText+"</th>");

			tmRecords.append("<th  valign='top'>"+Utility.getLocaleValuePropByKey("lblApplicants", locale)+" <a href='#' id='iconpophover1' class='net-header-text ' rel='tooltip'data-original-title='"+Utility.getLocaleValuePropByKey("msgTotalnumberofapplicantsappliedforthisjob2", locale)+"'><span class='icon-question-sign'></span></a></th>");

			tmRecords.append("<th  valign='top'>"+Utility.getLocaleValuePropByKey("lblHires", locale)+"</th>");

			tmRecords.append("<th  valign='top'>"+Utility.getLocaleValuePropByKey("lblCandidateGrid", locale)+"</th>");
			
			tmRecords.append("<th  valign='top'>"+Utility.getLocaleValuePropByKey("lblDistrictAttachment", locale)+"</th>");

			tmRecords.append(" <th   valign='top'>"+Utility.getLocaleValuePropByKey("lblAct", locale)+"</th>");

			tmRecords.append("</tr>");
			tmRecords.append("</thead>");

			if(resultFlag==false){
				totalRecord=0;
			}
			
			/*================= Checking If Record Not Found ======================*/
        
			/********** Start :: For get Number of awailable candidates ****** */
			
			List <StatusMaster> statusMasterLst=WorkThreadServlet.statusMasters;
			Map<String, StatusMaster> mapStatus = new HashMap<String, StatusMaster>();
			for (StatusMaster sMaster : statusMasterLst) {
				mapStatus.put(sMaster.getStatusShortName(),sMaster);
			}
			
			List<JobOrder> jobLst= new ArrayList<JobOrder>();
			List<TeacherDetail> teacherList = new ArrayList<TeacherDetail>();
			
			//Get List of teacher and joborder
			for(int i=0;i<jobForTeacherList.size();i++){
				jobLst.add(jobForTeacherList.get(i).getJobId());
				teacherList.add(jobForTeacherList.get(i).getTeacherId());
			}
			
			TeacherAssessmentStatus teacherAssessmentStatusJSI = null;
			Map<Integer, Integer> mapAssess = new HashMap<Integer, Integer>();
			Map<Integer, TeacherAssessmentStatus> mapJSI = new HashMap<Integer, TeacherAssessmentStatus>();
			List<AssessmentJobRelation> assessmentJobRelations1=assessmentJobRelationDAO.findRelationByJobOrder(jobLst);
			List<AssessmentDetail> adList=new ArrayList<AssessmentDetail>();
			
			Map<Integer,InternalTransferCandidates> mapLstITRA = new HashMap<Integer, InternalTransferCandidates>();
			Map<Integer,TeacherPortfolioStatus> maptPortfolioStatus = new HashMap<Integer, TeacherPortfolioStatus>();
			Map<Integer,TeacherAssessmentStatus> maptBaseAssmentStatus = new HashMap<Integer, TeacherAssessmentStatus>();
			List<TeacherAssessmentStatus> lstJSI =new ArrayList<TeacherAssessmentStatus>(); 

			mapLstITRA 				= internalTransferCandidatesDAO.getDistrictForTeacherShowSatus(teacherList);
			maptPortfolioStatus 	= teacherPortfolioStatusDAO.findPortfolioStatusByTeacherlist(teacherList);
			maptBaseAssmentStatus 	= teacherAssessmentStatusDAO.findAssessmentStatusByTeacherListForBase(teacherList);
			
			if(assessmentJobRelations1.size()>0){
				for(AssessmentJobRelation ajr: assessmentJobRelations1){
					mapAssess.put(ajr.getJobId().getJobId(),ajr.getAssessmentId().getAssessmentId());
					adList.add(ajr.getAssessmentId());
				}
				if(adList.size()>0)
					lstJSI = teacherAssessmentStatusDAO.findJSITakenByAssessMentListAndTeacherList(teacherList,adList);
			}

			for(TeacherAssessmentStatus ts : lstJSI)
			{
				mapJSI.put(ts.getAssessmentDetail().getAssessmentId(), ts);
			}

			/********** End :: For get Number of awailable candidates ****** */
			//Approval authority
			UserMaster user = null;
			Integer userId = userMaster.getUserId();
			if(userMaster.getEntityType()!=1)
			{
				List<DistrictKeyContact> districtKeyContactList = new ArrayList<DistrictKeyContact>();
				districtKeyContactList = districtKeyContactDAO.findByContactType(districtMaster,"Job Approval");
				System.out.println("userId::: "+userId);
				
				for (DistrictKeyContact districtKeyContact : districtKeyContactList) {
					System.out.println("districtKeyContact.getUserMaster().getUserId():: "+districtKeyContact.getUserMaster().getUserId());
					if(districtKeyContact.getUserMaster().getUserId().equals(userId))
						user = districtKeyContact.getUserMaster();
				}
			}
			////////////////////////////////////
			boolean miamiShowFlag=true;
			try{
				if(userMaster.getDistrictId().getDistrictId()==1200390 && userMaster.getRoleId().getRoleId()==3 && userMaster.getEntityType()==3){
					miamiShowFlag=false;
				}
			}catch(Exception e){}	
			String windowFunc="if(this.href!='javascript:void(0);')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
			String fileName 	= 	"";
			String filePath		=	"";
			Integer counter = 1;
			if(resultFlag){
				if(lstJobOrder.size()==0)
					tmRecords.append("<tr><td colspan='10' align='center'>"+Utility.getLocaleValuePropByKey("lblNoRecord", locale)+"</td></tr>" );
					
				for (JobOrder jobOrderDetails : lstJobOrder) {
					noOfRecordCheck++;   
					int jobForTeacherSize=0;
					int jftAvailableCandidates=0;
					int jobForHiresSize=0;
					int totalNoOfCGViewCandidate =	0;	
					totalNoOfCGViewCandidate =	getTotalCountOfCGView(mapTSHFJob,""+jobOrderDetails.getJobId(),jobCategoryMasterList,jobForTeacherList,statusMasterList,jobOrderList,teacherAssessmentStatusList,userMaster);
					tmRecords.append("<tr>" );
					tmRecords.append("<td nowrap >"+jobOrderDetails.getJobId()+"</td>");
					if(jobOrderDetails.getIsPoolJob()==2)
					{
						tmRecords.append("<td><span class='requiredlarge'>*</span>"+jobOrderDetails.getJobTitle()+"</td>");
					}
					else
					{
						tmRecords.append("<td>"+jobOrderDetails.getJobTitle()+"</td>");
					}
					
					// Mukesh add zone name in grid in joborder
					if(jobOrderDetails.getGeoZoneMaster()!=null){
						if(jobOrderDetails.getGeoZoneMaster().getDistrictMaster().getDistrictId()==4218990)
						{
							tmRecords.append("<td><a href='javascript:void(0);' onclick=\"showPhiladelphiaPdf();\" return false;>"+jobOrderDetails.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
						}
						else if(jobOrderDetails.getGeoZoneMaster().getDistrictMaster().getDistrictId()==1200390){
							tmRecords.append("<td><a href='http://jobs.dadeschools.net/teachers/pdf/Geo_Zones_With_Schools.pdf' target='_blank'>"+jobOrderDetails.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
						}else{
							tmRecords.append("<td><a href='javascript:void(0);' onclick=\"return showZoneSchoolList("+jobOrderDetails.getGeoZoneMaster().getGeoZoneId()+",'"+jobOrderDetails.getGeoZoneMaster().getGeoZoneName()+"',"+jobOrderDetails.getDistrictMaster().getDistrictId()+")\";>"+jobOrderDetails.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
						}
					} else {
						tmRecords.append("<td></td>");
					}
					
					if(jobOrderDetails.getSubjectMaster()!=null){
						tmRecords.append("<td>"+jobOrderDetails.getSubjectMaster().getSubjectName()+"</td>");
					}else{
						tmRecords.append("<td></td>");
					}
					tmRecords.append("<td>");
					if(jobOrderDetails.getStatus().equalsIgnoreCase("A"))
						tmRecords.append(Utility.getLocaleValuePropByKey("lblAct", locale));
					else
						tmRecords.append(Utility.getLocaleValuePropByKey("lblInActiv", locale));
					tmRecords.append("</td>");

					if(Utility.convertDateAndTimeToUSformatOnlyDate(jobOrderDetails.getJobEndDate()).equals("Dec 25, 2099"))
						tmRecords.append("<td>"+Utility.getLocaleValuePropByKey("lblUntilfilled", locale)+"</td>");
					else
						tmRecords.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(jobOrderDetails.getJobEndDate())+",11:59 PM CST</td>");

					try{
						jobForTeacherSize=findJFTApplicantbyORHideJobOrder(mapTSHFJob,jobForTeacherList,jobOrderDetails,statusMaster,1,userMaster);
						jftAvailableCandidates = findJFTApplicantAvailableCandidates(mapTSHFJob,jobForTeacherList,jobOrderDetails,statusMaster,1,userMaster,mapAssess,teacherAssessmentStatusJSI,mapJSI,mapStatus,maptPortfolioStatus,mapLstITRA);
						if(jobForTeacherSize!=0){
							tmRecords.append("<td>"+jobForTeacherSize+"/"+jftAvailableCandidates+"</td>");
						}else{
							tmRecords.append("<td>0/0</td>");
						}
					}catch(Exception e){
						e.printStackTrace();
						tmRecords.append("<td >0</td>");
					}
					try{
						jobForHiresSize=findJFTApplicantbyORHideJobOrder(mapTSHFJob,jobForTeacherList,jobOrderDetails,statusMasterHird,2,userMaster);
						if(jobForHiresSize!=0){
							tmRecords.append("<td>"+jobForHiresSize+"</td>");
						}else{
							tmRecords.append("<td>0</td>");
						}
					}catch(Exception e){
						e.printStackTrace();
						tmRecords.append("<td>0</td>");
					}
					tmRecords.append("<td style='text-align:center'>&nbsp;<a data-original-title='CG View' rel='tooltip' id='cgn"+noOfRecordCheck+"' target=\"_blank\" href='candidategrid.do?jobId="+jobOrderDetails.getJobId()+"&JobOrderType="+JobOrderType+"'><span class='icon-table icon-large'></span></a></td>");
					jobOrderDetails.getDistrictMaster().getDistrictId();
					filePath	=	jobOrderDetails.getDistrictMaster().getDistrictId()+"/DistAttachmentFile";
					fileName 	=	jobOrderDetails.getDistrictAttachment() == null ? "" : jobOrderDetails.getDistrictAttachment();
					//filePath=Utility.getValueOfPropByKey("districtRootPath")+jobOrderDetails.getDistrictMaster().getDistrictId()+"/DistAttachmentFile";
					
					if(!fileName.equals("")){
						tmRecords.append("<td style='text-align:center'>");
						tmRecords.append("<a href='javascript:void(0);' rel='tooltip' data-original-title="+fileName+" class='attchToolTip"+noOfRecordCheck+"' id='refNotesfileopen"+counter+"' onclick=\"downloadAttachment('"+filePath+"','"+fileName+"');"+windowFunc+"\"><span class='icon-download-alt icon-large iconcolorBlue'></span></a>");
						tmRecords.append("</td>");
					} else {
						tmRecords.append("<td>&nbsp;</td>");
					}

					
					if(entityID==3 && JobOrderType==2){
						if(userMaster.getDistrictId().getsACreateDistJOb()!=null){
							if(userMaster.getUserId().equals(jobOrderDetails.getCreatedBy()) && userMaster.getDistrictId().getsACreateDistJOb()){
							tmRecords.append("<td nowrap><a title="+Utility.getLocaleValuePropByKey("msgEditicon", locale)+" href='editjoborder.do?JobOrderType="+JobOrderType+"&JobId="+jobOrderDetails.getJobId()+"&districtId="+jobOrderDetails.getDistrictMaster().getDistrictId().toString()+"'><i class='fa fa-pencil-square-o fa-lg' /></a>");						
							tmRecords.append("<a title="+Utility.getLocaleValuePropByKey("msgClone", locale)+" href='clonejoborder.do?JobOrderType="+JobOrderType+"&JobId="+jobOrderDetails.getJobId()+"&districtId="+jobOrderDetails.getDistrictMaster().getDistrictId().toString()+"'>  |<span class='icon-copy fa-lg iconcolorBlue'></span></a>");
							if(jobOrderDetails.getStatus().equalsIgnoreCase("A"))
								tmRecords.append("<a title="+Utility.getLocaleValuePropByKey("lblDeactivate", locale)+" href='javascript:void(0);' onclick=\"return activateDeactivateJob("+entityID+","+jobOrderDetails.getJobId()+",'I')\">| <i class='fa fa-times fa-lg' /></a>");
							else
								tmRecords.append("<a title="+Utility.getLocaleValuePropByKey("msgActivateicon", locale)+" href='javascript:void(0);' onclick=\"return activateDeactivateJob("+entityID+","+jobOrderDetails.getJobId()+",'A')\">| <i class='fa fa-check fa-lg' /></a>");
						}else{
							if(roleAccess.indexOf("|2|")!=-1 && miamiShowFlag){
								tmRecords.append("<td nowrap><a href='editjoborder.do?JobOrderType="+JobOrderType+"&JobId="+jobOrderDetails.getJobId()+"&districtId="+jobOrderDetails.getDistrictMaster().getDistrictId().toString()+"'>"+Utility.getLocaleValuePropByKey("lblEdit", locale)+"</a>");
							}else if(roleAccess.indexOf("|4|")!=-1 || miamiShowFlag==false){
								tmRecords.append("<td nowrap><a href='editjoborder.do?JobOrderType="+JobOrderType+"&JobId="+jobOrderDetails.getJobId()+"&districtId="+jobOrderDetails.getDistrictMaster().getDistrictId().toString()+"'>"+Utility.getLocaleValuePropByKey("lnkV", locale)+"</a>");
							}
						}					
					}else {
						
						if(roleAccess.indexOf("|2|")!=-1 && miamiShowFlag){
							tmRecords.append("<td nowrap><a href='editjoborder.do?JobOrderType="+JobOrderType+"&JobId="+jobOrderDetails.getJobId()+"&districtId="+jobOrderDetails.getDistrictMaster().getDistrictId().toString()+"'>"+Utility.getLocaleValuePropByKey("lblEdit", locale)+"</a>");
						}else if(roleAccess.indexOf("|4|")!=-1 || miamiShowFlag==false){
							tmRecords.append("<td nowrap><a href='editjoborder.do?JobOrderType="+JobOrderType+"&JobId="+jobOrderDetails.getJobId()+"&districtId="+jobOrderDetails.getDistrictMaster().getDistrictId().toString()+"'>"+Utility.getLocaleValuePropByKey("lnkV", locale)+"</a>");
						}
				      }	
					}else{
						boolean pipeFlag=false;
						if(roleAccess.indexOf("|2|")!=-1 && miamiShowFlag){
							tmRecords.append("<td nowrap><a title="+Utility.getLocaleValuePropByKey("msgEditicon", locale)+" href='editjoborder.do?JobOrderType="+JobOrderType+"&JobId="+jobOrderDetails.getJobId()+"&districtId="+jobOrderDetails.getDistrictMaster().getDistrictId().toString()+"'><i class='fa fa-pencil-square-o fa-lg' /></a>");
							pipeFlag=true;
							tmRecords.append("<a title="+Utility.getLocaleValuePropByKey("msgClone", locale)+" href='clonejoborder.do?JobOrderType="+JobOrderType+"&JobId="+jobOrderDetails.getJobId()+"&districtId="+jobOrderDetails.getDistrictMaster().getDistrictId().toString()+"'> |<span class='icon-copy fa-lg iconcolorBlue'></span></a>");
							pipeFlag=true;
						}else if(roleAccess.indexOf("|4|")!=-1 || miamiShowFlag==false){							
							tmRecords.append("<td nowrap><a href='editjoborder.do?JobOrderType="+JobOrderType+"&JobId="+jobOrderDetails.getJobId()+"&districtId="+jobOrderDetails.getDistrictMaster().getDistrictId().toString()+"'>"+Utility.getLocaleValuePropByKey("lnkV", locale)+"</a>");
							pipeFlag=true;
						}
						if(roleAccess.indexOf("|7|")!=-1 && miamiShowFlag){
							if(pipeFlag)tmRecords.append(" | ");
							if(jobOrderDetails.getStatus().equalsIgnoreCase("A"))
								tmRecords.append("<a title="+Utility.getLocaleValuePropByKey("lblDeactivate", locale)+"  href='javascript:void(0);' onclick=\"return activateDeactivateJob("+entityID+","+jobOrderDetails.getJobId()+",'I')\"><i class='fa fa-times fa-lg' />");
							else
								tmRecords.append("<a title="+Utility.getLocaleValuePropByKey("msgActivateicon", locale)+" href='javascript:void(0);' onclick=\"return activateDeactivateJob("+entityID+","+jobOrderDetails.getJobId()+",'A')\"><i class='fa fa-check fa-lg' />");
						}else{
							tmRecords.append("&nbsp;");
						}
						if(user!=null)
						tmRecords.append("&nbsp;|<a href='javascript:void(0);' onclick=\"return approveJob("+entityID+","+jobOrderDetails.getJobId()+","+userMaster.getUserId()+")\">"+Utility.getLocaleValuePropByKey("btnApprove", locale)+"</a>");
						tmRecords.append("</td>");
					}
				}
			}else{
				tmRecords.append("<tr><td colspan='10'>"+Utility.getLocaleValuePropByKey("msgNorecordfound", locale)+"</td></tr>" );
			}
			tmRecords.append("</table>");
			tmRecords.append(PaginationAndSorting.getPaginationStringForManageJobOrdersAjax(request,totalRecord,noOfRow, pageNo));
		}catch (Exception e) 
		{
			e.printStackTrace();
		}

		return tmRecords.toString();
	}
	public int findJFTApplicantbyORHideJobOrder(Map<String,TeacherStatusHistoryForJob> mapTSHFJob,Map<Integer,JobForTeacher> jobForTeacherList,JobOrder jobOrder,StatusMaster statusMaster,int hireOrApplication,UserMaster userMaster) 
	{
		int statusIdForSP=0;
		boolean statusIdSP=false;
		List<JobForTeacher> lstJobForTeacher=new ArrayList<JobForTeacher>();
		for(int i=0;i<jobForTeacherList.size();i++){
			if(jobForTeacherList.get(i).getJobId().getJobId().equals(jobOrder.getJobId())){
				if(hireOrApplication==1){
					if(!statusMaster.getStatusId().equals(jobForTeacherList.get(i).getStatus().getStatusId())){
						lstJobForTeacher.add(jobForTeacherList.get(i));
					}
				}else{
					if(statusMaster.getStatusId().equals(jobForTeacherList.get(i).getStatus().getStatusId())){
						statusIdForSP=0;
						statusIdSP=false;
						if(userMaster.getEntityType()==3 && jobOrder.getDistrictMaster().getStatusMaster()!=null){
							TeacherStatusHistoryForJob forJob=mapTSHFJob.get(jobOrder.getJobId()+"##"+jobForTeacherList.get(i).getTeacherId().getTeacherId()+"##"+jobOrder.getDistrictMaster().getStatusMaster().getStatusId()+"##0");	
							if(forJob!=null && forJob.getStatusMaster()!=null)
								if(forJob.getStatusMaster().getStatusShortName().equals("hird")&& forJob.getStatusMaster().getStatusId().equals(jobOrder.getDistrictMaster().getStatusMaster().getStatusId())){
									statusIdForSP=1;
								}
							statusIdSP=true;
						}else if(userMaster.getEntityType()==3 && jobOrder.getDistrictMaster().getSecondaryStatus()!=null){
							statusIdSP=true;
						}

						if(userMaster.getEntityType()==3 && statusIdForSP!=0){
							lstJobForTeacher.add(jobForTeacherList.get(i));
						}else if(statusIdSP==false){
							lstJobForTeacher.add(jobForTeacherList.get(i));
						}
					}
				}
			}
		}
		if(lstJobForTeacher!=null){
			return lstJobForTeacher.size();	
		}else{
			return 0;
		}
	}
	
	public int findJFTApplicantAvailableCandidates(Map<String,TeacherStatusHistoryForJob> mapTSHFJob,Map<Integer,JobForTeacher> jobForTeacherList,JobOrder jobOrder,StatusMaster statusMaster,int hireOrApplication,UserMaster userMaster,Map<Integer, Integer> mapAssess,TeacherAssessmentStatus teacherAssessmentStatusJSI,Map<Integer, TeacherAssessmentStatus> mapJSI,Map<String, StatusMaster> mapStatus,Map<Integer,TeacherPortfolioStatus> maptPortfolioStatus,Map<Integer,InternalTransferCandidates> mapLstITRA) 
	{
		List<JobForTeacher> lstJobForTeacher=new ArrayList<JobForTeacher>();
		
		try
		{
			DashboardAjax dbajax= null;
			StatusMaster originalStatus = new StatusMaster();
			TeacherPortfolioStatus tPortfolioStatus = new TeacherPortfolioStatus();;
			TeacherAssessmentStatus teacherBaseAssessmentStatus = new TeacherAssessmentStatus();
			
			for(int i=0;i<jobForTeacherList.size();i++)
			{
				DistrictMaster districtMasterInternal=null;
				InternalTransferCandidates internalITRA = null;
				
				if(jobForTeacherList.get(i).getJobId().getJobId().equals(jobOrder.getJobId()))
				{
					if(hireOrApplication==1)
					{
						dbajax= new DashboardAjax();
						originalStatus = null;
						tPortfolioStatus = null;
						teacherBaseAssessmentStatus = null;
						
						if(mapLstITRA!=null && mapLstITRA.size()>0)
						{
							if(jobForTeacherList.get(i).getTeacherId()!=null)
							{
								internalITRA=mapLstITRA.get(jobForTeacherList.get(i).getTeacherId().getTeacherId());
								if(internalITRA!=null)
									districtMasterInternal=internalITRA.getDistrictMaster();
							}
						}
						if(maptPortfolioStatus!=null && maptPortfolioStatus.size()>0)
						{
							if(jobForTeacherList.get(i).getTeacherId()!=null)
							{
								tPortfolioStatus = maptPortfolioStatus.get(jobForTeacherList.get(i).getTeacherId().getTeacherId());	
							}
						}
							 
						originalStatus = mapStatus.get("icomp");
						originalStatus = dbajax.findByTeacherIdJobStausForLoop(internalITRA,jobForTeacherList.get(i),mapAssess,teacherAssessmentStatusJSI,mapJSI,districtMasterInternal,tPortfolioStatus,mapStatus,teacherBaseAssessmentStatus);
						
						if(originalStatus!=null)
						{
							if(!statusMaster.getStatusId().equals(originalStatus.getStatusId()))
							{
								if(!jobForTeacherList.get(i).getStatus().getStatusShortName().equals("dcln") &&  !jobForTeacherList.get(i).getStatus().getStatusShortName().equals("vlt") && !jobForTeacherList.get(i).getStatus().getStatusShortName().equals("rem")&&!jobForTeacherList.get(i).getStatus().getStatusShortName().equals("hide")&& !jobForTeacherList.get(i).getStatus().getStatusShortName().equals("icomp") && !jobForTeacherList.get(i).getStatus().getStatusShortName().equals("widrw"))
								{
									//System.out.println(" o>>>>>>>>>>>>> originalStatus >>:: "+originalStatus.getStatusShortName());
									lstJobForTeacher.add(jobForTeacherList.get(i));
								}
							}
						}
					}
				}
			}
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
		if(lstJobForTeacher!=null){
			return lstJobForTeacher.size();	
		}else{
			return 0;
		}
	}
	
	
	public JobCertification findCertificationByJob(Map<Integer,JobCertification> jobCertificationList,JobOrder jobOrder, String CertificationType) 
	{
		JobCertification jobCertification=null;
		for(int i=0;i<jobCertificationList.size();i++){
			if(jobCertificationList.get(i).getJobId().getJobId().equals(jobOrder.getJobId())){
				if(jobCertificationList.get(i).getCertType().indexOf(CertificationType.trim())!=-1){
					jobCertification=jobCertificationList.get(i);
				}
			}
		}
		if(jobCertification==null)
			return null;
		else
			return jobCertification;
	}
	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public int checkDuplicateCertification(int jobId,String certType) 
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		List<JobCertification> lstJobCertification= null;
		try{
			JobOrder  jobOrder = jobOrderDAO.findById(jobId, false, false);
			Criterion criterion1 = Restrictions.eq("jobId",jobOrder);
			CertificateTypeMaster certificateTypeMaster = new CertificateTypeMaster();
			certificateTypeMaster.setCertTypeId(Integer.parseInt(certType));
			Criterion criterion2 = Restrictions.eq("certificateTypeMaster",certificateTypeMaster);

			lstJobCertification = jobCertificationDAO.findByCriteria(criterion1,criterion2);	
		}catch(Exception e){
			e.printStackTrace();
			return 0;
		}
		return lstJobCertification.size();

	}
	public int addCertification(int jobId,String certType,Long stateId)
	{
		/* ========  For Session time Out Error =========*/
		System.out.println("############addCertification################");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster = null;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}else{
			userMaster=(UserMaster)session.getAttribute("userMaster");
		}
		
		//Log
		List<JobCertification> lstJobCertificationLog= null;
		JobOrder  jobOrder = jobOrderDAO.findById(jobId, false, false);
		Criterion criterion1 = Restrictions.eq("jobId",jobOrder);
		lstJobCertificationLog = jobCertificationDAO.findByCriteria(criterion1);
		
		
		int existFlag=0;
		try{
			existFlag=checkDuplicateCertification(jobId,certType);
			if(existFlag==0){
				JobCertification jobCertification =new JobCertification();
				//JobOrder  jobOrder = jobOrderDAO.findById(jobId, false, false);
				StateMaster stateMaster=stateMasterDAO.findById(stateId, false, false);
				jobCertification.setJobId(jobOrder);
				CertificateTypeMaster certificateTypeMaster = new CertificateTypeMaster();
				certificateTypeMaster.setCertTypeId(Integer.parseInt(certType));
				jobCertification.setCertificateTypeMaster(certificateTypeMaster);
				jobCertification.setStateMaster(stateMaster);
				jobCertificationDAO.makePersistent(jobCertification);
				
				
				//sandeep		
				System.out.println("###########lstJobCertificationLog  "+lstJobCertificationLog);
				if(lstJobCertificationLog!= null)
					createLog.logForCertificate(request,(ArrayList<JobCertification>)lstJobCertificationLog , jobOrder, userMaster);
				
		        //end
				
			}
		}catch (Exception e) 
		{
			e.printStackTrace();
			return 0;
		}
		//if(existFlag==0)
			//saveJobOrderLog(jobId,certType);

		
		return existFlag;
	}
// mukesh	
	@Transactional(readOnly=false)
	public String addEditJobOrder(String jobStartTime,String jobEndTime,int jobTimeZoneId,int jobCategoryId,int schoolId,int jobId,int districtHiddenId,int exitURLMessageVal,
			String exitMessage,String exitURL,String certificationType,String schoolVal,int JobOrderType, int districtOrSchooHiddenlId,
			String  jobTitle,String refNo,String  jobDescription,String  jobQualification,int  noOfExpHires,String  jobStartDate,
			String  jobEndDate,String positionStartDate,String positionEndDate,int tempType,int jobApplicationStatus,int allSchoolGradeDistrictVal,int  isJobAssessment,int offerJSI,int  attachJobAssessment,String  assessmentDocument,
			int writePrivilegeValue,int subjectId,String jobDescriptionFileName,String arrReqNum,
			String arrReqNumAndSchoolId,String jobAuthKey,boolean isExpHireNotEqualToReqNo,String jobType,Integer geoZoneId,String statusIds,String secStatusIds,Integer rdReqNoSourceValue,String newTextRequisitionNumbers,
			boolean offerVirtualVideoInterview,String quesId,String maxScoreForVVI,boolean sendAutoVVILink,String slctStatusID,String timePerQues,String vviExpDays,boolean isInviteOnly,boolean offerDASMT,String distASMTId,boolean sendNotificationToschools,String jobIntNotes,String serviceTechnician,String salaryRange, String additionalDocumentFileName
			,String hiddenJob,String hoursPerDay,String reqPositionNum,String jeffcoSpecialEdFlag ,int jobIdNC, String[] jeffcoExtraFields,String positionStart,String schoolType,String statusActiveOrNot,Boolean jobCompletedVVILink,String strAdams,String positionNumber,String salaryAdminPlan,String approvalCode,String step,int jobApprovalProcessId,int clonejobId)
	{
		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<positionStartDate: "+positionStartDate+"positionEndDate: "+positionEndDate+"tempType :"+tempType+"jobApplicationStatus :"+jobApplicationStatus+" salaryRange==="+salaryRange+"  serviceTechnician=="+serviceTechnician);
		//System.out.println("=================================== addEditJobOrder ================================================================");
		/* ========  For Session time Out Error =========*/
		System.out.println("@:::::::::::salaryAdminPlan:"+salaryAdminPlan+":::approvalCode"+approvalCode+":step"+step+"jobApprovalProcessId="+jobApprovalProcessId);
		//System.out.println("strAdams"+strAdams);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
        //	for log maintainance
		boolean isNew = false;
		UserMaster userMaster = null;
		JobOrder jobOrderForLog = new JobOrder();
		
		
		String JobPostingURL="";
		JobOrder jobOrder=null;
		
		String returnjobTitle="";
		List<SchoolInJobOrder> existSchoolsList= null; //For exist Schools List
		List<SchoolInJobOrder> beforeCloneSchool=null;
		JobOrder cloneJobOrder=null;
		String adamsExtraFields[]={};
		if(!strAdams.equalsIgnoreCase(""))
			adamsExtraFields= strAdams.split("~");
		int returnJobId=0;
		try{
			
			DistrictMaster districtMaster=null;
			SchoolMaster schoolMaster=null;
			int entityID=0;
			int createdBy=0;
			int attachDefault=0;
			int attachDefaultSchool=0;
			int attachDefaultDistrict=0;
			boolean jobAssessment=false;
			

			if (session == null || session.getAttribute("userMaster") == null) {
				//return "false";
			}else{
				userMaster=(UserMaster)session.getAttribute("userMaster");
				entityID=userMaster.getEntityType();
				createdBy=userMaster.getUserId();
			}
			if(districtHiddenId!=0){
				districtMaster =districtMasterDAO.findById(districtHiddenId, false, false);
			}
			JobOrder jobOrderCheck=null;
			
			if(jobIdNC!=0)
			{
				cloneJobOrder= jobOrderDAO.findById(jobIdNC, false, false);
				beforeCloneSchool=schoolInJobOrderDAO.findJobOrder(cloneJobOrder);	
			}
						
			//System.out.println(" \n\n\n Gagan  Job Id : "+jobId+"  \n\t isJobAssessment : "+isJobAssessment+"\n\t attachJobAssessment : "+attachJobAssessment);
			Integer attachNewPillar = 0;
			boolean bShiftData=false;
			if(jobId==0){
				jobOrder=new JobOrder();
				jobOrder.setIsPoolJob(0);
				jobOrder.setIsVacancyJob(false);
				bShiftData=true;
				isNew=true;
			}else{
				jobOrder = jobOrderDAO.findById(jobId, false, false);
				returnJobId=jobId;
				attachNewPillar = jobOrder.getAttachNewPillar()==null?0:jobOrder.getAttachNewPillar();
				//jobOrderCheck = jobOrder;
				
				//Get Exist Schools
				existSchoolsList = schoolInJobOrderDAO.findJobOrder(jobOrder);	
				BeanUtils.copyProperties(jobOrder, jobOrderForLog);
				
			}
			if(statusActiveOrNot!=null && statusActiveOrNot.equalsIgnoreCase("A")){
				jobOrder.setStatus("A");
			}
			jobOrder.setJobStartTime(jobStartTime);
			jobOrder.setJobEndTime(jobEndTime);
			jobOrder.setHoursPerDay(hoursPerDay);
			jobOrder.setDistrictMaster(districtMaster);
			jobOrder.setJobTimeZoneId(jobTimeZoneId);
			//System.out.println(" writePrivilegeValue "+writePrivilegeValue);
			if(jobId==0)
            {			
				boolean approvalBeforeGoLive=districtMaster.getApprovalBeforeGoLive()==null?false:districtMaster.getApprovalBeforeGoLive();
				if(approvalBeforeGoLive)
				{
					jobOrder.setApprovalBeforeGoLive(0);
				}
				else
				{
					jobOrder.setApprovalBeforeGoLive(1);
				}
              }            
            
			if(writePrivilegeValue	==	0)
			{
				if(userMaster!=null && userMaster.getDistrictId()!=null && userMaster.getDistrictId().getDistrictId()==804800)
					jobOrder.setWritePrivilegeToSchool(true);
				else
				    jobOrder.setWritePrivilegeToSchool(false);
			}
			else
			{
				if(writePrivilegeValue==1)
				{
					jobOrder.setWritePrivilegeToSchool(true);
				}else{
					if(userMaster!=null && userMaster.getDistrictId()!=null && userMaster.getDistrictId().getDistrictId()==804800)
						jobOrder.setWritePrivilegeToSchool(true);
				}
			}
			
			if(isExpHireNotEqualToReqNo==true)
				jobOrder.setIsExpHireNotEqualToReqNo(true);
			else
				jobOrder.setIsExpHireNotEqualToReqNo(false);
			
			if(jobTitle!=null)
				jobOrder.setJobTitle(jobTitle);
			
			if(jobStartDate!=null)
				jobOrder.setJobStartDate(Utility.getCurrentDateFormart(jobStartDate));

			if(jobEndDate!=null)
				jobOrder.setJobEndDate(Utility.getCurrentDateFormart(jobEndDate));
			
			//jeffco 804800  -- 5509600
			if(districtMaster!=null && districtMaster.getDistrictId().toString().equalsIgnoreCase("804800")){
				try{
					   jobOrder.setRequisitionNumber(reqPositionNum);
				       if(jeffcoSpecialEdFlag!=null){
				       jobOrder.setSpecialEdFlag(jeffcoSpecialEdFlag);
				       }
				       
					   if(positionStartDate!=null)
						jobOrder.setPositionStartDate(Utility.getCurrentDateFormart(positionStartDate));			
						if(positionEndDate!=null && !positionEndDate.trim().equalsIgnoreCase(""))
						jobOrder.setPositionEndDate(Utility.getCurrentDateFormart(positionEndDate));
						
						if(tempType!=0)
						jobOrder.setTempType(tempType);	
						if(jobApplicationStatus!=0)
						jobOrder.setJobApplicationStatus(jobApplicationStatus);
						
						if(!serviceTechnician.trim().equalsIgnoreCase("") && !serviceTechnician.trim().equalsIgnoreCase("0"))
						jobOrder.setEmploymentServicesTechnician(employmentServicesTechnicianDAO.findById(Integer.parseInt(serviceTechnician), false, false));
						
						if(!additionalDocumentFileName.equals("")){
							jobOrder.setAdditionalDocumentPath(additionalDocumentFileName);
							try {
								moveAndDeleteTempJobDescriptionFile(districtHiddenId, additionalDocumentFileName);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
						if(jeffcoExtraFields!=null && jeffcoExtraFields.length>0){
							jobOrder.setFte(jeffcoExtraFields[0]);
							jobOrder.setPayGrade(jeffcoExtraFields[1]);
							jobOrder.setDaysWorked(jeffcoExtraFields[2]);
							jobOrder.setFsalary(jeffcoExtraFields[3]);
							jobOrder.setFsalaryA(jeffcoExtraFields[4]);
							jobOrder.setSsalary(jeffcoExtraFields[5]);
						}
						
				}catch(Exception e){e.printStackTrace();}
			}
			
			if(districtMaster!=null && districtMaster.getDistrictId().toString().equalsIgnoreCase("806900")){
				System.out.println(":::::::::::::::::Adams::::::::::::::::::::::");
				if(!strAdams.equalsIgnoreCase(""))
					adamsExtraFields= strAdams.split("~");
				if(jobApplicationStatus!=0)
					jobOrder.setJobApplicationStatus(jobApplicationStatus);
				
				System.out.println("salaryAdminPlan:"+salaryAdminPlan);
				
				jobOrder.setSalaryAdminPlan(salaryAdminPlan);
				jobOrder.setApprovalCode(approvalCode);
				jobOrder.setStep(step);
				
				if(adamsExtraFields.length>0){
					if(!adamsExtraFields[0].equals("##"))
					jobOrder.setFte(adamsExtraFields[0]);
					if(!adamsExtraFields[1].equals("##"))
					jobOrder.setPayGrade(adamsExtraFields[1]);
					if(!adamsExtraFields[2].equals("##"))
					jobOrder.setmSalary(adamsExtraFields[2]);
					if(!adamsExtraFields[3].equals("##"))
					jobOrder.setAccountCode(adamsExtraFields[3]);
					if(!adamsExtraFields[4].equals("##"))
					jobOrder.setPositionType(adamsExtraFields[4]);
					if(!adamsExtraFields[5].equals("##"))
					jobOrder.setMonthsPerYear(adamsExtraFields[5]);
					//jobOrder.setApiJobId(refNo);
					jobOrder.setRequisitionNumber(positionNumber);
					if(!adamsExtraFields[6].equals("##"))
					jobOrder.setJobCode(adamsExtraFields[6]);
					/*if(adamsExtraFields.length>6){
						jobOrder.setDepartmentApproval(adamsExtraFields[6]);
						System.out.println("@@@@@@@@@@@@@@@@@:::::::::department Approval:"+adamsExtraFields[6]);
					}*/
				}
			}
			
			
			//Added by Gaurav Kumar
			//System.out.println(":::::::::::::::DistrictID:::::::::::"+districtMaster.getDistrictId().toString());
			if(districtMaster!=null && (districtMaster.getDistrictId().toString().equalsIgnoreCase("7800047")||districtMaster.getDistrictId().toString().equalsIgnoreCase("7800292"))){
				//System.out.println("::::::::::::::::::::::::For Strive District:===="+positionStart);
				 if(positionStartDate!=null)
					jobOrder.setPositionStart(positionStart);
				 //schoolType
				 jobOrder.setGradeLevel(schoolType);
			}
			
			if(jobDescription!=null)
				jobOrder.setJobDescription(jobDescription);
			
			if(jobQualification!=null)
				jobOrder.setJobQualification(jobQualification);
			System.out.println("==========================================================   "+jobIntNotes);
			try {
				if(jobIntNotes!=null){
					jobOrder.setJobInternalNotes(jobIntNotes);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			System.out.println("=========================================================");
			  //jobOrder.setIsPoolJob(0);
			  jobOrder.setJobType(jobType);
			  GeoZoneMaster geoZoneMaster=null;
			  if(geoZoneId!=null)
			 	{
				  if(geoZoneId==0)
					 jobOrder.setGeoZoneMaster(null);
				  else{
				    // geoZoneMaster=geoZoneMasterDAO.findById(geoZoneId, false, false);
					  geoZoneMaster = geoZoneMasterDAO.loadById(geoZoneId);   //Anurag Optimization
					 jobOrder.setGeoZoneMaster(geoZoneMaster);
				  }
			 	}
			  jobOrder.setNotificationToschool(sendNotificationToschools);
			if(allSchoolGradeDistrictVal==1 && JobOrderType==2){
				jobOrder.setAllSchoolsInDistrict(2);
				jobOrder.setAllGrades(2);
				jobOrder.setPkOffered(2);
				jobOrder.setKgOffered(2);
				jobOrder.setG01Offered(2);
				jobOrder.setG02Offered(2);
				jobOrder.setG03Offered(2);
				jobOrder.setG04Offered(2);
				jobOrder.setG05Offered(2);
				jobOrder.setG06Offered(2);
				jobOrder.setG07Offered(2);
				jobOrder.setG08Offered(2);
				jobOrder.setG09Offered(2);
				jobOrder.setG10Offered(2);
				jobOrder.setG11Offered(2);
				jobOrder.setG12Offered(2);
				jobOrder.setSelectedSchoolsInDistrict(2);
				jobOrder.setNoSchoolAttach(1);

				if(noOfExpHires!=0){
					jobOrder.setNoOfExpHires(noOfExpHires);
				}	
			}
			
			if((allSchoolGradeDistrictVal==2 && JobOrderType==2) || JobOrderType==3){
				jobOrder.setAllGrades(2);
				jobOrder.setAllSchoolsInDistrict(2);
				jobOrder.setAllGrades(2);
				jobOrder.setPkOffered(2);
				jobOrder.setKgOffered(2);
				jobOrder.setG01Offered(2);
				jobOrder.setG02Offered(2);
				jobOrder.setG03Offered(2);
				jobOrder.setG04Offered(2);
				jobOrder.setG05Offered(2);
				jobOrder.setG06Offered(2);
				jobOrder.setG07Offered(2);
				jobOrder.setG08Offered(2);
				jobOrder.setG09Offered(2);
				jobOrder.setG10Offered(2);
				jobOrder.setG11Offered(2);
				jobOrder.setG12Offered(2);
				jobOrder.setSelectedSchoolsInDistrict(1);
				jobOrder.setNoSchoolAttach(2);
			}
			if(jobId!=0){
				if(!assessmentDocument.equals("") && isJobAssessment==1 && attachJobAssessment==2){
					try{
						String filePath="";
						if(JobOrderType==2){
							filePath=Utility.getValueOfPropByKey("districtRootPath")+jobOrder.getDistrictMaster().getDistrictId()+"/"+jobOrder.getAssessmentDocument();
						}else{
							filePath=Utility.getValueOfPropByKey("schoolRootPath")+jobOrder.getSchool().get(0).getSchoolId()+"/"+jobOrder.getAssessmentDocument();
						}
						File file = new File(filePath);
						if(file.delete()){
							//System.out.println(file.getName() + " is deleted!");
						}else{
							//System.out.println("Delete operation is failed.");
						}
					}catch(Exception e){
						//e.printStackTrace();
					}
				}
			}
			
			System.out.println("04");
			
			
			/*	Del Dist File */
			/*if(jobId!=0){
				if(!distAttachmentDocument.equals("")){
					try{
						String filePath="";
						if(JobOrderType==2){
							filePath=Utility.getValueOfPropByKey("districtRootPath")+jobOrder.getDistrictMaster().getDistrictId()+"/"+jobOrder.getDistrictAttachment();
						}else{
							filePath=Utility.getValueOfPropByKey("schoolRootPath")+jobOrder.getSchool().get(0).getSchoolId()+"/"+jobOrder.getDistrictAttachment();
						}
						File file = new File(filePath);
						if(file.delete()){
							//System.out.println(file.getName() + " is deleted!");
						}else{
							//System.out.println("Delete operation is failed.");
						}
					}catch(Exception e){
						//e.printStackTrace();
					}
				}
			}*/
			if(jobId!=0){
				jobAssessment=jobOrder.getIsJobAssessment();
				attachDefault=jobOrder.getAttachNewPillar();
				attachDefaultDistrict=jobOrder.getAttachDefaultDistrictPillar();
				attachDefaultSchool=jobOrder.getAttachDefaultSchoolPillar();
			}
			if(isJobAssessment==1){
				jobOrder.setIsJobAssessment(true);
				jobOrder.setJobAssessmentStatus(offerJSI);
				
			}else{
				jobOrder.setIsJobAssessment(false);
				jobOrder.setJobAssessmentStatus(offerJSI);
			}
			if(isJobAssessment==1 && attachJobAssessment==2){ System.out.println("1111111111111111");

				if(!assessmentDocument.equals(""))
					jobOrder.setAssessmentDocument(assessmentDocument);

				jobOrder.setAttachNewPillar(1);
				jobOrder.setAttachDefaultDistrictPillar(2);
				jobOrder.setAttachDefaultSchoolPillar(2);
				jobOrder.setAttachDefaultJobSpecificPillar(2);
			}else if(isJobAssessment==1 && attachJobAssessment==1){ System.out.println("222222222222222");
				jobOrder.setAssessmentDocument(null);
				jobOrder.setAttachNewPillar(2);
				jobOrder.setAttachDefaultDistrictPillar(1);
				jobOrder.setAttachDefaultSchoolPillar(2);
				jobOrder.setAttachDefaultJobSpecificPillar(2);
			}else if(isJobAssessment==1 && attachJobAssessment==3){ System.out.println("33333333333333333333");
				jobOrder.setAssessmentDocument(null);
				jobOrder.setAttachNewPillar(2);
				jobOrder.setAttachDefaultDistrictPillar(2);
				jobOrder.setAttachDefaultSchoolPillar(1);
				jobOrder.setAttachDefaultJobSpecificPillar(2);
			}else if(isJobAssessment==1 && attachJobAssessment==4){
				jobOrder.setAssessmentDocument(null);
				jobOrder.setAttachNewPillar(2);
				jobOrder.setAttachDefaultDistrictPillar(2);
				jobOrder.setAttachDefaultSchoolPillar(2);
				jobOrder.setAttachDefaultJobSpecificPillar(1);
			}else if(isJobAssessment==2){ System.out.println("555555555555555555555");
				jobOrder.setAttachNewPillar(2);
				jobOrder.setAttachDefaultDistrictPillar(2);
				jobOrder.setAttachDefaultSchoolPillar(2);
				jobOrder.setAttachDefaultJobSpecificPillar(2);
			}
			if(exitURLMessageVal==1){
				jobOrder.setExitURL(exitURL);
				jobOrder.setFlagForURL(1);
				jobOrder.setFlagForMessage(2);
			}else if(exitURLMessageVal==2){
				jobOrder.setExitMessage(exitMessage);
				jobOrder.setFlagForMessage(1);
				jobOrder.setFlagForURL(2);
			}

			jobOrder.setCreatedBy(createdBy);
			jobOrder.setCreatedByEntity(entityID);
			jobOrder.setCreatedForEntity(JobOrderType);
			
			if(refNo!=null){
				if(refNo.trim()!=null && !refNo.trim().equals("")){
					jobOrder.setApiJobId(refNo);
				}else{
					jobOrder.setApiJobId(null);
				}
			}else{
				jobOrder.setApiJobId(null);
			}

			System.out.println("05");
			
			try{
				JobCategoryMaster jobCategoryMaster = jobCategoryMasterDAO.findById(jobCategoryId, false, false);
				jobOrder.setJobCategoryMaster(jobCategoryMaster);
				SubjectMaster subjectMaster=null;
				if(subjectId!=0){
					subjectMaster=subjectMasterDAO.findById(subjectId, false, false);	
				}
				jobOrder.setSubjectMaster(subjectMaster);
			}catch (Exception e) {
				// TODO: handle exception
				//e.printStackTrace();
				JobCategoryMaster jobCategoryMaster = jobCategoryMasterDAO.findById(1, false, false);
				jobOrder.setJobCategoryMaster(jobCategoryMaster);
			}
			
			if(jobId==0){
				if(JobOrderType==2){
					if(districtMaster.getIsPortfolioNeeded())
						jobOrder.setIsPortfolioNeeded(true);
					else
						jobOrder.setIsPortfolioNeeded(false);
				}else{
					schoolMaster = schoolMasterDAO.findById(Long.parseLong(schoolId+""), false, false);
					if(schoolMaster.getIsPortfolioNeeded())
						jobOrder.setIsPortfolioNeeded(true);
					else
						jobOrder.setIsPortfolioNeeded(false);
				}

				jobOrder.setJobStatus("O");
				if(jobOrder!=null && (jobOrder.getJobId()==null || jobOrder.getJobId()==0  ))    // Don't make Active while update jobOrder.
				  jobOrder.setStatus("A");
				if(!jobDescriptionFileName.equals("")){
					jobOrder.setPathOfJobDescription(jobDescriptionFileName);
					try {
						moveAndDeleteTempJobDescriptionFile(districtHiddenId, jobDescriptionFileName);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				
				 /*For VVI Add*/
				try
				{
					System.out.println(" offerVirtualVideoInterview :: "+offerVirtualVideoInterview);
					
					if(offerVirtualVideoInterview)
					{
						StatusMaster statusMasterForVVI=new StatusMaster();
						SecondaryStatus secondaryStatusForVVI=new SecondaryStatus();
						Integer statusIdForSchoolPrivilegeForVVI=null;
						Integer secondaryStatusIdForSchoolPrivilegeForVVI=null;
						
						I4QuestionSets i4QuestionSets = null;
						
						if(quesId!=null && !quesId.equals(""))
							i4QuestionSets = i4QuestionSetsDAO.findById(Integer.parseInt(quesId), false, false);
						
						jobOrder.setOfferVirtualVideoInterview(offerVirtualVideoInterview);
						jobOrder.setI4QuestionSets(i4QuestionSets);
						
						if(maxScoreForVVI!=null && !maxScoreForVVI.equals(""))
							jobOrder.setMaxScoreForVVI(Integer.parseInt(maxScoreForVVI));
						
						jobOrder.setSendAutoVVILink(sendAutoVVILink);
						
						if(timePerQues!=null && !timePerQues.equals(""))
							jobOrder.setTimeAllowedPerQuestion(Integer.parseInt(timePerQues));
						else
							jobOrder.setTimeAllowedPerQuestion(null);
						
						if(vviExpDays!=null && !vviExpDays.equals(""))
							jobOrder.setVVIExpiresInDays(Integer.parseInt(vviExpDays));
						else
							jobOrder.setVVIExpiresInDays(null);
					
						if(sendAutoVVILink==true)
						{
							if(jobCompletedVVILink==null){
								jobOrder.setJobCompletedVVILink(null);
								jobOrder.setStatusMaster(null);
								jobOrder.setSecondaryStatus(null);
							}else if(jobCompletedVVILink==false){
								jobOrder.setJobCompletedVVILink(false);
								if(slctStatusID.contains("SSID_"))
								{
									secondaryStatusIdForSchoolPrivilegeForVVI=new Integer(slctStatusID.substring(5));
									secondaryStatusForVVI=secondaryStatusDAO.findById(secondaryStatusIdForSchoolPrivilegeForVVI, false, false);
									if(secondaryStatusForVVI!=null)
									{
										jobOrder.setSecondaryStatus(secondaryStatusForVVI);
									}
									jobOrder.setStatusMaster(null);
								}
								else
								{
									statusIdForSchoolPrivilegeForVVI=new Integer(slctStatusID);
									statusMasterForVVI=WorkThreadServlet.statusIdMap.get(statusIdForSchoolPrivilegeForVVI);
									if(statusMasterForVVI!=null)
									{
										jobOrder.setStatusMaster(statusMasterForVVI);
									}
									jobOrder.setSecondaryStatus(null);
								}
							}else if(jobCompletedVVILink==true){
								jobOrder.setJobCompletedVVILink(true);
								jobOrder.setStatusMaster(null);
								jobOrder.setSecondaryStatus(null);
							}
							
								
						}
						/*else
						{
							jobOrder.setStatusMaster(null);
							jobOrder.setSecondaryStatus(null);
						}*/
					}
					else
					{
						jobOrder.setOfferVirtualVideoInterview(offerVirtualVideoInterview);
						jobOrder.setSendAutoVVILink(sendAutoVVILink);
					}
					jobOrder.setIsInviteOnly(isInviteOnly);
					
					if(isInviteOnly){
						if(hiddenJob!=null && hiddenJob!=""){
							if(hiddenJob.equalsIgnoreCase("0"))
								jobOrder.setHiddenJob(false);
							else if(hiddenJob.equalsIgnoreCase("1"))
								jobOrder.setHiddenJob(true);
						}
					}else{
						jobOrder.setHiddenJob(null);	
					}
					
					/*else
					{
						jobOrder.setOfferVirtualVideoInterview(false);
						jobOrder.setSendAutoVVILink(false);
						jobOrder.setMaxScoreForVVI(null);
						jobOrder.setStatusMaster(null);
						jobOrder.setSecondaryStatus(null);
						jobOrder.setI4QuestionSets(null);
						jobOrder.setTimeAllowedPerQuestion(null);
						jobOrder.setVVIExpiresInDays(null);
					}*/
					
				}catch(Exception e)
				{
					e.printStackTrace();
				}
				
				jobOrderDAO.makePersistent(jobOrder);
				//Jeffaco School Cloning
				/*if(jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId()==804800){
					if(jobId==0 && clonejobId!=0){
						try{
							JobOrder jobToBeCloned=new JobOrder();
							jobToBeCloned.setJobId(clonejobId);
							List<SchoolInJobOrder> schoolToBeCloned=schoolInJobOrderDAO.findJobOrder(jobToBeCloned);
							if(schoolToBeCloned!=null && schoolToBeCloned.size()>0){
					        	 SchoolInJobOrder schoolJob=null;
					        	 for(SchoolInJobOrder sJob : schoolToBeCloned){
					        		 schoolJob=new SchoolInJobOrder();
					        		 schoolJob.setJobId(jobOrder);
					        		 schoolJob.setDistrictMaster(sJob.getDistrictMaster());
					        		 schoolJob.setSchoolId(sJob.getSchoolId());
					        		 schoolJob.setNoOfSchoolExpHires(sJob.getNoOfSchoolExpHires());
					        		 schoolJob.setCreatedDateTime(new Date());
					        		 schoolInJobOrderDAO.makePersistent(schoolJob);
								}
							}
						}catch(Exception e){
							e.printStackTrace();
						}
					}
				}*/
				
				//Added by Gaurav Kumar
				//JobApprovalProcess jobApprovalProcess=	jobApprovalProcessDAO.findById(jobApprovalProcessId, false, false);
				if(jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId()==806900){
					try{
						System.out.println("jobOrder id"+jobOrder.getJobId());
						JobWiseApprovalProcess jobWiseApproval = new JobWiseApprovalProcess();
						jobWiseApproval.setCreatedBy(userMaster);
						jobWiseApproval.setCreatedDateTime(new Date());
						jobWiseApproval.setJobApprovalProcessId(jobApprovalProcessDAO.findById(jobApprovalProcessId, false, false));
						jobWiseApproval.setJobId(jobOrder);
						jobWiseApprovalProcessDAO.makePersistent(jobWiseApproval);
						
						//jobOrder.setJobApprovalProcess(jobApprovalProcessDAO.findById(Integer.parseInt(serviceTechnician), false, false));
					}catch(Exception e){
						e.printStackTrace();
						
					}
				}
			
				
				if(isInviteOnly){
					jobOrder.setApiJobId(jobOrder.getJobId()+"");
					jobOrderDAO.makePersistent(jobOrder);
				}
				returnJobId=jobOrder.getJobId();
				returnjobTitle = jobOrder.getJobTitle();
				
				//For Add District Assessment Questions
				try
				{
					System.out.println(" offerDASMT >>>>>>>> add case :: "+offerDASMT);
					
					if(offerDASMT)
					{
						jobOrder.setOfferAssessmentInviteOnly(true);
						
						String arr[]=null;
						if(distASMTId!=null && !distASMTId.equals(""))
						{
								if(distASMTId!="" && distASMTId.length() > 0)
								{
									arr=distASMTId.split("#");
								}
								
								if(arr!=null && arr.length > 0)
								{
									for(int i=0;i<arr.length;i++)
									{
										DistrictAssessmentDetail districtAssessmentDetail = new DistrictAssessmentDetail();
										districtAssessmentDetail.setDistrictAssessmentId(Integer.parseInt(arr[i]));
										if(districtAssessmentDetail!=null)
										{
											DistrictAssessmentJobRelation districtAssessmentJobRelation = new DistrictAssessmentJobRelation();
											
											districtAssessmentJobRelation.setCreatedDateTime(new Date());
											districtAssessmentJobRelation.setDistrictAssessmentDetail(districtAssessmentDetail);
											districtAssessmentJobRelation.setJobCategoryMaster(jobOrder.getJobCategoryMaster());
											districtAssessmentJobRelation.setStatus("A");
											districtAssessmentJobRelation.setUserMaster(userMaster);
											districtAssessmentJobRelation.setIpaddress(IPAddressUtility.getIpAddress(request));
											districtAssessmentJobRelation.setJobOrder(jobOrder);
											districtAssessmentJobRelationDAO.makePersistent(districtAssessmentJobRelation);
										}
									}
								}
						}
					}
					else
					{
						jobOrder.setOfferAssessmentInviteOnly(false);
						//jobOrder.setDistrictAssessmentId(null);
					}
				}catch(Exception e)
				{e.printStackTrace();}
				
				//Mailing Function was here.
			}else{
				 jobOrder.setIsInviteOnly(isInviteOnly);
				 if(isInviteOnly){
						if(hiddenJob!=null && hiddenJob!=""){
							if(hiddenJob.equalsIgnoreCase("0"))
								jobOrder.setHiddenJob(false);
							else if(hiddenJob.equalsIgnoreCase("1"))
								jobOrder.setHiddenJob(true);
						}
				 }else{
					 jobOrder.setHiddenJob(null);
				 }
				 /*For VVI Edit*/ 
				try
				{
					if(offerVirtualVideoInterview)
					{
						StatusMaster statusMasterForVVI=new StatusMaster();
						SecondaryStatus secondaryStatusForVVI=new SecondaryStatus();
						Integer statusIdForSchoolPrivilegeForVVI=null;
						Integer secondaryStatusIdForSchoolPrivilegeForVVI=null;
						
						I4QuestionSets i4QuestionSets = null;
						
						if(quesId!=null && !quesId.equals(""))
							i4QuestionSets = i4QuestionSetsDAO.findById(Integer.parseInt(quesId), false, false);
						
						jobOrder.setOfferVirtualVideoInterview(offerVirtualVideoInterview);
						jobOrder.setI4QuestionSets(i4QuestionSets);
						
						if(maxScoreForVVI!=null && !maxScoreForVVI.equals(""))
							jobOrder.setMaxScoreForVVI(Integer.parseInt(maxScoreForVVI));
						
						jobOrder.setSendAutoVVILink(sendAutoVVILink);
						
						if(timePerQues!=null && !timePerQues.equals(""))
							jobOrder.setTimeAllowedPerQuestion(Integer.parseInt(timePerQues));
						else
							jobOrder.setTimeAllowedPerQuestion(null);
						
						if(vviExpDays!=null && !vviExpDays.equals(""))
							jobOrder.setVVIExpiresInDays(Integer.parseInt(vviExpDays));
						else
							jobOrder.setVVIExpiresInDays(null);
					
						if(sendAutoVVILink==true)
						{
							if(jobCompletedVVILink==null){
								jobOrder.setJobCompletedVVILink(null);
								jobOrder.setStatusMaster(null);
								jobOrder.setSecondaryStatus(null);
							}else if(jobCompletedVVILink==false){
								jobOrder.setJobCompletedVVILink(false);
								if(slctStatusID.contains("SSID_"))
								{
									secondaryStatusIdForSchoolPrivilegeForVVI=new Integer(slctStatusID.substring(5));
									secondaryStatusForVVI=secondaryStatusDAO.findById(secondaryStatusIdForSchoolPrivilegeForVVI, false, false);
									if(secondaryStatusForVVI!=null)
									{
										jobOrder.setSecondaryStatus(secondaryStatusForVVI);
									}
									jobOrder.setStatusMaster(null);
								}
								else
								{
									statusIdForSchoolPrivilegeForVVI=new Integer(slctStatusID);
									statusMasterForVVI=WorkThreadServlet.statusIdMap.get(statusIdForSchoolPrivilegeForVVI);
									if(statusMasterForVVI!=null)
									{
										jobOrder.setStatusMaster(statusMasterForVVI);
									}
									jobOrder.setSecondaryStatus(null);
								}
							}else if(jobCompletedVVILink==true){
								jobOrder.setJobCompletedVVILink(true);
								jobOrder.setStatusMaster(null);
								jobOrder.setSecondaryStatus(null);
							}
						}
						else
						{
							jobOrder.setStatusMaster(null);
							jobOrder.setSecondaryStatus(null);
						}
					}
					else
					{
						jobOrder.setOfferVirtualVideoInterview(false);
						jobOrder.setSendAutoVVILink(false);
						jobOrder.setMaxScoreForVVI(null);
						jobOrder.setStatusMaster(null);
						jobOrder.setSecondaryStatus(null);
						jobOrder.setI4QuestionSets(null);
						jobOrder.setTimeAllowedPerQuestion(null);
						jobOrder.setVVIExpiresInDays(null);
					}
					
					
				}catch(Exception e)
				{
					e.printStackTrace();
				}
				
				//For Edit District Assessment Questions 
				try
				{
					if(offerDASMT)
					{
						String arr[]=null;
						String extArr[]=null;
						if(distASMTId!=null && !distASMTId.equals(""))
						{
								if(distASMTId!="" && distASMTId.length() > 0)
								{
									arr=distASMTId.split("#");
								}
								
								if(arr!=null && arr.length > 0)
								{
									List<DistrictAssessmentJobRelation> lstDAJR = new ArrayList<DistrictAssessmentJobRelation>();
									
									lstDAJR = districtAssessmentJobRelationDAO.findRelationByJobOrder(jobOrder);
									
									extArr = new String[lstDAJR.size()];
									
									for(int i=0;i<extArr.length;i++)
									{
										extArr[i]=lstDAJR.get(i).getDistrictAssessmentDetail().getDistrictAssessmentId().toString();
									
										System.out.println(">>>>>>> "+extArr[i]+" size :: "+lstDAJR.size());
									}
									
									//For deleting element 
									for(int i=0;i<lstDAJR.size();i++)
									{
										int chk = Arrays.binarySearch(arr,lstDAJR.get(i).getDistrictAssessmentDetail().getDistrictAssessmentId().toString());
										
										System.out.println(lstDAJR.get(i).getDistrictAssessmentDetail().getDistrictAssessmentId()+" arr "+arr+" >>>>>>>>>> chk "+chk);
										
										if(chk<0)
										{
											lstDAJR.get(i).setStatus("I");
											districtAssessmentJobRelationDAO.makePersistent(lstDAJR.get(i));
										}
									}
									
									
									for(int i=0;i<arr.length;i++)
									{
										int chk = Arrays.binarySearch(extArr, arr[i]);
										
										System.out.println(extArr+" arr[i] "+arr[i]+" chk "+chk);
										
										//For inserting element which is not exist in table
										if(chk<0)
										{
											DistrictAssessmentDetail districtAssessmentDetail = new DistrictAssessmentDetail();
											districtAssessmentDetail.setDistrictAssessmentId(Integer.parseInt(arr[i]));
											if(districtAssessmentDetail!=null)
											{
												DistrictAssessmentJobRelation districtAssessmentJobRelation = new DistrictAssessmentJobRelation();
												
												districtAssessmentJobRelation.setCreatedDateTime(new Date());
												districtAssessmentJobRelation.setDistrictAssessmentDetail(districtAssessmentDetail);
												districtAssessmentJobRelation.setJobCategoryMaster(jobOrder.getJobCategoryMaster());
												districtAssessmentJobRelation.setStatus("A");
												districtAssessmentJobRelation.setUserMaster(userMaster);
												districtAssessmentJobRelation.setIpaddress(IPAddressUtility.getIpAddress(request));
												districtAssessmentJobRelation.setJobOrder(jobOrder);
												districtAssessmentJobRelationDAO.makePersistent(districtAssessmentJobRelation);
											}
										}
									}
								}
						}
					}
					else
					{
						jobOrder.setOfferAssessmentInviteOnly(false);
					//	jobOrder.setDistrictAssessmentId(null);
					}
				}catch(Exception e)
				{e.printStackTrace();}
				
				
				
				if(!jobDescriptionFileName.equals("")){
					if(jobOrder.getPathOfJobDescription()!=null && !jobOrder.getPathOfJobDescription().equals("")){
						removeJobDescriptionFile(districtHiddenId, jobOrder.getPathOfJobDescription(), 0);
					}
					jobOrder.setPathOfJobDescription(jobDescriptionFileName);
					try {
						moveAndDeleteTempJobDescriptionFile(districtHiddenId, jobDescriptionFileName);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				jobOrderDAO.updatePersistent(jobOrder);
				//Added by Gaurav Kumar
				try{
					System.out.println("jobOrder id"+jobOrder.getJobId());
					System.out.println(":::::::::jobApprovalProcessId"+jobApprovalProcessId);
					List<JobWiseApprovalProcess> jobWiseApprovalList = jobWiseApprovalProcessDAO.findByJobId(jobOrder);
					JobWiseApprovalProcess jobWiseApprovalProcess=null;
					if(jobWiseApprovalList!=null && jobWiseApprovalList.size()>0){
						jobWiseApprovalProcess = jobWiseApprovalList.get(0);
					System.out.println(":::::::::::;jobWiseApprovalProcess is loaded jobWiseApprovalProcessId"+jobWiseApprovalProcess.getJobWiseApprovalProcessId());
					jobWiseApprovalProcess.setCreatedBy(userMaster);	
					jobWiseApprovalProcess.setCreatedDateTime(new Date());
					jobWiseApprovalProcess.setJobApprovalProcessId(jobApprovalProcessDAO.findById(jobApprovalProcessId, false, false));
					jobWiseApprovalProcessDAO.updatePersistent(jobWiseApprovalProcess);
					}
					
					//jobOrder.setJobApprovalProcess(jobApprovalProcessDAO.findById(Integer.parseInt(serviceTechnician), false, false));
				}catch(Exception e){
					e.printStackTrace();
					
				}
	
			}
			
			//Shift Req No temp to main tbl
			if(bShiftData)
			{
				
				String tempId=session.getId();
				Criterion criterion2 = Restrictions.eq("tempId",tempId);
				List<Jobrequisitionnumberstemp> lstjr=new ArrayList<Jobrequisitionnumberstemp>();
				lstjr=jobRequisitionNumbersTempDAO.findByCriteria(criterion2);
				
				//if(jobOrder.getDistrictMaster().getIsReqNoRequired())
				//{
					JobRequisitionNumbers jobRequisitionNumbers=null;
					
					Map<SchoolMaster, Integer> mapTemp=new HashMap<SchoolMaster, Integer>();
					
					SchoolMaster schoolMaster_temp=null;
					
					DistrictRequisitionNumbers districtRequisitionNumbers_temp=null;
					
					for(Jobrequisitionnumberstemp pojo:lstjr)
					{
						
						if(pojo.getReqText()!=null && !pojo.getReqText().equalsIgnoreCase(""))
						{
							DistrictRequisitionNumbers districtRequisitionNumbers=new DistrictRequisitionNumbers();
							districtRequisitionNumbers=new DistrictRequisitionNumbers();
							districtRequisitionNumbers.setCreatedDateTime(new Date());
							districtRequisitionNumbers.setDistrictMaster(districtMaster);
							districtRequisitionNumbers.setIsUsed(false);
							districtRequisitionNumbers.setUserMaster(userMaster);
							
							if(jobOrder.getJobType()!=null && !jobOrder.getJobType().equals("") && jobOrder.getJobType().equalsIgnoreCase("P"))
								districtRequisitionNumbers.setPosType("P");
							else
								districtRequisitionNumbers.setPosType("F");
							
							//districtRequisitionNumbers.setPosType("F");
							districtRequisitionNumbers.setRequisitionNumber(pojo.getReqText());
							districtRequisitionNumbersDAO.makePersistent(districtRequisitionNumbers);
							pojo.setDistrictRequisitionNumbers(districtRequisitionNumbers);
						}
						
						schoolMaster_temp=pojo.getSchoolMaster();						
						districtRequisitionNumbers_temp=new DistrictRequisitionNumbers();
						
						if(pojo.getDistrictRequisitionNumbers()!=null)
						{
							districtRequisitionNumbers_temp=pojo.getDistrictRequisitionNumbers();
							
							//System.out.println("Test 03 pojo "+pojo.getJobRequisitionId() +" :: "+pojo.getTempId());
							jobRequisitionNumbers=new JobRequisitionNumbers();
							
							jobRequisitionNumbers.setJobOrder(jobOrder);
							jobRequisitionNumbers.setDistrictRequisitionNumbers(pojo.getDistrictRequisitionNumbers());
							jobRequisitionNumbers.setSchoolMaster(schoolMaster_temp);
							jobRequisitionNumbers.setStatus(0);
							jobRequisitionNumbersDAO.makePersistent(jobRequisitionNumbers);
							
							districtRequisitionNumbers_temp.setIsUsed(true);
							districtRequisitionNumbersDAO.makePersistent(districtRequisitionNumbers_temp);
						}
						
						//Gaurav Kumar
						try{
						if(mapTemp.get(schoolMaster_temp)==null)
						{
							//System.out.println("Test 04 Add schoolInJobOrderDAO for School "+schoolMaster_temp.getSchoolId());
							mapTemp.put(schoolMaster_temp, pojo.getNoOfSchoolExpHires());
							System.out.println("hires::::::::::"+pojo.getNoOfSchoolExpHires());
							SchoolInJobOrder schoolInJobOrder=new SchoolInJobOrder();
							schoolInJobOrder.setDistrictMaster(districtMaster);
							schoolInJobOrder.setJobId(jobOrder);
							schoolInJobOrder.setNoOfSchoolExpHires(pojo.getNoOfSchoolExpHires());
							schoolInJobOrder.setSchoolId(schoolMaster_temp);
							schoolInJobOrder.setCreatedDateTime(new Date());
							schoolInJobOrderDAO.makePersistent(schoolInJobOrder);
							
						}
						}catch(Exception e){
							e.printStackTrace();
						}
						//System.out.println("Update 04.01 districtRequisitionNumbers");
						
						/*if(districtRequisitionNumbers_temp!=null)
						{
							districtRequisitionNumbers_temp.setIsUsed(true);
							districtRequisitionNumbersDAO.makePersistent(districtRequisitionNumbers_temp);
						}*/

						//System.out.println("Updated 04.02 districtRequisitionNumbers");
						
						//System.out.println("Test 05 try to delete");
						jobRequisitionNumbersTempDAO.makeTransient(pojo);
						//System.out.println("Test 06 try to deleted Id "+pojo.getJobRequisitionId());
						
						
					}
				//}
				/*else
				{
					for(Jobrequisitionnumberstemp pojo:lstjr)
					{
						SchoolInJobOrder schoolInJobOrder=new SchoolInJobOrder();
						schoolInJobOrder.setJobId(jobOrder);
						schoolInJobOrder.setNoOfSchoolExpHires(pojo.getNoOfSchoolExpHires());
						schoolInJobOrder.setSchoolId(pojo.getSchoolMaster());
						schoolInJobOrder.setCreatedDateTime(new Date());
						schoolInJobOrderDAO.makePersistent(schoolInJobOrder);
					}
				}*/
				
				
			}
			//System.out.println("Test 07");
			
			//System.out.println("Test DID 00 allSchoolGradeDistrictVal "+allSchoolGradeDistrictVal +" arrReqNum.length() "+arrReqNum.length() +" arrReqNum "+arrReqNum);
			//if(arrReqNum!="" && arrReqNum.length()>0 && allSchoolGradeDistrictVal==1)
			
			System.out.println("rdReqNoSourceValue "+rdReqNoSourceValue+" , "+newTextRequisitionNumbers);
			
			if(allSchoolGradeDistrictVal==1)
			{
				//System.out.println("Test DID 01");
				List<Integer> reqIdList = new ArrayList<Integer>();
				
				if(rdReqNoSourceValue==1)
				{
					String arr[]=null;
					if(newTextRequisitionNumbers!=null && newTextRequisitionNumbers!="" && newTextRequisitionNumbers.length() > 0)
					{
						arr=newTextRequisitionNumbers.split(",");
					}

					if(arr!=null && arr.length > 0)
					{
						for(int i=0;i<arr.length;i++)
						{
							DistrictRequisitionNumbers districtRequisitionNumbers=new DistrictRequisitionNumbers();
							districtRequisitionNumbers=new DistrictRequisitionNumbers();
							districtRequisitionNumbers.setCreatedDateTime(new Date());
							districtRequisitionNumbers.setDistrictMaster(districtMaster);
							districtRequisitionNumbers.setIsUsed(false);
							districtRequisitionNumbers.setUserMaster(userMaster);
							
							if(jobOrder.getJobType()!=null && !jobOrder.getJobType().equals("") && jobOrder.getJobType().equalsIgnoreCase("P"))
								districtRequisitionNumbers.setPosType("P");
							else
								districtRequisitionNumbers.setPosType("F");
							
							//districtRequisitionNumbers.setPosType("F");
							districtRequisitionNumbers.setRequisitionNumber(arr[i]);
							districtRequisitionNumbersDAO.makePersistent(districtRequisitionNumbers);
							reqIdList.add(districtRequisitionNumbers.getDistrictRequisitionId());
						}
					}
					
					
					
				}
				else if(rdReqNoSourceValue==2)
				{
					String arr[]=null;
					if(arrReqNum!="" && arrReqNum.length() > 0)
					{
						arr=arrReqNum.split("#");
					}
						
					
					//System.out.println("Test DID 02 "+arr.length +" arrReqNum "+arrReqNum);
					
					if(arr!=null && arr.length > 0)
					{
						for(int i=0;i<arr.length;i++)
						{
							reqIdList.add(Integer.parseInt(arr[i]));	
						}
					}
				}
				
				
				//System.out.println("Test DID 03 "+reqIdList.size() +" allSchoolGradeDistrictVal "+allSchoolGradeDistrictVal);

				
				
				if(allSchoolGradeDistrictVal==1)
				{
					if(jobId==0)
					{
						if(reqIdList!=null && reqIdList.size() > 0)
						{
							List<DistrictRequisitionNumbers> lstDistrictRequisitionNumbers=districtRequisitionNumbersDAO.getDistrictRequisitionList(reqIdList);
							//System.out.println(" lstDistrictRequisitionNumbers  "+lstDistrictRequisitionNumbers.size());
							
							for(DistrictRequisitionNumbers drqn : lstDistrictRequisitionNumbers)
							{
								JobRequisitionNumbers jrqn = new JobRequisitionNumbers();
								jrqn.setJobOrder(jobOrder);
								jrqn.setDistrictRequisitionNumbers(drqn);
								jrqn.setStatus(0);
								jobRequisitionNumbersDAO.makePersistent(jrqn);
								
								drqn.setIsUsed(true);
								districtRequisitionNumbersDAO.makePersistent(drqn);
								
								
							}	
						}
							
					}
					else if(jobId!=0) 
					{
						//System.out.println("Edit for DID");
						Criterion criterion=Restrictions.eq("jobOrder", jobOrder);
						//Criterion criterion1=Restrictions.eq("status", 0);
						List<JobRequisitionNumbers> requisitionNumbers=jobRequisitionNumbersDAO.findByCriteria(criterion);
						Map<DistrictRequisitionNumbers,JobRequisitionNumbers> mapDBStore=new HashMap<DistrictRequisitionNumbers,JobRequisitionNumbers>();
						Map<JobRequisitionNumbers, Boolean> mapDBStoreForOpertion=new HashMap<JobRequisitionNumbers, Boolean>();
						for(JobRequisitionNumbers jobqn : requisitionNumbers)
						{
							mapDBStoreForOpertion.put(jobqn, true); // Old db
							mapDBStore.put(jobqn.getDistrictRequisitionNumbers(), jobqn);
						}
						
						if(reqIdList!=null && reqIdList.size() > 0)
						{
							List<DistrictRequisitionNumbers> lstDistrictRequisitionNumbers=districtRequisitionNumbersDAO.getDistrictRequisitionList(reqIdList);
							for(DistrictRequisitionNumbers drqn : lstDistrictRequisitionNumbers)
							{
								if(drqn!=null)
								{
									if(mapDBStore.get(drqn)==null)
									{
										JobRequisitionNumbers jrqn = new JobRequisitionNumbers();
										jrqn.setJobOrder(jobOrder);
										jrqn.setDistrictRequisitionNumbers(drqn);
										jrqn.setStatus(0);
										jobRequisitionNumbersDAO.makePersistent(jrqn);
										
										drqn.setIsUsed(true);
										districtRequisitionNumbersDAO.makePersistent(drqn);
									}
									else
									{
										JobRequisitionNumbers jobRequisitionNumberTemp=mapDBStore.get(drqn);
										mapDBStoreForOpertion.put(jobRequisitionNumberTemp, false); // Old db
									}
								}
							}
						}
						
						
						
						// delete for district
						Iterator iter = mapDBStoreForOpertion.entrySet().iterator();
						while (iter.hasNext()) {
							Map.Entry mEntry = (Map.Entry) iter.next();
							if(mEntry.getValue().equals(new Boolean(true)))
								
							{
								JobRequisitionNumbers jobRequisitionNumberTemp=(JobRequisitionNumbers)mEntry.getKey();
								
								DistrictRequisitionNumbers drqnTemp=jobRequisitionNumberTemp.getDistrictRequisitionNumbers();
								drqnTemp.setIsUsed(false);
								districtRequisitionNumbersDAO.makePersistent(drqnTemp);
								//System.out.println("Try to delete "+jobRequisitionNumberTemp.getJobRequisitionId());
								jobRequisitionNumbersDAO.makeTransient(jobRequisitionNumberTemp);
								
							}
						}
					 }
				}
			}
			/*int live=-1;
			if(Utility.getValueOfPropByKey("isAPILive")!=null && !Utility.getValueOfPropByKey("isAPILive").equals(""))
				live=Integer.parseInt(Utility.getValueOfPropByKey("isAPILive"));
				live!=-1
			*/
			int live=Integer.parseInt(Utility.getValueOfPropByKey("isAPILive"));
			if(live==1 && jobId!=0 && districtMaster!=null &&  districtMaster.getDistrictId().equals(3628590) && jobOrder.getJobId()!=null && jobOrder.getJobId()!=0)//SCSD Pats Posting
			{
				System.out.println("================== Updated by vishwanath");
				//// Vishwanath ( SCSD Pats Update)
				try {
					String locationCode = districtMaster.getLocationCode();
					if(schoolMaster!=null && schoolMaster.getLocationCode()!=null)
						locationCode = schoolMaster.getLocationCode();

					List<JobRequisitionNumbers> list=new ArrayList<JobRequisitionNumbers>();
					list = jobRequisitionNumbersDAO.findRequisitionsByJob(jobOrder);
					
					List<DistrictRequisitionNumbers> lstDistrictRequisitionNumbers = new ArrayList<DistrictRequisitionNumbers>();
					
					String reqNo = "";
					String postingNo = "";
					String noOfHires = "";
					if(list.size()>0)
					{
						JobRequisitionNumbers jobRequisitionNumbers= list.get(0);
						reqNo = jobRequisitionNumbers.getDistrictRequisitionNumbers().getRequisitionNumber();
						
						Criterion criteria5 = Restrictions.eq("requisitionNumber", reqNo);
						lstDistrictRequisitionNumbers = districtRequisitionNumbersDAO.findByCriteria(criteria5);
						
						System.out.println("lstDistrictRequisitionNumbers**inside manageJobOrderAjax*******************: "+lstDistrictRequisitionNumbers.size());
						
						if(lstDistrictRequisitionNumbers.size()>0)
						{
							postingNo = lstDistrictRequisitionNumbers.get(0).getPostingNo();
						}
						
						SchoolMaster sm = jobRequisitionNumbers.getSchoolMaster();
						if(sm!=null)
						{	
							System.out.println("sm:: "+sm.getSchoolId());
							locationCode = sm.getLocationCode();
						}
						noOfHires = "1";
						System.out.println("locationCode: "+locationCode+" reqNo: "+reqNo+" postingNo : "+postingNo+" noOfHires: "+noOfHires);
					}
					//TMCommonUtil.updatePatsPosting(jobOrder, locationCode, reqNo,noOfHires);
					PATSPostThread ppt = new PATSPostThread();
					ppt.setJobOrder(jobOrder);
					ppt.setLocationCode(locationCode);
					ppt.setReqNo(reqNo);
					ppt.setPostingNo(postingNo);
					ppt.start();
					System.out.println("======================================updatePatsPosting");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			/* ============== Gagan [ Functionality for Adding Requisition Number Start Add Mode to job Order  ] ================= */
			
			/*System.out.println(" \n\n\n\n arrReqNum [] "+arrReqNum+" \n\n arrReqNumAndSchoolId : "+arrReqNumAndSchoolId+
					" \n\n allSchoolGradeDistrictVal : "+allSchoolGradeDistrictVal);
			if(arrReqNum!="" && arrReqNum.length()>0)
			{
				
				String arr[]=arrReqNum.split("#");
				System.out.println(">>>>>>>>>>>>>>>>>>    no of requision : >>>>>>>>>>>>>>>>>>>>>>"+arr.length);
				List reqIdList = new ArrayList();
				for(int i=0;i<arr.length;i++)
				{
					reqIdList.add(Integer.parseInt(arr[i]));	
				}
				if(allSchoolGradeDistrictVal==1){
					if(jobId==0){
							List<DistrictRequisitionNumbers> lstDistrictRequisitionNumbers=districtRequisitionNumbersDAO.getDistrictRequisitionList(reqIdList);
							System.out.println(" lstDistrictRequisitionNumbers  "+lstDistrictRequisitionNumbers.size());
							
							for(DistrictRequisitionNumbers drqn : lstDistrictRequisitionNumbers)
							{
								JobRequisitionNumbers jrqn = new JobRequisitionNumbers();
								jrqn.setJobOrder(jobOrder);
								jrqn.setDistrictRequisitionNumbers(drqn);
								jrqn.setStatus(0);
								jobRequisitionNumbersDAO.makePersistent(jrqn);
								drqn.setIsUsed(true);
								districtRequisitionNumbersDAO.makePersistent(drqn);
							}
					}else if(jobId!=0) {
					
					Map<DistrictRequisitionNumbers, Boolean> map=new HashMap<DistrictRequisitionNumbers, Boolean>();	
					
					Criterion criterion=Restrictions.eq("jobOrder", jobOrder);
					List<JobRequisitionNumbers>requisitionNumbers=jobRequisitionNumbersDAO.findByCriteria(criterion);
					
					List<DistrictRequisitionNumbers> lstDistrictRequisitionNumbers=districtRequisitionNumbersDAO.getDistrictRequisitionList(reqIdList);
					for(DistrictRequisitionNumbers drqn : lstDistrictRequisitionNumbers)
					{
						map.put(drqn, true);
					}
					
					for(JobRequisitionNumbers numbers:requisitionNumbers){
						numbers.getDistrictRequisitionNumbers().setIsUsed(false);
							jobRequisitionNumbersDAO.makePersistent(numbers);
						if(numbers.getStatus()==0){
							map.put(numbers.getDistrictRequisitionNumbers(),true);
							jobRequisitionNumbersDAO.makeTransient(numbers);
						}else{
							map.put(numbers.getDistrictRequisitionNumbers(),false);
						}
					}
					
					
					for(DistrictRequisitionNumbers drqn : lstDistrictRequisitionNumbers)
					{
						if(map.get(drqn)){
							JobRequisitionNumbers jrqn = new JobRequisitionNumbers();
							jrqn.setJobOrder(jobOrder);
							jrqn.setDistrictRequisitionNumbers(drqn);
							jrqn.setStatus(0);
							jobRequisitionNumbersDAO.makePersistent(jrqn);
							drqn.setIsUsed(true);
							districtRequisitionNumbersDAO.makePersistent(drqn);
						}else{
							drqn.setIsUsed(true);
							districtRequisitionNumbersDAO.makePersistent(drqn);
						}
					}
				 }
				}
			}	
			
			System.out.println(">>>>>>>>>>>>>>>>  arrReqNumAndSchoolId ========= >>>>>>>>>  "+arrReqNumAndSchoolId);
			if(arrReqNumAndSchoolId!="" && arrReqNumAndSchoolId.length()>0)
			{
				if(allSchoolGradeDistrictVal==2){
					if(jobId==0){
						try{
							String fStr[]=arrReqNumAndSchoolId.split("##");
							for(int x=0;x<fStr.length;x++){
								String[] sStr=fStr[x].split("!!");
								schoolMaster=schoolMasterDAO.findById(Long.parseLong(sStr[0]), false, false);
								if(!jobOrder.getDistrictMaster().getIsReqNoRequired()){
									noOfExpHires=Integer.parseInt(sStr[1].replace("@@",""));
								}else{
									String[] exdrqStr=sStr[1].split("@@");
									noOfExpHires=Integer.parseInt(exdrqStr[0]);
									String[] drqIds=exdrqStr[1].split("#");
									List<Integer>listDrqId=new ArrayList<Integer>();
									
									for(int xx=0;xx<drqIds.length;xx++){
										listDrqId.add(Integer.parseInt(drqIds[xx]));
									}
									List<DistrictRequisitionNumbers> lstDistrictRequisitionNumbers=districtRequisitionNumbersDAO.getDistrictRequisitionList(listDrqId);
									
									for(DistrictRequisitionNumbers drqn : lstDistrictRequisitionNumbers)
									{
										JobRequisitionNumbers jrqn = new JobRequisitionNumbers();
										jrqn.setJobOrder(jobOrder);
										jrqn.setSchoolMaster(schoolMaster);
										jrqn.setDistrictRequisitionNumbers(drqn);	
										jrqn.setStatus(0);
										jobRequisitionNumbersDAO.makePersistent(jrqn);
										drqn.setIsUsed(true);
										districtRequisitionNumbersDAO.makePersistent(drqn);
									}
								}
								SchoolInJobOrder schoolInJobOrder=new SchoolInJobOrder();
								schoolInJobOrder.setJobId(jobOrder);
								schoolInJobOrder.setNoOfSchoolExpHires(noOfExpHires);
								schoolInJobOrder.setSchoolId(schoolMaster);
								schoolInJobOrder.setCreatedDateTime(new Date());
								schoolInJobOrderDAO.makePersistent(schoolInJobOrder);
							}
						}catch(Exception e){e.printStackTrace();}
					}else if(jobId!=0){
						String fStr[]=arrReqNumAndSchoolId.split("##");
						int noOfExpHiresSchool;
						for(int x=0;x<fStr.length;x++){
							String[] sStr=fStr[x].split("!!");
							schoolMaster=schoolMasterDAO.findById(Long.parseLong(sStr[0]), false, false);
							if(!jobOrder.getDistrictMaster().getIsReqNoRequired()){
								noOfExpHiresSchool=Integer.parseInt(sStr[1].replace("@@",""));
							}else{
								String[] exdrqStr=sStr[1].split("@@");
								noOfExpHiresSchool=Integer.parseInt(exdrqStr[0]);
								String[] drqIds=exdrqStr[1].split("#");
								List<Integer>listDrqId=new ArrayList<Integer>();
								
								for(int xx=0;xx<drqIds.length;xx++){
									listDrqId.add(Integer.parseInt(drqIds[xx]));
								}
								
								List<DistrictRequisitionNumbers> lstDistrictRequisitionNumbers=districtRequisitionNumbersDAO.getDistrictRequisitionList(listDrqId);
								List<JobRequisitionNumbers> lstschInj=jobRequisitionNumbersDAO.findByCriteria(Restrictions.eq("schoolMaster", schoolMaster),Restrictions.not(Restrictions.in("districtRequisitionNumbers", lstDistrictRequisitionNumbers)));
								for(JobRequisitionNumbers jrq:lstschInj){
									jrq.getDistrictRequisitionNumbers().setIsUsed(false);
									jobRequisitionNumbersDAO.makePersistent(jrq);
									jobRequisitionNumbersDAO.makeTransient(jrq);
								}
								
								for(DistrictRequisitionNumbers drqn : lstDistrictRequisitionNumbers)
								{
									JobRequisitionNumbers jrqn = new JobRequisitionNumbers();
									jrqn.setJobOrder(jobOrder);
									jrqn.setSchoolMaster(schoolMaster);
									jrqn.setDistrictRequisitionNumbers(drqn);	
									jrqn.setStatus(0);
									int s=jobRequisitionNumbersDAO.findByCriteria(Restrictions.eq("districtRequisitionNumbers", drqn),Restrictions.eq("schoolMaster", schoolMaster)).size();
									if(s==0){
										jobRequisitionNumbersDAO.makePersistent(jrqn);
										drqn.setIsUsed(true);
										districtRequisitionNumbersDAO.makePersistent(drqn);
									}
								}
							}
							List<SchoolInJobOrder> lstschoolInJobOrder=schoolInJobOrderDAO.findByCriteria(Restrictions.eq("jobId", jobOrder),Restrictions.eq("schoolId", schoolMaster));
							SchoolInJobOrder schoolInJobOrder=null;
							if(lstschoolInJobOrder!=null && lstschoolInJobOrder.size()>0){
								schoolInJobOrder=lstschoolInJobOrder.get(0);
							}else{
								schoolInJobOrder=new SchoolInJobOrder();
							}
							schoolInJobOrder.setJobId(jobOrder);
							schoolInJobOrder.setNoOfSchoolExpHires(noOfExpHiresSchool);
							schoolInJobOrder.setSchoolId(schoolMaster);
							schoolInJobOrder.setCreatedDateTime(new Date());
							schoolInJobOrderDAO.makePersistent(schoolInJobOrder);
						}
					}
					
				}
			}*/
			/****************by rajendra end :****************************/
			//*********Check and Insert AssessmentJobRelation ********/ 
			if(!(isJobAssessment==1 && attachJobAssessment==2)){
				AssessmentJobRelation  assessmentJobRelation=null;				
				assessmentJobRelation=assessmentJobRelationDAO.findRelationExistByJobOrder(jobOrder);
				if(attachJobAssessment!=2 && isJobAssessment==1){
					
					AssessmentDetail assessmentDetail=null;
					if(schoolId!=0)
						schoolMaster = schoolMasterDAO.findById(Long.parseLong(schoolId+""), false, false);

					if(attachJobAssessment==1){
						assessmentDetail=assessmentDetailDAO.checkIsDefault(districtMaster, null,1,null);
					}else if(attachJobAssessment==3){
						assessmentDetail=assessmentDetailDAO.checkIsDefault(null, schoolMaster,2,null);
					}else if(attachJobAssessment==4){
						assessmentDetail=assessmentDetailDAO.checkIsDefault(districtMaster, null,1,jobOrder);
					}
	
					if(assessmentDetail==null){
						// No default JSI
						// Create new JSI assessment
						AssessmentDetail assessmentDetailNew = new AssessmentDetail();
						assessmentDetailNew.setAssessmentName(districtMaster.getDistrictName()+" default JSI");
						assessmentDetailNew.setAssessmentDescription("");
						assessmentDetailNew.setAssessmentType(2);
						assessmentDetailNew.setIsDefault(true);
						assessmentDetailNew.setDistrictMaster(districtMaster);
						assessmentDetailNew.setIsResearchEPI(false);
						if(schoolMaster!=null)
						{
							assessmentDetailNew.setSchoolMaster(schoolMaster);
							assessmentDetailNew.setJobOrderType(2);
							assessmentDetailNew.setAssessmentName(schoolMaster.getSchoolName()+" default JSI");
						}else
							assessmentDetailNew.setJobOrderType(1);

						if(attachJobAssessment==4)
						{
							assessmentDetailNew.setAssessmentName(districtMaster.getDistrictName()+" default JSI for "+jobOrder.getJobCategoryMaster().getJobCategoryName());
							assessmentDetailNew.setJobCategoryMaster(jobOrder.getJobCategoryMaster());
						}

						String ipAddress = IPAddressUtility.getIpAddress(request);
						assessmentDetailNew.setIpaddress(ipAddress);
						assessmentDetailNew.setStatus("A");
						assessmentDetailNew.setUserMaster(userMaster);
						assessmentDetailNew.setCreatedDateTime(new Date());
						try{
							List<AssessmentDetail> assessmentDetails = assessmentDetailDAO.checkAssessmentByName(assessmentDetailNew.getAssessmentName());
							if(assessmentDetails.size()>0)
								assessmentDetailNew.setAssessmentName(assessmentDetailNew.getAssessmentName()+" "+Utility.now());
								
							assessmentDetailDAO.makePersistent(assessmentDetailNew);
						}catch (Exception e) {
							if(e.getCause() instanceof com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException)
							{
								e.printStackTrace();
							}
							
						}


						// attaching assessment

						if(assessmentJobRelation==null){
							assessmentJobRelation= new AssessmentJobRelation();
							assessmentJobRelation.setJobId(jobOrder);
							assessmentJobRelation.setCreatedBy(userMaster);
							assessmentJobRelation.setAssessmentId(assessmentDetailNew);
							assessmentJobRelation.setStatus("A");
							assessmentJobRelation.setIpaddress(ipAddress);
							assessmentJobRelationDAO.makePersistent(assessmentJobRelation);
						}else{
							assessmentJobRelation.setJobId(jobOrder);
							assessmentJobRelation.setCreatedBy(userMaster);
							assessmentJobRelation.setAssessmentId(assessmentDetailNew);
							assessmentJobRelation.setStatus("A");
							assessmentJobRelation.setIpaddress(ipAddress);
							assessmentJobRelationDAO.updatePersistent(assessmentJobRelation);
						}
					}else
					{	
						if(assessmentJobRelation==null){
							assessmentJobRelation= new AssessmentJobRelation();
							String ipAddress = IPAddressUtility.getIpAddress(request);
							assessmentJobRelation.setJobId(jobOrder);
							assessmentJobRelation.setCreatedBy(userMaster);
							assessmentJobRelation.setAssessmentId(assessmentDetail);
							assessmentJobRelation.setStatus("A");
							assessmentJobRelation.setIpaddress(ipAddress);
							System.out.println("<<<>>>>>>>>>>  "+ipAddress);
							assessmentJobRelationDAO.makePersistent(assessmentJobRelation);
						}else{
							
							System.out.println("attachNewPillarattachNewPillarattachNewPillarattachNewPillar: "+attachNewPillar);
							AssessmentJobRelation  assessmentJobRelationOld=null;
							AssessmentDetail assessmentDetailOld = null;
							if(attachNewPillar==1)
							{	
								assessmentJobRelationOld=assessmentJobRelationDAO.findRelationExistByJobOrder(jobOrder);
								assessmentDetailOld = assessmentJobRelationOld.getAssessmentId();
							}
							
							String ipAddress = IPAddressUtility.getIpAddress(request);
							assessmentJobRelation.setJobId(jobOrder);
							assessmentJobRelation.setCreatedBy(userMaster);
							assessmentJobRelation.setAssessmentId(assessmentDetail);
							assessmentJobRelation.setStatus("A");
							assessmentJobRelation.setIpaddress(ipAddress);
							assessmentJobRelationDAO.updatePersistent(assessmentJobRelation);
							
							
							System.out.println("-----------------------------------------");
							System.out.println("-----------------------------------------"+assessmentDetail.getAssessmentName());
							
							if(attachNewPillar==1)
							{	
								
								System.out.println("vishwanath -----------------------------------------"+assessmentDetailOld.getAssessmentName());
								List<AssessmentSections> assessmentSections = assessmentSectionDAO.getSectionsByAssessmentId(assessmentDetailOld);
								if(assessmentSections.size()==0)
								{
									assessmentDetailDAO.makeTransient(assessmentDetailOld);
								}
								
							}

							
						}
					}
					
					
				}else{
					if(assessmentJobRelation!=null){
						try{
							if(jobId!=0)
							{
								AssessmentDetail assessmentDetail = assessmentJobRelation.getAssessmentId();
								assessmentJobRelationDAO.makeTransient(assessmentJobRelation);
								//Integer attachNewPillar = jobOrderCheck.getAttachNewPillar()==null?0:jobOrderCheck.getAttachNewPillar();
								System.out.println("attachNewPillarattachNewPillarattachNewPillarattachNewPillar: "+attachNewPillar);
								if(attachNewPillar==1)
								{
									List<AssessmentSections> assessmentSections = assessmentSectionDAO.getSectionsByAssessmentId(assessmentDetail);
									if(assessmentSections.size()==0)
									{
										assessmentDetailDAO.makeTransient(assessmentDetail);
									}
								}
							}
						}catch (Exception e) {
							e.printStackTrace();
						}
					}
				}

			}else
			{
				///////////////////////////////////////
				// let me add my jsi

				AssessmentJobRelation  assessmentJobRelation=null;
				assessmentJobRelation=assessmentJobRelationDAO.findRelationExistByJobOrder(jobOrder);
				System.out.println("=================================================================");
				System.out.println("assessmentJobRelation: "+assessmentJobRelation);
				AssessmentDetail assessmentDetail = null;
				if(assessmentJobRelation!=null)
				{
					assessmentDetail = assessmentJobRelation.getAssessmentId();
					System.out.println("assessmentDetail: "+assessmentDetail.getAssessmentName());
					if(assessmentDetail.getIsDefault())
						assessmentDetail = null;
				}
				System.out.println("assessmentJobRelation: "+assessmentJobRelation+" , assessmentDetail: "+assessmentDetail);
				if(assessmentDetail==null){

					// Create new JSI assessment
					AssessmentDetail assessmentDetailNew = new AssessmentDetail();

					assessmentDetailNew.setAssessmentName(districtMaster.getDistrictName()+" JSI for "+jobOrder.getJobTitle()+" (JobId: "+jobOrder.getJobId()+")");
					assessmentDetailNew.setAssessmentDescription("");
					assessmentDetailNew.setAssessmentType(2);
					assessmentDetailNew.setIsDefault(false);
					assessmentDetailNew.setDistrictMaster(districtMaster);
					assessmentDetailNew.setIsResearchEPI(false);
					if(schoolMaster!=null)
					{
						assessmentDetailNew.setSchoolMaster(schoolMaster);
						assessmentDetailNew.setJobOrderType(2);
						assessmentDetailNew.setAssessmentName(schoolMaster.getSchoolName()+" JSI for "+jobOrder.getJobTitle()+" (JobId: "+jobOrder.getJobId()+")");
					}else
						assessmentDetailNew.setJobOrderType(1);

					System.out.println("jobOrder.getJobCategoryMaster()::::::::::: "+jobOrder.getJobCategoryMaster());
					if(jobOrder.getJobCategoryMaster()!=null)
						assessmentDetailNew.setJobCategoryMaster(jobOrder.getJobCategoryMaster());
					
					String ipAddress = IPAddressUtility.getIpAddress(request);
					assessmentDetailNew.setIpaddress(ipAddress);
					assessmentDetailNew.setStatus("A");
					assessmentDetailNew.setUserMaster(userMaster);
					assessmentDetailNew.setCreatedDateTime(new Date());

					assessmentDetailDAO.makePersistent(assessmentDetailNew);

					// attaching 
					if(assessmentJobRelation==null){
						assessmentJobRelation= new AssessmentJobRelation();
						assessmentJobRelation.setJobId(jobOrder);
						assessmentJobRelation.setCreatedBy(userMaster);
						assessmentJobRelation.setAssessmentId(assessmentDetailNew);
						assessmentJobRelation.setStatus("A");
						assessmentJobRelation.setIpaddress(ipAddress);
						assessmentJobRelationDAO.makePersistent(assessmentJobRelation);
					}else{
						assessmentJobRelation.setJobId(jobOrder);
						assessmentJobRelation.setCreatedBy(userMaster);
						assessmentJobRelation.setAssessmentId(assessmentDetailNew);
						assessmentJobRelation.setStatus("A");
						assessmentJobRelation.setIpaddress(ipAddress);
						assessmentJobRelationDAO.updatePersistent(assessmentJobRelation);
					}

				}else
				{	
					if(assessmentJobRelation==null){
						assessmentJobRelation= new AssessmentJobRelation();
						String ipAddress = IPAddressUtility.getIpAddress(request);
						assessmentJobRelation.setJobId(jobOrder);
						assessmentJobRelation.setCreatedBy(userMaster);
						assessmentJobRelation.setAssessmentId(assessmentDetail);
						assessmentJobRelation.setStatus("A");
						assessmentJobRelation.setIpaddress(ipAddress);
						assessmentJobRelationDAO.makePersistent(assessmentJobRelation);
					}else{
						String ipAddress = IPAddressUtility.getIpAddress(request);
						assessmentJobRelation.setJobId(jobOrder);
						assessmentJobRelation.setCreatedBy(userMaster);
						assessmentJobRelation.setAssessmentId(assessmentDetail);
						assessmentJobRelation.setStatus("A");
						assessmentJobRelation.setIpaddress(ipAddress);
						assessmentJobRelationDAO.updatePersistent(assessmentJobRelation);
					}
				}

			}
			
			/////////////////////////////////////////////////
			if(noOfExpHires!=0){
				jobOrder.setNoOfExpHires(noOfExpHires);
			}

			if(allSchoolGradeDistrictVal==1 && JobOrderType==2){
				deleteSchoolInJobOrder(jobOrder.getJobId(),0);
			}
			//comment by rajendra :
			/*if(JobOrderType==2 && jobId==0){
				if(schoolVal!="" && !schoolVal.equalsIgnoreCase("") && allSchoolGradeDistrictVal==2){
					try{
						String idAndHires[]=schoolVal.split(",");
						for(int i=0;i<idAndHires.length;i++){
							SchoolInJobOrder schoolInJobOrder= new SchoolInJobOrder();
							String ids[]=idAndHires[i].split("-");
							schoolInJobOrder.setJobId(jobOrder);
							schoolMaster = schoolMasterDAO.findById(Long.parseLong(ids[0]), false, false);
							schoolInJobOrder.setSchoolId(schoolMaster);
							schoolInJobOrder.setNoOfSchoolExpHires(Integer.parseInt(ids[1]));
							schoolInJobOrder.setCreatedDateTime(new Date());
							schoolInJobOrderDAO.makePersistent(schoolInJobOrder);
						}
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			}*/

			if(JobOrderType==3){
				/*System.out.println("\n\n Gagan:  JobOrderType "+JobOrderType);
				System.out.println(" shksfshf___________EntityType --------------dfdffd "+userMaster.getEntityType()+" ==== requisitionNumber"+requisitionNumber.equalsIgnoreCase(null)+" -----Space------"+requisitionNumber.equalsIgnoreCase(""));
				if(!requisitionNumber.equalsIgnoreCase("") )
				{
					System.out.println(" if requisitionNumber"+requisitionNumber.length());
					jobOrder.setRequisitionNumber(requisitionNumber);
					jobOrder.setStatus("A");
				}
				else
				{
					System.out.println(" else requisitionNumber");
					jobOrder.setStatus("I");
				}*/
				
				//comment by rajendra :
				deleteSchoolInJobOrder(jobOrder.getJobId(),0);
				try{
					SchoolInJobOrder schoolInJobOrder= new SchoolInJobOrder();
					schoolInJobOrder.setJobId(jobOrder);
					schoolMaster = schoolMasterDAO.findById(Long.parseLong(schoolId+""), false, false);
					if(noOfExpHires!=0){
						schoolInJobOrder.setNoOfSchoolExpHires(noOfExpHires);
					}else{
						schoolInJobOrder.setNoOfSchoolExpHires(null);
					}
					schoolInJobOrder.setSchoolId(schoolMaster);
					schoolInJobOrder.setCreatedDateTime(new Date());
					schoolInJobOrderDAO.makePersistent(schoolInJobOrder);
				}catch(Exception e){
					e.printStackTrace();
				}				
			}
			
			
			
			if(certificationType!="" && !certificationType.equalsIgnoreCase("")){
				try{
					String certificationTypes[]=certificationType.split(",");
					for(int i=0;i<certificationTypes.length;i++){
						JobCertification jobCertification= new JobCertification();
						//String certificationTypeVal=certificationTypes[i].replace("<|>",",");
						String certificationTypeVal=certificationTypes[i];
						CertificateTypeMaster certificateTypeMaster = new CertificateTypeMaster();
						certificateTypeMaster.setCertTypeId(Integer.parseInt(certificationTypeVal));
						jobCertification.setCertificateTypeMaster(certificateTypeMaster);
						//jobCertification.setCertType(certificationTypeVal);
						jobCertification.setJobId(jobOrder);

						jobCertificationDAO.makePersistent(jobCertification);
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			}


			boolean schoolFlag=false;
			boolean jsaFlag=false;
			String nameOfEntity=null;
			String aUploadUrl=null;
			if(schoolVal!="" && !schoolVal.equalsIgnoreCase("") && allSchoolGradeDistrictVal==2){
				schoolFlag=true;
			}
			if(jobOrder.getIsJobAssessment()){
				jsaFlag=true;
			}
			
			List<SchoolMaster> schoolIds=schoolInJobOrderDAO.findSchoolIds(jobOrder);
			
			if(isJobAssessment==1){
				schoolMaster = schoolMasterDAO.findById(Long.parseLong(schoolId+""), false, false);
				if(attachJobAssessment==1){
					try{
						nameOfEntity=jobOrder.getDistrictMaster().getDistrictName();
					}catch(Exception e){}
					try{
						aUploadUrl=Utility.getBaseURL(request)+"showFile.do?type=2&id="+jobOrder.getDistrictMaster().getDistrictId()+"&file="+jobOrder.getDistrictMaster().getAssessmentUploadURL();
					}catch(Exception e){}
				}else if(attachJobAssessment==3){
					try{
						nameOfEntity=schoolMaster.getSchoolName();
					}catch(Exception e){}
					try{
						aUploadUrl=Utility.getBaseURL(request)+"showFile.do?type=3&id="+schoolMaster.getSchoolId()+"&file="+schoolMaster.getAssessmentUploadURL();
					}catch(Exception e){}
				}else if(attachJobAssessment==2){
					if(JobOrderType==2){
						try{
							nameOfEntity=jobOrder.getDistrictMaster().getDistrictName();
						}catch(Exception e){}
						try{
							aUploadUrl=Utility.getBaseURL(request)+"showFile.do?type=2&id="+jobOrder.getDistrictMaster().getDistrictId()+"&file="+jobOrder.getAssessmentDocument();
						}catch(Exception e){}
					}else{
						try{
							nameOfEntity=schoolMaster.getSchoolName();
						}catch(Exception e){}
						try{
							aUploadUrl=Utility.getBaseURL(request)+"showFile.do?type=3&id="+schoolMaster.getSchoolId()+"&file="+jobOrder.getAssessmentDocument();
						}catch(Exception e){}
					}
				}
			}
			List<SchoolInJobOrder> schoolJobOrderLst= null;
			schoolJobOrderLst = schoolInJobOrderDAO.findJobOrder(jobOrder);		
			int ischoolJobOrder=0;
			String schoolsRecords ="";
			for (SchoolInJobOrder schoolInJobOrder : schoolJobOrderLst){
				if(ischoolJobOrder!=0){
					schoolsRecords+=", ";
				}
				schoolsRecords+=schoolInJobOrder.getSchoolId().getSchoolName();
				ischoolJobOrder++;
			}
			
			System.out.println(" >>>>>jobOrder.getNotificationToschool(): "+jobOrder.getNotificationToschool()+">>>>>>>>>>>>>>> added school in schoolIn joborder   :::::::::::::::: "+schoolsRecords);
			
			try
			{
				boolean mailFlag = true;
				System.out.println(" jobOrder.getDistrictMaster().getDistrictId() :|||||||================: "+jobOrder.getDistrictMaster().getDistrictId());
				
				//prevent school Add/Edit mail according district
				if(jobOrder.getNotificationToschool()!=null && jobOrder.getNotificationToschool()){
					if(jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId()!=null && jobOrder.getDistrictMaster().getDistrictId()==804800){
						mailFlag=false;
					}else{
						mailFlag=true;
					}
				}else{
					if(jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId()!=null && jobOrder.getDistrictMaster().getDistrictId()==804800){
						mailFlag=true;
					}else{
						mailFlag=false;
					}
				}
				if(jobOrder.getDistrictMaster().getDistrictId()==4830640 || jobOrder.getDistrictMaster().getDistrictId()==4218990)
					mailFlag = false;
				
				if(mailFlag)
				{
					// Sending Email to Attach Schools 
					if(jobId==0)
					{
						System.out.println(" jobId is 0 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Email Code >>>>>>>>>>>>>>"+schoolJobOrderLst.size());
							List<SchoolMaster> schoolList = new ArrayList<SchoolMaster>();
							if(schoolJobOrderLst!=null && schoolJobOrderLst.size()>0)
							{
								for(SchoolInJobOrder sIj:schoolJobOrderLst) 
								{
									schoolList.add(sIj.getSchoolId());
								}
								
								List<UserMaster> schoolAdminList = userMasterDAO.getSchoolAdminList(schoolList);
								
								System.out.println(">>>> List of school Admin :>>>>>>>>>>>>>>>>>: "+schoolAdminList.size());
								
								String mailContent ="" ;
								for(UserMaster saObj:schoolAdminList)
								{
									System.out.println("  Send Email to School Admin-================>>>>>>>>>>>============email::"+saObj.getEmailAddress()+" schoolId "+saObj.getSchoolId().getSchoolName());
									DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
									dsmt.setEmailerService(emailerService);
									dsmt.setMailfrom("ashish.kumar@netsutra.com");
									//dsmt.setMailto("ashish.kumar@netsutra.com");
									dsmt.setMailto(saObj.getEmailAddress());
									dsmt.setMailsubject(saObj.getSchoolId().getSchoolName()+" "+Utility.getLocaleValuePropByKey("msgHasBeenAddedthe", locale)+" "+jobTitle+" "+Utility.getLocaleValuePropByKey("lblJobOdr", locale)+"");
									//mailContent = MailText.emailToSchoolAdmins(request,saObj,userMaster,districtMaster,jobTitle);
									mailContent = MailText.emailToSchoolAdminsForAddSchool(request,userMaster, districtMaster, jobTitle);
									dsmt.setMailcontent(mailContent);
									dsmt.start();
									
									System.out.println("mailContent ::-------> "+mailContent);
									
								}
							}
					}
					else
					{
						System.out.println(" jobId is Not 0 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Email Code >>>>>>>>>>>>>>");
					}
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			
			
			
			
			if(jobId==0){
				String subject="";
				if(userMaster.getEntityType()==1 && JobOrderType==2){
					subject=""+Utility.getLocaleValuePropByKey("msgDistrictJobOrderEmail", locale)+" "+userMaster.getFirstName()+" ( "+userMaster.getEmailAddress()+" )";	
				}else if(userMaster.getEntityType()==1 && JobOrderType==3){
					subject=""+Utility.getLocaleValuePropByKey("msgSchoolJobOrderEmail", locale)+" "+userMaster.getFirstName()+" ( "+userMaster.getEmailAddress()+" )";	
				}else if(userMaster.getEntityType()==2){
					subject=""+Utility.getLocaleValuePropByKey("msgDistrictJobOrderEmail", locale)+" "+userMaster.getFirstName()+" ( "+userMaster.getEmailAddress()+" )";	
				}else if(userMaster.getEntityType()==3){
					subject=""+Utility.getLocaleValuePropByKey("msgSchoolJobOrderEmail", locale)+" "+userMaster.getFirstName()+" ( "+userMaster.getEmailAddress()+" )";	
				}
				List<UserMaster> lstUserMaster= null;
				Properties  properties = new Properties();    		
				InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("smtpmail.properties");
				properties.load(inputStream);
				String sSupport=""+Utility.getLocaleValuePropByKey("msgJobInventoryCreation", locale)+""+jobOrder.getJobId()+". ";
				
				if(JobOrderType==2){
					if(schoolFlag==false && jsaFlag){
						mailSendByThread(JobOrderType,nameOfEntity,schoolsRecords,aUploadUrl,3,userMaster,properties.getProperty("smtphost.clientservices"),sSupport,request, jobOrder);
					}else if(schoolFlag && jsaFlag==false){
						Criterion criterionUser = Restrictions.eq("entityType",3);
						if(schoolIds.size()==0){
							SchoolMaster temP = new SchoolMaster();
							temP.setSchoolId(0L);
							schoolIds.add(temP);
						}
						Criterion criterionSchool = Restrictions.in("schoolId",schoolIds);
						lstUserMaster = userMasterDAO.findByCriteria(criterionUser,criterionSchool);
					}else if(schoolFlag && jsaFlag){
						Disjunction disjunction = Restrictions.disjunction();
						Criterion criterionUsers = Restrictions.eq("entityType",3);
						if(schoolIds.size()==0){
							SchoolMaster temP = new SchoolMaster();
							temP.setSchoolId(0L);
							schoolIds.add(temP);
						}
						Criterion criterionSchool = Restrictions.in("schoolId",schoolIds);
						lstUserMaster = userMasterDAO.findByCriteria(criterionUsers,criterionSchool);
						mailSendByThread(JobOrderType,nameOfEntity,schoolsRecords,aUploadUrl,3,userMaster,properties.getProperty("smtphost.clientservices"),sSupport,request, jobOrder);
					}
				}else{
					if(jsaFlag){
						mailSendByThread(JobOrderType,nameOfEntity,schoolsRecords,aUploadUrl,3,userMaster,properties.getProperty("smtphost.clientservices"),sSupport,request, jobOrder);
					}
				}
				if(!(schoolFlag==false && jsaFlag==false) && JobOrderType==2){
					if(schoolIds.size()>0 && schoolFlag){	
						for (UserMaster userMasterDetails : lstUserMaster){
							int noOfSchoolExpHires=0;
							if(userMasterDetails.getEntityType()==3){
								noOfSchoolExpHires=schoolInJobOrderDAO.getNoOfSchoolExpHiresBySchool(jobOrder,userMasterDetails.getSchoolId());
							}
							//mailSendByThread(1,noOfSchoolExpHires,userMaster,userMasterDetails.getEmailAddress(),subject,request, jobOrder, certificationType.replace(",",", "),userMasterDetails.getFirstName());
						}
					}
				}
			}else{
				if((jobAssessment==false && isJobAssessment==1)||(isJobAssessment==1 && attachJobAssessment==2 && !assessmentDocument.equals(""))||(attachDefaultDistrict==1 && attachJobAssessment!=1)||(attachDefaultSchool==1 && attachJobAssessment!=3)||(attachDefault==1 && attachJobAssessment!=2)){
					String subject="";
					if(userMaster.getEntityType()==1 && JobOrderType==2){
						subject=""+Utility.getLocaleValuePropByKey("msgDistrictJobOrderEmail", locale)+" "+userMaster.getFirstName()+" ( "+userMaster.getEmailAddress()+" )";	
					}else if(userMaster.getEntityType()==1 && JobOrderType==3){
						subject=""+Utility.getLocaleValuePropByKey("msgSchoolJobOrderEmail", locale)+" "+userMaster.getFirstName()+" ( "+userMaster.getEmailAddress()+" )";	
					}else if(userMaster.getEntityType()==2){
						subject=""+Utility.getLocaleValuePropByKey("msgDistrictJobOrderEmail", locale)+" "+userMaster.getFirstName()+" ( "+userMaster.getEmailAddress()+" )";	
					}else if(userMaster.getEntityType()==3){
						subject=""+Utility.getLocaleValuePropByKey("msgSchoolJobOrderEmail", locale)+" "+userMaster.getFirstName()+" ( "+userMaster.getEmailAddress()+" )";	
					}
					List<UserMaster> lstUserMaster= null;
					Properties  properties = new Properties();    		
					InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("smtpmail.properties");
					properties.load(inputStream);
					String sSupport=""+Utility.getLocaleValuePropByKey("msgJobInventoryCreation", locale)+""+jobOrder.getJobId()+". ";
					if(JobOrderType==2){
						if(schoolFlag==false && jsaFlag){
							mailSendByThread(JobOrderType,nameOfEntity,schoolsRecords,aUploadUrl,3,userMaster,properties.getProperty("smtphost.clientservices"),sSupport,request, jobOrder);
						}else if(schoolFlag && jsaFlag==false){
							Criterion criterionUser = Restrictions.eq("entityType",3);
							Criterion criterionSchool = Restrictions.in("schoolId",schoolIds);
							lstUserMaster = userMasterDAO.findByCriteria(criterionUser,criterionSchool);
						}else if(schoolFlag && jsaFlag){
							Disjunction disjunction = Restrictions.disjunction();
							Criterion criterionUsers = Restrictions.eq("entityType",3);
							Criterion criterionSchool = Restrictions.in("schoolId",schoolIds);
							lstUserMaster = userMasterDAO.findByCriteria(criterionUsers,criterionSchool);
							mailSendByThread(JobOrderType,nameOfEntity,schoolsRecords,aUploadUrl,3,userMaster,properties.getProperty("smtphost.clientservices"),sSupport,request, jobOrder);
						}
					}else{
						if(jsaFlag){
							mailSendByThread(JobOrderType,nameOfEntity,schoolsRecords,aUploadUrl,3,userMaster,properties.getProperty("smtphost.clientservices"),sSupport,request, jobOrder);
						}
					}
					if(!(schoolFlag==false && jsaFlag==false) && JobOrderType==2){
						if(schoolIds.size()>0 && schoolFlag){	
							for (UserMaster userMasterDetails : lstUserMaster){
								int noOfSchoolExpHires=0;
								if(userMasterDetails.getEntityType()==3){
									noOfSchoolExpHires=schoolInJobOrderDAO.getNoOfSchoolExpHiresBySchool(jobOrder,userMasterDetails.getSchoolId());
								}
							//	mailSendByThread(1,noOfSchoolExpHires,userMaster,userMasterDetails.getEmailAddress(),subject,request, jobOrder, certificationType.replace(",",", "),userMasterDetails.getFirstName());
							}
						}
					}
				}
			}

			//send mail
			if(jobId==0)
			{
				JobCategoryMaster jobCategoryMaster = jobOrder.getJobCategoryMaster();
				System.out.println("-----------------------------Start of Sending mail "+jobOrder.getJobId()+"-------------------------------");
				System.out.println("jobCategoryMaster:- "+jobCategoryMaster+", jobCategoryMaster.getApprovalBeforeGoLive():- "+jobCategoryMaster.getApprovalBeforeGoLive()+", jobCategoryMaster.getBuildApprovalGroup():- "+jobCategoryMaster.getBuildApprovalGroup());
				
				if(jobCategoryMaster.getApprovalByPredefinedGroups()!=null && jobCategoryMaster.getApprovalByPredefinedGroups() )	//Jeffco Specific Approval Flow
				{
					
					if(districtMaster.getDistrictId()!=804800)
				 	  jeffcoSpecialEdFlag = ( jeffcoSpecialEdFlag==null || jeffcoSpecialEdFlag.equals("") )? "N" : jeffcoSpecialEdFlag;
					else
					  jeffcoSpecialEdFlag="N"; 
					
					System.out.println("jeffcoSpecialEdFlag:- "+jeffcoSpecialEdFlag);
					
					DistrictSpecificApprovalFlow districtSpecificApprovalFlow = districtSpecificApprovalFlowDAO.findApprovalFlowByFlag(jeffcoSpecialEdFlag,jobOrder.getDistrictMaster());
					System.out.println("districtSpecificApprovalFlow:- "+districtSpecificApprovalFlow.getApprovalFlow());
					
					Map<String, String> groupShortNameWithGroupName = districtWiseApprovalGroupDAO.mapGroupShortNameWithGroupName(districtSpecificApprovalFlow.getApprovalFlow(),jobOrder.getDistrictMaster());
					System.out.println("groupShortNameWithGroupName:- "+groupShortNameWithGroupName.size());
					
					List<DistrictApprovalGroups> districtApprovalGroups = districtSpecificApprovalFlowDAO.findDistrictApprovalGroupByApprovalFlow(districtSpecificApprovalFlow.getApprovalFlow(), jobOrder.getDistrictMaster());
					System.out.println("districtApprovalGroups:- "+districtApprovalGroups.size());
					//Map<String, DistrictApprovalGroups> districtApprovalGroups = districtSpecificApprovalFlowDAO.findDistrictApprovalGroupByApprovalFlow(districtSpecificApprovalFlow.getApprovalFlow(), jobOrder.getDistrictMaster());
					
					
					for(DistrictApprovalGroups dag : districtApprovalGroups)	//iterate each Group and send the mail to first group
					{
						List<Integer> groupMemberIds = new ArrayList<Integer>();
						for(String groupMember : dag.getGroupMembers().split("#"))
							if(!groupMember.equals(""))
								try{groupMemberIds.add(Integer.parseInt(groupMember));}catch(Exception e){}
						
						System.out.println("groupMemberIds:- "+groupMemberIds);
						List<UserMaster> userMasters = new ArrayList<UserMaster>();
						if(groupMemberIds.size()>0)
						{
							List<DistrictKeyContact> approvalContacts = districtKeyContactDAO.findByCriteria(Restrictions.in("keyContactId", groupMemberIds));
							
							System.out.println("approvalContacts:- "+approvalContacts);
							for(DistrictKeyContact districtKeyContact : approvalContacts)
								userMasters.add(districtKeyContact.getUserMaster());
						}
						
						String groupFullName = (groupShortNameWithGroupName.get("SA")==null)? "" : groupShortNameWithGroupName.get("SA");
						if(dag.getGroupName().equals(groupFullName))
						{
							//Get the total Number of SA's
							List<SchoolInJobOrder> schoolInJobOrder = schoolInJobOrderDAO.findByCriteria(Restrictions.eq("jobId",jobOrder));
							
							List<SchoolMaster> schoolMasters = new ArrayList<SchoolMaster>();
							for(SchoolInJobOrder order : schoolInJobOrder)
								schoolMasters.add(order.getSchoolId());
							
							List<UserMaster> userMasters2 = null;
							if(schoolInJobOrder!=null && schoolInJobOrder.size()>0)
							{
								Criterion criterion1 = Restrictions.eq("entityType", new Integer(3));
								Criterion criterion2 = Restrictions.in("schoolId", schoolMasters);
								Criterion criterion3 = Restrictions.eq("districtId", districtMaster);
								Criterion criterion4 = Restrictions.eq("status","A");
								Criterion criterion5 = Restrictions.eq("roleId", roleMasterDAO.findById(3,false,false));
								
								userMasters2 = userMasterDAO.findByCriteria(criterion1,criterion2,criterion3,criterion4,criterion5);
								for(UserMaster master : userMasters2)
									userMasters.add(master);
							}
						}
						
						groupFullName = (groupShortNameWithGroupName.get("ES")==null)? "" : groupShortNameWithGroupName.get("ES");
						if(dag.getGroupName().equals(groupFullName))
						{
							//Logic For ES Shadab 322
							EmploymentServicesTechnician primaryServicesTechnician = jobOrder.getEmploymentServicesTechnician();
							
							List<String> emailAddress = new ArrayList<String>();
							emailAddress.add(primaryServicesTechnician.getPrimaryEmailAddress());
							emailAddress.add(primaryServicesTechnician.getBackupEmailAddress());
							
							System.out.println("primaryTechnicianEmailAddress:- "+primaryServicesTechnician.getPrimaryEmailAddress());
							System.out.println("backupTechnicianEmailAddress:- "+primaryServicesTechnician.getBackupEmailAddress());
							
							Criterion criterion = Restrictions.in("keyContactEmailAddress", emailAddress);
							List<DistrictKeyContact> districtKeyContacts = districtKeyContactDAO.findByCriteria(criterion);
							
							for(DistrictKeyContact contact : districtKeyContacts)
									userMasters.add(contact.getUserMaster());
						}
						
						String baseURL=Utility.getBaseURL(request);             
						String content="";
						
						if(userMasters!=null && userMasters.size()>0)
						{
							for (UserMaster master : userMasters)
							{
								DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
								dsmt.setEmailerService(emailerService);
								dsmt.setMailfrom("admin@netsutra.com");
								dsmt.setMailto(master.getEmailAddress());
								dsmt.setMailsubject("Approval Mail");
								String incJobId = Utility.encryptNo(returnJobId);
								String incUserId = Utility.encryptNo(master.getUserId());
								String groupId = Utility.encryptNo(dag.getDistrictApprovalGroupsId());
								
								String linkIds= incJobId+"###"+incUserId+"###"+groupId;
								String forMated = "";
								try
								{
									forMated = Utility.encodeInBase64(linkIds);
									forMated = baseURL+"approveJob.do?id="+forMated;
									
									System.out.println("forMated   "+forMated);
									System.out.println("returnjobTitle   "+returnjobTitle);
									content=MailText.getApproveJObMailTextJeffcoSpecific(request,master, forMated,returnjobTitle);
									dsmt.setMailcontent(content);
									dsmt.start();
								}
								catch (Exception e)
								{
									e.printStackTrace();
								}
							}
							break;
						}
					}
					
				try
				{
					jobOrder.setApprovalBeforeGoLive(2);
					jobOrderDAO.makePersistent(jobOrder);
				}
				catch(Exception e){e.printStackTrace();}
			}
			else
			if(jobCategoryMaster!=null && jobCategoryMaster.getApprovalBeforeGoLive()!=null && jobCategoryMaster.getBuildApprovalGroup()!=null && jobCategoryMaster.getApprovalBeforeGoLive() && jobCategoryMaster.getBuildApprovalGroup()&& districtMaster.getDistrictId()!=806900)
			{
				System.out.println("inside 1");
				List<DistrictApprovalGroups> districtApprovalGroupList = districtApprovalGroupsDAO.findDistrictApprovalGroupsByJobCategory(districtMaster,jobCategoryMaster);
				
				System.out.println("districtApprovalGroupList.size():- "+(districtApprovalGroupList.size()));
				if(districtApprovalGroupList!=null && districtApprovalGroupList.size()>0)
				{
					for(DistrictApprovalGroups districtApprovalGroups : districtApprovalGroupList)	//iterate each Group and send the mail to first group
					{
						List<Integer> groupMemberIds = new ArrayList<Integer>();
						for(String groupMember : districtApprovalGroups.getGroupMembers().split("#"))
							if(!groupMember.equals(""))
								groupMemberIds.add(Integer.parseInt(groupMember));
						
						System.out.println("groupMemberIds:- "+groupMemberIds);
						if(groupMemberIds.size()>0)
						{
							List<DistrictKeyContact> approvalContacts = districtKeyContactDAO.findByCriteria(Restrictions.in("keyContactId", groupMemberIds));
							String baseURL=Utility.getBaseURL(request);             
							String content="";
							
							System.out.println("approvalContacts:- "+approvalContacts);
							if(approvalContacts!=null && approvalContacts.size()>0)
							{
								for (DistrictKeyContact districtKeyContact : approvalContacts)
								{
									DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
									dsmt.setEmailerService(emailerService);
									dsmt.setMailfrom("admin@netsutra.com");
									dsmt.setMailto(districtKeyContact.getKeyContactEmailAddress());
									dsmt.setMailsubject("Approval Mail");
									System.out.println("returnJobId   "+returnJobId);
									System.out.println("districtKeyContact.getUserMaster().getUserId()   "+districtKeyContact.getUserMaster().getUserId());
									String incJobId = Utility.encryptNo(returnJobId);
									String incUserId = Utility.encryptNo(districtKeyContact.getUserMaster().getUserId());
									String groupId = Utility.encryptNo(districtApprovalGroups.getDistrictApprovalGroupsId());
									
									String linkIds= incJobId+"###"+incUserId+"###"+groupId;
									String forMated = "";
									try
									{
										forMated = Utility.encodeInBase64(linkIds);
										forMated = baseURL+"approveJob.do?id="+forMated;
										
										System.out.println("forMated   "+forMated);
										System.out.println("returnjobTitle   "+returnjobTitle);
										content=MailText.getApproveJObMailText(request,districtKeyContact, forMated,returnjobTitle);
										dsmt.setMailcontent(content);
										dsmt.start();
									}
									catch (Exception e)
									{
										e.printStackTrace();
									}
								}
							//	mailSendByThread(1,noOfSchoolExpHires,userMaster,userMasterDetails.getEmailAddress(),subject,request, jobOrder, certificationType.replace(",",", "),userMasterDetails.getFirstName());
							}
							try
							{
								jobOrder.setApprovalBeforeGoLive(2);
								jobOrderDAO.makePersistent(jobOrder);
							}
							catch(Exception e){e.printStackTrace();}
							break;
						}
					}
				}
			}//End of send mail functionality
				
				//hafeez
			else if(districtMaster!=null && districtMaster.getSkipApprovalProcess()!=null && districtMaster.getSkipApprovalProcess())
			{
				int status=jobApprovalProcessForDistrictSpecific(request, jobApprovalProcessId, districtMaster, userMaster,jobOrder);
				try
				{
					if(status==1){
					jobOrder.setApprovalBeforeGoLive(2);
					} else if(status==2){
						jobOrder.setApprovalBeforeGoLive(1);
					}
					jobOrderDAO.makePersistent(jobOrder);
				}
				catch(Exception e){e.printStackTrace();}
			}else if(districtMaster!=null && districtMaster.getApprovalBeforeGoLive()!=null && districtMaster.getApprovalBeforeGoLive())
			{
			String baseURL=Utility.getBaseURL(request);             
			String content="";
			List<DistrictKeyContact> approvalContacts = districtKeyContactDAO.findByContactType(districtMaster, "Job Approval");           
					if(approvalContacts!=null && approvalContacts.size()>0)
					for (DistrictKeyContact districtKeyContact : approvalContacts) {
						DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
						dsmt.setEmailerService(emailerService);
						dsmt.setMailfrom("admin@netsutra.com");
						dsmt.setMailto(districtKeyContact.getKeyContactEmailAddress());
						dsmt.setMailsubject("Approval Mail");
						System.out.println("returnJobId   "+returnJobId);
						System.out.println("districtKeyContact.getUserMaster().getUserId()   "+districtKeyContact.getUserMaster().getUserId());
						String incJobId = Utility.encryptNo(returnJobId);
						String incUserId = Utility.encryptNo(districtKeyContact.getUserMaster().getUserId());
						
						String linkIds= incJobId+"###"+incUserId;
						String forMated = "";
						try {
							forMated = Utility.encodeInBase64(linkIds);
							forMated = baseURL+"approveJob.do?id="+forMated;
						} catch (Exception e) {
							e.printStackTrace();
						}	
						System.out.println("forMated   "+forMated);
						System.out.println("returnjobTitle   "+returnjobTitle);
						content=MailText.getApproveJObMailText(request,districtKeyContact, forMated,returnjobTitle);					
						dsmt.setMailcontent(content);
						try {
							dsmt.start();	
						} catch (Exception e) {}
					}
			}

			if(returnJobId!=0){
//				JobPostingURL=Utility.getShortURL(Utility.getBaseURL(request)+"applyteacherjob.do?jobId="+returnJobId);
				JobPostingURL=Utility.getValueOfPropByKey("basePath")+"applyteacherjob.do?jobId="+returnJobId;
				
				if(false){
					try{
						if(jobOrder!=null && jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId()==5509600){
							TMCommonUtil tmCommonUtil = new TMCommonUtil();
							tmCommonUtil.sendJobDetailJSON(jobOrder,userMaster);
						}else{
							System.out.println(" JobOrder Is NULL ");
						}
						
					}catch(Exception e){
						e.printStackTrace();
					}
				}
				
				
			}
			
			///post On Social Network
			System.out.println("Post facebook wall");
			if(Utility.getIndiaFormatDate(jobOrder.getJobStartDate()).equals(Utility.getIndiaFormatDate(new Date()))){
				postOnSocialNetwork(socialService,JobOrderType,districtMaster,schoolMaster,Utility.getBaseURL(request),jobOrder,JobPostingURL);
			}

			try{
				if(jobOrder!=null)
				jobOrderDAO.totalNoOfHires(jobOrder);
			}catch (Exception e) {
				e.printStackTrace();
			}  
   				
		}
		}catch(Exception e) 
		{
			e.printStackTrace();
			return null;
		}
		
		//String statusIds,String secStatusIds
		
			try
			{
				JobOrder jobOrderPersist = new JobOrder();
				if(jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getHeadQuarterMaster() != null && jobOrder.getDistrictMaster().getHeadQuarterMaster().getHeadQuarterId() == DashboardAjax.NC_HEADQUARTER && jobId==0){
					Criterion criterion=Restrictions.eq("jobOrder.jobId", jobIdNC);
					List<JobRequisitionNumbers> requisitionNumbers=jobRequisitionNumbersDAO.findByCriteria(criterion);
					if(requisitionNumbers != null && requisitionNumbers.size()>0){      JobRequisitionNumbers jobrequisitionNumbers=new JobRequisitionNumbers();
				      BeanUtils.copyProperties(requisitionNumbers.get(0), jobrequisitionNumbers, new String[]{"jobRequisitionId"});
				      jobrequisitionNumbers.setJobOrder(jobOrder);
				      jobRequisitionNumbersDAO.makePersistent(jobrequisitionNumbers);}
					
					jobOrderPersist = jobOrderDAO.findById(jobIdNC, false, false);
					if(jobOrderPersist != null){
						jobOrderPersist.setStatus("R");
						jobOrderDAO.makePersistent(jobOrderPersist);
												
					}
					
					if(beforeCloneSchool !=null && beforeCloneSchool.size()>0)
					{ SchoolInJobOrder schoolInJobOrder=new SchoolInJobOrder();
				      BeanUtils.copyProperties(beforeCloneSchool.get(0), schoolInJobOrder, new String[]{"jobOrderSchoolId"});
				      schoolInJobOrder.setJobId(jobOrder);
				      schoolInJobOrderDAO.makePersistent(schoolInJobOrder);}					

				}
		
//end		
			//shadab start
			ElasticSearchService es=new ElasticSearchService();
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
					try
					{
						List<SchoolMaster> list=jobOrder.getSchool();
								
						//json string
						
						//	System.out.println("***********************Printing Json***************************************************");
						JSONObject obj=new JSONObject();
						//for deleting
						es.searchDocumentByJobid(jobOrder.getJobId());
						obj.put("jobid", jobOrder.getJobId());
						obj.put("serverName", ElasticSearchConfig.serverName);
						if(jobOrder.getDistrictMaster().getLatitude()!=null)
							obj.put("latitude", jobOrder.getDistrictMaster().getLatitude());
						else
							obj.put("latitude", "");
						if(jobOrder.getDistrictMaster().getLongitude()!=null)
							obj.put("longitude", jobOrder.getDistrictMaster().getLongitude());
						else
							obj.put("longitude", "");
						if(jobOrder.getDistrictMaster().getStateId()!=null 
								&& jobOrder.getDistrictMaster().getStateId().getCountryMaster()!=null
								&& jobOrder.getDistrictMaster().getStateId().getCountryMaster().getName()!=null)
							obj.put("countryName", jobOrder.getDistrictMaster().getStateId().getCountryMaster().getName());
						else
							obj.put("countryName", "");
						obj.put("districtid", jobOrder.getDistrictMaster().getDistrictId());
						obj.put("jobDescription", jobOrder.getJobDescription());
						obj.put("jobtitle", jobOrder.getJobTitle());
						obj.put("jobstartdate", sdf.format(jobOrder.getJobStartDate())+"T00:00:00.000+05:30");
						obj.put("jobenddate", sdf.format(jobOrder.getJobEndDate())+"T00:00:00.000+05:30");
						if(jobOrder.getSubjectMaster()!=null)
						{
							
							obj.put("subjectName", jobOrder.getSubjectMaster().getSubjectName());
							obj.put("subjectId", jobOrder.getSubjectMaster().getMasterSubjectId());
						}
						else
						{
							
							obj.put("subjectName", "null");
							obj.put("subjectId", "0");
						}
						if(jobOrder.getGeoZoneMaster()!=null)
						{
							obj.put("geoZoneId", jobOrder.getGeoZoneMaster().getGeoZoneId());
							obj.put("geoZoneName", jobOrder.getGeoZoneMaster().getGeoZoneName());
							
						}
						else
						{
							obj.put("geoZoneId", 0);
							obj.put("geoZoneName", "");
						}
						
						
						obj.put("districtName", jobOrder.getDistrictMaster().getDistrictName());
						obj.put("cityname", jobOrder.getDistrictMaster().getCityName());
						obj.put("zipCode", jobOrder.getDistrictMaster().getZipCode());
						obj.put("distaddress", jobOrder.getDistrictMaster().getAddress());
						obj.put("stateName", jobOrder.getDistrictMaster().getStateId().getStateName());
						
						//if joborder is creating the createddatetime will be null
						String createdDateTime=null;
						if(jobOrder.getCreatedDateTime()==null)
						{
							createdDateTime=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date());
						}
						else
						{
							createdDateTime=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(jobOrder.getCreatedDateTime());
						}
						obj.put("createdDateTime", createdDateTime.split(" ")[0]+"T"+createdDateTime.split(" ")[1].subSequence(0, 8)+".000+05:30");
						List<JobCertification> lstJobCertification= null;
						Criterion criterion1 = Restrictions.eq("jobId",jobOrder);
						lstJobCertification = jobCertificationDAO.findByCriteria(criterion1);
						//System.out.println("job end date====="+jobOrder.getJobEndDate());
						//System.out.println("status	"+jobOrder.getStatus());
						//System.out.println("isinvitedonly	"+jobOrder.getIsInviteOnly());
						//System.out.println("approvalbeforelgolive	"+jobOrder.getApprovalBeforeGoLive());
						//System.out.println("jobstartdate	"+jobOrder.getJobStartDate());
						//System.out.println("jobenddate	"+jobOrder.getJobEndDate());
						//System.out.println(list);
						System.out.println(jobOrder.getStatus().equalsIgnoreCase("A") && (jobOrder.getIsInviteOnly()==null ||  !jobOrder.getIsInviteOnly()) && jobOrder.getApprovalBeforeGoLive()!=null && jobOrder.getApprovalBeforeGoLive()==1 
								&& (jobOrder.getJobStartDate().before(new Date()) ||  jobOrder.getJobStartDate().equals(new Date())) 
								&& (jobOrder.getJobEndDate().after(sdf.parse(sdf.format(new Date()))) ||  jobOrder.getJobEndDate().equals(sdf.parse(sdf.format(new Date())))));
						if(jobOrder.getStatus().equalsIgnoreCase("A") && (jobOrder.getIsInviteOnly()==null ||  !jobOrder.getIsInviteOnly()) && jobOrder.getApprovalBeforeGoLive()!=null && jobOrder.getApprovalBeforeGoLive()==1 
								&& (jobOrder.getJobStartDate().before(new Date()) ||  jobOrder.getJobStartDate().equals(new Date())) 
								&& (jobOrder.getJobEndDate().after(sdf.parse(sdf.format(new Date()))) ||  jobOrder.getJobEndDate().equals(sdf.parse(sdf.format(new Date())))) 
								)
						{
							JSONArray schoolName = new JSONArray();
							JSONArray zip = new JSONArray();
							JSONArray schoolAddress = new JSONArray();
							JSONArray certType = new JSONArray();
							for(SchoolMaster school:list)
							{
								schoolName.add(school.getSchoolName());
								zip.add(school.getZip());
								schoolAddress.add(school.getAddress());
							}
							System.out.println("job end date====="+jobOrder.getJobEndDate());
							for(JobCertification certification:lstJobCertification)
							{
								
								obj.put("certType", certification.getCertificateTypeMaster().getCertType());
								certType.add(certification.getCertificateTypeMaster().getCertType()+" ["+
										certification.getCertificateTypeMaster().getStateId().getStateName()+"]");
								
							}
							System.out.println("job end date====="+jobOrder.getJobEndDate());
							obj.put("schoolName", schoolName);
							obj.put("zip", zip);
							obj.put("schoolAddress", schoolAddress);
							obj.put("certType", certType);
							//System.out.println("json String to insert==");
							//System.out.println(obj.toString());
							es.update("document","jdbc",String.valueOf(jobOrder.getJobId()),obj.toString());
							
						}
						//for updation in districtJobboard	
						
						if(jobOrder.getStatus().equalsIgnoreCase("A") && (jobOrder.getIsInviteOnly()==null ||  !jobOrder.getIsInviteOnly()) && jobOrder.getApprovalBeforeGoLive()!=null && jobOrder.getApprovalBeforeGoLive()==1 
								&& (jobOrder.getJobStartDate().before(new Date()) ||  jobOrder.getJobStartDate().equals(new Date())) 
								&& (jobOrder.getJobEndDate().after(sdf.parse(sdf.format(new Date()))) ||  jobOrder.getJobEndDate().equals(sdf.parse(sdf.format(new Date())))
										&& (jobOrder.getHiddenJob()==null ||  !jobOrder.getHiddenJob())	) 
								)
						{
							es.updateESDistrictJobBoardByJobId(jobOrder);
						}	
					}
					catch(Exception e)
					{
						e.printStackTrace();
					}
					
					try
					{
						if(jobOrder.getCreatedForEntity()==2)
						{
							//es.updateESDistrictJobOrderByJobId(jobOrder);
						}
					}
					catch(Exception e)
					{
						e.printStackTrace();
					}
					try
					{
						//for jobsofinterest not for candidate
						if(jobOrder.getStatus().equalsIgnoreCase("A") && (jobOrder.getJobStartDate().before(new Date()) ||  jobOrder.getJobStartDate().equals(new Date())) 
								&& (jobOrder.getJobEndDate().after(sdf.parse(sdf.format(new Date()))) ||  jobOrder.getJobEndDate().equals(sdf.parse(sdf.format(new Date())))) 
								)
						{
							es.updateESForJobsOfInterestNotCandidate(jobOrder);
							//for jobsofinterest for candidate
							//es.updateESForJobsOfInterestForCandidate(jobOrder);
						}
						
					}
					catch(Exception e)
					{
						e.printStackTrace();
					}
					try
					{
						if(jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getHeadQuarterMaster() != null && jobOrder.getDistrictMaster().getHeadQuarterMaster().getHeadQuarterId() == DashboardAjax.NC_HEADQUARTER && jobId==0){
							es.updateESSchoolJobOrderByJobId(jobOrderPersist);
						}
						
						es.updateESSchoolJobOrderByJobId(jobOrder);
					}
					catch(Exception e)
					{
						e.printStackTrace();
					}
					 try{
							if(jobOrder!=null)
							jobOrderDAO.totalNoOfHires(jobOrder);
						}catch (Exception e) {
							e.printStackTrace();
						}
				//	System.out.println("*****************************************enddddddd******************************************************");
					//shadab end
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		try
		{SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
			if(jobOrder.getStatus().equalsIgnoreCase("A") &&  (jobOrder.getJobStartDate().before(new Date()) ||  jobOrder.getJobStartDate().equals(new Date())) 
				&& (jobOrder.getJobEndDate().after(sdf.parse(sdf.format(new Date()))) ||  jobOrder.getJobEndDate().equals(sdf.parse(sdf.format(new Date())))) 
				)
			{
				updateJobForTeacherStatus(jobOrder.getJobId());
			}
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
	
		
		try {
			ArrayList<JobWisePanelStatus> listJobWise = new ArrayList<JobWisePanelStatus>();
			//listJobWise = saveJobWisePanelStatus(jobOrder,statusIds,secStatusIds);
			listJobWise = saveJobWisePanelStatusOp(jobOrder,statusIds,secStatusIds); // for Optimization Anurag
			if(isNew) {jobId=jobOrder.getJobId(); jobOrderForLog = jobOrder;}
			createLog.logForJobOrder(request, jobOrderForLog, isNew , jobId, userMaster , listJobWise);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return JobPostingURL+"||"+jobOrder.getJobId();	
	}

	public void postOnSocialNetwork(SocialService socialService,int JobOrderType,DistrictMaster districtMaster,SchoolMaster schoolMaster,String baseUrl,JobOrder jobOrder,String JobPostingURL){
		System.out.println("post on wall");
		UserMaster userMasterSS= new UserMaster();
		String messageToPost = baseUrl;
		if(!messageToPost.equals(""))
			messageToPost = JobPostingURL;

		userMasterSS.setDistrictId(districtMaster);

		try{
			if(JobOrderType==2){
				if(districtMaster.getPostingOnTMWall()==1){
					userMasterSS.setEntityType(1);
					socialService.postOnWall(messageToPost, userMasterSS,jobOrder,baseUrl);
					socialService.tweet(messageToPost, userMasterSS,jobOrder,baseUrl);
					socialService.shareOnLinkedIn(messageToPost, userMasterSS,jobOrder,baseUrl);
				}
				if(districtMaster.getPostingOnDistrictWall()==1){
					userMasterSS.setEntityType(2);
					socialService.postOnWall(messageToPost, userMasterSS,jobOrder,baseUrl);
					socialService.tweet(messageToPost, userMasterSS,jobOrder,baseUrl);
					socialService.shareOnLinkedIn(messageToPost, userMasterSS,jobOrder,baseUrl);
				}
			}else{
				userMasterSS.setSchoolId(schoolMaster);
				if(schoolMaster.getPostingOnTMWall()==1){
					userMasterSS.setEntityType(1);
					socialService.postOnWall(messageToPost, userMasterSS,jobOrder,baseUrl);
					socialService.tweet(messageToPost, userMasterSS,jobOrder,baseUrl);
					socialService.shareOnLinkedIn(messageToPost, userMasterSS,jobOrder,baseUrl);
				}
				if(schoolMaster.getPostingOnDistrictWall()==1){
					userMasterSS.setEntityType(2);
					socialService.postOnWall(messageToPost, userMasterSS,jobOrder,baseUrl);
					socialService.tweet(messageToPost, userMasterSS,jobOrder,baseUrl);
					socialService.shareOnLinkedIn(messageToPost, userMasterSS,jobOrder,baseUrl);
				}
				if(schoolMaster.getPostingOnSchoolWall()==1){
					userMasterSS.setEntityType(3);
					socialService.postOnWall(messageToPost, userMasterSS,jobOrder,baseUrl);
					socialService.tweet(messageToPost, userMasterSS,jobOrder,baseUrl);
					socialService.shareOnLinkedIn(messageToPost, userMasterSS,jobOrder,baseUrl);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public void mailSendByThread(int mailType,int noOfSchoolExpHires,UserMaster userSession,String to,String subject,HttpServletRequest request,JobOrder jobOrder,String jcRecords, String text){
		MailSend mst =new MailSend(mailType,noOfSchoolExpHires,userSession,to,subject,request,jobOrder,jcRecords,text);
		Thread currentThread = new Thread(mst);
		currentThread.start(); 
	}
	public void mailSendByThread(int jobOrderType,String nameOfEntity,String Schools,String aUploadUrl,int mailType,UserMaster userSession,String to,String subject,HttpServletRequest request,JobOrder jobOrder){
		MailSend mst =new MailSend(jobOrderType,nameOfEntity,Schools,aUploadUrl,mailType,userSession,to,subject,request,jobOrder);
		Thread currentThread = new Thread(mst);
		currentThread.start(); 
	}
	public class MailSend  extends Thread{
		int mailTypeTrdVal=0;
		int noOfSchoolExpHiresTrdVal=0;
		UserMaster userSessionTrdVal=null;
		String toTrdVal="";
		String subjectTrdVal="";
		String baseURL=null;
		JobOrder jobOrderTrdVal=null;
		String jcRecordsTrdVal="";
		String nameTrdVal="";
		BatchJobOrder batchJobOrderTrdVal=null;
		int jobOrderTypeTrdVal=0;
		String nameOfEntityTrdVal=null;
		String SchoolsTrdVal=null;
		String aUploadUrlTrdVal=null;
		public MailSend(int mailType,int noOfSchoolExpHires,UserMaster userSession, String to,String subject,HttpServletRequest request,JobOrder jobOrder,String jcRecords, String name){
			mailTypeTrdVal=mailType;
			noOfSchoolExpHiresTrdVal=noOfSchoolExpHires;
			userSessionTrdVal=userSession;
			toTrdVal=to;
			subjectTrdVal=subject;
			baseURL=Utility.getBaseURL(request);
			jobOrderTrdVal=jobOrder;
			jcRecordsTrdVal=jcRecords;
			nameTrdVal=name;
		}
		public MailSend(int jobOrderType,String nameOfEntity,String Schools,String aUploadUrl,int mailType,UserMaster userSession, String to,String subject,HttpServletRequest request,JobOrder jobOrder){
			mailTypeTrdVal=mailType;
			userSessionTrdVal=userSession;
			toTrdVal=to;
			subjectTrdVal=subject;
			baseURL=Utility.getBaseURL(request);
			jobOrderTrdVal=jobOrder;
			jobOrderTypeTrdVal=jobOrderType;
			nameOfEntityTrdVal=nameOfEntity;
			SchoolsTrdVal=Schools;
			aUploadUrlTrdVal=aUploadUrl;
		}
		public MailSend(int mailType,UserMaster userSession, String to,String subject,HttpServletRequest request,BatchJobOrder batchJobOrder,String jcRecords, String name){
			mailTypeTrdVal=mailType;
			userSessionTrdVal=userSession;
			toTrdVal=to;
			subjectTrdVal=subject;
			baseURL=Utility.getBaseURL(request);
			batchJobOrderTrdVal=batchJobOrder;
			jcRecordsTrdVal=jcRecords;
			nameTrdVal=name;
		}
		public void run() {
			try {
				
				System.out.println("toTrdVal:::::::::::::::"+toTrdVal);
				Thread.sleep(1000);
				if(mailTypeTrdVal==1){
					emailerService.sendMailAsHTMLText(toTrdVal,subjectTrdVal,MailText.createJobOrderForDistrict(noOfSchoolExpHiresTrdVal,userSessionTrdVal,baseURL, jobOrderTrdVal, jcRecordsTrdVal,nameTrdVal));
				}else if(mailTypeTrdVal==3){
					//emailerService.sendMailAsHTMLText(toTrdVal,subjectTrdVal,MailText.jobOrderSupport(jobOrderTypeTrdVal,nameOfEntityTrdVal,SchoolsTrdVal,aUploadUrlTrdVal,userSessionTrdVal,baseURL, jobOrderTrdVal));
				}
			}catch (NullPointerException en) {
				en.printStackTrace();
			}catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	}
	public int copyToClipBoard(String strVal)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		try
		{
			if (session == null || session.getAttribute("userMaster") == null)
			{
				return 0;
			}
			else
			{
				Toolkit toolkit = Toolkit.getDefaultToolkit();
				Clipboard clipboard = toolkit.getSystemClipboard();
				StringSelection strSel = new StringSelection(strVal);
				clipboard.setContents(strSel, null);
			}
		}catch (Exception e) 
		{
			e.printStackTrace();
		}

		return 1;
	}
	public String showFile(String districtORSchoool,int districtORSchooolId,String docFileName)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		String path="";
		String source="";
		String target="";
		try 
		{
			if(districtORSchoool.equalsIgnoreCase("district")){
				source = Utility.getValueOfPropByKey("districtRootPath")+districtORSchooolId+"/"+docFileName;
				target = context.getServletContext().getRealPath("/")+"/"+"/district/"+districtORSchooolId+"/";
			}else{
				source = Utility.getValueOfPropByKey("schoolRootPath")+districtORSchooolId+"/"+docFileName;
				target = context.getServletContext().getRealPath("/")+"/"+"/school/"+districtORSchooolId+"/";
			}
			File sourceFile = new File(source);
			File targetDir = new File(target);
			if(!targetDir.exists())
				targetDir.mkdirs();

			File targetFile = new File(targetDir+"/"+sourceFile.getName());

			FileUtils.copyFile(sourceFile, targetFile);

			if(districtORSchoool.equalsIgnoreCase("district")){
				path = Utility.getValueOfPropByKey("contextBasePath")+"/district/"+districtORSchooolId+"/"+docFileName; 	
			}else{
				path = Utility.getValueOfPropByKey("contextBasePath")+"/school/"+districtORSchooolId+"/"+docFileName;
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return path;
	}

	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public int checkDuplicateSchool(int jobId,int schoolId) 
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		List<SchoolInJobOrder> lstSchoolInJobOrder= null;
		try{
			JobOrder  jobOrder = jobOrderDAO.findById(jobId, false, false);
			Criterion criterion1 = Restrictions.eq("jobId",jobOrder);
			SchoolMaster  schoolMaster = schoolMasterDAO.findById(Long.parseLong(schoolId+""), false, false);
			Criterion criterion2 = Restrictions.eq("schoolId",schoolMaster);
			lstSchoolInJobOrder = schoolInJobOrderDAO.findByCriteria(criterion1,criterion2);	
		}catch(Exception e){
			e.printStackTrace();
			return 0;
		}
		return lstSchoolInJobOrder.size();

	}

	public int addSchoolInJobOrder(int jobId,int schoolId,int noOfSchoolExpHires){
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		int existFlag=0;
		try{
			existFlag=checkDuplicateSchool(jobId,schoolId);
			if(existFlag==0){
				SchoolInJobOrder schoolInJobOrder= new SchoolInJobOrder();
				JobOrder  jobOrder = jobOrderDAO.findById(jobId, false, false);
				schoolInJobOrder.setJobId(jobOrder);
				SchoolMaster  schoolMaster = schoolMasterDAO.findById(Long.parseLong(schoolId+""), false, false);
				schoolInJobOrder.setSchoolId(schoolMaster);
				schoolInJobOrder.setNoOfSchoolExpHires(noOfSchoolExpHires);
				schoolInJobOrder.setCreatedDateTime(new Date());
				schoolInJobOrderDAO.makePersistent(schoolInJobOrder);
			}

		}catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
		return existFlag;
	}
	/* @Author: Sudhandu
	 * @Discription: It is used to delete a particular School from database.
	 */
	//@Transactional(readOnly=false)
	public boolean deleteSchoolInJobOrder(int jobId,int schoolId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster = null;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}else{
			userMaster=(UserMaster)session.getAttribute("userMaster");
		}
		try{
			List<SchoolInJobOrder> lstschoolInJobOrder=null; 
			JobOrder  jobOrder = jobOrderDAO.findById(jobId, false, false);
			Criterion criterion1 = Restrictions.eq("jobId",jobOrder);
			lstschoolInJobOrder =schoolInJobOrderDAO.findByCriteria(criterion1);
			SchoolMaster schoolMaster = schoolMasterDAO.findById(Long.parseLong(""+schoolId), false, false);
			
			Criterion criterion2 = Restrictions.eq("schoolMaster",schoolMaster);
			Criterion criterion3 = Restrictions.eq("jobOrder",jobOrder);
			Criterion criterion4 = Restrictions.eq("status",1);
			Criterion criterion5 = Restrictions.and(criterion2, criterion3);
			Criterion criterion6 = Restrictions.and(criterion4, criterion5);
			
			List<JobRequisitionNumbers> lstcheckJRQNisAnyHired = new ArrayList<JobRequisitionNumbers>();
			lstcheckJRQNisAnyHired = jobRequisitionNumbersDAO.findByCriteria(criterion5,criterion6);
			
			if(lstcheckJRQNisAnyHired.size()>0)
				return false;
			
			List<JobRequisitionNumbers> lstJobRequisitionNumbers = new ArrayList<JobRequisitionNumbers>();
			lstJobRequisitionNumbers = jobRequisitionNumbersDAO.findByCriteria(criterion2,criterion3);
			
			List<DistrictRequisitionNumbers> lstDistrictRequisitionNumbers = new ArrayList<DistrictRequisitionNumbers>();
			
			for(JobRequisitionNumbers jbrn : lstJobRequisitionNumbers)
			{
					lstDistrictRequisitionNumbers.add(jbrn.getDistrictRequisitionNumbers());
					jobRequisitionNumbersDAO.makeTransient(jbrn);
			}
			for(DistrictRequisitionNumbers drqn: lstDistrictRequisitionNumbers)
			{
				drqn.setIsUsed(false);
				districtRequisitionNumbersDAO.makePersistent(drqn);
			}
			for(SchoolInJobOrder schoolInJobOrder: lstschoolInJobOrder){
				if(schoolId==0){
					schoolInJobOrderDAO.makeTransient(schoolInJobOrder);
				}else{
					if(schoolInJobOrder.getSchoolId().getSchoolId()==schoolId){
						schoolInJobOrderDAO.makeTransient(schoolInJobOrder);
					}
				}
			}
			
//sandeep			
			createLog.logSchoolInJobOrderLog(request,(ArrayList<SchoolInJobOrder>)lstschoolInJobOrder , userMaster , jobOrder);
			
//end			
			try
			{
				boolean mailFlag = true;
				System.out.println(" jobOrder.getDistrictMaster().getDistrictId() :||||||||||||||||||||||||||||: "+jobOrder.getDistrictMaster().getDistrictId());
				
				//prevent school Add/Edit mail according district
				if(jobOrder.getNotificationToschool()!=null && jobOrder.getNotificationToschool()){
					mailFlag=true;
				}else{
					mailFlag = false;
				}
				if(jobOrder.getDistrictMaster().getDistrictId()==4830640 || jobOrder.getDistrictMaster().getDistrictId()==4218990)
					mailFlag = false;
				System.out.println("mailFlag    "+mailFlag);
				if(mailFlag)
				{
					// Sending Email to Attach Schools 
					if(jobId!=0)
					{
							//List<SchoolMaster> schoolList = new ArrayList<SchoolMaster>();
							if(schoolId!=0)
							{
								List<UserMaster> schoolAdminList = userMasterDAO.getSchoolAdmin(schoolMaster);
								
								String mailContent ="" ;
								
								if(schoolAdminList!=null && schoolAdminList.size()>0)
								{
									for(UserMaster saObj:schoolAdminList)
									{
										System.out.println("  Send Email to School Admin-========== for delete school ==>>>>>>>>>>>============"+saObj.getEmailAddress()+" schoolId "+saObj.getSchoolId().getSchoolName());
										
										DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
										dsmt.setEmailerService(emailerService);
										dsmt.setMailfrom("ashish.kumar@netsutra.com");
										//dsmt.setMailto("ashish.kumar@netsutra.com");
										dsmt.setMailto(saObj.getEmailAddress());
										dsmt.setMailsubject(saObj.getSchoolId().getSchoolName()+" "+Utility.getLocaleValuePropByKey("msgHasBeenRemovedfromthe", locale)+" "+jobOrder.getJobTitle()+" "+Utility.getLocaleValuePropByKey("lblJobOdr", locale)+"");
										mailContent = MailText.emailToSchoolAdminsForDeleteSchool(request,saObj, jobOrder.getDistrictMaster(), jobOrder.getJobTitle());
										dsmt.setMailcontent(mailContent);
										dsmt.start();
										
										System.out.println("mailContent ::--for delete school -----> "+mailContent);
									}
								}
								else
								{
									System.out.println(" school is not in user master table.");
								}
							}
					}
				}
				
			}catch(Exception e)
			{
				e.printStackTrace();
			}
		
		}catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		//saveJobOrderLog(jobId, "");
		return true;
	}
	
	@Transactional(readOnly=false)
	public String  deleteByJobId(int jobId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		StringBuffer sb=new StringBuffer();
		try{
			JobOrder  jobOrder = jobOrderDAO.findById(jobId, false, false);
			
			Criterion criterion3 = Restrictions.eq("jobOrder",jobOrder);
			Criterion criterion4 = Restrictions.eq("status",0);
		
			List<JobRequisitionNumbers> lstcheckJRQNisAnyHired = new ArrayList<JobRequisitionNumbers>();
			lstcheckJRQNisAnyHired = jobRequisitionNumbersDAO.findByCriteria(criterion3,criterion4);
			int counter=0;
			
			for(JobRequisitionNumbers jbrn : lstcheckJRQNisAnyHired)
			{
					DistrictRequisitionNumbers dqrn=jbrn.getDistrictRequisitionNumbers();
					dqrn.setIsUsed(false);
					sb.append("<option value='"+dqrn.getDistrictRequisitionId()+"'>"+dqrn.getRequisitionNumber()+"</option>");
					districtRequisitionNumbersDAO.makePersistent(dqrn);
					jobRequisitionNumbersDAO.makeTransient(jbrn);
					counter++;
			}
			if(jobOrder!=null && counter>0){
				jobOrder.setNoOfExpHires(null);
				jobOrderDAO.makePersistent(jobOrder);
			}
		}catch (Exception e) {
			e.printStackTrace();
			return "";
		}
		return sb.toString();
	}
	
	public String editSchoolInJobOrder(int jobId,long schoolId)
	{
		StringBuffer sb=new StringBuffer();
		try{
			JobOrder jobOrder=jobOrderDAO.findById(jobId,false, false);
			Criterion  criterion1=Restrictions.eq("jobOrder",jobOrder);
			if(schoolId!=0){
				SchoolMaster schoolMaster=schoolMasterDAO.findById(schoolId,false, false);
				Criterion  criterion2=Restrictions.eq("schoolId",schoolMaster);
				
				criterion1=Restrictions.eq("jobId",jobOrder);
				List<SchoolInJobOrder>list=schoolInJobOrderDAO.findByCriteria(criterion1,criterion2);
				SchoolInJobOrder schoolInJobOrders=list.get(0);
				
				sb.append(schoolMaster.getSchoolId()+"!!"+schoolMaster.getSchoolName()+"##");
				sb.append(schoolInJobOrders.getNoOfSchoolExpHires()+"###");
				criterion1=Restrictions.eq("jobOrder",jobOrder);
				criterion2=Restrictions.eq("schoolMaster",schoolMaster);
				
				List<JobRequisitionNumbers>jobRequisitionNumbers=jobRequisitionNumbersDAO.findByCriteria(criterion1,criterion2);
			
				for(JobRequisitionNumbers jb:jobRequisitionNumbers){
					Integer districtreqId=jb.getDistrictRequisitionNumbers().getDistrictRequisitionId();
					String districtreqNumber=jb.getDistrictRequisitionNumbers().getRequisitionNumber();
					if(jb.getStatus()==0)
						sb.append("<option value='"+districtreqId+"'>"+districtreqNumber+"</option>");
					else
						sb.append("<option value='"+districtreqId+"'"+"disabled='disabled'>"+districtreqNumber+"</option>");
				}
			}
			else{
				List<SchoolInJobOrder>list=schoolInJobOrderDAO.findByCriteria(Restrictions.eq("jobId",jobOrder));
				List<JobRequisitionNumbers>jobRequisitionNumbers=jobRequisitionNumbersDAO.findByCriteria(criterion1);
				for(JobRequisitionNumbers jb:jobRequisitionNumbers){
					Integer districtreqId=jb.getDistrictRequisitionNumbers().getDistrictRequisitionId();
					String districtreqNumber=jb.getDistrictRequisitionNumbers().getRequisitionNumber();
					if(jb.getStatus()==0)
						sb.append("<option value='"+districtreqId+"'>"+districtreqNumber+"</option>");
					else
						sb.append("<option value='"+districtreqId+"'"+"disabled='disabled'>"+districtreqNumber+"</option>");
					
					DistrictRequisitionNumbers dd=jb.getDistrictRequisitionNumbers();
					dd.setIsUsed(false);
					districtRequisitionNumbersDAO.makePersistent(dd);
					jobRequisitionNumbersDAO.makeTransient(jb);
				}
				for(SchoolInJobOrder sch:list){
					schoolInJobOrderDAO.makeTransient(sch);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return sb.toString();
	}
	
	@Transactional(readOnly=false)
	public boolean deleteCertification(int jobCertificationId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster = null;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}else{
			userMaster=(UserMaster)session.getAttribute("userMaster");
		}
		
//sandeep		
		try
		{
			JobCertification jobCertification =jobCertificationDAO.findById(jobCertificationId, false, false);
			List<JobCertification> lstJobCertificationLog= null;
			JobOrder  jobOrder = jobOrderDAO.findById(jobCertification.getJobId().getJobId(), false, false);
			Criterion criterion1 = Restrictions.eq("jobId",jobOrder);
			lstJobCertificationLog = jobCertificationDAO.findByCriteria(criterion1);
		
			if(lstJobCertificationLog!= null && lstJobCertificationLog.size()>0)
				createLog.logForCertificate(request,(ArrayList<JobCertification>)lstJobCertificationLog , jobOrder, userMaster);
//end		
			//JobCertification jobCertification =jobCertificationDAO.findById(jobCertificationId, false, false);
			jobCertificationDAO.makeTransient(jobCertification);
			
			
		}catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		//saveJobOrderLog(jobCertification.getJobId().getJobId(), "");
		
		return true;
	}
	@Transactional(readOnly=false)
	public Boolean deleteAssessment(int jobId,int JobOrderType)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		try 
		{
			JobOrder jobOrder = jobOrderDAO.findById(jobId, false,false);
			String filePath="";
			if(JobOrderType==2){
				filePath=Utility.getValueOfPropByKey("districtRootPath")+jobOrder.getDistrictMaster().getDistrictId()+"/";
			}else{
				filePath=Utility.getValueOfPropByKey("schoolRootPath")+jobOrder.getSchool().get(0).getSchoolId()+"/";
			}
			try{
				File file = new File(filePath+"\\"+jobOrder.getAssessmentDocument());
				if(file.delete()){
					//System.out.println(file.getName() + " is deleted!");
				}else{
					//System.out.println("Delete operation is failed.");
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			attachFlagUpdate(jobId);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return null;
	}
	@Transactional(readOnly=false)
	public boolean attachFlagUpdate(int jobId){

		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		try{
			JobOrder  jobOrder = jobOrderDAO.findById(jobId, false, false);
			jobOrder.setAttachNewPillar(2);
			jobOrder.setAssessmentDocument("");
			jobOrderDAO.updatePersistent(jobOrder);
		}catch (Exception e) 
		{
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public List<SchoolMaster> getFieldOfSchoolList(String fieldOfStudy)
	{

		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		List<SchoolMaster> fieldOfSchoolList = null;
		try 
		{
			Criterion criterion = Restrictions.like("schoolName", fieldOfStudy,MatchMode.ANYWHERE);
			if(fieldOfStudy.trim()!=null && fieldOfStudy.trim().length()>0){
				fieldOfSchoolList = schoolMasterDAO.findWithLimit(Order.asc("schoolName"), 0, 25, criterion);
			}else{
				fieldOfSchoolList = schoolMasterDAO.findWithLimit(Order.asc("schoolName"), 0, 25);
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return fieldOfSchoolList;
	}
	/* ===============      Activate Deactivate Job        =========================*/
	@Transactional(readOnly=false)
	public boolean activateDeactivateJob(int jobId,String status)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		ElasticSearchService es=new ElasticSearchService();
		HttpSession session = request.getSession(false);
		UserMaster userMaster=null;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}else{
			userMaster=(UserMaster)session.getAttribute("userMaster");
		}
		try{
			JobOrder  jobOrder = jobOrderDAO.findById(jobId, false, false);
			jobOrder.setStatus(status);
			jobOrderDAO.updatePersistent(jobOrder);
			try{
				SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
				String logType="";
				if(status.equalsIgnoreCase("A")){
					
					//shadab start
					List<JobCertification> lstJobCertification= null;
					Criterion criterion1 = Restrictions.eq("jobId",jobOrder);
					lstJobCertification = jobCertificationDAO.findByCriteria(criterion1);
					
					
					
					
					try
					{
						es.updateESSchoolJobOrderByJobId(jobOrder);
						//for document---------------------------
						es.createJobOrderInElastic(jobOrder, lstJobCertification, 1);
						if(jobOrder.getStatus().equalsIgnoreCase("A") && (jobOrder.getJobStartDate().before(new Date()) ||  jobOrder.getJobStartDate().equals(new Date())) 
								&& (jobOrder.getJobEndDate().after(sdf.parse(sdf.format(new Date()))) ||  jobOrder.getJobEndDate().equals(sdf.parse(sdf.format(new Date())))) 
								)
						{
							//for jobsofinterest shadab
							es.updateESForJobsOfInterestNotCandidate(jobOrder);
							//for jobsofinterest for candidate
							//es.updateESForJobsOfInterestForCandidate(jobOrder);
						}
						
						
						if(jobOrder.getStatus().equalsIgnoreCase("A") && (jobOrder.getIsInviteOnly()==null ||  !jobOrder.getIsInviteOnly()) && jobOrder.getApprovalBeforeGoLive()!=null && jobOrder.getApprovalBeforeGoLive()==1 
								&& (jobOrder.getJobStartDate().before(new Date()) ||  jobOrder.getJobStartDate().equals(new Date())) 
								&& (jobOrder.getJobEndDate().after(sdf.parse(sdf.format(new Date()))) ||  jobOrder.getJobEndDate().equals(sdf.parse(sdf.format(new Date())))) 
								&& (jobOrder.getHiddenJob()==null ||  !jobOrder.getHiddenJob())	)
						{
							
							if(jobOrder.getCreatedForEntity()==2)
							{
								
								es.updateESDistrictJobBoardByJobId(jobOrder);
								//es.updateESDistrictJobOrderByJobId(jobOrder);
							}
							
						}
					}
					catch(Exception e)
					{
						e.printStackTrace();
					}
					
					 
				
					//shadab end
					
					try
					{
						if(jobOrder.getStatus().equalsIgnoreCase("A") 
								&& (jobOrder.getJobStartDate().before(new Date()) ||  jobOrder.getJobStartDate().equals(new Date())) 
								&& (jobOrder.getJobEndDate().after(sdf.parse(sdf.format(new Date()))) ||  jobOrder.getJobEndDate().equals(sdf.parse(sdf.format(new Date())))) 
								)
						{
							updateJobForTeacherStatus(jobOrder.getJobId());
						}
						
					}
					catch(Exception e)
					{
						e.printStackTrace();
					}
					logType="Activate Job Order";
				}else{
					
					//shadab start
					try
					{
						es.updateESSchoolJobOrderByJobId(jobOrder);
						es.searchDocumentByJobid(jobOrder.getJobId());
						if(jobOrder.getCreatedForEntity()==2)
						{
							es.searchDataAndDelete(jobOrder.getJobId(),ElasticSearchConfig.indexForjobsOfInterest);
							es.searchDataAndDelete(jobOrder.getJobId(),ElasticSearchConfig.indexForhqDistrictJobBoard);
							//es.updateESDistrictJobOrderByJobId(jobOrder);
						}
					}
					catch(Exception e)
					{
						e.printStackTrace();
					}
					
					
					//shadab end
					
					logType="Deactivate Job Order";
				}
				userLoginHistoryDAO.insertUserLoginHistory(userMaster,session.getId(),IPAddressUtility.getIpAddress(request),new Date(),logType);
				/*try
				{
					SessionFactory sessionFactory = jobOrderLogDAO.getSessionFactory();
					StatelessSession statelesSsession = sessionFactory.openStatelessSession();
					Transaction txOpen = statelesSsession.beginTransaction();
					JobOrderLog jobOrderLog=new JobOrderLog();
					
					BeanUtils.copyProperties(jobOrderLog, jobOrder);
					jobOrderLog.setJobOrder(jobOrder);
					//jobOrderLog.setCreatedByEntity(userMaster.getEntityType());
					
					try {
						txOpen.begin();
						statelesSsession.insert(jobOrderLog);
						txOpen.commit();
						statelesSsession.close();
					} catch (Exception e) {
						e.printStackTrace();
					}
					
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}*/
			}catch(Exception e){
				e.printStackTrace();
			}
		}catch (Exception e) 
		{
			e.printStackTrace();
			return false;
		}
		return true;
	}
	public List<DistrictMaster> getFieldOfDistrictList(String DistrictName)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}

		List<DistrictMaster> districtMasterList =  new ArrayList<DistrictMaster>();
		List<DistrictMaster> fieldOfDistrictList1 = null;
		List<DistrictMaster> fieldOfDistrictList2 = null;
		try{

			Criterion criterion = Restrictions.like("districtName", DistrictName.trim(),MatchMode.START);
			Criterion criterion1 = Restrictions.eq("status", "A");
			if(DistrictName.trim()!=null && DistrictName.trim().length()>0){
				fieldOfDistrictList1 = districtMasterDAO.findWithLimit(Order.asc("districtName"), 0, 25, criterion,criterion1);

				Criterion criterion2 = Restrictions.ilike("districtName","% "+DistrictName.trim()+"%" );
				fieldOfDistrictList2 = districtMasterDAO.findWithLimit(Order.asc("districtName"), 0, 25, criterion2,criterion1);

				districtMasterList.addAll(fieldOfDistrictList1);
				districtMasterList.addAll(fieldOfDistrictList2);
				Set<DistrictMaster> setDistrict = new LinkedHashSet<DistrictMaster>(districtMasterList);
				districtMasterList = new ArrayList<DistrictMaster>(new LinkedHashSet<DistrictMaster>(setDistrict));

			}else{
				fieldOfDistrictList1 = districtMasterDAO.findWithLimit(Order.asc("districtName"), 0, 25, criterion1);
				districtMasterList.addAll(fieldOfDistrictList1);
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return districtMasterList;
	}
	public List<SchoolMaster> getFieldOfSchoolList(int districtIdForSchool,String SchoolName)
	{
		System.out.println(" getFieldOfSchoolList :: "+districtIdForSchool+" ======== "+SchoolName);
		
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		List<SchoolMaster> schoolMasterList =  new ArrayList<SchoolMaster>();
		List<SchoolMaster> fieldOfSchoolList1 = null;
		List<SchoolMaster> fieldOfSchoolList2 = null;
		try 
		{
			Criterion criterion = Restrictions.like("schoolName", SchoolName,MatchMode.START);
			
			if(districtIdForSchool==0){
				List schoolIds = new ArrayList();
				schoolIds =	districtSchoolsDAO.findSchoolIdAllList();
				Criterion criterionSchoolIds = Restrictions.in("schoolId",schoolIds);
				if(schoolIds.size()>0){
					if(SchoolName.trim()!=null && SchoolName.trim().length()>0){
						fieldOfSchoolList1 = schoolMasterDAO.findWithLimit(Order.asc("schoolName"), 0, 25, criterion,criterionSchoolIds);

						Criterion criterion2 = Restrictions.ilike("schoolName","% "+SchoolName.trim()+"%" );
						fieldOfSchoolList2 = schoolMasterDAO.findWithLimit(Order.asc("schoolName"), 0, 25, criterion2,criterionSchoolIds);

						schoolMasterList.addAll(fieldOfSchoolList1);
						schoolMasterList.addAll(fieldOfSchoolList2);
						Set<SchoolMaster> setSchool = new LinkedHashSet<SchoolMaster>(schoolMasterList);
						schoolMasterList = new ArrayList<SchoolMaster>(new LinkedHashSet<SchoolMaster>(setSchool));
					}else{
						fieldOfSchoolList1 = schoolMasterDAO.findWithLimit(Order.asc("schoolName"), 0, 25,criterionSchoolIds);
						schoolMasterList.addAll(fieldOfSchoolList1);
					}
				}
			}else{
				DistrictMaster districtMaster = districtMasterDAO.findById(districtIdForSchool, false, false);
				Criterion criterion2 = Restrictions.eq("districtId",districtMaster);
				List schoolIds = new ArrayList();
				schoolIds =	districtSchoolsDAO.findSchoolIdList(districtMaster);
				Criterion criterionSchoolIds = Restrictions.in("schoolId",schoolIds);
				if(schoolIds.size()>0){
					if(SchoolName.trim()!=null && SchoolName.trim().length()>0){
						fieldOfSchoolList1 = schoolMasterDAO.findWithLimit(Order.asc("schoolName"), 0, 25, criterion,criterion2,criterionSchoolIds);
						Criterion criterion3 = Restrictions.ilike("schoolName","% "+SchoolName.trim()+"%" );
						fieldOfSchoolList2 = schoolMasterDAO.findWithLimit(Order.asc("schoolName"), 0, 25,criterion3,criterion2,criterionSchoolIds);

						schoolMasterList.addAll(fieldOfSchoolList1);
						schoolMasterList.addAll(fieldOfSchoolList2);
						Set<SchoolMaster> setSchool = new LinkedHashSet<SchoolMaster>(schoolMasterList);
						schoolMasterList = new ArrayList<SchoolMaster>(new LinkedHashSet<SchoolMaster>(setSchool));
					}else{
						fieldOfSchoolList1 = schoolMasterDAO.findWithLimit(Order.asc("schoolName"), 0, 25,criterion2,criterionSchoolIds);
						schoolMasterList.addAll(fieldOfSchoolList1);
					}
				}
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return schoolMasterList;
	}

	/*=================== GAgan : getJobCategoryByDistrictId ==============*/
	public String getJobCategoryByDistrictId(int districtId)
	{
		StringBuffer sb	=	new StringBuffer();
		try {
			DistrictMaster districtMaster	=	districtMasterDAO.findById(districtId, false, false);
			/*=== values to dropdown from jobCategoryMaster Table ===*/
			List<JobCategoryMaster> jobCategoryMasterlst = jobCategoryMasterDAO.findAllJobCategoryNameByDistrict(districtMaster);
			String jcjsiName="";
			sb.append("<select id='jobCategoryId' name='jobCategoryId' class='form-control' onchange='showDivMsg(this.value); '>");
			sb.append("<option value='0'>"+Utility.getLocaleValuePropByKey("optSltJoCatN", locale)+"</option>");
			if(jobCategoryMasterlst!=null && jobCategoryMasterlst.size()>0){
				for(JobCategoryMaster jb: jobCategoryMasterlst)
				{
					//sb.append("<option value='"+jb.getJobCategoryId()+"||"+jb.getBaseStatus()+"'>"+jb.getJobCategoryName()+"</option>");
					if(jb.getAssessmentDocument()!=null)
					{
						jcjsiName	=	jb.getAssessmentDocument();
					}
					//System.out.println("----------- jcjsiName --------"+jcjsiName);
					sb.append("<option value='"+jb.getJobCategoryId()+"||"+jb.getBaseStatus()+"||"+jcjsiName+"'>"+jb.getJobCategoryName()+"</option>");
				}
			}

			sb.append("</select>");
			// System.out.println(" select Html "+sb.toString());


		} catch (Exception e) 
		{
			e.printStackTrace();
		}

		return sb.toString();
	}

	/*=================== GAgan : getDistrictApprovalFromDistrictId ==============*/
	public DistrictMaster getDistrictApprovalFromDistrictId(int districtId)
	{
		//System.out.println("GAgan : getDistrictApprovalFromDistrictId  "+districtId);
		DistrictMaster districtMaster	=	null;
		try 
		{
			if(""+districtId!="")
				districtMaster	=	districtMasterDAO.findById(districtId, false, false);

		} catch (Exception e) 
		{
			e.printStackTrace();
		}
		return districtMaster;
	}
	
	
	/*=================== GAgan : getDistrictApprovalFromDistrictId [ Getting District wise Requisition Number ] ==============*/
	public String getRequisitionByDistrictId(int districtId)
	{
		//System.out.println("GAgan : getRequisitionByDistrictId method in Manage Joborder Ajax   "+districtId);
		StringBuffer sb = new StringBuffer();
		DistrictMaster districtMaster	=	null;
		
		try 
		{
			if(""+districtId!="")
				districtMaster	=	districtMasterDAO.findById(districtId, false, false);
			
			Criterion criterion = Restrictions.eq("districtMaster", districtMaster);
			Criterion criterion1 = Restrictions.eq("isUsed", false);
			List<DistrictRequisitionNumbers> lstDRQN = districtRequisitionNumbersDAO.findByCriteria(criterion,criterion1);
			//System.out.println(" List size :  "+lstDRQN.size());
			//sb.append("    ");
			sb.append("<select multiple id='avlbList' name='avlbList' class='span4' style='height: 150px;' >");
			if(lstDRQN!=null && lstDRQN.size() > 0)
				for(DistrictRequisitionNumbers dqrn : lstDRQN)
					sb.append("<option value='"+dqrn.getDistrictRequisitionId()+"'>"+dqrn.getRequisitionNumber()+"</option>");
			  sb.append("</select>");
		} catch (Exception e) 
		{
			e.printStackTrace();
		}
		return sb.toString();
	}

	/*=================== GAgan : getNoOfAttachedSchool Available in District School  ==============*/
	public int getNoOfAttachedSchool(int districtId)
	{
		//System.out.println("GAgan : getDistrictApprovalFromDistrictId  "+districtId);
		DistrictMaster districtMaster	=	null;
		List<DistrictSchools> lstDistrictSchools	=	null;
		try 
		{
			if(""+districtId!="")
				districtMaster		=	districtMasterDAO.findById(districtId, false, false);
			Criterion criterion1	=	Restrictions.eq("districtMaster", districtMaster);
			lstDistrictSchools	=	districtSchoolsDAO.findByCriteria(criterion1);
			System.out.println(" lstDistrictSchools.size() "+lstDistrictSchools.size());

		} catch (Exception e) 
		{
			e.printStackTrace();
		}
		return lstDistrictSchools.size();
	}
	
	/*=================== GAgan : getRequisationNumberByJobId Available in jobRequisitionNumbers Table  ==============*/
	public String getRequisationNumberByJobId(int jobId,int allSchoolGradeDistrictVal)
	{
		List<JobRequisitionNumbers> lstJobRequisitionNumbers= new ArrayList<JobRequisitionNumbers>();
		JobOrder jobOrder =new JobOrder();
		StringBuffer sb = new StringBuffer();
		try{
			jobOrder= jobOrderDAO.findById(jobId, false, false);
			Criterion criterion = Restrictions.eq("jobOrder", jobOrder);
			Criterion criterion1 = Restrictions.isNull("schoolMaster");
			Criterion criterion2 = Restrictions.isNotNull("schoolMaster");
			if(allSchoolGradeDistrictVal==1)
				lstJobRequisitionNumbers = jobRequisitionNumbersDAO.findByCriteria(criterion,criterion1);
			else
				lstJobRequisitionNumbers = jobRequisitionNumbersDAO.findByCriteria(criterion,criterion2);
			
			System.out.println(" lstJobRequisitionNumbers size :"+lstJobRequisitionNumbers.size());
			/*====== In edit MOde  ============= */
			for(JobRequisitionNumbers jbrn :lstJobRequisitionNumbers)
			{
				sb.append("<div class='span3' id='reqChild_"+jbrn.getJobRequisitionId()+"'>"+jbrn.getDistrictRequisitionNumbers().getRequisitionNumber()+" <a href='javascript:void(0);' onclick=\"return deleteReqNum("+jbrn.getJobRequisitionId()+");\"><img width='15' height='15' class='can' src='images/can-icon.png' alt=''></a></div>");
				//sb.append("<div class='span3' id='reqChild_"+jbrn.getJobRequisitionId()+"'>"+jbrn.getDistrictRequisitionNumbers().getRequisitionNumber()+"</div>");
			}
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();		

	}
	
	@Transactional(readOnly=false)
	public int addRequisationNumber(int jobId,int districtRequisitionId,long schoolId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		JobOrder jobOrder = jobOrderDAO.findById(jobId, false, false);
		SchoolMaster schoolMaster = null;
		if(schoolId+""!="")
			schoolMaster = schoolMasterDAO.findById(schoolId, false, false);
		
		DistrictRequisitionNumbers districtRequisitionNumbers = districtRequisitionNumbersDAO.findById(districtRequisitionId, false, false);
		Criterion criterion1 = Restrictions.eq("jobOrder", jobOrder);
		Criterion criterion2 = Restrictions.eq("districtRequisitionNumbers", districtRequisitionNumbers);
		JobRequisitionNumbers jrqn = new JobRequisitionNumbers();
		
		try
		{
			jrqn.setJobOrder(jobOrder);
			jrqn.setDistrictRequisitionNumbers(districtRequisitionNumbers);
			jrqn.setSchoolMaster(schoolMaster);
			jrqn.setStatus(0);
			List<JobRequisitionNumbers> lstjobRequisitionNumbers = jobRequisitionNumbersDAO.findByCriteria(criterion1,criterion2);
			System.out.println(" lstjobRequisitionNumbers size  "+lstjobRequisitionNumbers.size());
			if(lstjobRequisitionNumbers.size()>0)
				return 3;
			
			jobRequisitionNumbersDAO.makePersistent(jrqn);
			
			/* =========== Gagan Setting IsUsed flag is 1 =======*/
			districtRequisitionNumbers.setIsUsed(true);
			districtRequisitionNumbersDAO.makePersistent(districtRequisitionNumbers);
			
		}catch (Exception e) {
			e.printStackTrace();
			return 2;
		}
		return 1;
	}
	
	@Transactional(readOnly=false)
	public int addTempRequisationNumber(long schoolId,int districtRequisitionId,int expHire,String jobAuthKey)
	{
		System.out.println(" ============== addTempRequisationNumber method  ============== jobAuthKey : "+jobAuthKey);
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster=null;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		else{
			userMaster=(UserMaster)session.getAttribute("userMaster");
		}
	//	JobOrder jobOrder = jobOrderDAO.findById(jobId, false, false);
		//SchoolMaster schoolMaster = null;
		//if(schoolId+""!="")
		SchoolMaster schoolMaster = schoolMasterDAO.findById(schoolId, false, false);
		
		DistrictRequisitionNumbers districtRequisitionNumbers = districtRequisitionNumbersDAO.findById(districtRequisitionId, false, false);
		//Criterion criterion1 = Restrictions.eq("schoolMaster", schoolMaster);
		Criterion criterion2 = Restrictions.eq("districtRequisitionNumbers", districtRequisitionNumbers);
		TempJobRequisitionNumbers tjrqn = new TempJobRequisitionNumbers();
		
		try
		{
			//jrqn.setJobOrder(jobOrder);
			tjrqn.setDistrictRequisitionNumbers(districtRequisitionNumbers);
			tjrqn.setSchoolMaster(schoolMaster);
			tjrqn.setUserMaster(userMaster);
			tjrqn.setNoOfSchoolExpHires(expHire);
			tjrqn.setJobAuthKey(jobAuthKey);
			tjrqn.setCreatedDateTime(new Date());
			//tjrqn.setStatus(0);
			List<TempJobRequisitionNumbers> lstTempJobRequisitionNumbers = new ArrayList<TempJobRequisitionNumbers>();
			lstTempJobRequisitionNumbers = tempJobRequisitionNumbersDAO.findByCriteria(criterion2);
			System.out.println(" lstTempJobRequisitionNumbers size  "+lstTempJobRequisitionNumbers.size());
			if(lstTempJobRequisitionNumbers.size()>0)
				return 3;
			
			tempJobRequisitionNumbersDAO.makePersistent(tjrqn);
		}catch (Exception e) {
			e.printStackTrace();
			return 2;
		}
		return 1;
	}
	
	
	@Transactional(readOnly=false)
	public List<JobRequisitionNumbers> countTotalReqNo(int jobId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		List<JobRequisitionNumbers> lstJobRequisitionNumbers = new ArrayList<JobRequisitionNumbers>();
		try
		{
			
			System.out.println(" jobId "+jobId);
			JobOrder jobOrder = jobOrderDAO.findById(jobId, false, false);
			Criterion criterion1 = Restrictions.eq("jobOrder", jobOrder);
			Criterion criterion2 = Restrictions.isNull("schoolMaster");
			lstJobRequisitionNumbers =jobRequisitionNumbersDAO.findByCriteria(criterion1,criterion2);
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return lstJobRequisitionNumbers;
	}
	@Transactional(readOnly=false)
	public List<JobRequisitionNumbers> editReqNumber(int jobId,int schoolId,int jobAuthKey)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		List<JobRequisitionNumbers> lstJobRequisitionNumbers = new ArrayList<JobRequisitionNumbers>();
		try
		{
			System.out.println(" jobId "+jobId);
			JobOrder jobOrder = jobOrderDAO.findById(jobId, false, false);
			Criterion criterion1 = Restrictions.eq("jobOrder", jobOrder);
			lstJobRequisitionNumbers =jobRequisitionNumbersDAO.findByCriteria(criterion1);
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return lstJobRequisitionNumbers;
	}
	
	@Transactional(readOnly=false)
	public boolean deleteRequisationNumber(int jobRequisitionId,int jobId,int allSchoolGradeDistrictVal)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		JobOrder jobOrder = jobOrderDAO.findById(jobId, false, false);
		List<JobRequisitionNumbers> lstJobRequisitionNumbers = new ArrayList<JobRequisitionNumbers>();
		Criterion criterion1 = Restrictions.eq("jobOrder", jobOrder);
		Criterion criterion2 = Restrictions.isNull("schoolMaster");
		Criterion criterion3 = Restrictions.isNotNull("schoolMaster");
		try
	
		{
			if(jobRequisitionId!=0)
			{
				System.out.println(" jobRequisitionId "+jobRequisitionId);
				JobRequisitionNumbers jobRequisitionNumbers =jobRequisitionNumbersDAO.findById(jobRequisitionId, false, false);
				if(jobRequisitionNumbers.getStatus()==0)
				{
					int dReqId = jobRequisitionNumbers.getDistrictRequisitionNumbers().getDistrictRequisitionId();
					jobRequisitionNumbersDAO.makeTransient(jobRequisitionNumbers);
					
					/*======== Gagan : Setting IsUsed flag of current Req No =0 id status !=1 =============*/
					//System.out.println(" lalalala ala lal a al dReqId "+dReqId+" \n\n\n\n\n =================== Delete job Requisitio Number === jobRequisitionNumbers === : "+jobRequisitionNumbers.getDistrictRequisitionNumbers().getDistrictRequisitionId());
					DistrictRequisitionNumbers drqn = new DistrictRequisitionNumbers();
					drqn = districtRequisitionNumbersDAO.findById(dReqId, false, false);
					drqn.setDistrictRequisitionId(jobRequisitionNumbers.getDistrictRequisitionNumbers().getDistrictRequisitionId());
					drqn.setIsUsed(false);
					districtRequisitionNumbersDAO.makePersistent(drqn);
				}
				else
					return false;
			}
			else
			{
				if(allSchoolGradeDistrictVal==1) /* ===== That means i m deleting Requisition Number added  against to different Schools ========*/
				{
					lstJobRequisitionNumbers = jobRequisitionNumbersDAO.findByCriteria(criterion1,criterion3);
				}
				else
				{
					if(allSchoolGradeDistrictVal==2)
					{
						lstJobRequisitionNumbers = jobRequisitionNumbersDAO.findByCriteria(criterion1,criterion2);
					}
				}
				System.out.println(" List size : "+lstJobRequisitionNumbers.size()+" allSchoolGradeDistrictVal : "+allSchoolGradeDistrictVal);
				
				for(JobRequisitionNumbers jbrn : lstJobRequisitionNumbers)
				{
					jobRequisitionNumbersDAO.makeTransient(jbrn);
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	
	@Transactional(readOnly=false)
	public boolean deleteTempRequisationNumber(int delTempReqId,String jobAuthKey)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster =null;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		else{
			userMaster=(UserMaster)session.getAttribute("userMaster");
		}
		Criterion criterion1 = Restrictions.eq("userMaster", userMaster);
		Criterion criterion3 = Restrictions.eq("jobAuthKey", jobAuthKey);
		DistrictRequisitionNumbers districtRequisitionNumbers = districtRequisitionNumbersDAO.findById(delTempReqId, false, false);
		Criterion criterion2 = Restrictions.eq("districtRequisitionNumbers", districtRequisitionNumbers);
		TempJobRequisitionNumbers tjrqn = new TempJobRequisitionNumbers();
		List<TempJobRequisitionNumbers> lstTempJobRequisitionNumbers = new ArrayList<TempJobRequisitionNumbers>();
		
		try
		{
			lstTempJobRequisitionNumbers = tempJobRequisitionNumbersDAO.findByCriteria(criterion1,criterion2,criterion3);
			if(lstTempJobRequisitionNumbers.size()>0)
			{
				for(TempJobRequisitionNumbers tjrn :lstTempJobRequisitionNumbers)
				{
					tempJobRequisitionNumbersDAO.makeTransient(tjrn);
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public JobOrder findJobByJobId(Map<Integer,JobOrder> jobOrderList,int jobId)
	{
		JobOrder jobOrderObj = new JobOrder();
		try 
		{	if(jobOrderList!=null)
			for(int i=0;i<jobOrderList.size();i++){
				if(jobOrderList.get(i).getJobId().equals(jobId)){
					jobOrderObj=jobOrderList.get(i);
				}
			}	
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(jobOrderObj==null)
			return null;
		else
			return jobOrderObj;
	}
	public StatusMaster findStatusByShortName(List <StatusMaster> statusMasterList,String statusShortName)
	{
		StatusMaster status= null;
		try 
		{   if(statusMasterList!=null)
			for(StatusMaster statusMaster: statusMasterList){
				if(statusMaster.getStatusShortName().equalsIgnoreCase(statusShortName)){
					status=statusMaster;
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}		
		if(status==null)
			return null;
		else
			return status;
	}
	public List<JobForTeacher> findJFTbyJobOrder(Map<String,TeacherStatusHistoryForJob> mapTSHFJob,StatusMaster statusMaster,Map<Integer,JobForTeacher> jobForTeacherList,List<StatusMaster> lstStatusMasters, JobOrder jobOrder,UserMaster userMaster)
	{
		List<JobForTeacher> jobFTeacherList= new ArrayList<JobForTeacher>();
		try 
		{
			String statusStr="";
			boolean statusIdSP=false;
			boolean cgUpdated=false;
			int statusIdForSP=0;
			if(lstStatusMasters!=null)
				for(StatusMaster statusMasterObj: lstStatusMasters){
					statusStr+="||"+statusMasterObj.getStatus()+"||";

				}
			if(jobForTeacherList!=null)
				for(int i=0;i<jobForTeacherList.size();i++){
					cgUpdated=false;
					statusIdSP=false;
					statusIdForSP=0;
					if(jobForTeacherList.get(i).getCgUpdated()!=null){
						if(jobForTeacherList.get(i).getCgUpdated()){
							cgUpdated=true;
						}
					}
					if(lstStatusMasters!=null)
						if(jobForTeacherList.get(i).getJobId().equals(jobOrder) && statusStr.contains("||"+jobForTeacherList.get(i).getStatus().getStatus()+"||") && !jobForTeacherList.get(i).getStatus().equals(statusMaster)){
							if(!jobForTeacherList.get(i).getStatus().getStatusShortName().equalsIgnoreCase("comp")||(jobForTeacherList.get(i).getStatus().getStatusShortName().equalsIgnoreCase("comp") && cgUpdated)){
								if(userMaster.getEntityType()==3 && jobOrder.getDistrictMaster().getStatusMaster()!=null){
									TeacherStatusHistoryForJob forJob=mapTSHFJob.get(jobOrder.getJobId()+"##"+jobForTeacherList.get(i).getTeacherId().getTeacherId()+"##"+jobOrder.getDistrictMaster().getStatusMaster().getStatusId()+"##0");	
									if(forJob!=null && forJob.getStatusMaster()!=null)
										if(forJob.getStatusMaster().getStatusId().equals(jobOrder.getDistrictMaster().getStatusMaster().getStatusId())){
											statusIdForSP=1;
										}
									statusIdSP=true;
								}else if(userMaster.getEntityType()==3 && jobOrder.getDistrictMaster().getSecondaryStatus()!=null){
									statusIdSP=true;
									TeacherStatusHistoryForJob forJob=mapTSHFJob.get(jobOrder.getJobId()+"##"+jobForTeacherList.get(i).getTeacherId().getTeacherId()+"##"+jobOrder.getDistrictMaster().getSecondaryStatus().getSecondaryStatusId()+"##1");	
									if(forJob!=null && forJob.getSecondaryStatus()!=null){
										if(forJob.getSecondaryStatus().getSecondaryStatus_copy()!=null)
											if(forJob.getSecondaryStatus().getSecondaryStatus_copy().getSecondaryStatusId().equals(jobOrder.getDistrictMaster().getSecondaryStatus().getSecondaryStatusId())){
												statusIdForSP=1;
											}
									}
								}
								if(userMaster.getEntityType()==3 && statusIdForSP!=0){
									if(jobForTeacherList.get(i).getCgUpdated()!=null){
										if(jobForTeacherList.get(i).getCgUpdated()){
											jobFTeacherList.add(jobForTeacherList.get(i));
										}
									}
								}else if(statusIdSP==false){
									jobFTeacherList.add(jobForTeacherList.get(i));
								}
							}
						}
				}
		}catch (Exception e) {
			e.printStackTrace();
		}		
		return jobFTeacherList;	
	}
	public boolean baseStatusChecked(List<JobCategoryMaster> jobCategoryMasterList,int jobCategoryId)
	{
		boolean jobCategoryFlag=false;
		try 
		{   if(jobCategoryMasterList!=null)
			for(JobCategoryMaster jobCategoryMaster: jobCategoryMasterList){
				if(jobCategoryMaster.getStatus().equalsIgnoreCase("A") && jobCategoryMaster.getBaseStatus() && jobCategoryMaster.getJobCategoryId()==jobCategoryId){
					jobCategoryFlag=true;
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}		
		return jobCategoryFlag;
	}
	public List<TeacherAssessmentStatus> findAssessmentTaken(List<TeacherAssessmentStatus> teacherAssessmentStatusLst,TeacherDetail teacherDetail,JobOrder jobOrder)
	{
		List<TeacherAssessmentStatus> teacherAssessmentStatusList = new ArrayList<TeacherAssessmentStatus>();
		try {
			if(teacherAssessmentStatusLst!=null)
				for(TeacherAssessmentStatus teacherAssessmentStatus: teacherAssessmentStatusLst){
					JobOrder jobOrderObj=teacherAssessmentStatus.getJobOrder();
					if(jobOrder!=null && jobOrder.getJobId()!=null && jobOrder.getJobId()==0){

						if(teacherDetail==null){
							if(jobOrderObj==null){
								teacherAssessmentStatusList.add(teacherAssessmentStatus);
							}
						}
						else{
							if(teacherAssessmentStatus.getTeacherDetail().equals(teacherDetail) && jobOrderObj==null){
								teacherAssessmentStatusList.add(teacherAssessmentStatus);
							}
						}
					}else{
						if(teacherDetail==null){
							if(jobOrderObj!=null && jobOrder!=null)
								if(jobOrderObj.equals(jobOrder)){
									teacherAssessmentStatusList.add(teacherAssessmentStatus);
								}
						}
						else{
							if(teacherAssessmentStatus.getTeacherDetail().equals(teacherDetail) && jobOrderObj.equals(jobOrder)){
								teacherAssessmentStatusList.add(teacherAssessmentStatus);
							}
						}

					}
				}	
		}catch (Exception e){
			e.printStackTrace();
		}
		return teacherAssessmentStatusList;

	}
	public List<TeacherAssessmentStatus>  findAssessmentTakenByTeachers(List<TeacherAssessmentStatus> teacherAssessmentStatusList,List<TeacherDetail> teacherDetailList)
	{
		List<TeacherAssessmentStatus> teacherAssessmentStatuslst= new ArrayList<TeacherAssessmentStatus>();
		try 
		{	
			if(teacherAssessmentStatusList!=null)
				for(TeacherAssessmentStatus teacherAssessmentStatus: teacherAssessmentStatusList){
					if(teacherDetailList!=null)
						for(TeacherDetail teacherDetail: teacherDetailList){
							if(teacherAssessmentStatus.getTeacherDetail().equals(teacherDetail) && teacherAssessmentStatus.getAssessmentType()==1){
								teacherAssessmentStatuslst.add(teacherAssessmentStatus);
							}
						}
				}	
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return teacherAssessmentStatuslst;
	}
	public int getTotalCountOfCGView(Map<String,TeacherStatusHistoryForJob> mapTSHFJob,String jobId,List<JobCategoryMaster> jobCategoryMasterList,Map<Integer,JobForTeacher> jobForTeacherList,List <StatusMaster> statusMasterList,Map<Integer,JobOrder> jobOrderList,List<TeacherAssessmentStatus> teacherAssessmentStatusList,UserMaster userMaster)
	{
		List<JobForTeacher> lstJobForTeacher = null;
		JobOrder jobOrder = findJobByJobId(jobOrderList,new Integer(jobId));
		StatusMaster statusComp = findStatusByShortName(statusMasterList,"comp");
		StatusMaster statusIcomp = findStatusByShortName(statusMasterList,"icomp");
		StatusMaster statusHired = findStatusByShortName(statusMasterList,"hird");
		StatusMaster statusRemove = findStatusByShortName(statusMasterList,"rem"); 
		StatusMaster statusVlt = findStatusByShortName(statusMasterList,"vlt"); 
		StatusMaster statusDcln = findStatusByShortName(statusMasterList,"dcln"); 
		StatusMaster statusScomp = findStatusByShortName(statusMasterList,"scomp"); 
		StatusMaster statusEcomp = findStatusByShortName(statusMasterList,"ecomp"); 
		StatusMaster statusVcom = findStatusByShortName(statusMasterList,"vcomp"); 
		StatusMaster statusWith = findStatusByShortName(statusMasterList,"widrw"); 

		List<StatusMaster> lstStatusMasters = new ArrayList<StatusMaster>();
		lstStatusMasters.add(statusDcln);
		lstStatusMasters.add(statusScomp);
		lstStatusMasters.add(statusEcomp);
		lstStatusMasters.add(statusVcom);
		lstStatusMasters.add(statusVlt);
		lstStatusMasters.add(statusWith);
		lstStatusMasters.add(statusComp);
		lstStatusMasters.add(statusIcomp);
		lstStatusMasters.add(statusHired);
		lstStatusMasters.add(statusRemove);			
		StatusMaster statusMaster= findStatusByShortName(statusMasterList,"hide");
		lstJobForTeacher =findJFTbyJobOrder(mapTSHFJob,statusMaster,jobForTeacherList,lstStatusMasters,jobOrder,userMaster);
		return lstJobForTeacher.size();
	}

	public boolean findExistingRefNo(String refno,Integer jobId,Integer districtHiddenId)
	{	boolean duplicateRefNo=false;
	System.out.println("refno :"+refno);
	System.out.println("jobId :"+jobId);
	System.out.println("districtHiddenId :"+districtHiddenId);
	DistrictMaster districtMaster=districtMasterDAO.findById(districtHiddenId, false, false);
	if(districtHiddenId!=0){
		duplicateRefNo=jobOrderDAO.findExistJobId(districtMaster,jobId,refno);
	}
	if(duplicateRefNo){
		return true;
	}else{
		return false;
	}
	}
	
	
	public void addStatusSecStatus(Integer jobId,String statusIds,String secStatusIds){
		try{
			if(jobId!=0){
				JobOrder jobOrder=jobOrderDAO.findById(jobId, false, false);
				saveJobWisePanelStatus(jobOrder, statusIds, secStatusIds);
			}
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public String getStatusList(int jobId,Integer districtId,Integer jobCategoryId){
		
		StringBuffer sb=new StringBuffer();
		try{
			Map<Integer,SecondaryStatus> mapSStatus = new HashMap<Integer, SecondaryStatus>();
			List<StatusMaster> statusMasterList	=new ArrayList<StatusMaster>();
			JobOrder jobOrder=null;
			
			List<StatusMaster> lstStatusMaster	=	null;
			List<SecondaryStatus> lstSecondaryStatus=null;
			DistrictMaster districtMaster=null;
			JobCategoryMaster jobCategoryMaster=null;
			//String[] statuss = {"scomp","ecomp","vcomp"};
			//lstStatusMaster	=	statusMasterDAO.findStatusByStatusByShortNames(statuss);
			if(jobCategoryId!=null){
				jobCategoryMaster=jobCategoryMasterDAO.findById(jobCategoryId, false, false); 
				districtMaster=districtMasterDAO.findById(districtId, false, false);
				lstSecondaryStatus=  secondaryStatusDAO.findSecondaryStatusByJobCategory(districtMaster,jobCategoryMaster);
			}
			/*SecondaryStatus secondaryStatus=null;
			for(StatusMaster sm: lstStatusMaster){
				if(sm.getBanchmarkStatus()==1){  		check banchmarkStatus required
					secondaryStatus=mapSStatus.get(sm.getStatusId());
					if(secondaryStatus!=null){
						sm.setStatus(secondaryStatus.getSecondaryStatusName());
					}
				}
				statusMasterList.add(sm);
			}*/
			
			if(jobId!=0){
				jobOrder=jobOrderDAO.findById(jobId, false, false);
				if(jobCategoryId!=null){
					int x=1;
					sb.append("<div class='left20'>");
					sb.append("<input type='hidden' id='noofstatusCheckbox' value='"+statusMasterList.size()+"'>");
					/*
					List<JobWisePanelStatus>lstStatus=jobWisePanelStatusDAO.checkStatusJobOrderWise(jobOrder);
					Map<Integer, Boolean>stMap=new HashMap<Integer, Boolean>();
					for(JobWisePanelStatus jps:lstStatus){
						stMap.put(jps.getStatusMaster().getStatusId(),jps.getPanelStatus());
					}
					for(StatusMaster statusMaster:statusMasterList){
						sb.append("<div class='span3' style='padding-left: 10px;'>");
						boolean rs=false;
						if(stMap.size()>0){
							if(stMap.get(statusMaster.getStatusId())!=null){
								rs=stMap.get(statusMaster.getStatusId());
							}
						}
						if(rs){
							sb.append("<label class='checkbox inline'><input type='checkbox' checked=checked name='status' id='status_"+x+"' value='"+statusMaster.getStatusId()+"'/>"+statusMaster.getStatus()+"</label><br>");
						}	
						else{
							sb.append("<label class='checkbox inline'><input type='checkbox' name='status' id='status_"+x+"' value='"+statusMaster.getStatusId()+"'/>"+statusMaster.getStatus()+"</label><br>");
						}	
						sb.append("</div>");
						++x;
					}*/
					
					x=1;
					sb.append("<input type='hidden' id='noofsecStatusCheckbox' value='"+lstSecondaryStatus.size()+"'>");
					List<JobWisePanelStatus>lstSecStatus=jobWisePanelStatusDAO.checkSecStatusJobOrderWise(jobOrder);
					Map<Integer, Boolean>secStsMap=new HashMap<Integer, Boolean>();
					for(JobWisePanelStatus jps:lstSecStatus){
						secStsMap.put(jps.getSecondaryStatus().getSecondaryStatusId(),jps.getPanelStatus());
					}
					for(SecondaryStatus secsts:lstSecondaryStatus){
						
						//Anurag optimization   if(checkSecondaryStatus(secsts.getSecondaryStatusId())){
					 if(!(secsts!=null && ((secsts.getStatusMaster()!=null && secsts.getStatusMaster().getStatusShortName().equalsIgnoreCase("apl"))  || secsts.getStatusNodeMaster()!=null))){
							System.out.println("::::::::::Anurag replacement of checkSecondaryStatus() ::::::::::::::");
							sb.append("<div class='col-sm-3 col-md-3 top5'>");
							boolean rs=false;
							if(secStsMap.size()>0){
								if(secStsMap.get(secsts.getSecondaryStatusId())!=null){
									rs=secStsMap.get(secsts.getSecondaryStatusId());
								}	
							}
							if(rs){
								sb.append("<label class='checkbox inline' style='padding-left: 0px;'><input type='checkbox' checked=checked name='secstatus' id='secstatus_"+x+"' value='"+secsts.getSecondaryStatusId()+"'/>"+secsts.getSecondaryStatusName()+"</label><br>");
							}	
							else{
								sb.append("<label class='checkbox inline' style='padding-left: 0px;'><input type='checkbox' name='secstatus' id='secstatus_"+x+"' value='"+secsts.getSecondaryStatusId()+"'/>"+secsts.getSecondaryStatusName()+"</label><br>");
							}
							
							sb.append("</div>");
							++x;
						}
					}
					sb.append("</div>");
				}
			}else{
				if(jobCategoryId!=null){
				sb.append("<div class='left20'>");
					sb.append("<input type='hidden' id='noofstatusCheckbox' value='"+statusMasterList.size()+"'>");
					int x=1;
					Map<Integer, Boolean> secStatusMap=getJobCategoryWiseStatusPrevilege(districtMaster,jobCategoryMaster);
					/*for(StatusMaster statusMaster:statusMasterList){
						sb.append("<div class='span3' style='padding-left: 10px;'>");
						if(secStatusMap.get(statusMaster.getStatusId())!=null){
							if(secStatusMap.get(statusMaster.getStatusId())){
								sb.append("<label class='checkbox inline'><input type='checkbox' checked='checked' name='status' id='status_"+x+"' value='"+statusMaster.getStatusId()+"'/>"+statusMaster.getStatus()+"</label><br>");
							}else{
								sb.append("<label class='checkbox inline'><input type='checkbox' name='status' id='status_"+x+"' value='"+statusMaster.getStatusId()+"'/>"+statusMaster.getStatus()+"</label><br>");
							}
						}else{
							sb.append("<label class='checkbox inline'><input type='checkbox' name='status' id='status_"+x+"' value='"+statusMaster.getStatusId()+"'/>"+statusMaster.getStatus()+"</label><br>");
						}
						sb.append("</div>");
						++x;
					}*/
					x=1;
					sb.append("<input type='hidden' id='noofsecStatusCheckbox' value='"+lstSecondaryStatus.size()+"'>");
					
					for(SecondaryStatus secsts:lstSecondaryStatus){
						if(checkSecondaryStatus(secsts.getSecondaryStatusId())){
							sb.append("<div class='col-sm-3 col-md-3'>");
							if(secStatusMap.get(secsts.getSecondaryStatusId())!=null){
								if(secStatusMap.get(secsts.getSecondaryStatusId())){
									sb.append("<label class='checkbox inline' style='padding-left: 0px;'><input type='checkbox' checked='checked' name='secstatus' id='secstatus_"+x+"' value='"+secsts.getSecondaryStatusId()+"'/>"+secsts.getSecondaryStatusName()+"</label><br>");
								}else{
									sb.append("<label class='checkbox inline' style='padding-left: 0px;'><input type='checkbox' name='secstatus' id='secstatus_"+x+"' value='"+secsts.getSecondaryStatusId()+"'/>"+secsts.getSecondaryStatusName()+"</label><br>");
								}
							}else{
								sb.append("<label class='checkbox inline' style='padding-left: 0px;'><input type='checkbox' name='secstatus' id='secstatus_"+x+"' value='"+secsts.getSecondaryStatusId()+"'/>"+secsts.getSecondaryStatusName()+"</label><br>");
							}
							
							sb.append("</div>");
							++x;
						}
					}
					sb.append("</div>");
				}
			}
			
		}catch(Exception exception){
			exception.printStackTrace();
		}
		return sb.toString();
	}
	
	public Map<Integer, Boolean> getJobCategoryWiseStatusPrevilege(DistrictMaster districtMaster,JobCategoryMaster jobCategoryMaster){
		Map<Integer, Boolean>secondaryStatusStateMap=new HashMap<Integer, Boolean>();
		try{
			Criterion criterion1=Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion2=Restrictions.eq("jobCategoryMaster",jobCategoryMaster);
			List<JobCategoryWiseStatusPrivilege>list=jobCategoryWiseStatusPrivilegeDAO.findByCriteria(criterion1,criterion2);
			secondaryStatusStateMap=new HashMap<Integer, Boolean>();
			for(JobCategoryWiseStatusPrivilege jbps:list){
				if(jbps.getStatusMaster()!=null){
					secondaryStatusStateMap.put(jbps.getStatusMaster().getStatusId(),jbps.isPanel());	
				}if(jbps.getSecondaryStatus()!=null){
					secondaryStatusStateMap.put(jbps.getSecondaryStatus().getSecondaryStatusId(),jbps.isPanel());
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return secondaryStatusStateMap;
	}
	
	
	public ArrayList<JobWisePanelStatus> saveJobWisePanelStatus(JobOrder jobOrder,String statusIds ,String secStatusIds){
		System.out.println(">>>>>>>>> call saveJobWisePanelStatus");
		ArrayList<JobWisePanelStatus> listJobWise = new ArrayList<JobWisePanelStatus>();
		
		try{
			if(jobOrder!=null){
			SessionFactory factory=jobWisePanelStatusDAO.getSessionFactory();
			StatelessSession statelessSession=factory.openStatelessSession();
			Transaction transaction=statelessSession.beginTransaction();	
			Criterion criterion1=Restrictions.eq("jobOrder",jobOrder);
			
			/*********************** status *****************************/
			if(!statusIds.equals("")){
				StringTokenizer statusIdsTokens=new StringTokenizer(statusIds,",");
				Criterion criterion2=Restrictions.isNotNull("statusMaster");
				List<JobWisePanelStatus>jobWisePanelStatusList=jobWisePanelStatusDAO.findByCriteria(criterion1,criterion2);
			
				Map<Integer, JobWisePanelStatus>jwsStatusMap=new HashMap<Integer, JobWisePanelStatus>();
				for(JobWisePanelStatus jws:jobWisePanelStatusList){
					jwsStatusMap.put(jws.getStatusMaster().getStatusId(), jws);
				}
				
				List statusIdList = new ArrayList();
				List statusIdListPanelStatus=new ArrayList();
				while(statusIdsTokens.hasMoreElements()){
					String status=statusIdsTokens.nextToken();
					String[] token=status.split("#@@#");
					statusIdList.add(Integer.parseInt(token[0]));
					statusIdListPanelStatus.add(Boolean.valueOf(token[1]));
				}
				List<StatusMaster> lstStatusMasterByArrStatusId	=	statusMasterDAO.findStatusByStatusIdList(statusIdList);
				
				
				if(jobWisePanelStatusList.size()>0){
					for(int i=0;i<lstStatusMasterByArrStatusId.size();++i){
						StatusMaster statusMaster=lstStatusMasterByArrStatusId.get(i);
						Boolean statusState=(Boolean)statusIdListPanelStatus.get(i);
						
						JobWisePanelStatus jobWisePanelStatus=jwsStatusMap.get(statusMaster.getStatusId());
						
						jobWisePanelStatus.setStatusMaster(statusMaster);
						jobWisePanelStatus.setPanelStatus(statusState);
						jobWisePanelStatus.setJobOrder(jobOrder);
						statelessSession.update(jobWisePanelStatus);
						
					}
				}else{
					for(int i=0;i<lstStatusMasterByArrStatusId.size();++i){
						StatusMaster statusMaster=lstStatusMasterByArrStatusId.get(i);
						Boolean statusState=(Boolean)statusIdListPanelStatus.get(i);
						
						JobWisePanelStatus jobWisePanelStatus=new JobWisePanelStatus();
						jobWisePanelStatus.setStatusMaster(statusMaster);
						jobWisePanelStatus.setPanelStatus(statusState);
						jobWisePanelStatus.setJobOrder(jobOrder);
						
						statelessSession.insert(jobWisePanelStatus);
					}
				}
			}
			
			/**************secondary status *****************************/
			if(!secStatusIds.equals("")){
				System.out.println(">>>>>>>>> call saveJobWisePanelStatus secStatusIds not null");
				
				StringTokenizer secStatusIdsTokens=new StringTokenizer(secStatusIds,",");
				
				Criterion criterion3=Restrictions.isNotNull("secondaryStatus");
				List<JobWisePanelStatus>jobWisePanelSecStatusList=jobWisePanelStatusDAO.findByCriteria(criterion1,criterion3);
				Map<Integer, JobWisePanelStatus>jwsSecStatusMap=new HashMap<Integer, JobWisePanelStatus>();
				JobWisePanelStatus jobWise = null;
				for(JobWisePanelStatus jws:jobWisePanelSecStatusList){
					jobWise =new JobWisePanelStatus();
					BeanUtils.copyProperties(jws, jobWise);
					listJobWise.add(jobWise);
					jwsSecStatusMap.put(jws.getSecondaryStatus().getSecondaryStatusId(), jws);
				}
				
				List statusSecIdList = new ArrayList();
				List statusIdListSecPanelStatus=new ArrayList();
				while(secStatusIdsTokens.hasMoreElements()){
					String secStatus=secStatusIdsTokens.nextToken();
					String[] token=secStatus.split("#@@#");
					statusSecIdList.add(Integer.parseInt(token[0]));
					statusIdListSecPanelStatus.add(Boolean.valueOf(token[1]));
				}
				
				System.out.println("split condition");
				
				List<SecondaryStatus> lstSecStatusMasterByArrStatusId	=	secondaryStatusDAO.findSecStatusByStatusIdList(statusSecIdList);
				if(jobWisePanelSecStatusList.size()>0){
					System.out.println("if update");
					for(int i=0;i<lstSecStatusMasterByArrStatusId.size();++i){
						
						SecondaryStatus secondaryStatus=lstSecStatusMasterByArrStatusId.get(i);
						Boolean statusState=(Boolean)statusIdListSecPanelStatus.get(i);
						
						JobWisePanelStatus jobWisePanelStatus=jwsSecStatusMap.get(secondaryStatus.getSecondaryStatusId());
						if(jobWisePanelStatus!=null)
						{
							
							jobWisePanelStatus.setSecondaryStatus(secondaryStatus);
							jobWisePanelStatus.setPanelStatus(statusState);
							jobWisePanelStatus.setJobOrder(jobOrder);
							statelessSession.update(jobWisePanelStatus);
							
						}
					}
					
				}else{
					System.out.println("if insert");
					for(int i=0;i<lstSecStatusMasterByArrStatusId.size();++i){
						SecondaryStatus secondaryStatus=lstSecStatusMasterByArrStatusId.get(i);
						Boolean statusState=(Boolean)statusIdListSecPanelStatus.get(i);
						
						JobWisePanelStatus jobWisePanelStatus=new JobWisePanelStatus();
						jobWisePanelStatus.setSecondaryStatus(secondaryStatus);
						jobWisePanelStatus.setPanelStatus(statusState);
						jobWisePanelStatus.setJobOrder(jobOrder);
						
						statelessSession.insert(jobWisePanelStatus);
					}
				}
				
			}
			
			transaction.commit();
			System.out.println("txt commit");
			statelessSession.close();
		 }	
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return listJobWise;
	}
	
	public boolean checkSecondaryStatus(Integer secondaryStatusId)
	{
		boolean isQuestionAdd=true;
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		try 
		{
			SecondaryStatus secondaryStatus= secondaryStatusDAO.findById(secondaryStatusId, false, false);
			if(secondaryStatus!=null)
			{
				if(secondaryStatus.getStatusMaster()!=null)
					if(secondaryStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("apl"))
						isQuestionAdd=false;
				
				if(secondaryStatus.getStatusNodeMaster()!=null)
					isQuestionAdd=false;
			}
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			return isQuestionAdd;
		}
	}
	
	@Transactional(readOnly=false)
	public void removeJobDescriptionFile(Integer districtId,String files,int savecancelFlag){
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		try 
		{	
			int d=1;
			if(savecancelFlag==2){
				d=0;
			}else if(savecancelFlag==0){
				File file = new File(Utility.getValueOfPropByKey("districtRootPath")+districtId+"/JobDescription/"+files);
				if(file.exists()){
					file.delete();
				}
			}
			//System.out.println("districtId >>> "+districtId+"\n files >> "+files);
			String[] fileArrays=files.split("@##@");
			for(int x=0;x<fileArrays.length-d;x++){
				String fileName=fileArrays[x];
				//System.out.println(">>>>>>>>>>>>> fileNames>>>>> "+fileName);
				File file = new File(Utility.getValueOfPropByKey("districtRootPath")+districtId+"/JobDescription/"+fileName);
				if(file.exists()){
					file.delete();
				}
			}
			
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	
	
	public void  moveAndDeleteTempJobDescriptionFile(Integer districtId,String jobdescFile){
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		try 
		{	
			File tempfile = new File(Utility.getValueOfPropByKey("districtRootPath")+districtId+"/TempJobDescriptionFile/"+jobdescFile);
			File permanent = new File(Utility.getValueOfPropByKey("districtRootPath")+districtId+"/JobDescription/");
			
			InputStream inStream = null;
			OutputStream outStream = null;
    	  
			
			if(permanent!=null){
				if(!permanent.exists()){
					permanent.mkdir();
					permanent = new File(Utility.getValueOfPropByKey("districtRootPath")+districtId+"/JobDescription/"+jobdescFile);
				}else{
					permanent = new File(Utility.getValueOfPropByKey("districtRootPath")+districtId+"/JobDescription/"+jobdescFile);
				}
				outStream = new FileOutputStream(permanent);	
			}
    	    
 
    	    byte[] buffer = new byte[1024];
    	    int length;
    	    if(tempfile.exists()){
				inStream = new FileInputStream(tempfile);
				while ((length = inStream.read(buffer)) > 0){
	    	    	outStream.write(buffer, 0, length);
	    	    }
			}
    	    inStream.close();
    	    outStream.close();
    	    tempfile.delete();
    	    
    	    
    	    /**************** deleting old files **********************/
    	    File tempfiles = new File(Utility.getValueOfPropByKey("districtRootPath")+districtId+"/TempJobDescriptionFile");
    		File[] files= tempfiles.listFiles();
    		Calendar previousFileDate=Calendar.getInstance();
    		previousFileDate.add(Calendar.DATE, -2);
    		
    		for(File f:files){
    			Calendar modifiedDate=Calendar.getInstance();
    			modifiedDate.setTime(new Date(f.lastModified()));
    			if(previousFileDate.compareTo(modifiedDate)>=0){
    				System.out.println("file is deleted :"+f.getName()+"\t"+new Date(f.lastModified()));
    				f.delete();
    			}
    		}
    		/**************** deleting old files **********************/
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	public String getRequistionNos(String rqrString){
		StringBuffer tmRecords=new StringBuffer();
		try{
			String[] sStr=rqrString.split("#");
			List<Integer>lstrqr=new ArrayList<Integer>();
			for(String strRQ:sStr){
				lstrqr.add(Integer.parseInt(strRQ));
			}
			Criterion criterion=Restrictions.in("districtRequisitionId", lstrqr);
			List<DistrictRequisitionNumbers>list=districtRequisitionNumbersDAO.findByCriteria(criterion);
			
			for(DistrictRequisitionNumbers drq:list){
				tmRecords.append("<option value='"+drq.getDistrictRequisitionId()+"'>"+drq.getRequisitionNumber()+"</option>");
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		System.out.println("\n\n\n\n\n >>>>>>>>>rrrr>>>>>>>"+tmRecords.toString());
		return tmRecords.toString();
	}
	
	public Boolean IsRequisitionForDistrictORSchool(int jobId){
		Boolean result=false;
		try{
			JobOrder jobOrder=jobOrderDAO.findById(jobId,false,false);
			if(jobOrder.getNoSchoolAttach()==1)
				result=true;
		}catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	
	public String getRequistionNoByDID(int districtId)
	{
		
		StringBuffer buffer=new StringBuffer();
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		
		DistrictMaster districtMaster=districtMasterDAO.findById(districtId, false,  false);
		List<DistrictRequisitionNumbers> avlbList = new ArrayList<DistrictRequisitionNumbers>();
		
		Criterion criterion3 = Restrictions.eq("districtMaster", districtMaster);
		Criterion criterion4 = Restrictions.eq("isUsed", false);
		
		if(districtMaster.getIsReqNoRequired())
		{
			avlbList = districtRequisitionNumbersDAO.findByCriteria(criterion3,criterion4);
			if(avlbList!=null && avlbList.size() > 0)
			{
				for(DistrictRequisitionNumbers pojo:avlbList)
				{
					if(pojo!=null)
					{
						buffer.append("<option value="+pojo.getDistrictRequisitionId()+">"+pojo.getRequisitionNumber()+"</option>");
					}
				}
			}
		}
		
		return buffer.toString();
	}
	
	
	public String saveJobEequisitionNumbers(int jobId,String arrReqNumAndSchoolId,Integer sSource,String newRequisitionText)
	{
		System.out.println(" calling saveJobEequisitionNumbers jobId ================== "+jobId);
		System.out.println("jobId "+jobId +" arrReqNumAndSchoolId "+arrReqNumAndSchoolId +" sSource "+sSource +" newRequisitionText "+newRequisitionText);
		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		
		UserMaster userMaster=(UserMaster)session.getAttribute("userMaster");
		
		JobOrder jobOrder=jobOrderDAO.findById(jobId, false, false);
		SchoolMaster schoolMaster=null;
		
		//for email
		List<SchoolMaster> existSchoolList = null;
		List<JobRequisitionNumbers> exitReqIds = null;
		List<Integer>listDrqId =new ArrayList<Integer>();
		List<JobRequisitionNumbers> resultReqNumList = null;
		List<DistrictRequisitionNumbers> districtRequisitionNumbers = new ArrayList<DistrictRequisitionNumbers>();
		StringBuffer sb=new StringBuffer();
		//List<JobForTeacher> existJobList = null; 
		
		try {
			
			List<DistrictRequisitionNumbers> lstDistrictRequisitionNumbers=new ArrayList<DistrictRequisitionNumbers>();
			DistrictMaster districtMaster=jobOrder.getDistrictMaster();
			if(sSource==1)
			{
				if(newRequisitionText!=null && !newRequisitionText.equalsIgnoreCase(""))
				{
					String arryNewReq[]=newRequisitionText.trim().split(",");
					DistrictRequisitionNumbers objDRN=new DistrictRequisitionNumbers();
					lstDistrictRequisitionNumbers=new ArrayList<DistrictRequisitionNumbers>();
					for(String sReqT :arryNewReq)
					{
						if(sReqT!=null && !sReqT.equalsIgnoreCase(""))
						{
							sReqT=sReqT.trim();
							objDRN=new DistrictRequisitionNumbers();
							objDRN.setCreatedDateTime(new Date());
							objDRN.setDistrictMaster(districtMaster);
							objDRN.setIsUsed(false);
							objDRN.setUserMaster(userMaster);
							
							if(jobOrder.getJobType()!=null && !jobOrder.getJobType().equals("") && jobOrder.getJobType().equalsIgnoreCase("P"))
								objDRN.setPosType("P");
							else
								objDRN.setPosType("F");
							
							
							objDRN.setRequisitionNumber(sReqT);
							districtRequisitionNumbersDAO.makePersistent(objDRN);
							
							lstDistrictRequisitionNumbers.add(objDRN);
							sb.append("<option value="+ objDRN.getDistrictRequisitionId() +">" + objDRN.getRequisitionNumber() + "</option>");
							
						}
					}
						
				}
				
				
				
				
			}
			
			System.out.println(" arrReqNumAndSchoolId :>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>: "+arrReqNumAndSchoolId);
			
			if(arrReqNumAndSchoolId!="" && arrReqNumAndSchoolId.length()>0)
			{
					 if(jobId!=0)
					 { 
						//Email Code:: check school according jobId
						 existSchoolList = schoolInJobOrderDAO.findSchoolIds(jobOrder);
						 
						 System.out.println("  schoolIds >>>>>>>>>>>> "+existSchoolList.size());
						 
						 String fStr[]=arrReqNumAndSchoolId.split("##");
						int noOfExpHiresSchool;
						
						System.out.println(" ************* fStr *************** "+fStr.length);
						
						for(int x=0;x<fStr.length;x++)
						{
							String[] sStr=fStr[x].split("!!");
							schoolMaster=schoolMasterDAO.findById(Long.parseLong(sStr[0]), false, false);
							
							// for get existing reqsition number
							exitReqIds = jobRequisitionNumbersDAO.findJobRequisitionNumbersByJobWithSchool(jobOrder, schoolMaster); 
							
							/*if(!jobOrder.getDistrictMaster().getIsReqNoRequired())
							{
								noOfExpHiresSchool=Integer.parseInt(sStr[1].replace("@@",""));
							}
							else
							{*/
								String[] exdrqStr=sStr[1].split("@@");
								System.out.println("exdrqStr L "+exdrqStr.length);
								
								noOfExpHiresSchool=Integer.parseInt(exdrqStr[0]);
								System.out.println("noOfExpHiresSchool "+noOfExpHiresSchool);
								
								if(exdrqStr.length >1 || sSource==1)
								{
									
									if(sSource==2)
									{
										String[] drqIds=exdrqStr[1].split("#");
										for(int xx=0;xx<drqIds.length;xx++)
											listDrqId.add(Integer.parseInt(drqIds[xx]));
										
										lstDistrictRequisitionNumbers=districtRequisitionNumbersDAO.getDistrictRequisitionList(listDrqId);
									}
									try
									{
										//for Email
							//			resultReqNumList = jobRequisitionNumbersDAO.requisitionNumberExistOrNot(jobOrder, schoolMaster, districtRequisitionNumbers);
							//			districtRequisitionNumbers = districtRequisitionNumbersDAO.getDistrictRequisitionList(listDrqId);
									}
									catch(Exception e)
									{
										e.printStackTrace();
									}
									
									List<JobRequisitionNumbers> lstschInj=new ArrayList<JobRequisitionNumbers>();
									if(lstDistrictRequisitionNumbers!=null && lstDistrictRequisitionNumbers.size() >0)
									{
										lstschInj=jobRequisitionNumbersDAO.findByCriteria(Restrictions.eq("jobOrder", jobOrder),Restrictions.eq("schoolMaster", schoolMaster),Restrictions.not(Restrictions.in("districtRequisitionNumbers", lstDistrictRequisitionNumbers)));
										if(lstschInj!=null && lstschInj.size()>0)
										{
											for(JobRequisitionNumbers jrq:lstschInj)
											{	
												//jrq.getDistrictRequisitionNumbers().setIsUsed(false);
												//jobRequisitionNumbersDAO.makePersistent(jrq);
												
												DistrictRequisitionNumbers requisitionNumbers=jrq.getDistrictRequisitionNumbers();
												requisitionNumbers.setIsUsed(false);
												districtRequisitionNumbersDAO.makePersistent(requisitionNumbers);
												jobRequisitionNumbersDAO.makeTransient(jrq);
											}
										}
										
										
										for(DistrictRequisitionNumbers drqn : lstDistrictRequisitionNumbers)
										{
											JobRequisitionNumbers jrqn = new JobRequisitionNumbers();
											jrqn.setJobOrder(jobOrder);
											jrqn.setSchoolMaster(schoolMaster);
											jrqn.setDistrictRequisitionNumbers(drqn);	
											jrqn.setStatus(0);
											int s=jobRequisitionNumbersDAO.findByCriteria(Restrictions.eq("districtRequisitionNumbers", drqn),Restrictions.eq("schoolMaster", schoolMaster)).size();
											if(s==0)
											{
												jobRequisitionNumbersDAO.makePersistent(jrqn);
												drqn.setIsUsed(true);
												districtRequisitionNumbersDAO.makePersistent(drqn);
											}
										}
									}
									
								}
								else
								{
									// if all requisitions are pull-out
									List<JobRequisitionNumbers> lstschInj=jobRequisitionNumbersDAO.findByCriteria(Restrictions.eq("jobOrder", jobOrder),Restrictions.eq("schoolMaster", schoolMaster));
									for(JobRequisitionNumbers jrq:lstschInj)
									{	
										DistrictRequisitionNumbers requisitionNumbers=jrq.getDistrictRequisitionNumbers();
										requisitionNumbers.setIsUsed(false);
										districtRequisitionNumbersDAO.makePersistent(requisitionNumbers);
										jobRequisitionNumbersDAO.makeTransient(jrq);
									}
								}
								
								
								
							//}
							//for log
							List<SchoolInJobOrder> lstschoolInJobOrderLog=schoolInJobOrderDAO.findByCriteria(Restrictions.eq("jobId", jobOrder));
								
								
							List<SchoolInJobOrder> lstschoolInJobOrder=schoolInJobOrderDAO.findByCriteria(Restrictions.eq("jobId", jobOrder),Restrictions.eq("schoolId", schoolMaster));
							SchoolInJobOrder schoolInJobOrder=null;
							SchoolInJobOrder schoolInJobOrderLog=null;
							
							
							if(lstschoolInJobOrder!=null && lstschoolInJobOrder.size()>0){
								schoolInJobOrder=lstschoolInJobOrder.get(0);
							}else{
								schoolInJobOrder=new SchoolInJobOrder();
							}
							schoolInJobOrder.setJobId(jobOrder);
							schoolInJobOrder.setNoOfSchoolExpHires(noOfExpHiresSchool);
							schoolInJobOrder.setSchoolId(schoolMaster);
							schoolInJobOrder.setCreatedDateTime(new Date());
							schoolInJobOrderDAO.makePersistent(schoolInJobOrder);
							
							
//sandeep							
								createLog.logSchoolInJobOrderLog(request,(ArrayList<SchoolInJobOrder>)lstschoolInJobOrderLog , userMaster , schoolInJobOrder.getJobId());
//end							
							
							//for Email
							try
							{
								//System.out.println(" get existing req numbers ::::::::::::    "+exitReqIds.size()+" >>>>>>> add req numbers :: "+listDrqId.size());
								int countReqExist = 0;
								boolean checkReq=false;
								boolean mailFlag=false;
								
								//Send Mail to Attach School but this will send two email to one because of FOR LOOP
								if(existSchoolList!=null)
								{
									if(exitReqIds.size()>0)
									{
										if(exitReqIds.size()!=listDrqId.size())
										{
											System.out.println("11111111111111111");
											checkReq=true;
										}
										else
										{
											for(JobRequisitionNumbers existreqNum:exitReqIds)
											{
												for(Integer addNewReqNum:listDrqId)
												{
													System.out.println("existreqNum "+existreqNum.getDistrictRequisitionNumbers().getDistrictRequisitionId()+" addNewReqNum 6666666666:: "+addNewReqNum);
													if(existreqNum.getDistrictRequisitionNumbers().getDistrictRequisitionId().equals(addNewReqNum))
														countReqExist++;
												}
											}
											
											System.out.println(" exitReqIds.size() "+exitReqIds.size()+" countReqExist===================== "+countReqExist);
											
											if(exitReqIds.size()!=countReqExist)
											{
												System.out.println("2222222222222222222222");
												checkReq=true;
											}
										}
									}
									
									//System.out.println(" countReqExist :: "+countReqExist+" exitReqIds.size() :: "+exitReqIds.size());
									
									
										for(SchoolMaster lstIds:existSchoolList)
										{
											System.out.println(" ==== check exist schools :: ===== lstIds.getSchoolId() "+lstIds.getSchoolId()+" schoolMaster.getSchoolId() "+schoolMaster.getSchoolId()+" checkReq :: "+checkReq);
											
											//Edit condition
											if(exitReqIds.size()>0)
											{
												if(lstIds.getSchoolId().equals(schoolMaster.getSchoolId()))
												{
													if(checkReq)
															mailFlag=true;
												}
											} // Add Condition
											else
											{
												if(!lstIds.getSchoolId().equals(schoolMaster.getSchoolId()))
												{
													System.out
															.println("44444444444444444444444444444444444");
													mailFlag=true;
												}
											}
											//Add Condition
											/*else if(!lstIds.getSchoolId().equals(schoolMaster.getSchoolId()))
											{
												System.out
														.println("4444444444444444");
												mailFlag=true;
											}*/
											
											/*if(checkReq)
											{	
												if(lstIds.getSchoolId().equals(schoolMaster.getSchoolId()))
														mailFlag=true;
											}
											else
											{
												if(!lstIds.getSchoolId().equals(schoolMaster.getSchoolId()))
													mailFlag=true;
											}*/
										}
										
										System.out.println(" jobOrder.getDistrictMaster().getDistrictId() ::===========||||||||||||| "+jobOrder.getDistrictMaster().getDistrictId());
										
										//prevent school Add/Edit mail according district
										if(jobOrder.getNotificationToschool()!=null && jobOrder.getNotificationToschool()){
											mailFlag=true;
										}else{
											mailFlag = false;
										}
										if(jobOrder.getDistrictMaster().getDistrictId()==4830640 || jobOrder.getDistrictMaster().getDistrictId()==4218990){
											mailFlag = false;
										}
										System.out.println(" mailFlag :: "+mailFlag);
										
										if(mailFlag)
										{
											List<UserMaster> schoolAdminList = userMasterDAO.getSchoolAdmin(schoolMaster);
											String mailContent ="" ;
											if(schoolAdminList!=null && schoolAdminList.size()>0)
											{
												for(UserMaster saObj:schoolAdminList)
												{
												
													System.out.println("  Send Email to School Admin-========== for add school ==>>>>>>>>>>>============"+saObj.getEmailAddress()+" schoolId "+saObj.getSchoolId().getSchoolName());
													DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
													dsmt.setEmailerService(emailerService);
													dsmt.setMailfrom("ashish.kumar@netsutra.com");
													//dsmt.setMailto("ashish.kumar@netsutra.com");
													dsmt.setMailto(saObj.getEmailAddress());
													dsmt.setMailsubject(saObj.getSchoolId().getSchoolName()+" "+Utility.getLocaleValuePropByKey("msgHasBeenAddedthe", locale)+" "+jobOrder.getJobTitle()+" "+Utility.getLocaleValuePropByKey("lblJobOdr", locale)+"");
													//mailContent = MailText.emailToSchoolAdmins(request,saObj,userMaster,districtMaster,jobTitle);
													mailContent = MailText.emailToSchoolAdminsForAddSchool(request,saObj, jobOrder.getDistrictMaster(), jobOrder.getJobTitle());
													dsmt.setMailcontent(mailContent);
													dsmt.start();
													
													System.out.println("mailContent ::--for Add edit school -----> "+mailContent);
												}
											}
											else
											{
												System.out.println(" school is not in user master table.");
											}
										}
								}
							}catch(Exception e)
							{
								e.printStackTrace();
							}
						}
					}
			}
		} catch (NumberFormatException e) {
			e.printStackTrace();
			return "0";
		}
		if(jobId!=0)
		{
			//saveJobOrderLog(jobId, "");
		}
		return sb.toString();
	}
	
	
	
	public String saveJobEequisitionNumbersTemp(String arrReqNumAndSchoolId)
	{
		System.out.println("T calling saveJobEequisitionNumbersTemp");
		System.out.println(" arrReqNumAndSchoolId "+arrReqNumAndSchoolId);
		
		StringBuffer buffer=new StringBuffer();
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		
		String tempId=session.getId();
		
		SchoolMaster schoolMaster=null;
		try {
			//2==%%100512!!1@@##
			if(arrReqNumAndSchoolId!="" && arrReqNumAndSchoolId.length()>0)
			{
						String sNewReqTextDetails[]=arrReqNumAndSchoolId.split("%%");
						if(sNewReqTextDetails!=null && sNewReqTextDetails.length > 0)
						{
							String sNewReqText=sNewReqTextDetails[0];
							System.out.println("sNewReqText "+sNewReqText);
							if(sNewReqText!=null && !sNewReqText.equalsIgnoreCase(""))
							{
								String arryNewReqDetails[]=sNewReqText.split("==");
								System.out.println("arryNewReqDetails L "+arryNewReqDetails.length);
								String sSource=arryNewReqDetails[0];
								String sNewReqDetails="";
								if(arryNewReqDetails.length >1)
									sNewReqDetails=arryNewReqDetails[1];
								
								System.out.println("sSource "+sSource);
								System.out.println("sNewReqDetails "+sNewReqDetails);
								
									String sOldFormatReqText=sNewReqTextDetails[1];
									String fStr[]=sOldFormatReqText.split("##");
									int noOfExpHiresSchool;
									
									System.out.println("fStr.length "+fStr.length);
									
									for(int x=0;x<fStr.length;x++)
									{
										String[] sStr=fStr[x].split("!!");
										schoolMaster=schoolMasterDAO.findById(Long.parseLong(sStr[0]), false, false);
										
											String[] exdrqStr=sStr[1].split("@@");
											System.out.println("exdrqStr L "+exdrqStr.length);
											noOfExpHiresSchool=Integer.parseInt(exdrqStr[0]);
											System.out.println("noOfExpHiresSchool "+noOfExpHiresSchool);
											
											List<Integer>listDrqId=new ArrayList<Integer>();
											if(exdrqStr.length >1)
											{
												String[] drqIds=exdrqStr[1].split("#");
												for(int xx=0;xx<drqIds.length;xx++){
													listDrqId.add(Integer.parseInt(drqIds[xx]));
												}
											}
											
											//Start ... delete 
											Criterion criterion1=Restrictions.eq("schoolMaster", schoolMaster);
											Criterion criterion2=Restrictions.eq("tempId", tempId);
											List<Jobrequisitionnumberstemp> lstJobrequisitionnumberstemp=jobRequisitionNumbersTempDAO.findByCriteria(criterion1,criterion2);
											//System.out.println("lstJobrequisitionnumberstemp "+lstJobrequisitionnumberstemp.size());
											
											if(lstJobrequisitionnumberstemp!=null && lstJobrequisitionnumberstemp.size()>0)
											{
												for(Jobrequisitionnumberstemp jobrequisitionnumberstemp:lstJobrequisitionnumberstemp)
												{
													jobRequisitionNumbersTempDAO.makeTransient(jobrequisitionnumberstemp);
												}
											}
											
											//End ... delete
											System.out.println("sSource "+sSource);
											if(sSource.equalsIgnoreCase("2"))
											{
												List<DistrictRequisitionNumbers> lstDistrictRequisitionNumbers=new ArrayList<DistrictRequisitionNumbers>();
												if(listDrqId!=null && listDrqId.size() > 0)
												{
													lstDistrictRequisitionNumbers=districtRequisitionNumbersDAO.getDistrictRequisitionList(listDrqId);
													for(DistrictRequisitionNumbers drqn : lstDistrictRequisitionNumbers)
													{
														Jobrequisitionnumberstemp jrqn = new Jobrequisitionnumberstemp();
														//jrqn.setJobOrder(jobOrder);
														jrqn.setSchoolMaster(schoolMaster);
														jrqn.setDistrictRequisitionNumbers(drqn);	
														jrqn.setStatus(0);
														jrqn.setNoOfSchoolExpHires(noOfExpHiresSchool);
														jrqn.setTempId(tempId);
														jobRequisitionNumbersTempDAO.makePersistent(jrqn);
													}
													
												}
												else
												{
													Jobrequisitionnumberstemp jrqn = new Jobrequisitionnumberstemp();
													//jrqn.setJobOrder(jobOrder);
													jrqn.setSchoolMaster(schoolMaster);
													//jrqn.setDistrictRequisitionNumbers(drqn);	
													jrqn.setStatus(0);
													jrqn.setNoOfSchoolExpHires(noOfExpHiresSchool);
													jrqn.setTempId(tempId);
													jobRequisitionNumbersTempDAO.makePersistent(jrqn);
												}
											}
											else if(sSource.equalsIgnoreCase("1"))
											{
												System.out.println("sNewReqDetails "+sNewReqDetails);
												if(sNewReqDetails!=null && !sNewReqDetails.equalsIgnoreCase(""))
												{
													String arryNewReqNo[]=sNewReqDetails.trim().split(",");
													System.out.println("arryNewReqNo.length "+arryNewReqNo.length);
													if(arryNewReqNo!=null && arryNewReqNo.length >0)
													{
														for(String reqNo:arryNewReqNo)
														{
															
															if(reqNo!=null && !reqNo.equalsIgnoreCase(""))
															{
																System.out.println("reqNo "+reqNo);
																
																Jobrequisitionnumberstemp jrqn = new Jobrequisitionnumberstemp();
																jrqn.setSchoolMaster(schoolMaster);
																jrqn.setStatus(0);
																jrqn.setNoOfSchoolExpHires(noOfExpHiresSchool);
																jrqn.setTempId(tempId);
																jrqn.setReqText(reqNo);
																jobRequisitionNumbersTempDAO.makePersistent(jrqn);
															}
														}
													}
												}
												else
												{
													Jobrequisitionnumberstemp jrqn = new Jobrequisitionnumberstemp();
													jrqn.setSchoolMaster(schoolMaster);
													jrqn.setStatus(0);
													jrqn.setNoOfSchoolExpHires(noOfExpHiresSchool);
													jrqn.setTempId(tempId);
													jobRequisitionNumbersTempDAO.makePersistent(jrqn);
												}
												
												
											}
											
											
									}
								
							}
							
							
							
						}
				
						
			}
		} catch (NumberFormatException e) {
			e.printStackTrace();
			return "0";
		}
		return "1";
	}
	
	public String saveJobEequisitionNumbersTemp_NoReq(String arrReqNumAndSchoolId)
	{
		System.out.println(" calling saveJobEequisitionNumbersTemp_NoReq ");
		System.out.println(" arrReqNumAndSchoolId "+arrReqNumAndSchoolId);
		
		StringBuffer buffer=new StringBuffer();
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		
		String tempId=session.getId();
		
		SchoolMaster schoolMaster=null;
		try {
			
			if(arrReqNumAndSchoolId!="" && arrReqNumAndSchoolId.length()>0)
			{
						String fStr[]=arrReqNumAndSchoolId.split("##");
						int noOfExpHiresSchool=0;
						long schoolId=0;
						
						System.out.println("fStr "+fStr.length);
						
						if(fStr.length==2)
						{
							schoolId=Long.parseLong(fStr[0]);
							noOfExpHiresSchool=Integer.parseInt(fStr[1]);
							
							if(schoolId > 0)
								schoolMaster=schoolMasterDAO.findById(schoolId, false, false);
						}
						
						
						
						if(schoolMaster!=null)
						{
							
							Criterion criterion1=Restrictions.eq("schoolMaster", schoolMaster);
							Criterion criterion2=Restrictions.eq("tempId", tempId);
							List<Jobrequisitionnumberstemp> lstJobrequisitionnumberstemp=jobRequisitionNumbersTempDAO.findByCriteria(criterion1,criterion2);
							if(lstJobrequisitionnumberstemp.size()== 0)
							{
								Jobrequisitionnumberstemp jrqn = new Jobrequisitionnumberstemp();
								jrqn.setSchoolMaster(schoolMaster);
								jrqn.setNoOfSchoolExpHires(noOfExpHiresSchool);
								jrqn.setStatus(0);
								jrqn.setTempId(tempId);
								jobRequisitionNumbersTempDAO.makePersistent(jrqn);
								System.out.println("Added schoolId "+schoolId+" noOfExpHiresSchool "+noOfExpHiresSchool);
							}
							else
							{
								Jobrequisitionnumberstemp jobrequisitionnumberstemp=new Jobrequisitionnumberstemp();
								jobrequisitionnumberstemp=lstJobrequisitionnumberstemp.get(0);
								jobrequisitionnumberstemp.setSchoolMaster(schoolMaster);
								jobrequisitionnumberstemp.setNoOfSchoolExpHires(noOfExpHiresSchool);
								jobrequisitionnumberstemp.setStatus(0);
								jobrequisitionnumberstemp.setTempId(tempId);
								jobRequisitionNumbersTempDAO.makePersistent(jobrequisitionnumberstemp);
								System.out.println("Updated schoolId "+schoolId+" noOfExpHiresSchool "+noOfExpHiresSchool);
								
							}
							
							
							
							
						}
						
			}
		} catch (NumberFormatException e) {
			e.printStackTrace();
			return "0";
		}
		return "1";
	}
	
	public String saveJobEequisitionNumbers_NoReq(int jobId, String arrReqNumAndSchoolId)
	{
		System.out.println(" calling saveJobEequisitionNumbers_NoReq jobId "+jobId);
		System.out.println(" arrReqNumAndSchoolId "+arrReqNumAndSchoolId);
		
		JobOrder jobOrder=jobOrderDAO.findById(jobId, false, false);
		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		
		SchoolMaster schoolMaster=null;
		int existexpHires = 0;
		
		try {
			
			if(arrReqNumAndSchoolId!="" && arrReqNumAndSchoolId.length()>0)
			{
						String fStr[]=arrReqNumAndSchoolId.split("##");
						int noOfExpHiresSchool=0;
						long schoolId=0;
						
						System.out.println("fStr "+fStr.length);
						
						if(fStr.length==2)
						{
							schoolId=Long.parseLong(fStr[0]);
							noOfExpHiresSchool=Integer.parseInt(fStr[1]);
							
							if(schoolId > 0)
								schoolMaster=schoolMasterDAO.findById(schoolId, false, false);
						}
						
						if(schoolMaster!=null && jobOrder!=null)
						{
							Criterion criterion1=Restrictions.eq("schoolId", schoolMaster);
							Criterion criterion2=Restrictions.eq("jobId", jobOrder);
							List<SchoolInJobOrder> lstSchoolInJobOrder=schoolInJobOrderDAO.findByCriteria(criterion1,criterion2);
							
							//For email
							try{
								if(lstSchoolInJobOrder.size()== 1)
									existexpHires = (int)lstSchoolInJobOrder.get(0).getNoOfSchoolExpHires();
							}
							catch(Exception e)
							{
								e.printStackTrace();
							}
							
							if(lstSchoolInJobOrder.size()== 1)
							{
								SchoolInJobOrder schoolInJobOrder=lstSchoolInJobOrder.get(0);
								schoolInJobOrder.setNoOfSchoolExpHires(noOfExpHiresSchool);
								schoolInJobOrderDAO.makePersistent(schoolInJobOrder);
							}
							else
							{
								SchoolInJobOrder schoolInJobOrder=new SchoolInJobOrder();
								schoolInJobOrder.setJobId(jobOrder);
								schoolInJobOrder.setNoOfSchoolExpHires(noOfExpHiresSchool);
								schoolInJobOrder.setSchoolId(schoolMaster);
								schoolInJobOrder.setCreatedDateTime(new Date());
								schoolInJobOrderDAO.makePersistent(schoolInJobOrder);
							}
							
							
							//For Email
							try
							{
								boolean mailFlag = false;
								
								//For Edit
								if(lstSchoolInJobOrder.size()== 1)
								{
									if(noOfExpHiresSchool>0)
										if(existexpHires!=noOfExpHiresSchool)
											mailFlag=true;
								}//For Add
								else
								{
									if(noOfExpHiresSchool>0)
										mailFlag=true;
								}
								
								
								System.out.println(" jobOrder.getDistrictMaster().getDistrictId() ::gggggggggggggggggggg "+jobOrder.getDistrictMaster().getDistrictId());
								
								//prevent school Add/Edit mail according district
								if(jobOrder.getNotificationToschool()!=null && jobOrder.getNotificationToschool()){
									mailFlag=true;
								}else{
									mailFlag = false;
								}
								if(jobOrder.getDistrictMaster().getDistrictId()==4830640 || jobOrder.getDistrictMaster().getDistrictId()==4218990)
									mailFlag = false;
								
								if(mailFlag)
								{
									List<UserMaster> schoolAdminList = userMasterDAO.getSchoolAdmin(schoolMaster);
									String mailContent ="" ;
									if(schoolAdminList!=null && schoolAdminList.size()>0)
									{
										for(UserMaster saObj:schoolAdminList)
										{
											System.out.println("  Send Email to School Admin-========== for add school ==>>>>>>>>>>>============"+saObj.getEmailAddress()+" schoolId "+saObj.getSchoolId().getSchoolName());
											DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
											dsmt.setEmailerService(emailerService);
											dsmt.setMailfrom("ashish.kumar@netsutra.com");
											//dsmt.setMailto("ashish.kumar@netsutra.com");
											dsmt.setMailto(saObj.getEmailAddress());
											dsmt.setMailsubject(saObj.getSchoolId().getSchoolName()+" "+Utility.getLocaleValuePropByKey("msgHasBeenAddedthe", locale)+" "+jobOrder.getJobTitle()+" "+Utility.getLocaleValuePropByKey("lblJobOdr", locale)+"");
											//mailContent = MailText.emailToSchoolAdmins(request,saObj,userMaster,districtMaster,jobTitle);
											mailContent = MailText.emailToSchoolAdminsForAddSchool(request,saObj, jobOrder.getDistrictMaster(), jobOrder.getJobTitle());
											dsmt.setMailcontent(mailContent);
											dsmt.start();
											
											System.out.println("mailContent ::--for Add edit school -----> "+mailContent);
										}
									}
									else
									{
										System.out.println(" school is not in user master table.");
									}
								}
							}
							catch(Exception e)
							{
								e.printStackTrace();
							}
					}
			}
		} catch (NumberFormatException e) {
			e.printStackTrace();
			return "0";
		}
		return "1";
	}
	
	public String  displaySchoolTemp(int JobOrderType)
	{
		System.out.println("calling displaySchoolTemp");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		
		String tempId=session.getId();
		
		StringBuffer tmRecords =	new StringBuffer();
		try{

			UserMaster userMaster = null;
			SchoolMaster schoolMaster=null;
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) {
				return "false";
			}else{
				userMaster=(UserMaster)session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			try{
				if(JobOrderType==2){
					roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,27,"addeditjoborder.do",1);
				}else{
					roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,28,"addeditjoborder.do",2);
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			
			
			//JobOrder jobOrder =jobOrderDAO.findById(jobOrderId,false,false);
			//List<SchoolInJobOrder> listSchoolInJobOrder	= schoolInJobOrderDAO.findJobOrder(jobOrder);
			
			Criterion criterion1 = Restrictions.eq("tempId", tempId);
			
			Map<SchoolMaster, Integer> mapTemp=new HashMap<SchoolMaster, Integer>();
			Map<SchoolMaster, Integer> mapTempReqCount=new HashMap<SchoolMaster, Integer>();
			List<SchoolMaster> schoolMasters=new ArrayList<SchoolMaster>();
			List<Jobrequisitionnumberstemp> jobrequisitionnumberstemps=new ArrayList<Jobrequisitionnumberstemp>();
			jobrequisitionnumberstemps=jobRequisitionNumbersTempDAO.findByCriteria(criterion1);
			if(jobrequisitionnumberstemps.size() > 0)
			{
				for(Jobrequisitionnumberstemp pojo:jobrequisitionnumberstemps)
				{
					if(pojo!=null)
					{
						if(mapTemp.get(pojo.getSchoolMaster())==null)
						{
							mapTemp.put(pojo.getSchoolMaster(), pojo.getNoOfSchoolExpHires());
							schoolMasters.add(pojo.getSchoolMaster());
						}
						
						if(mapTempReqCount.get(pojo.getSchoolMaster())==null)
						{
							if(pojo.getDistrictRequisitionNumbers()!=null || pojo.getReqText()!=null)
								mapTempReqCount.put(pojo.getSchoolMaster(), 1);
							else
								mapTempReqCount.put(pojo.getSchoolMaster(), 0);
						}
						else
						{
							int iTempCount=mapTempReqCount.get(pojo.getSchoolMaster());
							if(pojo.getDistrictRequisitionNumbers()!=null || pojo.getReqText()!=null)
								iTempCount++;
							
							mapTempReqCount.put(pojo.getSchoolMaster(), iTempCount);
						}
						
					}
				}
			}
			int iSchoolCount=schoolMasters.size();
			
			//tmRecords.append("<input type='hidden' id='avlSchoolInJobOrder' value='"+listSchoolInJobOrder.size()+"'>");
			
			if(iSchoolCount > 0)
			{
				if(userMaster!=null && userMaster.getDistrictId()!=null && userMaster.getDistrictId().getDistrictId()!=null && userMaster.getDistrictId().getDistrictId()==804800){
					tmRecords.append("<table id=\"schoolTable\" width=\"100%\" border=\"0\" class=\"table table-bordered span11 mt10\"><thead class=\"bg\"><tr><th>"+Utility.getLocaleValuePropByKey("lblSchoolName", locale)+"</th><th>"+Utility.getLocaleValuePropByKey("lblAddedOn", locale)+"</th><th> "+Utility.getLocaleValuePropByKey("lblOfExpHi", locale)+"</th><th>"+Utility.getLocaleValuePropByKey("msgRequisitionNumbers", locale)+"</th></tr></thead><tbody>");	
				}else{
					tmRecords.append("<table id=\"schoolTable\" width=\"100%\" border=\"0\" class=\"table table-bordered span11 mt10\"><thead class=\"bg\"><tr><th>"+Utility.getLocaleValuePropByKey("lblSchoolName", locale)+"</th><th>"+Utility.getLocaleValuePropByKey("lblAddedOn", locale)+"</th><th> "+Utility.getLocaleValuePropByKey("lblOfExpHi", locale)+"</th><th>"+Utility.getLocaleValuePropByKey("msgRequisitionNumbers", locale)+"</th><th>"+Utility.getLocaleValuePropByKey("lblAct", locale)+"</th></tr></thead><tbody>");
				}
			
			}
			int noOfRecordCheck = 0;
			int countOfReqNo =0;
			String location ="";
			
			//List<Jobrequisitionnumberstemp> jobRequisitionNumbersTemp=new ArrayList<Jobrequisitionnumberstemp>();
			for (SchoolMaster schoolMaster2 : schoolMasters) 
			{
				
				try
				{
					if(schoolMaster2!=null && !schoolMaster2.equals(""))
					{
						if(schoolMaster2.getLocationCode()!=null && schoolMaster2.getLocationCode()!="")
							location =schoolMaster2.getSchoolName()+" ("+schoolMaster2.getLocationCode()+")";
						else
							location = schoolMaster2.getSchoolName();
					}
					
				}catch(Exception e)
				{
					e.printStackTrace();
				}
				
				
				int iNoOfSchoolExpHires=mapTemp.get(schoolMaster2);
				int iReqCount=mapTempReqCount.get(schoolMaster2);
				
				//jobRequisitionNumbersTemp=new ArrayList<Jobrequisitionnumberstemp>();
				noOfRecordCheck++;
				/*Criterion criterion2 = Restrictions.eq("schoolMaster", schoolMaster2.getSchoolId());
				
				jobRequisitionNumbersTemp = jobRequisitionNumbersTempDAO.findByCriteria(criterion1,criterion2);
				
				countOfReqNo = jobRequisitionNumbersTemp.size();*/
				
				tmRecords.append("<tr id='"+schoolMaster2.getSchoolId()+"'>" );
				tmRecords.append("<td nowrap >"+location+"</td>");
				tmRecords.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(new Date())+"</td>");
				tmRecords.append("<td>"+iNoOfSchoolExpHires+"</td>");
				
				//if(jobOrder.getDistrictMaster().getIsReqNoRequired())
				tmRecords.append("<td>"+iReqCount+"</td>");
				
				if(userMaster!=null && userMaster.getDistrictId()!=null && userMaster.getDistrictId().getDistrictId()!=null && userMaster.getDistrictId().getDistrictId()==804800){
									//nothing will display	
				}else{
					tmRecords.append("<td nowrap><a href='javascript:void(0);' onclick='return editSchoolInJobOrderTemp("+schoolMaster2.getSchoolId()+")'><span class='fa-pencil-square-o  icon-large  iconcolor'></span></a>&nbsp;&nbsp;&nbsp;&nbsp;");
					if(userMaster.getEntityType()!=3 && userMaster.getDistrictId().getDistrictId()!=null && userMaster.getDistrictId().getDistrictId()==806900)
					tmRecords.append("<a href='javascript:void(0);' onclick='return deleteSchoolInJobOrderTemp("+schoolMaster2.getSchoolId()+")'><span class='fa-trash-o  icon-large  iconcolor'></span></a></td>");					
				}
				
				int val=0;
				/*try{
					val=teacherAssessmentStatusDAO.findHireStatusByJobAndSchool(jobOrder,schoolJobOrder.getSchoolId());
				}catch(Exception e){
					e.printStackTrace();
				}*/
				if(val==0)
				{/*
					if(userMaster.getEntityType()==3 && JobOrderType==2)
					{
						tmRecords.append("<td nowrap>&nbsp;</td>");
					}
					else
					{
						if(roleAccess.indexOf("|3|")!=-1)
						{
							String sReqNumbers="";
							for(JobRequisitionNumbers pojo :jobRequisitionNumbers)
							{
								sReqNumbers=sReqNumbers+pojo.getDistrictRequisitionNumbers().getDistrictRequisitionId()+"#";
							}
							if(sReqNumbers.length() >1)
							{
								sReqNumbers=sReqNumbers.substring(0,sReqNumbers.length()-1);
							}
							System.out.println("Edit sReqNumbers "+sReqNumbers);
							
							tmRecords.append("<td nowrap><a href='javascript:void(0);' onclick='return editSchoolInJobOrder("+schoolJobOrder.getJobId().getJobId()+","+schoolJobOrder.getSchoolId().getSchoolId()+")'><span class='fa-pencil-square-o  icon-large  iconcolor'></span></a>&nbsp;&nbsp;&nbsp;&nbsp;");
							tmRecords.append("<a href='javascript:void(0);' onclick='return deleteSchoolInJobOrder("+schoolJobOrder.getJobId().getJobId()+","+schoolJobOrder.getSchoolId().getSchoolId()+")'><span class='fa-trash-o  icon-large  iconcolor'></span></a></td>");
							
							tmRecords.append("<td style='display:none;'>"+sReqNumbers+"</td>");
							tmRecords.append("<td style='display:none;'>"+schoolJobOrder.getSchoolId().getSchoolId()+"</td>");
							
						}
						else
						{
							tmRecords.append("<td nowrap>&nbsp;</td>");
						}
					}
				*/}
				else
				{
					tmRecords.append("<td nowrap>&nbsp;</td>");
				}
			}
			if(iSchoolCount > 0)
				tmRecords.append("</table>");
		}catch (Exception e) 
		{
			e.printStackTrace();
		}

		return tmRecords.toString();
	}
	
	
	public String  displaySchoolTemp_NoReq(int JobOrderType)
	{
		System.out.println("calling displaySchoolTemp_NoReq");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		
		String tempId=session.getId();
		
		StringBuffer tmRecords =	new StringBuffer();
		try{

			UserMaster userMaster = null;
			SchoolMaster schoolMaster=null;
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) {
				return "false";
			}else{
				userMaster=(UserMaster)session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			try{
				if(JobOrderType==2){
					roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,27,"addeditjoborder.do",1);
				}else{
					roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,28,"addeditjoborder.do",2);
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			
			Criterion criterion1 = Restrictions.eq("tempId", tempId);
			
			/*Map<SchoolMaster, Integer> mapTemp=new HashMap<SchoolMaster, Integer>();
			Map<SchoolMaster, Integer> mapTempReqCount=new HashMap<SchoolMaster, Integer>();
			List<SchoolMaster> schoolMasters=new ArrayList<SchoolMaster>();*/
			List<Jobrequisitionnumberstemp> jobrequisitionnumberstemps=new ArrayList<Jobrequisitionnumberstemp>();
			jobrequisitionnumberstemps=jobRequisitionNumbersTempDAO.findByCriteria(criterion1);
			if(jobrequisitionnumberstemps.size() > 0)
			{
			
				tmRecords.append("<table id=\"schoolTable\" width=\"100%\" border=\"0\" class=\"table table-bordered span11 mt10\"><thead class=\"bg\"><tr><th>"+Utility.getLocaleValuePropByKey("lblSchoolName", locale)+"</th><th>"+Utility.getLocaleValuePropByKey("lblAddedOn", locale)+"</th><th> "+Utility.getLocaleValuePropByKey("lblOfExpHi", locale)+"</th><th>"+Utility.getLocaleValuePropByKey("lblAct", locale)+"</th></tr></thead><tbody>");
				
				for(Jobrequisitionnumberstemp pojo:jobrequisitionnumberstemps)
				{
					tmRecords.append("<tr id='"+pojo.getSchoolMaster().getSchoolId()+"'>" );
					tmRecords.append("<td nowrap >"+pojo.getSchoolMaster().getSchoolName()+"</td>");
					tmRecords.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(new Date())+"</td>");
					tmRecords.append("<td>"+pojo.getNoOfSchoolExpHires()+"</td>");
					tmRecords.append("<td nowrap><a href='javascript:void(0);' onclick='return editSchoolInJobOrderTemp_NoReq("+pojo.getSchoolMaster().getSchoolId()+")'><span class='fa-pencil-square-o  icon-large  iconcolor'></span></a>&nbsp;&nbsp;&nbsp;&nbsp;");
					tmRecords.append("<a href='javascript:void(0);' onclick='return deleteSchoolInJobOrderTemp_NoReq("+pojo.getSchoolMaster().getSchoolId()+")'><span class='fa-trash-o  icon-large  iconcolor'></span></a></td></TR>");
				}
				tmRecords.append("</table>");
			}
				
		}catch (Exception e) 
		{
			e.printStackTrace();
		}

		return tmRecords.toString();
	}
	
	public boolean deleteSchoolInJobOrderTemp(int schoolId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		try{
			
			String tempId=session.getId();
			
			SchoolMaster schoolMaster = schoolMasterDAO.findById(Long.parseLong(""+schoolId), false, false);
			
			Criterion criterion1 = Restrictions.eq("schoolMaster",schoolMaster);
			Criterion criterion2 = Restrictions.eq("tempId",tempId);
			
			List<Jobrequisitionnumberstemp> jobrequisitionnumberstemps=new ArrayList<Jobrequisitionnumberstemp>();
			jobrequisitionnumberstemps=jobRequisitionNumbersTempDAO.findByCriteria(criterion1,criterion2);
			
			for(Jobrequisitionnumberstemp jobrequisitionnumberstemp:jobrequisitionnumberstemps)
			{
				jobRequisitionNumbersTempDAO.makeTransient(jobrequisitionnumberstemp);
				
			}
			

		}catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public String getRequistionNoByDIDTemp(int districtId)
	{
		//System.out.println("Calling getRequistionNoByDIDTemp districtId "+districtId);
		StringBuffer buffer=new StringBuffer();
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		
		DistrictMaster districtMaster=districtMasterDAO.findById(districtId, false,  false);
		List<DistrictRequisitionNumbers> avlbList = new ArrayList<DistrictRequisitionNumbers>();
		
		Criterion criterion3 = Restrictions.eq("districtMaster", districtMaster);
		Criterion criterion4 = Restrictions.eq("isUsed", false);
		
		String tempId=session.getId();
		List<Integer> districtRequisitionNumbers=new ArrayList<Integer>();
		List<Jobrequisitionnumberstemp> jobrequisitionnumberstemps=new ArrayList<Jobrequisitionnumberstemp>();
		Criterion criterion5 = Restrictions.eq("tempId", tempId);
		jobrequisitionnumberstemps=jobRequisitionNumbersTempDAO.findByCriteria(criterion5);
		for(Jobrequisitionnumberstemp pojo:jobrequisitionnumberstemps)
		{
			if(pojo.getDistrictRequisitionNumbers()!=null)
				districtRequisitionNumbers.add(pojo.getDistrictRequisitionNumbers().getDistrictRequisitionId());
		}
		
		//System.out.println("jobrequisitionnumberstemps "+jobrequisitionnumberstemps.size());
		//System.out.println("districtRequisitionNumbers "+districtRequisitionNumbers.size());
		
		
		Criterion criterion6 = Restrictions.not(Restrictions.in("districtRequisitionId", districtRequisitionNumbers));
		
		if(districtMaster.getIsReqNoRequired())
		{
			//System.out.println("1");
			if(districtRequisitionNumbers.size() ==0)
			{
				//System.out.println("2");
				avlbList = districtRequisitionNumbersDAO.findByCriteria(criterion3,criterion4);
				//System.out.println("avlbList "+avlbList.size());
			}
			else
			{
				//System.out.println("4");
				avlbList = districtRequisitionNumbersDAO.findByCriteria(criterion3,criterion4,criterion6);
				//System.out.println("avlbList "+avlbList.size());
			}
			
			if(avlbList!=null && avlbList.size() > 0)
			{
				for(DistrictRequisitionNumbers pojo:avlbList)
				{
					if(pojo!=null)
					{
						buffer.append("<option value="+pojo.getDistrictRequisitionId()+">"+pojo.getRequisitionNumber()+"</option>");
					}
				}
			}
		}
		
		return buffer.toString();
	}
	
	
	public String editSchoolInJobOrderTemp(long schoolId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		String sReqText="",sSource="";
		StringBuffer sb=new StringBuffer();
		try{
			
			String tempId=session.getId();
			
			SchoolMaster schoolMaster = schoolMasterDAO.findById(Long.parseLong(""+schoolId), false, false);
			
			Criterion criterion1 = Restrictions.eq("schoolMaster",schoolMaster);
			Criterion criterion2 = Restrictions.eq("tempId",tempId);
			
			List<Jobrequisitionnumberstemp> jobrequisitionnumberstemps=new ArrayList<Jobrequisitionnumberstemp>();
			jobrequisitionnumberstemps=jobRequisitionNumbersTempDAO.findByCriteria(criterion1,criterion2);
			
			
			
			int iNoOfSchoolExpHires=0;
			if(jobrequisitionnumberstemps.size() > 0)
			{
				iNoOfSchoolExpHires=jobrequisitionnumberstemps.get(0).getNoOfSchoolExpHires();
			}
				sb.append(schoolMaster.getSchoolId()+"!!"+schoolMaster.getSchoolName()+"##"+iNoOfSchoolExpHires+"###");
				for(Jobrequisitionnumberstemp jbtemp:jobrequisitionnumberstemps)
				{
					if(jbtemp.getDistrictRequisitionNumbers()!=null)
					{
						Integer districtreqId=jbtemp.getDistrictRequisitionNumbers().getDistrictRequisitionId();
						String districtreqNumber=jbtemp.getDistrictRequisitionNumbers().getRequisitionNumber();
						sb.append("<option value='"+districtreqId+"'>"+districtreqNumber+"</option>");
						sSource="2";
					}
					else if(jbtemp.getReqText()!=null && !jbtemp.getReqText().equalsIgnoreCase(""))
					{
						sReqText=sReqText+","+jbtemp.getReqText();
						sSource="1";
					}
						
				}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		if(sReqText!=null && !sReqText.equalsIgnoreCase("") && sReqText.length()>1)
		{
			sReqText=sReqText.substring(1);
			sb.append(sReqText);
			
		}
		sb.append("=="+sSource);
		return sb.toString();
	}
	
	public Jobrequisitionnumberstemp editSchoolInJobOrderTemp_NoReq(long schoolId)
	{
		System.out.println("Callibg editSchoolInJobOrderTemp_NoReq schoolId "+schoolId);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		Jobrequisitionnumberstemp jobrequisitionnumberstemp=new Jobrequisitionnumberstemp();
		try{
			
			String tempId=session.getId();
			SchoolMaster schoolMaster = schoolMasterDAO.findById(Long.parseLong(""+schoolId), false, false);
			
			Criterion criterion1 = Restrictions.eq("schoolMaster",schoolMaster);
			Criterion criterion2 = Restrictions.eq("tempId",tempId);
			
			List<Jobrequisitionnumberstemp> jobrequisitionnumberstemps=new ArrayList<Jobrequisitionnumberstemp>();
			jobrequisitionnumberstemps=jobRequisitionNumbersTempDAO.findByCriteria(criterion1,criterion2);
			
			System.out.println("jobrequisitionnumberstemps Size "+jobrequisitionnumberstemps.size());
			
			if(jobrequisitionnumberstemps.size()==1)
				jobrequisitionnumberstemp=jobrequisitionnumberstemps.get(0);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return jobrequisitionnumberstemp;
	}
	
	public String deleteTempReq()
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		
		String sReturnValue="1";
		try{
			
			String tempId=session.getId();
			Criterion criterion1 = Restrictions.eq("tempId",tempId);
			
			List<Jobrequisitionnumberstemp> jobrequisitionnumberstemps=new ArrayList<Jobrequisitionnumberstemp>();
			jobrequisitionnumberstemps=jobRequisitionNumbersTempDAO.findByCriteria(criterion1);
			
			for(Jobrequisitionnumberstemp jbtemp:jobrequisitionnumberstemps)
			{
				jobRequisitionNumbersTempDAO.makeTransient(jbtemp);
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return sReturnValue;
	}
	
	public String getDistrictReqFlag(int districtId)
	{
		System.out.println("Calling getDistrictReqFlag districtId "+districtId);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		
		String sReturnValue="0";
		try{
			
			DistrictMaster districtMaster= districtMasterDAO.findById(districtId, false, false);
			boolean IsReqNoRequire=districtMaster.getIsReqNoRequired();
			if(IsReqNoRequire)
				sReturnValue="1";
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return sReturnValue;
	}
	
	
	/***************************************************
	 Getting isReqNoForHiring and isReqNoRequired
	 For  hide requisition section on the job order page
	******************************************************/
	public String getHiringRequireFlagValue(int districtId)
	{
		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		
		String isReqNoRequire="0";
		String isReqNoForHiring="0";
		
		try{
			
			DistrictMaster districtMaster= districtMasterDAO.findById(districtId, false, false);
			
			boolean IsReqNoRequire=districtMaster.getIsReqNoRequired();
			
			boolean IsReqNoForHiring=districtMaster.getIsReqNoForHiring();
			
			if(IsReqNoRequire)
				isReqNoRequire="1";
			
			if(IsReqNoForHiring)
				isReqNoForHiring="1";
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return isReqNoRequire+"~"+isReqNoForHiring;
	}
	//End	
	
	
	
	
	public String getZoneList(int districtId)
	{
		StringBuffer getZone=new StringBuffer(); 
		try
		{
			DistrictMaster districtMaster=null;
			
			 districtMaster=districtMasterDAO.findById(districtId,false, false);
				 if(districtMaster!=null && districtMaster.getIsZoneRequired()==1)
				 {
				   	 Criterion criterion=Restrictions.eq("districtMaster",districtMaster);
				   	getZone.append("<option value='0'> "+Utility.getLocaleValuePropByKey("optSltZo", locale)+" </option>");
					 List<GeoZoneMaster> list=geoZoneMasterDAO.findByCriteria(criterion);
						for(GeoZoneMaster g:list)
						{
						getZone.append("<option value='"+g.getGeoZoneId()+"'>"+g.getGeoZoneName()+"</option>");
						}
				 }
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return getZone.toString();
	}

	public String getZoneListAllForEJob(int districtId)
	{
		StringBuffer getZone=new StringBuffer(); 
		try
		{
			DistrictMaster districtMaster=null;
			districtMaster=districtMasterDAO.findById(districtId,false, false);
				 if(districtMaster!=null && districtMaster.getIsZoneRequired()==1)
				 {
				   	 Criterion criterion=Restrictions.eq("districtMaster",districtMaster);
				   	 getZone.append("<option selected value='"+(-1)+"'>"+Utility.getLocaleValuePropByKey("optAll", locale)+"</option>");
					 List<GeoZoneMaster> list=geoZoneMasterDAO.findByCriteria(criterion);
						for(GeoZoneMaster g:list)
						{
						getZone.append("<option value='"+g.getGeoZoneId()+"'>"+g.getGeoZoneName()+"</option>");
						}
				 }
		}catch (Exception e) {
			e.printStackTrace();
		}
		return getZone.toString();
	}

public String getZoneListAll(int districtId)
	{
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	}
	UserMaster userMaster=(UserMaster)session.getAttribute("userMaster");
	Integer entityType=userMaster.getEntityType();
	
	StringBuffer getZone=new StringBuffer(); 
	try
	{
			 SchoolMaster 	schoolMaster=null;
			 DistrictMaster districtMaster=null;
			 Integer geozoneId=null;
			 Criterion criterion=null;
			 if(entityType!=3 ){
				 districtMaster=districtMasterDAO.findById(districtId,false, false);
				 getZone.append("<option selected value='"+(-1)+"'>"+Utility.getLocaleValuePropByKey("optAll", locale)+"</option>");
			 }else if(entityType==3){
				 schoolMaster=userMaster.getSchoolId();
				 districtMaster=schoolMaster.getDistrictId();
				 if(schoolMaster.getGeoZoneMaster()!=null){
					 getZone.append("<option value='"+(-1)+"'>"+Utility.getLocaleValuePropByKey("optAll", locale)+"</option>");
					 getZone.append("<option selected value='"+schoolMaster.getGeoZoneMaster().getGeoZoneId()+"'>"+schoolMaster.getGeoZoneMaster().getGeoZoneName()+"</option>");
					 geozoneId=schoolMaster.getGeoZoneMaster().getGeoZoneId();
				 }else{
					 getZone.append("<option selected value='"+(-1)+"'>"+""+Utility.getLocaleValuePropByKey("optAll", locale)+""+"</option>");
				 }
			 }
			 if(districtMaster!=null && districtMaster.getIsZoneRequired()==1)
			 {
					criterion=Restrictions.eq("districtMaster",districtMaster);
				 	List<GeoZoneMaster> list=geoZoneMasterDAO.findByCriteria(criterion);
			   		for(GeoZoneMaster g:list)
					{
			   			if(!g.getGeoZoneId().equals(geozoneId))
			   			getZone.append("<option value='"+g.getGeoZoneId()+"'>"+g.getGeoZoneName()+"</option>");
					}
			 }
	}catch (Exception e) {
		e.printStackTrace();
	}
	return getZone.toString();
	}

	
public String displayGeoZoneSchoolGrid(Integer geozoneId,Integer districtIdShow,String noOfRow, String pageNo,String sortOrder,String sortOrderType)
	{
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			
			StringBuffer gz=new StringBuffer();
			try{
				
				int noOfRowInPage 	= 	Integer.parseInt(noOfRow);
				int pgNo 			= 	Integer.parseInt(pageNo);
				int start 			= 	((pgNo-1)*noOfRowInPage);
				int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
				int totalRecord		= 	0;
				//------------------------------------
				
				List<SchoolMaster>lstSchoolMasters=new ArrayList<SchoolMaster>(); 
				String sortOrderFieldName	=	"schoolName";
				
				Order  sortOrderStrVal		=	null;
				
				if(!sortOrder.equals("") && !sortOrder.equals(null))
				{
					sortOrderFieldName		=	sortOrder;
				}
				
				String sortOrderTypeVal="0";
				if(!sortOrderType.equals("") && !sortOrderType.equals(null))
				{
					if(sortOrderType.equals("0"))
					{
						sortOrderStrVal		=	Order.asc(sortOrderFieldName);
					}
					else
					{
						sortOrderTypeVal	=	"1";
						sortOrderStrVal		=	Order.desc(sortOrderFieldName);
					}
				}
				else
				{
					sortOrderTypeVal		=	"0";
					sortOrderStrVal			=	Order.asc(sortOrderFieldName);
				}
				
				if(geozoneId!=0)
				{
					GeoZoneMaster geoZoneMaster=geoZoneMasterDAO.findById(geozoneId, false, false);
					Criterion criterion=Restrictions.eq("geoZoneMaster", geoZoneMaster);
					lstSchoolMasters=schoolMasterDAO.findByCriteria(sortOrderStrVal,criterion);
				}
				else
				{
					lstSchoolMasters=schoolMasterDAO.findByCriteria(sortOrderStrVal);
				}
				totalRecord=lstSchoolMasters.size();
				
				if(totalRecord<end)
					end=totalRecord;
				List<SchoolMaster> lstsortedGeoZoneSchools		=	lstSchoolMasters.subList(start,end);
				
				gz.append("<table  id='geozoneSchoolTable' width='100%' border='0' >");
				gz.append("<thead class='bg'>");
				gz.append("<tr>");
				String responseText="";
				responseText=PaginationAndSorting.responseSortingLink(""+Utility.getLocaleValuePropByKey("lblSchoolName", locale)+"",sortOrderFieldName,"schoolName",sortOrderTypeVal,pgNo);
				gz.append("<th width='60%' valign='top'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblAdd", locale),sortOrderFieldName,Utility.getLocaleValuePropByKey("lblAdd", locale),sortOrderTypeVal,pgNo);
				gz.append("<th width='40%' valign='top'>"+responseText+"</th>");
				gz.append("</tr>");
				gz.append("</thead>");
				
				for (SchoolMaster pojo : lstsortedGeoZoneSchools) 
				{
					gz.append("<tr>" );
					gz.append("<td>"+pojo.getSchoolName()+"</td>");
					gz.append("<td>"+pojo.getAddress()+"</td>");
					gz.append("</tr>");
				}
				gz.append("</table>");
				gz.append(PaginationAndSorting.getPaginationForZoneSchool(request,totalRecord,noOfRow, pageNo));
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
	return gz.toString();
  }	

/*	Implemented By Hanzala Subhani Dated 16-06-2014 */
public String displayGeoZoneSchoolPics(Integer geozoneId)
{
		System.out.println("Ajax Successfully deployed");
	
		// Place Image at the place of Grid  hanzala
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		
		StringBuffer gz=new StringBuffer();
		try{
			gz.append("<div style=\"padding-left:183px;\">");
			gz.append("<img alt=\"\" src=\"images/Geo_Zones_With_Schools.jpeg\">");
			gz.append("</div>");
	}catch (Exception e) 
	{
		e.printStackTrace();
	}
return gz.toString();
}

// printing 
public String displayRecordsByEntityTypeforPrinting(boolean resultFlag,int JobOrderType,int districtOrSchoolId,int schoolId,/*String subjectId,String certifications,*/String jobOrderIds,String status,String noOfRow,
		String pageNo,String sortOrder,String sortOrderType,String  disJobReqNo,String geoZonId,String jobCategoryIds,String jobApplicationStatus,String jobSubCategoryIds)
{
	int jobApplicationStatusId=0;
	if(jobApplicationStatus!=null && jobApplicationStatus!="" &&!jobApplicationStatus.equals(""))
	jobApplicationStatusId =  Integer.parseInt(jobApplicationStatus);
	
	int	geoZoneId=0;
	if(geoZonId!=null && geoZonId!="" &&!geoZonId.equals(""))
	  geoZoneId =  Integer.parseInt(geoZonId);
	
	StringBuffer tmRecords = new StringBuffer();
	
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	}
	String fileNames = null;
	try{
		UserMaster userMaster = null;
		Integer entityID=null;
		DistrictMaster districtMaster=null;
		SchoolMaster schoolMaster=null;
		SubjectMaster subjectMaster	=	null;
		int roleId=0;
		if (session == null || session.getAttribute("userMaster") == null) {
			return "false";
		}else{
			userMaster=(UserMaster)session.getAttribute("userMaster");
			
			if(userMaster.getSchoolId()!=null)
				schoolMaster =userMaster.getSchoolId();
			

			if(userMaster.getDistrictId()!=null){
				districtMaster=userMaster.getDistrictId();
			}

			entityID=userMaster.getEntityType();
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
		}
		String roleAccess=null;
		try{
			if(JobOrderType==3){
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,25,"schooljoborders.do",0);
			}else{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,24,"managejoborders.do",0);
			}
		}catch(Exception e){
			e.printStackTrace();
		}

		/** set default sorting fieldName **/
		String sortOrderFieldName="createdDateTime";
		String sortOrderNoField="jobId";

		boolean deafultFlag=false;
		/**Start set dynamic sorting fieldName **/
		Order  sortOrderStrVal=null;
		if(sortOrder!=null){
			if(!sortOrder.equals("") && !sortOrder.equals(null) && !sortOrder.equals("ofApplicant") && !sortOrder.equals("ofHire")){
				sortOrderFieldName=sortOrder;
				sortOrderNoField=sortOrder;
				deafultFlag=false;
			}else{
				deafultFlag=true;
			}
			if(sortOrder.equals("ofApplicant")){
				sortOrderNoField="ofApplicant";
			}
			if(sortOrder.equals("ofHire")){
				sortOrderNoField="ofHire";
			}
		}
		String sortOrderTypeVal="0";
		sortOrderStrVal=Order.asc(sortOrderFieldName);
		if(sortOrderType!=null){
			if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
				if(sortOrderType.equals("1")){
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}
		}
		if(sortOrderType.equals("")){
			sortOrderStrVal=Order.desc(sortOrderFieldName);
		}

		/**End ------------------------------------**/
		List<JobOrder> lstJobOrder	  =	 new ArrayList<JobOrder>();
		boolean jobOrderIdFlag=false;
		boolean statusFlag=false;

		Criterion criterionStatus=null;
		Criterion criterionJobFilter=null;
		if(!(jobOrderIds.equals("0") || jobOrderIds.equalsIgnoreCase("0"))){
			String [] jobsArr =  jobOrderIds.split(",");
			List<Integer> jobs = new ArrayList<Integer>();
			for(int i=0;i<jobsArr.length;i++)
			{	
				String jobId = jobsArr[i].trim();
				try{if(!jobId.equals(""))
					jobs.add(Integer.parseInt(jobId));
				}catch(Exception e){}
			}
			if(jobs.size()>0)
			{
				jobOrderIdFlag=true;
				criterionJobFilter	= Restrictions.in("jobId",jobs);
			}
		}
		if(status!=null){
			if(!status.equalsIgnoreCase("") && !status.equalsIgnoreCase(null)){
				statusFlag=true;
				criterionStatus=Restrictions.eq("status",status);
			}
		}else{
			statusFlag=false;
		}

		/*Criterion criterionSubject=null;
		if(!subjectId.equalsIgnoreCase(""))
		{
			subjectMaster=	subjectMasterDAO.findById(Integer.parseInt(subjectId), false, false);
			criterionSubject=Restrictions.eq("subjectMaster",subjectMaster);
		}
		*/
		/*************Start *************/
		List<JobOrder> filterJobOrderList = new ArrayList<JobOrder>();

		boolean jobFlag=false;
		List<JobOrder> certJobOrderList	  =	 new ArrayList<JobOrder>();
		
		/*boolean certTypeFlag=false;
		if(certifications!=null && !certifications.equals("") && !certifications.equals("0")){
			certTypeFlag=true;
			certJobOrderList=jobCertificationDAO.findCertificationByJob(certifications);
		}
		if(certTypeFlag && certJobOrderList.size()>0){
			if(jobFlag){
				filterJobOrderList.retainAll(certJobOrderList);
			}else{
				filterJobOrderList.addAll(certJobOrderList);
			}
		}
		if(certTypeFlag){
			jobFlag=true;
		}*/

		
		/*List<JobOrder> subjectJList	  =	 new ArrayList<JobOrder>();
		boolean subjectFlag=false;
		if(subjectId!=null && !subjectId.equals("") && !subjectId.equals("0")){
			SubjectMaster sMaster=null;
			if(subjectId!=null){
				sMaster=subjectMasterDAO.findById(Integer.parseInt(subjectId),false,false);
			}
			subjectFlag=true;
			subjectJList=jobOrderDAO.findJobBySubject(sMaster);
		}
		if(subjectFlag && subjectJList.size()>0){
			if(jobFlag){
				filterJobOrderList.retainAll(subjectJList);
			}else{
				filterJobOrderList.addAll(subjectJList);
			}
		}
		if(subjectFlag){
			jobFlag=true;
		}*/

		DistrictMaster  distMaster=null;
		List<JobOrder> districtJList	  =	 new ArrayList<JobOrder>();
		boolean districtFlag=false;
		if(districtOrSchoolId!=0){
			if(districtOrSchoolId!=0){
				distMaster=districtMasterDAO.findById(districtOrSchoolId,false,false);
			}
			districtFlag=true;
			districtJList=jobOrderDAO.findJobByDistrict(distMaster);
		}
		if(districtFlag && districtJList.size()>0){
			if(jobFlag){
				filterJobOrderList.retainAll(districtJList);
			}else{
				filterJobOrderList.addAll(districtJList);
			}
		}
		if(districtFlag){
			jobFlag=true;
		}
		
		/*List<JobOrder> listjJobOrders = new ArrayList<JobOrder>();
		boolean jobCateFlag=false;
		if(jobCategoryIds!=null && !jobCategoryIds.equals("") && !jobCategoryIds.equals("0"))
		{
			List<Integer> jobcatIds = new ArrayList<Integer>();
			String jobCategoryIdStr[] =jobCategoryIds.split(",");
		  if(!ArrayUtils.contains(jobCategoryIdStr, "0"))
		  {		  
			for(String str : jobCategoryIdStr){
			 if(!str.equals(""))
				 jobcatIds.add(Integer.parseInt(str));
			 } 
			 if(jobcatIds.size()>0){
				 listjJobOrders = jobOrderDAO.findByJobCategery(jobcatIds,districtOrSchoolId);
				 jobCateFlag=true;
			 }
			 if(listjJobOrders!=null && listjJobOrders.size()>0){
					if(jobCateFlag){
						filterJobOrderList.retainAll(listjJobOrders);
					}else{
						filterJobOrderList.addAll(listjJobOrders);
					}
				 }
			 }
		  }

		if(jobCateFlag){
			jobFlag=true;
		}*/
		
		
		

		if(jobSubCategoryIds!=null && !jobSubCategoryIds.equals("") && !jobSubCategoryIds.equals("0"))
					  {
						//Job Sub Category
							List<JobOrder> listJobOrdersBySubCatgry = new ArrayList<JobOrder>();
							boolean jobSubCateFlag=false;
							
							if(jobSubCategoryIds!=null && !jobSubCategoryIds.equals("") && !jobSubCategoryIds.equals("0"))
							{
								List<Integer> jobSubcatIds = new ArrayList<Integer>();
								
								String jobSubCategoryIdStr[] = jobSubCategoryIds.split(",");
							  if(!ArrayUtils.contains(jobSubCategoryIdStr, "0"))
							  {		  
								for(String str : jobSubCategoryIdStr){
								 if(!str.equals(""))
									 jobSubcatIds.add(Integer.parseInt(str));
								 } 
								 if(jobSubcatIds.size()>0){
									 listJobOrdersBySubCatgry = jobOrderDAO.findByJobCategery(jobSubcatIds,districtOrSchoolId);
									 System.out.println("  job list by jobcategory :: "+listJobOrdersBySubCatgry.size());
									 jobSubCateFlag=true;
								 }
								 if(jobSubCateFlag){
									filterJobOrderList.retainAll(listJobOrdersBySubCatgry);
								 }else{
									filterJobOrderList.addAll(listJobOrdersBySubCatgry);
								 }
								}
							  }

							if(jobSubCateFlag){
								jobFlag=true;
							}
					  }
					  else
					  {
						// Job category
							List<JobOrder> listjJobOrders = new ArrayList<JobOrder>();
							boolean jobCateFlag=false;
							if(jobCategoryIds!=null && !jobCategoryIds.equals("") && !jobCategoryIds.equals("0"))
							{
								List<Integer> jobcatIds = new ArrayList<Integer>();
								
								String jobCategoryIdStr[] =jobCategoryIds.split(",");
							  if(!ArrayUtils.contains(jobCategoryIdStr, "0"))
							  {		  
								for(String str : jobCategoryIdStr){
								 if(!str.equals(""))
									 jobcatIds.add(Integer.parseInt(str));
								 } 
								 if(jobcatIds.size()>0){
									 listjJobOrders = jobOrderDAO.findByJobCategery(jobcatIds,districtOrSchoolId);
									 System.out.println("  job list by jobcategory :: "+listjJobOrders.size());
									 jobCateFlag=true;
								 }
								 if(jobCateFlag){
									filterJobOrderList.retainAll(listjJobOrders);
								 }else{
									filterJobOrderList.addAll(listjJobOrders);
								 }
								}
							  }

							if(jobCateFlag){
								jobFlag=true;
							}
						  
					  }
		

		Criterion criterionJobOrderType = Restrictions.eq("createdForEntity",JobOrderType);
		List<JobOrder> schoolJList	  =	 new ArrayList<JobOrder>();
		boolean schoolFlag=false;
		int searchschoolflag=2;
		if(schoolId!=0){
			SchoolMaster  sclMaster=null;
			if(districtOrSchoolId!=0){
				sclMaster=schoolMasterDAO.findById(Long.parseLong(schoolId+""),false,false);
			}
			schoolFlag=true;
			searchschoolflag=1;
			if(distMaster!=null)
				schoolJList=schoolInJobOrderDAO.findJobListBySchool(sclMaster,distMaster,JobOrderType,entityID,searchschoolflag,userMaster);
		}
		else
		{
			if(userMaster.getEntityType()==3 && JobOrderType==2 && userMaster.getDistrictId()!=null && distMaster.getWritePrivilegeToSchool().equals(true))
			{
				searchschoolflag=0; // below [ schoolMaster ] is sending from school id by getting  session 
				schoolJList=schoolInJobOrderDAO.findJobListBySchool(schoolMaster,distMaster,JobOrderType,entityID,searchschoolflag,userMaster);
				schoolFlag=true;
			}
		}
		if(schoolFlag && schoolJList.size()>0){
			if(jobFlag){
				filterJobOrderList.retainAll(schoolJList);
			}else{
				filterJobOrderList.addAll(schoolJList);
			}
		}else if(schoolFlag && schoolJList.size()==0){
			filterJobOrderList = new ArrayList<JobOrder>();
		}

		if(schoolFlag){
			jobFlag=true;
		}
		
		
		boolean jobReqnoFlag=false;
		List<JobOrder> jobOrderOfReqNoList	  =	 new ArrayList<JobOrder>();
		if(disJobReqNo!=null && !disJobReqNo.equals("") && !disJobReqNo.equals("0")){
			jobReqnoFlag=true;
			List<DistrictRequisitionNumbers> districtRequisitionNumbers=null;
			Criterion crDrq=Restrictions.eq("requisitionNumber", disJobReqNo);
			Criterion crDSTrq=null;
			if(districtOrSchoolId!=0){
				crDSTrq=Restrictions.eq("districtMaster", distMaster);
				districtRequisitionNumbers=districtRequisitionNumbersDAO.findByCriteria(crDSTrq,crDrq);
			}else{
				districtRequisitionNumbers=districtRequisitionNumbersDAO.findByCriteria(crDrq);
			}
			if(districtRequisitionNumbers!=null && districtRequisitionNumbers.size()>0){
				
				Criterion crJrq=Restrictions.in("districtRequisitionNumbers", districtRequisitionNumbers);
				List<JobRequisitionNumbers> jobRequisitionNumbers=jobRequisitionNumbersDAO.findByCriteria(crJrq);
				for(JobRequisitionNumbers jqr:jobRequisitionNumbers){
					jobOrderOfReqNoList.add(jqr.getJobOrder());
				}
			}
		}
		
		if(jobReqnoFlag && jobOrderOfReqNoList.size()>0){
			if(jobFlag){
				filterJobOrderList.retainAll(jobOrderOfReqNoList);
			}else{
				filterJobOrderList.addAll(jobOrderOfReqNoList);
			}
		}else if(jobReqnoFlag && jobOrderOfReqNoList.size()==0){
			
			filterJobOrderList = new ArrayList<JobOrder>();
		}
		
		if(jobReqnoFlag){
			jobFlag=true;
		}


		// =============  Deepak filtering basis on job Status  added by 27-05-2015
		if(jobApplicationStatusId>0 && distMaster!=null && distMaster.getDistrictId().toString().equalsIgnoreCase("804800"))
		{
			boolean jobApplicationStatusFlag = false;
				List<JobOrder> jobApplicationStatusList = new ArrayList<JobOrder>();
					if (disJobReqNo != null && jobApplicationStatusId > 0) {
						jobApplicationStatusFlag = true;
						jobApplicationStatusList = jobOrderDAO.getJobApplicationStatusRole(jobApplicationStatusId);
						System.out.println("jobApplicationStatusList========"+ jobApplicationStatusList.size());
	
					}
					if (jobApplicationStatusFlag
							&& jobApplicationStatusList.size() > 0) {
						if (jobFlag == true) {
							filterJobOrderList.retainAll(jobApplicationStatusList);
						} else {
							filterJobOrderList.addAll(jobApplicationStatusList);
						}
					} else {
						filterJobOrderList = new ArrayList<JobOrder>();
					}
			if (jobApplicationStatusFlag) {
				jobFlag = true;
			}
		}
		
		
		List<JobOrder> listjobOrdersgeoZone =new ArrayList<JobOrder>();
		
		  if(geoZoneId>0)
			{
			  GeoZoneMaster geoZoneMaster=geoZoneMasterDAO.findById(geoZoneId, false, false);
				if(userMaster.getRoleId().getRoleId().equals(3))
					listjobOrdersgeoZone = jobOrderDAO.getJobOrderBygeoZoneRole(geoZoneMaster,userMaster);
				else
					listjobOrdersgeoZone = jobOrderDAO.getJobOrderBygeoZone(geoZoneMaster);
			}
		 
		  if(listjobOrdersgeoZone!=null && listjobOrdersgeoZone.size()>0)
		  {
			  if(jobFlag==true){
				  filterJobOrderList.retainAll(listjobOrdersgeoZone);
				}else{
					filterJobOrderList.addAll(listjobOrdersgeoZone);
				}
		  }	
		  if(listjobOrdersgeoZone.size()==0 && geoZoneId==-1 ||geoZoneId==0)
		  {
			  filterJobOrderList.addAll(listjobOrdersgeoZone);
		  
		  }
		  else if(listjobOrdersgeoZone.size()==0 &&geoZoneId!=-1)
		  {
			  filterJobOrderList.retainAll(listjobOrdersgeoZone);
		  }
			
	      if(jobFlag){
			if(filterJobOrderList.size()>0){
				List jobIds = new ArrayList();
				for(int e=0; e<filterJobOrderList.size(); e++) {
					jobIds.add(filterJobOrderList.get(e).getJobId());
				}
				Criterion criterionJobList = Restrictions.in("jobId",jobIds);

				Criterion criterionMix=criterionJobOrderType;
				if(statusFlag && jobOrderIdFlag){
					criterionMix=Restrictions.and(criterionStatus,criterionJobFilter);
				}else if(statusFlag){
					criterionMix=criterionStatus;
				}else if(jobOrderIdFlag){
					criterionMix=criterionJobFilter;
				}
				lstJobOrder=jobOrderDAO.findByCriteria(sortOrderStrVal,criterionJobList,criterionJobOrderType,criterionMix);
			}
		}else{
			Criterion criterionMix=criterionJobOrderType;
			if(statusFlag && jobOrderIdFlag){
				criterionMix=Restrictions.and(criterionStatus,criterionJobFilter);
			}else if(statusFlag){
				criterionMix=criterionStatus;
			}else if(jobOrderIdFlag){
				criterionMix=criterionJobFilter;
			}
			lstJobOrder=jobOrderDAO.findByCriteria(sortOrderStrVal,criterionJobOrderType,criterionMix);
			
		}
		/***************************** End Code **************************************/
	      
	    //configurable coloumn
	    Map<String,Boolean> displayCNFGColumn=null;
	    if(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==2){
	    	displayCNFGColumn=getCNFGColumn(districtMaster,true);
	    }
	    else
	    	displayCNFGColumn=getCNFGColumn(districtMaster,false);
		    
		Map<Integer,String> map = new HashMap<Integer, String>();
		if(lstJobOrder!=null && lstJobOrder.size()>0)
		 map = jobForTeacherDAO.countApplicantsByJobOrdersAllll(lstJobOrder);
		
		if(JobOrderType==2)
		 tmRecords.append("<div style='text-align: center; font-size: 25px;  font-weight:bold;'>District "+Utility.getLocaleValuePropByKey("lblJobOdr", locale)+"s</div><br/>");
		else if(JobOrderType==3) 
		 tmRecords.append("<div style='text-align: center; font-size: 25px; font-weight:bold;'>School "+Utility.getLocaleValuePropByKey("lblJobOdr", locale)+"s</div><br/>");
		
		tmRecords.append("<div style='width:100%'>");
		  tmRecords.append("<div style='width:50%; float:left; font-size: 15px; '> "+"Created by: "+userMaster.getFirstName() +" "+ userMaster.getLastName()+"</div>");
		  tmRecords.append("<div style='width:50%; float:right; font-size: 15px; text-align: right; '>"+Utility.getLocaleValuePropByKey("msgDateOfPrint", locale)+": "+Utility.convertDateAndTimeToUSformatOnlyDate(new Date())+"</div>");
		  tmRecords.append("<br/><br/>");
	    tmRecords.append("</div>");
		 
		tmRecords.append("<table  id='' width='100%' border='0'>");
		tmRecords.append("<thead class='bg' style='display: table-header-group; '>");
		tmRecords.append("<tr>");
		
		if(userMaster.getDistrictId()==null || displayCNFGColumn.get("JID")){
		tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+Utility.getLocaleValuePropByKey("lblJobId", locale)+"</th>");
		}
		
//Start Sandeep 03-09-15		
		
		  Boolean hrIntegratedFlag = false;
	      if(userMaster.getDistrictId() != null)
	    	  hrIntegratedFlag = userMaster.getDistrictId().getHrIntegrated();
		
//end		
		
		
		if(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==2 )
		{
			if(userMaster.getEntityType() == 2 && hrIntegratedFlag)
			  tmRecords.append("<th  valign='top'>"+Utility.getLocaleValuePropByKey("lblReq/Posi", locale)+"</th>");
			else if(userMaster.getEntityType() == 1)
				tmRecords.append("<th  valign='top'>"+Utility.getLocaleValuePropByKey("lblReq/Posi", locale)+"</th>");
		}
		
		if(userMaster.getDistrictId()==null || displayCNFGColumn.get("TIT")){
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+Utility.getLocaleValuePropByKey("lblTitle", locale)+"</th>");
		}
		
		if(userMaster.getDistrictId()==null || displayCNFGColumn.get("SCH")){
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+Utility.getLocaleValuePropByKey("optSchool", locale)+"</th>");
		}
		if(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==2)
		{
			
		}
		else
		{		
			if(userMaster.getDistrictId()==null || displayCNFGColumn.get("ZON")){
				tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+Utility.getLocaleValuePropByKey("lblZone", locale)+"</th>");
			}
			if(userMaster.getDistrictId()==null || displayCNFGColumn.get("SUB")){
				tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+Utility.getLocaleValuePropByKey("lblSub", locale)+"</th>");
			}
		}
		if(userMaster.getDistrictId()==null || displayCNFGColumn.get("STA")){
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+Utility.getLocaleValuePropByKey("lblStatus", locale)+"</th>");
		}

		if(userMaster.getDistrictId()==null || displayCNFGColumn.get("POSU")){
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+Utility.getLocaleValuePropByKey("lblPostedUntil", locale)+"</th>");
		}
		if(userMaster.getDistrictId()==null || displayCNFGColumn.get("POS")){
			tmRecords.append("<th  valign='top'>"+Utility.getLocaleValuePropByKey("msgPositions", locale)+"</th>");
		}
		
		if(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==2 )
		{			
		    if(userMaster.getEntityType() == 2 && hrIntegratedFlag)
			tmRecords.append("<th  valign='top'>"+Utility.getLocaleValuePropByKey("lblHrStatus", locale)+"</th>");
		    else if(userMaster.getEntityType() == 1)
		    tmRecords.append("<th  valign='top'>"+Utility.getLocaleValuePropByKey("lblHrStatus", locale)+"</th>");
		}

		if(userMaster.getDistrictId()==null || displayCNFGColumn.get("APP")){
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+Utility.getLocaleValuePropByKey("lblApplicants", locale)+"</th>");
		}
		if(userMaster.getDistrictId()==null || displayCNFGColumn.get("HPER")){
			tmRecords.append("<th style='text-align:left; font-size:12px;' valign='top'>"+Utility.getLocaleValuePropByKey("lblHires", locale)+"</th>");
		}
		
     	tmRecords.append("</tr>");
		tmRecords.append("</thead>");

		tmRecords.append("<tbody>");
		if(lstJobOrder.size()==0){
			 tmRecords.append("<tr><td colspan='10' align='left' class='net-widget-content'>"+Utility.getLocaleValuePropByKey("msgNorecordfound", locale)+"</td></tr>" );
			}
		
		if(resultFlag){
			
			List<JobRequisitionNumbers> jobRequisitionNumbers=new ArrayList<JobRequisitionNumbers>();
			Map<Integer, JobRequisitionNumbers> hrStatusMap = new HashMap<Integer, JobRequisitionNumbers>();
			if(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==2)
			{				
			jobRequisitionNumbers=jobRequisitionNumbersDAO.getHRStatusByJobId(lstJobOrder);
			System.out.println("jobRequisitionNumbers size:::::"+jobRequisitionNumbers.size());
			if(jobRequisitionNumbers!=null && jobRequisitionNumbers.size()>0)
			{
				for(JobRequisitionNumbers hrstatus: jobRequisitionNumbers)
				{
					System.out.println("hrstatus.getJobOrder().getJobId():::"+hrstatus.getJobOrder().getJobId());
					hrStatusMap.put(hrstatus.getJobOrder().getJobId(), hrstatus);
				
				}
			}
			}
			
		  for (JobOrder jobOrderDetails : lstJobOrder) 
			{
			  
				tmRecords.append("<tr>" );
				if(userMaster.getDistrictId()==null || displayCNFGColumn.get("JID")){
					tmRecords.append("<td style='font-size:11px;'>"+jobOrderDetails.getJobId()+"</td>");
				}
				if(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==2)						
				{
				 if(userMaster.getEntityType() == 2 && hrIntegratedFlag){
						if(hrStatusMap.containsKey(jobOrderDetails.getJobId()))
						{							
						tmRecords.append("<td>"+hrStatusMap.get(jobOrderDetails.getJobId()).getDistrictRequisitionNumbers().getRequisitionNumber()+"</td>");
						}
						else
						{
							tmRecords.append("<td>"+Utility.getLocaleValuePropByKey("optN/A", locale)+"</td>");	
						}
				 }else if(userMaster.getEntityType() == 1){
					    
					 if(hrStatusMap.containsKey(jobOrderDetails.getJobId()))
						{							
						tmRecords.append("<td>"+hrStatusMap.get(jobOrderDetails.getJobId()).getDistrictRequisitionNumbers().getRequisitionNumber()+"</td>");
						}
						else
						{
							tmRecords.append("<td>"+Utility.getLocaleValuePropByKey("optN/A", locale)+"</td>");	
						}
				 }
				}
				
				if(userMaster.getDistrictId()==null || displayCNFGColumn.get("TIT")){
					tmRecords.append("<td style='font-size:11px;'>"+jobOrderDetails.getJobTitle()+"</td>");
				}
				
				if(userMaster.getDistrictId()==null || displayCNFGColumn.get("SCH")){
					String schoolListNames="";
					 if(jobOrderDetails.getSchool()!=null && jobOrderDetails.getSchool().size()>1)
						{
	
							schoolListNames = jobOrderDetails.getSchool().get(0).getSchoolName();
							for(int l=1;l<jobOrderDetails.getSchool().size();l++)
							{
								schoolListNames = schoolListNames+" || "+jobOrderDetails.getSchool().get(l).getSchoolName();
							}
							
						}
						else if(jobOrderDetails.getSchool().size()==1)
							schoolListNames=jobOrderDetails.getSchool().get(0).getSchoolName();
					tmRecords.append("<td style='font-size:11px;'>"+schoolListNames+"</td>");
				}

				if(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==2)
				{
					
				}
				else
				{
					if(userMaster.getDistrictId()==null || displayCNFGColumn.get("ZON")){
							if(jobOrderDetails.getGeoZoneMaster()!=null){
								 tmRecords.append("<td style='font-size:11px;'>"+jobOrderDetails.getGeoZoneMaster().getGeoZoneName()+"</td>");
								}else
								{
									tmRecords.append("<td></td>");
								}
					}
				
					if(userMaster.getDistrictId()==null || displayCNFGColumn.get("SUB")){				
						if(jobOrderDetails.getSubjectMaster()!=null){
							tmRecords.append("<td style='font-size:11px;'>"+jobOrderDetails.getSubjectMaster().getSubjectName()+"</td>");
						}else{
							tmRecords.append("<td></td>");
						}
					}
				}
				
				if(userMaster.getDistrictId()==null || displayCNFGColumn.get("STA")){
					tmRecords.append("<td style='font-size:11px'>");
					if(jobOrderDetails.getStatus().equalsIgnoreCase("A"))
						tmRecords.append("Active ");
					else
						tmRecords.append("Inactive");
					tmRecords.append("</td>");
				}
				
				if(userMaster.getDistrictId()==null || displayCNFGColumn.get("POSU")){
					if(Utility.convertDateAndTimeToUSformatOnlyDate(jobOrderDetails.getJobEndDate()).equals("Dec 25, 2099"))
					tmRecords.append("<td style='font-size:11px'>"+Utility.getLocaleValuePropByKey("lblUntilfilled", locale)+"</td>");
					else
					tmRecords.append("<td style='font-size:11px'>"+Utility.convertDateAndTimeToUSformatOnlyDate(jobOrderDetails.getJobEndDate())+",11:59 PM CST</td>");
				}
				
				if(userMaster.getDistrictId()==null || displayCNFGColumn.get("POS")){
					tmRecords.append("<td>"+(jobOrderDetails.getNoOfExpHires()==null? 0 : jobOrderDetails.getNoOfExpHires())+"</td>");
				}
				
				if(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==2)						
				{				
					if(userMaster.getEntityType() == 2 && hrIntegratedFlag){
						if(hrStatusMap.containsKey(jobOrderDetails.getJobId()))
						{	
							String valueStatus="";
							String statusValue=hrStatusMap.get(jobOrderDetails.getJobId()).getDistrictRequisitionNumbers().getPostionStatus();
							if(statusValue!=null)
							{
							if(statusValue.equals("51"))
							{
								valueStatus="Vacant";
							}else if(statusValue.equals("01"))
							{
								valueStatus="Staffed � Fully";
							}else if(statusValue.equals("02"))
							{
								valueStatus="Staffed � Partially";
							}else if(statusValue.equals("60"))
							{
								valueStatus="Frozen";
							}else if(statusValue.equals("00"))
							{
								valueStatus="Inactive Position";
							}else if(statusValue.equals("PV"))
							{
								valueStatus="Pending Vacant";
							}
							}
						tmRecords.append("<td>"+valueStatus+"</td>");
						}
						else
						{
							tmRecords.append("<td>"+Utility.getLocaleValuePropByKey("optN/A", locale)+"</td>");	
						}
					}else if(userMaster.getEntityType() == 1){
						
						if(hrStatusMap.containsKey(jobOrderDetails.getJobId()))
						{	
							String valueStatus="";
							String statusValue=hrStatusMap.get(jobOrderDetails.getJobId()).getDistrictRequisitionNumbers().getPostionStatus();
							if(statusValue!=null)
							{
							if(statusValue.equals("51"))
							{
								valueStatus="Vacant";
							}else if(statusValue.equals("01"))
							{
								valueStatus="Staffed � Fully";
							}else if(statusValue.equals("02"))
							{
								valueStatus="Staffed � Partially";
							}else if(statusValue.equals("60"))
							{
								valueStatus="Frozen";
							}else if(statusValue.equals("00"))
							{
								valueStatus="Inactive Position";
							}else if(statusValue.equals("PV"))
							{
								valueStatus="Pending Vacant";
							}
							}
						tmRecords.append("<td>"+valueStatus+"</td>");
						}
						else
						{
							tmRecords.append("<td>"+Utility.getLocaleValuePropByKey("optN/A", locale)+"</td>");	
						}
						
					}
				}
				
				
				//Total/available/hired
				String appliedStr = null;
				appliedStr = map.get(jobOrderDetails.getJobId());
				if(userMaster.getDistrictId()==null || displayCNFGColumn.get("APP")){
					 if(appliedStr!=null){
						 tmRecords.append("<td style='font-size:11px'>"+appliedStr.split("##")[0]+"/"+appliedStr.split("##")[1]+"</td>");
					 }else
					 {
						 tmRecords.append("<td style='font-size:11px'>0/0</td>");
					 }
				}
				if(userMaster.getDistrictId()==null || displayCNFGColumn.get("HPER")){
					if(appliedStr!=null)
						tmRecords.append("<td style='font-size:11px'>"+appliedStr.split("##")[2]+"</td>");
					else
						tmRecords.append("<td style='font-size:11px'>0</td>");
				}
			}
		}
		tmRecords.append("</tr>" );
		tmRecords.append("</tbody>");
		tmRecords.append("</table>");
			
	}catch (Exception e) 
	{
		e.printStackTrace();
	}

	return tmRecords.toString();
}

public List<SchoolMaster> getFieldOfSchoolListByZones(int districtIdForSchool,String SchoolName,Integer geozoneId)
{
	//System.out.println(" getFieldOfSchoolListByZones >>>>>>>>>>>>>>>>>>>>>>>>>  "+districtIdForSchool+" "+SchoolName+" geozoneId "+geozoneId);
	
	/* ========  For Session time Out Error =========*/
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	}
	List<SchoolMaster> schoolMasterList =  new ArrayList<SchoolMaster>();
	List<SchoolMaster> fieldOfSchoolList1 = null;
	List<SchoolMaster> fieldOfSchoolList2 = null;
	try 
	{
		Criterion criterion = Restrictions.like("schoolName", SchoolName,MatchMode.START);
		Criterion criForLocation = Restrictions.like("locationCode", SchoolName,MatchMode.START);
		GeoZoneMaster  geoZoneMaster=null;
		Criterion criterionGeo=null;
		if(geozoneId!=0){
			geoZoneMaster = geoZoneMasterDAO.findById(geozoneId, false, false);
			criterionGeo = Restrictions.eq("geoZoneMaster",geoZoneMaster);
		}
		
		//
		boolean chkNum = true;
		try
		{
			 double d = Double.parseDouble(SchoolName);  
		}catch(Exception e)
		{
			chkNum = false;
		}
		
		
		
		System.out.println(geozoneId+"\t"+districtIdForSchool);
		if(districtIdForSchool==0){
			List schoolIds = new ArrayList();
			schoolIds =	districtSchoolsDAO.findSchoolIdAllList();
			Criterion criterionSchoolIds = Restrictions.in("schoolId",schoolIds);
			if(schoolIds.size()>0){
				if(SchoolName.trim()!=null && SchoolName.trim().length()>0){
					
					if(chkNum){
						fieldOfSchoolList1 = schoolMasterDAO.findWithLimit(Order.asc("schoolName"), 0, 25, criForLocation,criterionSchoolIds);
						Criterion criterion3 = Restrictions.ilike("schoolName","% "+SchoolName.trim()+"%" );
						fieldOfSchoolList2 = schoolMasterDAO.findWithLimit(Order.asc("schoolName"), 0, 25,criterion3,criterionSchoolIds);

					}else{
						fieldOfSchoolList1 = schoolMasterDAO.findWithLimit(Order.asc("schoolName"), 0, 25, criterion,criterionSchoolIds,criterionGeo);
						Criterion criterion2 = Restrictions.ilike("schoolName","% "+SchoolName.trim()+"%" );
						fieldOfSchoolList2 = schoolMasterDAO.findWithLimit(Order.asc("schoolName"), 0, 25, criterion2,criterionSchoolIds,criterionGeo);
					}
					schoolMasterList.addAll(fieldOfSchoolList1);
					schoolMasterList.addAll(fieldOfSchoolList2);
					Set<SchoolMaster> setSchool = new LinkedHashSet<SchoolMaster>(schoolMasterList);
					schoolMasterList = new ArrayList<SchoolMaster>(new LinkedHashSet<SchoolMaster>(setSchool));
				}else{
					fieldOfSchoolList1 = schoolMasterDAO.findWithLimit(Order.asc("schoolName"), 0, 25,criterionSchoolIds,criterionGeo);
					schoolMasterList.addAll(fieldOfSchoolList1);
				}
			}
		}else{
			DistrictMaster districtMaster = districtMasterDAO.findById(districtIdForSchool, false, false);
			Criterion criterion2 = Restrictions.eq("districtId",districtMaster);
			List schoolIds = new ArrayList();
			schoolIds =	districtSchoolsDAO.findSchoolIdList(districtMaster);
			
			Criterion criterionSchoolIds = Restrictions.in("schoolId",schoolIds);
			System.out.println("\t"+schoolIds.size());
			if(schoolIds.size()>0){
				if(SchoolName.trim()!=null && SchoolName.trim().length()>0)
				{
					if(chkNum){
						fieldOfSchoolList1 = schoolMasterDAO.findWithLimit(Order.asc("schoolName"), 0, 25, criForLocation,criterion2,criterionSchoolIds);
						Criterion criterion3 = Restrictions.ilike("schoolName","% "+SchoolName.trim()+"%" );
						fieldOfSchoolList2 = schoolMasterDAO.findWithLimit(Order.asc("schoolName"), 0, 25,criterion3,criterion2,criterionSchoolIds);
					}
					else{
						fieldOfSchoolList1 = schoolMasterDAO.findWithLimit(Order.asc("schoolName"), 0, 25, criterion,criterion2,criterionSchoolIds);
						Criterion criterion3 = Restrictions.ilike("schoolName","% "+SchoolName.trim()+"%" );
						fieldOfSchoolList2 = schoolMasterDAO.findWithLimit(Order.asc("schoolName"), 0, 25,criterion3,criterion2,criterionSchoolIds);
					}
					
					schoolMasterList.addAll(fieldOfSchoolList1);
					schoolMasterList.addAll(fieldOfSchoolList2);
					Set<SchoolMaster> setSchool = new LinkedHashSet<SchoolMaster>(schoolMasterList);
					schoolMasterList = new ArrayList<SchoolMaster>(new LinkedHashSet<SchoolMaster>(setSchool));
				}else{
					fieldOfSchoolList1 = schoolMasterDAO.findWithLimit(Order.asc("schoolName"), 0, 25,criterion2,criterionSchoolIds,criterionGeo);
					schoolMasterList.addAll(fieldOfSchoolList1);
				}
			}
			
			if(criterionGeo!=null)
			{
				List<SchoolMaster> schoolMasterListGeo=schoolMasterDAO.findByCriteria(criterionGeo);
				schoolMasterList.retainAll(schoolMasterListGeo);
			}
			
			System.out.println("schoolMasterList    "+schoolMasterList.size());
		}
	} 
	catch (Exception e) 
	{
		e.printStackTrace();
	}
	return schoolMasterList;
}



public String weeklyReportData(Integer districtId)
 {
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	}
	StringBuffer tmRecords =	new StringBuffer();
	try{
		UserMaster userMaster = null;
		Integer entityID=null;
		DistrictMaster districtMaster=null;
		SchoolMaster schoolMaster=null;
		SubjectMaster subjectMaster	=	null;
		int roleId=0;
		if (session == null || session.getAttribute("userMaster") == null) {
			return "false";
		}else{
			userMaster=(UserMaster)session.getAttribute("userMaster");

			if(userMaster.getSchoolId()!=null)
				schoolMaster =userMaster.getSchoolId();

			if(userMaster.getDistrictId()!=null){
				districtMaster=userMaster.getDistrictId();
			}

			entityID=userMaster.getEntityType();
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
		}
	
		
	List<JobForTeacher>	jobForTeacherLists=null;
	DistrictMaster districtMaster2=districtMasterDAO.findById(districtId, false, false);
	jobForTeacherLists=jobForTeacherDAO.findWeeklyJobsByDistrict(districtMaster2);
	List<TeacherDetail> listTeacherDetails = new ArrayList<TeacherDetail>();
	for(JobForTeacher jft:jobForTeacherLists) 
	{
		listTeacherDetails.add(jft.getTeacherId());
	}
	List<TeacherNormScore> listTeacherNormScores=null;
	if(listTeacherDetails.size()>0){
	listTeacherNormScores = teacherNormScoreDAO.findTeacersNormScoresList(listTeacherDetails);
	}	
		
    tmRecords.append("<table  id='abc' name='abc' width='100%' border='1'>");
	tmRecords.append("<thead class='bg' style='display: table-header-group; '>");
	tmRecords.append("<tr>");	
	
	tmRecords.append("<th width='22%' style='text-align: left;'>"+""+Utility.getLocaleValuePropByKey("msgJobName", locale)+""+"</th>");
	
	tmRecords.append("<th width='8%' style='text-align: left;'>"+""+Utility.getLocaleValuePropByKey("msgJobCode", locale)+""+"</th>");

	tmRecords.append("<th width='12%' style='text-align: left;'>"+""+Utility.getLocaleValuePropByKey("lblPostEnDate", locale)+""+"</th>");

	tmRecords.append("<th width='10%' style='text-align: left; ;'>"+""+Utility.getLocaleValuePropByKey("msgCandidateId", locale)+""+"</th>");

	tmRecords.append("<th width='19%' style='text-align: left;'>"+""+Utility.getLocaleValuePropByKey("lblCandidateFirstName", locale)+""+"</th>");

	tmRecords.append("<th width='19' style='text-align: left;'>"+""+Utility.getLocaleValuePropByKey("lblCandidateLastName", locale)+""+"</th>");

	tmRecords.append("<th width='10%' style='text-align: left;'>"+""+Utility.getLocaleValuePropByKey("lblEPINormScore", locale)+""+"</th>");
	
 	tmRecords.append("</tr>");
	tmRecords.append("</thead>");

	tmRecords.append("<tbody>");
	for(JobForTeacher jft:jobForTeacherLists) 
	{
		tmRecords.append("<tr>");	
		
		tmRecords.append("<td  style='text-align: left; font-size:12px;'>"+jft.getJobId().getJobTitle()+"</td>");
		
		tmRecords.append("<td style='text-align: left; font-size:12px;'>"+jft.getJobId().getJobId()+"</td>");

		tmRecords.append("<td style='text-align: left; font-size:12px;'>"+Utility.convertDateAndTimeToUSformatOnlyDate(jft.getJobId().getJobEndDate())+"</td>");

		tmRecords.append("<td  style='text-align: left; font-size:12px;'>"+jft.getTeacherId().getTeacherId()+"</td>");

		tmRecords.append("<td style='text-align: left; font-size:12px;'>"+jft.getTeacherId().getFirstName()+"</td>");

		tmRecords.append("<td style='text-align: left; font-size:12px;'>"+jft.getTeacherId().getLastName()+"</td>");

		boolean  flagForNorm=false;
		for(TeacherNormScore tns:listTeacherNormScores)
		{
			if(jft.getTeacherId().equals(tns.getTeacherDetail()))
			{
				tmRecords.append("<td style='text-align: left; font-size:12px;'>"+tns.getTeacherNormScore()+"</td>");
				flagForNorm=true;
			}
		}
		if(!flagForNorm)
			tmRecords.append("<td style='text-align: left; font-size:12px;'>0</td>");
		
	 	tmRecords.append("</tr>");
	}
	
	tmRecords.append("</tbody>");
	
	
	tmRecords.append("</table>");
	
	}	
	catch (Exception e) 
	{
		e.printStackTrace();
	}	
	return tmRecords.toString();
 }


//Excel weekly Export

public  String generateExcelReport(Integer districtId)
{
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	}
	String fileName = null;
	try{
		UserMaster userMaster = null;
		Integer entityID=null;
		DistrictMaster districtMaster=null;
		SchoolMaster schoolMaster=null;
		SubjectMaster subjectMaster	=	null;
		int roleId=0;
		if (session == null || session.getAttribute("userMaster") == null) {
			return "false";
		}else{
			userMaster=(UserMaster)session.getAttribute("userMaster");

			if(userMaster.getSchoolId()!=null)
				schoolMaster =userMaster.getSchoolId();

			if(userMaster.getDistrictId()!=null){
				districtMaster=userMaster.getDistrictId();
			}

			entityID=userMaster.getEntityType();
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
		}
	List<JobForTeacher>	jobForTeacherLists=null;
	DistrictMaster districtMaster2=districtMasterDAO.findById(districtId, false, false);
	jobForTeacherLists=jobForTeacherDAO.findWeeklyJobsByDistrict(districtMaster2);
	List<TeacherDetail> listTeacherDetails = new ArrayList<TeacherDetail>();
	for(JobForTeacher jft:jobForTeacherLists) 
	{
		listTeacherDetails.add(jft.getTeacherId());
	}
	List<TeacherNormScore> listTeacherNormScores =null;
	if(listTeacherDetails.size()>0){
	 listTeacherNormScores = teacherNormScoreDAO.findTeacersNormScoresList(listTeacherDetails);
	}	
	
	
	
//////////////////////Excel Writing //////////////////////////////
	
	String time = String.valueOf(System.currentTimeMillis()).substring(6);
	//String basePath = request.getRealPath("/")+"/candidate";
	String basePath = request.getSession().getServletContext().getRealPath ("/")+"/report";
	System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ :: "+basePath);
	fileName ="districtjobReport"+time+".xls";

	int k=2;
	File file = new File(basePath);
	if(!file.exists())
		file.mkdirs();

	Utility.deleteAllFileFromDir(basePath);

	file = new File(basePath+"/"+fileName);

	WorkbookSettings wbSettings = new WorkbookSettings();

	wbSettings.setLocale(new Locale("en", "EN"));

	WritableCellFormat timesBoldUnderline;
	WritableCellFormat header;
	WritableCellFormat headerBold;
	WritableCellFormat times;

	WritableWorkbook workbook = Workbook.createWorkbook(file, wbSettings);
	workbook.createSheet("Weekly Report", 0);
	WritableSheet excelSheet = workbook.getSheet(0);

	WritableFont times10pt = new WritableFont(WritableFont.ARIAL, 10);
	// Define the cell format
	times = new WritableCellFormat(times10pt);
	// Lets automatically wrap the cells
	times.setWrap(true);

	// Create create a bold font with unterlines
	WritableFont times20ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 20, WritableFont.BOLD, false);
	WritableFont times10ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD, false);

	timesBoldUnderline = new WritableCellFormat(times20ptBoldUnderline);
	timesBoldUnderline.setAlignment(Alignment.GENERAL);
	// Lets automatically wrap the cells
	timesBoldUnderline.setWrap(true);

	header = new WritableCellFormat(times10ptBoldUnderline);
	headerBold = new WritableCellFormat(times10ptBoldUnderline);
	CellView cv = new CellView();
	cv.setFormat(times);
	cv.setFormat(timesBoldUnderline);
	cv.setAutosize(true);

	header.setBackground(Colour.GRAY_25);
	

	// Write a few headers
	excelSheet.mergeCells(0, 0, 8, 1);
	Label label;
	label = new Label(0, 0, "Candidates Jobs Details", timesBoldUnderline);
	excelSheet.addCell(label);
	excelSheet.mergeCells(0, 3, 8, 3);
	label = new Label(0, 3, "");
	excelSheet.addCell(label);
	excelSheet.getSettings().setDefaultColumnWidth(18);

	
	label = new Label(0, k, Utility.getLocaleValuePropByKey("msgJobName", locale),header); 
	excelSheet.addCell(label);
	label = new Label(1, k, Utility.getLocaleValuePropByKey("msgJobCode", locale),header); 
	excelSheet.addCell(label);
	label = new Label(2, k, Utility.getLocaleValuePropByKey("lblPostEnDate", locale),header); 
	excelSheet.addCell(label);
	label = new Label(3, k, Utility.getLocaleValuePropByKey("msgCandidateId", locale),header); 
	excelSheet.addCell(label);
	label = new Label(4, k, Utility.getLocaleValuePropByKey("lblCandidateFirstName", locale),header); 
	excelSheet.addCell(label);
	label = new Label(5, k, Utility.getLocaleValuePropByKey("lblCandidateLastName", locale),header); 
	excelSheet.addCell(label);
	label = new Label(6, k, Utility.getLocaleValuePropByKey("lblEPINormScore", locale),header); 
	excelSheet.addCell(label);
	k=k+1;
	if(jobForTeacherLists.size()==0)
	{	
		excelSheet.mergeCells(0, k, 8, k);
		label = new Label(0, k, ""+Utility.getLocaleValuePropByKey("msgNorecordfound", locale)+""); 
		excelSheet.addCell(label);
	}	
	 for(JobForTeacher jft:jobForTeacherLists)
	 {
		 ++k;
		 label = new Label(0, k, jft.getJobId().getJobTitle()); 
			excelSheet.addCell(label);
			label = new Label(1, k, Integer.toString(jft.getJobId().getJobId())); 
			excelSheet.addCell(label);
			System.out.println(jft.getJobId().getJobId());
			label = new Label(2, k, Utility.convertDateAndTimeToUSformatOnlyDate(jft.getJobId().getJobEndDate())); 
			excelSheet.addCell(label);
			label = new Label(3, k, Integer.toString(jft.getTeacherId().getTeacherId())); 
			excelSheet.addCell(label);
			label = new Label(4, k, jft.getTeacherId().getFirstName()); 
			excelSheet.addCell(label);
			label = new Label(5, k, jft.getTeacherId().getLastName()); 
			excelSheet.addCell(label);
			boolean  flagForNorm=false;
			for(TeacherNormScore tns:listTeacherNormScores)
			{
				if(jft.getTeacherId().equals(tns.getTeacherDetail()))
				{
					label = new Label(6, k, Integer.toString(tns.getTeacherNormScore())); 
					excelSheet.addCell(label);
					flagForNorm=true;
				}
			}
			if(!flagForNorm){
				label = new Label(6, k, "0"); 
				excelSheet.addCell(label);
			}
					
	 }
	    workbook.write();
		workbook.close();
	}	catch (Exception e) 
	{
		e.printStackTrace();
	}
  return fileName;
}





public  String generateCompleteDistrictReport(Integer districtId)
{
	System.out.println("<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>abc ::  "+districtId);
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	}
	StringBuffer tmRecords =	new StringBuffer();
	try{
		UserMaster userMaster = null;
		Integer entityID=null;
		DistrictMaster districtMaster=null;
		SchoolMaster schoolMaster=null;
		SubjectMaster subjectMaster	=	null;
		int roleId=0;
		if (session == null || session.getAttribute("userMaster") == null) {
			return "false";
		}else{
			userMaster=(UserMaster)session.getAttribute("userMaster");

			if(userMaster.getSchoolId()!=null)
				schoolMaster =userMaster.getSchoolId();

			if(userMaster.getDistrictId()!=null){
				districtMaster=userMaster.getDistrictId();
			}

			entityID=userMaster.getEntityType();
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
		}
		DistrictMaster districtMaster2=districtMasterDAO.findById(districtId, false, false);
	//**************************** code of VishvaNath Sir******************************************************************************************************************
		Map<String, PercentileScoreByJob> mapJobPercentile = new HashMap<String, PercentileScoreByJob>();
		List<JobOrder> jobLst = new ArrayList<JobOrder>();
		 
		jobLst = jobOrderDAO.findJobOrdersbyDistrict(districtMaster2);
		System.out.println("jobLst.size(): "+jobLst.size());
		List<PercentileScoreByJob> lstPercentileScoreByJobs = percentileScoreByJobDAO.findPercentileScoreByJobList(jobLst);
		for(PercentileScoreByJob percentileScoreByJob : lstPercentileScoreByJobs){
			mapJobPercentile.put(percentileScoreByJob.getDomainMaster().getDomainId()+"##"+percentileScoreByJob.getJobOrder().getJobId()+"##"+percentileScoreByJob.getScore(), percentileScoreByJob);
		}			
		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>mapJobPercentile.size(): "+mapJobPercentile.size());
		List<DomainMaster> lstDomain = WorkThreadServlet.domainMastersPDRList;
		Map<String,RawDataForDomain> mapRawDomainScore = new HashMap<String, RawDataForDomain>();
		List<RawDataForDomain> lstRawDataForDomain = null;
		List<TeacherDetail> teacherDetails= new ArrayList<TeacherDetail>();
		
		teacherDetails = jobForTeacherDAO.findUniqueApplicantsbyJobOrders(jobLst);
		
		for(DomainMaster dm: lstDomain){				
			lstRawDataForDomain = rawDataForDomainDAO.findByDomainAndTeachers(dm, teacherDetails);
			for(RawDataForDomain rdm : lstRawDataForDomain){
				mapRawDomainScore.put(rdm.getDomainMaster().getDomainId()+"##"+rdm.getTeacherDetail().getTeacherId(), rdm);
			}
		}
		List<JobForTeacher> lstJobForTeacherList= null;
		lstJobForTeacherList = jobForTeacherDAO.findJFTbyJobOrders(jobLst);
		PercentileScoreByJob pScroreJob = null;
		RawDataForDomain rawDataForDomain= null;
		
	//********************End******************************************************************************************************	
	List<JobForTeacher>	jobForTeacherLists=null;
	
	jobForTeacherLists=jobForTeacherDAO.findTeacersByDistrict(districtMaster2);
		
		System.out.println("@@@@@@@@@@@@@ jobForTeacherLists jobForTeacherLists @@@@@@@ :: "+jobForTeacherLists.size());
	ArrayList<Integer> teacherIds = new ArrayList<Integer>();
	for(JobForTeacher jft:jobForTeacherLists) 
	{
		teacherIds.add(jft.getTeacherId().getTeacherId());
	}
	//TeacherPersonalInfo
	List<TeacherPersonalInfo> listTeacherPersonalInfos = new ArrayList<TeacherPersonalInfo>();
	listTeacherPersonalInfos=teacherPersonalInfoDAO.findByTeacherIds(teacherIds);
	Map<String,TeacherPersonalInfo> mapTeacherPersonalInfo = new HashMap<String, TeacherPersonalInfo>();
	for(TeacherPersonalInfo tpf:listTeacherPersonalInfos)
	 {
		mapTeacherPersonalInfo.put(tpf.getTeacherId().toString(),tpf);
	 }
	
	//StateMaster
	List<TeacherDetail> listTeacherDetails = new ArrayList<TeacherDetail>();
	List<JobOrder> listJoboOrders = new ArrayList<JobOrder>();
	
	
	for(JobForTeacher jft:jobForTeacherLists) 
	{
		listTeacherDetails.add(jft.getTeacherId());
		listJoboOrders.add(jft.getJobId());
	}
	
	// for tfaAffliate
	List<TeacherExperience> listTeacherExperiences = new ArrayList<TeacherExperience>();
	listTeacherExperiences = teacherExperienceDAO.findByTeacherDetailsObj(listTeacherDetails);
	Map<String,TeacherExperience> mapTeacherExp = new HashMap<String, TeacherExperience>();
	for(TeacherExperience te:listTeacherExperiences)
	 {
		mapTeacherExp.put(te.getTeacherId().getTeacherId().toString(),te);
	 }
	
	/*for Teacher norm score*/
    List<TeacherNormScore> listTeacherNormScores =null;
	if(listTeacherDetails.size()>0){
	 listTeacherNormScores = teacherNormScoreDAO.findTeacersNormScoresList(listTeacherDetails);
	}
	
	//for A Score && L/R Schore
    List<TeacherAchievementScore> listTeacherAchievementScores = teacherAchievementScoreDAO.getAchievementScoreListByTDList(listTeacherDetails);
    Map<String,TeacherAchievementScore> mapTeacherAchievementScore = new HashMap<String, TeacherAchievementScore>();
	 	for(TeacherAchievementScore tas:listTeacherAchievementScores)
	 	 {
	 		mapTeacherAchievementScore.put(tas.getTeacherDetail().getTeacherId().toString(),tas);
	 	 }
	
	 /* for fit score */
	List<JobWiseConsolidatedTeacherScore> listJobWiseCTScore = jobWiseConsolidatedTeacherScoreDAO.findbyTeacherDetails(listTeacherDetails);
	Map<String,JobWiseConsolidatedTeacherScore> mapJWCTeacherScore = new HashMap<String, JobWiseConsolidatedTeacherScore>();
 	for(JobWiseConsolidatedTeacherScore jwct:listJobWiseCTScore)
 	 {
 		mapJWCTeacherScore.put(jwct.getTeacherDetail().getTeacherId().toString(),jwct);
 	 }
	
//SCHOOL
	// for Campus/School  
	 List<SchoolInJobOrder> listSchoolInJobOrders = schoolInJobOrderDAO.getSIJO(listJoboOrders);
     Map<String,List<SchoolMaster>> maplistSchoolMaster = new HashMap<String, List<SchoolMaster>>();
     
     for(SchoolInJobOrder sjo:listSchoolInJobOrders)
     {
    	List<SchoolMaster> schools = null; 
    	
    	String jId=sjo.getJobId().getJobId().toString();
    	schools = maplistSchoolMaster.get(jId);
    	if(schools==null)
    	{
    		List<SchoolMaster> listSchoolMasters=new ArrayList<SchoolMaster>();
    		listSchoolMasters.add(sjo.getSchoolId());
    		maplistSchoolMaster.put(jId.toString(), listSchoolMasters);
    	}
    	else
    	{
    		schools.add(sjo.getSchoolId());
    		maplistSchoolMaster.put(jId.toString(), schools);
    	}
     }

	 
	// for Academics Recorcds..
	 
	 List<TeacherAcademics> listTeacherAcademics = teacherAcademicsDAO.findByTeacherDetailsObj(listTeacherDetails);
	 Map<String,List<TeacherAcademics>> maplistTeacherAcademics = new HashMap<String, List<TeacherAcademics>>();
	
	 for(JobForTeacher Jft_1:jobForTeacherLists)	{
	 if(listTeacherAcademics!=null && listTeacherAcademics.size()>0)
		{
			List<TeacherAcademics> teacherAcademicsList = null;
			for(TeacherAcademics ta:listTeacherAcademics)
			{
				if(Jft_1.getTeacherId().getTeacherId().equals(ta.getTeacherId().getTeacherId()))
				{
					if(teacherAcademicsList==null)
					{
						teacherAcademicsList = new ArrayList<TeacherAcademics>();
						teacherAcademicsList.add(ta);
					}else
						teacherAcademicsList.add(ta);
				}
			}
			maplistTeacherAcademics.put(Jft_1.getTeacherId().getTeacherId().toString(), teacherAcademicsList);					
		}
	}	
	
	// for teacher references
	 List<TeacherElectronicReferences> listTeacherElectronicReferences = teacherElectronicReferencesDAO.findByTeacherDetailsObj(listTeacherDetails);
	 Map<String,List<TeacherElectronicReferences>> mapTeacherEleReference = new HashMap<String, List<TeacherElectronicReferences>>();
	 for(JobForTeacher Jft_1:jobForTeacherLists)	{
		 if(listTeacherElectronicReferences!=null && listTeacherElectronicReferences.size()>0)
			{
				List<TeacherElectronicReferences> teacherAcademicsList = null;
				for(TeacherElectronicReferences ter:listTeacherElectronicReferences)
				{
					if(Jft_1.getTeacherId().getTeacherId().equals(ter.getTeacherDetail().getTeacherId()))
					{
						if(teacherAcademicsList==null)
						{
							teacherAcademicsList = new ArrayList<TeacherElectronicReferences>();
							teacherAcademicsList.add(ter);
						}else
							teacherAcademicsList.add(ter);
					}
				}
				mapTeacherEleReference.put(Jft_1.getTeacherId().getTeacherId().toString(), teacherAcademicsList);					
			}
		}	
	 
	// for race
	 Map<String,String> mapRaces =new HashMap<String, String>();
	 List<RaceMaster> lstRaceMasters = raceMasterDAO.findAll();
	 for(RaceMaster rm: lstRaceMasters){
		 mapRaces.put(rm.getRaceId().toString(), rm.getRaceName());
	 }
	 
    tmRecords.append("<table  id='' name='' width='100%' border='1'>");
	tmRecords.append("<thead class='bg' style='display: table-header-group; '>");
	tmRecords.append("<tr>");	
	
	tmRecords.append("<th width='.2%' style='text-align: left;'>"+Utility.getLocaleValuePropByKey("msgCandidateId", locale)+"</th>");
	
	tmRecords.append("<th width='2%' style='text-align: left;'>"+Utility.getLocaleValuePropByKey("lblLname", locale)+"</th>");

	tmRecords.append("<th width='2%' style='text-align: left;'>"+Utility.getLocaleValuePropByKey("lblFname", locale)+"</th>");

	tmRecords.append("<th width='.1%' style='text-align: left; ;'>"+Utility.getLocaleValuePropByKey("lblEmail", locale)+"</th>");

	tmRecords.append("<th width='5%' style='text-align: left;'>"+Utility.getLocaleValuePropByKey("msgAddedToSystem", locale)+"</th>");

	tmRecords.append("<th width='3' style='text-align: left;'>"+Utility.getLocaleValuePropByKey("msgFirstJobApplied", locale)+"</th>");

	tmRecords.append("<th width='5%' style='text-align: left;'>"+Utility.getLocaleValuePropByKey("msgAddedToThis", locale)+Utility.getLocaleValuePropByKey("lblJobOdr", locale)+""+"</th>");
	
	tmRecords.append("<th width='2%' style='text-align: left;'>"+Utility.getLocaleValuePropByKey("msgTFAAffiliate", locale)+"</th>");
	
	tmRecords.append("<th width='3%' style='text-align: left;'>"+Utility.getLocaleValuePropByKey("msgCorpsRegion", locale)+"</th>");
	
	tmRecords.append("<th width='2%' style='text-align: left;'>"+Utility.getLocaleValuePropByKey("msgCorpsYear", locale)+"</th>");
	
	tmRecords.append("<th width='3%' style='text-align: left;'>"+Utility.getLocaleValuePropByKey("msgStateTerritory", locale)+"</th>");
	
	tmRecords.append("<th width='3%' style='text-align: left;'>"+Utility.getLocaleValuePropByKey("lblGend", locale)+"</th>");
	
	tmRecords.append("<th width='3%' style='text-align: left;'>"+Utility.getLocaleValuePropByKey("lblRac", locale)+"</th>");
	
	tmRecords.append("<th width='2%' style='text-align: left;'>"+Utility.getLocaleValuePropByKey("lblEPINormScore", locale)+"</th>");
	
	tmRecords.append("<th width='2%' style='text-align: left;'>"+Utility.getLocaleValuePropByKey("lblTFA", locale)+"</th>");
	
	tmRecords.append("<th width='2%' style='text-align: left;'>"+Utility.getLocaleValuePropByKey("lblYOfTeachExp", locale)+"</th>");
	
	tmRecords.append("<th width='2%' style='text-align: left;'>"+Utility.getLocaleValuePropByKey("lblJobId", locale)+"</th>");
	
	tmRecords.append("<th width='8.9%' style='text-align: left;'>"+""+Utility.getLocaleValuePropByKey("lblJobOdr", locale)+Utility.getLocaleValuePropByKey("lblTitle", locale)+"</th>");
	
	tmRecords.append("<th width='2.5%' style='text-align: left;'>"+Utility.getLocaleValuePropByKey("lblCandStatus", locale)+"</th>");
	
	tmRecords.append("<th width='2%' style='text-align: left;'>"+Utility.getLocaleValuePropByKey("lblJoStatus", locale)+"</th>");
	
	tmRecords.append("<th width='1%' style='text-align: left;'>"+""+Utility.getLocaleValuePropByKey("lblHired", locale)+"?"+"</th>");
	
	tmRecords.append("<th width='3%' style='text-align: left;'>"+Utility.getLocaleValuePropByKey("msgDateofHire", locale)+"</th>");
	
	tmRecords.append("<th width='1%' style='text-align: left;'>"+Utility.getLocaleValuePropByKey("msgDistrictEmployee3", locale)+"</th>");
	
	tmRecords.append("<th width='2%' style='text-align: left;'>"+Utility.getLocaleValuePropByKey("lblAScore", locale)+"</th>");
	
	tmRecords.append("<th width='2%' style='text-align: left;'>"+Utility.getLocaleValuePropByKey("lblL/RScore", locale)+"</th>");
	
	tmRecords.append("<th width='2%' style='text-align: left;'>"+Utility.getLocaleValuePropByKey("lblFitScore", locale)+"</th>");
	
	tmRecords.append("<th width='5%' style='text-align: left;'>"+Utility.getLocaleValuePropByKey("msgCampus/School", locale)+"</th>");
	
	tmRecords.append("<th width='5%' style='text-align: left;'>"+Utility.getLocaleValuePropByKey("msgUndergraduateGradud", locale)+"</th>");
	
	tmRecords.append("<th width='2%' style='text-align: left;'>"+Utility.getLocaleValuePropByKey("msgGPAUG", locale)+"</th>");
	
	tmRecords.append("<th width='2.3%' style='text-align: left;'>"+Utility.getLocaleValuePropByKey("lblDgr", locale)+"</th>");
	
	tmRecords.append("<th width='2%' style='text-align: left;'>"+Utility.getLocaleValuePropByKey("msgDegreeType", locale)+"</th>");
	
	tmRecords.append("<th width='1%' style='text-align: left;'>"+Utility.getLocaleValuePropByKey("msgEPIAttitudinal", locale)+"</th>");
	
	tmRecords.append("<th width='1%' style='text-align: left;'>"+Utility.getLocaleValuePropByKey("msgEPICognitive", locale)+"</th>");
	
	tmRecords.append("<th width='1%' style='text-align: left;'>"+Utility.getLocaleValuePropByKey("msgEPITeaching", locale)+"</th>");
	
	tmRecords.append("<th width='1%' style='text-align: left;'>"+Utility.getLocaleValuePropByKey("lblCompositeScore", locale)+"</th>");
	
    tmRecords.append("<th width='3%' style='text-align: left;'>"+Utility.getLocaleValuePropByKey("msgFirstNameReference", locale)+"</th>");
	
	tmRecords.append("<th width='3%' style='text-align: left;'>"+Utility.getLocaleValuePropByKey("msgLastNameReference", locale)+"</th>");
	
	tmRecords.append("<th width='3%' style='text-align: left;'>"+Utility.getLocaleValuePropByKey("msgDesignationReference", locale)+"</th>");
	
	tmRecords.append("<th width='2%' style='text-align: left;'>"+Utility.getLocaleValuePropByKey("msgOrganizationReference", locale)+"</th>");
	
	tmRecords.append("<th width='2%' style='text-align: left;'>"+Utility.getLocaleValuePropByKey("msgEmailAddressReference", locale)+"</th>");
	
	tmRecords.append("<th width='2%' style='text-align: left;'>"+Utility.getLocaleValuePropByKey("msgReferenceContactNumber", locale)+"</th>");
	
 	tmRecords.append("</tr>");
	tmRecords.append("</thead>");

	tmRecords.append("<tbody>");
	for(JobForTeacher jft:jobForTeacherLists) 
	{
	  if(jft.getStatus().getStatusId()!=8){
	    tmRecords.append("<tr>");	
		
		tmRecords.append("<td  style='text-align: left; font-size:12px;'>"+jft.getTeacherId().getTeacherId()+"</td>");
		
		tmRecords.append("<td  style='text-align: left;'>"+jft.getTeacherId().getLastName()+"</td>");

		tmRecords.append("<td  style='text-align: left;'>"+jft.getTeacherId().getFirstName()+"</td>");

		tmRecords.append("<td  style='text-align: left; ;'>"+jft.getTeacherId().getEmailAddress()+"</td>");
		
		tmRecords.append("<td style='text-align: left;'>"+jft.getTeacherId().getCreatedDateTime()+"</td>");

		tmRecords.append("<td style='text-align: left;'>"+jft.getCreatedDateTime()+"</td>");

		tmRecords.append("<td style='text-align: left;'>"+jft.getJobId().getJobTitle()+"</td>");
		
		
		if(mapTeacherExp.get(jft.getTeacherId().getTeacherId().toString())!=null)
		{
			
			if(jft.getTeacherId().getTeacherId().toString().equals(mapTeacherExp.get(jft.getTeacherId().getTeacherId().toString()).getTeacherId().getTeacherId().toString()))
			{
				if(mapTeacherExp.get(jft.getTeacherId().getTeacherId().toString()).getTfaAffiliateMaster()!=null)
				tmRecords.append("<td style='text-align: left;'>"+mapTeacherExp.get(jft.getTeacherId().getTeacherId().toString()).getTfaAffiliateMaster().getTfaAffiliateName()+"</td>");
			}
		}
		else
		{
			tmRecords.append("<td style='text-align: left;'></td>");
		}
		
	// for Corps Region

		if(mapTeacherExp.get(jft.getTeacherId().getTeacherId().toString())!=null)
		{
			
			if(jft.getTeacherId().getTeacherId().toString().equals(mapTeacherExp.get(jft.getTeacherId().getTeacherId().toString()).getTeacherId().getTeacherId().toString()))
			{
				if((mapTeacherExp.get(jft.getTeacherId().getTeacherId().toString()).getTfaRegionMaster())!=null)
				tmRecords.append("<td style='text-align: left;'>"+mapTeacherExp.get(jft.getTeacherId().getTeacherId().toString()).getTfaRegionMaster().getTfaRegionName()+"</td>");
				else
				tmRecords.append("<td style='text-align: left;'></td>");
			}
		}
		else
		{
			tmRecords.append("<td style='text-align: left;'></td>");
		}
		
		
		//for Corp year
		if(mapTeacherExp.get(jft.getTeacherId().getTeacherId().toString())!=null)
		{
			
			if(jft.getTeacherId().getTeacherId().toString().equals(mapTeacherExp.get(jft.getTeacherId().getTeacherId().toString()).getTeacherId().getTeacherId().toString()))
			{
				if(mapTeacherExp.get(jft.getTeacherId().getTeacherId().toString()).getCorpsYear()!=null)
				tmRecords.append("<td style='text-align: left;'>"+mapTeacherExp.get(jft.getTeacherId().getTeacherId().toString()).getCorpsYear()+"</td>");
				else
				tmRecords.append("<td style='text-align: left;'></td>");
			}
		}
		else
		{
			tmRecords.append("<td style='text-align: left;'></td>");
		}
		
	   
		//State/Territory
		
		if(mapTeacherPersonalInfo.get(jft.getTeacherId().getTeacherId().toString())!=null)
		{
			
			if(jft.getTeacherId().getTeacherId().toString().equals(mapTeacherPersonalInfo.get(jft.getTeacherId().getTeacherId().toString()).getTeacherId().toString()))
			{
			 if(mapTeacherPersonalInfo.get(jft.getTeacherId().getTeacherId().toString()).getStateId()!=null)
			 tmRecords.append("<td style='text-align: left;'>"+mapTeacherPersonalInfo.get(jft.getTeacherId().getTeacherId().toString()).getStateId().getStateName()+"</td>");
			}
		}
		else
		{
			tmRecords.append("<td style='text-align: left;'></td>");
		}
		
		
	// Gender	
     if(mapTeacherPersonalInfo.get(jft.getTeacherId().getTeacherId().toString())!=null)
		{
			if(jft.getTeacherId().getTeacherId().toString().equals(mapTeacherPersonalInfo.get(jft.getTeacherId().getTeacherId().toString()).getTeacherId().toString()))
			{
				if(mapTeacherPersonalInfo.get(jft.getTeacherId().getTeacherId().toString()).getGenderId()!=null)
			    tmRecords.append("<td style='text-align: left;'>"+mapTeacherPersonalInfo.get(jft.getTeacherId().getTeacherId().toString()).getGenderId().getGenderName()+"</td>");
				else
				tmRecords.append("<td style='text-align: left;'></td>");
			}
		}
		else
		{
			tmRecords.append("<td style='text-align: left;'></td>");
		}
	
     //mukeshRace 
     //mapRaces
     if(mapTeacherPersonalInfo.get(jft.getTeacherId().getTeacherId().toString())!=null)
		{
			if(jft.getTeacherId().getTeacherId().toString().equals(mapTeacherPersonalInfo.get(jft.getTeacherId().getTeacherId().toString()).getTeacherId().toString()))
			{
				String raceName=null;
				if(mapTeacherPersonalInfo.get(jft.getTeacherId().getTeacherId().toString()).getRaceId()!=null){
					String[] raceIDS=mapTeacherPersonalInfo.get(jft.getTeacherId().getTeacherId().toString()).getRaceId().split(",");
					
				  	   for(int i=0;i<raceIDS.length;i++){
						   if(raceName==null){
								raceName=mapRaces.get(raceIDS[i]);
							}
							else{
								raceName=raceName+","+mapRaces.get(raceIDS[i]);
							}
					}
					
			    tmRecords.append("<td style='text-align: left;'>"+raceName+"</td>");
				}else
				tmRecords.append("<td style='text-align: left;'></td>");
			}
		}
		else
		{
			tmRecords.append("<td style='text-align: left;'></td>");
		}
		
	// for Norm Score	
		boolean  flagForNorm=false;
		for(TeacherNormScore tns:listTeacherNormScores)
		{
			if(jft.getTeacherId().equals(tns.getTeacherDetail()))
			{
				tmRecords.append("<td style='text-align: left;'>"+tns.getTeacherNormScore()+"</td>");
				flagForNorm=true;
			}
		}
		if(!flagForNorm){
			tmRecords.append("<td style='text-align: left;'>0</td>");
		}
		
		// for TFA
		
		if(mapTeacherExp.get(jft.getTeacherId().getTeacherId().toString())!=null)
		{
			
			if(jft.getTeacherId().getTeacherId().toString().equals(mapTeacherExp.get(jft.getTeacherId().getTeacherId().toString()).getTeacherId().getTeacherId().toString()))
			{
				if(mapTeacherExp.get(jft.getTeacherId().getTeacherId().toString()).getTfaAffiliateMaster()!=null)
				tmRecords.append("<td style='text-align: left;'>"+mapTeacherExp.get(jft.getTeacherId().getTeacherId().toString()).getTfaAffiliateMaster().getTfaShortAffiliateName()+"</td>");
			}
		}
		else
		{
			tmRecords.append("<td style='text-align: left;'></td>");
		}
			
		// Teaching Experience
		if(mapTeacherExp.get(jft.getTeacherId().getTeacherId().toString())!=null)
		{
			
			if(jft.getTeacherId().getTeacherId().toString().equals(mapTeacherExp.get(jft.getTeacherId().getTeacherId().toString()).getTeacherId().getTeacherId().toString()))
			{
				if(mapTeacherExp.get(jft.getTeacherId().getTeacherId().toString()).getExpCertTeacherTraining()!=null)
				tmRecords.append("<td style='text-align: left;'>"+mapTeacherExp.get(jft.getTeacherId().getTeacherId().toString()).getExpCertTeacherTraining()+"</td>");
				else
				tmRecords.append("<td style='text-align: left;'></td>");
			}
		}
		else
		{
			tmRecords.append("<td style='text-align: left;'></td>");
		}
		
		
		tmRecords.append("<td >"+jft.getJobId().getJobId()+"</td>");
		
		
		tmRecords.append("<td style='text-align: left;'>"+jft.getJobId().getJobTitle()+"</td>");
		
		tmRecords.append("<td  style='text-align: left;'>"+jft.getStatus().getStatus()+"</td>");
		
		//job status
		 Date d1=new Date();
		 Date d2=jft.getJobId().getJobEndDate();
		 if(d1 .before(d2) || d1.compareTo(d2)==0){
			 tmRecords.append("<td  style='text-align: left;'>"+Utility.getLocaleValuePropByKey("msgOpen", locale)+"</td>");
		 }
		 else {
			 tmRecords.append("<td  style='text-align: left;'>"+Utility.getLocaleValuePropByKey("msgClosed", locale)+"</td>");
		 }
		//"+Utility.getLocaleValuePropByKey("lblHired", locale)+"
		if(jft.getStatus().getStatusId()==6)
		tmRecords.append("<td style='text-align: left;'>"+"Yes"+"</td>");
		else
		tmRecords.append("<td style='text-align: left;'>"+"No"+"</td>");
		
		//Date of "+Utility.getLocaleValuePropByKey("lblHired", locale)+"
		if(jft.getStatus().getStatusId()==6 && jft.getLastActivityDate()!=null)
		tmRecords.append("<td  style='text-align: left;'>"+jft.getLastActivityDate()+"</td>");
		else
		tmRecords.append("<td  style='text-align: left;'> </td>");
		
		//Are you district Employee?(Internal)
		if(jft.getIsAffilated()==1)
		tmRecords.append("<td  style='text-align: left;'>"+"Yes"+"</td>");
		else
		tmRecords.append("<td  style='text-align: left;'>"+"No"+"</td>");	
		
		// for A Score
		   if(mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString())!=null)
			{
				if(jft.getTeacherId().getTeacherId().toString().equals(mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getTeacherDetail().getTeacherId().toString()))
				{
					if(!mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getAcademicAchievementScore().equals("") && mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getAcademicAchievementScore()!=null)
				    tmRecords.append("<td style='text-align: left;'>"+mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getAcademicAchievementScore()+"/"+mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getAcademicAchievementMaxScore()+"</td>");
					else
					tmRecords.append("<td style='text-align: left;'></td>");
				}
			}
			else
			{
				tmRecords.append("<td style='text-align: left;'></td>");
			}
	//for L/R Score 
		   if(mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString())!=null)
			{
				if(jft.getTeacherId().getTeacherId().toString().equals(mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getTeacherDetail().getTeacherId().toString()))
				{
					if(!mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getAcademicAchievementScore().equals("") &&  mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getAcademicAchievementScore()!=null )
				    tmRecords.append("<td style='text-align: left;'>"+mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getLeadershipAchievementScore()+"/"+mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getLeadershipAchievementMaxScore()+"</td>");
					else
					tmRecords.append("<td style='text-align: left;'></td>");
				}
			}
			else
			{
				tmRecords.append("<td style='text-align: left;'></td>");
			}
	/// for Fit Score 	
		//mapJWCTeacherScore	
		   
			 if(mapJWCTeacherScore.get(jft.getTeacherId().getTeacherId().toString())!=null)
				{
				  if(jft.getTeacherId().getTeacherId().toString().equals(mapJWCTeacherScore.get(jft.getTeacherId().getTeacherId().toString()).getTeacherDetail().getTeacherId().toString()) && jft.getJobId().getJobId().equals(mapJWCTeacherScore.get(jft.getTeacherId().getTeacherId().toString()).getJobOrder().getJobId()))
					{
					    tmRecords.append("<td style='text-align: left;'>"+mapJWCTeacherScore.get(jft.getTeacherId().getTeacherId().toString()).getJobWiseConsolidatedScore()+"/"+mapJWCTeacherScore.get(jft.getTeacherId().getTeacherId().toString()).getJobWiseMaxScore()+"</td>");
					}else{	
						 tmRecords.append("<td style='text-align: left;'></td>");
						 }
				}
				else
				{
					tmRecords.append("<td style='text-align: left;'></td>");
				}
		// tmRecords.append("<td style='text-align: left;'></td>");
		//Campus/School
	//maplistSchoolMaster
		if(maplistSchoolMaster.get(jft.getJobId().getJobId().toString())!=null)
		{
				List<SchoolMaster> listSchoolMasters=maplistSchoolMaster.get(jft.getJobId().getJobId().toString());
				
				if(listSchoolMasters.size()>1)
				{
				 tmRecords.append("<td>"+Utility.getLocaleValuePropByKey("msgMultipleSchools", locale)+"</td>");
				}	
				else if(listSchoolMasters.size()==1){
						tmRecords.append("<td>"+listSchoolMasters.get(0).getSchoolName()+"</td>");
				}
			}
		
		else
		{
			tmRecords.append("<td style='text-align: left;'></td>");
		}
		                 
	   /************************Academics Records*************************************/		 	
	 	
	 //for Undergraduate/Gradudate/College (College Major)
	 	
	  if(maplistTeacherAcademics.get(jft.getTeacherId().getTeacherId().toString())!=null)
		{
		  int count=1;
			 List<TeacherAcademics> ta=maplistTeacherAcademics.get(jft.getTeacherId().getTeacherId().toString());
				tmRecords.append("<td style='text-align: left;'>");
				 tmRecords.append("<table >");
				 if(ta!=null && ta.size()>0)
					for(TeacherAcademics lstta:ta)
				 	{
						tmRecords.append("<tr>");
						    if(lstta.getUniversityId().getUniversityName()!=null)
				 			tmRecords.append("<td>"+(count++)+") "+lstta.getUniversityId().getUniversityName()+"</td>");
						    else
						    	tmRecords.append("<td ></td>");
				 		tmRecords.append("</tr>");
				 	}
				 tmRecords.append("</table>");
				tmRecords.append("</td>");
		}
		else
		{
			tmRecords.append("<td ></td>");
		}

// CGPA 	
		if(maplistTeacherAcademics.get(jft.getTeacherId().getTeacherId().toString())!=null)
		{
			int count=1;
				List<TeacherAcademics> ta=maplistTeacherAcademics.get(jft.getTeacherId().getTeacherId().toString());
			 tmRecords.append("<td style='text-align: left;'>");
				 tmRecords.append("<table>");
				 if(ta!=null && ta.size()>0)
					for(TeacherAcademics lstta:ta)
				 	{
				 	  tmRecords.append("<tr>");
				 		 if(lstta.getGpaCumulative()!=null)
				 			tmRecords.append("<td>"+(count++)+") "+lstta.getGpaCumulative()+"</td>");
				 		 else	
				 			tmRecords.append("<td></td>");
				 	  tmRecords.append("</tr>");
				 	}
				 tmRecords.append("</table>");
			 tmRecords.append("</td>");
		}
		else
		{
			tmRecords.append("<td ></td>");
		}

		// for degree
		if(maplistTeacherAcademics.get(jft.getTeacherId().getTeacherId().toString())!=null)
		{
			int count=1;
				List<TeacherAcademics> ta=maplistTeacherAcademics.get(jft.getTeacherId().getTeacherId().toString());
			tmRecords.append("<td style='text-align: left;'>");
				 tmRecords.append("<table '>");
				 if(ta!=null && ta.size()>0)
					for(TeacherAcademics lstta:ta)
				 	{
				 	   tmRecords.append("<tr>");
				 		 if(lstta.getDegreeId().getDegreeName()!=null)
				 			tmRecords.append("<td>"+(count++)+") "+lstta.getDegreeId().getDegreeName()+"</td>");
				 		 else	
				 			tmRecords.append("<td></td>");
				 	   tmRecords.append("</tr>");
				 	}
				 tmRecords.append("</table>");
			 tmRecords.append("</td>");
		}
		else
		{
			tmRecords.append("<td ></td>");
		}
		
	// for degree type
		if(maplistTeacherAcademics.get(jft.getTeacherId().getTeacherId().toString())!=null)
		{
			int count=1;
				List<TeacherAcademics> ta=maplistTeacherAcademics.get(jft.getTeacherId().getTeacherId().toString());
			  tmRecords.append("<td style='text-align: left;'>");
				 tmRecords.append("<table '>");
				 if(ta!=null && ta.size()>0)
					for(TeacherAcademics lstta:ta)
				 	{
				 	   tmRecords.append("<tr>");
				 		 if(lstta.getDegreeId().getDegreeType()!=null)
				 		 {
					 			 String degreeType=lstta.getDegreeId().getDegreeType();
					 		       if(degreeType.equals("D")){  
							 	   tmRecords.append("<td>"+(count++)+") "+Utility.getLocaleValuePropByKey("msgDoctorate", locale)+"</td>");
					 		        }
							 	   else if(degreeType.equals("M")){
						 	       tmRecords.append("<td>"+(count++)+") "+Utility.getLocaleValuePropByKey("msgMaster", locale)+"</td>");
							 	   }
							 	  else if(degreeType.equals("B")){
							 	  tmRecords.append("<td>"+(count++)+") "+Utility.getLocaleValuePropByKey("msgBachelor", locale)+"</td>");
							 	  }
							 	 else if(degreeType.equals("A")){
								 tmRecords.append("<td>"+(count++)+") "+Utility.getLocaleValuePropByKey("msgAssociate", locale)+"</td>");
								 }
							 	 else
							 	 tmRecords.append("<td>"+(count++)+") "+"1"+"</td>");	 
				 		 }
				 		 else	
				 			tmRecords.append("<td></td>");
				 	   tmRecords.append("</tr>");
				 	}
				 tmRecords.append("</table>");
			 tmRecords.append("</td>");
		}
		else
		{
			tmRecords.append("<td ></td>");
		}
//removing key from mapList of TeacherAcademics		
		if(maplistTeacherAcademics.get(jft.getTeacherId().getTeacherId().toString())!=null)
			maplistTeacherAcademics.remove(jft.getTeacherId().getTeacherId().toString());
			
/*********** End Academics Records*******************/		
		
		//EPI Attitudinal	EPI Cognitive	EPI Teaching

		for(DomainMaster domain: lstDomain)
		{	
			String key = domain.getDomainId()+"##"+jft.getTeacherId().getTeacherId();
			
			rawDataForDomain =  mapRawDomainScore.get(key);
			//System.out.println(rawDataForDomain);
			if(rawDataForDomain!=null)
			{
				key = domain.getDomainId()+"##"+jft.getJobId().getJobId()+"##"+Math.round(rawDataForDomain.getScore());						
				//System.out.println(key);
				pScroreJob = mapJobPercentile.get(key);
				
				if(pScroreJob!=null)
				{
				 //System.out.println(Math.round(pScroreJob.gettValue()));
			      tmRecords.append("<td width='10%' style='text-align: left;'>"+Math.round(pScroreJob.gettValue())+"</td>");
				}
				else
				{
					//System.out.println("N/A");
					tmRecords.append("<td width='10%' style='text-align: left;'>"+Utility.getLocaleValuePropByKey("optN/A", locale)+"</td>");
				}
			}else{
				//System.out.println(Utility.getLocaleValuePropByKey("optN/A", locale));
				tmRecords.append("<td width='10%' style='text-align: left;'>"+Utility.getLocaleValuePropByKey("optN/A", locale)+"</td>");
			}
		}
	
		
	//	tmRecords.append("<th width='10%' style='text-align: left;'>"+Utility.getLocaleValuePropByKey("msgEPITeaching", locale)+"</th>");
		
/*********************** for references details		**********************/
		//for ref first name
		  if(mapTeacherEleReference.get(jft.getTeacherId().getTeacherId().toString())!=null)
			{
			  int count=1;
				 List<TeacherElectronicReferences> ter=mapTeacherEleReference.get(jft.getTeacherId().getTeacherId().toString());
					tmRecords.append("<td style='text-align: left;'>");
					 tmRecords.append("<table >");
					 if(ter!=null && ter.size()>0)
						for(TeacherElectronicReferences lstter:ter)
					 	{
							tmRecords.append("<tr>");
							    if(lstter.getFirstName()!=null)
					 			tmRecords.append("<td>"+(count++)+") "+lstter.getFirstName()+"</td>");
							    else
							    	tmRecords.append("<td >"+(count++)+"</td>");
					 		tmRecords.append("</tr>");
					 	}
					 tmRecords.append("</table>");
					tmRecords.append("</td>");
			}
			else
			{
				tmRecords.append("<td ></td>");
			}
		
	// for ref last name
		  if(mapTeacherEleReference.get(jft.getTeacherId().getTeacherId().toString())!=null)
			{
			  int count=1;
				 List<TeacherElectronicReferences> ter=mapTeacherEleReference.get(jft.getTeacherId().getTeacherId().toString());
					tmRecords.append("<td style='text-align: left;'>");
					 tmRecords.append("<table >");
					 if(ter!=null && ter.size()>0)
						for(TeacherElectronicReferences lstter:ter)
					 	{
							tmRecords.append("<tr>");
							    if(lstter.getLastName()!=null)
					 			tmRecords.append("<td>"+(count++)+") "+lstter.getLastName()+"</td>");
							    else
							    	tmRecords.append("<td >"+(count++)+"</td>");
					 		tmRecords.append("</tr>");
					 	}
					 tmRecords.append("</table>");
					tmRecords.append("</td>");
			}
			else
			{
				tmRecords.append("<td ></td>");
			}
		  // for designation of reference
		  if(mapTeacherEleReference.get(jft.getTeacherId().getTeacherId().toString())!=null)
			{
			  int count=1;
				 List<TeacherElectronicReferences> ter=mapTeacherEleReference.get(jft.getTeacherId().getTeacherId().toString());
					tmRecords.append("<td style='text-align: left;'>");
					 tmRecords.append("<table >");
					 if(ter!=null && ter.size()>0)
						for(TeacherElectronicReferences lstter:ter)
					 	{
							tmRecords.append("<tr>");
							    if(lstter.getDesignation()!=null)
					 			tmRecords.append("<td>"+(count++)+") "+lstter.getDesignation()+"</td>");
							    else
							    	tmRecords.append("<td >"+(count++)+"</td>");
					 		tmRecords.append("</tr>");
					 	}
					 tmRecords.append("</table>");
					tmRecords.append("</td>");
			}
			else
			{
				tmRecords.append("<td ></td>");
			}
		  
		  // for organization of reference
		  if(mapTeacherEleReference.get(jft.getTeacherId().getTeacherId().toString())!=null)
			{
			  int count=1;
				 List<TeacherElectronicReferences> ter=mapTeacherEleReference.get(jft.getTeacherId().getTeacherId().toString());
					tmRecords.append("<td style='text-align: left;'>");
					 tmRecords.append("<table >");
					 if(ter!=null && ter.size()>0)
						for(TeacherElectronicReferences lstter:ter)
					 	{
							tmRecords.append("<tr>");
							    if(lstter.getOrganization()!=null)
					 			tmRecords.append("<td>"+(count++)+") "+lstter.getOrganization()+"</td>");
							    else
							    	tmRecords.append("<td >"+(count++)+"</td>");
					 		tmRecords.append("</tr>");
					 	}
					 tmRecords.append("</table>");
					tmRecords.append("</td>");
			}
			else
			{
				tmRecords.append("<td ></td>");
			}
		  // for emailId of reference
		  if(mapTeacherEleReference.get(jft.getTeacherId().getTeacherId().toString())!=null)
			{
			  int count=1;
				 List<TeacherElectronicReferences> ter=mapTeacherEleReference.get(jft.getTeacherId().getTeacherId().toString());
					tmRecords.append("<td style='text-align: left;'>");
					 tmRecords.append("<table >");
					 if(ter!=null && ter.size()>0)
						for(TeacherElectronicReferences lstter:ter)
					 	{
							tmRecords.append("<tr>");
							    if(lstter.getFirstName()!=null)
					 			tmRecords.append("<td>"+(count++)+") "+lstter.getEmail()+"</td>");
							    else
							    	tmRecords.append("<td >"+(count++)+"</td>");
					 		tmRecords.append("</tr>");
					 	}
					 tmRecords.append("</table>");
					tmRecords.append("</td>");
			}
			else
			{
				tmRecords.append("<td ></td>");
			}
		  // for Contact Number of reference
		  if(mapTeacherEleReference.get(jft.getTeacherId().getTeacherId().toString())!=null)
			{
			  int count=1;
				 List<TeacherElectronicReferences> ter=mapTeacherEleReference.get(jft.getTeacherId().getTeacherId().toString());
					tmRecords.append("<td style='text-align: left;'>");
					 tmRecords.append("<table >");
					 if(ter!=null && ter.size()>0)
						for(TeacherElectronicReferences lstter:ter)
					 	{
							tmRecords.append("<tr>");
							    if(lstter.getFirstName()!=null)
					 			tmRecords.append("<td>"+(count++)+") "+lstter.getContactnumber()+"</td>");
							    else
							    	tmRecords.append("<td >"+(count++)+"</td>");
					 		tmRecords.append("</tr>");
					 	}
					 tmRecords.append("</table>");
					tmRecords.append("</td>");
			}
			else
			{
				tmRecords.append("<td ></td>");
			}
		//removing key from mapList of TeacherAcademics		
			if(mapTeacherEleReference.get(jft.getTeacherId().getTeacherId().toString())!=null)
				mapTeacherEleReference.remove(jft.getTeacherId().getTeacherId().toString());

		tmRecords.append("</tr>");
	  }		
	}
	
	tmRecords.append("</tbody>");
	tmRecords.append("</table>");
	
	}	
	catch (Exception e) 
	{
		e.printStackTrace();
	}	
	return tmRecords.toString();
}

//exporting
public  String generateExcelReportforDistrict(Integer districtId)
{
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	}
	String fileName = null;
	try{
		UserMaster userMaster = null;
		Integer entityID=null;
		DistrictMaster districtMaster=null;
		SchoolMaster schoolMaster=null;
		SubjectMaster subjectMaster	=	null;
		int roleId=0;
		if (session == null || session.getAttribute("userMaster") == null) {
			return "false";
		}else{
			userMaster=(UserMaster)session.getAttribute("userMaster");

			if(userMaster.getSchoolId()!=null)
				schoolMaster =userMaster.getSchoolId();

			if(userMaster.getDistrictId()!=null){
				districtMaster=userMaster.getDistrictId();
			}

			entityID=userMaster.getEntityType();
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
		}
		DistrictMaster districtMaster2=districtMasterDAO.findById(districtId, false, false);
		//******* code of VishvaNath Sir******************************************************************************************************************
		Map<String, PercentileScoreByJob> mapJobPercentile = new HashMap<String, PercentileScoreByJob>();
		List<JobOrder> jobLst = new ArrayList<JobOrder>();
		
		jobLst = jobOrderDAO.findJobOrdersbyDistrict(districtMaster2);
		System.out.println("jobLst.size(): "+jobLst.size());
		List<PercentileScoreByJob> lstPercentileScoreByJobs = percentileScoreByJobDAO.findPercentileScoreByJobList(jobLst);
		for(PercentileScoreByJob percentileScoreByJob : lstPercentileScoreByJobs){
			mapJobPercentile.put(percentileScoreByJob.getDomainMaster().getDomainId()+"##"+percentileScoreByJob.getJobOrder().getJobId()+"##"+percentileScoreByJob.getScore(), percentileScoreByJob);
		}			
		
		List<DomainMaster> lstDomain = WorkThreadServlet.domainMastersPDRList;
		Map<String,RawDataForDomain> mapRawDomainScore = new HashMap<String, RawDataForDomain>();
		List<RawDataForDomain> lstRawDataForDomain = null;
		List<TeacherDetail> teacherDetails= new ArrayList<TeacherDetail>();
		
		teacherDetails = jobForTeacherDAO.findUniqueApplicantsbyJobOrders(jobLst);
		
		for(DomainMaster dm: lstDomain){				
			lstRawDataForDomain = rawDataForDomainDAO.findByDomainAndTeachers(dm, teacherDetails);
			for(RawDataForDomain rdm : lstRawDataForDomain){
				mapRawDomainScore.put(rdm.getDomainMaster().getDomainId()+"##"+rdm.getTeacherDetail().getTeacherId(), rdm);
			}
		}
		List<JobForTeacher> lstJobForTeacherList= null;
		lstJobForTeacherList = jobForTeacherDAO.findJFTbyJobOrders(jobLst);
		PercentileScoreByJob pScroreJob = null;
		RawDataForDomain rawDataForDomain= null;
		
	//********************End******************************************************************************************************	
	List<JobForTeacher>	jobForTeacherLists=null;
	
	jobForTeacherLists=jobForTeacherDAO.findTeacersByDistrict(districtMaster2);
		
		
	ArrayList<Integer> teacherIds = new ArrayList<Integer>();
	for(JobForTeacher jft:jobForTeacherLists) 
	{
		teacherIds.add(jft.getTeacherId().getTeacherId());
	}
	//TeacherPersonalInfo
	List<TeacherPersonalInfo> listTeacherPersonalInfos = new ArrayList<TeacherPersonalInfo>();
	listTeacherPersonalInfos=teacherPersonalInfoDAO.findByTeacherIds(teacherIds);
	Map<String,TeacherPersonalInfo> mapTeacherPersonalInfo = new HashMap<String, TeacherPersonalInfo>();
	for(TeacherPersonalInfo tpf:listTeacherPersonalInfos)
	 {
		mapTeacherPersonalInfo.put(tpf.getTeacherId().toString(),tpf);
	 }
	
	//StateMaster
	List<TeacherDetail> listTeacherDetails = new ArrayList<TeacherDetail>();
	List<JobOrder> listJoboOrders = new ArrayList<JobOrder>();
	
	
	for(JobForTeacher jft:jobForTeacherLists) 
	{
		listTeacherDetails.add(jft.getTeacherId());
		listJoboOrders.add(jft.getJobId());
	}
	
	System.out.println("no of teacher :"+listTeacherDetails.size());
	System.out.println("no of job for teacher :"+jobForTeacherLists.size());
	
	
	// for tfaAffliate
	List<TeacherExperience> listTeacherExperiences = new ArrayList<TeacherExperience>();
	listTeacherExperiences = teacherExperienceDAO.findByTeacherDetailsObj(listTeacherDetails);
	Map<String,TeacherExperience> mapTeacherExp = new HashMap<String, TeacherExperience>();
	for(TeacherExperience te:listTeacherExperiences)
	 {
		mapTeacherExp.put(te.getTeacherId().getTeacherId().toString(),te);
	 }
	
	//for A Score && L/R Schore
     List<TeacherAchievementScore> listTeacherAchievementScores = teacherAchievementScoreDAO.getAchievementScoreListByTDList(listTeacherDetails);
     Map<String,TeacherAchievementScore> mapTeacherAchievementScore = new HashMap<String, TeacherAchievementScore>();
	 	for(TeacherAchievementScore tas:listTeacherAchievementScores)
	 	 {
	 		mapTeacherAchievementScore.put(tas.getTeacherDetail().getTeacherId().toString(),tas);
	 	 }
//for Teacher norm score
    List<TeacherNormScore> listTeacherNormScores =null;
	if(listTeacherDetails.size()>0){
	 listTeacherNormScores = teacherNormScoreDAO.findTeacersNormScoresList(listTeacherDetails);
	}
	/* mukesh * for fit score*/
	List<JobWiseConsolidatedTeacherScore> listJobWiseCTScore = jobWiseConsolidatedTeacherScoreDAO.findbyTeacherDetails(listTeacherDetails);
	Map<String,JobWiseConsolidatedTeacherScore> mapJWCTeacherScore = new HashMap<String, JobWiseConsolidatedTeacherScore>();
 	for(JobWiseConsolidatedTeacherScore jwct:listJobWiseCTScore)
 	 {
 		mapJWCTeacherScore.put(jwct.getTeacherDetail().getTeacherId().toString(),jwct);
 	 }
	
	
//SCHOOL
	// for Campus/School
	
	 List<SchoolInJobOrder> listSchoolInJobOrders = schoolInJobOrderDAO.getSIJO(listJoboOrders);
     Map<String,List<SchoolMaster>> maplistSchoolMaster = new HashMap<String, List<SchoolMaster>>();
   
     for(SchoolInJobOrder sjo:listSchoolInJobOrders)
     {
    	List<SchoolMaster> schools = null; 
    	String jId=sjo.getJobId().getJobId().toString();
    	schools = maplistSchoolMaster.get(jId);
    	if(schools==null)
    	{
    		List<SchoolMaster> listSchoolMasters=new ArrayList<SchoolMaster>();
    		listSchoolMasters.add(sjo.getSchoolId());
    		maplistSchoolMaster.put(jId.toString(), listSchoolMasters);
    	}
    	else{
    		schools.add(sjo.getSchoolId());
    		maplistSchoolMaster.put(jId.toString(), schools);
    	    }
     }

	 
	// for Academics Recorcds..
	 
	 List<TeacherAcademics> listTeacherAcademics = teacherAcademicsDAO.findByTeacherDetailsObj(listTeacherDetails);
	 Map<String,List<TeacherAcademics>> maplistTeacherAcademics = new HashMap<String, List<TeacherAcademics>>();
	
	 for(JobForTeacher Jft_1:jobForTeacherLists)	{
	 if(listTeacherAcademics!=null && listTeacherAcademics.size()>0)
		{
			List<TeacherAcademics> teacherAcademicsList = null;
			for(TeacherAcademics ta:listTeacherAcademics)
			{
				if(Jft_1.getTeacherId().getTeacherId().equals(ta.getTeacherId().getTeacherId()))
				{
					if(teacherAcademicsList==null)
					{
						teacherAcademicsList = new ArrayList<TeacherAcademics>();
						teacherAcademicsList.add(ta);
					}else
						teacherAcademicsList.add(ta);
				}
			}
			maplistTeacherAcademics.put(Jft_1.getTeacherId().getTeacherId().toString(), teacherAcademicsList);					
		}
	}	
	
	// for teacher references
	 List<TeacherElectronicReferences> listTeacherElectronicReferences = teacherElectronicReferencesDAO.findByTeacherDetailsObj(listTeacherDetails);
	 Map<String,List<TeacherElectronicReferences>> mapTeacherEleReference = new HashMap<String, List<TeacherElectronicReferences>>();
	 for(JobForTeacher Jft_1:jobForTeacherLists)	{
		 if(listTeacherElectronicReferences!=null && listTeacherElectronicReferences.size()>0)
			{
				List<TeacherElectronicReferences> teacherAcademicsList = null;
				for(TeacherElectronicReferences ter:listTeacherElectronicReferences)
				{
					if(Jft_1.getTeacherId().getTeacherId().equals(ter.getTeacherDetail().getTeacherId()))
					{
						if(teacherAcademicsList==null)
						{
							teacherAcademicsList = new ArrayList<TeacherElectronicReferences>();
							teacherAcademicsList.add(ter);
						}else
							teacherAcademicsList.add(ter);
					}
				}
				mapTeacherEleReference.put(Jft_1.getTeacherId().getTeacherId().toString(), teacherAcademicsList);					
			}
		}	
	 
	// for race
	 Map<String,String> mapRaces =new HashMap<String, String>();
	 List<RaceMaster> lstRaceMasters = raceMasterDAO.findAll();
	 for(RaceMaster rm: lstRaceMasters){
		 mapRaces.put(rm.getRaceId().toString(), rm.getRaceName());
	 }
	
//////////////////////Excel Writing //////////////////////////////
	
	String time = String.valueOf(System.currentTimeMillis()).substring(6);
	//String basePath = request.getRealPath("/")+"/candidate";
	String basePath = request.getSession().getServletContext().getRealPath ("/")+"/report";
	System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ :: "+basePath);
	fileName ="completeReport"+time+".xls";

	
	File file = new File(basePath);
	if(!file.exists())
		file.mkdirs();

	Utility.deleteAllFileFromDir(basePath);

	file = new File(basePath+"/"+fileName);

	WorkbookSettings wbSettings = new WorkbookSettings();

	wbSettings.setLocale(new Locale("en", "EN"));

	WritableCellFormat timesBoldUnderline;
	WritableCellFormat header;
	WritableCellFormat headerBold;
	WritableCellFormat times;

	WritableWorkbook workbook = Workbook.createWorkbook(file, wbSettings);
	workbook.createSheet(" Report", 0);
	WritableSheet excelSheet = workbook.getSheet(0);

	WritableFont times10pt = new WritableFont(WritableFont.ARIAL, 10);
	// Define the cell format
	times = new WritableCellFormat(times10pt);
	// Lets automatically wrap the cells
	times.setWrap(true);

	// Create create a bold font with unterlines
	WritableFont times20ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 20, WritableFont.BOLD, false);
	WritableFont times10ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD, false);

	timesBoldUnderline = new WritableCellFormat(times20ptBoldUnderline);
	timesBoldUnderline.setAlignment(Alignment.CENTRE);
	// Lets automatically wrap the cells
	timesBoldUnderline.setWrap(true);

	header = new WritableCellFormat(times10ptBoldUnderline);
	headerBold = new WritableCellFormat(times10ptBoldUnderline);
	CellView cv = new CellView();
	cv.setFormat(times);
	cv.setFormat(timesBoldUnderline);
	cv.setAutosize(true);

	header.setBackground(Colour.GRAY_25);
	

	// Write a few headers
	excelSheet.mergeCells(0, 0, 8, 1);
	Label label;
	label = new Label(0, 0, "Candidates Jobs Details", timesBoldUnderline);
	excelSheet.addCell(label);
	excelSheet.mergeCells(0, 3, 8, 3);
	label = new Label(0, 3, "");
	excelSheet.addCell(label);
	excelSheet.getSettings().setDefaultColumnWidth(18);
	
	int k=4;
	int col=1;
	label = new Label(0, k, Utility.getLocaleValuePropByKey("msgCandidateId", locale),header); 
	excelSheet.addCell(label);
	label = new Label(1, k, Utility.getLocaleValuePropByKey("lblLname", locale),header); 
	excelSheet.addCell(label);
	label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblFname", locale),header); 
	excelSheet.addCell(label);
	label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblEmail", locale),header); 
	excelSheet.addCell(label);
	label = new Label(++col, k, Utility.getLocaleValuePropByKey("msgAddedToSystem", locale),header); 
	excelSheet.addCell(label);
	label = new Label(++col, k, Utility.getLocaleValuePropByKey("msgFirstJobApplied", locale),header); 
	excelSheet.addCell(label);
	label = new Label(++col, k, Utility.getLocaleValuePropByKey("msgAddedToThis", locale)+Utility.getLocaleValuePropByKey("lblJobOdr", locale)+"",header); 
	excelSheet.addCell(label);
	label = new Label(++col, k, Utility.getLocaleValuePropByKey("msgTFAAffiliate", locale),header); 
	excelSheet.addCell(label);
	label = new Label(++col, k, Utility.getLocaleValuePropByKey("msgCorpsRegion", locale),header); 
	excelSheet.addCell(label);
	label = new Label(++col, k, Utility.getLocaleValuePropByKey("msgCorpsYear", locale),header); 
	excelSheet.addCell(label);
	label = new Label(++col, k, Utility.getLocaleValuePropByKey("msgStateTerritory", locale),header); 
	excelSheet.addCell(label);
	label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblGend", locale),header); 
	excelSheet.addCell(label);
	label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblRac", locale),header); 
	excelSheet.addCell(label);
	/*label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblEPINormScore", locale),header); 
	excelSheet.addCell(label);*/
	label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblTFA", locale),header); 
	excelSheet.addCell(label);
	label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblYOfTeachExp", locale),header); 
	excelSheet.addCell(label);
	label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblJobId", locale),header); 
	excelSheet.addCell(label);
	label = new Label(++col, k, ""+Utility.getLocaleValuePropByKey("lblJobOdr", locale)+Utility.getLocaleValuePropByKey("lblTitle", locale),header); 
	excelSheet.addCell(label);
	
	label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblCandStatus", locale),header); 
	excelSheet.addCell(label);
	label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblJoStatus", locale),header); 
	excelSheet.addCell(label);
	label = new Label(++col, k, ""+Utility.getLocaleValuePropByKey("lblHired", locale)+"?",header); 
	excelSheet.addCell(label);
	label = new Label(++col, k, Utility.getLocaleValuePropByKey("msgDateofHire", locale),header); 
	excelSheet.addCell(label);
	label = new Label(++col, k, Utility.getLocaleValuePropByKey("msgDistrictEmployee3", locale),header); 
	excelSheet.addCell(label);
	label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblAScore", locale),header); 
	excelSheet.addCell(label);
	label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblL/RScore", locale),header); 
	
	excelSheet.addCell(label);
	label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblFitScore", locale),header); 
	excelSheet.addCell(label);
	label = new Label(++col, k, Utility.getLocaleValuePropByKey("msgCampus/School", locale),header); 
	excelSheet.addCell(label);
	label = new Label(++col, k, Utility.getLocaleValuePropByKey("msgUndergraduateGradud", locale),header); 
	excelSheet.addCell(label);
	label = new Label(++col, k, Utility.getLocaleValuePropByKey("msgGPAUG", locale),header); 
	excelSheet.addCell(label);
	label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblDgr", locale),header); 
	excelSheet.addCell(label);
	label = new Label(++col, k, Utility.getLocaleValuePropByKey("msgDegreeType", locale),header); 
	excelSheet.addCell(label);
	label = new Label(++col, k, Utility.getLocaleValuePropByKey("msgEPIAttitudinal", locale),header); 
	excelSheet.addCell(label);
	label = new Label(++col, k, Utility.getLocaleValuePropByKey("msgEPICognitive", locale),header); 
	excelSheet.addCell(label);
	label = new Label(++col, k, Utility.getLocaleValuePropByKey("msgEPITeaching", locale),header); 
	excelSheet.addCell(label);
	label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblCompositeScore", locale),header); 
	excelSheet.addCell(label);
	label = new Label(++col, k, Utility.getLocaleValuePropByKey("msgFirstNameReference", locale),header); 
	excelSheet.addCell(label);
	label = new Label(++col, k, Utility.getLocaleValuePropByKey("msgLastNameReference", locale),header); 
	excelSheet.addCell(label);
	label = new Label(++col, k, Utility.getLocaleValuePropByKey("msgDesignationReference", locale),header); 
	excelSheet.addCell(label);
	label = new Label(++col, k, Utility.getLocaleValuePropByKey("msgOrganizationReference", locale),header); 
	excelSheet.addCell(label);
	label = new Label(++col, k, Utility.getLocaleValuePropByKey("msgEmailAddressReference", locale),header); 
	excelSheet.addCell(label);
	label = new Label(++col, k, Utility.getLocaleValuePropByKey("msgReferenceContactNumber", locale),header); 
	excelSheet.addCell(label);
	
	
	k=k+1;
	if(jobForTeacherLists.size()==0)
	{	
		excelSheet.mergeCells(0, k, 8, k);
		label = new Label(0, k, ""+Utility.getLocaleValuePropByKey("msgNorecordfound", locale)+""); 
		excelSheet.addCell(label);
	}	
	
	 for(JobForTeacher jft:jobForTeacherLists)
	 { 
		col=1;
		
	    if(jft.getStatus().getStatusId()!=8){
		    
		    label = new Label(0, k, Integer.toString(jft.getTeacherId().getTeacherId())); 
			excelSheet.addCell(label);
			
			label = new Label(1, k, jft.getTeacherId().getLastName()); 
			excelSheet.addCell(label);
			
			label = new Label(++col, k, jft.getTeacherId().getFirstName()); 
			excelSheet.addCell(label);
			
			label = new Label(++col, k, jft.getTeacherId().getEmailAddress()); 
			excelSheet.addCell(label);
			

			DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
            String strDate = dateFormat.format(jft.getTeacherId().getCreatedDateTime());
			label = new Label(++col, k, strDate); 
			excelSheet.addCell(label);
			
			DateFormat dateFormat1 = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
            String strDate1 = dateFormat1.format(jft.getCreatedDateTime());
			label = new Label(++col, k, strDate1); 
			excelSheet.addCell(label);
			
			label = new Label(++col, k, jft.getJobId().getJobTitle()); 
			excelSheet.addCell(label);
			
			
			//tfa Affliate
			if(mapTeacherExp.get(jft.getTeacherId().getTeacherId().toString())!=null)
			{
				
				if(jft.getTeacherId().getTeacherId().toString().equals(mapTeacherExp.get(jft.getTeacherId().getTeacherId().toString()).getTeacherId().getTeacherId().toString()))
				{
					if(mapTeacherExp.get(jft.getTeacherId().getTeacherId().toString()).getTfaAffiliateMaster()!=null){
					label = new Label(++col, k, mapTeacherExp.get(jft.getTeacherId().getTeacherId().toString()).getTfaAffiliateMaster().getTfaAffiliateName()); 
					excelSheet.addCell(label);
					}else
					{
						label = new Label(++col, k, ""); 
						excelSheet.addCell(label);
					}
				}else
				{
					label = new Label(++col, k, ""); 
					excelSheet.addCell(label);
				}
			}
			else
			{
				label = new Label(++col, k, ""); 
				excelSheet.addCell(label);
			}
			
		// for Corps Region

			if(mapTeacherExp.get(jft.getTeacherId().getTeacherId().toString())!=null)
			{
				
				if(jft.getTeacherId().getTeacherId().toString().equals(mapTeacherExp.get(jft.getTeacherId().getTeacherId().toString()).getTeacherId().getTeacherId().toString()))
				{
					if((mapTeacherExp.get(jft.getTeacherId().getTeacherId().toString()).getTfaRegionMaster())!=null)
					{
						label = new Label(++col, k, mapTeacherExp.get(jft.getTeacherId().getTeacherId().toString()).getTfaRegionMaster().getTfaRegionName()); 
						excelSheet.addCell(label);
					}else{
						label = new Label(++col, k, ""); 
					    excelSheet.addCell(label);
					    }
				}else{
					label = new Label(++col, k, ""); 
				    excelSheet.addCell(label);
				    }
			}
			else
			{
				label = new Label(++col, k, ""); 
				excelSheet.addCell(label);
			}
			
				//for Corp year
			if(mapTeacherExp.get(jft.getTeacherId().getTeacherId().toString())!=null)
			{
				
				if(jft.getTeacherId().getTeacherId().toString().equals(mapTeacherExp.get(jft.getTeacherId().getTeacherId().toString()).getTeacherId().getTeacherId().toString()))
				{
					if(mapTeacherExp.get(jft.getTeacherId().getTeacherId().toString()).getCorpsYear()!=null){
						DateFormat dateFormat2 = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
			            String strDate2 = dateFormat2.format(mapTeacherExp.get(jft.getTeacherId().getTeacherId().toString()).getCorpsYear());
						label = new Label(++col, k, strDate2); 
						excelSheet.addCell(label);
					}else{
						label = new Label(++col, k, " "); 
					    excelSheet.addCell(label);
					    }
				}else
				{
					label = new Label(++col, k, ""); 
					excelSheet.addCell(label);
				}
			}
			else
			{
				label = new Label(++col, k, ""); 
				excelSheet.addCell(label);
			}
			
		
			//State/Territory
			if(mapTeacherPersonalInfo.get(jft.getTeacherId().getTeacherId().toString())!=null)
			{
				
				if(jft.getTeacherId().getTeacherId().toString().equals(mapTeacherPersonalInfo.get(jft.getTeacherId().getTeacherId().toString()).getTeacherId().toString()))
				{
					if(mapTeacherPersonalInfo.get(jft.getTeacherId().getTeacherId().toString()).getStateId()!=null){
					label = new Label(++col, k, mapTeacherPersonalInfo.get(jft.getTeacherId().getTeacherId().toString()).getStateId().getStateName()); 
					excelSheet.addCell(label);
					}
				}
			}
			else
			{
				label = new Label(++col, k, ""); 
				excelSheet.addCell(label);
			}
			
		// Gender	
	     if(mapTeacherPersonalInfo.get(jft.getTeacherId().getTeacherId().toString())!=null)
			{
				if(jft.getTeacherId().getTeacherId().toString().equals(mapTeacherPersonalInfo.get(jft.getTeacherId().getTeacherId().toString()).getTeacherId().toString()))
				{
					if(mapTeacherPersonalInfo.get(jft.getTeacherId().getTeacherId().toString()).getGenderId()!=null){
						label = new Label(++col, k, mapTeacherPersonalInfo.get(jft.getTeacherId().getTeacherId().toString()).getGenderId().getGenderName()); 
						excelSheet.addCell(label);
					}
					else{
						label = new Label(++col, k,""); 
						excelSheet.addCell(label);
					}
				}
			}
			else
			{
				label = new Label(++col, k,""); 
				excelSheet.addCell(label);
			}
		
	     //Race 
		 if(mapTeacherPersonalInfo.get(jft.getTeacherId().getTeacherId().toString())!=null)
			{
				if(jft.getTeacherId().getTeacherId().toString().equals(mapTeacherPersonalInfo.get(jft.getTeacherId().getTeacherId().toString()).getTeacherId().toString()))
				{
					String raceName=null;
					if(mapTeacherPersonalInfo.get(jft.getTeacherId().getTeacherId().toString()).getRaceId()!=null){
						String[] raceIDS=mapTeacherPersonalInfo.get(jft.getTeacherId().getTeacherId().toString()).getRaceId().split(",");
						
					  	   for(int i=0;i<raceIDS.length;i++){
							   if(raceName==null){
									raceName=mapRaces.get(raceIDS[i]);
								}
								else{
									raceName=raceName+","+mapRaces.get(raceIDS[i]);
								}
						    }
					  	 label = new Label(++col, k,raceName); 
						 excelSheet.addCell(label);
					} else {
						 label = new Label(++col, k,""); 
						 excelSheet.addCell(label);
						} 
				}	  	
			}
			else
			{
				 label = new Label(++col, k,""); 
				 excelSheet.addCell(label);
			}
			
		// for Norm Score	
			/*boolean  flagForNorm=false;
			for(TeacherNormScore tns:listTeacherNormScores)
			{
				if(jft.getTeacherId().equals(tns.getTeacherDetail()))
				{
					 label = new Label(++col, k,Integer.toString(tns.getTeacherNormScore())); 
					 excelSheet.addCell(label);
					flagForNorm=true;
				}
			}
			if(!flagForNorm){
				 label = new Label(++col, k,""); 
				 excelSheet.addCell(label);
			}*/
			
			// for TFA
			if(mapTeacherExp.get(jft.getTeacherId().getTeacherId().toString())!=null)
			{
				if(jft.getTeacherId().getTeacherId().toString().equals(mapTeacherExp.get(jft.getTeacherId().getTeacherId().toString()).getTeacherId().getTeacherId().toString()))
				{
					if(mapTeacherExp.get(jft.getTeacherId().getTeacherId().toString()).getTfaAffiliateMaster()!=null){
					 label = new Label(++col, k,mapTeacherExp.get(jft.getTeacherId().getTeacherId().toString()).getTfaAffiliateMaster().getTfaShortAffiliateName()); 
					 excelSheet.addCell(label);
					}
				}
			}
			else
			{
				label = new Label(++col, k,""); 
				excelSheet.addCell(label);
			}
				
			// Teaching Experience
			if(mapTeacherExp.get(jft.getTeacherId().getTeacherId().toString())!=null)
			{
				
				if(jft.getTeacherId().getTeacherId().toString().equals(mapTeacherExp.get(jft.getTeacherId().getTeacherId().toString()).getTeacherId().getTeacherId().toString()))
				{
					if(mapTeacherExp.get(jft.getTeacherId().getTeacherId().toString()).getExpCertTeacherTraining()!=null){
					label = new Label(++col, k,Double.toString(mapTeacherExp.get(jft.getTeacherId().getTeacherId().toString()).getExpCertTeacherTraining())); 
					excelSheet.addCell(label);
					}else{
						label = new Label(++col, k,""); 
						excelSheet.addCell(label);
					}
				}else{
					label = new Label(++col, k,""); 
					excelSheet.addCell(label);
				}
			}
			else
			{
				label = new Label(++col, k,""); 
				excelSheet.addCell(label);
			}
			
			label = new Label(++col, k,Integer.toString(jft.getJobId().getJobId())); 
			excelSheet.addCell(label);
			
			
			label = new Label(++col, k,jft.getJobId().getJobTitle()); 
			excelSheet.addCell(label);
			
			
			label = new Label(++col, k,jft.getStatus().getStatus()); 
			excelSheet.addCell(label);
			
			//job status
			 Date d1=new Date();
			 Date d2=jft.getJobId().getJobEndDate();
			 if(d1 .before(d2) || d1.compareTo(d2)==0){
					label = new Label(++col, k,Utility.getLocaleValuePropByKey("msgOpen", locale)); 
					excelSheet.addCell(label);
			 }
			 else {
				 label = new Label(++col, k,Utility.getLocaleValuePropByKey("msgClosed", locale)); 
					excelSheet.addCell(label);
			 }
			//"+Utility.getLocaleValuePropByKey("lblHired", locale)+"
			if(jft.getStatus().getStatusId()==6){
				label = new Label(++col, k,"Yes"); 
				excelSheet.addCell(label);
			}else{
				label = new Label(++col, k,"No"); 
				excelSheet.addCell(label);
			}
			
			//Date of "+Utility.getLocaleValuePropByKey("lblHired", locale)+"
			if(jft.getStatus().getStatusId()==6 && jft.getLastActivityDate()!=null){
				DateFormat dateFormat3 = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
	            String strDate3 = dateFormat.format(jft.getLastActivityDate());
				label = new Label(++col, k,strDate3); 
				excelSheet.addCell(label);
				
			}else{
				label = new Label(++col, k,""); 
				excelSheet.addCell(label);
			}
			//Are you district Employee?(Internal)
			if(jft.getIsAffilated()==1){
				label = new Label(++col, k,"Yes"); 
				excelSheet.addCell(label);
			}	else{
				label = new Label(++col, k,"No"); 
				excelSheet.addCell(label);	
			}
			// for A Score
			   if(mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString())!=null)
				{
					if(jft.getTeacherId().getTeacherId().toString().equals(mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getTeacherDetail().getTeacherId().toString()))
					{
						if(mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getAcademicAchievementScore()!=null){
							label = new Label(++col, k,mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getAcademicAchievementScore()+"/"+mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getAcademicAchievementMaxScore()); 
							excelSheet.addCell(label);	
						}else{
							label = new Label(++col, k,""); 
							excelSheet.addCell(label);
						}
					}else{
						label = new Label(++col, k,""); 
						excelSheet.addCell(label);
					}
				}
				else
				{
					label = new Label(++col, k,""); 
					excelSheet.addCell(label);
				}
		//for L/R Score 
			   if(mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString())!=null)
				{
					if(jft.getTeacherId().getTeacherId().toString().equals(mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getTeacherDetail().getTeacherId().toString()))
					{
						if(mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getAcademicAchievementScore()!=null){
							label = new Label(++col, k,mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getLeadershipAchievementScore()+"/"+mapTeacherAchievementScore.get(jft.getTeacherId().getTeacherId().toString()).getLeadershipAchievementMaxScore()); 
							excelSheet.addCell(label);
						}else{
							label = new Label(++col, k,""); 
							excelSheet.addCell(label);
						}
					}else{
						label = new Label(++col, k,""); 
						excelSheet.addCell(label);
					}
				}
				else
				{
					label = new Label(++col, k,""); 
					excelSheet.addCell(label);
				}
	
			   // for fit Score	
			   if(mapJWCTeacherScore.get(jft.getTeacherId().getTeacherId().toString())!=null)
				{
				  if(jft.getTeacherId().getTeacherId().toString().equals(mapJWCTeacherScore.get(jft.getTeacherId().getTeacherId().toString()).getTeacherDetail().getTeacherId().toString()) && jft.getJobId().getJobId().equals(mapJWCTeacherScore.get(jft.getTeacherId().getTeacherId().toString()).getJobOrder().getJobId()))
					{
					  label = new Label(++col, k,mapJWCTeacherScore.get(jft.getTeacherId().getTeacherId().toString()).getJobWiseConsolidatedScore()+"/"+mapJWCTeacherScore.get(jft.getTeacherId().getTeacherId().toString()).getJobWiseMaxScore()); 
					  excelSheet.addCell(label); 
					}else{	
						 label = new Label(++col, k,""); 
						 excelSheet.addCell(label);
						 }
				}
				else
				{
					 label = new Label(++col, k,""); 
					 excelSheet.addCell(label);
				}
			//Campus/School
				if(maplistSchoolMaster.get(jft.getJobId().getJobId().toString())!=null)
				{
						List<SchoolMaster> listSchoolMasters=maplistSchoolMaster.get(jft.getJobId().getJobId().toString());
						
						if(listSchoolMasters.size()>1)
						{
							 label = new Label(++col, k,Utility.getLocaleValuePropByKey("msgMultipleSchools", locale)); 
								excelSheet.addCell(label);
						}	
						else if(listSchoolMasters.size()==1){
								 label = new Label(++col, k,listSchoolMasters.get(0).getSchoolName()); 
								 excelSheet.addCell(label);
						}
					}
				
				else
				{
					 label = new Label(++col, k,""); 
						excelSheet.addCell(label);
				}
	      /************************Academics Records*************************************/	 	
		 	
		 //for Undergraduate/Gradudate/College (College Major)
		 if(maplistTeacherAcademics.get(jft.getTeacherId().getTeacherId().toString())!=null)
			{
			  int count=1;
				 List<TeacherAcademics> ta=maplistTeacherAcademics.get(jft.getTeacherId().getTeacherId().toString());
				 StringBuffer uName=new StringBuffer();
					 if(ta!=null && ta.size()>0)
						for(TeacherAcademics lstta:ta)
					 	{
							String str=null;
							    if(lstta.getUniversityId().getUniversityName()!=null)
							    {
							    	 str=count++ + ") "+lstta.getUniversityId().getUniversityName();
							    	uName.append(str);
							    	 	
							    }else
							    {
							    	str=count++ + ")";
							    }
							 uName.append("\n");
					 	}

			 			label = new Label(++col, k,uName.toString()); 
						excelSheet.addCell(label);
			}
			else
			{
				label = new Label(++col, k,""); 
				excelSheet.addCell(label);
			}
		 
	// CGPA 	
			if(maplistTeacherAcademics.get(jft.getTeacherId().getTeacherId().toString())!=null)
			{
				int count=1;
					List<TeacherAcademics> ta=maplistTeacherAcademics.get(jft.getTeacherId().getTeacherId().toString());
					 StringBuffer gpaCom=new StringBuffer();
					 if(ta!=null && ta.size()>0)
						for(TeacherAcademics lstta:ta)
					 	{
							String str=null;
					 	
					 		 if(lstta.getGpaCumulative()!=null){
					 			 str=count++ + ") "+lstta.getGpaCumulative();
					 			gpaCom.append(str);
					 			
					 			}
					 		 else{	
					 			str=count++ + ")";
					 			}
					 		gpaCom.append("\n");
					 	}
					 label = new Label(++col, k,gpaCom.toString()); 
						excelSheet.addCell(label);	 
			}
			else
			{
				label = new Label(++col, k,""); 
				excelSheet.addCell(label);
			}
			
			// for degree
			if(maplistTeacherAcademics.get(jft.getTeacherId().getTeacherId().toString())!=null)
			{
				int count=1;
					List<TeacherAcademics> ta=maplistTeacherAcademics.get(jft.getTeacherId().getTeacherId().toString());
					 StringBuffer gpaCom=new StringBuffer();
					
					 if(ta!=null && ta.size()>0)
						for(TeacherAcademics lstta:ta)
					 	{
							String str=null;
					 		 if(lstta.getDegreeId().getDegreeName()!=null){
					 			 str=count++ + ") "+lstta.getDegreeId().getDegreeName();
						 			gpaCom.append(str);
					 			}
					 		 else{
					 			str=count++ + ")";
					 			}
					 		gpaCom.append("\n");
					 	}
				label = new Label(++col, k,gpaCom.toString()); 
				excelSheet.addCell(label); 
			}
			else
			{
				label = new Label(++col, k,""); 
				excelSheet.addCell(label);
			}
			
			 	// for degree type
			if(maplistTeacherAcademics.get(jft.getTeacherId().getTeacherId().toString())!=null)
			{
				int count=1;
					List<TeacherAcademics> ta=maplistTeacherAcademics.get(jft.getTeacherId().getTeacherId().toString());
					 StringBuffer gpaCom=new StringBuffer();
					
					 if(ta!=null && ta.size()>0)
						for(TeacherAcademics lstta:ta)
					 	{
							String str=null;
					 		 if(lstta.getDegreeId().getDegreeType()!=null)
					 		 {
						 			 String degreeType=lstta.getDegreeId().getDegreeType();
						 		       if(degreeType.equals("D")){  

								 			 str=count++ + ") "+Utility.getLocaleValuePropByKey("msgDoctorate", locale);
									 		 gpaCom.append(str);
						 		        }
								 	   else if(degreeType.equals("M")){
								 		  str=count++ + ") "+Utility.getLocaleValuePropByKey("msgMaster", locale);
									 		 gpaCom.append(str);
								 	   }
								 	  else if(degreeType.equals("B")){
								 		 str=count++ + ") "+Utility.getLocaleValuePropByKey("msgBachelor", locale);
								 		 gpaCom.append(str);
								 	  }
								 	 else if(degreeType.equals("A")){
								 		 str=count++ + ") "+Utility.getLocaleValuePropByKey("msgAssociate", locale);
								 		 gpaCom.append(str);
									 }
								 	 else{
								 		 str=count++ + ") "+"1";
								 		 gpaCom.append(str);
								 	 }
					 		 }
					 		else{	
					 			str=count++ + ")";
					 			}
					 		gpaCom.append("\n");
					 	}
					 label = new Label(++col, k,gpaCom.toString()); 
						excelSheet.addCell(label); 
			}
			else
			{
				label = new Label(++col, k,""); 
				excelSheet.addCell(label);
			}
	//removing key from mapList of TeacherAcademics		
			if(maplistTeacherAcademics.get(jft.getTeacherId().getTeacherId().toString())!=null)
				maplistTeacherAcademics.remove(jft.getTeacherId().getTeacherId().toString());
				
	//*********** End Academics Records*******************//*	
			
			//EPI Attitudinal	EPI Cognitive	EPI Teaching

			for(DomainMaster domain: lstDomain)
			{	
				String key = domain.getDomainId()+"##"+jft.getTeacherId().getTeacherId();
				
				rawDataForDomain =  mapRawDomainScore.get(key);
               if(rawDataForDomain!=null)
				{
					key = domain.getDomainId()+"##"+jft.getJobId().getJobId()+"##"+Math.round(rawDataForDomain.getScore());						
                    pScroreJob = mapJobPercentile.get(key);
					
					if(pScroreJob!=null)
					{
					 //System.out.println(Math.round(pScroreJob.gettValue()));
						 label = new Label(++col, k,Double.toString(Math.round(pScroreJob.gettValue()))); 
							excelSheet.addCell(label);
					}
					else
					{
						 label = new Label(++col, k,Utility.getLocaleValuePropByKey("optN/A", locale)); 
						 excelSheet.addCell(label);
					}
				}else{
					label = new Label(++col, k,Utility.getLocaleValuePropByKey("optN/A", locale)); 
					excelSheet.addCell(label);
				}
			}
		
			
		//	tmRecords.append("<th width='10%' style='text-align: left;'>"+Utility.getLocaleValuePropByKey("msgEPITeaching", locale)+"</th>");
			//FOR EPI Teaching not completed
			//label = new Label(++col, k,""); 
			//excelSheet.addCell(label);
					
			
	//*********************** for references details		**********************
		//mapTeacherEleReference
			//for ref first name
			  if(mapTeacherEleReference.get(jft.getTeacherId().getTeacherId().toString())!=null)
				{
				  int count=1;
				  StringBuffer gpaCom=new StringBuffer();
					 List<TeacherElectronicReferences> ter=mapTeacherEleReference.get(jft.getTeacherId().getTeacherId().toString());
					
						 if(ter!=null && ter.size()>0)
							for(TeacherElectronicReferences lstter:ter)
						 	{
								String str=null;
								
								    if(lstter.getFirstName()!=null){
								    	 str=count++ + ") "+lstter.getFirstName();
								 			gpaCom.append(str);
						 			}
								    else{
								    	str=count++ + ")";
						 			}
						 		gpaCom.append("\n");
						 	 }
						 label = new Label(++col, k,gpaCom.toString()); 
						excelSheet.addCell(label); 	
				}
				else
				{
					label = new Label(++col, k,""); 
					excelSheet.addCell(label);
				}
			
		// for ref last name
			  if(mapTeacherEleReference.get(jft.getTeacherId().getTeacherId().toString())!=null)
				{
				  int count=1;
				  StringBuffer gpaCom=new StringBuffer();
					 List<TeacherElectronicReferences> ter=mapTeacherEleReference.get(jft.getTeacherId().getTeacherId().toString());
						
						 if(ter!=null && ter.size()>0)
							for(TeacherElectronicReferences lstter:ter)
						 	{
								String str=null;
								    if(lstter.getLastName()!=null){
								    	str=count++ + ") "+lstter.getLastName();
							 			gpaCom.append(str);
						 			}
								    else{
								    	str=count++ + ")";
						 			}
						 		gpaCom.append("\n");
						 	}
						 label = new Label(++col, k,gpaCom.toString()); 
						 excelSheet.addCell(label); 	
				}
				else
				{
					label = new Label(++col, k,""); 
					excelSheet.addCell(label);
				}
	  // for designation of reference
			
			  if(mapTeacherEleReference.get(jft.getTeacherId().getTeacherId().toString())!=null)
				{
				  StringBuffer gpaCom=new StringBuffer();
				  int count=1;
					 List<TeacherElectronicReferences> ter=mapTeacherEleReference.get(jft.getTeacherId().getTeacherId().toString());
						
						 
						 if(ter!=null && ter.size()>0)
							for(TeacherElectronicReferences lstter:ter)
						 	{
								String str=null;
								    if(lstter.getDesignation()!=null){
								    	str=count++ + ") "+lstter.getDesignation();
							 			gpaCom.append(str);
						 			}
								    else{
								    	str=count++ + ")";
						 			}
						 		gpaCom.append("\n");
						 		
						 	}
						
					label = new Label(++col, k,gpaCom.toString()); 
					excelSheet.addCell(label);	
				}
				else
				{
					label = new Label(++col, k,""); 
					excelSheet.addCell(label);
				}
			  
			// for organization of reference
			  if(mapTeacherEleReference.get(jft.getTeacherId().getTeacherId().toString())!=null)
				{
				  int count=1;
				  StringBuffer gpaCom=new StringBuffer();
					 List<TeacherElectronicReferences> ter=mapTeacherEleReference.get(jft.getTeacherId().getTeacherId().toString());
						
						 if(ter!=null && ter.size()>0)
							for(TeacherElectronicReferences lstter:ter)
						 	{
								String str=null;
								    if(lstter.getOrganization()!=null){
								    	str=count++ + ") "+lstter.getOrganization();
							 			gpaCom.append(str);
						 			}
								    else{
								    	str=count++ + ")";
						 			}
						 		gpaCom.append("\n");
						 	
						 	}
						 label = new Label(++col, k,gpaCom.toString()); 
							excelSheet.addCell(label);		
				}
				else
				{
					label = new Label(++col, k,""); 
					excelSheet.addCell(label);	
				}
			  // for emailId of reference
			  if(mapTeacherEleReference.get(jft.getTeacherId().getTeacherId().toString())!=null)
				{
				  int count=1;
				  StringBuffer gpaCom=new StringBuffer();
					 List<TeacherElectronicReferences> ter=mapTeacherEleReference.get(jft.getTeacherId().getTeacherId().toString());
						 if(ter!=null && ter.size()>0)
							for(TeacherElectronicReferences lstter:ter)
						 	{
								String str=null;
								    if(lstter.getEmail()!=null){
								    	str=count++ + ") "+lstter.getEmail();
							 			gpaCom.append(str);
								    }
								    else{
								    	str=count++ + ")";
						 			}
						 		gpaCom.append("\n");
						 	}
								    label = new Label(++col, k,gpaCom.toString()); 
									excelSheet.addCell(label);		
				}
				else
				{
					label = new Label(++col, k,""); 
					excelSheet.addCell(label);	
				}
			  // for Contact Number of reference
			  if(mapTeacherEleReference.get(jft.getTeacherId().getTeacherId().toString())!=null)
				{
				  int count=1;
				  StringBuffer gpaCom=new StringBuffer();
					 List<TeacherElectronicReferences> ter=mapTeacherEleReference.get(jft.getTeacherId().getTeacherId().toString());
						
						 if(ter!=null && ter.size()>0)
							for(TeacherElectronicReferences lstter:ter)
						 	{
								String str=null;
								    if(lstter.getContactnumber()!=null){
								    	str=count++ + ") "+lstter.getContactnumber();
							 			gpaCom.append(str);
						 			}
								    else{
								    	
								    	str=count++ + ")";
						 			}
						 		gpaCom.append("\n");
						 	}
						 label = new Label(++col, k,gpaCom.toString()); 
						 excelSheet.addCell(label);	
						}
				else
				{
					label = new Label(++col, k,""); 
					excelSheet.addCell(label);	
				}
				
			//removing key from mapList of TeacherAcademics		
				if(mapTeacherEleReference.get(jft.getTeacherId().getTeacherId().toString())!=null)
					mapTeacherEleReference.remove(jft.getTeacherId().getTeacherId().toString());
	
		
	    } 
	    ++k;
	}			
	  workbook.write();
		workbook.close();
	}	
	   catch (Exception e) 
		{
			e.printStackTrace();
		}
  return fileName;
}


  public String getZoneListforTeacher(int districtId)
   {
	   StringBuffer getZone=new StringBuffer(); 
		try
		{
			DistrictMaster districtMaster=null;
			districtMaster=districtMasterDAO.findById(districtId,false, false);
				 if(districtMaster!=null && districtMaster.getIsZoneRequired()==1)
				 {
				   	Criterion criterion=Restrictions.eq("districtMaster",districtMaster);
				   	getZone.append("<option value='"+(-1)+"'>"+Utility.getLocaleValuePropByKey("optAll", locale)+"</option>");
					 List<GeoZoneMaster> list=geoZoneMasterDAO.findByCriteria(criterion);
						for(GeoZoneMaster g:list)
						   getZone.append("<option value='"+g.getGeoZoneId()+"'>"+g.getGeoZoneName()+"</option>");
				 }
		}catch (Exception e) {
			e.printStackTrace();
		  }
	return getZone.toString();
  }
  
  
  public String[] validateNewReq(Integer districtId,Integer jobId,String sRequisitions)
  {
	  WebContext context;
	  context = WebContextFactory.get();
	  HttpServletRequest request = context.getHttpServletRequest();
	  HttpSession session = request.getSession(false);
	  if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	  {
	  	throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	  }
	  UserMaster userMaster=(UserMaster)session.getAttribute("userMaster");
	  Integer entityType=userMaster.getEntityType();
	
	  StringBuffer sb=new StringBuffer(""); 
	  Map<String, Integer> mapReqCount=new HashMap<String, Integer>();
	  int iReturnValue=0;
	  try
	  {
		  if(districtId!=null && districtId > 0)
		  {
			  DistrictMaster districtMaster=districtMasterDAO.findById(districtId, false, false);
			  if(sRequisitions!=null && sRequisitions.trim().length() >0)
			  {
				  String sTempRequisitions[]=sRequisitions.split(",");
				  System.out.println("sTempRequisitions "+sTempRequisitions.length);
				  List<String> strRequisitions=new ArrayList<String>();
				  for(String strTemp :sTempRequisitions)
				  {
					  if(strTemp!=null && !strTemp.equalsIgnoreCase(""))
					  {
						  strRequisitions.add(strTemp.trim());
						  mapReqCount.put(strTemp.trim(), 0);
						  System.out.println(districtId+" strTemp.trim() "+strTemp.trim());
					  }
				  }
				  
				  if(strRequisitions!=null && strRequisitions.size() > 0)
				  {
					  List<DistrictRequisitionNumbers> lstDistrictRequisitionNumbers=new ArrayList<DistrictRequisitionNumbers>();
					  lstDistrictRequisitionNumbers=districtRequisitionNumbersDAO.getDistrictRequisitionNumbersBYReqNo(strRequisitions, districtMaster);
					  if(lstDistrictRequisitionNumbers!=null && lstDistrictRequisitionNumbers.size() >0)
					  {
						  iReturnValue=1;
						  sb.append(Utility.getLocaleValuePropByKey("msgduplicateRequisitionNo", locale));
						  for(DistrictRequisitionNumbers dRequisitionNumbers:lstDistrictRequisitionNumbers)
						  {
							  if(dRequisitionNumbers!=null)
							  {
								  sb.append(dRequisitionNumbers.getRequisitionNumber()+"</BR>");
							  }
						  }
					  } 
				  }
				  else
				  {
					  sb.append(Utility.getLocaleValuePropByKey("msgValidRequisitionNo", locale));
					  iReturnValue=2;
				  }
				  
				  
			  }
			  
		  }
	  }catch (Exception e) {
	  	e.printStackTrace();
	  }
	  
	  System.out.println("mapReqCount.size() "+mapReqCount.size());
	  System.out.println("sb.toString() "+sb.toString());
	  
	  String sReturnArry[]={mapReqCount.size()+"",sb.toString(),iReturnValue+""};
	  return sReturnArry;
  }
  
  public Integer validateCheckedMultiHireForReqNumber(Integer jobId,Integer isCheckedMultiHire,boolean isExpHireNotEqualToReqNoValue)
  {
	  System.out.println("validateCheckedMultiHireForReqNumber jobId "+jobId+" isCheckedMultiHire "+isCheckedMultiHire +" isExpHireNotEqualToReqNoValue "+isExpHireNotEqualToReqNoValue);
	  WebContext context;
	  context = WebContextFactory.get();
	  HttpServletRequest request = context.getHttpServletRequest();
	  HttpSession session = request.getSession(false);
	  if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	  {
	  	throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	  }
	
	  StringBuffer sb=new StringBuffer(""); 
	  Map<String, Integer> mapReqCount=new HashMap<String, Integer>();
	  int iReturnValue=0;
	  try
	  {
		  if(jobId!=null && jobId > 0 && isCheckedMultiHire!=null && isCheckedMultiHire==1)
		  {
			  JobOrder jobOrder=jobOrderDAO.findById(jobId, false, false);
			  if(jobOrder!=null && jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getIsReqNoRequired() &&  jobOrder.getNoOfExpHires()==null)
			  {
				  List<SchoolInJobOrder> inJobOrders=new ArrayList<SchoolInJobOrder>();
				  inJobOrders=schoolInJobOrderDAO.findJobOrder(jobOrder);
				  int iCountTotalHire=0;
				  if(inJobOrders!=null)
				  {
					  for(SchoolInJobOrder inJobOrder:inJobOrders)
					  {
						  if(inJobOrder!=null && inJobOrder.getNoOfSchoolExpHires()!=null && inJobOrder.getNoOfSchoolExpHires() >0)
							  iCountTotalHire=iCountTotalHire+inJobOrder.getNoOfSchoolExpHires();
					  }
				  }
				  
				  
				  List<JobRequisitionNumbers> jobRequisitionNumbers=new ArrayList<JobRequisitionNumbers>();
				  jobRequisitionNumbers=jobRequisitionNumbersDAO.findRequisitionsByJob(jobOrder);
				  
				  if(isExpHireNotEqualToReqNoValue)
				  {
					  if(jobRequisitionNumbers==null)
						  iReturnValue=1; 
					  else if(jobRequisitionNumbers!=null)
					  {
						  Map<Long, Integer> mapIsHire=new HashMap<Long, Integer>();
						  for(JobRequisitionNumbers jrn:jobRequisitionNumbers)
						  {
							  mapIsHire.put(jrn.getSchoolMaster().getSchoolId(), 1);
						  }
						  
						  if(inJobOrders!=null)
						  {
							  for(SchoolInJobOrder inJobOrder:inJobOrders)
							  {
								  if(inJobOrder!=null && mapIsHire.get(inJobOrder.getSchoolId().getSchoolId())==null)
								  {
									  iReturnValue=1;
									  break;
								  }
							  }
						  }
						  
						  
					  }
				  }
				  else
				  {
					  int iCountDB=0;
					  if(jobRequisitionNumbers!=null && jobRequisitionNumbers.size()>0)
						  iCountDB= jobRequisitionNumbers.size();
					  
					  if(iCountTotalHire > 0 && iCountTotalHire!=iCountDB)
						  iReturnValue=1;  
				  }
			  }
		  }
	  }
	  catch (Exception e) 
	  {
	  	e.printStackTrace();
	  }
	  
	  return iReturnValue;
  }
  
  public String downloadAttachment(String filePath,String fileName)
	{	System.out.println("filePath::::::::"+filePath);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		String path="";
		try 
		{
			String source = Utility.getValueOfPropByKey("districtRootPath")+filePath+"/"+fileName;
			String target = context.getServletContext().getRealPath("/")+"/teacher/"+filePath+"/";
			File sourceFile = new File(source);
			File targetDir = new File(target);
			if(!targetDir.exists())
				targetDir.mkdirs();

			File targetFile = new File(targetDir+"/"+sourceFile.getName());
			FileUtils.copyFile(sourceFile, targetFile);
			path = Utility.getValueOfPropByKey("contextBasePath")+"/teacher/"+filePath+"/"+fileName;
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			path="";
		}
	
		return path;
	 }
  
  
  public String showDistFile(String distId,String fileName) {
	  System.out.println("::::::::::  showDistFile  ::::::::::");
	  String filePath = "";
	  filePath = distId+"/DistAttachmentFile";
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) {
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		String path = "";
		try {
			String source 	= 	Utility.getValueOfPropByKey("districtRootPath")+filePath+"/"+fileName;
			String target 	= 	context.getServletContext().getRealPath("/")+"/teacher/"+filePath+"/";
			File sourceFile = 	new File(source);
			File targetDir 	= 	new File(target);
			if(!targetDir.exists())
				targetDir.mkdirs();

			File targetFile = new File(targetDir+"/"+sourceFile.getName());
			FileUtils.copyFile(sourceFile, targetFile);
			path = Utility.getValueOfPropByKey("contextBasePath")+"/teacher/"+filePath+"/"+fileName;
		} catch (Exception e) {
			e.printStackTrace();
			path="";
		}
		return path;
	 }
  
  public String viewJobDescription(Integer jobId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		String path="";
		try 
		{
			JobOrder jobOrder=null;
			if(jobId!=0){
				jobOrder=jobOrderDAO.findById(jobId, false, false);
			}
			String fileName= jobOrder.getPathOfJobDescription();

			String source=Utility.getValueOfPropByKey("districtRootPath")+jobOrder.getDistrictMaster().getDistrictId()+"/JobDescription/"+fileName;
			String target = context.getServletContext().getRealPath("/")+"/"+jobOrder.getDistrictMaster().getDistrictId()+"/JobDescription/";

			File sourceFile = new File(source);
			File targetDir = new File(target);
			if(!targetDir.exists())
				targetDir.mkdirs();

			File targetFile = new File(targetDir+"/"+sourceFile.getName());

			FileUtils.copyFile(sourceFile, targetFile);

			path = Utility.getValueOfPropByKey("contextBasePath")+jobOrder.getDistrictMaster().getDistrictId()+"/JobDescription/"+fileName;


		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return path;
	}
  
  
  
public String displayRecordsByEntityTypeExcelExport(boolean resultFlag,int JobOrderType,int districtOrSchoolId,int schoolId,/*String subjectId,String certifications,*/String jobOrderIds,String status,String noOfRow,
	String pageNo,String sortOrder,String sortOrderType,String  disJobReqNo,String geoZonId,String jobCategoryIds,String jobApplicationStatus, String jobSubCategoryIds)
	{
		int jobApplicationStatusId=0;
		if(jobApplicationStatus!=null && jobApplicationStatus!="" &&!jobApplicationStatus.equals(""))
		jobApplicationStatusId =  Integer.parseInt(jobApplicationStatus);
	
		int	geoZoneId=0;
		if(geoZonId!=null && geoZonId!="" &&!geoZonId.equals(""))
		  geoZoneId =  Integer.parseInt(geoZonId);
		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		String fileNames = null;
		try{
			UserMaster userMaster = null;
			Integer entityID=null;
			DistrictMaster districtMaster=null;
			SchoolMaster schoolMaster=null;
			SubjectMaster subjectMaster	=	null;
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) {
				return "false";
			}else{
				userMaster=(UserMaster)session.getAttribute("userMaster");
				
				if(userMaster.getSchoolId()!=null)
					schoolMaster =userMaster.getSchoolId();
				

				if(userMaster.getDistrictId()!=null){
					districtMaster=userMaster.getDistrictId();
				}

				entityID=userMaster.getEntityType();
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			try{
				if(JobOrderType==3){
					roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,25,"schooljoborders.do",0);
				}else{
					roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,24,"managejoborders.do",0);
				}
			}catch(Exception e){
				e.printStackTrace();
			}

			/** set default sorting fieldName **/
			String sortOrderFieldName="createdDateTime";
			String sortOrderNoField="jobId";

			boolean deafultFlag=false;
			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal=null;
			if(sortOrder!=null){
				if(!sortOrder.equals("") && !sortOrder.equals(null) && !sortOrder.equals("ofApplicant") && !sortOrder.equals("ofHire")){
					sortOrderFieldName=sortOrder;
					sortOrderNoField=sortOrder;
					deafultFlag=false;
				}else{
					deafultFlag=true;
				}
				if(sortOrder.equals("ofApplicant")){
					sortOrderNoField="ofApplicant";
				}
				if(sortOrder.equals("ofHire")){
					sortOrderNoField="ofHire";
				}
			}
			String sortOrderTypeVal="0";
			sortOrderStrVal=Order.asc(sortOrderFieldName);
			if(sortOrderType!=null){
				if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
					if(sortOrderType.equals("1")){
						sortOrderTypeVal="1";
						sortOrderStrVal=Order.desc(sortOrderFieldName);
					}
				}
			}
			if(sortOrderType.equals("")){
				sortOrderStrVal=Order.desc(sortOrderFieldName);
			}

			/**End ------------------------------------**/
			List<JobOrder> lstJobOrder	  =	 new ArrayList<JobOrder>();
			boolean jobOrderIdFlag=false;
			boolean statusFlag=false;

			Criterion criterionStatus=null;
			Criterion criterionJobFilter=null;
			if(!(jobOrderIds.equals("0") || jobOrderIds.equalsIgnoreCase("0"))){
				String [] jobsArr =  jobOrderIds.split(",");
				List<Integer> jobs = new ArrayList<Integer>();
				for(int i=0;i<jobsArr.length;i++)
				{	
					String jobId = jobsArr[i].trim();
					try{if(!jobId.equals(""))
						jobs.add(Integer.parseInt(jobId));
					}catch(Exception e){}
				}
				if(jobs.size()>0)
				{
					jobOrderIdFlag=true;
					criterionJobFilter	= Restrictions.in("jobId",jobs);
				}
			}
			if(status!=null){
				if(!status.equalsIgnoreCase("") && !status.equalsIgnoreCase(null)){
					statusFlag=true;
					criterionStatus=Restrictions.eq("status",status);
				}
			}else{
				statusFlag=false;
			}

			/*Criterion criterionSubject=null;
			if(!subjectId.equalsIgnoreCase(""))
			{
				subjectMaster=	subjectMasterDAO.findById(Integer.parseInt(subjectId), false, false);
				criterionSubject=Restrictions.eq("subjectMaster",subjectMaster);
			}
			*/
			/*************Start *************/
			List<JobOrder> filterJobOrderList = new ArrayList<JobOrder>();

			boolean jobFlag=false;
			List<JobOrder> certJobOrderList	  =	 new ArrayList<JobOrder>();
			
			/*boolean certTypeFlag=false;
			if(certifications!=null && !certifications.equals("") && !certifications.equals("0")){
				certTypeFlag=true;
				certJobOrderList=jobCertificationDAO.findCertificationByJob(certifications);
			}
			if(certTypeFlag && certJobOrderList.size()>0){
				if(jobFlag){
					filterJobOrderList.retainAll(certJobOrderList);
				}else{
					filterJobOrderList.addAll(certJobOrderList);
				}
			}
			if(certTypeFlag){
				jobFlag=true;
			}*/

			
			/*List<JobOrder> subjectJList	  =	 new ArrayList<JobOrder>();
			boolean subjectFlag=false;
			if(subjectId!=null && !subjectId.equals("") && !subjectId.equals("0")){
				SubjectMaster sMaster=null;
				if(subjectId!=null){
					sMaster=subjectMasterDAO.findById(Integer.parseInt(subjectId),false,false);
				}
				subjectFlag=true;
				subjectJList=jobOrderDAO.findJobBySubject(sMaster);
			}
			if(subjectFlag && subjectJList.size()>0){
				if(jobFlag){
					filterJobOrderList.retainAll(subjectJList);
				}else{
					filterJobOrderList.addAll(subjectJList);
				}
			}
			if(subjectFlag){
				jobFlag=true;
			}*/

			DistrictMaster  distMaster=null;
			List<JobOrder> districtJList	  =	 new ArrayList<JobOrder>();
			boolean districtFlag=false;
			if(districtOrSchoolId!=0){
				if(districtOrSchoolId!=0){
					distMaster=districtMasterDAO.findById(districtOrSchoolId,false,false);
				}
				districtFlag=true;
				districtJList=jobOrderDAO.findJobByDistrict(distMaster);
			}
			if(districtFlag && districtJList.size()>0){
				if(jobFlag){
					filterJobOrderList.retainAll(districtJList);
				}else{
					filterJobOrderList.addAll(districtJList);
				}
			}
			if(districtFlag){
				jobFlag=true;
			}
			
			/*List<JobOrder> listjJobOrders = new ArrayList<JobOrder>();
			boolean jobCateFlag=false;
			if(jobCategoryIds!=null && !jobCategoryIds.equals("") && !jobCategoryIds.equals("0"))
			{
				List<Integer> jobcatIds = new ArrayList<Integer>();
				String jobCategoryIdStr[] =jobCategoryIds.split(",");
			  if(!ArrayUtils.contains(jobCategoryIdStr, "0"))
			  {		  
				for(String str : jobCategoryIdStr){
				 if(!str.equals(""))
					 jobcatIds.add(Integer.parseInt(str));
				 } 
				 if(jobcatIds.size()>0){
					 listjJobOrders = jobOrderDAO.findByJobCategery(jobcatIds,districtOrSchoolId);
					 jobCateFlag=true;
				 }
				 if(listjJobOrders!=null && listjJobOrders.size()>0){
						if(jobCateFlag){
							filterJobOrderList.retainAll(listjJobOrders);
						}else{
							filterJobOrderList.addAll(listjJobOrders);
						}
					 }
				 }
			  }

			if(jobCateFlag){
				jobFlag=true;
			}*/

			
			
			if(jobSubCategoryIds!=null && !jobSubCategoryIds.equals("") && !jobSubCategoryIds.equals("0"))
			  {
				//Job Sub Category
					List<JobOrder> listJobOrdersBySubCatgry = new ArrayList<JobOrder>();
					boolean jobSubCateFlag=false;
					
					if(jobSubCategoryIds!=null && !jobSubCategoryIds.equals("") && !jobSubCategoryIds.equals("0"))
					{
						List<Integer> jobSubcatIds = new ArrayList<Integer>();
						
						String jobSubCategoryIdStr[] = jobSubCategoryIds.split(",");
					  if(!ArrayUtils.contains(jobSubCategoryIdStr, "0"))
					  {		  
						for(String str : jobSubCategoryIdStr){
						 if(!str.equals(""))
							 jobSubcatIds.add(Integer.parseInt(str));
						 } 
						 if(jobSubcatIds.size()>0){
							 listJobOrdersBySubCatgry = jobOrderDAO.findByJobCategery(jobSubcatIds,districtOrSchoolId);
							 System.out.println("  job list by jobcategory :: "+listJobOrdersBySubCatgry.size());
							 jobSubCateFlag=true;
						 }
						 if(jobSubCateFlag){
							filterJobOrderList.retainAll(listJobOrdersBySubCatgry);
						 }else{
							filterJobOrderList.addAll(listJobOrdersBySubCatgry);
						 }
						}
					  }

					if(jobSubCateFlag){
						jobFlag=true;
					}
			  }
			  else
			  {
				// Job category
					List<JobOrder> listjJobOrders = new ArrayList<JobOrder>();
					boolean jobCateFlag=false;
					if(jobCategoryIds!=null && !jobCategoryIds.equals("") && !jobCategoryIds.equals("0"))
					{
						List<Integer> jobcatIds = new ArrayList<Integer>();
						
						String jobCategoryIdStr[] =jobCategoryIds.split(",");
					  if(!ArrayUtils.contains(jobCategoryIdStr, "0"))
					  {		  
						for(String str : jobCategoryIdStr){
						 if(!str.equals(""))
							 jobcatIds.add(Integer.parseInt(str));
						 } 
						 if(jobcatIds.size()>0){
							 listjJobOrders = jobOrderDAO.findByJobCategery(jobcatIds,districtOrSchoolId);
							 System.out.println("  job list by jobcategory :: "+listjJobOrders.size());
							 jobCateFlag=true;
						 }
						 if(jobCateFlag){
							filterJobOrderList.retainAll(listjJobOrders);
						 }else{
							filterJobOrderList.addAll(listjJobOrders);
						 }
						}
					  }

					if(jobCateFlag){
						jobFlag=true;
					}
				  
			  }
			
			
			
			

			Criterion criterionJobOrderType = Restrictions.eq("createdForEntity",JobOrderType);
			List<JobOrder> schoolJList	  =	 new ArrayList<JobOrder>();
			boolean schoolFlag=false;
			int searchschoolflag=2;
			if(schoolId!=0){
				SchoolMaster  sclMaster=null;
				if(districtOrSchoolId!=0){
					sclMaster=schoolMasterDAO.findById(Long.parseLong(schoolId+""),false,false);
				}
				schoolFlag=true;
				searchschoolflag=1;
				if(distMaster!=null)
					schoolJList=schoolInJobOrderDAO.findJobListBySchool(sclMaster,distMaster,JobOrderType,entityID,searchschoolflag,userMaster);
			}
			else
			{
				if(userMaster.getEntityType()==3 && JobOrderType==2 && userMaster.getDistrictId()!=null && distMaster.getWritePrivilegeToSchool().equals(true))
				{
					searchschoolflag=0; // below [ schoolMaster ] is sending from school id by getting  session 
					schoolJList=schoolInJobOrderDAO.findJobListBySchool(schoolMaster,distMaster,JobOrderType,entityID,searchschoolflag,userMaster);
					schoolFlag=true;
				}
			}
			if(schoolFlag && schoolJList.size()>0){
				if(jobFlag){
					filterJobOrderList.retainAll(schoolJList);
				}else{
					filterJobOrderList.addAll(schoolJList);
				}
			}else if(schoolFlag && schoolJList.size()==0){
				filterJobOrderList = new ArrayList<JobOrder>();
			}

			if(schoolFlag){
				jobFlag=true;
			}
			
			
			boolean jobReqnoFlag=false;
			List<JobOrder> jobOrderOfReqNoList	  =	 new ArrayList<JobOrder>();
			if(disJobReqNo!=null && !disJobReqNo.equals("") && !disJobReqNo.equals("0")){
				jobReqnoFlag=true;
				List<DistrictRequisitionNumbers> districtRequisitionNumbers=null;
				Criterion crDrq=Restrictions.eq("requisitionNumber", disJobReqNo);
				Criterion crDSTrq=null;
				if(districtOrSchoolId!=0){
					crDSTrq=Restrictions.eq("districtMaster", distMaster);
					districtRequisitionNumbers=districtRequisitionNumbersDAO.findByCriteria(crDSTrq,crDrq);
				}else{
					districtRequisitionNumbers=districtRequisitionNumbersDAO.findByCriteria(crDrq);
				}
				if(districtRequisitionNumbers!=null && districtRequisitionNumbers.size()>0){
					
					Criterion crJrq=Restrictions.in("districtRequisitionNumbers", districtRequisitionNumbers);
					List<JobRequisitionNumbers> jobRequisitionNumbers=jobRequisitionNumbersDAO.findByCriteria(crJrq);
					for(JobRequisitionNumbers jqr:jobRequisitionNumbers){
						jobOrderOfReqNoList.add(jqr.getJobOrder());
					}
				}
			}
			
			if(jobReqnoFlag && jobOrderOfReqNoList.size()>0){
				if(jobFlag){
					filterJobOrderList.retainAll(jobOrderOfReqNoList);
				}else{
					filterJobOrderList.addAll(jobOrderOfReqNoList);
				}
			}else if(jobReqnoFlag && jobOrderOfReqNoList.size()==0){
				
				filterJobOrderList = new ArrayList<JobOrder>();
			}
			
			if(jobReqnoFlag){
				jobFlag=true;
			}
			
			// =============  Deepak filtering basis on job Status  added by 27-05-2015
			if(jobApplicationStatusId>0 && distMaster!=null && distMaster.getDistrictId().toString().equalsIgnoreCase("804800"))
			{
				boolean jobApplicationStatusFlag = false;
					List<JobOrder> jobApplicationStatusList = new ArrayList<JobOrder>();
						if (disJobReqNo != null && jobApplicationStatusId > 0) {
							jobApplicationStatusFlag = true;
							jobApplicationStatusList = jobOrderDAO.getJobApplicationStatusRole(jobApplicationStatusId);
							System.out.println("jobApplicationStatusList========"+ jobApplicationStatusList.size());
		
						}
						if (jobApplicationStatusFlag
								&& jobApplicationStatusList.size() > 0) {
							if (jobFlag == true) {
								filterJobOrderList.retainAll(jobApplicationStatusList);
							} else {
								filterJobOrderList.addAll(jobApplicationStatusList);
							}
						} else {
							filterJobOrderList = new ArrayList<JobOrder>();
						}
				if (jobApplicationStatusFlag) {
					jobFlag = true;
				}
			}
			
			List<JobOrder> listjobOrdersgeoZone =new ArrayList<JobOrder>();
			
			  if(geoZoneId>0)
				{
				  GeoZoneMaster geoZoneMaster=geoZoneMasterDAO.findById(geoZoneId, false, false);
					if(userMaster.getRoleId().getRoleId().equals(3))
						listjobOrdersgeoZone = jobOrderDAO.getJobOrderBygeoZoneRole(geoZoneMaster,userMaster);
					else
						listjobOrdersgeoZone = jobOrderDAO.getJobOrderBygeoZone(geoZoneMaster);
				}
			 
			  if(listjobOrdersgeoZone!=null && listjobOrdersgeoZone.size()>0)
			  {
				  if(jobFlag==true){
					  filterJobOrderList.retainAll(listjobOrdersgeoZone);
					}else{
						filterJobOrderList.addAll(listjobOrdersgeoZone);
					}
			  }	
			  if(listjobOrdersgeoZone.size()==0 && geoZoneId==-1 ||geoZoneId==0)
			  {
				  filterJobOrderList.addAll(listjobOrdersgeoZone);
			  
			  }
			  else if(listjobOrdersgeoZone.size()==0 &&geoZoneId!=-1)
			  {
				  filterJobOrderList.retainAll(listjobOrdersgeoZone);
			  }
				
		      if(jobFlag){
				if(filterJobOrderList.size()>0){
					List jobIds = new ArrayList();
					for(int e=0; e<filterJobOrderList.size(); e++) {
						jobIds.add(filterJobOrderList.get(e).getJobId());
					}
					Criterion criterionJobList = Restrictions.in("jobId",jobIds);

					Criterion criterionMix=criterionJobOrderType;
					if(statusFlag && jobOrderIdFlag){
						criterionMix=Restrictions.and(criterionStatus,criterionJobFilter);
					}else if(statusFlag){
						criterionMix=criterionStatus;
					}else if(jobOrderIdFlag){
						criterionMix=criterionJobFilter;
					}
					lstJobOrder=jobOrderDAO.findByCriteria(sortOrderStrVal,criterionJobList,criterionJobOrderType,criterionMix);
				}
			}else{
				Criterion criterionMix=criterionJobOrderType;
				if(statusFlag && jobOrderIdFlag){
					criterionMix=Restrictions.and(criterionStatus,criterionJobFilter);
				}else if(statusFlag){
					criterionMix=criterionStatus;
				}else if(jobOrderIdFlag){
					criterionMix=criterionJobFilter;
				}
				lstJobOrder=jobOrderDAO.findByCriteria(sortOrderStrVal,criterionJobOrderType,criterionMix);
				
			}
			/***************************** End Code **************************************/

		      List<JobRequisitionNumbers> jobRequisitionNumbers=new ArrayList<JobRequisitionNumbers>();
				Map<Integer, JobRequisitionNumbers> hrStatusMap = new HashMap<Integer, JobRequisitionNumbers>();
				if(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==2)
				{				
				jobRequisitionNumbers=jobRequisitionNumbersDAO.getHRStatusByJobId(lstJobOrder);
				System.out.println("jobRequisitionNumbers size:::::"+jobRequisitionNumbers.size());
				if(jobRequisitionNumbers!=null && jobRequisitionNumbers.size()>0)
				{
					for(JobRequisitionNumbers hrstatus: jobRequisitionNumbers)
					{
						System.out.println("hrstatus.getJobOrder().getJobId():::"+hrstatus.getJobOrder().getJobId());
						hrStatusMap.put(hrstatus.getJobOrder().getJobId(), hrstatus);
					
					}
				}
				}
				
			//Start Sandeep 03-09-15		
			  Boolean hrIntegratedFlag = false;
		      if(userMaster.getDistrictId() != null)
		    	  hrIntegratedFlag = userMaster.getDistrictId().getHrIntegrated();
		    //end
			      
		    //configurable coloumn
			    Map<String,Boolean> displayCNFGColumn=null;
			    if(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==2){
			    	displayCNFGColumn=getCNFGColumn(districtMaster,true);
			    	if(userMaster.getEntityType() == 2 && hrIntegratedFlag)
			    		totalShowCol+=2;
			    	else if(userMaster.getEntityType() == 1){
			    		List<String> listCol=new ArrayList<String>();
			    		listCol.add("ZON");
			    		listCol.add("SUB");
			    		listCol.add("CANG"); 
			    		listCol.add("ATT");
			    		listCol.add("ACT"); 	
			    		listCol.add("REQ"); 
			    		totalShowCol=(displayCNFGColumn.size()-listCol.size());
			    		totalShowCol+=2;
			    	}
			    }else if(userMaster.getDistrictId()==null && userMaster.getEntityType() == 1){
			    	displayCNFGColumn=getCNFGColumn(districtMaster,false);
			    	List<String> listCol=new ArrayList<String>();
		    		listCol.add("CANG"); 
		    		listCol.add("ATT");
		    		listCol.add("ACT"); 	
		    		listCol.add("REQ"); 
		    		totalShowCol=(displayCNFGColumn.size()-listCol.size());
			    }
			    displayCNFGColumn=getCNFGColumn(districtMaster,false);
		    
			Map<Integer,String> map = new HashMap<Integer, String>();
			if(lstJobOrder!=null && lstJobOrder.size()>0)
			 map = jobForTeacherDAO.countApplicantsByJobOrdersAllll(lstJobOrder);
			
			String time = String.valueOf(System.currentTimeMillis()).substring(6);
			//String basePath = request.getRealPath("/")+"/candidate";
			String basePath = request.getSession().getServletContext().getRealPath ("/")+"/managejoborder";
			System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ :: "+basePath);
			fileNames ="managejoborder"+time+".xls";

			File file = new File(basePath);
			if(!file.exists())
				file.mkdirs();

			Utility.deleteAllFileFromDir(basePath);

			file = new File(basePath+"/"+fileNames);

			WorkbookSettings wbSettings = new WorkbookSettings();

			wbSettings.setLocale(new Locale("en", "EN"));

			WritableCellFormat timesBoldUnderline;
			WritableCellFormat header;
			WritableCellFormat headerBold;
			WritableCellFormat times;

			WritableWorkbook workbook = Workbook.createWorkbook(file, wbSettings);
			workbook.createSheet("managejoborder", 0);
			WritableSheet excelSheet = workbook.getSheet(0);

			WritableFont times10pt = new WritableFont(WritableFont.ARIAL, 10);
			// Define the cell format
			times = new WritableCellFormat(times10pt);
			// Lets automatically wrap the cells
			times.setWrap(true);

			// Create create a bold font with unterlines
			WritableFont times20ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 20, WritableFont.BOLD, false);
			WritableFont times10ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD, false);

			timesBoldUnderline = new WritableCellFormat(times20ptBoldUnderline);
			timesBoldUnderline.setAlignment(Alignment.CENTRE);
			// Lets automatically wrap the cells
			timesBoldUnderline.setWrap(true);

			header = new WritableCellFormat(times10ptBoldUnderline);
			headerBold = new WritableCellFormat(times10ptBoldUnderline);
			CellView cv = new CellView();
			cv.setFormat(times);
			cv.setFormat(timesBoldUnderline);
			cv.setAutosize(true);

			header.setBackground(Colour.GRAY_25);
			
			// Write a few headers
			excelSheet.mergeCells(0, 0, (totalShowCol-1), 1);
			Label label;
			
			String str="";
			if(JobOrderType==2)
				str=""+Utility.getLocaleValuePropByKey("optDistrict", locale);
			else if(JobOrderType==3)
				str=""+Utility.getLocaleValuePropByKey("optSchool", locale);
			
			String HeadingName= str+" "+Utility.getLocaleValuePropByKey("lblJobOdr", locale)+"s";
			label = new Label(0, 0, HeadingName, timesBoldUnderline);
			excelSheet.addCell(label);
			excelSheet.mergeCells(0, 3, (totalShowCol-1), 3);
			label = new Label(0, 3, "");
			excelSheet.addCell(label);
			excelSheet.getSettings().setDefaultColumnWidth(18);
			
			int k=4;
			int col=1;
			int columnIncr=0;
			if(userMaster.getDistrictId()==null || displayCNFGColumn.get("JID")){
				label = new Label(columnIncr, k, Utility.getLocaleValuePropByKey("lblJobId", locale),header); 
				excelSheet.addCell(label);
				++columnIncr;
			}
			
			if(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==2)
			{
			  if(userMaster.getEntityType() ==2 && hrIntegratedFlag){
					label = new Label(columnIncr, k, Utility.getLocaleValuePropByKey("lblReq/Posi", locale),header); 
					excelSheet.addCell(label);
					++columnIncr;
			  }else if(userMaster.getEntityType() == 1){
				  label = new Label(columnIncr, k, Utility.getLocaleValuePropByKey("lblReq/Posi", locale),header); 
					excelSheet.addCell(label);
					++columnIncr;
			  }
			}
			
			if(userMaster.getDistrictId()==null || displayCNFGColumn.get("TIT")){
				label = new Label(columnIncr, k, Utility.getLocaleValuePropByKey("lblTitle", locale),header); 
				excelSheet.addCell(label);
				++columnIncr;
			}
			
			if(userMaster.getDistrictId()==null || displayCNFGColumn.get("SCH")){
				label = new Label(columnIncr, k, Utility.getLocaleValuePropByKey("optSchool", locale),header); 
				excelSheet.addCell(label);
				++columnIncr;
			}
				
			
			if(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==2)
			{
				
			}
			else
			{
				if(userMaster.getDistrictId()==null || displayCNFGColumn.get("ZON")){
					label = new Label(columnIncr, k, Utility.getLocaleValuePropByKey("lblZone", locale),header); 
					excelSheet.addCell(label);
					++columnIncr;
				}
			
				if(userMaster.getDistrictId()==null || displayCNFGColumn.get("SUB")){
					label = new Label(columnIncr, k, Utility.getLocaleValuePropByKey("lblSub", locale),header); 
					excelSheet.addCell(label);
					++columnIncr;
				}
			}			
			
			if(userMaster.getDistrictId()==null || displayCNFGColumn.get("STA")){
				label = new Label(columnIncr, k, Utility.getLocaleValuePropByKey("lblStatus", locale),header); 
				excelSheet.addCell(label);
				++columnIncr;
			}
			if(userMaster.getDistrictId()==null || displayCNFGColumn.get("POSU")){
				label = new Label(columnIncr, k, Utility.getLocaleValuePropByKey("lblPostedUntil", locale),header); 
				excelSheet.addCell(label);
				++columnIncr;
			}
			if(userMaster.getDistrictId()==null || displayCNFGColumn.get("POS")){
				label = new Label(columnIncr, k, Utility.getLocaleValuePropByKey("msgPositions", locale),header); 
				excelSheet.addCell(label);
				++columnIncr;
			}
			
			if(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==2)
			{
			  if(userMaster.getEntityType() == 2 && hrIntegratedFlag){
				label = new Label(columnIncr, k, Utility.getLocaleValuePropByKey("lblHrStatus", locale),header); 
				excelSheet.addCell(label);
				++columnIncr;
			  }else if(userMaster.getEntityType() == 1){
				  label = new Label(columnIncr, k, Utility.getLocaleValuePropByKey("lblHrStatus", locale),header); 
				  excelSheet.addCell(label);
				  ++columnIncr;
			  }
			}
			
			if(userMaster.getDistrictId()==null || displayCNFGColumn.get("APP")){
				label = new Label(columnIncr, k, Utility.getLocaleValuePropByKey("lblApplicants", locale),header); 
				excelSheet.addCell(label);
				++columnIncr;
			}
			if(userMaster.getDistrictId()==null || displayCNFGColumn.get("HPER")){
				label = new Label(columnIncr, k, Utility.getLocaleValuePropByKey("lblHires", locale),header); 
				excelSheet.addCell(label);
				++columnIncr;
			}
			
			k=k+1;
			

			if(resultFlag){
				if(lstJobOrder.size()==0){
					excelSheet.mergeCells(0, k, (totalShowCol-1), k);
					label = new Label(columnIncr, k, ""+Utility.getLocaleValuePropByKey("msgNorecordfound", locale)+""); 
					excelSheet.addCell(label);	
					++columnIncr;
				}
				
				Map<String, String> positionMap = getPositionsNumber(lstJobOrder);
				Map<String, String> hireCandidatePercentageMap = getHireCandidatePercentage(lstJobOrder,positionMap);
				for(JobOrder jobOrderDetails : lstJobOrder)
				{
					col=1;
					int jobForTeacherSize=0;
					int jftAvailableCandidates=0;
					int jobForHiresSize=0;
					columnIncr=0;
					
					if(userMaster.getDistrictId()==null || displayCNFGColumn.get("JID")){
						label = new Label(columnIncr, k, ""+jobOrderDetails.getJobId()); 
						excelSheet.addCell(label);
						++columnIncr;
					}
					
					if(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==2)						
					{
					   if(userMaster.getEntityType() ==2 && hrIntegratedFlag){
							if(hrStatusMap.containsKey(jobOrderDetails.getJobId()))
							{
								label = new Label(columnIncr, k,""+hrStatusMap.get(jobOrderDetails.getJobId()).getDistrictRequisitionNumbers().getRequisitionNumber()); 
								  excelSheet.addCell(label);
								  ++columnIncr;
							}
							else
							{
								label = new Label(columnIncr, k,""+Utility.getLocaleValuePropByKey("optN/A", locale)); 
								  excelSheet.addCell(label);
								  ++columnIncr;
							}
					   }else if(userMaster.getEntityType() == 1){
						    if(hrStatusMap.containsKey(jobOrderDetails.getJobId()))
							{
								label = new Label(columnIncr, k,""+hrStatusMap.get(jobOrderDetails.getJobId()).getDistrictRequisitionNumbers().getRequisitionNumber()); 
								  excelSheet.addCell(label);
								  ++columnIncr;
							}
							else
							{
								label = new Label(columnIncr, k,""+Utility.getLocaleValuePropByKey("optN/A", locale)); 
								  excelSheet.addCell(label);
								  ++columnIncr;
							}
					   }
					}
					
					if(userMaster.getDistrictId()==null || displayCNFGColumn.get("TIT")){
						label = new Label(columnIncr, k,jobOrderDetails.getJobTitle()); 
						excelSheet.addCell(label);
						++columnIncr;
					}
					
					if(userMaster.getDistrictId()==null || displayCNFGColumn.get("SCH")){
						if(jobOrderDetails.getSchool()!=null && jobOrderDetails.getSchool().size()>1)
						{
	
							String schoolListNames = jobOrderDetails.getSchool().get(0).getSchoolName();
							for(int l=1;l<jobOrderDetails.getSchool().size();l++)
							{
								schoolListNames = schoolListNames+" || "+jobOrderDetails.getSchool().get(l).getSchoolName();
							}
							//tmRecords.append("<td> <a href='javascript:void(0);' onclick=showSchoolList('"+schoolListIds+"')>Many Schools</a></td>");
							label = new Label(columnIncr, k,schoolListNames); 
							
						}
						else if(jobOrderDetails.getSchool().size()==1)
						{
							label = new Label(columnIncr, k,jobOrderDetails.getSchool().get(0).getSchoolName()); 
							//tmRecords.append("<td>"+jobOrder.getSchool().get(0).getSchoolName()+"</td>");
						}
						else
							label = new Label(columnIncr, k,"");
						excelSheet.addCell(label);
						++columnIncr;
					}

					if(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==2)
					{
						
					}
					else
					{
									
						if(userMaster.getDistrictId()==null || displayCNFGColumn.get("ZON"))
						{
							if(jobOrderDetails.getGeoZoneMaster()!=null){
								  label = new Label(columnIncr, k, jobOrderDetails.getGeoZoneMaster().getGeoZoneName()); 
								  excelSheet.addCell(label);
								  ++columnIncr;
								} else {
								  label = new Label(columnIncr, k, ""); 
								  excelSheet.addCell(label);
								  ++columnIncr;
								}
						}
						if(userMaster.getDistrictId()==null || displayCNFGColumn.get("SUB")){				
							if(jobOrderDetails.getSubjectMaster()!=null){
							  label = new Label(columnIncr, k,jobOrderDetails.getSubjectMaster().getSubjectName()); 
							  excelSheet.addCell(label);
							  ++columnIncr;
							}else{
							  label = new Label(columnIncr, k, ""); 
							  excelSheet.addCell(label);
							  ++columnIncr;
							}
						}
					}
					
					if(userMaster.getDistrictId()==null || displayCNFGColumn.get("STA")){
						if(jobOrderDetails.getStatus().equalsIgnoreCase("A")){
						  label = new Label(columnIncr, k, Utility.getLocaleValuePropByKey("optAct", locale)); 
						  excelSheet.addCell(label);
						  ++columnIncr;
						}else{
						  label = new Label(columnIncr, k, Utility.getLocaleValuePropByKey("optInActiv", locale)); 
						  excelSheet.addCell(label);
						  ++columnIncr;
						}
					}

					if(userMaster.getDistrictId()==null || displayCNFGColumn.get("POSU")){
						if(Utility.convertDateAndTimeToUSformatOnlyDate(jobOrderDetails.getJobEndDate()).equals("Dec 25, 2099"))
							label = new Label(columnIncr, k, " "+Utility.getLocaleValuePropByKey("lblUntilfilled", locale)+"");
						else
						label = new Label(columnIncr, k, Utility.convertDateAndTimeToUSformatOnlyDate(jobOrderDetails.getJobEndDate())+",11:59 PM CST"); 
						excelSheet.addCell(label);
						++columnIncr;
					}
					
					if(userMaster.getDistrictId()==null || displayCNFGColumn.get("POS")){
						label = new Label(columnIncr, k, positionMap.get(""+jobOrderDetails.getJobId()));
						excelSheet.addCell(label);
						++columnIncr;
					}
					 
					if(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==2)						
					{							
						if(userMaster.getEntityType() == 2 && hrIntegratedFlag){
							if(hrStatusMap.containsKey(jobOrderDetails.getJobId()))
							{
								String valueStatus="";
								String statusValue=hrStatusMap.get(jobOrderDetails.getJobId()).getDistrictRequisitionNumbers().getPostionStatus();
								if(statusValue!=null)
								{
								if(statusValue.equals("51"))
								{
									valueStatus="Vacant";
								}else if(statusValue.equals("01"))
								{
									valueStatus="Staffed � Fully";
								}else if(statusValue.equals("02"))
								{
									valueStatus="Staffed � Partially";
								}else if(statusValue.equals("60"))
								{
									valueStatus="Frozen";
								}else if(statusValue.equals("00"))
								{
									valueStatus="Inactive Position";
								}else if(statusValue.equals("PV"))
								{
									valueStatus="Pending Vacant";
								}
								}
								
								label = new Label(columnIncr, k,""+valueStatus); 
								  excelSheet.addCell(label);
								  ++columnIncr;
							}
							else
							{
								label = new Label(columnIncr, k,""+Utility.getLocaleValuePropByKey("optN/A", locale)); 
								  excelSheet.addCell(label);
								  ++columnIncr;
							}
						}else if(userMaster.getEntityType() == 1){
							
							if(hrStatusMap.containsKey(jobOrderDetails.getJobId()))
							{
								String valueStatus="";
								String statusValue=hrStatusMap.get(jobOrderDetails.getJobId()).getDistrictRequisitionNumbers().getPostionStatus();
								if(statusValue!=null)
								{
								if(statusValue.equals("51"))
								{
									valueStatus="Vacant";
								}else if(statusValue.equals("01"))
								{
									valueStatus="Staffed � Fully";
								}else if(statusValue.equals("02"))
								{
									valueStatus="Staffed � Partially";
								}else if(statusValue.equals("60"))
								{
									valueStatus="Frozen";
								}else if(statusValue.equals("00"))
								{
									valueStatus="Inactive Position";
								}else if(statusValue.equals("PV"))
								{
									valueStatus="Pending Vacant";
								}
								}
								
								label = new Label(columnIncr, k,""+valueStatus); 
								  excelSheet.addCell(label);
								  ++columnIncr;
							}
							else
							{
								label = new Label(columnIncr, k,""+Utility.getLocaleValuePropByKey("optN/A", locale)); 
								  excelSheet.addCell(label);
								  ++columnIncr;
							}
							
						}
					}
					//Total/available/"+Utility.getLocaleValuePropByKey("lblHired", locale)+"
					String appliedStr = null;
					appliedStr = map.get(jobOrderDetails.getJobId());
					if(userMaster.getDistrictId()==null || displayCNFGColumn.get("APP")){
						 if(appliedStr!=null){
							 label = new Label(columnIncr, k, appliedStr.split("##")[0]+"/"+appliedStr.split("##")[1]); 
							  excelSheet.addCell(label);
							  ++columnIncr;
						 }else
						 {
							 label = new Label(columnIncr, k, "0/0"); 
							  excelSheet.addCell(label);
							  ++columnIncr;
						 }
					}

					if(userMaster.getDistrictId()==null || displayCNFGColumn.get("HPER")){
						 label = new Label(columnIncr, k, hireCandidatePercentageMap.get(""+jobOrderDetails.getJobId())); 
						 excelSheet.addCell(label);
						 ++columnIncr;
					}
					  
				 k++;
				}
			}
		 workbook.write();
		 workbook.close();
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return fileNames;
	}


public String displayRecordsByEntityTypePDFExport(boolean resultFlag,int JobOrderType,int districtOrSchoolId,int schoolId,/*String subjectId,String certifications,*/String jobOrderIds,String status,String noOfRow,
		String pageNo,String sortOrder,String sortOrderType,String  disJobReqNo,String geoZonId,String jobCategoryIds,String jobApplicationStatus,String jobSubCategoryIds)
 {
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
		throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	}
	try
	{	
		String fontPath = request.getRealPath("/");
		BaseFont.createFont(fontPath+"fonts/tahoma.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
		BaseFont tahoma = BaseFont.createFont(fontPath+"fonts/tahoma.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);

		UserMaster userMaster = null;
		if (session == null || session.getAttribute("userMaster") == null) {
			//return "false";
		}else
			userMaster = (UserMaster)session.getAttribute("userMaster");

		int userId = userMaster.getUserId();

		
		String time = String.valueOf(System.currentTimeMillis()).substring(6);
		String basePath = context.getServletContext().getRealPath("/")+"/user/"+userId+"/";
		String fileName =time+"joborder.pdf";

		File file = new File(basePath);
		if(!file.exists())
			file.mkdirs();

		Utility.deleteAllFileFromDir(basePath);
		System.out.println("user/"+userId+"/"+fileName);
		generateJobOrderPDReport(resultFlag,JobOrderType,districtOrSchoolId,schoolId,/*subjectId,certifications,*/jobOrderIds,status,noOfRow,
				pageNo,sortOrder,sortOrderType,disJobReqNo,geoZonId,basePath+"/"+fileName,context.getServletContext().getRealPath("/"),jobCategoryIds,jobApplicationStatus,jobSubCategoryIds);
		return "user/"+userId+"/"+fileName;
	}
	catch(Exception e)
	{
		e.printStackTrace();
	}
	return "ABV";
 }


public boolean generateJobOrderPDReport(boolean resultFlag,int JobOrderType,int districtOrSchoolId,int schoolId,/*String subjectId,String certifications,*/String jobOrderIds,String status,String noOfRow,
		String pageNo,String sortOrder,String sortOrderType,String  disJobReqNo,String geoZonId,String path,String realPath,String jobCategoryIds,String jobApplicationStatus,String jobSubCategoryIds)
{
	Document document=null;
	FileOutputStream fos = null;
	PdfWriter writer = null;
	Paragraph footerpara = null;
	HeaderFooter headerFooter = null;
	
	Font font8 = null;
	Font font8Green = null;
	Font font8bold = null;
	Font font9 = null;
	Font font9bold = null;
	Font font10 = null;
	Font font10_10 = null;
	Font font10bold = null;
	Font font11 = null;
	Font font11bold = null;
	Font font20bold = null;
	Font font11b   =null;
	Color bluecolor =null;
	Font font8bold_new = null;
	
	String fileName = null;
	int jobApplicationStatusId=0;
	if(jobApplicationStatus!=null && jobApplicationStatus!="" &&!jobApplicationStatus.equals(""))
		jobApplicationStatusId =  Integer.parseInt(jobApplicationStatus);
	int	geoZoneId=0;
	if(geoZonId!=null && geoZonId!="" &&!geoZonId.equals(""))
	  geoZoneId =  Integer.parseInt(geoZonId);
	
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	}
	String fileNames = null;
	try{
		UserMaster userMaster = null;
		Integer entityID=null;
		DistrictMaster districtMaster=null;
		SchoolMaster schoolMaster=null;
		SubjectMaster subjectMaster	=	null;
		int roleId=0;
		if (session == null || session.getAttribute("userMaster") == null) {
			//return "false";
		}else{
			userMaster=(UserMaster)session.getAttribute("userMaster");
			
			if(userMaster.getSchoolId()!=null)
				schoolMaster =userMaster.getSchoolId();
			

			if(userMaster.getDistrictId()!=null){
				districtMaster=userMaster.getDistrictId();
			}

			entityID=userMaster.getEntityType();
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
		}
		String roleAccess=null;
		try{
			if(JobOrderType==3){
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,25,"schooljoborders.do",0);
			}else{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,24,"managejoborders.do",0);
			}
		}catch(Exception e){
			e.printStackTrace();
		}

		/** set default sorting fieldName **/
		String sortOrderFieldName="createdDateTime";
		String sortOrderNoField="jobId";

		boolean deafultFlag=false;
		/**Start set dynamic sorting fieldName **/
		Order  sortOrderStrVal=null;
		if(sortOrder!=null){
			if(!sortOrder.equals("") && !sortOrder.equals(null) && !sortOrder.equals("ofApplicant") && !sortOrder.equals("ofHire")){
				sortOrderFieldName=sortOrder;
				sortOrderNoField=sortOrder;
				deafultFlag=false;
			}else{
				deafultFlag=true;
			}
			if(sortOrder.equals("ofApplicant")){
				sortOrderNoField="ofApplicant";
			}
			if(sortOrder.equals("ofHire")){
				sortOrderNoField="ofHire";
			}
		}
		String sortOrderTypeVal="0";
		sortOrderStrVal=Order.asc(sortOrderFieldName);
		if(sortOrderType!=null){
			if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
				if(sortOrderType.equals("1")){
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}
		}
		if(sortOrderType.equals("")){
			sortOrderStrVal=Order.desc(sortOrderFieldName);
		}

		/**End ------------------------------------**/
		List<JobOrder> lstJobOrder	  =	 new ArrayList<JobOrder>();
		boolean jobOrderIdFlag=false;
		boolean statusFlag=false;

		Criterion criterionStatus=null;
		Criterion criterionJobFilter=null;
		if(!(jobOrderIds.equals("0") || jobOrderIds.equalsIgnoreCase("0"))){
			String [] jobsArr =  jobOrderIds.split(",");
			List<Integer> jobs = new ArrayList<Integer>();
			for(int i=0;i<jobsArr.length;i++)
			{	
				String jobId = jobsArr[i].trim();
				try{if(!jobId.equals(""))
					jobs.add(Integer.parseInt(jobId));
				}catch(Exception e){}
			}
			if(jobs.size()>0)
			{
				jobOrderIdFlag=true;
				criterionJobFilter	= Restrictions.in("jobId",jobs);
			}
		}
		if(status!=null){
			if(!status.equalsIgnoreCase("") && !status.equalsIgnoreCase(null)){
				statusFlag=true;
				criterionStatus=Restrictions.eq("status",status);
			}
		}else{
			statusFlag=false;
		}

		List<JobOrder> filterJobOrderList = new ArrayList<JobOrder>();
		
		Criterion criterionSubject=null;
		/*if(!subjectId.equalsIgnoreCase(""))
		{
			subjectMaster=	subjectMasterDAO.findById(Integer.parseInt(subjectId), false, false);
			criterionSubject=Restrictions.eq("subjectMaster",subjectMaster);
		}*/
		/*************Start *************/
		

		boolean jobFlag=false;
		
		
		/*List<JobOrder> certJobOrderList	  =	 new ArrayList<JobOrder>();
		boolean certTypeFlag=false;
		if(certifications!=null && !certifications.equals("") && !certifications.equals("0")){
			certTypeFlag=true;
			certJobOrderList=jobCertificationDAO.findCertificationByJob(certifications);
		}
		if(certTypeFlag && certJobOrderList.size()>0){
			if(jobFlag){
				filterJobOrderList.retainAll(certJobOrderList);
			}else{
				filterJobOrderList.addAll(certJobOrderList);
			}
		}
		if(certTypeFlag){
			jobFlag=true;
		}*/
		/*List<JobOrder> subjectJList	  =	 new ArrayList<JobOrder>();
		boolean subjectFlag=false;
		if(subjectId!=null && !subjectId.equals("") && !subjectId.equals("0")){
			SubjectMaster sMaster=null;
			if(subjectId!=null){
				sMaster=subjectMasterDAO.findById(Integer.parseInt(subjectId),false,false);
			}
			subjectFlag=true;
			subjectJList=jobOrderDAO.findJobBySubject(sMaster);
		}
		if(subjectFlag && subjectJList.size()>0){
			if(jobFlag){
				filterJobOrderList.retainAll(subjectJList);
			}else{
				filterJobOrderList.addAll(subjectJList);
			}
		}
		if(subjectFlag){
			jobFlag=true;
		}*/

		DistrictMaster  distMaster=null;
		List<JobOrder> districtJList	  =	 new ArrayList<JobOrder>();
		boolean districtFlag=false;
		if(districtOrSchoolId!=0){
			if(districtOrSchoolId!=0){
				distMaster=districtMasterDAO.findById(districtOrSchoolId,false,false);
			}
			districtFlag=true;
			districtJList=jobOrderDAO.findJobByDistrict(distMaster);
		}
		if(districtFlag && districtJList.size()>0){
			if(jobFlag){
				filterJobOrderList.retainAll(districtJList);
			}else{
				filterJobOrderList.addAll(districtJList);
			}
		}
		if(districtFlag){
			jobFlag=true;
		}

		Criterion criterionJobOrderType = Restrictions.eq("createdForEntity",JobOrderType);
		
		List<JobOrder> schoolJList	  =	 new ArrayList<JobOrder>();
		
		boolean schoolFlag=false;
		int searchschoolflag=2;
		if(schoolId!=0){
			SchoolMaster  sclMaster=null;
			if(districtOrSchoolId!=0){
				sclMaster=schoolMasterDAO.findById(Long.parseLong(schoolId+""),false,false);
			}
			schoolFlag=true;
			searchschoolflag=1;
			if(distMaster!=null)
				schoolJList=schoolInJobOrderDAO.findJobListBySchool(sclMaster,distMaster,JobOrderType,entityID,searchschoolflag,userMaster);
		}
		else
		{
			if(userMaster.getEntityType()==3 && JobOrderType==2 && userMaster.getDistrictId()!=null && distMaster.getWritePrivilegeToSchool().equals(true))
			{
				searchschoolflag=0; // below [ schoolMaster ] is sending from school id by getting  session 
				schoolJList=schoolInJobOrderDAO.findJobListBySchool(schoolMaster,distMaster,JobOrderType,entityID,searchschoolflag,userMaster);
				schoolFlag=true;
			}
		}
		if(schoolFlag && schoolJList.size()>0){
			if(jobFlag){
				filterJobOrderList.retainAll(schoolJList);
			}else{
				filterJobOrderList.addAll(schoolJList);
			}
		}else if(schoolFlag && schoolJList.size()==0){
			filterJobOrderList = new ArrayList<JobOrder>();
		}

		if(schoolFlag){
			jobFlag=true;
		}
		
		boolean jobReqnoFlag=false;
		List<JobOrder> jobOrderOfReqNoList	  =	 new ArrayList<JobOrder>();
		if(disJobReqNo!=null && !disJobReqNo.equals("") && !disJobReqNo.equals("0")){
			jobReqnoFlag=true;
			List<DistrictRequisitionNumbers> districtRequisitionNumbers=null;
			Criterion crDrq=Restrictions.eq("requisitionNumber", disJobReqNo);
			Criterion crDSTrq=null;
			if(districtOrSchoolId!=0){
				crDSTrq=Restrictions.eq("districtMaster", distMaster);
				districtRequisitionNumbers=districtRequisitionNumbersDAO.findByCriteria(crDSTrq,crDrq);
			}else{
				districtRequisitionNumbers=districtRequisitionNumbersDAO.findByCriteria(crDrq);
			}
			if(districtRequisitionNumbers!=null && districtRequisitionNumbers.size()>0){
				
				Criterion crJrq=Restrictions.in("districtRequisitionNumbers", districtRequisitionNumbers);
				List<JobRequisitionNumbers> jobRequisitionNumbers=jobRequisitionNumbersDAO.findByCriteria(crJrq);
				for(JobRequisitionNumbers jqr:jobRequisitionNumbers){
					jobOrderOfReqNoList.add(jqr.getJobOrder());
				}
			}
		}
		
		if(jobReqnoFlag && jobOrderOfReqNoList.size()>0){
			if(jobFlag){
				filterJobOrderList.retainAll(jobOrderOfReqNoList);
			}else{
				filterJobOrderList.addAll(jobOrderOfReqNoList);
			}
		}else if(jobReqnoFlag && jobOrderOfReqNoList.size()==0){
			
			filterJobOrderList = new ArrayList<JobOrder>();
		}
		
		if(jobReqnoFlag){
			jobFlag=true;
		}
		
		// =============  Deepak filtering basis on job Status  added by 27-05-2015
		if(jobApplicationStatusId>0 && distMaster!=null && distMaster.getDistrictId().toString().equalsIgnoreCase("804800"))
		{
			boolean jobApplicationStatusFlag = false;
				List<JobOrder> jobApplicationStatusList = new ArrayList<JobOrder>();
					if (disJobReqNo != null && jobApplicationStatusId > 0) {
						jobApplicationStatusFlag = true;
						jobApplicationStatusList = jobOrderDAO.getJobApplicationStatusRole(jobApplicationStatusId);
						System.out.println("jobApplicationStatusList========"+ jobApplicationStatusList.size());
	
					}
					if (jobApplicationStatusFlag
							&& jobApplicationStatusList.size() > 0) {
						if (jobFlag == true) {
							filterJobOrderList.retainAll(jobApplicationStatusList);
						} else {
							filterJobOrderList.addAll(jobApplicationStatusList);
						}
					} else {
						filterJobOrderList = new ArrayList<JobOrder>();
					}
			if (jobApplicationStatusFlag) {
				jobFlag = true;
			}
		}
		
		
		List<JobOrder> listjobOrdersgeoZone =new ArrayList<JobOrder>();
		
		  if(geoZoneId>0)
			{
			  GeoZoneMaster geoZoneMaster=geoZoneMasterDAO.findById(geoZoneId, false, false);
				if(userMaster.getRoleId().getRoleId().equals(3))
					listjobOrdersgeoZone = jobOrderDAO.getJobOrderBygeoZoneRole(geoZoneMaster,userMaster);
				else
					listjobOrdersgeoZone = jobOrderDAO.getJobOrderBygeoZone(geoZoneMaster);
			}
		 
		  if(listjobOrdersgeoZone!=null && listjobOrdersgeoZone.size()>0)
		  {
			  if(jobFlag==true){
				  	filterJobOrderList.retainAll(listjobOrdersgeoZone);
				}else{
					filterJobOrderList.addAll(listjobOrdersgeoZone);
				}
		  }	
		  if(listjobOrdersgeoZone.size()==0 && geoZoneId==-1 ||geoZoneId==0)
		  {
			  filterJobOrderList.addAll(listjobOrdersgeoZone);
		  
		  }
		  else if(listjobOrdersgeoZone.size()==0 &&geoZoneId!=-1)
		  {
			  filterJobOrderList.retainAll(listjobOrdersgeoZone);
		  }
		  
		 /* List<JobOrder> listjJobOrders = new ArrayList<JobOrder>();
			boolean jobCateFlag=false;
			if(jobCategoryIds!=null && !jobCategoryIds.equals("") && !jobCategoryIds.equals("0"))
			{
				List<Integer> jobcatIds = new ArrayList<Integer>();
				
				String jobCategoryIdStr[] =jobCategoryIds.split(",");
			  if(!ArrayUtils.contains(jobCategoryIdStr, "0"))
			  {		  
				for(String str : jobCategoryIdStr){
				 if(!str.equals(""))
					 jobcatIds.add(Integer.parseInt(str));
				 } 
				 if(jobcatIds.size()>0){
					 listjJobOrders = jobOrderDAO.findByJobCategery(jobcatIds,districtOrSchoolId);
					 System.out.println(" Oooooooooooo listjJobOrders "+listjJobOrders.size());
					 jobCateFlag=true;
				 }
				 if(listjJobOrders!=null && listjJobOrders.size()>0){
						if(jobCateFlag){
							filterJobOrderList.retainAll(listjJobOrders);
						}else{
							filterJobOrderList.addAll(listjJobOrders);
						}
					 }
				 }
			  }

			if(jobCateFlag){
				jobFlag=true;
			}*/
		  
		  

		  if(jobSubCategoryIds!=null && !jobSubCategoryIds.equals("") && !jobSubCategoryIds.equals("0"))
		  			  {
		  				//Job Sub Category
		  					List<JobOrder> listJobOrdersBySubCatgry = new ArrayList<JobOrder>();
		  					boolean jobSubCateFlag=false;
		  					
		  					if(jobSubCategoryIds!=null && !jobSubCategoryIds.equals("") && !jobSubCategoryIds.equals("0"))
		  					{
		  						List<Integer> jobSubcatIds = new ArrayList<Integer>();
		  						
		  						String jobSubCategoryIdStr[] = jobSubCategoryIds.split(",");
		  					  if(!ArrayUtils.contains(jobSubCategoryIdStr, "0"))
		  					  {		  
		  						for(String str : jobSubCategoryIdStr){
		  						 if(!str.equals(""))
		  							 jobSubcatIds.add(Integer.parseInt(str));
		  						 } 
		  						 if(jobSubcatIds.size()>0){
		  							 listJobOrdersBySubCatgry = jobOrderDAO.findByJobCategery(jobSubcatIds,districtOrSchoolId);
		  							 System.out.println("  job list by jobcategory :: "+listJobOrdersBySubCatgry.size());
		  							 jobSubCateFlag=true;
		  						 }
		  						 if(jobSubCateFlag){
		  							filterJobOrderList.retainAll(listJobOrdersBySubCatgry);
		  						 }else{
		  							filterJobOrderList.addAll(listJobOrdersBySubCatgry);
		  						 }
		  						}
		  					  }

		  					if(jobSubCateFlag){
		  						jobFlag=true;
		  					}
		  			  }
		  			  else
		  			  {
		  				// Job category
		  					List<JobOrder> listjJobOrders = new ArrayList<JobOrder>();
		  					boolean jobCateFlag=false;
		  					if(jobCategoryIds!=null && !jobCategoryIds.equals("") && !jobCategoryIds.equals("0"))
		  					{
		  						List<Integer> jobcatIds = new ArrayList<Integer>();
		  						
		  						String jobCategoryIdStr[] =jobCategoryIds.split(",");
		  					  if(!ArrayUtils.contains(jobCategoryIdStr, "0"))
		  					  {		  
		  						for(String str : jobCategoryIdStr){
		  						 if(!str.equals(""))
		  							 jobcatIds.add(Integer.parseInt(str));
		  						 } 
		  						 if(jobcatIds.size()>0){
		  							 listjJobOrders = jobOrderDAO.findByJobCategery(jobcatIds,districtOrSchoolId);
		  							 System.out.println("  job list by jobcategory :: "+listjJobOrders.size());
		  							 jobCateFlag=true;
		  						 }
		  						 if(jobCateFlag){
		  							filterJobOrderList.retainAll(listjJobOrders);
		  						 }else{
		  							filterJobOrderList.addAll(listjJobOrders);
		  						 }
		  						}
		  					  }

		  					if(jobCateFlag){
		  						jobFlag=true;
		  					}
		  				  
		  			  }
		  
		  
		  
		  
		  
		  
		  
			
	      if(jobFlag){
			if(filterJobOrderList.size()>0){
				List jobIds = new ArrayList();
				for(int e=0; e<filterJobOrderList.size(); e++) {
					jobIds.add(filterJobOrderList.get(e).getJobId());
				}
				Criterion criterionJobList = Restrictions.in("jobId",jobIds);

				Criterion criterionMix=criterionJobOrderType;
				if(statusFlag && jobOrderIdFlag){
					criterionMix=Restrictions.and(criterionStatus,criterionJobFilter);
				}else if(statusFlag){
					criterionMix=criterionStatus;
				}else if(jobOrderIdFlag){
					criterionMix=criterionJobFilter;
				}
				lstJobOrder=jobOrderDAO.findByCriteria(sortOrderStrVal,criterionJobList,criterionJobOrderType,criterionMix);
			}
		}else{
			Criterion criterionMix=criterionJobOrderType;
			if(statusFlag && jobOrderIdFlag){
				criterionMix=Restrictions.and(criterionStatus,criterionJobFilter);
			}else if(statusFlag){
				criterionMix=criterionStatus;
			}else if(jobOrderIdFlag){
				criterionMix=criterionJobFilter;
			}
			lstJobOrder=jobOrderDAO.findByCriteria(sortOrderStrVal,criterionJobOrderType,criterionMix);
			
		}
	      
	      
	     
	      
	      
		/***************************** End Code **************************************/
	      Map<Integer,String> map = new HashMap<Integer, String>();
	      if(lstJobOrder!=null && lstJobOrder.size()>0)
		   map = jobForTeacherDAO.countApplicantsByJobOrdersAllll(lstJobOrder);
	      String fontPath = realPath;
		   try {
				
				BaseFont.createFont(fontPath+"fonts/4637.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
				BaseFont tahoma = BaseFont.createFont(fontPath+"fonts/4637.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
				font8 = new Font(tahoma, 8);

				font8Green = new Font(tahoma, 8);
				font8Green.setColor(Color.BLUE);
				font8bold = new Font(tahoma, 8, Font.NORMAL);


				font9 = new Font(tahoma, 9);
				font9bold = new Font(tahoma, 9, Font.BOLD);
				font10 = new Font(tahoma, 10);
				
				font10_10 = new Font(tahoma, 10);
				font10_10.setColor(Color.white);
				font10bold = new Font(tahoma, 10, Font.BOLD);
				font11 = new Font(tahoma, 11);
				font11bold = new Font(tahoma, 11,Font.BOLD);
				bluecolor =  new Color(0,122,180); 
				
				//Color bluecolor =  new Color(0,122,180); 
				
				font20bold = new Font(tahoma, 20,Font.BOLD,bluecolor);
				//font20bold.setColor(Color.BLUE);
				font11b = new Font(tahoma, 11, Font.BOLD, bluecolor);


			} 
			catch (DocumentException e1) 
			{
				e1.printStackTrace();
			} 
			catch (IOException e1) 
			{
				e1.printStackTrace();
			}
			
			document = new Document(PageSize.A4.rotate(),10f,10f,10f,10f);		
			document.addAuthor("TeacherMatch");
			document.addCreator("TeacherMatch Inc.");
			document.addSubject("");
			document.addCreationDate();
			document.addTitle("");

			fos = new FileOutputStream(path);
			PdfWriter.getInstance(document, fos);
		
			document.open();
			
			PdfPTable mainTable = new PdfPTable(1);
			mainTable.setWidthPercentage(90);

			Paragraph [] para = null;
			PdfPCell [] cell = null;


			para = new Paragraph[3];
			cell = new PdfPCell[3];
			para[0] = new Paragraph(" ",font20bold);
			cell[0]= new PdfPCell(para[0]);
			cell[0].setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell[0].setBorder(0);
			mainTable.addCell(cell[0]);
			
			//Image logo = Image.getInstance (Utility.getValueOfPropByKey("basePath")+"/images/Logo%20with%20Beta300.png");
			Image logo = Image.getInstance (fontPath+"/images/Logo with Beta300.png");
			logo.scalePercent(75);

			//para[1] = new Paragraph(" ",font20bold);
			cell[1]= new PdfPCell(logo);
			cell[1].setHorizontalAlignment(Element.ALIGN_CENTER);
			cell[1].setBorder(0);
			mainTable.addCell(cell[1]);

			document.add(new Phrase("\n"));
			
			
			para[2] = new Paragraph("",font20bold);
			cell[2]= new PdfPCell(para[2]);
			cell[2].setHorizontalAlignment(Element.ALIGN_CENTER);
			cell[2].setBorder(0);
			mainTable.addCell(cell[2]);

			document.add(mainTable);
			
			document.add(new Phrase("\n"));
			//document.add(new Phrase("\n"));
			

			float[] tblwidthz={.15f};
			
			mainTable = new PdfPTable(tblwidthz);
			mainTable.setWidthPercentage(100);
			para = new Paragraph[1];
			cell = new PdfPCell[1];
			String str="";
			if(JobOrderType==2)
				str=""+Utility.getLocaleValuePropByKey("optDistrict", locale);
			else if(JobOrderType==3)
				str=""+Utility.getLocaleValuePropByKey("optSchool", locale);
			
			String HeadingName= str+" "+Utility.getLocaleValuePropByKey("lblJobOdr", locale)+"s";
			
			para[0] = new Paragraph(HeadingName,font20bold);
			cell[0]= new PdfPCell(para[0]);
			cell[0].setHorizontalAlignment(Element.ALIGN_CENTER);
			cell[0].setBorder(0);
			mainTable.addCell(cell[0]);
			document.add(mainTable);

			document.add(new Phrase("\n"));
			//document.add(new Phrase("\n"));
			
			
	        float[] tblwidths={.15f,.20f};
			
			mainTable = new PdfPTable(tblwidths);
			mainTable.setWidthPercentage(100);
			para = new Paragraph[2];
			cell = new PdfPCell[2];

			String name1 = userMaster.getFirstName()+" "+userMaster.getLastName();
			
			para[0] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblCreatedBy", locale)+": "+name1,font10);
			cell[0]= new PdfPCell(para[0]);
			cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
			cell[0].setBorder(0);
			mainTable.addCell(cell[0]);
			
			para[1] = new Paragraph(""+""+Utility.getLocaleValuePropByKey("lblCreatedOn", locale)+": "+Utility.convertDateAndTimeToUSformatOnlyDate(new Date()),font10);
			cell[1]= new PdfPCell(para[1]);
			cell[1].setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell[1].setBorder(0);
			mainTable.addCell(cell[1]);
			
			document.add(mainTable);
			
			
			
			document.add(new Phrase("\n"));
			
	//Start Sandeep 03-09-15		
			
			  Boolean hrIntegratedFlag = false;
		      if(userMaster.getDistrictId() != null)
		    	  hrIntegratedFlag = userMaster.getDistrictId().getHrIntegrated();
			
	//end
		      
		    //configurable coloumn
		    Map<String,Boolean> displayCNFGColumn=null;
		    if(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==2){
		    	displayCNFGColumn=getCNFGColumn(districtMaster,true);
		    	if(userMaster.getEntityType() == 2 && hrIntegratedFlag)
		    		totalShowCol+=2;
		    	else if(userMaster.getEntityType() == 1){
		    		List<String> listCol=new ArrayList<String>();
		    		listCol.add("ZON");
		    		listCol.add("SUB");
		    		listCol.add("CANG"); 
		    		listCol.add("ATT");
		    		listCol.add("ACT"); 	
		    		listCol.add("REQ"); 
		    		totalShowCol=(displayCNFGColumn.size()-listCol.size());
		    		totalShowCol+=2;
		    	}
		    }else if(userMaster.getDistrictId()==null && userMaster.getEntityType() == 1){
		    	displayCNFGColumn=getCNFGColumn(districtMaster,false);
		    	List<String> listCol=new ArrayList<String>();
	    		listCol.add("CANG"); 
	    		listCol.add("ATT");
	    		listCol.add("ACT"); 	
	    		listCol.add("REQ"); 
	    		totalShowCol=(displayCNFGColumn.size()-listCol.size());
		    }
		    displayCNFGColumn=getCNFGColumn(districtMaster,false);
			
			float[] tblwidth;
				if(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==2)
				{
					if(userMaster.getEntityType() == 1 || (userMaster.getEntityType() == 2 && hrIntegratedFlag)){
						 mainTable = new PdfPTable(totalShowCol);
					}else{
						 mainTable = new PdfPTable(totalShowCol);
					}
				}
				else if(userMaster.getDistrictId()==null || userMaster.getDistrictId().getDistrictId()!=null)
				{
					 mainTable = new PdfPTable(totalShowCol);
				}else{
						 mainTable = new PdfPTable(totalShowCol);
				}
			
			
			//mainTable = new PdfPTable(tblwidth);
			mainTable.setWidthPercentage(100);
			para = new Paragraph[totalShowCol+1];
			cell = new PdfPCell[totalShowCol+1];
	// header
			int k=0;
			if(userMaster.getDistrictId()==null || displayCNFGColumn.get("JID")){
				para[k] = new Paragraph(Utility.getLocaleValuePropByKey("lblJobId", locale),font10_10);
				cell[k]= new PdfPCell(para[k]);
				cell[k].setBackgroundColor(bluecolor);
				cell[k].setHorizontalAlignment(Element.ALIGN_LEFT);
				mainTable.addCell(cell[k++]);
			}
			
			if(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==2)						
			{
			  if(userMaster.getEntityType() == 2 && hrIntegratedFlag){
				para[k] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblReq/Posi", locale),font10_10);
				cell[k]= new PdfPCell(para[k]);
				cell[k].setBackgroundColor(bluecolor);
				cell[k].setHorizontalAlignment(Element.ALIGN_LEFT);
				mainTable.addCell(cell[k++]);
			}else if(userMaster.getEntityType() == 1){
				para[k] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblReq/Posi", locale),font10_10);
				cell[k]= new PdfPCell(para[k]);
				cell[k].setBackgroundColor(bluecolor);
				cell[k].setHorizontalAlignment(Element.ALIGN_LEFT);
				mainTable.addCell(cell[k++]);
				
			}
			}		
			
			if(userMaster.getDistrictId()==null || displayCNFGColumn.get("TIT")){
				para[k] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblTitle", locale),font10_10);
				cell[k]= new PdfPCell(para[k]);
				cell[k].setBackgroundColor(bluecolor);
				cell[k].setHorizontalAlignment(Element.ALIGN_LEFT);
				//cell[1].setBorder(1);
				mainTable.addCell(cell[k++]);
			}
			
			if(userMaster.getDistrictId()==null || displayCNFGColumn.get("SCH")){
				para[k] = new Paragraph(""+Utility.getLocaleValuePropByKey("optSchool", locale),font10_10);
				cell[k]= new PdfPCell(para[k]);
				cell[k].setBackgroundColor(bluecolor);
				cell[k].setHorizontalAlignment(Element.ALIGN_LEFT);
				mainTable.addCell(cell[k++]);		
			}

			if(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==2)
			{
				
			}
			else
			{
				if(userMaster.getDistrictId()==null || displayCNFGColumn.get("ZON")){
					para[k] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblZone", locale),font10_10);
					cell[k]= new PdfPCell(para[k]);
					cell[k].setBackgroundColor(bluecolor);
					cell[k].setHorizontalAlignment(Element.ALIGN_LEFT);
					mainTable.addCell(cell[k++]);
				}
				if(userMaster.getDistrictId()==null || displayCNFGColumn.get("SUB")){
					para[k] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblSub", locale),font10_10);
					cell[k]= new PdfPCell(para[k]);
					cell[k].setBackgroundColor(bluecolor);
					cell[k].setHorizontalAlignment(Element.ALIGN_LEFT);
					mainTable.addCell(cell[k++]);	
				}
			
			}
			
			if(userMaster.getDistrictId()==null || displayCNFGColumn.get("STA")){
				para[k] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblStatus", locale),font10_10);
				cell[k]= new PdfPCell(para[k]);
				cell[k].setBackgroundColor(bluecolor);
				cell[k].setHorizontalAlignment(Element.ALIGN_LEFT);
				mainTable.addCell(cell[k++]);
			}
			
			if(userMaster.getDistrictId()==null || displayCNFGColumn.get("POSU")){
				para[k] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblPostedUntil", locale),font10_10);
				cell[k]= new PdfPCell(para[k]);
				cell[k].setBackgroundColor(bluecolor);
				cell[k].setHorizontalAlignment(Element.ALIGN_LEFT);
				mainTable.addCell(cell[k++]);
			}
			
			if(userMaster.getDistrictId()==null || displayCNFGColumn.get("POS")){
				para[k] = new Paragraph(""+Utility.getLocaleValuePropByKey("msgPositions", locale),font10_10);
				cell[k]= new PdfPCell(para[k]);
				cell[k].setBackgroundColor(bluecolor);
				cell[k].setHorizontalAlignment(Element.ALIGN_LEFT);
				mainTable.addCell(cell[k++]);
			}
					
			if(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==2)						
			{				
				if(userMaster.getEntityType() == 2 && hrIntegratedFlag){
					para[k] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblHrStatus", locale),font10_10);
					cell[k]= new PdfPCell(para[k]);
					cell[k].setBackgroundColor(bluecolor);
					cell[k].setHorizontalAlignment(Element.ALIGN_LEFT);
					mainTable.addCell(cell[k++]);
				}else if(userMaster.getEntityType() == 1){
					para[k] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblHrStatus", locale),font10_10);
					cell[k]= new PdfPCell(para[k]);
					cell[k].setBackgroundColor(bluecolor);
					cell[k].setHorizontalAlignment(Element.ALIGN_LEFT);
					mainTable.addCell(cell[k++]);
				}
			
			}			
			
			if(userMaster.getDistrictId()==null || displayCNFGColumn.get("APP")){
				para[k] = new Paragraph(""+"Applicant(s)",font10_10);
				cell[k]= new PdfPCell(para[k]);
				cell[k].setBackgroundColor(bluecolor);
				cell[k].setHorizontalAlignment(Element.ALIGN_LEFT);
				mainTable.addCell(cell[k++]);
			}
			
			if(userMaster.getDistrictId()==null || displayCNFGColumn.get("HPER")){
				para[k] = new Paragraph(""+"Hire(s)",font10_10);
				cell[k]= new PdfPCell(para[k]);
				cell[k].setBackgroundColor(bluecolor);
				cell[k].setHorizontalAlignment(Element.ALIGN_LEFT);
				mainTable.addCell(cell[k++]);
			}
			
			document.add(mainTable);
			

			/********** End :: For get Number of awailable candidates ****** */
			
			List<JobRequisitionNumbers> jobRequisitionNumbers=new ArrayList<JobRequisitionNumbers>();
			Map<Integer, JobRequisitionNumbers> hrStatusMap = new HashMap<Integer, JobRequisitionNumbers>();
			if(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==2)
			{				
			jobRequisitionNumbers=jobRequisitionNumbersDAO.getHRStatusByJobId(lstJobOrder);
			System.out.println("jobRequisitionNumbers size:::::"+jobRequisitionNumbers.size());
			if(jobRequisitionNumbers!=null && jobRequisitionNumbers.size()>0)
			{
				for(JobRequisitionNumbers hrstatus: jobRequisitionNumbers)
				{
					System.out.println("hrstatus.getJobOrder().getJobId():::"+hrstatus.getJobOrder().getJobId());
					hrStatusMap.put(hrstatus.getJobOrder().getJobId(), hrstatus);
				
				}
			}
			}
			
			if(resultFlag){			
				if(lstJobOrder.size()==0){
					  float[] tblwidth11={.10f};
						
						 mainTable = new PdfPTable(tblwidth11);
						 mainTable.setWidthPercentage(100);
						 para = new Paragraph[1];
						 cell = new PdfPCell[1];
					
						 para[0] = new Paragraph(""+Utility.getLocaleValuePropByKey("msgNorecordfound", locale)+".",font8bold);
						 cell[0]= new PdfPCell(para[0]);
						 cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
						 mainTable.addCell(cell[0]);
				     	 document.add(mainTable);	
					}
				
				Map<String, String> positionMap = getPositionsNumber(lstJobOrder);
				Map<String, String> hireCandidatePercentageMap = getHireCandidatePercentage(lstJobOrder,positionMap);
				for(JobOrder jobOrderDetails : lstJobOrder)
				{
				  int index=0;
				  if(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==2)
					  mainTable = new PdfPTable(totalShowCol);
					else if(userMaster.getDistrictId()==null || userMaster.getDistrictId().getDistrictId()!=null)
						 mainTable = new PdfPTable(totalShowCol);
					else
						  mainTable = new PdfPTable(totalShowCol);
				  //mainTable = new PdfPTable(totalShowCol);
				  mainTable.setWidthPercentage(100);
				  para = new Paragraph[totalShowCol+1];
				  cell = new PdfPCell[totalShowCol+1];
					
					
					int jobForTeacherSize=0;
					int jftAvailableCandidates=0;
					int jobForHiresSize=0;
					
					if(userMaster.getDistrictId()==null || displayCNFGColumn.get("JID")){
						 para[index] = new Paragraph(""+jobOrderDetails.getJobId(),font8bold);
						 cell[index]= new PdfPCell(para[index]);
						 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
						 mainTable.addCell(cell[index]);
						 index++;
					}
					 
					 if(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==2)						
						{
							if(userMaster.getEntityType() == 2 && hrIntegratedFlag){
								if(hrStatusMap.containsKey(jobOrderDetails.getJobId())){
									para[index] = new Paragraph(""+hrStatusMap.get(jobOrderDetails.getJobId()).getDistrictRequisitionNumbers().getRequisitionNumber(),font8bold);
									 cell[index]= new PdfPCell(para[index]);
									 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
									 mainTable.addCell(cell[index]);
									 index++;
								}
								else{
									para[index] = new Paragraph(""+Utility.getLocaleValuePropByKey("optN/A", locale),font8bold);
									 cell[index]= new PdfPCell(para[index]);
									 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
									 mainTable.addCell(cell[index]);
									 index++;
								}
							}else if(userMaster.getEntityType() == 1){
								if(hrStatusMap.containsKey(jobOrderDetails.getJobId())){
									para[index] = new Paragraph(""+hrStatusMap.get(jobOrderDetails.getJobId()).getDistrictRequisitionNumbers().getRequisitionNumber(),font8bold);
									 cell[index]= new PdfPCell(para[index]);
									 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
									 mainTable.addCell(cell[index]);
									 index++;
								}
								else{
									para[index] = new Paragraph(""+Utility.getLocaleValuePropByKey("optN/A", locale),font8bold);
									 cell[index]= new PdfPCell(para[index]);
									 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
									 mainTable.addCell(cell[index]);
									 index++;
								}
								
							}
						}
					 
					 if(userMaster.getDistrictId()==null || displayCNFGColumn.get("TIT")){
						 para[index] = new Paragraph(""+jobOrderDetails.getJobTitle(),font8bold);
						 cell[index]= new PdfPCell(para[index]);
						 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
						 mainTable.addCell(cell[index]);
						 index++;
					 }
					 
					 if(userMaster.getDistrictId()==null || displayCNFGColumn.get("SCH")){
						 String schoolListNames="";
						 if(jobOrderDetails.getSchool()!=null && jobOrderDetails.getSchool().size()>1)
							{
								schoolListNames = jobOrderDetails.getSchool().get(0).getSchoolName();
								for(int l=1;l<jobOrderDetails.getSchool().size();l++)
									schoolListNames = schoolListNames+" || "+jobOrderDetails.getSchool().get(l).getSchoolName();
							}
							else if(jobOrderDetails.getSchool().size()==1)
							schoolListNames=jobOrderDetails.getSchool().get(0).getSchoolName(); 
						 para[index] = new Paragraph(""+schoolListNames,font8bold);
						 cell[index]= new PdfPCell(para[index]);
						 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
						 mainTable.addCell(cell[index]);
						 index++;
					 }
					 
					 if(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==2)
					 {
						 
					 }
					 else
					 {
						 
						 if(userMaster.getDistrictId()==null || displayCNFGColumn.get("ZON")){
							 if(jobOrderDetails.getGeoZoneMaster()!=null){
								 para[index] = new Paragraph(""+jobOrderDetails.getGeoZoneMaster().getGeoZoneName(),font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
							} else {
								 para[index] = new Paragraph("",font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
							}
						 }
					
						 if(userMaster.getDistrictId()==null || displayCNFGColumn.get("SUB")){
							if(jobOrderDetails.getSubjectMaster()!=null){
								para[index] = new Paragraph(""+jobOrderDetails.getSubjectMaster().getSubjectName(),font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
							}else{
								para[index] = new Paragraph("",font8bold);
								cell[index]= new PdfPCell(para[index]);
								cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								mainTable.addCell(cell[index]);
								index++;
							}
						 }
					 }	
					 
					if(userMaster.getDistrictId()==null || displayCNFGColumn.get("STA")){ 
						if(jobOrderDetails.getStatus().equalsIgnoreCase("A")){
							para[index] = new Paragraph(Utility.getLocaleValuePropByKey("optAct", locale),font8bold);
							cell[index]= new PdfPCell(para[index]);
							cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
							mainTable.addCell(cell[index]);
							index++;
						}else{
							para[index] = new Paragraph(Utility.getLocaleValuePropByKey("optInActiv", locale),font8bold);
							cell[index]= new PdfPCell(para[index]);
							cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
							mainTable.addCell(cell[index]);
							index++;
						}
					}
					
					if(userMaster.getDistrictId()==null || displayCNFGColumn.get("POSU")){
						if(Utility.convertDateAndTimeToUSformatOnlyDate(jobOrderDetails.getJobEndDate()).equals("Dec 25, 2099"))
						para[index] = new Paragraph(" "+Utility.getLocaleValuePropByKey("lblUntilfilled", locale)+"",font8bold);
						else
						para[index] = new Paragraph(Utility.convertDateAndTimeToUSformatOnlyDate(jobOrderDetails.getJobEndDate())+",11:59 PM CST",font8bold);
						cell[index]= new PdfPCell(para[index]);
						cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[index]);
						index++;
					}
					
					if(userMaster.getDistrictId()==null || displayCNFGColumn.get("POS")){
						para[index] = new Paragraph(positionMap.get(""+jobOrderDetails.getJobId()),font8bold);
						cell[index]= new PdfPCell(para[index]);
						cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[index]);
						index++;
					}
					
					if(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==2)						
					{
						if(userMaster.getEntityType() == 2 && hrIntegratedFlag){	
						    if(hrStatusMap.containsKey(jobOrderDetails.getJobId()))
							{	
								String valueStatus="";
								String statusValue=hrStatusMap.get(jobOrderDetails.getJobId()).getDistrictRequisitionNumbers().getPostionStatus();
								if(statusValue!=null)
								{
								if(statusValue.equals("51"))
								{
									valueStatus="Vacant";
								}else if(statusValue.equals("01"))
								{
									valueStatus="Staffed � Fully";
								}else if(statusValue.equals("02"))
								{
									valueStatus="Staffed � Partially";
								}else if(statusValue.equals("60"))
								{
									valueStatus="Frozen";
								}else if(statusValue.equals("00"))
								{
									valueStatus="Inactive Position";
								}else if(statusValue.equals("PV"))
								{
									valueStatus="Pending Vacant";
								}
								}
								
								para[index] = new Paragraph(""+valueStatus,font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
							
							}
							else
							{
								para[index] = new Paragraph(""+Utility.getLocaleValuePropByKey("optN/A", locale),font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								
							}
						}else if(userMaster.getEntityType() == 1){
							
							if(hrStatusMap.containsKey(jobOrderDetails.getJobId()))
							{	
								String valueStatus="";
								String statusValue=hrStatusMap.get(jobOrderDetails.getJobId()).getDistrictRequisitionNumbers().getPostionStatus();
								if(statusValue!=null)
								{
								if(statusValue.equals("51"))
								{
									valueStatus="Vacant";
								}else if(statusValue.equals("01"))
								{
									valueStatus="Staffed � Fully";
								}else if(statusValue.equals("02"))
								{
									valueStatus="Staffed � Partially";
								}else if(statusValue.equals("60"))
								{
									valueStatus="Frozen";
								}else if(statusValue.equals("00"))
								{
									valueStatus="Inactive Position";
								}else if(statusValue.equals("PV"))
								{
									valueStatus="Pending Vacant";
								}
								}
								
								para[index] = new Paragraph(""+valueStatus,font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
							
							}
							else
							{
								para[index] = new Paragraph(""+Utility.getLocaleValuePropByKey("optN/A", locale),font8bold);
								 cell[index]= new PdfPCell(para[index]);
								 cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
								 mainTable.addCell(cell[index]);
								 index++;
								
							}
						}
					}
					
					String appliedStr = null;
					appliedStr = map.get(jobOrderDetails.getJobId());
					if(userMaster.getDistrictId()==null || displayCNFGColumn.get("APP")){
						 if(appliedStr!=null){
							para[index] = new Paragraph(appliedStr.split("##")[0]+"/"+appliedStr.split("##")[1],font8bold);
							cell[index]= new PdfPCell(para[index]);
							cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
							mainTable.addCell(cell[index]);
							index++;
						 }else
						 {
							para[index] = new Paragraph("0/0",font8bold);
							cell[index]= new PdfPCell(para[index]);
							cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
							mainTable.addCell(cell[index]);
							index++;
						 }
					}
					
					if(userMaster.getDistrictId()==null || displayCNFGColumn.get("HPER")){
						para[index] = new Paragraph(hireCandidatePercentageMap.get(""+jobOrderDetails.getJobId()),font8bold);
						cell[index]= new PdfPCell(para[index]);
						cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[index]);
						index++;
					}
				document.add(mainTable);
				}
			}
	 }	
		catch (Exception e) {
			e.printStackTrace();
		}finally
		{
			if(document != null && document.isOpen())
				document.close();
			if(writer!=null){
				writer.flush();
				writer.close();
			}
		}
	return true;
}
@Transactional(readOnly=false)
public String[] approveJob(int entityType,Integer jobId,Integer userId)
{
	/* ========  For Session time Out Error =========*/
	String aData[] = new String[2];
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	UserMaster userMaster=null;
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	}else{
		userMaster=(UserMaster)session.getAttribute("userMaster");
	}
	try{
		
		JobOrder  jobOrder = jobOrderDAO.findById(jobId, false, false);
		aData[0]= jobOrder.getJobTitle();
		DistrictMaster districtMaster = jobOrder.getDistrictMaster();
		JobCategoryMaster jobCategoryMaster = jobOrder.getJobCategoryMaster();
		Integer noOfApprovalNeeded = districtMaster.getNoOfApprovalNeeded()==null?0:districtMaster.getNoOfApprovalNeeded();
		Integer approvalBeforeGoLive = jobOrder.getApprovalBeforeGoLive()==null?0:jobOrder.getApprovalBeforeGoLive();
		System.out.println("approvalBeforeGoLive:: "+approvalBeforeGoLive);
		if(approvalBeforeGoLive==1)
		{
			aData[1] = "2";
			return aData; // already approved
		}
		
		if( (approvalBeforeGoLive!=1 && jobCategoryMaster!=null && jobCategoryMaster.getApprovalBeforeGoLive()!=null && jobCategoryMaster.getBuildApprovalGroup()!=null && jobCategoryMaster.getApprovalBeforeGoLive() && jobCategoryMaster.getBuildApprovalGroup())  || (jobCategoryMaster.getApprovalByPredefinedGroups()!=null && jobCategoryMaster.getApprovalByPredefinedGroups()) )
		{
			String primaryEmailAddress="";
			String backupEmailAddress="";
			if(jobCategoryMaster.getApprovalByPredefinedGroups()!=null && jobCategoryMaster.getApprovalByPredefinedGroups() && jobOrder.getEmploymentServicesTechnician()!=null)
			{
				primaryEmailAddress = jobOrder.getEmploymentServicesTechnician().getPrimaryEmailAddress()==null? "" : jobOrder.getEmploymentServicesTechnician().getPrimaryEmailAddress().trim();
				backupEmailAddress = jobOrder.getEmploymentServicesTechnician().getBackupEmailAddress()==null? "" : jobOrder.getEmploymentServicesTechnician().getBackupEmailAddress().trim();
			}
			
			String useremailAddress = userMaster.getEmailAddress().trim();
			if(userMaster.getEntityType().equals(3) || useremailAddress.equalsIgnoreCase(primaryEmailAddress) || useremailAddress.equalsIgnoreCase(backupEmailAddress) || (jobCategoryMaster.getApprovalByPredefinedGroups()!=null && jobCategoryMaster.getApprovalByPredefinedGroups()) )
			{
				aData[1] = "4";//Approve job
				return aData;
			}
			
			List<DistrictKeyContact> districtKeyContactList = new ArrayList<DistrictKeyContact>();
			districtKeyContactList = districtKeyContactDAO.findByContactType(districtMaster,"Job Approval");
			UserMaster user = null;
			DistrictKeyContact userKeyContact=null;
			DistrictApprovalGroups userApprovalGroup=null;
			for (DistrictKeyContact districtKeyContact : districtKeyContactList) {
			//	System.out.println("districtKeyContact.getUserMaster().getUserId():: "+districtKeyContact.getUserMaster().getUserId());
				if(districtKeyContact.getUserMaster().getUserId().equals(userId))
				{
					user = districtKeyContact.getUserMaster();
					userKeyContact = districtKeyContact;
				}
			}
			
			if(user==null)
			{
				aData[1] = "1";
				return aData; //You are not authorized
			}
			
			List<DistrictApprovalGroups> districtApprovalGroupList = districtApprovalGroupsDAO.findDistrictApprovalGroupsByJobCategory(districtMaster,jobCategoryMaster);
			for(DistrictApprovalGroups approvalGroups : districtApprovalGroupList)
			{
				if(approvalGroups.getGroupMembers().contains("#"+userKeyContact.getKeyContactId()+"#"))
				{
					userApprovalGroup = approvalGroups;
					break;
				}
			}
			
			if(userApprovalGroup!=null)
			{
				aData[1] = "4";//Approve job
				return aData;
			}
		}
		/********************************************************** For Adams**********************************************************/		
		else if(approvalBeforeGoLive!=1 && districtMaster!=null && districtMaster.getSkipApprovalProcess()!=null && districtMaster.getSkipApprovalProcess())
		{
			List<JobWiseApprovalProcess>	jobWiseApprovalProcessList=	jobWiseApprovalProcessDAO.findByJobId(jobOrder);
			if(jobWiseApprovalProcessList!=null && jobWiseApprovalProcessList.size()>0){
					JobWiseApprovalProcess jobWiseApprovalProcess=jobWiseApprovalProcessList.get(0);
					JobApprovalProcess jobApprovalProcess=jobWiseApprovalProcess.getJobApprovalProcessId();
					if(jobApprovalProcess!=null && jobApprovalProcess.getStatus().equals(0)){
						aData[1] = "6";
						return aData;
					}
					String groups=jobApprovalProcess.getJobApprovalGroups();
					String groupIds[]=null;
					groupIds=groups.split("\\|");
					List<Integer> groupApprovalIds=new ArrayList<Integer>();
					if(groupIds!=null)
					{
						for(String goupid:groupIds){
							if(!goupid.equals("")){
								try{
									Integer grpid = Integer.parseInt(goupid);
									groupApprovalIds.add(grpid);
								}catch(Exception e){e.printStackTrace();}
							}
						}
					}
					List<DistrictKeyContact> userKeyContactList=districtKeyContactDAO.findByContactType(userMaster, "Job Approval");
					if(userKeyContactList!=null && userKeyContactList.size()>0){
						DistrictKeyContact userKeyContact=userKeyContactList.get(0);
						if(userKeyContact!=null){
							List<DistrictApprovalGroups> districtApprovalGroupsList=	districtApprovalGroupsDAO.findGroupsByDistrictAndDistrictApprovalGroupsId(jobOrder.getDistrictMaster(),groupApprovalIds);
							if(districtApprovalGroupsList!=null && districtApprovalGroupsList.size()>0){
								for(DistrictApprovalGroups dsgpl:districtApprovalGroupsList){
									if(dsgpl!=null)
									if(dsgpl.getGroupMembers().contains("#"+userKeyContact.getKeyContactId()+"#")){
										aData[1] = "4";
										return aData; //You are authorized authorized
									}
								}
									
							} else{
								aData[1] = "3";
								return aData; // no need to approve this job
							}
							
						}
						else{
							aData[1] = "1";
							return aData; //You are not authorized
						}
						
					}
					else{
						aData[1] = "1";
						return aData; //You are not authorized
					}
					
				
			} else{
				aData[1] = "3";// no need to approve this job
			return aData;
			}
		
		}			
		else	if(approvalBeforeGoLive!=1)
		{
			if(districtMaster.getApprovalBeforeGoLive())
			{
				List<DistrictKeyContact> districtKeyContactList = new ArrayList<DistrictKeyContact>();
				districtKeyContactList = districtKeyContactDAO.findByContactType(districtMaster,"Job Approval");
				UserMaster user = null;
				System.out.println("userId::: "+userId);
				//jobapprovalhistory
				for (DistrictKeyContact districtKeyContact : districtKeyContactList) {
					//System.out.println("districtKeyContact.getUserMaster().getUserId():: "+districtKeyContact.getUserMaster().getUserId());
					if(districtKeyContact.getUserMaster().getUserId().equals(userId))
					{
						user = districtKeyContact.getUserMaster();
					}
				}
				System.out.println("User: "+user);
				if(user==null)
				{
					aData[1] = "1";
					return aData; //You are not authorized
				}
				List<JobApprovalHistory> jobApprovalHistories = jobApprovalHistoryDAO.findByJobId(jobOrder);
				System.out.println(noOfApprovalNeeded+" jobApprovalHistories.size():: "+jobApprovalHistories.size());
				if(jobApprovalHistories.size()>0)
				{
					if(noOfApprovalNeeded==jobApprovalHistories.size())
					{
						//jobOrder.setApprovalBeforeGoLive(1);
						//jobOrderDAO.updatePersistent(jobOrder);
						aData[1] = "2";
						return aData;// already approved
					}
					boolean isApproved = false;
					for (JobApprovalHistory jobApprovalHistory : jobApprovalHistories) {
						if(jobApprovalHistory.getUserMaster().getUserId().equals(userId))
						{
							isApproved = true;
							break;
						}
					}
					if(!isApproved)
					{
						
						aData[1] = "4";//Approve job
						return aData;
					}
					if(isApproved)
					{
						aData[1] = "5";
						return aData;//Already
					}
				}else
				{
					aData[1] = "4";//Approve job
					return aData;
				}
				
				//jobOrderDAO.updatePersistent(jobOrder);
				try{
					String logType="Job Approved";
					//userLoginHistoryDAO.insertUserLoginHistory(userMaster,session.getId(),IPAddressUtility.getIpAddress(request),new Date(),logType);
				}catch(Exception e){
					e.printStackTrace();
				}
			}else
			{
					aData[1] = "3";// no need to approve this job
					return aData;
			}
		}
		else
		{
			aData[1] = "3";// no need to approve this job
			return aData;
		}
	}catch (Exception e) 
	{
		e.printStackTrace();
		aData[1] = "0";
		return aData;
	}
	aData[1] = "0";
	return aData;
}

@Transactional(readOnly=false)
public String approveJobOk(int entityType,Integer jobId,Integer userId)
{
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	UserMaster userMaster=null;
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	}else{
		userMaster=(UserMaster)session.getAttribute("userMaster");
	}
	if(userMaster!=null)
	if(userMaster.getDistrictId()!=null&& userMaster.getDistrictId().getSkipApprovalProcess()!=null && userMaster.getDistrictId().getSkipApprovalProcess()){
		return approveJobAjax.approveJobOkForAdams(entityType, jobId, userId);
	}
	else{
		return approveJobAjax.approveJobOk(entityType, jobId, userId);
	}
	return "0";
}


public String displayRecordsByEntityTypeNew(boolean resultFlag,int JobOrderType,int districtOrSchoolId,int 
		schoolId,/*String subjectId,String certifications,*/String jobOrderIds,String status,String noOfRow,
		String pageNo,String sortOrder,String sortOrderType,String  disJobReqNo,String geoZonId,String jobCategoryIds,String jobApplicationStatus,String jobSubCategoryIds)        //Adding jobApplicationStatus parameter by Deepak
{
	/* ========  For Session time Out Error =========*/
	System.out.println( resultFlag+"==="+ JobOrderType+"==="+ districtOrSchoolId+"==="+ schoolId+"==="+ jobOrderIds+"==="+ status+"==="+ noOfRow+"==="+ pageNo+"==="+ sortOrder+"==="+ sortOrderType+"==="+  disJobReqNo+"==="+ geoZonId+"==="+ jobCategoryIds+"==="+ jobApplicationStatus+"==="+ jobSubCategoryIds);
	System.out.println("------------------------ jobApplicationStatus::::::uuuuuuu::::::"+ jobApplicationStatus);
	int jobApplicationStatusId=0;
	if(jobApplicationStatus!=null && jobApplicationStatus!="" &&!jobApplicationStatus.equals(""))
		jobApplicationStatusId =  Integer.parseInt(jobApplicationStatus);
	
	int	geoZoneId=0;
	if(geoZonId!=null && geoZonId!="" &&!geoZonId.equals(""))
	  geoZoneId =  Integer.parseInt(geoZonId);
	
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	}
	StringBuffer tmRecords =	new StringBuffer();
	try{
		
		String locale = Utility.getValueOfPropByKey("locale");
		String lblEdit  = Utility.getLocaleValuePropByKey("lblEdit", locale);
		String lblDelete   = Utility.getLocaleValuePropByKey("lblDelete", locale);
		String lblActions = Utility.getLocaleValuePropByKey("lblActions", locale);
		String lblStatus    = Utility.getLocaleValuePropByKey("lblStatus", locale);
		String lblTitle  = Utility.getLocaleValuePropByKey("lblTitle", locale);
		String lblDeactivate = Utility.getLocaleValuePropByKey("lblDeactivate", locale);
		String lblActivate = Utility.getLocaleValuePropByKey("lblActivate", locale);
		String lblJobId  = Utility.getLocaleValuePropByKey("lblJobId", locale);
		String lblZone  = Utility.getLocaleValuePropByKey("lblZone", locale);
		String lblSubject  = Utility.getLocaleValuePropByKey("lblSubject", locale);
		String lblPostedUntil = Utility.getLocaleValuePropByKey("lblPostedUntil", locale);
		String lblApplicants = Utility.getLocaleValuePropByKey("lblApplicants", locale);
		String lblHires = Utility.getLocaleValuePropByKey("lblHires", locale);
		String lblCandidateGrid = Utility.getLocaleValuePropByKey("lblCandidateGrid", locale);
		String lblDistrictAttachment = Utility.getLocaleValuePropByKey("lblDistrictAttachment", locale);
		String lblUntilFilled = Utility.getLocaleValuePropByKey("lblUntilFilled", locale);
		String lblClone = Utility.getLocaleValuePropByKey("lblClone", locale);
		String lblApprovalRequest = Utility.getLocaleValuePropByKey("lblApprovalRequest", locale);
		String lblNoRecord = Utility.getLocaleValuePropByKey("lblNoRecord", locale);
		String lblActive    = Utility.getLocaleValuePropByKey("lblActive", locale);
        String lblInactive    = Utility.getLocaleValuePropByKey("lblInactive", locale);
        String lblView = Utility.getLocaleValuePropByKey("lblView", locale);
		
		
		UserMaster userMaster = null;
		Integer entityID=null;
		DistrictMaster districtMaster=null;
		SchoolMaster schoolMaster=null;
		SubjectMaster subjectMaster	=	null;
		int roleId=0;
		if (session == null || session.getAttribute("userMaster") == null) {
			return "false";
		}else{
			userMaster=(UserMaster)session.getAttribute("userMaster");

			if(userMaster.getSchoolId()!=null)
				schoolMaster =userMaster.getSchoolId();
			

			if(userMaster.getDistrictId()!=null){
				districtMaster=userMaster.getDistrictId();
			}

			entityID=userMaster.getEntityType();
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
		}
		String roleAccess=null;
		try{
			if(JobOrderType==3){
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,25,"schooljoborders.do",0);
			}else{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,24,"managejoborders.do",0);
			}
		}catch(Exception e){
			e.printStackTrace();
		}

		//-- get no of record in grid,
		//-- set start and end position

		int noOfRowInPage = Integer.parseInt(noOfRow);
		int pgNo = Integer.parseInt(pageNo);
		int start =((pgNo-1)*noOfRowInPage);
		int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
		int totalRecord = 0;
		//------------------------------------

		/** set default sorting fieldName **/
		String sortOrderFieldName="jobId";
		String sortOrderNoField="jobId";

		boolean deafultFlag=false;
		/**Start set dynamic sorting fieldName **/
		Order  sortOrderStrVal=null;
		if(sortOrder!=null){
			if(!sortOrder.equals("") && !sortOrder.equals(null) && !sortOrder.equals("ofApplicant") && !sortOrder.equals("ofHire")){
				sortOrderFieldName=sortOrder;
				sortOrderNoField=sortOrder;
				deafultFlag=false;
			}else{
				deafultFlag=true;
			}
			if(sortOrder.equals("ofApplicant")){
				sortOrderNoField="ofApplicant";
			}
			if(sortOrder.equals("ofHire")){
				sortOrderNoField="ofHire";
			}
		}
		String sortOrderTypeVal="0";
		sortOrderStrVal=Order.asc(sortOrderFieldName);
		if(sortOrderType!=null){
			if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
				if(sortOrderType.equals("1")){
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}
		}
		if(sortOrderType.equals("")){
			sortOrderStrVal=Order.desc(sortOrderFieldName);
		}

		/**End ------------------------------------**/
		List<JobOrder> lstJobOrder	  =	 new ArrayList<JobOrder>();
		boolean jobOrderIdFlag=false;
		boolean statusFlag=false;

		Criterion criterionStatus=null;
		Criterion criterionJobFilter=null;
		if(!(jobOrderIds.equals("0") || jobOrderIds.equalsIgnoreCase("0"))){
			String [] jobsArr =  jobOrderIds.split(",");
			List<Integer> jobs = new ArrayList<Integer>();
			for(int i=0;i<jobsArr.length;i++)
			{	
				String jobId = jobsArr[i].trim();
				try{if(!jobId.equals(""))
					jobs.add(Integer.parseInt(jobId));
				}catch(Exception e){}
			}
			if(jobs.size()>0)
			{
				jobOrderIdFlag=true;
				criterionJobFilter	= Restrictions.in("jobId",jobs);
			}
		}
		/*if(jobOrderId!=0){
			jobOrderIdFlag=true;
			criterionJobFilter	= Restrictions.eq("jobId",jobOrderId);
		}*/
		if(status!=null){
			if(!status.equalsIgnoreCase("") && !status.equalsIgnoreCase(null)){
				statusFlag=true;
				criterionStatus=Restrictions.eq("status",status);
			}
		}else{
			statusFlag=false;
		}

		/* ============== Gagan : For Adding Subject Filter ===============*/

		/*Criterion criterionSubject=null;
		if(!subjectId.equalsIgnoreCase(""))
		{
			subjectMaster=	subjectMasterDAO.findById(Integer.parseInt(subjectId), false, false);
			criterionSubject=Restrictions.eq("subjectMaster",subjectMaster);
		}*/
		//String windowFunc="if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
		int noOfRecordCheck = 0;
		int totalRecords=0;
		/*************Start *************/
		List<JobOrder> filterJobOrderList = new ArrayList<JobOrder>();

		boolean jobFlag=false;
		
		/*List<JobOrder> certJobOrderList	  =	 new ArrayList<JobOrder>();
		boolean certTypeFlag=false;
		if(certifications!=null && !certifications.equals("") && !certifications.equals("0")){
			certTypeFlag=true;
			certJobOrderList=jobCertificationDAO.findCertificationByJob(certifications);
		}
		if(certTypeFlag && certJobOrderList.size()>0){
			if(jobFlag){
				filterJobOrderList.retainAll(certJobOrderList);
			}else{
				filterJobOrderList.addAll(certJobOrderList);
			}
		}
		if(certTypeFlag){
			jobFlag=true;
		}*/
		
		/*List<JobOrder> subjectJList	  =	 new ArrayList<JobOrder>();
		boolean subjectFlag=false;
		if(subjectId!=null && !subjectId.equals("") && !subjectId.equals("0")){
			SubjectMaster sMaster=null;
			if(subjectId!=null){
				sMaster=subjectMasterDAO.findById(Integer.parseInt(subjectId),false,false);
			}
			subjectFlag=true;
			subjectJList=jobOrderDAO.findJobBySubject(sMaster);
		}
		if(subjectFlag && subjectJList.size()>0){
			if(jobFlag){
				filterJobOrderList.retainAll(subjectJList);
			}else{
				filterJobOrderList.addAll(subjectJList);
			}
		}
		if(subjectFlag){
			jobFlag=true;
		}*/

		DistrictMaster  distMaster=null;
		List<JobOrder> districtJList	  =	 new ArrayList<JobOrder>();
		boolean districtFlag=false;
		if(districtOrSchoolId!=0){
			if(districtOrSchoolId!=0){
				distMaster=districtMasterDAO.findById(districtOrSchoolId,false,false);
			}
			districtFlag=true;
			districtJList=jobOrderDAO.findJobByDistrict(distMaster);
		}
		if(districtFlag && districtJList.size()>0){
			if(jobFlag){
				filterJobOrderList.retainAll(districtJList);
			}else{
				filterJobOrderList.addAll(districtJList);
			}
		}
		if(districtFlag){
			jobFlag=true;
		}

		System.out.println(" >>>>>>>>>>>>>>>>>>>>>>>> jobCategoryIds :::::::: "+jobCategoryIds);
		
		/*// Job category
		List<JobOrder> listjJobOrders = new ArrayList<JobOrder>();
		boolean jobCateFlag=false;
		if(jobCategoryIds!=null && !jobCategoryIds.equals("") && !jobCategoryIds.equals("0"))
		{
			List<Integer> jobcatIds = new ArrayList<Integer>();
			
			String jobCategoryIdStr[] =jobCategoryIds.split(",");
		  if(!ArrayUtils.contains(jobCategoryIdStr, "0"))
		  {		  
			for(String str : jobCategoryIdStr){
			 if(!str.equals(""))
				 jobcatIds.add(Integer.parseInt(str));
			 } 
			 if(jobcatIds.size()>0){
				 listjJobOrders = jobOrderDAO.findByJobCategery(jobcatIds,districtOrSchoolId);
				 jobCateFlag=true;
			 }
			 if(listjJobOrders!=null && listjJobOrders.size()>0){
					if(jobCateFlag){
						filterJobOrderList.retainAll(listjJobOrders);
					}else{
						filterJobOrderList.addAll(listjJobOrders);
					}
				 }
			 }
		  }

		if(jobCateFlag){
			jobFlag=true;
		}*/
		
		
		Criterion criterionJobOrderType = Restrictions.eq("createdForEntity",JobOrderType);
		
		List<JobOrder> schoolJList	  =	 new ArrayList<JobOrder>();
		boolean schoolFlag=false;
		int searchschoolflag=2;
		if(schoolId!=0){
			SchoolMaster  sclMaster=null;
			if(districtOrSchoolId!=0){
				sclMaster=schoolMasterDAO.findById(Long.parseLong(schoolId+""),false,false);
			}
			schoolFlag=true;
			searchschoolflag=1;
			if(distMaster!=null)
				schoolJList=schoolInJobOrderDAO.findJobListBySchool(sclMaster,distMaster,JobOrderType,entityID,searchschoolflag,userMaster);
		}
		else
		{// Only for SA for Nobletype district
			if(userMaster.getEntityType()==3 && JobOrderType==2 && userMaster.getDistrictId()!=null && distMaster.getWritePrivilegeToSchool().equals(true))
			{
				searchschoolflag=0; // below [ schoolMaster ] is sending from school id by getting  session 
				schoolJList=schoolInJobOrderDAO.findJobListBySchool(schoolMaster,distMaster,JobOrderType,entityID,searchschoolflag,userMaster);
				schoolFlag=true;
			}
		}
		if(schoolFlag && schoolJList.size()>0){
			if(jobFlag){
				filterJobOrderList.retainAll(schoolJList);
			}else{
				filterJobOrderList.addAll(schoolJList);
			}
		}else if(schoolFlag && schoolJList.size()==0){
			filterJobOrderList = new ArrayList<JobOrder>();
		}

		if(schoolFlag){
			jobFlag=true;
		}
//		if(userMaster.getDistrictId()!=null && userMaster.getDistrictId().getDistrictId().equals(806900))
//			if(userMaster.getEntityType()==3){
//				List<JobOrder> approvaljobid=findJobOrderSaAsApprovalProcess(userMaster);
//				if(approvaljobid!=null &&approvaljobid.size()>0)
//					filterJobOrderList.addAll(approvaljobid);
//			}
		/**********rajendra: search by jobrequisition no**********************/
		boolean jobReqnoFlag=false;
		List<JobOrder> jobOrderOfReqNoList	  =	 new ArrayList<JobOrder>();
		if(disJobReqNo!=null && !disJobReqNo.equals("") && !disJobReqNo.equals("0")){
			jobReqnoFlag=true;
			List<DistrictRequisitionNumbers> districtRequisitionNumbers=null;
			Criterion crDrq=Restrictions.eq("requisitionNumber", disJobReqNo);
			Criterion crDSTrq=null;
			if(districtOrSchoolId!=0){
				crDSTrq=Restrictions.eq("districtMaster", distMaster);
				districtRequisitionNumbers=districtRequisitionNumbersDAO.findByCriteria(crDSTrq,crDrq);
			}else{
				districtRequisitionNumbers=districtRequisitionNumbersDAO.findByCriteria(crDrq);
			}
			if(districtRequisitionNumbers!=null && districtRequisitionNumbers.size()>0){
				
				Criterion crJrq=Restrictions.in("districtRequisitionNumbers", districtRequisitionNumbers);
				List<JobRequisitionNumbers> jobRequisitionNumbers=jobRequisitionNumbersDAO.findByCriteria(crJrq);
				for(JobRequisitionNumbers jqr:jobRequisitionNumbers){
					jobOrderOfReqNoList.add(jqr.getJobOrder());
				}
			}
		}
		
		if(jobReqnoFlag && jobOrderOfReqNoList.size()>0){
			if(jobFlag){
				filterJobOrderList.retainAll(jobOrderOfReqNoList);
			}else{
				filterJobOrderList.addAll(jobOrderOfReqNoList);
			}
		}else if(jobReqnoFlag && jobOrderOfReqNoList.size()==0){
			
			filterJobOrderList = new ArrayList<JobOrder>();
		}
		
		if(jobReqnoFlag){
			jobFlag=true;
		}
		
		
		/**********rajendra: search by jobrequisition no**********************/
		// =============  Deepak filtering basis on job Status  added by 27-05-2015
		if(jobApplicationStatusId>0 && distMaster!=null && distMaster.getDistrictId().toString().equalsIgnoreCase("804800"))
		{
			boolean jobApplicationStatusFlag = false;
				List<JobOrder> jobApplicationStatusList = new ArrayList<JobOrder>();
					if (disJobReqNo != null && jobApplicationStatusId > 0) {
						jobApplicationStatusFlag = true;
						jobApplicationStatusList = jobOrderDAO.getJobApplicationStatusRole(jobApplicationStatusId);
						System.out.println("jobApplicationStatusList========"+ jobApplicationStatusList.size());
	
					}
					if (jobApplicationStatusFlag
							&& jobApplicationStatusList.size() > 0) {
						if (jobFlag == true) {
							filterJobOrderList.retainAll(jobApplicationStatusList);
						} else {
							filterJobOrderList.addAll(jobApplicationStatusList);
						}
					} else {
						filterJobOrderList = new ArrayList<JobOrder>();
					}
			if (jobApplicationStatusFlag) {
				jobFlag = true;
			}
		}
	
		
		 //     Mukesh filtering on the basis of zone
			List<JobOrder> listjobOrdersgeoZone =new ArrayList<JobOrder>();
		
		  if(geoZoneId>0)
			{
				GeoZoneMaster geoZoneMaster=geoZoneMasterDAO.findById(geoZoneId, false, false);
				if(userMaster.getRoleId().getRoleId().equals(3))
					listjobOrdersgeoZone = jobOrderDAO.getJobOrderBygeoZoneRole(geoZoneMaster,userMaster);
				else
					listjobOrdersgeoZone = jobOrderDAO.getJobOrderBygeoZone(geoZoneMaster);
			}
		 
		  if(listjobOrdersgeoZone!=null && listjobOrdersgeoZone.size()>0)
		  {
			  if(jobFlag==true){
				  filterJobOrderList.retainAll(listjobOrdersgeoZone);
				}else{
					filterJobOrderList.addAll(listjobOrdersgeoZone);
				}
		  }	
		  if(listjobOrdersgeoZone.size()==0 && geoZoneId==-1 ||geoZoneId==0)
		  {
			  filterJobOrderList.addAll(listjobOrdersgeoZone);
		  
		  }
		  else if(listjobOrdersgeoZone.size()==0 &&geoZoneId!=-1)
		  {
			  filterJobOrderList.retainAll(listjobOrdersgeoZone);
		  }
		  
		  
		  if(jobSubCategoryIds!=null && !jobSubCategoryIds.equals("") && !jobSubCategoryIds.equals("0"))
		  {
			//Job Sub Category
				List<JobOrder> listJobOrdersBySubCatgry = new ArrayList<JobOrder>();
				boolean jobSubCateFlag=false;
				
				if(jobSubCategoryIds!=null && !jobSubCategoryIds.equals("") && !jobSubCategoryIds.equals("0"))
				{
					List<Integer> jobSubcatIds = new ArrayList<Integer>();
					
					String jobSubCategoryIdStr[] = jobSubCategoryIds.split(",");
				  if(!ArrayUtils.contains(jobSubCategoryIdStr, "0"))
				  {		  
					for(String str : jobSubCategoryIdStr){
					 if(!str.equals(""))
						 jobSubcatIds.add(Integer.parseInt(str));
					 } 
					 if(jobSubcatIds.size()>0){
						 listJobOrdersBySubCatgry = jobOrderDAO.findByJobCategeryWithSubCate(jobSubcatIds,districtOrSchoolId);
						 System.out.println("  job list by jobcategory :: "+listJobOrdersBySubCatgry.size());
						 jobSubCateFlag=true;
					 }
					 if(jobSubCateFlag){
						filterJobOrderList.retainAll(listJobOrdersBySubCatgry);
					 }else{
						filterJobOrderList.addAll(listJobOrdersBySubCatgry);
					 }
					}
				  }

				if(jobSubCateFlag){
					jobFlag=true;
				}
		  }
		  else
		  {
			// Job category
				List<JobOrder> listjJobOrders = new ArrayList<JobOrder>();
				boolean jobCateFlag=false;
				if(jobCategoryIds!=null && !jobCategoryIds.equals("") && !jobCategoryIds.equals("0"))
				{
					List<Integer> jobcatIds = new ArrayList<Integer>();
					
					String jobCategoryIdStr[] =jobCategoryIds.split(",");
				  if(!ArrayUtils.contains(jobCategoryIdStr, "0"))
				  {		  
					for(String str : jobCategoryIdStr){
					 if(!str.equals(""))
						 jobcatIds.add(Integer.parseInt(str));
					 } 
					 if(jobcatIds.size()>0){
						 listjJobOrders = jobOrderDAO.findByJobCategeryWithSubCate(jobcatIds,districtOrSchoolId);
						 System.out.println("  job list by jobcategory :: "+listjJobOrders.size());
						 jobCateFlag=true;
					 }
					 if(jobCateFlag){
						filterJobOrderList.retainAll(listjJobOrders);
					 }else{
						filterJobOrderList.addAll(listjJobOrders);
					 }
					}
				  }

				if(jobCateFlag){
					jobFlag=true;
				}
			  
		  }
		  
		
		  if(jobFlag){
			if(filterJobOrderList.size()>0){
				List jobIds = new ArrayList();
				for(int e=0; e<filterJobOrderList.size(); e++) {
					jobIds.add(filterJobOrderList.get(e).getJobId());
				}
				Criterion criterionJobList = Restrictions.in("jobId",jobIds);

				Criterion criterionMix=criterionJobOrderType;
				if(statusFlag && jobOrderIdFlag){
					criterionMix=Restrictions.and(criterionStatus,criterionJobFilter);
				}else if(statusFlag){
					criterionMix=criterionStatus;
				}else if(jobOrderIdFlag){
					criterionMix=criterionJobFilter;
				}
				
				Criterion crt[]={criterionJobList,criterionJobOrderType,criterionMix};
				Order poolord=Order.desc("isPoolJob");
				Order orders[]={poolord,sortOrderStrVal};
				
				if(userMaster.getEntityType()!=1 && userMaster.getDistrictId().getDistrictId().equals(1200390)){
					lstJobOrder=jobOrderDAO.getJobOrderByPool(crt, orders, start,noOfRowInPage);
				}else{
					lstJobOrder=jobOrderDAO.findWithLimit(sortOrderStrVal,start,noOfRowInPage,criterionJobList,criterionJobOrderType,criterionMix);
				}
				totalRecords=jobOrderDAO.getRowCountWithSort(sortOrderStrVal,criterionJobList,criterionJobOrderType,criterionMix);
				
			}
		}else{
			Criterion criterionMix=criterionJobOrderType;
			if(statusFlag && jobOrderIdFlag){
				criterionMix=Restrictions.and(criterionStatus,criterionJobFilter);
			}else if(statusFlag){
				criterionMix=criterionStatus;
			}else if(jobOrderIdFlag){
				criterionMix=criterionJobFilter;
			}
			
			Criterion crt[]={criterionJobOrderType,criterionMix};
			Order poolord=Order.desc("isPoolJob");
			Order orders[]={poolord,sortOrderStrVal};
			
			if(userMaster.getEntityType()!=1 && userMaster.getDistrictId()!=null && userMaster.getDistrictId().getDistrictId()==1200390){
				lstJobOrder=jobOrderDAO.getJobOrderByPool(crt, orders, start,noOfRowInPage);
			}else{
				lstJobOrder=jobOrderDAO.findWithLimit(sortOrderStrVal,start,noOfRowInPage,criterionJobOrderType,criterionMix);
			}
			totalRecords=jobOrderDAO.getRowCountWithSort(sortOrderStrVal,criterionJobOrderType,criterionMix);
		}
	 
		/***************************** End Code **************************************/
	      Map<Integer,String> map = new HashMap<Integer, String>();
			if(lstJobOrder.size()>0)
			 map = jobForTeacherDAO.countApplicantsByJobOrdersAllll(lstJobOrder);
									
		/*	Map<Integer,JobForTeacher> jobForTeacherList = new HashMap<Integer, JobForTeacher>();
		//Map<Integer,JobCertification> jobCertificationList = new HashMap<Integer, JobCertification>();
		//Map<Integer,JobOrder> jobOrderList =new HashMap<Integer, JobOrder>();
		List<TeacherDetail> teacherDetails = new ArrayList<TeacherDetail>();
		if(lstJobOrder.size()>0){
			jobForTeacherList = jobForTeacherDAO.findJFTApplicantbyJobOrders(lstJobOrder);
			List<Integer> joborderIds=new ArrayList<Integer>();
			for(JobOrder job: lstJobOrder){
				joborderIds.add(job.getJobId());
			}
			//jobOrderList = jobOrderDAO.findByAllJobOrders(joborderIds); 

			try{
				for(int t=0;t<jobForTeacherList.size();t++){
					teacherDetails.add(jobForTeacherList.get(t).getTeacherId());
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}

		//List<JobCategoryMaster> jobCategoryMasterList= jobCategoryMasterDAO.findAllJobCategoryNameByOrder();
		//List<TeacherAssessmentStatus> teacherAssessmentStatusList=teacherAssessmentStatusDAO.findByAllTeacherAssessStatusByTeacher(teacherDetails);
		List <StatusMaster> statusMasterList=WorkThreadServlet.statusMasters;
		StatusMaster statusMaster =findStatusByShortName(statusMasterList,"hide");
		StatusMaster statusMasterHird = findStatusByShortName(statusMasterList,"hird");

		List<TeacherStatusHistoryForJob> teacherSHFJobList=teacherStatusHistoryForJobDAO.findStatusAndSecStatusByTeacherList(teacherDetails);
		Map<String,TeacherStatusHistoryForJob> mapTSHFJob = new HashMap<String, TeacherStatusHistoryForJob>();
		for(TeacherStatusHistoryForJob forJob:teacherSHFJobList){
			if(forJob.getStatusMaster()!=null){
				mapTSHFJob.put(forJob.getJobOrder().getJobId()+"##"+forJob.getTeacherDetail().getTeacherId()+"##"+forJob.getStatusMaster().getStatusId()+"##0",forJob);
			}else{
				if(forJob.getSecondaryStatus()!=null){
					if(forJob.getSecondaryStatus().getSecondaryStatus_copy()!=null){
						mapTSHFJob.put(forJob.getJobOrder().getJobId()+"##"+forJob.getTeacherDetail().getTeacherId()+"##"+forJob.getSecondaryStatus().getSecondaryStatus_copy().getSecondaryStatusId()+"##1",forJob);
					}else{
						mapTSHFJob.put(forJob.getJobOrder().getJobId()+"##"+forJob.getTeacherDetail().getTeacherId()+"##"+forJob.getSecondaryStatus().getSecondaryStatusId()+"##1",forJob);
					}
				}
			}
		}*/
		totalRecord =totalRecords;

		if(totalRecord<end)
			end=totalRecord;

		tmRecords.append("<table  id='tblGrid' width='100%' border='0'>");
		tmRecords.append("<thead class='bg'>");
		tmRecords.append("<tr>");
		String responseText="";
		if(deafultFlag){
			sortOrderTypeVal="1";
		}
		responseText=PaginationAndSorting.responseSortingLink(lblJobId,sortOrderNoField,"jobId",sortOrderTypeVal,pgNo);
		tmRecords.append("<th valign='top'>"+responseText+"</th>");

		responseText=PaginationAndSorting.responseSortingLink(lblTitle,sortOrderFieldName,"jobTitle",sortOrderTypeVal,pgNo);
		tmRecords.append("<th  valign='top'>"+responseText+"</th>");
		// zone added by mukesh
		responseText=PaginationAndSorting.responseSortingLink(lblZone,sortOrderFieldName,"geoZoneMaster",sortOrderTypeVal,pgNo);
		tmRecords.append("<th  valign='top'>"+responseText+"</th>");

		responseText=PaginationAndSorting.responseSortingLink(lblSubject,sortOrderFieldName,"subjectMaster",sortOrderTypeVal,pgNo);
		tmRecords.append("<th  valign='top'>"+responseText+"</th>");

		responseText=PaginationAndSorting.responseSortingLink(lblStatus,sortOrderFieldName,"status",sortOrderTypeVal,pgNo);
		tmRecords.append("<th valign='top'>"+responseText+"</th>");

		//responseText=PaginationAndSorting.responseSortingLink("Activation Date",sortOrderFieldName,"jobStartDate",sortOrderTypeVal,pgNo);
		//tmRecords.append("<th width='14%' valign='top'>"+responseText+"</th>");

		responseText=PaginationAndSorting.responseSortingLink(lblPostedUntil,sortOrderFieldName,"jobEndDate",sortOrderTypeVal,pgNo);
		tmRecords.append("<th  valign='top'>"+responseText+"</th>");


		tmRecords.append("<th  valign='top'>"+lblApplicants+" <a href='#' id='iconpophover1' class='net-header-text ' rel='tooltip' data-original-title='"+Utility.getLocaleValuePropByKey("msgTotalnumberofapplicantsappliedforthisjob2", locale)+"'><span class='icon-question-sign'></span></a></th>");

		tmRecords.append("<th  valign='top'>"+lblHires+"</th>");

		tmRecords.append("<th  valign='top'>"+lblCandidateGrid+"</th>");
		
		tmRecords.append("<th  valign='top'>"+lblDistrictAttachment+"</th>");

		tmRecords.append(" <th   valign='top'>"+lblActions+"</th>");

		tmRecords.append("</tr>");
		tmRecords.append("</thead>");

		if(resultFlag==false){
			totalRecord=0;
		}
		
		/*================= Checking If Record Not Found ======================*/
    
		/********** Start :: For get Number of awailable candidates ****** */
		
	/*	List <StatusMaster> statusMasterLst=WorkThreadServlet.statusMasters;
		Map<String, StatusMaster> mapStatus = new HashMap<String, StatusMaster>();
		for (StatusMaster sMaster : statusMasterLst) {
			mapStatus.put(sMaster.getStatusShortName(),sMaster);
		}
		
		List<JobOrder> jobLst= new ArrayList<JobOrder>();
		List<TeacherDetail> teacherList = new ArrayList<TeacherDetail>();
		
		//Get List of teacher and joborder
		for(int i=0;i<jobForTeacherList.size();i++){
			jobLst.add(jobForTeacherList.get(i).getJobId());
			teacherList.add(jobForTeacherList.get(i).getTeacherId());
		}
		
		TeacherAssessmentStatus teacherAssessmentStatusJSI = null;
		Map<Integer, Integer> mapAssess = new HashMap<Integer, Integer>();
		Map<Integer, TeacherAssessmentStatus> mapJSI = new HashMap<Integer, TeacherAssessmentStatus>();
		List<AssessmentJobRelation> assessmentJobRelations1=assessmentJobRelationDAO.findRelationByJobOrder(jobLst);
		List<AssessmentDetail> adList=new ArrayList<AssessmentDetail>();
		
		Map<Integer,InternalTransferCandidates> mapLstITRA = new HashMap<Integer, InternalTransferCandidates>();
		Map<Integer,TeacherPortfolioStatus> maptPortfolioStatus = new HashMap<Integer, TeacherPortfolioStatus>();
		//Map<Integer,TeacherAssessmentStatus> maptBaseAssmentStatus = new HashMap<Integer, TeacherAssessmentStatus>();
		List<TeacherAssessmentStatus> lstJSI =new ArrayList<TeacherAssessmentStatus>(); 

		mapLstITRA 				= internalTransferCandidatesDAO.getDistrictForTeacherShowSatus(teacherList);
		maptPortfolioStatus 	= teacherPortfolioStatusDAO.findPortfolioStatusByTeacherlist(teacherList);
		//maptBaseAssmentStatus 	= teacherAssessmentStatusDAO.findAssessmentStatusByTeacherListForBase(teacherList);
		
		if(assessmentJobRelations1.size()>0){
			for(AssessmentJobRelation ajr: assessmentJobRelations1){
				mapAssess.put(ajr.getJobId().getJobId(),ajr.getAssessmentId().getAssessmentId());
				adList.add(ajr.getAssessmentId());
			}
			if(adList.size()>0)
				lstJSI = teacherAssessmentStatusDAO.findJSITakenByAssessMentListAndTeacherList(teacherList,adList);
		}

		for(TeacherAssessmentStatus ts : lstJSI)
		{
			mapJSI.put(ts.getAssessmentDetail().getAssessmentId(), ts);
		}
*/
		/********** End :: For get Number of awailable candidates ****** */
		//Approval authority   
		List<UserMaster> listUserMasters =new ArrayList<UserMaster>();
		List<JobApprovalHistory> listJobApprovalHistories =new ArrayList<JobApprovalHistory>();
		Map<Integer,List<UserMaster>> mapofApprovedUsers=new HashMap<Integer, List<UserMaster>>();
		List<UserMaster> tempUserList=null;
		UserMaster user = null;
		Integer userId = userMaster.getUserId();
		
		DistrictKeyContact userKeyContact=null;
		if(userMaster.getEntityType()!=1)
		{
			
			List<DistrictKeyContact> districtKeyContactList = new ArrayList<DistrictKeyContact>();
			districtKeyContactList = districtKeyContactDAO.findByContactType(districtMaster,"Job Approval");
			
			for (DistrictKeyContact districtKeyContact : districtKeyContactList) {
				listUserMasters.add(districtKeyContact.getUserMaster());
				//System.out.println("districtKeyContact.getUserMaster().getUserId():: "+districtKeyContact.getUserMaster().getUserId());
				if(districtKeyContact.getUserMaster().getUserId().equals(userId))
				{
					user = districtKeyContact.getUserMaster();
					userKeyContact = districtKeyContact;
				}
			}
		}
		if(user!=null)
		if(listUserMasters!=null && listUserMasters.size()>0){
			listJobApprovalHistories= jobApprovalHistoryDAO.findHistoryByUser(listUserMasters);
			if(listJobApprovalHistories!=null && listJobApprovalHistories.size()>0){
				for (JobApprovalHistory jobApprovalHistory : listJobApprovalHistories) {
					tempUserList=null;
					if(mapofApprovedUsers.get(jobApprovalHistory.getJobOrder().getJobId())!=null){
						tempUserList=mapofApprovedUsers.get(jobApprovalHistory.getJobOrder().getJobId());
						tempUserList.add(jobApprovalHistory.getUserMaster());
						//putting again in Map
						mapofApprovedUsers.put(jobApprovalHistory.getJobOrder().getJobId(), tempUserList);
						
					}else{
						tempUserList=new ArrayList<UserMaster>();
						tempUserList.add(jobApprovalHistory.getUserMaster());
						mapofApprovedUsers.put(jobApprovalHistory.getJobOrder().getJobId(), tempUserList);
					}
				}
			}
		}
		////////////////////////////////////
		boolean miamiShowFlag=true;
		try{
			if(userMaster.getDistrictId().getDistrictId()==1200390 && userMaster.getRoleId().getRoleId()==3 && userMaster.getEntityType()==3){
				miamiShowFlag=false;
			}
		}catch(Exception e){}	
		String windowFunc="if(this.href!='javascript:void(0);')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
		String fileName 	= 	"";
		String filePath		=	"";
		Integer counter = 1;
		int count=0;
		if(resultFlag){
			if(lstJobOrder.size()==0)
				tmRecords.append("<tr><td colspan='10' align='center'>"+lblNoRecord+"</td></tr>" );
			
			System.out.println(":::::::::::::::::::::::::::::::::::::::::::::::Job LevelText ::::::::::::::::::::::::::::::::::::::");
			Map<Integer, String> jobLabelText = new HashMap<Integer, String>();
			Map<Integer, String> jobUserLabelText = new HashMap<Integer, String>();
		//	jobLabelText = getJobLevelText(entityID, userKeyContact, lstJobOrder );
			if(userMaster.getDistrictId()!=null && userMaster.getDistrictId().getDistrictId()==806900)
			{
				jobLabelText=getUserJobLevelTextApprovalGroupForAdams(entityID, userMaster, lstJobOrder ,userKeyContact);
			}
			else
			{
				//jobUserLabelText=getUserJobLevelText(entityID, userMaster, lstJobOrder );
				jobLabelText = getJobLevelText(entityID, userKeyContact, lstJobOrder );
			}
			jobUserLabelText=getUserJobLevelText(entityID, userMaster, lstJobOrder );
			System.out.println(jobLabelText);
			System.out.println(jobUserLabelText);
			
			for (JobOrder jobOrderDetails : lstJobOrder) {
				count++;
				noOfRecordCheck++;   
				int jobForTeacherSize=0;
				int jftAvailableCandidates=0;
				int jobForHiresSize=0;
				
				//11111111111
			/*	int totalNoOfCGViewCandidate =	0;	
				totalNoOfCGViewCandidate =	getTotalCountOfCGView(mapTSHFJob,""+jobOrderDetails.getJobId(),jobCategoryMasterList,jobForTeacherList,statusMasterList,jobOrderList,teacherAssessmentStatusList,userMaster);
			*/	tmRecords.append("<tr>" );
				tmRecords.append("<td nowrap >"+jobOrderDetails.getJobId()+"</td>");
				if(jobOrderDetails.getIsPoolJob()==2)
				{
					tmRecords.append("<td><span class='requiredlarge'>*</span>"+jobOrderDetails.getJobTitle()+"</td>");
				}
				else
				{
					tmRecords.append("<td>"+jobOrderDetails.getJobTitle()+"</td>");
				}
				
				
				if(jobOrderDetails.getGeoZoneMaster()!=null){
					if(jobOrderDetails.getGeoZoneMaster().getDistrictMaster().getDistrictId()==4218990)
					{
						tmRecords.append("<td><a href='javascript:void(0);' onclick=\"showPhiladelphiaPdf();\" return false;>"+jobOrderDetails.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
					}
					else if(jobOrderDetails.getGeoZoneMaster().getDistrictMaster().getDistrictId()==1200390){
						tmRecords.append("<td><a href='http://jobs.dadeschools.net/teachers/pdf/Geo_Zones_With_Schools.pdf' target='_blank'>"+jobOrderDetails.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
					}else{
						tmRecords.append("<td><a href='javascript:void(0);' onclick=\"return showZoneSchoolList("+jobOrderDetails.getGeoZoneMaster().getGeoZoneId()+",'"+jobOrderDetails.getGeoZoneMaster().getGeoZoneName()+"',"+jobOrderDetails.getDistrictMaster().getDistrictId()+")\";>"+jobOrderDetails.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
					}
				} else {
					tmRecords.append("<td></td>");
				}
				
				if(jobOrderDetails.getSubjectMaster()!=null){
					tmRecords.append("<td>"+jobOrderDetails.getSubjectMaster().getSubjectName()+"</td>");
				}else{
					tmRecords.append("<td></td>");
				}
				tmRecords.append("<td>");
				if(jobOrderDetails.getStatus().equalsIgnoreCase("A"))
					tmRecords.append(lblActive);
				else
					tmRecords.append(lblInactive);
				tmRecords.append("</td>");

				if(Utility.convertDateAndTimeToUSformatOnlyDate(jobOrderDetails.getJobEndDate()).equals("Dec 25, 2099"))
					tmRecords.append("<td>"+lblUntilFilled+"</td>");
				else
					tmRecords.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(jobOrderDetails.getJobEndDate())+",11:59 PM CST</td>");

				/*try{
					jobForTeacherSize=findJFTApplicantbyORHideJobOrder(mapTSHFJob,jobForTeacherList,jobOrderDetails,statusMaster,1,userMaster);
					jftAvailableCandidates = findJFTApplicantAvailableCandidates(mapTSHFJob,jobForTeacherList,jobOrderDetails,statusMaster,1,userMaster,mapAssess,teacherAssessmentStatusJSI,mapJSI,mapStatus,maptPortfolioStatus,mapLstITRA);
					if(jobForTeacherSize!=0){
						tmRecords.append("<td>"+jobForTeacherSize+"/"+jftAvailableCandidates+"</td>");
					}else{
						tmRecords.append("<td>0/0</td>");
					}
				}catch(Exception e){
					e.printStackTrace();
					tmRecords.append("<td >0</td>");
				}
				try{
					jobForHiresSize=findJFTApplicantbyORHideJobOrder(mapTSHFJob,jobForTeacherList,jobOrderDetails,statusMasterHird,2,userMaster);
					if(jobForHiresSize!=0){
						tmRecords.append("<td>"+jobForHiresSize+"</td>");
					}else{
						tmRecords.append("<td>0</td>");
					}
				}catch(Exception e){
					e.printStackTrace();
					tmRecords.append("<td>0</td>");
				}*/
				//Total/available/"+Utility.getLocaleValuePropByKey("lblHired", locale)+"
				String appliedStr = null;
				appliedStr = map.get(jobOrderDetails.getJobId());
				 if(appliedStr!=null){
					 tmRecords.append("<td>"+appliedStr.split("##")[0]+"/"+appliedStr.split("##")[1]+"</td>");
					 tmRecords.append("<td >"+appliedStr.split("##")[2]+"</td>");
				 }else
				 {
					 tmRecords.append("<td>0/0</td>");
					 tmRecords.append("<td>0</td>");
				 }
				tmRecords.append("<td style='text-align:center'>&nbsp;<a data-original-title='CG View' rel='tooltip' id='cgn"+noOfRecordCheck+"' target=\"_blank\" href='candidategrid.do?jobId="+jobOrderDetails.getJobId()+"&JobOrderType="+JobOrderType+"'><span class='icon-table icon-large'></span></a></td>");
				jobOrderDetails.getDistrictMaster().getDistrictId();
				filePath	=	jobOrderDetails.getDistrictMaster().getDistrictId()+"/DistAttachmentFile";
				fileName 	=	jobOrderDetails.getDistrictAttachment() == null ? "" : jobOrderDetails.getDistrictAttachment();
				//filePath=Utility.getValueOfPropByKey("districtRootPath")+jobOrderDetails.getDistrictMaster().getDistrictId()+"/DistAttachmentFile";
				
				if(!fileName.equals("")){
					tmRecords.append("<td style='text-align:center'>");
					tmRecords.append("<a href='javascript:void(0);' rel='tooltip' data-original-title="+fileName+" class='attchToolTip"+noOfRecordCheck+"' id='refNotesfileopen"+counter+"' onclick=\"downloadAttachment('"+filePath+"','"+fileName+"');"+windowFunc+"\"><span class='icon-download-alt icon-large iconcolorBlue'></span></a>");
					tmRecords.append("</td>");
				} else {
					tmRecords.append("<td>&nbsp;</td>");
				}

				
				if(entityID==3 && JobOrderType==2){
			     	if(userMaster.getDistrictId().getsACreateDistJOb()!=null){
					if(userMaster.getUserId().equals(jobOrderDetails.getCreatedBy()) && userMaster.getDistrictId().getsACreateDistJOb()){
						tmRecords.append("<td nowrap><a title="+Utility.getLocaleValuePropByKey("msgEditicon", locale)+" href='editjoborder.do?JobOrderType="+JobOrderType+"&JobId="+jobOrderDetails.getJobId()+"&districtId="+jobOrderDetails.getDistrictMaster().getDistrictId().toString()+"'><i class='fa fa-pencil-square-o fa-lg' /></a>");						
						tmRecords.append("<a title="+Utility.getLocaleValuePropByKey("msgClone", locale)+" href='clonejoborder.do?JobOrderType="+JobOrderType+"&JobId="+jobOrderDetails.getJobId()+"&districtId="+jobOrderDetails.getDistrictMaster().getDistrictId().toString()+"'>  |<span class='icon-copy fa-lg iconcolorBlue'></span></a>");
						if(jobOrderDetails.getStatus().equalsIgnoreCase("A"))
							tmRecords.append("<a title="+Utility.getLocaleValuePropByKey("lblDeactivate", locale)+" href='javascript:void(0);' onclick=\"return activateDeactivateJob("+entityID+","+jobOrderDetails.getJobId()+",'I')\">| <i class='fa fa-times fa-lg' /></a>");
						else
							tmRecords.append("<a title="+Utility.getLocaleValuePropByKey("msgActivateicon", locale)+" href='javascript:void(0);' onclick=\"return activateDeactivateJob("+entityID+","+jobOrderDetails.getJobId()+",'A')\">| <i class='fa fa-check fa-lg' /></a>");
					}else{
						if(roleAccess.indexOf("|2|")!=-1 && miamiShowFlag){
							tmRecords.append("<td nowrap><a href='editjoborder.do?JobOrderType="+JobOrderType+"&JobId="+jobOrderDetails.getJobId()+"&districtId="+jobOrderDetails.getDistrictMaster().getDistrictId().toString()+"'>"+lblEdit+"</a>");						
						}else if(roleAccess.indexOf("|4|")!=-1 || miamiShowFlag==false){
							tmRecords.append("<td nowrap><a href='editjoborder.do?JobOrderType="+JobOrderType+"&JobId="+jobOrderDetails.getJobId()+"&districtId="+jobOrderDetails.getDistrictMaster().getDistrictId().toString()+"'>"+lblView+"</a>");
							if(userMaster.getDistrictId()!=null && userMaster.getDistrictId().getDistrictId().equals(806900) &&entityID==3){
								String text = jobLabelText.get(jobOrderDetails.getJobId());
								if(text!=null)
								tmRecords.append(text);
							}
						}
					}					
				}else {
					
					if(roleAccess.indexOf("|2|")!=-1 && miamiShowFlag){
						tmRecords.append("<td nowrap><a href='editjoborder.do?JobOrderType="+JobOrderType+"&JobId="+jobOrderDetails.getJobId()+"&districtId="+jobOrderDetails.getDistrictMaster().getDistrictId().toString()+"'>"+lblEdit+"</a>");						
					}else if(roleAccess.indexOf("|4|")!=-1 || miamiShowFlag==false){
						tmRecords.append("<td nowrap><a href='editjoborder.do?JobOrderType="+JobOrderType+"&JobId="+jobOrderDetails.getJobId()+"&districtId="+jobOrderDetails.getDistrictMaster().getDistrictId().toString()+"'>"+lblView+"</a>");
						if(userMaster.getDistrictId()!=null && userMaster.getDistrictId().getDistrictId().equals(806900) &&entityID==3){
							String text = jobLabelText.get(jobOrderDetails.getJobId());
							if(text!=null)
							tmRecords.append(text);
						}
					}
			      }	
			     	
			     	///start SAs Show Approval Link
			     	String text = jobUserLabelText.get(jobOrderDetails.getJobId());
					if(text!=null)
						tmRecords.append(text);
					if(jobOrderDetails.getIsInviteOnly() !=null && jobOrderDetails.getIsInviteOnly()){
						
						tmRecords.append("&nbsp;|<a  href='importcandidatedetails.do?jobId="+jobOrderDetails.getJobId()+"' rel='tooltip' id='exelexport"+count+"' data-original-title='Upload Candidates against this job'><i class='icon-upload' style='font-size: 1.5em;'></i></a>");
						tmRecords.append("<script>$('#exelexport"+count+"').tooltip();</script>");
					}
					tmRecords.append(" </td>");
					///SAs Show Approval Link end
				}else{
					boolean pipeFlag=false;
					if(roleAccess.indexOf("|2|")!=-1 && miamiShowFlag){
						tmRecords.append("<td nowrap><a title="+Utility.getLocaleValuePropByKey("msgEditicon", locale)+" href='editjoborder.do?JobOrderType="+JobOrderType+"&JobId="+jobOrderDetails.getJobId()+"&districtId="+jobOrderDetails.getDistrictMaster().getDistrictId().toString()+"'><i class='fa fa-pencil-square-o fa-lg' /></a>");						
						tmRecords.append("<a title="+Utility.getLocaleValuePropByKey("msgClone", locale)+" href='clonejoborder.do?JobOrderType="+JobOrderType+"&JobId="+jobOrderDetails.getJobId()+"&districtId="+jobOrderDetails.getDistrictMaster().getDistrictId().toString()+"'> | <span class='icon-copy fa-lg iconcolorBlue'></span></a>");
						pipeFlag=true;
					}else if(roleAccess.indexOf("|4|")!=-1 || miamiShowFlag==false){							
						tmRecords.append("<td nowrap><a href='editjoborder.do?JobOrderType="+JobOrderType+"&JobId="+jobOrderDetails.getJobId()+"&districtId="+jobOrderDetails.getDistrictMaster().getDistrictId().toString()+"'>"+lblEdit+"</a>");
						pipeFlag=true;
						
					}
					if(roleAccess.indexOf("|7|")!=-1 && miamiShowFlag){
						if(pipeFlag)tmRecords.append(" | ");
						if(jobOrderDetails.getStatus().equalsIgnoreCase("A"))
							tmRecords.append("<a title="+Utility.getLocaleValuePropByKey("lblDeactivate", locale)+" href='javascript:void(0);' onclick=\"return activateDeactivateJob("+entityID+","+jobOrderDetails.getJobId()+",'I')\"><i class='fa fa-times fa-lg' /></a>");
						else
							tmRecords.append("<a title="+Utility.getLocaleValuePropByKey("msgActivateicon", locale)+" href='javascript:void(0);' onclick=\"return activateDeactivateJob("+entityID+","+jobOrderDetails.getJobId()+",'A')\"><i class='fa fa-check fa-lg' /></a>");
					}else{
						tmRecords.append("&nbsp;");
					}
					if(user!=null){
						
						String text = jobLabelText.get(jobOrderDetails.getJobId());
						if(text!=null)
							tmRecords.append(text);
						else
						{
							if(jobOrderDetails.getApprovalBeforeGoLive()!=null && jobOrderDetails.getApprovalBeforeGoLive()==0){
								tmRecords.append("&nbsp;|<a href='javascript:void(0);' onclick=\"return approveJob("+entityID+","+jobOrderDetails.getJobId()+","+userMaster.getUserId()+")\">"+lblApprovalRequest+"</a>");
							}else if(jobOrderDetails.getApprovalBeforeGoLive()!=null && jobOrderDetails.getApprovalBeforeGoLive()==1){
								tmRecords.append("&nbsp;|Job Approved");
							}else if(jobOrderDetails.getApprovalBeforeGoLive()!=null && jobOrderDetails.getApprovalBeforeGoLive()!=0&& jobOrderDetails.getApprovalBeforeGoLive()!=1)
						       	{
										List<UserMaster> tempUsers=null;  
										tempUsers=mapofApprovedUsers.get(jobOrderDetails.getJobId());
										if(tempUsers==null){
											tmRecords.append("&nbsp;|<a href='javascript:void(0);' onclick=\"return approveJob("+entityID+","+jobOrderDetails.getJobId()+","+userMaster.getUserId()+")\"></a>"+lblApprovalRequest+"");
										}else if(tempUsers!=null && tempUsers.size()>0){
											if(tempUsers.contains(user)){
												tmRecords.append("&nbsp;|"+Utility.getLocaleValuePropByKey("msgPendingOthers", locale)+"");
											}else{
												tmRecords.append("&nbsp;|<a href='javascript:void(0);' onclick=\"return approveJob("+entityID+","+jobOrderDetails.getJobId()+","+userMaster.getUserId()+")\">"+lblApprovalRequest+"</a>");
											}
										}
						       	}
						}//End of else	
					}
					if(jobOrderDetails.getIsInviteOnly() !=null && jobOrderDetails.getIsInviteOnly()){
						tmRecords.append("&nbsp;|<a  href='importcandidatedetails.do?jobId="+jobOrderDetails.getJobId()+"' rel='tooltip' id='exelexport"+count+"' data-original-title='Upload Candidates against this job'><i class='icon-upload' style='font-size: 1.5em;'></i></a>");
						tmRecords.append("<script>$('#exelexport"+count+"').tooltip();</script>");
					}
					tmRecords.append(" </td>");
				}
			}
		}else{
			tmRecords.append("<tr><td colspan='10'>"+lblNoRecord+"</td></tr>" );
		}
		tmRecords.append("</table>");
		tmRecords.append(PaginationAndSorting.getPaginationStringForManageJobOrdersAjax(request,totalRecord,noOfRow, pageNo));
	}catch (Exception e) 
	{
		e.printStackTrace();
	}

	return tmRecords.toString();
}

private Map<Integer, String> getJobLevelText(Integer entityID, DistrictKeyContact userKeyContact, List<JobOrder> lstJobOrder )
{
	Map<Integer, String> jobLabelText = new HashMap<Integer, String>();
		
	try
	{
		if(entityID!=null && userKeyContact!=null && userKeyContact.getUserMaster()!=null && userKeyContact.getUserMaster().getDistrictId()!=null && lstJobOrder!=null && lstJobOrder.size()>0)
		{
			UserMaster user = userKeyContact.getUserMaster();
			DistrictMaster districtMaster = user.getDistrictId();
			
			Map<Integer, List<DistrictApprovalGroups>> jobcategoryWithDistrictApprovalGroupMap = new HashMap<Integer, List<DistrictApprovalGroups>>();
			Map<String, List<DistrictApprovalGroups>> flagWithDistrictApprovalGroupMap = new HashMap<String, List<DistrictApprovalGroups>>();
			List<String> jobHistoryWithDistrictApprovalGroup = new ArrayList<String>();
			
			//Getting the districtApprovalGroup jobcategorywise
			List<DistrictApprovalGroups> districtApprovalGroupList = districtApprovalGroupsDAO.findDistrictApprovalGroupsByDistrictIdAndJobIdIsNull(districtMaster);
			for(DistrictApprovalGroups dag : districtApprovalGroupList)
			{
				List<DistrictApprovalGroups> dagList = jobcategoryWithDistrictApprovalGroupMap.get(dag.getJobCategoryId().getJobCategoryId());
				if(dagList==null)
					dagList = new ArrayList<DistrictApprovalGroups>();
				dagList.add(dag);
				jobcategoryWithDistrictApprovalGroupMap.put(dag.getJobCategoryId().getJobCategoryId(), dagList);
			}
			
			{
				try
				{
				Criterion criterion3 = Restrictions.eq("districtMaster",districtMaster);
				Criterion criterion4 = Restrictions.eq("status","A");
				
				List<DistrictSpecificApprovalFlow> districtSpecificApprovalFlow = districtSpecificApprovalFlowDAO.findByCriteria(criterion3, criterion4);
				if(districtSpecificApprovalFlow!=null)
				for(DistrictSpecificApprovalFlow approvalFlow : districtSpecificApprovalFlow)
				{
					
					String spflag = approvalFlow.getSpecialEdFlag();
					List<DistrictApprovalGroups> approvalGroups = districtSpecificApprovalFlowDAO.findDistrictApprovalGroupByApprovalFlow(approvalFlow.getApprovalFlow(), districtMaster);
					flagWithDistrictApprovalGroupMap.put("#"+spflag+"#", approvalGroups);
				}
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
			}
			
			//Getting the jobHistroy
			List<JobApprovalHistory> jobApprovalHistoryList = jobApprovalHistoryDAO.findByCriteria(Restrictions.in("jobOrder", lstJobOrder));
			for(JobApprovalHistory jah : jobApprovalHistoryList)
			{
				if(jah.getDistrictApprovalGroups()!=null && jah.getJobOrder()!=null)
					jobHistoryWithDistrictApprovalGroup.add(jah.getDistrictApprovalGroups().getDistrictApprovalGroupsId()+"_"+jah.getJobOrder().getJobId());
			}
			System.out.println("JobApproval History:- "+jobHistoryWithDistrictApprovalGroup);
			
			String ESGroupName = "";
			try
			{
				Criterion criterion1 = Restrictions.eq("groupShortName", "ES");
				Criterion criterion2 = Restrictions.eq("status", "A");
				Criterion criterion3 = Restrictions.eq("districtId", districtMaster);
				List<DistrictWiseApprovalGroup> wiseApprovalGroups = districtWiseApprovalGroupDAO.findByCriteria(criterion1,criterion2,criterion3);
				if(wiseApprovalGroups!=null && wiseApprovalGroups.size()>0)
					ESGroupName = wiseApprovalGroups.get(0).getGroupName();
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			
			for(JobOrder jobOrder : lstJobOrder)	
			{
				if(jobOrder.getApprovalBeforeGoLive()!=null && jobOrder.getApprovalBeforeGoLive().equals(1))
					jobLabelText.put(jobOrder.getJobId(), "&nbsp;|Job Approved");
				else
				{
					JobCategoryMaster jobCategoryMaster = jobOrder.getJobCategoryMaster();
					if((jobCategoryMaster!=null && jobCategoryMaster.getApprovalBeforeGoLive()!=null && jobCategoryMaster.getApprovalBeforeGoLive() && jobCategoryMaster.getBuildApprovalGroup()!=null && jobCategoryMaster.getBuildApprovalGroup()) || (jobCategoryMaster!=null && jobCategoryMaster.getApprovalByPredefinedGroups()!=null && jobCategoryMaster.getApprovalByPredefinedGroups()) )
					{
						List<DistrictApprovalGroups> dagList = null;//jobcategoryWithDistrictApprovalGroupMap.get(jobCategoryMaster.getJobCategoryId());
						if(jobCategoryMaster.getApprovalByPredefinedGroups()!=null && jobCategoryMaster.getApprovalByPredefinedGroups())
						{
							dagList=flagWithDistrictApprovalGroupMap.get("#"+jobOrder.getSpecialEdFlag()+"#");
							//System.out.println("FROM 222222222 :- "+dagList);
						}
						else
							dagList = jobcategoryWithDistrictApprovalGroupMap.get(jobCategoryMaster.getJobCategoryId());
						
						if(dagList!=null && dagList.size()>0)	//Code according to grouping
						{
							//jobLabelText
							DistrictApprovalGroups previousApprovalGroup=null;
							DistrictApprovalGroups currentApprovalGroup=null;
							DistrictApprovalGroups nextApprovalGroup=null;
							for(int i=0; i<dagList.size(); i++)
							{
								boolean flag=false;
								if(jobCategoryMaster.getApprovalByPredefinedGroups()!=null && jobCategoryMaster.getApprovalByPredefinedGroups())
								{
									String primaryEmailAddress = jobOrder.getEmploymentServicesTechnician().getPrimaryEmailAddress();
									String backupEmailAddress = jobOrder.getEmploymentServicesTechnician().getBackupEmailAddress();
									
									primaryEmailAddress = primaryEmailAddress==null? "" : primaryEmailAddress.trim();
									backupEmailAddress = backupEmailAddress==null? "" : backupEmailAddress.trim();
									
									System.out.println("primaryEmailAddress:- "+primaryEmailAddress+", backupEmailAddress:- "+backupEmailAddress);
									String userEmailAddress = userKeyContact.getUserMaster().getEmailAddress().trim();
									System.out.println("if Value:- "+ (userEmailAddress.equalsIgnoreCase(primaryEmailAddress) || userEmailAddress.equalsIgnoreCase(backupEmailAddress)) );
									
									if(userEmailAddress.equalsIgnoreCase(primaryEmailAddress) || userEmailAddress.equalsIgnoreCase(backupEmailAddress))
										flag=true;
									
									System.out.println("flag:- "+flag+", ESGroupName:- "+ESGroupName);
								}
								
								if(dagList.get(i).getGroupMembers().contains("#"+userKeyContact.getKeyContactId()+"#") || flag)
								{
									String jhdap = dagList.get(i).getDistrictApprovalGroupsId()+"_"+jobOrder.getJobId();
									System.out.println("jhdap:- "+jhdap+"jobHistoryWithDistrictApprovalGroup.contains(jhdap):- "+jobHistoryWithDistrictApprovalGroup.contains(jhdap));
									if(!jobHistoryWithDistrictApprovalGroup.contains(jhdap))//identify that currentApproval Group does not approve the job
									{
										if(flag)
										{
											if(dagList.get(i).getGroupName().equalsIgnoreCase(ESGroupName))
											{
												currentApprovalGroup = dagList.get(i);
												i=i+1;
												if(i<dagList.size())
													nextApprovalGroup = dagList.get(i);
												
												//System.out.println("JOBiD:- "+jobOrder.getJobId()+", "+previousApprovalGroup+", "+currentApprovalGroup+", "+nextApprovalGroup);
												break;
											}
										}
										else
										{
											currentApprovalGroup = dagList.get(i);
											i=i+1;
											if(i<dagList.size())
												nextApprovalGroup = dagList.get(i);
											
											//System.out.println("JOBiD:- "+jobOrder.getJobId()+", "+previousApprovalGroup+", "+currentApprovalGroup+", "+nextApprovalGroup);
											break;
										}
									}
								}
								previousApprovalGroup = dagList.get(i);
							}
							
							System.out.println(jobOrder.getJobId()+", currentApprovalGroup:- "+currentApprovalGroup+", nextApprovalGroup:- "+nextApprovalGroup+", previousApprovalGroup:- "+previousApprovalGroup);
							if(currentApprovalGroup!=null)
							{
								String jhdap = currentApprovalGroup.getDistrictApprovalGroupsId()+"_"+jobOrder.getJobId();
								if(jobHistoryWithDistrictApprovalGroup.contains(jhdap))	//CurrentGroup Approved The Job 
								{
									if(nextApprovalGroup==null)								
										jobLabelText.put(jobOrder.getJobId(), "&nbsp;|Job Approved");	//CurrentGroup is the Last Group
									else
									{
										jobLabelText.put(jobOrder.getJobId(), "&nbsp;|Pending Others");//CurrentGroup is not the Last Group
										System.out.println("11111111111111111111111");
									}
								}
								else
								if(!jobHistoryWithDistrictApprovalGroup.contains(jhdap))	//CurrentApprovalGroup does not approve the job.
								{
									if(previousApprovalGroup==null)
										jobLabelText.put(jobOrder.getJobId(), "&nbsp;|<a href='javascript:void(0);' onclick=\"return approveJob("+entityID+","+jobOrder.getJobId()+","+user.getUserId()+")\">Approval Request</a> &nbsp; |<a href='#' onclick='return denyJob("+jobOrder.getJobId()+");'>Deny</a>");	//It's the First Group
									else
									{
										jhdap = previousApprovalGroup.getDistrictApprovalGroupsId()+"_"+jobOrder.getJobId();
										if(jobHistoryWithDistrictApprovalGroup.contains(jhdap))			//Previous Group Approved The Job
											jobLabelText.put(jobOrder.getJobId(), "&nbsp;|<a href='javascript:void(0);' onclick=\"return approveJob("+entityID+","+jobOrder.getJobId()+","+user.getUserId()+")\">Approval Request</a> &nbsp; |<a href='#' onclick='return denyJob("+jobOrder.getJobId()+");'>Deny</a>");
										else
										{
											jobLabelText.put(jobOrder.getJobId(), "&nbsp;|Pending Others");
											System.out.println("2222222222222222222222222");
										}
									}
								}
							}//End of if currentApprovalGroup
							else
							{
								jobLabelText.put(jobOrder.getJobId(), "&nbsp;|Pending Others");	//If user does not exist into group
								System.out.println("33333333333333333333333333333");
							}
						}
					}
				}
			}
		}
	}
	catch(Exception e)
	{
		e.printStackTrace();
	}
	return jobLabelText;
}

private Map<Integer, String> getUserJobLevelText(Integer entityID, UserMaster schoolAdmin, List<JobOrder> lstJobOrder )
{
	Map<Integer, String> jobLabelText = new HashMap<Integer, String>();
		
	try
	{
		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>.");
		if(entityID==3 && schoolAdmin!=null && schoolAdmin.getDistrictId()!=null && schoolAdmin.getDistrictId().getDistrictId().equals(804800) && schoolAdmin.getRoleId().getRoleId()==3 && lstJobOrder!=null && lstJobOrder.size()>0)
		{
			Map<String,DistrictSpecificApprovalFlow> dsafMap=new LinkedHashMap<String,DistrictSpecificApprovalFlow>();
			{
				Criterion cri1 = Restrictions.eq("districtMaster", schoolAdmin.getDistrictId());
				Criterion cri4 = Restrictions.eq("status", "A");
				List<DistrictSpecificApprovalFlow> districtSpecificApprovalFlowList =  districtSpecificApprovalFlowDAO.findByCriteria(cri1,cri4);
				if(districtSpecificApprovalFlowList!=null && districtSpecificApprovalFlowList.size()>0)
				{
					for(DistrictSpecificApprovalFlow dsaf:districtSpecificApprovalFlowList){
						dsafMap.put("##"+dsaf.getSpecialEdFlag(), dsaf);
					}
				}
			}
			
			/*System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<DistrictSpecificApprovalFlow>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
			for(Map.Entry<String,DistrictSpecificApprovalFlow> entry:dsafMap.entrySet()){
				System.out.println("key========"+entry.getKey()+"      value==" +entry.getValue());
			}*/
			
			
			Map<String,DistrictWiseApprovalGroup> dsaGMap=new LinkedHashMap<String,DistrictWiseApprovalGroup>();
			List<String> groupNames = new ArrayList<String>();
			{
				Criterion cri1 = Restrictions.eq("districtId", schoolAdmin.getDistrictId());
				Criterion cri4 = Restrictions.eq("status", "A");
				List<DistrictWiseApprovalGroup> lstDistrictApprovalGroups =  districtWiseApprovalGroupDAO.findByCriteria(cri1,cri4);
				if(lstDistrictApprovalGroups!=null && lstDistrictApprovalGroups.size()>0)
				{
					for(DistrictWiseApprovalGroup dsag:lstDistrictApprovalGroups){
						dsaGMap.put(dsag.getGroupShortName(), dsag);
						groupNames.add(dsag.getGroupName());
					}
				}
			}
			
			/*System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<DistrictWiseApprovalGroup>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
			for(Map.Entry<String,DistrictWiseApprovalGroup> entry:dsaGMap.entrySet()){
				System.out.println("key========"+entry.getKey()+"      value==" +entry.getValue());
			}*/
			
			Map<String,DistrictApprovalGroups> distritcApprovalGroupList =new LinkedHashMap<String,DistrictApprovalGroups>();
			Criterion cri2 = Restrictions.in("groupName",groupNames);
			Criterion cri1 = Restrictions.eq("districtId", schoolAdmin.getDistrictId());
			Criterion cri3 = Restrictions.isNull("jobCategoryId");
			Criterion cri4 = Restrictions.isNull("jobId");
			//Criterion cri4 = Restrictions.eq("status", "A");
			List<DistrictApprovalGroups> lstDistrictApprovalGroups = districtApprovalGroupsDAO.findByCriteria(cri1,cri2,cri3,cri4);
			
			/*for(Map.Entry<String, DistrictWiseApprovalGroup> entry :distritcApprovalGroupList.entrySet())
			for(DistrictWiseApprovalGroup dsag:lstDistrictApprovalGroups){
				dsaGMap.put(dsag.getGroupShortName(), dsag);
				groupNames.add(dsag.getGroupName());
			}
			*/
			for(DistrictApprovalGroups dsag:lstDistrictApprovalGroups){
				distritcApprovalGroupList.put(dsag.getGroupName(), dsag);
			}
			
			
			/*System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<DistrictApprovalGroups>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
			for(Map.Entry<String,DistrictApprovalGroups> entry:distritcApprovalGroupList.entrySet()){
				System.out.println("key========"+entry.getKey()+"      value==" +entry.getValue());
			}*/
			
			
			UserMaster user = schoolAdmin;
			/*
			DistrictMaster districtMaster = user.getDistrictId();
			*/
			Map<Integer, List<DistrictApprovalGroups>> jobcategoryWithDistrictApprovalGroupMap = new HashMap<Integer, List<DistrictApprovalGroups>>();
			List<String> jobHistoryWithDistrictApprovalGroup = new ArrayList<String>();
			
			//Getting the districtApprovalGroup jobcategorywise
			List<DistrictApprovalGroups> districtApprovalGroupList = districtApprovalGroupsDAO.findDistrictApprovalGroupsByDistrictIdAndJobIdIsNull(schoolAdmin.getDistrictId());
			for(DistrictApprovalGroups dag : districtApprovalGroupList)
			{
				List<DistrictApprovalGroups> dagList = jobcategoryWithDistrictApprovalGroupMap.get(dag.getJobCategoryId().getJobCategoryId());
				if(dagList==null)
					dagList = new ArrayList<DistrictApprovalGroups>();
				dagList.add(dag);
				jobcategoryWithDistrictApprovalGroupMap.put(dag.getJobCategoryId().getJobCategoryId(), dagList);
			}
			
			
			//Getting the jobHistroy
			//List<String> jobHistoryWithDistrictApprovalGroup = new ArrayList<String>();
			List<JobApprovalHistory> jobApprovalHistoryList = jobApprovalHistoryDAO.findByCriteria(Restrictions.in("jobOrder", lstJobOrder));
			for(JobApprovalHistory jah : jobApprovalHistoryList)
			{
				if(jah.getDistrictApprovalGroups()!=null && jah.getJobOrder()!=null)
					jobHistoryWithDistrictApprovalGroup.add(jah.getDistrictApprovalGroups().getDistrictApprovalGroupsId()+"_"+jah.getJobOrder().getJobId());
			}
			
			for(JobOrder jobOrder : lstJobOrder)	
			{
				if(jobOrder.getApprovalBeforeGoLive()!=null && jobOrder.getApprovalBeforeGoLive().equals(1))
					jobLabelText.put(jobOrder.getJobId(), "&nbsp;|Job Approved");
				else
				{
					JobCategoryMaster jobCategoryMaster = jobOrder.getJobCategoryMaster();
					if((jobCategoryMaster!=null && jobCategoryMaster.getApprovalBeforeGoLive()!=null && jobCategoryMaster.getApprovalBeforeGoLive() && jobCategoryMaster.getBuildApprovalGroup()!=null && jobCategoryMaster.getBuildApprovalGroup()) || (jobCategoryMaster.getApprovalByPredefinedGroups()!=null && jobCategoryMaster.getApprovalByPredefinedGroups()) )
					{
					    String specialFlag = jobOrder.getSpecialEdFlag();
						
						DistrictSpecificApprovalFlow checkNextStep=dsafMap.get("##"+specialFlag);
						
						DistrictApprovalGroups previousApprovalGroup=null;
						DistrictApprovalGroups currentApprovalGroup=null;
						DistrictApprovalGroups nextApprovalGroup=null;
						
						if(checkNextStep!=null && checkNextStep.getApprovalFlow().contains("#SA#"))
						{
							Boolean previousFlag=false;
							String previousGroup="";
							Boolean nextFlag=false;
							String nextGroup="";
							//System.out.println("checkNextStep.getApprovalFlow()====================="+checkNextStep.getApprovalFlow());
							String checkNextStep1=checkNextStep.getApprovalFlow().trim().equals("")?"":checkNextStep.getApprovalFlow().substring(1,(checkNextStep.getApprovalFlow().length()-1));
							String[] arrayGroup=checkNextStep1.split("#");
							//System.out.println("arrayGroup============"+Arrays.asList(arrayGroup));
							for(int i=0;i<arrayGroup.length;i++){
								//System.out.println("checking ArrayGroup============="+arrayGroup[i]);
								if(arrayGroup[i].equalsIgnoreCase("SA")){
									if(i!=0){
										previousGroup=arrayGroup[i-1];
										previousFlag=true;
									}
									try{
										if(arrayGroup[i+1]!=null && !arrayGroup[i+1].trim().equals("")){
										nextFlag=true;
										nextGroup=arrayGroup[i+1];
										}
									}catch(ArrayIndexOutOfBoundsException a){a.printStackTrace();nextFlag=false;}
									break;
								}
									
							}
							List<String> flow = new ArrayList<String>();
							if(checkNextStep.getApprovalFlow().contains("SA"))
							{
								previousApprovalGroup=distritcApprovalGroupList.get(dsaGMap.get(previousGroup)!=null?dsaGMap.get(previousGroup).getGroupName():null);
								//System.out.println("dsaGMap.get(SA)================="+dsaGMap.get("SA").getGroupName());
								//System.out.println("distritcApprovalGroupList======="+distritcApprovalGroupList.get(dsaGMap.get("SA").getGroupName()));
								currentApprovalGroup =distritcApprovalGroupList.get(dsaGMap.get("SA").getGroupName());
								nextApprovalGroup=distritcApprovalGroupList.get(dsaGMap.get(nextGroup)!=null?dsaGMap.get(nextGroup).getGroupName():null);
								
								System.out.println("previousApprovalGroup==========="+previousApprovalGroup);
								System.out.println("currentApprovalGroup==========="+currentApprovalGroup);
								System.out.println("nextApprovalGroup==========="+nextApprovalGroup);
								
								if(currentApprovalGroup!=null)
								{
									String jhdap = currentApprovalGroup.getDistrictApprovalGroupsId()+"_"+jobOrder.getJobId();
									if(jobHistoryWithDistrictApprovalGroup.contains(jhdap))	//CurrentGroup Approved The Job 
									{
										if(nextApprovalGroup==null)								
											jobLabelText.put(jobOrder.getJobId(), "&nbsp;|Job Approved");	//CurrentGroup is the Last Group
										else
											jobLabelText.put(jobOrder.getJobId(), "&nbsp;|Pending Others");//CurrentGroup is not the Last Group
									}
									else
									if(!jobHistoryWithDistrictApprovalGroup.contains(jhdap))	//CurrentApprovalGroup does not approve the job.
									{
										if(previousApprovalGroup==null)
											jobLabelText.put(jobOrder.getJobId(), "&nbsp;|<a href='javascript:void(0);' onclick=\"return approveJob("+entityID+","+jobOrder.getJobId()+","+user.getUserId()+")\">Approval Request</a> &nbsp; |<a href='#' onclick='return denyJob("+jobOrder.getJobId()+");'>Deny</a>");	//It's the First Group
										else
										{
											jhdap = previousApprovalGroup.getDistrictApprovalGroupsId()+"_"+jobOrder.getJobId();
											if(jobHistoryWithDistrictApprovalGroup.contains(jhdap))			//Previous Group Approved The Job
												jobLabelText.put(jobOrder.getJobId(), "&nbsp;|<a href='javascript:void(0);' onclick=\"return approveJob("+entityID+","+jobOrder.getJobId()+","+user.getUserId()+")\">Approval Request</a> &nbsp; |<a href='#' onclick='return denyJob("+jobOrder.getJobId()+");'>Deny</a>");
											else
												jobLabelText.put(jobOrder.getJobId(), "&nbsp;|Pending Others");
										}
									}
								}//End of if currentApprovalGroup
								else
									jobLabelText.put(jobOrder.getJobId(), "&nbsp;|Pending Others");	//If user does not exist into group

								
							}
						}
					}
				}
			}
		}
		else
			System.out.println("This is not a jeffco district");
		System.out.println("jobLabelText:- "+jobLabelText);
		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
	}
	catch(Exception e)
	{
		e.printStackTrace();
	}
	return jobLabelText;
}

/*
private void getApprovalGroupHistory(DistrictApprovalGroups districtApprovalGroup, UserMaster user)
{
	Map<Integer, List<JobApprovalHistory>> approveUserMap= new HashMap<Integer, List<JobApprovalHistory>>();
	List<JobApprovalHistory> jobApprovalHistoryList = jobApprovalHistoryDAO.findByCriteria( Restrictions.eq("districtApprovalGroups", districtApprovalGroup) );
	
	if(jobApprovalHistoryList!=null && jobApprovalHistoryList.size()>0)
	{
		for(JobApprovalHistory approvalHistory : jobApprovalHistoryList)
		{
			Integer jobId = approvalHistory.getJobOrder().getJobId();
			List<JobApprovalHistory> jobApprovalHistories = approveUserMap.get(jobId);
			
			if(jobApprovalHistories!=null)
			{
				jobApprovalHistories.add(approvalHistory);
				approveUserMap.put(jobId, jobApprovalHistories);
			}
			else
			{
				List<JobApprovalHistory> listOfJobApprovalHistory = new ArrayList<JobApprovalHistory>();
				listOfJobApprovalHistory.add(approvalHistory);
				approveUserMap.put(approvalHistory.getJobOrder().getJobId(), listOfJobApprovalHistory);
			}
		}
	}

	for(Map.Entry<Integer, List<JobApprovalHistory>> entry : approveUserMap.entrySet())
	{
		Integer noOfApproval = entry.getValue().get(0).getDistrictApprovalGroups().getNoOfApprovals();
		Integer noOfMember = (entry.getValue().get(0).getDistrictApprovalGroups().getGroupMembers().split("#").length-1);
		Integer noOfApprovalNeeded= 0;
		
		noOfApprovalNeeded = (noOfApproval > noOfMember)? noOfMember : noOfApproval;  
		
		if(noOfApprovalNeeded< entry.getValue().size())
			currentHistoryMap.put(entry.getKey(), "Pending Others");	//This group has approved.
		else
		if(noOfApprovalNeeded>entry.getValue().size())
		{
			boolean userFound=false;
			for(JobApprovalHistory history : entry.getValue())
				if(history.getUserMaster().getUserId().equals(user.getUserId()))
				{
					userFound=true;
					break;
				}
			
			if(userFound)
				currentHistoryMap.put(entry.getKey(), "Pending Others");	//This group has approved.
			else
				currentHistoryMap.put(entry.getKey(), "Approval Request");	//Approval Request
		}
		else
		if(noOfApprovalNeeded==entry.getValue().size())
		{
			if(nextApprovalGroup!=null)
				currentHistoryMap.put(entry.getKey(), "Pending Others");
			else
				currentHistoryMap.put(entry.getKey(), "Approved");
		}
		System.out.println("noOfApproval:- "+noOfApproval+", noOfMember:- "+noOfMember+", noOfApprovalNeeded:- "+noOfApprovalNeeded+", no of user has approved the job:- "+entry.getValue().size()+",Status:- "+currentHistoryMap.get(entry.getKey())+", jobId:- "+entry.getKey());
	}
	
}
*/
public String getStatusListForVVI(String districtId,String jobId)
{
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	}
	
	StringBuffer status=	new StringBuffer();
	
	List<StatusMaster> statusMasterList = null;
	List<StatusMaster> lstStatusMaster = null;
	
	try
	{
		
		Map<Integer,SecondaryStatus> mapSStatus = new HashMap<Integer, SecondaryStatus>();
		
		DistrictMaster districtMaster = districtMasterDAO.findById(Integer.parseInt(districtId), false, false);
		List<SecondaryStatus> lstSecStatus=	secondaryStatusDAO.findSecondaryStatusOnlyStatus(districtMaster);
		List<SecondaryStatus> lstsecondaryStatus=  secondaryStatusDAO.findSecondaryStatusOnly(districtMaster);
		JobOrder jobOrder = null;
		
		if(jobId!=null && !jobId.equals(""))
			jobOrder =jobOrderDAO.findById(Integer.parseInt(jobId), false, false);
		
		System.out.println(" jobOrder for VVI :: "+jobOrder);
		
		if(lstSecStatus!=null)
		for(SecondaryStatus secStatus : lstSecStatus){
			mapSStatus.put(secStatus.getStatusMaster().getStatusId(),secStatus);
		}
		
		
		SecondaryStatus secondaryStatus=null;

		String[] statuss = {"hird","scomp","ecomp","vcomp","dcln","rem","widrw"};
		lstStatusMaster	=	statusMasterDAO.findStatusByStatusByShortNames(statuss);
		
		statusMasterList = new ArrayList<StatusMaster>();
		
		for(StatusMaster sm: lstStatusMaster){
			if(sm.getBanchmarkStatus()==1){
				secondaryStatus=mapSStatus.get(sm.getStatusId());
				if(secondaryStatus!=null){
					sm.setStatus(secondaryStatus.getSecondaryStatusName());
				}
			}
			statusMasterList.add(sm);
		}
		
		status.append("<select class='form-control' id='slctStatusID'>");
		status.append("<option id='' value='' ></option>");
		
		//For Add
		if(jobOrder==null)
		{
			if(statusMasterList!=null && statusMasterList.size()>0)
			{
				for(StatusMaster sm:statusMasterList)
				{
					status.append("<option value='"+sm.getStatusId()+"' >"+sm.getStatus()+"</option>");
				}
			}
			
			if(lstsecondaryStatus!=null && lstsecondaryStatus.size()>0)
			{
				for(SecondaryStatus ssm:lstsecondaryStatus)
				{
					status.append("<option value='SSID_"+ssm.getSecondaryStatusId()+"' >"+ssm.getSecondaryStatusName()+"</option>");
				}
			}
		}
		else // For Edit
		{
			if(statusMasterList!=null && statusMasterList.size()>0)
			{
				
				for(StatusMaster sm:statusMasterList)
				{
						if(jobOrder.getStatusMaster()!=null)
						{
							if(jobOrder.getStatusMaster().getStatusId().equals(sm.getStatusId()))
								status.append("<option value='"+sm.getStatusId()+"' selected>"+sm.getStatus()+"</option>");
							else
								status.append("<option value='"+sm.getStatusId()+"' >"+sm.getStatus()+"</option>");
						}
						else
							status.append("<option value='"+sm.getStatusId()+"' >"+sm.getStatus()+"</option>");
				}
			}
			if(lstsecondaryStatus!=null && lstsecondaryStatus.size()>0)
			{
				for(SecondaryStatus ssm:lstsecondaryStatus)
				{
					if(jobOrder.getSecondaryStatus()!=null){
							if(jobOrder.getSecondaryStatus().getSecondaryStatusId().equals(ssm.getSecondaryStatusId()))
								status.append("<option value='SSID_"+ssm.getSecondaryStatusId()+"' selected >"+ssm.getSecondaryStatusName()+"</option>");
							else
								status.append("<option value='SSID_"+ssm.getSecondaryStatusId()+"' >"+ssm.getSecondaryStatusName()+"</option>");
					}else
						status.append("<option value='SSID_"+ssm.getSecondaryStatusId()+"' >"+ssm.getSecondaryStatusName()+"</option>");
				}
			}
		}
		
		
		status.append("</select>");
		
	}catch(Exception e)
	{
		e.printStackTrace();
	}

	return status.toString();
}

@Transactional(readOnly=false)
public JobCategoryMaster getVVIDefaultFields(Integer jobCategoryId)
{
	System.out.println(" ====== getVVIDefaultFields =======");
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	}
	
	JobCategoryMaster jobCategoryMaster	= new JobCategoryMaster();	
		
	try{
		if(jobCategoryId!=null){
			jobCategoryMaster = jobCategoryMasterDAO.findById(jobCategoryId, false, false);
		}
	}catch (Exception e) {
		e.printStackTrace();
	}
	return jobCategoryMaster;
}

public String displayAllDistrictAssessment(Integer jobcategoryId,Integer jobId)
{
	System.out.println(" ================1111111111=== displayAllDistrictAssessment 444================== ");
	
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	StringBuffer sb=new StringBuffer();
	StringBuffer sbAttachSchool=new StringBuffer();
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	}
	
	boolean selectedDAS = false;
	JobCategoryMaster jobCategoryMaster = null;
	JobOrder jobOrder = null;
	try
	{
		jobCategoryMaster = jobCategoryMasterDAO.findById(jobcategoryId, false, false);
		
		if(jobCategoryMaster!=null)
		{
			System.out.println(" jobId "+jobId);
			
			if(jobId!=null && !jobId.equals(""))
				jobOrder = jobOrderDAO.findById(jobId, false, false);
			
			//Job category Wise Data
			if(jobOrder!=null)
			{
				List<DistrictAssessmentDetail> AlldistrictAssessmentDetails = new ArrayList<DistrictAssessmentDetail>();
				List<DistrictAssessmentDetail> selecteddistAssessmentDetails = new ArrayList<DistrictAssessmentDetail>();
				
				AlldistrictAssessmentDetails = districtAssessmentDetailDAO.findByDistrict(jobCategoryMaster.getDistrictMaster());
				
				List<DistrictAssessmentJobRelation> districtAssessmentJobRelations = new ArrayList<DistrictAssessmentJobRelation>();
				
				
				boolean isDistAMt = false; 
				if(jobOrder.getOfferAssessmentInviteOnly()!=null)
					isDistAMt=jobOrder.getOfferAssessmentInviteOnly();
				System.out.println("isDistAMt: "+isDistAMt);
				
				if(isDistAMt)
				{
					districtAssessmentJobRelations = districtAssessmentJobRelationDAO.findRelationByJobOrder(jobOrder);
				}
				
				
				//String daIds = null;
				
				
			//	jobOrder.getDistrictAssessmentId();
				
				System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> districtAssessmentJobRelations :11111::::::::: "+districtAssessmentJobRelations.size());
				
				String arr[]=new String[districtAssessmentJobRelations.size()];
				List multiDAIds = new ArrayList();
				
				if(districtAssessmentJobRelations.size()>0)
				{
					for(int i=0;i<arr.length;i++)
					{
						arr[i]=districtAssessmentJobRelations.get(i).getDistrictAssessmentDetail().getDistrictAssessmentId().toString();
					}
					if(arr!=null && arr.length > 0)
					{
						for(int i=0;i<arr.length;i++)
						{
							multiDAIds.add(Integer.parseInt((arr[i])));	
							System.out.println(" multiDAIds "+multiDAIds.get(i));
						
						}
					}
				}
				
				
				
				
				/*if(daIds!=null && !daIds.equals(""))
				{
					if(daIds!="" && daIds.length() > 0)
					{
						arr=daIds.split("#");
					}
					
					if(arr!=null && arr.length > 0)
					{
						for(int i=0;i<arr.length;i++)
						{
							multiDAIds.add(Integer.parseInt((arr[i])));	
						}
					}
				}*/
				
				selecteddistAssessmentDetails = districtAssessmentDetailDAO.findByDistAssessmentIds(multiDAIds);
				
				System.out.println(" selecteddistAssessmentDetails :: "+selecteddistAssessmentDetails.size());
				
				AlldistrictAssessmentDetails.removeAll(selecteddistAssessmentDetails);

				System.out.println(" AlldistrictAssessmentDetails.removeAll(selecteddistAssessmentDetails) :: "+AlldistrictAssessmentDetails.removeAll(selecteddistAssessmentDetails));
				System.out.println(" AlldistrictAssessmentDetails :: "+AlldistrictAssessmentDetails.size());
				//Unselected DA List
				sb.append("<select multiple id=\"lstDistrictAdmins\" name=\"lstDistrictAdmins\" class=\"form-control\" style=\"height: 150px;\" >");
				if(AlldistrictAssessmentDetails.size()>0)
				{
					for (DistrictAssessmentDetail selectedDASMT : AlldistrictAssessmentDetails) {
						sb.append("<option value="+selectedDASMT.getDistrictAssessmentId()+">"+selectedDASMT.getDistrictAssessmentName()+"</option>");
					}
			//		selectedDAS = true;
				}
				sb.append("</select>");
				
				//Selected DA List
				sbAttachSchool.append("<select multiple class=\"form-control\" id=\"attachedDAList\" name=\"attachedDAList\" style=\"height: 150px;\">");
				if(selecteddistAssessmentDetails.size()>0)
				{
					for (DistrictAssessmentDetail selectdDASMT : selecteddistAssessmentDetails) {
						sbAttachSchool.append("<option value="+selectdDASMT.getDistrictAssessmentId()+">"+selectdDASMT.getDistrictAssessmentName()+"</option>");
					}
					System.out.println("01");
					selectedDAS = true;
				}
				sbAttachSchool.append("</select>");
			}else
			{
				List<DistrictAssessmentDetail> AlldistrictAssessmentDetails = new ArrayList<DistrictAssessmentDetail>();
				List<DistrictAssessmentDetail> selecteddistAssessmentDetails = new ArrayList<DistrictAssessmentDetail>();
				
				AlldistrictAssessmentDetails = districtAssessmentDetailDAO.findByDistrict(jobCategoryMaster.getDistrictMaster());
				
				System.out.println(" jobCategoryMaster.getDistrictAssessmentId() :: "+jobCategoryMaster.getDistrictAssessmentId());
				
				
				String daIds = jobCategoryMaster.getDistrictAssessmentId();
				
				String arr[]=null;
				List multiDAIds = new ArrayList();
				
				if(daIds!=null && !daIds.equals(""))
				{
					if(daIds!="" && daIds.length() > 0)
					{
						arr=daIds.split("#");
					}
					
					if(arr!=null && arr.length > 0)
					{
						for(int i=0;i<arr.length;i++)
						{
							multiDAIds.add(Integer.parseInt((arr[i])));	
						}
					}
				}
				
				selecteddistAssessmentDetails = districtAssessmentDetailDAO.findByDistAssessmentIds(multiDAIds);
				
				System.out.println(" selecteddistAssessmentDetails :: "+selecteddistAssessmentDetails.size());
				
				AlldistrictAssessmentDetails.removeAll(selecteddistAssessmentDetails);

				System.out.println(" AlldistrictAssessmentDetails.removeAll(selecteddistAssessmentDetails) :: "+AlldistrictAssessmentDetails.removeAll(selecteddistAssessmentDetails));
				System.out.println(" AlldistrictAssessmentDetails :: "+AlldistrictAssessmentDetails.size());
				//Unselected DA List
				sb.append("<select multiple id=\"lstDistrictAdmins\" name=\"lstDistrictAdmins\" class=\"form-control\" style=\"height: 150px;\" >");
				if(AlldistrictAssessmentDetails.size()>0)
				{
					for (DistrictAssessmentDetail selectedDASMT : AlldistrictAssessmentDetails) {
						sb.append("<option value="+selectedDASMT.getDistrictAssessmentId()+">"+selectedDASMT.getDistrictAssessmentName()+"</option>");
					}
			//		selectedDAS = true;
				}
				sb.append("</select>");
				
				//Selected DA List
				sbAttachSchool.append("<select multiple class=\"form-control\" id=\"attachedDAList\" name=\"attachedDAList\" style=\"height: 150px;\">");
				if(selecteddistAssessmentDetails.size()>0)
				{
					for (DistrictAssessmentDetail selectdDASMT : selecteddistAssessmentDetails) {
						sbAttachSchool.append("<option value="+selectdDASMT.getDistrictAssessmentId()+">"+selectdDASMT.getDistrictAssessmentName()+"</option>");
					}
					selectedDAS = true;	
				}
				sbAttachSchool.append("</select>");
			}
		}
		
	}catch(Exception e)
	{
		e.printStackTrace();
	}
	
	System.out.println(" sb.toString() :: "+sb.toString());
	System.out.println(" sbAttachSchool.toString() :: "+sbAttachSchool.toString());
	System.out.println(" selectedDAS :: "+selectedDAS);

	return sb.toString()+"||"+sbAttachSchool.toString()+"||"+selectedDAS;
}

public String getJobSubCategory(Integer jobId,Integer jobCateId,Integer districtId,Integer clonejobId)
{
	System.out.println(" jobId "+jobId+" jobCateId "+jobCateId);
	
	StringBuffer sb	=	new StringBuffer();
	try {
		DistrictMaster districtMaster	=	districtMasterDAO.findById(districtId, false, false);
		JobCategoryMaster jobCategoryMaster = jobCategoryMasterDAO.findById(jobCateId, false, false);
		/*=== values to dropdown from jobCategoryMaster Table ===*/
		List<JobCategoryMaster> jobCategoryMasterlst = jobCategoryMasterDAO.findAllJobSubCategoryByJobCateId(jobCategoryMaster,districtMaster);
		String jcjsiName="";
	
		if((jobId!=null && jobId>0) || (clonejobId!=null && clonejobId>0) )
		{
			JobOrder jobOrder = null;
		
			
			if(jobId!=null && jobId>0)
				jobOrder = jobOrderDAO.findById(jobId, false, false);
			
			if((clonejobId!=null && clonejobId>0))
				jobOrder = jobOrderDAO.findById(clonejobId, false, false);
			
			if(jobCategoryMasterlst.size()>0)
			{
				sb.append("<select id='jobSubCategoryId' name='jobSubCategoryId' class='form-control'  onchange='linkShowOrHide();'>");
				sb.append("<option value='0'>"+Utility.getLocaleValuePropByKey("msgSubJobCategoryName", locale)+"</option>");
				if(jobCategoryMasterlst!=null && jobCategoryMasterlst.size()>0){
					for(JobCategoryMaster jb: jobCategoryMasterlst)
					{
						if(jb.getAssessmentDocument()!=null)
						{
							jcjsiName	=	jb.getAssessmentDocument();
						}
						if(jobOrder.getJobCategoryMaster().getJobCategoryId().equals(jb.getJobCategoryId()))
							sb.append("<option linkShow='"+(jb.getApprovalByPredefinedGroups())+"' value='"+jb.getJobCategoryId()+"||"+jb.getBaseStatus()+"||"+jcjsiName+"' selected='selected'>"+jb.getJobCategoryName()+"</option>");
						else
							sb.append("<option linkShow='"+(jb.getApprovalByPredefinedGroups())+"' value='"+jb.getJobCategoryId()+"||"+jb.getBaseStatus()+"||"+jcjsiName+"'>"+jb.getJobCategoryName()+"</option>");
					}
				}

				sb.append("</select>");
			}
			
		}
		else
		{
			
			if(jobCategoryMasterlst.size()>0)
			{
				sb.append("<select id='jobSubCategoryId' name='jobSubCategoryId' class='form-control'  onchange='linkShowOrHide();'>");
				sb.append("<option value='0'>"+Utility.getLocaleValuePropByKey("msgSubJobCategoryName", locale)+"</option>");
				if(jobCategoryMasterlst!=null && jobCategoryMasterlst.size()>0){
					for(JobCategoryMaster jb: jobCategoryMasterlst)
					{
						if(jb.getAssessmentDocument()!=null)
						{
							jcjsiName	=	jb.getAssessmentDocument();
						}
						
						sb.append("<option linkShow='"+(jb.getApprovalByPredefinedGroups())+"' value='"+jb.getJobCategoryId()+"||"+jb.getBaseStatus()+"||"+jcjsiName+"'>"+jb.getJobCategoryName()+"</option>");
					}
				}

				sb.append("</select>");
			}
		}
		
		
		
	} catch (Exception e) 
	{
		e.printStackTrace();
	}

	return sb.toString();
}


public String getJobSubCategoryForSeach(Integer jobId,String jobCateIds,Integer districtId,Integer clonejobId)
{
	StringBuffer sb	=	new StringBuffer();
	
	try
	{
		String[] jobcateIdsArr = jobCateIds.split(",");
		List<Integer> jcateIds = new ArrayList<Integer>();
		List<JobCategoryMaster> jobcateList = new ArrayList<JobCategoryMaster>();
		
		System.out.println(" jobcateIdsArr length :: "+jobcateIdsArr.length);
		
		for(int i=0; i<jobcateIdsArr.length;i++)
		{
			jcateIds.add(Integer.parseInt(jobcateIdsArr[i]));
		}
		
		if(jcateIds.size()>0)
			//jobcateList = jobCategoryMasterDAO.findByCategoryID(jcateIds);
			jobcateList = jobCategoryMasterDAO.findByCategoryIDList(jcateIds);
		
		System.out.println(" jcateIds :: "+jcateIds);
		
		DistrictMaster districtMaster	=	districtMasterDAO.findById(districtId, false, false);
		List<JobCategoryMaster> jobCategoryMasterlst = jobCategoryMasterDAO.findAllJobSubCategoryByJobCateIds(jobcateList,districtMaster);
		String jcjsiName="";
		
		if(jobCategoryMasterlst.size()>0)
		{
			sb.append("<option value='0'>"+Utility.getLocaleValuePropByKey("optAll", locale)+"</option>");
			if(jobCategoryMasterlst!=null && jobCategoryMasterlst.size()>0){
				for(JobCategoryMaster jb: jobCategoryMasterlst)
				{
					if(jb.getAssessmentDocument()!=null)
					{
						jcjsiName =	jb.getAssessmentDocument();
					}
					
					sb.append("<option value='"+jb.getJobCategoryId()+"'>"+jb.getJobCategoryName()+"</option>");
				}
			}
		}
		else
		{
			sb.append("<option value='0'>"+Utility.getLocaleValuePropByKey("msgJobSubCatlist", locale)+"</option>");
		}
		
	}catch(Exception e)
	{
		e.printStackTrace();
	}
	
	return sb.toString();
}


public String getJobSubCateForSeach(Integer jobCateId,Integer districtId)
{
	StringBuffer sb	=	new StringBuffer();
	
	try
	{
		//String[] jobcateIdsArr = jobCateIds.split(",");
		List<Integer> jcateIds = new ArrayList<Integer>();
		List<JobCategoryMaster> jobcateList = new ArrayList<JobCategoryMaster>();
		
		jcateIds.add(jobCateId);
		
		if(jcateIds.size()>0)
			jobcateList = jobCategoryMasterDAO.findByCategoryID(jcateIds);
		
		
		DistrictMaster districtMaster	=	districtMasterDAO.findById(districtId, false, false);
		List<JobCategoryMaster> jobCategoryMasterlst = jobCategoryMasterDAO.findAllJobSubCategoryByJobCateIds(jobcateList,districtMaster);
		String jcjsiName="";
		
		
		sb.append("<select id='jobSubCategoryId' name='jobSubCategoryId' class='form-control'>");
		if(jobCategoryMasterlst.size()>0)
		{
			sb.append("<option value='0'>All</option>");
			if(jobCategoryMasterlst!=null && jobCategoryMasterlst.size()>0){
				for(JobCategoryMaster jb: jobCategoryMasterlst)
				{
					if(jb.getAssessmentDocument()!=null)
					{
						jcjsiName =	jb.getAssessmentDocument();
					}
					
					sb.append("<option value='"+jb.getJobCategoryId()+"'>"+jb.getJobCategoryName()+"</option>");
				}
			}
		}
		else
		{
			sb.append("<option value='0'>"+Utility.getLocaleValuePropByKey("optAll", locale)+"</option>");
		}
		sb.append("</select>");
	}catch(Exception e)
	{
		e.printStackTrace();
	}
	
	return sb.toString();
}


public Map<Integer,String> getPrimaryESTech(){
	Map<Integer,String> mapPrimaryESTech=new LinkedHashMap<Integer,String>();
	try{
		List<EmploymentServicesTechnician> lstEmploymentServicesTechnician=employmentServicesTechnicianDAO.findAll();
		for(EmploymentServicesTechnician eST:lstEmploymentServicesTechnician){
			mapPrimaryESTech.put(eST.getEmploymentservicestechnicianId(), eST.getPrimaryESTech());
		}
	}catch(Exception e){
		e.printStackTrace();
	}
	return mapPrimaryESTech;
}
public Map<Integer,String> getBackupESTech(String employmentServicesTechnicianId){
	System.out.println(":::::::::::::::::::::getBackupESTech:::::::::::::::::::::::::");
	Map<Integer,String> mapPrimaryESTech=new LinkedHashMap<Integer,String>();
	try{
		EmploymentServicesTechnician employmentServicesTechnician=employmentServicesTechnicianDAO.findById(Integer.parseInt(employmentServicesTechnicianId), false, false);
		if(employmentServicesTechnician!=null){
			//System.out.println("employmentServicesTechnician.getBackupESTech()=============="+employmentServicesTechnician.getBackupESTech());
			mapPrimaryESTech.put(employmentServicesTechnician.getEmploymentservicestechnicianId(), employmentServicesTechnician.getBackupESTech());
		}
	}catch(Exception e){
		e.printStackTrace();
	}
	return mapPrimaryESTech;
}

public String viewAdditionalDocument(Integer jobId)
{
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	String path="";
	try 
	{
		JobOrder jobOrder=null;
		if(jobId!=0){
			jobOrder=jobOrderDAO.findById(jobId, false, false);
		}
		String fileName= jobOrder.getAdditionalDocumentPath();

		String source=Utility.getValueOfPropByKey("districtRootPath")+jobOrder.getDistrictMaster().getDistrictId()+"/TempAdditionalDocumentFile/"+fileName;
		String target = context.getServletContext().getRealPath("/")+"/"+jobOrder.getDistrictMaster().getDistrictId()+"/TempAdditionalDocumentFile/";

		File sourceFile = new File(source);
		File targetDir = new File(target);
		if(!targetDir.exists())
			targetDir.mkdirs();

		File targetFile = new File(targetDir+"/"+sourceFile.getName());

		FileUtils.copyFile(sourceFile, targetFile);

		path = Utility.getValueOfPropByKey("contextBasePath")+jobOrder.getDistrictMaster().getDistrictId()+"/TempAdditionalDocumentFile/"+fileName;


	} 
	catch (Exception e) 
	{
		e.printStackTrace();
	}

	return path;
}

@Transactional(readOnly=false)
public void removeAdditionalDocumentFile(Integer districtId,String files,int savecancelFlag){
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
    }
	try 
	{	
		int d=1;
		if(savecancelFlag==2){
			d=0;
		}else if(savecancelFlag==0){
			File file = new File(Utility.getValueOfPropByKey("districtRootPath")+districtId+"/AdditionalDocument/"+files);
			if(file.exists()){
				file.delete();
			}
		}
		//System.out.println("districtId >>> "+districtId+"\n files >> "+files);
		String[] fileArrays=files.split("@##@");
		for(int x=0;x<fileArrays.length-d;x++){
			String fileName=fileArrays[x];
			//System.out.println(">>>>>>>>>>>>> fileNames>>>>> "+fileName);
			File file = new File(Utility.getValueOfPropByKey("districtRootPath")+districtId+"/JobDescription/"+fileName);
			if(file.exists()){
				file.delete();
			}
		}
		
	}
	catch (Exception e) 
	{
		e.printStackTrace();
	}
}


	/**
	 * used for updating jobforteacher  when job is activated from deactive status
	 * 
	 */
	@Transactional(readOnly=false)
	public void updateJobForTeacherStatus(int jobId)
	{
		System.out.println(":::::::::::::::::updateJobForTeacherStatus:::::::::::::::::::");
		try
		{
			JobOrder jobOrder=jobOrderDAO.findById(jobId, false, false);
			List<TeacherDetail> teacherDetails=new ArrayList<TeacherDetail>();
			List<JobForTeacher> list=jobForTeacherDAO.findbyJobId(jobOrder);
			for (JobForTeacher jobForTeacher : list) {
				teacherDetails.add(jobForTeacher.getTeacherId());
			}
			//////////////////////////// Add Dynamic Status ////////////////////////////////////////////////
			TeacherAssessmentStatus teacherBaseAssessmentStatus=null;
			InternalTransferCandidates internalITRA=null;
			Map<Integer, Integer> mapAssess=null;
			TeacherAssessmentStatus teacherAssessmentStatusJSI=null;
			DistrictMaster districtMasterInternal=null;
			TeacherPortfolioStatus tPortfolioStatus=null;
			Map<Integer, TeacherAssessmentStatus> mapJSI = new HashMap<Integer, TeacherAssessmentStatus>();
			List<TeacherAssessmentStatus> lstJSI= new ArrayList<TeacherAssessmentStatus>();
			
			Map<Integer,TeacherAssessmentStatus> baseTakenMap = new HashMap<Integer, TeacherAssessmentStatus>();		
			Map<Integer, Integer> isbaseComplete	=	new HashMap<Integer, Integer>();
			List<TeacherAssessmentStatus> teacherAssessmentStatus = null;
			if(teacherDetails!=null && teacherDetails.size()>0){
				teacherAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentTakenByTeachers(teacherDetails);
				for (TeacherAssessmentStatus teacherAssessmentStatus2 : teacherAssessmentStatus) {					
					baseTakenMap.put(teacherAssessmentStatus2.getTeacherDetail().getTeacherId(), teacherAssessmentStatus2);
					if(teacherAssessmentStatus2.getStatusMaster().getStatusShortName().equalsIgnoreCase("comp")){
						isbaseComplete.put(teacherAssessmentStatus2.getTeacherDetail().getTeacherId(), 1);
					}
				}
			}
			
			List<AssessmentJobRelation> assessmentJobRelations1=assessmentJobRelationDAO.findRelationByJobOrderNoDate(jobOrder);
			if(assessmentJobRelations1!=null && assessmentJobRelations1.size()>0 && teacherDetails.size()>0){
				AssessmentJobRelation assessmentJobRelation1 = assessmentJobRelations1.get(0);
				lstJSI = teacherAssessmentStatusDAO.findAssessmentTakenByTeacherList(teacherDetails,assessmentJobRelation1.getAssessmentId());
			}
					
			for(TeacherAssessmentStatus ts : lstJSI){
				mapJSI.put(ts.getTeacherDetail().getTeacherId(), ts);
			}
			Map<Integer, InternalTransferCandidates> mapITRA = new HashMap<Integer, InternalTransferCandidates>();
			List<InternalTransferCandidates> lstITRA=internalTransferCandidatesDAO.getInternalCandidateByTeachers(teacherDetails,jobOrder.getDistrictMaster());
			for(InternalTransferCandidates itc : lstITRA){
				mapITRA.put(itc.getTeacherDetail().getTeacherId(),itc);
			}
			List<TeacherPortfolioStatus> tPortfolioStatusList = teacherPortfolioStatusDAO.findAllTeacherPortfolioStatus(teacherDetails);
			Map<Integer, TeacherPortfolioStatus> mapPort = new HashMap<Integer, TeacherPortfolioStatus>();
			
			if(tPortfolioStatusList!=null)
			for (TeacherPortfolioStatus teacherPortfolioStatus : tPortfolioStatusList) {
				mapPort.put(teacherPortfolioStatus.getTeacherId().getTeacherId(), teacherPortfolioStatus);
			}
			List <StatusMaster> statusMasterList=WorkThreadServlet.statusMasters;
			Map<String, StatusMaster> mapStatus = new HashMap<String, StatusMaster>();
			for (StatusMaster statusMasterObj : statusMasterList) {
				mapStatus.put(statusMasterObj.getStatusShortName(),statusMasterObj);
			}
			SessionFactory sessionFactory=secondaryStatusDAO.getSessionFactory();
			StatelessSession statelesSsession = sessionFactory.openStatelessSession();
			Transaction txOpen =statelesSsession.beginTransaction();
			//////////////////////////// End Dynamic Status ///////////////////////////////////////
			for(JobForTeacher jft:list){
				System.out.println("TeacherId : "+jft.getTeacherId().getTeacherId());
				teacherBaseAssessmentStatus = baseTakenMap.get(jft.getTeacherId().getTeacherId());				
				teacherAssessmentStatusJSI =mapJSI.get(jft.getTeacherId().getTeacherId());
				internalITRA=mapITRA.get(jft.getTeacherId().getTeacherId());
				districtMasterInternal=null;
				if(internalITRA!=null){
					districtMasterInternal=internalITRA.getDistrictMaster();
				}
				tPortfolioStatus=mapPort.get(jft.getTeacherId().getTeacherId());
                StatusMaster statusMasterMain=dashboardAjax.findByTeacherIdJobStausForLoop(internalITRA,jft,mapAssess,teacherAssessmentStatusJSI,mapJSI,districtMasterInternal,tPortfolioStatus,mapStatus,teacherBaseAssessmentStatus); //assessmentDetailDAO.findByTeacherIdJobStaus(jft,flagList);
                System.out.println("statusMasterMain=="+statusMasterMain.getStatus());
				if(jft.getStatus()!=null && jft.getStatus().getStatusId().equals(3) && statusMasterMain!=null && statusMasterMain.getStatus().equalsIgnoreCase("Completed")){
					StatusMaster statusMaster=new StatusMaster();
					statusMaster.setStatusId(4);
					jft.setStatus(statusMaster);
					if(jft.getStatusMaster()!=null)
						jft.setStatusMaster(statusMaster);
					statelesSsession.update(jft);
				}
			}
			txOpen.commit();
			statelesSsession.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
	}



//shadab
//for schooljoborder
public String displayRecordsByEntityTypeNewES(boolean resultFlag,int JobOrderType,int districtOrSchoolId,int 
		schoolId,/*String subjectId,String certifications,*/String jobOrderIds,String status,String noOfRow,
		String pageNo,String sortOrder,String sortOrderType,String  disJobReqNo,String geoZonId,String jobCategoryIds,String searchTerm,String jobApplicationStatus)
{
	System.out.println("**************displayRecordsByEntityTypeNewES***********************");
	/* ========  For Session time Out Error =========*/
	
	int jobApplicationStatusId=0;
	if(jobApplicationStatus!=null && jobApplicationStatus!="" &&!jobApplicationStatus.equals(""))
		jobApplicationStatusId =  Integer.parseInt(jobApplicationStatus);
	
	JSONObject jsonObject=null;
	int	geoZoneId=0;
	if(geoZonId!=null && geoZonId!="" &&!geoZonId.equals(""))
	  geoZoneId =  Integer.parseInt(geoZonId);
	
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	}
	StringBuffer tmRecords =	new StringBuffer();
	try{
		UserMaster userMaster = null;
		Integer entityID=null;
		DistrictMaster districtMaster=null;
		SchoolMaster schoolMaster=null;
		SubjectMaster subjectMaster	=	null;
		int roleId=0;
		if (session == null || session.getAttribute("userMaster") == null) {
			return "false";
		}else{
			userMaster=(UserMaster)session.getAttribute("userMaster");

			if(userMaster.getSchoolId()!=null)
				schoolMaster =userMaster.getSchoolId();
			

			if(userMaster.getDistrictId()!=null){
				districtMaster=userMaster.getDistrictId();
			}

			entityID=userMaster.getEntityType();
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
		}
		String roleAccess=null;
		try{
			if(JobOrderType==3){
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,25,"schooljoborders.do",0);
			}else{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,24,"managejoborders.do",0);
			}
		}catch(Exception e){
			e.printStackTrace();
		}

		//-- get no of record in grid,
		//-- set start and end position

		int noOfRowInPage = Integer.parseInt(noOfRow);
		int pgNo = Integer.parseInt(pageNo);
		int start =((pgNo-1)*noOfRowInPage);
		int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
		int totalRecord = 0;
		//------------------------------------

		/** set default sorting fieldName **/
		String sortOrderFieldName="jobId";
		String sortOrderNoField="jobId";

		boolean deafultFlag=false;
		/**Start set dynamic sorting fieldName **/
		Order  sortOrderStrVal=null;
		if(sortOrder!=null){
			if(!sortOrder.equals("") && !sortOrder.equals(null) && !sortOrder.equals("ofApplicant") && !sortOrder.equals("ofHire")){
				sortOrderFieldName=sortOrder;
				sortOrderNoField=sortOrder;
				deafultFlag=false;
			}else{
				deafultFlag=true;
			}
			if(sortOrder.equals("ofApplicant")){
				sortOrderNoField="ofApplicant";
			}
			if(sortOrder.equals("ofHire")){
				sortOrderNoField="ofHire";
			}
		}
		String sortOrderTypeVal="0";
		sortOrderStrVal=Order.asc(sortOrderFieldName);
		if(sortOrderType!=null){
			if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
				if(sortOrderType.equals("1")){
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}
		}
		if(sortOrderType.equals("")){
			sortOrderStrVal=Order.desc(sortOrderFieldName);
		}

		/**End ------------------------------------**/
		List<JobOrder> lstJobOrder	  =	 new ArrayList<JobOrder>();
		boolean jobOrderIdFlag=false;
		boolean statusFlag=false;

		Criterion criterionStatus=null;
		Criterion criterionJobFilter=null;
		if(!(jobOrderIds.equals("0") || jobOrderIds.equalsIgnoreCase("0"))){
			String [] jobsArr =  jobOrderIds.split(",");
			List<Integer> jobs = new ArrayList<Integer>();
			for(int i=0;i<jobsArr.length;i++)
			{	
				String jobId = jobsArr[i].trim();
				try{if(!jobId.equals(""))
					jobs.add(Integer.parseInt(jobId));
				}catch(Exception e){}
			}
			if(jobs.size()>0)
			{
				jobOrderIdFlag=true;
				criterionJobFilter	= Restrictions.in("jobId",jobs);
			}
		}
		/*if(jobOrderId!=0){
			jobOrderIdFlag=true;
			criterionJobFilter	= Restrictions.eq("jobId",jobOrderId);
		}*/
		if(status!=null){
			if(!status.equalsIgnoreCase("") && !status.equalsIgnoreCase(null)){
				statusFlag=true;
				criterionStatus=Restrictions.eq("status",status);
			}
		}else{
			statusFlag=false;
		}

		/* ============== Gagan : For Adding Subject Filter ===============*/

		/*Criterion criterionSubject=null;
		if(!subjectId.equalsIgnoreCase(""))
		{
			subjectMaster=	subjectMasterDAO.findById(Integer.parseInt(subjectId), false, false);
			criterionSubject=Restrictions.eq("subjectMaster",subjectMaster);
		}*/
		//String windowFunc="if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
		int noOfRecordCheck = 0;
		int totalRecords=0;
		/*************Start *************/
		List<JobOrder> filterJobOrderList = new ArrayList<JobOrder>();

		boolean jobFlag=false;
		
		/*List<JobOrder> certJobOrderList	  =	 new ArrayList<JobOrder>();
		boolean certTypeFlag=false;
		if(certifications!=null && !certifications.equals("") && !certifications.equals("0")){
			certTypeFlag=true;
			certJobOrderList=jobCertificationDAO.findCertificationByJob(certifications);
		}
		if(certTypeFlag && certJobOrderList.size()>0){
			if(jobFlag){
				filterJobOrderList.retainAll(certJobOrderList);
			}else{
				filterJobOrderList.addAll(certJobOrderList);
			}
		}
		if(certTypeFlag){
			jobFlag=true;
		}*/
		
		/*List<JobOrder> subjectJList	  =	 new ArrayList<JobOrder>();
		boolean subjectFlag=false;
		if(subjectId!=null && !subjectId.equals("") && !subjectId.equals("0")){
			SubjectMaster sMaster=null;
			if(subjectId!=null){
				sMaster=subjectMasterDAO.findById(Integer.parseInt(subjectId),false,false);
			}
			subjectFlag=true;
			subjectJList=jobOrderDAO.findJobBySubject(sMaster);
		}
		if(subjectFlag && subjectJList.size()>0){
			if(jobFlag){
				filterJobOrderList.retainAll(subjectJList);
			}else{
				filterJobOrderList.addAll(subjectJList);
			}
		}
		if(subjectFlag){
			jobFlag=true;
		}*/

		DistrictMaster  distMaster=null;
		List<JobOrder> districtJList	  =	 new ArrayList<JobOrder>();
		boolean districtFlag=false;
		if(districtOrSchoolId!=0){
			if(districtOrSchoolId!=0){
				distMaster=districtMasterDAO.findById(districtOrSchoolId,false,false);
			}
			districtFlag=true;
			districtJList=jobOrderDAO.findJobByDistrict(distMaster);			
		}
		if(districtFlag && districtJList.size()>0){
			if(jobFlag){
				filterJobOrderList.retainAll(districtJList);
			}else{
				filterJobOrderList.addAll(districtJList);
			}
		}
		if(districtFlag){
			jobFlag=true;
		}

		System.out.println(" >>>>>>>>>>>>>>>>>>>>>>>111111> jobCategoryIds :::::::: "+jobCategoryIds);
		
		/*// Job category
		List<JobOrder> listjJobOrders = new ArrayList<JobOrder>();
		boolean jobCateFlag=false;
		if(jobCategoryIds!=null && !jobCategoryIds.equals("") && !jobCategoryIds.equals("0"))
		{
			List<Integer> jobcatIds = new ArrayList<Integer>();
			
			String jobCategoryIdStr[] =jobCategoryIds.split(",");
		  if(!ArrayUtils.contains(jobCategoryIdStr, "0"))
		  {		  
			for(String str : jobCategoryIdStr){
			 if(!str.equals(""))
				 jobcatIds.add(Integer.parseInt(str));
			 } 
			 if(jobcatIds.size()>0){
				 listjJobOrders = jobOrderDAO.findByJobCategery(jobcatIds,districtOrSchoolId);
				 jobCateFlag=true;
			 }
			 if(listjJobOrders!=null && listjJobOrders.size()>0){
					if(jobCateFlag){
						filterJobOrderList.retainAll(listjJobOrders);
					}else{
						filterJobOrderList.addAll(listjJobOrders);
					}
				 }
			 }
		  }

		if(jobCateFlag){
			jobFlag=true;
		}*/
		
		
		Criterion criterionJobOrderType = Restrictions.eq("createdForEntity",JobOrderType);
		
		List<JobOrder> schoolJList	  =	 new ArrayList<JobOrder>();
		boolean schoolFlag=false;
		int searchschoolflag=2;
		if(schoolId!=0){
			SchoolMaster  sclMaster=null;
			if(districtOrSchoolId!=0){
				sclMaster=schoolMasterDAO.findById(Long.parseLong(schoolId+""),false,false);
			}
			schoolFlag=true;
			searchschoolflag=1;
			if(distMaster!=null)
				schoolJList=schoolInJobOrderDAO.findJobListBySchool(sclMaster,distMaster,JobOrderType,entityID,searchschoolflag,userMaster);
		}
		else
		{// Only for SA for Nobletype district
			if(userMaster.getEntityType()==3 && JobOrderType==2 && userMaster.getDistrictId()!=null && distMaster.getWritePrivilegeToSchool().equals(true))
			{
				searchschoolflag=0; // below [ schoolMaster ] is sending from school id by getting  session 
				schoolJList=schoolInJobOrderDAO.findJobListBySchool(schoolMaster,distMaster,JobOrderType,entityID,searchschoolflag,userMaster);
				schoolFlag=true;
			}
		}
		if(schoolFlag && schoolJList.size()>0){
			if(jobFlag){
				filterJobOrderList.retainAll(schoolJList);
			}else{
				filterJobOrderList.addAll(schoolJList);
			}
		}else if(schoolFlag && schoolJList.size()==0){
			filterJobOrderList = new ArrayList<JobOrder>();
		}

		if(schoolFlag){
			jobFlag=true;
		}
		
		/**********rajendra: search by jobrequisition no**********************/
		boolean jobReqnoFlag=false;
		List<JobOrder> jobOrderOfReqNoList	  =	 new ArrayList<JobOrder>();
		if(disJobReqNo!=null && !disJobReqNo.equals("") && !disJobReqNo.equals("0")){
			jobReqnoFlag=true;
			List<DistrictRequisitionNumbers> districtRequisitionNumbers=null;
			Criterion crDrq=Restrictions.eq("requisitionNumber", disJobReqNo);
			Criterion crDSTrq=null;
			if(districtOrSchoolId!=0){
				crDSTrq=Restrictions.eq("districtMaster", distMaster);
				districtRequisitionNumbers=districtRequisitionNumbersDAO.findByCriteria(crDSTrq,crDrq);
			}else{
				districtRequisitionNumbers=districtRequisitionNumbersDAO.findByCriteria(crDrq);
			}
			if(districtRequisitionNumbers!=null && districtRequisitionNumbers.size()>0){
				
				Criterion crJrq=Restrictions.in("districtRequisitionNumbers", districtRequisitionNumbers);
				List<JobRequisitionNumbers> jobRequisitionNumbers=jobRequisitionNumbersDAO.findByCriteria(crJrq);
				for(JobRequisitionNumbers jqr:jobRequisitionNumbers){
					jobOrderOfReqNoList.add(jqr.getJobOrder());
				}
			}
		}
		
		if(jobReqnoFlag && jobOrderOfReqNoList.size()>0){
			if(jobFlag){
				filterJobOrderList.retainAll(jobOrderOfReqNoList);
			}else{
				filterJobOrderList.addAll(jobOrderOfReqNoList);
			}
		}else if(jobReqnoFlag && jobOrderOfReqNoList.size()==0){
			
			filterJobOrderList = new ArrayList<JobOrder>();
		}
		
		if(jobReqnoFlag){
			jobFlag=true;
		}
		/**********rajendra: search by jobrequisition no**********************/

		// =============  Deepak filtering basis on job Status  added by 27-05-2015
		if(jobApplicationStatusId>0 && distMaster!=null && distMaster.getDistrictId().toString().equalsIgnoreCase("804800"))
		{
			boolean jobApplicationStatusFlag = false;
				List<JobOrder> jobApplicationStatusList = new ArrayList<JobOrder>();
					if (disJobReqNo != null && jobApplicationStatusId > 0) {
						jobApplicationStatusFlag = true;
						jobApplicationStatusList = jobOrderDAO.getJobApplicationStatusRole(jobApplicationStatusId);
						System.out.println("jobApplicationStatusList========"+ jobApplicationStatusList.size());
	
					}
					if (jobApplicationStatusFlag
							&& jobApplicationStatusList.size() > 0) {
						if (jobFlag == true) {
							filterJobOrderList.retainAll(jobApplicationStatusList);
						} else {
							filterJobOrderList.addAll(jobApplicationStatusList);
						}
					} else {
						filterJobOrderList = new ArrayList<JobOrder>();
					}
			if (jobApplicationStatusFlag) {
				jobFlag = true;
			}
		}
		
		
		 //     Mukesh filtering on the basis of zone
			List<JobOrder> listjobOrdersgeoZone =new ArrayList<JobOrder>();
		
		  if(geoZoneId>0)
			{
				GeoZoneMaster geoZoneMaster=geoZoneMasterDAO.findById(geoZoneId, false, false);
				if(userMaster.getRoleId().getRoleId().equals(3))
					listjobOrdersgeoZone = jobOrderDAO.getJobOrderBygeoZoneRole(geoZoneMaster,userMaster);
				else
					listjobOrdersgeoZone = jobOrderDAO.getJobOrderBygeoZone(geoZoneMaster);
			}
		 
		  if(listjobOrdersgeoZone!=null && listjobOrdersgeoZone.size()>0)
		  {
			  if(jobFlag==true){
				  filterJobOrderList.retainAll(listjobOrdersgeoZone);
				}else{
					filterJobOrderList.addAll(listjobOrdersgeoZone);
				}
		  }	
		  if(listjobOrdersgeoZone.size()==0 && geoZoneId==-1 ||geoZoneId==0)
		  {
			  filterJobOrderList.addAll(listjobOrdersgeoZone);
		  
		  }
		  else if(listjobOrdersgeoZone.size()==0 &&geoZoneId!=-1)
		  {
			  filterJobOrderList.retainAll(listjobOrdersgeoZone);
		  }
		  
		  
		  
		// Job category
			List<JobOrder> listjJobOrders = new ArrayList<JobOrder>();
			boolean jobCateFlag=false;
			if(jobCategoryIds!=null && !jobCategoryIds.equals("") && !jobCategoryIds.equals("0"))
			{
				List<Integer> jobcatIds = new ArrayList<Integer>();
				
				String jobCategoryIdStr[] =jobCategoryIds.split(",");
			  if(!ArrayUtils.contains(jobCategoryIdStr, "0"))
			  {		  
				for(String str : jobCategoryIdStr){
				 if(!str.equals(""))
					 jobcatIds.add(Integer.parseInt(str));
				 } 
				 if(jobcatIds.size()>0){
					 listjJobOrders = jobOrderDAO.findJobByJobCategery(jobcatIds,districtOrSchoolId);
					 System.out.println("  job list by jobcategory :: "+listjJobOrders.size());
					 jobCateFlag=true;
				 }
				 if(jobCateFlag){
					filterJobOrderList.retainAll(listjJobOrders);
				 }else{
					filterJobOrderList.addAll(listjJobOrders);
				 }
				}
			  }

			if(jobCateFlag){
				jobFlag=true;
			}
		  
			
			
	      if(jobFlag){
			if(filterJobOrderList.size()>0){
				List jobIds = new ArrayList();
				String esJobIds="";
				for(int e=0; e<filterJobOrderList.size(); e++) {
					jobIds.add(filterJobOrderList.get(e).getJobId());
					esJobIds+=filterJobOrderList.get(e).getJobId()+" ";
				}
				Criterion criterionJobList = Restrictions.in("jobId",jobIds);

				Criterion criterionMix=criterionJobOrderType;
				if(statusFlag && jobOrderIdFlag){
					criterionMix=Restrictions.and(criterionStatus,criterionJobFilter);
				}else if(statusFlag){
					criterionMix=criterionStatus;
				}else if(jobOrderIdFlag){
					criterionMix=criterionJobFilter;
				}
				
				Criterion crt[]={criterionJobList,criterionJobOrderType,criterionMix};
				Order poolord=Order.desc("isPoolJob");
				Order orders[]={poolord,sortOrderStrVal};
				
				if(userMaster.getEntityType()!=1 && userMaster.getDistrictId().getDistrictId().equals(1200390))
				{
					//lstJobOrder=jobOrderDAO.getJobOrderByPool(crt, orders, start,noOfRowInPage);
					jsonObject=new ElasticSearchService().esForSchoolJobOrder(status, jobOrderIds, esJobIds, start, noOfRowInPage, sortOrderFieldName, sortOrderTypeVal, searchTerm.replaceAll("\"", ""),JobOrderType);
				}
				else
				{
					
					//lstJobOrder=jobOrderDAO.findWithLimit(sortOrderStrVal,start,noOfRowInPage,criterionJobList,criterionJobOrderType,criterionMix);
					jsonObject=new ElasticSearchService().esForSchoolJobOrder(status, jobOrderIds, esJobIds, start, noOfRowInPage, sortOrderFieldName, sortOrderTypeVal, searchTerm.replaceAll("\"", ""),JobOrderType);
				}
				//totalRecords=jobOrderDAO.getRowCountWithSort(sortOrderStrVal,criterionJobList,criterionJobOrderType,criterionMix);
				//System.out.println(((JSONObject)jsonObject.get("hits")).getInt("total"));
				totalRecords=((JSONObject)jsonObject.get("hits")).getInt("total");
			}
		}else{
			Criterion criterionMix=criterionJobOrderType;
			if(statusFlag && jobOrderIdFlag){
				criterionMix=Restrictions.and(criterionStatus,criterionJobFilter);
			}else if(statusFlag){
				criterionMix=criterionStatus;
			}else if(jobOrderIdFlag){
				criterionMix=criterionJobFilter;
			}
			
			Criterion crt[]={criterionJobOrderType,criterionMix};
			Order poolord=Order.desc("isPoolJob");
			Order orders[]={poolord,sortOrderStrVal};
			
			if(userMaster.getEntityType()!=1 && userMaster.getDistrictId().getDistrictId()==1200390)
			{
				//lstJobOrder=jobOrderDAO.getJobOrderByPool(crt, orders, start,noOfRowInPage);
				jsonObject=new ElasticSearchService().esForSchoolJobOrder(status, jobOrderIds, "", start, noOfRowInPage, sortOrderFieldName, sortOrderTypeVal, searchTerm.replaceAll("\"", ""),JobOrderType);
			}
			
			
			else
			{
				//lstJobOrder=jobOrderDAO.findWithLimit(sortOrderStrVal,start,noOfRowInPage,criterionJobOrderType,criterionMix);
				jsonObject=new ElasticSearchService().esForSchoolJobOrder(status, jobOrderIds, "", start, noOfRowInPage, sortOrderFieldName, sortOrderTypeVal, searchTerm.replaceAll("\"", ""),JobOrderType);
			}
			
			//totalRecords=jobOrderDAO.getRowCountWithSort(sortOrderStrVal,criterionJobOrderType,criterionMix);
			totalRecords=((JSONObject)jsonObject.get("hits")).getInt("total");
		}
	 
		/***************************** End Code **************************************/
	      JSONObject jsonObjectTemp=jsonObject;
	      if(jsonObjectTemp!=null)
			{
				try{
					if((JSONObject)jsonObjectTemp.get("hits")!=null){
						if(((JSONObject)jsonObjectTemp.get("hits")).get("hits")!=null){
							JSONArray hits= (JSONArray) ((JSONObject)jsonObjectTemp.get("hits")).get("hits");
							System.out.println("hits size===="+hits.size());
							for(int i=0; i<hits.size(); i++)
							{
								jsonObjectTemp=(JSONObject)hits.get(i);
								String index=(String)jsonObjectTemp.get("_index");
								String type=(String)jsonObjectTemp.get("_type");
								String id=(String)jsonObjectTemp.get("_id");
								jsonObjectTemp=(JSONObject)jsonObjectTemp.get("_source");
						    	JobOrder jobOrder=new JobOrder();
						    	jobOrder.setJobId(jsonObjectTemp.getInt("jobId"));
						    	lstJobOrder.add(jobOrder);
							}
						}
					}
				}catch(Exception e){
					e.printStackTrace();
				}	
				
			}

	      Map<Integer,String> map = new HashMap<Integer, String>();
			if(lstJobOrder.size()>0)
			 map = jobForTeacherDAO.countApplicantsByJobOrdersAllll(lstJobOrder);
									
	/*	Map<Integer,JobForTeacher> jobForTeacherList = new HashMap<Integer, JobForTeacher>();
		//Map<Integer,JobCertification> jobCertificationList = new HashMap<Integer, JobCertification>();
		//Map<Integer,JobOrder> jobOrderList =new HashMap<Integer, JobOrder>();
		List<TeacherDetail> teacherDetails = new ArrayList<TeacherDetail>();
		if(lstJobOrder.size()>0){
			jobForTeacherList = jobForTeacherDAO.findJFTApplicantbyJobOrders(lstJobOrder);
			List<Integer> joborderIds=new ArrayList<Integer>();
			for(JobOrder job: lstJobOrder){
				joborderIds.add(job
				.getJobId());
			}
			//jobOrderList = jobOrderDAO.findByAllJobOrders(joborderIds); 

			try{
				for(int t=0;t<jobForTeacherList.size();t++){
					teacherDetails.add(jobForTeacherList.get(t).getTeacherId());
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}

		//List<JobCategoryMaster> jobCategoryMasterList= jobCategoryMasterDAO.findAllJobCategoryNameByOrder();
		//List<TeacherAssessmentStatus> teacherAssessmentStatusList=teacherAssessmentStatusDAO.findByAllTeacherAssessStatusByTeacher(teacherDetails);
		List <StatusMaster> statusMasterList=WorkThreadServlet.statusMasters;
		StatusMaster statusMaster =findStatusByShortName(statusMasterList,"hide");
		StatusMaster statusMasterHird = findStatusByShortName(statusMasterList,"hird");

		List<TeacherStatusHistoryForJob> teacherSHFJobList=teacherStatusHistoryForJobDAO.findStatusAndSecStatusByTeacherList(teacherDetails);
		Map<String,TeacherStatusHistoryForJob> mapTSHFJob = new HashMap<String, TeacherStatusHistoryForJob>();
		for(TeacherStatusHistoryForJob forJob:teacherSHFJobList){
			if(forJob.getStatusMaster()!=null){
				mapTSHFJob.put(forJob.getJobOrder().getJobId()+"##"+forJob.getTeacherDetail().getTeacherId()+"##"+forJob.getStatusMaster().getStatusId()+"##0",forJob);
			}else{
				if(forJob.getSecondaryStatus()!=null){
					if(forJob.getSecondaryStatus().getSecondaryStatus_copy()!=null){
						mapTSHFJob.put(forJob.getJobOrder().getJobId()+"##"+forJob.getTeacherDetail().getTeacherId()+"##"+forJob.getSecondaryStatus().getSecondaryStatus_copy().getSecondaryStatusId()+"##1",forJob);
					}else{
						mapTSHFJob.put(forJob.getJobOrder().getJobId()+"##"+forJob.getTeacherDetail().getTeacherId()+"##"+forJob.getSecondaryStatus().getSecondaryStatusId()+"##1",forJob);
					}
				}
			}
		}*/
		totalRecord =totalRecords;

		if(totalRecord<end)
			end=totalRecord;
		
		//configurable coloumn
	    Map<String,Boolean> displayCNFGColumn=null;
	    if(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==2){
	    	displayCNFGColumn=getCNFGColumn(districtMaster,true);
	    }
	    else
	    	displayCNFGColumn=getCNFGColumn(districtMaster,false);
		
		tmRecords.append("<table  id='tblGrid' width='100%' border='0'>");
		tmRecords.append("<thead class='bg'>");
		tmRecords.append("<tr>");
		String responseText="";
		if(deafultFlag){
			sortOrderTypeVal="1";
		}
		
//Start Sandeep 03-09-15		
		
		  Boolean hrIntegratedFlag = false;
	      if(userMaster.getDistrictId() != null)
	    	  hrIntegratedFlag = userMaster.getDistrictId().getHrIntegrated();
		
//end	
	    if(userMaster.getDistrictId()==null || displayCNFGColumn.get("JID")){
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblJobId", locale),sortOrderNoField,"jobId",sortOrderTypeVal,pgNo);
			tmRecords.append("<th valign='top'>"+responseText+"</th>");
	    }
		
		if(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==2)
        {           
          if(hrIntegratedFlag && userMaster.getEntityType() == 2)
          {
                responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("msgReq", locale)+"<br>"+Utility.getLocaleValuePropByKey("msgPositions", locale)+" #",sortOrderFieldName,"districtRequisitionNo",sortOrderTypeVal,pgNo);
              tmRecords.append("<th  valign='top'>"+responseText+"</th>");
          }
          else if(userMaster.getEntityType() == 1)
          {
                responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("msgReq", locale)+"<br>"+Utility.getLocaleValuePropByKey("msgPositions", locale)+" #",sortOrderFieldName,"districtRequisitionNo",sortOrderTypeVal,pgNo);
                    tmRecords.append("<th  valign='top'>"+responseText+"</th>");
          }
        }

		if(userMaster.getDistrictId()==null || displayCNFGColumn.get("TIT")){
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblTitle", locale),sortOrderFieldName,"jobTitle",sortOrderTypeVal,pgNo);
			tmRecords.append("<th  valign='top'>"+responseText+"</th>");
		}
		// zone added by mukesh
		//if(userMaster.getDistrictId()!=null && userMaster.getDistrictId().getDistrictId()==804800)
		if(userMaster.getDistrictId()==null || displayCNFGColumn.get("SCH")){
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblSchool", locale),"schoolName","schoolName",sortOrderTypeVal,pgNo);
			tmRecords.append("<th  valign='top'>"+responseText+"</th>");
		}
		//else
		if(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==2)
		{
		
		}
		else 
		{
			if(userMaster.getDistrictId()==null || displayCNFGColumn.get("ZON")){
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblZone", locale),sortOrderFieldName,"geoZoneMaster",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
			}
			
			if(userMaster.getDistrictId()==null || displayCNFGColumn.get("SUB")){
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblSub", locale),sortOrderFieldName,"subjectMaster",sortOrderTypeVal,pgNo);
				tmRecords.append("<th  valign='top'>"+responseText+"</th>");
			}
		}
		
		if(userMaster.getDistrictId()==null || displayCNFGColumn.get("STA")){
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("optStatus", locale),sortOrderFieldName,"status",sortOrderTypeVal,pgNo);
			tmRecords.append("<th valign='top'>"+responseText+"</th>");
		}

		//responseText=PaginationAndSorting.responseSortingLink("Activation Date",sortOrderFieldName,"jobStartDate",sortOrderTypeVal,pgNo);
		//tmRecords.append("<th width='14%' valign='top'>"+responseText+"</th>");
		
		if(userMaster.getDistrictId()==null || displayCNFGColumn.get("POSU")){
			responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblPostedUntil", locale),sortOrderFieldName,"jobEndDate",sortOrderTypeVal,pgNo);
			tmRecords.append("<th  valign='top'>"+responseText+"</th>");
		}

//sandeep 04-08-15
	//	tmRecords.append("<th  valign='top'>Positions</th>");
		if(userMaster.getDistrictId()==null || displayCNFGColumn.get("POS")){
			tmRecords.append("<th  valign='top'>"+Utility.getLocaleValuePropByKey("msgPositions", locale)+"</th>");
		}
				
		if(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==2)
		{
		if(userMaster.getEntityType() == 2 && hrIntegratedFlag)
		  tmRecords.append("<th  valign='top'>"+Utility.getLocaleValuePropByKey("lblHrStatus", locale)+"</th>");
		else if(userMaster.getEntityType() == 1)
			tmRecords.append("<th  valign='top'>"+Utility.getLocaleValuePropByKey("lblHrStatus", locale)+"</th>");
		}
		
		if(userMaster.getDistrictId()==null || displayCNFGColumn.get("APP")){
			tmRecords.append("<th  valign='top'>"+Utility.getLocaleValuePropByKey("lblApplicants", locale)+" <a href='#' id='iconpophover1' class='net-header-text ' rel='tooltip' data-original-title='"+Utility.getLocaleValuePropByKey("msgTotalnumberofapplicantsappliedforthisjob2", locale)+"'><span class='icon-question-sign'></span></a></th>");
		}
		
		if(userMaster.getDistrictId()==null || displayCNFGColumn.get("HPER")){
				tmRecords.append("<th  valign='top'>"+Utility.getLocaleValuePropByKey("lblHires", locale)+"</th>");
		}
		
		if(userMaster.getDistrictId()==null || displayCNFGColumn.get("CANG")){
			tmRecords.append("<th  valign='top'>"+Utility.getLocaleValuePropByKey("lblCandidateGrid", locale)+"</th>");
		}
		
		if(userMaster.getDistrictId()==null || displayCNFGColumn.get("ATT")){
			tmRecords.append("<th  valign='top'>"+Utility.getLocaleValuePropByKey("lblDistrictAttachment", locale)+"</th>");
		}
		
		if(userMaster.getDistrictId()==null || displayCNFGColumn.get("ACT")){
			tmRecords.append(" <th   valign='top'>"+Utility.getLocaleValuePropByKey("lblAct", locale)+"</th>");
		}
		

		tmRecords.append("</tr>");
		tmRecords.append("</thead>");

		if(resultFlag==false){
			totalRecord=0;
		}
		
		/*================= Checking If Record Not Found ======================*/
  
		/********** Start :: For get Number of awailable candidates ****** */
		
	/*	List <StatusMaster> statusMasterLst=WorkThreadServlet.statusMasters;
		Map<String, StatusMaster> mapStatus = new HashMap<String, StatusMaster>();
		for (StatusMaster sMaster : statusMasterLst) {
			mapStatus.put(sMaster.getStatusShortName(),sMaster);
		}
		
		List<JobOrder> jobLst= new ArrayList<JobOrder>();
		List<TeacherDetail> teacherList = new ArrayList<TeacherDetail>();
		
		//Get List of teacher and joborder
		for(int i=0;i<jobForTeacherList.size();i++){
			jobLst.add(jobForTeacherList.get(i).getJobId());
			teacherList.add(jobForTeacherList.get(i).getTeacherId());
		}
		
		TeacherAssessmentStatus teacherAssessmentStatusJSI = null;
		Map<Integer, Integer> mapAssess = new HashMap<Integer, Integer>();
		Map<Integer, TeacherAssessmentStatus> mapJSI = new HashMap<Integer, TeacherAssessmentStatus>();
		List<AssessmentJobRelation> assessmentJobRelations1=assessmentJobRelationDAO.findRelationByJobOrder(jobLst);
		List<AssessmentDetail> adList=new ArrayList<AssessmentDetail>();
		
		Map<Integer,InternalTransferCandidates> mapLstITRA = new HashMap<Integer, InternalTransferCandidates>();
		Map<Integer,TeacherPortfolioStatus> maptPortfolioStatus = new HashMap<Integer, TeacherPortfolioStatus>();
		//Map<Integer,TeacherAssessmentStatus> maptBaseAssmentStatus = new HashMap<Integer, TeacherAssessmentStatus>();
		List<TeacherAssessmentStatus> lstJSI =new ArrayList<TeacherAssessmentStatus>(); 

		mapLstITRA 				= internalTransferCandidatesDAO.getDistrictForTeacherShowSatus(teacherList);
		maptPortfolioStatus 	= teacherPortfolioStatusDAO.findPortfolioStatusByTeacherlist(teacherList);
		//maptBaseAssmentStatus 	= teacherAssessmentStatusDAO.findAssessmentStatusByTeacherListForBase(teacherList);
		
		if(assessmentJobRelations1.size()>0){
			for(AssessmentJobRelation ajr: assessmentJobRelations1){
				mapAssess.put(ajr.getJobId().getJobId(),ajr.getAssessmentId().getAssessmentId());
				adList.add(ajr.getAssessmentId());
			}
			if(adList.size()>0)
				lstJSI = teacherAssessmentStatusDAO.findJSITakenByAssessMentListAndTeacherList(teacherList,adList);
		}

		for(TeacherAssessmentStatus ts : lstJSI)
		{
			mapJSI.put(ts.getAssessmentDetail().getAssessmentId(), ts);
		}
*/
		/********** End :: For get Number of awailable candidates ****** */
		//Approval authority   
		List<UserMaster> listUserMasters =new ArrayList<UserMaster>();
		List<JobApprovalHistory> listJobApprovalHistories =new ArrayList<JobApprovalHistory>();
		Map<Integer,List<UserMaster>> mapofApprovedUsers=new HashMap<Integer, List<UserMaster>>();
		List<UserMaster> tempUserList=null;
		UserMaster user = null;
		DistrictKeyContact userKeyContact=null;
		Integer userId = userMaster.getUserId();
		List<JobRequisitionNumbers> jobRequisitionNumbers=new ArrayList<JobRequisitionNumbers>();
		
		if(userMaster.getEntityType()!=1)
		{
			List<DistrictKeyContact> districtKeyContactList = new ArrayList<DistrictKeyContact>();
			districtKeyContactList = districtKeyContactDAO.findByContactType(districtMaster,"Job Approval");
			
			for (DistrictKeyContact districtKeyContact : districtKeyContactList) {
				listUserMasters.add(districtKeyContact.getUserMaster());
				//System.out.println("districtKeyContact.getUserMaster().getUserId():: "+districtKeyContact.getUserMaster().getUserId());
				if(districtKeyContact.getUserMaster().getUserId().equals(userId))
				{
					user = districtKeyContact.getUserMaster();
					userKeyContact = districtKeyContact;
				}
			}
			
			System.out.println("userId::: "+userId+" ::::::::::::::::::::::::::::::::::::::::::: user:- "+user+" :::::::::::::::::::::::::::: userKeyContact:- "+userKeyContact);

		}
		if(user!=null)
		if(listUserMasters!=null && listUserMasters.size()>0){
			listJobApprovalHistories= jobApprovalHistoryDAO.findHistoryByUser(listUserMasters);
			if(listJobApprovalHistories!=null && listJobApprovalHistories.size()>0){
				for (JobApprovalHistory jobApprovalHistory : listJobApprovalHistories) {
					tempUserList=null;
					if(mapofApprovedUsers.get(jobApprovalHistory.getJobOrder().getJobId())!=null){
						tempUserList=mapofApprovedUsers.get(jobApprovalHistory.getJobOrder().getJobId());
						tempUserList.add(jobApprovalHistory.getUserMaster());
						//putting again in Map
						mapofApprovedUsers.put(jobApprovalHistory.getJobOrder().getJobId(), tempUserList);
						
					}else{
						tempUserList=new ArrayList<UserMaster>();
						tempUserList.add(jobApprovalHistory.getUserMaster());
						mapofApprovedUsers.put(jobApprovalHistory.getJobOrder().getJobId(), tempUserList);
					}
				}
			}
		}
		////////////////////////////////////
		boolean miamiShowFlag=true;
		try{
			if(userMaster.getDistrictId().getDistrictId()==1200390 && userMaster.getRoleId().getRoleId()==3 && userMaster.getEntityType()==3){
				miamiShowFlag=false;
			}
		}catch(Exception e){}	
		String windowFunc="if(this.href!='javascript:void(0);')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
		String fileName 	= 	"";
		String filePath		=	"";
		Integer counter = 1;
		int count=0;
		//System.out.println("lstJobOrder size=="+lstJobOrder.size());
		
		if(resultFlag){
			if(lstJobOrder!=null && lstJobOrder.size()==0)
				tmRecords.append("<tr><td colspan='10' align='center'>"+Utility.getLocaleValuePropByKey("lblNoRecord", locale)+"</td></tr>" );
			
			if(jsonObject!=null)
			{
					//System.out.println("total districtjobOrder=="+((JSONObject)jsonObject.get("hits")).get("total"));
				    totalRecord=((JSONObject)jsonObject.get("hits")).getInt("total");
				    JSONArray hits= (JSONArray) ((JSONObject)jsonObject.get("hits")).get("hits");
				    //System.out.println("hits size===="+hits.size());				
				    Map<Integer, String> jobLabelText = new HashMap<Integer, String>();
				    Map<Integer, String> jobUserLabelText = new HashMap<Integer, String>();
				    Map<String, String> hireCandidatePercentageMap = new HashMap<String, String>();
				    Map<String, String> positionMap = new HashMap<String, String>();
				    Map<String, String> positionNumber = new HashMap<String, String>();
				    
				    try
				    {
				    	List<Integer> jobIds = new ArrayList<Integer>();
				    	for(JobOrder jobOrder  : lstJobOrder)
				    		jobIds.add(jobOrder.getJobId());
				    	
				    	if(jobIds!=null && jobIds.size()>0)
				    	{
				    		//System.out.println(jobIds);
				    		Criterion criterion = Restrictions.in("jobId", jobIds);
				    		lstJobOrder = jobOrderDAO.findByCriteria(criterion);
				    	}
				    }
				    catch(Exception e)
				    {
				    	e.printStackTrace();
				    }
				    
					jobLabelText = getJobLevelText(entityID, userKeyContact, lstJobOrder );
					jobUserLabelText=getUserJobLevelText(entityID, userMaster, lstJobOrder );
					positionMap = getPositionsNumber(lstJobOrder);
					if(userMaster.getDistrictId()==null || displayCNFGColumn.get("HPER")){
						hireCandidatePercentageMap = getHireCandidatePercentage(lstJobOrder,positionMap);
					}
					System.out.println(jobLabelText);
					System.out.println(jobUserLabelText);
					System.out.println("mapofApprovedUsers:- "+mapofApprovedUsers);
					
				//for (JobOrder jobOrderDetails : lstJobOrder) {
				Map<Integer, JobOrder> jobOrderMap = new HashMap<Integer, JobOrder>();
				if(lstJobOrder!=null && lstJobOrder.size()>0)
					for(JobOrder jobOrder : lstJobOrder)
						jobOrderMap.put(jobOrder.getJobId(), jobOrder);
				
				Map<Integer, JobRequisitionNumbers> hrStatusMap = new HashMap<Integer, JobRequisitionNumbers>();
				if(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==2)
				{				
				jobRequisitionNumbers=jobRequisitionNumbersDAO.getHRStatusByJobId(lstJobOrder);
				System.out.println("jobRequisitionNumbers size:::::"+jobRequisitionNumbers.size());
				if(jobRequisitionNumbers!=null && jobRequisitionNumbers.size()>0)
				{
					for(JobRequisitionNumbers hrstatus: jobRequisitionNumbers)
					{
						System.out.println("hrstatus.getJobOrder().getJobId():::"+hrstatus.getJobOrder().getJobId());
						hrStatusMap.put(hrstatus.getJobOrder().getJobId(), hrstatus);
					
					}
				}
				}
					
				
				for(int i=0; i<hits.size(); i++)
				 {
					jsonObject=(JSONObject)hits.get(i);
					String index=(String)jsonObject.get("_index");
					String type=(String)jsonObject.get("_type");
					String id=(String)jsonObject.get("_id");
					jsonObject=(JSONObject)jsonObject.get("_source");
					JobOrder jobOrder = jobOrderMap.get(jsonObject.getInt("jobId"));
					count++;
					noOfRecordCheck++;   
					int jobForTeacherSize=0;
					int jftAvailableCandidates=0;
					int jobForHiresSize=0;
					
					//11111111111
				/*	int totalNoOfCGViewCandidate =	0;	
					totalNoOfCGViewCandidate =	getTotalCountOfCGView(mapTSHFJob,""+jobOrderDetails.getJobId(),jobCategoryMasterList,jobForTeacherList,statusMasterList,jobOrderList,teacherAssessmentStatusList,userMaster);
				*/	tmRecords.append("<tr>" );
					if(userMaster.getDistrictId()==null || displayCNFGColumn.get("JID")){
						tmRecords.append("<td nowrap >"+jsonObject.get("jobId")+"</td>");
					}
					if(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==2)						
					{
						if(userMaster.getEntityType() == 2 && hrIntegratedFlag){
							if(hrStatusMap.containsKey(jobOrder.getJobId()))
							{							
							tmRecords.append("<td>"+hrStatusMap.get(jobOrder.getJobId()).getDistrictRequisitionNumbers().getRequisitionNumber()+"</td>");
							}
							else
							{
								tmRecords.append("<td>"+Utility.getLocaleValuePropByKey("optN/A", locale)+"</td>");	
							}
						}else if(userMaster.getEntityType() == 1){
							if(hrStatusMap.containsKey(jobOrder.getJobId()))
							{							
							tmRecords.append("<td>"+hrStatusMap.get(jobOrder.getJobId()).getDistrictRequisitionNumbers().getRequisitionNumber()+"</td>");
							}
							else
							{
								tmRecords.append("<td>"+Utility.getLocaleValuePropByKey("optN/A", locale)+"</td>");	
							}
							
						}
					}
					
					if(userMaster.getDistrictId()==null || displayCNFGColumn.get("TIT")){
						if(jsonObject.getInt("isPoolJob")==2)
						{
							tmRecords.append("<td><span class='requiredlarge'>*</span>"+jsonObject.get("jobTitle")+"</td>");
						}
						else
						{
							tmRecords.append("<td>"+jsonObject.get("jobTitle")+"</td>");
						}
					}
					
					
					/*if(jobOrder.getDistrictMaster()!=null 
							&& jobOrder.getSchool()!=null && jobOrder.getSchool().size()>0)
					{
						//tmRecords.append("<td>"+jobOrder.getSchool().get(0).getSchoolName()+"</td>");
					}*/
					if(userMaster.getDistrictId()==null || displayCNFGColumn.get("SCH")){
						if(jobOrder!=null && jobOrder.getSchool()!=null && jobOrder.getSchool().size()>1)
						{
	
							String schoolListIds = jobOrder.getSchool().get(0).getSchoolId().toString();
							for(int k=1;k<jobOrder.getSchool().size();k++)
							{
								schoolListIds = schoolListIds+","+jobOrder.getSchool().get(k).getSchoolId().toString();
							}
							tmRecords.append("<td> <a href='javascript:void(0);' onclick=showSchoolList('"+schoolListIds+"')>Many Schools</a></td>");
							
						}
						else if(jobOrder!=null && jobOrder.getSchool().size()==1)
						{
							//jeffcoSpecificSchool=jobOrder.getSchool().get(0).getSchoolName()+"<br>"+jobOrder.getSchool().get(0).getAddress();
							tmRecords.append("<td>"+jobOrder.getSchool().get(0).getSchoolName()+"</td>");
						}
						else
							tmRecords.append("<td></td>");
					}
					//else
					if(userMaster.getHeadQuarterMaster()!=null)
					{
					
					}
					else
					{
					
						if(userMaster.getDistrictId()==null || displayCNFGColumn.get("ZON")){
							if(!jsonObject.getString("geoZoneName").trim().equals("")){
								if(jsonObject.getInt("geoZoneDistrictId")==4218990)
								{
									tmRecords.append("<td><a href='javascript:void(0);' onclick=\"showPhiladelphiaPdf();\" return false;>"+jsonObject.getString("geoZoneName")+"</a></td>");
								}
								else if(jsonObject.getInt("geoZoneDistrictId")==1200390){
									tmRecords.append("<td><a href='http://jobs.dadeschools.net/teachers/pdf/Geo_Zones_With_Schools.pdf' target='_blank'>"+jsonObject.getString("geoZoneName")+"</a></td>");
								}else{
									tmRecords.append("<td><a href='javascript:void(0);' onclick=\"return showZoneSchoolList("+jsonObject.getInt("geoZoneId")+",'"+jsonObject.getString("geoZoneName")+"',"+jsonObject.getString("districtId")+")\";>"+jsonObject.getString("geoZoneName")+"</a></td>");
								}
							} else {
								tmRecords.append("<td></td>");
							}
						}
						
						if(userMaster.getDistrictId()==null || displayCNFGColumn.get("SUB")){
							if(!jsonObject.getString("subjectName").trim().equals("")){
								tmRecords.append("<td>"+jsonObject.getString("subjectName")+"</td>");
							}else{
								tmRecords.append("<td></td>");
							}
						}
					}
					
					if(userMaster.getDistrictId()==null || displayCNFGColumn.get("STA")){
						tmRecords.append("<td>");
						if(jsonObject.getString("status").equalsIgnoreCase("A"))
							tmRecords.append("Active ");
						else if(jsonObject.getString("status").equalsIgnoreCase("I"))
							tmRecords.append("Inactive");
						else
							tmRecords.append("Archived");
						tmRecords.append("</td>");
					}
						
					if(userMaster.getDistrictId()==null || displayCNFGColumn.get("POSU")){
						SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
						Date endDate=sdf.parse(jsonObject.getString("jobEndDate"));
						if(Utility.convertDateAndTimeToUSformatOnlyDate(endDate).equals("Dec 25, 2099"))
							tmRecords.append("<td>Until filled</td>");
						else
							tmRecords.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(endDate)+",11:59 PM CST</td>");
					}
					
					if(userMaster.getDistrictId()==null || displayCNFGColumn.get("POS")){
						tmRecords.append("<td>"+(positionMap.get(""+jobOrder.getJobId()))+"</td>");
					}
						
					if(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==2)						
					{									
						if(userMaster.getEntityType() == 2 && hrIntegratedFlag){
							if(hrStatusMap.containsKey(jobOrder.getJobId()))
							{				
								String valueStatus="";
								String statusValue=hrStatusMap.get(jobOrder.getJobId()).getDistrictRequisitionNumbers().getPostionStatus();
								if(statusValue!=null)
								{
								if(statusValue.equals("51"))
								{
									valueStatus="Vacant";
								}else if(statusValue.equals("01"))
								{
									valueStatus="Staffed � Fully";
								}else if(statusValue.equals("02"))
								{
									valueStatus="Staffed � Partially";
								}else if(statusValue.equals("60"))
								{
									valueStatus="Frozen";
								}else if(statusValue.equals("00"))
								{
									valueStatus="Inactive Position";
								}else if(statusValue.equals("PV"))
								{
									valueStatus="Pending Vacant";
								}
								}
							   tmRecords.append("<td>"+valueStatus+"</td>");
							
							}
							else
							{
								tmRecords.append("<td>"+Utility.getLocaleValuePropByKey("optN/A", locale)+"</td>");	
							}
						}else if(userMaster.getEntityType() == 1){
							if(hrStatusMap.containsKey(jobOrder.getJobId()))
							{				
								String valueStatus="";
								String statusValue=hrStatusMap.get(jobOrder.getJobId()).getDistrictRequisitionNumbers().getPostionStatus();
								if(statusValue!=null)
								{
								if(statusValue.equals("51"))
								{
									valueStatus="Vacant";
								}else if(statusValue.equals("01"))
								{
									valueStatus="Staffed � Fully";
								}else if(statusValue.equals("02"))
								{
									valueStatus="Staffed � Partially";
								}else if(statusValue.equals("60"))
								{
									valueStatus="Frozen";
								}else if(statusValue.equals("00"))
								{
									valueStatus="Inactive Position";
								}else if(statusValue.equals("PV"))
								{
									valueStatus="Pending Vacant";
								}
								}
							   tmRecords.append("<td>"+valueStatus+"</td>");
							
							}
							else
							{
								tmRecords.append("<td>"+Utility.getLocaleValuePropByKey("optN/A", locale)+"</td>");	
							}
						}
					}
					/*try{
						jobForTeacherSize=findJFTApplicantbyORHideJobOrder(mapTSHFJob,jobForTeacherList,jobOrderDetails,statusMaster,1,userMaster);
						jftAvailableCandidates = findJFTApplicantAvailableCandidates(mapTSHFJob,jobForTeacherList,jobOrderDetails,statusMaster,1,userMaster,mapAssess,teacherAssessmentStatusJSI,mapJSI,mapStatus,maptPortfolioStatus,mapLstITRA);
						if(jobForTeacherSize!=0){
							tmRecords.append("<td>"+jobForTeacherSize+"/"+jftAvailableCandidates+"</td>");
						}else{
							tmRecords.append("<td>0/0</td>");
						}
					}catch(Exception e){
						e.printStackTrace();
						tmRecords.append("<td >0</td>");
					}
					try{
						jobForHiresSize=findJFTApplicantbyORHideJobOrder(mapTSHFJob,jobForTeacherList,jobOrderDetails,statusMasterHird,2,userMaster);
						if(jobForHiresSize!=0){
							tmRecords.append("<td>"+jobForHiresSize+"</td>");
						}else{
							tmRecords.append("<td>0</td>");
						}
					}catch(Exception e){
						e.printStackTrace();
						tmRecords.append("<td>0</td>");
					}*/
					//Total/available/hired
					
//start sandeep 04-08-15					
					
	/*				if(jobOrder != null)
					    tmRecords.append("<td>"+(jobOrder.getNoOfHires()!=null?jobOrder.getNoOfHires():"")+"</td>");
					else
						tmRecords.append("<td>0</td>");
<<<<<<< ManageJobOrdersAjax.java
//end					

=======

					

>>>>>>> 1.359
					String appliedStr = null;
					appliedStr = map.get(jsonObject.getInt("jobId"));
					
					 if(appliedStr!=null){
						     tmRecords.append("<td>"+appliedStr.split("##")[0]+"/"+appliedStr.split("##")[1]+"</td>");
						 
							 if(!appliedStr.split("##")[2].equals("0") && jobOrder!=null){
								 int hirePercentage =0;
								 try{
								 hirePercentage = (Integer.parseInt(appliedStr.split("##")[2]!=null?appliedStr.split("##")[2]:"0")/jobOrder.getNoOfHires())*100;
								 }catch(NumberFormatException e){e.printStackTrace();}
								 tmRecords.append("<td>"+hirePercentage+"%</td>");
							      
							 }else
							 tmRecords.append("<td>0%</td>");
					 }else
					 {
						 
						 tmRecords.append("<td>0/0</td>");
						 tmRecords.append("<td>0%</td>");
					 }
 */				
					 
					String appliedStr = null;
				    appliedStr = map.get(jsonObject.getInt("jobId"));
					if(userMaster.getDistrictId()==null || displayCNFGColumn.get("APP")){
					      if(appliedStr!=null){
					       tmRecords.append("<td>"+appliedStr.split("##")[0]+"/"+appliedStr.split("##")[1]+"</td>");
					       //tmRecords.append("<td >"+appliedStr.split("##")[2]+"</td>");
					      }else
					      {
					       tmRecords.append("<td>0/0</td>");
					       //tmRecords.append("<td>0</td>");
					      }
					}
					
					if(userMaster.getDistrictId()==null || displayCNFGColumn.get("HPER")){
				    	  tmRecords.append("<td>"+hireCandidatePercentageMap.get(jsonObject.get("jobId").toString())+"</td>");
					}
					
				    if(userMaster.getDistrictId()==null || displayCNFGColumn.get("CANG")){
				    	tmRecords.append("<td style='text-align:center'>&nbsp;<a data-original-title='CG View' rel='tooltip' id='cgn"+noOfRecordCheck+"' target=\"_blank\" href='candidategrid.do?jobId="+jsonObject.getInt("jobId")+"&JobOrderType="+JobOrderType+"'><span class='icon-table icon-large'></span></a></td>");
				    }
					
					
					if(userMaster.getDistrictId()==null || displayCNFGColumn.get("ATT")){
						filePath	=	jsonObject.getString("districtId")+"/DistAttachmentFile";
						fileName 	=	jsonObject.getString("districtAttachment");
						//filePath=Utility.getValueOfPropByKey("districtRootPath")+jobOrderDetails.getDistrictMaster().getDistrictId()+"/DistAttachmentFile";
						
						if(!fileName.equals("")){
							tmRecords.append("<td style='text-align:center'>");
							tmRecords.append("<a href='javascript:void(0);' rel='tooltip' data-original-title="+fileName+" class='attchToolTip"+noOfRecordCheck+"' id='refNotesfileopen"+counter+"' onclick=\"downloadAttachment('"+filePath+"','"+fileName+"');"+windowFunc+"\"><span class='icon-download-alt icon-large iconcolorBlue'></span></a>");
							tmRecords.append("</td>");
						} else {
							tmRecords.append("<td>&nbsp;</td>");
						}
					}
	
					
					boolean jeffcoSAsEditDJO=false;
					if(userMaster.getDistrictId()!=null && userMaster.getDistrictId().getDistrictId().equals(804800)){//&& userMaster.getUserId().equals(jobOrder.getCreatedBy()) && userMaster.getDistrictId().getsACreateDistJOb()!=null && userMaster.getDistrictId().getsACreateDistJOb()
						jeffcoSAsEditDJO=true;
					}
					if(userMaster.getDistrictId()==null || displayCNFGColumn.get("ACT")){
						if(entityID==3 && JobOrderType==2){
							if((roleAccess.indexOf("|2|")!=-1 && miamiShowFlag) || jeffcoSAsEditDJO){
								tmRecords.append("<td nowrap><a title="+Utility.getLocaleValuePropByKey("msgEditicon", locale)+" href='editjoborder.do?JobOrderType="+JobOrderType+"&JobId="+jsonObject.getInt("jobId")+"&districtId="+jsonObject.getString("districtId")+"' style=\"vertical-align:middle;\"><i class='fa fa-pencil-square-o fa-lg' /></a>");
								if(jeffcoSAsEditDJO){
									if(jsonObject.getString("status").equalsIgnoreCase("A"))
										tmRecords.append("&nbsp;|<a title="+Utility.getLocaleValuePropByKey("lblDeactivate", locale)+"  href='javascript:void(0);' onclick=\"return activateDeactivateJob("+entityID+","+jsonObject.getInt("jobId")+",'I')\" style=\"vertical-align:middle;\"><i class='fa fa-times fa-lg' /></a>");
									else
										tmRecords.append("&nbsp;|<a title="+Utility.getLocaleValuePropByKey("msgActivateicon", locale)+" href='javascript:void(0);' onclick=\"return activateDeactivateJob("+entityID+","+jsonObject.getInt("jobId")+",'A')\" style=\"vertical-align:middle;\"><i class='fa fa-check fa-lg' /></a>");
								}
							}else if(roleAccess.indexOf("|4|")!=-1 || miamiShowFlag==false){
								tmRecords.append("<td nowrap><a href='editjoborder.do?JobOrderType="+JobOrderType+"&JobId="+jsonObject.getInt("jobId")+"&districtId="+jsonObject.getString("districtId")+"'>View</a>");
							}
							
							String text = jobUserLabelText.get(jsonObject.getInt("jobId")); //ElasticSearchCode
							if(text!=null)
								tmRecords.append(text);
							
							if(jsonObject.getBoolean("IsInviteOnly")){
								tmRecords.append("&nbsp;|<a  href='importcandidatedetails.do?jobId="+jsonObject.getInt("jobId")+"' rel='tooltip' id='exelexport"+count+"' data-original-title='Upload Candidates against this job'><i class='icon-upload' style='font-size: 1.5em;'></i></a>");
								tmRecords.append("<script>$('#exelexport"+count+"').tooltip();</script>");
							}
							tmRecords.append(" </td>");
							
						}else{
							boolean pipeFlag=false;
							if(roleAccess.indexOf("|2|")!=-1 && miamiShowFlag && !jsonObject.getString("status").equalsIgnoreCase("R")){						      
								if(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==2 && jobOrder.getStatus().equalsIgnoreCase("I"))						
								{
									List<JobForTeacher> listJobForTeacher= new ArrayList<JobForTeacher>();
									listJobForTeacher=jobForTeacherDAO.findJFTHiresByJobOrder(jobOrder);
							     	if(appliedStr!=null && listJobForTeacher.size()>0)
							    	{									
							        	tmRecords.append("<td nowrap><a href='#' data-original-title='This job order has candidates associated and cannot be edited.' rel='tooltip' id='edit"+noOfRecordCheck+"' onclick='return false;'><i class='fa fa-pencil-square-o fa-lg iconcolorhover' /></a>");
									}
									else
									{
										tmRecords.append("<td nowrap><a title="+Utility.getLocaleValuePropByKey("msgEditicon", locale)+" href='editjoborder.do?JobOrderType="+JobOrderType+"&JobId="+jsonObject.getInt("jobId")+"&districtId="+jsonObject.getString("districtId")+"'><i class='fa fa-pencil-square-o fa-lg' /></a>");
									}
								}
								else
								{
									tmRecords.append("<td nowrap><a title="+Utility.getLocaleValuePropByKey("msgEditicon", locale)+" href='editjoborder.do?JobOrderType="+JobOrderType+"&JobId="+jsonObject.getInt("jobId")+"&districtId="+jsonObject.getString("districtId")+"'><i class='fa fa-pencil-square-o fa-lg' /></a>");	
								}
								tmRecords.append("<a title="+Utility.getLocaleValuePropByKey("msgClone", locale)+" href='clonejoborder.do?JobOrderType="+JobOrderType+"&JobId="+jsonObject.getInt("jobId")+"&districtId="+jsonObject.getString("districtId")+"'> |<span class='icon-copy fa-lg iconcolorBlue'></span></a>&nbsp;|");
								//tmRecords.append("<a href='javascript:void(0);' onclick='return showHistory("+1+")'><i class='fa fa-history fa-lg'/> |<span class='icon-copy fa-lg iconcolorBlue'></span></a>");
	/*sandeep*/					tmRecords.append("<a title='History' href='javascript:void(0);' onclick='return showEditJobHistory("+jsonObject.getInt("jobId")+")'><i class='fa fa-history fa-lg'/></a>");
								pipeFlag=true;
							}else if(roleAccess.indexOf("|4|")!=-1 || miamiShowFlag==false ){	
								if(!jsonObject.getString("status").equalsIgnoreCase("R"))
								tmRecords.append("<td nowrap><a title="+Utility.getLocaleValuePropByKey("msgEditicon", locale)+" href='editjoborder.do?JobOrderType="+JobOrderType+"&JobId="+jsonObject.getInt("jobId")+"&districtId="+jsonObject.getString("districtId")+"'><i class='fa fa-pencil-square-o fa-lg' /></a>");
								pipeFlag=true;
								
							}
							
							if(roleAccess.indexOf("|7|")!=-1 && miamiShowFlag && !jsonObject.getString("status").equalsIgnoreCase("R")){
								if(pipeFlag)tmRecords.append(" | ");
								if(jsonObject.getString("status").equalsIgnoreCase("A"))
									tmRecords.append("<a title="+Utility.getLocaleValuePropByKey("lblDeactivate", locale)+"  href='javascript:void(0);' onclick=\"return activateDeactivateJob("+entityID+","+jsonObject.getInt("jobId")+",'I')\"><i class='fa fa-times fa-lg' /></a>");
								else
									tmRecords.append("<a title="+Utility.getLocaleValuePropByKey("msgActivateicon", locale)+" href='javascript:void(0);' onclick=\"return activateDeactivateJob("+entityID+","+jsonObject.getInt("jobId")+",'A')\"><i class='fa fa-check fa-lg' /></a>");
							}else{
								if(jsonObject.getString("status").equalsIgnoreCase("R"))
									tmRecords.append("<td>");
								tmRecords.append("&nbsp;");
							}
							tmRecords.append("<br>");
							if(user!=null){
								String text = jobLabelText.get(jsonObject.getInt("jobId"));
								if(text!=null)
									tmRecords.append(text);
								else
								{
									int approvalBeforeGoLive = (jobOrder==null)? -1 : jobOrder.getApprovalBeforeGoLive();
									
									if(approvalBeforeGoLive==0){
										tmRecords.append("&nbsp;|<a  href='javascript:void(0);' onclick=\"return approveJob("+entityID+","+jsonObject.getInt("jobId")+","+userMaster.getUserId()+")\">Approval Request</a>");
									}else if(approvalBeforeGoLive==1){
										tmRecords.append("&nbsp;|Job Approved");
									}else if(approvalBeforeGoLive!=0 && approvalBeforeGoLive!=1)
								       	{
												List<UserMaster> tempUsers=null;  
												tempUsers=mapofApprovedUsers.get(jsonObject.getInt("jobId"));
												if(tempUsers==null){
													tmRecords.append("&nbsp;|<a href='javascript:void(0);' onclick=\"return approveJob("+entityID+","+jsonObject.getInt("jobId")+","+userMaster.getUserId()+")\">Approval Request</a>");
												}else if(tempUsers!=null && tempUsers.size()>0){
													boolean userexist=false;
													for(UserMaster master : tempUsers)
														if(master.getUserId().equals(user.getUserId()))
															userexist = true;
													
													if(userexist){
														tmRecords.append("&nbsp;|Pending Others");
													}else{
														tmRecords.append("&nbsp;|<a href='javascript:void(0);' onclick=\"return approveJob("+entityID+","+jsonObject.getInt("jobId")+","+userMaster.getUserId()+")\">Approval Request</a>");
													}
												}
								       	}
								}//End of else	
							}
							if(jsonObject.getBoolean("IsInviteOnly")){
								
								tmRecords.append("&nbsp;|<a  href='importcandidatedetails.do?jobId="+jsonObject.getInt("jobId")+"' rel='tooltip' id='exelexport"+count+"' data-original-title='Upload Candidates against this job'><i class='icon-upload' style='font-size: 1.5em;'></i></a>");
								tmRecords.append("<script>$('#exelexport"+count+"').tooltip();</script>");
							}
							tmRecords.append(" </td>");
						}
					}
				}
			}
		}else{
			tmRecords.append("<tr><td colspan='10'>"+Utility.getLocaleValuePropByKey("lblNoRecord", locale)+"</td></tr>" );
		}
		tmRecords.append("</table>");
		tmRecords.append(PaginationAndSorting.getPaginationStringForManageJobOrdersAjax(request,totalRecord,noOfRow, pageNo));
	}catch (Exception e) 
	{
		e.printStackTrace();
	}

	return tmRecords.toString();
}

//shadab end


		
		
		public String populateJobOrderInfo(String requisionNumber,Integer jobId) throws Exception{
		    System.out.println("****IN*******populateJobOrderInfo*****************requisionNumber : "+requisionNumber+" jobId: "+jobId);

		    WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		    }
		    String JOBCODE =null;
		    String LOCATION_ID = "";// it is Deptid
		    String JobType = ""; // it is job type
		    String JOB_FAMILY_JOB_CATG = ""; //it is job category
		    String JobTitle = "";
		    String JobDescr = "";
		    String NBR_OF_HIRES = "";
		   
		    String RegTemp = "";
		    String FullPart = "";
		    String HeadCount ="";
		    String StdHours ="";
		    String SpecialEdFlag = "";
		    String fte="",payGrade="",daysWorks="",minRtHourly="",minRtAnnual="",maxRtAnnual="";
		    
		    String RESULT = null; // in case of position does not exist
		    String DESCR = null;  // in case of position does not exist
		    
		    boolean responseFlag = true;
		    JSONObject jsonResponse = new JSONObject();
		    
		    List<DistrictSpecificJobcode> lstDistrictSpecificJobcode = null;
		    
		    JobCategoryMaster jobCategoryObj=null;
			
			Integer parentJobCat=null;
			String parentJobCatName="";
			Integer subJobCat=null;
			String subJobCatName="";
			boolean isSubJobCat=false;
			
			
		    String returnResponse = null;
		    //String url = Utility.getValueOfPropByKey("positionAPIURL")+requisionNumber;
		    String url = Utility.getValueOfPropByKey("productionUrlOrTest")+"/PSIGW/RESTListeningConnector/PSFT_HR/JC_TM_POSITIONINFO.v1/POSITION_NBR="+requisionNumber;
			try{
			URL obj = new URL(url);
			HttpsURLConnection urlConnection = (HttpsURLConnection) obj.openConnection();
			String method = "GET";
			urlConnection.setRequestMethod(method); 
			urlConnection.setRequestProperty("User-Agent", "");
			urlConnection.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
			//urlConnection.setRequestProperty("Authorization", "Basic " + "VEVBQ0hNQVQ6QzBsZFN0MG4z");
			urlConnection.setRequestProperty("Authorization", "Basic " + Utility.getValueOfPropByKey("passwordAPIURL"));
			int responseCode = urlConnection.getResponseCode();
			System.out.println("\nSending '" + method + "' request to URL : " + url); 
			System.out.println("Response Code : " + responseCode);
			System.out.println("\nSending '" + method + "' request to URL : " + url); 
			System.out.println("Response Code : " + responseCode);
			    BufferedReader in = null; 
				StringBuffer response = new StringBuffer();
				try
				{
				in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
				String inputLine;
				while ((inputLine = in.readLine()) != null)
				{
					response.append(inputLine);
				}
				in.close();
				
				}
				catch (IOException IO)
				{
					IO.printStackTrace();
				}
				System.out.println("Valid Ouput: " + response.toString());
			
			    NodeList RESPONSE_NODE_LIST =null;
			    DocumentBuilderFactory builderFactoryReq = null;
			    DocumentBuilder docBuilderReq = null;
			    org.w3c.dom.Document docReq = null;
		    
		    try {
		            builderFactoryReq = DocumentBuilderFactory.newInstance();
			     	docBuilderReq = builderFactoryReq.newDocumentBuilder();
			     	
			} catch (ParserConfigurationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
				if(response!=null){
				 try {
					System.out.println();
					docReq = docBuilderReq.parse(new InputSource(new StringReader(response.toString())));
				    docReq.getDocumentElement().normalize();
				   
				    RESPONSE_NODE_LIST = docReq.getElementsByTagName("Response");
				  
				    System.out.println("docReq  ::::: "+docReq + "RESULT : "+RESPONSE_NODE_LIST.getLength());
			    
				    for(int i = 0;i<RESPONSE_NODE_LIST.getLength();i++){
			    	Node nNode = RESPONSE_NODE_LIST.item(i);
			    	if (nNode.getNodeType() == Node.ELEMENT_NODE) {
			    		org.w3c.dom.Element eElement = (org.w3c.dom.Element) nNode;
			
			    		if(eElement.getElementsByTagName("RESULT").getLength()>0){
			    		RESULT = eElement.getElementsByTagName("RESULT").item(0).getTextContent();
			    		}
			    		
			    		if(eElement.getElementsByTagName("JobCode").getLength()>0){
			    	    JOBCODE = eElement.getElementsByTagName("JobCode").item(0).getTextContent();
			    	   
			    	   //DistrictMaster districtMaster = districtMasterDAO.findById(804800, false, false);
			    	    try{
			    	    	DistrictMaster districtMaster = new DistrictMaster();
			    	    
 					    districtMaster.setDistrictId(804800);
 					    System.out.println("districtId*******************: "+districtMaster.getDistrictId());
			    	   if(districtMaster!=null){ 
				    	   //lstDistrictSpecificJobcode = districtSpecificJobcodeDAO.findByCriteria(criterion1,criterion2,criterion3);
				    	   lstDistrictSpecificJobcode = districtSpecificJobcodeDAO.findJobCategoryByJobcode(districtMaster,JOBCODE);
				    	   System.out.println("lstDistrictSpecificJobcode*>>>>>>>>******************: "+lstDistrictSpecificJobcode);
			    	   }
			    	  }catch(Exception e){e.printStackTrace();}
			    	  }
			    		if(RESULT!=null && JOBCODE==null){
			    			responseFlag = false;
			    			
			    			if(eElement.getElementsByTagName("DESCR").getLength()>0){
				    			DESCR = eElement.getElementsByTagName("DESCR").item(0).getTextContent();
				    		}
			    			System.out.println("RESULT : "+RESULT +" JOBCODE : "+JOBCODE +" DESCR: "+DESCR);
			    		}
			    		System.out.println("RESULT : "+RESULT +" JOBCODE : "+JOBCODE +" DESCR: "+DESCR);
			    		if(eElement.getElementsByTagName("Deptid").getLength()>0){
			    		LOCATION_ID = eElement.getElementsByTagName("Deptid").item(0).getTextContent().trim();
			    		}
			    		
			    		if(eElement.getElementsByTagName("FullPart").getLength()>0){
			   	        FullPart = eElement.getElementsByTagName("FullPart").item(0).getTextContent().trim();
			    		}
			    		
			    		if(eElement.getElementsByTagName("JobType").getLength()>0){
			   	        JobType = eElement.getElementsByTagName("JobType").item(0).getTextContent().trim();
			    		}
			    		
			    		if(eElement.getElementsByTagName("StdHours").getLength()>0){
		    			StdHours = eElement.getElementsByTagName("StdHours").item(0).getTextContent().trim();
			    		}
			    		
			    		if(eElement.getElementsByTagName("SpecialEdFlag").getLength()>0){
			    			SpecialEdFlag = eElement.getElementsByTagName("SpecialEdFlag").item(0).getTextContent().trim();
				    	}
			    		
			    		if(eElement.getElementsByTagName("JobFamily").getLength()>0){
			   	        JOB_FAMILY_JOB_CATG = eElement.getElementsByTagName("JobFamily").item(0).getTextContent().trim();
			    		}
			    		
			    		if(eElement.getElementsByTagName("JobTitle").getLength()>0){
			   	        JobTitle = eElement.getElementsByTagName("JobTitle").item(0).getTextContent().trim();
			    		}
			    		
			    		if(eElement.getElementsByTagName("JobDescr").getLength()>0)
			    		{
			   	           JobDescr = eElement.getElementsByTagName("JobDescr").item(0).getTextContent().trim();
			    		
			   	           JobDescr.replace("&amp;#x13&amp;#x10;", "\n");
			    		}
			    		
			    		if(eElement.getElementsByTagName("FTE").getLength()>0){
			    			fte = eElement.getElementsByTagName("FTE").item(0).getTextContent().trim();
				    		}
			    		if(eElement.getElementsByTagName("Grade").getLength()>0){
			    			payGrade = eElement.getElementsByTagName("Grade").item(0).getTextContent().trim();
				    		}
			    		if(eElement.getElementsByTagName("StdHrsFrequency").getLength()>0){
			    			daysWorks = eElement.getElementsByTagName("StdHrsFrequency").item(0).getTextContent().trim();
				    		}
			    		if(eElement.getElementsByTagName("MinRtHourly").getLength()>0){
			    			minRtHourly = eElement.getElementsByTagName("MinRtHourly").item(0).getTextContent().trim();
				    		}
			    		if(eElement.getElementsByTagName("MinRtAnnual").getLength()>0){
			    			minRtAnnual = eElement.getElementsByTagName("MinRtAnnual").item(0).getTextContent().trim();
				    		}
			    		if(eElement.getElementsByTagName("MaxRtAnnual").getLength()>0){
			    			maxRtAnnual = eElement.getElementsByTagName("MaxRtAnnual").item(0).getTextContent().trim();
				    		}
			    		
			    		System.out.println("JOBCODE : " + JOBCODE);
			    		System.out.println("LOCATION_ID : " + LOCATION_ID);
			    		System.out.println("FullPart : " + FullPart);
			    		System.out.println("JobType : " + JobType);
			    		System.out.println("JOB_FAMILY_JOB_CATG : " + JOB_FAMILY_JOB_CATG);
			    		System.out.println("JobTitle : " + JobTitle);
			    		System.out.println("JobDescr : " + JobDescr);
			    		System.out.println("StdHours : " + StdHours);	
			    		System.out.println("SpecialEdFlag : " + SpecialEdFlag);	
			    		System.out.println("fte : " + fte);
			    		System.out.println("payGrade : " + payGrade);
			    		System.out.println("daysWorks : " + daysWorks);
			    		System.out.println("minRtHourly : " + minRtHourly);
			    		System.out.println("minRtAnnual : " + minRtAnnual);	
			    		System.out.println("maxRtAnnual : " + maxRtAnnual);	
			    		
					}
			    }
				   
				 } catch (SAXException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			    
				String resMsg[]=new String[2];
				String reqMsg="";
				String schoolName="";
				String jeffcoSchoolId ="";
				String tempId=session.getId();

				Criterion criterion2=Restrictions.eq("tempId", tempId);
					Jobrequisitionnumberstemp jrqnDlt = jobRequisitionNumbersTempDAO.findByTempId(tempId);
					if(jrqnDlt!=null){
						jobRequisitionNumbersTempDAO.makeTransient(jrqnDlt);
					}
					if(LOCATION_ID!=null && LOCATION_ID!=""){			
						resMsg=saveJobEequisitionNumbersTempOnlyJeffco(LOCATION_ID,requisionNumber);
						reqMsg =resMsg[0];
						schoolName=resMsg[1];
						jeffcoSchoolId = resMsg[2];
					}
				if(responseFlag){
				jsonResponse.put("status", true);
			    jsonResponse.put("jobCode", JOBCODE);
			    jsonResponse.put("locationID", LOCATION_ID);
			    jsonResponse.put("FullPart", FullPart);
			    jsonResponse.put("jobType", JobType);
			    jsonResponse.put("jobCategory", JOB_FAMILY_JOB_CATG);
			    jsonResponse.put("jobTitle", JobTitle);
			    jsonResponse.put("StdHours", StdHours);
			    
			    jsonResponse.put("SpecialEdFlag", SpecialEdFlag);
			    
			    jsonResponse.put("reqMsg", reqMsg);
			    jsonResponse.put("schoolName", schoolName);
			    jsonResponse.put("jeffcoSchoolId", jeffcoSchoolId);
			    
			    jsonResponse.put("fte", fte);
			    jsonResponse.put("payGrade", payGrade);
			    jsonResponse.put("daysWorks", daysWorks);
			    jsonResponse.put("minRtHourly", minRtHourly);
			    jsonResponse.put("minRtAnnual", minRtAnnual);
			    jsonResponse.put("maxRtAnnual", maxRtAnnual);
			    
			    
			  if(lstDistrictSpecificJobcode!=null && lstDistrictSpecificJobcode.size()>0){
				  
			    jobCategoryObj =  lstDistrictSpecificJobcode.get(0).getJobCategoryId();
    			if(jobCategoryObj.getParentJobCategoryId()!=null){
			    	
			    	parentJobCat = jobCategoryObj.getParentJobCategoryId().getJobCategoryId();
			    	parentJobCatName = jobCategoryObj.getParentJobCategoryId().getJobCategoryName();
			    	
			    	subJobCat = jobCategoryObj.getJobCategoryId();
			    	subJobCatName =jobCategoryObj.getJobCategoryName();
			    	
			    	isSubJobCat=true;
			    	
			    	if(parentJobCat!=null){
			    		
				    	jsonResponse.put("jobCatStatus", true);
				    	jsonResponse.put("isSubJobCat", isSubJobCat);
				    	jsonResponse.put("jobCategoryId", parentJobCat);
				    	jsonResponse.put("parentJobCatName", parentJobCatName);
			    	}
			    	
			    	if(subJobCat!=null){
			    		
					    jsonResponse.put("subJobCategoryId", subJobCat);
					    jsonResponse.put("subJobCatName",subJobCatName);
			    	}
			    }else{
			    	parentJobCat=jobCategoryObj.getJobCategoryId();
			    	parentJobCatName = jobCategoryObj.getJobCategoryName();
			    	
			    	jsonResponse.put("jobCatStatus", true);
			    	jsonResponse.put("isSubJobCat", isSubJobCat);
			    	jsonResponse.put("jobCategoryId", parentJobCat);
			    	jsonResponse.put("parentJobCatName", parentJobCatName);
			    }
			  }
			  else{
				jsonResponse.put("jobCatStatus", false);
				jsonResponse.put("isSubJobCat", isSubJobCat);
			  }
			  
			    jsonResponse.put("jobDesc", JobDescr);
		        jsonResponse.put("DESCR_4_WRONG_POSITION", DESCR);
			  
			    System.out.println("jsonResponse :  "+jsonResponse.toString());
				}else{
					jsonResponse.put("status", false);
				    jsonResponse.put("DESCR_4_WRONG_POSITION", "Position No, "+requisionNumber+", does not exist");
				    System.out.println("jsonResponse :  "+jsonResponse.toString());
				}
				}catch(Exception e){
			  e.printStackTrace();
			  }

		return jsonResponse.toString();
		}

		public String checkDifferenceBetweenStartAndEndDate(Integer jobId,Integer districtHiddenId,String jobStartDate,String jobEndDate,String jobCategoryName)throws Exception
		{
		 	System.out.println("Inside checkDifferenceBetweenStartAndEndDate Method");
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}
			 
			  String ErrorMsg = null;
			  Integer differenceDays = null;
			  
			    JobOrder  jobOrder1 = jobOrderDAO.findById(jobId, false, false);
			    JobCategoryMaster jobCategoryMaster1 = jobOrder1.getJobCategoryMaster();
				Integer minDaysJobWillDisplay = jobCategoryMaster1.getMinDaysJobWillDisplay();
			  	try{
                    if(jobOrder1.getDistrictMaster().getDistrictId().toString().equalsIgnoreCase("804800")){
                          minDaysJobWillDisplay =7;
                    }
               }catch(Exception e){e.printStackTrace();}
				System.out.println("minDaysJobWillDisplay==================="+minDaysJobWillDisplay);
			
			 try {
				 SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy",Locale.ENGLISH);
		         Date dateObj1 = sdf.parse(jobStartDate);
		         Date dateObj2 = sdf.parse(jobEndDate);
		         DecimalFormat crunchifyFormatter = new DecimalFormat("###,###");
		         long differenceDate = dateObj2.getTime() - dateObj1.getTime();
		         differenceDays = (int) (differenceDate / (24 * 60 * 60 * 1000));
		         System.out.println("difference between days: " + differenceDays);
		        
		         if(minDaysJobWillDisplay!=null)
		         if(differenceDays < minDaysJobWillDisplay){
						ErrorMsg = "Positions must be posted for "+minDaysJobWillDisplay+" days";
						return ErrorMsg;
					}
			 } catch (Exception e) {
		         e.printStackTrace();
		     }
	
			return "1";
		}
		public String[] saveJobEequisitionNumbersTempOnlyJeffco(String locId,String districtreqId)
		{
			System.out.println("T calling saveJobEequisitionNumbersTemp");
			//System.out.println(" arrReqNumAndSchoolId "+arrReqNumAndSchoolId);
			
			StringBuffer buffer=new StringBuffer();
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		    }
			
			String tempId=session.getId();
			String[] returnmsg=new String[3];
			SchoolMaster schoolMaster=null;
			DistrictRequisitionNumbers districtRequisitionNumbers=null;
			try {
				Criterion criterion = Restrictions.eq("locationCode", locId);
				DistrictMaster districtMaster = new DistrictMaster();
				districtMaster.setDistrictId(804800);
				districtRequisitionNumbers=districtRequisitionNumbersDAO.getDistrictRequisitionNumber(districtMaster,districtreqId);
				Criterion cri2 = Restrictions.eq("districtId", districtRequisitionNumbers.getDistrictMaster());
				List<SchoolMaster> schoolMaster1=schoolMasterDAO.findByCriteria(criterion,cri2);
				
				if(schoolMaster1!=null && schoolMaster1.size()>0){
					Jobrequisitionnumberstemp jrqn = new Jobrequisitionnumberstemp();
					//jrqn.setJobOrder(jobOrder);
					jrqn.setSchoolMaster(schoolMaster1.get(0));
					jrqn.setDistrictRequisitionNumbers(districtRequisitionNumbers);	
					jrqn.setStatus(0);
					jrqn.setNoOfSchoolExpHires(1);
					jrqn.setTempId(tempId);
					jobRequisitionNumbersTempDAO.makePersistent(jrqn);
					returnmsg[0]="success";
					returnmsg[1]=schoolMaster1.get(0).getSchoolName();
					returnmsg[2]=""+schoolMaster1.get(0).getSchoolId();
				}else{
					returnmsg[0]="Location Code does not exist.";
					returnmsg[1] = "";
					returnmsg[2] = "";
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return returnmsg;
		}
		
		public String populateJobDiscription(Integer descriptionId)throws Exception
		{
			    WebContext context;
				context = WebContextFactory.get();
				HttpServletRequest request = context.getHttpServletRequest();
				HttpSession session = request.getSession(false);
				if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
				{
					throw new IllegalStateException("Your session has expired!");
			    }
				 List<JobTitleWithDescription> list = new  ArrayList<JobTitleWithDescription>();
				 list = jobTitleWithDescriptionDAO.findDescriptionByTitle(descriptionId);
			     String JobDescr = "";
				JobDescr = list.get(0).getJobDescription();
			return JobDescr;
		}
		public String  getApprovalInfoDetails(String schoolId, String primaryESTechnician,String backupESTechnician, Boolean specialEdFlagVal, Integer jobId, Integer districtId)
		{
			/* ========  For Session time Out Error =========*/
			System.out.println("calling getApprovalInfoDetails::::::::::"+districtId);
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException("Your session has expired!");
			}
			StringBuffer saBuffer =	new StringBuffer();
			try{
				
				boolean showspecialEdGroup=false;
				specialEdFlagVal = (specialEdFlagVal==null)? false : specialEdFlagVal;
				
				if(specialEdFlagVal==false)
				{
					showspecialEdGroup=false;
				}
				else
				if(specialEdFlagVal==true)
				{
					//Special Ed Approval Group
					showspecialEdGroup=true;
				}
								
				
				//ram nath
					//Grant Group
					DistrictMaster districtMaster = districtMasterDAO.findByDistrictId(districtId.toString());
					//districtMaster.setDistrictId(804800);
					Map<String,Map<String,DistrictKeyContact>> dWAG_dAG_Map=new LinkedHashMap<String,Map<String,DistrictKeyContact>>();
					Map<String,DistrictWiseApprovalGroup> dWAGMap=new LinkedHashMap<String,DistrictWiseApprovalGroup>();
					Map<String,DistrictKeyContact> dAGToKeyContactMap=new LinkedHashMap<String,DistrictKeyContact>();
					Criterion cri1 = Restrictions.eq("districtMaster", districtMaster);
					Criterion cri3 = Restrictions.eq("specialEdFlag", showspecialEdGroup?"Y":"N");
					Criterion cri4 = Restrictions.eq("status", "A");
					List<DistrictSpecificApprovalFlow> districtSpecificApprovalFlowList =  districtSpecificApprovalFlowDAO.findByCriteria(cri1, cri3,cri4);
					DistrictSpecificApprovalFlow dsaf=null;
					if(districtSpecificApprovalFlowList.size()>0){
						dsaf=districtSpecificApprovalFlowList.get(0);
						//System.out.println("dsaf.getApprovalFlow()===================="+dsaf.getApprovalFlow());
						String[] groupShortName=dsaf.getApprovalFlow().trim().equals("")?null:dsaf.getApprovalFlow().trim().substring(1,(dsaf.getApprovalFlow().length())).split("#");
						if(groupShortName!=null)
						for(String groupShort:groupShortName)
							dWAGMap.put(groupShort.trim(), null);
					
						//System.out.println("groupShortName=========================="+Arrays.asList(groupShortName)+"  districtId=="+ dsaf.getDistrictMaster().getDistrictId()+" status===A");
						if(groupShortName!=null){
							Criterion cri_shortName=Restrictions.in("groupShortName", Arrays.asList(groupShortName));
							Criterion cri_district=Restrictions.eq("districtId", dsaf.getDistrictMaster());
							Criterion cri_status= Restrictions.eq("status", "A");
							List<DistrictWiseApprovalGroup> districtWiseApprovalGroupLst=districtWiseApprovalGroupDAO.findByCriteria(cri_shortName,cri_district,cri_status);
							//System.out.println("districtWiseApprovalGroupLst==================================="+districtWiseApprovalGroupLst.size());
							if(districtWiseApprovalGroupLst.size()>0){
								List<String> groupNamelst=new ArrayList<String>();
								for(DistrictWiseApprovalGroup dwAG:districtWiseApprovalGroupLst){
									//System.out.println("  ------------ "+dwAG.getGroupShortName());
									dWAGMap.put(dwAG.getGroupShortName(), dwAG);
									groupNamelst.add(dwAG.getGroupName());
								}
								if(groupNamelst.size()>0){
									//System.out.println("groupNamelst======="+groupNamelst);
									List<DistrictApprovalGroups> dAGList=districtApprovalGroupsDAO.findByCriteria(Restrictions.in("groupName",groupNamelst),Restrictions.isNull("jobCategoryId"),Restrictions.isNull("jobId"),Restrictions.eq("districtId", dsaf.getDistrictMaster()));
									//System.out.println("dAGList=============="+dAGList.size());
										if(dAGList.size()>0){
											List<Integer> keyContactId=new ArrayList<Integer>();
											Map<String,DistrictKeyContact> memberToKeyContactMap=new LinkedHashMap<String,DistrictKeyContact>();
											for(DistrictApprovalGroups districtAppGroup:dAGList){
												if(districtAppGroup.getGroupMembers()!=null && !districtAppGroup.getGroupMembers().trim().equals(""))
												for(String memberKey:districtAppGroup.getGroupMembers().split("#")){
													if(!memberKey.trim().equals(""))
														try{
														keyContactId.add(Integer.parseInt(memberKey));
														//System.out.println("keyId============="+memberKey);
														}catch(NumberFormatException e){}
												}
											}
											if(keyContactId.size()>0){
												List<DistrictKeyContact> districtKeyContactList = districtKeyContactDAO.findByCriteria(Restrictions.in("keyContactId", keyContactId));
												for(DistrictKeyContact dkc:districtKeyContactList)
													memberToKeyContactMap.put(dkc.getKeyContactId().toString(), dkc);
											}
											
											for(DistrictApprovalGroups districtAppGroup:dAGList){
												dAGToKeyContactMap=new LinkedHashMap<String,DistrictKeyContact>();
												if(districtAppGroup.getGroupMembers()!=null && !districtAppGroup.getGroupMembers().trim().equals(""))
												for(String memberKey:districtAppGroup.getGroupMembers().split("#")){
													if(!memberKey.trim().equals(""))
														try{
															dAGToKeyContactMap.put(memberKey,memberToKeyContactMap.get(memberKey));
														}catch(NumberFormatException e){}
												}
												dWAG_dAG_Map.put(districtAppGroup.getGroupName(), dAGToKeyContactMap);
											}
										}
								}
							}
						}
					
					}
										
				for(Map.Entry<String, DistrictWiseApprovalGroup> entry:dWAGMap.entrySet()){
					if(entry.getKey().equals("SA") || entry.getKey().equals("ES")){
						if(entry.getKey().equals("SA")){
							
							saBuffer.append(getSchoolTable(schoolId, jobId,dWAG_dAG_Map.get(entry.getValue().getGroupName())));
						}
						if(entry.getKey().equals("ES")){
							saBuffer.append(getES(primaryESTechnician,backupESTechnician));
						}
					}else{
						boolean flag=true;
						int i=1;
						Map<String,DistrictKeyContact> dWAG_dAG1_Map=dWAG_dAG_Map.get(entry.getValue().getGroupName());
						if(dWAG_dAG1_Map!=null)
						for(Map.Entry<String, DistrictKeyContact> entry1:dWAG_dAG1_Map.entrySet()){
							if(flag){
								saBuffer.append("<B>"+entry.getValue().getGroupName()+"</B>");
								saBuffer.append("<table id='saList' width=\"100%\" border=\"0\" class=\"table table-bordered span11 mt10\"><thead class=\"bg\"><tr><th>First Name</th><th>Last Name</th><th>Email Address</th></tr></thead><tbody>");
								flag=false;
							}
							String css="";
							if(i%2==0)
								css="";
							else
								css=" bgcolor='#F2FAEF'";
							saBuffer.append("<tr"+css+">");
							saBuffer.append("<td>"+entry1.getValue().getKeyContactFirstName()+"</td>");
							saBuffer.append("<td>"+entry1.getValue().getKeyContactLastName()+"</td>");
							saBuffer.append("<td>"+entry1.getValue().getKeyContactEmailAddress()+"</td>");
							saBuffer.append("</tr>");
							i++;
						}
						if(!flag){
							saBuffer.append("</table>");
						}
					}
				}
			}
			catch (Exception e) 
			{
				e.printStackTrace();
			}

			return saBuffer.toString();
		}	
		
		private String getSchoolTable(String schoolId, Integer jobId, Map<String,DistrictKeyContact> keyContactSAs){
			StringBuffer saBuffer=new StringBuffer();
			if(schoolId==null || schoolId.equals(""))
			{
			JobOrder jobOrder = new JobOrder();
			jobOrder.setJobId(jobId);
			List<SchoolInJobOrder> schoolInJobOrders = schoolInJobOrderDAO.findJobOrder(jobOrder);
			schoolId="";
			for(SchoolInJobOrder order : schoolInJobOrders)
				schoolId = schoolId+","+order.getSchoolId().getSchoolId();
			}			
				
			if(schoolId!=null && !schoolId.equals(""))
			{
				System.out.println("schoolId:- "+schoolId);
				List<Long> schoolIdList = new ArrayList<Long>();
				for(String id : schoolId.split(","))
					try{schoolIdList.add(Long.parseLong(id));}catch(Exception e){}
				
				if(schoolIdList!=null && schoolIdList.size()>0)
				{
					Criterion cri1 = Restrictions.in("schoolId", schoolIdList);
					Criterion cri2 = Restrictions.eq("status", "A");
					
					List<SchoolMaster> schoolMasterList = schoolMasterDAO.findByCriteria(cri1,cri2);
					
					Criterion criterion1 = Restrictions.eq("entityType", new Integer(3));
					Criterion criterion_role = Restrictions.eq("roleId", roleMasterDAO.findById(3,false,false));
					Criterion criterion2 = Restrictions.in("schoolId", schoolMasterList);
					Criterion criterion4 = Restrictions.eq("status","A");
					
					List<UserMaster> userMaster = userMasterDAO.findByCriteria(criterion1,criterion2,criterion4,criterion_role);
					
					
					System.out.println("userMaster:- "+userMaster);
					saBuffer.append("<B>School Admins</B>");
					saBuffer.append("<table id='saList' width=\"100%\" border=\"0\" class=\"table table-bordered span11 mt10\"><thead class=\"bg\"><tr><th>First Name</th><th>Last Name</th><th>Email Address</th></tr></thead><tbody>");
					
					int i=1;
					boolean flagUserSAs=true;
					boolean flagKeyContactSAs=true;
					if(userMaster!=null && userMaster.size()>0)
					{
						for(UserMaster master : userMaster)
						{
							String css="";
							if(i%2==0)
								css="";
							else
								css=" bgcolor='#F2FAEF'";
							saBuffer.append("<tr"+css+">");
							saBuffer.append("<td>"+master.getFirstName()+"</td>");
							saBuffer.append("<td>"+master.getLastName()+"</td>");
							saBuffer.append("<td>"+master.getEmailAddress()+"</td>");
							saBuffer.append("</tr>");
							
							i++;
							flagUserSAs=false;
						}
					}
					if(keyContactSAs!=null)
					for(Map.Entry<String,DistrictKeyContact> entry:keyContactSAs.entrySet()){
						if(entry.getValue()!=null){
							String css="";
							if(i%2==0)
								css="";
							else
								css=" bgcolor='#F2FAEF'";
							saBuffer.append("<tr"+css+">");
							saBuffer.append("<td>"+entry.getValue().getKeyContactFirstName()+"</td>");
							saBuffer.append("<td>"+entry.getValue().getKeyContactLastName()+"</td>");
							saBuffer.append("<td>"+entry.getValue().getKeyContactEmailAddress()+"</td>");
							saBuffer.append("</tr>");
							
							i++;
						flagKeyContactSAs=false;
						}
					}
						if(flagUserSAs && flagKeyContactSAs)
						saBuffer.append("<td bgcolor='#F2FAEF' colspan='3'>No School Admin Found.</td>");
					saBuffer.append("</table>");
				}
			}
			return saBuffer.toString();
		}
		
		private String getES(String primaryESTechnician, String backupESTechnician){
			StringBuffer saBuffer=new StringBuffer();
			if(primaryESTechnician!=null && !primaryESTechnician.equals("") && backupESTechnician!=null && !backupESTechnician.equals(""))
			{
				try
				{
					System.out.println("primaryESTechnician:- "+primaryESTechnician+", backupESTechnician:- "+backupESTechnician);
					Integer pid= Integer.parseInt(primaryESTechnician);
					
					EmploymentServicesTechnician employmentServicesTechnician = employmentServicesTechnicianDAO.findById(pid, false, false);
					if(employmentServicesTechnician!=null)
					{
						saBuffer.append("<B>ES Tech</B>");
						saBuffer.append("<table id='saPList' width=\"100%\" border=\"0\" class=\"table table-bordered span11 mt10\"><thead class=\"bg\"><tr><th>Primary Employment Services Technician</th><th>Email Address</th> </tr></thead><tbody>");
						saBuffer.append("<tr bgcolor='#F2FAEF'><td>"+employmentServicesTechnician.getPrimaryESTech()+" </td><td>"+(employmentServicesTechnician.getPrimaryEmailAddress()!=null?employmentServicesTechnician.getPrimaryEmailAddress():"")+" </td> </tr>");
						saBuffer.append("</table>");
						
						saBuffer.append("<B>Backup ES Tech</B>");
						saBuffer.append("<table id='saBList' width=\"100%\" border=\"0\" class=\"table table-bordered span11 mt10\"><thead class=\"bg\"><tr><th>Backup Employment Services Technician</th><th>Email Address</th></tr></thead><tbody>");
						saBuffer.append("<tr bgcolor='#F2FAEF'><td>"+employmentServicesTechnician.getBackupESTech()+" </td><td>"+(employmentServicesTechnician.getBackupEmailAddress()!=null?employmentServicesTechnician.getBackupEmailAddress():"")+" </td> </tr>");
						saBuffer.append("</table>");
					}
					else
						saBuffer.append("<td colspan='3'>No Record Found.</td>");
				}
				catch(Exception e){e.printStackTrace();}
			}
			return saBuffer.toString();
		}
		
		public String  validateApprovalGroupDetails(String schoolId, String specialEdFlagVal)
		{
			/* ========  For Session time Out Error =========*/
			System.out.println("calling validateApprovalGroupDetails::::::::::");
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException("Your session has expired!");
			}
			UserMaster userMaster = (UserMaster)session.getAttribute("userMaster");
			
			String groupMemberNotExist="";
			String groupNotExist="";
			String schoolNotExist="";
			
			try{
				
				String jeffcoSpecialEdFlag = ( specialEdFlagVal==null || specialEdFlagVal.equals("") )? "N" : specialEdFlagVal;
				System.out.println("jeffcoSpecialEdFlag:- "+jeffcoSpecialEdFlag);
				
				DistrictSpecificApprovalFlow districtSpecificApprovalFlow = districtSpecificApprovalFlowDAO.findApprovalFlowByFlag(jeffcoSpecialEdFlag,userMaster.getDistrictId());
				System.out.println("districtSpecificApprovalFlow:- "+districtSpecificApprovalFlow.getApprovalFlow());
				
				Map<String, String> groupShortNameWithGroupName = districtWiseApprovalGroupDAO.mapGroupShortNameWithGroupName(districtSpecificApprovalFlow.getApprovalFlow(),userMaster.getDistrictId());
				System.out.println("groupShortNameWithGroupName:- "+groupShortNameWithGroupName.size());
				
				List<DistrictApprovalGroups> districtApprovalGroups = districtSpecificApprovalFlowDAO.findDistrictApprovalGroupByApprovalFlow(districtSpecificApprovalFlow.getApprovalFlow(), userMaster.getDistrictId());
				System.out.println("districtApprovalGroups:- "+districtApprovalGroups.size());
				
				String saGroupName = groupShortNameWithGroupName.get("SA")==null? "" : groupShortNameWithGroupName.get("SA");
				String esGroupName = groupShortNameWithGroupName.get("ES")==null? "" : groupShortNameWithGroupName.get("ES");
				String schoolName="";
				
				List<UserMaster> userMasterList = new ArrayList<UserMaster>();
				if(schoolId!=null && !schoolId.equals(""))
				{
					System.out.println("schoolId:- "+schoolId);
					List<Long> schoolIdList = new ArrayList<Long>();
					for(String id : schoolId.split(","))
						try{schoolIdList.add(Long.parseLong(id));}catch(Exception e){}
					
					System.out.println("schoolIdList:- "+schoolIdList);
					if(schoolIdList!=null && schoolIdList.size()>0)
					{
						Criterion cri1 = Restrictions.in("schoolId", schoolIdList);
						Criterion cri2 = Restrictions.eq("status", "A");
						
						List<SchoolMaster> schoolMasterList = schoolMasterDAO.findByCriteria(cri1,cri2);
						
						schoolName = schoolMasterList.get(0).getSchoolName();
						
						Criterion criterion1 = Restrictions.eq("entityType", new Integer(3));
						Criterion criterion_role = Restrictions.eq("roleId", roleMasterDAO.findById(3,false,false));
						Criterion criterion2 = Restrictions.in("schoolId", schoolMasterList);
						Criterion criterion4 = Restrictions.eq("status","A");
						
						userMasterList = userMasterDAO.findByCriteria(criterion1,criterion2,criterion4,criterion_role);
						if(userMasterList==null)
							userMasterList = new ArrayList<UserMaster>();
					}
					System.out.println("userMasterList:- "+userMasterList);
				}
				
				System.out.println("saGroupName:- "+saGroupName);
				System.out.println("districtApprovalGroups:- "+districtApprovalGroups);
				//for(Map.Entry<String, String> entry : groupShortNameWithGroupName.entrySet())
				{
					for(DistrictApprovalGroups approvalGroups : districtApprovalGroups)
					{
						String groupNAme = approvalGroups.getGroupName();
						String groupMembers = approvalGroups.getGroupMembers()==null?"": approvalGroups.getGroupMembers().trim();
						
						System.out.println("groupNAme:- "+groupNAme+", groupMembers:- "+groupMembers);
						if( groupMembers.equals("") )
						{
							if(groupNAme.equals(saGroupName))
							{
								if( userMasterList.size()==0 )
								{
									schoolNotExist= schoolName;
								}
							}
							else
							if(!groupNAme.equals(esGroupName))
								groupMemberNotExist = groupMemberNotExist+","+groupNAme;
						}
					}
				}
				
				for(Map.Entry<String, String> entry : groupShortNameWithGroupName.entrySet())
				{
					String shortGroupName = entry.getKey();
					String fullGroupName = entry.getValue();
					
					boolean isGroupExist=false;
					for(DistrictApprovalGroups approvalGroups : districtApprovalGroups)
					{
						String groupNAme = approvalGroups.getGroupName().trim();
						if(groupNAme.equals(fullGroupName))
						{
							isGroupExist=true;
							break;
						}
					}
					if(!isGroupExist)
						groupNotExist = groupNotExist+","+fullGroupName;
						
				}
			}
			catch (Exception e) 
			{
				e.printStackTrace();
			}

			return groupMemberNotExist+"####"+groupNotExist+"####"+schoolNotExist;
		}	
		
		public String getSchoolFullAddress(JobOrder jbOrder)
		{
			String schoolAddress="";
			String sAddress="";
			String sstate="";
			String scity = "";
			String szipcode="";
			try
			{
				if(jbOrder.getSchool().size()==1){
					if(jbOrder.getSchool()!=null)
					{
						if(jbOrder.getSchool().get(0).getAddress()!=null && !jbOrder.getSchool().get(0).getAddress().equals(""))
						{
							if(!jbOrder.getSchool().get(0).getCityName().equals(""))
							{
								schoolAddress = jbOrder.getSchool().get(0).getAddress()+", ";
							}
							else
							{
								schoolAddress = jbOrder.getSchool().get(0).getAddress();
							}
						}
						if(!jbOrder.getSchool().get(0).getCityName().equals(""))
						{
							if(jbOrder.getSchool().get(0).getStateMaster()!=null && !jbOrder.getSchool().get(0).getStateMaster().getStateName().equals(""))
							{
								scity = jbOrder.getSchool().get(0).getCityName()+", ";
							}else{
								scity = jbOrder.getSchool().get(0).getCityName();
							}
						}
						if(jbOrder.getSchool().get(0).getStateMaster()!=null && !jbOrder.getSchool().get(0).getStateMaster().getStateName().equals(""))
						{
							if(!jbOrder.getSchool().get(0).getZip().equals(""))
							{
								sstate = jbOrder.getSchool().get(0).getStateMaster().getStateName()+", ";
							}
							else
							{
								sstate = jbOrder.getSchool().get(0).getStateMaster().getStateName();
							}	
						}
						if(!jbOrder.getSchool().get(0).getZip().equals(""))
						{
							szipcode = jbOrder.getSchool().get(0).getZip().toString();
						}

						if(schoolAddress !="" || scity!="" ||sstate!="" || szipcode!=""){
							sAddress = schoolAddress+scity+sstate+szipcode;
						}else{
							sAddress="";
						}
					}
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			
			return sAddress;
		}
		
		/*public void saveJobOrderLog(int jobId,String jobCertificateId)
		{
			try
			{
				JobOrder jobOrder=jobOrderDAO.findById(jobId, false, false);
				//for certificate
				List<JobCertification> jobCertificationlist=jobCertificationDAO.findByCriteria(Restrictions.eq("jobId",jobOrder));
				StringBuilder jobCertificationBuilder=new StringBuilder();
				for(JobCertification jobCertification:jobCertificationlist)
				{
					jobCertificationBuilder.append(jobCertification.getCertificateTypeMaster().getCertTypeId()+",");
				}
				//end certificate
				//for school and requisitionnumber
				List<SchoolInJobOrder> listSchoolInJobOrder	= schoolInJobOrderDAO.findJobOrder(jobOrder);
				List<SchoolMaster> schoolMasters= new ArrayList<SchoolMaster>();
				StringBuilder attachSchoolData=new StringBuilder();
				for (SchoolInJobOrder schoolJobOrder : listSchoolInJobOrder){
					schoolMasters.add(schoolJobOrder.getSchoolId());
					attachSchoolData.append(schoolJobOrder.getSchoolId().getSchoolId()+"--"+schoolJobOrder.getNoOfSchoolExpHires()+",");
				}
				StringBuilder attachRequisitionData=new StringBuilder();
				Map<Long,List<JobRequisitionNumbers>> jobReqMap= new HashMap<Long, List<JobRequisitionNumbers>>();
				List<JobRequisitionNumbers> jobRequisitionNumbers = jobRequisitionNumbersDAO.findJobRequisitionNumbersBySchoolInJobOrder(jobOrder,schoolMasters);
				for(JobRequisitionNumbers jo:jobRequisitionNumbers)
				{
					
					attachRequisitionData.append(jo.getSchoolMaster()==null?"0":+jo.getSchoolMaster().getSchoolId()+"--"+jo.getDistrictRequisitionNumbers().getRequisitionNumber()+",");
				}
				
				//end school and requisitionnumber
				SessionFactory sessionFactory = jobOrderLogDAO.getSessionFactory();
				StatelessSession statelesSsession = sessionFactory.openStatelessSession();
				Transaction txOpen = statelesSsession.beginTransaction();
				JobOrderLog jobOrderLog=new JobOrderLog();
				
				BeanUtils.copyProperties(jobOrderLog, jobOrder);
				jobOrderLog.setJobOrder(jobOrder);
				jobOrderLog.setAttachSchoolData(attachSchoolData.toString());
				jobOrderLog.setAttachRequisitionData(attachRequisitionData.toString());
				//if(jobCertificateId!=null && !jobCertificateId.trim().equals(""))
				if(jobCertificationBuilder!=null && !jobCertificationBuilder.toString().trim().equals(""))
					jobOrderLog.setJobCertificationId(jobCertificationBuilder.toString());
				System.out.println(jobOrder.getJobId());
				System.out.println(jobOrder.getJobTitle());
				//jobOrderLog.setCreatedByEntity(userMaster.getEntityType());
				
				try {
					txOpen.begin();
					statelesSsession.insert(jobOrderLog);
					txOpen.commit();
					statelesSsession.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}*/
		
		private Map<String, String> getHireCandidatePercentage(List<JobOrder> jobOrderList, Map<String, String> positionMap)
		{
			Map<String, String> hireCandidatePercentageMap = new HashMap<String, String>();
			System.out.println("---------------------------Start getHireCandidatePercentage-----------------------------");
			try
			{
		    	if(jobOrderList!=null && jobOrderList.size()>0)
		    	{
		    		Map<Integer, List<JobForTeacher>> jobForTeacherWithJob = new HashMap<Integer, List<JobForTeacher>>();
		    		
		    		//getting the jobForTeacher
		    		StatusMaster statusMaster = WorkThreadServlet.statusIdMap.get(new Integer(6));
		    		Criterion criterion1 = Restrictions.in("jobId", jobOrderList);
		    		Criterion criterion2 = Restrictions.eq("status", statusMaster);
		    		Criterion criterion3 = Restrictions.eq("statusMaster", statusMaster);
		    		
		    		List<JobForTeacher> jobForTeacherList = jobForTeacherDAO.findByCriteria(criterion1,criterion2,criterion3);
		    		if(jobForTeacherList!=null)
		    		{
		    			for(JobForTeacher jobForTeacher : jobForTeacherList)
		    			{
		    				List<JobForTeacher> jjid = jobForTeacherWithJob.get(jobForTeacher.getJobId().getJobId());
		    				if(jjid==null)
		    					jjid = new ArrayList<JobForTeacher>();
		    				
		    				jjid.add(jobForTeacher);
		    				jobForTeacherWithJob.put(jobForTeacher.getJobId().getJobId(), jjid);
		    			}
		    		}
		    		
		    		//Now Calculating the position
		    		for(JobOrder jobOrder : jobOrderList)
		    		{
		    			float totalCandidate=0;
		    			float totalHireCandadte=0;
		    			Integer percentage=0;
		    			
		    			try{totalCandidate = Float.parseFloat(positionMap.get(""+jobOrder.getJobId()));} catch(Exception e){}
		    			
		    			List<JobForTeacher> jft = jobForTeacherWithJob.get(jobOrder.getJobId());
		    			if(jft!=null && jft.size()>0)
	    					totalHireCandadte = jft.size();
		    			
		    			try { 
		    				if(totalCandidate!=0)
		    					percentage = (int)((totalHireCandadte/totalCandidate)*100); 
		    				
		    			}catch(Exception e){}
		    			
		    			hireCandidatePercentageMap.put(jobOrder.getJobId().toString(), ((int)totalHireCandadte)+" ("+percentage+" %)");
		    			//System.out.println("Job Id:- "+jobOrder.getJobId()+", Hire Candidate:- "+totalHireCandadte+", totalCandidate:- "+totalCandidate+", percentage:- "+percentage+" %");
		    		}
		    		
		    	}
		    }
			catch(Exception e)
			{
				e.printStackTrace();
			}
			System.out.println("---------------------------End getHireCandidatePercentage-----------------------------");
			return hireCandidatePercentageMap;
		}
		
		private Map<String, String> getPositionsNumber(List<JobOrder> jobOrderList)
		{
			Map<String, String> jobPosition = new HashMap<String, String>();
			System.out.println("---------------------------Start getPositionsNumber-----------------------------");
			try
			{
		    	if(jobOrderList!=null && jobOrderList.size()>0)
		    	{
		    		Map<Integer, List<SchoolInJobOrder>> schoolInJobOrderWithJob = new HashMap<Integer, List<SchoolInJobOrder>>();
		    				    		
		    		//getting the schoolInJobOrder
		    		List<SchoolInJobOrder> schoolInJobOrderList = schoolInJobOrderDAO.findByCriteria(Restrictions.in("jobId", jobOrderList));
		    		if(schoolInJobOrderList!=null)
		    		{
		    			for(SchoolInJobOrder schoolInJobOrder : schoolInJobOrderList)
		    			{
		    				List<SchoolInJobOrder> sijo = schoolInJobOrderWithJob.get(schoolInJobOrder.getJobId().getJobId());
		    				if(sijo==null)
		    					sijo = new ArrayList<SchoolInJobOrder>();
		    				
		    				sijo.add(schoolInJobOrder);
		    				schoolInJobOrderWithJob.put(schoolInJobOrder.getJobId().getJobId(), sijo);
		    			}
		    		}
		    		
		    		//Now Calculating the position
		    		for(JobOrder jobOrder : jobOrderList)
		    		{
		    			Integer totalPosition=0;
		    			
		    			if(jobOrder.getNoOfExpHires()!=null)
		    				totalPosition+= jobOrder.getNoOfExpHires();
		    			else
		    			{
		    				List<SchoolInJobOrder> sijo = schoolInJobOrderWithJob.get(jobOrder.getJobId());
		    				if(sijo!=null)
		    					for(SchoolInJobOrder sjob : sijo)
		    						totalPosition+=	sjob.getNoOfSchoolExpHires()==null? 0 : sjob.getNoOfSchoolExpHires();
		    			}
		    			
		    			jobPosition.put(jobOrder.getJobId().toString(), ""+totalPosition);
		    			//System.out.println("Job Id:- "+jobOrder.getJobId()+", totalPosition:- "+totalPosition+", jobOrder.getNoOfExpHires():- "+jobOrder.getNoOfExpHires()+", jobOrder.getNoOfHires():- "+jobOrder.getNoOfHires());
		    		}
		    	}
		    }
			catch(Exception e)
			{
				e.printStackTrace();
			}
			System.out.println("---------------------------End getPositionsNumber-----------------------------");
			return jobPosition;
		}

		
		public List<DistrictRequisitionNumbers> getFieldOfPositionNumList(String positionNum, Long schoolId)
		{
			println("enter for getFieldOfPositionNumList-----------------------------------------"+positionNum+"   "+schoolId);
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException("Your session has expired!");
			}
			List<DistrictRequisitionNumbers> districtRequisitionNumbersList =  new ArrayList<DistrictRequisitionNumbers>();
			List<DistrictRequisitionNumbers> fieldOfHQList1 = null;
			List<DistrictRequisitionNumbers> fieldOfHQList2 = null;
			try{
				String positionNum1="";
				String jobTile="";
				String search[]=positionNum.trim().split("\\(");
				if(search.length>1){
					positionNum1=search[0].trim();
					jobTile=search[0].trim().split("\\)")[0];
				}else{
					positionNum1=search[0].trim();
					jobTile=search[0].trim();
				}
				Criterion criterion = Restrictions.or(Restrictions.like("requisitionNumber", positionNum1,MatchMode.ANYWHERE), Restrictions.like("jobTitle", jobTile,MatchMode.ANYWHERE));
				Criterion criterion1 = Restrictions.eq("status", "A");
				Criterion criterion2 = Restrictions.eq("isUsed", false);
				Criterion districtCri = Restrictions.eq("districtMaster", districtMasterDAO.findById(804800, false, false));
				Criterion schoolIdCri=null;
				if(schoolId!=null && schoolId!=0){schoolIdCri=Restrictions.eq("schoolMaster", schoolMasterDAO.findById(schoolId,false, false));}
				if(positionNum.trim()!=null && positionNum.trim().length()>0){
					if(schoolIdCri!=null)
					fieldOfHQList1 = districtRequisitionNumbersDAO.findWithLimit(Order.asc("requisitionNumber"), 0, 25, criterion,criterion1,criterion2,districtCri,schoolIdCri);
					else
					fieldOfHQList1 = districtRequisitionNumbersDAO.findWithLimit(Order.asc("requisitionNumber"), 0, 25, criterion,criterion1,criterion2,districtCri);

					Criterion criterion3 = Restrictions.or(Restrictions.ilike("requisitionNumber","%"+positionNum1+"%" ),Restrictions.ilike("jobTitle","%"+jobTile+"%" ));
					if(schoolIdCri!=null)
					fieldOfHQList2 = districtRequisitionNumbersDAO.findWithLimit(Order.asc("requisitionNumber"), 0, 25, criterion3,criterion2,criterion1,districtCri,schoolIdCri);
					else
					fieldOfHQList2 = districtRequisitionNumbersDAO.findWithLimit(Order.asc("requisitionNumber"), 0, 25, criterion3,criterion2,districtCri,criterion1);

					districtRequisitionNumbersList.addAll(fieldOfHQList1);
					districtRequisitionNumbersList.addAll(fieldOfHQList2);
					Set<DistrictRequisitionNumbers> setDistrict = new LinkedHashSet<DistrictRequisitionNumbers>(districtRequisitionNumbersList);
					districtRequisitionNumbersList = new ArrayList<DistrictRequisitionNumbers>(new LinkedHashSet<DistrictRequisitionNumbers>(setDistrict));
				}else{
					if(schoolIdCri!=null)
					fieldOfHQList1 = districtRequisitionNumbersDAO.findWithLimit(Order.asc("requisitionNumber"), 0, 25,criterion1,criterion2,districtCri,schoolIdCri);
					else
						fieldOfHQList1 = districtRequisitionNumbersDAO.findWithLimit(Order.asc("requisitionNumber"), 0, 25,criterion1,criterion2,districtCri);
					districtRequisitionNumbersList.addAll(fieldOfHQList1);
				}
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			println("districtRequisitionNumbersList=========="+districtRequisitionNumbersList.size());
			return districtRequisitionNumbersList;
		}
		public List<SchoolMaster> getFieldOfSchoolNameList(String schoolName,String requitionNumber)
		{
			println("enter for getFieldOfSchoolNameList-----------------------------------------");
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException("Your session has expired!");
			}
			List<SchoolMaster> schoolList =  new ArrayList<SchoolMaster>();
			List<SchoolMaster> fieldOfSMList1 = null;
			List<SchoolMaster> fieldOfSMList2 = null;
			try{
				if(schoolName.trim()!=null && schoolName.trim().length()>0){
					fieldOfSMList1 = districtRequisitionNumbersDAO.getSchoolMasterBySearchSNameAndRNumber(requitionNumber,schoolName);

					Criterion criterion3 = Restrictions.ilike("schoolName","% "+schoolName.trim()+"%" );
					fieldOfSMList2 = districtRequisitionNumbersDAO.getSchoolMasterBySearchSNameAndRNumber(requitionNumber,schoolName);

					schoolList.addAll(fieldOfSMList1);
					schoolList.addAll(fieldOfSMList2);
					Set<SchoolMaster> setDistrict = new LinkedHashSet<SchoolMaster>(schoolList);
					schoolList = new ArrayList<SchoolMaster>(new LinkedHashSet<SchoolMaster>(setDistrict));
				}else{
					fieldOfSMList1 = districtRequisitionNumbersDAO.getSchoolMasterBySearchSNameAndRNumber(requitionNumber,schoolName);
					schoolList.addAll(fieldOfSMList1);
				}
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			println("districtRequisitionNumbersList=========="+schoolList.size());
			return schoolList;
		}
		
		public List<Object> getAllRadioPositionOrSchool(String positionOrSchoolBy,String positionOrSchool){
			println("-------------------------------getAllRadioPositionOrSchool-------------------------------------");
			List<Object> listObject=new ArrayList<Object>();
			
			if(positionOrSchoolBy!=null && positionOrSchoolBy.trim().equalsIgnoreCase("position")){
				List<SchoolMaster> schoolLst=districtRequisitionNumbersDAO.getSchoolMasterByRNumber(positionOrSchool);
				println("schoolLst===="+schoolLst.size());
				for(SchoolMaster sm:schoolLst){
					listObject.add(sm);
				}
			}
			else if(positionOrSchoolBy!=null && positionOrSchoolBy.trim().equalsIgnoreCase("school")){
				List<DistrictRequisitionNumbers> lstDRN=districtRequisitionNumbersDAO.getSchoolMasterBySchoolId(positionOrSchool);
				println("lstDRN=========="+lstDRN.size());
				for(DistrictRequisitionNumbers drn:lstDRN){
					listObject.add(drn);
				}
			}
			return listObject;
		}

		
		public String getDataFromJobOrderHistory(Integer jobId){
			StringBuffer sb =	new StringBuffer();
			String responseText="";
			//  map of table column name ( as key )  and UI label ( as value)
			LinkedHashMap<String, String> columnVsUI  = getColumnVsUIMap();
			List<AdminActivityDetail> adminActivityDetails = adminActivityDetailDAO.findByEntityIdNdType(jobId);
			//List <JobCategoryMasterHistory> data= jobCategoryMasterHistoryDAO.listJobCategoryHistoryByPrevId(jobCategoryId);
			/*
			List<String> noCheckLabels=new ArrayList<String>();
				noCheckLabels.add(StringUtils.capitalize("JobCategoryHistoryId"));
				noCheckLabels.add(StringUtils.capitalize("updateTime"));
				noCheckLabels.add(StringUtils.capitalize("updateAction"));
			*/
			sb.append("<table border='0' width='750' class='tablecgtbl'>");
			if(adminActivityDetails!=null && adminActivityDetails.size()>0){
			 sb.append("<tr><td>");
			 
			 sb.append("<table border='0' style='padding:0px;' cellpadding=0 cellspacing=0  width='100%' class='tablecgtbl'>");
					
			 	int counter=0;
				String year="";
				String preYear="";
				//List<String> fields=new ArrayList<String>();
				String fields="";
			 if(adminActivityDetails!=null && adminActivityDetails.size()>0){
				//for(JobCategoryMasterHistory jobCatHis : data){
					for(AdminActivityDetail adminActivity : adminActivityDetails){
						//AdminActivityDetail adminActivityDetail = adminActivityDetails.get(i);
						// hide preAdd Flag object from UI ( preAdd - while old job category is updated first time )
						if(adminActivity.getActivityType()!=null && adminActivity.getActivityType().equalsIgnoreCase("preAdd")) continue;
			 
						if(adminActivity.getActivityType().equalsIgnoreCase("update")){
							try {
								//Map<String,Object> first =HqJobCategoryAjax.ConvertObjectToMap(jobCatHis);
								//Map<String,Object> second =HqJobCategoryAjax.ConvertObjectToMap(data.get(i+1));
								fields= getDiffJobValueKeys(adminActivity , columnVsUI);
								if(fields == null) continue;
							} catch (Exception e) {
								e.printStackTrace();
							}
						}else{
							if(adminActivity.getActivityType().equalsIgnoreCase("create"))
							fields="<span style='color:green'>New job Order created.</style>";
							else
								fields="N/A";						
							}
					 
				
				year = convertUSformatOnlyYear(adminActivity.getCreatedDate());
				counter++;
				if(!preYear.equals(year)){
				sb.append("<tr>");
				sb.append("<td colspan=3 style='text-align:right;padding-right:8px;padding-bottom:5px;verical-align:bottom;'><span class=\"txtyearbg\">"+year+"</span></td><td>&nbsp;</td>");
				sb.append("<tr>");
				sb.append("<tr>");
				sb.append("<td width='15%' style='height:10px;'>&nbsp;</td>");
				sb.append("<td style='text-align:right;vertical-align:middle;padding-left:12px;'  height=10>");
				sb.append("<span class=\"vertical-line\"></span></td><td ></td>");
				sb.append("</tr>");
				}
				preYear=year;
				
				sb.append("<tr>");
				
				
						sb.append("<td width='25%' style='text-align:right;vertical-align:middle;position:relative;'>"+Utility.convertDateAndTimeFormatForCommunication(adminActivity.getCreatedDate()));
							if(adminActivity.getActivityType().equalsIgnoreCase("create"))
								sb.append("<span class='note-com'><img src=\"images/add_circle.png\" style=\"width:20px;    margin-left: 3px; height:auto;\"></span>");
							else
							sb.append("<span class='note-com'><img src=\"images/edit_circle.png\" style=\"width:20px;    margin-left: 3px; height:auto;\"></span>");
							
						sb.append("</td><td style='text-align:right;vertical-align:middle;padding-left:12px;' width='8px;' height='13'>");
							sb.append("<span class=\"vertical-line\"></span></td>");
							sb.append("<td  style='text-align:right;vertical-align:middle;' width=20>");
						sb.append("<hr style=\"color: #f00; background:#007AB4; width: 100%; height: 3px;\"/></td>");
						sb.append("<td class='commbgcolor'  style='padding:5px;vertical-align:middle;'>");
						sb.append("<span class='com-user' style='line-height:14px;'>Action: "+(adminActivity.getActivityType().equalsIgnoreCase("create")==true?"Create":"Update") +"</span><br/>");
						sb.append("<span class='com-user' style='line-height:14px;'>User: "+adminActivity.getUserMaster().getFirstName()+" "+adminActivity.getUserMaster().getLastName()+" ( "+adminActivity.getUserMaster().getEmailAddress()+" )</span><br/>");
						sb.append("<span class='com-user' style='line-height:14px;'>Fields that were updated: "+fields.toString()+"</span>");
						sb.append("</tr>"); 
						sb.append("<tr>");
						sb.append("<td width='15%' style='height:20px;'>&nbsp;</td>");
						sb.append("<td style='text-align:right;vertical-align:middle;padding-left:12px;'  height=20>");
						sb.append("<span class=\"vertical-line\"></span></td><td ></td>");
						sb.append("</tr>");
						 
				//}
			 }
					/*if(counter!=0){
							sb.append("<tr><td>"+lblNoRecord+"</td></tr>");
						}*/
			 }
					sb.append("</table></td></tr>");
			}else{
				sb.append("<tr><td>"+lblNoRecord+"</td></tr>");
				}
				sb.append("</table>");

			
			
			return sb.toString();
			
		}


		
		public LinkedHashMap<String, String> getColumnVsUIMap(){
			LinkedHashMap<String, String> map  =  new LinkedHashMap<String, String>();
			map.put("JobTitle", "Job Title");
			map.put("branchMaster", "Branch Name");
			map.put("headQuarterMaster", "Headquarter Name");
			map.put("districtMaster", "District Name");
			map.put("jobStartDate", "Posting Start Date");
			map.put("jobEndDate", "Posting End Date");
			map.put("jobDescription", "job Description");
			map.put("jobQualification", "job Qualification");
			map.put("jobCategoryMaster", "Job Category Name");
			map.put("jobType", "job Type");
			map.put("noOfExpHires", "of Expected Hire(s)");
			map.put("requisitionNumber", "Enter Requisition Number(s)");
			
			map.put("GeoZoneMaster", "Zone");
			map.put("SubjectMaster", "Subject Name");
			map.put("apiJobId", "Reference No./Job Code");
			map.put("isExpHireNotEqualToReqNo", "Multiple hires will be allocated to a single requisition number");
			map.put("NoOfExpHires", "Expected Hire(s)");
			
			
			map.put("isJobAssessment", "No job specific inventory is needed");
			map.put("writePrivilegeToSchool", "District and attached school(s) have read/write privilege");
			map.put("OfferAssessmentInviteOnly", "Offer Assessment to candidates on invite only");
			map.put("IsInviteOnly", "IsInviteOnly");
			map.put("PathOfJobDescription", "Upload Job Description");
			
			
			
			map.put("attachDefaultDistrictPillar", "Use the default job specific inventory set at the District");
			map.put("jobAssessmentStatus", "JSI is mandatory");
			map.put("i4QuestionSets", "Question set");
			map.put("maxScoreForVVI", "Want to score");
			map.put("sendAutoVVILink", "Want to send the interview link automatically");
			map.put("timeAllowedPerQuestion", "Time Allowed Per Question");
			map.put("VVIExpiresInDays", "Virtual Video interview Expires In Days");
			map.put("notIficationToschool", "Send notification to attached schools");
			map.put("offerAssessmentInviteOnly", "Offer Assessment to candidates on invite only");
			map.put("exitMessage", "Exit Message");
			map.put("PanelStatus", "Panel Status");
			map.put("Certificate", "Certificate");
			map.put("School", "Selected School(s)");
			/*map.put("questionSetsForOnboarding", "Qualification Question Set to Job Category For Onboarding");
			map.put("approvalBeforeGoLive", "Approval process for positions prior to them going live");
			map.put("noOfApprovalNeeded", "Number of Approvals Needed");
			map.put("buildApprovalGroup", "Build Approval Groups ");
			map.put("questionSetVal", "e-Reference Question Set Id");
			map.put("userMaster", "User");
			map.put("updateAction", "Update Action");
			map.put("ipAddress", "IpAddress");
			map.put("spAssessmentId", "Pre-Hire Smart Practices Assesment Id");*/
			
			return map;
		}


		public String getDiffJobValueKeys(AdminActivityDetail adminActivity , LinkedHashMap<String, String> columnVsUI){
			StringBuffer result =new StringBuffer();
			result.append("<table class='table table-hover top2' style='margin-bottom: 10px !important;'>");
			result.append("<thead style='background-color:#007AB4;'><tr><th>#</th><th>Field Name</th><th>Previous Value</th><th>Current Value</th></tr></thead>");
			int counter=0;
			String columnVsUIKey="";
			try{
				HashMap<String, String> logMap = new HashMap<String, String>();
				logMap = Utility.jsonToMap(adminActivity.getDescription());
				//if(first!=null && second!=null){
					for(String key : columnVsUI.keySet()){
						columnVsUIKey = key;
						key = StringUtils.capitalize(key);        //  make key of columnVsUI map in Capitalize format.
						//if(key.equalsIgnoreCase("jobCategoryHistoryId")){System.out.println("********************    equal  no check label *******************");};
					/*	
						if(key.equalsIgnoreCase("JobTitle")){
							result.append("<tr><th scope='' style='min-width:20px; color:#007AB4;'>"+(counter+1)+"</th><td>"+columnVsUI.get(columnVsUIKey)+"</td><td>fisrt</td><td>second</td></tr>");
							counter++;
						}*/
					if(logMap.containsKey(key)){
						try {
							result.append("<tr><th scope='' style='min-width:20px; color:#007AB4;'>"+(counter+1)+"</th><td>"+columnVsUI.get(columnVsUIKey)+"</td><td>"+(logMap.get(key)).split("\\|",2)[0]+"</td><td>"+(logMap.get(key)).split("\\|",2)[1]+"</td></tr>");
							counter++;
						} catch (Exception e) {
							System.out.println("excedption for key ==="+key);
							e.printStackTrace();
						}
					}
						
					//	if(noCheckLabels.contains(key)) {continue;}  // for no check label
						/*if(first.get(key)==null || second.get(key)==null){
							if(!(second.get(key)==null && first.get(key)==null )){
								result.append("<tr><th scope='' style='min-width:20px; color:#007AB4;'>"+(counter+1)+"</th><td>"+columnVsUI.get(columnVsUIKey)+"</td><td>"+HqJobCategoryAjax.getObjValue(second.get(key))+"</td><td>"+HqJobCategoryAjax.getObjValue(first.get(key))+"</td></tr>");
								counter++;
								}
						}else{
							if(first.get(key).hashCode()!=second.get(key).hashCode()){
								result.append("<tr><th scope='' style='min-width:20px; color:#007AB4;'>"+(counter+1)+"</th><td>"+columnVsUI.get(columnVsUIKey)+"</td><td>"+HqJobCategoryAjax.getObjValue(second.get(key))+"</td><td>"+HqJobCategoryAjax.getObjValue(first.get(key))+"</td></tr>");
								counter++;
							}  
							}*/
					} 
				//}
				 
				result.append("</table>");
			}
			catch (Exception e) {
				System.out.println("*****************************   Error while comparing two map for key******************************");
				e.printStackTrace();
			}
			//sandeep if(counter<=1) result= null;
			return result !=null ? result.toString():null;
			
		}
		public List<DistrictAccountingCode> getAccountCodesMasterList(String accountCode, int districtId)
		{	
			List<DistrictAccountingCode> accountingCodeMasterList =  new ArrayList<DistrictAccountingCode>();
			List<DistrictAccountingCode> fieldOfSchoolList1 = null;
			List<DistrictAccountingCode> fieldOfSchoolList2 = null;
			try 
			{
				Criterion criterion = Restrictions.like("accountingCode", accountCode,MatchMode.START);
				Criterion criterion2 = Restrictions.eq("districtId", districtId);
				
				// Get School List, this code using when districtId.equals("0") || districtId.equals("") in JobsBoard {Not Tested}
				if(districtId>0){
					if(accountCode.trim()!=null && accountCode.trim().length()>0){
						fieldOfSchoolList1 = districtAccountingCodeDAO.findWithLimit(null, 0, 10, criterion);
					}
					else{
						//fieldOfSchoolList1 = districtAccountingCodeDAO.findWithLimit(null, 0, 10);
						//Collections.sort(fieldOfSchoolList1,DistrictAccountingCode.compSchoolMaster);
						//accountingCodeMasterList.addAll(fieldOfSchoolList1);
					}
				}
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
		   return accountingCodeMasterList;
     }	
		
		public String displayRecordsByEntityTypeNewESNew(boolean resultFlag,int JobOrderType,int districtOrSchoolId,int 
				schoolId,/*String subjectId,String certifications,*/String jobOrderIds,String status,String noOfRow,
				String pageNo,String sortOrder,String sortOrderType,String  disJobReqNo,String geoZonId,String jobCategoryIds,String searchTerm,String jobApplicationStatus)
		{

			System.out.println("**************displayRecordsByEntityTypeNewESNew***********************");
			/* ========  For Session time Out Error =========*/
			
			int jobApplicationStatusId=0;
			if(jobApplicationStatus!=null && jobApplicationStatus!="" &&!jobApplicationStatus.equals(""))
				jobApplicationStatusId =  Integer.parseInt(jobApplicationStatus);
			
			JSONObject jsonObject=null;
			int	geoZoneId=0;
			if(geoZonId!=null && geoZonId!="" &&!geoZonId.equals(""))
			  geoZoneId =  Integer.parseInt(geoZonId);
			
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}
			StringBuffer tmRecords =	new StringBuffer();
			try{
				UserMaster userMaster = null;
				Integer entityID=null;
				DistrictMaster districtMaster=null;
				SchoolMaster schoolMaster=null;
				SubjectMaster subjectMaster	=	null;
				int roleId=0;
				if (session == null || session.getAttribute("userMaster") == null) {
					return "false";
				}else{
					userMaster=(UserMaster)session.getAttribute("userMaster");

					if(userMaster.getSchoolId()!=null)
						schoolMaster =userMaster.getSchoolId();
					

					if(userMaster.getDistrictId()!=null){
						districtMaster=userMaster.getDistrictId();
					}

					entityID=userMaster.getEntityType();
					if(userMaster.getRoleId().getRoleId()!=null){
						roleId=userMaster.getRoleId().getRoleId();
					}
				}
				String roleAccess=null;
				try{
					if(JobOrderType==3){
						roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,25,"schooljoborders.do",0);
					}else{
						roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,24,"managejoborders.do",0);
					}
				}catch(Exception e){
					e.printStackTrace();
				}

				//-- get no of record in grid,
				//-- set start and end position

				int noOfRowInPage = Integer.parseInt(noOfRow);
				int pgNo = Integer.parseInt(pageNo);
				int start =((pgNo-1)*noOfRowInPage);
				int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
				int totalRecord = 0;
				//------------------------------------

				/** set default sorting fieldName **/
				String sortOrderFieldName="jobId";
				String sortOrderNoField="jobId";

				boolean deafultFlag=false;
				/**Start set dynamic sorting fieldName **/
				Order  sortOrderStrVal=null;
				if(sortOrder!=null){
					if(!sortOrder.equals("") && !sortOrder.equals(null) && !sortOrder.equals("ofApplicant") && !sortOrder.equals("ofHire")){
						sortOrderFieldName=sortOrder;
						sortOrderNoField=sortOrder;
						deafultFlag=false;
					}else{
						deafultFlag=true;
					}
					if(sortOrder.equals("ofApplicant")){
						sortOrderNoField="ofApplicant";
					}
					if(sortOrder.equals("ofHire")){
						sortOrderNoField="ofHire";
					}
				}
				String sortOrderTypeVal="0";
				sortOrderStrVal=Order.asc(sortOrderFieldName);
				if(sortOrderType!=null){
					if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
						if(sortOrderType.equals("1")){
							sortOrderTypeVal="1";
							sortOrderStrVal=Order.desc(sortOrderFieldName);
						}
					}
				}
				if(sortOrderType.equals("")){
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}

				/**End ------------------------------------**/
				List<JobOrder> lstJobOrder	  =	 new ArrayList<JobOrder>();
				boolean jobOrderIdFlag=false;
				boolean statusFlag=false;

				Criterion criterionStatus=null;
				Criterion criterionJobFilter=null;
				if(!(jobOrderIds.equals("0") || jobOrderIds.equalsIgnoreCase("0"))){
					String [] jobsArr =  jobOrderIds.split(",");
					List<Integer> jobs = new ArrayList<Integer>();
					for(int i=0;i<jobsArr.length;i++)
					{	
						String jobId = jobsArr[i].trim();
						try{if(!jobId.equals(""))
							jobs.add(Integer.parseInt(jobId));
						}catch(Exception e){}
					}
					if(jobs.size()>0)
					{
						jobOrderIdFlag=true;
						criterionJobFilter	= Restrictions.in("jobId",jobs);
					}
				}

				if(status!=null){
					if(!status.equalsIgnoreCase("") && !status.equalsIgnoreCase(null)){
						statusFlag=true;
						criterionStatus=Restrictions.eq("status",status);
					}
				}else{
					statusFlag=false;
				}

				int noOfRecordCheck = 0;
				int totalRecords=0;
				/*************Start *************/
				List<Integer> filterJobOrderListNew = new ArrayList<Integer>();
				//List<JobOrder> filterJobOrderList = new ArrayList<JobOrder>();

				boolean jobFlag=false;
				
				DistrictMaster  distMaster=null;
				List<JobOrder> districtJList	  =	 new ArrayList<JobOrder>();
				List<Integer> districtJListInt	  =	 new ArrayList<Integer>();
				boolean districtFlag=false;
				if(districtOrSchoolId!=0){
					if(districtOrSchoolId!=0){
						distMaster=districtMasterDAO.findById(districtOrSchoolId,false,false);
					}
					districtFlag=true;
					//districtJList=jobOrderDAO.findJobByDistrict(distMaster);					
					districtJListInt = jobOrderDAO.findJobIdsByDistrict(distMaster);
					
					System.out.println("districtJListInt    "+districtJListInt.size());
					//System.out.println(checkTest.size()+" :checkTest     distM: "+districtJList.size());
				}
				if(districtFlag && districtJListInt.size()>0){
					if(jobFlag){
						filterJobOrderListNew.retainAll(districtJListInt);
					}else{
						filterJobOrderListNew.addAll(districtJListInt);
					}
				}
				if(districtFlag){
					jobFlag=true;
				}

				System.out.println(" >>>>>>>>>>>>>>>>>>>>>>>111111> jobCategoryIds :::::::: "+jobCategoryIds);
				
				Criterion criterionJobOrderType = Restrictions.eq("createdForEntity",JobOrderType);
				
				List<JobOrder> schoolJList	  =	 new ArrayList<JobOrder>();
				List<Integer> schoolJListNew	  =	 new ArrayList<Integer>();
				boolean schoolFlag=false;
				int searchschoolflag=2;
				if(schoolId!=0){
					SchoolMaster  sclMaster=new SchoolMaster();
					if(districtOrSchoolId!=0){
						//sclMaster=schoolMasterDAO.findById(Long.parseLong(schoolId+""),false,false);
						sclMaster.setSchoolId(Long.parseLong(schoolId+""));
					}
					schoolFlag=true;
					searchschoolflag=1;
					if(distMaster!=null){
						//schoolJList=schoolInJobOrderDAO.findJobListBySchool(sclMaster,distMaster,JobOrderType,entityID,searchschoolflag,userMaster);
						schoolJListNew =schoolInJobOrderDAO.findJobIdBySchool1(sclMaster,distMaster,JobOrderType,entityID,searchschoolflag,userMaster);
						//schoolJListNew 3526						
					}
				}
				else
				{// Only for SA for Nobletype district
					if(userMaster.getEntityType()==3 && JobOrderType==2 && userMaster.getDistrictId()!=null && distMaster.getWritePrivilegeToSchool().equals(true))
					{
						searchschoolflag=0; // below [ schoolMaster ] is sending from school id by getting  session
						schoolJListNew =schoolInJobOrderDAO.findJobIdBySchool1(schoolMaster,distMaster,JobOrderType,entityID,searchschoolflag,userMaster);
						//schoolJList=schoolInJobOrderDAO.findJobListBySchool(schoolMaster,distMaster,JobOrderType,entityID,searchschoolflag,userMaster);
						schoolFlag=true;					

					}
				}
				if(schoolFlag && schoolJListNew.size()>0){
					if(jobFlag){
						filterJobOrderListNew.retainAll(schoolJListNew);
					}else{
						filterJobOrderListNew.addAll(schoolJListNew);
					}
				}else if(schoolFlag && schoolJListNew.size()==0){
					filterJobOrderListNew = new ArrayList<Integer>();
				}

				if(schoolFlag){
					jobFlag=true;
				}
				if(userMaster.getDistrictId()!=null && userMaster.getDistrictId().getDistrictId().equals(806900))
				if(userMaster.getEntityType()==3){
					List<Integer> approvaljobid=findJobIdSaAsApprovalProcess(userMaster);
					if(approvaljobid!=null &&approvaljobid.size()>0)
						filterJobOrderListNew.addAll(approvaljobid);
				}
				/**********rajendra: search by jobrequisition no**********************/
				boolean jobReqnoFlag=false;
				//List<JobOrder> jobOrderOfReqNoList	  =	 new ArrayList<JobOrder>();
				List<Integer> jobOrderOfReqNoListNew  =	 new ArrayList<Integer>();
				if(disJobReqNo!=null && !disJobReqNo.equals("") && !disJobReqNo.equals("0")){
					jobReqnoFlag=true;
					List<DistrictRequisitionNumbers> districtRequisitionNumbers=null;
					Criterion crDrq=Restrictions.eq("requisitionNumber", disJobReqNo);
					Criterion crDSTrq=null;
					if(districtOrSchoolId!=0){
						crDSTrq=Restrictions.eq("districtMaster", distMaster);
						districtRequisitionNumbers=districtRequisitionNumbersDAO.findByCriteria(crDSTrq,crDrq);
					}else{
						districtRequisitionNumbers=districtRequisitionNumbersDAO.findByCriteria(crDrq);
					}
					if(districtRequisitionNumbers!=null && districtRequisitionNumbers.size()>0){
						
						Criterion crJrq=Restrictions.in("districtRequisitionNumbers", districtRequisitionNumbers);
						List<JobRequisitionNumbers> jobRequisitionNumbers=jobRequisitionNumbersDAO.findByCriteria(crJrq);
						for(JobRequisitionNumbers jqr:jobRequisitionNumbers){
							//jobOrderOfReqNoList.add(jqr.getJobOrder());
							jobOrderOfReqNoListNew.add(jqr.getJobOrder().getJobId());
						}
					}
				}
				
				if(jobReqnoFlag && jobOrderOfReqNoListNew.size()>0){
					if(jobFlag){
						filterJobOrderListNew.retainAll(jobOrderOfReqNoListNew);
					}else{
						filterJobOrderListNew.addAll(jobOrderOfReqNoListNew);
					}
				}else if(jobReqnoFlag && jobOrderOfReqNoListNew.size()==0){
					filterJobOrderListNew = new ArrayList<Integer>();
				}
				
				if(jobReqnoFlag){
					jobFlag=true;
				}
				/**********rajendra: search by jobrequisition no**********************/

				// =============  Deepak filtering basis on job Status  added by 27-05-2015
				if(jobApplicationStatusId>0 && distMaster!=null && distMaster.getDistrictId().toString().equalsIgnoreCase("804800"))
				{
					boolean jobApplicationStatusFlag = false;						
						List<Integer> jobApplicationStatusListNew = new ArrayList<Integer>();
							if (disJobReqNo != null && jobApplicationStatusId > 0) {
								jobApplicationStatusFlag = true;
								//jobApplicationStatusList = jobOrderDAO.getJobApplicationStatusRole(jobApplicationStatusId);
								jobApplicationStatusListNew = jobOrderDAO.getJobApplicationIdsStatusRole(jobApplicationStatusId);
								System.out.println("jobApplicationStatusList========"+ jobApplicationStatusListNew.size());
							}
							if (jobApplicationStatusFlag && jobApplicationStatusListNew.size() > 0) {
								if (jobFlag == true) {
									filterJobOrderListNew.retainAll(jobApplicationStatusListNew);
								} else {
									filterJobOrderListNew.addAll(jobApplicationStatusListNew);
								}
							} else {
								filterJobOrderListNew = new ArrayList<Integer>();
							}
					if (jobApplicationStatusFlag) {
						jobFlag = true;
					}
				}
				
				
				 //     Mukesh filtering on the basis of zone					
					List<Integer> listjobOrdersgeoZoneNew =new ArrayList<Integer>();				
				  if(geoZoneId>0)
					{
						GeoZoneMaster geoZoneMaster=geoZoneMasterDAO.findById(geoZoneId, false, false);
						if(userMaster.getRoleId().getRoleId().equals(3)){
								//listjobOrdersgeoZone = jobOrderDAO.getJobOrderBygeoZoneRole(geoZoneMaster,userMaster);
								listjobOrdersgeoZoneNew = jobOrderDAO.getJobOrderIdsBygeoZoneRole(geoZoneMaster,userMaster);
						}else
							listjobOrdersgeoZoneNew = jobOrderDAO.getJobOrderIdsBygeoZoneRole(geoZoneMaster,null);
					}
				 
				  if(listjobOrdersgeoZoneNew!=null && listjobOrdersgeoZoneNew.size()>0)
				  {
					  if(jobFlag==true){
						  filterJobOrderListNew.retainAll(listjobOrdersgeoZoneNew);
						}else{
							filterJobOrderListNew.addAll(listjobOrdersgeoZoneNew);
						}
				  }	
				  if(listjobOrdersgeoZoneNew.size()==0 && geoZoneId==-1 ||geoZoneId==0)
				  {
					  filterJobOrderListNew.addAll(listjobOrdersgeoZoneNew);
				  
				  }
				  else if(listjobOrdersgeoZoneNew.size()==0 &&geoZoneId!=-1)
				  {
					  filterJobOrderListNew.retainAll(listjobOrdersgeoZoneNew);
				  }
				  
				  
				  
				// Job category
					List<Integer> listjJobOrdersNew = new ArrayList<Integer>();
					boolean jobCateFlag=false;
					if(jobCategoryIds!=null && !jobCategoryIds.equals("") && !jobCategoryIds.equals("0"))
					{
						List<Integer> jobcategoryIds = new ArrayList<Integer>();
						String jobCategoryIdStr[] =jobCategoryIds.split(",");
					  if(!ArrayUtils.contains(jobCategoryIdStr, "0"))
					  {		  
						for(String str : jobCategoryIdStr){
							 if(!str.equals("")){							 
							 	jobcategoryIds.add(Integer.parseInt(str));
							 }
						 } 
						 if(jobcategoryIds.size()>0){
							 listjJobOrdersNew = jobOrderDAO.findJobIdsByJobCategery(jobcategoryIds,districtOrSchoolId);
							 jobCateFlag=true;
						 }
						 if(jobCateFlag){
							filterJobOrderListNew.retainAll(listjJobOrdersNew);
						 }else{
							filterJobOrderListNew.addAll(listjJobOrdersNew);
						 }
						}
					  }

					if(jobCateFlag){
						jobFlag=true;
					}
				  
					
					
			      if(jobFlag){
			    	  //System.out.println("jobF  "+jobFlag);
			    	  System.out.println("filterJobOrderListNew   "+filterJobOrderListNew.size());
					if(filterJobOrderListNew.size()>0){
						List jobIds = new ArrayList();
						String esJobIds="";
						for(int e=0; e<filterJobOrderListNew.size(); e++) {
							jobIds.add(filterJobOrderListNew.get(e));
							esJobIds+=filterJobOrderListNew.get(e)+" ";
						}

						Criterion criterionJobList = Restrictions.in("jobId",jobIds);

						Criterion criterionMix=criterionJobOrderType;
						if(statusFlag && jobOrderIdFlag){
							criterionMix=Restrictions.and(criterionStatus,criterionJobFilter);
						}else if(statusFlag){
							criterionMix=criterionStatus;
						}else if(jobOrderIdFlag){
							criterionMix=criterionJobFilter;
						}
						
						Criterion crt[]={criterionJobList,criterionJobOrderType,criterionMix};
						Order poolord=Order.desc("isPoolJob");
						Order orders[]={poolord,sortOrderStrVal};
						
						if(userMaster.getEntityType()!=1 && userMaster.getDistrictId().getDistrictId().equals(1200390))
						{
							//lstJobOrder=jobOrderDAO.getJobOrderByPool(crt, orders, start,noOfRowInPage);
							jsonObject=new ElasticSearchService().esForSchoolJobOrder(status, jobOrderIds, esJobIds, start, noOfRowInPage, sortOrderFieldName, sortOrderTypeVal, searchTerm.replaceAll("\"", ""),JobOrderType);
						}
						else
						{
							//lstJobOrder=jobOrderDAO.findWithLimit(sortOrderStrVal,start,noOfRowInPage,criterionJobList,criterionJobOrderType,criterionMix);
							jsonObject=new ElasticSearchService().esForSchoolJobOrder(status, jobOrderIds, esJobIds, start, noOfRowInPage, sortOrderFieldName, sortOrderTypeVal, searchTerm.replaceAll("\"", ""),JobOrderType);
						}
						//totalRecords=jobOrderDAO.getRowCountWithSort(sortOrderStrVal,criterionJobList,criterionJobOrderType,criterionMix);
						//System.out.println(((JSONObject)jsonObject.get("hits")).getInt("total"));
						totalRecords=((JSONObject)jsonObject.get("hits")).getInt("total");
					}
				}else{
					Criterion criterionMix=criterionJobOrderType;
					if(statusFlag && jobOrderIdFlag){
						criterionMix=Restrictions.and(criterionStatus,criterionJobFilter);
					}else if(statusFlag){
						criterionMix=criterionStatus;
					}else if(jobOrderIdFlag){
						criterionMix=criterionJobFilter;
					}
					
					Criterion crt[]={criterionJobOrderType,criterionMix};
					Order poolord=Order.desc("isPoolJob");
					Order orders[]={poolord,sortOrderStrVal};
					
					if(userMaster.getEntityType()!=1 && userMaster.getDistrictId().getDistrictId()==1200390)
					{
						//lstJobOrder=jobOrderDAO.getJobOrderByPool(crt, orders, start,noOfRowInPage);
						jsonObject=new ElasticSearchService().esForSchoolJobOrder(status, jobOrderIds, "", start, noOfRowInPage, sortOrderFieldName, sortOrderTypeVal, searchTerm.replaceAll("\"", ""),JobOrderType);
					}
					
					
					else
					{
						//lstJobOrder=jobOrderDAO.findWithLimit(sortOrderStrVal,start,noOfRowInPage,criterionJobOrderType,criterionMix);
						jsonObject=new ElasticSearchService().esForSchoolJobOrder(status, jobOrderIds, "", start, noOfRowInPage, sortOrderFieldName, sortOrderTypeVal, searchTerm.replaceAll("\"", ""),JobOrderType);
					}
					
					//totalRecords=jobOrderDAO.getRowCountWithSort(sortOrderStrVal,criterionJobOrderType,criterionMix);
					totalRecords=((JSONObject)jsonObject.get("hits")).getInt("total");
				}
			      String testJ = "";
				/***************************** End Code **************************************/
			      JSONObject jsonObjectTemp=jsonObject;
			      if(jsonObjectTemp!=null)
					{
						try{
							if((JSONObject)jsonObjectTemp.get("hits")!=null){
								if(((JSONObject)jsonObjectTemp.get("hits")).get("hits")!=null){
									JSONArray hits= (JSONArray) ((JSONObject)jsonObjectTemp.get("hits")).get("hits");
									System.out.println("hits size new code===="+hits.size());
									
									for(int i=0; i<hits.size(); i++)
									{
										jsonObjectTemp=(JSONObject)hits.get(i);
										String index=(String)jsonObjectTemp.get("_index");
										String type=(String)jsonObjectTemp.get("_type");
										String id=(String)jsonObjectTemp.get("_id");
										jsonObjectTemp=(JSONObject)jsonObjectTemp.get("_source");
								    	JobOrder jobOrder=new JobOrder();
								    	jobOrder.setJobId(jsonObjectTemp.getInt("jobId"));
								    	lstJobOrder.add(jobOrder);
								    	testJ=testJ+jsonObjectTemp.getInt("jobId")+",";
									}
								}
							}
							
							System.out.println("testJtestJ    "+testJ);
						}catch(Exception e){
							e.printStackTrace();
						}	
						
					}
					
					long a = System.currentTimeMillis();
			      Map<Integer,String> map = new HashMap<Integer, String>();
					if(lstJobOrder.size()>0)
					 map = jobForTeacherDAO.countApplicantsByJobOrdersAllll(lstJobOrder);
					TestTool.getTraceSecond("time 2");	
					long b = System.currentTimeMillis();
					
					System.out.println("end running Execution time: " + TimeUnit.MILLISECONDS.toSeconds((b - a))+ " Sec;");
					totalRecord =totalRecords;

				if(totalRecord<end)
					end=totalRecord;
				
				//configurable coloumn
				Map<String,Boolean> displayCNFGColumn=null;
				if(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==2){
				displayCNFGColumn=getCNFGColumn(districtMaster,true);
				}
				else
				displayCNFGColumn=getCNFGColumn(districtMaster,false);

				tmRecords.append("<table  id='tblGrid' width='100%' border='0'>");
				tmRecords.append("<thead class='bg'>");
				tmRecords.append("<tr>");
				String responseText="";
				if(deafultFlag){
					sortOrderTypeVal="1";
				}
				
				//Start Sandeep 03-09-15		
				
				  Boolean hrIntegratedFlag = false;
			      if(userMaster.getDistrictId() != null)
			    	  hrIntegratedFlag = userMaster.getDistrictId().getHrIntegrated();
				
		//end	
			    if(userMaster.getDistrictId()==null || displayCNFGColumn.get("JID")){
					responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblJobId", locale),sortOrderNoField,"jobId",sortOrderTypeVal,pgNo);
					tmRecords.append("<th valign='top'>"+responseText+"</th>");
			    }
				
				if(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==2)
		        {           
		          if(hrIntegratedFlag && userMaster.getEntityType() == 2)
		          {
		                responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("msgReq", locale)+"<br>"+Utility.getLocaleValuePropByKey("msgPositions", locale)+" #",sortOrderFieldName,"districtRequisitionNo",sortOrderTypeVal,pgNo);
		              tmRecords.append("<th  valign='top'>"+responseText+"</th>");
		          }
		          else if(userMaster.getEntityType() == 1)
		          {
		                responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("msgReq", locale)+"<br>"+Utility.getLocaleValuePropByKey("msgPositions", locale)+" #",sortOrderFieldName,"districtRequisitionNo",sortOrderTypeVal,pgNo);
		                    tmRecords.append("<th  valign='top'>"+responseText+"</th>");
		          }
		        }

				if(userMaster.getDistrictId()==null || displayCNFGColumn.get("TIT")){
					responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblTitle", locale),sortOrderFieldName,"jobTitle",sortOrderTypeVal,pgNo);
					tmRecords.append("<th  valign='top'>"+responseText+"</th>");
				}
				// zone added by mukesh
				//if(userMaster.getDistrictId()!=null && userMaster.getDistrictId().getDistrictId()==804800)
				if(userMaster.getDistrictId()==null || displayCNFGColumn.get("SCH")){
					responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblSchool", locale),"schoolName","schoolName",sortOrderTypeVal,pgNo);
					tmRecords.append("<th  valign='top'>"+responseText+"</th>");
				}
				//else
				if(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==2)
				{
				
				}
				else 
				{
					if(userMaster.getDistrictId()==null || displayCNFGColumn.get("ZON")){
						responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblZone", locale),sortOrderFieldName,"geoZoneMaster",sortOrderTypeVal,pgNo);
						tmRecords.append("<th  valign='top'>"+responseText+"</th>");
					}
					
					if(userMaster.getDistrictId()==null || displayCNFGColumn.get("SUB")){
						responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblSub", locale),sortOrderFieldName,"subjectMaster",sortOrderTypeVal,pgNo);
						tmRecords.append("<th  valign='top'>"+responseText+"</th>");
					}
				}
				
				if(userMaster.getDistrictId()==null || displayCNFGColumn.get("STA")){
					responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("optStatus", locale),sortOrderFieldName,"status",sortOrderTypeVal,pgNo);
					tmRecords.append("<th valign='top'>"+responseText+"</th>");
				}

				//responseText=PaginationAndSorting.responseSortingLink("Activation Date",sortOrderFieldName,"jobStartDate",sortOrderTypeVal,pgNo);
				//tmRecords.append("<th width='14%' valign='top'>"+responseText+"</th>");
				
				if(userMaster.getDistrictId()==null || displayCNFGColumn.get("POSU")){
					responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblPostedUntil", locale),sortOrderFieldName,"jobEndDate",sortOrderTypeVal,pgNo);
					tmRecords.append("<th  valign='top'>"+responseText+"</th>");
				}

		//sandeep 04-08-15
			//	tmRecords.append("<th  valign='top'>Positions</th>");
				if(userMaster.getDistrictId()==null || displayCNFGColumn.get("POS")){
					tmRecords.append("<th  valign='top'>"+Utility.getLocaleValuePropByKey("msgPositions", locale)+"</th>");
				}
						
				if(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==2)
				{
				if(userMaster.getEntityType() == 2 && hrIntegratedFlag)
				  tmRecords.append("<th  valign='top'>"+Utility.getLocaleValuePropByKey("lblHrStatus", locale)+"</th>");
				else if(userMaster.getEntityType() == 1)
					tmRecords.append("<th  valign='top'>"+Utility.getLocaleValuePropByKey("lblHrStatus", locale)+"</th>");
				}
				
				if(userMaster.getDistrictId()==null || displayCNFGColumn.get("APP")){
					tmRecords.append("<th  valign='top'>"+Utility.getLocaleValuePropByKey("lblApplicants", locale)+" <a href='#' id='iconpophover1' class='net-header-text ' rel='tooltip' data-original-title='"+Utility.getLocaleValuePropByKey("msgTotalnumberofapplicantsappliedforthisjob2", locale)+"'><span class='icon-question-sign'></span></a></th>");
				}
				
				if(userMaster.getDistrictId()==null || displayCNFGColumn.get("HPER")){
						tmRecords.append("<th  valign='top'>"+Utility.getLocaleValuePropByKey("lblHires", locale)+"</th>");
				}
				
				if(userMaster.getDistrictId()==null || displayCNFGColumn.get("CANG")){
					tmRecords.append("<th  valign='top'>"+Utility.getLocaleValuePropByKey("lblCandidateGrid", locale)+"</th>");
				}
				
				if(userMaster.getDistrictId()==null || displayCNFGColumn.get("ATT")){
					tmRecords.append("<th  valign='top'>"+Utility.getLocaleValuePropByKey("lblDistrictAttachment", locale)+"</th>");
				}
				
				if(userMaster.getDistrictId()==null || displayCNFGColumn.get("ACT")){
					tmRecords.append(" <th   valign='top'>"+Utility.getLocaleValuePropByKey("lblAct", locale)+"</th>");
				}
				

				tmRecords.append("</tr>");
				tmRecords.append("</thead>");

				if(resultFlag==false){
					totalRecord=0;
				}
				
				/*================= Checking If Record Not Found ======================*/
		  
				/********** Start :: For get Number of awailable candidates ****** */
				
			/*	List <StatusMaster> statusMasterLst=WorkThreadServlet.statusMasters;
				Map<String, StatusMaster> mapStatus = new HashMap<String, StatusMaster>();
				for (StatusMaster sMaster : statusMasterLst) {
					mapStatus.put(sMaster.getStatusShortName(),sMaster);
				}
				
				List<JobOrder> jobLst= new ArrayList<JobOrder>();
				List<TeacherDetail> teacherList = new ArrayList<TeacherDetail>();
				
				//Get List of teacher and joborder
				for(int i=0;i<jobForTeacherList.size();i++){
					jobLst.add(jobForTeacherList.get(i).getJobId());
					teacherList.add(jobForTeacherList.get(i).getTeacherId());
				}
				
				TeacherAssessmentStatus teacherAssessmentStatusJSI = null;
				Map<Integer, Integer> mapAssess = new HashMap<Integer, Integer>();
				Map<Integer, TeacherAssessmentStatus> mapJSI = new HashMap<Integer, TeacherAssessmentStatus>();
				List<AssessmentJobRelation> assessmentJobRelations1=assessmentJobRelationDAO.findRelationByJobOrder(jobLst);
				List<AssessmentDetail> adList=new ArrayList<AssessmentDetail>();
				
				Map<Integer,InternalTransferCandidates> mapLstITRA = new HashMap<Integer, InternalTransferCandidates>();
				Map<Integer,TeacherPortfolioStatus> maptPortfolioStatus = new HashMap<Integer, TeacherPortfolioStatus>();
				//Map<Integer,TeacherAssessmentStatus> maptBaseAssmentStatus = new HashMap<Integer, TeacherAssessmentStatus>();
				List<TeacherAssessmentStatus> lstJSI =new ArrayList<TeacherAssessmentStatus>(); 

				mapLstITRA 				= internalTransferCandidatesDAO.getDistrictForTeacherShowSatus(teacherList);
				maptPortfolioStatus 	= teacherPortfolioStatusDAO.findPortfolioStatusByTeacherlist(teacherList);
				//maptBaseAssmentStatus 	= teacherAssessmentStatusDAO.findAssessmentStatusByTeacherListForBase(teacherList);
				
				if(assessmentJobRelations1.size()>0){
					for(AssessmentJobRelation ajr: assessmentJobRelations1){
						mapAssess.put(ajr.getJobId().getJobId(),ajr.getAssessmentId().getAssessmentId());
						adList.add(ajr.getAssessmentId());
					}
					if(adList.size()>0)
						lstJSI = teacherAssessmentStatusDAO.findJSITakenByAssessMentListAndTeacherList(teacherList,adList);
				}

				for(TeacherAssessmentStatus ts : lstJSI)
				{
					mapJSI.put(ts.getAssessmentDetail().getAssessmentId(), ts);
				}
		*/
				/********** End :: For get Number of awailable candidates ****** */
				//Approval authority   
				List<UserMaster> listUserMasters =new ArrayList<UserMaster>();
				List<JobApprovalHistory> listJobApprovalHistories =new ArrayList<JobApprovalHistory>();
				Map<Integer,List<UserMaster>> mapofApprovedUsers=new HashMap<Integer, List<UserMaster>>();
				List<UserMaster> tempUserList=null;
				UserMaster user = null;
				DistrictKeyContact userKeyContact=null;
				Integer userId = userMaster.getUserId();
				List<JobRequisitionNumbers> jobRequisitionNumbers=new ArrayList<JobRequisitionNumbers>();
				
				if(userMaster.getEntityType()!=1)
				{
					List<DistrictKeyContact> districtKeyContactList = new ArrayList<DistrictKeyContact>();
					districtKeyContactList = districtKeyContactDAO.findByContactType(districtMaster,"Job Approval");
					
					for (DistrictKeyContact districtKeyContact : districtKeyContactList) {
						listUserMasters.add(districtKeyContact.getUserMaster());
					//	System.out.println("districtKeyContact.getUserMaster().getUserId():: "+districtKeyContact.getUserMaster().getUserId());
						if(districtKeyContact.getUserMaster().getUserId().equals(userId))
						{
							user = districtKeyContact.getUserMaster();
							userKeyContact = districtKeyContact;
						}
					}
					
					System.out.println("userId::: "+userId+" ::::::::::::::::::::::::::::::::::::::::::: user:- "+user+" :::::::::::::::::::::::::::: userKeyContact:- "+userKeyContact);

				}
				if(user!=null)
				if(listUserMasters!=null && listUserMasters.size()>0){
					listJobApprovalHistories= jobApprovalHistoryDAO.findHistoryByUser(listUserMasters);
					if(listJobApprovalHistories!=null && listJobApprovalHistories.size()>0){
						for (JobApprovalHistory jobApprovalHistory : listJobApprovalHistories) {
							tempUserList=null;
							if(mapofApprovedUsers.get(jobApprovalHistory.getJobOrder().getJobId())!=null){
								tempUserList=mapofApprovedUsers.get(jobApprovalHistory.getJobOrder().getJobId());
								tempUserList.add(jobApprovalHistory.getUserMaster());
								//putting again in Map
								mapofApprovedUsers.put(jobApprovalHistory.getJobOrder().getJobId(), tempUserList);
								
							}else{
								tempUserList=new ArrayList<UserMaster>();
								tempUserList.add(jobApprovalHistory.getUserMaster());
								mapofApprovedUsers.put(jobApprovalHistory.getJobOrder().getJobId(), tempUserList);
							}
						}
					}
				}
				////////////////////////////////////
				boolean miamiShowFlag=true;
				try{
					if(userMaster.getDistrictId().getDistrictId()==1200390 && userMaster.getRoleId().getRoleId()==3 && userMaster.getEntityType()==3){
						miamiShowFlag=false;
					}
				}catch(Exception e){}	
				String windowFunc="if(this.href!='javascript:void(0);')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
				String fileName 	= 	"";
				String filePath		=	"";
				Integer counter = 1;
				int count=0;
				//System.out.println("lstJobOrder size=="+lstJobOrder.size());
				
				if(resultFlag){
					if(lstJobOrder!=null && lstJobOrder.size()==0)
						tmRecords.append("<tr><td colspan='10' align='center'>"+Utility.getLocaleValuePropByKey("lblNoRecord", locale)+"</td></tr>" );
					
					if(jsonObject!=null)
					{
							//System.out.println("total districtjobOrder=="+((JSONObject)jsonObject.get("hits")).get("total"));
						    totalRecord=((JSONObject)jsonObject.get("hits")).getInt("total");
						    JSONArray hits= (JSONArray) ((JSONObject)jsonObject.get("hits")).get("hits");
						    //System.out.println("hits size===="+hits.size());				
						    Map<Integer, String> jobLabelText = new HashMap<Integer, String>();
						    Map<Integer, String> jobUserLabelText = new HashMap<Integer, String>();
						    Map<String, String> hireCandidatePercentageMap = new HashMap<String, String>();
						    Map<String, String> positionMap = new HashMap<String, String>();
						    Map<String, String> positionNumber = new HashMap<String, String>();
						    
						    try
						    {
						    	List<Integer> jobIds = new ArrayList<Integer>();
						    	for(JobOrder jobOrder  : lstJobOrder)
						    		jobIds.add(jobOrder.getJobId());
						    	
						    	if(jobIds!=null && jobIds.size()>0)
						    	{
						    		//System.out.println(jobIds);
						    		Criterion criterion = Restrictions.in("jobId", jobIds);
						    		lstJobOrder = jobOrderDAO.findByCriteria(criterion);
						    	}
						    }
						    catch(Exception e)
						    {
						    	e.printStackTrace();
						    }
						    
							//jobLabelText = getJobLevelText(entityID, userKeyContact, lstJobOrder );
							if(userMaster.getDistrictId()!=null && userMaster.getDistrictId().getDistrictId()==806900)
							{
								jobLabelText=getUserJobLevelTextApprovalGroupForAdams(entityID, userMaster, lstJobOrder ,userKeyContact);
							}
							else
							{
								jobLabelText = getJobLevelText(entityID, userKeyContact, lstJobOrder );
								//jobUserLabelText=getUserJobLevelText(entityID, userMaster, lstJobOrder );
							}
							jobUserLabelText=getUserJobLevelText(entityID, userMaster, lstJobOrder );
							positionMap = getPositionsNumber(lstJobOrder);
							if(userMaster.getDistrictId()==null || displayCNFGColumn.get("HPER")){
								hireCandidatePercentageMap = getHireCandidatePercentage(lstJobOrder,positionMap);
							}
							System.out.println(jobLabelText);
							System.out.println(jobUserLabelText);
							System.out.println("mapofApprovedUsers:- "+mapofApprovedUsers);
							
						//for (JobOrder jobOrderDetails : lstJobOrder) {
						Map<Integer, JobOrder> jobOrderMap = new HashMap<Integer, JobOrder>();
						if(lstJobOrder!=null && lstJobOrder.size()>0)
							for(JobOrder jobOrder : lstJobOrder)
								jobOrderMap.put(jobOrder.getJobId(), jobOrder);
						
						Map<Integer, JobRequisitionNumbers> hrStatusMap = new HashMap<Integer, JobRequisitionNumbers>();
						if(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==2)
						{				
						jobRequisitionNumbers=jobRequisitionNumbersDAO.getHRStatusByJobId(lstJobOrder);
						System.out.println("jobRequisitionNumbers size:::::"+jobRequisitionNumbers.size());
						if(jobRequisitionNumbers!=null && jobRequisitionNumbers.size()>0)
						{
							for(JobRequisitionNumbers hrstatus: jobRequisitionNumbers)
							{
								System.out.println("hrstatus.getJobOrder().getJobId():::"+hrstatus.getJobOrder().getJobId());
								hrStatusMap.put(hrstatus.getJobOrder().getJobId(), hrstatus);
							
							}
						}
						}
							
						
						// Show Approval Group Details
						int showApprovalFlag=0;	
						if(userMaster.getDistrictId()!=null && userMaster.getDistrictId().getShowApprovalGropus()!=null)
						if((userMaster.getEntityType()==2||userMaster.getEntityType()==3)&&(userMaster.getDistrictId().getShowApprovalGropus().equals(3)||userMaster.getDistrictId().getShowApprovalGropus().equals(4) )){
							if( userMaster.getDistrictId().getDistrictId().equals(806900))
							showApprovalFlag=checkUserExistOrNotInDistrictApprovalGroups(userKeyContact);
						}
						
						
						
						Map<Integer,String> zoneList=new HashMap<Integer,String>();
						
						List<TimeZoneMaster>tmd=null;
						tmd=timeZoneMasterDAO.findAll();
						for(TimeZoneMaster tj: tmd)
						{
							zoneList.put(tj.getTimeZoneId(),tj.getTimeZoneShortName());
						}
						//shreeram done
						for(int i=0; i<hits.size(); i++)
						 {
							jsonObject=(JSONObject)hits.get(i);
							String index=(String)jsonObject.get("_index");
							String type=(String)jsonObject.get("_type");
							String id=(String)jsonObject.get("_id");
							jsonObject=(JSONObject)jsonObject.get("_source");
							JobOrder jobOrder = jobOrderMap.get(jsonObject.getInt("jobId"));
							count++;
							noOfRecordCheck++;   
							int jobForTeacherSize=0;
							int jftAvailableCandidates=0;
							int jobForHiresSize=0;
							
							//11111111111
						/*	int totalNoOfCGViewCandidate =	0;	
							totalNoOfCGViewCandidate =	getTotalCountOfCGView(mapTSHFJob,""+jobOrderDetails.getJobId(),jobCategoryMasterList,jobForTeacherList,statusMasterList,jobOrderList,teacherAssessmentStatusList,userMaster);
						*/	tmRecords.append("<tr>" );
							if(userMaster.getDistrictId()==null || displayCNFGColumn.get("JID")){
								tmRecords.append("<td nowrap >"+jsonObject.get("jobId")+"</td>");
							}
							if(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==2)						
							{
								if(userMaster.getEntityType() == 2 && hrIntegratedFlag){
									if(hrStatusMap.containsKey(jobOrder.getJobId()))
									{							
									tmRecords.append("<td>"+hrStatusMap.get(jobOrder.getJobId()).getDistrictRequisitionNumbers().getRequisitionNumber()+"</td>");
									}
									else
									{
										tmRecords.append("<td>"+Utility.getLocaleValuePropByKey("optN/A", locale)+"</td>");	
									}
								}else if(userMaster.getEntityType() == 1){
									if(hrStatusMap.containsKey(jobOrder.getJobId()))
									{							
									tmRecords.append("<td>"+hrStatusMap.get(jobOrder.getJobId()).getDistrictRequisitionNumbers().getRequisitionNumber()+"</td>");
									}
									else
									{
										tmRecords.append("<td>"+Utility.getLocaleValuePropByKey("optN/A", locale)+"</td>");	
									}
									
								}
							}
							
							if(userMaster.getDistrictId()==null || displayCNFGColumn.get("TIT")){
								if(jsonObject.getInt("isPoolJob")==2)
								{
									tmRecords.append("<td><span class='requiredlarge'>*</span>"+jsonObject.get("jobTitle")+"</td>");
								}
								else
								{
									tmRecords.append("<td>"+jsonObject.get("jobTitle")+"</td>");
								}
							}
							
							
							/*if(jobOrder.getDistrictMaster()!=null 
									&& jobOrder.getSchool()!=null && jobOrder.getSchool().size()>0)
							{
								//tmRecords.append("<td>"+jobOrder.getSchool().get(0).getSchoolName()+"</td>");
							}*/
							if(userMaster.getDistrictId()==null || displayCNFGColumn.get("SCH")){
								if(jobOrder!=null && jobOrder.getSchool()!=null && jobOrder.getSchool().size()>1)
								{
			
									String schoolListIds = jobOrder.getSchool().get(0).getSchoolId().toString();
									for(int k=1;k<jobOrder.getSchool().size();k++)
									{
										schoolListIds = schoolListIds+","+jobOrder.getSchool().get(k).getSchoolId().toString();
									}
									tmRecords.append("<td> <a href='javascript:void(0);' onclick=showSchoolList('"+schoolListIds+"')>Many Schools</a></td>");
									
								}
								else if(jobOrder!=null && jobOrder.getSchool().size()==1)
								{
									//jeffcoSpecificSchool=jobOrder.getSchool().get(0).getSchoolName()+"<br>"+jobOrder.getSchool().get(0).getAddress();
									tmRecords.append("<td>"+jobOrder.getSchool().get(0).getSchoolName()+"</td>");
								}
								else
									tmRecords.append("<td></td>");
							}
							//else
							if(userMaster.getHeadQuarterMaster()!=null)
							{
							
							}
							else
							{
							
								if(userMaster.getDistrictId()==null || displayCNFGColumn.get("ZON")){
									if(!jsonObject.getString("geoZoneName").trim().equals("")){
										if(jsonObject.getInt("geoZoneDistrictId")==4218990)
										{
											tmRecords.append("<td><a href='javascript:void(0);' onclick=\"showPhiladelphiaPdf();\" return false;>"+jsonObject.getString("geoZoneName")+"</a></td>");
										}
										else if(jsonObject.getInt("geoZoneDistrictId")==1200390){
											tmRecords.append("<td><a href='http://jobs.dadeschools.net/teachers/pdf/Geo_Zones_With_Schools.pdf' target='_blank'>"+jsonObject.getString("geoZoneName")+"</a></td>");
										}else{
											tmRecords.append("<td><a href='javascript:void(0);' onclick=\"return showZoneSchoolList("+jsonObject.getInt("geoZoneId")+",'"+jsonObject.getString("geoZoneName")+"',"+jsonObject.getString("districtId")+")\";>"+jsonObject.getString("geoZoneName")+"</a></td>");
										}
									} else {
										tmRecords.append("<td></td>");
									}
								}
								
								if(userMaster.getDistrictId()==null || displayCNFGColumn.get("SUB")){
									if(!jsonObject.getString("subjectName").trim().equals("")){
										tmRecords.append("<td>"+jsonObject.getString("subjectName")+"</td>");
									}else{
										tmRecords.append("<td></td>");
									}
								}
							}
							
							if(userMaster.getDistrictId()==null || displayCNFGColumn.get("STA")){
								tmRecords.append("<td>");
								if(jsonObject.getString("status").equalsIgnoreCase("A"))
									tmRecords.append("Active ");
								else if(jsonObject.getString("status").equalsIgnoreCase("I"))
									tmRecords.append("Inactive");
								else
									tmRecords.append("Archived");
								tmRecords.append("</td>");
							}
								String time=null;
								String zone=null;
							if(userMaster.getDistrictId()==null || displayCNFGColumn.get("POSU")){
								SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
								Date endDate=sdf.parse(jsonObject.getString("jobEndDate"));
								if(Utility.convertDateAndTimeToUSformatOnlyDate(endDate).equals("Dec 25, 2099")){
									tmRecords.append("<td>Until filled</td>");
								/*{   
									if(jobOrder.getJobEndTime()!=null){
										zone=zoneList.get(jobOrder.getJobTimeZoneId())!=null?zoneList.get(jobOrder.getJobTimeZoneId()):", 11:59 PM CST";
										tmRecords.append("<td>Until filled "+jobOrder.getJobEndTime()+" "+zone+"</td>");
									}else{
										if(jobOrder.getDistrictMaster().getPostingEndTime()!=null){
											zone=zoneList.get(Integer.parseInt(jobOrder.getDistrictMaster().getTimezone()))!=null?zoneList.get(Integer.parseInt(jobOrder.getDistrictMaster().getTimezone())):",11:59 PM CST";
											tmRecords.append("<td>Until filled "+jobOrder.getDistrictMaster().getPostingEndTime()+" "+zone+"</td>");
										}else{
											tmRecords.append("<td>Until filled, 11:59 PM CST</td>");
										}
									}
								}*/
								}else if(jobOrder!=null){
									if(jobOrder.getJobTimeZoneId()!=null){
										time=jobOrder.getJobEndTime()!=null?jobOrder.getJobEndTime():", 11:59 PM ";
										tmRecords.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(endDate)+" "+(time!=null?(", "+time):" ")+" "+(zoneList.get(jobOrder.getJobTimeZoneId())!=null?(zoneList.get(jobOrder.getJobTimeZoneId())):" CST")+"</td>");
									}else{
										if(jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getTimezone()!=null){
											time=/*jobOrder.getDistrictMaster().getPostingEndTime()!=null?jobOrder.getDistrictMaster().getPostingEndTime():*/", 11:59 PM ";
											tmRecords.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(endDate)+" "+(time!=null?(", "+time):" ")+" "+(zoneList.get(Integer.parseInt(jobOrder.getDistrictMaster().getTimezone()))!=null?(zoneList.get(Integer.parseInt(jobOrder.getDistrictMaster().getTimezone()))):" CST")+"</td>");
										 }else
											 tmRecords.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(endDate)+", 11:59 PM CST</td>");
									}
								}
							}
							
							if(userMaster.getDistrictId()==null || displayCNFGColumn.get("POS")){
								if(jobOrder!=null){
									tmRecords.append("<td>"+(positionMap.get(""+jobOrder.getJobId()))+"</td>");
								}else{
									tmRecords.append("<td>&nbsp;</td>");
								}
							}
								
							if(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==2)						
							{									
								if(userMaster.getEntityType() == 2 && hrIntegratedFlag){
									if(hrStatusMap.containsKey(jobOrder.getJobId()))
									{				
										String valueStatus="";
										String statusValue=hrStatusMap.get(jobOrder.getJobId()).getDistrictRequisitionNumbers().getPostionStatus();
										if(statusValue!=null)
										{
										if(statusValue.equals("51"))
										{
											valueStatus="Vacant";
										}else if(statusValue.equals("01"))
										{
											valueStatus="Staffed � Fully";
										}else if(statusValue.equals("02"))
										{
											valueStatus="Staffed � Partially";
										}else if(statusValue.equals("60"))
										{
											valueStatus="Frozen";
										}else if(statusValue.equals("00"))
										{
											valueStatus="Inactive Position";
										}else if(statusValue.equals("PV"))
										{
											valueStatus="Pending Vacant";
										}
										}
									   tmRecords.append("<td>"+valueStatus+"</td>");
									
									}
									else
									{
										tmRecords.append("<td>"+Utility.getLocaleValuePropByKey("optN/A", locale)+"</td>");	
									}
								}else if(userMaster.getEntityType() == 1){
									if(hrStatusMap.containsKey(jobOrder.getJobId()))
									{				
										String valueStatus="";
										String statusValue=hrStatusMap.get(jobOrder.getJobId()).getDistrictRequisitionNumbers().getPostionStatus();
										if(statusValue!=null)
										{
										if(statusValue.equals("51"))
										{
											valueStatus="Vacant";
										}else if(statusValue.equals("01"))
										{
											valueStatus="Staffed � Fully";
										}else if(statusValue.equals("02"))
										{
											valueStatus="Staffed � Partially";
										}else if(statusValue.equals("60"))
										{
											valueStatus="Frozen";
										}else if(statusValue.equals("00"))
										{
											valueStatus="Inactive Position";
										}else if(statusValue.equals("PV"))
										{
											valueStatus="Pending Vacant";
										}
										}
									   tmRecords.append("<td>"+valueStatus+"</td>");
									
									}
									else
									{
										tmRecords.append("<td>"+Utility.getLocaleValuePropByKey("optN/A", locale)+"</td>");	
									}
								}
							}
							/*try{
								jobForTeacherSize=findJFTApplicantbyORHideJobOrder(mapTSHFJob,jobForTeacherList,jobOrderDetails,statusMaster,1,userMaster);
								jftAvailableCandidates = findJFTApplicantAvailableCandidates(mapTSHFJob,jobForTeacherList,jobOrderDetails,statusMaster,1,userMaster,mapAssess,teacherAssessmentStatusJSI,mapJSI,mapStatus,maptPortfolioStatus,mapLstITRA);
								if(jobForTeacherSize!=0){
									tmRecords.append("<td>"+jobForTeacherSize+"/"+jftAvailableCandidates+"</td>");
								}else{
									tmRecords.append("<td>0/0</td>");
								}
							}catch(Exception e){
								e.printStackTrace();
								tmRecords.append("<td >0</td>");
							}
							try{
								jobForHiresSize=findJFTApplicantbyORHideJobOrder(mapTSHFJob,jobForTeacherList,jobOrderDetails,statusMasterHird,2,userMaster);
								if(jobForHiresSize!=0){
									tmRecords.append("<td>"+jobForHiresSize+"</td>");
								}else{
									tmRecords.append("<td>0</td>");
								}
							}catch(Exception e){
								e.printStackTrace();
								tmRecords.append("<td>0</td>");
							}*/
							//Total/available/hired
							
		//start sandeep 04-08-15					
							
			/*				if(jobOrder != null)
							    tmRecords.append("<td>"+(jobOrder.getNoOfHires()!=null?jobOrder.getNoOfHires():"")+"</td>");
							else
								tmRecords.append("<td>0</td>");
		<<<<<<< ManageJobOrdersAjax.java
		//end					

		=======

							

		>>>>>>> 1.359
							String appliedStr = null;
							appliedStr = map.get(jsonObject.getInt("jobId"));
							
							 if(appliedStr!=null){
								     tmRecords.append("<td>"+appliedStr.split("##")[0]+"/"+appliedStr.split("##")[1]+"</td>");
								 
									 if(!appliedStr.split("##")[2].equals("0") && jobOrder!=null){
										 int hirePercentage =0;
										 try{
										 hirePercentage = (Integer.parseInt(appliedStr.split("##")[2]!=null?appliedStr.split("##")[2]:"0")/jobOrder.getNoOfHires())*100;
										 }catch(NumberFormatException e){e.printStackTrace();}
										 tmRecords.append("<td>"+hirePercentage+"%</td>");
									      
									 }else
									 tmRecords.append("<td>0%</td>");
							 }else
							 {
								 
								 tmRecords.append("<td>0/0</td>");
								 tmRecords.append("<td>0%</td>");
							 }
		 */				
							 
							String appliedStr = null;
						    appliedStr = map.get(jsonObject.getInt("jobId"));
							if(userMaster.getDistrictId()==null || displayCNFGColumn.get("APP")){
							      if(appliedStr!=null){
							       tmRecords.append("<td>"+appliedStr.split("##")[0]+"/"+appliedStr.split("##")[1]+"</td>");
							       //tmRecords.append("<td >"+appliedStr.split("##")[2]+"</td>");
							      }else
							      {
							       tmRecords.append("<td>0/0</td>");
							       //tmRecords.append("<td>0</td>");
							      }
							}
							
							if(userMaster.getDistrictId()==null || displayCNFGColumn.get("HPER")){
						    	  tmRecords.append("<td>"+hireCandidatePercentageMap.get(jsonObject.get("jobId").toString())+"</td>");
							}
							
						    if(userMaster.getDistrictId()==null || displayCNFGColumn.get("CANG")){
						    	tmRecords.append("<td style='text-align:center'>&nbsp;<a data-original-title='CG View' rel='tooltip' id='cgn"+noOfRecordCheck+"' target=\"_blank\" href='candidategrid.do?jobId="+jsonObject.getInt("jobId")+"&JobOrderType="+JobOrderType+"'><span class='icon-table icon-large'></span></a></td>");
						    }
							
							
							if(userMaster.getDistrictId()==null || displayCNFGColumn.get("ATT")){
								filePath	=	jsonObject.getString("districtId")+"/DistAttachmentFile";
								fileName 	=	jsonObject.getString("districtAttachment");
								//filePath=Utility.getValueOfPropByKey("districtRootPath")+jobOrderDetails.getDistrictMaster().getDistrictId()+"/DistAttachmentFile";
								
								if(!fileName.equals("")){
									tmRecords.append("<td style='text-align:center'>");
									tmRecords.append("<a href='javascript:void(0);' rel='tooltip' data-original-title="+fileName+" class='attchToolTip"+noOfRecordCheck+"' id='refNotesfileopen"+counter+"' onclick=\"downloadAttachment('"+filePath+"','"+fileName+"');"+windowFunc+"\"><span class='icon-download-alt icon-large iconcolorBlue'></span></a>");
									tmRecords.append("</td>");
								} else {
									tmRecords.append("<td>&nbsp;</td>");
								}
							}
			
							
							boolean jeffcoSAsEditDJO=false;
							if(userMaster.getDistrictId()!=null && userMaster.getDistrictId().getDistrictId().equals(804800)){//&& userMaster.getUserId().equals(jobOrder.getCreatedBy()) && userMaster.getDistrictId().getsACreateDistJOb()!=null && userMaster.getDistrictId().getsACreateDistJOb()
								jeffcoSAsEditDJO=true;
							}
							if(userMaster.getDistrictId()==null || displayCNFGColumn.get("ACT")){
								if(entityID==3 && JobOrderType==2){
									if((roleAccess.indexOf("|2|")!=-1 && miamiShowFlag) || jeffcoSAsEditDJO){
										tmRecords.append("<td nowrap><a title="+Utility.getLocaleValuePropByKey("msgEditicon", locale)+" href='editjoborder.do?JobOrderType="+JobOrderType+"&JobId="+jsonObject.getInt("jobId")+"&districtId="+jsonObject.getString("districtId")+"' style=\"vertical-align:middle;\"><i class='fa fa-pencil-square-o fa-lg' /></a>");
										if(jeffcoSAsEditDJO){
											if(jsonObject.getString("status").equalsIgnoreCase("A"))
												tmRecords.append("&nbsp;|<a title="+Utility.getLocaleValuePropByKey("lblDeactivate", locale)+"  href='javascript:void(0);' onclick=\"return activateDeactivateJob("+entityID+","+jsonObject.getInt("jobId")+",'I')\" style=\"vertical-align:middle;\"><i class='fa fa-times fa-lg' /></a>");
											else
												tmRecords.append("&nbsp;|<a title="+Utility.getLocaleValuePropByKey("msgActivateicon", locale)+" href='javascript:void(0);' onclick=\"return activateDeactivateJob("+entityID+","+jsonObject.getInt("jobId")+",'A')\" style=\"vertical-align:middle;\"><i class='fa fa-check fa-lg' /></a>");
										}
									}else if(roleAccess.indexOf("|4|")!=-1 || miamiShowFlag==false){
										tmRecords.append("<td nowrap><a href='editjoborder.do?JobOrderType="+JobOrderType+"&JobId="+jsonObject.getInt("jobId")+"&districtId="+jsonObject.getString("districtId")+"'>View</a>");
										if(userMaster.getDistrictId()!=null && userMaster.getDistrictId().getDistrictId().equals(806900) && entityID==3){
											if(userMaster.getDistrictId().getShowApprovalGropus()!=null)
											if(userMaster.getDistrictId().getShowApprovalGropus().equals(2)){
												tmRecords.append("<a onclick='getapprovalgroupdetails("+JobOrderType+","+jsonObject.getInt("jobId")+","+jsonObject.getString("districtId")+")' href='#' javascript:void(0);>&nbsp;|&nbsp;Job Approval Details</a>");
											} else if(userMaster.getDistrictId().getShowApprovalGropus().equals(4) && showApprovalFlag==1){
												tmRecords.append("<a onclick='getapprovalgroupdetails("+JobOrderType+","+jsonObject.getInt("jobId")+","+jsonObject.getString("districtId")+")' href='#' javascript:void(0);>&nbsp;|&nbsp;Job Approval Details</a>");
											}
											String text = jobLabelText.get(jsonObject.getInt("jobId"));
											if(text!=null)
											tmRecords.append(text);
										}
									}
									
									String text = jobUserLabelText.get(jsonObject.getInt("jobId")); //ElasticSearchCode
									if(text!=null)
										tmRecords.append(text);
									
									if(jsonObject.getBoolean("IsInviteOnly")){
										tmRecords.append("&nbsp;|<a  href='importcandidatedetails.do?jobId="+jsonObject.getInt("jobId")+"' rel='tooltip' id='exelexport"+count+"' data-original-title='Upload Candidates against this job'><i class='icon-upload' style='font-size: 1.5em;'></i></a>");
										tmRecords.append("<script>$('#exelexport"+count+"').tooltip();</script>");
									}
									tmRecords.append(" </td>");
									
								}else{
									boolean pipeFlag=false;
									if(roleAccess.indexOf("|2|")!=-1 && miamiShowFlag && !jsonObject.getString("status").equalsIgnoreCase("R")){						      
										if(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==2 && jobOrder.getStatus().equalsIgnoreCase("I"))						
										{
											List<JobForTeacher> listJobForTeacher= new ArrayList<JobForTeacher>();
											listJobForTeacher=jobForTeacherDAO.findJFTHiresByJobOrder(jobOrder);
									     	if(appliedStr!=null && listJobForTeacher.size()>0)
									    	{									
									        	tmRecords.append("<td nowrap><a href='#' data-original-title='This job order has candidates associated and cannot be edited.' rel='tooltip' id='edit"+noOfRecordCheck+"' onclick='return false;'><i class='fa fa-pencil-square-o fa-lg iconcolorhover' /></a>");
											}
											else
											{
												tmRecords.append("<td nowrap><a title="+Utility.getLocaleValuePropByKey("msgEditicon", locale)+" href='editjoborder.do?JobOrderType="+JobOrderType+"&JobId="+jsonObject.getInt("jobId")+"&districtId="+jsonObject.getString("districtId")+"'><i class='fa fa-pencil-square-o fa-lg' /></a>");
											}
										}
										else
										{
											tmRecords.append("<td nowrap><a title="+Utility.getLocaleValuePropByKey("msgEditicon", locale)+" href='editjoborder.do?JobOrderType="+JobOrderType+"&JobId="+jsonObject.getInt("jobId")+"&districtId="+jsonObject.getString("districtId")+"'><i class='fa fa-pencil-square-o fa-lg' /></a>");	
										}
										tmRecords.append("<a title="+Utility.getLocaleValuePropByKey("msgClone", locale)+" href='clonejoborder.do?JobOrderType="+JobOrderType+"&JobId="+jsonObject.getInt("jobId")+"&districtId="+jsonObject.getString("districtId")+"'> |<span class='icon-copy fa-lg iconcolorBlue'></span></a>&nbsp;|");
										//tmRecords.append("<a href='javascript:void(0);' onclick='return showHistory("+1+")'><i class='fa fa-history fa-lg'/> |<span class='icon-copy fa-lg iconcolorBlue'></span></a>");
			/*sandeep*/					tmRecords.append("<a title='History' href='javascript:void(0);' onclick='return showEditJobHistory("+jsonObject.getInt("jobId")+")'><i class='fa fa-history fa-lg'/></a>");
										pipeFlag=true;
									}else if(roleAccess.indexOf("|4|")!=-1 || miamiShowFlag==false ){	
										if(!jsonObject.getString("status").equalsIgnoreCase("R"))
										tmRecords.append("<td nowrap><a title="+Utility.getLocaleValuePropByKey("msgEditicon", locale)+" href='editjoborder.do?JobOrderType="+JobOrderType+"&JobId="+jsonObject.getInt("jobId")+"&districtId="+jsonObject.getString("districtId")+"'><i class='fa fa-pencil-square-o fa-lg' /></a>");
										pipeFlag=true;
										
									}
									
									if(roleAccess.indexOf("|7|")!=-1 && miamiShowFlag && !jsonObject.getString("status").equalsIgnoreCase("R")){
										if(pipeFlag)tmRecords.append(" | ");
										if(jsonObject.getString("status").equalsIgnoreCase("A"))
											tmRecords.append("<a title="+Utility.getLocaleValuePropByKey("lblDeactivate", locale)+"  href='javascript:void(0);' onclick=\"return activateDeactivateJob("+entityID+","+jsonObject.getInt("jobId")+",'I')\"><i class='fa fa-times fa-lg' /></a>");
										else
											tmRecords.append("<a title="+Utility.getLocaleValuePropByKey("msgActivateicon", locale)+" href='javascript:void(0);' onclick=\"return activateDeactivateJob("+entityID+","+jsonObject.getInt("jobId")+",'A')\"><i class='fa fa-check fa-lg' /></a>");
									}else{
										if(jsonObject.getString("status").equalsIgnoreCase("R"))
											tmRecords.append("<td>");
										tmRecords.append("&nbsp;");
									}
									tmRecords.append("<br>");
								//	System.out.println("::::::::::::::::::::::::::::::::   "+jobOrder.getJobId()+"\t tesss  "+jsonObject.getInt("jobId"));
									if(user!=null){
										String text = jobLabelText.get(jsonObject.getInt("jobId"));
								//		System.out.println("##########"+jsonObject.containsKey("jobId"));
										if(text!=null)
											tmRecords.append(text);
										else
										{
											int approvalBeforeGoLive = (jobOrder==null)? -1 : jobOrder.getApprovalBeforeGoLive();
											
											if(approvalBeforeGoLive==0){
												tmRecords.append("&nbsp;|<a  href='javascript:void(0);' onclick=\"return approveJob("+entityID+","+jsonObject.getInt("jobId")+","+userMaster.getUserId()+")\">Approval Request</a>");
											}else if(approvalBeforeGoLive==1){
												tmRecords.append("&nbsp;|Job Approved");
											}else if(approvalBeforeGoLive!=0 && approvalBeforeGoLive!=1)
										       	{
														List<UserMaster> tempUsers=null;  
														tempUsers=mapofApprovedUsers.get(jsonObject.getInt("jobId"));
														if(tempUsers==null){
															tmRecords.append("&nbsp;|<a href='javascript:void(0);' onclick=\"return approveJob("+entityID+","+jsonObject.getInt("jobId")+","+userMaster.getUserId()+")\">Approval Request</a>");
														}else if(tempUsers!=null && tempUsers.size()>0){
															boolean userexist=false;
															for(UserMaster master : tempUsers)
																if(master.getUserId().equals(user.getUserId()))
																	userexist = true;
															
															if(userexist){
																tmRecords.append("&nbsp;|Pending Others");
															}else{
																tmRecords.append("&nbsp;|<a href='javascript:void(0);' onclick=\"return approveJob("+entityID+","+jsonObject.getInt("jobId")+","+userMaster.getUserId()+")\">Approval Request</a>");
															}
														}
										       	}
										}//End of else	
										if(userMaster.getDistrictId()!=null && userMaster.getDistrictId().getDistrictId().equals(806900) && userMaster.getDistrictId().getShowApprovalGropus()!=null && entityID==2){
											if(userMaster.getDistrictId().getShowApprovalGropus().equals(1)||userMaster.getDistrictId().getShowApprovalGropus().equals(2)){
												tmRecords.append("<a onclick='getapprovalgroupdetails("+JobOrderType+","+jsonObject.getInt("jobId")+","+jsonObject.getString("districtId")+")' href='#' javascript:void(0);>&nbsp;|&nbsp;Job Approval Details</a>");	
											} else if((userMaster.getDistrictId().getShowApprovalGropus().equals(3)||userMaster.getDistrictId().getShowApprovalGropus().equals(4)) && showApprovalFlag==1){
												tmRecords.append("<a onclick='getapprovalgroupdetails("+JobOrderType+","+jsonObject.getInt("jobId")+","+jsonObject.getString("districtId")+")' href='#' javascript:void(0);>&nbsp;|&nbsp;Job Approval Details</a>");
											}
										}
									}
									if(jsonObject.getBoolean("IsInviteOnly")){
										
										tmRecords.append("&nbsp;|<a  href='importcandidatedetails.do?jobId="+jsonObject.getInt("jobId")+"' rel='tooltip' id='exelexport"+count+"' data-original-title='Upload Candidates against this job'><i class='icon-upload' style='font-size: 1.5em;'></i></a>");
										tmRecords.append("<script>$('#exelexport"+count+"').tooltip();</script>");
									}
									tmRecords.append(" </td>");
								}
							}
						}
					}
				}else{
					tmRecords.append("<tr><td colspan='10'>"+Utility.getLocaleValuePropByKey("lblNoRecord", locale)+"</td></tr>" );
				}
				tmRecords.append("</table>");
				tmRecords.append(PaginationAndSorting.getPaginationStringForManageJobOrdersAjax(request,totalRecord,noOfRow, pageNo));
			}catch (Exception e) 
			{
				e.printStackTrace();
			}

			return tmRecords.toString();


		}
		

		/*			 Optimization Start @Anurag*/
			
			public String getStatusListForVVI_Op(String districtId,String jobId)
			{
				WebContext context;
				context = WebContextFactory.get();
				HttpServletRequest request = context.getHttpServletRequest();
				HttpSession session = request.getSession(false);
				if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
				{
					throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
				}
				
				StringBuffer status=	new StringBuffer();
				
				List<StatusMaster> statusMasterList = null;
				List<StatusMaster> lstStatusMaster = null;
				
				try
				{
					
				//	Map<Integer,SecondaryStatus> mapSStatus = new HashMap<Integer, SecondaryStatus>();
					
					DistrictMaster districtMaster = districtMasterDAO.findById(Integer.parseInt(districtId), false, false);
					//Anurag Old List<SecondaryStatus> lstSecStatus=	secondaryStatusDAO.findSecondaryStatusOnlyStatus(districtMaster);
					Map<Integer,String> mapSStatus =	secondaryStatusDAO.findSecondaryStatusOnlyStatusOp(districtMaster);
					List<SecondaryStatus> lstsecondaryStatus=  secondaryStatusDAO.findSecondaryStatusOnly_OP(districtMaster);
					JobOrder jobOrder = null;
					
					if(jobId!=null && !jobId.equals(""))
						jobOrder =jobOrderDAO.findById(Integer.parseInt(jobId), false, false);
					
					System.out.println(" jobOrder for VVI :: "+jobOrder);
					
					/*if(lstSecStatus!=null)
					for(SecondaryStatus secStatus : lstSecStatus){
						mapSStatus.put(secStatus.getStatusMaster().getStatusId(),secStatus);
					}*/
					
					
					SecondaryStatus secondaryStatus=null;

					String[] statuss = {"hird","scomp","ecomp","vcomp","dcln","rem","widrw"};
					lstStatusMaster	=	statusMasterDAO.findStatusByStatusByShortNames(statuss);
					
					statusMasterList = new ArrayList<StatusMaster>();
					
					for(StatusMaster sm: lstStatusMaster){
						if(sm.getBanchmarkStatus()==1){
							//secondaryStatus=mapSStatus.get(sm.getStatusId());
							if(secondaryStatus!=null){
								//sm.setStatus(secondaryStatus.getSecondaryStatusName());
								sm.setStatus(mapSStatus.get(sm.getStatusId()));
							}
						}
						statusMasterList.add(sm);
					}
					
					status.append("<select class='form-control' id='slctStatusID'>");
					status.append("<option id='' value='' ></option>");
					
					//For Add
					if(jobOrder==null)
					{
						if(statusMasterList!=null && statusMasterList.size()>0)
						{
							for(StatusMaster sm:statusMasterList)
							{
								status.append("<option value='"+sm.getStatusId()+"' >"+sm.getStatus()+"</option>");
							}
						}
						
						if(lstsecondaryStatus!=null && lstsecondaryStatus.size()>0)
						{
							for(SecondaryStatus ssm:lstsecondaryStatus)
							{
								status.append("<option value='SSID_"+ssm.getSecondaryStatusId()+"' >"+ssm.getSecondaryStatusName()+"</option>");
							}
						}
					}
					else // For Edit
					{
						if(statusMasterList!=null && statusMasterList.size()>0)
						{
							
							for(StatusMaster sm:statusMasterList)
							{
									if(jobOrder.getStatusMaster()!=null)
									{
										if(jobOrder.getStatusMaster().getStatusId().equals(sm.getStatusId()))
											status.append("<option value='"+sm.getStatusId()+"' selected>"+sm.getStatus()+"</option>");
										else
											status.append("<option value='"+sm.getStatusId()+"' >"+sm.getStatus()+"</option>");
									}
									else
										status.append("<option value='"+sm.getStatusId()+"' >"+sm.getStatus()+"</option>");
							}
						}
						if(lstsecondaryStatus!=null && lstsecondaryStatus.size()>0)
						{
							for(SecondaryStatus ssm:lstsecondaryStatus)
							{
								if(jobOrder.getSecondaryStatus()!=null){
										if(jobOrder.getSecondaryStatus().getSecondaryStatusId().equals(ssm.getSecondaryStatusId()))
											status.append("<option value='SSID_"+ssm.getSecondaryStatusId()+"' selected >"+ssm.getSecondaryStatusName()+"</option>");
										else
											status.append("<option value='SSID_"+ssm.getSecondaryStatusId()+"' >"+ssm.getSecondaryStatusName()+"</option>");
								}else
									status.append("<option value='SSID_"+ssm.getSecondaryStatusId()+"' >"+ssm.getSecondaryStatusName()+"</option>");
							}
						}
					}
					
					
					status.append("</select>");
					
				}catch(Exception e)
				{
					e.printStackTrace();
				}

				return status.toString();
			}

			
			//...........555555555
			public String districtJobDescriptionNmae(String jobcategoryyId)
			{
				System.out.println("iiiiidifisdfisdfi");
				WebContext context;
				context = WebContextFactory.get();
				HttpServletRequest request = context.getHttpServletRequest();
				HttpSession session = request.getSession(false);
				
				List<DistrictJobDescriptionLibrary> jobDescriptionLibraries=null;
				StringBuffer tmRecords=new StringBuffer();
				JobCategoryMaster jobCategoryymaster=null;
				
				if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
				{
					throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
				}
				try{
					UserMaster userMaster = null;
						userMaster=(UserMaster)session.getAttribute("userMaster");
						
						String dis=jobcategoryyId.replace("||", ",");
						String[] jobcategoryyyId=dis.split(",");
						String strin = jobcategoryyyId[0];
						Integer jobcategoryId=  Integer.parseInt(strin);
						System.out.println("jobcategoryId     "+jobcategoryId);
						if(jobcategoryId!=0){
							jobCategoryymaster=jobCategoryMasterDAO.findById(jobcategoryId, false, false);
							Criterion districtcriteria=Restrictions.eq("jobCategoryMaster", jobCategoryymaster);
							Criterion statuscriteria=Restrictions.eq("status", "A");
							jobDescriptionLibraries =districtJobDescriptionLibraryDAO.findByCriteria(districtcriteria,statuscriteria);
						}
						tmRecords.append("<select id='jobcategoryfordescriptionId' class='form-control' onchange='districtJobDescription();'>");
						tmRecords.append("<option  value='0'>Select Job Description Name</option>");
						if(jobDescriptionLibraries!=null && jobDescriptionLibraries.size()>0){
							
							for(DistrictJobDescriptionLibrary jcm:jobDescriptionLibraries){
								 tmRecords.append("<option  value='"+jcm.getDistrictjobdescriptionlibraryId()+"'  >"+jcm.getJobDescriptionName()+"</option>");
							}
						}
						tmRecords.append("</select>");
						
						if(jobDescriptionLibraries.size()==0){
							tmRecords.delete(0, tmRecords.length());
							tmRecords.append("no");
						}
						System.out.println("int the class  "+tmRecords.toString());
				}catch(Exception e){e.printStackTrace();}
				return tmRecords.toString();
		  }
			
			///////////////////////////////
			public String districtJobDescriptionn(Integer districtJbDescriptionLibraryId)
			{
				System.out.println("iiiiidifisdfisdfi");
				WebContext context;
				context = WebContextFactory.get();
				HttpServletRequest request = context.getHttpServletRequest();
				HttpSession session = request.getSession(false);
				
				DistrictJobDescriptionLibrary jobDescriptionLibraries=null;
				StringBuffer tmRecords=new StringBuffer();
				JobCategoryMaster jobCategoryymaster=null;
				
				if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
				{
					throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
				}
				try{
					UserMaster userMaster = null;
						userMaster=(UserMaster)session.getAttribute("userMaster");
						if(districtJbDescriptionLibraryId!=0)
							jobDescriptionLibraries =districtJobDescriptionLibraryDAO.findById(districtJbDescriptionLibraryId, false, false);
						if(jobDescriptionLibraries!=null){
							tmRecords.append(jobDescriptionLibraries.getJobDescription());
						}else{
							tmRecords.append("");
						}
						System.out.println("int the class  "+tmRecords.toString());
				}catch(Exception e){e.printStackTrace();}
				return tmRecords.toString();
		  }
			///////////////////////////////////////////

			public List<DistrictAccountingCode> getAccountingCodeMasterList(String accountingCode,int disctrictId){
				
				List<DistrictAccountingCode> list =new ArrayList<DistrictAccountingCode>();
				if(disctrictId==0){
					return null;
				}else{
					println("::::::::::::::::::getAccountingCodeMasterList::::::::::::"+accountingCode+"----************disctrictId"+disctrictId);
					DistrictMaster districtMaster = districtMasterDAO.findById(disctrictId,false, false);
					List<DistrictAccountingCode> accountCodeList = new ArrayList<DistrictAccountingCode>();
					//System.out.println("AAAAAA   "+districtMaster.getDistrictName());
					Criterion criterion1 = Restrictions.like("accountingCode", "%"+accountingCode+"%");
					Criterion criterion2 = Restrictions.eq("districtMaster", districtMaster);
					list = districtAccountingCodeDAO.findByCriteria(criterion1,criterion2);
					System.out.println("Lize size"+list.size());
					for(DistrictAccountingCode c:list){
						System.out.println("in the list"+c.getName());
					}
					return list;
				}
			}
			/**Gaurav Kumar**/
			public List<DistrictSpecificJobcode> getJobCodeMasterList(String jobCode,int disctrictId){
				List<DistrictSpecificJobcode> list  = new ArrayList<DistrictSpecificJobcode>();
				List<DistrictSpecificJobcode> list2 = new ArrayList<DistrictSpecificJobcode>(); 
				List<DistrictSpecificJobcode> finalList  = new ArrayList<DistrictSpecificJobcode>();
				List<String> list3 = new ArrayList<String>();
				
				if(disctrictId==0){
					return null;
				}else{
					println("::::::::::::::::::getJobCodeMasterList::::::::::::"+jobCode+"----************disctrictId"+disctrictId);
					WebContext context;
					context = WebContextFactory.get();
					HttpServletRequest request = context.getHttpServletRequest();
					HttpSession session = request.getSession(false);
					if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
					{
						throw new IllegalStateException("Your session has expired!");
					}
					
					DistrictMaster districtMaster = districtMasterDAO.findById(disctrictId,false, false);
					System.out.println("districtSpecificJobcodeDAO"+districtSpecificJobcodeDAO);
					
					
				   //boolean isExist = jobOrderDAO.findExistJobId(districtMaster,0,jobCode);
					//if(!isExist){
						list   				 =	districtSpecificJobcodeDAO.findJobCodeList(districtMaster,jobCode);
						System.out.println("Lize size"+list.size());
//						if(list!=null && list.size()>0){
//							boolean notExist=true;
//							for(DistrictSpecificJobcode c:list){
//								//list2
//								list3.add(c.getJobCode());
//								//System.out.println("Job Code:"+c.getJobCode());
//							}
//							List<JobOrder> jobOrderList = jobOrderDAO.findExistJobIdByMultipleJobCode(districtMaster,0,list3);
//							
//								for(DistrictSpecificJobcode c:list){
//									 notExist=true;
//									if(jobOrderList!=null && jobOrderList.size()>0){
//										for(JobOrder jobOrder:jobOrderList){
//											if(c.getJobCode().equals(jobOrder.getApiJobId())){
//												notExist =false;
//												break;
//											}
//										}
//									}
//								if(notExist)
//									finalList.add(c);
//
//							}
//						}
					//}
					return list;
				}
			}
			public void setJobBoardReferralURL(String referrer)
			{
				System.out.println("::::::::::::::::::::::::::::::: ManageJobOrdersAjax : setJobBoardReferralURL :::::::::::::::::::::::::::::::");
				try
				{
					WebContext context;
					context = WebContextFactory.get();
					HttpServletRequest request = context.getHttpServletRequest();
					HttpSession session = request.getSession(false);
					String jobBoardReferralURL = null;
					jobBoardReferralURL = session.getAttribute("jobBoardReferralURL")==null?"":(String)session.getAttribute("jobBoardReferralURL");
					if(jobBoardReferralURL!="" && jobBoardReferralURL!=null){
						System.out.println("jobBoardReferralURL : "+jobBoardReferralURL);
					}
					else if(referrer!="" && referrer!=null){
						System.out.println("referrer : "+referrer);
						session.setAttribute("jobBoardReferralURL", referrer);
					}
					else{
						System.out.println("****************** jobBoardReferralURL and referrer both are blank");
					}
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
			}
			
			
			// Anurag for Optimization replacement of saveJobWisePanelStatus
			
			public ArrayList<JobWisePanelStatus> saveJobWisePanelStatusOp(JobOrder jobOrder,String statusIds ,String secStatusIds){
				System.out.println(">>>>>>>>> call saveJobWisePanelStatus"+new Date().getTime());
				ArrayList<JobWisePanelStatus> listJobWise = new ArrayList<JobWisePanelStatus>();
				
			
					if(jobOrder!=null){
					SessionFactory factory=jobWisePanelStatusDAO.getSessionFactory();
					StatelessSession statelessSession=factory.openStatelessSession();
					Session session = factory.openSession();
					Transaction transaction=statelessSession.beginTransaction();	
					try{
					
					Criterion criterion1=Restrictions.eq("jobOrder",jobOrder);
					
					/*********************** status *****************************/
					if(!statusIds.equals("")){
						StringTokenizer statusIdsTokens=new StringTokenizer(statusIds,",");
						Criterion criterion2=Restrictions.isNotNull("statusMaster");
						//A List<JobWisePanelStatus>jobWisePanelStatusList=jobWisePanelStatusDAO.findByCriteria(criterion1,criterion2);
					
					/*A	Map<Integer, JobWisePanelStatus>jwsStatusMap=new HashMap<Integer, JobWisePanelStatus>();
						for(JobWisePanelStatus jws:jobWisePanelStatusList){
							jwsStatusMap.put(jws.getStatusMaster().getStatusId(), jws);
						}*/
						
						//optimized Anurag
						Map<Integer,String> jobWisePanelSecStatusId  =  jobWisePanelStatusDAO.jobwisePanelStatusList(criterion1,criterion2);
						Map<Integer,Integer> statusId2jobwisePanelId = new HashMap <Integer,Integer>();
						String data[];
						
						for(Map.Entry<Integer,String> entry: jobWisePanelSecStatusId.entrySet() ){
							data = entry.getValue().split("##");
							statusId2jobwisePanelId.put(Integer.parseInt(data[0]), entry.getKey());  // data[0] never null  A/C criterion2
							
						}
						
						
						List statusIdList = new ArrayList();
						List statusIdListPanelStatus=new ArrayList();
						while(statusIdsTokens.hasMoreElements()){
							String status=statusIdsTokens.nextToken();
							String[] token=status.split("#@@#");
							statusIdList.add(Integer.parseInt(token[0]));
							statusIdListPanelStatus.add(Boolean.valueOf(token[1]));
						}
						List<StatusMaster> lstStatusMasterByArrStatusId	=	statusMasterDAO.findStatusByStatusIdList(statusIdList);
						
						
						if(jobWisePanelSecStatusId.size()>0){
							for(int i=0;i<lstStatusMasterByArrStatusId.size();++i){
								StatusMaster statusMaster=lstStatusMasterByArrStatusId.get(i);
								Boolean statusState=(Boolean)statusIdListPanelStatus.get(i);
								
								/*A   JobWisePanelStatus jobWisePanelStatus=jwsStatusMap.get(statusMaster.getStatusId());
								
								jobWisePanelStatus.setStatusMaster(statusMaster);
								jobWisePanelStatus.setPanelStatus(statusState);
								jobWisePanelStatus.setJobOrder(jobOrder);
								statelessSession.update(jobWisePanelStatus);*/
								
								

								//start opt Anurag 
							 	String hqlUpdate = "update JobWisePanelStatus set  jobId=:jobId , panelStatus=:panelStatus,   statusId=:statusId  where   jobPanelStatusId=:jobPanelStatusId"; //"update jobwisepanelstatus c set c.secondaryStatusId = :secondaryStatusId, c.panelStatus = :panelStatus, c.jobId =:jobId where c.jobPanelStatusId = :jobPanelStatusId";
								// or String hqlUpdate = "update Customer set name = :newName where name = :oldName";
								//
								 int updatedEntities = session.createQuery( hqlUpdate )
								         .setInteger( "statusId", statusMaster.getStatusId() )
								         .setBoolean( "panelStatus", statusState )
								        .setInteger( "jobId", jobOrder.getJobId() )
								        .setInteger( "jobPanelStatusId", statusId2jobwisePanelId.get(statusMaster.getStatusId()) )
								        .executeUpdate();
								
								
							}
						}else{
							for(int i=0;i<lstStatusMasterByArrStatusId.size();++i){
								StatusMaster statusMaster=lstStatusMasterByArrStatusId.get(i);
								Boolean statusState=(Boolean)statusIdListPanelStatus.get(i);
								
								JobWisePanelStatus jobWisePanelStatus=new JobWisePanelStatus();
								jobWisePanelStatus.setStatusMaster(statusMaster);
								jobWisePanelStatus.setPanelStatus(statusState);
								jobWisePanelStatus.setJobOrder(jobOrder);
								
								statelessSession.insert(jobWisePanelStatus);
							}
						}
					}
					
					/**************secondary status *****************************/
					if(!secStatusIds.equals("")){
						System.out.println(">>>>>>>>> call saveJobWisePanelStatus secStatusIds not null");
						
						StringTokenizer secStatusIdsTokens=new StringTokenizer(secStatusIds,",");
						
						Criterion criterion3=Restrictions.isNotNull("secondaryStatus");
						//Optimization  
						Map<Integer,String> jobWisePanelSecStatusId  =  jobWisePanelStatusDAO.jobwisePanelStatusList(criterion1,criterion3);
						Map<Integer,Integer> secStatusId2jobwisePanelId = new HashMap <Integer,Integer>();
						String data[];
						
						for(Map.Entry<Integer,String> entry: jobWisePanelSecStatusId.entrySet() ){
							data = entry.getValue().split("##");
							secStatusId2jobwisePanelId.put(Integer.parseInt(data[1]), entry.getKey());  // data[1] never null  A/C criterion3
							/*JobWisePanelStatus jwps = new JobWisePanelStatus();jwps.setJobPanelStatusId(entry.getKey()); jwps.setPanelStatus(Boolean.parseBoolean(data[2]));
							listJobWise.add(jwps);*/
						}
						
					//A	List<JobWisePanelStatus>jobWisePanelSecStatusList=jobWisePanelStatusDAO.findByCriteria(criterion1,criterion3);
						
						Map<Integer, JobWisePanelStatus>jwsSecStatusMap=new HashMap<Integer, JobWisePanelStatus>();
						JobWisePanelStatus jobWise = null;
					/*	for(JobWisePanelStatus jws:jobWisePanelSecStatusList){
							//anurag		jobWise =new JobWisePanelStatus();
							//anurag		BeanUtils.copyProperties(jws, jobWise);
							//anurag	listJobWise.add(jobWise);
							jwsSecStatusMap.put(jws.getSecondaryStatus().getSecondaryStatusId(), jws);
						}*/
						
						List statusSecIdList = new ArrayList();
						List statusIdListSecPanelStatus=new ArrayList();
						while(secStatusIdsTokens.hasMoreElements()){
							String secStatus=secStatusIdsTokens.nextToken();
							String[] token=secStatus.split("#@@#");
							statusSecIdList.add(Integer.parseInt(token[0]));
							statusIdListSecPanelStatus.add(Boolean.valueOf(token[1]));
						}
						
						System.out.println("split condition");
						
						//Anurag List<SecondaryStatus> lstSecStatusMasterByArrStatusId	=	secondaryStatusDAO.findSecStatusByStatusIdList(statusSecIdList);
						List<Integer> lstSecStatusMasterByArrStatusId	=	secondaryStatusDAO.findSecStatusByStatusIdListOp(statusSecIdList);
						if(jobWisePanelSecStatusId.size()>0){
							System.out.println("if update");
							for(int i=0;i<lstSecStatusMasterByArrStatusId.size();++i){
								
								Integer secondaryStatus=lstSecStatusMasterByArrStatusId.get(i);
								Boolean statusState=(Boolean)statusIdListSecPanelStatus.get(i);
								
							//	JobWisePanelStatus jobWisePanelStatus=jwsSecStatusMap.get(secondaryStatus.getSecondaryStatusId());
								if(secStatusId2jobwisePanelId.containsKey(secondaryStatus))
								{
									
								/*	jobWisePanelStatus.setSecondaryStatus(secondaryStatus);
									jobWisePanelStatus.setPanelStatus(statusState);
									jobWisePanelStatus.setJobOrder(jobOrder);
									statelessSession.update(jobWisePanelStatus);*/
									

									//start opt Anurag 
								 	String hqlUpdate = "update JobWisePanelStatus set  jobId=:jobId , panelStatus=:panelStatus,   secondaryStatusId=:secondaryStatusId  where   jobPanelStatusId=:jobPanelStatusId"; //"update jobwisepanelstatus c set c.secondaryStatusId = :secondaryStatusId, c.panelStatus = :panelStatus, c.jobId =:jobId where c.jobPanelStatusId = :jobPanelStatusId";
									// or String hqlUpdate = "update Customer set name = :newName where name = :oldName";
									//
									 int updatedEntities = session.createQuery( hqlUpdate )
									         .setInteger( "secondaryStatusId", secondaryStatus )
									         .setBoolean( "panelStatus", statusState )
									        .setInteger( "jobId", jobOrder.getJobId() )
									        .setInteger( "jobPanelStatusId", secStatusId2jobwisePanelId.get(secondaryStatus) )
									        .executeUpdate();
									
									//end
									 
								}
							}
							
						}else{
							System.out.println("if insert");
							for(int i=0;i<lstSecStatusMasterByArrStatusId.size();++i){
								Integer secondaryStatus=lstSecStatusMasterByArrStatusId.get(i);
								Boolean statusState=(Boolean)statusIdListSecPanelStatus.get(i);
								SecondaryStatus secStatsus =  secondaryStatusDAO.findById(secondaryStatus, false, false);
								JobWisePanelStatus jobWisePanelStatus=new JobWisePanelStatus();
								jobWisePanelStatus.setSecondaryStatus(secStatsus);
								jobWisePanelStatus.setPanelStatus(statusState);
								jobWisePanelStatus.setJobOrder(jobOrder);
								
								statelessSession.insert(jobWisePanelStatus);
							}
						}
						
					}
					
					
				 	
				}catch (Exception e) {
					e.printStackTrace();
				}
				finally{
					if(transaction!=null)
					transaction.commit();
					System.out.println("txt commit");
					if(statelessSession!=null)
					statelessSession.close();
				}
					}
				
				System.out.println(">>>>>>>>> end saveJobWisePanelStatus"+new Date().getTime());
				
				return listJobWise;
			}
			
			@Transactional(readOnly=false)
			public void sendApprovalMail(JobOrder jobOrder,DistrictMaster districtMaster, Integer returnJobId, String returnjobTitle,String jeffcoSpecialEdFlag,JobOrderDAO jobOrderDAO, HttpServletRequest request){
				JobCategoryMaster jobCategoryMaster = jobOrder.getJobCategoryMaster();
				System.out.println("-----------------------------Start of Sending mail "+jobOrder.getJobId()+"-------------------------------");
				System.out.println("jobCategoryMaster:- "+jobCategoryMaster+", jobCategoryMaster.getApprovalBeforeGoLive():- "+jobCategoryMaster.getApprovalBeforeGoLive()+", jobCategoryMaster.getBuildApprovalGroup():- "+jobCategoryMaster.getBuildApprovalGroup());
				if(jobCategoryMaster.getApprovalByPredefinedGroups()!=null && jobCategoryMaster.getApprovalByPredefinedGroups() )	//Jeffco Specific Approval Flow
				{
					jeffcoSpecialEdFlag = (jeffcoSpecialEdFlag==null || jeffcoSpecialEdFlag.equals("") )? "N" : jeffcoSpecialEdFlag;
					System.out.println("jeffcoSpecialEdFlag:- "+jobOrder.getSpecialEdFlag());
					
					DistrictSpecificApprovalFlow districtSpecificApprovalFlow = districtSpecificApprovalFlowDAO.findApprovalFlowByFlag(jeffcoSpecialEdFlag,jobOrder.getDistrictMaster());
					System.out.println("districtSpecificApprovalFlow:- "+districtSpecificApprovalFlow.getApprovalFlow());
					
					Map<String, String> groupShortNameWithGroupName = districtWiseApprovalGroupDAO.mapGroupShortNameWithGroupName(districtSpecificApprovalFlow.getApprovalFlow(),jobOrder.getDistrictMaster());
					System.out.println("groupShortNameWithGroupName:- "+groupShortNameWithGroupName.size());
					
					List<DistrictApprovalGroups> districtApprovalGroups = districtSpecificApprovalFlowDAO.findDistrictApprovalGroupByApprovalFlow(districtSpecificApprovalFlow.getApprovalFlow(), jobOrder.getDistrictMaster());
					System.out.println("districtApprovalGroups:- "+districtApprovalGroups.size());
					//Map<String, DistrictApprovalGroups> districtApprovalGroups = districtSpecificApprovalFlowDAO.findDistrictApprovalGroupByApprovalFlow(districtSpecificApprovalFlow.getApprovalFlow(), jobOrder.getDistrictMaster());
					
					
					for(DistrictApprovalGroups dag : districtApprovalGroups)	//iterate each Group and send the mail to first group
					{
						List<Integer> groupMemberIds = new ArrayList<Integer>();
						for(String groupMember : dag.getGroupMembers().split("#"))
							if(!groupMember.equals(""))
								try{groupMemberIds.add(Integer.parseInt(groupMember));}catch(Exception e){}
						
						System.out.println("groupMemberIds:- "+groupMemberIds);
						List<UserMaster> userMasters = new ArrayList<UserMaster>();
						if(groupMemberIds.size()>0)
						{
							List<DistrictKeyContact> approvalContacts = districtKeyContactDAO.findByCriteria(Restrictions.in("keyContactId", groupMemberIds));
							
							System.out.println("approvalContacts:- "+approvalContacts);
							for(DistrictKeyContact districtKeyContact : approvalContacts)
								userMasters.add(districtKeyContact.getUserMaster());
						}
						
						String groupFullName = (groupShortNameWithGroupName.get("SA")==null)? "" : groupShortNameWithGroupName.get("SA");
						if(dag.getGroupName().equals(groupFullName))
						{
							//Get the total Number of SA's
							List<SchoolInJobOrder> schoolInJobOrder = schoolInJobOrderDAO.findByCriteria(Restrictions.eq("jobId",jobOrder));
							
							List<SchoolMaster> schoolMasters = new ArrayList<SchoolMaster>();
							for(SchoolInJobOrder order : schoolInJobOrder)
								schoolMasters.add(order.getSchoolId());
							
							List<UserMaster> userMasters2 = null;
							if(schoolInJobOrder!=null && schoolInJobOrder.size()>0)
							{
								Criterion criterion1 = Restrictions.eq("entityType", new Integer(3));
								Criterion criterion2 = Restrictions.in("schoolId", schoolMasters);
								Criterion criterion3 = Restrictions.eq("districtId", districtMaster);
								Criterion criterion4 = Restrictions.eq("status","A");
								Criterion criterion5 = Restrictions.eq("roleId", roleMasterDAO.findById(3,false,false));
								
								userMasters2 = userMasterDAO.findByCriteria(criterion1,criterion2,criterion3,criterion4,criterion5);
								for(UserMaster master : userMasters2)
									userMasters.add(master);
							}
						}
						
						groupFullName = (groupShortNameWithGroupName.get("ES")==null)? "" : groupShortNameWithGroupName.get("ES");
						if(dag.getGroupName().equals(groupFullName))
						{
							//Logic For ES Shadab 322
							EmploymentServicesTechnician primaryServicesTechnician = jobOrder.getEmploymentServicesTechnician();
							
							List<String> emailAddress = new ArrayList<String>();
							emailAddress.add(primaryServicesTechnician.getPrimaryEmailAddress());
							emailAddress.add(primaryServicesTechnician.getBackupEmailAddress());
							
							System.out.println("primaryTechnicianEmailAddress:- "+primaryServicesTechnician.getPrimaryEmailAddress());
							System.out.println("backupTechnicianEmailAddress:- "+primaryServicesTechnician.getBackupEmailAddress());
							
							Criterion criterion = Restrictions.in("keyContactEmailAddress", emailAddress);
							List<DistrictKeyContact> districtKeyContacts = districtKeyContactDAO.findByCriteria(criterion);
							
							for(DistrictKeyContact contact : districtKeyContacts)
									userMasters.add(contact.getUserMaster());
						}
						
						String baseURL=Utility.getBaseURL(request);             
						String content="";
						
						if(userMasters!=null && userMasters.size()>0)
						{
							for (UserMaster master : userMasters)
							{
								DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
								dsmt.setEmailerService(emailerService);
								dsmt.setMailfrom("admin@netsutra.com");
								dsmt.setMailto(master.getEmailAddress());
								dsmt.setMailsubject("Approval Mail");
								String incJobId = Utility.encryptNo(returnJobId);
								String incUserId = Utility.encryptNo(master.getUserId());
								String groupId = Utility.encryptNo(dag.getDistrictApprovalGroupsId());
								
								String linkIds= incJobId+"###"+incUserId+"###"+groupId;
								String forMated = "";
								try
								{
									forMated = Utility.encodeInBase64(linkIds);
									forMated = baseURL+"approveJob.do?id="+forMated;
									
									System.out.println("forMated   "+forMated);
									System.out.println("returnjobTitle   "+returnjobTitle);
									content=MailText.getApproveJObMailTextJeffcoSpecific(request,master, forMated,returnjobTitle);
									dsmt.setMailcontent(content);
									dsmt.start();
								}
								catch (Exception e)
								{
									e.printStackTrace();
								}
							}
							break;
						}
					}
					
				try
				{
					jobOrder.setApprovalBeforeGoLive(2);
					jobOrderDAO.getSessionFactory().getCurrentSession().merge(jobOrder);
				}
				catch(Exception e){e.printStackTrace();}
			}
			else
			if(jobCategoryMaster!=null && jobCategoryMaster.getApprovalBeforeGoLive()!=null && jobCategoryMaster.getBuildApprovalGroup()!=null && jobCategoryMaster.getApprovalBeforeGoLive() && jobCategoryMaster.getBuildApprovalGroup())
			{
				System.out.println("inside 1");
				List<DistrictApprovalGroups> districtApprovalGroupList = districtApprovalGroupsDAO.findDistrictApprovalGroupsByJobCategory(districtMaster,jobCategoryMaster);
				
				System.out.println("districtApprovalGroupList.size():- "+(districtApprovalGroupList.size()));
				if(districtApprovalGroupList!=null && districtApprovalGroupList.size()>0)
				{
					for(DistrictApprovalGroups districtApprovalGroups : districtApprovalGroupList)	//iterate each Group and send the mail to first group
					{
						List<Integer> groupMemberIds = new ArrayList<Integer>();
						for(String groupMember : districtApprovalGroups.getGroupMembers().split("#"))
							if(!groupMember.equals(""))
								groupMemberIds.add(Integer.parseInt(groupMember));
						
						System.out.println("groupMemberIds:- "+groupMemberIds);
						if(groupMemberIds.size()>0)
						{
							List<DistrictKeyContact> approvalContacts = districtKeyContactDAO.findByCriteria(Restrictions.in("keyContactId", groupMemberIds));
							String baseURL=Utility.getBaseURL(request);             
							String content="";
							
							System.out.println("approvalContacts:- "+approvalContacts);
							if(approvalContacts!=null && approvalContacts.size()>0)
							{
								for (DistrictKeyContact districtKeyContact : approvalContacts)
								{
									DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
									dsmt.setEmailerService(emailerService);
									dsmt.setMailfrom("admin@netsutra.com");
									dsmt.setMailto(districtKeyContact.getKeyContactEmailAddress());
									dsmt.setMailsubject("Approval Mail");
									System.out.println("returnJobId   "+returnJobId);
									System.out.println("districtKeyContact.getUserMaster().getUserId()   "+districtKeyContact.getUserMaster().getUserId());
									String incJobId = Utility.encryptNo(returnJobId);
									String incUserId = Utility.encryptNo(districtKeyContact.getUserMaster().getUserId());
									String groupId = Utility.encryptNo(districtApprovalGroups.getDistrictApprovalGroupsId());
									
									String linkIds= incJobId+"###"+incUserId+"###"+groupId;
									String forMated = "";
									try
									{
										forMated = Utility.encodeInBase64(linkIds);
										forMated = baseURL+"approveJob.do?id="+forMated;
										
										System.out.println("forMated   "+forMated);
										System.out.println("returnjobTitle   "+returnjobTitle);
										content=MailText.getApproveJObMailText(request,districtKeyContact, forMated,returnjobTitle);
										dsmt.setMailcontent(content);
										dsmt.start();
									}
									catch (Exception e)
									{
										e.printStackTrace();
									}
								}
							//	mailSendByThread(1,noOfSchoolExpHires,userMaster,userMasterDetails.getEmailAddress(),subject,request, jobOrder, certificationType.replace(",",", "),userMasterDetails.getFirstName());
							}
							try
							{
								jobOrder.setApprovalBeforeGoLive(2);
								jobOrderDAO.makePersistent(jobOrder);
							}
							catch(Exception e){e.printStackTrace();}
							break;
						}
					}
				}
			}//End of send mail functionality
			else
			if(districtMaster!=null && districtMaster.getApprovalBeforeGoLive()!=null && districtMaster.getApprovalBeforeGoLive())
			{
				List<DistrictKeyContact> approvalContacts = districtKeyContactDAO.findByContactType(districtMaster, "Job Approval");           
				String baseURL=Utility.getBaseURL(request);             
				String content="";
				
				if(approvalContacts!=null && approvalContacts.size()>0)
					for (DistrictKeyContact districtKeyContact : approvalContacts) {
						DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
						dsmt.setEmailerService(emailerService);
						dsmt.setMailfrom("admin@netsutra.com");
						dsmt.setMailto(districtKeyContact.getKeyContactEmailAddress());
						dsmt.setMailsubject("Approval Mail");
						System.out.println("returnJobId   "+returnJobId);
						System.out.println("districtKeyContact.getUserMaster().getUserId()   "+districtKeyContact.getUserMaster().getUserId());
						String incJobId = Utility.encryptNo(returnJobId);
						String incUserId = Utility.encryptNo(districtKeyContact.getUserMaster().getUserId());
						
						String linkIds= incJobId+"###"+incUserId;
						String forMated = "";
						try {
							forMated = Utility.encodeInBase64(linkIds);
							forMated = baseURL+"approveJob.do?id="+forMated;
						} catch (Exception e) {
							e.printStackTrace();
						}
						
						System.out.println("forMated   "+forMated);
						System.out.println("returnjobTitle   "+returnjobTitle);
						content=MailText.getApproveJObMailText(request,districtKeyContact, forMated,returnjobTitle);					
						dsmt.setMailcontent(content);
						try {
							dsmt.start();	
						} catch (Exception e) {}
					}
			}
		
			}
			
			
			
			
			// Optimized By Anurag replacement of countTotalReqNo(int jobId)
			
			
			@Transactional(readOnly=false)
			public Integer countTotalReqNoOp(int jobId)
			{
				/* ========  For Session time Out Error =========*/
				WebContext context;
				context = WebContextFactory.get();
				HttpServletRequest request = context.getHttpServletRequest();
				HttpSession session = request.getSession(false);
				if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
				{
					throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
				}
				Integer result = 0;
				try
				{
					
					System.out.println(" jobId "+jobId);
				//	JobOrder jobOrder = jobOrderDAO.findById(jobId, false, false);
					Criterion criterion1 = Restrictions.eq("jobOrder.jobId", jobId);
					Criterion criterion2 = Restrictions.isNull("schoolMaster");
					result =jobRequisitionNumbersDAO.countRowByCriterion(criterion1,criterion2);
				}catch (Exception e) {
					e.printStackTrace();
					return null;
				}
				return result;
			}
			/********************************SWADESH********************************/
			   public String getJobCodeDetails(int disctrictId, String jobcode)
			    {
			    List<DistrictSpecificJobcode> districtSpecificJobcode =new ArrayList<DistrictSpecificJobcode>();
			    JSONObject Jsonobj = new JSONObject();  
			    WebContext context;
			    context = WebContextFactory.get();
			    HttpServletRequest request = context.getHttpServletRequest();
			    HttpSession session = request.getSession(false);
			    if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			    {
			     throw new IllegalStateException("Your session has expired!");
			    }
			    
			    DistrictMaster districtMaster = districtMasterDAO.findById(disctrictId,false, false);
			    districtSpecificJobcode=districtSpecificJobcodeDAO.findJobCodedetails(districtMaster, jobcode);
			    JobCategoryMaster jobCategoryMaster=new JobCategoryMaster();
			    if(districtSpecificJobcode.size()!=0)
			    jobCategoryMaster=jobCategoryMasterDAO.findById(districtSpecificJobcode.get(0).getJobCategoryId().getJobCategoryId(), false, false);
			    
			    for (DistrictSpecificJobcode districtSpecificJobcode2 : districtSpecificJobcode) {
			     System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>     "+districtSpecificJobcode2.getJobCategoryId());
			     Jsonobj.put("JobTitle", districtSpecificJobcode2.getJobTitle());
			     Jsonobj.put("Hours", districtSpecificJobcode2.getHours());
			     Jsonobj.put("JobDescription", districtSpecificJobcode2.getJobDescription());
			     
			     Jsonobj.put("salaryAdminPlan", districtSpecificJobcode2.getSalaryAdminPlan());
			     Jsonobj.put("step", districtSpecificJobcode2.getStep());
			     Jsonobj.put("gradePay", districtSpecificJobcode2.getGradePay());
			     
			     Jsonobj.put("jobcategory", districtSpecificJobcode2.getJobCategoryId().getJobCategoryId()+"||"+jobCategoryMaster.getBaseStatus()+"||");
			     
			    }
			    return Jsonobj.toString();
			   
			    }
			   
		 private Map<String,Boolean> getCNFGColumn(DistrictMaster districtMaster,boolean flagHQ){
				   totalShowCol=0;
				   Map<String,Boolean> displayCNFGColumn=new LinkedHashMap<String, Boolean>();
					displayCNFGColumn.put("JID", false); //for Job Id
					displayCNFGColumn.put("TIT", false); //for Title
					displayCNFGColumn.put("SCH", false); //for School/Dept
					displayCNFGColumn.put("ZON", false); //for Zone
					displayCNFGColumn.put("SUB", false); //for Subject
					displayCNFGColumn.put("STA", false); //for Status
					displayCNFGColumn.put("POSU", false); //for Posted Until
					displayCNFGColumn.put("REQ", false); //for Position/Requisition Number
					displayCNFGColumn.put("POS", false); //for Number of Position
					displayCNFGColumn.put("APP", false); //for Applicant(s)
					displayCNFGColumn.put("HPER", false); //for Hires
					displayCNFGColumn.put("CANG", false); //for Candidate Grid
					displayCNFGColumn.put("ATT", false); //for District Attachment
					displayCNFGColumn.put("ACT", false); //for Actions
					if(districtMaster!=null && districtMaster.getDisplayCNFGColumn()!=null){
					String[] getDisplayCNFGCol=districtMaster.getDisplayCNFGColumn().split("##");
						for(String col:getDisplayCNFGCol){
							if(!col.trim().equals("")){
							displayCNFGColumn.put(col.trim(), true);
							if(flagHQ){
								if(!col.trim().equalsIgnoreCase("CANG") && !col.trim().equalsIgnoreCase("ATT") && !col.trim().equalsIgnoreCase("ACT") && !col.trim().equalsIgnoreCase("REQ") && !col.trim().equalsIgnoreCase("ZON") && !col.trim().equalsIgnoreCase("SUB"))
									++totalShowCol;
							}else if(!col.trim().equalsIgnoreCase("CANG") && !col.trim().equalsIgnoreCase("ATT") && !col.trim().equalsIgnoreCase("ACT") && !col.trim().equalsIgnoreCase("REQ"))
							++totalShowCol;
							}
						}
					}
					System.out.println("displayCNFGColumn======"+totalShowCol+"========"+displayCNFGColumn);
					return displayCNFGColumn;
			   }
			   		
		
		@Transactional(readOnly=false)
		public String savingDJobEequisitionNumbersTempOnlyJeffcoClone(int jobId,int clonejobId,String reqPositionNum)
		{			
			System.out.println(jobId+ " >>>>>>>>>>>>>>>>>>>>>>>> clonejobId :: "+ clonejobId+" >>>>>>>>>>>> reqPositionNum :: "+reqPositionNum);
			StringBuffer buffer=new StringBuffer();
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		    }
			String tempId=session.getId();
			String returnmsg="";
			SchoolMaster schoolMaster=null;
			try {
				
				if(jobId==0 && clonejobId!=0){
					JobOrder joJeffco= jobOrderDAO.findById(clonejobId, false, false);
					DistrictRequisitionNumbers fetchDrnForCloning=districtRequisitionNumbersDAO.findByCriteria(Restrictions.eq("requisitionNumber", reqPositionNum)).get(0);
					DistrictRequisitionNumbers drnClone=new DistrictRequisitionNumbers();
					BeanUtils.copyProperties(fetchDrnForCloning, drnClone);
					drnClone.setDistrictRequisitionId(null);
					drnClone.setIsUsed(false);
					drnClone.setCreatedDateTime(new Date());
					districtRequisitionNumbersDAO.makePersistent(drnClone);
				
					Criterion criterion = Restrictions.eq("locationCode", joJeffco.getSchool().get(0).getLocationCode());
					Criterion cri2 = Restrictions.eq("districtId", drnClone.getDistrictMaster());
					List<SchoolMaster> schoolMaster1=schoolMasterDAO.findByCriteria(criterion,cri2);
					if(schoolMaster1!=null && schoolMaster1.size()>0){
						Jobrequisitionnumberstemp jrqn = new Jobrequisitionnumberstemp();
						jrqn.setSchoolMaster(schoolMaster1.get(0));
						jrqn.setDistrictRequisitionNumbers(drnClone);	
						jrqn.setStatus(0);
						jrqn.setNoOfSchoolExpHires(1);
						jrqn.setTempId(tempId);
						jobRequisitionNumbersTempDAO.makePersistent(jrqn);
						return jrqn.getTempId();
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return returnmsg;
		}
		
	public int jobApprovalProcessForDistrictSpecific(HttpServletRequest request, Integer jobApprovalProcessId,DistrictMaster districtMaster,UserMaster userMaster,JobOrder jobOrder)
	{
		//System.out.println("start job approval start333333333333333333333333333333333333333333333333333333333333333333333333333 *******************");
		String ipAddress = IPAddressUtility.getIpAddress(request);
		JobApprovalProcess jobApprovalProcess=	jobApprovalProcessDAO.findById(jobApprovalProcessId, false, false);
		String approvalIds	=	jobApprovalProcess.getJobApprovalGroups();
		String groupIds[]=null;
		if(approvalIds!=null && !approvalIds.equals("")){
			groupIds=approvalIds.split("\\|");
			List<Integer> groupApprovalIds=new ArrayList<Integer>();
			if(groupIds!=null)
			{
				for(String goupid:groupIds){
					
					if(!goupid.equals("")){
						try{
							Integer grpid = Integer.parseInt(goupid);
							groupApprovalIds.add(grpid);
						}catch(Exception e){e.printStackTrace();}
					}
				}
			}
		DistrictKeyContact userKeyContact=null;
		DistrictApprovalGroups userApprovalGroups=null;
		List<DistrictApprovalGroups> districtApprovalGroupsList=	districtApprovalGroupsDAO.findGroupsByDistrictAndDistrictApprovalGroupsId(districtMaster,groupApprovalIds);
		Map<Integer ,DistrictApprovalGroups> distrMap=new LinkedHashMap<Integer, DistrictApprovalGroups>();
		List<DistrictKeyContact> districtKeyContactList = null;
		districtKeyContactList = districtKeyContactDAO.findByContactType(districtMaster,"Job Approval");
			if(districtKeyContactList!=null &&districtKeyContactList.size()>0){
				for(DistrictKeyContact dkc:districtKeyContactList){
					if(dkc.getUserMaster().getEmailAddress().equals(userMaster.getEmailAddress())){
						userKeyContact=dkc;
						break;
					}
				}
			}
		if(districtApprovalGroupsList!=null && districtApprovalGroupsList.size()>0){
			for(DistrictApprovalGroups districtApprovalGroups:districtApprovalGroupsList){
				if(userKeyContact!=null && districtApprovalGroups.getGroupMembers()!=null){
						if(districtApprovalGroups.getGroupMembers().contains("#"+userKeyContact.getKeyContactId()+"#")){
							userApprovalGroups=districtApprovalGroups;
					}
				}
				distrMap.put(districtApprovalGroups.getDistrictApprovalGroupsId(), districtApprovalGroups);
			}
			for(Integer id1:groupApprovalIds){
				
				DistrictApprovalGroups		districtApprovalGroups=distrMap.get(id1);
				if(districtApprovalGroups!=null){
				String groupMembers=districtApprovalGroups.getGroupMembers();
				if(userKeyContact!=null && groupMembers!=null){ 
					if(!districtApprovalGroups.getGroupMembers().contains("#"+userKeyContact.getKeyContactId()+"#")){
						List<Integer> groupMemberIds = new ArrayList<Integer>();
						for(String id:groupMembers.split("#")){
							if(!id.equals(""))
								groupMemberIds.add(Integer.parseInt(id));
						}
						if(groupMemberIds.size()>0){
							List<DistrictKeyContact> approvalContacts = districtKeyContactDAO.findByCriteria(Restrictions.in("keyContactId", groupMemberIds));
							if(approvalContacts!=null &&approvalContacts.size()>0){	
								JobApprovalHistory jobApprovalHistory = new JobApprovalHistory();
								jobApprovalHistory.setCreatedDateTime(new Date());
								jobApprovalHistory.setUserMaster(userMaster);
								jobApprovalHistory.setJobOrder(jobOrder);
								jobApprovalHistory.setIpAddress(ipAddress);
								if(userApprovalGroups!=null)
								jobApprovalHistory.setDistrictApprovalGroups(userApprovalGroups);
								jobApprovalHistoryDAO.makePersistent(jobApprovalHistory);
								int sendEmailSuccess =sendEmailToJobApprovalGroup(request,approvalContacts, jobOrder.getJobId(),jobOrder.getJobTitle());
								if(sendEmailSuccess==1)	
								return 1; 
								}
							}
						}
				} else{
					List<Integer> groupMemberIds = new ArrayList<Integer>();
					if(groupMembers!=null)
					for(String id:groupMembers.split("#")){
						if(!id.equals(""))
							groupMemberIds.add(Integer.parseInt(id));
					}
					if(groupMemberIds.size()>0){
						List<DistrictKeyContact> approvalContacts = districtKeyContactDAO.findByCriteria(Restrictions.in("keyContactId", groupMemberIds));
						if(approvalContacts!=null &&approvalContacts.size()>0){	
							JobApprovalHistory jobApprovalHistory = new JobApprovalHistory();
							jobApprovalHistory.setCreatedDateTime(new Date());
							jobApprovalHistory.setUserMaster(userMaster);
							jobApprovalHistory.setJobOrder(jobOrder);
							jobApprovalHistory.setIpAddress(ipAddress);
							if(userApprovalGroups!=null)
							jobApprovalHistory.setDistrictApprovalGroups(userApprovalGroups);
							jobApprovalHistoryDAO.makePersistent(jobApprovalHistory);
							int sendEmailSuccess =sendEmailToJobApprovalGroup(request,approvalContacts, jobOrder.getJobId(),jobOrder.getJobTitle());
							if(sendEmailSuccess==1)	
							return 1; 
						}
					}
				}
			}}
		} else
		{
			return 2; //no approval group attached
		}
	} else{
		return 2; //no approval group attached
	}
		return 2;  //no approval group attached
	}

	public int sendEmailToJobApprovalGroup(HttpServletRequest request, List<DistrictKeyContact>  approvalContacts, Integer returnJobId,String jobTitle)
	{
		String baseURL=Utility.getBaseURL(request);
		String content="";
		for (DistrictKeyContact districtKeyContact : approvalContacts) {
			DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
			dsmt.setEmailerService(emailerService);
			dsmt.setMailfrom("admin@netsutra.com");
			dsmt.setMailto(districtKeyContact.getKeyContactEmailAddress());
			dsmt.setMailsubject("Approval Mail");
			System.out.println("returnJobId   "+returnJobId);
			System.out.println("districtKeyContact.getUserMaster().getUserId()   "+districtKeyContact.getUserMaster().getUserId());
			String incJobId = Utility.encryptNo(returnJobId);
			String incUserId = Utility.encryptNo(districtKeyContact.getUserMaster().getUserId());
			
			String linkIds= incJobId+"###"+incUserId;
			String forMated = "";
			try {
				forMated = Utility.encodeInBase64(linkIds);
				forMated = baseURL+"approveJob.do?id="+forMated;
			} catch (Exception e) {
				e.printStackTrace();
				return 2;
			}
			
			System.out.println("forMated   "+forMated);
		//	System.out.println("returnjobTitle   "+returnjobTitle);
			content=MailText.getApproveJObMailText(request,districtKeyContact, forMated,jobTitle);					
			dsmt.setMailcontent(content);
			try {
				dsmt.start();	
			} catch (Exception e) {
				
			}
		}
		return 1;
	}
	
private Map<Integer, String> getUserJobLevelTextApprovalGroupForAdams(Integer entityID, UserMaster schoolAdmin, List<JobOrder> lstJobOrder ,DistrictKeyContact userKeyContact)
{
		Map<Integer, String> jobLabelText = new HashMap<Integer, String>();
			if(userKeyContact==null){
				return jobLabelText; //return Login user is not job approval type
			}
		try
		{
			if((entityID==2 || entityID==3)&& schoolAdmin!=null && schoolAdmin.getDistrictId()!=null && schoolAdmin.getDistrictId().getDistrictId().equals(806900) && lstJobOrder!=null && lstJobOrder.size()>0)
			{
				Map<JobApprovalProcess ,List<Integer>> jobProcessForGroupId= new LinkedHashMap<JobApprovalProcess, List<Integer>>();
				Map<Integer,DistrictApprovalGroups> districtApprovalGroupsMap=new LinkedHashMap<Integer, DistrictApprovalGroups>();
				List<JobWiseApprovalProcess>	jobWiseApprovalProcessList=	jobWiseApprovalProcessDAO.findByCriteria(Restrictions.in("jobId", lstJobOrder));
				List<Integer> districtGroupsId=new  ArrayList<Integer>();
				Map<JobOrder ,JobApprovalProcess> jobWiseProcessFlowMap= new LinkedHashMap<JobOrder ,JobApprovalProcess>();
				Map<JobOrder ,List<JobApprovalHistory>> jobWiseApproveHistoryMap= new LinkedHashMap<JobOrder ,List<JobApprovalHistory>>();
				if(jobWiseApprovalProcessList!=null && jobWiseApprovalProcessList.size()>0)
				for(JobWiseApprovalProcess jobWiseApprovalProcess :jobWiseApprovalProcessList){
				String approvalIds	=	jobWiseApprovalProcess.getJobApprovalProcessId().getJobApprovalGroups();
				String groupIds[]=null;
				groupIds=approvalIds.split("\\|");
				List<Integer> groupApprovalIds=new ArrayList<Integer>();
				if(groupIds!=null)
				{
				for(String goupid:groupIds){
					
					if(!goupid.equals("")){
						try{
							Integer grpid = Integer.parseInt(goupid);
							groupApprovalIds.add(grpid);
						}catch(Exception e){e.printStackTrace();}
					}
				}
				districtGroupsId.addAll(groupApprovalIds);
			}	
			jobProcessForGroupId.put(jobWiseApprovalProcess.getJobApprovalProcessId(), groupApprovalIds);
			jobWiseProcessFlowMap.put(jobWiseApprovalProcess.getJobId(), jobWiseApprovalProcess.getJobApprovalProcessId());
		}
		List<JobApprovalHistory> jobApprovalHistories =null;
		List<DistrictApprovalGroups> districtApprovalGroupsList=null;
		if(districtGroupsId!=null &&districtGroupsId.size()>0)
		districtApprovalGroupsList=	districtApprovalGroupsDAO.findGroupsByDistrictAndDistrictApprovalGroupsId(schoolAdmin.getDistrictId(),districtGroupsId);
		if(districtApprovalGroupsList!=null &&districtApprovalGroupsList.size()>0){
		Criterion criterion=Restrictions.in("districtApprovalGroupsId",districtApprovalGroupsList);
		jobApprovalHistories = jobApprovalHistoryDAO.findByCriteria(Restrictions.in("jobOrder", lstJobOrder));
		for(DistrictApprovalGroups dags:districtApprovalGroupsList){
			districtApprovalGroupsMap.put(dags.getDistrictApprovalGroupsId(), dags);
		}
		
		}
		if(jobApprovalHistories!=null && jobApprovalHistories.size()>0){
			for(JobOrder jobOrder:lstJobOrder){
				List<JobApprovalHistory> lst=new ArrayList<JobApprovalHistory>();
				for(JobApprovalHistory jobApprovalHistory:jobApprovalHistories){	
				if(jobApprovalHistory.getJobOrder().getJobId().equals(jobOrder.getJobId())){
					lst.add(jobApprovalHistory);
				}
			}
			jobWiseApproveHistoryMap.put(jobOrder, lst);
		}}
		
		UserMaster user = schoolAdmin;
		Map<Integer,DistrictKeyContact> creatorDistrictkeycontact=new LinkedHashMap<Integer, DistrictKeyContact>();
		List<Integer> userId=new ArrayList<Integer>();
		for(JobOrder job:lstJobOrder){
			if(job.getApprovalBeforeGoLive()!=1){
				userId.add(job.getCreatedBy());
			}
		}
		if(userId.size()>0){						
		List<UserMaster>	JobCreatorList=	userMasterDAO.findByCriteria(Restrictions.in("userId", userId));
		Criterion c1=Restrictions.eq("contactType", "Job Approval");
		Criterion c2=Restrictions.eq("status", "A");
		List<ContactTypeMaster>		contactTypeMaster=	contactTypeMasterDAO.findByCriteria(c1,c2);
			if(JobCreatorList!=null &&JobCreatorList.size()>0 && contactTypeMaster!=null && contactTypeMaster.size()>0){
				Criterion c3=Restrictions.in("userMaster", JobCreatorList);
				Criterion c4=Restrictions.eq("districtId", schoolAdmin.getDistrictId().getDistrictId());
				Criterion c5=Restrictions.eq("keyContactTypeId", contactTypeMaster.get(0));
				List<DistrictKeyContact> districtKeyContacts= districtKeyContactDAO.findByCriteria(c3,c4,c5);
				if(districtKeyContacts!=null &&districtKeyContacts.size()>0){
					for(DistrictKeyContact dkc:districtKeyContacts){
						creatorDistrictkeycontact.put(dkc.getUserMaster().getUserId(), dkc);
					}
				}
			}
		}
		for(JobOrder jobOrder : lstJobOrder)	
		{
			
			boolean saAsWithAssociatedJob=false;
			JobApprovalProcess jbap=	jobWiseProcessFlowMap.get(jobOrder);
			if(entityID==3){
				if(jbap!=null){
				List<Integer>	groupIds=jobProcessForGroupId.get(jbap);
				if(groupIds!=null && groupIds.size()>0){
					for(Integer districtApprovalGroupsId:groupIds){
						DistrictApprovalGroups districtApprovalGroups=districtApprovalGroupsMap.get(districtApprovalGroupsId);
						if(districtApprovalGroups!=null && districtApprovalGroups.getGroupMembers()!=null)
						if(districtApprovalGroups.getGroupMembers().contains("#"+userKeyContact.getKeyContactId()+"#")){
							saAsWithAssociatedJob=true;
							break;
							}
						}
					}	
				}
			}
			if(jobOrder.getApprovalBeforeGoLive()!=null && jobOrder.getApprovalBeforeGoLive().equals(1)){
				if(entityID==2)
					jobLabelText.put(jobOrder.getJobId(), "&nbsp;|Job Approved");	
				if(entityID==3)
				if(saAsWithAssociatedJob){
					jobLabelText.put(jobOrder.getJobId(), "&nbsp;|Job Approved");
				}
			}
				
			else
			{
				boolean approvalFlag=false;
				boolean requestFlag=false;
				boolean pendingFlag=false;
			//	JobApprovalProcess jbap=	jobWiseProcessFlowMap.get(jobOrder);
				boolean currentApproval=true;
				Integer jobCreatorUserId=	jobOrder.getCreatedBy();
				DistrictKeyContact dkc=creatorDistrictkeycontact.get(jobCreatorUserId);			
				List<JobApprovalHistory> jobapphistory =jobWiseApproveHistoryMap.get(jobOrder);
				DistrictApprovalGroups currentApprovalGroup=null;
				List<Integer>	groupIds=jobProcessForGroupId.get(jbap);
				if(groupIds!=null && groupIds.size()>0){
					for(Integer districtApprovalGroupsId:groupIds){
						DistrictApprovalGroups districtApprovalGroups=districtApprovalGroupsMap.get(districtApprovalGroupsId);
						currentApproval=true;
							
						if(jobapphistory!=null && jobapphistory.size()>0){ 
						for(JobApprovalHistory jobh:jobapphistory){
								if(districtApprovalGroups!=null &&jobh!=null)
								if(jobh.getDistrictApprovalGroups()!=null)	
								if(jobh.getDistrictApprovalGroups().getDistrictApprovalGroupsId()==districtApprovalGroups.getDistrictApprovalGroupsId()){
									currentApproval=false;
									break;
								}
							}
						
							if(currentApproval){
								if(dkc!=null){
								if(districtApprovalGroups!=null && districtApprovalGroups.getGroupMembers()!=null)
								if(!districtApprovalGroups.getGroupMembers().contains("#"+dkc.getKeyContactId()+"#")){
									currentApprovalGroup=districtApprovalGroups;
									break;
								}
								}else{
									currentApprovalGroup=districtApprovalGroups;
									break;
								}
							}
						} else{
							if(dkc!=null){
								if(districtApprovalGroups!=null && districtApprovalGroups.getGroupMembers()!=null)
								if(!districtApprovalGroups.getGroupMembers().contains("#"+dkc.getKeyContactId()+"#")){
									currentApprovalGroup=districtApprovalGroups;
									break;
								}
							}else{
								currentApprovalGroup=districtApprovalGroups;
								break;
							}
						}
					}
					
				}	
				if(currentApprovalGroup!=null && currentApprovalGroup.getGroupMembers()!=null){
				if(currentApprovalGroup.getGroupMembers().contains("#"+userKeyContact.getKeyContactId()+"#")){
					requestFlag=true;
				} else{
					pendingFlag=true;
				}
				}else{
					pendingFlag=true;
				}
				if(requestFlag){
					jobLabelText.put(jobOrder.getJobId(), "&nbsp;|<a href='javascript:void(0);' onclick=\"return approveJob("+entityID+","+jobOrder.getJobId()+","+user.getUserId()+")\">Approval Request</a>");
				}
				if(pendingFlag){
					if(entityID==2)
					jobLabelText.put(jobOrder.getJobId(), "&nbsp;|Pending Others");
					if(entityID==3)
						if(saAsWithAssociatedJob){
							jobLabelText.put(jobOrder.getJobId(), "&nbsp;|Pending Others");
						}
				}
			}
			}
			}else
				System.out.println("This is not a Adams district");
			}
			catch(Exception e){
				e.printStackTrace();
				}
			return jobLabelText;
}
public String  getApprovalInfoDetailsForAdams(Integer jobId,Integer approvalprocessId,Integer districtId){
	/* ========  For Session time Out Error =========*/
//	System.out.println("calling getApprovalInfoDetails::::::::::"+districtId);
	WebContext context;
	context = WebContextFactory.get();
	HttpServletRequest request = context.getHttpServletRequest();
	HttpSession session = request.getSession(false);
	if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
	{
		throw new IllegalStateException("Your session has expired!");
	} 
	StringBuffer saBuffer =	new StringBuffer();
	
	try{
		DistrictMaster districtMaster=null;
		if(districtId!=null)
		 districtMaster = districtMasterDAO.findByDistrictId(districtId.toString());
		if(approvalprocessId!=null){
		//Map<Integer ,List<Integer>> jobProcessForGroupId= new LinkedHashMap<JobApprovalProcess, List<Integer>>();
		//	Map<JobApprovalProcess ,List<DistrictApprovalGroups>> jobProcessFlowMapGroups= new LinkedHashMap<JobApprovalProcess, List<DistrictApprovalGroups>>();
			Map<Integer ,List<Integer>> groupKeyList= new LinkedHashMap<Integer, List<Integer>>();
			//Map<Integer ,List<DistrictKeyContact>> groupKeyContactList= new LinkedHashMap<Integer, List<DistrictKeyContact>>();
			Map<Integer, DistrictKeyContact> districtkeyContactMap=new LinkedHashMap<Integer, DistrictKeyContact>();
			Map<Integer ,DistrictApprovalGroups> jobProcessFlowMapGroups= new LinkedHashMap<Integer ,DistrictApprovalGroups>();
			JobApprovalProcess jobApprovalProcess =	jobApprovalProcessDAO.findById(approvalprocessId, false, false);
			if(jobApprovalProcess!=null){
			List<Integer> lst=new ArrayList<Integer>();	
			String groupIds[]=null;
			String approvalIds=	jobApprovalProcess.getJobApprovalGroups();
				groupIds=approvalIds.split("\\|");
				List<Integer> groupApprovalIds=new ArrayList<Integer>();
				if(groupIds!=null)
				{
					for(String goupid:groupIds){
						
						if(!goupid.equals("")){
							try{
								Integer grpid = Integer.parseInt(goupid);
								groupApprovalIds.add(grpid);
							}catch(Exception e){e.printStackTrace();}
						}
					}
				}
			//	jobProcessForGroupId.put(jobApprovalProcess.getJobApprovalProcessId(), groupApprovalIds);
				List<DistrictApprovalGroups> districtApprovalGroupsList=null;
				List<Integer> allKeyContactId = new ArrayList<Integer>();
				if(groupApprovalIds.size()>0)
				districtApprovalGroupsList=	districtApprovalGroupsDAO.findGroupsByDistrictAndDistrictApprovalGroupsId(districtMaster,groupApprovalIds);
				if(districtApprovalGroupsList!=null &&districtApprovalGroupsList.size()>0){
				for(DistrictApprovalGroups districtApprovalGroups:districtApprovalGroupsList){
					String groupMembers=districtApprovalGroups.getGroupMembers();
					List<Integer> groupMemberIds = new ArrayList<Integer>();
					if(groupMembers!=null)		
					for(String id:groupMembers.split("#")){
								if(!id.equals(""))
									try{
										groupMemberIds.add(Integer.parseInt(id));
										}catch(NumberFormatException e){}
									
								}
							allKeyContactId.addAll(groupMemberIds);
							groupKeyList.put(districtApprovalGroups.getDistrictApprovalGroupsId(), groupMemberIds);
							jobProcessFlowMapGroups.put(districtApprovalGroups.getDistrictApprovalGroupsId(), districtApprovalGroups);
					}
				List<DistrictKeyContact> approvalContacts = districtKeyContactDAO.findByCriteria(Restrictions.in("keyContactId", allKeyContactId));
				if(approvalContacts!=null && approvalContacts.size()>0){
					for(DistrictKeyContact districtKeyContact:approvalContacts){
						districtkeyContactMap.put(districtKeyContact.getKeyContactId(), districtKeyContact);
					}
//					for(Map.Entry<Integer ,List<Integer>> entry:groupKeyList.entrySet()){
//						List<DistrictKeyContact> dkc = new ArrayList<DistrictKeyContact>();
//						for(Integer id:entry.getValue()){
//							dkc.add(districtkeyContactMap.get(id));
//						}
//						groupKeyContactList.put(entry.getKey(), dkc);
//					}
				}
			for(Integer districtApprovalGroupId:groupApprovalIds){
					DistrictApprovalGroups dsgp=jobProcessFlowMapGroups.get(districtApprovalGroupId);
				//for(DistrictApprovalGroups dsgp:districtApprovalGroupsList){
					boolean flag=true;
					int i=1;
					List<Integer> groupmemberid=groupKeyList.get(dsgp.getDistrictApprovalGroupsId());
					//List<DistrictKeyContact> districtKeyContactList=groupKeyContactList.get(dsgp.getDistrictApprovalGroupsId());
					for(Integer memberid:groupmemberid){
					DistrictKeyContact districtKeyContact=districtkeyContactMap.get(memberid);
						if(flag){
							saBuffer.append("<B>"+dsgp.getGroupName()+"</B>");
							saBuffer.append("<table id='saList' width=\"100%\" border=\"0\" class=\"table table-bordered span11 mt10\"><thead class=\"bg\"><tr><th>First Name</th><th>Last Name</th><th>Email Address</th></tr></thead><tbody>");
							flag=false;
						}
						String css="";
						if(i%2==0)
							css="";
						else
							css=" bgcolor='#F2FAEF'";
						if(districtKeyContact!=null){
							saBuffer.append("<tr"+css+">");
							saBuffer.append("<td>"+districtKeyContact.getKeyContactFirstName()+"</td>");
							saBuffer.append("<td>"+districtKeyContact.getKeyContactLastName()+"</td>");
							saBuffer.append("<td>"+districtKeyContact.getKeyContactEmailAddress()+"</td>");
							saBuffer.append("</tr>");
							i++;
						}
					}
					if(!flag){
						saBuffer.append("</table>");
					}
				}
			}
				
			}
		}
	}catch(Exception ex){
		ex.printStackTrace();
	}
	
	return saBuffer.toString();
	}

//==================== Kumar Avinash Optimization ============================ 

public String getStatusList_New_Op(int jobId,Integer districtId,Integer jobCategoryId){

	
	StringBuffer sb=new StringBuffer();
	try{
		Map<Integer,SecondaryStatus> mapSStatus = new HashMap<Integer, SecondaryStatus>();
		List<StatusMaster> statusMasterList	=new ArrayList<StatusMaster>();
		JobOrder jobOrder=null;
		
		List<StatusMaster> lstStatusMaster	=	null;
		List<SecondaryStatus> lstSecondaryStatus=null;
		DistrictMaster districtMaster=null;
		JobCategoryMaster jobCategoryMaster=null;
		//String[] statuss = {"scomp","ecomp","vcomp"};
		//lstStatusMaster	=	statusMasterDAO.findStatusByStatusByShortNames(statuss);
		if(jobCategoryId!=null){
//Added by kumar AVINASH			  
			districtMaster = districtMasterDAO.getDistrictMasterByDistrictId_Op(districtId);
		  	  jobCategoryMaster = jobCategoryMasterDAO.getCategoryMasterByCategory_Op(jobCategoryId);
			
			//jobCategoryMaster=jobCategoryMasterDAO.findById(jobCategoryId, false, false); 
			//districtMaster=districtMasterDAO.findById(districtId, false, false);
//Added by kumar avinash
			/*if(request.getRequestURI()!=null && !request.getRequestURI().equals("") && request.getRequestURI().equals("/teachermatch/dwr/call/plaincall/ManageJobOrdersAjax.displaySchool.dwr"))
			*/	
			lstSecondaryStatus=  secondaryStatusDAO.findSecondaryStatusByJobCategory_OP(districtMaster,jobCategoryMaster);
			//else
				//lstSecondaryStatus=  secondaryStatusDAO.findSecondaryStatusByJobCategory(districtMaster,jobCategoryMaster);
		}
		/*SecondaryStatus secondaryStatus=null;
		for(StatusMaster sm: lstStatusMaster){
			if(sm.getBanchmarkStatus()==1){  		check banchmarkStatus required
				secondaryStatus=mapSStatus.get(sm.getStatusId());
				if(secondaryStatus!=null){
					sm.setStatus(secondaryStatus.getSecondaryStatusName());
				}
			}
			statusMasterList.add(sm);
		}*/
		
		if(jobId!=0){
			jobOrder=jobOrderDAO.findById(jobId, false, false);
			if(jobCategoryId!=null){
				int x=1;
				sb.append("<div class='left20'>");
				sb.append("<input type='hidden' id='noofstatusCheckbox' value='"+statusMasterList.size()+"'>");
				/*
				List<JobWisePanelStatus>lstStatus=jobWisePanelStatusDAO.checkStatusJobOrderWise(jobOrder);
				Map<Integer, Boolean>stMap=new HashMap<Integer, Boolean>();
				for(JobWisePanelStatus jps:lstStatus){
					stMap.put(jps.getStatusMaster().getStatusId(),jps.getPanelStatus());
				}
				for(StatusMaster statusMaster:statusMasterList){
					sb.append("<div class='span3' style='padding-left: 10px;'>");
					boolean rs=false;
					if(stMap.size()>0){
						if(stMap.get(statusMaster.getStatusId())!=null){
							rs=stMap.get(statusMaster.getStatusId());
						}
					}
					if(rs){
						sb.append("<label class='checkbox inline'><input type='checkbox' checked=checked name='status' id='status_"+x+"' value='"+statusMaster.getStatusId()+"'/>"+statusMaster.getStatus()+"</label><br>");
					}	
					else{
						sb.append("<label class='checkbox inline'><input type='checkbox' name='status' id='status_"+x+"' value='"+statusMaster.getStatusId()+"'/>"+statusMaster.getStatus()+"</label><br>");
					}	
					sb.append("</div>");
					++x;
				}*/
				
				x=1;
				sb.append("<input type='hidden' id='noofsecStatusCheckbox' value='"+lstSecondaryStatus.size()+"'>");
//Addedby kumar avinash		
				List<JobWisePanelStatus>lstSecStatus=jobWisePanelStatusDAO.checkSecStatusJobOrderWise_Op(jobOrder);
				Map<Integer, Boolean>secStsMap=new HashMap<Integer, Boolean>();
				for(JobWisePanelStatus jps:lstSecStatus){
					 
					secStsMap.put(jps.getSecondaryStatus().getSecondaryStatusId(),jps.getPanelStatus());
				}
				for(SecondaryStatus secsts:lstSecondaryStatus){
					
					//Anurag optimization   if(checkSecondaryStatus(secsts.getSecondaryStatusId())){
//Added by kumar avinash	
					 if(!(secsts!=null && ((secsts.getStatusMaster().getStatusId()!=0 && secsts.getStatusMaster().getStatusShortName().equalsIgnoreCase("apl"))  || secsts.getStatusNodeMaster().getStatusNodeId()!=0))){
							System.out.println("::::::::::Anurag replacement of checkSecondaryStatus() ::::::::::::::");
							sb.append("<div class='col-sm-3 col-md-3 top5'>");
							boolean rs=false;
							if(secStsMap.size()>0){
								if(secStsMap.get(secsts.getSecondaryStatusId())!=null){
									rs=secStsMap.get(secsts.getSecondaryStatusId());
								}	
							}
							if(rs){
								sb.append("<label class='checkbox inline' style='padding-left: 0px;'><input type='checkbox' checked=checked name='secstatus' id='secstatus_"+x+"' value='"+secsts.getSecondaryStatusId()+"'/>"+secsts.getSecondaryStatusName()+"</label><br>");
							}	
							else{
								sb.append("<label class='checkbox inline' style='padding-left: 0px;'><input type='checkbox' name='secstatus' id='secstatus_"+x+"' value='"+secsts.getSecondaryStatusId()+"'/>"+secsts.getSecondaryStatusName()+"</label><br>");
							}
							
							sb.append("</div>");
							++x;
						}
							
							
	           /*  	else
					if(!(secsts!=null && ((secsts.getStatusMaster()!=null && secsts.getStatusMaster().getStatusShortName().equalsIgnoreCase("apl"))  || secsts.getStatusNodeMaster()!=null))){
			
					 System.out.println("::::::::::Anurag replacement of checkSecondaryStatus() ::::::::::::::");
						sb.append("<div class='col-sm-3 col-md-3 top5'>");
						boolean rs=false;
						if(secStsMap.size()>0){
							if(secStsMap.get(secsts.getSecondaryStatusId())!=null){
								rs=secStsMap.get(secsts.getSecondaryStatusId());
							}	
						}
						if(rs){
							sb.append("<label class='checkbox inline' style='padding-left: 0px;'><input type='checkbox' checked=checked name='secstatus' id='secstatus_"+x+"' value='"+secsts.getSecondaryStatusId()+"'/>"+secsts.getSecondaryStatusName()+"</label><br>");
						}	
						else{
							sb.append("<label class='checkbox inline' style='padding-left: 0px;'><input type='checkbox' name='secstatus' id='secstatus_"+x+"' value='"+secsts.getSecondaryStatusId()+"'/>"+secsts.getSecondaryStatusName()+"</label><br>");
						}
						
						sb.append("</div>");
						++x;
					}*/
						
						
				}
				sb.append("</div>");
			}
		}else{
			if(jobCategoryId!=null){
			sb.append("<div class='left20'>");
				sb.append("<input type='hidden' id='noofstatusCheckbox' value='"+statusMasterList.size()+"'>");
				int x=1;
				Map<Integer, Boolean> secStatusMap=getJobCategoryWiseStatusPrevilege(districtMaster,jobCategoryMaster);
				/*for(StatusMaster statusMaster:statusMasterList){
					sb.append("<div class='span3' style='padding-left: 10px;'>");
					if(secStatusMap.get(statusMaster.getStatusId())!=null){
						if(secStatusMap.get(statusMaster.getStatusId())){
							sb.append("<label class='checkbox inline'><input type='checkbox' checked='checked' name='status' id='status_"+x+"' value='"+statusMaster.getStatusId()+"'/>"+statusMaster.getStatus()+"</label><br>");
						}else{
							sb.append("<label class='checkbox inline'><input type='checkbox' name='status' id='status_"+x+"' value='"+statusMaster.getStatusId()+"'/>"+statusMaster.getStatus()+"</label><br>");
						}
					}else{
						sb.append("<label class='checkbox inline'><input type='checkbox' name='status' id='status_"+x+"' value='"+statusMaster.getStatusId()+"'/>"+statusMaster.getStatus()+"</label><br>");
					}
					sb.append("</div>");
					++x;
				}*/
				x=1;
				sb.append("<input type='hidden' id='noofsecStatusCheckbox' value='"+lstSecondaryStatus.size()+"'>");
				
				for(SecondaryStatus secsts:lstSecondaryStatus){
					if(checkSecondaryStatus(secsts.getSecondaryStatusId())){
						sb.append("<div class='col-sm-3 col-md-3'>");
						if(secStatusMap.get(secsts.getSecondaryStatusId())!=null){
							if(secStatusMap.get(secsts.getSecondaryStatusId())){
								sb.append("<label class='checkbox inline' style='padding-left: 0px;'><input type='checkbox' checked='checked' name='secstatus' id='secstatus_"+x+"' value='"+secsts.getSecondaryStatusId()+"'/>"+secsts.getSecondaryStatusName()+"</label><br>");
							}else{
								sb.append("<label class='checkbox inline' style='padding-left: 0px;'><input type='checkbox' name='secstatus' id='secstatus_"+x+"' value='"+secsts.getSecondaryStatusId()+"'/>"+secsts.getSecondaryStatusName()+"</label><br>");
							}
						}else{
							sb.append("<label class='checkbox inline' style='padding-left: 0px;'><input type='checkbox' name='secstatus' id='secstatus_"+x+"' value='"+secsts.getSecondaryStatusId()+"'/>"+secsts.getSecondaryStatusName()+"</label><br>");
						}
						
						sb.append("</div>");
						++x;
					}
				}
				sb.append("</div>");
			}
		}
		
	}catch(Exception exception){
		exception.printStackTrace();
	}
	return sb.toString();

}


public String  displaySchool_New_Op(int jobOrderId,int JobOrderType){

/* ========  For Session time Out Error =========*/
//System.out.println("calling displaySchool");
WebContext context;
context = WebContextFactory.get();
HttpServletRequest request = context.getHttpServletRequest();
HttpSession session = request.getSession(false);
if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
{
	throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
}
String locale = Utility.getValueOfPropByKey("locale");
StringBuffer tmRecords =	new StringBuffer();
try{

	UserMaster userMaster = null;
	SchoolMaster schoolMaster=null;
	int roleId=0;
	if (session == null || session.getAttribute("userMaster") == null) {
		return "false";
	}else{
		userMaster=(UserMaster)session.getAttribute("userMaster");
		if(userMaster.getRoleId().getRoleId()!=null){
			roleId=userMaster.getRoleId().getRoleId();
		}
	}
	String roleAccess=null;
	try{
		if(JobOrderType==2){
			roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,27,"addeditjoborder.do",1);
		}else{
			roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,28,"addeditjoborder.do",2);
		}
	}catch(Exception e){
		e.printStackTrace();
	}

	//JobOrder jobOrder =jobOrderDAO.findById(jobOrderId,false,false);
	JobOrder jobOrder =jobOrderDAO.jobOrderByJobOrderId_Op(jobOrderId);
	//List<SchoolInJobOrder> listSchoolInJobOrder	= schoolInJobOrderDAO.findJobOrder(jobOrder);
	List<SchoolInJobOrder> listSchoolInJobOrder	= schoolInJobOrderDAO.findJobOrder_Op(jobOrder);
	
 	//Criterion criterion1 = Restrictions.eq("jobOrder", jobOrder);
	tmRecords.append("<input type='hidden' id='avlSchoolInJobOrder' value='"+listSchoolInJobOrder.size()+"'>");
	if(listSchoolInJobOrder.size()!=0){
		//if(jobOrder.getDistrictMaster().getIsReqNoRequired())
		if(jobOrder.getDistrictMaster().getDistrictId().equals(804800))
			tmRecords.append("<table id=\"schoolTable\" width=\"100%\" border=\"0\" class=\"table table-bordered span11 mt10\"><thead class=\"bg\"><tr><th>"+Utility.getLocaleValuePropByKey("lblSchoolName", locale)+"</th><th>"+Utility.getLocaleValuePropByKey("lblAddedOn", locale)+"</th><th>"+Utility.getLocaleValuePropByKey("lblOfExpHi", locale)+"</th><th>"+Utility.getLocaleValuePropByKey("lblRequisitionNumber", locale)+"</th></tr></thead><tbody>");
		else
			tmRecords.append("<table id=\"schoolTable\" width=\"100%\" border=\"0\" class=\"table table-bordered span11 mt10\"><thead class=\"bg\"><tr><th>"+Utility.getLocaleValuePropByKey("lblSchoolName", locale)+"</th><th>"+Utility.getLocaleValuePropByKey("lblAddedOn", locale)+"</th><th>"+Utility.getLocaleValuePropByKey("lblOfExpHi", locale)+"</th><th>"+Utility.getLocaleValuePropByKey("lblRequisitionNumber", locale)+"<th>"+Utility.getLocaleValuePropByKey("lblActions", locale)+"</th></tr></thead><tbody>");				//else
		//else
			//tmRecords.append("<table id=\"schoolTable\" width=\"100%\" border=\"0\" class=\"table table-bordered span11 mt10\"><thead class=\"bg\"><tr><th>School Name</th><th>Added on</th><th> # of Expected Hire(s)</th><th>Actions</th></tr></thead><tbody>");
	}
	int noOfRecordCheck = 0;
	

	 
	
	
	/*================= Checking If Record Not Found ======================*/
	List<SchoolMaster> schoolMasters= new ArrayList<SchoolMaster>();
	for (SchoolInJobOrder schoolJobOrder : listSchoolInJobOrder){
		schoolMasters.add(schoolJobOrder.getSchoolId());
	}
	Map<Long,List<JobRequisitionNumbers>> jobReqMap= new HashMap<Long, List<JobRequisitionNumbers>>();
	List<JobRequisitionNumbers> jobRequisitionNumbers=null;
 	 jobRequisitionNumbers = jobRequisitionNumbersDAO.findJobRequisitionNumbersBySchoolInJobOrder_OP(jobOrder,schoolMasters);		
	/*else
	jobRequisitionNumbers = jobRequisitionNumbersDAO.findJobRequisitionNumbersBySchoolInJobOrder(jobOrder,schoolMasters);
*/	//System.out.println("jobRequisitionNumbers::::::::::::::>>>"+jobRequisitionNumbers.size());
	try{
		Long schoolId;
		for (JobRequisitionNumbers jobReqObj : jobRequisitionNumbers) {
			 
					  schoolId=jobReqObj.getSchoolMaster().getSchoolId();
			
			List<JobRequisitionNumbers> tAlist = jobReqMap.get(schoolId);
			if(tAlist==null){
				List<JobRequisitionNumbers> jobs = new ArrayList<JobRequisitionNumbers>();
				jobs.add(jobReqObj);
				jobReqMap.put(schoolId, jobs);
			}else{
				tAlist.add(jobReqObj);
				jobReqMap.put(schoolId, tAlist);
			}
		}
	}catch(Exception e){
		e.printStackTrace();
	}
	
	 
	
	//System.out.println("jobReqMap::::::::::::::>>>"+jobReqMap.size());
	List<TeacherAssessmentStatus> teacherAssessmentStatusList=teacherAssessmentStatusDAO.findHireStatusByJobAndSchools(jobOrder,schoolMasters);
	Map<Long,List<TeacherAssessmentStatus>> assMap= new HashMap<Long, List<TeacherAssessmentStatus>>();
	try{
		for (TeacherAssessmentStatus assObj : teacherAssessmentStatusList) {
			Long schoolId=assObj.getUpdatedBy().getSchoolId().getSchoolId();
			List<TeacherAssessmentStatus> tAlist = assMap.get(schoolId);
			if(tAlist==null){
				List<TeacherAssessmentStatus> assList = new ArrayList<TeacherAssessmentStatus>();
				assList.add(assObj);
				assMap.put(schoolId, assList);
			}else{
				tAlist.add(assObj);
				assMap.put(schoolId, tAlist);
			}
		}
	}catch(Exception e){
		e.printStackTrace();
	}
	String location = "";
	
	TestTool.getTraceSecond("First req start grid");
	for (SchoolInJobOrder schoolJobOrder : listSchoolInJobOrder){
			if(schoolJobOrder.getSchoolId().getLocationCode()!=null && schoolJobOrder.getSchoolId().getLocationCode()!="")
				location = " ("+schoolJobOrder.getSchoolId().getLocationCode()+")";

		noOfRecordCheck++;
		int countOfReqNo =0;
		try{
			if(jobReqMap.get(schoolJobOrder.getSchoolId().getSchoolId())!=null){
				countOfReqNo = jobReqMap.get(schoolJobOrder.getSchoolId().getSchoolId()).size();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		tmRecords.append("<tr id='"+schoolJobOrder.getSchoolId().getSchoolId()+"'>" );
		tmRecords.append("<td nowrap >"+schoolJobOrder.getSchoolId().getSchoolName()+location+"</td>");
		tmRecords.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(schoolJobOrder.getCreatedDateTime())+"</td>");
		tmRecords.append("<td>"+schoolJobOrder.getNoOfSchoolExpHires()+"</td>");
		//if(jobOrder.getDistrictMaster().getIsReqNoRequired())
			tmRecords.append("<td>"+countOfReqNo+"</td>");
		int val=0;
		try{
			if(assMap.get(schoolJobOrder.getSchoolId().getSchoolId())!=null){
				val=assMap.get(schoolJobOrder.getSchoolId().getSchoolId()).size();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		if(!jobOrder.getDistrictMaster().getDistrictId().equals(804800))
		if(val==0){
			if(userMaster.getEntityType()==3 && JobOrderType==2){
			//	tmRecords.append("<td nowrap>&nbsp;</td>");
			    if(jobOrder.getDistrictMaster().getDistrictId().equals(806900))
				tmRecords.append("<td nowrap><a href='javascript:void(0);' onclick='return editSchoolInJobOrder("+schoolJobOrder.getJobId().getJobId()+","+schoolJobOrder.getSchoolId().getSchoolId()+")'><span class='fa-pencil-square-o  icon-large  iconcolor'></span></a>&nbsp;&nbsp;&nbsp;&nbsp;");
			}else{
				if(roleAccess.indexOf("|3|")!=-1)
				{
					String sReqNumbers="";
					for(JobRequisitionNumbers pojo :jobRequisitionNumbers)
					{
				 		sReqNumbers=sReqNumbers+pojo.getDistrictRequisitionNumbers().getDistrictRequisitionId()+"#";
					}
					if(sReqNumbers.length() >1)
					{
						sReqNumbers=sReqNumbers.substring(0,sReqNumbers.length()-1);
					}
					//System.out.println("Edit sReqNumbers "+sReqNumbers);
				
					//Start sandeep 31-08-15
					
					
					 if(jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getHeadQuarterMaster() != null && jobOrder.getDistrictMaster().getHeadQuarterMaster().getHeadQuarterId() ==DashboardAjax.NC_HEADQUARTER){
							
						  Criterion criterionRequestion  = Restrictions.eq("jobOrder", jobOrder);
						  List<JobRequisitionNumbers> jobRequNumber = jobRequisitionNumbersDAO.findByCriteria(criterionRequestion);    
						  if(jobRequNumber != null && jobRequNumber.size() >0)
							  if(jobRequNumber.get(0).getDistrictRequisitionNumbers() != null)
								  tmRecords.append("<td></td>");
							  
					 }else{
						tmRecords.append("<td nowrap><a href='javascript:void(0);' onclick='return editSchoolInJobOrder("+schoolJobOrder.getJobId().getJobId()+","+schoolJobOrder.getSchoolId().getSchoolId()+")'><span class='fa-pencil-square-o  icon-large  iconcolor'></span></a>&nbsp;&nbsp;&nbsp;&nbsp;");
						tmRecords.append("<a href='javascript:void(0);' onclick='return deleteSchoolInJobOrder("+schoolJobOrder.getJobId().getJobId()+","+schoolJobOrder.getSchoolId().getSchoolId()+")'><span class='fa-trash-o  icon-large  iconcolor'></span></a></td>");
					 }	
					tmRecords.append("<td style='display:none;'>"+sReqNumbers+"</td>");
					tmRecords.append("<td style='display:none;'>"+schoolJobOrder.getSchoolId().getSchoolId()+"</td>");
					
				}
				else
				{
					tmRecords.append("<td nowrap>&nbsp;</td>");
				}
			}
		}else{
			tmRecords.append("<td nowrap>&nbsp;</td>");
		}
	}
	TestTool.getTraceSecond("First req end grid");
	if(listSchoolInJobOrder.size()!=0)
		tmRecords.append("</table>");
}catch (Exception e) 
{
	e.printStackTrace();
}

return tmRecords.toString();

}

public String getJobSubCategory_New_Op(Integer jobId,Integer jobCateId,Integer districtId,Integer clonejobId)
{
	System.out.println(" jobId "+jobId+" jobCateId "+jobCateId);
	
	StringBuffer sb	=	new StringBuffer();
	try {
		//DistrictMaster districtMaster	=	districtMasterDAO.findById(districtId, false, false);
		
		//DistrictMaster districtMaster = districtMasterDAO.findByDistrictId(districtId.toString());
		DistrictMaster districtMaster = districtMasterDAO.getDistrictMasterByDistrictId_Op(districtId);
		
		//JobCategoryMaster jobCategoryMaster = jobCategoryMasterDAO.findById(jobCateId, false, false);
		JobCategoryMaster jobCategoryMaster = jobCategoryMasterDAO.getCategoryMasterByCategory_Op(jobCateId);
		
		
		/*=== values to dropdown from jobCategoryMaster Table ===*/
		//List<JobCategoryMaster> jobCategoryMasterlst = jobCategoryMasterDAO.findAllJobSubCategoryByJobCateId(jobCategoryMaster,districtMaster);
		List<JobCategoryMaster> jobCategoryMasterlst = jobCategoryMasterDAO.findAllJobSubCategoryByJobCateId_Op(jobCategoryMaster,districtMaster);
		String jcjsiName="";
	
		if((jobId!=null && jobId>0) || (clonejobId!=null && clonejobId>0) )
		{
			JobOrder jobOrder = null;
		
			
			if(jobId!=null && jobId>0)
				jobOrder = jobOrderDAO.findById(jobId, false, false);
			
			if((clonejobId!=null && clonejobId>0))
				jobOrder = jobOrderDAO.findById(clonejobId, false, false);
			
			if(jobCategoryMasterlst.size()>0)
			{
				sb.append("<select id='jobSubCategoryId' name='jobSubCategoryId' class='form-control'  onchange='linkShowOrHide();'>");
				sb.append("<option value='0'>"+Utility.getLocaleValuePropByKey("msgSubJobCategoryName", locale)+"</option>");
				if(jobCategoryMasterlst!=null && jobCategoryMasterlst.size()>0){
					for(JobCategoryMaster jb: jobCategoryMasterlst)
					{
						if(jb.getAssessmentDocument()!=null)
						{
							jcjsiName	=	jb.getAssessmentDocument();
						}
						if(jobOrder.getJobCategoryMaster().getJobCategoryId().equals(jb.getJobCategoryId()))
							sb.append("<option linkShow='"+(jb.getApprovalByPredefinedGroups())+"' value='"+jb.getJobCategoryId()+"||"+jb.getBaseStatus()+"||"+jcjsiName+"' selected='selected'>"+jb.getJobCategoryName()+"</option>");
						else
							sb.append("<option linkShow='"+(jb.getApprovalByPredefinedGroups())+"' value='"+jb.getJobCategoryId()+"||"+jb.getBaseStatus()+"||"+jcjsiName+"'>"+jb.getJobCategoryName()+"</option>");
					}
				}

				sb.append("</select>");
			}
			
		}
		else
		{
			
			if(jobCategoryMasterlst.size()>0)
			{
				sb.append("<select id='jobSubCategoryId' name='jobSubCategoryId' class='form-control'  onchange='linkShowOrHide();'>");
				sb.append("<option value='0'>"+Utility.getLocaleValuePropByKey("msgSubJobCategoryName", locale)+"</option>");
				if(jobCategoryMasterlst!=null && jobCategoryMasterlst.size()>0){
					for(JobCategoryMaster jb: jobCategoryMasterlst)
					{
						if(jb.getAssessmentDocument()!=null)
						{
							jcjsiName	=	jb.getAssessmentDocument();
						}
						
						sb.append("<option linkShow='"+(jb.getApprovalByPredefinedGroups())+"' value='"+jb.getJobCategoryId()+"||"+jb.getBaseStatus()+"||"+jcjsiName+"'>"+jb.getJobCategoryName()+"</option>");
					}
				}

				sb.append("</select>");
			}
		}
		
		
		
	} catch (Exception e) 
	{
		e.printStackTrace();
	}

	return sb.toString();
}

public List<Integer> findJobIdSaAsApprovalProcess(UserMaster userMaster){
	DistrictKeyContact userKeyContact=null;
	List<Integer> saJobApprovberJobOrderList=new ArrayList<Integer>();
	try{
	List<DistrictKeyContact>	districtKeyContactList=districtKeyContactDAO.findByContactType(userMaster, "Job Approval");
	if(districtKeyContactList!=null &&districtKeyContactList.size()>0){
		userKeyContact=districtKeyContactList.get(0);
	}
	if(userKeyContact==null){
		return saJobApprovberJobOrderList;
	}
	List<Integer> attachedGroupId=new ArrayList<Integer>();
	List<JobApprovalProcess> attachedApprovalProcess=new ArrayList<JobApprovalProcess>();
	List<DistrictApprovalGroups> districtApprovalGroupsList=	districtApprovalGroupsDAO.findAllDistrictApprovalGroupsByDistrict(userMaster.getDistrictId());
		if(districtApprovalGroupsList!=null &&districtApprovalGroupsList.size()>0){
			for(DistrictApprovalGroups dags:districtApprovalGroupsList){
				if(dags!=null && dags.getGroupMembers()!=null && dags.getGroupMembers().contains("#"+userKeyContact.getKeyContactId()+"#")){
					attachedGroupId.add(dags.getDistrictApprovalGroupsId());	
				}
			}
			
		}
		if(attachedGroupId.size()>0){
			List<JobApprovalProcess> jobApprovalProcessList=jobApprovalProcessDAO.findByCriteria(Restrictions.eq("districtId",userMaster.getDistrictId()));
			if(jobApprovalProcessList!=null && jobApprovalProcessList.size()>0){
				for(JobApprovalProcess jbap:jobApprovalProcessList){
					String groupsId=jbap.getJobApprovalGroups();
					if(groupsId!=null && !groupsId.equals("")){
						for(Integer grpid:attachedGroupId){
							if(groupsId.contains("|"+grpid+"|")){
								attachedApprovalProcess.add(jbap);
								break;
							}
								
						}
					}
				}
			}
			if(attachedApprovalProcess.size()>0){
				List<JobWiseApprovalProcess>	jobWiseApprovalProcessList=	jobWiseApprovalProcessDAO.findByCriteria(Restrictions.in("jobApprovalProcessId", attachedApprovalProcess));
				if(jobWiseApprovalProcessList!=null && jobWiseApprovalProcessList.size()>0){
					for(JobWiseApprovalProcess jwap:jobWiseApprovalProcessList)
						if(jwap!=null)
							saJobApprovberJobOrderList.add(jwap.getJobId().getJobId());
				}
			}
		}
		
	}catch (Exception e) {
		e.printStackTrace();
	}
	
	return saJobApprovberJobOrderList;
}
public int checkUserExistOrNotInDistrictApprovalGroups(DistrictKeyContact userKeyContact){
	//DistrictKeyContact userKeyContact=null;
	if(userKeyContact==null){
		return 0;
	}
	try{
//		List<DistrictKeyContact>	districtKeyContactList=districtKeyContactDAO.findByContactType(userMaster, "Job Approval");
//		if(districtKeyContactList!=null &&districtKeyContactList.size()>0){
//			userKeyContact=districtKeyContactList.get(0);
//			}
		if(userKeyContact!=null){
			List<DistrictApprovalGroups> districtApprovalGroupsList=	districtApprovalGroupsDAO.findAllDistrictApprovalGroupsByDistrict(userKeyContact.getUserMaster().getDistrictId());
			if(districtApprovalGroupsList!=null && districtApprovalGroupsList.size()>0){
					for(DistrictApprovalGroups districtApprovalGroups:districtApprovalGroupsList){
						if(districtApprovalGroups!=null && districtApprovalGroups.getGroupMembers()!=null){
							if(districtApprovalGroups.getGroupMembers().contains("#"+userKeyContact.getKeyContactId()+"#")){
								return 1;
								}
							}
						
						}
					}
			}
	
	}catch (Exception e) {
		e.printStackTrace();
	}
	
	return 0;
}

}

