<!-- @Author: Sonu Gupta
	 @Discription: view of add or edit groups page.-->
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type="text/javascript" src="dwr/interface/DistrictAjax.js?ver=${resourceMap['DistrictAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/JobApprovalProcessAjax.js?ver=${resourceMap['JobApprovalProcessAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/DWRAutoComplete.js?ver=${resouceMap['DWRAutoComplete.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/ManageJobOrdersAjax.js?ver=${resourceMap['ManageJobOrdersAjax.ajax']}"></script>
<script type='text/javascript' src="js/manageJobApprovalProcess.js?ver=${resourceMap['js/manageJobApprovalProcess.js']}"></script>
<!-- Optionally enable responsive features in IE8 -->
<script type='text/javascript' src="js/bootstrap-slider.js"></script>
<link rel="stylesheet" type="text/css" href="css/slider.css" />  
<script src="calender/js/jscal2.js"></script>
<script src="calender/js/lang/en.js"></script>
<link rel="stylesheet" type="text/css" href="calender/css/jscal2.css" />
<link rel="stylesheet" type="text/css" href="calender/css/border-radius.css" />
<link rel="stylesheet" type="text/css" href="calender/css/gold/gold.css" />
<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" />  
<style>
.table th, .table td {
    padding: 8px;
    line-height: 20px;
    text-align: left;
    vertical-align: top;  
}
.divwidth
{
float: left;
width: 40px;
}
.input-group-addon {
    background-color: #fff;
	border: none;
}
</style>
<script type="text/javascript">
var $j=jQuery.noConflict();
        $j(document).ready(function() {
        });
        
function applyScrollOnTbl()
{
	var $j=jQuery.noConflict();
    $j(document).ready(function() {
        $j('#tblGrid').fixheadertable({ //table id 
	        caption: '',
	        showhide: false,
	        theme: 'ui',
	       //height: 400,
	        width: 945,
	        minWidth: null,
	        minWidthAuto: false,
	        colratio:[145,120,570,110],
	        addTitles: false,
	        zebra: true,
	        zebraClass: 'net-alternative-row',
	        sortable: false,
	        sortedColId: null,
	        dateFormat: 'd-m-y',
	        pager: false,
	        rowsPerPage: 10,
	        resizeCol: false,
	        minColWidth: 100,
	        wrapper: false
		});
	});	
}
</script>
<style>
	.hide
	{
		display: none;
	}
	.inline
	{
		display: inline;
	}
	.nowrap
	{
		white-space: nowrap
	}
	.setSlider
	{
		margin-left: -20px; 
		margin-top: -8px;
	}
	
	.tooltip {
  position: absolute;
  z-index: 1030;
  display: block;
  font-size: 12px;
  line-height: 1.4;
  opacity: 0;
  filter: alpha(opacity=0);
  visibility: visible;
}

.tooltip.in {
  opacity: 0.9;
  filter: alpha(opacity=90);
}

.tooltip.top {
  padding: 5px 0;
  margin-top: -3px;
}

.tooltip.right {
  padding: 0 5px;
  margin-left: 3px;
}

.tooltip.bottom {
  padding: 5px 0;
  margin-top: 3px;
}

.tooltip.left {
  padding: 0 5px;
  margin-left: -3px;
}

.tooltip-inner {
  max-width: 200px;
  padding: 3px 8px;
  color: #ffffff;
  text-align: center;
  text-decoration: none;
  background-color: #000000;
  border-radius: 4px;
}

.tooltip-arrow {
  position: absolute;
  width: 0;
  height: 0;
  border-color: transparent;
  border-style: solid;
}

.tooltip.top .tooltip-arrow {
  bottom: 0;
  left: 50%;
  margin-left: -5px;
  border-top-color: #000000;
  border-width: 5px 5px 0;
}

.tooltip.top-left .tooltip-arrow {
  bottom: 0;
  left: 5px;
  border-top-color: #000000;
  border-width: 5px 5px 0;
}

.tooltip.top-right .tooltip-arrow {
  right: 5px;
  bottom: 0;
  border-top-color: #000000;
  border-width: 5px 5px 0;
}

.tooltip.right .tooltip-arrow {
  top: 50%;
  left: 0;
  margin-top: -5px;
  border-right-color: #000000;
  border-width: 5px 5px 5px 0;
}

.tooltip.left .tooltip-arrow {
  top: 50%;
  right: 0;
  margin-top: -5px;
  border-left-color: #000000;
  border-width: 5px 0 5px 5px;
}

.tooltip.bottom .tooltip-arrow {
  top: 0;
  left: 50%;
  margin-left: -5px;
  border-bottom-color: #000000;
  border-width: 0 5px 5px;
}

.tooltip.bottom-left .tooltip-arrow {
  top: 0;
  left: 5px;
  border-bottom-color: #000000;
  border-width: 0 5px 5px;
}

.tooltip.bottom-right .tooltip-arrow {
  top: 0;
  right: 5px;
  border-bottom-color: #000000;
  border-width: 0 5px 5px;
}
</style>

<div class="row" style="margin:0px;">
         <div style="float: left;">
         	<img src="images/add-district.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;">Manage Job Approval Process</div>	
         </div>			
		
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
</div>

<input type="hidden" id="districtId" name="districtId" value="${districtMaster.districtId}"/>

<div class="row" style="margin-left: 0px;">
	<div id="errorDiv"> </div>
</div>

<div class="row top10" style="margin-bottom: 20px;">
	<div class="col-sm-6 col-md-6">
		<label id="captionDistrictOrSchool">District Name<font color="red">*</font></label>
		<c:if test="${DistrictOrSchoolName==null}">
			<span>
				<input type="text" maxlength="255" id="districtORSchoolName" name="districtORSchoolName" class="help-inline form-control"
					   onfocus="getDistrictORSchoolAuto(this, event, 'divTxtShowData', 'districtORSchoolName','districtOrSchoolId','');"
				   	   onkeyup="getDistrictORSchoolAuto(this, event, 'divTxtShowData', 'districtORSchoolName','districtOrSchoolId','');"
				   	   onblur="hideDistrictMasterDiv(this,'districtOrSchoolId','divTxtShowData');"/>
			</span>
			<input type="hidden" id="districtOrSchooHiddenlId"/>							
		</c:if>
		<c:if test="${DistrictOrSchoolName!=null}"> 
			<span>
		    	<input type="text" maxlength="255"  class="help-inline form-control" value="${DistrictOrSchoolName}"  disabled="disabled"/>
			</span>	             	
			<input type="hidden" id="districtOrSchooHiddenlId" value="${DistrictOrSchoolId}"/>
			<input type="hidden" id="districtORSchoolName" name="districtORSchoolName"/>
			<input type="hidden" id="districtHiddenlIdForSchool"/>
		</c:if>
		<c:if test="${DistrictOrSchoolName==null && DistrictOrSchoolId!=null}">
			<input type="hidden" id="districtHiddenlIdForSchool" value="${DistrictOrSchoolId}"/>
		</c:if>
		<c:if test="${DistrictOrSchoolName==null && DistrictOrSchoolId==null}">
			<input type="hidden" id="districtHiddenlIdForSchool"/>
		</c:if>
		<input type="hidden" id="schoolName" name="schoolName" value="${schoolName}"/>
       	<input type="hidden" id="schoolId" name="schoolId" value="${schoolId}"/>
		<div id='divTxtShowData'  onmouseover="mouseOverChk('divTxtShowData','districtORSchoolName')" style=' display:none;position:absolute;z-index:5000;' class='result' ></div>			
	</div>
	<div class="col-sm-2 col-md-2 top25-sm2" style="width: 150px;text-align:right;padding:0px;">
		<label class=""></label>
		<button class="btn btn-primary " type="button" onclick=" searchByDistrict();">&nbsp;Search&nbsp;&nbsp; <i class="icon"></i></button>
	</div>
</div>
<input type="hidden" id="approvalProcessID" name="approvalProcessID"/>
<div class="row" style="padding-right: 20px;">
	 <div class="pull-right add-employment1">
			<a href="javascript:void(0);" onclick="showAddGroup()">+ Add Job Approval Process</a>
		 </div>	
</div>

<div class="TableContent">
	<div class="table-responsive" id="divMain" ></div>       
</div>
		
<div class="row mt10" style="margin-left: 32px;">
	<div id="privilegeForDistrictErrorDiv"> </div>
</div>
		
		<div id="groupMembersList" class="hideGroup" style="display: block;margin-left: 21px;margin-top: -11px;"> </div>
                    
                    <div class="bs-example bs-example-form" id="addApprovalGroupAndMemberDiv" class="hideGroup">
        	<div class="row" style="margin-left: -20px;margin-top: 15px;">
            	<input type="hidden" value="1" id="noOfApproval" name="noOfApproval" class="form-control"  maxlength="4">
                	<div class="col-lg-3">
                		<label class="left30">Job Approval Process  </label>
                		<div class="input-group">
                			<span class="input-group-addon">
                				<input type="radio" name="approvalRadioButtonId" id="approvalRadioButtonId1" value="radioApprovalButton1" checked="checked" onclick="approvalRadioButton()">
                			</span>
                			<input type="text" class="form-control" name="addApprovalName" id="addApprovalName">
                		</div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                	</div>
                	<div class="col-lg-3">
                		<label class="left30">Select Job Approval Process </label>
                		<div class="input-group">
                			<span class="input-group-addon">
                				<input type="radio" name="approvalRadioButtonId" id="approvalRadioButtonId2" value="radioApprovalButton2" onclick="approvalRadioButton()">
                			</span>
               				<select class="form-control" id="approvalName" name="approvalName">
               				</select>
               			</div>
               		</div>	
               		<div class="col-lg-3">
                		<label class="left0">Total Approvals Required</label>                		
                		<select id="approvalRequired" name="approvalRequired" class="form-control">
						  <option value="1">1</option>
						  <option value="2">2</option>
						  <option value="3">3</option>
						  <option value="4">4</option>
						  <option value="5">5</option>
						  <option value="6">6</option>
						  <option value="7">7</option>
						  <option value="8">8</option>
						  <option value="9">9</option>
						  <option value="10">10</option>
						</select>
                	</div>
               			
			</div>         
		 
		   
		   <div class="row left15">		 
		 <div class="col-sm-4 col-md-4" style="border: 0px solid red;">
		 		<label> Available Job Approval Groups </label>
               			<div class="input-group">
               				<select multiple="" class="form-control" id="groupList" name="groupList" style="height: 157px; min-width: 262px;">
               				</select>
               			</div>
			</div>		
			<div class="col-sm-1 col-md-1" style="padding-top: 80px;"> 
				<div class="span2"> <span id="addPop" style="cursor:pointer;" onclick="addSelectedGroupList();"><i class="fa fa-arrow-right fa-2x" style="cursor:pointer; color: #007AB4;" data-toggle='tooltip' title='Add Group'></i></span></div>
				<div class="span2"> <span id="removePop" style="cursor:pointer;" onclick="removeSelectedGroupList();"><i class="fa fa-arrow-left fa-2x" style="cursor:pointer; color: #007AB4;" data-toggle='tooltip' title='Remove Group'></i></span></div>
			</div>
			<div class="col-sm-4 col-md-4" style="border: 0px solid green;">
				<label>Selected Job Approval Groups</label>	
				<select size="8" class="form-control" id="attachedGroupList" name="attachedGroupList">
				   
				</select>
			</div>	
			<div class="col-sm-1 col-md-1" style="padding-top: 80px;"> 
				<div class="span2"> <i class="fa fa-arrow-up fa-2x" style="cursor:pointer; color: #007AB4;" onclick="upSelectedGroupList();" data-toggle='tooltip' title='Up'></i></div>
				<div class="span2"> <i class="fa fa-arrow-down fa-2x" style="cursor:pointer; color: #007AB4;" onclick="downSelectedGroupList();" data-toggle='tooltip' title='Down'></i></div>
			</div>
		  </div>
            <div class="row idone" style="margin-top: 15px; margin-left: 10px;">
               	<a href='javascript:void(0);' onclick="addJobApprovalProcessORGroup()"> <spring:message code="lnkImD"/> </a> &nbsp;&nbsp; 
               	<a href='javascript:void(0);' onclick="hideAddApprovalGroupAndMemberDiv()"> Cancel </a>
			</div>
		</div>

<div  class="modal hide"  id="myModalMsg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"   style="z-index: 5000;"  data-backdrop="static">
		<div class="modal-dialog">
		<div class="modal-content">
		<div class="modal-header">
	  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick="return aKeyContactDiv(0);">x</button>
			<h3 id="myModalLabel">Key Contact</h3>
		</div>
		<div class="modal-body">		
			<div class="control-group">
				<div class="">
			    	<span id="Msg"></span>	        	
				</div>
			</div>
	 	</div>
	 	<div class="modal-footer">
	 		<button class="btn" data-dismiss="modal" aria-hidden="true"  onclick="return aKeyContactDiv(1);">Ok</button>
	 		<button class="btn" data-dismiss="modal" aria-hidden="true"  onclick="return aKeyContactDiv(0);">Close</button> 		
 		</div>
	</div>
	</div> 
	</div>

	<div style="display:none;" id="loadingDiv">
		<table  align="center" >
			 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
		</table>
</div>

<script> 
	displayApprocalProcess();
	displayKeyContact();
</script>

<script type="text/javascript">
$('#myModal').modal('hide');

$(document).ready(function(){
  		$('#eMessage').find(".jqte").width(724);
  		$('#dNote').find(".jqte").width(724);
  		//$('#dDescription').find(".jqte").width(535);
}) 

</script>
<script>
 var sendNotificationOnNegativeQQ322 = '${sendNotificationOnNegativeQQValue}';
$(document).ready(function(){
	
	if(sendNotificationOnNegativeQQ322!=null)
		if(sendNotificationOnNegativeQQ322!="")
			$("#sendNotificationOnNegativeQQId").attr("checked","checked");
	
	buildApprovalGroup322();
	//displayAllActiveDistrictUser();
});
function buildApprovalGroup322()
{
	var isCheckedBuildApprovalGroups = $("#buildApprovalGroups").attr("checked");
 	var isCheckedApprovalBeforeGoLive = $("#approvalBeforeGoLive1").attr("checked");
	
	groupRadioButton();
	$(".hideGroup").hide();
	if(isCheckedApprovalBeforeGoLive!=null)
	{
		if(isCheckedApprovalBeforeGoLive=="checked")
		{
			$("#buildApprovalGroupsDivId").show();
			$("#buildApprovalGroups").show();
			$("#addGroupHyperLink").show();
			$("#groupMembersList").hide();
			$("#addApprovalGroupAndMemberDiv").hide();
			
			buildApprovalGroup();
			$("#buildApprovalGroupsDivId").show();
			$("#buildApprovalGroups").show();
			$("#addGroupHyperLink").show();
			$("#addApprovalGroupAndMemberDiv").hide();
		}
	}
	else
	{
		$("#buildApprovalGroupsDivId").hide();
		$("#buildApprovalGroups").hide();
		$("#addGroupHyperLink").hide();
		$("#groupMembersList").hide();
		$("#addApprovalGroupAndMemberDiv").hide();
	}
}

function fileContainsVirusDiv(msg)
{
	$('#loadingDiv').hide();
	$('#virusDivId .modal-body').html(msg);
	$('#virusDivId').modal('show');
}
</script>

<div class="modal hide" id="virusDivId" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog-for-cgcoverletter" style='width: 35%;'>
		<div class="modal-content">
			<div class="modal-header">
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body" style="max-height: 400px;overflow-y:auto;">
				<div class="control-group">
				</div>
			</div>
			<div class="modal-footer">
				<span id=""><button class="btn btn-primary" data-dismiss="modal" onclick="showAddGroup();">Ok <i class="icon"></i></button></span>&nbsp;&nbsp;
			</div>
		</div>
	</div>
</div>

<div class="modal hide" id="removeGroupConfirmation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false" removeApprovalGroupId="" removeGroupId="">
	<div class="modal-dialog-for-cgcoverletter" style='width: 35%;'>
		<div class="modal-content">
			<div class="modal-header">
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body" style="max-height: 400px;overflow-y:auto;">
				<div class="control-group">
					Do you want to remove this Group?
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-primary" data-dismiss="modal" onclick="removeGroup()">Ok <i class="icon"></i></button>&nbsp;
				<button class="btn" data-dismiss="modal">Cancel</button> &nbsp;&nbsp;
			</div>
		</div>
	</div>
</div>

<div class="modal hide" id="removeJobApprovalProcessConfirmation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false" removeJobApprovalProcessId="">
	<div class="modal-dialog-for-cgcoverletter" style='width: 35%;'>
		<div class="modal-content">
			<div class="modal-header">
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body" style="max-height: 400px;overflow-y:auto;">
				<div class="control-group">
					Do you want to remove this Job Approval Process?
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-primary" data-dismiss="modal" onclick="removeJobApprovalProcess()">Ok <i class="icon"></i></button>&nbsp;
				<button class="btn" data-dismiss="modal">Cancel</button> &nbsp;&nbsp;
			</div>
		</div>
	</div>
</div>

<div class="modal hide" id="addConfirmation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false" removeJobApprovalProcessId="">
	<div class="modal-dialog-for-cgcoverletter" style='width: 35%;'>
		<div class="modal-content">
			<div class="modal-header">
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body" style="max-height: 400px;overflow-y:auto;">
				<div class="control-group">
					Successfully Add Group in the Job Approval Process.
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-primary" data-dismiss="modal" onclick="displayApprocalProcess()">Ok <i class="icon"></i></button>&nbsp;
			</div>
		</div>
	</div>
</div>
<div class="modal hide" id="deactivateActivateJobApprovalProcessConfirmation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false" deactivateActivateJobApprovalProcessId="",status="">
	<div class="modal-dialog-for-cgcoverletter" style='width: 35%;'>
		<div class="modal-content">
			<div class="modal-header">
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body" style="max-height: 400px;overflow-y:auto;">
				<div class="control-group" id="deactivateBody">
					
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-primary" data-dismiss="modal" onclick="deactivateAndActivateJobApprovalProcess()">Ok <i class="icon"></i></button>&nbsp;
				<button class="btn" data-dismiss="modal">Cancel</button> &nbsp;&nbsp;
			</div>
		</div>
	</div>
</div>
