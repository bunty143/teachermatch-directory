package tm.services;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.time.DateUtils;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import edu.emory.mathcs.backport.java.util.Arrays;
import tm.bean.DistrictApprovalGroups;
import tm.bean.DistrictRequisitionNumbers;
import tm.bean.DistrictSpecificApprovalFlow;
import tm.bean.DistrictWiseApprovalGroup;
import tm.bean.EmploymentServicesTechnician;
import tm.bean.JobApprovalHistory;
import tm.bean.JobApprovalNotes;
import tm.bean.JobApprovalProcess;
import tm.bean.JobCertification;
import tm.bean.JobDenyApprovalHistory;
import tm.bean.JobOrder;
import tm.bean.JobRequisitionNumbers;
import tm.bean.JobWiseApprovalProcess;
import tm.bean.SchoolInJobOrder;
import tm.bean.TeacherAssessmentStatus;
import tm.bean.cgreport.TeacherStatusNotesHistory;
import tm.bean.master.DistrictKeyContact;
import tm.bean.master.DistrictMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.JobWisePanelStatus;
import tm.bean.master.SchoolMaster;
import tm.bean.master.SecondaryStatus;
import tm.bean.master.StatusMaster;
import tm.bean.user.UserMaster;
import tm.dao.DistrictApprovalGroupsDAO;
import tm.dao.DistrictRequisitionNumbersDAO;
import tm.dao.DistrictSpecificApprovalFlowDAO;
import tm.dao.DistrictWiseApprovalGroupDAO;
import tm.dao.EmploymentServicesTechnicianDAO;
import tm.dao.JobApprovalHistoryDAO;
import tm.dao.JobApprovalNotesDAO;
import tm.dao.JobCertificationDAO;
import tm.dao.JobDenyApprovalHistoryDAO;
import tm.dao.JobOrderDAO;
import tm.dao.JobRequisitionNumbersDAO;
import tm.dao.JobWiseApprovalProcessDAO;
import tm.dao.SchoolInJobOrderDAO;
import tm.dao.TeacherAssessmentStatusDAO;
import tm.dao.master.DistrictKeyContactDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.JobCategoryMasterDAO;
import tm.dao.master.JobWisePanelStatusDAO;
import tm.dao.master.SchoolMasterDAO;
import tm.dao.master.SecondaryStatusDAO;
import tm.dao.user.RoleMasterDAO;
import tm.dao.user.UserMasterDAO;
import tm.services.es.ElasticSearchService;
import tm.services.teacher.AcceptOrDeclineMailHtml;
import tm.utility.ElasticSearchConfig;
import tm.utility.IPAddressUtility;
import tm.utility.Utility;


public class ApproveJobAjax 
{
	
	String locale = Utility.getValueOfPropByKey("locale");
	
	
	@Autowired
	private DistrictApprovalGroupsDAO districtApprovalGroupsDAO;
	public void setDistrictApprovalGroupsDAO(DistrictApprovalGroupsDAO districtApprovalGroupsDAO) {
		this.districtApprovalGroupsDAO = districtApprovalGroupsDAO;
	}
	
	@Autowired
	private EmailerService emailerService;
	public void setEmailerService(EmailerService emailerService) 
	{
		this.emailerService = emailerService;
	}
	
	@Autowired
	private JobRequisitionNumbersDAO jobRequisitionNumbersDAO;	
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	public void setDistrictMasterDAO(DistrictMasterDAO districtMasterDAO) {
		this.districtMasterDAO = districtMasterDAO;
	}
	@Autowired
	private JobOrderDAO jobOrderDAO;
	public void setJobOrderDAO(JobOrderDAO jobOrderDAO) {
		this.jobOrderDAO = jobOrderDAO;
	}
	@Autowired
	private SchoolInJobOrderDAO schoolInJobOrderDAO;
	public void setSchoolInJobOrderDAO(SchoolInJobOrderDAO schoolInJobOrderDAO) {
		this.schoolInJobOrderDAO = schoolInJobOrderDAO;
	}
	@Autowired
	private JobCertificationDAO jobCertificationDAO;
	public void setJobCertificationDAO(JobCertificationDAO jobCertificationDAO) {
		this.jobCertificationDAO = jobCertificationDAO;
	}	
	@Autowired
	private TeacherAssessmentStatusDAO teacherAssessmentStatusDAO;
	public void setTeacherAssessmentStatusDAO(TeacherAssessmentStatusDAO teacherAssessmentStatusDAO) {
		this.teacherAssessmentStatusDAO = teacherAssessmentStatusDAO;
	}	
	@Autowired
	private JobCategoryMasterDAO jobCategoryMasterDAO;
	public void setJobCategoryMasterDAO(JobCategoryMasterDAO jobCategoryMasterDAO) {
		this.jobCategoryMasterDAO = jobCategoryMasterDAO;
	}	
	@Autowired
	private SecondaryStatusDAO secondaryStatusDAO;
	public void setSecondaryStatusDAO(SecondaryStatusDAO secondaryStatusDAO) {
		this.secondaryStatusDAO = secondaryStatusDAO;
	}	
	@Autowired
	private JobWisePanelStatusDAO jobWisePanelStatusDAO;
	public void setJobWisePanelStatusDAO(
			JobWisePanelStatusDAO jobWisePanelStatusDAO) {
		this.jobWisePanelStatusDAO = jobWisePanelStatusDAO;
	}
	@Autowired
	private DistrictKeyContactDAO districtKeyContactDAO;
	public void setDistrictKeyContactDAO(DistrictKeyContactDAO districtKeyContactDAO)
	{
		this.districtKeyContactDAO=districtKeyContactDAO;
	}
	@Autowired
	private JobApprovalHistoryDAO jobApprovalHistoryDAO;
	
	@Autowired
	private DistrictSpecificApprovalFlowDAO districtSpecificApprovalFlowDAO;
	
	@Autowired
	private DistrictWiseApprovalGroupDAO districtWiseApprovalGroupDAO;
	
	@Autowired
	private UserMasterDAO userMasterDAO;
	
	@Autowired
	private EmploymentServicesTechnicianDAO employmentServicesTechnicianDAO;

	@Autowired
	private RoleMasterDAO roleMasterDAO;

	@Autowired
	private ManageJobOrdersAjax manageJobOrdersAjax;

	@Autowired
	private JobDenyApprovalHistoryDAO jobDenyApprovalHistoryDAO;

	@Autowired
	private SchoolMasterDAO schoolMasterDAO;

	@Autowired
	private JobApprovalNotesDAO jobApprovalNotesDAO;

	@Autowired
	private DistrictRequisitionNumbersDAO districtRequisitionNumbersDAO;
	@Autowired
	private JobWiseApprovalProcessDAO jobWiseApprovalProcessDAO;

	public void setJobWiseApprovalProcessDAO(
			JobWiseApprovalProcessDAO jobWiseApprovalProcessDAO) {
		this.jobWiseApprovalProcessDAO = jobWiseApprovalProcessDAO;
	}
	public String  displaySchool(int jobOrderId,int JobOrderType){
		/* ========  For Session time Out Error =========*/
		System.out.println("calling displaySchool");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			//throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		StringBuffer tmRecords =	new StringBuffer();
		try{
			/*
			UserMaster userMaster = new UserMaster();
			SchoolMaster schoolMaster=null;
			int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) {
				return "false";
			}else{
				userMaster=(UserMaster)session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			try{
				if(JobOrderType==2){
					roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,27,"addeditjoborder.do",1);
				}else{
					roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,28,"addeditjoborder.do",2);
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			 */
			JobOrder jobOrder =jobOrderDAO.findById(jobOrderId,false,false);
			List<SchoolInJobOrder> listSchoolInJobOrder	= schoolInJobOrderDAO.findJobOrder(jobOrder);

			Criterion criterion1 = Restrictions.eq("jobOrder", jobOrder);
			tmRecords.append("<input type='hidden' id='avlSchoolInJobOrder' value='"+listSchoolInJobOrder.size()+"'>");
			System.out.println("listSchoolInJobOrder            "+listSchoolInJobOrder.size());
			if(listSchoolInJobOrder.size()!=0){
				//if(jobOrder.getDistrictMaster().getIsReqNoRequired())
				tmRecords.append("<table id=\"schoolTable\" width=\"100%\" border=\"0\" class=\"table table-bordered span11 mt10\"><thead class=\"bg\"><tr><th>"+Utility.getLocaleValuePropByKey("lblSchoolName", locale)+"</th><th>"+Utility.getLocaleValuePropByKey("lblAddedOn", locale)+"</th><th>"+Utility.getLocaleValuePropByKey("lblOfExpHi", locale)+"</th><th>"+Utility.getLocaleValuePropByKey("lblRequisitionNumber", locale)+"</th></tr></thead><tbody>");
				//else
				//tmRecords.append("<table id=\"schoolTable\" width=\"100%\" border=\"0\" class=\"table table-bordered span11 mt10\"><thead class=\"bg\"><tr><th>School Name</th><th>Added on</th><th> # of Expected Hire(s)</th><th>"+Utility.getLocaleValuePropByKey("lblAct", locale)+"</th></tr></thead><tbody>");
			}
			int noOfRecordCheck = 0;


			/*================= Checking If Record Not Found ======================*/
			List<SchoolMaster> schoolMasters= new ArrayList<SchoolMaster>();
			for (SchoolInJobOrder schoolJobOrder : listSchoolInJobOrder){
				schoolMasters.add(schoolJobOrder.getSchoolId());
			}
			Map<Long,List<JobRequisitionNumbers>> jobReqMap= new HashMap<Long, List<JobRequisitionNumbers>>();
			List<JobRequisitionNumbers> jobRequisitionNumbers = jobRequisitionNumbersDAO.findJobRequisitionNumbersBySchoolInJobOrder(jobOrder,schoolMasters);
			//System.out.println("jobRequisitionNumbers::::::::::::::>>>"+jobRequisitionNumbers.size());
			try{
				for (JobRequisitionNumbers jobReqObj : jobRequisitionNumbers) {
					Long schoolId=jobReqObj.getSchoolMaster().getSchoolId();
					List<JobRequisitionNumbers> tAlist = jobReqMap.get(schoolId);
					if(tAlist==null){
						List<JobRequisitionNumbers> jobs = new ArrayList<JobRequisitionNumbers>();
						jobs.add(jobReqObj);
						jobReqMap.put(schoolId, jobs);
					}else{
						tAlist.add(jobReqObj);
						jobReqMap.put(schoolId, tAlist);
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			//System.out.println("jobReqMap::::::::::::::>>>"+jobReqMap.size());
			List<TeacherAssessmentStatus> teacherAssessmentStatusList=teacherAssessmentStatusDAO.findHireStatusByJobAndSchools(jobOrder,schoolMasters);
			Map<Long,List<TeacherAssessmentStatus>> assMap= new HashMap<Long, List<TeacherAssessmentStatus>>();
			try{
				for (TeacherAssessmentStatus assObj : teacherAssessmentStatusList) {
					Long schoolId=assObj.getUpdatedBy().getSchoolId().getSchoolId();
					List<TeacherAssessmentStatus> tAlist = assMap.get(schoolId);
					if(tAlist==null){
						List<TeacherAssessmentStatus> assList = new ArrayList<TeacherAssessmentStatus>();
						assList.add(assObj);
						assMap.put(schoolId, assList);
					}else{
						tAlist.add(assObj);
						assMap.put(schoolId, tAlist);
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			String location = "";
			for (SchoolInJobOrder schoolJobOrder : listSchoolInJobOrder){
				if(schoolJobOrder.getSchoolId().getLocationCode()!=null && schoolJobOrder.getSchoolId().getLocationCode()!="")
					location = " ("+schoolJobOrder.getSchoolId().getLocationCode()+")";

				noOfRecordCheck++;
				int countOfReqNo =0;
				try{
					if(jobReqMap.get(schoolJobOrder.getSchoolId().getSchoolId())!=null){
						countOfReqNo = jobReqMap.get(schoolJobOrder.getSchoolId().getSchoolId()).size();
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				tmRecords.append("<tr id='"+schoolJobOrder.getSchoolId().getSchoolId()+"'>" );
				tmRecords.append("<td nowrap >"+schoolJobOrder.getSchoolId().getSchoolName()+location+"</td>");
				tmRecords.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(schoolJobOrder.getCreatedDateTime())+"</td>");
				tmRecords.append("<td>"+schoolJobOrder.getNoOfSchoolExpHires()+"</td>");
				if(jobOrder.getDistrictMaster().getIsReqNoRequired())
					tmRecords.append("<td>"+countOfReqNo+"</td>");

			}
			if(listSchoolInJobOrder.size()!=0)
				tmRecords.append("</table>");
		}catch (Exception e) 
		{
			e.printStackTrace();
		}		
		return tmRecords.toString();
	}	

	public String  displayCertification(int jobOrderId,int JobOrderType){
		/* ========  For Session time Out Error =========*/	
		System.out.println("JobOrderType==="+JobOrderType+"             "+jobOrderId);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			//throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		StringBuffer tmRecords =	new StringBuffer();
		try{
			UserMaster userMaster = new UserMaster();			
			/*int roleId=0;
			if (session == null || session.getAttribute("userMaster") == null) {
				return "false";
			}else{
				userMaster=(UserMaster)session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
			String roleAccess=null;
			try{
				if(JobOrderType==2){
					roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,27,"addeditjoborder.do",1);
				}else{
					roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,28,"addeditjoborder.do",2);
				}
			}catch(Exception e){
				e.printStackTrace();
			}*/
			JobOrder jobOrder =jobOrderDAO.findById(jobOrderId,false,false);
			List<JobCertification> lstJobCertification= null;
			Criterion criterion1 = Restrictions.eq("jobId",jobOrder);
			lstJobCertification = jobCertificationDAO.findByCriteria(criterion1);		
			int i=1;
			tmRecords.append("<div style='margin-top:20px;'></div>");
			tmRecords.append("<div></div>");
			for (JobCertification jobCertification : lstJobCertification) 
			{
				tmRecords.append("<div style='margin-top:-20px;' class=\"\" id=\"CertificationGrid\" nowrap>" +
						"<label name=\"certificateTypeText\">"+jobCertification.getCertificateTypeMaster().getCertType()+" ");

				tmRecords.append("</label></div></br>");

			}
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return tmRecords.toString();
	}
	public String getStatusList(int jobId,Integer districtId,Integer jobCategoryId){

		StringBuffer sb=new StringBuffer();
		try{
			Map<Integer,SecondaryStatus> mapSStatus = new HashMap<Integer, SecondaryStatus>();
			List<StatusMaster> statusMasterList	=new ArrayList<StatusMaster>();
			JobOrder jobOrder=null;

			List<StatusMaster> lstStatusMaster	=	null;
			List<SecondaryStatus> lstSecondaryStatus=null;
			DistrictMaster districtMaster=null;
			JobCategoryMaster jobCategoryMaster=null;
			if(jobCategoryId!=null){
				jobCategoryMaster=jobCategoryMasterDAO.findById(jobCategoryId, false, false); 
				districtMaster=districtMasterDAO.findById(districtId, false, false);
				lstSecondaryStatus=  secondaryStatusDAO.findSecondaryStatusByJobCategory(districtMaster,jobCategoryMaster);
			}

			if(jobId!=0){
				jobOrder=jobOrderDAO.findById(jobId, false, false);
				if(jobCategoryId!=null){
					int x=1;
					sb.append("<div class='left20'>");
					sb.append("<input type='hidden' id='noofstatusCheckbox' value='"+statusMasterList.size()+"'>");

					x=1;
					sb.append("<input type='hidden' id='noofsecStatusCheckbox' value='"+lstSecondaryStatus.size()+"'>");
					List<JobWisePanelStatus>lstSecStatus=jobWisePanelStatusDAO.checkSecStatusJobOrderWise(jobOrder);
					Map<Integer, Boolean>secStsMap=new HashMap<Integer, Boolean>();
					for(JobWisePanelStatus jps:lstSecStatus){
						secStsMap.put(jps.getSecondaryStatus().getSecondaryStatusId(),jps.getPanelStatus());
					}
					for(SecondaryStatus secsts:lstSecondaryStatus){
						if(checkSecondaryStatus(secsts.getSecondaryStatusId())){

							boolean rs=false;
							if(secStsMap.size()>0){
								if(secStsMap.get(secsts.getSecondaryStatusId())!=null){
									rs=secStsMap.get(secsts.getSecondaryStatusId());
								}	
							}
							if(rs){
								sb.append("<div class='col-sm-3 col-md-3 top5'>");
								sb.append("<label class='checkbox inline' style='padding-left: 0px;'><input type='checkbox' onclick='return false' checked=checked name='secstatus' id='secstatus_"+x+"' value='"+secsts.getSecondaryStatusId()+"'/>"+secsts.getSecondaryStatusName()+"</label><br>");
								sb.append("</div>");
								++x;
							}						
						}
					}
					sb.append("</div>");
				}
			}

		}catch(Exception exception){
			exception.printStackTrace();
		}
		return sb.toString();
	}
	public boolean checkSecondaryStatus(Integer secondaryStatusId)
	{
		boolean isQuestionAdd=true;		
		try 
		{
			SecondaryStatus secondaryStatus= secondaryStatusDAO.findById(secondaryStatusId, false, false);
			if(secondaryStatus!=null)
			{
				if(secondaryStatus.getStatusMaster()!=null)
					if(secondaryStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("apl"))
						isQuestionAdd=false;

				if(secondaryStatus.getStatusNodeMaster()!=null)
					isQuestionAdd=false;
			}

		}catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			return isQuestionAdd;
		}
	}
	@Transactional(readOnly=false)
	public String approveJobOk(int entityType,Integer jobId,Integer userId)
	{
		/* ========  For Session time Out Error =========*/
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster=null;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}else{
			userMaster=(UserMaster)session.getAttribute("userMaster");
		}
		try{
			String ipAddress = IPAddressUtility.getIpAddress(request);
			JobOrder  jobOrder = jobOrderDAO.findById(jobId, false, false);
			JobCategoryMaster jobCategoryMaster= jobOrder.getJobCategoryMaster();

			DistrictMaster districtMaster = jobOrder.getDistrictMaster();
			Integer noOfApprovalNeeded = districtMaster.getNoOfApprovalNeeded()==null?0:districtMaster.getNoOfApprovalNeeded();
			Integer approvalBeforeGoLive = jobOrder.getApprovalBeforeGoLive()==null?0:jobOrder.getApprovalBeforeGoLive();
			if(jobCategoryMaster.getApprovalByPredefinedGroups()!=null && jobCategoryMaster.getApprovalByPredefinedGroups()/* && userMaster.getEntityType().equals(3)*/)
			{
				System.out.println("He is in the approveJobOkapproveJobOkapproveJobOkapproveJobOkapproveJobOkapproveJobOk");
				List<DistrictApprovalGroups> districtApprovalGroupList=null;
				DistrictSpecificApprovalFlow districtSpecificApprovalFlow=null;
				Map<String, String> groupShortNameWithGroupName=null;
				
				String jeffcoSpecialEdFlag="N";
				if(districtMaster.getDistrictId()!=804800)
					jeffcoSpecialEdFlag= ( jobOrder.getSpecialEdFlag()==null || jobOrder.getSpecialEdFlag().equals("") )? "N" : jobOrder.getSpecialEdFlag();
				System.out.println("jeffcoSpecialEdFlag:- "+jeffcoSpecialEdFlag);

				districtSpecificApprovalFlow = districtSpecificApprovalFlowDAO.findApprovalFlowByFlag(jeffcoSpecialEdFlag,jobOrder.getDistrictMaster());
				System.out.println("districtSpecificApprovalFlow:- "+districtSpecificApprovalFlow.getApprovalFlow());

				groupShortNameWithGroupName = districtWiseApprovalGroupDAO.mapGroupShortNameWithGroupName(districtSpecificApprovalFlow.getApprovalFlow(),jobOrder.getDistrictMaster());
				System.out.println("groupShortNameWithGroupName:- "+groupShortNameWithGroupName.size());

				districtApprovalGroupList = districtSpecificApprovalFlowDAO.findDistrictApprovalGroupByApprovalFlow(districtSpecificApprovalFlow.getApprovalFlow(), jobOrder.getDistrictMaster());
				System.out.println("districtApprovalGroups:- "+districtApprovalGroupList.size());

				if( districtApprovalGroupList!=null && districtApprovalGroupList.size()>0 )
				{
					try
					{
						List<JobApprovalHistory> jobApprovalHistories = jobApprovalHistoryDAO.findByCriteria(Restrictions.and(Restrictions.in("districtApprovalGroups", districtApprovalGroupList), Restrictions.eq("jobOrder", jobOrder)));
						DistrictApprovalGroups currentApprovalGroup=null;
						DistrictApprovalGroups nextApprovalGroup=null;
						List<UserMaster> userMasterList = new ArrayList<UserMaster>();
						boolean isSendMailToNextGroup=false;

						for(int i=0;i<districtApprovalGroupList.size();i++)
						{
							DistrictApprovalGroups approvalGroups= districtApprovalGroupList.get(i);
							boolean isHistoryContainsCurrentGroup=false;
							for(JobApprovalHistory history : jobApprovalHistories)
							{
								if(history.getDistrictApprovalGroups().getDistrictApprovalGroupsId() == approvalGroups.getDistrictApprovalGroupsId())
								{
									isHistoryContainsCurrentGroup=true;
									break;
								}
							}

							if(!isHistoryContainsCurrentGroup)
							{
								currentApprovalGroup = approvalGroups;
								if( (i+1)< districtApprovalGroupList.size())
								{
									nextApprovalGroup = districtApprovalGroupList.get(i+1);

									ArrayList<Integer> membersIds = new ArrayList<Integer>();
									for(String nm : nextApprovalGroup.getGroupMembers().split("#"))
										if( !nm.equals("") )
											try{membersIds.add(Integer.parseInt(nm));}catch(Exception e){}
									
									if(membersIds.size()>0)
									{
										List<DistrictKeyContact> districtKeyContactList = districtKeyContactDAO.findByCriteria(Restrictions.in("keyContactId", membersIds));
										if(districtKeyContactList!=null && districtKeyContactList.size()>0)
											for(DistrictKeyContact contact : districtKeyContactList)
												userMasterList.add(contact.getUserMaster());
									}
								}
								break;
							}
						}

						String groupFullName = (groupShortNameWithGroupName.get("SA")==null)? "" : groupShortNameWithGroupName.get("SA");
						if(nextApprovalGroup!=null && nextApprovalGroup.getGroupName().equals(groupFullName))
						{
							//Logic For All SA's
							List<SchoolInJobOrder> schoolInJobOrder = schoolInJobOrderDAO.findByCriteria(Restrictions.eq("jobId",jobOrder)); //Get the total Number of SA's

							List<SchoolMaster> schoolMasters = new ArrayList<SchoolMaster>();
							for(SchoolInJobOrder order : schoolInJobOrder)
								schoolMasters.add(order.getSchoolId());

							List<UserMaster> userMasters2 = null;
							if(schoolInJobOrder!=null && schoolInJobOrder.size()>0)
							{
								Criterion criterion1 = Restrictions.eq("entityType", new Integer(3));
								Criterion criterion2 = Restrictions.in("schoolId", schoolMasters);
								Criterion criterion3 = Restrictions.eq("districtId", districtMaster);
								Criterion criterion4 = Restrictions.eq("status","A");
								Criterion criterion5 = Restrictions.eq("roleId", roleMasterDAO.findById(3,false,false));

								userMasters2 = userMasterDAO.findByCriteria(criterion1,criterion2,criterion3,criterion4,criterion5);
								for(UserMaster master : userMasters2)
									userMasterList.add(master);
							}
						}

						groupFullName = groupShortNameWithGroupName.get("ES");
						if(nextApprovalGroup!=null && nextApprovalGroup.getGroupName().equals(groupFullName))
						{
							//Logic For ES Shadab 322
							EmploymentServicesTechnician primaryServicesTechnician = jobOrder.getEmploymentServicesTechnician();

							List<String> emailAddress = new ArrayList<String>();
							emailAddress.add(primaryServicesTechnician.getPrimaryEmailAddress());
							emailAddress.add(primaryServicesTechnician.getBackupEmailAddress());

							System.out.println("primaryTechnicianEmailAddress:- "+primaryServicesTechnician.getPrimaryEmailAddress());
							System.out.println("backupTechnicianEmailAddress:- "+primaryServicesTechnician.getBackupEmailAddress());

							Criterion criterion = Restrictions.in("keyContactEmailAddress", emailAddress);
							List<DistrictKeyContact> districtKeyContacts = districtKeyContactDAO.findByCriteria(criterion);

							for(DistrictKeyContact contact : districtKeyContacts)
								userMasterList.add(contact.getUserMaster());
						}

						if(currentApprovalGroup!=null)
						{
							//if(currentApprovalGroup.getGroupMembers().contains("#"+userKeyContactId+"#"))
							//{
							JobApprovalHistory jobApprovalHistory = new JobApprovalHistory();
							jobApprovalHistory.setCreatedDateTime(new Date());
							jobApprovalHistory.setUserMaster(userMaster);
							jobApprovalHistory.setJobOrder(jobOrder);
							jobApprovalHistory.setIpAddress(ipAddress);
							jobApprovalHistory.setDistrictApprovalGroups(currentApprovalGroup);
							jobApprovalHistoryDAO.makePersistent(jobApprovalHistory);
							isSendMailToNextGroup=true;
							//}
						}

						if(isSendMailToNextGroup)//isSendMail
						{
							if(nextApprovalGroup!=null)
							{
								for(UserMaster usermaster : userMasterList)
									sendJobApproveURLMailOnJobCreation(usermaster, jobOrder.getJobId(), jobOrder.getJobTitle(), request, nextApprovalGroup);
							}
							else
								if(nextApprovalGroup==null)
								{
									boolean isJobApproved=true;
									if(isJobApproved)	//if does not sent to next group then update jobOrder
									{
										try
										{
											//code here for joborder update
											Integer minDaysJobWillDisplay = jobCategoryMaster.getMinDaysJobWillDisplay();
											long differenceBetweenDate = 0l;
											Date jobApprovalDate = new Date();

											Date PostingStartDate = jobOrder.getJobStartDate();
											Date PostingEndDate = jobOrder.getJobEndDate();
											long diffBetweenDate = PostingEndDate.getTime() - PostingStartDate.getTime();
											diffBetweenDate = (int) (diffBetweenDate / (24 * 60 * 60 * 1000));
											System.out.println("difference between days: " + diffBetweenDate);
											if(jobOrder!=null && jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId()==5304860){
												jobOrder.setJobStartDate(jobOrder.getJobStartDate());
												jobOrder.setJobEndDate(jobOrder.getJobEndDate());
											}else{
												if(jobApprovalDate.after(PostingStartDate))
												{
													PostingStartDate = jobApprovalDate;
													differenceBetweenDate = PostingEndDate.getTime() - PostingStartDate.getTime();
													differenceBetweenDate = (int) (differenceBetweenDate / (24 * 60 * 60 * 1000));
													if(differenceBetweenDate < minDaysJobWillDisplay)
													{
														PostingStartDate = jobApprovalDate;
														PostingEndDate = DateUtils.addDays(PostingStartDate, minDaysJobWillDisplay);
													}
													System.out.println("difference between days: " + differenceBetweenDate);
													jobOrder.setJobStartDate(PostingStartDate);
													jobOrder.setJobEndDate(PostingEndDate);
	
												}else if(diffBetweenDate < minDaysJobWillDisplay){
													PostingEndDate = DateUtils.addDays(PostingStartDate, minDaysJobWillDisplay);
													jobOrder.setJobEndDate(PostingEndDate);
												}
											}
										}
										catch(Exception e){}
										jobOrder.setApprovalBeforeGoLive(1);
										jobOrderDAO.updatePersistent(jobOrder);	
										setJobInJobBoard(jobOrder);
									}
								}
						}
					}
					catch(Exception e)
					{
						e.printStackTrace();
					}
				}
				else	//If district has not any group
				{
					//code here for joborder update
					jobOrder.setApprovalBeforeGoLive(1);
					jobOrderDAO.updatePersistent(jobOrder);
					setJobInJobBoard(jobOrder);
				}
			}
			else
				if(approvalBeforeGoLive!=1 && jobCategoryMaster.getApprovalBeforeGoLive()!=null && jobCategoryMaster.getBuildApprovalGroup()!=null && jobCategoryMaster.getApprovalBeforeGoLive()  && jobCategoryMaster.getBuildApprovalGroup() )	//JobCategory Specific ApprovalFlow
				{
					//Start of getting the key contact Id of user.
					List<DistrictKeyContact> districtKeyContactList322 = new ArrayList<DistrictKeyContact>();
					districtKeyContactList322 = districtKeyContactDAO.findByContactType(districtMaster,Utility.getLocaleValuePropByKey("msgJobApproval", locale));
					UserMaster user = null;
					Integer userKeyContactId=null;
					System.out.println("userId::: "+userId+", districtKeyContactList322:- "+districtKeyContactList322);
					for (DistrictKeyContact districtKeyContact : districtKeyContactList322) {
						System.out.println("districtKeyContact.getUserMaster().getUserId():: "+districtKeyContact.getUserMaster().getUserId());
						if(districtKeyContact.getUserMaster().getUserId().equals(userId))
						{
							user = districtKeyContact.getUserMaster();
							userKeyContactId = districtKeyContact.getKeyContactId();
							break;
						}
					}

					DistrictKeyContact districtKeyContact123 = districtKeyContactDAO.findByUserId(userMaster);
					if(districtKeyContact123!=null)
					{
						user = districtKeyContact123.getUserMaster();
						userKeyContactId = districtKeyContact123.getKeyContactId();
					}

					System.out.println("Now districtKeyContact123:- "+districtKeyContact123+", user:- "+user+", userKeyContactId:- "+userKeyContactId);
					//End of getting the key contact Id of User.

					List<DistrictApprovalGroups> districtApprovalGroupList = districtApprovalGroupsDAO.findDistrictApprovalGroupsByJobCategory(districtMaster, jobCategoryMaster);
					System.out.println("districtApprovalGroupList:- "+districtApprovalGroupList);
					System.out.println("user:- "+user);
					if( districtApprovalGroupList!=null && districtApprovalGroupList.size()>0 && user!=null )
					{
						try
						{
							List<JobApprovalHistory> jobApprovalHistories = jobApprovalHistoryDAO.findByCriteria(Restrictions.and(Restrictions.in("districtApprovalGroups", districtApprovalGroupList), Restrictions.eq("jobOrder", jobOrder)));
							DistrictApprovalGroups currentApprovalGroup=null;
							List<DistrictKeyContact> currentApprovalGroupKeyContactList = null;
							DistrictApprovalGroups nextApprovalGroup=null;
							List<DistrictKeyContact> nextApprovalGroupKeyContactList = null;
							boolean isSendMailToNextGroup=false;
							int totalNoOfApprovedGroup=0;

							for(int i=0;i<districtApprovalGroupList.size();i++)
							{
								DistrictApprovalGroups approvalGroups= districtApprovalGroupList.get(i);
								boolean isHistoryContainsCurrentGroup=false;
								for(JobApprovalHistory history : jobApprovalHistories)
								{
									if(history.getDistrictApprovalGroups().getDistrictApprovalGroupsId() == approvalGroups.getDistrictApprovalGroupsId())
									{
										isHistoryContainsCurrentGroup=true;
										break;
									}
								}

								if(!isHistoryContainsCurrentGroup)
								{
									List<DistrictKeyContact> districtKeyContactList = new ArrayList<DistrictKeyContact>();
									ArrayList<Integer> membersIds = new ArrayList<Integer>();
									for(String nm : approvalGroups.getGroupMembers().split("#"))
										if( !nm.equals("") )
											try{membersIds.add(Integer.parseInt(nm));}catch(Exception e){}

											if(membersIds.size()>0)
											{
												districtKeyContactList = districtKeyContactDAO.findByCriteria(Restrictions.in("keyContactId", membersIds));
												if(currentApprovalGroup==null)
												{
													currentApprovalGroup = approvalGroups;
													currentApprovalGroupKeyContactList = districtKeyContactList;
													totalNoOfApprovedGroup=i+1;
												}
												else
													if(currentApprovalGroup!=null && nextApprovalGroup==null)
													{
														nextApprovalGroup = approvalGroups;
														nextApprovalGroupKeyContactList = districtKeyContactList;
														break;
													}
											}
								}
							}

							//if(currentApprovalGroup!=null && currentApprovalGroupKeyContactList!=null)
							if(currentApprovalGroup!=null)
							{
								if(currentApprovalGroup.getGroupMembers().contains("#"+userKeyContactId+"#"))
								{
									JobApprovalHistory jobApprovalHistory = new JobApprovalHistory();
									jobApprovalHistory.setCreatedDateTime(new Date());
									jobApprovalHistory.setUserMaster(user);
									jobApprovalHistory.setJobOrder(jobOrder);
									jobApprovalHistory.setIpAddress(ipAddress);
									jobApprovalHistory.setDistrictApprovalGroups(currentApprovalGroup);
									jobApprovalHistoryDAO.makePersistent(jobApprovalHistory);
									isSendMailToNextGroup=true;

									System.out.println("::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
									Integer nOOfApprovalNeeded = currentApprovalGroup.getJobCategoryId().getNoOfApprovalNeeded();
									if(nOOfApprovalNeeded!=null)
									{
										if(nOOfApprovalNeeded<=totalNoOfApprovedGroup)
										{
											isSendMailToNextGroup=false;
											jobOrder.setApprovalBeforeGoLive(1);
											jobOrderDAO.updatePersistent(jobOrder);
											setJobInJobBoard(jobOrder);
											System.out.println("inside if condition isSendMailToNextGroup:- "+isSendMailToNextGroup);
										}
									}
									System.out.println("nOOfApprovalNeeded:- "+nOOfApprovalNeeded);
									System.out.println("totalNoOfApprovedGroup:- "+totalNoOfApprovedGroup);
									System.out.println("::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
								}
							}

							if(isSendMailToNextGroup)//isSendMail
							{
								if(nextApprovalGroup!=null && nextApprovalGroupKeyContactList!=null && nextApprovalGroupKeyContactList.size()>0)
								{
									for(DistrictKeyContact districtKeyContact : nextApprovalGroupKeyContactList)
										sendJobApproveURLMailOnJobCreation(districtKeyContact.getUserMaster(), jobOrder.getJobId(), jobOrder.getJobTitle(), request, nextApprovalGroup);
								}
								else
									if(nextApprovalGroup==null)
									{
										boolean isJobApproved=true;
										if(isJobApproved)	//if does not sent to next group then update jobOrder
										{
											//code here for joborder update
											Integer minDaysJobWillDisplay = jobCategoryMaster.getMinDaysJobWillDisplay();
											long differenceBetweenDate = 0l;
											Date jobApprovalDate = new Date();

											Date PostingStartDate = jobOrder.getJobStartDate();
											Date PostingEndDate = jobOrder.getJobEndDate();
											long diffBetweenDate = PostingEndDate.getTime() - PostingStartDate.getTime();
											diffBetweenDate = (int) (diffBetweenDate / (24 * 60 * 60 * 1000));
											System.out.println("difference between days: " + diffBetweenDate);
											if(jobOrder!=null && jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId()==5304860){
												jobOrder.setJobStartDate(jobOrder.getJobStartDate());
												jobOrder.setJobEndDate(jobOrder.getJobEndDate());
											}else{
												if(jobApprovalDate.after(PostingStartDate))
												{
													PostingStartDate = jobApprovalDate;
													differenceBetweenDate = PostingEndDate.getTime() - PostingStartDate.getTime();
													differenceBetweenDate = (int) (differenceBetweenDate / (24 * 60 * 60 * 1000));
													if(differenceBetweenDate < minDaysJobWillDisplay)
													{
														PostingStartDate = jobApprovalDate;
														PostingEndDate = DateUtils.addDays(PostingStartDate, minDaysJobWillDisplay);
													}
													System.out.println("difference between days: " + differenceBetweenDate);
													jobOrder.setJobStartDate(PostingStartDate);
													jobOrder.setJobEndDate(PostingEndDate);
	
												}else if(diffBetweenDate < minDaysJobWillDisplay){
													PostingEndDate = DateUtils.addDays(PostingStartDate, minDaysJobWillDisplay);
													jobOrder.setJobEndDate(PostingEndDate);
												}
											}
											jobOrder.setApprovalBeforeGoLive(1);
											jobOrderDAO.updatePersistent(jobOrder);		
											setJobInJobBoard(jobOrder);
										}
									}
							}
						}
						catch(Exception e)
						{
							e.printStackTrace();
						}
					}
					else	//If district has not any group
					{
						//code here for joborder update
						jobOrder.setApprovalBeforeGoLive(1);
						jobOrderDAO.updatePersistent(jobOrder);
						setJobInJobBoard(jobOrder);
					}
				}
				else
					if(approvalBeforeGoLive!=1)
					{

						List<DistrictKeyContact> districtKeyContactList = new ArrayList<DistrictKeyContact>();
						districtKeyContactList = districtKeyContactDAO.findByContactType(districtMaster,"Job Approval");
						UserMaster user = null;
						Integer userKeyContactId=null;
						System.out.println("userId::: "+userId);
						//jobapprovalhistory
						for (DistrictKeyContact districtKeyContact : districtKeyContactList) {
							System.out.println("districtKeyContact.getUserMaster().getUserId():: "+districtKeyContact.getUserMaster().getUserId());
							if(districtKeyContact.getUserMaster().getUserId().equals(userId))
							{
								user = districtKeyContact.getUserMaster();
								userKeyContactId = districtKeyContact.getKeyContactId();
							}
						}
						System.out.println("User: "+user);
						if(user==null)
						{
							return "1";//You are not authorized
						}
						boolean approve=false;
						List<JobApprovalHistory> jobApprovalHistories = jobApprovalHistoryDAO.findByJobId(jobOrder);
						if(jobApprovalHistories.size()>0)
						{
							if(noOfApprovalNeeded==jobApprovalHistories.size())
							{
								//jobOrder.setApprovalBeforeGoLive(1);
								//jobOrderDAO.updatePersistent(jobOrder);
								//setJobInJobBoard(jobOrder);
								return "2";// already approved
							}
							boolean isApproved = false;
							for (JobApprovalHistory jobApprovalHistory : jobApprovalHistories) {
								if(jobApprovalHistory.getUserMaster().getUserId().equals(userId))
								{
									isApproved = true;
									break;
								}
							}
							if(!isApproved)
								approve=true;//Approve job


						}else
						{
							approve=true;//Approve job
						}

						if(approve)
						{
							JobApprovalHistory jobApprovalHistory = new JobApprovalHistory();
							jobApprovalHistory.setCreatedDateTime(new Date());
							UserMaster um = new UserMaster();
							um.setUserId(userId);
							jobApprovalHistory.setUserMaster(um);
							jobApprovalHistory.setJobOrder(jobOrder);
							jobApprovalHistory.setIpAddress(ipAddress);
							try{
								jobApprovalHistoryDAO.makePersistent(jobApprovalHistory);
							}catch (Exception e) {
								System.out.println(e);
							}
							System.out.println("noOfApprovalNeeded : "+noOfApprovalNeeded+" "+jobApprovalHistories.size());
							//if(noOfApprovalNeeded>=jobApprovalHistories.size()+1)
							if(noOfApprovalNeeded<=jobApprovalHistories.size()+1)
							{
								Integer minDaysJobWillDisplay = jobCategoryMaster.getMinDaysJobWillDisplay();
								long differenceBetweenDate = 0l;
								Date jobApprovalDate = new Date();

								Date PostingStartDate = jobOrder.getJobStartDate();
								Date PostingEndDate = jobOrder.getJobEndDate();
								long diffBetweenDate = PostingEndDate.getTime() - PostingStartDate.getTime();
								diffBetweenDate = (int) (diffBetweenDate / (24 * 60 * 60 * 1000));
								System.out.println("difference between days: " + diffBetweenDate);
								
								if(jobOrder!=null && jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId()==5304860){
									jobOrder.setJobStartDate(jobOrder.getJobStartDate());
									jobOrder.setJobEndDate(jobOrder.getJobEndDate());
								}else{
									if(jobApprovalDate.after(PostingStartDate))
									{
										PostingStartDate = jobApprovalDate;
										differenceBetweenDate = PostingEndDate.getTime() - PostingStartDate.getTime();
										differenceBetweenDate = (int) (differenceBetweenDate / (24 * 60 * 60 * 1000));
										if(differenceBetweenDate < minDaysJobWillDisplay)
										{
											PostingStartDate = jobApprovalDate;
											PostingEndDate = DateUtils.addDays(PostingStartDate, minDaysJobWillDisplay);
										}
										System.out.println("difference between days: " + differenceBetweenDate);
										jobOrder.setJobStartDate(PostingStartDate);
										jobOrder.setJobEndDate(PostingEndDate);
	
									}else if(diffBetweenDate < minDaysJobWillDisplay){
										PostingEndDate = DateUtils.addDays(PostingStartDate, minDaysJobWillDisplay);
										jobOrder.setJobEndDate(PostingEndDate);
									}
								}

								jobOrder.setApprovalBeforeGoLive(1);
								jobOrderDAO.updatePersistent(jobOrder);
								setJobInJobBoard(jobOrder);

							}else
							{
								jobOrder.setApprovalBeforeGoLive(2);
								jobOrderDAO.updatePersistent(jobOrder);
							}
						}
						try{
							String logType="Job Approved";
							//userLoginHistoryDAO.insertUserLoginHistory(userMaster,session.getId(),IPAddressUtility.getIpAddress(request),new Date(),logType);
						}catch(Exception e){
							e.printStackTrace();
						}
					}	
		}catch (Exception e) 
		{
			e.printStackTrace();
			return "0";
		}
		return "0";
	}

	private void setJobInJobBoard(JobOrder jobOrder){
		System.out.println("Ok I m checking in job board.............");
		try{
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
			if(jobOrder.getStatus().equalsIgnoreCase("A") && (jobOrder.getIsInviteOnly()==null ||  !jobOrder.getIsInviteOnly()) && jobOrder.getApprovalBeforeGoLive()!=null && jobOrder.getApprovalBeforeGoLive()==1 
					&& (jobOrder.getJobStartDate().before(new Date()) ||  jobOrder.getJobStartDate().equals(new Date())) 
					&& (jobOrder.getJobEndDate().after(sdf.parse(sdf.format(new Date()))) ||  jobOrder.getJobEndDate().equals(sdf.parse(sdf.format(new Date())))) 
					&& (jobOrder.getHiddenJob()==null ||  !jobOrder.getHiddenJob())	)
			{
				new ElasticSearchService().updateESDistrictJobBoardByJobId(jobOrder);
			}
		}catch(Exception e){e.printStackTrace();}
	}

	public void sendJobApproveURLMailOnJobCreation(UserMaster usermaster, Integer returnJobId, String returnjobTitle, HttpServletRequest request,DistrictApprovalGroups approvalGroups)
	{
		if(usermaster!=null)
			try
		{
				DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
				dsmt.setEmailerService(emailerService);
				dsmt.setMailfrom("admin@netsutra.com");
				dsmt.setMailto(usermaster.getEmailAddress());
				dsmt.setMailsubject("Approval Mail");
				System.out.println("returnJobId   "+returnJobId);
				System.out.println("districtKeyContact.getUserMaster().getUserId()   "+usermaster.getUserId());
				String incJobId = Utility.encryptNo(returnJobId);
				String incUserId = Utility.encryptNo(usermaster.getUserId());
				String groupId =  Utility.encryptNo(approvalGroups.getDistrictApprovalGroupsId());

				String linkIds= incJobId+"###"+incUserId+"###"+groupId;
				String forMated = "";
				try
				{
					String baseURL=Utility.getBaseURL(request);
					forMated = Utility.encodeInBase64(linkIds);
					forMated = baseURL+"approveJob.do?id="+forMated;

					System.out.println("forMated   "+forMated);
					System.out.println("returnjobTitle   "+returnjobTitle);
					String content=MailText.getApproveJObMailTextJeffcoSpecific(request,usermaster, forMated,returnjobTitle);
					dsmt.setMailcontent(content);
					dsmt.start();
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Transactional(readOnly=false)
	public int denyJobApprovalJobOrderWithComment(Integer jobId,String comments){
		System.out.println("jobId==========="+comments+"================="+jobId);
		try{
			WebContext context;
			context = WebContextFactory.get();
			HttpServletRequest request = context.getHttpServletRequest();
			HttpSession session = request.getSession(false);
			UserMaster userMaster=null;
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}else{
				userMaster=(UserMaster)session.getAttribute("userMaster");
			}


			JobOrder jobOrder = jobOrderDAO.findById(jobId, false, false);

			List<JobApprovalNotes> JAHList=jobApprovalNotesDAO.findByCriteria(Restrictions.eq("jobOrder", jobOrder),Restrictions.eq("userMaster", userMaster));
			if(JAHList!=null && JAHList.size()>0)
				return 0;

			JobApprovalNotes jobApprovalNotes=new JobApprovalNotes();
			jobApprovalNotes.setCreatedDateTime(new Date());
			jobApprovalNotes.setIpAddress(request.getRemoteAddr());
			jobApprovalNotes.setNote(comments);
			jobApprovalNotes.setType(1);
			jobApprovalNotes.setJobOrder(jobOrder);
			jobApprovalNotes.setUserMaster(userMaster);
			jobApprovalNotesDAO.makePersistent(jobApprovalNotes);

			List<JobApprovalHistory> listJobApprovalHistory=jobApprovalHistoryDAO.findByJobId(jobOrder);
			for(JobApprovalHistory jah:listJobApprovalHistory){
				JobDenyApprovalHistory copyDJAH=new JobDenyApprovalHistory();
				BeanUtils.copyProperties(copyDJAH, jah);
				copyDJAH.setDenyIpAddress(request.getRemoteAddr());
				copyDJAH.setCreatedDateTimeDeny(new Date());
				copyDJAH.setComments(comments);
				copyDJAH.setUserMasterDeny(userMaster);
				jobDenyApprovalHistoryDAO.makePersistent(copyDJAH);
				jobApprovalHistoryDAO.makeTransient(jah);
			}	
			/*List<JobRequisitionNumbers> listJRN=jobRequisitionNumbersDAO.findRequisitionsByJob(jobOrder);
		if(listJRN!=null && !listJRN.isEmpty()){
			for(JobRequisitionNumbers objJRN:listJRN){
			DistrictRequisitionNumbers objDRN=objJRN.getDistrictRequisitionNumbers();
			objDRN.setStatus("I");
			districtRequisitionNumbersDAO.makePersistent(objDRN);
			}
		}*/
			jobOrder.setStatus("I");
			jobOrderDAO.makePersistent(jobOrder);
			ElasticSearchService es=new ElasticSearchService();
			es.updateESSchoolJobOrderByJobId(jobOrder);
			if(jobOrder.getStatus().trim().equalsIgnoreCase("I")){
				es.searchDataAndDelete(jobOrder.getJobId(),ElasticSearchConfig.indexForhqDistrictJobBoard);
			}else{
				SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
				if(jobOrder.getStatus().equalsIgnoreCase("A") && (jobOrder.getIsInviteOnly()==null ||  !jobOrder.getIsInviteOnly()) && jobOrder.getApprovalBeforeGoLive()!=null && jobOrder.getApprovalBeforeGoLive()==1 
						&& (jobOrder.getJobStartDate().before(new Date()) ||  jobOrder.getJobStartDate().equals(new Date())) 
						&& (jobOrder.getJobEndDate().after(sdf.parse(sdf.format(new Date()))) ||  jobOrder.getJobEndDate().equals(sdf.parse(sdf.format(new Date())))) 
						&& (jobOrder.getHiddenJob()==null ||  !jobOrder.getHiddenJob())	)
				{
					es.updateESDistrictJobBoardByJobId(jobOrder);
				}
			}

			UserMaster createdBy=userMasterDAO.findById(jobOrder.getCreatedBy(), false, false);
			manageJobOrdersAjax.sendApprovalMail(jobOrder,jobOrder.getDistrictMaster(),jobOrder.getJobId(),jobOrder.getJobTitle(),jobOrder.getSpecialEdFlag(),jobOrderDAO,request);
			String mailDenyTemplate=AcceptOrDeclineMailHtml.sendDenyNotificationTOJobCreaterMail(userMaster,jobOrder,comments);
			System.out.println("mailDenyTemplate==========="+mailDenyTemplate);
			new Thread(new DemoScheduleMailThread(emailerService,null,createdBy.getEmailAddress(),"ram.thakur@mailinator.com","Job Order Not Approved",mailDenyTemplate)).start();
			System.out.println("Ok====="+mailDenyTemplate);
		}catch(Exception t){t.printStackTrace();}
		return 1;
	}

	public int isApproveOrNot(Integer jobId){
		System.out.println("jobId================"+jobId);

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request=null;
		HttpSession session=null;
		if (context!=null) {
		request = context.getHttpServletRequest();
		session = request.getSession(false);
		}
		UserMaster userMaster=null;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}else{
			userMaster=(UserMaster)session.getAttribute("userMaster");
		}
		boolean SPED=false;
		Map<String,String> groupNameMap=new LinkedHashMap<String,String>();
		JobOrder jobOrder = jobOrderDAO.findById(jobId, false, false);
		if(jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId().toString().equals("804800")){
			groupNameMap.put("SA", null);
			groupNameMap.put("ES", "SA");
			System.out.println("jobOrder.getSpecialEdFlag()================="+jobOrder.getSpecialEdFlag());
			if(jobOrder.getSpecialEdFlag().trim().equals("Y")){
				groupNameMap.put("SA", "SPED");
				groupNameMap.put("ES", "SPED,SA");
				SPED=true;
			}
			System.out.println("groupNameMap====="+groupNameMap);
			if(jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId().toString().equals("804800")){
				Map<String,List<UserMaster>>  groupMap=getApprovalInfoDetails(jobOrder.getSchool().get(0).getSchoolId().toString(),jobOrder.getEmploymentServicesTechnician().getEmploymentservicestechnicianId().toString(), jobOrder.getSpecialEdFlag(), jobOrder.getJobId(), jobOrder.getDistrictMaster().getDistrictId());
				for(String s:groupMap.keySet()){
					System.out.println("group========"+s);
					if(groupMap.get(s).contains(userMaster)){
						return isUpperLevelGroupApproveOrNot(groupNameMap.get(s),groupMap,SPED);
					}
				}
			}else{
				return 1;
			}

			return 0;
		} else if(jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId().toString().equals("806900")){
			List<JobWiseApprovalProcess>	jobWiseApprovalProcessList=	jobWiseApprovalProcessDAO.findByJobId(jobOrder);
			if(jobWiseApprovalProcessList!=null && jobWiseApprovalProcessList.size()>0){
				JobWiseApprovalProcess jobWiseApprovalProcess=jobWiseApprovalProcessList.get(0);
				JobApprovalProcess jobApprovalProcess=jobWiseApprovalProcess.getJobApprovalProcessId();
				String groups=jobApprovalProcess.getJobApprovalGroups();
				if(jobApprovalProcess!=null && jobApprovalProcess.getStatus().equals(0)){
					return 2;
				}
				String groupIds[]=null;
				DistrictApprovalGroups userapprovalGroups=null;
				groupIds=groups.split("\\|");
				List<Integer> groupApprovalIds=new ArrayList<Integer>();
				if(groupIds!=null)
				{
					for(String goupid:groupIds){
						
						if(!goupid.equals("")){
							try{
								Integer grpid = Integer.parseInt(goupid);
								groupApprovalIds.add(grpid);
							}catch(Exception e){e.printStackTrace();}
						}
					}
				}
				List<DistrictApprovalGroups> districtApprovalGroupsList=	districtApprovalGroupsDAO.findGroupsByDistrictAndDistrictApprovalGroupsId(jobOrder.getDistrictMaster(),groupApprovalIds);
				if(districtApprovalGroupsList!=null && districtApprovalGroupsList.size()>0){
					List<Integer> groupMemberIds = new ArrayList<Integer>();
					for(DistrictApprovalGroups districtApprovalGroups:districtApprovalGroupsList){
						if(districtApprovalGroups.getGroupMembers()!=null && !districtApprovalGroups.getGroupMembers().trim().equals("")){
							for(String id:districtApprovalGroups.getGroupMembers().split("#")){
								if(!id.equals(""))
									groupMemberIds.add(Integer.parseInt(id));
							}
						}
					}
					if(groupMemberIds.size()>0){

						List<DistrictKeyContact> districtKeyContactList = districtKeyContactDAO.findByCriteria(Restrictions.in("keyContactId", groupMemberIds));
						DistrictKeyContact districtKeyContact=null;
						for(DistrictKeyContact dkc:districtKeyContactList){
							if(dkc.getUserMaster().getUserId().equals(userMaster.getUserId())){
								districtKeyContact=dkc;
								break;
							}
						}
						List<JobApprovalHistory>	jobApprovalHistoryList	=		jobApprovalHistoryDAO.findByCriteria(Restrictions.in("districtApprovalGroups", districtApprovalGroupsList),Restrictions.eq("jobOrder", jobOrder));
						boolean nextApprovalGroup = false;
						DistrictApprovalGroups districtApprovalGroup = null;
						for(DistrictApprovalGroups districtApprovalGroups:districtApprovalGroupsList){
							nextApprovalGroup = false;
							if(jobApprovalHistoryList!=null &&jobApprovalHistoryList.size()>0)
								for(JobApprovalHistory JobApprovalHistory:jobApprovalHistoryList){
									if(JobApprovalHistory.getDistrictApprovalGroups().getDistrictApprovalGroupsId()==districtApprovalGroups.getDistrictApprovalGroupsId()){
										nextApprovalGroup = true;
										break;
									}
								}
							if(nextApprovalGroup==false){
								districtApprovalGroup = districtApprovalGroups;
								break;
							}
							
						}
						boolean checkApproved=true;
						boolean isApproved=false;
						//List<JobApprovalHistory>	jobApprovalHistoryList	=		jobApprovalHistoryDAO.findByCriteria(Restrictions.in("districtApprovalGroups", districtApprovalGroupsList),Restrictions.eq("jobOrder", jobOrder));
						//boolean checkApproved=true;
						if(jobApprovalHistoryList!=null &&jobApprovalHistoryList.size()>0)
						for(JobApprovalHistory JobApprovalHistory:jobApprovalHistoryList){
							if(JobApprovalHistory.getDistrictApprovalGroups().getDistrictApprovalGroupsId()==districtApprovalGroup.getDistrictApprovalGroupsId()){
								checkApproved=false;
							}
						}
					
						if(checkApproved){
							isApproved = true;
						}
						if(districtApprovalGroup != null && districtApprovalGroup.getGroupMembers().contains("#"+districtKeyContact.getKeyContactId()+"#")){
							isApproved = true;
						}
						if(isApproved){
							return 1;
						} else{
							return 0;
						}

					}	
				}

			}
			return 0;
		}
		else{
			return 1;
		}
	}

	private int isUpperLevelGroupApproveOrNot(String groupName, Map<String,List<UserMaster>>  groupMap, boolean SPED){
		System.out.println("isUpperLevelGroupApproveOrNot");
		if(groupName!=null){
			String[] group=groupName.split(",");
			try{
				if(group.length==2){				
					List<UserMaster> umSAsList=groupMap.get("SA");
					List<UserMaster> umSPEDList=groupMap.get("SPED");
					System.out.println("umSPEDList======="+umSPEDList);
					System.out.println("umSAsList======="+umSAsList);
					List<JobApprovalHistory> JAH_SAList=jobApprovalHistoryDAO.findHistoryByUser(umSAsList);
					List<JobApprovalHistory> JAH_SPEDList=jobApprovalHistoryDAO.findHistoryByUser(umSPEDList);
					if(JAH_SAList!=null && JAH_SPEDList!=null && JAH_SAList.size()>0 && JAH_SPEDList.size()>0)
						return 1;
					else
						return 0;
				}else{
					boolean flagSPED=false;
					List<UserMaster> umList=groupMap.get(group[0]);
					List<UserMaster> umSPEDList=null;
					if(SPED)
						umSPEDList=groupMap.get("SPED");
					else
						umSPEDList=groupMap.get("SA");	
					System.out.println("umSPEDList======="+umSPEDList);
					List<JobApprovalHistory> JAH_SPEDList=jobApprovalHistoryDAO.findHistoryByUser(umSPEDList);
					if(JAH_SPEDList!=null && JAH_SPEDList.size()>0)
						return 1;
					else
						return 0;
				}
			}catch(Throwable t){t.printStackTrace();return 0;}
		}else{
			return 1;
		}
	}

	public Map<String,List<UserMaster>>  getApprovalInfoDetails(String schoolId, String primaryESTechnician, String showspecialEdGroup, Integer jobId, Integer districtId)
	{
		/* ========  For Session time Out Error =========*/
		System.out.println("calling getApprovalInfoDetails:::::"+showspecialEdGroup+":::::"+districtId);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException("Your session has expired!");
		}
		Map<String,List<UserMaster>> groupList=new LinkedHashMap<String,List<UserMaster>>();
		try{
			//ram nath
			//Grant Group
			DistrictMaster districtMaster = districtMasterDAO.findByDistrictId(districtId.toString());
			//districtMaster.setDistrictId(804800);
			Map<String,Map<String,DistrictKeyContact>> dWAG_dAG_Map=new LinkedHashMap<String,Map<String,DistrictKeyContact>>();
			Map<String,DistrictWiseApprovalGroup> dWAGMap=new LinkedHashMap<String,DistrictWiseApprovalGroup>();
			Map<String,DistrictKeyContact> dAGToKeyContactMap=new LinkedHashMap<String,DistrictKeyContact>();
			Criterion cri1 = Restrictions.eq("districtMaster", districtMaster);
			Criterion cri3 = Restrictions.eq("specialEdFlag", showspecialEdGroup);
			Criterion cri4 = Restrictions.eq("status", "A");
			List<DistrictSpecificApprovalFlow> districtSpecificApprovalFlowList =  districtSpecificApprovalFlowDAO.findByCriteria(cri1, cri3,cri4);
			DistrictSpecificApprovalFlow dsaf=null;
			if(districtSpecificApprovalFlowList.size()>0){
				dsaf=districtSpecificApprovalFlowList.get(0);
				//System.out.println("dsaf.getApprovalFlow()===================="+dsaf.getApprovalFlow());
				String[] groupShortName=dsaf.getApprovalFlow().trim().equals("")?null:dsaf.getApprovalFlow().trim().substring(1,(dsaf.getApprovalFlow().length())).split("#");
				if(groupShortName!=null)
					for(String groupShort:groupShortName)
						dWAGMap.put(groupShort.trim(), null);

				//System.out.println("groupShortName=========================="+Arrays.asList(groupShortName)+"  districtId=="+ dsaf.getDistrictMaster().getDistrictId()+" status===A");
				if(groupShortName!=null){
					Criterion cri_shortName=Restrictions.in("groupShortName", Arrays.asList(groupShortName));
					Criterion cri_district=Restrictions.eq("districtId", dsaf.getDistrictMaster());
					Criterion cri_status= Restrictions.eq("status", "A");
					List<DistrictWiseApprovalGroup> districtWiseApprovalGroupLst=districtWiseApprovalGroupDAO.findByCriteria(cri_shortName,cri_district,cri_status);
					//System.out.println("districtWiseApprovalGroupLst==================================="+districtWiseApprovalGroupLst.size());
					if(districtWiseApprovalGroupLst.size()>0){
						List<String> groupNamelst=new ArrayList<String>();
						for(DistrictWiseApprovalGroup dwAG:districtWiseApprovalGroupLst){
							//System.out.println("  ------------ "+dwAG.getGroupShortName());
							dWAGMap.put(dwAG.getGroupShortName(), dwAG);
							groupNamelst.add(dwAG.getGroupName());
						}
						if(groupNamelst.size()>0){
							//System.out.println("groupNamelst======="+groupNamelst);
							List<DistrictApprovalGroups> dAGList=districtApprovalGroupsDAO.findByCriteria(Restrictions.in("groupName",groupNamelst),Restrictions.isNull("jobCategoryId"),Restrictions.isNull("jobId"),Restrictions.eq("districtId", dsaf.getDistrictMaster()));
							//System.out.println("dAGList=============="+dAGList.size());
							if(dAGList.size()>0){
								List<Integer> keyContactId=new ArrayList<Integer>();
								Map<String,DistrictKeyContact> memberToKeyContactMap=new LinkedHashMap<String,DistrictKeyContact>();
								for(DistrictApprovalGroups districtAppGroup:dAGList){
									if(districtAppGroup.getGroupMembers()!=null && !districtAppGroup.getGroupMembers().trim().equals(""))
										for(String memberKey:districtAppGroup.getGroupMembers().split("#")){
											if(!memberKey.trim().equals(""))
												try{
													keyContactId.add(Integer.parseInt(memberKey));
													//System.out.println("keyId============="+memberKey);
												}catch(NumberFormatException e){}
										}
								}
								if(keyContactId.size()>0){
									List<DistrictKeyContact> districtKeyContactList = districtKeyContactDAO.findByCriteria(Restrictions.in("keyContactId", keyContactId));
									for(DistrictKeyContact dkc:districtKeyContactList)
										memberToKeyContactMap.put(dkc.getKeyContactId().toString(), dkc);
								}

								for(DistrictApprovalGroups districtAppGroup:dAGList){
									dAGToKeyContactMap=new LinkedHashMap<String,DistrictKeyContact>();
									if(districtAppGroup.getGroupMembers()!=null && !districtAppGroup.getGroupMembers().trim().equals(""))
										for(String memberKey:districtAppGroup.getGroupMembers().split("#")){
											if(!memberKey.trim().equals(""))
												try{
													dAGToKeyContactMap.put(memberKey,memberToKeyContactMap.get(memberKey));
												}catch(NumberFormatException e){}
										}
									dWAG_dAG_Map.put(districtAppGroup.getGroupName(), dAGToKeyContactMap);
								}
							}
						}
					}
				}

			}

			for(Map.Entry<String, DistrictWiseApprovalGroup> entry:dWAGMap.entrySet()){
				if(entry.getKey().equals("SA") || entry.getKey().equals("ES")){
					if(entry.getKey().equals("SA")){
						groupList.put("SA", getSchoolTable(schoolId, jobId,dWAG_dAG_Map.get(entry.getValue().getGroupName())));
					}
					if(entry.getKey().equals("ES")){
						groupList.put("ES",getES(primaryESTechnician));
					}
				}else{
					Map<String,DistrictKeyContact> dWAG_dAG1_Map=dWAG_dAG_Map.get(entry.getValue().getGroupName());
					if(dWAG_dAG1_Map!=null){
						List<UserMaster> umList=new ArrayList<UserMaster>();
						for(Map.Entry<String, DistrictKeyContact> entry1:dWAG_dAG1_Map.entrySet()){
							umList.add(entry1.getValue().getUserMaster());
						}
						groupList.put("SPED", umList);
					}
				}
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return groupList;
	}
	private List<UserMaster> getSchoolTable(String schoolId, Integer jobId, Map<String,DistrictKeyContact> keyContactSAs){
		List<UserMaster> umSA=new ArrayList<UserMaster>();
		if(schoolId==null || schoolId.equals(""))
		{
			JobOrder jobOrder = new JobOrder();
			jobOrder.setJobId(jobId);
			List<SchoolInJobOrder> schoolInJobOrders = schoolInJobOrderDAO.findJobOrder(jobOrder);
			schoolId="";
			for(SchoolInJobOrder order : schoolInJobOrders)
				schoolId = schoolId+","+order.getSchoolId().getSchoolId();
		}			

		if(schoolId!=null && !schoolId.equals(""))
		{
			System.out.println("schoolId:- "+schoolId);
			List<Long> schoolIdList = new ArrayList<Long>();
			for(String id : schoolId.split(","))
				try{schoolIdList.add(Long.parseLong(id));}catch(Exception e){}

				if(schoolIdList!=null && schoolIdList.size()>0)
				{
					Criterion cri1 = Restrictions.in("schoolId", schoolIdList);
					Criterion cri2 = Restrictions.eq("status", "A");

					List<SchoolMaster> schoolMasterList = schoolMasterDAO.findByCriteria(cri1,cri2);

					Criterion criterion1 = Restrictions.eq("entityType", new Integer(3));
					Criterion criterion_role = Restrictions.eq("roleId", roleMasterDAO.findById(3,false,false));
					Criterion criterion2 = Restrictions.in("schoolId", schoolMasterList);
					Criterion criterion4 = Restrictions.eq("status","A");

					List<UserMaster> userMaster = userMasterDAO.findByCriteria(criterion1,criterion2,criterion4,criterion_role);

					System.out.println("userMaster:- "+userMaster);
					int i=1;
					boolean flagUserSAs=true;
					boolean flagKeyContactSAs=true;
					if(userMaster!=null && userMaster.size()>0)
					{
						for(UserMaster master : userMaster)
						{
							umSA.add(master);
							i++;
							flagUserSAs=false;
						}
					}
					if(keyContactSAs!=null)
						for(Map.Entry<String,DistrictKeyContact> entry:keyContactSAs.entrySet()){
							if(entry.getValue()!=null){
								umSA.add(entry.getValue().getUserMaster());
								i++;
								flagKeyContactSAs=false;
							}
						}
				}
		}
		return umSA;
	}

	private List<UserMaster> getES(String primaryESTechnician){
		List<UserMaster> umES=new ArrayList<UserMaster>();
		List<String> emailList=new ArrayList<String>();
		if(primaryESTechnician!=null && !primaryESTechnician.equals(""))
		{
			try
			{
				System.out.println("primaryESTechnician:- "+primaryESTechnician);
				Integer pid= Integer.parseInt(primaryESTechnician);

				EmploymentServicesTechnician employmentServicesTechnician = employmentServicesTechnicianDAO.findById(pid, false, false);
				if(employmentServicesTechnician!=null)
				{
					emailList.add(employmentServicesTechnician.getPrimaryEmailAddress()!=null?employmentServicesTechnician.getPrimaryEmailAddress():"");
					emailList.add(employmentServicesTechnician.getBackupEmailAddress()!=null?employmentServicesTechnician.getBackupEmailAddress():"");
				}
				umES=userMasterDAO.findByCriteria(Restrictions.in("emailAddress", emailList));
			}
			catch(Exception e){e.printStackTrace();}
		}
		return umES;
	}
	@Transactional(readOnly=false)
	public String approveJobOkForAdams(int entityType,Integer jobId,Integer userId)
	{

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster=null;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}else{
			userMaster=(UserMaster)session.getAttribute("userMaster");
		}
		try{
			String ipAddress = IPAddressUtility.getIpAddress(request);
			JobOrder  jobOrder = jobOrderDAO.findById(jobId, false, false);
			DistrictMaster districtMaster = jobOrder.getDistrictMaster();
			Integer jobcreatorUserId=jobOrder.getCreatedBy();
			UserMaster jobCreatorUser=	userMasterDAO.findById(jobcreatorUserId, false, false);
			List<DistrictKeyContact> districtKeyContactOfJobCreator=districtKeyContactDAO.findByContactType(jobCreatorUser, "Job Approval");Map<Integer ,DistrictApprovalGroups> distrMap=new LinkedHashMap<Integer, DistrictApprovalGroups>();
			List<JobWiseApprovalProcess>	jobWiseApprovalProcessList=	jobWiseApprovalProcessDAO.findByJobId(jobOrder);
			if(jobWiseApprovalProcessList!=null && jobWiseApprovalProcessList.size()>0){
				JobWiseApprovalProcess jobWiseApprovalProcess=jobWiseApprovalProcessList.get(0);
				JobApprovalProcess jobApprovalProcess=jobWiseApprovalProcess.getJobApprovalProcessId();
				String groups=jobApprovalProcess.getJobApprovalGroups();
				String groupIds[]=null;
				groupIds=groups.split("\\|");
				List<Integer> groupApprovalIds=new ArrayList<Integer>();
				if(groupIds!=null)
				{
					for(String goupid:groupIds){
						
						if(!goupid.equals("")){
							try{
								Integer grpid = Integer.parseInt(goupid);
								groupApprovalIds.add(grpid);
							}catch(Exception e){e.printStackTrace();}
						}
					}
				}
				List<DistrictApprovalGroups> districtApprovalGroupsList=	districtApprovalGroupsDAO.findGroupsByDistrictAndDistrictApprovalGroupsId(jobOrder.getDistrictMaster(),groupApprovalIds);
				if( districtApprovalGroupsList!=null && districtApprovalGroupsList.size()>0 )
				{
						List<JobApprovalHistory> jobApprovalHistories = jobApprovalHistoryDAO.findByCriteria(Restrictions.and(Restrictions.in("districtApprovalGroups", districtApprovalGroupsList), Restrictions.eq("jobOrder", jobOrder)));
						DistrictApprovalGroups currentApprovalGroup=null;
						DistrictApprovalGroups nextApprovalGroup=null;
						List<DistrictKeyContact> districtKeyContactList = null;
						boolean isSendMailToNextGroup=false;
						boolean currentApproval=true;
						boolean nextapproval=false;
						DistrictKeyContact userKeyContact=null;
						List<DistrictKeyContact> userKeyContactList=districtKeyContactDAO.findByContactType(userMaster, "Job Approval");
						if(userKeyContactList!=null&&userKeyContactList.size()>0){
							userKeyContact=userKeyContactList.get(0);
						}
						for(DistrictApprovalGroups dgp:districtApprovalGroupsList){
							distrMap.put(dgp.getDistrictApprovalGroupsId(), dgp);
						}
						for(Integer gropuId:groupApprovalIds){
							DistrictApprovalGroups districtApprovalGroups=	distrMap.get(gropuId);
							boolean isHistoryContainsCurrentGroup=false;
							for(JobApprovalHistory history : jobApprovalHistories)
							{
								if(history.getDistrictApprovalGroups().getDistrictApprovalGroupsId() == districtApprovalGroups.getDistrictApprovalGroupsId())
								{
									isHistoryContainsCurrentGroup=true;
									break;
								}
							}
							if(!isHistoryContainsCurrentGroup){
								if(currentApproval==false){
									nextapproval=true;
								}
								if(currentApproval && districtApprovalGroups.getGroupMembers()!=null)
								if(districtApprovalGroups.getGroupMembers().contains("#"+userKeyContact.getKeyContactId()+"#")){
									currentApprovalGroup=districtApprovalGroups;
									currentApproval=false;
								}
								if(nextapproval && districtApprovalGroups.getGroupMembers()!=null){
									if(districtKeyContactOfJobCreator!=null &&districtKeyContactOfJobCreator.size()>0){	
									if(!districtApprovalGroups.getGroupMembers().contains("#"+districtKeyContactOfJobCreator.get(0).getKeyContactId()+"#")){
										nextApprovalGroup=districtApprovalGroups;
										isSendMailToNextGroup=true;
										break;
										}
										}else{
											nextApprovalGroup=districtApprovalGroups;
											isSendMailToNextGroup=true;
											break;
										}
									}
								}
						}
						if(currentApprovalGroup!=null)
						{
							JobApprovalHistory jobApprovalHistory = new JobApprovalHistory();
							jobApprovalHistory.setCreatedDateTime(new Date());
							jobApprovalHistory.setUserMaster(userMaster);
							jobApprovalHistory.setJobOrder(jobOrder);
							jobApprovalHistory.setIpAddress(ipAddress);
							jobApprovalHistory.setDistrictApprovalGroups(currentApprovalGroup);
							jobApprovalHistoryDAO.makePersistent(jobApprovalHistory);
						}
						if(isSendMailToNextGroup)//isSendMail
						{
							if(nextApprovalGroup!=null){
								ArrayList<Integer> membersIds = new ArrayList<Integer>();
								if(nextApprovalGroup.getGroupMembers()!=null && !nextApprovalGroup.getGroupMembers().trim().equals(""))
								for(String nm : nextApprovalGroup.getGroupMembers().split("#"))
									if( !nm.equals("") )
										try{membersIds.add(Integer.parseInt(nm));}catch(Exception e){}
										if(membersIds.size()>0)
										{
											districtKeyContactList = districtKeyContactDAO.findByCriteria(Restrictions.in("keyContactId", membersIds));
											if(districtKeyContactList!=null && districtKeyContactList.size()>0){
												sendEmailToJobApprovalGroup(request, districtKeyContactList, jobOrder.getJobId(), jobOrder.getJobTitle());
												jobOrder.setApprovalBeforeGoLive(2);
												jobOrderDAO.updatePersistent(jobOrder);	}}
							}
						} else{
							jobOrder.setApprovalBeforeGoLive(1);
							jobOrderDAO.updatePersistent(jobOrder);
							ElasticSearchService es=new ElasticSearchService();
							SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
							if(jobOrder.getStatus().equalsIgnoreCase("A") && (jobOrder.getIsInviteOnly()==null ||  !jobOrder.getIsInviteOnly()) && jobOrder.getApprovalBeforeGoLive()!=null && jobOrder.getApprovalBeforeGoLive()==1 
									&& (jobOrder.getJobStartDate().before(new Date()) ||  jobOrder.getJobStartDate().equals(new Date())) 
									&& (jobOrder.getJobEndDate().after(sdf.parse(sdf.format(new Date()))) ||  jobOrder.getJobEndDate().equals(sdf.parse(sdf.format(new Date())))
											&& (jobOrder.getHiddenJob()==null ||  !jobOrder.getHiddenJob())	) 
									)
							{
								es.updateESDistrictJobBoardByJobId(jobOrder);
							}

						}
				}
			}

		} catch (Exception e) 
		{
			e.printStackTrace();
			return "0";
		}


		return "0";
	}

	public int sendEmailToJobApprovalGroup(HttpServletRequest request, List<DistrictKeyContact>  approvalContacts, Integer returnJobId,String jobTitle)
	{
		String baseURL=Utility.getBaseURL(request);
		String content="";
		for (DistrictKeyContact districtKeyContact : approvalContacts) {
			DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
			dsmt.setEmailerService(emailerService);
			dsmt.setMailfrom("admin@netsutra.com");
			dsmt.setMailto(districtKeyContact.getKeyContactEmailAddress());
			dsmt.setMailsubject("Approval Mail");
			System.out.println("returnJobId   "+returnJobId);
			System.out.println("districtKeyContact.getUserMaster().getUserId()   "+districtKeyContact.getUserMaster().getUserId());
			String incJobId = Utility.encryptNo(returnJobId);
			String incUserId = Utility.encryptNo(districtKeyContact.getUserMaster().getUserId());

			String linkIds= incJobId+"###"+incUserId;
			String forMated = "";
			try {
				forMated = Utility.encodeInBase64(linkIds);
				forMated = baseURL+"approveJob.do?id="+forMated;
			} catch (Exception e) {
				e.printStackTrace();
				return 2;
			}

			System.out.println("forMated   "+forMated);
			//	System.out.println("returnjobTitle   "+returnjobTitle);
			content=MailText.getApproveJObMailText(request,districtKeyContact, forMated,jobTitle);					
			dsmt.setMailcontent(content);
			try {
				dsmt.start();	
			} catch (Exception e) {

			}
		}
		return 1;
	}

	
	public int 	isApproveOrNotforAdams(JobOrder jobOrder,HttpServletRequest request){
		
		System.out.println("jobId================"+jobOrder.getJobId());
		HttpSession session=null;
		session = request.getSession(false);
		UserMaster userMaster=null;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}else{
			userMaster=(UserMaster)session.getAttribute("userMaster");
		}
		//JobOrder jobOrder = jobOrderDAO.findById(jobId, false, false);
		if(jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId().toString().equals("806900")){
			List<JobWiseApprovalProcess>	jobWiseApprovalProcessList=	jobWiseApprovalProcessDAO.findByJobId(jobOrder);
		//	Map<Integer ,DistrictApprovalGroups> distrMap=new LinkedHashMap<Integer, DistrictApprovalGroups>();
			if(jobWiseApprovalProcessList!=null && jobWiseApprovalProcessList.size()>0){
				JobWiseApprovalProcess jobWiseApprovalProcess=jobWiseApprovalProcessList.get(0);
				JobApprovalProcess jobApprovalProcess=jobWiseApprovalProcess.getJobApprovalProcessId();
				String groups=jobApprovalProcess.getJobApprovalGroups();
				String groupIds[]=null;

				groupIds=groups.split("\\|");
				List<Integer> groupApprovalIds=new ArrayList<Integer>();
				if(groupIds!=null)
				{
					for(String goupid:groupIds){
						
						if(!goupid.equals("")){
							try{
								Integer grpid = Integer.parseInt(goupid);
								groupApprovalIds.add(grpid);
							}catch(Exception e){e.printStackTrace();}
						}
					}
				}
				List<DistrictApprovalGroups> districtApprovalGroupsList=	districtApprovalGroupsDAO.findGroupsByDistrictAndDistrictApprovalGroupsId(jobOrder.getDistrictMaster(),groupApprovalIds);
				if(districtApprovalGroupsList!=null && districtApprovalGroupsList.size()>0){
					List<Integer> groupMemberIds = new ArrayList<Integer>();
					for(DistrictApprovalGroups districtApprovalGroups:districtApprovalGroupsList){
						if(districtApprovalGroups.getGroupMembers()!=null && !districtApprovalGroups.getGroupMembers().trim().equals("")){
							for(String id:districtApprovalGroups.getGroupMembers().split("#")){
								if(!id.equals(""))
									groupMemberIds.add(Integer.parseInt(id));
							}
						}
					}
					if(groupMemberIds.size()>0){
						List<DistrictKeyContact> districtKeyContactList = districtKeyContactDAO.findByCriteria(Restrictions.in("keyContactId", groupMemberIds));
						DistrictKeyContact districtKeyContact=null;
						for(DistrictKeyContact dkc:districtKeyContactList){
							if(dkc.getUserMaster().getUserId().equals(userMaster.getUserId())){
								districtKeyContact=dkc;
								break;
							}
						}
						List<JobApprovalHistory>	jobApprovalHistoryList	=		jobApprovalHistoryDAO.findByCriteria(Restrictions.in("districtApprovalGroups", districtApprovalGroupsList),Restrictions.eq("jobOrder", jobOrder));
						boolean nextApprovalGroup = false;
						DistrictApprovalGroups districtApprovalGroup = null;
						for(DistrictApprovalGroups districtApprovalGroups:districtApprovalGroupsList){
							nextApprovalGroup = false;
							if(jobApprovalHistoryList!=null &&jobApprovalHistoryList.size()>0)
								for(JobApprovalHistory JobApprovalHistory:jobApprovalHistoryList){
									if(JobApprovalHistory.getDistrictApprovalGroups().getDistrictApprovalGroupsId()==districtApprovalGroups.getDistrictApprovalGroupsId()){
										nextApprovalGroup = true;
										break;
									}
								}
							if(nextApprovalGroup==false){
								districtApprovalGroup = districtApprovalGroups;
								break;
							}
							
						}
						//List<JobApprovalHistory>	jobApprovalHistoryList	=		jobApprovalHistoryDAO.findByCriteria(Restrictions.in("districtApprovalGroups", districtApprovalGroupsList),Restrictions.eq("jobOrder", jobOrder));
						boolean isApproved=false;
						boolean checkApproved=true;
						if(jobApprovalHistoryList!=null &&jobApprovalHistoryList.size()>0)
						for(JobApprovalHistory JobApprovalHistory:jobApprovalHistoryList){
							if(JobApprovalHistory.getDistrictApprovalGroups().getDistrictApprovalGroupsId()==districtApprovalGroup.getDistrictApprovalGroupsId()){
								checkApproved=false;
							}
						}
						
						if(districtApprovalGroup != null && districtApprovalGroup.getGroupMembers().contains("#"+districtKeyContact.getKeyContactId()+"#")){
							isApproved = true;
						}
						if(checkApproved){
							isApproved = true;
						}
						if(isApproved){
							return 1;
						} else{
							return 0;
						}

					}	
				}

			}
			return 0;
		}
		else{
			return 1;
		}
		}
	// Pavan Gupta
	public String getApprovalGroupDetails(int jobType, Integer jobId ,int districtId) {		
		StringBuffer jobApprovalBold = new StringBuffer();
		DistrictKeyContact jobCreatorKeyContact=null;
		try {
			JobOrder jobOrder = jobOrderDAO.findById(jobId, false, false);

			Integer createdBy = jobOrder.getCreatedBy();
			if (createdBy != null) {
				UserMaster userMaster = userMasterDAO.findById(createdBy,false, false);
				if (userMaster != null) {
					List<DistrictKeyContact>	districtKeyContactList=districtKeyContactDAO.findByContactType(userMaster, "Job Approval");
					if(districtKeyContactList!=null &&districtKeyContactList.size()>0){
						jobCreatorKeyContact=districtKeyContactList.get(0);
						}
				}

			}
			Map<Integer, DistrictApprovalGroups> dag = new HashMap<Integer, DistrictApprovalGroups>();
			List<JobWiseApprovalProcess> jobWiseApprovalProcessList = jobWiseApprovalProcessDAO.findByJobId(jobOrder);
			if (jobWiseApprovalProcessList != null && jobWiseApprovalProcessList.size() > 0) {
				JobWiseApprovalProcess jobWiseApprovalProcess = jobWiseApprovalProcessList.get(0);
				String approvalIds = jobWiseApprovalProcess.getJobApprovalProcessId().getJobApprovalGroups();
				String groupIds[] = null;
				groupIds = approvalIds.split("\\|");
				List<Integer> groupApprovalIds = new ArrayList<Integer>();
				if (groupIds != null) {
					for (String goupid : groupIds) {
						if (!goupid.equals("")) {
							try {
								Integer grpid = Integer.parseInt(goupid);
								groupApprovalIds.add(grpid);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					}
					if(groupApprovalIds.size()>0){
					DistrictMaster districtMaster = districtMasterDAO.findById(districtId, false, false);
					List<DistrictApprovalGroups> districtApprovalGroupsList = districtApprovalGroupsDAO.findGroupsByDistrictAndDistrictApprovalGroupsId(districtMaster, groupApprovalIds);
					if (districtApprovalGroupsList != null && districtApprovalGroupsList.size() > 0) {
						for (DistrictApprovalGroups districtApprovalGroups : districtApprovalGroupsList)
							dag.put(districtApprovalGroups.getDistrictApprovalGroupsId(),districtApprovalGroups);
						Criterion criterion = Restrictions.eq("jobOrder",jobOrder);
						Criterion criterion1 = Restrictions.isNotNull("districtApprovalGroups");
						List<JobApprovalHistory> jobApprovalHistoryList = jobApprovalHistoryDAO.findByCriteria(criterion, criterion1);
						DistrictApprovalGroups currentapprovalgroup = null;

						for (Integer id : groupApprovalIds) {
							DistrictApprovalGroups districtApprovalGroups = dag.get(id);
							if (jobApprovalHistoryList != null && jobApprovalHistoryList.size() > 0) {
								boolean currentapproval = true;
								for (JobApprovalHistory jobApprovalHistory : jobApprovalHistoryList) {					
									if (jobApprovalHistory.getDistrictApprovalGroups().getDistrictApprovalGroupsId() == districtApprovalGroups.getDistrictApprovalGroupsId()) {
										currentapproval = false;
										break;
									}
								}
								if (currentapproval) {
									if(jobCreatorKeyContact!=null){
										if(districtApprovalGroups.getGroupMembers()!=null && !districtApprovalGroups.getGroupMembers().contains("#"+jobCreatorKeyContact.getKeyContactId()+"#")){
											currentapprovalgroup = districtApprovalGroups;
											break;
										}
									} else{
										currentapprovalgroup = districtApprovalGroups;
										break;
									}
								}
							} else {
								currentapprovalgroup = districtApprovalGroups;
								break;
							}

						}
						for (Integer id : groupApprovalIds) {
							DistrictApprovalGroups districtApprovalGroups = dag.get(id);
							if(currentapprovalgroup!=null){
								if (districtApprovalGroups.getDistrictApprovalGroupsId() == currentapprovalgroup.getDistrictApprovalGroupsId() && jobOrder.getApprovalBeforeGoLive()!=1) {
									jobApprovalBold.append("<center><h5><b> <label>Group: </label> "+ districtApprovalGroups.getGroupName()+ "</b></h5></center>");
							} else{
								jobApprovalBold.append("<center><h5><label> Group: </label> "+ districtApprovalGroups.getGroupName()+ "</h5></center>");
							}
							}else if(districtApprovalGroups!=null){
								jobApprovalBold.append("<center><h5><label> Group: </label> "+ districtApprovalGroups.getGroupName()+ "</h5></center>");
							}
						}
					}else {
						jobApprovalBold.append("<center><h5><label></label> "+ Utility.getLocaleValuePropByKey("lblNoGroupWithApprovalProcess", locale)+ "</h5></center>");
					}
					}else {
						jobApprovalBold.append("<center><h5><label></label> "+Utility.getLocaleValuePropByKey("lblNoGroupWithApprovalProcess", locale)+ "</h5></center>");
					}

				}
			}else{
				jobApprovalBold.append("<center><h5><label></label> "+Utility.getLocaleValuePropByKey("lblNoApprovalProcessWithJobOrder", locale)+ "</h5></center>");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return jobApprovalBold.toString();
	}
	}