ALTER TABLE  `rawdatafordomain` ADD  `createdDateTimeOld` TIMESTAMP NULL AFTER  `createdDateTime` ;
update  `rawdatafordomain` set  `createdDateTimeOld` = `createdDateTime` ;

ALTER TABLE  `rawdataforcompetency` ADD  `createdDateTimeOld` TIMESTAMP NULL AFTER  `createdDateTime` ;
update  `rawdataforcompetency` set  `createdDateTimeOld` = `createdDateTime` ;

ALTER TABLE  `percentilecalculation` ADD  `createdDateTimeOld` DATE NULL AFTER  `createdDateTime` ;
update  `percentilecalculation` set  `createdDateTimeOld` = `createdDateTime` ;

ALTER TABLE  `percentilecalculationcompetency` ADD  `createdDateTimeOld` DATE NULL AFTER  `createdDateTime` ;
update  `percentilecalculationcompetency` set  `createdDateTimeOld` = `createdDateTime` ;

ALTER TABLE  `percentilescorebyjob` ADD  `createdDateTimeOld` DATE NULL AFTER  `createdDateTime` ;
update  `percentilescorebyjob` set  `createdDateTimeOld` = `createdDateTime` ;

ALTER TABLE  `teacherassessmentstatus` ADD  `createdDateTimeOld` DATETIME NULL AFTER  `createdDateTime` ;
update  `teacherassessmentstatus` set  `createdDateTimeOld` = `createdDateTime` ;

ALTER TABLE  `teacherassessmentattempt` ADD  `assessmentStartTimeOld` DATETIME NULL AFTER  `assessmentStartTime` ;
update  `teacherassessmentattempt` set  `assessmentStartTimeOld` = `assessmentStartTime` ;

ALTER TABLE  `teacherassessmentattempt` ADD  `assessmentEndTimeOld` DATETIME NULL AFTER  `assessmentEndTime` ;
update  `teacherassessmentattempt` set  `assessmentEndTimeOld` = `assessmentEndTime` ;

ALTER TABLE  `teacherstrikelog` ADD  `createdDateTimeOld` DATETIME NULL AFTER  `createdDateTime` ;
update  `teacherstrikelog` set  `createdDateTimeOld` = `createdDateTime` ;

UPDATE  `rawdatafordomain` SET  `createdDateTime` =  '2014-01-01 00:02:00';
UPDATE  `rawdataforcompetency` SET  `createdDateTime` =  '2014-01-01 00:02:00';
UPDATE  `percentilecalculation` SET  `createdDateTime` =  '2014-01-01'; 
UPDATE  `percentilecalculationcompetency` SET  `createdDateTime` =  '2014-01-01'; 
UPDATE  `percentilescorebyjob` SET  `createdDateTime` =  '2014-01-01'; 
UPDATE  `teacherassessmentstatus` SET  `createdDateTime` =  '2014-01-01 00:02:00';
UPDATE  `teacherassessmentattempt` SET  `assessmentStartTime` =  '2014-01-01 00:02:00';
UPDATE  `teacherassessmentattempt` SET  `assessmentEndTime` =  '2014-01-01 00:02:00';
UPDATE  `teacherstrikelog` SET  `createdDateTime` =  '2014-01-01 00:02:00';