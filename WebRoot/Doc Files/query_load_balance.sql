ALTER TABLE  `schoolmaster` ADD  `locationCode` VARCHAR( 15 ) NULL COMMENT  'LocationId will be Unique for all the Schools of a District' AFTER  `schoolName` ;
 
----Gourav 08-05-2014-----
CREATE TABLE `jobpoolmaster` (
  `JobpoolId` int(11) NOT NULL auto_increment,
  `districtId` int(5) NOT NULL COMMENT 'Foreign Key to "districtId" in table "districtMaster"',
  `poolId` int(11) NOT NULL,
  `jobId` int(11) NOT NULL,
  PRIMARY KEY  (`JobpoolId`),
  UNIQUE KEY `jobId` (`jobId`)
) ENGINE=InnoDB;

CREATE TABLE `poolmaster` (
  `poolId` int(11) NOT NULL auto_increment,
  `districtId` int(5) NOT NULL COMMENT 'Foreign Key to "districtId" in table "districtMaster"',
  `poolCode` varchar(10) NOT NULL,
  `poolTitle` varchar(250) NOT NULL,
  `status` varchar(1) NOT NULL default 'A' COMMENT '"I" - Inactive, "A" - Active By default "A"',
  `createdDateTime` datetime NOT NULL COMMENT 'Current Date Time',
  PRIMARY KEY  (`poolId`),
  UNIQUE KEY `poolId` (`poolId`),
  KEY `districtId` (`districtId`)
) ENGINE=InnoDB;

CREATE TABLE `jobmaster` (
  `jobId` int(11) NOT NULL auto_increment,
  `districtId` int(5) NOT NULL COMMENT 'Foreign Key to "districtId" in table "districtMaster"',
  `jobCode` varchar(10) NOT NULL,
  `jobTitle` varchar(200) NOT NULL,
  `jobDescription` text,
  `status` varchar(1) NOT NULL default 'A' COMMENT '"I" - Inactive, "A" - Active By default "A"',
  `createdDateTime` datetime NOT NULL COMMENT 'Current Date Time',
  PRIMARY KEY  (`jobId`),
  UNIQUE KEY `jobId` (`jobId`),
  KEY `districtId` (`districtId`)
) ENGINE=InnoDB ;

---- GOURAV 12-05-2014-------->
ALTER TABLE jobpoolmaster ADD FOREIGN KEY ( poolId ) REFERENCES poolmaster( poolId );
ALTER TABLE jobpoolmaster ADD FOREIGN KEY ( jobId ) REFERENCES jobmaster( jobId );

---------12-05-2014 ( Sekhar )--------
ALTER TABLE  `districtmaster` ADD  `isZoneRequired` TINYINT( 1 ) NOT NULL DEFAULT  '0' AFTER  `status` ;
ALTER TABLE  `joborder` ADD  `geoZoneId` INT( 6 ) NULL AFTER  `subjectId` ;
ALTER TABLE  `joborder` ADD  `isPoolJob` VARCHAR( 1 ) NOT NULL DEFAULT  '0' AFTER  `jobType` ;
ALTER TABLE  `joborder` CHANGE  `jobType`  `jobType` VARCHAR( 2 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT  'F';


--13-05-2014-------(Sekhar)-----------
UPDATE  `districtmaster` SET  `districtName` =  'Miami-Dade County Public Schools' WHERE  `districtmaster`.`districtId` =1200390 LIMIT 1 ;

--16-05-2014 ----rajendra --
CREATE TABLE  `certificationTypeMaster` (
`certificationTypeMasterId` INT( 3 ) NOT NULL AUTO_INCREMENT ,
`certificationType` VARCHAR( 50 ) NOT NULL ,
`status` VARCHAR( 1 ) NOT NULL COMMENT  '"I" - Inactive, "A" - Active',
PRIMARY KEY (`certificationTypeMasterId` )
) ENGINE = INNODB

ALTER TABLE  `teachercertificate` ADD  `yearExpired` INT( 4 ) NOT NULL AFTER  `yearReceived` ,
ADD  `doeNumber` VARCHAR( 25 ) NOT NULL AFTER  `yearExpired` ;

ALTER TABLE  `teachercertificate` ADD  `certificationTypeMasterId` INT( 1 ) NULL AFTER  `certificationStatusId` ;

INSERT INTO `certificationTypeMaster` (`certificationTypeMasterId`, `certificationType`, `status`) VALUES
(1, 'Certificate', 'A'),
(2, 'Statement Of Eligibility', 'A'),
(3, 'Endorsment', 'A');

--- Gourav 16-05-2014------------- have to run
ALTER TABLE  `schoolmaster` ADD  `division` INT( 4 ) NULL AFTER  `description` ,
ADD  `grades` VARCHAR( 50 ) NULL AFTER  `division` ,
ADD  `schoolSite` VARCHAR( 1 ) NULL COMMENT  'Y = "yes" N = "no"' AFTER  `grades` ,
ADD  `geoZoneId` INT( 6 ) NULL COMMENT  'Foreign Key to "geoZoneId" in table "geoZoneMaster"' AFTER  `schoolSite` ;

----------- AShish 17-05-2014------------------
ALTER TABLE  `teacherpersonalinfo` ADD  `dob` DATE NULL AFTER  `employeeCode` ,

ADD  `employeeType` TINYINT( 1 ) NULL COMMENT  '"0" - Not an Employee "1" - Current Employee "2" - Former Employee' AFTER  `dob` ,

ADD  `employeeNumber` VARCHAR( 50 ) NULL AFTER  `employeeType` ,

ADD  `isRetiredEmployee` TINYINT( 1 ) NULL COMMENT  '"0" - Not a Retired Teacher "1" - Retired Teacher' AFTER  `employeeNumber` ,

ADD  `isCurrentFullTimeTeacher` TINYINT( 1 ) NULL COMMENT  '"0" - Not a Full Time Teacher "1" - Full Time Teacher' AFTER  `isRetiredEmployee` ,

ADD  `isVateran` TINYINT NULL COMMENT  '"0" - Not a Vateran "1" - Vateran' AFTER  `isCurrentFullTimeTeacher` ;

ALTER TABLE  `districtportfolioconfig` ADD  `personalinfo` TINYINT( 1 ) NOT NULL DEFAULT  '0' AFTER  `phoneNumber` ,

ADD  `ssn` TINYINT( 1 ) NOT NULL DEFAULT  '0' AFTER  `personalinfo` ,

ADD  `race` TINYINT( 1 ) NOT NULL DEFAULT  '0' AFTER  `ssn` ;

ALTER TABLE  `districtportfolioconfig` ADD  `formeremployee` TINYINT NULL AFTER  `race` ;

ALTER TABLE  `districtportfolioconfig` CHANGE  `formeremployee`  `formeremployee` TINYINT( 4 ) NOT NULL DEFAULT  '0';

ALTER TABLE  `teacherpersonalinfo` ADD  `middleName` VARCHAR( 50 ) NULL AFTER  `firstName` ;

-----------------------------------------------------------------------------------------------------------


-- 17-05-2014 Rajendra 

CREATE TABLE  `ethinicitymaster` (
`ethnicityId` INT( 3 ) NOT NULL AUTO_INCREMENT ,
`ethnicityName` VARCHAR( 100 ) NOT NULL ,
`orderBy` INT( 2 ) NULL ,
`status` VARCHAR( 1 ) NOT NULL COMMENT  '"I" - Inactive, "A" - Active',
PRIMARY KEY (  `ethnicityId` )
) ENGINE = INNODB


CREATE TABLE  `ethnicoriginmaster` (
`ethnicOriginId` INT( 3 ) NOT NULL AUTO_INCREMENT ,
`ethnicOriginName` VARCHAR( 100 ) NOT NULL ,
`orderBy` INT( 2 ) NULL ,
`status` VARCHAR( 1 ) NOT NULL COMMENT  '"I" - Inactive, "A" - Active',
PRIMARY KEY (  `ethnicOriginId` )
) ENGINE = INNODB

ALTER TABLE  `teacherpersonalinfo` ADD  `ethnicOriginId` INT( 3 ) NULL COMMENT  'Foreign Key to "ethnicOriginId" in table "ethnicOriginMaster"' AFTER  `lastName` ,

ADD  `ethnicityId` INT( 3 ) NULL COMMENT  'Foreign Key to "ethnicityId" in table "ethnicityMaster"' AFTER  `ethnicOriginId` ;

ALTER TABLE `ethinicitymaster` ADD `ethnicityCode` VARCHAR( 5 ) NOT NULL AFTER `ethnicityId` ,
ADD UNIQUE (

`ethnicityCode`

)


INSERT INTO `ethinicitymaster` (`ethnicityId`, `ethnicityCode`, `ethnicityName`, `orderBy`, `status`) VALUES (NULL, 'E1', 'Hispanic/Latino', '1', 'A'), (NULL, 'E2', 'Not Hispanic/Latino', '2', 'A');
 

INSERT INTO `ethnicoriginmaster` (`ethnicOriginId`, `ethnicOriginName`, `orderBy`, `status`) VALUES (NULL, 'White/Not Hispanic origin', '1', 'A'), (NULL, 'Black/Not Hispanic origin', '2', 'A'), (NULL, 'Hispanic', '3', 'A'), (NULL, 'Asian or Pacific Islander', '4', 'A'), (NULL, 'American Indian/Alaskan', '5', 'A'), (NULL, 'Native Hawaiian', '6', 'A'), (NULL, 'Hispanic White Only', '7', 'A'), (NULL, 'Hispanic All Other', '8', 'A');


---- vishwanath 17-05-2014
ALTER TABLE  `usermaster` CHANGE  `employeeId`  `employeeId` INT( 10 ) NULL DEFAULT NULL;
ALTER TABLE  `usermaster` add foreign key (employeeId) references employeemaster(employeeId);
ALTER TABLE  `employeemaster` ADD  `emailAddress` VARCHAR( 75 ) NULL AFTER  `lastName` ;
ALTER TABLE  `usermaster` ADD  `middleName` VARCHAR( 50 ) NULL AFTER  `firstName` ;

---sekhar 18-05-2013 ----------

CREATE TABLE `teachergeneralknowledgeexam` (
  `generalKnowledgeExamId` int(10) NOT NULL auto_increment,
  `teacherId` int(10) NOT NULL COMMENT 'Foreign Key to "teacherId" in table "teacherDetail"',
  `generalKnowledgeExamStatus` varchar(1) NOT NULL COMMENT 'P = "Pass" F = "Fail"',
  `generalKnowledgeExamDate` date NOT NULL,
  `generalKnowledgeScoreReport` varchar(250) NOT NULL COMMENT 'Path of the file uploaded',
  `createdDateTime` timestamp NOT NULL default CURRENT_TIMESTAMP COMMENT 'Current Date Time',
  PRIMARY KEY  (`generalKnowledgeExamId`)
) ENGINE=InnoDB

CREATE TABLE  `teachersubjectareaexam` (
`subjectAreaExamId` INT( 10 ) NOT NULL AUTO_INCREMENT ,
`teacherId` INT( 10 ) NOT NULL COMMENT  'Foreign Key to "teacherId" in table "teacherDetail"',
`examStatus` VARCHAR( 1 ) NOT NULL COMMENT  'P = "Pass" F = "Fail"',
`examDate` DATE NOT NULL ,
`subjectId` INT( 3 ) NOT NULL COMMENT  'Foreign Key to "subjectId" in table "subjectMaster"',
`scoreReport` VARCHAR( 250 ) NOT NULL COMMENT  'Path of the file uploaded',
`createdDateTime` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT  'Current Date Time',
PRIMARY KEY (  `subjectAreaExamId` )
) ENGINE = INNODB


CREATE TABLE  `teacheradditionaldocuments` (
`additionDocumentId` INT( 10 ) NOT NULL AUTO_INCREMENT ,
`teacherId` INT( 10 ) NOT NULL COMMENT  'Foreign Key to "teacherId" in table "teacherDetail"',
`documentName` VARCHAR( 250 ) NOT NULL COMMENT  'P = "Pass" F = "Fail"',
`uploadedDocument` VARCHAR( 250 ) NOT NULL COMMENT  'Path of the file uploaded',
`createdDateTime` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT  'Current Date Time',
PRIMARY KEY (  `additionDocumentId` )
) ENGINE = INNODB

ALTER TABLE  `districtportfolioconfig` ADD  `generalKnowledge` TINYINT( 1 ) NOT NULL AFTER  `formeremployee` ,
ADD  `subjectAreaExam` TINYINT( 1 ) NOT NULL AFTER  `generalKnowledge` ;
ALTER TABLE  `districtportfolioconfig` ADD  `additionalDocuments` TINYINT( 1 ) NOT NULL AFTER  `subjectAreaExam` ;
 
-------gourav 18-05-2013--------
CREATE TABLE `useruploadfolderaccessdetails` (
  `useuploadFolderId` int(5) NOT NULL auto_increment,
  `districtId` int(5) NOT NULL,
  `functionality` varchar(25) NOT NULL,
  `folderPath` varchar(500) NOT NULL,
  `nameOfLastFileUploaded` varchar(100) default NULL,
  `lastUploadDateTime` datetime NOT NULL,
  `status` varchar(1) NOT NULL default 'A' COMMENT '"A" - Active, "I" - Inactive, Default "A"',
  PRIMARY KEY  (`useuploadFolderId`),
  UNIQUE KEY `functionality` (`functionality`),
  KEY `districtId` (`districtId`)
) ENGINE=InnoDB

-------sekhar 18-04-14--------
ALTER TABLE  `teachercertificate` CHANGE  `doeNumber`  `doeNumber` VARCHAR( 25 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL;
ALTER TABLE  `teachercertificate` CHANGE  `yearExpired`  `yearExpired` INT( 4 ) NULL;



-------Ashish 19-May-14--------
ALTER TABLE  `teacherpersonalinfo` ADD  `retirementdate` DATE NULL AFTER  `dob` ,
ADD  `moneywithdrawaldate` DATE NULL AFTER  `retirementdate` ;

ALTER TABLE  `teacherpersonalinfo` DROP  `isRetiredEmployee`

--------Gourav 19-05-2014------------
ALTER TABLE  `joborder` CHANGE  `isPoolJob`  `isPoolJob` TINYINT( 1 ) NOT NULL DEFAULT  '0'

------------ vishwanath 19-05-2014 ------------
ALTER TABLE  `teacherpersonalinfo` CHANGE  `employeeNumber`  `employeeNumber` VARCHAR( 50 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT  'EmployeeCode of EmployeeMaster';

ALTER TABLE  `teacherpersonalinfo` DROP  `employeeCode`;
ALTER TABLE  `teacherpersonalinfo` CHANGE  `SSN`  `SSN` VARCHAR( 300 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL

------Sekhar 19-05-2014---------
ALTER TABLE  `districtportfolioconfig` CHANGE  `generalKnowledge`  `generalKnowledgeExam` TINYINT( 1 ) NOT NULL
------rajendra 19-05-2014---------
ALTER TABLE schoolmaster ADD FOREIGN KEY (  `geoZoneId` ) REFERENCES geozonemaster(  `geoZoneId` )
ALTER TABLE joborder ADD FOREIGN KEY ( `geoZoneId`) REFERENCES geozonemaster( `geoZoneId`)  

------------------ vishwanath 20-05-2014 ----------------
ALTER TABLE  `teacherpersonalinfo` CHANGE  `employeeType`  `employeeType` TINYINT( 1 ) NULL DEFAULT NULL COMMENT  '"2" - Not an Employee "1" - Current Employee "0" - Former Employee'

------------------ Ashish 20-05-2014 ----------------
ALTER TABLE  `districtportfolioconfig` ADD  `veteran` VARCHAR( 1 ) NOT NULL DEFAULT  '0' AFTER  `additionalDocuments` ;

-------Sekhar 20-05-2014 -------
ALTER TABLE  `employeemaster` ADD  `staffType` VARCHAR( 1 ) NULL COMMENT  'N OR I' AFTER  `employeeCode` ;

----------- vishwanath 21-05-2013 -------------
ALTER TABLE  `districtmaster` ADD  `locationCode` VARCHAR( 15 ) NULL AFTER  `districtName`;

------------------ Ashish 21-05-2014 ----------------
ALTER TABLE  `districtportfolioconfig` ADD  `ethnicorigin` TINYINT( 1 ) NULL DEFAULT  '0' AFTER  `race` ,
ADD  `ethinicity` TINYINT( 1 ) NULL DEFAULT  '0' AFTER  `ethnicorigin` ,
ADD  `employment` TINYINT( 1 ) NULL DEFAULT  '0' AFTER  `ethinicity` ;

----------- vishwanath 21-05-2013 -------------
ALTER TABLE  `districtmaster` ADD  `locationCode` VARCHAR( 15 ) NULL AFTER  `districtName`;
UPDATE `districtmaster` SET  `locationCode` =  '9999' WHERE  `districtId` =1200390;

--------sekhar 22-05-2014------------
CREATE TABLE  `subjectareaexammaster` (
`subjectAreaExamId` INT( 10 ) NOT NULL AUTO_INCREMENT ,
`districtId` INT( 5 ) NOT NULL COMMENT  'Foreign Key to "districtId" in table "districtMaster"',
`subjectAreaExamCode` VARCHAR( 10 ) NOT NULL ,
`subjectAreaExamName` VARCHAR( 200 ) NOT NULL ,
`status` VARCHAR( 1 ) NOT NULL COMMENT  '"I" - Inactive, "A" - Active By default "A"',
`createdDateTime` TIMESTAMP NOT NULL COMMENT  'Current Date Time',
PRIMARY KEY (  `subjectAreaExamId` )
) ENGINE = INNODB
------------------------
ALTER TABLE  `teachersubjectareaexam` CHANGE  `subjectAreaExamId`  `teacherSubjectAreaExamId` INT( 10 ) NOT NULL AUTO_INCREMENT;

ALTER TABLE  `teachersubjectareaexam` CHANGE  `subjectId`  `subjectAreaExamId` INT( 3 ) NOT NULL COMMENT  'Foreign Key to "subjectAreaExamId" in table "subjectareaexammaster"';

ALTER TABLE teachersubjectareaexam ADD FOREIGN KEY ( subjectAreaExamId ) REFERENCES subjectareaexammaster( subjectAreaExamId );
--------- Done By Ramesh -----------------
ALTER TABLE teachersubjectareaexam ADD FOREIGN KEY ( teacherId ) REFERENCES teacherDetail( teacherId ); -- NR
ALTER TABLE teachergeneralknowledgeexam ADD FOREIGN KEY ( teacherId ) REFERENCES teacherDetail( teacherId );-- NR

---- vishwanath 22-5-2014
ALTER TABLE  `assessmentlog` CHANGE  `section`  `section` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL
ALTER TABLE  `jobforteacher` ADD  `staffType` VARCHAR( 1 ) NULL AFTER  `isAffilated` ;

---------sekhar 24-05-2014-----------
ALTER TABLE  `jobforteacher` ADD  offerAccepted TINYINT( 1 ) NULL DEFAULT  '0'  AFTER  hiredBySchool ;

------------- vishwanath 26-05-2014

CREATE TABLE `sapcandidatedetails` (
 `sapCandidateId` int(10) NOT NULL auto_increment,
 `teacherId` int(10) NOT NULL COMMENT 'Foreign Key to "teacherId" in table "teacherDetail"',
 `jobId` int(6) NOT NULL COMMENT 'Foreign Key to "jobId" in table "jobOrder"',
 `hireDate` date NOT NULL COMMENT 'Format: yyyy-mm-dd Current Date No time',
 `employeeNumber` varchar(8) default NULL COMMENT 'Can be Blank for non-internal Employees',
 `position` int(8) NOT NULL COMMENT 'Position/Reference Code as per SAP',
 `jobTitle` varchar(200) NOT NULL COMMENT 'Not Mandatory for SAP',
 `candidateFirstName` varchar(40) NOT NULL,
 `candidateMiddleName` varchar(40) default NULL COMMENT 'Can be Blank',
 `candidateLastName` varchar(40) NOT NULL,
 `candidateSecondName` varchar(40) default NULL COMMENT 'Will be BLANK',
 `candidateSirNamePrefix` varchar(15) default NULL COMMENT 'Can be Blank',
 `candidateInitials` varchar(10) default NULL,
 `candidateSSN` varchar(20) NOT NULL COMMENT 'SSN',
 `candidateGender` varchar(1) NOT NULL COMMENT '1=male 2 =female',
 `candidateDOB` date NOT NULL COMMENT 'Format: yyyy-mm-dd No time',
 `candidateCorrespondLanguage` varchar(1) NOT NULL COMMENT 'E=English',
 `candidateStreet` varchar(60) NOT NULL COMMENT 'Address 1',
 `candidateSecondAddressLine` varchar(40) default NULL COMMENT 'Address 2',
 `candidateCity` varchar(40) NOT NULL COMMENT 'City Name',
 `candidateZip` varchar(10) NOT NULL COMMENT 'Zip Code',
 `candidateCountry` varchar(3) NOT NULL COMMENT 'Country Code from State-Country Table',
 `candidateState` varchar(3) NOT NULL COMMENT 'State Code from State-Country Table',
 `candidateTelephoneNumber` varchar(14) NOT NULL COMMENT 'Phone Number',
 `candidateTelephoneNumber2` varchar(14) default NULL COMMENT 'Mobile Number',
 `candidateEthnicOrigin` varchar(2) NOT NULL COMMENT 'ethnicOriginId from table "ethnicOriginMaster"',
 `candidateMilitaryStatus` varchar(1) default NULL COMMENT 'X if Yes Currently BLANK',
 `candidateDisability` varchar(1) default NULL COMMENT 'X if Yes Currently BLANK',
 `candidateVeteranStatus` varchar(1) default NULL COMMENT 'X if Yes, BLANK if No Veteran from teacherPersonalInfor table',
 `candidateEthnicity` varchar(2) NOT NULL COMMENT 'ethnicityCode from table "ethinicityMaster"',
 `candidateRac01` varchar(2) NOT NULL COMMENT 'raceId from teacherPersonalInfo',
 `candidateRac02` varchar(2) default NULL COMMENT 'raceId from teacherPersonalInfo',
 `candidateRac03` varchar(2) default NULL COMMENT 'raceId from teacherPersonalInfo',
 `candidateRac04` varchar(2) default NULL COMMENT 'raceId from teacherPersonalInfo',
 `candidateRac05` varchar(2) default NULL COMMENT 'raceId from teacherPersonalInfo',
 `candidateRac06` varchar(2) default NULL COMMENT 'raceId from teacherPersonalInfo',
 `candidateRac07` varchar(2) default NULL COMMENT 'raceId from teacherPersonalInfo',
 `candidateRac08` varchar(2) default NULL COMMENT 'raceId from teacherPersonalInfo',
 `candidateRac09` varchar(2) default NULL COMMENT 'raceId from teacherPersonalInfo',
 `candidateRac10` varchar(2) default NULL COMMENT 'raceId from teacherPersonalInfo',
 `sapStatus` varchar(1) default NULL COMMENT 'S = Sent to SAP Q = In Queue (Default) E = Any error occurred while sending the record to SAP',
 `sapError` varchar(250) default NULL COMMENT 'Any error occurred while sending the record to SAP',
 `noOfTries` int(4) default '0' COMMENT 'By Default "0" Scheduler will increment it by 1',
 `lastUpdateDateTime` date default NULL COMMENT 'Update this field whenever we update this record',
 `createdDateTime` date NOT NULL COMMENT 'Current Date Time',
 PRIMARY KEY  (`sapCandidateId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1


