package tm.controller.textfile;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

import tm.bean.DistrictRequisitionNumbers;
import tm.bean.JobOrder;
import tm.bean.JobRequisitionNumbers;
import tm.bean.SchoolInJobOrder;
import tm.bean.UserUploadFolderAccess;

import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.hrmspostionstext.HrmsPositionsTextDetail;
import tm.bean.master.DistrictMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.user.UserMaster;

import tm.dao.DistrictRequisitionNumbersDAO;
import tm.dao.JobOrderDAO;
import tm.dao.JobRequisitionNumbersDAO;
import tm.dao.SchoolInJobOrderDAO;
import tm.dao.UserUploadFolderAccessDAO;

import tm.dao.hrmspostionstext.HrmsPositionsTextDetailDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.JobCategoryMasterDAO;
import tm.dao.master.SchoolMasterDAO;
import tm.dao.user.UserMasterDAO;
import tm.utility.IPAddressUtility;
import tm.utility.Utility;


@Controller
public class HrmsPostionsTextController {
	private static int count=0;
	private static int downloadCount=0;


	 @Autowired
	    DistrictRequisitionNumbersDAO districtRequisitionNumbersDAO;
	    
	    @Autowired
	    SchoolMasterDAO schoolMasterDAO;
	    
	    @Autowired
		private UserMasterDAO userMasterDAO;
	    
	    public void setUserMasterDAO(UserMasterDAO userMasterDAO) {
			this.userMasterDAO = userMasterDAO;
		}
	    
	    @Autowired
	    private DistrictMasterDAO districtMasterDAO;
	    public void setDistrictMasterDAO(DistrictMasterDAO districtMasterDAO) {
			this.districtMasterDAO = districtMasterDAO;
		}
	    
	    @Autowired
	    private UserUploadFolderAccessDAO userUploadFolderAccessDAO;
	    
	    @Autowired
	    private JobCategoryMasterDAO jobCategoryMasterDAO;
	    
	    @Autowired
	    private JobRequisitionNumbersDAO jobRequisitionNumbersDAO;
	    
	    @Autowired
	    private SchoolInJobOrderDAO schoolInJobOrderDAO;
	    
	    @Autowired
	    private JobOrderDAO jobOrderDAO;
	    @Autowired
	    private HrmsPositionsTextDetailDAO hrmsPositionsTextDetailDAO;
	    
	    
	    public void setUserUploadFolderAccessDAO(UserUploadFolderAccessDAO userUploadFolderAccessDAO) 
	    {
			this.userUploadFolderAccessDAO = userUploadFolderAccessDAO;
		}


	@RequestMapping(value="/hrmsPostionsFile.do", method=RequestMethod.GET)
	public @ResponseBody String getHrmsPostions(ModelMap map,HttpServletRequest request)
	{
		System.out.println("count start==="+count);
		if(count==0)
		{
			count=1;
			 System.out.print("::::: Use HRMS Postions Text Controller :::::");
			    List<UserUploadFolderAccess> uploadFolderAccessesrec = null;
				try {
					Criterion functionality = Restrictions.eq("functionality", "applicantTxtUpload");
					Criterion active = Restrictions.eq("status", "A");
					uploadFolderAccessesrec = userUploadFolderAccessDAO.findByCriteria(functionality,active);
				} catch (Exception e) {
					e.printStackTrace();
				}	
			    String fileName = "HRMS-CCYYMMDDHHMMSS-00-2260-POS--PositionData.txt";
			    String folderPath = uploadFolderAccessesrec.get(0).getFolderPath();			    
				String msge=saveLicense(fileName,folderPath);
			  	count=0;
			  	System.out.println("count end==="+count);
			  	String fullURL=request.getRequestURL().toString();
			  	String mainURL=fullURL.replace("hrmsPostionsFile.do", "autoattachJSI.do");			  
			  	String JSI="<br><a href='"+mainURL+"'>Click here</a>  to auto attach JSI with Jobs(It's mandatory).";
			  	return msge+JSI;
		}
		return "Other process is running";
		  
		  		
		
		
	}
	
	
	//private Vector vectorDataTxt ;
	@Transactional(readOnly=false)
    public String saveLicense(String fileName,String folderPath){	
			Vector	vectorDataTxt = new Vector();
			 System.out.println("::::::::saveLicense:::::::::");
			 if(vectorDataTxt !=null)
				 System.out.println(vectorDataTxt.size());
			 int nn=0;
			 int nnn=0;
			 
			 String filePath=folderPath+fileName;
			 Set<String> set=new HashSet<String>();
		 	 System.out.println("folderPath  "+folderPath);
		 	 try {
		 		 if(fileName.contains(".txt") || fileName.contains(".TXT")){
					vectorDataTxt = readDataTxt(filePath,set);
		 			   System.out.println(set.size());
		 		 }else{
	    			 //break;
	    	 }
		 		 
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		int n=0;
    	 
		SessionFactory sessionFactory=districtRequisitionNumbersDAO.getSessionFactory();
		StatelessSession statelesSsession = sessionFactory.openStatelessSession();
 	    Transaction txOpen =statelesSsession.beginTransaction();
 	    int insertData=0;
 	    int updateData=0;
 	    
    	 int LEA_Number_count=11;	    	 
    	 int POS_NUM_CD_count=11;
    	 int Site_Name_count=11;
    	 int POS_TITLE_CD_count=11; 
    	 int POS_DESC_count=11;
    	 int POS_CATEGORY_CD_count=11;
    	 int POS_FTORPT_CD_count=11;
    	 int POS_CLASS_CD_count=11;
    	 int POS_START_DTE_count=11;
    	 int POS_END_DATE_count=11;
    	 int POS_TERM_NUM_count=11;
    	 int POS_PAYMTHD_CD_count=11;
    	 int POS_STATUS_CD_count=11;
    	 int POS_CDLREQD_IND_count=11;
    	 int POS_FUNDED_PCT_count=11;
    	 int POS_HOURS_NUM_count=11;
    	 int POS_STDHRS_NUM_count=11;
    	 int UPDATE_TS_count=11;
    	 String final_error_store="";
    	 int numOfError=0;
    	 boolean row_error=false;
    	
    	 HeadQuarterMaster headQuarterMaster = new HeadQuarterMaster();
    	 headQuarterMaster.setHeadQuarterId(2);
    	 
    	 //List<DistrictMaster> districtMasterList =  districtMasterDAO.getDistrictListByHeadQuater(headQuarterMaster);
		 Criterion criterion = Restrictions.eq("headQuarterMaster",headQuarterMaster);
    	 Criterion criterion1 = Restrictions.in("locationCode",set);
    	 List<DistrictMaster> districtMasterList =districtMasterDAO.findByCriteria(criterion,criterion1);
    	 Map<String, DistrictMaster> districtMasterMap = new HashMap<String, DistrictMaster>();
    	 if(districtMasterList!=null && districtMasterList.size()>0)
    	 for (DistrictMaster districtMaster2 : districtMasterList) {
    		 districtMasterMap.put(districtMaster2.getLocationCode(), districtMaster2);
		}
    	 
    	 List<DistrictRequisitionNumbers> hrmsSites =  null;
    	 try {
    		// getRequisitionNumbersByDistricts
			//hrmsSites = districtRequisitionNumbersDAO.findAll();
    		hrmsSites = districtRequisitionNumbersDAO.getRequisitionNumbersByDistricts(districtMasterList);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
    	 Map<String, DistrictRequisitionNumbers> hrmsSitesList = new HashMap<String, DistrictRequisitionNumbers>();
    	 if(hrmsSites!=null && hrmsSites.size()>0)
    	 for (DistrictRequisitionNumbers hrmsSitesList2 : hrmsSites) {
    		 //hrmsSitesList.put(hrmsSitesList2.getRequisitionNumber(), hrmsSitesList2);
    		 DistrictMaster dm = hrmsSitesList2.getDistrictMaster();
    		 if(dm.getLocationCode()!=null)
    			 hrmsSitesList.put(dm.getLocationCode()+"###"+hrmsSitesList2.getRequisitionNumber(), hrmsSitesList2);
    		 
    		 //no need to process on null loaction code
    		 //else
    			// hrmsSitesList.put(hrmsSitesList2.getRequisitionNumber(), hrmsSitesList2);
		}
    	
    	 List<SchoolMaster> schoolMasterList =  schoolMasterDAO.findSchoolsByDistricts(districtMasterList);
    	// List<SchoolMaster> schoolMasterList =  schoolMasterDAO.findAll();
    	 Map<String, SchoolMaster> schoolMasterMap = new HashMap<String, SchoolMaster>();
    	 if(schoolMasterList!=null && schoolMasterList.size()>0)
    	 for (SchoolMaster schoolMaster2 : schoolMasterList) {
    		// schoolMasterMap.put(schoolMaster2.getLocationCode(), schoolMaster2);
    		 schoolMasterMap.put(schoolMaster2.getLocationCode()+"_"+schoolMaster2.getDistrictId().getLocationCode(), schoolMaster2);
		 } 
    	 List<JobCategoryMaster> jobCategoryMasterList =  jobCategoryMasterDAO.findJobCategorysByDistricts(districtMasterList);
     	 Map<String, JobCategoryMaster> jobCategoryMasterMap = new HashMap<String, JobCategoryMaster>();
     	 if(jobCategoryMasterList!=null && jobCategoryMasterList.size()>0)
     	 for (JobCategoryMaster jobCategoryMaster : jobCategoryMasterList) {
     		DistrictMaster dm = jobCategoryMaster.getDistrictMaster();
     		if(dm.getLocationCode()!=null)
     		
     			jobCategoryMasterMap.put(dm.getLocationCode().trim()+"###"+jobCategoryMaster.getJobCategoryName().trim(), jobCategoryMaster);
 		 } 
     	 
     	List<JobRequisitionNumbers> jobRequisitionNumbersList = new ArrayList<JobRequisitionNumbers>();
     	//Map<Integer, JobRequisitionNumbers> jobRequisitionNumbersMap = new HashMap<Integer, JobRequisitionNumbers>();
     	Map<String, JobRequisitionNumbers> jobRequisitionNumbersJobMap = new HashMap<String, JobRequisitionNumbers>();
     	
     	if(hrmsSites.size()!=0 && hrmsSites!=null){
	     	 jobRequisitionNumbersList =  jobRequisitionNumbersDAO.findJobRequsition(hrmsSites);
	   	     //jobRequisitionNumbersMap = new HashMap<Integer, JobRequisitionNumbers>();
	   	    if(jobRequisitionNumbersList!=null && jobRequisitionNumbersList.size()>0)
	   	    for (JobRequisitionNumbers jobRequisitionNumbers2 : jobRequisitionNumbersList) {
	   	    	if(jobRequisitionNumbers2.getJobOrder() !=null)
	   	    	{
	   	    		//jobRequisitionNumbersMap.put(jobRequisitionNumbers2.getJobOrder().getJobId(), jobRequisitionNumbers2);
	   	    		jobRequisitionNumbersJobMap.put(jobRequisitionNumbers2.getDistrictRequisitionNumbers().getDistrictMaster().getLocationCode().trim()
	   	    				+"##"+jobRequisitionNumbers2.getDistrictRequisitionNumbers().getRequisitionNumber().trim(),
	   	    				jobRequisitionNumbers2);
	   	    		
	   	    	}
	   	    	
			}
     	}
   	    
     	//no need to get Archieve as they are not updated
   	    //List<JobOrder> jobOrderList =  jobOrderDAO.findJobByDistricts(districtMasterList);
     	//List<JobOrder> jobOrderList =  jobOrderDAO.findNoArchieveJobByDistricts(districtMasterList);
	   // Map<String, JobOrder> jobOrderMap = new HashMap<String, JobOrder>();
	    /*if(jobOrderList!=null && jobOrderList.size()>0)
	    for (JobOrder jobOrder2 : jobOrderList) {
	    	jobOrderMap.put(jobOrder2.getApiJobId()+"_"+jobOrder2.getDistrictMaster().getDistrictId(), jobOrder2);
		}
     	 */
     	 
   	    List<SchoolInJobOrder> schoolInJobOrderList =  schoolInJobOrderDAO.findSchoolJobByDistricts(districtMasterList);
	    Map<Integer, SchoolInJobOrder> schoolInJobOrderMap = new HashMap<Integer, SchoolInJobOrder>();
	    if(schoolInJobOrderList!=null && schoolInJobOrderList.size()>0)
	    for (SchoolInJobOrder schoolInJobOrder2 : schoolInJobOrderList) {
	    	//schoolInJobOrderMap should be unique confusion
	    	if(schoolInJobOrder2.getJobId() !=null)
	    		schoolInJobOrderMap.put(schoolInJobOrder2.getJobId().getJobId(), schoolInJobOrder2);
		}
     	 
     	 
     	 
    	 UserMaster userMaster = userMasterDAO.findById(1, false, false);
    	 txOpen =statelesSsession.beginTransaction();
    	 int x=0;
    	  for(int i=0; i<vectorDataTxt.size(); i++) {
			 Vector vectorCellEachRowData = (Vector) vectorDataTxt.get(i);
			 x++;
			 
			 
	            String LEANumber="";
	            String PositionNumber="";
	            String SiteNumber="";
	            String PositionTitleCode =""; 
	            String PositionDescription="";
	            String PositionTypeCode="";
	            String fptimeCode="";
	            String ContinuingorTemporaryCode="";
	            String startDate =""; 
	            String endDate="";
	            String PostionTerm="";
	            String payMethod="";
	            String postionStatus="";
	            String commdriverLicences="";
	            String postionPercentFunded =""; 
	            String postionhoursweek="";
	            String standardhoursweek="";
	            String  updatetimestamp="";
	            String errorText="";
		  	    String rowErrorText="";
	  	     
	  	      
	  	      
	  	    for(int j=0; j<vectorCellEachRowData.size(); j++) {
  	        	try{
  	        		if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("POS_LEA_NUM")){
  	        			LEA_Number_count=j;
	            	}
	            	if(LEA_Number_count==j)
	            		LEANumber=vectorCellEachRowData.get(j).toString().trim();
  	        		
	            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("POS_NUM_CD")){
	            		POS_NUM_CD_count=j;
	            	}
	            	if(POS_NUM_CD_count==j)
	            		PositionNumber=vectorCellEachRowData.get(j).toString().trim();
	            	
	            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("POS_SITE_CD")){
	            		Site_Name_count=j;
					}
	            	if(Site_Name_count==j)
	            		SiteNumber=vectorCellEachRowData.get(j).toString().trim();
	            	
	            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("PCHD_DESC")){
	            		POS_TITLE_CD_count=j;
					}
	            	if(POS_TITLE_CD_count==j)
	            		PositionTitleCode=vectorCellEachRowData.get(j).toString().trim();
	            	
	            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("POS_DESC")){
	            		POS_DESC_count=j;
					}
	            	if(POS_DESC_count==j)
	            		PositionDescription=vectorCellEachRowData.get(j).toString().trim();
	            	
	            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("POS_CATEGORY_CD")){
	            		POS_CATEGORY_CD_count=j;
					}
	            	if(POS_CATEGORY_CD_count==j)
	            		PositionTypeCode=vectorCellEachRowData.get(j).toString().trim();
	            	
	            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("POS_FTORPT_CD")){
	            		POS_FTORPT_CD_count=j;
					}
	            	if(POS_FTORPT_CD_count==j)
	            		fptimeCode=vectorCellEachRowData.get(j).toString().trim();
	            	
	            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("POS_CLASS_CD")){
	            		POS_CLASS_CD_count=j;
					}
	            	if(POS_CLASS_CD_count==j)
	            		ContinuingorTemporaryCode=vectorCellEachRowData.get(j).toString().trim();
	            	
	            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("POS_START_DTE")){
	            		POS_START_DTE_count=j;
					}
	            	if(POS_START_DTE_count==j)
	            		startDate=vectorCellEachRowData.get(j).toString().trim();
	            	
	            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("POS_END_DATE")){
	            		POS_END_DATE_count=j;
					}
	            	if(POS_END_DATE_count==j)
	            		endDate=vectorCellEachRowData.get(j).toString().trim();
	            	
	            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("POS_TERM_NUM")){
	            		POS_TERM_NUM_count=j;
					}
	            	if(POS_TERM_NUM_count==j)
	            		PostionTerm=vectorCellEachRowData.get(j).toString().trim();
	            	
	            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("POS_PAYMTHD_CD")){
	            		POS_PAYMTHD_CD_count=j;
					}
	            	if(POS_PAYMTHD_CD_count==j)
	            		payMethod=vectorCellEachRowData.get(j).toString().trim();
	            	
	            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("POS_STATUS_CD")){
	            		POS_STATUS_CD_count=j;
					}
	            	if(POS_STATUS_CD_count==j)
	            		postionStatus=vectorCellEachRowData.get(j).toString().trim();
	            	
	            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("POS_CDLREQD_IND")){
	            		POS_CDLREQD_IND_count=j;
					}
	            	if(POS_CDLREQD_IND_count==j)
	            		commdriverLicences=vectorCellEachRowData.get(j).toString().trim();
	            	
	            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("POS_FUNDED_PCT")){
	            		POS_FUNDED_PCT_count=j;
					}
	            	if(POS_FUNDED_PCT_count==j)
	            		postionPercentFunded=vectorCellEachRowData.get(j).toString().trim();
	            	
	            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("POS_HOURS_NUM")){
	            		POS_HOURS_NUM_count=j;
					}
	            	if(POS_HOURS_NUM_count==j)
	            		postionhoursweek=vectorCellEachRowData.get(j).toString().trim();
	            	
	            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("POS_STDHRS_NUM")){
	            		POS_STDHRS_NUM_count=j;
					}
	            	if(POS_STDHRS_NUM_count==j)
	            		standardhoursweek=vectorCellEachRowData.get(j).toString().trim();
	            	
	            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("UPDATE_TS")){
	            		UPDATE_TS_count=j;
					}
	            	if(UPDATE_TS_count==j)
	            		updatetimestamp=vectorCellEachRowData.get(j).toString().trim();
	            	
            	}catch(Exception e){
            		e.printStackTrace();
            	}
  	        }
	  	    
	  	  if(i==0){
	  		  	  		  
	  		  if(!PositionNumber.equalsIgnoreCase("POS_NUM_CD")){
	  			rowErrorText="POS_NUM_CD column is not found";
	  			row_error=true;  
		  	  }
	  		  
	  		  if(!LEANumber.equalsIgnoreCase("POS_LEA_NUM")){
	  			if(row_error){
	  				rowErrorText+=",";
	  			  }
	  			rowErrorText+="POS_LEA_NUM column is not found";
	  			row_error=true;
	  		  }
	  		 
	  		
	  		if(!SiteNumber.equalsIgnoreCase("POS_SITE_CD")){
	  			if(row_error){
	  				rowErrorText+=",";
	  			  }
	  			rowErrorText+="POS_SITE_CD column is not found";
	  			row_error=true;  
	  		  }
	  		 
	  	  
		  	 if(!PositionTitleCode.equalsIgnoreCase("PCHD_DESC")){
	  			if(row_error){
	  				rowErrorText+=",";
	  			  }
	  			rowErrorText+="PCHD_DESC column is not found";
	  			row_error=true;  
	  		  }
		  	 
		  	 if(!PositionDescription.equalsIgnoreCase("POS_DESC")){
		  			if(row_error){
		  				rowErrorText+=",";
		  			  }
		  			rowErrorText+="POS_DESC column is not found";
		  			row_error=true;  
		  		  }
		  	 
		  	 if(!PositionTypeCode.equalsIgnoreCase("POS_CATEGORY_CD")){
		  			if(row_error){
		  				rowErrorText+=",";
		  			  }
		  			rowErrorText+="POS_CATEGORY_CD column is not found";
		  			row_error=true;  
		  		  }
		  	 
		  	 if(!fptimeCode.equalsIgnoreCase("POS_FTORPT_CD")){
		  			if(row_error){
		  				rowErrorText+=",";
		  			  }
		  			rowErrorText+="POS_FTORPT_CD column is not found";
		  			row_error=true;  
		  		  }
		  	 
		  	 if(!ContinuingorTemporaryCode.equalsIgnoreCase("POS_CLASS_CD")){
		  			if(row_error){
		  				rowErrorText+=",";
		  			  }
		  			rowErrorText+="POS_CLASS_CD column is not found";
		  			row_error=true;  
		  		  }
		  	 
		  	 if(!startDate.equalsIgnoreCase("POS_START_DTE")){
		  			if(row_error){
		  				rowErrorText+=",";
		  			  }
		  			rowErrorText+="POS_START_DTE column is not found";
		  			row_error=true;  
		  		  }
		  	 if(!endDate.equalsIgnoreCase("POS_END_DATE")){
		  			if(row_error){
		  				rowErrorText+=",";
		  			  }
		  			rowErrorText+="POS_END_DATE column is not found";
		  			row_error=true;  
		  		  }
		  	if(!PostionTerm.equalsIgnoreCase("POS_TERM_NUM")){
	  			if(row_error){
	  				rowErrorText+=",";
	  			  }
	  			rowErrorText+="POS_TERM_NUM column is not found";
	  			row_error=true;  
	  		  }
		  	if(!payMethod.equalsIgnoreCase("POS_PAYMTHD_CD")){
	  			if(row_error){
	  				rowErrorText+=",";
	  			  }
	  			rowErrorText+="POS_PAYMTHD_CD column is not found";
	  			row_error=true;  
	  		  }
		  	if(!postionStatus.equalsIgnoreCase("POS_STATUS_CD")){
	  			if(row_error){
	  				rowErrorText+=",";
	  			  }
	  			rowErrorText+="POS_STATUS_CD column is not found";
	  			row_error=true;  
	  		  }
		  	if(!commdriverLicences.equalsIgnoreCase("POS_CDLREQD_IND")){
	  			if(row_error){
	  				rowErrorText+=",";
	  			  }
	  			rowErrorText+="POS_CDLREQD_IND column is not found";
	  			row_error=true;  
	  		  }
		  	if(!postionPercentFunded.equalsIgnoreCase("POS_FUNDED_PCT")){
	  			if(row_error){
	  				rowErrorText+=",";
	  			  }
	  			rowErrorText+="POS_FUNDED_PCT column is not found";
	  			row_error=true;  
	  		  }
		  	if(!postionhoursweek.equalsIgnoreCase("POS_HOURS_NUM")){
	  			if(row_error){
	  				rowErrorText+=",";
	  			  }
	  			rowErrorText+="POS_HOURS_NUM column is not found";
	  			row_error=true;  
	  		  }
		  	 if(!standardhoursweek.equalsIgnoreCase("POS_STDHRS_NUM")){
		  			if(row_error){
		  				rowErrorText+=",";
		  			  }
		  			rowErrorText+="POS_STDHRS_NUM column is not found";
		  			row_error=true;
		  		  }
		  	 
	/*		 if(!updatetimestamp.equalsIgnoreCase("UPDATE_TS")){
		  			if(row_error){
		  				rowErrorText+=",";
		  			  }
		  			rowErrorText+="UPDATE_TS column is not found";
		  			row_error=true;
		  		  }
	*/	  	
		  	 
	  		 }
	  	    
	  	  
		  	if(row_error){
		  		numOfError++;
		  		File file = new File(folderPath+"hrmsPostionsError.txt");
				// if file doesnt exists, then create it
		  		try {
			  		if (!file.exists()) {
			  			file.createNewFile();
					}	    				
					FileWriter fw = new FileWriter(file.getAbsoluteFile());
					BufferedWriter bw = new BufferedWriter(fw);
					bw.write(rowErrorText);
		
					bw.close();
		  		} catch (IOException e) {
					
					e.printStackTrace();
				}
				
		  		 break;
		  	 }
	  	    
	  	    Boolean LEANumberFlag = true;
	  	    Boolean SiteNumberFlag = true;
	  	    Boolean PositionTitleCodeFlag = true;
	  	    Boolean PositionDescriptionFlag = true;
	  	    Boolean PositionTypeCodeFlag = true;
	  	    Boolean fptimeCodeFlag = true;
	  	    Boolean ContinuingorTemporaryCodeFlag = true;
	  	    Boolean startDateFlag = true;
	  	    Boolean endDateFlag = true;
	  	    Boolean PostionTermFlag = true;
	  	    Boolean payMethodFlag = true;
	  	    Boolean postionStatusFlag = true;
	  	    Boolean commdriverLicencesFlag = true;
	  	    Boolean postionPercentFundedFlag = true;
	  	    Boolean postionhoursweekFlag = true;
	  	    Boolean standardhoursweekFlag = true;
	  	  Boolean updatetimestampFlag = true;
	  	   
	  	    
	  	    if(i != 0 && row_error==false){
	  	    	
	  	    	boolean errorFlag=false;
	  	    	
	  	    	if(!PositionNumber.replaceAll("\"", "").equalsIgnoreCase("")){
	  	    		
	  	    		if(PositionNumber.replaceAll("\"", "").length()>6){
		  	    			errorText+="Position Number length does not greater than 6";
		  	    		    errorFlag=true;
	  	    		}	    
	  	    		if(errorFlag){
							errorText+=",";	
					}
	  	    		
	  	    	}else{
	        			if(errorFlag){
							errorText+=",";	
						}
	        			errorText+="Position Number is empty";
  	    			    errorFlag=true;
	        	}
	  	    	
                  if(!LEANumber.replaceAll("\"", "").equalsIgnoreCase("")){
	  	    		
	  	    		if(LEANumber.replaceAll("\"", "").length()>10){
	  	    			errorText+="LEA Number length does not greater than 10";
	  	    		    LEANumberFlag=false;
	  	    		    errorFlag=true;
	  	    		}    
	  	    		if(LEANumberFlag){
							errorText+=",";	
					}
	  	    		
	  	    	}else{
	        			if(errorFlag){
							errorText+=",";	
						}
	        			errorText+="Position Number is empty";
	        			LEANumberFlag=false;
	        			errorFlag=true;
	        	}
	  	    	
	  	    	
               if(!SiteNumber.replaceAll("\"", "").equalsIgnoreCase("")){
	  	    		
	  	    		if(SiteNumber.replaceAll("\"", "").length()>4){
	  	    			errorText+="Site  Number length does not greater than 4";
	  	    		    SiteNumberFlag=false;
	  	    		    errorFlag=true;
	  	    		}    
	  	    		if(SiteNumberFlag){
							errorText+=",";	
					}
	  	    		
	  	    	}else{
	        			if(errorFlag){
							errorText+=",";	
						}
	        			errorText+="Site Number is empty";
	        			SiteNumberFlag=false;
	        			errorFlag=true;
	        	}
               
               if(!PositionTitleCode.replaceAll("\"", "").equalsIgnoreCase("")){
	  	    		
	  	    		if(PositionTitleCode.replaceAll("\"", "").length()>40){
	  	    			errorText+="Postion Title does not greater than 40 ";
	  	    		    PositionTitleCodeFlag=false;
	  	    		    errorFlag=true;
	  	    		}    
	  	    		if(PositionTitleCodeFlag){
							errorText+=",";	
					}
	  	    		
	  	    	}else{
	        			if(errorFlag){
							errorText+=",";	
						}
	        			errorText+="Position Title  is empty";
	        			PositionTitleCodeFlag=false;
	        			errorFlag=true;
	        	}
               
               if(!PositionDescription.replaceAll("\"", "").equalsIgnoreCase("")){
	  	    		
	  	    		if(PositionDescription.replaceAll("\"", "").length()>250){
	  	    			errorText+="Position Description does not greater than 250";
	  	    		    PositionDescriptionFlag=false;
	  	    		    errorFlag = true;
	  	    		}   
	  	    		if(errorFlag){
							errorText+=",";	
					}
	  	    		
	  	    	}else{
	        			if(PositionDescriptionFlag){
							errorText+=",";	
						}
	        			errorText+="Position Description is empty";
	        			PositionDescriptionFlag=false;
	        			errorFlag=true;
	        	}
               
               if(!PositionTypeCode.replaceAll("\"", "").equalsIgnoreCase("")){
	  	    		
	  	    		if(PositionTypeCode.replaceAll("\"", "").length()>1){
	  	    			errorText+="Position Type Code does not contain greater than 1";
	  	    		    PositionTypeCodeFlag=false;
	  	    		    errorFlag = true;
	  	    		}    
	  	    		if(errorFlag){
							errorText+=",";	
					}
	  	    		
	  	    	}else{
	        			if(errorFlag){
							errorText+=",";	
						}
	        			errorText+="Position Type Code is empty";
	        			PositionTypeCodeFlag=false;
	        			errorFlag=true;
	        	}
               
               if(!fptimeCode.replaceAll("\"", "").equalsIgnoreCase("")){
	  	    		
	  	    		if(fptimeCode.replaceAll("\"", "").length()>1){
	  	    			errorText+="Full-Time/Part-Time Code does not contain greater than 1";
	  	    		    fptimeCodeFlag=false;
	  	    		    errorFlag = true;
	  	    		}   
	  	    		if(errorFlag){
							errorText+=",";	
					}
	  	    		
	  	    	}else{
	        			if(errorFlag){
							errorText+=",";	
						}
	        			errorText+="Full-Time/Part-Time Code is empty";
	        			fptimeCodeFlag=false;
	        			errorFlag=true;
	        	}
               
               if(!ContinuingorTemporaryCode.replaceAll("\"", "").equalsIgnoreCase("")){
	  	    		
	  	    		if(ContinuingorTemporaryCode.replaceAll("\"", "").length()>1){
	  	    			errorText+="Continuing or Temporary Code does not contain greater than 1";
	  	    		    ContinuingorTemporaryCodeFlag=false;
	  	    		    errorFlag = true;
	  	    		}    
	  	    		if(errorFlag){
							errorText+=",";	
					}
	  	    		
	  	    	}else{
	        			if(errorFlag){
							errorText+=",";	
						}
	        			errorText+="Continuing or Temporary Code is empty";
	        			ContinuingorTemporaryCodeFlag=true;
	        			errorFlag=true;
	        	}
               
                             
               
               if(!startDate.replaceAll("\"", "").equalsIgnoreCase("")){
	  	    		
	  	    		if (!startDate.replaceAll("\"", "").matches("([0-9]{2})/([0-9]{2})/([0-9]{4})")){
	  	    			startDateFlag=false;
	  	    			errorText+="Start Date should be 'MM/dd/yyyy' format";
	  	    			errorFlag=true;
	  	    		}	
	  	    		if(errorFlag){
							errorText+=",";	
					}
	  	    		
	  	    	}else{
	  	    		    if(errorFlag){
							errorText+=",";	
						}
	        			errorText+="Start Date is empty";
 	    			    errorFlag=true;
 	    			    startDateFlag=false;
	        	}
	  	    	
	  	    	 if(!endDate.replaceAll("\"", "").equalsIgnoreCase("")){
		  	    		
	                	if (!endDate.replaceAll("\"", "").matches("([0-9]{2})/([0-9]{2})/([0-9]{4})")){
	                		endDateFlag=false;
		  	    			errorText+="End Date should be 'MM/dd/yyyy' format";
		  	    			errorFlag=true;
		  	    		}	
		  	    		if(errorFlag){
								errorText+=",";	
						}
		  	    		
		  	    	}else{
	                	    if(errorFlag){
								errorText+=",";	
							}
		        			errorText+="End Date is empty";
	  	    			    errorFlag=true;
	  	    			    endDateFlag=false;
		         }
               
               
               
               if(!PostionTerm.replaceAll("\"", "").equalsIgnoreCase("")){
	  	    		
	  	    		if(PostionTerm.replaceAll("\"", "").length()>9){
	  	    			errorText+="Position Term contain error";
	  	    		    PostionTermFlag=true;
	  	    		    errorFlag=true;
	  	    		}    
	  	    		if(errorFlag){
							errorText+=",";	
					}
	  	    		
	  	    	}else{
	        			if(errorFlag){
							errorText+=",";	
						}
	        			errorText+="Position term is empty";
	        			PostionTermFlag=false;
	        			errorFlag=true;
	        	}
               
               if(!payMethod.replaceAll("\"", "").equalsIgnoreCase("")){
	  	    		
	  	    		if(payMethod.replaceAll("\"", "").length()>1){
	  	    			errorText+="Pay Method does not contain greater than 1";
	  	    		    payMethodFlag=false;
	  	    		  errorFlag = true;
	  	    		}    
	  	    		if(errorFlag){
							errorText+=",";	
					}
	  	    		
	  	    	}else{
	        			if(errorFlag){
							errorText+=",";	
						}
	        			errorText+="Pay Method is empty";
	        			payMethodFlag=false;
	        			errorFlag=true;
	        	}
               if(!postionStatus.replaceAll("\"", "").equalsIgnoreCase("")){
	  	    		
	  	    		if(postionStatus.replaceAll("\"", "").length()>2){
	  	    			errorText+="Position Status does not contain greater than 2";
	  	    		    postionStatusFlag=false;
	  	    		  errorFlag = true;
	  	    		}   
	  	    		if(errorFlag){
							errorText+=",";	
					}
	  	    		
	  	    	}else{
	        			if(errorFlag){
							errorText+=",";	
						}
	        			errorText+="Position Status is empty";
	        			postionStatusFlag=false;
	        			errorFlag=true;
	        	}
               if(!commdriverLicences.replaceAll("\"", "").equalsIgnoreCase("")){
	  	    		
	  	    		if(commdriverLicences.replaceAll("\"", "").length()>1){
	  	    			errorText+="Commercial Driver�s License Required Indicator does not contain greater than 1";
	  	    		    commdriverLicencesFlag=false;
	  	    		  errorFlag  = true;
	  	    		}    
	  	    		if(errorFlag){
							errorText+=",";	
					}
	  	    		
	  	    	}else{
	        			if(errorFlag){
							errorText+=",";	
						}
	        			errorText+="Commercial Driver�s License Required Indicator is empty";
	        			commdriverLicencesFlag=false;
	        			errorFlag=true;
	        	}
               if(!postionPercentFunded.replaceAll("\"", "").equalsIgnoreCase("")){
	  	    		
	  	    		if(postionPercentFunded.replaceAll("\"", "").length()>9){
	  	    			errorText+="Position Percent Funded contain error";
	  	    		    postionPercentFundedFlag=false;
	  	    		  errorFlag  = false;
	  	    		}    
	  	    		if(errorFlag){
							errorText+=",";	
					}
	  	    		
	  	    	}else{
	        			if(errorFlag){
							errorText+=",";	
						}
	        			errorText+="Position Percent Funded is empty";
	        			postionPercentFundedFlag=false;
	        			errorFlag=true;
	        	}
               if(!postionhoursweek.replaceAll("\"", "").equalsIgnoreCase("")){
	  	    		
	  	    		if(postionhoursweek.replaceAll("\"", "").length()>9){
	  	    			errorText+="Position Hours/Week Funded contain error";
	  	    		    postionhoursweekFlag=false;
	  	    		  errorFlag = true;
	  	    		}    
	  	    		if(errorFlag){
							errorText+=",";	
					}
	  	    		
	  	    	}else{
	        			if(errorFlag){
							errorText+=",";	
						}
	        			errorText+="Position Hours/Week Funded is empty";
	        			postionhoursweekFlag=false;
	        			errorFlag=true;
	        	}
               if(!standardhoursweek.replaceAll("\"", "").equalsIgnoreCase("")){
	  	    		
	  	    		if(standardhoursweek.replaceAll("\"", "").length()>9){
	  	    			errorText+="Standard Hours/Week contain error";
	  	    		    standardhoursweekFlag=false;
	  	    		  errorFlag = true;
	  	    		}    
	  	    		if(errorFlag){
							errorText+=",";	
					}
	  	    		
	  	    	}else{
	        			if(errorFlag){
							errorText+=",";	
						}
	        			errorText+="Standard Hours/Week is empty";
	        			standardhoursweekFlag=false;
	        			errorFlag=true;
	        	}
               
               if(!updatetimestamp.replaceAll("\"", "").equalsIgnoreCase("")){
	  	    		
	  	    		if(updatetimestamp.replaceAll("\"", "").length()>40){
	  	    		//	errorText+="Update Time Stamp contain error";
	  	    		    updatetimestampFlag=false;
	  	    		    errorFlag  = true;
	  	    		}    
	  	    		if(errorFlag){
							errorText+=",";	
					}
	  	    		
	  	    	}else{
	        			if(errorFlag){
							errorText+=",";	 
						}
	        		//	errorText+="Update Time Stamp is empty";
	        			updatetimestampFlag=false;
	        			errorFlag=true;
	        	}
                 
               /* if(!errorText.equals("")){
	        			int row = i+1;
  	    			final_error_store+="Row "+row+" : "+errorText+"\r\n"+LEANumber+","+PositionNumber+","+SiteNumber+","+PositionTitleCode+","+PositionDescription+","+PositionTypeCode+","+fptimeCode+","+ContinuingorTemporaryCode+","+startDate+","+endDate+","+PostionTerm+","+payMethod+","+postionStatus+","+commdriverLicences+","+postionPercentFunded+","+postionhoursweek+","+standardhoursweek+","+updatetimestamp+"<>";
  	    			numOfError++;
  	    		}*/
	  	    	
                try {
                	
                	SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
                	
                	
                	DistrictRequisitionNumbers districtRequisitionNumbers = null;
                	DistrictMaster districtMaster=null;
               	    SchoolMaster schoolMaster=null;
                	
                	if(LEANumber.replaceAll("\"", "").isEmpty()){
                		
                		continue;
                	}
                	
                	if(PositionNumber.replaceAll("\"", "").isEmpty()){
                		continue;
                	}
                	
                	if(hrmsSitesList.containsKey(LEANumber.replaceAll("\"", "")+"###"+(PositionNumber.replaceAll("\"", "")))){
                		districtRequisitionNumbers = hrmsSitesList.get(LEANumber.replaceAll("\"", "")+"###"+(PositionNumber.replaceAll("\"", "")));
                	}
                	
                	
                	if(districtMasterMap.containsKey(LEANumber.replaceAll("\"", ""))){
                		districtMaster = districtMasterMap.get(LEANumber.replaceAll("\"", ""));
                	}
                	
                	if(districtMaster == null){
                	    System.out.println("districtMaster null ####"+LEANumber);
                		continue;
                	}
                	 
                	
                	if(schoolMasterMap.containsKey(SiteNumber.replaceAll("\"", "")+"_"+districtMaster.getLocationCode())){
                		schoolMaster = schoolMasterMap.get(SiteNumber.replaceAll("\"", "")+"_"+districtMaster.getLocationCode());
                		if(schoolMaster == null){
                			System.out.println("schoolmaster null ###########"+SiteNumber.replaceAll("\"", "")+"_"+districtMaster.getLocationCode());
                		}
                	}
                	
                	  
                     if(districtRequisitionNumbers == null)
                     {
                    	 //List<DistrictRequisitionNumbers> lstDistrictRequisitionNumbers= districtRequisitionNumbersDAO.getRequisitionNumbersBySearch(PositionNumber.replaceAll("\"", ""), districtMaster);
                    	 //districtRequisitionNumbers= hrmsSitesList.get(districtMaster.getLocationCode()+"###"+PositionNumber.replaceAll("\"", ""));
                    	 //if(districtRequisitionNumbers!=null)
	                    {
	                    	 districtRequisitionNumbers = new DistrictRequisitionNumbers();
	                    	 districtRequisitionNumbers.setRequisitionNumber(PositionNumber.replaceAll("\"", ""));
	                    	 districtRequisitionNumbers.setDistrictMaster(districtMaster);
	                    	 districtRequisitionNumbers.setSchoolMaster(schoolMaster);
	                    	
	                    	 if(PositionTitleCodeFlag)
	                    	 districtRequisitionNumbers.setJobTitle(PositionTitleCode.replaceAll("\"", ""));
	                    	 
	                    	 if(PositionDescriptionFlag)
	                    	  districtRequisitionNumbers.setRequisitionDescription(PositionDescription.replaceAll("\"", ""));
	                    	
	                    	 if(PositionTypeCodeFlag)
	                    	 districtRequisitionNumbers.setRequisitionTypeCode(PositionTypeCode.replaceAll("\"", ""));
	                    	
	                    	 if(fptimeCodeFlag)
	                    	  districtRequisitionNumbers.setPosType(fptimeCode.replaceAll("\"", ""));
	                    	 
	                    	 if(ContinuingorTemporaryCodeFlag)
	                    	 districtRequisitionNumbers.setContinuingOrTemporaryCode(ContinuingorTemporaryCode.replaceAll("\"", ""));
	                    	
	                    	 if(startDateFlag)
	                    	  districtRequisitionNumbers.setStartDate(formatter.parse(startDate));
	                    	 
	                    	 if(endDateFlag)
	                    	 districtRequisitionNumbers.setEndDate(formatter.parse(endDate));
	                    	 
	                    	 if(PostionTermFlag){
	                    		 String Term=PostionTerm.replaceAll("\"", "");
	                    		 if((Term==null)||(Term.equals("")))
	                    		 {
	                    			 Term="0.0"; 
	                    			 districtRequisitionNumbers.setPostionTerm(Float.valueOf(Term));
	                    		 }
	                    		 else
	                    		 {
	                    		 districtRequisitionNumbers.setPostionTerm(Float.valueOf(PostionTerm.replaceAll("\"", "")));
	                    		 }
	                    	 }
	                    	 if(payMethodFlag)
	                    	 districtRequisitionNumbers.setPayMethod(payMethod.replaceAll("\"", ""));
	                    	 
	                    	 if(postionStatusFlag)
	                    	 districtRequisitionNumbers.setPostionStatus(postionStatus.replaceAll("\"", ""));
	                    	
	                    	 if(commdriverLicencesFlag)
	                    	 districtRequisitionNumbers.setCommDriverLicence(commdriverLicences.replaceAll("\"", ""));
	                    	 
	                    	 if(postionPercentFundedFlag){
	                    		 
	                    		 String Term=postionPercentFunded.replaceAll("\"", "");
	                    		 if((Term==null)||(Term.equals("")))
	                    		 {
	                    			 Term="0.0"; 
	                    			 districtRequisitionNumbers.setPositionPercentFunded(Float.valueOf(Term));
	                    		 }
	                    		 else
	                    		 {
	                    			 districtRequisitionNumbers.setPositionPercentFunded(Float.valueOf(postionPercentFunded.replaceAll("\"", "")));
	                    		 }
	                    		                   		 
	                    	 }
	                    	 if(postionhoursweekFlag){
	                    		 String Term=postionhoursweek.replaceAll("\"", "");
	                		 if((Term==null)||(Term.equals("")))
	                		 {
	                			 Term="0.0"; 
	                			 districtRequisitionNumbers.setPostionHoursWeekFunded(Float.valueOf(Term));
	                		 }
	                		 else
	                		 {
	                			 districtRequisitionNumbers.setPostionHoursWeekFunded(Float.valueOf(postionhoursweek.replaceAll("\"", "")));
	                		 }
	                    	 }
	                    	 if(standardhoursweekFlag){
	                    		 String Term=standardhoursweek.replaceAll("\"", "");
	                    		 if((Term==null)||(Term.equals("")))
	                    		 {
	                    			 Term="0.0"; 
	                    			 districtRequisitionNumbers.setStandardHoursWeek(Float.valueOf(Term));
	                    		 }
	                    		 else
	                    		 {
	                    			 districtRequisitionNumbers.setStandardHoursWeek(Float.valueOf(standardhoursweek.replaceAll("\"", "")));
	                    		 }
	                    	 }
	                    	 
	                    	 districtRequisitionNumbers.setUpdateTimestamp(updatetimestamp.replaceAll("\"", ""));
	                    	 districtRequisitionNumbers.setCreatedDateTime(new Date());
	                    	 districtRequisitionNumbers.setUserMaster(userMaster);
	                    	 districtRequisitionNumbers.setIsUsed(false);
	                    	 districtRequisitionNumbers.setUploadFrom("O");
	                    	 districtRequisitionNumbers.setStatus("A");
	                    	 
	                    	 statelesSsession.insert(districtRequisitionNumbers);
	                    	 hrmsSitesList.put(LEANumber.replaceAll("\"", "")+"###"+(PositionNumber.replaceAll("\"", "")), districtRequisitionNumbers);
			            	
			            	 System.out.println("Insert districtRequisitionId:: "+districtRequisitionNumbers.getDistrictRequisitionId());
		            	   	insertData++;
		            	   	
	//satrt sandeep
		            	   	
		            	   	String requisitionTypeCode = districtRequisitionNumbers.getRequisitionTypeCode()==null?"":districtRequisitionNumbers.getRequisitionTypeCode();
		    				String jobCategoryName = "";
		    				if(requisitionTypeCode.equalsIgnoreCase("C"))
		    					jobCategoryName = "Classified";
		    				else if(requisitionTypeCode.equalsIgnoreCase("L"))
		    					jobCategoryName = "Licensed";
		    				else if(requisitionTypeCode.equalsIgnoreCase("B"))
		    					jobCategoryName = "Bus Driver";
		    				else if(requisitionTypeCode.equalsIgnoreCase("S"))
		    					jobCategoryName = "Substitute";
		    				
		    				JobCategoryMaster jobCategoryMaster = jobCategoryMasterMap.get(districtRequisitionNumbers.getDistrictMaster().getLocationCode()+"###"+jobCategoryName);
		    				
		    				if(jobCategoryMaster==null){
		     	    			errorText+="Job Category "+jobCategoryName+" does not found for LEA Number: "+districtRequisitionNumbers.getDistrictMaster().getLocationCode();
		     	    			System.out.println("errorText: "+errorText);
		     	    			errorText+=",";	
		     	    			continue;
		     	    		     
		     	    		}    
		     	    		
		    				 if(!errorText.equals("")){
		    	        			int row = i+1;
		     	    			final_error_store+="Row "+row+" : "+errorText+"\r\n"+LEANumber+","+PositionNumber+","+SiteNumber+","+PositionTitleCode+","+PositionDescription+","+PositionTypeCode+","+fptimeCode+","+ContinuingorTemporaryCode+","+startDate+","+endDate+","+PostionTerm+","+payMethod+","+postionStatus+","+commdriverLicences+","+postionPercentFunded+","+postionhoursweek+","+standardhoursweek+","+updatetimestamp+"<>";
		     	    			numOfError++;
		     	    		}
		    				 //51check
		    				 if(districtRequisitionNumbers.getPostionStatus()!=null 
		    						 &&( districtRequisitionNumbers.getPostionStatus().equalsIgnoreCase("51")
		    						 || districtRequisitionNumbers.getPostionStatus().equalsIgnoreCase("01")
		    						 || districtRequisitionNumbers.getPostionStatus().equalsIgnoreCase("02")
		    						 || districtRequisitionNumbers.getPostionStatus().equalsIgnoreCase("60")
		    						 || districtRequisitionNumbers.getPostionStatus().equalsIgnoreCase("PV"))){
		    					 
		    					 persistJobOrder(headQuarterMaster , districtRequisitionNumbers , errorText ,  userMaster , schoolMaster , jobCategoryMasterMap ,  statelesSsession ,jobCategoryMaster); 
		    				 }
		    				 
	//end sandeep	            	   	
		            	   	
		            	   	/*joborder creation
		            	  	            	   	 */
	//Start  Added By Viswanath	            	  
	/*	            	   	JobOrder jobOrder = null;
		            	   	jobOrder = new JobOrder();
		            	   	jobOrder.setHeadQuarterMaster(headQuarterMaster);
							jobOrder.setDistrictMaster(districtRequisitionNumbers.getDistrictMaster());
							jobOrder.setJobTitle(districtRequisitionNumbers.getJobTitle());
							if(districtRequisitionNumbers.getStartDate()!=null)
								jobOrder.setJobStartDate(districtRequisitionNumbers.getStartDate());
							else
								jobOrder.setJobStartDate(new Date());
							
							if(districtRequisitionNumbers.getEndDate()!=null)
								jobOrder.setJobEndDate(districtRequisitionNumbers.getEndDate());
							else if(districtRequisitionNumbers.getEndDate()!=null){
								jobOrder.setJobEndDate(new Date());
							}else{
								jobOrder.setJobEndDate(Utility.getCurrentDateFormart("12-25-2099"));
							}
								
							
							String requisitionTypeCode = districtRequisitionNumbers.getRequisitionTypeCode()==null?"":districtRequisitionNumbers.getRequisitionTypeCode();
							String jobCategoryName = "";
							if(requisitionTypeCode.equalsIgnoreCase("C"))
								jobCategoryName = "Classified";
							else if(requisitionTypeCode.equalsIgnoreCase("L"))
								jobCategoryName = "Licensed";
							else if(requisitionTypeCode.equalsIgnoreCase("B"))
								jobCategoryName = "Bus Driver";
							else if(requisitionTypeCode.equalsIgnoreCase("S"))
								jobCategoryName = "Substitute";
							
							JobCategoryMaster jobCategoryMaster = jobCategoryMasterMap.get(districtRequisitionNumbers.getDistrictMaster().getLocationCode()+"###"+jobCategoryName);
							
							if(jobCategoryMaster==null){
			  	    			errorText+="Job Category "+jobCategoryName+" does not found for LEA Number: "+districtRequisitionNumbers.getDistrictMaster().getLocationCode();
			  	    			System.out.println("errorText: "+errorText);
			  	    		    errorFlag  = true;
			  	    		}    
			  	    		if(errorFlag){
									errorText+=",";	
							}
			  	    		
							 if(!errorText.equals("")){
				        			int row = i+1;
			  	    			final_error_store+="Row "+row+" : "+errorText+"\r\n"+LEANumber+","+PositionNumber+","+SiteNumber+","+PositionTitleCode+","+PositionDescription+","+PositionTypeCode+","+fptimeCode+","+ContinuingorTemporaryCode+","+startDate+","+endDate+","+PostionTerm+","+payMethod+","+postionStatus+","+commdriverLicences+","+postionPercentFunded+","+postionhoursweek+","+standardhoursweek+","+updatetimestamp+"<>";
			  	    			numOfError++;
			  	    		}
							
							jobOrder.setJobCategoryMaster(jobCategoryMaster);
							jobOrder.setIsJobAssessment(false);
							jobOrder.setJobAssessmentStatus(0);
							
							jobOrder.setJobStatus("O");
							if(districtRequisitionNumbers.getPostionStatus()!=null && districtRequisitionNumbers.getPostionStatus().equalsIgnoreCase("51"))
								jobOrder.setStatus("I");
							else
								jobOrder.setStatus("A");
							
							jobOrder.setJobType(districtRequisitionNumbers.getPosType());
							jobOrder.setCreatedBy(userMaster.getUserId());
							jobOrder.setJobDescription(districtRequisitionNumbers.getRequisitionDescription());
							jobOrder.setIsInviteOnly(false);
							
							jobOrder.setCreatedByEntity(new Integer(2));
	
							
							jobOrder.setCreatedForEntity(new Integer(2));
							if(schoolMaster==null)
							{
								try {
									
									jobOrder.setNoOfExpHires(1);
								} catch (Exception e) {
									jobOrder.setNoOfExpHires(0);
									//e.printStackTrace();
								}
								jobOrder.setWritePrivilegeToSchool(false);
								jobOrder.setSelectedSchoolsInDistrict(new Integer(2));
								jobOrder.setAllGrades(new Integer(2));
								jobOrder.setAllSchoolsInDistrict(new Integer(2));
								jobOrder.setNoSchoolAttach(1);
							}else{
								jobOrder.setWritePrivilegeToSchool(true);
								jobOrder.setSelectedSchoolsInDistrict(new Integer(1));
								jobOrder.setNoSchoolAttach(2);
							}
							//}
							
							jobOrder.setApiJobId(districtRequisitionNumbers.getRequisitionNumber());
							jobOrder.setExitURL("https://platform.teachermatch.com");
							jobOrder.setExitMessage("");
							jobOrder.setCreatedDateTime(new Date());
							jobOrder.setIpAddress(IPAddressUtility.getIpAddress(request));
							jobOrder.setIsPortfolioNeeded(false);
							jobOrder.setIsPoolJob(0);
							
							//jobOrder.setSelectedSchoolsInDistrict(2);
							jobOrder.setCreatedBy(userMaster.getUserId());
							
							jobOrder.setOfferVirtualVideoInterview(false);
							jobOrder.setVVIExpiresInDays(0);
							jobOrder.setSendAutoVVILink(false);
							jobOrder.setApprovalBeforeGoLive(1);
							jobOrder.setAssessmentDocument(null);
							jobOrder.setAttachNewPillar(2);
							jobOrder.setAttachDefaultDistrictPillar(2);
							jobOrder.setAttachDefaultSchoolPillar(2);
							jobOrder.setIsExpHireNotEqualToReqNo(false);
							jobOrder.setNotificationToschool(true);
							
						   jobOrder.setAllGrades(2);
					       jobOrder.setPkOffered(2);
					       jobOrder.setKgOffered(2);
					       jobOrder.setG01Offered(2);
					       jobOrder.setG02Offered(2);
					       jobOrder.setG03Offered(2);
					       jobOrder.setG04Offered(2);
					       jobOrder.setG05Offered(2);
					       jobOrder.setG06Offered(2);
					       jobOrder.setG07Offered(2);
					       jobOrder.setG08Offered(2);
					       jobOrder.setG09Offered(2);
					       jobOrder.setG10Offered(2);
					       jobOrder.setG11Offered(2);
					       jobOrder.setG12Offered(2);
					       jobOrder.setNoOfHires(1);
							//jobOrderDAO.makePersistent(jobOrder);
							try {
								statelesSsession.insert(jobOrder);
								System.out.println("++++++++++++++++++++++++++++ "+jobOrder.getJobId());
							} catch (Exception e2) {
								e2.printStackTrace();
							}
		            	   
		            	    try {
								SchoolInJobOrder schoolInJobOrder = null;
								if(schoolMaster!=null)
								{
									schoolInJobOrder = new SchoolInJobOrder();
									schoolInJobOrder.setJobId(jobOrder);
									schoolInJobOrder.setSchoolId(schoolMaster);
									schoolInJobOrder.setNoOfSchoolExpHires(new Integer(1));
									schoolInJobOrder.setCreatedDateTime(new Date());
									statelesSsession.insert(schoolInJobOrder);
									//schoolInJobOrderDAO.makePersistent(schoolInJobOrder);
								}
							} catch (Exception e1) {
								e1.printStackTrace();
							}
		            	   	
		            	   	try {
								JobRequisitionNumbers jrn = null;
								jrn = new JobRequisitionNumbers();
								jrn.setJobOrder(jobOrder);
								jrn.setStatus(0);
								jrn.setSchoolMaster(schoolMaster);
								jrn.setDistrictRequisitionNumbers(districtRequisitionNumbers);
								statelesSsession.insert(jrn);
							} catch (Exception e) {
								e.printStackTrace();
							}
	
	*/						
	// End Added By Viswanath   
		    				 
	                    }
                     }else{
                    	 
                   		 //districtRequisitionNumbers.setDistrictMaster(districtMaster);
                   		 districtRequisitionNumbers.setSchoolMaster(schoolMaster);
                   
                    	 if(PositionTitleCodeFlag)
                    	 districtRequisitionNumbers.setJobTitle(PositionTitleCode.replaceAll("\"", ""));
                    	 
                    	 if(PositionDescriptionFlag)
                    	 districtRequisitionNumbers.setRequisitionDescription(PositionDescription.replaceAll("\"", ""));
                    	
                    	 if(PositionTypeCodeFlag)
                    	 districtRequisitionNumbers.setRequisitionTypeCode(PositionTypeCode.replaceAll("\"", ""));
                    	
                    	 if(fptimeCodeFlag)
                    	 districtRequisitionNumbers.setPosType(fptimeCode.replaceAll("\"", ""));
                    	
                    	 if(ContinuingorTemporaryCodeFlag)
                    	 districtRequisitionNumbers.setContinuingOrTemporaryCode(ContinuingorTemporaryCode.replaceAll("\"", ""));
                    	 
                    	 if(startDateFlag)
                    	 districtRequisitionNumbers.setStartDate(formatter.parse(startDate));
                    	 
                    	 if(endDateFlag)
                    	 districtRequisitionNumbers.setEndDate(formatter.parse(endDate));
                    	 
                    	 if(PostionTermFlag){
                    		 String Term=PostionTerm.replaceAll("\"", "");
                    		 if((Term==null)||(Term.equals("")))
                    		 {
                    			 Term="0.0"; 
                    			 districtRequisitionNumbers.setPostionTerm(Float.valueOf(Term));
                    		 }
                    		 else
                    		 {
                    		 districtRequisitionNumbers.setPostionTerm(Float.valueOf(PostionTerm.replaceAll("\"", "")));
                    		 }
                    	 }
                    	 if(payMethodFlag)
                    	  districtRequisitionNumbers.setPayMethod(payMethod.replaceAll("\"", ""));
                    	 
                    	 if(postionStatusFlag)
                    	 districtRequisitionNumbers.setPostionStatus(postionStatus.replaceAll("\"", ""));
                    	
                    	 if(commdriverLicencesFlag)
                    	 districtRequisitionNumbers.setCommDriverLicence(commdriverLicences.replaceAll("\"", ""));
                    	 
                    	 if(postionPercentFundedFlag){
                    		 String Term=postionPercentFunded.replaceAll("\"", "");
                    		 if((Term==null)||(Term.equals("")))
                    		 {
                    			 Term="0.0"; 
                    			 districtRequisitionNumbers.setPositionPercentFunded(Float.valueOf(Term));
                    		 }
                    		 else
                    		 {
                    			 districtRequisitionNumbers.setPositionPercentFunded(Float.valueOf(postionPercentFunded.replaceAll("\"", "")));
                    		 }	 
                    		
                    	 }
                    	 if(postionhoursweekFlag){
                    		 String Term=postionhoursweek.replaceAll("\"", "");
                    		 if((Term==null)||(Term.equals("")))
                    		 {
                    			 Term="0.0"; 
                    			 districtRequisitionNumbers.setPostionHoursWeekFunded(Float.valueOf(Term));
                    		 }
                    		 else
                    		 {
                    			 districtRequisitionNumbers.setPostionHoursWeekFunded(Float.valueOf(postionhoursweek.replaceAll("\"", "")));
                    		 }
                    	 }
                    	 if(standardhoursweekFlag){
                    		 String Term=standardhoursweek.replaceAll("\"", "");
                    		 if((Term==null)||(Term.equals("")))
                    		 {
                    			 Term="0.0"; 
                    			 districtRequisitionNumbers.setStandardHoursWeek(Float.valueOf(Term));
                    		 }
                    		 else
                    		 {
                    			 districtRequisitionNumbers.setStandardHoursWeek(Float.valueOf(standardhoursweek.replaceAll("\"", "")));
                    		 }
                    	 }
                    	
                    	 
                    	 districtRequisitionNumbers.setUpdateTimestamp(updatetimestamp.replaceAll("\"", ""));
                    	 //districtRequisitionNumbers.setIsUsed(false);
                    	 //districtRequisitionNumbers.setCreatedDateTime(new Date());
                    	 //districtRequisitionNumbers.setUserMaster(userMaster);
                    	 //districtRequisitionNumbers.setUploadFrom("O");
                    	 //districtRequisitionNumbers.setStatus("A");
                    	 
		            	 //statelesSsession.update(districtRequisitionNumbers);		            	 	
		            	
	            	 	//updateData++;
	            	 	
	            	 	
	//start sandeep 28-07-2015
	            	 	//should be unique
	            	 	JobOrder jobOrder = null;
	            	 	//commented in order to get joborder object from jobRequisitionNumbersJobMap map
	            	   	/*if(jobOrderMap.containsKey(districtRequisitionNumbers.getRequisitionNumber()+"_"+districtRequisitionNumbers.getDistrictMaster().getDistrictId())){
	                		jobOrder = jobOrderMap.get(districtRequisitionNumbers.getRequisitionNumber()+"_"+districtRequisitionNumbers.getDistrictMaster().getDistrictId());
	                	}*/
	            	 	if(jobRequisitionNumbersJobMap.containsKey(districtRequisitionNumbers.getDistrictMaster().getLocationCode().trim()+"##"+districtRequisitionNumbers.getRequisitionNumber().trim())){
	                		JobRequisitionNumbers jobRequisitionNumbers = jobRequisitionNumbersJobMap.get(districtRequisitionNumbers.getDistrictMaster().getLocationCode().trim()+"##"+districtRequisitionNumbers.getRequisitionNumber().trim());
	                		jobOrder=jobRequisitionNumbers.getJobOrder();
	                		
	                	}
	            	 	
	            	 	//udpate districtrequisitionnumber where job is not active
	            	 	Boolean jobStatusFlag = true;
	            	 	if(jobOrder==null)
		           		{
		           			jobStatusFlag = false;
		           		}
	            	 	else if(jobOrder.getStatus()!=null && !jobOrder.getStatus().equalsIgnoreCase("A"))
            			    jobStatusFlag = false;
            			//else if(jobOrder != null && !Utility.checkDateBetweenTwoDate(jobOrder.getJobStartDate(), jobOrder.getJobEndDate()))
            					//jobStatusFlag = false;
	           		    
	           		
	            	   	if(jobStatusFlag){
	            	   		
	            	   		continue;	
	            	   	}
	            	 	statelesSsession.update(districtRequisitionNumbers);		            	 	
	            	 	updateData++;

	            	   	
	           	if(jobOrder != null){
	            	   	
	           		
	           		//If Status=51 , Job is active than ignor updation
		           	/*Boolean jobStatusFlag = true;
	           		if(jobOrder.getStatus()!=null && !jobOrder.getStatus().equalsIgnoreCase("A"))
	        			    jobStatusFlag = false;
	        		else if(!Utility.checkDateBetweenTwoDate(jobOrder.getJobStartDate(), jobOrder.getJobEndDate()))
	        			jobStatusFlag = false;
	           		    if(jobStatusFlag){
	            	   		
	            	   		continue;	
	            	   	}*/
	            	   	if(districtRequisitionNumbers.getPostionStatus()!=null 
	            	   			&& ( districtRequisitionNumbers.getPostionStatus().equalsIgnoreCase("61")
		    						 || districtRequisitionNumbers.getPostionStatus().equalsIgnoreCase("00")
		    						 ))
	            	   	{
	            	   		jobOrder.setStatus("H");  //Do Not Display
	            	   	}
	            	   	if(districtRequisitionNumbers.getPostionStatus()!=null
	            	   			&& jobOrder.getStatus().equalsIgnoreCase("H")
	           		    		&& ( districtRequisitionNumbers.getPostionStatus().equalsIgnoreCase("51")
		    						 || districtRequisitionNumbers.getPostionStatus().equalsIgnoreCase("01")
		    						 || districtRequisitionNumbers.getPostionStatus().equalsIgnoreCase("02")
		    						 || districtRequisitionNumbers.getPostionStatus().equalsIgnoreCase("60")
		    						 || districtRequisitionNumbers.getPostionStatus().equalsIgnoreCase("PV")))
	            	   	{
	            	   		jobOrder.setStatus("I");
	            	   	}
	            	   	jobOrder.setJobTitle(districtRequisitionNumbers.getJobTitle());
						if(districtRequisitionNumbers.getStartDate()!=null)
							jobOrder.setJobStartDate(districtRequisitionNumbers.getStartDate());
						if(districtRequisitionNumbers.getEndDate()!=null)
							jobOrder.setJobEndDate(districtRequisitionNumbers.getEndDate());
						String requisitionTypeCode = districtRequisitionNumbers.getRequisitionTypeCode()==null?"":districtRequisitionNumbers.getRequisitionTypeCode();
						String jobCategoryName = "";
						if(requisitionTypeCode.equalsIgnoreCase("C"))
							jobCategoryName = "Classified";
						else if(requisitionTypeCode.equalsIgnoreCase("L"))
							jobCategoryName = "Licensed";
						else if(requisitionTypeCode.equalsIgnoreCase("B"))
							jobCategoryName = "Bus Driver";
						else if(requisitionTypeCode.equalsIgnoreCase("S"))
							jobCategoryName = "Substitute";
						
						JobCategoryMaster jobCategoryMaster = jobCategoryMasterMap.get(districtRequisitionNumbers.getDistrictMaster().getLocationCode()+"###"+jobCategoryName);
						
						if(jobCategoryMaster==null){
		  	    			errorText+="Job Category "+jobCategoryName+" does not found for LEA Number: "+districtRequisitionNumbers.getDistrictMaster().getLocationCode();
		  	    			System.out.println("errorText: "+errorText);
		  	    		    errorText+=",";
		  	    		    continue;
		  	    		}    
		  	    		 if(!errorText.equals("")){
			        			int row = i+1;
		  	    			final_error_store+="Row "+row+" : "+errorText+"\r\n"+LEANumber+","+PositionNumber+","+SiteNumber+","+PositionTitleCode+","+PositionDescription+","+PositionTypeCode+","+fptimeCode+","+ContinuingorTemporaryCode+","+startDate+","+endDate+","+PostionTerm+","+payMethod+","+postionStatus+","+commdriverLicences+","+postionPercentFunded+","+postionhoursweek+","+standardhoursweek+","+updatetimestamp+"<>";
		  	    			numOfError++;
		  	    		}
						
						jobOrder.setJobCategoryMaster(jobCategoryMaster);
						if(jobCategoryMaster.getOfferJSI()==1)
						{
							jobOrder.setIsJobAssessment(true);
							jobOrder.setAttachDefaultJobSpecificPillar(1);
							jobOrder.setJobAssessmentStatus(1);
						}
						else
						{
							jobOrder.setIsJobAssessment(false);
							jobOrder.setJobAssessmentStatus(0);
						}
						jobOrder.setJobStatus("O");
						if(districtRequisitionNumbers.getPosType()!=null && !districtRequisitionNumbers.getPosType().trim().equals(""))
							jobOrder.setJobType(districtRequisitionNumbers.getPosType());
						else
							jobOrder.setJobType("F");
				
						jobOrder.setJobDescription(districtRequisitionNumbers.getRequisitionDescription());
						try {
							
							statelesSsession.update(jobOrder);
							System.out.println("Update JobOrder++++++++++++++ "+jobOrder.getJobId());
						} catch (Exception e2) {
							e2.printStackTrace();
						}
	            	   
	            	    try {
	            	    	SchoolInJobOrder schoolInJobOrder = null;
	            	    	if(schoolInJobOrderMap.containsKey(jobOrder.getJobId())){
	            	    		schoolInJobOrder = schoolInJobOrderMap.get(jobOrder.getJobId());
		                	}
	            	    	
							if(schoolMaster!=null)
							{
								if(schoolInJobOrder==null){
								
									schoolInJobOrder = new SchoolInJobOrder();
									schoolInJobOrder.setJobId(jobOrder);
									schoolInJobOrder.setDistrictMaster(districtRequisitionNumbers.getDistrictMaster());
									schoolInJobOrder.setSchoolId(schoolMaster);
									schoolInJobOrder.setNoOfSchoolExpHires(new Integer(1));
									schoolInJobOrder.setCreatedDateTime(new Date());
									
									statelesSsession.insert(schoolInJobOrder);
								}else{
									schoolInJobOrder.setSchoolId(schoolMaster);
									//schoolInJobOrder.setDistrictMaster(districtRequisitionNumbers.getDistrictMaster());
									//schoolInJobOrder.setNoOfSchoolExpHires(new Integer(1));
									//schoolInJobOrder.setCreatedDateTime(new Date());
									
									statelesSsession.update(schoolInJobOrder);
								}
								
							}
						} catch (Exception e1) {
							e1.printStackTrace();
						}
	            	   	
	            	   	try {
							JobRequisitionNumbers jrn = null;
							if(jobRequisitionNumbersJobMap.containsKey(districtRequisitionNumbers.getDistrictMaster().getLocationCode().trim()+"##"+districtRequisitionNumbers.getRequisitionNumber().trim()))
							{
		                		 jrn = jobRequisitionNumbersJobMap.get(districtRequisitionNumbers.getDistrictMaster().getLocationCode().trim()+"##"+districtRequisitionNumbers.getRequisitionNumber().trim());
		                	}
							
							if(jrn == null){
								jrn = new JobRequisitionNumbers();
								jrn.setJobOrder(jobOrder);
								jrn.setStatus(0);
								jrn.setSchoolMaster(schoolMaster);
								jrn.setDistrictRequisitionNumbers(districtRequisitionNumbers);
								statelesSsession.insert(jrn);
								jobRequisitionNumbersJobMap.put(districtRequisitionNumbers.getDistrictMaster().getLocationCode().trim()+"##"+districtRequisitionNumbers.getRequisitionNumber().trim(),jrn);
							}else{
								jrn.setStatus(0);
								jrn.setSchoolMaster(schoolMaster);
								jrn.setDistrictRequisitionNumbers(districtRequisitionNumbers);
								statelesSsession.update(jrn);
								
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
	            	 	
	           	}else{
	           		
	           		String requisitionTypeCode = districtRequisitionNumbers.getRequisitionTypeCode()==null?"":districtRequisitionNumbers.getRequisitionTypeCode();
    				String jobCategoryName = "";
    				if(requisitionTypeCode.equalsIgnoreCase("C"))
    					jobCategoryName = "Classified";
    				else if(requisitionTypeCode.equalsIgnoreCase("L"))
    					jobCategoryName = "Licensed";
    				else if(requisitionTypeCode.equalsIgnoreCase("B"))
    					jobCategoryName = "Bus Driver";
    				else if(requisitionTypeCode.equalsIgnoreCase("S"))
    					jobCategoryName = "Substitute";
    				
    				JobCategoryMaster jobCategoryMaster = jobCategoryMasterMap.get(districtRequisitionNumbers.getDistrictMaster().getLocationCode()+"###"+jobCategoryName);
    				
    				if(jobCategoryMaster==null){
     	    			errorText+="Job Category "+jobCategoryName+" does not found for LEA Number: "+districtRequisitionNumbers.getDistrictMaster().getLocationCode();
     	    			System.out.println("errorText: "+errorText);
     	    			errorText+=",";
     	    			continue;
     	    		}    
     	    		
     	    		
    				 if(!errorText.equals("")){
    	        			int row = i+1;
     	    			final_error_store+="Row "+row+" : "+errorText+"\r\n"+LEANumber+","+PositionNumber+","+SiteNumber+","+PositionTitleCode+","+PositionDescription+","+PositionTypeCode+","+fptimeCode+","+ContinuingorTemporaryCode+","+startDate+","+endDate+","+PostionTerm+","+payMethod+","+postionStatus+","+commdriverLicences+","+postionPercentFunded+","+postionhoursweek+","+standardhoursweek+","+updatetimestamp+"<>";
     	    			numOfError++;
     	    		}
    				 //51check
    				 if(districtRequisitionNumbers.getPostionStatus()!=null 
    						 && ( districtRequisitionNumbers.getPostionStatus().equalsIgnoreCase("51")
		    						 || districtRequisitionNumbers.getPostionStatus().equalsIgnoreCase("01")
		    						 || districtRequisitionNumbers.getPostionStatus().equalsIgnoreCase("02")
		    						 || districtRequisitionNumbers.getPostionStatus().equalsIgnoreCase("60")
		    						 || districtRequisitionNumbers.getPostionStatus().equalsIgnoreCase("PV"))){
    					 //if districtrequisition exist then no need to create job
    					 //JobRequisitionNumbers jrn= jobRequisitionNumbersDAO.findJobReqNumbersObj(districtRequisitionNumbers);
    					 JobRequisitionNumbers jrn= (JobRequisitionNumbers)statelesSsession.get(JobRequisitionNumbers.class,districtRequisitionNumbers.getDistrictRequisitionId());
    					 System.out.println("--------------------------"+districtRequisitionNumbers.getDistrictRequisitionId()+"--------------------------");
    					 if(jrn!=null)
    					 {
    						 System.out.println("****************************************"+jrn);
    					 }
    					 
    					 if(jrn==null)
    						 persistJobOrder (headQuarterMaster , districtRequisitionNumbers , errorText ,  userMaster , schoolMaster , jobCategoryMasterMap ,  statelesSsession ,jobCategoryMaster); 
    				 }
	           	}
	            	 	
    //end sandeep 28-07-2015	            	 	
	            	 	
	            	 	
                     }
                     
                if(x>=100)
                {
                	x=0;
                	txOpen.commit();
                	txOpen=statelesSsession.beginTransaction();
                }
                	
	  	    		} 
                catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	  	    }
	  	    
	  	    
	  	    
		 }   

		 try{
			if(!final_error_store.equalsIgnoreCase("")){
	 	 		String content = final_error_store;
				File file = new File(folderPath+fileName+"hrmsPostionError.txt");
				// if file doesnt exists, then create it
				if (!file.exists()) {
					file.createNewFile();
				}
				String[] parts = content.split("<>");	    				
				FileWriter fw = new FileWriter(file.getAbsoluteFile());
				BufferedWriter bw = new BufferedWriter(fw);
				bw.write("POS_LEA_NUM,POS_NUM_CD,POS_SITE_CD,POS_TITLE_CD,POS_DESC,POS_CATEGORY_CD,POS_FTORPT_CD,POS_CLASS_CD,POS_START_DTE,POS_END_DATE,POS_TERM_NUM,POS_PAYMTHD_CD,POS_STATUS_CD,POS_CDLREQD_IND,POS_FUNDED_PCT,POS_HOURS_NUM,POS_STDHRS_NUM,UPDATE_TS");
				bw.write("\r\n\r\n");
				int k =0;
				for(String cont :parts) {
					bw.write(cont+"\r\n\r\n");
				    k++;
				}
				bw.close();
				
	 	 	}	
		  } catch (Exception e) {
				e.printStackTrace();
			}
		
		  try
		  {
			  if(x!=0)
				  txOpen.commit();
			  statelesSsession.close();  
		  }
		  catch(Exception e)
		  {
			e.printStackTrace();  
		  }
		  
		System.out.print("After uploaded file "+insertData+" rows inserted and "+updateData+" rows updated.");
		String msg="After uploaded file "+insertData+" rows inserted and "+updateData+" rows updated.";

		return msg;
}

    
    
    
    public static Vector readDataTxt(String filePath,Set<String> set) throws FileNotFoundException{
    	
		Vector vectorData = new Vector();
		String filepathhdr=filePath;

		 int LEA_Number_count=11;	    	 
    	 int POS_NUM_CD_count=11;
    	 int Site_Name_count=11;
    	 int POS_TITLE_CD_count=11; 
    	 int POS_DESC_count=11;
    	 int POS_CATEGORY_CD_count=11;
    	 int POS_FTORPT_CD_count=11;
    	 int POS_CLASS_CD_count=11;
    	 int POS_START_DTE_count=11;
    	 int POS_END_DATE_count=11;
    	 int POS_TERM_NUM_count=11;
    	 int POS_PAYMTHD_CD_count=11;
    	 int POS_STATUS_CD_count=11;
    	 int POS_CDLREQD_IND_count=11;
    	 int POS_FUNDED_PCT_count=11;
    	 int POS_HOURS_NUM_count=11;
    	 int POS_STDHRS_NUM_count=11;
    	 int UPDATE_TS_count=11;
		
		    
	        BufferedReader brhdr=new BufferedReader(new FileReader(filepathhdr));
	        
	        String strLineHdr="";	        
	        String hdrstr="";
	        
	        StringTokenizer sthdr=null;
	        StringTokenizer stdtl=null;
	        int lineNumberHdr=0;
	        try{
	            
	            while((strLineHdr=brhdr.readLine())!=null){  
	            	
	            	if(strLineHdr.isEmpty())
	            		continue;
	            	Vector vectorCellEachRowData = new Vector();
	                int cIndex=0;
	                boolean cellFlag=false;
	                Map<String,String> mapCell = new TreeMap<String, String>();
	                int i=1;
	                
	                String[] parts = strLineHdr.split("\\|");
	                
	                for(int j=0; j<parts.length; j++) {
	                	hdrstr=parts[j];
	                	cIndex=j;
	                        
	                        if(hdrstr.toString().trim().equalsIgnoreCase("POS_LEA_NUM")){
	                        	LEA_Number_count=cIndex;
	                    		
	                    	}
	                        
	                        if(LEA_Number_count==cIndex){
	                    		cellFlag=true;
								if(!hdrstr.toString().trim().equalsIgnoreCase("POS_LEA_NUM"))
	                    		{
	                    			set.add(hdrstr.toString().trim());
	                    		}
	                    	}
	                        
	                        if(hdrstr.toString().trim().equalsIgnoreCase("POS_NUM_CD")){
	                        	POS_NUM_CD_count=cIndex;
	                    		
	                    	}
	                    	if(POS_NUM_CD_count==cIndex){
	                    		cellFlag=true;
	                    		
	                    	}	
	                    	  
	                    	if(hdrstr.toString().trim().equalsIgnoreCase("POS_SITE_CD")){
	                    		Site_Name_count=cIndex;
	                    		
	                    	}
	                    	if(Site_Name_count==cIndex){
	                    		cellFlag=true;
	                    	}
	                    	
	                    	
	                    	if(hdrstr.toString().trim().equalsIgnoreCase("POS_TITLE_CD")){
	                    		POS_TITLE_CD_count=cIndex;
	                    		
	                    	}
	                    	if(POS_TITLE_CD_count==cIndex){
	                    		cellFlag=true;
	                    	}
	                    	
	                    	if(hdrstr.toString().trim().equalsIgnoreCase("POS_DESC")){
	                    		POS_DESC_count=cIndex;
	                    		
	                    	}
	                    	if(POS_DESC_count==cIndex){
	                    		cellFlag=true;
	                    	}
	                    	
	                    	if(hdrstr.toString().trim().equalsIgnoreCase("POS_CATEGORY_CD")){
	                    		POS_CATEGORY_CD_count=cIndex;
	                    		
	                    	}
	                    	if(POS_CATEGORY_CD_count==cIndex){
	                    		cellFlag=true;
	                    	}
	                    	
	                    	if(hdrstr.toString().trim().equalsIgnoreCase("POS_FTORPT_CD")){
	                    		POS_FTORPT_CD_count=cIndex;
	                    		
	                    	}
	                    	if(POS_FTORPT_CD_count==cIndex){
	                    		cellFlag=true;
	                    	}
	                    	
	                    	if(hdrstr.toString().trim().equalsIgnoreCase("POS_CLASS_CD")){
	                    		POS_CLASS_CD_count=cIndex;
	                    		
	                    	}
	                    	if(POS_CLASS_CD_count==cIndex){
	                    		cellFlag=true;
	                    	}
	                    	
	                    	if(hdrstr.toString().trim().equalsIgnoreCase("POS_START_DTE")){
	                    		POS_START_DTE_count=cIndex;
	                    		
	                    	}
	                    	if(POS_START_DTE_count==cIndex){
	                    		cellFlag=true;
	                    	}
	                    	
	                    	if(hdrstr.toString().trim().equalsIgnoreCase("POS_END_DATE")){
	                    		POS_END_DATE_count=cIndex;
	                    		
	                    	}
	                    	if(POS_END_DATE_count==cIndex){
	                    		cellFlag=true;
	                    	}
	                    	
	                    	if(hdrstr.toString().trim().equalsIgnoreCase("POS_TERM_NUM")){
	                    		POS_TERM_NUM_count=cIndex;
	                    		
	                    	}
	                    	if(POS_TERM_NUM_count==cIndex){
	                    		cellFlag=true;
	                    	}
	                    	
	                    	if(hdrstr.toString().trim().equalsIgnoreCase("POS_PAYMTHD_CD")){
	                    		POS_PAYMTHD_CD_count=cIndex;
	                    		
	                    	}
	                    	if(POS_PAYMTHD_CD_count==cIndex){
	                    		cellFlag=true;
	                    	}
	                    	if(hdrstr.toString().trim().equalsIgnoreCase("POS_STATUS_CD")){
	                    		POS_STATUS_CD_count=cIndex;
	                    		
	                    	}
	                    	if(POS_STATUS_CD_count==cIndex){
	                    		cellFlag=true;
	                    	}
	                    	if(hdrstr.toString().trim().equalsIgnoreCase("POS_CDLREQD_IND")){
	                    		POS_CDLREQD_IND_count=cIndex;
	                    		
	                    	}
	                    	if(POS_CDLREQD_IND_count==cIndex){
	                    		cellFlag=true;
	                    	}
	                    	if(hdrstr.toString().trim().equalsIgnoreCase("POS_FUNDED_PCT")){
	                    		POS_FUNDED_PCT_count=cIndex;
	                    		
	                    	}
	                    	if(POS_FUNDED_PCT_count==cIndex){
	                    		cellFlag=true;
	                    	}
	                    	if(hdrstr.toString().trim().equalsIgnoreCase("POS_HOURS_NUM")){
	                    		POS_HOURS_NUM_count=cIndex;
	                    		
	                    	}
	                    	if(POS_HOURS_NUM_count==cIndex){
	                    		cellFlag=true;
	                    	}
	                    	if(hdrstr.toString().trim().equalsIgnoreCase("POS_STDHRS_NUM")){
	                    		POS_STDHRS_NUM_count=cIndex;
	                    		
	                    	}
	                    	if(POS_STDHRS_NUM_count==cIndex){
	                    		cellFlag=true;
	                    	}
	                    	if(hdrstr.toString().trim().equalsIgnoreCase("UPDATE_TS")){
	                    		UPDATE_TS_count=cIndex;
	                    		
	                    	}
	                    	if(UPDATE_TS_count==cIndex){
	                    		cellFlag=true;
	                    	}
	                    	
	                    	
	                    	if(cellFlag){
	                    		
	                    			try{
	                    				mapCell.put(cIndex+"",hdrstr);
	                    			}catch(Exception e){
	                    				mapCell.put(cIndex+"",hdrstr);
	                    			}
	                    		
	                    	}
	                        
	                    }
	                    
	                    vectorCellEachRowData=cellValuePopulate(mapCell);
	                    vectorData.addElement(vectorCellEachRowData);
	                lineNumberHdr++;
	            }
	        }
	        catch(Exception e){
	            System.out.println(e.getMessage());
	        }
	       // System.out.println("::::::::: read data from text ::::::::::       "+vectorData);       
		return vectorData;
	}
    
    
		    public static Vector cellValuePopulate(Map<String,String> mapCell){
		   	 Vector vectorCellEachRowData = new Vector();
		   	 Map<String,String> mapCellTemp = new TreeMap<String, String>();
		   	 
		   	 boolean flag0=false,flag1=false,flag2=false,flag3=false,flag4=false,flag5=false,flag6=false,flag7=false,flag8=false,flag9=false,flag10=false,flag11=false,flag12=false,flag13=false,flag14=false,flag15=false,flag16=false,flag17=false;
		   	 for(Map.Entry<String, String> entry : mapCell.entrySet()){
		   		 String key=entry.getKey();
		   		String cellValue=null;
		   		if(entry.getValue()!=null)
		   			cellValue=entry.getValue().trim();
		   		    
		   		
		   		if(key.equals("0")){
		   			mapCellTemp.put(key, cellValue);
		   			flag0=true;
		   		}
		   		if(key.equals("1")){
		   			flag1=true;
		   			mapCellTemp.put(key, cellValue);
		   		}
		   		if(key.equals("2")){
		   			flag2=true;
		   			mapCellTemp.put(key, cellValue);
		   		}
		   		if(key.equals("3")){
		   			flag3=true;
		   			mapCellTemp.put(key, cellValue);
		   		}
		   		if(key.equals("4")){
		   			flag4=true;
		   			mapCellTemp.put(key, cellValue);
		   		}
		   		if(key.equals("5")){
		   			flag5=true;
		   			mapCellTemp.put(key, cellValue);
		   		}
		   		if(key.equals("6")){
		   			flag6=true;
		   			mapCellTemp.put(key, cellValue);
		   		}
		   		
		   		if(key.equals("7")){
		   			flag7=true;
		   			mapCellTemp.put(key, cellValue);
		   		}
		   		if(key.equals("8")){
		   			flag8=true;
		   			mapCellTemp.put(key, cellValue);
		   		}
		   		if(key.equals("9")){
		   			flag9=true;
		   			mapCellTemp.put(key, cellValue);
		   		}
		   		if(key.equals("10")){
		   			flag10=true;
		   			mapCellTemp.put(key, cellValue);
		   		}
		   		if(key.equals("11")){
		   			flag11=true;
		   			mapCellTemp.put(key, cellValue);
		   		}
		   		if(key.equals("12")){
		   			flag12=true;
		   			mapCellTemp.put(key, cellValue);
		   		}
		   		if(key.equals("13")){
		   			flag13=true;
		   			mapCellTemp.put(key, cellValue);
		   		}
		   		if(key.equals("14")){
		   			flag14=true;
		   			mapCellTemp.put(key, cellValue);
		   		}
		   		if(key.equals("15")){
		   			flag15=true;
		   			mapCellTemp.put(key, cellValue);
		   		}
		   		if(key.equals("16")){
		   			flag16=true;
		   			mapCellTemp.put(key, cellValue);
		   		}
		   		if(key.equals("17")){
		   			flag17=true;
		   			mapCellTemp.put(key, cellValue);
		   		}
		   		
		   		
		   	 }
		   	 if(flag0==false){
		   		 mapCellTemp.put(0+"", "");
		   	 }
		   	 if(flag1==false){
		   		 mapCellTemp.put(1+"", "");
		   	 }
		   	 if(flag2==false){
		   		 mapCellTemp.put(2+"", "");
		   	 }
		   	if(flag3==false){
		   		 mapCellTemp.put(3+"", "");
		   	 }
		   	if(flag4==false){
		   		 mapCellTemp.put(4+"", "");
		   	 }
		   	if(flag5==false){
		   		 mapCellTemp.put(5+"", "");
		   	 }
		   	if(flag6==false){
		   		 mapCellTemp.put(6+"", "");
		   	 }
		   	if(flag7==false){
		   		 mapCellTemp.put(7+"", "");
		   	 }
		   	if(flag8==false){
		   		 mapCellTemp.put(8+"", "");
		   	 }
		   	if(flag9==false){
		   		 mapCellTemp.put(9+"", "");
		   	 }
		   	if(flag10==false){
		   		 mapCellTemp.put(10+"", "");
		   	 }
		   	if(flag11==false){
		   		 mapCellTemp.put(11+"", "");
		   	 }
		   	if(flag12==false){
		   		 mapCellTemp.put(12+"", "");
		   	 }
		   	if(flag13==false){
		   		 mapCellTemp.put(13+"", "");
		   	 }
		   	if(flag14==false){
		   		 mapCellTemp.put(14+"", "");
		   	 }
		   	if(flag15==false){
		   		 mapCellTemp.put(15+"", "");
		   	 }
		   	if(flag16==false){
		   		 mapCellTemp.put(16+"", "");
		   	 }
			if(flag17==false){
		   		 mapCellTemp.put(17+"", "");
		   	 }
		   	
		   	 			 
		   	 for(Map.Entry<String, String> entry : mapCellTemp.entrySet()){
		   		
		   		 vectorCellEachRowData.addElement(entry.getValue());
		   	 }
		   	 return vectorCellEachRowData;
		   }
    	
		    
		    @Transactional(readOnly=false)
		   public void persistJobOrder (HeadQuarterMaster headQuarterMaster , DistrictRequisitionNumbers districtRequisitionNumbers ,String errorText , UserMaster userMaster , SchoolMaster schoolMaster , Map<String, JobCategoryMaster> jobCategoryMasterMap , StatelessSession statelesSsession , JobCategoryMaster jobCategoryMaster){
			   
			   
			    JobOrder jobOrder = null;
       	    	jobOrder = new JobOrder();
        	// 	jobOrder.setHeadQuarterMaster(headQuarterMaster);
				jobOrder.setDistrictMaster(districtRequisitionNumbers.getDistrictMaster());
				jobOrder.setJobTitle(districtRequisitionNumbers.getJobTitle());
				if(districtRequisitionNumbers.getStartDate()!=null)
					jobOrder.setJobStartDate(districtRequisitionNumbers.getStartDate());
				else
					jobOrder.setJobStartDate(new Date());
				
				if(districtRequisitionNumbers.getEndDate()!=null)
					jobOrder.setJobEndDate(districtRequisitionNumbers.getEndDate());
				else if(districtRequisitionNumbers.getEndDate()!=null){
					jobOrder.setJobEndDate(new Date());
				}else{
					try {
						jobOrder.setJobEndDate(Utility.getCurrentDateFormart("12-25-2099"));
					} catch (ParseException e) {
						e.printStackTrace();
					}
				}
					
				jobOrder.setJobCategoryMaster(jobCategoryMaster);				
				if(jobCategoryMaster.getOfferJSI()==1)
				{
					jobOrder.setIsJobAssessment(true);
					jobOrder.setAttachDefaultJobSpecificPillar(1);
					jobOrder.setJobAssessmentStatus(1);
				}
				else
				{
					jobOrder.setIsJobAssessment(false);
					jobOrder.setJobAssessmentStatus(0);
				}				
				
				
				jobOrder.setJobStatus("O");
				//51check
				if(districtRequisitionNumbers.getPostionStatus()!=null 
						&& ( districtRequisitionNumbers.getPostionStatus().equalsIgnoreCase("51")
		    						 || districtRequisitionNumbers.getPostionStatus().equalsIgnoreCase("01")
		    						 || districtRequisitionNumbers.getPostionStatus().equalsIgnoreCase("02")
		    						 || districtRequisitionNumbers.getPostionStatus().equalsIgnoreCase("60")
		    						 || districtRequisitionNumbers.getPostionStatus().equalsIgnoreCase("PV")))
					jobOrder.setStatus("I");
				else
					jobOrder.setStatus("A");
				if(districtRequisitionNumbers.getPosType()!=null && !districtRequisitionNumbers.getPosType().trim().equals(""))
					jobOrder.setJobType(districtRequisitionNumbers.getPosType());
				else
					jobOrder.setJobType("F");
				jobOrder.setCreatedBy(userMaster.getUserId());
				jobOrder.setJobDescription(districtRequisitionNumbers.getRequisitionDescription());
				jobOrder.setIsInviteOnly(false);
				jobOrder.setCreatedByEntity(new Integer(2));
				jobOrder.setCreatedForEntity(new Integer(2));
				if(schoolMaster==null)
				{
					try {
						
						jobOrder.setNoOfExpHires(1);
					} catch (Exception e) {
						jobOrder.setNoOfExpHires(0);

					}
					jobOrder.setWritePrivilegeToSchool(false);
					jobOrder.setSelectedSchoolsInDistrict(new Integer(2));
					jobOrder.setAllGrades(new Integer(2));
					jobOrder.setAllSchoolsInDistrict(new Integer(2));
					jobOrder.setNoSchoolAttach(1);
				}else{
					jobOrder.setWritePrivilegeToSchool(false);
					jobOrder.setSelectedSchoolsInDistrict(new Integer(1));
					jobOrder.setNoSchoolAttach(2);
				}

				
				jobOrder.setApiJobId(districtRequisitionNumbers.getRequisitionNumber());
				if(districtRequisitionNumbers.getDistrictMaster()!=null 
						&& districtRequisitionNumbers.getDistrictMaster().getExitURL()!=null
						&& (!districtRequisitionNumbers.getDistrictMaster().getExitURL().trim().equals("")))
					jobOrder.setExitURL(districtRequisitionNumbers.getDistrictMaster().getExitURL());
				else
					jobOrder.setExitURL("https://nc.teachermatch.org");
				jobOrder.setExitMessage("");
				jobOrder.setFlagForURL(1);
				jobOrder.setCreatedDateTime(new Date());
				//jobOrder.setIpAddress(IPAddressUtility.getIpAddress(request));
				jobOrder.setIsPortfolioNeeded(false);
				jobOrder.setIsPoolJob(0);
				jobOrder.setCreatedBy(userMaster.getUserId());
				
				jobOrder.setOfferVirtualVideoInterview(false);
				jobOrder.setVVIExpiresInDays(0);
				jobOrder.setSendAutoVVILink(false);
				jobOrder.setApprovalBeforeGoLive(1);
				jobOrder.setAssessmentDocument(null);
				jobOrder.setAttachNewPillar(2);
				jobOrder.setAttachDefaultDistrictPillar(2);
				jobOrder.setAttachDefaultSchoolPillar(2);
				jobOrder.setIsExpHireNotEqualToReqNo(false);
				jobOrder.setNotificationToschool(true);
				
			   jobOrder.setAllGrades(2);
		       jobOrder.setPkOffered(2);
		       jobOrder.setKgOffered(2);
		       jobOrder.setG01Offered(2);
		       jobOrder.setG02Offered(2);
		       jobOrder.setG03Offered(2);
		       jobOrder.setG04Offered(2);
		       jobOrder.setG05Offered(2);
		       jobOrder.setG06Offered(2);
		       jobOrder.setG07Offered(2);
		       jobOrder.setG08Offered(2);
		       jobOrder.setG09Offered(2);
		       jobOrder.setG10Offered(2);
		       jobOrder.setG11Offered(2);
		       jobOrder.setG12Offered(2);
		       jobOrder.setNoOfHires(1);
		       
				try {
					statelesSsession.insert(jobOrder);
					System.out.println("Insert JobOrder JobId---"+jobOrder.getJobId());
				} catch (Exception e2) {
					e2.printStackTrace();
				}
       	   
       	    try {
					SchoolInJobOrder schoolInJobOrder = null;
					if(schoolMaster!=null)
					{
						schoolInJobOrder = new SchoolInJobOrder();
						schoolInJobOrder.setJobId(jobOrder);
						schoolInJobOrder.setDistrictMaster(districtRequisitionNumbers.getDistrictMaster());
						schoolInJobOrder.setSchoolId(schoolMaster);
						schoolInJobOrder.setNoOfSchoolExpHires(new Integer(1));
						schoolInJobOrder.setCreatedDateTime(new Date());
						statelesSsession.insert(schoolInJobOrder);
						System.out.println("schoolinjoborder insert ######");
			
					}
				} catch (Exception e1) {
					e1.printStackTrace();
				}
       	   	
       	   	try {
					JobRequisitionNumbers jrn = null;
					jrn = new JobRequisitionNumbers();
					jrn.setJobOrder(jobOrder);
					jrn.setStatus(0);
					jrn.setSchoolMaster(schoolMaster);
					jrn.setDistrictRequisitionNumbers(districtRequisitionNumbers);
					statelesSsession.insert(jrn);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			   
		   }
		    
		    @RequestMapping(value="/hrmspositionfiledownload.do", method=RequestMethod.GET)
			public @ResponseBody String downloadingPositionFile(HttpServletRequest request)
			{
		    	if(Utility.isNC())
			    {
			    	String hostname = "sftp.teachermatch.org";
			        String login = "mqfteteachertmatch";
			        String password = "Teacher@mqfte#20A5";
			        //String directory = "/usr/share/shadab/";
			       // String directory = "/inbox/shadab/new/";
			        String directory = "/inbox/";
			        //String folderPath="C:\\Users\\Adarsh\\Desktop\\SSN\\download\\";
			        String folderPath="/var/lib/tomcat8/uploadExcelByScheduler/shadab/";
			        System.out.println("downloadCount=="+downloadCount);
			        if(downloadCount==0)
					{
			        	Channel channel=null;
			        	Session session=null;
						try
						{
							
								downloadCount=1;
								java.util.Properties config = new java.util.Properties();
						        config.put("StrictHostKeyChecking", "no");
						        JSch ssh = new JSch();
						        session = ssh.getSession(login, hostname, 22);
						        session.setConfig(config);
						        session.setPassword(password);
						        session.connect();
						        channel = session.openChannel("sftp");
						        channel.connect();
						 
						        ChannelSftp sftp = (ChannelSftp) channel;
						        sftp.cd(directory);
						        Vector<ChannelSftp.LsEntry> files = sftp.ls("*");
						        int totalLineNo=0;
						        int fileNo=1;
						        boolean endLoop=true;
						        //System.out.printf("Found %d files in dir %s%n", files.size(), directory);
						 
						        for (ChannelSftp.LsEntry file1 : files) 
						        {
						        	//System.out.printf("Reading file : %s", file1.getFilename()+"\tsize="+file1.getAttrs().getSize());
						        	//System.out.println();
						        	if (Utility.getDateDifference(new Date((long)file1.getAttrs().getMTime()*1000),new Date()) <=1 &&
						        			file1.getFilename().startsWith("HRMS-") && 
						            	(file1.getFilename().endsWith("POS--PositionData.txt") || file1.getFilename().endsWith("LEA--PositionDeltaData.txt"))) 
						            {
						            	
						            	String fileName=file1.getFilename().replace(".txt", "")+"_"+file1.getAttrs().getMTime()+"_"+fileNo+".txt";
						            	System.out.println(fileName);
						            	Criterion criterion=Restrictions.eq("fileName", fileName);
							            List<HrmsPositionsTextDetail> list=hrmsPositionsTextDetailDAO.findByCriteria(criterion);
							            List<String> fileList=new ArrayList<String>();
							            //if empty means file is not downloaded
							            if(list==null || list.isEmpty())
							            {
							            	BufferedReader bis = new BufferedReader(new InputStreamReader(sftp.get(file1.getFilename())));
							            	while(endLoop)
							            	{
							            		File file=new File(folderPath+fileName);
							            		//File file=new File(folderPath+fileName);
							            		fileList.add(fileName);
									        	FileOutputStream fout=new FileOutputStream(file);
									        	String header="POS_LEA_NUM|POS_NUM_CD|POS_SITE_CD|PCHD_DESC|POS_DESC|POS_CATEGORY_CD|POS_FTORPT_CD|POS_CLASS_CD|POS_START_DTE|POS_END_DATE|POS_TERM_NUM|POS_PAYMTHD_CD|POS_STATUS_CD|POS_CDLREQD_IND|POS_FUNDED_PCT|POS_HOURS_NUM|POS_STDHRS_NUM|UPDATE_TS";
									            String line = null;
									            fout.write(header.getBytes());
									            fout.write("\n".getBytes());
									            int lineCount=0;
									            while ((line = bis.readLine()) != null) {
									            	endLoop=false;
									            	totalLineNo++;
									                lineCount++;
									            	fout.write(line.getBytes());
									            	fout.write("\n".getBytes());
									            	if(lineCount>=10000)
									            	{
									            		fileNo++;
									            		fileName=file1.getFilename().replace(".txt", "")+"_"+file1.getAttrs().getMTime()+"_"+fileNo+".txt";
									            		endLoop=true;
									            		break;
									            	}
									            		
									            }
									            if(line==null)
									            	endLoop=false;
									            fout.flush();
									            fout.close();
									            System.out.println();
									            System.out.println("*************Downloading complete*********************");
									            
							            	}
								            bis.close();
								            channel.disconnect();
									        session.disconnect();
								            System.out.println("Downloading Done");
								            for(String s:fileList)
								            {
								            	HrmsPositionsTextDetail positionsTextDetail=new HrmsPositionsTextDetail();
									            positionsTextDetail.setCreatedDateTime(new Date());
									            positionsTextDetail.setFileName(s);
									            positionsTextDetail.setFolderPath(folderPath);
									            //positionsTextDetail.setFolderPath("/var/lib/tomcat8/uploadExcelByScheduler/shadab/");
									            positionsTextDetail.setStatus("A");
									            long fileCreatedDateTime=file1.getAttrs().getATime();
									            positionsTextDetail.setFileCreatedDateTime(new Date(fileCreatedDateTime*1000));
									            long fileModifiedDateTime=file1.getAttrs().getMTime();
									            positionsTextDetail.setFileModifiedDateTime(new Date(fileModifiedDateTime*1000));
									            
									            positionsTextDetail.setFileSize((file1.getAttrs().getSize())); //in bytes
								            	positionsTextDetail.setUploaded(false);
										        hrmsPositionsTextDetailDAO.makePersistent(positionsTextDetail);
								            	String msg=saveLicense(positionsTextDetail.getFileName(),positionsTextDetail.getFolderPath());
								            	positionsTextDetail.setUploaded(true);
								            	positionsTextDetail.setUploadedDateTime(new Date());
								            	positionsTextDetail.setMessage(msg);
								            	hrmsPositionsTextDetailDAO.makePersistent(positionsTextDetail);
								            	System.out.println(positionsTextDetail.getFileName() +"uploaded");
								            }
								            
								            break;
							            	
							            }
						                
						            }
						          }
						       
						        System.out.println("*************uploading completed********************");
							
					        
						}
						catch(Exception e)
						{
							e.printStackTrace();
						}
						finally
						{
							downloadCount=0;
							channel.disconnect();
					        session.disconnect();
												
							
						}
				
					}
		    	}
		    	
		    	return "";
			}
		    
		    public void setDAO(DistrictRequisitionNumbersDAO districtRequisitionNumbersDAO,SchoolMasterDAO schoolMasterDAO,UserMasterDAO userMasterDAO,DistrictMasterDAO districtMasterDAO,
		    		UserUploadFolderAccessDAO userUploadFolderAccessDAO,JobCategoryMasterDAO jobCategoryMasterDAO,JobRequisitionNumbersDAO jobRequisitionNumbersDAO,
		    		SchoolInJobOrderDAO schoolInJobOrderDAO,JobOrderDAO jobOrderDAO,HrmsPositionsTextDetailDAO hrmsPositionsTextDetailDAO)
		    {
		    	this.districtRequisitionNumbersDAO=districtRequisitionNumbersDAO;
		    	this.schoolMasterDAO=schoolMasterDAO;
		    	this.userMasterDAO=userMasterDAO;
		    	this.districtMasterDAO=districtMasterDAO;
		    	this.userUploadFolderAccessDAO=userUploadFolderAccessDAO;
		    	this.jobCategoryMasterDAO=jobCategoryMasterDAO;
		    	this.jobRequisitionNumbersDAO=jobRequisitionNumbersDAO;
	    		this.schoolInJobOrderDAO=schoolInJobOrderDAO;
	    		this.jobOrderDAO=jobOrderDAO;
	    		this.hrmsPositionsTextDetailDAO=hrmsPositionsTextDetailDAO;
		    }
		  
}