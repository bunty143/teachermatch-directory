package tm.controller.master;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import tm.bean.EmployeeMaster;
import tm.bean.FileUploadForm;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.JobRequisitionNumbers;
import tm.bean.SchoolWiseCandidateStatus;
import tm.bean.TeacherAdditionalDocuments;
import tm.bean.TeacherAssessmentStatus;
import tm.bean.TeacherDetail;
import tm.bean.TeacherGeneralKnowledgeExam;
import tm.bean.TeacherPersonalInfo;
import tm.bean.TeacherStatusHistoryForJob;
import tm.bean.TeacherSubjectAreaExam;
import tm.bean.assessment.AssessmentDetail;
import tm.bean.assessment.AssessmentJobRelation;
import tm.bean.cgreport.TeacherStatusNotes;
import tm.bean.cgreport.UserMailSend;
import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.DistrictKeyContact;
import tm.bean.master.DistrictMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.JobWisePanelStatus;
import tm.bean.master.PanelAttendees;
import tm.bean.master.PanelSchedule;
import tm.bean.master.SchoolMaster;
import tm.bean.master.SecondaryStatus;
import tm.bean.master.SecondaryStatusMaster;
import tm.bean.master.StateMaster;
import tm.bean.master.StatusMaster;
import tm.bean.master.SubjectAreaExamMaster;
import tm.bean.master.SubjectMaster;
import tm.bean.user.UserMaster;
import tm.dao.DistrictRequisitionNumbersDAO;
import tm.dao.EmployeeMasterDAO;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.JobRequisitionNumbersDAO;
import tm.dao.SchoolWiseCandidateStatusDAO;
import tm.dao.TeacherAdditionalDocumentsDAO;
import tm.dao.TeacherAssessmentStatusDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.TeacherGeneralKnowledgeExamDAO;
import tm.dao.TeacherPersonalInfoDAO;
import tm.dao.TeacherPortfolioStatusDAO;
import tm.dao.TeacherStatusHistoryForJobDAO;
import tm.dao.TeacherSubjectAreaExamDAO;
import tm.dao.assessment.AssessmentDetailDAO;
import tm.dao.assessment.AssessmentJobRelationDAO;
import tm.dao.cgreport.TeacherStatusNotesDAO;
import tm.dao.hqbranchesmaster.BranchMasterDAO;
import tm.dao.hqbranchesmaster.HeadQuarterMasterDAO;
import tm.dao.master.ContactTypeMasterDAO;
import tm.dao.master.DistrictKeyContactDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.JobCategoryMasterDAO;
import tm.dao.master.JobWisePanelStatusDAO;
import tm.dao.master.PanelAttendeesDAO;
import tm.dao.master.PanelScheduleDAO;
import tm.dao.master.SecondaryStatusDAO;
import tm.dao.master.SecondaryStatusMasterDAO;
import tm.dao.master.StateMasterDAO;
import tm.dao.master.StatusMasterDAO;
import tm.dao.master.SubjectAreaExamMasterDAO;
import tm.dao.master.SubjectMasterDAO;
import tm.dao.master.TimeZoneMasterDAO;
import tm.dao.menu.RoleAccessPermissionDAO;
import tm.dao.user.UserMasterDAO;
import tm.services.DemoScheduleMailThread;
import tm.services.EmailerService;
import tm.services.MailSendToDASA;
import tm.services.MailText;
import tm.services.district.ManageStatusAjax;
import tm.services.district.PrintOnConsole;
import tm.services.report.CandidateGridService;
import tm.services.teacher.MailSendEvlCompThread;
import tm.services.teacher.OnlineActivityAjax;
import tm.utility.TestTool;
import tm.utility.Utility;

@Controller
public class TeacherController 
{	
	String locale = Utility.getValueOfPropByKey("locale");
	
	@Autowired
	private AssessmentDetailDAO assessmentDetailDAO;
	
	@Autowired
	private SecondaryStatusMasterDAO secondaryStatusMasterDAO;
	
	
	@Autowired
	private TeacherStatusNotesDAO teacherStatusNotesDAO;
	
	@Autowired
	private TeacherPersonalInfoDAO teacherPersonalInfoDAO;
	
	@Autowired
	private SchoolWiseCandidateStatusDAO schoolWiseCandidateStatusDAO;
	@Autowired
	private EmailerService emailerService;
	public void setEmailerService(EmailerService emailerService) 
	{
		this.emailerService = emailerService;
	}
	
	@Autowired
	private TeacherStatusHistoryForJobDAO teacherStatusHistoryForJobDAO;
	
	@Autowired
	private PanelScheduleDAO panelScheduleDAO;
	
	@Autowired
	private SecondaryStatusDAO secondaryStatusDAO; 
	
	
	@Autowired
	private JobWisePanelStatusDAO jobWisePanelStatusDAO;
	
	@Autowired
	private JobRequisitionNumbersDAO jobRequisitionNumbersDAO;
	
	@Autowired
	private DistrictRequisitionNumbersDAO districtRequisitionNumbersDAO;
	@Autowired
	private JobOrderDAO jobOrderDAO;
	
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	
	@Autowired
	private SubjectAreaExamMasterDAO subjectAreaExamMasterDAO;
	@Autowired
	private TeacherSubjectAreaExamDAO teacherSubjectAreaExamDAO;
	
	@Autowired
	private TeacherGeneralKnowledgeExamDAO teacherGeneralKnowledgeExamDAO;
	

	@Autowired
	private TeacherAdditionalDocumentsDAO teacherAdditionalDocumentsDAO;
	
	@Autowired
	private TimeZoneMasterDAO timeZoneMasterDAO;
	public void setTimeZoneMasterDAO(TimeZoneMasterDAO timeZoneMasterDAO) {
		this.timeZoneMasterDAO = timeZoneMasterDAO;
	}
	
	@Autowired
	private UserMasterDAO userMasterDAO;
	public void setUserMasterDAO(UserMasterDAO userMasterDAO) {
		this.userMasterDAO = userMasterDAO;
	}
	@Autowired
	private TeacherDetailDAO teacherDetailDAO;
	public void setTeacherDetailDAO(TeacherDetailDAO teacherDetailDAO)
	{
		this.teacherDetailDAO = teacherDetailDAO;
	}
	@Autowired
	private StatusMasterDAO statusMasterDAO;
	public void setStatusMasterDAO(StatusMasterDAO statusMasterDAO) {
		this.statusMasterDAO = statusMasterDAO;
	}
	
	@Autowired
	private JobCategoryMasterDAO jobCategoryMasterDAO;
	public void setJobCategoryMasterDAO(
			JobCategoryMasterDAO jobCategoryMasterDAO) {
		this.jobCategoryMasterDAO = jobCategoryMasterDAO;
	}
	
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	public void setDistrictMasterDAO(DistrictMasterDAO districtMasterDAO) {
		this.districtMasterDAO = districtMasterDAO;
	}
	
	@Autowired
	private StateMasterDAO stateMasterDAO;

	@Autowired
	private RoleAccessPermissionDAO roleAccessPermissionDAO;
	
	@Autowired
	private SubjectMasterDAO subjectMasterDAO;
	
	@Autowired
	private DistrictKeyContactDAO districtKeyContactDAO;
	
	@Autowired
	private PanelAttendeesDAO panelAttendeesDAO;
	
	@Autowired
	private ManageStatusAjax manageStatusAjax;
	
	@Autowired
	private ContactTypeMasterDAO contactTypeMasterDAO;
	
	@Autowired
	private TeacherPortfolioStatusDAO teacherPortfolioStatusDAO;
	
	@Autowired
	private TeacherAssessmentStatusDAO teacherAssessmentStatusDAO;
	
	@Autowired
	private AssessmentJobRelationDAO assessmentJobRelationDAO;
	
	@Autowired
	private EmployeeMasterDAO employeeMasterDAO;
	
	@Autowired
	private OnlineActivityAjax onlineActivityAjax;
	public void setOnlineActivityAjax(
			OnlineActivityAjax onlineActivityAjax) {
		this.onlineActivityAjax = onlineActivityAjax;
	} 
	
	@Autowired
	private HeadQuarterMasterDAO headQuarterMasterDAO;
	
	@Autowired
	private BranchMasterDAO branchMasterDAO;	
	public void setBranchMasterDAO(BranchMasterDAO branchMasterDAO) {
		this.branchMasterDAO = branchMasterDAO;
	}
	
	/* @Author: Gagan 
	 * @Discription: view of TeacherController in Admin panel.
	 */
	/*============ Get Method of TeacherController ====================*/
	@RequestMapping(value="/teacherinfoold.do", method=RequestMethod.GET)
	public String teaccherInfoGET(ModelMap map,HttpServletRequest request)
	{
		try 
		{
			System.out.println("\n =========== teaccherinfo.do in TeacherController  ===============");
			HttpSession session = request.getSession(false);

			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}

			UserMaster userSession		=	(UserMaster) session.getAttribute("userMaster");

			map.addAttribute("userSession", userSession);
			int entityID=0,roleId=0;
			if(userSession!=null){
				entityID=userSession.getEntityType();
				if(userSession.getRoleId().getRoleId()!=null){
					roleId=userSession.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,38,"candidatereport.do",0);
			}catch(Exception e){
				e.printStackTrace();
			}
			map.addAttribute("entityID", entityID);
			if(userSession.getEntityType()!=1){
				map.addAttribute("DistrictName",userSession.getDistrictId().getDistrictName());
				map.addAttribute("DistrictId",userSession.getDistrictId().getDistrictId());
			}else{
				map.addAttribute("DistrictName",null);
			}
			
			if(userSession.getEntityType()==2){
				map.addAttribute("DistrictName",userSession.getDistrictId().getDistrictName());
				map.addAttribute("DistrictId",userSession.getDistrictId().getDistrictId());
				map.addAttribute("activeuser",userMasterDAO.getUserByDistrict(userSession.getDistrictId()));
				
			}
			if(userSession.getEntityType()==3){
				map.addAttribute("SchoolId",userSession.getSchoolId().getSchoolId());
				map.addAttribute("schoolName",userSession.getSchoolId().getSchoolName());
				map.addAttribute("activeuser",userMasterDAO.getUserBySchool(userSession.getSchoolId()));
			}else{
				map.addAttribute("SchoolName",null);
			}
			List<StatusMaster> lstStatusMasters = new ArrayList<StatusMaster>();
			String[] statusTotal = {"comp","icomp","hird","rem","widrw","vlt"};
			try{
				lstStatusMasters = statusMasterDAO.findStatusByShortNames(statusTotal);
			}catch (Exception e) {
				e.printStackTrace();
			}
			List<StateMaster> lstStateMaster = new ArrayList<StateMaster>();
			lstStateMaster = stateMasterDAO.findAllStateByOrder();
			
			// @Ashish :: Add job Category list
			DistrictMaster districtMaster = null;
			List<JobCategoryMaster> lstJobCategoryMasters = new ArrayList<JobCategoryMaster>();
			
			boolean statusNotes=false;
			boolean writePrivilegeToSchool = false;
			boolean achievementScore=false,tFA=false,demoClass=false,JSI=false,teachingOfYear =false,expectedSalary=false,fitScore=false;
			boolean isTeacherPoolOnLoad = false;
			if(entityID!=1)
			{
				districtMaster = districtMasterDAO.findById(userSession.getDistrictId().getDistrictId(), false, false);
				lstJobCategoryMasters = jobCategoryMasterDAO.findAllJobCategoryNameByDistrict(districtMaster);
				try{

					if(districtMaster.getDisplayAchievementScore()){
						achievementScore=true;
					}
					if(districtMaster.getDisplayTFA()){
						tFA=true;
					}
					if(districtMaster.getDisplayDemoClass()){
						demoClass=true;
					}
					if(districtMaster.getDisplayJSI()){
						JSI=true;
					}
					if(districtMaster.getDisplayYearsTeaching()){
						teachingOfYear=true;
					}
					if(districtMaster.getDisplayExpectedSalary()){
						expectedSalary=true;
					}
					if(districtMaster.getDisplayFitScore()){
						fitScore=true;
					}
					
					if(districtMaster.getStatusNotes())
						statusNotes=true;
					
					writePrivilegeToSchool = districtMaster.getWritePrivilegeToSchool();
					isTeacherPoolOnLoad = districtMaster.getIsTeacherPoolOnLoad()==null?false:districtMaster.getIsTeacherPoolOnLoad()==true;
				}catch(Exception e){ e.printStackTrace();}
			}else
			{
				achievementScore=true;tFA=true;demoClass=true;JSI=true;teachingOfYear =true;expectedSalary=true;fitScore=true;
			}
			
			try{
				if(districtMaster!=null && userSession.getEntityType()!=1){
					if(districtMaster.getIsReqNoForHiring()!=null && districtMaster.getIsReqNoForHiring()){
						map.addAttribute("isReqNoForHiring",1);	
					}else{
						map.addAttribute("isReqNoForHiring",0);
					}
				}else{
					map.addAttribute("isReqNoForHiring",0);
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			
			map.addAttribute("isTeacherPoolOnLoad",isTeacherPoolOnLoad);
			
			Map<String,Boolean> prefMap = new HashMap<String, Boolean>();
			prefMap.put("achievementScore", achievementScore);
			prefMap.put("tFA", tFA);
			prefMap.put("demoClass", demoClass);
			prefMap.put("JSI", JSI);
			prefMap.put("teachingOfYear", teachingOfYear);
			prefMap.put("expectedSalary", expectedSalary);
			prefMap.put("fitScore", fitScore);
			
			map.addAttribute("prefMap",prefMap);
			map.addAttribute("lstStateMaster",lstStateMaster);
			map.addAttribute("lstJobCategoryMasters",lstJobCategoryMasters);
			map.addAttribute("lstStatusMasters",lstStatusMasters);
			map.addAttribute("roleAccess", roleAccess);
			map.addAttribute("timezone",timeZoneMasterDAO.getAllTimeZone());
			map.addAttribute("writePrivilegeToSchool",writePrivilegeToSchool);
			List<SubjectMaster> subjectMasters = new ArrayList<SubjectMaster>();
			
			if(userSession.getEntityType()==2)
				subjectMasters = subjectMasterDAO.findActiveSubjectByDistrict((DistrictMaster)userSession.getDistrictId());
			
			map.addAttribute("subjectMasters",subjectMasters);
			map.addAttribute("entityType",userSession.getEntityType());
			map.addAttribute("statusNotes",statusNotes);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return "teacherinfo";
	}
	/*============End Of Get Method of Domain Controller ====================*/
	
	@RequestMapping(value = "/multifileupload.do", method = RequestMethod.POST)
	public String save(@ModelAttribute("uploadForm") FileUploadForm uploadForm,Model map,HttpServletRequest request) {
		
		System.out.println(":::::::::::::multifileupload.do::::::::::::::::::::::::::::::");
		HttpSession session = request.getSession();			
		TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
		
		List<MultipartFile> files = uploadForm.getFiles();
		int teacherId=0;
		if(teacherDetail!=null){
			teacherId=teacherDetail.getTeacherId();
		}
		String generalKnowledgeExamStatus="P";
		String generalKnowledgeExamDate=null;
		String examStatus="P";
		String examDate=null;
		String subjectAreaExamId="1";
		String generalExamNote="";
		String documentName=null;
		
		String generalKnowledgeScoreReportHidden=null;
		String scoreReportHidden=null;
		String subjectExamTextarea="";
		String uploadedDocumentHidden=null;
		
		int uploadFileNumber=0;
		if(request.getParameter("uploadFileNumber")!=null){
			uploadFileNumber=Integer.parseInt(request.getParameter("uploadFileNumber"));
		}
		
		if(request.getParameter("generalKnowledgeScoreReportHidden")!=null){
			generalKnowledgeScoreReportHidden=request.getParameter("generalKnowledgeScoreReportHidden");
		}
		if(request.getParameter("scoreReportHidden")!=null){
			scoreReportHidden=request.getParameter("scoreReportHidden");
		}
		if(request.getParameter("uploadedDocumentHidden")!=null){
			uploadedDocumentHidden=request.getParameter("uploadedDocumentHidden");
		}
		
		if(request.getParameter("generalKnowledgeExamStatus")!=null){
			generalKnowledgeExamStatus=request.getParameter("generalKnowledgeExamStatus");
		}
		if(request.getParameter("subjectExamTextarea")!=null){
			subjectExamTextarea=request.getParameter("subjectExamTextarea");
		}
		if(request.getParameter("generalKnowledgeExamDate")!=null){
			generalKnowledgeExamDate=request.getParameter("generalKnowledgeExamDate");
		}
		if(request.getParameter("examStatus")!=null){
			examStatus=request.getParameter("examStatus");
		}
		if(request.getParameter("examDate")!=null){
			examDate=request.getParameter("examDate");
		}
		if(request.getParameter("generalExamNote")!=null){
			generalExamNote=request.getParameter("generalExamNote");
		}
		if(request.getParameter("subjectIdforDSPQ")!=null){
			subjectAreaExamId=request.getParameter("subjectIdforDSPQ");
		}
		
		if(request.getParameter("subjectIdforDSPQ")!=null){
			subjectAreaExamId=request.getParameter("subjectIdforDSPQ");
		}
		
		if(request.getParameter("documentName")!=null){
			documentName=request.getParameter("documentName");
		}
		
		
		TeacherAdditionalDocuments	teacherAdditionalDocuments = teacherAdditionalDocumentsDAO.findTeacherAdditionalDocuments(teacherDetail);
		TeacherGeneralKnowledgeExam teacherGeneralKnowledgeExam = teacherGeneralKnowledgeExamDAO.findTeacherGeneralKnowledgeExam(teacherDetail);
		TeacherSubjectAreaExam teacherSubjectAreaExam = teacherSubjectAreaExamDAO.findTeacherSubjectAreaExam(teacherDetail);
		
		int i=0;
		String ext="",filePath="";
		OutputStream outputStream = null;
		if(null != files && files.size() > 0) {
			for (MultipartFile multipartFile : files) {
				String fileName = multipartFile.getOriginalFilename();
				System.out.println("file name:"+fileName);
				if(fileName!=null && !fileName.equals("")){
					filePath=Utility.getValueOfPropByKey("teacherRootPath")+teacherId+"/";
					File uploadedFile=null;
					try{
						String myFullFileName = fileName, myFileName = "", slashType = (myFullFileName.lastIndexOf("\\") > 0) ? "\\" : "/";
						int startIndex = myFullFileName.lastIndexOf(slashType);
						myFileName = myFullFileName.substring(startIndex + 1, myFullFileName.length());
						ext=myFileName.substring(myFileName.lastIndexOf("."),myFileName.length()).toLowerCase();
						fileName=myFullFileName.substring(startIndex + 1, myFileName.lastIndexOf("."))+teacherId+Utility.getDateTime()+ext;
						
						File f=new File(filePath);
						if(!f.exists())
							 f.mkdirs();
						
						uploadedFile = new File(filePath, fileName);
						System.out.println(fileName+":::filePath::"+filePath);
					}catch(Exception e){
						//e.printStackTrace();
					}
					if(teacherGeneralKnowledgeExam!=null && i==0 && uploadFileNumber!=3){
						File file = new File(Utility.getValueOfPropByKey("teacherRootPath")+teacherId+"/"+teacherGeneralKnowledgeExam.getGeneralKnowledgeScoreReport());
						if(file.exists()){
							if(file.delete()){
								System.out.println(file.getName()+ " deleted");
							}
						}
					}
					if(teacherSubjectAreaExam!=null  && i==1  && uploadFileNumber!=3){
						File file = new File(Utility.getValueOfPropByKey("teacherRootPath")+teacherId+"/"+teacherSubjectAreaExam.getScoreReport());
						if(file.exists()){
							if(file.delete()){
								System.out.println(file.getName()+ " deleted");
							}
						}
					}
					if(teacherAdditionalDocuments!=null  && uploadFileNumber==3){
						/*File file = new File(Utility.getValueOfPropByKey("teacherRootPath")+teacherId+"/"+teacherAdditionalDocuments.getUploadedDocument());
						if(file.exists()){
							if(file.delete()){
								System.out.println(file.getName()+ " deleted");
							}
						}*/
					}
					try{
						InputStream stream = multipartFile.getInputStream();
						outputStream = new FileOutputStream(uploadedFile);
							int read = 0;
							byte[] bytes = new byte[1024];
							while ((read = stream.read(bytes)) != -1) {
								outputStream.write(bytes, 0, read);
							}
							System.out.println("Done!");
					}catch(Exception e){
				 		//e.printStackTrace();
				  	}
				}
					if(i==0 && uploadFileNumber!=3 &&  (!Utility.isNC())){
						if(fileName==null || fileName.equals("")){
							fileName=generalKnowledgeScoreReportHidden;
						}
						try{
							TeacherGeneralKnowledgeExam obj=new TeacherGeneralKnowledgeExam();
							obj.setTeacherDetail(teacherDetail);
							obj.setGeneralKnowledgeExamStatus(generalKnowledgeExamStatus);
							obj.setGeneralKnowledgeScoreReport(fileName);
							obj.setCreatedDateTime(new Date());
							obj.setGeneralExamNote(generalExamNote);
							obj.setGeneralKnowledgeExamDate(Utility.getCurrentDateFormart(generalKnowledgeExamDate));
							if(teacherGeneralKnowledgeExam!=null){
								obj.setGeneralKnowledgeExamId(teacherGeneralKnowledgeExam.getGeneralKnowledgeExamId());
								teacherGeneralKnowledgeExamDAO.updatePersistent(obj);
							}else{
								teacherGeneralKnowledgeExamDAO.makePersistent(obj);
							}
						}catch(Exception e){
							//e.printStackTrace();
						}
					}else if(i==1 && uploadFileNumber!=3){
						if(fileName==null || fileName.equals("")){
							fileName=scoreReportHidden;
						}
						try{
							SubjectAreaExamMaster subjectMaster= subjectAreaExamMasterDAO.findById(Integer.parseInt(subjectAreaExamId),false,false);
							TeacherSubjectAreaExam objSA=new TeacherSubjectAreaExam();
							objSA.setTeacherDetail(teacherDetail);
							objSA.setExamStatus(examStatus);
							objSA.setScoreReport(fileName);
							objSA.setExamNote(subjectExamTextarea);
							objSA.setCreatedDateTime(new Date());
							objSA.setExamDate(Utility.getCurrentDateFormart(examDate));
							objSA.setSubjectAreaExamMaster(subjectMaster);
							if(teacherSubjectAreaExam!=null){
								objSA.setTeacherSubjectAreaExamId(teacherSubjectAreaExam.getTeacherSubjectAreaExamId());
								teacherSubjectAreaExamDAO.updatePersistent(objSA);
							}else{
								teacherSubjectAreaExamDAO.makePersistent(objSA);
							}
						}catch(Exception e){
							//e.printStackTrace();
						}
					}else if(uploadFileNumber==3){
						if(fileName==null || fileName.equals("")){
							fileName=uploadedDocumentHidden;
						}
						try{
							TeacherAdditionalDocuments objAD=new TeacherAdditionalDocuments();
							objAD.setTeacherDetail(teacherDetail);
							objAD.setDocumentName(documentName);
							objAD.setCreatedDateTime(new Date());
							objAD.setUploadedDocument(fileName);
							teacherAdditionalDocumentsDAO.makePersistent(objAD);
						}catch(Exception e){
							//e.printStackTrace();
						}
					}
				i++;
			}
		}
		
		return null;
	}
	@RequestMapping(value="/verifyoffered.do", method=RequestMethod.GET)
	public String doVerifiedGET(ModelMap map,HttpServletRequest request){	
		request.getSession();
		
		System.out.println(" verifyoffered.do >::::::::::::");
		try{
			String key = request.getParameter("key")==null?"":request.getParameter("key").trim();
			System.out.println("key:::::::::::>>>>"+key);
			String msg="";
			String schoolLocation="";
			String schoolLocationWithCode="";
			String locationCode="";
			if(key!=null && !key.equals("")){
				String keyValue=Utility.decodeBase64(key);
				
				System.out.println("keyValue:::"+keyValue);
				
				String teacherId=keyValue.split("##")[0];
				String jobId=keyValue.split("##")[1];
				String offerAccepted=keyValue.split("##")[2];
				String statusFlag=keyValue.split("##")[3];
				String statusId=keyValue.split("##")[4];
				String userId=keyValue.split("##")[5];
				boolean offer=false;
				if(offerAccepted!=null && offerAccepted.equals("1")){
					offer=true;
				}
				StatusMaster statusMasterVcom=statusMasterDAO.findStatusByShortName("vcomp");
				System.out.println("teacherId="+teacherId);
				System.out.println("offerAccepted="+offerAccepted);
				System.out.println("jobId="+jobId);
				
				TeacherDetail teacherDetail= teacherDetailDAO.findById(Integer.parseInt(teacherId), false,false);
				JobOrder jobOrder= jobOrderDAO.findById(Integer.parseInt(jobId), false,false);
				JobForTeacher jobForTeacher=jobForTeacherDAO.getJobForTeacherDetails(teacherDetail, jobOrder);
				
				boolean isSubstituteInstructionalJob = false;
				try{
					if(jobForTeacher!=null && jobForTeacher.getJobId().getJobCategoryMaster().getJobCategoryName().equalsIgnoreCase("Substitute Instructional Positions"))
						isSubstituteInstructionalJob=true;
				}catch(Exception e){}
				
				SchoolMaster schoolObj=null;
				try{
					if(jobForTeacher!=null && jobForTeacher.getRequisitionNumber()!=null){
						List<JobRequisitionNumbers> jobReqList=jobRequisitionNumbersDAO.findJobRequisitionNumbersByJobAndDistrict(jobOrder,jobForTeacher.getJobId().getDistrictMaster(),jobForTeacher.getRequisitionNumber());
						if(jobReqList.size()>0){
							try{
								schoolObj=jobReqList.get(0).getSchoolMaster();
							}catch(Exception e){
								e.printStackTrace();
							}
						}
					}
					if(schoolObj!=null){	
						schoolLocation=schoolObj.getSchoolName();
						schoolLocationWithCode=schoolObj.getSchoolName();
						try{
							locationCode=schoolObj.getLocationCode();
						}catch(Exception e){
							e.printStackTrace();
						}
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				if(isSubstituteInstructionalJob)
				{	
						schoolLocation = jobOrder.getDistrictMaster().getDistrictName();
						schoolLocationWithCode=jobOrder.getDistrictMaster().getDistrictName();
				}
				try{
					if(locationCode!=null && !locationCode.equals("")){
						schoolLocationWithCode=schoolLocationWithCode+" ("+locationCode+")";
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				try{
					if(jobForTeacher!=null && jobForTeacher.getOfferAccepted()!=null){
						if(offer){
							if(jobForTeacher.getOfferAccepted()){
								msg=""+Utility.getLocaleValuePropByKey("msgTeacherController1", locale)+" "+schoolLocationWithCode+".";
							}else{
								msg=""+Utility.getLocaleValuePropByKey("msgTeacherController2", locale)+" "+schoolLocationWithCode+". "+Utility.getLocaleValuePropByKey("msgTeacherController3", locale)+"";
							}
						}else{
							if(jobForTeacher.getOfferAccepted()){
								msg=""+Utility.getLocaleValuePropByKey("msgTeacherController1", locale)+" "+schoolLocationWithCode+". "+Utility.getLocaleValuePropByKey("msgTeacherController4", locale)+"";
							}else{
								msg=""+Utility.getLocaleValuePropByKey("msgTeacherController1", locale)+" "+schoolLocationWithCode+".";
							}
						}
					}else{
						if(offer){
							msg=""+Utility.getLocaleValuePropByKey("msgAcceptOrDecline7", locale)+" "+jobOrder.getJobTitle()+" at "+schoolLocationWithCode+". "+Utility.getLocaleValuePropByKey("msgAcceptOrDecline8", locale)+"";
						}else{
							msg=""+Utility.getLocaleValuePropByKey("msgAcceptOrDecline9", locale)+" "+jobOrder.getJobTitle()+" at "+schoolLocationWithCode+".";
						}
					}
				}catch(Exception e){}
				if(jobForTeacher!=null && jobForTeacher.getOfferAccepted()==null){
					StatusMaster statusMasterDCLN= statusMasterDAO.findStatusByShortName("dcln");
					
					List<JobWisePanelStatus> jobWisePanelStatusList=new ArrayList<JobWisePanelStatus>();
					SecondaryStatus secondaryStatus_temp=null;
					StatusMaster statusMaster_temp=null;
					if(statusFlag.equals("ss")){
						secondaryStatus_temp=secondaryStatusDAO.findById(Integer.parseInt(statusId), false,false);
					}else{
						statusMaster_temp=statusMasterDAO.findById(Integer.parseInt(statusId), false,false);
					}
					jobWisePanelStatusList=jobWisePanelStatusDAO.getPanel(jobOrder,secondaryStatus_temp,statusMaster_temp);
					List<UserMaster> panelAttendees_ForEmail=new ArrayList<UserMaster>();
					List<PanelSchedule> panelScheduleList= new ArrayList<PanelSchedule>();
					UserMaster schoolUser=null;
					PanelSchedule panelSchedule=null;
					if(jobWisePanelStatusList.size() == 1){
						panelScheduleList=panelScheduleDAO.findPanelSchedulesByJobWisePanelStatusListAndTeacher(jobWisePanelStatusList,teacherDetail);
						if(panelScheduleList.size()==1){
							
							if(panelScheduleList.get(0)!=null){
								panelSchedule=panelScheduleList.get(0);
								List<PanelAttendees> panelAttendees=new ArrayList<PanelAttendees>();
								panelAttendees=panelAttendeesDAO.getPanelAttendees(panelSchedule);
								if(panelAttendees.size() > 0){
									for(PanelAttendees attendees:panelAttendees){
										if(attendees!=null){
											panelAttendees_ForEmail.add(attendees.getPanelInviteeId());
											if(attendees.getPanelInviteeId().getEntityType()==3){
												schoolUser=attendees.getPanelInviteeId();
											}
										}
									}
								}
								
							}
						}
					}
					System.out.println("schoolUser:::::::::::"+schoolUser);
					ManageStatusAjax manageStatusAjax= new ManageStatusAjax();
					List<UserMailSend> userMailSendListFotTeacher =new ArrayList<UserMailSend>();
					 if(!offer){
						for(UserMaster userMaster : panelAttendees_ForEmail){
							String panelUserName="";
							if(userMaster.getLastName()!=null)
								panelUserName=userMaster.getFirstName()+" "+userMaster.getLastName();	 
							 else
								 panelUserName=userMaster.getFirstName();
							
							String teacherName="";
							if(teacherDetail!=null){
								teacherName=teacherDetail.getFirstName()+" "+teacherDetail.getLastName();
							}
							
							 UserMailSend userMailSendForPanelMember= new UserMailSend();
							 userMailSendForPanelMember.setSubject("Offer Declined "+schoolLocationWithCode+" by "+teacherName);
							 userMailSendForPanelMember.setSchoolLocation(schoolLocation);
							 userMailSendForPanelMember.setLocationCode(locationCode);// Set school location code
							 userMailSendForPanelMember.setFromUserName(panelUserName);
							 userMailSendForPanelMember.setJobTitle(jobOrder.getJobTitle());
							 userMailSendForPanelMember.setUserMaster(userMaster);
							 userMailSendForPanelMember.setTeacherDetail(teacherDetail);
							 userMailSendForPanelMember.setIsUserOrTeacherFlag(7);
							 userMailSendForPanelMember.setEmail(userMaster.getEmailAddress());
							 if(offer){
								 userMailSendForPanelMember.setFlag(1);
							 }else{
								 userMailSendForPanelMember.setFlag(0);
							 }
							 userMailSendListFotTeacher.add(userMailSendForPanelMember);
						}
						try{
							MailSendToDASA mst =new MailSendToDASA(userMailSendListFotTeacher,emailerService);
							Thread currentThread = new Thread(mst);
							currentThread.start();
						}catch(Exception e){e.printStackTrace();}
					
					 }
					
					if(jobForTeacher!=null){
						if(offer){
							UserMaster userMaster=userMasterDAO.findById(Integer.parseInt(userId),false,false);
							////////////////////////////////////////////////////////////
							jobForTeacher.setStatus(statusMasterVcom);
							jobForTeacher.setStatusMaster(statusMasterVcom);
							jobForTeacher.setUpdatedBy(userMaster);
							jobForTeacher.setUpdatedByEntity(userMaster.getEntityType());		
							jobForTeacher.setUpdatedDate(new Date());
							jobForTeacher.setLastActivity(statusMasterVcom.getStatus());
							jobForTeacher.setLastActivityDate(new Date());
							jobForTeacher.setOfferAccepted(true);
							jobForTeacher.setOfferAcceptedDate(new Date());
							if(userMaster!=null){
								jobForTeacher.setUserMaster(userMaster);
							}
							jobForTeacherDAO.updatePersistent(jobForTeacher);
							
							try{
								TeacherStatusHistoryForJob tSHJ= new TeacherStatusHistoryForJob();
								tSHJ.setTeacherDetail(teacherDetail);
								tSHJ.setJobOrder(jobOrder);
								tSHJ.setStatusMaster(statusMasterVcom);
								tSHJ.setStatus("S");
								tSHJ.setCreatedDateTime(new Date());
								tSHJ.setUserMaster(userMaster);
								teacherStatusHistoryForJobDAO.makePersistent(tSHJ);
							}catch(Exception e){}
							
							/* need to work */
							try{
								userMailSendListFotTeacher =new ArrayList<UserMailSend>();
								UserMailSend userMailSendForTeacher= new UserMailSend();
								userMailSendForTeacher.setEmail(teacherDetail.getEmailAddress());
								userMailSendForTeacher.setTeacherDetail(teacherDetail);
								DistrictMaster districtMaster = jobForTeacher.getJobId().getDistrictMaster();
								String bccAndSchoolUser[]=new String[1];
								try{
								if(schoolUser!=null){
									bccAndSchoolUser[0]=schoolUser.getEmailAddress();
								}
								}catch(Exception e){
									e.printStackTrace();
								}
								userMailSendForTeacher.setBccEmail(bccAndSchoolUser);
								
								userMailSendForTeacher.setSubject(Utility.getLocaleValuePropByKey("headOfferAccept", locale));
								userMailSendForTeacher.setIsUserOrTeacherFlag(6);
								try{
									if(jobForTeacher.getIsAffilated()!=null){
										userMailSendForTeacher.setInternal(jobForTeacher.getIsAffilated());
									}
								}catch(Exception e){}	
								userMailSendForTeacher.setSchoolLocation(schoolLocation);
								userMailSendForTeacher.setLocationCode(locationCode);// Set school location code
								
								userMailSendForTeacher.setJobTitle(jobOrder.getJobTitle());
								userMailSendForTeacher.setUserMaster(userMaster);
							 	userMailSendListFotTeacher.add(userMailSendForTeacher);
							 	userMailSendForTeacher.setEmailService(emailerService);
							 	
							 	
							 	Integer teacherId1 = teacherDetail.getTeacherId();
								TeacherPersonalInfo teacherPersonalInfos = teacherPersonalInfoDAO.findById(teacherId1, false, false);							
								TeacherDetail teacherPersonalInfoTemp = teacherDetail;							
								teacherPersonalInfoTemp.setFgtPwdDateTime(teacherPersonalInfos.getDob());//Dob		
								
								String teacherAddress="";
								try{
									if(teacherPersonalInfos.getAddressLine1()!=null){
										teacherAddress=teacherPersonalInfos.getAddressLine1().trim();
									}
									if(teacherPersonalInfos.getAddressLine2()!=null && !teacherPersonalInfos.getAddressLine2().equals("")){
										teacherAddress+=" "+teacherPersonalInfos.getAddressLine2().trim();
									}
									if(teacherPersonalInfos.getCityId()!=null){
										teacherAddress+=", "+teacherPersonalInfos.getCityId().getCityName();
									}
									if(teacherPersonalInfos.getStateId()!=null){
										teacherAddress+=", "+teacherPersonalInfos.getStateId().getStateName();
									}
									if(teacherPersonalInfos.getCountryId()!=null){
										teacherAddress+=", "+teacherPersonalInfos.getCountryId().getName();
									}
									if(teacherPersonalInfos.getZipCode()!=null){
										teacherAddress+=", "+teacherPersonalInfos.getZipCode();
									}
								}catch(Exception e){}
								teacherPersonalInfoTemp.setAuthenticationCode(teacherAddress);//Address
								teacherPersonalInfoTemp.setPhoneNumber(teacherPersonalInfos.getPhoneNumber());//Phone
								
								String emailAddresses="";

								List<DistrictKeyContact> districtKeyContactList = districtKeyContactDAO.findOfferAccepted(districtMaster);
								if(districtKeyContactList!=null){
									System.out.println("districtKeyContactList::::::::::::::::"+districtKeyContactList.size());
									int count=0;
									for (DistrictKeyContact districtKeyContact : districtKeyContactList) {
										if(count==1){
											emailAddresses+=",";
											count=0;
										}
										emailAddresses+=districtKeyContact.getKeyContactEmailAddress();
										count++;
									}
									int countUser=0;
									for(UserMaster userMasterObj : panelAttendees_ForEmail){
										if(countUser==1 || count!=0){
											emailAddresses+=",";
											countUser=0;
											count=0;
										}
										emailAddresses+=userMasterObj.getEmailAddress();
										countUser++;
									}
									System.out.println("emailAddresses:::::::->>>>>"+emailAddresses);
									UserMailSend userMailSendForTeacher3= new UserMailSend();
									userMailSendForTeacher3.setEmail(emailAddresses);
									userMailSendForTeacher3.setTeacherDetail(teacherPersonalInfoTemp);
									userMailSendForTeacher3.setSubject(Utility.getLocaleValuePropByKey("msgNotificationOfferAccepted", locale));
									userMailSendForTeacher3.setIsUserOrTeacherFlag(7);
									userMailSendForTeacher3.setLocationCode(locationCode); //Set school  location code
									userMailSendForTeacher3.setSchoolLocation(schoolLocation);
									userMailSendForTeacher3.setJobTitle(jobOrder.getJobTitle());
									userMailSendListFotTeacher.add(userMailSendForTeacher3);
								}
							 	manageStatusAjax.mailSendToUserByThread(userMailSendListFotTeacher,null,request);
							}catch(Exception e){
								e.printStackTrace();
							}
							///////////////////////////////////////////////////////////////////
						}else{
							UserMaster userMaster=null;
							try{
								if(userId!=null && !userId.equals("")){
									userMaster=userMasterDAO.findById(Integer.parseInt(userId),false,false);
								}
							}catch(Exception e){
								e.printStackTrace();
							}
							CandidateGridService cgService=new CandidateGridService();
							try{
								Map<Integer,List<String>> statusHistoryMap=new HashMap<Integer, List<String>>();
								Map<String,StatusMaster> statusMasterMap=new HashMap<String,StatusMaster>();
								Map<String,SecondaryStatus> secondaryStatusMap=new HashMap<String,SecondaryStatus>();
								List<TeacherStatusHistoryForJob> historyList=teacherStatusHistoryForJobDAO.findByTeacherAndJob(jobForTeacher.getTeacherId(),jobForTeacher.getJobId());
								List<TeacherStatusNotes> statusNoteList=teacherStatusNotesDAO.getStatusNoteAllList(jobForTeacher.getTeacherId(),jobForTeacher.getJobId());
								List<SecondaryStatus> lstTreeStructure = secondaryStatusDAO.findSecondaryStatusByJobCategoryHeadAndBranch(jobOrder);
								LinkedHashMap<String,String> statusNameMap=new LinkedHashMap<String,String>();
								Map<String,Integer> statusHistMap=new HashMap<String, Integer>();
								if(schoolUser!=null && isSubstituteInstructionalJob==false){
									try{
										int counter=0; 
										for(SecondaryStatus tree: lstTreeStructure)
										{
											if(tree.getSecondaryStatus()==null)
											{
												if(tree.getChildren().size()>0){
													counter=cgService.getAllStatusForHistory(statusMasterMap,secondaryStatusMap,tree.getChildren(),statusHistMap,statusNameMap,counter);
												}
											}
										}
									}catch(Exception e){}
									
									if(statusNoteList.size()>0)
									for (TeacherStatusNotes teacherStatusNotes : statusNoteList) {
										if((teacherStatusNotes.getStatusMaster()!=null && teacherStatusNotes.getStatusMaster().getStatusShortName().equals("vcomp"))||(teacherStatusNotes.getSecondaryStatus()!=null && teacherStatusNotes.getSecondaryStatus().getSecondaryStatusName().equals("Offer Ready"))){
											teacherStatusNotesDAO.makeTransient(teacherStatusNotes);
										}
									}
									boolean vComp=false;
									if(historyList.size()>0)
									for (TeacherStatusHistoryForJob teacherStatusHistoryForJob : historyList) {
										if((teacherStatusHistoryForJob.getStatusMaster()!=null && teacherStatusHistoryForJob.getStatusMaster().getStatusShortName().equals("vcomp"))||(teacherStatusHistoryForJob.getSecondaryStatus()!=null && teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusName().equals("Offer Ready"))){
											if(teacherStatusHistoryForJob.getStatusMaster()!=null && teacherStatusHistoryForJob.getStatusMaster().getStatusShortName().equals("vcomp")){
												vComp=true;	
											}
											teacherStatusHistoryForJobDAO.makeTransient(teacherStatusHistoryForJob);
										}
										
									}
									
									try{		
										historyList=teacherStatusHistoryForJobDAO.findByTeacherAndJob(jobForTeacher.getTeacherId(),jobForTeacher.getJobId());
										System.out.println("historyList::::::::::"+historyList.size());
										
										List<String> statusList=new ArrayList<String>();
										if(historyList.size()>0)
										for (TeacherStatusHistoryForJob teacherStatusHistoryForJob : historyList) {
											if(statusHistoryMap.get(teacherStatusHistoryForJob.getTeacherDetail().getTeacherId())==null){
												statusList=new ArrayList<String>();
												if(teacherStatusHistoryForJob.getStatusMaster()!=null){
													statusList.add(teacherStatusHistoryForJob.getStatusMaster().getStatusId()+"##0");
												}else{
													statusList.add("0##"+teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusId());
												}
												statusHistoryMap.put(teacherStatusHistoryForJob.getTeacherDetail().getTeacherId(),statusList);
											}else{
												statusList=statusHistoryMap.get(teacherStatusHistoryForJob.getTeacherDetail().getTeacherId());
												if(teacherStatusHistoryForJob.getStatusMaster()!=null){
													statusList.add(teacherStatusHistoryForJob.getStatusMaster().getStatusId()+"##0");
												}else{
													statusList.add("0##"+teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusId());
												}
												statusHistoryMap.put(teacherStatusHistoryForJob.getTeacherDetail().getTeacherId(),statusList);
											}
										}
									}catch(Exception e){}
									System.out.println("statusHistoryMap>:::::::::::"+statusHistoryMap.size());
									List<String> statusIds=statusHistoryMap.get(jobForTeacher.getTeacherId().getTeacherId());
									String schoolStatusId=cgService.getSchoolStatusId(statusIds, statusHistMap, statusNameMap);
									SecondaryStatus secondaryStatusObj=null;
									StatusMaster statusMasterObj=null;
									try{
										String statusIdMapValue[]=schoolStatusId.split("#");
										if(statusIdMapValue[0].equals("0")){
											secondaryStatusObj=secondaryStatusMap.get(schoolStatusId);
											jobForTeacher.setSecondaryStatus(secondaryStatusObj);
											if(vComp){
												StatusMaster statusMasterEcom=statusMasterDAO.findStatusByShortName("ecomp");
												jobForTeacher.setStatus(statusMasterEcom);
												jobForTeacher.setStatusMaster(null);
												jobForTeacher.setLastActivity(statusMasterEcom.getStatus());
												jobForTeacher.setLastActivityDate(new Date());
												if(userMaster!=null){
													jobForTeacher.setUserMaster(userMaster);
												}
											}
										}else{
											statusMasterObj=statusMasterMap.get(schoolStatusId);
										}
									}catch(Exception e){e.printStackTrace();}
									try{
										if(jobForTeacher!=null && jobForTeacher.getRequisitionNumber()!=null){
											List<JobRequisitionNumbers> jobReqList=jobRequisitionNumbersDAO.findJobRequisitionNumbersByJobAndDistrict(jobForTeacher.getJobId(),jobForTeacher.getJobId().getDistrictMaster(),jobForTeacher.getRequisitionNumber());
											if(jobReqList.size()>0){
												for (JobRequisitionNumbers jobReqObj : jobReqList) {
													try{
														if(schoolUser!=null && jobReqObj.getSchoolMaster().getSchoolId().equals(schoolUser.getSchoolId().getSchoolId()) && jobReqObj.getStatus()==1){
															jobReqObj.setStatus(0);
															jobRequisitionNumbersDAO.updatePersistent(jobReqObj);
															break;
														}
													}catch(Exception e){
														e.printStackTrace();
													}
												}
											}
										}
									}catch(Exception e){
										e.printStackTrace();
									}
									jobForTeacher.setOfferReady(null);
									jobForTeacher.setOfferMadeDate(new Date());
									jobForTeacher.setRequisitionNumber(null);
									jobForTeacher.setOfferAcceptedDate(new Date());
									jobForTeacher.setOfferAccepted(false);
									if(isSubstituteInstructionalJob)
									{
										jobForTeacher.setStatus(statusMasterDCLN);
										jobForTeacher.setStatusMaster(statusMasterDCLN);
										jobForTeacher.setLastActivity(statusMasterDCLN.getStatus());
										jobForTeacher.setLastActivityDate(new Date());
										if(userMaster!=null){
											jobForTeacher.setUserMaster(userMaster);
										}
									}
									jobForTeacherDAO.updatePersistent(jobForTeacher);
									
									if(isSubstituteInstructionalJob==false)
									{
											try{
										
											JobWisePanelStatus jobWisePanelStatus =jobWisePanelStatusDAO.findJobWisePanelStatusObj(jobForTeacher.getJobId());
											if(jobWisePanelStatus!=null){
												panelScheduleDAO.updatePanelSchedule(jobForTeacher.getTeacherId(),jobForTeacher.getJobId(),jobWisePanelStatus);
											}
										}catch(Exception e){e.printStackTrace();}
									}
									if(isSubstituteInstructionalJob==false)
									{
										SchoolWiseCandidateStatus swcsObj= new SchoolWiseCandidateStatus();
										swcsObj.setStatusMaster(statusMasterDCLN);
										swcsObj.setJobOrder(jobForTeacher.getJobId());
										swcsObj.setDistrictId(schoolUser.getDistrictId().getDistrictId());
										swcsObj.setSchoolId(schoolObj.getSchoolId());
										swcsObj.setUserMaster(schoolUser);		
										swcsObj.setCreatedDateTime(new Date());
										swcsObj.setTeacherDetail(jobForTeacher.getTeacherId());
										swcsObj.setStatus("A");
										swcsObj.setJobForTeacher(jobForTeacher);
										schoolWiseCandidateStatusDAO.makePersistent(swcsObj);
									}
								}
							}catch(Exception e){
								e.printStackTrace();
							}
						}
					}
				}
				System.out.println("msg:::"+msg);
				map.addAttribute("msgpage",msg);
				map.addAttribute("verifyFlag",2);
			}
		}catch (Exception e){
			e.printStackTrace();
		}
		return "verifyoffer";
	}
	
	@RequestMapping(value="/verifyoffer.do", method=RequestMethod.GET)
	public String doVerifyGET(ModelMap map,HttpServletRequest request){
		request.getSession();
		
		System.out.println(" verifyoffer.do >>>::::::::::::");
		try{
			String key = request.getParameter("key")==null?"":request.getParameter("key").trim();
			String offerUrl=Utility.getValueOfPropByKey("basePath")+"/verifyoffered.do?key="+key;
			String msg="";
			String schoolLocation="";
			String schoolLocationWithCode="";
			String locationCode="";
			if(key!=null && !key.equals("")){
				String keyValue=Utility.decodeBase64(key);
				
				System.out.println("keyValue:::"+keyValue);
				
				String teacherId=keyValue.split("##")[0];
				String jobId=keyValue.split("##")[1];
				String offerAccepted=keyValue.split("##")[2];
				String statusFlag=keyValue.split("##")[3];
				String statusId=keyValue.split("##")[4];
				String userId=keyValue.split("##")[5];
				boolean offer=false;
				if(offerAccepted!=null && offerAccepted.equals("1")){
					offer=true;
				}
				StatusMaster statusMasterVcom=statusMasterDAO.findStatusByShortName("vcomp");
				System.out.println("teacherId="+teacherId);
				System.out.println("offerAccepted="+offerAccepted);
				System.out.println("jobId="+jobId);
				
				TeacherDetail teacherDetail= teacherDetailDAO.findById(Integer.parseInt(teacherId), false,false);
				JobOrder jobOrder= jobOrderDAO.findById(Integer.parseInt(jobId), false,false);
				JobForTeacher jobForTeacher=jobForTeacherDAO.getJobForTeacherDetails(teacherDetail, jobOrder);
				
				boolean isSubstituteInstructionalJob = false;
				try{
					if(jobForTeacher!=null && jobForTeacher.getJobId().getJobCategoryMaster().getJobCategoryName().equalsIgnoreCase("Substitute Instructional Positions"))
						isSubstituteInstructionalJob=true;
				}catch(Exception e){}
				
				SchoolMaster schoolObj=null;
				try{
					if(jobForTeacher!=null && jobForTeacher.getRequisitionNumber()!=null){
						List<JobRequisitionNumbers> jobReqList=jobRequisitionNumbersDAO.findJobRequisitionNumbersByJobAndDistrict(jobOrder,jobForTeacher.getJobId().getDistrictMaster(),jobForTeacher.getRequisitionNumber());
						if(jobReqList.size()>0){
							try{
								schoolObj=jobReqList.get(0).getSchoolMaster();
							}catch(Exception e){
								e.printStackTrace();
							}
						}
					}
					if(schoolObj!=null){	
						schoolLocation=schoolObj.getSchoolName();
						schoolLocationWithCode=schoolObj.getSchoolName();
						try{
							locationCode=schoolObj.getLocationCode();
						}catch(Exception e){
							e.printStackTrace();
						}
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				if(isSubstituteInstructionalJob)
				{	
						schoolLocation = jobOrder.getDistrictMaster().getDistrictName();
						schoolLocationWithCode=jobOrder.getDistrictMaster().getDistrictName();
				}
				try{
					if(locationCode!=null && !locationCode.equals("")){
						schoolLocationWithCode=schoolLocationWithCode+" ("+locationCode+")";
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				boolean firstTime=false;
				try{
					if(jobForTeacher!=null && jobForTeacher.getOfferAccepted()!=null){
						if(offer){
							if(jobForTeacher.getOfferAccepted()){
								msg=""+Utility.getLocaleValuePropByKey("msgTeacherController1", locale)+" "+schoolLocationWithCode+".";
							}else{
								msg=""+Utility.getLocaleValuePropByKey("msgTeacherController2", locale)+" "+schoolLocationWithCode+". "+Utility.getLocaleValuePropByKey("msgTeacherController3", locale)+"";
							}
						}else{
							if(jobForTeacher.getOfferAccepted()){
								msg=""+Utility.getLocaleValuePropByKey("msgTeacherController1", locale)+" "+schoolLocationWithCode+". "+Utility.getLocaleValuePropByKey("msgTeacherController4", locale)+"";
							}else{
								msg=""+Utility.getLocaleValuePropByKey("msgTeacherController2", locale)+" "+schoolLocationWithCode+".";
							}
						}
					}else{
						firstTime=true;
						if(offer){
							msg=""+Utility.getLocaleValuePropByKey("msgAcceptOrDecline7", locale)+" "+jobOrder.getJobTitle()+" at "+schoolLocationWithCode+". "+Utility.getLocaleValuePropByKey("msgAcceptOrDecline8", locale)+"";
						}else{
							msg=""+Utility.getLocaleValuePropByKey("msgAcceptOrDecline9", locale)+" "+jobOrder.getJobTitle()+" at "+schoolLocationWithCode+".";
						}
					}
				}catch(Exception e){}
				if(firstTime){
					if(offerAccepted!=null && offerAccepted.equals("1")){
						msg=Utility.getLocaleValuePropByKey("msgTeacherController5", locale);
					}else{
						msg=Utility.getLocaleValuePropByKey("msgTeacherController6", locale);
					}
					map.addAttribute("verifyFlag",1);
					map.addAttribute("msgpage","");
				}else{
					map.addAttribute("verifyFlag",2);
					map.addAttribute("msgpage",msg);
				}
			}
			map.addAttribute("msg",msg);
			map.addAttribute("offerUrl",offerUrl);
			System.out.println("offerUrl:::"+offerUrl);
			map.addAttribute("offerUrl",offerUrl);
		}catch (Exception e){
			e.printStackTrace();
		}
		return "verifyoffer";
	}
	@RequestMapping(value="/noresponseresendoffer.do", method=RequestMethod.GET)
	public String doNoResponseReSendOfferReady(ModelMap map,HttpServletRequest request)
	{	
		PrintOnConsole.debugPrintln("Calling doNoResponseReSendOfferReady ... noresponseresendoffer.do");
		try{
			String key = request.getParameter("key")==null?"":request.getParameter("key").trim();
			if(key!=null && !key.equals(""))
			{
				String keyValue=Utility.decodeBase64(key);
				PrintOnConsole.debugPrintln("keyValue "+keyValue);
				if(keyValue!=null && !keyValue.equalsIgnoreCase(""))
				{
					String arr[]=keyValue.split("##");
					
					String teacherId="";
					int iTeacherId=0;
					if(arr.length > 0)
					{
						teacherId=arr[0];
						iTeacherId=Utility.getIntValue(teacherId);
					}
					
					String jobId="";
					int ijobId=0;
					if(arr.length > 1)
					{
						jobId=arr[1];
						ijobId=Utility.getIntValue(jobId);
					}
					
					String userId="";
					int iuserId=0;
					if(arr.length > 2)
					{
						userId=arr[2];
						iuserId=Utility.getIntValue(userId);
					}
					
					String sSchoolName="";
					if(arr.length > 3)
						sSchoolName=arr[3];
					
					if(iTeacherId > 0 && ijobId > 0 && iuserId > 0)
					{
						try{
							
							TeacherDetail teacherDetail=teacherDetailDAO.findById(iTeacherId, false, false);
							JobOrder jobOrder=jobOrderDAO.findById(ijobId, false, false);
							UserMaster userMaster=userMasterDAO.findById(iuserId, false, false);
							
							DemoScheduleMailThread dsmt=new DemoScheduleMailThread();
							if(teacherDetail!=null && jobOrder!=null && userMaster!=null && jobOrder.getDistrictMaster()!=null)
							{
								dsmt.setEmailerService(emailerService);
								
								dsmt.setMailto(teacherDetail.getEmailAddress());
								dsmt.setMailsubject(Utility.getLocaleValuePropByKey("msgOfferLetter", locale));
								
								int iUserMasterId=userMaster.getUserId();
								String sJobTitle=jobOrder.getJobTitle();
								
								SecondaryStatus  secondaryStatusObj =secondaryStatusDAO.findSSByDID(jobOrder.getDistrictMaster(),"Offer Ready");
								int iSecondaryStatusId=0;
								if(secondaryStatusObj!=null)
									iSecondaryStatusId=secondaryStatusObj.getSecondaryStatusId();
								
								PrintOnConsole.debugPrintln("Get Secondary Status for [Offer Ready] and SSID is "+iSecondaryStatusId);
								PrintOnConsole.debugPrintln("TeacherId "+iTeacherId +" jobId "+ijobId +"userId"+ userId +" sSchoolName "+sSchoolName);
								
								String sAcceptParameters = Utility.encodeInBase64(teacherDetail.getTeacherId()+"##"+jobOrder.getJobId()+"##"+"1##ss##"+iSecondaryStatusId+"##"+iUserMasterId);
								String sDeclineParameters = Utility.encodeInBase64(teacherDetail.getTeacherId()+"##"+jobOrder.getJobId()+"##"+"0##ss##"+iSecondaryStatusId+"##"+iUserMasterId);
								
								String sAcceptURL=Utility.getValueOfPropByKey("basePath")+"/verifyoffer.do?key="+sAcceptParameters;
								String sDeclineURL=Utility.getValueOfPropByKey("basePath")+"/verifyoffer.do?key="+sDeclineParameters;
								boolean isSubstituteInstructionalJob = false;
								try{
									if(jobOrder!=null && jobOrder.getJobCategoryMaster().getJobCategoryName().equalsIgnoreCase("Substitute Instructional Positions"))
										isSubstituteInstructionalJob=true;
								}catch(Exception e){}
								
								String sEmailContent=MailText.getSecondOfferLetter(teacherDetail,sJobTitle,sSchoolName,sAcceptURL,sDeclineURL,isSubstituteInstructionalJob);
								
								dsmt.setMailcontent(sEmailContent);
								
								PrintOnConsole.debugPrintln("to "+teacherDetail.getEmailAddress()+" Subject :: Offer Letter");
								PrintOnConsole.debugPrintln("sEmailContent "+sEmailContent);
								
								
								dsmt.start();
							}
							else
							{
								PrintOnConsole.debugPrintln("Could not Sent Email ... because  teacherDetail!=null && jobOrder!=null && userMaster!=null && jobOrder.getDistrictMaster()!=null not fullfill condition");
							}
							
							}catch(Exception e){
							e.printStackTrace();
							}
					}
					else
					{
						PrintOnConsole.debugPrintln("Could not Sent Email ... because iTeacherId > 0 && ijobId > 0 && iuserId > 0 not fullfill condition");
					}
				}
				else
				{
					PrintOnConsole.debugPrintln("keyValue is null or Blank");
				}
			}
		}
			catch (Exception e) {
				// TODO: handle exception
			}
			String sSuccessMsg=Utility.getLocaleValuePropByKey("msgTeacherController7", locale);
			map.addAttribute("sSuccessMsg",sSuccessMsg);
			return "noresponseresendoffer";
		}
	

	@RequestMapping(value="/sendtosapemail.do", method=RequestMethod.GET)
	public String doSendToSapGET(ModelMap map,HttpServletRequest request){
		request.getSession();
		System.out.println(" sendtosapemail.do New2::::::::::::");
		try{
				
			List<String> ccEmailAddress		=	new ArrayList<String>();
	    	List<String> bccEmailAddress	=	new ArrayList<String>();
	    	List<PanelSchedule> pSList		= 	new ArrayList<PanelSchedule>();
	    	String principalEmailAddress	=	"";
	    	String principalName			=	"";
	    	String sCandidateName			=	"";
	    	String requisitionNumber; 

			String jobForTeacherId 		= 	request.getParameter("id")==null?"":request.getParameter("id").trim();
			System.out.println("jobForTeacherId:::::"+jobForTeacherId);
			JobForTeacher jobForTeacher = 	jobForTeacherDAO.findById(new Long(jobForTeacherId), false, false);
			
			//requisitionNumber 			= 	jobForTeacher.getRequisitionNumber()==null?"":jobForTeacher.getRequisitionNumber();

			//System.out.println("requisitionNumber:::::"+requisitionNumber);
			
			// ********** Start delta process **********
			StringBuffer sbEmail=new StringBuffer();
			boolean bExToIn=true;
			try {
				DistrictMaster districtMaster=null;
				if(jobForTeacher!=null  && jobForTeacher.getJobId()!=null && jobForTeacher.getJobId().getDistrictMaster()!=null)
				{
					districtMaster=jobForTeacher.getJobId().getDistrictMaster();
					
					System.out.println("Start update process for External to Internal of Source PNR");
		     	 	//Status
	 	 			List <StatusMaster> statusMasterList=statusMasterDAO.findAllStatusMaster();
	 				Map<String, StatusMaster> mapStatus = new HashMap<String, StatusMaster>();
	 				for (StatusMaster statusMaster : statusMasterList) {
	 					mapStatus.put(statusMaster.getStatusShortName(),statusMaster);
	 				}
	 				
	 				String sNoUpdateExToIn="<BR>"+Utility.getLocaleValuePropByKey("msgTeacherController8", locale)+"<BR>";
		     	 	if(bExToIn)
		     	 	{
		     	 		
		     	 		TeacherDetail teacherDetail =jobForTeacher.getTeacherId();
		     	 		List<TeacherDetail> lstTeacherExToIn=new ArrayList<TeacherDetail>();
		     	 		lstTeacherExToIn.add(teacherDetail);
		     	 		
		     	 		TeacherPersonalInfo teacherPersonalInfo=teacherPersonalInfoDAO.findTeacherPersonalInfoByTeacher(teacherDetail);
		     	 		if(teacherPersonalInfo!=null)
     	 				{
		     	 			EmployeeMaster employeeMaster=null;
			     	 		if(teacherPersonalInfo!=null && teacherPersonalInfo.getEmployeeNumber()!=null && !teacherPersonalInfo.getEmployeeNumber().equals(""))
			     	 			employeeMaster=employeeMasterDAO.getByEmployeeCodeANdDID(teacherPersonalInfo.getEmployeeNumber(), districtMaster,"0");
			     	 		if(employeeMaster==null && teacherPersonalInfo.getFirstName()!=null && !teacherPersonalInfo.getFirstName().equalsIgnoreCase("") && teacherPersonalInfo.getLastName()!=null && !teacherPersonalInfo.getLastName().equalsIgnoreCase("") && teacherPersonalInfo.getSSN()!=null && !teacherPersonalInfo.getSSN().equalsIgnoreCase(""))
			     	 		{
     	 						String sTempFirstName="",sTempLastName="",sTempSSN="";
     	 						sTempFirstName=teacherPersonalInfo.getFirstName();
     	 						sTempLastName=teacherPersonalInfo.getLastName();
     	 						sTempSSN=Utility.decodeBase64(teacherPersonalInfo.getSSN());
     	 						sTempSSN=sTempSSN.substring(sTempSSN.length()-4, sTempSSN.length());
     	 						employeeMaster=employeeMasterDAO.getByNameSSNAndDID(sTempSSN, sTempFirstName, sTempLastName, districtMaster,"0");
			     	 		}
			     	 		
			     	 		if(employeeMaster!=null)
			     	 		{
			     	 			employeeMaster.setFECode("1");
			     	 			try {
			     	 				SessionFactory sessionFactory=jobForTeacherDAO.getSessionFactory();
				    				StatelessSession statelesSsession = sessionFactory.openStatelessSession();
				    		 	    Transaction txOpen =statelesSsession.beginTransaction();
									statelesSsession.update(employeeMaster);
									txOpen.commit();
									statelesSsession.close();
									
									sbEmail.append("<BR>"+Utility.getLocaleValuePropByKey("msgTeacherController9", locale)+"<BR>");
							 	    
									sbEmail.append("<Table border='1' cellpadding='5' cellspacing='0'>");
								 	   
									sbEmail.append("<TR>");
									sbEmail.append("<TD>"+Utility.getLocaleValuePropByKey("msgTeacherController10", locale)+"</TD>");
									sbEmail.append("<TD>"+Utility.getLocaleValuePropByKey("lblFname", locale)+"</TD>");
									sbEmail.append("<TD>"+Utility.getLocaleValuePropByKey("lblLname", locale)+"</TD>");
									sbEmail.append("<TD>"+Utility.getLocaleValuePropByKey("lblEmailAddress", locale)+"</TD>");
									sbEmail.append("<TD>"+Utility.getLocaleValuePropByKey("lblSSN", locale)+"</TD>");
									sbEmail.append("<TD>"+Utility.getLocaleValuePropByKey("msgTeacherController11", locale)+"</TD>");
									sbEmail.append("<TD>"+Utility.getLocaleValuePropByKey("msgTeacherController12", locale)+"</TD>");
									sbEmail.append("</TR>");
									
									sbEmail.append("<TR>");
									sbEmail.append("<TD>"+employeeMaster.getEmployeeId()+"</TD>");
									sbEmail.append("<TD>"+employeeMaster.getFirstName()+"</TD>");
									sbEmail.append("<TD>"+employeeMaster.getLastName()+"</TD>");
									sbEmail.append("<TD>"+employeeMaster.getEmailAddress()+"</TD>");
									sbEmail.append("<TD>"+employeeMaster.getSSN()+"</TD>");
									sbEmail.append("<TD>"+employeeMaster.getEmployeeCode()+"</TD>");
									sbEmail.append("<TD>"+employeeMaster.getFECode()+"</TD>");
									sbEmail.append("</TR>");
									
									sbEmail.append("</Table><BR>");
								 	   
								} catch (Exception e) {
									// TODO: handle exception
								}
			     	 			
			     	 		}
			     	 		else
			     	 		{
			     	 			sbEmail.append("<BR>"+Utility.getLocaleValuePropByKey("msgTeacherController13", locale)+"<BR>");
			     	 		}
     	 				}
		     	 		
		     	 		
		     	 		if(lstTeacherExToIn!=null & lstTeacherExToIn.size()>0)
		     	 		{
		     	 			
		     	 			//EPI
		     	 			Map<Integer, TeacherAssessmentStatus> mapTAS=getMapTAS(lstTeacherExToIn);
		     	 			Map<Integer, Boolean> mapTeacherPortfolioStatus=teacherPortfolioStatusDAO.findActivePortfolioStatus(lstTeacherExToIn);
		     	 			
		     	 			List<JobForTeacher> lstJFTExToIn=new ArrayList<JobForTeacher>();
		     	 			lstJFTExToIn=jobForTeacherDAO.getJFTByTeacherIDExIn(teacherDetail, districtMaster);
		     	 			if(lstJFTExToIn!=null && lstJFTExToIn.size() > 0)
		     	 			{
		     	 				// Need to work for External To Internal
		     	 				String sReturnValueExToIn=processForExternalOrInternalPNR(lstTeacherExToIn,lstJFTExToIn,mapTAS,mapTeacherPortfolioStatus,mapStatus,1,districtMaster);
		     	 				sbEmail.append(sReturnValueExToIn);
		     	 				
		     	 			}
		     	 			else
		     	 			{
		     	 				//System.out.println("4 lstJFTExToIn is null");
		     	 				sbEmail.append(sNoUpdateExToIn);
		     	 			}
		     	 		}
		     	 		else
		     	 		{
		     	 			//System.out.println("5 lstTeacherExToIn is null");
		     	 			sbEmail.append(sNoUpdateExToIn);
		     	 		}
		     	 	}
		     	 	else
		     	 	{
		     	 		//System.out.println("6 No any updated for External To Internal");
		     	 		sbEmail.append(sNoUpdateExToIn);
		     	 	}
		     	 	
		     	 	// Update EmployeeMaster
		     	 	
		     	 	
				}
				
				
			} catch (Exception e) {
				// TODO: handle exception
			}
			finally
			{
				String sEmailSubject=Utility.getLocaleValuePropByKey("msgPNRHireJFT", locale);
				String sEmailBodyText=sbEmail.toString();
				
				DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
				dsmt.setEmailerService(emailerService);
				dsmt.setMailfrom("admin@netsutra.com");
				dsmt.setMailto("teachermatchemaillog@gmail.com");
				dsmt.setMailsubject(sEmailSubject);
				dsmt.setMailcontent(sEmailBodyText);
				
				System.out.println("sEmailSubject "+sEmailSubject);
				System.out.println("sEmailBodyText "+sEmailBodyText);
				
				try {
					dsmt.start();	
				} catch (Exception e) {}
			}
			// ********** End delta process **********
			
			try{
				// Get List of BCC Mail from districtKeyContact table				
				List<DistrictKeyContact> districtKeyContacts=new ArrayList<DistrictKeyContact>();
				districtKeyContacts=districtKeyContactDAO.findByContactType(jobForTeacher.getJobId().getDistrictMaster(), "Sent to SAP");
				if(districtKeyContacts.size() > 0){
					for(DistrictKeyContact districtKeyContactsTemp:districtKeyContacts){
						bccEmailAddress.add(districtKeyContactsTemp.getKeyContactEmailAddress());
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			if(jobForTeacher.getTeacherId().getFirstName()!=null && !jobForTeacher.getTeacherId().getFirstName().equalsIgnoreCase(""))
				sCandidateName=jobForTeacher.getTeacherId().getFirstName();
			
			if(jobForTeacher.getTeacherId().getLastName()!=null && !jobForTeacher.getTeacherId().getLastName().equalsIgnoreCase(""))
				sCandidateName=sCandidateName+" "+jobForTeacher.getTeacherId().getLastName();

			pSList	=	panelScheduleDAO.findPanelSchedules(jobForTeacher.getTeacherId(),jobForTeacher.getJobId());
			
			System.out.println("::::::::::::::::pSList::::::::::::::::"+pSList.size());
			if(pSList!=null && pSList.size()>0){
				System.out.println(":::::::::Inside Panel ::::::::::::");
				Criterion criteria = null;
				List<PanelAttendees> panelAttendeesList = null;
				criteria= Restrictions.eq("panelSchedule", pSList.get(0));
				panelAttendeesList 		= 	panelAttendeesDAO.findByCriteria(criteria);

				for(PanelAttendees panelAttendees:panelAttendeesList){
					if(panelAttendees.getPanelInviteeId().getEntityType() == 3){
						
						if(panelAttendees.getPanelInviteeId().getFirstName()!=null && !panelAttendees.getPanelInviteeId().getFirstName().equalsIgnoreCase(""))
							principalName=panelAttendees.getPanelInviteeId().getFirstName();
						
						if(panelAttendees.getPanelInviteeId().getLastName()!=null && !panelAttendees.getPanelInviteeId().getLastName().equalsIgnoreCase(""))
							principalName=principalName+" "+panelAttendees.getPanelInviteeId().getLastName();
					}
					principalEmailAddress	=	panelAttendees.getPanelInviteeId().getEmailAddress();
					ccEmailAddress.add(principalEmailAddress);
				}
			}else{
				System.out.println(":::::::::::::Not Panel:::");
				List<TeacherStatusHistoryForJob> historyForJobs=teacherStatusHistoryForJobDAO.findByTeacherandJob(jobForTeacher.getTeacherId(),jobForTeacher.getJobId());
				if(historyForJobs.size()>0)
				for (TeacherStatusHistoryForJob teacherStatusHistoryForJob : historyForJobs){
					if(teacherStatusHistoryForJob.getSecondaryStatus()!=null && teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusName().equals("Offer Ready")){
						System.out.println(":::::::Offer Ready:::::::::");
						UserMaster usermaster=teacherStatusHistoryForJob.getUserMaster();
						if(usermaster!=null){
							if(usermaster.getFirstName()!=null && !usermaster.getFirstName().equalsIgnoreCase(""))
							principalName=usermaster.getFirstName();
							if(usermaster.getLastName()!=null && !usermaster.getLastName().equalsIgnoreCase(""))
							principalName=principalName+" "+usermaster.getLastName();
							ccEmailAddress.add(usermaster.getEmailAddress());
						}
					}
				}
			}
			String mailContent	=	MailText.PNRSentToSap(jobForTeacher,principalName);
			String mailSubject	=	"Hiring Status for "+sCandidateName;
			System.out.println("::::::mailSubject:::::::::"+mailSubject);
			
			String mailTo="CArcher@dadeschools.net";
			if(jobForTeacher.getJobId().getJobId()!=6544 && jobForTeacher.getJobId().getJobId()!=6545)
				mailTo=jobForTeacher.getTeacherId().getEmailAddress() == null?"":jobForTeacher.getTeacherId().getEmailAddress().trim();
			
			bccEmailAddress.add("hanzala@netsutra.com");
			
			System.out.println("mailTo::::"+mailTo +" ccEmailAddress::"+ccEmailAddress.size()+" bccEmailAddress::"+bccEmailAddress.size());
			if(ccEmailAddress!=null && ccEmailAddress.size() > 0){
				System.out.println("::::::::::::Mails Send to Teacher::::::::::::");
				emailerService.sendMailAsHTMLByListToCc(mailTo,bccEmailAddress,ccEmailAddress,mailSubject,mailContent);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		//return "doSendToSapGETDemo";
		return null;
	}
	
	public Map<Integer, TeacherAssessmentStatus> getMapTAS(List<TeacherDetail> lstTeacherDetails)
	{
		Map<Integer, TeacherAssessmentStatus> mapTAS=new HashMap<Integer, TeacherAssessmentStatus>();
		List<TeacherAssessmentStatus> lstTAS = new ArrayList<TeacherAssessmentStatus>();
			lstTAS=teacherAssessmentStatusDAO.findByAllTeacherAssessStatusByTeacher(lstTeacherDetails);
			if(lstTAS!=null && lstTAS.size()>0)
			{
				for(TeacherAssessmentStatus teacherAssessmentStatus:lstTAS)
				{
				if(teacherAssessmentStatus!=null)
				{
					if(mapTAS.get(teacherAssessmentStatus.getTeacherDetail().getTeacherId())==null)
						mapTAS.put(teacherAssessmentStatus.getTeacherDetail().getTeacherId(),teacherAssessmentStatus);
				}
				}
			}
			return mapTAS;
	}
	
	public String processForExternalOrInternalPNR(List<TeacherDetail> teacherDetails, List<JobForTeacher> lstJFT,Map<Integer, TeacherAssessmentStatus> mapTAS,Map<Integer, Boolean> mapTeacherPortfolioStatus,Map<String, StatusMaster> mapStatus,int isAffilated,DistrictMaster districtMaster)
	{
		System.out.println("PNR Start Calling processForExternalOrInternal "+isAffilated);
		StringBuffer sb=new StringBuffer("");
		if(lstJFT!=null && lstJFT.size() > 0)
 		{
			SessionFactory sessionFactory=jobForTeacherDAO.getSessionFactory();
			StatelessSession statelesSsession = sessionFactory.openStatelessSession();
	 	    Transaction txOpen =statelesSsession.beginTransaction();
	 	    
 			//System.out.println("lstJFT Size "+lstJFT.size());
 			
 			//JSI
 			List<JobOrder> jobOrders=new ArrayList<JobOrder>();
 			for(JobForTeacher jobForTeacher:lstJFT)
 				jobOrders.add(jobForTeacher.getJobId());
 			
 			TeacherAssessmentStatus teacherAssessmentStatusJSI = null;
 			Map<Integer, Integer> mapAssess = new HashMap<Integer, Integer>();
 			Map<Integer, TeacherAssessmentStatus> mapJSI = new HashMap<Integer, TeacherAssessmentStatus>();
 			List<TeacherAssessmentStatus> lstJSI =new ArrayList<TeacherAssessmentStatus>(); 
 			List<AssessmentJobRelation> assessmentJobRelations1=assessmentJobRelationDAO.findRelationByJobOrder(jobOrders);
 			List<AssessmentDetail> adList=new ArrayList<AssessmentDetail>();
 			if(assessmentJobRelations1.size()>0){
 				for(AssessmentJobRelation ajr: assessmentJobRelations1){
 					mapAssess.put(ajr.getJobId().getJobId(),ajr.getAssessmentId().getAssessmentId());
 					adList.add(ajr.getAssessmentId());
 				}
 				if(adList.size()>0)
 				lstJSI = teacherAssessmentStatusDAO.findJSITakenByAssessMentListAndTIDS(teacherDetails,adList);
 			}
 			for(TeacherAssessmentStatus ts : lstJSI){
 				mapJSI.put(ts.getAssessmentDetail().getAssessmentId(), ts);
 			}
 			//History Hire Status
 			List<TeacherStatusHistoryForJob> hiredTSHFJList = new ArrayList<TeacherStatusHistoryForJob>();
 			hiredTSHFJList=teacherStatusHistoryForJobDAO.findHireTeachersWithDistrictListAndTeacherList(teacherDetails, districtMaster);
 			Map<String,TeacherStatusHistoryForJob> mapTSHFJ=new HashMap<String, TeacherStatusHistoryForJob>();
			if(hiredTSHFJList!=null && hiredTSHFJList.size() > 0)
			{
				for(TeacherStatusHistoryForJob tSHObj : hiredTSHFJList)
					if(tSHObj!=null)
					{
						mapTSHFJ.put(tSHObj.getTeacherDetail().getTeacherId()+"_"+tSHObj.getJobOrder().getJobId(),tSHObj);
						//System.out.println(tSHObj.getTeacherDetail().getTeacherId()+"_"+tSHObj.getJobOrder().getJobId());
					}
			}
 			//
			
			
			 StatusMaster statusMaster_Rejected=new StatusMaster();
			 statusMaster_Rejected=statusMasterDAO.findStatusByShortName("rem");
		 	    
			UserMaster userMaster=new UserMaster();
	 	    userMaster.setUserId(1);
 			
	 	   String sStatusNote=Utility.getLocaleValuePropByKey("msgTeacherController12", locale);
	 	    String sStatusNoteReject= Utility.getLocaleValuePropByKey("msgTeacherController11", locale) ;
			CandidateGridService cgService=new CandidateGridService();
			
			if(isAffilated==0)
	 	   		sb.append("<BR>"+Utility.getLocaleValuePropByKey("msgInternalExternalProcess", locale)+"</BR>");
	 	   	else if(isAffilated==1 || isAffilated==3)
	 	   		sb.append("<BR>"+Utility.getLocaleValuePropByKey("msgExternalInternalProcess", locale)+"</BR>");
			
		   sb.append("<Table border='1' cellpadding='5' cellspacing='0'>");
	 	   sb.append("<TR>");
	 	   sb.append("<TD>"+Utility.getLocaleValuePropByKey("lblSNo", locale)+"</TD>");
	 	   sb.append("<TD>"+Utility.getLocaleValuePropByKey("msgJobForTeacherId", locale)+"</TD>");
	 	   sb.append("<TD>"+Utility.getLocaleValuePropByKey("msgTeacherId", locale)+"</TD>");
	 	   sb.append("<TD>"+Utility.getLocaleValuePropByKey("lblEmailAddress", locale)+"</TD>");
	 	   sb.append("<TD>"+Utility.getLocaleValuePropByKey("msgJobDetails", locale)+"</TD>");
	 	   sb.append("<TD>"+Utility.getLocaleValuePropByKey("headNot", locale)+"</TD>");
	 	   sb.append("</TR>");
		 	   
	 	   int iCounter=0;
		 	   
 			for(JobForTeacher jobForTeacher:lstJFT)
 			{
 				iCounter++;
 				//System.out.println("Try to Update JFT "+jobForTeacher.getJobForTeacherId());
 				/*TeacherDetail teacherDetail = jobForTeacher.getTeacherId();
 				JobOrder jobOrder =  jobForTeacher.getJobId();
 				StatusMaster statusMasterHire=jobForTeacher.getStatus();*/
 				
 				
 				boolean bPortfolioStatus=false;
 				if(mapTeacherPortfolioStatus.get(jobForTeacher.getTeacherId().getTeacherId())!=null)
 					bPortfolioStatus=mapTeacherPortfolioStatus.get(jobForTeacher.getTeacherId().getTeacherId());
 				
 				TeacherAssessmentStatus teacherAssessmentStatus=null;
 				if(mapTAS.get(jobForTeacher.getTeacherId().getTeacherId())!=null)
 					teacherAssessmentStatus=mapTAS.get(jobForTeacher.getTeacherId().getTeacherId());
 				
 				StatusMaster statusMasterCIV=getLatestUpdate(jobForTeacher, mapAssess, teacherAssessmentStatusJSI, mapJSI, bPortfolioStatus, mapStatus, teacherAssessmentStatus,isAffilated);
 				
 				if(statusMasterCIV!=null)
 				{
 						String sStatusShortName="";
	 					jobForTeacher.setIsAffilated(isAffilated);
	 					if(jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("comp") || jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("icomp") || jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("vlt"))
	 					{
	 						jobForTeacher.setStatus(statusMasterCIV);
		 					if(jobForTeacher.getSecondaryStatus()!=null && cgService.priSecondaryStatusCheck(jobForTeacher.getStatus(),jobForTeacher.getSecondaryStatus()))
								jobForTeacher.setStatusMaster(null);
							else
								jobForTeacher.setStatusMaster(statusMasterCIV);
		 					
		 					sStatusShortName=statusMasterCIV.getStatusShortName();
	 					}
						txOpen =statelesSsession.beginTransaction();
						statelesSsession.update(jobForTeacher);
						txOpen.commit();
						
						sb.append("<TR>");
						sb.append("<TD>"+iCounter+"</TD>");
				 	   	sb.append("<TD>"+jobForTeacher.getJobForTeacherId()+"</TD>");
				 	   	sb.append("<TD>"+jobForTeacher.getTeacherId().getTeacherId()+"</TD>");
				 	   	sb.append("<TD>"+jobForTeacher.getTeacherId().getEmailAddress()+"</TD>");
				 	   	sb.append("<TD>"+jobForTeacher.getJobId()+"</TD>");
				 	   	
				 	   sb.append("<TD>");
				 	   if(isAffilated==1)
				 		  sb.append(Utility.getLocaleValuePropByKey("msgExternalToInternal", locale));
				 	   	
				 	   	if(!sStatusShortName.equalsIgnoreCase(""))
				 	   		sb.append(" , "+sStatusShortName);
				 	   sb.append("</TD>");
				 	   
				 	   	sb.append("</TR>");
 				}
 			}
 			statelesSsession.close();
 			sb.append("</Table>");
 		}
		System.out.println("PNR End Calling processForExternalOrInternal");
		
		return sb.toString();
	}
	
	
	public StatusMaster getLatestUpdate(JobForTeacher jobForTeacher,Map<Integer, Integer> mapAssess,TeacherAssessmentStatus teacherAssessmentStatusJSI,Map<Integer, TeacherAssessmentStatus>mapJSI,boolean tPortfolioStatus,Map<String, StatusMaster>mapStatus,TeacherAssessmentStatus teacherBaseAssessmentStatus,int isAffilated) 
	{
		
		jobForTeacher.setIsAffilated(isAffilated);
		TeacherDetail teacherDetail = jobForTeacher.getTeacherId();
		JobOrder jobOrder =  jobForTeacher.getJobId();
		StatusMaster statusMaster =  mapStatus.get("icomp");
		try{
			if(mapAssess!=null){
				Integer assessmentId=mapAssess.get(jobForTeacher.getJobId().getJobId());
				teacherAssessmentStatusJSI = mapJSI.get(assessmentId);
			}
			 boolean epiInternal=false;
			 int isPortfolio=0;
			 if(jobOrder.getIsPortfolioNeeded()!=null){
				 if(jobOrder.getIsPortfolioNeeded())
					 isPortfolio=1;
			 }
			int offerEPI=0;
			int offerJSI=0;
			int internalFlag=0;
			/*try{
				if(districtMasterInternal!=null)
				if(internalITRA!=null && districtMasterInternal.getDistrictId().equals(jobOrder.getDistrictMaster().getDistrictId())){
					internalFlag=1;
					if(districtMasterInternal.getOfferPortfolioNeeded()){
						isPortfolio=1;
					}else{
						isPortfolio=0;
					}
					if(districtMasterInternal.getOfferEPI()){
						offerEPI=1;
					}else{
						offerEPI=0;
					}
					if(districtMasterInternal.getOfferJSI()){
						offerJSI=1;
					}else{
						offerJSI=0;
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}*/
			
			try{
				 if(jobOrder.getJobCategoryMaster()!=null){
						if(jobOrder.getJobCategoryMaster().getEpiForFullTimeTeachers()!=null){
							if(jobOrder.getJobCategoryMaster().getEpiForFullTimeTeachers()){
								epiInternal=true;
							}
							if(jobForTeacher.getIsAffilated()!=null){
								 isAffilated=jobForTeacher.getIsAffilated();
							}
							if(internalFlag==0 && isAffilated==1){
								if(jobOrder.getJobCategoryMaster().getEpiForFullTimeTeachers()){
									offerEPI=1;
								}else{
									offerEPI=0;
								}
								//if(jobOrder.getJobCategoryMaster().getOfferJSI()==1)
								if(jobOrder.getJobCategoryMaster().getOfferJSI()!=0){
									offerJSI=1;
								}else {
									offerJSI=0;
								}
								if(jobOrder.getJobCategoryMaster().getOfferPortfolioNeeded()){
									isPortfolio=1;
								}else{
									isPortfolio=0;
								}
							}
						}
				 }
			}catch(Exception e){
				e.printStackTrace();
			}
			if(jobForTeacher.getIsAffilated()!=null){
				 if(epiInternal==false){
					 isAffilated=jobForTeacher.getIsAffilated();
				 }else{
					 isAffilated=0;
				 }
			}
			
			//System.out.println("Flags :"+isAffilated+" "+isPortfolio+" "+internalFlag+" "+offerEPI+" "+offerJSI+" ");
			if((teacherDetail.getIsPortfolioNeeded()) && !tPortfolioStatus && internalFlag==0 && isPortfolio==1)
			{
				statusMaster = mapStatus.get("icomp");
			}else{
				if(isPortfolio==0 || (isPortfolio==1 && tPortfolioStatus  && teacherDetail.getIsPortfolioNeeded()))
				{
					statusMaster = mapStatus.get("comp");
					if(!jobOrder.getJobCategoryMaster().getBaseStatus()  || (internalFlag==1 && offerEPI==0 )||(internalFlag==0 && offerEPI==0 && isAffilated==1)){
						if(jobOrder.getIsJobAssessment() && ((offerJSI==1 && internalFlag==1 )||(internalFlag==0 && isAffilated==1 && offerJSI==1)|| (internalFlag==0 && isAffilated==0))){
							if(teacherAssessmentStatusJSI!=null){
								statusMaster=teacherAssessmentStatusJSI.getStatusMaster();
							}else{
								statusMaster = mapStatus.get("icomp");
							}
						}else{
							statusMaster = mapStatus.get("comp");
						}
					}                                                                        
					else if(jobOrder.getIsJobAssessment()&& ((offerJSI==1 && internalFlag==1 )|| (internalFlag==0 && isAffilated==1 && offerJSI==1)|| (internalFlag==0 && isAffilated==0))){
						/*============== Gagan : Job specific Inventory (JSI) Status is checking here : ==============*/
						if(teacherBaseAssessmentStatus!=null){
							if(jobForTeacher.getIsAffilated()!=null && jobOrder.getJobCategoryMaster().getOfferJSI()==0 && jobForTeacher.getIsAffilated()==1)
							{
								statusMaster = mapStatus.get("comp");
							}else{
							if(teacherBaseAssessmentStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("comp")){
								if(teacherAssessmentStatusJSI!=null){
									statusMaster=teacherAssessmentStatusJSI.getStatusMaster();
								}else{
									statusMaster = mapStatus.get("icomp");
								}
							}else{
								statusMaster=teacherBaseAssessmentStatus.getStatusMaster();
							}
						}
						}else{
							statusMaster = mapStatus.get("icomp");
						}
					}
					else{
						boolean  evDistrict=false;
						try{
							if(jobOrder.getDistrictMaster()!=null){
								if(jobOrder.getDistrictMaster().getDistrictId()==5513170){
									evDistrict=true;
								}
							}
						}catch(Exception e){}
						/*============== Gagan : Base Status is checking here : ==============*/
						if((isAffilated!=1 || evDistrict )&&((offerEPI==1 && internalFlag==1 )|| ( internalFlag==0 && isAffilated==0) || ( internalFlag==0 && isAffilated==1 && offerEPI==1))) //if block will execute only if Integer.parseInt(isAffilated) value is 0 or null
						{	
							if(teacherBaseAssessmentStatus!=null){
								statusMaster=teacherBaseAssessmentStatus.getStatusMaster();
							}else{
								statusMaster = mapStatus.get("icomp");
							}
						}
						else // isAffilated else block
						{
							statusMaster = mapStatus.get("comp");
						}
					}
				}
				else
				{
					statusMaster = mapStatus.get("icomp");
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return statusMaster;

	
		
	}
	
	
	
	@RequestMapping(value="/teacherinfo.do", method=RequestMethod.GET)
	public String teaccherInfoGETNew(ModelMap map,HttpServletRequest request)
	{
		TestTool.getTraceTime("100");
		try 
		{
			System.out.println("\n =========== teaccherinfo.do in TeacherController  ===============");
			HttpSession session = request.getSession(false);
			boolean noEPIFlag=false;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}

			UserMaster userSession		=	(UserMaster) session.getAttribute("userMaster");

			if(userSession.getDistrictId()!=null){
				if(userSession.getDistrictId().getNoEPI()==null ||(userSession.getDistrictId().getNoEPI()!=null && userSession.getDistrictId().getNoEPI()==false)){
					noEPIFlag=true;
				}
			}
			
			
			map.addAttribute("userSession", userSession);
			int entityID=0,roleId=0;
			if(userSession!=null){
				entityID=userSession.getEntityType();
				if(userSession.getRoleId().getRoleId()!=null){
					roleId=userSession.getRoleId().getRoleId();
				}
			}
			if(entityID==1){
				noEPIFlag=true;
			}
			map.addAttribute("noEPIFlag", noEPIFlag);
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,38,"candidatereport.do",0);
			}catch(Exception e){
				e.printStackTrace();
			}
			TestTool.getTraceTime("101");
			map.addAttribute("entityID", entityID);
			if(userSession.getEntityType()!=1 && userSession.getEntityType()!=5 && userSession.getEntityType()!=6){
				map.addAttribute("DistrictName",userSession.getDistrictId().getDistrictName());
				map.addAttribute("DistrictId",userSession.getDistrictId().getDistrictId());
			}else{
				map.addAttribute("DistrictName",null);
			}
			
			if(userSession.getEntityType()==2){
				map.addAttribute("DistrictName",userSession.getDistrictId().getDistrictName());
				map.addAttribute("DistrictId",userSession.getDistrictId().getDistrictId());
				map.addAttribute("activeuser",userMasterDAO.getUserByDistrict(userSession.getDistrictId()));
				
			}
			if(userSession.getEntityType()==3){
				map.addAttribute("SchoolId",userSession.getSchoolId().getSchoolId());
				map.addAttribute("schoolName",userSession.getSchoolId().getSchoolName());
				map.addAttribute("activeuser",userMasterDAO.getUserBySchool(userSession.getSchoolId()));
			}else{
				map.addAttribute("SchoolName",null);
			}
			
			if(userSession.getEntityType()==5){
				HeadQuarterMaster headQuarterMaster = userSession.getHeadQuarterMaster()!=null?userSession.getHeadQuarterMaster():null;
				map.addAttribute("headQuarterMaster",headQuarterMaster);
				map.addAttribute("headQuarterId", headQuarterMaster.getHeadQuarterId());
				map.addAttribute("headQuarter", headQuarterMaster.getHeadQuarterName());
			}
			
			if(userSession.getEntityType()==6){
				BranchMaster branchMaster = userSession.getBranchMaster()!=null?userSession.getBranchMaster():null;
				map.addAttribute("branchMaster",branchMaster);
				map.addAttribute("branchId", branchMaster.getBranchId());
				map.addAttribute("headQuarter", branchMaster.getHeadQuarterMaster().getHeadQuarterName());
			}
						
			TestTool.getTraceTime("102");
			List<StatusMaster> lstStatusMasters = new ArrayList<StatusMaster>();
			String[] statusTotal = {"comp","icomp","hird","rem","widrw","vlt"};
			try{
				lstStatusMasters = statusMasterDAO.findStatusByShortNames(statusTotal);
			}catch (Exception e) {
				e.printStackTrace();
			}
			List<StateMaster> lstStateMaster = new ArrayList<StateMaster>();
				lstStateMaster = stateMasterDAO.findAllStateByOrder();
			TestTool.getTraceTime("103");
			// @Ashish :: Add job Category list
			DistrictMaster districtMaster = null;
			HeadQuarterMaster headQuarterMaster = null;
			BranchMaster branchMaster = null;
			List<JobCategoryMaster> lstJobCategoryMasters = new ArrayList<JobCategoryMaster>();
			
			boolean statusNotes=false;
			boolean writePrivilegeToSchool = false;
			boolean achievementScore=false,tFA=false,demoClass=false,JSI=false,teachingOfYear =false,expectedSalary=false,fitScore=false;
			boolean isTeacherPoolOnLoad = false;
			StringBuffer taglists=new StringBuffer();
			if(entityID!=1)
			{
				if(userSession.getDistrictId()!=null)
				{
					districtMaster = districtMasterDAO.findById(userSession.getDistrictId().getDistrictId(), false, false);
					List<SecondaryStatusMaster> listSecondaryStatusMasters = secondaryStatusMasterDAO.findTagswithDistrictInCandidatePool(districtMaster);
					if(listSecondaryStatusMasters!=null && listSecondaryStatusMasters.size()>0){
						for(SecondaryStatusMaster ssm : listSecondaryStatusMasters){
							taglists.append("<option value='"+ssm.getSecStatusId()+"'>"+ssm.getSecStatusName()+"</option>");	
						}  
					}    

						lstJobCategoryMasters = jobCategoryMasterDAO.findAllJobCategoryNameByDistrict(districtMaster);
					try{

						if(districtMaster.getDisplayAchievementScore()){
							achievementScore=true;
						}
						if(districtMaster.getDisplayTFA()){
							tFA=true;
						}
						if(districtMaster.getDisplayDemoClass()){
							demoClass=true;
						}
						if(districtMaster.getDisplayJSI()){
							JSI=true;
						}
						if(districtMaster.getDisplayYearsTeaching()){
							teachingOfYear=true;
						}
						if(districtMaster.getDisplayExpectedSalary()){
							expectedSalary=true;
						}
						if(districtMaster.getDisplayFitScore()){
							fitScore=true;
						}

						if(districtMaster.getStatusNotes())
							statusNotes=true;

						writePrivilegeToSchool = districtMaster.getWritePrivilegeToSchool();
						isTeacherPoolOnLoad = districtMaster.getIsTeacherPoolOnLoad()==null?false:districtMaster.getIsTeacherPoolOnLoad()==true;

					}catch(Exception e){ e.printStackTrace();}
				}
				else if(userSession.getHeadQuarterMaster()!=null)
				{
					headQuarterMaster = userSession.getHeadQuarterMaster();
					List<SecondaryStatusMaster> listSecondaryStatusMasters = secondaryStatusMasterDAO.findTagsWithHeadQuarterInCandidatePool(headQuarterMaster.getHeadQuarterId());
					if(listSecondaryStatusMasters!=null && listSecondaryStatusMasters.size()>0){
						for(SecondaryStatusMaster ssm : listSecondaryStatusMasters){
							taglists.append("<option value='"+ssm.getSecStatusId()+"'>"+ssm.getSecStatusName()+"</option>");	
						}
					} 

					lstJobCategoryMasters = jobCategoryMasterDAO.findJobCategoryByHQAandBranchAndDistrict(headQuarterMaster,null,null);
					try{

						if(headQuarterMaster.getDisplayAchievementScore()){
							achievementScore=true;
						}
						/*if(headQuarterMaster.getDisplayTFA()){
							tFA=true;
						}*/
						if(headQuarterMaster.getDisplayDemoClass()){
							demoClass=true;
						}
						if(headQuarterMaster.getDisplayJSI()){
							JSI=true;
						}
						/*if(headQuarterMaster.getDisplayYearsTeaching()){
							teachingOfYear=true;
						}
						if(headQuarterMaster.getDisplayExpectedSalary()){
							expectedSalary=true;
						}*/
						if(headQuarterMaster.getDisplayFitScore()){
							fitScore=true;
						}

						if(headQuarterMaster.getStatusNotes())
							statusNotes=true;

						writePrivilegeToSchool = headQuarterMaster.getWritePrivilegeToBranch();
						isTeacherPoolOnLoad = headQuarterMaster.getIsTeacherPoolOnLoad()==null?false:headQuarterMaster.getIsTeacherPoolOnLoad()==true;

					}catch(Exception e){ e.printStackTrace();}
				}
				else if(userSession.getBranchMaster()!=null)
				{
					branchMaster = userSession.getBranchMaster();
					List<SecondaryStatusMaster> listSecondaryStatusMasters = secondaryStatusMasterDAO.findTagsWithBranchInCandidatePool(branchMaster.getBranchId());
					if(listSecondaryStatusMasters!=null && listSecondaryStatusMasters.size()>0){
						for(SecondaryStatusMaster ssm : listSecondaryStatusMasters){
							taglists.append("<option value='"+ssm.getSecStatusId()+"'>"+ssm.getSecStatusName()+"</option>");	
						}
					} 

					lstJobCategoryMasters = jobCategoryMasterDAO.findJobCategoryByHQAandBranchAndDistrict(null,branchMaster,null);
					try{

						if(branchMaster.getDisplayAchievementScore()){
							achievementScore=true;
						}
						/*if(headQuarterMaster.getDisplayTFA()){
							tFA=true;
						}*/
						if(branchMaster.getDisplayDemoClass()){
							demoClass=true;
						}
						if(branchMaster.getDisplayJSI()){
							JSI=true;
						}
						/*if(headQuarterMaster.getDisplayYearsTeaching()){
							teachingOfYear=true;
						}
						if(headQuarterMaster.getDisplayExpectedSalary()){
							expectedSalary=true;
						}*/
						if(branchMaster.getDisplayFitScore()){
							fitScore=true;
						}

							statusNotes=true;

						writePrivilegeToSchool = branchMaster.getWritePrivilegeToDistrict();
						isTeacherPoolOnLoad = branchMaster.getIsTeacherPoolOnLoad()==null?false:branchMaster.getIsTeacherPoolOnLoad()==true;

					}catch(Exception e){ e.printStackTrace();}
				}
			}else
			{
				achievementScore=true;tFA=true;demoClass=true;JSI=true;teachingOfYear =true;expectedSalary=true;fitScore=true;
			}
			TestTool.getTraceTime("103");
			try{
				if(districtMaster!=null && userSession.getEntityType()!=1){
					if(districtMaster.getIsReqNoForHiring()!=null && districtMaster.getIsReqNoForHiring()){
						map.addAttribute("isReqNoForHiring",1);	
					}else{
						map.addAttribute("isReqNoForHiring",0);
					}
				}else{
					map.addAttribute("isReqNoForHiring",0);
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			
			System.out.println("*** isTeacherPoolOnLoad " +isTeacherPoolOnLoad);
			
			map.addAttribute("isTeacherPoolOnLoad",isTeacherPoolOnLoad);
			
			Map<String,Boolean> prefMap = new HashMap<String, Boolean>();
			prefMap.put("achievementScore", achievementScore);
			prefMap.put("tFA", tFA);
			prefMap.put("demoClass", demoClass);
			prefMap.put("JSI", JSI);
			prefMap.put("teachingOfYear", teachingOfYear);
			prefMap.put("expectedSalary", expectedSalary);
			prefMap.put("fitScore", fitScore);
			
			if(districtMaster!=null && districtMaster.getDistrictId().equals(7800038)){
				prefMap.put("MAScore", true);
				prefMap.put("NFScore", true);
			}
			
			
			map.addAttribute("prefMap",prefMap);
			map.addAttribute("lstStateMaster",lstStateMaster);
			map.addAttribute("lstJobCategoryMasters",lstJobCategoryMasters);
			map.addAttribute("lstStatusMasters",lstStatusMasters);
			map.addAttribute("roleAccess", roleAccess);
			map.addAttribute("timezone",timeZoneMasterDAO.getAllTimeZone());
			map.addAttribute("writePrivilegeToSchool",writePrivilegeToSchool);
			List<SubjectMaster> subjectMasters = new ArrayList<SubjectMaster>();
			
			if(userSession.getEntityType()==2)
	 		subjectMasters = subjectMasterDAO.findActiveSubjectByDistrict((DistrictMaster)userSession.getDistrictId());
			TestTool.getTraceTime("104");
			map.addAttribute("subjectMasters",subjectMasters);
			map.addAttribute("entityType",userSession.getEntityType());
			map.addAttribute("statusNotes",statusNotes);
			map.addAttribute("tagslist",taglists.toString());
			
			if(userSession.getHeadQuarterMaster()!=null && userSession.getHeadQuarterMaster().getHeadQuarterId()==2)
			{
				map.addAttribute("CheckNC",userSession.getHeadQuarterMaster().getHeadQuarterId());
				
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		TestTool.getTraceTime("199");
		return "teacherinfonew";
	}
	
	/*Add by Ram Nath*/
	@RequestMapping(value="/advOnBoarding.do", method=RequestMethod.GET)
	public String accOrDecAdvOnBoarding(ModelMap map,HttpServletRequest request)
	{	
		request.getSession();
		System.out.println("Calling accOrDecAdvOnBoarding.do ...accOrDecAdvOnBoarding.do"+request.getParameter("key"));
		String msg="";
		int verifyFlag=0;
		try{
			String key = request.getParameter("key")==null?"":request.getParameter("key").trim();
			String offerUrl=Utility.getValueOfPropByKey("basePath")+"/advOnBoardingCfrm.do?key="+key;
			if(key!=null && !key.equals(""))
			{
				String keyValue=Utility.decodeBase64(key);
				System.out.println("keyValue "+keyValue);
				if(keyValue!=null && !keyValue.equalsIgnoreCase(""))
				{
					String arr[]=keyValue.split("##");
					
					String teacherId="";
					int iTeacherId=0;
					if(arr.length > 0)
					{
						teacherId=arr[0];
						iTeacherId=Utility.getIntValue(teacherId);
					}
					
					String jobId="";
					int ijobId=0;
					if(arr.length > 1)
					{
						jobId=arr[1];
						ijobId=Utility.getIntValue(jobId);
					}
					
					String userId="";
					int iuserId=0;
					if(arr.length > 2)
					{
						userId=arr[2];	
						iuserId=Utility.getIntValue(userId);
					}
					
					String acceptOrDecline="";
					if(arr.length > 3)
					{
						acceptOrDecline=arr[3];						
					}
					TeacherDetail teacherDetail= teacherDetailDAO.findById(Integer.parseInt(teacherId), false,false);
					JobOrder jobOrder= jobOrderDAO.findById(Integer.parseInt(jobId), false,false);
					UserMaster usermaster=userMasterDAO.findById(iuserId,false,false);
					JobForTeacher jobForTeacher=jobForTeacherDAO.getJobForTeacherDetails(teacherDetail, jobOrder);
					if(jobForTeacher!=null){
						if(jobForTeacher.getOfferAccepted()!=null){
							if(acceptOrDecline.trim().equals("1")){
								if(jobForTeacher.getOfferAccepted()){
									msg=Utility.getLocaleValuePropByKey("msgAcceptOrDecline1", locale);
									verifyFlag=2;
								}else{
									msg=Utility.getLocaleValuePropByKey("msgAcceptOrDecline2", locale);
									verifyFlag=2;
								}
								}
								if(acceptOrDecline.trim().equals("0")){
									if(jobForTeacher.getOfferAccepted()){
										msg=Utility.getLocaleValuePropByKey("msgAcceptOrDecline3", locale);
										verifyFlag=2;
									}else{
										msg=Utility.getLocaleValuePropByKey("msgAcceptOrDecline4", locale);
										verifyFlag=2;
									}	
									
								}
							}
							else
							{
									if(acceptOrDecline!=null && acceptOrDecline.equals("1")){
										msg=Utility.getLocaleValuePropByKey("msgAcceptOrDecline5", locale);
										verifyFlag=1;
									}else if(acceptOrDecline!=null && acceptOrDecline.equals("0")){
										msg=Utility.getLocaleValuePropByKey("msgAcceptOrDecline6", locale);
										verifyFlag=1;
									}													
							}
					}
				}
			}
			map.addAttribute("offerUrl",offerUrl);
			}
			catch (Exception e) {
				// TODO: handle exception
			}
		if(verifyFlag==1){
				map.addAttribute("verifyFlag",1);
				map.addAttribute("msg",msg);
			}else {
				map.addAttribute("verifyFlag",2);
				map.addAttribute("msgpage",msg);
			}
			
			System.out.println(msg);					
		return "advonboarding";
		
		}
	@RequestMapping(value="/advOnBoardingCfrm.do", method=RequestMethod.GET)
	public String advOnBoardingCfrm(ModelMap map,HttpServletRequest request)
	{	
		request.getSession();
		System.out.println("Calling acceptOrDeclineAdvanceOnBoardingConfirm ...acceptOrDeclineAdvanceOnBoardingConfirm.do"+request.getParameter("key"));
		String msg="";
		try{
			String key = request.getParameter("key")==null?"":request.getParameter("key").trim();			
			{
				String keyValue=Utility.decodeBase64(key);
				System.out.println("keyValue "+keyValue);
				if(keyValue!=null && !keyValue.equalsIgnoreCase(""))
				{
					String arr[]=keyValue.split("##");
					
					String teacherId="";
					int iTeacherId=0;
					if(arr.length > 0)
					{
						teacherId=arr[0];
						iTeacherId=Utility.getIntValue(teacherId);
					}
					
					String jobId="";
					int ijobId=0;
					if(arr.length > 1)
					{
						jobId=arr[1];
						ijobId=Utility.getIntValue(jobId);
					}
					
					String userId="";
					int iuserId=0;
					if(arr.length > 2)
					{
						userId=arr[2];	
						iuserId=Utility.getIntValue(userId);
					}
					
					String acceptOrDecline="";
					if(arr.length > 3)
					{
						acceptOrDecline=arr[3];						
					}
					TeacherDetail teacherDetail= teacherDetailDAO.findById(Integer.parseInt(teacherId), false,false);
					JobOrder jobOrder= jobOrderDAO.findById(Integer.parseInt(jobId), false,false);
					UserMaster usermaster=userMasterDAO.findById(iuserId,false,false);
					JobForTeacher jobForTeacher=jobForTeacherDAO.getJobForTeacherDetails(teacherDetail, jobOrder);
					List<StatusMaster> lstStatusMaster=statusMasterDAO.findAllStatusMaster();
					if(jobForTeacher!=null){
						if(jobForTeacher.getOfferAccepted()!=null){
							if(acceptOrDecline.trim().equals("1")){
								if(jobForTeacher.getOfferAccepted()){
									msg=Utility.getLocaleValuePropByKey("msgAcceptOrDecline1", locale);
								}else{
									msg=Utility.getLocaleValuePropByKey("msgAcceptOrDecline2", locale);
								}
								}
							if(acceptOrDecline.trim().equals("0")){
								if(jobForTeacher.getOfferAccepted()){
									msg=Utility.getLocaleValuePropByKey("msgAcceptOrDecline3", locale);									
								}else{
									msg=Utility.getLocaleValuePropByKey("msgAcceptOrDecline4", locale);
								}	
								
							}
						}
						else
						{							
							if(acceptOrDecline.equals("1")){											
										jobForTeacher.setOfferAccepted(true);
										jobForTeacher.setOfferAcceptedDate(new Date());
										jobForTeacher.setLastActivity("Offer Accepted");
										jobForTeacher.setLastActivityDate(new Date());
										jobForTeacherDAO.updatePersistent(jobForTeacher);
										msg=Utility.getLocaleValuePropByKey("msgAcceptOrDecline7", locale)+"  "+jobOrder.getJobTitle()+". We will contact you shortly.";
							}else if(acceptOrDecline.equals("0")){						
									jobForTeacher.setOfferAccepted(false);
									jobForTeacher.setOfferAcceptedDate(new Date());
									StatusMaster statusMasterDCLN= findStatusByShortName(lstStatusMaster,"dcln");
									jobForTeacher.setStatus(statusMasterDCLN);
									jobForTeacher.setStatusMaster(statusMasterDCLN);
									jobForTeacher.setLastActivity(statusMasterDCLN.getStatus());
									jobForTeacher.setLastActivityDate(new Date());
									jobForTeacherDAO.updatePersistent(jobForTeacher);
									StatusMaster statusMasterHird= findStatusByShortName(lstStatusMaster,"hird");
									StatusMaster statusMasterRem= findStatusByShortName(lstStatusMaster,"rem");
									try{
										TeacherStatusHistoryForJob tSHJ= new TeacherStatusHistoryForJob();
										tSHJ=teacherStatusHistoryForJobDAO.findByTeacherStatusHistoryForJob(jobForTeacher.getTeacherId(),jobForTeacher.getJobId(),statusMasterHird,"A");
										if(tSHJ!=null){
											tSHJ.setStatus("I");
											tSHJ.setHiredByDate(null);
											tSHJ.setUpdatedDateTime(new Date());
											teacherStatusHistoryForJobDAO.updatePersistent(tSHJ);
										}
									}catch(Exception e){
										e.printStackTrace();
									}
									try{
										TeacherStatusHistoryForJob tSHJ= new TeacherStatusHistoryForJob();
										tSHJ=teacherStatusHistoryForJobDAO.findByTeacherStatusHistoryForJob(jobForTeacher.getTeacherId(),jobForTeacher.getJobId(),statusMasterRem,"A");
										if(tSHJ!=null){
											tSHJ.setStatus("I");
											tSHJ.setHiredByDate(null);
											tSHJ.setUpdatedDateTime(new Date());
											teacherStatusHistoryForJobDAO.updatePersistent(tSHJ);
										}
									}catch(Exception e){
										e.printStackTrace();
									}
									try{
										TeacherStatusHistoryForJob tSHJ= new TeacherStatusHistoryForJob();
										tSHJ.setTeacherDetail(teacherDetail);										
										tSHJ.setJobOrder(jobOrder);
										tSHJ.setStatusMaster(statusMasterDCLN);
										tSHJ.setStatus("A");
										tSHJ.setCreatedDateTime(new Date());
										tSHJ.setUserMaster(usermaster);
										teacherStatusHistoryForJobDAO.makePersistent(tSHJ);
									}catch(Exception e){
										e.printStackTrace();
									}
									msg= Utility.getLocaleValuePropByKey("msgAcceptOrDecline9", locale)+"  "+jobOrder.getJobTitle()+".";
							}
						}
					}
				}
			}
			}
			catch (Exception e) {
				// TODO: handle exception
			}
				map.addAttribute("verifyFlag",2);
				map.addAttribute("msgpage",msg);
			System.out.println(msg);					
		return "advonboarding";
		
		}
	
	/*Add by Ram Nath*/
	@RequestMapping(value="/evlComp.do", method=RequestMethod.GET)
	public String evlComp(ModelMap map,HttpServletRequest request)
	{	
		request.getSession();
		System.out.println("Calling evlComp.do ..."+request.getParameter("key"));
		String msg="";
		int verifyFlag=0;
		try{
			String key = request.getParameter("key")==null?"":request.getParameter("key").trim();
			String offerUrl=Utility.getValueOfPropByKey("basePath")+"/evlCompCfrm.do?key="+key;
			if(key!=null && !key.equals(""))
			{
				String keyValue=Utility.decodeBase64(key);
				System.out.println("keyValue "+keyValue);
				if(keyValue!=null && !keyValue.equalsIgnoreCase(""))
				{
					String arr[]=keyValue.split("##");
					
					String teacherId="";
					int iTeacherId=0;
					if(arr.length > 0)
					{
						teacherId=arr[0];
						iTeacherId=Utility.getIntValue(teacherId);
					}
					
					String jobId="";
					int ijobId=0;
					if(arr.length > 1)
					{
						jobId=arr[1];
						ijobId=Utility.getIntValue(jobId);
					}
					
					String userId="";
					int iuserId=0;
					if(arr.length > 2)
					{
						userId=arr[2];	
						iuserId=Utility.getIntValue(userId);
					}
					
					String acceptOrDecline="";
					if(arr.length > 5)
					{
						acceptOrDecline=arr[5];						
					}
					TeacherDetail teacherDetail= teacherDetailDAO.findById(Integer.parseInt(teacherId), false,false);
					JobOrder jobOrder= jobOrderDAO.findById(Integer.parseInt(jobId), false,false);
					JobForTeacher jobForTeacher=jobForTeacherDAO.getJobForTeacherDetails(teacherDetail, jobOrder);
					if(jobForTeacher!=null){
						if(jobForTeacher.getOfferAccepted()!=null){
							if(acceptOrDecline.trim().equals("1")){
								if(jobForTeacher.getOfferAccepted()){
									msg=Utility.getLocaleValuePropByKey("msgAcceptOrDecline1", locale);
									verifyFlag=2;
								}else{
									msg=Utility.getLocaleValuePropByKey("msgAcceptOrDecline2", locale);
									verifyFlag=2;
								}
								}
								if(acceptOrDecline.trim().equals("0")){
									if(jobForTeacher.getOfferAccepted()){
										msg=Utility.getLocaleValuePropByKey("msgAcceptOrDecline3", locale);
										verifyFlag=2;
									}else{
										msg=Utility.getLocaleValuePropByKey("msgAcceptOrDecline4", locale);
										verifyFlag=2;
									}	
									
								}
							}
							else
							{
									if(acceptOrDecline!=null && acceptOrDecline.equals("1")){
										msg=Utility.getLocaleValuePropByKey("msgAcceptOrDecline5", locale);
										verifyFlag=1;
									}else if(acceptOrDecline!=null && acceptOrDecline.equals("0")){
										msg=Utility.getLocaleValuePropByKey("msgAcceptOrDecline6", locale);
										verifyFlag=1;
									}													
							}
					}
				}
			}
			map.addAttribute("offerUrl",offerUrl);
			}
			catch (Exception e) {
				// TODO: handle exception
			}
		if(verifyFlag==1){
				map.addAttribute("verifyFlag",1);
				map.addAttribute("msg",msg);
			}else {
				map.addAttribute("verifyFlag",2);
				map.addAttribute("msgpage",msg);
			}
			
			System.out.println(msg);					
		return "acceptordecline";
		
		}
	@RequestMapping(value="/evlCompCfrm.do", method=RequestMethod.GET)
	public String evlCompCfrm(ModelMap map,HttpServletRequest request)
	{
		request.getSession();
		System.out.println("Calling evlCompCfrm.do ...evlCompCfrm.do"+request.getParameter("key"));
		String msg="";
		try{
			String key = request.getParameter("key")==null?"":request.getParameter("key").trim();			
			{
				String keyValue=Utility.decodeBase64(key);
				System.out.println("keyValue "+keyValue);
				if(keyValue!=null && !keyValue.equalsIgnoreCase(""))
				{
					String arr[]=keyValue.split("##");
					
					String teacherId="";
					int iTeacherId=0;
					if(arr.length > 0)
					{
						teacherId=arr[0];
						iTeacherId=Utility.getIntValue(teacherId);
					}
					
					String jobId="";
					int ijobId=0;
					if(arr.length > 1)
					{
						jobId=arr[1];
						ijobId=Utility.getIntValue(jobId);
					}
					
					String userId="";
					int iuserId=0;
					if(arr.length > 2)
					{
						userId=arr[2];	
						iuserId=Utility.getIntValue(userId);
					}
					
					String acceptOrDecline="";
					if(arr.length > 5)
					{
						acceptOrDecline=arr[5];						
					}
					
					String statusFlag=keyValue.split("##")[3];
					String statusId=keyValue.split("##")[4];
					
					TeacherDetail teacherDetail= teacherDetailDAO.findById(Integer.parseInt(teacherId), false,false);
					JobOrder jobOrder= jobOrderDAO.findById(Integer.parseInt(jobId), false,false);
					UserMaster usermaster=userMasterDAO.findById(iuserId,false,false);
					JobForTeacher jobForTeacher=jobForTeacherDAO.getJobForTeacherDetails(teacherDetail, jobOrder);
					List<StatusMaster> lstStatusMaster=statusMasterDAO.findAllStatusMaster();
				
					List<JobWisePanelStatus> jobWisePanelStatusList=new ArrayList<JobWisePanelStatus>();
					SecondaryStatus secondaryStatus_temp=null;
					StatusMaster statusMaster_temp=null;
					if(statusFlag.equals("ss")){
						secondaryStatus_temp=secondaryStatusDAO.findById(Integer.parseInt(statusId), false,false);
					}else{
						statusMaster_temp=statusMasterDAO.findById(Integer.parseInt(statusId), false,false);
					}
					jobWisePanelStatusList=jobWisePanelStatusDAO.getPanel(jobOrder,secondaryStatus_temp,statusMaster_temp);
					List<UserMaster> panelAttendees_ForEmail=new ArrayList<UserMaster>();
					List<PanelSchedule> panelScheduleList= new ArrayList<PanelSchedule>();
					UserMaster schoolUser=null;
					PanelSchedule panelSchedule=null;
					if(jobWisePanelStatusList.size() == 1){
						panelScheduleList=panelScheduleDAO.findPanelSchedulesByJobWisePanelStatusListAndTeacher(jobWisePanelStatusList,teacherDetail);
						if(panelScheduleList.size()==1){
							
							if(panelScheduleList.get(0)!=null){
								panelSchedule=panelScheduleList.get(0);
								List<PanelAttendees> panelAttendees=new ArrayList<PanelAttendees>();
								panelAttendees=panelAttendeesDAO.getPanelAttendees(panelSchedule);
								if(panelAttendees.size() > 0){
									for(PanelAttendees attendees:panelAttendees){
										if(attendees!=null){
											panelAttendees_ForEmail.add(attendees.getPanelInviteeId());
											if(attendees.getPanelInviteeId().getEntityType()==3){
												schoolUser=attendees.getPanelInviteeId();
											}
										}
									}
								}
								
							}
						}
					}
					
					
					
					System.out.println("schoolUser========"+schoolUser);
					if(jobForTeacher!=null){
						if(jobForTeacher.getOfferAccepted()!=null){
							if(acceptOrDecline.trim().equals("1")){
								if(jobForTeacher.getOfferAccepted()){
									msg=Utility.getLocaleValuePropByKey("msgAcceptOrDecline1", locale);
								}else{
									msg=Utility.getLocaleValuePropByKey("msgAcceptOrDecline2", locale);
								}
								}
							if(acceptOrDecline.trim().equals("0")){
								if(jobForTeacher.getOfferAccepted()){
									msg=Utility.getLocaleValuePropByKey("msgAcceptOrDecline3", locale);									
								}else{
									msg=Utility.getLocaleValuePropByKey("msgAcceptOrDecline4", locale);
								}	
								
							}
						}
						else
						{							
									if(acceptOrDecline.equals("1")){
										StatusMaster statusMasterHIRD= findStatusByShortName(lstStatusMaster,"hird");
										jobForTeacher.setStatus(statusMasterHIRD);
										jobForTeacher.setStatusMaster(statusMasterHIRD);
										jobForTeacher.setUpdatedBy(usermaster);
										jobForTeacher.setUpdatedByEntity(usermaster.getEntityType());		
										jobForTeacher.setUpdatedDate(new Date());
										jobForTeacher.setLastActivity(statusMasterHIRD.getStatus());
										jobForTeacher.setLastActivityDate(new Date());
										jobForTeacher.setOfferAccepted(true);
										jobForTeacher.setOfferAcceptedDate(new Date());
										jobForTeacherDAO.updatePersistent(jobForTeacher);
										
										try{
											TeacherStatusHistoryForJob tSHJ= new TeacherStatusHistoryForJob();
											tSHJ.setTeacherDetail(teacherDetail);
											tSHJ.setJobOrder(jobOrder);
											tSHJ.setStatusMaster(statusMasterHIRD);
											tSHJ.setStatus("A");
											tSHJ.setCreatedDateTime(new Date());
											tSHJ.setUserMaster(usermaster);
											teacherStatusHistoryForJobDAO.makePersistent(tSHJ);
										}catch(Exception e){}
										
										try{
										TeacherStatusNotes teacherStatusNoteObj=new TeacherStatusNotes();
										teacherStatusNoteObj.setTeacherDetail(teacherDetail);
										teacherStatusNoteObj.setJobOrder(jobOrder);
										if(statusMaster_temp!=null)
											teacherStatusNoteObj.setStatusMaster(statusMaster_temp);
										if(secondaryStatus_temp!=null){
											teacherStatusNoteObj.setSecondaryStatus(secondaryStatus_temp);
										}
										teacherStatusNoteObj.setUserMaster(schoolUser);
										teacherStatusNoteObj.setDistrictId(jobOrder.getDistrictMaster().getDistrictId());
										teacherStatusNoteObj.setStatusNotes(null);
										teacherStatusNoteObj.setStatusNoteFileName(null);
										teacherStatusNoteObj.setEmailSentTo(2);
										//teacherStatusNoteObj.setFinalizeStatus(null);
										teacherStatusNotesDAO.makePersistent(teacherStatusNoteObj);
										System.out.println(teacherStatusNoteObj.getTeacherStatusNoteId()+":::::::::AfterInsert Note::::::::"+teacherStatusNoteObj.getTeacherStatusNoteId());
										}catch(Exception e){}
										
											try{
												MailSendEvlCompThread loct = new MailSendEvlCompThread();
												loct.setOnlineActivityAjax(onlineActivityAjax);
													loct.setSchoolUser(schoolUser);
													loct.setTemp(2);
													loct.setJobId(jobOrder);
													loct.setTeacherId(teacherDetail);
													loct.setUserMaster(usermaster);
													loct.setDistrictId(jobOrder.getDistrictMaster().getDistrictId());
													try {
														loct.start();	
														System.out.println(loct.isMailSend());
													} catch (Exception e) {}					
										    }catch(Exception e){
										    	e.printStackTrace();							    	
										    }																					
										    msg=Utility.getLocaleValuePropByKey("msgAcceptOrDecline7", locale)+" "+jobOrder.getJobTitle()+". We will contact you shortly.";
							}else if(acceptOrDecline.equals("0")){	
								CandidateGridService cgService=new CandidateGridService();
								try{
									/*Criterion criterion = Restrictions.eq("districtMaster", districtMasterDAO.findById(jobForTeacher.getJobId().getDistrictMaster().getDistrictId(), false, false));
									Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
									Criterion criterion2 = Restrictions.eq("jobOrder", jobOrder);
									//Criterion criterionOr1 = Restrictions.or(Restrictions.eq("statusMaster", statusMasterDAO.findByCriteria(Restrictions.eq("statusShortName", "ecomp"))), Restrictions.in("secondaryStatus", secondaryStatusDAO.findByCriteria(Restrictions.eq("secondaryStatusName", "Evaluation Complete"))));									
									Criterion criterion3 = Restrictions.eq("status","A");
							        Criterion criterion4 = Restrictions.eq("status","S");
							        Criterion criterionOr2 = Restrictions.or(criterion3,criterion4);*/
									Map<Integer,List<String>> statusHistoryMap=new HashMap<Integer, List<String>>();
									Map<String,StatusMaster> statusMasterMap=new HashMap<String,StatusMaster>();
									Map<String,SecondaryStatus> secondaryStatusMap=new HashMap<String,SecondaryStatus>();
									List<TeacherStatusHistoryForJob> historyList=teacherStatusHistoryForJobDAO.findByTeacherAndJob(jobForTeacher.getTeacherId(),jobForTeacher.getJobId());									
									List<TeacherStatusNotes> statusNoteList=teacherStatusNotesDAO.getStatusNoteAllList(jobForTeacher.getTeacherId(),jobForTeacher.getJobId());
									//List<TeacherStatusHistoryForJob> historyList=teacherStatusHistoryForJobDAO.findByCriteria(criterion1,criterion2,criterionOr1,criterionOr2);
									//List<TeacherStatusNotes> statusNoteList=teacherStatusNotesDAO.findByCriteria(criterion,criterion1,criterion2,criterionOr1,criterionOr2);
									List<SecondaryStatus> lstTreeStructure = secondaryStatusDAO.findSecondaryStatusByJobCategoryHeadAndBranch(jobOrder);
									LinkedHashMap<String,String> statusNameMap=new LinkedHashMap<String,String>();
									Map<String,Integer> statusHistMap=new HashMap<String, Integer>();
									if(schoolUser!=null){
										try{
											int counter=0; 
											for(SecondaryStatus tree: lstTreeStructure)
											{
												if(tree.getSecondaryStatus()==null)
												{
													if(tree.getChildren().size()>0){
														counter=cgService.getAllStatusForHistory(statusMasterMap,secondaryStatusMap,tree.getChildren(),statusHistMap,statusNameMap,counter);
													}
												}
											}
											System.out.println("counter==="+counter);
										}catch(Exception e){e.printStackTrace();}
	
										boolean eComp=false;
										try{
										if(statusNoteList.size()>0)
										for (TeacherStatusNotes teacherStatusNotes : statusNoteList) {
											if((teacherStatusNotes.getStatusMaster()!=null && teacherStatusNotes.getStatusMaster().getStatusShortName().equals("ecomp"))||(teacherStatusNotes.getSecondaryStatus()!=null && teacherStatusNotes.getSecondaryStatus().getSecondaryStatusName().equals("Evaluation Complete"))){
												teacherStatusNotesDAO.makeTransient(teacherStatusNotes);
											}
										}

										if(historyList.size()>0)
										for (TeacherStatusHistoryForJob teacherStatusHistoryForJob : historyList) {
											if((teacherStatusHistoryForJob.getStatusMaster()!=null && teacherStatusHistoryForJob.getStatusMaster().getStatusShortName().equals("ecomp"))||(teacherStatusHistoryForJob.getSecondaryStatus()!=null && teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusName().equals("Evaluation Complete"))){
												if(teacherStatusHistoryForJob.getStatusMaster()!=null && teacherStatusHistoryForJob.getStatusMaster().getStatusShortName().equals("ecomp")){
													eComp=true;	
												}
												teacherStatusHistoryForJobDAO.makeTransient(teacherStatusHistoryForJob);
											}
											
										}
										}catch(Exception e){e.printStackTrace();}
										System.out.println("ecomp===="+eComp);
										try{		
											historyList=teacherStatusHistoryForJobDAO.findByTeacherAndJob(jobForTeacher.getTeacherId(),jobForTeacher.getJobId());										
											System.out.println("historyList::::::::::"+historyList.size());
											
											List<String> statusList=new ArrayList<String>();
											if(historyList.size()>0)
											for (TeacherStatusHistoryForJob teacherStatusHistoryForJob : historyList) {
												if(statusHistoryMap.get(teacherStatusHistoryForJob.getTeacherDetail().getTeacherId())==null){
													statusList=new ArrayList<String>();
													if(teacherStatusHistoryForJob.getStatusMaster()!=null){
														statusList.add(teacherStatusHistoryForJob.getStatusMaster().getStatusId()+"##0");
													}else{
														statusList.add("0##"+teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusId());
													}
													statusHistoryMap.put(teacherStatusHistoryForJob.getTeacherDetail().getTeacherId(),statusList);
												}else{
													statusList=statusHistoryMap.get(teacherStatusHistoryForJob.getTeacherDetail().getTeacherId());
													if(teacherStatusHistoryForJob.getStatusMaster()!=null){
														statusList.add(teacherStatusHistoryForJob.getStatusMaster().getStatusId()+"##0");
													}else{
														statusList.add("0##"+teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusId());
													}
													statusHistoryMap.put(teacherStatusHistoryForJob.getTeacherDetail().getTeacherId(),statusList);
												}
											}
										}catch(Exception e){}
										
										for(Map.Entry<Integer, List<String>> entry:statusHistoryMap.entrySet())
											System.out.println("key=="+entry.getKey()+" Value==="+entry.getValue());
										System.out.println("statusHistoryMap>:::::::::::"+statusHistoryMap.size());
										List<String> statusIds=statusHistoryMap.get(jobForTeacher.getTeacherId().getTeacherId());
										System.out.println("statusIds===="+statusIds);
										String schoolStatusId=cgService.getSchoolStatusId(statusIds, statusHistMap, statusNameMap);
										System.out.println("schoolStatusId===="+schoolStatusId);
										SecondaryStatus secondaryStatusObj=null;
										StatusMaster statusMasterObj=null;
										int[] flagList=assessmentDetailDAO.getInternalTeacherFalgs(jobForTeacher) ;
					                    StatusMaster statusMasterMain = assessmentDetailDAO.findByTeacherIdJobStaus(jobForTeacher,flagList);
					                    System.out.println("statusMasterMain=="+statusMasterMain.getStatus());

										try{
											String statusIdMapValue[]=schoolStatusId.split("#");
											System.out.println("statusIdMapValue[]==="+statusIdMapValue);
											if(statusIdMapValue[0].equals("0") && statusIdMapValue[1].equals("0")){
												jobForTeacher.setStatusMaster(statusMasterMain);												
												jobForTeacher.setStatus(statusMasterMain);
												jobForTeacher.setSecondaryStatus(null);
												jobForTeacher.setLastActivity(statusMasterMain.getStatus());
												jobForTeacher.setLastActivityDate(new Date());
												
												
											}
											else if(statusIdMapValue[0].equals("0")){
													secondaryStatusObj=secondaryStatusMap.get(schoolStatusId);
													jobForTeacher.setSecondaryStatus(secondaryStatusObj);												
													jobForTeacher.setStatus(statusMasterMain);
													jobForTeacher.setStatusMaster(null);
											}else{
													statusMasterObj=statusMasterMap.get(schoolStatusId);
													jobForTeacher.setStatusMaster(statusMasterObj);												
													jobForTeacher.setStatus(statusMasterObj);
													jobForTeacher.setSecondaryStatus(null);	
													jobForTeacher.setLastActivity(statusMasterObj.getStatus());
													jobForTeacher.setLastActivityDate(new Date());
											}
										}catch(Exception e){e.printStackTrace();}
									}
								}catch(Exception e){
									e.printStackTrace();
								}
								System.out.println("Status=="+jobForTeacher.getStatusMaster()+" status=="+jobForTeacher.getStatus());
								System.out.println("Sectatus=="+jobForTeacher.getSecondaryStatus());
								jobForTeacher.setOfferReady(null);
								jobForTeacher.setOfferAccepted(false);
								jobForTeacher.setOfferAcceptedDate(new Date());
								jobForTeacherDAO.updatePersistent(jobForTeacher);
							
								try{
									
									JobWisePanelStatus jobWisePanelStatus =jobWisePanelStatusDAO.findJobWisePanelStatusObj(jobForTeacher.getJobId(),"Evaluation Complete");
									if(jobWisePanelStatus!=null){
										panelScheduleDAO.updatePanelSchedule(jobForTeacher.getTeacherId(),jobForTeacher.getJobId(),jobWisePanelStatus);
									}
								}catch(Exception e){e.printStackTrace();}
								try{
									StatusMaster statusMasterDCLN= findStatusByShortName(lstStatusMaster,"dcln");
									////////////////////
									SchoolWiseCandidateStatus swcsObj= new SchoolWiseCandidateStatus();
									swcsObj.setStatusMaster(statusMasterDCLN);
									swcsObj.setJobOrder(jobForTeacher.getJobId());
									swcsObj.setDistrictId(schoolUser.getDistrictId().getDistrictId());
									swcsObj.setSchoolId(schoolUser.getSchoolId().getSchoolId());
									swcsObj.setUserMaster(schoolUser);		
									swcsObj.setCreatedDateTime(new Date());
									swcsObj.setTeacherDetail(jobForTeacher.getTeacherId());
									swcsObj.setStatus("A");
									swcsObj.setJobForTeacher(jobForTeacher);
									schoolWiseCandidateStatusDAO.makePersistent(swcsObj);
									}catch(Exception e){e.printStackTrace();}
									/////////////////////////////////////////////////
									StatusMaster statusMasterHird= findStatusByShortName(lstStatusMaster,"hird");
									StatusMaster statusMasterRem= findStatusByShortName(lstStatusMaster,"rem");
									try{
										TeacherStatusHistoryForJob tSHJ= new TeacherStatusHistoryForJob();
										tSHJ=teacherStatusHistoryForJobDAO.findByTeacherStatusHistoryForJob(jobForTeacher.getTeacherId(),jobForTeacher.getJobId(),statusMasterHird,"A");
										if(tSHJ!=null){
											tSHJ.setStatus("I");
											tSHJ.setHiredByDate(null);
											tSHJ.setUpdatedDateTime(new Date());
											teacherStatusHistoryForJobDAO.updatePersistent(tSHJ);
										}
									}catch(Exception e){
										e.printStackTrace();
									}
								
									/*try{
										TeacherStatusHistoryForJob tSHJ= new TeacherStatusHistoryForJob();
										tSHJ.setTeacherDetail(teacherDetail);										
										tSHJ.setJobOrder(jobOrder);
										tSHJ.setStatusMaster(statusMasterDCLN);
										tSHJ.setStatus("A");
										tSHJ.setCreatedDateTime(new Date());
										tSHJ.setUserMaster(usermaster);
										teacherStatusHistoryForJobDAO.makePersistent(tSHJ);
									}catch(Exception e){
										e.printStackTrace();
									}*/
									try{
										MailSendEvlCompThread loct = new MailSendEvlCompThread();																				
										loct.setOnlineActivityAjax(onlineActivityAjax);
											loct.setSchoolUser(schoolUser);
											loct.setTemp(3);
											loct.setJobId(jobOrder);
											loct.setTeacherId(teacherDetail);
											loct.setUserMaster(usermaster);
											loct.setDistrictId(jobOrder.getDistrictMaster().getDistrictId());
											try {
												loct.start();	
												System.out.println(loct.isMailSend());
											} catch (Exception e) {}					
								    }catch(Exception e){
								    	e.printStackTrace();							    	
								    }										
								    msg=Utility.getLocaleValuePropByKey("msgAcceptOrDecline9", locale) +" "+jobOrder.getJobTitle()+".";
							}							
						}
					}
				}
			}
			
			}
			catch (Exception e) {
				// TODO: handle exception
			}
				map.addAttribute("verifyFlag",2);
				map.addAttribute("msgpage",msg);
			System.out.println(msg);		
		return "acceptordecline";
		
		}
	/*End by Ram Nath*/
	public StatusMaster findStatusByShortName(List <StatusMaster> statusMasterList,String statusShortName)
	{
		StatusMaster status= null;
		try 
		{   if(statusMasterList!=null)
			for(StatusMaster statusMaster: statusMasterList){
				if(statusMaster.getStatusShortName().equalsIgnoreCase(statusShortName)){
					status=statusMaster;
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}		
		if(status==null)
			return null;
		else
			return status;
	}	
	@RequestMapping(value="/teachersinfo.do", method=RequestMethod.GET)
	public String teaccherInfoGETOpNew(ModelMap map,HttpServletRequest request)
	{
		TestTool.getTraceTime("100");
		try 
		{
			System.out.println("\n =========== teaccherinfo.do in TeacherController  ===============");
			HttpSession session = request.getSession(false);
			boolean noEPIFlag=false;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}

			UserMaster userSession		=	(UserMaster) session.getAttribute("userMaster");

			if(userSession.getDistrictId()!=null){
				if(userSession.getDistrictId().getNoEPI()==null ||(userSession.getDistrictId().getNoEPI()!=null && userSession.getDistrictId().getNoEPI()==false)){
					noEPIFlag=true;
				}
			}
			
			
			map.addAttribute("userSession", userSession);
			int entityID=0,roleId=0;
			if(userSession!=null){
				entityID=userSession.getEntityType();
				if(userSession.getRoleId().getRoleId()!=null){
					roleId=userSession.getRoleId().getRoleId();
				}
			}
			if(entityID==1){
				noEPIFlag=true;
			}
			map.addAttribute("noEPIFlag", noEPIFlag);
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,38,"candidatereport.do",0);
			}catch(Exception e){
				e.printStackTrace();
			}
			TestTool.getTraceTime("101");
			map.addAttribute("entityID", entityID);
			if(userSession.getEntityType()!=1 && userSession.getEntityType()!=5 && userSession.getEntityType()!=6){
				map.addAttribute("DistrictName",userSession.getDistrictId().getDistrictName());
				map.addAttribute("DistrictId",userSession.getDistrictId().getDistrictId());
			}else{
				map.addAttribute("DistrictName",null);
			}
			
			if(userSession.getEntityType()==2){
				map.addAttribute("DistrictName",userSession.getDistrictId().getDistrictName());
				map.addAttribute("DistrictId",userSession.getDistrictId().getDistrictId());
				map.addAttribute("activeuser",userMasterDAO.getUserByDistrict(userSession.getDistrictId()));
				
			}
			if(userSession.getEntityType()==3){
				map.addAttribute("SchoolId",userSession.getSchoolId().getSchoolId());
				map.addAttribute("schoolName",userSession.getSchoolId().getSchoolName());
				map.addAttribute("activeuser",userMasterDAO.getUserBySchool(userSession.getSchoolId()));
			}else{
				map.addAttribute("SchoolName",null);
			}
			
			if(userSession.getEntityType()==5){
				HeadQuarterMaster headQuarterMaster = userSession.getHeadQuarterMaster()!=null?userSession.getHeadQuarterMaster():null;
				map.addAttribute("headQuarterMaster",headQuarterMaster);
				map.addAttribute("headQuarterId", headQuarterMaster.getHeadQuarterId());
				map.addAttribute("headQuarter", headQuarterMaster.getHeadQuarterName());
			}
			
			if(userSession.getEntityType()==6){
				BranchMaster branchMaster = userSession.getBranchMaster()!=null?userSession.getBranchMaster():null;
				map.addAttribute("branchMaster",branchMaster);
				map.addAttribute("branchId", branchMaster.getBranchId());
				map.addAttribute("headQuarter", branchMaster.getHeadQuarterMaster().getHeadQuarterName());
			}
						
			TestTool.getTraceTime("102");
			List<StatusMaster> lstStatusMasters = new ArrayList<StatusMaster>();
			/* transfer it in Ajax By Anurag    String[] statusTotal = {"comp","icomp","hird","rem","widrw","vlt"};
			try{
				lstStatusMasters = statusMasterDAO.findStatusByShortNames(statusTotal);
			}catch (Exception e) {
				e.printStackTrace();
			}  */
			List<StateMaster> lstStateMaster = new ArrayList<StateMaster>();
			/*Anurag   lstStateMaster = stateMasterDAO.findAllStateByOrder();   */
			TestTool.getTraceTime("103");
			// @Ashish :: Add job Category list
			DistrictMaster districtMaster = null;
			HeadQuarterMaster headQuarterMaster = null;
			BranchMaster branchMaster = null;
			List<JobCategoryMaster> lstJobCategoryMasters = new ArrayList<JobCategoryMaster>();
			
			boolean statusNotes=false;
			boolean writePrivilegeToSchool = false;
			boolean achievementScore=false,tFA=false,demoClass=false,JSI=false,teachingOfYear =false,expectedSalary=false,fitScore=false;
			boolean isTeacherPoolOnLoad = false;
			StringBuffer taglists=new StringBuffer();
			if(entityID!=1)
			{
				if(userSession.getDistrictId()!=null)
				{
					districtMaster = districtMasterDAO.findById(userSession.getDistrictId().getDistrictId(), false, false);
					/*Anurag same on Ajax By Anurag   List<SecondaryStatusMaster> listSecondaryStatusMasters =   secondaryStatusMasterDAO.findTagswithDistrictInCandidatePool(districtMaster);
					if(listSecondaryStatusMasters!=null && listSecondaryStatusMasters.size()>0){
						for(SecondaryStatusMaster ssm : listSecondaryStatusMasters){
							taglists.append("<option value='"+ssm.getSecStatusId()+"'>"+ssm.getSecStatusName()+"</option>");	
						}
					}

					lstJobCategoryMasters = jobCategoryMasterDAO.findAllJobCategoryNameByDistrict(districtMaster);     */
					try{

						if(districtMaster.getDisplayAchievementScore()){
							achievementScore=true;
						}
						if(districtMaster.getDisplayTFA()){
							tFA=true;
						}
						if(districtMaster.getDisplayDemoClass()){
							demoClass=true;
						}
						if(districtMaster.getDisplayJSI()){
							JSI=true;
						}
						if(districtMaster.getDisplayYearsTeaching()){
							teachingOfYear=true;
						}
						if(districtMaster.getDisplayExpectedSalary()){
							expectedSalary=true;
						}
						if(districtMaster.getDisplayFitScore()){
							fitScore=true;
						}

						if(districtMaster.getStatusNotes())
							statusNotes=true;

						writePrivilegeToSchool = districtMaster.getWritePrivilegeToSchool();
						isTeacherPoolOnLoad = districtMaster.getIsTeacherPoolOnLoad()==null?false:districtMaster.getIsTeacherPoolOnLoad()==true;

					}catch(Exception e){ e.printStackTrace();}
				}
				else if(userSession.getHeadQuarterMaster()!=null)
				{
					headQuarterMaster = userSession.getHeadQuarterMaster();
					/*Anurag same on Ajax By Anurag  List<SecondaryStatusMaster> listSecondaryStatusMasters = secondaryStatusMasterDAO.findTagsWithHeadQuarterInCandidatePool(headQuarterMaster.getHeadQuarterId());
					if(listSecondaryStatusMasters!=null && listSecondaryStatusMasters.size()>0){
						for(SecondaryStatusMaster ssm : listSecondaryStatusMasters){
							taglists.append("<option value='"+ssm.getSecStatusId()+"'>"+ssm.getSecStatusName()+"</option>");	
						}
					}

					lstJobCategoryMasters = jobCategoryMasterDAO.findJobCategoryByHQAandBranchAndDistrict(headQuarterMaster,null,null);   */
					try{

						if(headQuarterMaster.getDisplayAchievementScore()){
							achievementScore=true;
						}
						/*if(headQuarterMaster.getDisplayTFA()){
							tFA=true;
						}*/
						if(headQuarterMaster.getDisplayDemoClass()){
							demoClass=true;
						}
						if(headQuarterMaster.getDisplayJSI()){
							JSI=true;
						}
						/*if(headQuarterMaster.getDisplayYearsTeaching()){
							teachingOfYear=true;
						}
						if(headQuarterMaster.getDisplayExpectedSalary()){
							expectedSalary=true;
						}*/
						if(headQuarterMaster.getDisplayFitScore()){
							fitScore=true;
						}

						if(headQuarterMaster.getStatusNotes())
							statusNotes=true;

						writePrivilegeToSchool = headQuarterMaster.getWritePrivilegeToBranch();
						isTeacherPoolOnLoad = headQuarterMaster.getIsTeacherPoolOnLoad()==null?false:headQuarterMaster.getIsTeacherPoolOnLoad()==true;

					}catch(Exception e){ e.printStackTrace();}
				}
				else if(userSession.getBranchMaster()!=null)
				{
					branchMaster = userSession.getBranchMaster();
					/*Anurag same on Ajax By Anurag    List<SecondaryStatusMaster> listSecondaryStatusMasters = secondaryStatusMasterDAO.findTagsWithBranchInCandidatePool(branchMaster.getBranchId());
					if(listSecondaryStatusMasters!=null && listSecondaryStatusMasters.size()>0){
						for(SecondaryStatusMaster ssm : listSecondaryStatusMasters){
							taglists.append("<option value='"+ssm.getSecStatusId()+"'>"+ssm.getSecStatusName()+"</option>");	
						}
					}

					lstJobCategoryMasters = jobCategoryMasterDAO.findJobCategoryByHQAandBranchAndDistrict(null,branchMaster,null);    */
					try{

						if(branchMaster.getDisplayAchievementScore()){
							achievementScore=true;
						}
						/*if(headQuarterMaster.getDisplayTFA()){
							tFA=true;
						}*/
						if(branchMaster.getDisplayDemoClass()){
							demoClass=true;
						}
						if(branchMaster.getDisplayJSI()){
							JSI=true;
						}
						/*if(headQuarterMaster.getDisplayYearsTeaching()){
							teachingOfYear=true;
						}
						if(headQuarterMaster.getDisplayExpectedSalary()){
							expectedSalary=true;
						}*/
						if(branchMaster.getDisplayFitScore()){
							fitScore=true;
						}

							statusNotes=true;

						writePrivilegeToSchool = branchMaster.getWritePrivilegeToDistrict();
						isTeacherPoolOnLoad = branchMaster.getIsTeacherPoolOnLoad()==null?false:branchMaster.getIsTeacherPoolOnLoad()==true;

					}catch(Exception e){ e.printStackTrace();}
				}
			}else
			{
				achievementScore=true;tFA=true;demoClass=true;JSI=true;teachingOfYear =true;expectedSalary=true;fitScore=true;
			}
			TestTool.getTraceTime("103");
			try{
				if(districtMaster!=null && userSession.getEntityType()!=1){
					if(districtMaster.getIsReqNoForHiring()!=null && districtMaster.getIsReqNoForHiring()){
						map.addAttribute("isReqNoForHiring",1);	
					}else{
						map.addAttribute("isReqNoForHiring",0);
					}
				}else{
					map.addAttribute("isReqNoForHiring",0);
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			
			System.out.println("*** isTeacherPoolOnLoad " +isTeacherPoolOnLoad);
			
			map.addAttribute("isTeacherPoolOnLoad",isTeacherPoolOnLoad);
			
			Map<String,Boolean> prefMap = new HashMap<String, Boolean>();
			prefMap.put("achievementScore", achievementScore);
			prefMap.put("tFA", tFA);
			prefMap.put("demoClass", demoClass);
			prefMap.put("JSI", JSI);
			prefMap.put("teachingOfYear", teachingOfYear);
			prefMap.put("expectedSalary", expectedSalary);
			prefMap.put("fitScore", fitScore);
			
			if(districtMaster!=null && districtMaster.getDistrictId().equals(7800038)){
				prefMap.put("MAScore", true);
				prefMap.put("NFScore", true);
			}
			
			
			map.addAttribute("prefMap",prefMap);
			map.addAttribute("lstStateMaster",lstStateMaster);
			map.addAttribute("lstJobCategoryMasters",lstJobCategoryMasters);
			map.addAttribute("lstStatusMasters",lstStatusMasters);
			map.addAttribute("roleAccess", roleAccess);
			map.addAttribute("timezone",timeZoneMasterDAO.getAllTimeZone());
			map.addAttribute("writePrivilegeToSchool",writePrivilegeToSchool);
			List<SubjectMaster> subjectMasters = new ArrayList<SubjectMaster>();
			
			   if(userSession.getEntityType()==2)
				subjectMasters = subjectMasterDAO.findActiveSubjectByDistrict((DistrictMaster)userSession.getDistrictId());   
			TestTool.getTraceTime("104");
			map.addAttribute("subjectMasters",subjectMasters);
			map.addAttribute("entityType",userSession.getEntityType());
			map.addAttribute("statusNotes",statusNotes);
			map.addAttribute("tagslist",taglists.toString());
			
			if(userSession.getHeadQuarterMaster()!=null && userSession.getHeadQuarterMaster().getHeadQuarterId()==2)
			{
				map.addAttribute("CheckNC",userSession.getHeadQuarterMaster().getHeadQuarterId());
				
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		TestTool.getTraceTime("199");
		return "teacherinfonew";
	}	
}
