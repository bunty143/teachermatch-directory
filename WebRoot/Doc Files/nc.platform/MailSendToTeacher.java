package tm.services;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.WebApplicationContextUtils;

import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.TeacherDetail;
import tm.bean.user.UserMaster;
import tm.services.report.CGInviteInterviewAjax;
import tm.utility.Utility;

public class MailSendToTeacher  extends Thread{
	
	String locale = Utility.getValueOfPropByKey("locale");
	
	List<UserMaster> lstuserListTRD=new ArrayList<UserMaster>();
	TeacherDetail teacherDetailTRD=null;
	JobOrder jobOrderTRD=null;
	HttpServletRequest requestTRD=null;
	String flagforEmailTRD="";
	String[] arrHrDetailTRD = new String[11];
	EmailerService emailerService = null;
	JobForTeacher jobForTeacherTRD=null;
	
	@Autowired
	CGInviteInterviewAjax cGInviteInterviewAjax;
	
	public MailSendToTeacher(TeacherDetail teacherDetail,JobOrder jobOrder,HttpServletRequest request,String[] arrHrDetail, String flagforEmail, List<UserMaster> lstusermaster,EmailerService emailerService,JobForTeacher jobForTeacher){
		try
		{
			lstuserListTRD =lstusermaster;
			teacherDetailTRD=teacherDetail;
			jobOrderTRD=jobOrder;
			requestTRD=request;
			flagforEmailTRD=flagforEmail;
			arrHrDetailTRD=arrHrDetail;
			this.emailerService = emailerService;
			jobForTeacherTRD=jobForTeacher;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
		
	}
	public void run() {
		try {
			//System.out.println(" \n \t Gagan : [ACA] external teacherDetailTRD:::::"+teacherDetailTRD.getTeacherId()+"\n jobId : "+jobOrderTRD.getJobTitle()+" ");
			Thread.sleep(1000);
			if(flagforEmailTRD.equalsIgnoreCase("applyJob"))
			{
				System.out.println(" ::::::::::::::::::Apply Mail::::::::::::::::: jobOrderTRD.getDistrictMaster().getDistrictId() :: "+jobOrderTRD.getDistrictMaster().getDistrictId());
				
				if(jobOrderTRD.getDistrictMaster().getDistrictId()!=1200390 && jobOrderTRD.getDistrictMaster().getDistrictId()!=7800038)
				{
					System.out.println("***************************** Apply Job Email ********************** jobOrderTRD.getDistrictMaster().getDistrictId() "+jobOrderTRD.getDistrictMaster().getDistrictId());
					if((jobForTeacherTRD.getStatus()!=null && jobForTeacherTRD.getStatus().getStatusId()==4 && (jobOrderTRD.getDistrictMaster().getDistrictId()==614730)) || ( jobOrderTRD.getDistrictMaster().getDistrictId()!=614730))
					{
						try {
							System.out.println("jobForTeacherTRD.getStatus().getStatusId() "+jobForTeacherTRD.getStatus().getStatusId());
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						//emailerService.sendMailAsHTMLText(teacherDetailTRD.getEmailAddress(),"You Have Successfully Applied for "+jobOrderTRD.getJobTitle()+" Position at "+arrHrDetailTRD[6]+"", MailText.mailtoTeacherAfterJobApply(requestTRD, teacherDetailTRD, jobOrderTRD,arrHrDetailTRD));
						emailerService.sendMailAsHTMLText(teacherDetailTRD.getEmailAddress(),Utility.getLocaleValuePropByKey("msgSuccApplied", locale)+ " " + jobOrderTRD.getJobTitle() + " " +Utility.getLocaleValuePropByKey("msgPositionAt", locale) + " " +arrHrDetailTRD[6]+"", MailText.mailtoTeacherAfterJobApply(requestTRD, teacherDetailTRD, jobOrderTRD,arrHrDetailTRD));
						
						if(lstuserListTRD.size()>0)
						{
							for(UserMaster userMailSend :lstuserListTRD)
							{
								emailerService.sendMailAsHTMLText(userMailSend.getEmailAddress(),teacherDetailTRD.getFirstName()+" "+teacherDetailTRD.getLastName()+" "+Utility.getLocaleValuePropByKey("msgAppliedFor", locale)+" "+jobOrderTRD.getJobTitle()+" "+Utility.getLocaleValuePropByKey("lblPosition", locale)+"", MailText.mailtoUsersAfterJobApply(requestTRD, teacherDetailTRD, jobOrderTRD,arrHrDetailTRD,userMailSend));
							}
						}
					}
				}
				
				//For VVI
				if(jobForTeacherTRD.getStatus()!=null && jobForTeacherTRD.getStatus().getStatusId()==4 && jobOrderTRD!=null && jobOrderTRD.getJobCompletedVVILink()!=null && jobOrderTRD.getJobCompletedVVILink())
				{
					System.out.println("***************************** VVI Email *********************");
						try {
							System.out.println("jobForTeacherTRD.getStatus().getStatusId() 111111111111111111 "+jobForTeacherTRD.getStatus().getStatusId());
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						try{
							ApplicationContext context0 =  WebApplicationContextUtils.getWebApplicationContext(ContextLoaderListener.getCurrentWebApplicationContext().getServletContext());
							cGInviteInterviewAjax = (CGInviteInterviewAjax)context0.getBean("cGInviteInterviewAjax");
							cGInviteInterviewAjax.saveIIStatus(jobOrderTRD.getJobId().toString(), teacherDetailTRD.getTeacherId().toString(), jobOrderTRD.getI4QuestionSets().getID().toString());
						}catch(Exception e){
							e.printStackTrace();
						}
				}
			}else
			{
				System.out.println(" [ ======== ELSE block  flagforEmailTRD ]"+flagforEmailTRD+" arrHrDetailTRD "+arrHrDetailTRD);
				if(flagforEmailTRD.equalsIgnoreCase("withdrawJob"))
					emailerService.sendMailAsHTMLText(teacherDetailTRD.getEmailAddress(),"You have withdrawn your application for "+jobOrderTRD.getJobTitle()+" Position at "+arrHrDetailTRD[6], MailText.getWithdrawMailToTeacher(requestTRD, teacherDetailTRD, jobOrderTRD,arrHrDetailTRD));
			}
			
			//emailerService.sendMailAsHTMLText(teacherDetailTRD.getEmailAddress(),"You have successfully applied this job", MailText.mailtoTeacherAfterJobApply(requestTRD, teacherDetailTRD, jobOrderTRD,schoolDistrictName,hrContactFirstName,hrContactLastName,title,phoneNumber,isHrContactExist));
			System.out.println(" Mail Sent Successfully");
		}catch (NullPointerException en) {
			en.printStackTrace();
		}catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}	
