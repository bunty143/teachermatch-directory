package tm.services.quartz;

import java.io.PrintStream;
import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.impl.StdSchedulerFactory;
import tm.utility.Utility;

public class StatusSchedule
{
  Scheduler sche = null;
  
  public StatusSchedule()
    throws Exception
  {
    SchedulerFactory sf = new StdSchedulerFactory();
    this.sche = sf.getScheduler();
    this.sche.start();
    
    int hour = Integer.parseInt(Utility.getValueOfPropByKey("jobAlertHour"));
    int minutes = Integer.parseInt(Utility.getValueOfPropByKey("jobAlertMinutes"));
    try
    {
      JobDetail nobleStDspqReport = new JobDetail("nobleStDspqReport", "DEFAULT", NobleStDspqReport.class);
      CronTrigger nobleStDspqReportCron = new CronTrigger("Cron nobleStDspqReport", "DEFAULT", "0 0 23 * * ?");
      
      this.sche.scheduleJob(nobleStDspqReport, nobleStDspqReportCron);
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    try
    {
      JobDetail nobleStJobsReport = new JobDetail("nobleStJobsReport", "DEFAULT", NobleStJobsReport.class);
      CronTrigger nobleStJobsReportCron = new CronTrigger("Cron nobleStJobsReport", "DEFAULT", "0 0 23 * * ?");
      
      this.sche.scheduleJob(nobleStJobsReport, nobleStJobsReportCron);
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    try
    {
      JobDetail nobleStApplicationreport = new JobDetail("nobleStApplicationreport", "DEFAULT", NobleStApplicationreport.class);
      CronTrigger nobleStApplicationreportCron = new CronTrigger("Cron nobleStApplicationreport", "DEFAULT", "0 0 23 * * ?");
      
      this.sche.scheduleJob(nobleStApplicationreport, nobleStApplicationreportCron);
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    try
    {
      JobDetail nobleStCandidateReport = new JobDetail("nobleStCandidateReport", "DEFAULT", NobleStCandidateReport.class);
      CronTrigger nobleStCandidateReportCron = new CronTrigger("Cron nobleStCandidateReport", "DEFAULT", "0 0 23 * * ?");
      
      this.sche.scheduleJob(nobleStCandidateReport, nobleStCandidateReportCron);
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    try
    {
      JobDetail nobleStSchoolPostionReport = new JobDetail("nobleStSchoolPostionReport", "DEFAULT", NobleStSchoolPostionReport.class);
      CronTrigger nobleStSchoolPostionReportCron = new CronTrigger("Cron nobleStSchoolPostionReport", "DEFAULT", "0 0 23 * * ?");
      
      this.sche.scheduleJob(nobleStSchoolPostionReport, nobleStSchoolPostionReportCron);
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    try
    {
      String applicantCronExp = Utility.getValueOfPropByKey("applicantCronExp");
      JobDetail applicantHire = new JobDetail("applicantHire", "DEFAULT", ApplicantHire.class);
      CronTrigger applicantHireCron = new CronTrigger("Cron Applicant Hire", "DEFAULT", applicantCronExp);
      this.sche.scheduleJob(applicantHire, applicantHireCron);
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    JobDetail jDetailUS = new JobDetail("Job Alerts", "DEFAULT", JobAlertsServices.class);
    CronTrigger cronTriggerUS = new CronTrigger("Cron Job Alerts", "DEFAULT", "0 " + minutes + " " + hour + " ? * *"); //1:10
    this.sche.scheduleJob(jDetailUS, cronTriggerUS);
    
    hour = Integer.parseInt(Utility.getValueOfPropByKey("jobAlertOnSocialSitesHour"));
    minutes = Integer.parseInt(Utility.getValueOfPropByKey("jobAlertOnSocialSitesMinutes"));
    
    JobDetail jDetailSocial = new JobDetail("Job Post", "DEFAULT", PostJobsOnSocialSitesService.class);
    CronTrigger cronTriggerSocial = new CronTrigger("Cron Job Post", "DEFAULT", "0 " + minutes + " " + hour + " ? * *"); //18:35
    this.sche.scheduleJob(jDetailSocial, cronTriggerSocial);
    
    hour = Integer.parseInt(Utility.getValueOfPropByKey("weeklyCgReportHour"));
    minutes = Integer.parseInt(Utility.getValueOfPropByKey("weeklyCgReportMinutes"));
    String day = Utility.getValueOfPropByKey("weeklyCgReportDay");
    
    JobDetail jWeeklyCgReport = new JobDetail("WeeklyCgReport", "DEFAULT", SendWeeklyCgReportService.class);
    CronTrigger cronTriggerWeeklyCgReport = new CronTrigger("Cron WeeklyCgReport", "DEFAULT", "0 " + minutes + " " + hour + " ? * " + day);  //5:00 TUE weekly
    
    this.sche.scheduleJob(jWeeklyCgReport, cronTriggerWeeklyCgReport);
    
    hour = Integer.parseInt(Utility.getValueOfPropByKey("cgUpdateHour"));
    minutes = Integer.parseInt(Utility.getValueOfPropByKey("cgUpdateMinutes"));
    
    JobDetail jbUpdataCGData = new JobDetail("cgDataUpdateService", "DEFAULT", CGDataUpdateService.class);
    
    CronTrigger cronTriggerUpdataCGData = new CronTrigger("Cron cgDataUpdateService", "DEFAULT", "0 0 5,11,15,20 ? * *"); //5,11,15,20
    
    this.sche.scheduleJob(jbUpdataCGData, cronTriggerUpdataCGData);
    
    hour = Integer.parseInt(Utility.getValueOfPropByKey("internalCandidateTransferHour"));
    minutes = Integer.parseInt(Utility.getValueOfPropByKey("internalCandidateTransferMinutes"));
    
    JobDetail jbInternalCandidateTransfer = new JobDetail("internalCandidateTransferService", "DEFAULT", InternalCandidateTransferService.class);
    CronTrigger cronInternalCandidateTransferService = new CronTrigger("Cron internalCandidateTransferService", "DEFAULT", "0 " + minutes + " " + hour + " ? * *");// 22:30
    this.sche.scheduleJob(jbInternalCandidateTransfer, cronInternalCandidateTransferService);
    
    hour = Integer.parseInt(Utility.getValueOfPropByKey("noResponseEmailHour"));
    minutes = Integer.parseInt(Utility.getValueOfPropByKey("noResponseEmailMinutes"));
    
    JobDetail jftNoResponseEmail = new JobDetail("NoResponseCandidate", "DEFAULT", NoResponseCandidate.class);
    
    CronTrigger cronTriggerNoResponseEmail = new CronTrigger("Cron NoResponseCandidate", "DEFAULT", "0 0/5 * * * ?"); // every 5 minute
    this.sche.scheduleJob(jftNoResponseEmail, cronTriggerNoResponseEmail);
    
    hour = Integer.parseInt(Utility.getValueOfPropByKey("jobReminderHour"));
    minutes = Integer.parseInt(Utility.getValueOfPropByKey("jobReminderMinutes"));
    
    JobDetail jJobReminder = new JobDetail("jobReminder", "DEFAULT", JobReminderService.class);
    CronTrigger cronJobReminder = new CronTrigger("Cron jobReminder", "DEFAULT", "0 " + minutes + " " + hour + " ? * *"); // 4:30
    this.sche.scheduleJob(jJobReminder, cronJobReminder);
    
    JobDetail elUpdataIndex = new JobDetail("elasticSearchIndexUpdateService", "DEFAULT", ElasticSearchIndexUpdateService.class);
    
    CronTrigger cronTriggerelUpdataIndex = new CronTrigger("Cron elasticSearchIndexUpdateService", "DEFAULT", "0 0/56 * * * ?"); // every 56 minute
    this.sche.scheduleJob(elUpdataIndex, cronTriggerelUpdataIndex);
    
    JobDetail elasticSearchDistrictJobBoardUpdateService = new JobDetail("elasticSearchDistrictJobBoardUpdateService", "DEFAULT", ElasticSearchDistrictJobBoardUpdateService.class);
    
    CronTrigger elasticSearchDistrictJobBoardUpdateServiceCron = new CronTrigger("Cron elasticSearchDistrictJobBoardUpdateService", "DEFAULT", "0 0/59 * * * ?"); // every 59 minute
    this.sche.scheduleJob(elasticSearchDistrictJobBoardUpdateService, elasticSearchDistrictJobBoardUpdateServiceCron);
    
    JobDetail elasticSearchSchoolJobOrderUpdateService = new JobDetail("elasticSearchSchoolJobOrderUpdateService", "DEFAULT", ElasticSearchSchoolJobOrderUpdateService.class);
    CronTrigger schoolJobOrder = new CronTrigger("Cron elasticSearchSchoolJobOrderUpdateService", "DEFAULT", "0 0/2 * * * ?"); // every 2 hrs daily
    this.sche.scheduleJob(elasticSearchSchoolJobOrderUpdateService, schoolJobOrder);
    
    /*JobDetail elasticSearchJobsOfInterestNotCandidateUpdateService = new JobDetail("elasticSearchJobsOfInterestNotCandidateUpdateService", "DEFAULT", ElasticSearchJobsOfInterestNotCandidateUpdateService.class);
    CronTrigger JobsOfInterestNotCandidate = new CronTrigger("Cron elasticSearchJobsOfInterestNotCandidateUpdateService", "DEFAULT", "0 0 0/1 1/1 * ? *"); // every 1 hr daily
    this.sche.scheduleJob(elasticSearchJobsOfInterestNotCandidateUpdateService, JobsOfInterestNotCandidate);*/
    
    try
    {
      JobDetail mailCommunicationSch = new JobDetail("mailCommunicationSch", "DEFAULT", MailCommunicationSch.class);
      CronTrigger mailCommunicationSchCron = new CronTrigger("Cron mailCommunicationSch", "DEFAULT", "0 0/10 * * * ?"); // every 10 minute
      
      this.sche.scheduleJob(mailCommunicationSch, mailCommunicationSchCron);

    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    try{
        JobDetail jAppReminder = new JobDetail("appReminder",Scheduler.DEFAULT_GROUP,ApplicantManagementReminderService.class);
        CronTrigger cronAppReminder = new CronTrigger("Cron appReminder",Scheduler.DEFAULT_GROUP," 0 0 22 * * ?");
        sche.scheduleJob(jAppReminder, cronAppReminder);
       }catch(Exception exception){
            exception.printStackTrace();
       }
  }
  
  public void shutDownScheduler()
  {
    try
    {
      this.sche.shutdown();
      if (this.sche.isShutdown())
      {
        System.out.println("Scheduler is shutdown!");
        System.out.println("Job cann't be executed here.");
      }
      else
      {
        System.out.println("Scheduler isn't shutdown!");
        System.out.println("Job is executed here.");
      }
    }
    catch (SchedulerException e)
    {
      e.printStackTrace();
    }
  }
}
