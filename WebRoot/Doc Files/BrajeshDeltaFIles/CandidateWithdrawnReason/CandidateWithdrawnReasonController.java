package tm.controller.report;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import tm.bean.master.DistrictMaster;
import tm.bean.user.UserMaster;
import tm.dao.master.DistrictMasterDAO;
import tm.utility.Utility;


@Controller
public class CandidateWithdrawnReasonController 
{

	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	
	
	 @RequestMapping(value="/candidateWithdrawnReason.do", method=RequestMethod.GET)
		public String candidateEECData(ModelMap map,HttpServletRequest request)
		{
		 try 
			{
				UserMaster userMaster=null;
				DistrictMaster districtMaster=null;
				
				HttpSession session = request.getSession(false);
				int roleId=0;
				if (session == null || session.getAttribute("userMaster") == null) 
				{
					return "redirect:index.jsp";
				}else{
					userMaster=	(UserMaster) session.getAttribute("userMaster");
					if(userMaster.getRoleId().getRoleId()!=null){
						roleId=userMaster.getRoleId().getRoleId();
					}
				}
				String roleAccess=null;
				String jobAuthKey=(Utility.randomString(8)+Utility.getDateTime());
				map.addAttribute("jobAuthKey",jobAuthKey);
				map.addAttribute("roleAccess", roleAccess);
				
				
				
				
				
				if(userMaster.getEntityType()!=1){
					map.addAttribute("DistrictOrSchoolName",userMaster.getDistrictId().getDistrictName());
					map.addAttribute("DistrictOrSchoolId",userMaster.getDistrictId().getDistrictId());
				}else{
					map.addAttribute("DistrictOrSchoolName",null);
				}
				map.addAttribute("JobOrderType","2");
				map.addAttribute("userMaster",userMaster);
			
				if(userMaster.getEntityType()==3){
					districtMaster=districtMasterDAO.findById(userMaster.getDistrictId().getDistrictId(),false, false);

					map.addAttribute("writePrivilegFlag",districtMaster.getWritePrivilegeToSchool());
					map.addAttribute("schoolName",userMaster.getSchoolId().getSchoolName());
					
			
					map.addAttribute("schoolId",userMaster.getSchoolId().getSchoolId());
					
					map.addAttribute("addJobFlag",false);
				}
				
				
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
		
		return "candidateWithdrawnReason";

		}
	

	
}
